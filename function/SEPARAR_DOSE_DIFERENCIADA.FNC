create or replace 
function separar_dose_diferenciada(
	ds_dose_diferenciada_p		varchar2,
	indice_p				number
) return varchar2 is

ds_dose_diferenciada_w	varchar2(255) default '0';

begin

if (indice_p > 0 and indice_p < 5 and ds_dose_diferenciada_p is not null) then
	select 	ds_dose_diferenciada
	into	ds_dose_diferenciada_w
	from(
		select rownum indice, regexp_substr(ds_dose_diferenciada_p,'[^-]+', 1, level) ds_dose_diferenciada from dual
		connect by regexp_substr(ds_dose_diferenciada_p, '[^-]+', 1, level) is not null
	)
	where	indice = indice_p;
end if;

return	ds_dose_diferenciada_w;

end separar_dose_diferenciada;
/