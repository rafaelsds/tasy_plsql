create or replace
function obter_desc_fase_tratamento (	nr_seq_fase_p		number)
				return varchar is

nm_fase_w	varchar2(40):='';

begin

if	(nr_seq_fase_p is not null) then
	select	nm_fase
	into	nm_fase_w
	from	rxt_fase_tratamento
	where	nr_sequencia = nr_seq_fase_p;
end if;	

return nm_fase_w;

end obter_desc_fase_tratamento;
/