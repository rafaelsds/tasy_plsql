create or replace function GET_LEGEND_VITAL_SIGN (nm_campo_p varchar2, valor_p number,nr_atendimento_p number,nr_seq_sinal_p number)
                                            return number is
  valor_w                   number(20,4);
  qt_idade_w				number(15,4);
  qt_idade_dia_w			number(15,4);
  ie_rn_w                   varchar2(1):='N';
  cd_pessoa_fisica_w  pessoa_fisica.cd_pessoa_fisica%type;
  cd_setor_Atendimento_w	number(10)	:=0;
  cd_escala_dor_w           VARCHAR2(5):= null;
  cd_perfil_w				number(10):=0;
  ie_sexo_w				    varchar2(1);  
  qt_min_aviso_w			number(15,4);
  qt_max_aviso_w			number(15,4);
  qt_minimo_per_w			number(15,4);
  vl_maximo_per_w			number(15,4);
  nr_seq_item_w      		number(10);
  cd_unidade_medida_w	varchar2(5):= CHR(176)||'C';
  ie_legenda_w number(10):=0;

cursor c03 is
select	b.qt_min_aviso,
	b.qt_max_aviso,
	b.qt_minimo,
	b.vl_maximo,
	b.nr_seq_sinal
from	sinal_vital_regra b,
	sinal_vital a
where	a.nm_atributo	= nm_campo_p
and	a.nr_sequencia	= b.nr_seq_sinal
and	(qt_idade_w  between nvl(b.qt_idade_min,0) and nvl(b.qt_idade_max,9999999999))
and	(qt_idade_dia_w between nvl(b.qt_idade_min_dias,0) and nvl(b.qt_idade_max_dias,9999999999))
and	nvl(b.cd_setor_Atendimento,cd_setor_Atendimento_w)	= cd_setor_Atendimento_w
and	(b.cd_escala_dor is null or 
	cd_escala_dor_w is null or 
	b.cd_escala_dor = cd_escala_dor_w)
and	nvl(b.cd_estabelecimento,Wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
and	nvl(b.cd_perfil,cd_perfil_w) = cd_perfil_w
and	nvl(b.ie_sexo,ie_sexo_w) = ie_sexo_w
order by nvl(b.cd_setor_atendimento,0), nvl(b.cd_escala_dor,'0') desc, nvl(b.cd_estabelecimento,0), nvl(b.ie_sitio,0) asc;

begin 
	
valor_w :=valor_p;
cd_perfil_w	:= nvl(obter_perfil_ativo,0);
cd_pessoa_fisica_w	:= obter_pessoa_atendimento(nr_atendimento_p,'C');
select OBTER_SE_RECEM_NASC(nr_atendimento_p) into ie_rn_w from dual;
if	(nvl(ie_rn_w,'N') = 'S') then
	qt_idade_w	:= null;
	qt_idade_dia_w	:= 0;
else
	select	max(obter_idade(dt_nascimento,sysdate,'A')),max(obter_idade(dt_nascimento,sysdate,'DIA'))
	into	qt_idade_w, qt_idade_dia_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
end if;
begin
if	(nvl(nr_atendimento_p,0) > 0) then
	cd_setor_Atendimento_w	:= nvl(obter_setor_atendimento(nr_atendimento_p),0);
end if;
exception
when others then
	cd_setor_Atendimento_w	:= 0;
end;
	
if	(nvl(nr_atendimento_p,0) > 0) then
	select	max(nvl(obter_dados_pf_sbis(nr_atendimento_p,'SEXO'),ie_sexo))
	into	ie_sexo_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
else
	select	max(ie_sexo)
	into	ie_sexo_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
end if;	

if (nr_seq_sinal_p is not null) then
	select nvl(max(IE_UM_TEMP),CHR(176)||'C') ,max(cd_escala_dor) into cd_unidade_medida_w ,cd_escala_dor_w from  atendimento_sinal_vital where nr_atendimento =  nr_atendimento_p
	and nr_sequencia = nr_seq_sinal_p;
end if;
	
	
	open C03;
	loop
	fetch C03 into	
		qt_min_aviso_w,
		qt_max_aviso_w,
		qt_minimo_per_w,
		vl_maximo_per_w,
		nr_seq_item_w;
	exit when C03%notfound;
	end loop;
	close C03;
	if	((qt_min_aviso_w is null)and
	 (qt_max_aviso_w is null)    and
	 (qt_minimo_per_w is null)   and
	 (vl_maximo_per_w is null)) then
		begin
			select	nvl(max(a.qt_min_aviso),0),
				nvl(max(a.qt_max_aviso),0),
				nvl(max(a.qt_minimo),0),
				nvl(max(a.vl_maximo),0),
				max(nr_sequencia)
			into	qt_min_aviso_w,
				qt_max_aviso_w,
				qt_minimo_per_w,
				vl_maximo_per_w,
				nr_seq_item_w
			from	sinal_vital a
			where	a.nm_atributo	= nm_campo_p;
		end;
	end if;
	
if	(valor_w > 0) and
	(nr_seq_item_w = 4) and
	(cd_unidade_medida_w = CHR(176)||'F') then
	valor_w := ((valor_w - 32) / 1.8); -- Converter de FAHRENHEIT para CELSIUS
end if;
	
if	((qt_min_aviso_w = 0) and
	 (qt_max_aviso_w = 0) and
	 (qt_minimo_per_w = 0) and
	 (vl_maximo_per_w = 0)) or ((valor_w = 0) or
	(valor_w between qt_min_aviso_w and qt_max_aviso_w)) then
	ie_legenda_w := 0;
elsif	((qt_minimo_per_w > 0) or (vl_maximo_per_w > 0)) and
	((qt_min_aviso_w > 0) or (qt_max_aviso_w > 0)) and
	(not (valor_w between qt_minimo_per_w and vl_maximo_per_w)) then
	ie_legenda_w := 11244;
elsif	((qt_minimo_per_w > 0) or (vl_maximo_per_w > 0)) and
	((qt_min_aviso_w > 0) or (qt_max_aviso_w > 0)) and
	(not (valor_w between qt_min_aviso_w and qt_max_aviso_w)) then
	ie_legenda_w := 11244;
elsif	(qt_minimo_per_w = 0) and
	(vl_maximo_per_w = 0) and
	((qt_min_aviso_w > 0) or (qt_max_aviso_w > 0)) and
	(not (valor_w between qt_min_aviso_w and qt_max_aviso_w)) then	
	ie_legenda_w := 11244;
elsif	(qt_min_aviso_w = 0) and
	(qt_max_aviso_w = 0) and
	((qt_minimo_per_w > 0) or (vl_maximo_per_w > 0)) and
	(not (valor_w between qt_minimo_per_w and vl_maximo_per_w)) then
	ie_legenda_w := 11244;
elsif	(not (valor_w between qt_minimo_per_w and vl_maximo_per_w)) then
	ie_legenda_w := 11244;
end if;		
	
return ie_legenda_w;

end  GET_LEGEND_VITAL_SIGN;
/