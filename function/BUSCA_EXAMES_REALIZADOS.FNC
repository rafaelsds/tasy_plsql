create or replace
FUNCTION BUSCA_EXAMES_REALIZADOS(nr_atendimento_p	number)
return varchar2 is

ds_retorno_w varchar2(255) := null;
qtd_rel_rx  number(10,0);
qtd_rel_rm  number(10,0);
qtd_rel_tc  number(10,0);
qtd_rel_us  number(10,0);
begin
  select count(1)
    into qtd_rel_rx
    from prescr_medica pm
   inner join prescr_procedimento pp on pp.nr_prescricao = pm.nr_prescricao and pp.ie_status_execucao >= '20'
   inner join proc_interno pi on pi.nr_sequencia = pp.nr_seq_proc_interno
   inner join procedimento p on p.cd_procedimento = pi.cd_procedimento and 
                                p.ie_origem_proced = pi.ie_origem_proced and
                                p.cd_tipo_procedimento in (1, 34, 97, 156, 104)
   where pm.nr_atendimento = nr_atendimento_p; 

  if(qtd_rel_rx > 0) then
    ds_retorno_w := obter_desc_expressao(948352)|| '(' || qtd_rel_rx || ')';
  end if; 

  select count(1)
    into qtd_rel_rm
    from prescr_medica pm
   inner join prescr_procedimento pp on pp.nr_prescricao = pm.nr_prescricao and pp.ie_status_execucao >= '20'
   inner join proc_interno pi on pi.nr_sequencia = pp.nr_seq_proc_interno
   inner join procedimento p on p.cd_procedimento = pi.cd_procedimento and 
                                p.ie_origem_proced = pi.ie_origem_proced and
                                p.cd_tipo_procedimento in (4)
   where pm.nr_atendimento = nr_atendimento_p;   

  if(qtd_rel_rm > 0) then
    if(ds_retorno_w is null) then
      ds_retorno_w := obter_desc_expressao(948350)|| '(' || qtd_rel_rm || ')';
    else
      ds_retorno_w := ds_retorno_w || ' ' || obter_desc_expressao(948350)|| '(' || qtd_rel_rm || ')';    
    end if;
  end if;  
  
  select count(1)
    into qtd_rel_tc
    from prescr_medica pm
   inner join prescr_procedimento pp on pp.nr_prescricao = pm.nr_prescricao and pp.ie_status_execucao >= '20'
   inner join proc_interno pi on pi.nr_sequencia = pp.nr_seq_proc_interno
   inner join procedimento p on p.cd_procedimento = pi.cd_procedimento and 
                                p.ie_origem_proced = pi.ie_origem_proced and
                                p.cd_tipo_procedimento in (3)
   where pm.nr_atendimento = nr_atendimento_p;    

  if(qtd_rel_tc > 0) then
    if(ds_retorno_w is null) then
      ds_retorno_w := obter_desc_expressao(948356)|| '(' || qtd_rel_tc || ')';
    else
      ds_retorno_w := ds_retorno_w || ' ' || obter_desc_expressao(948356)|| '(' || qtd_rel_tc || ')';    
    end if;
  end if;  

  select count(1)
    into qtd_rel_us
    from prescr_medica pm
   inner join prescr_procedimento pp on pp.nr_prescricao = pm.nr_prescricao and pp.ie_status_execucao >= '20'
   inner join proc_interno pi on pi.nr_sequencia = pp.nr_seq_proc_interno
   inner join procedimento p on p.cd_procedimento = pi.cd_procedimento and 
                                p.ie_origem_proced = pi.ie_origem_proced and
                                p.cd_tipo_procedimento in (25, 2, 121)
   where pm.nr_atendimento = nr_atendimento_p; 

  if(qtd_rel_us > 0) then
    if(ds_retorno_w is null) then
      ds_retorno_w := obter_desc_expressao(948348)|| '(' || qtd_rel_us || ')';
    else
      ds_retorno_w := ds_retorno_w || ' ' || obter_desc_expressao(948348)|| '(' || qtd_rel_us || ')';    
    end if;
  end if;  

  return ds_retorno_w;
END BUSCA_EXAMES_REALIZADOS;
/