create or replace function calculate_patient_days(	month_p 	number,
                                                    year_p number)

return number is

patient_days_w		number := 0;

begin

if(month_p is not null and year_p is not null ) then
select 
nvl(sum (pkg_date_utils.get_diffdate (dt_entrada_unidade,dt_saida_unidade,'DAY'))+ 1 , 0)
into patient_days_w 
from atend_paciente_unidade 
where dt_entrada_unidade between PKG_DATE_UTILS.get_Date(year_p,month_p,1) and  fim_dia(last_day(PKG_DATE_UTILS.get_Date(year_p,month_p,1)));

end if;

return patient_days_w;



end calculate_patient_days;

/
