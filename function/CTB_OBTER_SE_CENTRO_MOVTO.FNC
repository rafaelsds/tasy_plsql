create or replace
function ctb_obter_se_centro_movto(	nr_seq_movimento_p	number,
				cd_centro_custo_p		number)
				return varchar2 is

ie_existe_w			varchar2(1)	:= 'S';
qt_registro_w			number(10)	:= 0;

begin

if	(nvl(cd_centro_custo_p,0) <> 0) then
	
	select	count(*)
	into	qt_registro_w
	from	ctb_movto_centro_custo
	where	nr_seq_movimento	= nr_seq_movimento_p;
	
	ie_existe_w	:= 'N';

	if	(qt_registro_w > 0) then
		
		select	nvl(max('S'),'N')
		into	ie_existe_w
		from	dual
		where	exists(	select	1
			from	ctb_movto_centro_custo y
			where	y.nr_seq_movimento	= nr_seq_movimento_p
			and	y.cd_centro_custo	= cd_centro_custo_p);
	end if;
	
end if;

return	ie_existe_w;

end ctb_obter_se_centro_movto;
/