create or replace
function fin_obter_mtvo_fora_politica	(	nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);

begin
select	substr(max(a.ds_just_7_dias),1,255)
into	ds_retorno_w
from	fin_gv_pend_aprov a
where	a.nr_seq_viagem = nr_sequencia_p;

return	ds_retorno_w;

end fin_obter_mtvo_fora_politica;
/