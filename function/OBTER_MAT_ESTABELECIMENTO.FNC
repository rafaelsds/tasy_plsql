Create or Replace
Function Obter_mat_estabelecimento (	cd_estabelecimento_p		number,
				cd_estabelecimento_base_p		number,
				cd_material_p			number,
				ie_campo_p			Varchar2) 
return NUMBER is

/* campos para retorno (ie_campo_p)
MI	- qt_estoque_minimo_w
PP	- qt_ponto_pedido_w
MA	- qt_estoque_maximo_w
IR	- qt_dia_interv_ressup_w
RF	- qt_dia_ressup_forn_w
DM	- qt_dia_estoque_minimo_w
DC	- nr_minimo_cotacao_w
CM	- qt_consumo_mensal_w
CMR	- t_consumo_mensal_ressup_w
MC	- cd_material_conta_w
KT	- cd_kit_material_w
KG	- qt_peso_kg_w
DP	- Desvio padrao
QC	- qt_mes_consumo

*/

cd_estabelecimento_base_w		number(5);
qt_estoque_minimo_w		number(15,4);
qt_ponto_pedido_w			number(15,4);
qt_estoque_maximo_w		number(15,4);
qt_dia_interv_ressup_w		number(3);
qt_dia_ressup_forn_w		number(3);
qt_dia_estoque_minimo_w		number(3);
nr_minimo_cotacao_w		number(1);
qt_consumo_mensal_w		number(15,4);
qt_consumo_mensal_ressup_w	number(15,4);
cd_material_conta_w		number(6);
cd_kit_material_w			number(5);
vl_retorno_w			number(15,4);
vl_resultado_w			number(15,4);
qt_peso_kg_w			number(15,4);
qt_reg_w				Number(15,0);
qt_desvio_padrao_cons_w		number(22,4);
qt_mes_consumo_w		number(15,0);


BEGIN

select	count(*)
into	qt_reg_w
from	material_estab
where	cd_material = cd_material_p
and	cd_estabelecimento = cd_estabelecimento_p;

if	(qt_reg_w > 0) then
	select	qt_estoque_minimo,
		qt_ponto_pedido,
		qt_estoque_maximo,
		qt_dia_interv_ressup,
		qt_dia_ressup_forn,
		qt_dia_estoque_minimo,
		nr_minimo_cotacao,
		qt_consumo_mensal,
		cd_material_conta,
		cd_kit_material,
		qt_peso_kg,
		qt_desvio_padrao_cons,
		qt_mes_consumo,
		qt_consumo_mensal_ressup
	into	qt_estoque_minimo_w,
		qt_ponto_pedido_w,
		qt_estoque_maximo_w,
		qt_dia_interv_ressup_w,
		qt_dia_ressup_forn_w,
		qt_dia_estoque_minimo_w,
		nr_minimo_cotacao_w,
		qt_consumo_mensal_w,
		cd_material_conta_w,
		cd_kit_material_w,
		qt_peso_kg_w,
		qt_desvio_padrao_cons_w,
		qt_mes_consumo_w,
		qt_consumo_mensal_ressup_w
	from	material_estab
	where	cd_material = cd_material_p
	and	cd_estabelecimento = cd_estabelecimento_p;
else
	cd_estabelecimento_base_w	:= cd_estabelecimento_base_p;
	if	(cd_estabelecimento_base_p = 0) or 
		(cd_estabelecimento_base_p is null) then
		select	nvl(max(cd_estabelecimento_base),0)
		into	cd_estabelecimento_base_w
		from	parametro_geral_tasy;
	end if;
	if	(cd_estabelecimento_p = cd_estabelecimento_base_w) then
		select	qt_estoque_minimo,
			qt_ponto_pedido,
			qt_estoque_maximo,
			qt_dia_interv_ressup,
			qt_dia_ressup_forn,
			qt_dia_estoque_minimo,
			nr_minimo_cotacao,
			qt_consumo_mensal,
			cd_material_conta,
			cd_kit_material
		into	qt_estoque_minimo_w,
			qt_ponto_pedido_w,
			qt_estoque_maximo_w,
			qt_dia_interv_ressup_w,
			qt_dia_ressup_forn_w,
			qt_dia_estoque_minimo_w,
			nr_minimo_cotacao_w,
			qt_consumo_mensal_w,
			cd_material_conta_w,
			cd_kit_material_w
		from	material
		where	cd_material = cd_material_p;
	end if;
end if;

if	(ie_campo_p = 'MI') then
	vl_resultado_w	:= qt_estoque_minimo_w;
elsif	(ie_campo_p = 'PP') then
	vl_resultado_w	:= qt_ponto_pedido_w;
elsif	(ie_campo_p = 'MA') then
	vl_resultado_w	:= qt_estoque_maximo_w;
elsif	(ie_campo_p = 'IR') then
	vl_resultado_w	:= qt_dia_interv_ressup_w;
elsif	(ie_campo_p = 'RF') then
	vl_resultado_w	:= qt_dia_ressup_forn_w;
elsif	(ie_campo_p = 'DM') then
	vl_resultado_w	:= qt_dia_estoque_minimo_w;
elsif	(ie_campo_p = 'DC') then
	vl_resultado_w	:= nr_minimo_cotacao_w;
elsif	(ie_campo_p = 'CM') then
	vl_resultado_w	:= qt_consumo_mensal_w;
elsif	(ie_campo_p = 'MC') then
	vl_resultado_w	:= cd_material_conta_w;
elsif	(ie_campo_p = 'KT') then
	vl_resultado_w	:= cd_kit_material_w;
elsif	(ie_campo_p = 'KG') then
	vl_resultado_w	:= qt_peso_kg_w;
elsif	(ie_campo_p = 'DP') then
	vl_resultado_w	:= qt_desvio_padrao_cons_w;
elsif	(ie_campo_p = 'QC') then
	vl_resultado_w	:= qt_mes_consumo_w;
elsif	(ie_campo_p = 'CMR') then
	vl_resultado_w	:= qt_consumo_mensal_ressup_w;
end if;

vl_retorno_w	:= vl_resultado_w;

RETURN vl_retorno_w;

END;
/