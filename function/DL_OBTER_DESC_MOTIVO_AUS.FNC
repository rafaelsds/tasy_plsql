create or replace
function   dl_obter_desc_motivo_aus(nr_seq_motivo_aus_p number)
                                return varchar2 is

ds_motivo_aus_w  varchar2(255);

begin

select	substr(ds_motivo,1,255)
into    	ds_motivo_aus_w
from    	dl_motivo_ausencia
where   	nr_sequencia = nr_seq_motivo_aus_p;

return  ds_motivo_aus_w;

end dl_obter_desc_motivo_aus;
/
