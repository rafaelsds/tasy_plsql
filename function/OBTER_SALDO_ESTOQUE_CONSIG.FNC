create or replace
function obter_saldo_estoque_consig(
			cd_estabelecimento_p	number,
			cd_cgc_fornec_p		varchar2,
			cd_material_estoque_p	number,
			cd_local_estoque_p	number)
	return number is
			
qt_saldo_estoque_w		number(15,4)	:= 0;
dt_mesano_referencia_w		date;
cd_material_estoque_w		number(6);

begin

select	max(dt_mesano_vigente)
into 	dt_mesano_referencia_w
from 	parametro_estoque
where 	cd_estabelecimento	= cd_estabelecimento_p;

select	max(cd_material_estoque)
into	cd_material_estoque_w
from	material
where	cd_material	= cd_material_estoque_p;


select	nvl(sum(qt_estoque), 0)
into	qt_saldo_estoque_w
from	(
	select	nvl(sum(a.qt_estoque),0) qt_estoque
	from	fornecedor_mat_consignado a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_local_estoque		= cd_local_estoque_p
	and	a.cd_fornecedor		= nvl(cd_cgc_fornec_p, a.cd_fornecedor)
	and	a.cd_material		= cd_material_estoque_w
	and	nvl(cd_local_estoque_p,0) > 0
	and	a.dt_mesano_referencia	= (
		select	max(x.dt_mesano_referencia)
		from	fornecedor_mat_consignado x
		where	x.cd_estabelecimento	= a.cd_estabelecimento
		and	x.cd_local_estoque		= a.cd_local_estoque
		and	x.cd_fornecedor		= a.cd_fornecedor
		and	x.cd_material		= a.cd_material
		and	x.dt_mesano_referencia	>= dt_mesano_referencia_w)
	union all
	select	nvl(sum(a.qt_estoque),0) qt_estoque
	from	fornecedor_mat_consignado a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_fornecedor		= nvl(cd_cgc_fornec_p, a.cd_fornecedor)
	and	a.cd_material		= cd_material_estoque_w
	and	nvl(cd_local_estoque_p,0) = 0
	and	a.dt_mesano_referencia	= (
		select	max(x.dt_mesano_referencia)
		from	fornecedor_mat_consignado x
		where	x.cd_estabelecimento	= a.cd_estabelecimento
		and	x.cd_local_estoque		= a.cd_local_estoque
		and	x.cd_fornecedor		= a.cd_fornecedor
		and	x.cd_material		= a.cd_material
		and	x.dt_mesano_referencia	>= dt_mesano_referencia_w));


return qt_saldo_estoque_w;

end obter_saldo_estoque_consig;
/