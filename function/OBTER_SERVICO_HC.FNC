create or replace
function obter_servico_hc(	nr_seq_servico_p		number,
				cd_pessoa_fisica_p		varchar2)
 		    	return varchar2 is

ds_servicos_w				varchar(4000);
ie_exibir_procedimentos_w		varchar2(1);
begin

obter_param_usuario(867, 91, obter_perfil_ativo, Wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_exibir_procedimentos_w);

if	(ie_exibir_procedimentos_w = 'S') and
	(nr_seq_servico_p is not null) then

	select	max(ds_servico)
	into	ds_servicos_w
	from	hc_servico
	where	nr_sequencia = nr_seq_servico_p;
	
elsif	(ie_exibir_procedimentos_w = 'N') and
	(cd_pessoa_fisica_p is not null) then

	select	max(ltrim(sys_connect_by_path(ds_servico, ', '),', ')) ds_servico
	into	ds_servicos_w
	from	(
		select	a.ds_servico,
		row_number() over (order by ds_servico) as fila
		from	hc_servico a,
			hc_paciente_servico b,
			paciente_home_care c
		where	a.nr_sequencia = b.nr_seq_servico
		and	b.nr_seq_pac_home_care = c.nr_sequencia
		and	c.cd_pessoa_fisica = cd_pessoa_fisica_p)
	where connect_by_isleaf = 1
	start with fila = 1 
	connect by prior fila = (fila-1) and level <= 10;
end if;

return	substr(ds_servicos_w, 1, 255);

end obter_servico_hc;
/