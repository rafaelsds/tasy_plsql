create or replace
function obter_nfe_titulo_receb(
			nr_seq_nf_saida_p	varchar2,
			nr_interno_conta_p	number,
			nr_seq_protocolo_p	number)
 		    	return varchar2 is

nr_nfe_imp_w		nota_fiscal.nr_nfe_imp%type;

begin

if	(nr_seq_nf_saida_p is not null) then
	select	max(nr_nfe_imp)
	into	nr_nfe_imp_w
	from	nota_fiscal
	where	nr_sequencia = nr_seq_nf_saida_p;
elsif	(nr_interno_conta_p is not null) then
	select	max(nr_nfe_imp)
	into	nr_nfe_imp_w
	from	nota_fiscal
	where	nr_interno_conta = nr_interno_conta_p;
elsif	(nr_seq_protocolo_p is not null) then
	select	max(nr_nfe_imp)
	into	nr_nfe_imp_w
	from	nota_fiscal
	where	nr_seq_protocolo = nr_seq_protocolo_p;
end if;

return	nr_nfe_imp_w;

end obter_nfe_titulo_receb;
/