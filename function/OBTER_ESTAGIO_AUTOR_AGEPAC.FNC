create or replace
function obter_estagio_autor_agepac(	nr_seq_agenda_p	number,
					ie_opcao_p	varchar2)
						return varchar2 is

ds_retorno_w		varchar2(4000)	:= null;
nr_sequencia_autor_w	number(10);
nr_seq_estagio_w	number(10);
ds_estagio_w		varchar2(255);
cd_senha_w		varchar2(20);
ds_observacao_w		varchar2(4000);

begin

if	(nr_seq_agenda_p is not null) then

	--Alterado por lhalves em 09/12/2014 - OS 779271, para a genda primeiramente verificar se existe autorização para o procedimento da agenda.
	
	begin
	select	p.nr_sequencia_autor
	into	nr_sequencia_autor_w
	from	procedimento_autorizado p
	where	p.nr_seq_agenda	= nr_seq_agenda_p
	and	rownum	= 1;
	exception
	when others then
		select	max(nr_sequencia)
		into	nr_sequencia_autor_w
		from	autorizacao_convenio
		where	nr_seq_agenda	= nr_seq_agenda_p;		
	end;
	
	
	if (nr_sequencia_autor_w is null) and (obter_funcao_ativa = 914) then
		
		select 	max(substr(obter_valor_dominio(1227,a.ie_autorizacao),1,240)),
				max(c.nr_seq_estagio),
				max(c.cd_senha),
				max(c.ds_observacao)
		into	ds_estagio_w,
				nr_seq_estagio_w,
				cd_senha_w,
				ds_observacao_w
		from	agenda_paciente a,
				procedimento_autorizado b,
				autorizacao_convenio c
		where	a.nr_sequencia = nr_seq_agenda_p
		and		a.nr_sequencia = b.nr_seq_agenda
		and		c.nr_sequencia = b.nr_sequencia_autor;
		
		if	(ie_opcao_p	= 'C') then
			ds_retorno_w	:= nr_seq_estagio_w;
		elsif	(ie_opcao_p	= 'D') then
			ds_retorno_w	:= substr(ds_estagio_w,1,255);
		elsif	(ie_opcao_p	= 'OBS') then
			ds_retorno_w	:= ds_observacao_w;
		elsif	(ie_opcao_p	= 'S') then
			ds_retorno_w	:= cd_senha_w;
		end if;
		
	end if;
	if	(nr_sequencia_autor_w is not null) then

		select	max(b.nr_sequencia),
			substr(max(b.ds_estagio),1,255),
			max(a.cd_senha),
			max(a.ds_observacao)
		into	nr_seq_estagio_w,
			ds_estagio_w,
			cd_senha_w,
			ds_observacao_w
		from	estagio_autorizacao b,
			autorizacao_convenio a
		where	a.nr_seq_estagio	= b.nr_sequencia
		and	a.nr_sequencia		= nr_sequencia_autor_w;

		if	(ie_opcao_p	= 'C') then
			ds_retorno_w	:= nr_seq_estagio_w;
		elsif	(ie_opcao_p	= 'D') then
			ds_retorno_w	:= substr(ds_estagio_w,1,255);
		elsif	(ie_opcao_p	= 'OBS') then
			ds_retorno_w	:= ds_observacao_w;
		elsif	(ie_opcao_p	= 'S') then
			ds_retorno_w	:= cd_senha_w;
		end if;
	end if;

end if;

return ds_retorno_w;

end;
/