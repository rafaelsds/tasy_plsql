CREATE OR REPLACE
FUNCTION Obter_Tipo_Material	(cd_material_p  Number,
                           		ie_opcao_p      Varchar2)
                           		RETURN Varchar IS
/*
C - c�digo;
D - Descri��o;
*/
ie_tipo_material_w         Varchar2(3);
ds_retorno_w               Varchar2(255);
BEGIN
if (cd_material_p is not null) then
   select  ie_tipo_material
   into    ie_tipo_material_w
   from    material
   where   cd_material = cd_material_p;
   ds_retorno_w    := ie_tipo_material_w;
   if      (nvl(ie_opcao_p,'C') = 'D') then
           select  obter_valor_dominio(29, ie_tipo_material_w)
           into    ds_retorno_w
           from    dual;
   end if;
end if;
return     ds_retorno_w;
END        Obter_Tipo_Material;
/