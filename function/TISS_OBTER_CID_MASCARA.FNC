create or replace
function TISS_OBTER_CID_MASCARA	(cd_cid_p	varchar2) return varchar2 is

ds_retorno_w	varchar2(10);

begin

if	(cd_cid_p is not null) and
	(length(cd_cid_p) = 4) then
	ds_retorno_w	:= substr(cd_cid_p,1,3) || '.' || substr(cd_cid_p,4,1);
else
	ds_retorno_w	:= cd_cid_p;
end if;

return ds_retorno_w;

end TISS_OBTER_CID_MASCARA;
/
