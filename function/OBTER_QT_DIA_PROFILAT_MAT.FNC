create or replace
function obter_qt_dia_profilat_mat	(cd_material_p	number)
				return number is
				
qt_dia_profilat_w	number(3,0);
				
begin
if	(cd_material_p is not null) then

	select	max(obter_dados_medic_atb_num(cd_material,wheb_usuario_pck.get_cd_estabelecimento,qt_dia_profilatico,'DP',null,null,null))
	into		qt_dia_profilat_w
	from		material
	where	cd_material = cd_material_p;

end if;

return qt_dia_profilat_w;

end obter_qt_dia_profilat_mat;
/
