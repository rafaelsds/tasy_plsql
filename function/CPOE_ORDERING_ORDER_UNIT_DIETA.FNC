create or replace 
function cpoe_ordering_order_unit_dieta(
				nr_sequencia_p in number, 
				nr_seq_cpoe_order_unit_p in number, 
				ie_tipo_dieta_p in varchar2) 
				return varchar2 is

nm_attribute_w varchar2(100);

begin

if (ie_tipo_dieta_p = 'O') then
	nm_attribute_w	:= 'cd_dieta';
elsif (ie_tipo_dieta_p in ('S', 'E')) then
   	nm_attribute_w	:= 'cd_material';
elsif (ie_tipo_dieta_p = 'L') then
   	nm_attribute_w	:= 'cd_mat_prod1_w';
elsif (ie_tipo_dieta_p = 'J') then
	nm_attribute_w	:= 'nr_seq_tipo';
elsif (ie_tipo_dieta_p = 'P')then
	nm_attribute_w	:= 'cd_doenca_cid';
elsif (ie_tipo_dieta_p = 'I') then 
	nm_attribute_w	:= 'ie_bomba_infusao';
end if;

return cpoe_ordering_order_unit('cpoe_dieta', nm_attribute_w, nr_sequencia_p, nr_seq_cpoe_order_unit_p);

end cpoe_ordering_order_unit_dieta;
/
