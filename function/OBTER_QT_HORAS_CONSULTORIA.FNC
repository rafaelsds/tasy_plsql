create or replace
function obter_qt_horas_consultoria(	dt_mes_referencia_p	date)
				return number is

qt_registros_w		number(10);
qt_horas_w		number(15,2);

begin

select	count(*)
into	qt_registros_w
from	proj_rat
where	trunc(dt_aprov_fin,'month') = trunc(dt_mes_referencia_p,'month');

if (qt_registros_w > 0) then

	select	sum(nvl(qt_total_hora,0)) qt_horas
	into	qt_horas_w
	from	proj_rat
	where	trunc(dt_aprov_fin,'month') = trunc(dt_mes_referencia_p,'month');

else
	qt_horas_w := 0;
end if;

return	qt_horas_w;

end obter_qt_horas_consultoria;
/