create or replace
function Ageint_Obter_Conv_Item(nr_seq_agenda_p		number,
				nr_seq_agenda_item_p	number)
 		    	return varchar2 is

ds_convenio_w	varchar2(255);
			
begin

select	b.ds_convenio
into	ds_convenio_w
from	convenio b,
	agenda_integrada a
where	a.cd_convenio	= b.cd_convenio
and	a.nr_sequencia	= nr_seq_agenda_p;

select	nvl(max(b.ds_convenio), ds_convenio_w)
into	ds_convenio_w
from	convenio b,
	agenda_integrada_conv_item a
where	a.cd_convenio		= b.cd_convenio
and	a.nr_seq_agenda_item	= nr_seq_agenda_item_p;

return	ds_convenio_w;

end Ageint_Obter_Conv_Item;
/