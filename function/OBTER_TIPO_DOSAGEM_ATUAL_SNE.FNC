create or replace
function obter_tipo_dosagem_atual_sne ( nr_prescricao_p	number,
			nr_sequencia_p	number)
 		    	return varchar2 is

ie_unidade_infusao_w	varchar2(10);
nr_seq_evento_w		number(10);

begin

Select	max(nr_sequencia)
into	nr_seq_evento_w
from	prescr_solucao_evento
where	nr_prescricao	= nr_prescricao_p
and 	nr_seq_material	= nr_sequencia_p
and	nvl(ie_evento_valido,'S') = 'S'
and	ie_tipo_dosagem	is not null;

if	(nr_seq_evento_w is not null) then	
	Select	max(ie_tipo_dosagem)
	into	ie_unidade_infusao_w
	from	prescr_solucao_evento
	where	nr_sequencia = nr_seq_evento_w;
end if;


return	ie_unidade_infusao_w;

end obter_tipo_dosagem_atual_sne;
/
