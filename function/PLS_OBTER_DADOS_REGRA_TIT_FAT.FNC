create or replace
function pls_obter_dados_regra_tit_fat(nr_seq_regra_p	pls_regra_tit_fat_geral.nr_sequencia%type) return varchar2 is

ds_retorno_w		pls_regra_tit_fat_geral.nm_regra%type;

begin

if	(nr_seq_regra_p is not null) then
	select	nvl(max(nm_regra),'')
	into	ds_retorno_w
	from	pls_regra_tit_fat_geral
	where	nr_sequencia = nr_seq_regra_p;
end if;

return	ds_retorno_w;

end pls_obter_dados_regra_tit_fat;
/