CREATE OR REPLACE
FUNCTION Obter_Origem_Titulo_Receber	(nr_titulo_p	number)
						Return Varchar2 IS

/*
C - Conta;
O - Outros;
*/

nr_seq_protocolo_w	number(10,0);
nr_interno_conta_w	number(10,0);
cd_convenio_conta_w	number(5,0);
ds_retorno_w		varchar2(1)	:= 'C';

BEGIN

if	(nr_titulo_p is not null) then
	select	nvl(nr_seq_protocolo,0),
		nvl(nr_interno_conta,0),
		nvl(cd_convenio_conta,0)
	into	nr_seq_protocolo_w,
		nr_interno_conta_w,
		cd_convenio_conta_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;

	if	(nr_seq_protocolo_w 	= 0) and
		(nr_interno_conta_w	= 0) and
		(cd_convenio_conta_w	= 0) then
		ds_retorno_w	:= 'O';
	end if;
end if;

RETURN	ds_retorno_w;

END	Obter_Origem_Titulo_Receber;
/