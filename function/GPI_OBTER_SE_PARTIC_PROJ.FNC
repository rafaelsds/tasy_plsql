create or replace
function gpi_obter_se_partic_proj(	nr_sequencia_p	number,
					cd_pessoa_fisica_p varchar2) 
 		    	return varchar2 is

cd_pessoa_fisica_w	varchar2(20);
ie_retorno_w		varchar2(1)	:= 'N';
qt_registro_w		number(10);
begin

cd_pessoa_fisica_w	:= nvl(cd_pessoa_fisica_p,'0');

if	(nvl(nr_sequencia_p,0) <> 0) and
	(cd_pessoa_fisica_w <> '0') then
	begin
	
	select	count(*)
	into	qt_registro_w
	from	gpi_cron_etapa_equipe d,
		gpi_cron_etapa c,
		gpi_cronograma b,
		gpi_projeto a
	where	a.nr_sequencia		= b.nr_seq_projeto
	and	b.nr_sequencia		= c.nr_seq_cronograma
	and	c.nr_sequencia		= d.nr_seq_etapa
	and	d.cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	a.nr_sequencia		= nr_sequencia_p;
	
	if	(qt_registro_w > 0) then
		ie_retorno_w	:= 'S';
	else
		begin
		select	count(*)
		into	qt_registro_w
		from	gpi_proj_equipe_membro f,
			gpi_proj_Equipe e
		where	e.nr_sequencia	= f.nr_seq_equipe
		and	e.ie_situacao	= 'A'
		and	e.nr_seq_proj_gpi	= nr_sequencia_p
		and	f.cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		if	(qt_registro_w > 0) then
			ie_retorno_w	:= 'S';
		end if;
		end;
	end if;
	
	end;
end if;

return	ie_retorno_w;

end gpi_obter_se_partic_proj;
/