create or replace
function san_result_exame_procergs(
				nr_seq_doacao_p 	number,
				ie_tipo_exame_p		varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(3) := 'NAR';
nr_seq_exame_lote_w	san_exame_lote.nr_sequencia%type;
ds_resultado_w		san_exame_realizado.ds_resultado%type;
begin

if	(nr_seq_doacao_p is not null) and
	(ie_tipo_exame_p is not null) then
	
	select	max(a.nr_sequencia)
	into	nr_seq_exame_lote_w
	from	san_exame_lote a
	where	a.nr_seq_doacao = nr_seq_doacao_p;
	
	if	(nr_seq_exame_lote_w is not null) then
	
		select	max(a.ds_resultado)
		into	ds_resultado_w
		from	san_exame_realizado a,
			san_exame b
		where	a.nr_seq_exame_lote	= nr_seq_exame_lote_w
		and	a.nr_seq_exame 		= b.nr_sequencia
		and	b.ie_tipo_exame		= ie_tipo_exame_p
		and	a.dt_liberacao is not null;
	
		if	(ds_resultado_w is not null) then
			
			select	decode(a.ds_resultado, null, 'NAR', 
				decode(b.cd_tipo_resultado, 1356, decode(upper(a.ds_resultado), 'POSITIVO', 'POS', 'NEGATIVO', 'NEG', decode(ie_tipo_exame_p, '2', 'INC', 'NAR')) ,
				1357, decode(upper(a.ds_resultado), 'REAGENTE', 'POS', 'N�O REAGENTE', 'NEG', decode(ie_tipo_exame_p, '2', 'INC', 'NAR')), 
				san_obter_de_para_integracao('RES', c.nr_sequencia)))
			into	ds_retorno_w
			from	san_exame_realizado a,
				san_exame b,
				san_exame_valor c
			where	a.nr_seq_exame_lote	= nr_seq_exame_lote_w
			and	a.nr_seq_exame 		= b.nr_sequencia
			and	b.ie_tipo_exame		= ie_tipo_exame_p
			and	a.nr_seq_exame 		= c.nr_seq_exame(+)
			and	a.ds_resultado 		= c.ds_valor(+)
			and	a.ds_resultado		= ds_resultado_w
			and	a.dt_liberacao is not null
			and	rownum = 1;
		
		end if;
		
	end if;
	
end if;

return	ds_retorno_w;

end san_result_exame_procergs;
/