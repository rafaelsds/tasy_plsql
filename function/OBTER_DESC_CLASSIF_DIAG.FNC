create or replace
function obter_desc_classif_diag(	
				nr_atendimento_p		number,
				nr_seq_episodio_p		number,
				nr_seq_interno_p		number)	
				return varchar2	is

ds_classif_concatenado_w	varchar2(10000) := '';
ds_classificacao_w			classificacao_diagnostico.ds_classificacao%type;

cursor c01 is
	select	substr(c.ds_curta,1,255) ds_classificacao
	from	classificacao_diagnostico c,
			medic_diagnostico_doenca d,
			diagnostico_doenca e
	where	c.nr_sequencia	= d.nr_seq_classificacao 
	and		d.nr_atendimento in (select x.nr_atendimento from atendimento_paciente x, episodio_paciente y where x.nr_seq_episodio = y.nr_sequencia and y.nr_sequencia = nr_seq_episodio_p)
	and		e.nr_seq_interno = nr_seq_interno_p
	and		e.nr_atendimento = d.nr_atendimento
	and		e.cd_doenca = d.cd_doenca
	and		e.dt_diagnostico = d.dt_diagnostico
	and		e.dt_liberacao is not null
	and		e.dt_inativacao is null
	and		d.ie_situacao = 'A'
	and		c.ie_situacao = 'A';
	
cursor c02 is
	select	substr(c.ds_curta,1,255) ds_classificacao
	from	classificacao_diagnostico c,
			medic_diagnostico_doenca d,
			diagnostico_doenca e
	where	c.nr_sequencia	= d.nr_seq_classificacao 
	and		d.nr_atendimento = nr_atendimento_p
	and		e.nr_seq_interno = nr_seq_interno_p
	and		e.nr_atendimento = d.nr_atendimento
	and		e.cd_doenca = d.cd_doenca
	and		e.dt_diagnostico = d.dt_diagnostico
	and		e.dt_liberacao is not null
	and		e.dt_inativacao is null
	and		d.ie_situacao = 'A'
	and		c.ie_situacao = 'A';

begin

if (nr_seq_episodio_p is not null) then
	open c01;
	loop
	fetch c01 into 
		ds_classificacao_w;
	exit when c01%notfound;
		begin
		ds_classif_concatenado_w := ds_classif_concatenado_w || ds_classificacao_w || ' - ';
		end;
	end loop;
	close c01;
elsif (nr_atendimento_p is not null) then
	open c02;
	loop
	fetch c02 into 
		ds_classificacao_w;
	exit when c02%notfound;
		begin
		ds_classif_concatenado_w := ds_classif_concatenado_w || ' - ' || ds_classificacao_w || ' - ';
		end;
	end loop;
	close c02;
else
	return ds_classif_concatenado_w;
end if;

return	ds_classif_concatenado_w;

end obter_desc_classif_diag;
/
