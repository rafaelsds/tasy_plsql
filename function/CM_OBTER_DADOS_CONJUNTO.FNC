create or replace
function cm_obter_dados_conjunto(	nr_seq_conjunto_p	number,
				ie_opcao_p	varchar2)
 		    	return varchar2 is

ds_retorno_w			varchar2(255);
nm_conjunto_w			varchar2(200);
ie_situacao_w			varchar2(01);
qt_tempo_esterelizacao_w	number(10);
cd_estabelecimento_w		number(4);
ds_localizacao_conjunto_w	varchar2(200);
nr_seq_conj_cont_w        cm_conjunto_cont.nr_seq_conjunto%type;

/*
ie_opcao_p
N - Nome do conjunto
S - Situacao do conjunto
T - Tempo esterilizacao (min)
E - Estabelecimento
L - Descricao da Localizacao do conjunto
*/

begin

select	max(nm_conjunto),
	max(ie_situacao),
	max(qt_tempo_esterelizacao),
	max(cd_estabelecimento),
	max(cm_obter_desc_local_conj(nr_seq_local_conj))
into	nm_conjunto_w,
	ie_situacao_w,
	qt_tempo_esterelizacao_w,
	cd_estabelecimento_w, 
	ds_localizacao_conjunto_w
from	cm_conjunto
where	nr_sequencia = nr_seq_conjunto_p;

if (nm_conjunto_w is null and ie_opcao_p = 'N') then
    select nvl(max(nr_seq_conjunto),0)
    into    nr_seq_conj_cont_w
    from cm_conjunto_cont
    where	nr_sequencia = nr_seq_conjunto_p;
    
    if (nr_seq_conj_cont_w > 0) then
        select	max(nm_conjunto)
        into nm_conjunto_w
        from cm_conjunto
        where	nr_sequencia = nr_seq_conj_cont_w;
    end if;
end if;

if	(ie_opcao_p = 'N') then
	ds_retorno_w := nm_conjunto_w;
elsif	(ie_opcao_p = 'S') then
	ds_retorno_w := ie_situacao_w;
elsif	(ie_opcao_p = 'T') then
	ds_retorno_w := qt_tempo_esterelizacao_w;
elsif	(ie_opcao_p = 'E') then	
	ds_retorno_w := cd_estabelecimento_w;	
elsif	(ie_opcao_p = 'L') then	
	ds_retorno_w := ds_localizacao_conjunto_w;	
end if;

return	ds_retorno_w;

end cm_obter_dados_conjunto;
/
