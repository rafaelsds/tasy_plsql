create or replace
function man_obter_se_cert_calib_aprov(nr_sequencia_p	number)
 		    	return Varchar2 is

ds_retorno_w		varchar2(01) := 'N';
qt_erro_total_w		number(15,4);
qt_media_leitura_w		number(15,4);
vl_saida_w		number(15,4);
qt_incerteza_medicao_w	number(15,4);
qt_variavel_w		number(15,4) := 0;
qt_erro_max_permitido_w	number(15,4);
	
begin

select	obter_qt_erro_total_calibracao(l.nr_sequencia),
	l.qt_media_leitura,
	l.vl_saida,
	l.qt_incerteza_medicao,
	e.qt_erro_max_permitido
into	qt_erro_total_w,
	qt_media_leitura_w,
	vl_saida_w,
	qt_incerteza_medicao_w,
	qt_erro_max_permitido_w
from	man_equip_calib_escala e,
	man_calibracao l,
	man_calib_certificado c
where	c.nr_sequencia	= nr_sequencia_p
and	l.nr_sequencia	= c.nr_seq_calibracao
and	e.nr_sequencia	= l.nr_seq_equip_calib_escala;

/*Se n�o tiver informado o erro total calcula com a formula abaixo*/
if	(nvl(qt_erro_total_w,0) = 0) then
	qt_erro_total_w	:= nvl(vl_saida_w,0) - nvl(qt_media_leitura_w,0);
end if;

/*Variavel a ser considerada sobre o erro max permitido (para mais ou para menos, por isso o abs no if abaixo) */
qt_variavel_w	:= (nvl(qt_erro_total_w,0) - nvl(qt_incerteza_medicao_w,0));

/*verifica se a variavel � menor que o erro m�ximo*/
if	(abs(qt_variavel_w) < qt_erro_max_permitido_w) and
	((abs(qt_variavel_w) * -1) < qt_erro_max_permitido_w) then
	ds_retorno_w	:= 'S';	
end if;

return	ds_retorno_w;

end man_obter_se_cert_calib_aprov;
/
