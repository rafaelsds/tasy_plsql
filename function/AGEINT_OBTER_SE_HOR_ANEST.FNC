create or replace
function Ageint_Obter_Se_Hor_Anest(
			cd_agenda_p		number,
			dt_agenda_p		date,
			nr_seq_proc_interno_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1)	:= 'S';
nr_seq_anest_turno_w	number(10);
qt_perm_anest_w		number(10)	:= 0;
qt_regra_w		number(10);
dt_dia_semana_w		number(10);

Cursor C01 is
	select	nr_seq_anest_turno
	from	ageint_agenda_turno_anest
	where	cd_agenda	= cd_agenda_p
	and	qt_perm_anest_w	= 0
	order by 1;
			
begin
select	count(*)
into	qt_regra_w
from	ageint_agenda_turno_anest
where	cd_agenda	= cd_agenda_p;

if	(qt_regra_w	> 0) then
	dt_dia_semana_w	:= obter_cod_dia_semana(dt_agenda_p);
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_anest_turno_w;
	exit when C01%notfound;
		begin
		if	(qt_perm_anest_w	= 0) then
			select	count(*)
			into	qt_perm_anest_w
			from	ageint_anestesia_turno
			where	nr_sequencia		= nr_seq_anest_turno_w
			and 	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
			and	((trunc(dt_agenda_p) >= trunc(dt_inicial_vigencia)) or (dt_inicial_vigencia is null))
			and	((trunc(dt_agenda_p) <= trunc(dt_final_vigencia)) or (dt_final_vigencia is null))
			and	((dt_dia_semana	= dt_dia_semana_w) or (dt_dia_semana = 9 and dt_dia_semana_w not in (1,7)))
			and	dt_agenda_p between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_inicial,'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
					and to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_final,'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end loop;
	close C01;

	if	(qt_perm_anest_w	> 0) then
		ds_retorno_w	:= 'S';
	else
		ds_retorno_w	:= 'N';
	end if;	
end if;

return	ds_retorno_w;

end Ageint_Obter_Se_Hor_Anest;
/
