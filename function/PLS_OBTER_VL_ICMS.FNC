create or replace
function pls_obter_vl_icms	(
				nr_seq_operadora_p	pls_protocolo_conta.nr_sequencia%type,
				dt_vigencia_p		date,
				nm_usuario_p		varchar2,
				nr_seq_material_p	pls_material_unimed.nr_sequencia%type)
 		    	return number is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter valor de ICMS apartir do UF da operadora congenere
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ds_retorno_w	pls_regra_icms.vl_perc_icms%type;
uf_operadora_w	pessoa_juridica.sg_estado%type;
ie_generico_w	pls_material_unimed.ie_generico%type := 'A';

begin
if	(nr_seq_operadora_p is not null) then
	select	a.sg_estado
	into	uf_operadora_w
	from	pessoa_juridica	a,
		pls_congenere	b
	where	b.cd_cgc	= a.cd_cgc
	and	b.nr_sequencia	= nr_seq_operadora_p;

		if	(uf_operadora_w is not null) then
			
			if	(nr_seq_material_p is not null) then
				select	max(b.ie_generico)
				into	ie_generico_w
				from	pls_material_a900	a,
					pls_material_unimed	b
				where	b.cd_material		= a.cd_material_a900
				and 	b.nr_sequencia =  (	select max(nr_sequencia) from 	(
												select	b.nr_sequencia
												from	pls_material_a900	a,
													pls_material_unimed	b
												where	a.nr_seq_material	= nr_seq_material_p
												and	b.cd_material		= a.cd_material_a900
												and 	a.dt_fim_vigencia is null
												and	a.dt_inicio_vigencia < dt_vigencia_p
												union all
												select	b.nr_sequencia
												from	pls_material_a900	a,
													pls_material_unimed	b
												where	a.nr_seq_material	= nr_seq_material_p
												and	b.cd_material		= a.cd_material_a900
												and 	a.dt_fim_vigencia > dt_vigencia_p
												and 	a.dt_inicio_vigencia < dt_vigencia_p));
							
				if	(ie_generico_w is null) then
					
					select	max(b.ie_generico)
					into	ie_generico_w
					from	pls_material_a900	a,
						pls_material_unimed	b
					where	a.nr_seq_material	= nr_seq_material_p
					and	b.cd_material		= a.cd_material_a900;
					
					if (ie_generico_w is null) then
					
						select nvl(max(a.ie_generico),'N')
						into ie_generico_w
						from	pls_material_unimed a,
								pls_material b
						where b.nr_seq_material_unimed = a.nr_sequencia
						and		b.nr_sequencia = nr_seq_material_p;
					
					end if;
					
				end if;
			end if;	
			
			select	nvl(max(a.vl_perc_icms),0)
			into	ds_retorno_w
			from	pls_regra_icms		a,
				pls_regra_icms_uf	b
			where	b.nr_seq_regra_icms	= a.nr_sequencia
			and	b.sg_estado		= uf_operadora_w
			and	((b.ie_generico		= ie_generico_w) or (b.ie_generico = 'A'))
			and	dt_vigencia_p between	a.dt_inicio_vigencia and a.dt_fim_vigencia_ref;
		end if;

	return	ds_retorno_w;
end if;

return	ds_retorno_w;

end pls_obter_vl_icms;
/
