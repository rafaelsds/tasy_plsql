create or replace
function obter_se_interna_prescr (nr_atendimento_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(10) := '';
nr_prescricao_w		number(14);
qt_proc_nao_interna_w	number(10);

Cursor C01 is
	select	nr_prescricao
	from	prescr_medica a		
	where	a.nr_atendimento = nr_atendimento_p
	and (	select 	count(*)
		from	prescr_procedimento b
		where	b.nr_prescricao = a.nr_prescricao) > 1;

begin

open C01;
loop
fetch C01 into	
	nr_prescricao_w;
exit when C01%notfound;
	begin
	
	select 	count(*)
	into	qt_proc_nao_interna_w
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_w
	and	obter_se_gera_proc_inter_bpa(cd_procedimento, ie_origem_proced) = 'N';
	
	if	(nvl(qt_proc_nao_interna_w,0) > 0) then
		ds_retorno_w := ds_retorno_w||nr_prescricao_w||',';
	end if;
	
	end;
end loop;
close C01;

return	nvl(substr(ds_retorno_w,1,length(ds_retorno_w)-1),'');

end obter_se_interna_prescr;
/