create or replace
function pls_obter_id_maquina
		(	nr_seq_maquina_p		Number,
			nr_seq_local_atend_p		Number,
			ie_restringe_local_atend_p	Varchar2)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(255);

begin

if	(ie_restringe_local_atend_p	= 'S') then
	select	max(id_maquina)
	into	ds_retorno_w
	from	local_atend_med_maquina
	where	nr_sequencia		= nr_seq_maquina_p
	and	nr_seq_local_atend	= nr_seq_local_atend_p;
elsif	(ie_restringe_local_atend_p	= 'N') then
	select	max(id_maquina)
	into	ds_retorno_w
	from	local_atend_med_maquina
	where	nr_sequencia	= nr_seq_maquina_p;
end if;

return	ds_retorno_w;

end pls_obter_id_maquina;
/