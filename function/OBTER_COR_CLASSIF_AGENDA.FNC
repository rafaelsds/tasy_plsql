create or replace
function Obter_Cor_Classif_Agenda
		(cd_medico_p			number,
		ie_classif_agenda_p		varchar2,
		ie_tipo_cor_p			varchar2)
		return varchar2 is

ds_cor_w		varchar2(100);
ds_cor_fonte_w		varchar2(10);
ds_cor_fundo_w		varchar2(10);

/* F - Fonte
   U - Fundo */

begin

select	max(ds_cor_fonte),
	max(ds_cor_fundo)
into	ds_cor_fonte_w,
	ds_cor_fundo_w
from	med_classif_agenda
where	cd_medico		= cd_medico_p
and	ie_classif_agenda	= ie_classif_agenda_p;

if	(ie_tipo_cor_p = 'F') then
	ds_cor_w	:= ds_cor_fonte_w;
else
	ds_cor_w	:= ds_cor_fundo_w;
end if;

return ds_cor_w;

end Obter_Cor_Classif_Agenda;
/