create or replace
function obter_se_mat_exam_estab (cd_estabelecimento_p 	number,
								  nr_seq_exame_p		number,
								  nr_seq_material_p		number) return varchar2 is

ds_retorno_w varchar2(1) := 'S';
count_w		 number(10);
								  
begin
if (cd_estabelecimento_p  is not null) and 
   (nr_seq_exame_p is not null) and
   (nr_seq_material_p is not null) then

	select 	count(1)
	into 	count_w
	from	exame_lab_mat_estab
	where  	nr_seq_exame = nr_seq_exame_p
	and 	nr_seq_material  	= nr_seq_material_p	
	and 	rownum = 1;
	
	if (count_w > 0) then
		
		select 	nvl(max('S'),'N')
		into 	ds_retorno_w
		from	exame_lab_mat_estab
		where   nr_seq_material  	= nr_seq_material_p
		and	  	nr_seq_exame		= nr_seq_exame_p
		and 	cd_estabelecimento	= cd_estabelecimento_p;
	end if;
end if;

return ds_retorno_w;

end obter_se_mat_exam_estab;
/