create or replace
function pls_obter_dados_discussao(	nr_seq_lote_disc_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(255);
					
begin
if	(upper(ie_opcao_p) = 'LF') then
	select	max(nr_sequencia)
	into	ds_retorno_w
	from	pls_lote_faturamento
	where	nr_seq_lote_disc	= nr_seq_lote_disc_p;
	
elsif	(upper(ie_opcao_p) = 'LE') then
	select	max(a.nr_sequencia)
	into	ds_retorno_w
	from	pls_evento_movimento	b,
		pls_lote_evento		a
	where	a.nr_sequencia		= b.nr_seq_lote	
	and	b.nr_seq_lote_disc 	= nr_seq_lote_disc_p;
	
elsif	(upper(ie_opcao_p) = 'LA') then
	select	max(nr_sequencia)
	into	ds_retorno_w
	from	pls_ajuste_fatura
	where	nr_seq_lote_disc	= nr_seq_lote_disc_p;
	
end if;

return	ds_retorno_w;

end pls_obter_dados_discussao;
/