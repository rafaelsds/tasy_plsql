create or replace 
function sus_obter_status_laudo(nr_seq_status_p	number) 
				return varchar2 is

ds_status_w	sus_status_laudo.ds_status_laudo%type;

begin

if	(nr_seq_status_p is not null)  then
	begin
	select	ds_status_laudo
	into	ds_status_w
	from	sus_status_laudo
	where	nr_sequencia = nr_seq_status_p;
	exception
	when others then
		ds_status_w := null;
	end;	
end if;

return	ds_status_w;

end sus_obter_status_laudo;
/