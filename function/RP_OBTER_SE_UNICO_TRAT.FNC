create or replace
function rp_obter_se_unico_trat(nr_seq_pac_reab_p	number)
 		    	return varchar is

qt_tratamento_w	number(10,0);
ie_retorno_w	varchar2(1) := 'S';
			
begin

if	(nr_seq_pac_reab_p > 0) then

	select	count(*)
	into	qt_tratamento_w
	from	rp_tratamento
	where	nr_seq_pac_reav = nr_seq_pac_reab_p
	and	dt_fim_tratamento is null;
	
	if	(qt_tratamento_w > 1) then
		ie_retorno_w	:= 'N';
	end if;	
end if;

return	ie_retorno_w;

end rp_obter_se_unico_trat;
/