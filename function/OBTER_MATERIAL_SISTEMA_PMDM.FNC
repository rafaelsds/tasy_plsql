create or replace
function obter_material_sistema_pmdm( cd_sistema_ant_p  in  varchar2)
		return number is

cd_material_w		material.cd_material%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin
	cd_estabelecimento_w:= wheb_usuario_pck.get_cd_estabelecimento;
	if	(cd_sistema_ant_p is not null) then
		select	nvl(max(b.cd_material), 0)
		into	cd_material_w
		from	material_estab a,
			material b
		where	b.cd_material	= a.cd_material
		and	nvl(a.cd_sistema_ant, b.cd_sistema_ant)	= cd_sistema_ant_p
		and	a.cd_estabelecimento = cd_estabelecimento_w;
		if (cd_material_w = 0) then
			select nvl(max(cd_material), 0)
			into	cd_material_w
			from material b
			where b.cd_sistema_ant	= cd_sistema_ant_p;
		end if;
	end if;
	return	cd_material_w;
end obter_material_sistema_pmdm;
/
