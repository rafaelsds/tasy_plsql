create or replace 
function OBTER_DATA_REFERENCIA_PROC(nr_atendimento_p number)
			return date is 

date_w 		date default null;
ie_data_cid_w 	number default null;

begin 

begin
	select	a.ie_data_cid
	into 	ie_data_cid_w
	from 	parametro_atendimento a,
		atendimento_paciente b
	where 	a.cd_estabelecimento 	= b.cd_estabelecimento
	and	b.nr_atendimento	= nr_atendimento_p
	and	rownum			= 1;
exception
when others then
	ie_data_cid_w	:= null;
end;

if 	ie_data_cid_w is not null then
	if 	ie_data_cid_w = 1 then	
		begin
		select	dt_entrada
		into 	date_w
		from 	atendimento_paciente
		where 	nr_atendimento 	= nr_atendimento_p
		and	rownum		= 1;		
		exception
		when others then
			date_w	:= null;
		end;
	elsif 	ie_data_cid_w = 2 then    
		date_w	:= sysdate;    
	end if;
end if;

return date_w;

end OBTER_DATA_REFERENCIA_PROC;
/