CREATE OR REPLACE
FUNCTION Obter_Se_atend_sus		(	nr_atendimento_p		Number) 
										return Varchar2 is


ds_retorno_w			Varchar2(1)	:= 'N';
cd_convenio_sus_w		Number(5);
ie_nivel_atencao_w  	Varchar2(1)	:= 'T';

BEGIN

if	( nr_atendimento_p is not null) then

	Select  wheb_assist_pck.get_nivel_atencao_perfil
	into	ie_nivel_atencao_w
	from	dual;

	if ( ie_nivel_atencao_w in ('P','S')) then
	
		select	max(cd_convenio_sus)
		into	cd_convenio_sus_w
		from	parametro_faturamento
		where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento; 
		
		Select  nvl(max('S'),'N')
		into	ds_retorno_w
		from	atendimento_paciente a,
				atend_categoria_convenio b
		where	a.nr_atendimento =  nr_atendimento_p
		and		a.nr_atendimento =  b.nr_atendimento
		and		a.ie_tipo_atendimento	= 1
		and		a.ie_tipo_convenio	= 3
		and		b.cd_convenio = cd_convenio_sus_w;
		
	end if;
	
end if;	

return ds_retorno_w;

END Obter_Se_atend_sus;
/