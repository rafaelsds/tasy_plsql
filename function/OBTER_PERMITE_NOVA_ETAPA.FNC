create or replace
function obter_permite_nova_etapa(nr_interno_conta_p	number)
				return varchar2 is

ds_retorno_w	varchar2(255)	:= null;

begin

select	nvl(max('N'), 'S')
into	ds_retorno_w
from	fatur_etapa b,
	conta_paciente_etapa a
where	a.nr_seq_etapa	= b.nr_sequencia
and	a.nr_interno_conta = nr_interno_conta_p
and	a.dt_fim_etapa	is null
and	nvl(b.ie_situacao,'A') = 'A'
and	nvl(b.ie_nova_etapa_manual, 'S') = 'N';

return	ds_retorno_w;

end obter_permite_nova_etapa;
/
