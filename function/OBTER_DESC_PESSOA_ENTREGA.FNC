create or replace
function obter_desc_pessoa_entrega(
				nr_seq_pessoa_entrega_p 	varchar2)
				return varchar is

ds_retorno_w		varchar2(80);

begin
ds_retorno_w		:= '';
if (nr_seq_pessoa_entrega_p	is not null) then
	begin
	select	substr(max(ds_tipo_pessoa_entrega),1,80)
	into	ds_retorno_w
	from	cme_recebimento_terceiro
	where	nr_sequencia = nr_seq_pessoa_entrega_p;
	end;
end if;

return ds_retorno_w;
end obter_desc_pessoa_entrega;
/
