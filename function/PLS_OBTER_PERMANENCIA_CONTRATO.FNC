Create or replace
function pls_obter_permanencia_contrato(nr_contrato_p		number,
					nr_plano_p		number,
					ie_movimentacao_p	varchar2,
					dt_inicial_p		date,
					dt_final_p		date)
					return varchar2 is

qt_permanencia_0_3_w		number(10) := 0;
qt_permanencia_3_6_w		number(10) := 0;
qt_permanencia_6_9_w		number(10) := 0;
qt_permanencia_9_12_w		number(10) := 0;
qt_permanencia_1_2_w		number(10) := 0;
qt_permanencia_2_3_w		number(10) := 0;
qt_permanencia_3_4_w		number(10) := 0;
qt_permanencia_4_5_w		number(10) := 0;
qt_permanencia_5_10_w		number(10) := 0;
qt_permanencia_10_50_w		number(10) := 0;
qt_permanencia_50_100_w		number(10) := 0;
qt_meses_w			number(10);

Cursor C01 is
	select	pls_obter_meses_entre_datas(b.dt_contratacao, b.dt_rescisao)
	from	pessoa_fisica			g,	
		pls_contrato_pagador_fin	f,
		pls_contrato_pagador		e,	
		pls_contrato_plano		d,
		pls_segurado_carteira		c,	
		pls_segurado			b,
		pls_contrato			a
	where	a.nr_sequencia = b.nr_seq_contrato 	
	and	b.nr_sequencia = c.nr_seq_segurado
	and	a.nr_sequencia = d.nr_seq_contrato 	
	and	e.nr_sequencia = f.nr_seq_pagador(+)
	and	e.nr_sequencia = b.nr_seq_pagador	
	and	b.cd_pessoa_fisica = g.cd_pessoa_fisica
	and	a.nr_contrato = nr_contrato_p
	and	d.nr_sequencia = nr_plano_p
	and	b.dt_rescisao is not null
	and	((ie_movimentacao_p = 'I' and to_char(b.dt_contratacao, 'dd/mm/yyyy') between dt_inicial_p and dt_final_p) or
		(ie_movimentacao_p = 'E' and ((to_char(b.dt_cancelamento, 'dd/mm/yyyy') between dt_inicial_p and dt_final_p) or (to_char(b.dt_rescisao, 'dd/mm/yyyy') between dt_inicial_p and dt_final_p))) or
		(ie_movimentacao_p = 'A' and ((to_char(b.dt_contratacao, 'dd/mm/yyyy') between dt_inicial_p and dt_final_p) or (to_char(b.dt_cancelamento, 'dd/mm/yyyy') between dt_inicial_p and dt_final_p) or (to_char(b.dt_rescisao, 'dd/mm/yyyy') between dt_inicial_p and dt_final_p))));

begin

open C01;
loop
fetch C01 into	
	qt_meses_w;
exit when C01%notfound;
	begin
	
	if (qt_meses_w between 0 and 3) then
		qt_permanencia_0_3_w := qt_permanencia_0_3_w + 1;
	end if;
	
	if (qt_meses_w between 4 and 6) then
		qt_permanencia_3_6_w := qt_permanencia_3_6_w + 1;
	end if;
	
	if (qt_meses_w between 7 and 9) then
		qt_permanencia_6_9_w := qt_permanencia_6_9_w + 1;
	end if;
	
	if (qt_meses_w between 10 and 12) then
		qt_permanencia_9_12_w := qt_permanencia_9_12_w + 1;
	end if;
	
	if (qt_meses_w between 13 and 24) then
		qt_permanencia_1_2_w := qt_permanencia_1_2_w + 1;
	end if;
	
	if (qt_meses_w between 25 and 36) then
		qt_permanencia_2_3_w := qt_permanencia_2_3_w + 1;
	end if;
	
	if (qt_meses_w between 37 and 48) then
		qt_permanencia_3_4_w := qt_permanencia_3_4_w + 1;
	end if;
	
	if (qt_meses_w between 49 and 60) then
		qt_permanencia_4_5_w := qt_permanencia_4_5_w + 1;
	end if;
	
	if (qt_meses_w between 61 and 120) then
		qt_permanencia_5_10_w := qt_permanencia_5_10_w + 1;
	end if;
	
	if (qt_meses_w between 121 and 600) then
		qt_permanencia_10_50_w := qt_permanencia_10_50_w + 1;
	end if;
	
	if (qt_meses_w between 601 and 1200) then
		qt_permanencia_50_100_w := qt_permanencia_50_100_w + 1;
	end if;
	
	end;	
end loop;
close C01;

return 	qt_permanencia_0_3_w||'
'||	qt_permanencia_3_6_w||'
'||	qt_permanencia_6_9_w||'
'||	qt_permanencia_9_12_w||'
'||	qt_permanencia_1_2_w||'
'||	qt_permanencia_2_3_w||'
'||	qt_permanencia_3_4_w||'
'||	qt_permanencia_4_5_w||'
'||	qt_permanencia_5_10_w||'
'||	qt_permanencia_10_50_w||'
'||	qt_permanencia_50_100_w;

end pls_obter_permanencia_contrato;
/