create or replace
function cpoe_obter_prime_dialise_rel(	ie_ligar_prime_p		varchar2,
										qt_volume_prime_p		number,
										qt_sodio_p				number,
										qt_bicarbonato_p		number)
							return varchar2 is

ds_retorno_w		varchar2(4000);

	function ligar_prime return varchar2 is
	begin 
		return '                ' || obter_desc_expressao(292572, null) || ': '|| obter_valor_dominio(6,ie_ligar_prime_p) ||' ';
	end;

	function volume_prime return varchar2 is
	begin 
		return ' ' || obter_desc_expressao(302274, null) || ': '|| qt_volume_prime_p;
	end;
	
begin
--Inserido espa�os pois futuramente relat�rio wate que usa isto se tornar� um cate onde ser� melhorado isso.

	if (nvl(qt_sodio_p,0) > 0) then
		ds_retorno_w := ds_retorno_w || substr(obter_desc_expressao(298623, null) || ': '|| trim(to_char(nvl(qt_sodio_p,0),'99999G999G990D09')),1,255) || '	           ';
	end if; 
	
	if (nvl(qt_bicarbonato_p,0) > 0) then
		ds_retorno_w := ds_retorno_w || substr(obter_desc_expressao(284301, null) || ': '|| trim(to_char(nvl(qt_bicarbonato_p,0),'99999G999G990D09')),1,255) || '';
	end if; 

	ds_retorno_w := ds_retorno_w || ligar_prime();

	if (ie_ligar_prime_p = 'S') then
		ds_retorno_w := ds_retorno_w || '	          ' ||  volume_prime();
	end if;

	return	ds_retorno_w;

end cpoe_obter_prime_dialise_rel;
/
