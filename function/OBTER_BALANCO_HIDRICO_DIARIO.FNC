create or replace
function Obter_balanco_hidrico_diario(nr_atendimento_p	number)
				Return number IS

qt_acumulado_w			number(15,4);
cd_estabelecimento_w		number(4,0);
ie_liberar_ganho_perda_w	varchar2(1);
dt_inicio_w					date;
dt_fim_w					date;

begin

dt_inicio_w	:= trunc(sysdate);
dt_fim_w	:= fim_dia(dt_inicio_w);

select	cd_estabelecimento
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

select	nvl(max(ie_liberar_ganho_perda),'N')
into	ie_liberar_ganho_perda_w
from	parametro_medico
where	cd_estabelecimento	= cd_estabelecimento_w;
	    
if	(ie_liberar_ganho_perda_w	= 'N') then
	
	select	nvl(sum(decode(c.ie_perda_ganho,'G',a.qt_volume,0)) - sum(decode(c.ie_perda_ganho,'P',a.qt_volume,0)),0) qt_saldo
	into	qt_acumulado_w
	from	grupo_perda_ganho c,
			tipo_perda_ganho b,
			atendimento_perda_ganho a
	where	a.nr_seq_tipo	= b.nr_sequencia
	and		b.nr_seq_grupo	= c.nr_sequencia
	and		nvl(b.ie_soma_bh,'S')	= 'S'
	and		a.nr_atendimento = nr_atendimento_p
	and     nvl(a.ie_situacao,'A') = 'A'
	and		a.DT_MEDIDA between dt_inicio_w and  dt_fim_w;
	
else
	select	nvl(sum(decode(c.ie_perda_ganho,'G',a.qt_volume,0)) - sum(decode(c.ie_perda_ganho,'P',a.qt_volume,0)),0) qt_saldo
	into	qt_acumulado_w
	from	grupo_perda_ganho c,
			tipo_perda_ganho b,
			atendimento_perda_ganho a
	where	a.nr_seq_tipo	= b.nr_sequencia
	and		b.nr_seq_grupo	= c.nr_sequencia
	and		nvl(b.ie_soma_bh,'S')	= 'S'
	and		a.nr_atendimento = nr_atendimento_p
	and		nvl(a.dt_referencia, a.DT_MEDIDA) between dt_inicio_w and  dt_fim_w
	and		a.dt_liberacao is not null
	and     nvl(a.ie_situacao,'A') = 'A';
end if;		



Return qt_acumulado_w;

end Obter_balanco_hidrico_diario;
/
