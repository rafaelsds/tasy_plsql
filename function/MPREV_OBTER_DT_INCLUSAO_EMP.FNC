create or replace
function mprev_obter_dt_inclusao_emp(nr_seq_busca_emp_p		Number)
					return date is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar qual o dia que a pessoa indicada pela Busca Empresarial foi inclusa no programa.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dt_inclusao_w		date	:= null;
nr_seq_contrato_w	pls_contrato.nr_sequencia%type;
nr_seq_participante_w	mprev_participante.nr_sequencia%type;
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;

begin

begin
select	z.nr_seq_contrato,
	z.cd_pessoa_fisica
into	nr_seq_contrato_w,
	cd_pessoa_fisica_w
from	mprev_busca_empresarial z
where	z.nr_sequencia = nr_seq_busca_emp_p;
exception
	when others then
	nr_seq_contrato_w	:= null;
	cd_pessoa_fisica_w	:= null;
end;

begin
select	x.nr_sequencia
into	nr_seq_participante_w
from	mprev_participante x
where	x.cd_pessoa_fisica = cd_pessoa_fisica_w;
exception
	when others then
	nr_seq_participante_w	:= null;
end;

begin
select	max(y.dt_inclusao)
into	dt_inclusao_w
from	mprev_programa_partic y	
where	y.nr_seq_contrato_emp	= nr_seq_contrato_w
and	y.nr_seq_participante	= nr_seq_participante_w;
exception
	when others then
	dt_inclusao_w	:= null;
end;

return	dt_inclusao_w;

end mprev_obter_dt_inclusao_emp;
/