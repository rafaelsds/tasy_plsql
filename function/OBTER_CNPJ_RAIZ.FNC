create or replace
function OBTER_CNPJ_RAIZ 
		(cd_cgc_p	varchar2) return varchar2 is

cd_cnpj_raiz_w		varchar2(50);

begin

select	max(cd_cnpj_raiz)
into	cd_cnpj_raiz_w
from	pessoa_juridica
where	cd_cgc			= cd_cgc_p;

return	cd_cnpj_raiz_w;

end OBTER_CNPJ_RAIZ;
/
