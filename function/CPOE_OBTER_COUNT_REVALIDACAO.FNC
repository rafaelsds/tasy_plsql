create or replace 
function cpoe_obter_count_revalidacao(nr_atendimento_p	number,
									  nr_seq_revalidation_rule_p    cpoe_revalidation_rule.nr_sequencia%type default null,
									  nm_usuario_p varchar2 default null) 
                                     return number is

qt_retorno_w		number(10) := 0;
qt_hors_copia_w		number(10) := 0;

begin

qt_hors_copia_w 	:= get_qt_hours_after_copy_cpoe(obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo);

if (nr_atendimento_p is not null) then
	select	sum(qt_itens) 
	into	qt_retorno_w
	from (
		select     count(1) qt_itens 
		from	cpoe_dialise   
		where nr_atendimento = nr_atendimento_p  
		and dt_liberacao is not null 
		and	cd_funcao_origem = 2314 
		and cpoe_obter_data_prox_reval(nr_sequencia, 'D') < sysdate 
        and cpoe_obter_se_prescritor_reval(nr_seq_revalidation_rule_p, ie_motivo_prescricao, cd_perfil_ativo, nm_usuario_nrec) = 'S'
		and nvl(decode(dt_lib_suspensao, null, dt_fim, Obter_Data_Menor(dt_suspensao,dt_fim)), sysdate + 1) > (sysdate + (1/24))
		and dt_prox_geracao is not null
		and	((cpoe_obter_se_copia_item_term(ds_horarios,
											null,
											dt_prox_geracao + (qt_hors_copia_w/24),
											decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao, dt_fim))) = 'S')
			or ((dt_fim is null) and ie_duracao = 'C'))			
		union all
		select     count(1) qt_itens
		from	cpoe_gasoterapia 
		where nr_atendimento = nr_atendimento_p  
		and dt_liberacao is not null 
		and	cd_funcao_origem = 2314 
		and cpoe_obter_data_prox_reval(nr_sequencia, 'G') < sysdate 
        and cpoe_obter_se_prescritor_reval(nr_seq_revalidation_rule_p, ie_motivo_prescricao, cd_perfil_ativo, nm_usuario_nrec) = 'S'
		and nvl(decode(dt_lib_suspensao, null, dt_fim, Obter_Data_Menor(dt_suspensao,dt_fim)), sysdate + 1) > (sysdate + (1/24)) 
		and dt_prox_geracao is not null
		and	((cpoe_obter_se_copia_item_term(ds_horarios,
											cd_intervalo,
											dt_prox_geracao + (qt_hors_copia_w/24),
											decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao, dt_fim))) = 'S')
			or ((dt_fim is null) and ie_duracao = 'C'))
		union all
		select     count(1) qt_itens
		from cpoe_material 
		where nr_atendimento = nr_atendimento_p  
		and dt_liberacao is not null 
		and	cd_funcao_origem = 2314 
		and  nvl(ie_material, 'N') = 'N' 
		and cpoe_obter_data_prox_reval(nr_sequencia, 'M') < sysdate
        and cpoe_obter_se_prescritor_reval(nr_seq_revalidation_rule_p, ie_motivo_prescricao, cd_perfil_ativo, nm_usuario_nrec) = 'S'
		and nvl(decode(dt_lib_suspensao, null, nvl(dt_fim_cih,dt_fim), Obter_Data_Menor(dt_suspensao,dt_fim)), sysdate + 1) > (sysdate + (1/24)) 
		and dt_prox_geracao is not null
		and	((cpoe_obter_se_copia_item_term(ds_horarios,
											cd_intervalo,
											dt_prox_geracao + (qt_hors_copia_w/24),
											decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim))) = 'S')
			or ((dt_fim is null) and ie_duracao = 'C'))	
		union all
		select     count(1) qt_itens
		from	cpoe_recomendacao 
		where nr_atendimento = nr_atendimento_p  
		and dt_liberacao is not null 
		and	cd_funcao_origem = 2314 
		and cpoe_obter_data_prox_reval(nr_sequencia, 'R') < sysdate 
        and cpoe_obter_se_prescritor_reval(nr_seq_revalidation_rule_p, ie_motivo_prescricao, cd_perfil_ativo, nm_usuario_nrec) = 'S'
		and nvl(decode(dt_lib_suspensao, null, dt_fim, Obter_Data_Menor(dt_suspensao,dt_fim)), sysdate + 1) > (sysdate + (1/24)) 
		and dt_prox_geracao is not null
		and	((cpoe_obter_se_copia_item_term(ds_horarios,
											cd_intervalo,
											dt_prox_geracao + (qt_hors_copia_w/24),
											decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao, dt_fim))) = 'S')
			or ((dt_fim is null) and ie_duracao = 'C'))
		union all
		select     count(1) qt_itens
		from	cpoe_procedimento 
		where nr_atendimento = nr_atendimento_p  
		and dt_liberacao is not null 
		and	cd_funcao_origem = 2314 
		and cpoe_obter_data_prox_reval(nr_sequencia, 'P') < sysdate
        and cpoe_obter_se_prescritor_reval(nr_seq_revalidation_rule_p, ie_motivo_prescricao, cd_perfil_ativo, nm_usuario_nrec) = 'S'
		and nvl(decode(dt_lib_suspensao, null, dt_fim, Obter_Data_Menor(dt_suspensao,dt_fim)), sysdate + 1) > (sysdate + (1/24)) 
		and dt_prox_geracao is not null
		and	((cpoe_obter_se_copia_item_term(ds_horarios,
											cd_intervalo,
											dt_prox_geracao + (qt_hors_copia_w/24),
											decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao, dt_fim))) = 'S')
			or ((dt_fim is null) and ie_duracao = 'C'))
		union all	
		select     count(1) qt_itens
		from	cpoe_dieta
		where nr_atendimento = nr_atendimento_p  
		and dt_liberacao is not null 
		and	cd_funcao_origem = 2314 
		and cpoe_obter_data_prox_reval(nr_sequencia, 'N') < sysdate
        and cpoe_obter_se_prescritor_reval(nr_seq_revalidation_rule_p, ie_motivo_prescricao, cd_perfil_ativo, nm_usuario_nrec) = 'S'
		and nvl(decode(dt_lib_suspensao, null, dt_fim, Obter_Data_Menor(dt_suspensao,dt_fim)), sysdate + 1) > (sysdate + (1/24))
		and dt_prox_geracao is not null
		and	((cpoe_obter_se_copia_item_term(ds_horarios,
											cd_intervalo,
											dt_prox_geracao + (qt_hors_copia_w/24),
											decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao, dt_fim))) = 'S')
			or ((dt_fim is null) and ie_duracao = 'C')));
end if;

return qt_retorno_w;

end cpoe_obter_count_revalidacao;
/
