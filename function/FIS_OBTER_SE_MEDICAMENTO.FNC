create or replace
function fis_obter_se_medicamento	(cd_material_p		number)
				return varchar2 is
					
ie_medicamento_w		varchar2(1) := 'N';

begin

if cd_material_p > 0 then

	select	nvl(max('S'),'N')
	into	ie_medicamento_w
	from	material a
	where	a.cd_material = cd_material_p
	and	a.ie_tipo_material in ('0','2','3','6','8','9');
	
end if;	

return	ie_medicamento_w;

end fis_obter_se_medicamento;
/
