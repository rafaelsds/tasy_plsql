create or replace
function obter_ds_cor_estag_autorizacao(
		nr_seq_estagio_p	number)
		return varchar2 is

ds_cor_estagio_w varchar2(15);
begin
if	(nr_seq_estagio_p is not null) then
	begin
	select	ds_cor
	into	ds_cor_estagio_w
	from	estagio_autorizacao
	where	nr_sequencia = nr_seq_estagio_p;
	end;
end if;
return ds_cor_estagio_w;
end obter_ds_cor_estag_autorizacao;
/