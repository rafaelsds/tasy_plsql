create or replace
function pls_obter_dt_consist_pessoa(nr_seq_lote_p		number,
					cd_pessoa_fisica_p	varchar2,
					cd_cgc_p		varchar2)
							return date is

dt_retorno_w	date	:= null;

begin

if	(nr_seq_lote_p is not null) then
	if	(cd_pessoa_fisica_p is not null) then
		select	max(a.dt_consistente)
		into	dt_retorno_w
		from	pls_pessoa_inconsistente a
		where	a.nr_seq_lote	= nr_seq_lote_p
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	not exists	(select	1
					from	pls_pessoa_inconsistente x
					where	x.nr_seq_lote	= a.nr_seq_lote
					and	x.cd_pessoa_fisica = a.cd_pessoa_fisica
					and	x.dt_consistente is null);
	else
		select	max(a.dt_consistente)
		into	dt_retorno_w
		from	pls_pessoa_inconsistente a
		where	a.nr_seq_lote	= nr_seq_lote_p
		and	a.cd_cgc 	= cd_cgc_p
		and	not exists	(select	1
					from	pls_pessoa_inconsistente x
					where	x.nr_seq_lote	= a.nr_seq_lote
					and	x.cd_cgc	= a.cd_cgc
					and	x.dt_consistente is null);
	end if;
end if;

return dt_retorno_w;

end;
/