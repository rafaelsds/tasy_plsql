Create or replace
function Obter_turno_hor_prescr_lote(  	cd_estabelecimento_p    number,
                                      	 	cd_setor_atendimento_p  number,
		                                ds_horario_p            varchar2)
                                        	               return number is

nr_sequencia_w      number(10);
nr_seq_turno_w       number(10);
hr_inicial_w	varchar2(5);
hr_final_w              	varchar2(5);
dt_inicial_w            	date;
dt_final_w              	date;
dt_horario_w            date;

Cursor C01 is
        select  a.nr_sequencia,
                to_char(b.hr_inicial,'hh24:mi'),
                to_char(b.hr_final,'hh24:mi')
        from    regra_turno_disp_param b,
                regra_turno_disp a
        where   a.nr_sequencia          = b.nr_seq_turno
        and     a.cd_estabelecimento    = cd_estabelecimento_p
        and     (nvl(b.cd_setor_atendimento,nvl(cd_setor_atendimento_p,0))      = nvl(cd_setor_atendimento_p,0))
        and     a.ie_situacao           = 'A'
        order by nvl(b.cd_setor_atendimento,0),
                to_char(b.hr_inicial,'hh24:mi');

begin

dt_horario_w            := to_date('01/01/2000 '||ds_horario_p||':00','dd/mm/yyyy hh24:mi:ss');

OPEN C01;
LOOP
        FETCH C01 INTO
                nr_sequencia_w,
                hr_inicial_w,
                hr_final_w;
        exit when c01%notfound;
                begin

                dt_inicial_w    := to_date('01/01/2000 '||hr_inicial_w||':00','dd/mm/yyyy hh24:mi:ss');
                dt_final_w      := to_date('01/01/2000 '||hr_final_w||':00','dd/mm/yyyy hh24:mi:ss');

                if      (dt_inicial_w > dt_final_w) then
                        dt_final_w      := dt_final_w + 1;
                        if      (to_char(dt_horario_w,'hh24:mi') < hr_inicial_w) then
                                dt_horario_w    := dt_horario_w + 1;
                        end if;
                end if;

                if      (dt_horario_w >= dt_inicial_w) and
                        (dt_horario_w <= dt_final_w) then
                        nr_seq_turno_w  := nr_sequencia_w;
                end if;

                end;
END LOOP;
CLOSE C01;


return nr_seq_turno_w;

end Obter_turno_hor_prescr_lote;
/