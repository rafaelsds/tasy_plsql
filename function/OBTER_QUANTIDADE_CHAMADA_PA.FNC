create or replace function obter_quantidade_chamada_PA(nr_atendimento_p		number)
 		    	return number is

qt_total_chamada_w	number(15,0);
senha_w number(10,0);

begin

select max(nr_seq_pac_senha_fila)
	into senha_w
	from atendimento_paciente
	where nr_atendimento = nr_atendimento_p;

if (senha_w is null) then
	select 	count(*)
	into 	qt_total_chamada_w
	from 	log_chamada_pa a
	where 	a.nr_atendimento = nr_atendimento_p;

else 
    select nvl(max(QT_CHAMADAS),0)
    into qt_total_chamada_w
    from paciente_senha_fila
    where dt_chamada is not null
    and nr_sequencia = senha_w;
end if; 

return	qt_total_chamada_w;

end obter_quantidade_chamada_PA;
/
