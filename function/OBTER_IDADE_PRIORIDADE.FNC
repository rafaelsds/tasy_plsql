CREATE OR REPLACE
FUNCTION Obter_idade_prioridade(	nr_sequencia_p		number,
					ie_opcao_p		varchar2)
				return number IS

qt_retorno_w		number(15);
qt_idade_min_w		number(15,2);
qt_idade_min_mes_w	number(15,2);
qt_idade_min_dia_w	number(15,2);
qt_idade_max_w		number(15,2);
qt_idade_max_mes_w	number(15,2);
qt_idade_max_dia_w	number(15,2);
dt_referencia_w		date;

begin

select	nvl(max(qt_idade_min),0),
	nvl(max(qt_idade_min_mes),0),
	nvl(max(qt_idade_min_dia),0),
	nvl(max(qt_idade_max),0),
	nvl(max(qt_idade_max_mes),0),
	nvl(max(qt_idade_max_dia),0)
into	qt_idade_min_w,
	qt_idade_min_mes_w,
	qt_idade_min_dia_w,
	qt_idade_max_w,
	qt_idade_max_mes_w,
	qt_idade_max_dia_w
from	TRIAGEM_CLASSIF_PRIORIDADE
where	nr_sequencia	= nr_sequencia_p;

if	(ie_opcao_p = 'MIN') then
	begin
	dt_referencia_w	:= sysdate - qt_idade_min_dia_w;
	dt_referencia_w	:= pkg_date_utils.add_month(dt_referencia_w,qt_idade_min_mes_w * -1,0);
	dt_referencia_w	:= pkg_date_utils.add_month(dt_referencia_w,qt_idade_min_w * 12 * -1,0);
	end;
elsif	(ie_opcao_p = 'MAX') then
	begin
	dt_referencia_w	:= sysdate - qt_idade_max_dia_w;
	dt_referencia_w	:= pkg_date_utils.add_month(dt_referencia_w,qt_idade_max_mes_w * -1,0);
	dt_referencia_w	:= pkg_date_utils.add_month(dt_referencia_w,qt_idade_max_w * 12 * -1,0);
	end;
end if;

qt_retorno_w	:= sysdate - dt_referencia_w;

if	(ie_opcao_p = 'MAX') and
	(qt_retorno_w = 0) then
	qt_retorno_w	:= 9999999999;
end if;

RETURN qt_retorno_w;

END Obter_idade_prioridade;
/