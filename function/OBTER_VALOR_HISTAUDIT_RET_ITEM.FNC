create or replace
function Obter_Valor_HistAudit_Ret_Item
  (nr_sequencia_p	number,
   ie_acao_p		number) return number is

/*
 0 - N�o Auditado
 1 - Recebimento
 2 - Reapresenta��o
 3 - Glosa Devida
 4 - Glosa Indevida
 5 - Adequa��o a menor
 6 - Adequa��o a maior
*/

vl_retorno_w		number(15,2);

begin

select nvl(sum(vl_historico),0)
into vl_retorno_w
from	hist_audit_conta_paciente b,
	conta_paciente_ret_hist a
where a.nr_seq_hist_audit = b.nr_sequencia
  and a.nr_seq_ret_item = nr_sequencia_p
  and b.ie_acao  = ie_acao_p;

return vl_retorno_w;

end;
/