create or replace FUNCTION Get_Ordered_on_behalf(



    nr_sequencia_p NUMBER,



    ie_info_item_p VARCHAR)



  RETURN VARCHAR2



IS



  ds_retorno_w           VARCHAR2(255);



  nr_prescricao_w        NUMBER;



  nm_seocond_user_exisit VARCHAR(1) :='N';

  -- ie_info_item_p  P - Procedure
  --                  D - Material
BEGIN



  IF (nr_sequencia_p IS NOT NULL) THEN



    BEGIN



      IF ( ie_info_item_p = 'P') THEN



        BEGIN



          SELECT OBTER_PRESCR_ITEM_CPOE(nr_sequencia,'P', nr_atendimento, cd_pessoa_fisica)



          INTO nr_prescricao_w



          FROM cpoe_procedimento



          WHERE nr_sequencia = nr_sequencia_p;



        END;



      ELSE



        BEGIN



          SELECT OBTER_PRESCR_ITEM_CPOE(nr_sequencia,DECODE(ie_controle_tempo,'S','SOL','N','M'), nr_atendimento, cd_pessoa_fisica)



          INTO nr_prescricao_w



          FROM cpoe_material



          WHERE nr_sequencia = nr_sequencia_p;



        END;



      END IF;



      SELECT DECODE(COUNT(*),0,'N','S')



      INTO nm_seocond_user_exisit



      FROM PRESCR_MEDICA_COMPL



      WHERE nr_prescricao       = nr_prescricao_w



      AND NM_USUARIO_VALIDACAO IS NOT NULL;



      SELECT DECODE (COUNT(*),0,'N','S')



      INTO ds_retorno_w



      FROM CPOE_ACTION_LOG_V



      WHERE nr_sequencia         = nr_sequencia_p



      AND cd_medico             IS NOT NULL



      AND dt_liberacao          IS NOT NULL



      AND DT_CIENCIA_MEDICO     IS NULL



      AND nm_seocond_user_exisit ='S' 

      

      and ((ds_sigla not in ('M','P')) or (ds_sigla in ('M','P') and IE_ORDERED_ON_BEHALF ='S'));



    END;



  END IF;



RETURN ds_retorno_w;



END Get_Ordered_on_behalf;
/