create or replace
function gera_pdf_convenio ( 	cd_convenio_p		number,
				ie_tipo_convenio_p	number)
				return varchar2 is
ie_retorno_w	varchar2(1) := 'S';
begin

select  decode(count(*),0,'S','N')
into	ie_retorno_w
from 	regra_ger_arq_lab
where	ie_situacao = 'A';

if 	(ie_retorno_w = 'N') then
	begin	
	select  decode(count(*),0,'N','S')
	into	ie_retorno_w
	from 	regra_ger_arq_lab
	where	nvl(cd_convenio,cd_convenio_p) 		= cd_convenio_p
	and	nvl(ie_tipo_convenio,ie_tipo_convenio_p) 	= ie_tipo_convenio_p
	and	ie_situacao 		= 'A';
	end;
end if;

return	ie_retorno_w;

end gera_pdf_convenio;
/