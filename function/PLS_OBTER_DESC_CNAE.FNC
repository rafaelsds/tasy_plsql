create or replace
function pls_obter_desc_cnae
			(	nr_seq_cnae_p		Number)
 		    		return Varchar2 is

ds_retorno_w			Varchar2(255);

begin

select	nvl(max(ds_cnae),'')
into	ds_retorno_w
from	pls_cnae
where	nr_sequencia	= nr_seq_cnae_p;

return	ds_retorno_w;

end pls_obter_desc_cnae;
/