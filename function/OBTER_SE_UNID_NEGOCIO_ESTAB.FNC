create or replace
function Obter_se_unid_negocio_estab(
				cd_estabelecimento_p		Number,
				cd_unidade_negocio_p		Number)
		return varchar2 is

ie_retorno_w		varchar2(1);
qt_existe_w		Number(3);

begin

ie_retorno_w	:= 'N';

select	count(*)
into	qt_existe_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p
and	nr_seq_unid_neg = cd_unidade_negocio_p;
if	(qt_existe_w > 0) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end Obter_se_unid_negocio_estab;
/