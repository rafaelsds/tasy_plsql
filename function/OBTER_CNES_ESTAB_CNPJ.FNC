Create or Replace
Function obter_cnes_estab_cnpj(cnpj_p	varchar2)
			Return varchar2 IS

cd_cnes_w	varchar2(30);

BEGIN

select	max(cd_cns) 
into	cd_cnes_w
from	estabelecimento
where	cd_cgc = cnpj_p;

RETURN cd_cnes_w;

END obter_cnes_estab_cnpj;
/
