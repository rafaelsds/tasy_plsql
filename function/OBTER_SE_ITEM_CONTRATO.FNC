create or replace
	function obter_se_item_contrato(	cd_material_p		number,
									ie_restringe_estab_p varchar2 default 'N', 
									cd_estabelecimento_p number default 0)
return varchar2 is

ds_retorno_w			varchar2(1) := 'N';
qt_registro_w			number(10);

begin

if	(cd_material_p is not null) then
	select count(*) 
	into	qt_registro_w
	from   (select	1
		from	contrato_regra_nf a,
			contrato b
		where	a.nr_seq_contrato = b.nr_sequencia
		and	nvl(a.cd_material, cd_material_p) = cd_material_p
		and	substr(obter_dados_pf_pj(null,b.cd_cgc_contratado,'S'),1,1) = 'A'
		and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
		and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
		and	cd_pessoa_contratada is null
		and	nvl(b.ie_situacao,'A') = 'A'
		and	nvl(a.ie_tipo_regra,'NF') = 'NF'
		and (ie_restringe_estab_p = 'N' 
			 or (a.cd_estab_regra is not null and a.cd_estab_regra = cd_estabelecimento_p)
		     or (a.cd_estab_regra is null and b.cd_estabelecimento = cd_estabelecimento_p)
			)
		union all
		select	1
		from	contrato_regra_nf a,
			contrato b
		where	a.nr_seq_contrato = b.nr_sequencia
		and	nvl(a.cd_material, cd_material_p) = cd_material_p
		and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
		and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
		and	cd_pessoa_contratada is not null
		and	nvl(b.ie_situacao,'A') = 'A'
		and	nvl(a.ie_tipo_regra,'NF') = 'NF'
		and (ie_restringe_estab_p = 'N' 
			 or (a.cd_estab_regra is not null and a.cd_estab_regra = cd_estabelecimento_p)
			 or (a.cd_estab_regra is null and b.cd_estabelecimento = cd_estabelecimento_p)
			));

	if	(qt_registro_w > 0) then
		ds_retorno_w := 'S';
	end if;
end if;
return	ds_retorno_w;

end	obter_se_item_contrato;
/
