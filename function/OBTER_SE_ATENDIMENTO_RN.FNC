create or replace
function obter_se_atendimento_rn	(	nr_atendimento_p	number)
						return varchar2 is

ds_retorno_w	varchar2(1) := 'N';						
begin

if (nvl(nr_atendimento_p,0) > 0) then

	select 	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	atendimento_paciente
	where	nr_atendimento_mae = nr_atendimento_p;
end if;

return	ds_retorno_w;

end obter_se_atendimento_rn;
/