create or replace 
FUNCTION ctb_obter_se_conta_sup_vig(	cd_conta_contabil_p	Varchar2,
					cd_classificacao_p		Varchar2,
					dt_vigencia_p			date)
					return varchar2 is

ie_pertence_w		Varchar2(01)	:= 'N';
cd_classificacao_w		Varchar2(40);

BEGIN

select	substr(ctb_obter_classif_conta(cd_conta_contabil, cd_classificacao, dt_vigencia_p),1,40)
into	cd_classificacao_w
from	conta_contabil
where	cd_conta_contabil	= cd_conta_contabil_p;

if	(substr(cd_classificacao_w,1, length(cd_classificacao_p)) = cd_classificacao_p) then
	ie_pertence_w	:= 'S';
end if;

return ie_pertence_w;

END ctb_obter_se_conta_sup_vig;
/