CREATE OR REPLACE
FUNCTION Obter_cid_secun_atendimento(		
					nr_atendimento_p		Number)
					RETURN Varchar2 IS

dt_diagnostico_w	date;
cd_doenca_w		Varchar2(10);


cursor C01	is
	select	cd_doenca
	from	diagnostico_doenca
	where	nr_atendimento	= nr_atendimento_p
	and	dt_diagnostico	= dt_diagnostico_w
	and	ie_classificacao_doenca = 'S'
	order by dt_atualizacao;

BEGIN

begin
select	max(dt_diagnostico)
into	dt_diagnostico_w
from	diagnostico_medico
where	nr_atendimento	= nr_atendimento_p
and	ie_tipo_diagnostico = 2;
exception
	when no_data_found then
		dt_diagnostico_w := null;
end;

OPEN C01;
	LOOP
	FETCH C01 into
		cd_doenca_w;
	EXIT when C01%notfound;
	
	END LOOP;
CLOSE C01;	

RETURN cd_doenca_w;

END Obter_cid_secun_atendimento;
/