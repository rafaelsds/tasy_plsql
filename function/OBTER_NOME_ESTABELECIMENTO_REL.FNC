CREATE OR REPLACE
FUNCTION OBTER_NOME_ESTABELECIMENTO_REL(
					CD_ESTABELECIMENTO_P number)
					RETURN VARCHAR2 IS

ds_razao_social_w	pessoa_juridica.ds_razao_social%type;
nm_fantasia_w		varchar2(80);
ie_razao_fantasia_w	varchar2(2);
ds_retorno_w		Varchar2(80);

BEGIN
if	(cd_estabelecimento_p	is not null) then
	begin
	select	nvl(a.ie_nome_relatorio,'R'),
		b.ds_razao_social,
		nvl(a.nm_fantasia_estab,b.nm_fantasia)
	into	ie_razao_fantasia_w,
		ds_razao_social_w,
		nm_fantasia_w
	from	estabelecimento a,
             		pessoa_juridica b
	where	a.cd_cgc = b.cd_cgc
	and	a.cd_estabelecimento = cd_estabelecimento_p;
	end;
end if;

if	(ie_razao_fantasia_w = 'R') then
	ds_retorno_w	:= ds_razao_social_w;
elsif	(ie_razao_fantasia_w = 'F') then
	ds_retorno_w	:= nm_fantasia_w;
end if;

RETURN ds_retorno_w;

END Obter_Nome_Estabelecimento_rel;
/
