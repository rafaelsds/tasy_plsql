create or replace
function obter_convenio_proc_adic(	nr_seq_proc_interno_p	number,
					cd_procedimento_p	number,
					nr_seq_agenda_p		number,
					ie_opcao_p		varchar2)
					return varchar2 is

cd_convenio_w	number(6,0) := 0;
cd_categoria_w	varchar2(10);

begin

if	(nr_seq_proc_interno_p is not null) then
	select	max(a.cd_convenio),
		max(a.cd_categoria) 
	into	cd_convenio_w,
		cd_categoria_w
	from	agenda_paciente_proc a,
		agenda_paciente b
	where	a.nr_sequencia		=	b.nr_sequencia
	and	a.nr_seq_proc_interno	=	nr_seq_proc_interno_p
	and	a.nr_sequencia		=	nr_seq_agenda_p;
elsif	(cd_procedimento_p is not null) then
	select	max(a.cd_convenio),
		max(a.cd_categoria)
	into	cd_convenio_w,
		cd_categoria_w
	from	agenda_paciente_proc a,
		agenda_paciente b
	where	a.nr_sequencia		=	b.nr_sequencia
	and	a.cd_procedimento	=	cd_procedimento_p
	and	a.nr_sequencia		=	nr_seq_agenda_p;
end	if;

if	(ie_opcao_p = 'CO') then
	return	cd_convenio_w;
else
	return	cd_categoria_w;
end if;

end obter_convenio_proc_adic;
/