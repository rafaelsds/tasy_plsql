create or replace
function pls_obter_dados_benef_lote
			(	nr_seq_lote_p		Number,
				ie_opcao_p		Varchar2)
 		    		return Varchar2 is

/*
DTC  - Data da contratação do lote
*/
ds_retorno_w		Varchar2(255)	:= '';
dt_contratacao_w	Varchar2(10);

begin

select	to_char(dt_contratacao, 'dd/mm/yyyy') dt
into	dt_contratacao_w
from	pls_lote_inclusao_benef
where	nr_sequencia	= nr_seq_lote_p;


if	(ie_opcao_p = 'DTC') then
	ds_retorno_w := dt_contratacao_w;
end if;	


return	ds_retorno_w;
end pls_obter_dados_benef_lote;
/