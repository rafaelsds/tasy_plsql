CREATE OR REPLACE FUNCTION DAVITA_RELAT_MATERIAIS
(
   ie_tipo_hemodialise_p    VARCHAR2,
   ie_cateter_p             VARCHAR2,
   nr_seq_diametro_agulha_p NUMBER,
   nr_seq_mod_dialisador_p  NUMBER,
   cd_pessoa_fisica_p       VARCHAR2,
   dt_agenda_p              DATE,
   ie_tipo_info_p           VARCHAR2,
   nr_atendimento_p         NUMBER DEFAULT NULL
) RETURN VARCHAR2 IS
   --ie_tipo_hemodialise_p       cpoe_dialise.ie_tipo_hemodialise%TYPE;
   --ie_cateter_p                cpoe_dialise.ie_cateter%TYPE;
   --nr_seq_diametro_agulha_w    cpoe_dialise.nr_seq_diametro_agulha%TYPE;
   --nr_seq_mod_dialisador_p     cpoe_dialise.nr_seq_mod_dialisador%TYPE;
   --cd_pessoa_fisica_p          cpoe_dialise.cd_pessoa_fisica%TYPE;
   --dt_agenda_p              date;
   --ie_tipo_info_p              VARCHAR2(1);
   ie_sorologia_p           NUMBER;
   w_kit_name               kit_material.ds_kit_material%TYPE;
   w_mat_name               VARCHAR2(4000);
   w_item                   VARCHAR2(4000);
   w_return                 VARCHAR2(4000) := '';
   nr_seq_diametro_agulha_w cpoe_dialise.nr_seq_diametro_agulha%TYPE;
BEGIN
   BEGIN
      BEGIN
         SELECT decode(ie_cateter_p,
                       'C',
                       NULL,
                       'S',
                       NULL,
                       decode(nr_seq_diametro_agulha_p,
                              1,
                              NULL,
                              nr_seq_diametro_agulha_p))
           INTO nr_seq_diametro_agulha_w
           FROM dual;
      END;
      IF 1 = 1 THEN
         -- (ie_tipo_hemodialise_p IN ('CH','CHD','CHDF','SCUF','SLED','DPAV','IHDF','IHF','DDC','HD')) THEN
         BEGIN
            SELECT COUNT(1)
              INTO ie_sorologia_p
              FROM hd_paciente_classif_sor
             WHERE cd_pessoa_fisica = cd_pessoa_fisica_p
               AND dt_fim IS NULL
               AND nr_seq_classificacao <> 7;
         EXCEPTION
            WHEN OTHERS THEN
               ie_sorologia_p := 0;
         END;
         --Inicio Acesso = Fistula
         IF ie_cateter_p = 'N' THEN
            --Inicio Diametro Agulha FAV  17
            IF nr_seq_diametro_agulha_w = 2 THEN
               IF ( /*nr_seq_mod_dialisador_p IN (2, 3) AND */
                   ie_sorologia_p > 0) THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 12;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
               IF (ie_sorologia_p = 0) THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 7;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
            END IF;
            --Fim Diametro Agulha FAV  17
            --Inicio Diametro Agulha FAV  16
            IF nr_seq_diametro_agulha_w = 4 THEN
               IF ( /*nr_seq_mod_dialisador_p IN (2, 3) AND*/
                   ie_sorologia_p > 0) THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 11;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
               IF (ie_sorologia_p = 0) THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 8;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
            END IF;
            --Fim Diametro Agulha FAV  16
            --Inicio Diametro Agulha FAV  15
            IF nr_seq_diametro_agulha_w = 3 THEN
               IF ( /*nr_seq_mod_dialisador_p IN (2, 3) AND*/
                   ie_sorologia_p > 0) THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 10;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
               --
               IF (ie_sorologia_p = 0) THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 9;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
            END IF;
            --Fim Diametro Agulha FAV  15
            /*
                    --Inicio Diametro Agulha FAV 16G - Buttonhole
                    IF nr_seq_diametro_agulha_w = 2 THEN
                         IF ( ie_sorologia_p = 0 ) THEN
                            IF ie_tipo_info_p = 'K' THEN
                               BEGIN
                               SELECT ds_kit_material INTO w_kit_name FROM kit_material WHERE cd_kit_material = 13;
                               EXCEPTION
                               WHEN Others THEN
                                  w_kit_name := '';
                               END;            
                               w_return := w_kit_name; 
                            END IF;
            
                            IF ie_tipo_info_p = 'I' THEN
                               BEGIN
                               w_mat_name:='';
                                 FOR R IN ( SELECT CD_MATERIAL, DS_MATERIAL FROM MATERIAL WHERE CD_MATERIAL IN (83819,84655,84833,85052,85259,85353,85363) ORDER BY CD_MATERIAL ) LOOP
            
                                    CASE R.CD_MATERIAL
                                    WHEN 83819 THEN
                                       w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 1';
                                    WHEN 84655 THEN
                                       w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 2';
                                    WHEN 84833 THEN
                                       w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 1';                  
                                    WHEN 85052 THEN             
                                       w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 1';
                                    WHEN 85259 THEN             
                                       w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 1';
                                    WHEN 85353 THEN             
                                       w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 2';
                                    WHEN 85363 THEN             
                                       w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 4';                    
                                    END CASE;                                                  
            
                                    w_item := w_item || Chr(13)||Chr(10) || w_mat_name;
            
                                 END LOOP;
                               EXCEPTION
                               WHEN Others THEN
                                  w_item := '';
                               END;            
                               w_return := w_item; 
                            END IF;
                         END IF;
                    END IF;
                    --Fim Diametro Agulha FAV 16G - Buttonhole
            
                    --Inicio Diametro Agulha FAV 15G - Buttonhole
                    IF nr_seq_diametro_agulha_w = 2 THEN
                      IF (ie_sorologia_p = 0 ) THEN
                            IF ie_tipo_info_p = 'K' THEN
                               BEGIN
                               SELECT ds_kit_material INTO w_kit_name FROM kit_material WHERE cd_kit_material = 14;
                               EXCEPTION
                               WHEN Others THEN
                                  w_kit_name := '';
                               END;            
                               w_return := w_kit_name; 
                            END IF;
            
                            IF ie_tipo_info_p = 'I' THEN
                               BEGIN
                               w_mat_name:='';
                                  FOR R IN ( SELECT CD_MATERIAL, DS_MATERIAL FROM MATERIAL WHERE CD_MATERIAL IN (83819,84655,84833,85052,85259,85351,85363) ORDER BY CD_MATERIAL ) LOOP
            
                                     CASE R.CD_MATERIAL
                                     WHEN 83819 THEN
                                        w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 1';
                                     WHEN 84655 THEN
                                        w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 2';
                                     WHEN 84833 THEN
                                        w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 1';                  
                                     WHEN 85052 THEN             
                                        w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 1';
                                     WHEN 85259 THEN             
                                        w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 1';
                                     WHEN 85351 THEN             
                                        w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 2';
                                     WHEN 85363 THEN             
                                        w_mat_name := R.DS_MATERIAL || ' -  Quantidade: 4';                    
                                     END CASE;                                                  
            
                                     w_item := w_item || Chr(13)||Chr(10) || w_mat_name;
            
                                  END LOOP;
                               EXCEPTION
                               WHEN Others THEN
                                  w_item := '';
                               END;            
                               w_return := w_item; 
                            END IF;
                         END IF;  
                    END IF;
            */ --Fim Diametro Agulha FAV 15G - Buttonhole
         END IF;
         --Fim Acesso = Fistula
         --Inicio Acesso = Cateter
         IF ie_cateter_p IN ('C', 'S', 'T') THEN
            --Inicio Diametro Quando n?o tem agulha e cateter
            IF nr_seq_diametro_agulha_w IS NULL THEN
               IF ( /*nr_seq_mod_dialisador_p IN (2, 3) AND*/
                   ie_sorologia_p > 0) THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 15;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
               IF (ie_sorologia_p = 0) THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 22;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
               --
            END IF;
            --Fim Diametro Quando n?o tem agulha e cateter
         END IF;
         --Fim Acesso = Cateter
         --Inicio Acesso = PTFE
         IF ie_cateter_p = 'P' THEN
            --Inicio Diametro Agulha De FAV 16G
            IF nr_seq_diametro_agulha_w = 4 THEN
               IF ie_sorologia_p = 0 THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 19;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
            END IF;
            --Fim Diametro Agulha De FAV 16G
            --Inicio Diametro Agulha De FAV 17G
            IF nr_seq_diametro_agulha_w = 2 THEN
               IF ie_sorologia_p = 0 THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 21;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
            END IF;
            --Fim Diametro Agulha De FAV 17G
            --Inicio Diametro Agulha De FAV 15G
            IF nr_seq_diametro_agulha_w = 3 THEN
               IF nr_seq_mod_dialisador_p IS NULL
                  AND ie_sorologia_p = 1 THEN
                  IF ie_tipo_info_p = 'K' THEN
                     BEGIN
                        SELECT ds_kit_material
                          INTO w_kit_name
                          FROM kit_material
                         WHERE cd_kit_material = 20;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_kit_name := '';
                     END;
                     w_return := w_kit_name;
                  END IF;
                  IF ie_tipo_info_p = 'I' THEN
                     BEGIN
                        w_mat_name := '';
                        FOR r IN (SELECT DISTINCT obter_desc_material(cd_material) ||
                                                  ' Dose: ' || qt_dose || '(' ||
                                                  cd_unidade_medida || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_recons) ||
                                                  ' Dose: ' || qt_dose_recons || '(' ||
                                                  cd_unid_med_dose_recons || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                  UNION ALL
                                  SELECT DISTINCT obter_desc_material(cd_mat_dil) ||
                                                  ' Dose: ' || qt_dose_dil || '(' ||
                                                  cd_unid_med_dose_dil || ')' ds_material
                                    FROM cpoe_material
                                   WHERE nr_atendimento = nr_atendimento_p
                                     AND dt_liberacao IS NOT NULL
                                     AND cd_mat_recons IS NOT NULL
                                   ORDER BY ds_material)
                        LOOP
                           w_mat_name := r.ds_material;
                           w_item     := w_item || chr(13) || chr(10) ||
                                         w_mat_name;
                        END LOOP;
                     EXCEPTION
                        WHEN OTHERS THEN
                           w_item := '';
                     END;
                     w_return := w_item;
                  END IF;
               END IF;
            END IF;
            --Fim Diametro Agulha De FAV 15G     
         END IF;
      END IF;
   END;
   RETURN w_return;
END;
/
