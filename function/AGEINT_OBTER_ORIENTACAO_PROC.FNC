create or replace
function ageint_obter_orientacao_proc	(
					cd_procedimento_p			number,
					ie_origem_proced_p			number,
					cd_convenio_p				number,
					nr_seq_proc_interno_p		number,
					nr_atendimento_p			number,
					nr_seq_tipo_classif_pac_p 	number,
					cd_paciente_p				varchar2,
					nm_usuario_p				varchar2,
					cd_estabelecimento_p		number,
					nr_seq_ageint_item_p		number,
					ie_resumo_p					varchar2 default 'S')
					return varchar2 is

ds_orientacao_w		varchar2(32000) := '';
ds_orientacao_pac_w	varchar2(6000) := '';
ds_orientacao_obs_w	varchar2(32000)	:= '';
ds_orient_convenio_w	varchar2(2000) := '';
ie_orientacao_w		varchar2(01);
cd_plano_convenio_w	varchar2(10);
cd_categoria_w		varchar2(10);
qt_idade_w		number(10,0);
ds_orient_paciente_w	varchar2(4000) := '';
nr_seq_classif_w	number(10,0); 
cd_estabelecimento_w	number(4,0);
ds_orient_pac_preparo_w	varchar2(4000);
ie_forma_orientacao_w	varchar2(1);
ds_orientacao_externo_w	varchar2(4000);
cd_medico_item_w		agenda_integrada_prof_item.cd_pessoa_fisica%type;

Cursor C01 is
	select	ds_orientacao
	from	proc_interno_conv_orient
	where	nvl(nr_seq_proc_interno,nvl(nr_seq_proc_interno_p,0)) = nvl(nr_seq_proc_interno_p,0)
	and	((cd_convenio = cd_convenio_p) or (cd_convenio is null))
	and	((nvl(cd_categoria, nvl(cd_categoria_w,'0')) = nvl(cd_categoria_w,'0')) or (nvl(cd_categoria_w,'0') = '0'))
	and	((nvl(cd_plano, nvl(cd_plano_convenio_w,'0')) = nvl(cd_plano_convenio_w,'0')) or (nvl(cd_plano_convenio_w,'0') = '0'))
	and	qt_idade_w between nvl(qt_idade_min, qt_idade_w) and nvl(qt_idade_max, qt_idade_w)
	and	(nvl(nr_seq_classif, nvl(nr_seq_classif_w,0)) = nvl(nr_seq_classif_w,0))
	order by	nvl(nr_seq_proc_interno,0),	
		nvl(nr_seq_classif,0);
	
Cursor C02 is	
	select	ds_orientacao_pac,
		ds_orient_preparo
	from	proc_interno_pac_orient
	where	nvl(nr_seq_proc_interno,nvl(nr_seq_proc_interno_p,0)) = nvl(nr_seq_proc_interno_p,0)
	and	nvl(ie_situacao,'A') = 'A'
	and	qt_idade_w between nvl(qt_idade_min, qt_idade_w) and nvl(qt_idade_max, qt_idade_w)
	and	(nvl(nr_seq_classif, nvl(nr_seq_classif_w,0)) = nvl(nr_seq_classif_w,0))
	and	(nvl(nr_seq_tipo_classif_pac, nvl(nr_seq_tipo_classif_pac_p,0)) = nvl(nr_seq_tipo_classif_pac_p,0))
	and	(nvl(cd_estabelecimento, nvl(cd_estabelecimento_p,1)) = nvl(cd_estabelecimento_p,1))
	and (nvl(cd_pessoa_fisica, nvl(cd_medico_item_w,0)) = nvl(cd_medico_item_w,0))
	order by	nvl(nr_seq_proc_interno,0),	
		nvl(nr_seq_classif,0);


begin

--cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

Obter_Param_Usuario(869,106,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_forma_orientacao_w);

select	max(ds_orientacao)
into	ds_orientacao_w
from	procedimento
where	cd_procedimento = cd_procedimento_p
and	ie_origem_proced = ie_origem_proced_p;
if	(nvl(nr_seq_proc_interno_p,0) > 0) then
	select	max(ie_orientacao),
		max(ds_orientacao_usuario),
		max(DS_OBSERVACAO),
		max(DS_ORIENTACAO_PAC),
		max(DS_ORIENTACAO_EXTERNO)
	into	ie_orientacao_w,
		ds_orientacao_w,
		ds_orientacao_pac_w,
		ds_orientacao_obs_w,
		ds_orientacao_externo_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_p;
	--Ra1ise_app1lication_er1ror(-20011,ds_orientacao_obs_w);
	select	nvl(max(cd_plano_convenio),'0'),
		nvl(max(cd_categoria),'0')
	into	cd_plano_convenio_w,
		cd_categoria_w
	from	atend_categoria_convenio
	where	nr_atendimento = nr_atendimento_p;
	
	select	nvl(max(campo_numerico(obter_idade_pf(cd_pessoa_fisica, sysdate, 'A'))),0)
	into	qt_idade_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	if	(cd_paciente_p is not null) then
		select	to_number(obter_idade(dt_nascimento, sysdate, 'A'))
		into	qt_idade_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_paciente_p;
	end if;

	select	nvl(max(nr_seq_classif),0)
	into	nr_seq_classif_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_p;
	
	ds_orient_convenio_w := '';
	ds_orient_paciente_w := '';
	
	if (ie_resumo_p = 'S') then
		open C01;
		loop
		fetch C01 into	
			ds_orient_convenio_w;
		exit when C01%notfound;
			begin
			ds_orient_convenio_w := ds_orient_convenio_w;
			end;
		end loop;
		close C01;
	end if;
	
	/*busca o profissional do item*/
	select	max(cd_pessoa_fisica)
	into	cd_medico_item_w
	from 	agenda_integrada_prof_item
	where	nr_seq_agenda_item = nr_seq_ageint_item_p;
	
	open C02;
	loop
	fetch C02 into	
		ds_orient_paciente_w,
		ds_orient_pac_preparo_w;
	exit when C02%notfound;
		begin
		ds_orient_paciente_w 	:= ds_orient_paciente_w;
		ds_orient_pac_preparo_w	:= ds_orient_pac_preparo_w;
		end;
	end loop;
	close C02;
	
	if	(ie_forma_orientacao_w	= 'S') then
		if	(ie_orientacao_w = 'C') then
			ds_orientacao_w := ds_orient_convenio_w;
		elsif	(ie_orientacao_w = 'A') and
			((ds_orientacao_w  is not null) or
			(ds_orient_convenio_w is not null) or
			(ds_orient_paciente_w is not null) or
			(ds_orientacao_pac_w is not null)) then
			--(ds_orientacao_obs_w is not null))then
			ds_orientacao_w := 	substr(ds_orientacao_w || chr(13) || chr(10) || 
						ds_orient_convenio_w || chr(13) || chr(10) || 
						ds_orient_paciente_w||chr(13)||chr(10)||
						ds_orientacao_pac_w,1,32000);--||chr(13)||chr(10)||
						--ds_orientacao_obs_w,1,4000);
		elsif	(ie_orientacao_w	= 'I') and
			((ds_orientacao_w	is not null) or
			(ds_orientacao_pac_w	is not null)) then
			--(ds_orientacao_obs_w is not null)) 		then
			ds_orientacao_w := 	substr(ds_orientacao_w || chr(13) || chr(10) || 
						ds_orientacao_pac_w,1,32000);--||chr(13)||chr(10)||
						--ds_orientacao_obs_w,1,4000);
			
		end if;
	elsif	(ie_forma_orientacao_w	= 'P') then
		ds_orientacao_w	:= '';
		ds_orientacao_w	:= substr(ds_orientacao_w || chr(13) || chr(10) || 					
						ds_orient_paciente_w||chr(13)||chr(10)||
					  ds_orientacao_w || chr(13) || chr(10) || 					
						wheb_mensagem_pck.get_texto(791276) || ': '|| chr(13) || chr(10) || chr(13) || chr(10) || 
						ds_orient_pac_preparo_w,1,32000);
	elsif	(ie_forma_orientacao_w	= 'T') then
		ds_orientacao_w	:= substr(	ds_orientacao_w||chr(13)||chr(10)||
						ds_orientacao_obs_w,1,32000);
	elsif	(ie_forma_orientacao_w	= 'I') then
		if	(ie_orientacao_w = 'C') then
			ds_orientacao_w := ds_orient_convenio_w;
		elsif	(ie_orientacao_w = 'A') and
			((ds_orientacao_w  is not null) or
			(ds_orient_convenio_w is not null) or
			(ds_orient_paciente_w is not null) or
			(ds_orient_pac_preparo_w is not null)) then
			ds_orientacao_w := 	substr(ds_orientacao_w || chr(13) || chr(10) || 						
						ds_orient_convenio_w || chr(13) || chr(10) || 
						ds_orient_paciente_w||chr(13)||chr(10)||
						ds_orient_pac_preparo_w,1,32000);
		elsif	(ie_orientacao_w	= 'I') and
			((ds_orientacao_w	is not null) or
			(ds_orientacao_pac_w	is not null)) then
			ds_orientacao_w := 	substr(ds_orientacao_w || chr(13) || chr(10) || 						
						ds_orientacao_pac_w,1,32000);
		end if;
	elsif	(ie_forma_orientacao_w = 'E') then
		ds_orientacao_w	:= substr(ds_orientacao_externo_w,1,4000);
	elsif	(ie_forma_orientacao_w = 'R') then
		ds_orientacao_w	:= substr(ds_orientacao_w,1,4000);		
	end if;
end if;

return substr(ds_orientacao_w,1,4000);

end ageint_obter_orientacao_proc;
/
