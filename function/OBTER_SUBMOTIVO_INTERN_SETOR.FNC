create or replace
function Obter_submotivo_intern_setor(NR_SEQ_MOTIVO_INT_SUB_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(100);
			
begin

select  DS_SUBMOTIVO
into	ds_retorno_w
from	MOTIVO_INT_SETOR_SUB
where	nr_Sequencia = NR_SEQ_MOTIVO_INT_SUB_p;

return	ds_retorno_w;

end Obter_submotivo_intern_setor;
/