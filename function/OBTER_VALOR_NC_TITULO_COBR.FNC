create or replace
function OBTER_VALOR_NC_TITULO_COBR
			(	nr_seq_titulo_cobr_p		Number)
 		    	return number is

vl_retorno_w	number(15,2);
qt_baixa_w	number(10);

begin

select	count(*)
into	qt_baixa_w
from	nota_credito_baixa a
where	a.nr_seq_tit_rec_cobr	= nr_seq_titulo_cobr_p;

if	(qt_baixa_w	> 0) then
	select	sum(a.vl_baixa)
	into	vl_retorno_w
	from	nota_credito_baixa a
	where	a.nr_seq_tit_rec_cobr	= nr_seq_titulo_cobr_p;
end if;

if	(nvl(vl_retorno_w,0) = 0) then
	select	sum(a.vl_nota_credito)
	into	vl_retorno_w
	from	titulo_receber_nc a,
		titulo_receber_cobr b
	where	b.nr_sequencia	= nr_seq_titulo_cobr_p
	and	b.nr_titulo	= a.nr_titulo_rec
	and	a.nr_seq_liq	is null;
end if;

return	nvl(vl_retorno_w,0);

end OBTER_VALOR_NC_TITULO_COBR;
/