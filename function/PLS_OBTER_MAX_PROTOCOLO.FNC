create or replace
function pls_obter_max_protocolo
		(	nr_seq_prestador_p	number)
 		    	return 			number is
			
nr_seq_protocolo_w	number(10);

begin

select  nvl(max(nr_sequencia), 0)
into	nr_seq_protocolo_w
from    pls_protocolo_conta
where   ie_status = 1
and     nr_seq_prestador = nr_seq_prestador_p
and     ie_situacao  = 'DW'
and     trunc(dt_mes_competencia, 'month') = trunc(sysdate, 'month');

return	nr_seq_protocolo_w;
end 	pls_obter_max_protocolo;
/