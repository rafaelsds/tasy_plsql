create or replace
function HD_obter_tempo_dialise		(cd_pessoa_fisica_p	varchar2,
					seq_grid_p		number) 
					return number is
	
qt_hora_sessao_w	number(2);
dt_referencia_w		date;

begin

if (cd_pessoa_fisica_p is not null) and (seq_grid_p is not null) then
	select 	dt_referencia
	into	dt_referencia_w
	from	HD_EXAME_GRID_CONTROLE
	where 	nr_sequencia = seq_grid_p;
		
	select 	nvl(round(sum((dt_fim_dialise - dt_inicio_dialise)*24)/count(*),2),0)
	into	qt_hora_sessao_w
	from	hd_dialise
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_fim_dialise is not null
	and	dt_inicio_dialise is not null
	and	trunc(dt_dialise,'month') = trunc(dt_referencia_w,'month');
end if;

return qt_hora_sessao_w;

end;
/