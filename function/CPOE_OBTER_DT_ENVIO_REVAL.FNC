create or replace
function cpoe_obter_dt_envio_reval(	nr_seq_cpoe_p number,
										ie_tipo_item_p varchar2)
				return date is

dt_envio_comunicado_w 	cpoe_revalidation_events.dt_envio_comunicado%type;

begin

if (nr_seq_cpoe_p is not null and ie_tipo_item_p is not null) then
	select	dt_envio_comunicado
	into	dt_envio_comunicado_w
	from	cpoe_revalidation_events
	where	nr_sequencia = 	cpoe_obter_seq_reval_events(nr_seq_cpoe_p, ie_tipo_item_p);
end if;

return dt_envio_comunicado_w;

end cpoe_obter_dt_envio_reval;
/