create or replace
FUNCTION obter_se_check_list_liberado(nr_seq_servico_p		NUMBER)
 		    	RETURN VARCHAR2 IS

ds_retorno_w	VARCHAR2(1);
ie_ignorar_checklist_w	varchar2(1);
		
BEGIN


SELECT 	DECODE(COUNT(*),0,'N','S')
INTO	ds_retorno_w
FROM	sl_check_list_unid
WHERE	nr_seq_sl_unid	= nr_seq_servico_p
and	dt_liberacao is not null;

if	(ds_retorno_w = 'N') then
	
	select	nvl(max(ie_ignorar_checklist),'N')
	into	ie_ignorar_checklist_w
	from	unidade_atendimento a,
		sl_unid_atend b
	where	b.nr_seq_unidade = a.nr_seq_interno
	and	b.nr_sequencia	= nr_seq_servico_p;
	
	if	(ie_ignorar_checklist_w = 'S') then
		ds_retorno_w	:= 'S';
	end if;	
end if;

RETURN	ds_retorno_w;

END obter_se_check_list_liberado;
/
