create or replace
function pls_obter_regra_contrato_web
			(	nr_seq_contrato_p	number,
				nr_seq_segurado_p	number,
				ie_tipo_regra_p		varchar2)
				return varchar2 is

ds_retorno_w			varchar2(5)	:= null;
nr_seq_contrato_w		number(10);
ie_tipo_estipulante_w		varchar2(2);
ie_tipo_pagador_w		varchar2(2);

/*
ie_tipo_regra_p
TM - Permite o titular visualizar todos as mensalidades do contrato
PM - Permite somente os pagadores do contrato visualizarem as mensalidades

*/

Cursor C01 is
	select	nvl(ie_visualiza_demonstrativo,'N')
	from 	pls_regra_contrato_web
	where	(nr_seq_contrato = nr_seq_contrato_w or nr_seq_contrato is null)
	and	((ie_tipo_estipulante = ie_tipo_estipulante_w) or (nvl(ie_tipo_estipulante,'A') = 'A'))
	and	((ie_tipo_pagador = ie_tipo_pagador_w) or (nvl(ie_tipo_pagador,'A') = 'A'))
	and	ie_visualiza_demonstrativo = ie_tipo_regra_p
	and	sysdate	between dt_inicio_vigencia and nvl(dt_fim_vigencia, sysdate)
	order by nvl(nr_seq_contrato,0),
		nvl(ie_tipo_estipulante,'A');

begin

if	(ie_tipo_regra_p in ('TM', 'PM')) then
	if	(nr_seq_contrato_p is null) then
		nr_seq_contrato_w 	:= pls_obter_dados_segurado(nr_seq_segurado_p, 'NC');
		ie_tipo_estipulante_w	:= pls_obter_dados_contrato(nr_seq_contrato_w, 'TE');
	else
		nr_seq_contrato_w 	:= nr_seq_contrato_p;
		ie_tipo_estipulante_w	:= pls_obter_dados_contrato(nr_seq_contrato_w, 'TE');
	end if;
	
	if	(nr_seq_segurado_p is not null) then
		begin
		select	decode(a.cd_pessoa_fisica, null, 'PJ', 'PF')
		into	ie_tipo_pagador_w
		from	pls_contrato_pagador a,
			pls_segurado b
		where	a.nr_sequencia	= b.nr_seq_pagador
		and	b.nr_sequencia	= nr_seq_segurado_p;
		exception
		when others then
			ie_tipo_pagador_w	:= 'PJ';
		end;
	else
		ie_tipo_pagador_w	:= 'PJ';
	end if;
	
	open C01;
	loop
	fetch C01 into
		ds_retorno_w;
	exit when C01%notfound;
	end loop;
	close C01;
end if;

return	nvl(ds_retorno_w,'N');

end pls_obter_regra_contrato_web;
/