create or replace
function obter_se_tipo_aten_agenda(	ie_tipo_atendimento_p	number,
				cd_agenda_p		number)
				return varchar2 is
ie_possui_tipo_w	varchar2(1);
ie_consiste_w	varchar2(1):= 'N';
begin

select 	decode(count(*),0,'N','S')  
into	ie_possui_tipo_w
from   	agenda_tipo_atendimento 
where  	cd_agenda = cd_agenda_p;

if (ie_possui_tipo_w = 'S') then
	select 	decode(count(*),0,'S','N')
	into	ie_consiste_w
	from   	agenda_tipo_atendimento 
	where  	cd_agenda 		= cd_agenda_p
	and	ie_tipo_atendimento    	= ie_tipo_atendimento_p;
end if;	
	
return ie_consiste_w;

end obter_se_tipo_aten_agenda;
/