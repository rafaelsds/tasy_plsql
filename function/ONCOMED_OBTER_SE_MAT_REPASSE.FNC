create or replace
function ONCOMED_OBTER_SE_MAT_REPASSE(nr_seq_mat_p        number) return number is

cont_w  number(15);

/*
retorno:

0 - Gera repasse
1 - N�o gera
*/

begin

select	count(*)
into	cont_w
from	material_atend_paciente a
where	a.nr_sequencia		= nr_seq_mat_p
and     exists 	(
	select	1
	from	atendimento_paciente b,
		pessoa_fisica c
	where	b.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and	b.nr_atendimento	= a.nr_atendimento
	and	c.cd_medico		in (93,292) 
	);

if      (cont_w > 0) then
        return 1;
else
        return 0;
end if;

end ONCOMED_OBTER_SE_MAT_REPASSE;
/
