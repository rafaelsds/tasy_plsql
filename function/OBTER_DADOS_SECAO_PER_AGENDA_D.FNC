create or replace
function obter_dados_secao_per_agenda_d(	nr_seq_agenda_p		number,
											ie_diario_p			varchar2,
											ie_final_semana_p	varchar2,
											ie_opcao_p			varchar2,
											ds_dias_p			varchar2)
											return varchar2 is

qt_dia_w			number(2);
qt_total_secao_w	number(3,0);
nr_secao_w			number(4,0);
dt_agenda_w			date;
dt_atual_w			date;
cd_dia_semana_w		varchar2(1);
ds_retorno_w		varchar2(255);
ie_bloqueio_w		varchar2(1);
ie_existe_turno_w	varchar2(1);
ds_dias_w		varchar2(255);
ie_gerar_dia_w		varchar2(1)  := 'S';
qt_feriado_dia_w	number(10);
ie_agenda_feriado_w	varchar(1);
cd_agenda_w			number(10);
cd_estabelecimento_w		number(4,0);	
ie_existe_livre_w  varchar2(1);
ie_dia_gerado_w  varchar2(1);
qt_tentativas_w	number(10) := 2000;
qt_tentativas_atual_w number(10) := 0;


/*
D - Data t�rmino
S - Qtde Se��es no per�odo
*/

begin

if	(nr_seq_agenda_p is not null) then
	begin
	/* obter dados agenda */
	select	max(nvl(a.nr_secao,1)),
			max(a.dt_agenda),
			max(nvl(a.qt_total_secao,0)),
			max(b.cd_estabelecimento)
	into	nr_secao_w,
			dt_agenda_w,
			qt_total_secao_w,
			cd_estabelecimento_w
	from	agenda_consulta a,
		agenda b
	where	a.cd_agenda = b.cd_agenda
	and	a.nr_sequencia = nr_seq_agenda_p;
	
	if	(ds_dias_p is not null) then
	
		ds_dias_w := ds_dias_p;
		
		if	(ie_final_semana_p = 'N') then
			
			if not( (obter_se_contido('2' ,ds_dias_w) = 'S') or 
					(obter_se_contido('3' ,ds_dias_w) = 'S') or 
					(obter_se_contido('4' ,ds_dias_w) = 'S') or 
					(obter_se_contido('5' ,ds_dias_w) = 'S') or
					(obter_se_contido('6' ,ds_dias_w) = 'S'))  then
					qt_total_secao_w := 0;
			end if;	
		
		end if;
		
	end if;
	
	--nr_secao_w	:= 1;
	dt_atual_w	:= dt_agenda_w;
	if	(qt_total_secao_w > 0) then
		
		while	(nr_secao_w <= qt_total_secao_w and qt_tentativas_atual_w < qt_tentativas_w) loop
			begin
						
			select	substr(Obter_Cod_Dia_Semana(dt_atual_w),1,1)
			into	cd_dia_semana_w
			from	dual;	
						
			if	(ie_final_semana_p = 'N') and
				(((cd_dia_semana_w = 1)) or ((cd_dia_semana_w = 7)))then
					
				if	(cd_dia_semana_w = 1) then
					--SE N�O GERAR NO FINAL DE SEMANA, CASO FOR DOMINGO(1) ADICIONA +1 DIA A DATA ATUAL
					dt_atual_w	:= dt_atual_w + 1;
				elsif	(cd_dia_semana_w = 7) then
						--SE N�O GERAR NO FINAL DE SEMANA, CASO FOR SABADO(7) ADICIONA +2 DIAS A DATA ATUAL
						dt_atual_w	:= dt_atual_w + 2;
				end if;
				
				select	substr(Obter_Cod_Dia_Semana(dt_atual_w),1,1)
				into	cd_dia_semana_w
				from	dual;	
				
			end if;
			
			--VALIDAR SE EXISTE FERIADO NO MEIO DO PER�ODO DE GERA��O DOS DIAS DA AGENDA
			select	nvl(max(obter_se_feriado(cd_estabelecimento_w, dt_atual_w)), 0)
			into	qt_feriado_dia_w
			from	dual;
			
			--BUSCAR C�D. DA AGENDA PARA VERIFICAR SE � PERMITIDO O AGENDAMENTO EM DIAS QUE S�O FERIADO
			select	nvl(max(cd_agenda), 0)
			into	cd_agenda_w
			from	agenda_consulta
			where	nr_sequencia	= nr_seq_agenda_p;
			
			if	(cd_agenda_w > 0)then
				begin
				
				select 	nvl(max(ie_feriado),'N')
				into	ie_agenda_feriado_w
				from	agenda
				where	cd_agenda = cd_agenda_w;
				
				end;			
			end if;
			
			/* validar hor�rio x bloqueio */
			select	decode(count(*),0,'N','S')
			into	ie_bloqueio_w
			from	agenda_bloqueio
			where	cd_agenda = cd_agenda_w
			and	dt_final >= trunc(dt_atual_w)
			and	dt_inicial <= trunc(dt_atual_w)
			AND	(TO_CHAR(hr_final_bloqueio,'hh24:mi:ss') >= TO_CHAR(dt_atual_w,'hh24:mi') 
				OR hr_final_bloqueio IS NULL)
			AND	(TO_CHAR(hr_inicio_bloqueio, 'hh24:mi:ss') <= TO_CHAR(dt_atual_w,'hh24:mi') 
				OR hr_inicio_bloqueio IS NULL)
			and	((ie_dia_semana is null) or (ie_dia_semana = cd_dia_semana_w));
			
			/*
			VIG�NCIAS DO TURNO:
			*/
			select	decode(count(*),0,'N','S')
			into	ie_existe_turno_w
			from   	agenda_turno
			where  	cd_agenda       	= 	cd_agenda_w
			and    	((ie_dia_semana   	= 	cd_dia_semana_w) 
				or (ie_dia_semana = 9))
			AND	TO_CHAR(hr_final, 'hh24:mi:ss') >= TO_CHAR(dt_atual_w,'hh24:mi:ss')
			AND	TO_CHAR(hr_inicial, 'hh24:mi:ss') <= TO_CHAR(dt_atual_w,'hh24:mi:ss');	
			
			/* validar se existe horario */
			select	decode(count(*),0,'N','S')
			into	ie_existe_livre_w
			from	agenda_consulta
			where	cd_agenda = cd_agenda_w
			and		trunc(dt_agenda) = trunc(dt_atual_w)
			and 	ie_status_agenda = 'L';
			
			/*Validar se dia foi gerado*/
			select	decode(count(*),0,'N','S')
			into	ie_dia_gerado_w
			from	agenda_consulta
			where	cd_agenda = cd_agenda_w
			and		trunc(dt_agenda) = trunc(dt_atual_w);
						
		
			if	(ds_dias_w is not null) then
				ie_gerar_dia_w	:=  obter_se_contido(cd_dia_semana_w, ds_dias_w);
			end if;
			
						
			if	(ie_existe_turno_w = 'N') and
				(ie_gerar_dia_w = 'S') then
				ie_gerar_dia_w	:= 'N';			
			end if;
			
			if (ie_existe_livre_w = 'N' and ie_dia_gerado_w = 'S') and
				(ie_gerar_dia_w	= 'S')	then
				ie_gerar_dia_w	:= 'N';
			end if;
			
			if	(ie_bloqueio_w = 'S') and
				(ie_gerar_dia_w = 'S')  then
				ie_gerar_dia_w	:= 'N';
			end if;	
			
			if	(qt_feriado_dia_w > 0) and
				(ie_gerar_dia_w = 'S') and
				(ie_agenda_feriado_w = 'N')then
				ie_gerar_dia_w	:= 'N';
			end if;

			
			if	(ie_gerar_dia_w	= 'S') then
				nr_secao_w	:= nr_secao_w + 1;
			else 
				qt_tentativas_atual_w := qt_tentativas_atual_w + 1;
			end if;
			
			if	(nr_secao_w <= qt_total_secao_w)then
				dt_atual_w	:= dt_atual_w + 1;
			end if;				
	
			end;
		end loop;
	end if;
	
	if (qt_tentativas_atual_w = qt_tentativas_w) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1058532);
	end if;
	
	if	(ie_opcao_p = 'D') then
		ds_retorno_w	:= dt_atual_w;
	elsif	(ie_opcao_p = 'S') then
		ds_retorno_w	:= nr_secao_w;
	end if;
	end;
end if;
	
return ds_retorno_w;

end obter_dados_secao_per_agenda_d;
/
