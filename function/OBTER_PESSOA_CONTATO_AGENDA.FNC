create or replace
function Obter_pessoa_contato_agenda(	cd_pessoa_fisica_p	varchar2,
											dt_agenda_p			date)
										return varchar2 is

										
nm_pessoa_contato_w		varchar2(50):= '';
dt_agenda_w				date;
dt_agenda_agecons_w		date;
dt_agenda_agepac_w		date;
										
/*Function utilizada para obter a pessoa de contato do paciente, de acordo com o �ltimo agendamento realizado pelo mesmo
independente de status e agenda*/										
begin
dt_agenda_w	:= dt_agenda_p;

if	(cd_pessoa_fisica_p is not null) and
	(dt_agenda_w is not null)then 
	begin		
	--Agenda de Consultas
	select 	max(dt_agenda)
	into	dt_agenda_agecons_w
	from	agenda_consulta
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_p
	and		dt_agenda 			< dt_agenda_w
	and		nm_pessoa_contato	is not null;
	
	--Agenda de Exames
	select  max(hr_inicio)
	into	dt_agenda_agepac_w
	from	agenda_paciente
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_p
	and		hr_inicio	 		< dt_agenda_w
	and		nm_pessoa_contato	is not null;			
		
		--Se existirem agendamentos anteriores...		
		if	(dt_agenda_agecons_w is not null) or 
			(dt_agenda_agepac_w is not null)then
		
			if	(dt_agenda_agecons_w is null) then
				dt_agenda_agecons_w :=  to_date('01/01/1899 00:00:01', 'dd/mm/yyyy hh24:mi:ss');
			end if;
			
			if	(dt_agenda_agepac_w is null) then
				dt_agenda_agepac_w :=  to_date('01/01/1899 00:00:01', 'dd/mm/yyyy hh24:mi:ss');
			end if;
		
			--Compara as datas dos agendamentos da AGENDA_CONSULTA x AGENDA_PACIENTE verificando qual o mais recente, retornando a pessoa contato para o 'nm_pessoa_contato_w'
			if	(dt_agenda_agecons_w > dt_agenda_agepac_w) then
				select	max(nm_pessoa_contato)
				into	nm_pessoa_contato_w
				from	agenda_consulta
				where	trunc(dt_agenda) 	= trunc(dt_agenda_agecons_w)
				and		cd_pessoa_fisica	= cd_pessoa_fisica_p
				and		nm_pessoa_contato	is not null
				and		ie_status_agenda	not in('C', 'B', 'L', 'I', 'F', 'II', 'R');
			elsif
				(dt_agenda_agecons_w < dt_agenda_agepac_w) then
				select	max(nm_pessoa_contato)
				into	nm_pessoa_contato_w
				from	agenda_paciente
				where	trunc(hr_inicio) 	= trunc(dt_agenda_agepac_w)
				and		cd_pessoa_fisica	= cd_pessoa_fisica_p
				and		nm_pessoa_contato	is not null
				and		ie_status_agenda	not in('C', 'B', 'L', 'I', 'F', 'II', 'R');
			end if;							
			
		end if;				
	
	end;
	end if;

return	nm_pessoa_contato_w;

end Obter_pessoa_contato_agenda;
/