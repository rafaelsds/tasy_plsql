create or replace
function pls_obter_ds_alegacao_relat	(	nr_seq_conta_p 	pls_processo_conta.nr_sequencia%type,
						cd_alegacao_p	pls_alegacao_processo.cd_alegacao%type)
						return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [ X ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

--declaracao das variaveis usadas pela funcao.
ds_retorno_w			varchar2(4000);
cd_alegacao_w 			pls_alegacao_processo.cd_alegacao%type;
ds_cd_alegacao_w 		varchar2(3) := '( )';
cd_clausula_w			pls_impugnacao.cd_clausula%type;
ds_termo_fim_carencia_w		pls_impugnacao.ds_termo_fim_carencia%type;
ds_procedimento_w		varchar2(4000) := '';
nr_seq_impug_w			pls_impugnacao.nr_sequencia%type;
cd_clausula_reembolso_w 	pls_impugnacao.cd_clausula_reembolso%type;
cd_clausula_atend_trans_w	pls_impugnacao.cd_clausula_atend_trans%type;
qt_cobertura_w			pls_impugnacao.qt_cobertura%type;
vl_deferido_w			pls_impugnacao.vl_deferido%type;
ds_especificacao_w		pls_impugnacao.ds_especificacao%type;
nr_seq_processo_w		pls_impugnacao_proc.nr_seq_processo_proc%type;

cursor c01 is
	select 	nr_seq_processo_proc
	from	pls_impugnacao_proc
	where	nr_seq_impugnacao = nr_seq_impug_w;

begin

--obter dados da impugnacao
select	max(a.cd_alegacao) cd_alegacao,
	max(b.cd_clausula) cd_clausula,
	max(b.ds_termo_fim_carencia) ds_termo,
	max(b.nr_sequencia) nr_seq,
	max(b.cd_clausula_reembolso) cd_cl_reembolso,
	max(b.cd_clausula_atend_trans) cd_cl_transito,
	max(b.qt_cobertura) qt_cobertura,
	max(b.vl_deferido) vl_deferido,
	max(b.ds_especificacao) ds_especificacao
into	cd_alegacao_w,
	cd_clausula_w,
	ds_termo_fim_carencia_w,
	nr_seq_impug_w,
	cd_clausula_reembolso_w,
	cd_clausula_atend_trans_w,
	qt_cobertura_w,
	vl_deferido_w,
	ds_especificacao_w
from	pls_alegacao_processo a,
	pls_impugnacao b,
	pls_processo_conta c
where	a.nr_sequencia = b.nr_seq_motivo_impugnacao
and	b.nr_seq_conta = c.nr_sequencia
and	c.nr_sequencia = nr_seq_conta_p
and	a.cd_alegacao = cd_alegacao_p;

--obter o processo do procedimento impugnado
if(nr_seq_impug_w is not null)then
	open c01;
	loop
	fetch c01 into
		nr_seq_processo_w;
	exit when c01%notfound;
		begin

		--obeter os codigos dos procedimentos impugnados
		if( nr_seq_processo_w is not null)	then
			ds_procedimento_w := obter_select_concatenado_bv(' 	select	b.ds_procedimento
								from	procedimento b,
									pls_processo_procedimento a
								where	a.cd_procedimento	= b.cd_procedimento
								and	a.ie_origem_proced	= b.ie_origem_proced
								and	a.nr_sequencia = :nr_seq_processo',
								'nr_seq_processo='||nr_seq_processo_w,';');			
		end if;
		end;
	end loop;
	close c01;
end if;

--se a verificao da conta com o item atual trouxer resutado prenche como 'x' o campo cd_alegacao
if ( cd_alegacao_w is not null) then
	ds_cd_alegacao_w := '(X)';
end if;

--monto as freases de retorno
CASE	WHEN(cd_alegacao_p = 1)THEN
		ds_retorno_w := ds_cd_alegacao_w||'1.'||' Benefici�rio em car�ncia - Cl�usula do contrato: '||cd_clausula_w||
			' Termo final de car�ncia: '||ds_termo_fim_carencia_w;
	WHEN(cd_alegacao_p = 2)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'2.'||' Usu�rio do procedimento n�o � o benefici�rio de produto da operadora(hom�nimo)';
	WHEN(cd_alegacao_p = 3)	THEN
		
		ds_retorno_w := ds_cd_alegacao_w||'3.'||' Produto n�o cobre o procedimento - Cl�usula do contrato: '||cd_clausula_w||
			' - Procedimento: '||ds_procedimento_w;
	WHEN(cd_alegacao_p = 4)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'4.'||' Atendimento fora da abrang�ncia geogr�fica do produto - Cl�usula do contrato: '||
		CHR(13)||CHR(10)||cd_clausula_w||' - Cl�usula de Reembolso: '||cd_clausula_reembolso_w||
			' Cl�usula de atendimento em tr�nsito: '||cd_clausula_atend_trans_w;
	WHEN(cd_alegacao_p = 5)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'5.'||' Produto n�o cobre interna��o - Cl�usula do contrato: '||cd_clausula_w;
	WHEN(cd_alegacao_p = 6)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'6.'||' Quantidade de procedimento n�o coberta pelo produto - Cl�usula do contrato: '||
			cd_clausula_w||' Procedimento: '||ds_procedimento_w||' Quantidade n�o coberta: '||qt_cobertura_w;
	WHEN(cd_alegacao_p = 7)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'7.'||' Atendimento j� pago pela operadora';
	WHEN(cd_alegacao_p = 8)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'8.'||' Franquia ou coparticipa��o - Cl�usula do contrato: '||cd_clausula_w||
			' Valor: '||vl_deferido_w;
	WHEN(cd_alegacao_p = 16)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'16.'||' CPT - Cobertura Parcial Tempor�ria - Cl�usula do contrato: '||
			cd_clausula_w||' DLP:___________ ';
	WHEN(cd_alegacao_p = 22)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'22.'||' Repasse integral e individuaizado da despesa assistencial ao benefici�rio -
			Cl�usula do contrato: '||cd_clausula_w;
	WHEN(cd_alegacao_p = 34)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'34.'||' Acidente de trabalho - Cl�usula do contrato: '||cd_clausula_w;
	WHEN(cd_alegacao_p = 13)	THEN
		ds_retorno_w := ds_cd_alegacao_w||'13.'||' Outras alega��es de natureza administrativa - Especificar: '||ds_especificacao_w;
	WHEN(cd_alegacao_p = 14)THEN
		ds_retorno_w := ds_cd_alegacao_w||'14.'||' Alega��es de Natureza T�cnica - Especificar: '||ds_especificacao_w;
end case;

return	ds_retorno_w;

end pls_obter_ds_alegacao_relat;
/
