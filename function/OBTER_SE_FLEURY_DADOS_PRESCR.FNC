create or replace
function obter_se_fleury_dados_prescr(	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					ie_tipo_p		varchar2)
 		    	return varchar2 is

ie_retorno_w	varchar2(5) := 'N';
qt_itens_w	number(10);

begin

if	(ie_tipo_p = 'MAT') then
	
	select	count(*)
	into	qt_itens_w
	from 	fleury_prescr_proc_mat 
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_seq_prescr = nr_seq_prescr_p;
	
	if	(qt_itens_w > 0) then
		ie_retorno_w := 'S';
	end if;

elsif	(ie_tipo_p = 'PER') then

	select	nvl(max(ie_possui_pergunta_tecnica),'N')
	into	ie_retorno_w
	from	fleury_prescr_proced_valid
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_seq_prescr = nr_seq_prescr_p;
	
end if;

return	ie_retorno_w;

end obter_se_fleury_dados_prescr;
/