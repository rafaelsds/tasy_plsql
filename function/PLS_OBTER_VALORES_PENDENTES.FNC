create or replace
function pls_obter_valores_pendentes(nr_seq_pagador_p	pls_mensalidade.nr_sequencia%type,
									ie_liq_perda_p		varchar2,
									ie_tipo_valor_p		varchar2)
 		    	return number is

vl_retorno_w	number(15,2)	:= 0;

Cursor C01 is
	select	distinct a.nr_titulo, a.vl_saldo_titulo,
         	'A' ie_classif,
			0 vl_baixa_inad
	from    pls_notificacao_item	c,
			pls_notificacao_pagador d,
			titulo_receber		a,
			pls_mensalidade 	b
	where   a.nr_seq_mensalidade	= b.nr_sequencia
	and     b.nr_sequencia 		= c.nr_seq_mensalidade(+)
	and     c.nr_seq_notific_pagador 	= d.nr_sequencia(+)
	and     b.nr_seq_pagador    	= nr_seq_pagador_p
	and     trunc(dt_pagamento_previsto,'dd')   <= trunc(sysdate,'dd')
	and		(a.ie_situacao = '1' or (a.ie_situacao = '6'  and ie_liq_perda_p = 'S'))
	union all
	select	distinct a.nr_titulo, a.vl_saldo_titulo,
			'P' ie_classif,
			0 vl_baixa_inad
	from    pls_notificacao_item 	c,
			pls_notificacao_pagador	d,
			titulo_receber 		a,
			pls_mensalidade 	b
	where   a.nr_seq_mensalidade	= b.nr_sequencia
	and     b.nr_sequencia 		= c.nr_seq_mensalidade(+)
	and     c.nr_seq_notific_pagador 	= d.nr_sequencia(+)
	and     b.nr_seq_pagador     	= nr_seq_pagador_p
	and     trunc(dt_pagamento_previsto,'dd')   > trunc(sysdate,'dd')
	and		(a.ie_situacao = '1' or (a.ie_situacao = '6'  and ie_liq_perda_p = 'S'))
	union all
	select	distinct a.nr_titulo, a.vl_saldo_titulo,
			'BI' ie_classif,
			obter_valor_baixa_inad_tit(a.nr_titulo) vl_baixa_inad
	from    pls_notificacao_item 	c,
			pls_notificacao_pagador	d,
			titulo_receber 		a,
			pls_mensalidade 	b
	where   a.nr_seq_mensalidade	= b.nr_sequencia
	and     b.nr_sequencia 			= c.nr_seq_mensalidade(+)
	and     c.nr_seq_notific_pagador 	= d.nr_sequencia(+)
	and     b.nr_seq_pagador     	= nr_seq_pagador_p
	and     a.ie_situacao = '2'
	and		a.ie_liq_inadimplencia	= 'S'
	and		nvl(a.ie_negociado,'N')	= 'N';
	
begin

for r_c01_w in C01() loop
	begin
	if	(ie_tipo_valor_p = 'A') and
		(r_c01_w.ie_classif = 'A') then
		vl_retorno_w	:= vl_retorno_w + r_c01_w.vl_saldo_titulo;
	elsif (ie_tipo_valor_p = 'P') and
		(r_c01_w.ie_classif = 'P') then
		vl_retorno_w	:= vl_retorno_w + r_c01_w.vl_saldo_titulo;
	elsif (ie_tipo_valor_p = 'I') and
		  (r_c01_w.ie_classif = 'BI') then
		vl_retorno_w	:= vl_retorno_w + r_c01_w.VL_BAIXA_INAD;
	end if;
	end;
end loop;

return	vl_retorno_w;

end pls_obter_valores_pendentes;
/