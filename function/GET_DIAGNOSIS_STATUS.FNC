create or replace function GET_DIAGNOSIS_STATUS (	cd_diagnostico_p	varchar2,
                                                nr_atendimento_p number)
				return varchar2 is

ie_status_w		varchar2(5)	:= '';

begin

if	(cd_diagnostico_p is not null and nr_atendimento_p is not null ) then

select max(ie_status) 
into ie_status_w
from	lista_problema_pac
where cd_doenca = cd_diagnostico_p
and nr_atendimento = nr_atendimento_p;

end if;

return ie_status_w;

end GET_DIAGNOSIS_STATUS;
/
