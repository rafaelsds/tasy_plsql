create or replace
function Obter_Mascara_Convertida(ds_mascara_p	varchar2,
								nm_atributo_p	varchar2 default null)
 		    	return varchar2 is
				
qt_decimais_w	number(10);
ds_decimais_w	varchar2(255);

begin


if 	(upper(ds_mascara_p) = 'DD/MM/YYYY HH:MM:SS') then
	return 'date(timestamp)';
elsif (upper(ds_mascara_p) = 'DD/MM/YYYY') then
	return 'date(shortDate)';
elsif (upper(ds_mascara_p) = 'DD/MM/YYYY HH:MM') then
	return 'date(short)';
elsif (upper(ds_mascara_p) = 'MM/YYYY') then
	return 'date(shortMonth)';
elsif (upper(ds_mascara_p) = 'HH:MM:SS') then
	return 'date(mediumTime)';
elsif (upper(ds_mascara_p) = 'HH:MM') then
	return  'date(shortTime)';
elsif 	(upper(ds_mascara_p) like '%#%') and
		(upper(ds_mascara_p) like '%0.00%') and
		(upper(nm_atributo_p) like 'VL_%') then
	return  'currency(minFrac:2,maxFrac:2)';
elsif 	(upper(ds_mascara_p) like '%#%') and
		(upper(ds_mascara_p) like '%0.0%') and
		(upper(nm_atributo_p) not like 'VL_%' or nm_atributo_p is null) then
	ds_decimais_w	:=	substr(ds_mascara_p,instr(ds_mascara_p,'0.0') + 2,length(ds_mascara_p));
	qt_decimais_w	:= length(ds_decimais_w);
		
	return  'number(maxFrac:' || qt_decimais_w || ')';
end if;

return	null;

end Obter_Mascara_Convertida;
/
