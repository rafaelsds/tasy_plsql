create or replace
function obter_aliq_trib(cd_tributo_p		number,
			 nr_sequencia_p         number)	
					return varchar2 is

aliquota_w			number;


begin

select max(a.PR_ALIQUOTA)
into aliquota_w
from TRIBUTO_CONTA_PAGAR a
where a.CD_TRIBUTO = cd_tributo_p
and a.NR_SEQUENCIA = nr_sequencia_p;

return aliquota_w;

end obter_aliq_trib;
/