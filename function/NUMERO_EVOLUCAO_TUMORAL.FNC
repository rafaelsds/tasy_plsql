create or replace
function numero_evolucao_tumoral( 	ds_campo_p    	varchar2)
				return 		number is

ds_campo_w    	varchar2(255);
cd_campo_w    	number(38,5) := 0;
ds_campo_ww	varchar2(255);
i     		integer;

begin

ds_campo_ww		:= ds_campo_p;
ds_campo_w		:= '';

if	(ds_campo_p is null) or (ds_campo_p = '') then
	cd_campo_w	:= null;
else
	begin
	select	to_number(ds_campo_ww)
	into	cd_campo_w
	from	dual;
	exception
		when INVALID_NUMBER then
		begin
		for i in 1..length(ds_campo_p) loop
	 		begin
		 	if 	(substr(ds_campo_p, i, 1) in
				('1','2','3','4','5','6','7','8','9','0',',','.')) then
	  			ds_campo_w := ds_campo_w || substr(ds_campo_p, i, 1);
				
				
			end if;
			end;
		end loop;
		if	(ds_campo_w is null) or
			(ds_campo_w = '') then
			cd_campo_w		:= null;
		else
			ds_campo_w	:= replace(ds_campo_w,'.',',');
			cd_campo_w := to_number(substr(ds_campo_w,1,38));
		end if;
		end;
	end;
end if;

return cd_campo_w;

end numero_evolucao_tumoral;
/