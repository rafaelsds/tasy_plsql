create or replace
function OBTER_CODIGO_DARF
		(cd_tributo_p		number,
		cd_estabelecimento_p	number,
		cd_cgc_p		varchar2,
		cd_pessoa_fisica_p	varchar2) return varchar2 is


cd_beneficiario_w		varchar2(20)	:= null;
pr_aliquota_w			number(10,4)	:= null;
vl_minimo_base_w		number(15,2)	:= 0;
vl_minimo_tributo_w		number(15,2)	:= 0;
VL_TETO_BASE_w			number(15,2)	:= 0;
vl_desc_dependente_w		number(15,2)	:= 0;
cd_cond_pagto_w			number(10,0)	:= null;
cd_conta_financ_w		number(10,0)	:= null;
nr_seq_trans_reg_w		number(10,0)	:= null;
nr_seq_trans_baixa_w		number(10,0)	:= null;
ie_acumulativo_w		varchar2(1);
cd_darf_w			varchar2(20)	:= null;
dt_referencia_w			date;
cd_variacao_w			varchar2(2)	:= null;
ie_periodicidade_w		varchar2(1)	:= null;
ds_irrelevante_w			varchar2(4000);

begin

obter_dados_trib_tit_pagar
		(cd_tributo_p,
		cd_estabelecimento_p,
		cd_cgc_p,
		cd_pessoa_fisica_p,
		cd_beneficiario_w,
		pr_aliquota_w,
		cd_cond_pagto_w,
		cd_conta_financ_w,
		nr_seq_trans_reg_w,
		nr_seq_trans_baixa_w,
		vl_minimo_base_w,
		vl_minimo_tributo_w,
		ie_acumulativo_w,
		vl_teto_base_w,
		vl_desc_dependente_w,
		cd_darf_w,
		dt_referencia_w,
		cd_variacao_w,
		ie_periodicidade_w,
		null,
		null,
		null,
		null,
		null,
		null,
		ds_irrelevante_w,
		null,
		0,
		ds_irrelevante_w,
		ds_irrelevante_w,
		null,
		'N',
		null,
		null,
		null,
		null);

return		cd_darf_w;

end OBTER_CODIGO_DARF;
/