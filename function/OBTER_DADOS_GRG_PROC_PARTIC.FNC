Create or Replace
FUNCTION OBTER_DADOS_GRG_PROC_PARTIC(	nr_seq_proc_p	integer,
					nr_seq_partic_p	integer,
					ie_opcao_p	varchar2)
					RETURN VarChar2 IS

/*	ie_opcao_p

'CM'	C�digo do m�dico
'NM'	Nome do m�dico

*/

ds_retorno_w		varchar2(255);
cd_pessoa_fisica_w	varchar2(10);
nm_pessoa_fisica_w	varchar2(255);

BEGIN

if	(nr_seq_partic_p	is null) then

	select	max(a.cd_medico_executor) cd_pessoa_fisica,
		substr(obter_nome_medico(max(a.cd_medico_executor),'N'),1,255) nm_pessoa_fisica
	into	cd_pessoa_fisica_w,
		nm_pessoa_fisica_w
	from	procedimento_paciente a
	where	a.nr_sequencia	= nr_seq_proc_p;

else

	select	max(a.cd_pessoa_fisica) cd_pessoa_fisica,
		substr(obter_nome_pf_pj(max(a.cd_pessoa_fisica),null),1,255) nm_pessoa_fisica
	into	cd_pessoa_fisica_w,
		nm_pessoa_fisica_w
	from	procedimento_participante a
	where	a.nr_seq_partic	= nr_seq_partic_p
	and	a.nr_sequencia	= nr_seq_proc_p;

end if;

if	(ie_opcao_p	= 'CM') then
	ds_retorno_w	:= cd_pessoa_fisica_w;
elsif	(ie_opcao_p	= 'NM') then
	ds_retorno_w	:= nm_pessoa_fisica_w;
end if;

RETURN ds_retorno_w;

END OBTER_DADOS_GRG_PROC_PARTIC;
/