create or replace
function Obter_se_solucao_vencida(dt_inicio_prescr_p	Date,
				 nr_horas_validade_p	number default 0)
 		    	return Varchar2 is

ie_vencida_w		varchar2(1) := 'N';
nr_horas_duracao_w	number(10);

begin

if	(nr_horas_validade_p > 0) then

	nr_horas_duracao_w	:= Obter_Hora_Entre_datas(sysdate, dt_inicio_prescr_p);
	
	if	((dt_inicio_prescr_p + (nr_horas_validade_p/24)) < sysdate) then
		ie_vencida_w := 'S';
	end if;
	
end if;

return	ie_vencida_w;

end Obter_se_solucao_vencida;
/
