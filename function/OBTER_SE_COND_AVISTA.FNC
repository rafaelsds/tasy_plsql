CREATE OR REPLACE
FUNCTION obter_se_cond_avista	(nr_sequencia_p	number,
				cd_estabelecimento_p	number) 
				return varchar2 is 

ds_retorno_w	varchar2(80) := '';

begin


select   decode(count(*),0,'S',1,'S','N')
into	 ds_retorno_w
from     nota_fiscal_venc
where    nr_sequencia = nr_sequencia_p;

return ds_retorno_w;

end obter_se_cond_avista;
/
