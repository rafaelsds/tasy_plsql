create or replace
function gpi_obter_conta(	cd_empresa_p	number,
			dt_vigencia_p	date,
			cd_material_p	number)
			return number is

cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
dt_vigencia_w		date;
nr_seq_conta_gpi_w	number(10);
nr_seq_conta_gpi_ww	number(10);

cursor c01 is
select	a.nr_seq_conta_gpi
from	gpi_regra_plano a
where	a.cd_empresa					= cd_empresa_p
and	nvl(a.cd_material,cd_material_p) 			= cd_material_p
and	nvl(a.cd_grupo_material,cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material,cd_classe_material_w)		= cd_classe_material_w
and	(nvl(dt_inicio_vigencia, dt_vigencia_w) <= dt_vigencia_w and nvl(dt_fim_vigencia, dt_vigencia_w) >= dt_vigencia_w)
order by	a.dt_inicio_vigencia,
		nvl(a.cd_material,0),
		nvl(a.cd_classe_material,0),
		nvl(a.cd_subgrupo_material,0),
		nvl(a.cd_grupo_material,0);
BEGIN

dt_vigencia_w	:= trunc(dt_vigencia_p,'dd');

select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v a
where	a.cd_material	= cd_material_p;

open c01;
loop
fetch c01 into
	nr_seq_conta_gpi_w;
exit when c01%notfound;
	nr_seq_conta_gpi_ww	:= nr_seq_conta_gpi_w;
end loop;
close c01;

return nr_seq_conta_gpi_ww;

END gpi_obter_conta;
/