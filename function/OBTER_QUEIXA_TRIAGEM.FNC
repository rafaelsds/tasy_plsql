create or replace
function obter_queixa_triagem(	nr_atendimento_p	in atendimento_paciente.nr_atendimento%type,
										nr_seq_queixa_p		in queixa_paciente.nr_sequencia%type) 
										return varchar2 is

ds_retorno_w	varchar2(255);
				
begin

if (nvl(pkg_i18n.get_user_locale, 'pt_BR') in ('de_DE', 'de_AT')) then
	select	max(tpa.ds_queixa_princ)
	into	ds_retorno_w
	from	triagem_pronto_atend tpa
	where	tpa.nr_sequencia = (	select	max(nr_sequencia)
									from	triagem_pronto_atend tp
									where	tpa.nr_atendimento = tp.nr_atendimento
									and		tp.nr_atendimento = nr_atendimento_p);
else
	select	substr(Obter_Queixas_Atendimento(nr_atendimento_p, nr_seq_queixa_p),1,255)
	into	ds_retorno_w
	from 	dual;
end if;

return	ds_retorno_w;

end obter_queixa_triagem;
/
