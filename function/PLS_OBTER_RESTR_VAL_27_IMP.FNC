create or replace
function pls_obter_restr_val_27_imp (	dados_segurado_p	pls_ocor_imp_pck.dados_benef,
					nr_id_transacao_p	pls_oc_cta_selecao_imp.nr_id_transacao%type,
					dados_limitacao_p	pls_ocor_imp_pck.dados_limitacao,
					ie_proc_mat_p		varchar2,
					dados_proc_limitacao_p	pls_ocor_imp_pck.t_pls_limitacao_proc_row,
					dados_mat_limitacao_p	pls_ocor_imp_pck.t_pls_limitacao_mat_row,
					ie_restricao_p		varchar2,
					dado_bind_p		in out sql_pck.t_dado_bind) 
				return pls_ocor_imp_pck.dados_restricao_select is
					
dados_retorno_w		pls_ocor_imp_pck.dados_restricao_select;
					
begin

dados_retorno_w.qt_restricao := 0;

if	(ie_restricao_p = 'IMP') then
	-- sempre deve se ter a informa��o do segurado para chamar esta function, pois se n�o ir� pegar todas as contas, o que ocasionar� transtornos e lentid�o e o resultado do select n�o ser� 
	-- v�lido. Portanto deve se olhar se existe informa��o do segurado fora daqui. 
	if	(dados_limitacao_p.ie_tipo_incidencia is not null) then
		
		-- Verificar se a limita��o se aplica por benefici�rio ou pelos seus dependentes legais.
		-- Se for apenas por benefici�rio devem ser consideradas as contas daquele benefici�rio.
		if	(dados_limitacao_p.ie_tipo_incidencia = 'B') then
			
			dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
								'and	conta.nr_seq_segurado_conv = :nr_seq_segurado_imp ';
			sql_pck.bind_variable(':nr_seq_segurado_imp' , dados_segurado_p.nr_sequencia, dado_bind_p);
			
		-- Se for para os dependentes tabm�m devem ser obidos os dependentes legais daquele benefici�rio e considerar as contas dele tamb�m.
		elsif	(dados_limitacao_p.ie_tipo_incidencia = 'T') then
		
			-- Utiliza a table function pls_util_cta_pck.obter_dependentes_benef para obter os dependentes legais do benefici�rio.
			dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w ||  
							'and	conta.nr_seq_segurado_conv in (	select	depend.nr_seq_segurado ' || pls_util_pck.enter_w || 
							'					from	table(pls_util_cta_pck.obter_dependentes_benef(:nr_seq_segurado_imp, ''N'', ''S'')) depend )';
			-- Se n�o for um titular ent�o informa o titular para que sejam buscados seus dependentes. Caso seja um titular ent�o informa a pr�pria sequencia para buscar seus dependetes.
			if	(dados_segurado_p.nr_seq_titular is not null) then
				
				sql_pck.bind_variable(':nr_seq_segurado_imp' , dados_segurado_p.nr_seq_titular, dado_bind_p);
			else
				sql_pck.bind_variable(':nr_seq_segurado_imp' , dados_segurado_p.nr_sequencia, dado_bind_p);
			end if;
		end if;
		
		-- Verificar o per�odo de consist�ncia da  regra de limita��o cadastrada.
		if	(dados_limitacao_p.dt_inicio_periodo is not null) and
			(dados_limitacao_p.dt_fim_periodo is not null) then
				
			-- Verificar se � para procedimento ou material
			-- Procedimento
			if	(ie_proc_mat_p = 'P') then
			
				dados_retorno_w.ds_restricao_proc :=	dados_retorno_w.ds_restricao_proc || pls_util_pck.enter_w ||
									'and	proc.dt_execucao_conv between :dt_inicio_periodo_imp and :dt_fim_periodo_imp ';
			-- Material
			elsif	(ie_proc_mat_p = 'M') then
				
				dados_retorno_w.ds_restricao_mat :=	dados_retorno_w.ds_restricao_mat || pls_util_pck.enter_w ||
									'and	mat.dt_execucao_conv between :dt_inicio_periodo_imp and :dt_fim_periodo_imp ';
			end if;
			
			sql_pck.bind_variable(':dt_inicio_periodo_imp' , dados_limitacao_p.dt_inicio_periodo, dado_bind_p);
			sql_pck.bind_variable(':dt_fim_periodo_imp' , dados_limitacao_p.dt_fim_periodo, dado_bind_p);
		end if;
		
		-- Daqui para baixo ser�o verificados os campos referentes a procedimentos e materiais da limita��o. Manter agrupado a verifica��o por campos e tratar sempre procedimento e material junto 
		-- para que ao alterar a informa��o em um dos campos seja mais f�cil alterar para os dois casos se necess�rio.
		
		---------------------------------------------------------- IE_TIPO_GUIA ---------------------------------------------------------------------------
		-- Verificar se � para procedimento ou material.
		-- Procedimento
		if	(ie_proc_mat_p = 'P') then
		
			-- Se a regra da limita��o informar para restringir o tipo de guia ent�o s� busca contas do tipo de guia informado.
			if	(dados_proc_limitacao_p.ie_tipo_guia is not null) then 

				dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w ||
									'and	prot.ie_tipo_guia = :ie_tipo_guia_imp'; 
									
				sql_pck.bind_variable(':ie_tipo_guia_imp' , dados_proc_limitacao_p.ie_tipo_guia, dado_bind_p);
			end if;
		-- Material
		elsif	(ie_proc_mat_p = 'M') then
			
			-- Se a regra da limita��o informar para restringir o tipo de guia ent�o s� busca contas do tipo de guia informado.
			if	(dados_mat_limitacao_p.ie_tipo_guia is not null) then 
				
				dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
									'and	prot.ie_tipo_guia = :ie_tipo_guia_imp'; 
									
				sql_pck.bind_variable(':ie_tipo_guia_imp' , dados_mat_limitacao_p.ie_tipo_guia, dado_bind_p);
			end if;
		end if;
		
		------------------------------------------------------------------------ CD_DOENCA_CID ------------------------------------------------------------------
		-- Verificar se � para procedimento ou material.
		-- Procedimento
		if	(ie_proc_mat_p = 'P') then
			
			-- Se a regra da limita��o informar para restringir por cid ent�o deve ser verificado apenas as contas que contenham o cid informado no diagn�stico.
			if	(dados_proc_limitacao_p.cd_doenca_cid is not null) then

				dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
									'and	exists	(	select	1 						' || pls_util_pck.enter_w || 
									'			from	pls_diagnostico_conta_imp diag 			' || pls_util_pck.enter_w || 
									'			where	diag.nr_seq_conta	= conta.nr_sequencia	' || pls_util_pck.enter_w || 
									'			and	diag.cd_doenca_conv	= :cd_cid_imp 		' || pls_util_pck.enter_w ||  
									'	) ';
								
				sql_pck.bind_variable(':cd_cid_imp' , dados_proc_limitacao_p.cd_doenca_cid, dado_bind_p);
			end if;
		-- Material
		elsif	(ie_proc_mat_p = 'M') then
			
			-- Se a regra da limita��o informar para restringir por cid ent�o deve ser verificado apenas as contas que contenham o cid informado no diagn�stico.
			if	(dados_mat_limitacao_p.cd_doenca_cid is not null) then

				dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
									'and	exists	(	select	1 						' || pls_util_pck.enter_w ||  
									'			from	pls_diagnostico_conta_imp diag 			' || pls_util_pck.enter_w || 
									'			where	diag.nr_seq_conta	= conta.nr_sequencia 	' || pls_util_pck.enter_w ||  
									'			and	diag.cd_doenca_conv	= :cd_cid_imp 		' || pls_util_pck.enter_w ||  
									'	) ';
									
				sql_pck.bind_variable(':cd_cid_imp' , dados_mat_limitacao_p.cd_doenca_cid, dado_bind_p);
			end if;
		end if;
		
		------------------------------------------------------------- CD_PROCEDIMENTO / NR_SEQ_MATERIAL -----------------------------------------------------------
		-- Verificar se � para procedimento ou material.
		-- Procedimento
		if	(ie_proc_mat_p = 'P') then
			
			-- Se existir o procedimento informado na estrutura ent�o busca por procedimento e origem.
			if	(dados_proc_limitacao_p.cd_procedimento is not null) then
				
				dados_retorno_w.ds_restricao_proc :=	dados_retorno_w.ds_restricao_proc || pls_util_pck.enter_w || 
									'and	proc.cd_procedimento_conv = :ie_origem_proced_imp ' || pls_util_pck.enter_w || 
									'and	proc.ie_origem_proced_conv = :cd_procedimento_imp ';
			
				sql_pck.bind_variable(':cd_procedimento_imp' , dados_proc_limitacao_p.cd_procedimento, dado_bind_p);
				sql_pck.bind_variable(':ie_origem_proced_imp' , dados_proc_limitacao_p.ie_origem_proced, dado_bind_p);
			end if;
		-- Material
		elsif	(ie_proc_mat_p = 'M') then
			
			-- Se existir o material informado na estrutura ent�o busca por material
			if	(dados_mat_limitacao_p.nr_seq_material is not null) then
				
				dados_retorno_w.ds_restricao_mat :=	dados_retorno_w.ds_restricao_mat || pls_util_pck.enter_w || 
									'and	mat.nr_seq_material_conv = :nr_seq_material_imp ';
									
				sql_pck.bind_variable(':nr_seq_material_imp' , dados_mat_limitacao_p.nr_seq_material, dado_bind_p);
			end if;
		end if;
	end if;
elsif	(ie_restricao_p = 'N') then
	-- sempre deve se ter a informa��o do segurado para chamar esta function, pois se n�o ir� pegar todas as contas, o que ocasionar� transtornos e lentid�o e o resultado do select n�o ser� 
	-- v�lido. Portanto deve se olhar se existe informa��o do segurado fora daqui. 
	if	(dados_limitacao_p.ie_tipo_incidencia is not null) then
		
		-- Verificar se a limita��o se aplica por benefici�rio ou pelos seus dependentes legais.
		-- Se for apenas por benefici�rio devem ser consideradas as contas daquele benefici�rio.
		if	(dados_limitacao_p.ie_tipo_incidencia = 'B') then
			
			dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
								'and	conta.nr_seq_segurado = :nr_seq_segurado ';
			sql_pck.bind_variable(':nr_seq_segurado' , dados_segurado_p.nr_sequencia, dado_bind_p);
			
		-- Se for para os dependentes tabm�m devem ser obidos os dependentes legais daquele benefici�rio e considerar as contas dele tamb�m.
		elsif	(dados_limitacao_p.ie_tipo_incidencia = 'T') then
		
			-- Utiliza a table function pls_util_cta_pck.obter_dependentes_benef para obter os dependentes legais do benefici�rio.
			dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
							'and	conta.nr_seq_segurado in (	select	depend.nr_seq_segurado ' || pls_util_pck.enter_w || 
							'					from	table(pls_util_cta_pck.obter_dependentes_benef(:nr_seq_segurado, ''N'', ''S'')) depend )';
			-- Se n�o for um titular ent�o informa o titular para que sejam buscados seus dependentes. Caso seja um titular ent�o informa a pr�pria sequencia para buscar seus dependetes.
			if	(dados_segurado_p.nr_seq_titular is not null) then
				
				sql_pck.bind_variable(':nr_seq_segurado' , dados_segurado_p.nr_seq_titular, dado_bind_p);
			else
				sql_pck.bind_variable(':nr_seq_segurado' , dados_segurado_p.nr_sequencia, dado_bind_p);
			end if;
		end if;
			
		-- Verificar o per�odo de consist�ncia da  regra de limita��o cadastrada.
		if	(dados_limitacao_p.dt_inicio_periodo is not null) and
			(dados_limitacao_p.dt_fim_periodo is not null) then
				
			-- Verificar se � para procedimento ou material
			-- Procedimento
			if	(ie_proc_mat_p = 'P') then
			
				dados_retorno_w.ds_restricao_proc :=	dados_retorno_w.ds_restricao_proc || pls_util_pck.enter_w ||
									'and	proc.dt_procedimento between :dt_inicio_periodo and :dt_fim_periodo ';
			-- Material
			elsif	(ie_proc_mat_p = 'M') then
				
				dados_retorno_w.ds_restricao_mat :=	dados_retorno_w.ds_restricao_mat || pls_util_pck.enter_w ||
									'and	mat.dt_atendimento between :dt_inicio_periodo and :dt_fim_periodo ';
			end if;
			
			sql_pck.bind_variable(':dt_inicio_periodo' , dados_limitacao_p.dt_inicio_periodo, dado_bind_p);
			sql_pck.bind_variable(':dt_fim_periodo' , dados_limitacao_p.dt_fim_periodo, dado_bind_p);
		end if;
		
		-- Daqui para baixo ser�o verificados os campos referentes a procedimentos e materiais da limita��o. Manter agrupado a verifica��o por campos e tratar sempre procedimento e material junto 
		-- para que ao alterar a informa��o em um dos campos seja mais f�cil alterar para os dois casos se necess�rio.
		
		---------------------------------------------------------- IE_TIPO_GUIA ---------------------------------------------------------------------------
		-- Verificar se � para procedimento ou material.
		-- Procedimento
		if	(ie_proc_mat_p = 'P') then
		
			-- Se a regra da limita��o informar para restringir o tipo de guia ent�o s� busca contas do tipo de guia informado.
			if	(dados_proc_limitacao_p.ie_tipo_guia is not null) then 

				dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
									'and	conta.ie_tipo_guia = :ie_tipo_guia'; 
									
				sql_pck.bind_variable(':ie_tipo_guia' , dados_proc_limitacao_p.ie_tipo_guia, dado_bind_p);
			end if;
		-- Material
		elsif	(ie_proc_mat_p = 'M') then
			
			-- Se a regra da limita��o informar para restringir o tipo de guia ent�o s� busca contas do tipo de guia informado.
			if	(dados_mat_limitacao_p.ie_tipo_guia is not null) then 
				
				dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
									'and	conta.ie_tipo_guia = :ie_tipo_guia'; 
									
				sql_pck.bind_variable(':ie_tipo_guia' , dados_mat_limitacao_p.ie_tipo_guia, dado_bind_p);
			end if;
		end if;
		
		------------------------------------------------------------------------ CD_DOENCA_CID ------------------------------------------------------------------
		-- Verificar se � para procedimento ou material.
		-- Procedimento
		if	(ie_proc_mat_p = 'P') then
			
			-- Se a regra da limita��o informar para restringir por cid ent�o deve ser verificado apenas as contas que contenham o cid informado no diagn�stico.
			if	(dados_proc_limitacao_p.cd_doenca_cid is not null) then

				dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
									'and	exists	(	select	1 ' || pls_util_pck.enter_w || 
									'			from	pls_diagnostico_conta	diag ' || pls_util_pck.enter_w || 
									'			where	diag.nr_seq_conta	= conta.nr_sequencia ' || pls_util_pck.enter_w || 
									'			and	diag.cd_doenca		= :cd_cid ' || pls_util_pck.enter_w || 
									'	) ';
								
				sql_pck.bind_variable(':cd_cid' , dados_proc_limitacao_p.cd_doenca_cid, dado_bind_p);
			end if;
		-- Material
		elsif	(ie_proc_mat_p = 'M') then
			
			-- Se a regra da limita��o informar para restringir por cid ent�o deve ser verificado apenas as contas que contenham o cid informado no diagn�stico.
			if	(dados_mat_limitacao_p.cd_doenca_cid is not null) then

				dados_retorno_w.ds_restricao_conta := 	dados_retorno_w.ds_restricao_conta || pls_util_pck.enter_w || 
									'and	exists	(	select	1 ' || pls_util_pck.enter_w || 
									'			from	pls_diagnostico_conta	diag ' || pls_util_pck.enter_w || 
									'			where	diag.nr_seq_conta	= conta.nr_sequencia ' || pls_util_pck.enter_w || 
									'			and	diag.cd_doenca		= :cd_cid ' || pls_util_pck.enter_w || 
									'	) ';
									
				sql_pck.bind_variable(':cd_cid' , dados_mat_limitacao_p.cd_doenca_cid, dado_bind_p);
			end if;
		end if;
		
		------------------------------------------------------------- CD_PROCEDIMENTO / NR_SEQ_MATERIAL -----------------------------------------------------------
		-- Verificar se � para procedimento ou material.
		-- Procedimento
		if	(ie_proc_mat_p = 'P') then
			
			-- Se existir o procedimento informado na estrutura ent�o busca por procedimento e origem.
			if	(dados_proc_limitacao_p.cd_procedimento is not null) then
				
				dados_retorno_w.ds_restricao_proc :=	dados_retorno_w.ds_restricao_proc || pls_util_pck.enter_w || 
									'and	proc.ie_origem_proced = :ie_origem_proced ' || pls_util_pck.enter_w || 
									'and	proc.cd_procedimento = :cd_procedimento ';
			
				sql_pck.bind_variable(':cd_procedimento' , dados_proc_limitacao_p.cd_procedimento, dado_bind_p);
				sql_pck.bind_variable(':ie_origem_proced' , dados_proc_limitacao_p.ie_origem_proced, dado_bind_p);
			end if;
		-- Material
		elsif	(ie_proc_mat_p = 'M') then
			
			-- Se existir o material informado na estrutura ent�o busca por material
			if	(dados_mat_limitacao_p.nr_seq_material is not null) then
				
				dados_retorno_w.ds_restricao_mat :=	dados_retorno_w.ds_restricao_mat || pls_util_pck.enter_w || 
									'and	mat.nr_seq_material = :nr_seq_material ';
									
				sql_pck.bind_variable(':nr_seq_material' , dados_mat_limitacao_p.nr_seq_material, dado_bind_p);
			end if;
		end if;
	end if;
end if;

if	(not(	(dados_retorno_w.ds_restricao_conta is null) and
		(dados_retorno_w.ds_restricao_benef is null) and
		(dados_retorno_w.ds_restricao_proc is null) and
		(dados_retorno_w.ds_restricao_mat is null))) then	
	dados_retorno_w.qt_restricao := 1;
end if;

return dados_retorno_w;

end pls_obter_restr_val_27_imp;
/