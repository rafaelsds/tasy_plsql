create or replace
function tx_obter_desc_comorbidade(nr_seq_comorbidade_p		number)
 		    	return varchar2 is
ds_comorbidade_w	varchar2(80);
begin
if	(nr_seq_comorbidade_p is not null) then
	select	max(ds_comorbidade)
	into	ds_comorbidade_w
	from	tx_comorbidade
	where	nr_sequencia = nr_seq_comorbidade_p;
end if;

return	ds_comorbidade_w;

end tx_obter_desc_comorbidade;
/