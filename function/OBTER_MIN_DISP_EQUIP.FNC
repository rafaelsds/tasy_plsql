create or replace
function obter_min_disp_equip(
				nr_seq_equip_p	number,
				dt_referencia_p	date,
				ie_opcao_p	varchar2)
				return number is

qt_min_disp_w			number(15,2);
qt_min_parado_w			number(15,2);
qt_min_carga_w			number(15,2);
qt_retorno_w			number(15,2);

BEGIN

qt_min_disp_w	:= 0;
qt_retorno_w	:= 0;

select	nvl(max(qt_carga_horaria),24) * 60
into	qt_min_carga_w
from	man_equipamento
where	nr_sequencia = nr_seq_equip_p;

if	(trunc(sysdate,'month') = trunc(dt_referencia_p,'month')) then
	select	(campo_numerico(to_char(sysdate,'dd')) * qt_min_carga_w)	
	into	qt_min_disp_w
	from	dual;
else	
	select	(campo_numerico(to_char(last_day(dt_referencia_p),'dd')) * qt_min_carga_w)	
	into	qt_min_disp_w
	from	dual;
end if;

select	nvl(sum((dt_fim_real - dt_ordem_servico) * qt_min_carga_w),0)
into	qt_min_parado_w
from	man_ordem_servico
where	nr_seq_equipamento = nr_seq_equip_p
and	ie_parado = 'S'
and	dt_inicio_real is not null
and	dt_fim_real is not null
and	dt_fim_real > dt_inicio_real
and	dt_ordem_servico between trunc(dt_referencia_p,'month') and last_day(trunc(dt_referencia_p,'month')) + 86399/86400;
  
if	(ie_opcao_p = 'T') then
	qt_retorno_w	:= qt_min_disp_w;
elsif	(ie_opcao_p = 'P') then
	qt_retorno_w	:= qt_min_parado_w;
elsif	(ie_opcao_p = 'D') then
	qt_retorno_w	:= (qt_min_disp_w - qt_min_parado_w);
end if;

return qt_retorno_w;

end obter_min_disp_equip;
/