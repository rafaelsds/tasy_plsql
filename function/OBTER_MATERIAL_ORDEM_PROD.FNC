create or replace FUNCTION Obter_material_ordem_prod(nr_seq_ordem_p	NUMBER,
									ie_opcao_p	VARCHAR2,
                                    nr_sequencia_p can_ordem_item_prescr.nr_sequencia%type default null)
return VARCHAR2 is

ds_retorno_w				VARCHAR2(255);
ds_material_w				prescr_solucao.ds_solucao%type;
ds_unid_med_w				VARCHAR2(30);
qt_dose_w					VARCHAR2(20);
nr_sequencia_w				NUMBER(10);
cd_material_w				NUMBER(6);
ie_exibe_sem_apresent_w 	VARCHAR2(1);
ie_exibe_titulo_w			VARCHAR2(1);
nr_sequencia_solucao_w		NUMBER(6);
ie_considerar_medic_quimio 	VARCHAR2(1) := 'N';
cd_estabeleciumento_w		NUMBER(10);
cd_perfil_w					NUMBER(10);
nm_usuario_w				VARCHAR2(15);
nr_seq_idioma_w				NUMBER(3);
nr_seq_prescricao_w			can_ordem_item_prescr.nr_seq_prescricao%type;

nr_seq_atendimento_w	paciente_atend_medic.nr_seq_atendimento%type;
nr_seq_material_w		paciente_atend_medic.nr_seq_material%type;
/* ie_opcao_p C - Codigo material, M - Material, D - dose U - unidade medida, SP - Sequencia da prescricao */

BEGIN
cd_estabeleciumento_w := 	wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w			  := 	obter_perfil_ativo;
nm_usuario_w		  := 	wheb_usuario_pck.get_nm_usuario;
ie_exibe_titulo_w	  :=    nvl(quimio_parametros_pck.get_param_192,'');
nr_seq_idioma_w		  :=	nvl(philips_param_pck.get_nr_seq_idioma,1);


if 	((ie_exibe_titulo_w = '') or (ie_exibe_titulo_w is null)) then
	Obter_Param_Usuario(3130,192,cd_perfil_w,nm_usuario_w,cd_estabeleciumento_w,ie_exibe_titulo_w);
end if;

select	nvl(nr_sequencia_p, min(nr_sequencia))
into	nr_sequencia_w
from	can_ordem_item_prescr
where	nr_seq_ordem	= nr_seq_ordem_p
and		nr_sequencia_diluente is null;

-- obter a sequencia da prescricao
select	max(nr_seq_prescricao)
into	nr_seq_prescricao_w
from	can_ordem_item_prescr
where 	nr_sequencia = nr_sequencia_w;

if 	(ie_exibe_titulo_w = 'S') then

	select  max(ds_solucao),
		max(b.cd_material),
		max(obter_unid_med_usua('ml')),
		max(qt_solucao_total)
	into	ds_material_w,
		cd_material_w,
		ds_unid_med_w,
		qt_dose_w
	from   	prescr_solucao f,
		can_ordem_prod d,
		paciente_atend_medic b,
		prescr_material c,
		can_ordem_item_prescr x
	where  	c.nr_seq_atendimento 	= b.nr_seq_atendimento
	and    	c.nr_seq_material		= b.nr_seq_material
	and		c.nr_seq_ordem_prod  	= d.nr_sequencia
	and 	x.cd_material 			= c.cd_material
	and		d.nr_sequencia			= x.nr_seq_ordem
	and 	f.nr_prescricao			= c.nr_prescricao
	and 	c.nr_sequencia_solucao  = f.nr_seq_solucao
	and		x.nr_sequencia  		= nr_sequencia_w;
end if;


if 	(ds_material_w is null) and (cd_material_w is null) and
	(ds_unid_med_w is null) and (qt_dose_w is null) then

	ie_exibe_sem_apresent_w 	:= nvl(quimio_parametros_pck.get_param_180,'');

	if ((ie_exibe_sem_apresent_w = '') or (ie_exibe_sem_apresent_w is null))  then
		Obter_Param_Usuario(3130,180,cd_perfil_w,nm_usuario_w,cd_estabeleciumento_w,ie_exibe_sem_apresent_w);
	end if;

	ie_considerar_medic_quimio 	:= nvl(quimio_parametros_pck.get_param_209,'');

	if ((ie_considerar_medic_quimio = '') or (ie_considerar_medic_quimio is null)) then
		Obter_Param_Usuario(3130,209,cd_perfil_w,nm_usuario_w,cd_estabeleciumento_w,ie_considerar_medic_quimio);
	end if;

	if 	(ie_exibe_sem_apresent_w = 'S') then
		select 	--max(substr(obter_desc_material(b.cd_material),1,60)),
				--max(b.cd_material),
				--max(b.cd_unid_med_prescr),
				--max(b.qt_dose_prescricao),
				min(b.nr_seq_atendimento),
				min(b.nr_seq_material)
		into	--ds_material_w,
				--cd_material_w,
				--ds_unid_med_w,
				--qt_dose_w,
				nr_seq_atendimento_w,
				nr_seq_material_w
		from   	material e,
				paciente_atend_medic b,
				prescr_material c,
				can_ordem_prod d,
				can_ordem_item_prescr a
		where  	c.nr_seq_atendimento 	= b.nr_seq_atendimento
		and    	c.nr_seq_material	= b.nr_seq_material
		and		c.nr_prescricao  	= d.nr_prescricao
		and		c.nr_seq_ordem_prod  	= d.nr_sequencia
		and 	e.cd_material		= c.cd_material
		and		((e.ie_mistura 		= 'S') or 
				 ((e.ie_mistura	= 'R') and (obter_se_gera_ordem(c.ie_via_aplicacao,c.cd_intervalo, c.qt_dose, c.cd_material, c.cd_unidade_medida,'T', nvl(c.ie_medicacao_paciente, 'N'),d.nr_atendimento) = 'S')))
		and 	a.nr_sequencia	= nr_sequencia_w		
		and   	a.nr_seq_ordem = d.nr_sequencia
		and 	d.nr_sequencia 		= nr_seq_ordem_p;

		select	max(substr(obter_desc_material(b.cd_material),1,60)),
				max(b.cd_material),
				max(b.cd_unid_med_prescr),
				max(b.qt_dose_prescricao)
		into	ds_material_w,
				cd_material_w,
				ds_unid_med_w,
				qt_dose_w
		from	paciente_atend_medic b
		where	nr_seq_atendimento = nr_seq_atendimento_w
		and		nr_seq_material = nr_seq_material_w;

		if 	(ds_material_w is null) then
			Select	distinct
				max(substr(obter_desc_material(a.cd_material),1,60)),
				max(a.cd_unidade_medida),
				max(a.qt_dose),
				max(a.cd_material)
			into	ds_material_w,
					ds_unid_med_w,
					qt_dose_w,
					cd_material_w
			from	MATERIAL b,
					can_ordem_item_prescr a
			where	b.cd_material = a.cd_material
			and 	ie_mistura = 'S'
			and 	a.nr_sequencia	= nr_sequencia_w;
		end if;
	elsif  (ie_considerar_medic_quimio = 'S') then
		select	nvl(nr_sequencia_p, min(nr_sequencia))
		into	nr_sequencia_w
		from	MATERIAL b,
				can_ordem_item_prescr a
		where	nr_seq_ordem	= nr_seq_ordem_p
		and 	b.cd_material = a.cd_material
		and 	ie_mistura = 'S'
		and		nr_sequencia_diluente is null;

		Select	distinct
			substr(obter_desc_material(a.cd_material),1,60),
			a.cd_unidade_medida,
			a.qt_dose,
			a.cd_material
		into	ds_material_w,
				ds_unid_med_w,
				qt_dose_w,
				cd_material_w
		from	MATERIAL b,
				can_ordem_item_prescr a
		where	b.cd_material = a.cd_material
		and 	ie_mistura = 'S'
		and 	a.nr_sequencia	= nr_sequencia_w;
	else
		Select	distinct
			substr(obter_desc_material(cd_material),1,60),
			cd_unidade_medida,
			qt_dose,
			cd_material
		into	ds_material_w,
				ds_unid_med_w,
				qt_dose_w,
				cd_material_w
		from	can_ordem_item_prescr
		where	nr_sequencia	= nr_sequencia_w;
	end if;
end if;


if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= to_char(cd_material_w);
elsif	(ie_opcao_p = 'M') then
	ds_retorno_w	:= ds_material_w;
elsif	(ie_opcao_p = 'D') then
	ds_retorno_w	:= qt_dose_w;
elsif	(ie_opcao_p = 'U') then
	ds_retorno_w	:= ds_unid_med_w;
elsif   (ie_opcao_p = 'SP') then
	ds_retorno_w 	:= nr_seq_prescricao_w;
end if;

return	ds_retorno_w;

END Obter_material_ordem_prod;
/
