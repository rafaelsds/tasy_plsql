create or replace function OBTER_JSON_MEDRECEITA(	nr_atendimento_p med_receita.nr_atendimento%type,
							cd_pessoa_fisica_p pessoa_fisica.cd_pessoa_fisica%type)
return clob is

med_Especialidades_w	 	VARCHAR2(4000);
alergias_w          		VARCHAR2(4000);
med_diagnostico_w    		VARCHAR2(4000);
cd_doenca_w       		VARCHAR2(4000);

receita_numero_w               	med_receita.nr_sequencia%type;
dt_receita_w                   	Varchar(10);
registroprontuarioeletronico_w 	med_receita.nr_atendimento_hosp%type;
med_referenciaexterna_w        	pessoa_fisica.cd_pessoa_fisica%type;
med_reg_conselho_w             	CONSELHO_PROFISSIONAL.sg_conselho%type;
med_reg_numero_w               	VARCHAR2(30);
med_reg_uf_w                   	medico.uf_crm%type;
med_nome_w                     	pessoa_fisica.nm_pessoa_fisica%type;
med_documento_w                	pessoa_fisica.nr_cpf%type ;
med_end_cep_w                  	compl_pessoa_fisica.cd_cep%type;
med_end_endereco1_w            	VARCHAR2(4000);
pac_ReferenciaExterna_w        	pessoa_fisica.cd_pessoa_fisica%type;
pac_nome_w                     	pessoa_fisica.nm_pessoa_fisica%type;
pac_sexo_w                     	pessoa_fisica.ie_sexo%type;
pac_cpf_w                      	pessoa_fisica.NR_CPF%type;
pac_end_cep_w                  	compl_pessoa_fisica.cd_cep%type;
pac_end_endereco1_w            	VARCHAR2(4000);
pac_end_endereco2_w            	compl_pessoa_fisica.ds_complemento%type;
pac_end_bairro_w               	compl_pessoa_fisica.ds_BAIRRO%type;
pac_end_municipio_w            	compl_pessoa_fisica.DS_MUNICIPIO%type;
pac_end_estado_w               	compl_pessoa_fisica.SG_ESTADO%type;
pac_celular_w                  	pessoa_fisica.NR_TELEFONE_CELULAR%type;
pac_dt_nascimento_w            	Varchar(10);
estab_cep_w                    	pessoa_juridica.cd_cep%type;
estab_endereco_w               	VARCHAR2(4000);
estab_complemento_w            	pessoa_juridica.ds_complemento%type;
estab_municipio_w              	pessoa_juridica.DS_MUNICIPIO%type;
estab_estado_w                 	pessoa_juridica.SG_ESTADO%type;
estab_razao_social_w           	pessoa_juridica.ds_razao_social%type;
estab_cns_w                    	estabelecimento.CD_CNS%type;
estab_bairro_w                 	pessoa_juridica.ds_BAIRRO%type;
fone_w                         	pessoa_juridica.nr_telefone%type;
empresa_w                       empresa.nm_razao_social%type;
json_cid_list_w		            philips_json_list;
json_alergia_list_w	            philips_json_list;
json_especial_list_w	        philips_json_list;

CURSOR C01 IS
  select  distinct(a.cd_doenca)
  from  diagnostico_doenca a
  where  nr_atendimento  = nr_atendimento_p;

CURSOR C02 IS 
 select   
   COALESCE(COALESCE(substr(obter_estrut_princ_ativo(a.nr_seq_ficha_tecnica,'P'),1,2000),substr(obter_desc_dcb(a.nr_seq_dcb),1,80)), 
   decode(a.ie_nega_alergias,'N',substr(obter_desc_alergeno(a.nr_seq_tipo),1,80),obter_desc_expressao(346740)))
   from	nivel_seguranca_alerta b,
	paciente_alergia a
	where	a.nr_seq_nivel_seg = b.nr_sequencia(+)	
	and a.cd_pessoa_fisica = pac_ReferenciaExterna_w	
	and (a.dt_liberacao is not null)
	and	a.dt_inativacao is null
	and	consiste_se_exibe_alergia(nr_seq_tipo)	= 'S'
	and	nvl(a.ie_alerta,'S')			= 'S'				
	and	(dt_fim is null or dt_fim > sysdate)
	and	nvl(a.ie_nega_alergias, 'N') = 'N'
	and	cd_pessoa_fisica = pac_ReferenciaExterna_w;
    

 
 cursor	c03 is
	select 	b.ds_especialidade
	from 	especialidade_medica b,
		medico_especialidade a
	where 	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	a.cd_especialidade	= b.cd_especialidade;
 
begin

declare
  json_aux_w       philips_json;
  json_it_aux_w    philips_json;
  ret_json_aux_w   philips_json;
  ds_message_w clob;
begin
  json_aux_w     := philips_json();
  json_it_aux_w  := philips_json();
  ret_json_aux_w := philips_json();
  begin
 
    select	TO_CHAR(sysdate, 'YYYY-MM-DD'),
		ap.nr_atendimento as registroprontuarioeletronico,
		pf.cd_pessoa_fisica as pac_ReferenciaExterna,
		pf.nm_pessoa_fisica as pac_nome,
		pf.ie_sexo as pac_sexo,
		pf.NR_CPF as pac_cpf,
		pacf.cd_cep as pac__end_cep,
		pacf.ds_endereco || ',' || pacf.NR_ENDERECO as pac__end_endereco1,
		pacf.ds_complemento as pac_end_endereco2,
		pacf.ds_BAIRRO as  pac__end_bairro,
		pacf.DS_MUNICIPIO as pac_end_municipio,
		pacf.SG_ESTADO as pac__end_estado,
		pf.NR_DDD_CELULAR || pf.NR_TELEFONE_CELULAR as pac_celular,
		TO_CHAR(pf.dt_nascimento, 'YYYY-MM-DD'),
		pj.cd_cep as estab_cep,
		pj.ds_endereco as estab_endereco,
		pj.ds_complemento || '-' || pj.NR_ENDERECO as estab_complemento,
		pj.ds_BAIRRO as  estab_bairro,
		PJ.DS_MUNICIPIO as estab_municipio,
		PJ.SG_ESTADO as estab_estado,
		pj.ds_razao_social,
		COALESCE (es.CD_CNS, pj.cd_cnes),
		pj.nr_telefone as contato,
		COALESCE(ge.nm_grupo_empresa, emp.nm_razao_social)  as  empresa
    into
		dt_receita_w,
		registroprontuarioeletronico_w,
		pac_ReferenciaExterna_w,
		pac_nome_w,
		pac_sexo_w,
		pac_cpf_w,
		pac_end_cep_w,
		pac_end_endereco1_w,
		pac_end_endereco2_w,
		pac_end_bairro_w,
		pac_end_municipio_w,
		pac_end_estado_w,
		pac_celular_w,
		pac_dt_nascimento_w,
		estab_cep_w,
		estab_endereco_w,
		estab_complemento_w,
		estab_bairro_w,
		estab_municipio_w,
		estab_estado_w,
		estab_razao_social_w,
		estab_cns_w,
		fone_w,
		empresa_w
    from 	atendimento_paciente ap
    left join 	pessoa_fisica pf on pf.cd_pessoa_fisica = ap.cd_pessoa_fisica
    left join   compl_pessoa_fisica pacf on  pacf.cd_pessoa_fisica  = pf.cd_pessoa_fisica
    and         pacf.ie_tipo_complemento = 1  and pacf.nr_seq_tipo_compl_adic is null
    left join 	estabelecimento es on es.cd_estabelecimento = ap.cd_estabelecimento
    left join 	pessoa_juridica pj on pj.cd_cgc = es.cd_cgc
	left join   empresa         emp on emp.cd_empresa = es.cd_empresa
	left join   grupo_emp_estrutura  gee on gee.cd_empresa = emp.cd_empresa
	left join   grupo_empresa   ge  on ge.nr_sequencia = gee.nr_seq_grupo
    where   	ap.nr_atendimento =  nr_atendimento_p;

    select	md.cd_pessoa_fisica as med_referenciaexterna,
		cp.sg_conselho as med_reg_conselho,
		COALESCE(mdc.nr_crm,md.DS_CODIGO_PROF)  as med_reg_numero,
		COALESCE(mdc.uf_crm,md.uf_conselho)  as med_reg_uf,
		md.nm_pessoa_fisica as med_nome,
		md.nr_cpf as med_documento,
		cpf.cd_cep as med_end_cep,
		cpf.ds_endereco || ',' || cpf.nr_endereco as med_end_endereco1
    into 	med_referenciaexterna_w,
		med_reg_conselho_w,
		med_reg_numero_w,
		med_reg_uf_w,
		med_nome_w,
		med_documento_w,
		med_end_cep_w,
		med_end_endereco1_w
    from 	pessoa_fisica md
    left join 	CONSELHO_PROFISSIONAL cp on cp.nr_sequencia = md.nr_seq_conselho
    left join 	medico mdc on mdc.cd_pessoa_fisica = md.cd_pessoa_fisica
    left join 	compl_pessoa_fisica cpf on  cpf.cd_pessoa_fisica  = md.cd_pessoa_fisica and cpf.nr_sequencia = 1
    where       md.cd_pessoa_fisica = cd_pessoa_fisica_p;
  
  json_cid_list_w	:= philips_json_list();
 OPEN C01;
  LOOP
  FETCH C01 into
    cd_doenca_w;
  exit when c01%notfound;
    BEGIN
    json_cid_list_w.append(cd_doenca_w);
    END;
  END LOOP;
  CLOSE C01;
  
  json_alergia_list_w	:= philips_json_list();
  OPEN C02;
  LOOP
  FETCH C02 into
    alergias_w;
  exit when c02%notfound;
    BEGIN
      json_alergia_list_w.append(alergias_w);
    END;
  END LOOP;
  CLOSE C02;

  json_especial_list_w	:= philips_json_list();
  OPEN C03;
  LOOP
  FETCH C03 into
    med_especialidades_w;
  exit when C03%notfound;
    BEGIN
    json_especial_list_w.append(med_especialidades_w);
    END;
  END LOOP;
  CLOSE C03;
  
  ret_json_aux_w.put('DataPrescricao', dt_receita_w);
  ret_json_aux_w.put('SubParceiro', empresa_w);
  json_aux_w.put('TipoDocumento', '"CPF"');
 -- json_aux_w.put('Especialidades','[' || med_especialidades_w || ']');
  json_aux_w.put('Especialidades',json_especial_list_w);
  json_it_aux_w.put('Numero',med_reg_numero_w);
  json_it_aux_w.put('Conselho',med_reg_conselho_w);
  json_it_aux_w.put('UF',med_reg_uf_w);
  json_aux_w.path_put('RegistroProfissional', json_it_aux_w);
  json_aux_w.put('Nome', med_nome_w);
  json_aux_w.put('Documento', med_documento_w);
  json_aux_w.put('ReferenciaExterna', med_referenciaexterna_w);
  ret_json_aux_w.path_put('Medico',json_aux_w);
  
  json_it_aux_w := philips_json();
  json_aux_w := philips_json();
  
  json_aux_w.put('Alergias',json_alergia_list_w);
  json_aux_w.put('Nome',pac_nome_w );
  json_aux_w.put('Documento',pac_cpf_w );
  json_aux_w.put('Nascimento',pac_dt_nascimento_w );
  json_aux_w.put('Sexo',pac_sexo_w );
  json_it_aux_w.put('Endereco1',pac_end_endereco1_w);
  json_it_aux_w.put('Endereco2',pac_end_endereco2_w);
  json_it_aux_w.put('Bairro',pac_end_bairro_w);
  json_it_aux_w.put('Cidade',pac_end_municipio_w);
  json_it_aux_w.put('Estado',pac_end_estado_w);
  json_it_aux_w.put('CodigoPostal',pac_end_cep_w);
  json_aux_w.path_put('Endereco',json_it_aux_w);
  json_aux_w.put('ReferenciaExterna',pac_ReferenciaExterna_w);
  json_aux_w.put('TelefoneCelular',pac_celular_w);
  ret_json_aux_w.path_put('Paciente',json_aux_w);
  
  json_it_aux_w := philips_json();
  json_aux_w := philips_json();
 
  json_it_aux_w.put('ReferenciaExterna',registroprontuarioeletronico_w) ;
  ret_json_aux_w.path_put('RegistroProntuarioEletronico',json_it_aux_w);

  json_it_aux_w := philips_json();

  json_aux_w.put('Nome',estab_razao_social_w);
  json_aux_w.put('CNES',estab_cns_w);
  json_it_aux_w.put('Endereco1',estab_endereco_w);
  json_it_aux_w.put('Endereco2',estab_complemento_w);
  json_it_aux_w.put('Bairro',estab_bairro_w);
  json_it_aux_w.put('Cidade',estab_municipio_w);
  json_it_aux_w.put('Estado',estab_estado_w);
  json_it_aux_w.put('CodigoPostal',estab_cep_w);
  json_aux_w.path_put('Endereco',json_it_aux_w);
  json_it_aux_w := philips_json();
  json_it_aux_w.put('TelefoneComercial',fone_w);
  json_aux_w.path_put('Contato',json_it_aux_w);
  ret_json_aux_w.path_put('Estabelecimento',json_aux_w);
  ret_json_aux_w.put('CID10',json_cid_list_w);

  dbms_lob.createtemporary(ds_message_w, TRUE);
  ret_json_aux_w.to_clob(ds_message_w);

  exception
    when no_data_found OR too_many_rows then
      ds_message_w := empty_clob();
  end;

  return ds_message_w;
  end;

end OBTER_JSON_MEDRECEITA;
/