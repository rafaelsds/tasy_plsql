create or replace
function san_obter_unidade_atend( nr_atendimento_p number)
	        return number is

dt_entrada_unidade_w  date;
nr_seq_interno_w number(10);

begin

if (nr_atendimento_p is not null) then
	select  max(dt_entrada_unidade)
	into 	dt_entrada_unidade_w
	from   	atend_paciente_unidade
	where 	nr_atendimento = nr_atendimento_p
	and    	dt_saida_unidade is null;

	select 	max(nr_seq_interno)
	into 	nr_seq_interno_w
	from    atend_paciente_unidade
	where   nr_atendimento  = nr_atendimento_p
	and     dt_entrada_unidade  = dt_entrada_unidade_w
	and     dt_saida_unidade is null;	
end if;

return nr_seq_interno_w;

end san_obter_unidade_atend;
/
