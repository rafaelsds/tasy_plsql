create or replace
function rxt_obter_desc_aplicador(
				nr_seq_aplicador_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(80);
			
begin

select	max(ds_aplicador)
into	ds_retorno_w
from	rxt_aplicador
where	nr_sequencia	= nr_seq_aplicador_p;

return	ds_retorno_w;

end rxt_obter_desc_aplicador;
/