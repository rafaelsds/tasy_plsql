create or replace
function obter_classif_setor(	cd_setor_atendimento_p	number  )
			return number is

cd_classif_setor_w		number;

begin

select			max(cd_classif_setor)
into			cd_classif_setor_w
from			setor_atendimento
where			cd_setor_atendimento = cd_setor_atendimento_p;

return			cd_classif_setor_w;

end obter_classif_setor;
/