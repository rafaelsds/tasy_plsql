create or replace
function obter_saldo_total_disp_estoque(cd_material_p         	in number,
					cd_local_estoque_p      in number,
					cd_estabelecimento_p    in number,
					nr_seq_lote_fornec_p	in number default null,
					cd_cgc_fornecedor_p	in varchar2 default null,
					ie_soma_saldo_consig_p	in varchar2 default 'N')
					return number is

/* Function utilizada para obter o saldo disponivel dos materiais quando necessario verificar regra de material consignado ambos.
Caso o material for do tipo consignado ambos e houver regra de saldo consignado cadastrada, entao realizara a soma das quantidades disponiveis.
Do contrario, retorna o saldo consignado quando material consignado e o saldo proprio quando material nao consignado.
Caso o material for do tipo consignado ambos e nao houver regra de saldo consignado cadastrada,
sera retornado o saldo consignado caso houver fornecedor informado. Senao, retorna o sado proprio. */

/* ie_soma_saldo_consig_p -	Parametro utilizado para a funcao Requisicao de Materiais e Medicamentos,
				para apresentar a quantidade de estoque do item requisitado.
				Caso a operacao da requisicao for do tipo consignada e nao houver regra de consignado ambos,
				entao deve somar apenas o saldo consignado do item. */

ie_regra_saldo_consig_w	parametro_estoque.ie_regra_saldo_consig%type;
dt_mesano_vigente_w	parametro_estoque.dt_mesano_vigente%type;
cd_material_estoque_w	material.cd_material_estoque%type;
ie_consignado_w		material.ie_consignado%type;
ie_estoque_lote_w	material_estab.ie_estoque_lote%type;
cd_cgc_fornecedor_w	material_lote_fornec.cd_cgc_fornec%type := cd_cgc_fornecedor_p;
qt_saldo_estoque_w	number(15,4) := 0;
qt_estoque_consigado_w	number(15,4) := 0;

begin

select	nvl(max(ie_regra_saldo_consig),0),
	PKG_DATE_UTILS.start_of(sysdate, 'MONTH', 0)
into	ie_regra_saldo_consig_w,
	dt_mesano_vigente_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_p;

select	a.cd_material_estoque,
	nvl(a.ie_consignado,'0'),
	nvl(b.ie_estoque_lote, 'N')
into	cd_material_estoque_w,
	ie_consignado_w,
	ie_estoque_lote_w
from	material a,
	material_estab b
where	a.cd_material = b.cd_material
and	a.cd_material = cd_material_p
and	b.cd_estabelecimento = cd_estabelecimento_p;

/* Atencao aqui!
Caso nao for informado o fornecedor e houver lote fornecedor informado, sera obtido o fornecedor do lote.
Tratamento realizado para correto retorno do saldo no caso da utilizacao do objeto ao bipar item por barras. */
/*if	(cd_cgc_fornecedor_w is null) and
	(nr_seq_lote_fornec_p is not null) then
	cd_cgc_fornecedor_w := obter_dados_lote_fornec(
					nr_sequencia_p	=> nr_seq_lote_fornec_p,
					ie_opcao_p	=> 'CF');
end if;*/

/* Item proprio ou consignado ambos sem fornecedor informado. */
if	(ie_consignado_w = '0') or
	(ie_consignado_w = '2' and cd_cgc_fornecedor_w is null and
	(ie_soma_saldo_consig_p = 'N' or ie_regra_saldo_consig_w > 0)) then
	qt_saldo_estoque_w := obter_saldo_disp_estoque(
					cd_estabelecimento_p	=> cd_estabelecimento_p,
					cd_material_p		=> cd_material_estoque_w,
					cd_local_estoque_p	=> cd_local_estoque_p,
					dt_mesano_referencia_p	=> dt_mesano_vigente_w,
					nr_seq_lote_p		=> nr_seq_lote_fornec_p);

	/* Caso o item for consignado ambos e houver regra de consignado, devera somar o saldo consignado ao proprio. */
	if	(ie_consignado_w = '2') and (ie_regra_saldo_consig_w > 0)then
		/* Utilizada a function abaixo porque ela era utilizada anteriormente para obter o saldo de estoque do item da requisicao ao cria-la,
		quando a requisicao fosse de operacao consignado. Caso for necessario alterar essa consulta,
		observar se os valores do item da requisicao estao sendo apresentados corretamente. */
		qt_estoque_consigado_w := obter_saldo_estoque_consig(
						cd_estabelecimento_p	=> cd_estabelecimento_p,
						cd_cgc_fornec_p		=> cd_cgc_fornecedor_w,
						cd_material_estoque_p	=> cd_material_estoque_w,
						cd_local_estoque_p	=> cd_local_estoque_p);

		qt_saldo_estoque_w	:= qt_saldo_estoque_w + qt_estoque_consigado_w;
	end if;

/* Item consignado ou consignado ambos com fornecedor informado. */
elsif	(ie_consignado_w = '1') or
	(ie_consignado_w = '2' and
	(cd_cgc_fornecedor_w is not null or ie_soma_saldo_consig_p = 'S')) then
	/* Utilizada a function abaixo porque ela era utilizada anteriormente para obter o saldo de estoque do item da requisicao ao cria-la,
	quando a requisicao fosse de operacao consignado. Caso for necessario alterar essa consulta,
	observar se os valores do item da requisicao estao sendo apresentados corretamente. */
	qt_saldo_estoque_w := obter_saldo_estoque_consig(
					cd_estabelecimento_p	=> cd_estabelecimento_p,
					cd_cgc_fornec_p		=> cd_cgc_fornecedor_w,
					cd_material_estoque_p	=> cd_material_estoque_w,
					cd_local_estoque_p	=> cd_local_estoque_p);
end if;

return qt_saldo_estoque_w;
end obter_saldo_total_disp_estoque;
/
