create or replace
function obter_prescricao_agenda(	nr_seq_agenda_p	number)
					return number is

nr_prescricao_w	number(14,0);
			
begin

begin
select	max(nr_prescricao)
into	nr_prescricao_w
from	prescr_medica
where	nr_seq_agenda	=	nr_seq_agenda_p
and 	ie_tipo_prescr_cirur is null ;
exception
	when others then
		nr_prescricao_w	:= 0;
end;

return	nr_prescricao_w;

end obter_prescricao_agenda;
/