create or replace
function ageint_obter_grupos_regra_proc(nr_seq_proc_interno_p		 number,
					nr_seq_ageint_p			number,
					cd_estabelecimento_p		number)
 		    	return varchar2 is
		
nr_seq_grupo_check_list_w varchar2(10);
cd_procedimento_w 	  number(15,0);
ie_origem_proced_w	  number(10,0);
ds_retorno_w		  varchar2(4000);
cd_convenio_w		number(5,0);	
cd_categoria_w		varchar2(10);
cd_estabelecimento_w	number(4,0);
cd_plano_w		varchar2(10);
		
Cursor C01 is
	select	x.nr_seq_grupo_check_list
	from	ageint_regra_checklist x
	where	x.ie_origem_proced = ie_origem_proced_w and
		(x.cd_procedimento = cd_procedimento_w) or
		(x.nr_seq_proc_interno = nr_seq_proc_interno_p) or
		(x.cd_area_procedimento = obter_area_procedimento(cd_procedimento_w, ie_origem_proced_w)) or
		(x.cd_especialidade = obter_especialidade_proced(cd_procedimento_w, ie_origem_proced_w)) or
		(x.cd_grupo_proc = obter_grupo_procedimento(cd_procedimento_w, ie_origem_proced_w,'C'))
	and	nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	order by 1;

begin

select	cd_estabelecimento,
	cd_convenio,
	cd_categoria,
	cd_plano
into	cd_estabelecimento_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w
from	agenda_integrada
where	nr_sequencia = nr_seq_ageint_p;

obter_proc_tab_interno_conv(
				nr_seq_proc_interno_p,
				cd_estabelecimento_w,
				cd_convenio_w,
				cd_categoria_w,
				cd_plano_w,
				null,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				sysdate,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);

open	c01;
loop
fetch	c01 into nr_seq_grupo_check_list_w;
	exit when c01%notfound;
	begin

	ds_retorno_w	:= ds_retorno_w || nr_seq_grupo_check_list_w || ',';

	end;
end loop;
close c01;

ds_retorno_w	:= substr(ds_retorno_w, 1, length(ds_retorno_w) - 1);

return	ds_retorno_w;

end ageint_obter_grupos_regra_proc;
/
