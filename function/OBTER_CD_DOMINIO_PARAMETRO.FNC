create or replace FUNCTION obter_cd_dominio_parametro	(CD_FUNCAO_P	NUMBER, CD_PARAMETRO_P NUMBER)
			RETURN Number IS

cd_dominio_w	FUNCAO_PARAMETRO.CD_DOMINIO%TYPE;

BEGIN

IF	(CD_FUNCAO_P	IS NOT NULL and CD_PARAMETRO_P is not null) THEN
	BEGIN
	
	 select nvl(max(CD_DOMINIO),0)
	 into	cd_dominio_w
	 from   FUNCAO_PARAMETRO 
	 where  CD_FUNCAO = CD_FUNCAO_P 
	 and    NR_SEQUENCIA = CD_PARAMETRO_P;

	END;
END IF;

RETURN	cd_dominio_w;

END obter_cd_dominio_parametro;
/