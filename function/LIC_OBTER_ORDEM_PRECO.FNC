create or replace
function LIC_OBTER_ORDEM_PRECO (nr_seq_item_p	number,
				nr_seq_fornec_p	number) return varchar2 is

nr_ordem_w			number(10,0);
nr_ordem_ww			number(10,0);
nr_ordem_retorno_w		number(10,0);
nr_seq_fornec_w		number(10,0);
vl_item_w			number(15,4);
vl_anterior_w			number(15,4);
ie_vencedor_w			Varchar2(1);

cursor c01 is
	select	nr_seq_fornec,
		vl_item,
		ie_vencedor
	from	lic_item_fornec 
	where	nr_seq_item = nr_seq_item_p
	and	vl_item > 0
	order	by 	ie_vencedor desc,
			vl_item asc;

BEGIN

nr_ordem_w	:= 0;
nr_ordem_ww	:= 0;
open c01;
loop
fetch c01 into
	nr_seq_fornec_w,
	vl_item_w,
	ie_vencedor_w;
exit when c01%notfound;
	nr_ordem_ww		:= nr_ordem_ww + 1;
	if	(nr_ordem_w = 0) then
		nr_ordem_w	:= nr_ordem_w + 1;
	elsif	(vl_item_w	= vl_anterior_w) and
		(nr_ordem_w	= 1)	then
		nr_ordem_w	:= nr_ordem_w + 1;
	elsif	(vl_item_w	<> vl_anterior_w) then
		nr_ordem_w	:= nr_ordem_ww;
	end if;
	vl_anterior_w		:= vl_item_w;
	if	(nr_seq_fornec_w = nr_seq_fornec_p) then
		nr_ordem_retorno_w := nr_ordem_w;
		exit;
	end if;

end loop;
close c01;
return to_char(nr_ordem_retorno_w);
end;
/