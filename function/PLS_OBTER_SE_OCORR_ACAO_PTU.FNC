create or replace
function pls_obter_se_ocorr_acao_ptu	(	nr_seq_guia_p		Number,
						nr_seq_requisicao_p	Number,
						nr_seq_complemento_p	Number)
						return Varchar2 is

ie_tipo_transacao_w				Varchar2(2);
ie_retorno_w					Varchar2(2)	:= 'N';

nr_seq_ocorrencia_w				Number(10);
nr_seq_procedimento_w				Number(10);
nr_seq_material_w				Number(10);

qt_registros_w					Number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p
	and	(nr_seq_compl_ptu	= nr_seq_complemento_p	or nr_seq_complemento_p is null)
	union
	select	nr_sequencia
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	(nr_seq_compl_ptu	= nr_seq_complemento_p	or nr_seq_complemento_p is null);
	
Cursor C02 is
	select	nr_sequencia
	from	pls_guia_plano_mat
	where	nr_seq_guia		= nr_seq_guia_p
	and	(nr_seq_compl_ptu	= nr_seq_complemento_p	or nr_seq_complemento_p is null)
	union
	select	nr_sequencia
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	(nr_seq_compl_ptu	= nr_seq_complemento_p	or nr_seq_complemento_p is null);

Cursor C03 is
	select	nr_seq_ocorrencia
	from	pls_ocorrencia_benef
	where	((nr_seq_proc		= nr_seq_procedimento_w)
	or	(nr_seq_mat		= nr_seq_material_w))
	and	nr_seq_guia_plano	= nr_seq_guia_p
	and	ie_tipo_transacao_w	= 'G'
	union
	select	nr_seq_ocorrencia
	from	pls_ocorrencia_benef
	where	((nr_seq_proc		= nr_seq_procedimento_w)
	or	(nr_seq_mat		= nr_seq_material_w))
	and	nr_seq_requisicao	= nr_seq_requisicao_p
	and	ie_tipo_transacao_w	= 'R';
begin

if	(nr_seq_guia_p	is not null) then
	ie_tipo_transacao_w	:= 'G';
elsif	(nr_seq_requisicao_p	is not null) then
	ie_tipo_transacao_w	:= 'R';
end if;
	
open C01;
loop
fetch C01 into	
	nr_seq_procedimento_w;
exit when C01%notfound;
	begin
	open C03;
	loop
	fetch C03 into	
		nr_seq_ocorrencia_w;
	exit when C03%notfound;
		begin
		select	count(*)
		into	qt_registros_w
		from	pls_ocorrencia_scs
		where	nr_seq_ocorrencia	= nr_seq_ocorrencia_w;
		
		if	(qt_registros_w	<> 0) then
			ie_retorno_w	:= 'S';
		end if;
		end;
	end loop;
	close C03;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_seq_material_w;
exit when C02%notfound;
	begin
	open C03;
	loop
	fetch C03 into	
		nr_seq_ocorrencia_w;
	exit when C03%notfound;
		begin
		select	count(*)
		into	qt_registros_w
		from	pls_ocorrencia_scs
		where	nr_seq_ocorrencia	= nr_seq_ocorrencia_w;
		
		if	(qt_registros_w	<> 0) then
			ie_retorno_w	:= 'S';
		end if;
		end;
	end loop;
	close C03;
	end;
end loop;
close C02;

return	ie_retorno_w;

end pls_obter_se_ocorr_acao_ptu;
/