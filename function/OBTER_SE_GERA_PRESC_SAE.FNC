create or replace
function obter_se_gera_presc_sae(nr_sequencia_p  number)
			return varchar2 is

qt_prescr_w number(10);
ie_existe_w  varchar2(1) := 'N';
begin

select  count(*)
into 	qt_prescr_w
from 	pe_prescr_proc a,
	pe_material_proced b
where 	b.nr_seq_proc = a.nr_seq_proc
and 	a.nr_seq_prescr = nr_sequencia_p;

if  (nvl(qt_prescr_w,0) = 0) then
	select 	count(*)
	into  	qt_prescr_w
	from 	pe_prescr_proc b,
		pe_interv_protocolo a
	where 	b.nr_seq_proc = a.nr_seq_interv
	and	b.nr_seq_prescr = nr_sequencia_p;
end if;

if  (nvl(qt_prescr_w,0) > 0) then
	ie_existe_w := 'S';
end if;


return ie_existe_w;

end obter_se_gera_presc_sae;
/