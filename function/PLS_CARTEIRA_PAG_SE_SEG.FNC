create or replace
function pls_carteira_pag_se_seg(	nr_seq_pagador_p	number,
					cd_pessoa_fisica_p	varchar2)
					return varchar2 is

cd_carteira_w		varchar2(30);
nr_seq_contrato_w	number(10);			
			
begin
if	(cd_pessoa_fisica_p is not null) then
	select	max(b.cd_usuario_plano)
	into	cd_carteira_w
	from	pls_segurado_carteira	b,
		pls_segurado		a
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	a.nr_seq_pagador	= nr_seq_pagador_p;

	if	(cd_carteira_w is null) then
		select	max(a.nr_seq_contrato)
		into	nr_seq_contrato_w
		from	pls_contrato_pagador a
		where	a.nr_sequencia	= nr_seq_pagador_p;

		select	max(b.cd_usuario_plano)
		into	cd_carteira_w
		from	pls_segurado_carteira	b,
			pls_segurado		a
		where	b.nr_seq_segurado	= a.nr_sequencia
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	a.nr_seq_contrato	= nr_seq_contrato_w
		and	a.nr_seq_pagador	<> nr_seq_pagador_p;	
	end if;
end if;

if	(cd_carteira_w is null) then
	select	max(b.cd_usuario_plano)
	into	cd_carteira_w
	from	pls_segurado_carteira	b,
		pls_segurado		a
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	a.nr_seq_titular	is null
	and	a.nr_seq_pagador	= nr_seq_pagador_p;
end if;

return	cd_carteira_w;

end pls_carteira_pag_se_seg;
/