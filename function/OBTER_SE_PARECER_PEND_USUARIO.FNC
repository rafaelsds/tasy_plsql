create or replace
function obter_se_parecer_pend_usuario(nr_seq_spa_p		number,
				       nm_usuario_p	        varchar2)
 		    	return varchar2 is

ie_parecer_w		varchar2(1);
nr_sequencia_w		spa_parecer.nr_sequencia%type;
qt_reg_w		number(10,0);
			
begin

select	nvl(max(a.nr_sequencia),0)
into	nr_sequencia_w
from	spa_parecer a
where	a.nr_seq_spa = nr_seq_spa_p;

select	count(*)
into	qt_reg_w
from	spa_parecer
where	nr_sequencia = nr_sequencia_w
and 	NM_USUARIO_PARECER = nm_usuario_p
and 	DT_RESP_PARECER is null;

if	(qt_reg_w > 0) then
	ie_parecer_w := 'S';
else
	ie_parecer_w := 'N'; 
end if;
	

return	ie_parecer_w;

end obter_se_parecer_pend_usuario;
/