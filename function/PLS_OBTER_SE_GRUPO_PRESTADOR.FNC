create or replace
function pls_obter_se_grupo_prestador
			(	nr_seq_prestador_p		number,
				nr_seq_grupo_prestador_p	number) 
				return Varchar2 is
				
ds_retorno_w			varchar2(1);			
				
begin

/* Utilizar a rotina PLS_SE_GRUPO_PRECO_PRESTADOR */

select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	pls_preco_grupo_prestador	b,
	pls_preco_prestador		a
where	b.nr_sequencia		= nr_seq_grupo_prestador_p
and	b.nr_sequencia		= a.nr_seq_grupo
and	a.nr_seq_prestador	= nr_seq_prestador_p
and	b.ie_situacao = 'A';

return	ds_retorno_w;

end pls_obter_se_grupo_prestador;
/
