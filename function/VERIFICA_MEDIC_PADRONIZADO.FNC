create or replace
function Verifica_medic_padronizado(	nr_prescricao_p			Number,
					cd_material_p			Number,
					ie_padronizado_p		Varchar2)
				return varchar2 is

ie_permite_w			varchar2(1) := 'S';
ie_padronizado_w		Varchar2(1);
ie_prescricao_w			Varchar2(1);
ie_lib_paciente_w		Number(10) := 0;
cd_pessoa_fisica_w		Varchar2(10);
ie_prescr_nao_padrao_w		Varchar2(10);
nr_atendimento_w		Number(10);
qt_prescricao_w			Number(10);
qt_dias_tratamento_w		Number(10);
dt_inicio_validade_w		Date;
cd_estabelecimento_w		Number(5);
nm_usuario_w		varchar2(50);
ie_usa_liberacao_w	varchar2(50);
			
begin

Select	cd_pessoa_fisica,
	nr_atendimento,
	nm_usuario,
	cd_estabelecimento
into	cd_pessoa_fisica_w,
	nr_atendimento_w,
	nm_usuario_w,
	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

obter_param_usuario(47, 1, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_usa_liberacao_w);

Select	count(*)
into	ie_lib_paciente_w
from	lib_material_paciente
where	cd_material = cd_material_p
and	((ie_usa_liberacao_w = 'N') or (dt_liberacao is not null))
and	cd_pessoa_fisica = cd_pessoa_fisica_w
and	dt_suspenso is null;

/*select	nvl(max(ie_padronizado),'N'),		Nao e necessario pois ja atualiza a variavel mais abaixo
	nvl(max(ie_prescricao),'N')
into	ie_padronizado_w,
	ie_prescricao_w
from	material
where	cd_material = cd_material_p;*/

if	(cd_estabelecimento_w > 0) then
	ie_padronizado_w	:= substr(obter_se_material_padronizado(cd_estabelecimento_w, cd_material_p),1,1);
end if;

ie_prescr_nao_padrao_w	:= OBTER_PRESCR_NAO_PADRONIZADO(nr_atendimento_w);

if	(ie_padronizado_w = 'N') and
	((ie_padronizado_p = 'O') or
	 ((ie_padronizado_p = 'R') and
	  (ie_prescr_nao_padrao_w = 'N'))) and
	(ie_lib_paciente_w = 0) then
	ie_permite_w := 'N';	
end if;

if	(ie_lib_paciente_w = 0) and
	((ie_padronizado_p = 'O') or
	 ((ie_padronizado_p = 'R') and
	  (ie_prescr_nao_padrao_w = 'N'))) and	
	(ie_padronizado_w = 'N') then
	ie_permite_w := 'N';	
end if;	

if	(ie_lib_paciente_w > 0) and
	((ie_padronizado_p = 'O') or
	 ((ie_padronizado_p = 'R') and
	  (ie_prescr_nao_padrao_w = 'N'))) and
	(ie_padronizado_w = 'N') and
	(nr_atendimento_w is not null) then

	Select	nvl(max(qt_dias_tratamento),0),
		max(nvl(dt_inicio_validade,dt_atualizacao))
	into	qt_dias_tratamento_w,
		dt_inicio_validade_w
	from	lib_material_paciente
	where	cd_material 	= cd_material_p
	and	((ie_usa_liberacao_w = 'N') or (dt_liberacao is not null))
	and	cd_pessoa_fisica = cd_pessoa_fisica_w
	and	dt_suspenso 	is null;

	if	(qt_dias_tratamento_w > 0) then

		select	count(*)
		into	qt_prescricao_w
		from	prescr_medica a
		where	a.nr_prescricao = nr_prescricao_p
		and	establishment_timezone_utils.startofday(a.dt_prescricao)	>= establishment_timezone_utils.startofday(dt_inicio_validade_w + qt_dias_tratamento_w)
		and	(a.dt_liberacao is null or a.dt_liberacao_medico is null);
	
		if	(qt_prescricao_w > 0) then
			ie_permite_w := 'N';		
		end if;
	end if;

end if;

if	(ie_lib_paciente_w > 0) and
	((ie_padronizado_p = 'O') or
	 ((ie_padronizado_p = 'R') and
	  (ie_prescr_nao_padrao_w = 'N'))) and
	(ie_padronizado_w = 'N') and
	(nr_atendimento_w is not null) then
	
	Select	max(qt_dias_tratamento),
		max(nvl(dt_inicio_validade,dt_atualizacao))
	into	qt_dias_tratamento_w,
		dt_inicio_validade_w
	from	lib_material_paciente
	where	cd_material = cd_material_p
	and	((ie_usa_liberacao_w = 'N') or (dt_liberacao is not null))
	and	cd_pessoa_fisica = cd_pessoa_fisica_w
	and	dt_suspenso is null;

	select	count(distinct(establishment_timezone_utils.startofday(a.dt_prescricao)))
	into	qt_prescricao_w
	from	prescr_material b,
		prescr_medica a
	where	a.nr_prescricao = b.nr_prescricao
	and	b.cd_material = cd_material_p
	and	a.nr_atendimento = nr_atendimento_w
	and	establishment_timezone_utils.startofday(a.dt_prescricao)	>= establishment_timezone_utils.startofday(dt_inicio_validade_w) 
	and	(a.dt_liberacao is not null or a.dt_liberacao_medico is not null);
	
	if	(qt_prescricao_w >= qt_dias_tratamento_w) then
		ie_permite_w := 'N';		
	end if;

end if;

if	(cd_estabelecimento_w > 0) and
	(ie_permite_w = 'S') then
	ie_prescricao_w	:= substr(obter_se_material_prescricao(cd_estabelecimento_w, cd_material_p),1,1);
end if;

if	(ie_prescricao_w = 'N') then
	ie_permite_w := 'N';	
end if;

return	ie_permite_w;

end Verifica_medic_padronizado;
/
