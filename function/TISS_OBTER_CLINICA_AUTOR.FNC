create or replace
function TISS_OBTER_CLINICA_AUTOR
	(ie_clinica_p	number,
	 nr_seq_classificacao_p  varchar2) return varchar2 is

ds_retorno_w	varchar2(20);


Cursor C01 is
	select	ie_tipo_internacao_tiss
	from	tiss_tipo_internacao
	WHERE	nvl(ie_clinica,nvl(ie_clinica_p,0))	= nvl(ie_clinica_p,0)
	AND    nvl(nr_seq_classificacao,nvl(nr_seq_classificacao_p,0)) = nvl(nr_seq_classificacao_p,0)
	order by nvl(nr_seq_classificacao,0),
		 nvl(ie_clinica,0);

begin
open C01;
loop
fetch C01 into	
	ds_retorno_w;
exit when C01%notfound;
	begin
	ds_retorno_w := ds_retorno_w;
	end;
end loop;
close C01;

return	ds_retorno_w;

end TISS_OBTER_CLINICA_AUTOR;
/
