create or replace
function obter_se_exige_aval_inspecao(nr_seq_registro_p		number)
return varchar2 is						


cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
ie_exige_aval_inspecao_w		varchar2(1) := 'N';
ie_exige_avaliacao_w			varchar2(1);
cd_material_w				number(6);
cd_cgc_w				inspecao_recebimento.cd_cgc%type;
qt_registros_w				number(10);
nr_sequencia_w				exige_inspecao_item_nf.nr_sequencia%type;

cursor c01 is
select	cd_material,
	cd_cgc
from	inspecao_recebimento
where	nr_seq_registro = nr_seq_registro_p
order by cd_material;

cursor c02 is
select	nvl(ie_exige_avaliacao,'N'),
	nr_sequencia
from	exige_inspecao_item_nf
where	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(cd_material, cd_material_w) 			= cd_material_w or cd_material_w = 0)
order by nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);

begin

open c01;
loop
fetch c01 into	
	cd_material_w,
	cd_cgc_w;
exit when c01%notfound;
	begin
	select	a.cd_grupo_material,
		a.cd_subgrupo_material,
		a.cd_classe_material
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from	estrutura_material_v a
	where	a.cd_material = cd_material_w;
		
	open c02;
	loop
	fetch c02 into	
		ie_exige_avaliacao_w,
		nr_sequencia_w;
	exit when c02%notfound;
		begin
		nr_sequencia_w := nr_sequencia_w;
		if	(ie_exige_aval_inspecao_w = 'N') then
			ie_exige_aval_inspecao_w := ie_exige_avaliacao_w;
		end if;
		end;
	end loop;
	close c02;
	
	
	select	count(*)
	into	qt_registros_w
	from	exige_inspecao_item_nf_for
	where	nr_seq_regra = nr_sequencia_w;

	if	(qt_registros_w > 0) then

		select	count(*)
		into	qt_registros_w
		from	exige_inspecao_item_nf_for
		where	nr_seq_regra = nr_sequencia_w
		and	cd_cnpj = cd_cgc_w;
		
		if	(qt_registros_w = 0) then
			ie_exige_aval_inspecao_w := 'N';
		end if;
	end if;
	end;	
end loop;
close c01;

return nvl(ie_exige_aval_inspecao_w,'N');

end obter_se_exige_aval_inspecao;
/
