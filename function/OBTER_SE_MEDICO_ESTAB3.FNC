create or replace
function obter_se_medico_estab3	(cd_medico_p	varchar2,
				cd_estabelecimento_p	number)
				return	varchar2 is					

ds_retorno_w		varchar2(1):= 'S';
qt_reg_w		number(10,0);
qt_reg_estab_w		number(10,0);
cd_empresa_w		number(4,0);
ie_restringe_estab_w	varchar2(1);

begin

select	nvl(max(ie_consite_medico_estab),'N')
into	ie_restringe_estab_w
from	parametro_atendimento
where	cd_estabelecimento = cd_estabelecimento_p;

If	(ie_restringe_estab_w = 'S') then

	begin
		select	cd_pessoa_fisica
		into	qt_reg_w
		from	medico_estabelecimento
		where	cd_pessoa_fisica = cd_medico_p
		and	rownum = 1;
	exception
		when 	no_data_found then
			qt_reg_w := 0;
		when	others then
			qt_reg_w := 1;
	end;
		
	if 	(qt_reg_w > 0) then
		
		select	nvl(max(cd_empresa),0)
		into	cd_empresa_w
		from	estabelecimento
		where	cd_estabelecimento = cd_estabelecimento_p;

		begin
			select	cd_pessoa_fisica
			into	qt_reg_estab_w
			from	medico_estabelecimento
			where	cd_pessoa_fisica				= cd_medico_p
			and	nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
			and	nvl(cd_empresa, cd_empresa_w)			= cd_empresa_w;
		exception
			when 	no_data_found then
				qt_reg_estab_w := 0;
			when	others then
				qt_reg_estab_w := 1;
		end;
		
		if 	(qt_reg_estab_w = 0) then
			ds_retorno_w := 'N';
		end if;

	end if;
end if;

return	ds_retorno_w;

end obter_se_medico_estab3;
/
