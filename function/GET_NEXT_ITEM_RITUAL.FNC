create or replace function get_next_item_ritual(cd_perfil_p		number,
												nm_usuario_p	varchar2,
												nr_seq_item_p	number) return varchar2 is
												
ds_retorno_w		varchar2(10);
nr_seq_ritual_w		number(10);
nr_seq_acao_w   	number(10);
nr_seq_item_w   	number(10);

begin
if	(cd_perfil_p is not null) and 
	(nm_usuario_p is not null) and
	(nr_seq_item_p is not null) then
	
	select	 max(a.nr_seq_ritual), max(b.nr_seq_acao)
	into     nr_seq_ritual_w, nr_seq_acao_w
	from	 pep_ritual_item a,
			 pep_ritual_item_acao b
	where	 a.nr_sequencia = b.nr_seq_ritual_item
	and	     a.nr_seq_ritual = obter_ritual_pep(cd_perfil_p,nm_usuario_p)
	and      a.nr_seq_item = nr_seq_item_p
	and      b.nr_seq_acao in (32, 33, 39)
	order by a.dt_atualizacao;
	
	if (nr_seq_acao_w = 32) then
		ds_retorno_w := 'MODAL';
	
	elsif (nr_seq_acao_w = 33) then
		select	distinct first_value(a.nr_seq_item) over (order by a.nr_seq_apresent)
		into	nr_seq_item_w
		from	pep_ritual_item_sequencia a
		where	a.nr_seq_apresent is not null
		and		a.nr_seq_ritual = nr_seq_ritual_w
		and		(((select	count(*)
				from		pep_ritual_item_sequencia b
				where		b.nr_seq_item = nr_seq_item_p
				and			b.nr_seq_ritual = nr_seq_ritual_w) > 0)
				and			((select	max(c.nr_seq_apresent)
							from		pep_ritual_item_sequencia c
							where		c.nr_seq_item = nr_seq_item_p
							and			c.nr_seq_apresent is not null
							and			c.nr_seq_ritual = nr_seq_ritual_w) < a.nr_seq_apresent));
									 									 		
		if (nr_seq_item_w is not null) then
			ds_retorno_w := to_char(nr_seq_item_w);

		end if;

  end if;

  if (nr_seq_acao_w = 39) then
    ds_retorno_w := ds_retorno_w || '#SEND_MAIL';
	end if;

end if;

return ds_retorno_w;

end get_next_item_ritual;
/