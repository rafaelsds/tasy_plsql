create or replace
function obter_prioridade_triagem(nr_seq_queixa_p       number)
														return varchar2 is
								 
ds_retorno_w	varchar(10) := 'N';
qt_reg_w		number(10);

begin

select	count(*)
into 	qt_reg_w
from 	queixa_paciente_triagem;

if(qt_reg_w > 0) then 

	if(nr_seq_queixa_p is not null) then
	
		select 	nr_sequencia as cd
		into  	ds_retorno_w
		from 	triagem_classif_risco
		where 	nr_sequencia = (select nr_seq_classif from queixa_paciente_triagem where nr_seq_queixa = nr_seq_queixa_p);
		return	ds_retorno_w;

	end if;

end if;

return ds_retorno_w;

end obter_prioridade_triagem;
/