create or replace
function obter_cor_tipo_ocorrencia	(	cd_tipo_ocorrencia_p	varchar2)
 		    	return varchar2 is
ds_cor_w	varchar2(15);
begin

if	(cd_tipo_ocorrencia_p	is not null) then
	select	max(ds_cor)
	into	ds_cor_w
	from	tipo_ocorrencia_turno
	where	cd_tipo_ocorrencia	= cd_tipo_ocorrencia_p;
end if;

return	ds_cor_w;

end obter_cor_tipo_ocorrencia;
/