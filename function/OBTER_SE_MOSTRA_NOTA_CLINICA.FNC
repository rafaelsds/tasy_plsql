create or replace
function	obter_se_mostra_nota_clinica(ds_tipo_p 	  varchar2,
										 ie_tipo_nota_clinica_p varchar2) 
				return varchar2 is

ds_retorno_w 			varchar2(1);
nr_qtd_w				number;
nr_seq_config_w			number;
cd_pessoa_fisica_w  	varchar2(10);
cd_perfil_w 			number(10);
cd_setor_atendimento_w 	number(10);
cd_estabelecimento_w 	number(10);

Cursor C01 is
	select 	nr_sequencia
	from 	config_notas_clinicas
	where 	ie_tipo_nota_clinica = ie_tipo_nota_clinica_p
	and		(ie_evolucao_clinica = ds_tipo_p or ie_evolucao_clinica is null);

begin


cd_pessoa_fisica_w := obter_pf_usuario(wheb_usuario_pck.get_nm_usuario, 'C');
cd_perfil_w := wheb_usuario_pck.get_cd_perfil;
cd_setor_atendimento_w := wheb_usuario_pck.get_cd_setor_atendimento;
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;


select 	count(1)
into   	nr_qtd_w
from 	config_notas_clinicas;

if(nr_qtd_w > 0)then

	open C01;
	loop
	fetch C01 into	
		nr_seq_config_w;
	exit when C01%notfound;
	begin
		ds_retorno_w := 'S';
		
		select  count(1)
		into   	nr_qtd_w
		from 	config_notas_clinicas_lib
		where 	nr_seq_config = nr_seq_config_w;
		
		if (nr_qtd_w > 0) then
		
			select 	max(nvl('S','N'))
			into   	ds_retorno_w
			from 	config_notas_clinicas_lib
			where 	nr_seq_config = nr_seq_config_w
			and     nvl(cd_pessoa_fisica,	  nvl(cd_pessoa_fisica_w,0))	 = nvl(cd_pessoa_fisica_w,0)
			and		nvl(cd_perfil, 			  nvl(cd_perfil_w,0)) 			 = nvl(cd_perfil_w,0)
			and		nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
			and		nvl(cd_estabelecimento,   nvl(cd_estabelecimento_w,0))	 = nvl(cd_estabelecimento_w,0);
			
			if(ds_retorno_w = 'S')then
				return ds_retorno_w;
			end if;
			
		end if;
		
	end;
	end loop;
	close C01;
else
	ds_retorno_w := 'S';
end if;


if (ds_retorno_w is null) then
	ds_retorno_w := 'N';
end if;

return ds_retorno_w;

end obter_se_mostra_nota_clinica;
/