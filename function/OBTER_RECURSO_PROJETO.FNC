create or replace
function obter_recurso_projeto (
		nr_seq_projeto_p	number,
		nr_seq_tipo_equipe_p	number,
		nr_seq_tipo_papel_p	number,
		ie_funcao_migr_p	varchar2,
		ie_opcao_p		varchar2)
		return varchar2 is
		
nr_seq_recurso_w	number(10,0);
cd_recurso_w		varchar2(10);
ds_retorno_w		varchar2(60) := null;
		
begin
if	(nr_seq_projeto_p is not null) and
	(nr_seq_tipo_equipe_p is not null) and
	(nr_seq_tipo_papel_p is not null) then
	begin
	select	nvl(max(i.nr_sequencia),0)
	into	nr_seq_recurso_w
	from	proj_equipe e,
		proj_equipe_papel i
	where	e.nr_sequencia = i.nr_seq_equipe
	and	e.nr_seq_proj = nr_seq_projeto_p
	and	e.nr_seq_equipe_funcao = nr_seq_tipo_equipe_p
	and	i.nr_seq_funcao = nr_seq_tipo_papel_p
	and	((ie_funcao_migr_p is null) or (i.ie_funcao_rec_migr = ie_funcao_migr_p))
	and	i.cd_pessoa_fisica is not null
	and	nvl(i.ie_situacao,'A') = 'A';
	
	if	(nr_seq_recurso_w > 0) then
		begin
		select	max(cd_pessoa_fisica)
		into	cd_recurso_w
		from	proj_equipe_papel
		where	nr_sequencia = nr_seq_recurso_w;
		
		if	(cd_recurso_w is not null) then
			begin
			if	(ie_opcao_p = 'D') then
				begin
				ds_retorno_w := obter_nome_pf(cd_recurso_w);
				end;
			else
				begin
				ds_retorno_w := cd_recurso_w;
				end;
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;
return ds_retorno_w;
end obter_recurso_projeto;
/