create or replace
function Man_Obter_OS_Desenv
		(nr_seq_estagio_p			number)
 		return varchar2 is

ie_desenv_w	varchar2(05);
		
begin

select	max(IE_DESENV)
into	ie_desenv_w
from	man_estagio_processo
where 	NR_SEQUENCIA	= nr_seq_estagio_p;

return	ie_desenv_w;

end Man_Obter_OS_Desenv;
/