create or replace
function rxt_obter_se_campo_aplic(	nr_seq_agenda_p		number,
					nr_seq_campo_p		number,
					ie_opcao_p		varchar2)
					return varchar2 is

/*
F - Fase
B - Braquiterapia
*/
					
ie_aplicacao_w	varchar2(1) := 'N';

begin
if	(nr_seq_agenda_p is not null) and
	(nr_seq_campo_p is not null) then
	begin

	select	decode(count(*),0,'N','S')
	into	ie_aplicacao_w
	from	rxt_campo_aplic
	where	nr_seq_agenda	= nr_seq_agenda_p
	and	((nr_seq_campo	= nr_seq_campo_p and ie_opcao_p = 'F')
	or	(nr_seq_campo_braq = nr_seq_campo_p and ie_opcao_p = 'B'))
	and	ie_evento_aplic	= 'A'
	and	ie_evento_valido	= 'S';

	end;
end if;

return ie_aplicacao_w;

end rxt_obter_se_campo_aplic;
/