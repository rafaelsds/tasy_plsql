create or replace
function is_external_physician_valid(
		cd_pessoa_fisica_paciente_p		number,
		cd_medico_p	number)
		return varchar2 is

return_w varchar2(1);

begin

select nvl(max('S'), 'N')
into return_w
from PF_MEDICO_EXTERNO pme 
where pme.cd_pessoa_fisica = cd_pessoa_fisica_paciente_p 
and pme.cd_medico = cd_medico_p 
and ((pme.DT_FIM_VIGENCIA is not null and pme.DT_INICIO_VIGENCIA is not null and sysdate BETWEEN pme.DT_INICIO_VIGENCIA and pme.DT_FIM_VIGENCIA)
or (pme.DT_FIM_VIGENCIA is null and pme.DT_INICIO_VIGENCIA is not null and sysdate BETWEEN pme.DT_INICIO_VIGENCIA and sysdate + 1)
or (pme.DT_FIM_VIGENCIA is not null and pme.DT_INICIO_VIGENCIA is null and sysdate BETWEEN sysdate - 1 and pme.DT_FIM_VIGENCIA)
or (pme.DT_FIM_VIGENCIA is null and pme.DT_INICIO_VIGENCIA is null));

return	return_w;

end is_external_physician_valid;
/