create or replace
FUNCTION obter_dias_entre_alta_vig(	nr_atendimento_p 	number)
					return number is

qt_dias_internacao_w	number(10,4) := 0;
dt_final_w		date;
dt_alta_w		date;
ds_retorno_w		number(10);

begin

if	(nr_atendimento_p is not null) then
	
	select	max(dt_final_vigencia)
	into	dt_final_w
	from	atend_categoria_convenio 
	where	nr_atendimento = nr_atendimento_p;
	
	select	distinct
		nvl(Obter_data_alta_Atend(nr_atendimento),sysdate)
	into	dt_alta_w
	from	atend_categoria_convenio 
	where	nr_atendimento = nr_atendimento_p
	and	Obter_data_alta_Atend(nr_atendimento) is not null;
	
end if;
	
if 	(dt_alta_w > dt_final_w) then
	qt_dias_internacao_w	:= (dt_alta_w - dt_final_w);
	ds_retorno_w := lpad(to_char(trunc(qt_dias_internacao_w)),3,'0');
elsif 	(dt_final_w > dt_alta_w) then
	qt_dias_internacao_w	:= (dt_final_w - dt_alta_w); 
	ds_retorno_w :=  lpad(to_char(trunc(qt_dias_internacao_w)),3,'0');
end if;

RETURN	ds_retorno_w;

end obter_dias_entre_alta_vig;
/
