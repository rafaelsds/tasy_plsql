create or replace
function Obter_se_obriga_just_interacao(nr_prescricao_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w	varchar2(1):= 'N';
qt_registro_w	number(10) := 0;

begin

if	(ie_opcao_p = 'L') then
	begin
	select	count(*)
	into	qt_registro_w
	from	prescr_medica_interacao
	where	nr_prescricao	= nr_prescricao_p
	and	ie_severidade in ('L','M','S','I');
	end;
elsif	(ie_opcao_p = 'M') then
	begin
	select	count(*)
	into	qt_registro_w
	from	prescr_medica_interacao
	where	nr_prescricao	= nr_prescricao_p
	and	ie_severidade in ('M','S');
	end;
elsif	(ie_opcao_p = 'S') then
	begin
	select	count(*)
	into	qt_registro_w
	from	prescr_medica_interacao
	where	nr_prescricao	= nr_prescricao_p
	and	ie_severidade = 'S';
	end;
elsif	(ie_opcao_p = 'I') then
	begin
	select	count(*)
	into	qt_registro_w
	from	prescr_medica_interacao
	where	nr_prescricao	= nr_prescricao_p
	and	ie_severidade 	= 'I';
	end;
end if;

if	(qt_registro_w > 0) then
	ds_retorno_w	:= 'S';
end if;

return ds_retorno_w;

end Obter_se_obriga_just_interacao;
/
