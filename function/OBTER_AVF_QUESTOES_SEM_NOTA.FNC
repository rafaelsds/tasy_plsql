create or replace
function obter_avf_questoes_sem_nota(nr_seq_avf_resultado_p	number)
 		    	return number is
			
vl_retorno_w	number(10);
			
begin

select	count(1)
into	vl_retorno_w
from	avf_resultado a,
	avf_resultado_item b
where	b.nr_seq_resultado = a.nr_sequencia
and	b.nr_seq_pergunta is not null
and	to_number(substr(obter_dados_avf_tipo_nota_item(b.nr_seq_nota_item,decode(obter_valor_param_usuario(6,73,obter_perfil_ativo,obter_usuario_ativo,obter_estabelecimento_ativo),'N','N','P'),b.nr_seq_pergunta,b.nr_seq_resultado),1,10)) is null
and	a.nr_sequencia = nr_seq_avf_resultado_p;

return	vl_retorno_w;

end obter_avf_questoes_sem_nota;
/