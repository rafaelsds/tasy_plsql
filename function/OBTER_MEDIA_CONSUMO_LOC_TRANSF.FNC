create or replace
function obter_media_consumo_loc_transf(	cd_estabelecimento_p		number,
						cd_local_estoque_p		number,
						cd_material_p			number,
						ie_tipo_p			varchar2)
						return number is

qt_mes_consumo_w				number(2);
IE_MES_ATUAL_CONSUMO_w			varchar2(1);

dt_mesano_inicio_w				date;
dt_mesano_fim_w					date;

vl_retorno_w					number(18,4);
qt_consumo_w					number(18,4);
vl_consumo_w					number(18,2);

BEGIN
select 	max(qt_mes_consumo),
		max(IE_MES_ATUAL_CONSUMO)
into	qt_mes_consumo_w,
		IE_MES_ATUAL_CONSUMO_w
from 	PARAMETRO_COMPRAS
where	cd_estabelecimento = cd_estabelecimento_p;

if (IE_MES_ATUAL_CONSUMO_w = 'S') then
	dt_mesano_inicio_w 	:= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(sysdate,((qt_mes_consumo_w - 1) * -1), 0),'MONTH', 0);
	dt_mesano_fim_w		:= pkg_date_utils.start_of(sysdate,'MONTH',0);
else
	dt_mesano_inicio_w	:= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(sysdate,(qt_mes_consumo_w * -1), 0),'MONTH', 0);
	dt_mesano_fim_w		:= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(sysdate,-1, 0),'MONTH', 0);
end if;

select	nvl(sum(a.qt_estoque),0) qt_estoque,
		nvl(sum(a.vl_estoque),0) vl_consumo
into	qt_consumo_w,
		vl_consumo_w
from	movto_estoque_operacao_v a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and		a.cd_local_estoque	= nvl(cd_local_estoque_p, a.cd_local_estoque)
and		a.cd_material		= cd_material_p
and	pkg_date_utils.start_of(a.dt_mesano_referencia,'MONTH',0) between dt_mesano_inicio_w and dt_mesano_fim_w;

qt_consumo_w 	:= dividir(qt_consumo_w,qt_mes_consumo_w);
vl_consumo_w	:= dividir(vl_consumo_w,qt_mes_consumo_w);

if	(ie_tipo_p = 'Q') then
	vl_retorno_w := qt_consumo_w;
elsif	(ie_tipo_p = 'V') then
	vl_retorno_w := vl_consumo_w;
end if;

return  vl_retorno_w;

END obter_media_consumo_loc_transf;
/