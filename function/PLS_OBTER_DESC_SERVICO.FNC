create or replace
function pls_obter_desc_servico(	cd_procedimento_p	procedimento.cd_procedimento%type,
					ie_origem_proced_p	procedimento.ie_origem_proced%type)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se a conta se encaixa nos filtros espec�ficos de conta para
as ocorr�ncias combinadas de conta m�dica.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ds_retorno_w	procedimento.ds_procedimento%type;

begin
if	((cd_procedimento_p is not null) and (ie_origem_proced_p is not null)) then
	
	select	max(ds_procedimento)
	into	ds_retorno_w
	from	procedimento
	where	cd_procedimento 	= cd_procedimento_p
	and	ie_origem_proced 	= ie_origem_proced_p
	and	ie_classificacao 	= '2';
	
end if;

return	ds_retorno_w;

end pls_obter_desc_servico;
/
