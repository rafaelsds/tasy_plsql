create or replace
function obter_final_agendamento(	nr_seq_agenda_p	number)
					return date is

dt_final_agendamento_w	date;

begin

if	(nr_seq_agenda_p is not null) then

	select	hr_inicio +	(nr_minuto_duracao /1440)
	into 	dt_final_agendamento_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;

end if;

return dt_final_agendamento_w;

end obter_final_agendamento;
/