create or replace
function obter_se_lib_prescr_horario (
	dt_inicial_p    date,
	dt_final_p      date,
	dt_liberacao_p  date)
	return varchar2 is

dt_inicial_w    date;
dt_final_w      date;
ie_vigente_w    varchar2(1);

begin

if	(dt_inicial_p is not null) then
	dt_inicial_w	:= ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_liberacao_p, dt_inicial_p);
else 
	dt_inicial_w	:= null;
end if;

if	(dt_final_p is not null) then
	dt_final_w	:= ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_liberacao_p, dt_final_p);
else 
	dt_final_w	:= null;
end if;

if	(dt_final_w is not null) and
	(dt_inicial_w is not null) and
	(dt_liberacao_p between trunc(dt_liberacao_p) and dt_final_w) and 
	(to_char(dt_inicial_w,'hh24:mi:ss') > to_char(dt_final_w,'hh24:mi:ss')) then
	dt_inicial_w := dt_inicial_w - 1;
end if;

if	(dt_inicial_w is not null) and
	(dt_final_w is not null) and
	(to_char(dt_inicial_w,'hh24:mi:ss') > to_char(dt_final_w,'hh24:mi:ss')) then
	dt_final_w := dt_final_w + 1;
end if;

if	(dt_inicial_w is null) or
	(dt_inicial_w <= dt_liberacao_p) then
	ie_vigente_w := 'S';
else
	ie_vigente_w := 'N';
end if;


if	(ie_vigente_w = 'S') and
	((dt_final_w is null) or
	 (dt_final_w >= dt_liberacao_p)) then
	ie_vigente_w := 'S';
else
	ie_vigente_w := 'N';
end if;


return ie_vigente_w;
end obter_se_lib_prescr_horario;
/