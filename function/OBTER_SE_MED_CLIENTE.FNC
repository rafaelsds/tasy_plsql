create or replace
function obter_se_med_cliente( cd_pessoa_fisica_p	Varchar2)
 		    	return varchar2 is

qtd_w 		number(10)  := 0;
retorno_w		varchar2(1) := 'N';

begin

 select 	count(*)
 into	qtd_w 
 from 	med_cliente
 where 	cd_pessoa_fisica 	= cd_pessoa_fisica_p;
 
 if ( 	qtd_w > 0) then
	retorno_w := 'S';
 end if;
 
return	retorno_w;

end obter_se_med_cliente;
/