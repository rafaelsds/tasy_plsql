CREATE OR REPLACE
FUNCTION ucc_obter_se_seg_aprov_laudo	(nr_seq_propaci_p	number)
						RETURN	VARCHAR2 IS

ie_retorno_w		number(1);
nr_laudo_w		procedimento_paciente.nr_laudo%type;
cd_resp_seg_aprov_w	laudo_paciente.cd_resp_seg_aprov%type;
nm_usuario_seg_aprov_w	laudo_paciente.nm_usuario_seg_aprov%type;

/*	0 - Gera repasse
	1 - N�o gera
*/

begin

ie_retorno_w	:= 1;

select	max(nr_laudo)
into	nr_laudo_w
from	procedimento_paciente
where	nr_sequencia = nr_seq_propaci_p;

if	(nr_laudo_w is not null) then
	select	cd_resp_seg_aprov,
		nm_usuario_seg_aprov
	into	cd_resp_seg_aprov_w,
		nm_usuario_seg_aprov_w
	from	laudo_paciente
	where	nr_sequencia = nr_laudo_w;
	
	if	((cd_resp_seg_aprov_w is not null) or
		(nm_usuario_seg_aprov_w is not null))then
		ie_retorno_w	:= 0;
	else	
		ie_retorno_w	:= 1;
	end if;
end if;

RETURN	ie_retorno_w;

END	ucc_obter_se_seg_aprov_laudo;
/