create or replace
function	obter_result_med_item_avaliar(	nr_sequencia_p	number)
					return varchar2 is

ie_resultado_w	varchar2(2) := '';

begin

if	(nvl(nr_sequencia_p,0) > 0) then

	select	max(ie_resultado)
	into	ie_resultado_w
	from	med_item_avaliar
	where	nr_sequencia = nr_sequencia_p;

end if;

return	ie_resultado_w;

end obter_result_med_item_avaliar;
/