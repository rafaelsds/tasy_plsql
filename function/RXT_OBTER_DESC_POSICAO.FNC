create or replace
function rxt_obter_desc_posicao( nr_seq_posicao_p 		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(80);

begin
if	(nr_seq_posicao_p is not null) then

	select	max(ds_posicao)
	into	ds_retorno_w
	from	rxt_posicao
	where	nr_sequencia = nr_seq_posicao_p;

end if;
return	ds_retorno_W;

end rxt_obter_desc_posicao;
/