create or replace
function Ageint_Obter_Orient_Medico(
				cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(4000);
			
begin

select	substr(max(ds_orientacao_medico),1,4000)
into	ds_retorno_w
from	medico
where	cd_pessoa_Fisica	= cd_pessoa_fisica_p;

return	ds_retorno_w;

end Ageint_Obter_Orient_Medico;
/