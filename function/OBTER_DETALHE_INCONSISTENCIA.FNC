create or replace
function obter_detalhe_inconsistencia(cd_inconsistencia_p number,
				      nr_interno_conta_p  number)
 		    	return varchar2 is

ds_retorno_w		varchar2(2000);	
ds_proc_mat_erro_w	varchar2(2000);
ie_codigo_convenio_w	varchar2(1);
cd_convenio_mat_w	number(5);
cd_tipo_acomodacao_w	number(10);
ie_classificacao_w	varchar2(05);
cd_estabelecimento_w	number(10,0);
dia_feriado_w		number(3)	:= 0;
ie_feriado_w		varchar2(1)	:= 'N';
qt_conversao_mat_conv_w	number(10);
ie_tipo_atendimento_w	number(3)   	:= 0;
ie_tipo_material_w	varchar2(3);
ds_tipo_w		varchar2(10);
nr_atendimento_w	conta_paciente.nr_atendimento%type;

Cursor C01 is
	select	'('||ie_proc_mat||cd_item||') ' 
	from (	select 	to_char(a.cd_item) cd_item, 
			max(decode(a.ie_proc_mat,1,'P.','M.')) ie_proc_mat
		from	conta_paciente_consiste_v a,
			conta_paciente b
		where	a.nr_interno_conta  = nr_interno_conta_p
		and 	a.nr_interno_conta = b.nr_interno_conta
		and	nvl(a.nr_seq_proc_pacote,a.nr_sequencia) = a.nr_sequencia
		and 	b.ie_cancelamento is null
		and	a.cd_motivo_exc_conta is null
		and	a.vl_item < 0
		group by a.cd_item);
		
Cursor C02 is	
	select	'('||ie_proc_mat||cd_item||') ' 
	from (	select 	to_char(a.cd_item) cd_item, 
			max(decode(a.ie_proc_mat,1,'P.','M.')) ie_proc_mat
		from	conta_paciente_consiste_v  a,
			conta_paciente b
		where	a.nr_interno_conta = nr_interno_conta_p
		and 	a.nr_interno_conta = b.nr_interno_conta
		and	nvl(a.nr_seq_proc_pacote,a.nr_sequencia) = a.nr_sequencia
		and 	b.ie_cancelamento is null
		and	a.cd_motivo_exc_conta is null
		group by a.cd_item
		having sum(nvl(a.vl_unitario * a.qt_item,0)) < 0);
		
Cursor C03 is
	select	a.cd_material
	from	material_atend_paciente a,
		material c,
		estrutura_material_v b
	where	a.cd_material = c.cd_material
	and	a.cd_material = b.cd_material
	and	c.ie_tipo_material in (1,5)
	and	b.cd_grupo_material <> 15
	and	not exists (	select	1
				from	conversao_material_convenio x
				where	a.cd_material = x.cd_material
				and	x.cd_convenio = cd_convenio_mat_w
				and	a.dt_atendimento between nvl(x.dt_inicio_vigencia, a.dt_atendimento) and nvl(x.dt_final_vigencia, a.dt_atendimento)
				and	x.ie_situacao = 'A')
	and	a.nr_interno_conta = nr_interno_conta_p
	and	a.cd_motivo_exc_conta is null;
		
cursor C04 is
	select	a.cd_grupo_material cd_grupo,
		a.cd_subgrupo_material cd_subgrupo,
		a.cd_classe_material cd_classe,
		a.ie_tipo_material ie_tipo_mat,
		a.cd_material,
		b.dt_atendimento,
		b.cd_setor_atendimento,
		b.cd_convenio,
		nvl(b.cd_cgc_fornecedor,'0') cd_cgc,
		b.cd_categoria,
		b.nr_seq_atepacu,
		b.nr_sequencia
	from 	estrutura_material_v a,
		material_atend_paciente b
	where 	a.cd_material = b.cd_material
	and	b.nr_interno_conta = nr_interno_conta_p;

cursor c05 is
	select 	a.cd_material,
		1 ie_tipo
	from 	material_atend_paciente a,
		material b
	where 	a.nr_interno_conta = nr_interno_conta_p
	and 	a.cd_material = b.cd_material
	and	nvl(cd_material_tuss,0) = 0  
	and	cd_motivo_exc_conta is null
	and 	b.ie_tipo_material in (1,5,7,10)
	union all
	select 	a.cd_material,
		2 ie_tipo
	from 	material_atend_paciente a,
		material b
	where 	a.nr_interno_conta = nr_interno_conta_p
	and 	a.cd_material = b.cd_material
	and	nvl(cd_material_tuss,0) = 0  
	and	cd_motivo_exc_conta is null
	and 	b.ie_tipo_material in (0,2,3,4,8,9)
	order by 2;
	
c04_w	c04%rowtype;
c05_w	c05%rowtype;

Cursor C06 is
	select '('||cd_item|| ') '
	from	Conta_Paciente_consiste_v
	where	nr_interno_conta = nr_interno_conta_p
	and	nr_doc_convenio is null;

Cursor C07 is
	select	a.cd_procedimento
	from	procedimento_paciente a
	where	a.nr_interno_conta	= nr_interno_conta_p
	and	a.cd_motivo_exc_conta is null
	and	obter_classificacao_proced(cd_procedimento,ie_origem_proced,'C')	= '1'
	and	nvl(a.cd_medico_executor,a.cd_pessoa_fisica) is null;

Cursor C08 is
	select '('||to_char(b.cd_item)||') '
	from	Conta_Paciente_consiste_v b,
			conta_paciente a
	where	b.dt_conta not between a.dt_periodo_inicial and dt_periodo_final
	and		b.nr_interno_conta = nr_interno_conta_p
	and		a.nr_interno_conta = nr_interno_conta_p
	and		b.cd_motivo_exc_conta is null;
	
Cursor C09 is
	select '('||to_char(b.cd_item)||') '
	from	atendimento_paciente a,
			Conta_Paciente_consiste_v b
	where	b.nr_atendimento = a.nr_atendimento
	and	b.dt_conta not between a.dt_entrada and nvl(a.dt_alta,sysdate)
	and	b.nr_atendimento = nr_atendimento_w
	and	b.nr_interno_conta = nr_interno_conta_p
	and	b.cd_motivo_exc_conta is null;
	
begin

ds_retorno_w:= '';

if	(nr_interno_conta_p = 0) then
	goto final;
end if;
	
if	(cd_inconsistencia_p = 2254) then
	
	--ds_retorno_w:= 'Existem itens com valores negativos na conta' ||chr(13) || chr(10);
	ds_retorno_w:= wheb_mensagem_pck.get_texto(308686) ||chr(13) || chr(10);
	open C01;
	loop
	fetch C01 into	
		ds_proc_mat_erro_w;
	exit when C01%notfound;
		begin
		ds_retorno_w:= ds_retorno_w || ds_proc_mat_erro_w || chr(13) || chr(10);
		end;
	end loop;
	close C01;
	
end if;

if	(cd_inconsistencia_p = 22) then
		
	Consiste_Valor_Conta(nr_interno_conta_p, ds_proc_mat_erro_w, 'S');	
	--ds_retorno_w:= 'Existem Procedimentos/Materiais Com Preco Zerado' ||chr(13) || chr(10);
	ds_retorno_w:= wheb_mensagem_pck.get_texto(308688) ||chr(13) || chr(10);
	ds_retorno_w:= ds_retorno_w || ds_proc_mat_erro_w;
	
end if;

if	(cd_inconsistencia_p = 25) then
	
	--ds_retorno_w:= 'Existem Procedimentos/Materiais com saldo negativo' ||chr(13) || chr(10);
	ds_retorno_w:= wheb_mensagem_pck.get_texto(308690) ||chr(13) || chr(10);
	open C02;
	loop
	fetch C02 into	
		ds_proc_mat_erro_w;
	exit when C02%notfound;
		begin
		ds_retorno_w:= ds_retorno_w || ds_proc_mat_erro_w || chr(13) || chr(10);
		end;
	end loop;
	close C02;
	
end if;

if	(cd_inconsistencia_p in (2245,2246)) then

	select 	max(a.cd_convenio_parametro),
		max(a.cd_estabelecimento),
		max(b.ie_tipo_atendimento)
	into	cd_convenio_mat_w,
		cd_estabelecimento_w,
		ie_tipo_atendimento_w
	from 	conta_paciente a,
		atendimento_paciente b
	where 	a.nr_interno_conta = nr_interno_conta_p
	and 	a.nr_atendimento = b.nr_atendimento;

	select 	max(ie_codigo_convenio)
	into	ie_codigo_convenio_w
	from 	convenio
	where 	cd_convenio = cd_convenio_mat_w;
	
	open C04;
	loop
	fetch C04 into	
		c04_w;
	exit when C04%notfound;
		begin
		
		select	max(cd_tipo_acomodacao)
		into	cd_tipo_acomodacao_w
		from	atend_paciente_unidade
		where	nr_seq_interno	= c04_w.nr_seq_atepacu;
		
		begin
		select	max(ie_classificacao)
		into	ie_classificacao_w
		from	tipo_acomodacao
		where	cd_tipo_acomodacao	= cd_tipo_acomodacao_w;
		exception
			when others then
				ie_classificacao_w	:= null;
		end;
		
		
				
		select 	count(*)
		into 	dia_feriado_w
		from 	feriado
		where 	cd_estabelecimento = nvl(cd_estabelecimento_w, cd_estabelecimento)
		and	to_char(dt_feriado,'dd/mm/yyyy')  = to_char(c04_w.dt_atendimento,'dd/mm/yyyy');
		
		if	(dia_feriado_w > 0) then
			ie_feriado_w:= 'S';
		else
			ie_feriado_w:= 'N';
		end if;
					
		if	(ie_codigo_convenio_w = 'I') then
		
			select 	count(*)
			into	qt_conversao_mat_conv_w
			from 	mat_atend_pac_convenio a,
				material_atend_paciente b
			where 	a.nr_seq_material = b.nr_sequencia
			and 	b.nr_interno_conta = nr_interno_conta_p
			and 	b.nr_sequencia = c04_w.nr_sequencia;
			
		else
			
			select	count(*)
			into	qt_conversao_mat_conv_w
			from	conversao_material_convenio
			where	nvl(cd_material, c04_w.cd_material) 			= c04_w.cd_material
			and	(nvl(cd_categoria, c04_w.cd_categoria)			= c04_w.cd_categoria or c04_w.cd_categoria = '0')
			and	nvl(cd_grupo_material, c04_w.cd_grupo)			= c04_w.cd_grupo
			and	nvl(cd_subgrupo_material, c04_w.cd_subgrupo)		= c04_w.cd_subgrupo
			and	nvl(cd_classe_material, c04_w.cd_classe)		= c04_w.cd_classe
			and	nvl(ie_tipo_material,c04_w.ie_tipo_mat) 		= c04_w.ie_tipo_mat
			and	(nvl(cd_setor_atendimento, c04_w.cd_setor_atendimento)	= c04_w.cd_setor_atendimento)
			and	(nvl(cd_cgc, c04_w.cd_cgc)				= c04_w.cd_cgc)
			and	(nvl(ie_classif_acomod, nvl(ie_classificacao_w, 0))	= nvl(ie_classificacao_w, 0)) 
			and	(nvl(cd_estabelecimento, cd_estabelecimento_w)		= cd_estabelecimento_w)
			and	((c04_w.dt_atendimento is null) or (c04_w.dt_atendimento between nvl(dt_inicio_vigencia, c04_w.dt_atendimento) 
														and nvl(dt_final_vigencia, c04_w.dt_atendimento) + 86399/89400))
			and	cd_convenio = cd_convenio_mat_w
			and	ie_situacao = 'A'
			and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,0)
			and	((nvl(ie_feriado,'A') = ie_feriado_w ) or (nvl(ie_feriado,'A')	='A'));
		end if;
								
		select	max(ie_tipo_material)
		into	ie_tipo_material_w
		from	material
		where	cd_material = c04_w.cd_material;
		
		
		if	((cd_inconsistencia_p = 2245) and (ie_tipo_material_w in (1,5))) then
			if	(qt_conversao_mat_conv_w = 0) then
				ds_retorno_w:= ds_retorno_w || c04_w.cd_material || chr(13) || chr(10);
			end if;
		end if;
		
		if	((cd_inconsistencia_p = 2246) and (ie_tipo_material_w in (2,3,4))) then
			if	(qt_conversao_mat_conv_w = 0) then
				ds_retorno_w:= ds_retorno_w || c04_w.cd_material || chr(13) || chr(10);
			end if;
		end if;		
		
		end;
	end loop;
	close C04;
		
end if;

if	(cd_inconsistencia_p = 2244) then
	
	select 	max(a.cd_convenio_parametro)
	into	cd_convenio_mat_w
	from 	conta_paciente a,
		atendimento_paciente b
	where 	a.nr_interno_conta = nr_interno_conta_p
	and 	a.nr_atendimento = b.nr_atendimento;
	
	ds_retorno_w:= '';
	open C03;
	loop
	fetch C03 into	
		ds_proc_mat_erro_w;
	exit when C03%notfound;
		begin
		ds_retorno_w:= ds_retorno_w || ds_proc_mat_erro_w || chr(13) || chr(10);
		end;
	end loop;
	close C03;	
	
end if;



if	(cd_inconsistencia_p = 2261) then

	ds_retorno_w:= '';	
	
	open C05;
	loop
	fetch C05 into	
		c05_w;
	exit when C05%notfound;
		begin
		
		if	(c05_w.ie_tipo = 1) then
			ds_tipo_w   := Wheb_mensagem_pck.get_texto(309274); --'Mat: ';
			ds_retorno_w:= ds_retorno_w || ds_tipo_w || c05_w.cd_material || chr(13) || chr(10);
		elsif	(c05_w.ie_tipo = 2) then
			ds_tipo_w   := Wheb_mensagem_pck.get_texto(309275); --'Med: ';
			ds_retorno_w:= ds_retorno_w || ds_tipo_w || c05_w.cd_material || chr(13) || chr(10);
		end if;
			
		end;
	end loop;
	close C05;
	
/*
   sql_mat := '1,5,7,10';
   sql_med := '0,2,3,4,8,9';
*/

end if;

if	(cd_inconsistencia_p = 23) then
	begin
	open C06;
	loop
	fetch C06 into	
		ds_proc_mat_erro_w;
	exit when C06%notfound;
		begin
		ds_retorno_w:= ds_retorno_w || ds_proc_mat_erro_w || chr(13) || chr(10);
		end;
	end loop;
	close C06;
	end;
end if;

if	(cd_inconsistencia_p = 29) then
	begin
	open C07;
	loop
	fetch C07 into	
		ds_proc_mat_erro_w;
	exit when C07%notfound;
		begin 
		ds_retorno_w:= ds_retorno_w || '(' || Wheb_mensagem_pck.get_texto(309276) || ':' /*'(Proc:'*/||ds_proc_mat_erro_w||')' || chr(13) || chr(10);
		end;
	end loop;
	close C07;
	end;
end if;

if	(cd_inconsistencia_p = 21) then
	begin
	open C08;
	loop
	fetch C08 into	
		ds_proc_mat_erro_w;
	exit when C08%notfound;
		begin 
		ds_retorno_w:= ds_retorno_w || ds_proc_mat_erro_w || chr(13) || chr(10);
		end;
	end loop;
	close C08;
	end;
end if;

if	(cd_inconsistencia_p = 20) then
	begin
	
	select 	max(nr_atendimento)
	into 	nr_atendimento_w
	from 	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;
	
	open C09;
	loop
	fetch C09 into	
		ds_proc_mat_erro_w;
	exit when C09%notfound;
		begin 
		ds_retorno_w:= ds_retorno_w || ds_proc_mat_erro_w || chr(13) || chr(10);
		end;
	end loop;
	close C09;
	end;
end if;



<< final >>

return	ds_retorno_w;

end obter_detalhe_inconsistencia;
/
