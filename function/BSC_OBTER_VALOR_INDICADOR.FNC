create or replace
function bsc_obter_valor_indicador(	nr_seq_ind_inf_p	number,
				nr_seq_formula_p	number)
				return number is

cd_ano_w			number(04,0);
cd_periodo_w			number(02,0);					
nr_seq_indicador_w			number(10,0);
qt_real_w				number(15,2);

BEGIN
/* Indicador de refer�ncia*/
select	nr_seq_ind_ref
into	nr_seq_indicador_w
from	bsc_regra_formula
where	nr_sequencia	= nr_seq_formula_p;

/* Ano/per�odo que est� sendo calculado */
select	cd_ano,
	cd_periodo
into	cd_ano_w,
	cd_periodo_w
from	bsc_ind_inf
where	nr_sequencia 	= nr_seq_ind_inf_p;

/*Quantidade Real no per�odo do Indicador de Referencia que est� sendo utilizado para comparar os valores*/
select	nvl(max(qt_real),0)
into	qt_real_w
from	bsc_ind_inf
where	nr_seq_indicador	= nr_seq_indicador_w
and	cd_ano		= cd_ano_w
and	cd_periodo	= cd_periodo_w;

return	qt_real_w;

END bsc_obter_valor_indicador;
/