create or replace
function reg_is_user_reviewer (	nr_seq_revision_p	number,
				nm_usuario_p		varchar2,
				ie_tipo_revisor_p	varchar2 )
 		    	return varchar2 is

nm_author_w			reg_doc_revisor.nm_usuario_revisor%type;
ie_user_reviewer_w		varchar2(1);

begin

if (ie_tipo_revisor_p = 'C') then

	select	reg_fetch_revision_author(nr_seq_revision_p)
	into	nm_author_w
	from	dual;

	select	decode(count(1), 0, 'N', 'S')
	into	ie_user_reviewer_w
	from	reg_version_revision
	where	nr_sequencia = nr_seq_revision_p
	and	(nm_author_w = nm_usuario_p or reg_doc_has_user_delegation(nr_seq_documento, 'C', nm_author_w, nm_usuario_p) = 'S');

else

	select	decode(count(1), 0, 'N', 'S')
	into	ie_user_reviewer_w
	from	reg_version_revision vr,
		reg_version_approvers va
	where	vr.nr_sequencia = va.nr_seq_revision
	and	vr.nr_sequencia = nr_seq_revision_p
	and	(va.nm_approver = nm_usuario_p or reg_doc_has_user_delegation(vr.nr_seq_documento, ie_tipo_revisor_p, va.nm_approver, nm_usuario_p) = 'S')
	and	va.ie_approver_type = ie_tipo_revisor_p;

end if;

return ie_user_reviewer_w;

end reg_is_user_reviewer;
/
