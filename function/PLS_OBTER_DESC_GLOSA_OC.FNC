create or replace
function pls_obter_desc_glosa_oc
			(	cd_motivo_glosa_oc_p	varchar2,
				ie_tipo_p		varchar2)
				return varchar2 is
				
ds_retorno_w		varchar2(255);

begin

if	(ie_tipo_p = 'G') then

ds_retorno_w := substr(tiss_obter_motivo_glosa(pls_obter_seq_motivo_glosa(cd_motivo_glosa_oc_p),'D'),1,255);

elsif	(ie_tipo_p = 'O') then

select	max(ds_ocorrencia)
into	ds_retorno_w
from	pls_ocorrencia
where	upper(cd_ocorrencia) = upper(cd_motivo_glosa_oc_p);

end if;

return	ds_retorno_w;

end pls_obter_desc_glosa_oc;
/