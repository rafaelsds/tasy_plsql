create or replace
function USJCCT_Trata_Decimais_Laudo( ds_campo_p    varchar2)
     				return float is

ds_campo_w    	varchar2(4000);
cd_campo_w    	float(38) := 0;
i     			integer;

begin
ds_campo_w		:= '';
if	(ds_campo_p is null) or
	(ds_campo_p = '') then
	cd_campo_w	:= 0;
else
	begin
	for i in 1..length(ds_campo_p) loop
 		begin
	 	if 	(substr(ds_campo_p, i, 1) in
			('0','1','2','3','4','5','6','7','8','9','.',',')) then
  			ds_campo_w := ds_campo_w || substr(ds_campo_p,i,1);
		end if;
		end;
	end loop;
	if	(ds_campo_w is null) or
		(ds_campo_w = '') then
		cd_campo_w		:= 0;
	else
		cd_campo_w := ds_campo_w;
	end if;
	end;
end if;
return cd_Campo_w;
end USJCCT_Trata_Decimais_Laudo;
/