create or replace
function Setor_u(	dt_i_p				date,
						dt_f_p				date,
						nm_usuario_p		Varchar2) 
						return varchar2 is

cd_setor_atend_atual_w	varchar2(4000);
						
begin

select	Obter_Setor_Atual_Usuario(nm_usuario_p, 'USUARIO', dt_i_p, dt_f_p)
into	cd_setor_atend_atual_w
from	dual;

return	cd_setor_atend_atual_w;

end Setor_u;
/