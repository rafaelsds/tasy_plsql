create or replace
function INTPD_OBTER_REG_ALTER_CAMPO
		(nr_seq_fila_p	number,
		nm_tabela_p	varchar2,
		nm_atributo_p	varchar2,
		ie_tipo_valor_p	varchar2)
		return	varchar2 is

ds_retorno_w	varchar2(4000);

cursor c01 is
select	b.nr_sequencia,
	b.ds_valor_new,
	b.ds_valor_old
from	intpd_reg_alter_fila a,
	intpd_reg_alter_campo b,
	intpd_reg_alteracao c
where	a.nr_seq_reg_alter 	= b.nr_sequencia
and	a.nr_seq_reg_alter 	= c.nr_sequencia
and	a.nr_seq_fila 		= nr_seq_fila_p
and	b.nm_atributo 		= nm_atributo_p
and	c.nm_tabela 		= nm_tabela_p
order by b.nr_sequencia asc;

c01_w	c01%rowtype;

begin	

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	if	(ie_tipo_valor_p = 'NEW') then
		ds_retorno_w	:= c01_w.ds_valor_new;
	elsif	(ie_tipo_valor_p = 'OLD') then
		ds_retorno_w	:= c01_w.ds_valor_old;
	end if;

end loop;
close C01;

return	ds_retorno_w;

end INTPD_OBTER_REG_ALTER_CAMPO;
/