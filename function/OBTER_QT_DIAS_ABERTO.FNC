create or replace
function obter_qt_dias_aberto(dt_inicio_p	date,
				cd_estabelecimento_p	varchar2,
				ie_status_p     varchar2)
				return number is
				
qt_dias_w		varchar2(60) := 0;
qt_var_w		number(5);
qt_dias_func_w		number(5)    := 0;
horas_atend_w		varchar2(10);
horas_atual_w		varchar2(10);
		
begin

if((ie_status_p = 'A') or (ie_status_p = 'G') or (ie_status_p = 'P')) then


	while (qt_dias_func_w + trunc(dt_inicio_p) <= trunc(sysdate)) loop
	  
		select	decode((OBTER_SE_DIA_UTIL((trunc(dt_inicio_p) + qt_dias_func_w), cd_estabelecimento_p)), 'S', 1, 0)
		into 	qt_var_w
		from	dual;
		
		qt_dias_w := qt_dias_w + qt_var_w;
		qt_dias_func_w := qt_dias_func_w + 1;
	end loop;
	  
	select	to_char(dt_inicio_p, 'hh24:mi:ss'),
		to_char(sysdate, 'hh24:mi:ss')
	into	horas_atend_w,
		horas_atual_w
	from	dual;

	if (horas_atend_w < horas_atual_w) then
		qt_dias_w := qt_dias_w - 1;
	else
		qt_dias_w := qt_dias_w - 2;
	end if;

	return	qt_dias_w;

end if;
	
return	null;

end obter_qt_dias_aberto;
/