create or replace
function obter_se_inf_autorizada(cd_tipo_informacao_p in varchar2)
 		    	return varchar2 is

qt_controle_w number(1);

begin

select	count(*)
into	qt_controle_w
from	autorizacao_inf_cliente
where	cd_tipo_informacao = cd_tipo_informacao_p;

if	(qt_controle_w > 0 ) then
	return 'S';
else
	return 'N';
end if;

end obter_se_inf_autorizada;
/