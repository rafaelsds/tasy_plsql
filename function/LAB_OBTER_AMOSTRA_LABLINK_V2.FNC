create or replace
function Lab_obter_amostra_lablink_v2
				(ds_valor_p 	varchar2,
				nr_prescricao_p number,
				nr_seq_exame_p	number,
				nr_seq_prescr_p	number)
				return varchar2 is

nr_seq_grupo_w		       number(10);
nr_seq_grupo_imp_w	     number(10);
nr_seq_lab_w		         varchar2(20);
dt_liberacao_W		       date;
dt_liberacao_medico_w	   date;
dt_prescricao_w		       date;
nr_prescricao_w		       number(14);
ds_resultado_w		       varchar(30);
nr_seq_mat_w		         number(10);
ds_seq_mat_w		         varchar(10);
nr_identificador_w	     number(10);
nr_seq_barras_w		       number(10);
nr_seq_mat_result_w	     number(10);
dt_prior_codigo_barras_w varchar2(2);

begin
select	max(c.nr_seq_grupo),
	max(c.nr_seq_grupo_imp),
	nvl(max(a.nr_seq_lab),0),
	max(b.dt_liberacao),
	max(b.dt_prescricao),
	max(b.dt_liberacao_medico),
	max(a.nr_prescricao),
	max(d.nr_sequencia)
into	nr_seq_grupo_w,
	nr_seq_grupo_imp_w,
	nr_seq_lab_w,
	dt_liberacao_W,
	dt_prescricao_w,
	dt_liberacao_medico_w,
	nr_prescricao_w,
	nr_seq_mat_w	
from 	material_exame_lab d,
	exame_laboratorio c,
	prescr_medica b,
	prescr_procedimento a
where	b.nr_prescricao = a.nr_prescricao
and	d.cd_material_exame = a.cd_material_exame
and	c.nr_seq_exame	= a.nr_seq_exame
and	a.nr_prescricao = nr_prescricao_p
and	a.nr_sequencia  = nr_seq_prescr_p
and	a.nr_seq_exame	= nr_seq_exame_p;

select	max(a.nr_identificador)
into	nr_identificador_w
from	empresa_integr_dados a,
	empresa_integracao b,
	estabelecimento c
where	a.nr_seq_empresa_integr = b.nr_sequencia
and	b.cd_cnpj = c.cd_cgc
and	c.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

ds_seq_mat_w := to_char(nr_seq_mat_w);

dt_prior_codigo_barras_w := lab_obter_valor_parametro(722,373);

if ((nr_seq_mat_w is not null) and (length(nr_seq_mat_w) < 2)) then
	ds_seq_mat_w := '0'||nr_seq_mat_w;
end if;

if	(ds_valor_p = 'PM') then
	ds_resultado_w := lpad(nr_prescricao_w || 'M' ||ds_seq_mat_w ,10,'0');
	
elsif	(ds_valor_p = 'PM11') then
	ds_resultado_w := lpad(nr_prescricao_w || 'M' || nr_seq_mat_w,11,'0');
elsif	(ds_valor_p = 'PM13') then
	ds_resultado_w := lpad(nr_prescricao_w || 'M' || nr_seq_mat_w,13,'0');
elsif	(ds_valor_p = 'PMR11') then
	select	nvl(max(d.nr_seq_material),nr_seq_mat_w)
	into	nr_seq_mat_result_w
	from 	exame_lab_result_item d,
			exame_lab_resultado c,
			prescr_medica b,
			prescr_procedimento a
	where	b.nr_prescricao = a.nr_prescricao
	and	b.nr_prescricao = c.nr_prescricao
	and	c.nr_seq_resultado = d.nr_seq_resultado
	and	d.nr_seq_prescr = a.nr_sequencia
	and	a.nr_prescricao = nr_prescricao_p
	and	a.nr_sequencia  = nr_seq_prescr_p
	and	a.nr_seq_exame	= nr_seq_exame_p;
	ds_resultado_w := lpad(nr_prescricao_w || 'M' || nr_seq_mat_result_w,11,'0');	
elsif	(ds_valor_p = 'P') then
	ds_resultado_w := nr_prescricao_w;

elsif	(ds_valor_p = 'DGS') then
	ds_resultado_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_grupo_w,2,'0') 			
			|| lpad(nr_seq_lab_w,3,'0');

elsif	(ds_valor_p = 'DGIS') then
	ds_resultado_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_grupo_imp_w,2,'0') 			
			|| lpad(nr_seq_lab_w,3,'0');

elsif	(ds_valor_p = 'DS4GI') then
	ds_resultado_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_lab_w,4,'0') 			
			|| lpad(nr_seq_grupo_imp_w,2,'0');
			
elsif	(ds_valor_p = 'DGSL') then
	ds_resultado_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'dd') || lpad(nr_seq_grupo_w,2,'0') 			
			|| lpad(nr_seq_lab_w,4,'0');

elsif	(ds_valor_p = 'DAGS4') then
      if (dt_prior_codigo_barras_w = 'LE') then
        ds_resultado_w := to_char(nvl(dt_liberacao_w,nvl(dt_liberacao_medico_w,dt_prescricao_w)),'ddmmyy') || lpad(nr_seq_grupo_w,2,'0') || lpad(nr_seq_lab_w,4,'0');
       else  
			  ds_resultado_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmmyy') || lpad(nr_seq_grupo_w,2,'0') || lpad(nr_seq_lab_w,4,'0');
      end if;          
elsif	(ds_valor_p = 'DGS7') then
	ds_resultado_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_grupo_w,2,'0') 			
			|| lpad(nr_seq_lab_w,7,'0');		

elsif	(ds_valor_p = 'DGS6') then
	ds_resultado_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_grupo_w,2,'0') 			
			|| lpad(nr_seq_lab_w,6,'0');	
			
elsif	(ds_valor_p = 'DGS5') then
	ds_resultado_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_grupo_w,2,'0') 			
			|| lpad(nr_seq_lab_w,6,'0');	
end if;

return ds_resultado_w;

end;
/