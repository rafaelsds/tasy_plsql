create or replace function pls_obter_rodape_auditor_pf (	nr_seq_conta_p		pls_conta.nr_sequencia%type,
															cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type)
														return varchar2 is

ds_retorno_w			Varchar2(200);
qt_auditor_w			pls_integer;
nr_crm_auditor_w		medico.nr_crm%type;
nm_medico_auditor_w		pessoa_fisica.nm_pessoa_fisica%type;
cd_uf_crm_w				medico.uf_crm%type;
nr_coren_auditor_w		medico.nr_crm%type;
nm_enfer_auditor_w		pessoa_fisica.nm_pessoa_fisica%type;
cd_uf_coren_w			medico.uf_crm%type;

begin

select	count(1)
into	qt_auditor_w
from	pls_conta_auditor
where	nr_seq_conta = nr_seq_conta_p;

if	(qt_auditor_w > 0) then

	ptu_obter_dados_aud_analise_pf(	nr_seq_conta_p, cd_pessoa_fisica_p, nr_crm_auditor_w, nm_medico_auditor_w,
					cd_uf_crm_w, nr_coren_auditor_w, nm_enfer_auditor_w,
					cd_uf_coren_w);

	if	(nr_crm_auditor_w is not null) then
		ds_retorno_w := 'M�d.: ' || nm_medico_auditor_w || ' / CRM: ' ||  nr_crm_auditor_w;
	elsif	(nr_coren_auditor_w is not null) then
		ds_retorno_w := 'Enf.: ' || nm_enfer_auditor_w || ' / COREN: ' ||  nr_coren_auditor_w;
	end if;

end if;

return	ds_retorno_w;

end pls_obter_rodape_auditor_pf;
/
