create or replace
function Obter_se_Motivo_atend_Lib	(nr_seq_queixa_p   number,
									 ie_clinica_p	   number default 0,
									 nr_seq_classif_p  number default 0) return varchar2 is

				
qt_reg_w				number(10);
qt_reg_cl_w				number(10);
cd_perfil_w				number(10)	:= obter_perfil_ativo;
cd_setor_logado_w		number(10)	:= wheb_usuario_pck.get_cd_setor_atendimento;
begin
select	count(*)
into	qt_reg_w
from	QUEIXA_PACIENTE_LIB
where	nr_seq_queixa	= nr_seq_queixa_p;

qt_reg_cl_w := 0;

if (qt_reg_w = 0) then
	return 'S';
end if;

if (ie_clinica_p > 0) or (nr_seq_classif_p > 0) then
	select 	count(*)
	into	qt_reg_cl_w
	from 	QUEIXA_PACIENTE_LIB
	where	((ie_clinica is null) or (ie_clinica = ie_clinica_p))
	and		((nr_seq_classif is null) or (nr_seq_classif = nr_seq_classif_p))
	and    ((ie_clinica is not null) or (nr_seq_classif is not null));
end if;

if	(qt_reg_cl_w = 0) then

		select	count(*)
		into	qt_reg_w
		from	QUEIXA_PACIENTE_LIB
		where	nr_seq_queixa	= nr_seq_queixa_p
		and		nvl(cd_perfil,cd_perfil_w)	= cd_perfil_w
		and		nvl(cd_setor_atendimento,cd_setor_logado_w)	= cd_setor_logado_w;
	
	if	(qt_reg_w	= 0) then
		return 'N';
	end if;

elsif (qt_reg_cl_w > 0) then

	select	count(*)
	into	qt_reg_w
	from	QUEIXA_PACIENTE_LIB
	where	nr_seq_queixa	= nr_seq_queixa_p
	and		nvl(cd_perfil,cd_perfil_w)	= cd_perfil_w
	and		nvl(cd_setor_atendimento,cd_setor_logado_w)	= cd_setor_logado_w
	and		((ie_clinica is null) or (ie_clinica = ie_clinica_p))
	and		((nr_seq_classif is null) or (nr_seq_classif = nr_seq_classif_p))
	and    ((ie_clinica is not null) or (nr_seq_classif is not null));
	
	if	(qt_reg_w	= 0) then
		return 'N';
	end if;
	
end if;

return	'S';

end Obter_se_Motivo_atend_Lib;
/
