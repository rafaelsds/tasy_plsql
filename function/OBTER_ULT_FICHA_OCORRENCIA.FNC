create or replace
function obter_ult_ficha_ocorrencia(nr_atendimento_p		number)
 		    	return number is

nr_ficha_ocorrencia_w		number(10);
			
begin

select 	nvl(max(nr_ficha_ocorrencia),0)
into	nr_ficha_ocorrencia_w
from	cih_ficha_ocorrencia
where	nr_atendimento = nr_atendimento_p;

return	nr_ficha_ocorrencia_w;

end obter_ult_ficha_ocorrencia;
/