create or replace
function obter_media_atend_fila(nr_seq_fila_p	number,
			        ie_opcao_p	varchar2,
				dt_inicio_p	date,
				dt_fim_p	date)
 		    	return varchar2 is

nr_sequencia_w		number(10);
qt_total_tempo_atend_seg_w	number(10);
qt_total_senha_w	number(10);
qt_media_seg_w		number(10);
nr_horas_w		number(10);
qt_segundo_w 		number(10);
qt_minutos_w 		number(10);
ds_tempo_w		varchar2(11);
ds_tempo_media_w	varchar2(20);

Cursor C01 is
	select	nr_sequencia
	from	atendimentos_senha
	where	nr_seq_fila_espera = nr_seq_fila_p
	and	trunc(dt_inicio_atendimento) between trunc(dt_inicio_p) and trunc(dt_fim_p)
	and	dt_inicio_atendimento is not null
	and	dt_fim_atendimento is not null;
			
begin

qt_total_tempo_atend_seg_w := 0;
qt_total_senha_w := 0;
nr_horas_w := 0;
qt_minutos_w := 0;
qt_segundo_w := 0;


open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
		begin
			select  (OBTER_DIF_SEGUNDOS(dt_inicio_atendimento,dt_fim_atendimento))
			into	qt_segundo_w	
			from	atendimentos_senha
			where	nr_sequencia = nr_sequencia_w;

		exception
		when others then
			qt_segundo_w := 9999999999;
		end;
		
	qt_total_tempo_atend_seg_w := qt_total_tempo_atend_seg_w + qt_segundo_w;
	qt_total_senha_w := qt_total_senha_w + 1;
	end;
end loop;
close C01;

if	(nvl(qt_total_senha_w,0) > 0) then
	
	if	(ie_opcao_p = 'F') then
	
		qt_media_seg_w := (qt_total_tempo_atend_seg_w/qt_total_senha_w);
		
		while	(qt_media_seg_w >= 60) loop
			qt_minutos_w := qt_minutos_w + 1;
			qt_media_seg_w := qt_media_seg_w - 60;
		end loop;
		
		while	(qt_minutos_w >= 60) loop
			nr_horas_w := nr_horas_w + 1;
			qt_minutos_w := qt_minutos_w - 60;
		end loop;
		
		if (nr_horas_w < 100) then
			ds_tempo_w := lpad(nr_horas_w, 2, '0') || ':' || lpad(qt_minutos_w, 2, '0') || ':' || lpad(nvl(qt_media_seg_w,'00'), 2, '0');
		
		elsif (nr_horas_w < 1000) then
			ds_tempo_w := lpad(nr_horas_w, 3, '0') || ':' || lpad(qt_minutos_w, 2, '0') || ':' || lpad(nvl(qt_media_seg_w,'00'), 2, '0');
		
		elsif (nr_horas_w < 10000) then
			ds_tempo_w := lpad(nr_horas_w, 4, '0') || ':' || lpad(qt_minutos_w, 2, '0') || ':' || lpad(nvl(qt_media_seg_w,'00'), 2, '0');
		
		else
			ds_tempo_w := lpad(nr_horas_w, 5, '0') || ':' || lpad(qt_minutos_w, 2, '0') || ':' || lpad(nvl(qt_media_seg_w,'00'), 2, '0');
		end if;

		ds_tempo_media_w := ds_tempo_w;


	elsif (ie_opcao_p = 'N') then
	
		ds_tempo_media_w := round((qt_total_tempo_atend_seg_w/qt_total_senha_w) / 60,0);
	
	end if;
end if;

If	(ds_tempo_media_w is null) and 
	(ie_opcao_p = 'F') then
	ds_tempo_media_w := '00:00:00';
end if;

return	ds_tempo_media_w;

end obter_media_atend_fila;
/