create or replace 
function obter_nivel_prot
	(nr_interno_conta_p	number)
	return number is

ie_status_w			number(5);
ie_status_conta_w		Number(01,0);
nr_seq_protocolo_w	Number(10,0);

begin

select	ie_status_acerto,
	nr_seq_protocolo
into	ie_status_conta_w,
	nr_seq_protocolo_w
from 	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

if	(nvl(ie_status_conta_w,0) = 0) then
	select nr_seq_protocolo, 
		ie_status_acerto
	into 	nr_seq_protocolo_w,
		ie_status_conta_w
	from	conta_paciente a
	where a.nr_interno_conta = nr_interno_conta_p;
end if;

if	(ie_status_conta_w = 2) then
	if	(nr_seq_protocolo_w is null) then
		ie_status_w := '6';
	else
		select decode(ie_status_protocolo,1,'8','10')
		into ie_status_w
		from protocolo_convenio
		where nr_seq_protocolo = nr_seq_protocolo_w;
	end if;
else
	select decode(b.dt_alta,null,'2','4')
	into ie_status_w
	from	atendimento_paciente b,
		conta_paciente a
	where a.nr_atendimento = b.nr_atendimento
	  and a.nr_interno_conta = nr_interno_conta_p;
end if;

return somente_numero(ie_status_w);

end obter_nivel_prot;
/