create or replace
function obter_desc_grupo_sme(nr_sequencia_p	sme_equipe.nr_sequencia%type) return varchar2 is

ds_equipe_w	sme_equipe.ds_equipe%type;

begin

if	(nr_sequencia_p	is not null) then
	begin
		select	substr(ds_equipe, 1, 255)
		into	ds_equipe_w
		from	sme_equipe
		where	nr_sequencia = nr_sequencia_p;
	end;
end if;

return ds_equipe_w;

end obter_desc_grupo_sme;
/