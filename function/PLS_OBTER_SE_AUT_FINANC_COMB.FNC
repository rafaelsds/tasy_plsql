create or replace
function pls_obter_se_aut_financ_comb
			(	nr_seq_ocor_combinada_p		number,
				nr_seq_segurado_p		number,
				qt_dias_mensal_vencido_p	number,
				ie_cheque_devolvido_p		varchar2,
				ie_tipo_pagador_p		varchar2,
				ie_venc_titulo_p		varchar2,
				ie_pagador_complementar_p	pls_validacao_aut_fin.ie_pagador_complementar%type)
				return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se deve utilizar "Valida caracter�stica financeira"
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Consistencia de ocorr�ncia
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

ie_retorno_w			varchar2(1) := 'N';
qt_dias_vencido_w		number(10) := 0;
qt_dias_ret_pag_w		number(10) := 0;
qt_dias_ret_mens_w		number(10) := 0;
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
ie_tipo_pagador_w		varchar2(5);
ie_cheque_w			varchar2(1) := 'N';
ie_pagador_isento_w		varchar2(1) := 'N';
nr_titulo_w			titulo_receber.nr_titulo%type;
nr_titulo_mens_w		titulo_receber.nr_titulo%type;

Cursor C01 is
	select	nr_seq_pagador
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p
	and	nr_seq_pagador is not null
	union all
	select	c.nr_seq_pagador_item nr_seq_pagador
	from	pls_segurado a,
		pls_contrato_pagador b,
		pls_pagador_item_mens c
	where	b.nr_sequencia	= a.nr_seq_pagador
	and	b.nr_sequencia	= c.nr_seq_pagador
	and	a.nr_sequencia	= nr_seq_segurado_p
	and	ie_pagador_complementar_p = 'S';

begin

for r_c01_w in C01 loop
	begin
	
	-- OBTER O TIPO DO PAGADOR
	if	(nvl(ie_tipo_pagador_p,'A') <> 'A') then
		select	cd_cgc,
			cd_pessoa_fisica
		into	cd_cgc_w,
			cd_pessoa_fisica_w
		from	pls_contrato_pagador
		where	nr_sequencia	= r_c01_w.nr_seq_pagador;
		
		if	(cd_pessoa_fisica_w is null) then
			ie_tipo_pagador_w := 'PJ';
		else
			ie_tipo_pagador_w := 'PF';
		end if;
		
		ie_pagador_isento_w	:= pls_obter_se_pagador_isento(r_c01_w.nr_seq_pagador,cd_cgc_w,sysdate);
	end if;

	-- OBTER A QUANTIDADE DE DIAS DE VENCIMENTO
	if	(nvl(ie_venc_titulo_p,'V') = 'V') then
		select	max(sysdate - a.dt_pagamento_previsto),
			max(a.nr_titulo)
		into	qt_dias_ret_pag_w,
			nr_titulo_w
		from	titulo_receber	a
		where	a.ie_pls		= 'S'
		and	a.dt_liquidacao		is null
		and	a.dt_pagamento_previsto	< sysdate
		and	a.nr_seq_pagador	= r_c01_w.nr_seq_pagador
		and	a.ie_origem_titulo	= '3'
		and	a.ie_situacao		= '1'
		and	a.dt_pagamento_previsto	= (	select	min(c.dt_pagamento_previsto)
							from	titulo_receber c
							where	c.ie_pls		= 'S'
							and	c.dt_liquidacao		is null
							and	c.dt_pagamento_previsto	< sysdate
							and	c.nr_seq_pagador	= r_c01_w.nr_seq_pagador
							and	c.ie_origem_titulo	= '3'
							and	c.ie_situacao		= '1');

		-- Select adicional para buscar apenas os casos onde ainda n�o gravava o NR_SEQ_PAGADOR no t�tulo
		select	max(sysdate - a.dt_pagamento_previsto),
			max(a.nr_titulo)
		into	qt_dias_ret_mens_w,
			nr_titulo_mens_w
		from	pls_mensalidade b,
			titulo_receber	a
		where	a.nr_seq_mensalidade	= b.nr_sequencia
		and	a.ie_pls		= 'S'
		and	a.dt_liquidacao is null
		and	a.dt_pagamento_previsto < sysdate
		and	b.nr_seq_pagador	= r_c01_w.nr_seq_pagador
		and	a.ie_origem_titulo = '3'
		and	a.nr_seq_pagador is null
		and	a.ie_situacao = '1'
		and	a.dt_pagamento_previsto	= (	select	min(c.dt_pagamento_previsto)
							from	titulo_receber c,
								pls_mensalidade d
							where	c.nr_seq_mensalidade	= d.nr_sequencia
							and	c.ie_pls		= 'S'
							and	c.dt_liquidacao		is null
							and	c.dt_pagamento_previsto	< sysdate
							and	d.nr_seq_pagador	= r_c01_w.nr_seq_pagador
							and	c.ie_origem_titulo	= '3'
							and	c.nr_seq_pagador	is null
							and	c.ie_situacao		= '1');
	else
		select	max(sysdate - a.dt_vencimento),
			max(a.nr_titulo)
		into	qt_dias_ret_pag_w,
			nr_titulo_w
		from	titulo_receber	a
		where	a.ie_pls		= 'S'
		and	a.dt_liquidacao		is null
		and	a.dt_vencimento	< sysdate
		and	a.nr_seq_pagador	= r_c01_w.nr_seq_pagador
		and	a.ie_origem_titulo	= '3'
		and	a.ie_situacao		= '1'
		and	a.dt_vencimento	= (		select	min(c.dt_vencimento)
							from	titulo_receber c
							where	c.ie_pls		= 'S'
							and	c.dt_liquidacao		is null
							and	c.dt_vencimento	< sysdate
							and	c.nr_seq_pagador	= r_c01_w.nr_seq_pagador
							and	c.ie_origem_titulo	= '3'
							and	c.ie_situacao		= '1');

		-- Select adicional para buscar apenas os casos onde ainda n�o gravava o NR_SEQ_PAGADOR no t�tulo
		select	max(sysdate - a.dt_vencimento),
			max(a.nr_titulo)
		into	qt_dias_ret_mens_w,
			nr_titulo_mens_w
		from	pls_mensalidade b,
			titulo_receber	a
		where	a.nr_seq_mensalidade	= b.nr_sequencia
		and	a.ie_pls		= 'S'
		and	a.dt_liquidacao is null
		and	a.dt_vencimento < sysdate
		and	b.nr_seq_pagador	= r_c01_w.nr_seq_pagador
		and	a.ie_origem_titulo = '3'
		and	a.nr_seq_pagador is null
		and	a.ie_situacao = '1'
		and	a.dt_vencimento	= (		select	min(c.dt_vencimento)
							from	titulo_receber c,
								pls_mensalidade d
							where	c.nr_seq_mensalidade	= d.nr_sequencia
							and	c.ie_pls		= 'S'
							and	c.dt_liquidacao		is null
							and	c.dt_vencimento	< sysdate
							and	d.nr_seq_pagador	= r_c01_w.nr_seq_pagador
							and	c.ie_origem_titulo	= '3'
							and	c.nr_seq_pagador	is null
							and	c.ie_situacao		= '1');
	end if;
	
	if	(qt_dias_ret_pag_w > 0) then
		qt_dias_vencido_w	:= qt_dias_ret_pag_w;
		
		-- Tratamento para se achar por mensalidade com quantidade de dias maior, usar este
		if	(qt_dias_ret_mens_w > qt_dias_ret_pag_w) then
			nr_titulo_w		:= nr_titulo_mens_w;
			qt_dias_vencido_w	:= qt_dias_ret_mens_w;
		end if;
	end if;
	
	-- OBTER CHEQUE DEVOLVIDO
	if	(cd_pessoa_fisica_w is not null) then
		select	decode(count(1),0,'N','S')
		into	ie_cheque_w
		from	cheque_cr		a
		where	a.cd_pessoa_fisica 	= cd_pessoa_fisica_w
		and	obter_status_cheque(a.nr_seq_cheque) in (3,5,6,7,10)
		and	rownum <= 1;
	end if;
	
	if	((nvl(qt_dias_mensal_vencido_p,0) = 0) or ((nvl(qt_dias_vencido_w,0) >= nvl(qt_dias_mensal_vencido_p,0)) and (nvl(ie_pagador_isento_w,'N') = 'N'))) and
		((nvl(ie_cheque_devolvido_p,'N') = 'N') or ((ie_cheque_devolvido_p = ie_cheque_w) or (ie_cheque_devolvido_p is null)))  and
		((nvl(ie_tipo_pagador_p,'A') = 'A') or (ie_tipo_pagador_p = ie_tipo_pagador_w)) then
		return 'S' || to_char(nr_titulo_w);
	end if;
	end;
end loop; --C01

return	'N';

end pls_obter_se_aut_financ_comb;
/