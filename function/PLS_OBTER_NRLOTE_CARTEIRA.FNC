create or replace
function pls_obter_nrlote_carteira
		(	nr_seq_segurado_p	Number,
			ie_tipo_lote_p		Varchar)
 		    	return Number is
			
/* IE_TIPO_LOTE_P
	V - Vencimento
	E - Emissao
*/			
			
nr_retorno_w		Number(10);

begin

select	nvl(max(d.nr_sequencia),0)
into	nr_retorno_w
from	pls_lote_carteira	d,
	pls_carteira_emissao	c,
	pls_segurado_carteira	b,
	pls_segurado		a
where	a.nr_sequencia		= nr_seq_segurado_p
and	d.ie_tipo_lote		= ie_tipo_lote_p
and	a.nr_sequencia		= b.nr_seq_segurado
and	b.nr_sequencia		= c.nr_seq_seg_carteira
and	c.nr_seq_lote		= d.nr_sequencia;

return	nr_retorno_w;

end pls_obter_nrlote_carteira;
/
