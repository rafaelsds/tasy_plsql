create or replace
function OBTER_REGRA_CONV_PLANO_ATEND
		(nr_atendimento_p	in	number,
		nr_seq_procedimento_p	in	number,
		nr_seq_material_p	in	number,
		ie_opcao_p		in	varchar2) return varchar2 is

/*

ie_opcao_p
'1' - NR_SEQUENCIA da regra selecionada
'2' - IE_REGRA

*/

cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
cd_convenio_w			number(10,0);
cd_area_procedimento_w	regra_Convenio_Plano.cd_area_procedimento%type;
cd_especialidade_w		regra_convenio_plano.cd_especialidade_proc%type;
cd_grupo_proc_w			regra_Convenio_Plano.cd_grupo_proc%type;
ie_tipo_atendimento_w		number(10,0);
nr_atendimento_w		number(10,0);
cd_material_w			number(15,0);
cd_grupo_material_w		number(10,0);
cd_subgrupo_material_w		number(10,0);
cd_classe_material_w		number(10,0);
ds_retorno_w			varchar2(254);
cd_tipo_acomodacao_w		number(10,0);
cd_plano_w			varchar2(40);
nr_sequencia_w			number(10,0);
cd_setor_atendimento_w		number(10,0);
ie_regra_w			number(2,0);
ie_tipo_material_w		varchar2(3);
ie_classificacao_w		varchar2(3);
cd_simpro_w			number(15,0);

cursor c01 is
select	nr_sequencia,
	ie_regra
from	(
	select	nr_sequencia,
		ie_regra
	from 	regra_Convenio_Plano
	where	cd_convenio					= cd_convenio_w
	and	nvl(cd_plano, cd_plano_w)			= cd_plano_w
	and	nvl(cd_procedimento,cd_procedimento_w) 		= cd_procedimento_w
	and	nvl(cd_area_procedimento,cd_area_procedimento_w)= cd_area_procedimento_w
	and	nvl(cd_especialidade_proc,cd_especialidade_w)	= cd_especialidade_w
	and	nvl(cd_grupo_proc,cd_grupo_proc_w)		= cd_grupo_proc_w
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)	= ie_tipo_atendimento_w
	and	nvl(cd_tipo_acomodacao, cd_tipo_acomodacao_w)	= cd_tipo_acomodacao_w
	and	nvl(cd_setor_atendimento, cd_setor_atendimento_w) = cd_setor_atendimento_w
	and	nr_seq_procedimento_p is not null
	and	nvl(ie_situacao,'A') = 'A'
	and	(sysdate between 
		nvl(dt_inicio_vigencia,to_date('01/01/1900','dd/mm/yyyy')) and 
		nvl(dt_fim_vigencia,to_date('31/12/2099','dd/mm/yyyy')))
	order 	by nvl(ie_tipo_atendimento,0),
		nvl(cd_procedimento,0),
		nvl(ie_origem_proced,ie_origem_proced_w),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade_proc,0),
		nvl(cd_area_procedimento,0))
union all
select	nr_sequencia,
	ie_regra
from	(
	select	nr_sequencia,
		ie_regra
	from	regra_Convenio_Plano_mat
	where	cd_convenio					= cd_convenio_w
	and	nvl(cd_plano, cd_plano_w)			= cd_plano_w
	and	nvl(cd_material,cd_material_w)			= cd_material_w
	and	nvl(cd_grupo_material,cd_grupo_material_w)	= cd_grupo_material_w	
	and	nvl(cd_subgrupo_material,cd_subgrupo_material_w)= cd_subgrupo_material_w
	and	nvl(cd_classe_material,cd_classe_material_w)	= cd_classe_material_w
	and	nvl(ie_tipo_atendimento, ie_tipo_atendimento_w) = ie_tipo_atendimento_w
	and	nvl(cd_tipo_acomodacao, cd_tipo_acomodacao_w)	= cd_tipo_acomodacao_w
	and	nvl(cd_setor_atendimento, cd_setor_atendimento_w) = cd_setor_atendimento_w
	and	nr_seq_material_p is not null
	and 	nvl(ie_tipo_material, nvl(ie_tipo_material_w,'0')) = nvl(ie_tipo_material_w,'0')
	and	(sysdate between 
		nvl(dt_inicio_vigencia,to_date('01/01/1900','dd/mm/yyyy')) and 
		nvl(dt_fim_vigencia,to_date('31/12/2099','dd/mm/yyyy')))
	and	nvl(ie_classificacao, nvl(ie_classificacao_w, '0'))	=	nvl(ie_classificacao_w, '0')
	order	by nvl(ie_tipo_atendimento,0),
		nvl(cd_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_classe_material,0),
		nvl(ie_tipo_material,'0'));

begin

if	(nr_seq_procedimento_p is not null) then

	select	a.cd_procedimento,
		a.ie_origem_proced,
		e.cd_area_procedimento,
		e.cd_especialidade,
		e.cd_grupo_proc,
		b.ie_tipo_atendimento,
		a.nr_atendimento,
		a.cd_setor_atendimento
	into	cd_procedimento_w,
		ie_origem_proced_w,
		cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		ie_tipo_atendimento_w,
		nr_atendimento_w,
		cd_setor_atendimento_w
	from	estrutura_procedimento_v e,
		atendimento_paciente b,
		procedimento_paciente a
	where	a.nr_atendimento	= b.nr_atendimento
	and	a.cd_procedimento	= e.cd_procedimento
	and	a.ie_origem_proced	= e.ie_origem_proced
	and	a.nr_sequencia		= nr_seq_procedimento_p;

elsif	(nr_seq_material_p is not null) then

	select	a.cd_material,
		e.cd_grupo_material,
		e.cd_subgrupo_material,
		e.cd_classe_material,
		b.ie_tipo_atendimento,
		a.nr_atendimento,
		a.CD_SETOR_ATENDIMENTO
	into	cd_material_w,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		ie_tipo_atendimento_w,
		nr_atendimento_w,
		cd_setor_atendimento_w
	from	estrutura_material_v e,
		atendimento_paciente b,
		material_atend_paciente a
	where	a.nr_atendimento	= b.nr_atendimento
	and	a.cd_material		= e.cd_material
	and	a.nr_sequencia		= nr_seq_material_p;

	select 	nvl(max(ie_tipo_material),'0')
	into	ie_tipo_material_w
	from 	material	
	where 	cd_material = cd_material_w;

end if;

select	min(a.cd_convenio)
into	cd_convenio_w
from	convenio a, atend_categoria_convenio b
where	b.nr_atendimento			= nr_atendimento_p
and	a.cd_convenio				= b.cd_convenio
and	a.ie_tipo_convenio			= 2;


select	nvl(max(a.cd_tipo_acomodacao), 0),
	nvl(max(a.cd_plano_convenio),0)
into	cd_tipo_acomodacao_w,
	cd_plano_w
from	convenio b, atend_categoria_convenio a
where	a.nr_atendimento	= nr_atendimento_w
and	a.cd_convenio		= cd_convenio_w
and	a.cd_convenio		= b.cd_convenio
and	b.ie_tipo_convenio	= 2
and	a.dt_inicio_vigencia	= 
			(select	max(x.dt_inicio_vigencia)
			from	convenio y, atend_categoria_convenio x
			where	x.nr_atendimento	= a.nr_atendimento
			and	x.cd_convenio		= a.cd_convenio
			and	x.cd_convenio		= y.cd_convenio
			and	y.ie_tipo_convenio	= 2);
			
select	nvl(max(cd_simpro), 0)
into	cd_simpro_w
from	material_simpro
where	nr_sequencia	=	nr_seq_material_p;

select	nvl(max(ie_classificacao), '0')
into	ie_classificacao_w
from	simpro_preco
where	cd_simpro	=	cd_simpro_w;			

nr_sequencia_w	:= null;
open c01;
loop
fetch c01 into
	nr_sequencia_w,
	ie_regra_w;
exit when c01%notfound;
	nr_sequencia_w		:= nr_sequencia_w;
end loop;
close c01;

ds_retorno_w		:= null;
if	(ie_opcao_p = '1') then
	ds_retorno_w	:= nr_sequencia_w;
elsif	(ie_opcao_p = '2') then
	ds_retorno_w	:= ie_regra_w;
end if;

return	ds_retorno_w;

end OBTER_REGRA_CONV_PLANO_ATEND;
/