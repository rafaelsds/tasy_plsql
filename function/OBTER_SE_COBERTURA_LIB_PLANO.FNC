create or replace
function Obter_Se_Cobertura_Lib_Plano
		(cd_convenio_p		number,
		cd_plano_p		varchar2,
		nr_seq_cobertura_p	number) 
		return varchar2 is

ie_liberado_w		varchar2(01)	:= 'S';
qt_regra_w		number(10,0);

begin

select	count(*)
into	qt_regra_w
from	convenio_cobertura_regra
where	cd_convenio		= cd_convenio_p
and 	cd_plano		= cd_plano_p;

if	(qt_regra_w	<> 0) then
	select	nvl(max('S'), 'N')
	into	ie_liberado_w
	from	convenio_cobertura_regra
	where	cd_convenio		= cd_convenio_p
	and	cd_plano		= cd_plano_p
	and 	nr_seq_cobertura	= nr_seq_cobertura_p;
end if;

return	ie_liberado_w;

end Obter_Se_Cobertura_Lib_Plano;
/