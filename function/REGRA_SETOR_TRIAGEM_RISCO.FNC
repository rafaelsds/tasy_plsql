create or replace
function regra_setor_triagem_risco(	    cd_setor_p		  number,
                    					nr_seq_triagem_p  number)  
								    return varchar2 is									
ie_permitir_w  	varchar2(1) := 'S';										
										
begin
										
	select 	 NVL(MAX(b.ie_permite),'S')
	into     ie_permitir_w 
	from 	 triagem_classif_risco a, 
	 		 triagem_clas_risco_regra b 
	where	 a.nr_sequencia = b.nr_seq_triagem(+)
	and      a.nr_sequencia = nr_seq_triagem_p			
	and      b.cd_setor_atendimento  = cd_setor_p
	order by  nvl(b.ie_permite,'S') desc; 

return 	ie_permitir_w;					
										
end regra_setor_triagem_risco;
/