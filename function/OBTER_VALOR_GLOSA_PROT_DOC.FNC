create or replace
function obter_valor_glosa_prot_doc(nr_seq_protocolo_p		number)
 		    	return number is
nr_seq_retorno_w	number(10);
vl_glosa_w		number(15,2) := 0;
begin
if	(nr_seq_protocolo_p is not null) then

	select	max(nr_sequencia)
	into	nr_seq_retorno_w
	from	convenio_retorno
	where	nr_seq_protocolo = nr_seq_protocolo_p;
end if;

select	sum(vl_glosado)
into	vl_glosa_w
from	convenio_retorno_item
where	nr_seq_retorno = nr_seq_retorno_w;

return	vl_glosa_w;

end obter_valor_glosa_prot_doc;
/