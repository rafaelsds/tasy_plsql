create or replace
function Obter_Se_MotCanc_Agenda_Regra(cd_perfil_p			Number,
					nr_seq_motivo_cancel_agenda_p	Number,
					ie_agenda_p			Varchar2,
					nm_usuario_p			Varchar2,
					cd_estabelecimento_p		Number)
					
 		    	return Varchar2 is

qt_motivo_regra_w	Number(5);
ds_retorno_w		Varchar2(1);
qt_regra_w		Number(5);
			
begin

select	count(*)
into	qt_regra_w 
from	agenda_motivo_canc_regra
where	nr_seq_motivo_cancel_agenda	= nr_seq_motivo_cancel_agenda_p;

if	(qt_Regra_w > 0) then
	select	count(*)
	into	qt_motivo_regra_w
	from	agenda_motivo_canc_regra
	where	nvl(cd_perfil,cd_perfil_p)	= cd_perfil_p
	and	((cd_estabelecimento		= cd_estabelecimento_p) or (cd_estabelecimento is null))
	and	nr_seq_motivo_cancel_agenda	= nr_seq_motivo_cancel_agenda_p
	and	((nvl(ie_Agenda, 'T')			= ie_Agenda_p)
	or	(nvl(ie_Agenda, 'T')			= 'T'));

	if	(qt_motivo_regra_w	> 0) then
		ds_retorno_w	:= 'N';
	else
		ds_retorno_w	:= 'S';
	end if;
else
	ds_retorno_w	:= 'N';
end if;
	
return	ds_retorno_w;

end Obter_Se_MotCanc_Agenda_Regra;
/