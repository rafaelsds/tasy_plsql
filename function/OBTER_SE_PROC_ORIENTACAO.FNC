create or replace
function Obter_se_proc_orientacao(	nr_seq_proc_interno_p	Number)
			return Varchar2 is
			
ie_imp_orientacao_w	varchar2(1);

begin

select	nvl(max('S'),'N')
into	ie_imp_orientacao_w
from	proc_interno
where	nr_sequencia = nr_seq_proc_interno_p
and	ie_imp_orientacao = 'S';

return	ie_imp_orientacao_w;

end Obter_se_proc_orientacao;
/