create or replace
function reg_fetch_revision_author (	nr_seq_revision_p		number )
 		    	return varchar2 is

nm_author_user_w		reg_doc_revisor.nm_usuario_revisor%type;
nr_seq_documento_w		reg_version_revision.nr_seq_documento%type;

begin

select	dr.nm_usuario_revisor
into	nm_author_user_w
from	reg_version_revision vr,
	reg_doc_revisor dr
where	vr.nr_sequencia = nr_seq_revision_p
and	vr.nr_seq_documento = dr.nr_seq_documento
and	dr.ie_tipo_revisor = 'C'
and	(
		(dr.dt_fim_vigencia is null and vr.dt_atualizacao_nrec > dr.dt_inicio_vigencia) or
		(vr.dt_atualizacao_nrec between dr.dt_inicio_vigencia and fim_dia(dr.dt_fim_vigencia))
	);

return nm_author_user_w;

end reg_fetch_revision_author;
/
