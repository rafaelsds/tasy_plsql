CREATE OR REPLACE
FUNCTION Obter_Se_Proc_Interno_Conv	(nr_seq_proc_interno_p	number,
						nr_atendimento_p		number)
						RETURN VARCHAR2 IS

ie_situacao_w			Varchar2(01);
cd_estabelecimento_w		Number(4,0);
cd_convenio_w			Number(5,0);
cd_categoria_w		Varchar2(10);
dt_vigencia_w			Date;
cd_edicao_amb_w		Number(6,0);
ie_origem_proc_w		number(10);
ie_origem_edicao_w		number(10);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
ie_proc_conv_w			Varchar2(01);
ie_origem_proc_filtro_w		number(10); 
ie_tipo_convenio_w		number(2);
ie_tipo_atendimento_w		number(3);
cd_setor_atendimento_w		number(6,0);
cd_tipo_acomodacao_w		number(4,0);
ds_auxiliar_w	varchar2(255);

BEGIN

if	(nr_atendimento_p is not null) then
	select	a.cd_estabelecimento,
		b.cd_convenio,
		b.cd_categoria,
		b.dt_inicio_vigencia,
		a.ie_tipo_atendimento,
		b.cd_tipo_acomodacao
	into	cd_estabelecimento_w,
		cd_convenio_w,
		cd_categoria_w,
		dt_vigencia_w,
		ie_tipo_atendimento_w,
		cd_tipo_acomodacao_w
	from	atend_categoria_convenio b,
		atendimento_paciente a
	where	a.nr_atendimento = b.nr_atendimento
	and	a.nr_atendimento = nr_atendimento_p
	and	b.nr_seq_interno = obter_atecaco_atendimento(nr_atendimento_p);

	select 	obter_setor_atendimento(nr_atendimento_p)
	into	cd_setor_atendimento_w
	from 	dual;
	
end if;

dt_vigencia_w	:= trunc(nvl(dt_vigencia_w, sysdate));

select	cd_procedimento,
	ie_origem_proced
into	cd_procedimento_w,
	ie_origem_proced_w
from	proc_interno
where	nr_sequencia	= nr_seq_proc_interno_p;

select 	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from 	convenio
where 	cd_convenio = cd_convenio_w;

Obter_Edicao_Proc_Conv(cd_estabelecimento_w,cd_convenio_w,cd_categoria_w,sysdate,cd_procedimento_w,cd_edicao_amb_w,ds_auxiliar_w,ds_auxiliar_w,ds_auxiliar_w,ds_auxiliar_w,ds_auxiliar_w,ds_auxiliar_w,ie_origem_proced_w);

ie_origem_proc_filtro_w:= Obter_Origem_Proced_Cat(cd_estabelecimento_w, ie_tipo_atendimento_w, ie_tipo_convenio_w, cd_convenio_w, cd_categoria_w);

select	decode(count(*), 0, 'N', 'S')
into	ie_situacao_w
from	proc_interno_conv a
where	(a.nr_seq_proc_interno = nvl(nr_seq_proc_interno_p, 0))
and	((cd_convenio		= nvl(cd_convenio_w,0) OR cd_convenio IS NULL))
and	((cd_edicao_amb		= nvl(cd_edicao_amb_w,0) OR cd_edicao_amb IS NULL))
and	((ie_tipo_atendimento	= nvl(ie_tipo_atendimento_w,0) OR ie_tipo_atendimento IS NULL))
and 	((ie_origem_proc_filtro = nvl(ie_origem_proc_filtro_w,0)) or (ie_origem_proc_filtro is null))
and 	((cd_setor_atendimento  = nvl(cd_setor_atendimento_w,0)) or (cd_setor_atendimento is null))
and 	((cd_estabelecimento   = nvl(cd_estabelecimento_w, wheb_usuario_pck.get_cd_estabelecimento)) or (cd_estabelecimento is null))
and	dt_vigencia_w between nvl(a.dt_inicio_vigencia, dt_vigencia_w) and nvl(a.dt_final_vigencia, dt_vigencia_w)
and	((cd_tipo_acomodacao = nvl(cd_tipo_acomodacao_w,0)) or (cd_tipo_acomodacao is null));

ie_proc_conv_w	:= ie_situacao_w;

if 	(ie_situacao_w = 'N') then
	begin

	select	max(ie_origem_proced)
	into	ie_origem_proc_w
	from	proc_interno
	where	nr_sequencia		= nr_seq_proc_interno_p;

	select	max(ie_origem_proced)
	into	ie_origem_edicao_w
	from	edicao_amb
	where	cd_edicao_amb		= cd_edicao_amb_w;

	select	nvl(max('S'), 'N')
	into	ie_situacao_w
	from	dual
	where	((ie_origem_proc_w	= ie_origem_edicao_w) or (obter_tipo_convenio(cd_convenio_w) = 3));

	end;
end if;

if	(ie_proc_conv_w = 'N') and
	(ie_origem_proced_w = 1) and
	(obter_tipo_convenio(cd_convenio_w) = 3) then
	ie_situacao_w	:= 'N';
end if;

RETURN	ie_situacao_w;

END	Obter_Se_Proc_Interno_Conv;
/