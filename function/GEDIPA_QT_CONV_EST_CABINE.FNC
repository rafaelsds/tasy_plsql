create or replace
function gedipa_qt_conv_est_cabine(	cd_material_p 			number,
					nr_seq_gedipa_cabine_p 		number,
					cd_unidade_medida_dest_p 	varchar2) return number is
						
qt_dose_disp_w              	number(18,6);
qt_estoque_w			number(18,6);
cd_unidade_medida_consumo_w 	varchar(30);

begin

SELECT 	max(a.qt_estoque),
	max(b.cd_unidade_medida_consumo)
into 	qt_estoque_w,
	cd_unidade_medida_consumo_w
FROM 	gedipa_estoque_cabine a, material b
WHERE   a.cd_material = b.cd_material
AND 	a.nr_sequencia = nr_seq_gedipa_cabine_p;

qt_dose_disp_w := Obter_dose_convertida_quimio(cd_material_p, qt_estoque_w, cd_unidade_medida_consumo_w, cd_unidade_medida_dest_p);

return qt_dose_disp_w;

end gedipa_qt_conv_est_cabine;
/
