create or replace function obter_status_suporte_remoto( nr_seq_ordem_p      number, 
                                      cd_solicitante_p    number, 
                                      nm_usuario_exec_p   varchar2, 
                                      ie_sup_remoto_p     varchar2, 
                                      ie_status_ordem_p   number,
                                      nr_seq_estagio_p    number,
                                      ie_origem_os_p      varchar2)
return varchar2 is

ds_retorno_w            varchar2(50);
iniciar_sup_remoto_w    varchar2(1);
aceitar_sup_remoto_w    varchar2(1);
finalizar_sup_remoto_w  varchar2(1);

begin
  
begin
  
select 'S' 
into iniciar_sup_remoto_w 
from dual
where   (select decode(count(*), 0, 'N', 'S') ie_sup_remoto
        from	man_ordem_servico_exec
        where	nr_seq_ordem = nr_seq_ordem_p
        and	nm_usuario_exec = nm_usuario_exec_p) = 'S'
and (obter_se_base_corp() = 'S' or obter_se_base_wheb() = 'S')
and (ie_sup_remoto_p is null or (ie_sup_remoto_p is not null and ie_sup_remoto_p in ('', 'N', 0)))
and (ie_status_ordem_p is null or (ie_status_ordem_p is not null and ie_status_ordem_p <> 3))
and (nr_seq_estagio_p is null or (nr_seq_estagio_p is not null and nr_seq_estagio_p <> 9))
and ie_origem_os_p = 1;
exception
	when others then
	ds_retorno_w	:= '';
end;

begin 

select 'S' 
into aceitar_sup_remoto_w 
from dual
where   (select	decode(count(*), 0, 'N', 'S') ie_inicia_sup_remoto
        from man_ordem_servico a
        where	a.nr_sequencia = nr_seq_ordem_p
        and	a.ie_suporte_remoto = 'S'
        and	a.cd_pessoa_solicitante = cd_solicitante_p) = 'S'
and ie_origem_os_p in (1, 4);

exception
	when others then
	ds_retorno_w	:= '';
end;

begin

select 'S' 
into finalizar_sup_remoto_w 
from dual
where (select	decode(count(*), 0, 'N', 'S') ie_finaliza_sup_remoto
      from	man_ordem_servico a,
        man_ordem_serv_tecnico b
      where	a.nr_sequencia = b.nr_seq_ordem_serv
      and	a.nr_sequencia = nr_seq_ordem_p
      and	a.ie_suporte_remoto = 'A'
      and	b.dt_liberacao is not null) = 'S'
and ie_origem_os_p in (1, 4)
and (iniciar_sup_remoto_w is null or (iniciar_sup_remoto_w is not null and iniciar_sup_remoto_w = ''));

exception
	when others then
	ds_retorno_w	:= '';
end;

if (iniciar_sup_remoto_w is not null and iniciar_sup_remoto_w = 'S') then
  --pode iniciar o suporte remoto
  ds_retorno_w	:= 'I';
elsif (aceitar_sup_remoto_w is not null and aceitar_sup_remoto_w = 'S') then
  --pode aceitar o suporte remoto
  ds_retorno_w	:= 'A';
elsif (finalizar_sup_remoto_w is not null and finalizar_sup_remoto_w = 'S') then
  --pode finalizar o suporte remoto
  ds_retorno_w	:= 'F';
end if;

if(ds_retorno_w is null) then
  ds_retorno_w	:= '';
end if;

return  ds_retorno_w;
end obter_status_suporte_remoto;
/