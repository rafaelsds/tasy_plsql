create or replace
function proj_obter_desc_classif_ata
			(	nr_seq_classificacao_p		Number)
 		    		return Varchar2 is
				
ds_retorno_w			Varchar2(255);

begin

select	nvl(max(ds_classificacao),'')
into	ds_retorno_w
from	proj_ata_classif_pend
where	nr_sequencia	= nr_seq_classificacao_p;

return	ds_retorno_w;

end proj_obter_desc_classif_ata;
/