create or replace
function Obter_Lista_Esp_Estagio_Ant
		(nr_sequencia_p		number)
		return			varchar2 is

ds_retorno_w		varchar2(100);

begin

if (nr_sequencia_p is not null) then
	select	nvl(a.ds_estagio, '')           
	into	ds_retorno_w
	from	AG_LISTA_ESPERA_ESTAGIO a
	where	EXISTS( select	1 
					from AG_LISTA_ESP_ESTAGIO_ANT b 
					where a.nr_sequencia = b.nr_seq_estagio 
					and b.nr_seq_estagio_ant = nr_sequencia_p);
end if;

return	ds_retorno_w;

end Obter_Lista_Esp_Estagio_Ant;
/