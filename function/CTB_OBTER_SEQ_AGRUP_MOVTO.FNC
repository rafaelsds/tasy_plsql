create or replace
function ctb_obter_seq_agrup_movto(	nr_lote_contabil_p	number,
			nr_seq_agrupamento_p	number,
			nr_sequencia_p		number,
			nr_seq_movto_cc_p	number)
 		    	return number is

nr_sequencia_w		number(10)	:= 0;

cursor c01 is
select	rownum nr_sequencia,
	y.nr_seq_movto,
	y.nr_seq_agrupamento,
	y.nr_seq_movto_cc
from	(select	a.nr_sequencia nr_seq_movto,
		b.nr_sequencia nr_seq_movto_cc,
		a.nr_seq_agrupamento
	from	ctb_movto_centro_custo b,
		ctb_movimento a
	where	a.nr_sequencia		= b.nr_seq_movimento(+)
	and	a.nr_lote_contabil	= nr_lote_contabil_p
	and	a.nr_seq_agrupamento	= nr_seq_agrupamento_p
	order by a.nr_sequencia,nvl(b.nr_sequencia,0)) y
where	1 = 1
order by 1;

Vet01	C01%RowType;

begin

open C01;
loop
fetch C01 into	
	Vet01;
exit when C01%notfound;
	begin
	
	if	(nr_sequencia_w = 0) and
		(nvl(nr_seq_movto_cc_p,0) = 0) and
		(Vet01.nr_seq_movto = nr_sequencia_p) then
		nr_sequencia_w	:= Vet01.nr_sequencia;
	end if;
	
	if	(nr_sequencia_w = 0) and
		(nvl(nr_seq_movto_cc_p,0) <> 0) and
		(vet01.nr_seq_movto_cc = nr_seq_movto_cc_p) then
		nr_sequencia_w	:= vet01.nr_sequencia;
	end if;
	end;
end loop;
close C01;


return	nr_sequencia_w;

end ctb_obter_seq_agrup_movto;
/