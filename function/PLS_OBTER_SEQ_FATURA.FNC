create or replace
function pls_obter_seq_fatura(nm_usuario_p		usuario.nm_usuario%type)
 		    	return number is
			
nr_seq_fatura_w		ptu_fatura.nr_sequencia%type;

begin

select	max(nr_sequencia)
into	nr_seq_fatura_w
from	ptu_fatura
where	nm_usuario_nrec		= nm_usuario_p
and	dt_atualizacao_nrec	= (select	max(dt_atualizacao_nrec)
				   from		ptu_fatura
				   where	nm_usuario_nrec		= nm_usuario_p);

return	nr_seq_fatura_w;

end pls_obter_seq_fatura;
/