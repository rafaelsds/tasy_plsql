create or replace
function man_obter_se_mostra_os_confid(
				nr_sequencia_p	number,
				nm_usuario_p	varchar2)
				return varchar2 is

ds_retorno_w			varchar2(1) := 'S';
qt_regra_w			number(10,0);
nr_seq_confidencialidade_w	number(10,0);
cd_pessoa_solicitante_w		varchar2(10);
cd_pessoa_usuario_w		varchar2(10);
cd_cargo_w			number(10,0);
cd_setor_atendimento_w		number(10,0);
cd_perfil_w			number(10,0) := obter_perfil_ativo;

begin

if	(nr_sequencia_p > 0) then
	select	nr_seq_confidencialidade,
		cd_pessoa_solicitante
	into	nr_seq_confidencialidade_w,
		cd_pessoa_solicitante_w
	from	man_ordem_servico
	where	nr_sequencia = nr_sequencia_p;

	if	(nr_seq_confidencialidade_w > 0) then
		select	count(1)
		into	qt_regra_w
		from	man_confidencialidade_lib
		where	nr_seq_confidencialidade = nr_seq_confidencialidade_w
		and	rownum = 1;

		if	(qt_regra_w > 0) then
			begin
			select	max(cd_pessoa_fisica),
				max(cd_setor_atendimento)
			into	cd_pessoa_usuario_w,
				cd_setor_atendimento_w
			from	usuario
			where	nm_usuario = nm_usuario_p;
			/* Independente se existir em alguma regra, o usu�rio solicitante da OS tem permiss�o para visualizar */
			if	(cd_pessoa_solicitante_w <> nvl(cd_pessoa_usuario_w,'X')) then
				begin
				begin
				select	cd_cargo
				into	cd_cargo_w
				from	pessoa_fisica
				where	cd_pessoa_fisica = cd_pessoa_usuario_w;
				exception
				when others then
					cd_cargo_w := '';
				end;

				begin
				select	'S'
				into	ds_retorno_w
				from	(select	1
					from	man_confidencialidade_lib
					where	nr_seq_confidencialidade = nr_seq_confidencialidade_w
					and	cd_cargo = cd_cargo_w
					and	cd_cargo is not null
					union all
					select	1
					from	man_confidencialidade_lib
					where	nr_seq_confidencialidade = nr_seq_confidencialidade_w
					and	cd_pessoa_fisica = cd_pessoa_usuario_w
					and	cd_pessoa_fisica is not null
					union all
					select	1
					from	man_confidencialidade_lib
					where	nr_seq_confidencialidade = nr_seq_confidencialidade_w
					and	cd_perfil = cd_perfil_w
					and	cd_perfil is not null
					union all
					select	1
					from	man_confidencialidade_lib
					where	nr_seq_confidencialidade = nr_seq_confidencialidade_w
					and	cd_setor_atendimento = cd_setor_atendimento_w
					and	cd_setor_atendimento is not null)
				where	rownum < 2;
				exception
				when others then
					ds_retorno_w := 'N';
				end;

				/* Verifica se tem regra por tipo de executor */
				if	(ds_retorno_w = 'N') then
					select	count(1)
					into	qt_regra_w
					from	man_confidencialidade_lib
					where	nr_seq_confidencialidade = nr_seq_confidencialidade_w
					and	(nr_seq_tipo_exec is not null or nvl(ie_executor,'N') = 'S');

					if	(qt_regra_w > 0) then
						begin
						select	'S'
						into	ds_retorno_w
						from	(select	1
							from	man_confidencialidade_lib b,
								man_ordem_servico_exec a
							where	a.nr_seq_ordem 		= nr_sequencia_p
							and	a.nm_usuario_exec		= nm_usuario_p
							and	b.nr_seq_confidencialidade 	= nr_seq_confidencialidade_w
							and	b.nr_seq_tipo_exec		= a.nr_seq_tipo_exec
							and	b.nr_seq_tipo_exec is not null
							union all
							select	1
							from	man_confidencialidade_lib b,
								man_ordem_servico_exec a
							where	a.nr_seq_ordem 		= nr_sequencia_p
							and	a.nm_usuario_exec		= nm_usuario_p
							and	b.nr_seq_confidencialidade 	= nr_seq_confidencialidade_w
							and	nvl(ie_executor,'N') 		= 'S')
						where	rownum = 1;
						exception
						when others then
							ds_retorno_w := 'N';
						end;
					end if;
				end if;
				end;
			end if;
			end;
		end if;
	end if;
end if;

return	ds_retorno_w;

end man_obter_se_mostra_os_confid;
/