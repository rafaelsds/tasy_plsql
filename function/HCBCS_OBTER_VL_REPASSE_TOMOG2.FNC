create or replace
function HCBCS_OBTER_VL_REPASSE_TOMOG2(nr_seq_proc_p number)
						return number is

vl_retorno_w		number(18,4);
cd_convenio_w		number(15);
vl_contraste_w		number(18,4);
vl_custo_medio_w	number(18,4);
vl_total_contraste_w	number(18,4);
vl_procedimento_w		number(18,4);
nr_atendimento_w		number(15);
cd_material_w		number(6);
cd_procedimento_w	number(15);
qt_material_w		number(15,4);
nr_seq_proc_interno_w	number(10);
ie_origem_proced_w	number(10);
cd_edicao_amb_w		number(10);
qt_filme_w		number(10);
vl_filme_w		number(17,4);
cd_estabelecimento_w	number(10);
nr_irrelevante_w		number(15,4);
ds_irrelevante_w		varchar2(255);
nr_seq_regra_w		number(10);
vl_materiais_w		number(15,2);
cd_unidade_medida_w	varchar2(40);
qt_padrao_w		number(15,4);
dt_procedimento_w	date;
qt_conv_estoque_consumo_w	number(13,4);

cursor c01 is
select	a.cd_material,
	a.qt_material
from	material_atend_paciente a
where	a.nr_atendimento	= nr_atendimento_w
and	a.nr_seq_proc_princ	= nr_seq_proc_p
and	a.cd_motivo_exc_conta	is null;

begin

select	a.vl_procedimento,	
	a.nr_atendimento,
	a.nr_seq_proc_interno,
	b.cd_estabelecimento,
	b.cd_convenio_parametro,
	a.vl_materiais,
	a.dt_procedimento
into	vl_procedimento_w,	
	nr_atendimento_w,
	nr_seq_proc_interno_w,
	cd_estabelecimento_w,
	cd_convenio_w,
	vl_materiais_w,
	dt_procedimento_w
from	procedimento_paciente a,
	conta_paciente b
where	a.nr_interno_conta	= b.nr_interno_conta
and	a.nr_sequencia		= nr_seq_proc_p;

vl_total_contraste_w	:= 0;
vl_custo_medio_w	:= 0;

--vl_procedimento_w	:= vl_procedimento_w - nvl(vl_materiais_w,0);

open c01;
loop
fetch c01 into
	cd_material_w,
	qt_material_w;
exit when c01%notfound;

	select	nvl(qt_conv_estoque_consumo,1)
	into	qt_conv_estoque_consumo_w
	from	material
	where	cd_material	= cd_material_w;

	obter_custo_medio_mat(cd_estabelecimento_w,dt_procedimento_w,cd_material_w,vl_custo_medio_w);				
	
	vl_contraste_w	:= (dividir(vl_custo_medio_w,qt_conv_estoque_consumo_w) * qt_material_w);

	vl_total_contraste_w	:= vl_total_contraste_w + vl_contraste_w;
	
end loop;
close c01;


vl_retorno_w		:= nvl(vl_procedimento_w,0) - nvl(vl_total_contraste_w,0);

/*lhalves OS346383 em 03/08/2011 - Obter o quantidade do filme da tabela procedimento AMB vinculado ao proc interno*/
if	(nr_seq_proc_interno_w is not null) then

	select	nvl(max(vl_negociado),0)
	into	vl_filme_w
	from	regra_ajuste_material
	where	cd_convenio		= cd_convenio_w
	and	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_material		= 53791
	and	ie_situacao		= 'A';
	
	vl_retorno_w	:= nvl(vl_retorno_w,0) - round((nvl(vl_filme_w,0) * 0.30),2);
	
end if;

return	vl_retorno_w;

end	HCBCS_OBTER_VL_REPASSE_TOMOG2;
/