create or replace
function nut_obter_custo_total(
		nr_seq_rec_real_p		number)
 		return number is

vl_total_w	number(17,4);

begin
if	(nr_seq_rec_real_p is not null) then
	begin
	select	sum(vl_custo) vl_total
	into	vl_total_w
	from	nut_rec_real_comp
	where	nr_seq_rec_real = nr_seq_rec_real_p;
	end;
end if;
return	vl_total_w;
end nut_obter_custo_total;
/