create or replace function obter_se_conectado_3916(dt_acesso_p date, cd_estab_p number)
return number is

qt_reg_w number(10);

begin

select count(distinct nm_usuario)
into qt_reg_w
from tasy_controle_login
where ds_aplicacao = 'TasySwing'
and  trunc(dt_acesso) = trunc(dt_acesso_p)
  and   cd_estabelecimento = cd_estab_p
and  TRUNC(dt_acesso,'hh') = dt_acesso_p;

return qt_reg_w;

end;
/