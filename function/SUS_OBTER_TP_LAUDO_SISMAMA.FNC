create or replace
function sus_obter_tp_laudo_sismama(	nr_seq_laudo_p		number,
					it_tipo_retorno_p	number)
					return varchar2 is
qt_laudo_mamo_w		number(10) := 0;
qt_laudo_cito_w		number(10) := 0;
qt_laudo_histo_w	number(10) := 0;
ds_retorno_w		varchar2(100);
/*Tipo retorno:
	1 - número:
		1 - mamografia
		2 - citopatológico
		3 - histopatológico
	2 - descrição*/
					
begin

select	count(*)
into	qt_laudo_mamo_w
from	sismama_mam_ind_clinica
where	nr_seq_sismama = nr_seq_laudo_p;

if	(qt_laudo_mamo_w = 0) then
	begin	
	select	count(*)
	into	qt_laudo_mamo_w
	from	sismama_mam_conclusao
	where	nr_seq_sismama = nr_seq_laudo_p;	
	if	(qt_laudo_mamo_w = 0) then
		begin		
		select	count(*)
		into	qt_laudo_mamo_w
		from	sismama_mam_anamnese
		where	nr_seq_sismama = nr_seq_laudo_p;		
		if	(qt_laudo_mamo_w = 0) then
			begin			
			select	count(*)
			into	qt_laudo_mamo_w
			from	sismama_anamnese_rad
			where	nr_seq_sismama = nr_seq_laudo_p;
			end;
		end if;
		end;
	end if;	
	end;
end if;

select	count(*)
into	qt_laudo_cito_w
from	sismama_cit_anamnese
where	nr_seq_sismama = nr_seq_laudo_p;

if	(qt_laudo_cito_w = 0) then
	begin
	select	count(*)
	into	qt_laudo_cito_w
	from	sismama_cit_exame_clinico
	where	nr_seq_sismama = nr_seq_laudo_p;
	if	(qt_laudo_cito_w = 0) then
		begin
		select	count(*)
		into	qt_laudo_cito_w
		from	sismama_cit_resultados
		where	nr_seq_sismama = nr_seq_laudo_p;		
		end;
	end if;	
	end;
end if;

select	count(*)
into	qt_laudo_histo_w
from	sismama_his_dados_clinico
where	nr_seq_sismama = nr_seq_laudo_p;

if	(qt_laudo_histo_w = 0) then
	begin
	select	count(*)
	into	qt_laudo_histo_w
	from	sismama_his_exame_micro
	where	nr_seq_sismama = nr_seq_laudo_p;
	if	(qt_laudo_histo_w = 0) then
		begin
		select	count(*)
		into	qt_laudo_histo_w
		from	sismama_his_resultado
		where	nr_seq_sismama = nr_seq_laudo_p;		
		end;
	end if;	
	end;
end if;

if	(qt_laudo_mamo_w > 0) then
	begin
	if	(it_tipo_retorno_p = 1) then
		ds_retorno_w := '1';
	else
		--ds_retorno_w := 'Mamografia';
		ds_retorno_w := wheb_mensagem_pck.get_texto(314222);
	end if;	
	end;
elsif	(qt_laudo_cito_w > 0) then
	begin
	if	(it_tipo_retorno_p = 1) then
		ds_retorno_w := '2';
	else
		--ds_retorno_w := 'Citopatológico';
		ds_retorno_w := wheb_mensagem_pck.get_texto(313993);
	end if;	
	end;
elsif	(qt_laudo_histo_w > 0) then
	begin
	if	(it_tipo_retorno_p = 1) then
		ds_retorno_w := '3';
	else
		--ds_retorno_w := 'Histopatológico';
		ds_retorno_w := wheb_mensagem_pck.get_texto(313994);
	end if;	
	end;
end if;	

return	nvl(ds_retorno_w,'');

end sus_obter_tp_laudo_sismama;
/