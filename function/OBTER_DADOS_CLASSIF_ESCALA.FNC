create or replace
function obter_dados_classif_escala(	nr_seq_classif_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

/* C - Celular */

ds_retorno_w	varchar2(200);
nr_celular_w	varchar2(40);
					
begin

if	(nr_seq_classif_p > 0) then 
	select	max(nr_telefone_celular)
	into	nr_celular_w
	from	escala_diaria_adic_classif
	where	nr_sequencia	=	nr_seq_classif_p;
end if;

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= nr_celular_w;
end if;

return	ds_retorno_w;

end obter_dados_classif_escala;
/