create or replace
function obter_prescr_controle	(
				nr_controle_p		number,
				cd_estabelecimento_p	number default null
				)
				return number is

nr_prescricao_w		number(15,0);

begin

nr_prescricao_w	:= 0;

if	(nr_controle_p is not null) then

	select	max(nr_prescricao)
	into	nr_prescricao_w
	from	prescr_medica
	where	nr_controle	   = nr_controle_p
	and	cd_estabelecimento = nvl(cd_estabelecimento_p,cd_estabelecimento);
	
	if 	(nr_prescricao_w is null) then
	
		select	max(p.nr_prescricao)
		into	nr_prescricao_w
		from	prescr_procedimento r,
			prescr_medica p
		where	r.nr_controle_ext	= nr_controle_p
		and	r.nr_prescricao		= p.nr_prescricao
		and	p.cd_estabelecimento	= nvl(cd_estabelecimento_p,cd_estabelecimento);
		
	end if;
	
end if;

return	nr_prescricao_w;

end obter_prescr_controle;
/