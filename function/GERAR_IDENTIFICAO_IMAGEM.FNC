create or replace
Function	gerar_identificao_imagem (nr_atendimento_p 	number)
			return varchar2 is

nr_atendimento_w		number;
cd_PrestadorOperadora_w		varchar2(14);

ds_retorno_w			varchar2(40);

begin
nr_atendimento_w := nr_atendimento_p;

Obter_Param_Usuario(-2260, 5, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, cd_PrestadorOperadora_w);

if cd_PrestadorOperadora_w is null then
	cd_PrestadorOperadora_w := '_';
end if;

ds_retorno_w := LPAD(cd_PrestadorOperadora_w, 14, '_') || LPAD(nvl(Obter_guia_atend(nr_atendimento_w, 
								obter_convenio_atendimento(nr_atendimento_w), 
								Obter_categoria_atendimento(nr_atendimento_w)),'_'), 20, '_'); 
 					
return	ds_retorno_w;

end	gerar_identificao_imagem;	
/
