create or replace
function obter_desc_motivo_term_prec(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);				
begin
ds_retorno_w := '';
if (nr_sequencia_p <> 0 ) then
	select	ds_motivo 
	into	ds_retorno_w
	from	motivo_termino_precaucao
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end obter_desc_motivo_term_prec;
/
