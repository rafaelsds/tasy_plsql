create or replace
function Obter_desc_modelo_atual(cd_pessoa_fisica_p		varchar2)
 		    	return varchar2 is

ds_retorno_w			varchar(255);			
			
begin

if (cd_pessoa_fisica_p is not null) then

SELECT		MAX(SUBSTR(rp_obter_desc_modelo_agend(x.nr_seq_modelo_agendamento),1,80))
INTO		ds_retorno_w
FROM		RP_PAC_MODELO_AGENDAMENTO x,
		RP_PACIENTE_REABILITACAO a  
WHERE		a.nr_sequencia = x.nr_seq_pac_reab
AND		a.cd_pessoa_fisica = cd_pessoa_fisica_p
AND		x.dt_inicio_modelo = (SELECT 	MAX(x.dt_inicio_modelo)
				  FROM	                RP_PAC_MODELO_AGENDAMENTO x,
						RP_PACIENTE_REABILITACAO a  
				  WHERE	                a.nr_sequencia = x.nr_seq_pac_reab
				  AND	                a.cd_pessoa_fisica = cd_pessoa_fisica_p); 	

end if;

return	ds_retorno_w;

end Obter_desc_modelo_atual;
/