create or replace
function obter_se_sms_setor_alerta	(	nr_atendimento_p	number,
						nr_seq_regra_p		number)
					return varchar2 is

ie_recebe_w		varchar2(1);
qt_existe_regra_w	number;
nr_sequencia_w		number(10);
		
begin

ie_recebe_w := 'S';

	select	count(*)
	into	qt_existe_regra_w
	from	regra_envio_sms_setor
	where	nr_seq_regra = nr_seq_regra_p;
	
if	(qt_existe_regra_w = 0) then
	ie_recebe_w := 	'S';
else

	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	regra_envio_sms_setor
	where	(nvl(cd_setor_atendimento,obter_setor_atendimento(nr_atendimento_p)) = obter_setor_atendimento(nr_atendimento_p))
	and	nr_seq_regra = nr_seq_regra_p; 

	if (nr_sequencia_w is not null) then
	
		select	nvl(max(ie_envia_alerta),'S')
		into	ie_recebe_w
		from	regra_envio_sms_setor
		where	nr_sequencia = nr_sequencia_w;
	else
		ie_recebe_w := 'N';
	end if;
	
end if; 

return	ie_recebe_w;

end obter_se_sms_setor_alerta;
/
