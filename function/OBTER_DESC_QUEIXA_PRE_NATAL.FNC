CREATE OR REPLACE
FUNCTION	obter_desc_queixa_pre_natal (	nr_seq_queixa_p		number)
						return varchar2 is


ds_queixa_w		varchar2(255);

BEGIN
if	(nr_seq_queixa_p	is not null) then
	select	DS_QUEIXA
	into	ds_queixa_w
	from	MED_PRE_NATAL_QUEIXA
	where	nr_sequencia = nr_seq_queixa_p;
end if;
return ds_queixa_w;

END obter_desc_queixa_pre_natal;
/