create or replace
FUNCTION  obter_status_unidade_data(	dt_parametro_p		DATE,
					nr_seq_interno_p	NUMBER)
					RETURN VARCHAR2 IS
ie_retorno_w	VARCHAR2(1);
nr_seq_hist_w	NUMBER(10);

BEGIN

SELECT	NVL(MAX(nr_sequencia),0)
INTO	nr_seq_hist_w
FROM	unidade_atend_hist
WHERE	nr_seq_unidade = nr_seq_interno_p
AND	TRUNC(dt_parametro_p,'dd') >= TRUNC(dt_historico,'dd');

IF	(nr_seq_hist_w > 0) THEN
	SELECT 	NVL(MAX(IE_STATUS_UNIDADE),'X')
	INTO	ie_retorno_w
	FROM	unidade_atend_hist
	WHERE	nr_sequencia = nr_seq_hist_w;
ELSE
	ie_retorno_w := 'X';
END IF;


RETURN	ie_retorno_w;

END  obter_status_unidade_data;
/