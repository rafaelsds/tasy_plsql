create or replace
function obter_unid_med_anv(cd_unidade_medida_p	varchar2)
 		    	return varchar2 is

ie_unidade_med_inter_w	varchar2(30);

begin

select	max(ie_unidade_med_inter)
into	ie_unidade_med_inter_w
from 	unidade_medida 
where  	upper(cd_unidade_medida)  = upper(cd_unidade_medida_p);


return	nvl(ie_unidade_med_inter_w,cd_unidade_medida_p);

end obter_unid_med_anv;
/