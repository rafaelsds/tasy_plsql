create or replace function gqa_obter_campo_etapa_liberado (nr_seq_etapa_p   gqa_protocolo_etapa_pac.nr_sequencia%type)
  return varchar2 is

nr_seq_score_flex_ii_w    gqa_protocolo_etapa_pac.nr_seq_score_flex_ii%type;
nr_seq_avaliacao_w        gqa_protocolo_etapa_pac.nr_seq_avaliacao%type;
nr_seq_evolucao_w         gqa_protocolo_etapa_pac.nr_seq_evolucao%type;
nr_seq_acao_w             gqa_protocolo_etapa_pac.nr_seq_acao%type;

ie_tipo_acao_w            gqa_acao.ie_tipo_acao%type;
contador                  number(1) := 0;

begin
  select
          nr_seq_acao,
          nr_seq_score_flex_ii,
          nr_seq_avaliacao,
          nr_seq_evolucao
  into
          nr_seq_acao_w,
          nr_seq_score_flex_ii_w,
          nr_seq_avaliacao_w,
          nr_seq_evolucao_w
  from gqa_protocolo_etapa_pac
  where nr_sequencia = nr_seq_etapa_p;
  
  select ie_tipo_acao into ie_tipo_acao_w from gqa_acao where nr_sequencia = nr_seq_acao_w;

  begin
    if(ie_tipo_acao_w = 'ES') then
      select count(1) into contador from escala_eif_ii where nr_sequencia = nr_seq_score_flex_ii_w and dt_liberacao is not null;
    elsif(ie_tipo_acao_w = 'AV') then
      select count(1) into contador from med_avaliacao_paciente where nr_sequencia = nr_seq_avaliacao_w and dt_liberacao is not null;
    elsif(ie_tipo_acao_w = 'NC') then
      select count(1) into contador from evolucao_paciente where cd_evolucao = nr_seq_evolucao_w and dt_liberacao is not null;
    end if;
  exception when no_data_found then
    contador := 0;
  end;

  if (contador is not null and contador > 0) then
    return 'S';
  else
    return 'N';
  end if;

end gqa_obter_campo_etapa_liberado;
/
