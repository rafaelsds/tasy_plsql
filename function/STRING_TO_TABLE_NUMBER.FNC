create or replace
function string_to_table_number(	ds_texto_p	varchar2,
				ie_separador_p	varchar2) return dbms_sql.number_table deterministic is


x					dbms_sql.number_table;
ds_texto_w				varchar2(32767);
pos_w					number(10);
begin

begin
	ds_texto_w	:= replace(ds_texto_p,' ','');
	ds_texto_w	:= replace(ds_texto_w,chr(13),'');
	ds_texto_w	:= replace(ds_texto_w,chr(10),'');
exception when others then
	ds_texto_w	:= '';
end;

if	(ds_texto_w is not null) then
	begin
	while	(ds_texto_w is not null) loop
		begin
		pos_w	:= nvl(instr(ds_texto_w,ie_separador_p),0);
		if	(pos_w = 0) then
			begin
			x(x.count+1)	:= to_number(substr(ds_texto_w,1,255));
			ds_texto_w	:= null;
			exception when others then
				exit;
			end;
		else
			begin
			x(x.count+1)	:= to_number(substr(ds_texto_w,1,pos_w-1));
			ds_texto_w	:= substr(ds_texto_w,pos_w+1,32767);
			exception when others then
				exit;
			end;
		end if;
		end;
	end loop;
	
	end;
end if;

return x;

end string_to_table_number;
/