create or replace function get_case_encounter_type(
		nr_seq_episodio_p		number,
		nr_seq_tipo_episodio_p	number,
		nr_atendimento_p		number,
		ie_tipo_atendimento_p	varchar2)
		return varchar2 is

ie_tipo_atend_case_w		atendimento_paciente.ie_tipo_atendimento%type;
ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;
nr_seq_episodio_w			episodio_paciente.nr_sequencia%type;
nr_seq_tipo_episodio_w		episodio_paciente.nr_seq_tipo_episodio%type;
ie_tipo_atend_retorno_w		atendimento_paciente.ie_tipo_atendimento%type;

    function get_tipo_atendimento(nr_atendimento_p number) return varchar2 is
        ie_tipo_atendimento_w atendimento_paciente.ie_tipo_atendimento%type := null;
        pragma autonomous_transaction;
    begin
        begin
            select	ie_tipo_atendimento
            into	ie_tipo_atendimento_w
            from	atendimento_paciente
            where	nr_atendimento = nr_atendimento_p;
        exception
            when others then
                ie_tipo_atendimento_w := null;
        end;
        
        return ie_tipo_atendimento_w;
        
    end get_tipo_atendimento;

    function get_nr_episodio(nr_atendimento_p number) return number is
        nr_seq_episodio_w	atendimento_paciente.nr_seq_episodio%type;
        pragma autonomous_transaction;
    begin
        begin
            select	nr_seq_episodio
            into	nr_seq_episodio_w
            from	atendimento_paciente
            where	nr_atendimento = nr_atendimento_p;
        exception
            when others then
                nr_seq_episodio_w := null;
        end;
        
        return nr_seq_episodio_w;
    
    end get_nr_episodio;

begin
if	(nvl(ie_tipo_atendimento_p,'X') = 'X') then
    ie_tipo_atendimento_w := get_tipo_atendimento(nr_atendimento_p);
else
	ie_tipo_atendimento_w	:=	ie_tipo_atendimento_p;
end if;

if	(nr_seq_episodio_p is null) then
    nr_seq_episodio_w := get_nr_episodio(nr_atendimento_p);
else
	nr_seq_episodio_w		:=	nr_seq_episodio_p;
end if;

if	(nr_seq_tipo_episodio_p is null) then	
	begin
	select	nr_seq_tipo_episodio
	into	nr_seq_tipo_episodio_w
	from	episodio_paciente
	where	nr_sequencia = nr_seq_episodio_w;
	exception
	when others then
		nr_seq_tipo_episodio_w	:=	null;
	end;
else	
	nr_seq_tipo_episodio_w	:=	nr_seq_tipo_episodio_p;
end if;

begin
select	ie_tipo
into	ie_tipo_atend_case_w
from	tipo_episodio
where	nr_sequencia = nr_seq_tipo_episodio_w;
exception
when others then
	ie_tipo_atend_case_w	:=	null;
end;

if	(ie_tipo_atendimento_w in ('1','8')) then
	ie_tipo_atend_retorno_w	:=	ie_tipo_atendimento_w;
else
	ie_tipo_atend_retorno_w	:=	nvl(ie_tipo_atend_case_w, ie_tipo_atendimento_w);
end if;

return	ie_tipo_atend_retorno_w;

end get_case_encounter_type;
/