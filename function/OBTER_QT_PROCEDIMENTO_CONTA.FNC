create or replace
function Obter_Qt_Procedimento_Conta
		(	nr_interno_conta_p	Number,
			cd_procedimento_p	Number,
			ie_origem_proced_p	Number)
 		    	return Number is
			
qt_retorno_w		Number(5)	:= 0;

begin

select	sum(nvl(qt_procedimento,0))
into	qt_retorno_w
from	procedimento_paciente
where	nr_interno_conta	= nr_interno_conta_p
and	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p
and	cd_motivo_exc_conta is null;

return	qt_retorno_w;

end Obter_Qt_Procedimento_Conta;
/