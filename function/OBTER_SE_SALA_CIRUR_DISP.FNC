create or replace
function obter_se_sala_cirur_disp(	cd_setor_atendimento_p		number,
					cd_unidade_basica_p		varchar2,
					cd_unidade_compl_p		varchar2,
					nr_atendimento_p		number)
					return varchar2 is
				
ie_disponivel_w		varchar2(1) := 'S';
nr_atendimento_w	number(10);
ds_classificacoes_w		varchar2(255) := '';

begin
if	(obter_funcao_ativa = 3111) then
	Obter_param_Usuario(3111, 306, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ds_classificacoes_w);
end if;

select	nvl(max('N'),'S'),
	nvl(max(nr_atendimento),0)
into	ie_disponivel_w,
	nr_atendimento_w
from	atend_paciente_unidade
where	cd_setor_atendimento	=	cd_setor_atendimento_p
and	cd_unidade_basica	=	cd_unidade_basica_p
and	cd_unidade_compl	=	cd_unidade_compl_p
and	((nvl(ds_classificacoes_w,'XPTO') = 'XPTO') or (obter_se_contido_entre_virgula(ds_classificacoes_w, obter_classif_setor(cd_setor_atendimento)) = 'N'))
and	dt_entrada_unidade > sysdate - 3
and	dt_saida_unidade is null;

if	(ie_disponivel_w = 'N') and
	(nr_atendimento_w > 0) and
	(nr_atendimento_w = nr_atendimento_p) then
	ie_disponivel_w:= 'S';
end if;		

return ie_disponivel_w;

end obter_se_sala_cirur_disp;
/

