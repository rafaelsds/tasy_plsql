Create or Replace
FUNCTION HSL_OBTER_DADOS_DEM_JUR	(dt_referencia_p	date,
					cd_tipo_portador_p	varchar2,
					ie_opcao_p		varchar2)
					RETURN varchar2 IS

/*	ie_opcao_p

'SA'	Saldo anterior
'BX'	Baixas			(-)
'CH'	Cheques			(-)
'PE'	Perdas			(-)
'SJ'	Sa�da jur�dico		(-)
'RP'	Recupera��o de perdas
'JR'	Juros
'IN'	Inclus�es		(+)
'SF'	Saldo final

*/

ds_retorno_w		varchar2(255);
vl_original_w		number(15,2);
vl_alteracao_w		number(15,2);
vl_saldo_anterior_w	number(15,2);
vl_baixas_w		number(15,2);
vl_cheques_w		number(15,2);
vl_perdas_w		number(15,2);
vl_saida_juridico_w	number(15,2);
vl_recuperacao_perdas_w	number(15,2);
vl_juros_w		number(15,2);
vl_inclusoes_w		number(15,2);

dt_anterior_w		date;
dt_inicial_w		date;
dt_final_w		date;

BEGIN

dt_anterior_w	:= fim_dia(last_day(add_months(trunc(dt_referencia_p,'month'),-1)));
dt_inicial_w	:= trunc(dt_referencia_p,'month');
dt_final_w	:= fim_dia(last_day(dt_referencia_p));

if	(ie_opcao_p = 'SA') or (ie_opcao_p = 'SF') then

	select	sum(vl_documento) vl_original
	into	vl_original_w
	from	(select	a.vl_titulo vl_documento
		from	titulo_receber a
		where	obter_portador_titulo_data(a.nr_titulo,dt_anterior_w,2) = cd_tipo_portador_p
		and	a.ie_situacao	not in ('3','5')
		and	a.dt_emissao	<= dt_anterior_w
		union all
		select	a.vl_cheque vl_documento
		from	cheque_cr a
		where	nvl(obter_portador_cheque_data(a.nr_seq_cheque,dt_anterior_w,'CT'),a.cd_tipo_portador) = cd_tipo_portador_p
		and	obter_status_cheque(a.nr_seq_cheque) not in ('4')
		and	a.ie_origem_cheque = 'P'
		and	(a.dt_devolucao is null or a.dt_devolucao > dt_anterior_w)
		and	a.dt_contabil	<= dt_anterior_w);

	select	sum(decode(a.ie_aumenta_diminui,'A',a.vl_alteracao,a.vl_alteracao * -1)) vl_alteracao
	into	vl_alteracao_w
	from	titulo_receber b,
		alteracao_valor a
	where	b.ie_situacao		not in ('3','5')
	and	a.nr_titulo		= b.nr_titulo
	and	a.dt_alteracao		<= dt_anterior_w;

end if;

if	(ie_opcao_p = 'SA') or (ie_opcao_p = 'SF') then

	/* baixas */
	select	sum(a.vl_recebido)
	into	vl_baixas_w
	from	titulo_receber_liq a
	where	a.dt_recebimento	<= dt_anterior_w;

	/* cheques */
	select	sum(vl_baixas) baixas
	into	vl_cheques_w
	from	(select	nvl(sum(a.vl_transacao),0) vl_baixas
		from  	cheque_cr b,
			movto_trans_financ a
		where 	obter_status_cheque(b.nr_seq_cheque) not in ('4')
		and	b.ie_origem_cheque	= 'P'
		and	a.nr_seq_cheque		= b.nr_seq_cheque
		and	obter_portador_cheque_data(a.nr_seq_cheque,a.dt_transacao,'CT') = cd_tipo_portador_p
		and	a.dt_transacao		<= dt_anterior_w
		and	a.nr_seq_trans_financ	in (896,897)
		union all
		select	nvl(sum(b.vl_cheque),0) vl_baixas
		from	cheque_cr b,
			deposito_cheque c,
			movto_trans_financ a
		where	obter_portador_cheque_data(b.nr_seq_cheque,a.dt_transacao,'CT') = cd_tipo_portador_p
		and	obter_status_cheque(b.nr_seq_cheque) not in ('4')
		and	b.ie_origem_cheque 	= 'P'
		and	c.nr_seq_cheque		= b.nr_seq_cheque
		and	a.nr_seq_deposito	= c.nr_seq_deposito
		and	a.nr_seq_caixa		is not null
		and	a.dt_transacao		<= dt_anterior_w
		and	a.nr_seq_trans_financ	= 3915
		union all
		select	nvl(sum(a.vl_transacao),0) vl_baixas
		from  	movto_trans_financ a
		where 	a.dt_transacao		<= dt_anterior_w
		and	a.nr_seq_trans_financ	= 3977
		);

	/* perdas */
	select	sum(desc_perdas) desc_perdas
	into	vl_perdas_w
	from	(select	nvl(sum(a.vl_descontos),0) + nvl(sum(a.vl_glosa),0) + nvl(sum(a.vl_perdas),0) + nvl(sum(a.vl_nota_credito),0) desc_perdas
		from	titulo_receber b,
			titulo_receber_liq a
		where	b.ie_situacao		not in ('3','5')
		and	a.nr_titulo		= b.nr_titulo
		and	obter_portador_titulo_data(a.nr_titulo,a.dt_recebimento,2) = '3'
		and	a.dt_recebimento	<= dt_anterior_w
		union all
		select	nvl(sum(a.vl_transacao),0) desc_perdas
		from	movto_trans_financ a
		where	a.dt_transacao		<= dt_anterior_w
		and	a.nr_seq_trans_financ	in (3976,4865,4885,4886,4889,4125)
		);

	vl_saldo_anterior_w	:= nvl(vl_original_w,0) - nvl(vl_baixas_w,0) - nvl(vl_cheques_w,0) - nvl(vl_perdas_w,0) + nvl(vl_alteracao_w,0);

end if;

if	(ie_opcao_p = 'BX') or (ie_opcao_p = 'SF') then

	select	sum(a.vl_recebido)
	into	vl_baixas_w
	from	titulo_receber_liq a
	where	a.dt_recebimento	between dt_inicial_w and dt_final_w;

end if;

if	(ie_opcao_p = 'CH') or (ie_opcao_p = 'SF') then

	select	sum(vl_baixas) baixas
	into	vl_cheques_w
	from	(select	nvl(sum(a.vl_transacao),0) vl_baixas
		from  	cheque_cr b,
			movto_trans_financ a
		where 	obter_status_cheque(b.nr_seq_cheque) not in ('4')
		and	b.ie_origem_cheque	= 'P'
		and	a.nr_seq_cheque		= b.nr_seq_cheque
		and	obter_portador_cheque_data(a.nr_seq_cheque,a.dt_transacao,'CT') = cd_tipo_portador_p
		and	a.dt_transacao		between dt_inicial_w and dt_final_w
		and	a.nr_seq_trans_financ	in (896,897)
		union all
		select	nvl(sum(b.vl_cheque),0) vl_baixas
		from	cheque_cr b,
			deposito_cheque c,
			movto_trans_financ a
		where	obter_portador_cheque_data(b.nr_seq_cheque,a.dt_transacao,'CT') = cd_tipo_portador_p
		and	obter_status_cheque(b.nr_seq_cheque) not in ('4')
		and	b.ie_origem_cheque 	= 'P'
		and	c.nr_seq_cheque		= b.nr_seq_cheque
		and	a.nr_seq_deposito	= c.nr_seq_deposito
		and	a.nr_seq_caixa		is not null
		and	a.dt_transacao		between dt_inicial_w and dt_final_w
		and	a.nr_seq_trans_financ	= 3915
		union all
		select	nvl(sum(a.vl_transacao),0) vl_baixas
		from  	movto_trans_financ a
		where 	a.dt_transacao		between dt_inicial_w and dt_final_w
		and	a.nr_seq_trans_financ	= 3977
		);

end if;

if	(ie_opcao_p = 'PE') or (ie_opcao_p = 'SF') then

	select	sum(desc_perdas) desc_perdas
	into	vl_perdas_w
	from	(select	nvl(sum(a.vl_descontos),0) + nvl(sum(a.vl_glosa),0) + nvl(sum(a.vl_perdas),0) + nvl(sum(a.vl_nota_credito),0) desc_perdas
		from	titulo_receber b,
			titulo_receber_liq a
		where	b.ie_situacao		not in ('3','5')
		and	a.nr_titulo		= b.nr_titulo
		and	obter_portador_titulo_data(a.nr_titulo,a.dt_recebimento,2) = '3'
		and	a.dt_recebimento	between dt_inicial_w and dt_final_w
		union all
		select	nvl(sum(a.vl_transacao),0) desc_perdas
		from	movto_trans_financ a
		where	a.dt_transacao		between dt_inicial_w and dt_final_w
		and	a.nr_seq_trans_financ	in (3976,4865,4885,4886,4889,4125)
		);

end if;

if	(ie_opcao_p = 'SJ') or (ie_opcao_p = 'SF') then

	select	nvl(sum(vl_tit_cheque),0) transf_saida
	into	vl_saida_juridico_w
	from	(select	sum(a.vl_titulo) vl_tit_cheque
		from	titulo_receber a
		where 	obter_portador_titulo_data(a.nr_titulo,dt_anterior_w,2) = cd_tipo_portador_p
		and	obter_portador_titulo_data(a.nr_titulo,dt_final_w,2) <> cd_tipo_portador_p
		and	a.ie_situacao	not in ('3','5')
		and	a.dt_emissao	<= dt_anterior_w
		union all
		select 	sum(a.vl_cheque) vl_tit_cheque
		from	cheque_cr a
		where	obter_portador_cheque_data(a.nr_seq_cheque,dt_anterior_w,'CT') = cd_tipo_portador_p
		and	obter_portador_cheque_data(a.nr_seq_cheque,dt_final_w,'CT') <> cd_tipo_portador_p
		and	obter_status_cheque(a.nr_seq_cheque) not in ('4')
		and	(a.dt_devolucao is null or a.dt_devolucao > dt_final_w)
		and	a.ie_origem_cheque	= 'P'
		and	a.dt_contabil		<= dt_anterior_w
		);

end if;

if	(ie_opcao_p = 'RP') or (ie_opcao_p = 'SF') then

	select	sum(a.vl_transacao)
	into	vl_recuperacao_perdas_w
	from	movto_trans_financ_v a
	where 	a.dt_transacao	between dt_inicial_w and dt_final_w
	and	a.nr_seq_trans_financ	in (1217,1218,1654,1655);

end if;

if	(ie_opcao_p = 'JR') or (ie_opcao_p = 'SF') then

	select	sum(vl_juros)
	into	vl_juros_w
	from	titulo_receber b,
		titulo_receber_liq a
	where	b.ie_situacao		not in ('3','5')
	and	a.nr_titulo		= b.nr_titulo
	and	obter_portador_titulo_data(a.nr_titulo,a.dt_recebimento,2) = cd_tipo_portador_p
	and	a.dt_recebimento	between dt_inicial_w and dt_final_w;

end if;

if	(ie_opcao_p = 'IN') or (ie_opcao_p = 'SF') then

	select	sum(vl_inclusao)
	into	vl_inclusoes_w
	from	(select	sum(a.vl_titulo) vl_inclusao
		from	titulo_receber a
		where 	obter_portador_titulo_data(a.nr_titulo,dt_anterior_w,2) <> cd_tipo_portador_p
		and	obter_portador_titulo_data(a.nr_titulo,dt_final_w,2) = cd_tipo_portador_p
		and	a.ie_situacao	not in ('3','5')
		and	a.dt_emissao	<= dt_anterior_w
		union all
		select 	sum(a.vl_cheque) vl_inclusao
		from	cheque_cr a
		where	obter_portador_cheque_data(a.nr_seq_cheque,dt_anterior_w,'CT') <> cd_tipo_portador_p
		and	obter_portador_cheque_data(a.nr_seq_cheque,dt_final_w,'CT') = cd_tipo_portador_p
		and	obter_status_cheque(a.nr_seq_cheque) not in ('4')
		and	(a.dt_devolucao is null or a.dt_devolucao > dt_final_w)
		and	a.ie_origem_cheque	= 'P'
		and	a.dt_contabil		<= dt_anterior_w
		union all
		/* novos t�tulos e cheques */
		select	a.vl_titulo vl_inclusao
		from	titulo_receber a
		where	obter_portador_titulo_data(a.nr_titulo,dt_final_w,2) = cd_tipo_portador_p
		and	a.ie_situacao	not in ('3','5')
		and	a.dt_emissao	between dt_inicial_w and dt_final_w
		union all
		select	a.vl_cheque vl_inclusao
		from	cheque_cr a
		where	nvl(obter_portador_cheque_data(a.nr_seq_cheque,dt_final_w,'CT'),a.cd_tipo_portador) = cd_tipo_portador_p
		and	obter_status_cheque(a.nr_seq_cheque) not in ('4')
		and	a.ie_origem_cheque = 'P'
		and	(a.dt_devolucao is null or a.dt_devolucao > dt_final_w)
		and	a.dt_contabil	between dt_inicial_w and dt_final_w);

	select	sum(decode(a.ie_aumenta_diminui,'A',a.vl_alteracao,a.vl_alteracao * -1)) vl_alteracao
	into	vl_alteracao_w
	from	titulo_receber b,
		alteracao_valor a
	where	b.ie_situacao		not in ('3','5')
	and	a.nr_titulo		= b.nr_titulo
	and	a.dt_alteracao		between dt_inicial_w and dt_final_w;

	vl_inclusoes_w	:= nvl(vl_inclusoes_w,0) + nvl(vl_alteracao_w,0);

end if;

if	(ie_opcao_p = 'SA') then
	ds_retorno_w	:= nvl(vl_saldo_anterior_w,0);
elsif	(ie_opcao_p = 'BX') then
	ds_retorno_w	:= nvl(vl_baixas_w,0);
elsif	(ie_opcao_p = 'CH') then
	ds_retorno_w	:= nvl(vl_cheques_w,0);
elsif	(ie_opcao_p = 'PE') then
	ds_retorno_w	:= nvl(vl_perdas_w,0);
elsif	(ie_opcao_p = 'SJ') then
	ds_retorno_w	:= nvl(vl_saida_juridico_w,0);
elsif	(ie_opcao_p = 'RP') then
	ds_retorno_w	:= nvl(vl_recuperacao_perdas_w,0);
elsif	(ie_opcao_p = 'JR') then
	ds_retorno_w	:= nvl(vl_juros_w,0);
elsif	(ie_opcao_p = 'IN') then
	ds_retorno_w	:= nvl(vl_inclusoes_w,0);
elsif	(ie_opcao_p = 'SF') then
	ds_retorno_w	:=	nvl(vl_saldo_anterior_w,0) - nvl(vl_baixas_w,0) - nvl(vl_cheques_w,0) -
				nvl(vl_perdas_w,0) - nvl(vl_saida_juridico_w,0) + nvl(vl_inclusoes_w,0);
end if;

RETURN	ds_retorno_w;

END HSL_OBTER_DADOS_DEM_JUR;
/