create or replace
function Cih_Obter_Dados_Cirurgia(nr_ficha_ocorrencia_p	Number,
				ie_opcao_p		Number)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(255);		
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(15);

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced
	from	cih_cirurgia
	where	nr_ficha_ocorrencia	= nr_ficha_ocorrencia_p
	order by 1;
			
begin
open C01;
loop
fetch C01 into	
	cd_procedimento_w,
	ie_origem_proced_w;
exit when C01%notfound;
	begin
	ds_Retorno_w	:= '';
	end;
end loop;
close C01;

if	(ie_opcao_p	= 1) then
	ds_retorno_w	:= cd_procedimento_w;
elsif	(ie_opcao_p	= 2) then
	ds_retorno_w	:= ie_origem_proced_w;
elsif	(ie_opcao_p	= 3) then
	ds_retorno_w	:= substr(OBTER_DESCRICAO_PROCEDIMENTO(cd_procedimento_w,ie_origem_proced_w),1,255);

end if;


return	ds_Retorno_w;

end Cih_Obter_Dados_Cirurgia;
/
