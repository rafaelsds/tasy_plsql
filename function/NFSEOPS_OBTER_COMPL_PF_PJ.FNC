create or replace
function nfseops_obter_compl_pf_pj(cd_pessoa_fisica_p	varchar2,
				cd_cgc_p		varchar2,
				nr_seq_mensalidade_p	number,
				ie_opcao_p		varchar2)	
				return varchar2 is
				

ds_retorno_w		varchar2(255);
ie_complemento_w		varchar2(1);

begin

ie_complemento_w := 	nfse_ops_compl_pf_pj(nr_seq_mensalidade_p, cd_cgc_p);

if	(cd_pessoa_fisica_p is not null) then
	ds_retorno_w := nfse_obter_compl_pf(cd_pessoa_fisica_p, ie_opcao_p);
elsif	((cd_cgc_p is not null) and (ie_complemento_w <> '9')) then
	ds_retorno_w := obter_compl_pj(cd_cgc_p, ie_complemento_w, ie_opcao_p);
elsif	((cd_cgc_p is not null) and (ie_complemento_w = '9')) then
	ds_retorno_w := obter_dados_pf_pj(cd_pessoa_fisica_p, cd_cgc_p, ie_opcao_p);
end if;

return	ds_retorno_w;

end nfseops_obter_compl_pf_pj;
/
