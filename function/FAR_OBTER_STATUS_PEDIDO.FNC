create or replace
function	far_obter_status_pedido(
		nr_seq_pedido_p		number) return varchar2 is

ds_retorno_w		varchar2(255);
dt_pedido_w		date;
dt_inicio_atendimento_w	date;
dt_fechamento_w		date;
dt_saida_w		date;
dt_entrega_real_w		date;
ie_entregar_w		varchar2(1);

begin

select	dt_pedido,
	dt_inicio_atendimento,
	dt_fechamento,
	dt_saida,
	dt_entrega_real,
	nvl(ie_entregar,'N')
into	dt_pedido_w,
	dt_inicio_atendimento_w,
	dt_fechamento_w,
	dt_saida_w,
	dt_entrega_real_w,
	ie_entregar_w
from	far_pedido_v
where	nr_sequencia = nr_seq_pedido_p;

if	(ie_entregar_w = 'S') then
	begin

	if	(dt_entrega_real_w is not null) then
		ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308429); --'Atendido/Entregue';

	elsif	(dt_saida_w is not null) then
		ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308430); --'Saiu para entrega';

	elsif	(dt_fechamento_w is not null) then
		ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308431); --'Pendente de entrega';

	elsif	(dt_inicio_atendimento_w is not null) then
		ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308432); --'Em atendimento';

	elsif	(dt_pedido_w is not null) then
		ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308433); --'Aguardando atendimento';
	end if;	

	end;
else
	begin

	if	(dt_fechamento_w is not null) then
		ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308429); --'Atendido/Entregue';

	elsif	(dt_inicio_atendimento_w is not null) then
		ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308432); --'Em atendimento';

	elsif	(dt_pedido_w is not null) then
		ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308433); --'Aguardando atendimento';
	end if;	

	end;
end if;

return	ds_retorno_w;

end far_obter_status_pedido;
/