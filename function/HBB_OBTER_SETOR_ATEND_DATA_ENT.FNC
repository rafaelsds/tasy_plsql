create or replace 
function hbb_obter_setor_atend_data_ent	(
			nr_atendimento_p	number,
			dt_referencia_p		date,
			ie_opcao_p		varchar2)
			return varchar2 is


cd_setor_atendimento_w		number(10,0);
cd_unidade_basica_w		unidade_atendimento.cd_unidade_basica%type;
cd_unidade_compl_w		unidade_atendimento.cd_unidade_compl%type;

ds_retorno_w			varchar2(15);

/*
ie_opcao_p

S = C�d. Setor
UB = Unidade b�sica
UC = Unidade complementar
L = Leito (unidade b�sica + unidade complementar)

*/

begin

if	(nvl(nr_atendimento_p,0) > 0) then

	select	max(cd_setor_atendimento),
		max(cd_unidade_basica),
		max(cd_unidade_compl)
	into	cd_setor_atendimento_w,
		cd_unidade_basica_w,
		cd_unidade_compl_w
	from 	atend_paciente_unidade a
	where	a.nr_atendimento = nr_atendimento_p
	and 	nr_seq_interno	 = 	(select max(nvl(nr_seq_interno,0))
					from 	Setor_Atendimento y,
						atend_paciente_unidade b
					where 	nr_atendimento 	= nr_atendimento_p
					and 	b.cd_setor_atendimento	= y.cd_setor_atendimento
					and	b.dt_entrada_unidade <= dt_referencia_p
					and	y.cd_classif_setor in (1,3,4,8));

	if	(ie_opcao_p = 'S') then
		ds_retorno_w := cd_setor_atendimento_w;
	elsif	(ie_opcao_p = 'UB') then
		ds_retorno_w := cd_unidade_basica_w;
	elsif	(ie_opcao_p = 'UC') then
		ds_retorno_w := cd_unidade_compl_w;
	elsif	(ie_opcao_p = 'L') then
		ds_retorno_w := cd_unidade_basica_w || ' ' || cd_unidade_compl_w;
	end if;

end if;
	
return ds_retorno_w;

end hbb_obter_setor_atend_data_ent;
/