create or replace
function obter_se_edicao_ativa(	cd_edicao_amb_p		Number)
				return varchar2 is

ie_situacao_w	varchar2(1):= 'I';

begin

select 	nvl(max(ie_situacao),'I')
into	ie_situacao_w
from 	edicao_amb
where 	cd_edicao_amb = cd_edicao_amb_p;

return	ie_situacao_w;

end obter_se_edicao_ativa;
/