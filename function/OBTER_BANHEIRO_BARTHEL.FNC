create or replace
function OBTER_BANHEIRO_BARTHEL(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w 	varchar2(255);
			
begin

select 	DECODE(IE_BANHEIRO,0,'Dependente',5,'Precisa de alguma ajuda mas pode fazer alguma coisa sozinho',10,'Independente (entrar e sair se vestir e limpar-se)')
into	ds_retorno_w
from	escala_barthel
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end OBTER_BANHEIRO_BARTHEL;
/