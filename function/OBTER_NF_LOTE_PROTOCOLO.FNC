create or replace
function obter_nf_lote_protocolo(nr_seq_lote_protocolo_p 	Number)
 		    	return varchar2 is

nr_nota_w		varchar2(255);
ds_nota_w		varchar2(4000) := ' ';


cursor C01 is
select	nr_nota_fiscal
from	nota_fiscal
where	(nr_seq_lote_protocolo_p  <> 0) 
and	ie_situacao = '1'
and	nr_seq_lote_prot = nr_seq_lote_protocolo_p
order by 1;

BEGIN

open C01;
loop
fetch C01 into nr_nota_w;
	exit when C01%notfound;
	if	(length(ds_nota_w) < 3600) then
		ds_nota_w := substr(ds_nota_w || nr_nota_w ||', ',1,4000);
	end if;
end loop;
close C01;

RETURN substr(ds_nota_w,1,length(ds_nota_w) - 2);

end obter_nf_lote_protocolo;
/