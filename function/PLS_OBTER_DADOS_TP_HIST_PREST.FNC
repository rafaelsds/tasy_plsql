create or replace
function pls_obter_dados_tp_hist_prest
			(	nr_seq_tipo_hist_p		number,
				ie_opcao_p			varchar2)
				return varchar2 is

/*	D - Descri��o
	E - Exibe prestador
*/
				
ds_retorno_w			varchar2(255);
ds_tipo_hist_w			varchar2(255);
ie_wxibe_w			varchar2(20);

begin

select	ds_tipo,
	decode(ie_exibe_prestador,'S','Sim','N�o')
into	ds_tipo_hist_w,
	ie_wxibe_w
from	pls_tipo_hist_prestador
where	nr_sequencia	= nr_seq_tipo_hist_p;

if	(ie_opcao_p	= 'D') then
	ds_retorno_w	:= ds_tipo_hist_w;
elsif	(ie_opcao_p	= 'E') then
	ds_retorno_w	:= ie_wxibe_w;
end if;

return	ds_retorno_w;

end pls_obter_dados_tp_hist_prest;
/