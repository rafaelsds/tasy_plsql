create or replace
function obter_qt_dependencia_obj(	ds_objeto_p	in varchar2)
				return number is 
				
type colunas is record (ds_objeto_w 	varchar2(30));

type vetor is table of colunas index by binary_integer;

vetor_w		vetor;
vetor_aux_w	vetor;

ds_objeto_ww	varchar2(30);
ds_objeto_www	varchar2(30);

i  		number(10);
j		number(10);
cont_w 		number(10);
qt_obj_w		number(10);
ie_existe_w	varchar2(1);

cursor c01 is
select	name
from	user_dependencies
where	referenced_name like ds_objeto_ww;

begin
ds_objeto_ww := ds_objeto_p;
cont_w := 0;

open c01;
loop
fetch c01 into	
	ds_objeto_www;
exit when c01%notfound;
	begin
	vetor_w(cont_w).ds_objeto_w := ds_objeto_www;
	cont_w := cont_w + 1;
	end;
end loop;
close c01;

i := 0;
while (i < (vetor_w.count)) loop
	begin
	ds_objeto_ww := vetor_w(i).ds_objeto_w;
	open c01;
	loop
	fetch c01 into	
		ds_objeto_www;
	exit when c01%notfound;
		begin
		ie_existe_w := 'N';
		
		for j in 0..vetor_w.count-1 loop
			begin
			if	(vetor_w(j).ds_objeto_w like ds_objeto_www) then
				ie_existe_w := 'S';
			end if;
			end;
		end loop;
		
		if	(ie_existe_w = 'N') then
			begin
			vetor_w(cont_w).ds_objeto_w := ds_objeto_www;
			cont_w := cont_w + 1;
			end;
		end if;
		end;
	end loop;
	close c01;
	
	i := i+1;
	end;
end loop;

qt_obj_w := vetor_w.count;

return qt_obj_w;
end obter_qt_dependencia_obj;
/