create or replace
function obter_se_prescr_proc_atend_pa( cd_estabelecimento_p	varchar2,
					nr_atendimento_p	number) return varchar2 is
					
					
IE_CONS_PRESCR_PROCED_PA_w	varchar2(1);
ds_retorno_w			varchar2(1) := 'N';

ie_mat_w			char(1);
ie_proced_w			char(1);
ie_proced_pend_w	char(1);
begin

select 	nvl(max(IE_CONS_PRESCR_PROCED_PA),'N')
into	IE_CONS_PRESCR_PROCED_PA_w
from	parametro_medico
where	cd_estabelecimento = cd_estabelecimento_p;

if	(IE_CONS_PRESCR_PROCED_PA_w = 'S') then

	select 	nvl(max('S'), 'N')
	into	ie_proced_w
	from	prescr_procedimento a,
            prescr_medica b	
	where	a.nr_prescricao = b.nr_prescricao
	and	a.ie_suspenso = 'N'
	and	b.nr_atendimento = nr_atendimento_p;
	
	select 	nvl(max('S'), 'N')
	into	ie_mat_w
	from	prescr_material a,
            prescr_medica b	
	where	a.nr_prescricao = b.nr_prescricao
	and	b.nr_atendimento = nr_atendimento_p;
	
	if	(ie_proced_w = 'S') and
		(ie_mat_w = 'N') then
	
		select 	nvl(max('S'), 'N')
		into	ie_proced_pend_w
		from	prescr_procedimento a,
                prescr_medica b	
		where	a.nr_prescricao = b.nr_prescricao
		and	a.ie_suspenso = 'N'
		and	a.cd_motivo_baixa = 0
		and	b.nr_atendimento = nr_atendimento_p;	
		
		if	(ie_proced_pend_w = 'N') then
					
			ds_retorno_w	:= 'S';
		
		end if;
	
	
	end if;
end if;


return	ds_retorno_w;

end obter_se_prescr_proc_atend_pa;
/