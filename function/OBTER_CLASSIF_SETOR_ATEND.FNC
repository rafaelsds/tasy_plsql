CREATE OR REPLACE
FUNCTION Obter_classif_setor_atend(		
					nr_atendimento_p		Number)
					RETURN Number IS


cd_setor_atendimento_w	Number(05,0);
cd_classif_setor_w	VARCHAR2(2);

BEGIN

select	max(cd_setor_atendimento)
into	cd_setor_atendimento_w
from	atend_paciente_unidade
where	nr_seq_interno = (
	select Obter_Atepacu_paciente(nr_atendimento_p, 'A')
	from dual);
	
if	(cd_setor_atendimento_w is not null) then
	select  max(cd_classif_setor)
	into	cd_classif_setor_w
	from	setor_atendimento
	where 	cd_setor_atendimento = cd_setor_atendimento_w;
end if;	

RETURN cd_classif_setor_w;

END Obter_classif_setor_atend;
/
