create or replace
function TP_OBTER_NR_SEQUENCIA( CD_PESSOA_FISICA_P   varchar2,
				DT_CHAMADO_P		date)
 		    	return number is

nr_seq_regulacao_w	number;				
				
begin

if ((cd_pessoa_fisica_p is not null) and (dt_chamado_p is not null)) then

select	max(nr_sequencia)
into	nr_seq_regulacao_w
from 	EME_REGULACAO
where 	1 = 1
and 	cd_pessoa_fisica = cd_pessoa_fisica_p
and	trunc(dt_chamado) = trunc(dt_chamado_p);

end if;

return	nr_seq_regulacao_w;

end TP_OBTER_NR_SEQUENCIA;
/
