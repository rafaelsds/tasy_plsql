create or replace
function HD_obter_ultima_infusao (	nr_sequencia_p	number)
				return number is

vl_retorno_w		number(10);
cd_pessoa_fisica_w	number(10);
dt_dialise_w		date;
hr_dialise_w		date;
hr_dialise_peritonial_w	date;		
			
begin
if (nr_sequencia_p is not null) then

	select	cd_pessoa_fisica,
		dt_dialise_peritonial,
		hr_dialise_peritonial
	into	cd_pessoa_fisica_w,
		dt_dialise_w,
		hr_dialise_peritonial_w
	from	hd_dialise_peritonial
	where	nr_sequencia = nr_sequencia_p;

	select	max(hr_dialise_peritonial)
	into	hr_dialise_w
	from	hd_dialise_peritonial
	where	nr_sequencia <> nr_sequencia_p
	and	cd_pessoa_fisica = cd_pessoa_fisica_w
	and	dt_dialise_peritonial = dt_dialise_w
	and	to_date(to_char(sysdate,'dd/mm/yyyy')||''||to_char(hr_dialise_peritonial,'hh24:mi'),'dd/mm/yyyy hh24:mi') <= 
		to_date(to_char(sysdate,'dd/mm/yyyy')||''||to_char(hr_dialise_peritonial_w,'hh24:mi'),'dd/mm/yyyy hh24:mi');

	select	max(qt_volume_infusao)
	into	vl_retorno_w
	from	hd_dialise_peritonial
	where	nr_sequencia <> nr_sequencia_p
	and	cd_pessoa_fisica = cd_pessoa_fisica_w
	and	dt_dialise_peritonial = dt_dialise_w
	and	hr_dialise_peritonial = hr_dialise_w;
end if;

return	vl_retorno_w;

end HD_obter_ultima_infusao;
/