create or replace
function lab_obter_observacao_desaprov(	nr_prescricao_p		number,
					nr_seq_prescr_p		number) 
					return varchar2 is
ds_retorno_w	varchar2(2000);

begin

	SELECT	ds_observacao
	INTO 	ds_retorno_w
	FROM   	prescr_proc_desaprov
	WHERE 	nr_sequencia = (SELECT MAX(nr_sequencia) 
				 FROM prescr_proc_desaprov 
				 WHERE nr_prescricao = nr_prescricao_p
				 AND nr_seq_prescr = nr_seq_prescr_p);

return ds_retorno_w ;

end lab_obter_observacao_desaprov;
/