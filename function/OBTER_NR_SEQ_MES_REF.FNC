create or replace
function obter_nr_seq_mes_ref(cd_estab_p	number,
			dt_referencia_p	date)
 		    	return number is

nr_sequencia_w	number(10,0);

begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	ctb_mes_ref r
where	r.cd_empresa = obter_empresa_estab(cd_estab_p)
and	r.dt_referencia = trunc(dt_referencia_p,'mm')
and	substr(ctb_obter_se_mes_fechado(nr_sequencia,0),1,1) = 'A';

return	nr_sequencia_w;

end obter_nr_seq_mes_ref;
/