create or replace
function pls_obter_dados_anexo_tiss(nr_seq_guia_p		pls_guia_plano.nr_sequencia%type,
				    nr_seq_requisicao_p		pls_requisicao.nr_sequencia%type,
				    ie_tipo_info_p		Varchar2)
				return Varchar2 is
				
retorno_w		Varchar2(255);
nr_seq_segurado_w	Varchar2(255);

/* ie_opcao_p
	NRS = N�mero sequencia segurado
*/


begin

if (nr_seq_guia_p is not null) then

	select	nr_seq_segurado
	into	nr_seq_segurado_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_p;

elsif (nr_seq_requisicao_p is not null) then

	select	nr_seq_segurado
	into	nr_seq_segurado_w
	from	pls_requisicao
	where	nr_sequencia = nr_seq_requisicao_p;
	
end if;

if (ie_tipo_info_p = 'NRS') then
	retorno_w := nr_seq_segurado_w;
end if;


return	retorno_w;

end pls_obter_dados_anexo_tiss;
/