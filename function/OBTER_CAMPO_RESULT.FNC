CREATE OR REPLACE
function obter_campo_result(ds_formato_p	varchar2)
		             return	varchar2	is
ds_retorno_w varchar2(2) := '';

begin
	if (ds_formato_p in ('D', 'DL', 'DV', 'MS', 'MSL', 'S', 'SDM', 'SM')) then
		ds_retorno_w := 'D';
	elsif (ds_formato_p = 'P') then
		ds_retorno_w := 'PR';
	elsif (ds_formato_p in ('V', 'CV', 'VP')) then
		ds_retorno_w := 'V';
	end if;

return	ds_retorno_w;

end obter_campo_result;
/
