create or replace function pls_obter_nome_usu_prop (
    nm_usuario_p varchar2
) return varchar2 is
    nm_completo_w pls_solic_credenciame_user.nm_pessoa%type;
begin
    nm_completo_w := '';
    if (nm_usuario_p is not null) then
        select max(ds_usuario)
        into   nm_completo_w
        from   usuario
        where  nm_usuario = nm_usuario_p;
       
        if (nm_completo_w is null or nm_completo_w = '') then
            select max(nm_pessoa)
            into   nm_completo_w
            from   pls_solic_credenciame_user
            where  nm_usuario = nm_usuario_p;
        end if;
    end if;
   
    return nm_completo_w;
end pls_obter_nome_usu_prop;
/
