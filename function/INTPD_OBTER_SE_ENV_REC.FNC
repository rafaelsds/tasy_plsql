create or replace
function intpd_obter_se_env_rec(ie_opcao varchar2)
return 	varchar2 is

ds_valor varchar2(255) := '';

begin

if	(ie_opcao = 'E') then
	ds_valor := substr(wheb_mensagem_pck.get_texto(736527),1,50);
elsif	(ie_opcao = 'R') then
	ds_valor := substr(wheb_mensagem_pck.get_texto(736528),1,50);
elsif	(ie_opcao = 'C') then
	ds_valor := substr(wheb_mensagem_pck.get_texto(1040659),1,50);
end if;

return ds_valor;

end	intpd_obter_se_env_rec;
/
