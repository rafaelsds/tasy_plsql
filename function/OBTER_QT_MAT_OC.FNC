create or replace
function obter_qt_mat_oc(cd_material_p		number)
 		    	return number is

ds_retorno_w      number;
			
begin

if (cd_material_p is not null) then
	select	nvl(sum(c.qt_prevista_entrega - nvl(c.qt_real_entrega,0)),0)
	into 	ds_retorno_w
	from	ordem_compra a,
		ordem_compra_item b,
		ordem_compra_item_entrega c
	where	a.nr_ordem_compra = b.nr_ordem_compra
	and	b.nr_ordem_compra = c.nr_ordem_compra
	and	b.nr_item_oci = c.nr_item_oci
	and	a.dt_baixa is null
	and	a.nr_seq_motivo_cancel is null
	and	c.dt_cancelamento is null
	and	a.cd_estabelecimento = 1
	and	b.cd_material = cd_material_p;
end if;	

return	ds_retorno_w;

end obter_qt_mat_oc;
/