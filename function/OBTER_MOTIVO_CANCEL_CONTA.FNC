create or replace
function obter_motivo_cancel_conta(nr_seq_motivo_cancel_p	Number)
 		    	return Varchar2 is

ds_retorno_w	varchar2(200);			
			
begin

select	max(ds_motivo)
into	ds_retorno_w
from	motivo_cancel_conta
where	nr_sequencia = nr_seq_motivo_cancel_p;

return	ds_retorno_w;

end obter_motivo_cancel_conta;
/