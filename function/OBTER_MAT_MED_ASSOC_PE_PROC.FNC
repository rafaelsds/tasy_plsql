create or replace
function obter_mat_med_assoc_pe_proc	(nr_prescricao_p	number,
						nr_seq_item_p		number)
						return varchar2 is

qt_dose_w		number(18,6);
cd_um_dose_w		varchar2(30);
ds_matmed_w		varchar2(40);
ds_associados_w		varchar2(4000) := null;

cursor c01 is
select	substr(obter_desc_material(cd_material),1,40) ds_componente,
	qt_dose,
	cd_unidade_medida_dose
from	prescr_material
where	nr_prescricao = nr_prescricao_p
and	nr_seq_pe_proc = nr_seq_item_p
and	ie_agrupador in (1,2)
order by
	1;

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_item_p is not null) then
	open c01;
	loop
	fetch c01 into
		ds_matmed_w,
		qt_dose_w,
		cd_um_dose_w;
	exit when c01%notfound;
		begin
		if	(ds_associados_w is null) then
			ds_associados_w := qt_dose_w || ' - ' || cd_um_dose_w || ' - ' || ds_matmed_w;
		elsif	(ds_associados_w is not null) then
			ds_associados_w := ds_associados_w || chr(10) || qt_dose_w || ' - ' || cd_um_dose_w || ' - ' || ds_matmed_w;
		end if;
		end;
	end loop;
	close c01;
end if;

return ds_associados_w;

end obter_mat_med_assoc_pe_proc;
/
