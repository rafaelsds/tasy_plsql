create or replace
function obter_habilita_trans_nf(	nr_sequencia_p		varchar2,
					ie_tipo_nota_p		varchar2)
					return varchar2 is

ds_retorno_w			varchar2(1) := 'S';
qt_transmissao_w		number(10);
ie_tipo_transmissao_w		nfe_transmissao.ie_tipo_transmissao%type;
ie_status_transmissao_w		nfe_transmissao.ie_status_transmissao%type;
nr_protocolo_w			nota_fiscal_lote_nfe.nr_protocolo%type;	

begin

select	count(a.nr_sequencia)
into	qt_transmissao_w
from	nfe_transmissao a,
	nfe_transmissao_nf b
where	a.nr_sequencia = b.nr_seq_transmissao
and	b.nr_seq_nota_fiscal = nr_sequencia_p
and	a.ie_tipo_nota = ie_tipo_nota_p;

if	(qt_transmissao_w > 0) then
	begin
	
	select  ie_tipo_transmissao,
		ie_status_transmissao,
		nr_protocolo
	into	ie_tipo_transmissao_w,
		ie_status_transmissao_w,
		nr_protocolo_w
	from
	(Select nvl(a.ie_tipo_transmissao,1) ie_tipo_transmissao,
		nvl(a.ie_status_transmissao,'E') ie_status_transmissao,
		substr(obter_protocolo_trans(a.nr_sequencia),1,255) nr_protocolo
	from	nfe_transmissao a,
		nfe_transmissao_nf b
	where	a.nr_sequencia = b.nr_seq_transmissao
	and	b.nr_seq_nota_fiscal = nr_sequencia_p
	and	a.ie_tipo_nota = ie_tipo_nota_p
	order by a.nr_sequencia desc)
	where	rownum <= 1;
	
	if	(ie_tipo_transmissao_w = 1) and 
		(ie_status_transmissao_w = 'T') and
		(nr_protocolo_w is not null) then
		begin
		
		ds_retorno_w := 'N';
		
		end;
	end if;
	
	end;
end if;

return	ds_retorno_w;

end obter_habilita_trans_nf;
/