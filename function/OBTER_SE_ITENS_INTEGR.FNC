create or replace
function	obter_se_itens_integr(nr_seq_formato_p	number,
			    cd_equipamento_p	varchar2,
			    ds_sigla_p		varchar2)
				return number is

qt_exame_w	number(5);

begin

select 	nvl(count(*),0)
into	qt_exame_w
from	exame_lab_format_item a,
	lab_exame_equip b
where 	a.nr_seq_formato = nr_seq_formato_p
and	b.nr_seq_exame = a.nr_seq_exame
and	obter_equipamento_exame(a.nr_seq_exame,null,ds_sigla_p) is not null
and	b.cd_equipamento = cd_equipamento_p;


return  qt_exame_w;

end;
/