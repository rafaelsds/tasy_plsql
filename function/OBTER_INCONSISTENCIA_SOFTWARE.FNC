create or replace
function obter_inconsistencia_software(nr_seq_inconsistencia_p in number) 
			return varchar2 is

ds_retorno_w varchar2(512);

begin

if	(nr_seq_inconsistencia_p is not null) then
	select	max(ds_inconsistencia)
	into	ds_retorno_w
	from	inconsistencia_software
	where	nr_sequencia = nr_seq_inconsistencia_p;
end if;

return ds_retorno_w;

end	obter_inconsistencia_software;
/
