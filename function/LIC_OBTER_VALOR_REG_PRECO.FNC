create or replace
function lic_obter_valor_reg_preco(	nr_seq_reg_compra_p		number,
				nr_seq_lic_item_p			number)
 		    	return number is

vl_item_w			number(15,4);			
			
begin

select	nvl(max(vl_item), 0)
into	vl_item_w
from	reg_compra_item
where	nr_seq_reg_compra	= nr_seq_reg_compra_p
and	nr_seq_lic_item	= nr_seq_lic_item_p
and	dt_cancelamento is null;


return	vl_item_w;

end lic_obter_valor_reg_preco;
/