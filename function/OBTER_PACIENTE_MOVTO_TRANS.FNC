create or replace
function obter_paciente_movto_trans(nr_titulo_p			number,
				    nr_seq_caixa_receb_p	number,
				    nr_adiantamento_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
begin

if 	(nr_titulo_p is not null) then
	select	obter_nome_pf(b.cd_pessoa_fisica)
	into	ds_retorno_w
	from	titulo_receber a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	a.nr_titulo = nr_titulo_p;
elsif	(nr_adiantamento_p is not null) then
	select 	substr(obter_pessoa_atendimento(max(a.nr_atendimento),'N'),1,255)
	into	ds_retorno_w
	from	adiantamento a
	where	a.nr_adiantamento = nr_adiantamento_p;
elsif	(nr_seq_caixa_receb_p is not null) then
	select 	substr(obter_pessoa_atendimento(max(a.nr_atendimento),'N'),1,255)
	into	ds_retorno_w
	from	titulo_receber a,
		titulo_receber_liq b
	where	a.nr_titulo = b.nr_titulo
	and	b.nr_seq_caixa_rec = nr_seq_caixa_receb_p;
	
	if ds_retorno_w = '' then
		select 	substr(obter_pessoa_atendimento(max(a.nr_atendimento),'N'),1,255)
		into	ds_retorno_w
		from	adiantamento a
		where	a.nr_seq_caixa_rec = nr_seq_caixa_receb_p;
	end if;
end if;

return	ds_retorno_w;

end obter_paciente_movto_trans;
/