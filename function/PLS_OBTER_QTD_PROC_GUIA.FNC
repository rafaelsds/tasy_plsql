create or replace
function pls_obter_qtd_proc_guia
			(	nr_seq_guia_p		number)
				return number is

qt_retorno_w		number;

begin

select	count(*)
into	qt_retorno_w
from	pls_guia_plano_proc
where	nr_seq_guia	= nr_seq_guia_p;

return	qt_retorno_w;

end pls_obter_qtd_proc_guia; 
/