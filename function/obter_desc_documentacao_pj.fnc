CREATE OR REPLACE FUNCTION obter_desc_documentacao_pj(ie_tipo_item_p IN VARCHAR2) RETURN VARCHAR2 IS
    ds_retorno_w VARCHAR2(4000) := NULL;
BEGIN

    SELECT obter_desc_expressao((SELECT MAX(cd_exp_informacao)
                                  FROM itens_pj_documentacao
                                 WHERE ie_tipo_item = ie_tipo_item_p))
      INTO ds_retorno_w
      FROM dual;

    RETURN ds_retorno_w;

END;
/
