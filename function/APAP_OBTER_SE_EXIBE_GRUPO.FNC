create or replace
function Apap_Obter_Se_Exibe_Grupo	(	nr_seq_grupo_apap_p	number,
						nm_usuario_p		varchar2)
 		    	return varchar2 is

			
			
qt_reg_w	number(10);
ie_profissional_w	varchar2(15);
begin

if	(nr_seq_grupo_apap_p	is not null) and
	(nm_usuario_p	is not null) then
	
	select	count(*)
	into	qt_reg_w
	from	pep_apap_grupo_regra
	where	nr_seq_grupo_apap	= nr_seq_grupo_apap_p;
	
	if	(qt_reg_w	> 0) then
	
		select	max(ie_profissional)
		into	ie_profissional_w
		from	usuario
		where	nm_usuario	= nm_usuario_p;
	
		select	count(*)
		into	qt_reg_w
		from	pep_apap_grupo_regra
		where	nr_seq_grupo_apap	= nr_seq_grupo_apap_p
		and	ie_profissional = nvl(ie_profissional_w,ie_profissional);
		
		if	(qt_reg_w	= 0) then
			return 'N';
		end if;
	end if;
	
end if;
return	'S';

end Apap_Obter_Se_Exibe_Grupo;
/