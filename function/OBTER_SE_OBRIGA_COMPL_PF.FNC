create or replace
function OBTER_SE_OBRIGA_COMPL_PF(
			cd_estabelecimento_p	number,
			cd_perfil_p		number,
			ie_tipo_complemento_p	number,
			cd_pessoa_fisica_p		varchar2)
			return varchar2 is

ie_obriga_w		varchar2(100);
qt_idade_w		number(5);

begin

select	substr(obter_idade(dt_nascimento,sysdate,'A'),1,5)
into	qt_idade_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

begin
select	x.ie_obriga
into	ie_obriga_w
from	(select	ie_obriga
	from	regra_compl_pessoa_fisica
	where	cd_estabelecimento = cd_estabelecimento_p
	and	nvl(cd_perfil,nvl(cd_perfil_p,0)) = nvl(cd_perfil_p,0)
	and	((qt_idade_minima is null or qt_idade_maxima is null) or (qt_idade_w between qt_idade_minima and qt_idade_maxima))
	and	ie_tipo_complemento = ie_tipo_complemento_p
	order by cd_perfil,
		cd_setor_atendimento desc,
		nvl(qt_idade_minima,0) desc,
		nvl(qt_idade_maxima,99999)) x
where	rownum < 2;
exception
when others then
	ie_obriga_w := 'N';
end;

return	ie_obriga_w;

end OBTER_SE_OBRIGA_COMPL_PF;
/