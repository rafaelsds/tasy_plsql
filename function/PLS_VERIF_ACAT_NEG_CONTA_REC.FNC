create or replace
function pls_verif_acat_neg_conta_rec ( 	nr_seq_conta_rec_p	pls_rec_glosa_conta.nr_sequencia%type,
						nr_seq_protocolo_p	pls_rec_glosa_protocolo.nr_sequencia%type,
						nm_usuario_p		varchar2)
						return varchar2 is

ds_retorno_w		varchar2(5) := '';
qnt_acatado_w		pls_integer;
qnt_negado_w		pls_integer;
qnt_total_acatado_w	pls_integer;
qnt_total_negado_w	pls_integer;
qt_analise_rec_w	pls_integer;

Cursor C01 (nr_seq_protocolo_pc		pls_rec_glosa_protocolo.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_conta_rec
	from	pls_rec_glosa_conta a
	where	a.nr_seq_protocolo	= nr_seq_protocolo_pc
	and	a.ie_status		!= '3';

begin
qnt_total_acatado_w := 0;
qnt_total_negado_w := 0;

if	(nr_seq_conta_rec_p is not null) then
	select	sum(cont)
	into	qnt_acatado_w
	from	(select	count(1) cont
		from	pls_rec_glosa_proc
		where	nr_seq_conta_rec = nr_seq_conta_rec_p
		and	ie_status = 3
		union all
		select	count(1) cont
		from	pls_rec_glosa_mat
		where	nr_seq_conta_rec = nr_seq_conta_rec_p
		and	ie_status = 3);

	select	sum(cont)
	into	qnt_negado_w
	from	(select	count(1) cont
		from	pls_rec_glosa_proc
		where	nr_seq_conta_rec = nr_seq_conta_rec_p
		and	ie_status = 4
		union all
		select	count(1) cont
		from	pls_rec_glosa_mat
		where	nr_seq_conta_rec = nr_seq_conta_rec_p
		and	ie_status = 4);
		
qnt_total_acatado_w := qnt_acatado_w;
qnt_total_negado_w := qnt_negado_w;

elsif	(nr_seq_protocolo_p is not null) then

	for r_C01_w in C01 (nr_seq_protocolo_p) loop
		qnt_acatado_w := 0;
		qnt_negado_w := 0;
		
		select	sum(cont)
		into	qnt_acatado_w
		from	(select	count(1) cont
			from	pls_rec_glosa_proc
			where	nr_seq_conta_rec = r_C01_w.nr_seq_conta_rec
			and	ie_status = 3
			union all
			select	count(1) cont
			from	pls_rec_glosa_mat
			where	nr_seq_conta_rec = r_C01_w.nr_seq_conta_rec
			and	ie_status = 3);

		select	sum(cont)
		into	qnt_negado_w
		from	(select	count(1) cont
			from	pls_rec_glosa_proc
			where	nr_seq_conta_rec = r_C01_w.nr_seq_conta_rec
			and	ie_status = 4
			union all
			select	count(1) cont
			from	pls_rec_glosa_mat
			where	nr_seq_conta_rec = r_C01_w.nr_seq_conta_rec
			and	ie_status = 4);
		
		qnt_total_acatado_w := qnt_total_acatado_w + qnt_acatado_w;
		qnt_total_negado_w := qnt_total_negado_w + qnt_negado_w;
	end loop;
end if;

if	( qnt_total_acatado_w = 0 ) and
	( qnt_total_negado_w > 0 ) then
	ds_retorno_w := 'N';
elsif	( qnt_total_negado_w = 0 ) and
	( qnt_total_acatado_w > 0 ) then
	ds_retorno_w := 'A';
end if;

return	ds_retorno_w ;

end pls_verif_acat_neg_conta_rec ;
/