create or replace
function obter_lista_depart_medico (cd_setor_atendimento_p	departamento_setor.cd_setor_atendimento%type)
				return varchar2 as

ds_list_medical_depart_w	varchar2(2000) := '';

begin
  for list_medical_depart in ( 
      select obter_nome_departamento_medico(ds.cd_departamento) medical_depart_name
      from   departamento_setor ds
      join   setor_atendimento sa
      on     sa.cd_setor_atendimento = ds.cd_setor_atendimento
      where  ds.cd_setor_atendimento = cd_setor_atendimento_p
      order by 1
  )
  
  loop  
      ds_list_medical_depart_w := ds_list_medical_depart_w || list_medical_depart.medical_depart_name || ', ';
  end loop;

return	substr(ds_list_medical_depart_w, 1, length(ds_list_medical_depart_w) - 2);

end obter_lista_depart_medico;
/