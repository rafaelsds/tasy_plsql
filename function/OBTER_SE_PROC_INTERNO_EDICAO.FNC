create or replace
function obter_se_proc_interno_edicao(	cd_estabelecimento_p	number,
					nr_seq_interno_p	number,
					cd_convenio_p		number,
					cd_categoria_p		varchar2,
					dt_conta_p		date)
 		    	return varchar2 is

cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
ds_retorno_w			varchar2(2);

begin

Obter_Proc_Tab_Interno_Conv(	nr_seq_interno_p,
				cd_estabelecimento_p,
				cd_convenio_p,
				cd_categoria_p,
				null,
				null,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				dt_conta_p,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);
				

select	obter_se_proc_edicao(cd_estabelecimento_p,cd_convenio_p,cd_categoria_p,dt_conta_p,cd_procedimento_w, null)
into	ds_retorno_w
from	dual;



return	ds_retorno_w;

end obter_se_proc_interno_edicao;
/
