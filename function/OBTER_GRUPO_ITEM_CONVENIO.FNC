CREATE OR REPLACE
FUNCTION Obter_Grupo_Item_Convenio(
					cd_convenio_p			number,
					ie_tipo_item_p			number,
					cd_item_p				number,
					ie_origem_proced_p		number,
					cd_especialidade_med_p		number,
					cd_medico_p			varchar2,
					ie_tipo_atendimento_p		number)	
					RETURN VARCHAR2 IS

/* tipo do item    1-Procedimentos/servi�os 2-Materiais/Medicamentos */

cd_grupo_convenio_w	Varchar2(10);
cd_grupo_proc_w		Number(15);
cd_espec_proc_w		Number(15);
cd_area_proc_w		number(15);
cd_grupo_mat_w		Number(03);
cd_subgrupo_mat_w		Number(03);
cd_classe_mat_w		Number(05);
ie_tipo_mat_w		Varchar2(03);

CURSOR C01 IS
	select	cd_grupo
	from		conversao_proc_convenio 
	where		cd_convenio							= cd_convenio_p
	and		nvl(cd_procedimento,cd_item_p) 			= cd_item_p
	and		nvl(ie_origem_proced,ie_origem_proced_p)		= ie_origem_proced_p
	and		nvl(cd_area_proced,cd_area_proc_w)			= cd_area_proc_w	
	and		nvl(cd_especial_proced,cd_espec_proc_w)		= cd_espec_proc_w
	and		nvl(cd_grupo_proced,cd_grupo_proc_w)		= cd_grupo_proc_w
	and		ie_situacao					= 'A'
	order by
			nvl(cd_procedimento,0),
			nvl(cd_grupo_proced,0),
			nvl(cd_especial_proced,0),
			nvl(cd_area_proced,0);

CURSOR C02 IS
	select	cd_grupo
	from		conversao_material_convenio 
	where		cd_convenio							= cd_convenio_p
	and		nvl(cd_material,cd_item_p) 				= cd_item_p
	and		nvl(cd_grupo_material,cd_grupo_mat_w) 		= cd_grupo_mat_w
	and		nvl(cd_subgrupo_material,cd_subgrupo_mat_w) 	= cd_subgrupo_mat_w
	and		nvl(cd_classe_material,cd_classe_mat_w) 		= cd_classe_mat_w
	and		nvl(ie_tipo_material,ie_tipo_mat_w) 	= ie_tipo_mat_w
	order by
			nvl(cd_material,0),
			nvl(cd_grupo_material,0),
			nvl(cd_subgrupo_material,0),
			nvl(cd_classe_material,0),
			nvl(ie_tipo_material,'0');

BEGIN
if	(ie_tipo_item_p	= 1) then
	BEGIN
	/* busca estrutura procedimento */
	begin
	select 	cd_grupo_proc,
			cd_especialidade,
			cd_area_procedimento
	into		cd_grupo_proc_w,
			cd_espec_proc_w,
			cd_area_proc_w
	from		Estrutura_Procedimento_V
	where		cd_procedimento 	= cd_item_p
	and		ie_origem_proced	= ie_origem_proced_p;
	exception
     			when others then
			cd_grupo_proc_w	:= 0;
	end;

	/* busca descri�ao do procedimento para o conv�nio */
	begin
	OPEN C01;
	LOOP
	FETCH C01 into
		cd_grupo_convenio_w;
	exit when c01%notfound;
		begin
		cd_grupo_convenio_w	:= cd_grupo_convenio_w;
		end;
	END LOOP;
	CLOSE C01;
	end;
	END;
else
	BEGIN
	/* busca estrutura material */
	begin
	select 	cd_grupo_material,
			cd_subgrupo_material,
			cd_classe_material,
			ie_tipo_material	
	into		cd_grupo_mat_w,
			cd_subgrupo_mat_w,
			cd_classe_mat_w,
			ie_tipo_mat_w
	from 		estrutura_material_v
	where 	cd_material		= cd_item_p;
	exception
     			when others then
			cd_grupo_mat_w			:= 0;
	end;

	/* busca descri�ao do material para o conv�nio */
	begin
	OPEN C02;
	LOOP
	FETCH C02 into
		cd_grupo_convenio_w;
	exit when c02%notfound;
		begin
		cd_grupo_convenio_w	:= cd_grupo_convenio_w;
		end;
	END LOOP;
	CLOSE C02;
	end;
	END;
end if;
RETURN cd_grupo_convenio_w;
END Obter_Grupo_Item_Convenio;
/
