create or replace
function pls_obter_regra_endereco_benef
			(	nr_seq_segurado_p	number,
				ie_opcao_p		varchar2)
				return varchar2 is
				
/*ie_opcao_p
EN - Endere�o
CO - Complemento
NR - N�mero
B - Bairro
CEP - CEP
DM - Municipio
UF - Estado
T - Telefone
CEL - Celuar
M - E-mail
*/
			
ds_retorno_w			varchar2(255);
nr_seq_pagador_w		number(10);
nr_seq_contrato_w		number(10);
ie_pessoa_endereco_w		varchar2(10);
qt_regras_corresp_w		number(10);
nr_seq_seg_end_corresp_w	number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
ie_endereco_boleto_w		varchar2(10);
ie_endereco_w			number(10);
ie_opcao_w			varchar2(10);

begin

select	nr_seq_pagador,
	nr_seq_contrato
into	nr_seq_pagador_w,
	nr_seq_contrato_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

ie_opcao_w	:= ie_opcao_p;

select	count(*)
into	qt_regras_corresp_w
from	pls_segurado_end_corresp
where	nr_seq_segurado	= nr_seq_segurado_p;

if	(qt_regras_corresp_w > 0) then
	select	max(nr_sequencia)
	into	nr_seq_seg_end_corresp_w
	from	pls_segurado_end_corresp
	where	nr_seq_segurado	= nr_seq_segurado_p;
else
	select	max(nr_sequencia)
	into	nr_seq_seg_end_corresp_w
	from	pls_segurado_end_corresp
	where	nr_seq_contrato	= nr_seq_contrato_w;
end if;

if	(nr_seq_seg_end_corresp_w is not null) then
	select	ie_endereco_boleto,
		ie_pessoa_endereco,
		cd_pessoa_fisica,
		cd_cgc
	into	ie_endereco_boleto_w,
		ie_pessoa_endereco_w,
		cd_pessoa_fisica_w,
		cd_cgc_w		
	from	pls_segurado_end_corresp
	where	nr_sequencia	= nr_seq_seg_end_corresp_w;
	
	if	(ie_endereco_boleto_w in ('PFR','PJC')) then
		ie_endereco_w	:= 1;
	elsif	(ie_endereco_boleto_w in ('PFC','PJF')) then
		ie_endereco_w	:= 2;
	elsif	(ie_endereco_boleto_w in ('PJ','PFV')) THEN
		ie_endereco_w	:= 3;
	elsif	(ie_endereco_boleto_w = 'PFP') THEN
		ie_endereco_w	:= 4;
	elsif	(ie_endereco_boleto_w = 'PFM') THEN
		ie_endereco_w	:= 5;
	elsif	(ie_endereco_boleto_w = 'PFJ') THEN
		ie_endereco_w	:= 6;		
	end if;
	
	if	(cd_pessoa_fisica_w is null) and
		(cd_cgc_w is null) then
		if	(ie_pessoa_endereco_w = 'B') then
			select	cd_pessoa_fisica
			into	cd_pessoa_fisica_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_segurado_p;
		elsif	(ie_pessoa_endereco_w = 'P') then
			select	cd_pessoa_fisica,
				cd_cgc
			into	cd_pessoa_fisica_w,
				cd_cgc_w
			from	pls_contrato_pagador
			where	nr_sequencia	= nr_seq_pagador_w;
		end if;
	end if;
	
	if	(ie_endereco_w	= 3) then
		
		if	(ie_opcao_w = 'DM') then
			ie_opcao_w	:= 'CI';
		end if;
		
		ds_retorno_w		:= substr(obter_dados_pf_pj('',cd_cgc_w,ie_opcao_w),1,255);
	else
		if	(cd_pessoa_fisica_w is not null) then
			ds_retorno_w		:= substr(OBTER_COMPL_PF(cd_pessoa_fisica_w,ie_endereco_w,ie_opcao_w),1,255);
		elsif	(cd_cgc_w is not null) then
		
			if	(ie_opcao_w = 'DM') then
				ie_opcao_w	:= 'CI';
			elsif	(ie_opcao_w = 'EN') then
				ie_opcao_w	:= 'E';
			end if;
			
			ds_retorno_w		:= substr(obter_compl_pj(cd_cgc_w,ie_endereco_w,ie_opcao_w),1,255);
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_regra_endereco_benef;
/