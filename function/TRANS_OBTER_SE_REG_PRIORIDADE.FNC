create or replace
function trans_obter_se_reg_prioridade(	nm_usuario_p	varchar2,
				nr_sequencia_p	number)
 		    	return varchar2 is

cd_pessoa_fisica_w		varchar2(10);
ie_regra_prioridade_w		varchar2(1);
ie_possui_solicitacao_aberta_w	varchar2(1);
nr_sequencia_w			number(10);

begin
cd_pessoa_fisica_w := substr(obter_pf_usuario(nm_usuario_p, 'C'), 10);

select	max(ie_regra_prioridade)
into	ie_regra_prioridade_w
from	trans_solicitacao
where	nr_sequencia = nr_sequencia_p
and	cd_transportador = cd_pessoa_fisica_w;

select	decode(count(*), 0, 'N', 'S')
into	ie_possui_solicitacao_aberta_w
from	trans_solicitacao
where	nr_sequencia <> nr_sequencia_p
and	ie_status not in (8, 9)
and	cd_transportador = cd_pessoa_fisica_w
and	ie_regra_prioridade = 'S'
and	nvl(ie_regra_prioridade_w, 'N') = 'N';

If	(ie_possui_solicitacao_aberta_w = 'S') then
	return 'S';
else
	return 'N';
end if;

end trans_obter_se_reg_prioridade;
/