CREATE OR REPLACE 
FUNCTION	obter_dados_tiss28
			(nr_atendimento_p	number,
			ie_opcao_p		varchar2)
			return varchar2 is

ds_cor_fonte_w	varchar2(80);
ds_cor_fundo_w	varchar2(80);
ds_classe_w	varchar2(255);
qt_pontos_w	number(10) := 0;
ds_descricao_w	varchar2(255);
ie_classe_w	varchar(10);
nr_sequencia_w	number(10);

/*
ie_opcao_p
 CT - Cor fonte
 CD - Cor fundo
 G - Classe
 P - Pontos
 S - Sequencia
*/

BEGIN

select	qt_pontuacao
into	qt_pontos_w
from	tiss_interv_terapeutica
where	nr_sequencia	=	(select	max(nr_sequencia)
				from	tiss_interv_terapeutica
				where	nr_atendimento	= nr_atendimento_p);

if	(qt_pontos_w >= 0) and (qt_pontos_w <= 19) then
	ie_classe_w	:= 'I';
elsif	(qt_pontos_w >= 20) and (qt_pontos_w <= 34) then
	ie_classe_w	:= 'II';
elsif	(qt_pontos_w >= 35) and (qt_pontos_w <= 60) then
	ie_classe_w	:= 'III';
elsif	(qt_pontos_w > 60) then
	ie_classe_w	:= 'IV';
end if;

select	nr_sequencia,
	ds_cor_fonte,
	ds_cor_fundo,
	nvl(trim(ds_gradacao_hospital),substr(obter_valor_dominio(1894, ie_classe),1,60))
into	nr_sequencia_w,
	ds_cor_fonte_w,
	ds_cor_fundo_w,
	ds_classe_w
from	gca_gradacao_tiss28
where	ie_classe 		= ie_classe_w
and	cd_estabelecimento	= obter_estab_atend(nr_atendimento_p);


if	(ie_opcao_p =	'CT') then
	ds_descricao_w	:=	ds_cor_fonte_w;
elsif	(ie_opcao_p =	'CD') then
	ds_descricao_w	:=	ds_cor_fundo_w;
elsif	(ie_opcao_p =	'G') then
	ds_descricao_w	:=	ds_classe_w;
elsif	(ie_opcao_p =	'P') then
	ds_descricao_w	:=	qt_pontos_w;
elsif	(ie_opcao_p =	'S') then
	ds_descricao_w	:=	nr_sequencia_w;
end if;

RETURN ds_descricao_w;

END obter_dados_tiss28;
/
