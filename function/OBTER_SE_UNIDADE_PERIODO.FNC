create or replace 
function obter_se_unidade_Periodo(	nr_sequencia_p		number,
					cd_setor_desejado_p	number,
					dt_prevista_p	date,
					qt_dia_p	number,
					ie_consiste_p	varchar2,
					nr_seq_interno_p	number)
					return varchar2 is
qt_reg_w		number(10);
ie_retorno_w		varchar2(1)	:= 'S';
cd_unidade_compl_w	varchar2(10);
cd_unidade_basica_w	varchar2(10);
nr_atendimento_w	number(10);
dt_previsto_alta_w	date		:= null;

begin

if	(ie_consiste_p = 'S') then

	select	cd_unidade_basica,
		cd_unidade_compl,
		nr_atendimento
	into	cd_unidade_basica_w,
		cd_unidade_compl_w,
		nr_atendimento_w
	from	unidade_atendimento
	where	nr_seq_interno = nr_seq_interno_p;

	if	(nr_atendimento_w is not null) then
		
		select	dt_previsto_alta
		into	dt_previsto_alta_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_w;

		if	(dt_previsto_alta_w is not null) then
	
			if	(dt_previsto_alta_w <= dt_prevista_p) then
	
				SELECT	COUNT(*)
				into	qt_reg_w
				FROM	gestao_vaga
				WHERE	cd_setor_desejado	= cd_setor_desejado_p
				AND	cd_unidade_basica	= cd_unidade_basica_w
				AND	cd_unidade_compl	= cd_unidade_compl_w
				and	nr_sequencia		<> nr_sequencia_p
				AND	((TRUNC(dt_prevista_p)			BETWEEN TRUNC(dt_prevista) AND TRUNC(dt_prevista + qt_dia-1))
				OR	 (TRUNC(dt_prevista_p + qt_dia_p-1)	BETWEEN TRUNC(dt_prevista) AND TRUNC(dt_prevista + qt_dia-1)));
			else
				ie_retorno_W	:= 'N';
			end	if;
		else
			ie_retorno_W	:= 'N';
		end	if;
		
	else
		
		SELECT	COUNT(*)
		into	qt_reg_w
		FROM	gestao_vaga
		WHERE	cd_setor_desejado	= cd_setor_desejado_p
		AND	cd_unidade_basica	= cd_unidade_basica_w
		AND	cd_unidade_compl	= cd_unidade_compl_w
		and	nr_sequencia		<> nr_sequencia_p
		AND	((TRUNC(dt_prevista_p)			BETWEEN TRUNC(dt_prevista) AND TRUNC(dt_prevista + qt_dia-1))
		OR	 (TRUNC(dt_prevista_p + qt_dia_p-1)	BETWEEN TRUNC(dt_prevista) AND TRUNC(dt_prevista + qt_dia-1)));

	end	if;

	if	(qt_reg_w > 0) then
		ie_retorno_w := 'N';
	end	if;

end	if;

return	ie_retorno_w;
	
end	obter_se_unidade_Periodo;
/