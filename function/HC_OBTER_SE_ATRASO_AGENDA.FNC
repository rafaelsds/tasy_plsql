create or replace
function hc_obter_se_atraso_agenda(qt_min_p		number,
			nr_seq_agenda_p	number)
 		    	return varchar2 is
ds_retorno_w	varchar2(1) := 'N';
dt_agenda_w	date;
qt_min_real_w	number(10);
begin
select	dt_agenda
into	dt_agenda_w
from	agenda_hc_paciente
where	nr_sequencia = nr_seq_agenda_p;

if	(dt_agenda_w = sysdate) then
	ds_retorno_w := 'N';
elsif	(dt_agenda_w > sysdate) then
	select	round(((dt_agenda) -(sysdate))*24*60)
	into	qt_min_real_w
	from	agenda_hc_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	
	if	(qt_min_real_w > qt_min_p) then
		ds_retorno_w := 'S';
	else
		ds_retorno_w := 'N';
	end if;	
elsif	(dt_agenda_w < sysdate) then
	select	round(((sysdate)-(dt_agenda))*24*60)
	into	qt_min_real_w
	from	agenda_hc_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	
	if	(qt_min_real_w > qt_min_p) then
		ds_retorno_w := 'S';
	else
		ds_retorno_w := 'N';
	end if;	
end if;
		

return	ds_retorno_w;

end hc_obter_se_atraso_agenda;
/