create or replace
function pls_obter_meses_renov_inter
		(	nr_seq_segurado_p	number)
 		    	return number is

qt_dias_renovacao_w	number(10);
nr_seq_intercambio_w	number(10);

begin

select	max(nr_seq_intercambio)
into	nr_seq_intercambio_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

select	nvl(max(qt_meses_renovacao),0)
into	qt_dias_renovacao_w
from    pls_carteira_renovacao
where   nr_seq_intercambio 	= nr_seq_intercambio_w
and	((ie_tipo_renovacao	= 'R')
or	(ie_tipo_renovacao is null));

return	qt_dias_renovacao_w;

end pls_obter_meses_renov_inter;
/
