create or replace
FUNCTION obter_se_perfil_liber_part  (  cd_perfil_p	     NUMBER,
					cd_pessoa_p          VARCHAR2)
				    RETURN number IS

qt_regra_perfil_w  number(10,0);

BEGIN

IF (cd_perfil_p IS NOT NULL) AND (cd_pessoa_p IS NOT NULL) THEN
	
	SELECT COUNT(*)
	INTO   qt_regra_perfil_w
	FROM   regra_consulta_contato a
	WHERE  cd_pessoa_fisica = cd_pessoa_p
	AND    EXISTS ( SELECT  1
	   		FROM   regra_consulta_contato_lib x
			WHERE  x.nr_seq_regra_consulta = a.nr_sequencia
			AND    x.cd_perfil = cd_perfil_p);

END IF;


RETURN	qt_regra_perfil_w;

END obter_se_perfil_liber_part;
/