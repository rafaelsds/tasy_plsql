create or replace
function obter_se_regra_estagio_autor(nr_seq_agenda_p		number,
									  cd_agenda_p		number default null)
 		    	return varchar2 is
			
ds_retorno_w		Varchar2(1);
qt_regra_w		number(10);
nr_atendimento_w	number(10);
ie_interno_agenda_w	Varchar2(5);
cd_tipo_agenda_w	number(10);
ie_agenda_serv_w	varchar2(1);
ie_agenda_cons_w	varchar2(1);
ie_agenda_exame_w	agenda_regra_estagio_autor.ie_agenda_exame%type;

begin

if (nr_seq_agenda_p is not null) then

	select obter_tipo_agenda(cd_agenda_p)
	into  cd_tipo_agenda_w
	from  dual;
	
	if (cd_tipo_agenda_w in (3,4,5)) then
	        select 	max(nr_atendimento)
		into	nr_atendimento_w
		from	agenda_consulta
		where	nr_sequencia = nr_seq_agenda_p;

		select	max(b.ie_interno)
  		into	ie_interno_agenda_w
  		from	estagio_autorizacao b,
			autorizacao_convenio a
  		where	a.nr_seq_estagio = b.nr_sequencia
  		and 	a.nr_seq_agenda_consulta = nr_seq_agenda_p;
  	else
  		select 	max(nr_atendimento)
	  	into	nr_atendimento_w
	  	from	agenda_paciente
	  	where	nr_sequencia = nr_seq_agenda_p;

		select	max(b.ie_interno)
  		into	ie_interno_agenda_w
	  	from	estagio_autorizacao b,
			autorizacao_convenio a
	  	where	a.nr_seq_estagio = b.nr_sequencia
	  	and 	a.nr_seq_agenda = nr_seq_agenda_p;
	end if;
	
	select	nvl(max(ie_agenda_servico), 'S'), 
			nvl(max(ie_agenda_consulta), 'N'),
			nvl(max(ie_agenda_exame), 'N')
	into	ie_agenda_serv_w,
			ie_agenda_cons_w,
			ie_agenda_exame_w
	from	agenda_regra_estagio_autor
	where	ie_interno = ie_interno_agenda_w
	and		ie_situacao = 'A';
	
	select	count(*)
	into	qt_regra_w
	from	agenda_regra_estagio_autor
	where	ie_interno = ie_interno_agenda_w
	and		ie_situacao = 'A';

end if;

if (qt_regra_w > 0) and
   (nr_atendimento_w is null) then
	if((cd_tipo_agenda_w = 3) or (cd_tipo_agenda_w = 4)) and (nvl(ie_agenda_cons_w, 'N') = 'S') then
		ds_retorno_w := 'S';
	elsif(cd_tipo_agenda_w = 5) and (nvl(ie_agenda_serv_w, 'S') = 'S') then
		ds_retorno_w := 'S';
	elsif(cd_tipo_agenda_w = 2) and (nvl(ie_agenda_exame_w, 'N') = 'S') then
		ds_retorno_w := 'S';
	else
		ds_retorno_w := 'N';
	end if;
else
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end obter_se_regra_estagio_autor;
/
