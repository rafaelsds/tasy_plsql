create or replace
function obter_baixa_conrece(nr_sequencia_p	number,
			  ie_considera_adic_p	varchar2 default 'S')
				    return number is

vl_vinculado_w		number(15,2);
vl_estorno_w		number(15,2);
vl_adicional_w		number(15,2);
cd_estabelecimento_w	number(4);
ie_vl_conv_rec_adic_w	varchar2(1);
nr_lote_contabil_rec_w	number(10);
vl_vinc_lote_audit_w	number(15,2);
ie_deduzir_adic_w	varchar2(1);
ie_soma_vl_adic_w	varchar2(1) := 'N';
cd_convenio_w		number(10);
dt_recebimento_w	date;
ie_passivo_saldo_tit_w	parametro_contas_receber.ie_passivo_saldo_tit%type;
vl_recebido_bordero_w	convenio_receb_titulo.vl_recebido%type;

begin

select	nvl(sum(vl_vinculacao),0)
into 	vl_vinculado_w
from 	convenio_ret_receb
where 	nr_seq_receb = nr_sequencia_p;

select	nvl(sum(vl_vinculado),0)
into 	vl_vinc_lote_audit_w
from 	lote_audit_hist_receb
where 	nr_seq_receb = nr_sequencia_p;

select	max(cd_estabelecimento),
	max(ie_deduzir_adic),
	max(cd_convenio),
	max(dt_recebimento)
into	cd_estabelecimento_w,
	ie_deduzir_adic_w,
	cd_convenio_w,
	dt_recebimento_w
from	convenio_receb
where 	nr_sequencia = nr_sequencia_p;

select 	nvl(max(ie_vl_conv_rec_adic),'N'),
	nvl(max(ie_passivo_saldo_tit),'S')
into	ie_vl_conv_rec_adic_w,
	ie_passivo_saldo_tit_w
from 	parametro_contas_receber
where 	cd_estabelecimento = cd_estabelecimento_w;

ie_vl_conv_rec_adic_w	:= nvl(OBTER_SE_ATUALIZA_JUROS_RECEB(cd_convenio_w,dt_recebimento_w), ie_vl_conv_rec_adic_w);

select 	nvl(sum(vl_estorno + nvl(vl_adic_estorno,0)),0)
into 	vl_estorno_w
from 	convenio_receb_estorno
where 	nr_seq_receb = nr_sequencia_p;

vl_adicional_w := 0;

if	(ie_considera_adic_p = 'S')  then
	select	nvl(sum(nvl(a.vl_adicional,0) + nvl(a.vl_cambial_ativo,0) - decode(ie_passivo_saldo_tit_w,'N',0,nvl(a.vl_cambial_passivo,0))),0)
	into	vl_adicional_w
	from	convenio_receb_adic a,
		transacao_financeira b
	where	a.nr_seq_receb = nr_sequencia_p
	and	a.nr_seq_trans_financ = b.nr_sequencia
	and	(ie_vl_conv_rec_adic_w = 'S' or
		(ie_vl_conv_rec_adic_w = 'O' and nvl(ie_deduzir_adic_w,'S') = 'S'));
end if;
	
obter_param_usuario(27,212,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,cd_estabelecimento_w,ie_soma_vl_adic_w);	

if	(nvl(ie_soma_vl_adic_w,'N') = 'S') and
	(nvl(vl_adicional_w,0) > 0) then
		
	vl_adicional_w := vl_adicional_w * -1;	
end if;

/*OS 2041319 - Considerar tambem os recebimentos de convenio vinculados ao titulo no bordero*/
select	nvl(sum(a.vl_recebido),0)
into	vl_recebido_bordero_w
from	convenio_receb_titulo a
where	a.nr_seq_receb = nr_sequencia_p
and	a.nr_bordero is not null; --Somente deduzir itens desta tabela que possuem nr_bordero informado, senao vai deduzir indevidamente os titulos que constam nesta tabela mas que foram incluidos no processo normal quando baixa o retorno convenio

return vl_vinculado_w + vl_vinc_lote_audit_w + vl_estorno_w + vl_adicional_w + vl_recebido_bordero_w;

end obter_baixa_conrece;
/
