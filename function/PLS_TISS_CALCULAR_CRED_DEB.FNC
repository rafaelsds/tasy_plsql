create or replace
function pls_tiss_calcular_cred_deb	(nr_seq_protocolo_p		number,
					 nm_usuario_p			varchar2,
					 ie_opcao_p			varchar2)
 		    	return number is
			
vl_retorno_w		number(15,2);
vl_credito_w		number(15,2);
vl_debito_w		number(15,2);
vl_liberacao_w		number(15,2);
vl_credito_ww		number(15,2);
vl_debito_ww		number(15,2);
vl_liberacao_ww		number(15,2);

/* ie_opcao_p
	D - Demonstrativo
	P - Data de Pagamento
*/

begin
if	(ie_opcao_p 	= 'D') then
	select	nvl(sum(vl_liberado),0)
	into	vl_liberacao_w
	from	w_pls_resumo_protocolo
	where	nm_usuario	= nm_usuario_p;
	
	select 	nvl(sum(vl_debito_cred),0)
	into	vl_debito_w
	from	w_pls_mov_pagamento 
	where	ie_indicacao = 'D' 
	and 	nr_seq_protocolo_w = nr_seq_protocolo_p;
	
	select 	nvl(sum(vl_debito_cred),0)
	into	vl_credito_w
	from	w_pls_mov_pagamento 
	where	ie_indicacao = 'P' 
	and 	nr_seq_protocolo_w = nr_seq_protocolo_p;
	
	vl_retorno_w	:= vl_liberacao_w - vl_debito_w + vl_credito_w;

elsif 	(ie_opcao_p	= 'P') then
	select	nvl(sum(vl_liberado),0)
	into	vl_liberacao_ww
	from	w_pls_resumo_protocolo
	where	nr_seq_protocolo_w	= nr_seq_protocolo_p;
	
	select 	nvl(sum(vl_debito_cred),0)
	into	vl_debito_ww
	from	w_pls_mov_pagamento 
	where	ie_indicacao = 'D' 
	and 	nr_seq_protocolo_w = nr_seq_protocolo_p;
	
	select 	nvl(sum(vl_debito_cred),0)
	into	vl_credito_ww
	from	w_pls_mov_pagamento 
	where	ie_indicacao = 'P' 
	and 	nr_seq_protocolo_w = nr_seq_protocolo_p;
	
	vl_retorno_w	:= vl_liberacao_ww - vl_debito_ww + vl_credito_ww;
end if;

return	vl_retorno_w;

end pls_tiss_calcular_cred_deb;
/