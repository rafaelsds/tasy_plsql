create or replace
function obter_se_pront_estab
			(	nr_documento_p			number,
				cd_estabelecimento_p		number)
 		    		return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter se o prontu�rio informado pertence ao estabelecimento informado
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_regra_pront_w		varchar2(15);
ds_retorno_w			varchar2(255);

begin
select	nvl(max(vl_parametro),'BASE')
into	ie_regra_pront_w
from	funcao_parametro
where	cd_funcao	= 0
and	nr_sequencia	= 120;

if	(ie_regra_pront_w = 'BASE') then
	select	decode(count(1),0,'N','S')
	into	ds_retorno_w
	from	pessoa_fisica x
	where	x.nr_prontuario		= nr_documento_p
	and	x.cd_estabelecimento	= cd_estabelecimento_p;
elsif	(ie_regra_pront_w = 'ESTAB') and
	(nvl(cd_estabelecimento_p,0) <> 0) then
	select	decode(count(1),0,'N','S')
	into	ds_retorno_w
	from	pessoa_fisica_pront_estab x
	where	x.nr_prontuario		= nr_documento_p
	and	x.cd_estabelecimento	= cd_estabelecimento_p;
end if;

return ds_retorno_w;

end obter_se_pront_estab;
/