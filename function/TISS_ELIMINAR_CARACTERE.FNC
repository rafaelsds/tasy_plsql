create or replace
function TISS_ELIMINAR_CARACTERE
		(ds_conteudo_p	varchar2) return varchar2 deterministic is

begin

-- Edgar 07/01/2009, OS 122608, n�o pode retirar o pipe "|"

return	ltrim(rtrim(replace(
	replace(
	replace(
	replace(
	replace(
	replace(
	replace(
	ds_conteudo_p,	'&', 'e'),
			'<',' '),
			'�', ' '),
			'�', ' '),
			'�', ' '),
			'"',' '),
			'>', ' ')));

end TISS_ELIMINAR_CARACTERE;
/
