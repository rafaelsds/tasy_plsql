create or replace
function obter_desc_tipo_curativo(	nr_sequencia_p	number )
					return varchar2 is

ds_tipo_curativo_w	varchar2(100);
					
begin

if	(nr_sequencia_p > 0) then
	select	ds_tipo_curativo
	into	ds_tipo_curativo_w
	from	tipo_curativo
	where	nr_sequencia	=	nr_sequencia_p;	
end if;

return	ds_tipo_curativo_w;

end obter_desc_tipo_curativo;
/