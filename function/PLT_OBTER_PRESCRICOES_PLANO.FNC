create or replace
function PLT_obter_prescricoes_plano(	ie_opcao_p		varchar2,
					nm_usuario_p		varchar2)
 		    	return varchar2 is
/*
ie_opcao_p
T - todas
N - n�o liberadas
L - liberadas
*/

nr_prescricoes_w		varchar2(4000);			
ds_prescricoes_w		varchar2(1000);
nr_prescricao_w			number(20);
tam_lista_w			number(10);
ie_pos_virgula_w		number(3,0);
nm_usuario_original_w		varchar2(15);
ie_emergencia_w			varchar2(1);
ie_cartao_emergencia_w		varchar2(1);
cd_pessoa_usuario_w		varchar2(10);
cd_medico_w			varchar2(10);
ie_prescritor_aux_w	varchar2(1);
			
cursor c01 is
select	nr_prescricoes
from	w_rep_t
where	nm_usuario = nm_usuario_p
and		nvl(dt_atualizacao,sysdate) between (sysdate - 1) and (sysdate + 1)
and	(((ie_opcao_p	= 'L') and
	  (ie_liberado	= 'S')) or
	 ((ie_opcao_p	= 'N') and
	  (ie_liberado	= 'N')) or
	 (ie_opcao_p	= 'T'));
			
begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_usuario_w
from	usuario
where	nm_usuario	= nm_usuario_p;

open C01;
loop
fetch C01 into	
	ds_prescricoes_w;
exit when C01%notfound;
	begin
	while ds_prescricoes_w is not null loop
		begin
		tam_lista_w		:= length(ds_prescricoes_w);
		ie_pos_virgula_w	:= instr(ds_prescricoes_w,',');
		if	(ie_pos_virgula_w <> 0) then
			nr_prescricao_w		:= to_number(substr(ds_prescricoes_w,1,(ie_pos_virgula_w - 1)));
			ds_prescricoes_w	:= substr(ds_prescricoes_w,(ie_pos_virgula_w + 1),tam_lista_w);
		else
			nr_prescricao_w		:= to_number(ds_prescricoes_w);
			ds_prescricoes_w	:= '';
		end if;
		
		select	max(nm_usuario_original),
			max(cd_medico),
			max(ie_emergencia),
			max(ie_cartao_emergencia),
			max(ie_prescritor_aux)
		into	nm_usuario_original_w,
			cd_medico_w,
			ie_emergencia_w,
			ie_cartao_emergencia_w,
			ie_prescritor_aux_w
		from	prescr_medica
		where	nr_prescricao	= nr_prescricao_w;
		
		if	((nm_usuario_original_w	= nm_usuario_p) or
			 ((cd_medico_w		= cd_pessoa_usuario_w) and
			  ((ie_emergencia_w	= 'S') or
			   (ie_cartao_emergencia_w = 'S') or
			   (ie_prescritor_aux_w = 'S')))) or
			(ie_opcao_p		<> 'N') then
			nr_prescricoes_w 	:= adep_juntar_prescricao(nr_prescricoes_w, nr_prescricao_w);				
		end if;

		end;
	end loop;
	end;
end loop;
close C01;

return	nr_prescricoes_w;

end PLT_obter_prescricoes_plano;
/
