create or replace
function proj_capitalizacao_dados_per(		nr_seq_projeto_p		number,
					nm_usuario_recurso_p	varchar2,
					dt_inicial_p		date,
					dt_final_p			date,
					ie_opcao_p		varchar2)
 		    	return number is

/* ie_opcao_p

HEM	- horas executadas m�s
HA	- horas acumuladas
PEP	- possui execu��o de alguma os no per�odo

HEMR	- horas executadas m�s recurso
HAR	- horas acumuladas recurso
*/

qt_ordem_serv_w	number(10);
qt_minutos_w	number(15,4);

qt_retorno_w	number(15,4);

begin

select	sum(qt_ordem_serv) qt_ordem_serv,
	sum(qt_minutos) qt_minutos
into	qt_ordem_serv_w,
	qt_minutos_w
from (	select	sum(v.qt_minuto) qt_minutos,
		count(m.nr_sequencia) qt_ordem_serv
	from 	proj_projeto a,
		proj_cronograma p,
		proj_cron_etapa e,
		man_ordem_servico m,
		man_ordem_serv_ativ v
	where	a.nr_sequencia = p.nr_seq_proj
	and	p.nr_sequencia = e.nr_seq_cronograma
	and	e.nr_sequencia = m.nr_seq_proj_cron_etapa
	and	m.nr_sequencia = v.nr_seq_ordem_serv	
	and	((p.ie_classificacao <> 'N') or (p.ie_classificacao is null))	
	and	a.nr_sequencia = nr_seq_projeto_p
	and	((ie_opcao_p = 'HA') or 
		 (trunc(v.dt_atividade, 'dd') between dt_inicial_p and dt_final_p and ie_opcao_p = 'HEM') or
		 (ie_opcao_p = 'HAR' and v.nm_usuario = nm_usuario_recurso_p) or
		 (trunc(v.dt_atividade, 'dd') between dt_inicial_p and dt_final_p and
		  v.nm_usuario = nm_usuario_recurso_p and ie_opcao_p = 'HEMR'))
	union
	select	sum(v.qt_minuto) qt_minutos,
		count(m.nr_sequencia) qt_ordem_serv
	from 	proj_projeto a,
		man_ordem_servico m,
		man_ordem_serv_ativ v
	where	a.nr_seq_ordem_serv = m.nr_sequencia
	and	m.nr_sequencia = v.nr_seq_ordem_serv	
	and	a.nr_sequencia = nr_seq_projeto_p
	and	((ie_opcao_p = 'HA') or 
		 (trunc(v.dt_atividade, 'dd') between dt_inicial_p and dt_final_p and ie_opcao_p = 'HEM') or
		 (ie_opcao_p = 'HAR' and v.nm_usuario = nm_usuario_recurso_p) or
		 (trunc(v.dt_atividade, 'dd') between dt_inicial_p and dt_final_p and
		  v.nm_usuario = nm_usuario_recurso_p and ie_opcao_p = 'HEMR')));

if	(ie_opcao_p in ('HA','HEM','HAR','HEMR')) then
	qt_retorno_w := nvl(qt_minutos_w,0);
else
	qt_retorno_w := nvl(qt_ordem_serv_w,0);
end if;

return	qt_retorno_w;

end proj_capitalizacao_dados_per;
/
