create or replace
function pls_obter_se_benef_rescindido
			(	nr_seq_conta_p		number,
				nr_seq_segurado_p	number,
				dt_referencia_p		date	)
				return varchar2 is

dt_rescisao_w			date;
dt_limite_utilizacao_w		date;
dt_emissao_w			date;
ie_retorno_w			varchar2(1) := 'N';
	
begin

if	(nr_seq_conta_p is not null) then

	select	max(dt_rescisao),
		max(fim_dia(dt_limite_utilizacao))
	into	dt_rescisao_w,
		dt_limite_utilizacao_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;

	if	(dt_rescisao_w is not null) and
		(trunc(dt_referencia_p) > dt_limite_utilizacao_w) then
		ie_retorno_w := 'S';
	end if;
	
end if;

return	ie_retorno_w;

end pls_obter_se_benef_rescindido;
/