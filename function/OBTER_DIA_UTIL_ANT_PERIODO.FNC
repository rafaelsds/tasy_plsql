create or replace
function	obter_dia_util_ant_periodo(
			dt_inicial_p		date,
			qt_dias_anterior_p	number,
			cd_estabelecimento_p	number)
		return	date is

dt_retorno_w		date;
dt_referencia_w		date;
qt_dias_util_w		number(5);
ie_dia_semana_w		number(2);
qt_feriado_w		number(5);

begin

dt_referencia_w		:= trunc(nvl(dt_inicial_p,sysdate));
dt_retorno_w		:= null;
qt_dias_util_w		:= 0;

if	(nvl(qt_dias_anterior_p,0) > 0) then
	begin	

	while	(dt_retorno_w is null) loop

		dt_referencia_w	:= (dt_referencia_w - 1);
		
		ie_dia_semana_w := pkg_date_utils.is_business_day(dt_referencia_w,0);

		select	obter_se_feriado(cd_estabelecimento_p, dt_referencia_w)
		into	qt_feriado_w
		from	dual;

		if	(ie_dia_semana_w = 1) and
			(qt_feriado_w = 0) then
			qt_dias_util_w	:= qt_dias_util_w + 1;
		end if;

		if	(qt_dias_util_w	= qt_dias_anterior_p) then
			dt_retorno_w	:= dt_referencia_w;
		end if;		

	end loop;		

	end;
else
	dt_retorno_w	:= sysdate;
end if;

return	dt_retorno_w;

end obter_dia_util_ant_periodo;
/
