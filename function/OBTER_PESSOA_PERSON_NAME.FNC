create or replace
function OBTER_PESSOA_PERSON_NAME(nr_seq_person_name_p		number)
 		    	return varchar2 is

cd_pessoa_fisica_w  varchar2(10);
				
begin

if (nr_seq_person_name_p is not null) then
	select 	b.cd_pessoa_fisica 
	into 	cd_pessoa_fisica_w
	from 	pessoa_fisica b 
	where 	b.nr_seq_person_name = nr_seq_person_name_p;
end if;

return	cd_pessoa_fisica_w;

end OBTER_PESSOA_PERSON_NAME;
/