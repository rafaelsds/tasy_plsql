create or replace
function obter_situacao_estoque_mat(	cd_estabelecimento_p		number,
				cd_material_p			number)
 		    	return varchar2 is

ds_retorno_w			varchar2(10);			
qt_estoque_w			number(13,4);
qt_estoque_minimo_w		number(13,4);
qt_estoque_maximo_w		number(13,4);
qt_estoque_maximo_w2		number(13,4) := 0;
qt_ponto_pedido_w			number(13,4);
cd_material_estoque_w		number(6);

begin

select	a.cd_material_estoque,
	b.qt_estoque_minimo,
	b.qt_estoque_maximo,
	b.qt_ponto_pedido
into	cd_material_estoque_w,	
	qt_estoque_minimo_w,
	qt_estoque_maximo_w,
	qt_ponto_pedido_w
from	material a,
	material_estab b
where	a.cd_material = b.cd_material
and	b.cd_Estabelecimento = cd_estabelecimento_p
and	a.cd_material = cd_material_p;

qt_estoque_maximo_w2 := qt_estoque_maximo_w * qt_estoque_maximo_w;
	
select	sum(qt_estoque)
into	qt_estoque_w
from	saldo_estoque
where	cd_estabelecimento = cd_estabelecimento_p
and	cd_material = cd_material_estoque_w
and	dt_mesano_referencia = trunc(sysdate,'mm');

if	(qt_estoque_w <= 0) then
	ds_retorno_w := 'ZE';
elsif	(qt_estoque_w < qt_estoque_minimo_w) then
	ds_retorno_w := 'MB';
elsif	(qt_estoque_w < qt_ponto_pedido_w) then
	ds_retorno_w := 'BA';
elsif	(qt_estoque_w >= qt_ponto_pedido_w) and (qt_estoque_w <= qt_estoque_maximo_w) then
	ds_retorno_w := 'ID';
elsif	(qt_estoque_w >= qt_estoque_maximo_w2) then
	ds_retorno_w := 'MA';
elsif	(qt_estoque_w >= qt_estoque_maximo_w) then
	ds_retorno_w := 'AL';
end if;


return	ds_retorno_w;

end obter_situacao_estoque_mat;
/