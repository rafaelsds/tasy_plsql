create or replace
function Obter_semana_Idade_IGC	(	dt_atual_p	date,
								dt_nascimento_p	date,
								dt_nascimento_ig_p	date,
								qt_dias_ig_p	number,
								qt_semanas_ig_p	number)
 		    	return number is

				
qt_retorno_w	number(15);
qt_diferenca_w	number(15);
qt_dias_total_w	number(10);
qt_semanas_w	number(10);
qt_dias_w		number(10);
begin

if	(dt_nascimento_ig_p	is not null) and
	(qt_dias_ig_p is not null) and
	(qt_semanas_ig_p is not null) then
	
	qt_diferenca_w	:= trunc(dt_atual_p - dt_nascimento_p);
	qt_dias_total_w:= nvl(qt_dias_ig_p,0) + nvl(qt_semanas_ig_p,0) * 7; 
	qt_dias_total_w	:= qt_dias_total_w + qt_diferenca_w;
	qt_semanas_w	:= trunc(dividir(qt_dias_total_w,7));
	qt_dias_w		:= mod(qt_dias_total_w,7);
	qt_retorno_w	:= nvl(qt_semanas_w,0);
	
end if;

return	qt_retorno_w;

end Obter_semana_Idade_IGC;
/
