create or replace
function obter_grupo_usuario(nm_usuario_p		varchar2)
 		    	return number is

nr_seq_grupo_w		number(10);

begin
	
	select	max(nr_seq_grupo)
	into	nr_seq_grupo_w
	from	usuario_grupo_des
	where	nm_usuario_grupo = nm_usuario_p;
	
	if	(nr_seq_grupo_w is null) then

		select	max(nr_seq_grupo)
		into	nr_seq_grupo_w
		from	usuario_grupo_sup
		where	nm_usuario_grupo = nm_usuario_p;

	end if;
	
	return	nr_seq_grupo_w;

end obter_grupo_usuario;
/