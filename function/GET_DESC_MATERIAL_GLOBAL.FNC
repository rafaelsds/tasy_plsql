create or replace
FUNCTION get_desc_material_global(
		cd_material_p	NUMBER)
	RETURN VARCHAR IS

ds_retorno_w		VARCHAR2(255) := '';

BEGIN

IF	( cd_material_p IS NOT NULL ) THEN
	SELECT 	SUBSTR(ds_material_glo,1,255)
	INTO	ds_retorno_w
	FROM 	material_global
	WHERE 	cd_material_glo =  cd_material_p;
END IF;

RETURN ds_retorno_w;

END get_desc_material_global;
/
