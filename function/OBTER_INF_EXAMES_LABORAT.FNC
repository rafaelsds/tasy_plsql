create or replace
function 	obter_inf_exames_laborat(	nr_seq_exame_p	Number,
						nr_prescricao_p	Number,
						nm_usuario_p	Varchar2) 
				return varchar2 is

				
ds_exame_w	varchar2(4000) := '';
dt_coleta_w	Date;
dt_aprovacao_w	Date;
nr_resultado_w	Number(10);

begin
if	(nr_seq_exame_p is not null) and
	(nr_prescricao_p is not null) then

	
	obter_param_usuario(281,17,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,nr_resultado_w );
	
	if	( nvl(nr_resultado_w,1)  = 1 ) then
		
		
		select	max(nvl(k.dt_coleta,B.dt_coleta)),
			max(Obter_data_aprov_lab(k.nr_prescricao,k.nr_sequencia))
		into	dt_coleta_w,
			dt_aprovacao_w	
		from	grupo_exame_lab c,
			exame_laboratorio e,
			exame_lab_result_item b,
			prescr_procedimento k,
			prescr_medica x,
			exame_lab_resultado a
		where	a.nr_seq_resultado	= b.nr_seq_resultado
		and	b.nr_seq_exame		= e.nr_seq_exame
		and	e.nr_seq_grupo		= c.nr_sequencia
		and	x.nr_prescricao		= a.nr_prescricao
		and	k.nr_sequencia		= b.nr_seq_prescr
		and	x.nr_prescricao		= k.nr_prescricao
		and	x.nr_prescricao		= nr_prescricao_p
		and	b.nr_seq_exame		= nr_seq_exame_p;		
		
		
		ds_exame_w := wheb_mensagem_pck.get_texto(308348,	'DT_COLETA_W=' || to_char(dt_coleta_w, 'dd/mm/yyyy hh24:mi:ss') || ';' ||
															'DT_APROVACAO_W=' || to_char(dt_aprovacao_w, 'dd/mm/yyyy hh24:mi:ss'));
					/*
						Data coleta do exame: #@DT_COLETA_W#@
						Data de aprova��o do exame: #@DT_APROVACAO_W#@
					*/
					
	elsif	( nvl(nr_resultado_w,4)  = 4 ) then
		select	max(nvl(k.dt_coleta,B.dt_coleta)),
				max(Obter_data_aprov_lab(k.nr_prescricao,k.nr_sequencia))
		into	dt_coleta_w,
				dt_aprovacao_w	
		from	grupo_exame_lab c,
			exame_laboratorio e,
			exame_lab_result_item b,
			prescr_procedimento k,
			prescr_medica x,
			exame_lab_resultado a
		where	a.nr_seq_resultado	= b.nr_seq_resultado
		and	b.nr_seq_exame		= e.nr_seq_exame
		and	e.nr_seq_grupo		= c.nr_sequencia
		and	x.nr_prescricao		= a.nr_prescricao
		and	k.nr_sequencia		= b.nr_seq_prescr
		and	x.nr_prescricao		= k.nr_prescricao
		and	x.nr_prescricao		= nr_prescricao_p
		and	((b.nr_seq_exame = nr_seq_exame_p) or (e.nr_seq_exame_pep = nr_seq_exame_p));		
		
		ds_exame_w := wheb_mensagem_pck.get_texto(308348,	'DT_COLETA_W=' || to_char(dt_coleta_w, 'dd/mm/yyyy hh24:mi:ss') || ';' ||
															'DT_APROVACAO_W=' || to_char(dt_aprovacao_w, 'dd/mm/yyyy hh24:mi:ss'));					
					/*
						Data coleta do exame: #@DT_COLETA_W#@
						Data de aprova��o do exame: #@DT_APROVACAO_W#@
					*/						
	else
	
		select 	max(b.dt_coleta),
			max(b.dt_aprovacao)
		into	dt_coleta_w,
			dt_aprovacao_w
		from   	exame_lab_result_item b, 
			exame_lab_resultado a
		where 	a.nr_seq_resultado	= b.nr_seq_resultado 
		and	a.nr_prescricao 	= nr_prescricao_p
		and	b.nr_seq_exame		= nr_seq_exame_p ;
	
		ds_exame_w := wheb_mensagem_pck.get_texto(308348,	'DT_COLETA_W=' || to_char(dt_coleta_w, 'dd/mm/yyyy hh24:mi:ss') || ';' ||
															'DT_APROVACAO_W=' || to_char(dt_aprovacao_w, 'dd/mm/yyyy hh24:mi:ss'));
					/*
						Data coleta do exame: #@DT_COLETA_W#@
						Data de aprova��o do exame: #@DT_APROVACAO_W#@
					*/
					
	end if;	

end if;


return  substr(ds_exame_w,1,255);

end obter_inf_exames_laborat;
/
