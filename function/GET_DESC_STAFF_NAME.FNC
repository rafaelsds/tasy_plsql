create or replace function get_desc_staff_name(	nr_sequencia_p		number)
 		    	return varchar2 is
			

ds_staff_name_w	nurse_shift_user.ds_staff_name%type;

begin

select	ds_staff_name
into	ds_staff_name_w
from	nurse_shift_user
where	nr_sequencia = nr_sequencia_p;


return	ds_staff_name_w;

end get_desc_staff_name;
/