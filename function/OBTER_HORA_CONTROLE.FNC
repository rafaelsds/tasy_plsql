create or replace
function Obter_Hora_Controle(nr_Seq_dialise_p		number,
			Qt_dif_tempo_p		number)
 		    	return date is

dt_hora_w			date;			
nr_sequencia_w			number(10);
			
begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	hd_controle
where	nr_seq_dialise = nr_seq_dialise_p;

if (nr_sequencia_w is not null) then
	select	dt_controle + ((1/1440) * qt_dif_tempo_p)
	into	dt_hora_w
	from	hd_controle
	where	nr_sequencia = nr_sequencia_w;
end if;

return	dt_hora_w;

end Obter_Hora_Controle;
/