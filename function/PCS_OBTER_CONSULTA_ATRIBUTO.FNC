create or replace
function pcs_obter_consulta_atributo(	nm_tabela_p		varchar2,
					nm_atributo_p		varchar2,
					ie_possui_integridade_p	varchar2,
					ie_cadastro_geral_p	varchar2 default 'N')
 		    	return varchar2 is

ds_consulta_w			varchar2(2000);
ds_consulta_integridade_w	varchar2(2000);
ds_consulta_dominio_w		varchar2(2000);
nm_tabela_referencia_w		varchar2(50);
nm_atributo_referencia_w	varchar2(80);
cd_dominio_w			number(5,0);
ds_descricao_atributo_w		varchar2(255);
qt_posicao_ds_ini_w		number(5,0);
qt_posicao_ds_fim_w		number(5,0);
qt_posicao_from_w		number(5,0);

begin

if	(ie_cadastro_geral_p = 'N') then
	begin
	select	upper(max(nm_tabela_referencia))
	into	nm_tabela_referencia_w
	from	integridade_referencial a,
		integridade_atributo b
	where	b.nm_tabela = upper(nm_tabela_p )
	and	b.nm_atributo = upper(nm_atributo_p)
	and	a.nm_tabela = b.nm_tabela
	and	a.nm_integridade_referencial = b.nm_integridade_referencial;
	
	select	upper(max(c.column_name))	
	into	nm_atributo_referencia_w	
	from	user_constraints a,
		user_constraints b,	
		user_cons_columns c  --ligando o nome das constrainst
	where	a.table_name = nm_tabela_p
	and	b.table_name = nm_tabela_referencia_w
	and	a.r_constraint_name = b.constraint_name
	and	b.constraint_name = c.constraint_name;
	
	if	(ie_possui_integridade_p = 'R') then
		begin	
		select	max(ds_valores)
		into	ds_consulta_integridade_w
		from	tabela_atributo
		where	nm_tabela = nm_tabela_referencia_w
		and	nm_atributo = nm_atributo_referencia_w
		and	ds_valores like 'select%';
		
		if	(ds_consulta_integridade_w is null) then
			begin		
			select	max(ds_sql_lookup)
			into	ds_consulta_integridade_w
			from	tabela_sistema
			where	nm_tabela = nm_tabela_referencia_w;		
			end;			
		end if;	
		end;
	end if;	
	
	if	(ie_possui_integridade_p = 'D') then
		begin		
		select	max(cd_dominio)
		into	cd_dominio_w
		from	tabela_atributo
		where	nm_tabela = upper(nm_tabela_p)
		and	nm_atributo = upper(nm_atributo_p);
		
		
		ds_consulta_dominio_w :=' select vl_dominio cd, ' || chr(13) || chr (10) ||
					'	 ds_valor_dominio ds ' || chr(13) || chr (10) ||
					' from	 valor_dominio ' || chr(13) || chr (10) ||
					' where	 cd_dominio = ' || to_char(cd_dominio_w);
	
		end;
	end if;		
	end;
elsif	(ie_cadastro_geral_p in ('DS_DESCRICAO','S')) then
	begin	
	select	upper(max(ds_sql_lookup))
	into	ds_consulta_integridade_w
	from	tabela_sistema
	where	nm_tabela = nm_tabela_p;	
	
	qt_posicao_ds_ini_w := INSTR(ds_consulta_integridade_w,'DS'); --Verificado a posi��o inicial do campo descri��o
	qt_posicao_from_w   := INSTR(ds_consulta_integridade_w,'FROM');	--Verificado a posi��o inicial do from
  	qt_posicao_ds_fim_w := instr(substr(ds_consulta_integridade_w,1,qt_posicao_from_w),'DS',qt_posicao_ds_ini_w + 1); --Verifica at� o from,  se existe algum tipo de alias DS
		
	if (qt_posicao_ds_fim_w = 0) then --Caso n�o existir o alias 'DS' 
		ds_descricao_atributo_w := substr(ds_consulta_integridade_w,qt_posicao_ds_ini_w,(qt_posicao_from_w-2) - qt_posicao_ds_ini_w);
	else				  --Caso existir o alias 'DS' 
		ds_descricao_atributo_w := substr(ds_consulta_integridade_w,qt_posicao_ds_ini_w,qt_posicao_ds_fim_w - qt_posicao_ds_ini_w);
	end if;
		
	end;
end if;	
	
if	(nvl(ds_consulta_integridade_w,'X' ) <> 'X') then
	ds_consulta_w := substr(ds_consulta_integridade_w,1,2000);
elsif	(nvl(ds_consulta_dominio_w,'X' ) <> 'X') then
	ds_consulta_w := substr(ds_consulta_dominio_w,1,2000);
end if;	

if	(ie_cadastro_geral_p = 'DS_DESCRICAO') and
	(ds_descricao_atributo_w is not null) then
	ds_consulta_w := substr(UPPER(ds_descricao_atributo_w),1,255);
end if;	
		
return	ds_consulta_w;

end pcs_obter_consulta_atributo;
/
