create or replace 
function pls_obter_tipo_exp_fatura(	nr_sequencia_p   pls_fatura.nr_sequencia%type) return varchar2 is

ie_tipo_exportacao_w	ptu_fatura.ie_tipo_exportacao%type;

begin

select nvl(max(pc.ie_tipo_exportacao),'TXT') 
into 	ie_tipo_exportacao_w
from 	pls_fatura pf,
	ptu_fatura ptf,
	pls_congenere pc
where 	ptf.nr_seq_pls_fatura 		= pf.nr_sequencia
and 	lpad(pc.cd_cooperativa,4,'0') 	= lpad(ptf.cd_unimed_destino,4,'0') 
and 	pf.nr_sequencia 		= nr_sequencia_p;

return ie_tipo_exportacao_w;

end pls_obter_tipo_exp_fatura;
/
