create or replace
function	lab_obter_aprovador_exame(
				nr_prescricao_p number
			)
    		return varchar2 is
			
ds_retorno_w			varchar2(4000);
nm_usuario_aprovacao_w	varchar(100);
ds_string_separacao_w	varchar(3);

cursor c01 is
	select	distinct
			substr(obter_nome_pf(b.cd_medico_resp), 1, 100)
	from    exame_lab_result_item b,
			exame_lab_resultado a
	where	a.nr_seq_resultado	= b.nr_seq_resultado
	and		a.nr_prescricao     = nr_prescricao_p
	and		b.nm_usuario_aprovacao is not null
	order by 1;

begin

open c01;
loop
	fetch c01 into	
		nm_usuario_aprovacao_w;
	exit when c01%notfound;
		begin
		ds_retorno_w			:= ds_retorno_w || ds_string_separacao_w || nm_usuario_aprovacao_w;
		ds_string_separacao_w	:= ' - ';
		end;
	end loop;
close c01;
	
return substr(wheb_mensagem_pck.get_texto(805161,'DS_RETORNO='||ds_retorno_w),1,2000);

end lab_obter_aprovador_exame;
/

