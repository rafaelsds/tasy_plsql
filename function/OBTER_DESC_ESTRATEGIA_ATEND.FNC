create or replace
function Obter_Desc_Estrategia_Atend(	ds_regra_p	varchar2)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(4000);
letra_w		varchar2(10);
parentesesAberto	boolean;
encerramento		boolean;
dsFila_w		varchar2(4000);
ieAcao_w		varchar2(10);
dsCabecalho		varchar2(4000);
lista_w	dbms_sql.varchar2_table;
nr_seq_fila_w	number(10);
ds_fila_w	varchar2(4000);

begin
begin

if	(ds_regra_p	is not null) then
	
	for i in 1..length(ds_regra_p) loop
		begin
		
		
		letra_w	:= substr(ds_regra_p,i,1);
		
		if 	(letra_w = '(') then
			parentesesAberto := true;
                elsif 	(letra_w = ')') then
			parentesesAberto := false;
                end if;
		
		
		if	(parentesesAberto) and
			(letra_w <> ')') and
			(letra_w <> '(') then
			dsFila_w := dsFila_w ||letra_w;
			
		end if;
		
		 if 	(not parentesesAberto)	and 
			(letra_w != '/')	and 
			(letra_w != '!') and 
			(trim(dsFila_w) is not null) then
			dsFila_w := dsFila_w|| ',';
                end if;
		
		
		if (	not parentesesAberto	and 
			letra_w != ')'	and 
			letra_w != '('	and 
			letra_w != '/'	and 
			i != (length(ds_regra_p) - 1)) then
                    ieAcao_w := letra_w;
                end if;
		
                if (letra_w = '/'	or 
			i = (length(ds_regra_p) -1 )) then
                    encerramento := true;
                end if;
		
		
		if	(encerramento) then
			if 	(ds_retorno_w	is not null) then
				ds_retorno_w	:= ds_retorno_w||chr(10);
		
				dsCabecalho := wheb_mensagem_pck.get_texto(292885);--"Caso n�o houver senhas, ";

				if (ieAcao_w = '!') then
				    dsCabecalho := dsCabecalho ||wheb_mensagem_pck.get_texto(292892);--"intercalar filas:";
				else 
				    dsCabecalho := dsCabecalho || wheb_mensagem_pck.get_texto(292893);--"chamar filas:";
				end if;
			else
				if (ieAcao_w = '!') then
				    dsCabecalho := dsCabecalho ||wheb_mensagem_pck.get_texto(292894);--"Intercalar filas:";
				 else 
				    dsCabecalho := dsCabecalho ||wheb_mensagem_pck.get_texto(292895);--"Chamar filas:";
				end if;
				
			end if;
			ds_retorno_w	:= ds_retorno_w || dsCabecalho;
			ds_retorno_w	:= ds_retorno_w||chr(10);
			if	(dsFila_w	is not null) then
				lista_w	:= obter_lista_string(dsFila_w,',');
				
				for x in lista_w.first..lista_w.last loop
					begin
					nr_seq_fila_w	:= lista_w(x);
					ds_fila_w	:= OBTER_DESC_FILA(nr_seq_fila_w);
					ds_retorno_w	:= ds_retorno_w|| '      - ' || ds_fila_w || ' (' || nr_seq_fila_w || ')';
					ds_retorno_w 	:= ds_retorno_w ||chr(10);
					end;
				end loop;
			end if;
			parentesesAberto := false;
			encerramento := false;
			dsFila_w := null;
			ieAcao_w := ' ';
			dsCabecalho := '';
			
			
		end if;
		
		
		end;
	end loop;
	
	
end if;
exception
when others then
	ds_retorno_w	:= null;
end;



return	ds_retorno_w;

end Obter_Desc_Estrategia_Atend;
/