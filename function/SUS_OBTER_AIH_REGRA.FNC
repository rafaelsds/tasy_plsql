create or replace
function sus_obter_aih_regra(	nr_seq_regra_p	number,
			ie_opcao_p	varchar2,
			ie_tipo_p		varchar2)
			return number as

/*Op��o:
P - Protocolo
E - Execu��o(fora de protocolo)
Tipo retorno
V - Valor
Q - Quantidade
*/
qt_retorno_w		number(15);
vl_retorno_w		number(15,2);
ds_retorno_w		number(15,2);
cd_estabelecimento_w	number(4);
ie_complexidade_w		varchar2(15);
ie_tipo_financiamento_w	varchar2(15);
dt_competencia_w		date;
nr_seq_grupo_w		number(10) := 0;
nr_seq_subgrupo_w	number(10) := 0;
nr_seq_forma_org_w	number(10) := 0;
cd_municipio_ibge_w	varchar2(6) := 'X';

begin

begin
select	cd_estabelecimento,
	dt_competencia,
	ie_complexidade,
	ie_tipo_financiamento,
	nr_seq_grupo,
	nr_seq_subgrupo,
	nr_seq_forma_org,
	cd_municipio_ibge
into	cd_estabelecimento_w,
	dt_competencia_w,
	ie_complexidade_w,
	ie_tipo_financiamento_w,
	nr_seq_grupo_w,
	nr_seq_subgrupo_w,
	nr_seq_forma_org_w,
	cd_municipio_ibge_w
from	sus_limite_aih_comp
where	nr_sequencia = nr_seq_regra_p;
exception
when others then
	cd_estabelecimento_w	:= 0;
	dt_competencia_w		:= sysdate;
	ie_complexidade_w		:= null;
	ie_tipo_financiamento_w	:= null;
	nr_seq_grupo_w		:= 0;
	nr_seq_subgrupo_w	:= 0;
	nr_seq_forma_org_w	:= 0;
	cd_municipio_ibge_w	:= 'X';
end;

if	(ie_opcao_p = 'P') then
	begin
	if	(ie_tipo_p = 'Q') then
		begin

		select	count(*)
		into	qt_retorno_w
		from	sus_aih_unif a,
			conta_paciente b,
			protocolo_convenio c,
			sus_grupo d,
			sus_subgrupo e,
			sus_forma_organizacao f,
			sus_procedimento g
		where	a.nr_interno_conta 			= b.nr_interno_conta
		and	a.cd_procedimento_real			= g.cd_procedimento
		and	a.ie_origem_proc_real			= g.ie_origem_proced
		and	g.nr_seq_forma_org			= f.nr_sequencia
		and	f.nr_seq_subgrupo			= e.nr_sequencia
		and	e.nr_seq_grupo				= d.nr_sequencia
		and	trunc(c.dt_mesano_referencia,'month')	= trunc(dt_competencia_w,'month')
		and	sus_obter_complexidade_aih(b.nr_interno_conta)	= nvl(ie_complexidade_w,sus_obter_complexidade_aih(b.nr_interno_conta))
		and	a.ie_tipo_financiamento 		= nvl(ie_tipo_financiamento_w,a.ie_tipo_financiamento)
		and	(d.nr_sequencia = nvl(nr_seq_grupo_w,d.nr_sequencia))
		and	(e.nr_sequencia = nvl(nr_seq_subgrupo_w,e.nr_sequencia))
		and	(f.nr_sequencia = nvl(nr_seq_forma_org_w,f.nr_sequencia))
		and	((nvl(cd_municipio_ibge_w,'X') = 'X') or (sus_obter_municipio_atend(a.nr_atendimento,'C') = cd_municipio_ibge_w))
		and	b.cd_estabelecimento			= cd_estabelecimento_w
		and	b.nr_seq_protocolo			= c.nr_seq_protocolo;

		ds_retorno_w := qt_retorno_w;

		end;
	elsif	(ie_tipo_p = 'V') then
		begin

		select	sum(nvl(sus_obter_valor_unif(b.nr_interno_conta,0),0))
		into	vl_retorno_w
		from	sus_aih_unif a,
			conta_paciente b,
			protocolo_convenio c,
			sus_grupo d,
			sus_subgrupo e,
			sus_forma_organizacao f,
			sus_procedimento g
		where	a.nr_interno_conta 			= b.nr_interno_conta
		and	a.cd_procedimento_real			= g.cd_procedimento
		and	a.ie_origem_proc_real			= g.ie_origem_proced
		and	g.nr_seq_forma_org			= f.nr_sequencia
		and	f.nr_seq_subgrupo			= e.nr_sequencia
		and	e.nr_seq_grupo				= d.nr_sequencia
		and	trunc(c.dt_mesano_referencia,'month')	= trunc(dt_competencia_w,'month')
		and	sus_obter_complexidade_aih(b.nr_interno_conta)	= nvl(ie_complexidade_w,sus_obter_complexidade_aih(b.nr_interno_conta))
		and	a.ie_tipo_financiamento 		= nvl(ie_tipo_financiamento_w,a.ie_tipo_financiamento)
		and	(d.nr_sequencia = nvl(nr_seq_grupo_w,d.nr_sequencia))
		and	(e.nr_sequencia = nvl(nr_seq_subgrupo_w,e.nr_sequencia))
		and	(f.nr_sequencia = nvl(nr_seq_forma_org_w,f.nr_sequencia))
		and	((nvl(cd_municipio_ibge_w,'X') = 'X') or (sus_obter_municipio_atend(a.nr_atendimento,'C') = cd_municipio_ibge_w))
		and	b.cd_estabelecimento			= cd_estabelecimento_w
		and	b.nr_seq_protocolo			= c.nr_seq_protocolo;

		ds_retorno_w := vl_retorno_w;

		end;
	end if;
	end;
elsif	(ie_opcao_p = 'E') then
	begin

	if	(ie_tipo_p = 'Q') then
		begin

		select	count(*)
		into	qt_retorno_w
		from	sus_aih_unif a,
			conta_paciente b,
			sus_grupo d,
			sus_subgrupo e,
			sus_forma_organizacao f,
			sus_procedimento g
		where	a.nr_interno_conta 			= b.nr_interno_conta
		and	a.cd_procedimento_real			= g.cd_procedimento
		and	a.ie_origem_proc_real			= g.ie_origem_proced
		and	g.nr_seq_forma_org			= f.nr_sequencia
		and	f.nr_seq_subgrupo			= e.nr_sequencia
		and	e.nr_seq_grupo				= d.nr_sequencia
		and	trunc(b.dt_mesano_referencia,'month')	= trunc(dt_competencia_w,'month')
		and	sus_obter_complexidade_aih(b.nr_interno_conta)	= nvl(ie_complexidade_w,sus_obter_complexidade_aih(b.nr_interno_conta))
		and	a.ie_tipo_financiamento 		= nvl(ie_tipo_financiamento_w,a.ie_tipo_financiamento)
		and	(d.nr_sequencia = nvl(nr_seq_grupo_w,d.nr_sequencia))
		and	(e.nr_sequencia = nvl(nr_seq_subgrupo_w,e.nr_sequencia))
		and	(f.nr_sequencia = nvl(nr_seq_forma_org_w,f.nr_sequencia))
		and	((nvl(cd_municipio_ibge_w,'X') = 'X') or (sus_obter_municipio_atend(a.nr_atendimento,'C') = cd_municipio_ibge_w))
		and	b.cd_estabelecimento			= cd_estabelecimento_w
		and	nvl(b.nr_seq_protocolo,0) 		= 0;

		ds_retorno_w := qt_retorno_w;

		end;
	elsif	(ie_tipo_p = 'V') then
		begin

		select	sum(nvl(sus_obter_valor_unif(b.nr_interno_conta,0),0))
		into	vl_retorno_w
		from	sus_aih_unif a,
			conta_paciente b,
			sus_grupo d,
			sus_subgrupo e,
			sus_forma_organizacao f,
			sus_procedimento g
		where	a.nr_interno_conta 			= b.nr_interno_conta
		and	a.cd_procedimento_real			= g.cd_procedimento
		and	a.ie_origem_proc_real			= g.ie_origem_proced
		and	g.nr_seq_forma_org			= f.nr_sequencia
		and	f.nr_seq_subgrupo			= e.nr_sequencia
		and	e.nr_seq_grupo				= d.nr_sequencia
		and	trunc(b.dt_mesano_referencia,'month')	= trunc(dt_competencia_w,'month')
		and	sus_obter_complexidade_aih(b.nr_interno_conta)	= nvl(ie_complexidade_w,sus_obter_complexidade_aih(b.nr_interno_conta))
		and	a.ie_tipo_financiamento 		= nvl(ie_tipo_financiamento_w,a.ie_tipo_financiamento)
		and	(d.nr_sequencia = nvl(nr_seq_grupo_w,d.nr_sequencia))
		and	(e.nr_sequencia = nvl(nr_seq_subgrupo_w,e.nr_sequencia))
		and	(f.nr_sequencia = nvl(nr_seq_forma_org_w,f.nr_sequencia))
		and	((nvl(cd_municipio_ibge_w,'X') = 'X') or (sus_obter_municipio_atend(a.nr_atendimento,'C') = cd_municipio_ibge_w))
		and	b.cd_estabelecimento			= cd_estabelecimento_w
		and	nvl(b.nr_seq_protocolo,0) 		= 0;

		ds_retorno_w := vl_retorno_w;

		end;
	end if;

	end;
end if;

return	nvl(ds_retorno_w,0);

end sus_obter_aih_regra;
/
