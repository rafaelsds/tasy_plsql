create or replace
function Obter_desc_Orcamento_Padrao(nr_seq_orc_padrao_p	number)
 		    	return varchar2 is


ds_orcamento_padrao_w		varchar2(255);	

begin

begin
select	ds_orcamento_padrao
into	ds_orcamento_padrao_w
from	orcamento_padrao
where	nr_sequencia = nr_seq_orc_padrao_p;
exception
when others then
	ds_orcamento_padrao_w:='';
end;

return	ds_orcamento_padrao_w;

end Obter_desc_Orcamento_Padrao;
/