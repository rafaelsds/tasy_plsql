CREATE OR REPLACE
FUNCTION obter_dias_pos_operatorio_suep
				(	nr_cirurgia_p	number,
					nr_atendimento_p number)
					return number is

dt_inicio_real_w	date;
dt_alta_w		date;
qt_dias_po_w		number;

BEGIN

begin
select	max(dt_inicio_real)
into	dt_inicio_real_w
from	cirurgia
where	nr_cirurgia		= nr_cirurgia_p
and	ie_status_cirurgia	= 2;
exception
	when others then
		dt_inicio_real_w	:= null;
end;

if	(dt_inicio_real_w is not null) then
	begin
	
	
	begin
    select	dt_alta 
	into	dt_alta_w	
	from	atendimento_paciente
	where	nr_atendimento		= nr_atendimento_p;
    exception
		when others then
			dt_alta_w	:= null;
	end;
		
	qt_dias_po_w	:= trunc(nvl(trunc(dt_alta_w),trunc(sysdate)) - trunc(dt_inicio_real_w));
	end;
end if;

RETURN qt_dias_po_w;

END obter_dias_pos_operatorio_suep;
/