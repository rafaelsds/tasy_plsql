create or replace
function obter_valor_atrib_san_exame_js(	nr_seq_exame_p	number,
					nm_atributo_p	varchar2)
					return varchar is


vl_atributo_retorno_w			varchar2(255) := '';
begin

if	('IE_RESULTADO' = upper(nm_atributo_p)) then
	begin
	
	select	max(ie_resultado)
	into	vl_atributo_retorno_w
	from	san_exame
	where	nr_sequencia	= nr_seq_exame_p;
	
	end;
elsif ('CD_TIPO_RESULTADO' = upper(nm_atributo_p)) then
	begin
	
	select	max(cd_tipo_resultado)
	into	vl_atributo_retorno_w
	from	san_exame
	where	nr_sequencia	= nr_seq_exame_p;
	
	end;
end if;

return	vl_atributo_retorno_w;

end obter_valor_atrib_san_exame_js;
/