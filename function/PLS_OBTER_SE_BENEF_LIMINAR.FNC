create or replace
function pls_obter_se_benef_liminar
			(	nr_seq_requisicao_p	Number)
				return Varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se o benefici�rio possui liminar judicial cadastrada na fun��o OPS - Processo judicial.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [X] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Avisar ao ao pessoal do portal e do Delphi/Java sempre que for alterado ou incluido algum par�metro .
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_area_w			Number(15);
cd_especialidade_w		Number(15);
cd_grupo_w			Number(15);
cd_procedimento_w		Number(15);

ie_origem_proced_w		Number(10);
ie_origem_proc_w		Number(10);
ie_retorno_w			Varchar2(2)	:= 'N';

qt_reg_proc_w			Number(10);
qt_reg_mat_w			Number(10);
qt_reg_prest_w			Number(10);
qt_registros_w			Number(10);

nr_seq_segurado_w		Number(10);
nr_seq_prestador_w		Number(10);
nr_seq_material_w		Number(10);
nr_seq_contrato_w		Number(10);

cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	
Cursor C02 is
	select	nr_seq_material
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

begin

select	nr_seq_segurado,
	nr_seq_prestador
into	nr_seq_segurado_w,
	nr_seq_prestador_w
from	pls_requisicao
where	nr_sequencia	= nr_seq_requisicao_p;

begin
	select	nr_seq_contrato,
		cd_pessoa_fisica
	into	nr_seq_contrato_w,
		cd_pessoa_fisica_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_w;
exception
when others then
	nr_seq_contrato_w := 0;
end;

select	count(1)
into	qt_registros_w
from	processo_judicial_liminar
where	(((nvl(ie_considera_codigo_pf, 'N') = 'N'	and nr_seq_segurado	= nr_seq_segurado_w)
or	(nvl(ie_considera_codigo_pf, 'N') = 'S'		and pls_obter_dados_segurado(nr_seq_segurado, 'PF')	= cd_pessoa_fisica_w))
or	nr_seq_contrato		= nr_seq_contrato_w)
and	ie_estagio		= 2
and	ie_impacto_autorizacao	= 'S'
and	sysdate	between (dt_inicio_validade) and (nvl(dt_fim_validade,sysdate));

if	(qt_registros_w	> 0) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end pls_obter_se_benef_liminar;
/