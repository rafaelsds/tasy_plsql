Create or replace
Function obter_dados_cartao_cr(	nr_seq_movto_cartao_p	number,
				ie_opcao_p		varchar2)
				return varchar2 is

nr_cartao_w			varchar2(255);
ds_cartao_w			varchar2(255) := '';
ds_pessoa_w			varchar2(255);
ds_bandeira_w			varchar2(255):= '';
ds_tipo_cartao_w			varchar2(255) := '';
ds_retorno_w			varchar2(255);
nm_pessoa_caixa_rec_w		varchar2(255);
nr_autorizacao_w			varchar2(255);
pos_w				number(10,0) := 1;
ds_observacao_w			varchar2(4000);
ds_comprovante_w			varchar2(100);
nr_atendimento_w			number(10,0);
nr_atend_pac_w			number(10,0);
nr_seq_nf_saida_w			number(15,0);
ds_pessoa_atend_w		varchar2(255);
vl_imposto_w			movto_cartao_cr_trib.vl_imposto%type;

BEGIN

if	(nr_seq_movto_cartao_p is not null) then
	select	nr_cartao,
		substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,80),
		b.ds_bandeira,
		--decode(a.ie_tipo_cartao,'C','Cr�dito','D�bito')
		decode(a.ie_tipo_cartao,'C',wheb_mensagem_pck.get_texto(302518),wheb_mensagem_pck.get_texto(302519)),
		a.nr_autorizacao,
		substr(a.ds_observacao,1,4000),
		a.ds_comprovante,
		substr(obter_nome_pf_pj(c.cd_pessoa_fisica, c.cd_cgc),1,255)
	into	nr_cartao_w,
		ds_pessoa_w,
		ds_bandeira_w,
		ds_tipo_cartao_w,
		nr_autorizacao_w,
		ds_observacao_w,
		ds_comprovante_w,
		nm_pessoa_caixa_rec_w
	from	caixa_receb c,
		bandeira_cartao_cr b,
		movto_cartao_cr a
	where	a.nr_seq_bandeira	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_movto_cartao_p
	and	a.nr_seq_caixa_rec	= c.nr_sequencia(+);

	-- N�mero com m�scara
	if	(ie_opcao_p	= 'NM') then 
		while	(nvl(length(substr(nr_cartao_w,pos_w,length(nr_cartao_w))),0) > 0) loop
			ds_cartao_w	:= substr(ds_cartao_w || substr(nr_cartao_w,pos_w,1), 1, 255);
			if	(pos_w in (4,8,12)) then
				ds_cartao_w	:= substr(ds_cartao_w || ' ', 1, 255);
			end if;
			pos_w	:= pos_w + 1;
		end loop;
	
		ds_retorno_w	:= ds_cartao_w;
	
	-- Pessoa
	elsif	(ie_opcao_p	= 'P') then
		ds_retorno_w	:= ds_pessoa_w;
	
	-- Bandeira
	elsif	(ie_opcao_p	= 'B') then
		ds_retorno_w	:= ds_bandeira_w;

	-- Tipo do cart�o
	elsif	(ie_opcao_p	= 'T') then
		ds_retorno_w	:= ds_tipo_cartao_w;
	
	-- Dados movimento
	elsif	(ie_opcao_p	= 'DM') then
		ds_retorno_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(817271) || ': ' || nr_autorizacao_w || ' - ' || WHEB_MENSAGEM_PCK.get_texto(817312) || ': '|| ds_comprovante_w || ' - ' || ds_tipo_cartao_w || 
				' - ' || ds_bandeira_w || ' - ' || ds_pessoa_w, 1, 255);
	elsif	(ie_opcao_p	= 'DMT') then
		ds_retorno_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(817271) || ': ' || nr_autorizacao_w || ' - ' || WHEB_MENSAGEM_PCK.get_texto(817312) || ': '|| ds_comprovante_w || ' - ' || ds_tipo_cartao_w || 
				' - ' || ds_bandeira_w || ' - ' || nm_pessoa_caixa_rec_w, 1, 255);
	elsif	(ie_opcao_p	= 'OBS') then
		ds_retorno_w	:= substr(ds_observacao_w,1,255);
	elsif	(ie_opcao_p	= 'PR') then
		ds_retorno_w	:= nm_pessoa_caixa_rec_w;
	elsif	(ie_opcao_p	= 'AUT') then
		ds_retorno_w	:= substr(nr_autorizacao_w,1,40);
	elsif	(ie_opcao_p	= 'AT') then
		select	max(d.nr_atendimento)
		into	nr_atendimento_w
		from	titulo_receber d,
			titulo_receber_liq c,
			caixa_receb b,
			movto_cartao_cr a
		where	a.nr_sequencia		= nr_seq_movto_cartao_p
		and	a.nr_seq_caixa_rec	= b.nr_sequencia
		and	b.nr_sequencia		= c.nr_seq_caixa_rec
		and	c.nr_titulo		= d.nr_titulo;

		ds_retorno_w	:= nr_atendimento_w;
	--Pessoa Atendimento
	elsif (ie_opcao_p	= 'PA') then
		select	max(d.nr_atendimento)
		into	nr_atend_pac_w
		from	titulo_receber d,
			titulo_receber_liq c,
			caixa_receb b,
			movto_cartao_cr a
		where	a.nr_sequencia		= nr_seq_movto_cartao_p
		and	a.nr_seq_caixa_rec	= b.nr_sequencia
		and	b.nr_sequencia		= c.nr_seq_caixa_rec
		and	c.nr_titulo		= d.nr_titulo;
		
		SELECT	substr(MAX(OBTER_NOME_PF_PJ(CD_PESSOA_FISICA,NULL)),1,255)
		INTO 	DS_PESSOA_ATEND_W
		FROM	ATENDIMENTO_PACIENTE
		WHERE	NR_ATENDIMENTO = NR_ATEND_PAC_W;
		
		ds_retorno_w	:= ds_pessoa_atend_w;
			
	elsif (ie_opcao_p	= 'PANF') then
		select	max(d.nr_seq_nf_saida)
		into	nr_seq_nf_saida_w
		from	titulo_receber d,
			titulo_receber_liq c,
			caixa_receb b,
			movto_cartao_cr a
		where	a.nr_sequencia		= nr_seq_movto_cartao_p
		and	a.nr_seq_caixa_rec	= b.nr_sequencia
		and	b.nr_sequencia		= c.nr_seq_caixa_rec
		and	c.nr_titulo		= d.nr_titulo;

		select	max(nr_atendimento)
		into	nr_atend_pac_w
		from	nota_fiscal_item
		where	nr_sequencia	= nr_seq_nf_saida_w;
		
		SELECT	substr(MAX(OBTER_NOME_PF_PJ(CD_PESSOA_FISICA,NULL)),1,255)
		INTO 	DS_PESSOA_ATEND_W
		FROM	ATENDIMENTO_PACIENTE
		WHERE	NR_ATENDIMENTO = NR_ATEND_PAC_W;
		
		ds_retorno_w	:= ds_pessoa_atend_w;
	
	elsif (ie_opcao_p = 'VLI') then /*Valor do Imposto do cartao*/
	
		select	sum(a.vl_imposto)
		into	vl_imposto_w
		from	movto_cartao_cr_trib a
		where	a.nr_seq_movto_cartao = nr_seq_movto_cartao_p;
		
		ds_retorno_w := nvl(vl_imposto_w,0);
		
	end if;
end if;

return	ds_retorno_w;

END obter_dados_cartao_cr;
/