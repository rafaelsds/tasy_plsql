create or replace
function lab_obter_condicao_prescr_pard(	nr_prescricao_p		number,
						nr_seq_prescr_p		number,
						nr_seq_exame_p		number,
						nr_seq_amostra_p 	number,
						nr_Seq_lote_exter_p	number,
						ie_opcao_p		varchar2)
						return varchar2 is
qt_exame_iguais_w	number(5);
qt_exame_total_igual_w	number(5);
ds_retorno_w		varchar2(255);
material_exame_integr_w	varchar2(255);
exame_integr_w		varchar2(255);

/* ie_opcao_p 
1	- descri��o amostra
2	- amostra
*/

begin
select	max(nvl(substr(Obter_Equipamento_Exame_dados(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI','M'),1,50),nvl(cd_material_integracao,b.cd_material_exame))),
	max(nvl(nvl(Obter_Equipamento_Exame_Mat(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI'),cd_exame_integracao),cd_exame))
into	material_exame_integr_w,
	exame_integr_w
from	prescr_procedimento a,
	material_exame_lab b,
	exame_laboratorio c
where	a.cd_material_exame = b.cd_material_exame
and	a.nr_seq_exame = c.nr_Seq_exame
and	a.nr_prescricao = nr_prescricao_p
and	a.nr_sequencia	= nr_seq_prescr_p
and	a.nr_seq_exame	= nr_seq_exame_p;

qt_exame_iguais_w	:= 1;

if ((material_exame_integr_w is not null) and (exame_integr_w is not null)) then
	
	if (nvl(nr_seq_amostra_p,0) > 0) then

		select 	count(*) 
		into	qt_exame_total_igual_w	
		from 	exame_laboratorio c,
			prescr_proc_material e,
			prescr_procedimento a,
			material_exame_lab b
		where	a.cd_material_exame 			= b.cd_material_exame
		and	e.nr_prescricao				= a.nr_prescricao
		and	e.nr_seq_material 			= b.nr_sequencia
		and	DECODE(e.nr_Seq_grupo,0,c.nr_seq_grupo,e.nr_Seq_grupo)	= c.nr_seq_grupo
		and	a.nr_Seq_exame				= c.nr_seq_exame
		and	a.nr_seq_exame				= nr_seq_exame_p
		and	a.nr_prescricao 			= nr_prescricao_p
		and	a.nr_Seq_lote_externo			= nr_Seq_lote_exter_p
		and	(nvl(substr(Obter_Equipamento_Exame_dados(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI','M'),1,50),nvl(cd_material_integracao,b.cd_material_exame))) = material_exame_integr_w
		and	(nvl(nvl(Obter_Equipamento_Exame_Mat(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI'),cd_exame_integracao),cd_exame)) = exame_integr_w;

		select 	count(*) 
		into	qt_exame_iguais_w	
		from 	exame_laboratorio c,
			prescr_proc_material e,
			prescr_procedimento a,
			material_exame_lab b
		where	a.cd_material_exame 			= b.cd_material_exame
		and	e.nr_prescricao				= a.nr_prescricao
		and	a.nr_Seq_lote_externo			= nr_Seq_lote_exter_p
		and	e.nr_seq_material 			= b.nr_sequencia
		and	DECODE(e.nr_Seq_grupo,0,c.nr_seq_grupo,e.nr_Seq_grupo)	= c.nr_seq_grupo
		and	a.nr_Seq_exame				= c.nr_seq_exame
		and	a.nr_seq_exame				= nr_seq_exame_p
		and	a.nr_prescricao 			= nr_prescricao_p
		and	(nvl(substr(Obter_Equipamento_Exame_dados(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI','M'),1,50),nvl(cd_material_integracao,b.cd_material_exame))) = material_exame_integr_w
		and	(nvl(nvl(Obter_Equipamento_Exame_Mat(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI'),cd_exame_integracao),cd_exame)) = exame_integr_w
		and	a.nr_sequencia	<= nr_seq_prescr_p
		and	e.nr_sequencia <= nr_seq_amostra_p;


	else

		select 	count(*) 
		into	qt_exame_total_igual_w	
		from 	prescr_procedimento a,
			material_exame_lab b,
			exame_Laboratorio d
		where	a.cd_material_exame 		= b.cd_material_exame
		and	a.nr_seq_exame			= nr_seq_exame_p
		and	a.nr_Seq_lote_externo		= nr_Seq_lote_exter_p
		and 	a.nr_Seq_exame			= d.nr_seq_exame
		and	a.nr_prescricao			= nr_prescricao_p
		and	(nvl(substr(Obter_Equipamento_Exame_dados(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI','M'),1,50),nvl(cd_material_integracao,b.cd_material_exame))) = material_exame_integr_w
		and	(nvl(nvl(Obter_Equipamento_Exame_Mat(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI'),cd_exame_integracao),cd_exame)) = exame_integr_w;

		select 	count(*) 
		into	qt_exame_iguais_w	
		from 	prescr_procedimento a,
			material_exame_lab b,
			exame_Laboratorio d
		where	a.cd_material_exame 		= b.cd_material_exame
		and 	a.nr_Seq_exame			= d.nr_seq_exame
		and	a.nr_Seq_lote_externo		= nr_Seq_lote_exter_p
		and	a.nr_seq_exame			= nr_seq_exame_p
		and	a.nr_prescricao 		= nr_prescricao_p
		and	(nvl(substr(Obter_Equipamento_Exame_dados(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI','M'),1,50),nvl(cd_material_integracao,b.cd_material_exame))) = material_exame_integr_w
		and	(nvl(nvl(Obter_Equipamento_Exame_Mat(a.nr_seq_exame,null,b.nr_sequencia,'PARDINI'),cd_exame_integracao),cd_exame)) = exame_integr_w
		and	a.nr_sequencia	<= nr_seq_prescr_p;
	end if;
	
end if;

if (qt_exame_total_igual_w > 1)	then

	if (ie_opcao_p = 1) then
		ds_retorno_w	:= qt_exame_iguais_w || '� Amostra';
	end if;

end if;

if (ie_opcao_p = 2) then
	ds_retorno_w	:= nvl(qt_exame_iguais_w,1);
end if;

return ds_retorno_w;
end;
/