create or replace
function cpoe_obter_seq_elem_mat_adic(cd_material_p	number) return number is 

nr_sequencia_w		nut_elem_material.nr_sequencia%type;

begin

select	max(d.nr_sequencia)
into	nr_sequencia_w
from	material c,
		nut_elem_material d
where	c.cd_material	= d.cd_material
and		c.cd_material = cd_material_p
and		nvl(d.ie_situacao, 'A') = 'A'
and		nvl(d.ie_tipo,'NPT') = 'NPT';

return nr_sequencia_w;

end cpoe_obter_seq_elem_mat_adic;
/