create or replace
function obter_se_existe_issue_aberta(	nr_story_p	number,
					nr_sprint_p	number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);
			
begin

select	decode(count(*),0,'N','S')
into	ie_retorno_w
from	desenv_story a,
	desenv_story_sprint b
where	a.nr_story = nr_story_p
and	b.nr_sprint = nr_sprint_p
and	a.nr_sequencia = b.nr_story
and	a.cd_tipo = 'IS'
and	nvl(b.cd_status,0) <> 4;

return	ie_retorno_w;

end obter_se_existe_issue_aberta;
/