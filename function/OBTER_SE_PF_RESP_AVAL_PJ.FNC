create or replace
function Obter_se_pf_resp_aval_pj(cd_cnpj_p		varchar2,
				cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ds_retorno_w			varchar2(1) := 'S';
cd_cargo_w			pessoa_fisica.cd_cargo%type;
ie_avaliacao_fornecedor_w	pessoa_jur_resp.ie_avaliacao_fornecedor%type;			

Cursor c01 is
select	a.ie_avaliacao_fornecedor
from	pessoa_jur_resp a
where	a.cd_cgc	= cd_cnpj_p
and	nvl(a.cd_cargo,nvl(cd_cargo_w,0)) = nvl(cd_cargo_w,0)
and	nvl(a.cd_pessoa_fisica,cd_pessoa_fisica_p)	= cd_pessoa_fisica_p
order by nvl(a.cd_pessoa_fisica,'0'),
	nvl(a.cd_cargo,0);
			
begin

select	max(a.cd_cargo)
into	cd_cargo_w
from	pessoa_fisica a
where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;

open C01;
loop
fetch C01 into	
	ie_avaliacao_fornecedor_w;
exit when C01%notfound;
	begin
	ds_retorno_w	:= ie_avaliacao_fornecedor_w;
	end;
end loop;
close C01;

return	ds_retorno_w;

end Obter_se_pf_resp_aval_pj;
/
