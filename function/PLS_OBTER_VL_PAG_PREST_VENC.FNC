create or replace
function pls_obter_vl_pag_prest_venc
			(	nr_seq_pag_prest_venc_p		Number,
				ie_opcao_p			varchar2) return Number is

/*
ie_opcao_p

1 - Valor dos itens de evento positivos
2 - Valor dos itens de eventos positivos menos os tributos
3 - Valor dos eventos positivos menos os tributos menos os eventos negativos

*/

nr_seq_pag_prestador_w			Number(10,0) := 0;
vl_item_positivo_w			Number(15,2) := 0;
vl_item_negativo_w			Number(15,2) := 0;
vl_tributo_w				Number(15,2) := 0;
vl_retorno_w				Number(15,2) := 0;

begin

select	max(nr_seq_pag_prestador)
into	nr_seq_pag_prestador_w
from	pls_pag_prest_vencimento
where	nr_sequencia			= nr_seq_pag_prest_venc_p;

select	nvl(sum(vl_item),0)
into	vl_item_positivo_w
from	pls_pagamento_item
where	nr_seq_pagamento	= nr_seq_pag_prestador_w
and	vl_item			> 0;

select	nvl(sum(vl_imposto),0)
into	vl_tributo_w
from	pls_pag_prest_venc_trib
where	nr_seq_vencimento	= nr_seq_pag_prest_venc_p
and	ie_pago_prev	= 'V';

select	nvl(sum(vl_item),0)
into	vl_item_negativo_w
from	pls_pagamento_item
where	nr_seq_pagamento	= nr_seq_pag_prestador_w
and	vl_item			< 0;

if	(ie_opcao_p = 1) then
	vl_retorno_w			:= vl_item_positivo_w;
elsif	(ie_opcao_p = 2) then
	vl_retorno_w			:= vl_item_positivo_w - vl_tributo_w;
elsif	(ie_opcao_p = 3) then
	vl_retorno_w			:= vl_item_positivo_w - vl_tributo_w + vl_item_negativo_w;
end if;

return	vl_retorno_w;

end pls_obter_vl_pag_prest_venc;
/
