CREATE OR REPLACE
FUNCTION obter_venc_cot_fornec_item(
			nr_cot_compra_p		number,
			nr_item_cot_compra_p	number)
			RETURN Number IS

nr_seq_item_fornec_w		number(10);
ie_regra_preco_w			varchar2(01);
cd_estabelecimento_w		number(4);
ie_somente_encerrada_w		varchar2(01);
ie_rentabilidade_w			varchar2(01) := 'N';
qt_existe_w			number(10);
ie_utiliza_conv_fornec_w		varchar2(01);
ie_forma_venc_cotacao_w			varchar2(15);
cd_cgc_fornecedor_venc_alt_w		cot_compra_item.cd_cgc_fornecedor_venc_alt%type;
ie_gerar_oc_itens_confirm_w		parametro_compras.ie_gerar_oc_itens_confirm%type;
ie_status_w	cot_compra_forn.ie_status%type;

CURSOR C01 IS
	select	a.nr_sequencia,
		NVL(b.ie_status,'AH')
	from	cot_compra_forn b,
		cot_compra_forn_item a
	where	a.nr_seq_cot_forn	= b.nr_sequencia
	and	a.nr_cot_compra 	= nr_cot_compra_p
	and	a.nr_item_cot_compra 	= nr_item_cot_compra_p
	and	a.vl_unitario_material	> 0
	and	nvl(a.ie_situacao,'A') = 'A'
	and	((cd_cgc_fornecedor_venc_alt_w = 'X') or
		((b.cd_cgc_fornecedor = cd_cgc_fornecedor_venc_alt_w)))
	order by
		decode(ie_utiliza_conv_fornec_w,'S',
			decode(nvl(a.qt_conv_unid_fornec,0), 0,
				dividir(a.vl_presente, to_number(obter_dados_material(a.cd_material,'QCE'))),
				dividir(a.vl_presente, a.qt_conv_unid_fornec)),0) desc,
		decode(obter_se_qt_dif_item_forn_cot(nr_cot_compra_p, nr_item_cot_compra_p), 'N',
			a.vl_presente, nvl(a.vl_unitario_liquido, a.vl_presente)) desc,
		a.qt_prioridade,
		a.vl_preco_liquido desc;

CURSOR C02 IS
	select	a.nr_sequencia,
		NVL(b.ie_status,'AH')
	from	cot_compra_forn b,
		cot_compra_forn_item a
	where	a.nr_seq_cot_forn	= b.nr_sequencia
	and	a.nr_cot_compra 	= nr_cot_compra_p
	and	a.nr_item_cot_compra 	= nr_item_cot_compra_p
	and	a.vl_unitario_material	> 0
	and	nvl(a.ie_situacao,'A') = 'A'
	and	((cd_cgc_fornecedor_venc_alt_w = 'X') or
		((b.cd_cgc_fornecedor = cd_cgc_fornecedor_venc_alt_w)))
	order by
		decode(ie_utiliza_conv_fornec_w,'S',
			decode(nvl(a.qt_conv_unid_fornec,0), 0,
				dividir(a.vl_presente, to_number(obter_dados_material(a.cd_material,'QCE'))),
				dividir(a.vl_presente, a.qt_conv_unid_fornec)),0) desc,
		decode(obter_se_qt_dif_item_forn_cot(a.nr_cot_compra, a.nr_item_cot_compra), 'N',
			a.vl_presente, nvl(a.vl_unitario_liquido, a.vl_presente)) desc,
		a.vl_presente desc,
		a.qt_prioridade,
		a.vl_preco_liquido desc;

CURSOR C03 IS
	select	a.nr_sequencia,
		NVL(b.ie_status,'AH')
	from	cot_compra_forn b,
		cot_compra_forn_item a
	where	a.nr_seq_cot_forn	= b.nr_sequencia
	and	a.nr_cot_compra 	= nr_cot_compra_p
	and	a.nr_item_cot_compra 	= nr_item_cot_compra_p
	and	a.vl_unitario_material	> 0
	and	nvl(a.ie_situacao,'A') = 'A'
	and	((cd_cgc_fornecedor_venc_alt_w = 'X') or
		((b.cd_cgc_fornecedor = cd_cgc_fornecedor_venc_alt_w)))
	order by
		decode(ie_utiliza_conv_fornec_w,'S',
			decode(nvl(a.qt_conv_unid_fornec,0), 0,
				dividir(a.vl_presente, to_number(obter_dados_material(a.cd_material,'QCE'))),
				dividir(a.vl_presente, a.qt_conv_unid_fornec)),0) desc,
		obter_result_cot_fornec_item(a.nr_sequencia),
		decode(obter_se_qt_dif_item_forn_cot(a.nr_cot_compra, a.nr_item_cot_compra), 'N',
			a.vl_presente, nvl(a.vl_unitario_liquido, a.vl_presente)) desc,
		a.vl_presente desc,
		a.qt_prioridade,
		a.vl_preco_liquido desc;

CURSOR C04 IS
	select	a.nr_sequencia,
		NVL(b.ie_status,'AH')
	from	cot_compra_forn b,
		cot_compra_forn_item a
	where	a.nr_seq_cot_forn	= b.nr_sequencia
	and	a.nr_cot_compra 	= nr_cot_compra_p
	and	a.nr_item_cot_compra 	= nr_item_cot_compra_p
	and	a.vl_unitario_material	> 0
	and	nvl(a.ie_situacao,'A') = 'A'
	and	((cd_cgc_fornecedor_venc_alt_w = 'X') or
		((b.cd_cgc_fornecedor = cd_cgc_fornecedor_venc_alt_w)))
	order by
		decode(ie_utiliza_conv_fornec_w,'S',
			decode(nvl(a.qt_conv_unid_fornec,0), 0,
				dividir(a.vl_presente, to_number(obter_dados_material(a.cd_material,'QCE'))),
				dividir(a.vl_presente, a.qt_conv_unid_fornec)),0) desc,
		nvl(a.vl_unitario_material, a.vl_presente) desc,
		a.vl_presente desc,
		a.qt_prioridade,
		a.vl_preco_liquido desc;

BEGIN

select	nvl(max(nr_seq_cot_item_forn),'0'),
	nvl(max(ie_regra_preco),'N')
into	nr_seq_item_fornec_w,
	ie_regra_preco_w
from	cot_compra_item
where	nr_cot_compra		= nr_cot_compra_p
and	nr_item_cot_compra	= nr_item_cot_compra_p;

select	cd_estabelecimento,
	ie_forma_venc_cotacao
into	cd_estabelecimento_w,
	ie_forma_venc_cotacao_w
from	cot_compra
where	nr_cot_compra = nr_cot_compra_p;

select	nvl(max(ie_somente_cot_encerrada),'N'),
	nvl(max(nvl(ie_forma_venc_cotacao_w, ie_forma_venc_cotacao)),'N'),
	nvl(max(ie_utiliza_conv_fornec),'N'),
	nvl(max(ie_gerar_oc_itens_confirm),'N')
into	ie_somente_encerrada_w,
	ie_rentabilidade_w,
	ie_utiliza_conv_fornec_w,
	ie_gerar_oc_itens_confirm_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;

begin
select	1
into	qt_existe_w
from	regra_conv_calc_cotacao
where	cd_estabelecimento = cd_estabelecimento_w
and	ie_situacao = 'A'
and	rownum = 1;
exception
when others then
	qt_existe_w	:=	0;
end;

if	(qt_existe_w > 0) then
	begin
	nr_seq_item_fornec_w := obter_venc_cot_for_item_regra(nr_cot_compra_p,nr_item_cot_compra_p);
	end;
else
	begin
	
	/*Para processos da bionexo. � quando j� vem o fornecedor selecionado no XML da bionexo. Entao independente do pre�o, esse fornecedor � sempre o vencedor.*/
	cd_cgc_fornecedor_venc_alt_w := 'X';

	if	(ie_gerar_oc_itens_confirm_w = 'S') then

		select	nvl(cd_cgc_fornecedor_venc_alt,'X')
		into	cd_cgc_fornecedor_venc_alt_w
		from	cot_compra_item
		where	nr_cot_compra = nr_cot_compra_p
		and	nr_item_cot_compra = nr_item_cot_compra_p;
	end if;
	/*Fim*/
	
	if	(nr_seq_item_fornec_w = 0) then
		begin
		if	(nvl(ie_rentabilidade_w,'N') = 'N') then
			if	(ie_regra_preco_w = 'N') then
				open c01;
				loop
				FETCH C01 into
					nr_seq_item_fornec_w,
					ie_status_w;
				exit when c01%notfound;
					if	(ie_somente_encerrada_w = 'N') or
						((ie_somente_encerrada_w = 'S') and (ie_status_w in ('AH','FF','FH'))) then
						nr_seq_item_fornec_w		:= nr_seq_item_fornec_w;
					end if;
				END LOOP;
				CLOSE C01;
			else
				open c02;
				loop
				FETCH C02 into
					nr_seq_item_fornec_w,
					ie_status_w;
				exit when c02%notfound;
					if	(ie_somente_encerrada_w = 'N') or
						((ie_somente_encerrada_w = 'S') and (ie_status_w in ('AH','FF','FH'))) then
						nr_seq_item_fornec_w		:= nr_seq_item_fornec_w;
					end if;
				END LOOP;
				CLOSE C02;
			end if;
		elsif	(nvl(ie_rentabilidade_w,'N') = 'S') then
			open c03;
				loop
				FETCH C03 into
					nr_seq_item_fornec_w,
					ie_status_w;
				exit when c03%notfound;
					if	(ie_somente_encerrada_w = 'N') or
						((ie_somente_encerrada_w = 'S') and (ie_status_w in ('AH','FF','FH'))) then
						nr_seq_item_fornec_w		:= nr_seq_item_fornec_w;
					end if;
				END LOOP;
				CLOSE C03;
		elsif	(nvl(ie_rentabilidade_w,'N') = 'U') then
			open c04;
				loop
				FETCH C04 into
					nr_seq_item_fornec_w,
					ie_status_w;
				exit when c04%notfound;
					if	(ie_somente_encerrada_w = 'N') or
						((ie_somente_encerrada_w = 'S') and (ie_status_w in ('AH','FF','FH'))) then
						nr_seq_item_fornec_w		:= nr_seq_item_fornec_w;
					end if;
				END LOOP;
				CLOSE C04;
		end if;
		end;
	end if;
	end;
end if;

RETURN	nr_seq_item_fornec_w;

END Obter_Venc_Cot_Fornec_item;
/
