create or replace
function	obter_empresa_integracao(
		nr_seq_sistema_p		number) return varchar2 is

ds_retorno_w		varchar2(80);

begin

select	a.nm_empresa
into	ds_retorno_w
from	int_empresa a,
	int_sistema b
where	b.nr_seq_empresa = a.nr_sequencia
and	b.nr_sequencia = nr_seq_sistema_p;

return	ds_retorno_w;

end obter_empresa_integracao;
/
