create or replace
function obter_seq_diaria_cont_escala(	nr_seq_escala_p			number,
										nr_seq_escala_diaria_p	number)
 		    	return varchar2 is

ds_retorno_w 	varchar2(1) := 'N';
nr_seq_w		number;		
		
begin
select	b.nr_sequencia
into	nr_seq_w
from	escala a,
		escala_diaria b
where	a.nr_sequencia = b.nr_seq_escala
and		a.nr_sequencia = nr_seq_escala_p
and		b.nr_sequencia = nr_seq_escala_diaria_p;

if		(nr_seq_w = nr_seq_escala_diaria_p) then
		ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end obter_seq_diaria_cont_escala;
/