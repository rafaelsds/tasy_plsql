CREATE OR REPLACE 
VIEW ENVIO_RPS_CAUCAIA_V AS
select	 1									tp_registro,
	'001'									nr_versao,	
	Max(substr(obter_dados_pf_pj('', n.cd_cgc_emitente, 'IM'), 1, 30))		nr_inscricao_municipal,
	count(n.nr_nota_fiscal)							qt_linhas,
	sum(n.vl_mercadoria)							vl_total_servicos,
	sum(n.vl_descontos)							vl_total_descontos,
	sum(decode(obter_se_nf_retem_iss(nr_sequencia), 'S',  null, 
	round_abnt(obter_valor_tipo_tributo_nota(nr_sequencia, 'B', 'ISS') * (obter_valor_tipo_tributo_nota(nr_sequencia, 'X', 'ISS') / 100),2)))	vl_total_iss,
	'P'									tp_processamento,
	''									nr_rps,
	''									cd_verificacao,
	null									dt_emissao,
	''									situacao_rps,
	null									vl_total,
	null									vl_servico,
	null									vl_deducao,
	null									vl_base,
	null									vl_aliquota,
	null									vl_iss,
	''									ie_iss_retido,
	null									vl_desconto_incond,
	null									vl_liquido,
	''									item_servico,
	''									cd_cnae,
	null									indicador_cpf_cnpj_tomador,
	''									cpf_cnpj_tomador,
	''									insc_munic_tomador,
	''									razao_social_tomador,
	''									end_tomador,
	''									numero_end_tomador,
	''									compl_tomador,
	''									bairro_tomador,
	''									cidade_tomador,
	''									uf_tomador,
	''									cep_tomador,
	''									email_tomador,
	''									ds_servicos,
	null									qt_linhas_arq,
	Max(n.cd_estabelecimento) 						cd_estabelecimento
FROM	NOTA_FISCAL N
WHERE	N.IE_SITUACAO = 1
AND	EXISTS(
	SELECT	1
	FROM	W_NOTA_FISCAL X
	WHERE	X.NR_SEQ_NOTA_FISCAL = N.NR_SEQUENCIA)
union
select	2									tp_registro,
	'001'									nr_versao,	
	''									nr_inscricao_municipal,
	null									qt_linhas,
	null									vl_total_servicos,
	null									vl_total_descontos,
	null									vl_total_iss,
	'P'									tp_processamento,
	n.nr_nota_fiscal 							nr_rps,
	n.nr_nfe_imp								cd_verificacao,
	n.dt_emissao								dt_emissao,
	decode(n.ie_situacao,'1','N','2','I','8','C','I')			situacao_rps,
	n.VL_TOTAL_NOTA								vl_total,
	n.vl_mercadoria								vl_servico,
	n.vl_descontos								vl_deducao,
	decode(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS'), 0, n.vl_mercadoria, obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS'))	vl_base,
	obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ISS')			vl_aliquota,
	decode(obter_se_nf_retem_iss(n.nr_sequencia), 'S',  null, 
	round_abnt(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS') * (obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ISS') / 100),2))	vl_iss,
	decode(obter_se_nf_retem_iss(n.nr_sequencia), 'S', '1', '2')		ie_iss_retido,
	n.vl_descontos								vl_desconto_incond,
	decode(obter_se_nf_retem_iss(n.nr_sequencia),'S',(campo_mascara((n.vl_mercadoria) - obter_Valor_Tributo_Nota(n.nr_sequencia, 'V') - nvl(n.vl_descontos, 0), 2)), 
	  ((n.vl_mercadoria) - (nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'PIS'), 0))
	- (nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'COFINS'), 0))
	- (nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'INSS'), 0))
	- (nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'IR'), 0))
	- (nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'CSLL'), 0))
	- (nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'O'), 0))
	- (decode(obter_se_nf_retem_iss(n.nr_sequencia), 'S', nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS'), 0), 0))
	- (nvl(n.vl_descontos, 0))))						vl_liquido,
	obter_cod_grupo_ativ(n.cd_estabelecimento, n.nr_sequencia, 'G')		item_servico,
	pls_obter_cd_cnae(obter_dados_pf_pj(NULL, n.cd_cgc_emitente, 'CNAE'))	cd_cnae,
	decode(n.cd_pessoa_fisica,null,2,1)					indicador_cpf_cnpj_tomador,
	nvl(substr(obter_cpf_pessoa_fisica(n.cd_pessoa_fisica),1,14),n.cd_cgc) 	cpf_cnpj_tomador,
	substr(obter_dados_pf_pj(null,n.cd_cgc,'IM'),1,20)			insc_munic_tomador,
	substr(tiss_eliminar_caractere(elimina_acentuacao(obter_nome_pf_pj(n.cd_pessoa_fisica, n.cd_cgc))), 1, 255) razao_social_tomador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'E'),1,50)	end_tomador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'NR'),1,10)	numero_end_tomador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CO'),1,30)	compl_tomador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'B'),1,30)	bairro_tomador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CI'),1,50)	cidade_tomador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'UF'),1,2)	uf_tomador,
	to_char(somente_numero(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CEP'),1,15)))	cep_tomador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'M'),1,60)	email_tomador,
	NVL(SUBSTR(obter_descricao_rps(n.cd_estabelecimento, n.nr_sequencia, 'DS_SERVICOS'), 1, 1000), 'Servicos')	ds_servicos,
	null									qt_linhas_arq,
	n.cd_estabelecimento							cd_estabelecimento
from	nota_fiscal n
where	n.ie_situacao = 1
and	exists(
	select	1
	from	w_nota_fiscal x
	where	x.nr_seq_nota_fiscal = n.nr_sequencia)
union
select	9									tp_registro,
	''									nr_versao,	
	''									nr_inscricao_municipal,
	null									qt_linhas,
	null									vl_total_servicos,
	null									vl_total_descontos,
	null									vl_total_iss,
	'P'									tp_processamento,
	''									nr_rps,
	''									cd_verificacao,
	null									dt_emissao,
	''									situacao_rps,
null										vl_total,
	null									vl_servico,
	null									vl_deducao,
	null									vl_base,
	null									vl_aliquota,
	null									vl_iss,
	''									ie_iss_retido,
	null									vl_desconto_incond,
	null									vl_liquido,
	''									item_servico,
	''									cd_cnae,
	null									indicador_cpf_cnpj_tomador,
	''									cpf_cnpj_tomador,
	''									insc_munic_tomador,
	''									razao_social_tomador,
	''									end_tomador,
	''									numero_end_tomador,
	''									compl_tomador,
	''									bairro_tomador,
	''									cidade_tomador,
	''									uf_tomador,
	''									cep_tomador,
	''									email_tomador,
	''									ds_servicos,
	(count(n.nr_sequencia) + 1)						qt_linhas_arq,
	n.cd_estabelecimento							cd_estabelecimento
from	nota_fiscal n
where	n.ie_situacao = 1
and	exists(
	select	1
	from	w_nota_fiscal x
	where	x.nr_seq_nota_fiscal = n.nr_sequencia)
group by n.cd_estabelecimento
/