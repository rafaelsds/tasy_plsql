create or replace
function mp_obter_tipo_grupo_obj(nr_seq_proc_objeto_p	number)
					return varchar2 is

ie_tipo_grupo_w		varchar2(2)	:= null;
nr_seq_objeto_w		number(10);
nr_seq_grupo_w		number(10);

begin

if	(nr_seq_proc_objeto_p is not null) then
	select	max(nr_seq_objeto)
	into	nr_seq_objeto_w
	from	mp_processo_objeto
	where	nr_sequencia	= nr_seq_proc_objeto_p;

	if	(nr_seq_objeto_w is not null) then
		select	max(nr_seq_grupo)
		into	nr_seq_grupo_w
		from	mp_objeto
		where	nr_sequencia	= nr_seq_objeto_w;

		if	(nr_seq_grupo_w is not null) then
			select	max(ie_tipo_grupo)
			into	ie_tipo_grupo_w
			from	mp_grupo_objeto
			where	nr_sequencia	= nr_seq_grupo_w;
		end if;	
	end if;
end if;

return	ie_tipo_grupo_w;

end mp_obter_tipo_grupo_obj;
/