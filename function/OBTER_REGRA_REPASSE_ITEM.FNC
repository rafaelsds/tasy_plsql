CREATE OR REPLACE FUNCTION obter_regra_repasse_item(nr_seq_repasse_p NUMBER) RETURN VARCHAR2 IS

ds_retorno_w  VARCHAR2(255);
nr_seq_regra_w NUMBER(10);
      
begin

  ds_retorno_w  := '';
  
  SELECT NVL(MAX(nr_seq_regra), 0)
  INTO   nr_seq_regra_w
  FROM   pls_repasse_mens_item
  WHERE  nr_seq_repasse = nr_seq_repasse_p;
  
  IF (nr_seq_regra_w > 0) then
    ds_retorno_w := to_char(nr_seq_regra_w);
  END IF;

RETURN  ds_retorno_w;

END obter_regra_repasse_item;
/