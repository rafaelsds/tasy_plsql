create or replace
function obter_prescr_dieta_interv(	cd_intervalo_p	varchar2,
					ie_item_p	varchar2)
					return varchar2 is

ie_prescricao_dieta_w		varchar2(1);
ie_visualiza_item_w		varchar2(255) := 'S';
cont_w				number(10);

begin

select	count(*)
into	cont_w
from	regra_intervalo_exclusivo
where	cd_intervalo = cd_intervalo_p;

if	(cont_w > 0) then

	select	count(*)
	into	cont_w
	from	regra_intervalo_exclusivo
	where	cd_intervalo 		= cd_intervalo_p
	and	ie_prescricao_dieta	= ie_item_p;
	
	if	(cont_w = 0) then
		ie_visualiza_item_w := 'N';
	end if;
end if;

return ie_visualiza_item_w;

end obter_prescr_dieta_interv;
/
