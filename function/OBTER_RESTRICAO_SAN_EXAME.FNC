create or replace
function obter_restricao_san_exame( nr_seq_exame_p 	number,
				    nr_seq_lote_p	number)
 		    	return varchar2 is

ie_restricao_w		varchar2(1);
nr_seq_producao_w	number(10);
nr_seq_reserva_w	number(10);
nr_seq_res_prod_w	number(10);
nr_seq_transfusao_w	number(10);
nr_seq_doacao_w		number(10);
ds_retorno_w		varchar2(255) := '';
ie_retorno_w		varchar2(1);	
	
begin

if (nr_seq_exame_p is not null) then

	select 	nvl(max(ie_restricao_exame),'N')
	into	ie_restricao_w
	from	san_exame
	where	nr_sequencia	= nr_seq_exame_p;

	select	max(nr_seq_producao),
		max(nr_seq_reserva),
		max(nr_seq_transfusao),
		max(nr_seq_res_prod),
		max(nr_seq_doacao)
	into	nr_seq_producao_w,
		nr_seq_reserva_w,
		nr_seq_transfusao_w,
		nr_seq_res_prod_w,
		nr_seq_doacao_w
	from	san_exame_lote
	where	nr_sequencia = nr_seq_lote_p;
	
	if ((ie_restricao_w = 'B') and ((nr_seq_reserva_w is not null) or (nr_seq_transfusao_w is not null) or (nr_seq_doacao_w is not null))) then
		ds_retorno_w := wheb_mensagem_pck.get_texto(796966);
	elsif (((nr_seq_producao_w is not null) or (nr_seq_res_prod_w is not null)) and (ie_restricao_w = 'P')) then
		ds_retorno_w := wheb_mensagem_pck.get_texto(796967);
	else
		ds_retorno_w := 'S';
	end if;
	
end if;

return	ds_retorno_w;

end obter_restricao_san_exame;
/
