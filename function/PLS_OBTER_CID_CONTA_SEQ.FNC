create or replace
function pls_obter_cid_conta_seq
			(	nr_seq_conta_p		number)
				return Varchar2 is

nr_seq_diagnostico_w	number(10);

begin

select	max(nr_sequencia)
into	nr_seq_diagnostico_w
from	pls_diagnostico_conta
where 	nr_seq_conta = nr_seq_conta_p
and	ie_classificacao = 'P';

return	nr_seq_diagnostico_w;

end pls_obter_cid_conta_seq;
/