create or replace
function obter_nome_tipo_aval_med(	nr_sequencia_p		number)
return varchar2 is

ds_tipo_w		varchar2(255);
			
begin

select	ds_tipo
into	ds_tipo_w
from	 med_tipo_avaliacao
where	nr_sequencia = nr_sequencia_p;

return	ds_tipo_w;

end obter_nome_tipo_aval_med;
/
