CREATE OR REPLACE function TASY.EMS_OBTER_CODIGO_ESTAB     (cd_estabelecimento_p number)
                            return number is

 cd_resultado_w         number(3);
begin

if    (cd_estabelecimento_p = 1) then
    cd_resultado_w := 16;
else    
    cd_resultado_w := 1;
end if;

return(cd_resultado_w);

end EMS_OBTER_CODIGO_ESTAB;
/