create or replace
function POA_obter_dias_isol(cd_motivo_isolamento_p		number)
 		    	return number is 
			
qt_dias_isolamento_w	number(3);
			
begin

if (cd_motivo_isolamento_p is not null) then
    select 	distinct(nvl(a.qt_dias_isolamento,0))
    into   	qt_dias_isolamento_w
    from  	motivo_isolamento a,
	atendimento_precaucao b
    where  	a.nr_sequencia = b.nr_seq_motivo_isol
    and	b.nr_seq_motivo_isol = cd_motivo_isolamento_p;
    
end if;
    
return	qt_dias_isolamento_w;

end POA_obter_dias_isol;
/