create or replace 
FUNCTION Obter_Ult_Cid_Atend_Pac_cod(nr_atendimento_p	number,
				ie_classificacao_p	varchar2,
				ie_opcao_p varchar2)
				RETURN VARCHAR2 IS

/*	ie_classificacao_p(classificação do cid)	 P-Principal S-Secundário
	ie_opcao_p		C-Código D-Descricao*/

cd_pessoa_fisica_w	varchar2(10);
cd_doenca_w			Varchar2(10);
ds_doenca_w			Varchar2(100);
ds_resultado_w			Varchar2(100);
ie_classificacao_w	varchar2(3);
dt_diagnostico_w	Date;

cursor c01 is
	select	a.cd_doenca,
			substr(obter_desc_cid(a.cd_doenca),1,100),
			a.ie_classificacao_doenca
	from	diagnostico_medico b,
			diagnostico_doenca a,
			atendimento_paciente x
	where	a.nr_atendimento			= x.nr_atendimento
	and		x.cd_pessoa_fisica			= cd_pessoa_fisica_w
	and		a.nr_atendimento			= b.nr_atendimento
	and		a.dt_diagnostico			= b.dt_diagnostico
	order by a.dt_diagnostico desc;

BEGIN
cd_doenca_w	:= '';

begin
cd_pessoa_fisica_w	:=	obter_pessoa_atendimento(nr_atendimento_p,'C');

if	(ie_opcao_p = 'C') then
	cd_doenca_w := obter_ultimo_cid_atend(nr_atendimento_p,ie_classificacao_p);
elsif (ie_opcao_p = 'D') then
	ds_doenca_w := obter_diagnostico_atendimento(nr_atendimento_p);
end if;

if	((ie_opcao_p = 'C') and (cd_doenca_w is null)) or
	((ie_opcao_p = 'D') and (ds_doenca_w is null)) then
	open C01;
	loop
	fetch C01 into
		cd_doenca_w,
		ds_doenca_w,
		ie_classificacao_w;
	exit when C01%notfound;
		begin
		if	((ie_opcao_p = 'C') and (cd_doenca_w is not null) and (ie_classificacao_w = ie_classificacao_p)) or
			((ie_opcao_p = 'D') and (ds_doenca_w is not null)) then
			exit;
		end if;
		end;
	end loop;
	close C01;
end if;

if	(ie_opcao_p = 'C') then
	ds_resultado_w := cd_doenca_w;
elsif (ie_opcao_p = 'D') then
	ds_resultado_w :=  cd_doenca_w || ' - ' || ds_doenca_w;
end if;

return ds_resultado_w;

exception
	when others then
	return '';
end;

END Obter_Ult_Cid_Atend_Pac_cod;
/