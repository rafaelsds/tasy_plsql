create or replace
function obter_se_acesso_bloqueado(ds_login_p varchar2)
return varchar2 is
ds_retorno varchar2(1) := '';
begin
	select	decode(nvl(max(ie_situacao),'A'),'A','N','S')
	into	ds_retorno
	from	controle_acesso_web
	where	ds_login = ds_login_p;
	return 	ds_retorno;
end;
/
