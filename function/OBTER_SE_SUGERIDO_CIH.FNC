create or replace
function obter_se_sugerido_cih(nr_prescricao_p		number,
			  nr_seq_material_p 	                number) 
			   return varchar2 is

ie_sugerido_w	varchar2(1) := 'N';                                                                                     

begin

select nvl(max('S'),'N')
into  ie_sugerido_w
from     medicamento_cih_sug
where  nr_prescricao    = nr_prescricao_p
and      nr_seq_material  = nr_seq_material_p;

return ie_sugerido_w;

end obter_se_sugerido_cih;
/