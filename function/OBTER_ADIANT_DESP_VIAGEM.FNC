create or replace
function obter_adiant_desp_viagem	(	cd_pessoa_fisica_p		varchar2	)
			return number is

vl_saldo_w		number(15,2) := 0;

begin

select	nvl(sum(nvl(vl_saldo, 0)), 0)
into	vl_saldo_w
from	adiantamento_pago
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	dt_baixa is null
and	vl_saldo > 0;

return	vl_saldo_w;

end obter_adiant_desp_viagem;
/