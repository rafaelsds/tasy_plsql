create or replace
function obter_itens_negativo_audit(	nr_seq_auditoria_p	number)
					return number is

qt_item_negativo_w	number(10) := 0;

begin

if	(nr_seq_auditoria_p is not null) then

	select	nvl(sum(x.qt_item_negativo),0)
	into	qt_item_negativo_w
	from 	(
		select  count(*) qt_item_negativo
		from    auditoria_conta_paciente a,
			material_atend_paciente  b,
			auditoria_matpaci        c
		where   a.nr_sequencia = nr_seq_auditoria_p
		and     a.nr_sequencia = c.nr_seq_auditoria
		and    	b.nr_sequencia = c.nr_seq_matpaci
		group by	b.cd_material
		having	sum(nvl(c.qt_ajuste, c.qt_original)) < 0
		union all
		select  count(*) qt_item_negativo
		from    auditoria_conta_paciente a,
			procedimento_paciente  b,
			auditoria_propaci        c
		where   a.nr_sequencia = nr_seq_auditoria_p
		and     a.nr_sequencia = c.nr_seq_auditoria
		and    	b.nr_sequencia = c.nr_seq_propaci
		group by	b.cd_procedimento,
			b.ie_origem_proced
		having	sum(nvl(c.qt_ajuste, c.qt_original)) < 0) x;

end if;	

return	qt_item_negativo_w;

end obter_itens_negativo_audit;
/