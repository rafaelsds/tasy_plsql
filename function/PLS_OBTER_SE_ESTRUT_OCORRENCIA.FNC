create or replace
function pls_obter_se_estrut_ocorrencia
			(	nr_seq_estrutura_p		number,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				nr_seq_material_p		number)
				return varchar2 is

ie_estrutura_ww			varchar2(3);				
ie_estrutura_w			varchar2(1)	:= 'N';
ie_estrut_mat_w			varchar2(1);
cd_area_procedimento_w		number(15) := 0;
cd_especialidade_w		number(15) := 0;
cd_grupo_proc_w			number(15) := 0;
nr_seq_estrutura_mat_w		number(15) := 0;
ie_origem_proced_w		number(10);
nr_seq_estrut_regra_w		number(10);

Cursor C01 is
	select	a.ie_estrutura,
		a.nr_seq_estrut_mat
	from	pls_ocorrencia_estrut_item	a
	where	a.nr_seq_estrutura	= nr_seq_estrutura_p
	and	((a.cd_procedimento is null)		or (a.cd_procedimento		= cd_procedimento_p))
	and	((a.ie_origem_proced is null)		or (a.ie_origem_proced		= ie_origem_proced_w))
	and	((a.cd_grupo_proc is null)		or (a.cd_grupo_proc		= cd_grupo_proc_w))
	and	((a.cd_especialidade is null)		or (a.cd_especialidade		= cd_especialidade_w))
	and	((a.cd_area_procedimento is null)	or (a.cd_area_procedimento	= cd_area_procedimento_w))
	and	((a.nr_seq_material is null)		or (a.nr_seq_material		= nr_seq_material_p))
	order by
		nvl(a.cd_procedimento,0),
		nvl(a.cd_grupo_proc,0),
		nvl(a.cd_especialidade,0),
		nvl(a.cd_area_procedimento,0),
		nvl(a.nr_seq_material,0),
		nvl(a.nr_seq_estrut_mat,0);
	
TYPE 		fetch_array IS TABLE OF C01%ROWTYPE;
s_array 	fetch_array;
i		Integer := 1;
type Vetor is table of fetch_array index by binary_integer;
Vetor_c01_w			Vetor;

begin
if	((cd_procedimento_p is not null) and 
	(ie_origem_proced_p is not null)) then /* Obter a estrutura do procedimento */
	pls_obter_estrut_proc(	cd_procedimento_p, ie_origem_proced_p, cd_area_procedimento_w,
				cd_especialidade_w, cd_grupo_proc_w, ie_origem_proced_w);
end if;

if	(nr_seq_material_p is not null) then /* Obter dados do material */
	begin
	select	nr_seq_estrut_mat
	into	nr_seq_estrutura_mat_w
	from	pls_material
	where	nr_sequencia = nr_seq_material_p;
	exception
	when others then
		nr_seq_estrutura_mat_w	:= 0;
	end;
end if;
/*
insert into logxxxx_tasy
	(cd_log, ds_log, dt_atualizacao, nm_usuario)
values	(11055, 'nr_seq_estrutura_p='||to_Char(nr_seq_estrutura_p)||chr(10)||
		'cd_procedimento_p='||to_Char(cd_procedimento_p)||chr(10)||
		'ie_origem_proced_w='||to_Char(ie_origem_proced_w)||chr(10)||
		'cd_grupo_proc_w='||to_Char(cd_grupo_proc_w)||chr(10)||
		'cd_especialidade_w='||to_Char(cd_especialidade_w)||chr(10)||
		'cd_area_procedimento_w='||to_Char(cd_area_procedimento_w)||chr(10)||
		'nr_seq_material_p='||to_Char(nr_seq_material_p)||chr(10)||
		'nr_seq_estrutura_mat_w='||to_Char(nr_seq_estrutura_mat_w)||chr(10), sysdate, 'Tasy');	*/

open C01;
loop
FETCH C01 BULK COLLECT INTO s_array LIMIT 1000;
	Vetor_c01_w(i) := s_array;
	i := i + 1;
EXIT WHEN C01%NOTFOUND;
END LOOP;
CLOSE C01;

for i in 1..Vetor_c01_w.COUNT loop
	s_array := Vetor_c01_w(i);
	for z in 1..s_array.COUNT loop
		begin
		nr_seq_estrut_regra_w	:= s_array(z).nr_seq_estrut_mat;
		
		ie_estrut_mat_w		:= 'S';
		
		if	(nr_seq_estrut_regra_w is not null) then
			if	(pls_obter_se_mat_estrutura(nr_seq_material_p, nr_seq_estrut_regra_w) = 'N') then
				ie_estrut_mat_w		:= 'N';
			end if;
		end if;
		
		if	(ie_estrut_mat_w = 'S') then
			ie_estrutura_w	:= s_array(z).ie_estrutura;
		end if;
		end;
	end loop;
end loop;

/*open C01;
loop
fetch C01 into	
	ie_estrutura_ww,
	nr_seq_estrut_regra_w;	
exit when C01%notfound;
	begin
	ie_estrut_mat_w	:= 'S';
	if	(nr_seq_estrut_regra_w is not null) then
		if	(pls_obter_se_mat_estrutura(nr_seq_material_p, nr_seq_estrut_regra_w) = 'N') then
			ie_estrut_mat_w		:= 'N';
		end if;
	end if;
	
	if	(ie_estrut_mat_w = 'S') then
		ie_estrutura_w	:= ie_estrutura_ww;
	end if;
	end;
end loop;
close C01;*/

return ie_estrutura_w;

end pls_obter_se_estrut_ocorrencia;
/