create or replace
function fu_obter_itens_adic_prod(	nr_sequencia_p		number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is
				
ds_retorno_w			varchar2(255) := null;				
nr_retorno_w			varchar2(255) := null;
ds_desc_w			varchar2(255);
cd_material_w			nut_atend_serv_dia_dieta.cd_material%type;
qt_porcentagem_W		nut_prod_cond_item_adic.qt_porcentagem%type;
qt_dose_w			nut_prod_cond_item_adic.qt_dose%type;
cd_unidade_medida_dosa_w	nut_atend_serv_dia_dieta.cd_unidade_medida_dosa%type;

Cursor C01 is
	select substr(nut_obter_dados_produto(b.NR_SEQ_PRODUTO,'DS'),1,80),
		b.cd_material,
		a.qt_porcentagem,
		a.qt_dose,
		b.cd_unidade_medida_dosa
	from  	nut_prod_cond_item_adic a,
		nut_atend_serv_dia_dieta b
	where 	a.nr_seq_dieta = b.nr_sequencia
	and	a.nr_seq_prod_item = nr_sequencia_p;
	
begin

if (nr_sequencia_p is not null and ie_opcao_p is not null) then
	open C01;
	loop
	fetch C01 into	
		ds_desc_w,
		cd_material_w,
		qt_porcentagem_w,
		qt_dose_w,
		cd_unidade_medida_dosa_w;
	exit when C01%notfound;
		begin
		
		if (nvl(qt_dose_w,0) > 0) then
			ds_retorno_w := ds_retorno_w||', '||ds_desc_w||'('||nvl(qt_dose_w,0)||' '||cd_unidade_medida_dosa_w||')';
			nr_retorno_w := nr_retorno_w||', '||cd_material_w||nvl(qt_dose_w,0) * 1000||0;
		else
			ds_retorno_w := ds_retorno_w||', '||ds_desc_w||' ('||nvl(qt_porcentagem_w,0)||'%)';
			nr_retorno_w := nr_retorno_w||', '||cd_material_w||0||nvl(qt_porcentagem_w,0) * 1000;
		end if;	
		
		end;
	end loop;
	close C01;

	if (ie_opcao_p = 'DS') then
		return	substr(ds_retorno_w,3,255);
	else
		return substr(nr_retorno_w,3,255);
	end if;	
end if;	

end fu_obter_itens_adic_prod;
/