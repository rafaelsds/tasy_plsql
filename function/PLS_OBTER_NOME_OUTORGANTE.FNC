create or replace
function PLS_OBTER_NOME_OUTORGANTE
		(nr_seq_outorgante_p	number) return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	obter_nome_pf_pj(null, cd_cgc_outorgante)
into	ds_retorno_w
from	pls_outorgante
where	nr_sequencia	= nr_seq_outorgante_p;

return	ds_retorno_w;

end PLS_OBTER_NOME_OUTORGANTE;
/
