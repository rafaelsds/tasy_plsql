create or replace
function CA_obter_result_exame(nr_seq_analise_p	number,
								nr_seq_exame_p	number)
 		    	return varchar2 is
				
ds_retorno_w	varchar2(255);
				
begin

select 	substr(decode(b.ie_tipo_resultado,'N', REPLACE(to_char(a.vl_resultado,'99999999990.9999'),' ',''), a.ds_resultado),1,255)
into	ds_retorno_w
from	ca_exame b,
		ca_exame_realizado a
where a.nr_seq_exame		= b.nr_sequencia
  and a.nr_seq_analise	= nr_seq_analise_p
  and a.nr_seq_exame		= nr_seq_exame_p;

return	ds_retorno_w;

end CA_obter_result_exame;
/