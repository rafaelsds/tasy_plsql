create or replace
function hpcs_verifica_valor_exame_rela	(cd_pessoa_fisica_p	varchar2,
					dt_dialise_p		date,
					nr_seq_exame_p		number)
 		    	return varchar2 is

ds_resultado_w		varchar2(255);
ds_resultado_ant_w	varchar2(255);
ds_retorno_w		varchar2(255);

begin
if	(cd_pessoa_fisica_p is not null) then
	select	substr(obter_resultado_mes_lab(cd_pessoa_fisica_p, dt_dialise_p, nr_seq_exame_p),1,255),
		substr(obter_resultado_mes_lab_ant(cd_pessoa_fisica_p, dt_dialise_p, nr_seq_exame_p),1,255)
	into	ds_resultado_w,
		ds_resultado_ant_w
	from	dual;
	
	
	if	(ds_resultado_w = '0') or (ds_resultado_w = 'NA') then
		ds_retorno_w	:= ds_resultado_ant_w;
	elsif	(ds_resultado_ant_w = '0') or (ds_resultado_ant_w = 'NA') then
		ds_retorno_w	:= ds_resultado_w;
	else
		ds_retorno_w	:= ds_resultado_w;		
	end if;
	
end if;

return	ds_retorno_w;

end hpcs_verifica_valor_exame_rela;
/