create or replace
function obter_se_atendida_farmacia(	nr_prescricao_p	number)
				return varchar2 is

ie_atendida_farmacia_w	varchar2(3);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(327113, null, wheb_usuario_pck.get_nr_seq_idioma);--Sim
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(327114, null, wheb_usuario_pck.get_nr_seq_idioma);--N�o

begin
if	(nr_prescricao_p is not null) then

	select	decode(count(*),0,expressao1_w,expressao2_w)
	into	ie_atendida_farmacia_w
	from	prescr_material
	where	NR_PRESCRICAO 	= nr_prescricao_p	
	and	ie_agrupador 	= 1
	and	cd_motivo_baixa <> 0;

end if;

return ie_atendida_farmacia_w;

end obter_se_atendida_farmacia;
/
