create or replace
function obter_tipo_fonte_prescr		(cd_material_p		number,
						 ie_opcao_p		varchar2)
				return varchar2 is
				
ie_tipo_fonte_prescr_w varchar2(3);				


begin

if (nvl(cd_material_p,0) > 0) then
	
	select	max(ie_tipo_fonte_prescr)
	into	ie_tipo_fonte_prescr_w
	from 	material c
	where	cd_material = cd_material_p;
	
end if;

return	ie_tipo_fonte_prescr_w;

end obter_tipo_fonte_prescr;
/