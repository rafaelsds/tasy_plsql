create or replace
function reg_obter_se_contingencia(	nr_regulacao_p		Number)
 		    	return  Varchar2 is

ds_retorno_w	Varchar2(1) := 'N';
			
begin
if	(nvl(nr_regulacao_p,0) > 0) then

	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	reg_log_integracao_v a,
		regulacao_atendimento b,
		informacao_integracao c
	where	b.nr_sequencia(+)	= a.nr_seq_regulacao_atendimento
	and	(a.cd_evento		IN ('ER','N') OR a.ie_tipo_evento = 'C')
	and	a.nr_seq_informacao	= c.nr_sequencia
	and	a.ie_tipo_evento 	<> 'C'
	and	b.nr_regulacao		= nr_regulacao_p;

end if;

return	ds_retorno_w;

end reg_obter_se_contingencia;
/

