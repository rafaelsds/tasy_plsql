create or replace
function pls_obter_nome_classif_estrut
			(	nr_sequencia_p		Number)
				return Varchar2 is
				
nm_estrutura_w			Varchar2(255);

begin

select	max(nm_estrutura)
into	nm_estrutura_w
from	pls_oc_classif_estrutura
where	nr_sequencia	= nr_sequencia_p;

return	nm_estrutura_w;

end pls_obter_nome_classif_estrut;
/