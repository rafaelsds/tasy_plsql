create or replace
function Lab_Obter_Obs_Prescr_Exames(nr_prescricao_p		number)
 		    	return varchar2 is
obs_prescricao_w	varchar2(2000);
cd_exame_w		varchar2(20);
obs_prescr_exame_w	varchar2(255);

Cursor C01 is
	select	p.ds_observacao_coleta,
		e.cd_exame
	from	prescr_procedimento p,
		exame_laboratorio e
	where	p.nr_prescricao = nr_prescricao_p
	and	p.nr_seq_exame = e.nr_seq_exame
	and	p.ds_observacao_coleta is not null;
begin
obs_prescricao_w := '';

open C01;
loop
fetch C01 into	
	obs_prescr_exame_w,
	cd_exame_w;
exit when C01%notfound;
	begin
	
	obs_prescricao_w := substr(obs_prescricao_w || cd_exame_w || '-' || obs_prescr_exame_w ||';',1,2000);		

	end;
end loop;
close C01;

return	obs_prescricao_w;

end Lab_Obter_Obs_Prescr_Exames;
/