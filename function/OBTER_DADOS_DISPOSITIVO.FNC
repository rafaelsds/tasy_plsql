create or replace
function obter_dados_dispositivo(	nr_seq_disp_pac_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(255);
ds_setor_atendimento_w	varchar2(100);
nr_seq_proc_interno_w	number(10,0);
cd_setor_atendimento_w	number(5,0);
					
begin

if	(nr_seq_disp_pac_p > 0) then

	select	nvl(max(a.nr_seq_proc_interno),0)
	into	nr_seq_proc_interno_w
	from	dispositivo_proc_interno a,
		atend_pac_dispositivo b
	where	a.nr_seq_dispositivo	=	b.nr_seq_dispositivo
	and	b.nr_sequencia		=	nr_seq_disp_pac_p
	and	a.ie_acao in ('I','U');
	
	if	(nr_seq_proc_interno_w > 0) then
		select	max(a.cd_setor_atendimento)
		into	cd_setor_atendimento_w
		from	atend_pac_disp_proc a
		where	a.nr_seq_proc_interno	= nr_seq_proc_interno
		and	a.nr_seq_disp_pac 	= nr_seq_disp_pac_p;
	end if;
		
end if;

if	(ie_opcao_p = 'S') then
	ds_retorno_w := substr(obter_nome_setor(cd_setor_atendimento_w),1,100);
elsif	(ie_opcao_p = 'CS') then
	ds_retorno_w := to_char(cd_setor_atendimento_w);
end if;

return	ds_retorno_w;

end obter_dados_dispositivo;
/