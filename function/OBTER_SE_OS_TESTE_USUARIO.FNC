create or replace
function obter_se_os_teste_usuario (
		nr_seq_os_p	number,
		nm_usuario_p	varchar2)
		return varchar2 is
		
ie_os_teste_w		varchar2(1) := 'N';
cd_pessoa_usuario_w	varchar2(10);
		
begin
if	(nr_seq_os_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_usuario_w
	from	usuario
	where	nm_usuario = nm_usuario_p;	
	
	select	decode(count(*),0,'N','S')
	into	ie_os_teste_w
	from	man_ordem_servico
	where	nr_sequencia = nr_seq_os_p
	and	((cd_pessoa_solicitante = cd_pessoa_usuario_w)
	or	 (nm_usuario_exec = nm_usuario_p)
	or	 (nm_usuario_nrec = nm_usuario_p))
	and	nr_seq_estagio in (2,41,1511);
	end;
end if;
return ie_os_teste_w;
end obter_se_os_teste_usuario;
/