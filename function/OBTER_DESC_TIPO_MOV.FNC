create or replace
function obter_desc_tipo_mov
			(	ie_tipo_movimentacao_p	varchar2)
				return varchar2 is

ds_descricao_w			varchar2(100) := '';

begin
if	(ie_tipo_movimentacao_p is not null) then
	if	(ie_tipo_movimentacao_p	= 'P') then
		ds_descricao_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(1016453),1,255);
	else
		ds_descricao_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(1016454),1,255);
	end if;
end if; 

return ds_descricao_w;

end obter_desc_tipo_mov;
/