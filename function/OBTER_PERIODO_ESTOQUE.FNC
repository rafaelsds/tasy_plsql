create or replace
function obter_periodo_estoque(	dt_inicio_p		date,
					dt_final_p		date,
					cd_estabelecimento_p	number) 
				return number is

qt_dias_w		number(3);

begin

select	/* + index (a EISESTO_I1) */
	count(distinct dt_referencia)
into	qt_dias_w
from	eis_estoque_v a
where	cd_estabelecimento = cd_estabelecimento_p 
and	cd_local_estoque is not null
and	dt_referencia between dt_inicio_p and dt_final_p;

return	qt_dias_w;

end obter_periodo_estoque;
/