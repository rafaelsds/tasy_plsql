create or replace
function obter_resultado_CPIS( qt_pontuacao_p		Number)
			return varchar2 is

ds_retorno_w		Varchar2(100);

/*
0 a 6	= Baixa probabilidade de pneumonia relacionada a ventila��o mec�nica
7 a 17	= Alta probabilidade de pneumonia relacionada a ventila��o mec�nica
*/

begin
ds_retorno_w	:= '';
if	(qt_pontuacao_p between 0 and 6) then
	ds_retorno_w	:= Wheb_mensagem_pck.get_texto(309688); --'Baixa probabilidade de pneumonia relacionada a ventila��o mec�nica';
elsif	(qt_pontuacao_p between 7 and 16) then
	ds_retorno_w	:= Wheb_mensagem_pck.get_texto(309691); --'Alta probabilidade de pneumonia relacionada a ventila��o mec�nica';
end if;

return ds_retorno_w;

end obter_resultado_CPIS;
/