create or replace
function reg_doc_has_user_delegation (	nr_seq_document_p		number,
					ie_reviewer_type_p		varchar2,
					nm_user_reviewer_p		varchar2,
					nm_user_delegation_p		varchar2 )
 		    	return varchar2 is

ie_has_delegation_w		varchar2(1);

begin

select	decode(count(1), 0, 'N', 'S')
into	ie_has_delegation_w
from	reg_documento doc,
	reg_doc_revisor dr,
	reg_doc_delegacao dd
where	doc.nr_sequencia = dr.nr_seq_documento
and	dr.nr_sequencia = dd.nr_seq_reg_doc_revisor
and	doc.nr_sequencia = nr_seq_document_p
and	dr.ie_tipo_revisor = ie_reviewer_type_p
and	dr.nm_usuario_revisor = nm_user_reviewer_p
and	dd.nm_usuario_delegacao = nm_user_delegation_p
and	(
		(dd.dt_fim_vigencia is null and sysdate > dd.dt_inicio_vigencia) or
		(sysdate between dd.dt_inicio_vigencia and fim_dia(dd.dt_fim_vigencia))
	);

return ie_has_delegation_w;

end reg_doc_has_user_delegation;
/