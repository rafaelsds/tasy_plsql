create or replace
function cus_obter_custo_proc_mat(	cd_estabelecimento_p	number,
				cd_material_p		number,
				cd_procedimento_p		number,
				ie_origem_proced_p	number,
				nr_seq_proc_interno_p	number,
				dt_referencia_p		date)
				return number is

cd_tabela_custo_w			number(10);
cd_tipo_tabela_custo_w		number(10);
dt_referencia_w			date;
vl_retorno_w			number(15,4);

begin

dt_referencia_w		:= trunc(dt_referencia_p,'mm');
cd_tipo_tabela_custo_w	:= 3;

if	(cd_material_p is not null) then
	cd_tipo_tabela_custo_w	:= 4;
end if;



select	max(cd_tabela_custo)
into	cd_tabela_custo_w
from	tabela_custo
where	cd_estabelecimento		= cd_estabelecimento_p
and	cd_tipo_tabela_custo	= cd_tipo_tabela_custo_w
and	dt_mes_referencia		= dt_referencia_w;

if	(cd_tabela_custo_w is not null) then
	begin
	if	(cd_tipo_tabela_custo_w = 3) then
		begin
		
		if	(nvl(nr_seq_proc_interno_p,0) = 0) then
			begin
			select	nvl(max(vl_cotado),0)
			into	vl_retorno_w
			from	custo_procedimento
			where	cd_tabela_custo		= cd_tabela_custo_w
			and	cd_estabelecimento		= cd_estabelecimento_p
			and	cd_procedimento		= cd_procedimento_p
			and	ie_origem_proced		= ie_origem_proced_p;
			end;
		else
			begin
			select	nvl(max(vl_cotado),0)
			into	vl_retorno_w
			from	custo_procedimento
			where	cd_tabela_custo		= cd_tabela_custo_w
			and	cd_estabelecimento		= cd_estabelecimento_p
			and	nr_seq_proc_interno	= nr_seq_proc_interno_p;
			end;
		end if;
		
		end;
	elsif	(cd_tipo_tabela_custo_w = 4) then
		begin
		select	nvl(max(vl_cotado_consumo),0)
		into	vl_retorno_w
		from	custo_material
		where	cd_tabela_custo	= cd_tabela_custo_w
		and	cd_estabelecimento	= cd_estabelecimento_p
		and	cd_material	= cd_material_p;
		end;
		
	end if;
	end;
end if;

return	vl_retorno_w;

end cus_obter_custo_proc_mat;
/