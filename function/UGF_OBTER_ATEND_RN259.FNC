create or replace
function UGF_obter_atend_rn259(	dt_inicial_p		date,
				dt_final_p		date,
				cd_agenda_p		number,
				cd_tipo_agenda_p	number)
				return varchar2 is
					

qt_dias_portaria_w		number(10);
qt_dias_atendimento_w		number(10);
cd_especialidade_agenda_w	number(5,0);
ds_retorno_w			varchar2(255);
					
begin
qt_dias_portaria_w	:= 0;
qt_dias_atendimento_w	:= 0;

select	nvl(obter_especialidade_agenda(cd_agenda_p), 0)
into	cd_especialidade_agenda_w
from	dual;	

if	(dt_final_p <> to_date('30/12/1899','dd/mm/yyyy')) then
	
	select 	obter_dias_entre_datas_uteis(dt_inicial_p,dt_final_p)
	into	qt_dias_atendimento_w
	from 	dual;	

	if	(cd_tipo_agenda_p = 3)then
	
		if	(cd_especialidade_agenda_w <> 0) and
			(cd_especialidade_agenda_w in (3, 4, 9, 20)) then
			qt_dias_portaria_w	:= 7;	

			if	(qt_dias_atendimento_w > qt_dias_portaria_w) then
				ds_retorno_w := 'Fora do Prazo';
			else
				ds_retorno_w := 'Dentro do Prazo';
			end if;		
		elsif	(cd_especialidade_agenda_w not in (3, 4, 9, 20)) then
				qt_dias_portaria_w	:= 14;	
				
				if	(qt_dias_atendimento_w > qt_dias_portaria_w) then
					ds_retorno_w := 'Fora do Prazo';
				else
					ds_retorno_w := 'Dentro do Prazo';
				end if;	
			
		end if;
		
	elsif	(cd_tipo_agenda_p = 5) then
	
		select nvl(decode(cd_agenda_p,	1075,3,
						3626,3,
						3496,3,
						1005,3,
						1015,3,
						1025,3,
						1045,3,
						1055,3,
						1056,3,
						1057,3,
						1058,3,
						1065,3,
						1066,3,
						2855,3,
						2856,3,
						4655,3,
						4656,3,
						4657,3,
						4658,3,
						4495,10,
						2305,10,
						4767,0,
						107,0,
						3555,0,
						4826,0,
						4785,0,
						5125,0,
						3138,0,
						3140,0,
						337,0,
						3143,0,
						3142,0,
						308,0,
						3206,0,
						309,0,
						358,0,
						125,0,
						295,0,
						307,0,
						236,0,
						359,0,
						3141,0,
						4697,0,
						3556,0,
						360,0,
						3957,0,
						355,0,
						247,0,
						3144,0,
						2995,0,
						3136,0,
						278,0,
						4355,0,
						4636,0,
						4466,0,
						4415,0,
						5157,0,
						248,0,
						157,0,
						249,0,
						4775,0,
						246,0,
						825,0,
						365,0,
						126,0,
						4689,0,
						3139,0,
						4965,0,
						245,0,
						4245,0,
						3906,0,
						4185,0,
						4125,0,
						3697,0,
						4435,0,
						4635,0,
						5295,0,
						115,0,
						104,0,
						2825,0,
						1645,0,
						645,0
						),0)
		into	qt_dias_portaria_w
		from	dual;				
			
		if	(qt_dias_atendimento_w > qt_dias_portaria_w) then
			ds_retorno_w := 'Fora do Prazo';
		else
			ds_retorno_w := 'Dentro do Prazo';
		end if;	
		
	else
		
		select	nvl(decode(cd_agenda_p, 1265, 21, 1225, 21, 1285, 10, 1275, 21, 1205, 10, 1215, 10, 1905, 10, 1625, 10, 1615, 10, 1845, 10, 1835, 10, 1605, 10, 2345, 10, 2305, 10, 1985, 10), 0)
		into	qt_dias_portaria_w
		from	dual;

		if	(qt_dias_portaria_w <> 0)then
		
			if	(qt_dias_atendimento_w > qt_dias_portaria_w) then
				ds_retorno_w := 'Fora do Prazo';
			else
				ds_retorno_w := 'Dentro do Prazo';
			end if;	
		else
			ds_retorno_w := 'Fora do Prazo';
		end if;
		
	end if;
	
else
	ds_retorno_w := 'Fora do Prazo';
	
end if;
return	ds_retorno_w;

end UGF_obter_atend_rn259;
/
