create or replace
function OBTER_SE_MOSTRA_ALERTA_CLASSIF (cd_funcao_ativa_p		number,
					nr_seq_classif_pessoa_p		number) return varchar2 is

ie_mostra_alerta_w	varchar2(10);

cursor c01 is
select	ie_mostra_alerta
from	regra_alerta_classif_pf
where	cd_funcao	= cd_funcao_ativa_p
and	nr_seq_classif	= nr_seq_classif_pessoa_p
and	ie_situacao	= 'A';

begin

ie_mostra_alerta_w	:= 'S';

open c01;
loop
fetch c01 into
	ie_mostra_alerta_w;
exit when c01%notfound;
	ie_mostra_alerta_w	:= ie_mostra_alerta_w;
end loop;
close c01;

return ie_mostra_alerta_w;

end OBTER_SE_MOSTRA_ALERTA_CLASSIF;
/
