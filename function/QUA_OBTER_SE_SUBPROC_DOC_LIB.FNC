create or replace
function qua_obter_se_subproc_doc_lib(
			nr_sequencia_p	number,
			nm_usuario_p	varchar2)
 		    	return varchar2 is
			
ie_retorno_w		varchar2(1) := 'S';
qt_reg_w			number(15,0);
cd_setor_w		number(5,0);

begin

select	count(*)
into	qt_reg_w
from	qua_doc_subproc_emp a
where	a.nr_seq_doc = nr_sequencia_p
and exists(	select	1
		from	qua_subprocesso_lib x
		where	x.nr_seq_subprocesso = a.nr_seq_subprocesso);

if	(qt_reg_w > 0) then
	begin
	ie_retorno_w := 'N';
	
	begin
	select	cd_setor_atendimento
	into	cd_setor_w
	from	usuario
	where	nm_usuario = nm_usuario_p;
	exception
	when others then
		ie_retorno_w := 'S';
	end;
	
	if	(ie_retorno_w = 'N') then
		begin
		select	'S'
		into	ie_retorno_w
		from	qua_subprocesso_lib b,
			qua_doc_subproc_emp a
		where	a.nr_seq_subprocesso	= b.nr_seq_subprocesso
		and	a.nr_seq_doc		= nr_sequencia_p
		and	b.cd_setor_lib		= cd_setor_w	
		and	rownum < 2;
		exception
		when others then
			ie_retorno_w := 'N';
		end;
	end if;	
	end;
end if;

return	ie_retorno_w;

end qua_obter_se_subproc_doc_lib;
/