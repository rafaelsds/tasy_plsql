create or replace
function sae_obter_valor_resultado(
		nr_seq_prescr_p	number,
		nr_seq_diag_p	number,
		nr_seq_likert_p	number)
 		return number is

nr_sequencia_w	number(10);
ie_resultado_w	number(3);

begin

	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	pe_prescr_diag
	where	nr_seq_prescr = nr_seq_prescr_p
	and	nr_seq_diag = nr_seq_diag_p;

	select	nvl(max(ie_resultado),5)
	into	ie_resultado_w
	from	pe_prescr_diag_likert
	where	nr_seq_diag = nr_sequencia_w
	and	nr_seq_likert = nr_seq_likert_p;

return	ie_resultado_w;

end sae_obter_valor_resultado;
/