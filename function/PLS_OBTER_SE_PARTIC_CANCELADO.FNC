create or replace
function pls_obter_se_partic_cancelado(nr_seq_conta_proc_p 	pls_conta_proc.nr_sequencia%type)
 		    	return varchar2 is

qt_partic_conta_w	pls_integer;
qt_partic_cancelado_w	pls_integer;
ie_partic_zero_w	varchar2(1);
begin
select	nvl(sum(decode(ie_status,'C',1,0)),0),
	count(1)
into	qt_partic_cancelado_w,
	qt_partic_conta_w
from	pls_proc_participante
where	nr_seq_conta_proc = nr_seq_conta_proc_p;

/*Ir� buscar se no procedimento existem participantes que n�o foram cancelados.*/
if	(qt_partic_cancelado_w = qt_partic_conta_w) and
	(qt_partic_conta_w > 0) then
	ie_partic_zero_w := 'S';
else
	ie_partic_zero_w := 'N';
end if;
return	ie_partic_zero_w;

end pls_obter_se_partic_cancelado;
/
