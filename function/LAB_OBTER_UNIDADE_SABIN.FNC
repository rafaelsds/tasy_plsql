create or replace
function lab_obter_unidade_sabin(cd_estabelecimento_p number)
 		    	return varchar2 is
ds_retorno_w 	varchar2(10);
begin

ds_retorno_w := null;

select to_char(cd_identificador)
    into ds_retorno_w
   from empresa_integr_dados
where cd_estabelecimento = cd_estabelecimento_p
    and nr_seq_empresa_integr = 154
    and rownum = 1;

return ds_retorno_w;

end lab_obter_unidade_sabin;
/		