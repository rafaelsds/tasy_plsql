create or replace 
function obter_nivel_clinico_news(qt_pontuacao_p number,
								  nr_sequencia_p number default 0) return varchar2 is

ie_parametro3_w  varchar2(1) := 'N';

begin

ie_parametro3_w := obter_parametro_3_news(nr_sequencia_p);

if (qt_pontuacao_p >= 7) then
	return substr(obter_desc_expressao(327347,'Alto'),1,255);
elsif (qt_pontuacao_p between 5 and 6) or (ie_parametro3_w = 'S') then
	return substr(obter_desc_expressao(293174,'M�dio'),1,255);
elsif (qt_pontuacao_p between 0 and 4) then
	return substr(obter_desc_expressao(327348,'Baixo'),1,255);
end if;

return '';

end;
/
