CREATE OR REPLACE
FUNCTION cm_obter_desc_local_conj(nr_seq_local_conj_p NUMBER)
 		    	RETURN VARCHAR2 IS

ds_retorno_w	VARCHAR2(255);

BEGIN

SELECT	MAX(ds_localizacao) ds_localizacao
INTO	ds_retorno_w
FROM	cm_local_conjunto
WHERE	nr_sequencia = nr_seq_local_conj_p;

RETURN	ds_retorno_w;

END cm_obter_desc_local_conj;
/