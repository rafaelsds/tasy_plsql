create or replace
function pls_obter_valores_prop_2170
			(	nr_seq_proposta_p	number,
				nr_seq_contrato_p	number,
				nr_seq_plano_p		number,
				ie_taxa_inscricao_p	varchar2,
				ie_opcao		varchar2)
			return number is	
/*
TC - Taxa de coparticipação				
MC - Valor máximo de coparticipação
 TI - Taxa de inscrição
 */
			
ds_retorno	number(15,2);
		
begin

if	(ie_opcao = 'TC') then
	select	sum(a.tx_coparticipacao) 
	into	ds_retorno
	from	pls_regra_coparticipacao	a
	where	a.nr_seq_proposta = nr_seq_proposta_p;

	if	(ds_retorno is null) then
		select	max(a.tx_coparticipacao) 
		into	ds_retorno
		from	pls_regra_coparticipacao	a
		where	a.nr_seq_contrato = nr_seq_contrato_p;
		
		if	(ds_retorno is null) then
			select	max(a.tx_coparticipacao) 
			into	ds_retorno
			from	pls_regra_coparticipacao	a
			where	a.nr_seq_plano = nr_seq_plano_p;
		end if;
	end if;
end if;

if	(ie_opcao = 'MC') then
	select	sum(a.vl_maximo)	
	into	ds_retorno
	from	pls_regra_coparticipacao	a
	where	a.nr_seq_proposta = nr_seq_proposta_p;

	if	(ds_retorno is null) then
		select	max(a.vl_maximo) 
		into	ds_retorno
		from	pls_regra_coparticipacao	a
		where	a.nr_seq_contrato = nr_seq_contrato_p;
		
		if	(ds_retorno is null) then
			select	max(a.vl_maximo) 
			into	ds_retorno
			from	pls_regra_coparticipacao	a
			where	a.nr_seq_plano = nr_seq_plano_p;
		end if;
	end if;
end if;

if	(ie_opcao = 'TI') then
	select	sum(a.vl_inscricao)	
	into	ds_retorno
	from	pls_regra_inscricao	a
	where	a.nr_seq_proposta = nr_seq_proposta_p
	and	nvl(ie_taxa_inscricao_p,'S') ='S'
	and	((sysdate between nvl(a.dt_inicio_vigencia,sysdate) and nvl(a.dt_fim_vigencia,sysdate)	and a.ie_grau_dependencia in ('A','T')));

	if	(ds_retorno is null) then
		select	max(a.vl_inscricao) 
		into	ds_retorno
		from	pls_regra_inscricao	a
		where	a.nr_seq_contrato = nr_seq_contrato_p
		and	nvl(ie_taxa_inscricao_p,'S') ='S'
		and	((sysdate between nvl(a.dt_inicio_vigencia,sysdate) and nvl(a.dt_fim_vigencia,sysdate)	and a.ie_grau_dependencia in ('A','T')));
		
		if	(ds_retorno is null) then
			select	max(a.vl_inscricao) 
			into	ds_retorno
			from	pls_regra_inscricao	a
			where	a.nr_seq_plano = nr_seq_plano_p
			and	nvl(ie_taxa_inscricao_p,'S') ='S'
			and	((sysdate between nvl(a.dt_inicio_vigencia,sysdate) and nvl(a.dt_fim_vigencia,sysdate)	and a.ie_grau_dependencia in ('A','T')));
		end if;
	end if;
end if;


return	ds_retorno;

end pls_obter_valores_prop_2170;
/