create or replace
function obter_sup_pasta(nr_seq_pasta_p	number)
 		    	return number is

nr_seq_obj_sup_w dic_objeto.nr_seq_obj_sup%Type;

begin

select 	nr_seq_obj_sup 
into   	nr_seq_obj_sup_w
from 	dic_objeto 
where 	nr_sequencia = nr_seq_pasta_p;

return	nvl(nr_seq_obj_sup_w,0);

end obter_sup_pasta;
/