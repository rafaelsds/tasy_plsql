create or replace
function obter_inf_hor_prescr(nr_atendimento_p	number,
				  nr_prescricao_p	number,
				  dt_inicial_p		date,
				  dt_final_p		date,
				  cd_material_w		number,
				  qt_dose_w		number,
				  cd_unid_med_dose_w	varchar2,
				  cd_intervalo_w	varchar2,
				  ie_via_w		varchar2,			
				  ie_opcao_p		varchar2)
 		    	return varchar2 is
			
/*  IE_OPCAO_P
D =	prescr_dieta_hor
M = 	prescr_mat_hor
SNE =   prescr_mat_hor
R = 	prescr_rec_hor
*/

dt_horario_w		date;
dt_horario_ant_w	date := to_date('01/01/2000','dd/mm/yyyy');
ie_situacao_w 		varchar2(1);
dt_horarios_w		varchar2(2000) := ' ';

cursor c01 is
	select	distinct h.dt_horario,
			substr(replace(obter_status_hor_prescr(h.nr_sequencia,'D'),'N',''),1,1)
	from	prescr_dieta_hor h,
		prescr_medica m
	where	h.nr_prescricao = m.nr_prescricao
	and	h.dt_horario between dt_inicial_p and dt_final_p
	and	m.nr_atendimento = nr_atendimento_p
	and	ie_opcao_p	= 'D'
	and	Obter_se_horario_liberado(h.dt_lib_horario, h.dt_horario) = 'S'
	union all
	select	distinct h.dt_horario,
		decode(replace(obter_status_hor_prescr(h.nr_sequencia,'M'),'N',''),'E','', substr(replace(obter_status_hor_prescr(h.nr_sequencia,'M'),'N',''),1,1))
	from	prescr_material a,
		prescr_mat_hor h,
		prescr_medica m
	where	h.nr_prescricao = m.nr_prescricao
	and	a.nr_prescricao = m.nr_prescricao
	and	a.cd_material 	= h.cd_material
	and	h.dt_horario between dt_inicial_p and dt_final_p
	and	m.nr_atendimento = nr_atendimento_p
	and	((m.nr_prescricao = nr_prescricao_p)  or  (nr_prescricao_p = 0))
	and	h.cd_material	= cd_material_w
	and	a.qt_dose	= qt_dose_w
	and	a.cd_unidade_medida_dose = cd_unid_med_dose_w
	and	a.cd_intervalo	= cd_intervalo_w
	and	a.ie_via_aplicacao = ie_via_w
	and	nvl(a.ie_se_necessario,'N') = 'N'
	and	nvl(a.ie_acm,'N') = 'N'
	and	ie_opcao_p	= 'M'
	and	Obter_se_horario_liberado(h.dt_lib_horario, h.dt_horario) = 'S'
	union all
	select	distinct h.dt_horario,
		substr(replace(obter_status_hor_prescr(h.nr_sequencia,'R'),'N',''),1,1)
	from	prescr_recomendacao a,
		prescr_rec_hor h,
		prescr_medica m
	where	h.nr_prescricao = m.nr_prescricao
	and	a.nr_prescricao = m.nr_prescricao
	and	h.dt_horario between dt_inicial_p and dt_final_p
	and	m.nr_atendimento = nr_atendimento_p
	and	a.cd_intervalo	= cd_intervalo_w	
	and	a.cd_recomendacao = cd_material_w
	and	ie_opcao_p	= 'R'
	and	Obter_se_horario_liberado(h.dt_lib_horario, h.dt_horario) = 'S'
	union all
	select	--distinct to_date(to_char(m.dt_prescricao,'dd/mm/yyyy') || ' ' || s.hr_prim_horario || ':00','dd/mm/yyyy hh24:mi:ss'),
		distinct h.dt_horario,
		substr(obter_status_solucao_prescr(2, a.nr_prescricao, a.nr_sequencia),1,5)
	from	prescr_material a,
		prescr_mat_hor h,
		prescr_medica m
	where	h.nr_prescricao = m.nr_prescricao
	and	a.nr_prescricao = m.nr_prescricao
	and	a.cd_material 	= h.cd_material
	and	h.dt_horario between dt_inicial_p and dt_final_p
	and	m.nr_atendimento = nr_atendimento_p
	and	h.cd_material	= cd_material_w
	and	a.qt_dose	= qt_dose_w
	and	a.cd_unidade_medida_dose = cd_unid_med_dose_w
	and	a.cd_intervalo	= cd_intervalo_w
	and	a.ie_via_aplicacao = ie_via_w
	and	ie_opcao_p	= 'SNE'
	and	Obter_se_horario_liberado(h.dt_lib_horario, h.dt_horario) = 'S'
	order by
		1;


begin
open c01;
loop
fetch c01 into	
	dt_horario_w,
	ie_situacao_w;
exit when c01%notfound;
	begin
	
	if	(trunc(dt_horario_ant_w) <> trunc(dt_horario_w)) then
		
		dt_horarios_w := dt_horarios_w || chr(13) || chr(10) || replace(to_char(dt_horario_w,'dd/mm hh24:mi'),':00','') || ie_situacao_w ;
	else
		dt_horarios_w := dt_horarios_w || ' ' || replace(to_char(dt_horario_w,'hh24:mi'),':00','') || ie_situacao_w;		
	end if;
	
	dt_horario_ant_w := dt_horario_w;
	
	end;
end loop;
close c01;

return	substr(dt_horarios_w,4,length(dt_horarios_w) - 3);

end obter_inf_hor_prescr;
/