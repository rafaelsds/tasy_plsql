create or replace
function consis_estab_usua_mont_consig	(ie_opcao_p		varchar2,
					 cd_estabelecimento_p	number)
					 return varchar2 is
					 

					 
ie_restr_estab_montagem_w	varchar2(1);
ie_restr_estab_consig_w		varchar(1);
ds_retorno_w			varchar(1) := 'S';

begin

obter_param_usuario(871,755 , wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_restr_estab_montagem_w);
obter_param_usuario(871,756 , wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_restr_estab_consig_w);



if ((ie_opcao_p = 'M') and
    (ie_restr_estab_montagem_w = 'S')) then
    
	SELECT 	NVL(MAX('S'),'N')
	into	ds_retorno_w
	from    usuario_estabelecimento_v a
	where   a.cd_estabelecimento = cd_estabelecimento_p
	and	a.nm_usuario_param = wheb_usuario_pck.get_nm_usuario;
	
elsif ((ie_opcao_p = 'PC') and
      (ie_restr_estab_consig_w = 'S')) then
      
       SELECT 	NVL(MAX('S'),'N')
       into	ds_retorno_w
       from    usuario_estabelecimento_v a
       where   a.cd_estabelecimento = cd_estabelecimento_p
       and	a.nm_usuario_param = wheb_usuario_pck.get_nm_usuario;
       
end if;

return	ds_retorno_w;

end consis_estab_usua_mont_consig;
/