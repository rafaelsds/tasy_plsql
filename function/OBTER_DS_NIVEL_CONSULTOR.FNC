create or replace
function obter_ds_nivel_consultor(
		nr_sequencia_p	number)
		return varchar2 is
		
ds_nivel_w	varchar2(80);
begin
if	(nr_sequencia_p is not null) then
	begin
	select 	Obter_desc_expressao(cd_exp_nivel, ds_nivel) ds_nivel
	into	ds_nivel_w
	from	proj_nivel_consultor
	where	nr_sequencia	= nr_sequencia_p;
	end;
end if;
return	ds_nivel_w;
end obter_ds_nivel_consultor;
/