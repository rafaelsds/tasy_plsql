create or replace
function obter_se_componente_substituto(	nr_lote_producao_p	number,
						cd_material_p		number )
						return varchar2 is

cd_material_w		varchar2(6);
nr_ficha_tecnica_w	number(10,0);
nr_seq_componente_w	number(10,0);
ie_possui_substituto_w	varchar2(1) := 'N';
						
begin

select	max(cd_material)
into	cd_material_w
from	lote_producao
where	nr_lote_producao = nr_lote_producao_p;

if	(cd_material_w is not null) then
	
	select	max(nr_ficha_tecnica)
	into	nr_ficha_tecnica_w
	from	ficha_tecnica
	where	cd_material = cd_material_w;
		
	if	(nr_ficha_tecnica_w is not null) then
		select	max(nr_seq_componente)
		into	nr_seq_componente_w
		from	ficha_tecnica_componente
		where	nr_ficha_tecnica	=	nr_ficha_tecnica_w
		and	cd_material		=	cd_material_p;
	
		if	(nr_seq_componente_w is not null) then
			select	nvl(max('S'),'N')
			into	ie_possui_substituto_w
			from	ficha_tecnica_comp_subs
			where	nr_ficha_tecnica	=	nr_ficha_tecnica_w
			and	nr_seq_componente	=	nr_seq_componente_w;
		end if;
	end if;
end if;

return	ie_possui_substituto_w;

end obter_se_componente_substituto;
/