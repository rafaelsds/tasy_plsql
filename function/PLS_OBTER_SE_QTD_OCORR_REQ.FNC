create or replace
function pls_obter_se_qtd_ocorr_req
			(	nr_seq_requisicao_p	Number,
				qt_tipo_quantidade_p	Number,
				ie_tipo_qtde_p		Varchar2,
				qt_liberada_p		Number)
				return Varchar2 is

/*
 IE_TIPO_QTDE_P - Dominio 3540
	D - Dia
	M - Mes
	A - Ano	
	G - Guia
*/

ie_retorno_w			Varchar2(1)	:= 'S';
nr_seq_segurado_w		Number(10);
nr_seq_prestador_w		Number(10);
qt_liberacao_w			Number(9,3);
dt_liberacao_w			Date;
dt_liberacao_ww			date;				
				
begin

select	dt_requisicao,
	nr_seq_segurado,
	nr_seq_prestador
into	dt_liberacao_w,
	nr_seq_segurado_w,
	nr_seq_prestador_w
from	pls_requisicao
where	nr_sequencia	= nr_seq_requisicao_p;

if	(ie_tipo_qtde_p	= 'D') then   	
	dt_liberacao_ww	:= trunc(dt_liberacao_w - (qt_tipo_quantidade_p - 1));		
elsif	(ie_tipo_qtde_p	= 'M') then
	dt_liberacao_ww	:= (add_months(dt_liberacao_w, -qt_tipo_quantidade_p) + 1);
elsif	(ie_tipo_qtde_p	= 'A') then
	dt_liberacao_ww	:= (add_months(dt_liberacao_w, -qt_tipo_quantidade_p * 12) + 1); /* Vezes 12 meses ao ano */
elsif	(ie_tipo_qtde_p = 'G') then
	dt_liberacao_ww := sysdate;
end if;

select 	count(*)
into	qt_liberacao_w
from	pls_requisicao		
where	ie_estagio		= 2
and	nr_seq_segurado		= nr_seq_segurado_w
and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_requisicao) 	between dt_liberacao_ww and ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_liberacao_w);

if	(qt_liberacao_w	>= qt_liberada_p - 1) then
	ie_retorno_w	:= 'N';
end if;

return	ie_retorno_w;

end pls_obter_se_qtd_ocorr_req;
/