create or replace
function obter_nome_pf_reserva_gv(
	  	cd_unidade_basica_p  varchar2,
		cd_unidade_compl_p  varchar2,
	  	cd_setor_atendimento_p  number)
        return varchar2 is


nr_seq_vaga_w   	number(10);
cd_pessoa_reserva_w  	varchar2(20);
begin


select  nvl(min(nr_sequencia),0)
into	nr_seq_vaga_w
from	fila_espera_reserva
where	ie_status  = 'R'
and	cd_unidade_basica = cd_unidade_basica_p
and	cd_unidade_compl = cd_unidade_compl_p
and	cd_setor_atendimento = cd_setor_atendimento_p;

if	(nr_seq_vaga_w > 0) then

	select 	max(cd_pessoa_reserva)
	into	cd_pessoa_reserva_w
	from	fila_espera_reserva
	where	nr_sequencia = nr_seq_vaga_w;
else
	cd_pessoa_reserva_w := 'X';
end if;

return cd_pessoa_reserva_w;

end obter_nome_pf_reserva_gv;
/