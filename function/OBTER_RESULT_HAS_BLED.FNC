create or replace
function obter_result_has_bled(qt_escore_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(80);			
begin

if	(qt_escore_p	= 0) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309303, 'PR=' || '0,9'); -- Risco de sangramento maior de 0,9%
elsif	(qt_escore_p	= 1) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309303, 'PR=' || '3,4'); -- Risco de sangramento maior de 3,4%
elsif	(qt_escore_p	= 2) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309303, 'PR=' || '4,1'); -- Risco de sangramento maior de 4,1%	
elsif	(qt_escore_p	= 3) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309303, 'PR=' || '5,8'); -- Risco de sangramento maior de 5,8%	
elsif	(qt_escore_p	= 4) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309303, 'PR=' || '8,9'); -- Risco de sangramento maior de 8,9%
elsif	(qt_escore_p	= 5) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309303, 'PR=' || '9,1'); -- Risco de sangramento maior de 9,1%
elsif	(qt_escore_p	> 5) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309400); -- Escores superiores a 5 s�o muito raros para determinar o risco
end if;

return	ds_retorno_w;

end obter_result_has_bled;
/