create or replace
function Obter_se_loc_tnm_regra(nr_seq_regra_p		number,
				nr_seq_localizacao_p	number)
				return varchar2 is
				
ie_retorno_w	varchar2(1);
begin

select 	decode(count(*),0,'S','N')
into	ie_retorno_w
from	can_tnm_regra_loc
where	nr_seq_regra = nr_Seq_regra_p;

if	(ie_retorno_w	= 'N') then

	select 	decode(count(*),0,'N','S')
	into	ie_retorno_w
	from	can_tnm_regra_loc
	where	nr_seq_regra = nr_Seq_regra_p
	and	nr_seq_loc = nr_seq_localizacao_p;

end if;


return	ie_retorno_w;

end Obter_se_loc_tnm_regra;
/