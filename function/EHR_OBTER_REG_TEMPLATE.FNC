create or replace
function ehr_obter_reg_template(	nr_sequencia_p	number,
					ie_opcao_p	varchar2)
 		    	return number is

nr_seq_template_w	number(10);
nr_seq_reg_template_w	number(10);			
			
begin


select	max(nr_sequencia),
	max(nr_seq_template)
into	nr_seq_reg_template_w,
	nr_seq_template_w
from	ehr_reg_template
where	NR_SEQ_REG	= nr_sequencia_p
and	NR_SEQ_TEMPLATE	is not null;

if	(ie_opcao_p	 = 'R') then
	return nr_seq_reg_template_w;
elsif	(ie_opcao_p	= 'T') then	
	return nr_seq_template_w;
end if;



return	null;

end ehr_obter_reg_template;
/