create or replace
function Tx_obter_metodo_lab(	cd_estabelecimento_p	number,
				nr_seq_laboratorio_p	number,			
				ie_opcao_p		varchar2)
				return number is

nr_seq_metodo_w	number(10) 	:= 0;
nr_seq_exame_w	number(10);

begin

if (ie_opcao_p = 'HLA') then

	select 	max(nr_seq_exame_hla)
	into	nr_seq_exame_w
	from	tx_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;


elsif (ie_opcao_p = 'PAINEL') then

	select 	max(nr_seq_exame_painel)
	into	nr_seq_exame_w
	from	tx_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;


elsif (ie_opcao_p = 'CROSS') then

	select 	max(nr_seq_exame_crossmatch)
	into	nr_seq_exame_w
	from	tx_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;

elsif (ie_opcao_p = 'MELD') then

	select 	max(nr_seq_exame_meld)
	into	nr_seq_exame_w
	from	tx_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;

end if;

if (nr_seq_exame_w is not null) then

	select 	max(nr_seq_metodo)
	into	nr_seq_metodo_w
	from  	tx_lab_metodo
	where	nr_seq_laboratorio = nr_seq_laboratorio_p
	and	nr_seq_exame = nr_seq_exame_w;


end if;

return nr_seq_metodo_w;

end Tx_obter_metodo_lab;
/
