create or replace
function pls_obter_valor_censo
			(	ie_dimensao_p		Varchar2,
				ie_dimensao_secundaria_p Varchar2,
				dt_mes_referencia_p	Date,
				ie_tipo_segurado_p	Varchar2,
				ie_opcao_p		Varchar2,
				nr_seq_referencia_p	Varchar2,
				ie_benef_sca_p		varchar2)
				return number is
/* ie_ocpao_p
	D - Detalhado (por dimens�o)
	DI - Detalhado (pela sub-dimens�o) 
	T - Total
*/
vl_retorno_w			number(30);

begin
if	(ie_benef_sca_p = 'S') then
	if	(ie_opcao_p = 'D') then
		select	count(1)
		into	vl_retorno_w 
		from	w_pls_censo_geral a
		where	dt_mes_referencia	= dt_mes_referencia_p
		and	ie_dimensao		= ie_dimensao_p
		and	ie_tipo_segurado	= nvl(ie_tipo_segurado_p,ie_tipo_segurado)
		and	exists	(select count(1) from pls_sca_vinculo h where h.nr_seq_segurado = a.nr_seq_segurado );
	elsif	(ie_opcao_p = 'DI') then
		select	count(1)
		into	vl_retorno_w
		from	w_pls_censo_geral a
		where	dt_mes_referencia	= dt_mes_referencia_p
		and	ie_dimensao		= ie_dimensao_p
		and	ie_dimensao_secundaria	= ie_dimensao_secundaria_p
		and	ie_tipo_segurado	= nvl(ie_tipo_segurado_p,ie_tipo_segurado)
		and	nr_seq_referencia	= nr_seq_referencia_p
		and	exists	(select count(1) from pls_sca_vinculo h where h.nr_seq_segurado = a.nr_seq_segurado );
	elsif	(ie_opcao_p = 'T') then
		select	count(1)
		into	vl_retorno_w
		from	w_pls_censo_geral a
		where	dt_mes_referencia	= dt_mes_referencia_p
		and	ie_tipo_segurado	= nvl(ie_tipo_segurado_p,ie_tipo_segurado)
		and	ie_dimensao is not null
		and	exists	(select count(1) from pls_sca_vinculo h where h.nr_seq_segurado = a.nr_seq_segurado );
	end if;

else
	if	(ie_opcao_p = 'D') then
		select	count(1)
		into	vl_retorno_w
		from	w_pls_censo_geral
		where	dt_mes_referencia	= dt_mes_referencia_p
		and	ie_dimensao		= ie_dimensao_p
		and	ie_tipo_segurado	= nvl(ie_tipo_segurado_p,ie_tipo_segurado);
	elsif	(ie_opcao_p = 'DI') then
		select	count(1)
		into	vl_retorno_w
		from	w_pls_censo_geral
		where	dt_mes_referencia	= dt_mes_referencia_p
		and	ie_dimensao		= ie_dimensao_p
		and	ie_dimensao_secundaria	= ie_dimensao_secundaria_p
		and	ie_tipo_segurado	= nvl(ie_tipo_segurado_p,ie_tipo_segurado)
		and	nr_seq_referencia	= nr_seq_referencia_p;
	elsif	(ie_opcao_p = 'T') then
		select	count(1)
		into	vl_retorno_w
		from	w_pls_censo_geral
		where	dt_mes_referencia	= dt_mes_referencia_p
		and	ie_tipo_segurado	= nvl(ie_tipo_segurado_p,ie_tipo_segurado)
		and	ie_dimensao is not null;
	end if;
end if;

	
return	vl_retorno_w;

end pls_obter_valor_censo;
/
