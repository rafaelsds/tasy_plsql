CREATE OR REPLACE
FUNCTION Obter_Moda_Relatorio
			(dt_inicial_p	date,
			 dt_final_p	date,
			 ie_opcao_p	varchar)
			 return varchar is

/*	
ie_opcao_p
 MD - Matrícula / Diagnóstico
 DT - Diagnóstico / Tratamento
 MT - Matrícula / Tratamento
*/

qt_contador_w	number(10);
vl_result_w	number(10);
ds_retorno_w	varchar2(80);

cursor c01 is
	select	trunc((dt_preench_ficha) - (dt_diagnostico))vl_moda
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_p
	having	count(trunc((dt_preench_ficha) - (dt_diagnostico))) = qt_contador_w
	group by	trunc((dt_preench_ficha) - (dt_diagnostico));


cursor c02 is
	select	trunc((dt_diagnostico) - (dt_inicio_trat))vl_moda
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_p
	having	count(trunc((dt_diagnostico) - (dt_inicio_trat))) = qt_contador_w
	group by	trunc((dt_diagnostico) - (dt_inicio_trat));


cursor c03 is
	select	trunc((dt_preench_ficha) - (dt_inicio_trat))vl_moda
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_p
	having	count(trunc((dt_preench_ficha) - (dt_inicio_trat))) = qt_contador_w
	group by	trunc((dt_preench_ficha) - (dt_inicio_trat));


BEGIN

if	(ie_opcao_p = 'MD') then
	
	select	max(count(trunc((dt_preench_ficha) - (dt_diagnostico)))) qt_contador
	into	qt_contador_w
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_p
	group by	trunc((dt_preench_ficha) - (dt_diagnostico));

	open c01;
	loop
	fetch c01 into vl_result_W;
		exit when c01%notfound;
		ds_retorno_w := ds_retorno_w || vl_result_w || '; ';

	end loop;

	close c01;


elsif	(ie_opcao_p = 'DT') then

	select	max(count(trunc((dt_diagnostico) - (dt_inicio_trat)))) qt_contador
	into	qt_contador_w
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_p
	group by	trunc((dt_diagnostico) - (dt_inicio_trat));

	open c02;
	loop
	fetch c02 into vl_result_W;
		exit when c02%notfound;
		ds_retorno_w := ds_retorno_w || vl_result_w || '; ';

	end loop;

	close c02;


elsif	(ie_opcao_p = 'MT') then

	select	max(count(trunc((dt_preench_ficha) - (dt_inicio_trat)))) qt_contador
	into	qt_contador_w
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_p
	group by	trunc((dt_preench_ficha) - (dt_inicio_trat));

	open c03;
	loop
	fetch c03 into vl_result_W;
		exit when c03%notfound;
		ds_retorno_w := ds_retorno_w || vl_result_w || '; ';

	end loop;

	close c03;	

end if;

RETURN ds_retorno_w;	

END Obter_Moda_Relatorio;
/