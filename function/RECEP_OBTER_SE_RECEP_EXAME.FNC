create or replace
function recep_obter_se_recep_exame(	cd_pessoa_fisica_p	number,
									nr_seq_age_proc_p	number,
									nr_seq_agenda_p		number,
									nr_seq_exame_p		number)
									return varchar is
								
ie_recepcionado_w		varchar(1) := 'N';
ie_presc_procedimento_w	varchar(1) := 'N';
cd_procedimento_w 		number(15,0);
nr_seq_ficha_w			number(10,0);
nr_atendimento_w		number(10,0);
ie_origem_proced_w		number(10,0);
nr_seq_proc_interno_w	number(6,0);

begin

if	(cd_pessoa_fisica_p is not null) then
	begin
	
	ie_recepcionado_w := 'N';
	
	select 	max(nr_sequencia), 
			max(nr_atendimento)
	into 	nr_seq_ficha_w, 
			nr_atendimento_w
	from 	recep_ficha_pre
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p
	and 	ie_status <> 1
	and 	TRUNC(dt_pre_recepcao) = TRUNC(SYSDATE);
	
	if	(nr_seq_ficha_w is not null)
		and (nr_atendimento_w is not null) then
		begin
		
		select 	decode(count(*), 0, 'N', 'S')
		into 	ie_presc_procedimento_w
		from 	prescr_medica a,
				prescr_procedimento b
		where 	a.nr_atendimento = nr_atendimento_w
		and   	b.nr_prescricao = a.nr_prescricao
		and 	a.dt_suspensao is null;

		
		if	(nr_seq_agenda_p is not null)
			and (ie_presc_procedimento_w = 'S') then
			
			begin
			if	(nr_seq_age_proc_p is null) then
				begin
						select 	max(cd_procedimento), 
								max(ie_origem_proced),
								max(nr_seq_proc_interno)
						into	cd_procedimento_w,
								ie_origem_proced_w,
								nr_seq_proc_interno_w
						from 	agenda_paciente
						where 	nr_sequencia = nr_seq_agenda_p;
							
				end;
				else
				begin
						select 	max(cd_procedimento), 
								max(ie_origem_proced),
								max(nr_seq_proc_interno)
						into	cd_procedimento_w, 
								ie_origem_proced_w,
								nr_seq_proc_interno_w
						from 	agenda_paciente_proc
						where 	nr_sequencia = nr_seq_agenda_p
						and		nr_seq_agenda = nr_seq_age_proc_p;
				end;			
			end if;
			
			if (cd_procedimento_w is not null) then
				select 	decode(count(*), 0, 'N', 'S')	
				into 	ie_recepcionado_w
				from 	prescr_medica b,
						prescr_procedimento c
				where 	b.nr_prescricao = c.nr_prescricao
				and 	c.cd_procedimento = cd_procedimento_w
				and 	c.ie_origem_proced = ie_origem_proced_w
				and		c.nr_seq_proc_interno = nr_seq_proc_interno_w
				and 	b.nr_atendimento = nr_atendimento_w
				and		c.nr_seq_agenda = nr_seq_agenda_p
				and		nvl(c.ie_suspenso,'N') = 'N';
			else
				select 	decode(count(*), 0, 'N', 'S')	
				into 	ie_recepcionado_w
				from 	prescr_medica b,
						prescr_procedimento c
				where 	b.nr_prescricao = c.nr_prescricao
				and 	b.nr_atendimento = nr_atendimento_w
				and		c.nr_seq_exame = nr_seq_exame_p
				and		nvl(c.ie_suspenso,'N') = 'N';
			end if;
			end;
			
		else
			begin
					
			select 	decode(count(*), 0, 'N', 'S')	
			into 	ie_recepcionado_w
			from 	prescr_medica b,
					prescr_procedimento c
			where 	b.nr_prescricao = c.nr_prescricao
			and 	b.nr_atendimento = nr_atendimento_w
			and		c.nr_seq_exame = nr_seq_exame_p
			and		nvl(c.ie_suspenso,'N') = 'N';
		end;
		end if;
		end;
	end if;
	end;
end if;
return	ie_recepcionado_w;
	
end recep_obter_se_recep_exame;
/
