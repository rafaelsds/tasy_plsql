create or replace
function ageint_obter_se_apres_aval(nr_seq_tipo_aval_p		number,
				   nr_seq_ageint_p		number,
				   nr_seq_proc_interno_p	number)
 		    	return varchar2 is
			
ie_retorno_w		varchar2(1);
ie_exige_aval_w		varchar2(1);
cd_procedimento_w 	number(15,0);
ie_origem_proced_w	number(10,0);
cd_convenio_w		number(5,0);	
cd_categoria_w		varchar2(10);
cd_estabelecimento_w	number(4,0);
cd_plano_w		varchar2(10);

begin

select	cd_estabelecimento,
	cd_convenio,
	cd_categoria,
	cd_plano
into	cd_estabelecimento_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w
from	agenda_integrada
where	nr_sequencia = nr_seq_ageint_p;

obter_proc_tab_interno_conv(
				nr_seq_proc_interno_p,
				cd_estabelecimento_w,
				cd_convenio_w,
				cd_categoria_w,
				cd_plano_w,
				null,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				sysdate,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);
				
				
select	decode(count(*),'0','N','S')
into	ie_retorno_w
from	med_tipo_aval_proc
where	nr_seq_tipo_aval = nr_seq_tipo_aval_p
and	ie_obrigatorio = 'S'
and	ie_origem_proced = ie_origem_proced_w and
	(cd_procedimento = cd_procedimento_w) or
	(nr_seq_proc_interno = nr_seq_proc_interno_p) or
	(cd_area_procedimento = obter_area_procedimento(cd_procedimento_w, ie_origem_proced_w)) or
	(cd_especialidade = obter_especialidade_proced(cd_procedimento_w, ie_origem_proced_w)) or
	(cd_grupo_proc = obter_grupo_procedimento(cd_procedimento_w, ie_origem_proced_w,'C'));
	
return	ie_retorno_w;

end ageint_obter_se_apres_aval;
/
