create or replace
function rop_obter_qtde_lavagens(	nr_seq_roupa_p			number)
return number is

qt_lavagens_w			number(10);

begin

select	count(distinct a.nr_sequencia)
into	qt_lavagens_w
from	rop_lote_movto a,
	rop_movto_roupa b
where	a.nr_sequencia = b.nr_seq_lote
and	b.nr_seq_roupa = nr_seq_roupa_p
and	a.dt_liberacao is not null
and	(((a.nr_seq_lavanderia is null) 	and (rop_obter_dados_operacao(a.nr_seq_operacao,'T') = 'EL')) or
	 ((a.nr_seq_lavanderia is not null) 	and (obter_dados_lavanderia(a.nr_seq_lavanderia,'EV') = 'S')));
	
return	qt_lavagens_w;

end rop_obter_qtde_lavagens;
/
