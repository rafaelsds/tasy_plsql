create or replace
function obter_se_datas_entre_periodo(	dt_periodo_inicial_p	date,
					dt_periodo_final_p	date,
					dt_inicial_p		date,
					dt_final_p		date) 
							return varchar2 is
ie_retorno_w	varchar2(1) := 'N';

begin

if	(dt_inicial_p	>= dt_periodo_inicial_p) and
	(dt_final_p	<= dt_periodo_final_p) then

	ie_retorno_w	:= 'S';

elsif	(dt_inicial_p	<= dt_periodo_inicial_p) and
	(dt_final_p 	<= dt_periodo_final_p and dt_final_p >= dt_periodo_inicial_p) then
	
	ie_retorno_w	:= 'S';

elsif	(dt_inicial_p	>= dt_periodo_inicial_p and dt_inicial_p <= dt_periodo_final_p) and
	(dt_final_p 	>= dt_periodo_final_p) then

	ie_retorno_w	:= 'S';

end if;

return ie_retorno_w;

end;
/