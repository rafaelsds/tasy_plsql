create or replace
function mprev_obter_desc_possib_sist(nr_seq_possibilidade_p	number)
					return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obt�m a descri��o da possibilidade do sistema de informa��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(255);

begin

if	(nr_seq_possibilidade_p is not null) then
	select	a.ds_possibilidade
	into	ds_retorno_w
	from	mprev_possibilidade_sist a
	where	a.nr_sequencia	= nr_seq_possibilidade_p;
end if;

return	ds_retorno_w;

end mprev_obter_desc_possib_sist;
/