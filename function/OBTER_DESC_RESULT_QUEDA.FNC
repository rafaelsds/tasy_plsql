create or replace
function Obter_desc_result_queda( nr_sequencia_p 	number,
				ie_opcao_p		varchar)
 		    	return varchar2 is
/*
Condição		'CQ'
Condição ambiental	'CA'
Consequências	'CS'
Conduta		'CD'
Local		'L'
Fator de risco	'F'
*/			
ds_retorno_w	varchar2(255);
	
begin

If	(ie_opcao_p = 'CQ') then

	select  substr(ds_condicao,1,255)
	into 	ds_retorno_w
	from   	queda_condicao
	where  	ie_situacao = 'A'
	and    	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'CA') then

	select  substr(ds_condicao,1,255)
	into 	ds_retorno_w
	from	queda_cond_ambiente
	where	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'CS') then

	select  substr(ds_consequencia,1,255)
	into 	ds_retorno_w
	from	queda_consequencia
	where	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'CD') then

	select  substr(ds_conduta,1,255)
	into 	ds_retorno_w
	from	queda_conduta
	where	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'L') then

	select  substr(ds_local,1,255)
	into 	ds_retorno_w
	from	queda_local
	where	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'F') then

	select	max(substr(obter_descricao_item_EIF(nr_seq_item),1,255))
	into 	ds_retorno_w
	from	escala_eif_item
	where	nr_seq_item = nr_sequencia_p;	

end if;
return	ds_retorno_w;

end Obter_desc_result_queda;
/
