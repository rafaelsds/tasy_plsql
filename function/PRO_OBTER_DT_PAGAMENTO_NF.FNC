create or replace
function Pro_Obter_dt_pagamento_NF(	nr_sequencia_p	number)
			return date is

dt_retorno_w	date;

begin
dt_retorno_w	:= null;

select	n.databaixa
into	dt_retorno_w
from	rm.fcfo f,
	rm.flan n
where	f.codcfo		= n.codcfo
and	n.pagrec		= 2
and	statuslan		= 1
and	n.nfoudup	<> 2
and	n.codtb1flx in (161,162,163,164,165,168,169)
and	n.codtdo like 'NF%'
and	n.idlan		= nr_sequencia_p;

return	dt_retorno_w;

end Pro_Obter_dt_pagamento_NF;
/
