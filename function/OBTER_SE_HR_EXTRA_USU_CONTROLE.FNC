create or replace
function obter_se_hr_extra_usu_controle(
				dt_entrada_p			date,
				qt_min_total_p			number,
				qt_min_intervalo_p		number,
				cd_estabelecimento_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(10) := 'N';
ie_feriado_w	varchar2(01) := 'N';
ie_dia_semana_w	varchar2(01);
ie_dia_util_w	varchar2(01) := 'N';

begin

if	(pkg_date_utils.get_WeekDay(dt_entrada_p) in ('1','7')) then
	ds_retorno_w := 'S';
end if;

if	(ds_retorno_w = 'N') then
	begin	
	begin
	select	'S'
	into	ds_retorno_w
	from 	feriado 
	where 	dt_feriado = dt_entrada_p
	and		cd_estabelecimento	= cd_estabelecimento_p;
	exception
	when others then
		ds_retorno_w := 'N';
	end;
	
	if	(ds_retorno_w = 'N') then
		begin
		if	((qt_min_total_p - qt_min_intervalo_p) > 525) then
			begin
			ds_retorno_w := 'S';
			end;
		end if;
		end;
	end if;
	end;
end if;

return ds_retorno_w;

end obter_se_hr_extra_usu_controle;
/