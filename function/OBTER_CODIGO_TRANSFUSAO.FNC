create or replace
function Obter_codigo_transfusao(nr_seq_reserva_p	number)
 		    	return number is
			
nr_transfusao_w		number(15,0);			

begin

if	(nr_seq_reserva_p is not null) then
	select	max(nr_sequencia)
	into	nr_transfusao_w
	from	san_transfusao
	where	nr_seq_reserva = nr_seq_reserva_p;
end if;

return	nr_transfusao_w;

end Obter_codigo_transfusao;
/