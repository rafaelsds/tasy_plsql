create or replace
function obter_se_novos_itens_GPT(nr_atendimento_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p varchar2,
			ie_lib_farm_p  varchar2,
			cd_pessoa_fisica_p      pessoa_fisica.cd_pessoa_fisica%type) return varchar2 is
			
/*
S - SIM
N - NAO
I - Contem intervencao farmacia
*/

ie_tipo_usuario_w		varchar(1);
ds_retorno 				varchar(1) := 'N';
dt_inicio_analise_w		gpt_hist_analise_plano.dt_inicio_analise%type;
dt_fim_analise_w		gpt_hist_analise_plano.dt_fim_analise%type;
dt_prescr				date;
ie_prescr_farm_w		prescr_medica.ie_prescr_farm%type;
nr_prescricao_w			prescr_medica.nr_prescricao%type;

Cursor C01 is
	select	nvl(ie_prescr_farm,'N'),
			nr_prescricao
	from	prescr_medica
	where	((nr_atendimento = nr_atendimento_p) or (cd_pessoa_fisica = cd_pessoa_fisica_p and nr_atendimento is null))
	and		(ie_lib_farm_p = 'N' or nvl(ie_lib_farm,'N') = 'S')
	and		dt_liberacao is not null
	and		dt_liberacao_farmacia is null
	and		dt_inicio_analise_farm is null
	and		nm_usuario_analise_farm is null
	and		(dt_prescricao >= dt_inicio_analise_w or 
			(ie_tipo_usuario_w = 'F' and dt_liberacao >= dt_inicio_analise_w and dt_fim_analise_w is null));

begin
wheb_assist_pck.set_informacoes_usuario(cd_estabelecimento_p, obter_perfil_ativo, nm_usuario_p);
ie_tipo_usuario_w := wheb_assist_pck.obterparametrofuncao(252,1);

if (ie_tipo_usuario_w = 'E') then
	
	dt_inicio_analise_w	:= obter_dt_analise_gpt(nr_atendimento_p, null, null, 'E', 'DTI', ie_lib_farm_p, cd_pessoa_fisica_p);
	
	select	nvl(max('S'),'N')
	into 	ds_retorno
	from	prescr_medica a,
			prescr_medica_compl b
	where 	a.nr_prescricao = b.nr_prescricao
	and		((a.nr_atendimento = nr_atendimento_p) or (a.cd_pessoa_fisica = cd_pessoa_fisica_p and a.nr_atendimento is null))
	--and 	dt_revisao 	is null
	and		b.dt_inicio_analise_enf is null
	and 	a.dt_liberacao 	is null
	--and 	nm_usuario_revisao 	is null
	and		b.nm_usuario_analise_enf is null
	and 	a.cd_funcao_origem 	not in(900,924,950)
	and		a.dt_liberacao_medico is not null
	and 	a.dt_prescricao >= dt_inicio_analise_w;
	
elsif (ie_tipo_usuario_w = 'F') then

	dt_inicio_analise_w	:= obter_dt_analise_gpt(nr_atendimento_p, null, null, 'F', 'DTI', ie_lib_farm_p, cd_pessoa_fisica_p);
	dt_fim_analise_w := obter_dt_analise_gpt(nr_atendimento_p, null, null, 'F', 'DTF', ie_lib_farm_p, cd_pessoa_fisica_p);

	open c01;
	loop
	fetch c01 into	
		ie_prescr_farm_w,
		nr_prescricao_w;
	exit when c01%notfound;
		begin
		if (ie_prescr_farm_w = 'N') then
			return 'S';
		else 
			ds_retorno := 'I';
		end if;
		end;
	end loop;
	close c01;
end if;

return ds_retorno;

end obter_se_novos_itens_GPT;
/
