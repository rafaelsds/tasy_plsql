create or replace function PFCS_GET_RECOMMENDATION_TIME(
    nr_sequencia_p in number,
    ie_option_p    in varchar2 )
  return number as
  
dt_recommendation_time cpoe_recomendacao.dt_liberacao%type;
  
begin

  if (ie_option_p = 'R') then
    -- R status maps to duration from when request has been raised
    select a.dt_liberacao 
    into dt_recommendation_time
    from cpoe_recomendacao a
    where a.nr_sequencia   = nr_sequencia_p; 
 elsif (ie_option_p = 'S') then
	-- S status maps to duration from when recommendation has been started
    select dt_start_time 
    into dt_recommendation_time 
    from (
      select c.dt_horario dt_start_time
      from cpoe_recomendacao a, 
         prescr_recomendacao b, 
         prescr_rec_hor c
      where a.nr_sequencia  = nr_sequencia_p 
      and   a.nr_sequencia  = b.nr_seq_rec_cpoe
      and   b.nr_sequencia  = c.nr_seq_recomendacao
      and   b.nr_prescricao = c.nr_prescricao
      order by c.nr_sequencia desc )
    where rownum = 1;
 end if;

  if (dt_recommendation_time is not null) then
    return round((sysdate - dt_recommendation_time) * 24 * 60);
  else
    return 0;
  end if;

end PFCS_GET_RECOMMENDATION_TIME;
/