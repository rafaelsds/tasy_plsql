create or replace
function OBTER_SE_USUARIO_EVOLUCAO(	
			nr_sequencia_p		number,
			ie_evolucao_p		varchar2)
 		    	return varchar2 is
ds_retorno_w 	varchar2(1) := 'S';
ie_tipo_w		varchar2(3);
begin

select  nvl(MAX(d.IE_PROFISSIONAL),'N')
into    ie_tipo_w
FROM 	PE_PRESCR_PROC d,
		PE_PRESCR_PROC_HOR e
WHERE  	e.nr_seq_pe_proc = d.nr_sequencia
and		e.nr_sequencia = nr_sequencia_p;

if( ie_tipo_w <> 'N' and
	ie_tipo_w <> ie_evolucao_p) then
		ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end OBTER_SE_USUARIO_EVOLUCAO;
/