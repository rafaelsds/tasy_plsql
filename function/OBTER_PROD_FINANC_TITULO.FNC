create or replace
function OBTER_PROD_FINANC_TITULO
				(nr_titulo_p	in number,
				ie_opcao_p	in varchar2)
				return varchar2 is
/*
ie_opcao_p
C	C�digo
D	Descri��o
*/

ds_retorno_w		varchar2(255);	
nr_seq_produto_w	number(10);
	
begin

ds_retorno_w := '';

select	max(nr_seq_produto)
into	nr_seq_produto_w
from	titulo_receber_classif
where	nr_titulo	= nr_titulo_p;

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= nr_seq_produto_w;
elsif	(ie_opcao_p = 'D') then
	ds_retorno_w	:= OBTER_DESC_PRODUTO_FINANCEIRO(nr_seq_produto_w);
end if;

return ds_retorno_w;

end;
/