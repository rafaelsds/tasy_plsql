create or replace
function Nut_Obter_Se_Conduta_Suspensa(	nr_seq_serv_dia_p	Number)
				return Varchar2 is

ds_retorno_w	Varchar2(1);
ds_observacao_w	Varchar2(4000);
				
begin

if (nr_seq_serv_dia_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	nut_atend_serv_dieta b,
		nut_atend_serv_dia_dieta c
	where	b.nr_seq_dieta 	= c.nr_sequencia
	and	b.nr_seq_servico = nr_seq_serv_dia_p
	and	c.dt_suspensao is not null
	and	c.ie_tipo_nutricao <> 'O'
	and not exists (select	1
			from	nut_atend_serv_dieta d,
				nut_atend_serv_dia_dieta e
			where	d.nr_seq_dieta 	= e.nr_sequencia
			and	d.nr_seq_servico = nr_seq_serv_dia_p
			and	e.dt_suspensao is null);
			
	/*select	max(ds_observacao)
	into	ds_observacao_w
	from	nut_atend_serv_dia
	where	nr_sequencia = nr_seq_serv_dia_p;
		
	if (ds_observacao_w is not null) then
		ds_retorno_w := 'N';
	end if;*/
		
end if;

return	ds_retorno_w;

end Nut_Obter_Se_Conduta_Suspensa;
/