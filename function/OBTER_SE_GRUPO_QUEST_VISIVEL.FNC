create or replace
function obter_se_grupo_quest_visivel(nr_seq_grupo_p	number)
					return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_visivel_w	varchar2(1);

begin

select	decode(count(1),0,'N','S')
into	ie_visivel_w
from	modelo_grupo_regra_visual a,
	modelo_grupo_conteudo b
where	a.nr_seq_grupo	= b.nr_sequencia
and	b.nr_sequencia	= nr_seq_grupo_p;

return	ie_visivel_w;

end obter_se_grupo_quest_visivel;
/