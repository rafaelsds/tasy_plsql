CREATE OR REPLACE
FUNCTION obter_localizacao_mat_estab(
			cd_material_p		Number,
			cd_local_estoque_p	Number,
			cd_estabelecimento_p	number)
			RETURN VARCHAR2 IS

ds_localizacao_w			Varchar2(80);

/* Esta function � uma c�pia da Obter_Localizacao_Material por�m com a passagem do par�metro estabelecimento */

BEGIN


select	nvl(max(decode(nr_seq_estrut_mat,null,ds_localizacao,substr(obter_desc_estrut_loc_material(nr_seq_estrut_mat),1,80))), 'X')
into	ds_localizacao_w
from	localizacao_estoque_local
where	cd_material		= cd_material_p
and	cd_local_estoque	= cd_local_estoque_p
and	cd_estabelecimento	= cd_estabelecimento_p;

if	(ds_localizacao_w = 'X') then
	begin
	select	decode(nr_seq_estrut_mat,null,ds_localizacao,substr(obter_desc_estrut_loc_material(nr_seq_estrut_mat),1,80))
	into	ds_localizacao_w
	from 	padrao_estoque_local b,
		material a
	where	a.cd_material_estoque 	= b.cd_material
	and	a.cd_material		= cd_material_p
	and	b.cd_local_estoque	= cd_local_estoque_p
	and	b.cd_estabelecimento	= cd_estabelecimento_p;
	exception
		when others then
			ds_localizacao_w	:= '';
	end;

	if	(nvl(ds_localizacao_w, 'X') = 'X') then
		ds_localizacao_w	:= Obter_Dados_Material(cd_material_p, 'DL');
	end if;

end if;

RETURN ds_localizacao_w;

END obter_localizacao_mat_estab;
/