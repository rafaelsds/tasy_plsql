create or replace
function pls_obter_dados_solic
			(	nr_sequencia_p		in number,
				ie_tipo_acesso_p	in varchar2,
				ie_opcao_p		in varchar2)
 		    	return varchar2 is

/*
	IE_OPCAO_P
	N - Nome de quem fez a solicita��o
	T - Tipo do usu�rio
*/
			
ds_retorno_w		varchar2(255);
nm_usuario_w		varchar2(15);
ds_chave_simples_w	varchar(255);
ds_chave_composta_w	varchar2(500);
qt_registro_w		number(2);
nm_solicitante_w	varchar2(255);
ds_tipo_usu_solic_w	varchar2(60);
k			number(2);
cd_pf_pj_w		varchar2(255);

begin

select	nm_usuario_nrec,
	ds_chave_simples,
	ds_chave_composta
into	nm_usuario_w,
	ds_chave_simples_w,
	ds_chave_composta_w
from	tasy_solic_alt_campo
where	nr_sequencia = nr_sequencia_p;

if	(ds_chave_simples_w is not null) then
	cd_pf_pj_w := ds_chave_simples_w;
else
	k := instr(ds_chave_composta_w,'#@#@');
	ds_chave_simples_w := substr(ds_chave_composta_w,1,k-1);
	k := instr(ds_chave_simples_w,'=');
	cd_pf_pj_w := substr(ds_chave_simples_w,k+1,k-1);
end if;



if	(ie_tipo_acesso_p = 'B') then
	nm_solicitante_w 	:= pls_obter_dados_segurado(nm_usuario_w, 'N');
	ds_tipo_usu_solic_w 	:= 'Benefici�rio';
elsif	(ie_tipo_acesso_p = 'E') then
	nm_solicitante_w 	:= pls_obter_nm_estipulante_web(nm_usuario_w);
	ds_tipo_usu_solic_w 	:= 'Estipulante';
elsif	(ie_tipo_acesso_p = 'GC') then
	nm_solicitante_w 	:= pls_obter_dados_grupo_web(nm_usuario_w,'N');
	ds_tipo_usu_solic_w 	:= 'Grupo contrato';
elsif	(ie_tipo_acesso_p = 'P') then
	nm_solicitante_w 	:= pls_obter_dados_prestador(nm_usuario_w,'N');
	ds_tipo_usu_solic_w 	:= 'Prestador';
end if;

/*
select	count(*)
into	qt_registro_w
from	pls_segurado
where	nr_sequencia 	= nm_usuario_w
and	cd_pessoa_fisica = cd_pf_pj_ w;

if	(qt_registro_w > 0) then
	nm_solicitante_w 	:= pls_obter_dados_segurado(nm_usuario_w, 'N');
	ds_tipo_usu_solic_w 	:= 'Benefici�rio';
else
	nm_solicitante_w 	:= pls_obter_nm_estipulante_web(nm_usuario_w);
	ds_tipo_usu_solic_w 	:= 'Estipulante';
end if;
*/

if	(ie_opcao_p = 'N') then
	ds_retorno_w := nm_solicitante_w;
elsif	(ie_opcao_p = 'T') then
	ds_retorno_w := ds_tipo_usu_solic_w;
end if;

return	ds_retorno_w;

end pls_obter_dados_solic;
/