create or replace
function obter_forma_apres_proc(	cd_procedimento_p	number,
				ie_origem_proced_p	number)
	 		    	return number is

ie_forma_apresentacao_w	number;

begin

if	(ie_origem_proced_p is null) then
	select	ie_forma_apresentacao
	into	ie_forma_apresentacao_w
	from	procedimento
	where	cd_procedimento	= cd_procedimento_p;
else
	select	ie_forma_apresentacao
	into	ie_forma_apresentacao_w
	from	procedimento
	where	cd_procedimento	= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;
end if;	

return	ie_forma_apresentacao_w;

end obter_forma_apres_proc;
/