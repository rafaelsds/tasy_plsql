create or replace
function pls_obter_email_pf_pj_lista(
		cd_pessoa_fisica_lista_p	varchar2,
		cd_cgc_lista_p		varchar2)
 	return varchar2 is

cd_pessoa_fisica_lista_w	varchar2(2000);
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_lista_w		varchar2(2000);
cd_cgc_w			varchar2(14);
ds_email_w		varchar(255);
ds_email_lista_w		varchar2(2000);

begin

ds_email_lista_w	:= '';

cd_pessoa_fisica_lista_w	:= cd_pessoa_fisica_lista_p;
while cd_pessoa_fisica_lista_w is not null loop 
	begin
	cd_pessoa_fisica_w		:= substr(cd_pessoa_fisica_lista_w, 1, instr(cd_pessoa_fisica_lista_w, ',') - 1);
	cd_pessoa_fisica_lista_w	:= substr(cd_pessoa_fisica_lista_w, instr(cd_pessoa_fisica_lista_w, ',') + 1, length(cd_pessoa_fisica_lista_w));

	select	distinct ds_email
	into	ds_email_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

	ds_email_lista_w	:= ds_email_lista_w || ',' || ds_email_w;
	end;
end loop;

cd_cgc_lista_w	:= cd_cgc_lista_p;
while cd_cgc_lista_w is not null loop 
	begin
	cd_cgc_w		:= substr(cd_cgc_lista_w, 1, instr(cd_cgc_lista_w, ',') - 1);
	cd_cgc_lista_w	:= substr(cd_cgc_lista_w, instr(cd_cgc_lista_w, ',') + 1, length(cd_cgc_lista_w));

	select	ds_email
	into	ds_email_w
	from	pessoa_juridica
	where	cd_cgc = cd_cgc_w;

	ds_email_lista_w	:= ds_email_lista_w || ',' || ds_email_w;
	end;
end loop;

return	ds_email_lista_w;
end pls_obter_email_pf_pj_lista;
/