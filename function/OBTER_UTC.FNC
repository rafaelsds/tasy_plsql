create or replace 
function obter_utc(dt_referencia_p timestamp default sysdate)
return varchar2 is

ie_utc_w	varchar2(15);

begin

begin
	select	ie_utc
	into	ie_utc_w
	from	estab_horario_verao
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and	dt_referencia_p between dt_inicial and dt_final;

exception
when others then
	ie_utc_w	:= null;
end;

if	(ie_utc_w is null) then
	begin
		select	ie_utc
		into	ie_utc_w
		from	estabelecimento
		where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;	
	exception
	when others then
		ie_utc_w	:= TO_CHAR(SYSTIMESTAMP,'TZR');
	end;
end	if;
	

return ie_utc_w;
  
end obter_utc;
/
