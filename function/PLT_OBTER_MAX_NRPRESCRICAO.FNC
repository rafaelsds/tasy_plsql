create or replace
function PLT_obter_max_nrprescricao
 		    	return number is

nr_prescricao_w 	number(14);			
			
begin

select	max(plt_obter_max_nr_prescricao(nr_prescricoes))
into	nr_prescricao_w
from	w_rep_t
where	nvl(dt_atualizacao,sysdate) between (sysdate - 1) and (sysdate + 1);

return	nr_prescricao_w;

end PLT_obter_max_nrprescricao;
/
