create or replace
function OBTER_CD_CONTA_BANCO (nr_sequencia_p	number) return varchar2 is

cd_conta_w	banco_estabelecimento.cd_conta%type;
ie_digito_conta_w 	banco_estabelecimento.ie_digito_conta%type;

begin
if ( nvl(nr_sequencia_p,0) <> 0 ) then
	select	substr(max(cd_conta),1,254),
		substr(max(ie_digito_conta),1,254)
	into	cd_conta_w,
		ie_digito_conta_w
	from	banco_estabelecimento_v
	where	nr_sequencia = nr_sequencia_p;
end if;

return	cd_conta_w || '-' || ie_digito_conta_w;

end;
/