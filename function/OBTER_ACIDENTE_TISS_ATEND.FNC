create or replace
function obter_acidente_tiss_atend(	nr_atendimento_p        number)
                           		return                  number  is

cd_acidente_atend_w		number(10);
ie_retorno_w			number(10);

begin

select	nvl(nr_seq_tipo_acidente,0)
into	cd_acidente_atend_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select	max(ie_tipo_acidente)
into	ie_retorno_w
from	tiss_tipo_acidente
where	nr_seq_acidente = cd_acidente_atend_w
and	(dt_inicio_vigencia is null or dt_inicio_vigencia <= sysdate);

return ie_retorno_w;

end obter_acidente_tiss_atend;
/