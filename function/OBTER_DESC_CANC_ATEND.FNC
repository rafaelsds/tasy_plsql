create or replace
function obter_desc_canc_atend(nr_atendimento_p	number)
 		    	return varchar2 is

nr_seq_motivo_cancelamento_w	number(10);			
ds_motivo_cancelamento_w	varchar2(80);			
			
begin

select	nvl(max(nr_seq_motivo),0)
into	nr_seq_motivo_cancelamento_w
from	atendimento_cancelamento
where	nr_atendimento = nr_atendimento_p;

if	(nr_seq_motivo_cancelamento_w > 0) then
	
	select	substr(ds_motivo,1,80)
	into	ds_motivo_cancelamento_w
	from	motivo_cancel_atend
	where	nr_sequencia = nr_seq_motivo_cancelamento_w;
 
end if; 

return	ds_motivo_cancelamento_w;

end obter_desc_canc_atend;
/