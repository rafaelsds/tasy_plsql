create or replace 
FUNCTION obter_prioridade_tcr(
			nr_sequencia_p		Number)
			return Number is

ie_prioridade_w		Number(3);

BEGIN
ie_prioridade_w	 := 999;

if	(nr_sequencia_p	is not null) then

	select	nvl(max(nr_seq_prioridade),999)
	into	ie_prioridade_w
	from	triagem_classif_risco
	where	nr_sequencia	= nr_sequencia_p;

end if;

return ie_prioridade_w;

END obter_prioridade_tcr;
/
