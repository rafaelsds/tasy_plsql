create or replace
FUNCTION lab_obter_valor_campo_tabela(	nm_tabela   VARCHAR2,
										nm_campo  VARCHAR2,
										ds_restricao VARCHAR2)
			RETURN VARCHAR2 AS

query		VARCHAR2(4000);
resultado	VARCHAR2(4000);

BEGIN

query := 'SELECT '||nm_campo||' FROM '||nm_tabela||' WHERE '||ds_restricao;
DBMS_OUTPUT.PUT_LINE('Query: ' || query);
EXECUTE IMMEDIATE query INTO resultado;
--DBMS_OUTPUT.PUT_LINE('Record: ' || record);

RETURN resultado;

END;
/