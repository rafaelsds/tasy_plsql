create or replace
function paf_obter_dados_aliquota( 	nr_seq_aliquota_p	number,
				nr_fabricacao_ecf_p varchar2,
				ie_opcao_p	varchar2)
 		    	return varchar2 is
		
ds_retorno_w		varchar2(255);
pr_aliquota_w		varchar2(10);
ie_tipo_aliquota_w		varchar2(10);
ds_aliquota_w		varchar2(10);
nr_ordem_cadastro_w	varchar2(10);
qt_aliquotas_w	Number(10);
qt_ecf_w				Number(10);
nr_seq_ecf_w	Number(10);
	
begin

select lpad( paf_somente_numero(to_char(pr_aliquota, '99.99')),4,0),
	ie_tipo_aliquota,
	ds_aliquota
into	pr_aliquota_w,
	ie_tipo_aliquota_w,
	ds_aliquota_w
from	paf_aliquotas
where	nr_sequencia = nr_seq_aliquota_p;

if	(ie_opcao_p = 'IN') then
	
	select	count(nr_sequencia)
	into	qt_ecf_w
	from	paf_ecf
	where	nr_fabricacao_ecf = nr_fabricacao_ecf_p;
	
	if (qt_ecf_w > 0) then
	
		select	max(nr_sequencia)
		into	nr_seq_ecf_w
		from	paf_ecf
		where	nr_fabricacao_ecf = nr_fabricacao_ecf_p;
	
		select count(*)
		into	qt_aliquotas_w
		from	paf_aliquota_ecf a
		where	a.nr_seq_ecf = nr_seq_ecf_w
		and	a.nr_seq_aliquota = nr_seq_aliquota_p;	
	
		if (qt_aliquotas_w > 0) then
		
			select	to_char(max(nr_ordem_cadastro))
			into	nr_ordem_cadastro_w
			from	paf_aliquota_ecf a
			where	a.nr_seq_ecf = nr_seq_ecf_w
			and	a.nr_seq_aliquota = nr_seq_aliquota_p;
		
			ds_retorno_w := nr_ordem_cadastro_w;
		end if;
		
	end if;	
elsif	(ie_opcao_p = 'PR') then
	ds_retorno_w := pr_aliquota_w;
elsif	(ie_opcao_p = 'TP') then	
	ds_retorno_w := ie_tipo_aliquota_w;
elsif	(ie_opcao_p = 'DS') then		
	ds_retorno_w := ds_aliquota_w;
end if;

return	ds_retorno_w;

end paf_obter_dados_aliquota;
/