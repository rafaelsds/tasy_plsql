create or replace
function obter_barras_material(cd_material_p number,
									cd_cgc_p varchar2)
									return varchar2 is

ds_retorno_w 		varchar2(4000);
cd_barra_material_w 	varchar2(255);

cursor c01 is
select	a.cd_barra_material
from	MATERIAL_COD_BARRA a
where	a.cd_material = cd_material_p
and	((a.cd_cgc_fabricante = cd_cgc_p) or (nvl(cd_cgc_p,'0') = '0'));

begin
open C01;
loop
fetch C01 into
	cd_barra_material_w;
exit when C01%notfound;
	begin
	if (length(ds_retorno_w || cd_barra_material_w || ', ') < 4000) then
		ds_retorno_w := ds_retorno_w || cd_barra_material_w || ', ';
	end if;
	end;
end loop;
close C01;

ds_retorno_w := substr(ds_retorno_w,1,length(ds_retorno_w)-2);

return ds_retorno_w;

end obter_barras_material;
/