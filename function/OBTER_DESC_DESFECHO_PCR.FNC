CREATE OR REPLACE
FUNCTION Obter_desc_desfecho_pcr(	ie_desfecho_p		Varchar2)
		             	RETURN Varchar IS

ds_desfecho_w		Varchar2(254) := null;
BEGIN

if	( ie_desfecho_p is not null) then
	begin
	
	if	(ie_desfecho_p = 'R') then
	
		ds_desfecho_w := Wheb_mensagem_pck.get_texto(308958); --'Revers�o da parada';
	
	elsif 	(ie_desfecho_p = 'O') then
	
		ds_desfecho_w := Wheb_mensagem_pck.get_texto(308959); --'�bito';
	end if;

	end;
end if;

return ds_desfecho_w;

END Obter_desc_desfecho_pcr;
/
