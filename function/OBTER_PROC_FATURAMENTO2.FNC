create or replace 
FUNCTION obter_proc_faturamento2(nr_seq_proc_interno_p		NUMBER,
				ie_opcao_p			varchar2)
 		    	RETURN NUMBER IS


cd_procedimento_w	NUMBER(10);
ie_origem_proced_w	number(10);

BEGIN

SELECT 	NVL(MAX(cd_procedimento),0),
	NVL(MAX(ie_origem_proced),0)
INTO	cd_procedimento_w,
	ie_origem_proced_w
FROM	proc_interno
WHERE	nr_sequencia = nr_seq_proc_interno_p;

if	(ie_opcao_p = 'P') then
	RETURN	cd_procedimento_w;
else
	RETURN	ie_origem_proced_w;
end if;

END obter_proc_faturamento2;
/
