create or replace
function HSL_obter_nome_agenda_rel(cd_agenda_p		number)
 		    	return varchar2 is

cd_tipo_agenda_w	number(10);
ie_ordenacao_w		varchar2(10);
cd_estabelecimento_w	number(10);
ds_retorno_w		varchar2(255);
ie_parametro_w		varchar2(2);

begin
obter_param_usuario(898,51, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_parametro_w);

if	(cd_agenda_p > 0) then
	select  max(cd_estabelecimento),
		max(cd_tipo_agenda),
		max(ie_ordenacao)
	into	cd_estabelecimento_w,
		cd_tipo_agenda_w,
		ie_ordenacao_w
	from	agenda
	where	cd_agenda = cd_agenda_p;
	
	select	substr(obter_nome_medico_combo_agcons(cd_estabelecimento_w, cd_agenda_p, cd_tipo_agenda_w, decode(ie_ordenacao_w, null, ie_parametro_w, ie_ordenacao_w)),1,240)
	into	ds_retorno_w
	from dual;
	
end if;
return	ds_retorno_w;

end HSL_obter_nome_agenda_rel;
/