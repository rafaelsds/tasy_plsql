CREATE OR REPLACE
FUNCTION obter_classif_setor_cross(cd_setor_atendimento_p		number)
 		    	RETURN VARCHAR2 IS

nr_retorno_w 		number(10) default 0;
cd_classif_setor_w 	varchar(2);

begin

if (nvl(cd_setor_atendimento_p,0) > 0) then

	select 	max(cd_classif_setor)
	into	cd_classif_setor_w
	from	setor_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_p;

	select	count(*)
	into	nr_retorno_w
	from	regra_classif_setor_cross
	where	ie_situacao = 'A'
	and		nvl(cd_classif_setor, cd_classif_setor_w) = cd_classif_setor_w;
	
	if (nr_retorno_w = 0) then	
		select	decode(count(*),0,1,0)
		into	nr_retorno_w
		from	regra_classif_setor_cross
		where	ie_situacao = 'A';
	end if;

end if;
	
return nr_retorno_w;

end obter_classif_setor_cross;
/