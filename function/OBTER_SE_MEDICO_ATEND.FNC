create or replace
function Obter_se_medico_atend(cd_medico_p		number,
				qt_horas_p		number)
 		    	return varchar2 is
			
ie_atend_w	varchar2(1);

begin

if (cd_medico_p is not null) and
   (qt_horas_p is not null) then
	select	decode(count(*), 0, 'N', 'S')
	into	ie_atend_w
	from 	atendimento_ps_v 
	where 	cd_medico_resp = cd_medico_p
	and   	dt_entrada between (sysdate - (qt_horas_p / 24)) and sysdate 
	and   	hr_inicio_consulta is not null 
	and   	hr_fim_consulta is null
	and   	dt_alta is null;
end if;

return	ie_atend_w;

end Obter_se_medico_atend;
/