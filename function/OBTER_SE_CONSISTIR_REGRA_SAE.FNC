create or replace
function obter_se_consistir_regra_sae	(	nr_seq_regra_p	number,
						cd_perfil_p	number)
				return varchar2 is

ie_regra_existe_w		varchar2(1);
ie_consistir_w		varchar2(1);
ie_forma_consistencia_w	varchar2(15);
ie_regra_lib_perfil_w	varchar2(15);

Cursor C01 is
	select	ie_forma_consistencia
	from	regra_consiste_sae_par
	where	nr_seq_regra	= nr_seq_regra_p
	and	nvl(cd_perfil,cd_perfil_p)	= cd_perfil_p
	order by nvl(cd_perfil,0);
	
	
begin

open C01;
loop
fetch C01 into	
	ie_forma_consistencia_w;
exit when C01%notfound;
end loop;
close C01;

return nvl(ie_forma_consistencia_w,'X');

end obter_se_consistir_regra_sae;
/
