create or replace
function obter_data_estab_mat_dil(cd_material_p		number,
									dt_atualizacao_nrec_p date)
 		    	return date is
		
qt_estabilidade_w	number;

begin

select 	min(converte_estab_hor(ie_tempo_estab, qt_estabilidade))
into	qt_estabilidade_w
from 	material_armazenamento
where 	cd_material = cd_material_p
and 	nr_seq_estagio = (select max(nr_sequencia)
                          from mat_estagio_armaz
						  where ie_estagio = 1)
and		nvl(cd_estabelecimento, nvl(wheb_usuario_pck.get_cd_estabelecimento,0)) = nvl(wheb_usuario_pck.get_cd_estabelecimento,0);

if(qt_estabilidade_w is null) then
	qt_estabilidade_w := 0;
end if;

return	dt_atualizacao_nrec_p + qt_estabilidade_w/24;

end obter_data_estab_mat_dil;
/