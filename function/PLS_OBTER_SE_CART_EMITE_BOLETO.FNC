create or replace
function pls_obter_se_cart_emite_boleto(nr_seq_carteira_cobr_p 		number,
					ds_carteiras_p			varchar2)
						return varchar2 is

ds_retorno_w		varchar2(1) := 'S';
cd_carteira_w		varchar2(40);
ds_virgula_w		varchar2(1);
qt_registro_w		number(10) := 0;
			
begin
if	(nr_seq_carteira_cobr_p is not null) then
	select	cd_carteira
	into	cd_carteira_w
	from	banco_carteira
	where	nr_sequencia = nr_seq_carteira_cobr_p;
	
	select	instr(replace(','|| ds_carteiras_p || ',',' ',''),',' || cd_carteira_w || ',')
	into	qt_registro_w
	from	dual;
	
	if	(qt_registro_w > 0) then
		ds_retorno_w := 'N';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_cart_emite_boleto;
/