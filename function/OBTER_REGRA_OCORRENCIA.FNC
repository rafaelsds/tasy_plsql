create or replace
function obter_regra_ocorrencia(nr_sequencia_p	number,
				nr_seq_turno_p	number,
				nr_seq_tipo_ocorrencia_p varchar2)
 		    	return varchar2 is

qt_reg_w	number(10);
ie_retorno_w	varchar2(1);

begin
select	count(*)
into	qt_reg_w
from	regra_ocorrencia_turno_lib
where	nr_seq_regra = nr_sequencia_p;

if	(qt_reg_w > 0) then
	select	count(*)
	into	qt_reg_w
	from	regra_ocorrencia_turno a,
		regra_ocorrencia_turno_lib b
	where	a.nr_sequencia = b.nr_seq_regra
	and	nvl(b.nr_seq_turno,nvl(nr_seq_turno_p,0)) = nvl(nr_seq_turno_p,0)
	and	b.nr_seq_tipo_ocorrencia = nr_seq_tipo_ocorrencia_p
	and	a.nr_sequencia = nr_sequencia_p;
	
	if	(qt_reg_w > 0) then
		ie_retorno_w := 'S';
	else
		ie_retorno_w := 'N';
	end if;
else
	ie_retorno_w := 'S';
end if;

return	ie_retorno_w;

end obter_regra_ocorrencia;
/