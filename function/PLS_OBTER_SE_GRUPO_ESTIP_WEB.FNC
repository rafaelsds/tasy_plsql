create or replace
function pls_obter_se_grupo_estip_web( nr_seq_contrato_int_p		Number,
				       nr_seq_contrato_p		Number,
				       nr_seq_auditoria_p		Number)
 		    	return Number is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter se o grupo atual da an�lise � o mesmo do estipulante
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/			
			
nm_usuario_exec_w	Varchar2(15);
qt_registros_w		Number(10);
nr_seq_grupo_w		Number(10) := null;
nr_seq_grupo_atual_w	Number(10);
ie_status_aud_w		Varchar2(3);

	
begin

select  max(ie_status)
into	ie_status_aud_w
from	pls_auditoria
where	nr_sequencia = nr_seq_auditoria_p;

-- Verifica se auditoria esta em analise
if	(nvl(ie_status_aud_w,'X') in ('A','AJ','AF','AN','AP')) then

	-- Verifica se o estipulante tem permiss�o para auditar
	if	(nvl(nr_seq_contrato_int_p,0) > 0) then
		select  max(a.nr_sequencia)
		into	nr_seq_grupo_w
		from	pls_grupo_auditor a,
			pls_membro_grupo_aud b
		where	a.nr_sequencia = b.nr_seq_grupo
		and	nvl(a.ie_permissao, 'X') = 'T'
		and	a.ie_tipo_auditoria = 3
		and	b.nr_seq_intercambio  = nr_seq_contrato_int_p;
	elsif	(nvl(nr_seq_contrato_p,0) > 0) then
		select  max(a.nr_sequencia)
		into	nr_seq_grupo_w
		from	pls_grupo_auditor a,
			pls_membro_grupo_aud b
		where	a.nr_sequencia = b.nr_seq_grupo
		and	nvl(a.ie_permissao, 'X') = 'T'
		and	a.ie_tipo_auditoria = 3
		and	b.nr_seq_contrato  = nr_seq_contrato_p;
	end if;

	if	(nr_seq_grupo_w is not null) then
		
		nr_seq_grupo_atual_w := pls_obter_grupo_analise_atual(nr_seq_auditoria_p);

		if	(nvl(nr_seq_grupo_atual_w,0) = 0) then
			nr_seq_grupo_w := null; -- An�lise encerrada
		else
			-- Verifica se esta em an�lise ou se o grupo atual da an�lise � o mesmo do estipulante
			select	count(1)
			into	qt_registros_w
			from	pls_auditoria_grupo a
			where	a.nr_sequencia = nr_seq_grupo_atual_w
			and	((a.nm_usuario_exec is not null)
			or	(a.nr_seq_grupo = nr_seq_grupo_w));
			
			if	(qt_registros_w > 0) then
				nr_seq_grupo_w := null;
			end if;
		end if;
	end if;
end if;

return	nr_seq_grupo_w;

end pls_obter_se_grupo_estip_web;
/
