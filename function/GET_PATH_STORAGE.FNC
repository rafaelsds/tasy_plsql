create or replace
function GET_PATH_STORAGE
			(	ds_path_p 		file_storage_path.ds_path%type,
				cd_empresa_p		file_storage_path.cd_empresa%type,
				cd_estabelecimento_p	file_storage_path.cd_estabelecimento%type,
				cd_perfil_p		file_storage_path.cd_perfil%type,
				cd_setor_atendimento_p	file_storage_path.cd_setor_atendimento%type,
				nm_usuario_p 		file_storage_path.nm_usuario_path%type)
 		    	return varchar2 is
ds_path_w	file_storage_path.ds_path%type := '';
cursor	C01 is
	select	b.ds_path
	from 	file_storage a,
		file_storage_path b
	where   a.nm_storage = b.nm_storage
	and 	a.nm_storage = ds_path_p
	and 	b.ie_writable = 'S'
	and 	(b.cd_empresa is null or b.cd_empresa = cd_empresa_p)
	and 	(b.cd_estabelecimento is null or b.cd_estabelecimento = cd_estabelecimento_p)
	and 	(b.cd_perfil is null or b.cd_perfil = cd_perfil_p)
	and 	(b.cd_setor_atendimento is null or b.cd_setor_atendimento = cd_setor_atendimento_p)
	and 	(b.nm_usuario_path is null  or b.nm_usuario_path = nm_usuario_p)
order by b.nm_usuario_path, b.cd_setor_atendimento, b.cd_perfil, b.cd_estabelecimento, b.cd_empresa;
begin
	for r_c01_w in C01 loop
		begin
		ds_path_w := r_c01_w.ds_path;
		end;
	end loop;

return	ds_path_w;

end GET_PATH_STORAGE;
/