CREATE OR REPLACE FUNCTION obter_justif_exec_procedimento (
                                                          nr_seq_proc_pac_p INTEGER
) RETURN VARCHAR2 IS
    ds_justificativa_exec_w procedimento_paciente_dado.ds_justificativa_exec%TYPE := NULL;
BEGIN
    IF ( nr_seq_proc_pac_p IS NOT NULL ) THEN
        SELECT
            ds_justificativa_exec
        INTO ds_justificativa_exec_w
        FROM
            procedimento_paciente_dado
        WHERE
            nr_seq_proc = nr_seq_proc_pac_p;

    END IF;

    RETURN ds_justificativa_exec_w;
END obter_justif_exec_procedimento;
/
