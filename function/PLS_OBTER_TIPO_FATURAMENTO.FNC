create or replace function pls_obter_tipo_faturamento(	ds_tipo_faturamento_w	pls_conta.ie_tipo_faturamento%type)
							return varchar2 is

ds_retorno_w	pls_conta.ie_tipo_faturamento%type;
begin

if	(ds_tipo_faturamento_w = '1') then
	ds_retorno_w := 'P';
elsif	(ds_tipo_faturamento_w = '2') then
	ds_retorno_w := 'F';
elsif	(ds_tipo_faturamento_w = '3') then
	ds_retorno_w := 'C';
elsif	(ds_tipo_faturamento_w = '4') then
	ds_retorno_w := 'T';
end if;

return	ds_retorno_w;

end pls_obter_tipo_faturamento;
/
