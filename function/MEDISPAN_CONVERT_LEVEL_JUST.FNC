create or replace 
function medispan_convert_level_just( cd_api_p			in 	varchar2,
									  ds_severidade_p	in	varchar2 ) 
								return varchar2 is

	/*
		Nem todas as APIs da Lexicomp retornam todos os niveis de severidade, 
		entao a gente preenche os niveis que faltam na API para se encaixar no cadastro e assim obrigar a justificativa
		Todos os niveis da API estao documentados na function get_severity_level
		Route - Possui somente 2 niveis que sao major e contraindicado, nao precisa fazer nada pois eles ja retornam os maiores levels
	*/

	ds_severidade_conv_w	varchar2(50 char);

begin
	ds_severidade_conv_w := ds_severidade_p;

	if ((cd_api_p is not null) and (ds_severidade_p is not null)) then
		/* Drug Interaction - Possui somente 3 niveis */
		if (cd_api_p = 'A') then
			if (ds_severidade_p = 'CONTRAINDICATED') then
				ds_severidade_conv_w := 'MAJOR';
			end if;
		end if;
	end if;

	return ds_severidade_conv_w;

end medispan_convert_level_just;
/
