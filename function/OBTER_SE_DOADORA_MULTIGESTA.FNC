create or replace
function obter_se_doadora_multigesta( nr_sequencia_p	number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);

begin

ie_retorno_w := 'N';

if (nr_sequencia_p is not null) then

	begin
	SELECT	'S'
	into	ie_retorno_w
	FROM	san_impedimento b,
		san_questionario c,
		san_doacao d
	WHERE	b.nr_sequencia 		= c.nr_seq_impedimento
	AND	c.nr_seq_doacao 	= d.nr_sequencia
	AND	b.ie_situacao 		= 'A'
	AND	c.nr_seq_doacao 	= nr_sequencia_p
	and	nvl(ie_resposta,'N') 	= 'S'
	AND	NVL(b.ie_multigesta,'N')= 'S'
	AND	((san_obter_sexo_pf_doador(d.cd_pessoa_fisica) = b.ie_sexo) OR (b.ie_sexo = 'A') OR (b.ie_sexo IS NULL));
	exception 
		when others then
			ie_retorno_w := 'N';
	end;
end if;	
	
return	ie_retorno_w;

end obter_se_doadora_multigesta;
/
