create or replace
function hsjb_obter_dietas_concat(nr_sequencia_p	number)
 		    	return varchar2 is
				
ds_retorno_w		varchar2(4000);
ds_retorno_aux_w	varchar2(1000);				
				
cursor C01 is
	select	obter_nome_dieta(a.cd_dieta)
	from	mapa_dieta a			
	where	a.nr_sequencia = nr_Sequencia_p
	and		cd_dieta is not null
	union
	select	obter_nome_dieta(a.cd_dieta)
	from	mapa_dieta a			
	where	a.nr_seq_superior = nr_Sequencia_p
	and		cd_dieta is not null
	order by 1;

begin

open c01;
loop
fetch c01 into ds_retorno_aux_w;
exit when c01%notfound;

	ds_retorno_w := ds_retorno_w||', '||ds_retorno_aux_w;
	
end loop;
close c01;

return	substr(ds_retorno_w,2,3999);

end hsjb_obter_dietas_concat;
/
