create or
replace function san_obter_dados_json_sht(nr_seq_lote_p number,
                                          nr_seq_item_p number,
										  nm_usuario_p varchar2) 
return clob is

  json_aux_w   		    philips_json;
  json_receptor_w	    philips_json;
  json_reacao_w 	    philips_json;
  json_reacoes_w	    philips_json;
  JSON_LIST_REACTIONS   PHILIPS_JSON_LIST;
  JSON_LIST_ANTIBODIES  PHILIPS_JSON_LIST;
  JSON_LIST_BAGS        PHILIPS_JSON_LIST;
  json_anticorpos_w     philips_json;
  json_anticorpo_w	    philips_json;
  json_bolsas_w		    philips_json;
  json_bolsa_w		    philips_json;
  
  cd_tipo_receptor_w 	varchar2(2);
  ie_houve_reacao_w     varchar2(5);
  ie_houve_pai_w        varchar2(5);
  ie_aliquotado_w		varchar2(5);
  ie_pediatrico_w       varchar2(5);
  ie_rejuvenescido_w	varchar2(5);
  ie_filtrado_w			varchar2(5);
  ie_irradiado_w		varchar2(5);
  ie_lavado_w			varchar2(5);
  cd_reacao_w           varchar2(255);
  vinculo_paciente_w	varchar2(10);
  motivo_inutil_w		san_lote_item.cd_motivo_inutilizacao%type;
  uf_cedencia_w			san_lote_item.uf_entidade%type;
  cd_entidade_w			san_lote_item.cd_entidade%type;
  ds_entidade_w			san_lote_item.ds_entidade%type;
  cd_componente_w       san_lote_item.cd_seq_derivado%type;
  varchar_field_w       varchar2(1);
  numeric_field_w       number(1);
   
  ds_message_w clob;
  
  cursor c_dados_sht is
	select nr_seq_trans,
		   nr_seq_emp,
		   nr_seq_inutil,
		   dt_destino,
		   cd_destino,
		   cd_agencia,
		   /* receiver */
		   cd_pessoa_mae, 
		   nm_mae, 
		   cd_pessoa_fisica, 
		   nm_pessoa_fisica, 
		   ie_sexo, 
		   dt_nascimento, 
		   nr_cpf, 
		   nr_cartao_nac_sus,
		   ie_tipo_fator_sangue,
		   /* reactions */
		   ie_houve_reacao, 
		   cd_tipo_reacao,								  
		   ie_houve_pai_positivo,
		   /* antibodies */
		   ds_lista_anticorpos,								  
		   ie_tipo_convenio,
		   cd_motivo_inutilizacao,
		   cd_entidade,
		   uf_entidade,
		   ds_entidade,								  
		   ie_exsanguineo,
		   ie_aliquotado,
		   ie_pediatrico, 
		   ie_pool
	from san_lote_item
	where nr_seq_lote = nr_seq_lote_p
    and nr_sequencia = nr_seq_item_p
	order by nm_pessoa_fisica, nm_mae;

	
  cursor c_anticorpos(cd_anticorpo_p varchar2) is
	select regexp_substr(cd_anticorpo_p,'[^,]+', 1, level) cd_anticorpo
	from dual
	connect by regexp_substr(cd_anticorpo_p, '[^,]+', 1, level) is not null ;
	
  cursor c_bolsas is
	select nr_sec_saude, 
		   cd_seq_derivado,
		   ie_lavado, 
		   ie_filtrado, 
		   ie_irradiado, 
		   ie_rejuvenescido
	from san_lote_item
	where nr_seq_lote = nr_seq_lote_p
    and nr_sequencia = nr_seq_item_p;
    
  
begin
	json_aux_w 		:= philips_json();
	json_receptor_w	:= philips_json();
	json_reacao_w 	:= philips_json();
	json_reacoes_w	:= philips_json();
    json_list_reactions := philips_json_list();
	json_anticorpos_w 	:= philips_json();
	json_anticorpo_w 	:=	philips_json();
    json_list_antibodies := philips_json_list();
	json_bolsas_w		:=	philips_json();
	json_bolsa_w		:=	philips_json();
    json_list_bags  	:=  philips_json_list();
	
	varchar_field_w := null;
    numeric_field_w := null;
	
	for c_dados_sht_w in c_dados_sht loop
	begin        
    
		if (c_dados_sht_w.cd_destino = 1) then
			/* 1 = identificado | 2 = identificado pelo nome da mae | 3 = nao identificado */
			select nvl2(c_dados_sht_w.nm_pessoa_fisica, '1', nvl2(c_dados_sht_w.nm_mae, '2', '3')),
				   decode(c_dados_sht_w.ie_houve_reacao, 'S', 'true', 'N', 'false'),
				   decode(c_dados_sht_w.ie_houve_pai_positivo, 'S', 'true', 'N', 'false', 'false'),
				   to_char(c_dados_sht_w.ie_tipo_convenio)
			into cd_tipo_receptor_w,
				 ie_houve_reacao_w,
				 ie_houve_pai_w,
				 vinculo_paciente_w
			from dual;
			
			json_receptor_w.put('tipoReceptor', cd_tipo_receptor_w);
			json_receptor_w.put('nomeMae', c_dados_sht_w.nm_mae);
			json_receptor_w.put('nomeReceptor', c_dados_sht_w.nm_pessoa_fisica);
			json_receptor_w.put('sexo', c_dados_sht_w.ie_sexo);
			json_receptor_w.put('dataNascimento', to_char(c_dados_sht_w.dt_nascimento, 'dd/mm/yyyy'));
			json_receptor_w.put('cpf', c_dados_sht_w.nr_cpf);
		else
			select decode(c_dados_sht_w.cd_destino, 6, c_dados_sht_w.uf_entidade, varchar_field_w),
				decode(c_dados_sht_w.cd_destino, 6, c_dados_sht_w.ds_entidade, varchar_field_w),
				nvl(c_dados_sht_w.cd_entidade, varchar_field_w),
				decode(c_dados_sht_w.cd_destino, 2, c_dados_sht_w.cd_motivo_inutilizacao, varchar_field_w)
			into uf_cedencia_w,
				 ds_entidade_w,
				 cd_entidade_w,
				 motivo_inutil_w
			from dual;
		
			json_receptor_w.put('tipoReceptor', numeric_field_w);
			json_receptor_w.put('nomeMae', numeric_field_w);
			json_receptor_w.put('nomeReceptor', numeric_field_w);
			json_receptor_w.put('sexo', numeric_field_w);
			json_receptor_w.put('dataNascimento', numeric_field_w);
			json_receptor_w.put('cpf', numeric_field_w);
			
			ie_houve_reacao_w  := varchar_field_w;
			ie_houve_pai_w 	   := varchar_field_w;
			vinculo_paciente_w := varchar_field_w;
		end if;	
		
		json_receptor_w.put('cns', c_dados_sht_w.nr_cartao_nac_sus);
		json_receptor_w.put('tipoSangue', c_dados_sht_w.ie_tipo_fator_sangue);		
        
		json_aux_w.put('dataHoraDestino', to_char(c_dados_sht_w.dt_destino,'dd/mm/yyyy hh24:mi:ss'));
		json_aux_w.put('codAgencia', c_dados_sht_w.cd_agencia);
		json_aux_w.put('destino', c_dados_sht_w.cd_destino);
		json_aux_w.put('receptor', json_receptor_w);
		if (length(ie_houve_reacao_w) > 0) then
			json_aux_w.put('houveReacao', ie_houve_reacao_w);
		else
			json_aux_w.put('houveReacao', numeric_field_w);
		end if;
        
		
		cd_reacao_w := c_dados_sht_w.cd_tipo_reacao;
        
		for c_reacoes_w in (select regexp_substr(cd_reacao_w,'[^,]+', 1, level) cd_reacao
                            from dual
                            connect by regexp_substr(cd_reacao_w, '[^,]+', 1, level) is not null) 
        loop			
			if (c_reacoes_w.cd_reacao is not null) then			
				json_reacao_w.put('codReacao', c_reacoes_w.cd_reacao);
                json_list_reactions.append(json_reacao_w.to_json_value());
			end if;			
		end loop;
        
        		
        if (json_list_reactions.COUNT() > 0) THEN
            json_aux_w.put('reacoes', json_list_reactions);
        else
            json_aux_w.put('reacoes', numeric_field_w);
        end if;

        if (length(ie_houve_pai_w) > 0) then		
			json_aux_w.put('houvePai', ie_houve_pai_w);
		else
			json_aux_w.put('houvePai', numeric_field_w);
		end if;
		
		for c_anticorpos_w in c_anticorpos(c_dados_sht_w.ds_lista_anticorpos) loop			
			if (c_anticorpos_w.cd_anticorpo is not null) then							
				json_anticorpo_w.put('codAnticorpo', c_anticorpos_w.cd_anticorpo);
                json_list_antibodies.append(json_anticorpo_w.to_json_value());                		
			end if;						
		end loop;
		
		if (json_list_antibodies.COUNT() > 0) THEN
            json_aux_w.put('anticorpos', json_list_antibodies);
        else
            json_aux_w.put('anticorpos', numeric_field_w);
        end if;   
				
		if (length(vinculo_paciente_w) > 0) then
			json_aux_w.put('vinculoPaciente', vinculo_paciente_w);
		else
			json_aux_w.put('vinculoPaciente', numeric_field_w);
		end if;
		
		if (length(motivo_inutil_w) > 0) then
			json_aux_w.put('motivoInutilizacao', motivo_inutil_w);
		else
			json_aux_w.put('motivoInutilizacao', numeric_field_w);
		end if;
		
		if (length(cd_entidade_w) > 0 and c_dados_sht_w.cd_destino = 3) then
			json_aux_w.put('codBancoCedencia', cd_entidade_w);
		else
			json_aux_w.put('codBancoCedencia', numeric_field_w);
		end if;
					
		if (length(cd_entidade_w) > 0 and c_dados_sht_w.cd_destino = 4) then
			json_aux_w.put('codAgenciaCedencia', cd_entidade_w);
		else
			json_aux_w.put('codAgenciaCedencia', numeric_field_w);
		end if;
		
		if (length(uf_cedencia_w) > 0) then
			json_aux_w.put('ufCedencia', uf_cedencia_w);
		else
			json_aux_w.put('ufCedencia', numeric_field_w);
		end if;
		
		if (length(ds_entidade_w) > 0) then
			json_aux_w.put('servicoOutroEstado', ds_entidade_w);
		else
			json_aux_w.put('servicoOutroEstado', numeric_field_w);
		end if;
		
		select decode(c_dados_sht_w.ie_aliquotado, 'S', 'true', 'N', 'false'),
               decode(c_dados_sht_w.ie_pediatrico, 'S', 'true', 'N', 'false')               
		into ie_aliquotado_w,
             ie_pediatrico_w             
		from dual;
		
		json_aux_w.put('exsanguineo', 'false');
		json_aux_w.put('aliquotagem', nvl(ie_aliquotado_w, 'false'));
		json_aux_w.put('pediatrico', nvl(ie_pediatrico_w,'false'));			
		
		for c_bolsas_w in c_bolsas loop
        
            select case when length(trim(translate(c_bolsas_w.cd_seq_derivado, ' +-0123456789.', ' '))) > 0 then null else c_bolsas_w.cd_seq_derivado end
            into cd_componente_w
            from dual;
		
			select case when cd_componente_w = 2 or cd_componente_w = 3 then decode(c_bolsas_w.ie_rejuvenescido, 'S', 'true', 'N', 'false') else varchar_field_w end,				   
				   decode(c_bolsas_w.ie_filtrado, 'S', 'true', 'N', 'false', numeric_field_w),
				   decode(c_bolsas_w.ie_irradiado, 'S', 'true', 'N', 'false', numeric_field_w),
				   case when cd_componente_w = 2 or cd_componente_w = 3 or cd_componente_w = 12 then decode(c_bolsas_w.ie_lavado, 'S', 'true', 'N', 'false') else varchar_field_w end
			into ie_rejuvenescido_w,				 
				 ie_filtrado_w,
				 ie_irradiado_w,
				 ie_lavado_w
			from dual;
					
			if (c_bolsas_w.nr_sec_saude > 0) then
				json_bolsa_w.put('numBolsa', c_bolsas_w.nr_sec_saude);
				json_bolsa_w.put('codComponente', cd_componente_w);
				if (length(ie_lavado_w) > 0) then
					json_bolsa_w.put('lavado', ie_lavado_w);
				else
					json_bolsa_w.put('lavado', numeric_field_w);
				end if;
				json_bolsa_w.put('irradiado', ie_irradiado_w);
				json_bolsa_w.put('filtrado', ie_filtrado_w);
				if (length(ie_rejuvenescido_w) > 0) then
					json_bolsa_w.put('rejuvenescido', ie_rejuvenescido_w);
				else
					json_bolsa_w.put('rejuvenescido', numeric_field_w);
				end if;
                json_list_bags.append(json_bolsa_w.to_json_value());
			end if;
		end loop;		
				
		json_aux_w.put('pool', 'false');
				
		json_aux_w.put('bolsas', json_list_bags);			
	end;
	end loop;
	
	dbms_lob.createtemporary(ds_message_w, TRUE);
    json_aux_w.to_clob(ds_message_w);
	
	return ds_message_w;
	
end san_obter_dados_json_sht;
/
