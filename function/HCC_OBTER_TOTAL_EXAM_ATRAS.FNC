create or replace
function hcc_obter_total_exam_atras(cd_medico_p number,
			            cd_tipo_procedimento_p number)
			return number is

/*retorna o total de exames de determinado m�dico em determinado tipo de procedimento com atraso de 30 min entre a data de resultado da entrada unica e a data de retirada  do registro de localiza��o dos laudos*/

qt_total_exames_w number(10);

begin

	select	count(*)
	into	qt_total_exames_w
	from	laudo_paciente a,
		laudo_paciente_loc b,
		prescr_procedimento c
	where 	a.nr_sequencia = b.nr_sequencia
	and	a.nr_prescricao = c.nr_prescricao	   
	and	a.cd_medico_resp = cd_medico_p	
	and	b.ie_tipo_local = '10'
	and	((c.dt_resultado - b.dt_retirada)*1440) < 30	
	and	substr(obter_tipo_procedimento(c.cd_procedimento, c.ie_origem_proced, 'C'),1,90) = cd_tipo_procedimento_p;

return qt_total_exames_w;

end hcc_obter_total_exam_atras;
/