create or replace function obter_legendas_presc_gpt(	ie_tipo_gpt_p			varchar2,
					nr_atendimento_p		number,
					dt_inicial_p			date,
					dt_final_p			date,
					ie_reconciliacao_p		varchar2,
					ie_param_91_p			varchar2,
					ie_forma_reconciliacao_s_p	varchar2,
					ie_param_92_p			varchar2,
					cd_classif_setor_p		number,
					dt_alta_medico_p		date,
					dt_alta_p			date,
					ie_lib_farm_p                   varchar2,
					ie_tipo_legenda_p		varchar2)
 		    	return number is

nr_seq_cor_w			number;
ie_emergencia_w			varchar2(1) := 'N';
ie_inicio_analise_farm_w	varchar2(1) := 'N';
ie_liberacao_farm_w		varchar2(1) := 'N';
ie_prioridade_revisao_w		varchar2(1) := 'N';
dt_inicio_analise_w   date;

begin

select	decode(count(nr_prescricao),0,'N','S')
into	ie_emergencia_w
from	prescr_medica
where	nr_atendimento = nr_atendimento_p
and	((ie_tipo_gpt_p = 'F' and dt_liberacao is not null		
		and( ie_lib_farm_p = 'N' or (nvl(ie_lib_farm,'N') = 'S'))
		and	dt_liberacao_farmacia is null)
	or (ie_tipo_gpt_p = 'F' and dt_liberacao is null))
and	((nvl(ie_prescr_emergencia,'N') = 'S') or
	 (nvl(ie_emergencia,'N') = 'S'))
and	dt_prescricao between dt_inicial_p and dt_final_p;

	if	(ie_tipo_gpt_p = 'F') then
		begin
		
      select 	max(dt_inicio_analise)
      into    dt_inicio_analise_w
      from 	  gpt_hist_analise_plano
      where	  (nr_atendimento = nr_atendimento_p)
      and	    dt_fim_analise is null
      and	    ie_tipo_usuario = 'F';    
    
      select	decode(count(dt_inicio_analise_farm),0,'N','S'),
              decode(count(dt_liberacao_farmacia),0,'N','S')
      into	  ie_inicio_analise_farm_w,
              ie_liberacao_farm_w
      from	  prescr_medica
			where	  nr_atendimento = nr_atendimento_p				
			and	    ie_tipo_gpt_p = 'F' 
			and     dt_liberacao is not null 			
			and( ie_lib_farm_p = 'N' or (nvl(ie_lib_farm,'N') = 'S'))
			and     dt_liberacao_farmacia is null
			and	    nm_usuario_analise_farm	is not null
			and	    dt_inicio_analise_farm = dt_inicio_analise_w;    

			select	decode(count(*),0,'N','S')
			into 	ie_prioridade_revisao_w
			from 	prescr_medica a left join prescr_material b on (a.nr_prescricao = b.nr_prescricao)
			where	a.nr_atendimento = nr_atendimento_p
			and 	a.dt_prescricao between dt_inicial_p and dt_final_p
			and 	(b.ie_urgencia = 'S' or b.qt_dose_especial is not null);


			if	(ie_tipo_legenda_p = 'E' and ie_emergencia_w = 'S') then
				nr_seq_cor_w := 7398;

			elsif	(ie_tipo_legenda_p = 'A' and ie_inicio_analise_farm_w = 'S' and 
				ie_liberacao_farm_w = 'N') then
				nr_seq_cor_w := 7399;
				
			elsif	(ie_tipo_legenda_p = 'R' and ie_prioridade_revisao_w = 'S') or 
				((ie_param_92_p = 'S') and 
				(cd_classif_setor_p is not null and cd_classif_setor_p = 1)) then
				nr_seq_cor_w := 7397;

			elsif	(ie_tipo_legenda_p = 'R1' and (ie_reconciliacao_p = 'S' and ie_param_91_p = '1') or
				(ie_forma_reconciliacao_s_p = 'R' and ie_param_91_p = '2'))then
				nr_seq_cor_w := 7400;

			elsif	(ie_tipo_legenda_p = 'R2' and ie_forma_reconciliacao_s_p = 'A' and ie_param_91_p = '2') then
				nr_seq_cor_w := 7401;
				
			end if;
		end;
	 elsif (ie_tipo_gpt_p = 'E') then
		begin
			if	(dt_alta_medico_p is not null) then
				nr_seq_cor_w := 7402;
				
			elsif	(dt_alta_p is not null) then
				nr_seq_cor_w := 7403;
				
			elsif	(ie_emergencia_w = 'S') then
				nr_seq_cor_w := 7404;
			end if;
		end;
	end if;

return	nr_seq_cor_w;

end obter_legendas_presc_gpt;
/
