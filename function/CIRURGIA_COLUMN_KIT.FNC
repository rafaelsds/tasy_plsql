create or replace
function cirurgia_column_kit(	nr_prescricao_p	number)
 		    	return varchar is

				
ds_return_w		varchar2(255);	
qt_material_w	number(15,3);	
qt_material_total_w	number(15,3);			
				
begin

if	(nr_prescricao_p	> 0) and
	(nr_prescricao_p	is not null) then
	
	--Verified
	select 	nvl(sum(a.qt_material),0)
	into	qt_material_w
	from	prescr_material a
	where	a.nr_prescricao = nr_prescricao_p
	and	a.cd_kit_material is not null
	and	a.ie_status_cirurgia = 'CB';
	
	
	select 	nvl(sum(a.qt_material),0)
	into	qt_material_total_w
	from	prescr_material a
	where	a.nr_prescricao = nr_prescricao_p
	and	a.cd_kit_material is not null;

	
	
	if	(qt_material_total_w <> 0) then
		ds_return_w	:= qt_material_w ||' / '||qt_material_total_w;
	end if;
	

	
end if;


return	ds_return_w;

end cirurgia_column_kit;
/