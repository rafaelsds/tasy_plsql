create or replace
function obter_status_solic_proj_rec(nr_solic_compra_p		number)
 		    	return varchar2 is


ds_retorno_mensagem_w		varchar(255) := '';
dt_liberacao_w				solic_compra.dt_liberacao%type;
nr_seq_motivo_cancel_w		solic_compra.nr_seq_motivo_cancel%type;
qt_itens_nao_reprov_w		number(10);

	
begin

select	dt_liberacao,
	nr_seq_motivo_cancel
into	dt_liberacao_w,
	nr_seq_motivo_cancel_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

if	(nr_seq_motivo_cancel_w is null) then
	
	if	(obter_se_solic_em_ordem(nr_solic_compra_p) <> 'S') then
		begin
			
			if	(obter_se_solic_em_cotacao(nr_solic_compra_p) = 'S') then
				--Com cota��o
				ds_retorno_mensagem_w := wheb_mensagem_pck.get_Texto(402817);
			elsif	(dt_liberacao_w is null) then
				--N�o liberada
				ds_retorno_mensagem_w := wheb_mensagem_pck.get_Texto(402814);
			else
				select  count(*)
				into	qt_itens_nao_reprov_w
				from    solic_compra_item
				where   nr_solic_compra = nr_solic_compra_p
				and     dt_reprovacao is null;
					
				if (qt_itens_nao_reprov_w = 0) then 
					--Reprovada
					ds_retorno_mensagem_w := wheb_mensagem_pck.get_Texto(667706);
				else 
					--Liberada
					ds_retorno_mensagem_w := wheb_mensagem_pck.get_Texto(402815);
				end if;
				
			end if;
		end;
	else
		--Com ordem
		ds_retorno_mensagem_w := wheb_mensagem_pck.get_Texto(445167);
	end if;
else
	--Cancelada
	ds_retorno_mensagem_w := wheb_mensagem_pck.get_Texto(402833);

end if;

return	ds_retorno_mensagem_w;

end obter_status_solic_proj_rec;
/