create or replace
function 	CPOE_REGRA_ALTERACAO_SV  (	nr_atendimento_p	number) 
			return varchar2 is

ie_param_716_w			varchar2(2);
cd_perfil_w				number(5);
nm_usuario_w			varchar2(15);
cd_estabelecimento_w	number(5);
dt_atualizacao_w		date;
ie_sinal_vital_w		varchar2(2000) := '';
nr_sequencia_w			atendimento_sinal_vital.nr_sequencia%type;
count_w					number(10);
ie_retorno_w			varchar2(2000) := '';

begin

cd_perfil_w				:= wheb_usuario_pck.get_cd_perfil;
nm_usuario_w			:= wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

obter_param_usuario(1113, 716, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_param_716_w);

if (nr_atendimento_p is not null and ie_param_716_w = 'S') then

	select	max(dt_atualizacao),
			max(nr_sequencia)
	into	dt_atualizacao_w,
			nr_sequencia_w
	from 	atendimento_sinal_vital
	where 	nr_atendimento	= nr_atendimento_p
	and		dt_liberacao is not null;

	ie_sinal_vital_w := obter_alerta_sinal_vital(nr_sequencia_w);
	
	if (ie_sinal_vital_w is not null) then
	
		select	count(*)
		into	count_w
		from	cpoe_material
		where	dt_liberacao < dt_atualizacao_w
		and		dt_suspensao is null;
		
		if  (count_w > 0) then
			ie_retorno_w := ie_sinal_vital_w;
		end if;
		
	end if;		

end if;

return ie_retorno_w;

end cpoe_regra_alteracao_sv;
/