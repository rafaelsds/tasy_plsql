create or replace 
function  cpoe_display_order_reason (ie_motivo_prescricao_p	rep_exibir_motivo_prescr.ie_motivo_prescricao%type,
                                    nm_usuario_p			varchar2,
                                    cd_perfil_p				rep_exibir_motivo_prescr.cd_perfil%type)
                                    return varchar2 as
	

ie_funcao_prescritor_w     	usuario.ie_tipo_evolucao%type;
ie_exibir_itens_w		varchar2(2):='S';

cursor c01 is
select	ie_exibir_itens
from	rep_exibir_motivo_prescr
where	nvl(ie_funcao_prescritor,ie_funcao_prescritor_w)	= ie_funcao_prescritor_w
and	nvl(cd_perfil, cd_perfil_p)				= cd_perfil_p
and	nvl(ie_motivo_prescricao,ie_motivo_prescricao_p)	= ie_motivo_prescricao_p
order by nvl(cd_perfil,0),
	 nvl(ie_motivo_prescricao,'0');

begin

IF (ie_motivo_prescricao_p is null) THEN
	ie_exibir_itens_w:= 'S';
else	
	Select nvl(max(ie_tipo_evolucao),0)
	into	ie_funcao_prescritor_w
	from	usuario
	where	nm_usuario = nm_usuario_p;
	
	open C01;
	loop
	fetch C01 into
		ie_exibir_itens_w;
	exit when C01%notfound;
		ie_exibir_itens_w	:= ie_exibir_itens_w;
	end loop;
	close C01;	

END IF;

return    ie_exibir_itens_w;
	
end cpoe_display_order_reason;
/
