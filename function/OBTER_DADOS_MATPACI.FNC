create or replace
function OBTER_DADOS_MATPACI
			(nr_sequencia_p	in number,
			ie_opcao_p	in varchar2)
			return	varchar2 is
			
nr_doc_convenio_w	varchar2(255);
ds_retorno_w		varchar2(255);
cd_material_tiss_w	varchar2(20);
cd_material_tuss_w	varchar2(255);
nr_seq_proc_princ_w	number(10,0);

/*ie_opcao_p
	NG	N�mero da guia
	CT	C�digo TISS do material
	CTU	C�digo TUSS do material
	PP	Sequ�ncia do proc principal vinculado ao item
*/
			
begin

select	max(nr_doc_convenio),
	max(cd_material_tiss),
	max(cd_material_tuss),
	max(nr_seq_proc_princ)
into	nr_doc_convenio_w,
	cd_material_tiss_w,
	cd_material_tuss_w,
	nr_seq_proc_princ_w
from	material_atend_paciente
where	nr_sequencia	= nr_sequencia_p;

if	(ie_opcao_p = 'NG') then
	ds_retorno_w	:= nr_doc_convenio_w;
elsif	(ie_opcao_p = 'CT') then
	ds_retorno_w	:= cd_material_tiss_w;
elsif	(ie_opcao_p = 'CTU') then
	ds_retorno_w	:= cd_material_tuss_w;
elsif	(ie_opcao_p = 'PP') then
	ds_retorno_w	:= nr_seq_proc_princ_w;
end if;

return	ds_retorno_w;

end;
/