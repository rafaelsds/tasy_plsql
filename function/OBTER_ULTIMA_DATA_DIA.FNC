create or replace
function Obter_Ultima_Data_Dia(	nr_dia_p		number)
				return date is

dt_retorno_w		date;
nr_dia_w		number(10);
				
begin

select	to_number(to_char(sysdate,'dd'))
into	nr_dia_w
from dual;

if	(nr_dia_w >= nr_dia_p) then
	select	to_date(to_char(nr_dia_p)||'/'||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy')
	into	dt_retorno_w
	from	dual;
else
	select	to_date(to_char(nr_dia_p)||'/'||to_char(trunc(SYSDATE, 'month')-1,'mm/yyyy'),'dd/mm/yyyy')
	into	dt_retorno_w
	from	dual;
end if;

return	dt_retorno_w;

end Obter_Ultima_Data_Dia;
/
