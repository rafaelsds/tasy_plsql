create or replace function pfcs_get_order_telemetry(
				cd_estabelecimento_p in number,
				nr_seq_indicator_p in number
				) return number as

nr_rownum_w			number(15) := 0;

cursor cur_get_order_telemetry is
select	rownum nr_rownum,
		indicator_sequencia,
		nr_seq_ord,
		ie_disabled
from (	select	indicator_sequencia,
				to_number(pfcs_get_indicator_rule(indicator_sequencia, 0, cd_estabelecimento_p, 'ORDER')) nr_seq_ord,
				case when indicator_sequencia in (44, 132, 51, 127, 144) then 'N' else 'S' end ie_disabled
		  from (select	indicator_sequencia
				  from	pfcs_telemetry_v
				 where	indicator_sequencia not in (select	indicator_sequencia
													  from	pfcs_telemetry_v
													 where	cd_estabelecimento = cd_estabelecimento_p
													   and	indicator_sequencia in (144,44,41,129,127,160,51,128,132)
													group by indicator_sequencia)
				group by indicator_sequencia
				union all
				select	indicator_sequencia
				  from	pfcs_telemetry_v
				 where	cd_estabelecimento = cd_estabelecimento_p
				   and	indicator_sequencia in (144,44,129,127,160,51,132)
				group by indicator_sequencia
				union
				select	indicator_sequencia
				  from	pfcs_telemetry_v
				 where	cd_estabelecimento = cd_estabelecimento_p
				   and	indicator_sequencia in (41, 128)
				   and	vl_indicator > 0
				group by indicator_sequencia)
		where	indicator_sequencia in (144,44,41,129,127,160,51,128,132)
		group by indicator_sequencia
		union all
        select	indi.nr_sequencia,
				to_number(pfcs_get_indicator_rule(indi.nr_sequencia, 0, cd_estabelecimento_p, 'ORDER')) nr_seq_ord,
				'S' ie_disabled
		  from	pfcs_indicator indi,
				pfcs_indicator_rule pfr
		where indi.nr_sequencia = pfr.nr_seq_indicator
			and indi.ie_classification = 'TL'
			and pfcs_get_indicator_rule(indi.nr_sequencia, 0, cd_estabelecimento_p, 'SPACE') = 'S'
		order by nr_seq_ord asc, 1);

begin
	for c01 in cur_get_order_telemetry
	loop
		if (c01.indicator_sequencia = nr_seq_indicator_p and c01.ie_disabled = 'N') then
			nr_rownum_w := c01.nr_rownum - 1;
			exit;		
		end if;	
	end loop;

	return nr_rownum_w;
	
	exception
		-- If there are no records found
		when no_data_found then
		  return 0;

end pfcs_get_order_telemetry;
/
