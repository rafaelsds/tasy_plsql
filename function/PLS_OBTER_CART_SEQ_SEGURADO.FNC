Create or replace
function pls_obter_cart_seq_segurado
				(cd_usuario_plano_p		varchar,
				nr_seq_segurado_p		number)
				return varchar2 is

ds_retorno_w		varchar2(30) := null;
nr_seq_segurado_w	number(10);
cd_pessoa_fisica_w	varchar2(10);

begin
if	(cd_usuario_plano_p is not null) then
	select	max(nr_seq_segurado)
	into	ds_retorno_w
	from	pls_segurado_carteira
	where	cd_usuario_plano = cd_usuario_plano_p;
elsif	(nr_seq_segurado_p is not null) then
	select	max(cd_usuario_plano)
	into	ds_retorno_w
	from	pls_segurado_carteira
	where	nr_seq_segurado = nr_seq_segurado_p;
end if;

return ds_retorno_w;

end pls_obter_cart_seq_segurado;
/