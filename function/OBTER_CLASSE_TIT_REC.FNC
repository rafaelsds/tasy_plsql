create or replace
function obter_classe_tit_rec(	nr_seq_classe_p number,
				nr_sequencia_p number)
			return varchar2 is

ds_retorno_w varchar(255);
			
begin

select		b.ds_classe ds
into		ds_retorno_w
from		classe_titulo_receber b,
		dmed_regra_classe_tit a 
where		b.nr_sequencia 	= a.nr_seq_classe
and 		a.nr_seq_classe = nr_seq_classe_p
and 		a.nr_sequencia 	= nr_sequencia_p
order by	1;

return ds_retorno_w;

end obter_classe_tit_rec;
/
