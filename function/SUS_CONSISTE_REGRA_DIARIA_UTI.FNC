create or replace
function Sus_Consiste_Regra_Diaria_Uti(nr_interno_conta_p		Number)
 		    	return Varchar2 is

ie_inconsistente_w	Varchar2(1)	:= 'N';
cd_estabelecimento_w	Number(4);
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(10);
qt_procedimento_w	Number(10);
dt_procedimento_w	Date;
dt_proced_fim_mes_w	Date;
qt_proc_perm_w		Number(10);
qt_dias_mes_w		Number(10);
qt_proc_diaria_uti_w	Number(10);
ds_retorno_w		Varchar2(1);
			
Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		qt_diaria_uti
	from	sus_regra_diaria_uti_mes
	where	cd_estabelecimento	= cd_estabelecimento_w;

Cursor C02 is
	select	nvl(sum(qt_procedimento),0),
		establishment_timezone_utils.startofmonth(dt_procedimento)
	from	procedimento_paciente
	where	nr_interno_conta	= nr_interno_conta_p
	and	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w
	and	cd_motivo_exc_conta is null
	group by establishment_timezone_utils.startofmonth(dt_procedimento);
	
begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

open C01;
loop
fetch C01 into	
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_proc_perm_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		qt_procedimento_w,
		dt_procedimento_w;
	exit when C02%notfound;
		begin
		
		dt_proced_fim_mes_w := last_day(dt_procedimento_w);
		
		select	sum(qt_procedimento)
		into	qt_proc_diaria_uti_w
		from	procedimento_paciente
		where	cd_motivo_exc_conta is null
		and	dt_procedimento	between dt_procedimento_w and dt_proced_fim_mes_w
		and	cd_procedimento			= cd_procedimento_w
		and	ie_origem_proced		= ie_origem_proced_w;
		
		select	to_char(last_day(dt_procedimento_w),'dd')
		into	qt_dias_mes_w
		from	dual;
		
		if	(qt_proc_diaria_uti_w > (qt_proc_perm_w * qt_dias_mes_w)) then
			begin
			ie_inconsistente_w	:= 'S';
			end;
		end if;
		
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

ds_retorno_w	:= ie_inconsistente_w;

return	ds_retorno_w;

end Sus_Consiste_Regra_Diaria_Uti;
/
