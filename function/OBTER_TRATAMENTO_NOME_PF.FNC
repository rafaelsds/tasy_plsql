create or replace
function obter_tratamento_nome_pf(cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
qt_idade_w	number(10);
ds_profissao_w	varchar2(80);
nm_pessoa_w	varchar2(60);

begin
if	(cd_pessoa_fisica_p is not null) then

	select	to_number(obter_idade(dt_nascimento, nvl(dt_obito,sysdate),'A')),
		substr(obter_desc_profissao(b.cd_profissao),1,255),
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255)
	into	qt_idade_w,
		ds_profissao_w,
		nm_pessoa_w
	from	pessoa_fisica a,
		compl_pessoa_fisica b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	b.ie_tipo_complemento = '1';

	ds_retorno_w	:= nm_pessoa_w;
	if	(qt_idade_w >= 18) then
		ds_retorno_w	:= 'Sr(a). '||nm_pessoa_w;
		if	(upper(sem_acento(ds_profissao_w)) like upper('%Medico%')) then
			ds_retorno_w	:= 'Dr(a). '||nm_pessoa_w;
		end if;
		if	(upper(sem_acento(ds_profissao_w)) like upper('%Advogado%')) then
			ds_retorno_w	:= 'Sr(a). '||nm_pessoa_w;
		end if;
		if	(upper(sem_acento(ds_profissao_w)) like upper('%Professor%')) then
			ds_retorno_w	:= 'Professor '||nm_pessoa_w;
		end if;
	else
		ds_retorno_w	:= 'Menor '||nm_pessoa_w;
	end if;

end if;

return	ds_retorno_w;

end obter_tratamento_nome_pf;
/
