create or replace
function obter_valor_venc_oc_mes(	nr_ordem_compra_p		number,
				dt_mesano_referencia_p		date)
 		    	return number is

nr_ordem_compra_w		number(10);
vl_vencimento_w			number(15,4);	

begin

select	nvl(sum(d.vl_vencimento),0)
into	vl_vencimento_w
from	ordem_compra a,
	ordem_compra_item b,
	local_estoque c,
	ordem_compra_venc d
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_ordem_compra = d.nr_ordem_compra
and	b.cd_local_estoque = c.cd_local_estoque
and	a.nr_ordem_compra = nr_ordem_compra_p
and	c.ie_tipo_local = 8
and	trunc(d.dt_vencimento,'mm') = trunc(dt_mesano_referencia_p,'mm');
	
return	vl_vencimento_w;

end obter_valor_venc_oc_mes;
/