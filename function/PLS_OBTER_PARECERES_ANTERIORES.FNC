create or replace
function pls_obter_pareceres_anteriores( nr_seq_conta_p			pls_conta.nr_sequencia%type,
										 nr_seq_conta_recurso_p	pls_rec_glosa_conta.nr_sequencia%type
										) return varchar2 is 

ds_retorno_w	varchar2(4000);

Cursor C01 is
	select	b.nr_seq_conta,
			b.nr_sequencia nr_seq_conta_recurso,
			a.ds_parecer,
			to_char(a.dt_atualizacao,'dd/mm/yyyy hh24:mi:ss') dt_parecer
	from   	pls_analise_parecer_rec a,
			pls_rec_glosa_conta b
	where  	a.nr_seq_conta_rec = b.nr_sequencia
	and    	b.nr_seq_conta = nr_seq_conta_p
	and		b.nr_sequencia <> nr_seq_conta_recurso_p
	order by a.dt_atualizacao desc;

begin

	ds_retorno_w := '';
	for r_c01_w in C01 loop
	
		ds_retorno_w := ds_retorno_w ||'Data parecer: '||r_c01_w.dt_parecer || ' Conta : '||r_c01_w.nr_seq_conta||' - Conta recurso: '||
						r_c01_w.nr_seq_conta_recurso||'. Parecer: '||r_c01_w.ds_parecer ||chr(13)||chr(13);
	
	end loop;
	

return ds_retorno_w;

end pls_obter_pareceres_anteriores;
/