create or replace
function pls_obter_se_necessario_glosar
			(	qt_liberada_p			number,
				qt_apresentada_p		number)
							return varchar2 is
/*
	CHAMADA NO FONTE DA CORPLS_AC 
*/
vl_retorno_w		varchar2(1):= 'S'; --Por padr�o sempre ser� necess�rio glosar	
							
begin

/*VERIFICA QTDE GLOSA SE ZERADA*/
if	(nvl(qt_liberada_p,0) > 0) and (nvl(qt_apresentada_p,0) > 0) and
	(qt_liberada_p = qt_apresentada_p) then	
	
	vl_retorno_w	:= 'N';	-- Se qtde glossa zerado , ent�o n�o precisa glosar.
	
end if;

return	vl_retorno_w;

end pls_obter_se_necessario_glosar;
/