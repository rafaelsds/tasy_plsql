create or replace
function obter_se_exibe_sv_apap( ie_mediana_p         in varchar2,
                                 ie_mediana_sv_p      in varchar2,
                                 ie_mediana_1_min_p	in varchar2,
                                 ie_mediana_5_min_p	in varchar2,
                                 ie_mediana_10_min_p	in varchar2,
                                 ie_mediana_15_min_p	in varchar2) return varchar2 is

ds_retorno_w   varchar2(1) := 'N';
begin
if (nvl(ie_mediana_p,'N') = 'N') and (ie_mediana_sv_p is null) then
   ds_retorno_w := 'S';
elsif (nvl(ie_mediana_p,'N') in ('S','C')) then
   if (ie_mediana_p = 'C') and (ie_mediana_sv_p is null) then
      ds_retorno_w := 'S';
   elsif (ie_mediana_sv_p = '1') and (nvl(ie_mediana_1_min_p,'N') = 'S') then
      ds_retorno_w := 'S';
   elsif (ie_mediana_sv_p = '5') and (nvl(ie_mediana_5_min_p,'N') = 'S') then   
      ds_retorno_w := 'S';
   elsif (ie_mediana_sv_p = '10') and (nvl(ie_mediana_10_min_p,'N') = 'S') then   
      ds_retorno_w := 'S';
   elsif (ie_mediana_sv_p = '15') and (nvl(ie_mediana_15_min_p,'N') = 'S') then   
      ds_retorno_w := 'S';
   end if;
end if;   

return   ds_retorno_w;

end obter_se_exibe_sv_apap;
/