create or replace
function obter_se_exibe_equip(cd_equipamento_p		varchar2)
				return varchar2 is
ie_exibe_equip_w  		varchar2(1);
ie_filtra_estabelecimento_w	varchar2(1);
begin

Obter_Param_Usuario(873,26,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_filtra_estabelecimento_w);

if 	(ie_filtra_estabelecimento_w = 'S') then 
	select 	nvl(max('S'),'N')
	into	ie_exibe_equip_w
	from	equipamento
	where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and 	cd_equipamento = cd_equipamento_p
	and 	ie_situacao = 'A';
else 
	ie_exibe_equip_w := 'S';
end if;

return	ie_exibe_equip_w;

end obter_se_exibe_equip;
/