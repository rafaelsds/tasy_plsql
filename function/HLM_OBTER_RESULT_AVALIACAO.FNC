create or replace
function HLM_Obter_Result_Avaliacao(
					nr_seq_avaliacao_p		NUMBER,
					nr_seq_item_p			NUMBER)
					return VARCHAR2 is
/*
  B - Booleano
  D - Descri��o
  M - Multisele��o
  S - Sele��o Simples
  O - Valor Dom�nio
  C - Sele��o Simples
  L - C�lculo
  T - T�tulo
  V - Valor
  U - Sele��o Simples (Radio Group);
*/

type ValorComplemento is record (	ds_valor	varchar2(2000));
type Dados is table of ValorComplemento index by binary_integer;

ds_resultado_w			VARCHAR2(4000);
qt_resultado_w			NUMBER(15,4);
ie_resultado_w			VARCHAR2(02);
cd_funcao_w			NUMBER(05,0) := '';
cd_dominio_w			NUMBER(5);
ds_complemento_w			VARCHAR2(4000);
ds_retorno_w			varchar2(4000);

i				INTEGER;
ds_texto_w			VARCHAR2(255);
pos_w				number(10);

complemento_w			Dados;
ds_comando_w			varchar2(2000);

begin

select		nvl(MAX(a.cd_funcao), 0)
into		cd_funcao_w
from		med_item_avaliar b,
		med_tipo_avaliacao a
where		a.nr_sequencia	= b.nr_seq_tipo
and		b.nr_sequencia	= nr_seq_item_p;

ds_resultado_w			:= '';

select	MAX(ie_resultado),
	MAX(cd_dominio),
	MAX(ds_complemento) || ';'
into 	ie_resultado_w,
	cd_dominio_w,
	ds_complemento_w
from 	med_item_avaliar
where nr_sequencia	= nr_seq_item_p;

if	(cd_funcao_w	= 2000) then
	begin
	select		MAX(qt_resultado),
			MAX(ds_resultado)
	into 		qt_resultado_w,
			ds_resultado_w
	from		sac_pesquisa_result
	where		nr_seq_pesquisa	= nr_seq_avaliacao_p
	and		nr_seq_item	= nr_seq_item_p;
	end;
elsif	(cd_funcao_w	= 4000) then /* Gest�o da Qualidade */
	begin
	select		MAX(qt_resultado),
			MAX(ds_resultado)
	into 		qt_resultado_w,
			ds_resultado_w
	from		qua_avaliacao_result
	where		nr_seq_avaliacao	= nr_seq_avaliacao_p
	and		nr_seq_item	= nr_seq_item_p;
	end;
elsif	(cd_funcao_w	= 299) then /* Ordem de servi�o */
	begin
	select		MAX(qt_resultado),
			MAX(ds_resultado)
	into 		qt_resultado_w,
			ds_resultado_w
	from		man_ordem_serv_aval_result
	where		nr_seq_ordem_serv_aval	= nr_seq_avaliacao_p
	and		nr_seq_item		= nr_seq_item_p;
	end;
elsif	(cd_funcao_w	= 230) then /* Cadastro de Funcion�rios */
	begin
	select		MAX(qt_resultado),
			MAX(ds_resultado)
	into		qt_resultado_w,
			ds_resultado_w
	from		pessoa_avaliacao_result
	where		nr_seq_avaliacao	= nr_seq_avaliacao_p
	and		nr_seq_item	= nr_seq_item_p;
	end;
else
	begin
	select		MAX(qt_resultado),
			MAX(ds_resultado)
	into 		qt_resultado_w,
			ds_resultado_w
	from		med_avaliacao_result
	where		nr_seq_avaliacao	= nr_seq_avaliacao_p
	and		nr_seq_item	= nr_seq_item_p;
	end;
end if;

if	(ie_resultado_w = 'B') then
	begin
	if	(nvl(qt_resultado_w,0) = 0) then
		ds_resultado_w	:= 'N';
	else
		ds_resultado_w	:= 'S';
	end if;
	end;
elsif	(ie_resultado_w = 'D') or
	(ie_resultado_w = 'C') then
	begin
	ds_resultado_w	:= ds_resultado_w;
	end;
elsif	(ie_resultado_w = 'V') then
	ds_resultado_w	:= qt_resultado_w;
elsif	(ie_resultado_w = 'U') then	/*Elemar e Philippe OS 186211*/
	
	if	(cd_dominio_w is null) then
		for i in 0..100 loop
			ds_texto_w		:= substr(ds_complemento_w, 1, instr(ds_complemento_w,';') - 1);
			ds_complemento_w        := replace(ds_complemento_w, ds_texto_w || ';', '');
			if	(i = qt_resultado_w) then
				ds_resultado_w	:= ds_texto_w;
				exit;
			end if;
		end loop;
	else
		
		if	(ds_resultado_w	is null) then
			select	nvl(max(ds_valor_dominio),'')
			into	ds_resultado_w
			from	med_valor_dominio
			where	nr_seq_dominio = cd_dominio_w
			and	to_number(vl_dominio) = qt_resultado_w;
		  
		end if;
	end if;
	if	(nvl(ds_resultado_w,'') = '') then
		ds_resultado_w	:= qt_resultado_w;
	end if;
elsif	(ie_resultado_w = 'L') then
	begin
	--ds_resultado_w	:= to_char(qt_resultado_w, '999999999999999.9999');
	ds_resultado_w	:= Obter_Valor_Formula_WAvaliar( nr_seq_avaliacao_p, nr_seq_item_p, '@SEQ', 'M');
	end;
elsif	(ie_resultado_w = 'O') then
	begin
	if	(qt_resultado_w = 0) or
		(qt_resultado_w is null) then
		ds_resultado_w	:= ds_resultado_w;
	else
		ds_resultado_w	:= qt_resultado_w;
	end if;
	end;
elsif	(ie_resultado_w = 'S') then
	begin
		if (cd_dominio_w is NOT null)	then
			begin
			if	(qt_resultado_w = 0) or
				(qt_resultado_w is null) then
				ds_resultado_w	:= ds_resultado_w;
			else
				ds_resultado_w	:= qt_resultado_w;
			end if;
			end;
		else
			begin
				select	MAX(ds_resultado)
				into		ds_resultado_w
				from		MED_ITEM_AVALIAR_RES
				where		nr_seq_item = nr_seq_item_p
				and		nr_seq_res  = qt_resultado_w;
			end;
		end if;
	end;		
elsif	(ie_resultado_w	= 'Z') then
	begin
	
	pos_w	:= instr(ds_complemento_w,';');
	
	i	:= 0;
	while (pos_w	> 0) loop 
		begin
		
		complemento_w(i).ds_valor	:= substr(ds_complemento_w,1,pos_w-1);
		
		ds_complemento_w		:= substr(ds_complemento_w,pos_w+1,2000);
		
		i	:= i+1;
		
		pos_w	:= instr(ds_complemento_w,';');
		end;
	end loop;
	
	if	(complemento_w.count	>= 8)then
		
		ds_comando_w	:= 'select	'||complemento_w(8).ds_valor||
				    ' from	'||complemento_w(6).ds_valor||
				    ' where	'||complemento_w(7).ds_valor  ||' = :ds_result';
				    
		ds_retorno_w	:=	Obter_select_concatenado_bv(	ds_comando_w,
									'ds_result='||ds_resultado_w||';','');

		if	(ds_retorno_w	is not null)then
			ds_resultado_w	:= ds_resultado_w||' - '||ds_retorno_w;
			
		end if;
	
	end if;

	end;
end if;

return ds_resultado_w;

end HLM_Obter_Result_Avaliacao;
/
