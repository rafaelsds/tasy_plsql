create or replace function OBTER_DIAS_NORM_GLICOSE(nr_atendimento_p number, dt_inicio date, dt_fim date)
  return number is
  QTD_DIAS_V number;
begin
  SELECT SUM(OBTER_DIAS_ENTRE_DATAS(MIN(T.DT_CONTROLE), DT_ALTERACAO)) DIAS
  INTO QTD_DIAS_V     
  FROM (SELECT R.*,
               DECODE(DS_RESULTADO, 'Glucose Compliance', 1, 0) COMPLIANCE,
               DECODE(DS_RESULTADO,
                      'Glucose Compliance',
                      (SELECT MIN(DT_CONTROLE)
                         FROM REL_RESULTADO_GLICEMIA_V A
                        WHERE A.NR_ATENDIMENTO = R.NR_ATENDIMENTO
                          AND A.DS_RESULTADO <> R.DS_RESULTADO
                          AND A.DT_CONTROLE BETWEEN dt_inicio AND FIM_DIA(dt_fim)
                          AND A.NR_SEQ_PROTOCOLO = 1302
                          AND A.DT_CONTROLE > R.DT_CONTROLE),
                      (SELECT MIN(DT_CONTROLE)
                         FROM REL_RESULTADO_GLICEMIA_V C
                        WHERE C.NR_ATENDIMENTO = R.NR_ATENDIMENTO
                          AND C.DS_RESULTADO = 'Glucose Compliance'
                          AND C.DT_CONTROLE BETWEEN dt_inicio AND FIM_DIA(dt_fim)
                          AND C.NR_SEQ_PROTOCOLO = 1302
                          AND C.DT_CONTROLE > R.DT_CONTROLE)) DT_ALTERACAO
          FROM REL_RESULTADO_GLICEMIA_V R
         WHERE NR_ATENDIMENTO = nr_atendimento_p
         AND R.DT_CONTROLE BETWEEN dt_inicio AND FIM_DIA(dt_fim)
         AND R.NR_SEQ_PROTOCOLO = 1302
         ORDER BY R.DT_CONTROLE
         ) T
 GROUP BY T.NR_ATENDIMENTO, T.CD_PESSOA_FISICA, DT_ALTERACAO, COMPLIANCE
 ORDER BY 1;
  
  return QTD_DIAS_V;

end OBTER_DIAS_NORM_GLICOSE;
/
