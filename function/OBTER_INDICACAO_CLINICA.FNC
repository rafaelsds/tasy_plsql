create or replace
function Obter_indicacao_clinica (nr_atendimento_p	Number,
				  cd_procedimento_p	Number,
				  ie_origem_proced_p	Number)
					return varchar2 is

ds_retorno_w	varchar2(2000):= ' ';

begin

select 	max(nvl(ds_indicacao,' '))
into	ds_retorno_w
from 	autorizacao_convenio
where 	nr_atendimento 			= nr_atendimento_p
and 	cd_procedimento_principal 	= cd_procedimento_p
and 	ie_origem_proced		= ie_origem_proced_p;

return ds_retorno_w;

end Obter_indicacao_clinica;
/