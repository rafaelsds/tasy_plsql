create or replace
function EHR_Obter_Tipo_Comp_Elem(	nr_seq_elemento_p	Number,
					ie_opcao_p		Varchar2)
				return varchar2 is

ds_retorno_w		varchar2(255);
ie_componente_w		varchar2(5);
	
begin

if	(nr_seq_elemento_p > 0) then
	begin
	select	ie_componente
	into	ie_componente_w
	from	ehr_elemento
	where	nr_sequencia	= nr_seq_elemento_p;
	
	ds_retorno_w	:= ie_componente_w;
	
	if	(ie_opcao_p = 'D') then
		select	ds_valor_dominio
		into	ds_retorno_w
		from	valor_dominio
		where	cd_dominio	= 1043
		and	vl_dominio	= ie_componente_w;
	end if;
	end;
end if;

return	ds_retorno_w;

end EHR_Obter_Tipo_Comp_Elem;
/