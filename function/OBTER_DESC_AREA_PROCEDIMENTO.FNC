create or replace
function obter_desc_area_procedimento (
				cd_area_procedimento_p	number)
				return varchar2 is
				
ds_area_procedimento_w	varchar2(40);
				
begin
if	(cd_area_procedimento_p is not null) then

	select	max(ds_area_procedimento)
	into	ds_area_procedimento_w
	from	area_procedimento
	where	cd_area_procedimento = cd_area_procedimento_p;

end if;

return ds_area_procedimento_w;

end obter_desc_area_procedimento;
/