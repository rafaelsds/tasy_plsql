create or replace function bsc_ind_satisf_suporte(	dt_inicial_p	date,
				dt_final_p		date)
 		    	return number is

qt_retorno_w		number(15,2);
dt_inicial_w		date;
dt_final_w			date;
qt_total_os_w		number(10);

begin

dt_inicial_w	:= dt_inicial_p;
dt_final_w		:= dt_final_p;

select	count(*)
into	qt_total_os_w
from	os_encer_satisf_gerencia_sup_v
where nvl(ie_situacao, 'A') = 'A'
and nr_seq_gerencia_sup in (5,58)
and dt_fim_real between dt_inicial_w and PKG_DATE_UTILS.END_OF(dt_final_w, 'DAY');

select	count(*)
into	qt_retorno_w
from	os_insatisfacao_gerencia_sup_v
where nvl(ie_situacao, 'A') = 'A'
and nr_seq_gerencia_sup in (5,58) 
and dt_fim_real between dt_inicial_w and PKG_DATE_UTILS.END_OF(dt_final_w, 'DAY');

qt_retorno_w	:= 100 - dividir(qt_retorno_w * 100, qt_total_os_w);

return	qt_retorno_w;

end bsc_ind_satisf_suporte;
/