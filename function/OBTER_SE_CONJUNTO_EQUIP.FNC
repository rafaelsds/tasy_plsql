create or replace
function obter_se_conjunto_equip(	nr_seq_conjunto_p		number,
					nr_seq_equipamento_p			number)
 		    	return varchar2 is

nr_seq_equipamento_w	number(10);
ds_retorno_w		varchar2(1) := 'S';
qt_conjuntos_w	number(10);
			
begin
	
select 	count(*)
into	qt_conjuntos_w
from	cm_equip_conjunto
where	nr_seq_conjunto = nr_seq_conjunto_p;

if	(qt_conjuntos_w > 0) then
	select 	count(*)
	into	qt_conjuntos_w
	from	cm_equip_conjunto
	where	nr_seq_equipamento = nr_seq_equipamento_p
	and	nr_seq_conjunto = nr_seq_conjunto_p;	
	
	if	(qt_conjuntos_w = 0)  then
		ds_retorno_w := 'N';
	end if;
end if;

return	ds_retorno_w;

end obter_se_conjunto_equip;
/