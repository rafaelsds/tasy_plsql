create or replace
function qua_obter_resp_analise_probl(	nr_seq_problema_p		number,
				cd_estabelecimento_p	number,
				ie_opcao_p		varchar2)
			return varchar2 is

qt_total_w		number(10,0);
ds_retorno_w		varchar2(255);
ds_resposta_w		varchar2(255);
ie_abrir_rnc_w		varchar2(15);
nr_seq_resposta_w		number(10,0);

/*
ie_opcao_p
R - Resposta
A - Abrir RNC - Dom2582
*/

begin

select	qua_obter_total_analise_probl(nr_seq_problema_p)
into	qt_total_w
from	dual;

select	max(nr_sequencia)
into	nr_seq_resposta_w
from	qua_resposta_problema
where	vl_resposta_min <= qt_total_w
and	vl_resposta_max >= qt_total_w
and	cd_estabelecimento = cd_estabelecimento_p;

select	ds_resposta,
	ie_abrir_rnc
into	ds_resposta_w,
	ie_abrir_rnc_w
from	qua_resposta_problema
where	nr_sequencia = nr_seq_resposta_w;

if	(ie_opcao_p = 'R') then
	ds_retorno_w := ds_resposta_w;
elsif	(ie_opcao_p = 'A') then
	ds_retorno_w := ie_abrir_rnc_w;
end if;

return	ds_retorno_w;

end qua_obter_resp_analise_probl;
/
