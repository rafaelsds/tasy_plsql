create or replace 
function sus_obter_tiporeg_bpa
		(	cd_procedimento_p	number,
			ie_origem_proced_p	number,
			ie_tipo_retorno_p	varchar2,
			dt_procedimento_p	date default sysdate)
			return varchar2 is


/* IE_TIPO_RETORNO_P
	- C c�digo do registro;
	- D descri��o do registro;
*/


cd_registro_w		Number(2) := 0;
ds_retorno_w		Varchar2(30);
qt_regra_bpa_w		number(10);
qt_hab_bpa_w		number(10);
qt_serv_bpa_w		number(10);
cd_estab_usuario_w	number(5) := 0;

BEGIN

begin
cd_estab_usuario_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
exception
when others then
	cd_estab_usuario_w := 0;
end;

if	(Sus_Obter_Se_Detalhe_Proc(cd_procedimento_p,ie_origem_proced_p,'020',dt_procedimento_p) > 0) then
	begin
	
	select	count(1)
	into	qt_regra_bpa_w
	from	sus_procedimento a
	where	a.cd_procedimento	= cd_procedimento_p
	and	a.ie_origem_proced	= ie_origem_proced_p
	and	exists (select	1
			from	sus_procedimento_registro x
			where	x.cd_procedimento	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced
			and	x.cd_registro 		= 1)
	and	exists (select	1
			from	sus_procedimento_registro x
			where	x.cd_procedimento	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced
			and	x.cd_registro 		= 2)
	and	rownum = 1;
			
	if	(qt_regra_bpa_w > 0) then
		begin		
		
		select	count(1)
		into	qt_hab_bpa_w
		from	sus_habilitacao_hospital
		where	cd_estabelecimento = cd_estab_usuario_w
		and	cd_habilitacao in (403,404,405)
		and	rownum = 1;
		
		select 	count(1)
		into	qt_serv_bpa_w
		from	sus_serv_classif_hosp x,
			sus_servico_hospital y
		where	x.nr_seq_serv_classif = 6761
		and	y.nr_seq_servico = 1351
		and	x.nr_seq_serv_hosp = y.nr_sequencia
		and	rownum = 1;
		
		if	(qt_hab_bpa_w > 0) and
			(qt_serv_bpa_w > 0) then
			cd_registro_w := 2;				
		else
			cd_registro_w := 1;				
		end if;
		
		end;
	else
		begin
		
		begin
		select	cd_registro
		into	cd_registro_w
		from	sus_proc_registro_bpa
		where	cd_procedimento = cd_procedimento_p
		and	ie_origem_proced = ie_origem_proced_p
		and	nvl(cd_estabelecimento,cd_estab_usuario_w) = cd_estab_usuario_w;
		exception
		when others then
			cd_registro_w := 0;
		end;
		
		if	(nvl(cd_registro_w,0) = 0) then
			begin
			
			select	nvl(max(cd_registro),0)
			into	cd_registro_w
			from	sus_procedimento_registro
			where	cd_procedimento		= cd_procedimento_p
			and	ie_origem_proced	= ie_origem_proced_p
			and	cd_registro		in (1,2);
			
			end;
		end if;
		
		end;
	end if;
	
	end;
else
	begin
	
	select	count(1)
	into	qt_regra_bpa_w
	from	sus_procedimento a
	where	a.cd_procedimento	= cd_procedimento_p
	and	a.ie_origem_proced	= ie_origem_proced_p
	and	exists (select	1
			from	sus_procedimento_registro x
			where	x.cd_procedimento	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced
			and	x.cd_registro 		= 1)
	and	exists (select	1
			from	sus_procedimento_registro x
			where	x.cd_procedimento	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced
			and	x.cd_registro 		= 2)
	and	rownum = 1;
	
	if	(qt_regra_bpa_w > 0) then
		begin
		
		begin
		select	cd_registro
		into	cd_registro_w
		from	sus_proc_registro_bpa
		where	cd_procedimento = cd_procedimento_p
		and	ie_origem_proced = ie_origem_proced_p
		and	nvl(cd_estabelecimento,cd_estab_usuario_w) = cd_estab_usuario_w;
		exception
		when others then
			cd_registro_w := 0;
		end;
		
		
		end;
	end if;
	
	if	(nvl(cd_registro_w,0) = 0) then
		begin
		
		select	nvl(max(cd_registro),0)
		into	cd_registro_w
		from	sus_procedimento_registro
			where	cd_procedimento		= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p
		and	cd_registro		in (1,2);
		
		end;
	end if;
	
	end;
end if;

if	(ie_tipo_retorno_p	= 'D') then
	select	nvl(max(ds_registro),'')
	into	ds_retorno_w
	from	sus_registro
	where	cd_registro	= cd_registro_w;
elsif	(ie_tipo_retorno_p	= 'C') then
	ds_retorno_w		:= cd_registro_w;
end if;

return	ds_retorno_w;

end sus_obter_tiporeg_bpa;
/
