create or replace
function obter_lanc_mens_negociacao(nr_seq_negociacao_mens_p	number)
						return varchar2 is
						
ds_retorno_w		varchar2(4000)	:= null;
nr_seq_lancamento_w	number(10);

Cursor c01 is
select	a.nr_sequencia
from	pls_lancamento_mensalidade a
where	a.nr_seq_negociacao_mens	= nr_seq_negociacao_mens_p
union all
select	a.nr_sequencia
from	pls_segurado_mensalidade a
where	a.nr_seq_negociacao_mens	= nr_seq_negociacao_mens_p;

begin

if	(nr_seq_negociacao_mens_p is not null) then
	open c01;
	loop
	fetch c01 into	
		nr_seq_lancamento_w;
	exit when c01%notfound;
		begin
		if	(ds_retorno_w is null) then
			ds_retorno_w	:= ds_retorno_w || nr_seq_lancamento_w;
		else
			ds_retorno_w	:= ds_retorno_w || ', ' || nr_seq_lancamento_w;
		end if;
		end;
	end loop;
	close c01;
end if;

return	ds_retorno_w;

end obter_lanc_mens_negociacao;
/