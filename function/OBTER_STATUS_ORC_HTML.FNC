create or replace
function obter_status_orc_html( nr_seq_tabela_p		number,
				ie_meses_anteriores_p 	varchar2,
				ie_atualiza_custo_p	varchar2,
				cd_estabelecimento_p	number,
				ie_verif_p		number)
 		    	return varchar2 is

dt_mes_referencia_w	date;
dt_referencia_w		date;
ie_retorno_w 		varchar2(1) := 'N';
ie_resultado_w		varchar2(1) := 'N';

begin

select 	dt_mes_referencia
into	dt_mes_referencia_w
from 	tabela_custo
where 	nr_sequencia = nr_seq_tabela_p;

if (nr_seq_tabela_p is not null
	and dt_mes_referencia_w is not null) then

	select	max(dt_referencia)
	into	dt_referencia_w
	from 	parametro_custo y
	where 	cd_estabelecimento = cd_estabelecimento_p;
	
	if ((dt_referencia_w is not null and dt_referencia_w > dt_mes_referencia_w and 
		ie_meses_anteriores_p <> 'S') or ie_atualiza_custo_p <> 'S') then
		ie_resultado_w := 'S';
	else
		ie_resultado_w := 'N';
	end if;
	
	if (ie_verif_p = 1) then
		ie_retorno_w := ie_resultado_w;
	else
		if (ie_resultado_w = 'N') then
			ie_retorno_w := 'S';
		else
			ie_retorno_w := 'N';
		end if;
	end if;
else
	if (ie_verif_p = 1) then
		ie_retorno_w := 'S';
	else
		ie_retorno_w := 'N';
	end if;
end if;

return	ie_retorno_w;

end obter_status_orc_html;
/