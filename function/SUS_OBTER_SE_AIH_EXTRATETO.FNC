create or replace
function sus_obter_se_aih_extrateto(	nr_aih_p	number )
				return varchar2 as

ie_retorno_w			varchar2(15) := 'N';
					
begin

select 	nvl(max('S'),'N')
into	ie_retorno_w
from	dual
where	substr(nr_aih_p,5,1) = '5';

return ie_retorno_w;

end sus_obter_se_aih_extrateto;
/