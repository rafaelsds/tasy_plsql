create or replace
function obter_se_solucao_instalada	(nr_prescricao_p	number,
				nr_seq_solucao_p	number,
				nr_etapa_p	number)
				return varchar2 is

ie_instalada_w	varchar2(1);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) and
	(nr_etapa_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ie_instalada_w
	from	adep_processo
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_solucao	= nr_seq_solucao_p
	and	nr_etapa		= nr_etapa_p
	and	ie_status_processo	= 'A';

end if;

return ie_instalada_w;

end obter_se_solucao_instalada;
/