create or replace
function obter_dados_estat_apae(	nr_seq_dado_p	number,
					ie_opacao_p	varchar2)
					return varchar2 is 

ds_retorno_w	varchar2(255);

begin

if	(ie_opacao_p = 'DS') then
	select	ds_dado
	into	ds_retorno_w
	from	apa_dado_estat
	where	nr_sequencia	=	nr_seq_dado_p;
elsif	(ie_opacao_p = 'UN') then
	select	ds_unidade
	into	ds_retorno_w
	from	apa_dado_estat
	where	nr_sequencia	=	nr_seq_dado_p;
end if;

return	ds_retorno_w;

end obter_dados_estat_apae;
/