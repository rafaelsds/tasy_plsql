create or replace
function sip_obter_estrutura_proc
		(	cd_procedimento_p	Number,
			ie_origem_proced_p	Number,
			ie_tipo_retorno_p	Varchar2)
 		    	return Varchar2 is
			
/* IE_TIPO_RETORNO_P
	S - Sequencia da estrutura
	C - C�digo da estrutura
	D - Descri��o da estrutura
*/			
			
ds_retorno_w		Varchar2(80);
nr_seq_estrutura_w	Number(10);
cd_estrutura_w		Varchar2(40);
ds_estrutura_w		Varchar2(80);
cd_area_w		Number(15)	:= 0;
cd_especialidade_w	Number(15)	:= 0;
cd_grupo_w		Number(15)	:= 0;

begin

/* Obter Estrutura do procedimento */
begin
select	cd_grupo_proc,
	cd_especialidade,
	cd_area_procedimento
into	cd_grupo_w,
	cd_especialidade_w,
	cd_area_w
from	estrutura_procedimento_v
where	cd_procedimento 	= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
exception
     	when others then
	begin
	cd_grupo_w		:= 0;
	cd_especialidade_w	:= 0;
	cd_area_w		:= 0;
	end;
end;

begin
select	b.nr_sequencia,
	b.cd_estrutura,
	b.ds_estrutura
into	nr_seq_estrutura_w,
	cd_estrutura_w,
	ds_estrutura_w
from	sip_estrutura_proc	b,
	sip_procedimento	a
where	a.nr_seq_estrutura	= b.nr_sequencia
and	nvl(cd_procedimento,cd_procedimento_p) 		= cd_procedimento_p
and	nvl(ie_origem_proced,ie_origem_proced_p)	= ie_origem_proced_p
and	nvl(cd_area_procedimento,cd_area_w)		= cd_area_w	
and	nvl(cd_especialidade,cd_especialidade_w)	= cd_especialidade_w
and	nvl(cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
and	a.ie_estrutura		= 'S'
and	b.ie_situacao		= 'A';
exception
	when others then
	nr_seq_estrutura_w	:= 0;
	cd_estrutura_w		:= '';
	ds_estrutura_w		:= '';
end;

if	(ie_tipo_retorno_p	= 'S') then
	ds_retorno_w	:= nr_seq_estrutura_w;
elsif	(ie_tipo_retorno_p	= 'C') then
	ds_retorno_w	:= cd_estrutura_w;
elsif	(ie_tipo_retorno_p	= 'D') then
	ds_retorno_w	:= ds_estrutura_w;
end if;

return	ds_retorno_w;

end sip_obter_estrutura_proc;
/