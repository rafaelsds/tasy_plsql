create or replace
function obter_exibe_atrib_diag(nr_seq_episodio_p        	number,
				ds_atributo_p             	varchar2,
				cd_perfil_p			number)
                return varchar2 is

ds_resultado_w            varchar2(1);            
ds_atrib_dominio_w        valor_dominio.vl_dominio%type;
nr_seq_classif_diag_w        classificacao_diagnostico.nr_sequencia%type;
nr_seq_tipo_episodio_w        restricao_diagnostico.nr_seq_tipo_episodio%type;
nr_seq_tipo_admi_fat_w        restricao_diagnostico.nr_seq_tipo_admissao_fat%type;


cursor C01 is
select  a.nr_sequencia,
       b.cd_perfil,
       b.nr_seq_tipo_episodio,
       b.nr_seq_tipo_admissao_fat
from    classificacao_diagnostico a ,
       restricao_diagnostico b
where   b.nr_seq_classificacao   = a.nr_sequencia
and     a.ie_classificacao_diagnostico = ds_atrib_dominio_w;

c01_w    C01%rowtype;

begin
ds_resultado_w     := 'S';

select  nr_seq_tipo_admissao_fat
into     nr_seq_tipo_admi_fat_w
from    atendimento_paciente
where   nr_atendimento = obter_atendimento_episodio(nr_seq_episodio_p);

select     nr_seq_tipo_episodio
into     nr_seq_tipo_episodio_w
from     episodio_paciente
where     nr_sequencia = nr_seq_episodio_p;

if (ds_atributo_p = 'IE_DIAG_PRINC_EPISODIO') then
    ds_atrib_dominio_w := 'DPE';
end if;

open C01;
loop
fetch C01 into
    c01_w;
exit when C01%notfound;

        if (c01_w.cd_perfil = cd_perfil_p and c01_w.nr_seq_tipo_episodio is null and c01_w.nr_seq_tipo_admissao_fat is null) then
            ds_resultado_w := 'S';
        elsif (c01_w.cd_perfil = cd_perfil_p and c01_w.nr_seq_tipo_episodio = nr_seq_tipo_episodio_w and c01_w.nr_seq_tipo_admissao_fat is null) then
            ds_resultado_w := 'S';
        elsif (c01_w.cd_perfil = cd_perfil_p and c01_w.nr_seq_tipo_episodio = nr_seq_tipo_episodio_w and c01_w.nr_seq_tipo_admissao_fat = nr_seq_tipo_admi_fat_w) then
            ds_resultado_w := 'S';
        else
            ds_resultado_w := 'N';
        end if;
	
end loop;
close C01;

return    ds_resultado_w;

end obter_exibe_atrib_diag;
/