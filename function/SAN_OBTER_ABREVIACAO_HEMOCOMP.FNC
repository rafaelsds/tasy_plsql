create or replace
function SAN_Obter_Abreviacao_Hemocomp(	nr_seq_derivado_p number,
										ie_opcao_p		varchar2)
				return varchar2 is
i number (2);
k number(2);
/*
ie_opcao_p - Para futuras customizações
*/

ds_deriv_temp_w		varchar2(255);
ds_derivado_abreviado_w varchar2(50);

begin

select	initcap(obter_desc_san_derivado(nr_seq_derivado_p))
into		ds_deriv_temp_w
from		dual;

k:=instr(ds_deriv_temp_w,' ');

ds_derivado_abreviado_w := substr(ds_deriv_temp_w,1,1);

while length(ds_deriv_temp_w)	 > 0
loop
	if	(substr(ds_deriv_temp_w,1,1) = ' ') then
			ds_derivado_abreviado_w := ds_derivado_abreviado_w ||substr(ds_deriv_temp_w,2,1);
	end if;
	ds_deriv_temp_w:= substr(ds_deriv_temp_w,2,length(ds_deriv_temp_w));
	end loop;

return	ds_derivado_abreviado_w;

end SAN_Obter_Abreviacao_Hemocomp;
/