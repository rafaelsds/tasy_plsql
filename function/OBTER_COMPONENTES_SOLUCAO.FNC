create or replace 
function obter_componentes_solucao	(nr_prescricao_p	number,
					nr_seq_solucao_p	number,
					ie_quebra_linha_p	varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number)
					return varchar2 is

qt_dose_w		number(15,3);
qt_dose_corrigida_w		number(15,3);
cd_unidade_medida_w	varchar2(30);
ds_material_w		varchar2(80);
ds_componentes_w	varchar2(2000);
ie_separador_w		varchar2(3);
ds_dose_w		varchar2(50);
ds_dose_corrigida_w		varchar2(50);
ds_observacao_w		varchar2(1000);
ie_exibir_obs_w		varchar2(1) := 'N';
qt_dias_util_w		number(3);
ie_cpoe_w			boolean	:= false;
ds_dose_information_w varchar(255) := '';
ds_rediluir_w		varchar2(120 char);
qt_rediluir_w		cpoe_material.qt_solucao_red%type;
nr_seq_mat_cpoe_w	cpoe_material.nr_sequencia%type;	
cd_material_w		material.cd_material%type;

cursor c01 is
select	a.qt_dose,
		a.cd_unidade_medida_dose,
		substr(b.ds_reduzida,1,80) ds_material,
		substr(a.ds_observacao,1,1000),
		nvl(a.nr_dia_util,0),
		a.qt_volume_corrigido
from	material b,
		prescr_material a,
		prescr_medica c
where	a.nr_prescricao 	= c.nr_prescricao
and		a.nr_prescricao		= nr_prescricao_p
and		a.nr_sequencia_solucao	= nr_seq_solucao_p
and		a.cd_material		= b.cd_material
and		a.dt_suspensao		is null
and		nvl(c.cd_funcao_origem,924) <> 2314
order by
	a.nr_sequencia;

cursor c02 is
select qt_dose,  
       cd_unidade_medida_dose, 
       ds_material, 
       ds_observacao, 
       nr_dia_util,
	   qt_volume_corrigido,
	   cd_material
from (select  a.qt_dose,
              a.cd_unidade_medida_dose,
              substr(b.ds_reduzida,1,80) ds_material,
              substr(a.ds_observacao,1,1000) ds_observacao,
              nvl(a.nr_dia_util,0) nr_dia_util,
              a.nr_Seq_mat_cpoe,
              a.cd_material,
			  a.qt_volume_corrigido
      from		material b,
              prescr_material a,
              cpoe_material c,
              prescr_medica d
      where		a.nr_prescricao  = d.nr_prescricao
      and		  a.nr_seq_mat_cpoe 	= c.nr_sequencia
      and		  a.nr_prescricao		= nr_prescricao_p
      and		  a.nr_sequencia_solucao	= nr_seq_solucao_p
      and		  a.cd_material		= b.cd_material
      and		  a.dt_suspensao		is null
      and		  nvl(c.cd_funcao_origem,924) = 2314
      and		  a.ie_agrupador 		<> 13
      union
      select  a.qt_dose,
              a.cd_unidade_medida_dose,
              substr(b.ds_reduzida,1,80) ds_material,
              substr(a.ds_observacao,1,1000) ds_observacao,
              nvl(a.nr_dia_util,0) nr_dia_util,
              0 nr_Seq_mat_cpoe,
              a.cd_material,
			  a.qt_volume_corrigido
      from		material b,
              prescr_material a,
              prescr_medica d
      where		a.nr_prescricao 	= d.nr_prescricao
      and		  a.nr_prescricao		= nr_prescricao_p
      and		  a.nr_sequencia_solucao	= nr_seq_solucao_p
      and		  a.cd_material		= b.cd_material
      and		  a.dt_suspensao		is null
      and		  nvl(d.cd_funcao_origem,924) = 2314
      and		  a.ie_agrupador 		= 13)
order by
		ordena_item_cpoe(nr_Seq_mat_cpoe, cd_material);
		
begin
if (nm_usuario_p is not null) then
	obter_param_usuario(1113, 135, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_exibir_obs_w);
end if;

if (pkg_i18n.get_user_locale() = 'en_AU') then
	ie_exibir_obs_w := 'N';
end if;


ie_separador_w := chr(13)||chr(10);
if	(nr_prescricao_p	is not null) and
	(nr_seq_solucao_p	is not null) then
	
	select	nvl(max(a.nr_seq_mat_cpoe), 0)
	into	nr_seq_mat_cpoe_w
	from	prescr_material a
	where	a.nr_prescricao = nr_prescricao_p
	and		a.nr_sequencia_solucao = nr_seq_solucao_p
	and		a.ie_agrupador = 4;

	if	(ie_quebra_linha_p = 'N') then
		ie_separador_w := ',';
	end if;
	
	open c01;
	loop
	fetch c01 into	qt_dose_w,
			cd_unidade_medida_w,
			ds_material_w,
			ds_observacao_w,
			qt_dias_util_w,
			qt_dose_corrigida_w;
		exit when c01%notfound;
		begin
		ds_dose_w	:= to_char(qt_dose_w);
		ds_dose_information_w := obter_terap_dose_solucao(nr_seq_solucao_p, nr_prescricao_p);
		
		ds_dose_corrigida_w := '';
		if (qt_dose_corrigida_w is not null) then
			ds_dose_corrigida_w	:= obter_desc_expressao(746875)|| ' ' ||to_char(qt_dose_corrigida_w) || ' ' || obter_desc_expressao(703346) || ' '; -- Dose corrigida {qt_dose_corrigida} ml
		end if;
		
		if	(substr(to_char(qt_dose_w),1,1) = ',') then
			ds_dose_w	:= '0'||to_char(qt_dose_w);
		end if;
		
		if	(ds_material_w is not null) and
			(nvl(length(ds_componentes_w),0) < 1900) and
			(ds_observacao_w is not null) and
			(ie_exibir_obs_w = 'S') then			
			if (ds_dose_information_w is not null) then
				ds_componentes_w := substr(ds_componentes_w || ds_dose_w || ' ' || cd_unidade_medida_w || ' ' || ds_material_w || ' ' || ds_dose_corrigida_w || '(' || OBTER_DESC_EXPRESSAO(959696) ||qt_dias_util_w||')'|| ds_dose_information_w || ie_separador_w ||OBTER_DESC_EXPRESSAO(502224)|| ds_observacao_w,1,1999) || chr(10);			-- 959696: 'Days used'  502224: 'Obs:'
			else
				ds_componentes_w := substr(ds_componentes_w || ds_dose_w || ' ' || cd_unidade_medida_w || ' ' || ds_material_w || ' ' || ds_dose_corrigida_w || '(' || OBTER_DESC_EXPRESSAO(959696) ||qt_dias_util_w||')' || ie_separador_w ||OBTER_DESC_EXPRESSAO(502224)|| ds_observacao_w,1,1999) || chr(10);			-- 959696: 'Business days'  502224: 'Obs:'
			end if;
		elsif   (ds_material_w is not null) and
			(nvl(length(ds_componentes_w),0) < 1900) then			
			if (ds_dose_information_w is not null) then
				ds_componentes_w := substr(ds_componentes_w || ds_dose_w || ' ' || cd_unidade_medida_w || ' ' || ds_material_w || ' ' || ds_dose_corrigida_w  || '(' || OBTER_DESC_EXPRESSAO(959696)||qt_dias_util_w||')',1,1999)|| ie_separador_w || ds_dose_information_w ;
			else
				ds_componentes_w := substr(ds_componentes_w || ds_dose_w || ' ' || cd_unidade_medida_w || ' ' || ds_material_w || ' ' || ds_dose_corrigida_w  || '(' || OBTER_DESC_EXPRESSAO(959696)||qt_dias_util_w||')',1,1999)|| ie_separador_w;
			end if;
		end if;

		end;
	end loop;
	close c01;
	
	open c02;
	loop
	fetch c02 into	qt_dose_w,
			cd_unidade_medida_w,
			ds_material_w,
			ds_observacao_w,
			qt_dias_util_w,
			qt_dose_corrigida_w,
			cd_material_w;
		exit when c02%notfound;
		begin

		ds_rediluir_w := '';

		if (nr_seq_mat_cpoe_w > 0) then
			select 	nvl(max(a.qt_solucao_red), 0)
			into	qt_rediluir_w
			from	cpoe_material a
			where	a.nr_sequencia = nr_seq_mat_cpoe_w
			and		a.cd_mat_dil = cd_material_w;
			
			if (qt_rediluir_w > 0) then
				ds_rediluir_w := obter_desc_expressao(302239) || ' ' || to_char(qt_rediluir_w) || ' ';
			end if;
		end if;

		ds_dose_w	:= to_char(qt_dose_w);

		ds_dose_corrigida_w := '';
		if (qt_dose_corrigida_w is not null) then
			ds_dose_corrigida_w	:= obter_desc_expressao(746875)|| ' ' ||to_char(qt_dose_corrigida_w) || ' ' || obter_desc_expressao(703346) || ' '; -- Dose corrigida {qt_dose_corrigida} ml
		end if;

		if	(substr(to_char(qt_dose_w),1,1) = ',') then
			ds_dose_w	:= '0'||to_char(qt_dose_w);
		end if;
		if  (ds_material_w is not null) and
			(nvl(length(ds_componentes_w),0) < 1900) then			
			ds_componentes_w := substr(ds_componentes_w || ds_dose_w || ' ' || cd_unidade_medida_w || ' ' || ds_material_w || ' ' || ds_dose_corrigida_w ||  '(' || OBTER_DESC_EXPRESSAO(959696)||qt_dias_util_w||')',1,1999)|| ie_separador_w;			
			if (nvl(length(ds_rediluir_w), 0) > 0 and
				nvl(length(ds_componentes_w), 0) < 1900) then
				ds_componentes_w := substr(ds_componentes_w || ds_rediluir_w,1,1999)|| ie_separador_w;
			end if;
		end if;
		
		ie_cpoe_w	:= true;
		
		end;
	end loop;
	close c02;

	if (ie_cpoe_w) then
		if (obter_terap_dose_solucao(nr_seq_solucao_p, nr_prescricao_p) is not null) then
			ds_componentes_w := substr(ds_componentes_w || obter_terap_dose_solucao(nr_seq_solucao_p, nr_prescricao_p),1,1999) || chr(10);
		end if;
		
		if (trim(ds_observacao_w) is not null)	and (pkg_i18n.get_user_locale() <> 'en_AU') then
			ds_componentes_w := substr(ds_componentes_w || ie_separador_w || OBTER_DESC_EXPRESSAO(502224)|| ds_observacao_w,1,1999) || chr(10);
		end if;
	end if;
	
end if;

return ds_componentes_w;

end obter_componentes_solucao;
/
