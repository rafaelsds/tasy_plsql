create or replace
function com_obter_dados_mod_lic(	nr_sequencia_p	number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is

/*
D - Descri��o da Modalidade
*/			
			
ds_retorno_w		varchar2(80);
ds_modalidade_w	varchar2(80);
			
begin

if	(ie_opcao_p = 'D') then
	select	ds_modalidade
	into	ds_modalidade_w
	from	com_modalidade_lic
	where	nr_sequencia = nr_sequencia_p;

	ds_retorno_w	:= ds_modalidade_w;
end if;

return	ds_retorno_w;

end com_obter_dados_mod_lic;
/