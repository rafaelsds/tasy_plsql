Create or Replace
FUNCTION Obter_Valor_terceiro_Conta(
		nr_interno_conta_p	Number,
		ie_opcao_p		Varchar2)
		RETURN Number IS

vl_repasse_w		Number(15,2);

vl_repasse_proc_w		Number(15,2);
vl_Liberado_Proc_w		Number(15,2);
vl_pendente_Proc_w		Number(15,2);

vl_repasse_mat_w		Number(15,2);
vl_Liberado_mat_w		Number(15,2);
vl_pendente_mat_w		Number(15,2);

/* Op��o
R - Repasse
L - Liberado
P - Pendente
*/
BEGIN

select 
	nvl(sum(vl_repasse),0),
	nvl(sum(vl_liberado),0),
	nvl(sum(vl_repasse),0) - nvl(sum(vl_liberado),0)
into	vl_repasse_proc_w,
	vl_liberado_proc_w,
	vl_pendente_proc_w
from 	procedimento_repasse b,
	procedimento_paciente a
where	b.nr_seq_procedimento	= a.nr_sequencia
and	a.nr_interno_conta		= nr_interno_conta_p;
--and	b.nr_repasse_terceiro is null
--and	b.nr_seq_terceiro is not null;

select 
	nvl(sum(vl_repasse),0),
	nvl(sum(vl_liberado),0),
	nvl(sum(vl_repasse),0) - nvl(sum(vl_liberado),0)
into	vl_repasse_mat_w,
	vl_liberado_mat_w,
	vl_pendente_mat_w
from 	material_repasse b,
	material_atend_paciente a
where	b.nr_seq_material	= a.nr_sequencia
and	a.nr_interno_conta	= nr_interno_conta_p;
--and	b.nr_repasse_terceiro is null
--and	b.nr_seq_terceiro is not null;

if	(ie_opcao_p	= 'R') then
	vl_repasse_w	:= vl_repasse_proc_w + vl_repasse_mat_w;
elsif	(ie_opcao_p	= 'P') then
	vl_repasse_w	:= vl_pendente_proc_w + vl_pendente_mat_w;
else
	vl_repasse_w	:= vl_liberado_proc_w + vl_liberado_mat_w;
end if;

RETURN vl_repasse_w;

END Obter_Valor_terceiro_Conta;
/