create or replace
function obter_lote_multiplo_mat(		cd_estabelecimento_p	number,					
					cd_cnpj_p		varchar2,
					cd_material_p		number)
 		    	return number is

qt_conv_compra_est_w		number(15,4);

begin

if	(cd_cnpj_p is null) then
	
	select	nvl(max(qt_conv_compra_est),0)
	into	qt_conv_compra_est_w
	from	material
	where	cd_material = cd_material_p;

else
	select	nvl(obter_dados_material_fornec(cd_estabelecimento_p, cd_cnpj_p, cd_material_p, 2),0)
	into	qt_conv_compra_est_w
	from	dual;
	
	if	(qt_conv_compra_est_w = 0) then
	
		select	nvl(max(qt_conv_compra_est),0)
		into	qt_conv_compra_est_w
		from	material
		where	cd_material = cd_material_p;
	end if;
end if;

return	qt_conv_compra_est_w;

end obter_lote_multiplo_mat;
/