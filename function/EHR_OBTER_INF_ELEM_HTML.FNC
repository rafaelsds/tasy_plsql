create or replace
function EHR_Obter_Inf_Elem_html(	nr_atendimento_p		number,
									cd_pessoa_fisica_p		varchar2,
									nr_seq_conteudo_p		number,
									nr_seq_reg_elemento_p	number,
									nr_seq_entidade_p		number)
 		    	return varchar2 is

				
ds_retorno_w		varchar2(4000);
nm_function_w		varchar2(50);

begin


if (nr_seq_entidade_p = 26) then

	select  EHR_PEP_PAC_CI(nr_seq_reg_elemento_p, nr_seq_entidade_p, nr_seq_conteudo_p)
	into	ds_retorno_w
	from    dual;

else

	Select  EHR_Obter_Inf_Elem(nr_atendimento_p,cd_pessoa_fisica_p,nr_seq_conteudo_p)
	into	ds_retorno_w
	from    dual;

end if;

return	ds_retorno_w;

end EHR_Obter_Inf_Elem_html;
/