create or replace
function obter_se_pre_aprovacao(	nr_solic_compra_p		number)
 		    	return varchar2 is

cd_estabelecimento_w			number(4);			
ie_pre_aprovacao_w			varchar2(1) := 'N';
ie_considerar_param_w			varchar2(1) := 'S';	
cd_material_w				number(6);
cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
ie_consiste_qt_maior_w			varchar2(1);
qt_material_w				number(13,4);
qt_estoque_maximo_w			number(15,4);
dt_pre_aprovacao_w			date;

cursor c01 is
select	b.cd_material,
	b.qt_material,
	obter_mat_estabelecimento(a.cd_estabelecimento, a.cd_estabelecimento, b.cd_material, 'MA')
from	solic_compra a,
	solic_compra_item b
where	a.nr_solic_compra = b.nr_solic_compra
and	a.dt_liberacao is not null
and	a.dt_pre_aprovacao is null
and	a.dt_autorizacao is null
and	a.dt_baixa is null
and	a.nr_seq_motivo_cancel is null
and	b.dt_reprovacao is null
and	a.nr_solic_compra = nr_solic_compra_p
and	dt_pre_aprovacao_w is null;

cursor c02 is
select	ie_consiste_qt_maior,
	nvl(ie_considerar_param,'S')
from	regra_pre_aprovacao_compra
where	cd_estabelecimento = cd_estabelecimento_w
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(cd_material, cd_material_w) = cd_material_w or cd_material_w = 0)
order by
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);
	
begin

select	cd_estabelecimento,
	dt_pre_aprovacao
into	cd_estabelecimento_w,
	dt_pre_aprovacao_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

open C01;
loop
fetch C01 into	
	cd_material_w,
	qt_material_w,
	qt_estoque_maximo_w;
exit when C01%notfound;
	begin
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from	estrutura_material_v
	where	cd_material = cd_material_w;
	
	open C02;
	loop
	fetch C02 into	
		ie_consiste_qt_maior_w,
		ie_considerar_param_w;
	exit when C02%notfound;
		begin
		
		
		if	(ie_considerar_param_w = 'N') then
			ie_pre_aprovacao_w := 'S';
		elsif	(ie_pre_aprovacao_w  = 'N') and
			(ie_consiste_qt_maior_w = 'S') and
			(qt_material_w > qt_estoque_maximo_w) then
			
			ie_pre_aprovacao_w := 'S';
		end if;			
		
		
		end;
	end loop;
	close C02;	
	
	end;
end loop;
close C01;

return	ie_pre_aprovacao_w;

end obter_se_pre_aprovacao;
/
