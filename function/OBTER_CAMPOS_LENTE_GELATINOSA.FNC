create or replace
function 	obter_campos_lente_gelatinosa        (nm_campo_p		varchar2,
						      nm_tabela_p		varchar2,
						      nr_atendimento_p      number)
					return number is
					
					
vl_od_me_cu_ceratoscopia_w     number(15,4);
vl_od_ma_cu_ceratoscopia_w     number(15,4);
vl_od_eixo_ceratoscopia_w      number(15,4);	 
vl_oe_me_cu_ceratoscopia_w     number(15,4);
vl_oe_ma_cu_ceratoscopia_w     number(15,4);
vl_oe_eixo_ceratoscopia_w      number(15,4);

vl_od_me_cu_ceratometria_w     number(15,4);
vl_od_ma_cu_ceratometria_w	number(15,4);
vl_od_eixo_ceratometria_w	number(15,4);
vl_oe_me_cu_ceratometria_w	number(15,4);
vl_oe_ma_cu_ceratometria_w	number(15,4);
vl_oe_eixo_ceratometria_w	number(15,4);
vl_od_pl_dio_esf_w		number(15,4);
vl_od_pl_dio_cil_w		number(15,4);
vl_od_pl_eixo_w			number(3,0);
vl_od_pl_adicao_w		number(15,4);
vl_oe_pl_dio_esf_w		number(5,2);
vl_oe_pl_dio_cil_w		number(5,2);
vl_oe_pl_eixo_w			number(5,2);	
vl_oe_pl_adicao_w		number(5,2);	

ds_retorno_w 		        number(15,4);

begin

if (nm_tabela_p = 'OFT_OCULOS') and
   (nr_atendimento_p is not null) then	
	SELECT  MAX(VL_OD_PL_DIO_ESF),
		MAX(VL_OD_PL_DIO_CIL),
		MAX(VL_OD_PL_EIXO),
		MAX(VL_OD_PL_ADICAO),
		MAX(VL_OE_PL_DIO_ESF),
		MAX(VL_OE_PL_DIO_CIL),
		MAX(VL_OE_PL_EIXO),
		MAX(VL_OE_PL_ADICAO)
	into	vl_od_pl_dio_esf_w,
		vl_od_pl_dio_cil_w,
		vl_od_pl_eixo_w,
		vl_od_pl_adicao_w,
		vl_oe_pl_dio_esf_w,
		vl_oe_pl_dio_cil_w,
		vl_oe_pl_eixo_w,
		vl_oe_pl_adicao_w
	FROM   OFT_OCULOS 
	WHERE  IE_SITUACAO = 'A' 
	AND    DT_REGISTRO =  ( SELECT MAX(DT_REGISTRO)
				FROM OFT_OCULOS 
				WHERE nr_seq_consulta IN (SELECT y.nr_sequencia 
	   				   	   	   FROM  atendimento_paciente x, 
  	   					  		 oft_consulta y 
						  	   WHERE  y.nr_atendimento = x.nr_atendimento 
						   	   AND    x.nr_atendimento = nr_atendimento_p)
							   AND IE_SITUACAO = 'A');						   
elsif (nm_tabela_p = 'OFT_TOPOGRAFIA_CORNEANA') and
   (nr_atendimento_p is not null) then	
	SELECT  MAX(VL_OD_ME_CU_CERATOSCOPIA),
		MAX(VL_OD_MA_CU_CERATOSCOPIA),
		MAX(VL_OD_EIXO_CERATOSCOPIA),
		MAX(VL_OE_ME_CU_CERATOSCOPIA),
		MAX(VL_OE_MA_CU_CERATOSCOPIA),
		MAX(VL_OE_EIXO_CERATOSCOPIA)
	into	vl_od_me_cu_ceratoscopia_w,    
		vl_od_ma_cu_ceratoscopia_w,  
		vl_od_eixo_ceratoscopia_w,    	 
		vl_oe_me_cu_ceratoscopia_w,     
		vl_oe_ma_cu_ceratoscopia_w,     
		vl_oe_eixo_ceratoscopia_w      	
	FROM OFT_TOPOGRAFIA_CORNEANA 
	where IE_SITUACAO = 'A' 
	AND DT_REGISTRO =  (SELECT MAX(DT_REGISTRO) 
			    FROM OFT_TOPOGRAFIA_CORNEANA 
			    WHERE nr_seq_consulta IN (SELECT y.nr_sequencia 
	   				   	   	FROM  atendimento_paciente x, 
  	   					  		oft_consulta y 
						  	WHERE  y.nr_atendimento = x.nr_atendimento 
						   	AND    x.nr_atendimento = nr_atendimento_p)
					  AND IE_SITUACAO = 'A');
			    
elsif   (nm_tabela_p = 'OFT_CERASTOCOPIA') and
	(nr_atendimento_p is not null) then	
	SELECT MAX(VL_OD_ME_CU_CERATOMETRIA),
		MAX(VL_OD_MA_CU_CERATOMETRIA),
		MAX(VL_OD_EIXO_CERATOMETRIA),
		MAX(VL_OE_ME_CU_CERATOMETRIA),
		MAX(VL_OE_MA_CU_CERATOMETRIA),
		MAX(VL_OE_EIXO_CERATOMETRIA)
	into	vl_od_me_cu_ceratometria_w,    
		vl_od_ma_cu_ceratometria_w,	
		vl_od_eixo_ceratometria_w,	
		vl_oe_me_cu_ceratometria_w,	
		vl_oe_ma_cu_ceratometria_w,	
		vl_oe_eixo_ceratometria_w		
	FROM OFT_CERASTOCOPIA 
	where IE_SITUACAO = 'A' 
	AND DT_REGISTRO =  (SELECT MAX(DT_REGISTRO) 
			    FROM OFT_CERASTOCOPIA 
			     WHERE nr_seq_consulta IN (SELECT y.nr_sequencia 
	   				   	   	FROM  atendimento_paciente x, 
  	   					  		oft_consulta y 
						  	WHERE  y.nr_atendimento = x.nr_atendimento 
						   	AND    x.nr_atendimento = nr_atendimento_p)
					  AND IE_SITUACAO = 'A');
		    		    
end if;


if (nm_tabela_p = 'OFT_OCULOS') then
	if    (nm_campo_p = 'VL_OD_PL_DIO_ESF') then
		ds_retorno_w := vl_od_pl_dio_esf_w;
	elsif (nm_campo_p = 'VL_OD_PL_DIO_CIL') then
		ds_retorno_w := vl_od_pl_dio_cil_w;	
	elsif (nm_campo_p = 'VL_OD_PL_EIXO') then
		ds_retorno_w := vl_od_pl_eixo_w;
	elsif (nm_campo_p = 'VL_OD_PL_ADICAO') then
		ds_retorno_w := vl_od_pl_adicao_w;
	elsif (nm_campo_p = 'VL_OE_PL_DIO_ESF') then
		ds_retorno_w := vl_oe_pl_dio_esf_w;
	elsif (nm_campo_p = 'VL_OE_PL_DIO_CIL') then
		ds_retorno_w := vl_oe_pl_dio_cil_w;
	elsif (nm_campo_p = 'VL_OE_PL_EIXO') then
		ds_retorno_w := vl_oe_pl_eixo_w;
	elsif (nm_campo_p = 'VL_OE_PL_ADICAO') then
		ds_retorno_w := vl_oe_pl_adicao_w;
	end if;
		
elsif (nm_tabela_p = 'OFT_TOPOGRAFIA_CORNEANA') then
	if    (nm_campo_p = 'VL_OD_ME_CU_CERATOSCOPIA') then
		ds_retorno_w := vl_od_me_cu_ceratoscopia_w ;
	elsif (nm_campo_p = 'VL_OD_MA_CU_CERATOSCOPIA') then
		ds_retorno_w := vl_od_ma_cu_ceratoscopia_w;
	elsif (nm_campo_p = 'VL_OD_EIXO_CERATOSCOPIA') then
		ds_retorno_w := vl_od_eixo_ceratoscopia_w;	
	elsif (nm_campo_p = 'VL_OE_ME_CU_CERATOSCOPIA') then
		ds_retorno_w := vl_oe_me_cu_ceratoscopia_w;
	elsif (nm_campo_p = 'VL_OE_MA_CU_CERATOSCOPIA') then
		ds_retorno_w := vl_oe_ma_cu_ceratoscopia_w;
	elsif (nm_campo_p = 'VL_OE_EIXO_CERATOSCOPIA') then
		ds_retorno_w := vl_oe_eixo_ceratoscopia_w;
	end if;
	
elsif (nm_tabela_p = 'OFT_CERASTOCOPIA') then
	if (nm_campo_p = 'VL_OD_ME_CU_CERATOMETRIA') then
		ds_retorno_w := VL_OD_ME_CU_CERATOMETRIA_w;
	elsif (nm_campo_p = 'VL_OD_MA_CU_CERATOMETRIA') then
		ds_retorno_w := VL_OD_MA_CU_CERATOMETRIA_w;	
	elsif (nm_campo_p = 'VL_OD_EIXO_CERATOMETRIA') then
		ds_retorno_w := VL_OD_EIXO_CERATOMETRIA_w;	
	elsif (nm_campo_p = 'VL_OE_ME_CU_CERATOMETRIA') then
		ds_retorno_w := VL_OE_ME_CU_CERATOMETRIA_w;	
	elsif (nm_campo_p = 'VL_OE_MA_CU_CERATOMETRIA') then
		ds_retorno_w := VL_OE_MA_CU_CERATOMETRIA_w;	
	elsif (nm_campo_p = 'VL_OE_EIXO_CERATOMETRIA') then
		ds_retorno_w := VL_OE_EIXO_CERATOMETRIA_w;
	end if;
	
end if;

return	ds_retorno_w;

end obter_campos_lente_gelatinosa;
/