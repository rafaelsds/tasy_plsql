create or replace
function Qt_Obter_Desc_Tipo_Agenda(
				ie_tipo_pend_agenda_w	varchar2)
 		    	return varchar2 is

ds_Retorno_w	varchar2(255);			
			
begin

select	max(ds_item)
into	ds_Retorno_w
from	qt_item_agenda
where	cd_item	= ie_tipo_pend_agenda_w;

return	ds_Retorno_w;

end Qt_Obter_Desc_Tipo_Agenda;
/