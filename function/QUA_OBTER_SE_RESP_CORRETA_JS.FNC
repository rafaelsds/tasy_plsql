create or replace
function qua_obter_se_resp_correta_js(nr_seq_questionario_p	number,
					nr_sequencia_p		number)
 		    	return varchar2 is

ie_correto_w	varchar2(1) := 'N';			
begin

if	(nvl(nr_seq_questionario_p,0) > 0) and
	(nvl(nr_sequencia_p,0) > 0) then
	
	select	nvl(max(ie_correto),'N')
	into	ie_correto_w
	from	qua_doc_quest_resp
	where	nr_seq_questionario = nr_seq_questionario_p
	and	nr_sequencia = nr_sequencia_p;

end if;

return	ie_correto_w;

end qua_obter_se_resp_correta_js;
/