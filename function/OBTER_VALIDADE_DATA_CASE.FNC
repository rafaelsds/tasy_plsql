create or replace
function Obter_validade_data_case (	nr_seq_episodio_p  number)
				return Date is
			

dt_validade_case_w	 atendimento_paciente_inf.dt_validade%type;

begin

if ( nr_seq_episodio_p is not null ) then

	Select  max(c.dt_validade)
	into	dt_validade_case_w
	from    episodio_paciente a,
			atendimento_paciente b,
			atendimento_paciente_inf c
	where 	a.nr_sequencia = b.nr_seq_episodio
	and		c.nr_atendimento = b.nr_atendimento
	and		a.nr_sequencia = nr_seq_episodio_p;
	

end if;

return	dt_validade_case_w;

end Obter_validade_data_case;
/