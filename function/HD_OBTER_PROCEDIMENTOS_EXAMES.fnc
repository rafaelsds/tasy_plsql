create or replace
function hd_obter_procedimentos_exames (nr_seq_proc_p number,
													nr_sequencia_p	number,	
													cd_protocolo_p	number)
														return varchar2 is


ds_procedimento_exame_w		varchar2(255);

begin

select 
	max(nvl(substr(obter_desc_proc_interno(a.nr_seq_proc_interno),1,100),
	substr(nvl(obter_desc_estrut_proc(null,null,null,a.cd_procedimento,a.ie_origem_proced),obter_desc_exame(a.nr_seq_exame)),1,200))) ds
into ds_procedimento_exame_w
from	protocolo_medic_proc a
where a.nr_sequencia = nr_sequencia_p
and	a.nr_seq_proc = nr_seq_proc_p
and	a.cd_protocolo = cd_protocolo_p;

return ds_procedimento_exame_w;

end hd_obter_procedimentos_exames;
/
