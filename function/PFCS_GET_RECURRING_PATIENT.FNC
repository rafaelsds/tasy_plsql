CREATE OR REPLACE FUNCTION PFCS_GET_RECURRING_PATIENT(
    nr_seq_patient_p number,
    ie_frequent_flyer_p varchar2 default 'S'
) RETURN VARCHAR2 IS


recurring_days_w        varchar(10)  := ' ';
recurring_year_w        varchar(10)  := ' ';
recurring_year_ed_w     varchar(10)  := ' ';
recurring_details_w     varchar(255) := '';

BEGIN
if (ie_frequent_flyer_p = 'S') then
    -- Number of days since last encounter
    select max(nr_value) into recurring_days_w FROM
       (
            SELECT obs_com.nr_value
            from pfcs_observation obs,
            pfcs_observ_component obs_com
            where obs_com.nr_seq_observation = obs.nr_sequencia
            and obs_com.cd_code = 'FreqFlyer-Red30'
            and obs.nr_seq_patient = nr_seq_patient_p
            order by obs_com.dt_atualizacao desc
        ) where  rownum = 1;

    -- Recurring last year (inpatient)
    select max(nr_value) into recurring_year_w FROM
       (
            SELECT obs_com.nr_value
            from pfcs_observation obs,
            pfcs_observ_component obs_com
            where obs_com.nr_seq_observation = obs.nr_sequencia
            and obs_com.cd_code = 'FreqFlyer-inpatient_visits'
            and obs.nr_seq_patient = nr_seq_patient_p
            order by obs_com.dt_atualizacao desc
        ) where  rownum = 1;

    -- Recurring last year (Emergency)
    select max(nr_value) into recurring_year_ed_w
    	FROM
    	(
    	    SELECT obs_com.nr_value
            from pfcs_observation obs,
            pfcs_observ_component obs_com
            where obs_com.nr_seq_observation = obs.nr_sequencia
            and obs_com.cd_code = 'FreqFlyer-ed_visits'
            and obs.nr_seq_patient = nr_seq_patient_p
    		order by obs_com.dt_atualizacao desc
    	) where rownum = 1;

    if (length(recurring_days_w) > 0) then
        recurring_details_w := recurring_details_w || concat(obter_desc_expressao(1051623) || ': ', recurring_days_w) || ';';
    end if;

    if (length(recurring_year_w) > 0) then
        recurring_details_w := recurring_details_w || concat(obter_desc_expressao(1046402) || ': ', recurring_year_w) || ';';
    end if;

    if (length(recurring_year_ed_w) > 0) then
        recurring_details_w := recurring_details_w || concat(obter_desc_expressao(1046404) || ': ', recurring_year_ed_w) || ';';
    end if;

    if (length(recurring_details_w) > 1) then
        recurring_details_w := substr(recurring_details_w, 1, length(recurring_details_w)-1);
    end if;
end if;
RETURN recurring_details_w;

END PFCS_GET_RECURRING_PATIENT;
/