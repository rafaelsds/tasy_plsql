create or replace
function man_obter_dados_solicitante (	cd_pessoa_solicitante_p		varchar2,
					ie_opcao_p			varchar2)
 		    			return varchar2 is
					
/*
IE_OP��O:
CC - Centro de custro;
DCC- Descri��o do centro de custo;
*/					
					
ds_retorno_w		varchar2(255) := '';	
cd_centro_custo_w	number(8);

begin

select	nvl(max(b.cd_centro_custo),0)
into	cd_centro_custo_w	
from	setor_atendimento b,
	usuario a
where	b.cd_setor_atendimento = a.cd_setor_atendimento
and    a.cd_pessoa_fisica = cd_pessoa_solicitante_p;

if	(ie_opcao_p = 'CC') then
	begin
	ds_retorno_w := cd_centro_custo_w;
	end;
elsif	(ie_opcao_p = 'DCC') then
	begin
	select	substr(nvl(obter_desc_centro_custo(cd_centro_custo_w),''),1,255)
	into	ds_retorno_w
	from	dual;
	end;
end if;

return	ds_retorno_w;

end man_obter_dados_solicitante;
/