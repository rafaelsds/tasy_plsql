create or replace
function obter_execucao_laudo_pep( nr_prescricao_p number, nr_atendimento_p number )

  return varchar2 is

  ds_execucao_w varchar(40);


BEGIN

  select decode(nr_prescricao_p, null, obter_desc_expressao(718223), obter_desc_expressao(697085) ) as ds_externo 
  into ds_execucao_w
  from atendimento_paciente
  where nr_atendimento = nr_atendimento_p;
  
  Return   ds_execucao_w;

END obter_execucao_laudo_pep;
/