create or replace
function pls_obter_vl_excluir_afpergs
			(	vl_incluir_p		Number,
				dt_referencia_p		Date,
				nr_titulo_p		Number,
				ie_tipo_item_p		Varchar2,
				ie_virgula_p		Varchar2)
 		    	return Number is
/*
ie_tipo_item_p
I	= Incluir
E	= Excluir
T	= Se existe altera��o no valor da mensalidade
TI	= Total incluir
TE	= Total excluir
VI	= Verifica se existe valor � incluir
VE	= Verifica se existe valor � excluir
*/

ds_retorno_w		Number(15,2) := 0;
nr_seq_pagador_w	Number(10);
nr_lote_desc_folha_w	Number(10);
vl_cobranca_w		Number(15,2);
nr_titulo_w		Number(10);
nr_seq_mensalidade_w	Number(10);

begin

select	max(a.nr_seq_pagador)
into	nr_seq_pagador_w
from	pls_mensalidade	a,
	titulo_receber	b
where	b.nr_titulo =  nr_titulo_p
and	b.nr_seq_mensalidade	= a.nr_sequencia;

select	max(a.nr_sequencia),
	max(b.nr_titulo)
into	nr_seq_mensalidade_w,
	nr_titulo_w
from	pls_mensalidade	a,
	titulo_receber	b
where	a.nr_seq_pagador = nr_seq_pagador_w
and	b.nr_seq_mensalidade	= a.nr_sequencia
and	a.ie_cancelamento is null
and	trunc(dt_referencia, 'mm') = trunc(add_months(dt_referencia_p,-1), 'mm');

select	max(a.nr_sequencia)
into	nr_lote_desc_folha_w
from	cobranca_escritural	a,
	titulo_receber_cobr	b
where	a.nr_sequencia = b.nr_seq_cobranca
and	b.nr_seq_mensalidade = nr_seq_mensalidade_w;

select	max(vl_cobranca)
into	vl_cobranca_w
from	titulo_receber_cobr
where	nr_seq_cobranca	= nr_lote_desc_folha_w
and	nr_titulo	= nr_titulo_w;

if	(ie_tipo_item_p = 'I') then
	if	(vl_incluir_p > nvl(vl_cobranca_w,0)) then
		ds_retorno_w	:= vl_incluir_p - nvl(vl_cobranca_w,0);
	end if;
elsif	(ie_tipo_item_p = 'E') then
	if	(vl_incluir_p < nvl(vl_cobranca_w,0)) then
		ds_retorno_w	:= nvl(vl_cobranca_w,0) - vl_incluir_p;
	end if;
elsif	(ie_tipo_item_p = 'TI') then /* Total incluir */
	ds_retorno_w	:= vl_incluir_p;
elsif	(ie_tipo_item_p = 'TE') then /* Total excluir */
	ds_retorno_w	:= vl_cobranca_w;
elsif	(ie_tipo_item_p	= 'VI') then /* Verifica se existe valor � incluir */
	if	(nvl(vl_incluir_p,0) <> 0) then
	ds_retorno_w := 1;
	end if;
elsif	(ie_tipo_item_p	= 'VE') then /* Verifica se existe valor � exlcuir */
	if	(nvl(vl_cobranca_w,0) <> 0) then
	ds_retorno_w := 1;
	end if;
end if;

if	(nvl(vl_cobranca_w,0) = vl_incluir_p) then
	ds_retorno_w := 0;
end if;

if	(ie_virgula_p	= 'N') then
ds_retorno_w	:= obter_Valor_sem_virgula(nvl(ds_retorno_w,0));
end if;

if	(ie_tipo_item_p = 'T') then
	if	(vl_incluir_p <> nvl(vl_cobranca_w,0)) then
		ds_retorno_w	:= 1;
	end if;
end if;

return	nvl(ds_retorno_w,0);

end pls_obter_vl_excluir_afpergs;
/