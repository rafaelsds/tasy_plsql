CREATE OR REPLACE
FUNCTION Obter_Endereco_pf(cd_pessoa_fisica_p	VARCHAR2, 
			  ie_opcao_p	VARCHAR2)
			RETURN VARCHAR2 IS

ds_endereco_w			VARCHAR2(255);
cd_cep_w			VARCHAR2(40);
ds_complemento_w		VARCHAR2(255);
nr_endereco_w			NUMBER;
sg_estado_w			compl_pessoa_fisica.sg_estado%type;
ds_retorno_w			VARCHAR2(255);

/*
END	= Endereco
CEP	= CEP
UF	= Unidade Federativa
*/		   
BEGIN

if	(ie_opcao_p = 'END') then
	select	max(a.ds_endereco),
		max(a.ds_complemento),
		max(a.nr_endereco)
	into	ds_endereco_w,
		ds_complemento_w,
		nr_endereco_w
	from	compl_pessoa_fisica a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p;

	if	(ds_complemento_w is not null) then
		ds_retorno_w := ds_endereco_w ||','|| nr_endereco_w || ',' || ds_complemento_w;
	else	
		ds_retorno_w := ds_endereco_w ||','|| nr_endereco_w;
	end if;

elsif	(ie_opcao_p = 'CEP') then
	select	max(a.cd_cep)
	into	cd_cep_w
	from	compl_pessoa_fisica a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p;

	ds_retorno_w := cd_cep_w;
elsif	(ie_opcao_p = 'UF') then
	select	max(sg_estado)
	into	sg_estado_w
	from	compl_pessoa_fisica a
	where 	a.cd_pessoa_fisica = cd_pessoa_fisica_p;

	ds_retorno_w := sg_estado_w;
end if;

return	ds_retorno_w;
	
END Obter_Endereco_pf;
/
