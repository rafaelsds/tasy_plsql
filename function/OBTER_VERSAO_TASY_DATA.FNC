create or replace function obter_versao_tasy_data(dt_referencia_p    date,
        ie_tipo_p    varchar2)
        return varchar2 is

/*
IE_TIPO_P
'D' - Diaria
'S' - Semanal
*/

cd_versao_w  varchar2(15)  := null;
dt_versao_w  date;

begin

if  (dt_referencia_p is not null) then
  if  (ie_tipo_p = 'D') then
    select  min(a.dt_versao)
    into  dt_versao_w
    from  aplicacao_tasy_versao a
    where  a.cd_aplicacao_tasy  in ('Tasy','HIS')
    and  a.dt_versao  > dt_referencia_p
    and  a.dt_versao  < trunc(a.dt_versao,'dd') + 1;
  elsif  (ie_tipo_p = 'S') then
    select  min(a.dt_versao)
    into  dt_versao_w
    from  aplicacao_tasy_versao a
    where  a.cd_aplicacao_tasy  in ('Tasy','HIS')
    and  a.dt_versao  > dt_referencia_p
    and  a.dt_versao  < trunc(a.dt_versao,'dd') + 7;
  elsif  (ie_tipo_p  = 'V') then
    select  max(a.dt_versao)
    into  dt_versao_w
    from  aplicacao_tasy_versao a
    where  a.cd_aplicacao_tasy  in ('Tasy','HIS')
    and  a.dt_versao  < dt_referencia_p;
  end if;

  if  (dt_versao_w is not null) then
    select  max(a.cd_versao)
    into  cd_versao_w
    from  aplicacao_tasy_versao a
    where  a.cd_aplicacao_tasy  in ('Tasy','HIS')
    and  a.dt_versao  = dt_versao_w;
  end if;
end if;

return  cd_versao_w;

end obter_versao_tasy_data;
/
