--  utilizada apenas no Hospital Sa�de de Caxias do Sul no relat�rio CATE 580
create or replace
function obter_dados_ult_compra_data_2	(cd_estabelecimento_p	Number,
				cd_material_p		Number,
				cd_local_estoque_p	Number,
				dt_final_p			Date,
				qt_dias_p			Number,
				ie_opcao_p		Varchar2)
	 		    	return Varchar2 is

/*
'DT' -> Data da �tima compra
'CM' -> Custo m�dio NF
*/

ds_retorno_w			Varchar2(255);
nr_sequencia_w			Number(10);
dt_emissao_w			Date;
vl_custo_medio_nf_w		Number(15,4);
dt_final_w			date;
			
begin
dt_final_w := (TRUNC(NVL(dt_final_p,SYSDATE),'dd') + 86399/86400);

if 	(nvl(qt_dias_p,0) <> 0) then
	select 	dt_emissao,
		vl_custo_medio_nf
	into	dt_emissao_w,
		vl_custo_medio_nf_w
	from	(select a.dt_emissao dt_emissao,
		decode(b.vl_liquido,0,0,dividir(b.vl_liquido, b.qt_item_estoque)) vl_custo_medio_nf
		from	nota_fiscal a,
			nota_fiscal_item b,
			natureza_operacao c,
			operacao_nota d
		where	a.nr_sequencia = b.nr_sequencia
		and 	a.dt_emissao between (dt_final_p - nvl(qt_dias_p,0)) and dt_final_p
		and	a.cd_natureza_operacao = c.cd_natureza_operacao
		and	a.cd_operacao_nf = d.cd_operacao_nf
		and	((a.cd_estabelecimento = cd_estabelecimento_p) OR (cd_estabelecimento_p is null))
		and	((b.cd_local_estoque = cd_local_estoque_p) OR (nvl(cd_local_estoque_p,0) = 0))
		and	b.cd_material = cd_material_p
		and	a.ie_situacao = '1'
		and	c.ie_entrada_saida = 'E'
		and	d.ie_ultima_compra = 'S') a
	where   a.vl_custo_medio_nf = (select max(a.vl_custo_medio_nf) from dual)
	and	rownum < 2;
else
	select	max(a.dt_emissao)
	into	dt_emissao_w
	from	nota_fiscal a,
		nota_fiscal_item b,
		natureza_operacao c,
		operacao_nota d
	where	a.nr_sequencia = b.nr_sequencia
	and 	a.dt_emissao <= dt_final_w
	and	a.cd_natureza_operacao = c.cd_natureza_operacao
	and	a.cd_operacao_nf = d.cd_operacao_nf
	and	((a.cd_estabelecimento = cd_estabelecimento_p) OR (cd_estabelecimento_p is null))
	and	((b.cd_local_estoque = cd_local_estoque_p) OR (nvl(cd_local_estoque_p,0) = 0))
	and	b.cd_material = cd_material_p
	and	a.ie_situacao = '1'
	and	c.ie_entrada_saida = 'E'
	and	d.ie_ultima_compra = 'S';

	select	max(decode(b.vl_liquido,0,0,dividir(b.vl_liquido, b.qt_item_estoque)))
	into	vl_custo_medio_nf_w
	from	nota_fiscal a,
		nota_fiscal_item b,
		natureza_operacao c,
		operacao_nota d
	where	a.nr_sequencia = b.nr_sequencia
	and 	a.dt_emissao  = dt_emissao_w
	and	a.cd_natureza_operacao = c.cd_natureza_operacao
	and	a.cd_operacao_nf = d.cd_operacao_nf
	and	((a.cd_estabelecimento = cd_estabelecimento_p) OR (cd_estabelecimento_p is null))
	and	((b.cd_local_estoque = cd_local_estoque_p) OR (nvl(cd_local_estoque_p,0) = 0))
	and	b.cd_material = cd_material_p
	and	a.ie_situacao = '1'
	and	c.ie_entrada_saida = 'E'
	and	d.ie_ultima_compra = 'S';
end if;

if 	(ie_opcao_p = 'DT') then
	ds_retorno_w := trunc(dt_emissao_w, 'dd');
elsif	(ie_opcao_p = 'CM') then
	ds_retorno_w := vl_custo_medio_nf_w;
end if;

return	ds_retorno_w;

end obter_dados_ult_compra_data_2;
/