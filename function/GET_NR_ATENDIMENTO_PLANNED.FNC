create or replace function get_nr_atendimento_planned( nr_atendimento_p	    number) return varchar2 is
			
nr_atendimento_w    atendimento_paciente.nr_atendimento%type;
ds_retorno_w        varchar2(100);

begin

ds_retorno_w := '';

if (nr_atendimento_p is not null) then   

    if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_DE') then

        select  max(apt.nr_atendimento)
        into    nr_atendimento_w
        from    atendimento_paciente apt
        where   apt.nr_seq_episodio in (    select  max(ap.nr_seq_episodio)
                                            from    atendimento_paciente ap
                                            where   ap.nr_atendimento = nr_atendimento_p)
        and     apt.dt_cancelamento is null
        and     apt.dt_alta is null
        and     obter_se_atendimento_futuro(apt.nr_atendimento) = 'N';

        if (nr_atendimento_w is not null) then
            ds_retorno_w:= nr_atendimento_w;
        end if;
    else
		ds_retorno_w:= nr_atendimento_p;
	end if;

end if;
return ds_retorno_w;
end get_nr_atendimento_planned;
/
