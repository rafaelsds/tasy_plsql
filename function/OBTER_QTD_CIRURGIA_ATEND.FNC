create or replace
function obter_qtd_cirurgia_atend(nr_atendimento_p	number)
				return number is

qt_cirurgia_w	number(10);

begin

select	count(*)
into	qt_cirurgia_w
from	cirurgia
where	nr_atendimento = nr_atendimento_p;

return	qt_cirurgia_w;

end obter_qtd_cirurgia_atend;
/
