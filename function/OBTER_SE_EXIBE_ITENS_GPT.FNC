create or replace
function obter_se_exibe_itens_gpt(	ds_itens_p		varchar2,
					nr_atendimento_p	number,
					ie_tipo_gpt_p		varchar2)
 		    	return varchar2 is

ds_lista_w		varchar2(1000);
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);
ie_item_w		varchar2(10);
ie_item_valido_w	varchar2(1) := 'N';

begin

ds_lista_w := ds_itens_p;

if	(substr(ds_lista_w,length(ds_lista_w) - 1, length(ds_lista_w))	<> ',') then
	ds_lista_w	:= ds_lista_w ||',';
end if;

while (ds_lista_w is not null and ie_item_valido_w = 'N') loop 
	begin

	tam_lista_w		:= length(ds_lista_w);
	ie_pos_virgula_w	:= instr(ds_lista_w,',');
	
	if	(ie_pos_virgula_w <> 0) then
		ie_item_w		:= substr(ds_lista_w,1,(ie_pos_virgula_w - 1));
		ds_lista_w		:= substr(ds_lista_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;
	
	ie_item_valido_w := obter_itens_gpt(nr_atendimento_p,ie_tipo_gpt_p,ie_item_w);
	
	end;
	
end loop;

return	ie_item_valido_w;

end obter_se_exibe_itens_gpt;
/
