create or replace
function obter_cid_secun_desc_atend(	nr_atendimento_p		number,
					qt_diagnosticos_p		number)
					return varchar2 is

cd_doenca_w		varchar2(10);
ds_retorno_w		varchar2(2000) := '';


cursor C01	is
	select	cd_doenca
	from	diagnostico_doenca
	where	nr_atendimento	= nr_atendimento_p
	and	ie_classificacao_doenca = 'S'
	and	rownum <= qt_diagnosticos_p
	order by dt_diagnostico desc;

begin

open c01;
	loop
	fetch c01 into
		cd_doenca_w;
	exit when c01%notfound;
	begin
	
	ds_retorno_w := cd_doenca_w || ' - ' || obter_desc_cid(cd_doenca_w) || Chr(13) || ds_retorno_w;
	
	end;
	end loop;
close c01;	

ds_retorno_w := substr(ds_retorno_w, 1, length(ds_retorno_w) - 2);

return ds_retorno_w;

end obter_cid_secun_desc_atend;
/