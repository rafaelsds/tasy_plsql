create or replace
function obter_desc_motivo_solic(	nr_sequencia_p		number)
 		    	return varchar2 is

ds_motivo_w			motivo_solic_compra.ds_motivo%type;
			
begin

select	ds_motivo
into	ds_motivo_w
from	motivo_solic_compra
where	nr_sequencia = nr_sequencia_p;


return	ds_motivo_w;

end obter_desc_motivo_solic;
/