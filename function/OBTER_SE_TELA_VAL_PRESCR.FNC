create or replace
function obter_se_tela_val_prescr	(cd_estabelecimento_p	number)
 		    	return varchar2 is

ie_exibe_tela_w		varchar2(1) := 'S';
hr_inicial_w		varchar2(5);
hr_final_w			varchar2(5);
dt_ini_w			date;
dt_fim_w			date;

begin

if	(nvl(cd_estabelecimento_p,0) > 0) then	
	select 	max(hr_inicial_tela),
			max(hr_final_tela)
	into	hr_inicial_w,
			hr_final_w
	from	parametro_medico
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(hr_inicial_w is not null) and
		(hr_final_w is not null) then
		
		dt_ini_w := to_date(to_char(sysdate,'dd/mm/yyyy ') || hr_inicial_w || ':00','dd/mm/yyyy hh24:mi:ss');
		dt_fim_w := to_date(to_char(sysdate,'dd/mm/yyyy ') || hr_final_w   || ':00','dd/mm/yyyy hh24:mi:ss');		
		
		select	nvl(max('S'),'N')
		into	ie_exibe_tela_w
		from	dual
		where	sysdate between dt_ini_w and dt_fim_w;	
	end if;
	
end if;

return	ie_exibe_tela_w;

end obter_se_tela_val_prescr;
/