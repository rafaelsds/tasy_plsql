create or replace
function obter_produtividade_sup ( dt_inicio_p		date,
									dt_fim_p		date)
								return number is
qt_funcionario_w	number;
qt_os_w				number;
ds_retorno_w		number;
begin
select	count(*) qt_funcionario,
		sum(count(distinct a.nr_sequencia)) qt_os
into	qt_funcionario_w,
		qt_os_w
from	man_ordem_servico a,
		man_ordem_serv_ativ b
where	b.nr_seq_ordem_serv = a.nr_sequencia
and		trunc(b.dt_atividade) between dt_inicio_p and dt_fim_p + 86399/86400
and		trunc(b.dt_atividade) = (select	trunc(min(dt_atividade))
							  	from	man_ordem_serv_ativ y
								where	y.nr_seq_ordem_serv = a.nr_sequencia
								and		y.nm_usuario_exec = a.nm_usuario_exec)
and		(a.nr_grupo_trabalho = '2')
and		(obter_setor_usuario(a.nm_usuario_exec) = '4')
group by a.nm_usuario_exec
order by 2;

if ((qt_funcionario_w <> 0) and (qt_os_w <> 0)) then
	ds_retorno_w := qt_os_w / qt_funcionario_w;
else
	ds_retorno_w := 0;
end if;
return ds_retorno_w;
end obter_produtividade_sup ;
/