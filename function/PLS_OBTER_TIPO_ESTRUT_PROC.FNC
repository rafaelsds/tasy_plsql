create or replace
function pls_obter_tipo_estrut_proc(	cd_procedimento_p	number,
					ie_origem_proced_p	number,
					cd_grupo_proc_p		number,
					cd_especialidade_proc_p	number,
					cd_area_procedimento_p	number)
						return varchar2 is
					
ds_retorno_w		varchar2(255)	:= null;

begin

if	(cd_procedimento_p is not null) then
	ds_retorno_w	:= 'Procedimento';
elsif	(cd_grupo_proc_p is not null) then
	ds_retorno_w	:= 'Grupo';
elsif	(cd_especialidade_proc_p is not null) then
	ds_retorno_w	:= 'Especialidade';
elsif	(cd_area_procedimento_p	is not null) then
	ds_retorno_w	:= '�rea';
end if;

return	ds_retorno_w;

end pls_obter_tipo_estrut_proc;
/