create or replace 
FUNCTION Obter_pixeon_accession_Number(
				nr_prescricao_p		Number,
				cd_procedimento_p		Number,
				ie_origem_proced_p		Number)
				return Varchar2 is

ds_accession_number_w	varchar2(30);
cd_tipo_procedimento_w	Number(15,0);

BEGIN

select campo_numerico(obter_tipo_procedimento(cd_procedimento_p, ie_origem_proced_p, 'C'))
into	cd_tipo_procedimento_w
from	dual;

ds_accession_number_w	:= 	to_char(cd_tipo_procedimento_w,'FM000') ||
					to_char(nr_prescricao_p,'FM00000000');
return	ds_accession_number_w;

END Obter_pixeon_accession_Number;
/