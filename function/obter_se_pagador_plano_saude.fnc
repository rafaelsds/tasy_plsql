create or replace
function obter_se_pagador_plano_saude(cd_pessoa_fisica_p varchar2)
													return number is

qt_pagador_w			number(10);

begin

select   count(*) 
into qt_pagador_w
from     pessoa_fisica a, 
         pls_contrato_pagador b,
			pls_segurado c
where    a.cd_pessoa_fisica = b.cd_pessoa_fisica 
and      (b.dt_rescisao is null or b.dt_rescisao > sysdate) 
and      a.cd_pessoa_fisica = cd_pessoa_fisica_p
and		c.cd_pessoa_fisica = b.cd_pessoa_fisica
and		c.ie_tipo_segurado <> 'B';

return qt_pagador_w;

end;
/
