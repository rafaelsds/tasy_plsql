create or replace FUNCTION GET_LOAN_INSTLMNT_PYMNT_DATE 
(
  NR_TITULO_P IN NUMBER 
) RETURN DATE IS 

DT_PAGAMENTO_W DATE := null;

BEGIN

if	(NR_TITULO_P is not null) then

  select x.dt_liquidacao 
  into DT_PAGAMENTO_W
  from titulo_pagar x 
  where x.nr_titulo = NR_TITULO_P
  and x.IE_SITUACAO <> 'C';

end	if;

  RETURN DT_PAGAMENTO_W;
  
END GET_LOAN_INSTLMNT_PYMNT_DATE;
/