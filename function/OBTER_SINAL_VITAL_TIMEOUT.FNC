create or replace function obter_sinal_vital_timeout(nr_seq_pepo_p number,
													 nr_seq_cirurgia_p number,
													 nr_seq_modelo_p number, 
													 ie_sinal_vital_p varchar2) return varchar2 is
										 
cursor cVitalTime is
select pmss.nr_seq_pepo_sv, ps.*,pr.ie_verif_timeout, obter_valor_dominio(1768,ps.ie_sinal_vital) desc_sv, pr.qt_timeout_sv,decode(ps.nm_tabela,'ATENDIMENTO_SINAL_VITAL', 'DT_SINAL_VITAL','ATEND_MONIT_HEMOD','DT_MONITORACAO','DT_MONITORIZACAO') dt_reg
from pepo_modelo_secao_sv pmss 
inner join pepo_modelo_secao pms on pmss.nr_seq_modelo_secao = pms.nr_sequencia
inner join pepo_modelo pm on pms.nr_seq_modelo = pm.nr_sequencia
inner join pepo_sinal_vital psv on pmss.nr_seq_pepo_sv = psv.nr_sequencia
inner join pepo_sv ps on ps.ie_sinal_vital = psv.ie_sinal_vital
inner join pepo_modelo_sec_sv_regra pr on pr.nr_seq_modelo_secao_sv = pmss.nr_sequencia
where pm.nr_sequencia = nr_seq_modelo_p
  and psv.ie_sinal_vital = nvl(ie_sinal_vital_p, psv.ie_sinal_vital)
  and ps.nm_tabela <> 'ATEND_ANAL_BIOQ_PORT'
  and ps.nr_sequencia = (select max(nr_sequencia) from pepo_sv svP where ps.nm_tabela = svP.nm_tabela and ps.nm_atributo = svp.nm_atributo);

w_query varchar2(4000);		
w_count number(5);
w_messageRet varchar2(4000);
w_check_sv number(10);											 
													 
begin	
	select max(maxTurnOn) into w_check_sv 
	  from ( select (select nvl(max(ecp.nr_sequencia) ,0)
					   from evento_cirurgia_paciente ecp, 
							evento_cirurgia ec
					  where ecp.nr_seq_evento = ec.nr_sequencia
						and ec.ie_inicia_integracao  = 'S'
						and (nr_cirurgia = nr_seq_cirurgia_p or ecp.nr_seq_pepo = nr_seq_pepo_p)
					) maxTurnOn,
					(select nvl(max(ecp.nr_sequencia),0) 
					   from evento_cirurgia_paciente ecp, evento_cirurgia ec
					  where ecp.nr_seq_evento = ec.nr_sequencia
						and ec.ie_inicia_integracao  = 'N'
						and (nr_cirurgia = nr_seq_cirurgia_p or ecp.nr_seq_pepo = nr_seq_pepo_p)
					) maxturnoff 
				from dual)
	 where maxTurnOn > maxturnoff;	
	
	if(w_check_sv is not null) then
		for r1 in cVitalTime loop
			if(r1.ie_verif_timeout = 'S') then
				w_query := 'select count(1) from '|| r1.nm_tabela;
				w_query := w_query || ' where ((:nr_seq_cirurgia_p is not null and nr_cirurgia = nvl(:nr_seq_cirurgia_p,0)) or (:nr_seq_pepo_p is not null and nr_seq_pepo = nvl(:nr_seq_pepo_p,0)))';
				w_query := w_query || ' and ' || r1.nm_atributo  ||' is not null ';
				w_query := w_query || ' and ie_integracao = :ie_integracao'; 
				w_query := w_query || ' and ((sysdate - '||r1.dt_reg||') * 24 * 60 * 60) < '|| r1.qt_timeout_sv;
				
				dbms_output.put_line(w_query);
				execute immediate w_query into w_count using nr_seq_cirurgia_p,nr_seq_cirurgia_p, nr_seq_pepo_p, nr_seq_pepo_p,'S';
				
				if (w_count = 0) then
					if(ie_sinal_vital_p is null) then
						if (w_messageRet is null) then
							w_messageRet := '<b>'|| wheb_mensagem_pck.get_texto(1164754) ||'</b><br>';
						else
							w_messageRet := w_messageRet ||', ';
						end if;
						w_messageRet := w_messageRet || r1.desc_sv;
					else 
						w_messageRet := 'S';
					end if;
				end if;
			end if;
		end loop;
		if(w_messageRet is not null and w_messageRet <> 'S') then
			w_messageRet := w_messageRet || '</br>';
		end if;
	end if;
return w_messageRet;
end OBTER_SINAL_VITAL_TIMEOUT;
/
