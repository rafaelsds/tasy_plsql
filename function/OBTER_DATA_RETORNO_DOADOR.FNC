create or replace 
function obter_data_retorno_doador( cd_pessoa_fisica_p	number)
 		    	return date deterministic is

ie_sexo_w		varchar2(1);
qt_dias_ult_dia_w	number(10);
qt_doacao_ano_regra_w	number(10);
qt_doacao_ano_doador_w	number(10);
dt_retorno_w		date;
ie_tipo_coleta_w    	san_doacao.ie_tipo_coleta%type; 
nr_seq_doacao_w      	san_doacao.nr_sequencia%type; 
nr_seq_regra_w      	san_regra_doacao.nr_sequencia%type; 
nr_seq_ultima_doacao_w 	san_doacao.nr_sequencia%type;
ie_tipo_coleta_ant_w    san_doacao.ie_tipo_coleta%type;
dt_ano_anterior_w	Date;
dt_ult_doacao_w		Date;

begin

ie_sexo_w := obter_sexo_pf(cd_pessoa_fisica_p, 'C');

if	(ie_sexo_w is not null) then
	
	select 	max(nr_sequencia),
		max(dt_doacao)
	into	nr_seq_doacao_w,
		dt_ult_doacao_w
	from 	san_doacao
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p;

	dt_ano_anterior_w := trunc(PKG_DATE_UTILS.ADD_MONTH(dt_ult_doacao_w,-12,0));

	select 	max(nr_sequencia) 
	into	nr_seq_ultima_doacao_w
	from 	san_doacao
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p
	and 	dt_fim_coleta is not null;		
	
	select	max(ie_tipo_coleta)
	into 	ie_tipo_coleta_w
	from 	san_doacao
	where 	nr_sequencia = nr_seq_doacao_w; 
		
	if	(nr_seq_ultima_doacao_w is not null) then
		select	max(ie_tipo_coleta)
		into 	ie_tipo_coleta_ant_w
		from 	san_doacao
		where 	nr_sequencia = nr_seq_ultima_doacao_w;
	end if;

	select 	max(nr_sequencia)
	into	nr_seq_regra_w
	from 	san_regra_doacao
	where 	ie_sexo = ie_sexo_w
	and 	nr_tipo_doacao = ie_tipo_coleta_w
	and	(nr_tipo_doacao_anterior = ie_tipo_coleta_ant_w or (nr_tipo_doacao_anterior is null and ie_tipo_coleta_ant_w is null));
	
	if	(nr_seq_regra_w is null) then
		select 	max(nr_sequencia)
		into	nr_seq_regra_w
		from 	san_regra_doacao
		where 	ie_sexo = ie_sexo_w
		and	nr_tipo_doacao is null
		and	(nr_tipo_doacao_anterior = ie_tipo_coleta_ant_w or (nr_tipo_doacao_anterior is null and ie_tipo_coleta_ant_w is null));
	end if;
	
	if	(nr_seq_regra_w is null) then
		select 	max(nr_sequencia)
		into	nr_seq_regra_w
		from 	san_regra_doacao
		where 	ie_sexo = ie_sexo_w
		and 	nr_tipo_doacao is null
		and	nr_tipo_doacao_anterior is null;
	end if;
	
	if	(nr_seq_regra_w is not null) then
	
		select 	max(qt_dias_ult_dia), 
			max(qt_doacao_ano)
		into	qt_dias_ult_dia_w,
			qt_doacao_ano_regra_w
		from 	san_regra_doacao
		where 	nr_sequencia = nr_seq_regra_w;

		/*Adicionado mais um para considerar a doacao do dia como mais uma doacao no ano*/
		select	count(1) + 1
		into	qt_doacao_ano_doador_w
		from   	san_doacao
		where  	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_doacao between dt_ano_anterior_w and sysdate
		and 	dt_fim_coleta is not null;

		if 	(qt_doacao_ano_doador_w < qt_doacao_ano_regra_w) then
			dt_retorno_w := trunc(dt_ult_doacao_w + qt_dias_ult_dia_w);
		else
			dt_retorno_w := PKG_DATE_UTILS.ADD_MONTH(dt_ult_doacao_w, 12, 0);
		end if;
	end if;
  
end if;

return	dt_retorno_w;

end obter_data_retorno_doador;
/
