create or replace
function obter_horario_orig_reapraz	(ie_tipo_item_p	varchar2,
				nr_seq_horario_p	number)
				return date is

dt_horario_w		date;
qt_min_reaprazado_w	number(10,0);
dt_original_w		date := null;

begin
if	(ie_tipo_item_p is not null) and
	(nr_seq_horario_p is not null) then

	if	(ie_tipo_item_p = 'D') then

		select	max(dt_horario),
			nvl(max(qt_hor_reaprazamento),0)
		into	dt_horario_w,
			qt_min_reaprazado_w
		from	prescr_dieta_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

	elsif	(ie_tipo_item_p in ('S','M','MAT','IA','IAG','LD')) then

		select	max(dt_horario),
			nvl(max(qt_hor_reaprazamento),0)
		into	dt_horario_w,
			qt_min_reaprazado_w
		from	prescr_mat_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

	elsif	(ie_tipo_item_p in ('P','G','C','I')) then

		select	max(dt_horario),
			nvl(max(qt_hor_reaprazamento),0)
		into	dt_horario_w,
			qt_min_reaprazado_w
		from	prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

	elsif	(ie_tipo_item_p = 'R') then

		select	max(dt_horario),
			nvl(max(qt_hor_reaprazamento),0)
		into	dt_horario_w,
			qt_min_reaprazado_w
		from	prescr_rec_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

	elsif	(ie_tipo_item_p = 'E') then

		select	max(dt_horario),
			nvl(max(qt_hor_reaprazamento),0)
		into	dt_horario_w,
			qt_min_reaprazado_w
		from	pe_prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p;
		
	elsif	(ie_tipo_item_p = 'O') then

		select	max(dt_horario),
			nvl(max(qt_hor_reaprazamento),0)
		into	dt_horario_w,
			qt_min_reaprazado_w
		from	prescr_gasoterapia_hor
		where	nr_sequencia = nr_seq_horario_p;
				
	end if;

	if	(dt_horario_w is not null) and
		(qt_min_reaprazado_w <> 0) then
		if	(qt_min_reaprazado_w > 0) then
			dt_original_w	:= dt_horario_w - (qt_min_reaprazado_w / 1440);
		else
			dt_original_w	:= dt_horario_w + (abs(qt_min_reaprazado_w) / 1440);
		end if;
	end if;

end if;

return dt_original_w;

end obter_horario_orig_reapraz;
/