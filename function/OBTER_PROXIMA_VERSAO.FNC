create or replace
function obter_proxima_versao(	dt_referencia_p date,
				nr_seq_local_p	number default 0,
				ie_opcao_retorno_p		varchar2 default 'E'	)
		return varchar2 is
	
/*	IE_OPCAO_RETORNO_P

	E: Extenso: Atualizar o sistema com a vers�o do dia dd/mm/yyyy.
	A: Abreviado (s� a data): dd/mm/yyyy
	
*/
	
ds_dia_versao_w			varchar2(100);
ie_dia_w				number(1);
dt_referencia_w			date;
dt_versao_w			date;
cd_estab_w			number(5);
nr_seq_versao_alteracao_w		number(10);
ds_versao_mmed_w		varchar2(80);

dt_prim_versao_quinzenal_w		date := to_date('08/08/2014 17:00:00','dd/mm/yyyy hh24:mi:ss');
qt_dias_desde_ult_geracao_w	number(10);

begin
	cd_estab_w := wheb_usuario_pck.get_cd_estabelecimento;

	/*  Libera��o das vers�es do Tasy de 15 em 15 dias.  */
	dt_referencia_w	:= nvl(dt_referencia_p,sysdate);

	/* tratamento para vers�es no final dos anos (para colocar a data fixa do ano seguinte)  TODO ANO DEVER� SER INSERIDO UM NOVO IF */
	if	((dt_referencia_w >= to_date('28/11/2014 17:00:00','dd/mm/yyyy hh24:mi:ss')) and (dt_referencia_w <= to_date('09/01/2015 17:59:59','dd/mm/yyyy hh24:mi:ss'))) then
		dt_versao_w := to_date('27/01/2015','dd/mm/yyyy');
	elsif	((dt_referencia_w >= to_date('23/12/2015 17:00:00','dd/mm/yyyy hh24:mi:ss')) and (dt_referencia_w <= to_date('08/01/2016 17:59:59','dd/mm/yyyy hh24:mi:ss'))) then
		dt_versao_w := to_date('26/01/2016','dd/mm/yyyy');
	else
		/* a cada ano esta parte dever� ser alterada para definir o primeiro dia de vers�o do ano */
		case	extract(year from dt_referencia_w)
		when	2014 then dt_prim_versao_quinzenal_w := to_date('08/08/2014 17:00:00','dd/mm/yyyy hh24:mi:ss');
		when	2015 then dt_prim_versao_quinzenal_w := to_date('09/01/2015 17:00:00','dd/mm/yyyy hh24:mi:ss');
		when	2016 then dt_prim_versao_quinzenal_w := to_date('08/01/2016 17:00:00','dd/mm/yyyy hh24:mi:ss');
		else	-- caso a data do primeiro dia da vers�o do ano n�o for definida, assumir� o primeiro dia do ano, �s 17:00 
			dt_prim_versao_quinzenal_w := to_date('01/01/'||to_char(dt_referencia_w,'yyyy')||' 17:00:00','dd/mm/yyyy hh24:mi:ss');
		end case;
		
		/* calcula quantos dias passou desde a �ltima gera��o da vers�o */
		select	mod(trunc(dt_referencia_w) - trunc(dt_prim_versao_quinzenal_w),14)
		into	qt_dias_desde_ult_geracao_w
		from	dual;
		
		/* 	considerando que a vers�o ser� liberada 18 dias ap�s a gera��o da vers�o,
			a linha abaixo adiciona � data atual a diferen�a entre os 18 dias e a quantidade de dias que passou desde a �ltima gera��o da vers�o */
		dt_versao_w	:= trunc(dt_referencia_w + (18 - qt_dias_desde_ult_geracao_w));
		
		if	(qt_dias_desde_ult_geracao_w > 0) or						-- caso n�o for o dia da gera��o da vers�o, ou...
			(to_number(to_char(dt_referencia_w,'hh24')) >= 17) then 			-- for o dia da gera��o da vers�o, mas passar das 17:00
			dt_versao_w	:= dt_versao_w + 14;						-- dever� adicionar 15 dias � data da pr�xima libera��o ao cliente.
		end if;
	end if;
	
	if (ie_opcao_retorno_p = 'E') then
		ds_dia_versao_w	:= wheb_mensagem_pck.get_texto(795908) || ' ' || to_char(dt_versao_w, pkg_date_formaters.localize_mask('shortDate', pkg_date_formaters.getUserLanguageTag(wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario))) || '.';
	elsif (ie_opcao_retorno_p = 'A') then
		ds_dia_versao_w := to_char(dt_versao_w,'dd/mm/yyyy');
	end if;


return	ds_dia_versao_w;

end obter_proxima_versao;
/
