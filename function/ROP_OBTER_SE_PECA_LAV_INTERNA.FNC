create or replace
function rop_obter_se_peca_lav_interna(	nr_seq_roupa_p		number)
return varchar2 is

ie_interna_w			varchar2(1) := 'N';
ie_tipo_local_w			varchar2(15);

begin

select	rop_obter_tipo_local(rop_obter_local_atual_roupa(nr_seq_roupa_p))
into	ie_tipo_local_w
from	dual;

if	(upper(ie_tipo_local_w) = 'LI') then
	ie_interna_w := 'S';
end if;

return	ie_interna_w;

end rop_obter_se_peca_lav_interna;
/