CREATE OR REPLACE
FUNCTION Obter_QtleitosUTI_Utilizados
			(	cd_tipo_leito_p	number,
				dt_referencia_p	date)
				return number is

qt_leitos_uti_w	number(5);

BEGIN

select	nvl(sum(qt_diaria),0)
	into	qt_leitos_uti_w
	from	sus_leito_movto
	where	trunc(dt_referencia,'month')	= trunc(dt_referencia_p,'month')
	and	cd_tipo_leito			= cd_tipo_leito_p;

Return qt_leitos_uti_w;

END Obter_QtleitosUTI_Utilizados;
/