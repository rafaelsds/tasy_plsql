create or replace
function obter_conv_unid_med_tox
		(cd_material_p		number,
		cd_unid_med_p		varchar2,
		qt_dose_p		number,
		ie_conversao_p	varchar2 default 'UME')
		return number is


qt_retorno_w		number(18,6);
cd_unid_med_cons_w	varchar2(30);
qt_conversao_w		number(18,6);
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;

begin


cd_unid_med_cons_w	:= upper(substr(obter_dados_material_estab(cd_material_p,cd_estabelecimento_w,nvl(ie_conversao_p,'UME')),1,30));

if	(upper(cd_unid_med_cons_w) = upper(cd_unid_med_p)) then
	qt_retorno_w := qt_dose_p;
else
	select	nvl(max(qt_conversao),1)
	into	qt_conversao_w
	from	material_conversao_unidade
	where	cd_material		= cd_material_p
	and	upper(cd_unidade_medida)= upper(cd_unid_med_cons_w);

	
	qt_retorno_w	:= dividir(qt_dose_p,qt_conversao_w);
	if	qt_conversao_w <> 0 then
		begin
		qt_retorno_w	:= qt_dose_p/qt_conversao_w;
		exception
		when others then
			qt_retorno_w	:= dividir(qt_dose_p,qt_conversao_w);
		end;
	end if;
end if;

return qt_retorno_w;

end obter_conv_unid_med_tox;
/
