create or replace
function obter_se_existe_itens_zerado(	nr_sequencia_p			number,
					ie_permite_lib_item_zerado_p	varchar2)
				return varchar2 is

ie_existe_w	varchar2(1) := 'N';
nr_seq_item_w	number(10);
ie_opcao_w	varchar2(1);

Cursor C01 is
	select	nr_seq_matpaci,
		'M' ie_opcao
	from	auditoria_matpaci
	where	nr_seq_auditoria = nr_sequencia_p	
	union all
	select	nr_seq_propaci,
		'P' ie_opcao
	from	auditoria_propaci
	where	nr_seq_auditoria = nr_sequencia_p
	and 	ie_permite_lib_item_zerado_p <> 'M';

begin

open C01;
loop
fetch C01 into	
	nr_seq_item_w,
	ie_opcao_w;
exit when C01%notfound;
	begin
	if	(ie_opcao_w = 'M') then
		select	nvl(max('S'),'N')
		into	ie_existe_w
		from	material_atend_paciente
		where	nr_sequencia = nr_seq_item_w
		and	vl_material = 0;
		
		if	(ie_existe_w = 'S') then
			exit;
		end if;
	end if;
	
	if	(ie_opcao_w = 'P') then
		select	nvl(max('S'),'N')
		into	ie_existe_w
		from	procedimento_paciente
		where	nr_sequencia = nr_seq_item_w
		and	vl_procedimento = 0;
		
		if	(ie_existe_w = 'S') then
			exit;
		end if;
	end if;
	end;
end loop;
close C01;

return	ie_existe_w;

end obter_se_existe_itens_zerado;
/