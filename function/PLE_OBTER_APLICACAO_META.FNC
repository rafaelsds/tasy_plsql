CREATE OR REPLACE
FUNCTION ple_obter_aplicacao_meta(	nr_seq_meta_pe_p	number,
					nr_seq_indicador_p	number)
					return varchar2 is

ds_retorno_w	Varchar2(15) := '';

BEGIN

if	(nr_seq_meta_pe_p is not null) then
	select	c.ie_aplicacao
	into	ds_retorno_w
	from	ple_edicao c,
		ple_objetivo b,
		ple_meta a   
	where	a.nr_seq_objetivo	= b.nr_sequencia
	and	b.nr_seq_edicao	= c.nr_sequencia
	and	a.nr_sequencia	= nr_seq_meta_pe_p;
elsif	(nr_seq_indicador_p is not null) then
	select	c.ie_aplicacao
	into	ds_retorno_w
	from	ple_edicao c,
		ple_objetivo b,
		indicador_gestao a   
	where	a.nr_seq_objetivo_bsc	= b.nr_sequencia
	and	b.nr_seq_edicao		= c.nr_sequencia
	and	a.nr_sequencia		= nr_seq_indicador_p;
end if;

return	ds_retorno_w;

END ple_obter_aplicacao_meta;
/