create or replace
function pls_obter_dados_familia
			(	cd_familia_p		number,
				nr_seq_contrato_p	number,
				ie_opcao_p		varchar2)
 		    	return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

ie_opcao_p

	ST	Sequ�ncia do titular da fam�lia
	NT	Nome do titular da fam�lia
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(255);
nr_seq_segurado_w	number(10);
nm_pessoa_fisica_w	varchar2(255);
begin

if	(ie_opcao_p = 'ST') then
	select	max(nr_sequencia)
	into	nr_seq_segurado_w
	from	pls_segurado
	where	nr_seq_contrato		= nr_seq_contrato_p
	and	cd_matricula_familia	= cd_familia_p;
	ds_retorno_w	:= nr_seq_segurado_w;
elsif	(ie_opcao_p = 'NT') then
	select	obter_nome_pf(max(cd_pessoa_fisica))
	into	nm_pessoa_fisica_w
	from	pls_segurado
	where	nr_seq_contrato		= nr_seq_contrato_p
	and	cd_matricula_familia	= cd_familia_p;
	ds_retorno_w	:= nm_pessoa_fisica_w;
end if;

return	ds_retorno_w;

end pls_obter_dados_familia;
/