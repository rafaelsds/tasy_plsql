create or replace
function hsm_obter_saldo_tit_protocolo
			(nr_Seq_protocolo_p	number,
			nr_Seq_retorno_p	number) 
			return number is
			
vl_retorno_w		number(15,2);			
vl_saldo_titulo_w	number(15,2);

begin
select	decode(Obter_Se_Negativo(to_number(nvl(max(substr(obter_total_protocolo(v.nr_seq_protocolo),1,40)),0)) - 
	nvl(sum((	select	sum(j.vl_pago) + sum(j.vl_adicional)
		from	convenio_retorno_item j
		where	j.nr_Sequencia		= p.nr_sequencia
		and	j.nr_seq_retorno	=	(select	min(k.nr_seq_retorno)
							from	convenio_retorno_item k,
								convenio_retorno h,
								conta_paciente i
							where	k.nr_Seq_retorno = h.nr_sequencia
							and	k.nr_interno_conta = i.nr_interno_conta
							and	i.nr_Seq_protocolo = v.nr_Seq_protocolo))),0) - 
		nvl(sum((select	nvl(sum(d.vl_glosado),0) +
			nvl(sum(d.vl_amenor),0)
		from	convenio_retorno_item d
		where	d.nr_Sequencia	= p.nr_sequencia	
		and	d.ie_glosa <> 'S')),0) + hsm_obter_saldo_protocolo(v.nr_Seq_protocolo,e.nr_sequencia,'R')),'S',0,to_number(nvl(max(substr(obter_total_protocolo(v.nr_seq_protocolo),1,40)),0)) - 
	nvl(sum((	select	sum(j.vl_pago) + sum(j.vl_adicional)
		from	convenio_retorno_item j
		where	j.nr_Sequencia		= p.nr_sequencia
		and	j.nr_seq_retorno	=	(select	min(k.nr_seq_retorno)
							from	convenio_retorno_item k,
								convenio_retorno h,
								conta_paciente i
							where	k.nr_Seq_retorno = h.nr_sequencia
							and	k.nr_interno_conta = i.nr_interno_conta
							and	i.nr_Seq_protocolo = v.nr_Seq_protocolo))),0) - 
		nvl(sum((select	nvl(sum(d.vl_glosado),0) +
			nvl(sum(d.vl_amenor),0)
		from	convenio_retorno_item d
		where	d.nr_Sequencia	= p.nr_sequencia	
		and	d.ie_glosa <> 'S')),0) + hsm_obter_saldo_protocolo(v.nr_Seq_protocolo,e.nr_sequencia,'R'))
into	vl_saldo_titulo_w
from 	protocolo_convenio v,	
	conta_paciente l,
	convenio_retorno_item p,
	convenio_retorno e,
	convenio_receb a,
	titulo_receber tr
where 	p.nr_seq_retorno	= e.nr_sequencia
and	tr.nr_Seq_protocolo	= v.nr_Seq_protocolo
and	p.nr_interno_conta	= l.nr_interno_conta
and	l.nr_seq_protocolo	= v.nr_seq_protocolo
and	exists	
	(select	1 
	from	convenio_Ret_receb h
	where	h.nr_Seq_receb		= a.nr_sequencia
	and	h.nr_seq_retorno	= e.nr_sequencia)
and	v.nr_Seq_protocolo	= nr_Seq_protocolo_p
and	e.nr_sequencia		= nr_Seq_retorno_p
and	substr(obter_titulo_conta_protocolo(v.nr_seq_protocolo,0),1,254) is not null
group by hsm_obter_saldo_protocolo(v.nr_Seq_protocolo,e.nr_sequencia,'R');

vl_retorno_w	:= vl_saldo_titulo_w;

return vl_retorno_w;

end hsm_obter_saldo_tit_protocolo;
/