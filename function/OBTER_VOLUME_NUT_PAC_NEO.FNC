create or replace
function obter_volume_nut_pac_neo	(nr_seq_nut_neo_p	number)
					return number is

qt_volume_w	number(15,4);

begin
if	(nr_seq_nut_neo_p is not null) then
	select	sum(nvl(obter_vol_elem_nut_pac(nr_sequencia),0))
	into	qt_volume_w
	from	nut_pac_elemento
	where	nr_seq_nut_pac = nr_seq_nut_neo_p;
end if;

return qt_volume_w;

end obter_volume_nut_pac_neo;
/