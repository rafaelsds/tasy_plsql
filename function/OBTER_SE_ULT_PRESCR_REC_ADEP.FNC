create or replace
function obter_se_ult_prescr_rec_adep	(nr_atendimento_p	number,
				nr_prescricao_p		number,
				cd_recomendacao_p	number,
				ds_recomendacao_p	varchar2,
				cd_intervalo_p		varchar2,
				qt_recomendacao_p	number,
				dt_inicial_p		date,
				dt_final_p			date)
				return varchar2 is

nr_prescricao_w	number(14,0);
ie_ultima_w	varchar2(1) := 'S';

begin

select	nvl(max(a.nr_prescricao),0)
into	nr_prescricao_w
from	prescr_medica b,
	prescr_recomendacao a
where	b.nr_prescricao								= a.nr_prescricao
and	a.ds_horarios								is null
and	nvl(to_char(a.cd_recomendacao),a.ds_recomendacao)				= nvl(to_char(cd_recomendacao_p),ds_recomendacao_p)
and	nvl(a.cd_intervalo,'XPTO')							= nvl(cd_intervalo_p,'XPTO')
and	nvl(a.qt_recomendacao,1)							= nvl(qt_recomendacao_p,1)
and	obter_se_prescr_vig_adep(b.dt_inicio_prescr,b.dt_validade_prescr,dt_inicial_p,dt_final_p)	= 'S'
and	b.nr_atendimento										= nr_atendimento_p;

if	(nr_prescricao_p = nr_prescricao_w) then
	ie_ultima_w := 'S';
else
	ie_ultima_w := 'N';
end if;

return ie_ultima_w;

end obter_se_ult_prescr_rec_adep;
/