create or replace function tiss_obter_tab_preco_mat_autor(
								nr_seq_mat_autor_p 	in material_autorizado.nr_sequencia%type,
								nm_usuario_p		in usuario.nm_usuario%type,
								ie_retorno_p		in varchar2 default 'T'
							) return number is
							
cd_tab_mat_preco_w 		number;
ie_origem_preco_w 		number;
vl_retorno_w			number;
dt_referencia_w			date;

/*
ie_retorno_p
'T' => cd_tab_mat_preco_w
'O' => ie_origem_preco_w

*/
							
begin

	vl_retorno_w := null;
	
	if	nvl(nr_seq_mat_autor_p, 0) > 0 then
	
	select	max(a.dt_autorizacao)
	into	dt_referencia_w
	from	autorizacao_convenio a,
		material_autorizado b
	where	b.nr_sequencia = nr_seq_mat_autor_p
	and	b.nr_sequencia = nr_seq_mat_autor_p;

	obter_tabela_preco_mat (
				nr_seq_mat_autor_p, 
				nm_usuario_p, 
				cd_tab_mat_preco_w, 
				ie_origem_preco_w,
				dt_referencia_w
	);
	
	
	
	if ie_retorno_p = 'T' then
		vl_retorno_w := cd_tab_mat_preco_w;
	elsif ie_retorno_p = 'O' then
		vl_retorno_w := ie_origem_preco_w;
	end if;
	
	end if;

	
	
	return vl_retorno_w;
	
end tiss_obter_tab_preco_mat_autor;
/
