create or replace
function obter_se_banco_estab(	nr_seq_conta_banco_p	number,
				cd_estabelecimento_p	number)
				return varchar2 is

ds_retorno_w		varchar2(2) := 'N';
qt_registro_w		number(10);
qt_reg_banco_estab_w	number(10);
cd_estabelecimento_w	banco_estabelecimento.nr_sequencia%type;
cd_estab_financeiro_w	estabelecimento.cd_estabelecimento%type;
					
begin

select	max(a.cd_estabelecimento)
into	cd_estabelecimento_w
from	banco_estabelecimento a
where	a.nr_sequencia	= nr_seq_conta_banco_p;

if	(cd_estabelecimento_w	is not null) then

	cd_estab_financeiro_w	:= obter_estab_financeiro(cd_estabelecimento_w);

end if;

if	(cd_estabelecimento_w	is null) or
	(cd_estabelecimento_w	= cd_estabelecimento_p) or
	(cd_estab_financeiro_w	= cd_estabelecimento_p) then

	ds_retorno_w	:= 'S';

else

	select	count(*)
	into	qt_reg_banco_estab_w
	from	banco_estab_estab
	where	nr_seq_conta_banco	= nr_seq_conta_banco_p
	and	cd_estabelecimento	= cd_estabelecimento_p;

	if	(qt_reg_banco_estab_w > 0) then
		ds_retorno_w := 'S';
	end if;

end if;

return	ds_retorno_w;
end obter_se_banco_estab;
/