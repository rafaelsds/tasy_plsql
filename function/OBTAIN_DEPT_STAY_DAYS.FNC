create or replace FUNCTION obtain_dept_stay_days(cd_setor_atendimento_p number,nr_attendimento_p number,isDay VARCHAR2)
RETURN number IS number_of_days number(10,0) := 0; 
BEGIN
  SELECT Count(*) 
  INTO   number_of_days 
  FROM   atend_paciente_unidade a 
  WHERE  a.nr_atendimento=nr_attendimento_p 
  AND    a.cd_setor_atendimento=cd_setor_atendimento_p 
  AND    a.cd_setor_atendimento=Decode(Get_conversion_qhapdc('HCP_DEPART_TYPE', 'SETOR_ATENDIMENTO', NULL, a.cd_setor_atendimento, 'I'),4, a.cd_setor_atendimento, -99);
  
  IF(number_of_days>0 and isDay='S')then 
  SELECT pkg_date_utils.get_DiffDate(a.dt_entrada_unidade,a.dt_saida_unidade,'DAY') 
  INTO number_of_days
  from   atend_paciente_unidade a 
  WHERE  a.nr_atendimento=nr_attendimento_p 
  AND    a.cd_setor_atendimento=cd_setor_atendimento_p 
  AND    a.cd_setor_atendimento=decode(get_conversion_qhapdc('HCP_DEPART_TYPE', 'SETOR_ATENDIMENTO', NULL, a.cd_setor_atendimento, 'I'),4, a.cd_setor_atendimento, -99);
   
  RETURN number_of_days; 
  
  ELSIF(number_of_days>0 and isDay='N') then
  SELECT pkg_date_utils.get_DiffDate(a.dt_entrada_unidade,a.dt_saida_unidade,'HOUR') 
  INTO number_of_days
  from   atend_paciente_unidade a 
  WHERE  a.nr_atendimento=nr_attendimento_p 
  AND    a.cd_setor_atendimento=cd_setor_atendimento_p 
  AND    a.cd_setor_atendimento=decode(get_conversion_qhapdc('HCP_DEPART_TYPE', 'SETOR_ATENDIMENTO', NULL, a.cd_setor_atendimento, 'I'),4, a.cd_setor_atendimento, -99);
   
  RETURN number_of_days; 
  ELSE
  RETURN number_of_days;
END IF;
END obtain_dept_stay_days;
/