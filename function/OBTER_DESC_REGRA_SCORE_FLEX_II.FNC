create or replace
function obter_desc_regra_score_flex_II ( 	nr_seq_flex_p	Number,
										nr_seq_escala_p	Number,
										ie_opcao_p		varchar2 default 'P')
 		    			return varchar2 is
						
						
/*
ie_opcap_p				Op��o
'P'				Panorama
'R'				Risco de queda


*/						

ds_retorno_w		varchar2(2000)	:= '';
ds_escala_flex_w		Varchar2(255);
ds_escala_w		Varchar2(255);



begin


if 	(nr_seq_flex_p is not null) then
	
	if ( nr_seq_escala_p = 347 and ie_opcao_p = 'P') or
		( nr_seq_escala_p = 142 and ie_opcao_p = 'R' ) then 
	
		select	max(ds_escala)
		into	ds_escala_flex_w
		from	eif_escala_ii
		where	nr_sequencia = nr_seq_flex_p;
		
	end if;

	ds_retorno_w := ds_escala_flex_w;
	
end if;


return	ds_retorno_w;

end obter_desc_regra_score_flex_II;
/
