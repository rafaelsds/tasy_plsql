create or replace
function gel_obter_data_antecipacao_dt(nr_prescricao_p		number,
				nr_seq_prescricao_p		number)
 		    	return date is

ds_retorno_w		date;
nr_sequencia_w		varchar2(80);

begin

select	max(to_char(nr_sequencia))
into	nr_sequencia_w
from	prescr_proc_pedido_entrega
where	nr_prescricao = nr_prescricao_p
and	nr_seq_prescricao = nr_seq_prescricao_p
and	dt_confirmacao is null
and	dt_cancelamento is null;

select  max(dt_antecipacao)
into	ds_retorno_w
from	prescr_proc_pedido_entrega
where	nr_sequencia = nr_sequencia_w;

return ds_retorno_w;

end gel_obter_data_antecipacao_dt;
/
