create or replace
function obter_total_pago_item_glosa(nr_seq_ret_glosa_p	number)
					return number is

nr_seq_propaci_w		number(15);
nr_seq_matpaci_w		number(15);
nr_seq_partic_w			number(10,0);
ds_retorno_w			number(15,4);

begin

select	max(nr_seq_propaci),
	max(nr_seq_matpaci),
	max(nr_seq_partic)
into	nr_seq_propaci_w,
	nr_seq_matpaci_w,
	nr_seq_partic_w
from 	convenio_retorno_glosa
where	nr_sequencia	= nr_seq_ret_glosa_p;

if	(nr_seq_partic_w is not null) then

	select	sum(vl_pago_digitado)
	into	ds_retorno_w
	from	convenio_retorno_glosa
	where	nr_seq_propaci	= nr_seq_propaci_w
	and 	nr_seq_partic	= nr_seq_partic_w;

elsif	(nr_seq_propaci_w is not null) then
	
	select	sum(vl_pago_digitado)
	into	ds_retorno_w
	from	convenio_retorno_glosa
	where	nr_seq_propaci	= nr_seq_propaci_w;

elsif	(nr_seq_matpaci_w is not null) then
	
	select	sum(vl_pago_digitado)
	into	ds_retorno_w
	from	convenio_retorno_glosa
	where	nr_seq_matpaci	= nr_seq_matpaci_w;
end if;

return nvl(ds_retorno_w,0);


end obter_total_pago_item_glosa;
/