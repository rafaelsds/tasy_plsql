create or replace
function Sus_Obter_Se_Exige_Aih_Esp(	cd_procedimento_p		Number,
			ie_origem_proced_p		Number)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(1)	:= 'N';
qt_proc_esp_w		Number(5);

begin
select	count(*)
into	qt_proc_esp_w
from	sus_regra_proc_aih_esp
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p
and	ie_situacao		= 'A';

if	(qt_proc_esp_w > 0) then
	begin
	ds_retorno_w	:= 'S';
	end;
end if;

return	ds_retorno_w;

end Sus_Obter_Se_Exige_Aih_Esp;
/