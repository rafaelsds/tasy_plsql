create or replace
function hsl_obter_dados_dieta_concat(nr_atendimento_p 		number,
									cd_setor_atendimento_p	number,
									dt_inicial_p			date,
									dt_final_p				date,
									nr_seq_servico_p		number,
									ie_opcao_p				varchar2)		
 		    	return varchar2 is
				
/*
D - Dieta
OR - Orientação
OB - Observação
HR - Hora
OP - Observação da Prescrição - Dieta ORAL (prescr_dieta).
*/

ds_resultado_w		varchar2(4000);
ds_resultado_aux_w	varchar2(4000);
				
cursor c01 is
	select	substr(nvl(d.nm_curto,d.nm_dieta),1,80) 
	from	nut_atend_serv_dia a,
			nut_atend_serv_dia_rep b,
			PRESCR_DIETA c,
			dieta d
	where	a.nr_sequencia = b.nr_seq_serv_dia
	and		b.nr_prescr_oral = c.nr_prescricao
	and		c.cd_dieta = d.cd_dieta
	and		a.nr_atendimento = nr_atendimento_p  
	and		a.cd_setor_atendimento = cd_setor_atendimento_p  
	and		a.dt_servico between dt_inicial_p and fim_dia(dt_final_p)
	and		a.nr_seq_servico = nr_seq_servico_p
	and		c.cd_dieta is not null;
	
cursor c02 is
	select	substr(nut_obter_orientacao_servico(a.nr_sequencia),1,250) 
	from	nut_atend_serv_dia a
	where	a.nr_atendimento = nr_atendimento_p  
	and		a.cd_setor_atendimento = cd_setor_atendimento_p  
	and		a.dt_servico between dt_inicial_p and fim_dia(dt_final_p)
	and		a.nr_seq_servico = nr_seq_servico_p
	and		substr(nut_obter_orientacao_servico(a.nr_sequencia),1,250)  is not null;
	
cursor c03 is
	select	a.ds_observacao
	from	nut_atend_serv_dia a
	where	a.nr_atendimento = nr_atendimento_p  
	and		a.cd_setor_atendimento = cd_setor_atendimento_p  
	and		a.dt_servico between dt_inicial_p and fim_dia(dt_final_p)
	and		a.nr_seq_servico = nr_seq_servico_p
	and		a.ds_observacao is not null;
	
cursor c04 is
	select	to_char(a.dt_servico,'hh24:mi:ss')
	from	nut_atend_serv_dia a
	where	a.nr_atendimento = nr_atendimento_p  
	and		a.cd_setor_atendimento = cd_setor_atendimento_p  
	and		a.dt_servico between dt_inicial_p and fim_dia(dt_final_p)
	and		a.nr_seq_servico = nr_seq_servico_p
	and		to_char(a.dt_servico,'hh24:mi:ss') is not null;
	
cursor c05 is

	select	c.ds_observacao
	from	nut_atend_serv_dia a,
			nut_atend_serv_dia_rep b,
			prescr_dieta c
	where	a.nr_sequencia = b.nr_seq_serv_dia
	and		b.nr_prescr_oral = c.nr_prescricao	
	and		a.nr_atendimento = nr_atendimento_p  
	and		a.cd_setor_atendimento = cd_setor_atendimento_p  
	and		a.dt_servico between dt_inicial_p and fim_dia(dt_final_p)
	and		a.nr_seq_servico = nr_seq_servico_p
	and		c.ds_observacao is not null;

begin

if	(ie_opcao_p = 'M') then
	open c01;
	loop
	fetch c01 into ds_resultado_aux_w;
	exit when c01%notfound;	
		ds_resultado_w := ds_resultado_w||' / '||ds_resultado_aux_w;		
	end loop;
	close c01;
		
elsif	(ie_opcao_p = 'OR') then
	open c02;
	loop
	fetch c02 into ds_resultado_aux_w;
	exit when c02%notfound;	
		ds_resultado_w := ds_resultado_w||' / '||ds_resultado_aux_w;		
	end loop;
	close c02;	
	
elsif	(ie_opcao_p = 'OB') then
	open c03;
	loop
	fetch c03 into ds_resultado_aux_w;
	exit when c03%notfound;	
		ds_resultado_w := ds_resultado_w||' / '||ds_resultado_aux_w;		
	end loop;
	close c03;
	
elsif	(ie_opcao_p = 'HR') then
	open c04;
	loop
	fetch c04 into ds_resultado_aux_w;
	exit when c04%notfound;	
		ds_resultado_w := ds_resultado_w||' / '||ds_resultado_aux_w;		
	end loop;
	close c04;

elsif	 (ie_opcao_p = 'OP') then
	open c05;
	loop
	fetch c05 into ds_resultado_aux_w;
	exit when c05%notfound;
		 ds_resultado_w := ds_resultado_w||' / '||ds_resultado_aux_w;
	end loop;
	close c05;
		
end if;

return	substr(ds_resultado_w,4,4000);

end hsl_obter_dados_dieta_concat;
/