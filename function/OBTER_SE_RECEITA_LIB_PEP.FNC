create or replace
function Obter_se_receita_lib_pep	(ie_tipo_receita_p	varchar)
 		    	return varchar2 is

ie_liberado_w	varchar2(1) := 'S';
qt_reg_w	number(10);
cd_perfil_w	number(10);
cd_estabelecimento_w	number(10);

Cursor C01 is
	select	ie_permite
	from	tipo_receita_lib
	where	ie_tipo_receita = ie_tipo_receita_p
	and	nvl(cd_perfil,cd_perfil_w) = cd_perfil_w
	and	nvl(cd_estabelecimento,cd_estabelecimento_w)	= cd_estabelecimento_w
	order by nvl(cd_estabelecimento,0),nvl(cd_perfil,0);
			
begin

cd_perfil_w := obter_perfil_Ativo;
cd_estabelecimento_w	:= obter_estabelecimento_ativo;

select	count(*)
into	qt_reg_w
from	tipo_receita_lib
where	ie_tipo_receita = ie_tipo_receita_p;

if	(qt_reg_w > 0) then
/*
	select	count(*)
	into	qt_reg_w
	from	tipo_receita_lib
	where	ie_tipo_receita = ie_tipo_receita_p
	and	nvl(cd_perfil,cd_perfil_w) = cd_perfil_w;
	*/
	
	open C01;
	loop
	fetch C01 into	
		ie_liberado_w;
	exit when C01%notfound;
	end loop;
	close C01;
	
end if;

return	ie_liberado_w;

end Obter_se_receita_lib_pep;
/
