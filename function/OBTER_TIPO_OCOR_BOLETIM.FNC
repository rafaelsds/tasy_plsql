create or replace
function obter_tipo_ocor_boletim(nr_sequencia_p		number)
 		    	return varchar2 is

ds_tipo_ocor_boletim_w	varchar2(255);			
			
begin
if	(nr_sequencia_p	is not  null) then
	select	ds_tipo_ocorrencia
	into	ds_tipo_ocor_boletim_w
	from	sac_tipo_ocorrencia
	where	nr_sequencia	= nr_sequencia_p;
end if;

return	ds_tipo_ocor_boletim_w;

end obter_tipo_ocor_boletim;
/