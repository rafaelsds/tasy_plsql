create or replace
function pls_obter_cd_tiss_atendimento
			(	nr_seq_atendimento_p		Number)
				return Varchar2 is

cd_tiss_w			Varchar2(20);
begin
select	max(cd_tiss)
into	cd_tiss_w
from	pls_tipo_atendimento
where	nr_sequencia	= nr_seq_atendimento_p;

return	cd_tiss_w;

end pls_obter_cd_tiss_atendimento;
/