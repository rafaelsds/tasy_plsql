create or replace
function ageint_consiste_espec_proced (
		nr_seq_proc_interno_p		number,
		nr_seq_proc_interno_item_p		number,
		cd_procedimento_item_p		number,
		ie_origem_proced_item_p		number,
		cd_estabelecimento_p		number,
		cd_convenio_p		number,
		cd_categoria_p		varchar2,
		cd_plano_p		varchar2)

		return varchar2 is
-- Exame adic
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_especialidade_w		number(15);
-- Item principal
cd_procedimento_item_w		number(15);
ie_origem_proced_item_w		number(10);
cd_especialidade_item_w		number(15);

ds_retorno_w		varchar2(1) := 'N';
begin
if	(nr_seq_proc_interno_p	> 0) then
	
	-- Obter procedimento Exame Adicional
	obter_proc_tab_interno_conv(
				nr_seq_proc_interno_p,
				cd_estabelecimento_p,
				cd_convenio_p,
				cd_categoria_p,
				cd_plano_p,
				null,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);

	select	max(cd_especialidade)
	into	cd_especialidade_w
	from	estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w;

	if	(cd_procedimento_item_p is null) then
		obter_proc_tab_interno_conv(
			nr_seq_proc_interno_item_p,
			cd_estabelecimento_p,
			cd_convenio_p,
			cd_categoria_p,
			cd_plano_p,
			null,
			cd_procedimento_item_w,
			ie_origem_proced_item_w,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null);
	else
		cd_procedimento_item_w	:= cd_procedimento_item_p;
		ie_origem_proced_item_w	:= ie_origem_proced_item_p;
	end if;
	
	select	max(cd_especialidade)
	into	cd_especialidade_item_w
	from	estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_item_w
	and	ie_origem_proced	= ie_origem_proced_item_w;
	
	if	(cd_especialidade_w = cd_especialidade_item_w) then
		ds_retorno_w := 'N';
	else
		ds_retorno_w := 'S';
	end if;
end if;
return	ds_retorno_w;

end ageint_consiste_espec_proced;
/