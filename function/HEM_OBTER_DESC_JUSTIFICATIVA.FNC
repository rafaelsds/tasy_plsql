create or replace
function Hem_obter_desc_justificativa(nr_seq_justificativa_p		Number)
 		    	return Varchar2 is
ds_retorno_w	Varchar2(255);

begin
if	(nvl(nr_seq_justificativa_p,0) > 0 ) then

	select	ds_motivo
	into	ds_retorno_w
	from	escala_motivo_teste_allen
	where	nr_sequencia = nr_seq_justificativa_p;
end if;
return	ds_retorno_w;

end Hem_obter_desc_justificativa;
/