create or replace
function obter_desc_agenda_integrada( cd_agenda_p  number,
					cd_tipo_agenda_p  number,
					ie_opcao_combo_p  varchar2,
					cd_estabelecimento_p number)
				return varchar2 is

ds_agenda_w		varchar2(50);
ds_nome_medico_w	varchar2(240);
ds_retorno_w		varchar2(255);

begin

if (cd_agenda_p is not null) and
	(cd_tipo_agenda_p is not null) then
 
	select	decode(nvl(ie_desc_curta_exame,'N'), 'S', nvl(ds_curta,ds_agenda),ds_agenda) ds_agenda,
  		substr(obter_nome_medico_combo_agcons(nvl(cd_estabelecimento_p,cd_estabelecimento), cd_agenda, cd_tipo_agenda, nvl(ie_opcao_combo_p,nvl(ie_ordenacao,'N'))),1,240)
	into 	ds_agenda_w,
		ds_nome_medico_w
 	from	agenda
 	where	cd_agenda = cd_agenda_p
	and	cd_tipo_agenda = cd_tipo_agenda_p;
 
	if (cd_tipo_agenda_p in (1,2,5)) then
		ds_retorno_w := ds_agenda_w;
	elsif (cd_tipo_agenda_p in (3,4)) then
		ds_retorno_w := ds_nome_medico_w;
	end if;
 
end if;

return ds_retorno_w;

end obter_desc_agenda_integrada;
/