create or replace
function obter_estab_reg_inspecao(	nr_sequencia_p		number)
 		    	return number is

cd_estabelecimento_w		number(10);
begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	inspecao_registro
where	nr_sequencia = nr_sequencia_p;

return	cd_estabelecimento_w;

end obter_estab_reg_inspecao;
/