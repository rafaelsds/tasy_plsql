create or replace
function CIH_OBTER_DADOS_CONTATO_FICHA (nr_ficha_ocorrencia_p	number,
					ie_opcao_p		varchar2) return varchar2 is

ie_houve_contato_w	varchar2(20);
nr_seq_motivo_w		number(15,0);
ie_houve_complicacao_w	varchar2(1);
nr_atendimento_w	number(10);

begin

select	max(CIH_OBTER_MOTIVO_CONTATO_ATUAL(nr_ficha_ocorrencia_p))
into	nr_seq_motivo_w
from	dual;

if	(nr_seq_motivo_w is null) then

	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	cih_ficha_ocorrencia
	where	nr_ficha_ocorrencia = nr_ficha_ocorrencia_p;

	select	max(ie_houve_contato)
	into	ie_houve_contato_w
	from	cih_contato
	where	((nr_ficha_ocorrencia = nr_ficha_ocorrencia_p)
		or ((nr_atendimento = nr_atendimento_w) and (nr_ficha_ocorrencia is null)))
	and	ie_houve_contato	= 'S';

end if;

if	(ie_opcao_p	= 'C') then
	return nvl(ie_houve_contato_w, 'N');
end if;

if	(ie_opcao_p	= 'E') then
	select	decode(count(*),0,'N','S')
	into	ie_houve_complicacao_w
	from	cih_contato
	where	nr_ficha_ocorrencia = nr_ficha_ocorrencia_p
	and	ie_complicacao_infect = 'S';

	return nvl(ie_houve_complicacao_w, 'N');
end if;

end CIH_OBTER_DADOS_CONTATO_FICHA;
/
