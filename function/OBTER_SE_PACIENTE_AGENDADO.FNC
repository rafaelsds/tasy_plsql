create or replace
function obter_se_paciente_agendado(	cd_pessoa_fisica_p	varchar2,
					cd_agenda_p		number,
					dt_agenda_p		date,					
					ie_agenda_p		Varchar2,
					ie_opcao_p		Varchar2)
					return varchar2 is

/*
			Agenda
Agenda de consulta		C

			Op��o
Nome da agenda		N
C�digo da agenda		C			
*/					
					
ds_retorno_w 		Varchar2(255) := 'N';
cd_agenda_w		Number(10);
nm_agenda_w		Varchar2(4000);

				
begin

If	(cd_pessoa_fisica_p is not null) and
	(cd_agenda_p is not null) and
	(dt_agenda_p is not null) then	

		
	If	(ie_agenda_p = 'C') then

		Select 	nvl(max(cd_agenda),0)
		into	cd_agenda_w
		from	agenda_consulta
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_agenda = dt_agenda_p
		and	ie_status_agenda not in ('C','F','I')
		and	cd_agenda <> cd_agenda_p;
		
		if	(cd_agenda_w > 0) then
		
			select 	substr(obter_nome_pf(cd_pessoa_fisica) || decode(DS_CURTA,null,'',' ('||DS_CURTA||')'),1,120)||' ('||ds_complemento||')' 
			into	nm_agenda_w
			from	agenda
			where	cd_agenda = cd_agenda_w;			
						
			if	(ie_opcao_p = 'N') then
				
				ds_retorno_w := substr(nm_agenda_w,1,255);
				
			else
			
				ds_retorno_w := cd_agenda_w;
			
			end if;
			
		end if;

	end if;

end if;

return	ds_retorno_w;

end obter_se_paciente_agendado;
/