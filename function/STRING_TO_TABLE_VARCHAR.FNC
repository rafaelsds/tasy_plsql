create or replace function string_to_table_varchar (
  ds_texto_p       varchar2,
  ie_separador_p   varchar2
) return dbms_sql.varchar2_table
  deterministic
is
  x            sys.dbms_sql.varchar2_table;
  ds_texto_w   varchar2(32767);
  pos_w        number(10);
begin
  begin
    ds_texto_w := replace(ds_texto_p, chr(32), null);
    ds_texto_w := replace(ds_texto_w, chr(13), null);
    ds_texto_w := replace(ds_texto_w, chr(10), null);
  exception
    when others then
      ds_texto_w := null;
  end;

  if ( ds_texto_w is not null ) then
    begin
      << while_loop >>
      while ( ds_texto_w is not null ) loop
        begin
          pos_w := nvl(instr(ds_texto_w, ie_separador_p), 0);
          if ( pos_w = 0 ) then
            begin
              x(x.count + 1) := substr(ds_texto_w, 1, 255);
              ds_texto_w := null;
            end;
          else
            begin
              x(x.count + 1) := substr(ds_texto_w, 1, pos_w - 1);
              ds_texto_w := substr(ds_texto_w, pos_w + 1, 32767);
            end;
          end if;
        end;
      end loop;
    end;
  end if;

  return x;
end string_to_table_varchar;
/
