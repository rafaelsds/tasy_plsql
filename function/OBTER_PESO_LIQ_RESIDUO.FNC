create or replace
function obter_peso_liq_residuo(nr_seq_equipamento_p	number,
				nr_sequencia_p		number) return number is

qt_peso_liq_w		number(15,4) := 0;
qt_peso_equipamento_w	number(15,4) := 0;
qt_peso_residuo_item_w	number(15,4) := 0;

begin

select	nvl(max(qt_peso),0)
into	qt_peso_equipamento_w
from	man_equipamento
where	nr_sequencia = nr_seq_equipamento_p;

select 	nvl(max(qt_peso),0)
into	qt_peso_residuo_item_w
from	setor_residuo_item
where	nr_sequencia = nr_sequencia_p;

if	(nr_seq_equipamento_p is not null) then
	begin
	if	(qt_peso_residuo_item_w > qt_peso_equipamento_w) then
		qt_peso_liq_w	:= qt_peso_residuo_item_w - qt_peso_equipamento_w;
	elsif	(qt_peso_equipamento_w > qt_peso_residuo_item_w) then
		qt_peso_liq_w	:= qt_peso_residuo_item_w;
	end if;		
	end;
else
	begin
	qt_peso_liq_w	:= qt_peso_residuo_item_w;
	end;
end if;

return	qt_peso_liq_w;

end obter_peso_liq_residuo;
/