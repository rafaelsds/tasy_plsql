create or replace
function obter_dietas_serv_evol(nr_seq_serv_dia_p		number,
							ie_opcao_origem_p		number,	
							ie_tipo_evolucao_p		number)
 		    	return varchar2 is
				
nr_atendimento_w	number(10);
dt_servico_w		date;
ds_dieta_w		varchar2(255);
ds_retorno_w	varchar2(2000) := null;
qt_rep_defindos_w	number(10);
	
Cursor C01 is
SELECT	c.nm_dieta
FROM 	prescr_medica a,
	prescr_dieta b,
	dieta c
WHERE 	a.nr_prescricao = b.nr_prescricao
AND	c.cd_dieta = b.cd_dieta
AND 	dt_servico_w BETWEEN a.dt_inicio_prescr AND a.dt_validade_prescr
AND	a.dt_suspensao IS NULL
AND	obter_cod_funcao_usuario_orig(a.nm_usuario_original) = ie_tipo_evolucao_p
ORDER BY 1;

Cursor C02 is
SELECT	c.nm_dieta
FROM 	nut_atend_serv_dia_rep d,
	prescr_medica a,
	prescr_dieta b,
	dieta c
WHERE 	a.nr_prescricao = b.nr_prescricao
AND	c.cd_dieta = b.cd_dieta
AND 	a.nr_prescricao = d.nr_prescr_oral 
AND	d.NR_SEQ_SERV_DIA = nr_seq_serv_dia_p
AND	a.dt_suspensao IS NULL
AND	obter_cod_funcao_usuario_orig(a.nm_usuario_original) = ie_tipo_evolucao_p
ORDER BY 1;

begin
Select	nvl(count(*),0)
into	qt_rep_defindos_w
from	nut_atend_serv_dia_rep a
where	a.NR_SEQ_SERV_DIA = nr_seq_serv_dia_p;

if (qt_rep_defindos_w  > 0) then
	open C02;
	loop
	fetch C02 into	
		ds_dieta_w;
	exit when C02%notfound;
		ds_retorno_w := ds_retorno_w||ds_dieta_w||',';
	end loop;
	close C02;
else
	Select	max(a.dt_servico),
		max(a.nr_atendimento)
	into	dt_servico_w,
		nr_atendimento_w
	from	nut_atend_serv_dia a,
		nut_servico b
	where	a.nr_sequencia = nr_seq_serv_dia_p
	and	a.nr_seq_servico = b.nr_sequencia
	and	((b.ie_tipo_prescricao_servico is null) or (b.ie_tipo_prescricao_servico = ie_opcao_origem_p));

	open C01;
	loop
	fetch C01 into	
		ds_dieta_w;
	exit when C01%notfound;
		ds_retorno_w := ds_retorno_w||ds_dieta_w||',';
	end loop;
	close C01;
end if;

return	substr(ds_retorno_w,1,length(ds_retorno_w)-1)	;

end obter_dietas_serv_evol;
/
