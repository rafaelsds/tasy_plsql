create or replace
function obter_qt_horas_ativ_cron ( nr_sequencia_p		number)
 		    	return number is
nr_seq_etapa_cron_w	number;
qt_horas_acres_w	number;
ie_soma_subtrai_w	varchar2(1);
qt_horas_cron_w		number;
qt_retorno_w		number;
begin
select	max(nr_seq_etapa_cron),
		max(qt_horas_acres),
		max(ie_soma_subtrai)
into	nr_seq_etapa_cron_w,
		qt_horas_acres_w,
		ie_soma_subtrai_w
from	proj_alt_escopo_ativ
where	nr_sequencia = nr_sequencia_p;

select	qt_hora_prev
into	qt_horas_cron_w
from	proj_cron_etapa
where	nr_sequencia = nr_seq_etapa_cron_w;

qt_retorno_w := qt_horas_cron_w;

if (ie_soma_subtrai_w = 'P') then
	qt_retorno_w := qt_horas_cron_w + qt_horas_acres_w;
else	if (ie_soma_subtrai_w = 'N') then
			qt_retorno_w := qt_horas_cron_w - qt_horas_acres_w;
		end if;
end if;

return	qt_retorno_w ;

end obter_qt_horas_ativ_cron ;
/