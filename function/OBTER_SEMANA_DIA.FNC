create or replace
function Obter_Semana_Dia(dt_referencia_p		date)
 		    	return number is

dt_inicial_w	date := trunc(dt_referencia_p,'Month');
dt_final_w	date := pkg_date_utils.get_datetime(pkg_date_utils.end_of(dt_referencia_p, 'MONTH', 0), NVL(dt_referencia_p, PKG_DATE_UTILS.GET_TIME('00:00:00')));
nr_dia_semana_w	number(10):= 1;
begin

while dt_inicial_w <=dt_final_w loop 
	begin
	if	(dt_inicial_w	= trunc(dt_referencia_p)) then
		exit;
	end if;
	if	(pkg_date_utils.get_WeekDay(dt_inicial_w)	= '7') then
		nr_dia_semana_w	:= nr_dia_semana_w +1;
	end if;
	dt_inicial_w	:= dt_inicial_w +1;

	end;
end loop;

return	nr_dia_semana_w;

end Obter_Semana_Dia;
/