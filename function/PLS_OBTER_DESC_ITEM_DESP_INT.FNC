create or replace
function pls_obter_desc_item_desp_int
		(	ie_ordem_p		number,
			ie_singular_p		varchar2	)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
ds_tipo_despesa_w	varchar2(2);

/*
ie_singular_p
P	Plural
S	Singular
*/

begin

select	decode(ie_singular_p, 'S', decode(ie_ordem_p,'1','PROCEDIMENTO','3','TAXA','2','DI�RIA','4','PACOTE','6','GASE','5','MEDICAMENTO','7','MATERIAL','8','OPM'),
	decode(ie_ordem_p,'1','PROCEDIMENTOS','3','TAXAS','2','DI�RIAS','4','PACOTES','6','GASES','5','MEDICAMENTOS','7','MATERIAIS','8','OPM'))
into	ds_retorno_w
from	dual;

return	ds_retorno_w;

end pls_obter_desc_item_desp_int;
/