create or replace
function obter_se_estabelecido_usuario(nm_usuario_p			varchar2,
									cd_estabelecimento_verificar_p		varchar2)	
									return varchar2 is
									
ds_retorno_w				varchar2(1) := 'S';
begin



select  nvl(max('S'),'N')
into	ds_retorno_w
from    usuario a
where   a.nm_usuario = nm_usuario_p
and     a.cd_estabelecimento = cd_estabelecimento_verificar_p;


if (ds_retorno_w = 'N') then 

	select  nvl(max('S'),'N')
	into	ds_retorno_w
	from    USUARIO_ESTABELECIMENTO a,
		usuario b
	where   a.nm_usuario_param = b.nm_usuario
	and     a.nm_usuario_param = nm_usuario_p
	and     a.cd_estabelecimento = cd_estabelecimento_verificar_p;
	
end if;

return ds_retorno_w;

end;
/