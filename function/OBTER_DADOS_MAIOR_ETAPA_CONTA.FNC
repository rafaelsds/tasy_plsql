create or replace
function obter_dados_maior_etapa_conta
		(nr_interno_conta_p		number,
		ie_opcao_p		varchar2)
		return varchar2 is
		
/*IE_OPCAO_P:
S - Sequ�ncia
D - Descri��o
*/		

ds_etapa_w		varchar2(80)	:= null;
ds_retorno_w		varchar2(255)	:= null;
nr_sequencia_w		number(10,0);
nr_seq_etapa_w		number(10,0);

Cursor C01 is
	select	nvl(a.nr_seq_etapa,0)		
	from	fatur_etapa		b,
		conta_paciente_etapa	a	  
	where	a.nr_seq_etapa		= b.nr_sequencia
	and	a.nr_interno_conta	= nr_interno_conta_p
	and	nvl(b.ie_situacao,'A')	= 'A'
	order by b.nr_seq_etapa;

begin

open C01;
loop
fetch C01 into	
	nr_seq_etapa_w;
exit when C01%notfound;
	begin	
	nr_seq_etapa_w:= nr_seq_etapa_w;	
	end;
end loop;
close C01;

if	(ie_opcao_p = 'S') then
	begin
	ds_retorno_w := nr_seq_etapa_w;
	end;
elsif	(ie_opcao_p = 'D') then
	select 	nvl(max(ds_etapa),Wheb_mensagem_pck.get_texto(799238))
	into	ds_retorno_w
	from 	fatur_etapa
	where 	nr_sequencia = nr_seq_etapa_w;
end if;

return	ds_retorno_w;

end obter_dados_maior_etapa_conta;
/