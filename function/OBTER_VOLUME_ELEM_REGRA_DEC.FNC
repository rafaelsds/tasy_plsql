create or replace
function obter_volume_elem_regra_dec(	
				nr_seq_elemento_p	number,
				qt_elemento_p		number,
				ie_tipo_npt_p		varchar2) return number is 
nr_casas_w	number(15);
ie_existe_w	varchar2(1);
qt_retorno_w	number(15,4);
begin
qt_retorno_w := qt_elemento_p;
if	(nr_seq_elemento_p is not null) then
	/* Verifica se existe alguma regra para arredondar as casas decimais da quantidade de elemento */
	select	nvl(max('S'),'N')
	into	ie_existe_w
	from	nut_arredonda_volume
	where	nr_seq_elemento = nr_seq_elemento_p
	and	ie_npt 		= ie_tipo_npt_p
	and	qt_elemento_p	between qt_inicial and qt_final;
	/* Se existir busca a quantidade de casas para arredondar */
	if	(ie_existe_w = 'S') then
		select	nvl(max(nr_casas),0)
		into	nr_casas_w
		from	nut_arredonda_volume
		where	nr_seq_elemento = nr_seq_elemento_p
		and	ie_npt 		= ie_tipo_npt_p
		and	qt_elemento_p	between qt_inicial and qt_final;
		qt_retorno_w := round(qt_elemento_p,nr_casas_w);
	end if;
end if;
return	qt_retorno_w;
end obter_volume_elem_regra_dec;
/