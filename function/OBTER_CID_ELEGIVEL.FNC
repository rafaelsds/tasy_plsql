create or replace 
function obter_cid_elegivel( 	nr_atendimento_p	number,
				cd_pessoa_fisica_p	varchar2,
				cd_cid_p		varchar2,				
				ie_morte_geral_p	varchar2,
				ie_tipo_diagnostico_p	varchar2,
				ie_morte_fetal_p	varchar2,
				ie_permite_anomalia_p	varchar2)
				return varchar2 is

ie_permite_principal_w		regra_utilizacao_cid.ie_permite_principal%type;
ie_permite_basica_w		regra_utilizacao_cid.ie_permite_basica%type; 
ie_permite_fetal_w		regra_utilizacao_cid.ie_permite_fetal%type; 
ie_obito_w			regra_utilizacao_cid.ie_permite_obito%type;
qt_achou_item_w			number(10);
ie_sexo_w			pessoa_fisica.ie_sexo%type;
ie_sexo_regra_w			regra_utilizacao_cid.ie_sexo%type;
idade_w				number(10);
qt_idade_minima_w		number(10);
qt_idade_maxima_w		number(10);
qt_idade_minima_dias_w		number(10);
qt_idade_maxima_dias_w		number(10);
qt_idade_minima_anos_w		number(10);
qt_idade_maxima_anos_w		number(10);
ie_permite_cid_w		varchar2(1);
ie_rubrica_type_w           	varchar2(1);
ie_permite_anomalia_w		regra_utilizacao_cid.ie_permite_anomalia%type;
ie_permite_selecionar_w		regra_utilizacao_cid.ie_permite_selecionar%type;
qt_reg_w			number(10);

begin
ie_permite_cid_w := 'S';

select nvl(max(ie_permite_principal),'S'),
	nvl(max(ie_permite_basica),'S'),
	nvl(max(ie_permite_fetal),'S'),
	nvl(max(ie_permite_obito),'S'),
	nvl(max(qt_idade_min_dias),0),
	nvl(max(qt_idade_max_dias),0),	
	nvl(max(qt_idade_minima),0),
	nvl(max(qt_idade_maxima),0),
	max(ie_sexo),
	nvl(max(1),0) qt_achou_item,
	nvl(max(ie_rubrica_type), 'N'),
	nvl(max(ie_permite_anomalia),'N'),
	nvl(max(ie_permite_selecionar), 'S')
into	ie_permite_principal_w,
	ie_permite_basica_w,
	ie_permite_fetal_w,
	ie_obito_w,
	qt_idade_minima_dias_w,
	qt_idade_maxima_dias_w,	
	qt_idade_minima_anos_w,
	qt_idade_maxima_anos_w,	
	ie_sexo_regra_w,
	qt_achou_item_w,
	ie_rubrica_type_w,
	ie_permite_anomalia_w,
	ie_permite_selecionar_w
from 	regra_utilizacao_cid
where 	cd_doenca = trim(cd_cid_p)
and 	ie_situacao = 'A';

if	(qt_achou_item_w	> 0) then

	Select 	max(pf.ie_sexo) ie_sexo,
		max(obter_idade(pf.dt_nascimento,sysdate,'DIA')) idade
	into	ie_sexo_w,
		idade_w
	from    pessoa_fisica pf
	where   pf.cd_pessoa_fisica = nvl(cd_pessoa_fisica_p, obter_pessoa_atendimento(nr_atendimento_p,'C')) ;


	if (qt_idade_minima_dias_w <> 0 and qt_idade_minima_anos_w <> 0) then
		qt_idade_minima_w := qt_idade_minima_dias_w + (qt_idade_minima_anos_w * 365);	
	elsif (qt_idade_minima_dias_w <> 0) then
		qt_idade_minima_w := qt_idade_minima_dias_w;
	else
		qt_idade_minima_w := qt_idade_minima_anos_w * 365;	
	end if;

	if (qt_idade_maxima_dias_w <> 0 and qt_idade_maxima_anos_w <> 0) then
		qt_idade_maxima_w := qt_idade_maxima_dias_w + (qt_idade_maxima_anos_w * 365);
	elsif (qt_idade_maxima_dias_w <> 0) then
		qt_idade_maxima_w := qt_idade_maxima_dias_w;
	elsif (qt_idade_maxima_anos_w <> 0) then
		qt_idade_maxima_w := qt_idade_maxima_anos_w * 365;
	else
		qt_idade_maxima_w := 999999;
	end if;


	--Se ele achar uma regra por CID ele come�a a valida��o
	if (qt_achou_item_w = 1) then
		if (ie_permite_selecionar_w = 'N') then
			ie_permite_cid_w := 'N';
		elsif (ie_permite_anomalia_p = 'S') and (ie_permite_anomalia_w = 'N') then
			ie_permite_cid_w := 'N';
		elsif (ie_rubrica_type_w = 'S') then
		ie_permite_cid_w := 'N';
		elsif (ie_permite_fetal_w = 'N') and (ie_morte_fetal_p = 'S') then 
			ie_permite_cid_w := 'N';
		elsif (ie_obito_w = 'N') and (ie_morte_geral_p = 'S') then
			ie_permite_cid_w := 'N';
		elsif (ie_permite_principal_w = 'N') and (nvl(ie_tipo_diagnostico_p,'X') = 'P') then --Principal
			ie_permite_cid_w := 'N';
		elsif (ie_permite_basica_w = 'N') and (nvl(ie_tipo_diagnostico_p,'X')  = 'S') then ---Secundario
			ie_permite_cid_w := 'N';
		elsif (idade_w < qt_idade_minima_w or idade_w > qt_idade_maxima_w) then
			ie_permite_cid_w := 'N';			
		elsif (nvl(ie_sexo_w,ie_sexo_regra_w) <> nvl(ie_sexo_regra_w,ie_sexo_w)) then
			ie_permite_cid_w := 'N';
		end if;	
	end if;

end if;

return ie_permite_cid_w;	
	
end obter_cid_elegivel;
/