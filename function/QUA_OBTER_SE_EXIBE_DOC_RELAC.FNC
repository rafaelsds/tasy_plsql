create or replace
function qua_obter_se_exibe_doc_relac(	nr_seq_doc_p		Number,
					nm_usuario_p		Varchar2)
 		    		return Varchar2 is



ie_visualiza_w		Varchar2(2);
nr_seq_revisao_w	Number(10);
cd_pessoa_w		Varchar2(10);
cd_cargo_w		Number(10);
cd_setor_atendimento_w	number(10);

begin

select	max(cd_pessoa_fisica),
	max(cd_setor_atendimento)
into	cd_pessoa_w,
	cd_setor_atendimento_w
from	usuario
where	nm_usuario = nm_usuario_p;

select	nvl(max(nr_sequencia),0)
into	nr_seq_revisao_w
from	qua_doc_revisao a
where	nr_seq_doc = nr_seq_doc_p;

--Se nao existir revisao verifica as pessoas do documento
if	(nr_seq_revisao_w = 0) then
	begin
	--Pessoas elaboracao - documento
	begin
	select	'S'
	into	ie_visualiza_w
	from	qua_documento
	where	nr_sequencia = nr_seq_doc_p
	and	cd_pessoa_elaboracao = cd_pessoa_w
	and	rownum < 2;
	exception
	when others then
		ie_visualiza_w := 'N';
	end;
	
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		select	to_number(obter_cargo_pf(cd_pessoa_w,'C')),
			case when (nm_usuario = nm_usuario_p and cd_pessoa_elaboracao is null) then 'S' else 'N'end
		into 	cd_cargo_w,
			ie_visualiza_w
		from	qua_documento
		where	nr_sequencia = nr_seq_doc_p;
		end;
	end if;
	
	--Pasta participantes - elaboracao - documento
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_participante
		where	nr_seq_doc = nr_seq_doc_p
		and	ie_tipo = 'E'
		and	cd_participante = cd_pessoa_w
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	
	--Pessoas validacao - documento
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_documento
		where	nr_sequencia = nr_seq_doc_p
		and	cd_pessoa_validacao = cd_pessoa_w
		and	(dt_elaboracao is not null or dt_validacao is not null)
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	--Pasta Validadores - documento
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_validacao b,
			qua_documento a
		where	a.nr_sequencia = b.nr_seq_doc
		and	b.nr_seq_doc = nr_seq_doc_p
		and	nvl(b.cd_pessoa_validacao,cd_pessoa_w) = cd_pessoa_w
		and	nvl(b.cd_cargo,nvl(cd_cargo_w,0)) = nvl(cd_cargo_w,0)
		and	(a.dt_elaboracao is not null or b.dt_validacao is not null)
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	--Pasta participantes - validacao - documento
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_participante b,
			qua_documento a
		where	a.nr_sequencia = b.nr_seq_doc
		and	b.nr_seq_doc = nr_seq_doc_p
		and	b.ie_tipo = 'V'
		and	b.cd_participante = cd_pessoa_w
		and	a.dt_elaboracao is not null
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	
	--Pessoas aprovacao - Documento
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_documento
		where	nr_sequencia = nr_seq_doc_p
		and	cd_pessoa_aprov = cd_pessoa_w
		and	(dt_validacao is not null or dt_aprovacao is not null)
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	--Pasta Aprovadores  - aprovacao - documento
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_aprov b,
			qua_documento a
		where	nr_seq_doc = nr_seq_doc_p
		and	nvl(b.cd_pessoa_aprov,cd_pessoa_w) = cd_pessoa_w
		and	nvl(b.cd_cargo,nvl(cd_cargo_w,0)) = nvl(cd_cargo_w,0)
		and	nvl(b.cd_setor_atendimento,nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
		and	(a.dt_validacao is not null or b.dt_aprovacao is not null)
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	--Pasta participantes - aprovacao - documento
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_participante b,
			qua_documento a
		where	b.nr_seq_doc = nr_seq_doc_p
		and	b.ie_tipo = 'A'
		and	b.cd_participante = cd_pessoa_w
		and	a.dt_elaboracao is not null
		and	rownum < 2;
		
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	end;
else -- Se existir revisao considera somente as pessoas da ultima revisao
	begin
	cd_cargo_w := to_number(obter_cargo_pf(cd_pessoa_w,'C'));
	--Pessoas revisao
	begin
	select	'S'
	into	ie_visualiza_w
	from	qua_doc_revisao
	where	nr_sequencia = nr_seq_revisao_w
	and	cd_pessoa_revisao = cd_pessoa_w
	and	rownum < 2;
	exception
	when others then
		ie_visualiza_w := 'N';
	end;
	--Pessoas validacao - revisao
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_revisao
		where	nr_sequencia = nr_seq_revisao_w
		and	cd_pessoa_validacao = cd_pessoa_w
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	--Pasta Validacao - revisao
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_revisao_validacao
		where	nr_seq_doc_revisao = nr_seq_revisao_w
		and	nvl(cd_pessoa_validacao,cd_pessoa_w) = cd_pessoa_w
		and	nvl(cd_cargo,nvl(cd_cargo_w,0)) = nvl(cd_cargo_w,0)
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	--Aprovador
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_revisao
		where	nr_sequencia = nr_seq_revisao_w
		and	cd_pessoa_aprovacao = cd_pessoa_w
		and	(dt_validacao is not null or dt_aprovacao is not null)
		and	rownum < 2;
		
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	--Pasta Aprovadores
	if	(nvl(ie_visualiza_w,'N') = 'N') then
		begin
		begin
		select	'S'
		into	ie_visualiza_w
		from	qua_doc_revisao_aprovacao b,
			qua_doc_revisao	a
		where	a.nr_sequencia = b.nr_seq_revisao
		and	a.nr_sequencia = nr_seq_revisao_w
		and	nvl(b.cd_pessoa_aprov,cd_pessoa_w) = cd_pessoa_w
		and	nvl(b.cd_cargo,nvl(cd_cargo_w,0)) = nvl(cd_cargo_w,0)
		and	(a.dt_validacao is not null or b.dt_aprovacao is not null)
		and	rownum < 2;
		exception
		when others then
			ie_visualiza_w := 'N';
		end;
		end;
	end if;
	end;
end if;

return	ie_visualiza_w;

end qua_obter_se_exibe_doc_relac;
/
