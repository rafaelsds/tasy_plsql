create or replace
function obter_subtipo_billing(nr_atendimento_p		number) 		    	
			return varchar2 is

ds_tipo_w		varchar2(2255);			
			
begin


select 	max(ds_subtipo)
into 	ds_tipo_w
from 	episodio_paciente a, 
	atendimento_paciente b,
	subtipo_episodio c
where	a.nr_sequencia = b.nr_seq_episodio 
and 	a.nr_seq_subtipo_episodio = c.nr_sequencia
and 	b.nr_atendimento = nr_atendimento_p;


return	ds_tipo_w;

end obter_subtipo_billing;
/