create or replace
function obter_se_lote_prot_contabil(
		nr_seq_lote_protocolo_p		number)
		return varchar2 is

ie_contabilizado_w		varchar2(1);

begin

select	decode(nvl(max(nr_lote_contabil),0),0,'N','S')
into	ie_contabilizado_w
from	conta_paciente a,
	protocolo_convenio b
where	a.nr_seq_protocolo = b.nr_seq_protocolo
and	b.nr_seq_lote_protocolo = nr_seq_lote_protocolo_p;

return	ie_contabilizado_w;

end obter_se_lote_prot_contabil;
/