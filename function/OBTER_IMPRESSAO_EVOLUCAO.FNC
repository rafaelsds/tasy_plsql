create or replace
function obter_impressao_evolucao(nr_atendimento_p integer)
 		    	return varchar2 is
				
ds_impressao_w	varchar2(255);				

begin

select	max(ds_impressao)
into	ds_impressao_w
from	evolucao_paciente
where	cd_evolucao = (	select	max(cd_evolucao)
						from	evolucao_paciente
						where	nr_atendimento = nr_atendimento_p
						and		ds_impressao is not null);

return	ds_impressao_w;

end obter_impressao_evolucao;
/