create or replace 
FUNCTION Obter_desc_plano_versao_ans(
				cd_plano_p		Number,
				cd_empresa_p		Number,
				ie_versao_p		varchar2)
		return varchar2 is

ds_retorno_w	Varchar2(255);

BEGIN

if	(cd_plano_p is not null) and
	(cd_empresa_p is not null) and
	(ie_versao_p is not null) then
	select	ds_conta
	into	ds_retorno_w
	from	ctb_plano_ans
	where	cd_empresa	= cd_empresa_p
	and	cd_plano	= cd_plano_p
	and	ie_versao	= ie_versao_p;
end if;

return ds_retorno_w;

END Obter_desc_plano_versao_ans;
/
