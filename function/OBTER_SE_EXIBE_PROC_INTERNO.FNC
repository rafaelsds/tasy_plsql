create or replace
function obter_se_exibe_proc_interno(nr_seq_proc_interno_p	number)
 		    	return varchar2 is

ie_copiar_rep_w		varchar2(1);

begin

select	nvl(max(ie_copiar_rep),'S')
into	ie_copiar_rep_w
from	proc_interno
where	nr_sequencia	= nr_seq_proc_interno_p;

return	ie_copiar_rep_w;

end obter_se_exibe_proc_interno;
/