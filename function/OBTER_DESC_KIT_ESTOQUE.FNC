create or replace
function obter_desc_kit_estoque(	nr_seq_kit_estoque_p	number )
					return varchar2 is

ds_kit_estoque_w	varchar2(80);

begin

select	substr(obter_descricao_padrao('KIT_MATERIAL','DS_KIT_MATERIAL',CD_KIT_MATERIAL),1,80)
into	ds_kit_estoque_w
from	kit_estoque
where	nr_sequencia	=	nr_seq_kit_estoque_p;
		
return	ds_kit_estoque_w;

end obter_desc_kit_estoque;
/