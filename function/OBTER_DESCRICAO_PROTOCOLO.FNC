CREATE OR REPLACE FUNCTION OBTER_DESCRICAO_PROTOCOLO	(nr_seq_protocolo_p	number)
							RETURN VARCHAR2 IS

ds_retorno_w		varchar2(40);

BEGIN

Select	nvl(max(nr_protocolo),'')
into	ds_retorno_w
from	protocolo_convenio
where	nr_seq_protocolo = nr_seq_protocolo_p;

Return	ds_retorno_w;

END OBTER_DESCRICAO_PROTOCOLO;
/
