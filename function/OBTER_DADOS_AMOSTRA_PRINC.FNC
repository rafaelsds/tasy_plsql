create or replace
function obter_dados_amostra_princ(nr_seq_amostra_p	number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);
begin

select substr(ds_amostra,1,255)
into ds_retorno_w
from tipo_amostra_patologia
where nr_sequencia = nr_seq_amostra_p;

return	ds_retorno_w;

end obter_dados_amostra_princ;
/