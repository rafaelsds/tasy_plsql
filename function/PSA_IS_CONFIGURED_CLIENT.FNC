create or replace FUNCTION psa_is_configured_client RETURN VARCHAR2 AS
  id_client   client.id%TYPE;
BEGIN
  BEGIN
    SELECT
      id
    INTO
      id_client
    FROM
      client
    WHERE
      nm_client = 'tasy_front-end';

    RETURN id_client;
  EXCEPTION
    WHEN no_data_found THEN
      RETURN NULL;
  END;
END psa_is_configured_client;
/