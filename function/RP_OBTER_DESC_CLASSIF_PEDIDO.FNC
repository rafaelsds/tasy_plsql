create or replace
function rp_obter_desc_classif_pedido(nr_sequencia_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin
ds_retorno_w := '';
if (nr_sequencia_p <> 0) and (nr_sequencia_p is not null) then
	select 	substr(DS_CLASSIFICACAO,1,255)
	into	ds_retorno_w
	from	RP_CLASSIFICACAO_PEDIDO
	where	nr_sequencia = nr_sequencia_p;
end if;



return	ds_retorno_w;

end rp_obter_desc_classif_pedido;
/
