create or replace
function sup_obter_saldo_proj_rec(	nr_seq_projeto_p			number,
				dt_referencia_p			date)
 		    	return number is

vl_saldo_w		number(22,4);
ie_param_25_w		varchar2(1) := 'N';

begin

select 	projeto_recurso_pck.obter_vl_saldo_proj_rec(nr_seq_projeto_p,Obter_Usuario_Ativo,obter_estabelecimento_ativo) 
into	vl_saldo_w
from 	dual;

return	vl_saldo_w;

end sup_obter_saldo_proj_rec;
/