CREATE OR REPLACE 
function adep_obter_um_dosagem_prescr (	nr_prescricao_p		number,
										nr_sequencia_p		number,
										ie_acm_p			varchar2,
										ie_sn_p				varchar2)
return varchar is

qt_dose_w						varchar2(15);
qt_solucao_w					number(15,4);
qt_vel_infusao_w				number(15,4);
cd_unidade_medida_dose_w		varchar2(40);
ds_prescr_w						varchar2(50);
ds_prescricao_w					varchar2(60);
ds_dose_diferenciada_w			varchar2(200);
ds_retorno_w					varchar2(200);
nr_agrupamento_w				number(07,1);
qt_reg_w						number(06,0);
nr_seq_w						number(06,0);
ie_via_w						varchar2(80);
ie_agrupador_w					number(2);
qt_dose_especial_w				number(18,6);
hr_dose_especial_w				varchar2(5);
VarIeConverteDoseFrac_w			varchar2(1);
ie_item_superior_w				prescr_material.ie_item_superior%type;
ie_diff_dose_w 					varchar2(1);
ds_sn_acm_w						varchar2(200);
nr_seq_cpoe_w					prescr_material.nr_seq_mat_cpoe%type;
ie_dose_range_w					cpoe_material.ie_dose_range%type	:= 'N';

begin

Obter_Param_Usuario(1113,633,Obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,VarIeConverteDoseFrac_w);

select	decode(VarIeConverteDoseFrac_w, 'S', converte_fracao_dose(a.cd_unidade_medida_dose,vipe_obter_dose_composto(a.nr_prescricao, a.nr_sequencia, a.nr_agrupamento)), a.qt_dose) qt_dose,
	Obter_Desc_Unid_Med(a.cd_unidade_medida_dose),
	nvl(c.ds_prescricao,c.ds_intervalo),
	cpoe_get_differential_dose(a.ds_dose_diferenciada),
	qt_solucao,
	a.nr_agrupamento,
	obter_desc_via(a.ie_via_aplicacao),
	a.ie_agrupador,
	a.qt_dose_especial,
	a.hr_dose_especial,
	nvl(obter_vel_infusao_sne(a.nr_prescricao, a.nr_sequencia),a.qt_vel_infusao),
	nvl(nr_seq_mat_cpoe,0)
into	qt_dose_w,
	cd_unidade_medida_dose_w,
	ds_prescr_w,
	ds_dose_diferenciada_w,
	qt_solucao_w,
	nr_agrupamento_w,
	ie_via_w,
	ie_agrupador_w,
	qt_dose_especial_w,
	hr_dose_especial_w,
	qt_vel_infusao_w,
	nr_seq_cpoe_w
from  	intervalo_prescricao c,
	material b,
	prescr_material a
where 	a.cd_material    	= b.cd_material
and 	a.cd_intervalo 		= c.cd_intervalo(+)
and 	a.nr_prescricao  	= nr_prescricao_p
and	a.nr_sequencia		= nr_sequencia_p;

if	(substr(qt_dose_w,1,1) = ',') then
	qt_dose_w	:= '0' || qt_dose_w;
end if;
qt_dose_w	:= replace_number_locale(qt_dose_w);

select	count(*),
	min(nr_sequencia)
into	qt_reg_w,
	nr_seq_w
from	prescr_material
where	nr_prescricao		= nr_prescricao_p
  and	ie_agrupador		= 1
  and	nr_agrupamento		= nr_agrupamento_w
  and	nr_seq_substituto	is null;

select 	nvl(max(ie_item_superior), 'N')
  into	ie_item_superior_w
  from	prescr_material
 where	nr_prescricao		= nr_prescricao_p
   and	nr_sequencia		= nr_sequencia_p
   and	ie_agrupador		= 1
   and	nr_agrupamento		= nr_agrupamento_w
   and	nr_seq_substituto	is null;

ie_diff_dose_w :='N';

if	(qt_reg_w > 1) and
	(nr_sequencia_p <> nr_seq_w) and
	(ie_item_superior_w = 'N')	then
	ds_prescricao_w	:= '';
	ie_via_w		:= '';
elsif	(ds_dose_diferenciada_w is not null) then
	ds_prescricao_w	:= substr(ds_dose_diferenciada_w || ' ' || cd_unidade_medida_dose_w || ' ' || ds_prescr_w,1,60);
	cd_unidade_medida_dose_w	:= '';
	qt_dose_w			:= '';
	ie_diff_dose_w :='S';
else
	ds_prescricao_w	:= substr(ds_prescr_w,1,50);
end if;

if	(ie_agrupador_w	= 16) then
	select	nvl(max(decode(a.ie_via_aplicacao,'O',obter_desc_expressao(294826), 'OS',obter_desc_expressao(781460)/*'Oral/Sonda'*/,obter_desc_expressao(298781)/*'Sonda'*/)),ie_via_w)
	into	ie_via_w
	from	prescr_material b,
		prescr_leite_deriv a
	where	b.nr_seq_leite_deriv	= a.nr_sequencia
	and	b.nr_prescricao	= a.nr_prescricao
	and	b.nr_prescricao	= nr_prescricao_p
	and	b.nr_sequencia	= nr_sequencia_p
	and	a.nr_prescricao	= nr_prescricao_p;
end if;

if	(nvl(ie_sn_p,'N') = 'S') then
	ds_sn_acm_w	:= '  '|| obter_desc_expressao(309405);
elsif	(nvl(ie_acm_p,'N') = 'S') then
	ds_sn_acm_w	:= '  '|| obter_desc_expressao(283158);
end if;

/* Ivan em 08/02/2008 OS82257 - Consistency included to compare qt_dosagem_w only in grouper 1 */
if	(qt_dose_especial_w is not null) then
	ds_retorno_w	:= substr(ds_retorno_w || '  ' || obter_desc_expressao(310214) || '  ' || to_char(qt_dose_especial_w) || ' ' || cd_unidade_medida_dose_w ||ds_sn_acm_w|| '(' || hr_dose_especial_w || ')',1,200);
end if;

if 	((ie_agrupador_w <> 1) or
	(ie_agrupador_w = 1)) then -- and (qt_solucao_w is null)) then //Retirado por conta da OS 1400053(BP)
	ds_retorno_w	:= substr(qt_dose_w || ' ' || cd_unidade_medida_dose_w ||ds_sn_acm_w||
			'   ' ||ds_prescricao_w || '   ' || ie_via_w,1,200);
else
	ds_retorno_w	:= substr(ds_prescricao_w || '   ' || ie_via_w,1,200);
end if;

if	(ie_agrupador_w = 8) and
	(nvl(qt_vel_infusao_w,0) > 0) then
	ds_retorno_w	:= substr(ds_retorno_w || '  ' || qt_vel_infusao_w || obter_desc_expressao(326013),1,200);
end if;


if(ie_diff_dose_w = 'S') then
  ds_retorno_w            := substr(ds_retorno_w || '  ' ||'['||trim(obter_desc_expressao(288235))||']',1,200);
end if;

if	(nr_seq_cpoe_w > 0) then

	select 	nvl(max(ie_dose_range), 'N')
	into	ie_dose_range_w
	from	cpoe_material
	where	nr_sequencia = nr_seq_cpoe_w;
	
end if;

if	(ie_dose_range_w = 'S') then
	ds_retorno_w := substr(obter_desc_expressao(970707) || ': ' || ds_retorno_w,1,200);
else
	ds_retorno_w := substr(obter_desc_expressao(863724) || ': ' || ds_retorno_w,1,200);
end if;

return ds_retorno_w;

end adep_obter_um_dosagem_prescr;
/