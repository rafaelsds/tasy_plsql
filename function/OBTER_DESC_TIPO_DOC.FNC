create or replace 
function obter_desc_tipo_doc(
		nr_sequencia_p	number)
		return varchar2 is

ds_tipo_doc_w	varchar2(140);

begin

select	ds_tipo_item
into	ds_tipo_doc_w
from 	tipo_doc_item
where 	nr_sequencia = nr_sequencia_p;

return	ds_tipo_doc_w;

end  obter_desc_tipo_doc;
/