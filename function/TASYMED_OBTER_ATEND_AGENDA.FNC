create or replace
function TasyMed_obter_atend_agenda (	dt_agenda_p		date,
					cd_pessoa_fisica_p	varchar2)
					return number is
nr_atendimento_w	number(10,0);

begin

select	nvl(max(a.nr_atendimento),0) 
into	nr_atendimento_w
from    med_atendimento a 
where   trunc(a.dt_entrada) = dt_agenda_p
and	exists(	select	1 
		from    med_cliente b 
		where   b.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and 	b.nr_sequencia    	= a.nr_seq_cliente);

return	nr_atendimento_w;

end TasyMed_obter_atend_agenda;
/