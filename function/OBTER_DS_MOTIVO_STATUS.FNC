create or replace
function obter_ds_motivo_status	(nr_seq_agenda_p	number,
				 cd_agenda_p		number)
						return varchar2 is
						
cd_tipo_agenda_w		number;
ds_motivo_w		varchar2(255);

begin
if	(nvl(nr_seq_agenda_p, 0) <> 0 and nvl(cd_agenda_p,0) <> 0) then

	select  max(cd_tipo_agenda)
	into	cd_tipo_agenda_w
	from	agenda 
	where	cd_agenda = cd_agenda_p;
	
	if(cd_tipo_agenda_w is not null)then
		if(cd_tipo_agenda_w = 3)then
			select	max(ds_motivo_status)		
			into	ds_motivo_w		
			from	agenda_consulta
			where	nr_sequencia = nr_seq_agenda_p;			
		end if;
		if(cd_tipo_agenda_w = 2)then
			select	max(ds_motivo_status)		
			into	ds_motivo_w		
			from	agenda_paciente
			where	nr_sequencia = nr_seq_agenda_p;
		end if;
	end if;	
end if;

return ds_motivo_w;

end obter_ds_motivo_status;
/
