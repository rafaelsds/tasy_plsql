create or replace
function pls_obter_item_fora_period_int
			(	nr_seq_conta_p		number,
				nr_seq_item_p		number,
				ie_tipo_item_p		number)
				return varchar2 is

dt_item_w		date;			
dt_inicio_w		date;
dt_final_w		date;
hr_inicio_w		date;
hr_fim_w		date;
ie_tipo_guia_w		varchar2(2);
ie_retorno_w		varchar2(1) := 'S';
cd_guia_referencia_w	varchar2(20);
nr_seq_conta_w		Number(10);
nr_seq_segurado_w	Number(10);
qt_registros_w		number(10);

begin

nr_seq_conta_w := nr_seq_conta_p;


/*Obtido os dados da conta*/
select	ie_tipo_guia,
	cd_guia_referencia,
	nr_seq_segurado
into	ie_tipo_guia_w,
	cd_guia_referencia_w,
	nr_seq_segurado_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_p;

if	(ie_tipo_item_p = 3) then
	select	dt_procedimento,
		dt_inicio_proc,
		dt_fim_proc
	into	dt_item_w,
		hr_inicio_w,
		hr_fim_w
	from	pls_conta_proc
	where	nr_sequencia = nr_seq_item_p;
elsif	(ie_tipo_item_p = 4) then
	select	dt_atendimento,
		dt_inicio_atend,
		dt_fim_atend
	into	dt_item_w,
		hr_inicio_w,
		hr_fim_w
	from	pls_conta_mat
	where	nr_sequencia = nr_seq_item_p;
end if;
	
if	(ie_tipo_guia_w in ('4', '6')) then
	select	max(nr_sequencia)
	into	nr_seq_conta_w
	from	pls_conta
	where	ie_tipo_guia = '5'
	and	cd_guia_referencia = cd_guia_referencia_w
	and	nr_seq_segurado	   = nr_seq_segurado_w;
end if;

/*O campo data de procedimento � segmentado para isso sereve este tratamento*/
if	(dt_item_w is not null) then
	dt_inicio_w := to_date(to_char(dt_item_w,'dd/mm/yyyy'));
	dt_final_w := to_date(to_char(dt_item_w,'dd/mm/yyyy'));
end if;

begin
if	(hr_inicio_w is not null) then
	dt_inicio_w	:= to_date(to_char(dt_inicio_w,'dd/mm/yyyy') ||' '||to_char(hr_inicio_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
else	
	dt_inicio_w	:= to_date(to_char(dt_inicio_w,'dd/mm/yyyy') ||' '||'00:00:00','dd/mm/yyyy hh24:mi:ss');
end if;
exception
when others then
	dt_inicio_w	:= null;
end;

begin
if	(hr_fim_w is not null) then
	dt_final_w	:= to_date(to_char(dt_final_w,'dd/mm/yyyy') ||' '||to_char(hr_fim_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
else
	if	(hr_inicio_w is not null) then
		dt_final_w	:= to_date(to_char(dt_final_w,'dd/mm/yyyy') ||' '||to_char(hr_inicio_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
	else
		dt_final_w	:= to_date(to_char(dt_final_w,'dd/mm/yyyy') ||' '||'00:00:00','dd/mm/yyyy hh24:mi:ss');
	end if;
end if;
exception
when others then
	dt_final_w	:= null;
end;

if	(dt_inicio_w is not null) or (dt_final_w is not null) then
	select	count(1) 
	into	qt_registros_w
	from	pls_conta
	where	nr_sequencia = nr_seq_conta_w
	and	(((dt_inicio_w < dt_entrada or dt_final_w > dt_alta) and (hr_inicio_w is not null)) or
		((not(trunc(dt_item_w) between trunc(dt_entrada) and trunc(dt_alta))) and (hr_inicio_w is null)));
		
	if	(qt_registros_w = 0) then
		ie_retorno_w	:= 'S';
	else
		ie_retorno_w	:= 'N';
	end if;
end if;

return	ie_retorno_w;

end pls_obter_item_fora_period_int;
/
