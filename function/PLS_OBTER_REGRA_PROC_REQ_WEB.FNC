create or replace
function pls_obter_regra_proc_req_web
			(	ie_tipo_guia_p		Varchar2,
				ie_evento_p		Varchar2,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type)
				return Varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter as regras de lan�amento autom�tico
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/				
				
nr_retorno_w			number(5);
ds_retorno_w			Varchar2(255)	:= 'S';
ie_restringe_estab_w		varchar2(2);
				
begin

ie_restringe_estab_w	:= pls_obter_se_controle_estab('LA');

select	count(1)
into	nr_retorno_w
from	pls_regra_lanc_automatico a,
	pls_regra_lanc_aut_item b
where	b.nr_seq_regra = a.nr_sequencia
and	a.ie_situacao	= 'A'
and	a.ie_evento 	= ie_evento_p
and	a.ie_tipo_guia	= ie_tipo_guia_p
and	(a.ie_origem_lancamento is null or a.ie_origem_lancamento = 'P' or a.ie_origem_lancamento = 'A')
and	((ie_restringe_estab_w	= 'N')
or	((ie_restringe_estab_w	= 'S') and (cd_estabelecimento = cd_estabelecimento_p)));

if	(nr_retorno_w	= 0) then
	ds_retorno_w	:= 'N';
end if;

return	ds_retorno_w;

end pls_obter_regra_proc_req_web;
/
