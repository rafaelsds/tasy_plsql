create or replace
function Obter_Desc_profissao
		(cd_profissao_p		number)
		return varchar2 is

ds_retorno_w		varchar2(80);

begin

if	(cd_profissao_p is not null) then
	begin
	select	ds_profissao
	into	ds_retorno_w
	from	profissao
	where	cd_profissao	= cd_profissao_p;
	end;
end if;

return	ds_retorno_w;

end Obter_Desc_profissao;
/