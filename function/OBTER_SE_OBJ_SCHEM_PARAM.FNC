create or replace function obter_se_obj_schem_param(	nr_seq_obj_schem_param_p		objeto_schematic_param.nr_sequencia%type,
					cd_funcao_p				funcao.cd_funcao%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					cd_perfil_p				perfil.cd_perfil%type,
					nm_usuario_p				usuario.nm_usuario%type,
					nr_seq_obj_schematic_p			objeto_schematic.nr_sequencia%type default null,
					ie_configuravel_p               			objeto_schematic.ie_configuravel%type default null)
					return varchar2  is

vl_retorno_w			varchar2(255) := 'S';
vl_parametro_padrao_w		varchar2(10);
vl_parametro_default_w		varchar2(10);
vl_parametro_pais_w			varchar2(10);
cd_estabelecimento_w		number(5);
i				integer;
nr_seq_obj_schem_param_w	number(10,0);
ie_configuravel_w		varchar2(10);


ie_nivel_atencao_perfil_w			varchar2(1);
ie_restringe_nivel_prim_w		varchar2(1);
ie_restringe_nivel_sec_w		varchar2(1);
ie_restringe_nivel_ter_w		varchar2(1);


cursor c01 is
	select	vl_parametro
	from obj_schem_perfil
	where nr_seq_obj_schem_param	= nr_seq_obj_schem_param_w
	  and cd_perfil    = cd_perfil_p
	  and coalesce(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	order by coalesce(cd_estabelecimento,0);

begin

if (ie_configuravel_p is not null) then
    ie_configuravel_w := ie_configuravel_p;
else
    select	max(ie_configuravel)
    into	ie_configuravel_w
    from	objeto_schematic
    where	nr_sequencia	= nr_seq_obj_schematic_p;
end if;

if	(ie_configuravel_w = 'S') then --Apenas se est� definido como 'Configur�vel', deve ser as regras.

	SELECT max(ie_visualiza)
	into vl_parametro_pais_w
	FROM Tab_pais
	WHERE nr_Seq_tab_ref = nr_seq_obj_schematic_p
	and cd_pais = NVL(Obter_Nr_Seq_Locale(nm_usuario_p), 1);

	select	max(a.nr_sequencia),
			max(a.vl_parametro_padrao),
			nvl(max(ie_restringe_nivel_primario),'N'),
			nvl(max(ie_restringe_nivel_secundario),'N'),
			nvl(max(ie_restringe_nivel_terciario),'N'),
			NVL(MAX(COALESCE(vl_parametro, COALESCE(vl_parametro_pais_w, VL_PARAMETRO_PADRAO))),'S')
	into	nr_seq_obj_schem_param_w,
			vl_parametro_padrao_w,
			ie_restringe_nivel_prim_w,
			ie_restringe_nivel_sec_w,
			ie_restringe_nivel_ter_w,
			vl_parametro_default_w
	from	objeto_schematic_param a
	where	a.nr_seq_obj_sch = nr_seq_obj_schematic_p;
	
	


	if ( cd_perfil_p is not null) and
	   ( cd_perfil_p > 0 ) then

		Select  nvl(max(ie_nivel_atencao),'T')
		into	ie_nivel_atencao_perfil_w
		from	perfil
		where  	cd_perfil =  cd_perfil_p;

		if (( ie_nivel_atencao_perfil_w is not null) and
		    ((ie_restringe_nivel_prim_w = 'S' and ie_nivel_atencao_perfil_w = 'P' ) or
   			 (ie_restringe_nivel_sec_w = 'S' and ie_nivel_atencao_perfil_w = 'S' ) or
			 (ie_restringe_nivel_ter_w = 'S' and ie_nivel_atencao_perfil_w = 'T' )))then

			vl_retorno_w := 'N';

		end if;

	end if;

	if (nvl(vl_retorno_w,'S') = 'S') then



		if	(vl_parametro_padrao_w = 'C') then

			if	(obter_se_base_corp = 'S' or obter_se_base_wheb = 'S') then
				vl_retorno_w := 'S';
			else
				vl_retorno_w := 'N';
			end if;

		elsif (vl_parametro_padrao_w = 'I') then
				vl_retorno_w := 'N';
		else
			cd_estabelecimento_w	:= coalesce(cd_estabelecimento_p, 1);

			begin
			select	vl_parametro
			into	vl_retorno_w
			from obj_schem_usuario
			where nr_seq_obj_schem_param	= nr_seq_obj_schem_param_w
			  and nm_usuario_param = nm_usuario_p
			  and coalesce(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
			  and	rownum = 1;
			 exception
				when others then
					begin
					i := 0;
					open c01;
					loop
					fetch c01 into vl_retorno_w;
					exit when c01%notfound;
					i := i + 1;
					end loop;
					close c01;

					if	(i = 0) then
						begin
						select	vl_parametro
						into	vl_retorno_w
						from obj_schem_estab
						where nr_seq_obj_schem_param	= nr_seq_obj_schem_param_w
						  and cd_estabelecimento    = cd_estabelecimento_w;
						exception
							when others then
								begin
								vl_retorno_w	:= vl_parametro_default_w;
								exception
									when others then
									   vl_retorno_w := 'S';
								end;
							end;
					end if;
					end;

			end;
		end if;
	end if;
end if;

return	nvl(vl_retorno_w,'S');

end obter_se_obj_schem_param;
/