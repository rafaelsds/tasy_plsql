create or replace
function obter_se_proc_classe(nr_seq_proc_interno_p		number,
		  	nm_usuario_p			varchar2)
 		 	return varchar2 is
ds_retorno_w	varchar2(1);
begin

select	nvl(max('S'), 'N')
into	ds_retorno_w
from	proc_interno_prof
where	ie_profissional = obter_classe_prof_usuario(nm_usuario_p)
and	nr_seq_proc_interno = nr_seq_proc_interno_p;

return	ds_retorno_w;

end obter_se_proc_classe;
/