create or replace
function obter_desc_dia_ciclo_ordem(nr_sequencia_p number)
 		    	return varchar2 is

ds_retorno_w  	varchar2(100);			
begin
if (nr_sequencia_p is not null) then
	SELECT MAX(ds_dia_ciclo)
	into   ds_retorno_w 
	FROM   paciente_atendimento a,
		   can_ordem_prod b
	WHERE  a.nr_seq_atendimento = b.nr_seq_atendimento
	AND    b.nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end obter_desc_dia_ciclo_ordem;
/