create or replace
function cpoe_obter_proxima_data_proced (
					dt_horario_p		date,
					nr_sequencia_p		number,
					ds_horarios_p		varchar2)
								return date is

ie_dia_semana_w		varchar2(1);
ie_segunda_w		cpoe_procedimento.ie_segunda%type;
ie_terca_w		cpoe_procedimento.ie_terca%type;
ie_quarta_w		cpoe_procedimento.ie_quarta%type;
ie_quinta_w		cpoe_procedimento.ie_quinta%type;
ie_sexta_w		cpoe_procedimento.ie_sexta%type;
ie_sabado_w		cpoe_procedimento.ie_sabado%type;
ie_domingo_w		cpoe_procedimento.ie_domingo%type;
ie_dia_dif		number;
dt_inicio_cpoe_w		date;
dt_horario_w		date;
ds_horarios_w		varchar2(255);
cont_w			number(3,0) := 0;

begin

-- Utilizado pra saber usa o by days
select	cpoe_obter_se_dia_diferenciado(nr_sequencia_p)
into	ie_dia_dif
from	dual;

-- Intervalo diferenciado por dias
if	(ie_dia_dif is not null) then
	begin

	-- Buscar data inicio do Procedimento
	select	cpoe_obter_data_hora_form(max(dt_inicio), max(hr_prim_horario))
	into	dt_inicio_cpoe_w
	from	cpoe_procedimento
	where	nr_sequencia = nr_sequencia_p
	and		trunc(dt_inicio) >= trunc(sysdate)
	and		hr_prim_horario is not null;

	 -- Pegar o minimo pra data futura
	select	min(d.dt_procedimento)
	into	dt_horario_w
	from	cpoe_dias_procedimento d
	where	d.nr_seq_proc_cpoe = nr_sequencia_p
	and		trunc(d.dt_procedimento) >= trunc(sysdate)
	order by dt_procedimento asc;

	if	(dt_horario_w is not null) then
		if	(dt_inicio_cpoe_w is not null) and
			(dt_inicio_cpoe_w < dt_horario_w) then
			return dt_inicio_cpoe_w;
		else
			return dt_horario_w;
		end if;
	end if;

	return dt_horario_p;
	end;

else
	begin
	dt_horario_w := dt_horario_p + 1;

	ds_horarios_w := obter_prim_dshorarios(ds_horarios_p);
	if	ds_horarios_w is null then
		ds_horarios_w := to_char(dt_horario_p, 'hh24:mi');
	end if;

	select	max(ie_segunda),
		max(ie_terca),
		max(ie_quarta),
		max(ie_quinta),
		max(ie_sexta),
		max(ie_sabado),
		max(ie_domingo)
	into	ie_segunda_w,
		ie_terca_w,
		ie_quarta_w,
		ie_quinta_w,
		ie_sexta_w,
		ie_sabado_w,
		ie_domingo_w
	from	cpoe_procedimento
	where	nr_sequencia	= nr_sequencia_p;

	loop
		select	pkg_date_utils.get_WeekDay(dt_horario_w)
		into	ie_dia_semana_w
		from	dual;

		if	(ie_dia_semana_w	= 1) and
			(ie_domingo_w		= 'S') then
			exit;
		elsif	(ie_dia_semana_w	= 2) and
			(ie_segunda_w		= 'S') then
			exit;
		elsif	(ie_dia_semana_w	= 3) and
			(ie_terca_w		= 'S') then
			exit;
		elsif	(ie_dia_semana_w	= 4) and
			(ie_quarta_w		= 'S') then
			exit;
		elsif	(ie_dia_semana_w	= 5) and
			(ie_quinta_w		= 'S') then
			exit;
		elsif	(ie_dia_semana_w	= 6) and
			(ie_sexta_w		= 'S') then
			exit;
		elsif	(ie_dia_semana_w	= 7) and
			(ie_sabado_w		= 'S') then
			exit;
		end if;

		dt_horario_w	:= dt_horario_w + 1;
		cont_w		:= cont_w + 1;

		-- condicao para nao entrar em looping caso nao houver dias pra gerar horario
		if	(cont_w >= 7) then
			dt_horario_w := dt_horario_p + 1;
			exit;
		end if;
	end loop;

	return to_date(to_char(dt_horario_w, 'dd/mm/yyyy') || ' ' || ds_horarios_w || ':00', 'dd/mm/yyyy hh24:mi:ss');

	end;
end if;

end cpoe_obter_proxima_data_proced;
/
