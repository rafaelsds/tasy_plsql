create or replace
function pls_obter_dados_item_exc_anexo	(	nr_seq_guia_p		pls_guia_plano.nr_sequencia%type,
						nr_seq_item_p		number,						
						ie_tipo_item_p		varchar2,	
						ie_opcao_p		varchar2)
						return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter dados dos procedimentos e materiais exclu�dos dos lotes de anexos das guias.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [   ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

/*
ie_tipo_item_p:
	P - procedimento
	M - material
-
ie_opcao_p:
	DPR - data prov�vel da administra��o
	OF - op��o  do fabricante
	NRA - n�mero registro Anvisa
	RF - refer�ncia do fabricante
	CAF - c�digo de autoriza��o de funcionamento	
	VA - via de administra��o
	FA - frequ�ncia de administra��o
*/

ds_retorno_w			varchar2(255);
dt_prev_realizacao_w		pls_lote_anexo_proc_aut.dt_prev_realizacao%type;
ds_opcao_fabricante_w		varchar2(255);
nr_registro_anvisa_w		pls_lote_anexo_mat_aut.nr_registro_anvisa%type;
cd_ref_fabricante_imp_w		pls_lote_anexo_mat_aut.cd_ref_fabricante_imp%type;
cd_aut_funcionamento_w		pls_lote_anexo_mat_aut.cd_aut_funcionamento%type;
ds_via_administracao_w		varchar2(255);
ie_frequencia_dose_w		pls_lote_anexo_mat_aut.ie_frequencia_dose%type;

begin

if (ie_tipo_item_p = 'P') then
	begin
		select	a.dt_prev_realizacao
		into	dt_prev_realizacao_w
		from	pls_lote_anexo_proc_aut a,
			pls_lote_anexo_guias_aut b
		where	a.nr_seq_lote_anexo_guia	= b.nr_sequencia
		and	a.nr_seq_motivo_exc is not null
		and 	b.nr_seq_guia			= nr_seq_guia_p
		and	a.nr_seq_plano_proc		= nr_seq_item_p;
	exception
	when others then
		dt_prev_realizacao_w	:= null;
	end;		
end if;

if (ie_tipo_item_p = 'M') then
	begin
		select	substr(obter_valor_dominio(7024, a.ie_opcao_fabricante), 1, 255) ds_opcao_fabricante,
			a.nr_registro_anvisa,
			a.cd_ref_fabricante_imp,
			a.cd_aut_funcionamento,
			substr(obter_valor_dominio(5674, a.ie_via_administracao), 1, 255) ds_via_administracao,
			ie_frequencia_dose
		into	ds_opcao_fabricante_w,
			nr_registro_anvisa_w,
			cd_ref_fabricante_imp_w,
			cd_aut_funcionamento_w,	
			ds_via_administracao_w,
			ie_frequencia_dose_w
		from	pls_lote_anexo_mat_aut a,
			pls_lote_anexo_guias_aut b
		where	a.nr_seq_lote_anexo_guia	= b.nr_sequencia
		and	a.nr_seq_motivo_exc is not null
		and 	b.nr_seq_guia			= nr_seq_guia_p
		and	a.nr_seq_plano_mat		= nr_seq_item_p;
	exception
	when others then
		ds_opcao_fabricante_w 		:= null;
		nr_registro_anvisa_w 		:= null;
		cd_ref_fabricante_imp_w 	:= null;
		cd_aut_funcionamento_w 		:= null;
		ds_via_administracao_w 		:= null;
	end;
end if;

if (ie_opcao_p = 'DPR') then
	ds_retorno_w	:= dt_prev_realizacao_w;
end if;
if (ie_opcao_p = 'OF') then
	ds_retorno_w	:= ds_opcao_fabricante_w;
end if;
if (ie_opcao_p = 'NRA') then
	ds_retorno_w	:= nr_registro_anvisa_w;
end if;
if (ie_opcao_p = 'RF') then
	ds_retorno_w	:= cd_ref_fabricante_imp_w;
end if;
if (ie_opcao_p = 'CAF') then
	ds_retorno_w	:= cd_aut_funcionamento_w;
end if;
if (ie_opcao_p = 'VA') then
	ds_retorno_w	:= ds_via_administracao_w;
end if;

if (ie_opcao_p = 'FA') then
	ds_retorno_w	:= ie_frequencia_dose_w;
end if;

return	ds_retorno_w;

end pls_obter_dados_item_exc_anexo;
/
