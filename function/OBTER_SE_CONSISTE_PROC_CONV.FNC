create or replace
function obter_se_consiste_proc_conv(cd_procedimento_p		number,
					ie_origem_proced_p	number)
 		    	return varchar2 is

ie_consistir_w	varchar2(1) := 'S';
cont_w		number(10,0);

begin

select	count(*)
into	cont_w
from	regra_proced_prescr_conv
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

if	(cont_w	> 0) then
	ie_consistir_w	:= 'N';
end if;

return	ie_consistir_w;

end obter_se_consiste_proc_conv;
/
