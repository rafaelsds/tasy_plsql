create or replace
function pls_obter_contrato_benef_pag
		(	nr_seq_pagador_p	number,
			nr_seq_segurado_p	number)
			return number is

nr_contrato_w		number(10);

begin
if	(nr_seq_pagador_p is not null) then
	select	b.nr_contrato
	into	nr_contrato_w
	from	pls_contrato 		b,
		pls_contrato_pagador	a
	where	a.nr_seq_contrato	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_pagador_p;
elsif   (nr_seq_segurado_p is not null) then
	select	b.nr_contrato
	into	nr_contrato_w
	from	pls_segurado		a,
		pls_contrato		b
	where	a.nr_seq_contrato	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_p;
end if;	
	
return	nr_contrato_w;

end pls_obter_contrato_benef_pag;
/