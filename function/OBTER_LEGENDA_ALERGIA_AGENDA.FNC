create or replace
function obter_legenda_alergia_agenda(dt_liberacao_p	date,
				dt_inativacao_p	date,
				dt_fim_p	date,
				dt_assinatura_p	date,
				ie_pendencia_assinatura_p	varchar2)
			return number is

nr_seq_legenda_w number;
ie_liberar_hist_saude_w varchar2(1);

begin

select	nvl(ie_liberar_hist_saude,'S')
into	ie_liberar_hist_saude_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if (ie_liberar_hist_saude_w = 'S') then
	begin

		if (dt_liberacao_p is null) then
			nr_seq_legenda_w := 1385;
		elsif (dt_inativacao_p is not null) then
			nr_seq_legenda_w := 1386;
		elsif (dt_fim_p is not null 
			and dt_fim_p >= sysdate ) then
			nr_seq_legenda_w := 1387;
		elsif (dt_liberacao_p is not null 
			and dt_inativacao_p is null
			and dt_assinatura_p is null
			and ie_pendencia_assinatura_p = 'S') then
			nr_seq_legenda_w := 4348;
		end if;
	end;
else
	if(dt_inativacao_p is not null) then
		nr_seq_legenda_w := 1386;
	end if;
end if;

return	nr_seq_legenda_w;

end obter_legenda_alergia_agenda;
/