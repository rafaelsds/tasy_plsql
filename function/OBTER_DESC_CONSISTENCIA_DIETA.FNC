create or replace
function Obter_desc_consistencia_dieta(	nr_seq_consistencia_p	Number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);			
			
begin

select	ds_consistencia
into	ds_retorno_w
from	consistencia_dieta
where	nr_sequencia = nr_seq_consistencia_p;

return	ds_retorno_w;

end Obter_desc_consistencia_dieta;
/
