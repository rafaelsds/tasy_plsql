create or replace
function obter_revisao_servico_pad(	nr_sequencia_p		number
									)
									return varchar2 is
									
ds_retorno_w			varchar2(4000);

begin

select count(*)
into 	ds_retorno_w
from 	hc_paciente_servico a,
		paciente_home_care	 b
where a.nr_seq_pac_home_care = b.nr_sequencia
and 	b.nr_sequencia = nr_sequencia_p
and 	a.dt_revisao is not null
and		a.dt_revisao <= sysdate;

if(ds_retorno_w > 0) then
	ds_retorno_w := 'S';
else 
	ds_retorno_w := 'N';	
end if;
		
return	ds_retorno_w;

end obter_revisao_servico_pad;
/
