create or replace
function hem_obter_cod_ventr(	nr_sequencia_p		Number)
 		    	return number is

cd_ventriculo_w	hem_desc_ventriculografia.cd_ventriculo%type;
begin
if	(nvl(nr_sequencia_p,0) > 0) then

	select	max(cd_ventriculo)
	into	cd_ventriculo_w
	from	hem_desc_ventriculografia
	where	nr_sequencia = nr_sequencia_p;
	
end if;
return	cd_ventriculo_w;

end hem_obter_cod_ventr;
/