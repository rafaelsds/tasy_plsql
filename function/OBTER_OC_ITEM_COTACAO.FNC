create or replace
function obter_oc_item_cotacao(	nr_cot_compra_p			number,
				nr_item_cot_compra_p		number,
				cd_material_p			number)
 		    	return number is

nr_ordem_compra_w			number(10);			
			
begin

select	max(nr_ordem_compra)
into	nr_ordem_compra_w
from	ordem_compra_item
where	nr_cot_compra		= nr_cot_compra_p
and	nr_item_cot_compra	= nr_item_cot_compra_p
and	cd_material		= cd_material_p;

return	nr_ordem_compra_w;

end obter_oc_item_cotacao;
/
