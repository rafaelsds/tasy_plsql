create or replace
function get_data_doc_nut_plan(	nr_atendimento_p	number,
					si_option_return_p	varchar2)
 		    	return varchar2 is

/*si_option_return_p
SEQ	 NR_SEQUENCIA
CDD	CD_DEPARTMENT
DSD	DS_DEPARTMENT
CDW	CD_WARD
DSW	DS_WARD
CDAD	CD_ATTENDING_DOCTOR
NMAD	NM_ATTENDING_DOCTOR
IEST	IE_STATUS
DSST	DS_STATUS*/

nr_sequencia_w			aval_nutricao.nr_sequencia%type;
cd_departamento_w		departamento_medico.cd_departamento%type;
ds_departmento_w		departamento_medico.ds_departamento%type;
cd_setor_atendimento_w		aval_nutricao.cd_setor_atendimento%type;
ds_setor_atendimento_w		setor_atendimento.ds_setor_atendimento%type;
cd_physician_w			pessoa_fisica.cd_pessoa_fisica%type;
nm_physician_w			pessoa_fisica.nm_pessoa_fisica%type;
ie_status_w			valor_dominio.vl_dominio%type;
ds_status_w			dic_expressao.ds_expressao_br%type;
ds_return_w			varchar2(255);
			
begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	aval_nutricao
where	nr_atendimento = nr_atendimento_p
and	ie_situacao = 'A';


/*SEQ	 NR_SEQUENCIA*/
if	(si_option_return_p = 'SEQ') and
	(nr_sequencia_w > 0) then
	begin
	ds_return_w := nr_sequencia_w;
	end;
	
/*CDD	CD_DEPARTMENT*/
elsif	(si_option_return_p = 'CDD') and
	(nr_sequencia_w > 0) then
	begin
	select	obter_departamento_setor(cd_setor_atendimento)
	into	cd_departamento_w
	from	aval_nutricao
	where	nr_sequencia = nr_sequencia_w;	
	
	ds_return_w := cd_departamento_w;
	end;
	
/*DSD	DS_DEPARTMENT*/
elsif	(si_option_return_p = 'DSD') and
	(nr_sequencia_w > 0) then
	begin
	select	substr(obter_nome_departamento_medico(obter_departamento_setor(cd_setor_atendimento)),1,255) 
	into	ds_departmento_w
	from	aval_nutricao
	where	nr_sequencia = nr_sequencia_w;	
	
	ds_return_w := ds_departmento_w;
	end;
	
/*CDW	CD_WARD*/
elsif	(si_option_return_p = 'CDW') and
	(nr_sequencia_w > 0) then
	begin
	select	cd_setor_atendimento
	into	cd_setor_atendimento_w
	from	aval_nutricao
	where	nr_sequencia = nr_sequencia_w;	
	
	ds_return_w := cd_setor_atendimento_w;
	end;

/*DSW	DS_WARD*/
elsif	(si_option_return_p = 'DSW') and
	(nr_sequencia_w > 0) then
	begin	
	select	substr(obter_nome_setor(cd_setor_atendimento),1,255)
	into	ds_setor_atendimento_w
	from	aval_nutricao
	where	nr_sequencia = nr_sequencia_w;	
	
	ds_return_w := ds_setor_atendimento_w;
	end;

/*CDAD	CD_ATTENDING_DOCTOR*/
elsif	(si_option_return_p = 'CDAD') and
	(nr_sequencia_w > 0) then
	begin	
	
	select	cd_profissional
	into	cd_physician_w
	from	aval_nutricao
	where	nr_sequencia = nr_sequencia_w;	
		
	ds_return_w := cd_physician_w;
	end;
	
/*NMAD	NM_ATTENDING_DOCTOR*/
elsif	(si_option_return_p = 'NMAD') and
	(nr_sequencia_w > 0) then
	begin
	
	select	substr(max(obter_nome_medico(cd_profissional,'N')),1,255)
	into	nm_physician_w
	from	aval_nutricao
	where	nr_sequencia = nr_sequencia_w;	
	
	ds_return_w := nm_physician_w;
	end;
	
/*IEST	IE_STATUS
DSST	DS_STATUS*/
elsif	(si_option_return_p in ('IEST','DSST')) then
	begin	
	
	if	(nvl(nr_sequencia_w,0) = 0) then
		ie_status_w := 'N'; /*Not created*/
		ds_status_w := wheb_mensagem_pck.get_Texto(1147367); /*Not created*/
	else
		
		select	decode(ie_status,'W','P',decode(dt_liberacao,null,'C','I')),
			decode(ie_status,'W',wheb_mensagem_pck.get_Texto(1152692),decode(dt_liberacao,null,wheb_mensagem_pck.get_Texto(1147368),wheb_mensagem_pck.get_Texto(1147369))) /*Not applicable or Created or Issued*/
		into	ie_status_w,
			ds_status_w
		from	aval_nutricao
		where	nr_sequencia = nr_sequencia_w;		
	end if;	
	
	if	(si_option_return_p = 'IEST') then
		ds_return_w := ie_status_w;
	elsif	(si_option_return_p = 'DSST') then
		ds_return_w := ds_status_w;
	end if;	
	end;	
end if;

return	ds_return_w;

end get_data_doc_nut_plan;
/

