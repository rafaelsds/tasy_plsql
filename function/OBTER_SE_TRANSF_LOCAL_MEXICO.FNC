create or replace
function obter_se_transf_local_mexico(cd_estabelecimento_p	in number,
					cd_operacao_estoque_p	in number)
					return varchar2 is

ie_requisicao_transferencia_w	varchar2(1) := 'N';
cd_oper_transf_saida_consig_w	parametro_estoque.cd_op_tr_saida_consig%type;
			
begin

if	(nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'es_MX') then
	select	cd_op_tr_saida_consig
	into	cd_oper_transf_saida_consig_w
	from	parametro_estoque
	where   cd_estabelecimento = cd_estabelecimento_p;
	
	if	(cd_operacao_estoque_p = cd_oper_transf_saida_consig_w) then
		ie_requisicao_transferencia_w := 'S';
	end if;
end if;

return ie_requisicao_transferencia_w;
end obter_se_transf_local_mexico;
/
