create or replace
function HLS_obter_medic_sol(nr_prescricao_p		number,
				nr_seq_solucao_p	number)
 		    	return number is

cd_material_w	number(15,4);
			
begin

select	max(cd_material)
into	cd_material_w
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia_solucao	= nr_seq_solucao_p;

return	cd_material_w;

end HLS_obter_medic_sol;
/
