create or replace
function Obter_dt_Visita_Hospitalidade (	nr_atendimento_p	number)
						return			date is
						
dt_visita_w		date;

begin


select	max(dt_evolucao)
into	dt_visita_w
from	evolucao_paciente a,
		tipo_evolucao b
where	a.ie_evolucao_clinica = b.cd_tipo_evolucao	
and 	a.nr_atendimento		= nr_atendimento_p
and		a.dt_liberacao is not null
and		a.dt_inativacao is null
and 	b.ie_evolucao_visita = 'S';

return dt_visita_w;

end Obter_dt_Visita_Hospitalidade;
/