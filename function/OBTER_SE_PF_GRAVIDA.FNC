create or replace 
function obter_se_pf_gravida(cd_pessoa_fisica_p	varchar2)
 	return varchar2 is

ds_retorno_w	varchar2(255);
ie_sexo_w	varchar2(1);

begin

if (cd_pessoa_fisica_p is not null) then
	select	a.ie_sexo
	into	ie_sexo_w
	from	pessoa_fisica a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p;

	if (ie_sexo_w = 'F') then
		
		select	max(a.ie_pac_gravida)
		into	ds_retorno_w
		from	historico_saude_mulher a
		where	nr_sequencia = (select max(nr_sequencia)
								from	historico_saude_mulher a
								where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
								and		dt_liberacao is not null
								and		dt_inativacao is null);
	else
		ds_retorno_w := obter_desc_expressao(293950);
	end if;
	
end if;

return	ds_retorno_w;

end obter_se_pf_gravida;
/
