create or replace
function obter_possui_medicamento_disp	(nr_seq_grupo_p	       number,
		                                                 ie_medicamento_p        varchar2,
                                	                                 cd_medico_p                varchar2,
                                                    		 cd_estabelecimento_p  number)
					 return varchar2 is
					
ie_retorno_w		varchar2(1) := 'N';

begin
select     nvl(max('S'), 'N')
into         ie_retorno_w  
from     	med_medic_padrao
where    	nr_seq_grupo_medic     = nr_seq_grupo_p 
and      	(     (ie_medicamento_p    = 'U' and cd_medico = cd_medico_p) 
          	 or  (ie_medicamento_p    = 'I' and ((cd_estabelecimento = cd_estabelecimento_p and cd_medico is null) or (cd_estabelecimento is null and cd_medico is null))))
and	      ((ie_medicamento_p     = 'I' and obter_se_mostra_medic_espec(nr_sequencia, obter_especialidade_medico(cd_medico_p, 'C')) = 'S') or (ie_medicamento_p     = 'U'))
and      	((ie_medicamento_p     = 'I' and obter_se_mostra_medic_perfil(nr_sequencia, OBTER_PERFIL_ATIVO) = 'S') or (ie_medicamento_p     = 'U'))
and         ie_situacao          = 'A';      

return ie_retorno_w;

end obter_possui_medicamento_disp;
/


