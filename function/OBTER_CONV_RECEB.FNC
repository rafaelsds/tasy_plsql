create or replace
function obter_conv_receb(
	nm_tabela_p		varchar2,
	nm_atributo_p	varchar2,
	cd_sistema_codificacao_p varchar2,
	cd_externo_p	varchar2
)
return varchar2 is

ds_value_w		varchar2(4000);

begin

select	cd_interno
into	ds_value_w
from	conversao_meio_externo
where	upper(nm_tabela)	= upper(nm_tabela_p)
and	upper(nm_atributo)	= upper(nm_atributo_p)
and (
	(cd_sistema_codificacao_p is null)
	or (upper(cd_sistema_codificacao) = upper(cd_sistema_codificacao_p))
)
and	upper(cd_externo)	= upper(cd_externo_p)
and	nvl(ie_envio_receb,'A') in ('R','A')
and rownum = 1
;

return ds_value_w;

exception
	when others then
		return null;

end obter_conv_receb;
/