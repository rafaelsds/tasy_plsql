create or replace
function obter_status_dispositivo(	nr_sequencia_p	number,
				qt_horario_p	number)
				return varchar2 is
					
ie_status_disp_w	varchar2(1);
dt_retirada_w	date;
dt_retirada_prev_w	date;

begin

select	dt_retirada,
	dt_retirada_prev
into	dt_retirada_w,
	dt_retirada_prev_w
from	atend_pac_dispositivo
where	nr_sequencia = nr_sequencia_p;

if	(dt_retirada_w is null) and (dt_retirada_prev_w is not null) and
	(dt_retirada_prev_w <= sysdate) then
	ie_status_disp_w	:= 'V';
elsif	(dt_retirada_w is null) and (dt_retirada_prev_w is not null) and
	(dt_retirada_prev_w > sysdate) and (dt_retirada_prev_w <= (sysdate + (qt_horario_p/24))) then
	ie_status_disp_w	:= 'E';
elsif	(dt_retirada_w is null) then
	ie_status_disp_w	:= 'A';
elsif	(dt_retirada_w is not null) then
	ie_status_disp_w	:= 'R';
end if;

return	ie_status_disp_w;

end obter_status_dispositivo;
/
