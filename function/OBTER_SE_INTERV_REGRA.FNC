create or replace
function Obter_se_interv_regra(	cd_intervalo_p		varchar2,
								ie_regra_p			varchar2)
 		    	return varchar2 is

ie_retorno_w	varchar2(10)	:= 'S';
cd_perfil_w		number;

begin

if	(cd_intervalo_p is not null) and
	(ie_regra_p is not null) then
	select	nvl(max('N'),'S')
	into	ie_retorno_w
	from	intervalo_prescr_regra
	where	cd_intervalo	= cd_intervalo_p;
	
	cd_perfil_w := obter_perfil_ativo;
	
	if	(ie_retorno_w = 'N') then
		select	nvl(max(ie_exibir),'S')
		into	ie_retorno_w
		from	intervalo_prescr_regra
		where	cd_intervalo	= cd_intervalo_p
		and		ie_regra		= ie_regra_p
		and 	nvl(cd_perfil, cd_perfil_w) = cd_perfil_w;
	end if;
end if;

return	ie_retorno_w;

end Obter_se_interv_regra;
/
