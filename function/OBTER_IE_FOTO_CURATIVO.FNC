create or replace
function obter_ie_foto_curativo ( nr_sequencia_p number)
				return varchar2 is

ds_retorno_w		varchar2(1) := '';

begin

if (nr_sequencia_p is not null) then

	select 	nvl(max('S'),'N')
	into 	ds_retorno_w
	from 	cur_ferida b,
			cur_foto c
	where 	c.nr_seq_ferida = b.nr_sequencia
	AND 	b.nr_sequencia    = nr_sequencia_p;	

end if ;

return ds_retorno_w;

end obter_ie_foto_curativo;
/