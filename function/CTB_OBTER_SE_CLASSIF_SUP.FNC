create or replace 
function ctb_obter_se_classif_sup(	cd_classif_atual_p		varchar2,
				cd_classificacao_p		varchar2)
				return 			varchar2 is

ie_pertence_w			varchar2(01)	:= 'N';
cd_classificacao_w			varchar2(40);

begin

cd_classificacao_w	:= cd_classif_atual_p;

if	(substr(cd_classificacao_w,1, length(cd_classificacao_p)) = cd_classificacao_p) then
	ie_pertence_w	:= 'S';
end if;

return ie_pertence_w;

end ctb_obter_se_classif_sup;
/