create or replace
function get_atend_checklist_result(nr_seq_checklist_p	number,
				nr_seq_item_checklist_p	number,
				ie_componente_p		varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
ds_result_checklist_w	atend_pac_checklist_result.ds_valor_result%type;
nr_seq_valor_result_w	atend_pac_checklist_result.nr_seq_valor_result%type;
ds_arquivo_w		atend_pac_checklist_result.ds_arquivo%type;

begin

if (nr_seq_checklist_p is not null) then

	select 	nvl(obter_descricao_padrao('CONVENIO_CHECKLIST_ITEM_VL', 'DS_VALOR', nr_seq_valor_result), ds_valor_result) ds_result_checklist,
		nr_seq_valor_result,
		ds_arquivo
	into	ds_result_checklist_w,
		nr_seq_valor_result_w,
		ds_arquivo_w
	from 	atend_pac_checklist_result
	where	nr_seq_checklist 	= nr_seq_checklist_p
	and	nr_seq_item_checklist 	= nr_seq_item_checklist_p;

	if (ie_componente_p in ('CB', 'LCB', 'RG')) then
		ds_retorno_w :=	nr_seq_valor_result_w;
	elsif (ie_componente_p in ('ED', 'ME')) then
		ds_retorno_w := ds_result_checklist_w;
	elsif (ie_componente_p = 'SERVER_FILE') then
		ds_retorno_w := ds_arquivo_w;
	end if;
	
end if;

return	ds_retorno_w;

end get_atend_checklist_result;
/
