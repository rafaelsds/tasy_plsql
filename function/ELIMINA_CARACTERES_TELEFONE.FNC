create or replace
function elimina_caracteres_telefone(	ds_campo_p	varchar2)
				return varchar2 is

ds_retorno_w			varchar2(255);
i				integer;

begin

if 	(ds_campo_p is not null) then
	begin

	for	i in 1 .. length(ds_campo_p) loop
		begin
		if	not (substr(ds_campo_p,i,1) in (' ','-','.',',','/','\','(',')')) then
			ds_retorno_w	:= substr(ds_retorno_w || substr(ds_campo_p,i,1),1,255);
		end if;
		end;
	end loop;
	end;
end if;

return	ds_retorno_w;

end elimina_caracteres_telefone;
/