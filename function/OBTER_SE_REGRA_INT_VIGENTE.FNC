create or replace
function obter_se_regra_int_vigente(
			cd_material_p			number,
			ie_sistema_externo_p		varchar2,
			cd_estabelecimento_p		number)
 		    	return varchar2 is

qt_existe_w		number(5);
ie_retorno_w		varchar2(1) := 'N';

begin

select	count(*)
into	qt_existe_w
from	conv_mat_sistema_externo
where	cd_sistema_externo is not null
and	cd_material = cd_material_p
and	ie_sistema = ie_sistema_externo_p
and	cd_estabelecimento = cd_estabelecimento_p
and	((sysdate between dt_inicio_vigencia and dt_fim_vigencia) or
	(dt_inicio_vigencia is null or dt_fim_vigencia is null));

if	(qt_existe_w > 0) then
	ie_retorno_w := 'S';
end if;

return	ie_retorno_w;

end obter_se_regra_int_vigente;
/