create or replace
function obter_se_regra_aut_lib_perfil(nr_seq_regra_autor_p	number)
 		    	return varchar2 is

ie_permite_w	varchar2(15) 	:= 'S';
qt_regra_w	number(10,0) 	:= 0;
cd_perfil_w	number(5,0) 	:= obter_perfil_ativo;
begin

begin
select	1
into	qt_regra_w
from	regra_gerar_autor_perfil a
where	a.nr_seq_regra_autor = nr_seq_regra_autor_p
and	rownum = 1;
exception
when others then
	qt_regra_w	:= 0;
end;

if	(qt_regra_w > 0) then

	begin
	select	'S'
	into	ie_permite_w
	from	regra_gerar_autor_perfil a
	where	nr_seq_regra_autor 	= nr_seq_regra_autor_p
	and	a.cd_perfil		= cd_perfil_w
	and	rownum			< 2;
	exception
	when others then
		ie_permite_w := 'N';
	end;

end if;

return	ie_permite_w;

end obter_se_regra_aut_lib_perfil;
/