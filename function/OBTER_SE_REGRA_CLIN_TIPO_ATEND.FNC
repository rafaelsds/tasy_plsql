create or replace
function obter_se_regra_clin_tipo_atend
			(cd_estabelecimento_p  	number,
			ie_tipo_atendimento_p  	number)
			return varchar2 is

qt_regra_w		number(10);

begin

select	count(*)
into	qt_regra_w
from	clinica_tipo_atendimento
where	ie_tipo_atendimento		= ie_tipo_atendimento_p
and 	cd_estabelecimento		= cd_estabelecimento_p
and	nvl(ie_situacao,'A') = 'A';

if	(qt_regra_w > 0) then
	return 'S';
else
	return 'N';
end if;

end obter_se_regra_clin_tipo_atend;
/
