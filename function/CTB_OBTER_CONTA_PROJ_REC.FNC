create or replace
function ctb_obter_conta_proj_rec(nr_seq_proj_rec_p projeto_recurso.nr_sequencia%type,
                                  ie_tipo_p         proj_rec_conta_contabil.ie_tipo%type)

return proj_rec_conta_contabil.cd_conta_contabil%type is

cd_conta_contabil_w proj_rec_conta_contabil.cd_conta_contabil%type;

begin
    begin
    select  a.cd_conta_contabil
    into    cd_conta_contabil_w
    from    proj_rec_conta_contabil a
    where   a.nr_seq_proj_rec = nr_seq_proj_rec_p
    and     a.ie_tipo = ie_tipo_p;
    exception 
        when others then
            cd_conta_contabil_w := null;
    end;

    return cd_conta_contabil_w;
end;
/
