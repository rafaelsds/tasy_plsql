create or replace FUNCTION cpoe_obter_documentos(cd_procedimento_p       NUMBER DEFAULT NULL,
                                                 ie_origem_proced_p      NUMBER DEFAULT NULL,
                                                 cd_perfil_p             NUMBER DEFAULT NULL,
                                                 ie_tipo_consentimento_p VARCHAR2 DEFAULT NULL,
                                                 nr_seq_proc_interno_p   NUMBER DEFAULT NULL,
                                                 ds_retorno_p              VARCHAR2 DEFAULT NULL) RETURN VARCHAR2 IS
  CURSOR cdocumento(cd_procedimento_p       NUMBER,
                    ie_origem_proced_p      NUMBER,
                    cd_perfil_p             NUMBER,
                    ie_tipo_consentimento_p VARCHAR2,
                    nr_seq_proc_interno_p   NUMBER) IS
    SELECT qc.ds_arquivo,
           qc.nr_sequencia,
           qc.nr_seq_tipo    
      FROM regra_geracao_consent rgc,
           qua_documento         qc
     WHERE ((rgc.cd_procedimento = cd_procedimento_p) OR (rgc.cd_procedimento IS NULL AND cd_procedimento_p IS NULL))
       AND (rgc.ie_origem_proced IS NULL OR ie_origem_proced_p IS NULL OR  rgc.ie_origem_proced = ie_origem_proced_p)
       AND (rgc.cd_perfil IS NULL OR rgc.cd_perfil = cd_perfil_p)
       AND (rgc.ie_tipo_consentimento IS NULL OR ie_tipo_consentimento_p IS NULL OR rgc.ie_tipo_consentimento = ie_tipo_consentimento_p)
       AND ((rgc.nr_seq_proc_interno = nr_seq_proc_interno_p) OR (rgc.nr_seq_proc_interno IS NULL AND  nr_seq_proc_interno_p IS NULL))
       AND rgc.nr_seq_documento = qc.nr_sequencia
       AND qc.ds_arquivo IS NOT NULL;

  ds_retorno_w VARCHAR2(4000) := '';
BEGIN
  FOR c IN cdocumento(cd_procedimento_p       => cd_procedimento_p,
                      ie_origem_proced_p      => ie_origem_proced_p,
                      cd_perfil_p             => cd_perfil_p,
                      ie_tipo_consentimento_p => ie_tipo_consentimento_p,
                      nr_seq_proc_interno_p   => nr_seq_proc_interno_p) LOOP

    if (ds_retorno_p = 'NR_SEQUENCIA') then
        ds_retorno_w := c.nr_sequencia||chr(10)||ds_retorno_w;
    elsif (ds_retorno_p = 'CD_TIPO_DOCUMENTO') then
        ds_retorno_w := c.nr_seq_tipo||chr(10)||ds_retorno_w;
    else
        ds_retorno_w := c.ds_arquivo||chr(10)||ds_retorno_w;
    end if;
  END LOOP;
  RETURN ds_retorno_w;
END;
/
