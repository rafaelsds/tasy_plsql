create or replace
function OBTER_VALOR_PEND_GUIA_DATA(	nr_interno_conta_p	in	number,
					cd_autorizacao_p	in	varchar2,
					dt_referencia_p		in date) return number is

vl_pendente_w		number(15,2)	:= 0;
vl_guia_w		number(15,2);
vl_pago_w		number(15,2);
vl_grg_w		number(15,2);

begin

select	nvl(sum(a.vl_guia),0)
into	vl_guia_w
from	conta_paciente_guia a
where 	a.nr_interno_conta	= nr_interno_conta_p
and	nvl(a.cd_autorizacao,wheb_mensagem_pck.get_Texto(310406))	= nvl(nvl(cd_autorizacao_p,a.cd_autorizacao),wheb_mensagem_pck.get_Texto(310406));

select	nvl(sum(a.vl_pago),0) + nvl(sum(a.vl_glosado),0)
into	vl_pago_w
from	convenio_retorno b,
	convenio_retorno_item a
where	a.nr_seq_retorno	= b.nr_sequencia
and	b.ie_status_retorno	= 'F'
and 	a.nr_interno_conta	= nr_interno_conta_p
and 	nvl(a.cd_autorizacao,wheb_mensagem_pck.get_Texto(310406)) 	= nvl(nvl(cd_autorizacao_p,a.cd_autorizacao),wheb_mensagem_pck.get_Texto(310406))
and	nvl(b.dt_baixa_cr,b.dt_fechamento)	<= dt_referencia_p;

select	nvl(sum(a.vl_glosa),0) + nvl(sum(a.vl_pago),0)
into	vl_grg_w
from	lote_audit_hist c,
	lote_audit_hist_item a,
	lote_audit_hist_guia b
where	b.nr_interno_conta		= nr_interno_conta_p
and	b.nr_sequencia			= a.nr_seq_guia
and	nvl(b.cd_autorizacao,wheb_mensagem_pck.get_Texto(310406))	= nvl(nvl(cd_autorizacao_p,b.cd_autorizacao),wheb_mensagem_pck.get_Texto(310406))
and	b.nr_seq_lote_hist		= c.nr_sequencia
and	c.nr_sequencia			<> to_number(obter_ultima_analise(c.nr_seq_lote_audit));

vl_pendente_w	:= nvl(vl_guia_w,0) - nvl(vl_pago_w,0) - nvl(vl_grg_w,0);

return vl_pendente_w;

end;
/
