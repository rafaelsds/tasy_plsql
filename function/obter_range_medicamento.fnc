create or replace function obter_range_medicamento(ds_material varchar2, pattern in varchar2) 
return varchar2 deterministic is
begin
return instr(ds_material, substr(pattern, 2, length(pattern) - 2)) || '-' ||  (LENGTH(pattern) - 2);
exception
    when others then
    return '0-0';
end;
/
