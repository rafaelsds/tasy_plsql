create or replace
function verificar_historico_alteracao(nr_seq_resultado_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin

ds_retorno_w := '';

select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	w_exame_lab_result_item
where 	nr_seq_resultado = nr_seq_resultado_p;

return	ds_retorno_w;

end verificar_historico_alteracao;
/
