create or replace
function obter_nome_diretor (	ie_cargo_p		varchar2,
				cd_estabelecimento_p	varchar2,
				ie_opcao_p	varchar2 default 'NCER')
 		    	return varchar2 is

ds_medico_w	varchar2(255);
			
begin

select	max(obter_nome_medico(cd_pessoa_fisica, nvl(ie_opcao_p,'NCER')))
into	ds_medico_w
from	ESTAB_DIRETORIA
where	ie_cargo		= ie_cargo_p
and	cd_estabelecimento	= cd_estabelecimento_p
and	sysdate	between dt_inicial and nvl(dt_fim_vigencia,sysdate +1);

return	ds_medico_w;

end obter_nome_diretor;
/