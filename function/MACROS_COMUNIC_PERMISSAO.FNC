create or replace
function macros_comunic_permissao return varchar2 is

ds_mensagem_w	varchar2(500);

begin

ds_mensagem_w :=	obter_traducao_macro_pront('@MEDICO', 3001) || ' = ' || wheb_mensagem_pck.get_texto(1034431) || chr(13) ||chr(10) || 			/*M�dico que possui a pemiss�o �nica para atendimento.*/
		obter_traducao_macro_pront('@DATA_LIBERACAO', 3001) || ' = ' || wheb_mensagem_pck.get_texto(1034432) || chr(13) ||chr(10) || 		/*Data de libera��o do registro de permiss�o �nica.*/
		obter_traducao_macro_pront('@USUARIO_LIBERACAO', 3001) ||  ' = ' || wheb_mensagem_pck.get_texto(1034433) || chr(13) ||chr(10) || 	/*Nome completo do usu�rio que liberou a permiss�o �nica.*/
		obter_traducao_macro_pront('@DATA_INICIO', 3001) ||  ' = ' || wheb_mensagem_pck.get_texto(1034434);				/*Data de �nicio da permiss�o �nica do m�dico.*/

return ds_mensagem_w;

end macros_comunic_permissao;
/