create or replace
function     obter_valor_padrao_pergunta(nr_seq_modelo_p   number)
return               varchar2 is

nr_sequencia_w       number(10);
vl_padrao_w  varchar2(80);
ds_retorno_w varchar2(2000);

cursor C01 is
     select  b.nr_sequencia,
             b.vl_padrao
     from    modelo_conteudo b,
             modelo a
     where   a.nr_sequencia  = nr_seq_modelo_p
     and     a.nr_sequencia  = b.nr_seq_modelo
     and     b.vl_padrao is not null;
begin

open c01;
loop
fetch        c01 into
     nr_sequencia_w,
     vl_padrao_w;
exit         when c01%notfound;
     begin
     ds_retorno_w := ds_retorno_w || to_char(nr_sequencia_w) ||'='||to_char(vl_padrao_w) ||'#@#@';
     end;
end  loop;
close        c01;

return ds_retorno_w;

end;
/