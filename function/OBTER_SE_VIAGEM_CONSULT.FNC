create or replace
function Obter_Se_Viagem_Consult
		(cd_consultor_p		varchar2,
		dt_parametro_p		date)
 		return varchar2 is

ds_retorno_w		varchar2(255);
		
begin

select	decode(b.nm_curto_agenda,null,'      ',' A ')  || nvl(( select 	decode(count(*), 0, ' ', 'V') ie_agen_viag
								from	via_viagem x, VIA_RESERVA  y
								where	x.nr_sequencia	= y.nr_seq_viagem
								and	((trunc(x.DT_SAIDA_PREV)		= trunc(a.dt_agenda)) or
									 (trunc(x.DT_RETORNO_PREV)		= trunc(a.dt_agenda)))
								and	x.cd_pessoa_fisica	= a.cd_consultor
								),'  ')  nm_curto_agenda
into	ds_retorno_w
from	proj_agenda a,
	proj_projeto b
where	a.nr_seq_proj 	= b.nr_sequencia(+)
and	a.cd_consultor 	= cd_consultor_p
and	a.ie_status not in ('Suspensa','Bloqueada','Cancelada')
and	trunc(dt_agenda,'dd') = dt_parametro_p;

return	ds_retorno_w;

end Obter_Se_Viagem_Consult;
/