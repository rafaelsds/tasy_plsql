create or replace
function obter_visibilidade_menu (
		nr_seq_objeto_p		number,
		ie_configuravel_p	varchar2,
		ie_visivel_p		varchar2,
		cd_estabelecimento_p	number,
		cd_perfil_p		number,
		nm_usuario_p		varchar2)
return varchar2 is

ie_visible_w	varchar2(1);
qt_registro_w	number(5);

Cursor C01 is
	select	ie_visible
	from	funcao_popup_regra
	where	nr_seq_objeto = nr_seq_objeto_p
	and		ie_tipo_regra = 'E'
	and		nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
	and		nvl(cd_perfil, cd_perfil_p) = cd_perfil_p
	and		nvl(nm_usuario_regra, nm_usuario_p) = nm_usuario_p
	and rownum = 1
	order by 
	nvl(nm_usuario_regra,'ZZZZZZZZZZZZZ'), 
	nvl(cd_perfil,0),
	nvl(cd_estabelecimento,0);

begin

if(ie_configuravel_p = 'N') then
	return ie_visivel_p;
end if;

open C01;
loop
fetch C01 into	
	ie_visible_w;
exit when C01%notfound;
end loop;
close C01;

if(ie_configuravel_p = 'S') then
	return	nvl(ie_visible_w, 'N');
end if;

return	nvl(ie_visible_w, 'S');

end	obter_visibilidade_menu;
/