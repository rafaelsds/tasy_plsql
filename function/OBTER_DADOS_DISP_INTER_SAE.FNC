create or replace
function obter_dados_disp_inter_sae	(	nr_seq_prescr_p			number,
										nr_seq_dispositivo_p	number,
										nr_seq_disp_atend_p 	number,
										ie_opcao_p				varchar2,
										nr_atendimento_p 		number,
										nr_seq_proc_p 			number)
					return varchar2 is
ds_retorno_w	varchar2(255);
ie_gerar_w	varchar2(1);
ds_hora_w	varchar2(255);


/*
G - Gerar
H - Hora
*/

begin

select 	max(ie_gerar),
		max(hr_prim_horario)
into	ie_gerar_w,
		ds_hora_w
from	dispositivo_interv_sae_alt
where	nr_seq_prescr = nr_seq_prescr_p
and		nr_seq_dispositivo = nr_seq_dispositivo_p
and		nr_seq_disp_atend = nr_seq_disp_atend_p
and		nr_atendimento = nr_atendimento_p
and		nr_seq_proc = nr_seq_proc_p;


if	(ie_opcao_p = 'G') then
	ds_retorno_w	:= nvl(ie_gerar_w,'S');

elsif	(ie_opcao_p = 'H') then
	ds_retorno_w := ds_hora_w;

end if;

return ds_retorno_w;

end;
/