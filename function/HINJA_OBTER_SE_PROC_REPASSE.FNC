create or replace
function HINJA_OBTER_SE_PROC_REPASSE(nr_seq_proc_p number)
					return number is


/*
retorno:

0 - Gera repasse
1 - N�o gera
*/

cont_w			number(15);
cd_medico_executor_w	varchar2(10);

begin

select	max(a.cd_medico_executor)
into	cd_medico_executor_w
from	procedimento_paciente a
where	a.nr_sequencia	= nr_seq_proc_p;

select	count(*)
into	cont_w
from	dual
where	nr_seq_proc_p	in	(
				select	n.nr_sequencia
				from	(
					select	m.nr_sequencia
					from	procedimento_paciente m
					where	m.cd_procedimento		in ('25040014', '25020145')
					and	m.cd_medico_executor		= cd_medico_executor_w
					and	trunc(m.dt_procedimento, 'dd')	= trunc(sysdate, 'dd')
					order 	by 1
					) n
				where	rownum	<= 2
				);


if	(cont_w > 0) then
	return	0;
else
	return	1;
end if;

end	HINJA_OBTER_SE_PROC_REPASSE;
/
