create or replace
function pls_obter_registro_ans
			(	cd_estabelecimento_p	varchar2)
 		    		return varchar2 is

/*
 * Retorna o registro ANS a partir do estabelecimento passado por par�metro.
 */

ds_retorno_w	pls_outorgante.cd_ans%type;

begin

select	max(cd_ans)
into	ds_retorno_w
from	pls_outorgante
where	cd_estabelecimento = cd_estabelecimento_p;
    
if (ds_retorno_w is null) then
    select	max(cd_ans)
    into	ds_retorno_w
    from	pls_outorgante;
end if;

return ds_retorno_w;

end pls_obter_registro_ans;
/
