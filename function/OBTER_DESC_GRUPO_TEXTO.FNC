create or replace
function obter_desc_grupo_texto(	nr_seq_grupo_p	number)
								return varchar2 is

ds_retorno_w		varchar2(255);

begin
if	(nvl(nr_seq_grupo_p,0) > 0) then
	select	max(ds_grupo)
	into	ds_retorno_w
	from	grupo_texto_padrao
	where	nr_sequencia = nr_seq_grupo_p;
end if;

return	ds_retorno_w;

end obter_desc_grupo_texto;
/