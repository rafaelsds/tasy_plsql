create or replace
function regra_convenio_protocolo_venc(	cd_convenio_p		number,
					dt_entrega_p		date)
					return date is
	
dt_vencimento_w	date;
qt_existe_w	number(10);
nr_dias_venc_w	number(10);

begin

select	count(*)
into	qt_existe_w
from	conv_regra_venc
where	cd_convenio = cd_convenio_p;

if	(qt_existe_w > 0) then

	select	max(dt_vencimento)
	into	dt_vencimento_w
	from	conv_regra_venc
	where	cd_convenio = cd_convenio_p
	and	trunc(dt_entrega_p,'dd') between trunc(dt_entrega_inicial,'dd') and trunc(dt_entrega_final,'dd')
	and	ie_situacao = 'A';

else
	
	select	trunc(dt_entrega_p,'dd') + nvl(max(nr_dias_venc),0)
	into	dt_vencimento_w
	from	convenio_estabelecimento
	where	cd_convenio = cd_convenio_p
	and	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

end if;

return	dt_vencimento_w;

end regra_convenio_protocolo_venc;
/