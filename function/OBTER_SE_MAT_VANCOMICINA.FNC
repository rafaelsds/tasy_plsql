create or replace
function Obter_se_mat_vancomicina(	cd_material_p	number)
 		    	return varchar2 is

ie_vancomicina_w	varchar2(1)	:= 'N';
begin

if	(cd_material_p is not null) then
	select	nvl(max(ie_vancomicina), 'N')
	into	ie_vancomicina_w
	from	material
	where	cd_material = cd_material_p;
end if;

return	ie_vancomicina_w;

end Obter_se_mat_vancomicina;
/