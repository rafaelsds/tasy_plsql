create or replace
function obter_cor_exame_grid(	ds_cor_p	varchar2,
				nr_coluna_p	number)
 		    	return number is

lista_w	dbms_sql.varchar2_table;
ds_retorno_w	number(10);
ds_linha_w	varchar2(255);
begin

if	(ds_cor_p	is not null) then

	lista_w	:= obter_lista_string(ds_cor_p,',');

	if	(lista_w.count	>0) then
		
		for i in lista_w.first..lista_w.last loop
			begin
			ds_linha_w	:= lista_w(i);
			
			if	(substr(ds_linha_w,1,2)	= nr_coluna_p) then
				ds_retorno_w	:= somente_numero(substr(ds_linha_w,3));
			end if;
			
			end;
		end loop;
	end if;
end if;

return	ds_retorno_w;

end obter_cor_exame_grid;
/
