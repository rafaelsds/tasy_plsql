create or replace
function tre_obter_se_treinamento_setor(	nr_seq_tipo_p		number,
					cd_setor_atendimento_p	number,
					ie_padrao_retorno_p	varchar2 default 'S')
					return varchar2 is

ds_retorno_w	varchar2(1);
qt_existe_w	number(10,0);

begin
ds_retorno_w := ie_padrao_retorno_p;

select	count(*)
into	qt_existe_w
from	tre_tipo_setor
where	nr_seq_tipo	= nr_seq_tipo_p;

if	(qt_existe_w > 0) then
	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	tre_tipo_setor a
	where	a.nr_seq_tipo		= nr_seq_tipo_p
	and	a.cd_setor_atendimento	= cd_setor_atendimento_p;	
end if;
	
return	ds_retorno_w;

end tre_obter_se_treinamento_setor;
/