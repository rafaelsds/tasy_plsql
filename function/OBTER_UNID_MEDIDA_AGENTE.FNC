create or replace
function obter_unid_medida_agente(	nr_seq_agente_p	number)
					return varchar2 is

ds_retorno_w	varchar2(100);	

begin

select	obter_desc_unid_med(cd_unid_med_apres)
into	ds_retorno_w
from	agente_anestesico
where	nr_sequencia	=	nr_seq_agente_p;

return	ds_retorno_w;

end obter_unid_medida_agente;
/