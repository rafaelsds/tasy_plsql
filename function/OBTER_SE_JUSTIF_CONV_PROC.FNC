create or replace
function obter_se_justif_conv_proc( 	nr_atendimento_p 		number,
										nr_seq_proc_interno_p	number,
										cd_procedimento_p		number,
										ie_origem_proced_p		number)
 		    	return varchar2 is

qt_regra_w			Number(10) := 0;
ie_exige_w			Varchar(255) := 'N';
cont_w				number(10,0) := 0;
cd_convenio_w		Number(5);
			
begin

If (nr_atendimento_p > 0 ) then


	Select 	Obter_Convenio_Atendimento(nr_atendimento_p)
	into	cd_convenio_w
	from	dual;

	if	(nr_seq_proc_interno_p > 0) then

		select	count(*)
		into	cont_w
		from	regra_convenio_plano a
		where	nr_seq_proc_interno = nr_seq_proc_interno_p
		and	cd_convenio = nvl(cd_convenio_w,0)
		and	ie_regra = 6;

	end if;	

	if	( cont_w = 0)  and
		( cd_procedimento_p > 0 ) and
		( ie_origem_proced_p > 0) then
		
		select	count(*)
		into	cont_w
		from	regra_convenio_plano a
		where	cd_procedimento = cd_procedimento_p
		and	nr_seq_proc_interno = nr_seq_proc_interno_p
		and	cd_convenio = nvl(cd_convenio_w,0)
		and	ie_regra = 6;
		
	end if;

	if (cont_w > 0 ) then

		ie_exige_w := 'S';

	end if;

end if;

return	ie_exige_w;

end obter_se_justif_conv_proc;
/
