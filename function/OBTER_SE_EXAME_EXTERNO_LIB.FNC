CREATE OR REPLACE
FUNCTION Obter_se_exame_externo_lib(
     				nr_atendimento_p  number)
			     RETURN Varchar2 IS
qt_exame_w		Number(10);
qt_exame_lib_w		Number(10);
ds_retorno_w		Varchar2(1);
ie_aprov_bioquimico_w	varchar2(3);
ie_status_atend_w	Number(2)	:= 35;

/*

P - Exame prescrito e realizado externamente, j� liberado e pendente por fazer
S - Exame prescrito e realizado externamente, j� liberado e j� feito
R - Exame prescrito e pendente de realiza��o
N - N�o tem exame prescrito e realizado externamente para o paciente
*/

BEGIN

select	max(nvl(vl_parametro,vl_parametro_padrao))
into	ie_aprov_bioquimico_w
from	funcao_parametro
where	cd_funcao	= 281
and	nr_sequencia	= 176;

if	(ie_aprov_bioquimico_w = 'S') then
	ie_status_atend_w	:= 30;
end if;

select	count(*)
into	qt_exame_w
from	prescr_procedimento b,	
	prescr_medica a
where	a.nr_prescricao  = b.nr_prescricao
and	a.nr_atendimento = nr_atendimento_p
and	(Obter_se_exame_presc_externo(b.cd_procedimento,b.nr_seq_proc_interno) = 'S')
and	b.nr_seq_exame is not null
and	(a.dt_liberacao is not null or a.dt_liberacao_medico is not null);

select	count(*)
into	qt_exame_lib_w
from	prescr_procedimento b,	
	prescr_medica a
where	a.nr_prescricao  = b.nr_prescricao
and	a.nr_atendimento = nr_atendimento_p
and	(Obter_se_exame_presc_externo(b.cd_procedimento,b.nr_seq_proc_interno) = 'S')
and	b.ie_status_atend >= ie_status_atend_w
and	b.nr_seq_exame is not null
and	(a.dt_liberacao is not null or a.dt_liberacao_medico is not null);

if 	(qt_exame_lib_w < qt_exame_w ) and ( qt_exame_lib_w <> 0 ) then
	ds_retorno_w := 'P';
elsif ( qt_exame_lib_w = qt_exame_w) and  (qt_exame_w <> 0) then
	ds_retorno_w := 'S';
elsif (qt_exame_lib_w < qt_exame_w ) then
	ds_retorno_w := 'R';	
else
	ds_retorno_w := 'N';
end if;

RETURN	ds_retorno_w;

END	Obter_se_exame_externo_lib;
/