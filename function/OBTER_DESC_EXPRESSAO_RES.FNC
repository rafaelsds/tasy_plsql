create or replace 
function obter_desc_expressao_res(	ds_expressao_p		VARCHAR2,
									nr_seq_idioma_p		VARCHAR2 DEFAULT NULL) RETURN VARCHAR2 IS

return_w				VARCHAR2(4000);

BEGIN

IF (upper(ds_expressao_p) IS NOT NULL) AND
	(nr_seq_idioma_p IS NOT NULL ) THEN

--Problemas
	IF (upper(ds_expressao_p) = 'DATE OF ONSET') THEN
		return_w:= obter_desc_expressao_idioma(286964, NULL, nr_seq_idioma_p);
	END IF;
--Vital Signs
	IF (upper(ds_expressao_p) = 'SYSTOLIC') THEN
		return_w:= obter_desc_expressao_idioma(649100, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'DIASTOLIC') THEN
		return_w:= obter_desc_expressao_idioma(649101, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'HEART RATE') THEN
		return_w:= obter_desc_expressao_idioma(310715, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'WEIGHT') THEN
		return_w:= obter_desc_expressao_idioma(295698, NULL, nr_seq_idioma_p);
	END IF;

--FAMILY HISTORY - 	Family History Summary / ExclusionFamilyHistory
	IF (upper(ds_expressao_p) = 'RELATIONSHIP') THEN
		return_w:= obter_desc_expressao_idioma(295291, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'DATE OF BIRTH') THEN
		return_w:= obter_desc_expressao_idioma(287065, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'DECEASED?') THEN
		return_w:= obter_desc_expressao_idioma(490833, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'AGE AT DEATH') THEN
		return_w:= obter_desc_expressao_idioma(887708, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PROBLEM/DIAGNOSIS') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(857340, NULL, nr_seq_idioma_p)||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'CLINICAL DESCRIPTION') THEN
		return_w:= obter_desc_expressao_idioma(291927, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'AGE AT ONSET') THEN
		return_w:= obter_desc_expressao_idioma(286838, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'CAUSE OF DEATH?') THEN
		return_w:= obter_desc_expressao_idioma(284709, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'SUMMARY') THEN
		return_w:= obter_desc_expressao_idioma(315608, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'FAMILY HISTORY') THEN
		return_w:= obter_desc_expressao_idioma(314081, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'COMMENT') THEN
		return_w:= obter_desc_expressao_idioma(285521, NULL, nr_seq_idioma_p);
	END IF;

--ANTHROPOMETRY -  HeightLength / BodyWeight

	IF (upper(ds_expressao_p) = 'HEIGHT/LENGTH') THEN
		return_w:= obter_desc_expressao_idioma(283402, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'COMMENT') THEN
		return_w:= obter_desc_expressao_idioma(285521, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'WEIGHT') THEN
		return_w:= obter_desc_expressao_idioma(295698, NULL, nr_seq_idioma_p);
	END IF;


--FAMILY HISTORY -  Family History /  Family History Summary / Exclusion of Family History

	IF (upper(ds_expressao_p) = 'SUBSTANCE') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(298923, NULL, nr_seq_idioma_p)||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'USAGE STATUS') THEN
		return_w:= obter_desc_expressao_idioma(291200, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'CONSUMPTION SUMMARY') THEN
		return_w:= obter_desc_expressao_idioma(315608, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'FORM') THEN
		return_w:= obter_desc_expressao_idioma(290241, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'DATE COMMENCED') THEN
		return_w:= obter_desc_expressao_idioma(286737, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'AGE COMMENCED') THEN
		return_w:= obter_desc_expressao_idioma(291565, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'DATE CEASED') THEN
		return_w:= obter_desc_expressao_idioma(286732, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'AGE CEASED') THEN
		return_w:= obter_desc_expressao_idioma(291557, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PHYSICAL ACTIVITY NOTE') THEN
		return_w:= obter_desc_expressao_idioma(283895, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'BARRIERS TO ACTIVITY') THEN
		return_w:= obter_desc_expressao_idioma(887914, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'MODERATE EXERCISE/WEEK') THEN
		return_w:= obter_desc_expressao_idioma(887908, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'VIGOROUS EXERCISE/WEEK') THEN
		return_w:= obter_desc_expressao_idioma(887910, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'METHOD OF USE') THEN
		return_w:= obter_desc_expressao_idioma(293273, null, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PHYSICAL ACTIVITY STATUS') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(762644, null, nr_seq_idioma_p)||'</b>';
	END IF;

--IMUNIZAÇOES
	IF (upper(ds_expressao_p) = 'BATCHID') THEN
		return_w:= obter_desc_expressao_idioma(333447, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'VACCINE') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(301214, NULL, nr_seq_idioma_p)||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'EXPIRY DATE') THEN
		return_w:= obter_desc_expressao_idioma(759795, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'SEQUENCE NUMBER') THEN
		return_w:= obter_desc_expressao_idioma(761598, NULL, nr_seq_idioma_p);
	END IF;
	
--EDUCAÇÃO AO PACIENTE
	IF (upper(ds_expressao_p) = 'TOPIC') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(300152, NULL, nr_seq_idioma_p)||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'DESCRIPTION') THEN
		return_w:= obter_desc_expressao_idioma(861051, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'METHOD') THEN
		return_w:= obter_desc_expressao_idioma(315277, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'IDENTIFICATION') THEN
		return_w:= obter_desc_expressao_idioma(291637, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'REASON') THEN
		return_w:= obter_desc_expressao_idioma(293478, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'OUTCOME') THEN
		return_w:= obter_desc_expressao_idioma(297817, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'SESSION NUMBER') THEN
		return_w:= obter_desc_expressao_idioma(629764, NULL, nr_seq_idioma_p);
	END IF;


--TIPOS SANGUÍNEOS

	IF (upper(ds_expressao_p) = 'RHESUS') THEN
		return_w:= obter_desc_expressao_idioma(311794, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'OVERALL COMMENT') THEN
		return_w:= obter_desc_expressao_idioma(285521, NULL, nr_seq_idioma_p);
	END IF;
	
	
--RESULTADO DE TESTES DE PATOLOGIA
	IF (upper(ds_expressao_p) = 'DIAGNOSTIC SERVICE') THEN
		return_w:= obter_desc_expressao_idioma(287694, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'SPECIMEN TYPE') THEN
		return_w:= obter_desc_expressao_idioma(299540, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'DATE/TIME OF COLLECTION') THEN
		return_w:= obter_desc_expressao_idioma(286665, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'OVERALL TEST RESULT STATUS') THEN
		return_w:= obter_desc_expressao_idioma(712701, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'DATETIME RESULT ISSUED') THEN
		return_w:= obter_desc_expressao_idioma(506568, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'RESULT VALUE') THEN
		return_w:= obter_desc_expressao_idioma(301357, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'RESULT COMMENT') THEN
		return_w:= obter_desc_expressao_idioma(887898 , NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'RESULT VALUE STATUS') THEN
		return_w:= obter_desc_expressao_idioma(298824, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'CONCLUSION') THEN
		return_w:= obter_desc_expressao_idioma(285638, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PATHOLOGICAL DIAGNOSIS') THEN
		return_w:= obter_desc_expressao_idioma(622049, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'TEST COMMENT') THEN
		return_w:= obter_desc_expressao_idioma(285521, NULL, nr_seq_idioma_p);
	END IF;
	
--EXAMES DE IMAGEM
	IF (upper(ds_expressao_p) = 'EXAMINATION REQUESTED') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(610596, NULL, nr_seq_idioma_p)||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'DESCRIPTION OF EXAMINATION') THEN
		return_w:= obter_desc_expressao_idioma(753404, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'EXAM CATEGORY') THEN
		return_w:= obter_desc_expressao_idioma(284678, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'NAME OF LOCATION') THEN
		return_w:= obter_desc_expressao_idioma(596229, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'SIDE') THEN
		return_w:= obter_desc_expressao_idioma(292416, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'NUMERICAL IDENTIFIER') THEN
		return_w:= obter_desc_expressao_idioma(679044, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'ANATOMICAL PLANE') THEN
		return_w:= obter_desc_expressao_idioma(291047, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'REASON FOR REQUEST') THEN
		return_w:= obter_desc_expressao_idioma(292337, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'INTENT') THEN
		return_w:= obter_desc_expressao_idioma(294566, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'URGENCY') THEN
		return_w:= obter_desc_expressao_idioma(300864, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'DATE AND/OR TIME SERVICE REQUIRED') THEN
		return_w:= obter_desc_expressao_idioma(312817, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'LATEST DATE SERVICE REQUIRED') THEN
		return_w:= obter_desc_expressao_idioma(887896, NULL, nr_seq_idioma_p);
	END IF;

--PROCEDIMENTOS
	IF (upper(ds_expressao_p) = 'PROCEDURE') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(296422, NULL, nr_seq_idioma_p)||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'REASON/S FOR PROCEDURE') THEN
		return_w:= obter_desc_expressao_idioma(293478, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'METHOD/TECHNIQUE') THEN
		return_w:= obter_desc_expressao_idioma(293273, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'COMPLICATION') THEN
		return_w:= obter_desc_expressao_idioma(285579, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'COMMENTS') THEN
		return_w:= obter_desc_expressao_idioma(753406, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PROCEDURE REQUESTED') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(296458, NULL, nr_seq_idioma_p)||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'DESCRIPTION OF PROCEDURE') THEN
		return_w:= obter_desc_expressao_idioma(287571, NULL, nr_seq_idioma_p);
	END IF;
	
	
	
--Antecedente Familiar
	IF (upper(ds_expressao_p) = 'EVER PREGNANT?') THEN
		return_w:= obter_desc_expressao_idioma(289481, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'GRAVIDITY (G)') THEN
		return_w:= obter_desc_expressao_idioma(293752, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PARITY') THEN
		return_w:= obter_desc_expressao_idioma(703120, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'TERM BIRTHS (T)') THEN
		return_w:= obter_desc_expressao_idioma(888117, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PRETERM BIRTHS (P)') THEN
		return_w:= obter_desc_expressao_idioma(888119, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'ABORTIONS (A)') THEN
		return_w:= obter_desc_expressao_idioma(283073, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'MISCARRIAGES') THEN
		return_w:= obter_desc_expressao_idioma(802415, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'TERMINATIONS') THEN
		return_w:= obter_desc_expressao_idioma(797532, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'ECTOPIC PREGNANCIES') THEN
		return_w:= obter_desc_expressao_idioma(888121, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'STILLBIRTHS') THEN
		return_w:= obter_desc_expressao_idioma(293982, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'LIVE BIRTHS') THEN
		return_w:= obter_desc_expressao_idioma(293971, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'CAESAREAN SECTIONS') THEN
		return_w:= obter_desc_expressao_idioma(559834, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'MULTIPLE BIRTHS (M)') THEN
		return_w:= obter_desc_expressao_idioma(490468, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'LIVING CHILDREN (L)') THEN
		return_w:= obter_desc_expressao_idioma(293971, NULL, nr_seq_idioma_p);
	END IF;

---

	IF (upper(ds_expressao_p) = 'MODALITY') THEN
		return_w:= obter_desc_expressao_idioma(293381, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'EXCLUSION STATEMENT') THEN
		return_w:= obter_desc_expressao_idioma(289757, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'OVERALL RESULT STATUS') THEN
		return_w:= obter_desc_expressao_idioma(753441, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'FINDINGS') THEN
		return_w:= obter_desc_expressao_idioma(306200, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'RADIOLOGICAL DIAGNOSIS') THEN
		return_w:= obter_desc_expressao_idioma(769600, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'EXAMINATION COMMENT') THEN
		return_w:= obter_desc_expressao_idioma(285521, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PATOLOGIA') THEN
		return_w:= '<b>'||'PATOLOGIA'||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'PATHOLOGY TEST REQUESTED') THEN
		return_w:= '';
	END IF;
	IF (upper(ds_expressao_p) = 'EXAMINATION_RESULT_NAME') THEN
		return_w:= '<b>'||obter_desc_expressao_idioma(515570, NULL, nr_seq_idioma_p)||'</b>';
	END IF;
	IF (upper(ds_expressao_p) = 'ABO') THEN
		return_w:= obter_desc_expressao_idioma(283059, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'PATHOLOGY TEST REQUESTED') THEN
		return_w:='';
	END IF;
	IF (upper(ds_expressao_p) = 'RATE') THEN
		return_w:= 'NUSANUDSANUDUSNADUNANUD';
	END IF;
	IF (upper(ds_expressao_p) = 'DATE OF ATTEMPT') THEN
		return_w:= obter_desc_expressao_idioma(287188, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'ATTEMPT DETAILS') THEN
		return_w:= obter_desc_expressao_idioma(287643, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'CESSATION ATTEMPTS') THEN
		return_w:= obter_desc_expressao_idioma(327443, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'LOCATION') THEN
		return_w:= obter_desc_expressao_idioma(292719, NULL, nr_seq_idioma_p);
	END IF;
	IF (upper(ds_expressao_p) = 'NO INFORMATION') THEN
		return_w:= '';
	END IF;

END IF;

IF(return_w IS NULL) THEN

	return_w:= '';


END IF;

RETURN return_w;


END obter_desc_expressao_res;
/