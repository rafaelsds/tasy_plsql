create or replace
function man_obter_qt_os_cliente
			(	nr_seq_gerencia_p	number,
				nr_seq_localizacao_p	number)
 		    	return number is

qt_registros_w		number(10);			
			
begin

select	count(*)
into	qt_registros_w
from	man_ordem_servico		a,
	grupo_desenvolvimento	b,
	gerencia_wheb 		c,
	man_localizacao		d
where	a.nr_seq_grupo_des	= b.nr_sequencia
and	b.nr_seq_gerencia	= c.nr_sequencia
and	a.nr_seq_localizacao	= d.nr_sequencia
and	c.nr_sequencia		= nr_seq_gerencia_p
and	(substr(obter_se_os_pend_cliente(a.nr_sequencia),1,1) = 'N')
and	(substr(Obter_Se_OS_Desenv(a.nr_sequencia),1,1) = 'S')
and	a.ie_status_ordem <> 3
and	proj_obter_dados_ordem_serv(a.nr_sequencia, 'PROJD') is null
and	d.nr_sequencia	= nr_seq_localizacao_p;

return	qt_registros_w;

end man_obter_qt_os_cliente;
/
