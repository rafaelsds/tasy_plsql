create or replace
function sus_obter_desc_tipo_logr (	cd_tipo_logradouro_p	varchar2)
					return varchar2 is

ds_retorno_w		varchar2(40);
					
begin

begin
select 	nvl(ds_tipo_logradouro,'')
into	ds_retorno_w
from 	sus_tipo_logradouro
where	cd_tipo_logradouro = cd_tipo_logradouro_p
and	rownum = 1;
exception
when others then
	ds_retorno_w := '';
end;

return	ds_retorno_w;

end sus_obter_desc_tipo_logr;
/
