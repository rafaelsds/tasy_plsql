create or replace
function Obter_se_vis_evol_nao_lib(cd_perfil_p		number,
				   cd_tipo_evolucao_p 	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);			
qt_Registros_w	number(3);
			
begin

select  count(1)
into	qt_registros_w
from	TIPO_EVOLUCAO_REGRA_LIB
where	cd_perfil 	 = cd_perfil_p
and	cd_tipo_evolucao = cd_tipo_evolucao_p;

If  	(qt_registros_w > 0) Then
	 ds_retorno_w := 'S';
Else	 ds_retorno_w := 'N';
End If;

return	ds_retorno_w;

end Obter_se_vis_evol_nao_lib;
/