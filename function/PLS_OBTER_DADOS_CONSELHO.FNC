create or replace
function pls_obter_dados_conselho(nr_seq_conselho_p		conselho_profissional.nr_sequencia%type,
								  ie_opcao_p			varchar2)
 		    	return varchar2 is
				
retorno_w		varchar2(255);
ds_conselho_w		conselho_profissional.ds_conselho%type;
sg_conselho_w		conselho_profissional.sg_conselho%type;

/*IE_OPCAO_P
	DS - Descri��o do conselho
	SG - Sigla do conselho
*/

begin

retorno_w := '';

if (nr_seq_conselho_p is not null) then
	
	select	ds_conselho,
		sg_conselho
	into	ds_conselho_w,
		sg_conselho_w
	from	conselho_profissional
	where	nr_sequencia = nr_seq_conselho_p;
	
	if 	(ie_opcao_p = 'DS') then
		retorno_w := ds_conselho_w;
	elsif 	(ie_opcao_p = 'SG') then
		retorno_w := sg_conselho_w;
	end if;	
	
end if;


return	retorno_w;

end pls_obter_dados_conselho;
/
