create or replace function get_blood_product_status(nr_seq_cpoe_p in number
	) return varchar2 is 

ds_status_return_w varchar2(1) := 'X';

begin
	if(nr_seq_cpoe_p is not null) then

		select	max(sr.ie_status)
		into	ds_status_return_w
		from	san_reserva sr,
			san_reserva_item sri,
			cpoe_hemoterapia cph
		where	cph.nr_sequencia = nr_seq_cpoe_p
		and	sr.nr_sequencia = sri.nr_seq_reserva
		and	sr.nr_atendimento = cph.nr_atendimento
		and	sri.nr_seq_derivado = cph.nr_seq_derivado
		and	sr.dt_liberacao_solicitacao is not null;

		if(ds_status_return_w <> 'L') then
			ds_status_return_w := 'X';
		end if;

	end if;
	
return ds_status_return_w;

end get_blood_product_status;
/
