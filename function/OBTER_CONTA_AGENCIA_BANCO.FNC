create or replace
function OBTER_CONTA_AGENCIA_BANCO(nr_sequencia_p	number) return varchar2 is

ds_conta_w	varchar2(254);

begin
if ( nvl(nr_sequencia_p,0) <> 0 ) then
	select obter_conta_banco(nr_seq_conta_banco)
       	into ds_conta_w
        	from banco_escritural
        	where nr_sequencia = nr_sequencia_p;
end if;

return	ds_conta_w;

end;
/