  create or replace 
function cpoe_valid_lib_novo(
	 dt_fim_p  date,
	 dt_inicio_p	date,
	 dt_referencia_p date,
   	 cd_pessoa_fisica_param_p varchar,
   	 nr_atendimento_param_p number,
   	 cd_pessoa_fisica_tabela_p varchar,
   	 nr_atendimento_tabela_p number,
   	 dt_liberacao_p date,
   	 dt_suspensao_p date,
   	 nm_user_p  varchar,
   	 nm_user_tabela_p varchar,
  	 nr_seq_cpoe_anterior_p number,
                 ie_retrogrado_p varchar
 )
        	return char  is 
   	begin

  if(	((nr_atendimento_tabela_p = nr_atendimento_param_p) OR (cd_pessoa_fisica_tabela_p = cd_pessoa_fisica_param_p 	AND	nr_atendimento_param_p is null)) 
	AND	 ((cpoe_reg_valido_ativacao( dt_fim_p,dt_inicio_p, dt_referencia_p) = 'S')    OR (ie_retrogrado_p = 'S')) 
	AND	 ((dt_liberacao_p is null) 	AND	(nm_user_tabela_p = nm_user_p))
	AND	 (dt_suspensao_p is null )
	AND	( nr_seq_cpoe_anterior_p is null)
  	) then
  	begin
   	    return 'S';
 	 end;
  	end if;
  
  return 'N';
  

end cpoe_valid_lib_novo;
/
  