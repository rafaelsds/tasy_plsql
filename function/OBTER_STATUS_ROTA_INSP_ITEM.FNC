create or replace
function obter_status_rota_insp_item(
			nr_sequencia_p	number)
			return varchar2 is
ie_status_w	varchar2(1);
begin
select	max(ie_status)
into	ie_status_w 
from	man_rota_inspecao_item 
where	nr_sequencia 	= nr_sequencia_p;

return	ie_status_w;

end obter_status_rota_insp_item;
/ 