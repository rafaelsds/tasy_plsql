CREATE OR REPLACE FUNCTION GET_CODE_NURSING_CARE_EXPORT (
    NR_SEQ_VERSION_P NURSING_CARE_VERSION.NR_SEQUENCIA%TYPE,
    NR_SEQ_ITEM_CLASS_P NURSING_CARE_ITEM.NR_SEQ_CLASSIFICACAO%TYPE,
    DT_AVALIACAO_P nursing_care_encounter.dt_avaliacao%TYPE
) RETURN VARCHAR2 AS
    w_RETORNO varchar2(10):= '';
    w_VERSION varchar2(1) := '';
    w_IE_TIPO_FORMULARIO varchar2(1) := '';
BEGIN
    SELECT 
        IE_TIPO_FORMULARIO, 
        CASE WHEN TO_DATE(DT_AVALIACAO_P,'DD/MM/YYYY') between DT_INICIO_VIGENCIA AND DT_FIM_VIGENCIA  
            THEN 'A' ELSE 'U' END
    INTO 
        w_IE_TIPO_FORMULARIO,
        w_VERSION
    FROM NURSING_CARE_VERSION
    WHERE 
        NR_SEQUENCIA = NR_SEQ_VERSION_P;

    -- condicionais
    if (w_VERSION = 'A') THEN
        if (NR_SEQ_ITEM_CLASS_P = 'A') THEN
            if (w_IE_TIPO_FORMULARIO = 'G' ) THEN 
                w_RETORNO := 'ASS0010';
            ELSIF (w_IE_TIPO_FORMULARIO = 'I' ) THEN 
                w_RETORNO := 'ASS0040';
            ELSIF (w_IE_TIPO_FORMULARIO = 'H' ) THEN 
                w_RETORNO := 'ASS0060';
            END IF;
        ELSIF (NR_SEQ_ITEM_CLASS_P = 'B' ) THEN
            if (w_IE_TIPO_FORMULARIO = 'G' ) THEN 
                w_RETORNO := 'ASS0020';
            ELSIF (w_IE_TIPO_FORMULARIO = 'I' ) THEN 
                w_RETORNO := 'ASS0050';
            ELSIF (w_IE_TIPO_FORMULARIO = 'H' ) THEN 
                w_RETORNO := 'ASS0070';
            END IF;            
        ELSIF (NR_SEQ_ITEM_CLASS_P = 'C' AND w_IE_TIPO_FORMULARIO = 'G' ) THEN
            w_RETORNO := 'ASS0030';         
        END IF;
    ELSE
        if (NR_SEQ_ITEM_CLASS_P = 'A') THEN
            if (w_IE_TIPO_FORMULARIO = 'G' ) THEN 
                w_RETORNO := 'ASS0011';
            ELSIF (w_IE_TIPO_FORMULARIO = 'I' ) THEN 
                w_RETORNO := 'ASS0041';
            ELSIF (w_IE_TIPO_FORMULARIO = 'H' ) THEN 
                w_RETORNO := 'ASS0061';
            END IF;
        ELSIF (NR_SEQ_ITEM_CLASS_P = 'B' ) THEN
            if (w_IE_TIPO_FORMULARIO = 'G' ) THEN 
                w_RETORNO := 'ASS0021';
            ELSIF (w_IE_TIPO_FORMULARIO = 'I' ) THEN 
                w_RETORNO := 'ASS0051';
            ELSIF (w_IE_TIPO_FORMULARIO = 'H' ) THEN 
                w_RETORNO := 'ASS0071';
            END IF;            
        ELSIF (NR_SEQ_ITEM_CLASS_P = 'C' AND w_IE_TIPO_FORMULARIO = 'G' ) THEN
            w_RETORNO := 'ASS0031';         
        END IF;
    END IF;
    RETURN w_RETORNO;
END;
/
