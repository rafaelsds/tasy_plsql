create or replace
function obter_se_laudo_pendente(	nr_prescricao_p	number,
					nr_seq_prescr_p	number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1) := 'S';
			
begin

if 	(nr_prescricao_p is not null) and
	(nr_seq_prescr_p is not null) then
	begin
	
	select 	decode(count(*),0,'N','S')
	into	ie_retorno_w
	from 	prescr_procedimento d,
		procedimento_paciente c,
		laudo_paciente a
	where 	d.nr_prescricao = nr_prescricao_p
	and	d.nr_sequencia	= nr_seq_prescr_p
	and   	c.nr_sequencia_prescricao = d.nr_sequencia
	and   	c.nr_prescricao = d.nr_prescricao
	and   	a.nr_seq_proc   = c.nr_sequencia
	and   	a.dt_liberacao is null;
	
	
	if 	(ie_retorno_w = 'N') then
		
		select 	decode(count(*),0,'S','N')
		into	ie_retorno_w
		from 	prescr_procedimento d,
			procedimento_paciente c,
			laudo_paciente a
		where 	d.nr_prescricao = nr_prescricao_p
		and	d.nr_sequencia	= nr_seq_prescr_p
		and   	c.nr_sequencia_prescricao = d.nr_sequencia
		and   	c.nr_prescricao = d.nr_prescricao
		and   	a.nr_seq_proc   = c.nr_sequencia;
			
	end if;

	end;
end if;

return	ie_retorno_w;

end obter_se_laudo_pendente;
/