create or replace
function obter_excluir_etapa_conta(	dt_etapa_p	date,
				nr_tempo_p	number) 
				return varchar2 is

ie_retorno_w	varchar2(1) := 'S';	

begin

select	nvl(max('S'),'N')
into	ie_retorno_w
from	dual
where	(dt_etapa_p + nr_tempo_p/60/24) <= to_date(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

return	ie_retorno_w;

end	obter_excluir_etapa_conta;
/