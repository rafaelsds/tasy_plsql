CREATE OR REPLACE FUNCTION rxt_tratamento_pend_bloco(
    nr_seq_tratamento_p rxt_tratamento.nr_sequencia%TYPE
) RETURN VARCHAR2 IS

nr_count_w NUMBER(3);

BEGIN

    SELECT COUNT(tratamento.nr_sequencia)
    INTO nr_count_w
    FROM 
        rxt_tratamento tratamento
    WHERE tratamento.nr_sequencia = nr_seq_tratamento_p
    AND tratamento.ie_necessita_bloco = 'S'
    AND (tratamento.ie_bloco_concluido = 'N' OR tratamento.ie_bloco_concluido IS NULL)
    AND rxt_tratamento_ag_autorizacao(tratamento.nr_sequencia) <> 'S'
    AND tratamento.dt_liberacao IS NULL
    AND tratamento.dt_suspensao IS NULL
    AND tratamento.dt_cancelamento IS NULL
    AND rxt_tratamento_finalizado(tratamento.nr_sequencia) <> 'S';

    IF (nr_count_w > 0) THEN
        RETURN 'S';
    ELSE 
        RETURN 'N';
    END IF;

END rxt_tratamento_pend_bloco;
/