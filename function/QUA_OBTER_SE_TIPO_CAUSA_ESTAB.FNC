create or replace
function qua_obter_se_tipo_causa_estab(
			nr_sequencia_p		number,
			cd_estabelecimento_p	number)
			return varchar2 is

ie_retorno_w		varchar2(1) := 'S';
qt_reg_w			number(10,0);

begin

select	count(*)
into	qt_reg_w
from	qua_tipo_causa_estab
where	nr_seq_tipo_causa = nr_sequencia_p;

if	(qt_reg_w > 0) then
	begin
	select	'S'
	into	ie_retorno_w
	from	qua_tipo_causa_estab
	where	nr_seq_tipo_causa = nr_sequencia_p
	and	cd_estabelecimento = cd_estabelecimento_p
	and	rownum < 2;
	exception
	when others then
		ie_retorno_w := 'N';
	end;
end if;

return	ie_retorno_w;

end qua_obter_se_tipo_causa_estab;
/