create or replace
function Obter_dados_hemodialise_spktv	(cd_pessoa_fisica_p	Varchar2,
					seq_grid_p		Number,
					nr_seq_exame_p		Number,
					ie_opcao_p		Varchar2)
					return varchar2 is

/*
TH - Tempo Hemodi�lise
FS - Fluxo sangue
FD - Fluxo dialisato
M - Modelo dialisador
A - Tipo acesso para hemodialise
PR - PA's pr� sentado
PO - PA's p�s sentado
GP - Ganho de peso interdial�tico GPID
T - Temperatura
FC - Frequencia card�aca
I - Idade
*/
					
nr_sequencia_w		Number(10);
dt_referencia_w		Date;
dt_retirada_w		Date;
dt_inicio_dialise_w	Date;
ds_modelo_w		Varchar2(100);
ds_acesso_w		Varchar2(100);
qt_tempo_dialise_w	Varchar2(10);
pas_pre_sentado_w	Varchar2(10);
pas_pos_sentado_w	Varchar2(10);
qt_gpid_w		Number(15,7);
nr_seq_controle_w	Number(10);
qt_fluxo_sangue_w	Number(15,4);
qt_fluxo_dialisado_w	Number(15,4);
qt_temp_axiliar_w	Number(15,4);
qt_pulso_w		Number(15,4);
ds_retorno_w		Varchar2(100);
			
begin

select 	dt_referencia
into	dt_referencia_w
from	hd_exame_grid_controle
where 	nr_sequencia = seq_grid_p;

select	max(e.nr_sequencia)
into	nr_sequencia_w
from	hd_dialise e
where	e.cd_pessoa_fisica = cd_pessoa_fisica_p
and exists (	select	1
		from	exame_laboratorio d,
			exame_lab_resultado a,
			exame_lab_result_item c
		where	a.nr_seq_resultado 	= c.nr_seq_resultado
		and	d.nr_seq_exame 		= c.nr_seq_exame
		and	trunc(nvl(c.dt_digitacao,a.dt_resultado),'month') = trunc(dt_referencia_w,'month')
		and	a.cd_pessoa_fisica	= e.cd_pessoa_fisica
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	c.nr_seq_exame		= nr_seq_exame_p
		and	(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado)) is not null
		and	e.dt_dialise between trunc(c.dt_coleta) and fim_dia(c.dt_coleta))
or exists (	select	1
		from	exame_laboratorio d,
			exame_lab_resultado a,
			exame_lab_result_item c,
			prescr_medica b
		where	a.nr_seq_resultado 	= c.nr_seq_resultado
		and	d.nr_seq_exame 		= c.nr_seq_exame
		and 	(Obter_data_aprov_lab(a.nr_prescricao, c.nr_seq_prescr) is not null)
		and	trunc(nvl(c.dt_digitacao,a.dt_resultado),'month') = trunc(dt_referencia_w,'month')
		and	b.cd_pessoa_fisica	= e.cd_pessoa_fisica
		and	b.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	c.nr_seq_exame		= nr_seq_exame_p
		and	b.nr_prescricao		= a.nr_prescricao
		and	(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado)) is not null
		and	e.dt_dialise between trunc(c.dt_coleta) and fim_dia(c.dt_coleta));

if (nr_sequencia_w is null) then
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	hd_dialise
	where	dt_dialise between dt_referencia_w and last_day(dt_referencia_w);
end if;

if (nr_sequencia_w is not null) then
	select	max(dt_retirada),
		max(substr(hd_obter_modelo_dialisador(nr_seq_dialisador),1,80)) ds_modelo
	into	dt_retirada_w,
		ds_modelo_w
	from	hd_dialise_dialisador
	where	nr_seq_dialise = nr_sequencia_w;

	select	dt_inicio_dialise,
		(dt_retirada_w - dt_inicio_dialise)*1440 qt_tempo_dialise,
		qt_pa_sist_pre_deitado||'/'||qt_pa_diast_pre_deitado pas_pre_sentado,
		qt_pa_sist_pos_deitado||'/'||qt_pa_diast_pos_deitado pas_pos_sentado,
		hd_obter_gpid(nr_sequencia_w,cd_pessoa_fisica_p, nvl(qt_peso_pre,0)) qt_gpid
	into	dt_inicio_dialise_w,
		qt_tempo_dialise_w,
		pas_pre_sentado_w,
		pas_pos_sentado_w,
		qt_gpid_w
	from	hd_dialise
	where	nr_sequencia = nr_sequencia_w;

	select 	max(substr(hd_obter_desc_acesso(nr_seq_acesso),1,100)) ds_acesso
	into	ds_acesso_w
	from	hd_dialise_acesso
	where	nr_seq_dialise = nr_sequencia_w;

	select	min(nr_sequencia)
	into	nr_seq_controle_w
	from	hd_controle
	where	nr_seq_dialise = nr_sequencia_w
	and	nr_sequencia <> (select	min(nr_sequencia)
				 from	hd_controle
				 where	nr_seq_dialise = nr_sequencia_w);
				 
	select	qt_fluxo_sangue,
		qt_fluxo_dialisado,
		qt_temperatura_axiliar,
		qt_pulso
	into	qt_fluxo_sangue_w,
		qt_fluxo_dialisado_w,
		qt_temp_axiliar_w,
		qt_pulso_w
	from	hd_controle
	where	nr_sequencia = nr_seq_controle_w;
	
	if (ie_opcao_p = 'TH') then
		ds_retorno_w := qt_tempo_dialise_w;
	elsif (ie_opcao_p = 'FS') then
		ds_retorno_w := qt_fluxo_sangue_w;
	elsif (ie_opcao_p = 'FD') then
		ds_retorno_w := qt_fluxo_dialisado_w;
	elsif (ie_opcao_p = 'M') then
		ds_retorno_w := ds_modelo_w;
	elsif (ie_opcao_p = 'A') then
		ds_retorno_w := ds_acesso_w;
	elsif (ie_opcao_p = 'PR') then
		ds_retorno_w := pas_pre_sentado_w;
	elsif (ie_opcao_p = 'PO') then
		ds_retorno_w := pas_pos_sentado_w;
	elsif (ie_opcao_p = 'GP') then
		ds_retorno_w := qt_gpid_w;
	elsif (ie_opcao_p = 'T') then
		ds_retorno_w := qt_temp_axiliar_w;
	elsif (ie_opcao_p = 'FC') then
		ds_retorno_w := qt_pulso_w;
	elsif (ie_opcao_p = 'I') then
		ds_retorno_w := substr(Obter_Idade_PF(cd_pessoa_fisica_p, dt_inicio_dialise_w, 'A'),1,100);
	end if;
end if;
		
return	ds_retorno_w;

end Obter_dados_hemodialise_spktv;
/
