create or replace
function obter_dados_dcb(	nr_sequencia_p	number,
				ie_opcao_p	varchar2)
				return varchar2 is

ds_retorno_w	varchar2(255);
cd_dcb_w	varchar2(20);
				
begin

if	(nr_sequencia_p > 0) then
	select	cd_dcb
	into	cd_dcb_w
	from	dcb_medic_controlado
	where	nr_sequencia	= nr_sequencia_p;
end if;

if	(ie_opcao_p = 'C') then
	ds_retorno_w := cd_dcb_w;
end if;

return	ds_retorno_w;

end obter_dados_dcb;
/