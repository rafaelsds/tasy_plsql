create or replace 
function lab_obter_se_exame_prescr	(nr_prescricao_p	number,
				nr_seq_exame_p		number) 
				return varchar2 is

ie_retorno_w	varchar2(1);

begin


select	decode(count(*),0,'N','S')
into	ie_retorno_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and	nr_seq_exame = nr_seq_exame_p;

return ie_retorno_w;

end;
/