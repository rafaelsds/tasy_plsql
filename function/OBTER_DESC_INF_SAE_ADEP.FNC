create or replace
function obter_desc_inf_sae_adep (
		ie_sn_p		varchar2,
		ds_intervalo_p	varchar2,
		nr_seq_topografia_p	number default 0,
		ie_lado_p	varchar2 default null,
		nr_seq_modelo_p	number default 0)
		return varchar2 is
		
ds_informacao_w	varchar2(4000);
ie_exibir_sae_modelo_w	varchar2(1);

begin
ds_informacao_w	:= ds_intervalo_p;
if	(ie_sn_p = 'S') then
	begin
	ds_informacao_w	:= obter_desc_expressao(309405) || ' ' || ds_informacao_w;
	end;
end if;

if (nvl(nr_seq_topografia_p, 0) > 0) then
	ds_informacao_w	:= ds_informacao_w || ' - ' || obter_desc_expressao(343115) ||substr(Obter_desc_topografia_dor(nr_seq_topografia_p),1,150);
end if;

if (ie_lado_p is not null) then
	ds_informacao_w	:= ds_informacao_w || obter_desc_expressao(347323) || ' ' ||substr(obter_valor_dominio(1372,ie_lado_p),1,150);
end if;

ie_exibir_sae_modelo_w := wheb_assist_pck.obterParametroFuncao(1113,703, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
if (nvl(nr_seq_modelo_p, 0) > 0) and (ie_exibir_sae_modelo_w = 'S') then
	ds_informacao_w	:= obter_desc_expressao(330326) || ' ' ||obter_desc_mod_sae(nr_seq_modelo_p) || ' - ' || ds_informacao_w;
end if;

return ds_informacao_w;
end obter_desc_inf_sae_adep;
/