create or replace 
function cme_obter_desc_item_requisicao(nr_seq_conjunto_p	number,
										nr_seq_classif_p	number,
										nr_seq_classif_conj_p	number) 
			return varchar2 is

begin

if	(nr_seq_conjunto_p is not null) then
	begin
	return cme_obter_nome_conjunto(nr_seq_conjunto_p);
	end;
elsif (nr_seq_classif_p is not null) then
	begin
	return cme_obter_desc_classif_item(nr_seq_classif_p);
	end;
elsif (nr_seq_classif_conj_p is not null) then
	begin
	return cme_obter_desc_classif_conj(nr_seq_classif_conj_p);
	end;
end if;

return '';

end cme_obter_desc_item_requisicao;
/