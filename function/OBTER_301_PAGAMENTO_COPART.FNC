create or replace function obter_301_pagamento_copart(cd_pessoa_fisica_p in pessoa_fisica_taxa.cd_pessoa_fisica%type,

                                                      dt_pagamento_p     in pessoa_fisica_taxa.dt_pagamento%type,

                                                      vl_coparticipacao  in number,

                                                      ie_status_acerto_p in conta_paciente.ie_status_acerto%type,

                                                      qt_max_taxas_p     in regra_lanc_automatico.qt_max_taxas%type) return number is

  

  ie_1_nen_obr_copagamento       constant number := 1; --nenhuma obriga��o de co-pagamento

  ie_2_pag_adic_cob_pag          constant number := 2; --pagamento adicional cobrado e pago integralmente pelo segurado

  ie_4_nen_obr_pag_bas_cert_inv  constant number := 4; --nenhuma obriga��o de pagar com base em certificado inv�lido

  ie_5_nen_obr_pag_bas_reci_inv  constant number := 5; --nenhuma obriga��o de pagar com base em um recibo inv�lido

  ie_6_men_pgto_adi_rec_val_cob  constant number := 6; --menor pagamento adicional devido ao recebimento v�lido de acordo com o � 61 sgb v cobrado e pago integralmente pelo segurado

  ie_7_men_pgto_adi_rec_val_parc constant number := 7; --menor pagamento adicional com base em recibo v�lido de acordo com o � 61 sgb v glasschnet e n�o ou parcialmente pago pelo segurado

  ie_8_men_pgto_adi_nao_par_pago constant number := 8; --pagamento adicional e n�o ou parcialmente pago pelo segurado

  

  ie_status_acerto_pago          constant number := 2;

  

  cursor c01 is

    select qt_dias_pagamento,

           nr_seq_justificativa

    from pessoa_fisica_taxa pft

    where pft.cd_pessoa_fisica            = cd_pessoa_fisica_p

      and trunc(pft.dt_pagamento,'yyyy')  = trunc(dt_pagamento_p,'yyyy')

     order by pft.dt_pagamento desc;



   c01_w c01%rowtype;  

 

begin



  if vl_coparticipacao = 0 then

    return ie_1_nen_obr_copagamento;

  else

  

    open c01;

    fetch c01 into c01_w;

    close c01;

  

    if c01_w.nr_seq_justificativa is not null then

      return ie_4_nen_obr_pag_bas_cert_inv;

    elsif c01_w.qt_dias_pagamento = 0 and ie_status_acerto_p = ie_status_acerto_pago then

      return ie_2_pag_adic_cob_pag;

    elsif c01_w.qt_dias_pagamento = 0 and ie_status_acerto_p != ie_status_acerto_pago then      

      return ie_8_men_pgto_adi_nao_par_pago;

    elsif c01_w.qt_dias_pagamento = qt_max_taxas_p then

      return ie_5_nen_obr_pag_bas_reci_inv;

    elsif c01_w.qt_dias_pagamento < qt_max_taxas_p and ie_status_acerto_p != ie_status_acerto_pago then

      return ie_7_men_pgto_adi_rec_val_parc;

    elsif c01_w.qt_dias_pagamento < qt_max_taxas_p and ie_status_acerto_p = ie_status_acerto_pago then 

      return ie_6_men_pgto_adi_rec_val_cob;

    end if;

    

  end if;

  

  return 0; --Otherwise



end obter_301_pagamento_copart;
/