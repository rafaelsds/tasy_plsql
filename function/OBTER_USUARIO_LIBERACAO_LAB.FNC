create or replace
function obter_usuario_liberacao_lab(	nr_prescricao_p number,
					nr_seq_prescr_p	number)
    					return Varchar2 is
nm_usuario_liberacao_w	Varchar2(200);

begin

select	max(b.nm_usuario_liberacao)
into	nm_usuario_liberacao_w
from	exame_lab_result_item b,
	exame_lab_resultado a
where	a.nr_seq_resultado	= b.nr_seq_resultado
and	a.nr_prescricao		= nr_prescricao_p
and	b.nr_seq_prescr		= nr_seq_prescr_p;

return nm_usuario_liberacao_w;

end obter_usuario_liberacao_lab;
/