create or replace function rxt_obter_se_fisico_lib(nr_seq_fase_trat_p number,
														ie_tipo_trat_p varchar2) return VARCHAR2 is
				
qt_registros_w number(10,0);
qt_reg_lib_w number(10,0);
ie_lib_w  varchar2(1) := 'N';

/*retorna se todos os registros est�o liberados ou n�o*/
				
begin

if (ie_tipo_trat_p = 'T') then
	select count(*)
	into qt_reg_lib_w
	from RXT_CAMPO a
	where nr_seq_fase = nr_seq_fase_trat_p
	and  ie_situacao = 'A'
	and  dt_lib_fisico is not null
	and  dt_liberacao is not null;
	
	select count(*)
	into qt_registros_w
	from RXT_CAMPO a
	where nr_seq_fase = nr_seq_fase_trat_p
	and  ie_situacao = 'A';

elsif (ie_tipo_trat_p = 'B') then
	select count(*)
	into qt_reg_lib_w
	from rxt_braq_campo_aplic_trat a
	where nr_seq_aplic_trat = nr_seq_fase_trat_p
	and ie_situacao = 'A'
	and dt_lib_fisico is not null
	and  dt_liberacao is not null;
	
	select count(*)
	into qt_registros_w
	from rxt_braq_campo_aplic_trat a
	where nr_seq_aplic_trat = nr_seq_fase_trat_p
	and ie_situacao = 'A';

end if;

	if (qt_registros_w = qt_reg_lib_w) then
		if (qt_registros_w = 0) then
			ie_lib_w := 'X';
		else
			ie_lib_w := 'S';
		end if;	
	else
		ie_lib_w := 'N';
	end if;


return ie_lib_w;

end rxt_obter_se_fisico_lib;
/
