create or replace 
function hd_obter_dados_modelo_maq (nr_seq_modelo_p	number,
				 ie_opcao_p		varchar2)
				return varchar2 is

ds_retorno_w	varchar2(255);
ds_modelo_w	varchar2(255);
nr_seq_marca_w	number(10,0);
ds_marca_w	varchar2(255);
ie_clearence_w	varchar2(01);
ie_ktv_w	varchar2(01);

/*
D - Descri��o do modelo
CM - C�digo da marca
DM - Descri��o da marca
CL - Clearence
K - KTV
*/


begin

if (nvl(nr_seq_modelo_p,0) > 0) then
	begin
	select	ds_modelo,
		nr_seq_marca,
		substr(hd_desc_marca_maquina(nr_seq_marca,'D'),1,255) ds_marca,
		ie_informar_clearence,
		ie_informar_ktv
	into	ds_modelo_w,
		nr_seq_marca_w,
		ds_marca_w,
		ie_clearence_w,
		ie_ktv_w
	from	hd_modelo_maquina
	where	nr_sequencia = nr_seq_modelo_p;

	if (ie_opcao_p = 'D') then
		ds_retorno_w	:= ds_modelo_w;
	elsif (ie_opcao_p = 'CM') then
		ds_retorno_w	:= nr_seq_marca_w;
	elsif (ie_opcao_p = 'DM') then
		ds_retorno_w	:= ds_marca_w;
	elsif (ie_opcao_p = 'CL') then
		ds_retorno_w	:= ie_clearence_w;
	elsif (ie_opcao_p = 'K') then
		ds_retorno_w	:= ie_ktv_w;
	end if;
	end;	
end if;

return ds_retorno_w;

end hd_obter_dados_modelo_maq;
/
