create or replace
function diops_obter_valor_balancete
			(	nr_seq_periodo_p	number,
				ds_conta_p		varchar2,
				ie_tipo_balancete_p	varchar2)
 		    	return number is

vl_retorno_w	number(15,2);

begin

if	(ie_tipo_balancete_p = 'A') then
	select	max(vl_saldo_final)
	into	vl_retorno_w
	from	diops_fin_mov_ativo
	where	nr_seq_periodo	= nr_seq_periodo_p
	and	ds_conta = ds_conta_p;
elsif	(ie_tipo_balancete_p = 'P') then
	select	max(vl_saldo_final)
	into	vl_retorno_w
	from	diops_fin_mov_passivo
	where	nr_seq_periodo	= nr_seq_periodo_p
	and	ds_conta = ds_conta_p;
elsif	(ie_tipo_balancete_p = 'R') then
	select	max(vl_saldo_final)
	into	vl_retorno_w
	from	diops_fin_mov_receita
	where	nr_seq_periodo	= nr_seq_periodo_p
	and	ds_conta = ds_conta_p;
elsif	(ie_tipo_balancete_p = 'D') then
	select	max(vl_saldo_final)
	into	vl_retorno_w
	from	diops_fin_mov_despesa
	where	nr_seq_periodo	= nr_seq_periodo_p
	and	ds_conta = ds_conta_p;
end if;

return	nvl(vl_retorno_w,0);

end diops_obter_valor_balancete;
/