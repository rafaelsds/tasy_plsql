create or replace
function pcs_obter_vl_confiabilidade_t(pr_confianca_p		varchar2)
 		    	return varchar2 is

ds_retorno_w varchar2(5);			
			
begin


if (pr_confianca_p = '99') then
   ds_retorno_w := '0,01';
elsif (pr_confianca_p = '98') then
   ds_retorno_w := '0,01';
elsif (pr_confianca_p = '97') then
   ds_retorno_w := '0,02';
elsif (pr_confianca_p = '96') then
   ds_retorno_w := '0,02';
elsif (pr_confianca_p = '95') then
   ds_retorno_w := '0,03';
elsif (pr_confianca_p = '94') then
   ds_retorno_w := '0,03';
elsif (pr_confianca_p = '93') then
   ds_retorno_w := '0,04';
elsif (pr_confianca_p = '92') then
   ds_retorno_w := '0,04';
elsif (pr_confianca_p = '91') then
   ds_retorno_w := '0,05';
elsif (pr_confianca_p = '90') then
   ds_retorno_w := '0,05';
elsif (pr_confianca_p = '89') then
   ds_retorno_w := '0,06';
elsif (pr_confianca_p = '88') then
   ds_retorno_w := '0,06';
elsif (pr_confianca_p = '87') then
   ds_retorno_w := '0,07';
elsif (pr_confianca_p = '86') then
   ds_retorno_w := '0,07';
elsif (pr_confianca_p = '85') then
   ds_retorno_w := '0,08';
elsif (pr_confianca_p = '84') then
   ds_retorno_w := '0,08';
elsif (pr_confianca_p = '83') then
   ds_retorno_w := '0,09';  
elsif (pr_confianca_p = '82') then
   ds_retorno_w := '0,09';
elsif (pr_confianca_p = '81') then
   ds_retorno_w := '0,1';  
elsif (pr_confianca_p = '80') then
   ds_retorno_w := '0,1';    
end if;   
   
return	ds_retorno_w;

end pcs_obter_vl_confiabilidade_t;
/