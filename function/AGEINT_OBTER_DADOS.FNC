create or replace
function ageint_obter_dados( nr_seq_ageint_p	number,
				  ie_retorno_p	varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
			
begin

ds_retorno_w := '';

if (nr_seq_ageint_p is not null) and (nr_seq_ageint_p <> 0) then
	select 	max(a.nm_paciente)
	into	ds_retorno_w
	from	agenda_integrada a
	where	a.nr_sequencia = nr_seq_ageint_p;
end if;

return	ds_retorno_w;

end ageint_obter_dados;
/