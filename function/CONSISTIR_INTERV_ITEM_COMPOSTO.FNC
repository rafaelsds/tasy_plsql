create or replace
function consistir_interv_item_composto	(nr_prescricao_p	number,
				nr_seq_material_p	number,
				nr_agrupamento_p	number,
				cd_intervalo_p	varchar2)
				return varchar2 is
					
ie_intervalo_valido_w	varchar2(1);
					
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_material_p is not null) and
	(cd_intervalo_p is not null) then
	
	select	decode(count(*),0,'S','N')
	into	ie_intervalo_valido_w
	from	prescr_material
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	<> nr_seq_material_p
	and	nr_agrupamento	= nr_agrupamento_p	
	and	cd_intervalo	<> cd_intervalo_p
	and	ie_agrupador	= 1;
	
end if;

return ie_intervalo_valido_w;

end consistir_interv_item_composto;
/