create or replace
function ehr_obter_valor_elemento(	nr_seq_temp_conteudo_p	number,
					nr_seq_reg_template_p	number)
	return varchar2 is

ds_retorno_w	varchar2(4000);

begin
	
select	substr(nvl(nvl(ds_resultado,vl_resultado),dt_resultado),1,4000)
into	ds_retorno_w
from	ehr_reg_elemento
where	nr_seq_temp_conteudo	= nr_seq_temp_conteudo_p
and	nr_seq_reg_template	= nr_seq_reg_template_p;

return	ds_retorno_w;

end;
/
