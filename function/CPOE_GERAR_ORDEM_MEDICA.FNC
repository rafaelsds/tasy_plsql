create or replace 
procedure cpoe_gerar_ordem_medica (	nr_atendimento_p number,
									nr_seq_cpoe_p	number,
									nr_seq_tipo_ordem_p	number,
									ds_ordem_p		varchar2,
									dt_ordem_p		date,
									cd_medico_p		varchar2,
									nm_usuario_p		varchar2,
									nr_sequencia_p	out	number) is 

nr_prescricao_w       	prescr_medica.nr_prescricao%type:= null;
cd_material_w			cpoe_material.cd_material%type;
ie_controle_tempo_w		cpoe_material.ie_controle_tempo%type;
ie_material_w			cpoe_material.ie_material%type;
nr_seq_material_w     	prescr_material.nr_sequencia%type:= null;
nr_seq_solucao_w      	prescr_solucao.nr_seq_solucao%type:= null;

begin

nr_sequencia_p := 0;

select	max(cd_material) cd_material,
		nvl(max(ie_controle_tempo),'N'),
		nvl(max(ie_material),'N')
into	cd_material_w,
		ie_controle_tempo_w, 
		ie_material_w
from	cpoe_material
where	nr_sequencia = nr_seq_cpoe_p
and	((decode(dt_lib_suspensao, null, nvl(dt_fim_cih,dt_fim), nvl(dt_suspensao,dt_fim)) >= sysdate) or 
	(decode(dt_lib_suspensao, null, nvl(dt_fim_cih,dt_fim), nvl(dt_suspensao,dt_fim)) is null));


if  (ie_material_w = 'N') and
	(ie_controle_tempo_w = 'S') then	

	select max(a.nr_prescricao)
	into  nr_prescricao_w
	from prescr_solucao a,
		prescr_material b
	where a.nr_prescricao = b.nr_prescricao
	and a.nr_seq_solucao = b.nr_sequencia_solucao
	and b.nr_seq_mat_cpoe = nr_seq_cpoe_p;
	
	select min(a.nr_seq_solucao)
	into nr_seq_solucao_w
	from prescr_solucao a,
		prescr_material b
	where a.nr_prescricao = b.nr_prescricao
	and a.nr_seq_solucao = b.nr_sequencia_solucao
	and b.nr_prescricao = nr_prescricao_w
	and b.nr_seq_mat_cpoe = nr_seq_cpoe_p;    
else    

	select max(nr_prescricao)
	into nr_prescricao_w
	from prescr_material
	where nr_seq_mat_cpoe = nr_seq_cpoe_p; 
 
	select min(nr_sequencia)
	into nr_seq_material_w
	from prescr_material
	where nr_prescricao = nr_prescricao_w
	and nr_seq_mat_cpoe = nr_seq_cpoe_p;    
end if;

Gerar_ordem_medica(	nr_atendimento_p, 
				  nr_prescricao_w, 
				  null, 
				  null, 
				  nr_seq_tipo_ordem_p, 
				  ds_ordem_p, 
				  nr_seq_solucao_w, 
				  nr_seq_material_w, 
				  cd_medico_p, 
				  dt_ordem_p,
				  null, 
				  cd_material_w, 
				  nm_usuario_p, 
				  nr_sequencia_p);
  

end CPOE_gerar_ordem_medica;
/
