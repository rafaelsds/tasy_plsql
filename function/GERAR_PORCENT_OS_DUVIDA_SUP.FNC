create or replace
function gerar_porcent_os_duvida_sup (	dt_inicial_p		date,
										dt_final_p		date)
										return float is
qt_total_os_duv_sup_n2_w	number;
qt_total_os_sup_n2_w	number;
pr_os_duv_sup_n2_w	number := 0;

begin

SELECT ROUND(NVL(dividir(SUM(qt_total_os_duv_n2)*100,SUM(qt_tot_os_duv)),0),2)
into pr_os_duv_sup_n2_w
FROM (SELECT  DISTINCT 
	(SELECT COUNT(*)
	FROM	man_ordem_servico w
	WHERE	1 = 1
	AND	a.nr_sequencia = w.nr_seq_grupo_sup
	AND EXISTS(	SELECT	1
			FROM	man_estagio_processo y,
				man_ordem_serv_estagio x
			WHERE	y.nr_sequencia = x.nr_seq_estagio
			AND	x.nr_seq_ordem = w.nr_sequencia
			AND	y.ie_suporte = 'S')
	AND	TRUNC(w.dt_fim_real) BETWEEN TRUNC(dt_inicial_p,'dd') AND fim_dia(TRUNC(dt_final_p,'dd')) --tbschulz 15/05 restringir pelo dt_fim_real ao inv�s de dt_ordem_servico
	AND	w.ie_classificacao = 'D'
	and	w.ie_classificacao_cliente = 'D' -- tbschulz 15/05 para computar somente OS origem d�vida, pois o processo de defeitos virando d�vidas ser� computado em um indicador separado futuramente
	) qt_tot_OS_duv, --total de OS de d�vida encerradas no per�odo pelo grupo no n�vel 1
	(SELECT DISTINCT COUNT(k.nr_sequencia)
	FROM    man_ordem_servico k
	WHERE	1=1
	AND 	'S' = (SELECT	DECODE(COUNT(*),0,'N','S')
		FROM		man_ordem_serv_estagio y,
				man_estagio_processo q
		WHERE		y.nr_seq_estagio = q.nr_sequencia
		AND		NVL(ie_sup_nivel2,'N') = 'S'
		AND		y.nr_seq_ordem = k.nr_sequencia
		and		y.nr_seq_estagio <> 1601) --1601 - An�lise PACS passa direto para o N2
	AND	TRUNC(k.dt_fim_real) BETWEEN TRUNC(dt_inicial_p,'dd') AND fim_dia(TRUNC(dt_final_p,'dd')) --tbschulz 15/05 restringir pelo dt_fim_real ao inv�s de dt_ordem_servico
	AND	k.nr_seq_grupo_sup = a.nr_sequencia
	AND	k.ie_classificacao = 'D'
	and	k.ie_classificacao_cliente = 'D'
	) qt_total_os_duv_n2 --quantidade de OS de d�vida encerradas no per�odo que o grupo do n�vel 1 passou para o n�vel 2
FROM  grupo_suporte a
WHERE a.nr_sequencia NOT IN(27,70,60,63) --27 ger assist / 63 N2 / 60 exped / 70 ger back
AND a.nr_seq_gerencia_sup IN (5,58) 
AND a.ie_situacao = 'A');					

return	pr_os_duv_sup_n2_w ;

end gerar_porcent_os_duvida_sup;
/

