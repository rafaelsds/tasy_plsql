create or replace 
function obter_atraso_tempo_cirurgico	(nr_seq_tempo_p	number,
					 nr_cirurgia_p  number,
					 nr_seq_pepo_p	number)
					return	number is
					
qt_tempo_atraso_w	number(10):=null;
begin

if	(nvl(nr_seq_tempo_p,0) > 0) and
	((nvl(nr_cirurgia_p,0) > 0) or
	(nvl(nr_seq_pepo_p,0) > 0)) then
	select	max(qt_tempo_atraso)
	into	qt_tempo_atraso_w
	from	cirurgia_tempo_atraso
	where	nr_seq_tempo = nr_seq_tempo_p
	and	((nr_cirurgia  = nr_cirurgia_p) and (nvl(nr_cirurgia_p,0) > 0) or
		 (nr_seq_pepo  = nr_seq_pepo_p) and (nvl(nr_seq_pepo_p,0) > 0))
	and	nvl(ie_situacao,'A') = 'A';
end if;
	 
return	qt_tempo_atraso_w;	 
	 		 
end	obter_atraso_tempo_cirurgico;
/