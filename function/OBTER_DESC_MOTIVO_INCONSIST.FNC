create or replace
function Obter_Desc_Motivo_Inconsist(nr_seq_motivo_p		number)
 		    	return varchar2 is

ds_inconsistencia_w 	varchar2(255);
begin

select	substr(ds_inconsistencia,1,255)
into	ds_inconsistencia_w
from 	motivo_inconsist_servico 
where 	nr_sequencia = nr_seq_motivo_p;

return	ds_inconsistencia_w;

end Obter_Desc_Motivo_Inconsist;
/