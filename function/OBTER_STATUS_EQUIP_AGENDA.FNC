create or replace
function obter_status_equip_agenda(	nr_seq_agenda_p		number,
						cd_equipamento_p	number)
						return	varchar2 is

ds_status_w		varchar2(255):='';
qt_equipamento_w	number(10);
						
begin

if	(cd_equipamento_p is not null) then
	select	count(*)
	into	qt_equipamento_w
	from	consistencia_agenda_cir
	where	nr_seq_agenda 	= nr_seq_agenda_p
	and	cd_equipamento	= cd_equipamento_p;

	if	(qt_equipamento_w > 0) then
		ds_status_w := wheb_mensagem_pck.get_texto(309381); -- Indisponível
	else
		ds_status_w := wheb_mensagem_pck.get_texto(309380);-- Disponível
	end if;
end if;	
	

return	ds_status_w;

end obter_status_equip_agenda;
/