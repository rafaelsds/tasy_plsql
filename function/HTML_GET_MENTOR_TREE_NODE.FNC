create or replace
function	html_get_mentor_tree_node(
			ie_tipo_item_p	varchar2,
			ie_assistencial_p	varchar2,
			nr_seq_pend_pac_p number)
	return	number is

nr_tree_node_w		number(10);
ie_pendente_w		varchar2(1);

begin

nr_tree_node_w	:= 0;

if	(ie_tipo_item_p = 'PAC') then
	nr_tree_node_w	:= 633859;

elsif	(ie_tipo_item_p = 'PROT') then
	nr_tree_node_w	:= 633866;

elsif	(ie_tipo_item_p = 'INTERV' or ie_tipo_item_p = 'INTERV_ENF') then

	if (ie_assistencial_p = 'G') then
		return 633840;
	end if;

	if (nr_seq_pend_pac_p is not null and nr_seq_pend_pac_p >0) then

		select decode(count(1), 0, 'S', 'N') ie_pendente
		into ie_pendente_w
		from pe_prescricao
		where nr_seq_pend_pac_acao = nr_seq_pend_pac_p;

		if (ie_pendente_w = 'S') then
			return 633850;
		else
			return 633840;
		end if;
	else
		return 633850;
	end if;

elsif	(ie_tipo_item_p = 'URL') then
	nr_tree_node_w	:= 633885;

elsif	(ie_tipo_item_p = 'CUIDADOS_SAPS' or ie_tipo_item_p = 'SAPS') then

	if (ie_assistencial_p = 'G') then
		return 633819;
	end if;

	if (nr_seq_pend_pac_p is not null and nr_seq_pend_pac_p >0) then

		select decode(count(1), 0, 'S', 'N') ie_pendente
		into ie_pendente_w
		from pe_prescricao
		where nr_seq_pend_pac_acao = nr_seq_pend_pac_p;

		if (ie_pendente_w = 'S') then
			return 633829;
		else
			return 633819;
		end if;
	else
		return 633829;
	end if;

elsif	(ie_tipo_item_p = 'REGRA') and (ie_assistencial_p = 'S') then
	nr_tree_node_w	:= 633871;
elsif	(ie_tipo_item_p = 'PROGRAMA') then
	nr_tree_node_w := 1111008;
end if;

return	nr_tree_node_w;

end html_get_mentor_tree_node;
/
