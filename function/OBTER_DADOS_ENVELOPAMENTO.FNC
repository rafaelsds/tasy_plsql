create or replace
function obter_dados_envelopamento(
			nr_prescricao_p			number,
			nr_sequencia_prescricao_p	number,
			ie_opcao_p			varchar2)			
 		    	return varchar2 is
			

ds_retorno_w		varchar2(255);

begin

begin

if	(upper(ie_opcao_p) = 'N') then

	select	substr(obter_nome_usuario(a.nm_usuario_nrec),1,255)
	into	ds_retorno_w
	from	envelope_laudo_item a
	where	a.nr_prescricao	= nr_prescricao_p
	and	a.nr_seq_prescr	= nr_sequencia_prescricao_p;	

elsif	(upper(ie_opcao_p) = 'D') then

	select	to_char(dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss')
	into	ds_retorno_w
	from	envelope_laudo_item a
	where	a.nr_prescricao	= nr_prescricao_p
	and	a.nr_seq_prescr	= nr_sequencia_prescricao_p;	

end if;

exception
when others then
	ds_retorno_w	:=	null;
end;


	
return	ds_retorno_w;

end obter_dados_envelopamento;
/