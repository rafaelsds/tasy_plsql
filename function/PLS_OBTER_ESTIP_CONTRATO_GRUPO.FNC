create or replace
function pls_obter_estip_contrato_grupo
			(	nr_seq_contrato_p	Number,
				nr_seq_usu_grupo_p	Number)
				return Varchar2 is

ds_retorno_w			varchar2(255);
nr_seq_grupo_contrato_w		Number(10);

begin

begin
select	nr_seq_grupo_contrato
into	nr_seq_grupo_contrato_w
from	pls_grupo_contrato_web
where	nr_sequencia	= nr_seq_usu_grupo_p;
exception
when others then
	nr_seq_grupo_contrato_w	:= null;
end;

begin
select	substr(obter_nome_pf_pj(a.cd_pf_estipulante, a.cd_cgc_estipulante),1,200)
into	ds_retorno_w
from	pls_contrato		a,
	pls_contrato_grupo	b
where	a.nr_sequencia		= b.nr_seq_contrato
and	b.nr_seq_grupo		= nr_seq_grupo_contrato_w
and	b.nr_seq_contrato	= nr_seq_contrato_p;
exception
when others then
	ds_retorno_w		:= '';
end;

return ds_retorno_w;

end pls_obter_estip_contrato_grupo;
/