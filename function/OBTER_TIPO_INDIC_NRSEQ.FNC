create or replace
function obter_tipo_indic_nrseq(nr_seq_indicacao_p		varchar2)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(2);
ie_pessoa_w	varchar2(1);
ie_pj_w		varchar2(1);
ie_midia_w	varchar2(1);
ie_medico_w	varchar2(1);
ie_medico_cc_w	varchar2(1);

begin
	
select 	ie_pessoa, 
	ie_pessoa_juridica, 
	ie_midia,
	ie_medico,
	ie_medico_clinico
into	ie_pessoa_w,
	ie_pj_w,
	ie_midia_w,
	ie_medico_w,
	ie_medico_cc_w
from 	tipo_indicacao 
where 	nr_sequencia = to_number(nr_seq_indicacao_p);

if (ie_pessoa_w = 'S')  then
	ds_retorno_w := 'P';
elsif (ie_pj_w = 'S') then
	ds_retorno_w := 'J';
elsif (ie_midia_w = 'S') then
	ds_retorno_w := 'M';
elsif (ie_medico_w = 'S') then
	ds_retorno_w := 'N';
elsif (ie_medico_cc_w = 'S') then
	ds_retorno_w := 'CC';
end if;


return	ds_retorno_w;

end obter_tipo_indic_nrseq;
/