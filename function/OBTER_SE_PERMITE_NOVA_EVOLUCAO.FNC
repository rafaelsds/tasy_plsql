create or replace 
function Obter_se_permite_nova_evolucao ( 
			cd_tipo_evolucao_p varchar2,
			nr_atendimento_p   number,
               cd_evolucao_p number default 0)
			return varchar2 is

qt_regra_w  number;
qt_registro_w number;
ds_retorno_w varchar2(1);
ie_lib_evolucao_paciente_w	varchar2(1);

begin
ds_retorno_w := 'S';

obter_param_usuario(0,37,obter_perfil_ativo,obter_usuario_ativo,obter_estabelecimento_ativo,ie_lib_evolucao_paciente_w);

if  (cd_tipo_evolucao_p is not null) and
	(nr_atendimento_p is not null) and 
	(nr_atendimento_p > 0) then
	begin
	select 	nvl(qt_max_reg,0)
	into	qt_regra_w	
	from	tipo_evolucao
	where 	cd_tipo_evolucao = cd_tipo_evolucao_p;
	
	
	if  (qt_regra_w > 0) then
		select  count(1)
		into 	qt_registro_w
		from  evolucao_paciente
		where ie_evolucao_clinica = cd_tipo_evolucao_p
		and  	nr_atendimento = nr_atendimento_p
    and   cd_evolucao <> cd_evolucao_p
		and		((ie_lib_evolucao_paciente_w = 'N') or (dt_liberacao is not null))
		and 	dt_inativacao is null;

		if 	(qt_regra_w <= qt_registro_w) then	
			ds_retorno_w := 'N';
		end if;
		
	end if;
	end;
end if;

return ds_retorno_w;

end Obter_se_permite_nova_evolucao;
/