create or replace
function obter_valores_sv_graf_pepo(	nr_seq_item_p		number,
					nr_cirurgia_p		number,
					dt_registro_p		date,
					nr_seq_pepo_p		number )
					return varchar2 is

nm_tabela_w		varchar2(50);
nm_atributo_w		varchar2(50);
qt_retorno_w		number(15,4);
ds_retorno_w		varchar(255);
ie_retorno_w		varchar2(255);
vl_padrao_w		varchar2(30);


begin

select	max(nm_tabela),
	max(nm_atributo),	
	max(vl_padrao)
into	nm_tabela_w,
	nm_atributo_w,
	vl_padrao_w
from	pepo_sv
where	nr_sequencia	=	nr_seq_item_p
and	nr_sequencia <> 74;

if	(substr(nm_atributo_w,1,2) = 'QT') then
	if	(nr_seq_pepo_p > 0) then
		obter_valor_dinamico(	'select a.'||nm_atributo_w||' from '||nm_tabela_w||' a where a.nr_sequencia = (select max(b.nr_sequencia) from ' || 			nm_tabela_w|| ' b where b.nr_seq_pepo = '||to_char(nr_seq_pepo_p)||' and b.ie_situacao = ''A'') and a.'||nm_atributo_w||' is not null ',qt_retorno_w);
	else
		obter_valor_dinamico(	'select a.'||nm_atributo_w||' from '||nm_tabela_w||' a where a.nr_sequencia = (select max(b.nr_sequencia) from ' || 			nm_tabela_w|| ' b where b.nr_cirurgia = '||to_char(nr_cirurgia_p)||' and b.ie_situacao = ''A'') and a.'||nm_atributo_w||' is not null ',qt_retorno_w);
	end if;

	ds_retorno_w := to_char(qt_retorno_w);
	if	(qt_retorno_w = 0) and (nm_atributo_w <> 'QT_SEGMENTO_ST') then
		ds_retorno_w := null;
	end if;
	
elsif	(substr(nm_atributo_w,1,2) = 'DT') then
	ds_retorno_w := to_char(nvl(dt_registro_p,sysdate),'dd/mm/yyyy hh24:mi:ss');
elsif	(substr(nm_atributo_w,1,2) = 'IE') then
	if	(nr_seq_pepo_p > 0) then
		obter_valor_dinamico_char_bv(	'select a.'||nm_atributo_w||' from '||nm_tabela_w||' a where a.nr_sequencia = (select max(b.nr_sequencia) 		from ' || nm_tabela_w|| ' b where b.nr_seq_pepo = :nr_seq_pepo and b.ie_situacao = ''A'') and a.'||nm_atributo_w||' is not null ','nr_seq_pepo='||to_char(nr_seq_pepo_p)||'; ',ie_retorno_w);
	else
		obter_valor_dinamico_char_bv(	'select a.'||nm_atributo_w||' from '||nm_tabela_w||' a where a.nr_sequencia = (select max(b.nr_sequencia) 			from ' || nm_tabela_w|| ' b where b.nr_cirurgia = :nr_cirurgia and b.ie_situacao = ''A'') and a.'||nm_atributo_w||' is not null ','nr_cirurgia='||to_char(nr_cirurgia_p)||'; ',ie_retorno_w);
	end if;
	

	
	ds_retorno_w := ie_retorno_w;
end if;

if	(ds_retorno_w is null) then 
	ds_retorno_w := vl_padrao_w;
end if;

return	ds_retorno_w;

end obter_valores_sv_graf_pepo;
/