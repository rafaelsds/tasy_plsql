create or replace
Function ptu_obter_qtd_transacoes(	nr_seq_guia_p		number,
					nr_seq_requisicao_p	number,
					nr_seq_execucao_p	number,
					nr_seq_origem_p		number,
					ie_tipo_transacao_p	varchar2
					) return number is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter a quantidade de transa��es existentes para a guia/requisi��o/transa��o origem/transa��o execu��o informadas
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_transacao_w	varchar2(10);
qt_registros_w		number(10);
vl_retorno_w		number(10);
ds_comando_w		varchar2(4000);
nr_cur_w		pls_integer;
nr_exec_w		pls_integer;


/*	ie_tipo_transacao_p
	AUT 	- Pedido de autoriza��o
	RAUT 	- Resposta de autoriza��o
	COMPL 	- Pedido de complemento
	INS 	- Pedido de insist�ncia
	CANC 	- Pedido de cancelamento
	AUD 	- Resposta de auditoria
	ERR	- Erro inesperado
	CONF	- Confirma��o
	ORD	- Requisi��o de Ordem de servi�o
	RORD	- Resposta de ordem de servi�o
	AORD	- Autoriza��o de ordem de servi�o
	STAT	- Status da transa��o
	RSTAT	- Resposta do pedido de status
	DECP	- Decurso de prazo
	CONSP	- Consulta de dados do prestador
	CONSB	- Consulta de dados do benefici�rio	
*/

begin
ds_comando_w	:= 'select count(1) ';

select	decode(ie_tipo_transacao_p,1,'AUT',2,'RAUT',3,'COMPL',4,'INS',5,'CANC',6,'AUD',7,'ERR',8,'CONF',9,'ORD',10,'RORD',11,'AORD',12,'STAT',13,'RSTAT',14,'DECP',15,'CONSP',16,'CONSB')
into	ie_tipo_transacao_w
from 	dual;

if	(ie_tipo_transacao_w	= 'AUT') then
	ds_comando_w	:= ds_comando_w || 'from ptu_pedido_autorizacao ';
elsif	(ie_tipo_transacao_w	= 'RAUT') then
	ds_comando_w	:= ds_comando_w || 'from ptu_resposta_autorizacao ';
elsif	(ie_tipo_transacao_w	= 'COMPL') then
	ds_comando_w	:= ds_comando_w || 'from ptu_pedido_compl_aut ';
elsif	(ie_tipo_transacao_w	= 'INS') then
	ds_comando_w	:= ds_comando_w || 'from ptu_pedido_insistencia ';
elsif	(ie_tipo_transacao_w	= 'CANC') then
	ds_comando_w	:= ds_comando_w || 'from ptu_cancelamento ';
elsif	(ie_tipo_transacao_w	= 'AUD') then
	ds_comando_w	:= ds_comando_w || 'from ptu_resposta_auditoria ';
elsif	(ie_tipo_transacao_w	= 'ERR') then
	ds_comando_w	:= ds_comando_w || 'from ptu_erro_inesperado ';
elsif	(ie_tipo_transacao_w	= 'CONF') then
	ds_comando_w	:= ds_comando_w || 'from ptu_confirmacao ';
elsif	(ie_tipo_transacao_w	= 'ORD') then
	ds_comando_w	:= ds_comando_w || 'from ptu_requisicao_ordem_serv ';
elsif	(ie_tipo_transacao_w	= 'RORD') then
	ds_comando_w	:= ds_comando_w || 'from ptu_resposta_req_ord_serv ';
elsif	(ie_tipo_transacao_w	= 'AORD') then
	ds_comando_w	:= ds_comando_w || 'from ptu_autorizacao_ordem_serv ';	
elsif	(ie_tipo_transacao_w	= 'STAT') then
	ds_comando_w	:= ds_comando_w || 'from ptu_pedido_status ';
elsif	(ie_tipo_transacao_w	= 'RSTAT') then
	ds_comando_w	:= ds_comando_w || 'from ptu_resp_pedido_status ';
elsif	(ie_tipo_transacao_w	= 'DECP') then
	ds_comando_w	:= ds_comando_w || 'from ptu_decurso_prazo ';
elsif	(ie_tipo_transacao_w	= 'CONSP') then
	ds_comando_w	:= ds_comando_w || 'from ptu_consulta_prestador ';
elsif	(ie_tipo_transacao_w	= 'CONSB') then
	ds_comando_w	:= ds_comando_w || 'from ptu_consulta_beneficiario ';
end if;

if	(nr_seq_execucao_p is not null) then
	if	(ie_tipo_transacao_w	= 'STAT') then
		ds_comando_w	:= ds_comando_w || 'where nr_transacao_uni_exec	= :nr_seq_execucao ';
	elsif	(ie_tipo_transacao_w	= 'ORD') then
		ds_comando_w	:= ds_comando_w || 'where nr_transacao_solicitante = :nr_seq_execucao ';	
	else
		ds_comando_w	:= ds_comando_w || 'where nr_seq_execucao	= :nr_seq_execucao ';
	end if;	

	execute immediate ds_comando_w 
	into 	qt_registros_w
	using 	nr_seq_execucao_p;
	
	vl_retorno_w	:= qt_registros_w;
	
elsif	(nr_seq_origem_p is not null) then
	ds_comando_w	:= ds_comando_w || 'where nr_seq_origem	= :nr_seq_origem ';
	
	execute immediate ds_comando_w 
	into 	qt_registros_w
	using 	nr_seq_origem_p;
	
	vl_retorno_w	:= qt_registros_w;
	
elsif	(nr_seq_guia_p is not null) then
	ds_comando_w	:= ds_comando_w || 'where nr_seq_guia	= :nr_seq_guia ';
	
	execute immediate ds_comando_w 
	into 	qt_registros_w
	using 	nr_seq_guia_p;
	
	vl_retorno_w	:= qt_registros_w;	
	
elsif	(nr_seq_requisicao_p is not null) then
	ds_comando_w	:= ds_comando_w || 'where nr_seq_requisicao	= :nr_seq_requisicao ';	
	
	execute immediate ds_comando_w 
	into 	qt_registros_w
	using 	nr_seq_requisicao_p;
	
	vl_retorno_w	:= qt_registros_w;		
end if;



return	vl_retorno_w;

commit;

end ptu_obter_qtd_transacoes;
/