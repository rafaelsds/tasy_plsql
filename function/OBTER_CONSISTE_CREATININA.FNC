create or replace
function obter_consiste_creatinina( nr_sequencia_p 	number,
				    nr_seq_material_p	number,		
				ie_opcao_p	varchar2)
 		    	return varchar2 is

/*
Pasta protocolo 		Nr_seq_paciente_w 	:= nr_sequencia_p     
			Ie_opcao_p	:= P
			
Pasta atendimento do ciclo	Nr_seq_atendimento_w := nr_sequencia_p
			Ie_opcao_p	:= A
*/			

nr_sequencia_w	number(6);
qt_reg_w	number(10);
			
begin

if (ie_opcao_p = 'P') then

	select   count(*)
	into	 qt_reg_w
	from     paciente_protocolo_medic a
	where    a.nr_seq_diluicao is null
	and	 a.nr_seq_paciente = nr_sequencia_p
	and	 a.nr_seq_material = nr_seq_material_p
	and 	 a.nr_seq_diluicao is null
	and 	 a.nr_seq_solucao is null
	and 	 a.nr_seq_procedimento is null
	and 	exists  (select   1
		     from     unidade_medida b
		     where    a.cd_unidade_medida = b.cd_unidade_medida 
		     and      b.cd_unidade_medida <> b.cd_unidade_med_princ
		     and      b.cd_unidade_med_sec = 'mgc');

elsif (ie_opcao_p = 'A') then

	select   count(*)
	into	 qt_reg_w
	from     paciente_atend_medic a
	where    a.nr_seq_diluicao is null
	and	 a.nr_seq_atendimento = nr_sequencia_p
	and      a.nr_seq_material = nr_seq_material_p
	and 	 a.nr_seq_diluicao is null
	and 	 a.nr_seq_solucao is null
	and 	 a.nr_seq_procedimento is null
	and exists  (select   1
		     from     unidade_medida b
		     where    a.cd_unid_med_dose = b.cd_unidade_medida 
		     and      b.cd_unidade_medida <> b.cd_unidade_med_princ
		     and      b.cd_unidade_med_sec = 'mgc');
end if;

if	(qt_reg_w > 0) then
	return	'S';
else
	return	'N';
end	if;

end obter_consiste_creatinina;
/