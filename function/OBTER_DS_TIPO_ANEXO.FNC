create or replace
function obter_ds_tipo_anexo(nr_sequencia_p number)
 		    	return varchar2 is
ds_tipo_anexo_w tipo_anexo_agenda.ds_tipo_anexo%type;
begin

select 	substr(nvl(max(ds_tipo_anexo),''),1,255)
into 	ds_tipo_anexo_w
from	tipo_anexo_agenda
where 	nr_sequencia = nr_sequencia_p;

return	ds_tipo_anexo_w;

end obter_ds_tipo_anexo;
/

