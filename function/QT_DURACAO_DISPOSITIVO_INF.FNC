create or replace
function qt_duracao_dispositivo_inf(ie_bomba_infusao_p		varchar2)
 		    	return number is

qt_retorno_w	number(10,0);
			
begin

select	max(nr_minuto_duracao)
into	qt_retorno_w
from	qt_tempo_dispositivo
where	ie_bomba_infusao = ie_bomba_infusao_p;

return	qt_retorno_w;

end qt_duracao_dispositivo_inf;
/