create or replace
function qua_obter_qt_os(	ie_status_p			varchar2,
				dt_inicial_p			date,
				dt_final_p			date,
				nr_seq_estagio_p		varchar2,
				ie_tipo_ordem_p			number	)
 		    	return number is

cont_os_w 	number(10);

begin

select	decode(count(q.nr_sequencia), 0, 1, count(q.nr_sequencia))
into 	cont_os_w
from	man_ordem_servico q
where	(((ie_status_p = 'N') and q.ie_status_ordem in ('1','2')) or (q.ie_status_ordem = ie_status_p))
and	q.dt_ordem_servico between dt_inicial_p and dt_final_p + 86399/86400
and exists(	select	1
		from	man_ordem_servico d,
			man_tipo_solucao c,
			man_estagio_processo b,
			man_ordem_serv_estagio a
		where	b.nr_sequencia 		= a.nr_seq_estagio
		and	a.nr_seq_tipo_solucao 	= c.nr_sequencia(+)
		and	d.nr_sequencia 		= a.nr_seq_ordem
		and	a.nr_seq_ordem 		= q.nr_sequencia
		and   	((a.nr_seq_estagio 	= nr_seq_estagio_p) or (nr_seq_estagio_p = '0'))
		and   	((d.ie_tipo_ordem	= ie_tipo_ordem_p) or (ie_tipo_ordem_p = '0')));

return	cont_os_w;

end qua_obter_qt_os;
/