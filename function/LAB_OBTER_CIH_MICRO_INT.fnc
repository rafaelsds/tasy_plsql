CREATE OR REPLACE FUNCTION LAB_OBTER_CIH_MICRO_INT(CD_MICROORGANISMO_INTEGRACAO_P VARCHAR2,
                                                   DS_SIGLA_EQUIP_P               VARCHAR2)
  RETURN VARCHAR2 IS
  CD_MICROORGANISMO_W VARCHAR2(20);
BEGIN
  SELECT MAX(A.CD_MICROORGANISMO)
    INTO CD_MICROORGANISMO_W
    FROM CIH_MICROORGANISMO_INT A, EQUIPAMENTO_LAB B
   WHERE A.CD_EQUIPAMENTO = B.CD_EQUIPAMENTO
     AND UPPER(B.DS_SIGLA) = UPPER(DS_SIGLA_EQUIP_P)
     AND A.CD_MICROORGANISMO_INTEGRACAO = CD_MICROORGANISMO_INTEGRACAO_P;

  RETURN UPPER(CD_MICROORGANISMO_W);
END LAB_OBTER_CIH_MICRO_INT;
/
