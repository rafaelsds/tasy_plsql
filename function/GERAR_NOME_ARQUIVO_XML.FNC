create or replace
function GERAR_NOME_ARQUIVO_XML(nr_lote_ext_p number)
 		    	return varchar2 is 
  nomeArquivo_w varchar2(45);
  cnes_w varchar2(20);

begin


select p.cd_cnes lab_cnes
	into 	cnes_w
 from estabelecimento e
 join pessoa_juridica p on p.cd_cgc=e.cd_cgc
 where e.cd_estabelecimento=obter_estabelecimento_ativo()  ;    

nomearquivo_w :=  cnes_w ||
        to_char(sysdate,'yyyy') ||
        to_char(sysdate,'MM') ||
        to_char(sysdate,'dd') || 
        to_char(sysdate,'hh24') || 
        to_char(sysdate,'mi') ||
        to_char(sysdate,'ss');
        
return	nomeArquivo_w;

end gerar_nome_arquivo_xml;
/