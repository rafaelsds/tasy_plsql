create or replace
function lab_obter_setor_liberado_lote (nr_seq_lote_sec_p		number,
										nm_usuario_p		varchar2,
							  ie_opcao_p		    varchar2) 	
							  return varchar2 is
				
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
ds_setor_atendimento_w	setor_atendimento.ds_setor_atendimento%type;
ds_retorno_w			varchar2(255);
nm_usuario_w			usuario.nm_usuario%type;
ie_lote_liberado_w		varchar2(1);

begin
nm_usuario_w := nm_usuario_p;

if (nm_usuario_p is null) then
	nm_usuario_w:= Wheb_usuario_pck.get_nm_usuario;
end if;


if (nr_seq_lote_sec_p is not null) then


	select  decode(count(*),0,'N','S')
	into	ie_lote_liberado_w
	from 	lote_ent_secretaria
	where	nr_sequencia = nr_seq_lote_sec_p
	and		dt_liberacao is null;


	if (ie_lote_liberado_w = 'N') then
		select  max(b.cd_setor_atendimento),
				max(obter_nome_setor(b.cd_setor_atendimento))
		into	cd_setor_atendimento_w,
				ds_setor_atendimento_w
		from	prescr_medica a,
				prescr_procedimento b,
				usuario_setor c
		where 	a.nr_seq_lote_entrada = nr_seq_lote_sec_p
		and		a.nr_prescricao = b.nr_prescricao
		and		b.cd_setor_atendimento = c.cd_setor_atendimento
		and		c.nm_usuario_param = nm_usuario_w
		and		b.nr_seq_exame is not null
		and		rownum = 1;

		if (ie_opcao_p = 'C') then
			ds_retorno_w	:= cd_setor_atendimento_w;
		else
			ds_retorno_w	:= ds_setor_atendimento_w;
		end if;
	else
		ds_retorno_w := ie_lote_liberado_w;
	end if;

end if;

return	ds_retorno_w;

end lab_obter_setor_liberado_lote;

/