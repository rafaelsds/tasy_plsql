create or replace
function Obter_Dt_Retorno_Agenda_Mult (	nr_seq_agenda_p		number)
					return			date is

qt_media_w		number(10,0);

begin

/* Pega a m�dia do retorno */
begin
select	nvl(avg(qt_dias_retorno),0)
into	qt_media_w
from	agenda_consulta_prof
where	nr_seq_agenda	= nr_seq_agenda_p
and	qt_dias_retorno	> 0;
exception
	when others then
	qt_media_w	:= 0;
end;

return	trunc(sysdate+qt_media_w);

end Obter_Dt_Retorno_Agenda_Mult;
/