create or replace
function Obter_data_prev_exec
		(dt_prescricao_p		Date,
		dt_procedimento_p		Date,
		cd_setor_atendimento_p		Number,
		nr_prescricao_p			Number,
		ie_data_atual_p			varchar2)
		return date is

dt_retorno_w		Date;
dt_inicio_prescr_w	Date;
ie_data_ok_w		varchar2(1) := 'N';
ie_utilDtProc_w		varchar2(1);

Cursor C01 is
	select	pkg_date_utils.get_datetime(NVL(dt_procedimento_p,dt_prescricao_p), dt_horario)
	from	setor_horario_atend
	where	cd_setor_atendimento = cd_setor_atendimento_p
	and	((hr_limite is null) or
		 (to_char(Decode(nvl(ie_utilDtProc_w,'S'), 'N', nvl(dt_procedimento_p,dt_prescricao_p),sysdate),'hh24:mi') <= hr_limite))
	order by dt_horario;

begin

obter_param_usuario(924, 544, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, 0, ie_utilDtProc_w);

select	max(dt_inicio_prescr)
into	dt_inicio_prescr_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

OPEN C01;
LOOP
FETCH C01 into
	dt_retorno_w;
exit when c01%notfound;
	begin
	if	(ie_data_atual_p	= 'A') then
		if	(dt_retorno_w >= dt_prescricao_p) then
			begin
			ie_data_ok_w	:= 'S';
			exit;
			end;
		end if;
	else
		if	(dt_retorno_w >= nvl(dt_inicio_prescr_w, dt_prescricao_p)) then
			begin
			ie_data_ok_w	:= 'S';
			exit;
			end;
		end if;
	end if;
	end;
END LOOP;
Close C01;

if	(ie_data_ok_w = 'N') then
	OPEN C01;
	LOOP
	FETCH C01 into
		dt_retorno_w;
	exit when c01%notfound;
		begin
		dt_retorno_w	:= dt_retorno_w + 1;
		exit;
		end;
	END LOOP;
	Close C01;
end if;

if	(dt_retorno_w is null) or
	(dt_retorno_w < dt_prescricao_p) then
	dt_retorno_w	:= trunc(dt_procedimento_p,'hh24');
end if;

if	(dt_retorno_w < dt_prescricao_p) then
	if	(ie_data_atual_p	= 'A') then
		dt_retorno_w	:= dt_prescricao_p;
	else
		dt_retorno_w	:= dt_inicio_prescr_w;
	end if;
end if;

return	dt_retorno_w;

end Obter_data_prev_exec;
/
