create or replace
function pls_obter_qt_contratos_rpc
			(	nr_seq_lote_p		number)
				return number is

qt_retorno_w	number;

begin

select	count(*)
into	qt_retorno_w
from	pls_rpc_contrato
where	nr_seq_lote	= nr_seq_lote_p;

return	nvl(qt_retorno_w,0);

end pls_obter_qt_contratos_rpc; 
/