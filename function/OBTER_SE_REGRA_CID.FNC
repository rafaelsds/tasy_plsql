create or replace 
function obter_se_regra_cid(	nr_atendimento_p	number,
				cd_cid_p		varchar)
			return varchar2 is

ie_retorno_w	varchar2(01);
cd_estabelecimento_w		number(4);
cd_pessoa_fisica_w		varchar2(10);
ie_tipo_atendimento_w		number(3);
cd_convenio_w			number(5);
cd_setor_atendimento_w		number(5);
cd_medico_resp_w		varchar2(10);
qt_retorno_w			number(3);
nr_atendimento_w		number(10);
cd_medico_w			varchar2(10);
ie_regra_w			varchar2(1);
qt_dia_w			number(5);
nr_seq_classificacao_w		number(10);
ie_dentro_mes_w			Varchar2(1)	:= 'N';
nr_seq_queixa_w			number(10);
nr_seq_queixa_origem_w		number(10);
ie_consiste_setor_w		varchar2(10);
cd_cid_principal_w		varchar2(10);
cd_cid_principal_origem_w	varchar2(10);
cd_categoria_cid_origem_w	varchar2(10);
ie_mesmo_tipo_atend_w		varchar2(1);
qt_existe_w			number(10);

cursor C01 is
	select	ie_regra,
		qt_dia,
		nvl(qt_retorno,0),
		nvl(ie_dentro_mes,'N'),
		nvl(ie_consiste_setor,'S'),
		ie_mesmo_tipo_atend
	from	regra_atend_retorno
	where	(nvl(cd_estabelecimento, cd_estabelecimento_w)		= cd_estabelecimento_w or cd_estabelecimento_w is null)
	and	(nvl(ie_tipo_atendimento, ie_tipo_atendimento_w) 	= ie_tipo_atendimento_w or ie_tipo_atendimento_w is null)
	and	(nvl(cd_convenio, cd_convenio_w) 			= cd_convenio_w or cd_convenio_w is null)
	and	(nvl(cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w or cd_setor_atendimento_w is null)
	and	(nvl(nr_seq_classificacao, nr_seq_classificacao_w)	= nr_seq_classificacao_w or nr_seq_classificacao_w is null)
	and sysdate between nvl(dt_inicio_vigencia, sysdate) and nvl(dt_fim_vigencia, sysdate)
	and nvl(ie_situacao,'A') = 'A'
	order by qt_dia;

cursor C02 is
	select	nr_atendimento,
		cd_medico_resp,
		nr_seq_queixa,
		cd_cid_principal
	from 	atendimento_paciente_v
	where 	(nvl(cd_estabelecimento, cd_estabelecimento_w)		= cd_estabelecimento_w or cd_estabelecimento_w is null)
	and 	(nvl(ie_tipo_atendimento, ie_tipo_atendimento_w) 	= ie_tipo_atendimento_w or ie_tipo_atendimento_w is null)
	and 	(nvl(cd_convenio, cd_convenio_w) 			= cd_convenio_w or cd_convenio_w is null)
	and	((nvl(cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w or cd_setor_atendimento_w is null 
	and 	ie_consiste_setor_w = 'S') or (ie_consiste_setor_w = 'N'))
	and	(nvl(nr_seq_classificacao, nr_seq_classificacao_w)	= nr_seq_classificacao_w or nr_seq_classificacao_w is null)
	and 	cd_pessoa_fisica					= cd_pessoa_fisica_w
	and 	nr_atendimento						<> nr_atendimento_p
	and 	dt_entrada between sysdate - qt_dia_w and sysdate
	and	nr_atend_original	is null
	order by nr_atendimento desc;

begin

begin
select	cd_estabelecimento,
	cd_pessoa_fisica,
	ie_tipo_Atendimento,
	cd_convenio,
	cd_setor_atendimento,
	cd_medico_resp,
	nvl(nr_seq_classificacao,''),
	nr_seq_queixa,
	cd_cid_principal,
	Obter_Categoria_Cid(cd_cid_principal)
into	cd_estabelecimento_w,
	cd_pessoa_fisica_w,
	ie_tipo_atendimento_w,
	cd_convenio_w,
	cd_setor_atendimento_w,
	cd_medico_resp_w,
	nr_seq_classificacao_w,
	nr_seq_queixa_origem_w,
	cd_cid_principal_origem_w,
	cd_categoria_cid_origem_w
from	atendimento_paciente_v
where	nr_atendimento = nr_atendimento_p;
exception
when others then
	cd_estabelecimento_w		:= null;
	cd_pessoa_fisica_w		:= null;
	ie_tipo_atendimento_w		:= null;
	cd_convenio_w			:= null;
	cd_setor_atendimento_w		:= null;
	cd_medico_resp_w		:= null;
	nr_seq_classificacao_w		:= null;
	cd_cid_principal_origem_w	:= null;
	cd_categoria_cid_origem_w	:= null;
end;

open C01;
loop
	fetch C01 into	ie_regra_w,
			qt_dia_w,
			qt_retorno_w,
			ie_dentro_mes_w,
			ie_consiste_setor_w,
			ie_mesmo_tipo_atend_w;
	exit when C01%notfound;
end loop;
close C01;

ie_retorno_w	:= 'N';

if	(qt_dia_w > 0) and
	(ie_regra_w = 'K') then
	begin
		
	open C02;
	loop
	fetch C02 into	nr_atendimento_w,
			cd_medico_w,
			nr_seq_queixa_w,
			cd_cid_principal_w;
	exit when C02%notfound;
		if	(cd_cid_principal_w = cd_cid_p) and
			(cd_cid_principal_w is not null) and
			(cd_cid_principal_origem_w is not null) and
			((cd_cid_principal_origem_w = cd_cid_principal_w) or 
			 (obter_se_cid_equivalente(cd_cid_principal_w,cd_cid_principal_origem_w) = 'S')) then
			begin
			select	count(*)
			into	qt_existe_w
			from	atendimento_paciente_v
			where cd_estabelecimento	= cd_estabelecimento_w
			  and ((ie_tipo_atendimento	= ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N')) 
			  and cd_convenio		= cd_convenio_w
			  and cd_medico_resp		= cd_medico_resp_w
			  and dt_entrada		>= sysdate - qt_dia_w
			  and cd_pessoa_fisica		= cd_pessoa_fisica_w
			  and nr_atend_original	is null
			  and nr_atendimento		<> nr_atendimento_p
			  and nr_atendimento		= nr_atendimento_w;
			  
			if	(qt_existe_w > 0) then
				ie_retorno_w	:= 'S';
			else
				ie_retorno_w	:= 'N';
			end if;

			end;
		end if;
	end loop;
	close C02;
	end;
end if;

return ie_retorno_w;

end obter_se_regra_cid;
/
