create or replace
function		cm_obter_estab_atual_conj(
			nr_seq_conjunto_p		number,
			cd_estabelecimento_p		number default null)
			return number is

cd_retorno_w		number(4);
nr_sequencia_w		number(10);

begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	cm_conjunto_movto
where	nr_seq_conjunto = nr_seq_conjunto_p;

select	nvl(max(cd_estab_atual),0)
into	cd_retorno_w
from	cm_conjunto_movto
where	nr_sequencia = nr_sequencia_w;

if	(nvl(cd_retorno_w,0) = 0) then
	
	if	(cd_estabelecimento_p	is null) then
	
		select	cd_estabelecimento
		into	cd_retorno_w
		from	cm_conjunto_cont
		where	nr_sequencia = nr_seq_conjunto_p;
	else
		cd_retorno_w	:= cd_estabelecimento_p;

	end if;
	

end if;

return	cd_retorno_w;

end cm_obter_estab_atual_conj;
/