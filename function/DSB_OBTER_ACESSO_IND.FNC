create or replace
function dsb_obter_acesso_ind(	nr_seq_indicador_p	number,
				 nm_usuario_p		varchar2)
 		    		return varchar2 is

ie_permite_w		varchar2(1)	:= 'S';
cd_setor_w		number(10);
cd_cargo_w		number(10);
cd_perfil_w		number(10);
cd_perfil_ativo_w		number(10);
nm_usuario_lib_w		varchar2(15);
nr_seq_grupo_usuario_w	number(10);
qt_registro_w		number(10);

cursor c01 is
select	nvl(a.ie_permite,'N') ie_permite,
	a.cd_perfil,
	a.cd_setor_atendimento,
	a.nm_usuario_lib,
	a.nr_seq_grupo_usuario
from	dsb_lib_indicador a
where	nr_seq_indicador			= nr_seq_indicador_p
and	nvl(cd_cargo, cd_cargo_w)		= cd_cargo_w
and	nvl(cd_perfil, cd_perfil_ativo_w)	= cd_perfil_ativo_w
and	nvl(nm_usuario_lib, nm_usuario_p)	= nm_usuario_p
order by	nvl(nm_usuario_lib,'A'),
		nvl(nr_seq_grupo_usuario,0),
	nvl(cd_perfil,0),
	 nvl(cd_cargo,0),
	 nvl(cd_setor_atendimento,0);
	 

begin

select	count(*)
into	qt_registro_w
from	dsb_lib_indicador
where	nr_seq_indicador = nr_seq_indicador_p;

begin
cd_perfil_ativo_w	:= nvl(wheb_usuario_pck.get_cd_perfil,0);
exception when others then
	cd_perfil_ativo_w	:= 0;
end;

if	(qt_registro_w > 0) then
	
	ie_permite_w	:= 'N';
	
	select	nvl(max(b.cd_cargo),0)
	into	cd_cargo_w
	from	usuario a,
		pessoa_fisica b
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	a.nm_usuario		= nm_usuario_p;
	
	open C01;
	loop
	fetch C01 into	
		ie_permite_w,
		cd_perfil_w,
		cd_setor_w,
		nm_usuario_lib_w,
		nr_seq_grupo_usuario_w;
	exit when C01%notfound;
		begin
		
		if	(nm_usuario_lib_w is null) and
			(cd_perfil_w is not null) then
			
			select	nvl(max('S'),'N')
			into	ie_permite_w
			from	usuario_perfil b
			where	b.cd_perfil	= cd_perfil_w
			and	b.nm_usuario	= nm_usuario_p
			and	ie_permite_w	= 'S';
			
		end if;
		
		if	(nm_usuario_lib_w is null) and
			(cd_setor_w is not null) then
			
			select	nvl(max('S'),'N')
			into	ie_permite_w
			from	usuario_setor_v b
			where	b.cd_setor_atendimento	= cd_setor_w
			and	b.nm_usuario		= nm_usuario_p
			and	ie_permite_w		= 'S';
			
		end if;
		
		if	(nm_usuario_lib_w is null) and
			(nr_seq_grupo_usuario_w is not null) then
			
			select	nvl(max('S'),'N')
			into	ie_permite_w
			from	usuario_grupo b
			where	b.nr_seq_grupo		= nr_seq_grupo_usuario_w
			and	b.nm_usuario_grupo	= nm_usuario_p
			and	ie_permite_w		= 'S';
		end if;
		
		ie_permite_w	:= ie_permite_w;
		end;
	end loop;
	close C01;
end if;

return	ie_permite_w;

end dsb_obter_acesso_ind;
/