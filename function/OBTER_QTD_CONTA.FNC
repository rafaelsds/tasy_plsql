create or replace
function obter_qtd_conta(nr_cirurgia_p			number,
						 cd_material_p			number,
						 nr_seq_lote_fornec_p	number
						)
 		    	return number is

qt_material_w	number := 0;				
				
begin

	SELECT		SUM(qt_material)
	INTO		qt_material_w
	FROM 		material_atend_paciente
	WHERE 		nr_seq_atepacu = obter_unid_setor_cirurgia(nr_cirurgia_p,'N')
	AND 		cd_material    = cd_material_p
	AND 		NVL(nr_seq_lote_fornec,NVL(nr_seq_lote_fornec_p,0)) = NVL(nr_seq_lote_fornec_p,0)
	AND 		CD_MOTIVO_EXC_CONTA IS NULL;

return	nvl(qt_material_w,0);

end obter_qtd_conta;
/	