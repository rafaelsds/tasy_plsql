create or replace
function obter_etapas_adep_dp(	nr_prescricao_p		number,
				nr_seq_solucao_p	number,
				nm_usuario_p		varchar2,
				cd_perfil_p		number)
				return number is

nr_etapas_w			number(10,0) := 0;
cd_estabelecimento_w		number(4);
ie_utiliza_drenagem_w		varchar2(1);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) then

	select	nvl(max(cd_estabelecimento),1)
	into	cd_estabelecimento_w
	from	prescr_medica
	where	nr_prescricao	= nr_prescricao_p;
	
	Obter_Param_Usuario(1113, 503, cd_perfil_p, nm_usuario_p, cd_estabelecimento_w, ie_utiliza_drenagem_w);	
	
	select	count(*)
	into	nr_etapas_w
	from	hd_prescricao_evento
	where	nr_prescricao		= nr_prescricao_p
	and	nr_seq_solucao		= nr_seq_solucao_p
	and	((ie_evento		= 'DT') or
		 ((ie_evento		= 'IT') and
		  (ie_utiliza_drenagem_w = 'N')) or
		 (ie_evento		= 'SE'));

end if;

return nr_etapas_w;

end obter_etapas_adep_dp;
/
