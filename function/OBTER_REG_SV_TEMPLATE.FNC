create or replace
function obter_reg_SV_template(nr_sequencia_p	number)
 		    	return number is

nr_reg_w	number(10);

begin
select	max(a.nr_sequencia)
into	nr_reg_w
from	atendimento_sinal_vital a,
	ehr_reg_template c,
	ehr_reg_elemento d
where	c.nr_sequencia = d.nr_seq_reg_template
and	d.nr_sequencia = a.nr_seq_reg_elemento
and	c.nr_seq_reg = nr_sequencia_p;

return nr_reg_w;

end obter_reg_SV_template;
/