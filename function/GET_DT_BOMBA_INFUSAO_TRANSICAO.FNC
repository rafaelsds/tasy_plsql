create or replace FUNCTION GET_DT_BOMBA_INFUSAO_TRANSICAO(nr_sequencia_interface_p		NUMBER)
 		    	RETURN DATE IS
nr_seq_bomba_transicao_v         bomba_infusao_transicao.nr_sequencia%TYPE;                 
dt_transicao_w	DATE;
BEGIN

    BEGIN
        SELECT nr_sequencia 
          INTO nr_seq_bomba_transicao_v 
          FROM (
            SELECT nr_sequencia 
              FROM bomba_infusao_transicao 
             WHERE nr_seq_bomba_interface = nr_sequencia_interface_p 
               AND (ie_status IN ('ST', 'DS') OR ds_campo = 'DS_MEDICAMENTO')
             ORDER BY dt_transicao DESC)
         WHERE ROWNUM = 1;
    
        SELECT nvl(dt_transicao, sysdate) INTO dt_transicao_w  
          FROM bomba_infusao_transicao 
         WHERE ROWNUM = 1 
           AND nr_seq_bomba_interface = nr_sequencia_interface_p  
           AND nr_sequencia > nr_seq_bomba_transicao_v
         ORDER BY nr_sequencia;
    EXCEPTION
        WHEN no_data_found THEN dt_transicao_w := sysdate;
    END;

RETURN	dt_transicao_w;

END GET_DT_BOMBA_INFUSAO_TRANSICAO;
/
