create or replace
function obter_atend_gerado(nr_atendimento_p	number)
 		    	return number is

nr_atendimento_w	number(10);
			
begin

select 	max(nr_atendimento) 
into	nr_atendimento_w
from 	atendimento_paciente 
where 	nr_atend_alta = nr_atendimento_p;

return	nvl(nr_atendimento_w,nr_atendimento_p);

end obter_atend_gerado;
/