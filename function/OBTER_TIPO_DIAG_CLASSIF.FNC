create or replace
function obter_tipo_diag_classif(ie_tipo_diag_classif_p		varchar2)
 		    	return varchar2 is

ds_retorno_w 	varchar2(150);

begin

if 	(ie_tipo_diag_classif_p = 'E') then 
	ds_retorno_w := obter_desc_expressao(694629);
elsif (ie_tipo_diag_classif_p = 'A') then 
	ds_retorno_w := obter_desc_expressao(694631);
end if;

return	ds_retorno_w;

end obter_tipo_diag_classif;
/