create or replace
function obter_qt_encaixe_perm_agenda	(cd_agenda_p	number)
						return number is

qt_encaixe_w	number(10,0) := 0;

begin
if	(cd_agenda_p is not null) then
	select	nvl(max(qt_encaixe),0)
	into	qt_encaixe_w
	from	agenda
	where	cd_agenda = cd_agenda_p;
end if;

return qt_encaixe_w;

end obter_qt_encaixe_perm_agenda;
/