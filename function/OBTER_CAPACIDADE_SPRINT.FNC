create or replace
function obter_capacidade_sprint(	nr_sequencia_p		number,
					nr_time_p		number)
 		    	return number is

qt_capacidade_w		number(10);
nr_art_w		desenv_art.nr_sequencia%type;

begin

select	nr_art
into	nr_art_w
from	desenv_art a,
	desenv_release b,
	desenv_sprint c
where	c.nr_release = b.nr_sequencia
and	b.nr_art = a.nr_sequencia
and	c.nr_sequencia = nr_sequencia_p;

select	count(*) * 8
into	qt_capacidade_w
from	desenv_team a,
	desenv_member b
where	a.nr_sequencia = b.nr_team
and	a.nr_sequencia = nvl(nr_time_p,a.nr_sequencia)
and	a.nr_art = nr_art_w
and	((cd_papel in (1,4)) or (ie_desenvolvedor = 'S') or (ie_testador = 'S'));

return	qt_capacidade_w;

end obter_capacidade_sprint;
/