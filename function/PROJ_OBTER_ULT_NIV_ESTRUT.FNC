CREATE OR REPLACE
FUNCTION proj_obter_ult_niv_estrut(nr_sequencia_p	number,
				nr_seq_cronograma_p	number)
				RETURN number IS

ds_retorno_w	number(15);

BEGIN

if	(nr_sequencia_p is not null) then
	select	nvl(max(a.nr_seq_apres),0)
	into	ds_retorno_w
	from	proj_cron_estrut a
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_seq_cronograma = nr_seq_cronograma_p
	and	not exists (	select	b.nr_seq_apres
				from	proj_cron_estrut b
				where	a.nr_seq_apres = b.nr_seq_superior);
end if;

RETURN	ds_retorno_w;

END proj_obter_ult_niv_estrut;
/