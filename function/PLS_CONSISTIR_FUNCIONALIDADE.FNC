create or replace
function pls_consistir_funcionalidade(	cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					ie_tipo_outorgante_p		pls_outorgante.ie_tipo_outorgante%type,
					ie_sub_tipo_outorgante_p	varchar2,
					ie_acao_p			varchar2 )
					return varchar2 is 
--IE_TIPO_OUTORGANTE_P
--1	Administradora
--2	Autogest�o
--3	Cooperativa m�dica
--4	Cooperativa odontol�gica
--5	Filantropia
--6	Medicina de grupo
--7	Odontologia de grupo
--8	Seguradora especializada
	
-- IE_SUB_TIPO_OUTORGANTE_P
-- U	Unimed

-- IE_ACAO_P
-- F	Fun��o
-- P	Pasta
					
ds_retorno_w			varchar2(4000) := null;
ie_tipo_outorgante_w		pls_outorgante.ie_tipo_outorgante%type;
cd_cgc_outorgante_w		pls_outorgante.cd_cgc_outorgante%type;
ds_tipo_outorgante_w		varchar2(255);
ds_razao_social_w		pessoa_juridica.ds_razao_social%type;

begin

select	max(cd_cgc_outorgante),
	max(ie_tipo_outorgante),
	substr(obter_valor_dominio(1753,max(ie_tipo_outorgante_p)),1,255) ds_tipo_outorgante
into	cd_cgc_outorgante_w,
	ie_tipo_outorgante_w,
	ds_tipo_outorgante_w
from	pls_outorgante 
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_tipo_outorgante_w != ie_tipo_outorgante_p) then
	ds_retorno_w := obter_texto_dic_objeto( 454237 ,wheb_usuario_pck.get_nr_seq_idioma, 'DS_TIPO_OUTORGANTE=' || ds_tipo_outorgante_w || ';');
end if;

if	(ds_retorno_w is null) and
	(ie_tipo_outorgante_w = 3) and
	(ie_sub_tipo_outorgante_p = 'U') then
	select	max(ds_razao_social)
	into	ds_razao_social_w
	from	pessoa_juridica
	where	cd_cgc	= cd_cgc_outorgante_w;
	
	if	(upper(ds_razao_social_w) not like '%UNIMED%') then
		ds_retorno_w := obter_texto_dic_objeto( 454237 ,wheb_usuario_pck.get_nr_seq_idioma, 'DS_TIPO_OUTORGANTE=' || ds_tipo_outorgante_w || ';');
	end if;
end if;

return	ds_retorno_w;

end pls_consistir_funcionalidade;
/