create or replace function cpoe_recalcula_horarios_copia(	ds_horarios_p	in cpoe_material.ds_horarios%type,
										dt_referencia_p	in cpoe_material.dt_prox_geracao%type,
										dt_fim_p		in cpoe_material.dt_fim%type,
										ie_acm_p		in intervalo_prescricao.ie_acm%type,
										ie_sn_p			in intervalo_prescricao.ie_se_necessario%type,
										cd_intervalo_p	in intervalo_prescricao.cd_intervalo%type ) 
									return varchar2 is

ds_retorno_w	cpoe_material.ds_horarios%type;
ds_horarios_w	cpoe_material.ds_horarios%type;
qt_dia_adic_w	pls_integer := 0;
ds_hora_w		varchar2(255 char) := '';
dt_horario_w	date;
ie_operacao_w	intervalo_prescricao.ie_operacao%type;
qt_pertence_w	pls_integer;
i_pos			pls_integer;
sql_w           varchar2(2000);

begin
	ds_retorno_w := '';

	if ((dt_fim_p is not null) and (nvl(ie_acm_p, 'N') = 'N') and (nvl(ie_sn_p, 'N') = 'N')) then
		ds_horarios_w := padroniza_horario_prescr(ds_horarios_p, dt_referencia_p);

		select max(a.ie_operacao)
		  into ie_operacao_w
		  from intervalo_prescricao a
		 where a.cd_intervalo = cd_intervalo_p;

        begin
            sql_w := 'CALL OBTER_RECALCULA_HORARIOS_MD(:1, :2, :3, :4) INTO :ds_retorno_w;';
            EXECUTE IMMEDIATE sql_w USING 
                IN dt_referencia_p,
                IN ie_operacao_w,
                IN dt_fim_p,
                IN OUT ds_horarios_w,
                OUT ds_retorno_w;
        exception
            when others then
                ds_retorno_w := '';
        end;
	end if;

	ds_retorno_w := nvl(replace(padroniza_horario_prescr(ds_retorno_w, dt_referencia_p), 'A', ''), ds_horarios_p);
	return ds_retorno_w;

end cpoe_recalcula_horarios_copia;
/
