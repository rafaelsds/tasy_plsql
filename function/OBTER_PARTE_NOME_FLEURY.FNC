create or replace
function obter_parte_nome_fleury( nm_pessoa_fisica_p		varchar2,
				  ie_tipo_nome_p		varchar2)
 		    	return varchar2 is

			
ds_retorno_w		varchar2(255);
nm_pessoa_fisica_w	varchar2(255);
complemento_nome_w	varchar2(255);
ds_nome_w		varchar2(255);
ds_sobrenome_w		varchar2(255);
			
pos			number(10);	
tam			number(10);
tamNome			number(10);
tamResto		number(10);
			
begin

nm_pessoa_fisica_w := trim(nm_pessoa_fisica_p);


if (upper(Obter_Parte_Nome(nm_pessoa_fisica_w,'sobrenome')) in ('II','III','IV','V','VI','VII','PRIMEIRO','SEGUNDO','TERCEIRO','QUARTO','QUINTO','SEXTO','SETIMO','DE','FILHO','FO','JUNIOR','JR','SOBRINHO','NETO','NETTO','SENIOR')) then

	
	pos		   := Instr(nm_pessoa_fisica_w, chr(32),-1);
	
	complemento_nome_w := substr(nm_pessoa_fisica_w, pos + 1, 100);
	
	nm_pessoa_fisica_w := trim(substr(nm_pessoa_fisica_w, 1 ,pos));
	
	if	not (instr(nm_pessoa_fisica_w,chr(32)) > 0) then
	
		complemento_nome_w := '';
		nm_pessoa_fisica_w := trim(nm_pessoa_fisica_p);
	
	end if;
	
end if;

pos		:= Instr(nm_pessoa_fisica_w, chr(32),-1);

ds_sobrenome_w 	:= substr(nm_pessoa_fisica_w, pos + 1, 100);	
ds_nome_w 	:= substr(nm_pessoa_fisica_w, 1 ,pos);
	

tam := length(ds_nome_w||ds_sobrenome_w||complemento_nome_w);		
if (tam >= 40) then
	
	tamNome  := length(ds_nome_w);
	tamResto := length(ds_sobrenome_w||complemento_nome_w);
	
	if	( tamNome < (tam - tamResto)) then
	
		ds_nome_w := substr(ds_nome_w ,1 , 1 );
		
		complemento_nome_w := '';
	
	else
	
		ds_nome_w := substr(ds_nome_w ,1 , tamNome - (tam - 38));
				
	end if;
	
end if;



tam := length(ds_nome_w||ds_sobrenome_w||complemento_nome_w);	
	
if (tam >= 40) then
	
	tamResto := length(ds_sobrenome_w||complemento_nome_w);
	
	complemento_nome_w := '';
	
	tam := length(ds_nome_w||ds_sobrenome_w||complemento_nome_w);
	if (tam >= 40) then
		ds_sobrenome_w := substr(ds_sobrenome_w,1,38);
	end if;

	
end if;


	
if (lower(ie_tipo_nome_p) = 'nome') then
	
	ds_retorno_w := ds_nome_w;
	
elsif (lower(ie_tipo_nome_p) = 'sobrenome') then

	ds_retorno_w := ds_sobrenome_w;

elsif (lower(ie_tipo_nome_p) = 'complementonome') then

	ds_retorno_w := complemento_nome_w;

end if;

return	ds_retorno_w;

end obter_parte_nome_fleury;
/