create or replace 
function Obter_vol_cor_elem_nut_pac	(
			nr_sequencia_p		number)
			return Number is


qt_volume_w		Number(15,3) := 0;

BEGIN

select	nvl(sum(nvl(qt_vol_cor,0)),0)
into	qt_volume_w
from	nut_pac_elem_mat
where	nr_seq_pac_elem	= nr_sequencia_p;

return qt_volume_w;

end Obter_vol_cor_elem_nut_pac;
/