create or replace
function obter_se_perfil_exame(	nr_seq_exame_p		number,
				cd_perfil_ativo_p	number )
				return varchar2 is

ie_utiliza_w	varchar2(1) := 'N';

begin

select	nvl(max('N'),'S')
into	ie_utiliza_w
from	med_exame_padrao_perfil
where	nr_seq_exame_padrao	=	nr_seq_exame_p;

if	(ie_utiliza_w = 'N') then	
	select	nvl(max('S'),'N')
	into	ie_utiliza_w
	from	med_exame_padrao_perfil
	where	nr_seq_exame_padrao	=	nr_seq_exame_p
	and	cd_perfil		=	cd_perfil_ativo_p;
end if;

return	ie_utiliza_w;

end obter_se_perfil_exame;
/