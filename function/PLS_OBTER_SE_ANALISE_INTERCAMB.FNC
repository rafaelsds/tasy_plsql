create or replace
function pls_obter_se_analise_intercamb
			(	nr_seq_auditoria_p	Number)
				return Varchar2 is

ie_tipo_processo_w		Varchar2(2);
ie_tipo_intercambio_w		Varchar2(10);
ie_retorno_w			Varchar2(2)	:= 'N';

nr_seq_guia_w			Number(10);
nr_seq_requisicao_w		Number(10);

qt_registros_w			Number(10);

begin

begin
select	nvl(nr_seq_guia,0),
	nvl(nr_seq_requisicao,0)
into	nr_seq_guia_w,
	nr_seq_requisicao_w
from	pls_auditoria
where	nr_sequencia	= nr_seq_auditoria_p;
exception
when others then
	nr_seq_guia_w		:= 0;
	nr_seq_requisicao_w	:= 0;
end;

if	(nr_seq_guia_w	<> 0) then
	select	ie_tipo_processo,
		ie_tipo_intercambio
	into	ie_tipo_processo_w,
		ie_tipo_intercambio_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_w;
	
	if	(ie_tipo_processo_w	= 'I') then
		ie_retorno_w	:= 'S';
	end if;
elsif	(nr_seq_requisicao_w	<> 0) then
	select	ie_tipo_processo,
		ie_tipo_intercambio
	into	ie_tipo_processo_w,
		ie_tipo_intercambio_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_w;
	
	if	(ie_tipo_processo_w	= 'I') then
		ie_retorno_w	:= 'S';
	end if;
end if;

return	ie_retorno_w;

end pls_obter_se_analise_intercamb;
/
