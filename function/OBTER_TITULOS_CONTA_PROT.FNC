create or replace
function obter_titulos_conta_prot(nr_seq_protocolo_p	number) 
				 return varchar2 is

ie_todas_contas_w	varchar2(1) := 'S';
nr_titulo_w		number(10,0);
nr_interno_conta_w	number(10,0);

Cursor C01 is
	select	nr_interno_conta
	from	conta_paciente
	where	nr_seq_protocolo = nr_seq_protocolo_p;
				 
begin

ie_todas_contas_w	:= 'S';

open C01;
loop
fetch C01 into	
	nr_interno_conta_w;
exit when C01%notfound;
	begin
	
	select	nvl(max(nr_titulo),0)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_interno_conta = nr_interno_conta_w;
	
	if	(nr_titulo_w = 0) then
		ie_todas_contas_w	:= 'N';
		exit;
	end if;
	
	
	end;
end loop;
close C01;

return ie_todas_contas_w;

end obter_titulos_conta_prot;
/
