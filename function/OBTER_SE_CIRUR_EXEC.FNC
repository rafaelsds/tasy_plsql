create or replace
function obter_se_cirur_exec(	nr_cirurgia_p	number )
				return varchar2 is

ie_matmed_nao_exec_w	varchar2(1);
ie_proc_nao_exec_w	varchar2(1);
nr_prescricao_w		number(10,0);
ds_atendimento_normal_w	varchar2(255);
ie_alerta_exec_Mat_Proc	varchar(255);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(494245, null, wheb_usuario_pck.get_nr_seq_idioma);--Atendimento Normal
					
begin

obter_param_usuario(900, 190, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_alerta_exec_Mat_Proc);

select	substr(obter_se_atendimento_normal(nr_atendimento, nr_cirurgia, cd_procedimento_princ, ie_origem_proced),1,20),
	nr_prescricao
into	ds_atendimento_normal_w,
	nr_prescricao_w
from	cirurgia
where	nr_cirurgia	=	nr_cirurgia_p;

select	nvl(max('S'),'N')
into	ie_matmed_nao_exec_w
from	prescr_material
where	nr_prescricao = nr_prescricao_w
and	nvl(cd_motivo_baixa,0) = 0;

if (ie_alerta_exec_Mat_Proc <> 'SM') and
   (ie_alerta_exec_Mat_Proc <> 'NM') then
	select	nvl(max('S'),'N')
	into	ie_proc_nao_exec_w
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_w
	and	nvl(cd_motivo_baixa,0) = 0;
end if;

if	(ie_matmed_nao_exec_w = 'S') or 
	(ie_proc_nao_exec_w = 'S') or
	((ie_alerta_exec_Mat_Proc <> 'SM') and
	(ie_alerta_exec_Mat_Proc <> 'NM') and	
	(ds_atendimento_normal_w <> expressao1_w)) then
	return 'N';
else
	return 'S';
end if;

end obter_se_cirur_exec;
/
