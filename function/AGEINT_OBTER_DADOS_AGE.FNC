CREATE OR REPLACE function AGEINT_OBTER_DADOS_AGE (nr_sequencia_p IN number,
                                                   nm_parametro_p IN varchar2,
                                                   ds_agenda_p    IN varchar2)                
  return varchar2
 IS

 vl_param_retorno_w varchar2(4000);
 ds_comando_w		varchar2(400);
 ds_param_w		    varchar2(400);

 BEGIN
 
    IF (ds_agenda_p = 'C') THEN 
	    
		ds_comando_w :=	'select	' || nm_parametro_p || ' ' ||
	 					'from		agenda_consulta ' ||
						'where		nr_sequencia = :nr_sequencia_p';
		ds_param_w := 'nr_sequencia= ' || nr_sequencia_p;

	ELSIF (ds_agenda_p = 'E') THEN

	 	ds_comando_w :=	'select	' || nm_parametro_p || ' ' ||
	 					'from		agenda_paciente ' ||
						'where		nr_sequencia = :nr_sequencia_p';
		ds_param_w := 'nr_sequencia= ' || nr_sequencia_p;
	
 END IF;

	select	obter_select_concatenado_bv(ds_comando_w, ds_param_w, ',')
	into	vl_param_retorno_w
	from	dual;

RETURN vl_param_retorno_w;

END AGEINT_OBTER_DADOS_AGE;
/
