create or replace
function Consistir_acoes_vanco_nvs(	nr_sequencia_p	number,
									ie_tipo_seq_p	varchar2)
 		    	return varchar2 is			

nr_prescricao_w			number(15);
nr_atendimento_w		number(15);
ie_retorno_w			varchar2(1);
ie_dialise_w			varchar2(15);
nr_seq_regra_pai_w		number(15);
ie_ajuste_dose_w		varchar2(15);
qt_nivel_serico_w		number(15,4);
dt_inicio_vig_result_w	date;

cursor C02 is
	select	qt_nivel_serico
	from	rep_result_vanco
	where	1 = 1
	and		dt_resultado > dt_inicio_vig_result_w
	and		ie_novo_resultado = 'S'
	and		nr_atendimento = nr_atendimento_w
	order by dt_resultado;
	
Cursor C03 is
	select	ie_ajuste_dose
	from	rep_nivel_serico
	where	ie_risco in ('I', 'C')
	and		(((nr_seq_superior is null) and (nr_seq_regra_pai_w is null)) or 
			 (nr_seq_superior = nr_seq_regra_pai_w))
	and		qt_nivel_serico_w between qt_inicial and qt_final
	and		ie_paciente_dialise = ie_dialise_w
	order by nr_sequencia;
	
begin

if	(nr_sequencia_p is not null) and
	(ie_tipo_seq_p is not null) then
	
	if	(ie_tipo_seq_p = 'PROCESSO') then
		select	max(nr_prescricao)
		into	nr_prescricao_w
		from	adep_processo
		where	nr_sequencia = nr_sequencia_p;
	end if;
	
	if	(nr_prescricao_w is not null) then
	
		select	max(nr_atendimento),
				max(dt_inicio_prescr),
				nvl(max(ie_paciente_dialise), 'N')
		into	nr_atendimento_w,
				dt_inicio_vig_result_w,
				ie_dialise_w
		from	prescr_medica
		where	dt_suspensao is null
		and		nr_prescricao = nr_prescricao_w;
		
		if	(nr_atendimento_w is not null) then
			select	nvl(max('S'), 'N')
			into	ie_retorno_w
			from	prescr_material
			where	obter_se_mat_vancomicina(cd_material) = 'S'
			and		dt_suspensao is null
			and		ie_agrupador = 1
			and		nr_prescricao = nr_prescricao_w;
			
			if	(ie_retorno_w = 'S') then
				select	trunc(dt_inicio_vig_result_w) - round(nvl(max(qt_horas_proced), 168)/24)
				into	dt_inicio_vig_result_w
				from	rep_nivel_serico;

				-- Verifica se possui resultado de vancomicinemia/vancocinemia
				select	nvl(max('S'), 'N')
				into	ie_retorno_w
				from	rep_result_vanco
				where	dt_resultado > dt_inicio_vig_result_w
				and		ie_novo_resultado = 'S'
				and		nr_atendimento = nr_atendimento_w;
				
				if	(ie_retorno_w = 'S') then
					
					-- Busca novos resultados do atendimento
					open C02;
					loop
					fetch C02 into	
						qt_nivel_serico_w;
					exit when C02%notfound;
						begin
						
						-- Verificar qual regra assumida para o �ltimo resultado
						select	max(nr_seq_regra_resultado)
						into	nr_seq_regra_pai_w
						from	rep_result_vanco
						where	dt_resultado > dt_inicio_vig_result_w
						and		ie_novo_resultado = 'N'
						and		nr_atendimento = nr_atendimento_w
						and		dt_atualizacao = (	select	max(dt_atualizacao)
													from	rep_result_vanco
													where	dt_resultado > dt_inicio_vig_result_w
													and		ie_novo_resultado = 'N'
													and		nr_atendimento = nr_atendimento_w);
						
						-- Verificar se a regra assumida possui regras dependentes
						select	max(nr_seq_superior)
						into	nr_seq_regra_pai_w
						from	rep_nivel_serico
						where	qt_nivel_serico_w between qt_inicial and qt_final
						and		ie_paciente_dialise = ie_dialise_w
						and		nr_seq_superior = nr_seq_regra_pai_w;
						
						-- Buscar regras conforme resultado // Regras pais || ou || Regras filhas
						open C03;
						loop
						fetch C03 into	
							ie_ajuste_dose_w;
						exit when C03%notfound;
						
							if	(ie_ajuste_dose_w <> 'M') then
								return 'S';
							end if;
							
						end loop;
						close C03;
						
						end;
					end loop;
					close C02;
					
				end if;
			end if;
		end if;	
	end if;
end if;

return 'N';

end Consistir_acoes_vanco_nvs;
/