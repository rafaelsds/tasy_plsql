create or replace
function pls_obter_email_prest_endereco (	nr_seq_prestador_p	pls_prestador.nr_sequencia%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) return varchar2 is
						
ds_email_w		varchar2(4000);

Cursor c01 (	nr_seq_prestador_pc	pls_prestador.nr_sequencia%type,
		cd_estabelecimento_pc	pls_prestador.cd_estabelecimento%type) is
	select	cd_pessoa_fisica,
		cd_cgc,
		decode(ie_tipo_endereco, 'PFC', '2', 'PFA', '9', 'PFJ', '6', 'PFM', '5', 'PFP', '4', 'PFR', '1', 'PFV', '3') ie_tipo_endereco, -- 2 = Comercial | 9 = Adicional | 6 = Conjuge | 5 = Mae |  4 = Pai | 1 = Residencial | 3 = Responsavel
		ie_tipo_endereco ie_tipo_endereco_orig,
		nr_seq_tipo_compl_adic,
		nr_seq_compl_pf_tel_adic,
		nr_seq_compl_pj
	from	pls_prestador
	where	nr_sequencia 		= nr_seq_prestador_pc
	and	cd_estabelecimento	= cd_estabelecimento_pc;
	
Cursor c02 (	cd_pessoa_fisica_pc	pls_prestador.cd_pessoa_fisica%type,
		ie_tipo_endereco_pc	pls_prestador.ie_tipo_endereco%type) is
	select	ds_email
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_pc
	and	ie_tipo_complemento 	= ie_tipo_endereco_pc
	and 	ds_email is not null
	order by ds_email;
	
begin

for r_c01_w in c01 ( nr_seq_prestador_p , cd_estabelecimento_p ) loop
	if	(r_c01_w.cd_cgc is not null) then
		if	(r_c01_w.ie_tipo_endereco_orig = 'PJA') and (r_c01_w.nr_seq_compl_pj is not null) then
			select	max(ds_email)
			into	ds_email_w
			from	pessoa_juridica_compl
			where 	cd_cgc = r_c01_w.cd_cgc 
			and 	nr_sequencia = r_c01_w.nr_seq_compl_pj;
		end if;
		
		if	(ds_email_w is null) then
			ds_email_w	:= substr(obter_dados_pf_pj_estab(cd_estabelecimento_p, null, r_c01_w.cd_cgc, 'M'), 1, 255);
		end if;
	else
		if	(r_c01_w.ie_tipo_endereco = '9') and (r_c01_w.nr_seq_tipo_compl_adic is not null) then
			select	max(ds_email)
			into	ds_email_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica 	= r_c01_w.cd_pessoa_fisica
			and	ie_tipo_complemento	= 9	-- Adicional
			and	nr_seq_tipo_compl_adic	= r_c01_w.nr_seq_tipo_compl_adic;
			
		elsif	(r_c01_w.ie_tipo_endereco = '2') and (r_c01_w.nr_seq_compl_pf_tel_adic is not null) then
			select	max(ds_email)
			into	ds_email_w
			from	compl_pf_tel_adic
			where	nr_sequencia = r_c01_w.nr_seq_compl_pf_tel_adic;
		end if;
		
		if	(ds_email_w is null) then
			for r_c02_w in c02 ( r_c01_w.cd_pessoa_fisica , r_c01_w.ie_tipo_endereco ) loop
				ds_email_w	:= substr((ds_email_w || r_c02_w.ds_email || ';'), 1, 4000);
			end loop;
			
			ds_email_w	:= substr(ds_email_w, 1, length(ds_email_w) - 1);
		end if;
	end if;
end loop;

return	ds_email_w;

end pls_obter_email_prest_endereco;
/