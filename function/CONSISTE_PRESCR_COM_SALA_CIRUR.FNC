create or replace
function consiste_prescr_com_sala_cirur( 	nr_prescricao_p 	number,
						nr_SeqUnidAtend_p 	number,
						nr_evento_saida_sala_p number,
						ie_validar_sala_p varchar2)
				return varchar2 is

dt_entrada_unidade_w	date;
nr_atendimento_w	number(10,0);
nr_cirurgia_w		number(10,0);
nr_seq_interno_w 	number(10,0) := 0;
dt_termino_w		date;

begin

SELECT 	MAX(a.dt_entrada_unidade),
	MAX(a.nr_atendimento),
	MAX(a.nr_cirurgia),
	MAX(a.dt_termino)
into	dt_entrada_unidade_w,
	nr_atendimento_w,
	nr_cirurgia_w,
	dt_termino_w
FROM 	cirurgia a
WHERE 	a.nr_prescricao = nr_prescricao_p
and 	NOT EXISTS(	SELECT 	1
			FROM 	evento_cirurgia_paciente b
			WHERE 	a.nr_cirurgia = b.nr_cirurgia
			AND 	b.nr_seq_evento = nr_evento_saida_sala_p
			AND 	b.dt_inativacao IS NULL);

if	((dt_entrada_unidade_w is not null) and (nr_atendimento_w > 0) and (nr_SeqUnidAtend_p is not null)) then
	select	nvl(max(a.nr_seq_interno), 0)
	into 	nr_seq_interno_w
	from	atend_paciente_unidade a, unidade_atendimento b
	where	a.nr_atendimento 	= nr_atendimento_w
	and	a.dt_entrada_unidade 	= dt_entrada_unidade_w
	and 	b.nr_seq_interno 	= nr_SeqUnidAtend_p
	and 	a.cd_setor_atendimento 	= b.cd_setor_atendimento
	and 	a.cd_unidade_basica 	= b.cd_unidade_basica
	and 	a.cd_unidade_compl 	= b.cd_unidade_compl
	and 	dt_saida_unidade is null;
end if;

if (nr_seq_interno_w > 0) or ((nr_cirurgia_w > 0) and (dt_termino_w is null) and (nr_SeqUnidAtend_p is null)) then
	return 'S';
else
	return 'N';
end if;

end consiste_prescr_com_sala_cirur;
/