create or replace 
function obter_exam_lab_integr_mat_ind  (
					nr_prescricao_p		number,
					cd_setor_atendimento_p	number,
					cd_material_p		varchar2,
					nr_seq_grupo_p		number,
					ie_equipamento_p	varchar2,
					ie_opcao_p		varchar2,
					ie_separador_p		varchar2,
					nr_seq_GRUPO_IMP_P	number,
					NR_SEQ_LAB_P		varchar2,
					ie_status_envio_p	number,
					ds_sigla_p		varchar2,
					nr_seq_apresent_p	number,
					nr_seq_lote_externo_p	number)
					RETURN Varchar2 IS

ie_opcao_w				varchar2(255);
ie_status_atend_w			varchar2(2);
cd_procedimento_w			number(15);
nr_seq_exame_w				number(10);
cd_exame_w				varchar2(20);
cd_exame_integracao_w			varchar2(20);
Resultado_w	 			Varchar2(1000);
qt_linha_w				number(2) := 0;
ds_material_especial_w			varchar2(255);
nr_sequencia_w				number(10);

ds_aux_w				varchar2(100);
nr_seq_amostra_w			varchar2(2);
nm_exame_w				Varchar2(80);
ie_padrao_amostra_w			varchar2(5);	
nr_seq_material_w		number(10);

cursor  c01 is
	select		a.cd_procedimento,
			b.nr_seq_exame,
			b.cd_exame,
			nvl(b.cd_exame_integracao, b.cd_exame),
			max(a.nr_sequencia),
			b.nm_exame
	from	exame_lab_material d,
		material_exame_lab c,
		exame_laboratorio b,
		prescr_procedimento a,
		exame_lab_resultado k,
		exame_lab_result_item y
	where 1=1
	and	k.nr_seq_resultado = y.nr_seq_resultado
	and	k.nr_prescricao = a.nr_prescricao
	and	y.nr_seq_prescr = a.nr_sequencia
	  and a.nr_seq_exame		= b.nr_seq_exame
	  and a.nr_seq_exame		= d.nr_seq_exame
	  and d.nr_seq_material	= c.nr_sequencia
	  and a.nr_prescricao		= nr_prescricao_p
	  and nvl(a.cd_setor_atendimento,0)	= nvl(nvl(cd_setor_atendimento_p, a.cd_setor_atendimento),0)
	and	(y.nr_seq_material = nvl(nr_seq_material_w, y.nr_seq_material) or
		(c.cd_material_exame = cd_material_p and d.ie_prioridade = 888))
	--  and (a.cd_material_exame	= nvl(cd_material_p, a.cd_material_exame) or 
	--	(c.cd_material_exame	= cd_material_p and d.ie_prioridade = 888))
	  and ((nr_seq_GRUPO_IMP = nvl(nr_seq_GRUPO_IMP_P,nr_seq_GRUPO_IMP)) or (nr_seq_GRUPO_IMP is null))
	  and ((a.nr_seq_lab = nvl(NR_SEQ_LAB_P,a.nr_seq_lab)) or (ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
	  and b.nr_seq_grupo		= nvl(nr_seq_grupo_p, b.nr_seq_grupo)
	  and nvl(d.qt_coleta,1) >= to_number(nvl(ie_equipamento_p,'1'))
	  and a.ie_status_atend		= nvl(ie_status_atend_w, a.ie_status_atend)
	  and (a.nr_seq_lote_externo	= nvl(nr_seq_lote_externo_p, a.nr_seq_lote_externo) or (nr_seq_lote_externo is null))	
	  and (	(ds_material_especial_w = 'X' and a.ds_material_especial is null) or
	  	(a.ds_material_especial = ds_material_especial_w))
	group by 	a.cd_procedimento,
			b.nr_seq_exame,
			b.cd_exame,
			nvl(b.cd_exame_integracao, b.cd_exame),
			b.nm_exame
	order by 5;

BEGIN

/* Op��es
	CP	- C�digo Procedimento
	EX	- Exame
	CE	- C�digo Exame
	CI	- C�digo Integra��o
	CI3	- C�digo Integra��o com 3 d�gitos
	CI8	- C�digo Integra��o com 8 d�gitos
	NME3	- Nome do exame com 3 digitos
	CE8	- C�digo Exame com 8 d�gitos
*/

ie_opcao_w	:= ie_opcao_p;
resultado_w	:= '';

select nvl(max(ie_padrao_amostra),'PM')
into	ie_padrao_amostra_w
from lab_parametro;

if	(instr(ie_opcao_p, ';') > 0) then
	ie_opcao_w		:= substr(ie_opcao_p, 1, instr(ie_opcao_p, ';') - 1);
	ie_status_atend_w	:= substr(ie_opcao_p, instr(ie_opcao_p, ';') + 1, 2);
	ds_aux_w		:= replace(ie_opcao_p, ie_opcao_w || ';' || ie_status_atend_w,'');
	ds_aux_w		:= substr(ds_aux_w, 2, 255);
	ds_material_especial_w  := substr(ds_aux_w, 1, instr(ds_aux_w, ';') - 1);
	if	(instr(ds_aux_w, ';') = 0) then
		ds_material_especial_w := ds_aux_w;
	else
		nr_seq_amostra_w	:= substr(ds_aux_w, instr(ds_aux_w, ';') + 1, 255);
	end if;
end if;

ie_status_atend_w := nvl(ie_status_envio_p,ie_status_atend_w);


if	(ds_material_especial_w is null) then
	ds_material_especial_w := 'X';
end if;

if (cd_material_p is not null) then
	select	max(nr_sequencia)
	into	nr_seq_material_w
	from	material_exame_lab
	where	cd_material_exame = cd_material_p;
end if;

OPEN C01;
LOOP
FETCH C01 into	cd_procedimento_w,
			nr_seq_exame_w,
			cd_exame_w,
			cd_exame_integracao_w,
			nr_sequencia_w,
			nm_exame_w;
	EXIT WHEN C01%NOTFOUND;
	begin
	if	(ie_opcao_w = 'CP') then
		Resultado_w	:= Resultado_w || cd_procedimento_w || ie_separador_p;
	elsif	(ie_opcao_w = 'EX') then
		Resultado_w	:= Resultado_w || nr_seq_exame_w || ie_separador_p;
	elsif	(ie_opcao_w = 'CE') then
		Resultado_w	:= Resultado_w || cd_exame_w || ie_separador_p;
	elsif	(ie_opcao_w = 'CI') then
		Resultado_w	:= Resultado_w || cd_exame_integracao_w || ie_separador_p;
	elsif	(ie_opcao_w = 'CI3') then
		Resultado_w	:= Resultado_w || substr(cd_exame_integracao_w || '   ',1,3) || ie_separador_p;
	elsif	(ie_opcao_w = 'CI8') then
		if (nr_seq_apresent_p is not null) then
			select	Obter_Equipamento_Exame(nr_seq_exame,null,ds_sigla_p)
			into	cd_exame_integracao_w
			from	exame_laboratorio
			where nr_seq_superior = nr_seq_exame_w
			  and nr_seq_apresent = nr_seq_apresent_p;

			Resultado_w	:= Resultado_w || substr(cd_exame_integracao_w || '        ',1,8) || ie_separador_p;
		else
			Resultado_w	:= Resultado_w || substr(cd_exame_integracao_w || '        ',1,8) || ie_separador_p;
		end if;
	elsif	(ie_opcao_w = 'NME3') then
		Resultado_w	:= Resultado_w || substr(nm_exame_w || '   ',1,3) || ie_separador_p;
	elsif	(ie_opcao_w = 'CE8') then
		Resultado_w	:= Resultado_w || substr(cd_exame_w || '        ',1,8) || ie_separador_p;
	end if;

	end;
END LOOP;

RETURN trim(resultado_w || ' ');

END;
/