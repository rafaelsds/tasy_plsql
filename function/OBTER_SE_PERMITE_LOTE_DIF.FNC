create or replace
function obter_se_permite_lote_dif(	cd_material_p	number )
					return varchar2 is


cd_grupo_material_w	number(3,0);
cd_subgrupo_material_w	number(3,0);
cd_classe_material_w	number(5,0);
ie_permite_w		varchar2(1);
					
begin

select	max(cd_grupo_material),
	max(cd_subgrupo_material),
	max(cd_classe_material)
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v
where	cd_material = cd_material_p;

select	nvl(max('S'),'N')
into	ie_permite_w
from	regra_lote_fornec_sep
where	nvl(cd_material,nvl(cd_material_p,0))			=	nvl(cd_material_p,0)
and	nvl(cd_grupo_material,nvl(cd_grupo_material_w,0))	=	nvl(cd_grupo_material_w,0)
and	nvl(cd_subgrupo_material,nvl(cd_subgrupo_material_w,0))	=	nvl(cd_subgrupo_material_w,0)
and	nvl(cd_classe_material,nvl(cd_classe_material_w,0))	=	nvl(cd_classe_material_w,0);

return	ie_permite_w;

end obter_se_permite_lote_dif;
/