create or replace
function cus_obter_componente_ficha(	cd_estabelecimento_p	number,
					nr_ficha_tecnica_p	number,
					nr_seq_componente_p	number,
					ie_opcao_p		number)
					return varchar2 is

ds_retorno_w				varchar2(255);
cd_material_w				number(10);
cd_procedimento_w			number(15);
ie_origem_proced_w			number(10);
nr_seq_proc_interno_w			number(10);
nr_seq_exame_w				number(10);

begin
	begin
	select	cd_material,
		cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		nr_seq_exame
	into	cd_material_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		nr_seq_exame_w
	from	ficha_tecnica_componente
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	nr_ficha_tecnica	= nr_ficha_tecnica_p
	and	nr_seq_componente	= nr_seq_componente_p;
	exception when others then
		ds_retorno_w	:= '';
	end;

if	(ie_opcao_p = 1) then
	begin
	if	(cd_material_w is not null) then
		ds_retorno_w	:= cd_material_w;
	elsif	(cd_procedimento_w is not null) then
		ds_retorno_w	:= cd_procedimento_w;
	elsif	(nr_seq_proc_interno_w is not null) then
		ds_retorno_w	:= nr_seq_proc_interno_w;
	elsif	(nr_seq_exame_w is not null) then
		ds_retorno_w	:= nr_seq_exame_w;
	end if;
	end;
elsif	(ie_opcao_p = 2) then
	begin
	if	(cd_material_w is not null) then
		ds_retorno_w	:= substr(obter_desc_material(cd_material_w),1,255);
	elsif	(cd_procedimento_w is not null) then
		ds_retorno_w	:= substr(obter_descricao_procedimento(cd_procedimento_w, ie_origem_proced_w),1,255);
	elsif	(nr_seq_proc_interno_w is not null) then
		ds_retorno_w	:= substr(obter_desc_proc_interno(nr_seq_proc_interno_w),1,255);
	elsif	(nr_seq_exame_w is not null) then
		ds_retorno_w	:= substr(Obter_Desc_Exame_Lab(nr_seq_exame_w,null,null,null),1,255);
	end if;
	end;
end if;

return	ds_retorno_w;

end cus_obter_componente_ficha;
/