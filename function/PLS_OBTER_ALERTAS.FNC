create or replace
function pls_obter_alertas(nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
						   ie_evento_p				pls_alerta_evento.ie_evento%type)
 		    	return varchar2 is		
	
Cursor C01(nr_seq_segurado_pc number) is
	select	expressao_pck.obter_desc_expressao(623685) || ' ' || OBTER_NOME_PESSOA_FISICA(c.cd_pessoa_fisica,'') ||CHR(13) || a.ds_alerta ds_alerta,
		a.dt_inicio_vigencia
	from    pls_alerta         a,
		pls_alerta_evento  b,
		pls_segurado       c,
		pls_contrato       d
	where	a.nr_sequencia = b.nr_seq_alerta
	and	a.nr_seq_segurado  = c.nr_sequencia
	and	c.nr_seq_contrato  = d.nr_sequencia
	and	c.nr_sequencia     = nr_seq_segurado_pc
	and	b.ie_evento        = ie_evento_p
	and	a.ie_situacao      = 'A'
	and	sysdate	between trunc(nvl(a.dt_inicio_vigencia,sysdate),'dd') and nvl(a.dt_fim_vigencia,sysdate)
	union all
	select	expressao_pck.obter_desc_expressao(623685) || ' ' || OBTER_NOME_PESSOA_FISICA(c.cd_pessoa_fisica,'') ||CHR(13) || a.ds_alerta ds_alerta,
	a.dt_inicio_vigencia
	from 	pls_alerta              a,
			pls_alerta_evento  	b,
			pls_segurado		c,
			pls_intercambio     d
	where   a.nr_sequencia 		= b.nr_seq_alerta
	and     a.nr_seq_segurado 	= c.nr_sequencia
	and     c.nr_seq_intercambio 	= d.nr_sequencia
	and     c.nr_sequencia 		= nr_seq_segurado_pc
	and		c.nr_seq_intercambio is not null
	and		b.ie_evento 		= ie_evento_p
	and	sysdate	between trunc(nvl(a.dt_inicio_vigencia,sysdate),'dd') and nvl(a.dt_fim_vigencia,sysdate)
	union all
	select	expressao_pck.obter_desc_expressao(286142) || ' ' || d.nr_contrato || '   ' ||CHR(13)|| a.ds_alerta ds_alerta,
			a.dt_inicio_vigencia
	from	pls_alerta         a,
			pls_alerta_evento  b,
			pls_segurado       c,
			pls_contrato       d
	where	a.nr_sequencia      = b.nr_seq_alerta
	and		a.nr_seq_contrato   = d.nr_sequencia
	and		c.nr_seq_contrato   = d.nr_sequencia
	and		c.nr_sequencia      = nr_seq_segurado_pc
	and		b.ie_evento         = ie_evento_p
	and		a.ie_situacao       = 'A'
	and		sysdate  between trunc(nvl(a.dt_inicio_vigencia,sysdate),'dd') and nvl(a.dt_fim_vigencia,sysdate)
	order by dt_inicio_vigencia desc;
	
ds_aviso_w varchar2(4000);	

begin

	for r_C01_w in C01(nr_seq_segurado_p) loop
		begin
			ds_aviso_w := r_C01_w.ds_alerta || CHR(10) || CHR(10) || ds_aviso_w;
		end;
	end loop;

return	ds_aviso_w;

end pls_obter_alertas;
/
