create or replace
function far_obter_tributo_nota_item(nr_nota_fiscal_p		number,
			cd_material_p			number)
 		    	return number is

/*				
1 - Quando h� ICMS (indicar al�quota e valor)  / Produto isento de ICMS (deixar al�quota e valor zerados) ;
2 - Quando h� ST (Substitui��o Tribut�ria) e j� est� embutido no pre�o;
3 - Quando h� ST (Substitui��o Tribut�ria) e n�o est� embutido no pre�o.

Integra��o IMS 

*/

qt_nf_w			number(13);
qt_tributo_w		number(13);
cd_tritubo_w		number(6);
ie_tipo_tributo_w		Varchar2(15);
retorno_w			number(15):= 0;

begin

select	count(*)
into	qt_nf_w
from	nota_fiscal_item_trib f,
	nota_fiscal_item i
where	f.nr_sequencia = i.nr_sequencia		
and	f.nr_item_nf = i.nr_item_nf
and	f.nr_sequencia = nr_nota_fiscal_p
and	i.cd_material = cd_material_p;


if	(qt_nf_w > 0) then
	
	select	max(cd_tributo)
	into	cd_tritubo_w
	from	nota_fiscal_item_trib f,
		nota_fiscal_item i
	where	f.nr_sequencia = i.nr_sequencia		
	and	f.nr_item_nf = i.nr_item_nf
	and	f.nr_sequencia = nr_nota_fiscal_p
	and	i.cd_material = cd_material_p;
	
	select	count(*)
	into	qt_tributo_w
	from	tributo
	where	cd_tributo = cd_tritubo_w;
	
	if	(qt_tributo_w > 0) then
		
		select decode(ie_tipo_tributo,'ICMS',1,'ICMSST',3,0)
		into	retorno_w
		from	tributo
		where	cd_tributo = cd_tritubo_w;
		
	end if;
	
end if;

return	nvl(retorno_w,0);

end far_obter_tributo_nota_item;
/