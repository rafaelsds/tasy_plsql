create or replace
function hc_obter_procedimento (	nr_seq_paciente_hc_p		number,
					nr_seq_servico_p		number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);
ds_procedimento_w	varchar2(255);
ie_exibir_procedimentos_w		varchar2(1);

Cursor C01 is
	select	distinct substr(obter_desc_procedimento(b.cd_procedimento,b.ie_origem_proced),1,250)
	from	hc_paciente_servico a,
		hc_paciente_servico_proc b
	where	a.nr_sequencia	  = b.nr_seq_servico
	and	((ie_exibir_procedimentos_w = 'N')
		or (a.nr_seq_pac_home_care = nr_seq_paciente_hc_p))
	and	a.nr_seq_servico = nr_seq_servico_p;

begin
obter_param_usuario(867, 91, obter_perfil_ativo, Wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_exibir_procedimentos_w);

if (nvl(nr_seq_paciente_hc_p,0) > 0) then
	begin
	
	open C01;
	loop
	fetch C01 into	
		ds_procedimento_w;
	exit when C01%notfound;
		begin
		
		if (ds_retorno_w is null) then
			ds_retorno_w := ds_procedimento_w;
		else
			ds_retorno_w := substr(ds_retorno_w || ', ' || ds_procedimento_w,1,255);
		end if;
		
		end;
	end loop;
	close C01;
	end;
end if;

return	ds_retorno_w;

end hc_obter_procedimento;
/