Create or Replace
FUNCTION OBTER_SE_USUARIO_MOTIVO_DESC	(nr_seq_motivo_desc_p	number,
					nm_usuario_p		varchar2)
					RETURN		VarChar2 IS

qt_usuario_lib_w	number(10);
ie_usuario_lib_w	varchar2(1)	:= 'S';

BEGIN

select	count(*)
into	qt_usuario_lib_w
from	motivo_desc_usuario a
where	a.nr_seq_motivo	= nr_seq_motivo_desc_p;

if	(qt_usuario_lib_w > 0) then

	select	nvl(max('S'),'N')
	into	ie_usuario_lib_w
	from	motivo_desc_usuario a
	where	a.nr_seq_motivo		= nr_seq_motivo_desc_p
	and	a.nm_usuario_lib	= nm_usuario_p;

end if; 

RETURN ie_usuario_lib_w;

END OBTER_SE_USUARIO_MOTIVO_DESC;
/