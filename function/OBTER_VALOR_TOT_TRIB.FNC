create or replace
function obter_valor_tot_trib(	nr_titulo_p		number)
				return number is

ds_retorno_w		number(10,2);				

begin

select	nvl(sum(a.vl_imposto),0)
into	ds_retorno_w
from	tributo b,
	titulo_pagar_imposto a
where	a.cd_tributo		= b.cd_tributo
and	a.nr_titulo		= nr_titulo_p
and	a.ie_pago_prev		= nvl(null, a.ie_pago_prev);

return	ds_retorno_w;

end obter_valor_tot_trib;
/




