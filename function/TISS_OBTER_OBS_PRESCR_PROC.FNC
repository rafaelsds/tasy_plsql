create or replace
function TISS_OBTER_OBS_PRESCR_PROC
			(nr_prescricao_p	number,
			cd_procedimento_p	number,
			ie_origem_proced_p	number)
			return	varchar2 is

ds_observacao_w		varchar2(4000);
ds_retorno_w		varchar2(4000);			
	
cursor c01 is
select	distinct ds_observacao
from	prescr_procedimento
where	nr_prescricao		= nr_prescricao_p
and	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
	
begin

ds_retorno_w	:= null;
ds_observacao_w	:= null;

open C01;
loop
fetch C01 into	
	ds_observacao_w;
exit when C01%notfound;
	
	if	(ds_observacao_w is not null) then	
		if	(ds_retorno_w is null) then
			ds_retorno_w	:= substr(ds_observacao_w,1,3999);
		else
			ds_retorno_w	:= substr((ds_retorno_w||' - '||ds_observacao_w),1,3999);
		end if;
	end if;

end loop;
close C01;

return 	ds_retorno_w;

end;
/