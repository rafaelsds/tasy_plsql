create or replace
function obter_categoria_PCR( 	   ie_categoria_p	Varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);			
			
begin

if	(ie_categoria_p = 'M') then

	ds_retorno_w := obter_desc_expressao(293090);
	
elsif	(ie_categoria_p = 'E') then

	ds_retorno_w := obter_desc_expressao(311499);

elsif	(ie_categoria_p = 'F') then

	ds_retorno_w := obter_desc_expressao(311159);

else

	ds_retorno_w := obter_desc_expressao(327119);

end if;

return	ds_retorno_w;

end obter_categoria_PCR;
/