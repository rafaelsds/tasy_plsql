create or replace
FUNCTION FEC_Obter_Exames_Frasco       (nr_prescricao_p		number,
					nr_seq_frasco_p		number,
					nr_seq_material_p	number
					)
					RETURN Varchar2 IS
Resultado_w 		varchar2(4000);
cd_exame_w		varchar2(4000);		
					
CURSOR  C01 is
select	distinct
		b.cd_exame			
from	exame_lab_material d,
		material_exame_lab c,
		exame_laboratorio b,
		prescr_procedimento a,
		exame_lab_frasco e
where 	a.nr_seq_exame		= b.nr_seq_exame
and 	a.nr_seq_exame		= d.nr_seq_exame
and 	d.nr_seq_material	= c.nr_sequencia
and		b.nr_seq_exame 		= e.nr_seq_exame
and		nvl(e.ie_situacao,'A') = 'A'
and 	a.nr_prescricao		= nr_prescricao_p
and		e.nr_seq_frasco		= nr_seq_frasco_p
and		c.nr_sequencia	= nr_seq_material_p;
	
BEGIN

OPEN C01;
	LOOP
	FETCH 	C01 
	into	cd_exame_w;
	EXIT WHEN C01%NOTFOUND;
	Resultado_w := Resultado_w || cd_exame_w||', ';
END loop;
close c01;

RETURN trim(substr(Resultado_w,1,length(Resultado_w)-2));

END FEC_Obter_Exames_Frasco;
/