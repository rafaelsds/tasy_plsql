create or replace
function HD_obter_Chegada_Paciente (	cd_pessoa_fisica_p		varchar2,
										ie_pac_faltou_p	varchar2 default 'T')
					return				varchar2 is

dt_chegada_w		date;

begin

begin
select	max(dt_chegada)
into	dt_chegada_w
from	hd_prc_chegada
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and		dt_chegada between  trunc(sysdate) and fim_dia(sysdate)
and		((nvl(ie_pac_faltou,'N') = nvl(ie_pac_faltou_p,'N')) or (ie_pac_faltou_p = 'T'));  
exception
	when others then
	dt_chegada_w		:= null;
end;

return to_char(dt_chegada_w, 'dd/mm/yyyy hh24:mi:ss');

end HD_Obter_Chegada_Paciente;
/
