create or replace function wsuite_obter_usuario_info(
    cd_pessoa_fisica_p in varchar2,
    ie_option_p        in varchar2)
  return varchar2
is
  ds_retorno_w varchar2(255);
  /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  Finality:
  E    Retrieve user active email id
  N    Retrieve registered user name
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
  if (cd_pessoa_fisica_p is not null) then
    if(ie_option_p        = 'E') then
      select max(nm_usuario_web)
      into ds_retorno_w
      from wsuite_usuario
      where ie_situacao    = 'A'
      and cd_pessoa_fisica = cd_pessoa_fisica_p;
    elsif(ie_option_p      = 'N') then
      begin
        select nvl(obter_nome_pf(a.cd_pessoa_fisica), obter_desc_expressao(330784))
        into ds_retorno_w
        from wsuite_usuario a left outer join wsuite_solic_inclusao_pf b
        on b.nr_sequencia = a.nr_seq_inclusao_pf
        where (a.ie_situacao        = 'A' or b.ie_status    = 1)        
        and (trim(a.nm_usuario_web) = trim(cd_pessoa_fisica_p) or (trim(b.ds_email) = trim(cd_pessoa_fisica_p)));
        exception
        when NO_DATA_FOUND then
        ds_retorno_w := null;
      end;
    end if;
  end if;
return ds_retorno_w;
end wsuite_obter_usuario_info;
/