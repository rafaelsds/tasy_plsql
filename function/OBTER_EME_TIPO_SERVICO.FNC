CREATE OR REPLACE
FUNCTION Obter_eme_tipo_servico(  	nr_seq_contrato_p	Number,
					nr_seq_pessoa_p		Number)
			RETURN VARCHAR IS
				
ds_retorno_w		Varchar2(100);

BEGIN

select	max(c.ds_tipo_servico)
into	ds_retorno_w
from	eme_tipo_servico c,
	eme_pf_contrato b
where	b.nr_seq_contrato  = nr_seq_contrato_p
and	c.nr_sequencia     = b.nr_seq_tipo_servico
and	b.cd_pessoa_fisica = nr_seq_pessoa_p;
	
RETURN	ds_retorno_w;

END Obter_eme_tipo_servico;
/