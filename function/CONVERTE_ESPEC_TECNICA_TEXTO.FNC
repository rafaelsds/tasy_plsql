CREATE OR REPLACE 
FUNCTION converte_espec_tecnica_texto(nr_sequencia_p	number)
RETURN VARCHAR2 is

ds_texto_w	VARCHAR2(4000);
nr_seq_rtf_srtring_w		varchar2(50);

BEGIN

converte_rtf_string('select ds_espec_tecnica
		 from  material_espec_tecnica
		     where	nr_sequencia = :nr_sequencia_p', nr_sequencia_p, wheb_usuario_pck.get_nm_usuario, nr_seq_rtf_srtring_w );
select	ds_texto
into	ds_texto_w
from	tasy_conversao_rtf
where	nr_sequencia = nr_seq_rtf_srtring_w;

  
RETURN	ds_texto_w;

END;
/