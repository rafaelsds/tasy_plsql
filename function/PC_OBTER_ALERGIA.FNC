create or replace
function pc_obter_alergia(	cd_pessoa_fisica_p varchar2)
							return varchar2 is
			
ie_liberar_hist_saude_w		parametro_medico.ie_liberar_hist_saude%type;
ds_retorno_w			varchar(4000);


cursor c_alergias is
        select	distinct coalesce(
			v.ds_ficha_tecnica,
			v.ds_dcb,            
			v.ds_material,
			v.ds_dcb_mat,
			v.ds_classe_mat,
			v.ds_medic_nao_cad,
			v.ds_alergeno) ds_alergia
	from	(
			(	select	
					cd_pessoa_fisica,
					dt_liberacao,
					dt_fim,
					nr_seq_tipo,
					substr(obter_desc_dcb(a.nr_seq_dcb),1,80) ds_dcb,
					decode(a.ie_nega_alergias,'N',substr(obter_desc_alergeno(a.nr_seq_tipo),1,80),obter_desc_expressao(346740)) ds_alergeno,
					substr(obter_estrut_princ_ativo(a.nr_seq_ficha_tecnica,'P'),1,2000) ds_ficha_tecnica,	
					substr(obter_desc_material(a.cd_material),1,100) ds_material,
					substr(obter_desc_dcb_mat(a.cd_material,'D'),1,80) ds_dcb_mat,
					substr(obter_desc_classe_mat(a.cd_classe_mat),1,60) ds_classe_mat,
					substr(ds_medic_nao_cad,1,100) ds_medic_nao_cad
				from	nivel_seguranca_alerta b,
						paciente_alergia a
				where	a.nr_seq_nivel_seg = b.nr_sequencia(+)	
				and a.cd_pessoa_fisica = cd_pessoa_fisica_p	
				and ((ie_liberar_hist_saude_w = 'N') or (a.dt_liberacao is not null))
				and	a.dt_inativacao is null
				and	consiste_se_exibe_alergia(nr_seq_tipo)	= 'S'
				and	nvl(a.ie_alerta,'S')			= 'S'				
				and	(dt_fim is null or dt_fim > sysdate)
				and	nvl(a.ie_nega_alergias, 'N') = 'N'
				and	cd_pessoa_fisica = cd_pessoa_fisica_p)
	) v
	order	by ds_alergia;
		
begin

if (cd_pessoa_fisica_p is not null) then

	select	nvl(max(ie_liberar_hist_saude),'N')
	into	ie_liberar_hist_saude_w
	from	parametro_medico
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;	
	
	for r_c_alergias in c_alergias loop
		if (r_c_alergias.ds_alergia is not null)then
			ds_retorno_w := ds_retorno_w || r_c_alergias.ds_alergia || ', ';
		end if;
	end loop;

	if (substr((ds_retorno_w),1,length(ds_retorno_w)-2) is not null) then
		ds_retorno_w := substr((ds_retorno_w),1,length(ds_retorno_w)-2) || '.';
	else
		ds_retorno_w := substr((ds_retorno_w),1,length(ds_retorno_w)-2);
	end if;

end if;

return ds_retorno_w;

end pc_obter_alergia;
/