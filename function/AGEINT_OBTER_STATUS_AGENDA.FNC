create or replace
function ageint_obter_status_agenda(
					nr_seq_agenda_cons_p	number,	
					nr_seq_agenda_exame_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar(100)	:= 'X';	
nr_seq_agenda_cons_w	number(10);
nr_seq_agenda_exame_w	number(10);
		
begin
if 	(nr_seq_agenda_cons_p > 0) then
	select	nvl(max(ie_status_agenda),'X')
	into	ds_retorno_w
	from 	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_cons_p;
elsif 	(nr_seq_agenda_exame_p > 0) then
	select 	nvl(max(ie_status_agenda),'X')
	into	ds_retorno_w
	from 	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_exame_p;
end if;

return	ds_retorno_w;

end ageint_obter_status_agenda;
/