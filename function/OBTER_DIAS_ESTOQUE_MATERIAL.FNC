create or replace
function obter_dias_estoque_material(
			cd_estabelecimento_p		number,
			cd_material_p			number,
			nm_usuario_p	in varchar2 default 'Tasy')
 		    	return number is

qt_estoque_w			number(15,4) := 0;
qt_estoque_ww			number(15,4) := 0;
qt_emprestimo_w			number(15,4) := 0;
qt_consumo_mensal_w		number(15,4);
qt_dias_estoque_w		number(15);
cd_local_estoque_w		number(10);
dt_mesano_referencia_w	    	date;
ie_saldo_requisicao_w		varchar2(1);
ie_material_saldo_ressup_w	varchar2(1);
ie_consumo_ressup_w		varchar2(15);


cursor c01 is
select	distinct b.cd_local_estoque
from	local_estoque b,
	saldo_estoque a
where	cd_material = cd_material_p
and	dt_mesano_referencia >= dt_mesano_referencia_w
and	a.cd_local_estoque = b.cd_local_estoque
and	a.cd_estabelecimento = cd_estabelecimento_p
and	b.ie_proprio = 'S'
and	(substr(obter_se_considera_ressup(a.cd_estabelecimento,a.cd_local_estoque, a.cd_material),1,1) = 'S')
and not exists(	select	1
		from	w_loc_estoque_ressup x
		where	x.cd_local_estoque = a.cd_local_estoque
		and	x.nm_usuario = nm_usuario_p)
and	dt_mesano_referencia = (select	max(c.dt_mesano_referencia)
				from	saldo_estoque c
				where	c.cd_material = a.cd_material
				and	c.cd_estabelecimento = a.cd_estabelecimento
				and	c.cd_local_estoque = a.cd_local_estoque
				and	dt_mesano_referencia >= dt_mesano_referencia_w);

begin

select	pkg_date_utils.start_of(max(dt_mesano_vigente),'MONTH',0),
	nvl(max(ie_saldo_requisicao),'C')
into	dt_mesano_referencia_w,
	ie_saldo_requisicao_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_p;

select	nvl(max(ie_material_saldo_ressup), 'N'),
	nvl(max(ie_consumo_ressup),'S')
into	ie_material_saldo_ressup_w,
	ie_consumo_ressup_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;


/* Qt Estoque */

select	nvl(sum(qt_estoque),0)
into	qt_estoque_w
from	local_estoque b,
	saldo_estoque a
where	cd_material = cd_material_p
and	dt_mesano_referencia >= dt_mesano_referencia_w
and	a.cd_local_estoque = b.cd_local_estoque
and	a.cd_estabelecimento = cd_estabelecimento_p
and	b.ie_proprio = 'S'
and	(substr(obter_se_considera_ressup(a.cd_estabelecimento,a.cd_local_estoque, a.cd_material),1,1) = 'S')	
and not exists(	select	1
		from	w_loc_estoque_ressup x
		where	x.cd_local_estoque = a.cd_local_estoque
		and	x.nm_usuario = nm_usuario_p)
and	dt_mesano_referencia = (select	max(c.dt_mesano_referencia)
				from	saldo_estoque c
				where	c.cd_material = a.cd_material
				and	c.cd_estabelecimento = a.cd_estabelecimento
				and	c.cd_local_estoque = a.cd_local_estoque
				and	dt_mesano_referencia >= dt_mesano_referencia_w);

if	(nvl(ie_saldo_requisicao_w, 'C') = 'D') then
		
	qt_estoque_w	:= 0;
	qt_estoque_ww	:= 0;
		
	open C01;
	loop
	fetch C01 into	
		cd_local_estoque_w;
	exit when C01%notfound;
		begin
		qt_estoque_ww := obter_saldo_disp_estoque(
					cd_estabelecimento_p,
					cd_material_p,
					cd_local_estoque_w,
					pkg_date_utils.start_of(sysdate,'MONTH',0));
		qt_estoque_w := qt_estoque_w + qt_estoque_ww;
		end;
	end loop;
	close C01;		
end if;
	

if	(nvl(ie_saldo_requisicao_w, 'C') = 'E') then
	begin
	select	nvl(sum(qt_material),0)
	into	qt_emprestimo_w
	from	emprestimo c,
		emprestimo_material b			
	where	b.nr_emprestimo	= c.nr_emprestimo
	and	c.ie_tipo		= 'S'
	and	c.ie_situacao		<> 'I'
	and	c.dt_liberacao	is not null
	and	b.qt_material	> 0
	and	exists (
		select 1 from material a
		where	a.cd_material_estoque	= cd_material_p
		and	a.cd_material 		= b.cd_material);

	qt_estoque_w	:= qt_estoque_w	- qt_emprestimo_w;
	end;
end if;	

/* Consumo Mensal */

select	decode(ie_consumo_ressup_w,'N',c.qt_consumo_mensal_ressup,c.qt_consumo_mensal)
into	qt_consumo_mensal_w
from	material_estab c,
	material a
where	c.cd_material = a.cd_material
and	c.cd_estabelecimento = cd_estabelecimento_p
and	a.ie_situacao = 'A'
and	(nvl(c.ie_ressuprimento, 'S') = 'S')
and	a.cd_material = nvl(a.cd_material_estoque, a.cd_material)
and	a.cd_material = cd_material_p
and	((exists(
		select	1
		from	saldo_estoque b
		where	a.cd_material	= b.cd_material
		and	b.dt_mesano_referencia >= pkg_date_utils.add_month(dt_mesano_referencia_w, -3,0)
		and	b.cd_estabelecimento = cd_estabelecimento_p) and ie_material_saldo_ressup_w = 'N') or
	(ie_material_saldo_ressup_w = 'S'));

/* Qt Dias Estoque */

qt_dias_estoque_w := trunc(dividir(qt_estoque_w, dividir(qt_consumo_mensal_w, 30)));

return	qt_dias_estoque_w;

end obter_dias_estoque_material;
/