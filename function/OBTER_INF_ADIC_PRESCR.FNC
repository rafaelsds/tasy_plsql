create or replace
function obter_inf_adic_prescr(	nr_prescricao_p	number,
				nr_seq_item_p	number)
				return varchar2 is


cd_procedimento_w		number(15,0);
ie_origem_proced_w 		number(10,0);
nr_seq_exame_w			number(10,0);
nr_seq_proc_interno_w		number(10,0);
nr_seq_inf_adic_w		number(10,0);

cd_grupo_proc_w			number(15,0);
cd_especialidade_w		number(8,0);
cd_area_procedimento_w		number(8,0);
ie_tipo_w			varchar2(5);
nr_seq_classif_w		number(10,0);


cursor c01 is
	select	nr_seq_inf_adic
	from	rep_regra_inf_adic
	where	nvl(cd_procedimento,nvl(cd_procedimento_w,0))		=	nvl(cd_procedimento_w,0)
	and	nvl(ie_origem_proced,nvl(ie_origem_proced_w,0))		=	nvl(ie_origem_proced_w,0)
	and	nvl(nr_seq_exame,nvl(nr_seq_exame_w,0))			=	nvl(nr_seq_exame_w,0)
	and	nvl(nr_seq_proc_interno,nvl(nr_seq_proc_interno_w,0)) 	=	nvl(nr_seq_proc_interno_w,0)
	and	nvl(cd_grupo_proc,nvl(cd_grupo_proc_w,0))		=	nvl(cd_grupo_proc_w,0)
	and	nvl(cd_especialidade,nvl(cd_especialidade_w,0))		=	nvl(cd_especialidade_w,0)
	and	nvl(cd_area_procedimento,nvl(cd_area_procedimento_w,0)) =	nvl(cd_area_procedimento_w,0)
	and	nvl(ie_tipo,nvl(ie_tipo_w,0)) 				=	nvl(ie_tipo_w,0)
	and	nvl(nr_seq_classif,nvl(nr_seq_classif_w,0)) 		=	nvl(nr_seq_classif_w,0)
	order by nvl(cd_procedimento,0),
		nvl(nr_seq_exame,0),
		nvl(nr_seq_proc_interno,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0),
		nvl(ie_tipo,'A'),
		nvl(nr_seq_classif,0);

begin

select	cd_procedimento,
	ie_origem_proced,
	nr_seq_exame,
	nr_seq_proc_interno
into	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_exame_w,
	nr_seq_proc_interno_w
from	prescr_procedimento
where	nr_prescricao	=	nr_prescricao_p
and	nr_sequencia	=	nr_seq_item_p;

select	max(cd_grupo_proc),
	max(cd_especialidade),
	max(cd_area_procedimento)
into	cd_grupo_proc_w,
	cd_especialidade_w,
	cd_area_procedimento_w
from	estrutura_procedimento_v
where	cd_procedimento		=	cd_procedimento_w
and	ie_origem_proced	=	ie_origem_proced_w;

if	(nr_seq_proc_interno_w > 0) then
	select	max(ie_tipo),
		max(nr_seq_classif)
	into	ie_tipo_w,
		nr_seq_classif_w
	from	proc_interno
	where	nr_sequencia	=	nr_seq_proc_interno_w;
end if;

open C01;
loop
fetch C01 into	
	nr_seq_inf_adic_w;
exit when C01%notfound;
	begin
	nr_seq_inf_adic_w := nr_seq_inf_adic_w;
	end;
end loop;
close C01;

return	nr_seq_inf_adic_w;

end obter_inf_adic_prescr;
/