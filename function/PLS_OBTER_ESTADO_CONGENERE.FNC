create or replace
function pls_obter_estado_congenere(
					nr_seq_congenere_p	pls_congenere.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type)
 		    	return pessoa_juridica.sg_estado%type is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter a sigla do estado da Operadora congenere conforme sequ�ncia passada por par�metro
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Esta function � utilizada na gera��o das Ocorr�ncias, favor cuidar se for alterar para utilizar em outro lugar.

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjung OS 597885 - 04/05/2013 - 	Cria��o da function para obter o estado da congenere na 
			PLS_CONTA_INTERCAMBIO_V que � usada pelos filtros de
			interc�mbio da ocorr�ncia;
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
sg_estado_w	pessoa_juridica.sg_estado%type;
cd_cgc_w	pessoa_juridica.cd_cgc%type;

begin

if	(nr_seq_congenere_p is not null) then

	-- Obter o c�digo da congenere
	select	max(a.cd_cgc)
	into	cd_cgc_w
	from	pls_congenere a
	where	a.nr_sequencia		= nr_seq_congenere_p
	and	a.cd_estabelecimento	= cd_estabelecimento_p;
	
	-- S� buscar a informa��o do estado caso encontrar o dado da pessoa_juridica
	if	(cd_cgc_w is not null) then
		
		-- Obter o estador da congenere
		select	max(a.sg_estado)
		into	sg_estado_w
		from	pessoa_juridica a
		where	a.cd_cgc		= cd_cgc_w;
	end if;
end if;

return	sg_estado_w;

end pls_obter_estado_congenere;
/
