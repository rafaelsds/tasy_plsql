create or replace
function Obter_Valor_Total_RAT(	nr_rat_p 		NUMBER,
					cd_consultor_p 	NUMBER)
					return 		NUMBER is

qt_min_w	NUMBER(15,4) := 0;
vl_total_w	NUMBER(15,4) := 0;

begin

select	sum(qt_min_ativ)
into	qt_min_w
from	proj_rat_ativ
where	nr_seq_rat = nr_rat_p;

select	sum(dividir(qt_min_w, 60) * a.vl_hora)
into	vl_total_w
from	PROJ_CONSULTOR_AVAL a,
	com_cliente b,
	proj_rat c
where	a.nr_seq_cliente	= b.nr_sequencia
and	a.cd_consultor 	= cd_consultor_p
and	c.nr_seq_cliente 	= b.nr_sequencia
and	c.nr_sequencia 	= nr_rat_p;

return vl_total_w;

end Obter_Valor_Total_rat;
/