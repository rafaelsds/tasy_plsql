create or replace FUNCTION	Get_Patient_Current_Dept(
    nr_atendimento_p	NUMBER,
    cd_pessoa_fisica_p	NUMBER,
    ie_opcao_p			VARCHAR2,
    ie_informacao_p		VARCHAR2)
RETURN	VARCHAR2
IS
  ds_unidade_w				VARCHAR2(255);
  recent_nr_atendiemnto		NUMBER;
BEGIN
  IF	(nr_atendimento_p IS NOT NULL AND (NVL(nr_atendimento_p,0) > 0))	THEN
	BEGIN
      ds_unidade_w := SUBSTR(Obter_Unidade_Atendimento(nr_atendimento_p,ie_opcao_p,ie_informacao_p),1,255);
    END;
  elsif	(cd_pessoa_fisica_p IS NOT NULL AND (NVL(cd_pessoa_fisica_p,0) > 0))	THEN
	BEGIN
		SELECT MAX(nr_atendimento)
		INTO recent_nr_atendiemnto
		FROM atendimento_paciente_v
		WHERE cd_pessoa_fisica	= cd_pessoa_fisica_p
		AND dt_alta IS NULL;
		IF	(recent_nr_atendiemnto IS NOT NULL)	THEN
			SELECT ds_setor_atendimento || ' ' || cd_unidade
			INTO ds_unidade_w
			FROM atendimento_paciente_v
			WHERE cd_pessoa_fisica = cd_pessoa_fisica_p
			AND nr_atendimento     = recent_nr_atendiemnto;
		ELSE
			select obter_desc_expressao(798768) into ds_unidade_w from dual;
		END IF;
    END;
  END IF;
RETURN ds_unidade_w;
END Get_Patient_Current_Dept;
/