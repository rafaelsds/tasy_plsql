create or replace
function obter_estabelecimento_base(cd_pessoa_fisica_titular_p		varchar2)
 		    	return number is

cd_estabelecimento_base_w		number(10);
			
begin

select	max(b.cd_estabelecimento_base)
into	cd_estabelecimento_base_w
from	pessoa_fisica_loc_trab	a,
	setor_atendimento	b
where	a.cd_setor_atendimento	= b.cd_setor_atendimento
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_titular_p
and	a.ie_local_principal	= 'S';

return	cd_estabelecimento_base_w;

end obter_estabelecimento_base;
/