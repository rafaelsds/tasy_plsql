create or replace function obter_topografia_lib(nr_seq_topografia_p number) return varchar2 is



ds_retorno_w 		 varchar2(1) := 'S';

qt_reg_w	 		 number(10);

cd_estabelecimento_w number(10);

cd_empresa_w		 number(10);



begin



if (nr_seq_topografia_p > 0) then



	select 	count(*)

	into	qt_reg_w

	from	topografia_dor_liberacao

	where 	nr_seq_topografia = nr_seq_topografia_p

	and 	nvl(ie_situacao,'A') = 'A';



	if (qt_reg_w > 0)  then

	

		cd_estabelecimento_w := obter_estabelecimento_ativo;
		
		cd_empresa_w := obter_empresa_estab(cd_estabelecimento_w);

		select 	nvl(max('S'),'N')

		into	ds_retorno_w

		from	topografia_dor_liberacao

		where 	nr_seq_topografia = nr_seq_topografia_p

		and 	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
		
		and		nvl(cd_empresa,cd_empresa_w) = cd_empresa_w

		and 	nvl(ie_situacao,'A') = 'A'
		
		and 	(ie_visualizar is null or ie_visualizar = 'S');

	

	end if;	

	

end if;



return ds_retorno_w;



end obter_topografia_lib;
/