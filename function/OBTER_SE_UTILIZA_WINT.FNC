create or replace
function obter_se_utiliza_wint	(	cd_convenio_p		number,
					cd_estabelecimento_p	number)
 		    	return varchar2 is
ds_retorno_w 	varchar2(1) := 'N';
begin

if ((nvl(cd_convenio_p,0) > 0) and (nvl(cd_estabelecimento_p,0) > 0)) then
	
	begin
		select 	'S'
		into	ds_retorno_w
		from 	convenio_estabelecimento b
		where 	b.cd_convenio = cd_convenio_p
		and	b.cd_estabelecimento = cd_estabelecimento_p
		and	nvl(b.ie_utiliza_wint,'N') = 'S'
		and	rownum = 1;
		
	exception
	when others then
		ds_retorno_w := 'N';
	end;
	
end if;
return	ds_retorno_w;

end obter_se_utiliza_wint;
/
