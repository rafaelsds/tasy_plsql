create or replace
function man_obter_se_os_problema(nr_seq_os_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(01) := 'N';

begin
begin
select	'S'
into	ds_retorno_w
from	man_ordem_serv_problema
where	nr_seq_ordem_serv = nr_seq_os_p
and	rownum < 2;
exception
when others then
	ds_retorno_w := 'N';
end;
return	ds_retorno_w;

end man_obter_se_os_problema;
/