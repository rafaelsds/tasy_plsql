Create or Replace
Function obter_Senha_Letra_Numero(qt_digito_p	Number)
			return varchar2 is

ds_senha_w		varchar2(20);
ds_letra_w		Varchar2(100);
ds_numero_w		Varchar2(100);
ds_adic_w		Varchar2(20);
qt_seg_w		Number(15,0);		
qt_rand_w		Number(15,0);
qt_base_w		Number(15,0);
qt_adic_w		Number(15,0);

BEGIN

if	(qt_digito_p > 20) then
	Wheb_mensagem_pck.exibir_mensagem_abort(279608);
end if;	
ds_letra_w		:= 'ABCDEFGHIJKLMNOPQRSTUVXWYABCDEFGHIJKLMNOPQRSTUVXWYABCDEFGHIJKLMNOPQRSTUVXWYABCDEFGHIJKLMNOPQRSTUVXWY';
ds_numero_w		:= '0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789';
ds_adic_w		:= '07749676749584025423';
qt_seg_w		:= campo_numerico(to_char(sysdate,'ss'));
qt_rand_w		:= substr(obter_numero_randomico,1,2);

FOR i IN 1..qt_digito_p LOOP 
	qt_adic_w	:= campo_numerico(substr(ds_adic_w,i,1));
	if	((i / 2) = trunc(i/2)) then
		qt_base_w	:= qt_seg_w + qt_adic_w;
	else
		qt_base_w	:= qt_rand_w + qt_adic_w;
	end if;

	if	(qt_base_w < 1) or
		(qt_base_w > 99) then
		qt_base_w	:= qt_seg_w;
	end if;	
	qt_adic_w		:= qt_adic_w + qt_seg_w;
	if	((qt_adic_w  / 2) = trunc(qt_adic_w/2)) then
		ds_senha_w		:= ds_senha_w	 || substr(ds_letra_w,qt_base_w,1);
	else
		ds_senha_w		:= ds_senha_w	 || substr(ds_numero_w,qt_base_w,1);
	end if;
END LOOP;

return ds_senha_w;

END;
/