create or replace function fis_efd_icmsipi_elimina_c(ds_campo_p varchar2)
  return varchar2 is

	/* Tratamento de string do EFD -ICMSIPI */

	ds_retorno_w  varchar2(255);
	cd_ascii_w    Integer;
	i             Integer;
	ds_campo_w	varchar2(255);

begin
/* Regra Geral da EFD - ICMSIPI */
-- Para campos alfanum�ricos, representados por "C", podem ser usados todos os caracteres da Tabela ASCII,
-- exceto os caracteres "|" (Pipe, c�digo 124 da Tabela ASCII) e os n�o-imprim�veis (caracteres 00 a 31 da Tabela ASCII).

ds_campo_w:=	substr(ds_campo_p,1,255);

begin
/* Varre toda a string caracter a caracter*/
for i in 1 .. length(ds_campo_w) loop

	/* Busca o valor da tabela ascii do caracter */
	cd_ascii_w := ASCII(substr(ds_campo_w, i, 1));

	/* Valida��o de regra*/
	if (cd_ascii_w >= 48 and cd_ascii_w <= 57) or
	(cd_ascii_w >= 65 and cd_ascii_w <= 90) or
	(cd_ascii_w >= 97 and cd_ascii_w <= 122) then

	/* Adiciona caracter a string de retorno*/
	ds_retorno_w := ds_retorno_w || substr(ds_campo_w, i, 1);

end if;
end loop;
end;

return trim(ds_retorno_w);

end fis_efd_icmsipi_elimina_c;
/
