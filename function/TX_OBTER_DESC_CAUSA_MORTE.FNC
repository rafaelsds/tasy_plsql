create or replace
function tx_obter_desc_causa_morte(	nr_seq_causa_morte_p	number)
				return			varchar2 is
					
ds_retorno_w		varchar2(100);

begin

if	(nr_seq_causa_morte_p is not null) then
	select	ds_causa
	into	ds_retorno_w
	from	causa_morte
	where	nr_sequencia = nr_seq_causa_morte_p;
end if;

return	ds_retorno_w;
	
end tx_obter_desc_causa_morte;
/