create or replace
function seq_regulacao_home_care(nr_sequencia_p		number)
 		    	return number is


nr_sequencia_w		number;
			
begin

	Select  nvl(max(a.nr_sequencia),'')
	into 	nr_sequencia_w
	from	regulacao_atend a,
			paciente_home_care b
	where	a.nr_seq_home_care = b.nr_sequencia
	and		b.nr_sequencia = nr_sequencia_p;

	return	nr_sequencia_w;

end seq_regulacao_home_care;
/