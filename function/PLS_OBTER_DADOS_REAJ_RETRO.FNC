create or replace
function pls_obter_dados_reaj_retro
			(	nr_seq_reaj_cobr_retro_p	number,
				ie_opcao_p			varchar2)
 		    	return varchar2 is

/*	ie_opcao_p
	R	Sequ�ncia do reajuste
*/

ds_retorno_w		varchar2(255);

begin

if	(ie_opcao_p = 'R') then
	select	max(nr_seq_reajuste)
	into	ds_retorno_w
	from	pls_reajuste_cobr_retro
	where	nr_sequencia	= nr_seq_reaj_cobr_retro_p;
end if;

return	ds_retorno_w;

end pls_obter_dados_reaj_retro;
/