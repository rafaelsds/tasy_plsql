create or replace
function	hd_obter_unid_dialise_data	(cd_pessoa_fisica_p	varchar2,
						dt_referencia_p		date)
						return number is
						
nr_seq_unid_dialise_origem_w	number(10);
nr_seq_unid_dialise_atual_w	number(10);
nr_seq_unid_dialise_w		number(10);
nr_seq_escala_dialise_w		number(10);

begin

select  max(nr_seq_unid_dialise)
into	nr_seq_unid_dialise_origem_w
from	hd_pac_renal_cronico
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

select 	max(nr_sequencia)
into	nr_seq_escala_dialise_w
from   	hd_escala_dialise
where  	dt_inicio <= dt_referencia_p
and	cd_pessoa_fisica = cd_pessoa_fisica_p
and	nvl(dt_fim,dt_referencia_p) >= dt_referencia_p;

nr_seq_unid_dialise_atual_w	:= null;

if	(nr_seq_escala_dialise_w > 0) then

	select 	nr_seq_unid_dialise
	into	nr_seq_unid_dialise_atual_w
	from   	hd_escala_dialise
	where	nr_sequencia = nr_seq_escala_dialise_w;
	

end if;

nr_seq_unid_dialise_w	:= nr_seq_unid_dialise_origem_w;

if	(nr_seq_unid_dialise_atual_w is not null) then
	nr_seq_unid_dialise_w	:= nr_seq_unid_dialise_atual_w;
end if;

return nr_seq_unid_dialise_w;

end;
/