create or replace
function Proj_obter_cronograma_etapa(nr_seq_etapa_p	number)
 	    	return number is

nr_sequencia_w	number(10,0);

begin

select	max(b.nr_sequencia)
into	nr_sequencia_w
from	proj_cronograma b,
	proj_cron_etapa a
where	a.nr_sequencia = nr_seq_etapa_p
and	a.nr_seq_cronograma = b.nr_sequencia;

return	nr_sequencia_w;

end Proj_obter_cronograma_etapa;
/