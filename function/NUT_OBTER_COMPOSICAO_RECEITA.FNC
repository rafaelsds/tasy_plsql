create or replace
function Nut_obter_composicao_receita(	nr_seq_receita_p	Number)
 		    	return Number is

nr_seq_composicao_w	Number(10);
			
begin
if	(nr_seq_receita_p is not null) then

	select	max(nr_seq_composicao)
	into	nr_seq_composicao_w
	from	nut_receita
	where	nr_sequencia = nr_seq_receita_p;

end if;

return	nr_seq_composicao_w;

end Nut_obter_composicao_receita;
/