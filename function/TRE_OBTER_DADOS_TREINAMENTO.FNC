CREATE OR REPLACE FUNCTION TRE_OBTER_DADOS_TREINAMENTO(	nr_seq_curso_p	number,
							ie_opcao_p	varchar2,
							ie_cod_desc_p	varchar2)
Return Varchar2 IS

nr_seq_tipo_w	Number;
ds_tipo_w	Varchar2(80);
BEGIN
if	(ie_opcao_p = 'TT') then
	select	a.nr_sequencia,
		a.ds_tipo
	into	nr_seq_tipo_w,
		ds_tipo_w
	from	tre_tipo a,
		tre_curso b
	where	a.nr_sequencia = b.nr_seq_tipo
	and	b.nr_sequencia = nr_seq_curso_p;

	if	(ie_cod_desc_p = 'C') then
		return nr_seq_tipo_w;
	else
		return ds_tipo_w;
	end if;
end if;

END TRE_OBTER_DADOS_TREINAMENTO;
/