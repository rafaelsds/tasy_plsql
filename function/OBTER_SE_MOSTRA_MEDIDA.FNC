create or replace
function Obter_Se_mostra_medida(cd_protocolo_p		number,
				nr_seq_tipo_medida_p	number)
 		    	return varchar2 is

ie_mostrar_w	varchar2(1) := 'S';
cont_w		number(10,0);

begin

select	count(*)
into	cont_w
from	regra_laudo_medida
where	cd_protocolo	= cd_protocolo_p;

if	(cont_w > 0) then

	select	count(*)
	into	cont_w
	from	regra_laudo_medida
	where	cd_protocolo	= cd_protocolo_p
	and	nr_seq_tipo_medida = nr_seq_tipo_medida_p;
	
	if	(cont_w = 0) then
		ie_mostrar_w	:= 'N';
	end if;
	
end if;

return	ie_mostrar_w;

end Obter_Se_mostra_medida;
/