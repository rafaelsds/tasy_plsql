create or replace
function sup_obter_saldo_disp_proj_rec(	nr_seq_projeto_p			number)
 		    	return number is

vl_saldo_w		projeto_recurso.vl_inicio_projeto%type;
vl_inicio_projeto_w	projeto_recurso.vl_inicio_projeto%type;
ie_param_25_w		varchar2(1) := 'N';
nr_seq_projeto_w	projeto_recurso.nr_sequencia%type;

begin
/*
31/07/2015	100.000,00 valor do recurso do projeto
01/08/2015	10.000,00 numa solicita��o de compras
01/08/2015	90.000,00 saldo dispon�vel (nesse momento se tentar emitir uma ordem de compra ou uma solicita��o com valor maior que 90.000,00 dever� bloquear).
03/08/2015	a solicita��o de 10.000,00 virou uma cota��o de 9.500,00.
03/08/2015	9.500,00 numa ordem de compra (daquela solicita��o de 10.000,00).
03/08/2015	90.500,00 de saldo dispon�vel.

31/07/2015	100.000,00 valor do recurso do projeto
01/08/2015	10.000,00 numa solicita��o de compras
01/08/2015	90.000,00 saldo dispon�vel (nesse momento se tentar emitir uma ordem de compra ou uma solicita��o com valor maior que 90.000,00 dever� bloquear).
02/08/2015	90.000,00 nova solic compra
02/08/2015	ZERO dispon�vel
03/08/2015	a solicita��o de 10.000,00 virou uma cota��o de 11.000,00.
03/08/2015	11.000,00 numa ordem de compra (daquela solicita��o de 10.000,00). N�o ir� permitir.
03/08/2015	porque tem ZERO de saldo dispon�vel.*/

select	nvl(obter_valor_param_usuario(928, 25, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'N')
into	ie_param_25_w
from	dual;

ie_param_25_w := 'N';

if	(ie_param_25_w = 'N') then
	select	nvl(sum(vl_inicio_projeto),0) vl_inicio_projeto
	into	vl_inicio_projeto_w
	from	projeto_recurso
	where	nr_sequencia = nr_seq_projeto_p;
elsif	(ie_param_25_w = 'S') then

	select	nvl(nr_seq_superior,0)
	into	nr_seq_projeto_w
	from	projeto_recurso
	where	nr_sequencia = nr_seq_projeto_p;

	if	(nr_seq_projeto_w = 0) then
		nr_seq_projeto_w := nr_seq_projeto_p;
	end if;

	select	nvl(sum(vl_inicio_projeto),0) vl_inicio_projeto
	into	vl_inicio_projeto_w	
	from	projeto_recurso
	where	(nr_sequencia = nr_seq_projeto_w or nr_sequencia in (select nr_sequencia from projeto_recurso where nr_seq_superior = nr_seq_projeto_w));
end if;

return	vl_inicio_projeto_w;

end sup_obter_saldo_disp_proj_rec;
/
