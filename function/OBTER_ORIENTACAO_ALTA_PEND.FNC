create or replace
function Obter_orientacao_alta_pend	(	nr_atendimento_p	number)
					return	varchar2 is

ds_retorno_w		varchar2(2) := 'N';
qt_registro_w		Number(10)  := 0;
dt_alta_w		Date;

begin

select	max(dt_alta)
into	dt_alta_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;


if	(dt_alta_w is null) then

	select  count(*)
	into	qt_registro_w	
	from    atendimento_anot_enf a
	where   a.nr_atendimento = nr_atendimento_p
	and	a.ie_pendente = 'S'
	and	a.dt_inativacao is null;

end if;
	
if	( qt_registro_w > 0 ) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end Obter_orientacao_alta_pend;
/