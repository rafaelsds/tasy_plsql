create or replace 
function TISS_OBTER_TIPO_CONSULTA
		(nr_atendimento_p number) 
		return number is

nr_seq_classif_atend_w	number(10);
cd_convenio_w		number(5);
nr_atend_original_w	number(15);
ie_tipo_consulta_tiss_w	number(10);
cd_estabelecimento_w	number(4);


cursor 	c01 is 
select	nvl(IE_TIPO_CONSULTA_TISS,0)
from	tiss_regra_tipo_consulta
where	cd_estabelecimento					= cd_estabelecimento_w
and	nvl(nr_seq_classif_atend, nr_seq_classif_atend_w)	= nr_seq_classif_atend_w
and 	nvl(cd_convenio, cd_convenio_w)				= cd_convenio_w
and	((nvl(ie_atend_retorno, 'T') = 'T') or
	 ((nr_atend_original_w is null) and (nvl(ie_atend_retorno, 'T') = 'N')) or
	 ((nr_atend_original_w is not null) and (nvl(ie_atend_retorno, 'T') = 'R')))
order 	by nvl(cd_convenio,0),
	nvl(nr_seq_classif_atend, 0);

begin

select	MAX(a.nr_seq_classificacao),
	MAX(b.cd_convenio),
	MAX(a.nr_atend_original),
	max(a.cd_estabelecimento)
into	nr_seq_classif_atend_w,
	cd_convenio_w,
	nr_atend_original_w,
	cd_estabelecimento_w
from	atend_categoria_convenio b,
	atendimento_paciente a
where	a.nr_atendimento	= b.nr_atendimento
and	b.nr_seq_interno	= obter_Atecaco_atendimento(a.nr_atendimento)
and	a.nr_atendimento	= nr_atendimento_p;

open c01;
loop
fetch c01 into 
	ie_tipo_consulta_tiss_w;
exit when c01%notfound;
end loop;
close c01;

return ie_tipo_consulta_tiss_w;

end TISS_OBTER_TIPO_CONSULTA;
/