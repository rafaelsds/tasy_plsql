create or replace
function obter_med_dominio_concatenado(nr_seq_dominio_p		NUMBER)
 		    	return VARCHAR2 is

ds_valor_dominio_w			VARCHAR2(255) := '';
ds_valor_dominio_ww			VARCHAR2(255);

cursor C01 is
select	ds_valor_dominio
from	med_valor_dominio
where	nr_seq_dominio = nr_seq_dominio_p
order by 1;

begin

open C01;
	loop
	fetch c01 into	ds_valor_dominio_ww;
	exit when c01%notfound;
		begin
		ds_valor_dominio_w := ds_valor_dominio_w||ds_valor_dominio_ww||';';
		end;
	end loop;
	close c01;

return	ds_valor_dominio_w;

end obter_med_dominio_concatenado;
/