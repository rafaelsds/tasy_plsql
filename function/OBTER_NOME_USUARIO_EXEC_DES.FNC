CREATE OR REPLACE 
FUNCTION OBTER_NOME_USUARIO_EXEC_des 
					(NR_ORDEM_SERVICO_P 		NUMBER,
					ie_tipo_p					varchar2)
					RETURN VARCHAR2 IS

nm_usuario_exec_w	varchar2(15);
nr_seq_exec_w		number(10,0);
nm_usuario_w		varchar2(80);
ds_resultado_w		varchar2(80);
cd_pessoa_fisica_w	varchar2(30);
cd_perfil_ativo_w		number(5) := obter_perfil_ativo;

BEGIN

if 	(NR_ORDEM_SERVICO_P is not null) then
	begin
	select nvl(max(nm_usuario_exec), 'N�o Informado')
	into	nm_usuario_exec_w
	from	usuario_grupo_des b,
			man_ordem_servico a
	where	a.nr_sequencia 	= nr_ordem_servico_p
	and		a.nm_usuario_exec = b.nm_usuario_grupo;


	select nvl(max(a.nr_sequencia),0)
	into	nr_seq_exec_w
	from	usuario_grupo_des c,
			man_ordem_servico_exec a,
		man_ordem_servico b
	where	a.nr_seq_ordem = b.nr_sequencia
	and	a.nr_seq_ordem 	= nr_ordem_servico_p
	and		a.nm_usuario_exec = c.nm_usuario_grupo
	and  	b.nr_seq_estagio in 
		(select nr_seq_estagio 
 		from  	man_estagio_usuario  
 		where   nm_usuario_acao = a.nm_usuario_exec
		and	decode(nvl(cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),cd_perfil) =
			decode(nvl(cd_perfil_ativo_w,0),0,decode(nvl(cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),cd_perfil),cd_perfil_ativo_w));


	if 	(nr_seq_exec_w <> 0) then
		begin
		select nm_usuario_exec
		into	nm_usuario_w
		from	man_ordem_servico_exec
		where	nr_seq_ordem 	= nr_ordem_servico_p
		and	nr_sequencia	= nr_seq_exec_w;
		
		ds_resultado_w	:= nm_usuario_w;
		end;
	else

		begin

		select nvl(max(a.nr_sequencia),0)
		into	nr_seq_exec_w
		from	usuario_grupo_des c,
			man_ordem_servico_exec a,
			man_ordem_servico b
		where	a.nr_seq_ordem = b.nr_sequencia
		and		a.nm_usuario_exec = c.nm_usuario_grupo
		and	a.nr_seq_ordem 	= nr_ordem_servico_p;

		if 	(nr_seq_exec_w <> 0) then
			begin
			select nm_usuario_exec
			into	nm_usuario_w
			from	man_ordem_servico_exec
			where	nr_seq_ordem 	= nr_ordem_servico_p
			and	nr_sequencia	= nr_seq_exec_w;
		
			ds_resultado_w	:= nm_usuario_w;
			end;
		else
			ds_resultado_w	:= nm_usuario_exec_w;	

		end if;
		end;
		
	end if;
	end;
end if;

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	usuario
where	nm_usuario		= DS_RESULTADO_W;

if	(ie_tipo_p		= 'N') then
	RETURN cd_pessoa_fisica_w;
else
	RETURN DS_RESULTADO_W;
end if;

END OBTER_NOME_USUARIO_EXEC_des;
/