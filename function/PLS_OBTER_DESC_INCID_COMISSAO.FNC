create or replace
function pls_obter_desc_incid_comissao
			(	ie_acao_contrato_p		varchar2)
				return varchar2 is

ds_retorno_w			varchar2(255);
ds_lista_w			varchar2(15);
pos_virgula_w			number(5);
ie_acao_w			varchar2(5);

begin

ds_lista_w	:= ie_acao_contrato_p || ',';

while (ds_lista_w is not null) loop 
	begin
	
	select  instr(ds_lista_w,',')
	into    pos_virgula_w
	from    dual;
	
	if	(pos_virgula_w > 0) then
		ie_acao_w	:= substr(ds_lista_w,1,pos_virgula_w-1);
		
		if	(ie_acao_w is not null) then
			ds_lista_w	:= substr(ds_lista_w,pos_virgula_w+1,length(ds_lista_w));
			ds_retorno_w	:= ds_retorno_w || obter_valor_dominio(2115,ie_acao_w) || ', ';
		else
			ds_lista_w	:= null;
		end if;
	end if;
	
	end;
end loop;

return	substr(ds_retorno_w,1,length(ds_retorno_w)-2);

end pls_obter_desc_incid_comissao;
/