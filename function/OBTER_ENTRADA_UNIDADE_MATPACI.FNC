create or replace
function obter_entrada_unidade_matpaci(nr_seq_matpaci_p	number)
 		    	return  date is

dt_entrada_unidade_w	date;
			
			
begin
begin
if	(nvl(nr_seq_matpaci_p,0) > 0) then
	select 	dt_entrada_unidade
	into	dt_entrada_unidade_w
	from	material_atend_paciente
	where	nr_sequencia = nr_seq_matpaci_p;
end if;
exception
when others then
	dt_entrada_unidade_w := sysdate;
end;

return	dt_entrada_unidade_w;

end obter_entrada_unidade_matpaci;
/



