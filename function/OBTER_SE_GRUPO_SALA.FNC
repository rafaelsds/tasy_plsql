create or replace
function obter_se_grupo_sala(nr_seq_local_p		number,
			     nr_seq_agrupamento_p	number)
 		    	return varchar2 is

ie_agrupamento_w	varchar2(1);
begin
select	nvl(max('S'),'N') 
into	ie_agrupamento_w
from	qt_local
where 	nr_seq_grupo_quimio = nr_seq_agrupamento_p
and	nr_sequencia = nr_seq_local_p;

return	ie_agrupamento_w;

end obter_se_grupo_sala;
/