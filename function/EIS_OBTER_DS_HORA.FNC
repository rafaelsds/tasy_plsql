create or replace
function eis_obter_ds_hora(ie_valor_p		varchar2)
 		    	return varchar2 is

ds_retorno_w varchar2(255);

begin

select	decode(ie_valor_p, '00:00',0,'01:00',1,'02:00',2,'03:00',3,'04:00',4,'05:00',5,'06:00',6,'07:00',7,'08:00',8,'09:00',9,'10:00',10,'11:00',11,'12:00',12,'13:00',13,'14:00',14,'15:00',15,'16:00',16,'17:00',17,'18:00',18,'19:00',19,'20:00',20,'21:00',21,'22:00',22,'23:00',23)
into	ds_retorno_w
from dual;

return	ds_retorno_w;

end eis_obter_ds_hora;
/