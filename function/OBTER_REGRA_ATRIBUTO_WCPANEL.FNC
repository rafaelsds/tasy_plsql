create or replace
function obter_regra_atributo_wcpanel (
		nr_seq_wcpanel_p	number,
		cd_perfil_p		number,
		nm_usuario_p		varchar2,
		ie_mmed varchar2 default null)
		return varchar2 is

nr_seq_atributo_w	number(10,0);
ie_atributo_w		varchar2(1);
ds_regra_wcpanel_w	varchar2(3000);
ie_configuravel_w	varchar2(1);
ds_titulo_w		varchar2(50);

cursor c01 is
select	a.nr_sequencia,
	a.ie_configuravel
from 	dic_objeto a
where 	a.nr_seq_obj_sup = nr_seq_wcpanel_p
and	a.ie_tipo_objeto = 'AC'
and 	a.ie_configuravel in ('S','P','R')
order by
	a.nr_seq_apres;
	
cursor c02 is
select	ie_exibir_atributo,
	ds_titulo
from	wcp_regra_atributo
where	nr_seq_wcpanel = nr_seq_wcpanel
and	nr_seq_atributo = nr_seq_atributo_w
and	nvl(nm_usuario_regra,nm_usuario_p) = nm_usuario_p
and	nvl(cd_perfil,cd_perfil_p) = cd_perfil_p
order by
	nvl(nm_usuario_regra,'AAAAAAAAAAAAAA'),
	nvl(cd_perfil,0);
	
begin
if	(nr_seq_wcpanel_p is not null) and
	(cd_perfil_p is not null) and
	(nm_usuario_p is not null) then
	begin
	open c01;
	loop
	fetch c01 into 
		nr_seq_atributo_w,
		ie_configuravel_w;
	exit when c01%notfound;
		begin
		ie_atributo_w	:= null;
		ds_titulo_w	:= null;
		if	(ie_configuravel_w = 'P') then
			ie_atributo_w	:= 'S';
		elsif	(ie_configuravel_w = 'R') then
			ie_atributo_w	:= 'N';
		end if;
		
		open c02;
		loop
		fetch c02 into
			ie_atributo_w,
			ds_titulo_w;
		exit when c02%notfound;
			begin
			ie_atributo_w	:= ie_atributo_w;
			ds_titulo_w	:= ds_titulo_w;
			end;
		end loop;
		close c02;
		
		if(ie_mmed is null) then
			ds_regra_wcpanel_w := ds_regra_wcpanel_w || nr_seq_atributo_w || '=' || nvl(ie_atributo_w,'N') || '@1' || ds_titulo_w || ';';		
		else
			ds_regra_wcpanel_w := ds_regra_wcpanel_w || nr_seq_atributo_w || '=' || nvl(ie_atributo_w,'N') || '@1;';		
		end if;
		

		end;
	end loop;
	close c01;
	end;
end if;
return ds_regra_wcpanel_w;
end obter_regra_atributo_wcpanel;
/