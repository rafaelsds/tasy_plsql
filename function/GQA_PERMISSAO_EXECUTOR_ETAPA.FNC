CREATE OR REPLACE FUNCTION gqa_permissao_executor_etapa(nr_seq_etapa_p    NUMBER
                                                       ,nr_seq_prot_pac_p NUMBER
                                                       ,nr_atendimento_p  NUMBER)
  RETURN VARCHAR2 IS

BEGIN
  IF gqa_obter_se_executor_etapa(nr_seq_etapa_p, nr_seq_prot_pac_p) = 'S' THEN
    
    IF obter_se_gqa_regra_liberada(nr_seq_etapa_p) = 'S' THEN

      IF gqa_regra_complementar(wheb_usuario_pck.get_nm_usuario
                               ,nr_atendimento_p
                               ,nr_seq_etapa_p) = 'X' THEN
        RETURN 'S';
      END IF;
    END IF;
  END IF;

  RETURN 'N';
END;
/
