create or replace
function ELIMINA_ASPAS_CONV(	ds_campo_p	varchar2)
		return varchar2 is

ds_retorno_w	varchar2(4000);
i		Integer;

begin

if 	(ds_campo_p is not null) then
	begin

	for	i in 1 .. length(ds_campo_p) loop
		begin
		if	not (substr(ds_campo_p,i,1) = chr(39)) then
			ds_retorno_w	:= ds_retorno_w || substr(ds_campo_p,i,1);	
		end if;
		end;
	end loop;
	end;
end if;
	
return	ds_retorno_w;

end elimina_aspas_conv;
/
