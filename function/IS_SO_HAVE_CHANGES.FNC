create or replace
function is_so_have_changes(nr_service_order_p number) return varchar2 is

qt_commit_w 	number(1);
qt_script_w 	number(1);

begin 

Select  count(1)
into    qt_commit_w
from    corp.man_commit_git@whebl01_dbcorp
where   nr_Seq_ordem_serv = nr_service_order_p;

 
select  count(1)
into    qt_script_w
from    tasy.ajuste_versao@whebl02_orcl
where   nr_seq_ordem_serv =nr_service_order_p;

 

if    (qt_commit_w > 0 or qt_script_w > 0) then
        return 'S';
    else
        return 'N';
end if;

end is_so_have_changes;
/
