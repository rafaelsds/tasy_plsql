create or replace
function pls_obter_se_estip_filantropia(nr_seq_contrato_p	number,
					dt_referencia_p		date)
					return varchar2 is
					
ie_filantropia_w	varchar2(1)	:= 'N';
cd_cgc_estipulante_w	varchar2(14);

Cursor c01 is
select	a.ie_isencao_tributaria
from	pessoa_juridica_inf_trib a
where	a.cd_cgc	= cd_cgc_estipulante_w
and	nvl(dt_referencia_p,sysdate) between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,nvl(dt_referencia_p,sysdate));

begin

if	(nr_seq_contrato_p is not null) then
	select	max(cd_cgc_estipulante)
	into	cd_cgc_estipulante_w
	from	pls_contrato a
	where	a.nr_sequencia	= nr_seq_contrato_p;
	
	if	(cd_cgc_estipulante_w is not null) then
		open c01;
		loop
		fetch c01 into	
			ie_filantropia_w;
		exit when c01%notfound;
			begin
			null;
			end;
		end loop;
		close c01;
	end if;
end if;

return	ie_filantropia_w;

end pls_obter_se_estip_filantropia;
/