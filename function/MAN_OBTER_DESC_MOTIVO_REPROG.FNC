create or replace
function man_obter_desc_motivo_reprog(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);

begin

if	(nr_sequencia_p > 0) then
	select	ds_motivo
	into	ds_retorno_w
	from	man_motivo_reprog_os
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end man_obter_desc_motivo_reprog;
/