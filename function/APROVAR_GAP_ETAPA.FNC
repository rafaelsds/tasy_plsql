create or replace 
FUNCTION Aprovar_Gap_Etapa(
			nr_seq_processo_p	number,
			nr_seq_etapa_p		number,
			dt_prevista_p		date)
			return varchar2 is

qt_etapa_w	number(10) := 0;
ie_aprova_w	varchar(2) := 'N';
ie_resultado_w	varchar2(1):= 'X';

cursor c01 is
	select	ie_resultado
	from	gap_processo_etapa
	where	nr_seq_processo = nr_seq_processo_p
	and	dt_prevista < dt_prevista_p
	order by	dt_prevista;

begin

select	count(*)
into	qt_etapa_w
from	gap_processo_etapa
where	nr_seq_processo = nr_seq_processo_p;

if	(qt_etapa_w = 0) then
	ie_aprova_w	:= 'N';
elsif	(qt_etapa_w = 1) then
	ie_aprova_w	:= 'S';
else	begin
	open c01;
		loop
		fetch c01 into	
			ie_resultado_w;
			exit when c01%notfound;
		end loop;
	close c01;
	
	if	(ie_resultado_w = 'A') then
		ie_aprova_w	:= 'S';
	elsif	(ie_resultado_w = 'R') then
		ie_aprova_w	:= 'S';
	elsif	(ie_resultado_w = 'X') then
		ie_aprova_w	:= 'S';
	elsif	(ie_resultado_w = 'N') then
		ie_aprova_w	:= 'N';
	end if;
	end;
end if;

return ie_aprova_w;

END Aprovar_Gap_Etapa;
/