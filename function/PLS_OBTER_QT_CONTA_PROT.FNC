create or replace
function pls_obter_qt_conta_prot(	nr_seq_protocolo_p	pls_protocolo_conta.nr_sequencia%type)
 		    	return number is

qt_contas_w	 number(10);

begin

select	count(1)
into	qt_contas_w
from	pls_conta_imp a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p;

return	qt_contas_w;

end pls_obter_qt_conta_prot;
/