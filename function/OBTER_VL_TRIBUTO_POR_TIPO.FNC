create or replace
function obter_vl_tributo_por_tipo (	nr_seq_nota_p	number,	
					ie_tributo_p	varchar2,
					ie_opcao_p	varchar2 default 'VT')
					return number is
/* Rotina feita especificamente para a DACON 

VT - Valor do tributo
VB - Valor da base de c�lculo do PIS ou do COFINS
*/

vl_resultado_w	number(10,2);
qt_registro_w	number(10);

begin

if (ie_opcao_p = 'VT') then
	select 	replace(campo_mascara(nvl(max(nvl(vl_tributo,0)),'0'),2),'.','')
	into   	vl_resultado_w
	from   	nota_fiscal_trib c,
		tributo d
	where  	d.cd_tributo = c.cd_tributo	   
	and    	c.nr_sequencia = nr_seq_nota_p
	and    	d.ie_tipo_tributo = ie_tributo_p;
elsif (ie_opcao_p = 'VB') then
	select 	count(*)
	into	qt_registro_w
	from 	nota_fiscal_trib c,
		tributo d
	where 	d.cd_tributo = c.cd_tributo
	and	c.nr_sequencia = nr_seq_nota_p
	and	(d.ie_tipo_tributo = 'PIS' or d.ie_tipo_tributo = 'COFINS');
	if (qt_registro_w > 0) then
		select 	replace(campo_mascara(nvl(max(c.vl_base_calculo),'0'),2),'.','')
		into	vl_resultado_w
		from	nota_fiscal_trib c,
			tributo d
		where	d.cd_tributo = c.cd_tributo
		and	c.nr_sequencia = nr_seq_nota_p
		and	d.ie_tipo_tributo = 'PIS';
		if	(vl_resultado_w = 0) then
			select 	max(c.vl_base_calculo)
			into	vl_resultado_w
			from	nota_fiscal_trib c,
				tributo d
			where	d.cd_tributo = c.cd_tributo
			and	c.nr_sequencia = nr_seq_nota_p
			and	d.ie_tipo_tributo = 'COFINS';
		end if;	
	elsif	(qt_registro_w = 0) then
		vl_resultado_w := 0;
	end if;
end if;

return vl_resultado_w;
end obter_vl_tributo_por_tipo;
/