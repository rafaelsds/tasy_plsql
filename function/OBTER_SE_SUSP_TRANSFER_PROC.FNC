create or replace
function OBTER_SE_SUSP_TRANSFER_PROC(	cd_setor_atendimento_p in number,
										nr_seq_proc_interno_p in number) return varchar2 is
								
nr_seq_regra_w CPOE_RULE_SUSP_TRANSFER.NR_SEQUENCIA%TYPE;
nr_seq_regra_proc_w	CPOE_RULE_SUSP_ITEMS.NR_SEQUENCIA%TYPE;
cont_rule_susp_proc_w number(5);
qt_regra_espec_proc_w CPOE_RULE_SUSP_PROC.NR_SEQUENCIA%TYPE;
ds_retorno_w varchar2(1) := 'N';
								
begin

select	max(a.nr_sequencia)
into	nr_seq_regra_w
from	CPOE_RULE_SUSP_TRANSFER a
where	a.CD_SETOR_ATENDIMENTO = cd_setor_atendimento_p
and		a.IE_SITUACAO = 'S';

if	(nr_seq_regra_w > 0) then -- if there is rule for the department
	
	select	b.nr_sequencia
	into	nr_seq_regra_proc_w
	from	CPOE_RULE_SUSP_ITEMS b
	where	b.NR_SEQ_RULE_SUSP = nr_seq_regra_w
	and		b.IE_GRUPO = 'P'
	and		b.IE_SITUACAO = 'S';
	
	if	(nr_seq_regra_proc_w > 0) then
	
		ds_retorno_w := 'S';
	
		select	count(*)
		into	cont_rule_susp_proc_w
		from	CPOE_RULE_SUSP_PROC c
		where	c.NR_SEQ_RULE_SUSP_ITEMS = nr_seq_regra_proc_w
		and		c.IE_SITUACAO = 'S';
		
		if	(cont_rule_susp_proc_w > 0) then
			
			select	count(*)
			into	qt_regra_espec_proc_w
			from 	CPOE_RULE_SUSP_PROC d
			where	d.NR_SEQ_RULE_SUSP_ITEMS = nr_seq_regra_proc_w
			and		d.ie_situacao = 'S'
			and		((d.nr_seq_proc_interno is null) or (d.nr_seq_proc_interno = nr_seq_proc_interno_p))
			and 	((d.ie_tipo is null) or (d.ie_tipo = obter_tipo_proc_interno(nr_seq_proc_interno_p)));
			
			if	(qt_regra_espec_proc_w = 0) then
				ds_retorno_w := 'N';
			end if;
			
		end if;
	
	end if;
	
end if;

return ds_retorno_w;

end OBTER_SE_SUSP_TRANSFER_PROC;
/