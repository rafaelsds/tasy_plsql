create or replace
function lab_cons_val_ref_massas(nr_seq_exame_p	in number,
				qt_result_p			in number,
				ds_result_p			in varchar,
				nr_seq_prescr_p		in number,
				ie_opcao_p			in number,
				nr_seq_resultado_p 	in number,
				nr_seq_patologia_p	in number,
				nr_seq_grupo_pat_p	in number)
 		    	return varchar2 is

ds_retorno_w		varchar2(4000);
qt_dias_w		number(10,2)	:= 0;
qt_horas_w		number(10,0)	:= 0;
ie_tipo_valor_w		number(10,0)	:= 0;
ie_valor_percent_w	varchar2(10);
ie_sexo_w		varchar2(1);	
nr_sequencia_w		number(10,0);
contador_abaixo_w	number(10,0);			
contador_acima_w	number(10,0);
qt_percent_min_w	number(10,0);
qt_percent_max_w	number(10,0);
qt_result_min_w		number(10,0);
qt_result_max_w		number(10,0);
ds_msg_criterio_w	varchar2(4000);
ds_acao_criterio_w	varchar2(4000);
cd_pessoa_fisica_w	varchar2(10);
nr_prescricao_w		number(10);
qt_dias_prev_w		varchar2(255);
dt_coleta_w			date;
dt_transf_w			date;
qt_dias_coleta_w	number(10,0) := 0;
qt_horas_coleta_w	number(10,0) := 0;
qt_dias_transf_w	number(10,0) := 0;
qt_horas_transf_w	number(10,0) := 0;
ie_tipo_busca_w		varchar2(1);
ie_acao_criterio_w	lote_ent_patol_exam_crit.ie_acao_criterio%type;
ds_resultado_sugerido_w  varchar2(255);
ds_material_criterio_w		varchar2(255);
ds_metodo_criterio_w		varchar2(255);
qt_minima_patol_w			number(15,4);
qt_maxima_patol_w			number(15,4);
ds_observacao_ref_w			varchar2(2000);
			
begin


/*
ie_opcao_p 
1 - Retorna descri��o do tipo de valor
2 - Retorna o ie_tipo_valor como number
3 - Retorna a mensagem do criterio
4 - Retorna a acao cadastrada no criterio
5 - Retorna o QT_DIAS cadastrado no criterio
6 - Retorna o TIPO DE BUSCA cadastrado para o criterio
7 - Retorna c�digo IE_ACAO_CRITERIO
8 - Retorna resultado sugerido
9 - Retorna o material do crit�rio
10 - Retorna o m�todo do crit�rio
11 - Retorna o qt_minina da referencia da patologia
12 - Retorna o qt_maxima da referencia da patologia
13 - Retorna o ds_observacao da referencia da patologia
*/


/*select 	obter_dias_entre_datas(Lab_obter_data_coleta(nr_prescricao_w, nr_seq_prescr_p),sysdate),
	round(Obter_Hora_Entre_datas(Lab_obter_data_coleta(nr_prescricao_w, nr_seq_prescr_p),sysdate))
into 	qt_dias_w,
	qt_horas_w
from 	dual;*/

select	max(nr_prescricao)
into	nr_prescricao_w
from	exame_lab_resultado
where	nr_seq_resultado = nr_seq_resultado_p;

select 	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_w;

select 	obter_dias_entre_datas(obter_data_nascto_pf(cd_pessoa_fisica_w),sysdate),
	round(Obter_Hora_Entre_datas(obter_data_nascto_pf(cd_pessoa_fisica_w),sysdate))
into 	qt_dias_w,
	qt_horas_w
from 	dual;

select 	max(IE_FORMATO_RESULTADO)
into	ie_valor_percent_w
from	exame_laboratorio
where	nr_seq_exame = nr_seq_exame_p;

select 	max(Obter_Sexo_Prescricao(nr_prescricao_w))
into	ie_sexo_w
from	dual;

select	TO_DATE(TO_CHAR(max(nvl(b.DT_COLETA_FICHA_F,sysdate)),'dd/mm/yyyy')||' '||TO_CHAR(max(nvl(b.HR_COLETA_F,sysdate)),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
		max(nvl(b.DT_TRANSF_F,sysdate))
into	dt_coleta_w,
		dt_transf_w
from	prescr_procedimento a,
		lote_ent_sec_ficha b
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_prescricao = nr_prescricao_w;


if 	(dt_coleta_w is not null) then

	select 	obter_dias_entre_datas(obter_data_nascto_pf(cd_pessoa_fisica_w), dt_coleta_w),
			round(Obter_Hora_Entre_datas(obter_data_nascto_pf(cd_pessoa_fisica_w),dt_coleta_w))
	into 	qt_dias_coleta_w,
			qt_horas_coleta_w
	from 	dual;

end if;

if	(dt_transf_w is not null) then
	
	select 	obter_dias_entre_datas(dt_transf_w,dt_coleta_w),
			round(Obter_Hora_Entre_datas(dt_transf_w,dt_coleta_w))
	into 	qt_dias_transf_w,
			qt_horas_transf_w
	from 	dual;
	
end if;

if 	(ie_valor_percent_w = 'P') then
	begin
	select 	a.ie_tipo_valor,
			a.nr_sequencia,
			a.qt_minima,
			a.qt_maxima,
			a.ds_observacao
	into	ie_tipo_valor_w,
			nr_sequencia_w,
			qt_minima_patol_w,
			qt_maxima_patol_w,
			ds_observacao_ref_w
	from 	LOTE_ENT_PATOL_EXAM_REF a,
			lab_patologia_exame b,
			lab_patologia c
	where 	a.nr_seq_exame = b.nr_seq_exame
	--and a.nr_seq_patologia = nr_seq_patologia_p
	and		b.nr_seq_patologia = nr_seq_patologia_p
	and		c.nr_seq_grupo_pat = nr_seq_grupo_pat_p
	and		((a.ie_sexo = nvl(ie_sexo_w,'0')) or (a.ie_sexo = '0'))
	and 	a.nr_seq_exame = nr_seq_exame_p
--	and 	nvl(a.nr_seq_material,nr_seq_material_p) = nr_seq_material_p
	and 	(((nvl(a.ie_tipo_data,'N') = 'N') and (((nvl(trunc((qt_dias_w / 365.25),2),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
		or	((nvl(a.ie_tipo_data,'N') = 'C') and (((nvl((qt_dias_coleta_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_coleta_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
		or	((nvl(a.ie_tipo_data,'N') = 'T') and (((nvl((qt_dias_transf_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_transf_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H')))))	
	--and 	qt_result_p between a.qt_percent_min and a.qt_percent_max
	and	a.ie_tipo_valor = 0
	and rownum <= 1
	order by a.NR_SEQ_PRIORIDADE, a.nr_seq_material, a.ie_sexo, a.ie_tipo_valor;
	exception
		when no_data_found then
			ie_tipo_valor_w := 0;
	end;
elsif	(ie_valor_percent_w = 'V') then
	begin
	select 	a.ie_tipo_valor,
			a.nr_sequencia,
			a.qt_minima,
			a.qt_maxima,
			a.ds_observacao
	into	ie_tipo_valor_w,
			nr_sequencia_w,
			qt_minima_patol_w,
			qt_maxima_patol_w,
			ds_observacao_ref_w
	from 	LOTE_ENT_PATOL_EXAM_REF a,
			lab_patologia_exame b,
			lab_patologia c			
	where 	a.nr_seq_exame = b.nr_seq_exame
	--and a.nr_seq_patologia = nr_seq_patologia_p
	and		b.nr_seq_patologia = c.nr_sequencia
	and		b.nr_seq_patologia = nr_seq_patologia_p
	and		c.nr_seq_grupo_pat = nr_seq_grupo_pat_p
	and		((a.ie_sexo = nvl(ie_sexo_w,'0')) or (a.ie_sexo = '0'))
	and 	a.nr_seq_exame = nr_seq_exame_p
--	and 	nvl(a.nr_seq_material,nr_seq_material_p) = nr_seq_material_p
	and 	(((nvl(a.ie_tipo_data,'N') = 'N') and (((nvl(trunc((qt_dias_w / 365.25),2),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
		or	((nvl(a.ie_tipo_data,'N') = 'C') and (((nvl((qt_dias_coleta_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_coleta_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
		or	((nvl(a.ie_tipo_data,'N') = 'T') and (((nvl((qt_dias_transf_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_transf_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H')))))
	--and 	qt_result_p between a.qt_minima and a.qt_maxima
	and	a.ie_tipo_valor = 0
	and rownum <= 1
	order by a.NR_SEQ_PRIORIDADE, a.nr_seq_material, a.ie_sexo, a.ie_tipo_valor;
	exception
		when no_data_found then
			ie_tipo_valor_w := 0;
	end;
elsif	(ie_valor_percent_w = 'D') then
	begin
	select 	a.ie_tipo_valor,
			a.nr_sequencia,
			a.qt_minima,
			a.qt_maxima,
			a.ds_observacao
	into	ie_tipo_valor_w,
			nr_sequencia_w,
			qt_minima_patol_w,
			qt_maxima_patol_w,
			ds_observacao_ref_w
	from 	LOTE_ENT_PATOL_EXAM_REF a,
			lab_patologia_exame b,
			lab_patologia c	
	where 	a.nr_seq_exame = b.nr_seq_exame
	and		b.nr_seq_patologia = c.nr_sequencia
	--and a.nr_seq_patologia = nr_seq_patologia_p
	and		b.nr_seq_patologia = nr_seq_patologia_p
	and		c.nr_seq_grupo_pat = nr_seq_grupo_pat_p
	and		((a.ie_sexo = nvl(ie_sexo_w,'0')) or (a.ie_sexo = '0'))
	and 	a.nr_seq_exame = nr_seq_exame_p
--	and 	nvl(a.nr_seq_material,nr_seq_material_p) = nr_seq_material_p
	and 	(((nvl(a.ie_tipo_data,'N') = 'N') and (((nvl(trunc((qt_dias_w / 365.25),2),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
		or	((nvl(a.ie_tipo_data,'N') = 'C') and (((nvl((qt_dias_coleta_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_coleta_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
		or	((nvl(a.ie_tipo_data,'N') = 'T') and (((nvl((qt_dias_transf_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
			(((nvl((qt_dias_transf_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
			(((nvl(qt_dias_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
			(((nvl(qt_horas_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H')))))
	--and 	upper(ELIMINA_ACENTOS(ds_result_p)) like upper(ELIMINA_ACENTOS(a.DS_OBSERVACAO))
	and	a.ie_tipo_valor = 0
	and rownum <= 1
	order by a.NR_SEQ_PRIORIDADE, a.nr_seq_material, a.ie_sexo, a.ie_tipo_valor;
	exception
		when no_data_found then
			ie_tipo_valor_w := 0;
	end;
elsif	(ie_valor_percent_w = 'DV') then
	if  (nvl(ds_result_p,'0') <> '0') then	
		begin
		select 	a.ie_tipo_valor,
				a.nr_sequencia,
				a.qt_minima,
				a.qt_maxima,
				a.ds_observacao
		into	ie_tipo_valor_w,
				nr_sequencia_w,
				qt_minima_patol_w,
				qt_maxima_patol_w,
				ds_observacao_ref_w
		from 	LOTE_ENT_PATOL_EXAM_REF a,
				lab_patologia_exame b,
				lab_patologia c	
		where 	a.nr_seq_exame = b.nr_seq_exame
		--and a.nr_seq_patologia = nr_seq_patologia_p
		and		b.nr_seq_patologia = c.nr_sequencia
		and		b.nr_seq_patologia = nr_seq_patologia_p
		and		c.nr_seq_grupo_pat = nr_seq_grupo_pat_p
		and		((a.ie_sexo = nvl(ie_sexo_w,'0')) or (a.ie_sexo = '0'))
		and 	a.nr_seq_exame = nr_seq_exame_p
	--	and 	nvl(a.nr_seq_material,nr_seq_material_p) = nr_seq_material_p
		and 	(((nvl(a.ie_tipo_data,'N') = 'N') and (((nvl(trunc((qt_dias_w / 365.25),2),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
				(((nvl((qt_dias_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
				(((nvl(qt_dias_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
				(((nvl(qt_horas_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
			or	((nvl(a.ie_tipo_data,'N') = 'C') and (((nvl((qt_dias_coleta_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
				(((nvl((qt_dias_coleta_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
				(((nvl(qt_dias_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
				(((nvl(qt_horas_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
			or	((nvl(a.ie_tipo_data,'N') = 'T') and (((nvl((qt_dias_transf_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
				(((nvl((qt_dias_transf_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
				(((nvl(qt_dias_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
				(((nvl(qt_horas_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H')))))
		--and 	upper(ELIMINA_ACENTOS(ds_result_p)) like upper(ELIMINA_ACENTOS(a.DS_OBSERVACAO))
		and	a.ie_tipo_valor = 0
		and rownum <= 1
		order by a.NR_SEQ_PRIORIDADE, a.nr_seq_material, a.ie_sexo, a.ie_tipo_valor;
		exception
			when no_data_found then
				ie_tipo_valor_w := 0;
		end;	
		
	else
	
		begin
		select 	a.ie_tipo_valor,
				a.nr_sequencia,
				a.qt_minima,
				a.qt_maxima,
				a.ds_observacao
		into	ie_tipo_valor_w,
				nr_sequencia_w,
				qt_minima_patol_w,
				qt_maxima_patol_w,
				ds_observacao_ref_w
		from 	LOTE_ENT_PATOL_EXAM_REF a,
				lab_patologia_exame b,
				lab_patologia c			
		where 	a.nr_seq_exame = b.nr_seq_exame
	--	and a.nr_seq_patologia = nr_seq_patologia_p
		and		b.nr_seq_patologia = c.nr_sequencia
		and		b.nr_seq_patologia = nr_seq_patologia_p
		and		c.nr_seq_grupo_pat = nr_seq_grupo_pat_p
		and		((a.ie_sexo = nvl(ie_sexo_w,'0')) or (a.ie_sexo = '0'))
		and 	a.nr_seq_exame = nr_seq_exame_p
	--	and 	nvl(a.nr_seq_material,nr_seq_material_p) = nr_seq_material_p
		and 	(((nvl(a.ie_tipo_data,'N') = 'N') and (((nvl(trunc((qt_dias_w / 365.25),2),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
				(((nvl((qt_dias_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
				(((nvl(qt_dias_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
				(((nvl(qt_horas_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
			or	((nvl(a.ie_tipo_data,'N') = 'C') and (((nvl((qt_dias_coleta_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
				(((nvl((qt_dias_coleta_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
				(((nvl(qt_dias_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
				(((nvl(qt_horas_coleta_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H'))))
			or	((nvl(a.ie_tipo_data,'N') = 'T') and (((nvl((qt_dias_transf_w / 365.25),0) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'A')) or
				(((nvl((qt_dias_transf_w / 365.25),0) * 12) between a.qt_idade_min and qt_idade_max) and (a.ie_periodo = 'M')) or
				(((nvl(qt_dias_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'D')) or
				(((nvl(qt_horas_transf_w,0)) between a.qt_idade_min and a.qt_idade_max) and (a.ie_periodo = 'H')))))
		--and 	qt_result_p between a.qt_minima and a.qt_maxima
		and	a.ie_tipo_valor = 0
		and rownum <= 1
		order by a.NR_SEQ_PRIORIDADE, a.nr_seq_material, a.ie_sexo, a.ie_tipo_valor;
		exception
			when no_data_found then
				ie_tipo_valor_w := 0;
		end;
	
	end if;
	
	if (ie_opcao_p = 1) then
		if 	(ie_tipo_valor_w = 0)	then
			ds_retorno_w := Wheb_mensagem_pck.get_texto(308858); --'Refer�ncia';
		elsif 	(ie_tipo_valor_w = 1)	then
			ds_retorno_w := Wheb_mensagem_pck.get_texto(308859); --'Aceit�vel';
		elsif	(ie_tipo_valor_w = 2) 	then
			ds_retorno_w := Wheb_mensagem_pck.get_texto(308860); --'N�o aceit�vel';
		end if;
	elsif (ie_opcao_p = 2) then
		if 	(ie_tipo_valor_w = 0)	then
			ds_retorno_w := '0';
		elsif 	(ie_tipo_valor_w = 1)	then
			ds_retorno_w := '1';
		elsif	(ie_tipo_valor_w = 2) 	then
			ds_retorno_w := '2';
		end if;
	elsif (ie_opcao_p = 11) then
		ds_retorno_w := to_char(qt_minima_patol_w);
	elsif (ie_opcao_p = 12) then
		ds_retorno_w := to_char(qt_maxima_patol_w);
	elsif (ie_opcao_p = 13) then
		ds_retorno_w := substr(ds_observacao_ref_w,1,255);
	end if;
end if;

if 	(ie_tipo_valor_w = 0)	and
	(nvl(nr_sequencia_w,0) <> 0) then
	
	if (ie_opcao_p = 1) then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(308858); --'Refer�ncia';
	elsif (ie_opcao_p = 2) then
		ds_retorno_w := '0';
	elsif (ie_opcao_p = 11) then
		ds_retorno_w := to_char(qt_minima_patol_w);
	elsif (ie_opcao_p = 12) then
		ds_retorno_w := to_char(qt_maxima_patol_w);	
	elsif (ie_opcao_p = 13) then
		ds_retorno_w := substr(ds_observacao_ref_w,1,255);		
	end if;
	
end if;

return	ds_retorno_w;

end lab_cons_val_ref_massas;
/
