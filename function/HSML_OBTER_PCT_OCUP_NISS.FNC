create or replace 
FUNCTION hsml_obter_pct_ocup_niss(dt_parametro_p  DATE,
				    ie_opcao_p  VARCHAR2,
				    nr_seq_classif_p number)
        RETURN NUMBER IS

pct_ocup_niss_w NUMBER(10,4);

BEGIN

IF (ie_opcao_p = 'M') THEN
 BEGIN
	SELECT  dividir(SUM(nr_leitos_ocupados)*100,hsml_obter_leito_ocup_niss(max(dt_referencia),'M'))
	into 	pct_ocup_niss_w
	FROM 	HMSL_EIS_OCUP_CLASSIF_NISS_V a
	WHERE 	ie_periodo = 'M'
	and	a.nr_seq_classif_niss = nr_seq_classif_p
	and	trunc(dt_parametro_p,'month') = trunc(dt_referencia,'month');	
 END;
END IF;
IF (ie_opcao_p = 'D') THEN
 BEGIN
	SELECT  dividir(SUM(nr_leitos_ocupados)*100,hsml_obter_leito_ocup_niss(max(dt_referencia),'D'))
	into    pct_ocup_niss_w
	FROM    HMSL_EIS_OCUP_CLASSIF_NISS_V a
	WHERE   ie_periodo = 'D'
	and	a.nr_seq_classif_niss = nr_seq_classif_p
	and	trunc(dt_parametro_p,'dd') = trunc(dt_referencia,'dd');
 END;
END IF;
IF (ie_opcao_p = '6h') THEN
 BEGIN
	SELECT  dividir(SUM(nr_leitos_ocupados)*100,hsml_obter_leito_ocup_niss(max(dt_referencia),'6h'))
	into    pct_ocup_niss_w
	FROM    HMSL_EIS_OCUP_CLASSIF_NISS_V a
	WHERE   ie_periodo = 'D'
	and	a.nr_seq_classif_niss = nr_seq_classif_p
	and	trunc(dt_parametro_p,'dd') = trunc(dt_referencia,'dd')
	and	to_char(dt_referencia,'hh24') = '06';
 END;
END IF;

RETURN pct_ocup_niss_w;

END hsml_obter_pct_ocup_niss;
