create or replace
function obter_se_dieta_consumida(nr_seq_mapa_p	Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(1);			

begin

if (nr_seq_mapa_p is not null) then

	select	max(ie_dieta_consumida)
	into	ds_retorno_w
	from	mapa_dieta
	where	nr_sequencia = nr_seq_mapa_p;

end if;


return	ds_retorno_w;

end obter_se_dieta_consumida;
/