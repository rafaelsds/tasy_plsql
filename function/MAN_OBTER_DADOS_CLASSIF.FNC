create or replace
function man_obter_dados_classif
			(	nr_seq_classif_p		number,
				ie_opcao_p			varchar2)
				return varchar2 is

ds_retorno_w		varchar2(255);
ds_classificacao_w	varchar2(255);

begin
select	substr(obter_desc_expressao(cd_exp_classificacao,ds_classificacao),1,255)
into	ds_classificacao_w
from	man_classif_gestao
where	nr_sequencia	= nr_seq_classif_p;

if	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_classificacao_w;
end if;

return ds_retorno_w;

end man_obter_dados_classif;
/