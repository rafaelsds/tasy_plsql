create or replace
function obter_se_pac_age_futura	(nr_seq_agenda_p	number)
					return varchar2 is

ie_futuras_w		varchar2(1) := 'N';
dt_agenda_w		date;
cd_pessoa_fisica_w	varchar2(10);
cd_tipo_agenda_w	number(10,0);
qt_agenda_w		number(10,0);

begin
if	(nr_seq_agenda_p is not null) then
	select	b.dt_agenda,
		b.cd_pessoa_fisica,
		a.cd_tipo_agenda
	into	dt_agenda_w,
		cd_pessoa_fisica_w,
		cd_tipo_agenda_w
	from	agenda a,
		agenda_consulta b
	where	a.cd_agenda		= b.cd_agenda
	and	b.nr_sequencia 	= nr_seq_agenda_p;

	if (cd_pessoa_fisica_w is not null and dt_agenda_w is not null) then
		select	nvl(count(*),0)
		into	qt_agenda_w
		from	agenda a,
			agenda_consulta b
		where	a.cd_agenda 		= b.cd_agenda
		and	a.cd_tipo_agenda	= cd_tipo_agenda_w
		and	b.dt_agenda		> dt_agenda_w
		and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	b.ie_status_agenda	<> 'C'
		and 	rownum < 2;
	end if;

	if	(nvl(qt_agenda_w,0) > 0) then
		ie_futuras_w	:= 'S';
	end if;
end if;

return	ie_futuras_w;		

end obter_se_pac_age_futura;
/