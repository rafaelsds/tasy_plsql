create or replace 
FUNCTION rop_obter_desc_motivo(nr_sequencia_p	number)
					return varchar2 is

ds_motivo_w		varchar2(80);

begin
select	ds_motivo
into	ds_motivo_w
from	rop_motivo_baixa
where	nr_sequencia	= nr_sequencia_p;

return ds_motivo_w;

end rop_obter_desc_motivo;
/