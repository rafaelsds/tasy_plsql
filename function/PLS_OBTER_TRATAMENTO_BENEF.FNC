create or replace
function pls_obter_tratamento_benef
			(	dt_requisicao_p		date,
				nr_seq_segurado_p	number)
				return number is
				
nr_retorno_w			number(10);

begin

select	max(nr_sequencia)
into	nr_retorno_w
from	pls_tratamento_benef
where	nr_seq_segurado = nr_seq_segurado_p
and	dt_requisicao_p between trunc(dt_inicio_tratamento) and  fim_dia(dt_fim_tratamento);

return	nr_retorno_w;

end pls_obter_tratamento_benef;
/