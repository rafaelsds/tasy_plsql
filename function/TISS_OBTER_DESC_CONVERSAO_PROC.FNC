create or replace
function tiss_obter_desc_conversao_proc
			(cd_convenio_p		Number,
			cd_procedimento_p	Number,
			ie_origem_proced_p	Number,
			nr_seq_proc_interno_p	number,
			cd_proc_convenio_p	varchar2)
			return varchar2 is
			
ds_retorno_w	varchar2(255);			

begin

begin
select	max(ds_proc_convenio)
into	ds_retorno_w
from	conversao_proc_convenio
where	cd_convenio					= cd_convenio_p
and	cd_proc_convenio				= cd_proc_convenio_p
and	nvl(cd_procedimento,cd_procedimento_p)		= cd_procedimento_p
and	nvl(ie_origem_proced,ie_origem_proced_p)	= ie_origem_proced_p
and	nvl(nr_seq_proc_interno,nvl(nr_seq_proc_interno_p,0))	= nvl(nr_seq_proc_interno_p,0)
and	nvl(ie_situacao,'A') = 'A';
exception
	when others then
	ds_retorno_w	:= null;
end;

return	ds_retorno_w;

end tiss_obter_desc_conversao_proc;
/