create or replace
function obter_se_sol_vig_adep (
				nr_prescricao_p			number,
				ie_status_p			varchar2,				
				qt_hora_anterior_p		number,
				qt_hora_adicional_p		number,
				dt_filtro_p			date,
				dt_suspensao_p			date,
				dt_status_p			date)
				return varchar2 is
			
ie_vigente_w			varchar2(1) := 'N';
dt_inicio_prescr_w		date;
dt_validade_prescr_w	date;
dt_inicial_w			date;
dt_final_w				date;
dt_filtro_w				date := dt_filtro_p;
			
begin
if	(nr_prescricao_p is not null) then
		
	select	max(dt_inicio_prescr),
			max(dt_validade_prescr)
	into	dt_inicio_prescr_w,
			dt_validade_prescr_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
		
	if	(dt_filtro_p is not null) then		
		dt_inicial_w := dt_filtro_p;		
	elsif	(qt_hora_anterior_p > 0) then		
		select	to_date(to_char(trunc(sysdate - qt_hora_anterior_p/24, 'hh24'), 'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
		into	dt_inicial_w
		from	dual;
	else
		select	to_date(to_char(trunc(sysdate, 'hh24'), 'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
		into	dt_inicial_w
		from	dual;	
	end if;
	
	if	(dt_filtro_p is not null) then			
		dt_final_w := to_date(to_char(trunc(dt_filtro_w + qt_hora_adicional_p/24, 'hh24'), 'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
	elsif	(qt_hora_adicional_p > 0) then
		select	to_date(to_char(trunc(sysdate + qt_hora_adicional_p/24, 'hh24'), 'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
		into	dt_final_w
		from	dual;
	else
		select	to_date(to_char(trunc(sysdate, 'hh24'), 'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
		into	dt_final_w
		from	dual;
	end if;
	
	if	(dt_filtro_p is null) and
		(dt_inicio_prescr_w between dt_inicial_w and dt_final_w) and
		(ie_status_p in ('N', 'I', 'INT', 'R', 'II')) then
		ie_vigente_w := 'S';
	elsif	((dt_inicial_w <= dt_inicio_prescr_w) or
			(dt_inicial_w >= dt_inicio_prescr_w)) and
		(ie_status_p in ('N', 'I', 'INT', 'R', 'II')) then
		ie_vigente_w := 'S';
	elsif	(((ie_status_p = 'S') and
			  ((dt_inicial_w <= dt_inicio_prescr_w) and
			   (dt_suspensao_p <= dt_final_w))) or
			 ((ie_status_p = 'T') and
			  ((dt_inicial_w <= dt_inicio_prescr_w) and 
			   (dt_status_p <= dt_final_w)))) then
			ie_vigente_w := 'S';		
	else
		ie_vigente_w := 'N';
	end if;
	
end if;

return ie_vigente_w;

end obter_se_sol_vig_adep;
/