create or replace
function OBTER_CM_DIARIO_ESTOQUE(
				cd_estabelecimento_p		Number,
				dt_mesano_referencia_p	Date,
				cd_material_p			Number,
				dt_referencia_p		Date)
				return number is


vl_custo_medio_w		Number(15,4);

cursor c01 is
select	vl_custo_medio
from	saldo_estoque_cm_diario
where	cd_estabelecimento	= cd_estabelecimento_p
and	dt_mesano_referencia	= dt_mesano_referencia_p
and	cd_material		= cd_material_p
and	dt_referencia		<= dt_referencia_p
order by dt_referencia;

BEGIN

OPEN c01;
LOOP
FETCH c01 into vl_custo_medio_w;
EXIT when c01%notfound;
	vl_custo_medio_w	:= vl_custo_medio_w;
END LOOP;
CLOSE c01;

return	nvl(vl_custo_medio_w,0);

end OBTER_CM_DIARIO_ESTOQUE;
/


