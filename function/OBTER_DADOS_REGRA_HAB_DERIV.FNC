create or replace
function Obter_dados_regra_hab_deriv( nr_seq_derivado_p    	    number,
									  cd_setor_atendimento_p	number,
									  ie_opcao_p		    	varchar2,
									  qt_peso_p                 number)
									  return varchar2 is
			
ie_aliquotado_w		varchar2(1);
ds_retorno_w		varchar2(4000);

/*
   A - Aliquotado
*/

begin

if	(nr_seq_derivado_p is not null) then
	select	max(ie_permite_aliquotado)
	into	ie_aliquotado_w
	from	rep_definicao_hab_hem
	where	nr_seq_derivado = nr_seq_derivado_p
	and	((cd_setor_atendimento is null) or (cd_setor_atendimento = cd_setor_atendimento_p))
	and	((qt_peso_min is null and qt_peso_max is null) or
	     (qt_peso_p between nvl(qt_peso_min,0) and nvl(qt_peso_max,999)));
		 
	if 	(ie_opcao_p = 'A') then
		ds_retorno_w := nvl(ie_aliquotado_w,'S');
	end if;
	
end if;

return	ds_retorno_w;

end Obter_dados_regra_hab_deriv;
/
