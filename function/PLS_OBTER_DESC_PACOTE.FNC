create or replace
function pls_obter_desc_pacote
			(	nr_seq_pacote_p		Number,
				ie_tipo_retorno_p	Varchar2)
				return Varchar2 is
				
/* ie_tipo_retorno_p
	C - C�digo do procedimento
	D - Descri��o do procedimento
	CD - C�digo + Descri��o do procedimento
*/
		
cd_procedimento_w		Number(15);		
ds_procedimento_w		Varchar2(255);
ds_retorno_w			Varchar2(255);

begin

select	cd_procedimento,
	substr(obter_descricao_procedimento(cd_procedimento, ie_origem_proced),1,255)
into	cd_procedimento_w,
	ds_procedimento_w
from	pls_pacote
where	nr_sequencia	= nr_seq_pacote_p;

if	(ie_tipo_retorno_p	= 'C') then
	ds_retorno_w	:= cd_procedimento_w;
elsif	(ie_tipo_retorno_p	= 'D') then
	ds_retorno_w	:= ds_procedimento_w;
elsif	(ie_tipo_retorno_p	= 'CD') then
	ds_retorno_w	:= cd_procedimento_w || ' - ' || ds_procedimento_w;
end if;

return	ds_retorno_w;

end pls_obter_desc_pacote;
/