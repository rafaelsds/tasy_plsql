create or replace
function cm_obter_req_conjunto	(nr_seq_conj_real_p		number)
 		    	return number is

nr_seq_requisicao_w 	number(10);			
			
			
begin

if	(nvl(nr_seq_conj_real_p,0) > 0) then

	select	max(a.nr_seq_requisicao)
	into	nr_seq_requisicao_w	
	from	cm_requisicao_item a,
		cm_requisicao_conj b
	where	b.nr_seq_item_req = a.nr_sequencia
	and	b.nr_seq_conj_real = nr_seq_conj_real_p;

end if;
	
return	nr_seq_requisicao_w;

end cm_obter_req_conjunto;
/