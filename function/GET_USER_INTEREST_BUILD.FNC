CREATE OR REPLACE FUNCTION get_user_interest_build(
    nr_seq_sche_build_p NUMBER)
  RETURN VARCHAR2
IS
  build_interested_list_w VARCHAR2(350) := '';
BEGIN
  BEGIN
    FOR i IN
    (SELECT nm_user_build FROM build_user_interest WHERE nr_seq_sche_build = nr_seq_sche_build_p
    )
    LOOP
      build_interested_list_w := i.nm_user_build||'; '||build_interested_list_w;
    END LOOP;
  EXCEPTION
  WHEN OTHERS THEN
    build_interested_list_w := '';
  END;
RETURN build_interested_list_w;
END get_user_interest_build;
/