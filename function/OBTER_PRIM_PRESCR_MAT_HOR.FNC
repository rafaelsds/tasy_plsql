create or replace
function obter_prim_prescr_mat_hor(	nr_prescricao_p		number)
					return date is

dt_primeiro_horario_w	date	:= sysdate;
				
begin

select	nvl(min(dt_horario),sysdate+60)
into	dt_primeiro_horario_w
from	prescr_mat_hor
where	nr_prescricao = nr_prescricao_p
and	dt_suspensao is null;

return	dt_primeiro_horario_w;

end	obter_prim_prescr_mat_hor;
/