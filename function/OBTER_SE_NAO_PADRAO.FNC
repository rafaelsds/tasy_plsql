create or replace
function obter_se_nao_padrao	(nr_prescricao_p	number)
				return number is

qt_nao_padrao_w		number(10);

begin

select	count(*)
into	qt_nao_padrao_w
from	prescr_material b,
	prescr_medica a
where	a.nr_prescricao	= b.nr_prescricao
and	a.nr_prescricao	= nr_prescricao_p
and	obter_se_material_padronizado(a.cd_estabelecimento,b.cd_material) = 'N'
and	exists (select	1
		from	prescr_mat_hor x
		where	a.nr_prescricao	= x.nr_prescricao
		and	b.nr_sequencia	= x.nr_seq_material
		and	nvl(x.ie_dispensar_farm,'S') = 'S'
		and	x.dt_emissao_farmacia is null
		and	Obter_se_horario_liberado(x.dt_lib_horario, x.dt_horario) = 'S');

return qt_nao_padrao_w;

end obter_se_nao_padrao;
/