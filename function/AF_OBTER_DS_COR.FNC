create or replace
function Af_obter_ds_cor(nr_seq_status_p	number,
		        ie_opcao_p		Varchar2)
 		    	return Varchar2 is
			
ds_retorno_w	varchar2(255);			

begin

if (nr_seq_status_p is not null) and (ie_opcao_p = 'V') then

	select	ds_cor
	into	ds_retorno_w
	from	status_atend_futuro
	where	nr_sequencia = nr_seq_status_p
	and	ie_situacao = 'A';
	
elsif (nr_seq_status_p is not null) and (ie_opcao_p = 'A') then

	select	ds_cor_alerta
	into	ds_retorno_w
	from	status_atend_futuro
	where	nr_sequencia = nr_seq_status_p
	and	ie_situacao = 'A';	

end if;	
	
return	ds_retorno_w;

end Af_obter_ds_cor;
/