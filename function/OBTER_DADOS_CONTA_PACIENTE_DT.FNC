create or replace
function obter_dados_conta_paciente_dt(	nr_interno_conta_p		number,
					ie_opcao_p		varchar2)
 		    	return date is

/*ie_opcao_p

DVNF	- Data de vencimento da NF - Usado no repasse para terceiros (repasses pendentes) - ahoffelder 05/10/2010 OS 253507
DPI	- Per�odo inicial - jcaraujo 27/10/10 - OS261902
DPF	- Per�odo final - jcaraujo 27/10/10 - OS261902
DTA	- Data da alta do paciente
DTC	- data de cancelamento null
DR	- Data m�s ano refer�ncia

*/

dt_retorno_w		date;

nr_atendimento_w		number(15,0);
dt_periodo_inicial_w	date;
dt_periodo_final_w		date;
dt_mesano_referencia_w	date;

begin

select	max(dt_periodo_inicial),
	max(dt_periodo_final),
	max(dt_mesano_referencia),
	max(nr_atendimento)
into	dt_periodo_inicial_w,
	dt_periodo_final_w,
	dt_mesano_referencia_w,
	nr_atendimento_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

if	(ie_opcao_p = 'DVNF') then
	select	min(b.dt_vencimento)
	into	dt_retorno_w
	from	nota_fiscal_venc b,
		nota_fiscal a
	where	a.nr_interno_conta	= nr_interno_conta_p
	and	a.nr_sequencia	= b.nr_sequencia;

elsif	(ie_opcao_p = 'DPI') then
	dt_retorno_w	:= dt_periodo_inicial_w;
		
elsif	(ie_opcao_p = 'DPF') then
	dt_retorno_w	:= dt_periodo_final_w;
	
elsif	(ie_opcao_p = 'DTA') then
	select	dt_alta
	into	dt_retorno_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;
		
elsif	(ie_opcao_p = 'DR') then
	dt_retorno_w	:= dt_mesano_referencia_w;
	
end if;

return	dt_retorno_w;

end obter_dados_conta_paciente_dt;
/
