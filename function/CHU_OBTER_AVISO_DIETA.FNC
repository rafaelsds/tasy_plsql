create or replace
function chu_obter_aviso_dieta(cd_setor_atendimento_p		number)
 		    	return number is


			
qt_registros_w		number(10) := 0;
nr_unidades_ocupadas_w	number(10);
cd_refeicao_w		varchar2(15);

Cursor C01 is
	select	cd_refeicao
	from	horario_refeicao_dieta b
	where	ie_situacao = 'A'
	and	to_char(sysdate,'dd/mm/yyyy')||' '||ds_horarios = to_char(sysdate + 60/1440,'dd/mm/yyyy hh24:mi');

begin

select	nvl(max(nr_unidades_ocupadas),0)
into	nr_unidades_ocupadas_w
from	ocupacao_setores_v2
where	cd_setor_atendimento = cd_setor_atendimento_p;

if (nr_unidades_ocupadas_w = 0) then
	qt_registros_w := 1;
else

	qt_registros_w := 1;
	
	open C01;
	loop
	fetch C01 into	
		cd_refeicao_w;
	exit when C01%notfound;
		begin
		
		select  count(*)
		into	qt_registros_w
		from	mapa_dieta a
		where	a.cd_refeicao = cd_refeicao_w
		and	trunc(a.dt_dieta) = trunc(sysdate)
		and	a.cd_setor_atendimento = cd_setor_atendimento_p;
		
		end;
	end loop;
	close C01;
	
end if;

return	qt_registros_w;

end chu_obter_aviso_dieta;
/
