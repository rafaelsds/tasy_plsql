create or replace
function obter_desc_fator_stress(       nr_seq_fator_stress_p   number)
                                        return                  varchar2 is
ds_retorno_w            varchar2(100);
begin
select  ds_fator
into    ds_retorno_w
from    nut_fator_stress
where   nr_sequencia = nr_seq_fator_stress_p;
return  ds_retorno_w;
end obter_desc_fator_stress;
/