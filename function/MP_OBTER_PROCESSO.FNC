create or replace
function mp_obter_processo(nr_seq_proc_objeto_p	number)
						return number is

nr_seq_processo_w	number(10);

begin

if	(nr_seq_proc_objeto_p is not null) then
	select	max(nr_seq_processo)
	into	nr_seq_processo_w
	from	mp_processo_objeto
	where	nr_sequencia	= nr_seq_proc_objeto_p;
end if;

return	nr_seq_processo_w;

end mp_obter_processo;
/