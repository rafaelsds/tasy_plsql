create or replace function valida_dose_comulativa(cd_material_p            number,
                                                  qt_material_p            number,
                                                  cd_unidade_medida_p      varchar2,
                                                  cd_pessoa_fisica_p       varchar2, 
                                                  nr_ocorrencia_p          number,
                                                  dose_cumulativa_p        number,
                                                  cd_unidade_cumulativa_p  varchar2,
                                                  ds_mensagem_o            out varchar2) return varchar2 is  
  qt_total_dose_prescr_w     number(15, 6); 
  qt_dose_total_w            number(15, 6); 
  cd_estabelecimento_w       number(4) := wheb_usuario_pck.get_cd_estabelecimento;
  materiais_sem_conversao_w  number;
  valida_comulativa_o        varchar2(1);
  
  cursor cur_sem_conversao is
    select prescr_material.cd_material, count(*) materiais_sem_conversao
      from prescr_medica, prescr_material
     where prescr_medica.nr_prescricao = prescr_material.nr_prescricao
       and prescr_medica.cd_pessoa_fisica = cd_pessoa_fisica_p
       and (prescr_material.dt_liberacao is null and sysdate < prescr_medica.dt_validade_prescr)
       and upper(nvl(prescr_material.ie_status,'N')) not in ('INT', 'V')
       and prescr_material.dt_suspensao is null
       and not exists
           (select 1
              from material_conversao_unidade
             where material_conversao_unidade.cd_material = prescr_material.cd_material
               and material_conversao_unidade.cd_unidade_medida = prescr_material.cd_unidade_medida_dose)
       
       and prescr_material.cd_material in (SELECT material.cd_material
                                  FROM material_estab, 
                                       material, 
                                       medic_ficha_tecnica
                                 WHERE material.nr_seq_ficha_tecnica = medic_ficha_tecnica.nr_sequencia
                                   AND material_estab.cd_material = material.cd_material
                                   AND material_estab.cd_estabelecimento = cd_estabelecimento_w
                                   AND medic_ficha_tecnica.nr_sequencia = (select m.nr_seq_ficha_tecnica 
                                                                             from material m 
                                                                            where m.cd_material = cd_material_p))
     group by prescr_material.cd_material;
begin
  valida_comulativa_o := 'S';
     
  -- converts the dose in the configured mesurement if necessary qt_dose_total_w := qt_material_p;
  if (upper(cd_unidade_cumulativa_p) <> upper(cd_unidade_medida_p)) then 
    select obter_dose_convertida(cd_material_p,
                                 qt_material_p,
                                 cd_unidade_medida_p,
                                 cd_unidade_cumulativa_p)
      into qt_dose_total_w
      from dual;
  else
    qt_dose_total_w := NVL(qt_material_p,0);
  end if;
   
  qt_dose_total_w:=qt_dose_total_w * nr_ocorrencia_p;
    
  -- validates if there's any prescription without mesurement code informed 
  select count(*)
    into materiais_sem_conversao_w
    from material_conversao_unidade
   where cd_material = cd_material_p
     and cd_unidade_medida = cd_unidade_cumulativa_p;
  
  
  if (materiais_sem_conversao_w > 0) then 
    FOR I IN cur_sem_conversao LOOP
      if (I.materiais_sem_conversao > 0) then
        Wheb_mensagem_pck.exibir_mensagem_abort(1171764, 'cd_medicamento='||i.cd_material);
      END IF;
    END LOOP;
    
     -- sums all the prescripted doses in the same configured mesurement 
    select sum(obter_dose_convertida(pm2.cd_material,
                                     (pm2.qt_dose*pm2.nr_ocorrencia),
                                     pm2.cd_unidade_medida_dose,
                                     cd_unidade_cumulativa_p))
      into qt_total_dose_prescr_w
      from prescr_medica pm, prescr_material pm2
     where pm.nr_prescricao = pm2.nr_prescricao
       and pm.cd_pessoa_fisica = cd_pessoa_fisica_p
       and pm2.cd_material in (SELECT material.cd_material
                                FROM material_estab, 
                                     material, 
                                     medic_ficha_tecnica
                               WHERE material.nr_seq_ficha_tecnica = medic_ficha_tecnica.nr_sequencia
                                 AND material_estab.cd_material = material.cd_material
                                 AND material_estab.cd_estabelecimento = cd_estabelecimento_w
                                 AND medic_ficha_tecnica.nr_sequencia = (select m.nr_seq_ficha_tecnica 
                                                                           from material m 
                                                                          where m.cd_material = cd_material_p))
       and (pm2.dt_liberacao is null and sysdate < pm.dt_validade_prescr)
       and upper(nvl(pm2.ie_status,'N')) not in ('INT', 'V')
       and pm2.dt_suspensao is null;
      
    qt_dose_total_w := NVL(qt_dose_total_w,0) + nvl(qt_total_dose_prescr_w,0);
    -- verifys if the sum and the actual dose pass from the configured amount 
    if (nvl(qt_dose_total_w,0) < nvl(dose_cumulativa_p,0)) then 
      valida_comulativa_o := 'N';
    else
      ds_mensagem_o := Wheb_mensagem_pck.get_texto(1171765, 'cd_medicamento='||cd_material_p||';soma_dose='||qt_total_dose_prescr_w||';unidade='||cd_unidade_cumulativa_p);
    end if; 
  else 
    Wheb_mensagem_pck.exibir_mensagem_abort(1171764, 'cd_medicamento='||cd_material_p);
  end if; 
  
  return valida_comulativa_o;
end valida_dose_comulativa;
/
