create or replace
function obter_cd_setor_atend_padrao( 	nr_seq_proc_interno_p	in 	number,                              							
							nr_seq_rotina_p			in number,
							cd_estabelecimento_p	in number,
							cd_setor_atendimento_p	in number,
							nr_atendimento_p		in	number)						
		return number is
		
cd_setor_atendimento_w		procedimento_setor_atend.cd_setor_atendimento%type;
cd_procedimento_w			proc_interno.cd_procedimento%type;
ie_origem_proced_w			proc_interno.ie_origem_proced%type;
nr_seq_exame_w				prescr_procedimento.nr_seq_exame%type;
param_CPOE_493_w            varchar2(1) := 'N';
ie_retorno_w				varchar2(2) := 'N';

begin
	Obter_param_Usuario(924, 493, WHEB_USUARIO_PCK.get_cd_perfil, WHEB_USUARIO_PCK.get_nm_usuario, nvl(WHEB_USUARIO_PCK.get_cd_estabelecimento, obter_estabelecimento_ativo), param_CPOE_493_w);
	Obter_Proc_Tab_Interno(nr_seq_proc_interno_p, null, nr_atendimento_p, 0, cd_procedimento_w, ie_origem_proced_w);
	nr_seq_exame_w := obter_nr_seq_exame( nr_seq_proc_interno_p, nr_atendimento_p, nr_seq_rotina_p);
	
	if	(param_CPOE_493_w = 'N') then
		if	(nvl(nr_seq_exame_w, 0) > 0) then -- e exame?
			cd_setor_atendimento_w := Obter_setor_Atend_proc_lab(cd_estabelecimento_p,cd_procedimento_w,ie_origem_proced_w,null,cd_setor_atendimento_p,null,nr_seq_exame_w, nr_atendimento_p);
		else
			cd_setor_atendimento_w := obter_setor_atend_proc(cd_estabelecimento_p, cd_procedimento_w, ie_origem_proced_w, null, null, null, nr_seq_proc_interno_p, nr_atendimento_p);
		end if;
		
		if	(nvl(cd_setor_atendimento_w, 0) = 0) then
			cd_setor_atendimento_w := obter_setor_atend_proc_estab(cd_procedimento_w, ie_origem_proced_w, null, cd_setor_atendimento_p, null, nr_seq_proc_interno_p, nr_atendimento_p);
		end if;
	else
		cd_setor_atendimento_w := obter_setor_atual_paciente(nr_atendimento_p);
	end if;
	return cd_setor_atendimento_w;
	
end obter_cd_setor_atend_padrao;
/
