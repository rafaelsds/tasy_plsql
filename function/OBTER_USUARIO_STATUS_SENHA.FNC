create or replace
function obter_usuario_status_senha(nr_seq_status_p		Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(255);			

begin

if (nr_seq_status_p is not null) then

	begin
	
	select	max(ds_status)
	into	ds_retorno_w
	from	status_atendente_senha
	where	nr_sequencia = nr_seq_status_p;
	
	end;
	
end if;	

return	ds_retorno_w;

end obter_usuario_status_senha;
/