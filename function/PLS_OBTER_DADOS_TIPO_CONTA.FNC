create or replace
function pls_obter_dados_tipo_conta
			(	nr_seq_tipo_conta_p		number,
				ie_opcao_p			varchar2)
				return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter dados do tipo da conta
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	IE_OPCAO_P : 
	DS - Descri��o do tipo da conta
*/

ds_retorno_w			varchar2(255) := null;

begin
if	(nr_seq_tipo_conta_p is not null) then
	if	(ie_opcao_p = 'DS')then
		select	w.ds_tipo_conta ds
		into	ds_retorno_w
		from	pls_tipo_conta w
		where  	nr_sequencia 	= nr_seq_tipo_conta_p;
	end if;
end if;

return ds_retorno_w;

end pls_obter_dados_tipo_conta;
/