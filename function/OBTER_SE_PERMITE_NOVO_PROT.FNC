create or replace
function obter_se_permite_novo_prot(	ie_tipo_protocolo_p		varchar2,
					cd_convenio_p			number,
					cd_estabelecimento_p		number)
 		    	return varchar2 is

ie_retorno_w		varchar2(1) := 'S';
ie_regra_w		number(5,0);
ie_liberado_w		number(5,0);
ie_consiste_prot_prov_w	varchar2(1);
qt_prot_prov_w		number(10,0);
	
begin

select 	count(*)
into	ie_regra_w
from	tipo_prot_lib_conv
where	cd_convenio = cd_convenio_p              
and	cd_estabelecimento = cd_estabelecimento_p;

if (ie_regra_w > 0) then

	select 	count(*)
	into	ie_liberado_w
	from	tipo_prot_lib_conv
	where	cd_convenio = cd_convenio_p              
	and	cd_estabelecimento = cd_estabelecimento_p
	and	ie_tipo_protocolo = ie_tipo_protocolo_p;

	if (ie_liberado_w > 0) then
		
		select	nvl(max(ie_consiste_prot_prov),'N')
		into	ie_consiste_prot_prov_w
		from	tipo_prot_lib_conv
		where	cd_convenio = cd_convenio_p              
		and	cd_estabelecimento = cd_estabelecimento_p
		and	ie_tipo_protocolo = ie_tipo_protocolo_p;
		
		if	(ie_consiste_prot_prov_w = 'S') then
		
			select	count(*)
			into	qt_prot_prov_w
			from	protocolo_convenio
			where	cd_convenio = cd_convenio_p
			and	ie_tipo_protocolo = ie_tipo_protocolo_p
			and	cd_estabelecimento = cd_estabelecimento_p
			and	ie_status_protocolo = 1;
			
			if	(qt_prot_prov_w > 0) then
				ie_retorno_w	:= 'N';
			else
				ie_retorno_w	:= 'S';
			end if;
		
		else
			ie_retorno_w	:= 'S';
		end if;
		
	else
		ie_retorno_w	:= 'S';
	end if;
else
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end obter_se_permite_novo_prot;
/
