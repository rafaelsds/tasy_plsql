create or replace
function cpoe_obter_desc_interacao(nr_seq_interacao_p varchar2, ie_tipo_item_p varchar2)
			return varchar2 is

cd_item_interacao_w	varchar2(10);
ds_retorno_w		varchar2(300);
begin

if (ie_tipo_item_p = 'MAT' or ie_tipo_item_p = 'MCOMP1' or ie_tipo_item_p = 'MCOMP1' or ie_tipo_item_p = 'MCOMP1'
			or ie_tipo_item_p = 'MCOMP4' or ie_tipo_item_p = 'MCOMP5' or ie_tipo_item_p = 'MCOMP6'
			or ie_tipo_item_p = 'PMAT1' or ie_tipo_item_p = 'PMAT2' or ie_tipo_item_p = 'PMAT3') then

	if (ie_tipo_item_p = 'MAT') then
		select	cd_material
		into	cd_item_interacao_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'MCOMP1') then
		select	cd_mat_comp1
		into	cd_item_interacao_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'MCOMP2') then
		select	cd_mat_comp2
		into	cd_item_interacao_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'MCOMP3') then
		select	cd_mat_comp3
		into	cd_item_interacao_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'MCOMP4') then
		select	cd_mat_comp4
		into	cd_item_interacao_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'MCOMP5') then
		select	cd_mat_comp5
		into	cd_item_interacao_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'MCOMP6') then
		select	cd_mat_comp6
		into	cd_item_interacao_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'PMAT1') then
		select	cd_mat_proc1
		into	cd_item_interacao_w
		from	cpoe_procedimento
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'PMAT2') then
		select	cd_mat_proc2
		into	cd_item_interacao_w
		from	cpoe_procedimento
		where	nr_sequencia = nr_seq_interacao_p;
	elsif (ie_tipo_item_p = 'PMAT3') then
		select	cd_mat_proc3
		into	cd_item_interacao_w
		from	cpoe_procedimento
		where	nr_sequencia = nr_seq_interacao_p;
	end if;

	ds_retorno_w := cpoe_obter_item_incons(cd_item_interacao_w, ie_tipo_item_p);

elsif (ie_tipo_item_p = 'E' or ie_tipo_item_p = 'L' or ie_tipo_item_p = 'S') then
	select	cpoe_obter_item_incons(to_char(max(cd_material)), ie_tipo_item_p)
	into	ds_retorno_w
	from	cpoe_dieta
	where	nr_sequencia = nr_seq_interacao_p;
elsif (ie_tipo_item_p = 'J') then
	select	cpoe_obter_item_incons(to_char(max(nr_seq_tipo)), ie_tipo_item_p)
	into	ds_retorno_w
	from	cpoe_dieta
	where	nr_sequencia = nr_seq_interacao_p;
elsif (ie_tipo_item_p = 'O') then
	select	cpoe_obter_item_incons(to_char(max(cd_dieta)), ie_tipo_item_p)
	into	ds_retorno_w
	from	cpoe_dieta
	where	nr_sequencia = nr_seq_interacao_p;
end if;

return	substr(ds_retorno_w,1,255);

end cpoe_obter_desc_interacao;
/
