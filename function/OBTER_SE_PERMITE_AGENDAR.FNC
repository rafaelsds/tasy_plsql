create or replace
function Obter_Se_Permite_Agendar( nr_seq_agenda_lista_espera_p		number,
									ie_contato_p					varchar2 default 'N')
 		    	return varchar2 is

ds_retorno_w				varchar2(2) := 'S';				
qt_agend_list_espe_reg_w	number(10,0);				
qt_regra_ord_list_w			number(10,0);	


ie_estado_w			varchar2(1) := 'N';
ie_procedimento_w	varchar2(1) := 'N';
ie_especialidade_w	varchar2(1) := 'N';
ie_tipo_servico_w	varchar2(1) := 'N';
ie_municipio_w		varchar2(1) := 'N';

ie_tipo_w				varchar2(3);
nr_sequencia_cursor_w	number(10);
nr_sequencia_retorno_w	number(10);
cd_esp_medica_w			number(10);	
municipio_ibge_w		varchar(255);
cd_procedimento_w		number(10);
estado_w				varchar(255);
qt_pontuacao_w			number(10);



Cursor C01 is
	select	a.nr_sequencia,
			to_number(obter_score_paciente(a.nr_sequencia, 'T')) qt_pontuacao
	from	agenda_lista_espera a,
			regulacao_Atend b
	where	a.nr_seq_regulacao = b.nr_sequencia
	and		(ie_tipo_servico_w = 'N' or nvl(b.ie_tipo,0) = nvl(ie_tipo_w, 0))
	and		(ie_especialidade_w = 'N' or nvl(a.cd_especialidade,0) = nvl(cd_esp_medica_w,0))
	and		(ie_municipio_w = 'N' or   nvl((select max(substr(obter_compl_pf(a.cd_pessoa_fisica,1,'DM'),1,255))from dual),0) = nvl(municipio_ibge_w,0))
	and		(ie_procedimento_w = 'N' or nvl(a.cd_procedimento,0) = nvl(cd_procedimento_w,0))
	and		(ie_estado_w = 'N' or   nvl((select max(substr(obter_compl_pf(a.cd_pessoa_fisica,1,'UF'),1,255))from dual),0) = nvl(estado_w,0))
	and		UPPER(b.ie_status) not in ('CA', 'NG')  --Cancelado/Negado
	and		a.IE_STATUS_ESPERA = 'A'
	and   		NVL(a.IE_CONSORCIO,'N') = 'N'
	and 	( ie_contato_p = 'S' or substr(obter_status_contato_lista(a.nr_sequencia),1,255) is not null)
	and 	nvl(substr(obter_status_contato_lista(a.nr_sequencia),1,255),'N') not in('AR','PN')
	and 	not exists (select 1 from paciente_mutirao w where a.nr_sequencia = w.nr_seq_lista_espera and w.dt_exclusao is null 
						and w.cd_pessoa_fisica = a.cd_pessoa_fisica)	
	order by qt_pontuacao asc,
			 nr_sequencia desc;

begin

select 	count(*)
into	qt_agend_list_espe_reg_w
from	agenda_lista_espera 
where	nr_sequencia = nr_seq_agenda_lista_espera_p  
and		nr_seq_regulacao is not null;


select	count(*) 
into	qt_regra_ord_list_w
from	regra_ord_lista_espera 
where	ie_situacao = 'A';

if ( qt_agend_list_espe_reg_w > 0) then


	ds_retorno_w := 'N';

    if ( qt_regra_ord_list_w > 0) then
	
	
		select	nvl(max('S'),'N')
		into	ie_estado_w
		from	regra_ord_lista_espera 		
		where	ie_situacao = 'A'
		and		ie_prioridade  = 'ES';
		
		select	nvl(max('S'),'N')
		into	ie_procedimento_w
		from	regra_ord_lista_espera 		
		where	ie_situacao = 'A'
		and		ie_prioridade  = 'PR';
		
		
		select	nvl(max('S'),'N')
		into	ie_especialidade_w
		from	regra_ord_lista_espera 		
		where	ie_situacao = 'A'
		and		ie_prioridade  = 'EM';
		
		
		select	nvl(max('S'),'N')
		into	ie_tipo_servico_w
		from	regra_ord_lista_espera 		
		where	ie_situacao = 'A'
		and		ie_prioridade  = 'TS';
		
		select	nvl(max('S'),'N')
		into	ie_municipio_w
		from	regra_ord_lista_espera 		
		where	ie_situacao = 'A'
		and		ie_prioridade  = 'MI';
	
	
	end if;
	
	
	Select  max(b.ie_tipo),
			max(a.cd_especialidade),
			max(substr(obter_compl_pf(a.cd_pessoa_fisica,1,'DM'),1,255)),
			max(a.cd_procedimento),
			max(substr(obter_compl_pf(a.cd_pessoa_fisica,1,'UF'),1,255))
	into	ie_tipo_w,
			cd_esp_medica_w,
			municipio_ibge_w,
			cd_procedimento_w,
			estado_w	
	from	agenda_lista_espera a,
			regulacao_Atend b
	where	a.nr_sequencia = nr_seq_agenda_lista_espera_p
	and		a.nr_seq_regulacao = b.nr_Sequencia;

	
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_cursor_w,
		qt_pontuacao_w;
	exit when C01%notfound;
		begin
		nr_sequencia_retorno_w := nr_sequencia_cursor_w;
		end;
	end loop;
	close C01;
	
	if ( nr_sequencia_retorno_w = nr_seq_agenda_lista_espera_p ) then
		ds_retorno_w := 'S';
	end if;

else 
	ds_retorno_w := 'S';
end if;


return	ds_retorno_w;

end Obter_Se_Permite_Agendar;
/