create or replace
function pls_obter_gp_tipo_desp_analise
			(	nr_seq_grupo_p		number,
				nr_seq_conta_proc_p	number,
				nr_seq_conta_mat_p	number,
				nr_seq_proc_partic_p	number)
			return varchar2 is

ie_retorno_w		varchar2(1)	:= null;
ie_tipo_despesa_w	varchar2(1);
nr_seq_regra_w		number(10);
qt_oco_despesa_w	Number(10);
ie_tipo_despesa_aux_w	varchar2(10);
ie_tipo_w		varchar2(10);

cursor c01 is
	select	a.nr_sequencia,
		decode(ie_tipo_w,'P',a.ie_tipo_despesa_proc,a.ie_tipo_despesa_mat)
	from	pls_oc_grupo_tipo_desp	a
	where	a.nr_seq_ocorrencia_grupo	= nr_seq_grupo_p
	and	(a.ie_tipo_despesa_proc is not null or ie_tipo_w = 'M')
	and	(a.ie_tipo_despesa_mat is not null or ie_tipo_w = 'P')
	order by
		nvl(a.ie_tipo_despesa_mat,' '),
		nvl(a.ie_tipo_despesa_proc,' ');

begin

select	count(1)
into	qt_oco_despesa_w
from	pls_oc_grupo_tipo_desp
where	nr_seq_ocorrencia_grupo = nr_seq_grupo_p;

if	(qt_oco_despesa_w = 0) then
	ie_retorno_w := 'S';
	goto final;
end if;

if	(nr_seq_conta_proc_p is not null) then
	select	max(ie_tipo_despesa)
	into	ie_tipo_despesa_aux_w
	from	pls_conta_proc
	where	nr_sequencia	= nr_seq_conta_proc_p;
	
	ie_tipo_w	:= 'P';
end if;

if	(nr_seq_conta_mat_p is not null) then
	select	max(ie_tipo_despesa)
	into	ie_tipo_despesa_aux_w
	from	pls_conta_mat
	where	nr_sequencia	= nr_seq_conta_mat_p;
	
	ie_tipo_w	:= 'M';
end if;

if	(nr_seq_proc_partic_p is not null) then
	select	max(b.ie_tipo_despesa)
	into	ie_tipo_despesa_aux_w
	from	pls_conta_proc		b,
		pls_proc_participante	a
	where	a.nr_seq_conta_proc	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_proc_partic_p;
	
	ie_tipo_w	:= 'P';
end if;

ie_retorno_w	:= 'N';
open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	ie_tipo_despesa_w;
exit when C01%notfound;
	begin
	if	(ie_tipo_despesa_aux_w = ie_tipo_despesa_w) then
		ie_retorno_w	:= 'S';
		goto final;
	end if;
	end;
end loop;
close C01;

<<final>>
ie_retorno_w	:= nvl(ie_retorno_w,'S');

return ie_retorno_w;

end pls_obter_gp_tipo_desp_analise;
/
