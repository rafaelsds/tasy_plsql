create or replace
function cpoe_obter_dose_medic_pac(
					cd_pessoa_fisica_p		varchar2,
					cd_material_p			number,
					cd_unidade_medida_p		varchar2) return number is
					
qt_dose_retorno_w		prescr_material.qt_dose%type;

begin	
select	nvl(sum(obter_dose_convertida(a.cd_material, a.qt_dose, a.cd_unidade_medida, cd_unidade_medida_p)),0)
into	qt_dose_retorno_w
from	cpoe_pac_dose_material a
where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
and		a.cd_material		= cd_material_p;

return qt_dose_retorno_w;

end cpoe_obter_dose_medic_pac;
/