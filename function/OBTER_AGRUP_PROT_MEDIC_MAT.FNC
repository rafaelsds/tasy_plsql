create or replace
function obter_agrup_prot_medic_mat(
		cd_protocolo_p	number,
		nr_sequencia_p	number,
		ie_agrupador_p	number) return number is

nr_agrupamento_w	number(10,0);

begin
if	(cd_protocolo_p is not null) and
	(nr_sequencia_p is not null) then
	begin
	
	if	(ie_agrupador_p is null) or
		(ie_agrupador_p = 0) then
		begin
		
		select (nvl(max(nr_agrupamento),0) + 1)
		into	nr_agrupamento_w
		from	protocolo_medic_material
		where	cd_protocolo = cd_protocolo_p
		and	nr_sequencia = nr_sequencia_p
		and	ie_agrupador < 3;
		end;
	else
		begin		
		select (nvl(max(nr_agrupamento),0) + 1)
		into	nr_agrupamento_w
		from	protocolo_medic_material
		where	cd_protocolo = cd_protocolo_p
		and	nr_sequencia = nr_sequencia_p
		and	ie_agrupador = ie_agrupador_p;
		end;
	end if;	
	end;
end if;

return nr_agrupamento_w;
end obter_agrup_prot_medic_mat;
/