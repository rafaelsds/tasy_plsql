create or replace
function possui_regra_tipo_cons_oft	  ( nr_seq_consulta_p		number,
					    ie_opcao_p			varchar2)
 		    	return varchar2 is
			
ie_possui_w		varchar2(1)  := 'N';
nm_usuario_w		varchar2(15) := Wheb_usuario_pck.get_nm_usuario;
cd_perfil_w		number(10)   := wheb_usuario_pck.get_cd_perfil;
qt_regra_w		number(5);
nr_seq_tipo_consulta_w	number(10);

begin

if (nr_seq_consulta_p is not null) then
	select max(nr_seq_tipo_consulta)
	into   nr_seq_tipo_consulta_w
	from   oft_consulta
	where  nr_sequencia =  nr_seq_consulta_p;

	select count(*)
	into   qt_regra_w
	from   oft_tipo_consulta_item
	where  nr_seq_tipo_consulta = nr_seq_tipo_consulta_w;
	
	if (qt_regra_w > 0) then
		ie_possui_w := 'S';
			
	else 
		ie_possui_w := 'N';
	end if;
end if;

return	ie_possui_w;

end possui_regra_tipo_cons_oft;
/