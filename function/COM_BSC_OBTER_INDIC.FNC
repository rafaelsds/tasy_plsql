create or replace
function COM_BSC_Obter_Indic(	dt_referencia_p	Date,
				ie_opcao_p	Varchar2)
 		    		return number is

qt_retorno_w			Number(15,2);
dt_ref_mes_w			Date;
dt_fim_mes_w			Date;
qt_atual_w			Number(15,2);
qt_anterior_w			Number(15,2);
qt_total_w			Number(15,2);
qt_erro_w			Number(15,2);
qt_duvida_w			Number(15,2);
qt_solic_w			Number(15,2);
qt_sugest_w			Number(15,2);

/* Op��es
	Cliente - Obter os clientes ativos at� o m�s em quest�o
	Contrato - Obter os contratos em aberto no m�s em quest�o
	%ClienteAno - Obter o percentual de aumento de clientes de um ano para outro
	%ErroProduto - Obter o percentual de OS de Erro dentre as recebidas no m�s
	OS_Rec_Tecnologia - Obter o n�mero de OS recebidas no m�s pelo depto de tecnologia excluindo as de d�vida
	OS_Rec_Desenvolvimento - Obter o n�mero de OS recebidas no m�s pelo depto de desenvolvimento excluindo as de d�vida
	OS_Rec_Suporte - Obter o n�mero de OS recebidas no m�s pelo depto de suporte com classifica��o de d�vida ou Erro
*/

BEGIN
dt_ref_mes_w			:= PKG_DATE_UTILS.start_of(dt_referencia_p,'month',0);
dt_fim_mes_w			:= PKG_DATE_UTILS.END_OF(dt_ref_mes_w, 'MONTH', 0);
			
if	(ie_opcao_p = 'Cliente') then
	begin
	select	count(*)
	into	qt_retorno_w
	from	com_cliente a
	where	cd_cnpj not in ('01950338000177','81627838000535') --- Wheb e A��o
	and	ie_classificacao = 'C'
	and	a.ie_produto in ('I', 'M', 'MD', 'MDC', 'P', 'O')
	and	a.dt_revisao_prevista <= dt_fim_mes_w;
	end;
elsif	(ie_opcao_p = '%ClienteAno') then
	begin
	select	count(*)
	into	qt_atual_w
	from	com_cliente a
	where	cd_cnpj not in ('01950338000177','81627838000535') --- Wheb e A��o
	and	ie_classificacao = 'C'
	and	a.ie_produto in ('P','O')
	and	a.dt_revisao_prevista <= dt_fim_mes_w;
	select	count(*)
	into	qt_anterior_w
	from	com_cliente a
	where	cd_cnpj not in ('01950338000177','81627838000535') --- Wheb e A��o
	and	ie_classificacao = 'C'
	and	a.ie_produto in ('P','O')
	and	a.dt_revisao_prevista <= PKG_DATE_UTILS.END_OF(PKG_DATE_UTILS.ADD_MONTH(dt_fim_mes_w,-12,0), 'MONTH', 0);
	qt_retorno_w	:= (dividir(qt_atual_w, qt_anterior_w)-1)*100;
	end;
elsif	(ie_opcao_p = 'Contrato') then
	begin
	if	(dt_ref_mes_w = PKG_DATE_UTILS.start_of(sysdate,'month',0)) then
		select 	count(*)
		into	qt_retorno_w
		from 	com_cliente
		where 	cd_cnpj not in ('01950338000177','81627838000535') --- Wheb e A��o
 		and 	ie_classificacao = 'P'
		and	ie_fase_venda in ('C');
	else	
		select	sum(a.clientes)
		into	qt_retorno_w
		from	(	
		select	count(*) clientes
		from	com_cliente a
		where	cd_cnpj not in ('01950338000177','81627838000535') --- Wheb e A��o
		and	ie_classificacao = 'C'
		and	a.dt_revisao_prevista > dt_ref_mes_w
		and	exists (
			select 1
			from Com_cliente_log b
			where a.nr_sequencia = b.nr_seq_cliente
			and b.ie_log		= '1'
			and b.ie_fase_venda	in ('C')
			and PKG_DATE_UTILS.start_of(dt_log,'day',0)	<= dt_fim_mes_w)
		union
		select	count(*) prospect
		from	com_cliente a
		where	cd_cnpj not in ('01950338000177','81627838000535') --- Wheb e A��o
		and	ie_classificacao = 'P'
		and	exists (
			select 1
			from Com_cliente_log b
			where a.nr_sequencia = b.nr_seq_cliente
			and b.ie_log		= '1'
			and b.ie_fase_venda	in ('C')
			and PKG_DATE_UTILS.start_of(dt_log,'day',0)	<= dt_fim_mes_w)) a;
	end if;
	end;
elsif	(ie_opcao_p = '%ErroProduto') then
	begin
	select	count(*) qt_total_os, 
		sum(decode(COM_Obter_Tipo_Classif_OS(nr_sequencia),'W',1,0)) qt_erro_os
	into	qt_total_w,
		qt_erro_w
	from	man_ordem_servico
	where	dt_ordem_servico between dt_ref_mes_w and dt_fim_mes_w;
	qt_retorno_w	:= dividir(qt_erro_w, qt_total_w)*100;
	end;
elsif	(ie_opcao_p = 'OS_Rec_Suporte') then
	begin
	select  count(*) qt_total_os,
		sum(decode(ie_classificacao,'D',1,0)) qt_duvida_os,
		sum(decode(ie_classificacao,'E',1,0)) qt_erro_os,
		sum(decode(ie_classificacao,'S',1,0)) qt_solicitacao_os,
		sum(decode(ie_classificacao,'T',1,0)) qt_sugestao_os
	into	qt_total_w,
		qt_duvida_w,
		qt_erro_w,	
		qt_solic_w,
		qt_sugest_w
	from    man_ordem_servico a
	where   dt_ordem_servico between dt_ref_mes_w and dt_fim_mes_w
	and	obter_gerencia_grupo_sup(a.nr_seq_grupo_sup,'C') in (5,58)
	and exists (	select	1
			from	man_ordem_serv_estagio x,
				man_estagio_processo y
			where	x.nr_seq_estagio = y.nr_sequencia
			and	x.nr_seq_ordem = a.nr_sequencia
			and	x.nr_seq_estagio <> 511
			and	y.ie_suporte = 'S');
	qt_retorno_w	:= qt_duvida_w + qt_erro_w + qt_solic_w + qt_sugest_w;
	end;
elsif	(ie_opcao_p = 'OS_Rec_Tecnologia') then
	begin
	
	select  count(*) qt_total_os,
		sum(decode(ie_classificacao,'D',1,0)) qt_duvida_os,
		sum(decode(ie_classificacao,'E',1,0)) qt_erro_os,
		sum(decode(ie_classificacao,'S',1,0)) qt_solicitacao_os,
		sum(decode(ie_classificacao,'T',1,0)) qt_sugestao_os
	into	qt_total_w,
		qt_duvida_w,
		qt_erro_w,	
		qt_solic_w,
		qt_sugest_w
	from    man_ordem_servico a
	where   dt_ordem_servico between dt_ref_mes_w and dt_fim_mes_w
	and	nr_seq_grupo_des in (7,31,32,33)
	and exists 
	  (select 1 from man_ordem_serv_estagio x, man_estagio_processo y
	  where x.nr_seq_estagio = y.nr_sequencia
	  and x.nr_seq_ordem = a.nr_sequencia
	  and y.ie_tecnologia = 'S');

	qt_retorno_w	:= qt_solic_w + qt_sugest_w + qt_erro_w + qt_duvida_w;
	
	end;
elsif	(ie_opcao_p = 'OS_Rec_Desenvolvimento') then
	begin
	
	select  count(*) qt_total_os,
		sum(decode(ie_classificacao,'D',1,0)) qt_duvida_os,
		sum(decode(ie_classificacao,'E',1,0)) qt_erro_os,
		sum(decode(ie_classificacao,'S',1,0)) qt_solicitacao_os,
		sum(decode(ie_classificacao,'T',1,0)) qt_sugestao_os
	into	qt_total_w,
		qt_duvida_w,
		qt_erro_w,	
		qt_solic_w,
		qt_sugest_w
	from    man_ordem_servico a
	where   dt_ordem_servico between dt_ref_mes_w and dt_fim_mes_w
	and exists 
	  (select 1 from man_ordem_serv_estagio x, man_estagio_processo y
	  where x.nr_seq_estagio = y.nr_sequencia
	  and x.nr_seq_ordem = a.nr_sequencia
	  and y.ie_desenv = 'S')
	and	nr_seq_grupo_des not in (7,31,32,33);

	qt_retorno_w	:= qt_solic_w + qt_sugest_w + qt_erro_w + qt_duvida_w;
	end;
end if;

return	qt_retorno_w;

END COM_BSC_Obter_Indic;
/