create or replace
function man_obter_min_os_desenv(nr_seq_ordem_serv_p	Number)
 		    	return Number is

			
retorno_w	Number(10);
			
begin

select 	sum(QT_MINUTO)
	into	retorno_w
	from	MAN_ORDEM_SERV_ATIV a,
		usuario b,
		man_ordem_servico c,
		man_localizacao d
	where	a.NM_USUARIO_EXEC 	= b.nm_usuario
	and	a.nr_seq_ordem_serv 	= c.nr_sequencia
	and	c.nr_seq_localizacao	= d.nr_sequencia
	and	b.CD_SETOR_ATENDIMENTO 	= 2
	and	a.nr_seq_ordem_serv = nr_seq_ordem_serv_p;

return	retorno_w;

end man_obter_min_os_desenv;
/
