create or replace
function ageweb_obter_se_permite_canc(dt_agenda_p	date)
 		    	return varchar2 is

ie_permite_w		varchar2(1) := 'S';			
qt_min_cancelamento_w	number(10,0);
			
begin

select	nvl(max(qt_min_cancelamento),0)
into	qt_min_cancelamento_w
from	parametro_agenda_web;

if	((dt_agenda_p - qt_min_cancelamento_w/1440) < sysdate) then
	ie_permite_w := 'N';
end if;

return	ie_permite_w;

end ageweb_obter_se_permite_canc;
/