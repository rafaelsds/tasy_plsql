create or replace
function qt_obter_dur_aplicacao_pend(ds_dia_ciclo_p		varchar2, 
				nr_seq_prot_medic_p 	number,
				cd_protocolo_p		number,
				nr_seq_atendimento_p	number,
				dt_agenda_p		date,
				nr_seq_pendencia_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number)
 		    	return varchar2 is

qt_retorno_w			number(10,0);
ie_tempo_medic_w		varchar2(1)	:= 'N';
ie_classif_w			varchar2(1)	:= 'N';	
ds_retorno_w			varchar2(255);
ds_classif_w			varchar2(255);

qt_hora_aplicacao_w		number(3,0);
qt_tempo_aplic_soluc_w		number(15,4);
qt_min_dispositivo_w		number(10,0);
qt_min_aplicacao_w		number(4,0);
qt_min_dispositivo_soluc_w 	number(10,0);
qt_tempo_agenda_w		number(10,0);
nr_ciclo_w			number(5);

qt_duracao_w			number(10,0)    := 0;

Cursor C01 is
	select	nvl(qt_hora_aplicacao,0),
		nvl(qt_min_aplicacao,0),
		qt_duracao_dispositivo_inf(ie_bomba_infusao)
	from	paciente_atend_medic
	where	nr_seq_atendimento	= nr_seq_atendimento_p
	and	nr_seq_diluicao is null
	and	nr_seq_solucao is null
	order by 1;
	
Cursor C02 is
	select	nvl(qt_tempo_aplicacao,0),
		qt_duracao_dispositivo_inf(ie_bomba_infusao)
	from	paciente_atend_soluc
	where	nr_seq_atendimento	= nr_seq_atendimento_p;
	
begin

ie_tempo_medic_w	:= nvl(Obter_Valor_Param_Usuario(865, 24, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N');
ie_classif_w		:= nvl(Obter_Valor_Param_Usuario(865, 89, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N');

select	max(qt_tempo_agenda),
	max(nr_ciclo)
into	qt_tempo_agenda_w,
	nr_ciclo_w
from	paciente_atendimento
where	nr_seq_atendimento = nr_seq_atendimento_p;

if	(qt_tempo_agenda_w is not null) then
	qt_retorno_w := qt_tempo_agenda_w;
	ds_retorno_w := qt_retorno_w;
else
	if	(ie_tempo_medic_w = 'C') then

		qt_retorno_w	:= nvl(qt_obter_tempo_dur_dia(ds_dia_ciclo_p, nr_seq_prot_medic_p, cd_protocolo_p, nr_ciclo_w),0);

	elsif 	(ie_tempo_medic_w = 'S') then
	

		open C01;
		loop
		fetch C01 into	
			qt_hora_aplicacao_w,
			qt_min_aplicacao_w,
			qt_min_dispositivo_w;
		exit when C01%notfound;
			begin
			
			if	(qt_min_dispositivo_w is not null) then
				qt_duracao_w	:= qt_duracao_w + qt_min_dispositivo_w;
			else
				qt_duracao_w	:= qt_duracao_w + qt_min_aplicacao_w + (nvl(qt_hora_aplicacao_w,0) * 60);
			end if;
			
			end;
		end loop;
		close C01;

		
		open C02;
		loop
		fetch C02 into	
			qt_tempo_aplic_soluc_w,
			qt_min_dispositivo_soluc_w;
		exit when C02%notfound;
			begin
				
			if	(qt_min_dispositivo_soluc_w is not null) then
				qt_duracao_w	:= qt_duracao_w + qt_min_dispositivo_soluc_w;
			else
				qt_duracao_w	:= qt_duracao_w + (nvl(qt_tempo_aplic_soluc_w,0) * 60);
			end if;
			
			end;
		end loop;
		close C02;
		
		qt_retorno_w	:= qt_duracao_w;
		
	elsif	(ie_tempo_medic_w = 'T') then
		
		select	max(a.qt_tempo_medic)
		into	qt_retorno_w
		from	paciente_setor a,
			paciente_atendimento b
		where	b.nr_seq_pend_agenda	= nr_seq_pendencia_p
		and	a.nr_seq_paciente	= b.nr_seq_paciente
		and	trunc(nvl(b.dt_real, b.dt_prevista))	= trunc(dt_agenda_p);
		
		if	(qt_retorno_w is null) then
			open C01;
			loop
			fetch C01 into	
				qt_hora_aplicacao_w,
				qt_min_aplicacao_w,
				qt_min_dispositivo_w;
			exit when C01%notfound;
				begin
				
				if	(qt_min_dispositivo_w is not null) then
					qt_duracao_w	:= qt_duracao_w + qt_min_dispositivo_w;
				else
					qt_duracao_w	:= qt_duracao_w + qt_min_aplicacao_w + (nvl(qt_hora_aplicacao_w,0) * 60);
				end if;
				
				end;
			end loop;
			close C01;

			
			open C02;
			loop
			fetch C02 into	
				qt_tempo_aplic_soluc_w,
				qt_min_dispositivo_soluc_w;
			exit when C02%notfound;
				begin
					
				if	(qt_min_dispositivo_soluc_w is not null) then
					qt_duracao_w	:= qt_duracao_w + qt_min_dispositivo_soluc_w;
				else
					qt_duracao_w	:= qt_duracao_w + (nvl(qt_tempo_aplic_soluc_w,0) * 60);
				end if;
				
				end;
			end loop;
			close C02;
			
			qt_retorno_w	:= qt_duracao_w;
		end if;
		
		if	(qt_retorno_w = 0) then
			select	nvl(sum(nr_duracao),0)
			into	qt_retorno_w
			from	protocolo_medic_dur_dia
			where	ds_dia_ciclo 		= ds_dia_ciclo_p
			and	nr_seq_prot_medic	= nr_seq_prot_medic_p
			and	cd_protocolo		= cd_protocolo_p
			and	((nr_ciclo		= nr_ciclo_w) or (nr_ciclo is null));
		end if;
		
	elsif	(ie_tempo_medic_w = 'D') then
		
		qt_retorno_w	:= nvl(qt_obter_tempo_dur_dia(ds_dia_ciclo_p, nr_seq_prot_medic_p, cd_protocolo_p, nr_ciclo_w),0);
		
		if	(qt_retorno_w = 0) then
			select	max(a.qt_tempo_medic)
			into	qt_retorno_w
			from	paciente_setor a,
				paciente_atendimento b
			where	b.nr_seq_pend_agenda	= nr_seq_pendencia_p
			and	a.nr_seq_paciente	= b.nr_seq_paciente
			and	trunc(nvl(b.dt_real, b.dt_prevista))	= trunc(dt_agenda_p);
		end if;	
		
	elsif	(ie_tempo_medic_w = 'M') then
		select	nvl(sum(a.qt_hora_aplicacao),0),
				nvl(sum(a.qt_min_aplicacao),0)
		into	qt_hora_aplicacao_w,
				qt_min_aplicacao_w
		from	paciente_protocolo_medic a,
				paciente_atendimento b
		where	a.nr_seq_paciente = b.nr_seq_paciente
		and		b.nr_seq_atendimento	= nr_seq_atendimento_p
		and		obter_se_contido_char(ds_dia_ciclo_p, a.ds_dias_aplicacao)	= 'S';
		
		
		if	(qt_hora_aplicacao_w > 0) or
			(qt_min_aplicacao_w > 0) then
			qt_retorno_w	:= ((nvl(qt_hora_aplicacao_w,0) * 60) + nvl(qt_min_aplicacao_w,0));
		
		else
			if	(nvl(qt_retorno_w,0) = 0) then
				select	max(a.qt_tempo_medic)
				into	qt_retorno_w
				from	paciente_setor a,
						paciente_atendimento b
				where	b.nr_seq_pend_agenda	= nr_seq_pendencia_p
				and		a.nr_seq_paciente	= b.nr_seq_paciente
				and		trunc(nvl(b.dt_real, b.dt_prevista))	= trunc(dt_agenda_p);
			end if;
		end if;
	else
		select	max(a.qt_tempo_medic)
		into	qt_retorno_w
		from	paciente_setor a,
			paciente_atendimento b
		where	b.nr_seq_pend_agenda	= nr_seq_pendencia_p
		and	a.nr_seq_paciente	= b.nr_seq_paciente
		and	trunc(nvl(b.dt_real, b.dt_prevista))	= trunc(dt_agenda_p);

	end if;
	
	if	(ie_classif_w = 'S') then
		select	max(ds_classificacao)
		into	ds_classif_w
		from	qt_classif_duracao
		where	nr_minuto_duracao = qt_retorno_w;
	end if;
	
	if	(ds_classif_w is not null) then
		ds_retorno_w := substr(qt_retorno_w||' / '||ds_classif_w,1,255);
	else
		ds_retorno_w := qt_retorno_w;
	end if;
	
end if;

return	ds_retorno_w;

end qt_obter_dur_aplicacao_pend;
/
