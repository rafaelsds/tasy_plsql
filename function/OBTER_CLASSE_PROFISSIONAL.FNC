create or replace
function obter_classe_profissional(	nm_usuario_p		varchar2)
					return varchar2 is

ie_profissional_w	Varchar2(15);					

					
begin
if (nm_usuario_p is not null) then

	select	max(ie_profissional)
	into	ie_profissional_w
	from	usuario
	where	upper(nm_usuario) = upper(nm_usuario_p);

end if;
	
return	ie_profissional_w;

end obter_classe_profissional;
/