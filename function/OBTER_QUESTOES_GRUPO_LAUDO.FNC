create or replace
function Obter_questoes_grupo_laudo(nr_seq_laudo_p	number)
 		    	return varchar2 is

ds_texto_final_w	varchar2(2000);
ds_texto_w		varchar2(4000);

cursor c01 is
select	a.ds_texto
from	grupo_questao_laudo c,
	laudo_questao_item a,
	laudo_grupo_questao b
where	b.nr_seq_laudo		= nr_seq_laudo_p
and	a.nr_seq_laudo_grupo	= b.nr_sequencia
and	c.nr_sequencia		= b.nr_seq_grupo_questao
and	a.dt_liberacao		is not null
order by c.nr_seq_apres;

begin

open C01;
loop
fetch C01 into	
	ds_texto_w;
exit when C01%notfound;
	if	(ds_texto_final_w is null) then
		ds_texto_final_w	:= ds_texto_w;
	else
		ds_texto_final_w	:= ds_texto_final_w || chr(13) || ds_texto_w;
	end if;
end loop;
close C01;


return	ds_texto_final_w;

end Obter_questoes_grupo_laudo;
/
