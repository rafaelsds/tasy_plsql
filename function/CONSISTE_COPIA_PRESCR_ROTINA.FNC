create or replace
function consiste_copia_prescr_rotina
			(	nr_seq_proc_interno_p		Number,
				cd_procedimento_p		Number,
				ie_origem_proced_p		Number,
				nr_seq_exame_p			Number,
				ie_procedimento_p		Varchar2,
				ie_exames_p			Varchar2)
 		    		return Varchar2 is

ie_copia_w			Varchar2(1)	:= 'X';

begin

if	(ie_exames_p = 'S') and
	(nvl(nr_seq_exame_p,0)	> 0) then
	ie_copia_w	:= 'S';
elsif	(ie_procedimento_p = 'S') and
	(nvl(nr_seq_exame_p,0)	= 0) then
	ie_copia_w	:= 'S';
else
	if	(nvl(nr_seq_proc_interno_p,0)	> 0) then
		select	nvl(max(ie_copia),'X')
		into	ie_copia_w
		from	proc_interno
		where	nr_sequencia	= nr_seq_proc_interno_p;
	end if;

	if	((nvl(nr_seq_proc_interno_p,0) = 0) or 
		 (ie_copia_w = 'X')) and
		(nvl(cd_procedimento_p,0) > 0) then
		if	((ie_exames_p = 'C') and (nvl(nr_seq_exame_p,0) > 0)) or
			((ie_procedimento_p = 'C') and (nvl(nr_seq_exame_p,0) = 0)) then
			select nvl(max(ie_copia),'N')
			into	ie_copia_w
			from	procedimento_prescricao
			where	cd_procedimento		= cd_procedimento_p
			and	ie_origem_proced	= ie_origem_proced_p;
		end if;
	end if;
end if;

return	ie_copia_w;

end consiste_copia_prescr_rotina;
/
