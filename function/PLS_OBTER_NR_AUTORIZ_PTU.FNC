create or replace
function pls_obter_nr_autoriz_ptu
		(	nr_seq_item_p		Number,
			ie_tipo_item_p		Varchar2	)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(10) := '';

begin

if	(ie_tipo_item_p	= 'P') then

	select	max(nr_autorizacao)
	into	ds_retorno_w
	from	ptu_nota_servico
	where	nr_seq_conta_proc = nr_seq_item_p;

elsif	(ie_tipo_item_p	= 'M') then

	select	max(nr_autorizacao)
	into	ds_retorno_w
	from	ptu_nota_servico
	where	nr_seq_conta_mat = nr_seq_item_p;

end if;

return	ds_retorno_w;

end pls_obter_nr_autoriz_ptu;
/