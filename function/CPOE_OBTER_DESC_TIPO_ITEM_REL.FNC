create or replace
function cpoe_obter_desc_tipo_item_rel	(	ie_tipo_item_p		varchar2)
							return varchar2 is
				
ds_tipo_item_w		varchar2(240);
				
begin
if	(ie_tipo_item_p is not null) then

	if	(ie_tipo_item_p = 'M') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(308901); --'Medicamento';
	elsif	(ie_tipo_item_p = 'MAT') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(308902); --'Material';
	elsif	(ie_tipo_item_p = 'SOL') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(308904); --'Solucao';
	elsif	(ie_tipo_item_p = 'P') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(308905); --'Procedimento';
	elsif	(ie_tipo_item_p = 'O') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(460653); --'Dieta oral';
	elsif	(ie_tipo_item_p = 'SNE') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(294752); --'SNE';
	elsif	(ie_tipo_item_p = 'S') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(1041900); --'Suplemento';
	elsif	(ie_tipo_item_p = 'DI') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(309168); --'dialise';
	elsif	(ie_tipo_item_p = 'L') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(309821); --'Leites e derivados';
	elsif	(ie_tipo_item_p = 'E') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(1041901); --'Enteral';
	elsif	(ie_tipo_item_p = 'R') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(296989); --'Recomendacao';
	elsif	(ie_tipo_item_p = 'G') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(296653); --'Gasoterapia';
	elsif	(ie_tipo_item_p = 'AP') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(728072); --'Anatomia Patologia';			
	elsif	(ie_tipo_item_p = 'NPTA') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(305646); --'NPT Adulta';
	elsif	(ie_tipo_item_p = 'NPTI') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(988514); --'NPT Pediatrica';		
	elsif	(ie_tipo_item_p in ('H','HM')) then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(333812); --'Hemoterapia';		
	elsif	(ie_tipo_item_p = 'J') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(309541); --'Jejum';
	elsif	(ie_tipo_item_p = 'NANNPT') then
		ds_tipo_item_w	:= Wheb_mensagem_pck.get_texto(1024990); --'NPT Adulta/enteral'
	end if;	
	
end if;

return ds_tipo_item_w;

end cpoe_obter_desc_tipo_item_rel;
/
