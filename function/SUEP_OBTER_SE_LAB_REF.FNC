create or replace
function SUEP_OBTER_SE_LAB_REF(nr_prescricao_p	number,
					nr_seq_prescr_p		number,
					nr_seq_exame_p		number,
					nr_seq_material_p	number,
					ds_resultado_p	varchar2,
					qt_resultado_p	number,
					pr_resultado_p	 number) 
					return varchar2 is

ie_resultado_critico_w		varchar2(1);					
ie_tipo_valor_w 			number(1);
qt_dias_w					number(10,2);
qt_horas_w					number(10,0);
ie_sexo_w					varchar2(1);
nm_exame_w					varchar2(255);
ie_formato_resultado_w		varchar2(4);
qt_casas_decimais_dias_w	number(10,0) := 2;
cd_estabelecimento_w		prescr_medica.cd_estabelecimento%type;

begin

ie_resultado_critico_w	:= 'N';

qt_dias_w 	:= 0;
qt_horas_w	:= 0;

ie_formato_resultado_w := Obter_formato_result_exame(nr_seq_exame_p, nr_seq_material_p);
			
select Obter_dias_entre_datas_lab(obter_nascimento_prescricao(nr_prescricao_p),sysdate),
	Obter_Hora_Entre_datas(obter_nascimento_prescricao(nr_prescricao_p),sysdate),
	Obter_sexo_prescricao(nr_prescricao_p)
into	qt_dias_w,
		qt_horas_w,
		ie_sexo_w
from 	dual;

select 	nvl(max(cd_estabelecimento),0)
into	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select	decode(nvl(max(ie_idade_int_val_ref), 'N'), 'N', 2, 0)
into	qt_casas_decimais_dias_w
from	lab_parametro
where 	cd_estabelecimento = cd_estabelecimento_w;

if 	(ie_formato_resultado_w = 'P') then
	begin
		select ie_tipo_valor
		into	 ie_tipo_valor_w
		from exame_lab_padrao
		where ((ie_sexo = nvl(ie_sexo_w,'0')) or (ie_sexo = '0'))
		  and nvl(ie_gera_alerta,'N') = 'S'
		  and nr_seq_exame = nr_seq_exame_p
		  and (((nvl(trunc((qt_dias_w / 365.25),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'A')) or
			 ((nvl(trunc(((qt_dias_w / 365.25) * 12),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'M')) or
			 (((nvl(qt_dias_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'D')) or
			 (((nvl(qt_horas_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'H')))
		  and pr_resultado_p between qt_percent_min and qt_percent_max
		  and nvl(ie_situacao,'A') = 'A'
		order by nr_seq_material, ie_sexo, ie_tipo_valor;
		exception
			when others then
				ie_tipo_valor_w := 0;
	end;


	if (ie_tipo_valor_w = 2) then
		ie_resultado_critico_w := 'S';
	end if;

elsif	(ie_formato_resultado_w = 'V') then
	begin
		select nvl(max(ie_tipo_valor),0)
		into	 ie_tipo_valor_w
		from exame_lab_padrao
		where ((ie_sexo = nvl(ie_sexo_w,'0')) or (ie_sexo = '0'))
		  and nr_seq_exame = nr_seq_exame_p
		  and nvl(ie_gera_alerta,'N') = 'S'
		  and (((nvl(trunc((qt_dias_w / 365.25),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'A')) or
			 ((nvl(trunc(((qt_dias_w / 365.25) * 12),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'M')) or
			 (((nvl(qt_dias_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'D')) or
			 (((nvl(qt_horas_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'H')))
		  and qt_resultado_p between qt_minima and qt_maxima
		  and nvl(ie_situacao,'A') = 'A'
		order by nr_seq_material, ie_sexo, ie_tipo_valor;
		exception
			when others then
				ie_tipo_valor_w := 0;
	end;

	if (ie_tipo_valor_w = 2) then
		ie_resultado_critico_w := 'S';
	end if;

elsif	((ie_formato_resultado_w = 'D') or 
		(ie_formato_resultado_w = 'SM') or 
		(ie_formato_resultado_w = 'SDM')) then
	begin
		select ie_tipo_valor
		into	 ie_tipo_valor_w
		from exame_lab_padrao
		where ((ie_sexo = nvl(ie_sexo_w,'0')) or (ie_sexo = '0'))
		  and nr_seq_exame = nr_seq_exame_p
		  and nvl(ie_gera_alerta,'N') = 'S'
		  and (((nvl(trunc((qt_dias_w / 365.25),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'A')) or
			 ((nvl(trunc(((qt_dias_w / 365.25) * 12),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'M')) or
			 (((nvl(qt_dias_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'D')) or
			 (((nvl(qt_horas_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'H')))
		  and upper(ELIMINA_ACENTOS(ds_resultado_p)) like upper(ELIMINA_ACENTOS(DS_OBSERVACAO))
		  and nvl(ie_situacao,'A') = 'A'
		order by nr_seq_material, ie_sexo;
		exception
			when others then
				ie_tipo_valor_w := 0;			
	end;

	if (ie_tipo_valor_w = 2) then
		ie_resultado_critico_w := 'S';
	end if;

elsif (ie_formato_resultado_w = 'DV') then

	if(ds_resultado_p is not null) then
		if  ((ds_resultado_p <> somente_numero_virg_char(ds_resultado_p)) or
			(somente_numero_virg_char(ds_resultado_p) is null)) then

			begin
				select nvl(max(ie_tipo_valor),0)
				into	 ie_tipo_valor_w
				from exame_lab_padrao
				where ((ie_sexo = nvl(ie_sexo_w,'0')) or (ie_sexo = '0'))
				  and nr_seq_exame = nr_seq_exame_p
				  and nvl(ie_gera_alerta,'N') = 'S'
				  and (((nvl(trunc((qt_dias_w / 365.25),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'A')) or
					 ((nvl(trunc(((qt_dias_w / 365.25) * 12),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'M')) or
					 (((nvl(qt_dias_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'D')) or
					 (((nvl(qt_horas_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'H')))
				  and upper(ELIMINA_ACENTOS(trim(ds_resultado_p))) like upper(ELIMINA_ACENTOS(trim(DS_OBSERVACAO)))
				  and nvl(ie_situacao,'A') = 'A'
				order by nr_seq_material, ie_sexo;	
				
				exception
				when others then
					ie_tipo_valor_w := 0;		
			end;
		
		else
			
			begin
				select nvl(max(ie_tipo_valor),0)
				into	 ie_tipo_valor_w
				from exame_lab_padrao
				where ((ie_sexo = nvl(ie_sexo_w,'0')) or (ie_sexo = '0'))
				  and nr_seq_exame = nr_seq_exame_p
				  and nvl(ie_gera_alerta,'N') = 'S'
				  and (((nvl(trunc((qt_dias_w / 365.25),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'A')) or
					 ((nvl(trunc(((qt_dias_w / 365.25) * 12),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'M')) or
					 (((nvl(qt_dias_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'D')) or
					 (((nvl(qt_horas_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'H')))
				  and ds_resultado_p between qt_minima and qt_maxima
				  and nvl(ie_situacao,'A') = 'A'
				order by nr_seq_material, ie_sexo, ie_tipo_valor;	
				exception
				when others then
					ie_tipo_valor_w := 0;		
			end;
		end if;
	else 
		begin
			select nvl(max(ie_tipo_valor),0)
			into	 ie_tipo_valor_w
			from exame_lab_padrao
			where ((ie_sexo = nvl(ie_sexo_w,'0')) or (ie_sexo = '0'))
			  and nr_seq_exame = nr_seq_exame_p
			  and nvl(ie_gera_alerta,'N') = 'S'
			  and (((nvl(trunc((qt_dias_w / 365.25),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'A')) or
				 ((nvl(trunc(((qt_dias_w / 365.25) * 12),qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'M')) or
				 (((nvl(qt_dias_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'D')) or
				 (((nvl(qt_horas_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'H')))
			  and qt_resultado_p between qt_minima and qt_maxima
			  and nvl(ie_situacao,'A') = 'A'
			order by nr_seq_material, ie_sexo, ie_tipo_valor;	
			exception
			when others then
				ie_tipo_valor_w := 0;		
		end;
	end if;
	
	if (ie_tipo_valor_w = 2) then
		ie_resultado_critico_w := 'S';
	end if;
	
end if;

return ie_resultado_critico_w;

end SUEP_OBTER_SE_LAB_REF;
/