create or replace
function obter_ds_compatibilidade(
					ie_fator_rh_p		varchar2,
					ie_fator_rh_pai_p	varchar2)
					return varchar2 is

ds_retorno_w	varchar2(255);					
					
begin

ds_retorno_w	:= obter_texto_tasy(690044,null);
if	(ie_fator_rh_p = '-') and (ie_fator_rh_pai_p = '+') then
	ds_retorno_w :=	obter_texto_tasy(690045,null);
end if;

return	ds_retorno_w;	

end obter_ds_compatibilidade;
/