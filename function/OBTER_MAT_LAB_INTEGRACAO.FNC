CREATE OR REPLACE
FUNCTION obter_mat_lab_integracao(nr_seq_material_p	Number)
	RETURN VARCHAR2 IS

ds_retorno_w		Varchar2(50);

BEGIN

if	( nr_seq_material_p is not null ) then
	select 	max(a.cd_material_integracao)
	into	ds_retorno_w
	from   	material_exame_lab a,
		exame_lab_material b
	where  	a.nr_sequencia = b.nr_seq_material
	and	a.nr_sequencia = nr_seq_material_p;
end if;

RETURN ds_retorno_w;

END obter_mat_lab_integracao;
/