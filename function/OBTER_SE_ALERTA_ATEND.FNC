create or replace
function obter_se_alerta_atend	(nr_atendimento_p	number)
				return varchar2 is

ie_alerta_w	varchar2(1) := 'N';

begin
ie_alerta_w	:= 'N';
if	(nr_atendimento_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ie_alerta_w
	from	atendimento_alerta
	where	nr_atendimento = nr_atendimento_p
	and	ie_situacao ='A'
	and	nvl(dt_fim_alerta,sysdate) >= sysdate
	and	obter_se_tipo_alerta_lib(nr_seq_tipo_alerta,wheb_usuario_pck.get_nm_usuario)	= 'S';

end if;

return ie_alerta_w;

end obter_se_alerta_atend;
/
