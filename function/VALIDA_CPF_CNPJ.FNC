create or replace 
function valida_cpf_cnpj(cpf_cnpj_p varchar2) return varchar2 is

type array_dv is varray(2) of pls_integer;
v_array_dv_w 		array_dv := array_dv(0, 0);
cpf_digit_w		constant pls_integer := 11;
cnpj_digit_w		constant pls_integer := 14;
is_cpf_w		boolean;
is_cnpj_w		boolean;
v_cpf_number_w		varchar2(20);
total_w			number := 0;
coeficiente_w		number := 0;
dv1_w			number := 0;
dv2_w			number := 0;
digito_w		number := 0;
j			integer;
i			integer;

begin

if	(cpf_cnpj_p is null) then
	return 'N';
end if;
--retira os caracteres n�o num�ricos do cpf/cnpj caso seja enviado para valida��o um valor com a m�scara.
v_cpf_number_w	:= regexp_replace(cpf_cnpj_p, '[^0-9]');
--verifica se o valor passado � um cpf atrav�s do n�mero de d�gitos informados. cpf = 11
is_cpf_w	:= (length(v_cpf_number_w) = cpf_digit_w);
--verifica se o valor passado � um cnpj atrav�s do n�mero de d�gitos informados. cnpj = 14
is_cnpj_w := (length(v_cpf_number_w) = cnpj_digit_w);
if (is_cpf_w or is_cnpj_w) then
	total_w := 0;
else
	return 'N';
end if;
--armazena os valores de d�gitos informados para posterior compara��o com os d�gitos verificadores calculados.
dv1_w	:= to_number(substr(v_cpf_number_w, length(v_cpf_number_w) - 1, 1));
dv2_w	:= to_number(substr(v_cpf_number_w, length(v_cpf_number_w), 1));
v_array_dv_w(1) := 0;
v_array_dv_w(2) := 0;
--la�o para c�lculo dos d�gitos verificadores. � utilizado m�dulo 11 conforme norma da receita federal.
for j in 1 .. 2 loop
	total_w := 0;
	coeficiente_w := 2;
	for i in reverse 1 .. ((length(v_cpf_number_w) - 3) + j) loop
		digito_w	:= to_number(substr(v_cpf_number_w, i, 1));
		total_w		:= total_w + (digito_w * coeficiente_w);
		coeficiente_w	:= coeficiente_w + 1;
		if (coeficiente_w > 9) and is_cnpj_w then
			coeficiente_w	:= 2;
		end if;
	end loop;
	v_array_dv_w(j) := 11 - mod(total_w, 11);
	if (v_array_dv_w(j) >= 10) then
		v_array_dv_w(j)	:= 0;
	end if;
end loop;
--compara os d�gitos calculados com os informados para informar resultado.
if	((dv1_w = v_array_dv_w(1)) and (dv2_w = v_array_dv_w(2))) then
	return 'S';
else
	return 'N';
end if;
end valida_cpf_cnpj;
/