create or replace
function bsc_obter_variacao_periodo_ant(cd_ano_p		number,
					cd_periodo_p		number,
					nr_seq_indicador_p	number)
 		    	return number is

qt_real_w	number(15,2);
qt_real_ww	number(15,2);
pr_retorno_w	number(15,4);
			
begin

select	sum(a.qt_real)
into	qt_real_w
from	bsc_ind_inf a,
	bsc_indicador b
where	a.nr_seq_indicador	= b.nr_sequencia
and	a.nr_seq_indicador	= nr_seq_indicador_p
and	a.cd_ano		= cd_ano_p
and	a.cd_periodo		= cd_periodo_p - 11;

select	sum(a.qt_real)
into	qt_real_ww
from	bsc_ind_inf a,
	bsc_indicador b
where	a.nr_seq_indicador	= b.nr_sequencia
and	a.nr_seq_indicador	= nr_seq_indicador_p
and	a.cd_ano		= cd_ano_p - 1
and	a.cd_periodo		= cd_periodo_p;

pr_retorno_w	:= obter_variacao_entre_valor(qt_real_ww, qt_real_w);

return	pr_retorno_w;

end bsc_obter_variacao_periodo_ant;
/