create or replace
function sug_obter_dt_ultimo_his(	nr_sequencia_p		number)
				return date is

dt_retorno_w	date;

begin

select 	max(dt_historico) 
into	dt_retorno_w
from 	SUG_SUGESTAO_HIST 
where 	nr_seq_sugestao = nr_sequencia_p
and	dt_liberacao is not null;

return	dt_retorno_w;

end sug_obter_dt_ultimo_his;
/