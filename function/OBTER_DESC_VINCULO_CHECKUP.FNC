create or replace
function obter_desc_vinculo_checkup(ie_vinculo_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255) := '';			
			
begin

if	(ie_vinculo_p = 'P') then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308151); -- Pendente
	else if (ie_vinculo_p = 'E') then
		ds_retorno_w	:= wheb_mensagem_pck.get_texto(308152); -- Encontrado
		else if	(ie_vinculo_p = 'C') then
			ds_retorno_w	:= wheb_mensagem_pck.get_texto(308153); -- Confirmado
		end if;
	end if;
end if;

return	ds_retorno_w;

end obter_desc_vinculo_checkup;
/
