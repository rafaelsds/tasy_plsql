create or replace
function obter_motivo_cancel_ageserv(nr_sequencia_p		varchar2)
 		    	return varchar2 is
			
ds_motivo_dom_w	varchar2(255);
cd_motivo_cancel_w	agenda_consulta.cd_motivo_cancelamento%type;

begin


select 	max(cd_motivo_cancelamento)
into	cd_motivo_cancel_w
from	agenda_consulta
where	nr_sequencia = nr_sequencia_p;

if	(cd_motivo_cancel_w is not null) then

	select 	max(ds_valor_dominio)
	into	ds_motivo_dom_w
	from 	valor_dominio 
	where  	cd_dominio  = 1011
	and	vl_dominio = cd_motivo_cancel_w;
	
end if;

return	ds_motivo_dom_w;

end obter_motivo_cancel_ageserv;
/

