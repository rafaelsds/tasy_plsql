create or replace
function Obter_se_intervalo_estab( 	cd_intervalo_p		varchar2,
					cd_estabelecimento_p	number)
 		    	return varchar2 is

ie_mostrar_w	varchar2(1) := 'S';
cont_w		number(10);

begin

select	count(*)
into	cont_w
from	intervalo_estabelecimento
where	cd_intervalo	= cd_intervalo_p;

if	(cont_w > 0) then
	select	nvl(max(ie_mostrar),'S')
	into	ie_mostrar_w
	from	intervalo_estabelecimento
	where	cd_intervalo		= cd_intervalo_p
	and	cd_estab		= cd_estabelecimento_p;
end if;

return	ie_mostrar_w;

end Obter_se_intervalo_estab;
/
