create or replace 
function Obter_Qte_Avisos_Usuario(	nm_usuario_p		in varchar2,
				cd_aviso_p		in varchar2,
				ie_lib_setor_usuario_p	in varchar2,
				ie_consid_toda_regra_p	in varchar2  ) -- Troca OR por AND
				return number as

qt_retorno_w			number(15);
qt_retorno_ww			number(15);
ds_retorno_w			varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
cd_cargo_w			number(10);

begin

--cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');

if	(cd_aviso_p = 'CI') then
	Verifica_comunicacao_interna(nm_usuario_p,qt_retorno_w);
elsif	(cd_aviso_p = 'OCA') then

	select	count(*)
	into	qt_retorno_w
	from	w_processo_aprov_compra
	where	nm_usuario = nm_usuario_p;

elsif	(cd_aviso_p = 'DPA') then
	qua_obter_doc_usuario(	wheb_usuario_pck.get_cd_estabelecimento,
							nm_usuario_p,
							ie_lib_setor_usuario_p,
							ie_consid_toda_regra_p,
							qt_retorno_ww,
							qt_retorno_ww,
							qt_retorno_ww,
							qt_retorno_w,
							qt_retorno_ww,
							ds_retorno_w);
elsif	(cd_aviso_p = 'DPL') then
	qua_obter_doc_usuario(	wheb_usuario_pck.get_cd_estabelecimento,
							nm_usuario_p,
							ie_lib_setor_usuario_p,
							ie_consid_toda_regra_p,
							qt_retorno_ww,
							qt_retorno_ww,
							qt_retorno_ww,
							qt_retorno_ww,
							qt_retorno_w,
							ds_retorno_w);

elsif	(cd_aviso_p = 'RQV') then --Documento /Revisao/Validador
	
	cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
	
	select	count(*) qt_doc_revisao_val
	into	qt_retorno_w
	from 	qua_documento a
	where 	1 = 1
	and exists (	select 1 
			from qua_doc_revisao_validacao x, qua_doc_revisao y 
			where x.nr_seq_doc_revisao = y.nr_sequencia 
			and y.nr_seq_doc = a.nr_sequencia 
			and 	x.dt_validacao is null and x.cd_pessoa_validacao = cd_pessoa_fisica_w)
	and	a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and     ie_situacao = 'A';
	
elsif	(cd_aviso_p = 'DQV') then --Documento /Validador
	
	cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
	
	select	count(*) qt_doc_revisao_val
	into	qt_retorno_w
	from 	qua_documento a
	where 	1 = 1 
        AND	nr_seq_superior is null 
	AND	ie_situacao = 'A' 
	AND	nr_sequencia in (SELECT nr_sequencia
	FROM	qua_documento a
	WHERE a.cd_pessoa_validacao = cd_pessoa_fisica_w 
		AND	a.dt_validacao is null 
		AND	a.dt_elaboracao is not null OR exists (SELECT 1
	FROM	qua_doc_revisao x
	WHERE x.nr_seq_doc = a.nr_sequencia 
		AND	x.dt_validacao is null 
		AND	x.cd_pessoa_validacao = cd_pessoa_fisica_w) OR exists (SELECT 1
	FROM	qua_doc_revisao_validacao x,
		qua_doc_revisao y
	WHERE x.nr_seq_doc_revisao = y.nr_sequencia 
		AND	y.nr_seq_doc = a.nr_sequencia 
		AND	x.dt_validacao is null 
		AND	x.cd_pessoa_validacao = cd_pessoa_fisica_w) OR exists (SELECT 1
	FROM	qua_doc_validacao y
	WHERE y.nr_seq_doc = a.nr_sequencia 
		AND	y.dt_validacao is null 
	AND	y.cd_pessoa_validacao = cd_pessoa_fisica_w ));
							
elsif (cd_aviso_p = 'APL') then
	cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
	
	select	count(*)
	into	qt_retorno_w
	from  	med_avaliacao_paciente p,
		med_avaliacao_lote l	
	where 	p.nr_seq_lote = l.nr_sequencia
	and	p.cd_pessoa_fisica = cd_pessoa_fisica_w
	and   	p.nr_seq_lote > 0
	and   	p.dt_liberacao is null
	and	l.dt_liberacao is not null
	and   	sysdate < l.dt_validade;

elsif (cd_aviso_p = 'OSH') then	
	
	select	count(*)
	into	qt_retorno_w
	from	man_ordem_servico x
	where	substr(Man_Obter_Se_Novo_Hist(x.nr_sequencia, nm_usuario_p),1,3) = 'S'
	and	x.ie_status_ordem <> 3;
	
elsif (cd_aviso_p = 'ALE') then

	qt_retorno_w := obter_qtd_pacientes_medico(nm_usuario_p,-2223);

elsif (cd_aviso_p = 'CNL') then	
	select	count(*)
	into	qt_retorno_w
	from 	classificado
	where	sysdate between dt_inicio_classificado and dt_fim_classificado
	and		dt_leitura is null
	and		ie_situacao = 'A';
elsif (cd_aviso_p = 'TPC') then
	qt_retorno_w := qua_obter_qt_trein_pend_confir(obter_pessoa_fisica_usuario(nm_usuario_p,'C'));
	
elsif (cd_aviso_p = 'NPI') then

	select	count(*)
	into	qt_retorno_w
	from	proj_cron_etapa b,
		proj_cron_etapa_equipe a
	where	b.nr_sequencia = a.nr_seq_etapa_cron
	and	obter_usuario_pf(a.cd_programador) = nm_usuario_p
	and	b.ie_status_etapa = '2'
	and	trunc(b.dt_inicio_prev) <= trunc(sysdate);

elsif (cd_aviso_p = 'GV') then
	cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
	
	select	count(*)
	into	qt_retorno_w
	from	fin_gv_pend_aprov a,
		via_viagem b
	where 	b.nr_sequencia = a.nr_seq_viagem
	and	(((a.cd_pessoa_aprov = cd_pessoa_fisica_w) or (cd_pessoa_fisica_w = '0')) or
		((a.cd_aprov_sub = cd_pessoa_fisica_w) or (cd_pessoa_fisica_w = '0')));

elsif (cd_aviso_p = 'APC') then

	select	count(*)
	into	qt_retorno_w
	from	processo_aprov_compra a,
		w_processo_aprov_compra b
	where	a.nr_sequencia = b.nr_sequencia
	and	a.nr_seq_proc_aprov = b.nr_seq_proc_aprov
	and	b.nm_usuario = nm_usuario_p
	and	((obter_dados_parametro_compras(wheb_usuario_pck.get_cd_estabelecimento,32) = 'N') or
		((obter_dados_parametro_compras(wheb_usuario_pck.get_cd_estabelecimento,32) = 'S') and not exists( select	1
														   from		processo_aprov_compra x
														   where	a.nr_sequencia = x.nr_sequencia
														   and		x.nm_usuario_aprov = b.nm_usuario)));
elsif (cd_aviso_p = 'ATL') then

	select	count(*)
	into	qt_retorno_w
	from	ajuste_versao_cliente a
	where	a.ie_compila <> 'S';

elsif (cd_aviso_p = 'AAV') then

	select	count(*)
	into	qt_retorno_w
	from	agendamento_versao a
	where	trunc(sysdate) between trunc(a.dt_aviso_ini) and trunc(a.dt_aviso_fim)
	and	a.ie_situacao = 'A';

elsif (cd_aviso_p = 'TMP') then
	cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
	
	select 	count(*)
	into 	qt_retorno_w
	from  	qms_treinamento
	where 	cd_responsavel = cd_pessoa_fisica_w
	and   	dt_liberacao is not null
	and	coalesce(ie_situacao, 'A') = 'A'
	and 	dt_confirmacao is null;

elsif (cd_aviso_p = 'TPP') then
	cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
	
	select 	count(*)
	into 	qt_retorno_w
	from  	qms_treinamento
	where 	cd_pessoa_fisica = cd_pessoa_fisica_w
	and	coalesce(ie_situacao, 'A') = 'A'
	and   	dt_liberacao is not null
	and 	dt_confirmacao is null;
elsif (cd_aviso_p = 'TPPG') then
	cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
	
	select 	count(*)
	into 	qt_retorno_w
	from  	qms_treinamento
	where 	cd_gerente_pessoa = cd_pessoa_fisica_w
	and	coalesce(ie_situacao, 'A') = 'A'
	and   	dt_liberacao is not null
	and	nvl(ie_analise_eficacia, 'S') = 'N'
	and 	dt_confirmacao is null;

elsif (cd_aviso_p = 'PEP') then
	cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
	
	select	count(*)
	into 	qt_retorno_w
	from  	qms_treinamento a
	where 	a.cd_gerente_pessoa = cd_pessoa_fisica_w
	and	nvl(a.ie_analise_eficacia, 'N') = 'S'
	and	a.dt_confirmacao is null;
					
elsif (cd_aviso_p = 'CIP') then
	qt_retorno_w := 0;
														  
	/*OS 2245738
	select	count(*)
	into	qt_retorno_w
	from	tasy_indice_pendente
	where	ie_status <> 'G'; */
	
end if;

return	qt_retorno_w;

end Obter_Qte_Avisos_Usuario;
/
