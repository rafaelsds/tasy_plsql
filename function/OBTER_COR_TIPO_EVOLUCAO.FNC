create or replace
function obter_cor_tipo_evolucao ( cd_tipo_evolucao_p	varchar2)
				return varchar2 is

ds_cor_w		varchar2(15);

begin

select	substr(ds_cor,1,15)
into	ds_cor_w
from	tipo_evolucao
where	cd_tipo_evolucao = cd_tipo_evolucao_p;

return ds_cor_w;

end obter_cor_tipo_evolucao;
/