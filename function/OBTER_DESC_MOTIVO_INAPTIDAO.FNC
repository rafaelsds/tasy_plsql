create or replace
function Obter_Desc_Motivo_Inaptidao
		(cd_motivo_inaptidao_p	number)
		return	varchar2 is

ds_motivo_w		varchar2(80);

begin

if	(cd_motivo_inaptidao_p is not null) then
	begin

	select	ds_motivo_inaptidao
	into	ds_motivo_w
	from	san_motivo_inaptidao
	where	nr_sequencia	= cd_motivo_inaptidao_p
	and	ie_situacao 	= 'A';
	
	end;
end if;

return	ds_motivo_w;

end Obter_Desc_Motivo_Inaptidao;
/