create or replace
function cus_obter_regra_custo_mat(	cd_estabelecimento_p	number,
				cd_material_p		number)
				return number is

cd_grupo_material_w		number(10);
cd_subgrupo_material_w		number(10);
cd_classe_material_w		number(10);
cd_material_w			number(10);
cd_retorno_w			number(10);

cursor c01 is
select	a.cd_tabela_preco
from	cus_regra_custo_material a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	nvl(a.cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material, cd_subgrupo_material_w) 	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	nvl(a.cd_material, cd_material_w)			= cd_material_w
order by nvl(a.cd_material,0),
	 nvl(a.cd_classe_material,0),
	 nvl(a.cd_subgrupo_material,0),
	 nvl(a.cd_grupo_material,0);

begin

cd_material_w	:= cd_material_p;

select	max(a.cd_grupo_material),
	max(a.cd_subgrupo_material),
	max(a.cd_classe_material)
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v a
where	a.cd_material	= cd_material_p;

open C01;
loop
fetch C01 into	
	cd_retorno_w;
exit when C01%notfound;
	begin
	cd_retorno_w	:= cd_retorno_w;
	end;
end loop;
close C01;

return	cd_retorno_w;

end cus_obter_regra_custo_mat;
/