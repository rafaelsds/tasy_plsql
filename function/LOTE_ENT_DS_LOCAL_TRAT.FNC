create or replace
function lote_ent_ds_local_trat(nr_seq_trat_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);						
				
begin

if (nr_seq_trat_p is not null) then

	select	ds_local_trat
	into	ds_retorno_w
	from	LOTE_ENT_LOCAL_TRAT
	where	nr_sequencia = nr_seq_trat_p;

end if;

return	ds_retorno_w;

end lote_ent_ds_local_trat;
/