create or replace
function obter_se_corpo_assist(cd_pessoa_fisica_p	varchar)
	return varchar2 is

ie_retorno_w	varchar2(1);

begin

select	ie_corpo_assist
into	ie_retorno_w
from	medico
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

return	ie_retorno_w;

end	obter_se_corpo_assist;
/
