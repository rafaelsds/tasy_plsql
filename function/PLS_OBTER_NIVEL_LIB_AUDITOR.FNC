create or replace
function pls_obter_nivel_lib_auditor
			(	nr_seq_grupo_p		number,
				nm_usuario_p		varchar2)
				return number is
				
nr_retorno_w			number(5) := null;
nr_seq_nivel_lib_w		number(10);

begin


begin
	select	a.nr_seq_nivel_lib
	into	nr_seq_nivel_lib_w
	from	pls_membro_grupo_aud a,
		pls_grupo_auditor b	
	where	a.nr_seq_grupo = nr_seq_grupo_p
	and	a.nm_usuario_exec = nm_usuario_p
	and	a.nr_seq_grupo = b.nr_sequencia;
exception
when others then
	nr_seq_nivel_lib_w := 0;
end;

if	(nvl(nr_seq_nivel_lib_w,0) = 0) then	
	
	begin
		select	nr_seq_nivel_lib
		into	nr_seq_nivel_lib_w
		from	pls_grupo_auditor
		where	nr_sequencia = nr_seq_grupo_p;
	exception
	when others then
		nr_seq_nivel_lib_w := 0;
	end;
end if;	
	
if	(nvl(nr_seq_nivel_lib_w,0) > 0) then
	
	begin
	select	nr_nivel_liberacao
	into	nr_retorno_w
	from	pls_nivel_liberacao
	where	nr_sequencia = nr_seq_nivel_lib_w;		
	exception
	when others then
		nr_retorno_w := null; 
	end;
end if;

return	nr_retorno_w;

end pls_obter_nivel_lib_auditor;
/