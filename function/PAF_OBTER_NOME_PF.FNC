create or replace
function paf_obter_nome_pf(
			cd_pessoa_fisica_p 	varchar2)
			return varchar2 is

ds_retorno_w	varchar2(100);

begin

if	(cd_pessoa_fisica_p is not null) then

	select	max(nm_pessoa_fisica)
	into	ds_retorno_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
end if;
	
return ds_retorno_w;

end paf_obter_nome_pf;
/