create or replace
function pls_obter_se_varios_produtos(
		nr_seq_contrato_p	number)
		return number is
		
qt_produtos_w		number;

begin

select  count(nr_sequencia)
into	qt_produtos_w
from    pls_contrato_plano
where   nr_seq_contrato = nr_seq_contrato_p;

return qt_produtos_w;

end pls_obter_se_varios_produtos;
/