create or replace
function GET_SERVICE_AUT_RULE
		(cd_convenio_p		in	number,
		cd_estabelecimento_p	in	number,
		ie_type_message_p		in	varchar2) return number is

nr_sequencia_w		message_type_autorization.nr_sequencia%type;

begin
select 	min(nr_sequencia)
into 	nr_sequencia_w
from 	MESSAGE_TYPE_AUTORIZATION
where 	ie_situacao 		= 'A'
and 	ie_type_message  	= ie_type_message_p
and 	nr_seq_insur_aut 	= (select 	min(nr_sequencia)
								from 	INSURANCE_AUT_SERVICE
								where 	cd_convenio 		= cd_convenio_p
								and 	cd_estabelecimento 	= cd_estabelecimento_p
								and 	ie_situacao = 'A');

return	nr_sequencia_w;
end GET_SERVICE_AUT_RULE;
/
