create or replace
function INGOH_obter_se_exame_valido(	nr_seq_doacao_p		number,
					nr_seq_producao_p 	number,
					ie_tipo_exame_p		varchar2)
 		    	return varchar2 is

ds_retorno1_w	varchar2(20);			
ds_retorno2_w	varchar2(20);					
ds_retorno3_w	varchar2(20);					
begin

	
if	(ie_tipo_exame_p = 'S') or (ie_tipo_exame_p = 'P')  then

	select	decode(count(*),0,'Liberado','')
	into	ds_retorno1_w
	from	san_exame_lote a,
		san_exame_realizado b
	where	a.nr_sequencia = b.nr_seq_exame_lote
	and	((b.dt_liberacao is null) or (upper(b.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')))
	and	a.nr_seq_doacao = nr_seq_doacao_p
	--and	b.nr_sequencia in (17,10,11,12,16,13,14,15,9,8,20)
	and	san_obter_destino_exame(b.nr_seq_exame,3) = 'S';
	
	
	if	(ds_retorno1_w = '') or (ds_retorno1_w is null) then
		select	decode(count(*),0,'','Andamento')
		into	ds_retorno1_w
		from	san_exame_lote a,
			san_exame_realizado b
		where	a.nr_sequencia = b.nr_seq_exame_lote
		and	((b.dt_liberacao is null) or (upper(b.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')))
		and	a.nr_seq_doacao= nr_seq_doacao_p						
		and	b.nr_seq_exame in (10,11,12);				
	end if;

	if	(ds_retorno1_w = '') or (ds_retorno1_w is null) then
		select	decode(count(*),0,'','Andamento')
		into	ds_retorno1_w
		from	san_exame_lote a,
			san_exame_realizado b
		where	a.nr_sequencia = b.nr_seq_exame_lote				
		and	a.nr_seq_doacao = nr_seq_doacao_p
		--and	b.nr_sequencia in (17,16,13,14,15,9,8,20)
		and	san_obter_destino_exame(b.nr_seq_exame,3) = 'S'
		and	((b.dt_liberacao is null) or (upper(b.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')));			
	end if;	
	
	if	(ie_tipo_exame_p = 'S') then
		return	ds_retorno1_w;
	end if;	
	
	if	(ds_retorno1_w = '') or (ds_retorno1_w is null) then
		ds_retorno1_w := 'Andamento';
	end if;
end if;

if	(ie_tipo_exame_p = 'I') or (ie_tipo_exame_p = 'P') then  
	
	select	decode(count(*),0,'Liberado','Ocorrência')
	into	ds_retorno2_w
	from	san_exame_lote a,
		san_exame_realizado b
	where	a.nr_sequencia = b.nr_seq_exame_lote
	and	((b.dt_liberacao is null) or (upper(b.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')))
	and	a.nr_seq_doacao = nr_seq_doacao_p
	--and	b.nr_sequencia in (17,10,11,12,16,13,14,15,9,8,20)
	and	san_obter_destino_exame(b.nr_seq_exame,0) = 'S'
	and	b.nr_seq_exame not in (1,2,36);
	
	if	(ie_tipo_exame_p = 'I') then
		return ds_retorno2_w;
	end if;
end if;	

if	(ie_tipo_exame_p = 'F') or (ie_tipo_exame_p = 'P') then
	
	select  decode(count(*),0,'Ocorrência','Liberado')
	into	ds_retorno3_w
	from	san_prod_analise_critica a		
	where	nr_seq_producao = nr_seq_producao_p
	and	nr_seq_analise_critica = 7;
	
	if	(ie_tipo_exame_p = 'F') then
		return ds_retorno3_w;
	end if;
end if;	

if	(ie_tipo_exame_p = 'P') then
	if	((ds_retorno1_w = 'Liberado') and
		(ds_retorno2_w = 'Liberado') and
		(ds_retorno3_w = 'Liberado')) then
		return 'L';
	elsif	((ds_retorno1_w = 'Andamento') or
		(ds_retorno2_w = 'Ocorrência') or
		(ds_retorno3_w = 'Ocorrência')) then
		return 'D';
	else
		return '';
	end if;	

end if;
	


--return	ds_retorno1_w;

end INGOH_obter_se_exame_valido;
/