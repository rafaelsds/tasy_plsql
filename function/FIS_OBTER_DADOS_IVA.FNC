create or replace
function fis_obter_dados_iva
			(	nr_sequencia_nf_p 	number,		
				ie_opcao_p     		number,
				dt_inicio_p		date default sysdate,
				dt_fim_p		date default sysdate)
				return number is

vl_retorno_w 		number(15,2) := 0;

/* Op��es
1- IVA 16%
2- IVA Isento
3- IVA 0%
4- IVA Nota de cr�dito
5- IVA Retido
6- IVA nota de cr�dito
7  IVA 8%
*/

begin

if (nr_sequencia_nf_p > 0) then

	if ie_opcao_p = 1 then

		select 	nvl(sum((a.qt_item_nf * a.vl_unitario_item_nf)),0) -- pedido por danny OS 1815589
		into 	vl_retorno_w
		from	nota_fiscal_item a,
			nota_fiscal_item_trib b,
			tributo c
		where	a.nr_sequencia = b.nr_sequencia
		and 	a.nr_item_nf = b.nr_item_nf
		and	b.cd_tributo = c.cd_tributo
		and	c.ie_tipo_tributo = 'IVA'
		and	c.ie_isento = 'N'
		and 	b.tx_tributo = 16
		and	a.nr_sequencia   = nr_sequencia_nf_p;

	elsif ie_opcao_p = 2 then

		select 	nvl(sum(((a.qt_item_nf * a.vl_unitario_item_nf) - a.vl_desconto)),0)
		into 	vl_retorno_w
		from	nota_fiscal_item a,
			nota_fiscal_item_trib b,
			tributo c
		where	a.nr_sequencia = b.nr_sequencia
		and 	a.nr_item_nf = b.nr_item_nf
		and	b.cd_tributo = c.cd_tributo
		and	c.ie_tipo_tributo = 'IVA'
		and	c.ie_isento = 'S'
		and 	b.tx_tributo = 0
		and	a.nr_sequencia   = nr_sequencia_nf_p;

	elsif ie_opcao_p = 3 then

		select 	nvl(sum(((a.qt_item_nf * a.vl_unitario_item_nf) - a.vl_desconto)),0)
		into 	vl_retorno_w
		from	nota_fiscal_item a,
			nota_fiscal_item_trib b,
			tributo c
		where	a.nr_sequencia = b.nr_sequencia
		and 	a.nr_item_nf = b.nr_item_nf
		and	b.cd_tributo = c.cd_tributo
		and	c.ie_tipo_tributo = 'IVA'
		and	c.ie_isento = 'N'
		and 	b.tx_tributo = 0
		and	a.nr_sequencia   = nr_sequencia_nf_p;

	elsif ie_opcao_p = 4 then

		select 	nvl(sum(b.vl_tributo), 0)
		into 	vl_retorno_w
		from	nota_fiscal_item a,
			nota_fiscal_item_trib b,
			tributo c,
			nota_fiscal x
		where	a.nr_sequencia = b.nr_sequencia
		and 	a.nr_item_nf = b.nr_item_nf
		and	b.cd_tributo = c.cd_tributo
		and	a.nr_sequencia = x.nr_sequencia
		and	c.ie_tipo_tributo = 'IVA'
		and	c.ie_isento = 'N'
		and 	b.tx_tributo = 16
		and	a.nr_sequencia   = nr_sequencia_nf_p
		and	(select max(z.ie_nota_credito) from operacao_nota z where z.cd_operacao_nf = x.cd_operacao_nf) = 'S';
		
	elsif ie_opcao_p = 5 then

		select 	nvl(sum(b.vl_tributo),0)
		into 	vl_retorno_w
		from	nota_fiscal_item a,
			nota_fiscal_item_trib b,
			tributo c
		where	a.nr_sequencia = b.nr_sequencia
		and 	a.nr_item_nf = b.nr_item_nf
		and	b.cd_tributo = c.cd_tributo
		and	c.ie_tipo_tributo = 'IVAR'
		and	c.ie_isento = 'N'
		--and 	b.tx_tributo = 10.6666
		and	a.nr_sequencia   = nr_sequencia_nf_p;	
		
	elsif ie_opcao_p = 6 then

		select 	nvl(sum(b.vl_tributo),0)
		into 	vl_retorno_w
		from	nota_fiscal_item a,
			nota_fiscal_item_trib b,
			tributo c,
			nota_fiscal d
		where	a.nr_sequencia = b.nr_sequencia
		and	a.nr_sequencia = d.nr_sequencia
		and 	a.nr_item_nf = b.nr_item_nf
		and	b.cd_tributo = c.cd_tributo
		and	c.ie_tipo_tributo = 'IVA'
		and	c.ie_isento = 'N'
		and 	b.tx_tributo = 16
		--and	trunc(d.dt_emissao) between dt_inicio_p and dt_fim_p
		and	a.nr_sequencia in (select nr_sequencia
					   from   nota_fiscal
					   where  nr_sequencia_ref = nr_sequencia_nf_p
					   and	  obter_se_nf_ativa(nr_sequencia_ref) = 'S');
					   
					   
	elsif ie_opcao_p = 7 then	

		select 	nvl(sum((a.qt_item_nf * a.vl_unitario_item_nf)),0) -- pedido por danny OS 1815589
		into 	vl_retorno_w
		from	nota_fiscal_item a,
			nota_fiscal_item_trib b,
			tributo c
		where	a.nr_sequencia = b.nr_sequencia
		and 	a.nr_item_nf = b.nr_item_nf
		and	b.cd_tributo = c.cd_tributo
		and	c.ie_tipo_tributo = 'IVA'
		and	c.ie_isento = 'N'
		and 	b.tx_tributo = 8
		and	a.nr_sequencia   = nr_sequencia_nf_p;	
					   

	end if;

end if;

if vl_retorno_w = 0 then
	vl_retorno_w := null;
end if;

return vl_retorno_w;

end fis_obter_dados_iva;
/
