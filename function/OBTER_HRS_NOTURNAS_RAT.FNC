create or replace
function obter_hrs_noturnas_rat(nr_seq_rat_p		number
		) return number is
		
nr_hr_w		number(10,0);
dt_inicio_w	date;
dt_fim_w	date;
dt_ini_alf_w	date;
dt_fim_alf_w	date;
qt_hrs_w	number;
hr_ini_w	date;
hr_fim_w	date;
result_w 	number;

					
cursor c01 is
	SELECT 	   l.dt_inicio_ativ, 
		   l.dt_fim_ativ
	FROM       PROJ_RAT_ATIV l,
		   proj_rat g
	WHERE	   g.nr_sequencia = l.nr_seq_rat
	AND	   nr_seq_rat = nr_seq_rat_p	
	and	l.dt_fim_ativ is not null
	order by 1;

	
begin
result_w:= 0;
open c01;
loop
fetch c01 into	dt_inicio_w,
		dt_fim_w;		
exit when c01%notfound;
    begin
	
	
	
	if (dt_inicio_w <= TO_DATE(TO_CHAR(dt_inicio_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss')) and
		(dt_fim_w >= TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss')) then
	
		hr_ini_w := TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss');
		hr_fim_w     := dt_fim_w;
		
		result_w:= result_w + substr(obter_hora_entre_datas(hr_ini_w, hr_fim_w),1,5);
				

	end if;
	
	
	if (dt_inicio_w >= TO_DATE(TO_CHAR(dt_inicio_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss')) and
		(dt_fim_w >= TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss')) then
	
		hr_fim_w := TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss');
		hr_ini_w  := dt_inicio_w;
		
		result_w:= result_w + substr(obter_hora_entre_datas(hr_ini_w, hr_fim_w),1,5);
		

	end if;
	
	
	
	if (dt_inicio_w >= TO_DATE(TO_CHAR(dt_inicio_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss')) and
		(dt_fim_w <= TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss')) and
		trunc(dt_fim_w) = trunc(dt_inicio_w + 1) then
	
		hr_fim_w     := dt_fim_w;
		hr_ini_w  := dt_inicio_w;

		result_w:= result_w + substr(obter_hora_entre_datas(hr_ini_w, hr_fim_w),1,5);

	end if;
	
	
	if (dt_inicio_w <= TO_DATE(TO_CHAR(dt_inicio_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss')) and
		(dt_fim_w >= TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss')) and
		trunc(dt_fim_w) = trunc(dt_inicio_w + 1) then
	
		hr_fim_w     := TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss');
		hr_ini_w  := TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss');
		
		
		result_w:= result_w + substr(obter_hora_entre_datas(hr_ini_w, hr_fim_w),1,5);
		
	end if;
	
	
	if (dt_inicio_w <= TO_DATE(TO_CHAR(dt_inicio_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss')) and
		(dt_fim_w <= TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss')) then
	
		hr_fim_w     := dt_fim_w;
		hr_ini_w  := dt_inicio_w ;
		
		result_w:= result_w + substr(obter_hora_entre_datas(hr_ini_w, hr_fim_w),1,5);
		


	end if;
	
	
	if (dt_inicio_w <= TO_DATE(TO_CHAR(dt_inicio_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss')) and
		(dt_fim_w >= TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss')) and
		trunc(dt_inicio_w) = trunc(dt_fim_w) then
	
		hr_fim_w     := TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss');
		hr_ini_w     := dt_inicio_w ;

			
		result_w:= result_w + substr(obter_hora_entre_datas(hr_ini_w, hr_fim_w),1,5);
				    
		

	end if;
	

	
	
	if (dt_inicio_w <= TO_DATE(TO_CHAR(dt_inicio_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss')) and
		(dt_fim_w <= TO_DATE(TO_CHAR(dt_fim_w, 'dd/mm/yyyy') || ' 04:59:59','dd/mm/yyyy hh24:mi:ss')) and
		trunc(dt_inicio_w) < trunc(dt_fim_w) then
	
		hr_fim_w     := dt_fim_w;
		hr_ini_w     := TO_DATE(TO_CHAR(dt_inicio_w, 'dd/mm/yyyy') || ' 22:00:00','dd/mm/yyyy hh24:mi:ss');

			
		result_w:= result_w + substr(obter_hora_entre_datas(hr_ini_w, hr_fim_w),1,5);
				    
	end if;
	
	

   end;
	
end loop;
close c01;
	
	return result_w;
	
end obter_hrs_noturnas_rat;
/
