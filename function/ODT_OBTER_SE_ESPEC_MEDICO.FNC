create or replace
function Odt_Obter_se_espec_medico(	cd_especialidade_p	Number,
					cd_profissional_p	Varchar2)
					return varchar2 is

ds_retorno_w	Varchar2(1);
					
begin

if (cd_profissional_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	especialidade_prof_odont
	where	cd_pessoa_fisica = cd_profissional_p
	and	cd_especialidade = cd_especialidade_p;
	
end if;

return	ds_retorno_w;

end Odt_Obter_se_espec_medico;
/