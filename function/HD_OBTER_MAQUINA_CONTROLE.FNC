create or replace
function hd_obter_maquina_controle(nr_seq_dialise_p		number)
 		    	return number is

nr_seq_dialisador_w	number(10);			
nr_seq_maquina_w	number(10);
begin
if	(nr_seq_dialise_p is not null) then
	select	max(nr_sequencia)
	into	nr_seq_dialisador_w
	from	hd_dialise_dialisador
	where	nr_seq_dialise = nr_seq_dialise_p;

	select	max(nr_seq_maquina)
	into	nr_seq_maquina_w
	from	hd_dialise_dialisador
	where	nr_sequencia = nr_seq_dialisador_w;
end if;

return	nr_seq_maquina_w;

end hd_obter_maquina_controle;
/