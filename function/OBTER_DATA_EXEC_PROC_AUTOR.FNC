create or replace 
function obter_data_exec_proc_autor(nr_seq_procedimento_p	number)
				return date is
dt_retorno_w	date;

begin

select 	max(b.dt_procedimento) dt_procedimento
into	dt_retorno_w
from 	procedimento_autorizado a,
	procedimento_paciente b
where	a.nr_sequencia		= nr_seq_procedimento_p
and 	a.nr_sequencia		= b.nr_seq_proc_autor
and	((a.nr_prescricao	= b.nr_prescricao) or
	(nvl(a.nr_prescricao,0) = 0))
and 	b.cd_motivo_exc_conta 	is null;
	

if	(dt_retorno_w is null) then

	select	max(b.dt_procedimento) dt_procedimento
	into	dt_retorno_w
	from 	procedimento_autorizado a,
		procedimento_paciente b,
		autorizacao_convenio c
	where	a.nr_sequencia 		= nr_seq_procedimento_p
	and 	c.nr_sequencia		= a.nr_sequencia_autor
	and 	c.nr_atendimento	= b.nr_atendimento
	and 	c.cd_convenio		= b.cd_convenio
	and 	b.cd_procedimento	= a.cd_procedimento
	and 	b.ie_origem_proced	= a.ie_origem_proced
	and	((a.nr_prescricao	= b.nr_prescricao) or
		(nvl(a.nr_prescricao,0) = 0))
	and 	b.cd_motivo_exc_conta 	is null;
end if;	

return dt_retorno_w;

end obter_data_exec_proc_autor;
/