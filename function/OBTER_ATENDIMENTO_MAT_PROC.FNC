create or replace
function obter_atendimento_mat_proc(
          nr_seq_mat_proc_p number,
          ie_mat_proc_p     varchar2)
          return number as

nr_atendimento_w number(10, 0);

begin
if (ie_mat_proc_p = '1') then -- material
  select nr_atendimento
  into   nr_atendimento_w
  from   material_atend_paciente
  where  nr_sequencia = nr_seq_mat_proc_p;
elsif (ie_mat_proc_p = '2') then -- procedimento
  select nr_atendimento
  into   nr_atendimento_w
  from   procedimento_paciente
  where  nr_sequencia = nr_seq_mat_proc_p;
end if;

return nr_atendimento_w;
end obter_atendimento_mat_proc;
/