create or replace
function	sup_obter_adto_nf_devol(
		nr_seq_nf_p		number) return varchar2 is

ds_retorno_w		varchar2(255);
nr_adiantamento_w	number(10);

cursor	c01 is
select	nr_adiantamento
from	adiantamento_pago
where	nr_seq_nf = nr_seq_nf_p;

begin

open c01;
loop
fetch c01 into
	nr_adiantamento_w;
exit when c01%notfound;
	begin
	ds_retorno_w	:= substr((ds_retorno_w || ', ' || nr_adiantamento_w),1,255);
	end;
end loop;
close c01;

if	(ds_retorno_w is not null) then
	ds_retorno_w	:= substr(ds_retorno_w,3,255);
end if;

return ds_retorno_w;	

end sup_obter_adto_nf_devol;
/