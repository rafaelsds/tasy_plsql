create or replace
function obter_se_permite_lactante(	nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
									cd_material_p		material.cd_material%type) return varchar2 is
						
ie_sexo_pf_w		pessoa_fisica.ie_sexo%type;
ds_retorno_w		varchar2(1) := 'S';
begin

if (nr_atendimento_p is not null and cd_material_p is not null) then
	select 	obter_sexo_pf(obter_pessoa_Atendimento(nr_atendimento_p,'C'),'C')
	into	ie_sexo_pf_w
	from	dual;

	if (ie_sexo_pf_w = 'F') then
		select	nvl(max('N'), 'S')
		into	ds_retorno_w
		from	material
		where	cd_material = cd_material_p
		and		nvl(ie_permite_lactante, 'N') = 'S' --Rotina alterada. Quando este campo estiver checado ir� consistir lactante.
		and		nvl(obter_inf_saude_mulher(nr_atendimento_p, 'A'), 'N') = 'S';
	end if;
end if;

return ds_retorno_w;

end obter_se_permite_lactante;
/
