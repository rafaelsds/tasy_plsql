create or replace FUNCTION OBTER_MEDICO_VALIDATION_INFO(

					nr_sequencia_p 	number,

          ie_info_type_p varchar2)

					RETURN VARCHAR2 IS



ds_retorno_w		Varchar2(255);



-- ie_info_type_p   S - Validate by Physician

--                  D - Date/Time of validation done by physican

BEGIN

if	(nr_sequencia_p 	is not null) then

Begin



If ( ie_info_type_p = 'S') then

Begin

select max(obter_nome_medico(cd_medico,'N'))

  into ds_retorno_w

	from	 CPOE_ACTION_LOG_V

	where	 nr_sequencia 	= nr_sequencia_p

  and  cd_medico is not null

  and  dt_liberacao is not null

  and IE_ORDERED_ON_BEHALF ='S';

  end;

end if ;

If ( ie_info_type_p = 'D') then

begin

  select max(DT_CIENCIA_MEDICO) into ds_retorno_w

	from	 CPOE_ACTION_LOG_V

	where	 nr_sequencia 	= nr_sequencia_p

  and  cd_medico is not null
  
  and IE_ORDERED_ON_BEHALF ='S'

  and  dt_liberacao is not null

  and DT_CIENCIA_MEDICO is not null;

  end;

end if;

end;

end if;



RETURN ds_retorno_w;

END OBTER_MEDICO_VALIDATION_INFO;
/