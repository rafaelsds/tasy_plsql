CREATE OR REPLACE
FUNCTION Campo_Mascara_casas	(vl_campo_p		number,
			qt_decimais_p		number)
 		    	return varchar2 IS

ds_retorno_w		varchar2(80) := '';
ds_mascara_w		varchar2(100) := '';
vl_campo_w			varchar2(100);
qt_campos_antes_virg_w	number(10)	:= 0;
i				number(10)  := 0;

begin


if	(vl_campo_p is not null) then
	begin
	vl_campo_w			:= to_char(vl_campo_p);

	select	replace(vl_campo_w, ',', '.')
	into	vl_campo_w
	from	dual;

	i := 0;

	for	i in 1..length(vl_campo_w) loop
		if	(substr(vl_campo_w,i,1) <> '.') then
			qt_campos_antes_virg_w	:= qt_campos_antes_virg_w + 1; 
		else
			begin
			if	(qt_campos_antes_virg_w = 0) then
				qt_campos_antes_virg_w := 1;
			end if;
			exit;
			end;
		end if;
	end loop;

	i := 0;

	for	i in 1..qt_campos_antes_virg_w loop
		ds_mascara_w	:= ds_mascara_w || '0';	
	end loop;

	if 	(qt_decimais_p is not null) and
		(qt_decimais_p > 0) then
		ds_mascara_w	:= ds_mascara_w || '.';	
	end if;

	i := 0;

	for	i in 1..qt_decimais_p loop
		ds_mascara_w	:= ds_mascara_w || '9';
	end loop;

	if	(vl_campo_p is not null) then
    		begin
		select	to_char(vl_campo_p, ds_mascara_w)
		into		ds_retorno_w
		from		dual;
		
		ds_retorno_w := replace(ds_retorno_w,'.',',');
		end;
	end if;
	end;
	end if;

select	replace(ds_retorno_w, ' ', '')
into		ds_retorno_w
from		dual;

RETURN ds_retorno_w;
END Campo_Mascara_casas;
/


 
