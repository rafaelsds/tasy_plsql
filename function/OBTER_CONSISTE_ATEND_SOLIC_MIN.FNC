create or replace
function obter_consiste_atend_solic_min(
			cd_operacao_estoque_p	number,
			cd_local_estoque_p	number,
			cd_material_p		number) 
			return varchar2 is 
			
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
cd_material_w		number(6);
cd_operacao_estoque_w	number(3);
ie_consiste_w		varchar2(1) := 'N';

cursor c01 is
select	ie_consiste
from	regra_atend_req_minimo
where	nvl(cd_operacao_estoque, cd_operacao_estoque_w) = cd_operacao_estoque_w
and	nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w) = cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w) = cd_classe_material_w
and	nvl(cd_local_estoque, cd_local_estoque_p) = cd_local_estoque_p
and	nvl(cd_material, cd_material_p) = cd_material_p
order by	nvl(cd_material,0),
	nvl(cd_classe_material,0),
	nvl(cd_subgrupo_material,0),
	nvl(cd_grupo_material,0),
	nvl(cd_operacao_estoque,0);
	  
begin
cd_operacao_estoque_w := nvl(cd_operacao_estoque_p, 0);

select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v
where	cd_material = cd_material_p;

open c01;
loop
fetch c01 into	
	ie_consiste_w;
exit when c01%notfound;
end loop;
close c01;

return ie_consiste_w;
end obter_consiste_atend_solic_min;
/
