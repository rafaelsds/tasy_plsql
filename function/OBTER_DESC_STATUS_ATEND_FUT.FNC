create or replace
function obter_desc_status_atend_fut(nr_seq_status_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
			
begin

if	(nr_seq_status_p is not null) then
	select	ds_status
	into	ds_retorno_w
	from	status_atend_futuro
	where	nr_sequencia = nr_seq_status_p
	and	ie_situacao = 'A';
end if;

return	ds_retorno_w;

end obter_desc_status_atend_fut;
/