create or replace
function Obter_local_Processo_Aprov(	nr_seq_processo_p	Number)
		return Number is


cd_local_estoque_w		Number(10);

begin

select	nvl(max(a.cd_local_estoque), 0)
into	cd_local_estoque_w
from	ordem_compra b,
	ordem_compra_item a
where	a.nr_ordem_compra		= b.nr_ordem_compra
and	a.nr_seq_aprovacao	= nr_seq_processo_p;

if	(cd_local_estoque_w = 0) then
	select	nvl(max(b.cd_local_estoque),0)
	into	cd_local_estoque_w
	from	solic_compra b,
		solic_compra_item a
	where	a.nr_solic_compra		= b.nr_solic_compra
	and	a.nr_seq_aprovacao	= nr_seq_processo_p;
end if;

if	(cd_local_estoque_w = 0) then
	select	nvl(max(b.cd_local_estoque),0)
	into	cd_local_estoque_w
	from	requisicao_material b,
		item_requisicao_material a
	where	a.nr_requisicao		= b.nr_requisicao
	and	a.nr_seq_aprovacao	= nr_seq_processo_p;
end if;

return cd_local_estoque_w;

end Obter_local_Processo_Aprov;
/
