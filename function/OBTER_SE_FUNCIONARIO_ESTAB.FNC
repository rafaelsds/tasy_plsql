create or replace
function OBTER_SE_FUNCIONARIO_ESTAB
		(cd_pessoa_fisica_p	varchar2,
		 cd_estabelecimento_p	number) return varchar2 is

cont_w			number(10,0);
ie_funcionario_w	varchar2(255);

begin

select	count(*)
into	cont_w
from	pessoa_fisica_estab
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(cont_w > 0) then
	select	nvl(max(ie_funcionario), 'N')
	into	ie_funcionario_w
	from	pessoa_fisica_estab
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	cd_estabelecimento	= cd_estabelecimento_p;
else
	select	ie_funcionario
	into	ie_funcionario_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
end if;

return	ie_funcionario_w;

end OBTER_SE_FUNCIONARIO_ESTAB;
/
