create or replace
function obter_nr_seq_regra_contrato(		cd_cnpj_p		varchar2,
					cd_pessoa_fisica_p		varchar2,
					cd_material_p		number)
return number is

nr_sequencia_w			number(10);

begin

select	max(a.nr_sequencia)
into	nr_sequencia_w
from	contrato_regra_nf a,
	contrato b
where	a.nr_seq_contrato = b.nr_sequencia
and	a.cd_material = cd_material_p
and	(((cd_cnpj_p is not null) and (b.cd_cgc_contratado = cd_cnpj_p)) or
	((cd_pessoa_fisica_p is not null) and (b.cd_pessoa_contratada = cd_pessoa_fisica_p)))
and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')));

return	nr_sequencia_w;

end	obter_nr_seq_regra_contrato;
/
