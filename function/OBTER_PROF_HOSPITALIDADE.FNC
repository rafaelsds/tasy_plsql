create or replace 
function Obter_Prof_Hospitalidade (	nr_atendimento_p	number)
					return			varchar2 is
nr_sequencia_w		number(10);
cd_pessoa_fisica_w	varchar2(10);

begin

begin
select	max(nr_sequencia)
into	nr_sequencia_w
from	atend_profissional
where	nr_atendimento		= nr_atendimento_p
and	((ie_profissional		= 'H') or (IS_COPY_BASE_LOCALE('es_MX') = 'S' and ie_profissional = '12'));
exception
	when others then
		nr_sequencia_w	:= 0;
end;

if	(nr_sequencia_w > 0) then
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	atend_profissional
	where	nr_sequencia		= nr_sequencia_w;
end if;	

return	cd_pessoa_fisica_w;

end Obter_Prof_Hospitalidade;
/
