create or replace
function hupa_obter_conversao_medic(	cd_material_p		number,
					qt_dose_p		number,
					ie_via_aplicacao_p	varchar2,
					ie_tipo_p		number)
					return varchar2 is

ds_retorno_w	varchar2(15);

/*
Par�metro: IE_OPCAO_P
1 - Volume aspirado
2 - Volume soro dose
3 - Tipo
*/
begin

if	(nvl(cd_material_p,0) = 2188) then --Aciclovir 250mg
	
	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := ((nvl(qt_dose_p,1) * 10) / 250);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 5);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF';
	end if;

elsif	(nvl(cd_material_p,0) = 51206) then --Amicacina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.004);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 10);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51207) then --Aminofilina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 24);
	end if;

elsif	(nvl(cd_material_p,0) = 51211) then --Amoxicilina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.02);
	end if;

elsif	(nvl(cd_material_p,0) = 51783) then --Ampicilina + Sulbactam

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.00266);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 30);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF';
	end if;

elsif	(nvl(cd_material_p,0) = 51214) then --Ampicilina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := ((nvl(qt_dose_p,1) * 5) / 500);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 10);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF';
	end if;

elsif	(nvl(cd_material_p,0) = 51215) then --Anfotericina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.2);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 10);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SG';
	end if;

elsif	(nvl(cd_material_p,0) = 52032) then --Captopril

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 2.5);
	end if;

elsif	(nvl(cd_material_p,0) = 671) then --Cefalotina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.005);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 50);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 657) then --Cefazolina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := ((nvl(qt_dose_p,1) * 3) / 1000);
	end if;

	if	(ie_via_aplicacao_p = 'EV') then
		if	(nvl(ie_tipo_p,0) = 1) then
			ds_retorno_w := (nvl(qt_dose_p,1) * 0.01);
		elsif	(nvl(ie_tipo_p,0) = 2) then
			ds_retorno_w := (nvl(qt_dose_p,1) / 20);
		elsif	(nvl(ie_tipo_p,0) = 3) then
			ds_retorno_w := 'SF';
		end if;
	end if;

elsif	(nvl(cd_material_p,0) = 673) then --Cefepime

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.005);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 20);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51293) then --Cefotaxima

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.005);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 30);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 637) then --Ceftazidima

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.01);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 20);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51294) then --Ceftriaxona

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.003);
	end if;

elsif	(nvl(cd_material_p,0) = 51299) then --Cetoprofeno

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.05);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 1);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF';
	end if;

elsif	(nvl(cd_material_p,0) = 51390) then --Oxacilina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.01);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 20);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51295) then --Cefuroxima

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := ((nvl(qt_dose_p,1) * 10) / 750);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 20);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51301) then --Ciprofloxacino

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.5);
	end if;

elsif	(nvl(cd_material_p,0) = 51302) then --Clindamicina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := ((nvl(qt_dose_p,1) * 4) / 600);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 10);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51304) then --Clonidina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 150);
	end if;

elsif	(nvl(cd_material_p,0) = 51309) then --Cloranfenicol

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.005);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 25);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG,AD';
	end if;

elsif	(nvl(cd_material_p,0) = 51325) and
	(nvl(ie_via_aplicacao_p,'0') = 'EV')then --Dimenidrinato

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 3);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(hupa_obter_conversao_medic(cd_material_p,qt_dose_p,ie_via_aplicacao_p,1),1) * 2);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51319) then --Dexametasona

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 4);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 2);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF';
	end if;

elsif	(nvl(cd_material_p,0) = 51320) then --DEXCLORFENIRAMINA

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 0.4);
	end if;

elsif	(nvl(cd_material_p,0) = 51325) and
	(nvl(ie_via_aplicacao_p,'0') = 'VO') then --DIMENIDRATO DL

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 3);
	end if;

elsif	(nvl(cd_material_p,0) = 51333) then --Eritromicina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.02);
	end if;

elsif	(nvl(cd_material_p,0) = 51965) then --ESPIRONOLACTONA

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 2.5);
	end if;

elsif	(nvl(cd_material_p,0) = 51342) then --Fluconazol

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) * 0.5);
	end if;

elsif	(nvl(cd_material_p,0) = 51344) and
	(nvl(ie_via_aplicacao_p,'0') = 'EV') then --Furosemida

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 10);
	end if;

elsif	(nvl(cd_material_p,0) = 51346) then --Gentamicina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 40);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 10);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51354) then --Hidralazina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 20);
	end if;

elsif	(nvl(cd_material_p,0) = 36432) then --HIDROCLOROTIAZIDA

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 5);
	end if;

elsif	(nvl(cd_material_p,0) = 51357) then --Hidrocortisona

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 100);
	end if;

elsif	(nvl(cd_material_p,0) = 51368) then --Meropenem

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 50);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 25);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 524) then --Metoclopramida

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 5);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 1);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF';
	end if;

elsif	(nvl(cd_material_p,0) = 51369) and
	(nvl(ie_via_aplicacao_p,'0') = 'EV') then --Metilprednisolona

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := ((nvl(qt_dose_p,1) * 8) / 500);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 4);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51372) then --Metronidazol

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 5);
	end if;

elsif	(nvl(cd_material_p,0) = 51373) then --MIDAZOLAN SOL.

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 2);
	end if;

elsif	(nvl(cd_material_p,0) = 9914) then --N-ACETILCISTE�NA XPE

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 20);
	end if;

elsif	(nvl(cd_material_p,0) = 51383) then --NISTATINA SUSP.

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 100000);
	end if;

elsif	(nvl(cd_material_p,0) = 14191) then --Omeprazol

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 4);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 20);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51388) then --OMEPRAZOL 10MG DIL.EM 5ML

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 2);
	end if;

elsif	(nvl(cd_material_p,0) = 51397) then --PENICILINA BENZ.

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := ((nvl(qt_dose_p,1) * 2) / 1200000);
	end if;

elsif	(nvl(cd_material_p,0) = 51395) then --Penicilina Cristalina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 500000);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 10000);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51997) then --PENICILINA PROCA.

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 200000);
	end if;

elsif	(nvl(cd_material_p,0) = 51369) and
	(nvl(ie_via_aplicacao_p,'0') = 'VO') then --PREDNISOLONA SOL.

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 3);
	end if;

elsif	(nvl(cd_material_p,0) = 51409) then --Ranitidina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 25);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 2.5);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51408) then --RANITIDINA SUSP - Mesmo c�digo?

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 15);
	end if;

elsif	(nvl(cd_material_p,0) = 51411) then --Salbutamol

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 0.5);
	end if;

elsif	(nvl(cd_material_p,0) = 51427) then --Sulfametoxazol + Trimetoprima

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 56);
	end if;

elsif	(nvl(cd_material_p,0) = 51428) then --SULFAMTOX. + TRIMETOPRIMA 480mg/5mL - Mesmo c�digo?

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 96);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 3);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51438) then --VALPROATO DE S�DIO

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 50);
	end if;

elsif	(nvl(cd_material_p,0) = 51437) then --Vancomicina

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 100);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 3);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

elsif	(nvl(cd_material_p,0) = 51294) then --Ceftriaxona

	if	(nvl(ie_tipo_p,0) = 1) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 200);
	elsif	(nvl(ie_tipo_p,0) = 2) then
		ds_retorno_w := (nvl(qt_dose_p,1) / 20);
	elsif	(nvl(ie_tipo_p,0) = 3) then
		ds_retorno_w := 'SF,SG';
	end if;

end if;

if	(instr(' ' || ds_retorno_w,' ,') > 0) then
	ds_retorno_w := replace(' ' || ds_retorno_w,' ,','0,');
end if;

return substr(ds_retorno_w,1,255);

end hupa_obter_conversao_medic;
/