create or replace
function pls_obt_seguro_beneficio_obit
		(	nr_seq_segurado_p	number,
			ie_opcao_p		varchar2)
 		    	return varchar2 is
			
/*
ie_opcao_p
S - Seguro de �bito
B - Benef�cio por �bito
*/		
			
ie_retorno_w		varchar2(10) := 'N';		
qt_retorno_w		number(10);

begin

select	count(*)
into	qt_retorno_w
from	pls_segurado	c,
	pls_plano	b,
	pls_sca_vinculo	a
where	a.nr_seq_segurado	= c.nr_sequencia
and	a.nr_seq_plano		= b.nr_sequencia
and	c.nr_sequencia		= nr_seq_segurado_p
and	b.ie_seguro_obito	= ie_opcao_p;

if	(qt_retorno_w = 0) and
	(ie_opcao_p = 'B') then
	select	count(*)
	into	qt_retorno_w
	from	pls_segurado		c,
		pls_bonificacao		b,
		pls_bonificacao_vinculo	a
	where	a.nr_seq_segurado	= c.nr_sequencia
	and	a.nr_seq_bonificacao	= b.nr_sequencia
	and	c.nr_sequencia		= nr_seq_segurado_p
	and	b.IE_SEGURO_OBITO	= 'S';
end if;

if	(qt_retorno_w	> 0) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end pls_obt_seguro_beneficio_obit;
/
