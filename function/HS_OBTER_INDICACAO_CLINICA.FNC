create or replace
function hs_obter_indicacao_clinica (nr_atendimento_p	number,
				  cd_procedimento_p	number,
				  ie_origem_proced_p	number)
					return varchar2 is

ds_retorno_w	varchar2(2000):= ' ';

begin

select 	max(nvl(ds_indicacao,' '))
into	ds_retorno_w
from 	autorizacao_convenio
where 	nr_atendimento 			= nr_atendimento_p
and 	((cd_procedimento_principal 	= nvl(cd_procedimento_p,0)) or (nvl(cd_procedimento_p,0) = 0))
and 	((ie_origem_proced		= nvl(ie_origem_proced_p,0))or (nvl(ie_origem_proced_p,0) = 0));

return ds_retorno_w;

end hs_obter_indicacao_clinica;
/
