create or replace
function obter_desc_med_justificar(	nr_sequencia_p	number )
					return varchar2 is

ds_mat_justificativa_w	varchar2(1000);

begin

begin
select	substr(obter_desc_material(cd_material),1,40) || '  ' || Wheb_mensagem_pck.get_texto(309034) || ': ' /*'  Consumo: '*/ || 
	sum(nvl(qt_dose_consumo,0)) || ' ' || cd_unidade_consumo || '  ' || Wheb_mensagem_pck.get_texto(309035) || ': ' /*'  Dispensado: '*/ ||
	sum(nvl(qt_dispensacao,0)) || ' ' || cd_unidade_dispensacao
into	ds_mat_justificativa_w
from	w_resumo_agente
where	nr_sequencia = nr_sequencia_p
group by substr(obter_desc_material(cd_material),1,40),
	cd_unidade_consumo,
	cd_unidade_dispensacao;
exception
	when others then
	ds_mat_justificativa_w	:= Wheb_mensagem_pck.get_texto(309100); --'Justificativa';
end;

return ds_mat_justificativa_w;

end obter_desc_med_justificar;
/