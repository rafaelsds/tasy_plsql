create or replace
function obter_declaracao_estab_agenda		 ( cd_declaracao_p	varchar2,
						   cd_estab_agenda_p		number )
						   return varchar2 is

cd_declaracao_w varchar(10);
					
begin



if  (cd_declaracao_p is not null) and
    (nvl(cd_estab_agenda_p,0) > 0) then
	select    cd_declaracao
	into 	  cd_declaracao_w
	from      declaracao
	where    (obter_se_contido_char(cd_declaracao,cd_declaracao_p) = 'S')
	and      (OBTER_SE_LIB_DECLARACAO_ESTAB(cd_declaracao,cd_estab_agenda_p) = 'S')
	and 	 rownum = 1;
end if;

return	cd_declaracao_w;

end obter_declaracao_estab_agenda;
/