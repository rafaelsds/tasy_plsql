create or replace
function sus_obter_motivo_alter_stat(	nr_seq_motivo_p	number)
					return varchar2 is

ds_motivo_alteracao_w	sus_motiv_alter_stat_laudo.ds_motivo_alteracao%type;
					
begin

if	(nr_seq_motivo_p is not null) then

	begin
	select	ds_motivo_alteracao
	into	ds_motivo_alteracao_w
	from	sus_motiv_alter_stat_laudo
	where	nr_sequencia = nr_seq_motivo_p;
	exception
	when others then
		ds_motivo_alteracao_w := null;
	end;	

end if;

return ds_motivo_alteracao_w;

end sus_obter_motivo_alter_stat;
/