create or replace 
procedure calc_vol_total_sol_protocol(	
				nr_prescricao_p			number,
				nr_seq_hd_prescricao_p	number,
				nr_seq_solucao_p		number) is

				
qt_multiplicacao_w	hd_prescricao.qt_hora_sessao%type;
ie_unid_vel_inf_w	prescr_solucao.ie_unid_vel_inf%type;
qt_dosagem_w		prescr_solucao.qt_dosagem%type;
qt_solucao_total_w	prescr_solucao.qt_solucao_total%type;
ie_tipo_solucao_w	prescr_solucao.ie_tipo_solucao%type;
nr_seq_solucao_w	prescr_solucao.nr_seq_solucao%type;

cursor c01 is
select	ie_unid_vel_inf,
		qt_dosagem,
		ie_tipo_solucao,
		nr_seq_solucao
from	prescr_solucao
where	nr_prescricao = nr_prescricao_p
and		((nr_seq_solucao = nr_seq_solucao_p) or 
		 ((nvl(nr_seq_solucao_p,0) = 0) and 
		  (nr_seq_dialise = nr_seq_hd_prescricao_p)));	
begin

open c01;
loop
fetch c01 into	ie_unid_vel_inf_w,
				qt_dosagem_w,
				ie_tipo_solucao_w,
				nr_seq_solucao_w;
exit when c01%notfound;
	begin
	
	select	decode(ie_unid_vel_inf_w, 'mlm', qt_min_sessao, 'mlh', qt_hora_sessao, 0) qt_multiplicacao
	into	qt_multiplicacao_w
	from	hd_prescricao
	where	nr_sequencia = nr_seq_hd_prescricao_p
	and		nr_prescricao = nr_prescricao_p;
	
	if (ie_tipo_solucao_w = 'C') then
		qt_solucao_total_w := 0;
	elsif (ie_tipo_solucao_w <> 'C') then
		qt_solucao_total_w :=	nvl(qt_dosagem_w,0) * nvl(qt_multiplicacao_w,0);
	end if;	

	if (qt_solucao_total_w is not null) then
		ajustar_vol_sol_dialise(nr_prescricao_p, nr_seq_solucao_w, qt_solucao_total_w);
	end if;
	end;
end loop;
close c01;

end calc_vol_total_sol_protocol;
/
