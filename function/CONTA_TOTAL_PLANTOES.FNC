create or replace
function conta_total_plantoes(dt_mes_p varchar2,
			       dt_ano_p varchar2,
				nr_seq_classif_p number,
				nr_seq_grupo_p number,
				nr_seq_escala_p number,
				cd_pessoa_fisica_p number,
				ds_periodo_p varchar2,
				nr_sequencia_p number,
				nm_usuario_p varchar2,
				ds_hora_p varchar2,
				mes_p varchar2,
				ano_p varchar2,
				nm_professional_p varchar2) 
			        return number is
total_w number;
dia1_w varchar2(5);
dia2_w varchar2(5);
dia3_w varchar2(5);
dia4_w varchar2(5);
dia5_w varchar2(5);
dia6_w varchar2(5);
dia7_w varchar2(5);
dia8_w varchar2(5);
dia9_w varchar2(5);
dia10_w varchar2(5);
dia11_w varchar2(5);
dia12_w varchar2(5);
dia13_w varchar2(5);
dia14_w varchar2(5);
dia15_w varchar2(5);
dia16_w varchar2(5);
dia17_w varchar2(5);
dia18_w varchar2(5);
dia19_w varchar2(5);
dia20_w varchar2(5);
dia21_w varchar2(5);
dia22_w varchar2(5);
dia23_w varchar2(5);
dia24_w varchar2(5);
dia25_w varchar2(5);
dia26_w varchar2(5);
dia27_w varchar2(5);
dia28_w varchar2(5);
dia29_w varchar2(5);
dia30_w varchar2(5);
dia31_w varchar2(5);

cursor c01 is
SELECT	DISTINCT
	0,
	dia1,
	dia2,
	dia3,
	dia4,
	dia5,
	dia6,
	dia7,
	dia8,
	dia9,
	dia10,
	dia11,
	dia12,
	dia13,
	dia14,
	dia15,
	dia16,
	dia17,
	dia18,
	dia19,
	dia20,
	dia21,
	dia22,
	dia23,
	dia24,
	dia25,
	dia26,
	dia27,
	dia28,
	dia29,
	dia30,
	dia31
	FROM    escala_diaria_v
	WHERE	dt_inicio = dt_mes_p
	AND	dt_fim   = dt_ano_p
	AND	((nr_seq_classif = nr_seq_classif_p) OR (NVL(nr_seq_classif_p,0) = 0))
	AND	((nr_seq_grupo = nr_seq_grupo_p) OR (NVL(nr_seq_grupo_p,0) = 0))
	AND	((nr_seq_escala = nr_seq_escala_p) OR (NVL(nr_seq_escala_p,0) = 0))
	and	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	ds_periodo = ds_periodo_p
	and	nr_sequencia = nr_sequencia_p
	and	nm_usuario = nm_usuario_p
	and	ds_hora = ds_hora_p
	and	mes = mes_p
	and	ano = ano_p
	and	nm_profissional = nm_professional_p
	ORDER BY   1;


begin

open c01;
loop
fetch c01 into	
	total_w,
	dia1_w, 
	dia2_w,
	dia3_w,
        dia4_w,
	dia5_w, 
	dia6_w,
	dia7_w,
	dia8_w,
	dia9_w,
	dia10_w,
	dia11_w,
	dia12_w,
	dia13_w,
	dia14_w,
	dia15_w,
	dia16_w,
	dia17_w,
	dia18_w,
	dia19_w,
	dia20_w,
	dia21_w,
	dia22_w,
	dia23_w,
	dia24_w,
	dia25_w,
	dia26_w,
	dia27_w,
	dia28_w,
	dia29_w,
	dia30_w,
	dia31_w;
exit when c01%notfound;
	begin
		
		if dia1_w is not null then
		total_w := total_w +1;
		end if;
		if dia2_w is not null then
		total_w := total_w +1;
		end if;
		if dia3_w is not null then
		total_w := total_w +1;
		end if;
		if dia4_w is not null then
		total_w := total_w +1;
		end if;
		if dia5_w is not null then
		total_w := total_w +1;
		end if;
		if dia6_w is not null then
		total_w := total_w +1;
		end if;
		if dia7_w is not null then
		total_w := total_w +1;
		end if;
		if dia8_w is not null then
		total_w := total_w +1;
		end if;
		if dia9_w is not null then
		total_w := total_w +1;
		end if;
		if dia10_w is not null then
		total_w := total_w +1;
		end if;
		if dia11_w is not null then
		total_w := total_w +1;
		end if;
		if dia12_w is not null then
		total_w := total_w +1;
		end if;
		if dia13_w is not null then
		total_w := total_w +1;
		end if;
		if dia14_w is not null then
		total_w := total_w +1;
		end if;
		if dia15_w is not null then
		total_w := total_w +1;
		end if;
		if dia16_w is not null then
		total_w := total_w +1;
		end if;
		if dia17_w is not null then
		total_w := total_w +1;
		end if;
		if dia18_w is not null then
		total_w := total_w +1;
		end if;
		if dia19_w is not null then
		total_w := total_w +1;
		end if;
		if dia20_w is not null then
		total_w := total_w +1;
		end if;
		if dia21_w is not null then
		total_w := total_w +1;
		end if;
		if dia22_w is not null then
		total_w := total_w +1;
		end if;
		if dia23_w is not null then
		total_w := total_w +1;
		end if;
		if dia24_w is not null then
		total_w := total_w +1;
		end if;
		if dia25_w is not null then
		total_w := total_w +1;
		end if;
		if dia26_w is not null then
		total_w := total_w +1;
		end if;
		if dia27_w is not null then
		total_w := total_w +1;
		end if;
		if dia28_w is not null then
		total_w := total_w +1;
		end if;
		if dia29_w is not null then
		total_w := total_w +1;
		end if;
		if dia30_w is not null then
		total_w := total_w +1;
		end if;
		if dia31_w is not null then
		total_w := total_w +1;
		end if;
	  end;
end loop;
close c01;

return total_w;

end conta_total_plantoes;
/ 