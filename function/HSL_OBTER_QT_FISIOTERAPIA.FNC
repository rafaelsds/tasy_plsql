CREATE OR REPLACE
function HSL_Obter_Qt_Fisioterapia(dia_p	Varchar2,
								   mes_p	Varchar2,
								   ano_p	Varchar2,
								   ie_opcao_p Varchar2,
								   ie_periodo_p	Varchar2)
				return Number is
				
/* ie_opcao_p  = ('M')  Fisioterapias Motoras
     ie_opcao_p  =  ('R')  Fisioterapia Respiratoria
    ie_opcao_p  =  ('P') Quantidades de pacientes
    ie_opcao_p  = ('A') Quantidade de Prescri��es 
    ie_opcao_p  = ('ST') Soma dos dois tipos de Fisioterapias
    ie_opcao_p  = ('QD')  Quantidade de dias do m�s*/
	
/*  ie_periodo =  ('D')  Peroiodo por dia
      ie_periodo=   ('M') Periodo M�s	*/	
	
qt_retorno_w		Number(10);
dt_inicial_w		Date;
dt_final_w			Date;
qt_dias_w			Number(10);
qt_total_w			Number(10):= 0;
cont_w				Number(10);
cont_ww				Number(10);

	
BEGIN
IF	(ie_periodo_p = 'D') THEN 
	cont_w			:= 1;
	cont_ww			:= 1;
	dt_inicial_w	:= converte_char_data(dia_p||'/'||mes_p||'/'||ano_p,'00:00:00',null);
	dt_final_w		:= converte_char_data(dia_p||'/'||mes_p||'/'||ano_p,'23:59:00',null);
ELSIF	(ie_periodo_p = 'M') THEN 
	qt_dias_w		:= obter_qt_dias_mes(converte_char_data('01'||'/'||mes_p||'/'||ano_p,'00:00:00',null));
	cont_w			:= 1;
	cont_ww			:= qt_dias_w;
	dt_inicial_w	:= converte_char_data('01'||'/'||mes_p||'/'||ano_p,'00:00:00',null);
	dt_final_w	    := converte_char_data('01'||'/'||mes_p||'/'||ano_p,'23:59:00',null);
END IF;	


FOR i IN cont_w..cont_ww LOOP	
	IF	(ie_opcao_p = 'M') THEN
		SELECT COUNT(A.CD_PROCEDIMENTO)	
		INTO	qt_retorno_w
		FROM 	PROCEDIMENTO B,
			PRESCR_PROCEDIMENTO A
		WHERE  	A.CD_PROCEDIMENTO    	= B.CD_PROCEDIMENTO
		AND  	A.IE_ORIGEM_PROCED   	= B.IE_ORIGEM_PROCED
		AND	A.IE_SUSPENSO		<> 'S'
		AND	A.CD_PROCEDIMENTO 	= B.CD_PROCEDIMENTO
		AND 	A.DT_PREV_EXECUCAO BETWEEN dt_inicial_w and dt_final_w 
		AND 	A.CD_PROCEDIMENTO IN(25030019,25070070,25020137,25020030,25020080,25020129,25020102,25020110,25060066);
	ELSIF	(ie_opcao_p	 = 'R') THEN
		SELECT COUNT(A.CD_PROCEDIMENTO)
		INTO 	qt_retorno_w
		FROM 	PROCEDIMENTO B,
			PRESCR_PROCEDIMENTO A
		WHERE  	A.CD_PROCEDIMENTO    	= B.CD_PROCEDIMENTO
		AND  	A.IE_ORIGEM_PROCED   	= B.IE_ORIGEM_PROCED
		AND	A.IE_SUSPENSO		<> 'S'
		AND	A.CD_PROCEDIMENTO 	= B.CD_PROCEDIMENTO
		AND 	A.DT_PREV_EXECUCAO BETWEEN dt_inicial_w and dt_final_w  
		AND 	A.CD_PROCEDIMENTO IN(25040014,25040022,25070037);
	ELSIF	(ie_opcao_p	 = 'P') THEN
		SELECT COUNT(DISTINCT(C.NR_ATENDIMENTO))
		INTO 	qt_retorno_w
		FROM 	PROCEDIMENTO B,
			PRESCR_PROCEDIMENTO A,
			PRESCR_MEDICA C
		WHERE  	A.CD_PROCEDIMENTO    	= B.CD_PROCEDIMENTO
		AND  	A.IE_ORIGEM_PROCED   	= B.IE_ORIGEM_PROCED
		AND	A.NR_PRESCRICAO		= C.NR_PRESCRICAO
		AND	A.IE_SUSPENSO		<> 'S'
		AND	A.CD_PROCEDIMENTO 	= B.CD_PROCEDIMENTO
		AND 	A.DT_PREV_EXECUCAO BETWEEN dt_inicial_w and dt_final_w  
		AND 	A.CD_PROCEDIMENTO IN(25040014,25040022,25070037,25030019,25070070,25020137,25020030,25020080,25020129,25020102,25020110,25060066);
	ELSIF	(ie_opcao_p	 = 'A') THEN
		SELECT COUNT(A.NR_PRESCRICAO)
		INTO 	qt_retorno_w
		FROM 	PROCEDIMENTO B,
			PRESCR_PROCEDIMENTO A,
			PRESCR_MEDICA C
		WHERE  	A.CD_PROCEDIMENTO    	= B.CD_PROCEDIMENTO
		AND  	A.IE_ORIGEM_PROCED   	= B.IE_ORIGEM_PROCED
		AND	A.NR_PRESCRICAO		= C.NR_PRESCRICAO
		AND	A.IE_SUSPENSO		<> 'S'
		AND	A.CD_PROCEDIMENTO 	= B.CD_PROCEDIMENTO
		AND 	A.DT_PREV_EXECUCAO BETWEEN dt_inicial_w and dt_final_w 
		AND 	A.CD_PROCEDIMENTO IN(25040014,25040022,25070037,25030019,25070070,25020137,25020030,25020080,25020129,25020102,25020110,25060066);
	ELSIF	(ie_opcao_p	 = 'ST') THEN
		SELECT COUNT(B.CD_PROCEDIMENTO)
		INTO 	qt_retorno_w
		FROM 	PROCEDIMENTO B,
			PRESCR_PROCEDIMENTO A
		WHERE  	A.CD_PROCEDIMENTO    	= B.CD_PROCEDIMENTO
		AND  	A.IE_ORIGEM_PROCED   	= B.IE_ORIGEM_PROCED
		AND	A.IE_SUSPENSO		<> 'S'
		AND	A.CD_PROCEDIMENTO 	= B.CD_PROCEDIMENTO
		AND 	A.DT_PREV_EXECUCAO BETWEEN dt_inicial_w and dt_final_w 
		AND 	A.CD_PROCEDIMENTO IN(25040014,25040022,25070037,25030019,25070070,25020137,25020030,25020080,25020129,25020102,25020110,25060066);	
	ELSIF	(ie_opcao_p	 = 'QD') THEN	
		qt_retorno_w	:= obter_qt_dias_mes(converte_char_data(dia_p||'/'||mes_p||'/'||ano_p,'00:00:00',null));	
	END IF;
		
	qt_total_w	 := qt_total_w + qt_retorno_w;
	dt_inicial_w 	 := dt_inicial_w +1;
		
	IF	(cont_ww < cont_ww +1) THEN
		dt_final_w	:= dt_final_w +1;
	END IF;
END LOOP;
	
IF	(ie_periodo_p = 'M') THEN
	qt_retorno_w := qt_total_w;
END IF;	
	
		
RETURN qt_retorno_w;

END HSL_Obter_Qt_Fisioterapia;
/