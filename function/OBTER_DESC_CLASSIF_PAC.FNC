CREATE OR REPLACE
FUNCTION Obter_desc_classif_pac(nr_seq_tipo_pac_p		NUMBER)
 		    	RETURN VARCHAR2 IS
				
ds_classificacao_w	VARCHAR2(2000);
BEGIN

IF	(nr_seq_tipo_pac_p IS NOT NULL) THEN
	SELECT  ds_classif_paciente
	INTO	ds_classificacao_w
	FROM 	tipo_classificao_paciente
	WHERE 	ie_situacao = 'A'
	AND	nr_sequencia = nr_seq_tipo_pac_p;
END IF;

RETURN	ds_classificacao_w;

END Obter_desc_classif_pac;
/