create or replace
function obter_se_altera_san_doacao(	ie_status_p	number,
										dt_inicio_coleta_p	date)
 		    	return varchar2 is
				
ie_retorno_w	varchar2(10);

ie_parametro299_w varchar2(10);
ie_parametro151_w varchar2(10);
ie_parametro341_w varchar2(10);

begin
ie_parametro299_w	:= OBTER_PARAM_USUARIO_LOGADO(450,299);
ie_parametro151_w	:= OBTER_PARAM_USUARIO_LOGADO(450,151);
ie_parametro341_w	:= OBTER_PARAM_USUARIO_LOGADO(450,341);


if	(((ie_parametro299_w = 'S') or (ie_status_p in (1,5))) and
	(ie_parametro151_w = 'N' or ie_parametro341_w = 'S' or dt_inicio_coleta_p is not null)) then
	ie_retorno_w	:= 'S';
else
	ie_retorno_w	:= 'N';
end if;


return	ie_retorno_w;

end obter_se_altera_san_doacao;
/
