create or replace
function obter_medico_por_exame_regra(cd_procedimento_p	number,
				      ie_origem_proced_p	varchar2,
				      nr_seq_proc_interno_p	number,
				      cd_agenda_p		number,
				      dt_agenda_p		date)
 		    	return number is

retorno_w		number(10);
nr_seq_horario_w	number(10);

begin

select 	max(nr_seq_horario)
into   	nr_seq_horario_w
from   	agenda_paciente
where  	cd_agenda = cd_agenda_p
and	trunc(dt_agenda) = trunc(dt_agenda_p)
and	to_char(hr_inicio, 'hh24:mi') = to_char(dt_agenda_p, 'hh24:mi');

select	nvl(max(a.cd_medico),0)
into 	retorno_w
from	agenda_medico_proced a,
	agenda_horario b
where	a.cd_agenda = b.cd_agenda
and	((a.cd_procedimento = cd_procedimento_p) or (a.cd_procedimento is null)) 
and	((a.cd_procedimento is null) or ((a.ie_origem_proced = ie_origem_proced_p) or (a.ie_origem_proced is null))) 
and	((a.nr_seq_proc_interno = nr_seq_proc_interno_p) or (a.nr_seq_proc_interno is null)) 
and	((a.cd_area_procedimento = obter_area_procedimento(cd_procedimento_p, ie_origem_proced_p)) or (a.cd_area_procedimento is null)) 
and	((a.cd_especialidade = obter_especialidade_proced(cd_procedimento_p, ie_origem_proced_p)) or (a.cd_especialidade is null)) 
and	((a.cd_grupo_proc = obter_grupo_procedimento(cd_procedimento_p, ie_origem_proced_p,'C')) or (a.cd_grupo_proc is null))
and	(a.cd_medico = (select 	max(x.cd_medico) 
		        from	agenda_medico x
		        where	x.nr_sequencia = b.nr_seq_medico_exec) or (b.nr_seq_medico_exec is null)) 
and	a.cd_agenda = cd_agenda_p
and	b.nr_sequencia = nr_seq_horario_w;

return	retorno_w;

end obter_medico_por_exame_regra;
/

