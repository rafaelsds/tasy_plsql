create or replace
function pls_obter_medico_partic_dif_oc
			(	nr_seq_proc_p		number,
				cd_medico_p		number,
				nr_seq_grau_partic_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2)
				return varchar2 is

ie_gerar_ocorrencia_w	varchar2(1) := 'N';
nr_seq_proc_ref_w	number(10);
qt_duplicado_w		pls_integer;
	
begin

select	max(nr_seq_proc_ref)
into	nr_seq_proc_ref_w
from	pls_conta_proc
where	nr_sequencia = nr_seq_proc_p;	

if	(nr_seq_proc_ref_w	is null) then
	nr_seq_proc_ref_w	:= nr_seq_proc_p;
end if;

select	count(1)
into	qt_duplicado_w
from	dual
where	exists	(select	'S'
		from	pls_conta_proc a,		
			pls_conta c
		where	c.nr_sequencia		= a.nr_seq_conta
		and	((a.nr_seq_proc_ref 	= nr_seq_proc_ref_w) or ((a.nr_sequencia = nr_seq_proc_ref_w) and (a.nr_seq_proc_ref is null)))
		and	c.cd_medico_executor	= cd_medico_p
		and	c.nr_seq_grau_partic	<> nr_seq_grau_partic_p
		union
		select	'S'
		from	pls_conta_proc a,
			pls_proc_participante b		
		where	b.nr_seq_conta_proc	= a.nr_sequencia
		and	((a.nr_seq_proc_ref 	= nr_seq_proc_ref_w) or ((a.nr_sequencia = nr_seq_proc_ref_w) and (a.nr_seq_proc_ref is null)))
		and	b.cd_medico		= cd_medico_p
		and	b.nr_seq_grau_partic	<> nr_seq_grau_partic_p);

if	(qt_duplicado_w > 0) then
	ie_gerar_ocorrencia_w	:= 'S';
end if;	
	
return	ie_gerar_ocorrencia_w;

end pls_obter_medico_partic_dif_oc;
/