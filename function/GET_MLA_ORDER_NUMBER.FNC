create or replace function get_mla_order_number ( 
    nr_prescricao_p varchar2 ) return number as
							
order_number_w cpoe_order_type_identify.nr_order_patient_seq%type;

begin

select 	max(nr_order_patient_seq)
into  	order_number_w
from 	cpoe_order_type_identify 
where 	nr_prescricao = nr_prescricao_p
and 	ie_situacao = 'A';

return order_number_w;

end get_mla_order_number;
/