create or replace
function man_obter_se_visualiza_os
			(	nr_seq_grupo_des_p		number,
				nr_seq_grupo_sup_p		number,
				nr_seq_grupo_des_os_p		number,
				nr_seq_grupo_sup_os_p		number,
				nr_seq_gerencia_des_os_p	number,
				nr_seq_gerencia_sup_os_p	number,
				ie_forma_sla_p			varchar2,
				nm_usuario_p			varchar2)
 		    	return varchar2 is

nr_seq_gerencia_sup_w		varchar2(2000);
nr_seq_gerencia_sup_aux_w	varchar2(2000);
nr_seq_grupo_sup_os_w		varchar2(2000);
nr_seq_grupo_sup_os_aux_w	varchar2(2000);
nm_usuario_lider_w		varchar2(15);
cd_pessoa_fisica_w		varchar2(10);
ie_retorno_w			varchar2(1)	:= 'N';
ie_contido_w			varchar2(1);
nr_seq_gerencia_des_w		number(10);
nr_seq_gerencia_des_os_w	number(10);

Cursor C01 is
	select	to_char(a.nr_seq_gerencia_sup)
	from	grupo_suporte	a
	where	a.nr_sequencia	= nr_seq_grupo_sup_p;
	
Cursor C02 is
	select	to_char(a.nr_sequencia)
	from	grupo_suporte	a
	where	a.nm_usuario_lider	= nm_usuario_lider_w;
	
Cursor C03 is
	select	to_char(nr_seq_grupo)
	from	usuario_grupo_sup
	where 	nm_usuario_grupo = nm_usuario_p;

begin
if	(nr_seq_grupo_des_p is not null) then
	select	a.nr_seq_gerencia
	into	nr_seq_gerencia_des_w
	from	grupo_desenvolvimento	a
	where	a.nr_sequencia	= nr_seq_grupo_des_p;
end if;

if	(nr_seq_grupo_sup_p is not null) then
	select	a.nm_usuario_lider
	into	nm_usuario_lider_w
	from	grupo_suporte	a
	where	a.nr_sequencia	= nr_seq_grupo_sup_p;
	
	nr_seq_gerencia_sup_w := '(';
	open C01;
	loop
	fetch C01 into	
		nr_seq_gerencia_sup_aux_w;
	exit when C01%notfound;
		begin
		nr_seq_gerencia_sup_w	:= nr_seq_gerencia_sup_w || nr_seq_gerencia_sup_aux_w || ',';
		end;
	end loop;
	close C01;
	
	nr_seq_gerencia_sup_w	:= nr_seq_gerencia_sup_w || ')';
	
	if	(nm_usuario_lider_w = nm_usuario_p) then
		nr_seq_grupo_sup_os_w	:= '(';
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_grupo_sup_os_aux_w;
		exit when C02%notfound;
			begin
			nr_seq_grupo_sup_os_w	:= nr_seq_grupo_sup_os_w || nr_seq_grupo_sup_os_aux_w || ',';
			end;
		end loop;
		close C02;
		
		nr_seq_grupo_sup_os_w	:= nr_seq_grupo_sup_os_w ||  ')';
	else
		begin
		nr_seq_grupo_sup_os_w	:= '(';
		
		open C03;
		loop
		fetch C03 into	
			nr_seq_grupo_sup_os_aux_w;
		exit when C03%notfound;
			begin
			nr_seq_grupo_sup_os_w := nr_seq_grupo_sup_os_w || nr_seq_grupo_sup_os_aux_w || ',';
			end;
		end loop;
		close C03;
		
		nr_seq_grupo_sup_os_w	:= nr_seq_grupo_sup_os_w ||  ')';
		end;
	end if;
	
end if;

if	(ie_forma_sla_p = 'GR') then
	if	(nr_seq_grupo_des_os_p = nr_seq_grupo_des_p) then
		ie_retorno_w	:= 'S';
	end if;
	
	select	obter_se_contido(nr_seq_grupo_sup_os_p, nr_seq_grupo_sup_os_w)
	into	ie_contido_w
	from	dual;
	
	if	(ie_contido_w = 'S') then
		ie_retorno_w	:= 'S';
	end if;
elsif	(ie_forma_sla_p = 'GE') then
	if	(nr_seq_gerencia_des_os_p = nr_seq_gerencia_des_w) then
		ie_retorno_w	:= 'S';
	end if;
	
	select	obter_se_contido(nr_seq_gerencia_sup_os_p, nr_seq_gerencia_sup_w)
	into	ie_contido_w
	from	dual;
	
	if	(ie_contido_w = 'S') then
		ie_retorno_w	:= 'S';
	end if;
elsif	(ie_forma_sla_p = 'GRG') then
	if	(nr_seq_grupo_des_p is not null) then
		if	(nr_seq_grupo_des_os_p = nr_seq_grupo_des_p) then
			ie_retorno_w	:= 'S';
		end if;
		
		if	(ie_retorno_w = 'N') then
			select	max(b.nm_usuario_grupo)
			into	nm_usuario_lider_w
			from	grupo_desenvolvimento	a,
				usuario_grupo_des b
			where	a.nr_sequencia = b.nr_seq_grupo
			and	b.ie_funcao_usuario = 'S'
			and	a.nr_sequencia	= nr_seq_grupo_des_os_p;

			select	a.cd_pessoa_fisica
			into	cd_pessoa_fisica_w
			from	usuario	a
			where	a.nm_usuario	= nm_usuario_lider_w;

			select	decode(count(*),0,'N','S')
			into	ie_retorno_w
			from	gerencia_wheb	a
			where	a.nr_sequencia	= nr_seq_gerencia_des_w
			and	cd_responsavel	= cd_pessoa_fisica_w;
		end if;
	end if;

	if	(nr_seq_grupo_sup_p is not null) then
		select	obter_se_contido(nr_seq_grupo_sup_os_p, nr_seq_grupo_sup_os_w)
		into	ie_contido_w
		from	dual;
		
		if	(ie_contido_w = 'S') then
			ie_retorno_w	:= 'S';
		end if;

		if	(ie_retorno_w = 'N') then
			select	max(a.nm_usuario_lider)
			into	nm_usuario_lider_w
			from	grupo_suporte	a
			where	a.nr_sequencia	= nr_seq_grupo_sup_os_p;

			select	a.cd_pessoa_fisica
			into	cd_pessoa_fisica_w
			from	usuario	a
			where	a.nm_usuario	= nm_usuario_lider_w;

			select	decode(count(*),0,'N','S')
			into	ie_retorno_w
			from	gerencia_wheb	a
			where	obter_se_contido(a.nr_sequencia, nr_seq_gerencia_sup_w) = 'S'
			and	cd_responsavel	= cd_pessoa_fisica_w;
		end if;
	end if;
end if;

return	ie_retorno_w;

end man_obter_se_visualiza_os;
/
