create or replace
function pls_quant_itens_pendentes_exec
			(	qt_solicitada_p	Number,
				qt_executada_p	Number)
				return Number is
				
qt_pendente_w			Number(10);

begin

qt_pendente_w	:= nvl(qt_solicitada_p,0) - nvl(qt_executada_p, 0);

return	qt_pendente_w;

end pls_quant_itens_pendentes_exec;
/