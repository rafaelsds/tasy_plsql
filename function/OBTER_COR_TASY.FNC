create or replace
function obter_cor_tasy(
			nr_sequencia_p		number,
			ie_opcao_p		varchar2,
			cd_perfil_p		number default null,
			cd_estabelecimento_p	number default null)
			return varchar2 is

ds_retorno_w		varchar2(15);
ds_cor_fundo_w		varchar2(15);
ds_cor_fonte_w		varchar2(15);
ds_cor_selecao_w		varchar2(15);
cd_perfil_w		number(05,0);
cd_estabelecimento_w	number(04,0);

/*	ie_opcao_p
	F - Cor de fundo
	T - Cor da fonte
	S - Cor da sele��o	*/

begin

if	(nvl(nr_sequencia_p,0) > 0) then
	begin
	cd_perfil_w		:= nvl(cd_perfil_p,nvl(wheb_usuario_pck.get_cd_perfil,0));
	cd_estabelecimento_w	:= nvl(cd_estabelecimento_p,nvl(wheb_usuario_pck.get_cd_estabelecimento,0));
	
	tasy_obter_cor(nr_sequencia_p,cd_perfil_w,cd_estabelecimento_w,ds_cor_fundo_w,ds_cor_fonte_w,ds_cor_selecao_w);
	
	if	(ie_opcao_p = 'F') then
		ds_retorno_w := ds_cor_fundo_w;
	elsif	(ie_opcao_p = 'T') then
		ds_retorno_w := ds_cor_fonte_w;
	elsif	(ie_opcao_p = 'S') then
		ds_retorno_w := ds_cor_selecao_w;
	end if;
	end;
end if;

return	ds_retorno_w;

end obter_cor_tasy;
/