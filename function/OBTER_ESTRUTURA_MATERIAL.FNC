Create or Replace
Function Obter_estrutura_material (	cd_material_p		number,
					ie_estrutura_p		Varchar2) 
			return Number is

/* 	ie_estrutura_p
	C = Classe do material
	S = SubGrupo do material
	G = Grupo do material
*/

cd_classe_w		Number(5);
cd_subgrupo_w		Number(3);
cd_grupo_w		Number(3);
cont_w			Number(3);
ds_resultado_w		Number(5);

BEGIN

begin
select	1
into	cont_w
from	material
where	cd_material	= cd_material_p;

select	cd_classe_material
into	cd_classe_w
from	material
where	cd_material = cd_material_p;

if	(ie_estrutura_p = 'C') then
	ds_resultado_w	:= cd_classe_w;
elsif	(ie_estrutura_p = 'S') then
	begin
	select	cd_subgrupo_material
	into	cd_subgrupo_w
	from	classe_material
	where	cd_classe_material = cd_classe_w;
	
	ds_resultado_w	:= cd_subgrupo_w;
	end;
elsif	(ie_estrutura_p = 'G') then
	begin
	select	cd_subgrupo_material
	into	cd_subgrupo_w
	from	classe_material
	where	cd_classe_material = cd_classe_w;

	select	cd_grupo_material
	into	cd_grupo_w
	from	subgrupo_material
	where	cd_subgrupo_material = cd_subgrupo_w;
	
	ds_resultado_w	:= cd_grupo_w;
	end;
end if;
exception
when others then
	ds_resultado_w := null;
end;

RETURN ds_resultado_w;

END Obter_estrutura_material;
/