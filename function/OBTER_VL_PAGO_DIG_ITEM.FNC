create or replace
function OBTER_VL_PAGO_DIG_ITEM
			(nr_seq_propaci_p	in number,
			nr_seq_partic_p	in number,
			nr_seq_matpaci_p	in number)
			return number is

vl_retorno_w	number(15,2);			
			
begin



if	(nr_seq_propaci_p is not null) and
	(nr_seq_partic_p is not null) then
	
	select	sum(vl_pago_digitado)
	into	vl_retorno_w
	from	convenio_retorno_glosa
	where	nr_seq_propaci	= nr_seq_propaci_p
	and	nr_seq_partic	= nr_seq_partic_p;
	
elsif	(nr_seq_propaci_p is not null) then
	
	select	sum(vl_pago_digitado)
	into	vl_retorno_w
	from	convenio_retorno_glosa
	where	nr_seq_propaci	= nr_seq_propaci_p
	and	nr_seq_partic	is null;
	
elsif	(nr_seq_matpaci_p is not null) then
	
	select	sum(vl_pago_digitado)
	into	vl_retorno_w
	from	convenio_retorno_glosa
	where	nr_seq_matpaci	= nr_seq_matpaci_p;
	
end if;

return nvl(vl_retorno_w,0);

end OBTER_VL_PAGO_DIG_ITEM;
/