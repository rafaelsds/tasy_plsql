create or replace
function obter_se_nota_entrada_nfe(nr_sequencia_p		number)
 		    	return varchar2 is
			
ie_entrada_saida_w varchar2(3);
ie_recusa_w        operacao_nota.ie_recusa%type;
ie_nota_recusa_w   operacao_nota.ie_nota_recusa%type;
			
begin

select nvl(obter_se_nota_entrada_saida(a.nr_sequencia),'S'),
       nvl(b.ie_recusa,'N'),
       nvl(b.ie_nota_recusa,'N')
into   ie_entrada_saida_w,
       ie_recusa_w,
       ie_nota_recusa_w
from   nota_fiscal a,
       operacao_nota b
where  a.nr_sequencia = nr_sequencia_p
and    a.cd_operacao_nf = b.cd_operacao_nf;

if ((ie_entrada_saida_w = 'E') and (ie_recusa_w = 'S') and (ie_nota_recusa_w = 'N')) then
    return 'S';
else
    return 'N';
end if;

end obter_se_nota_entrada_nfe;
/
