create or replace
function cpoe_obter_se_exibe_protocolo(	nr_seq_protocolo_p	number,
					ie_tipo_peritoneal_p	varchar2,
					ie_destino_p		number,
					ie_tipo_peritoneal_cpoe_p varchar2)
					return varchar2 is

ie_exibe_w				varchar2(1) := 'N';
ie_tipo_peritoneal_w	varchar2(15);
cont_w					number(5,0);

begin

ie_tipo_peritoneal_w := ie_tipo_peritoneal_cpoe_p;
	
	if	(ie_tipo_peritoneal_w is not null) and
		(ie_destino_p = 3) and
		((ie_tipo_peritoneal_w = ie_tipo_peritoneal_p) or
		 (ie_tipo_peritoneal_p is null)) then
		ie_exibe_w	:= 'S';
	elsif	(ie_tipo_peritoneal_w is null) and
		(ie_destino_p = 2) then
		ie_exibe_w	:= 'S';
	end if;

	if	(ie_exibe_w	= 'N') then
		select	count(*)
		into	cont_w
		from	tipo_dialise_solucao
		where	nr_seq_Protocolo_npt	= nr_seq_protocolo_p;
		
		if	(cont_w	> 0) then
			select	count(*)
			into	cont_w
			from	tipo_dialise_solucao
			where	nr_seq_Protocolo_npt	= nr_seq_protocolo_p
			and	ie_tipo_peritoneal	= ie_tipo_peritoneal_w;
			
			if	(cont_w	> 0) then
				ie_exibe_w	:= 'S';
			end if;
		end if;
	end if;
	
return ie_exibe_w;

end cpoe_obter_se_exibe_protocolo;
/
