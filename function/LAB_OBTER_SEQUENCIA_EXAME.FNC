create or replace
function lab_obter_sequencia_exame( nr_prescricao_p		varchar2,
									nr_seq_prescr_p		varchar2,
									nr_seq_exame_p		varchar2)
 		    	return varchar2 is
				
ds_retorno_w		varchar2(15);				
nr_seq_exame_w		exame_laboratorio.nr_seq_exame%type;

begin


select 	linhaTeste,
		nr_seq_exame
into	ds_retorno_w,
		nr_seq_exame_w
from 
	(	SELECT rownum linhaTeste, nr_seq_prescr, nr_seq_exame
		from (
			 select r.nr_seq_prescr,
					w.nr_prescricao,
					r.nr_seq_exame
			 from exame_lab_resultado w,
					exame_lab_result_item r
			 where w.nr_seq_resultado = r.nr_seq_resultado
			 and w.nr_prescricao = nr_prescricao_p
			)
		where nr_seq_prescr = nr_seq_prescr_p)
where	nr_seq_exame = nr_seq_exame_p;




return	ds_retorno_w;

end lab_obter_sequencia_exame;
/
