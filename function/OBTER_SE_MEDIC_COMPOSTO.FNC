create or replace
function obter_se_medic_composto(	nr_prescricao_p		number,
									nr_seq_material_p	number,
									nr_agrupamento_p	number)
								return char is
					
ie_retorno_w	char(1)	:= 'N';
					
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_material_p is not null) and
	(nr_agrupamento_p is not null) then	
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	prescr_material
	where	rownum = 1
	and		nr_prescricao	= nr_prescricao_p
	and		nr_sequencia	<> nr_seq_material_p
	and		nr_agrupamento	= nr_agrupamento_p
	and		ie_agrupador	= 1
	and		cd_kit_material is null
	and		nr_seq_substituto is null;	
end if;

return ie_retorno_w;

end obter_se_medic_composto;
/
