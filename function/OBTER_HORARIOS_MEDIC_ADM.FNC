create or replace
function obter_horarios_medic_adm(nr_prescricao_p	in number,
			          nr_seq_material_p	in number) return varchar2 is 
			      
ds_horario_w		varchar2(50);
ds_horario_ww		varchar2(50);
ds_horario_adm_w	varchar2(50);
ds_horarios_w		varchar2(2000);

cursor c01 is 
select	ds_horario
from	prescr_mat_hor
where	nr_prescricao 	= nr_prescricao_p
and	nr_seq_material = nr_seq_material_p
order by 
	ds_horario;
	
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_material_p is not null) then
	open c01;
	loop
	fetch c01 into ds_horario_w;
	exit when c01%notfound;
		begin
		
		select	max('('||ds_horario||')') ds_horario_adm 
		into 	ds_horario_adm_w
		from	prescr_mat_hor
		where	dt_fim_horario is not null
		and 	nr_prescricao 	= nr_prescricao_p 
		and 	nr_seq_material = nr_seq_material_p
		and 	ds_horario	= ds_horario_w
		order by
			ds_horario;
		
		if	(ds_horario_adm_w is not null) then
			if (ds_horarios_w is null) then
				ds_horarios_w := ds_horario_adm_w;
			else
				ds_horarios_w := ds_horarios_w||' '||ds_horario_adm_w;
			end if;
		else
			if (ds_horarios_w is null) then
				ds_horarios_w := ds_horario_w;
			else
				ds_horarios_w := ds_horarios_w||' '||ds_horario_w;
			end if;
		end if;
		
		end;
	end loop;
	close c01;
	
return ds_horarios_w;

end if;

end obter_horarios_medic_adm;
/