create or replace
function obter_cd_siscoserv_pais(nr_seq_pais_p number)
 		    	return varchar2 is

cd_siscoserv_w	varchar2(10);
			
begin

select	cd_pais_siscoserv
into	cd_siscoserv_w
from	pais
where	nr_sequencia = nr_seq_pais_p;

return	cd_siscoserv_w;

end obter_cd_siscoserv_pais;
/
