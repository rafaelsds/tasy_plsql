create or replace function pls_desc_tipo_solic (
    nr_seq_tipo_solicitacao_p number
) return varchar2 is
    ds_retorno_w varchar2(255);
begin 
    select nr_sequencia || ' - ' || ds_tipo_solicitacao 
    into ds_retorno_w    
    from pls_solic_creden_prest 
    where nr_sequencia = nr_seq_tipo_solicitacao_p;
    
    return ds_retorno_w;
    
end pls_desc_tipo_solic;
/
