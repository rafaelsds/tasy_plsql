create or replace
function synapse_obter_se_laudo(
				ie_gestao_entrega_p	varchar2,
				ie_executar_leito_p	varchar2,
				ie_avisar_result_p	varchar2)
 		    	return varchar2 is

ie_envia_laudo_w		varchar2(1);
			
begin

if	(nvl(ie_gestao_entrega_p,'N') = 'S') then
	begin
	
	if	(nvl(ie_executar_leito_p,ie_avisar_result_p) = 'S') then
		ie_envia_laudo_w	:=	'Y';
	elsif   (nvl(ie_executar_leito_p,ie_avisar_result_p) = 'N') then
		ie_envia_laudo_w	:=	'N';
	end if;
	
	end;
	
elsif  (nvl(ie_gestao_entrega_p,'N') = 'N') then
	begin
	
	if	(nvl(ie_executar_leito_p,ie_avisar_result_p) = 'S') then
		ie_envia_laudo_w	:=	'N';
	elsif   (nvl(ie_executar_leito_p,ie_avisar_result_p) = 'N') then
		ie_envia_laudo_w	:=	'N';
	end if;
	
	end;
end if;


return	ie_envia_laudo_w;

end synapse_obter_se_laudo;
/
