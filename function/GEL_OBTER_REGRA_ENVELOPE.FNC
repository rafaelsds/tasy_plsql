create or replace
function Gel_Obter_Regra_Envelope(nr_seq_envelope_p			number)
 		    	return number is

nr_Seq_regra_w			number(10);				
				
begin

select	nr_seq_regra_envelope
into	nr_seq_regra_w
from	envelope_laudo
where	nr_sequencia = nr_seq_envelope_p;

return	nr_seq_regra_w;

end Gel_Obter_Regra_Envelope;
/