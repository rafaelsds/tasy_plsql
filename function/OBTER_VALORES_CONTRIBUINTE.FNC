create or replace
function obter_valores_contribuinte(	nr_seq_contribuinte_p	number,
					ie_opcao_p		varchar2)
					return number is

/*
Op��es
T - Valor Total
U - valor Utilizado
S - Valor Saldo
*/					
									
vl_retorno_w			number(15,2);
vl_total_w			number(15,2);
vl_utilizado_w			number(15,2);
vl_saldo_w			number(15,2);

begin

select	nvl(sum(a.vl_total), 0) vl_total,
	nvl(sum(a.vl_utilizado), 0) vl_utilizado,
	nvl(sum(a.vl_total), 0) - nvl(sum(a.vl_utilizado), 0) vl_saldo 
into	vl_total_w,
	vl_utilizado_w,
	vl_saldo_w
from	totais_contribuinte_v a
where	a.nr_seq_contribuinte	= nr_seq_contribuinte_p;

if (ie_opcao_p = 'T') then
	vl_retorno_w := vl_total_w;
elsif (ie_opcao_p = 'U') then
	vl_retorno_w := vl_utilizado_w;
elsif (ie_opcao_p = 'S') then
	vl_retorno_w := vl_saldo_w;
end if;

return	vl_retorno_w;

end obter_valores_contribuinte;
/