create or replace
function obter_asteristicos_num ( qt_caracteres_p	 number)
 		    	return varchar2 is
			
i    integer;
retorno_w 		varchar2(400) := '';
qt_caracteres_w		number(10);


begin

qt_caracteres_w := nvl(qt_caracteres_p, 0);


for i in 1..qt_caracteres_w loop
	begin
	retorno_w := retorno_w || '*';
	end;
end loop;

return	retorno_w;

end obter_asteristicos_num;
/