create or replace 
function obter_dados_mensuracao_atual(
				nr_seq_cp_indicator_p	number,
				nr_seq_prescr_p			number,
				ie_opcao_p				varchar2)
			return varchar2 is 

ds_retorno_w	varchar2(255);
nr_sequencia_w	number(10);
nr_seq_aux_w 	number(10);

begin

if (nr_seq_prescr_p is not null) and
	(nr_seq_cp_indicator_p is not null) then

	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	patient_cp_indic_measure
	where	nr_seq_cp_indicator   = nr_seq_cp_indicator_p
	and		nr_seq_prescr		= nr_seq_prescr_p
	and 	(dt_liberacao is not null or nm_usuario = wheb_usuario_pck.get_nm_usuario)
	order 	by dt_evaluation desc;

	if (nr_sequencia_w is not null) then
	
		if (ie_opcao_p = 'SC') then
		
			Select 	nvl(max(a.nr_score),0) nr_score
			into	ds_retorno_w
			from 	patient_cp_indic_measure a
			where 	nr_seq_cp_indicator   = nr_seq_cp_indicator_p
			and		nr_seq_prescr		= nr_seq_prescr_p
			and 	dt_liberacao is not null
			and		nr_score > 0
			order 	by a.dt_evaluation desc;
		
		elsif (ie_opcao_p = 'DSM') then
		
			select	nvl(a.ds_alternative,a.ds_display_name) ds_display_name
			into	ds_retorno_w
			from	cp_measure a
			where 	a.nr_sequencia = (select 	max(b.nr_seq_measure) 
									from 		patient_cp_indic_measure b
									where 	b.nr_sequencia = nr_sequencia_w);
									
		elsif (ie_opcao_p = 'CDM') then
		
			Select 	nvl(a.nr_seq_measure,0) nr_seq_measure
			into	nr_seq_aux_w
			from 	patient_cp_indic_measure a
			where 	a.nr_sequencia = nr_sequencia_w
			order 	by a.dt_evaluation desc;
			
			select 
					max(nvl(b.ds_alternative,b.ds_display_name)) ds
			into	ds_retorno_w
			from 	cp_measure_type a,
					cp_measure b,
					cp_indicator c
			where 	a.nr_sequencia = b.nr_seq_measure_type
			and 	c.nr_seq_measure_type = a.nr_sequencia
			and     b.nr_sequencia = nr_seq_aux_w;
			
			if(ds_retorno_w is null) then
				select
						max(nvl(b.ds_alternative,b.ds_display_name)) ds
				into	ds_retorno_w
				from 	cp_indicator_measure a,
						cp_measure b,
						cp_indicator c
				where 	a.nr_seq_measure = b.nr_sequencia
				and 	c.nr_sequencia = a.nr_seq_indicator
				and     b.nr_sequencia = nr_seq_aux_w
				and 	c.nr_seq_measure_type is null;
			end if;
	
		elsif (ie_opcao_p = 'NT') then
		
			Select 	a.ds_note
			into	ds_retorno_w
			from 	patient_cp_indic_measure a
			where 	a.nr_sequencia = nr_sequencia_w
			order 	by a.dt_evaluation desc;
			
		elsif (ie_opcao_p = 'SEQ') then
		
			ds_retorno_w := nr_sequencia_w;
		
		elsif (ie_opcao_p = 'EVL') then
		
			Select 	a.ie_evolution
			into	ds_retorno_w
			from 	patient_cp_indic_measure a
			where 	a.nr_sequencia = nr_sequencia_w
			order 	by a.dt_evaluation desc;
			
		elsif (ie_opcao_p = 'IEM') then
		
			Select 	obter_valor_dominio(10106, a.IE_MEASURE)
			into	ds_retorno_w
			from 	patient_cp_indic_measure a
			where 	a.nr_sequencia = nr_sequencia_w
			order 	by a.dt_evaluation desc;
			
		elsif (ie_opcao_p = 'NRM') then
		
			select 	max(a.NR_SEQ_MEASURE)
            into	nr_seq_aux_w
			from 	PATIENT_CP_INDIC_PLAN a, 
					PATIENT_CP_INDIC_MEASURE b 
			where 	b.nr_seq_plan_indic = a.nr_sequencia 
			and 	b.nr_seq_cp_indicator   = nr_seq_cp_indicator_p
			and		b.nr_seq_prescr		= nr_seq_prescr_p
			order 	by b.dt_evaluation desc;
		
			select 
					max(nvl(b.ds_alternative,b.ds_display_name)) ds
			into	ds_retorno_w
			from 	cp_measure_type a,
					cp_measure b,
					cp_indicator c
			where 	a.nr_sequencia = b.nr_seq_measure_type
			and 	c.nr_seq_measure_type = a.nr_sequencia
			and     b.nr_sequencia = nr_seq_aux_w;
			
			if(ds_retorno_w is null) then
				select
						max(nvl(b.ds_alternative,b.ds_display_name)) ds
				into	ds_retorno_w
				from 	cp_indicator_measure a,
						cp_measure b,
						cp_indicator c
				where 	a.nr_seq_measure = b.nr_sequencia
				and 	c.nr_sequencia = a.nr_seq_indicator
				and     b.nr_sequencia = nr_seq_aux_w
				and 	c.nr_seq_measure_type is null;
			end if;
			
		
		elsif (ie_opcao_p = 'NMG') then
		
			select 	max(a.NR_SEQ_MEASURE_GOAL)
            into	nr_seq_aux_w
			from 	PATIENT_CP_INDIC_PLAN a, 
					PATIENT_CP_INDIC_MEASURE b 
			where 	b.nr_seq_plan_indic = a.nr_sequencia 
			and 	b.nr_seq_cp_indicator   = nr_seq_cp_indicator_p
			and		b.nr_seq_prescr		= nr_seq_prescr_p
			order 	by b.dt_evaluation desc;
			
			select 
					max(nvl(b.ds_alternative,b.ds_display_name)) ds
			into	ds_retorno_w
			from 	cp_measure_type a,
					cp_measure b,
					cp_indicator c
			where 	a.nr_sequencia = b.nr_seq_measure_type
			and 	c.nr_seq_measure_type = a.nr_sequencia
			and     b.nr_sequencia = nr_seq_aux_w;
			
			if(ds_retorno_w is null) then
				select
						max(nvl(b.ds_alternative,b.ds_display_name)) ds
				into	ds_retorno_w
				from 	cp_indicator_measure a,
						cp_measure b,
						cp_indicator c
				where 	a.nr_seq_measure = b.nr_sequencia
				and 	c.nr_sequencia = a.nr_seq_indicator
				and     b.nr_sequencia = nr_seq_aux_w
				and 	c.nr_seq_measure_type is null;
			end if;

    elsif (ie_opcao_p = 'IPM') THEN
        select max(nvl(a.ie_planned_measure,'N'))
        into ds_retorno_w
        from patient_cp_indic_measure a
        where a.nr_sequencia = nr_sequencia_w
        order by a.dt_evaluation desc;

		elsif (ie_opcao_p = 'NRSEQ') THEN
			Select 	nvl(a.nr_seq_measure,0) nr_seq_measure
			into	ds_retorno_w
			from 	patient_cp_indic_measure a
			where 	a.nr_sequencia = nr_sequencia_w
			order 	by a.dt_evaluation desc;
		end if;
	end if;
	
end if;

return ds_retorno_w;

end obter_dados_mensuracao_atual;
/