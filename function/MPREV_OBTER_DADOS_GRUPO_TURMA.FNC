create or replace
function mprev_obter_dados_grupo_turma(	nr_seq_turma_p		number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar diferentes informa��es da turma de um grupo de atendimento da medicina preventiva.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
ie_opcao_p:
'N' - Nome da turma
'NG' - Nome do grupo coletivo
'NNG' - Nome do grupo coletivo ||' - '|| Nome da turma 
'ST' - Situa��o da turma. ( Vigente na data atual ).
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 	

ds_retorno_w 		varchar2(4000);

begin
if 	(nr_seq_turma_p is not null) and
	(ie_opcao_p is not null) then
	if 	(ie_opcao_p = 'N') then
		select	substr(a.nm_turma, 1, 255)
		into	ds_retorno_w
		from	mprev_grupo_col_turma a
		where	a.nr_sequencia = nr_seq_turma_p;
	elsif 	(ie_opcao_p = 'NG') then
		select	substr(mprev_obter_dados_grupo_atend(a.nr_seq_grupo_coletivo, 'NM'), 1, 255)
		into	ds_retorno_w
		from	mprev_grupo_col_turma a
		where	a.nr_sequencia = nr_seq_turma_p;
	elsif	(ie_opcao_p = 'NNG') then
		select substr(mprev_obter_dados_grupo_atend(a.nr_seq_grupo_coletivo, 'NM') ||' - '|| a.nm_turma,1,255)
		into	ds_retorno_w
		from 	mprev_grupo_col_turma a
		where	a.nr_sequencia = nr_seq_turma_p;
	elsif	(ie_opcao_p = 'ST') then
		select	coalesce(max('S'),'N')
		into	ds_retorno_w
		from	mprev_grupo_col_turma a
		where	a.nr_sequencia = nr_seq_turma_p
		and	sysdate between nvl(a.dt_inicio,sysdate) and nvl(a.dt_termino,sysdate);
	end if;	
end if;

return	ds_retorno_w;

end mprev_obter_dados_grupo_turma;
/
