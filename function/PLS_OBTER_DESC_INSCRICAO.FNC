create or replace
function pls_obter_desc_inscricao
			(	nr_seq_inscricao_p	Number)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(2000);

begin

select	substr('Da '||qt_parcela_inicial||'� � '||qt_parcela_final||'� parcela - Vl. inscri��o:'||Campo_Mascara_virgula(vl_inscricao)||' - Tx. inscri��o:'||tx_inscricao||'% '||Decode(ie_grau_dependencia,'T',' - Titulares','D',' - Dependentes'),1,255)
into	ds_retorno_w
from	pls_regra_inscricao
where	nr_sequencia	= nr_seq_inscricao_p;

return	ds_retorno_w;

end pls_obter_desc_inscricao;
/