create or replace function	obter_dif_data(	dt_inicial_p	date,
							dt_final_p 		date,
							ie_opcao_p 		varchar2)
							return varchar2 is
/*
 Utilizar o parametro ie_opcao_p para customizações
*/
qt_segundo_w    number(10);
qt_min_w   	number(10);
qt_hora_w	number(10);
qt_dia_w	number(10);
qt_retorno_w	varchar2(100);

qt_segundo_inicial_w 	number(10);
qt_segundo_fim_w 	number(10);

qt_hora_aux_w	number(10);

begin

	qt_segundo_inicial_w 	:= to_number(to_char(dt_inicial_p,'SSSSS'));
	qt_segundo_fim_w	:= to_number(to_char(dt_final_p,'SSSSS'));

	if	( qt_segundo_inicial_w > qt_segundo_fim_w ) then
		qt_hora_aux_w	:= 24;
	end if;

	qt_segundo_w 	:= qt_segundo_fim_w - qt_segundo_inicial_w;

	qt_dia_w     	:= TRUNC(dt_final_p - dt_inicial_p);
	qt_hora_w 	:= TRUNC(qt_segundo_w/60/60);
	qt_min_w	:= TRUNC(MOD(qt_segundo_w/60, 60));
	qt_segundo_w	:= MOD(qt_segundo_w, 60);

	if	( qt_hora_aux_w is not null ) then
		qt_hora_w := qt_hora_aux_w - abs(qt_hora_w);
	end if;

	if	( qt_segundo_w < 0 )then
		qt_segundo_w 	:= 60 + qt_segundo_w;
		qt_min_w	:= qt_min_w - 1;
	end if;
	if	( qt_min_w < 0 )then
		qt_min_w 	:= 60 + qt_min_w;
		qt_hora_w	:= qt_hora_w - 1;
	end if;
	if	( qt_dia_w > 0 ) then
		qt_hora_w := qt_hora_w + (qt_dia_w * 24) ;
	end if;
	if	( qt_hora_w < 10 ) then
		qt_retorno_w := '0';
	end if;
	qt_retorno_w := qt_retorno_w || qt_hora_w || ':';		
	if	( qt_min_w < 10 ) then
		qt_retorno_w := qt_retorno_w || '0';
	end if;
	qt_retorno_w := qt_retorno_w || qt_min_w || ':' ;
	if	( qt_segundo_w < 10) then
		qt_retorno_w := qt_retorno_w || '0'; 
	end if;
	
	--Completo: 0 dia(s) 0 hora(s) 0 minuto(s) 0 segundo(s)
	if	(ie_opcao_p = 'TD') then
		begin
		qt_retorno_w	:= WHEB_MENSAGEM_PCK.get_texto(458071, 'qt_dia_w='||qt_dia_w||';qt_hora_w='|| (qt_hora_w - (qt_dia_w*24)) ||
                    ';qt_min_w='||qt_min_w||';qt_segundo_w='||qt_segundo_w);
                    
		return qt_retorno_w;
		end;
	end if;
	
	--Simplificado: 0d 00h : 00min : 0s
	if	(ie_opcao_p = 'S') then
		begin
		qt_retorno_w	:= 	WHEB_MENSAGEM_PCK.get_texto(458086, 'qt_dia_w='||nvl(qt_dia_w,0)||';qt_hora_w='
    ||nvl((qt_hora_w - (qt_dia_w * 24)),0) || ';qt_min_w='||lpad(nvl(qt_min_w,0),2,0));
		
    return qt_retorno_w;
		end;
	end if;
	
	--Retornar apenas a qtd. total no formato númerico: dias + horas + minutos (quantidade total de minutos)
	if	(ie_opcao_p = 'TM') then
		begin
		qt_hora_w := (qt_hora_w - (qt_dia_w*24));
		qt_retorno_w := ((nvl(qt_dia_w, 0) * 24) * 60) + (nvl(qt_hora_w, 0) * 60) + nvl(qt_min_w,0);
		return qt_retorno_w;
		end;
	end if;
	
	--Retornar apenas a qtd. total no formato númerico: dias + horas + minutos + segundos (quantidade total de segundos)
	if	(ie_opcao_p = 'TMS') then
		begin
		qt_hora_w := (qt_hora_w - (qt_dia_w*24));
		qt_retorno_w := ((nvl(qt_dia_w, 0) * 24) * 60 * 60) + (nvl(qt_hora_w, 0) * 60 * 60) + (nvl(qt_min_w,0) * 60) + nvl(qt_segundo_w,0);
		return qt_retorno_w;
		end;
	end if;
	
	--Retornar apenas a qtd. total de segundos sem formatação
	if	(ie_opcao_p = 'TS') then
		begin
		qt_retorno_w := (((nvl(qt_dia_w, 0) * 24) * 60) * 60) + ((nvl(qt_hora_w, 0) * 60) * 60) + (nvl(qt_min_w,0) * 60) + nvl(qt_segundo_w,0);
		return qt_retorno_w;
		end;
	end if;

	/*Retorna Zero se a data fim for menor que a inicial, ou seja, quando estiver adiantado - usado apenas no cirurgico*/
	if	((ie_opcao_p = 'CIR') and
		(dt_inicial_p > dt_final_p)) then
		begin
		return '00:00:00';
		end;
	end if;
	
	if	(qt_retorno_w || qt_segundo_w = '::') then
		return null;
	else	
		return qt_retorno_w || qt_segundo_w;
	end if;
end;
/