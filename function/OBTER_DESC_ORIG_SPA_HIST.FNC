create or replace 
function obter_desc_orig_spa_hist(	ie_origem_historico_p	varchar2)
				return varchar2 is


ds_tipo_historico		varchar2(4000);

begin

if (ie_origem_historico_p = 'M') then
	ds_tipo_historico := wheb_mensagem_pck.get_texto(1084725);
else
	ds_tipo_historico := wheb_mensagem_pck.get_texto(1084724);
end if;


return	ds_tipo_historico;

end obter_desc_orig_spa_hist;
/