CREATE OR REPLACE
FUNCTION Obter_Classif_Proc_Interno	(nr_seq_classificacao_p	number)
						RETURN VARCHAR2 IS

ds_retorno_w	VARCHAR2(80);

BEGIN

if	(nr_seq_classificacao_p is not null) then
	select	ds_classificacao
	into	ds_retorno_w
	from	proc_interno_classif
	where	nr_sequencia = nr_seq_classificacao_p;
end if;

RETURN	ds_retorno_w;

END 	Obter_Classif_Proc_Interno;
/