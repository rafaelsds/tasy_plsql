create or replace 
FUNCTION hrm_obter_proximo_setor( nr_seq_interno_p NUMBER,
				 nr_atendimento_p NUMBER)
				RETURN NUMBER IS

cd_setor_atendimento_w NUMBER(5);
cd_proximo_setor_w NUMBER(5);
nr_seq_interno_w  NUMBER(15);

BEGIN

SELECT NVL(MIN(nr_seq_interno),0)
INTO nr_seq_interno_w
FROM atend_paciente_unidade
WHERE nr_atendimento = nr_atendimento_p
AND nr_seq_interno > nr_seq_interno_p;

IF (nr_seq_interno_w > 0) THEN
 SELECT NVL(a.cd_setor_atendimento,cd_setor_atendimento_w)
 INTO cd_proximo_setor_w
 FROM atend_paciente_unidade a
 WHERE nr_atendimento = nr_atendimento_p
 AND nr_seq_interno = nr_seq_interno_w;
ELSE
 cd_proximo_setor_w := 0;
END IF;

RETURN cd_proximo_setor_w;

END hrm_obter_proximo_setor;

/