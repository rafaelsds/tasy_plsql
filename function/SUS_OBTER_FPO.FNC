CREATE OR REPLACE
FUNCTION Sus_Obter_Fpo
		(	cd_procedimento_p	Number,
			ie_origem_proced_p	Number,
			dt_vigencia_p		Date,
			ie_tipo_p		Number)
			return Number is

/* IE_TIPO_P
	1 - Fisico
	2 - Orcamentario
	3 - Sequencia
*/


cd_subgrupo_bpa_w	Number(15);
cd_grupo_bpa_w		Number(15);
qt_fisico_w		Number(10);
vl_orcamentario_w	Number(12,2);
ds_retorno_w		Number(12,2);
nr_sequencia_w		Number(10);


CURSOR C01 IS
select	qt_fisico,
	vl_orcamentario,
	nr_sequencia
from	sus_fpo_bpa
where	ie_origem_proced		= ie_origem_proced_p
and	((cd_procedimento is null)	or (cd_procedimento = cd_procedimento_p))
and	((cd_subgrupo_bpa is null) 	or (cd_subgrupo_bpa = cd_subgrupo_bpa_w))
and	((cd_grupo_bpa is null) 	or (cd_grupo_bpa = cd_grupo_bpa_w))
and	dt_vigencia_p 			>= dt_competencia
order by	
	nvl(cd_procedimento,0),
	nvl(cd_subgrupo_bpa,0),
	nvl(cd_grupo_bpa,0);


BEGIN

/* Obtera estrutura BPA */
begin
select	cd_subgrupo_bpa,
	cd_grupo_bpa
into	cd_subgrupo_bpa_w,
	cd_grupo_bpa_w
from	sus_estrutura_bpa_v
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
exception
	when others then
	cd_subgrupo_bpa_w	:= null;
	cd_grupo_bpa_w 		:= null;
end;

OPEN C01;
LOOP
FETCH C01 into
	qt_fisico_w,
	vl_orcamentario_w,
	nr_sequencia_w;
	exit when c01%notfound;
	BEGIN
	if	(ie_tipo_p	= 1) then
		ds_retorno_w	:= qt_fisico_w;
	elsif	(ie_tipo_p	= 2) then
		ds_retorno_w	:= vl_orcamentario_w;
	elsif	(ie_tipo_p	= 3) then
		ds_retorno_w	:= nr_sequencia_w;
	end if;
	END;
END LOOP;
CLOSE C01;

return	ds_retorno_w;

END Sus_Obter_Fpo;
/
