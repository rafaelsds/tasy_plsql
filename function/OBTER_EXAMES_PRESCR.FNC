CREATE OR REPLACE
FUNCTION Obter_Exames_Prescr	(nr_prescricao_p	number)
					return varchar2 IS

ds_procedimento_w 	PROCEDIMENTO.DS_PROCEDIMENTO%type;
ds_exames_w		Varchar2(2000);

cursor c01 is
select	substr(obter_desc_prescr_proc(b.cd_procedimento, b.ie_origem_proced, a.nr_seq_proc_interno),1,240)
from	procedimento b,
	prescr_procedimento a
where	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	a.nr_prescricao		= nr_prescricao_p;

begin
if	(nr_prescricao_p is not null) then
	open c01;
	loop
	fetch c01 into	ds_procedimento_w;
	exit when c01%notfound;

	ds_exames_w	:= ds_exames_w || ds_procedimento_w || ', ';

end loop;
end if;

return	ds_exames_w;

END	Obter_Exames_Prescr;
/