create or replace
function lab_obter_se_exames_entregues(	nr_prescricao_p		number)
				return varchar2 is
				
cd_pessoa_fisica_w	varchar2(10);
lista_nr_prescricao_w	varchar2(255);
nr_prescricao_w		number;
				
cursor c01 is
select	d.nr_prescricao
from 	(select a.nr_prescricao, c.dt_prescricao 
	from	prescr_procedimento a,
		prescr_medica c
	where	a.nr_seq_exame is not null
	and	c.nr_prescricao = a.nr_prescricao
	and	not exists(	select b.nr_sequencia
				from	lab_entrega_exame b
				where  b.nr_prescricao = a.nr_prescricao
				and	b.nr_seq_prescr = a.nr_sequencia)
	and	a.nr_prescricao < nr_prescricao_p
	and	c.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	a.ie_status_atend >= 40
	and	a.dt_baixa is null
	order by a.nr_prescricao desc) d
where rownum <= 10;

begin

select cd_pessoa_fisica
into cd_pessoa_fisica_w
from prescr_medica
where nr_prescricao = nr_prescricao_p;

lista_nr_prescricao_w 	:= '';
nr_prescricao_w 	:= 0;

open c01;
loop
	fetch c01 into nr_prescricao_w;
	exit when c01%notfound;	
	begin
		lista_nr_prescricao_w := lista_nr_prescricao_w || nr_prescricao_w || ', ';
	end;
end loop;
close c01;

lista_nr_prescricao_w := substr(lista_nr_prescricao_w, 1, length(lista_nr_prescricao_w)-2);
	
return	lista_nr_prescricao_w;

end lab_obter_se_exames_entregues;
/
