create or replace
function obter_nr_nota_fiscal_adiant(nr_adiantamento_p		number)
 		    	return number is
			
nr_nota_fiscal_w  nota_fiscal.nr_nota_fiscal%type;			

begin

select min(nr_nota_fiscal)
into   nr_nota_fiscal_w
from   nota_fiscal
where  nr_seq_adiantamento = nr_adiantamento_p;

return nr_nota_fiscal_w;

end obter_nr_nota_fiscal_adiant;
/
