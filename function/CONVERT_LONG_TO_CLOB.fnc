CREATE OR REPLACE FUNCTION CONVERT_LONG_TO_CLOB(DS_TABELA_P     VARCHAR2,
                                                DS_CAMPO_LONG_P VARCHAR2,
                                                DS_CONDICAO_P   VARCHAR2) RETURN CLOB IS
  CURSOR_W       BINARY_INTEGER;
  SQL_W          VARCHAR2(32767);
  VALUE_W        VARCHAR2(32767);
  CLOB_W         CLOB;
  VALUE_LENGTH_W INTEGER := 32767;
  LENGTH_W       INTEGER := 0;
BEGIN
  SQL_W := 'SELECT ' || DS_CAMPO_LONG_P || ' FROM ' || DS_TABELA_P;

  IF (TRIM(DS_CONDICAO_P) IS NOT NULL) THEN
    SQL_W := SQL_W || ' WHERE ' || DS_CONDICAO_P;
  END IF;

  CURSOR_W := DBMS_SQL.OPEN_CURSOR;
  DBMS_SQL.PARSE(CURSOR_W, SQL_W, DBMS_SQL.NATIVE);

  DBMS_SQL.DEFINE_COLUMN_LONG(CURSOR_W, 1);

  IF DBMS_SQL.EXECUTE_AND_FETCH(CURSOR_W) = 1 THEN
    LOOP
      DBMS_SQL.COLUMN_VALUE_LONG(CURSOR_W,
                                 1,
                                 32767,
                                 LENGTH_W,
                                 VALUE_W,
                                 VALUE_LENGTH_W);
      CLOB_W   := CLOB_W || VALUE_W;
      LENGTH_W := LENGTH_W + 32767;
      EXIT WHEN VALUE_LENGTH_W < 32767;
    END LOOP;
  END IF;

  IF DBMS_SQL.IS_OPEN(CURSOR_W) THEN
    DBMS_SQL.CLOSE_CURSOR(CURSOR_W);
  END IF;

  RETURN CLOB_W;
END CONVERT_LONG_TO_CLOB;
/
