create or replace
function pls_obter_se_lib_contratante
			(	nr_seq_guia_p		number,
				nr_seq_requisicao_p	number)
				return Varchar2 is

nr_seq_contrato_w		number(10);
nr_seq_plano_w			number(10);
qt_regra_pos_estab_w		number(10);
cd_area_w			number(10);
cd_especialidade_w		number(10);
cd_grupo_w			number(10);
cd_procedimento_w		number(10);
ie_origem_proced_w		number(10);
nr_seq_regra_pos_estab_w	number(10);
ie_liberado_w			varchar2(1);
vl_procedimento_w		number(15,2);
ds_retorno_w			varchar2(1) := 'S';

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		vl_procedimento
	from	pls_guia_plano_proc
	where	nr_seq_guia = nr_seq_guia_p
	order by 1;
	
Cursor C02 is
	select	cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao = nr_seq_requisicao_p
	and	ie_status in ('P','S')
	order by 1;
	
Cursor C03 is
	select	ie_liberado,
		nr_sequencia
	from(
		select	c.ie_liberado ie_liberado,
			b.nr_sequencia nr_sequencia,
			c.cd_area_procedimento cd_area_procedimento,
			c.cd_especialidade cd_especialidade,
			c.cd_grupo_proc cd_grupo_proc,
			c.cd_procedimento cd_procedimento
		from	pls_tipo_pos_estab_proc		c,
			pls_pos_estab_aut_previa	b,
			pls_tipo_pos_estabelecido	a
		where	c.nr_seq_tipo_pos_estab = a.nr_sequencia
		and	b.nr_seq_tipo_pos_estab	= a.nr_sequencia
		and	nvl(c.cd_procedimento,cd_procedimento_w)	= cd_procedimento_w
		and	nvl(c.ie_origem_proced,ie_origem_proced_w)	= ie_origem_proced_w
		and     nvl(c.cd_grupo_proc,cd_grupo_w)                 = cd_grupo_w
		and     nvl(c.cd_especialidade, cd_especialidade_w)     = cd_especialidade_w
		and     nvl(c.cd_area_procedimento, cd_area_w)          = cd_area_w
		and	nvl(c.vl_liberado,0) <= nvl(vl_procedimento_w,0)
		and	b.ie_situacao = 'A'
		and	a.ie_situacao = 'A'
		and	b.nr_seq_contrato = nr_seq_contrato_w
		and 	(nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento and pls_obter_se_controle_estab('RE') = 'S')
		union all
		select	c.ie_liberado ie_liberado,
			b.nr_sequencia nr_sequencia,
			c.cd_area_procedimento cd_area_procedimento,
			c.cd_especialidade cd_especialidade,
			c.cd_grupo_proc cd_grupo_proc,
			c.cd_procedimento cd_procedimento
		from	pls_tipo_pos_estab_proc		c,
			pls_pos_estab_aut_previa	b,
			pls_tipo_pos_estabelecido	a
		where	c.nr_seq_tipo_pos_estab = a.nr_sequencia
		and	b.nr_seq_tipo_pos_estab	= a.nr_sequencia
		and	nvl(c.cd_procedimento,cd_procedimento_w)	= cd_procedimento_w
		and	nvl(c.ie_origem_proced,ie_origem_proced_w)	= ie_origem_proced_w
		and     nvl(c.cd_grupo_proc,cd_grupo_w)                 = cd_grupo_w
		and     nvl(c.cd_especialidade, cd_especialidade_w)     = cd_especialidade_w
		and     nvl(c.cd_area_procedimento, cd_area_w)          = cd_area_w
		and	nvl(c.vl_liberado,0) <= nvl(vl_procedimento_w,0)
		and	b.ie_situacao = 'A'
		and	a.ie_situacao = 'A'
		and	b.nr_seq_contrato = nr_seq_contrato_w
		and 	pls_obter_se_controle_estab('RE') = 'N')
	order by cd_area_procedimento,
                cd_especialidade,
                cd_grupo_proc,
                cd_procedimento;

begin

if	(nvl(nr_seq_guia_p,0) <> 0) then
	select	b.nr_seq_contrato,
		b.nr_seq_plano	
	into	nr_seq_contrato_w,
		nr_seq_plano_w
	from	pls_segurado	b,
		pls_guia_plano	a
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_guia_p;

	open C01;
	loop
	fetch C01 into	
		cd_procedimento_w,
		ie_origem_proced_w,
		vl_procedimento_w;
	exit when C01%notfound;
		begin	
		pls_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, cd_area_w, cd_especialidade_w, cd_grupo_w, ie_origem_proced_w);	
		open C03;
		loop
		fetch C03 into	
			ie_liberado_w,
			nr_seq_regra_pos_estab_w;
		exit when C03%notfound;
		end loop;
		close C03;	
		if	(ie_liberado_w = 'N') then
			ds_retorno_w := 'N';
		end if;	
		end;
	end loop;
	close C01;
elsif	(nvl(nr_seq_requisicao_p,0) <> 0) then
	select	b.nr_seq_contrato,
		b.nr_seq_plano	
	into	nr_seq_contrato_w,
		nr_seq_plano_w
	from	pls_segurado	b,
		pls_requisicao	a
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_requisicao_p;
	
	open C02;
	loop
	fetch C02 into	
		cd_procedimento_w,
		ie_origem_proced_w;
	exit when C02%notfound;
		begin	
		pls_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, cd_area_w, cd_especialidade_w, cd_grupo_w, ie_origem_proced_w);
		vl_procedimento_w := 0;
		open C03;
		loop
		fetch C03 into	
			ie_liberado_w,
			nr_seq_regra_pos_estab_w;
		exit when C03%notfound;
		end loop;
		close C03;	
		if	(ie_liberado_w = 'N') then
			ds_retorno_w := 'N';
		end if;	
		end;
	end loop;
	close C02;
end if;

return ds_retorno_w;

end pls_obter_se_lib_contratante;
/ 
