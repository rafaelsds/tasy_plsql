CREATE OR REPLACE FUNCTION obter_atend_agend_consulta (cd_medico_p number) RETURN NUMBER AS

nr_atendimento_w  number(10) := 0;

BEGIN
select *
into nr_atendimento_w
from (
  select    a.nr_atendimento
  from      agenda_consulta a, agenda_classif b, agenda c
  where     a.IE_CLASSIF_AGENDA = b.CD_CLASSIFICACAO
  and       a.ie_status_agenda = 'O'
  and       b.IE_UTILIZA_TELEMEDICINA = 'S'
  and       a.cd_agenda = c.CD_AGENDA
  and       c.CD_PESSOA_FISICA = cd_medico_p -- param
  and		a.dt_consulta >= sysdate - 1
  order by  a.dt_consulta desc)
where rownum = 1;
  
return nr_atendimento_w;


END obter_atend_agend_consulta;
/
