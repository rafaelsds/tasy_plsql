create or replace
function pda_consiste_impressora (nr_seq_impressora_p	number)
 		    	return varchar2 is

ds_erro_w		varchar2(255) := '';
ds_endereco_rede_w     	Varchar2(255);
ds_endereco_ip_w		Varchar2(255);

begin

select 	max(ds_endereco_rede),
	max(ds_endereco_ip)
into	ds_endereco_rede_w,
	ds_endereco_ip_w
from	impressora
where	nr_sequencia = nr_seq_impressora_p;

if	(ds_endereco_rede_w is null or ds_endereco_ip_w is null) then
	ds_erro_w := wheb_mensagem_pck.get_texto(309618); -- Impressora n�o cadastrada
end if;

return	ds_erro_w;

end pda_consiste_impressora;
/