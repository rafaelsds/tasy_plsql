create or replace
FUNCTION CTB_Obter_Centro_Contido_orc(
				cd_centro_custo_p		Varchar2,
				cd_centro_parametro_p	Varchar2)
                        return varchar2 is

cd_centro_custo_ww		Varchar2(2000);
ie_gerar_w			Varchar2(0001);
cd_centro_regra_w		number(08,0);

cursor c01 is
	select	distinct
		cd_centro_destino
	from	centro_custo_orc_estab
	where	substr(ctb_obter_se_centro_contido(cd_centro_custo, cd_centro_custo_p),1,1) = 'S';

BEGIN


if	(nvl(cd_centro_custo_p, '0') <> '0') then
	open C01;
	loop
	fetch C01 into	
		cd_centro_regra_w;
	exit when C01%notfound;
		begin
		cd_centro_custo_ww := cd_centro_custo_ww || cd_centro_regra_w || ',';
		end;
	end loop;
	close C01;
end if;

cd_centro_custo_ww              := ' ' || cd_centro_parametro_p || ' ';
cd_centro_custo_ww              := replace(cd_centro_custo_ww, ',', ' , ');
cd_centro_custo_ww              := replace(cd_centro_custo_ww, '(', ' ');
cd_centro_custo_ww              := replace(cd_centro_custo_ww, ')', ' ');
cd_centro_custo_ww              := replace(cd_centro_custo_ww, '  ', ' ');

select  nvl(max('S') , 'N')
into    ie_gerar_w
from    dual
where   cd_centro_custo_ww like '% ' || cd_centro_custo_p || ' %';

return  ie_gerar_w;

END CTB_Obter_Centro_Contido_orc;
/

