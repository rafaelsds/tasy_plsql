create or replace
function qt_obter_tempo_dia(
			nr_seq_atendimento_p	number)
 		    	return number is

qt_hora_aplicacao_w	number(3);
qt_tempo_aplic_soluc_w	number(15,4);
qt_min_aplicacao_w	number(4);

posicao_w		number(10);
ds_retorno_w		number(10);
qt_duracao_w		number(10);
ds_dia_ciclo_w		varchar2(5);
			
begin

select	sum(nvl(qt_hora_aplicacao,0)),
	sum(nvl(qt_min_aplicacao,0))
into	qt_hora_aplicacao_w,
	qt_min_aplicacao_w
from	paciente_atend_medic
where	nr_seq_atendimento	= nr_seq_atendimento_p
and	nr_seq_diluicao is null
and	nr_seq_solucao is null
order by 1;

select	sum(nvl(qt_tempo_aplicacao,0))
into	qt_tempo_aplic_soluc_w
from	paciente_atend_soluc
where	nr_seq_atendimento	= nr_seq_atendimento_p;

qt_duracao_w	:= qt_min_aplicacao_w + (nvl(qt_hora_aplicacao_w,0) * 60) + (nvl(qt_tempo_aplic_soluc_w,0) * 60);

ds_retorno_w	:= qt_duracao_w;

return	ds_retorno_w;

end qt_obter_tempo_dia;
/
