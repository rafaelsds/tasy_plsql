create or replace
FUNCTION obter_desc_prot_medic(nr_seq_paciente_p Number)
   		return varchar2 is
ds_retorno_w  	Varchar2(500);
ds_protocolo_w 	 Varchar2(500);
ds_medic_w  	Varchar2(500);
BEGIN
if (nr_seq_paciente_p is not null) then
 		select 	nvl(max(obter_desc_protocolo(cd_protocolo)),' '),
  			nvl(max(obter_desc_protocolo_medic(nr_seq_medicacao, cd_protocolo)),' ')
 		into 	ds_protocolo_w,
 		 	ds_medic_w
		 from	 paciente_setor
 		where	 nr_seq_paciente = nr_seq_paciente_p;
 		if 	(ds_protocolo_w <> ' ') or (ds_medic_w <> ' ') then
  			ds_retorno_w := ds_protocolo_w ||' / '||ds_medic_w;
 		end if;
end if;
return ds_retorno_w;
END obter_desc_prot_medic;
/
