CREATE OR REPLACE
FUNCTION Ageint_Obter_Qt_Dia_Util_Per	(dt_inicial_p		date,
					dt_final_p		date,
					cd_estabelecimento_p	number)
					RETURN NUMBER IS

dt_inicial_w	Date;
dt_final_w	Date;
ie_dia_util_w	Varchar2(1);
qt_dia_util_w	Number(10,0)	:= 0;

BEGIN
if	(dt_inicial_p is not null) and
	(dt_final_p is not null) then
	dt_inicial_w	:= trunc(dt_inicial_p);
	dt_final_w	:= trunc(dt_final_p);

	while	(dt_inicial_w <= dt_final_w) loop
		begin
		ie_dia_util_w	:= nvl(Ageint_Obter_Se_Dia_Util(dt_inicial_w, cd_estabelecimento_p), 'S');

		if	(nvl(ie_dia_util_w, 'S') = 'S') then
			qt_dia_util_w	:= qt_dia_util_w + 1;
		end if;

		dt_inicial_w	:= dt_inicial_w + 1;
		end;
	end loop;
end if;
RETURN	qt_dia_util_w;
END	Ageint_Obter_Qt_Dia_Util_Per;
/