create or replace
function same_obter_desc_tipo_hist(nr_seq_tipo_hist_p number)
		 return varchar2 is
ds_retorno_w varchar2(255) := null;				
begin
if (nr_seq_tipo_hist_p is not null) then
	select 	ds_tipo_historico
	into	ds_retorno_w
	from   	SAME_TIPO_HISTORICO_SOLIC
	where	nr_sequencia = nr_seq_tipo_hist_p;	
	return ds_retorno_w;
end if;
return	ds_retorno_w;
end same_obter_desc_tipo_hist;
/