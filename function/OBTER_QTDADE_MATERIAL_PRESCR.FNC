create or replace
function obter_qtdade_material_prescr(	nr_prescricao_p		number,
						cd_material_p		number)
						return number is

qt_material_w	number(15,3);
						
begin

select	sum(qt_material)
into	qt_material_w
from	prescr_material
where	nr_prescricao	=	nr_prescricao_p
and	cd_material	=	cd_material_p;

return	qt_material_w;

end obter_qtdade_material_prescr;
/