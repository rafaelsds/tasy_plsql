create or replace
function obter_tipo_isolamento_cid
				(cd_doenca_cid_p varchar2,
				ie_opcao_p	 number,
				ie_retorno_p	 varchar2 default 'TO') 
				 return varchar2 is

/*
ie_retorno_p
'TI' - Tipo isolamento.
'TO' - Tipo isolamento + Observação
'SO' - Somente observação
*/				 
				 
ds_resultado_w		varchar2(4000);
ds_tipo_isolamento_w	varchar2(80);
ds_observacao_w		varchar2(2000);
ie_quebra_w		varchar2(1);



Cursor c01 is
	select	distinct
		ds_tipo_isolamento,
		ds_observacao
	from	cih_doenca_infecto a,
		cih_tipo_isolamento b
	where	a.nr_seq_tipo_isolamento = b.nr_sequencia
	and	cd_doenca_cid	= cd_doenca_cid_p;

begin
	
ie_quebra_w	:= 'N';
OPEN C01;
LOOP
FETCH C01 into
		ds_tipo_isolamento_w,
		ds_observacao_w;
exit when c01%notfound;

	if 	(ie_opcao_p = 0) then
		if	(ie_quebra_w = 'S') then
			ds_resultado_w := ds_resultado_w || ' - ';
		end if;
	else
		if	(ie_quebra_w = 'S') then
			ds_resultado_w := ds_resultado_w || chr(10);
		end if;
	end if;
	
	If	(ie_retorno_p = 'TI') then
		ds_resultado_w := ds_resultado_w || ds_tipo_isolamento_w;
	Elsif	(ie_retorno_p = 'TO') then
		ds_resultado_w := ds_resultado_w || ds_tipo_isolamento_w || ' (' || ds_observacao_w || ') ';
	Elsif	(ie_retorno_p = 'SO') then
		ds_resultado_w := ds_observacao_w;
	Else
		ds_resultado_w := ds_resultado_w || ds_tipo_isolamento_w || ' (' || ds_observacao_w || ') ';
	End if;

	ie_quebra_w := 'S';

END LOOP;
CLOSE C01;

return ds_resultado_w;
end;
/