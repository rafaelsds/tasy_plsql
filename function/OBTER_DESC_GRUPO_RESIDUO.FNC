create or replace
function obter_desc_grupo_residuo(	nr_sequencia_p		number)
				return varchar is

ds_retorno_w	grupo_residuo.ds_grupo%type;
				
				
begin

if	(nr_sequencia_p is not null) then
	select	max(a.ds_grupo) ds_grupo
	into	ds_retorno_w
	from	grupo_residuo a
	where	a.nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end obter_desc_grupo_residuo;
/