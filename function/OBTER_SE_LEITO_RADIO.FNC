create or replace
function obter_se_leito_radio(cd_setor_atendimento_p		number,
			      cd_unidade_basica_p		varchar2,
			      cd_unidade_compl_p	        varchar2)
 		    	return varchar2 is

ie_radio_w 	varchar2(1);

begin

	select ie_radioterapia
	into   ie_radio_w
	from   unidade_atendimento
	where  cd_setor_atendimento = cd_setor_atendimento_p
	and    cd_unidade_basica = cd_unidade_basica_p
	and    cd_unidade_compl = cd_unidade_compl_p;

return	ie_radio_w;

end obter_se_leito_radio;
/
