CREATE OR REPLACE
FUNCTION Sus_Consiste_Data_Laudo
			(	cd_pessoa_fisica_p	Varchar)
				return Varchar2 is

qt_reg_laudo_w	Number(5)	:= 0;
ds_retorno_w	Varchar2(1)	:= 'N';

BEGIN

select	count(*)
into	qt_reg_laudo_w
from	atendimento_paciente		b,
	sus_laudo_paciente		a
where	a.nr_atendimento		= b.nr_atendimento
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_p
and	a.ie_tipo_laudo_apac		= 1
and	to_char(dt_emissao,'mm/yyyy')	= to_char(sysdate,'mm/yyyy');

if	(qt_reg_laudo_w	> 0) then
	ds_retorno_w	:= 'S';	
end if;

return	ds_retorno_w;

END Sus_Consiste_Data_Laudo;
/