create or replace
function pls_obter_ds_indic_clinica(
		ds_parametros_p		varchar2,
		nr_seq_atendimento_p	number,
		cd_doenca_p		number,
		cd_estabelecimento_p	number)
 		    	return varchar2 is

ds_indicacao_clinica_w	varchar2(4000);
ie_existe_param_atend_w	varchar2(2);
			
begin
if	(ds_parametros_p is not null) and
	(nr_seq_atendimento_p is not null) and
	(cd_doenca_p is not null) and
	(cd_estabelecimento_p is not null) then
	begin
	
	select	pls_obter_se_item_lista(ds_parametros_p, lpad(pls_obter_cd_tipo_atend_tiss(nr_seq_atendimento_p, cd_estabelecimento_p),2,0))
	into	ie_existe_param_atend_w
	from	dual;
	
	if	(ie_existe_param_atend_w = 'S') then
		begin
		
		select	substr(obter_desc_cid(cd_doenca_p),1,255)
		into	ds_indicacao_clinica_w
		from	dual;
		
		end;
	end if;
	end;
end if;

return	ds_indicacao_clinica_w;

end pls_obter_ds_indic_clinica;
/