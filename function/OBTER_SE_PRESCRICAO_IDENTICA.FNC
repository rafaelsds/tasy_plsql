create or replace
function Obter_se_prescricao_identica(	nr_prescricao_ant_p		number,
										nr_prescricao_nov_p		number)
				return char is

ie_identica_w				char(1);
qt_nova_w					number(10);
qt_ante_w					number(10);
cd_material_w				material.cd_material%type;
qt_dose_w					prescr_material.qt_dose%type;
cd_unidade_medida_dose_w	prescr_material.cd_unidade_medida_dose%type;
ie_via_aplicacao_w			prescr_material.ie_via_aplicacao%type;
cd_intervalo_w				prescr_material.cd_intervalo%type;
ie_agrupador_w				prescr_material.ie_agrupador%type;
hr_prim_horario_w			prescr_material.hr_prim_horario%type;
ie_se_necessario_w			prescr_material.ie_se_necessario%type;
ie_acm_w					prescr_material.ie_acm%type;
ie_urgencia_w				prescr_material.ie_urgencia%type;
ie_dose_espec_agora_w		prescr_material.ie_dose_espec_agora%type;
hr_dose_especial_w			prescr_material.hr_dose_especial%type;
qt_dose_especial_w			prescr_material.qt_dose_especial%type;
nr_ocorrencia_w				prescr_material.nr_ocorrencia%type;
nr_sequencia_w				prescr_material.nr_sequencia%type;
ds_dose_diferenciada_w		prescr_material.ds_dose_diferenciada%type;

cursor c01 is
select	a.cd_material,
		a.qt_dose,
		coalesce(a.cd_unidade_medida_dose,'XPTO'),
		coalesce(a.ie_via_aplicacao,'XPTO'),
		coalesce(a.cd_intervalo,'XPTO'),
		a.ie_agrupador,
		coalesce(a.hr_prim_horario,'99:99'),
		coalesce(a.ie_se_necessario,'N'),
		coalesce(a.ie_acm,'N'),
		coalesce(a.ie_urgencia,'N'),
		coalesce(a.ds_dose_diferenciada,'XPTO'),
		coalesce(a.ie_dose_espec_agora,'N'),
		coalesce(a.hr_dose_especial,'99:99'),
		coalesce(qt_dose_especial,-1),
		coalesce(a.nr_ocorrencia,-1),
		a.nr_sequencia
from   	prescr_material a
where  	a.nr_prescricao = nr_prescricao_ant_p;

begin

select	count(*)
into	qt_ante_w
from	prescr_material 
where	nr_prescricao = nr_prescricao_ant_p;

select	count(*)
into	qt_nova_w
from	prescr_material 
where	nr_prescricao = nr_prescricao_nov_p;

if	(qt_ante_w = qt_nova_w) then
	select	coalesce(max('N'),'S')
	into	ie_identica_w
	from   	prescr_material a
	where	rownum = 1
	and		not exists(	select	1
						from	prescr_material b
						where	b.nr_prescricao = nr_prescricao_ant_p
						and		b.cd_material = a.cd_material)
	and		a.nr_prescricao = nr_prescricao_nov_p;
	
	if	(ie_identica_w = 'S') then
		open C01;
		loop
		fetch C01 into	
			cd_material_w,
			qt_dose_w,
			cd_unidade_medida_dose_w,
			ie_via_aplicacao_w,
			cd_intervalo_w,
			ie_agrupador_w,
			hr_prim_horario_w,
			ie_se_necessario_w,
			ie_acm_w,
			ie_urgencia_w,
			ds_dose_diferenciada_w,
			ie_dose_espec_agora_w,
			hr_dose_especial_w,
			qt_dose_especial_w,
			nr_ocorrencia_w,
			nr_sequencia_w;
		exit when C01%notfound;
			select	coalesce(max('S'),'N')
			into	ie_identica_w
			from   	prescr_material b
			where  	rownum = 1
			and		b.nr_prescricao = nr_prescricao_nov_p
			and		b.cd_material = cd_material_w
			and		b.qt_dose = qt_dose_w
			and		b.cd_unidade_medida_dose = cd_unidade_medida_dose_w
			and		coalesce(b.ie_via_aplicacao,ie_via_aplicacao_w) = ie_via_aplicacao_w
			and 	coalesce(b.cd_intervalo,cd_intervalo_w) = cd_intervalo_w
			and		b.ie_agrupador = ie_agrupador_w
			and		coalesce(b.ie_se_necessario,'N') = ie_se_necessario_w
			and		coalesce(b.ie_acm,'N') = ie_acm_w
			and		coalesce(b.ie_urgencia,'N') = ie_urgencia_w
			and		coalesce(ds_dose_diferenciada,ds_dose_diferenciada_w) = ds_dose_diferenciada_w
			and		coalesce(b.nr_sequencia_anterior,nr_sequencia_w) = nr_sequencia_w
			and		coalesce(b.nr_prescricao_anterior,b.nr_prescricao_original,nr_prescricao_ant_p) = nr_prescricao_ant_p;
			
			if	(ie_identica_w = 'N') then
				return 'N';
			end if;
		end loop;
		close C01;
	
		return 'S';
	end if;
	
	return 'N';
end if;

return 'N';

end Obter_se_prescricao_identica;
/