create or replace
function Obter_Se_Dia_Ciclo_Autorizado	(	nr_seq_paciente_setor_p	number,
						nr_ciclo_p		number)
 		    	return varchar2 is
NR_SEQ_ESTAGIO_w	number(10);
ds_retorno_w		varchar2(10)	:= 'N';
ie_interno_w		varchar2(10);
nr_seq_paciente_w	number(10,0);
begin

select	max(nr_seq_paciente)
into	nr_seq_paciente_w
from	paciente_atendimento
where	nr_seq_atendimento = nr_seq_paciente_setor_p;


select	max(NR_SEQ_ESTAGIO)
into	NR_SEQ_ESTAGIO_w
from	autorizacao_convenio
where	nr_seq_paciente  = nr_seq_paciente_setor_p;

if	(nvl(NR_SEQ_ESTAGIO_w,0) = 0) and
	(nvl(nr_seq_paciente_w,0) > 0)then

	select	max(NR_SEQ_ESTAGIO)
	into	NR_SEQ_ESTAGIO_w
	from	autorizacao_convenio
	where	nr_seq_paciente_setor  = nr_seq_paciente_w
	and	NR_CICLO		= nr_ciclo_p;

end if;


if	(NR_SEQ_ESTAGIO_w	is not null) then
	select	max(IE_INTERNO)
	into	ie_interno_w
	from	ESTAGIO_AUTORIZACAO
	where	nr_sequencia	= NR_SEQ_ESTAGIO_w
	and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
	
	if	(ie_interno_w	= '10') then
		ds_retorno_w	:= 'S';
	end if;
end if;


return	ds_retorno_w;

end Obter_Se_Dia_Ciclo_Autorizado;
/
