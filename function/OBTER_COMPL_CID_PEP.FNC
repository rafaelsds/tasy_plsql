create or replace
function Obter_compl_CID_PEP(	ds_diagnostico_p	varchar2,
				ie_classificacao_doenca_p	varchar2,
				ie_tipo_doenca_p		varchar2,
				ie_unidade_tempo_p		varchar2,
				qt_tempo_p			number,
				ie_lado_p			varchar2,
				dt_manifestacao_p		date)
 		    	return varchar2 is

ds_retorno_w			varchar2(10000);
ds_diagnostico_w		varchar2(2000);
ie_classificacao_doenca_w	varchar2(60);
ie_tipo_doenca_w		varchar2(60);
ie_unidade_tempo_w		varchar2(10);
qt_tempo_w			number(15);
ie_lado_w			varchar2(60);
dt_manifestacao_w		date;

begin



ds_diagnostico_w		:= ds_diagnostico_p;	
ie_classificacao_doenca_w	:= substr(obter_valor_dominio(58,ie_classificacao_doenca_p),1,60);
ie_tipo_doenca_w		:= substr(obter_valor_dominio(1758,ie_tipo_doenca_p),1,60);
ie_unidade_tempo_w		:= ie_unidade_tempo_p;		
qt_tempo_w			:= qt_tempo_p;
ie_lado_w			:= substr(obter_valor_dominio(1372,ie_lado_p),1,60);
dt_manifestacao_w		:= dt_manifestacao_p;


if	(dt_manifestacao_w is not null) then
	begin
	ds_retorno_w	:= Wheb_mensagem_pck.get_texto(308867) || ': ' /*'Manifestação: '*/||to_char(dt_manifestacao_w,'dd/mm/yyyy') ||chr(13) ||chr(10);
	end;
end if;

if	(ie_classificacao_doenca_w is not null) then
	begin
	ds_retorno_w	:= ds_retorno_w || Wheb_mensagem_pck.get_texto(308868) || ': ' /*'Classificação: '*/||ie_classificacao_doenca_w ||chr(13) ||chr(10);
	end;
end if;

if	(ie_tipo_doenca_w is not null) then
	begin
	ds_retorno_w	:= ds_retorno_w || Wheb_mensagem_pck.get_texto(308869) || ': ' /*'Tipo: '*/||ie_tipo_doenca_w ||chr(13) ||chr(10);
	end;
end if;

if	(ie_unidade_tempo_w is not null) and
	(nvl(qt_tempo_w,0) > 0) then
	begin
	ds_retorno_w	:= ds_retorno_w || Wheb_mensagem_pck.get_texto(308870) || ': ' /*'Tempo doença: '*/||to_char(qt_tempo_w) || ' '|| ie_unidade_tempo_w ||chr(13) ||chr(10);
	end;
end if;

if	(ie_lado_w is not null) then
	begin
	ds_retorno_w	:= ds_retorno_w || Wheb_mensagem_pck.get_texto(308871) || ': ' /*'Lado: '*/||ie_lado_w ||chr(13) ||chr(10);
	end;
end if;

if	(ds_diagnostico_w is not null) then
	begin
	ds_retorno_w	:= ds_retorno_w || Wheb_mensagem_pck.get_texto(308872) || ': ' /*'Observação: '*/||ds_diagnostico_w ||chr(13) ||chr(10);
	end;
end if;

return	substr(ds_retorno_w,1,2000);

end Obter_compl_CID_PEP;
/
