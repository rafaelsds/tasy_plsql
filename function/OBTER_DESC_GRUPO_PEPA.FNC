create or replace FUNCTION Obter_desc_grupo_pepa(
			nr_seq_grupo_p	number)
			return varchar2 is

ds_retorno_w	varchar2(255);

BEGIN

select substr(nvl(max(DS_GRUPO),''),1,255) 
into ds_retorno_w
from EHR_ITEM_GRUPO 
where nr_sequencia = nr_seq_grupo_p;

return	ds_retorno_w;

END Obter_desc_grupo_pepa;
/