create or replace
function obter_cd_setor_prescr_proc(nr_prescricao_p	number,
									nr_seq_prescr_p number)
 		    	return number is	
				
cd_setor_atendimento_w number(5);

begin

if (nr_prescricao_p is not null) and	
   (nr_seq_prescr_p is not null) then

	select	nvl(max(a.cd_setor_atendimento),0)
	into 	cd_setor_atendimento_w
	from    prescr_procedimento a
	where	a.nr_prescricao	= nr_prescricao_p
	and		a.nr_sequencia	= nr_seq_prescr_p;	
		
end if;   

return	cd_setor_atendimento_w;

end obter_cd_setor_prescr_proc;
/