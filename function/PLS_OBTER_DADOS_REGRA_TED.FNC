create or replace
function pls_obter_dados_regra_ted(	nr_seq_regra_p	number,
					ie_opcao_p	varchar2)
					return varchar2 is

ds_retorno_w	varchar2(255);
					
begin
if	(upper(ie_opcao_p) = 'NM') then
	select	max(a.nm_regra)
	into	ds_retorno_w
	from	pls_regra_ted	a
	where	a.nr_sequencia = nr_seq_regra_p;
end if;

return	ds_retorno_w;

end pls_obter_dados_regra_ted;
/