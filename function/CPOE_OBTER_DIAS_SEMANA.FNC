create or replace function CPOE_OBTER_DIAS_SEMANA(nr_seq_cpoe_p number,
                                                  ie_tipo_item_p varchar2
                                                  ) return varchar2 is

ie_segunda_w   varchar2(100);
ie_terca_w     varchar2(100);
ie_quarta_w    varchar2(100);
ie_quinta_w    varchar2(100);
ie_sexta_w     varchar2(100);
ie_sabado_w    varchar2(100);
ie_domingo_w   varchar2(100);

ds_dia_semana_w    varchar2(2000);
ds_retorno_html_w  varchar2(4000);

begin

    if (ie_tipo_item_p in ('M', 'SOL')) then    
        select decode(ie_segunda, 'S', obter_desc_expressao(298106) || '<br>', ''),
               decode(ie_terca, 'S', obter_desc_expressao(299302) || '<br>', ''),
               decode(ie_quarta, 'S', obter_desc_expressao(297139) || '<br>', ''),
               decode(ie_quinta, 'S', obter_desc_expressao(297214) || '<br>', ''),
               decode(ie_sexta, 'S', obter_desc_expressao(298488) || '<br>', ''),
               decode(ie_sabado, 'S', obter_desc_expressao(297960) || '<br>', ''),
               decode(ie_domingo, 'S', obter_desc_expressao(288200) || '<br>', '')
          into ie_segunda_w,
               ie_terca_w,
               ie_quarta_w,
               ie_quinta_w,
               ie_sexta_w,
               ie_sabado_w,
               ie_domingo_w
          from cpoe_material
        where nr_sequencia = nr_seq_cpoe_p;
		
	elsif (ie_tipo_item_p in ('DI', 'DP')) then
		select decode(ie_segunda, 'S', obter_desc_expressao(298106) || '<br>', ''),
			   decode(ie_terca, 'S', obter_desc_expressao(299302) || '<br>', ''),
			   decode(ie_quarta, 'S', obter_desc_expressao(297139) || '<br>', ''),
			   decode(ie_quinta, 'S', obter_desc_expressao(297214) || '<br>', ''),
			   decode(ie_sexta, 'S', obter_desc_expressao(298488) || '<br>', ''),
			   decode(ie_sabado, 'S', obter_desc_expressao(297960) || '<br>', ''),
			   decode(ie_domingo, 'S', obter_desc_expressao(288200) || '<br>', '')
			into ie_segunda_w,
			   ie_terca_w,
			   ie_quarta_w,
			   ie_quinta_w,
			   ie_sexta_w,
			   ie_sabado_w,
			   ie_domingo_w
			from cpoe_dialise
			where nr_sequencia = nr_seq_cpoe_p;
	elsif (ie_tipo_item_p in ('P')) then
		select decode(ie_segunda, 'S', obter_desc_expressao(298106) || '<br>', ''),
			   decode(ie_terca, 'S', obter_desc_expressao(299302) || '<br>', ''),
			   decode(ie_quarta, 'S', obter_desc_expressao(297139) || '<br>', ''),
			   decode(ie_quinta, 'S', obter_desc_expressao(297214) || '<br>', ''),
			   decode(ie_sexta, 'S', obter_desc_expressao(298488) || '<br>', ''),
			   decode(ie_sabado, 'S', obter_desc_expressao(297960) || '<br>', ''),
			   decode(ie_domingo, 'S', obter_desc_expressao(288200) || '<br>', '')
			into ie_segunda_w,
			   ie_terca_w,
			   ie_quarta_w,
			   ie_quinta_w,
			   ie_sexta_w,
			   ie_sabado_w,
			   ie_domingo_w
			from cpoe_procedimento
			where nr_sequencia = nr_seq_cpoe_p;
    end if;
    
    ds_dia_semana_w := ie_segunda_w || ie_terca_w || ie_quarta_w || ie_quinta_w || ie_sexta_w || ie_sabado_w || ie_domingo_w;
        
    if (ds_dia_semana_w is not null) then
        ds_retorno_html_w := '<div>' || replace(obter_desc_expressao(1040439), '#@DIAS_SEMANA#@', '<br>' || ds_dia_semana_w) || '</div>';
    end if;
    
    return ds_retorno_html_w;

end CPOE_OBTER_DIAS_SEMANA;
/
