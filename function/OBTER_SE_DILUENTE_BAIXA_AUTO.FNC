create or replace
function obter_se_diluente_baixa_auto(	cd_material_p		number,
				cd_diluente_p		number)
 		    	return varchar2 is

cd_motivo_w		number(3);
ie_baixa_w		varchar2(1) := 'N';			
			
begin

select	nvl(max(cd_motivo_baixa),0)
into	cd_motivo_w
from	material_diluicao
where	cd_material = cd_material_p
and	cd_diluente = cd_diluente_p;

if	(cd_motivo_w <> 0) then
	ie_baixa_w := 'S';
end if;

return	ie_baixa_w;

end obter_se_diluente_baixa_auto;
/