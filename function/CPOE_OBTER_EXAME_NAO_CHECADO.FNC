-- Procedure desenvolvida pelo usu�rio ljcmelo na OS 1709911. Somente documentamos na OS 2035406 devido a falha gerada por deixar de documentar essa function para as vers�es.

create or replace 
function cpoe_obter_exame_nao_checado  (nr_atendimento_p    number,
                                        cd_pessoa_fisica_p  varchar2) return varchar2 is

c01_w                   cpoe_procedimento%rowtype;
nr_seq_proc_cpoe_w      cpoe_procedimento.nr_sequencia%type;
ie_exame_nao_checado_w  varchar2(1 char);

cursor  C01 is
select	nr_sequencia
from	cpoe_procedimento
where	((nr_atendimento = nr_atendimento_p) or
        (cd_pessoa_fisica = cd_pessoa_fisica_p and nr_atendimento is null))
and     dt_liberacao is not null
and	    ((cpoe_obter_dt_suspensao(nr_sequencia,'N') is null) or
        (cpoe_obter_dt_suspensao(nr_sequencia,'N') >= sysdate))
and	    (dt_fim is null or to_char(dt_fim, 'dd/mm/yyyy') >= to_char(sysdate, 'dd/mm/yyyy'))
and	    to_char(dt_inicio, 'dd/mm/yyyy') <= to_char(sysdate, 'dd/mm/yyyy');

cursor  C02 is
select  b.nr_prescricao,
        b.nr_sequencia
from    prescr_procedimento b,
        prescr_proc_hor c
where   b.nr_seq_proc_cpoe = nr_seq_proc_cpoe_w
and     b.nr_sequencia = c.nr_seq_procedimento
and     b.nr_prescricao is not null
and     c.dt_lib_horario is not null
and     c.nr_prescricao = b.nr_prescricao;

begin

<<loop_items_cpoe>>
for c01_w in C01 loop
    nr_seq_proc_cpoe_w := c01_w.nr_sequencia;
    for c02_w in C02 loop 
        select  decode(count(0), 0, 'S', 'N')
        into    ie_exame_nao_checado_w
        from    prescr_mat_alteracao d
        where   d.nr_seq_procedimento = c02_w.nr_sequencia
        and     d.nr_prescricao = c02_w.nr_prescricao
        and     d.ie_alteracao = 3; 

        if (ie_exame_nao_checado_w = 'S') then 
            exit loop_items_cpoe;
        end if;
    end loop;
end loop loop_items_cpoe;

return nvl(ie_exame_nao_checado_w,'N');

end cpoe_obter_exame_nao_checado;
/