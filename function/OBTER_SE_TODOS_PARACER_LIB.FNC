create or replace
function	obter_se_todos_paracer_lib(
		nr_seq_lista_p		number)
		return varchar2 is

nr_seq_item_w		number(10);
qt_existe_w		number(10);
ie_retorno_w		varchar2(1);

cursor	c01 is
select	nr_sequencia
from	segmento_compras_rel_it
where	nr_seq_seg_rel = nr_seq_lista_p;

begin

ie_retorno_w	:= 'S';

open c01;
loop
fetch c01 into
	nr_seq_item_w;
exit when c01%notfound;
	begin

	if	(ie_retorno_w = 'S') then
		begin

		select	count(*)
		into	qt_existe_w
		from	segmento_compras_rel_parec
		where	nr_seq_item_rel = nr_seq_item_w
		and	dt_liberacao_parec is null;

		if	(qt_existe_w > 0) then
			ie_retorno_w	:= 'N';
		end if;

		end;
	end if;

	end;
end loop;
close c01;

return	ie_retorno_w;

end obter_se_todos_paracer_lib;
/