create or replace
function Obter_se_destaca_risco(cd_setor_atendimento_p		number)
 		    	return varchar2 is

ie_destacar_w	varchar2(1);
				
cursor c01 is
select	ie_destacar
from	adep_medic_risco
where	nvl(cd_setor_atendimento,nvl(cd_setor_atendimento_p,0)) = nvl(cd_setor_atendimento_p,0)
order by nvl(cd_setor_atendimento,0);
				
begin

open C01;
loop
fetch C01 into	
	ie_destacar_w;
exit when C01%notfound;
	ie_destacar_w	:= ie_destacar_w;
end loop;
close C01;

return	ie_destacar_w;

end Obter_se_destaca_risco;
/
