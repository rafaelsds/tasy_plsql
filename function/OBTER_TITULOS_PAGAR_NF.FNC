create or replace
function obter_titulos_pagar_nf(	nr_seq_nota_p 	number,
					ds_separador_p	varchar2)
					return varchar2 is

nr_titulo_pagar_w			number(10);
ds_titulo_pagar_w			varchar2(400);

cursor c01 is
select	nr_titulo
from	titulo_pagar
where	nr_seq_nota_fiscal = nr_seq_nota_p;

BEGIN

open c01;
loop
fetch c01 into
	nr_titulo_pagar_w;
exit when c01%notfound;
	ds_titulo_pagar_w	:= substr(ds_titulo_pagar_w || nr_titulo_pagar_w	 || ds_separador_p,1,400);
end loop;
close c01;

return ds_titulo_pagar_w;
END obter_titulos_pagar_nf;
/