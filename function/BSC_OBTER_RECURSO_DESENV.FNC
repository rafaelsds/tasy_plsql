create or replace
FUNCTION bsc_obter_recurso_desenv(	nm_usuario_p		VARCHAR2)
 		    	RETURN NUMBER IS

qt_retorno_w	NUMBER(10) := 0;

BEGIN
SELECT	COUNT(DISTINCT a.nm_usuario_grupo)
INTO	qt_retorno_w
FROM	usuario_grupo_des a
WHERE	a.ie_funcao_usuario IN ('P','M','S','N')	-- Programador / Analista de Manuten��o/ Analista de sistema e de neg�cio
AND	EXISTS(	SELECT	1
		FROM	grupo_desenvolvimento b,
			gerencia_wheb c
		WHERE	b.nr_sequencia = a.nr_seq_grupo
		AND	b.ie_situacao = 'A'
		AND	b.nr_seq_gerencia = c.nr_sequencia
		and	c.ie_area_gerencia IN ('DES') -- Ger�ncia desenvolvimento
		AND	c.ie_situacao = 'A');

RETURN	qt_retorno_w;

END bsc_obter_recurso_desenv;
/