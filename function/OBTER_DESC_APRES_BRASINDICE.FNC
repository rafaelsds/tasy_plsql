create or replace
function obter_desc_apres_brasindice(	cd_apresentacao_p		varchar2)
return varchar2 is

ds_apresentacao_w		varchar2(155) := '';

begin

if	(cd_apresentacao_p is not null) then
	select	ds_apresentacao
	into	ds_apresentacao_w
	from	brasindice_apresentacao
	where	cd_apresentacao = cd_apresentacao_p
	and	nvl(ie_situacao, 'A') = 'A';

	return ds_apresentacao_w;
end if;

end obter_desc_apres_brasindice;
/
