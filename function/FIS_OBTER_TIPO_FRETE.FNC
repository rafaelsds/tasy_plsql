create or replace
function fis_obter_tipo_frete(	ie_tipo_frete_p		varchar2)
								return varchar2 is
								
ds_tipo_frete_w	varchar2(40);

begin
/*
T - 'Por conta de terceiros'
C - 'Por conta do emitente'
F - 'Por conta do destinatário/remetente'
'Sem frete'
*/
select	substr(decode(ie_tipo_frete_p,'T',wheb_mensagem_pck.get_texto(310761) ,'C', wheb_mensagem_pck.get_texto(310762)	,'F', wheb_mensagem_pck.get_texto(310763) ,wheb_mensagem_pck.get_texto(310764)),1,255)
into	ds_tipo_frete_w
from	dual;

return	ds_tipo_frete_w;

end fis_obter_tipo_frete;
/
