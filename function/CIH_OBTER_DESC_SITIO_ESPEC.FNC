create or replace
function cih_obter_desc_sitio_espec(nr_seq_sitio_espec_p		number)
 		    	return varchar2 is
ds_retorno_w	varchar2(255);
begin
select	max(ds_sitio_especifico)
into	ds_retorno_w
from	niss_sitio_especifico
where	nr_sequencia = nr_seq_sitio_espec_p;

return	ds_retorno_w;

end cih_obter_desc_sitio_espec;
/