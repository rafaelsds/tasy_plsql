create or replace
function retorna_nivel_inicial_precad(ie_tipo_precadastro_p varchar,
									  cd_tipo_pessoa_p number,
									  nm_usuario_p varchar) return number is

retorno_w number(3) := null;

begin

	if (ie_tipo_precadastro_p != 'MAT') then
		select min(pa.nr_nivel) 
		into retorno_w
		from processo_avaliacao pa 
		where pa.CD_TIPO_PESSOA = cd_tipo_pessoa_p
		and   pa.IE_TIPO_PROCESSO = ie_tipo_precadastro_p
		and exists (select nr_sequencia 
						from USUARIO_PROCESSO_AVAL usu
						where pa.nr_sequencia = USU.NR_PROCESSO_AVAL);
	else
		select min(pa.nr_nivel) 
		into retorno_w
		from processo_avaliacao pa 
		where   pa.IE_TIPO_PROCESSO = ie_tipo_precadastro_p
		and exists (select nr_sequencia 
						from USUARIO_PROCESSO_AVAL usu
						where pa.nr_sequencia = USU.NR_PROCESSO_AVAL); 
	end if;

	
	return retorno_w;
		
end;
/