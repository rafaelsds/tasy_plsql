create or replace
function pls_desc_status_interno_prop
			(	nr_seq_status_interno_p	pls_status_interno_prop.nr_sequencia%type)
				return varchar2 is

ds_retorno_w		varchar2(255);

begin

select	max(ds_status)
into	ds_retorno_w
from	pls_status_interno_prop
where	nr_sequencia = nr_seq_status_interno_p;

return	ds_retorno_w;

end pls_desc_status_interno_prop;
/

