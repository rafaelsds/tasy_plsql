create or replace function obter_desc_escala_padua (qt_pontuacao_p number)
			      return varchar2 is

ds_retorno_w varchar2 (255);
sql_w        varchar2(300);
begin
  begin
      sql_w := 'CALL OBTER_DESC_ESC_PADUA_MD(:1) INTO :ds_retorno_w';
      execute immediate sql_w using in qt_pontuacao_p,
                                    out ds_retorno_w;
   
  exception
    when others then
      ds_retorno_w := null;
  end;

return ds_retorno_w;

end obter_desc_escala_padua;
/
