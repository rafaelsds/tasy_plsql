create or replace
function obter_dados_saida_paciente(	nr_seq_unidade_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

/*
A	-	N�mero de atendimento
P	-	Nome do paciente
*/
					
ds_retorno_w		varchar2(100) := null;
cd_setor_atendimento_w	number(5,0);
cd_unidade_basica_w	varchar2(10);
cd_unidade_compl_w      varchar2(10);
nr_atendimento_w	number(10,0);
dt_referencia_w		date	:= trunc(sysdate);
dt_refer_final_w	date;
					
begin
dt_refer_final_w	:= fim_dia(dt_referencia_w);

select	cd_setor_atendimento,
	cd_unidade_basica,
	cd_unidade_compl
into	cd_setor_atendimento_w,
	cd_unidade_basica_w,
	cd_unidade_compl_w
from	unidade_atendimento
where	nr_seq_interno = nr_seq_unidade_p;

select	nvl(max(a.nr_atendimento),0)
into	nr_atendimento_w
from	atend_paciente_unidade b,
	atendimento_paciente a
where 	a.nr_atendimento	= b.nr_atendimento
and   	a.dt_alta_interno between dt_referencia_w and dt_refer_final_w
and	a.dt_alta is not null
and	a.dt_saida_real is null
and	b.cd_setor_atendimento	=	cd_setor_atendimento_w
and	b.cd_unidade_basica	=	cd_unidade_basica_w
and	b.cd_unidade_compl	=	cd_unidade_compl_w;


if	(ie_opcao_p = 'A') and (nr_atendimento_w > 0) then
	ds_retorno_w	:= to_char(nr_atendimento_w);
elsif	(ie_opcao_p = 'P') and (nr_atendimento_w > 0) then
	begin
	select	substr(obter_nome_pf(cd_pessoa_fisica),1,60)
	into	ds_retorno_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;
	end;
end if;
	
return	ds_retorno_w;

end obter_dados_saida_paciente;
/
