create or replace
function obter_se_prof_liberado_escala	(cd_pessoa_fisica_p	varchar2,
					nr_seq_escala_p		number,	
					dt_escala_p		date)
					return varchar2 is

nr_seq_afastamento_prof_w	number(10);
ds_motivo_w			varchar2(40);
ds_retorno_w			varchar2(255);
dt_inicio_w			varchar2(20);
dt_final_w			varchar2(20);
ds_mascara_w			varchar2(100);

begin

select 	nvl(max(nr_sequencia),0)
into	nr_seq_afastamento_prof_w
from	escala_afastamento_prof a
where	a.cd_profissional = cd_pessoa_fisica_p
and	dt_escala_p between a.dt_inicio and a.dt_final
and	(a.nr_seq_escala = nr_seq_escala_p or a.nr_seq_escala is null);

ds_retorno_w	:= null;
ds_mascara_w 	:= pkg_date_formaters.localize_mask('timestamp', pkg_date_formaters.getUserLanguageTag(wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario));

if	(nr_seq_afastamento_prof_w > 0) then

	select 	substr(obter_desc_motivo_afast(NR_SEQ_MOTIVO_AFAST),1,40),
		to_char(dt_inicio, ds_mascara_w),
		to_char(dt_final, ds_mascara_w)
	into	ds_motivo_w,
		dt_inicio_w,
		dt_final_w
	from	escala_afastamento_prof a
	where	nr_sequencia  = nr_seq_afastamento_prof_w;

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(802896,
						'NM_PESSOA_FISICA='||substr(Obter_nome_pf(cd_pessoa_fisica_p),1,20)||
						';DT_INICIO='||dt_inicio_w||
						';DT_FIM='||dt_final_w||
						';DS_MOTIVO='||ds_motivo_w);
						
end if;

return ds_retorno_w;
end;
/
