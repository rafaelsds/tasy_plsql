create or replace function obter_pr_prev_etapa_proj( nr_seq_proj_p number,dt_ref_p date ) return number is
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter percentual previsto de execu��o de uma etapa do projeto
---------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [ ]  Relat�rios [ ] Outros: 
 --------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

pr_retorno_w		number(15,2) := 0;
hrs_prev_w		number(15,2);
dias_uteis_w		number(15,2);
dias_uteis_data_atual_w	number(15,2);
hrs_executadas_prev_w	number(15,2);
total_hrs_prev_w	number(15,2) := 0;
total_hrs_exec_prev_w	number(15,2) := 0;
dt_inicio_prev_w	date;


Cursor C01 is

select  e.qt_hora_prev hrs_previstas,
	obter_dias_uteis_periodo(e.dt_inicio_prev, e.dt_fim_prev, 1) dias_uteis,
        obter_dias_uteis_periodo(e.dt_inicio_prev, trunc(dt_ref_p), 1) dias_uteis_data_atual
from    proj_projeto p,
	proj_cronograma c, 
	proj_cron_etapa e
where	p.nr_sequencia = c.nr_seq_proj
and     c.nr_sequencia = e.nr_seq_cronograma
and     c.ie_situacao  = 'A'
and 	p.nr_sequencia = nr_seq_proj_p
and	e.ie_fase = 'N'
and	c.dt_aprovacao is not null
and  	not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  e.nr_sequencia)
order by c.ie_tipo_cronograma, e.dt_inicio_prev;

begin

	select  min(e.dt_inicio_prev)
	into	dt_inicio_prev_w
	from    proj_projeto p,
		proj_cronograma c, 
		proj_cron_etapa e
	where	p.nr_sequencia = c.nr_seq_proj
	and     c.nr_sequencia = e.nr_seq_cronograma
	and     c.ie_situacao  = 'A'
	and 	p.nr_sequencia = nr_seq_proj_p
	and	e.ie_fase = 'N'
	and	c.dt_aprovacao is not null
	and  	not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  e.nr_sequencia);


	if (dt_ref_p < dt_inicio_prev_w) then
		pr_retorno_w := 0;
		
	else
		open C01;
		loop
		fetch C01 into	
			hrs_prev_w,
			dias_uteis_w,
			dias_uteis_data_atual_w;
		exit when C01%notfound;
			begin

				if(dias_uteis_data_atual_w >= dias_uteis_w) then
					hrs_executadas_prev_w := hrs_prev_w;
				else
					hrs_executadas_prev_w := ((dias_uteis_data_atual_w / dias_uteis_w) * hrs_prev_w);
				end if;
				
				total_hrs_exec_prev_w := total_hrs_exec_prev_w + hrs_executadas_prev_w;
				total_hrs_prev_w := total_hrs_prev_w + hrs_prev_w;
			
			end;
		end loop;
		close C01;	

		if(total_hrs_exec_prev_w = 0 and total_hrs_prev_w = 0)then
			pr_retorno_w := 0;
		else
			pr_retorno_w := ((total_hrs_exec_prev_w / total_hrs_prev_w) * 100);
		end if;
	end if;
		
return  pr_retorno_w;

end obter_pr_prev_etapa_proj;
/