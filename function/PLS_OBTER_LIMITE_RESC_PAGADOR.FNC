create or replace
function pls_obter_limite_resc_pagador
			(	nr_seq_alteracao_motivo_p	number,
				nr_seq_segurado_p		number,
				dt_inicio_vigencia_p		date,
				qt_contribuicao_p		number)
 		    	return number is

qt_minima_meses_w	number(10);
qt_maximo_meses_w	number(10);
tx_fracao_w		number(3);
qt_meses_incluido_w	number(5);
qt_meses_lancados_w	number(10) := 0;
qt_tempo_operadora_w	number(10);

begin

begin
select	nvl(qt_minima_meses,0),
	nvl(qt_maximo_meses,0),
	nvl(tx_fracao,0),
	nvl(qt_tempo_operadora,0)
into	qt_minima_meses_w,
	qt_maximo_meses_w,
	tx_fracao_w,
	qt_tempo_operadora_w
from	pls_motivo_alt_pagador
where	nr_sequencia	= nr_seq_alteracao_motivo_p
and	ie_situacao	= 'A';
exception
when others then
	qt_minima_meses_w	:= 0;
	qt_maximo_meses_w	:= 0;
	tx_fracao_w		:= 1;
end;

if	(nvl(qt_contribuicao_p,0) > 0) then
	qt_meses_incluido_w	:= qt_contribuicao_p ;
else
	qt_meses_incluido_w	:= pls_obter_tempo_benef_contrib( nr_seq_segurado_p, nr_seq_alteracao_motivo_p, dt_inicio_vigencia_p);
end if;

if	((nvl(qt_tempo_operadora_w,0) = 0) or (nvl(qt_meses_incluido_w,0) < nvl( qt_tempo_operadora_w,0))) then
	qt_meses_lancados_w := dividir(qt_meses_incluido_w,tx_fracao_w);
	
	if	(qt_meses_lancados_w	< qt_minima_meses_w) then
		qt_meses_lancados_w	:= qt_minima_meses_w;
	elsif	(qt_meses_lancados_w	> qt_maximo_meses_w) and
		(qt_maximo_meses_w	<> 0) then
		qt_meses_lancados_w	:= qt_maximo_meses_w;
	end if;
end if;

return	qt_meses_lancados_w ;

end pls_obter_limite_resc_pagador;
/
