create or replace
function Obter_Descricao_Fase(nr_sequencia_p		number)
 		    	return varchar2 is
				
ds_fase_w	varchar2(4000);				

begin

select	ds_fase
into 	ds_fase_w
from 	proj_fase 
where	nr_sequencia = nr_sequencia_p;

return	ds_fase_w;

end Obter_Descricao_Fase;
/