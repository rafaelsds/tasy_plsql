create or replace
function man_obter_se_novo_hist_dia(	dt_dia_p	date,
					nm_usuario_prev_p	varchar2,
					nm_usuario_p	varchar2)
					return varchar2 is

ie_retorno_w	varchar2(1)	:= 'N';
qt_registro_w	number(10);

begin

if	(dt_dia_p is not null) then
	select	count(*)
	into	qt_registro_w
	from	man_estagio_processo c,
		man_ordem_servico b,
		man_ordem_ativ_prev a
	where	a.nr_seq_ordem_serv	= b.nr_sequencia
	and	b.nr_seq_estagio	= c.nr_sequencia
	and	c.ie_desenv		= 'S'
	and	a.nm_usuario_prev	= nm_usuario_prev_p
	and	a.dt_prevista between trunc(dt_dia_p,'dd') and fim_dia(dt_dia_p)
	and	Man_Obter_Se_Novo_Hist(a.nr_seq_ordem_serv,nm_usuario_p) = 'S';
	
	if	(qt_registro_w > 0) then
		ie_retorno_w	:= 'S';
	end if;
end if;

return	ie_retorno_w;

end man_obter_se_novo_hist_dia;
/