create or replace
function ctb_obter_contas_estrut_orc(	cd_estabelecimento_p	number,
				cd_centro_custo_p		number,
				nr_seq_estrutura_p		number)
					return varchar2 is

type reg_contas is record(	cd_conta_contabil varchar2(20));

type contas is table of reg_contas index by binary_integer;
contas_w				contas;
cd_conta_contabil_w		varchar2(20);
ds_origem_w			varchar2(4000);
ds_contas_w			varchar2(4000);
i				integer;
ind				integer;
ie_caracter_w			varchar2(1);
inicial_w				integer;
vl_retorno_w			number(15,2);

begin

begin
select	ds_origem
into	ds_origem_w
from	ctb_estrutura_orc
where	nr_sequencia	= nr_seq_estrutura_p;
exception when no_data_found then
	wheb_mensagem_pck.exibir_mensagem_abort(225449);
end;

vl_retorno_w		:= 0;
ds_origem_w		:= replace(ds_origem_w,' ','');
i			:= 1;
ind			:= 0;
while	(ds_origem_w is not null) loop
	begin
	ie_caracter_w	:= substr(ds_origem_w,i,1);
	if	(ie_caracter_w in('+','-','/','*','(',')')) then
		begin
		i					:= i + 1;
		if	(cd_conta_contabil_w is not null) then
			ind				:= ind + 1;
			contas_w(ind).cd_conta_contabil	:= cd_conta_contabil_w;
			ds_origem_w			:= substr(ds_origem_w,i,4000);
			cd_conta_contabil_w		:= '';
			i				:= 1;
		end if;
		end;
	else
		begin
		cd_conta_contabil_w	:= cd_conta_contabil_w || ie_caracter_w;
		if	(i >= length(ds_origem_w)) then
			ind				:= ind + 1;
			contas_w(ind).cd_conta_contabil	:= cd_conta_contabil_w;
			ds_origem_w			:= '';
		end if;
		i			:= i + 1;
		end;
	end if;
	end;
end loop;

if	(contas_w.Count = 1) then

	ds_contas_w	:= contas_w(1).cd_conta_contabil;
	
elsif	(contas_w.Count > 1) then
	for ind in 1..contas_w.Count loop
		begin
		
		if	(ds_contas_w is not null) then
			ds_contas_w	:= ds_contas_w || ',' || contas_w(ind).cd_conta_contabil;
		else
			ds_contas_w	:= contas_w(ind).cd_conta_contabil;
		end if;
		end;
	end loop;
end if;

return	ds_contas_w;

end ctb_obter_contas_estrut_orc;
/