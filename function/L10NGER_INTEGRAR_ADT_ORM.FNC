create or replace
function l10nger_integrar_adt_orm(
	cd_pessoa_fisica_p		varchar2,
	nr_atendimento_p		number,
	nr_prescricao_p		number,
	nr_seq_episodio_p		number)
	return varchar2 is

ie_integrar_w			varchar2(1)	:=	'S';
cd_pessoa_fisica_externo_w		pf_codigo_externo.cd_pessoa_fisica_externo%type;

nr_seq_episodio_w			episodio_paciente.nr_sequencia%type;
nr_episodio_w			episodio_paciente.nr_episodio%type;
ie_msg_hl7_case_ext_w   parametro_atendimento.ie_msg_hl7_case_ext%TYPE;

pragma autonomous_transaction;

begin
	BEGIN
        SELECT  nvl(max(ie_msg_hl7_case_ext), 'N') ie_msg_hl7_case_ext
        INTO    ie_msg_hl7_case_ext_w
        FROM    parametro_atendimento
        WHERE   cd_estabelecimento = obter_estabelecimento_ativo();
    END;

if	(nvl(cd_pessoa_fisica_p,'null') <> 'null') then
	begin
	select	'S',
		cd_pessoa_fisica_externo
	into	ie_integrar_w,
		cd_pessoa_fisica_externo_w
	from	pf_codigo_externo
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	ie_tipo_codigo_externo = 'ISH'
        and     nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
	and	rownum = 1;
	exception
	when others then
		ie_integrar_w	:=	'N';
	end;
end if;

if	(nr_seq_episodio_p is not null) then
	nr_seq_episodio_w	:=	nr_seq_episodio_p;
elsif	(nr_atendimento_p is not null) then
	begin
	select	nr_seq_episodio
	into	nr_seq_episodio_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	exception
	when others then
		nr_seq_episodio_w	:=	null;
	end;
elsif	(nr_prescricao_p is not null) then
	begin
	select	b.nr_seq_episodio
	into	nr_seq_episodio_w
	from	prescr_medica a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	a.nr_prescricao = nr_prescricao_p;
	exception
	when others then
		nr_seq_episodio_w	:=	null;
	end;
end if;

if	(nr_seq_episodio_w is not null) then
	begin
	select	ie_integrar_w,
		nr_episodio
	into	ie_integrar_w,
		nr_episodio_w
	from	episodio_paciente
	where	nr_sequencia = nr_seq_episodio_w;
	exception
	when others then
		ie_integrar_w	:=	'N';
	end;

	IF((ie_msg_hl7_case_ext_w = 'S') AND (nr_episodio_w IS NOT NULL))THEN
        ie_integrar_w := 'S';
    ELSIF (ie_msg_hl7_case_ext_w = 'N') THEN
        ie_integrar_w := 'S';
    ELSE
        ie_integrar_w := 'N';
    END IF;
end if;

if	(ie_integrar_w = 'N') then
	gravar_log_tasy(43,
		'CD_PESSOA_FISICA=' || cd_pessoa_fisica_p || '#' || chr(13) || chr(10) ||
		'NR_ATENDIMENTO=' || nr_atendimento_p || '#' || chr(13) || chr(10) ||
		'NR_PRESCRICAO=' || nr_prescricao_p || '#' || chr(13) || chr(10) ||
		'NR_SEQ_EPISODIO=' || nr_seq_episodio_w || '#' || chr(13) || chr(10) ||
		'CD_PF_EXTERNO=' || cd_pessoa_fisica_externo_w || '#' || chr(13) || chr(10) ||
		'NR_EPISODIO=' || nr_episodio_w || '#',
		'L10NGerHL7');

	commit;
end if;

return	ie_integrar_w;
end l10nger_integrar_adt_orm;
/
