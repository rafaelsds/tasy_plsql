create or replace
function PLS_OBTER_SALDO_REC_GLO_GUIA	( nr_seq_conta_p	pls_conta.nr_sequencia%type,
					  nm_usuario_p		varchar2 )
					return number is

vl_retorno_w	number(15,2);

begin

select	nvl((	select	sum(PLS_OBTER_SALDO_REC_GLOSA_PROC(z.nr_sequencia, null))
		from	pls_conta_proc	z
		where	z.nr_seq_conta	= a.nr_sequencia
		and	not exists(	select	1
					from	w_pls_recurso_glosa	y 
					where	y.nr_seq_conta_proc	= z.nr_sequencia
					and	y.ie_tipo_inclusao	= 'P')
		and	z.vl_glosa > 0), 0)
	+
	nvl((	select	sum(PLS_OBTER_SALDO_REC_GLOSA_MAT(z.nr_sequencia, null))
		from	pls_conta_mat	z
		where	z.nr_seq_conta	= a.nr_sequencia
		and	not exists(	select	1
					from	w_pls_recurso_glosa	y 
					where	y.nr_seq_conta_proc	= z.nr_sequencia
					and	y.ie_tipo_inclusao	= 'M')
		and	z.vl_glosa > 0), 0)
into	vl_retorno_w
from	pls_conta	a
where	a.nr_sequencia	= nr_seq_conta_p;

return	vl_retorno_w ;

end PLS_OBTER_SALDO_REC_GLO_GUIA ;
/