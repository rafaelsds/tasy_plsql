create or replace
function pls_regra_exo_dem_bonificacao
		(	nr_seq_desconto_p	number,
			ie_vinculo_bonific_p	varchar2,
			qt_vidas_exonerados_p	number,
			qt_vidas_p		number,
			qt_vidas_bonificacao_p	number)
 		    	return number is
/*	ie_vinculo_bonific_p
	B = Beneficiário
	C = Contrato
	P = Pagador
	SB = SCA Beneficiário
	GC = Grupo de contrato
*/			
nr_seq_preco_regra_www	number(10);
nr_seq_preco_regra_w	number(10);
nr_seq_preco_regra_ww	number(10);
ie_demitido_exonerado_w	varchar2(10);
ie_possui_bonificacao_w	varchar2(1);
			
Cursor C01 is
	select	ie_demitido_exonerado,
		ie_possui_bonificacao,
		nr_sequencia
	from	pls_preco_regra
	where	nr_seq_regra = nr_seq_desconto_p;

begin

open C01;
loop
fetch C01 into	
	ie_demitido_exonerado_w,
	ie_possui_bonificacao_w,
	nr_seq_preco_regra_ww;
exit when C01%notfound;
	begin
	if	((nvl(ie_possui_bonificacao_w,'N') = 'S') and (ie_vinculo_bonific_p in ('B','SB'))) then
		select	max(nr_sequencia)
		into	nr_seq_preco_regra_www
		from	pls_preco_regra
		where	nr_sequencia = nr_seq_preco_regra_ww
		and	qt_vidas_bonificacao_p between nvl(qt_min_vidas,qt_vidas_bonificacao_p) and nvl(qt_max_vidas,qt_vidas_bonificacao_p);
	elsif	(nvl(ie_demitido_exonerado_w,'N') = 'S') then
		select	max(nr_sequencia)
		into	nr_seq_preco_regra_www
		from	pls_preco_regra
		where	nr_sequencia = nr_seq_preco_regra_ww
		and	qt_vidas_exonerados_p between nvl(qt_min_vidas,qt_vidas_exonerados_p) and nvl(qt_max_vidas,qt_vidas_exonerados_p);
	elsif	(nvl(ie_demitido_exonerado_w,'N') = 'N') then
		select	max(nr_sequencia)
		into	nr_seq_preco_regra_www
		from	pls_preco_regra
		where	nr_sequencia = nr_seq_preco_regra_ww
		and	qt_vidas_p between nvl(qt_min_vidas,qt_vidas_p) and nvl(qt_max_vidas,qt_vidas_p);
	end if;
	
	if	(nr_seq_preco_regra_www is not null) then
		nr_seq_preco_regra_w	:= nr_seq_preco_regra_www;
	end if;	
	
	end;
end loop;
close C01;


return	nr_seq_preco_regra_w;

end pls_regra_exo_dem_bonificacao;
/