create or replace
function obter_unidade_pac (
		cd_pessoa_fisica_p	varchar2,
		ie_opcao_p		varchar2,
		ie_informacao_p		varchar)
		return varchar2 is
		
nr_atendimento_w	number(10,0);		
ds_unidade_w		varchar2(255);
		
begin
if	(cd_pessoa_fisica_p is not null) and
	(ie_opcao_p is not null) and
	(ie_informacao_p is not null) then
	begin
	select	nvl(max(nr_atendimento),0)
	into	nr_atendimento_w
	from	atendimento_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_alta is null
	and	nvl(obter_setor_atendimento(nr_atendimento),0) > 0;
	
	if	(nr_atendimento_W > 0) then
		begin
		ds_unidade_w := obter_unidade_atendimento(nr_atendimento_w, ie_opcao_p, ie_informacao_p);
		end;
	end if;
	end;
end if;
return ds_unidade_w;
end obter_unidade_pac;
/