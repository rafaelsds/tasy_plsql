create or replace
function obter_desc_proc_material(	cd_material_p			number,
				cd_procedimento_p			number,
				ie_origem_proced_p		number)	
return varchar2 is

/*Essa function � a mesma coisa da OBTER_DESC_PROC_MAT. Mas a function OBTER_DESC_PROC_MAT retorna a frase "N�o encontrado" quando n�o encontra o Material ou o Procedimento.
Tive que criar essa quase igual para retornar vazio*/

ds_retorno_w	varchar2(240);

begin

if	(nvl(cd_material_p,0) > 0) then
	select	substr(nvl(max(ds_material), ''),1,240)
	into	ds_retorno_w
	from 	material
	where	cd_material = cd_material_p;
else
	select nvl(max(ds_procedimento), '')
	into	ds_retorno_w
	from	procedimento
	where	cd_procedimento = cd_procedimento_p
	and	ie_origem_proced = nvl(ie_origem_proced_p,ie_origem_proced);
end if;

return	ds_retorno_w;
end	obter_desc_proc_material;
/
