create or replace
function obter_qt_mesmo_mat_estoque(	cd_material_estoque_p	number)
			return number is

qt_mesmo_estoque_w		number(5);

begin

select	count(*)
into	qt_mesmo_estoque_w
from	material
where	cd_material_estoque = cd_material_estoque_p;

return	qt_mesmo_estoque_w;

end obter_qt_mesmo_mat_estoque;
/