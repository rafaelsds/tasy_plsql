create or replace
function obter_cor_copia_REP	(cd_material_p		number,
				 ie_opcao_p		varchar2)
				return varchar2 is

/*
C = Cor
M = Mensagem
S = Campo Marcado
*/
cd_grupo_material_w 	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
ds_material_w		varchar2(255);
ds_cor_w		varchar2(15);
ds_mensagem_w		varchar2(255);
ie_marcado_w		varchar2(1);
ds_retorno_w		varchar2(255);

cursor c01 is
	select	ds_cor,
		ds_mensagem,
		ie_marcado
	from	regra_cor_material_copia 
	where	nvl(cd_material,cd_material_p) 				= cd_material_p
	and	nvl(cd_grupo_material,cd_grupo_material_w) 		= cd_grupo_material_w
	and	nvl(cd_subgrupo_material,cd_subgrupo_material_w) 	= cd_subgrupo_material_w
	and	nvl(cd_classe_material,cd_classe_material_w) 		= cd_classe_material_w
	and	nvl(ie_situacao,'A')	= 'A'
	order by nvl(cd_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_classe_material,0);
BEGIN

select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material,
	substr(ds_material,1,240)
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	ds_material_w
from 	estrutura_material_v
where 	cd_material		= cd_material_p;
	
	
open C01;
loop
fetch C01 into	
	ds_cor_w,
	ds_mensagem_w,
	ie_marcado_w;
exit when C01%notfound;
	begin
	ds_cor_w	:= ds_cor_w;
	ds_mensagem_w	:= ds_mensagem_w;
	ie_marcado_w	:= ie_marcado_w;
	end;
end loop;
close C01;
					 

if	(ie_opcao_p = 'C') then
	ds_retorno_w := ds_cor_w;
elsif	(ie_opcao_p = 'M') then
	ds_retorno_w := ds_mensagem_w;
elsif	(ie_opcao_p = 'S') then
	ds_retorno_w := ie_marcado_w;
end if;

return	ds_retorno_w;

end obter_cor_copia_REP;
/
