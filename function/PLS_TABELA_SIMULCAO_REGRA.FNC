create or replace
function pls_tabela_simulcao_regra
		(	nr_seq_plano_p		number)
 		    	return number is

nr_seq_tabela_w		number(10);			
			
begin

select	max(nr_seq_tabela)
into	nr_seq_tabela_w
from	pls_regra_simulador_web
where	nr_seq_plano	= nr_seq_plano_p
and	ie_tabela_padrao = 'S'
and	sysdate between dt_inicio_vigencia and nvl(dt_fim_vigencia,sysdate);

if	(nr_seq_tabela_w is null) then
	select	max(nr_seq_tabela)
	into	nr_seq_tabela_w
	from	pls_regra_simulador_web
	where	nr_seq_plano	= nr_seq_plano_p
	and	sysdate between dt_inicio_vigencia and nvl(dt_fim_vigencia,sysdate);
end if;

return	nr_seq_tabela_w;

end pls_tabela_simulcao_regra;
/
