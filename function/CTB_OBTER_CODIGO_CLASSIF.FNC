create or replace
function ctb_obter_codigo_classif(	cd_empresa_p	number,	
				cd_classificacao_p	varchar2)
				return 		varchar2 is
				
cd_conta_contabil_w		varchar2(20);
nr_seq_classif_w			number(10);

BEGIN

if	(cd_classificacao_p is not null) and
	(cd_empresa_p is not null) then
	
	select	max(a.nr_sequencia)
	into	nr_seq_classif_w
	from	conta_contabil b,
		conta_contabil_classif a
	where	a.cd_conta_contabil	= b.cd_conta_contabil
	and	a.cd_classificacao		= cd_classificacao_p
	and	b.cd_empresa		= cd_empresa_p
	and	a.dt_inicio_vigencia 	<= sysdate
	and	nvl(a.dt_fim_vigencia, sysdate) >= sysdate;
	
	if	(nr_seq_classif_w is null) then
		begin
		select	max(cd_conta_contabil)
		into	cd_conta_contabil_w
		from	conta_contabil
		where	cd_classificacao = cd_classificacao_p
		and	cd_empresa	= cd_empresa_p;
		end;
	else
		select	cd_conta_contabil
		into	cd_conta_contabil_w
		from	conta_contabil_classif
		where	nr_sequencia	= nr_seq_classif_w;
	end if;

end if;

return cd_conta_contabil_w;

END ctb_obter_codigo_classif;
/