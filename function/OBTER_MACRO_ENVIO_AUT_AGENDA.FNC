create or replace
function obter_macro_envio_aut_agenda return varchar2 is

ds_retorno_w		varchar2(2000);

begin

ds_retorno_w	:=	'@anestesia - M�dico anestesista'||chr(13)||
			'@opme - Lista das OPME''s'||chr(13)||
			'@opmeautorizado -  - Lista das OPME''s autorizadas'||chr(13)||
			'@equipamento - Lista dos equipamentos'||chr(13)||
			'@procedimento - Descri��o do procedimento'||chr(13)||
			'@cirurgiao - M�dico cirurgi�o'||chr(13)||
			'@paciente - Nome do paciente'||chr(13)||
			'@idade - Idade do paciente'||chr(13)||
			'@data - Data/hora do agendamento'||chr(13)||
			'@duracao - Dura��o do agendamento'||chr(13)||
			'@cirurgia - N� da cirurgia'||chr(13)||
			'@reserva - N� da reserva'||chr(13)||
			'@convenio - Conv�nio'||chr(13)||
			'@categoria - Categoria'||chr(13)||
			'@docusuconv - C�digo do usu�rio do conv�nio'||chr(13)||
			'@observacao - Observa��o'||chr(13)||
			'@procadic - Lista dos procedimentos adicionais'||chr(13)||
			'@coddescproced - C�digo e descri��o do procedimento principal'||chr(13)||
			'@servico - Lista de servi�os'||chr(13)||
			'@agendamento - Data que foi criado o agendamento'||chr(13)||
			'@atendimento - Atendimento do agendamento'||chr(13)||
			'@bancosangue - Obter dados banco de sangue';

return	ds_retorno_w;

end obter_macro_envio_aut_agenda;
/