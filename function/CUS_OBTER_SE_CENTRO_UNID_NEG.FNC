create or replace
function cus_obter_se_centro_unid_neg(	cd_estabelecimento_p	number,
					cd_centro_controle_p	number,
					nr_seq_unid_neg_p	number)
					return varchar2 is

ie_retorno_w				varchar2(1)	:= 'N';

begin

select	nvl(max('S'),'N')
into	ie_retorno_w
from	centro_controle_unid_neg a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_centro_controle	= cd_centro_controle_p
and	a.nr_seq_unid_neg	= nr_seq_unid_neg_p;

return	ie_retorno_w;

end cus_obter_se_centro_unid_neg;
/