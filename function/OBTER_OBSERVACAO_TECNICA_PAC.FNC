create or replace
function obter_observacao_tecnica_pac(	cd_pessoa_fisica_p	varchar2,
					cd_refeicao_p		varchar2,
					dt_dieta_p		date,
					nr_atendimento_p	number default 0)
 		    	return varchar2 is

nr_sequencia_w		number(10);
ds_obs_tecnica_w	varchar2(255);
ie_obs_tecnica_atendimento_w	varchar2(1);
	
begin
Obter_Param_Usuario(1000,72,Obter_Perfil_Ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_obs_tecnica_atendimento_w);

select 	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	mapa_dieta_obs_tec_pf
where	cd_pessoa_fisica 	= cd_pessoa_fisica_p
and	cd_refeicao 		= cd_refeicao_p
and	((ie_obs_tecnica_atendimento_w <> 'P') or (nr_atendimento = nr_atendimento_p))
and	trunc(dt_referencia)   <= dt_dieta_p;

if (nr_sequencia_w > 0) then

	select	max(ds_obs_tecnica)
	into	ds_obs_tecnica_w
	from	mapa_dieta_obs_tec_pf
	where	nr_sequencia = nr_sequencia_w	;

end if;

return	ds_obs_tecnica_w;

end obter_observacao_tecnica_pac;
/
