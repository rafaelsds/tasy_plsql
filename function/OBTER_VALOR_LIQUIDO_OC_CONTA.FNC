create or replace
function Obter_Valor_Liquido_OC_Conta	(nr_ordem_compra_p	number,
					cd_conta_contabil_p	varchar2)
				return Number is

vl_item_w				number(15,4) := 0;
pr_desconto_w			number(15,4) := 0;
vl_desconto_w			number(15,4) := 0;
vl_retorno_w			number(15,4) := 0;
vl_frete_w			number(15,4) := 0;
vl_despesa_acessoria_w		number(13,2) := 0;
vl_desconto_oc_w			number(13,2) := 0;
ie_frete_w			varchar2(01);

begin

select		nvl(max(pr_desconto),0),
		nvl(max(ie_frete),'C'),
		nvl(max(vl_frete),0),
		nvl(max(vl_despesa_acessoria),0),
		nvl(max(vl_desconto),0)
into		pr_desconto_w,
		ie_frete_w,
		vl_frete_w,
		vl_despesa_acessoria_w,
		vl_desconto_oc_w
from		ordem_compra
where		nr_ordem_compra	= nr_ordem_compra_p;

select	nvl(sum(Obter_Valor_Liquido_Oci(nr_ordem_compra, nr_item_oci)),0)
into	vl_item_w
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
and	dt_reprovacao is null
and	cd_conta_contabil = cd_conta_contabil_p;

if	(pr_desconto_w > 0) then
	vl_desconto_w	:= (vl_item_w * pr_desconto_w) / 100;
end if;

vl_retorno_w		:= (vl_item_w - vl_desconto_w) - nvl(vl_desconto_oc_w,0);

if	(vl_despesa_acessoria_w > 0) then
	vl_retorno_w	:= (vl_retorno_w + vl_despesa_acessoria_w);
end if;

if	(ie_frete_w	= 'F')then
	vl_retorno_w	:= (vl_retorno_w + vl_frete_w);	
end if;

return vl_retorno_w;

end Obter_Valor_Liquido_OC_Conta;
/
