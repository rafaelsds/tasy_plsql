CREATE OR REPLACE
FUNCTION Obter_Se_Atend_Agenda_pac	(nr_atendimento_p	Number,
					cd_agenda_p		Varchar2)
					Return Varchar IS

ie_agenda_w	Varchar2(1)	:= 'S';

BEGIN
if	(nr_atendimento_p is not null) and
	(cd_agenda_p is not null) then
	
	
	select	decode(count(*), 0, 'N', 'S')
	into	ie_agenda_w
	from	agenda_consulta a
	where	a.nr_atendimento = nr_atendimento_p
	and	a.cd_agenda = cd_agenda_p;
	
	if	(ie_agenda_w = 'N') then
		select	decode(count(*), 0, 'N', 'S')
		into	ie_agenda_w
		from	agenda_paciente a
		where	a.nr_atendimento = nr_atendimento_p
		and	a.cd_agenda = cd_agenda_p;	
	end if;	
end if;

Return	ie_agenda_w;

END	Obter_Se_Atend_Agenda_pac;
/
