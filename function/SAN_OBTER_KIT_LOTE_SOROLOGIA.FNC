create or replace
function san_obter_kit_lote_sorologia(	nr_Seq_lote_kit_p	number,
					dt_referencia_p		date,
					ie_tipo_p		varchar2)
 		    	return varchar2 is

nr_sequencia_w	number(10);
ds_kit_w	varchar(255);
dt_vencimento_w	date;
			
ds_retorno_w	varchar2(255);
			
begin

if (nr_Seq_lote_kit_p is not null) then

	select	max(nr_sequencia),
		max(ds_kit),
		max(dt_vigencia_final)
	into 	nr_Sequencia_w,
		ds_kit_w,
		dt_vencimento_w
	from	san_kit_exame
	where	nr_sequencia = nr_Seq_lote_kit_p
	and	dt_referencia_p between dt_vigencia_ini and dt_vigencia_final;
	
	if (ie_tipo_p = 'C') then
		ds_retorno_w := nr_sequencia_w;
	elsif (ie_tipo_p = 'D') then
		ds_retorno_w := ds_kit_w;
	elsif (ie_tipo_p = 'T') then
		ds_retorno_w := to_char(dt_vencimento_w,'dd/mm/yyyy hh24:mi:ss');
	end if;
	
end if;

return	ds_retorno_w;

end san_obter_kit_lote_sorologia;
/