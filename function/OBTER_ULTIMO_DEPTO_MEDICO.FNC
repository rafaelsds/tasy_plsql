create or replace function obter_ultimo_depto_medico(nr_atendimento_p  number)
        return number is
cd_departamento_w number(10);
dt_entrada_unidade_w date;
begin


select  max(dt_entrada_unidade)
into dt_entrada_unidade_w
from  atend_paciente_unidade
where nr_atendimento = nr_atendimento_p;

if (dt_entrada_unidade_w is not null) then
 select  CD_DEPARTAMENTO
 into cd_departamento_w
 from  atend_paciente_unidade
 where nr_atendimento = nr_atendimento_p
 and dt_entrada_unidade   = dt_entrada_unidade_w;
end if;


return cd_departamento_w;

end obter_ultimo_depto_medico;
 /