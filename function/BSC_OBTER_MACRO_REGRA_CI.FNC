create or replace
function bsc_obter_macro_regra_ci(	nm_usuario_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(4000) := '';

begin
ds_retorno_w := 	substr('@NR_INICIATIVA'			|| CHR(13) || CHR(10) ||
		'@NM_SOLICITANTE'		|| CHR(13) || CHR(10) ||
		'@NM_RESPONSAVEL'		|| CHR(13) || CHR(10) ||
		'@DS_DESCRICAO'		|| CHR(13) || CHR(10) ||
		'@DS_ESTAGIO'			|| CHR(13) || CHR(10) ||
		'@DT_FIM_PREVISTO'		|| CHR(13) || CHR(10) ||
		'@DT_INICIO_PREVISTO'		|| CHR(13) || CHR(10) ||
		'@NM_RESP_INDICADOR'		|| CHR(13) || CHR(10) ||
		'@NM_INDICADOR_VINCULADO'	|| CHR(13) || CHR(10) ||
		'@DS_HISTORICO'			|| CHR(13) || CHR(10) ||
		'@DS_DANO' || CHR(13) || CHR(10) ||
		'@DS_EXECUTORES',1,4000);

return	ds_retorno_w;

end bsc_obter_macro_regra_ci;
/