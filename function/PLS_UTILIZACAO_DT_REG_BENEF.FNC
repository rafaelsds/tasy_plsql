create or replace
function pls_utilizacao_dt_reg_benef	(	dt_inicio_p		varchar2,
						dt_fim_p		varchar2) 
							return varchar2 is

-- Verifica se a data de inicio e a data fim da consulta se encaixa nas regras de consulta e de vig�ncia
				
dt_inicio_w		date := to_date(dt_inicio_p);
dt_fim_w		date := to_date(dt_fim_p);
dt_vigencia_inicio_w	date;
dt_vigencia_fim_w	date;
dt_consulta_inicio_w	date;
dt_consulta_fim_w	date;
ie_dia_vig_ini_w	pls_regra_utili_benef_web.ie_dia_vig_ini%type;
ie_dia_vig_fin_w	pls_regra_utili_benef_web.ie_dia_vig_fin%type;
nr_mes_inicio_w		pls_regra_utili_benef_web.nr_mes_inicio%type;
nr_mes_final_w		pls_regra_utili_benef_web.nr_mes_final%type;
ie_dia_utilizado_ini_w	pls_regra_utili_benef_web.ie_dia_utilizado_ini%type;
ie_dia_utilizado_fin_w	pls_regra_utili_benef_web.ie_dia_utilizado_fin%type;
nr_mes_util_ini_w	pls_regra_utili_benef_web.nr_mes_util_ini%type;
nr_mes_util_fin_w	pls_regra_utili_benef_web.nr_mes_util_fin%type;
ds_mensagem_w		pls_regra_utili_benef_web.ds_mensagem%type;

Cursor C01 is
	select	ie_dia_vig_ini,
		nr_mes_inicio,
		ie_dia_vig_fin,
		nr_mes_final,
		ie_dia_utilizado_ini,
		nr_mes_util_ini,
		ie_dia_utilizado_fin,
		nr_mes_util_fin,
		ds_mensagem
	from	pls_regra_utili_benef_web
	where	ie_situacao = 'A';
	
begin

open C01;
loop
fetch C01 into
	ie_dia_vig_ini_w,
	nr_mes_inicio_w,
	ie_dia_vig_fin_w,
	nr_mes_final_w,
	ie_dia_utilizado_ini_w,
	nr_mes_util_ini_w,
	ie_dia_utilizado_fin_w,
	nr_mes_util_fin_w,
	ds_mensagem_w;
exit when C01%notfound;
	--monta data vig�ncia
	dt_vigencia_inicio_w := to_date('01/'||to_char(nr_mes_inicio_w, '09')||'/'||to_char(sysdate,'yyyy'));
	
	if	(ie_dia_vig_ini_w = 2) then --�ltimo dia do m�s	
		dt_vigencia_inicio_w := trunc(last_day(dt_vigencia_inicio_w),'dd');
	end if;
		
	dt_vigencia_fim_w := to_date('01/'||to_char(nr_mes_final_w, '09')||'/'||to_char(sysdate,'yyyy'));
	
	if	(ie_dia_vig_fin_w = 2) then --�ltimo dia do m�s	
		dt_vigencia_fim_w := trunc(last_day(dt_vigencia_fim_w),'dd');
	end if;
		
	--verifica se a data atual se encaixa na vig�ncia da regra
	if	(trunc(sysdate,'dd') between dt_vigencia_inicio_w and dt_vigencia_fim_w) then
		--monta datas consulta
		dt_consulta_inicio_w := to_date('01/'||to_char(nr_mes_util_ini_w, '09')||'/'||to_char(sysdate,'yyyy'));
		
		if	(ie_dia_utilizado_ini_w = 2) then --�ltimo dia do m�s	
			dt_consulta_inicio_w := trunc(last_day(dt_consulta_inicio_w),'dd');
		end if;
		
		dt_consulta_fim_w := to_date('01/'||to_char(nr_mes_util_fin_w, '09')||'/'||to_char(sysdate,'yyyy'));
		
		if	(ie_dia_utilizado_fin_w = 2) then --�ltimo dia do m�s	
			dt_consulta_fim_w := trunc(last_day(dt_consulta_fim_w),'dd');
		end if;
		
		--verifica se as datas se encaixam no per�odo da consulta
		if	(dt_inicio_w not between dt_consulta_inicio_w and dt_consulta_fim_w) or
			(dt_fim_w not between dt_consulta_inicio_w and dt_consulta_fim_w) then
			--caso n�o se encaixa retorna a mensagem 
			return ds_mensagem_w;   
		end if;
	end if;
	end loop;
close C01;

return null;

end pls_utilizacao_dt_reg_benef;
/