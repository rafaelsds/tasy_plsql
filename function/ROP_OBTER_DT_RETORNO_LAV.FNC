create or replace
function rop_obter_dt_retorno_lav(	nr_sequencia_p		number,
				cd_barras_p		varchar2)
 		    	return date is

dt_retorno_w		date;
dt_liberacao_w		date;
			
begin

select	dt_liberacao
into	dt_liberacao_w
from	rop_lote_movto
where	nr_sequencia = nr_sequencia_p;

if	(dt_liberacao_w is not null) then
	select	min(a.dt_liberacao)
	into	dt_retorno_w
	from	rop_lote_movto a,
		rop_movto_roupa b		
	where	a.nr_sequencia = b.nr_seq_lote
	and	a.dt_liberacao > dt_liberacao_w
	and	substr(rop_obter_dados_roupa(b.nr_seq_roupa, 'C'),1,40) = cd_barras_p
	and	rop_obter_tipo_local(a.nr_seq_local) = 'LI';
end if;

return	dt_retorno_w;

end rop_obter_dt_retorno_lav;
/