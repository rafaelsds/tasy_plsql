create or replace
function man_obter_dados_catal_servico(	nr_seq_servico_p		number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is


/*
ie_opcao_p

D - Descri��o
C - C�digo
DT - Descri��o do tipo do cat�logo
CT - C�digo do tipo
H - N�mero de horas
*/

cd_servico_w			varchar2(80);
ds_tipo_servico_w		varchar2(255);
nr_seq_tipo_servico_w	number(10);	
qt_hora_util_w			number(07,2);
ds_servico_w			varchar2(255);
ds_retorno_w			varchar2(255);

begin
select	cd_servico,
		substr(obter_descricao_padrao('MAN_TIPO_CATALOGO','DS_TIPO',nr_seq_tipo),1,255) ds_tipo_servico,
		nr_seq_tipo nr_seq_tipo_servico,
		qt_hora_util,
		ds_servico
into	cd_servico_w,
		ds_tipo_servico_w,
		nr_seq_tipo_servico_w,
		qt_hora_util_w,
		ds_servico_w
from	man_catalogo_servico
where	nr_sequencia = nr_seq_servico_p;

if	(ie_opcao_p = 'D') then
	ds_retorno_w := ds_servico_w;
elsif	(ie_opcao_p = 'C') then
	ds_retorno_w := cd_servico_w;
elsif	(ie_opcao_p = 'DT') then
	ds_retorno_w := ds_tipo_servico_w;
elsif	(ie_opcao_p = 'CT') then
	ds_retorno_w := nr_seq_tipo_servico_w;	
elsif	(ie_opcao_p = 'H') then
	ds_retorno_w := qt_hora_util_w;
end if;
	
return	ds_retorno_w;

end man_obter_dados_catal_servico;
/