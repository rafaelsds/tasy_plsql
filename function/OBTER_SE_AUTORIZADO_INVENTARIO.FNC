create or replace
function obter_se_autorizado_inventario(nm_usuario_p	varchar2)
 		    	return varchar2 is

ie_tipo_inventario_w	varchar2(1);
ds_retorno_w		varchar2(15) := 'N';
			
begin

select	max(ie_tipo_inventario)
into	ie_tipo_inventario_w
from	sup_autoriza_inventario
where	obter_pessoa_fisica_usuario(nm_usuario_p,'C') = cd_pessoa_autorizada
and	sysdate between dt_inicio and dt_final
and	dt_liberacao is not null;

if	(ie_tipo_inventario_w is not null) then
	ds_retorno_w := ie_tipo_inventario_w;
end if;

return	ds_retorno_w;

end obter_se_autorizado_inventario;
/