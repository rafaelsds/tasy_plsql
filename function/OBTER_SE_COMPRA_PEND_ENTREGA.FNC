create or replace
function obter_se_compra_pend_entrega(
				cd_material_p		number)
				return varchar2 is

ie_pendente_w			varchar2(1) := 'N';
ie_entrega_pendente_w		varchar2(1) := 'N';
nr_solic_compra_w		number(10);
nr_ordem_compra_w		number(10);
nr_item_oci_w			number(5);
qt_existe_w			number(5);

cursor	c01 is
	select	b.nr_solic_compra
	from	solic_compra_item a,
		solic_compra b
	where	a.cd_material = cd_material_p
	and	a.dt_reprovacao is null
	and	b.nr_seq_motivo_cancel is null
	and	a.nr_solic_compra = b.nr_solic_compra
	and	b.dt_solicitacao_compra > (sysdate -90)
	group by b.nr_solic_compra
	order by b.nr_solic_compra;

cursor c02 is
	select	nr_ordem_compra,
		nr_item_oci
	from	ordem_compra_item
	where	cd_material = cd_material_p
	and	nr_solic_compra = nr_solic_compra_w;

begin

open c01;
loop
fetch c01 into	
	nr_solic_compra_w;
exit when c01%notfound;
	begin

	select	count(*)
	into	qt_existe_w
	from	ordem_compra_item
	where	cd_material = cd_material_p
	and	nr_solic_compra = nr_solic_compra_w;
	
	if	(qt_existe_w = 0) then
		ie_entrega_pendente_w := 'S';
		goto final;
	end if;
	
	open c02;
	loop
	fetch c02 into	
		nr_ordem_compra_w,
		nr_item_oci_w;
	exit when c02%notfound;
		begin
		
		ie_entrega_pendente_w := obter_se_oci_pend_entrega(nr_ordem_compra_w,nr_item_oci_w);
		
		if	(ie_entrega_pendente_w = 'S') then
			goto final;
		end if;
		
		end;
	end loop;
	close c02;
	
	end;
end loop;
close c01;

<<final>>

if	(ie_entrega_pendente_w = 'S') then
	ie_pendente_w := 'S';
end if;

return ie_pendente_w;

end obter_se_compra_pend_entrega;
/