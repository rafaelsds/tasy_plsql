create or replace
function obter_inf_hist_saude(cd_pessoa_fisica_p	varchar2,
							ie_opcao_p varchar2	)
 		    	return varchar2 is
ds_retorno_w varchar2(2000);
				
ds_medicamento_w varchar2(255);
ds_lista_medicamentos_w varchar2(2000);
ds_cirurgia_w	varchar2(255);
ds_lista_cirurgias_w varchar2(2000);
ds_anestesia_w varchar2(255);
ds_lista_anestesia_w varchar2(2000);
ds_doenca_w varchar2(255);
ds_lista_doenca_w varchar2(2000);
ds_habitos_w varchar2(255);
ds_lista_habitos_w varchar2(2000);
ds_alergias_w varchar2(400);
ds_lista_alergias_w varchar2(2000);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(294059, null, wheb_usuario_pck.get_nr_seq_idioma);--Nega Doen�as



cursor C01 is
select	nvl(b.ds_material,a.ds_medicamento)
from	paciente_medic_uso a,
		material b
where	a.cd_material = b.cd_material(+)
and		a.cd_pessoa_fisica = cd_pessoa_fisica_p;

cursor C02 is
select	nvl(b.ds_procedimento,a.ds_procedimento_inf),
		substr(obter_valor_dominio(36,a.ie_tipo_anestesia),1,60)
from	historico_saude_cirurgia a,
		procedimento b
where	a.cd_procedimento  = b.cd_procedimento(+)
and		a.ie_origem_proced = b.ie_origem_proced(+)	
and		a.cd_pessoa_fisica = cd_pessoa_fisica_p;

cursor C03 is
select	nvl(a.ds_doenca,nvl(b.ds_doenca,c.ds_doenca_cid))
from	paciente_antec_clinico a,
		pe_doenca b,
		cid_doenca c
where	a.nr_seq_doenca = b.nr_sequencia(+)
and		a.cd_doenca 	= c.cd_doenca_cid(+)
and	NVL(a.ie_paciente,'S')	= 'S'
and	nvl(a.ds_doenca,nvl(b.ds_doenca,c.ds_doenca_cid)) is not null
and		a.cd_pessoa_fisica = cd_pessoa_fisica_p
union 
select	expressao1_w ds_doenca
from	paciente_antec_clinico a,
	pe_doenca b,
	cid_doenca c
where	a.nr_seq_doenca = b.nr_sequencia(+)
and	a.cd_doenca 	= c.cd_doenca_cid(+)
and	nvl(a.ie_paciente,'S')	= 'S'
and 	nvl(a.ie_nega_doencas,'S') = 'S'
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;

cursor C04 is
select	b.ds_vicio
from	paciente_habito_vicio a,
		med_tipo_vicio b
where	a.nr_seq_tipo = b.nr_sequencia
and		a.cd_pessoa_fisica = cd_pessoa_fisica_p;

cursor C05 is
SELECT	DECODE(d.ds_substancia,NULL,'',d.ds_substancia) ||
	DECODE(c.ds_dcb,NULL,'',c.ds_dcb) ||
	DECODE(b.ds_material,NULL,'',b.ds_material) ||		
	DECODE(e.ds_classe_material,NULL,'',e.ds_classe_material) ||
	DECODE(a.DS_MEDIC_NAO_CAD,NULL,'',a.DS_MEDIC_NAO_CAD)
FROM	paciente_alergia a,
	material b,
	dcb_medic_controlado c,
	medic_ficha_tecnica d,
	classe_material e
WHERE	a.cd_material = b.cd_material(+)
AND	a.nr_seq_dcb = c.nr_sequencia(+)
AND	a.nr_seq_ficha_tecnica = d.nr_sequencia(+)
AND 	a.CD_CLASSE_MAT = e.cd_classe_material(+) 
AND	a.cd_pessoa_fisica = cd_pessoa_fisica_p;

begin

open C01;
	loop
	fetch C01 into
		ds_medicamento_w;
	exit when C01%notfound;
	
		if (ds_lista_medicamentos_w is not null) then
			ds_lista_medicamentos_w := substr(ds_lista_medicamentos_w || ' , ',1,2000);
		end if;
		ds_lista_medicamentos_w := substr(ds_lista_medicamentos_w || ds_medicamento_w,1,2000);
		
	end loop;
close C01;

open C02;
	loop
	fetch C02 into
		ds_cirurgia_w,
		ds_anestesia_w;
	exit when C02%notfound;
		
		if (ds_lista_cirurgias_w is not null) then
			ds_lista_cirurgias_w := substr(ds_lista_cirurgias_w || ' , ',1,2000);
		end if;
		ds_lista_cirurgias_w := substr(ds_lista_cirurgias_w || ds_cirurgia_w,1,2000);
		
		if (ds_lista_anestesia_w is not null) then
			ds_lista_anestesia_w := substr(ds_lista_anestesia_w || ' , ',1,2000);
		end if;
		ds_lista_anestesia_w := substr(ds_lista_anestesia_w || ds_anestesia_w,1,2000);
		
	end loop;
close C02;


open C03;
	loop
	fetch C03 into
		ds_doenca_w;
	exit when C03%notfound;
	
		if (ds_lista_doenca_w is not null) then
			ds_lista_doenca_w := substr(ds_lista_doenca_w || ' , ',1,2000);
		end if;
		ds_lista_doenca_w := substr(ds_lista_doenca_w || ds_doenca_w,1,2000);
		
	end loop;
close C03;


open C04;
	loop
	fetch C04 into
		ds_habitos_w;
	exit when C04%notfound;
	
		if (ds_lista_habitos_w is not null) then
			ds_lista_habitos_w := substr(ds_lista_habitos_w || ' , ',1,2000);
		end if;
		ds_lista_habitos_w := substr(ds_lista_habitos_w || ds_habitos_w,1,2000);
		
	end loop;
close C04;


open C05;
	loop
	fetch C05 into
		ds_alergias_w;
	exit when C05%notfound;
	
		if (ds_lista_alergias_w is not null) then
			ds_lista_alergias_w := substr(ds_lista_alergias_w || ' , ',1,2000);
		end if;
		ds_lista_alergias_w := substr(ds_lista_alergias_w || ds_alergias_w,1,2000);
		
	end loop;
close C05;


if (ie_opcao_p = 'M') then
	ds_retorno_w := ds_lista_medicamentos_w;
else if (ie_opcao_p = 'C') then
	ds_retorno_w := ds_lista_cirurgias_w;
else if (ie_opcao_p = 'A') then
	ds_retorno_w := ds_lista_anestesia_w;
else if (ie_opcao_p = 'D') then
	ds_retorno_w := ds_lista_doenca_w;
else if (ie_opcao_p = 'H') then
	ds_retorno_w := ds_lista_habitos_w;
else if (ie_opcao_p = 'AL') then
	ds_retorno_w := ds_lista_alergias_w;
end if;
end if;
end if;
end if;	
end if;
end if;

return ds_retorno_w;

end obter_inf_hist_saude;
/
