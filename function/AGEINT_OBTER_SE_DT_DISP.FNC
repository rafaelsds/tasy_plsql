create or replace
function ageint_obter_se_dt_disp(dt_agenda_p		date,
				nr_seq_agenda_int_p	number,
				nr_seq_ageint_item_p	number)
 		    	return varchar2 is

qt_data_item_w	number(10,0);
ie_retorno_w	varchar2(1);

begin

select	count(*)
into	qt_data_item_w
from	ageint_horarios_usuario a,
	ageint_lib_usuario b,
	agenda_integrada_item c
where 	a.nr_seq_ageint_lib 	= b.nr_sequencia
and	b.nr_seq_ageint_item	= c.nr_sequencia
and	trunc(a.hr_Agenda)	= trunc(dt_agenda_p)
and	a.ie_status_agenda	= 'L'
and	c.nr_seq_agenda_int 	= nr_seq_agenda_int_p
and	c.nr_sequencia		= nr_seq_ageint_item_p;

if	(qt_data_item_w = 0) then
	ie_retorno_w	:= 'N';
else
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end ageint_obter_se_dt_disp;
/