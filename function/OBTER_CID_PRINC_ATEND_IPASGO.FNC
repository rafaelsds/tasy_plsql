create or replace
function obter_cid_princ_atend_ipasgo(	cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,	
					nr_atendimento_p		atendimento_paciente.nr_atendimento%type)
					return diagnostico_doenca.cd_doenca%type is

dt_diagnostico_w		diagnostico_medico.dt_diagnostico%type;
cd_doenca_w			diagnostico_doenca.cd_doenca%type;
ie_forma_cid_princ_ipasgo_w	parametro_faturamento.ie_forma_cid_princ_ipasgo%type := 'U';

cursor C01	is
	select	cd_doenca
	from	diagnostico_doenca
	where	nr_atendimento	= nr_atendimento_p
	and	dt_diagnostico	= dt_diagnostico_w
	and	ie_classificacao_doenca = 'P'
	order by dt_atualizacao;

begin

begin
select	nvl(ie_forma_cid_princ_ipasgo,'U')
into	ie_forma_cid_princ_ipasgo_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_p
and	rownum = 1;
exception
when others then
	ie_forma_cid_princ_ipasgo_w := 'U';
end;

if	(ie_forma_cid_princ_ipasgo_w = 'U') then
	begin
	
	begin
	select	max(dt_diagnostico)
	into	dt_diagnostico_w
	from	diagnostico_medico
	where	nr_atendimento	= nr_atendimento_p
	and	ie_tipo_diagnostico = 2;
	exception
	when no_data_found then
		dt_diagnostico_w := null;
	end;
	
	end;
elsif	(ie_forma_cid_princ_ipasgo_w = 'P') then
	begin
	
	begin
	select	min(dt_diagnostico)
	into	dt_diagnostico_w
	from	diagnostico_medico
	where	nr_atendimento	= nr_atendimento_p
	and	ie_tipo_diagnostico = 2;
	exception
	when no_data_found then
		dt_diagnostico_w := null;
	end;
	
	end;
end if;

open c01;
	loop
	fetch c01 into
		cd_doenca_w;
	exit when c01%notfound;	
	end loop;
close c01;	

return cd_doenca_w;

end obter_cid_princ_atend_ipasgo;
/
