create or replace
function OBTER_CHEQUE_BORDERO	(nr_bordero_p	number) return varchar2 is

nr_cheque_w		varchar2(20);
ds_cheques_w		varchar2(254);

Cursor c01 is
	select	decode(a.nr_cheque,null,'',a.nr_cheque || ', ')
	from	cheque b,
		CHEQUE_BORDERO_TITULO a
	WHERE	a.nr_seq_cheque	= b.nr_sequencia
	and	b.dt_cancelamento is null
	and	a.nr_bordero = nr_bordero_p;

begin

open c01;
loop
fetch c01 into
	nr_cheque_w;
exit	when c01%notfound;
	ds_cheques_w := substr(ds_cheques_w || nr_cheque_w,1,254);
end loop;
close c01;

return	ds_cheques_w;

end;
/