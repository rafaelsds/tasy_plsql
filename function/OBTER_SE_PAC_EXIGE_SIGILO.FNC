create or replace
function obter_se_pac_exige_sigilo( 	cd_pessoa_fisica_p varchar2,
					nr_classif_pf_p   varchar2)
    return varchar2 is

ds_retorno_w  		varchar2(255);

begin
ds_retorno_w := '';
if   	(cd_pessoa_fisica_p  is not null) and
	(nr_classif_pf_p is not null) then

	select  max(ds_classificacao)
	into    ds_retorno_w
	from    pessoa_classif a,
		classif_pessoa b
	where   cd_estabelecimento =  wheb_usuario_pck.get_cd_estabelecimento
	and  	cd_pessoa_fisica   =  cd_pessoa_fisica_p
	and 	a.nr_seq_classif = b.nr_sequencia
	and 	to_char(nr_seq_classif)    =  nr_classif_pf_p;

end if;

return ds_retorno_w;

end obter_se_pac_exige_sigilo;
/