create or replace
function Obter_aviso_alta_paciente(nr_atendimento_p	number)
 		    	return varchar2 is

ie_tipo_atendimento_w	number(3);
cd_convenio_w		number(5,0);
ds_mensagem_w		varchar2(255);
qt_regra_w		number(10) := 0;
nr_seq_atepacu_w		number(10);
nr_seq_unidade_w		number(10);

begin

begin
	select 	1
	into	qt_regra_w
	from 	regra_aviso_alta
	where	rownum = 1;
exception
when others then
	qt_regra_w := 0;
end;

if	(nvl(qt_regra_w,0) > 0) then

	select	max(ie_tipo_atendimento),
		max(obter_convenio_atendimento(nr_atendimento)),
		max(Obter_Atepacu_paciente(nr_atendimento,'IAA'))
	into	ie_tipo_atendimento_w,
		cd_convenio_w,
		nr_seq_atepacu_w
	from	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_p;
	
	select  	max(a.nr_seq_interno)
	into	nr_seq_unidade_w
	from 	unidade_atendimento a,
		atend_paciente_unidade b
	where	a.cd_setor_atendimento = b.cd_setor_atendimento
	and	a.cd_unidade_basica = b.cd_unidade_basica
	and	a.cd_unidade_compl = b.cd_unidade_compl
	and	b.nr_seq_interno = nr_seq_atepacu_w;
	

	select 	max(ds_mensagem)
	into	ds_mensagem_w
	from	regra_aviso_alta
	where	(nvl(ie_tipo_atendimento, ie_tipo_atendimento_w)	= ie_tipo_atendimento_w)
	and  	(nvl(cd_convenio, cd_convenio_w)			= cd_convenio_w)
	and	((cd_equipamento) is null or (Obter_se_equip_unid(nr_seq_unidade_w,cd_equipamento) = 'S'));
end if;
	
return	ds_mensagem_w;

end Obter_aviso_alta_paciente;
/
