Create or Replace
FUNCTION OBTER_VALOR_LIMITE(	vl_valor_p	number,
				vl_minimo_p	number,
				vl_maximo_p	number)
				RETURN number IS

vl_final_w	number(15,2);

BEGIN

if	(vl_minimo_p is not null) and (vl_valor_p < vl_minimo_p) then
	vl_final_w	:= nvl(vl_minimo_p,0);
elsif	(vl_maximo_p is not null) and (vl_valor_p > vl_maximo_p) then
	vl_final_w	:= nvl(vl_maximo_p,0);
else
	vl_final_w	:= nvl(vl_valor_p,0);
end if;

RETURN	vl_final_w;

END OBTER_VALOR_LIMITE;
/