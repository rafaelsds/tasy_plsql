create or replace
function obter_mascara_cid (cd_doenca_cid_p 	varchar2)
					return varchar2 is

qt_tamanho_w		number(10,0);
cd_doenca_cid_w		varchar2(10);

begin

select 	length(cd_doenca_cid_p)
into	qt_tamanho_w
from 	dual;

if	(qt_tamanho_w = 4) then
	cd_doenca_cid_w:=  substr(cd_doenca_cid_p,1,3) || '.' || substr(cd_doenca_cid_p,4,4);
else
	cd_doenca_cid_w:=  cd_doenca_cid_p;
end if;	


return cd_doenca_cid_w;

end obter_mascara_cid;
/
