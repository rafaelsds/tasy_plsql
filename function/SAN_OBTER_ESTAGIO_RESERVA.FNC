create or replace
function SAN_Obter_Estagio_Reserva (	nr_seq_reserva_p	number)
 		    	return varchar2 is

ie_retorno_w			Varchar2(1);
ie_status_w			Varchar2(1);
ie_possui_hemocomp_w		Varchar2(1);
dt_coleta_receptor_w		date;
ie_transfusao_finalizada_w	Varchar2(1);
			
begin

if (nr_seq_reserva_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ie_possui_hemocomp_w
	from    san_reserva_prod
	where	nr_seq_reserva = nr_seq_reserva_p;

	select	ie_status,
		dt_coleta_receptor
	into	ie_status_w,
		dt_coleta_receptor_w
	from	san_reserva
	where	nr_sequencia = nr_seq_reserva_p;

	select	decode(count(*),0,'N','S')
	into	ie_transfusao_finalizada_w
	from	san_transfusao
	where	nr_seq_reserva = nr_seq_reserva_p
	and	ie_status = 'F';

	if (ie_possui_hemocomp_w = 'N') then
		ie_retorno_w := 'G';
	end if;

	if (ie_possui_hemocomp_w = 'S') and (dt_coleta_receptor_w is null) then
		ie_retorno_w := 'C';
	end if;

	if (ie_possui_hemocomp_w = 'S') and (dt_coleta_receptor_w is not null) then
		ie_retorno_w := 'T';
	end if;

	if (ie_status_w = 'T') and (ie_transfusao_finalizada_w = 'N') then
		ie_retorno_w := 'I';
	end if;
	
	if (ie_status_w = 'T') and (ie_transfusao_finalizada_w = 'S') then
		ie_retorno_w := 'F';
	end if;
end if;
	
return	ie_retorno_w;

end SAN_Obter_Estagio_Reserva;
/