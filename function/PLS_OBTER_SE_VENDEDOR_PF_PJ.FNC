create or replace
function pls_obter_se_vendedor_pf_pj(
		nr_seq_vendedor_canal_p	number)
 	return varchar2 is

ie_retorno_w	varchar2(2);

begin

select	substr( decode( cd_cgc, null, 'PF', 'PJ'), 1,2)
into	ie_retorno_w
from	pls_vendedor
where	nr_sequencia	= nr_seq_vendedor_canal_p;

return	ie_retorno_w;
end pls_obter_se_vendedor_pf_pj;
/