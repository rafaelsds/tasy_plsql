create or replace
function obter_se_consquim_ideal (cd_pessoa_fisica_p		varchar2,
				dt_agenda_p			date)
 		    	return varchar2 is

/*
Rotina utilizada para determinar se o tempo entre 
*/			
			
qt_agenda_w		number(1);
ds_retorno_w		varchar2(1) := 'N';
ie_existe_ag_w		number(1);

begin

select 	nvl(max(1), 0)
into	qt_agenda_w
from 	agenda a,
	agenda_consulta b,
	agenda_classif c
where 	a.cd_agenda = b.cd_agenda
and	b.cd_pessoa_fisica = cd_pessoa_fisica_p
and 	b.ie_status_agenda not in ('C', 'F', 'I', 'S')
and	b.IE_CLASSIF_AGENDA = c.CD_CLASSIFICACAO
and	c.ie_validar_poltrona = 'S'
and 	b.dt_agenda between pkg_date_utils.START_OF( dt_agenda_p , 'DD') and pkg_date_utils.END_OF( dt_agenda_p , 'DAY')
and 	b.dt_agenda+decode(b.NR_MINUTO_DURACAO, 0, 0, b.NR_MINUTO_DURACAO/24/60) between dt_agenda_p-DECODE(NVL(c.QT_TEMPO_AGENDAMENTO,0), 0, 0, c.QT_TEMPO_AGENDAMENTO/24/60) AND dt_agenda_p;

if (qt_agenda_w > 0) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w ;

end obter_se_consquim_ideal;
/