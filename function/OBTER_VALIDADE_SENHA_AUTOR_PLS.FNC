create or replace
function obter_validade_senha_autor_pls(nr_seq_agenda_w		number)
 		    	return date is
dt_validade_w		date;
nr_seq_guia_plano_w	number(10,0);

begin

begin
select	max(nr_seq_guia_plano)
into	nr_seq_guia_plano_w
from	autorizacao_convenio
where	nr_seq_agenda_consulta = nr_seq_agenda_w;
exception
when others then
	nr_seq_guia_plano_w := 0;
end;

if	(nvl(nr_seq_guia_plano_w,0)  > 0) then
	
	select	max(dt_validade_senha)
	into	dt_validade_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_plano_w;
end if;

return	dt_validade_w;

end obter_validade_senha_autor_pls;
/