create or replace
function hc_obter_turno_visualizacao (	hr_atual_p	Date,
					ie_opcao_p	Number)
 		    	return Number is
			
/*
ie_opcao_p

1 = Turno para o WCPanel da esquerda (agenda_matutino_wcp)
2 = Turno para o WCPanel da direita  (agenda_vespertino_wcp)
*/
			
nr_retorno_w		Number(10);
nr_sequencia_w		Number(10);
hr_ini_visualizacao_w	Date;
hr_fim_visualizacao_w	Date;
qt_final_w		Number(3);
qt_hr_outro_dia_w	Number(3);
ie_turno_w		Varchar2(1);
ie_matutino_w		Varchar2(1);
nr_seq_mat_w		Number(10);
ie_vespertino_w		Varchar2(1);
nr_seq_ves_w		Number(10);
ie_noturno_w		Varchar2(1);
nr_seq_not_w		Number(10);

Cursor C01 is
	select	hr_ini_visualizacao,
		nr_sequencia,
		to_number(to_char(hr_ini_visualizacao,'hh24')+qt_horas_visual),
		ie_turno
	from	hc_turno
	where	nvl(ie_situacao,'A') = 'A';
			
begin

if	((hr_atual_p is not null) and (nvl(ie_opcao_p,0) > 0)) then
	begin
	ie_matutino_w	:= 'N';
	ie_vespertino_w := 'N';
	ie_noturno_w	:= 'N';
	
	open C01;
	loop
	fetch C01 into 	
		hr_ini_visualizacao_w,
		nr_sequencia_w,
		qt_final_w,
		ie_turno_w ;
	exit when C01%notfound;
		begin 
		qt_hr_outro_dia_w := 0;
		if (qt_final_w >= 24) then
			qt_hr_outro_dia_w := qt_final_w - 24;
		end if;
		/*
		if	(to_char(hr_ini_visualizacao_w,'hh24:mi:ss') >= to_char(hr_fim_visualizacao_w,'hh24:mi:ss')) then
			
			if	(to_char(hr_ini_visualizacao_w,'hh24:mi:ss') >= to_char(hr_atual_p,'hh24:mi:ss')) then
				begin				
				hr_fim_visualizacao_w := to_date(to_char(hr_atual_p,'dd/mm/yyyy')||' '|| to_char(lpad(qt_final_w,2,'0'))||to_char(hr_ini_visualizacao_w,'mi:ss'),'dd/mm/yyyy hh24:mi:ss');
				hr_ini_visualizacao_w := to_date(to_char(hr_atual_p-1,'dd/mm/yyyy')||' '||to_char(hr_ini_visualizacao_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
				end;
			end if;
			
			if	(to_char(hr_ini_visualizacao_w,'hh24:mi:ss') <= to_char(hr_atual_p,'hh24:mi:ss')) then
				begin
				hr_fim_visualizacao_w := to_date(to_char(hr_atual_p+1,'dd/mm/yyyy')||' '|| to_char(lpad(qt_final_w,2,'0'))||to_char(hr_ini_visualizacao_w,'mi:ss'),'dd/mm/yyyy hh24:mi:ss');
				hr_ini_visualizacao_w := to_date(to_char(hr_atual_p,'dd/mm/yyyy')||' '||to_char(hr_ini_visualizacao_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
				end;	
			end if;
		else
			hr_fim_visualizacao_w := to_date(to_char(hr_atual_p,'dd/mm/yyyy')||' '|| to_char(lpad(qt_final_w,2,'0'))||to_char(hr_ini_visualizacao_w,'mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			hr_ini_visualizacao_w := to_date(to_char(hr_atual_p,'dd/mm/yyyy')||' '||to_char(hr_ini_visualizacao_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		end if;
		
		
		if 	((hr_ini_visualizacao_w < hr_atual_p)
			and (hr_fim_visualizacao_w >= hr_atual_p)) then
			*/
		if	((to_char(hr_ini_visualizacao_w,'hh24:mi:ss') <= to_char(hr_atual_p,'hh24:mi:ss'))
			and	(((qt_final_w > to_number(to_char(hr_atual_p,'hh24'))))
			or	((qt_hr_outro_dia_w >= to_number(to_char(hr_atual_p,'hh24')))
				and	(qt_final_w >= 24))))
		or	((to_char(hr_ini_visualizacao_w,'hh24:mi:ss') >= to_char(hr_atual_p,'hh24:mi:ss'))
			and	(qt_hr_outro_dia_w >= to_number(to_char(hr_atual_p,'hh24')))) then
			
			if	(ie_turno_w = 'M') then
				ie_matutino_w	:= 'S';
				nr_seq_mat_w	:= nr_sequencia_w;
			elsif	(ie_turno_w = 'V') then
				ie_vespertino_w := 'S';
				nr_seq_ves_w	:= nr_sequencia_w;
			elsif	(ie_turno_w = 'N') then
				ie_noturno_w	:= 'S';
				nr_seq_not_w	:= nr_sequencia_w;
			end if;
		end if;
		
		end;
	end loop;
	close C01;
	if	(ie_opcao_p = 1) then
		select	decode( ie_matutino_w,'S',nr_seq_mat_w,decode(ie_vespertino_w,'S',nr_seq_ves_w,decode(ie_noturno_w,'S',nr_seq_not_w,0)))
		into	nr_retorno_w
		from	dual;
	end if;
	if	(ie_opcao_p = 2) then
		select	decode( ie_noturno_w,'S',nr_seq_not_w,decode(ie_vespertino_w,'S',nr_seq_ves_w,decode(ie_matutino_w,'S',nr_seq_mat_w,0)))
		into	nr_retorno_w
		from	dual;
	end if;
	
	if	(ie_matutino_w = 'S') and (ie_noturno_w = 'S') and (ie_vespertino_w = 'N') then
	
		if	(ie_opcao_p = 1) then
			nr_retorno_w := nr_seq_not_w;
		end if;
		
		if	(ie_opcao_p = 2) then
			nr_retorno_w := nr_seq_mat_w;
		end if;	
	
	end if;
	end;
end if;

return	nr_retorno_w;

end hc_obter_turno_visualizacao;
/