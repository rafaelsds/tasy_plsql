create or replace function gpt_se_item_doc_reval_atual(	nm_tabela_p			in varchar2 default null,
													nr_seq_cpoe_p		in number,
													ie_gpt_item_p		in varchar2 default null,
													ie_opcao_p			in int,
													nm_usuario_nrec_p 	in varchar2 default null,
													ie_tipo_item_p 		in varchar2 default null,
													ie_tipo_grupo_p 	in varchar2 default null)
				return varchar2 is

/*
ie_opcao_p:
1 - Retorna tipo do item se pendente
2 - Retorna se esta pendente (S)
*/

ds_retorno_w			varchar2(5 char) := 'XPTO';
ie_profissional_w		usuario.ie_profissional%type;
ie_dados_usuario_w		usuario.ie_tipo_evolucao%type;
cd_perfil_w				perfil.cd_perfil%type;

begin

if	(nm_tabela_p is not null or ie_gpt_item_p is not null) and 
	(nr_seq_cpoe_p is not null) and
	(ie_opcao_p is not null) then

	if (nm_usuario_nrec_p is not null) then

		ie_profissional_w := gpt_obter_Profissional_Usuario(nm_usuario_nrec_p);
		ie_dados_usuario_w := Obter_dados_usuario_opcao(nm_usuario_nrec_p, 'F');
		cd_perfil_w := wheb_usuario_pck.get_cd_perfil;

		if(('S' = gpt_obter_se_apresenta_grupo(cd_perfil_w, ie_tipo_grupo_p)) and (
			(((ie_profissional_w is not null) and  gpt_funcao_prescr_liberada(cd_perfil_w ,ie_profissional_w) = 'S') or
			((ie_profissional_w is null) and gpt_funcao_prescr_liberada(cd_perfil_w ,ie_dados_usuario_w) = 'S')))) then

				if (ie_opcao_p = 1) then
					ds_retorno_w := ie_tipo_item_p;
				else
					ds_retorno_w := 'S';
				end if;

		end if;

	end if;

end if;

	return ds_retorno_w;

end gpt_se_item_doc_reval_atual;
/
