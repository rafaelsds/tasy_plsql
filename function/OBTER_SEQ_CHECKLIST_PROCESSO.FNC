create or replace 
function obter_seq_checklist_processo(nr_seq_check_item_p	number) return number is

nr_seq_checklist_w 	number(10);

begin

if (nr_seq_check_item_p is not null) then
	SELECT	MAX(b.nr_sequencia)
	into	nr_seq_checklist_w
	FROM  	CHECKLIST_PROCESSO_ITEM a, 
			CHECKLIST_PROCESSO b
	WHERE	b.nr_sequencia = a.nr_seq_checklist
	AND		a.nr_sequencia = nr_seq_check_item_p;
end if;

return nr_seq_checklist_w;

end obter_seq_checklist_processo;
/

