create or replace
function obter_dados_bco_ocorr_escr_ret
			(	nr_seq_ocorr_ret_p		number,
				ie_opcao_p			varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);

begin


if	(ie_opcao_p = 'C') then
	select	max(cd_ocorrencia)
	into	ds_retorno_w
	from	banco_ocorr_escrit_ret
	where	nr_sequencia	= nr_seq_ocorr_ret_p;
elsif	(ie_opcao_p = 'D') then
	select	max(ds_ocorrencia)
	into	ds_retorno_w
	from	banco_ocorr_escrit_ret
	where	nr_sequencia	= nr_seq_ocorr_ret_p;
elsif	(ie_opcao_p = 'I') then
	select	max(ie_rejeitado)
	into	ds_retorno_w
	from	banco_ocorr_escrit_ret
	where	nr_sequencia	= nr_seq_ocorr_ret_p;
end if;

return	ds_retorno_w;

end obter_dados_bco_ocorr_escr_ret;
/