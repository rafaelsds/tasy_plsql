create or replace
function Obter_se_exibe_grupo(	nr_seq_grupo_p			number,
								cd_perfil_p				number,
								cd_setor_atendimento_p	number) return varchar2 is

ie_retorno_w			char(1) := 'S';
nr_seq_agrupamento_w	number(10);

cursor c01 is 
select	nvl(a.ie_incluir,'S')
from	grupo_exame_rotina_lib a
where	a.nr_seq_grupo = nr_seq_grupo_p
and		nvl(a.cd_perfil,cd_perfil_p) = cd_perfil_p
and		((cd_setor_atendimento is null) or (cd_setor_atendimento = cd_setor_atendimento_p))
and		nvl(a.nr_seq_agrupamento,nr_seq_agrupamento_w) = nr_seq_agrupamento_w
order by
	nvl(a.cd_perfil,0),
	nvl(a.cd_setor_atendimento,0),
	nvl(a.nr_seq_agrupamento,0);

begin

Select	nvl(max('N'),'S')
into	ie_retorno_w
from	grupo_exame_rotina_lib
where	rownum = 1
and		nr_seq_grupo = nr_seq_grupo_p;

if	(ie_retorno_w = 'N') then
	nr_seq_agrupamento_w	:= obter_agrupamento_setor(cd_setor_atendimento_p);
open c01;
loop
fetch c01 into ie_retorno_w;
exit when c01%notfound;
	ie_retorno_w := ie_retorno_w;
end loop;
close c01;
end if;
	
return	ie_retorno_w;

end Obter_se_exibe_grupo;
/
