create or replace
function pls_obter_status_repasse
			(	nr_seq_repasse_p	number)
 		    	return varchar2 is
			
/*

LT - Titulo liquidado
LI- Liberado pelo us�ario
AB - Aberto
*/			

ie_retorno_w		varchar2(2);
dt_liberacao_w		date;
nr_titulo_w		number(10);
qt_liquidado_w		number(10);

begin

select	substr(pls_obter_titulo_mensalidade(null,nr_seq_mens_seg),1,10),
	dt_liberacao
into	nr_titulo_w,
	dt_liberacao_w
from	pls_repasse_mens
where	nr_sequencia	= nr_seq_repasse_p;

if	(dt_liberacao_w is not null) then
	ie_retorno_w	:= 'LI';
elsif	(nr_titulo_w	is not null) then
	select	count(*)
	into	qt_liquidado_w
	from	titulo_receber
	where	ie_situacao	= '2'
	and	nr_titulo	= nr_titulo_w;
	
	if	(qt_liquidado_w	> 0) then
		ie_retorno_w	:= 'LT';
	else
		ie_retorno_w	:= 'AB';
	end if;	
else
	ie_retorno_w	:= 'AB';
end if;	

return	ie_retorno_w;

end pls_obter_status_repasse;
/
