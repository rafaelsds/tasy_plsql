create or replace function get_if_use_episode( cd_pessoa_fisica_p 	in episodio_paciente.cd_pessoa_fisica%type)
        return varchar2 is

ds_return_w 	varchar2(1);
is_use_case_w    varchar2(1);

begin

ds_return_w := 'S';

select  obter_uso_case(wheb_usuario_pck.get_nm_usuario)
into    is_use_case_w
from    dual;

if (cd_pessoa_fisica_p is not null and is_use_case_w = 'S') then
	select	nvl(max('S'),'N')
	into	ds_return_w
	from	episodio_paciente a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
    and dt_cancelamento is null;
end if;

return	ds_return_w;

end get_if_use_episode;
/
