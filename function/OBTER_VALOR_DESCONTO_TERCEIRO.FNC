create or replace
function OBTER_VALOR_DESCONTO_TERCEIRO(nr_repasse_terceiro_p number)
 		    	return number is

vl_retorno_w		number(15,2) := 0;			
			
begin

if	(nr_repasse_terceiro_p is not null) then
	
	select 	nvl(sum(x.vl_repasse),0)
	into	vl_retorno_w
	from 	repasse_terceiro_item x
	where 	x.nr_repasse_terceiro = nr_repasse_terceiro_p
	and	x.vl_repasse < 0;

end if;

return	vl_retorno_w;

end OBTER_VALOR_DESCONTO_TERCEIRO;
/