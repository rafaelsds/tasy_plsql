create or replace
function pls_coparticapaco_rdc_unimed
			(	nr_seq_plano_p		number,
				ie_opcao_p		varchar2)
	 		    	return number is

/* ie_opcao_p

T - Taxa percentual da coparticipacao ambulatorial
V - Valor diária da coparticipação do tipo internação

*/				
				
vl_retorno_w			number(5);
tx_co_participacao_amb_w	number(2);
vl_co_participacao_int_w	number(5);

			
begin

select	max(substr(decode(ie_tipo_atendimento,'E',nvl(tx_coparticipacao,0),0),1,2)),
	max(substr(decode(ie_tipo_atendimento,'I',nvl(vl_coparticipacao,0),0),1,5))
into	tx_co_participacao_amb_w,
	vl_co_participacao_int_w
from	pls_regra_coparticipacao
where	nr_seq_plano	= nr_seq_plano_p;

if	(ie_opcao_p = 'T') then
	vl_retorno_w := tx_co_participacao_amb_w;
elsif	(ie_opcao_p = 'V') then	
	vl_retorno_w := vl_co_participacao_int_w;
end if;

if	(ie_opcao_p = 'T') then	
	return	tx_co_participacao_amb_w;
elsif	(ie_opcao_p = 'V') then	
	return	vl_retorno_w;
end if;	

end pls_coparticapaco_rdc_unimed;
/
