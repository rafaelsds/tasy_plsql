create or replace
function eis_obter_faixa_etaria_mx(qt_idade_p number)
					return varchar2 is
begin
if	(qt_idade_p > 64) then
	return '65 y >';
elsif	(qt_idade_p > 59) then
	return '060 064';
elsif	(qt_idade_p > 49) then
	return '050 059';
elsif	(qt_idade_p > 44) then
	return '045 049';
elsif	(qt_idade_p > 24) then
	return '025 044';
elsif	(qt_idade_p > 19) then
	return '020 024';
elsif	(qt_idade_p > 14) then
	return '015 019';
elsif	(qt_idade_p > 9) then
	return '010 014';
elsif	(qt_idade_p > 4) then
	return '005 009';
elsif	(qt_idade_p > 0) then
	return '001 004';
elsif	(qt_idade_p >= 0) then
	return '< de 1 a�o';
else
	return 'Ign';
end if;

end eis_obter_faixa_etaria_mx;
/