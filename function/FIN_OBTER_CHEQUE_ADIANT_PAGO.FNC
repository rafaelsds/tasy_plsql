create or replace 
function fin_obter_cheque_adiant_pago(nr_adiant_pago_p	number) 
return varchar is

nr_cheque_adiant_pago_w varchar2(255);

begin

nr_cheque_adiant_pago_w         := '';

for rec in (select      b.nr_cheque
            from        adiantamento_pago_cheque a,
                        cheque b
            where	a.nr_adiantamento       = nr_adiant_pago_p
            and	        a.nr_seq_cheque         = b.nr_sequencia
            and	        b.dt_cancelamento       is null
            order by    1)
loop
        if	(nr_cheque_adiant_pago_w is null) then
                nr_cheque_adiant_pago_w	:= substr(rec.nr_cheque, 1, 255);
	else
                nr_cheque_adiant_pago_w	:= substr(nr_cheque_adiant_pago_w || ', ' || rec.nr_cheque, 1, 255);
	end if;
end loop;

return	nr_cheque_adiant_pago_w;

end fin_obter_cheque_adiant_pago;
/