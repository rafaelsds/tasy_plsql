CREATE OR REPLACE
FUNCTION Obter_Equip_Exame_online(	nr_seq_exame_p		number,
					cd_equipamento_p	number,
					ie_opcao_p		varchar2,
					ie_tipo_atendimento_p	number,
					cd_estabelecimento_p	number)
					RETURN Varchar2 IS

cd_equipamento_w		number(5);
ds_sigla_w			varchar2(10);
ds_equipamento_w		varchar2(50);
cd_exame_equip_w		varchar2(20);
cd_exame_equip_integr_w		varchar2(20);
Resultado_w	 		Varchar2(50);
nr_dia_semana_w 		number(2);

BEGIN

/* Op��es
	C	- C�digo Equipamento
	S	- Sigla Equipamento
	D	- Descri��o Equipamento
	CI	- C�digo Integra��o
	Caso nao seja uma destas op��es busca o equipamento para a sigla
*/

nr_dia_semana_w := pkg_date_utils.get_WeekDay(sysdate);

select 	nvl(max(a.cd_equipamento),null),
	nvl(max(b.ds_sigla),null),
	nvl(max(b.ds_equipamento),null),
	nvl(max(a.cd_exame_equip),null)
into	cd_equipamento_w,
	ds_sigla_w,
	ds_equipamento_w,
	cd_exame_equip_w
from	equipamento_lab b,
	exame_lab_equip_regra c,
	lab_exame_equip a
where a.cd_equipamento 	= b.cd_equipamento
  and a.nr_sequencia	= c.nr_seq_exame_equip
  and a.nr_seq_exame 	= nr_seq_exame_p
  and ((a.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,a.ie_tipo_atendimento)) or (a.ie_tipo_atendimento is null))
  and a.cd_equipamento	= nvl(cd_equipamento_p,a.cd_equipamento)
  and c.cd_estabelecimento = cd_estabelecimento_p
  and c.ie_dia_semana	= nr_dia_semana_w
  and	sysdate	>= to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || c.ds_hora_inicio || ':00','dd/mm/yyyy hh24:mi:ss')
  and	sysdate	<= to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || c.ds_hora_fim || ':00','dd/mm/yyyy hh24:mi:ss')  
  AND TO_NUMBER(TO_CHAR(SYSDATE,'hh24')) >= TO_NUMBER(TO_CHAR(TO_DATE(c.ds_hora_inicio,'hh24:mi'),'hh24'))
  and nvl(DS_FORMA_ENVIO_EQUIP,'X') <> 'BCO'
  AND TO_NUMBER(TO_CHAR(SYSDATE,'hh24')) <= TO_NUMBER(TO_CHAR(TO_DATE(c.ds_hora_fim,'hh24:mi'),'hh24'));
 
if	(cd_equipamento_w is null) then
	select 	nvl(max(a.cd_equipamento),null),
		nvl(max(b.ds_sigla),null),
		nvl(max(b.ds_equipamento),null),
		nvl(max(a.cd_exame_equip),null)
	into	cd_equipamento_w,
		ds_sigla_w,
		ds_equipamento_w,
		cd_exame_equip_w
	from	equipamento_lab b,
		exame_lab_equip_regra c,
		lab_exame_equip a
	where a.cd_equipamento 	= b.cd_equipamento
	  and a.nr_sequencia	= c.nr_seq_exame_equip
	  and nvl(DS_FORMA_ENVIO_EQUIP,'X') <> 'BCO'
	  and a.nr_seq_exame 	= nr_seq_exame_p
	  and a.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,a.ie_tipo_atendimento)
	  and a.cd_equipamento	= nvl(cd_equipamento_p,a.cd_equipamento)
	  and c.cd_estabelecimento = cd_estabelecimento_p;
	  
	if	(cd_equipamento_w is null) then
  
		select 	nvl(max(a.cd_equipamento),null),
			nvl(max(b.ds_sigla),null),
			nvl(max(b.ds_equipamento),null),
			nvl(max(a.cd_exame_equip),null)
		into	cd_equipamento_w,
			ds_sigla_w,
			ds_equipamento_w,
			cd_exame_equip_w
		from	equipamento_lab b,
			lab_exame_equip a
		where a.cd_equipamento 	= b.cd_equipamento
		and nvl(DS_FORMA_ENVIO_EQUIP,'X') <> 'BCO'
		and a.nr_seq_exame 	= nr_seq_exame_p
		and a.cd_equipamento	= nvl(cd_equipamento_p,a.cd_equipamento);
	end if;
		
end if;

if    (ie_opcao_p = 'C') then
	resultado_w := cd_equipamento_w;
elsif (ie_opcao_p = 'S') then
	resultado_w := ds_sigla_w;
elsif (ie_opcao_p = 'D') then
	resultado_w := ds_equipamento_w;
elsif (ie_opcao_p = 'CI') then
	resultado_w := cd_exame_equip_w;
else
	select nvl(max(b.cd_exame_equip),null)
	into cd_exame_equip_integr_w
	from equipamento_lab a,
		exame_lab_equip_regra c,
	     lab_exame_equip b	
        where a.cd_equipamento	= b.cd_equipamento 
         and b.nr_sequencia	= c.nr_seq_exame_equip
	 and b.nr_seq_exame 	= nr_seq_exame_p
	 and a.ds_sigla 	= ie_opcao_p
	 and nvl(DS_FORMA_ENVIO_EQUIP,'X') <> 'BCO'
	 and b.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,b.ie_tipo_atendimento)
	 and c.cd_estabelecimento = cd_estabelecimento_p
	 and NVL(c.ie_dia_semana, nr_dia_semana_w)	= nr_dia_semana_w
	 and sysdate	>= to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || nvl(c.ds_hora_inicio,to_char(sysdate,'hh24:mi')) || ':00','dd/mm/yyyy hh24:mi:ss')
	 and sysdate	<= to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || nvl(c.ds_hora_inicio,to_char(sysdate,'hh24:mi')) || ':00','dd/mm/yyyy hh24:mi:ss')  
	 and ds_hora_inicio is not null
	 and ds_hora_fim is not null;
	 
	if	(cd_exame_equip_integr_w is null) then
	
		select nvl(max(b.cd_exame_equip),null)
		into cd_exame_equip_integr_w
		from equipamento_lab a,
			exame_lab_equip_regra c,
		     lab_exame_equip b	
	        where a.cd_equipamento	= b.cd_equipamento 
	         and b.nr_sequencia	= c.nr_seq_exame_equip
		 and b.nr_seq_exame 	= nr_seq_exame_p
		 and a.ds_sigla 	= ie_opcao_p
		 and b.ie_tipo_atendimento is null
		 and nvl(DS_FORMA_ENVIO_EQUIP,'X') <> 'BCO'
		 and c.cd_estabelecimento = cd_estabelecimento_p
		 and NVL(c.ie_dia_semana, nr_dia_semana_w)	= nr_dia_semana_w
		 and sysdate	>= to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || nvl(c.ds_hora_inicio,to_char(sysdate,'hh24:mi')) || ':00','dd/mm/yyyy hh24:mi:ss')
		 and sysdate	<= to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || nvl(c.ds_hora_inicio,to_char(sysdate,'hh24:mi')) || ':00','dd/mm/yyyy hh24:mi:ss')  
		 and ds_hora_inicio is not null
		 and ds_hora_fim is not null;
	
	
		if	(cd_exame_equip_integr_w is null) then
			select 	nvl(max(b.cd_exame_equip),null)
			into 	cd_exame_equip_integr_w
			from 	exame_lab_equip_regra c,
				equipamento_lab a,
				lab_exame_equip b	
			where a.cd_equipamento	= b.cd_equipamento 
			and b.nr_sequencia	= c.nr_seq_exame_equip
			and b.nr_seq_exame 	= nr_seq_exame_p
			and c.cd_estabelecimento = cd_estabelecimento_p
			and nvl(nvl(DS_FORMA_ENVIO_EQUIP,'X'),'X') <> 'BCO'
			and (b.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,b.ie_tipo_atendimento))
			and a.ds_sigla 	= ie_opcao_p
			and ds_hora_inicio is null
			and ds_hora_fim is null;
			
			if	(cd_exame_equip_integr_w is null) then	
			
				select 	nvl(max(b.cd_exame_equip),null)
				into 	cd_exame_equip_integr_w
				from 	exame_lab_equip_regra c,
					equipamento_lab a,
					lab_exame_equip b	
				where a.cd_equipamento	= b.cd_equipamento 
				and b.nr_sequencia	= c.nr_seq_exame_equip
				and b.nr_seq_exame 	= nr_seq_exame_p
				and c.cd_estabelecimento = cd_estabelecimento_p
				and b.ie_tipo_atendimento is null
				and a.ds_sigla 	= ie_opcao_p
				and nvl(nvl(DS_FORMA_ENVIO_EQUIP,'X'),'X') <> 'BCO'
				and ds_hora_inicio is null				
				and ds_hora_fim is null;

				if	(cd_exame_equip_integr_w is null) then
					select 	nvl(max(b.cd_exame_equip),null)
					into 	cd_exame_equip_integr_w
					from 	equipamento_lab a,
						lab_exame_equip b	
					where a.cd_equipamento	= b.cd_equipamento 
					and b.nr_seq_exame 	= nr_seq_exame_p
					and nvl(nvl(DS_FORMA_ENVIO_EQUIP,'X'),'X') <> 'BCO'
					and b.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,b.ie_tipo_atendimento)
					and a.ds_sigla 	= ie_opcao_p;
					
					if	(cd_exame_equip_integr_w is null) then
						select 	nvl(max(b.cd_exame_equip),null)
						into 	cd_exame_equip_integr_w
						from 	equipamento_lab a,
							lab_exame_equip b	
						where a.cd_equipamento	= b.cd_equipamento 
						and b.nr_seq_exame 	= nr_seq_exame_p
						and nvl(nvl(DS_FORMA_ENVIO_EQUIP,'X'),'X') <> 'BCO'
						and b.ie_tipo_atendimento is null
						and a.ds_sigla 	= ie_opcao_p;
					end if;
				end if;
			end if;
		end if;
	end if;
	resultado_w := cd_exame_equip_integr_w;
end if;

RETURN resultado_w;

END Obter_Equip_Exame_online;
/
