create or replace
function Sus_Obter_Espec_Leito(nr_seq_espec_leito_p		Number,
				ie_tipo_retorno_p		Varchar2)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(255);
cd_espec_leito_w	Number(10);
ds_espec_leito_w	Varchar2(60);
			
begin

select	max(cd_espec_leito),
	max(ds_espec_leito)
into	cd_espec_leito_w,
	ds_espec_leito_w
from	sus_espec_leito
where	nr_sequencia	= nr_seq_espec_leito_p;

if	(ie_tipo_retorno_p = 'C') then
	ds_retorno_w	:= cd_espec_leito_w;
else
	ds_retorno_w	:= ds_espec_leito_w;
end if;

return	ds_retorno_w;

end Sus_Obter_Espec_Leito;
/