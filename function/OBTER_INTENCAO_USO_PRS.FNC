create or replace
function obter_intencao_uso_prs(
			nr_sequencia_p	number,
			ie_opcao_p	varchar2 default null)
 		    	return varchar2 is

nr_sequencia_w		reg_intencao_uso.nr_sequencia%type;
ds_intencao_uso_w	reg_intencao_uso.ds_intencao_uso%type;

/*	ie_opcao_p
	S - Sequencia da intencao de uso	
	D - Descricao da intencao de uso	*/

begin

if	(nr_sequencia_p > 0) then
	begin
	select	e.nr_sequencia,
		e.ds_intencao_uso
	into	nr_sequencia_w,
		ds_intencao_uso_w
	from	reg_intencao_uso e,
		reg_area_customer d,
		reg_features_customer c,
		reg_customer_requirement b,
		reg_product_requirement a
	where	e.nr_sequencia = d.nr_seq_intencao_uso
	and	d.nr_sequencia = c.nr_seq_area_customer
	and	c.nr_sequencia = b.nr_seq_features
	and	b.nr_sequencia = a.nr_customer_requirement
	and	a.nr_sequencia = nr_sequencia_p;
	
	if	(ie_opcao_p = 'D') then
		return ds_intencao_uso_w;	
	else
		return nr_sequencia_w;
	end if;
	end;
end if;

return null;

end obter_intencao_uso_prs;
/