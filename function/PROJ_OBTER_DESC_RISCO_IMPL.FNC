create or replace
function proj_obter_desc_risco_impl(nr_seq_risco_p	number)
					return varchar2 is

ds_titulo_w	varchar2(80);

begin

if	(nr_seq_risco_p is not null) then
	select	ds_titulo
	into	ds_titulo_w
	from	proj_risco_implantacao
	where	nr_sequencia = nr_seq_risco_p;
end if;

return	ds_titulo_w;
end proj_obter_desc_risco_impl;
/
