create or replace
Function Obter_se_auditado( 	nr_seq_auditoria_p 	Number,
				cd_item_p 		Number,
				cd_setor_atendimento_p 	Number,
				dt_conta_p 		Date,
				vl_unitario_p		Number,
				ie_tipo_item_p		Number,
				ie_tipo_ajuste_p	Varchar2,
				cd_material_tiss_p		Varchar2 Default Null)
 		    	return varchar2 is

ie_auditado_w	varchar2(1):= 'N';
qt_auditado_w	number(10,0);
			
begin

if	(nvl(ie_tipo_ajuste_p,'DIA') = 'DIA') then

	if (ie_tipo_item_p = 1 ) then
		select 	count(*)
		into	qt_auditado_w
		from 	auditoria_matpaci a,
			material_atend_paciente b
		where	a.nr_seq_matpaci = b.nr_sequencia
		and	a.ie_tipo_auditoria = 'D'
		and	a.nr_seq_auditoria = nr_seq_auditoria_p
		and	b.cd_material = cd_item_p
		and	nvl((select max(x.cd_setor_atendimento) from atend_paciente_unidade x where x.nr_seq_interno = a.nr_seq_atepacu_ajuste),b.cd_setor_atendimento) = cd_setor_atendimento_p
		and	trunc(b.dt_conta) = trunc(dt_conta_p);
	elsif (ie_tipo_item_p = 2 ) then
		select 	count(*)
		into	qt_auditado_w
		from 	auditoria_propaci a,
			procedimento_paciente b
		where	a.nr_seq_propaci = b.nr_sequencia
		and	a.ie_tipo_auditoria = 'D'
		and	a.nr_seq_auditoria = nr_seq_auditoria_p
		and	b.cd_procedimento = cd_item_p
		and	nvl((select max(x.cd_setor_atendimento) from atend_paciente_unidade x where x.nr_seq_interno = a.nr_seq_atepacu_ajuste),b.cd_setor_atendimento) = cd_setor_atendimento_p
		and	trunc(b.dt_conta) = trunc(dt_conta_p);
	end if;
	
elsif	(nvl(ie_tipo_ajuste_p,'DIA') = 'SETOR') then

	if (ie_tipo_item_p = 1 ) then
		select 	count(*)
		into	qt_auditado_w
		from 	auditoria_matpaci a,
			material_atend_paciente b
		where	a.nr_seq_matpaci = b.nr_sequencia
		and	a.ie_tipo_auditoria = 'D'
		and	a.nr_seq_auditoria = nr_seq_auditoria_p
		and	b.cd_material = cd_item_p
		and	nvl((select max(x.cd_setor_atendimento) from atend_paciente_unidade x where x.nr_seq_interno = a.nr_seq_atepacu_ajuste),b.cd_setor_atendimento) = cd_setor_atendimento_p
		and	b.vl_unitario = vl_unitario_p
		and	((b.cd_material_tiss = cd_material_tiss_p) or (cd_material_tiss_p Is Null));		
	elsif (ie_tipo_item_p = 2 ) then
		select 	count(*)
		into	qt_auditado_w
		from 	auditoria_propaci a,
			procedimento_paciente b
		where	a.nr_seq_propaci = b.nr_sequencia
		and	a.ie_tipo_auditoria = 'D'
		and	a.nr_seq_auditoria = nr_seq_auditoria_p
		and	b.cd_procedimento = cd_item_p
		and	nvl((select max(x.cd_setor_atendimento) from atend_paciente_unidade x where x.nr_seq_interno = a.nr_seq_atepacu_ajuste),b.cd_setor_atendimento) = cd_setor_atendimento_p
		and	trunc(b.dt_conta) = trunc(dt_conta_p);
	end if;

end if;		
	
if	(qt_auditado_w > 0) then
	ie_auditado_w:= 'S';
end if;

return	ie_auditado_w;

end Obter_se_auditado;
/