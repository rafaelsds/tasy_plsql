create or replace
function pls_obter_tempo_op_total
			(	nr_seq_operador_p	number,
				dt_inicio_p		date,
				dt_fim_p		date)
				return number is

nr_retorno		number(20,2);
qt_atend_w		number(20);
qt_tempo_w		number(20);

begin

select	count(nr_sequencia) valor
into	qt_atend_w
from	pls_atendimento
where	nr_seq_operador = nr_seq_operador_p
and	Trunc(dt_inicio) between trunc(dt_inicio_p) and fim_dia(dt_fim_p);

select	sum(b.qt_tempo_atendimento)
into	qt_tempo_w
from	pls_atendimento_operador b
where	nr_seq_operador = nr_seq_operador_p
and	Trunc(b.dt_inicio_atendimento) between trunc(dt_inicio_p) and fim_dia(dt_fim_p);

nr_retorno := ((qt_tempo_w /qt_atend_w) / 60);

return	nr_retorno;

end pls_obter_tempo_op_total;
/
