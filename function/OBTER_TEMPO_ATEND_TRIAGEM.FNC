create or replace function obter_tempo_atend_triagem (
    nr_seq_classif_p	number
) return number as

qt_min_espera_w				number(10);
immediate_resuscitation_w 	triagem_classif_risco.ie_classif_manchester%type :=	'V';
less_urgent_w 				triagem_classif_risco.ie_classif_manchester%type :=	'VE';
non_urgent_w 				triagem_classif_risco.ie_classif_manchester%type :=	'AZ';
urgente_w 					triagem_classif_risco.ie_classif_manchester%type :=	'A';
very_urgent_w 				triagem_classif_risco.ie_classif_manchester%type :=	'L';
ie_classif_manchester_w		triagem_classif_risco.ie_classif_manchester%type;

begin

	select 	max(ie_classif_manchester)
	into	ie_classif_manchester_w
	from 	triagem_classif_risco
	where 	IE_SITUACAO = 'A'
	and 	nr_sequencia = nr_seq_classif_p;

	if (ie_classif_manchester_w = immediate_resuscitation_w) then
		qt_min_espera_w := 0;
	elsif (ie_classif_manchester_w = very_urgent_w) then
		qt_min_espera_w := 10;
	elsif (ie_classif_manchester_w = urgente_w) then
		qt_min_espera_w := 30;
	elsif (ie_classif_manchester_w = less_urgent_w) then
		qt_min_espera_w := 90;
	elsif (ie_classif_manchester_w = non_urgent_w) then
		qt_min_espera_w := 120;
	end if;

	return qt_min_espera_w;
	
end obter_tempo_atend_triagem;
/
