create or replace
function spa_obter_dados_negociacao(	nr_seq_spa_p		number,
					cd_forma_negociacao_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

vl_negociado_w		spa_negociacao.vl_negociado%type := 0;
ds_retorno_w		Varchar2(200) := '';
begin

if (nvl(nr_seq_spa_p,0) > 0) then
	if ( ie_opcao_p = 'V') then
		select 	sum(nvl(vl_negociado,0))
		into   	vl_negociado_w
		from   	spa_negociacao
		where  	cd_forma_negociacao 	= cd_forma_negociacao_p
		and  	nr_seq_spa 		= nr_seq_spa_p;
		
		ds_retorno_w :=  to_char(vl_negociado_w);
	end if;
end if;

return	ds_retorno_w;

end spa_obter_dados_negociacao;
/