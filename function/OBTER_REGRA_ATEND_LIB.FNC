create or replace
function Obter_regra_atend_lib
		(cd_perfil_p 			number,
		nr_sequencia_p 			number,
		cd_estabelecimento_p 	number,
		ie_clinica_p		number default null,
		ie_tipo_atendimento_p	number default null,
		nr_seq_agenda_p		number default null)
		return varchar is
		
ds_retorno_w	  	 varchar2(1):= 'S';
qt_regra_perfil_w	 number(10);
qt_regra_perfil_lib_w	 number(10);
ie_clinica_w		 number(10);
ie_tipo_atendimento_w	 number(3);
nr_seq_agenda_w		 number(10);
qt_regra_especialidade_w number(10);
		
begin
ie_clinica_w	:= nvl(ie_clinica_p,0);
ie_tipo_atendimento_w := nvl(ie_tipo_atendimento_p,0);
nr_seq_agenda_w := nvl(nr_seq_agenda_p,0);

select 	count(*)
into	qt_regra_especialidade_w
from 	classificacao_atend_lib
where 	nvl(cd_perfil,cd_perfil_P) = cd_perfil_P
and	nvl(ie_clinica,ie_clinica_w)	= ie_clinica_w
and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w
and	cd_especialidade = Obter_Especialidade_agenda(Obter_codigo_agenda(nr_seq_agenda_w));

if (nr_seq_agenda_w = 0) or (qt_regra_especialidade_w = 0) then

select 	count(*)
into	qt_regra_perfil_w
from 	classificacao_atend_lib
where 	nvl(cd_perfil,cd_perfil_P) = cd_perfil_P
and	nvl(ie_clinica,ie_clinica_w)	= ie_clinica_w
and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w;

	if (qt_regra_perfil_w > 0) then
		select 	count(*)
		into	qt_regra_perfil_lib_w
		from 	classificacao_atend_lib
		where 	nvl(cd_perfil,cd_perfil_P) = cd_perfil_P
		and 	nr_seq_classificacao = nr_sequencia_p
		and	nvl(ie_clinica,ie_clinica_w)	= ie_clinica_w
		and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w;
	end if;	
else

select 	count(*)
into	qt_regra_perfil_w
from 	classificacao_atend_lib
where 	nvl(cd_perfil,cd_perfil_P) = cd_perfil_P
and	nvl(ie_clinica,ie_clinica_w)	= ie_clinica_w
and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w
and	cd_especialidade = Obter_Especialidade_agenda(Obter_codigo_agenda(nr_seq_agenda_w));

	if (qt_regra_perfil_w > 0) then
		select 	count(*)
		into	qt_regra_perfil_lib_w
		from 	classificacao_atend_lib
		where 	nvl(cd_perfil,cd_perfil_P) = cd_perfil_P
		and 	nr_seq_classificacao = nr_sequencia_p
		and	nvl(ie_clinica,ie_clinica_w)	= ie_clinica_w
		and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w
		and	cd_especialidade = Obter_Especialidade_agenda(Obter_codigo_agenda(nr_seq_agenda_w));
	end if;
end if;


if	(qt_regra_perfil_w > 0) and
	(qt_regra_perfil_lib_w = 0) then
	ds_retorno_w	:= 'N';
else
	ds_retorno_w	:= 'S';
end if;



return	ds_retorno_w;

end Obter_regra_atend_lib;
/
