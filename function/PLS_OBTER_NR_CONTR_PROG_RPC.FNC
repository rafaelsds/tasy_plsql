create or replace
function pls_obter_nr_contr_prog_rpc
			(	nr_seq_prog_reaj_p	number)
 		    	return varchar2 is

ds_retorno_w			varchar2(255);
tx_reajuste_programado_w	pls_prog_reaj_coletivo.tx_reajuste_programado%type;
dt_reajuste_w			pls_prog_reaj_coletivo.dt_reajuste%type;
ie_numero_contrato_w		pls_regra_rpc.ie_numero_contrato%type;
nr_seq_contrato_w		pls_prog_reaj_coletivo.nr_seq_contrato%type;
cd_cgc_outorgante_w		pls_outorgante.cd_cgc_outorgante%type;
cd_cooperativa_w		pls_congenere.cd_cooperativa%type;
nr_contrato_w			pls_contrato.nr_contrato%type;
cd_operadora_empresa_w		pls_contrato.cd_operadora_empresa%type;
cd_cod_anterior_w		pls_contrato.cd_cod_anterior%type;

begin

select	nvl(tx_reajuste_programado,0),
	dt_reajuste,
	nr_seq_contrato
into	tx_reajuste_programado_w,
	dt_reajuste_w,
	nr_seq_contrato_w
from	pls_prog_reaj_coletivo
where	nr_sequencia	= nr_seq_prog_reaj_p;

select	max(ie_numero_contrato)
into	ie_numero_contrato_w
from	pls_regra_rpc
where	dt_reajuste_w between nvl(dt_inicio_vigencia,dt_reajuste_w) and nvl(dt_fim_vigencia,dt_reajuste_w)
and	tx_reajuste_programado_w between nvl(pr_reajuste_inicial,tx_reajuste_programado_w) and nvl(pr_reajuste_final,tx_reajuste_programado_w);

select	max(nr_contrato),
	max(cd_operadora_empresa),
	max(cd_cod_anterior)
into	nr_contrato_w,
	cd_operadora_empresa_w,
	cd_cod_anterior_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_w;

if	(ie_numero_contrato_w is not null) then
	select	max(cd_cgc_outorgante)
	into	cd_cgc_outorgante_w
	from	pls_outorgante;
	
	select	max(cd_cooperativa)
	into	cd_cooperativa_w
	from	pls_congenere
	where	cd_cgc	= cd_cgc_outorgante_w;
	
	if	(nvl(ie_numero_contrato_w,'C')	= 'C') then
		ds_retorno_w	:= nr_contrato_w;
	elsif	(nvl(ie_numero_contrato_w,'C')	= 'CC') then
		ds_retorno_w	:= cd_cooperativa_w || nr_contrato_w;
	elsif	(nvl(ie_numero_contrato_w,'C')	= 'CE') then
		ds_retorno_w	:= to_number(cd_cooperativa_w) ||' '|| lpad(cd_operadora_empresa_w,4,'0');
	elsif	(nvl(ie_numero_contrato_w,'C')	= 'CA') then
		ds_retorno_w	:= nvl(cd_cod_anterior_w,nr_contrato_w);
	end if;
else
	ds_retorno_w	:= nr_contrato_w;
end if;

return	ds_retorno_w;

end pls_obter_nr_contr_prog_rpc;
/