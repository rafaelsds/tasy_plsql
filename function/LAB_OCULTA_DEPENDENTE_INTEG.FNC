create or replace
function lab_oculta_dependente_integ(	nr_prescricao_p	number,
					nr_seq_prescr_p	number)
 		    	return varchar2 is
			
nr_seq_superior_w	prescr_procedimento.nr_seq_superior%type;
nr_seq_exame_w		prescr_procedimento.nr_seq_exame%type;
ie_oculta_integr_w	exame_lab_dependente.ie_oculta_integr%type;
nr_seq_material_w	material_exame_lab.nr_sequencia%type;
nr_seq_material_princ_w	material_exame_lab.nr_sequencia%type;
cd_setor_atendimento_w	prescr_procedimento.cd_setor_atendimento%type;
cd_setor_prescricao_w	prescr_medica.cd_setor_atendimento%type;
cd_convenio_w		atend_categoria_convenio.cd_convenio%type;
nr_atendimento_w	prescr_medica.nr_atendimento%type;
dt_integracao_w		prescr_procedimento.dt_integracao%type;
cd_motivo_baixa_w	prescr_procedimento.cd_motivo_baixa%type;
cd_estabelecimento_w	prescr_medica.cd_estabelecimento%type;
			
begin
	ie_oculta_integr_w := 'N';
	select	nvl(max(d.nr_seq_exame), 0),
		nvl(max(a.nr_seq_exame),0),
		nvl(max(c.cd_estabelecimento), 0),
		nvl(max(Obter_Material_Exame_Lab(null, a.cd_material_exame, 1)),0),
		nvl(max(d.nr_seq_material), 0) nr_seq_material_princ,
		nvl(max(c.cd_setor_atendimento),0),
		nvl(max(a.cd_setor_atendimento),0),
		nvl(max(c.nr_atendimento),0),
		max(a.dt_integracao),
		nvl(max(a.cd_motivo_baixa),0)
	into	nr_seq_superior_w,
		nr_seq_exame_w,
		cd_estabelecimento_w,
		nr_seq_material_w,
		nr_seq_material_princ_w,
		cd_setor_prescricao_w,
		cd_setor_atendimento_w,
		nr_atendimento_w,
		dt_integracao_w,
		cd_motivo_baixa_w
	from	prescr_procedimento a,
		prescr_medica c,
		(select	b.nr_seq_exame,
			Obter_Material_Exame_Lab(null, b.cd_material_exame, 1) nr_seq_material,
			b.nr_sequencia,
			b.nr_prescricao
		from	prescr_procedimento b) d
	where	a.nr_prescricao = c.nr_prescricao
	  and 	d.nr_prescricao = a.nr_prescricao
	  and 	d.nr_sequencia = a.nr_seq_superior
	  and	a.nr_prescricao = nr_prescricao_p	  
	  and	a.nr_sequencia = nr_seq_prescr_p;
	  
	if (nr_seq_superior_w > 0 and dt_integracao_w is not null and cd_motivo_baixa_w > 0) then
		select	nvl(max(cd_convenio),0)
		into	cd_convenio_w
		from	atend_categoria_convenio a
		where	a.nr_atendimento = nr_atendimento_w
		  and 	a.dt_inicio_vigencia =	(select max(dt_inicio_vigencia)
						 from	Atend_categoria_convenio b
						 where	nr_atendimento = nr_atendimento_w);
	
		select	nvl(max(ie_oculta_integr), 'N')
		into	ie_oculta_integr_w
		from	exame_lab_dependente
		where	nr_seq_exame = nr_seq_superior_w
		  and	nr_seq_exame_dep = nr_seq_exame_w
		  and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
		  and	nvl(nr_seq_material_regra, nr_seq_material_princ_w) = nr_seq_material_princ_w
		  and 	nvl(nr_seq_material, nr_seq_material_w) = nr_seq_material_w
		  and	nvl(cd_setor_atendimento, cd_setor_atendimento_w) = cd_setor_atendimento_w
		  and	nvl(cd_setor_prescricao, cd_setor_prescricao_w) = cd_setor_prescricao_w
		  and	nvl(cd_convenio, cd_convenio_w) = cd_convenio_w;
	end if;
	return ie_oculta_integr_w;
end lab_oculta_dependente_integ;
/
