CREATE OR REPLACE
FUNCTION Sus_Obter_Indicador_Equipe
			(	cd_funcao_p	Number)
				return number is

cd_retorno_w	Number(1)	:= 0;

BEGIN

select	nvl(max(ie_ind_equipe_sus),0)
into	cd_retorno_w
from	funcao_medico
where	cd_funcao	= cd_funcao_p;

return	cd_retorno_w;

END Sus_Obter_Indicador_Equipe;
/