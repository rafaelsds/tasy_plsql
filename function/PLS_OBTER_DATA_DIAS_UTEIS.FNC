create or replace
function pls_obter_data_dias_uteis
			(	dt_inicial_p		Date,
				qt_dias_p		Number,
				cd_estabelecimento_p	Number)
				return Date is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar a data futura considerando s� dias ute�s.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [X]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_auxiliar_w			Number(10)	:= 1;
dt_auxiliar_w			Date;
dt_auxiliar2_w			Date;
dt_retorno_w			Date;

begin

dt_auxiliar_w	:= trunc(dt_inicial_p);

while qt_auxiliar_w <= qt_dias_p loop 
	begin
	dt_auxiliar2_w	:= dt_auxiliar_w + 1;
	
	if	(obter_se_dia_util(dt_auxiliar2_w, cd_estabelecimento_p)	= 'S') then
		qt_auxiliar_w	:= qt_auxiliar_w + 1;
	end if;
	
	dt_auxiliar_w	:= dt_auxiliar2_w;
	end;
end loop;

dt_retorno_w	:= dt_auxiliar2_w;

return	dt_retorno_w;

end pls_obter_data_dias_uteis;
/