create or replace
function Obter_Desc_Tipo_Avaliacao
			(nr_seq_tipo_aval_p	number)
			return varchar2 is

ds_tipo_avaliacao_w		varchar2(100)	:= '';

begin

if	(nr_seq_tipo_aval_p is not null) then
	begin

	select	ds_tipo
	into		ds_tipo_avaliacao_w
	from		med_tipo_avaliacao
	where		nr_sequencia	= nr_seq_tipo_aval_p;

	end;
end if;

return	ds_tipo_avaliacao_w;

end Obter_Desc_Tipo_Avaliacao;
/