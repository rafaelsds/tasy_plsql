create or replace
function gratuidade_obter_campo_local (	nr_sequencia_p		number)
 		    			return varchar2 is
			
ds_retorno_w		varchar2(50) := '';			
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
nr_seq_proc_interno_w	number(15);	
ie_classificacao_w	varchar2(1);
ie_tipo_util_w		varchar2(15);

begin

select	cd_procedimento,
	ie_origem_proced,
	nvl(nr_seq_proc_interno,0)
into	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_proc_interno_w
from	procedimento_paciente
where	nr_sequencia = nr_sequencia_p;

select	ie_classificacao 
into	ie_classificacao_w 
from	procedimento
where	cd_procedimento = cd_procedimento_w
and	ie_origem_proced = ie_origem_proced_w;

if	(ie_classificacao_w = 3) then
	begin	
	if	(nr_seq_proc_interno_w in(4664,4663,5045,5043,5046,5047,5001,5339,5008)) then
		ds_retorno_w := '2';
	elsif	(nr_seq_proc_interno_w in(5044,4666,5048,5002,5009)) then
		ds_retorno_w := '3';
	elsif	(nr_seq_proc_interno_w in(4665,4667,4900)) then
		ds_retorno_w := '1';
	end if;
	end;
else
	begin
	
	select	nvl(max(ie_tipo_util),'X')
	into	ie_tipo_util_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_w;
	
	if	(ie_tipo_util_w = 'C') then
		ds_retorno_w	:= '7';
	elsif	(ie_tipo_util_w = 'E') then
		ds_retorno_w	:= '6';
	end if;
	end;
end if;

return	ds_retorno_w;

end gratuidade_obter_campo_local;
/
