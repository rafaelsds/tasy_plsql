create or replace
function pls_obter_se_itens_penden_exec(	nr_seq_execucao_p		number)
						return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter se os itens da execu��o ainda est�o pendentes para execu��o.
Rotina chamada antes de habilitar a op��o de  "Liberar execu��o negada", na OSP _ Execu��o da Requisi��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [   ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ds_retorno_w		varchar2(2)	:= 'S';
qt_item_exec_w		number(10);
qt_item_pend_w		number(10);

Cursor C01 is
	select 	nvl(a.qt_item,0),
		pls_quant_itens_pendentes_exec(b.qt_material, b.qt_mat_executado)
	from	pls_execucao_req_item a,
		pls_requisicao_mat b
	where	b.nr_seq_requisicao	= a.nr_seq_requisicao
	and	a.nr_seq_material	= b.nr_seq_material
	and	a.nr_seq_req_mat 	= b.nr_sequencia
	and	a.nr_seq_execucao	= nr_seq_execucao_p
	and	b.ie_status		in ('S', 'P', 'T') 
	union	all
	select 	nvl(a.qt_item,0),
		pls_quant_itens_pendentes_exec(b.qt_procedimento, b.qt_proc_executado)
	from	pls_execucao_req_item a,
		pls_requisicao_proc b
	where	b.nr_seq_requisicao	= a.nr_seq_requisicao
	and	a.nr_seq_req_proc	= b.nr_sequencia
	and	a.nr_seq_req_proc 	= b.nr_sequencia
	and	a.nr_seq_execucao	= nr_seq_execucao_p
	and	b.ie_status		in ('S', 'P', 'T');

begin

open C01;
loop
fetch C01 into	
	qt_item_exec_w,
	qt_item_pend_w;
exit when C01%notfound;
	begin
		if (qt_item_pend_w < qt_item_exec_w) then
			ds_retorno_w	:= 'N';
			exit;
		end if;
	end;
end loop;
close C01;

return	ds_retorno_w;

end pls_obter_se_itens_penden_exec;
/
