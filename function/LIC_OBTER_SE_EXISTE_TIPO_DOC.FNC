create or replace
function	lic_obter_se_existe_tipo_doc(
			nr_seq_fornec_docto_p	number)
		return varchar2 is

ie_existe_w		varchar2(1)	:= 'N';
cd_cgc_w		reg_lic_fornec.cd_cgc_fornec%type;
nr_seq_tipo_docto_w	reg_lic_fornec_docto.nr_seq_tipo_docto%type;
nr_seq_fornec_w		reg_lic_fornec_docto.nr_seq_fornec%type;

begin

select	nr_seq_tipo_docto,
	nr_seq_fornec
into	nr_seq_tipo_docto_w,
	nr_seq_fornec_w
from	reg_lic_fornec_docto
where	nr_sequencia = nr_seq_fornec_docto_p;

select	cd_cgc_fornec
into	cd_cgc_w
from	reg_lic_fornec
where	nr_sequencia = nr_seq_fornec_w;

select	decode(count(*),0,'N','S')
into	ie_existe_w
from	pessoa_juridica_doc
where	nr_seq_tipo_docto = nr_seq_tipo_docto_w
and	cd_cgc = cd_cgc_w;

return	ie_existe_w;

end lic_obter_se_existe_tipo_doc;
/