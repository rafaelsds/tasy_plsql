create or replace
function Obter_se_protese_lib (	ie_respiracao_p		varchar2,
				ie_canula_traqueo_p	varchar2)
 		    	return varchar2 is

ie_liberado_w	varchar2(1) := 'S';
cont_w		number(10);

begin

select	count(*)
into	cont_w
from	regra_protese_respiracao
where	ie_respiracao	= ie_respiracao_p;

if	(cont_w	> 0) then
	select	count(*)
	into	cont_w
	from	regra_protese_respiracao
	where	ie_respiracao		= ie_respiracao_p
	and	ie_canula_traqueo	= ie_canula_traqueo_p;
	if	(cont_w = 0) then
		ie_liberado_w	:= 'N';
	end if;
end if;

return	ie_liberado_w;

end Obter_se_protese_lib;
/