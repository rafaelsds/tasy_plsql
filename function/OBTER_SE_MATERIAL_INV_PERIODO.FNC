create or replace
function obter_se_material_inv_periodo( 
	cd_material_p		number,
	cd_estabelecimento_p	number,
	cd_local_estoque_p	number,
	qt_dias_inventario_p	number)
	return varchar2 is

cd_material_estoque_w	number(6);
qt_inventario_w		number(10);
ds_retorno_w		varchar2(1);
dt_inicio_consulta_w	date;
			
begin
begin
select	cd_material_estoque
into	cd_material_estoque_w
from	material
where	cd_material = cd_material_p;
exception
when others then
	cd_material_estoque_w	:=	cd_material_p;	
end;

dt_inicio_consulta_w	:=	(trunc(sysdate - qt_dias_inventario_p,'dd') - 1/86400);

begin

select	1
into	qt_inventario_w
from	inventario a,
	inventario_material b
where	a.nr_sequencia = b.nr_seq_inventario
and	a.cd_estabelecimento = cd_estabelecimento_p
and	a.cd_local_estoque = cd_local_estoque_p
and	cd_material = cd_material_estoque_w
and	a.dt_atualizacao_saldo is not null
and	b.qt_inventario is not null
and	a.dt_bloqueio > dt_inicio_consulta_w
and	rownum = 1; 
exception
when others then
	begin
	select	1
	into	qt_inventario_w
	from	movimento_estoque
	where	cd_estabelecimento = cd_estabelecimento_p
	and	cd_local_estoque = cd_local_estoque_p
	and	cd_material_estoque = cd_material_estoque_w
	and	dt_movimento_estoque > dt_inicio_consulta_w
	and	ie_origem_documento = '5'
	and	dt_processo is not null
	and	cd_acao = 1
	and	rownum = 1;
	exception
	when others then
		qt_inventario_w	:=	0;
	end;
end;

if	(qt_inventario_w > 0) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end obter_se_material_inv_periodo;
/
