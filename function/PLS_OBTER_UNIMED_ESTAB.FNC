create or replace
function pls_obter_unimed_estab
			(	cd_estabelecimento_p	Number)
				return Varchar2 is
		
cd_cgc_outorgante_w		Varchar2(14);
cd_cooperativa_w		varchar2(10);

begin

select	max(cd_cgc_outorgante)
into	cd_cgc_outorgante_w
from	pls_outorgante
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(cd_cgc_outorgante_w	is null) then
	select	max(cd_cgc_outorgante)
	into	cd_cgc_outorgante_w
	from	pls_outorgante;
	
	if	(cd_cgc_outorgante_w	is not null) then
		select	max(cd_cooperativa)
		into	cd_cooperativa_w
		from	pls_congenere
		where	cd_cgc		= cd_cgc_outorgante_w
		and	cd_cooperativa 	is not null;
	end if;
else
	select	max(cd_cooperativa)
	into	cd_cooperativa_w
	from	pls_congenere
	where	cd_cgc			= cd_cgc_outorgante_w
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_cooperativa is not null;
	
	if	(cd_cooperativa_w	is null) then
		select	max(cd_cooperativa)
		into	cd_cooperativa_w
		from	pls_congenere
		where	cd_cgc		= cd_cgc_outorgante_w
		and	cd_cooperativa 	is not null;
	end if;
end if;

return	cd_cooperativa_w;

end pls_obter_unimed_estab;
/