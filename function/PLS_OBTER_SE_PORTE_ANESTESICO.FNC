create or replace
function pls_obter_se_porte_anestesico
			(	nr_seq_requisicao_p		Number,
				nr_seq_guia_plano_p		Number,
				nr_seq_conta_p			Number,
				nr_seq_proc_p			Number, 
				nr_seq_mat_p			Number,
				ie_tipo_item_p			number,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				nr_seq_segurado_p		number,
				qt_dias_p			number,
				dt_emissao_p			date)
				return varchar2 is
				
cd_porte_anestesico_w		varchar2(20);
dt_final_w			date;
qt_reg_w			number(5);
ds_retorno_w			varchar2(1) := 'N';
				
begin
dt_final_w := fim_dia(dt_emissao_p);
if	(ie_tipo_item_p = 1) then

	select	cd_porte_anestesico
	into	cd_porte_anestesico_w
	from	pls_guia_plano_proc
	where	nr_sequencia = nr_seq_proc_p;
	
	if	(cd_porte_anestesico_w is not null) then
		select	count(*)
		into	qt_reg_w
		from	pls_guia_plano_proc	b,
			pls_guia_plano		a
		where	b.nr_seq_guia		= a.nr_sequencia
		and	a.ie_status		= 1
		and	a.nr_seq_segurado	= nr_seq_segurado_p
		and	a.nr_sequencia 		<> nr_seq_guia_plano_p
		and	nvl(b.cd_porte_anestesico,0) 	> 0
		and	a.dt_solicitacao between (trunc(dt_emissao_p) - qt_dias_p) and dt_final_w;
		
		if	(qt_reg_w > 0) then
			ds_retorno_w := 'S';
		end if;
	end if;
	
elsif	(ie_tipo_item_p = 5) then
	/*select	cd_porte_anestesico
	into	cd_porte_anestesico_w
	from	pls_requisicao_proc
	where	nr_sequencia = nr_seq_proc_p;

	if	(cd_porte_anestesico_w is not null) then*/
	
	select	count(*)
	into	qt_reg_w
	from	pls_requisicao_proc	b,
		pls_requisicao		a
	where	b.nr_seq_requisicao	= a.nr_sequencia
	and	a.ie_estagio		= 2
	and	b.ie_status		in ('P','S')
	and	a.nr_seq_segurado	= nr_seq_segurado_p
	and	a.nr_sequencia 		<> nr_seq_requisicao_p
	and	nvl(b.cd_porte_anestesico,0) 	> 0
	and	a.dt_requisicao between (trunc(dt_emissao_p) - qt_dias_p) and dt_final_w;
	
	if	(qt_reg_w > 0) then
		ds_retorno_w := 'S';
	end if;
	
end if;

return	ds_retorno_w;

end pls_obter_se_porte_anestesico;
/
