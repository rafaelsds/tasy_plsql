create or replace
function obter_transcricao_adep(nr_prescricao_p		Number,
				nr_prescricoes_p	Varchar2
				)return Number is

nr_prescricao_w		Number(14);
nr_seq_transcricao_w	Number(14);
				
begin

if	(nr_prescricao_p > 0) then
	nr_prescricao_w := nr_prescricao_p;
else
	nr_prescricao_w := Obter_Max_NrPrescricao(nr_prescricoes_p);
end if;

nr_seq_transcricao_w := obter_transcricao_prescricao(nr_prescricao_w);

return	nr_seq_transcricao_w;

end obter_transcricao_adep;
/