create or replace
function pls_obter_qt_max_lote_emissao
		(	nr_seq_lote_p		number,
			cd_interface_p		number,
			ie_tipo_restricao_p	varchar2)
			return number is

qt_retorno_w		number(10);

begin

qt_retorno_w	:= 0;

if	(cd_interface_p	= 1878) then
	select	count(1)
	into	qt_retorno_w
	from	pls_cartao_unimed_v
	where	nr_seq_lote	= nr_seq_lote_p;
elsif	(cd_interface_p	= 1956) then
	select	count(1)
	into	qt_retorno_w
	from	pls_cartao_unimed_sjrp_v
	where	nr_seq_lote	= nr_seq_lote_p;
elsif	(cd_interface_p	= 1971) then
	select	count(1)
	into	qt_retorno_w
	from	pls_cart_provisoria_unimed_sjc
	where	nr_seq_lote	= nr_seq_lote_p;
elsif	(cd_interface_p	= 1856) then
	select	count(1)
	into	qt_retorno_w
	from	pls_cartao_identificacao_v
	where	nr_seq_lote	= nr_seq_lote_p
	and	ie_plano_regulamentado	= ie_tipo_restricao_p;
elsif	(cd_interface_p	= 1992) then
	select	count(1)
	into	qt_retorno_w
	from	pls_cart_definit_unimed_sjc_v
	where	nr_seq_lote	= nr_seq_lote_p;
elsif	(cd_interface_p	= 2143) then
	select	count(1)
	into	qt_retorno_w
	from	pls_cartao_life_empresarial_v
	where	nr_seq_lote	= nr_seq_lote_p;
elsif	(cd_interface_p	in (2237,2205,2185,2180,2155,2243,2433,2523,2553,2720,2704,2603)) then
	select	count(1)
	into	qt_retorno_w
	from	w_pls_interface_carteira
	where	nr_seq_lote	= nr_seq_lote_p;
elsif	(cd_interface_p = 3053) then
	select	count(1)
	into	qt_retorno_w
	from	pls_cartao_identificacao_v
	where	nr_seq_lote	= nr_seq_lote_p;
end if;

return	qt_retorno_w;

end pls_obter_qt_max_lote_emissao;
/