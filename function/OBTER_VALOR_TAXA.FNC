create or replace
function Obter_Valor_taxa
		(nr_seq_taxa_p		number,
		ie_tipo_valor_p		number)
		return	number is

vl_taxa_w	number(15,2);

begin

select  nvl(sum(a.vl_ajuste),0)
into	vl_taxa_w
from    TAXA_RESUMO_CONTA a, material_atend_paciente b
where   a.nr_seq_servico                = nr_seq_taxa_p
and 	a.NR_SEQ_MAT_PROC = b.nr_sequencia
and 	b.nr_seq_proc_pacote is null;


return	vl_taxa_w;

end Obter_Valor_taxa;
/