create or replace
function adep_obter_hor_padroes_item(	cd_item_p			number,
										nr_prescricao_p		varchar2,
										nr_seq_horario_p	number,
										nr_seq_solucao_p	number,
										ie_tipo_item_p		varchar2,
										cd_intervalo_p 		varchar2,
										nr_seq_proc_interno_p   number)
				return varchar2 is

ds_horario_w			varchar2(15);
ds_resultado_w			varchar2(2000) := null;
nr_prescricao_w			number(15);
nr_seq_material_w		number(15);
i						number(15);
qt_registro_w			number(15);
dt_horario_w			date;
dt_horario_inicio_w		date;
dt_ult_hora_w date;
cd_setor_atendimento_w	number(15);
hr_prim_horario_w		varchar2(10);
cd_funcao_origem_w		prescr_medica.cd_funcao_origem%type;
ds_horarios_w varchar2(2000);
ds_horarios2_w varchar2(2000);
nr_ocorrencia_w number(10);
cd_intervalo_w			prescr_material.cd_intervalo%type;
cd_material_w			prescr_material.cd_material%type;
nr_seq_solucao_w		prescr_material.nr_sequencia%type;
nr_etapas_w				prescr_solucao.nr_etapas%type;
qt_hora_fase_w			prescr_solucao.qt_hora_fase%type;
ie_etapa_especial_w		prescr_solucao.ie_etapa_especial%type;
nr_seq_cpoe_w    		prescr_material.nr_seq_mat_cpoe%type;
nr_prescr_param_w		prescr_medica.nr_prescricao%type;
ie_reordena_w			varchar2(1);
qt_operacao_w 			prescr_material.nr_ocorrencia%type;
dt_inicio_prescr_w 		prescr_medica.dt_inicio_prescr%type;
nr_seq_proc_w                  number(15);

type campos is record (nr_prescricao 	number(15,4));
type Vetor is table of campos index 	by binary_integer;
Vetor_w			Vetor;			

type Datas is record (	ds_horario 	varchar2(15), dt_horario	date);

type Vetor_data is table of Datas index 	by binary_integer;
Vetor_data_w			Vetor_data;	

Cursor C01 is
select	to_char(b.dt_horario,'hh24:mi'),
		b.dt_horario
from	prescr_mat_hor b,
		prescr_material a
where	a.nr_sequencia		= b.nr_seq_material
and		a.nr_prescricao		= b.nr_prescricao
and		Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
and		nvl(b.ie_horario_especial,'N') = 'N'
and		nvl(b.ie_dose_especial,'N') = 'N'
and		b.dt_horario >= dt_horario_inicio_w
and		a.dt_suspensao is null
and		a.cd_material		= cd_material_w
and		a.nr_sequencia		= nr_seq_material_w
and		a.nr_prescricao		= nr_prescricao_w
and		a.ie_agrupador not in (3,7,9)
group by b.dt_horario
order by b.dt_horario;

Cursor C02 is
select	to_char(b.dt_horario,'hh24:mi'),
		b.dt_horario
from	prescr_mat_hor b,
		prescr_material a
where	a.nr_sequencia		= b.nr_seq_material
and		a.nr_prescricao		= b.nr_prescricao
and		Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
and		nvl(b.ie_horario_especial,'N') = 'N'
and		nvl(b.ie_dose_especial,'N') = 'N'
and		a.dt_suspensao is null
and		a.cd_material		= cd_material_w
and		a.nr_sequencia		= nr_seq_material_w
and		a.nr_prescricao		= nr_prescricao_w
and		a.ie_agrupador not in (3,7,9)
group by b.dt_horario
order by b.dt_horario;


Cursor C03 is
select	to_char(b.dt_horario,'hh24:mi'),
	b.dt_horario		
from	prescr_procedimento a,
	prescr_proc_hor b
where	a.nr_prescricao = b.nr_prescricao
and	a.nr_sequencia = b.nr_seq_procedimento
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
and	a.nr_prescricao = nr_prescricao_w				
and	a.dt_suspensao is null		
and     a.nr_seq_proc_interno = nr_seq_proc_interno_p		
and     a.nr_sequencia = nr_seq_proc_w		
group by b.dt_horario
order by b.dt_horario;


Cursor C04 is
select	to_char(b.dt_horario,'hh24:mi'),
	b.dt_horario		
from	prescr_procedimento a,
	prescr_proc_hor b
where	a.nr_prescricao = b.nr_prescricao
and	a.nr_sequencia = b.nr_seq_procedimento
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
and	b.dt_horario >= dt_horario_inicio_w
and	a.nr_prescricao = nr_prescricao_w				
and	a.dt_suspensao is null		
and     a.nr_seq_proc_interno = nr_seq_proc_interno_p		
and     a.nr_sequencia = nr_seq_proc_w		
group by b.dt_horario
order by b.dt_horario;




	procedure adiciona_horario(	dt_horario_p	date,
					ds_horarios_p	varchar2) is
	
	begin
	qt_registro_w	:= 0;
	for k in 1..vetor_data_w.count loop
		begin
		if	(dt_horario_p	= vetor_data_w(k).dt_horario) then
			qt_registro_w	:= 1;
			exit;
		end if;
		end;
	end loop;
	
	if	(qt_registro_w	= 0) then
		vetor_data_w(vetor_data_w.Count + 1).dt_horario	:= dt_horario_p;
		vetor_data_w(vetor_data_w.Count + 1).ds_horario	:= ds_horarios_p;
	end if;
	
	end;
	-- fim Procedure 
	
	--Inicio Sub function
	
	function tratarCursor ( nr_prescricao_w number, 
				nr_seq_material_w number, 
				nr_seq_proc_w number ) return  varchar2 is 
			 
	ds_resultado_ww varchar2(2000);
	begin 	
	  if (nr_prescricao_w is not null) and
	     ((nr_seq_material_w is not null) or (nr_seq_proc_w is not null))then
	     
			select	max(a.dt_prescricao),
					max(Obter_Setor_Atendimento(a.nr_atendimento)),
					max(cd_funcao_origem),
					max(dt_inicio_prescr)
			into	dt_horario_inicio_w,
					cd_setor_atendimento_w,
					cd_funcao_origem_w,
					dt_inicio_prescr_w
			from	prescr_medica a
			where	a.nr_prescricao = nr_prescricao_w;
			
			if	(cd_setor_atendimento_w is not null) then
				dt_horario_inicio_w	:=	converte_char_data(to_char(dt_horario_inicio_w,'dd/mm/yyyy'),to_char(Obter_Prim_Horario_Setor(cd_setor_atendimento_w),'hh24:mi:ss'),dt_horario_inicio_w);
			end if;
					
			
			if (cd_funcao_origem_w = 2314) then	
				
				if(nr_seq_material_w is not null)then
				
					select	max(b.hr_prim_horario),
							max(a.cd_intervalo),
							max(a.nr_ocorrencia)
					into	hr_prim_horario_w,
							cd_intervalo_w,
							qt_operacao_w
					from	prescr_material a,
							cpoe_material_vig_v b
					where	nvl(a.nr_seq_mat_cpoe,a.nr_seq_dieta_cpoe) = b.nr_sequencia
					and		nvl(a.nr_seq_mat_cpoe,a.nr_seq_dieta_cpoe) is not null
					and		a.nr_prescricao = nr_prescricao_w
					and		a.nr_sequencia = nr_seq_material_w;
					
				elsif(nr_seq_proc_w is not null) then
				
					select  to_char( max(a.dt_primeiro_horario),'HH24:MI'),
						max(b.cd_intervalo),
						max(b.nr_ocorrencia)	
					into	hr_prim_horario_w,
						cd_intervalo_w,
						qt_operacao_w			
					from 	prescr_medica a,
						prescr_procedimento b
					where 	a.nr_prescricao = b.nr_prescricao
					and	a.nr_prescricao = nr_prescricao_w
					and	b.nr_sequencia = nr_seq_proc_w;				
				end if;	

				
				
				if (hr_prim_horario_w is not null) then
					if (qt_operacao_w = 1) then
						dt_horario_inicio_w	:=	dt_inicio_prescr_w;		
					else
						dt_horario_inicio_w	:=	converte_char_data(to_char(dt_horario_inicio_w,'dd/mm/yyyy'), hr_prim_horario_w,dt_horario_inicio_w);
					end if;
				end if;				
				
			end if;				
			
			
			if(nr_seq_material_w is not null) then	
			
			
				if(obter_operacao_intervalo(cd_intervalo_p) = 'X') then	
					ds_resultado_ww:= null;
					open c02;
					loop
					fetch c02 into
						ds_horario_w,
						dt_horario_w;
					exit when c02%notfound;
						begin	
						ds_resultado_ww	:= ds_resultado_ww ||' '|| ds_horario_w;			
						end;
					end loop;
					close c02;
				 return ds_resultado_ww;
				 
				else
				
					open c01;
					loop
					fetch c01 into
						ds_horario_w,
						dt_horario_w;
					exit when c01%notfound;
						begin			
						adiciona_horario(dt_horario_w,ds_horario_w);
						
						if	(cd_funcao_origem_w = 2314) and
							((dt_ult_hora_w is null) or
							 (dt_horario_w > dt_ult_hora_w)) then
							dt_ult_hora_w := dt_horario_w;
						end if;
						end;
					end loop;
					close c01;	 
				end if;
			
			
			elsif(nr_seq_proc_w is not null) then
				
				
			
				if(obter_operacao_intervalo(cd_intervalo_p) = 'X') then
				
					ds_resultado_ww:= null;
					open c03;
					loop
					fetch c03 into
						ds_horario_w,
						dt_horario_w;
					exit when c03%notfound;
						begin	
						ds_resultado_ww	:= ds_resultado_ww ||' '|| ds_horario_w;			
						end;
					end loop;
					close c03;
					
					return ds_resultado_ww;
				
				else
				
					open c04;
					loop
					fetch c04 into
						ds_horario_w,
						dt_horario_w;
					exit when c04%notfound;
						begin			
						adiciona_horario(dt_horario_w,ds_horario_w);
						
						if	(cd_funcao_origem_w = 2314) and
							((dt_ult_hora_w is null) or
							 (dt_horario_w > dt_ult_hora_w)) then
							dt_ult_hora_w := dt_horario_w;
						end if;
						end;
					end loop;
					close c04;
				
				end if;		
			end if;
						
			 			
						
			ds_resultado_ww	:= null;
			
			for i in 1..Vetor_data_w.Count loop
				ds_resultado_ww	:= ds_resultado_ww ||' '|| Vetor_data_w(i).ds_horario;
			end loop;		
			
			
			if (cd_funcao_origem_w = 2314) then
			
			
				if(nr_seq_material_w is not null or nr_seq_proc_w is not null) then	
				
					if (ie_tipo_item_p <> 'SOL') then
						Calcular_Horario_Prescricao(nr_prescricao_w,
													cd_intervalo_w,
													dt_ult_hora_w,
													dt_ult_hora_w,
													24,
													cd_item_p,
													0,
													0,
													nr_ocorrencia_w,
													ds_horarios_w,
													ds_horarios2_w,
													'N', 
													null);
												
					
					else
						select	max(a.nr_etapas),
								max(a.qt_hora_fase),
								nvl(max(a.ie_etapa_especial),'N')
						into	nr_etapas_w,
								qt_hora_fase_w,
								ie_etapa_especial_w
						from	prescr_solucao a,
								prescr_material b
						where	a.dt_suspensao	is null
						and		a.nr_prescricao = b.nr_prescricao
						and		a.nr_seq_solucao = b.nr_sequencia_solucao
						and		b.nr_seq_mat_cpoe = nr_seq_cpoe_w
						and		a.nr_prescricao	= nr_prescricao_w;

						CPOE_Calcula_horarios_etapas(	wheb_usuario_pck.get_nm_usuario,
														dt_ult_hora_w,
														'N',
														nr_etapas_w,
														null,
														24,
														qt_hora_fase_w,
														0,
														ds_horarios_w,
														'N',
														null,
														null,
														'N');
				
					end if;
					
					
				
				end if;				
				
				select	nvl(max('S'), 'N')
				into	ie_reordena_w
				from	intervalo_prescricao
				where	cd_intervalo = cd_intervalo_w
				and		((ie_operacao = 'H' and (qt_operacao <= 12 or qt_operacao >= 24)) or (ie_operacao <> 'H'));
				
				
				if (ie_reordena_w = 'S') then
					if	(to_char(dt_ult_hora_w,'mi') = '00') then		
							ds_resultado_ww := replace(ds_horarios_w||' '||ds_horarios2_w,to_char(dt_ult_hora_w,'hh24'),'') ||' '||to_char(dt_ult_hora_w,'hh24') ;
							
						else
							ds_resultado_ww := replace(ds_horarios_w||' '||ds_horarios2_w,to_char(dt_ult_hora_w,'hh24:mi'),'')||' '||to_char(dt_ult_hora_w,'hh24:mi');
							
						end if;
					else 
						ds_resultado_ww := ds_horarios_w;
						
				end if;
						
				ds_resultado_ww := ' '||replace(replace(Padroniza_horario_prescr(ds_resultado_ww,to_char(dt_ult_hora_w,'dd/mm/yyyy hh24:mi:ss')),'AA',''),'A','');
				
			end if;
			
		end if;		
		
		return ds_resultado_ww;
	end;
	
	 
begin

if	(instr(nr_prescricao_p, ',') = 0) then
	nr_prescr_param_w	:= nr_prescricao_p;
else
	nr_prescr_param_w	:= obter_max_nrprescricao(nr_prescricao_p);
end if;

	if(ie_tipo_item_p = 'SOL' or ie_tipo_item_p in ('M','SNE'))then --Medicamento e Solu?
		if (ie_tipo_item_p = 'SOL') then
		    select	max(a.cd_material)			
			into	cd_material_w		
			from	prescr_material a
			where	a.dt_suspensao	is null
			and		a.nr_sequencia_solucao	= nr_seq_solucao_p
			and		a.nr_prescricao = nr_prescr_param_w
			and		a.ie_agrupador not in (3,7,9);

			if	(cd_material_w is null) then
			
				select	max(a.cd_material)			
				into	cd_material_w		
				from	prescr_material a
				where	a.dt_suspensao	is null
				and		a.nr_sequencia_solucao	= nr_seq_solucao_p
				and		a.nr_prescricao like '%'|| nr_prescricao_p || '%'
				and		a.ie_agrupador not in (3,7,9);
				
			end if;	
			  
			  
		end if;

		if (ie_tipo_item_p <> 'SOL') then
			cd_material_w := cd_item_p;
		end if;

		if	((nvl(nr_seq_horario_p,0) > 0) and (ie_tipo_item_p <> 'SOL')) then

		    select 	nvl(max(a.nr_seq_mat_cpoe), 0) 
			into 	nr_seq_cpoe_w
			from	prescr_mat_hor b,
					prescr_material a
			where	a.nr_sequencia = b.nr_seq_material
			and		a.nr_prescricao = b.nr_prescricao
			and		nvl(b.ie_horario_especial,'N') = 'N'
			and		nvl(b.ie_dose_especial,'N') = 'N'
			and		b.dt_horario between sysdate - 5 and sysdate + 5
			and		a.dt_suspensao is null
			and		a.cd_material	= cd_material_w 	
			and		b.nr_sequencia	= nr_seq_horario_p
			and		a.ie_agrupador not in (3,7,9);
			
		elsif (ie_tipo_item_p = 'SOL') then

			select 	nvl(max(a.nr_seq_mat_cpoe), 0) 
			into 	nr_seq_cpoe_w
			from	prescr_mat_hor b,
					prescr_material a
			where	a.nr_sequencia = b.nr_seq_material
			and		a.nr_prescricao = b.nr_prescricao
			and		nvl(b.ie_horario_especial,'N') = 'N'
			and		nvl(b.ie_dose_especial,'N') = 'N'
			and		b.dt_horario between sysdate - 5 and sysdate + 5
			and		a.dt_suspensao is null
			and		a.cd_material	= cd_material_w 	
			and		b.nr_etapa_sol = nr_seq_horario_p
			and		a.nr_sequencia_solucao	= nr_seq_solucao_p
			and		b.nr_prescricao = nr_prescr_param_w
			and		a.ie_agrupador not in (3,7,9);
			
			if	(nr_seq_cpoe_w = 0) then
			
				select 	nvl(max(a.nr_seq_mat_cpoe), 0) 
				into 	nr_seq_cpoe_w
				from	prescr_mat_hor b,
						prescr_material a
				where	a.nr_sequencia = b.nr_seq_material
				and		a.nr_prescricao = b.nr_prescricao
				and		nvl(b.ie_horario_especial,'N') = 'N'
				and		nvl(b.ie_dose_especial,'N') = 'N'
				and		b.dt_horario between sysdate - 5 and sysdate + 5
				and		a.dt_suspensao is null
				and		a.cd_material	= cd_material_w 
				and		a.nr_prescricao like '%'|| nr_prescricao_p || '%'
				and		b.nr_etapa_sol = nr_seq_horario_p
				and		a.nr_sequencia_solucao	= nr_seq_solucao_p
				and		a.ie_agrupador not in (3,7,9);	
				
			end if;

		else

			select 	nvl(max(a.nr_seq_mat_cpoe), 0) 
			into 	nr_seq_cpoe_w
			from	prescr_mat_hor b,
					prescr_material a
			where	a.nr_sequencia = b.nr_seq_material
			and		a.nr_prescricao = b.nr_prescricao
			and		nvl(b.ie_horario_especial,'N') = 'N'
			and		nvl(b.ie_dose_especial,'N') = 'N'
			and		b.dt_horario between sysdate - 5 and sysdate + 5
			and		a.dt_suspensao is null
			and		a.cd_material	= cd_material_w 
			and		a.nr_prescricao like '%'|| nr_prescricao_p || '%'
			and		a.ie_agrupador not in (3,7,9);
			
		end if;
		
		if	(nr_seq_cpoe_w > 0) then

		    select	max(a.nr_prescricao)
			into	nr_prescricao_w
			from	prescr_mat_hor b,
					prescr_material a
			where	a.nr_sequencia = b.nr_seq_material
			and		a.nr_prescricao = b.nr_prescricao
			and		nvl(b.ie_horario_especial,'N') = 'N'
			and		nvl(b.ie_dose_especial,'N') = 'N'
			and		b.dt_horario between sysdate - 5 and sysdate + 5
			and		a.dt_suspensao is null
			and		a.cd_material	= cd_material_w --cd_item_p	
			and   	a.nr_seq_mat_cpoe = nr_seq_cpoe_w
			and		a.ie_agrupador not in (3,7,9);
			
		else

			select	max(a.nr_prescricao)
			into	nr_prescricao_w
			from	prescr_mat_hor b,
					prescr_material a
			where	a.nr_sequencia = b.nr_seq_material
			and		a.nr_prescricao = b.nr_prescricao
			and		nvl(b.ie_horario_especial,'N') = 'N'
			and		nvl(b.ie_dose_especial,'N') = 'N'
			and		b.dt_horario between sysdate - 5 and sysdate + 5
			and		a.dt_suspensao is null
			and		a.cd_material	= cd_material_w --cd_item_p
			and		b.nr_prescricao = nr_prescr_param_w
			and		a.ie_agrupador not in (3,7,9);
			
			if	(nr_prescricao_w is null) then
				
				select	max(a.nr_prescricao)
				into	nr_prescricao_w
				from	prescr_mat_hor b,
						prescr_material a
				where	a.nr_sequencia = b.nr_seq_material
				and		a.nr_prescricao = b.nr_prescricao
				and		nvl(b.ie_horario_especial,'N') = 'N'
				and		nvl(b.ie_dose_especial,'N') = 'N'
				and		b.dt_horario between sysdate - 5 and sysdate + 5
				and		a.dt_suspensao is null
				and		a.cd_material	= cd_material_w --cd_item_p
				and		a.nr_prescricao like '%'|| nr_prescricao_p || '%'
				and		a.ie_agrupador not in (3,7,9);
				
			end if;
			
			
		end if;		

		select	max(x.nr_seq_material)
		into	nr_seq_material_w
		from	prescr_mat_hor x,
				prescr_material b
		where	x.dt_suspensao	is null
		and		x.cd_material	= cd_material_w 
		and		x.nr_prescricao = b.nr_prescricao
		and		x.nr_seq_material = b.nr_sequencia
		and		x.nr_prescricao	= nr_prescricao_w
		and		x.ie_agrupador not in (3,7,9)
		and		(((nr_seq_cpoe_w > 0) and (b.nr_seq_mat_cpoe = nr_seq_cpoe_w)) or
				  (nr_seq_cpoe_w = 0));
				  
		if ((nr_Prescricao_w is not null) or
		    (nr_seq_material_w is not null)) and
		   (nvl(nr_seq_horario_p,0) > 0) and
		   (nr_seq_cpoe_w = 0)   then
		   
		   select	max(x.nr_prescricao),
					max(x.nr_seq_material)
		   into		nr_prescricao_w,
					nr_seq_material_w
		   from		prescr_mat_hor x,
					prescr_material a
		   where	a.nr_sequencia	= x.nr_seq_material
		   and		a.nr_prescricao = x.nr_prescricao
		   and		(x.nr_sequencia	= nr_seq_horario_p or (x.nr_etapa_sol = nr_seq_horario_p and ie_tipo_item_p = 'SOL'))
		   and		a.dt_suspensao is null
		   and		x.ie_agrupador not in (3,7,9)
		   and		a.cd_material	= cd_material_w --cd_item_p
		   and		a.nr_prescricao	= nr_prescricao_w;
		   
		end if;		
		
		ds_resultado_w := tratarCursor(nr_prescricao_w, nr_seq_material_w, null);
		
	end if; -- fim Medicamento e Solu? 
	
	if(ie_tipo_item_p = 'P' or ie_tipo_item_p = 'L') then 
	
		if	((nvl(nr_seq_horario_p,0) > 0)) then		
			select	nvl(max(a.nr_seq_proc_cpoe), 0) 
			into 	nr_seq_cpoe_w
			from	prescr_procedimento a,
				prescr_proc_hor b
			where	a.nr_prescricao = b.nr_prescricao
			and	a.nr_sequencia = b.nr_seq_procedimento
			and	a.nr_prescricao = nr_prescr_param_w			
			and     b.nr_sequencia = nr_seq_horario_p
			and     b.dt_horario between sysdate - 5 and sysdate + 5
			and	a.dt_suspensao is null
			and     a.nr_seq_proc_interno = nr_seq_proc_interno_p;
					
		else 
			select	nvl(max(a.nr_seq_proc_cpoe), 0) 
			into 	nr_seq_cpoe_w
			from	prescr_procedimento a,
				prescr_proc_hor b
			where	a.nr_prescricao = b.nr_prescricao
			and	a.nr_sequencia = b.nr_seq_procedimento
			and	a.nr_prescricao = nr_prescr_param_w			
			and     b.dt_horario between sysdate - 5 and sysdate + 5
			and	a.dt_suspensao is null
			and     a.nr_seq_proc_interno = nr_seq_proc_interno_p;	
		end if; -- Buscar Seq CPOE
		
		if	(nr_seq_cpoe_w > 0) then
			-- obter maior prescr
			select max(a.nr_prescricao)
			into	nr_prescricao_w
			from prescr_medica a,
				prescr_procedimento b
			where a.nr_prescricao = b.nr_prescricao
			and   b.nr_seq_proc_interno = nr_seq_proc_interno_p
			and   b.nr_seq_proc_cpoe = nr_seq_cpoe_w
			and   a.dt_suspensao is null;		
		else 		
			select max(a.nr_prescricao)
			into	nr_prescricao_w
			from prescr_medica a,
				prescr_procedimento b
			where a.nr_prescricao = b.nr_prescricao
			and   b.nr_seq_proc_interno = nr_seq_proc_interno_p	
			and   a.nr_prescricao = nr_prescr_param_w
			and   a.dt_suspensao is null;
			
			if (nr_prescricao_w is null) then
			
				select	nvl(max(a.nr_prescricao), 0) 
				into 	nr_prescricao_w
				from	prescr_procedimento a,
					prescr_proc_hor b
				where	a.nr_prescricao = b.nr_prescricao
				and	a.nr_sequencia = b.nr_seq_procedimento
				and	a.nr_prescricao like '%'|| nr_prescricao_p || '%'			
				and     b.nr_sequencia = nr_seq_horario_p
				and     b.dt_horario between sysdate - 5 and sysdate + 5
				and	a.dt_suspensao is null
				and     a.nr_seq_proc_interno = nr_seq_proc_interno_p;
										
			end if;		
		end if; 
		
		if( nvl(nr_prescricao_w,0) = 0) then 
			nr_prescricao_w := nr_prescr_param_w;
		end if;
			
		
		select max(a.nr_sequencia)
		into nr_seq_proc_w
		from  prescr_procedimento a
		where a.nr_prescricao = nr_prescricao_w
		and   a.nr_seq_proc_interno = nr_seq_proc_interno_p		
		and   a.ds_horarios is not null
		and  (((nr_seq_cpoe_w > 0) and (a.nr_seq_proc_cpoe = nr_seq_cpoe_w)) or (nr_seq_cpoe_w = 0));	
		
		
		
	ds_resultado_w := tratarCursor(nr_prescricao_w, null, nr_seq_proc_w);
				
	end if;
	
	
if (cd_funcao_origem_w = 2314) then
	return replace(substr(ds_resultado_w,1,2000),'  ', ' ');
else
	return replace(substr(ds_resultado_w,2,2000),'  ', ' ');
end if;

end; 
/
