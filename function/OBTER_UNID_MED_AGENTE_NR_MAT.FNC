create or replace FUNCTION OBTER_UNID_MED_AGENTE_NR_MAT 
(
  NR_SEQ_MAT_P IN NUMBER 
) RETURN VARCHAR2 is ds_retorno_w varchar2(100);
BEGIN
    select	obter_desc_unid_med(aa.cd_unid_med_apres)
    into	ds_retorno_w
    from    agente_anestesico aa, agente_anest_material aam
    where aam.nr_sequencia = nr_seq_mat_p
        and aam.nr_seq_agente = aa.nr_sequencia;
    return	ds_retorno_w;
END OBTER_UNID_MED_AGENTE_NR_MAT;
/
