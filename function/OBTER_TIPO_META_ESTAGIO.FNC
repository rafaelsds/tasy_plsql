create or replace
function Obter_Tipo_Meta_Estagio(nr_seq_estagio_p	number,
				 pr_estagio_p		number,
				 qt_horas_estagio_p	number)
 		    	return varchar2 is

ie_tipo_meta_w	varchar2(3);
			
begin

select	nvl(max(ie_tipo_meta),'D')
into	ie_tipo_meta_w
from 	regra_meta_estagio_audit
where 	nr_seq_estagio = nr_seq_estagio_p
and 	qt_horas_estagio_p between nvl(qt_hora_inicial,0) and nvl(qt_hora_final, qt_horas_estagio_p);
--and 	pr_estagio_p between pr_inicial and pr_final;  -- Esse % foi descontinuado (OS 737923)

return	ie_tipo_meta_w;

end Obter_Tipo_Meta_Estagio;
/