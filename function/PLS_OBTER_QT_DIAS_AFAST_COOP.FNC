create or replace
function pls_obter_qt_dias_afast_coop(nr_seq_ausencia_p	number)
						return number is
qt_dias_w	number(10);						

begin

if	(nr_seq_ausencia_p is not null) then

	select	obter_dias_entre_datas(trunc(a.dt_inicio,'dd'), trunc(nvl(a.dt_fim, sysdate),'dd') + 1)
	into	qt_dias_w
	from	pls_cooperado_ausencia	a
	where	nr_sequencia	= nr_seq_ausencia_p;
end if;

return	qt_dias_w;

end pls_obter_qt_dias_afast_coop;
/