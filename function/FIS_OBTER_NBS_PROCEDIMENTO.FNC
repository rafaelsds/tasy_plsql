create or replace
function fis_obter_nbs_procedimento(	cd_area_procedimento_p	number,
					cd_especialidade_p	number,
					cd_grupo_proc_p		number,
					cd_procedimento_p	number,
					ie_retorno_p		varchar2) return varchar2 is

/*
IE_RETORNO_P
S = Sequ�ncia do NBS
C = C�digo do NBS
*/

ds_retorno_w		varchar2(255);
cd_nbs_w		varchar2(20);
nr_sequencia_w		number(10);

begin

begin

select	cd_nbs,
	nr_sequencia
into	cd_nbs_w,
	nr_sequencia_w
from	(
	select	cd_nbs,
		nr_sequencia
	from	lista_nbs a
	where	((obter_se_estrutura_proc(cd_area_procedimento_p, cd_especialidade_p, cd_grupo_proc_p, cd_procedimento_p,
					a.cd_area_procedimento, a.cd_especialidade, a.cd_grupo_proc, a.cd_procedimento) = 'S') or
		(coalesce(a.cd_area_procedimento, a.cd_especialidade, a.cd_grupo_proc, a.cd_procedimento) is null))
	order by
		a.cd_procedimento,
		a.cd_grupo_proc,
		a.cd_especialidade,
		a.cd_area_procedimento)
where	rownum = 1;

if	(nvl(ie_retorno_p,'S') = 'S') then
	begin
	ds_retorno_w	:= nr_sequencia_w;
	end;
elsif	(nvl(ie_retorno_p,'S') = 'C') then
	ds_retorno_w	:= cd_nbs_w;
end if;

exception
when others then
	cd_nbs_w	:= '';
	nr_sequencia_w	:= '';
	ds_retorno_w	:= '';
end;

return	ds_retorno_w;

end fis_obter_nbs_procedimento;
/
