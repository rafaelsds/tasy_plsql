create or replace function get_equipment_acronym_dispatch(nr_seq_dispatch_p Number)
			return VARCHAR2 is

ds_interface_w VARCHAR2(100);

begin

if (NVL(nr_seq_dispatch_p, 0) > 0) then

	SELECT MAX(DS_SIGLA_EQUIP)
	INTO ds_interface_w
	FROM LAB_INTERFACE_ENVIO
	WHERE NR_SEQUENCIA = NR_SEQ_DISPATCH_P;

end if;

return	ds_interface_w;

end get_equipment_acronym_dispatch;
/