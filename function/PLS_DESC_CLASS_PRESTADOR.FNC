create or replace
function pls_desc_class_prestador
			(	nr_seq_classif_p		Number)
				return varchar2 is

ds_retorno_w			Varchar2(255);

begin

select	substr(nvl(ds_classificacao,''),1,255)
into	ds_retorno_w
from	pls_classif_prestador
where	nr_sequencia	= nr_seq_classif_p;

return	ds_retorno_w;

end pls_desc_class_prestador;
/