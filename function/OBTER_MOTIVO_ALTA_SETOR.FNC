create or replace
function Obter_motivo_alta_setor(cd_motivo_alta_setor_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(100);
			
begin

if	(cd_motivo_alta_setor_p	is not null) then

	select  ds_motivo_alta_setor
	into	ds_retorno_w
	from	motivo_alta_setor
	where	cd_motivo_alta_setor = cd_motivo_alta_setor_p;

end if;

return	ds_retorno_w;

end Obter_motivo_alta_setor;
/
