CREATE OR REPLACE
FUNCTION Obter_Classificacao_Atend (NR_ATENDIMENTO_P	NUMBER,
				    IE_OPCAO_P		VARCHAR2)
				RETURN VARCHAR2 IS

nr_seq_classificacao_w	Number(10,0);
ds_classificacao_w	Varchar2(60);
ds_retorno_w		varchar2(60);

/*
C	- Sequencia
D	- Descri��o
*/

BEGIN
if	(nr_atendimento_p is not null) then
	select	max(nr_seq_classificacao)
	into	nr_seq_classificacao_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;

	if	(nvl(ie_opcao_p, 'C') = 'C') then
		ds_retorno_w	:= nr_seq_classificacao_w;
	elsif	(nvl(ie_opcao_p, 'C') = 'D') then
		select 	max(ds_classificacao)
		into	ds_classificacao_w
		from 	classificacao_atendimento
		where 	nr_sequencia = nr_seq_classificacao_w;
		
		ds_retorno_w	:= ds_classificacao_w;
	end if;
end if;

RETURN	ds_retorno_w;

END Obter_Classificacao_Atend;
/