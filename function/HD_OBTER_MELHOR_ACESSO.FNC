create or replace 
function HD_OBTER_MELHOR_ACESSO (cd_pessoa_fisica_p	varchar2,
				ie_opcao_p		varchar2)
				return			varchar2 is

/*
	C -> Retorna a sequencia do acesso
	D -> Retornar a descricao tecnica concatenada com o local
*/

nr_seq_acesso_w			number(10);
ds_tecnica_acesso_w		varchar2(80);
ds_local_acesso_w		varchar2(80);
ds_retorno_w			varchar2(160);

begin

select	max(nr_sequencia)
	into	nr_seq_acesso_w
from	hd_acesso
where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	ie_adequado = 'S'
	and	dt_instalacao <= sysdate
	and	dt_perda_retirada is null;

if (nr_seq_acesso_w is null) then
   begin
	select	max(nr_sequencia)
		into	nr_seq_acesso_w
	from	hd_acesso
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_instalacao <= sysdate
		and	dt_perda_retirada is null;
   end;
end if;

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= nr_seq_acesso_w;
elsif	(ie_opcao_p = 'D') then
	begin

	select	substr(c.ds_tecnica_acesso,1,80),
		substr(b.ds_local_acesso,1,80)
	into	ds_tecnica_acesso_w,
		ds_local_acesso_w
	from	hd_tecnica_acesso c,
		hd_local_acesso b,
		hd_acesso a
	where	c.nr_sequencia	= a.nr_seq_tecnica
		and	b.nr_sequencia	= a.nr_seq_local	
		and	a.nr_sequencia	= nr_seq_acesso_w;

	ds_retorno_w	:= ds_tecnica_acesso_w || ' ' || ds_local_acesso_w;
	end;
else
	ds_retorno_w	:= '';
end if;

return ds_retorno_w;
	
end HD_OBTER_MELHOR_ACESSO;
/
