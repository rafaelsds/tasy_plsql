create or replace
function obter_lista_dados_classif_hsl(cd_pessoa_fisica_p		varchar2,
								   ie_campo_retorno_p 	    varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(2000) := '';
ds_classif_w		varchar2(80);
ds_observacao_w		varchar2(255);

Cursor C01 is
	select	substr(obter_classif_pessoa(nr_seq_classif),1,80) ds_classif,
			substr(ds_observacao,1,255) ds_observacao 
	from    pessoa_classif
	where cd_pessoa_fisica = cd_pessoa_fisica_p
	group by substr(obter_classif_pessoa(nr_seq_classif),1,80),
		substr(ds_observacao,1,255)
	order by 1;
	
begin

open C01;
loop
fetch C01 into	
	ds_classif_w,
	ds_observacao_w;
exit when C01%notfound;
	begin
	
	if (ie_campo_retorno_p = 'O') then
		ds_retorno_w := ds_retorno_w || ', ' || ds_observacao_w;	
	elsif (ie_campo_retorno_p = 'D') then
		ds_retorno_w := ds_retorno_w || ', ' || ds_classif_w;		
	end if;
	
	end;
end loop;
close C01;

ds_retorno_w := substr(ds_retorno_w, 2, Length(ds_retorno_w) - 1);

return	ds_retorno_w;

end obter_lista_dados_classif_hsl;
/
