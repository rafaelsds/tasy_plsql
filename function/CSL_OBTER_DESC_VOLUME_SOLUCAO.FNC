Create or replace
FUNCTION Csl_Obter_Desc_Volume_Solucao(
					nr_prescricao_p		Number,
					nr_Seq_solucao_p	Number)
					RETURN VARCHAR2 IS


qt_solucao_total_w		number(15,4);
cd_unidade_medida_w		varchar2(30);
qt_tempo_aplicacao_w		number(15,4);
ds_total_w			varchar2(60);
ds_bomba_w			varchar2(40);
ds_dosagem_w			varchar2(60);
ds_solucao_w			varchar2(100);
ds_retorno_w			Varchar2(255);
ie_esquema_alternado_w		Varchar2(1);
ds_acm_agora_w			Varchar2(20);
ie_acm_sn_w			Varchar2(1);
qt_dosagem_w		Number(15,4);

BEGIN
if	(nvl(nr_prescricao_p,0) <> 0) then
	begin
	select	qt_dosagem,	
		qt_solucao_total,
		cd_unidade_medida,
		qt_tempo_aplicacao,
		ds_solucao,
		decode(ie_bomba_infusao, 'S' , 'em bomba infus�o','A','em bomba seringa', ''),
		decode(upper(ie_tipo_dosagem), 'ACM', '(ACM)', qt_dosagem  || DECODE(upper(ie_tipo_dosagem),'MGM','mcg/m', DECODE(upper(ie_tipo_dosagem),'MLH','ml/h',DECODE(upper(ie_tipo_dosagem),'GTM','gt/m',ie_tipo_dosagem))) || decode(ie_acm, 'S', ' ACM', '')) || decode(ie_urgencia,'S',' Agora','') || decode(ie_se_necessario,'S',' SN',''),
		decode(ie_acm, 'S', ' ACM', '') || decode(ie_urgencia,'S',' Agora','') || decode(ie_se_necessario,'S','SN',''),
		ie_esquema_alternado,
		obter_se_acm_sn(ie_acm, ie_se_necessario)
	into	qt_dosagem_w,	
		qt_solucao_total_w,
		cd_unidade_medida_w,
		qt_tempo_aplicacao_w,
		ds_solucao_w,
		ds_bomba_w,
		ds_dosagem_w,
		ds_acm_agora_w,
		ie_esquema_alternado_w,
		ie_acm_sn_w
	from 	prescr_solucao
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_seq_solucao	= nr_seq_solucao_p;

	if 	(nvl(qt_solucao_total_w, 0) <> 0) then
		ds_total_w	:= 'Vol. ' || qt_solucao_total_w || cd_unidade_medida_w || '/'|| qt_tempo_aplicacao_w || 'h';
	else
		ds_total_w	:= '';
	end if;
	
	if	(((ie_esquema_alternado_w = 'N') and
		(ie_acm_sn_w = 'S')) or
		(qt_dosagem_w is null)) then
		ds_retorno_w	:= ds_total_w || ' ' || ds_dosagem_w || ' ' || ds_bomba_w;
	elsif (ie_esquema_alternado_w = 'N') and
		  (ie_acm_sn_w = 'N') then
		ds_retorno_w	:= ds_total_w || ' a ' || ds_dosagem_w || ' ' || ds_bomba_w;
	else 
		ds_retorno_w	:= ds_total_w || ' Esquema Alternado ' || ds_acm_agora_w ||' '|| ds_bomba_w;
	end if;
	/*if	(ds_solucao_w is not null) then
		ds_retorno_w	:= ds_retorno_w || ' (' || ds_solucao_w || ')';
	end if;*/
	end;
end if;

RETURN replace(replace(replace(ds_retorno_w,' .',' 0,'),' ,',' 0,'),'/,','/0,');
END Csl_Obter_Desc_Volume_Solucao;
/