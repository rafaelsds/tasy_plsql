create or replace
function pls_obter_se_limitacao
			(	nr_seq_segurado_p		number,
				nr_seq_tipo_limitacao_p		number,
				qt_permitida_p			number,
				qt_intervalo_p			number,
				ie_periodo_p			varchar2,
				qt_solicitada_p			number,
				ie_tipo_guia_p			varchar2,
				nr_seq_guia_p			number,
				ie_tipo_consistencia_p		varchar2,
				ie_tipo_item_p			varchar2,
				ie_tipo_periodo_p		varchar2,
				nr_seq_limitacao_p		number)
				return varchar2 is

ie_limitacao_w			varchar2(1);
ie_tipo_guia_w			varchar2(2);
nr_seq_guia_w			number(10);
nr_seq_guia_proc_w		number(10);
qt_liberada_w			number(7,3) := 0;
qt_autorizada_w			number(7,3);
qt_permitida_w			number(9,3);
dt_inicial_w			date;
dt_final_w			date;
dt_contratacao_w		date;
dt_inicio_w			date;
dt_atual_w			date;
dt_guia_w			date;
qt_dias_autorizados_w		number(5);
qt_dia_solicitado_w		number(5);
cd_doenca_w			varchar2(10);
nr_seq_diagnostico_w		number(10);
qt_dias_autorizados_ww		number(5);
ie_tipo_incidencia_w		varchar2(1);
nr_seq_segurado_w		number(10);
nr_seq_titular_w		number(10);
nr_seq_contrato_w		Number(10);
nr_seq_guia_mat_w		Number(10);
ie_tipo_periodo_w		varchar2(1):= 'N';
ie_intercambio_w		varchar2(1);
nr_seq_intercambio_w		number(10);

Cursor c01 is
	select	b.nr_sequencia,
		b.qt_dia_solicitado,
		a.nr_sequencia
	from	pls_guia_plano	b,
		pls_segurado	a
	where	a.nr_sequencia	= b.nr_seq_segurado
	and	a.nr_sequencia	= nr_seq_segurado_p
	and	b.ie_estagio in (2,3,5,6)
	and	nvl(ie_tipo_incidencia_w,'B')	= 'B'
	and	b.ie_status = '1'
	and	b.dt_autorizacao between dt_inicial_w and fim_dia(dt_final_w)
	and	b.nr_sequencia <> nr_seq_guia_p
	union all
	select	b.nr_sequencia,
		b.qt_dia_solicitado,
		a.nr_sequencia
	from	pls_guia_plano	b,
		pls_segurado	a
	where	a.nr_sequencia		= b.nr_seq_segurado
	and	((a.nr_seq_contrato	= nr_seq_contrato_w  and nr_seq_contrato_w is not null) 
	or	(a.nr_seq_intercambio	= nr_seq_intercambio_w and nr_seq_intercambio_w is not null))
	and	b.ie_estagio in (2,3,5,6)
	and	nvl(ie_tipo_incidencia_w,'B') = 'T'
	and	b.ie_status = '1'
	and	b.dt_autorizacao between dt_inicial_w and fim_dia(dt_final_w)
	and	((a.nr_seq_titular = nr_seq_segurado_p)
	or	(a.nr_seq_titular = nr_seq_titular_w)
	or	(a.nr_sequencia = nr_seq_titular_w))
	and	b.nr_sequencia <> nr_seq_guia_p
	union all
	select	b.nr_sequencia,
		b.qt_dia_solicitado,
		a.nr_sequencia
	from	pls_guia_plano	b,
		pls_segurado	a
	where	a.nr_sequencia	= b.nr_seq_segurado
	and	a.nr_sequencia	= nr_seq_segurado_p
	and	nvl(ie_tipo_incidencia_w,'B')	= 'B'
	and	b.nr_sequencia = nr_seq_guia_p
	union all
	select	b.nr_sequencia,
		b.qt_dia_solicitado,
		a.nr_sequencia
	from	pls_guia_plano	b,
		pls_segurado	a
	where	a.nr_sequencia		= b.nr_seq_segurado
	and	((a.nr_seq_contrato	= nr_seq_contrato_w  and nr_seq_contrato_w is not null) 
	or	(a.nr_seq_intercambio	= nr_seq_intercambio_w and nr_seq_intercambio_w is not null))
	and	nvl(ie_tipo_incidencia_w,'B') = 'T'
	and	((a.nr_seq_titular = nr_seq_segurado_p)
	or	(a.nr_seq_titular = nr_seq_titular_w)
	or	(a.nr_sequencia = nr_seq_titular_w))
	and	b.nr_sequencia = nr_seq_guia_p;

Cursor c02 is
	select	pls_obter_regra_limitacao(a.cd_procedimento,null,a.ie_origem_proced,nr_seq_segurado_w,null),
		sum(nvl(qt_autorizada,0))
	from	pls_guia_plano_proc a
	where	nr_seq_guia	= nr_seq_guia_w
	and	pls_obter_regra_limitacao(a.cd_procedimento,null,a.ie_origem_proced,nr_seq_segurado_w,null) = nr_seq_tipo_limitacao_p
	and	a.ie_status in ('L','S','P')
	group	by pls_obter_regra_limitacao(a.cd_procedimento,null,a.ie_origem_proced,nr_seq_segurado_w,null);

Cursor C03 is
	select	pls_obter_regra_limitacao(null,null,null,nr_seq_segurado_w,a.cd_doenca),
		sum(nvl(b.qt_dia_solicitado,0))
	from	pls_diagnostico a,
		pls_guia_plano b
	where	a.nr_seq_guia	= b.nr_sequencia
	and	a.nr_seq_guia	= nr_seq_guia_w
	and	pls_obter_regra_limitacao(null,null,null,nr_seq_segurado_w,a.cd_doenca) = nr_seq_tipo_limitacao_p
	group	by pls_obter_regra_limitacao(null,null,null,nr_seq_segurado_w,a.cd_doenca);

Cursor C04 is
	select	pls_obter_regra_limitacao(null,a.nr_seq_material,null,nr_seq_segurado_w,null),
		sum(nvl(qt_autorizada,0))
	from	pls_guia_plano_mat a
	where	nr_seq_guia	= nr_seq_guia_w
	and	pls_obter_regra_limitacao(null,a.nr_seq_material,null,nr_seq_segurado_w,null) = nr_seq_tipo_limitacao_p
	and	a.ie_status in ('L','S','P')
	group	by pls_obter_regra_limitacao(null,a.nr_seq_material,null,nr_seq_segurado_w,null);
begin

ie_limitacao_w	:= 'N';
qt_permitida_w	:= nvl(qt_permitida_p,0);
ie_tipo_guia_w	:= nvl(ie_tipo_guia_p,'X');
qt_dias_autorizados_w	:= 0;

select	nvl(dt_autorizacao,dt_solicitacao)
into	dt_guia_w
from	pls_guia_plano
where	nr_sequencia = nr_seq_guia_p;

if	(nr_seq_segurado_p is not null) then
	select	nvl(nr_seq_titular,nr_seq_segurado_p),
		nr_seq_contrato,
		nr_seq_intercambio    
		--pls_obter_se_segurado_intercam(null,null,nr_seq_segurado_p)
	into	nr_seq_titular_w,
		nr_seq_contrato_w,
		nr_seq_intercambio_w
		--ie_intercambio_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
	
	-- Tratamento para verificar o per�odo a ser consistido
	-- Consistir pelo ano contratual
	if	(ie_periodo_p = 'CO') then
		/*
		ie_tipo_periodo_w 
		M - Meses
		S - Semanas
		D - dias
		Tratamento para definir o per�odo de meses ap�s a data de contrata��o
		Tambem � tratado os periodos de dias e semanas
		Ex: 	Contrata��o 		= 20/05/2006
			Qt meses per�odo 	= 3
			Per�odo 1 		= 20/05/2006 at� 19/08/2006
			Per�odo 2 		= 20/08/2006 at� 19/11/2006
			Per�odo 3 		= 20/11/2006 at� 19/02/2007
			...
		*/
		
		if	(nvl(nr_seq_contrato_w,0) > 0) then
			select	trunc(dt_contrato,'dd'),
				trunc(dt_guia_w)
			into	dt_contratacao_w,
				dt_atual_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_w;
		elsif	(nvl(nr_seq_intercambio_w,0) > 0) then
			select	trunc(dt_inclusao,'dd'),
				trunc(dt_guia_w)
			into	dt_contratacao_w,
				dt_atual_w
			from	pls_intercambio
			where	nr_sequencia	= nr_seq_intercambio_w;
		end if;
		
		dt_inicio_w		:= dt_contratacao_w;
		
		if	(ie_tipo_periodo_p = 'M') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	add_months(dt_inicio_w,qt_intervalo_p)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	add_months(dt_inicio_w, -qt_intervalo_p),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'S') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	dt_inicio_w + (qt_intervalo_p*7)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - (qt_intervalo_p*7),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'D') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	dt_inicio_w + qt_intervalo_p
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - qt_intervalo_p,
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		end if;
	-- Consistir pelo ano atual
	elsif (ie_periodo_p = 'CA') then
		/*
		Tratamento para definir o per�odo de meses do ano calend�rio
		Ex:	Contrata��o 	= 20/05/2006
			Qt meses per�odo 	= 3
			Per�odo 1 		= 01/01/2006 at� 31/03/2006
			Per�odo 2 		= 01/04/2006 at� 31/06/2006
			Per�odo 3 		= 01/07/2006 at� 31/09/2007
			Per�odo 4 		= 01/10/2006 at� 31/12/2007
			...
		*/
		dt_atual_w	:= trunc(dt_guia_w);
		dt_inicio_w	:= trunc(dt_guia_w,'year');
		if	(ie_tipo_periodo_p = 'M') then
			if	(qt_intervalo_p = 1) then
				dt_inicial_w	:= trunc(dt_guia_w,'month');
				dt_final_w	:= last_day(trunc(dt_guia_w,'month'));
			elsif	(qt_intervalo_p in (2,3,4,6)) then
				dt_inicio_w	:= trunc(dt_guia_w,'year');
				
				while	(dt_inicio_w <= dt_atual_w) loop
					begin
					select	add_months(dt_inicio_w,qt_intervalo_p)
					into	dt_inicio_w
					from	dual;
					end;
				end loop;
				
				select	add_months(dt_inicio_w,-qt_intervalo_p),
					dt_inicio_w -1
				into	dt_inicial_w,
					dt_final_w
				from	dual;
				
			elsif	(qt_intervalo_p = 12) then
				dt_inicial_w	:= trunc(dt_guia_w,'year');
				dt_final_w	:= last_day(add_months(trunc(dt_guia_w,'year'),qt_intervalo_p));
			end if;
		elsif	(ie_tipo_periodo_p = 'S') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	dt_inicio_w + (qt_intervalo_p*7)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - (qt_intervalo_p*7),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'D') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	dt_inicio_w + qt_intervalo_p
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - qt_intervalo_p,
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		end if;
	--Data de ades�o do benefici�rio
	elsif	(ie_periodo_p = 'A') then
		select	trunc(dt_contratacao,'dd'),
			trunc(dt_guia_w)
		into	dt_contratacao_w,
			dt_atual_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_p;
		
		dt_inicio_w		:= dt_contratacao_w;
		
		if	(ie_tipo_periodo_p = 'M') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	add_months(dt_inicio_w,qt_intervalo_p)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	add_months(dt_inicio_w, -qt_intervalo_p),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'S') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	dt_inicio_w + (qt_intervalo_p*7)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - (qt_intervalo_p*7),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'D') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	dt_inicio_w + qt_intervalo_p
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - qt_intervalo_p,
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		end if;
	--Data da primeira utiliza��o
	elsif	(ie_periodo_p = 'D') then
		dt_atual_w	:= trunc(dt_guia_w);
		
		/*aaschlote  05/09/2012 - Conforme conversado com o Eder, foi colocado a data atual, pois � a mais pr�xima da data de autoriza��o*/
		pls_gravar_primeira_limi_benef(nr_seq_segurado_p,nr_seq_limitacao_p,nr_seq_guia_p,sysdate,'TASY');
		
		select	max(dt_primeira_utilizacao)
		into	dt_inicio_w
		from	pls_segurado_limitacao
		where	nr_seq_segurado		= nr_seq_segurado_p
		and	nr_seq_limitacao	= nr_seq_limitacao_p;
		
		if	(ie_tipo_periodo_p = 'M') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	add_months(dt_inicio_w,qt_intervalo_p)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	add_months(dt_inicio_w, -qt_intervalo_p),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'S') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	dt_inicio_w + (qt_intervalo_p*7)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - (qt_intervalo_p*7),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'D') then
			while	(dt_inicio_w <= dt_atual_w) loop
				select	dt_inicio_w + qt_intervalo_p
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - qt_intervalo_p,
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		end if;
	end if;
	
	if	(ie_tipo_guia_w = 'X') then
		begin
		
		select	ie_tipo_incidencia
		into	ie_tipo_incidencia_w
		from	pls_tipo_limitacao
		where	nr_sequencia	= nr_seq_tipo_limitacao_p;
		
		open c01;
		loop
		fetch c01 into
			nr_seq_guia_w,
			qt_dia_solicitado_w,
			nr_seq_segurado_w;
		exit when c01%notfound;
			
			if	(ie_tipo_consistencia_p = 'C') then
				open C03;
				loop
				fetch C03 into
					nr_seq_diagnostico_w,
					qt_dia_solicitado_w;
				exit when C03%notfound;
					qt_dias_autorizados_w := (qt_dias_autorizados_w + nvl(qt_dia_solicitado_w,0));
				end loop;
				close C03;
			else
				if	(ie_tipo_item_p	= 'P') then
					open c02;
					loop
					fetch c02 into
						nr_seq_guia_proc_w,
						qt_autorizada_w;
					exit when c02%notfound;
						qt_liberada_w	:= nvl(qt_liberada_w,0) + nvl(qt_autorizada_w,0);
					end loop;
					close c02;
				elsif	(ie_tipo_item_p	= 'M') then
					open c04;
					loop
					fetch c04 into
						nr_seq_guia_mat_w,
						qt_autorizada_w;
					exit when c04%notfound;
						qt_liberada_w	:= nvl(qt_liberada_w,0) + nvl(qt_autorizada_w,0);
					end loop;
					close c04;
				end if;
			end if;
		end loop;
		close c01;
		
		if	(ie_tipo_consistencia_p = 'Q') then
			qt_liberada_w	:= nvl(qt_liberada_w,0) + nvl(qt_solicitada_p,0);
		elsif	(ie_tipo_consistencia_p = 'C') then
			qt_dias_autorizados_w := (qt_dias_autorizados_w + nvl(qt_solicitada_p,0));
		end if;
		
		end;
	else
		if	(ie_tipo_consistencia_p = 'Q') then
			select	count(*)
			into	qt_liberada_w
			from	pls_guia_plano
			where	nr_seq_segurado		= nr_seq_segurado_p
			and	dt_autorizacao between	dt_inicial_w and fim_dia(dt_final_w)
			and	ie_status		= '1'
			and	ie_tipo_guia		= ie_tipo_guia_w;
			
			qt_liberada_w	:= qt_liberada_w + 1;
		elsif	(ie_tipo_consistencia_p = 'C') then
			select	nvl(sum(qt_dia_autorizado),0)
			into	qt_dias_autorizados_ww
			from	pls_guia_plano
			where	nr_seq_segurado		= nr_seq_segurado_p
			and	dt_autorizacao between	dt_inicial_w and fim_dia(dt_final_w)
			and	ie_status		= '1'
			and	ie_tipo_guia		= ie_tipo_guia_w;
			
			qt_dias_autorizados_w := (qt_dias_autorizados_ww + nvl(qt_solicitada_p,0));
		end if;
	end if;
	
	if	((qt_permitida_w < qt_liberada_w) and (ie_tipo_consistencia_p = 'Q')) or
		((qt_permitida_w < qt_dias_autorizados_w) and (ie_tipo_consistencia_p = 'C')) then
		ie_limitacao_w	:= 'S';
	end if;
end if;

return ie_limitacao_w;

end pls_obter_se_limitacao;
/