create or replace 
function obter_proj_rec_oc_nf(nr_sequencia_inspec_receb_p number)
return varchar2 is

ds_projeto_w     projeto_recurso.ds_projeto%type;
nr_seq_projeto_w projeto_recurso.nr_sequencia%type;

begin

if (nvl(nr_sequencia_inspec_receb_p, 0) <> 0) then

    select substr(obter_desc_proj_recurso(a.nr_seq_proj_rec),1,80) ds_projeto,
           a.nr_seq_proj_rec nr_seq_projeto
    into   ds_projeto_w,
           nr_seq_projeto_w
    from   inspecao_recebimento c, 
           ordem_compra_item a 
    where  c.nr_ordem_compra = a.nr_ordem_compra
    and    c.nr_sequencia = nr_sequencia_inspec_receb_p
    and    (c.nr_ordem_compra is not null 
    and    c.nr_item_oci = a.nr_item_oci);
    
    if (ds_projeto_w is null) then
    
    select substr(obter_desc_proj_recurso(b.nr_seq_proj_rec),1,80) ds_projeto,
           b.nr_seq_proj_rec nr_seq_projeto
    into   ds_projeto_w,
           nr_seq_projeto_w
    from   inspecao_recebimento c, 
           nota_fiscal_item b 
    where  c.nr_nota_fiscal = b.nr_nota_fiscal
    and    c.nr_sequencia = nr_sequencia_inspec_receb_p
    and    c.nr_seq_nota_fiscal = b.nr_sequencia
    and    (c.nr_nota_fiscal is not null
    and    c.nr_seq_item_nf = b.nr_item_nf);
    
    end if;

end if;

if (nr_seq_projeto_w is null) then
    return null;
end if;

return nr_seq_projeto_w || ' - ' || ds_projeto_w;

end obter_proj_rec_oc_nf;
 /
