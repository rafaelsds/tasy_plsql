create or replace 
function obter_medico_protocolo(nr_prescricao_p		number,
				ie_opcao_p		varchar2) 
				return varchar2 is
ds_resultado_w		varchar2(80);
nr_seq_paciente_w	number(10);
/* 	ie_opcao_p:
	C = C�digo
	N = Nome
*/
begin
if	(nr_prescricao_p is not null) and
	(ie_opcao_p is not null) then
	select	max(a.nr_seq_paciente)
	into	nr_seq_paciente_w
	from	paciente_atendimento a
	where	a.nr_prescricao = nr_prescricao_p;
	if	(nr_seq_paciente_w is not null) then
		if	(ie_opcao_p = 'C')then
			select	max(p.cd_medico_resp)
			into	ds_resultado_w
			from	paciente_setor p
			where	p.nr_seq_paciente 	= nr_seq_paciente_w;
		elsif	(ie_opcao_p = 'N') then
			select	max(substr(obter_nome_pf(p.cd_medico_resp),1,80))
			into	ds_resultado_w
			from	paciente_setor p
			where	p.nr_seq_paciente = nr_seq_paciente_w;
		end if;
	end if;
end if;
return ds_resultado_w;
end obter_medico_protocolo;
/