create or replace
function obter_label_grid_atributo (
	nm_atributo_p	varchar2,
	nr_seq_visao_p	number) 
	return varchar2 is
	
ds_label_w	varchar2(60);

begin
if	(nm_atributo_p is not null) then
	begin
	if	(nr_seq_visao_p is not null) then
		begin
		select	nvl(max(ds_label_grid),obter_desc_expressao(327119)/*'N�o informado'*/)
		into	ds_label_w
		from	tabela_visao_atributo
		where	nm_atributo = nm_atributo_p
		and	nr_sequencia = nr_seq_visao_p;
		end;
	else
		begin
		select	nvl(max(ds_label_grid),obter_desc_expressao(327119)/*'N�o informado'*/)
		into	ds_label_w
		from	tabela_atributo
		where	nm_atributo = nm_atributo_p;		
		end;
	end if;
	end;
end if;
return ds_label_w;
end obter_label_grid_atributo;
/