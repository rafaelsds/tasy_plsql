create or replace
function pls_obter_val_honor_ocor(	nr_seq_honorario_crit_p	pls_conta_proc.nr_seq_honorario_crit%type,
					vl_procedimento_p	pls_conta_proc.vl_procedimento%type,
					vl_total_partic_p	pls_conta_proc.vl_total_partic%type,
					vl_materiais_p		pls_conta_proc.vl_materiais%type,
					vl_custo_operacional_p	pls_conta_proc.vl_custo_operacional%type) 
					return pls_tipos_ocor_pck.dados_valor_glosa_9919 is

					 
ie_repassa_medico_w		pls_regra_honorario.ie_repassa_medico%type;
vl_repasse_glosa_w		number(15,2);
ds_observacao_w			varchar2(1000);
dados_valor_glosa_9919_w	pls_tipos_ocor_pck.dados_valor_glosa_9919;

begin

select	max(ie_repassa_medico)
into	ie_repassa_medico_w
from	pls_regra_honorario
where	nr_sequencia	= nr_seq_honorario_crit_p;

if	(ie_repassa_medico_w	= 'S') then
	vl_repasse_glosa_w		:= vl_procedimento_p;
elsif	(ie_repassa_medico_w	= 'H') then
	ds_observacao_w	:= ds_observacao_w||'3)Regra de honor�rio paga ao m�dico executor os honor�rio m�dicos, por�m o mesmo n�o foi calculado.';
	vl_repasse_glosa_w		:= vl_total_partic_p;
elsif	(ie_repassa_medico_w	= 'N') then
	ds_observacao_w	:= ds_observacao_w||'3)Regra de honor�rio paga ao m�dico executor o CO e Filme, por�m o mesmo n�o foi calculado.';
	vl_repasse_glosa_w		:= nvl(vl_materiais_p,0) + nvl(vl_custo_operacional_p,0);
end if;

dados_valor_glosa_9919_w.vl_glosa 	:= vl_repasse_glosa_w;
dados_valor_glosa_9919_w.ds_observacao	:= ds_observacao_w;	

return dados_valor_glosa_9919_w;

end pls_obter_val_honor_ocor;
/