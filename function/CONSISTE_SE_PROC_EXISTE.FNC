create or replace
function Consiste_se_proc_existe(cd_procedimento_p	number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);
qt_proc_w	number(10,0);
			
begin

select	count(*)
into	qt_proc_w
from	procedimento
where	cd_procedimento = cd_procedimento_p;

if	(qt_proc_w = 0) then
	ie_retorno_w := 'N';
else
	ie_retorno_w := 'S';
end if;

return	ie_retorno_w;

end Consiste_se_proc_existe;
/