create or replace
function Obter_ds_local_senha( ds_maquina_chamada_p		varchar2)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(80);			
			
begin

	SELECT	ds_local
	into 	ds_retorno_w
	FROM	maquina_local_senha a,
			computador b
	WHERE	a.nr_seq_computador	= b.nr_sequencia
	AND		NVL(a.ie_situacao, 'A') = 'A'
	AND 	nm_computador = ds_maquina_chamada_p ;

return	ds_retorno_w;

end Obter_ds_local_senha;
/