create or replace
function sus_obter_desc_grupo_laudo (	nr_sequencia_p		number)
 		    			return varchar2 is
					
ds_retorno_w		varchar2(255) := '';					

begin

begin
select	substr(ds_grupo_laudo,1,255)
into	ds_retorno_w
from 	sus_grupo_laudo_padrao
where 	nr_sequencia = nr_sequencia_p;
exception
when others then
	ds_retorno_w := '';
end;

return	ds_retorno_w;

end sus_obter_desc_grupo_laudo;
/