create or replace
function Consiste_Tempo_Exames_Ag_Exame(
					cd_agenda_p		Number,
					hr_Agenda_p		Date,
					nr_minuto_duracao_p	Number,
					nr_seq_proc_interno_p	number,
					nr_seq_agenda_p		number,
					cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		Varchar2)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(1)	:= 'S';
hr_agenda_w		Date;
nr_minuto_duracao_w	Number(10,0);
qt_sobra_w		Number(10,0);
nr_seq_agenda_w		number(10,0);
nr_Seq_regra_w		Number(10,0);
nr_seq_proc_interno_w	number(10,0);
qt_Tempo_w		Number(10,0);
ie_regra_exclusiva_w	ageint_tempo_entre_exames.ie_regra_exclusiva%type;
ie_tempo_exclusivo_w	ageint_tempo_entre_exames.ie_tempo_exclusivo%type;
cd_exame_pri_w		ageint_tempo_entre_exames.cd_exame_pri%type;
cd_exame_seg_w		ageint_tempo_entre_exames.cd_exame_seg%type;

Cursor C01 is
	select	nr_Sequencia,
		ie_regra_exclusiva,
		ie_tempo_exclusivo,
		cd_exame_pri,
		cd_exame_seg
	from	ageint_tempo_entre_exames
	where	((cd_exame_pri	= nr_Seq_proc_interno_p)
	or	(cd_exame_seg = nr_seq_proc_interno_p))
	and	ie_situacao	= 'A';

Cursor C02 is
	select	b.hr_inicio,
		b.nr_minuto_duracao,
		b.nr_sequencia
	from	agenda_paciente b,
		agenda a
	where	a.cd_agenda		= b.cd_agenda
	and	a.cd_tipo_agenda	= 2
	and	b.nr_seq_proc_interno	= nr_seq_proc_interno_w
	and	b.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	b.nr_sequencia		<> nr_seq_agenda_p
	and	b.ie_status_agenda not in ('C','I');
		
begin

/*
cursor das regras a qual o exame do item se aplica
*/
open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	ie_regra_exclusiva_w,
	ie_tempo_exclusivo_w,
	cd_exame_pri_w,
	cd_exame_seg_w;
exit when C01%notfound;
	begin
	select	decode(cd_Exame_pri, nr_Seq_proc_interno_p, cd_exame_Seg, cd_exame_pri),
		qt_tempo
	into	nr_seq_proc_interno_w,
		qt_tempo_w
	from	ageint_tempo_entre_exames
	where	nr_sequencia	= nr_seq_regra_w;
	
	/*
	cursor das marcacoes com os exames ao qual a regra se aplica
	*/
	open C02;
	loop
	fetch C02 into	
		hr_agenda_w,
		nr_minuto_duracao_w,
		nr_seq_agenda_w;
	exit when C02%notfound;
		begin
		if (ie_regra_exclusiva_w = 'N') or 
			(cd_exame_pri_w = nr_seq_proc_interno_w and hr_agenda_w >= hr_agenda_p) or
			(cd_exame_seg_w = nr_seq_proc_interno_w and hr_agenda_w <= hr_agenda_p) then
			if	(ds_retorno_w	<> 'N') then
				if	(hr_agenda_w	< hr_Agenda_p) then
					select	Obter_Min_Entre_Datas((hr_agenda_w + nr_minuto_duracao_w / 1440), hr_Agenda_p, 1)
					into	qt_sobra_w
					from	dual;		
				else
					select	Obter_Min_Entre_Datas((hr_Agenda_p + nr_minuto_duracao_p / 1440), hr_agenda_w, 1)
					into	qt_sobra_w
					from	dual;		
				end if;
				
				if	(qt_sobra_w	< qt_tempo_w) or (((qt_sobra_w - qt_tempo_w) <> 0) and (ie_tempo_exclusivo_w = 'S')) then
					ds_retorno_w	:= 'N';
				else
					ds_Retorno_w	:= 'S';
				end if;
			end if;
		end if;
		end;
	end loop;
	close C02;
			
	end;
end loop;
close C01;

return	ds_retorno_w;

end Consiste_Tempo_Exames_Ag_Exame;
/
