create or replace 
function obter_tempo_problema_ativo (nr_seq_problema_p number)
					return varchar2 is
	dt_inicio_w		date;
	dt_fim_w		date;
	qt_diff_days_w	number(5);
	qt_diff_month_w	number(5);
	qt_diff_year_w	number(5);
	
	ds_retorno_w varchar2(255) := '';
begin
	select	nvl(max(nvl(obter_dt_origem_problema(nr_sequencia,'O'),dt_inicio)),''),
			nvl(max(dt_fim),sysdate)
	into	dt_inicio_w,
			dt_fim_w
	from	lista_problema_pac 
	where	nr_sequencia = nr_seq_problema_p;

	if (dt_inicio_w is not null and dt_inicio_w <= dt_fim_w) then
		select	pkg_date_utils.get_DiffDate(dt_inicio_w,nvl(dt_fim_w,sysdate),'YEAR'),
				pkg_date_utils.get_DiffDate(dt_inicio_w,nvl(dt_fim_w,sysdate),'MONTH%'),
				pkg_date_utils.get_DiffDate(dt_inicio_w,nvl(dt_fim_w,sysdate),'DAY%')
		into	qt_diff_year_w,
				qt_diff_month_w,
				qt_diff_days_w
		from	dual;
		
		if (qt_diff_year_w is not null and qt_diff_year_w >= 1) then
			select	qt_diff_year_w || obter_desc_expressao(770641) 
			into	ds_retorno_w
			from	dual;
		end if;
		
		if (qt_diff_month_w is not null and qt_diff_month_w >= 1) then
			select	decode(ds_retorno_w, null, ds_retorno_w, ds_retorno_w || ' ') || qt_diff_month_w ||  obter_desc_expressao(347200) 
			into	ds_retorno_w
			from	dual;
		end if;
		
		if (qt_diff_days_w is not null and qt_diff_days_w >=1 ) then
			select	decode(ds_retorno_w, null, ds_retorno_w, ds_retorno_w || ' ') || qt_diff_days_w || obter_desc_expressao(770638) 
			into	ds_retorno_w
			from	dual;
		end if;
			
	end if; 
	
	return ds_retorno_w;

end obter_tempo_problema_ativo;
/