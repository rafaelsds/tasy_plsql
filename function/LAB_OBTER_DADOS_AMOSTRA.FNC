create or replace function lab_obter_dados_amostra (nr_prescricao_p		number,
						nr_seq_material_p	number,
						nr_seq_grupo_p		number,
						ie_opcao_p	number)
						return varchar2 is

qt_tempo_w	number(4,2);
qt_volume_w	number(5);
ds_retorno_w	varchar2(255);
ie_padrao_amostra_w	varchar(5);

/*
1 - Volume
2 - Tempo
*/

begin


select	max(a.qt_tempo),
	max(a.qt_volume)
into	qt_tempo_w,
	qt_volume_w
from 	prescr_proc_material a,
	material_exame_lab b
where 	a.nr_prescricao 				= nr_prescricao_p
and	a.nr_Seq_material				= b.nr_sequencia
and	ie_volume_tempo				=	'S'
and	a.nr_seq_material				= nr_seq_material_p
and	((decode(a.nr_seq_grupo,0,nr_seq_grupo_p,a.nr_seq_grupo) 	= nr_seq_grupo_p) or (a.nr_seq_grupo is null));

if (qt_tempo_w is null) and
   (qt_volume_w is null) then
	
	select 	MAX(a.ie_padrao_amostra)
	into 	ie_padrao_amostra_w
	from 	lab_parametro a,
		prescr_medica b
	where 	a.cd_estabelecimento = b.cd_estabelecimento
	and	b.nr_prescricao = nr_prescricao_p;

	if (ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')) then
		select	max(a.qt_tempo),
			max(a.qt_volume)
		into	qt_tempo_w,
			qt_volume_w
		from 	prescr_proc_material a,
			material_exame_lab b
		where 	a.nr_prescricao 			= nr_prescricao_p
		and	a.nr_Seq_material			= b.nr_sequencia
		and	ie_volume_tempo				=	'S'
		and	a.nr_seq_material			= nr_seq_material_p;
	end if;
end if;

if	(ie_opcao_p = 1) then
	ds_retorno_w := qt_volume_w;
else
	ds_retorno_w := qt_tempo_w;
end if;

return ds_retorno_w;

end;
/