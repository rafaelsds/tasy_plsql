create or replace
function obter_tempo_atend(	cd_setor_atendimento_p		number,
				dt_inicial_p			date,
				dt_final_p			date,
				ie_tipo_atendimento_p		number,
				ie_opcao_p			varchar2)
				return varchar2 is
-- Recece horas do cursor				
qt_horas_w	number(38);
qt_min_w	number(38);
qt_seg_w	number(38);

qt_total_w	number(15);

qt_retorno_w	varchar2(255);

pos_ini_w	number(5);
pos_fim_w	number(5);

segundo_total_w	number(38) := 0;

hora_total_w	varchar2(15);
min_total_w	varchar2(15);
seg_total_w	varchar2(15);

ds_retorno_w	varchar2(255);

Cursor C01 is
	select	obter_dif_data(a.dt_entrada,a.dt_alta,null)
	from	atendimento_paciente a
	where	a.dt_entrada between trunc(dt_inicial_p) and fim_dia(dt_final_p)
	and	((cd_setor_atendimento_p = '0') or (cd_setor_atendimento_p = obter_setor_atendimento(a.nr_atendimento)))
	AND	((ie_tipo_atendimento_p = 0) OR (ie_tipo_atendimento_p = ie_tipo_atendimento))
	AND 	obter_dif_data(a.dt_entrada,a.dt_alta,NULL) IS NOT NULL;
begin

select	count(*)
into	qt_total_w
from	atendimento_paciente a
where	a.dt_entrada between trunc(dt_inicial_p) and fim_dia(dt_final_p)
and	((cd_setor_atendimento_p = '0') or (cd_setor_atendimento_p = obter_setor_atendimento(a.nr_atendimento)))
AND	((ie_tipo_atendimento_p = 0) OR (ie_tipo_atendimento_p = ie_tipo_atendimento));			

open C01;
loop
fetch C01 into	
	qt_retorno_w;
exit when C01%notfound;
	begin
		pos_ini_w 	:= 1;
		pos_fim_w	:= instr(qt_retorno_w,':');
		qt_horas_w 	:= to_number(substr(qt_retorno_w,pos_ini_w,pos_fim_w-1));
		
		pos_ini_w 	:= pos_fim_w+1;
		pos_fim_w	:= instr(substr(qt_retorno_w,pos_ini_w,length(qt_retorno_w)),':');
		qt_min_w 	:= to_number(substr(qt_retorno_w,pos_ini_w,pos_fim_w-1));
		
		
		pos_ini_w 	:= length(qt_retorno_w) - 1;
		pos_fim_w	:= 2;
		qt_seg_w	:= to_number(substr(qt_retorno_w,pos_ini_w,pos_fim_w));
		
		
		segundo_total_w := segundo_total_w + qt_seg_w + ( qt_min_w * 60) + ( qt_horas_w * 3600);
	end;
end loop;
close C01;
	if (ie_opcao_p = 1) then
		seg_total_w 	:= to_char(mod( segundo_total_w ,60),'00'); 
		min_total_w	:= to_char(trunc(mod(segundo_total_w/60,60)),'00'); 
		hora_total_w	:= to_char(trunc(segundo_total_w/60/60));
		ds_retorno_w := trim(hora_total_w)||':'||trim(min_total_w)||':'||trim(seg_total_w);
	elsif (ie_opcao_p = 2) then
		segundo_total_w := round(segundo_total_w / qt_total_w);
		seg_total_w 	:= to_char(mod(segundo_total_w,60),'00'); 
		min_total_w	:= to_char(trunc(mod(segundo_total_w/60,60)),'00'); 
		hora_total_w	:= to_char(trunc(segundo_total_w/60/60)); 
		ds_retorno_w := trim(hora_total_w)||':'||trim(min_total_w)||':'||trim(seg_total_w);
	elsif (ie_opcao_p = 3) then
		ds_retorno_w := qt_total_w;
	end if;	
return	ds_retorno_w;

end obter_tempo_atend;
/