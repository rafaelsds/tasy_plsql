create or replace
function obter_nfe_convenio_receb(	nr_seq_receb_p	number)
				return varchar2 is

nr_nota_fiscal_w			varchar2(255);
ds_notas_fiscais_w			varchar2(4000);
ds_nota_w			varchar2(4000);
ie_contido_w			varchar2(1);

cursor c01 is
select	c.NR_NFE_IMP
from	nota_fiscal c,
	conta_paciente b,	
	convenio_retorno_item a,
	convenio_ret_receb d
where	a.nr_interno_conta	= b.nr_interno_conta
and	b.nr_interno_conta	= c.nr_interno_conta
and	a.nr_seq_retorno	= d.nr_seq_retorno
and	d.nr_seq_receb	= nr_seq_receb_p
union
select	c.NR_NFE_IMP
from	nota_fiscal c,
	conta_paciente b,	
	convenio_retorno_item a,
	convenio_ret_receb d
where	a.nr_interno_conta	= b.nr_interno_conta
and	b.nr_seq_protocolo	= c.nr_seq_protocolo
and	a.nr_seq_retorno	= d.nr_seq_retorno
and	d.nr_seq_receb	= nr_seq_receb_p
union
select	e.NR_NFE_IMP
from	nota_fiscal e,
	titulo_receber d,
	protocolo_convenio c,
	convenio_retorno b,
	convenio_ret_receb a
where	a.nr_seq_receb	= nr_seq_receb_p
and	a.nr_seq_retorno	= b.nr_sequencia
and	b.nr_seq_protocolo	= c.nr_seq_protocolo
and	c.nr_seq_protocolo	= d.nr_seq_protocolo
and	d.nr_seq_nf_saida	= e.nr_sequencia
order	by 1;




cursor	c02 is
select	c.NR_NFE_IMP
from	nota_fiscal c,
	convenio_retorno b,
	convenio_ret_receb a
where	a.nr_seq_retorno	= b.nr_sequencia
and	b.nr_seq_protocolo	= c.nr_seq_protocolo 
and	c.ie_situacao = '1'
and	a.nr_seq_receb	= nr_seq_receb_p
union
select	d.NR_NFE_IMP
from	nota_fiscal d,
	conta_paciente c,
	convenio_retorno b,
	convenio_ret_receb a
where	a.nr_seq_retorno	= b.nr_sequencia
and	b.nr_seq_protocolo	= c.nr_seq_protocolo
and	c.nr_interno_conta	= d.nr_interno_conta
and	d.ie_situacao = '1'
and	a.nr_seq_receb	= nr_seq_receb_p
union 
select	e.NR_NFE_IMP
from	nota_fiscal e,
	titulo_receber c,
	convenio_retorno b,
	convenio_ret_receb a
where	a.nr_seq_retorno	= b.nr_sequencia
and	b.nr_seq_protocolo	= c.nr_seq_protocolo
and	c.nr_seq_nf_saida	= e.nr_sequencia
and  	nvl(c.nr_nota_fiscal,'0') <> '0'
and	a.nr_seq_receb	= nr_seq_receb_p
union
select	c.NR_NFE_IMP
from	protocolo_convenio d,
	nota_fiscal c,
	convenio_retorno b,
	convenio_ret_receb a
where	a.nr_seq_retorno	= b.nr_sequencia
and	b.nr_seq_protocolo	= d.nr_seq_protocolo
and	c.nr_seq_lote_prot	= d.nr_seq_lote_protocolo
and	c.ie_situacao = '1'
and	a.nr_seq_receb	= nr_seq_receb_p;


begin

ds_notas_fiscais_w	:= '';
open c01;
loop
fetch c01 into
	nr_nota_fiscal_w;
exit when c01%notfound;
	if	(ds_notas_fiscais_w is not null) then
		ds_notas_fiscais_w	:= substr(ds_notas_fiscais_w || ', ' || nr_nota_fiscal_w, 1, 255);
	else
		ds_notas_fiscais_w	:= nr_nota_fiscal_w;
	end if;
end loop;
close c01;

open c02;
loop
fetch c02 into
	ds_nota_w;
exit when c02%notfound;
	if	(ds_notas_fiscais_w is not null) then
		
		ie_contido_w		:= substr(ctb_obter_se_elemento_contido(ds_nota_w, ds_notas_fiscais_w),1,1);
		if	(ie_contido_w <> 'S') then
			ds_notas_fiscais_w	:= substr(ds_notas_fiscais_w || ', ' || ds_nota_w, 1, 255);
		end if;
	else
		ds_notas_fiscais_w	:= ds_nota_w;
	end if;
end loop;
close c02;

return	ds_notas_fiscais_w;

end obter_nfe_convenio_receb;
/