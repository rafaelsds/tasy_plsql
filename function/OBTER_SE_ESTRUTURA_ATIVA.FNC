create or replace
function obter_se_estrutura_ativa(cd_classe_material_p	number) 
					return varchar2 is

ds_retorno			varchar2(255);
ie_classe_inativa_w			varchar2(2);
ie_subgrupo_inativa_w		varchar2(2);	
ie_grupo_inativo_w			varchar2(2);
cd_subgrupo_material_w		number(5);
cd_grupo_material_w		number(5);


/*CI = Classe inativa
SI = Subgrupo inativo
GI = Grupo inativo
A = quando todos ativos*/
begin

select	max(ie_situacao),
	cd_subgrupo_material
into	ie_classe_inativa_w,
	cd_subgrupo_material_w
from	classe_material
where	cd_classe_material = cd_classe_material_p
group by cd_subgrupo_material;

select	max(ie_situacao),
	cd_grupo_material
into   	ie_subgrupo_inativa_w,
	cd_grupo_material_w	   
from   	subgrupo_material
where	cd_subgrupo_material = cd_subgrupo_material_w
group by cd_grupo_material;

select	max(ie_situacao)
into   	ie_grupo_inativo_w
from   	grupo_material
where	cd_grupo_material = cd_grupo_material_w;

if (ie_classe_inativa_w = 'I') then
	ds_retorno := 'CI';
elsif (ie_subgrupo_inativa_w = 'I') then
	ds_retorno := 'SI';
elsif (ie_grupo_inativo_w = 'I') then
	ds_retorno := 'GI';
end if;	

return nvl(ds_retorno,'A');

end obter_se_estrutura_ativa;
/