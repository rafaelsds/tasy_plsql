create or replace function obter_se_reg_fim_consulta_pepa(
      nr_seq_consulta_pepo_p number,
      nr_seq_item_pronto_p number)
  return varchar2 is

    qt_horas_apos_consulta_p number(5);
    qt_diff_hours_p number(10);
    dt_liberacao_p date;
    ie_lib_p varchar2(1) := 'N';

begin

  select qt_horas_novo_registro
  into qt_horas_apos_consulta_p
  from perfil_item_pepa
  where cd_perfil = wheb_usuario_pck.get_cd_perfil
  and nr_seq_item_pront = nr_seq_item_pronto_p;

  if (nvl(qt_horas_apos_consulta_p,0) > 0) then
    select dt_liberacao
    into dt_liberacao_p
    from atend_consulta_pepa
    where nr_sequencia = nr_seq_consulta_pepo_p;

    select nvl(pkg_date_utils.get_DiffDate(dt_liberacao_p, sysdate, 'HOUR'),0)
    into qt_diff_hours_p
    from dual;

    if (qt_diff_hours_p >= 0 and qt_diff_hours_p <= qt_horas_apos_consulta_p) then
      ie_lib_p := 'S';
    end if;
  else
    select nvl(max('S'),'N')
    into ie_lib_p
    from atend_consulta_pepa
    where nr_sequencia = nr_seq_consulta_pepo_p
    and dt_liberacao is null
    and dt_inativacao is null;
  end if;

  return ie_lib_p;

end obter_se_reg_fim_consulta_pepa;
/
