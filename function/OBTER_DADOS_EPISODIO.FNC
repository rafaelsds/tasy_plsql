create or replace
function obter_dados_episodio(nr_seq_episodio_p		number,
			ie_opcao_p		varchar2)
 		    	return varchar2 is
/*
	E - N�mero Case
*/

ds_retorno_w	varchar2(255);

begin

if (ie_opcao_p = 'E') then
	select 	max(nr_episodio)
	into	ds_retorno_w
	from	episodio_paciente
	where 	nr_sequencia = nr_seq_episodio_p;
end if;

return	ds_retorno_w;

end obter_dados_episodio;
/