create or replace
function OBTER_DADOS_TITULO_REPASSE
		(nr_repasse_terceiro_p	number,
		ie_opcao_p	varchar2) 
		return varchar2 is

/*
T - Titulo
D - data de vencimento
I - impostos
*/

ds_lista_titulo_w	varchar2(254) := '';
nr_titulo_w		number(10,0);
dt_vencimento_w		date;
vl_imposto_w		number(15,2);

cursor c01 is
select	nr_titulo,
	dt_vencimento_original
from	titulo_pagar
where	nr_repasse_terceiro	= nr_repasse_terceiro_p
union
select	nr_titulo,
	dt_vencimento
from	repasse_terceiro_venc
where	nr_repasse_terceiro	= nr_repasse_terceiro_p
order	by nr_titulo;

begin

open c01;
loop
fetch c01 into
	nr_titulo_w,
	dt_vencimento_w;
exit when c01%notfound;

	if	(ds_lista_titulo_w is null) then
		if (ie_opcao_p = 'D') then
			ds_lista_titulo_w	:= to_char(dt_vencimento_w,'dd/mm/yyyy');
		else		
			ds_lista_titulo_w	:= nr_titulo_w;
		end if;
	else
		if	(ie_opcao_p = 'D') then
			ds_lista_titulo_w	:= ds_lista_titulo_w || ', ' || to_char(dt_vencimento_w,'dd/mm/yyyy');
		else
			ds_lista_titulo_w	:= ds_lista_titulo_w || ', ' || nr_titulo_w;
		end if;
	end if;

end loop;
close c01;

if	(ie_opcao_p = 'I') then

	select	sum(vl_imposto)
	into	vl_imposto_w
	from
	(select	a.vl_imposto
	from	repasse_terc_venc_trib a,
		repasse_terceiro_venc b
	where	b.nr_repasse_terceiro	= nr_repasse_terceiro_p
	and	a.nr_seq_rep_venc		= b.nr_sequencia
	union	all
	select	a.vl_imposto
	from	titulo_pagar_imposto a,
		titulo_pagar b
	where	b.nr_repasse_terceiro	= nr_repasse_terceiro_p
	and	b.nr_titulo		= a.nr_titulo
	and	not exists
		(select	1
		from	repasse_terc_venc_trib y,
			repasse_terceiro_venc x
		where	x.nr_repasse_terceiro	= b.nr_repasse_terceiro
		and	x.nr_sequencia		= y.nr_seq_rep_venc)
	);

	ds_lista_titulo_w	:= vl_imposto_w;

end if;

return ds_lista_titulo_w;

end;
/