create or replace
function Sus_Obter_Se_Mat_Compativel(	cd_estabelecimento_p	number,
					cd_convenio_p		number,
					nr_seq_proc_interno_p	number,
					cd_procedimento_p		number,
					ie_origem_proced_p	number,
					cd_material_p		number)
					return varchar2 is
					
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
qt_registro_w		number(10);
cd_procedimento_opme_w	number(15);
ie_origem_proc_opme_w	number(10);
ds_retorno_w		varchar2(10)	:= 'S';		

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced
	from	sus_material_opm
	where	cd_material		= cd_material_p
	and	cd_estabelecimento	= cd_estabelecimento_p
	order by nr_sequencia;

begin

select	count(cd_material)
into	qt_registro_w
from	sus_material_opm
where	cd_estabelecimento	= cd_estabelecimento_p;


if	(cd_procedimento_p is null) then
	begin
	obter_proc_tab_interno_conv(	nr_seq_proc_interno_p,
				cd_estabelecimento_p,
				cd_convenio_p,
				0,
				null,
				null,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				sysdate,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);
	end;
else
	begin
	cd_procedimento_w	:= cd_procedimento_p;
	ie_origem_proced_w	:= nvl(ie_origem_proced_p,7);
	end;
end if;

if	(qt_registro_w		> 0) and
	(ie_origem_proced_w	= 7)then

	ds_retorno_w	:= 'N';

	open C01;
	loop
	fetch C01 into
		cd_procedimento_opme_w,
		ie_origem_proc_opme_w;
	exit when C01%notfound;
		begin

		select	count(cd_proc_principal)
		into	qt_registro_w
		from	sus_proc_compativel
		where	cd_proc_principal    	= cd_procedimento_w
		and	ie_origem_proc_princ 	= ie_origem_proced_w
		and	cd_proc_secundario	= cd_procedimento_opme_w
		and	ie_origem_proc_sec		= ie_origem_proc_opme_w
		and	ie_tipo_compatibilidade	in ('1','4','5')
		and	sus_validar_regra(4, cd_procedimento_w, ie_origem_proced_w, null) = 0;

		if	(qt_registro_w	>	0) then
			ds_retorno_w	:= 'S';
		end if;

		select	count(cd_proc_secundario)
		into	qt_registro_w
		from	sus_proc_compativel
		where 	cd_proc_secundario    	= cd_procedimento_w
		and   	ie_origem_proc_sec 	= ie_origem_proced_w
		and	cd_proc_principal		= cd_procedimento_opme_w
		and	ie_origem_proc_princ	= ie_origem_proc_opme_w
		and	ie_tipo_compatibilidade	in ('1','4','5')
		and	sus_validar_regra(4, cd_procedimento_w, ie_origem_proced_w, null) > 0;

		if	(qt_registro_w	>	0) then
			ds_retorno_w	:= 'S';
		end if;

		end;
	end loop;
	close C01;
end if;


return	ds_retorno_w;

end Sus_Obter_Se_Mat_Compativel;
/
