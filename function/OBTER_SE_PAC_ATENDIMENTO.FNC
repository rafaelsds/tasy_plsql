create or replace
function obter_se_pac_atendimento(

		cd_pessoa_fisica_p	varchar2,

		cd_estabelecimento_p	number)

 		return varchar2 is

ie_atendimento_w	varchar2(5);
nr_atendimento_w	number(10);

begin

if	(cd_pessoa_fisica_p is not null) and

	(cd_estabelecimento_p is not null) then

	begin

	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	atendimento_paciente
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	cd_estabelecimento	= cd_estabelecimento_p;

	end;
	
	if	(nr_atendimento_w is not null) then
		ie_atendimento_w	:= wheb_mensagem_pck.get_texto(306949); -- Sim
	else
		ie_atendimento_w	:= wheb_mensagem_pck.get_texto(306950); -- N�o
	end if;

end if;

return	ie_atendimento_w;

end obter_se_pac_atendimento;
/