CREATE OR REPLACE
FUNCTION Obter_Desc_dcb(	
			nr_sequencia_p	Number)
			RETURN VARCHAR IS

ds_retorno_w dcb_medic_controlado.ds_dcb%type;

BEGIN

if	(nr_sequencia_p is not null) then
	select	ds_dcb
	into	ds_retorno_w
	from	dcb_medic_controlado
	where	nr_sequencia	= nr_sequencia_p;
end if;

RETURN ds_retorno_w;

END Obter_Desc_dcb;
/
