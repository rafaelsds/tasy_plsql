create or replace
function obter_qt_solucao_diluente (ie_proporcao_dil_p				varchar2,
									cd_material_dil_p				number,
									qt_diluente_p					number,
									cd_unid_med_diluente_p			varchar2,
									qt_ref_diluente_p				number,
									qt_dose_medic_p					number,
									cd_unidade_medida_dose_p		varchar2,
									cd_material_p					number,
									ie_via_aplicacao_p				varchar2,
									cd_pessoa_fisica_p				varchar2,
									nr_atendimento_p				number,
									ie_subtrair_volume_medic_p		varchar2,
									ie_ConsisteDoseConsumoMedic_p	varchar2,
									cd_diluente_red_p				number,
									qt_solucao_red_p				number,
									cd_unid_med_dil_red_p			varchar2,
									qt_volume_medic_p				number,
									cd_estabelecimento_p			number,
									cd_mat_recons_p					number,
									qt_dose_recons_p				number,
									cd_unid_med_dose_recons_p		varchar2,
									ie_alterou_dose_p				varchar2)
			return number is

qt_diluente_w					cpoe_material.qt_dose%type;
qt_dose_medic_w					cpoe_material.qt_dose%type;
qt_dose_medic_ml_w				cpoe_material.qt_dose%type;
qt_dose_ml_item_w				cpoe_material.qt_dose%type;
qt_unitaria_medic_w				cpoe_material.qt_dose%type;
qt_ml_total_item_w				cpoe_material.qt_dose%type;
qt_dose_aux_w					cpoe_material.qt_dose%type;
qt_dose_ref_w					cpoe_material.qt_dose%type;
qt_conv_ml_w					cpoe_material.qt_dose%type;
qt_conversao_ml_w				cpoe_material.qt_dose%type;
qt_dose_mat_w					cpoe_material.qt_dose%type;
qt_conversao_ml_ret_w			cpoe_material.qt_dose%type;
qt_volume_medic_convertido_w	cpoe_material.qt_dose%type;
qt_conversao_ml_dil_w			cpoe_material.qt_dose%type;
qt_dose_dil_w					cpoe_material.qt_dose%type;
qt_solucao_dil_w				cpoe_material.qt_dose%type;
cd_unidade_medida_consumo_w		material.cd_unidade_medida_consumo%type;
qt_ref_diluente_aux_w			material_diluicao.qt_referencia%type;
qt_ref_ml_dil_w					material_diluicao.qt_referencia%type;
qt_vol_adic_reconst_w			material_diluicao.qt_volume_adic%type;
cd_unid_med_diluente_w			material_diluicao.cd_unid_med_diluente%type;
qt_conversao_w					material_conversao_unidade.qt_conversao%type;
cd_unid_med_dose_mat_w			material_conversao_unidade.cd_unidade_medida%type;
cd_estabelecimento_w			estabelecimento.cd_estabelecimento%type;
nr_seq_agrupamento_w			setor_atendimento.nr_seq_agrupamento%type;
cd_setor_atendimento_w			setor_atendimento.cd_setor_atendimento%type;
cd_setor_atendimento_ww			setor_atendimento.cd_setor_atendimento%type;
qt_idade_w						number(15,2);
qt_peso_w						pessoa_fisica.qt_peso%type;
ie_param_REP_42_w				varchar2(1 char);
ie_param_REP_136_w				varchar2(1 char);

begin

	qt_diluente_w := qt_diluente_p;
	cd_unid_med_diluente_w := nvl(cd_unid_med_diluente_p, obter_unid_med_usua('ml'));

	if (ie_proporcao_dil_p = 'F') then

		qt_diluente_w := nvl(obter_conversao_ml(cd_material_dil_p, qt_diluente_w, cd_unid_med_diluente_w),0);

	elsif (ie_proporcao_dil_p = 'SC') or
		  (ie_proporcao_dil_p = 'ST') and
		  (nvl(qt_ref_diluente_p,0) > 0) then

		qt_ref_diluente_aux_w := qt_ref_diluente_p;

		qt_dose_medic_w := qt_dose_medic_p;

		qt_dose_medic_ml_w := qt_dose_medic_w;

		qt_dose_ml_item_w := nvl(cpoe_obter_conversao_ml(cd_material_p, qt_dose_medic_ml_w, cd_unidade_medida_dose_p, ie_via_aplicacao_p, cd_pessoa_fisica_p, nr_atendimento_p),0);

		if	(upper(cd_unid_med_diluente_w) <> upper(obter_unid_med_usua('ml'))) then
			qt_ref_ml_dil_w	:= nvl(obter_conversao_ml(cd_material_dil_p, qt_ref_diluente_aux_w, cd_unid_med_diluente_w),0);
		else 
			qt_ref_ml_dil_w := qt_ref_diluente_aux_w;
		end if;

		select	nvl(max(qt_conversao),1)
		into	qt_conversao_w
		from	material_conversao_unidade
		where	cd_unidade_medida = cd_unidade_medida_dose_p
		and		cd_material = cd_material_p;

		qt_unitaria_medic_w := dividir_sem_round(qt_dose_medic_w, qt_conversao_w);

		qt_ml_total_item_w := (qt_unitaria_medic_w * qt_ref_ml_dil_w);

		if ((qt_ml_total_item_w - qt_dose_ml_item_w) > 0) and (nvl(ie_subtrair_volume_medic_p,'N') = 'S') then
			qt_diluente_w := qt_ml_total_item_w - qt_dose_ml_item_w;

		elsif ((qt_ml_total_item_w - qt_dose_ml_item_w) > 0) then
			qt_diluente_w := qt_ml_total_item_w;
		else
			if (ie_proporcao_dil_p in ('SC','ST')) then
				if (ie_ConsisteDoseConsumoMedic_p = 'S') then
					qt_diluente_w := ceil(qt_dose_medic_ml_w) * qt_ref_ml_dil_w; 
				else
					qt_diluente_w := qt_dose_medic_ml_w * qt_ref_ml_dil_w; 
				end if;

				if (ie_proporcao_dil_p = 'ST') then
					qt_dose_aux_w := obter_conversao_unid_med_cons(cd_material_p, cd_unidade_medida_dose_p, qt_dose_medic_w);
				end if;
			end if;
		end if;
	end if;

	if (ie_subtrair_volume_medic_p = 'S') and (ie_proporcao_dil_p = 'F') then

		select	max(cd_unidade_medida_consumo)
		into	cd_unidade_medida_consumo_w
		from	material
		where	cd_material = cd_material_p;

		qt_peso_w := nvl(obter_ultimo_sinal_vital_pf(cd_pessoa_fisica_p,'PESO'),nvl(obter_peso_pf(cd_pessoa_fisica_p),0));

		if (nr_atendimento_p is null) then
			qt_idade_w := obter_idade_pf(cd_pessoa_fisica_p, sysdate, 'DIA');
			cd_setor_atendimento_w := null;
			cd_estabelecimento_w := cd_estabelecimento_p;
		else
			select	max(obter_setor_atendimento(c.nr_atendimento)),
					max(c.cd_estabelecimento),
					max(substr(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'DIA'),1,5))
			into	cd_setor_atendimento_ww,
					cd_estabelecimento_w,
					qt_idade_w
			from	pessoa_fisica b,
					atendimento_paciente c
			where	c.cd_pessoa_fisica = b.cd_pessoa_fisica
			and		c.nr_atendimento = nr_atendimento_p;

			obter_param_usuario(924, 42, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w, ie_param_REP_42_w);
			obter_param_usuario(924, 136, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w, ie_param_REP_136_w);
			cpoe_obter_setor_prescricao(ie_param_REP_42_w, ie_param_REP_136_w, wheb_usuario_pck.get_cd_setor_atendimento, nr_atendimento_p, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_setor_atendimento_w);

			if (cd_setor_atendimento_w is null) then
				cd_setor_atendimento_w := cd_setor_atendimento_ww;
			end if;
		end if;

		select	max(nr_seq_agrupamento)
		into	nr_seq_agrupamento_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_w;

		select	max(qt_volume_adic)
		into	qt_vol_adic_reconst_w
		from	material_diluicao
		where	qt_idade_w between obter_idade_diluicao(cd_material,nr_sequencia,'MIN') and obter_idade_diluicao(cd_material,nr_sequencia,'MAX')
		and		qt_peso_w between nvl(qt_peso_min,0) and nvl(qt_peso_max,999)
		and		((ie_via_excluir <> ie_via_aplicacao_p)	or (ie_via_excluir is null))
		and		((nr_seq_agrupamento = nr_seq_agrupamento_w) or (nr_seq_agrupamento is null))
		and		((ie_via_aplicacao = ie_via_aplicacao_p) or (ie_via_aplicacao is null))
		and		nvl(cd_perfil,nvl(obter_perfil_ativo,0)) = nvl(obter_perfil_ativo,0)
		and		((cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento is null))
		and		obter_se_material_ativo(cd_diluente) = 'A'
		and		qt_volume is not null
		and		ie_reconstituicao = 'R'
		and		ie_gerar_rediluente	= 'S'
		and		cd_material	= cd_material_p
		and		cd_diluente = cd_material_dil_p
		and		nvl(qt_diluicao,nvl(qt_diluente_p,0)) = nvl(qt_diluente_p,0)
		and		cd_unid_med_diluente = cd_unid_med_diluente_p;

		qt_dose_ref_w := obter_dose_convertida(cd_material_p, qt_dose_medic_w, cd_unidade_medida_dose_p, cd_unidade_medida_consumo_w);
		qt_conv_ml_w := obter_conversao_ml(cd_diluente_red_p, qt_solucao_red_p, cd_unid_med_dil_red_p) + qt_vol_adic_reconst_w;
		qt_conversao_ml_w := 0;

		if (ie_ConsisteDoseConsumoMedic_p = 'N') then
			
			if (nvl(qt_volume_medic_p,0) > 0) then
				qt_conversao_ml_w := qt_volume_medic_p;
			else
				qt_conversao_ml_w := obter_conversao_ml(cd_material_p,qt_dose_medic_w,cd_unidade_medida_dose_p);
			end if;

			cd_unid_med_dose_mat_w := cd_unidade_medida_dose_p;
			qt_dose_mat_w := qt_dose_medic_w;
		else 
			qt_conversao_ml_w := obter_conversao_ml(cd_material_p,qt_dose_medic_w, cd_unidade_medida_dose_p);
			cd_unid_med_dose_mat_w := substr(obter_dados_material_estab(cd_material_p, cd_estabelecimento_w, 'UMS'),1,30);
			qt_dose_mat_w := ceil(qt_unitaria_medic_w);
		end if;

		if 	(cd_mat_recons_p is not null) and
			(qt_dose_recons_p is not null) and
			(cd_unid_med_dose_recons_p is not null) then

			if (upper(cd_unid_med_dose_mat_w) <> upper(obter_unid_med_usua('ml'))) then
				qt_conv_ml_w := nvl(cpoe_obter_conversao_ml(cd_material_p, qt_dose_medic_w, cd_unidade_medida_dose_p, ie_via_aplicacao_p, cd_pessoa_fisica_p, nr_atendimento_p),0);
			else
				qt_conv_ml_w := qt_conversao_ml_w;
			end if;
		else
			qt_conv_ml_w := qt_conversao_ml_w;
		end if;

		qt_conversao_ml_ret_w := qt_conv_ml_w;

		select	nvl(obter_dose_convertida(cd_mat_recons_p, decode(nvl(ie_proporcao_dil_p,'F'), 'F', qt_diluente_w, qt_dose_ref_w * qt_ref_diluente_p),cd_unidade_medida_dose_p, cd_unidade_medida_dose_p),0)
		into	qt_volume_medic_convertido_w
		from	dual;

		select	nvl(obter_conversao_ml(cd_material_dil_p, decode(nvl(qt_volume_medic_convertido_w,0),0,qt_diluente_w,qt_volume_medic_convertido_w), upper(obter_unid_med_usua('ml'))),0)
		into	qt_conversao_ml_dil_w
		from	dual;

		if (nvl(ie_alterou_dose_p,'N') = 'S') and (qt_dose_ml_item_w > 0) then
			qt_conversao_ml_ret_w := qt_dose_ml_item_w;
		end if;
		
		if ((qt_conversao_ml_dil_w - qt_conversao_ml_ret_w) > 0) then							
			qt_dose_dil_w := qt_conversao_ml_dil_w - qt_conversao_ml_ret_w;
		end if;	
	end if;

	if	(nvl(qt_dose_dil_w,0) > 0) then
		qt_solucao_dil_w := qt_dose_dil_w;
	else 
		qt_solucao_dil_w := qt_diluente_w;
	end if;

return qt_solucao_dil_w;

end;
/
