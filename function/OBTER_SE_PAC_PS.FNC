create or replace
function obter_se_pac_PS(nr_atendimento_p	number)
 		    	return varchar2 is

ie_possui_w			varchar2(2) := 'N';
cd_setor_atendimento_w		number(5);
cd_classif_setor_w		varchar2(2);
ie_extensao_ps_w		varchar2(1 char);

begin

obter_param_usuario(950, 181, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_extensao_ps_w);

select	max(nvl(obter_unidade_atendimento(nr_atendimento,'IA','CS'),
		obter_unidade_atendimento(nr_atendimento,'A','CS')))
into	cd_setor_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

select	max(cd_classif_setor)
into	cd_classif_setor_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_w;

if	(cd_classif_setor_w	<> '1')	then

	select	decode(count(*),0,'N','S')
	into	ie_possui_w
	from	prescr_medica a,
			setor_atendimento b
	where	a.nr_atendimento = nr_atendimento_p
	and		a.dt_validade_prescr > sysdate
	and		a.cd_setor_atendimento = b.cd_setor_atendimento
	and		a.dt_suspensao is null
	and		nvl(a.dt_liberacao, a.dt_liberacao_medico) is not null
	and		b.cd_classif_setor = '1'
	and		(ie_extensao_ps_w <> 'C' or
			not exists (select	1
						from	prescr_medica  a,
								setor_atendimento b
						where	a.nr_atendimento = nr_atendimento_p
						and		a.dt_validade_prescr > sysdate
						and		a.cd_setor_atendimento = b.cd_setor_atendimento
						and		a.dt_suspensao is null
						and		nvl(a.dt_liberacao, a.dt_liberacao_medico) is not null
						and		b.cd_classif_setor <> '1'));

end if;

return	ie_possui_w;

end obter_se_pac_PS;
/
