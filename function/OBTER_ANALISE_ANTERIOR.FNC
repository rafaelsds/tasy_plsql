Create or Replace
FUNCTION OBTER_ANALISE_ANTERIOR	(nr_seq_lote_hist_p	number)
				RETURN	number IS

nr_seq_lote_hist_w	number(10);
nr_seq_lote_audit_w	number(10);

BEGIN

select	max(a.nr_seq_lote_audit)
into	nr_seq_lote_audit_w
from	lote_audit_hist a
where	a.nr_sequencia = nr_seq_lote_hist_p;

select	max(a.nr_sequencia)
into	nr_seq_lote_hist_w
from	lote_audit_hist a
where	a.nr_sequencia		<> nr_seq_lote_hist_p
and	a.nr_seq_lote_audit	= nr_seq_lote_audit_w;

RETURN nr_seq_lote_hist_w;

END OBTER_ANALISE_ANTERIOR;
/