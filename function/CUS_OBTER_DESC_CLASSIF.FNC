create or replace
function cus_obter_desc_classif(	cd_estabelecimento_p		number,
					cd_classificacao_p		number)
					return varchar2 is

ds_classificacao_w			varchar2(40);
cd_empresa_w				number(4);

begin

select	nvl(max(ds_classificacao),'')
into	ds_classificacao_w
from	classif_result
where	cd_classificacao	= cd_classificacao_p;

return	ds_classificacao_w;
end cus_obter_desc_classif;
/
