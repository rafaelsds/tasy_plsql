create or replace
function ptu_obter_inconsist_trans_v50
			(	nr_seq_guia_p		Number,
				nr_seq_requisicao_p	Number,
				nr_seq_consulta_p	Number,
				cd_transacao_p		Varchar2,
				nr_qt_reg_p		Number,
				nr_seq_proc_p		Number,
				nr_seq_mat_p		Number)
				return Number is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter inconsistências transações v50.

ROTINA UTILIZADA NAS TRANSAÇÕES PTU VIA SCS HOMOLOGADAS COM A UNIMED BRASIL.
QUANDO FOR ALTERAR, FAVOR VERIFICAR COM O ANÁLISTA RESPONSÁVEL PARA A REALIZAÇÃO DE TESTES.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionário [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_inconsistencia_w		Number(4)	:= 0;
qt_registro_w			Number(2);
nr_seq_inconsist_w		Number(10);
count_registro_w		Number(10) := 1;
nr_seq_servico_w		Number(10) := 0;

Cursor C01 is
	select	nr_seq_inconsistencia
	from	(
		select	a.nr_seq_inconsistencia
		from	ptu_intercambio_consist	a,
			ptu_inconsistencia	b
		where	a.nr_seq_inconsistencia	= b.nr_sequencia
		and	b.cd_inconsistencia	between 2001 and 2999
		and	(a.nr_seq_guia		= nr_seq_guia_p)
		and	(a.nr_seq_procedimento	is null)
		and	a.cd_transacao		= cd_transacao_p
		and	(exists (	select	1
					from	pls_guia_plano_proc	x
					where	x.ie_status	in ('N','M','K')
					and	x.nr_sequencia	= nr_seq_proc_p))
		union all
		select	a.nr_seq_inconsistencia
		from	ptu_intercambio_consist	a,
			ptu_inconsistencia	b
		where	a.nr_seq_inconsistencia	= b.nr_sequencia
		and	b.cd_inconsistencia	between 2001 and 2999
		and	(a.nr_seq_guia		= nr_seq_guia_p)
		and	(a.nr_seq_procedimento	= nr_seq_proc_p)
		and	a.cd_transacao		= cd_transacao_p
		and	(exists (	select	1
					from	pls_guia_plano_proc	x
					where	x.ie_status	in ('N','M','K')
					and	x.nr_sequencia	= nr_seq_proc_p)))
	group by nr_seq_inconsistencia;				

Cursor C02 is
	select	nr_seq_inconsistencia
	from	(
		select	a.nr_seq_inconsistencia
		from	ptu_intercambio_consist	a,
			ptu_inconsistencia	b
		where	a.nr_seq_inconsistencia	= b.nr_sequencia
		and	b.cd_inconsistencia	between 2001 and 2999
		and	(a.nr_seq_requisicao	= nr_seq_requisicao_p)
		and	(a.nr_seq_procedimento	is null)
		and	a.cd_transacao		= cd_transacao_p
		and	(exists (	select	1
					from	pls_requisicao_proc	x
					where	x.ie_status	in ('N','G')
					and	x.nr_sequencia	= nr_seq_proc_p))
		union all
		select	a.nr_seq_inconsistencia
		from	ptu_intercambio_consist	a,
			ptu_inconsistencia	b
		where	a.nr_seq_inconsistencia	= b.nr_sequencia
		and	b.cd_inconsistencia	between 2001 and 2999
		and	(a.nr_seq_requisicao	= nr_seq_requisicao_p)
		and	(a.nr_seq_procedimento	= nr_seq_proc_p)
		and	a.cd_transacao		= cd_transacao_p
		and	(exists (	select	1
					from	pls_requisicao_proc	x
					where	x.ie_status	in ('N','G')
					and	x.nr_sequencia	= nr_seq_proc_p)))
	group by nr_seq_inconsistencia;

Cursor C03 is
	select	nr_seq_inconsistencia
	from	(
		select	a.nr_seq_inconsistencia
		from	ptu_intercambio_consist	a,
			ptu_inconsistencia	b
		where	a.nr_seq_inconsistencia	= b.nr_sequencia
		and	b.cd_inconsistencia	between 2001 and 2999
		and	(a.nr_seq_guia		= nr_seq_guia_p)
		and	(a.nr_seq_material	is null)
		and	a.cd_transacao		= cd_transacao_p
		and	(exists (	select	1
					from	pls_guia_plano_mat	x
					where	x.ie_status	in ('N','M','K')
					and	x.nr_sequencia	= nr_seq_mat_p))
		union all
		select	a.nr_seq_inconsistencia
		from	ptu_intercambio_consist	a,
			ptu_inconsistencia	b
		where	a.nr_seq_inconsistencia	= b.nr_sequencia
		and	b.cd_inconsistencia	between 2001 and 2999
		and	(a.nr_seq_guia		= nr_seq_guia_p)
		and	(a.nr_seq_material	= nr_seq_mat_p)
		and	a.cd_transacao		= cd_transacao_p
		and	(exists (	select	1
					from	pls_guia_plano_mat	x
					where	x.ie_status	in ('N','M','K')
					and	x.nr_sequencia	= nr_seq_mat_p)))
	group by nr_seq_inconsistencia;		

Cursor C04 is
	select	nr_seq_inconsistencia
	from	(
		select	a.nr_seq_inconsistencia
		from	ptu_intercambio_consist	a,
			ptu_inconsistencia	b
		where	a.nr_seq_inconsistencia	= b.nr_sequencia
		and	b.cd_inconsistencia	between 2001 and 2999
		and	(a.nr_seq_requisicao	= nr_seq_requisicao_p)
		and	(a.nr_seq_material	is null)
		and	a.cd_transacao		= cd_transacao_p
		and	(exists (	select	1
					from	pls_requisicao_mat	x
					where	x.ie_status	in ('N','G')
					and	x.nr_sequencia	= nr_seq_mat_p))
		union all
		select	a.nr_seq_inconsistencia
		from	ptu_intercambio_consist	a,
			ptu_inconsistencia	b
		where	a.nr_seq_inconsistencia	= b.nr_sequencia
		and	b.cd_inconsistencia	between 2001 and 2999
		and	(a.nr_seq_requisicao	= nr_seq_requisicao_p)
		and	(a.nr_seq_material	= nr_seq_mat_p)
		and	a.cd_transacao		= cd_transacao_p
		and	(exists (	select	1
					from	pls_requisicao_mat	x
					where	x.ie_status	in ('N','G')
					and	x.nr_sequencia	= nr_seq_mat_p)))
	group by nr_seq_inconsistencia;				
	
Cursor C05 is
	select	distinct(nr_seq_inconsistencia)
	from	ptu_intercambio_consist
	where	((nr_seq_consulta_benef	= nr_seq_consulta_p)
	or	(nr_seq_consulta_prest	= nr_seq_consulta_p))
	and	cd_transacao		= cd_transacao_p;

Cursor C06 is
	select	nr_seq_inconsistencia
	from	(
		select	nr_seq_inconsistencia
		from	ptu_intercambio_consist
		where	nr_seq_guia		= nr_seq_guia_p
		and	nr_seq_requisicao	is null
		and	nr_seq_procedimento	is null
		and	nr_seq_material		is null
		and	cd_transacao		= cd_transacao_p
		union all
		select	nr_seq_inconsistencia
		from	ptu_intercambio_consist
		where	nr_seq_guia		= nr_seq_guia_p
		and	nr_seq_requisicao	is null
		and	nr_seq_procedimento	= nr_seq_consulta_p
		and	nr_seq_material		is null
		and	cd_transacao		= cd_transacao_p
		union all
		select	nr_seq_inconsistencia
		from	ptu_intercambio_consist
		where	nr_seq_guia		= nr_seq_guia_p
		and	nr_seq_requisicao	is null
		and	nr_seq_procedimento	is null
		and	nr_seq_material		= nr_seq_consulta_p 
		and	cd_transacao		= cd_transacao_p
		union all
		select	nr_seq_inconsistencia
		from	ptu_intercambio_consist
		where	nr_seq_guia		is null
		and	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_procedimento	is null
		and	nr_seq_material		is null
		and	cd_transacao		= cd_transacao_p
		union all
		select	nr_seq_inconsistencia
		from	ptu_intercambio_consist
		where	nr_seq_guia		is null
		and	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_procedimento	= nr_seq_consulta_p
		and	nr_seq_material		is null
		and	cd_transacao		= cd_transacao_p
		union all
		select	nr_seq_inconsistencia
		from	ptu_intercambio_consist
		where	nr_seq_guia		is null
		and	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_procedimento	is null
		and	nr_seq_material		= nr_seq_consulta_p
		and	cd_transacao		= cd_transacao_p)
	group by nr_seq_inconsistencia;	

begin

if	(nvl(nr_seq_consulta_p,0)	= 0) then
	if	(nr_seq_requisicao_p	is not null) then	
		for r_C02_w in C02 loop
			begin
			cd_inconsistencia_w	:= 0;

			select	count(distinct(nr_seq_inconsistencia))
			into	qt_registro_w
			from	(
				select	nr_seq_inconsistencia
				from	ptu_intercambio_consist
				where	nr_seq_requisicao	= nr_seq_requisicao_p
				and	nr_seq_procedimento 		is null
				and	cd_transacao		= cd_transacao_p
				union all
				select	nr_seq_inconsistencia
				from	ptu_intercambio_consist
				where	nr_seq_requisicao	= nr_seq_requisicao_p
				and	nr_seq_procedimento	= nr_seq_proc_p
				and	cd_transacao		= cd_transacao_p);

			select	max(cd_inconsistencia)
			into	cd_inconsistencia_w
			from	ptu_inconsistencia
			where	nr_sequencia	= r_C02_w.nr_seq_inconsistencia;

			if	(nr_qt_reg_p	<= qt_registro_w) then
				if	(count_registro_w  =  nr_qt_reg_p) then
					return	cd_inconsistencia_w;
				else
					count_registro_w := count_registro_w  + 1;
				end if;
			else
				cd_inconsistencia_w	:= 0;
			end if;
			end;
		end loop; -- C02

		for r_C04_w in C04 loop
			begin
			cd_inconsistencia_w	:= 0;
			
			select	count(distinct(nr_seq_inconsistencia))
			into	qt_registro_w
			from	(
				select	nr_seq_inconsistencia
				from	ptu_intercambio_consist
				where	nr_seq_requisicao	= nr_seq_requisicao_p
				and	nr_seq_material 	is null
				and	cd_transacao		= cd_transacao_p
				union all
				select	nr_seq_inconsistencia
				from	ptu_intercambio_consist
				where	nr_seq_requisicao	= nr_seq_requisicao_p
				and	nr_seq_material	= nr_seq_mat_p
				and	cd_transacao		= cd_transacao_p);

			select	max(cd_inconsistencia)
			into	cd_inconsistencia_w
			from	ptu_inconsistencia
			where	nr_sequencia	= r_C04_w.nr_seq_inconsistencia;

			if	(nr_qt_reg_p	<= qt_registro_w) then
				if	(count_registro_w  =  nr_qt_reg_p) then
					return	cd_inconsistencia_w;
				else
					count_registro_w 	:= count_registro_w  + 1;
				end if;
			else
				cd_inconsistencia_w	:= 0;
			end if;
			end;
		end loop; -- C04

	elsif	(nr_seq_guia_p	is not null) then
	
		for r_C01_w in C01 loop
			begin
			cd_inconsistencia_w	:= 0;
			
			select	count(distinct(nr_seq_inconsistencia))
			into	qt_registro_w
			from	(
				select	nr_seq_inconsistencia
				from	ptu_intercambio_consist
				where	nr_seq_guia		= nr_seq_guia_p
				and	nr_seq_procedimento 		is null
				and	cd_transacao		= cd_transacao_p
				union all
				select	nr_seq_inconsistencia
				from	ptu_intercambio_consist
				where	nr_seq_guia		= nr_seq_guia_p
				and	nr_seq_procedimento	= nr_seq_proc_p
				and	cd_transacao		= cd_transacao_p);

			select	max(cd_inconsistencia)
			into	cd_inconsistencia_w
			from	ptu_inconsistencia
			where	nr_sequencia	= r_C01_w.nr_seq_inconsistencia;

			if	(nr_qt_reg_p	<= qt_registro_w) then
				if	(count_registro_w  =  nr_qt_reg_p) then
					return	cd_inconsistencia_w;
				else
					count_registro_w := count_registro_w  + 1;
				end if;
			else
				cd_inconsistencia_w	:= 0;
			end if;
			end;
		end loop; -- C01
		
		for r_C03_w in C03 loop
			begin
			cd_inconsistencia_w	:= 0;
			
			select	count(distinct(nr_seq_inconsistencia))
			into	qt_registro_w
			from	(
				select	nr_seq_inconsistencia
				from	ptu_intercambio_consist
				where	nr_seq_guia		= nr_seq_guia_p
				and	nr_seq_material		is null
				and	cd_transacao		= cd_transacao_p
				union all
				select	nr_seq_inconsistencia
				from	ptu_intercambio_consist
				where	nr_seq_guia		= nr_seq_guia_p
				and	nr_seq_material		= nr_seq_mat_p
				and	cd_transacao		= cd_transacao_p);

			select	max(cd_inconsistencia)
			into	cd_inconsistencia_w
			from	ptu_inconsistencia
			where	nr_sequencia	= r_C03_w.nr_seq_inconsistencia;

			if	(nr_qt_reg_p	<= qt_registro_w) then
				if	(count_registro_w  =  nr_qt_reg_p) then
					return	cd_inconsistencia_w;
				else
					count_registro_w 	:= count_registro_w  + 1;
				end if;
			else
				cd_inconsistencia_w	:= 0;
			end if;
			end;
		end loop;

	elsif	(nr_seq_requisicao_p is null) and (nr_seq_guia_p is null) then
		if	(nr_qt_reg_p	= 1) then
			cd_inconsistencia_w	:= 2002;
		end if;
	end if;
elsif	(cd_transacao_p	= '00361') then

	for r_C06_w in C06 loop
		begin
		
		select	count(distinct(nr_seq_inconsistencia))
		into	qt_registro_w
		from	(
			select	nr_seq_inconsistencia
			from	ptu_intercambio_consist
			where	nr_seq_guia		= nr_seq_guia_p
			and	nr_seq_requisicao	is null
			and	nr_seq_procedimento	is null
			and	nr_seq_material		is null
			and	cd_transacao		= cd_transacao_p
			union all
			select	nr_seq_inconsistencia
			from	ptu_intercambio_consist
			where	nr_seq_guia		= nr_seq_guia_p
			and	nr_seq_requisicao	is null
			and	nr_seq_procedimento	= nr_seq_consulta_p
			and	nr_seq_material		is null
			and	cd_transacao		= cd_transacao_p
			union all
			select	nr_seq_inconsistencia
			from	ptu_intercambio_consist
			where	nr_seq_guia		= nr_seq_guia_p
			and	nr_seq_requisicao	is null
			and	nr_seq_procedimento	is null
			and	nr_seq_material		= nr_seq_consulta_p
			and	cd_transacao		= cd_transacao_p
			union all
			select	nr_seq_inconsistencia
			from	ptu_intercambio_consist
			where	nr_seq_guia		is null
			and	nr_seq_requisicao	= nr_seq_requisicao_p
			and	nr_seq_procedimento	is null
			and	nr_seq_material		is null
			and	cd_transacao		= cd_transacao_p
			union all
			select	nr_seq_inconsistencia
			from	ptu_intercambio_consist
			where	nr_seq_guia		is null
			and	nr_seq_requisicao	= nr_seq_requisicao_p
			and	nr_seq_procedimento	= nr_seq_consulta_p
			and	nr_seq_material		is null
			and	cd_transacao		= cd_transacao_p
			union all
			select	nr_seq_inconsistencia
			from	ptu_intercambio_consist
			where	nr_seq_guia		is null
			and	nr_seq_requisicao	= nr_seq_requisicao_p
			and	nr_seq_procedimento	is null
			and	nr_seq_material		= nr_seq_consulta_p
			and	cd_transacao		= cd_transacao_p);

		select	max(cd_inconsistencia)
		into	cd_inconsistencia_w
		from	ptu_inconsistencia
		where	nr_sequencia	= r_C06_w.nr_seq_inconsistencia;

		if	(nr_qt_reg_p	<= qt_registro_w) then
			if	(count_registro_w  =  nr_qt_reg_p) then
				return	cd_inconsistencia_w;
			else
				count_registro_w := count_registro_w  + 1;
			end if;
		else
			cd_inconsistencia_w	:= 0;
		end if;		
		end;
	end loop;
else
	
	for r_C05_w in C05 loop
		begin
		select	count(distinct(nr_seq_inconsistencia))
		into	qt_registro_w
		from	ptu_intercambio_consist
		where	((nr_seq_consulta_benef	= nr_seq_consulta_p)
		or	(nr_seq_consulta_prest	= nr_seq_consulta_p))
		and	cd_transacao		= cd_transacao_p;

		select	max(cd_inconsistencia)
		into	cd_inconsistencia_w
		from	ptu_inconsistencia
		where	nr_sequencia	= r_C05_w.nr_seq_inconsistencia;

		if	(nr_qt_reg_p	<= qt_registro_w) then
			if	(count_registro_w  =  nr_qt_reg_p) then
				return	cd_inconsistencia_w;
			else
				count_registro_w := count_registro_w  + 1;
			end if;
		else
			cd_inconsistencia_w	:= 0;
		end if;
		end;
	end loop;
end if;


return	cd_inconsistencia_w;

end ptu_obter_inconsist_trans_v50;
/