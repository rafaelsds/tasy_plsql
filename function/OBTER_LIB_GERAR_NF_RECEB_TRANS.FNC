create or replace
function obter_lib_gerar_nf_receb_trans(	nr_ordem_compra_p	number,
				nm_usuario_p		varchar2)
 		    	return varchar2 is
				
qt_existe_w	number(10);
ie_libera_w	varchar2(1);

ie_somente_barras_receb_w	varchar2(1);
ie_tipo_itens_w	number(1);
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;
begin
ie_libera_w := 'N';
ie_tipo_itens_w := to_number(obter_valor_param_usuario(146,21,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w));
ie_somente_barras_receb_w := obter_valor_param_usuario(146,28,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w);

if	((ie_somente_barras_receb_w = 'S') or (ie_tipo_itens_w = 1)) then
	begin
	select	count(*)
	into	qt_existe_w
	from	ordem_compra_item_cb
	where	nr_ordem_compra 	= nr_ordem_compra_p
	and	ie_atende_recebe	= 'R'
	and	nr_seq_nota is null
	and	ie_status = 'CB';
	
	if	(qt_existe_w > 0) then
		ie_libera_w := 'S';
	end if;
	end;
else
	ie_libera_w := 'S';
end if;

return	ie_libera_w;
end obter_lib_gerar_nf_receb_trans;
/
