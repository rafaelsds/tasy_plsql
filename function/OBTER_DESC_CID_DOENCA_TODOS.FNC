create or replace 
Function Obter_Desc_CID_Doenca_Todos(	cd_cid_doenca_p		Varchar2) 
				return varchar2 is

ds_retorno_w	Varchar2(240);

begin

if	(cd_cid_doenca_p is not null) then
	select	ds_doenca_cid
	into	ds_retorno_w
	from	cid_doenca
	where	cd_doenca_cid = cd_cid_doenca_p;
end if;

return ds_retorno_w;

end Obter_Desc_CID_Doenca_Todos;
/