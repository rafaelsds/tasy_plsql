create or replace
function obter_se_existe_sc_na_oc(
			nr_solic_compra_p		number,
			nr_ordem_compra_p		number)
return varchar2 is

ie_existe_w			varchar2(1) := 'N';
qt_existe_w			number(5);

begin

select	count(*)
into	qt_existe_w
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_solic_compra = nr_solic_compra_p;

if	(qt_existe_w > 0) then
	ie_existe_w := 'S';
end if;
	
return	ie_existe_w;

end obter_se_existe_sc_na_oc;
/
