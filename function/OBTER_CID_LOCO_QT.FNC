create or replace
function obter_cid_loco_qt(cd_pessoa_fisica_p		number)
 		    	return varchar2 is

ds_doenca_cid_w 		varchar2(255);
cd_doenca_cid_w			varchar2(10);
		
begin

if		(cd_pessoa_fisica_p is not null) then
		select 	max(a.cd_doenca_cid)
		into	cd_doenca_cid_w
		from	can_loco_regional a
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	a.nr_sequencia = (select 	max(b.nr_sequencia)
					  from		can_loco_regional b
					  where		a.cd_pessoa_fisica = b.cd_pessoa_fisica
					  and		b.dt_liberacao is not null);


		if	(cd_doenca_cid_w is not null) then
			ds_doenca_cid_w	:= cd_doenca_cid_w||' - '||substr(obter_desc_cid(cd_doenca_cid_w),1,255);
		end if;	
end if;
			
return	ds_doenca_cid_w;

end obter_cid_loco_qt;
/