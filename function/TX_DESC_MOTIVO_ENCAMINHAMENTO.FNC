create or replace
function tx_desc_motivo_encaminhamento(nr_seq_motivo_p		number)
 		    	return varchar2 is
ds_motivo_w	varchar2(80);
begin
if	(nr_seq_motivo_p is not null) then
	select	max(ds_motivo)
	into	ds_motivo_w
	from	tx_motivo_encaminhamento
	where	nr_sequencia = nr_seq_motivo_p;
end if;

return	ds_motivo_w;

end tx_desc_motivo_encaminhamento;
/