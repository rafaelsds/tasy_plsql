CREATE OR REPLACE
FUNCTION Obter_exame_bloqueado(	nr_sequencia_p		NUMBER,
				nr_prescricao_p 	NUMBER )
				RETURN VARCHAR2 IS
/**
	Retorna se o exame est� bloqueado ou n�o
*/

ie_restringe_result_w	Varchar2(1) := 'N';

BEGIN

if	(nr_prescricao_p is not null) then

	SELECT 	nvl(g.ie_restringe_resultado,'N') ie_restringe_resultado
	into	ie_restringe_result_w
	from 	prescr_procedimento b, 
        	prescr_medica c,
       		exame_lab_resultado e, 
        	exame_lab_result_item g,
		exame_laboratorio f                               
	where 	b.nr_prescricao         = c.nr_prescricao
       	and   	b.nr_prescricao         = e.nr_prescricao
	and   	e.nr_seq_resultado   = g.nr_seq_resultado
	and   	g.nr_seq_exame       = f.nr_seq_exame
	and   	b.nr_seq_exame       = f.nr_seq_exame
	and     g.nr_seq_prescr        = b.nr_sequencia
       	and   	b.nr_prescricao         = nr_prescricao_p
       	and   	b.nr_sequencia         = nr_sequencia_p;

end if;

RETURN ie_restringe_result_w;

END Obter_exame_bloqueado;
/