create or replace
function obter_informacao_ditado (nr_seq_prescr_proc_p   number, opcao_p varchar2)
          return varchar2 is

ds_retorno  varchar2(255);

begin

if(opcao_p = 'NM') then
  select max(nm_usuario) INTO ds_retorno from PRESCR_PROC_DITADO where nr_seq_prescr_proc = nr_seq_prescr_proc_p;
else if (opcao_p = 'DT') then
  select to_char(max(dt_atualizacao),'dd/mm/yyyy hh24:mi:ss') INTO ds_retorno from PRESCR_PROC_DITADO where nr_seq_prescr_proc = nr_seq_prescr_proc_p;
    end if;
end if;

return ds_retorno;

end obter_informacao_ditado;
/