create or replace
function obter_desc_grupo_atend_pac (nr_seq_grupo_p		number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);			

begin

select	max(ds_grupo)
into	ds_retorno_w
from	pac_grupo
where	nr_sequencia = nr_seq_grupo_p;


return	ds_retorno_w;

end obter_desc_grupo_atend_pac;
/