create or replace function definir_ie_classif_urgente(dt_horario_p            date,
                                                     cd_local_estoque_p      number,
                                                     cd_estabelecimento_p    number,
                                                     cd_setor_atendimento_p  number,
                                                     dt_inicio_prescr_p      date,
                                                     ie_classif_nao_padrao_p varchar2,
                                                     ie_padronizado_p        varchar2,
                                                     dt_limite_especial_p    date,
                                                     dt_limite_agora_p       date,
                                                     ie_agora_impressao_p    varchar2)
return varchar2 is

nr_seq_turno_hor_ag_w  regra_turno_disp.nr_sequencia%type;
dt_limite_agora_w      date;
hr_turno_agora_w       varchar2(15);
hr_final_turno_agora_w varchar2(15);
qt_min_antes_atend_w   regra_tempo_disp.qt_min_antes_atend%type;
ie_define_agora_w      regra_tempo_disp.ie_define_agora%type;
ie_classif_urgente_w   varchar2(1);

begin

  dt_limite_agora_w := dt_limite_agora_p;

  if	(ie_agora_impressao_p = 'S') then

    nr_seq_turno_hor_ag_w  := Obter_turno_horario_prescr(cd_estabelecimento_p,cd_setor_atendimento_p,to_char(dt_horario_p,'hh24:mi'),cd_local_estoque_p);

    select max(to_char(b.hr_inicial,'hh24:mi')),
           max(to_char(b.hr_final,'hh24:mi'))
      into hr_turno_agora_w,
           hr_final_turno_agora_w
      from regra_turno_disp_param b,
           regra_turno_disp a
     where a.nr_sequencia        = b.nr_seq_turno
       and a.cd_estabelecimento  = cd_estabelecimento_p
       and a.nr_sequencia        = nr_seq_turno_hor_ag_w
       and (nvl(b.cd_setor_atendimento,nvl(cd_setor_atendimento_p,0))  = nvl(cd_setor_atendimento_p,0));

    select nvl(max(qt_min_antes_atend), 0),
           nvl(max(ie_define_agora), 'N')
      into qt_min_antes_atend_w,
           ie_define_agora_w
      from regra_tempo_disp
     where cd_estabelecimento  = cd_estabelecimento_p
       and (nvl(cd_setor_atendimento,nvl(cd_setor_atendimento_p,0)) = nvl(cd_setor_atendimento_p,0))
       and nr_seq_turno        = nr_seq_turno_hor_ag_w
       and ie_situacao         = 'A';

    if  (hr_turno_agora_w > hr_final_turno_agora_w) then
      if  (ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_horario_p) > ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_inicio_prescr_p)) then
        dt_limite_agora_w := pkg_date_utils.get_time(dt_horario_p - 1, hr_turno_agora_w, 0) - (qt_min_antes_atend_w/1440);
      else
        dt_limite_agora_w := pkg_date_utils.get_time(dt_horario_p, hr_turno_agora_w, 0) - (qt_min_antes_atend_w/1440);
      end if;
    else
      dt_limite_agora_w := pkg_date_utils.get_time(dt_horario_p, hr_turno_agora_w, 0) - (qt_min_antes_atend_w/1440);
    end if;
  end if;

  if	(ie_classif_nao_padrao_p is null) or
			(ie_padronizado_p = 'S') then
			begin
			ie_classif_urgente_w	:= 'N';
			if	(dt_horario_p <= dt_limite_agora_w) then
				ie_classif_urgente_w	:= 'A';
			elsif	((dt_limite_agora_w <= sysdate) and (ie_agora_impressao_p = 'S') and (ie_define_agora_w = 'N')) then
				ie_classif_urgente_w	:= 'A';
			elsif	(ie_agora_impressao_p = 'S' and ie_define_agora_w = 'S') and
				(dt_horario_p <= (sysdate + (qt_min_antes_atend_w /1440))) then
				ie_classif_urgente_w	:= 'A';
			elsif	(dt_horario_p <= dt_limite_especial_p) then
				ie_classif_urgente_w	:= 'E';
			end if;
			end;
		else
			return ie_classif_nao_padrao_p;
		end if;

  return ie_classif_urgente_w;

end definir_ie_classif_urgente;
/