Create or Replace
Function Obter_cnes_cadastro_estab(
			cd_estabelecimento_p	Number)
			Return Number IS

cd_cnes_w	estabelecimento.cd_cns%type;

BEGIN

select	max(cd_cns)
into	cd_cnes_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

RETURN cd_cnes_w;

END Obter_cnes_cadastro_estab;
/