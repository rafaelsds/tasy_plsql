create or replace
function Obter_total_item_audit_externa(nr_sequencia_p	number,
					ie_tipo_item_p	varchar2)
 		    	return number is

vl_retorno_w	number(15,2);
			
begin

if	(ie_tipo_item_p = 'P') then

	select 	sum(nvl(c.vl_procedimento,0))
	into	vl_retorno_w
	from 	procedimento_paciente c,
		auditoria_externa_item b,
		auditoria_externa a
	where 	a.nr_sequencia = b.nr_seq_audit_ext
	and 	b.nr_seq_item = c.nr_sequencia
	and 	a.nr_sequencia = nr_sequencia_p;
		

else

	select 	sum(nvl(c.vl_material,0))
	into	vl_retorno_w
	from 	material_atend_paciente c,
		auditoria_externa_item b,
		auditoria_externa a
	where 	a.nr_sequencia = b.nr_seq_audit_ext
	and 	b.nr_seq_item = c.nr_sequencia
	and 	a.nr_sequencia = nr_sequencia_p;

end if;

return	vl_retorno_w;

end Obter_total_item_audit_externa;
/