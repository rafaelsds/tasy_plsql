create or replace
function obter_se_pj_matriz_filial(	cd_cnpj_orig_p		varchar2,
					cd_cnpj_alt_p		varchar2)
 		    	return varchar2 is

qt_registros_w		number(10);
ie_matriz_filial_w	varchar2(1) := 'N';
			
begin

select	count(*)
into	qt_registros_w
from	pessoa_juridica
where	cd_cgc = cd_cnpj_orig_p
and	substr(cd_cgc,1,8) = substr(cd_cnpj_alt_p,1,8);

if	(qt_registros_w > 0) then
	ie_matriz_filial_w := 'S';
end if;

return	ie_matriz_filial_w;

end obter_se_pj_matriz_filial;
/