create or replace
function Obter_Cod_Mun_ibge_atend(nr_atendimento_p	Number)
				return varchar2 is

cd_municipio_ibge_w		varchar2(20);
cd_pessoa_fisica_w		varchar2(20);
				
begin

select 	nvl(max(cd_pessoa_fisica),0)
into	cd_pessoa_fisica_w
from 	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

select 	nvl(obter_compl_pf(cd_pessoa_fisica_w,1,'CDM'), obter_desc_expressao(327119)/*'N�o Informado'*/)
into	cd_municipio_ibge_w
from 	dual;

return	cd_municipio_ibge_w;

end Obter_Cod_Mun_ibge_atend;
/
