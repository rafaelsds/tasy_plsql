create or replace
function pls_obter_boni_vinc_sca
		(	nr_seq_segurado_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(4000);
ds_bonificacao_w	varchar2(4000);
nr_seq_plano_w		number(10);
nr_seq_bonificacao_w	number(10);

Cursor C01 is
	select	nr_seq_plano
	into	nr_seq_plano_w
	from	pls_sca_vinculo
	where	nr_seq_segurado	= nr_seq_segurado_p;
	
Cursor C02 is
	select	nr_seq_bonificacao
	into	nr_seq_bonificacao_w
	from	pls_bonificacao_vinculo
	where	nr_seq_plano	= nr_seq_plano_w;

begin

open C01;
loop
fetch C01 into	
	nr_seq_plano_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_bonificacao_w;
	exit when C02%notfound;
		begin
		
		select	nm_bonificacao
		into	ds_bonificacao_w
		from	pls_bonificacao
		where	nr_sequencia	= nr_seq_bonificacao_w;
		
		ds_retorno_w		:= ds_bonificacao_w ||', '||ds_retorno_w;
		ds_bonificacao_w	:= '';
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;


return	substr(ds_retorno_w,1,length(ds_retorno_w)-2);

end pls_obter_boni_vinc_sca;
/
