create or replace
function obter_dados_pessoa_autor( cd_pessoa_fisica_p	varchar2,
					nr_sequencia_autor_p number,
					ie_opcao_p varchar2)
 		    	return varchar2 is

dt_obito_w			date;
nr_atendimento_w		number(10,0);
nr_seq_gestao_w			number(10,0);
nr_seq_autor_cirurgia_w		number(10,0);
nr_seq_agenda_w			number(10,0);
nr_seq_agenda_consulta_w	number(10,0);
nr_seq_age_integ_w		number(10,0);
nr_seq_paciente_setor_w		number(10,0);
cd_pessoa_fisica_w		varchar2(10);
ds_retorno_w			varchar2(255);

/*
DO - Data do �bito

*/

begin

if 	(cd_pessoa_fisica_p is not null) then

	select 	to_char(dt_obito,'dd/mm/yyyy hh24:mi:ss')
	into	ds_retorno_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
elsif	(nr_sequencia_autor_p is not null) then
	select	nr_atendimento,
		nr_seq_gestao,
		nr_seq_autor_cirurgia,
		nr_seq_agenda,
		nr_seq_agenda_consulta,
		nr_seq_age_integ,
		nr_seq_paciente_setor
	into	nr_atendimento_w,
		nr_seq_gestao_w,
		nr_seq_autor_cirurgia_w,
		nr_seq_agenda_w,
		nr_seq_agenda_consulta_w,
		nr_seq_age_integ_w,
		nr_seq_paciente_setor_w
	from	autorizacao_convenio
	where	nr_sequencia = nr_sequencia_autor_p;
	
	if	(nr_atendimento_w is not null) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_w;
	elsif	(nr_seq_agenda_w is not null) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	agenda_paciente
		where	nr_sequencia = nr_seq_agenda_w;
	elsif	(nr_seq_agenda_consulta_w is not null) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	agenda_consulta
		where	nr_sequencia = nr_seq_agenda_consulta_w;
	elsif	(nr_seq_age_integ_w is not null) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	agenda_integrada
		where	nr_sequencia = nr_seq_age_integ_w;
	elsif	(nr_seq_gestao_w is not null) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	gestao_vaga
		where	nr_sequencia = nr_seq_gestao_w;
	elsif	(nr_seq_paciente_setor_w is not null) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	paciente_setor
		where	nr_seq_paciente = nr_seq_paciente_setor_w;
	end if;
	
	if	(cd_pessoa_fisica_w is not null) and
		(ie_opcao_p = 'DO')then
		begin
		select 	to_char(dt_obito,'dd/mm/yyyy hh24:mi:ss')
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		end;
	end if;
end if;

return	ds_retorno_w;

end obter_dados_pessoa_autor;
/
