create or replace function pc_obter_term_cir(nr_atendimento_p atendimento_paciente.nr_atendimento%type)
			return varchar2 deterministic is

ie_term_cir_w varchar2(1) := 'N';
count_w	pls_integer := 0;

begin

if (nr_atendimento_p is not null) then
	select count(a.ie_tipo_consentimento)
	into count_w
	from  pep_pac_ci a,
		  agenda_paciente b
	where a.nr_seq_agenda = b.nr_sequencia
	and b.cd_pessoa_fisica = ( select cd_pessoa_fisica
							   from atendimento_paciente
							   where nr_atendimento = nr_atendimento_p)
	and a.ie_tipo_consentimento = 'C';
	
	if (count_w = 0) then
		select count(*)
		into count_w
		from  pep_pac_ci a,
			  cirurgia b
		where a.nr_atendimento = b.nr_atendimento
		and b.nr_atendimento = nr_atendimento_p
		and a.dt_liberacao is not null
		and a.ie_tipo_consentimento = 'C'
		and a.dt_liberacao < b.dt_termino
		and a.dt_liberacao > (select max(dt_inicio_prevista)
							  from cirurgia
							  where nr_atendimento = nr_atendimento_p
							  and nr_cirurgia != b.nr_cirurgia);
		
		if (count_w > 0) then
			ie_term_cir_w := 'S';
		end if;
	else 
		ie_term_cir_w := 'S';
	end if;
end if;

return ie_term_cir_w;

end pc_obter_term_cir;
/
