create or replace
FUNCTION obter_motivo_canc_agecir	(	cd_motivo_p	VARCHAR2,
						ie_opcao_p	VARCHAR2)
					RETURN VARCHAR2 IS
					
ds_motivo_w	VARCHAR2(255);
ds_retorno_w	VARCHAR2(255);					

BEGIN
IF	(cd_motivo_p IS NOT NULL) THEN
	SELECT	MAX(ds_motivo)
	INTO	ds_motivo_w
	FROM	agenda_motivo_cancelamento
	WHERE	cd_motivo	=	cd_motivo_p
	AND	ie_agenda IN ('CI','T');
END IF;

IF	(ie_opcao_p = 'D') THEN
	ds_retorno_w	:= ds_motivo_w;
END IF;

RETURN	ds_retorno_w;

end obter_motivo_canc_agecir;
/