create or replace
function obter_de_para_tipo_obj_dic_obj (ie_tipo_objeto_p	varchar2)
	return varchar2 is
	
ie_tipo_objeto_w	varchar2(15);
	
begin
if	(ie_tipo_objeto_p is not null) then
	begin
	if	(ie_tipo_objeto_p = 'P') then
		begin
		ie_tipo_objeto_w := 'WJP';
		end;
	elsif	(ie_tipo_objeto_p = 'M') then
		begin
		ie_tipo_objeto_w := 'WJPUM';
		end;
	elsif	(ie_tipo_objeto_p = 'MI') then
		begin
		ie_tipo_objeto_w := 'WJMI';
		end;		
	elsif	(ie_tipo_objeto_p in ('C','AC')) then
		begin
		ie_tipo_objeto_w := 'WCP';
		end;
	elsif	(ie_tipo_objeto_p = 'WF') then
		begin
		ie_tipo_objeto_w := 'WF';
		end;		
	elsif	(ie_tipo_objeto_p = 'CLCB') then
		begin
		ie_tipo_objeto_w := 'WDBLCB';
		end;
	elsif	(ie_tipo_objeto_p in ('CEWSCB','CWSCB','ACCWSCB','ACBLWSCB','APWSCB')) then
		begin
		ie_tipo_objeto_w := 'WSCB';
		end;
	elsif	(ie_tipo_objeto_p = 'CD') then
		begin
		ie_tipo_objeto_w := 'WDP';
		end;
	elsif	(ie_tipo_objeto_p in ('ASQL','A','AA','APA')) then
		begin
		ie_tipo_objeto_w := 'WJT';
		end;
	elsif	(ie_tipo_objeto_p = 'WT') then
		begin
		ie_tipo_objeto_w := 'WT';
		end;
	elsif	(ie_tipo_objeto_p in ('F','APF')) then
		begin
		ie_tipo_objeto_w := 'CORSISFV';
		end;
	elsif	(ie_tipo_objeto_p = 'T') then
		begin
		ie_tipo_objeto_w := 'TXT';
		end;
	elsif	(ie_tipo_objeto_p in ('PSQL','SQL')) then
		begin
		ie_tipo_objeto_w := 'SQL';
		end;				
	elsif	(ie_tipo_objeto_p = 'WCS') then
		begin
		ie_tipo_objeto_w := 'WCS';
		end;						
	end if;
	end;
end if;
return ie_tipo_objeto_w;
end obter_de_para_tipo_obj_dic_obj;
/