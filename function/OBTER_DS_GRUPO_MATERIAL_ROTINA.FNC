create or replace
function Obter_ds_grupo_material_rotina	(nr_sequencia_p		number)
 		    	return varchar2 is

DS_GRUPO_w	varchar2(255);
			
begin

select	max(DS_GRUPO)
into	DS_GRUPO_w
from	GRUPO_MATERIAL_ROTINA
where	nr_sequencia	= nr_sequencia_p;

return	DS_GRUPO_w;

end Obter_ds_grupo_material_rotina;
/