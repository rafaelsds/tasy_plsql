create or replace function get_nm_department_wpb( nr_atendimento_p	    in atendimento_paciente.nr_atendimento%type,
                                ie_department_short_p in char) return varchar2 is
			
nr_atendimento_w    atendimento_paciente.nr_atendimento%type;
ds_retorno_w        varchar2(100);

begin

ds_retorno_w := '';

if (nr_atendimento_p is not null) then   

    if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_DE') then

        select  max(apt.nr_atendimento)
        into    nr_atendimento_w
        from    atendimento_paciente apt
        where   apt.nr_seq_episodio in (    select  max(ap.nr_seq_episodio)
                                            from    atendimento_paciente ap
                                            where   ap.nr_atendimento = nr_atendimento_p)
        and     apt.dt_cancelamento is null
        and     apt.dt_alta is null
        and     obter_se_atendimento_futuro(apt.nr_atendimento) = 'N';

        if (nr_atendimento_w is not null) then
            if (ie_department_short_p = 'N') then
                select  obter_nome_departamento_medico(obter_departamento_data(nr_atendimento_w))
                into    ds_retorno_w
                from    dual;
            elsif (ie_department_short_p = 'S') then
                select  obter_nome_curto_depto_medico(obter_departamento_data(nr_atendimento_w))
                into    ds_retorno_w
                from    dual;
			elsif (ie_department_short_p = 'C') then
				select	obter_classif_esp_pac(nr_atendimento_w)
				into	ds_retorno_w
				from 	dual;
            else
                select  substr(obter_unidade_atendimento(nr_atendimento_w, 'IAR','SU'),1,255)
                into    ds_retorno_w
                from    dual;
            end if;
        end if;
    elsif (ie_department_short_p = 'N') then
        select  obter_nome_departamento_medico(obter_departamento_data(nr_atendimento_p))
        into    ds_retorno_w
        from    dual;
    elsif (ie_department_short_p = 'S') then
        select  obter_nome_curto_depto_medico(obter_departamento_data(nr_atendimento_p))
        into    ds_retorno_w
        from    dual;
	elsif (ie_department_short_p = 'C') then
		select	obter_classif_esp_pac(nr_atendimento_p)
		into	ds_retorno_w
		from 	dual;
    else
        select  substr(obter_unidade_atendimento(nr_atendimento_p, 'IAR','SU'),1,255)
        into    ds_retorno_w
        from    dual;
	end if;

end if;
return ds_retorno_w;
end get_nm_department_wpb;
/
