create or replace
function obter_interacao_medicamentosa(nr_prescricao_p	Number)
 		    	return varchar2 is

ds_motivo_w		varchar2(2000);

begin

if	(nr_prescricao_p > 0) then
	select	substr(max(a.ds_interacao),1,2000)
	into	ds_motivo_w
	from 	medic_interacao_alimento a, 
			prescr_dieta b, 
			prescr_material c 
	where c.nr_prescricao = nr_prescricao_p
	and b.nr_prescricao = c.nr_prescricao
	and a.cd_material = c.cd_material
	and a.cd_dieta = b.cd_dieta;
end if;

return	ds_motivo_w;

end obter_interacao_medicamentosa;
/