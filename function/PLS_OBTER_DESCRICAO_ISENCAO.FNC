create or replace
function pls_obter_descricao_isencao
			(	nr_seq_motivo_isencao_p		number)
				return varchar2 is

ds_retorno_w			varchar2(255);

begin

select	ds_motivo_isencao
into	ds_retorno_w
from	pls_motivo_isencao_notific
where	nr_sequencia	= nr_seq_motivo_isencao_p;

return	ds_retorno_w;

end pls_obter_descricao_isencao;
/