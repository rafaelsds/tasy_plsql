create or replace
function fagf_obter_vl_fat_espec(dt_referencia_p	varchar2,
				ie_status_protocolo_p	number,
				ie_complexidade_p	varchar2,
				ie_identificacao_aih_p	varchar2,
				cd_especialidade_p	number)
 		    	return number is

vl_retorno_w	number(10,4) := 0;
begin

select	sum(nvl(y.vl_sadt,0)) + 
	sum(nvl(w.vl_medico,0)) + 
	sum(nvl(y.vl_matmed,0)) + 
	sum(nvl(Obter_Valor_Participante(w.nr_sequencia),0))
into	vl_retorno_w
from	sus_aih_unif s,
	conta_paciente c,
	procedimento_paciente w,
	protocolo_convenio l,
	sus_valor_proc_paciente y
where	w.nr_interno_conta = c.nr_interno_conta
and	w.nr_sequencia = y.nr_sequencia
and	w.cd_motivo_exc_conta is null
and	c.nr_interno_conta = s.nr_interno_conta(+)
and	l.ie_status_protocolo = ie_status_protocolo_p
and	s.ie_complexidade = ie_complexidade_p
and	s.ie_identificacao_aih = ie_identificacao_aih_p
and	l.nr_seq_protocolo = c.nr_seq_protocolo
and	s.cd_especialidade_aih = cd_especialidade_p
and	to_char(l.dt_mesano_referencia, 'yyyymm') = dt_referencia_p;

return	nvl(vl_retorno_w,0);

end fagf_obter_vl_fat_espec;
/ 
