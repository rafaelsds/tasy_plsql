create or replace
function obter_cnae_nfse(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w 	varchar2(50);	

begin

select	max(pls_obter_cd_cnae(decode(obter_cnae_emitente(a.cd_estabelecimento, a.cd_cgc_emitente, obter_item_servico_proced(b.cd_procedimento, b.ie_origem_proced), b.cd_procedimento),
		0, 
			c.nr_seq_cnae,
				obter_cnae_emitente(a.cd_estabelecimento, a.cd_cgc_emitente, obter_item_servico_proced(b.cd_procedimento, b.ie_origem_proced), b.cd_procedimento))))
into	ds_retorno_w
from 	operacao_nota c,
	nota_fiscal_item b,
	nota_fiscal a
where	a.nr_sequencia = nr_sequencia_p
and	a.nr_sequencia = b.nr_sequencia
and 	a.cd_operacao_nf = c.cd_operacao_nf
and	decode(obter_cnae_emitente(a.cd_estabelecimento, a.cd_cgc_emitente, obter_item_servico_proced(b.cd_procedimento, b.ie_origem_proced), b.cd_procedimento),
		0, 
			c.nr_seq_cnae,
				obter_cnae_emitente(a.cd_estabelecimento, a.cd_cgc_emitente, obter_item_servico_proced(b.cd_procedimento, b.ie_origem_proced), b.cd_procedimento)) <> 0;
				
return	ds_retorno_w;

end obter_cnae_nfse;
/
