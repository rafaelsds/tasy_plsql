create or replace
function obter_se_interv_prescr_agora (
		cd_intervalo_p	varchar2)
		return varchar2 is

ie_agora_w	varchar2(1) := 'N';

begin
if	(cd_intervalo_p is not null) then
	begin
	select	nvl(max('S'),'N')
	into	ie_agora_w
	from    intervalo_prescricao
	where   ie_agora     = 'S'
	and     cd_intervalo = cd_intervalo_p;
	end;
end if;
return	ie_agora_w;
end obter_se_interv_prescr_agora;
/