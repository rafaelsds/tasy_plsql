create or replace
function obter_data_reg_comparecimento(	cd_tipo_controle_p		number,
					cd_pessoa_fisica_p		varchar2,
					ie_opcao_p		varchar2)
 		    	return date is

dt_entrada_saida_w			date;
nr_sequencia_w				number(10);
			
begin

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	controle_pessoa_comp
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	cd_tipo_controle = cd_tipo_controle_p;

if	(nr_sequencia_w > 0) then

	if	(ie_opcao_p = 'E') then
		select	dt_entrada
		into	dt_entrada_saida_w
		from	controle_pessoa_comp
		where	nr_sequencia = nr_sequencia_w;	
	elsif	(ie_opcao_p = 'S') then
		select	dt_saida
		into	dt_entrada_saida_w
		from	controle_pessoa_comp
		where	nr_sequencia = nr_sequencia_w;	
	end if;	
end if;

return	dt_entrada_saida_w;

end obter_data_reg_comparecimento;
/