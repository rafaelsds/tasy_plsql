create or replace
function cus_obter_se_apresenta_valor(nr_seq_indicador	number)
 		    	return varchar is
						
ie_retorno_w		varchar2(1);

begin

select 	nvl(ie_valor,'S')
into	ie_retorno_w
from 	resultado_indicador
where	nr_sequencia = nr_seq_indicador;

return	ie_retorno_w;

end cus_obter_se_apresenta_valor;
/