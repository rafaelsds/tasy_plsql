create or replace
function obter_dados_ultima_compra_est(	cd_estabelecimento_p		number,
					cd_material_p			number,
					ie_retorno_p			varchar2,
					qt_dias_pesquisa_p			number)
return varchar2 is

/*dados:
UC	= unidade medida compra
QT	= Quantidade Item NF
VTI	= valor total item nf
VU	= valor unitario item nf
VE	= valor unit�rio do item, convertido para quantidade de estoque
VI	= valor do item (considera descontos, frete, etc)
SN	= Sequ�ncia NF
DT	= Data Entrada Sa�da
C	= Comprador
OC	= Ordem Compra
PJ	= CGC fornecedor
NFS	= Numero sequencia da Nota 	Matheus 06/11/208 OS 115473
*/

nr_sequencia_w			number(10,0);
cd_unidade_medida_compra_w	varchar2(30);
vl_total_item_nf_w			number(13,2);
vl_unitario_item_nf_w		number(13,4);
vl_item_nf_w			number(13,4);
qt_item_nf_w			number(13,4);
nr_nota_fiscal_w			varchar2(255);
nr_seq_ultima_compra_w		number(10);
nr_ordem_compra_w		number(10);
cd_cgc_emitente_w		varchar2(14);
dt_entrada_saida_w		date;
ie_ultima_compra_w		varchar2(1);
vl_retorno_w			varchar2(100);
qt_item_estoque_w			number(13,4);
ie_inf_ultima_compra_w		varchar2(1);
vl_liquido_w			number(15,2);
cd_material_estoque_w		number(6);

begin
nr_sequencia_w := 0;

select	cd_material_estoque
into	cd_material_estoque_w
from	material
where	cd_material = cd_material_p;

select	nvl(ie_inf_ultima_compra,'S')
into	ie_inf_ultima_compra_w
from	material
where	cd_material = cd_material_p;

if	(ie_inf_ultima_compra_w = 'S') then
	begin
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_ultima_compra_w
	from	sup_dados_ultima_compra
	where	cd_material = cd_material_p
	and	cd_estabelecimento = cd_estabelecimento_p;

	if	(nr_seq_ultima_compra_w > 0) then 
		/*Identifica se a nota � mesmo de opera��o que atualiza ultima compra, e se n�o for n�o entra no pr�ximo if abaixo*/
		select	nvl(max(nr_seq_nota),0)
		into	nr_sequencia_w
		from	sup_dados_ultima_compra
		where	nr_sequencia = nr_seq_ultima_compra_w;
		
		if	(nr_sequencia_w > 0) then
			select	nvl(max(b.ie_ultima_compra),'S')
			into	ie_ultima_compra_w
			from	nota_fiscal a,
				operacao_nota b
			where	a.cd_operacao_nf = b.cd_operacao_nf
			and	a.nr_sequencia = nr_Sequencia_w;
		
			if	(ie_ultima_compra_w = 'N') then
				nr_sequencia_w := 0;
			end if;
		end if;
	end if;

	if	(nr_sequencia_w = 0) then
		begin
		select	nvl(max(x.nr_sequencia),0)
		into	nr_sequencia_w
		from	natureza_operacao n,
			operacao_nota o,
			nota_fiscal x,
			nota_fiscal_item y
		where	x.nr_sequencia = y.nr_sequencia
		and	n.cd_natureza_operacao = x.cd_natureza_operacao
		and	x.cd_operacao_nf = o.cd_operacao_nf
		and	n.ie_entrada_saida = 'E'
		and	x.ie_acao_nf = 1
		and	nvl(o.ie_ultima_compra, 'S') = 'S'
		and	y.cd_material_estoque	= cd_material_estoque_w
		and	y.cd_estabelecimento = cd_estabelecimento_p
		and	x.dt_emissao >= sysdate - qt_dias_pesquisa_p;
		end;
	end if;

	if	(nr_sequencia_w > 0) then
		begin		
		
		select	nvl(max(cd_cgc_emitente),''),
			nvl(max(dt_entrada_saida),''),
			nvl(max(somente_numero(nr_nota_fiscal)),0)
		into	cd_cgc_emitente_w,
			dt_entrada_saida_w,
			nr_nota_fiscal_w
		from	nota_fiscal
		where	nr_sequencia = nr_sequencia_w;
		
		select 	max(cd_unidade_medida_compra),
			nvl(max(vl_total_item_nf),0),
			nvl(max(vl_unitario_item_nf),0),
			nvl(max(nr_ordem_compra),0),
			nvl(max(qt_item_nf),0),
			nvl(max(dividir((vl_total_item_nf - vl_desconto -
			vl_desconto_rateio + vl_frete + vl_despesa_acessoria + vl_seguro),
			qt_item_nf)),0),
			nvl(max(qt_item_estoque),0),
			nvl(max(vl_liquido),0)
		into	cd_unidade_medida_compra_w,
			vl_total_item_nf_w,
			vl_unitario_item_nf_w,
			nr_ordem_compra_w,
			qt_item_nf_w,
			vl_item_nf_w,
			qt_item_estoque_w,
			vl_liquido_w
		from	nota_fiscal_item
		where	nr_sequencia = nr_sequencia_w
		and	cd_material_estoque = cd_material_estoque_w;
		end;
	end if;

	if	(ie_retorno_p = 'UC') then
		vl_retorno_w	:= cd_unidade_medida_compra_w;
	elsif	(ie_retorno_p = 'VTI') then
		vl_retorno_w	:= vl_total_item_nf_w;
	elsif	(ie_retorno_p = 'VU') then
		vl_retorno_w 	:= vl_unitario_item_nf_w;
	elsif	(ie_retorno_p = 'NF') then
		vl_retorno_w 	:= substr(nr_nota_fiscal_w,1,100);
	elsif	(ie_retorno_p = 'NFS') then
		vl_retorno_w 	:= nr_sequencia_w;
	elsif	(ie_retorno_p = 'PJ') then
		vl_retorno_w	:= cd_cgc_emitente_w;
	elsif	(ie_retorno_p = 'DT') then
		vl_retorno_w	:= to_char(dt_entrada_saida_w,'dd/mm/yyyy');
	elsif	(ie_retorno_p = 'OC') then
		vl_retorno_w	:= to_char(nr_ordem_compra_w);
	elsif	(ie_retorno_p = 'C') and
		(nr_ordem_compra_w > 0) then
		begin
		select	substr(sup_obter_nome_comprador(cd_estabelecimento_p,cd_comprador),1,100)
		into	vl_retorno_w
		from	ordem_compra
		where	nr_ordem_compra = nr_ordem_compra_w;
		end;
	elsif	(ie_retorno_p = 'QT') then
		vl_retorno_w	:= to_char(qt_item_nf_w);
	elsif	(ie_retorno_p = 'VI') then
		vl_retorno_w	:= to_char(vl_item_nf_w);
	elsif	(ie_retorno_p = 'VE') then
		vl_retorno_w 	:= to_char(dividir(vl_unitario_item_nf_w, qt_item_estoque_w));
	elsif	(ie_retorno_p = 'CM') then
		vl_retorno_w 	:= to_char(dividir(vl_liquido_w, qt_item_estoque_w));		
	end if;
	end;
end if;

return	vl_retorno_w;
end obter_dados_ultima_compra_est;
/
