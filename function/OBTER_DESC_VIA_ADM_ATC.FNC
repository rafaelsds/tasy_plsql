create or replace
function Obter_Desc_Via_Adm_ATC ( 	cd_via_administracao_p		varchar2) 
					return				varchar2 is

ds_retorno_w		varchar2(80);
begin

select	substr(ds_via_adm,1,80)
into	ds_retorno_w
from	atc_via_adm
where	cd_via_adm	= cd_via_administracao_p;

return ds_retorno_w;

end Obter_Desc_Via_Adm_ATC;
/