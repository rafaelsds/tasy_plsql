create or replace
function Obter_se_prescr_retrograda(nr_prescricao_p	number)
 		    	return varchar2 is
	
ie_retrograda_w	varchar2(1);

begin

select	nvl(max(ie_prescr_emergencia),'N')
into	ie_retrograda_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

return	ie_retrograda_w;

end Obter_se_prescr_retrograda;
/