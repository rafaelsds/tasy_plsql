create or replace
function obter_se_prescr_atend_parcial(	nr_prescricao_p		number)
 		    	return Varchar2 is


ds_retorno_w			varchar2(01) := 'N';
qt_existe_w			number(10,0);
nr_seq_item_w			number(10,0);
cd_material_w			number(06,0);
qt_material_w			number(15,3);
qt_material_atend_w		number(15,3);
dt_baixa_prescr_w			date;
ie_parcial_w			varchar2(01) := 'N';

Cursor C01 is
select	nr_sequencia,
	cd_material,
	qt_material
from	prescr_material
where	nr_prescricao	= nr_prescricao_p;

begin

select	dt_baixa
into	dt_baixa_prescr_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

if	(dt_baixa_prescr_w is null) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_item_w,
		cd_material_w,
		qt_material_w;
	exit when C01%notfound;
		begin

		select	count(*)
		into	qt_existe_w
		from	material_atend_paciente
		where	nr_prescricao = nr_prescricao_p;
		
		select	sum(qt_material)
		into	qt_material_atend_w
		from	material_atend_paciente
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia_prescricao = nr_seq_item_w;

		if	(qt_existe_w > 0) and
			(nvl(qt_material_w,0) > nvl(qt_material_atend_w,0)) then
			ie_parcial_w	:= 'S';
		end if;
		
		if	(ds_retorno_w = 'N') then	/*se tiver um item com parcial n�o entra mais neste if*/
			ds_retorno_w := ie_parcial_w;
		end if;
		
		end;
	end loop;
	close C01;
end if;

return	ds_retorno_w;

end obter_se_prescr_atend_parcial;
/
