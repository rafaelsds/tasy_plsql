create or replace function Obter_se_sol_continua2(	nr_prescricao_p			number,
									nr_seq_solucao_p		number,
									ds_solucao_p			varchar2,
									dt_inicial_horarios_p	date,
									dt_final_horarios_p		date,
									dt_inicio_solucoes_p	date,
									dt_final_solucoes_p		date,
									nr_prescricoes_p		varchar)
				return varchar2 is
			
ie_retorno_w			varchar2(1) := 'N';
ds_solucao_w			varchar2(255);
nr_seq_solucao_w		number(6); 
nr_seq_solucao_ww		number(6):= null;
ie_status_w				prescr_solucao.ie_status%type;
ie_status_ww  			prescr_solucao.ie_status%type;
cd_estabelecimento_w	number(10);
cd_material_ww			number(6);
cd_material_w			number(6);
qt_comp_atual_w			number(5);
qt_comp_sol_w			number(5);
ie_verifica_comp_w		varchar2(1);
nr_atendimento_w		number(10);
nr_prescricao_w			number(14);
nr_prescricao_ww		number(14):= null;
ie_existe_comp_w		varchar2(1);
ie_continua_w			varchar2(1) := 'N';
ie_parametro_w			varchar2(15);
nr_etapas_atual_w		number(5,0);
nr_etapas_w				number(5,0);
ie_via_aplic_atual_w	varchar2(10);
ie_via_aplicacao_w		varchar2(10);
qt_dose_atual_w			number(15,3);
qt_dose_w				number(15,3);
dt_prescricao_w			date;
dt_validade_prescr_w	date;
ie_acm_w				varchar2(1);
ie_se_necessario_w		varchar2(1);
ie_urgencia_w			varchar2(1);
ie_etapa_especial_w		varchar2(1);
ie_status_sol_atual_w	varchar2(3);
cd_intervalo_w			prescr_material.cd_intervalo%type;

Cursor C01 is --Buscar todas solucoes do atendimento com o mesmo titulo
	select	a.nr_seq_solucao,
			a.nr_prescricao
	from	prescr_solucao a,
			prescr_medica b
	where	a.nr_prescricao = b.nr_prescricao
	and		((b.nr_prescricao > nr_prescricao_p) or 
			 ((a.nr_prescricao = nr_prescricao_p) and
			  (a.nr_seq_solucao <> nr_seq_solucao_p)))
	and		((a.ie_status in ('I','INT')) or
			 ((Obter_se_acm_sn_agora_especial(a.ie_acm, a.ie_se_necessario, a.ie_urgencia, a.ie_etapa_especial) = 'N') and 
			 exists( select 1 from prescr_mat_hor k where nvl(k.ie_horario_especial,'N') = 'N' and k.nr_prescricao = a.nr_prescricao and k.nr_seq_solucao = a.nr_seq_solucao and k.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
			((Obter_se_acm_sn_agora_especial(a.ie_acm, a.ie_se_necessario, a.ie_urgencia, a.ie_etapa_especial) = 'S') and 
			 (obter_se_prescr_vig_adep(b.dt_prescricao,b.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and		nvl(a.ie_acm,'N') = ie_acm_w
	and		nvl(a.ie_se_necessario,'N') = ie_se_necessario_w
	and		nvl(a.ie_urgencia,'N') = ie_urgencia_w
	and		nvl(a.ie_etapa_especial,'N') = ie_etapa_especial_w
	and		nvl(a.ie_suspenso,'N') = 'N'
	and		nvl(a.ds_solucao,'XPTO') = nvl(ds_solucao_p,'XPTO')
	and		b.dt_validade_prescr between dt_inicio_solucoes_p and dt_final_solucoes_p
	and		b.nr_atendimento = nr_atendimento_w
	order by b.dt_validade_prescr desc;
	
Cursor C02 is --Componentes da solucao atual
	select	a.cd_material		
	from	prescr_material a,
		prescr_solucao b
	where	b.nr_seq_solucao = a.nr_sequencia_solucao
	and	b.nr_prescricao = a.nr_prescricao
	and	b.nr_seq_solucao = nr_seq_solucao_p
	and	b.nr_prescricao = nr_prescricao_p;
	
Cursor C03 is --Componentes das demais solucoes
	select	a.nr_seq_solucao,
			a.nr_prescricao,
			a.ie_status
	from	prescr_solucao a,
			prescr_material b,
			prescr_medica c
	where	c.nr_prescricao = a.nr_prescricao
	and		c.nr_prescricao = b.nr_prescricao
	and		b.nr_sequencia_solucao = a.nr_seq_solucao
	and		b.cd_material in (	select	d.cd_material
						from	prescr_material d
						where	d.nr_sequencia_solucao = nr_seq_solucao_p
						and	d.nr_prescricao	= nr_prescricao_p)
	and		c.dt_validade_prescr > trunc(sysdate - 30)	
	--and		(c.nr_prescricao < nr_prescricao_p)
  and		((b.nr_prescricao > nr_prescricao_p) or 
        ((a.nr_prescricao = nr_prescricao_p) and ((ie_status_w <> a.ie_status and a.nr_seq_solucao <> nr_seq_solucao_p) or (a.nr_seq_solucao < nr_seq_solucao_p))))
			and		((a.ie_status in ('I','INT')) or
			 ((Obter_se_acm_sn_agora_especial(a.ie_acm, a.ie_se_necessario, a.ie_urgencia, a.ie_etapa_especial) = 'N') and 
			 exists( select 1 from prescr_mat_hor k where nvl(k.ie_horario_especial,'N') = 'N' and k.nr_prescricao = a.nr_prescricao and k.nr_seq_solucao = a.nr_seq_solucao and k.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
			((Obter_se_acm_sn_agora_especial(a.ie_acm, a.ie_se_necessario, a.ie_urgencia, a.ie_etapa_especial) = 'S') and 
			 (obter_se_prescr_vig_adep(c.dt_prescricao,c.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and		c.nr_atendimento = nr_atendimento_w
	and		nvl(a.ie_acm,'N') = ie_acm_w
	and		nvl(a.ie_se_necessario,'N') = ie_se_necessario_w
	and		nvl(a.cd_intervalo, 'XPTO') = cd_intervalo_w
	order by a.nr_prescricao desc;
	
Cursor C04 is
	select	a.nr_seq_solucao,
			a.nr_prescricao,
			b.qt_dose,
			a.ie_via_aplicacao,
			nvl(a.nr_etapas,0)
	from	prescr_solucao a,
			prescr_material b,
			prescr_medica c
	where	c.nr_prescricao = a.nr_prescricao
	and		c.nr_prescricao = b.nr_prescricao
	and		b.nr_sequencia_solucao = a.nr_seq_solucao
	and		nvl(a.ie_acm,'N') = ie_acm_w
	and		nvl(a.ie_se_necessario,'N') = ie_se_necessario_w
	and		nvl(a.cd_intervalo, 'XPTO') = cd_intervalo_w
	and		b.cd_material in (	select	d.cd_material
								from	prescr_material d
								where	d.nr_sequencia_solucao = nr_seq_solucao_p
								and		d.nr_prescricao	= nr_prescricao_p)	
	and		a.nr_prescricao = b.nr_prescricao
	and		((b.nr_prescricao > nr_prescricao_p) or 
			 ((a.nr_prescricao = nr_prescricao_p) and
			  (a.nr_seq_solucao <> nr_seq_solucao_p)))
	and		((a.ie_status in ('I','INT')) or
			 ((Obter_se_acm_sn_agora_especial(a.ie_acm, a.ie_se_necessario, a.ie_urgencia, a.ie_etapa_especial) = 'N') and 
			 exists( select 1 from prescr_mat_hor k where nvl(k.ie_horario_especial,'N') = 'N' and k.nr_prescricao = a.nr_prescricao and k.nr_seq_solucao = a.nr_seq_solucao and k.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
			((Obter_se_acm_sn_agora_especial(a.ie_acm, a.ie_se_necessario, a.ie_urgencia, a.ie_etapa_especial) = 'S') and 
			 (obter_se_prescr_vig_adep(c.dt_prescricao,c.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and		c.nr_atendimento = nr_atendimento_w
	and		dt_final_horarios_p >= buscar_ref_dt_inicio_solucao(a.nr_prescricao, a.nr_seq_solucao)
	and		((nr_prescricoes_p is null) or (obter_se_valor_contido(c.nr_prescricao, nr_prescricoes_p)= 'S'))	
	order by c.dt_validade_prescr desc;
	

begin

select 	max(nr_atendimento),
		max(cd_estabelecimento),
		max(dt_prescricao),
		max(dt_validade_prescr)
into	nr_atendimento_w,
		cd_estabelecimento_w,
		dt_prescricao_w,
		dt_validade_prescr_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select	nvl(max(ie_solucao_continua),'X') --Parametro
into	ie_parametro_w
from	parametro_medico
where	cd_estabelecimento = cd_estabelecimento_w; 

select	count(*) --Quantidade de componentes da solucao atual
into	qt_comp_atual_w
from	prescr_material
where	nr_sequencia_solucao = nr_seq_solucao_p
and		nr_prescricao = nr_prescricao_p
and     dt_suspensao is null;

select	max(ie_via_aplicacao),
		nvl(max(nr_etapas),0),
		nvl(max(ie_acm),'N'),
		nvl(max(ie_se_necessario),'N'),
		nvl(max(ie_urgencia),'N'),
		nvl(max(ie_etapa_especial),'N'),
		nvl(max(cd_intervalo), 'XPTO'),
		nvl(max(ie_status),'XPTO')
into	ie_via_aplic_atual_w,
		nr_etapas_atual_w,
		ie_acm_w,
		ie_se_necessario_w,
		ie_urgencia_w,
		ie_etapa_especial_w,
		cd_intervalo_w,
	    ie_status_w
from	prescr_solucao
where	nr_prescricao = nr_prescricao_p
and		nr_seq_solucao = nr_seq_solucao_p;

if	(ie_parametro_w = 'A') then --Titulo e componentes

	open C01;
	loop
	fetch C01 into	
		nr_seq_solucao_w,
		nr_prescricao_w;
	exit when C01%notfound;
		begin
		
		select	count(*)
		into	qt_comp_sol_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_w
		and	nr_sequencia_solucao = nr_seq_solucao_w
		and     dt_suspensao is null;
		
		if	(qt_comp_sol_w = qt_comp_atual_w) then --Verificar se a solucao tem a mesma quantidade de compoentes da solucao atual
			ie_verifica_comp_w := 'S';
		else
			ie_verifica_comp_w := 'N';
		end if;
		
		if	(ie_verifica_comp_w = 'S') then --Verificar se a solucao tem os mesmo componentes da Sol. atual
		
			open C02;
			loop
			fetch C02 into	
				cd_material_w;
			exit when C02%notfound;
				begin
				
				select	nvl(max('S'),'N')
				into	ie_existe_comp_w
				from	prescr_material
				where	cd_material = cd_material_w
				and		nr_prescricao = nr_prescricao_w
				and		nr_sequencia_solucao = nr_seq_solucao_w;
				
				if	(ie_existe_comp_w = 'N') then
					Exit;
				end if;		
				
				end;
			end loop;
			close C02;
			
			if	(ie_existe_comp_w = 'S') then
				nr_prescricao_ww 	:= nr_prescricao_w;
				nr_seq_solucao_ww	:= nr_seq_solucao_w;
				ie_continua_w := 'S';
				Exit;
			end if;
		
		end if;
			
		end;
	end loop;
	close C01;
  
elsif	(ie_parametro_w = 'C') then --Componentes

	open C03;
	loop
	fetch C03 into	
		nr_seq_solucao_w,
		nr_prescricao_w,
		ie_status_ww;
	exit when C03%notfound;
		begin

		select	count(*)
		into	qt_comp_sol_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_w
		and		nr_sequencia_solucao = nr_seq_solucao_w
		and     dt_suspensao is null;	
    
		if	(qt_comp_sol_w = qt_comp_atual_w) and
        	(ie_status_ww = ie_status_w) then 		
			nr_prescricao_ww 	:= nr_prescricao_w;
			nr_seq_solucao_ww	:= nr_seq_solucao_w;			
			ie_continua_w := 'S';
			Exit;
		else
			ie_continua_w := 'N';
		end if;
		
					
		end;
	end loop;
	close C03;	

elsif	(ie_parametro_w = 'T') then --Titulo

	open C01;
	loop
	fetch C01 into	
		nr_seq_solucao_w,
		nr_prescricao_w;
	exit when C01%notfound;
		begin
		nr_prescricao_ww 	:= nr_prescricao_w;
		nr_seq_solucao_ww	:= nr_seq_solucao_w;
		ie_continua_w := 'S';
		Exit;	
		
		end;
	end loop;
	close C01;
	
elsif	(ie_parametro_w = 'CDVF') then --HSL
 	open C04;
	loop
	fetch C04 into	
		nr_seq_solucao_w,
		nr_prescricao_w,
		qt_dose_w,
		ie_via_aplicacao_w,
		nr_etapas_w;
	exit when C04%notfound;
		begin
		
		select	count(*)
		into	qt_comp_sol_w
		from	prescr_material a
		where	a.nr_prescricao = nr_prescricao_w
		and  	a.nr_sequencia_solucao = nr_seq_solucao_w
		and     a.dt_suspensao is null;
		
		if 	(qt_comp_sol_w = qt_comp_atual_w) then
			select	count(*)
			into	qt_comp_sol_w
			from	prescr_material a
			where	a.nr_prescricao = nr_prescricao_w
			and	a.nr_sequencia_solucao = nr_seq_solucao_w		
			and	a.qt_dose  in (	select 	x.qt_dose
						from	prescr_material x
						where 	x.nr_prescricao = nr_prescricao_p
						and	x.nr_sequencia_solucao = nr_seq_solucao_p
						and	x.cd_material = a.cd_material)
			and     a.dt_suspensao is null;
		end if;
				
		if	(qt_comp_sol_w = qt_comp_atual_w) and		
			(ie_via_aplicacao_w = ie_via_aplic_atual_w) and
			(nr_etapas_w = nr_etapas_atual_w) and
			((nr_prescricao_w <> nr_prescricao_p) or
			 ((nr_seq_solucao_w > nr_seq_solucao_p) and
			  (nr_prescricao_p = nr_prescricao_w)))then
			nr_prescricao_ww 	:= nr_prescricao_w;
			nr_seq_solucao_ww	:= nr_seq_solucao_w;
			ie_continua_w := 'S';
			Exit;
		else
			ie_continua_w := 'N';
		end if;		
		
		end;
	end loop;
	close C04;
	
end if;

if	(ie_continua_w = 'S') then
		
	select	nvl(ie_status,'N')
	into	ie_status_w
	from	prescr_solucao
	where	nr_prescricao = nr_prescricao_ww
	and		nr_seq_solucao = nr_seq_solucao_ww;
	
	if	(ie_status_w <> 'T') then
	
		/*select 	nvl(max('N'),'S')
		into	ie_retorno_w
		from 	prescr_medica
		where	nr_prescricao = nr_prescricao_ww
		and	dt_validade_prescr < sysdate;*/
		
		
		
		ie_retorno_w	:= 'S';
	else
		ie_retorno_w := 'N';	
	end if;		
else
	ie_retorno_w	:=  'N';
end if;

return	ie_retorno_w; 

end Obter_se_sol_continua2;
/