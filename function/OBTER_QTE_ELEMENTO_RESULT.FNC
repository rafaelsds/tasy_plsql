create or replace
function obter_qte_elemento_result(nr_sequencia_p number) 
	return number is

qt_reg_w	number(3);

begin

select	count(*)
into	qt_reg_w
from	ehr_elemento_result
where	nr_seq_elemento = nr_sequencia_p;

return	qt_reg_w;

end	obter_qte_elemento_result;
/