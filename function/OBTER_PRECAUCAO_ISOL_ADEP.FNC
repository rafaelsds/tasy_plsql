create or replace
function obter_precaucao_isol_adep(nr_atendimento_p	number,
				ie_opcao_p			varchar2)
 		    	return varchar2 is

ds_precaucao_w			varchar2(100);
nr_seq_precaucao_atend_w	number;
nr_seq_precaucao_w		number;

begin

select	nvl(max(nr_Sequencia),0)
into	nr_seq_precaucao_atend_w
from	atendimento_precaucao
where	nr_atendimento = nr_atendimento_p;

if (nr_seq_precaucao_atend_w > 0) then
	select  nr_seq_precaucao
	into	nr_seq_precaucao_w
	from	atendimento_precaucao
	where	nr_sequencia = nr_seq_precaucao_atend_w
	and	dt_inativacao is null
	and	nvl(dt_final_precaucao,sysdate +1) > sysdate
	and	dt_termino is null;

	select  ds_precaucao
	into	ds_precaucao_w
	from	cih_precaucao
	where	nr_sequencia = nr_seq_precaucao_w;
end if;

if (upper(ie_opcao_p) = 'C') then
	return	nr_seq_precaucao_w;
else
	return ds_precaucao_w;
end if;

end obter_precaucao_isol_adep;
/
