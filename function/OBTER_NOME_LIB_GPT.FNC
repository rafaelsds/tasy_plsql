create or replace
function obter_nome_lib_gpt	(nm_usuario_lib_p 	varchar2 )
				return varchar2 is

/*function criada para obter nome dos usu�rios que ficaram registrados no campo com o CD_PESSOA_FISICA,
pois era assim que o sistema estava gravando at� a altera��o para gravar de fato o NM_USUARIO no campo correspondente*/

ds_retorno_w		varchar2(60);

begin
if (nm_usuario_lib_p is not null) then
	begin
	
	ds_retorno_w	:= substr(obter_pf_usuario(nm_usuario_lib_p,'N'),1,60);
	
	if	(ds_retorno_w	is null) and
		(OBTER_SE_SOMENTE_NUMERO(nm_usuario_lib_p) = 'S') then
		ds_retorno_w	:= substr(obter_nome_pf(nm_usuario_lib_p),1,60);
	end if;
	
	end;
end if;

return ds_retorno_w;

end obter_nome_lib_gpt;
/