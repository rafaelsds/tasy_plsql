create or replace
function obter_rotina_digito_usuario(	cd_convenio_p	number,
					cd_categoria_p	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

Cursor C01 is
	select	ds_rotina_digito
	from	conv_regra_rotina_digito
	where	cd_categoria = cd_categoria_p
	and	cd_convenio = cd_convenio_p
	and 	nvl(ie_situacao,'A') = 'A'
	order by cd_categoria;
			
begin


open C01;
loop
fetch C01 into	
	ds_retorno_w;
exit when C01%notfound;
	begin
	ds_retorno_w:=ds_retorno_w;
	end;
end loop;
close C01;


return	ds_retorno_w;

end obter_rotina_digito_usuario;
/