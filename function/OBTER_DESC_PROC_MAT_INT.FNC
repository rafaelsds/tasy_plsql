CREATE OR REPLACE
FUNCTION Obter_Desc_Proc_mat_int(
			cd_material_p		Number,
			cd_procedimento_p	number,
			ie_origem_proced_p	number,
			nr_seq_propaci_p	number)	
			RETURN VarChar2 IS

ds_Retorno_w		VarChar2(240);
nr_seq_proc_interno_w	number(10);

BEGIN

if	(nvl(cd_material_p,0) > 0) then		--'N�o Encontrado'
	select 	substr(nvl(max(ds_material), wheb_mensagem_pck.get_texto(308664)),1,240)
	into	ds_retorno_w
	from 	material
	where 	cd_material = cd_material_p;
else
	begin
	select	max(nr_seq_proc_interno)
	into	nr_seq_proc_interno_w
	from 	procedimento_paciente
	where 	nr_sequencia = nr_seq_propaci_p;
	exception 
	when	others then
		nr_seq_proc_interno_w:= null;
	end;
	
	if	(nr_seq_proc_interno_w is null) then ----'N�o Encontrado'	
		select 	nvl(max(ds_procedimento),wheb_mensagem_pck.get_texto(308664))
		into 	ds_retorno_w
		from 	procedimento
		where 	cd_procedimento = cd_procedimento_p
		and 	ie_origem_proced = nvl(ie_origem_proced_p,ie_origem_proced);
	else
		select	obter_desc_proc_interno(nr_seq_proc_interno_w)
		into	ds_retorno_w
		from 	dual;
	end if;
end if;

RETURN ds_retorno_w;
END Obter_Desc_Proc_mat_int;
/