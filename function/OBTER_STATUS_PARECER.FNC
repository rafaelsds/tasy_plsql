create or replace
function obter_status_parecer(	nr_parecer_p	number)
 		    	return varchar2 is

			
ie_retorno_w	varchar2(10);
qt_reg_w	number(10);

			
begin

select	count(1)
into	qt_reg_w
from	parecer_medico  a
where	a.nr_parecer	= nr_parecer_p
and	a.dt_inativacao is null
and	a.dt_liberacao is not null;

if	(qt_reg_w	= 0) then
	ie_retorno_w	:= 'P';
else
	select	count(1)
	into	qt_reg_w
	from	parecer_medico  a
	where	a.nr_parecer	= nr_parecer_p
	and	a.dt_inativacao is null
	and	(a.IE_STATUS  = 'A')
	and	a.dt_liberacao is not null;
	
	
	if	(qt_reg_w	> 0) then
		ie_retorno_w	:= 'A';
	else
	
		select	count(1)
		into	qt_reg_w
		from	parecer_medico  a
		where	a.nr_parecer	= nr_parecer_p
		and	a.dt_inativacao is null
		and	(a.IE_STATUS  = 'F')
		and	a.dt_liberacao is not null;
		
		if	(qt_reg_w	> 0) then
			ie_retorno_w	:= 'F';
		end if;
	
	end if;
	
end if;


return	ie_retorno_w;

end obter_status_parecer;
/