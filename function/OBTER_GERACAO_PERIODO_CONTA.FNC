create or replace
FUNCTION Obter_geracao_periodo_conta(dt_inicial_p	DATE,
				     nr_dias_p		NUMBER,
				     ie_fim_semana_p	VARCHAR2,
				     nr_seq_atepacu_p	NUMBER,
				     nr_atendimento_p	NUMBER)
 		    	RETURN DATE IS

qt_registro_w 		NUMBER(10,0);
nr_dias_final_semana_w	NUMBER;
dt_retorno_w		DATE;
dt_entrada_unidade_w   	DATE;
dt_entrada_w		DATE;
dt_procedimento_ww	DATE;


BEGIN

qt_registro_w := 0;
nr_dias_final_semana_w:= 0;
dt_procedimento_ww := dt_inicial_p;

SELECT	dt_entrada_unidade
INTO	dt_entrada_unidade_w
FROM	atend_paciente_unidade
WHERE	nr_seq_interno = nr_seq_atepacu_p;

SELECT	b.dt_entrada
INTO	dt_entrada_w
FROM 	estabelecimento a,
	atendimento_paciente b
WHERE	a.cd_estabelecimento 	= b.cd_estabelecimento
AND	b.nr_atendimento 	= nr_atendimento_p;


WHILE	(qt_registro_w	< nr_dias_p) LOOP
BEGIN
IF	(dt_entrada_w > dt_procedimento_ww) THEN
	dt_procedimento_ww := dt_entrada_unidade_w;
END IF;

--IF	(ie_fim_semana_p = 'S') OR (TO_CHAR(dt_procedimento_ww,'d') NOT IN ('1','7')) THEN	
IF	(ie_fim_semana_p = 'S') OR (PKG_DATE_UTILS.IS_BUSINESS_DAY(dt_procedimento_ww,0) = 1) THEN -- Dias de semana
	qt_registro_w := qt_registro_w + 1;
--ELSIF	(ie_fim_semana_p = 'N') AND (TO_CHAR(dt_procedimento_ww,'d') IN ('1','7')) THEN
ELSIF	(ie_fim_semana_p = 'N') AND (PKG_DATE_UTILS.IS_BUSINESS_DAY(dt_procedimento_ww,0) = 0) THEN -- Finais de semana
	nr_dias_final_semana_w:= nr_dias_final_semana_w + 1;
END IF;

dt_procedimento_ww := dt_procedimento_ww + 1;
END;

END LOOP;

dt_retorno_w := dt_inicial_p + (nr_dias_p - 1) - nr_dias_final_semana_w;

RETURN	dt_retorno_w;

END obter_geracao_periodo_conta;
/
