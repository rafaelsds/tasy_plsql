create or replace
function consiste_exigencia_senha(nr_interno_conta_p	number)
 		     	 return varchar is

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc	
	from	proc_exige_senha_eup
	where	ie_situacao = 'A';
	
ie_consiste_w		varchar2(1) :='N';
qt_procedimento_w	number(10) := 0;
qt_sem_senha_w		number(10) := 0;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type:=0;

begin

begin	
select	nr_atendimento
into	nr_atendimento_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;	
exception
when others then
	ie_consiste_w:='N';
	nr_atendimento_w:=0;
end;
	
if	(nr_atendimento_w > 0) then

	select	count(1)
	into	qt_sem_senha_w
	from	convenio b,
		atendimento_paciente c,
		atend_categoria_convenio a
	where	a.cd_convenio		= b.cd_convenio
	and	a.nr_atendimento	= c.nr_atendimento
	and	a.nr_atendimento	= nr_atendimento_w
	and	b.ie_tipo_convenio	in (2,5,6,7,8,9)
	and	a.cd_senha is null;

	if (qt_sem_senha_w > 0) then
	
		for	r1 in  c01 loop
			
			if (r1.cd_procedimento > 0) then
			
				select	count(1)
				into	qt_procedimento_w
				from	procedimento_paciente p
				where	p.nr_interno_conta = nr_interno_conta_p
				and	p.cd_procedimento =  r1.cd_procedimento
				and	p.ie_origem_proced =  r1.ie_origem_proced;
			
			elsif (r1.cd_area_procedimento > 0) then
			
				select	count(1)
				into	qt_procedimento_w
				from	procedimento_paciente p,
					procedimento r,
					grupo_proc g,
					especialidade_proc e,
					area_procedimento a	
				where	p.nr_interno_conta = nr_interno_conta_p
				and	p.cd_procedimento = r.cd_procedimento
				and	p.ie_origem_proced = r.ie_origem_proced
				and	r.cd_grupo_proc = g.cd_grupo_proc
				and	g.cd_especialidade = e.cd_especialidade
				and	e.cd_area_procedimento = a.cd_area_procedimento				
				and	a.cd_area_procedimento =  r1.cd_area_procedimento;
				
			elsif (r1.cd_especialidade > 0) then
			
				select	count(1)
				into	qt_procedimento_w
				from	procedimento_paciente p,
					procedimento r,
					grupo_proc g,
					especialidade_proc e	
				where	p.nr_interno_conta = nr_interno_conta_p
				and	p.cd_procedimento = r.cd_procedimento
				and	p.ie_origem_proced = r.ie_origem_proced
				and	r.cd_grupo_proc = g.cd_grupo_proc
				and	g.cd_especialidade = e.cd_especialidade			
				and	e.cd_especialidade =  r1.cd_especialidade;
				
			elsif (r1.cd_grupo_proc > 0) then
			
				select	count(1)
				into	qt_procedimento_w
				from	procedimento_paciente p,
					procedimento r,
					grupo_proc g	
				where	p.nr_interno_conta = nr_interno_conta_p
				and	p.cd_procedimento = r.cd_procedimento
				and	p.ie_origem_proced = r.ie_origem_proced
				and	r.cd_grupo_proc = g.cd_grupo_proc		
				and	g.cd_grupo_proc =  r1.cd_grupo_proc;
				
			end if;
			
			if (qt_procedimento_w > 0) then
				ie_consiste_w:='S';
				exit;
			end if;

		end loop;
		
	end if;
end if;

return	ie_consiste_w;

end consiste_exigencia_senha;
/