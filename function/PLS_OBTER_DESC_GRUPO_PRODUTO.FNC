create or replace
function pls_obter_desc_grupo_produto
			(	nr_seq_grupo_prod_p	number)
				return varchar2 is
				
ds_retorno_w			varchar2(255);

begin

if	(nr_seq_grupo_prod_p is not null) then
	select	substr(ds_grupo,1,255)
	into	ds_retorno_w
	from	pls_preco_grupo_produto
	where	nr_sequencia	= nr_seq_grupo_prod_p;
end if;

return	ds_retorno_w;

end pls_obter_desc_grupo_produto;
/