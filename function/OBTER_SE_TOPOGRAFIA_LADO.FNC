create or replace
function obter_se_topografia_lado(	nr_seq_topografia_p	number )
					return varchar2 is


ie_lado_w	varchar2(1);

begin

select	max(ie_lado)
into	ie_lado_w
from	topografia_dor
where	nr_sequencia =	nr_seq_topografia_p;

return	ie_lado_w;

end obter_se_topografia_lado;
/