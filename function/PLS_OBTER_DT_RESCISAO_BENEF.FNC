create or replace
function pls_obter_dt_rescisao_benef
		(	nr_seq_segurado_p	number,
			nr_seq_motivo_cancel_p	number,
			dt_rescisao_p		date,
			cd_estabelecimento_p	varchar2)
			return date is
			
			
nr_seq_contrato_w		number(10);
qt_dia_rescisao_w		number(10);
nr_seq_regra_rescisao_w		number(10);
dt_rescisao_nova_w		date;
dt_dia_rescisao_w		number(10);
ie_tipo_data_w			varchar2(10);
qt_meses_sub_w			number(10);
ie_estipulante_w		varchar2(10);
			
Cursor C01 is
	select	nr_sequencia
	from	pls_regra_rescisao_benef
	where	nr_seq_contrato = nr_seq_contrato_w
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate)
	and	((qt_dia_rescisao_w	<= dt_limite_movimentacao) or (dt_limite_movimentacao is null))
	and	((nr_seq_motivo_cancelamento	= nr_seq_motivo_cancel_p) or (nr_seq_motivo_cancelamento is null))	
	order by dt_limite_movimentacao desc, 
		nvl(nr_seq_motivo_cancelamento,-1);

Cursor C02 is
	select	nr_sequencia
	from	pls_regra_rescisao_benef
	where	nr_seq_contrato is null
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate)
	and	((qt_dia_rescisao_w	<= dt_limite_movimentacao) or (dt_limite_movimentacao is null))
	and	((nr_seq_motivo_cancelamento	= nr_seq_motivo_cancel_p) or (nr_seq_motivo_cancelamento is null))
	and 	((ie_tipo_estipulante = ie_estipulante_w) or (ie_tipo_estipulante = 'A'))
	order by dt_limite_movimentacao desc, 
		nvl(nr_seq_motivo_cancelamento,-1);		

begin

select	decode (max(b.cd_pf_estipulante), null,'PJ','PF'),
	max(a.nr_seq_contrato)
into	ie_estipulante_w,
	nr_seq_contrato_w
from	pls_contrato b,
	pls_segurado a
where	a.nr_seq_contrato	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_segurado_p;

qt_dia_rescisao_w	:= to_number(to_char(dt_rescisao_p,'dd'));
dt_rescisao_nova_w	:= dt_rescisao_p;

open C01;
loop
fetch C01 into	
	nr_seq_regra_rescisao_w;
exit when C01%notfound;
end loop;
close C01;

if (nr_seq_regra_rescisao_w is null) then
	open C02;
	loop
	fetch C02 into	
		nr_seq_regra_rescisao_w;
	exit when C02%notfound;
	end loop;
	close C02;
end if;

if	(nr_seq_regra_rescisao_w is not null) then
	select	dt_dia_inclusao,
		ie_tipo_data
	into	dt_dia_rescisao_w,
		ie_tipo_data_w
	from	pls_regra_rescisao_benef
	where	nr_sequencia	= nr_seq_regra_rescisao_w;
	
	qt_meses_sub_w	:= 0;
	
	if	(ie_tipo_data_w = 'A') then
		qt_meses_sub_w	:= 0;
	elsif	(ie_tipo_data_w = 'P') then
		qt_meses_sub_w	:= 1;
	elsif	(ie_tipo_data_w = 'SP') then
		qt_meses_sub_w	:= 2;
	end if;
	
	begin
	dt_rescisao_nova_w	:= add_months(to_date(dt_dia_rescisao_w||'/'||to_char(dt_rescisao_p,'mm/yyyy')),qt_meses_sub_w);
	exception
	when others then
		dt_rescisao_nova_w	:= add_months(last_day(dt_rescisao_p),qt_meses_sub_w);
	end;
end if;

return	dt_rescisao_nova_w;

end pls_obter_dt_rescisao_benef;
/
