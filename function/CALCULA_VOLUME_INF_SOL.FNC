create or replace
function calcula_volume_inf_sol(nr_prescricao_p  in number,
								nr_seq_solucao_p in number) return number is

qt_duracao_w		number(15,4) := 0;
qt_dosagem_ant_w	number(15,4) := 0;
dt_evento_ant_w		date;
qt_total_w			number(15,4) := 0;
qt_volume_w			number(15,4) := 0;
qt_dosagem_w		number(15,4) := 0;
dt_alteracao_w		date;
qt_volume_adep_w	prescr_solucao.qt_volume_adep%type;

cursor c01 is
select	converte_vel_infusao(ie_tipo_dosagem,'mlh',qt_dosagem),
		dt_alteracao
from	prescr_solucao_evento
where	nr_prescricao = nr_prescricao_p
and 	nr_seq_solucao = nr_seq_solucao_p
and		obter_status_etapa_sol(nr_prescricao,nr_seq_solucao,nr_etapa_evento) = 'I'
and		ie_evento_valido = 'S'
and		ie_alteracao in (1,3,5,35)
and		qt_dosagem is not null
order by
		nr_sequencia;
begin
dt_evento_ant_w := null;

select	nvl(max(a.qt_volume_adep),0)
into	qt_volume_adep_w
from	prescr_solucao	a
where	a.nr_prescricao	= nr_prescricao_p
and		a.nr_seq_solucao = nr_seq_solucao_p;

open c01;
loop
fetch c01 into	qt_dosagem_w,
				dt_alteracao_w;
exit when c01%notfound;
	begin
		if (dt_evento_ant_w is not null) then
			select to_number(round((dt_alteracao_w - dt_evento_ant_w) * 24,2))
			into qt_duracao_w
			from dual;
			qt_volume_w := (qt_duracao_w * qt_dosagem_ant_w);
			qt_total_w:= qt_total_w + qt_volume_w;
		end if;
		dt_evento_ant_w  := dt_alteracao_w;
		qt_dosagem_ant_w := qt_dosagem_w;
	end;
end loop;
if (dt_evento_ant_w is not null) then
	select to_number(round((sysdate - dt_evento_ant_w) * 24,2))
	into qt_duracao_w
	from dual;
	qt_volume_w := (qt_duracao_w * qt_dosagem_ant_w);
	qt_total_w:= qt_total_w + qt_volume_w;
end if;

if	(qt_total_w > qt_volume_adep_w) then
	qt_total_w	:= qt_volume_adep_w;
end if;

return nvl(qt_total_w,0);

end calcula_volume_inf_sol;
/
