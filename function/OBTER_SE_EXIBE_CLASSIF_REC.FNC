create or replace
function obter_se_exibe_classif_rec
		(nr_seq_classif_rec_p	number,
		cd_setor_atendimento_p	number,
		cd_perfil_p		number,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2 default null) 
		return varchar2 is

ie_exibir_w		varchar2(10) := 'S';
qt_reg_w		number(5,0);

cursor c01 is
select	nvl(ie_exibir,'N')
from	classif_recomendacao_regra
where	nr_seq_classificacao					= nr_seq_classif_rec_p
and	nvl(cd_setor_atendimento,nvl(cd_setor_atendimento_p,0))	= nvl(cd_setor_atendimento_p,nvl(cd_setor_atendimento,0))
and	nvl(cd_perfil,nvl(cd_perfil_p,0))				= nvl(cd_perfil_p,nvl(cd_perfil,0))
--and	((cd_estabelecimento is null) or (cd_estabelecimento = cd_estabelecimento_p))
and	((cd_estab is null) or (cd_estab = cd_estabelecimento_p))
and	((cd_pessoa_fisica is null) or (cd_pessoa_fisica = obter_pf_usuario(nm_usuario_p,'C')))
order by nvl(cd_pessoa_fisica,0),
	nvl(cd_perfil,0),
	nvl(cd_setor_atendimento,0);

begin
select	count(*)
into	qt_reg_w
from	classif_recomendacao_regra
where	nr_seq_classificacao	= nr_seq_classif_rec_p;

if	(qt_reg_w > 0) then
	ie_exibir_w	:= 'N';
	open C01;
	loop
	fetch C01 into	
		ie_exibir_w;
	exit when C01%notfound;
		ie_exibir_w	:= ie_exibir_w;
	end loop;
	close C01;

end if;

return ie_exibir_w;

end obter_se_exibe_classif_rec;
/
