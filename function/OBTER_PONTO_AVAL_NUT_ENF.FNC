create or replace
function obter_ponto_aval_nut_enf(	nr_sequencia_p	number)
 		    	return number is
				
qt_ponto_w	tipo_aval_nut_enf.qt_ponto%type;
begin

select	max(qt_ponto)
into	qt_ponto_w
from	tipo_aval_nut_enf
where	nr_sequencia	= nr_sequencia_p;

return	nvl(somente_numero(QT_PONTO_w),0);

end obter_ponto_aval_nut_enf;
/