CREATE OR REPLACE FUNCTION OBTER_BANCO_PF_PJ_CSFX (	cd_pessoa_fisica_p	varchar2,
							cd_cgc_p		varchar2,
							ie_opcao_p		varchar2,
							cd_estabelecimento_p 	number) return varchar2 is

cd_banco_w		number(3,0);
cd_banco2_w		varchar2(20);
ds_retorno_w		varchar2(255);
cd_agencia_bancaria_w	varchar2(8);
cd_agencia_bancaria2_w	varchar2(20);
nr_conta_w		varchar(20);
NR_DIGITO_CONTA_w	varchar2(2);
NR_DIGITO_CONTA2_w	varchar2(50);
ie_digito_agencia_w	varchar2(20);

ds_banco_w		varchar2(20);
ds_agencia_w		varchar2(20);
ds_conta_w		varchar2(20);
ds_digito_w		varchar2(20);
begin

if	(nvl(cd_pessoa_fisica_p, 'X') <> 'X') then

	select	max(A.cd_banco),
		max(A.cd_agencia_bancaria),
		max(A.ie_digito_agencia),
		max(A.nr_conta),
		max(A.nr_digito_conta)
	into	cd_banco_w,
		cd_agencia_bancaria_w,
		ie_digito_agencia_w,
		nr_conta_w,
		nr_digito_conta_w
	from	pessoa_fisica_conta A,
        PESSOA_FIS_CONTA_ESTAB B
      
	where	a.CD_BANCO = b.cd_banco and
        a.CD_AGENCIA_BANCARIA = a.CD_AGENCIA_BANCARIA and
        a.NR_CONTA = b.nr_conta and
         a.CD_PESSOA_FISICA = b.cd_pessoa_fisica and
         B.CD_ESTABELECIMENTO = cd_estabelecimento_p and
         A.cd_pessoa_fisica	= cd_pessoa_fisica_p and	
         A.ie_situacao		= 'A'	and	
         A.ie_conta_pagamento	= 'S';

elsif	(nvl(cd_cgc_p, 'X') <> 'X') then

	select	max(cd_banco),
		max(cd_agencia_bancaria),
		max(ie_digito_agencia),
		max(nr_conta),
		max(nr_digito_conta)
	into	cd_banco_w,
		cd_agencia_bancaria_w,
		ie_digito_agencia_w,
		nr_conta_w,
		nr_digito_conta_w
	from	pessoa_juridica_conta a
	where	cd_cgc		= cd_cgc_p
	and	ie_situacao	= 'A'
	and	ie_conta_pagamento = 'S';

end if;

if	(ie_opcao_p	= 'B') then
	ds_retorno_w	:= cd_banco_w;
end if;

if	(ie_opcao_p	= 'CC') then

	if (nr_conta_w is not null) then
		ds_retorno_w	:= nr_conta_w;
	end if;
	if (nr_digito_conta_w is not null) then
		ds_retorno_w	:= ds_retorno_w || '/' || nr_digito_conta_w;
	end if;
end if;

if	(ie_opcao_p in('C','CD','D')) then
	ds_banco_w	:= wheb_mensagem_pck.get_texto(303140);
	ds_agencia_w	:= wheb_mensagem_pck.get_texto(303141);
	ds_conta_w	:= wheb_mensagem_pck.get_texto(303142);
	ds_digito_w	:= wheb_mensagem_pck.get_texto(303143);
end if;

if	(ie_opcao_p	= 'C') then
	
	if (cd_banco_w is not null) then
		cd_banco2_w := ds_banco_w || cd_banco_w;
	end if;
		
	if (cd_agencia_bancaria_w is not null) then
		cd_agencia_bancaria2_w := ds_agencia_w || cd_agencia_bancaria_w;
	end if;

	if (nr_conta_w is not null) then
		nr_conta_w := ds_conta_w || nr_conta_w;
	end if;

	ds_retorno_w	:= cd_banco2_w || ' ' || cd_agencia_bancaria2_w || ' ' || nr_conta_w;
end if;

if	(ie_opcao_p	= 'CD') then
	
	if (cd_banco_w is not null) then
		cd_banco2_w := ds_banco_w || cd_banco_w;
	end if;
		
	if (cd_agencia_bancaria_w is not null) then
		cd_agencia_bancaria2_w := ds_agencia_w || cd_agencia_bancaria_w || '-' || ie_digito_agencia_w;
	end if;

	if (nr_conta_w is not null) then
		nr_conta_w := substr(ds_conta_w || nr_conta_w || '-' || nr_digito_conta_w,1,20);
	end if;

	ds_retorno_w	:= cd_banco2_w || ' ' || cd_agencia_bancaria2_w || ' ' || nr_conta_w;
end if;

if	(ie_opcao_p	= 'D') then
	
	if (cd_banco_w is not null) then
		cd_banco2_w := ds_banco_w || cd_banco_w;
	end if;
		
	if (cd_agencia_bancaria_w is not null) then
		cd_agencia_bancaria2_w := ds_agencia_w || cd_agencia_bancaria_w;
	end if;

	if (nr_conta_w is not null) then
		nr_conta_w := ds_conta_w || nr_conta_w;
	end if;
	
	if (nr_digito_conta_w is not null) then
		nr_digito_conta2_w := ds_digito_w || nr_digito_conta_w;
	end if;

	ds_retorno_w	:= cd_banco2_w || ' ' || cd_agencia_bancaria2_w || ' ' || nr_conta_w || ' ' || nr_digito_conta2_w;
end if;

if	(ie_opcao_p	= 'AC') then -- Apenas Conta
	ds_retorno_w	:= nr_conta_w;
end if;

if	(ie_opcao_p	= 'ADC') then -- Apenas Digito Conta
	ds_retorno_w	:= nr_digito_conta_w;
end if;

return	ds_retorno_w;

end OBTER_BANCO_PF_PJ_CSFX;

/