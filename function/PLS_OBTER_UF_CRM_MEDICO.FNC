create or replace 
function pls_obter_uf_crm_medico
		(cd_pessoa_fisica_p	varchar2) 
		return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter UF do CRM do m�dico
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [   ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/		
		
ds_retorno_w	varchar2(20);

begin

if	(cd_pessoa_fisica_p is not null) then
	begin

	select	max(uf_crm)
	into	ds_retorno_w
	from	medico
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

	end;
end if;

return	ds_retorno_w;

end pls_obter_uf_crm_medico;
/
