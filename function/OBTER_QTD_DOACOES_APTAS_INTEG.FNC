create or replace
function obter_qtd_doacoes_aptas_integ( cd_pessoa_fisica_p	varchar2)
 		    	return number is

qtd_w	number(5);
begin

select	count(*)
into	qtd_w
from   	san_doacao
where  	ie_avaliacao_final 	= 'A'
and	cd_pessoa_fisica 	= cd_pessoa_fisica_p;

return	qtd_w;

end obter_qtd_doacoes_aptas_integ;
/
