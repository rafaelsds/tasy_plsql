create or replace
function FPR_peso_controle_dialise(	cd_pessoa_fisica_p	varchar2,
					seq_grid_p		number,
					ie_opcao_p		varchar2)
 		    	return number is

nr_seq_resultado_w		number(10);
dt_resultado_exame_w		date;
nr_seq_resultado_prescr_w	number(10);
dt_referencia_w			date;		
peso_w			number(15,4);		

begin
select 	max(dt_referencia)
into	dt_referencia_w
from	HD_EXAME_GRID_CONTROLE
where 	nr_sequencia = seq_grid_p;

select 	nvl(max(c.nr_seq_resultado),0)
into	nr_seq_resultado_w
from	exame_laboratorio d,
	exame_lab_resultado a,
	exame_lab_result_item c
where	a.nr_seq_resultado 	= c.nr_seq_resultado
and	d.nr_seq_exame 		= c.nr_seq_exame
and	trunc(nvl(c.dt_digitacao,a.dt_resultado),'month') = trunc(dt_referencia_w,'month')
--and	c.dt_digitacao		<= dt_referencia_p
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
and	c.nr_seq_exame		= 475
and	(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado)) is not null;

select 	nvl(max(c.nr_seq_resultado),0)
into	nr_seq_resultado_prescr_w
from	exame_laboratorio d,
	exame_lab_resultado a,
	exame_lab_result_item c,
	prescr_medica b
where	a.nr_seq_resultado 	= c.nr_seq_resultado
and	d.nr_seq_exame 		= c.nr_seq_exame
and 	(Obter_data_aprov_lab(a.nr_prescricao, c.nr_seq_prescr) is not null)
and	trunc(nvl(c.dt_digitacao,a.dt_resultado),'month') = trunc(dt_referencia_w,'month')
and	b.cd_pessoa_fisica	= cd_pessoa_fisica_p
and	c.nr_seq_exame		= 475
and	b.nr_prescricao		= a.nr_prescricao
and	(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado)) is not null;



if	(nr_seq_resultado_prescr_w > nr_seq_resultado_w) then
	nr_seq_resultado_w	:= nr_seq_resultado_prescr_w;
end if;


if	(nr_seq_resultado_w > 0) then

	select	nvl(max(c.dt_digitacao),max(a.dt_resultado))
	into	dt_resultado_exame_w
	from	exame_laboratorio d,
		exame_lab_result_item c,
		exame_lab_resultado a
	where	c.nr_seq_resultado 	= nr_seq_resultado_w
	and	a.nr_seq_resultado 	= c.nr_seq_resultado
	and	d.nr_seq_exame 		= c.nr_seq_exame
	and	c.nr_seq_exame		= 475
	and	d.nr_seq_exame 		= c.nr_seq_exame;
	
end if;

if	(ie_opcao_p = 'PE') then
	select 	round(sum(qt_peso_pre)/count(*),2)
	into	peso_w
	from	hd_dialise
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	qt_peso_pre > 0
	and	trunc(dt_dialise) = trunc(dt_resultado_exame_w);
elsif	(ie_opcao_p = 'PO') then
	select 	round(sum(qt_peso_pos)/count(*),2)
	into	peso_w
	from	hd_dialise
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	qt_peso_pre > 0
	and	trunc(dt_dialise) = trunc(dt_resultado_exame_w);	
end if;


return	peso_w;

end FPR_peso_controle_dialise;
/