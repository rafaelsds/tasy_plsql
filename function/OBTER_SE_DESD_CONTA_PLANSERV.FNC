create or replace
function obter_se_desd_conta_planserv(	nr_interno_conta_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) 
 		    	return varchar2 is

cd_convenio_w			convenio.cd_convenio%type:= 0;
ie_status_acerto_w		conta_paciente.ie_status_acerto%type;
ie_permite_desd_planserv_w	convenio_estabelecimento.ie_permite_desd_planserv%type:= 'N';
ie_permite_desd_etapa_w		varchar2(1) :='N';
ds_retorno_w			varchar2(1) :='N';

begin

begin
	select	cd_convenio_parametro,
		ie_status_acerto
	into	cd_convenio_w,
		ie_status_acerto_w	
	from	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;
exception
	when others then
	cd_convenio_w := 0;
	ie_status_acerto_w:=0;
end;

if (cd_convenio_w > 0) and
   (ie_status_acerto_w = 1) then

	begin
		select	nvl(ie_permite_desd_planserv,'N')
		into	ie_permite_desd_planserv_w
		from	convenio_estabelecimento
		where	cd_estabelecimento = cd_estabelecimento_p
		and	cd_convenio        = cd_convenio_w;
		
		ie_permite_desd_etapa_w:= nvl(substr(obter_conta_paciente_etapa(nr_interno_conta_p,'E'),1,1),'S');
		
	exception 
		when others then
		ie_permite_desd_planserv_w:= 'N';
		ie_permite_desd_etapa_w:= 'N';
	end;	
	
	if  (ie_permite_desd_etapa_w = 'S') and
	    (ie_permite_desd_planserv_w = 'S') then
	    ds_retorno_w:='S';
	end if;

end if;

return	ds_retorno_w;

end obter_se_desd_conta_planserv;
/