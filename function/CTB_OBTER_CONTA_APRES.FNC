create or replace 
FUNCTION CTB_Obter_Conta_Apres(	cd_conta_contabil_p	varchar2)
				return varchar2 is

cd_classificacao_w			varchar2(40);
ds_conta_contabil_w		Varchar2(255);
ds_conta_w			Varchar2(254);

begin

select	substr(max(ctb_obter_classif_conta(cd_conta_contabil,cd_classificacao,null)),1,40) cd_classificacao,
	max(ds_conta_contabil)
into	cd_classificacao_w,
	ds_conta_contabil_w
from	conta_contabil
where	cd_conta_contabil	= cd_conta_contabil_p;

ds_conta_w	:= SUBSTR(lpad(' ', length(cd_classificacao_w)-1) || ds_conta_contabil_w,1,254);

return ds_conta_w;

end CTB_Obter_Conta_Apres;
/