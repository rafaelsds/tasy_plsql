create or replace
function qua_obter_se_gravid_evento_lib(
			nr_seq_gravidade_p	number,
			ie_classificacao_p	varchar2)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(1) := 'S';
qt_existe_w		number(10,0);
nr_seq_classif_w		number(10);

begin

if	(nvl(nr_seq_gravidade_p,0) > 0) and
	(nvl(ie_classificacao_p,'0') <> '0') then
	begin
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_classif_w
	from	qua_classif_evento
	where	cd_empresa = nvl(obter_empresa_estab(wheb_usuario_pck.get_cd_estabelecimento),cd_empresa)
	and	ie_classificacao = ie_classificacao_p;
	
	select	count(*)
	into	qt_existe_w
	from	qua_grav_evento_classif
	where	ie_situacao = 'A'
	and	nr_seq_gravidade = nr_seq_gravidade_p;
	
	if	(nr_seq_classif_w > 0) and
		(qt_existe_w > 0) then
		begin
		select	'S'
		into	ds_retorno_w
		from	qua_grav_evento_classif
		where	nr_seq_gravidade = nr_seq_gravidade_p
		and	nr_seq_classificacao = nr_seq_classif_w
		and	ie_situacao = 'A'
		and	rownum < 2;
		exception
		when others then
			ds_retorno_w := 'N';
		end;
	end if;
	end;
end if;

return	ds_retorno_w;

end qua_obter_se_gravid_evento_lib;
/