create or replace
function pls_obter_se_exige_espec
			(	nr_seq_conta_p		number)
				return varchar2 is


cd_medico_w			varchar2(10);
nr_seq_conselho_exec_w		number(10); 
nr_seq_conselho_solic_w		number(10); 
uf_crm_exec_w			varchar2(2);
uf_crm_solic_imp_w		varchar2(20); 
qt_reg_w			number(10);
nr_seq_prestador_exec_w		number(10);
qt_esp_w			number(5);
ie_cooperador_w			varchar2(1);
ds_retorno_w			varchar2(1) := 'N';
dt_conta_w			date;

begin

select	cd_medico_executor,
	nr_seq_conselho_exec, 
	nr_seq_conselho_solic, 
	uf_crm_exec, 
	uf_crm_solic_imp,
	nr_seq_prestador_exec,
	dt_atendimento_referencia
into	cd_medico_w,
	nr_seq_conselho_exec_w, 
	nr_seq_conselho_solic_w, 
	uf_crm_exec_w, 
	uf_crm_solic_imp_w,
	nr_seq_prestador_exec_w,
	dt_conta_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_p;


if	(cd_medico_w is not null) then
	select	decode(pls_obter_se_cooperado_ativo(cd_medico_w,dt_conta_w,null),'S','C','N')
	into	ie_cooperador_w
	from	dual;
	
	select	count(*)
	into	qt_reg_w
	from	pls_regra_exige_espec
	where	nr_seq_conselho = nr_seq_conselho_exec_w
	and	uf_crm = uf_crm_exec_w
	and	ie_situacao = 'A'
	and	(ie_tipo_medico = ie_cooperador_w or ie_tipo_medico = 'A');
	
	if	(qt_reg_w > 0) then
		
		select	count(*)
		into	qt_esp_w
		from	medico_especialidade
		where	cd_pessoa_fisica	= cd_medico_w;		
		
		if	(qt_esp_w = 0) then
			ds_retorno_w := 'S';
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_exige_espec;
/