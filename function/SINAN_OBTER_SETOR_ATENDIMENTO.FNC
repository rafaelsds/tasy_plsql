create or replace
function sinan_obter_setor_atendimento(nm_usuario_p	varchar2)
 		    	return varchar2 is

ds_setor_atendimento_w	varchar2(255);			
begin
select 	max((select max(b.ds_setor_atendimento)
	from	setor_atendimento b
	where	a.cd_setor_atendimento = b.cd_setor_atendimento))
into	ds_setor_atendimento_w
from	usuario a 
where	a.nm_usuario = nm_usuario_p;

return	ds_setor_atendimento_w;

end sinan_obter_setor_atendimento;
/