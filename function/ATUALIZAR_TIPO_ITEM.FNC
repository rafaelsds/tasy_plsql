CREATE OR REPLACE
PROCEDURE ATUALIZAR_TIPO_ITEM  	(ie_tipo_item_instituicao_p   VARCHAR2,
					 nm_usuario_p                      VARCHAR2,
					 nr_sequencia_p	             NUMBER) IS
BEGIN

	UPDATE pepo_item 
	SET         ie_tipo_item_instituicao = ie_tipo_item_instituicao_p,
	                nm_usuario = nm_usuario_p
	WHERE  nr_sequencia = nr_sequencia_p;	
	
	COMMIT;

END ATUALIZAR_TIPO_ITEM;
/