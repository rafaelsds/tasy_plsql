create or replace
function Obter_PESSOA_FISICA_LOC_TRAB(
			cd_pessoa_fisica_p		Varchar2) 
			return varchar2 is 

ds_setor_atendimento_w		varchar2(255);
cd_setor_atendimento_w		number(15,2);
			
begin

select	nvl(max(cd_setor_atendimento),0)
into	cd_setor_atendimento_w
from 	PESSOA_FISICA_LOC_TRAB
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(cd_setor_atendimento_w = 0) then
	
	select	nvl(max(a.cd_setor_atendimento),0)
	into	cd_setor_atendimento_w
	from	usuario a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
	
end if;

if		(cd_setor_atendimento_w > 0) then
		select	max(ds_setor_atendimento)
		into	ds_setor_atendimento_w
		from	setor_atendimento
		where	cd_setor_atendimento	= cd_setor_atendimento_w;
end if;

return	ds_setor_atendimento_w;

end Obter_PESSOA_FISICA_LOC_TRAB;
/