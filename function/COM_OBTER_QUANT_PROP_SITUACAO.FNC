create or replace
function com_obter_quant_prop_situacao (
		nr_seq_cliente_p	number,
		ie_situacao_p		varchar2)
		return number is
		
qt_propostas_w	number(10,0) := 0;
		
begin
if	(nr_seq_cliente_p is not null) then
	begin
	if	(ie_situacao_p = 'C') then
		begin
		select	count(distinct(p.nr_sequencia))
		into	qt_propostas_w
		from	com_cliente_prop_item i,
			com_cliente_proposta p
		where	i.nr_seq_proposta = p.nr_sequencia
		and	p.nr_seq_cliente = nr_seq_cliente_p
		--and	p.dt_liberacao is not null
		and	p.dt_cancelamento is not null
		--and	nvl(p.dt_vencimento,sysdate) >= sysdate
		and	nvl(i.ie_orcam_coml_wheb,'N') = 'S';
		end;
	elsif	(ie_situacao_p = 'V') then
		begin
		select	count(distinct(p.nr_sequencia))
		into	qt_propostas_w
		from	com_cliente_prop_item i,
			com_cliente_proposta p
		where	i.nr_seq_proposta = p.nr_sequencia
		and	p.nr_seq_cliente = nr_seq_cliente_p
		--and	p.dt_liberacao is not null
		and	trunc(p.dt_vencimento,'dd') < trunc(sysdate,'dd')
		--and	nvl(p.dt_vencimento,sysdate) >= sysdate
		and	nvl(i.ie_orcam_coml_wheb,'N') = 'S';		
		end;
	end if;
	end;
end if;
return qt_propostas_w;
end com_obter_quant_prop_situacao;
/