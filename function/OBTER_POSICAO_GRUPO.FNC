create or replace 
function obter_posicao_grupo(
					cd_estabelecimento_p	number,
					cd_perfil_p				number,
					cd_setor_usuario_p		number,
					nm_usuario_p			varchar2,
					ie_grupo_p				varchar2,
					ie_funcao_p 			varchar2 ) return number is
					
nr_sequencia_w			cpoe_regra_ordenacao.nr_sequencia%type;
nr_seq_apresentacao_w	cpoe_regra_ordem_grupo.nr_seq_apresentacao%type;

cursor c01 is 
select	a.nr_seq_regra_ordem
from	cpoe_regra_ordem_lib_v a
where	nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0)
and		nvl(a.cd_setor_atendimento, nvl(cd_setor_usuario_p,0)) = nvl(cd_setor_usuario_p,0)
and		nvl(a.nm_usuario_regra, nvl(nm_usuario_p,'XPTO')) = nvl(nm_usuario_p,'XPTO')
and		nvl(a.cd_perfil, nvl(cd_perfil_p,0)) = nvl(cd_perfil_p,0)
and		nvl(a.ie_situacao,'A') = 'A'
--and 		nvl(a.ie_funcao_cpoe, 'A') = ie_funcao_p 
and (a.ie_funcao_cpoe = 'A' or a.ie_funcao_cpoe = ie_funcao_p)
order by	nvl(a.nm_usuario_regra,'') desc,
			nvl(a.cd_setor_atendimento,0),
			nvl(a.cd_perfil,0),
			nvl(a.cd_estabelecimento,0);
begin 

open c01;
loop
fetch c01 into	nr_sequencia_w;
exit when c01%notfound;
end loop;
close c01;

select	max(nr_seq_apresentacao)
into	nr_seq_apresentacao_w 
from	cpoe_regra_ordem_grupo
where	nr_seq_regra_ordem = nr_sequencia_w
and		ie_grupo = ie_grupo_p;

return nr_seq_apresentacao_w;

end obter_posicao_grupo;
/
