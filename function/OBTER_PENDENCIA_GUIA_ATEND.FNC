create or replace
function obter_pendencia_guia_atend(
				nr_atendimento_p	number) return varchar2 is

dt_alta_w			date;
cd_autorizacao_w		varchar2(20);
cd_senha_w		varchar2(20);
dt_inicio_w		date;
dt_final_w		date;
qt_dia_w			number(3);
qt_fora_vigencia_w		number(3);
dt_final_max_w		date;
qt_proc_pend_w		number(10);
ds_retorno_w		varchar2(255) := '';
cd_estabelecimento_w	number(4);
qt_mat_atend_paciente_w	number(10);
qt_proced_expirada_w	number(10);


cursor c01 is
select	cd_autorizacao,
	cd_senha,
	dt_inicio_vigencia,
	nvl(dt_fim_vigencia, nvl(dt_alta_w, sysdate)),
	nvl(qt_dia_autorizado,0)
from	convenio b,
	autorizacao_convenio a
where	a.cd_convenio	= b.cd_convenio
and	a.nr_atendimento	= nr_atendimento_p
and	b.ie_tipo_convenio	<> 1
union
select	nr_doc_convenio,
	cd_senha,
	dt_inicio_vigencia,
	nvl(dt_final_vigencia, nvl(dt_alta_w, sysdate)),
	0
from	convenio b,
	atend_categoria_convenio a
where	a.cd_convenio	= b.cd_convenio
and	a.nr_atendimento	= nr_atendimento_p
and	b.ie_tipo_convenio	<> 1;


begin

select	dt_alta,
	cd_estabelecimento
into	dt_alta_w,
	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

select	decode(count(*), 0, '', '33 ')
into	ds_retorno_w
from	convenio b,
	atendimento_paciente c,
	atend_categoria_convenio a
where	a.cd_convenio	= b.cd_convenio
and	a.nr_atendimento	= c.nr_atendimento
and	a.nr_atendimento	= nr_atendimento_p
and	b.ie_tipo_convenio	in (2,5,6,7,8,9)
--and	b.ie_exige_senha_atend	= 'S'
and	obter_se_exige_guia_senha(a.cd_convenio, c.ie_tipo_atendimento, null, 'S', cd_estabelecimento_w, c.ie_carater_inter_sus, c.ie_clinica,null,null,null,null,a.cd_categoria, a.cd_Plano_Convenio, c.cd_procedencia)	= 'S'
and	a.cd_senha	is null;

open c01;
loop
fetch c01 into
	cd_autorizacao_w,
	cd_senha_w,
	dt_inicio_w,
	dt_final_w,
	qt_dia_w;
exit when c01%notfound;

	if	(dt_final_w > dt_final_max_w) then
		dt_final_max_w := dt_final_w;
	end if;

	select	count(*)
	into	qt_proced_expirada_w
	from	procedimento_paciente
	where	nr_atendimento	= nr_atendimento_p
	and	nr_doc_convenio	= cd_autorizacao_w
	and	dt_conta not between dt_inicio_w and dt_final_w;
	
	select	count(*)
	into	qt_mat_atend_paciente_w
	from	material_atend_paciente
	where	nr_atendimento	= nr_atendimento_p
	and	nr_doc_convenio	= cd_autorizacao_w
	and	dt_conta not between dt_inicio_w and dt_final_w;

	qt_proc_pend_w := (qt_proced_expirada_w + qt_mat_atend_paciente_w);
	
/*	os144325 - diego
	select	sum(qt_proced_expirada)
	into	qt_proc_pend_w
	from	(
		select	count(*) qt_proced_expirada
		from	procedimento_paciente
		where	nr_atendimento	= nr_atendimento_p
		and	nr_doc_convenio	= cd_autorizacao_w
		and	dt_conta not between dt_inicio_w and dt_final_w
		union all
		select	count(*)
		from	material_atend_paciente
		where	nr_atendimento	= nr_atendimento_p
		and	nr_doc_convenio	= cd_autorizacao_w
		and	dt_conta not between dt_inicio_w and dt_final_w);*/

	if	(qt_proc_pend_w > 0) and
		(nvl(instr(ds_retorno_w, '300 '),0) = 0) then
		ds_retorno_w	:= ds_retorno_w || '300 ';
	end if;	

end loop;
close c01;

if	(nvl(instr(ds_retorno_w, '300 '), 0) = 0) and
	(dt_alta_w is null) and
	(dt_final_max_w < sysdate) then
	ds_retorno_w := '300 ';
end if;

select	count(*)
into	qt_proc_pend_w
from	convenio c,
	procedimento_paciente b,
	conta_paciente a
where	a.nr_atendimento	= nr_atendimento_p
and	a.nr_interno_conta	= b.nr_interno_conta
and	a.cd_convenio_parametro	= c.cd_convenio
and	c.ie_tipo_convenio	<> 1
and	b.cd_situacao_glosa	<> 0
and	a.ie_status_acerto	= 1
and	b.nr_doc_convenio is null;

if	(qt_proc_pend_w > 0) then
	ds_retorno_w := ds_retorno_w || '302 ';
else
	select	count(*)
	into	qt_proc_pend_w
	from	convenio c,
		material_atend_paciente b,
		conta_paciente a
	where	a.nr_atendimento	= nr_atendimento_p
	  and	a.nr_interno_conta	= b.nr_interno_conta
	  and	a.cd_convenio_parametro	= c.cd_convenio
	  and	c.ie_tipo_convenio	<> 1
	  and	b.cd_situacao_glosa	<> 0
	  and	a.ie_status_acerto	= 1
	  and	b.nr_doc_convenio is null;

	if	(qt_proc_pend_w > 0) then
		ds_retorno_w	:= ds_retorno_w || '302 ';
	end if;
end if;

/* fabio 13/02/2004 - inclui select abaixo*/
select	count(*)
into	qt_fora_vigencia_w
from	atend_categoria_convenio a,
	atendimento_paciente c
where	a.nr_atendimento	= c.nr_atendimento
and	a.nr_atendimento	= nr_atendimento_p
and	a.dt_final_vigencia is not null
and	a.dt_final_vigencia	= 
		(select	max(b.dt_final_vigencia)
		from	atend_categoria_convenio b
		where	b.nr_atendimento = a.nr_atendimento) 
and	a.dt_final_vigencia	< sysdate
and	c.dt_alta is null;

if	(qt_fora_vigencia_w > 0) then
	ds_retorno_w	:= ds_retorno_w || '32 ';
end if;

return ds_retorno_w;

end obter_pendencia_guia_atend;
/