create or replace
function obter_endereco_completo(nr_sequencia_p number)
 		    	return varchar2 is

ds_endereco_w	end_endereco.ds_endereco_completo%type;
nm_pais_w	pais.nm_pais%type;
qt_pais_w	number(5)	:= 0;

Cursor c01 is
select	ds_endereco,
		ie_informacao
from 	end_endereco
connect by prior nr_seq_superior = nr_sequencia
start with nr_sequencia = nr_sequencia_p;
c01_w	c01%rowtype;

begin

open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin
	if	(nvl(c01_w.ds_endereco,'X') <> 'X') then
		ds_endereco_w := substr(ds_endereco_w || c01_w.ds_endereco || ', ',1,4000);
	end if;
	
	if	(c01_w.ie_informacao = 'PAIS') then
		qt_pais_w	:= qt_pais_w + 1;
	end if;
	
	end;
end loop;
close c01;

ds_endereco_w := substr(ds_endereco_w,1,instr(ds_endereco_w,',',-1)-1);

begin
select 	c.nm_pais
into	nm_pais_w
from	end_endereco a,
	end_catalogo b,
	pais c
where	a.nr_seq_catalogo = b.nr_sequencia
and	b.nr_seq_pais = c.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;
exception
when others then
	nm_pais_w := null;
end;

if (nvl(nm_pais_w,'X') <> 'X') and
	(qt_pais_w = 0) then
	ds_endereco_w := ds_endereco_w || ', ' || nm_pais_w;
end if;

return	ds_endereco_w;

end obter_endereco_completo;
/