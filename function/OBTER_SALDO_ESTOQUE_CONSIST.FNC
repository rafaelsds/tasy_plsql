create or replace
function obter_saldo_estoque_consist(	
			cd_estabelecimento_p    	number,
			cd_material_p         		number,
  		     	cd_local_estoque_p      	number,
			dt_mesano_referencia_p		date,
			nr_seq_lote_p			number default null)
			return number is 
			
/*
SEMPRE QUE ALTERAR VERIFICAR TRATAMENTO PARA O SALDO DE LOTE
*/			

qt_saldo_w			number(15,4);
qt_estoque_w			number(15,4)	:= 0;
qt_cirurgia_w			number(18,6)	:= 0;
qt_material_gedipa_w		number(18,6)	:= 0;
qt_esp_cirurgia_w			number(18,6)	:= 0;
qt_emergencia_w			number(18,6)	:= 0;
qt_agenda_cirurgia_w		number(18,6)	:= 0;
qt_emprestimo_w			number(16,4)	:= 0;
qt_quimioterapia_w			number(15,4)	:= 0;
qt_requisicao_w			number(15,4)	:= 0;
qt_req_transf_estab_w		number(15,4)	:= 0;
qt_kit_w				number(15,4)	:= 0;
qt_kit_ww			number(15,4)	:= 0;
qt_kit_avulso_w			number(15,4)	:= 0;
cd_material_w			number(06,0);
ie_tipo_local_w			varchar2(5);
qt_conv_estoque_w		number(13,4);
cd_local_estoque_w		number(05,0);
dt_mesano_referencia_w		date;
ie_disp_quimioterapia_w		varchar2(1);
ie_disp_req_trans_w		varchar2(1);
ie_disp_prescr_eme_w		varchar2(1);
ie_disp_ag_cirur_w			varchar2(1);
ie_baixa_disp_w			varchar2(1);
ie_disp_comp_kit_estoque_w		varchar2(1);
ie_disp_reg_kit_estoque_w		varchar2(1);
ie_disp_emprestimo_lib_w		varchar2(1);
ie_disp_nf_emprestimo_w		varchar2(1);
ie_disp_esp_cirurgia_w		varchar2(1);
ie_disp_cirurgia_w			varchar2(1);
ie_disp_sep_gedipa_w		varchar2(1);
ie_disp_req_trans_estab_w	varchar2(1);
ie_estoque_lote_w		material_estab.ie_estoque_lote%type := 'X';
nr_seq_lote_w			material_lote_fornec.nr_sequencia%type;

cursor c01 is 
	select	/*+ index (s salesto_i2) */
		distinct a.cd_local_estoque
	from	saldo_estoque a, local_estoque b
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_material		= cd_material_w
	and a.cd_local_estoque = b.cd_local_estoque
	and	a.dt_mesano_referencia	>= dt_mesano_referencia_w
	and	a.cd_local_estoque	= cd_local_estoque_p	
	and	nvl(cd_local_estoque_p,0) > 0
	and nvl(b.ie_consiste_saldo_rep,'S') = 'S'
	union
	select	/*+ index (s salesto_i2) */
		distinct a.cd_local_estoque
	from	saldo_estoque a, local_estoque b
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and     a.cd_local_estoque = b.cd_local_estoque
	and	a.cd_material		= cd_material_w
	and	a.dt_mesano_referencia	>= dt_mesano_referencia_w
	and nvl(b.ie_consiste_saldo_rep,'S') = 'S'
	and	nvl(cd_local_estoque_p,0) = 0;
	
cursor c02 is
	select	cd_local_estoque
	from	saldo_estoque_lote
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_material		= cd_material_w
	and	dt_mesano_referencia	>= dt_mesano_referencia_w
	and	cd_local_estoque	= cd_local_estoque_p	
	and	nvl(cd_local_estoque_p,0) > 0
	and	nr_seq_lote 		= nr_seq_lote_w
	group 	by cd_local_estoque 
	union
	select	cd_local_estoque
	from	saldo_estoque_lote
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_material		= cd_material_w
	and	dt_mesano_referencia	>= dt_mesano_referencia_w
	and	nvl(cd_local_estoque_p,0) = 0
	and	nr_seq_lote 		= nr_seq_lote_w
	group 	by cd_local_estoque;

begin

select	max(dt_mesano_vigente),
	nvl(max(ie_disp_quimioterapia),'N'),
	nvl(max(ie_disp_req_trans), 'N'),
	nvl(max(ie_disp_prescr_eme), 'S'),
	nvl(max(ie_disp_ag_cirur), 'N'),
	nvl(max(ie_disp_comp_kit_estoque), 'N'),
	nvl(max(ie_disp_reg_kit_estoque), 'N'),
	nvl(max(ie_disp_emprestimo_lib), 'N'),
	nvl(max(ie_disp_nf_emprestimo),'N'),
	nvl(max(ie_disp_esp_cirurgia),'N'),
	nvl(max(ie_disp_cirurgia),'S'),
	nvl(max(ie_disp_req_trans_estab),'N'),
	nvl(max(ie_disp_sep_gedipa),'N')
into	dt_mesano_referencia_w,
	ie_disp_quimioterapia_w,
	ie_disp_req_trans_w,
	ie_disp_prescr_eme_w,
	ie_disp_ag_cirur_w,
	ie_disp_comp_kit_estoque_w,
	ie_disp_reg_kit_estoque_w,
	ie_disp_emprestimo_lib_w,
	ie_disp_nf_emprestimo_w,
	ie_disp_esp_cirurgia_w,
	ie_disp_cirurgia_w,
	ie_disp_req_trans_estab_w,
	ie_disp_sep_gedipa_w
from	parametro_estoque
where	cd_estabelecimento	= cd_estabelecimento_p;

select	max(cd_material_estoque)
into	cd_material_w
from	material
where	cd_material		= cd_material_p;

select	nvl(max(qt_conv_estoque_consumo),1)
into	qt_conv_estoque_w
from	material
where	cd_material		= cd_material_w;

nr_seq_lote_w := nvl(nr_seq_lote_p,0);

if	(nr_seq_lote_w > 0) then
	select	nvl(max(ie_estoque_lote),'N')
	into	ie_estoque_lote_w
	from	material_estab
	where	cd_material = cd_material_w
	and		cd_estabelecimento = cd_estabelecimento_p;
end if;

if	(ie_estoque_lote_w = 'N') or
	(nr_seq_lote_w <= 0) then
	begin
	
	open  c01;
	loop
	fetch c01 into
		cd_local_estoque_w;
	exit when c01%notfound;
		begin
		qt_emprestimo_w		:= 0;
		qt_cirurgia_w		:= 0;

		obter_saldo_estoque(cd_estabelecimento_p, cd_material_w, cd_local_estoque_w, null, qt_saldo_w);

		select	ie_tipo_local,
				nvl(ie_baixa_disp, 'N')
		into	ie_tipo_local_w,
				ie_baixa_disp_w
		from 	local_estoque
		where	cd_local_estoque = cd_local_estoque_w;

		if	(ie_tipo_local_w = 2) or (ie_baixa_disp_w = 'S') then /*Baixa do disponivel se o local for direto ou se o campo IE_DISP_ESTOQUE estiver marcada*/
			begin
			if	(ie_disp_cirurgia_w = 'S') then
				/*Busca as quantidades que estao em cirurgia no local do setor da cirurgia*/
				select	nvl(sum(qt_total_dispensar),0)
				into	qt_cirurgia_w
				from	setor_atendimento c,
					cirurgia b,
					material x,
					prescr_material a,
					prescr_medica m
				where	a.cd_motivo_baixa		= 0
				and	a.cd_material		= x.cd_material
				and	x.cd_material_estoque	= cd_material_w
				and	m.nr_prescricao		= b.nr_prescricao				
				and	m.nr_prescricao		= a.nr_prescricao
				and	b.cd_setor_atendimento	= c.cd_setor_atendimento
				and	c.cd_local_estoque	= cd_local_estoque_w
				and	a.ie_status_cirurgia	in ('CB','AD')
				and	a.ie_baixa_estoque_cir	is null;
			elsif	(ie_disp_cirurgia_w = 'C') then
				/*Busca as quantidades que estao em cirurgia no local de consistencia, se nao ter local de consistencia, busca do local do setor da cirurgia*/
				select 	sum(qt_cirurgia)
				into	qt_cirurgia_w
				from (  select	nvl(sum(qt_total_dispensar),0) qt_cirurgia
					from	setor_atendimento c,
						cirurgia b,
						material x,
						prescr_material a,
						prescr_medica m
					where	a.cd_motivo_baixa		= 0
					and	a.cd_material		= x.cd_material
					and	x.cd_material_estoque	= cd_material_w
					and	m.nr_prescricao		= b.nr_prescricao

					and	m.nr_prescricao		= a.nr_prescricao
					and	b.cd_setor_atendimento	= c.cd_setor_atendimento
					and 	a.cd_local_estoque is null
					and	c.cd_local_estoque = cd_local_estoque_w
					and	a.ie_status_cirurgia	in ('CB','AD')
					and	a.ie_baixa_estoque_cir	is null
					union all
					select	nvl(sum(qt_total_dispensar),0) qt_cirurgia
					from	setor_atendimento c,
						cirurgia b,
						material x,
						prescr_material a,
						prescr_medica m
					where	a.cd_motivo_baixa		= 0
					and	a.cd_material		= x.cd_material
					and	x.cd_material_estoque	= cd_material_w
					and	m.nr_prescricao		= b.nr_prescricao
					and	m.nr_prescricao		= a.nr_prescricao
					and	b.cd_setor_atendimento	= c.cd_setor_atendimento
					and	a.cd_local_estoque = cd_local_estoque_w
					and	a.ie_status_cirurgia	in ('CB','AD')
					and	a.ie_baixa_estoque_cir	is null );
			else
				qt_cirurgia_w := 0;
			end if;
			
			if	(ie_disp_esp_cirurgia_w = 'S') then
				SELECT	/*+ index (d PRESMAT_I3)*/
					NVL(SUM(obter_quantidade_convertida(d.cd_material, d.qt_total_dispensar, d.cd_unidade_medida, 'UME')),0)
				into	qt_esp_cirurgia_w
				FROM	setor_atendimento a,
					cirurgia b,
					prescr_medica c,
					prescr_material d,
					material e
				WHERE	b.nr_cirurgia		= c.nr_cirurgia
				AND	c.nr_prescricao		= d.nr_prescricao 
				AND	a.cd_setor_atendimento	= b.cd_setor_atendimento
				AND	d.cd_material		= e.cd_material		
				AND	a.cd_local_estoque	= cd_local_estoque_w
				AND	e.cd_material_estoque	= cd_material_w
				AND	d.cd_motivo_baixa	= 0
				AND	d.ie_status_cirurgia	IN ('CB','AD')
				AND	d.ie_baixa_estoque_cir	IS NULL;
			elsif	(ie_disp_esp_cirurgia_w = 'C') then
				select 	sum(qt_cirurgia)


				into	qt_esp_cirurgia_w
				from (  SELECT	/*+ index (d PRESMAT_I3)*/
						NVL(SUM(obter_quantidade_convertida(d.cd_material, d.qt_total_dispensar, d.cd_unidade_medida, 'UME')),0) qt_cirurgia
					FROM	setor_atendimento a,
						cirurgia b,
						prescr_medica c,
						prescr_material d,
						material e
					WHERE	b.nr_cirurgia		= c.nr_cirurgia
					AND	c.nr_prescricao		= d.nr_prescricao
					AND	a.cd_setor_atendimento	= b.cd_setor_atendimento
					AND	d.cd_material		= e.cd_material
					and 	d.cd_local_estoque is null
					and	a.cd_local_estoque = cd_local_estoque_w
					AND	e.cd_material_estoque	= cd_material_w
					AND	d.cd_motivo_baixa	= 0
					AND	d.ie_status_cirurgia	IN ('CB','AD')
					AND	d.ie_baixa_estoque_cir	IS NULL
					union all
					SELECT	/*+ index (d PRESMAT_I3)*/
						NVL(SUM(obter_quantidade_convertida(d.cd_material, d.qt_total_dispensar, d.cd_unidade_medida, 'UME')),0) qt_cirurgia
					FROM	setor_atendimento a,
						cirurgia b,
						prescr_medica c,
						prescr_material d,
						material e
					WHERE	b.nr_cirurgia		= c.nr_cirurgia
					AND	c.nr_prescricao		= d.nr_prescricao
					AND	a.cd_setor_atendimento	= b.cd_setor_atendimento
					AND	d.cd_material		= e.cd_material
					and	d.cd_local_estoque = cd_local_estoque_w
					AND	e.cd_material_estoque	= cd_material_w
					AND	d.cd_motivo_baixa	= 0
					AND	d.ie_status_cirurgia	IN ('CB','AD')
					AND	d.ie_baixa_estoque_cir	IS NULL ) ;
			end if;
			

			/*Busca as quantidades que estao prescricao de emergencia. indice definido, motivo OS276540*/
			if	(ie_disp_prescr_eme_w = 'S') then
				select	/*+ index (a PRESMAT_I3)*/
					nvl(sum(qt_total_dispensar),0)
				into	qt_emergencia_w
				from	setor_atendimento c,
					material x,
					prescr_medica b,
					prescr_material a
				where	a.nr_prescricao		= b.nr_prescricao
				and	a.cd_material           = x.cd_material
				and	b.cd_setor_atendimento  = c.cd_setor_atendimento
				and	b.ie_emergencia			= 'S'			
				and	x.cd_material_estoque   = cd_material_w			
				and	c.cd_local_estoque     	= cd_local_estoque_w
				and	a.cd_motivo_baixa		= 0
				and	a.ie_status_cirurgia	= 'PE';
			end if;


			/*Busca as quantidades que estao consistidos pela gestao da agenda cirurgica, e ainda nao tenham cirurgia vinculada*/
			if	(ie_disp_ag_cirur_w = 'S') then
				select	nvl(sum(qt_total_dispensar),0)
				into	qt_agenda_cirurgia_w
				from	setor_atendimento c,
					material x,
					prescr_medica b,
					prescr_material a
				where	a.nr_prescricao		= b.nr_prescricao
				and	b.cd_setor_atendimento	= c.cd_setor_atendimento
				and	a.cd_material		= x.cd_material
				and	c.cd_local_estoque	= cd_local_estoque_w
				and	x.cd_material_estoque	= cd_material_w
				and	a.cd_motivo_baixa	= 0
				and	a.ie_status_cirurgia	= 'CB'
				and	b.nr_seq_agenda		is not null
				and	b.nr_cirurgia		is null
				and not exists(
					select	1
					from	cirurgia c
					where	c.nr_prescricao = a.nr_prescricao);
			end if;			
			
			qt_cirurgia_w := dividir((nvl(qt_cirurgia_w,0) + nvl(qt_emergencia_w,0) + nvl(qt_agenda_cirurgia_w, 0)) , nvl(qt_conv_estoque_w,1));
			end;
		end if;
		
		/*Busca as quantidades que foram separadas no GEDIPA, porem que serao cobradas apenas na checagem do ADEP*/
		if	(ie_disp_sep_gedipa_w = 'S') then
		
			select	nvl(sum(c.qt_dispensar_hor),0)
			into	qt_material_gedipa_w
			from	adep_processo_item a,
				prescr_material b,
				prescr_mat_hor c,
				adep_processo d
			where	a.nr_seq_horario = c.nr_sequencia
			and	c.nr_prescricao = b.nr_prescricao
			and	c.nr_seq_material = b.nr_sequencia
			and	a.nr_seq_processo = d.nr_sequencia
			and	nvl(a.ie_nao_requisitado,'N') <> 'S'
			and	a.ie_lanca_conta_adep = 'S'
			and	b.cd_motivo_baixa = 0
			and	a.cd_material = cd_material_w
			and	b.cd_local_estoque = cd_local_estoque_w
			and	d.dt_leitura is null
			--and	d.dt_preparo is null
			and	d.dt_cancelamento is null;
			
			qt_material_gedipa_w := dividir(nvl(qt_material_gedipa_w,0), nvl(qt_conv_estoque_w,1));
			
		end if;

		if	(ie_disp_nf_emprestimo_w = 'N') then
		
			select	/*+ index(b empmate_i1) index (c emprest_pk) */ 
				nvl(sum(decode(c.ie_tipo,'S', qt_material * -1, qt_material)),0)
			into	qt_emprestimo_w
			from	emprestimo c,
				emprestimo_material b			
			where	b.nr_emprestimo		= c.nr_emprestimo
			and	c.cd_local_estoque	= cd_local_estoque_w	
			and	b.qt_material		> 0
			and	nvl(b.ie_atualiza_estoque,'X') <> 'S'
			and	c.ie_situacao		<> 'I'
			and	exists (
				select 1 from material a
				where	a.cd_material_estoque	= cd_material_w
				and	a.cd_material 		= b.cd_material);
		else
			select	/*+ index(b empmate_i1) index (c emprest_pk) */ 
				nvl(sum(decode(c.ie_tipo,'S', (qt_material - nvl(b.qt_nota_fiscal,0)) * -1, (qt_material - nvl(b.qt_nota_fiscal,0)))),0)
			into	qt_emprestimo_w
			from	emprestimo c,
				emprestimo_material b			
			where	b.nr_emprestimo		= c.nr_emprestimo
			and	c.cd_local_estoque	= cd_local_estoque_w	
			and	b.qt_material		> 0
			and	nvl(b.ie_atualiza_estoque,'X') <> 'S'
			and	c.ie_situacao		<> 'I'
			and	exists (
				select 1 from material a
				where	a.cd_material_estoque	= cd_material_w
				and	a.cd_material 		= b.cd_material);

		end if;

		if	(ie_disp_emprestimo_lib_w	= 'S') then
			begin
			
			if	(ie_disp_nf_emprestimo_w = 'N') then
				select	/*+ index(b empmate_i1) index (c emprest_pk) */ 
					nvl(sum(decode(c.ie_tipo,'S', qt_material * -1, qt_material)),0)
				into	qt_emprestimo_w
				from	emprestimo c,
					emprestimo_material b			
				where	b.nr_emprestimo		= c.nr_emprestimo
				and	c.cd_local_estoque	= cd_local_estoque_w
				and	c.dt_liberacao		is not null
				and	b.qt_material		> 0
				and	nvl(b.ie_atualiza_estoque,'X') <> 'S'
				and	c.ie_situacao		<> 'I'
				and	exists (
					select 1 from material a
					where	a.cd_material_estoque	= cd_material_w
					and	a.cd_material 		= b.cd_material);
			else
				select	/*+ index(b empmate_i1) index (c emprest_pk) */ 
					nvl(sum(decode(c.ie_tipo,'S', (qt_material - nvl(b.qt_nota_fiscal,0)) * -1, (qt_material - nvl(b.qt_nota_fiscal,0)))),0)
				into	qt_emprestimo_w
				from	emprestimo c,
					emprestimo_material b			
				where	b.nr_emprestimo		= c.nr_emprestimo
				and	c.cd_local_estoque	= cd_local_estoque_w
				and	c.dt_liberacao		is not null
				and	b.qt_material		> 0
				and	nvl(b.ie_atualiza_estoque,'X') <> 'S'
				and	c.ie_situacao		<> 'I'
				and	exists (
					select 1 from material a
					where	a.cd_material_estoque	= cd_material_w
					and	a.cd_material 		= b.cd_material);
			end if;
			end;
		end if;

		if	(ie_disp_quimioterapia_w = 'S') then
			select	nvl(sum(a.qt_dose_real),0)
			into	qt_quimioterapia_w
			from	far_etapa_producao c,
				can_ordem_prod b,
				can_ordem_prod_mat a
			where	a.cd_material in (	select	x.cd_material
						from	material x
						where	x.cd_material_estoque = cd_material_w)
			and	b.nr_sequencia = a.nr_seq_ordem
			and	b.dt_fim_preparo is not null
			and	b.dt_entrega_setor is null
			and	b.dt_devolucao is null
			and	b.ie_cancelada <> 'S'
			and	c.nr_sequencia = b.nr_seq_etapa_prod
			and	c.nr_seq_cabine in (
				select	x.nr_sequencia
				from	far_cabine_seg_biol x
				where	x.cd_local_estoque = cd_local_estoque_w);
			qt_quimioterapia_w	:= dividir(nvl(qt_quimioterapia_w,0) , nvl(qt_conv_estoque_w,1));
		end if;

		if	(ie_disp_req_trans_w = 'S') then
			select	sum(nvl(a.qt_estoque, 0)) - sum(nvl(a.qt_material_atendida, 0)) qt_material
			into	qt_requisicao_w
			from	operacao_estoque o,
				requisicao_material b,
				item_requisicao_material a
			where	a.nr_requisicao		= b.nr_requisicao
			and	b.cd_operacao_estoque	= o.cd_operacao_estoque
			and	b.cd_estabelecimento	= cd_estabelecimento_p
			and	b.cd_local_estoque		= cd_local_estoque_w
			and	o.ie_tipo_requisicao		in ('2','21')
			and	exists ( Select 	d.cd_motivo_baixa 
							 from	sup_motivo_baixa_req d
							 where 	a.cd_motivo_baixa = d.nr_sequencia 
							 and	d.cd_motivo_baixa = 0
							 and 	rownum = 1) 
			and	b.dt_liberacao is not null
			and	a.cd_material in (
				select	x.cd_material
				from	material x
				where	x.cd_material_estoque = cd_material_w);
		end if;
		
		if	(ie_disp_req_trans_estab_w = 'S') then
			select	sum(qt_estoque)
			into	qt_req_transf_estab_w
			from (	select	obter_quantidade_convertida(b.cd_material, SUM(c.qt_prevista_entrega) - max(obter_qt_oci_trans_nota(a.nr_ordem_compra, b.nr_item_oci,'S')), b.cd_unidade_medida_compra, 'UME') qt_estoque
				from	ordem_compra a,
					ordem_compra_item b,
					ordem_compra_item_entrega c
				where	a.nr_ordem_compra = b.nr_ordem_compra
				and	a.nr_ordem_compra = c.nr_ordem_compra
				and	b.nr_item_oci = c.nr_item_oci
				and	c.dt_cancelamento is null
				and	b.dt_reprovacao is null
				and	b.dt_aprovacao is not null
				and	a.dt_baixa is null
				and	a.nr_seq_motivo_cancel is null
				and	a.ie_tipo_ordem = 'T'
				and	(nvl(c.qt_prevista_entrega,0) - nvl(c.qt_real_entrega,0)) > 0
				and	a.cd_local_transf = cd_local_estoque_w
				and	exists (select	1
						from	material x
						where	x.cd_material_estoque = cd_material_w
						and	x.cd_material = b.cd_material)
				group by a.nr_ordem_compra, b.nr_item_oci, b.cd_material, b.cd_unidade_medida_compra);
		end if;

		if	(ie_disp_comp_kit_estoque_w = 'S') then

			select	nvl(sum(a.qt_material), 0)
			into	qt_kit_w
			from	kit_estoque b,
				kit_estoque_comp a
			where	a.nr_seq_kit_estoque	= b.nr_sequencia
			and	b.cd_local_estoque	= cd_local_estoque_w
			and	b.cd_estabelecimento	= cd_estabelecimento_p
			and	(b.nr_seq_solic_kit is null or a.ie_gerado_barras = 'S')
			and	b.dt_utilizacao is null
			and	b.nr_seq_reg_kit is null
			and	a.cd_material in (
				select	x.cd_material
				from	material x
				where	x.cd_material_estoque = cd_material_w);

			qt_kit_w	:= dividir(nvl(qt_kit_w,0) , nvl(qt_conv_estoque_w,1));
		end if;

		if	(ie_disp_reg_kit_estoque_w = 'S') then

			SELECT	NVL(SUM(a.qt_material),0)
			into	qt_kit_ww
			FROM	kit_estoque_reg c,
				kit_estoque b,
				material x,
				kit_estoque_comp a
			WHERE	c.nr_sequencia		= b.nr_seq_reg_kit
			AND	a.nr_seq_kit_estoque	= b.nr_sequencia
			AND	a.cd_material = x.cd_material
			and	x.cd_material_estoque	= cd_material_w
			and	b.cd_local_estoque	= cd_local_estoque_w
			AND	b.cd_estabelecimento	= cd_estabelecimento_p
			AND	a.ie_gerado_barras	= 'S'
			AND	a.nr_seq_motivo_exclusao IS NULL
			AND	b.dt_utilizacao IS NULL
			AND	c.ie_situacao		= 'A'
			AND	c.dt_utilizacao IS NULL;
			
			qt_kit_ww	:= dividir(nvl(qt_kit_ww,0) , nvl(qt_conv_estoque_w,1));

			select	nvl(sum(a.qt_material),0)
			into	qt_kit_avulso_w
			from	kit_estoque_reg b,
				material x,
				kit_estoque_comp_avulso a
			where	b.nr_sequencia	= a.nr_seq_reg_kit
			and	x.cd_material		= a.cd_material
			and	a.cd_local_estoque	= cd_local_estoque_w
			and	b.cd_estabelecimento	= cd_estabelecimento_p
			and	x.cd_material_estoque	= cd_material_w
			and	b.ie_situacao		= 'A'
			and	a.ie_gerado_barras	= 'S'
			and	b.dt_utilizacao is null;
			
			qt_kit_avulso_w	:= dividir(nvl(qt_kit_avulso_w,0) , nvl(qt_conv_estoque_w,1));

		end if;
				
		if	(PKG_DATE_UTILS.start_of(dt_mesano_referencia_p, 'month', 0) = PKG_DATE_UTILS.start_of(sysdate, 'month', 0)) or
			(PKG_DATE_UTILS.start_of(dt_mesano_referencia_p, 'month', 0)	< sysdate - 2000) then
			qt_estoque_w	:=	qt_estoque_w +
						nvl(qt_saldo_w, 0) +
						nvl(qt_emprestimo_w, 0) - 
						nvl(qt_cirurgia_w, 0) -
						nvl(qt_esp_cirurgia_w, 0) -
						nvl(qt_material_gedipa_w, 0) -
						nvl(qt_quimioterapia_w, 0) -
						nvl(qt_kit_w, 0) - 
						nvl(qt_kit_ww, 0) - 
						nvl(qt_kit_avulso_w, 0) - 
						nvl(qt_requisicao_w, 0) - 
						nvl(qt_req_transf_estab_w, 0);
		else
			qt_estoque_w	:=	nvl(qt_saldo_w,0);
		end if;
		end;
	end loop;
	close c01;
	end;
else
	begin
	open  c02;
	loop
	fetch c02 into
		cd_local_estoque_w;
	exit when c02%notfound;
		begin
		qt_emprestimo_w		:= 0;
		qt_cirurgia_w		:= 0;

		qt_saldo_w := obter_saldo_estoque_lote(cd_estabelecimento_p, cd_material_w, cd_local_estoque_w, dt_mesano_referencia_p, nr_seq_lote_w);

		select	ie_tipo_local,
			nvl(ie_baixa_disp, 'N')
		into	ie_tipo_local_w,
			ie_baixa_disp_w
		from 	local_estoque
		where	cd_local_estoque	= cd_local_estoque_w;

		if	(ie_tipo_local_w = 2) or (ie_baixa_disp_w = 'S') then /*Baixa do disponivel se o local for direto ou se o campo IE_DISP_ESTOQUE estiver marcada*/
			begin
			if	(ie_disp_cirurgia_w = 'S') then
				/*Busca as quantidades que estao em cirurgia no local do setor da cirurgia*/
				select	nvl(sum(qt_total_dispensar),0)
				into	qt_cirurgia_w
				from	setor_atendimento c,
					cirurgia b,
					material x,
					prescr_material a,
					prescr_medica m
				where	a.cd_motivo_baixa		= 0
				and	a.cd_material		= x.cd_material
				and	x.cd_material_estoque	= cd_material_w
				and	m.nr_prescricao		= b.nr_prescricao
				and	m.nr_prescricao		= a.nr_prescricao
				and	b.cd_setor_atendimento	= c.cd_setor_atendimento
				and	c.cd_local_estoque	= cd_local_estoque_w
				and	a.ie_status_cirurgia	in ('CB','AD')
				and	a.ie_baixa_estoque_cir	is null
				and	a.nr_seq_lote_fornec 	= nr_seq_lote_w;
			elsif	(ie_disp_cirurgia_w = 'C') then 
				/*Busca as quantidades que estao em cirurgia no local de consistencia, se nao ter local de consistencia, busca do local do setor da cirurgia*/
				select	nvl(sum(qt_total_dispensar),0)
				into	qt_cirurgia_w
				from	setor_atendimento c,
					cirurgia b,
					material x,
					prescr_material a,
					prescr_medica m
				where	a.cd_motivo_baixa		= 0
				and	a.cd_material		= x.cd_material
				and	x.cd_material_estoque	= cd_material_w
				and	m.nr_prescricao		= b.nr_prescricao
				and	m.nr_prescricao		= a.nr_prescricao
				and	b.cd_setor_atendimento	= c.cd_setor_atendimento
				and	nvl(a.cd_local_estoque, c.cd_local_estoque) = cd_local_estoque_w
				and	a.ie_status_cirurgia	in ('CB','AD')
				and	a.ie_baixa_estoque_cir	is null
				and	a.nr_seq_lote_fornec 	= nr_seq_lote_w;
			else
				qt_cirurgia_w := 0;
			end if;
			
			if	(ie_disp_esp_cirurgia_w = 'S') then
				SELECT	/*+ index (d PRESMAT_I3)*/
					NVL(SUM(obter_quantidade_convertida(d.cd_material, d.qt_total_dispensar, d.cd_unidade_medida, 'UME')),0)
				into	qt_esp_cirurgia_w
				FROM	setor_atendimento a,
					cirurgia b,
					prescr_medica c,
					prescr_material d,
					material e
				WHERE	b.nr_cirurgia		= c.nr_cirurgia
				AND	c.nr_prescricao		= d.nr_prescricao 
				AND	a.cd_setor_atendimento	= b.cd_setor_atendimento
				AND	d.cd_material		= e.cd_material		
				AND	a.cd_local_estoque	= cd_local_estoque_w
				AND	e.cd_material_estoque	= cd_material_w
				AND	d.cd_motivo_baixa	= 0
				AND	d.ie_status_cirurgia	IN ('CB','AD')
				AND	d.ie_baixa_estoque_cir	IS NULL
				and	d.nr_seq_lote_fornec 	= nr_seq_lote_w;
			elsif	(ie_disp_esp_cirurgia_w = 'C') then
				SELECT	/*+ index (d PRESMAT_I3)*/
					NVL(SUM(obter_quantidade_convertida(d.cd_material, d.qt_total_dispensar, d.cd_unidade_medida, 'UME')),0)
				into	qt_esp_cirurgia_w
				FROM	setor_atendimento a,
					cirurgia b,
					prescr_medica c,
					prescr_material d,
					material e
				WHERE	b.nr_cirurgia		= c.nr_cirurgia
				AND	c.nr_prescricao		= d.nr_prescricao 
				AND	a.cd_setor_atendimento	= b.cd_setor_atendimento
				AND	d.cd_material		= e.cd_material		
				and	nvl(d.cd_local_estoque, a.cd_local_estoque) = cd_local_estoque_w
				AND	e.cd_material_estoque	= cd_material_w
				AND	d.cd_motivo_baixa	= 0
				AND	d.ie_status_cirurgia	IN ('CB','AD')
				AND	d.ie_baixa_estoque_cir	IS NULL
				and	d.nr_seq_lote_fornec 	= nr_seq_lote_w;
			end if;
			
			/*Busca as quantidades que estao prescricao de emergencia. indice definido, motivo OS276540*/
			if	(ie_disp_prescr_eme_w = 'S') then
				select	/*+ index (a PRESMAT_I3)*/
					nvl(sum(qt_total_dispensar),0)
				into	qt_emergencia_w
				from	setor_atendimento c,
					material x,
					prescr_medica b,
					prescr_material a
				where	a.nr_prescricao		= b.nr_prescricao
				and	a.cd_material           = x.cd_material
				and	b.cd_setor_atendimento  = c.cd_setor_atendimento
				and	b.ie_emergencia		= 'S'			
				and	x.cd_material_estoque   = cd_material_w			
				and	c.cd_local_estoque     	= cd_local_estoque_w
				and	a.cd_motivo_baixa	= 0
				and	a.ie_status_cirurgia	= 'PE'
				and	a.nr_seq_lote_fornec 	= nr_seq_lote_w;
			end if;


			/*Busca as quantidades que estao consistidos pela gestao da agenda cirurgica, e ainda nao tenham cirurgia vinculada*/
			if	(ie_disp_ag_cirur_w = 'S') then
				select	nvl(sum(qt_total_dispensar),0)
				into	qt_agenda_cirurgia_w
				from	setor_atendimento c,
					material x,
					prescr_medica b,
					prescr_material a
				where	a.nr_prescricao		= b.nr_prescricao
				and	b.cd_setor_atendimento	= c.cd_setor_atendimento
				and	a.cd_material		= x.cd_material
				and	c.cd_local_estoque	= cd_local_estoque_w
				and	x.cd_material_estoque	= cd_material_w
				and	a.cd_motivo_baixa	= 0
				and	a.ie_status_cirurgia	= 'CB'
				and	b.nr_seq_agenda		is not null
				and	b.nr_cirurgia		is null
				and	a.nr_seq_lote_fornec 	= nr_seq_lote_w
				and not exists(
					select	1
					from	cirurgia c
					where	c.nr_prescricao = a.nr_prescricao);
			end if;
			
			qt_cirurgia_w := dividir((nvl(qt_cirurgia_w,0) + nvl(qt_emergencia_w,0) + nvl(qt_agenda_cirurgia_w, 0)) , nvl(qt_conv_estoque_w,1));
			end;
		end if;
		
		/*Busca as quantidades que foram separadas no GEDIPA, porem que serao cobradas apenas na checagem do ADEP*/
		if	(ie_disp_sep_gedipa_w = 'S') then
		
			select	nvl(sum(c.qt_dispensar_hor),0)
			into	qt_material_gedipa_w
			from	adep_processo_item a,
				prescr_material b,
				prescr_mat_hor c,
				adep_processo d
			where	a.nr_seq_horario = c.nr_sequencia
			and	c.nr_prescricao = b.nr_prescricao
			and	c.nr_seq_material = b.nr_sequencia
			and	a.nr_seq_processo = d.nr_sequencia
			and	nvl(a.ie_nao_requisitado,'N') <> 'S'
			and	a.ie_lanca_conta_adep = 'S'
			and	b.cd_motivo_baixa = 0
			and	b.cd_material = cd_material_w
			and	b.nr_seq_lote_fornec = nr_seq_lote_w
			and	b.cd_local_estoque = cd_local_estoque_w
			and	d.dt_leitura is null
			--and	d.dt_preparo is null
			and	d.dt_cancelamento is null;
			
			qt_material_gedipa_w := dividir(nvl(qt_material_gedipa_w,0), nvl(qt_conv_estoque_w,1));
			
		end if;

		if	(ie_disp_quimioterapia_w = 'S') then
			select	nvl(sum(a.qt_dose_real),0)
			into	qt_quimioterapia_w
			from	far_etapa_producao c,
				can_ordem_prod b,
				can_ordem_prod_mat a
			where	a.cd_material in (	select	x.cd_material
						from	material x
						where	x.cd_material_estoque = cd_material_w)
			and	b.nr_sequencia = a.nr_seq_ordem
			and	b.dt_fim_preparo is not null
			and	b.dt_entrega_setor is null
			and	b.dt_devolucao is null
			and	b.ie_cancelada <> 'S'
			and	c.nr_sequencia = b.nr_seq_etapa_prod
			and	a.nr_seq_lote_fornec = nr_seq_lote_w
			and	c.nr_seq_cabine in (
				select	x.nr_sequencia
				from	far_cabine_seg_biol x
				where	x.cd_local_estoque = cd_local_estoque_w);
			qt_quimioterapia_w	:= dividir(nvl(qt_quimioterapia_w,0) , nvl(qt_conv_estoque_w,1));
		end if;

		if	(ie_disp_req_trans_w = 'S') then
			select	sum(nvl(a.qt_estoque, 0)) - sum(nvl(a.qt_material_atendida, 0)) qt_material
			into	qt_requisicao_w
			from	operacao_estoque o,
				requisicao_material b,
				item_requisicao_material a
			where	a.nr_requisicao		= b.nr_requisicao
			and	b.cd_operacao_estoque	= o.cd_operacao_estoque
			and	b.cd_estabelecimento	= cd_estabelecimento_p
			and	b.cd_local_estoque		= cd_local_estoque_w
			and	o.ie_tipo_requisicao		in ('2','21')
			and	exists ( Select 	d.cd_motivo_baixa 
							 from	sup_motivo_baixa_req d
							 where 	a.cd_motivo_baixa = d.nr_sequencia 
							 and	d.cd_motivo_baixa = 0
							 and 	rownum = 1)
			and	b.dt_liberacao is not null
			and	a.nr_seq_lote_fornec 	= nr_seq_lote_w
			and	a.cd_material in (
				select	x.cd_material
				from	material x
				where	x.cd_material_estoque = cd_material_w);
		end if;
		
		if	(ie_disp_req_trans_estab_w = 'S') then
			select	sum(qt_estoque)
			into	qt_req_transf_estab_w
			from (	select	obter_quantidade_convertida(b.cd_material, SUM(c.qt_prevista_entrega) - max(obter_qt_oci_trans_nota(a.nr_ordem_compra, b.nr_item_oci,'S')), b.cd_unidade_medida_compra, 'UME') qt_estoque
				from	ordem_compra a,
					ordem_compra_item b,
					ordem_compra_item_entrega c
				where	a.nr_ordem_compra = b.nr_ordem_compra
				and	a.nr_ordem_compra = c.nr_ordem_compra
				and	b.nr_item_oci = c.nr_item_oci
				and	c.dt_cancelamento is null
				and	b.dt_reprovacao is null
				and	b.dt_aprovacao is not null
				and	a.dt_baixa is null
				and	a.nr_seq_motivo_cancel is null
				and	a.ie_tipo_ordem = 'T'
				and	(nvl(c.qt_prevista_entrega,0) - nvl(c.qt_real_entrega,0)) > 0
				and	a.cd_local_transf = cd_local_estoque_w
				and	b.nr_seq_lote_fornec = nr_seq_lote_w
				and	exists (select	1
						from	material x
						where	x.cd_material_estoque = cd_material_w
						and	x.cd_material = b.cd_material)
				group by a.nr_ordem_compra, b.nr_item_oci, b.cd_material, b.cd_unidade_medida_compra);
		end if;

		if	(ie_disp_comp_kit_estoque_w = 'S') then

			select	nvl(sum(a.qt_material), 0)
			into	qt_kit_w
			from	kit_estoque b,
				kit_estoque_comp a
			where	a.nr_seq_kit_estoque	= b.nr_sequencia
			and	b.cd_local_estoque	= cd_local_estoque_w
			and	b.cd_estabelecimento	= cd_estabelecimento_p
			and	b.dt_utilizacao is null
			and	b.nr_seq_reg_kit is null
			and	a.nr_seq_lote_fornec = nr_seq_lote_w
			and	a.cd_material in (
				select	x.cd_material
				from	material x
				where	x.cd_material_estoque = cd_material_w);

			qt_kit_w	:= dividir(nvl(qt_kit_w,0) , nvl(qt_conv_estoque_w,1));
		end if;

		if	(ie_disp_reg_kit_estoque_w = 'S') then

			SELECT	NVL(SUM(a.qt_material),0)
			into	qt_kit_ww
			FROM	kit_estoque_reg c,
				kit_estoque b,
				material x,
				kit_estoque_comp a
			WHERE	c.nr_sequencia		= b.nr_seq_reg_kit
			AND	a.nr_seq_kit_estoque	= b.nr_sequencia
			AND	a.cd_material = x.cd_material
			and	x.cd_material_estoque	= cd_material_w
			and	b.cd_local_estoque	= cd_local_estoque_w
			AND	b.cd_estabelecimento	= cd_estabelecimento_p
			AND	a.ie_gerado_barras	= 'S'
			AND	a.nr_seq_motivo_exclusao IS NULL
			AND	b.dt_utilizacao IS NULL
			AND	c.ie_situacao		= 'A'
			AND	c.dt_utilizacao IS NULL
			and	a.nr_seq_lote_fornec = nr_seq_lote_w;
			
			qt_kit_ww	:= dividir(nvl(qt_kit_ww,0) , nvl(qt_conv_estoque_w,1));

			select	nvl(sum(a.qt_material),0)
			into	qt_kit_avulso_w
			from	kit_estoque_reg b,
				material x,
				kit_estoque_comp_avulso a
			where	b.nr_sequencia	= a.nr_seq_reg_kit
			and	x.cd_material		= a.cd_material
			and	a.cd_local_estoque	= cd_local_estoque_w
			and	b.cd_estabelecimento	= cd_estabelecimento_p
			and	x.cd_material_estoque	= cd_material_w
			and	b.ie_situacao		= 'A'
			and	a.ie_gerado_barras	= 'S'
			and	b.dt_utilizacao is null
			and	a.nr_seq_lote_fornec = nr_seq_lote_w;
			
			qt_kit_avulso_w	:= dividir(nvl(qt_kit_avulso_w,0) , nvl(qt_conv_estoque_w,1));

		end if;

		if	(PKG_DATE_UTILS.start_of(dt_mesano_referencia_p, 'month', 0) = PKG_DATE_UTILS.start_of(sysdate, 'month', 0)) or
			(PKG_DATE_UTILS.start_of(dt_mesano_referencia_p, 'month', 0)	< sysdate - 2000) then
			qt_estoque_w	:=	qt_estoque_w +
						nvl(qt_saldo_w, 0) +
						nvl(qt_emprestimo_w, 0) - 
						nvl(qt_cirurgia_w, 0) -
						nvl(qt_esp_cirurgia_w, 0) -
						nvl(qt_material_gedipa_w, 0) -
						nvl(qt_quimioterapia_w, 0) - 
						nvl(qt_kit_w, 0) - 
						nvl(qt_kit_ww, 0) - 
						nvl(qt_kit_avulso_w, 0) - 
						nvl(qt_requisicao_w, 0) - 
						nvl(qt_req_transf_estab_w, 0);
		else
			qt_estoque_w	:=	nvl(qt_saldo_w,0);
		end if;
		end;
	end loop;
	close c02;
	end;
end if;

return qt_estoque_w;

end obter_saldo_estoque_consist;
/
