create or replace
function obter_desc_que_questao(nr_sequencia_p		number)
 		    	return varchar2 is

ds_questao_w	que_questao.ds_questao%type;
begin

select	max(ds_questao)
into	ds_questao_w
from	que_questao
where	nr_sequencia = nr_sequencia_p;

return	ds_questao_w;

end obter_desc_que_questao;
/