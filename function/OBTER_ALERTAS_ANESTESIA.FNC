create or replace
function obter_alertas_anestesia	(	cd_pessoa_fisica_p	in	varchar2)
										return varchar2 is

ds_return_w			varchar2(4000 char) := null;
ie_first_value		varchar2(1 char) := 1;

cursor c01 is
select 		decode(	a.ie_tipo_alerta, 0,
				(select substr(max(b.ds_alerta),1,100) from alerta_anestesia b where b.nr_sequencia = a.nr_seq_alerta),
				(select substr(max(c.ds_diagnostico),1,100) from apa_diagnostico c where c.nr_sequencia = a.nr_seq_diag)) ds_alerta
from 		ALERTA_ANESTESIA_APAE a
where 		nvl(a.ie_situacao, 'A') = 'A'
and 		((a.dt_fim_alerta is null) or (sysdate between nvl(a.dt_alerta,sysdate-1) and a.dt_fim_alerta))
and 		a.dt_inativacao is null
and 		a.dt_liberacao is not null
and 		a.cd_pessoa_fisica = cd_pessoa_fisica_p
order by 	ds_alerta asc;

begin

if (cd_pessoa_fisica_p is not null) then

	for r_c01 in c01 loop
		if (r_c01.ds_alerta is not null) then
			if (ie_first_value = 1) then
				ds_return_w := ds_return_w || r_c01.ds_alerta;
				ie_first_value := 0;
			else
				ds_return_w := ds_return_w || ',' || r_c01.ds_alerta;
			end if;
		end if;
	end loop;
end if;

return ds_return_w;

end obter_alertas_anestesia;
/
