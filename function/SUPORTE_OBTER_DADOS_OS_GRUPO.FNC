create or replace
function suporte_obter_dados_OS_grupo( nr_seq_grupo_sup_p		number,
								dt_parametro_inicial_p	date,
								dt_parametro_final_p	date,
								ie_opcao_p				number)
 		    	return number is

/*
IE_OPCAO_P:
1 - N�mero total de OS de D�vida por Grupo no per�odo
2 - Percent OS's total de d�vida grupo X OS's  passadas para o N�vel 2
3 - Qtde OS passada para o N�vel 2
*/				
				
qt_retorno_w					number(14,2);
qt_total_os_grupo_w				number(14,2);
qt_total_os_grupo_passada_w		number(14,2);
pr_os_passada_w					number(14,2);
				
begin

qt_retorno_w := 0;

if 	(nr_seq_grupo_sup_p is not null) and
	(dt_parametro_inicial_p is not null) and
	(dt_parametro_final_p is not null) then
	
	if 	(ie_opcao_p = 1) then

		SELECT 	COUNT(*) 
		INTO	qt_total_os_grupo_w
		FROM (
			SELECT	a.nr_sequencia,
				a.dt_ordem_servico,
				a.dt_fim_real,
				a.nr_seq_estagio,
				a.ie_classificacao
			FROM	Man_ordem_servico a
			WHERE	1 = 1
			AND	((nr_seq_grupo_sup_p IS NULL) OR (nr_seq_grupo_sup_p = a.nr_seq_grupo_sup))
			AND	a.nr_sequencia IN (
				SELECT  x.nr_sequencia
				FROM	man_ordem_servico x
				WHERE	x.ie_status_ordem	= 3
				AND	x.dt_fim_real 	>= TRUNC(dt_parametro_inicial_p,'dd')
				AND	x.dt_ordem_servico 	<= TRUNC(dt_parametro_final_p,'dd')
				UNION
				SELECT  x.nr_sequencia
				FROM	man_ordem_servico x
				WHERE	x.ie_status_ordem	<> 3
				AND	x.dt_ordem_servico 	<= TRUNC(dt_parametro_final_p,'dd'))
			AND EXISTS(	SELECT	1
								FROM	man_estagio_processo y,
									man_ordem_serv_estagio x
								WHERE	y.nr_sequencia = x.nr_seq_estagio
								AND	x.nr_seq_ordem = a.nr_sequencia
								AND	y.ie_suporte = 'S')
			AND	a.dt_ordem_servico BETWEEN TRUNC(dt_parametro_inicial_p,'dd') AND fim_dia(TRUNC(dt_parametro_final_p,'dd'))
			and	a.ie_classificacao = 'D');

		if (qt_total_os_grupo_w > 0) then
			qt_retorno_w := qt_total_os_grupo_w;
		else
			qt_retorno_w := 0;
		end if;
		
	elsif (ie_opcao_p = 2) then
	
		SELECT 	COUNT(*) 
		INTO	qt_total_os_grupo_w
		FROM (
			SELECT	a.nr_sequencia,
				a.dt_ordem_servico,
				a.dt_fim_real,
				a.nr_seq_estagio,
				a.ie_classificacao
			FROM	Man_ordem_servico a
			WHERE	1 = 1
			AND	((nr_seq_grupo_sup_p IS NULL) OR (nr_seq_grupo_sup_p = a.nr_seq_grupo_sup))
			AND	a.nr_sequencia IN (
				SELECT  x.nr_sequencia
				FROM	man_ordem_servico x
				WHERE	x.ie_status_ordem	= 3
				AND	x.dt_fim_real 	>= TRUNC(dt_parametro_inicial_p,'dd')
				AND	x.dt_ordem_servico 	<= TRUNC(dt_parametro_final_p,'dd')
				UNION
				SELECT  x.nr_sequencia
				FROM	man_ordem_servico x
				WHERE	x.ie_status_ordem	<> 3
				AND	x.dt_ordem_servico 	<= TRUNC(dt_parametro_final_p,'dd'))
			AND EXISTS(	SELECT	1
								FROM	man_estagio_processo y,
									man_ordem_serv_estagio x
								WHERE	y.nr_sequencia = x.nr_seq_estagio
								AND	x.nr_seq_ordem = a.nr_sequencia
								AND	y.ie_suporte = 'S')
			AND	a.dt_ordem_servico BETWEEN TRUNC(dt_parametro_inicial_p,'dd') AND fim_dia(TRUNC(dt_parametro_final_p,'dd'))
			and	a.ie_classificacao = 'D');
	
		SELECT 	COUNT(*) 
		INTO	qt_total_os_grupo_passada_w
		FROM (				
		SELECT  DISTINCT 
				a.nr_sequencia
		FROM    man_ordem_servico a
		WHERE	1=1
		AND a.nr_seq_grupo_sup IS NOT NULL
		/*AND 	EXISTS (  SELECT  1                
					  FROM    man_estagio_processo x                
					  WHERE   a.nr_seq_estagio  = x.nr_sequencia               
					  AND     x.ie_acao           = 1)*/ 
		AND a.ie_status_ordem IN ('1','2') 
		AND 'S' = (SELECT	DECODE(COUNT(*),0,'N','S')
				   FROM		man_ordem_serv_estagio y
				   WHERE	verifica_se_estagio_nv2(y.nr_seq_estagio) = 'S'
				   AND		y.nr_seq_ordem = a.nr_sequencia
				   AND 		suporte_obter_data_minima(a.nr_sequencia,1,NULL) BETWEEN TRUNC(dt_parametro_inicial_p) AND fim_dia(TRUNC(dt_parametro_final_p)))
		AND	a.nr_seq_grupo_sup = nr_seq_grupo_sup_p
		AND	a.ie_classificacao = 'D');
		
		select	dividir(qt_total_os_grupo_passada_w * 100, qt_total_os_grupo_w)
		into 	pr_os_passada_w
		from 	dual;
		
		if 	(pr_os_passada_w > 0) then
			qt_retorno_w := pr_os_passada_w;
		else
			qt_retorno_w := 0;
		end if;
		
	elsif (ie_opcao_p = 3) then
	
		SELECT 	COUNT(*) 
		INTO	qt_total_os_grupo_passada_w
		FROM (				
		SELECT  DISTINCT 
				a.nr_sequencia
		FROM    man_ordem_servico a
		WHERE	1=1
		AND a.nr_seq_grupo_sup IS NOT NULL
		/*AND 	EXISTS (  SELECT  1                
					  FROM    man_estagio_processo x                
					  WHERE   a.nr_seq_estagio  = x.nr_sequencia               
					  AND     x.ie_acao           = 1)*/ 
		AND a.ie_status_ordem IN ('1','2') 
		AND 'S' = (SELECT	DECODE(COUNT(*),0,'N','S')
				   FROM		man_ordem_serv_estagio y
				   WHERE	verifica_se_estagio_nv2(y.nr_seq_estagio) = 'S'
				   AND		y.nr_seq_ordem = a.nr_sequencia
				   AND 		suporte_obter_data_minima(a.nr_sequencia,1,NULL) BETWEEN TRUNC(dt_parametro_inicial_p) AND fim_dia(TRUNC(dt_parametro_final_p)))
		AND	a.nr_seq_grupo_sup = nr_seq_grupo_sup_p
		AND	a.ie_classificacao = 'D');
	
		if 	(qt_total_os_grupo_passada_w > 0) then
			qt_retorno_w := qt_total_os_grupo_passada_w;
		else
			qt_retorno_w := 0;
		end if;
	
	end if;

end if;
	
return	qt_retorno_w;

end suporte_obter_dados_OS_grupo;
/