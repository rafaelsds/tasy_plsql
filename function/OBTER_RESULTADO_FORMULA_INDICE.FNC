create or replace function obter_resultado_formula_indice
		(ds_indice_p varchar,
		dt_vencimento_parcela_p date) return number is

nr_seq_reajuste_w number;
valor_indice_w number;

begin 
  
  select max(nr_sequencia) into nr_seq_reajuste_w from tipo_indice_reajuste_fin where ds_indice = ds_indice_p;
  valor_indice_w := obter_taxa_ajuste(nr_seq_reajuste_w, dt_vencimento_parcela_p);
    
  return valor_indice_w;

end obter_resultado_formula_indice;
/