create or replace
function EIS_Obter_Causa_Indicador(
			nr_seq_indicador_p		Number,
			nr_seq_causa_p		Number,
			dt_inicial_p			date)
			return	Number is

qt_causa_w		Number(15,0);
nr_seq_ind_dsb_w indicador_gestao.nr_sequencia%type;
ie_ativo_w constant varchar2(2) := 'A';
nr_dias_w constant number(10) := 180;
begin

select  max(g.nr_sequencia) 
into    nr_seq_ind_dsb_w 
from    ind_base b
inner join indicador_gestao g 
	on g.nr_seq_wheb = b.nr_seq_ind_gestao
where   b.nr_seq_ind_gestao = nr_seq_indicador_p
and     b.ie_situacao = ie_ativo_w;

select count(*)
into   qt_causa_w
from	qua_nao_conformidade a
inner join qua_nao_conform_causa b 
	on b.nr_seq_nao_conform	= a.nr_sequencia
where	 (a.nr_seq_indicador	= nr_seq_ind_dsb_w
or     a.nr_seq_indicador = nr_seq_indicador_p)
and	   b.nr_seq_tipo_causa	= nr_seq_causa_p
and	   a.dt_abertura		>= nvl(dt_inicial_p, sysdate - nr_dias_w);

return qt_causa_w;

end EIS_Obter_Causa_Indicador;
/
