create or replace
function sinan_inf_tipo_doenca(nr_seq_doenca_compultoria_p		number)
 		    	return varchar2 is
ds_retorno_w	varchar2(2);
begin

if (nr_seq_doenca_compultoria_p is not null) then
	select	max(ie_tipo_doenca) 
	into	ds_retorno_w
	from	CIH_DOENCA_COMPULSORIA x 
	where	x.nr_sequencia = nr_seq_doenca_compultoria_p;
end if;	

return	ds_retorno_w;

end sinan_inf_tipo_doenca;
/