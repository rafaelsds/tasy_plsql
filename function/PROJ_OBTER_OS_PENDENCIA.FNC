create or replace
function proj_obter_os_pendencia(	nr_seq_pendencia_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
nr_seq_ordem_w		number(10);

Cursor C01 is
	select	nr_seq_ordem
	from	proj_ata_pendencia_os
	where	nr_seq_pendencia = nr_seq_pendencia_p
	order by nr_seq_ordem;

begin

open C01;
loop
fetch C01 into	
	nr_seq_ordem_w;
exit when C01%notfound;
	begin
	ds_retorno_w	:= ds_retorno_w || to_char(nr_seq_ordem_w) || ', ';
	end;
end loop;
close C01;

return	substr(ds_retorno_w,1,length(ds_retorno_w) - 2);

end proj_obter_os_pendencia;
/