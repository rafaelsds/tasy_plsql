create or replace
function obter_esas( 	dt_inicio_p		date,
			dt_final_p		date,
			cd_setor_p		Number,
			ie_sexo_p		Varchar2,
			ie_faixa_p		Varchar2,
			ie_tipo_atendimento_p	Number,
			ds_item_p		Varchar2,
			ds_filtro_p		Varchar2 default null)
							return Number is

nr_atendimento_w		Number(10);
nr_seq_inicial_w		Number(10);
nr_seq_ultimo_w		Number(10);	
qt_avaliacao_inicial_w	Number(10,3);
qt_avaliacao_ultima_w	Number(10,3);
qt_total_inicial_w	Number(10,3)	:= 0;
qt_total_ultimo_w	Number(10,3)	:= 0;
qt_total_w		Number(10,3)	:= 0;
qt_dor_w		Number(10,3);
qt_cansaco_w		Number(10,3);
qt_nausea_w		Number(10,3);
qt_depressao_w		Number(10,3);
qt_ansiedade_w		Number(10,3);		
qt_sonolencia_w		Number(10,3);
qt_bom_apetite_w	Number(10,3);
qt_sensacao_bem_estar_w	Number(10,3);
qt_falta_ar_w		Number(10,3);
qt_somatoria_inicial_w	Number(10,3);
qt_somatoria_final_w	Number(10,3);

Cursor C01 is
	select	distinct(a.nr_atendimento)
	from	escala_edmonton a,
		resumo_atendimento_paciente_v b,
		pessoa_fisica c
	where	a.nr_atendimento = b.nr_atendimento
	and	b.cd_pessoa_fisica = c.cd_pessoa_fisica
	and	a.dt_inativacao is null
	and	trunc(a.dt_avaliacao) between trunc(dt_inicio_p) and trunc(nvl(dt_final_p,dt_inicio_p))
	and	b.cd_setor_atendimento = nvl(cd_setor_p,b.cd_setor_atendimento)
	and	c.ie_sexo = nvl(ie_sexo_p,c.ie_sexo)
	and	obter_idade(c.dt_nascimento,nvl(c.dt_obito,sysdate),'E') = nvl(ie_faixa_p,obter_idade(c.dt_nascimento,nvl(c.dt_obito,sysdate),'E'))
	and	b.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,b.ie_tipo_atendimento)
	and	((	select	count(*)
			from	escala_edmonton x,
				resumo_atendimento_paciente_v y,
				pessoa_fisica z
			where	x.nr_atendimento = y.nr_atendimento
			and	y.cd_pessoa_fisica = z.cd_pessoa_fisica
			and	x.nr_atendimento = a.nr_atendimento
			and	trunc(x.dt_avaliacao) between trunc(dt_inicio_p) and trunc(nvl(dt_final_p,dt_inicio_p))
			and	y.cd_setor_atendimento = nvl(cd_setor_p,y.cd_setor_atendimento)
			and	c.ie_sexo = nvl(ie_sexo_p,c.ie_sexo)
			and	obter_idade(z.dt_nascimento,nvl(z.dt_obito,sysdate),'E') = nvl(ie_faixa_p,obter_idade(z.dt_nascimento,nvl(z.dt_obito,sysdate),'E'))
			and	y.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,y.ie_tipo_atendimento)
			and	x.dt_inativacao is null) >= 2)
	order by a.nr_atendimento;
	
begin

open C01;
loop
fetch C01 into	
	nr_atendimento_w;
exit when C01%notfound;
	begin
	
	select	min(nr_sequencia),
		max(nr_sequencia)
	into	nr_seq_inicial_w,
		nr_seq_ultimo_w
	from	escala_edmonton a,
		resumo_atendimento_paciente_v b,
		pessoa_fisica c
	where	a.nr_atendimento = b.nr_atendimento
	and	b.cd_pessoa_fisica = c.cd_pessoa_fisica
	and	a.dt_inativacao is null
	and	trunc(a.dt_avaliacao) between trunc(dt_inicio_p) and trunc(nvl(dt_final_p,dt_inicio_p))
	and	b.cd_setor_atendimento = nvl(cd_setor_p,b.cd_setor_atendimento)
	and	c.ie_sexo = nvl(ie_sexo_p,c.ie_sexo)
	and	obter_idade(c.dt_nascimento,nvl(c.dt_obito,sysdate),'E') = nvl(ie_faixa_p,obter_idade(c.dt_nascimento,nvl(c.dt_obito,sysdate),'E'))
	and	b.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,b.ie_tipo_atendimento)
	and	a.nr_atendimento = nr_atendimento_w;
	
	if	(ds_item_p is not null) then
	
		if	upper(ds_item_p) = obter_desc_expressao(303807) then
		
			select	nvl(a.ie_dor,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_dor,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;

		elsif upper(ds_item_p) = 'CANSA�O' then
		
			select	nvl(a.ie_cansaco,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_cansaco,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;
			
		elsif upper(ds_item_p) = 'N�USEA' then
		
			select	nvl(a.ie_nausea,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_nausea,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;
			
		elsif upper(ds_item_p) = 'DEPRESS�O' then
		
			select	nvl(a.ie_depressao,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_depressao,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;
			
		elsif upper(ds_item_p) = 'ANSIEDADE' then
		
			select	nvl(a.ie_ansiedade,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_ansiedade,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;
			
		elsif upper(ds_item_p) = 'SONOL�NCIA' then
		
			select	nvl(a.ie_sonolencia,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_sonolencia,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;
			
		elsif upper(ds_item_p) = 'INAPET�NCIA' then
		
			select	nvl(a.ie_bom_apetite,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_bom_apetite,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;
			
		elsif upper(ds_item_p) = 'MAL-ESTAR' then
		
			select	nvl(a.ie_sensacao_bem_estar,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_sensacao_bem_estar,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;
			
		elsif upper(ds_item_p) = 'FALTA DE AR' then
		
			select	nvl(a.ie_falta_ar,0)
			into	qt_avaliacao_inicial_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_inicial_w;

			select	nvl(a.ie_falta_ar,0)
			into	qt_avaliacao_ultima_w	
			from	escala_edmonton a,
				atendimento_paciente b
			where	a.nr_atendimento = b.nr_atendimento	
			and	a.nr_sequencia = nr_seq_ultimo_w;
			
			
		end if;
		
		qt_total_inicial_w 	:= qt_total_inicial_w + qt_avaliacao_inicial_w;
		qt_total_ultimo_w 	:= qt_total_ultimo_w + qt_avaliacao_ultima_w;
	
	else
	
		select	nvl(a.ie_dor,0),
			nvl(a.ie_cansaco,0),
			nvl(a.ie_nausea,0),
			nvl(a.ie_depressao,0),
			nvl(a.ie_ansiedade,0),
			nvl(a.ie_sonolencia,0),
			nvl(a.ie_bom_apetite,0),
			nvl(a.ie_sensacao_bem_estar,0),
			nvl(a.ie_falta_ar,0)
		into	qt_dor_w,
			qt_cansaco_w,
			qt_nausea_w,
			qt_depressao_w,
			qt_ansiedade_w,		
			qt_sonolencia_w,
			qt_bom_apetite_w,
			qt_sensacao_bem_estar_w,
			qt_falta_ar_w
		from	escala_edmonton a,
			atendimento_paciente b
		where	a.nr_atendimento = b.nr_atendimento	
		and	a.nr_sequencia = nr_seq_inicial_w;
		
		qt_somatoria_inicial_w := 	dividir((qt_dor_w + qt_cansaco_w + qt_nausea_w + qt_depressao_w + 
						qt_ansiedade_w + qt_sonolencia_w + qt_bom_apetite_w +
						qt_sensacao_bem_estar_w + qt_falta_ar_w),9);
						
		select	nvl(a.ie_dor,0),
			nvl(a.ie_cansaco,0),
			nvl(a.ie_nausea,0),
			nvl(a.ie_depressao,0),
			nvl(a.ie_ansiedade,0),
			nvl(a.ie_sonolencia,0),
			nvl(a.ie_bom_apetite,0),
			nvl(a.ie_sensacao_bem_estar,0),
			nvl(a.ie_falta_ar,0)
		into	qt_dor_w,
			qt_cansaco_w,
			qt_nausea_w,
			qt_depressao_w,
			qt_ansiedade_w,		
			qt_sonolencia_w,
			qt_bom_apetite_w,
			qt_sensacao_bem_estar_w,
			qt_falta_ar_w
		from	escala_edmonton a,
			atendimento_paciente b
		where	a.nr_atendimento = b.nr_atendimento	
		and	a.nr_sequencia = nr_seq_ultimo_w;
		
		qt_somatoria_final_w := 	dividir((qt_dor_w + qt_cansaco_w + qt_nausea_w + qt_depressao_w + 
						qt_ansiedade_w + qt_sonolencia_w + qt_bom_apetite_w +
						qt_sensacao_bem_estar_w + qt_falta_ar_w),9);
		
		qt_total_inicial_w 	:= qt_total_inicial_w + qt_somatoria_inicial_w;
		qt_total_ultimo_w 	:= qt_total_ultimo_w + qt_somatoria_final_w;
		
		
	end if;
	
	end;
end loop;
close C01;

qt_total_w	:= dividir((qt_total_inicial_w - qt_total_ultimo_w),10);	

return	qt_total_w;

end obter_esas;
/