create or replace
function pls_obter_analise_grupo_atual
			(	nr_seq_analise_p	number,
				nm_usuario_p		varchar2)
 		    	return number is

nr_fatura_ptu_w			ptu_fatura.nr_fatura%type;
ie_pre_analise_w		varchar2(1);
nr_seq_grupo_atual_w		number(10);
nr_seq_conta_w			number(10);

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin
select	pls_obter_conta_principal(a.cd_guia, a.nr_sequencia, a.nr_seq_segurado, a.nr_seq_prestador),
	nvl(ie_pre_analise,'N')
into	nr_seq_conta_w,
	ie_pre_analise_w
from	pls_analise_conta	a
where	a.nr_sequencia	= nr_seq_analise_p;

select	substr(pls_obter_dados_ptu_fatura(a.nr_seq_fatura,'F'),1,255)
into	nr_fatura_ptu_w
from	pls_conta	a
where	a.nr_sequencia	= nr_seq_conta_w;

if	(ie_pre_analise_w = 'S') and
	(nr_fatura_ptu_w is not null) then
	select	nvl(max(a.nr_seq_grupo),0)
	into	nr_seq_grupo_atual_w
	from	pls_auditoria_conta_grupo a
	where	a.nr_seq_analise	= nr_seq_analise_p
	and	((
		--pls_obter_se_auditor_grupo(a.nr_seq_grupo, nm_usuario_p) ='S' 
		(exists	(select	1
				from	pls_membro_grupo_aud
				where	nr_seq_grupo	= a.nr_seq_grupo
				and	nm_usuario_exec	= nm_usuario_p
				and	ie_situacao	= 'A')) or
		(a.nm_auditor_atual  = nm_usuario_p)) and
		nvl(a.ie_pre_analise, 'N') = 'S') and
		a.dt_liberacao is null and
		a.nr_seq_ordem	=	(select	min(x.nr_seq_ordem)
					from	pls_auditoria_conta_grupo x
					where	x.nr_seq_analise         = nr_seq_analise_p
					and	nvl(x.ie_pre_analise, 'N') = 'S'
					and	x.dt_liberacao is null);
else
	select	nvl(max(a.nr_seq_grupo),0)
	into	nr_seq_grupo_atual_w
	from	pls_auditoria_conta_grupo a
	where	a.nr_seq_analise	= nr_seq_analise_p
	--and	((pls_obter_se_auditor_grupo(a.nr_seq_grupo, nm_usuario_p) = 'S') or
	and	((exists	(select	1
				from	pls_membro_grupo_aud
				where	nr_seq_grupo	= a.nr_seq_grupo
				and	nm_usuario_exec	= nm_usuario_p
				and	ie_situacao	= 'A')) or
		(a.nm_auditor_atual = nm_usuario_p))
	and	nvl(a.ie_pre_analise, 'N')	= 'N'
	and	a.dt_liberacao is null
	and	((not exists	(select	x.nr_sequencia
				from	pls_auditoria_conta_grupo x
				where	x.nr_seq_analise         = nr_seq_analise_p
				and	nvl(x.ie_pre_analise, 'N') = 'N'
				and	x.nm_auditor_atual is not null
				and	x.dt_liberacao is null
				and	x.nr_seq_ordem	= a.nr_seq_ordem)) or
					(a.nm_auditor_atual	= nm_usuario_p))
	and	a.nr_seq_ordem	=	(select	min(x.nr_seq_ordem)
					from	pls_auditoria_conta_grupo	x
					where	x.nr_seq_analise         = nr_seq_analise_p
					and	nvl(x.ie_pre_analise, 'N') = 'N'
					and	x.dt_liberacao is null);
end if;

return	nr_seq_grupo_atual_w;

end pls_obter_analise_grupo_atual;
/
