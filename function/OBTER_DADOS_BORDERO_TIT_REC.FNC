create or replace function obter_dados_bordero_tit_rec(
		nr_bordero_p	number,
		nr_titulo_p	number,
		ie_opcao_p	varchar2)
		return number is
		
/*'
retorno:
VC:	valor calculado
'*/

vl_calculado_w	        number(15,2);
ds_retorno_w	        varchar2(255);
ie_acresc_bordero_w		parametro_contas_receber.ie_acresc_bordero%type;
cd_estabelecimento_w    number(5);


begin

	begin

	cd_estabelecimento_w := obter_estabelecimento_ativo();

	select	nvl(max(a.ie_acresc_bordero),'S')
	into	ie_acresc_bordero_w
	from	parametro_contas_receber a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;

	select	nvl(sum(a.vl_abaixar),0) + decode(ie_acresc_bordero_w,'N',0,nvl(sum(a.vl_juros),0) + nvl(sum(a.vl_multa),0) + nvl(sum(a.vl_rec_maior),0))
	into	vl_calculado_w
	from	bordero_tit_rec a
	where	a.nr_bordero	= nr_bordero_p
	and	    a.nr_titulo	= nr_titulo_p;

	exception
	when others then
		ds_retorno_w := null;
	end;

if	(ie_opcao_p = 'VC') then
	ds_retorno_w := vl_calculado_w;
end if;

return	ds_retorno_w;

end	obter_dados_bordero_tit_rec;
/