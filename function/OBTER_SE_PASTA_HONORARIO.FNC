create or replace function 
Obter_se_pasta_honorario(
			cd_regra_honorario_p	Varchar2)
			return Varchar2 is


ie_pasta_honorario_w	Varchar2(1)		:= 'N';

begin

select nvl(max(ie_pasta_honorario),'N')
into	ie_pasta_honorario_w
from	Regra_honorario
where	cd_regra	= nvl(cd_regra_honorario_p,'99999');

return ie_pasta_honorario_w;

end Obter_se_pasta_honorario;
/


