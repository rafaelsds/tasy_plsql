create or replace
function obter_tipo_cobranca_hotel (nr_sequencia_p	number)
					return varchar2 is

ds_tipo_cobranca_w	varchar2(255);

begin

select 	max(nvl(ds_tipo_cobranca,Wheb_mensagem_pck.get_texto(798802)))
into	ds_tipo_cobranca_w
from 	via_tipo_cobranca_hotel
where 	nr_sequencia = nr_sequencia_p;

return ds_tipo_cobranca_w;

end obter_tipo_cobranca_hotel;
/