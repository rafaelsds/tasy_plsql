create or replace
function Obter_se_apres_orient_mat(	cd_material_p		number,
									cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ie_mostra_orient_w		char(1) := 'S';
ie_sexo_w				varchar2(1);
qt_idade_w				number(15,4);
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;

begin

if	(cd_material_p is not null) and
	(cd_pessoa_fisica_p is not null) then

	select	nvl(max('N'),'S')
	into	ie_mostra_orient_w
	from	regra_orient_usuario
	where	rownum = 1
	and		cd_material = cd_material_p;
	
	if	(ie_mostra_orient_w = 'N') then
		select	max(obter_idade(dt_nascimento,nvl(dt_obito,sysdate),'DIA')),
				max(ie_sexo)
		into	qt_idade_w,
				ie_sexo_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
		select	nvl(max(ie_mostrar),'N')
		into	ie_mostra_orient_w
		from	regra_orient_usuario
		where	rownum = 1
		and		cd_material = cd_material_p
		and		nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
		and		(qt_idade_w between 
					obter_idade_conversao(qt_idade_min,qt_idade_min_mes,qt_idade_min_dia,qt_idade_max,qt_idade_max_mes,qt_idade_max_dia,'MIN') and
					obter_idade_conversao(qt_idade_min,qt_idade_min_mes,qt_idade_min_dia,qt_idade_max,qt_idade_max_mes,qt_idade_max_dia,'MAX'))
		and		((ie_sexo is null) or (ie_sexo = ie_sexo_w));
	end if;
end if;

return	ie_mostra_orient_w;

end Obter_se_apres_orient_mat;
/