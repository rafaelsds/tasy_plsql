create or replace
function PEP_obter_se_campo_marcado(nr_sequencia_p	number)
 		    	return varchar2 is

qt_reg_w		number(10);
ie_inf_1_w		varchar2(1);
ie_inf_2_w		varchar2(1);
ie_inf_3_w		varchar2(1);
ie_inf_4_w		varchar2(1);
ds_retorno_w	varchar2(255);

begin
select	count(*)
into	qt_reg_w
from	escala_icpp
where	nr_sequencia = nr_sequencia_p;

if	(qt_reg_w > 0) then
	select	max(ie_part_acomp),
			max(ie_part_acomp2),
			max(ie_part_acomp3),
			max(ie_part_acomp4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_apoio_fam),
			max(ie_apoio_fam2),
			max(ie_apoio_fam3),
			max(ie_apoio_fam4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_atividade),
			max(ie_atividade2),
			max(ie_atividade3),
			max(ie_atividade4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_oxigenacao),
			max(ie_oxigenacao2),
			max(ie_oxigenacao3),
			max(ie_oxigenacao4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_mob_deam),
			max(ie_mob_deam2),
			max(ie_mob_deam3),
			max(ie_mob_deam4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_alim_hid),
			max(ie_alim_hid2),
			max(ie_alim_hid3),
			max(ie_alim_hid4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_eliminicacao),
			max(ie_eliminicacao2),
			max(ie_eliminicacao3),
			max(ie_eliminicacao4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_hig_corp),
			max(ie_hig_corp2),
			max(ie_hig_corp3),
			max(ie_hig_corp4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_int_controle),
			max(ie_int_controle2),
			max(ie_int_controle3),
			max(ie_int_controle4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_terap_med),
			max(ie_terap_med2),
			max(ie_terap_med3),
			max(ie_terap_med4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
	select	max(ie_integridade),
			max(ie_integridade2),
			max(ie_integridade3),
			max(ie_integridade4)
	into	ie_inf_1_w,
			ie_inf_2_w,
			ie_inf_3_w,
			ie_inf_4_w
	from	escala_icpp
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_inf_1_w = 'N') and
		(ie_inf_2_w = 'N') and
		(ie_inf_3_w = 'N') and
		(ie_inf_4_w = 'N') then
		ds_retorno_w := Wheb_mensagem_pck.get_texto(1038944); -- Favor preencher uma informa��o de cada aspecto � analisar antes de liberar a escala.
		goto final;
	end if;
	
end if;

<<final>>
return	ds_retorno_w;

end PEP_obter_se_campo_marcado;
/