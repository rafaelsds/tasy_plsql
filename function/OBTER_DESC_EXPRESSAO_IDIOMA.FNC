create or replace
function obter_desc_expressao_idioma
			(	cd_expressao_p		number,
				ds_expressao_p		varchar2 default null,
				nr_seq_idioma_p		number)
				return varchar2 is
   
		 			 
ds_retorno_w			varchar2(4000)	:= null;
begin
if	(cd_expressao_p is not null) then  
	begin 
	ds_retorno_w	:= expressao_pck.obter_desc_expressao(cd_expressao_p,nvl(nr_seq_idioma_p,0));
	end;
else
	ds_retorno_w	:= ds_expressao_p;
end if;

return	nvl(ds_retorno_w,ds_expressao_p);

end obter_desc_expressao_idioma;
/
