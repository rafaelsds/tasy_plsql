create or replace
	function	obter_cor_conta_etapa(	nr_seq_etapa_p		number	)
			return varchar2 is

nr_seq_etapa_w	number(10,0);
ds_cor_etapa_w	varchar2(15) := '0';

begin	

select	max(nvl(ds_cor_etapa,0))
into	ds_cor_etapa_w
from	fatur_etapa
where	nr_sequencia	= nr_seq_etapa_p
and	nvl(ie_situacao,'A') = 'A';

if	(ds_cor_etapa_w is null) then
	ds_cor_etapa_w	:=	0;
end if;

return	ds_cor_etapa_w;

end obter_cor_conta_etapa;
/