create or replace
function smb_obter_total_reinternacoes(dt_inicial_p		date,
				       dt_final_p		date,
				       ie_tipo_convenio_p       number)
					return number is
soma_w	number(10);
conta_w number(10);
nr_atendimento_w number(10);
cd_pessoa_fisica_w varchar(15);
dt_entrada_w date;


Cursor C01 is

SELECT	a.nr_atendimento,
	a.dt_entrada,
	a.cd_pessoa_fisica
FROM 	atendimento_paciente_v a,
	diagnostico_doenca  b
WHERE  a.nr_atendimento = b.nr_atendimento
AND	a.cd_cid_principal = b.cd_doenca
and	to_char(a.dt_entrada,'dd/mm/yyyy') between dt_inicial_p and dt_final_p
and	((a.ie_tipo_convenio = ie_tipo_convenio_p) or (ie_tipo_convenio_p = '0'))
and	a.nr_atendimento = (select max(x.nr_atendimento) from atendimento_paciente_v x,
				       diagnostico_doenca  y
			WHERE  x.nr_atendimento = y.nr_atendimento
			AND	x.cd_cid_principal = y.cd_doenca
			and     x.cd_pessoa_fisica = a.cd_pessoa_fisica
			and     x.ie_tipo_atendimento = '1'
			and	to_char(x.dt_entrada,'dd/mm/yyyy') between dt_inicial_p and dt_final_p)
and	a.ie_tipo_atendimento = '1'
AND	((((obter_tipo_convenio(a.cd_convenio) = '3') 
	AND (TO_DATE(TO_CHAR(a.dt_entrada,'dd/mm/yyyy'))) >= TO_DATE(TO_CHAR(smb_obter_dt_ult_atend(a.cd_pessoa_fisica,
		    	 	 	 a.nr_atendimento,a.ie_tipo_atendimento,'DE'),'dd/mm/yyyy')) - 3)) 
	OR ((obter_tipo_convenio(a.cd_convenio) <> '3') 
	AND TO_DATE(TO_CHAR(a.dt_entrada,'dd/mm/yyyy')) >= TO_DATE(TO_CHAR(smb_obter_dt_ult_atend(a.cd_pessoa_fisica,
		    	 	 	 a.nr_atendimento,a.ie_tipo_atendimento,'DE'),'dd/mm/yyyy')) - 15))
and	obter_dados_ultimo_atend(a.cd_pessoa_fisica,a.nr_atendimento,a.ie_tipo_atendimento,'NA') is not null 
order by a.nr_atendimento;

			
			
begin

soma_w := 0;

open C01;
loop
fetch C01 into	
	nr_atendimento_w,
	dt_entrada_w,
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	
		SELECT count(*)
		into   conta_w
		FROM   	atendimento_paciente_v a,
			diagnostico_doenca b
		WHERE  a.nr_atendimento = b.nr_atendimento
		and	a.cd_cid_principal = b.cd_doenca
		AND	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		AND	a.nr_atendimento < nr_atendimento_w
		AND	a.ie_tipo_atendimento = '1' 
		AND	 (((TO_DATE(TO_CHAR(a.dt_entrada,'dd/mm/yyyy')) BETWEEN TO_DATE(TO_CHAR(dt_entrada_w,'dd/mm/yyyy')) -15 AND TO_DATE(TO_CHAR(dt_entrada_w,'dd/mm/yyyy'))) AND (obter_tipo_convenio(a.cd_convenio) <> '3'))
			OR (TO_DATE(TO_CHAR(a.dt_entrada,'dd/mm/yyyy')) BETWEEN TO_DATE(TO_CHAR(dt_entrada_w,'dd/mm/yyyy')) -3 AND to_date(to_char(dt_entrada_w,'dd/mm/yyyy'))) AND (obter_tipo_convenio(a.cd_convenio) = '3'));
			
			soma_w := soma_w + conta_w;
	
	end;
end loop;
close C01;

return	soma_w;

end smb_obter_total_reinternacoes;
/
