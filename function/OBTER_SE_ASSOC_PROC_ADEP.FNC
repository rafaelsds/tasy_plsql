create or replace
function obter_se_assoc_proc_adep	(cd_procedimento_p	number,
					ie_origem_proced_p	number)
					return varchar2 is

ie_adep_w	varchar2(1) := 'N';

begin
if	(cd_procedimento_p is not null) and
	(ie_origem_proced_p is not null) then
	select	nvl(max(ie_assoc_adep),'N')
	into	ie_adep_w
	from	procedimento
	where	cd_procedimento = cd_procedimento_p
	and	ie_origem_proced = ie_origem_proced_p;
end if;

return ie_adep_w;

end obter_se_assoc_proc_adep;
/