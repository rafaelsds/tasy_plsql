create or replace
function pat_obter_valor_transf_conta(	nr_seq_bem_p		number,
					dt_referencia_p		date,
					cd_conta_contabil_p	varchar2,
					ie_valor_p		varchar2)
								return number is

vl_retorno_w    number(15,2);
dt_inicial_w	date := trunc(dt_referencia_p,'mm');
dt_final_w	date := fim_mes(dt_referencia_p);

begin

if	(ie_valor_p = 'E') then

	select	nvl(sum(vl_original),0)
	into	vl_retorno_w
	from	pat_conta_alteracao a
	where	a.dt_transacao between dt_inicial_w and dt_final_w
	and	a.nr_seq_bem	= nvl(nr_seq_bem_p, a.nr_seq_bem)
	and	a.cd_conta_nova  = cd_conta_contabil_p;

elsif (ie_valor_p = 'S') then
	
	select	nvl(sum(vl_original),0)
	into	vl_retorno_w
	from	pat_conta_alteracao a
	where	a.dt_transacao between dt_inicial_w and dt_final_w
	and	a.nr_seq_bem	= nvl(nr_seq_bem_p, a.nr_seq_bem)
	and	a.cd_conta_antiga = cd_conta_contabil_p
	and	(	select 	count(*) -- Se n�o teve deprecia��o no m�s ent�o n�o contabiliza saida porque o bem foi cadastrado e j� foi trasnferido
		from	pat_valor_bem b
		where	b.nr_seq_bem = a.nr_seq_bem
		and	b.dt_valor < dt_final_w) > 1;
elsif	(ie_valor_p = 'EDA') then /* Entrada deprecia��o acumulada */

	select	nvl(sum(vl_deprec_acum),0)
	into	vl_retorno_w
	from	pat_conta_alteracao a
	where	a.dt_transacao between dt_inicial_w and dt_final_w
	and	a.nr_seq_bem	= nvl(nr_seq_bem_p, a.nr_seq_bem)
	and	a.cd_conta_nova  = cd_conta_contabil_p
	and	(	select 	count(*)
		from	pat_valor_bem b
		where	b.nr_seq_bem = a.nr_seq_bem
		and	b.dt_valor < dt_final_w) > 1;
	
elsif (ie_valor_p = 'SDA') then	/* Deprecia��o acumulada */
	select	nvl(sum(vl_deprec_acum),0)
	into	vl_retorno_w
	from	pat_conta_alteracao a
	where	a.dt_transacao between dt_inicial_w and dt_final_w
	and	a.nr_seq_bem	= nvl(nr_seq_bem_p, a.nr_seq_bem)
	and	a.cd_conta_antiga = cd_conta_contabil_p
	and	(	select 	count(*)
		from	pat_valor_bem b
		where	b.nr_seq_bem = a.nr_seq_bem
		and	b.dt_valor < dt_final_w) > 1;
end if;

return vl_retorno_w;

end pat_obter_valor_transf_conta;
/