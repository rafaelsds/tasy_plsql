create or replace
function obter_tempo_atend_local_pa(nr_atendimento_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);			

begin

select 	MAX(substr(obter_dif_data(dt_entrada_local,nvl(dt_saida_local,sysdate),null),1,255))
into	ds_retorno_w
from   	historico_localizacao_pa a
where  	a.nr_atendimento = nr_atendimento_p
and    	a.dt_entrada_local = (select max(b.dt_entrada_local) from historico_localizacao_pa b where a.nr_atendimento = b.nr_atendimento);


return	ds_retorno_w;

end obter_tempo_atend_local_pa;
/
