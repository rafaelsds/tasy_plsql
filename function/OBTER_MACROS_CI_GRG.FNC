create or replace
function OBTER_MACROS_CI_GRG
return varchar2 is

ds_retorno_w	varchar2(4000);

begin

ds_retorno_w	:= 	'@ds_evento - '||wheb_mensagem_pck.get_texto(314744)||chr(10)||
			'@nr_seq_lote - '||wheb_mensagem_pck.get_texto(314757)||chr(10)||
			'@nr_seq_analise - '||wheb_mensagem_pck.get_texto(314758)||chr(10)||
			'@ds_convenio - '||wheb_mensagem_pck.get_texto(314759)||chr(10)||
			'@ds_analise - '||wheb_mensagem_pck.get_texto(314761)||chr(10)||
			'@dt_lote - '||wheb_mensagem_pck.get_texto(314763)||chr(10)||
			'@vl_glosa_inf - '||wheb_mensagem_pck.get_texto(314764)||chr(10)||
			'@vl_glosa_aceita - '||wheb_mensagem_pck.get_texto(314766)||chr(10)||
			'@vl_reapresentacao - '||wheb_mensagem_pck.get_texto(314767);
			
return 	ds_retorno_w;

end OBTER_MACROS_CI_GRG;
/
