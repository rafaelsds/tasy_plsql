create or replace
function obter_tipo_mascara(
			ds_mascara_p  	varchar2,
			cd_mascara_p  	number,
			nr_sequencia_p 	number,
			ie_tipo_obj_p  	varchar2)
 		    	return varchar2 is
ds_mask_new_w varchar2(255);
ds_mask_upper_w varchar2(255);
begin
ds_mask_new_w := ds_mascara_p;
ds_mask_upper_w := upper(ds_mascara_p);

if (cd_mascara_p is not null) then
	begin
	select upper(DS_MASCARA_BR)
	into	ds_mask_upper_w
	from	dic_mascara
	where cd_mascara = cd_mascara_p;
	end;
end if;

if 	(ds_mask_upper_w = 'DD/MM/YYYY HH:MM:SS') or
	(ds_mask_upper_w = 'DD/MM/YYYY HH:MI:SS') or
	(ds_mask_upper_w = 'DD/MM/YYYY HH:MM.SS') or
	(ds_mask_upper_w = 'DD/MM/YYYY HH:M:SS') or
	(ds_mask_upper_w = 'DD/MM/YYYY HH:24:MI:SS') or
	(ds_mask_upper_w = 'DD/MM/YYY HH:MM:SS') or
	(ds_mask_upper_w = 'DD/MM/YYYY HH:MM:SSS') or
	(ds_mask_upper_w = 'DD/MM/YYYY HH24:MI:SS') or
	(ds_mask_upper_w = 'DD/MM/YYYY 24HH:MM:S') or
	(ds_mask_upper_w = 'DD/MM HH:MM:SS') then	
	begin
		ds_mask_new_w := 'date(timestamp)';
	end;
elsif (ds_mask_upper_w = 'DD/MM/YYYY') or 
	(ds_mask_upper_w = '00/00/0000;1;_') or
		(ds_mask_upper_w = 'DD/MM/YYYYY') or 
		(ds_mask_upper_w = 'MM/YYYY') then
	begin
		ds_mask_new_w := 'date(shortDate)';
	end;
elsif (ds_mask_upper_w = 'HH:MM:SS') or
		(ds_mask_upper_w = 'HH24:MI:SS') then
	begin
		ds_mask_new_w := 'date(mediumTime)';
	end;
elsif (ds_mask_upper_w = 'DD/MM/YYYY HH:MM') or
	(ds_mask_upper_w = 'DD/MM HH:MM') or 
	(ds_mask_upper_w = 'DD/MM/YY HH:MM') then	
	begin
		ds_mask_new_w := 'date(short)';
	end;
elsif (ds_mask_upper_w = 'HH:MM') or 
	(ds_mask_upper_w = '##:##') then
	begin
	ds_mask_new_w := 'date(shortTime)';
	end;
end if;

return	ds_mask_new_w;

end obter_tipo_mascara;
/