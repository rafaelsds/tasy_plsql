create or replace function cpoe_reg_valido_ativacao(
				dt_fim_p  			date,
				dt_inicio_p			date,
				dt_referencia_p 	date,
				cd_funcao_origem 	number default null,
				cd_perfil_p			number default null,
				ie_retrogrado_p		varchar default null,
				dt_liberacao_p 		date default null,
				ie_dia_inteiro_p	varchar2 default 'N',
				ie_futuro_p			varchar default null,
				dt_liberacao_fut_p	date default null,
				qt_horas_quimio_p	number default 0,
				ie_timeline_today_p			varchar2 default 'N',
				ie_oculta_nao_iniciado_p	varchar2 default 'N')	return char deterministic is 

nr_fim_w			number;
dt_ref_inicio_w		date;
dt_ref_fim_w		date;
qt_hor_w			number := 0;
qt_horas_ant_w		cpoe_presentation_rule.qt_horas_anteriores%type;
dt_referencia_w    date;
dt_limite_timeline_w    date;
begin

if	(dt_referencia_p is null) then
	dt_referencia_w := sysdate;
else
	dt_referencia_w := dt_referencia_p;
end if;

if (ie_oculta_nao_iniciado_p = 'S') then
	if (ie_timeline_today_p = 'N') then
		dt_limite_timeline_w := to_date(to_char(dt_referencia_w + 1,'dd/mm/yyyy')||' 08:00' ,'dd/mm/yyyy hh24:mi');
		if (to_date(to_char(dt_inicio_p,'dd/mm/yyyy hh24:mi'),'dd/mm/yyyy hh24:mi') >= dt_limite_timeline_w) then
		   return 'N';
		end if;
	else
		qt_horas_ant_w := cpoe_obter_quant_horas_ant(cd_perfil_p);
		dt_limite_timeline_w := to_date(to_char(dt_referencia_w + ((24 - qt_horas_ant_w)/24) ,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		if (to_date(to_char(dt_inicio_p,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') >= dt_limite_timeline_w) then
		   return 'N';
		end if;
	end if;
end if;

if	(nvl(ie_retrogrado_p,'N') = 'S') and
	(dt_liberacao_p is null) then
	return 'S';
end if;

if (nvl(ie_futuro_p, 'N') = 'S') and
	(dt_liberacao_fut_p is null) then
	return 'S';
end if;

if (dt_fim_p is null) then
	return 'S';
end if;

nr_fim_w := to_number(to_char( dt_fim_p , 'hh24'));

if (nr_fim_w = '24') then
	nr_fim_w  := 00;
end if;

if (cd_funcao_origem = 281 or cd_funcao_origem = 3130) then
	if (trunc(dt_referencia_w) > trunc(sysdate)) then
		dt_ref_inicio_w := (to_date(TO_CHAR(dt_referencia_w, 'dd/mm/yyyy')||' 08:00:00', 'dd/mm/yyyy hh24:mi:ss'));
	else 
		qt_horas_ant_w := cpoe_obter_quant_horas_ant(cd_perfil_p);

		dt_ref_inicio_w := (to_date(TO_CHAR(dt_referencia_w, 'dd/mm/yyyy')||to_char(sysdate,' hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') -(qt_horas_ant_w/24));
	end if;

	dt_ref_fim_w := (dt_ref_inicio_w + 1) - (1/1440);

	if ((dt_ref_inicio_w between dt_inicio_p and dt_fim_p)
		or (dt_ref_fim_w between dt_inicio_p and dt_fim_p)) then
		return 'S';
	end if;

	if ((trunc(dt_fim_p) = trunc(sysdate)) and
	   to_date(to_char(dt_fim_p,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') >=  to_date(to_char(sysdate -(qt_horas_ant_w/24) ,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) then
	   return 'S';
	end if;	

	if	(nvl(qt_horas_quimio_p,0) > 0 and
		trunc(dt_inicio_p,'mi') < (trunc(dt_referencia_w,'mi') + (qt_horas_quimio_p / 24)) and
		trunc(dt_fim_p,'dd') > trunc(sysdate,'dd')) then
		return 'S';
	end if;

elsif (trunc(dt_referencia_w) = trunc(sysdate) and ie_dia_inteiro_p = 'S') then
	if (trunc(dt_fim_p) = trunc(dt_referencia_w + 1) and nr_fim_w between 00 and 08) then
	   return 'S';
	end if;
	if ((trunc(dt_fim_p) >= trunc(dt_referencia_w + 1) or
		trunc(dt_fim_p) = trunc(sysdate)) and
		 to_date(to_char(dt_fim_p,'dd/mm/yyyy hh24'),'dd/mm/yyyy hh24:mi') >=  to_date(to_char(sysdate,'dd/mm/yyyy')||' 08:00' ,'dd/mm/yyyy hh24:mi')) then
	   return 'S';
	end if;
elsif (trunc(dt_referencia_w) = trunc(sysdate)) then
	begin

	if (trunc(dt_fim_p) = trunc(dt_referencia_w + 1) and
		nr_fim_w between 00 and 08) then
	   return 'S';
	end if;
	qt_horas_ant_w := cpoe_obter_quant_horas_ant(cd_perfil_p);
	if ((trunc(dt_fim_p) >= trunc(dt_referencia_w + 1) or
		trunc(dt_fim_p) = trunc(sysdate)) and
		to_date(to_char(dt_fim_p,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') >=  to_date(to_char(sysdate -(qt_horas_ant_w/24) ,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) then
	   return 'S';
	end if;

	end;
elsif (trunc(dt_referencia_w) = trunc(sysdate + 1)) then
	if (trunc(dt_fim_p) >= trunc(dt_referencia_w)) then
		if (trunc(dt_fim_p) > trunc(dt_referencia_w)) then
			return 'S';
		elsif (trunc(dt_fim_p) = trunc(dt_referencia_w) and
			nr_fim_w > 08) then
			return 'S';
		end if;
	end if;

elsif (trunc(dt_fim_p) >= trunc(dt_referencia_w)) then
	if ((nr_fim_w < 08) and
		((to_date(to_char(dt_fim_p,'dd/mm/yyyy hh24'),'dd/mm/yyyy hh24')
		between to_date(to_char(dt_referencia_w,'dd/mm/yyyy ')||' 08','dd/mm/yyyy hh24')
		and to_date(to_char(dt_referencia_w + 1,'dd/mm/yyyy ')||' 07','dd/mm/yyyy hh24')) or
		trunc(dt_fim_p) > to_char(dt_referencia_w + 1))) then
		return 'S';
	elsif (nr_fim_w between 09 and 23) then
		return 'S';
	end if;
end if;

return 'N';

end cpoe_reg_valido_ativacao;
/
