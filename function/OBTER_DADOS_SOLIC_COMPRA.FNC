create or replace
function obter_dados_solic_compra(
			nr_solic_compra_p		number,
			ie_tipo_dado_p		varchar2)
	return varchar2 is

ds_retorno_w			varchar2(100)	:= '';
cd_pessoa_solicitante_w		varchar2(20);
ds_pessoa_solicitante_w		varchar2(60);
dt_baixa_w			date;
ds_setor_responsavel_w		varchar2(100);
cd_setor_responsavel_w		number(5);
cd_comprador_resp_w		varchar2(10);
ie_aviso_aprov_oc_w		varchar2(1);
cd_centro_custo_w			number(8);
cd_setor_atendimento_w		number(5);
cd_local_estoque_w		number(4);
dt_pre_aprovacao_w		date;
dt_liberacao_w			date;

/*ie_tipo_dado
0 - pessoa solicitante
1 - nome pessoa solicitante
2 - data baixa
3 - Nome do setor responsavel (� o setor do comprador responsavel ou do solicitante)
4 - C�digo do setor responsavel (� o setor do comprador responsavel ou do solicitante)
5 - C�digo comprador responsavel
6 - Avisa aprova��o OC
7 - c�digo do centro de custo
8 - Codigo setor atendimento
9 - c�digo local estoque
10 - dt_pre_aprovacao_w
11 - Descri��o do centro de custo
12 - dt_liberacao
13 - t�tulo a pagar
*/

begin

if	(nvl(nr_solic_compra_p,0) > 0) then
	select	cd_pessoa_solicitante,
		substr(obter_nome_pessoa_fisica(cd_pessoa_solicitante, null),1,60),
		dt_baixa,
		substr(obter_nome_setor(obter_setor_usuario(obter_usuario_pessoa(nvl(cd_comprador_resp, cd_pessoa_solicitante)))),1,100),
		obter_setor_usuario(obter_usuario_pessoa(nvl(cd_comprador_resp, cd_pessoa_solicitante))),
		cd_comprador_resp,
		nvl(ie_aviso_aprov_oc,'N'),
		cd_centro_custo,
		cd_setor_atendimento,
		cd_local_estoque,
		dt_pre_aprovacao,
		dt_liberacao
	into	cd_pessoa_solicitante_w,
		ds_pessoa_solicitante_w,
		dt_baixa_w,
		ds_setor_responsavel_w,
		cd_setor_responsavel_w,
		cd_comprador_resp_w,
		ie_aviso_aprov_oc_w,
		cd_centro_custo_w,
		cd_setor_atendimento_w,
		cd_local_estoque_w,
		dt_pre_aprovacao_w,
		dt_liberacao_w
	from	solic_compra
	where	nr_solic_compra	= nr_solic_compra_p;

	if	(ie_tipo_dado_p = 0) then
		ds_retorno_w	:= cd_pessoa_solicitante_w;
	elsif	(ie_tipo_dado_p = 1) then
		ds_retorno_w	:= ds_pessoa_solicitante_w;
	elsif	(ie_tipo_dado_p = 2) then
		ds_retorno_w	:= dt_baixa_w;
	elsif	(ie_tipo_dado_p = 3) then
		ds_retorno_w	:= ds_setor_responsavel_w;
	elsif	(ie_tipo_dado_p = 4) then
		ds_retorno_w	:= cd_setor_responsavel_w;
	elsif	(ie_tipo_dado_p = 5) then
		ds_retorno_w	:= cd_comprador_resp_w;
	elsif	(ie_tipo_dado_p = 6) then
		ds_retorno_w	:= ie_aviso_aprov_oc_w;
	elsif	(ie_tipo_dado_p = 7) then
		ds_retorno_w	:= cd_centro_custo_w;
	elsif	(ie_tipo_dado_p = 8) then
		ds_retorno_w	:= cd_setor_atendimento_w;
	elsif	(ie_tipo_dado_p = 9) then
		ds_retorno_w	:= cd_local_estoque_w;
	elsif	(ie_tipo_dado_p = 10) then
		ds_retorno_w	:= dt_pre_aprovacao_w;
	elsif	(ie_tipo_dado_p = 11) then
		select	substr(obter_desc_centro_custo(cd_centro_custo_w),1,100)
		into	ds_retorno_w
		from	dual;
	elsif	(ie_tipo_dado_p = 12) then
		ds_retorno_w	:= dt_liberacao_w;
	elsif	(ie_tipo_dado_p = 13) then
		select	max(nr_titulo)
		into	ds_retorno_w
		from	titulo_pagar
		where	nr_solic_compra = nr_solic_compra_p;
	end if;
end if;

return ds_retorno_w;

end obter_dados_solic_compra;
/
