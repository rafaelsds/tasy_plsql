create or replace
function rp_obter_desc_classif_paciente(nr_seq_classif_p		number)
 		    	return varchar2 is
ds_classificacao_w	varchar2(80);
begin
if	(nr_seq_classif_p is not null) then
	select	max(DS_CLASSIFICACAO)
	into	ds_classificacao_w
	from	rp_classif_paciente
	where	nr_sequencia = nr_seq_classif_p;
end if;

return	ds_classificacao_w;

end rp_obter_desc_classif_paciente;
/
