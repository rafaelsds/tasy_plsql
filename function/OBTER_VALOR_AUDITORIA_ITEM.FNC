create or replace
function Obter_Valor_Auditoria_item(	nr_sequencia_p	number,
										ie_procmat_p	    varchar2)
 		    	return number is
			
			
vl_item_w		number(15,2);
vl_total_w		number(15,2):=0;
qt_ajuste_w		auditoria_matpaci.qt_ajuste%type;
qt_original_w		auditoria_matpaci.qt_original%type;
qt_dif_w		number(15,3);
ie_tipo_w		number(1,0);

vl_preco_w		number(15,2);
vl_unitario_w		number(15,4);
nr_interno_conta_w	number(10,0);

tx_proc_original_w	number(15,4);
tx_proc_ajuste_w	number(15,4);
tx_dif_w		number(15,4);
ie_tipo_auditoria_w	varchar2(1);
ie_tipo_item_w		varchar2(1);
nr_seq_audit_ext_w	number(10,0);
nr_seq_item_w		number(10,0);
nr_sequencia_w		number(10,0);

qt_procedimento_w	number(10,0);

cursor c01 is
	select	nvl(a.vl_material,0),
			b.qt_ajuste,
			b.qt_original,
			1,
			a.vl_unitario,
			null,
			null,
			'I',
			null,
			null
	from	material_atend_paciente a,
			auditoria_matpaci b
	where	a.nr_sequencia	    = b.nr_seq_matpaci
	and		a.nr_sequencia		= nr_sequencia_p
	and		a.cd_motivo_exc_conta is null
	and		nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia
	and		ie_procmat_p = 2
	union all
	select	nvl(a.vl_procedimento,0),
			b.qt_ajuste,
			b.qt_original,
			2,
			null,
			b.tx_proc_original,
			b.tx_proc_ajuste,
			'I',
			null,
			null
	from	procedimento_paciente a,
			auditoria_propaci b
	where	a.nr_sequencia	    = b.nr_seq_propaci 
	and		a.nr_sequencia		= nr_sequencia_p
	and		a.cd_motivo_exc_conta is null
	and		ie_procmat_p = 1
	and		nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia;
	
		
begin

open C01;
loop
fetch C01 into	
	vl_item_w,
	qt_ajuste_w,
	qt_original_w,
	ie_tipo_w,
	vl_unitario_w,
	tx_proc_original_w,
	tx_proc_ajuste_w,
	ie_tipo_auditoria_w,
	nr_seq_audit_ext_w,
	nr_sequencia_w;
exit when C01%notfound;
	begin		
		
	if	(ie_tipo_auditoria_w = 'I') then -- Auditoria Interna
	
		if 	(qt_ajuste_w is not null) then
			qt_dif_w:= qt_ajuste_w - qt_original_w;
		
			if	(ie_tipo_w = 1) then
					
				vl_total_w	:=   (vl_unitario_w * qt_dif_w);
				
			elsif	(ie_tipo_w = 2) then
		
				vl_preco_w	:=  dividir(vl_item_w, qt_original_w);
				vl_total_w	:=  (vl_preco_w * qt_dif_w);
		
			end if;
		
		elsif	(tx_proc_ajuste_w is not null) then
			tx_dif_w:= tx_proc_ajuste_w - tx_proc_original_w;
				
			vl_total_w:=  (vl_item_w * tx_dif_w / 100);
		end if;
		
	end if;
	
	end;
end loop;
close C01;

return	vl_total_w;

end Obter_Valor_Auditoria_item;
/
