create or replace
function sdh_formata_medida_laudo(	nr_seq_laudo_med_p	number,
									ie_opcao_p		varchar2)
 		    	return varchar2 is
				
/*Op��es 
VU - Valor formatado, Unidade de medida e valor de refer�ncia
*/

qt_decimais_w			number;
qt_medida_w				number;
cd_unidade_medida_w 	varchar2(25);
ds_retorno_w			varchar2(2000);
ds_valor_referencia_w	varchar(1000);
				
begin

select	obter_dado_medida_laudo(a.nr_seq_medida, 'Q'),
		round(a.qt_medida,nvl(obter_dado_medida_laudo(a.nr_seq_medida, 'Q'),999)),
		OBTER_DADO_MEDIDA_LAUDO(a.nr_seq_medida, 'U'),
		OBTER_MEDIDA_REFERENCIA_LAUDO(a.nr_sequencia)
into	qt_decimais_w,
		qt_medida_w,
		cd_unidade_medida_w,
		ds_valor_referencia_w
from	laudo_paciente_medida a 
where	a.nr_sequencia = nr_seq_laudo_med_p
and		(select	max(b.IE_TIPO_VALOR)
		from	MEDIDA_EXAME_LAUDO b
		where	a.nr_seq_medida = b.nr_sequencia) = 'N';

if	(qt_medida_w is null) then
	return null;
end if;

if	(qt_decimais_w is not null) then
	ds_retorno_w := rtrim(CAMPO_MASCARA_VIRGULA_CASAS(qt_medida_w,qt_decimais_w),',');
else	
	ds_retorno_w := qt_medida_w;
end if;

if	(ie_opcao_p = 'VU') then
	ds_retorno_w := ds_retorno_w||' '||cd_unidade_medida_w;
		
end if;

ds_retorno_w := trim(ds_retorno_w);

if	(substr(ds_Retorno_w,1,1) = ',' or substr(ds_retorno_w,1,1) = '.') then
	ds_retorno_w := '0'||ds_retorno_w;
end if;

return	trim(ds_retorno_w);

end sdh_formata_medida_laudo;
/
