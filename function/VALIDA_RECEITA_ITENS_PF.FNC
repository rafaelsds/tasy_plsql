create or replace
function valida_receita_itens_pf(	nr_seq_receita_p	number,
					tp_receita_p		varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number)
				return varchar2 is
			
ie_retorno_w		varchar(1);
cd_receita_especial_w	varchar(1);
qt_itens_w		number(10) := 0;

begin

cd_receita_especial_w := obter_valor_param_usuario(10015, 114, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);

if	((nr_seq_receita_p is not null) and
	 (tp_receita_p is not null) and
	 (cd_receita_especial_w is not null)) then

	-- Receita atual � receita especial, exige apenas medicamentos especiais 
	if (tp_receita_p = cd_receita_especial_w) then
		SELECT 	COUNT(*)		-- Se > 0 entao n�o validou itens;
		into	qt_itens_w
		FROM	fa_receita_farmacia_item a,
			material b
		WHERE 	a.cd_material = b.cd_material 
		AND   	a.nr_seq_receita = nr_seq_receita_p
		AND   	NVL(b.ie_receita,'N') = 'N';
	else
		SELECT 	COUNT(*)		-- Se > 0 entao n�o validou itens;
		into	qt_itens_w
		FROM	fa_receita_farmacia_item a,
			material b
		WHERE 	a.cd_material = b.cd_material 
		AND   	a.nr_seq_receita = nr_seq_receita_p
		AND   	NVL(b.ie_receita,'N') = 'S';		
	end if;
end if;
	
if (qt_itens_w > 0) then
	ie_retorno_w := 'N';	-- N�o validou itens
else
	ie_retorno_w := 'S';	-- Validou itens
end if;
	
return	ie_retorno_w;

end valida_receita_itens_pf;
/
