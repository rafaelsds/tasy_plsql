create or replace
function hsl_obter_data_ultima_compra(	cd_material_p			number,
				cd_estabelecimento_p		number)
return date is

dt_retorno_w			date := null;
nr_seq_ultima_compra_w		Number(10);

begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_ultima_compra_w
from	sup_dados_ultima_compra
where	cd_material 		= cd_material_p
and	cd_estabelecimento	= cd_estabelecimento_p
and	obter_se_nf_transferencia(nr_seq_nota) = 'N';

if	(nr_seq_ultima_compra_w > 0) then
	select	max(b.dt_entrada_saida)
	into	dt_retorno_w
	from	operacao_nota o,
		nota_fiscal_item a,
		nota_fiscal b
	where	a.nr_sequencia		= b.nr_sequencia
	and	a.cd_material		= cd_material_p
	and	b.cd_operacao_nf		= o.cd_operacao_nf
	and	nvl(o.ie_ultima_compra, 'S')	= 'S'	
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	b.nr_sequencia = (
		select	x.nr_seq_nota
		from	sup_dados_ultima_compra x
		where	x.nr_sequencia = nr_seq_ultima_compra_w);
else
	select	max(b.dt_entrada_saida)
	into	dt_retorno_w
	from	operacao_nota o,
		nota_fiscal_item a,
		nota_fiscal b
	where	a.nr_sequencia	= b.nr_sequencia
	and	a.cd_material	= cd_material_p
	and	b.cd_operacao_nf	= o.cd_operacao_nf
	and	nvl(o.ie_ultima_compra, 'S')	= 'S'		
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	b.nr_sequencia = (
			select	max(x.nr_sequencia)
			from	natureza_operacao n,
				operacao_nota o,
				nota_fiscal_item y,
				nota_fiscal x
			where	x.nr_sequencia		= y.nr_sequencia
			and	x.cd_natureza_operacao	= n.cd_natureza_operacao
			and	x.ie_acao_nf		= 1
			and	x.ie_situacao		= '1'
			and	n.ie_entrada_saida		= 'E'
			and	y.cd_material		= cd_material_p
			and	x.cd_operacao_nf		= o.cd_operacao_nf
			and	nvl(o.ie_ultima_compra, 'S') 	= 'S'				
			and	x.cd_estabelecimento	= cd_estabelecimento_p
			and	obter_se_nf_transferencia(x.nr_sequencia) = 'N');
end if;

return	dt_retorno_w;

end hsl_obter_data_ultima_compra;
/
