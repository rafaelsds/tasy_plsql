create or replace procedure 
tasylab_unimed_problema_ret (	dt_inicial_p	date,
				dt_final_p	date) is

ie_existe_w	varchar2(1);
dt_inicial_w	date	:= trunc(dt_inicial_p);
dt_final_w	date	:= trunc(dt_final_p) + 86399/86400;

Cursor C01 is
	select	a.nr_prescricao_origem nr_prescricao,
		a.nr_sequencia,
		a.ie_status_atend
	from	tasy.litoral_dados_integracao@tasy_litoral a
	where	a.dt_atualizacao between dt_inicial_p and fim_dia(dt_final_p);

cursor c02 is
	select	a.nr_prescricao,
		a.nr_sequencia
	from	prescr_procedimento a,
		lab_lote_externo b
	where	a.nr_seq_lote_externo = b.nr_sequencia	
	and	exists(select 1 from pessoa_juridica_estab x where x.cd_cgc = b.cd_cgc and x.nr_seq_xml_lab_envio = 100598)
	and	a.dt_atualizacao between dt_inicial_p and fim_dia(dt_final_p)	
	and	not exists(select 1 from tasy.litoral_dados_integracao@tasy_litoral x where x.nr_prescricao_origem = a.nr_prescricao and x.nr_sequencia = a.nr_sequencia);

c01_w		c01%rowtype;
c02_w		c02%rowtype;

begin

delete	w_lab_mapa_matricial
where	nm_usuario = 'TASYLAB';

open c01;
loop
fetch c01 into c01_w;
	exit when c01%notfound;
	begin

	if	(c01_w.ie_status_atend >= 35) then

		select	decode(count(*),0,'N','S')
		into	ie_existe_w
		from	prescr_procedimento a
		where	a.nr_prescricao = c01_w.nr_prescricao
		and	a.nr_sequencia = c01_w.nr_sequencia
		and	a.nr_seq_exame is null
		and	a.ie_status_atend >= 20
		and	a.ie_status_atend < 35;

		if	(ie_existe_w = 'S') then

			insert into w_lab_mapa_matricial(	nr_sequencia,
								nr_prescricao,
								nr_seq_prescr,
								dt_atualizacao,
								dt_atualizacao_nrec,
								nm_usuario,
								nm_usuario_nrec)
						values(		w_lab_mapa_matricial_seq.NextVal,
								c01_w.nr_prescricao,
								c01_w.nr_sequencia,
								sysdate,
								sysdate,
								'TASYLAB',
								'1');
		end if;

		select	decode(count(*),0,'N','S')
		into	ie_existe_w
		from	prescr_procedimento a
		where	a.nr_prescricao = c01_w.nr_prescricao
		and	a.nr_sequencia = c01_w.nr_sequencia
		and	a.nr_seq_exame is not null
		and	a.ie_status_atend >= 20
		and	a.ie_status_atend < 35;

		if	(ie_existe_w = 'S') then

			insert into w_lab_mapa_matricial(	nr_sequencia,
								nr_prescricao,
								nr_seq_prescr,
								dt_atualizacao,
								dt_atualizacao_nrec,
								nm_usuario,
								nm_usuario_nrec)
						values(		w_lab_mapa_matricial_seq.NextVal,
								c01_w.nr_prescricao,
								c01_w.nr_sequencia,
								sysdate,
								sysdate,
								'TASYLAB',
								'5');
		end if;

	end if;

	if	(c01_w.ie_status_atend = 12) then

		select	decode(count(*),0,'N','S')
		into	ie_existe_w
		from	prescr_procedimento a
		where	a.nr_prescricao = c01_w.nr_prescricao
		and	a.nr_sequencia = c01_w.nr_sequencia
		and	a.nr_seq_exame is not null
		and	a.ie_status_atend = 20;

		if	(ie_existe_w = 'S') then

			insert into w_lab_mapa_matricial(	nr_sequencia,
								nr_prescricao,
								nr_seq_prescr,
								dt_atualizacao,
								dt_atualizacao_nrec,
								nm_usuario,
								nm_usuario_nrec)
						values(		w_lab_mapa_matricial_seq.NextVal,
								c01_w.nr_prescricao,
								c01_w.nr_sequencia,
								sysdate,
								sysdate,
								'TASYLAB',
								'15');

		end if;
	end if;
	end;
end loop;
close c01;

open c02;
loop
fetch c02 into c02_w;
	exit when c02%notfound;
	begin

	insert into w_lab_mapa_matricial(	nr_sequencia,
						nr_prescricao,
						nr_seq_prescr,
						dt_atualizacao,
						dt_atualizacao_nrec,
						nm_usuario,
						nm_usuario_nrec)
				values(		w_lab_mapa_matricial_seq.NextVal,
						c02_w.nr_prescricao,
						c02_w.nr_sequencia,
						sysdate,
						sysdate,
						'TASYLAB',
						'10');

	end;
end loop;
close c02;

commit;

end tasylab_unimed_problema_ret;
/