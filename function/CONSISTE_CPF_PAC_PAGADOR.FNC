CREATE OR REPLACE
FUNCTION Consiste_Cpf_Pac_Pagador
			(	nr_atendimento_p	Number)
				return varchar2 is


cd_pessoa_fisica_w	Varchar2(10);
nr_cpf_w		varchar2(11);
ie_brasileiro_w		Varchar2(1);
ds_retorno_w		Varchar2(1)	:= 'N';


BEGIN

select	nvl(max(cd_pessoa_fisica),'')
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

select	nvl(max(cd_pessoa_fisica),cd_pessoa_fisica_w)
into	cd_pessoa_fisica_w
from	atendimento_pagador
where	nr_atendimento	= nr_atendimento_p;

select	obter_cpf_pessoa_fisica(cd_pessoa_fisica_w),
	obter_se_brasileiro(cd_pessoa_fisica_w)
into	nr_cpf_w,
	ie_brasileiro_w
from	dual;

if	(nr_cpf_w is null) and
	(ie_brasileiro_w	= 'S') then
	ds_retorno_w		:= 'S';
end if;

return	ds_retorno_w;

END Consiste_Cpf_Pac_Pagador;
/