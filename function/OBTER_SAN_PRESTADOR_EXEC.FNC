Create or Replace
Function Obter_san_prestador_exec (nr_sequencia_p		number,
				   ie_responsavel_credito_p	varchar2,
				   cd_convenio_p		number)
					return varchar2 is

cd_cgc_prestador_w	varchar2(14);
ie_entra_conta_w	varchar2(1):= 'S';
cd_estabelecimento_w	number(4);
ds_retorno_w		varchar2(12):= '';

begin

begin
select 	max(nvl(ie_entra_conta,'S'))
into	ie_entra_conta_w
from 	regra_honorario
where 	cd_regra = ie_responsavel_credito_p;
exception
	when others then
	ie_entra_conta_w:= 'S';
end;

begin
select 	max(nvl(cd_cgc_prestador,'0')),
	max(obter_estab_atend(nr_atendimento))
into	cd_cgc_prestador_w,
	cd_estabelecimento_w
from 	procedimento_paciente
where	nr_sequencia = nr_sequencia_p;
exception
	when others then
	cd_cgc_prestador_w:= '0';
end;

if	(cd_cgc_prestador_w = '0') or (ie_entra_conta_w = 'S') then	
	select 	max(substr(obter_valor_conv_estab(cd_convenio_p,cd_estabelecimento_w,'CD_INTERNO'),1,12))
	into	ds_retorno_w
	from 	dual;
else
	select 	max(substr(obter_prestador_convenio(cd_cgc_prestador_w,cd_convenio_p,null),1,12))
	into	ds_retorno_w
	from 	dual;	
end if;

return ds_retorno_w;

end Obter_san_prestador_exec;
/
