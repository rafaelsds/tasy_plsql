create or replace function Obter_point_of_care(    CD_SETOR_ATENDIMENTO_p    number, 
                                cd_unidade_basica_p        varchar2, 
                                cd_unidade_compl_p        varchar2) 
               return varchar2 is 
ds_setor_atendimento_w    varchar2(200); 
begin 
 
if  (CD_SETOR_ATENDIMENTO_p    is not null) and 
    (cd_unidade_basica_p       is not null) and 
    (cd_unidade_compl_p        is not null) then 
    select    max(nvl(nvl(NM_SETOR_INTEGRACAO, SUBSTR(obter_dados_setor(cd_setor_atendimento_p,'NC'),1,255)),
                                               SUBSTR(obter_dados_setor(cd_setor_atendimento_p,'DS'),1,255)))
    into    ds_setor_atendimento_w
    from    unidade_atendimento 
    where    cd_setor_atendimento    = CD_SETOR_ATENDIMENTO_p 
    and      cd_unidade_basica       = cd_unidade_basica_p 
    and      cd_unidade_compl        = cd_unidade_compl_p; 
end if; 
  
return    ds_setor_atendimento_w; 
 
end Obter_point_of_care;
/

