create or replace
function man_obter_qt_casos_teste(nr_story_p	number)
 		    	return number is

qt_casos_teste_w	number(10);
			
begin

select	count(*)
into	qt_casos_teste_w
from	desenv_suite_teste a,
	desenv_caso_teste b
where	a.nr_sequencia = b.nr_suite
and	a.nr_story = nr_story_p;

return	qt_casos_teste_w;

end man_obter_qt_casos_teste;
/