create or replace
function pls_obter_desc_tabela_linear
		(	nr_seq_tabela_p	number,
			ie_opcao_p	varchar2)
			return varchar2 is
			
/*
ie_opcao_p
	D - Descri��o
	C - C�digo
*/			

ds_retorno_w		varchar2(255);
nm_tabela_w		varchar2(255);

begin

if	(nr_seq_tabela_p is not null) then
	select	nm_tabela
	into	nm_tabela_w
	from	pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_p;
	
	if	(upper(nm_tabela_w) like '%LINEAR%') then
		if	(ie_opcao_p = 'D') then
			ds_retorno_w	:= 'Linear';
		elsif	(ie_opcao_p = 'C') then
			ds_retorno_w	:= 'L';
		end if;
	else 
		if	(ie_opcao_p = 'D') then
			ds_retorno_w	:= 'Faixa et�ria';
		elsif	(ie_opcao_p = 'C') then
			ds_retorno_w	:= 'F';
		end if;
	end if;	
end if;

return	ds_retorno_w;

end pls_obter_desc_tabela_linear;
/    