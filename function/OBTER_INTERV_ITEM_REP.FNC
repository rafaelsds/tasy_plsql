create or replace
function obter_interv_item_rep	(nr_prescricao_p	number,
			nr_seq_item_p	number,
			ie_tipo_item_p	varchar2)
			return varchar2 is

cd_intervalo_w	varchar2(7);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_item_p is not null) and
	(ie_tipo_item_p is not null) then

	if	(ie_tipo_item_p in ('S','M','MAT','IA','IAG')) then

		select	nvl(max(cd_intervalo),'XPTO')
		into	cd_intervalo_w
		from	prescr_material
		where	nr_prescricao 	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_item_p;

	elsif	(ie_tipo_item_p in ('P','G','C','I','HM','H')) then

		select	nvl(max(cd_intervalo),'XPTO')
		into	cd_intervalo_w
		from	prescr_procedimento
		where	nr_prescricao 	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_item_p;

	elsif	(ie_tipo_item_p = 'R') then

		select	nvl(max(cd_intervalo),'XPTO')
		into	cd_intervalo_w
		from	prescr_recomendacao
		where	nr_prescricao 	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_item_p;

	elsif	(ie_tipo_item_p = 'E') then

		select	nvl(max(cd_intervalo),'XPTO')
		into	cd_intervalo_w
		from	pe_prescr_proc
		where	nr_seq_prescr	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_item_p;
		
	elsif	(ie_tipo_item_p = 'D') then

		select	nvl(max(cd_intervalo),'XPTO')
		into	cd_intervalo_w
		from	prescr_dieta
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_item_p;

	end if;

end if;

return cd_intervalo_w;

end obter_interv_item_rep;
/