CREATE OR REPLACE
function obter_qt_dias_postergar_sc(		cd_estabelecimento_p	number,
					cd_material_p		number,
					ie_retorno_p		varchar2)
return varchar2 is

/*ie_retorno_p
Q - Qt dias postergar
D - Somente dias �teis*/

cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
qt_dias_postergar_w			number(6)	:= 0;
ie_somente_dia_util_w			varchar2(1)	:= 'S';
ds_retorno_w				varchar2(255);


cursor c01 is
select	qt_dias_postergar,
	nvl(ie_somente_dia_util,'S')
from	regra_postergar_entrega_sc
where	cd_estabelecimento = cd_estabelecimento_p
and	ie_situacao = 'A'
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(cd_material, cd_material_p) 			= cd_material_p or cd_material_p = 0)
order by
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);

begin

select	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v a
where	a.cd_material = cd_material_p;

open C01;
loop
fetch C01 into	
	qt_dias_postergar_w,
	ie_somente_dia_util_w;
exit when C01%notfound;
	begin	
	qt_dias_postergar_w 	:= qt_dias_postergar_w;
	ie_somente_dia_util_w	:= ie_somente_dia_util_w;
	end;
end loop;
close C01;


if	(ie_retorno_p = 'Q') then
	ds_retorno_w := nvl(qt_dias_postergar_w,0);
elsif	(ie_retorno_p = 'D') then
	ds_retorno_w := nvl(ie_somente_dia_util_w,'S');
end if;

return ds_retorno_w;

end obter_qt_dias_postergar_sc;
/
