create or replace
function obter_qt_ultima_compra_forn(
			cd_estabelecimento_p		number,
			qt_dia_p				number,
			cd_material_p			number,
			cd_local_estoque_p		number,
			cd_cnpj_p			varchar2,
			ie_tipo_p				varchar2)
return number is

vl_retorno_w			number(15,4);
nr_sequencia_w			number(10,0);
ie_tipo_w				varchar2(10);
cd_material_estoque_w		number(6);

begin
ie_tipo_w			:= ie_tipo_p;
vl_retorno_w		:= 0;

if	(ie_tipo_w = 'N') then
	select	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	natureza_operacao o,
		operacao_nota p,
		nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_sequencia		= b.nr_sequencia
	and	b.cd_natureza_operacao	= o.cd_natureza_operacao
	and	o.ie_entrada_saida		= 'E'
	and	cd_material  		= cd_material_p
	and	b.cd_operacao_nf = p.cd_operacao_nf
	and	nvl(p.ie_ultima_compra, 'S') = 'S'	
	and	a.dt_atualizacao >= sysdate - nvl(qt_dia_p,90)
	and	((cd_local_estoque_p is null) or (cd_local_estoque = cd_local_estoque_p))
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.cd_cgc_emitente		= cd_cnpj_p
	and	b.ie_situacao		= '1';
else
	select	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	natureza_operacao o,
		operacao_nota p,
		nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_sequencia		= b.nr_sequencia
	and	b.cd_natureza_operacao	= o.cd_natureza_operacao
	and	o.ie_entrada_saida		= 'E'
	and	cd_material_estoque	= cd_material_p
	and	b.cd_operacao_nf = p.cd_operacao_nf
	and	nvl(p.ie_ultima_compra, 'S') = 'S'	
	and	a.dt_atualizacao >= sysdate - nvl(qt_dia_p,90)
	and	((cd_local_estoque_p is null) or (cd_local_estoque = cd_local_estoque_p))
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.cd_cgc_emitente		= cd_cnpj_p	
	and	b.ie_situacao		= '1';
end if;

if	(nr_sequencia_w > 0) then
	select	nvl(max(a.QT_ITEM_NF),0)
	into	vl_retorno_w
	from 	nota_fiscal_item a
	where	nr_sequencia 		= nr_sequencia_w
	and	(((ie_tipo_p = 'N') and (cd_material = cd_material_p)) or
		((ie_tipo_p <> 'N') and (cd_material_estoque = cd_material_p)));
end if;

return	vl_retorno_w;

end obter_qt_ultima_compra_forn;
/
