create or replace
function obter_se_plano_lib_perfil
		(cd_convenio_p		number,
		cd_plano_p		varchar2,
		cd_perfil_p		number)
		return varchar2 is

ie_liberado_w		varchar2(01)	:= 'S';
qt_regra_w		number(10,0);

begin

select	count(*)
into	qt_regra_w
from	regra_plano_perfil
where	cd_convenio		= cd_convenio_p
and	cd_perfil		= cd_perfil_p;

if	(qt_regra_w	<> 0) then
	select	nvl(max('S'), 'N')
	into	ie_liberado_w
	from	regra_plano_perfil
	where	cd_convenio		= cd_convenio_p
	and	cd_plano		= cd_plano_p
	and	cd_perfil		= cd_perfil_p;

end if;

return	ie_liberado_w;

end obter_se_plano_lib_perfil;
/
