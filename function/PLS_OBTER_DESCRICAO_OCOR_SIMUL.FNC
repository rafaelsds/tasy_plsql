create or replace
function pls_obter_descricao_ocor_simul
			(	nr_seq_simultaneo_p	number)
				return varchar is

ds_retorno_w			varchar(255);
			
begin

begin
select	ds_regra
into	ds_retorno_w
from	pls_ocorrencia_simultaneo
where	nr_sequencia = nr_seq_simultaneo_p;
exception
when others then
	ds_retorno_w := '';
end;

return	ds_retorno_w;

end pls_obter_descricao_ocor_simul;
/