create or replace
function  lab_obter_aviso_sonoro_ciencia	(nr_prescricao_p		number,
					 ie_status_atend_p		number)
 return varchar2 is

ds_retorno_w  varchar2(1) := 'N';

begin
if	(nr_prescricao_p > 0) then
	SELECT 	decode(COUNT(*), 0, 'N', 'S')
	into 	ds_retorno_w
	FROM 	prescr_procedimento a
	WHERE 	a.nr_prescricao = nr_prescricao_p
	AND		a.ie_status_atend = ie_status_atend_p
	AND		a.dt_ciencia_laboratorio is null;
end if;

return	ds_retorno_w;

end lab_obter_aviso_sonoro_ciencia;
/