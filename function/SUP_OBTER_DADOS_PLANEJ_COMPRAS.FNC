create or replace 
function sup_obter_dados_planej_compras(
			nr_seq_planejamento_p	number,
			ie_tipo_dado_p		varchar2)
		return number is

nr_solic_compra_w		Number(10);
ds_retorno_w		Varchar2(255);

begin

select	nvl(max(nr_solic_compra),0)
into	nr_solic_compra_w
from	solic_compra
where	nr_seq_planejamento	= nr_seq_planejamento_p;

if	(ie_tipo_dado_p = 'SC') then
	ds_retorno_w	:= nr_solic_compra_w;
end if;

return ds_retorno_w;

end sup_obter_dados_planej_compras;
/