create or replace
FUNCTION ingoh_qtd_transfusao_reserva( nr_sequencia_p	NUMBER,
				ie_opcao_p	VARCHAR2)
 		    	RETURN NUMBER IS

qtd_w			NUMBER(10);
qtd_sem_reserva_w	number(10);
cd_pessoa_w		varchar2(10);
BEGIN

IF (nr_sequencia_p IS NOT NULL) THEN
	IF	(ie_opcao_p = 'T') THEN
		SELECT 	COUNT(*)
		INTO	qtd_w
		FROM   	san_doacao
		WHERE  	nr_seq_transfusao = nr_sequencia_p;
	ELSE
		select 	max(cd_pessoa_fisica)
		into	cd_pessoa_w
		from	san_reserva
		where	nr_sequencia = nr_sequencia_p;
		
		SELECT 	COUNT(*)
		INTO	qtd_sem_reserva_w
		FROM   	san_doacao
		WHERE  	nr_seq_reserva is null
		and 	cd_pessoa_dest = cd_pessoa_w;
		
		SELECT 	COUNT(*) + qtd_sem_reserva_w
		INTO	qtd_w
		FROM   	san_doacao
		WHERE  	nr_seq_reserva = nr_sequencia_p;
	END IF;
END IF;

RETURN	qtd_w;

END ingoh_qtd_transfusao_reserva;
/