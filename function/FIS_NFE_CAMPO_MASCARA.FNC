create or replace
function fis_nfe_campo_mascara(
			vl_campo_p		number,
			qt_decimais_p		number,
			ie_ponto_virgula_p	varchar2)
	    	return varchar2 IS


ds_retorno_w		varchar2(60) := '';
vl_campo_w		varchar2(60);
vl_novo_campo_w		varchar2(60);
i			number(10)  := 0;
ie_decimais_w		varchar2(10);

begin

if	(qt_decimais_p = '1') then
	ie_decimais_w	:= '.9';
elsif	(qt_decimais_p = '2') then
	ie_decimais_w	:= '.99';
elsif	(qt_decimais_p = '3') then
	ie_decimais_w	:= '.999';
elsif	(qt_decimais_p = '4') then
	ie_decimais_w	:= '.9999';
elsif	(qt_decimais_p = '0') then
	ie_decimais_w	:= '';
end if;


if	(vl_campo_p is not null) then
	begin
	vl_campo_w	:= to_char(vl_campo_p);

	if(ie_ponto_virgula_p = 'V') then
		select replace(replace(replace(replace(to_char(vl_campo_p, '999,999,999,990' || ie_decimais_w), ',', 'X'),'.', ','), 'X', '.'),' ','')
		into vl_campo_w
		from dual;
		
	else
		select	replace(replace(to_char(vl_campo_p, '999,999,999,990' || ie_decimais_w), ',', ''),' ','')
		into	vl_campo_w
		from	dual;
		
	end if;
	
	i := 0;
	for	i in 1..length(vl_campo_w) loop
		if	(substr(vl_campo_w,i,1) <> ' ') then
			vl_novo_campo_w	:= vl_novo_campo_w || substr(vl_campo_w,i,1);
		end if;
	end loop;
	end;
end if;

ds_retorno_w	:= vl_novo_campo_w;

return ds_retorno_w;

end fis_nfe_campo_mascara;
/