create or replace
FUNCTION Med_Obter_Minutos_Espera
		(nr_seq_agecons_p	NUMBER)
		RETURN number IS

qt_min_espera_w		NUMBER(15,0);
qt_min_consulta_w	NUMBER(15,0);
qt_minutos_w		NUMBER(15,0);

BEGIN

SELECT	TO_NUMBER(DECODE(f.dt_entrada, NULL, NULL,
			DECODE(a.ie_status_agenda, 'N', NULL,
			ROUND((NVL(f.dt_inicio_consulta, SYSDATE) - f.dt_entrada) *
			1440)))) qt_min_espera
INTO	qt_min_espera_w
FROM  	MED_ATENDIMENTO f,
      	AGENDA_CONSULTA a
WHERE  	a.nr_sequencia 		= f.nr_seq_agenda (+)
AND	a.nr_sequencia		= nr_seq_agecons_p;

RETURN	qt_min_espera_w;

END Med_Obter_Minutos_Espera;
/