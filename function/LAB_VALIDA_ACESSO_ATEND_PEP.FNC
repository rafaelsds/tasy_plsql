create or replace
function lab_valida_acesso_atend_pep(
        cd_estabelecimento_p    in estabelecimento.cd_estabelecimento%type,
        nr_atendimento_p        in atendimento_paciente.nr_atendimento%type,
        cd_paciente_p           in pessoa_fisica.cd_pessoa_fisica%type,
        nm_usuario_p            in usuario.nm_usuario%type,
        dt_logon_p              in date
) return varchar2 is

ie_permite_w                    varchar2(1 char) := 'S';
cd_pessoa_fisica_w	        pessoa_fisica.cd_pessoa_fisica%type;
cd_medico_resp_w	        varchar2(10 char);
ie_existe_regra_w		number(1);
ie_libera_regra_w		number(1);

begin

begin
select	cd_pessoa_fisica
into 	cd_pessoa_fisica_w
from 	usuario
where 	nm_usuario = nm_usuario_p;
exception
when no_data_found then
	cd_pessoa_fisica_w := null;
end;

select 	count(1)
into 	ie_existe_regra_w
from 	pep_autor_acesso
where 	cd_paciente     = cd_paciente_p
and     rownum          = 1;

if ( ie_existe_regra_w > 0 ) and 
   ( nr_atendimento_p > 0 ) then
		
        ie_permite_w := 'N';

        select	count(*)
        into	ie_libera_regra_w
        from	pep_autor_acesso
        where	cd_paciente		= cd_paciente_p
        and	cd_pessoa_fisica	= cd_pessoa_fisica_w
        and	(sysdate	        between	nvl( dt_inicio, to_date('01/01/1900','dd/mm/yyyy') ) 
                                        and     nvl( dt_fim,    to_date('01/01/2900','dd/mm/yyyy') ) )
        and dt_inativacao is null;

        if ( ie_libera_regra_w > 0 ) then
                ie_permite_w := 'S';
        else
                begin
                select	cd_medico_resp
                into	cd_medico_resp_w
                from	atendimento_paciente
                where	nr_atendimento	= nr_atendimento_p;
                exception
                when no_data_found then
                        cd_pessoa_fisica_w := null;
                end;
                if ( cd_medico_resp_w = cd_pessoa_fisica_w ) then
                        ie_permite_w := 'S';
                end if;
        end if;
end if;

return ie_permite_w;

end lab_valida_acesso_atend_pep;
/
