create or replace
function Obter_UM_Dispositivo_solucao(	ie_bomba_infusao_p	varchar2,
					ie_opcao_p		varchar2)
 		    	return varchar2 is

ie_tipo_dosagem_w	varchar2(10);

begin

if	(ie_opcao_p	= 'S') then
	select	max(ie_tipo_dosagem)
	into	ie_tipo_dosagem_w
	from	rep_dispositivo
	where	ie_bomba_infusao	= ie_bomba_infusao_p
	and	ie_tipo_item		= 1;
end if;

return	ie_tipo_dosagem_w;

end Obter_UM_Dispositivo_solucao;
/