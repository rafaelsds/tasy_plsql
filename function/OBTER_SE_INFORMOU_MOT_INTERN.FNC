create or replace
function Obter_se_informou_mot_intern(nr_atendimento_p	number)
 		    	return varchar2 is
nr_seq_interno_w  	number(10);
nr_seq_motivo_p		number(10);
begin

nr_seq_interno_w := Obter_Atepacu_paciente(nr_atendimento_p,'A');

select	max(nr_seq_motivo_int)
into	nr_seq_motivo_p
from	atend_paciente_unidade
where	nr_seq_interno	= nr_seq_interno_w;	

If	(nr_seq_motivo_p is null)
Or 	(nr_seq_motivo_p <= 0) then
	return 'N';
else
	return 'S';
end if;

end Obter_se_informou_mot_intern;
/
