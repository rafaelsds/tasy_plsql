create or replace
function obter_etapa_evento_dp_adep	(nr_prescricao_p	number,
				nr_seq_solucao_p	number,
				ie_dialise_p	varchar2,
				ie_evento_p	varchar2)
				return varchar2 is

nr_etapa_w	number(3,0);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) and
	(ie_dialise_p is not null) and
	(ie_evento_p is not null) then

	if	(ie_dialise_p <> 'DPA') then

		select	count(*)
		into	nr_etapa_w
		from	hd_prescricao_evento
		where	nr_prescricao	= nr_prescricao_p
		and	nr_seq_solucao	= nr_seq_solucao_p
		and	ie_evento	= 'II';

		if	(ie_evento_p = 'II') then		
			nr_etapa_w := nr_etapa_w + 1;
		elsif	(nr_etapa_w = 0) then
			nr_etapa_w := 1;
		end if;

	else

		select	count(*)
		into	nr_etapa_w
		from	hd_prescricao_evento
		where	nr_prescricao	= nr_prescricao_p
		and	nr_seq_solucao	= nr_seq_solucao_p
		and	ie_evento	= 'DPAI';

		if	(ie_evento_p = 'DPAI') then		
			nr_etapa_w := nr_etapa_w + 1;
		elsif	(nr_etapa_w = 0) then
			nr_etapa_w := 1;
		end if;		

	end if;

end if;

return nr_etapa_w;

end obter_etapa_evento_dp_adep;
/