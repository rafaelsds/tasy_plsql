create or replace
function pls_obter_qt_guias_prest_prot(		nr_seq_prestador_p	Number,
						ie_tipo_guia_p		Number,
						dt_inicio_p		Date,
						dt_fim_p		Date,
						ie_tiss_p		Varchar)
 		    	return Number is

/*

  ==========================================================================================================
||				ATEN��O						||
||										||
||	Esta function foi criada apenas para uso no relat�rio RT11 - Qtd. e Valor dos eventos por prestador		||
||										||
   =========================================================================================================
   
 */
vl_retorno_w	Number(30) := 0;
qt_guias_w	Number(30) := 0;
	
begin

if	(nr_seq_prestador_p is not null) then

	if	(ie_tipo_guia_p is not null) then
		
		begin
		
		select	count(a.nr_sequencia)
		into	qt_guias_w
		from	pls_conta a,
			pls_protocolo_conta b
		where	a.nr_seq_protocolo = b.nr_sequencia
		and	a.nr_seq_prestador_exec = nr_seq_prestador_p
		and	(
			((ie_tipo_guia_p = 5) and (a.ie_tipo_guia = 6))
			or ((ie_tipo_guia_p = 4) and (a.ie_tipo_guia = 5))
			or ((ie_tipo_guia_p = 2) and (a.ie_tipo_guia = 4) and (pls_obter_cd_tiss_atendimento(a.nr_seq_tipo_atendimento) <> '04'))
			or ((ie_tipo_guia_p = 1) and (a.ie_tipo_guia = 4) and (pls_obter_cd_tiss_atendimento(a.nr_seq_tipo_atendimento) = '04'))
			or ((ie_tipo_guia_p = 1) and (a.ie_tipo_guia = 3))
			)
		/*and	a.ie_tipo_guia = (
						case
							when (ie_tipo_guia_p = 5) then 6
							when (ie_tipo_guia_p = 4) then 5
							when (
								(ie_tipo_guia_p = 2)
								and (a.ie_tipo_guia = 4)
								and (a.nr_seq_tipo_atendimento <> 8)
								) then 4
							when (
								(ie_tipo_guia_p = 1)
								and (a.ie_tipo_guia = 4)
								and (a.nr_seq_tipo_atendimento = 8)
								) then 4
							when (ie_tipo_guia_p = 1) then 3
							else 0
						end
					)*/
		/*and	(
			(a.ie_tipo_guia = ie_tipo_guia_p) or 
						(	
							(ie_tipo_guia_p = 3) and 
							(a.ie_tipo_guia = 4) and 
							(pls_obter_cd_tiss_atendimento(a.nr_seq_tipo_atendimento) = '04')
						)
			)*/
		and	((b.dt_mes_competencia >= dt_inicio_p) and (b.dt_mes_competencia <= dt_fim_p))
		and	b.ie_situacao in ('A','D','T')
		and	b.ie_tipo_protocolo <> 'R'
		--and	((((b.ie_origem_protocolo = 'E') and (nvl(ie_tiss_p,'N') = 'S')) or (nvl(ie_tiss_p,'N') = 'N'))
		and 	(((a.ie_origem_conta = 'E') and (nvl(ie_tiss_p,'N') = 'S')) or (nvl(ie_tiss_p,'N') = 'N'));
		exception
		when others then
			qt_guias_w := 0;
		end;
	end if;
end if;

vl_retorno_w := nvl(qt_guias_w,0);

return	vl_retorno_w;

end pls_obter_qt_guias_prest_prot;
/