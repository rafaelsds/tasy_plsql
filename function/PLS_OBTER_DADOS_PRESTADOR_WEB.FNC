create or replace
function  pls_obter_dados_prestador_web
			(	nr_seq_usuario_web_p	Integer,
				ie_tipo_p		varchar2)
 		    		return Varchar2 is
				
ds_retorno_w			Varchar2(60) := '';
/*
 Tipos
 NP  - Nome prestador
 NU - Nome usu�rio web 
 PW - Perfil web
*/


begin

if	(ie_tipo_p = 'NP') then
	select	substr(nvl(max(pls_obter_dados_prestador(nr_seq_prestador, 'N')),''),1,60)
	into	ds_retorno_w
	from	pls_usuario_web
	where	nr_sequencia	= nr_seq_usuario_web_p;
elsif 	(ie_tipo_p = 'NU') then
	select	substr(nvl(max(nm_usuario_web),''),1,60)
	into	ds_retorno_w
	from	pls_usuario_web
	where	nr_sequencia	= nr_seq_usuario_web_p;
elsif	(ie_tipo_p = 'PW') then
	select	substr(nvl(max(nr_seq_perfil_web),0),1,60)
	into	ds_retorno_w
	from	pls_usuario_web
	where	ie_situacao = 'A'
	and	nr_sequencia = nr_seq_usuario_web_p;
end if;

return	ds_retorno_w;

end  pls_obter_dados_prestador_web;
/