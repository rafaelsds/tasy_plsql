create or replace
function obter_inf_exame_rotina(nr_seq_rotina_p		number)
 		    	return varchar2 is

ds_informacao_w		varchar2(4000);
ds_prescricao_w		varchar2(4000);

cursor c01 is
select	ds_prescricao
from	exame_lab_rotina
where	nr_seq_rotina = nr_seq_rotina_p
union all
select	ds_prescricao
from	procedimento_rotina
where	nr_seq_rotina = nr_seq_rotina_p
order by 1 desc;
			
begin

open c01;
loop
	fetch c01 into
		ds_prescricao_w;
	exit when c01%notfound;
	
	if length(ds_prescricao_w || chr(13) || ds_informacao_w) > 4000-3 then
		ds_informacao_w := substr(ds_informacao_w, 1, 4000-3) || '...';
		exit;
	end if;
	
	ds_informacao_w	:= ds_prescricao_w || chr(13) || ds_informacao_w;
end loop;
close c01;

return	ds_informacao_w;

end obter_inf_exame_rotina;
/
