create or replace
function pls_obter_dados_pag_prestador(	nr_seq_pag_prest_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(255)	:= null;				

/*ie_opcao_p
NP 	- Nome do prestador
CP	- C�digo do prestador
*/				
begin
if	(nr_seq_pag_prest_p is not null) then
	if	(ie_opcao_p = 'NP') then
		select	substr(pls_obter_dados_prestador(a.nr_seq_prestador, 'N'),1,255)
		into	ds_retorno_w
		from	pls_pagamento_prestador a	
		where	a.nr_sequencia = nr_seq_pag_prest_p;
	elsif	(ie_opcao_p = 'CP') then
		select	substr(pls_obter_cod_prestador(a.nr_seq_prestador,null),1,255)
		into	ds_retorno_w
		from	pls_pagamento_prestador a	
		where	a.nr_sequencia = nr_seq_pag_prest_p;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_dados_pag_prestador;
/