create or replace
function obter_dados_agent_mat(	nr_seq_agente_p	number,
				ie_opcao_p	varchar2 )
				return varchar2 is

ds_material_w		varchar2(100);
ds_retorno_w		varchar2(255);
ie_tipo_material_w	varchar2(3);
ie_tipo_agente_w	varchar2(255);
/*
ie_opcao_p:
D - Descricao do material
T - Tipo do material
*/
				
begin

if	(nr_seq_agente_p > 0) then
	select	substr(obter_desc_material(a.cd_material),1,100),
		Obter_Tipo_Material(a.cd_material,'C'),
		obter_valor_dominio(1613,c.ie_tipo) 
	into	ds_material_w,
		ie_tipo_material_w,
		ie_tipo_agente_w
	from	agente_anest_material a,
		agente_anestesico c
	where	a.nr_sequencia	= nr_seq_agente_p
	and	c.nr_sequencia = a.NR_SEQ_AGENTE;
end if;

if	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_material_w;
elsif	(ie_opcao_p = 'T') then
	ds_retorno_w	:= ie_tipo_material_w;
elsif	(ie_opcao_p = 'TA') then
	ds_retorno_w	:= ie_tipo_agente_w;
end if;

return ds_retorno_w;

end obter_dados_agent_mat;
/
