create or replace
function obter_Se_titulo_pago(nr_seq_mensalidade_p		number,
			dt_liquidacao_p			varchar2)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);			
qt_registro_W	number(10);

begin

select 	count(*)
into	qt_registro_W
from	titulo_receber r,
	pls_mensalidade p
where	r.nr_seq_mensalidade = p.nr_sequencia
and	p.nr_sequencia = nr_seq_mensalidade_p
and	to_char(r.dt_liquidacao, 'YYYY') = dt_liquidacao_p
and	((r.vl_saldo_titulo < vl_titulo)
or	(r.ie_situacao = 2));

if qt_registro_W > 0 then
	ds_retorno_w	:= 'S';
else
	ds_retorno_w	:= 'N';
end if;


return	ds_retorno_w;

end obter_Se_titulo_pago;
/
