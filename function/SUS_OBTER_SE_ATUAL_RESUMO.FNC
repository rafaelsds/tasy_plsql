create or replace
function sus_obter_se_atual_resumo (nr_interno_conta_p	conta_paciente.nr_interno_conta%type)
 		    		return varchar2 is
				
ds_retorno_w		varchar2(1) := 'S';
cd_convenio_w		conta_paciente.cd_convenio_parametro%type;
ie_status_acerto_w	conta_paciente.ie_status_acerto%type;
ie_tipo_convenio_w	convenio.ie_tipo_convenio%type;
qt_rateio_sus_w		number(10) := 0;

begin

begin
select	nvl(cd_convenio_parametro,0),
	nvl(ie_status_acerto,1)
into	cd_convenio_w,
	ie_status_acerto_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p
and	rownum = 1;
exception
when others then
	cd_convenio_w		:= 0;
	ie_status_acerto_w	:= 1;
end;

if	(cd_convenio_w > 0) then
	begin
	
	ie_tipo_convenio_w := nvl(Obter_Tipo_Convenio(cd_convenio_w),0);
	
	if	(ie_tipo_convenio_w = 3) and
		(ie_status_acerto_w = 2) then
		begin
		
		select	count(1)
		into	qt_rateio_sus_w
		from	material_atend_paciente
		where	nr_interno_conta = nr_interno_conta_p
		and	cd_motivo_exc_conta is null
		and	nvl(vl_material,0) > 0
		and	rownum = 1;
		
		if	(qt_rateio_sus_w > 0) then
			ds_retorno_w := 'N';
		end if;
		
		end;
	end if;
	
	end;
end if;	

return	nvl(ds_retorno_w,'S');

end sus_obter_se_atual_resumo;
/
