create or replace
function Obter_Se_Mat_Lote_Validade
		(nr_atendimento_p		number,
		cd_material_p			number,
		dt_material_p			date)
		return varchar2 is

ie_material_validade_w		varchar2(01)	:= 'N';		
qt_reg_w			number(10,0);	
dt_validade_w			date;	
	
Cursor 	C01 is
	select	b.dt_validade
	from	material_lote_fornec b,
		material_atend_paciente a
	where	a.NR_SEQ_LOTE_FORNEC	= b.nr_sequencia
	and	a.nr_atendimento	= nr_atendimento_p
	and	a.cd_motivo_exc_conta is null
	and	a.cd_material		= cd_material_p
	order by b.dt_validade;	

begin

open C01;
loop
fetch C01 into	
	dt_validade_w;
exit when C01%notfound;
	begin
	if	(dt_validade_w >= dt_material_p) then
		ie_material_validade_w	:= 'S';
	end if;
	
	end;
end loop;
close C01;

return	ie_material_validade_w;

end Obter_Se_Mat_Lote_Validade;
/