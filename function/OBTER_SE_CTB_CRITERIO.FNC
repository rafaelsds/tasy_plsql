create or replace
function OBTER_SE_CTB_CRITERIO
		(nr_seq_criterio_p		number,
		cd_pessoa_fisica_p		varchar2,
		cd_cgc_p			varchar2) return varchar2 is

cont_w			number(10,0);
qt_regra_w		number(10,0);
ie_liberado_w		varchar2(255) := 'S';

Begin

select	count(*)
into	qt_regra_w
from 	ctb_criterio_rateio a,
	ctb_crit_rateio_lib_cp b
where	a.nr_sequencia 		= b.nr_seq_criterio
and 	ie_situacao = 'A'
and	(b.nr_seq_criterio	= nr_seq_criterio_p
or	b.cd_pessoa_fisica	= cd_pessoa_fisica_p
or	b.cd_cgc		= cd_cgc_p);

if	(qt_regra_w > 0) then
	if	(cd_pessoa_fisica_p is not null) then
		select	count(*)
		into	cont_w
		from 	ctb_crit_rateio_lib_cp
		where	nr_seq_criterio		= nr_seq_criterio_p
		and	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	elsif	(cd_cgc_p is not null) then
		select	count(*)
		into	cont_w
		from 	ctb_crit_rateio_lib_cp
		where	nr_seq_criterio	= nr_seq_criterio_p
		and	cd_cgc		= cd_cgc_p;
	end if;
end if;

if	(cont_w = 0) then
	ie_liberado_w	:= 'N';
else
	ie_liberado_w	:= 'S';
end if;

return		ie_liberado_w;

end OBTER_SE_CTB_CRITERIO;
/
