create or replace FUNCTION CP_obter_desc_cp_interv(
    nr_sequencia_p NUMBER)
  RETURN VARCHAR2
IS
  ds_retorno_w   VARCHAR2(2000);
BEGIN
  BEGIN
    SELECT ds_display_name
    INTO ds_retorno_w
    FROM cp_intervention
    WHERE nr_sequencia = nr_sequencia_p;
  EXCEPTION
  WHEN OTHERS THEN
    ds_retorno_w := '';
  END;
RETURN ds_retorno_w;
END CP_obter_desc_cp_interv;
/ 