create or replace
function rxt_obter_se_agenda_pac_livre(	cd_pessoa_fisica_p	varchar2,
			dt_agenda_p		date,
			qt_duracao_p		number)
 		    	return varchar2 is
ds_retorno_w	varchar2(1) := 'S';	
	
begin
select	decode(count(*),0,'S','N')
into	ds_retorno_w
from	agenda_paciente
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	dt_agenda between dt_agenda_p and dt_agenda_p + (qt_duracao_p/1440)
and	ie_status_agenda <> 'C';

if	(ds_retorno_w = 'S') then
	select	decode(count(*),0,'S','N')
	into	ds_retorno_w
	from	agenda_quimio
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_agenda between dt_agenda_p and dt_agenda_p + (qt_duracao_p/1440)
	and	ie_status_agenda <> 'C';
end if;

if	(ds_retorno_w = 'S') then
	select	decode(count(*),0,'S','N')
	into	ds_retorno_w
	from	agenda_consulta
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_agenda between dt_agenda_p and dt_agenda_p + (qt_duracao_p/1440)
	and	ie_status_agenda <> 'C';
end if;

if	(ds_retorno_w = 'S') then
	select	decode(count(*),0,'S','N')
	into	ds_retorno_w
	from	rxt_agenda
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_agenda between dt_agenda_p and dt_agenda_p + (qt_duracao_p/1440)
	and	ie_status_agenda <> 'C';
end if;

return	ds_retorno_w;

end rxt_obter_se_agenda_pac_livre;
/