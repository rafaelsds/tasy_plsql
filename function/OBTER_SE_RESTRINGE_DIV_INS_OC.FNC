create or replace
function obter_se_restringe_div_ins_oc(	nr_seq_regra_p		number,
				cd_cnpj_p		varchar2)
 		    	return varchar2 is

ie_restringe_w		varchar2(1);
qt_registro_w		number(10);			
	

begin

select	count(*)
into	qt_registro_w
from	regra_diverg_ins_oc_rest
where	nr_seq_regra = nr_seq_regra_p;

if	(qt_registro_w = 0) then
	ie_restringe_w := 'S';
else
	ie_restringe_w	:= 'N';
	
	select	count(*)
	into	qt_registro_w
	from	regra_diverg_ins_oc_rest	
	where	nr_seq_regra = nr_seq_regra_p
	and	cd_cnpj = cd_cnpj_p
	and	ie_restringe = 'S';
	
	if	(qt_registro_w > 0) then
		ie_restringe_w := 'S';
	end if;

end if;

return	ie_restringe_w;

end obter_se_restringe_div_ins_oc;
/
