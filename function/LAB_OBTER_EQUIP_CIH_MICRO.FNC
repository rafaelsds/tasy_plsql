create or replace FUNCTION LAB_OBTER_EQUIP_CIH_MICRO(cd_microorganismo_p NUMBER,
ds_sigla_equip_p VARCHAR2) RETURN VARCHAR2
IS
cd_microorganismo_w number(10);
BEGIN
  select max(a.CD_MICROORGANISMO_INTEGRACAO)
  into cd_microorganismo_w
  from CIH_MICROORGANISMO_INT a, EQUIPAMENTO_LAB b
  where a.cd_equipamento = b.cd_equipamento
  and b.ds_sigla = ds_sigla_equip_p
  and a.cd_microorganismo = cd_microorganismo_p;
  
  RETURN cd_microorganismo_w;
END LAB_OBTER_EQUIP_CIH_MICRO;
/