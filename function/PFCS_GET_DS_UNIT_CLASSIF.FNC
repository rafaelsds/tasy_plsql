CREATE OR REPLACE FUNCTION PFCS_GET_DS_UNIT_CLASSIF(
    cd_unit_classification_p varchar2,
    nr_seq_language_p number default null,
    ie_abbreviation_p varchar default 'N'
) RETURN VARCHAR2 AS
BEGIN
    if (ie_abbreviation_p = 'N') then
        if (nvl(nr_seq_language_p, 1) <> 1) then
            if (cd_unit_classification_p = '3') then
                return obter_desc_expressao_idioma(1062434, null, nr_seq_language_p);
            elsif (cd_unit_classification_p = 'SD') then
                return obter_desc_expressao_idioma(1062432, null, nr_seq_language_p);
            end if;
        elsif (cd_unit_classification_p = 'SD') then
            return obter_desc_expressao_idioma(1029708, null, 1);
        end if;
    else
        if (cd_unit_classification_p = '3') then
            return obter_desc_expressao_idioma(1062150, null, nr_seq_language_p);
        elsif (cd_unit_classification_p = '4') then
            return obter_desc_expressao_idioma(301095, null, nr_seq_language_p);
        elsif (cd_unit_classification_p = 'SD') then
            return obter_desc_expressao_idioma(1062152, null, nr_seq_language_p);
        end if;
    end if;
    return obter_valor_dominio(1, cd_unit_classification_p);
END PFCS_GET_DS_UNIT_CLASSIF;
/
