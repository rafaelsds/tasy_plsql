create or replace
function obter_se_possui_hemoc_inutil( nr_seq_doacao_p	number)
 		    	return number is

qtd_w 	number(8);			
begin

if	(nr_seq_doacao_p is not null) then

	select 	count(*)
	into	qtd_w
	from  	san_producao
	where 	nr_seq_doacao = nr_seq_doacao_p
	and	nr_seq_inutil is not null;
	
end if;

return	qtd_w;

end obter_se_possui_hemoc_inutil;
/