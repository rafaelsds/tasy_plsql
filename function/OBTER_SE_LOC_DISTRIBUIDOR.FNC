create or replace
function obter_se_loc_distribuidor(nr_seq_loc_os_p in number, nr_seq_loc_distribuidor_p in number,cd_cnpj_dist_p in varchar2)
 		    	return varchar2 is

qt_reg_w 	number(10);
cd_cnpj_w	varchar2(14);
ie_retorno_w 	varchar2(1) := 'N';	
	
begin

if	(nr_seq_loc_os_p = nr_seq_loc_distribuidor_p) then
	ie_retorno_w := 'S';
else
	if	(cd_cnpj_dist_p is null ) then
		select	cd_cnpj
		into	cd_cnpj_w
		from	man_localizacao
		where	nr_sequencia = nr_seq_loc_distribuidor_p;
	else
		cd_cnpj_w := cd_cnpj_dist_p;
	end if;
	
	select 	count(*)
	into	qt_reg_w
	FROM	com_canal_cliente a,
		com_cliente b,
		com_canal c,
		man_localizacao d
	WHERE	d.cd_cnpj = b.cd_cnpj
	AND	c.cd_cnpj = cd_cnpj_w  
	and	d.nr_sequencia = nr_seq_loc_os_p
	AND	a.nr_seq_canal = c.nr_sequencia
	AND	a.ie_tipo_atuacao = 'V'
	AND	b.nr_sequencia = a.nr_seq_cliente 
	AND	b.ie_fase_venda = 'F'
	AND	b.ie_resp_atend = 'D'
	AND	b.ie_classificacao = 'C'
	AND	a.dt_fim_atuacao IS NULL;
	
	if	(qt_reg_w = 0 ) then	
		SELECT	count(*)
		into	qt_reg_w
		FROM	com_canal_cliente a,
			com_cliente b,
			com_canal c,
			man_localizacao d
		WHERE EXISTS (SELECT	1
			FROM	man_localizacao_pj f
			WHERE	d.nr_sequencia = f.nr_seq_localizacao
			AND	f.cd_cnpj = b.cd_cnpj)
		AND	c.cd_cnpj = cd_cnpj_w  
		and	d.nr_sequencia = nr_seq_loc_os_p
		AND	a.nr_seq_canal = c.nr_sequencia
		AND	a.ie_tipo_atuacao = 'V'
		AND	b.nr_sequencia = a.nr_seq_cliente 
		AND	b.ie_fase_venda = 'F'
		AND	b.ie_resp_atend = 'D'
		AND	b.ie_classificacao = 'C'
		AND	a.dt_fim_atuacao IS NULL;
	end if;
	
	if	(qt_reg_w > 0 ) then
		ie_retorno_w := 'S';
	end if;
end if;
return	ie_retorno_w;

end obter_se_loc_distribuidor;
/