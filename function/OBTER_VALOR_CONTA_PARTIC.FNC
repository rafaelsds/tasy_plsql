CREATE OR REPLACE
FUNCTION Obter_Valor_Conta_Partic	(nr_sequencia_p	number)
					RETURN Number IS

vl_retorno_w	procedimento_participante.vl_Conta%type:=0;

BEGIN

if	(nr_sequencia_p is not null) then
	select	nvl(sum(nvl(p.vl_Conta,0)),0)
	into	vl_retorno_w
	from	procedimento_participante p
	where	nr_sequencia = nr_sequencia_p;
end if;

RETURN	vl_retorno_w;

END	Obter_Valor_Conta_Partic;
/
