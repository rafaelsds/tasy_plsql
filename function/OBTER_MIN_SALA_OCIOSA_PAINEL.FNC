create or replace
function obter_min_sala_ociosa_painel 	(nr_cirurgia_p	number)
					return number is
					
nr_atendimento_w	number(10)	:= null;
dt_entrada_unidade_w	date		:= null;
dt_termino_w		date		:= null;
qt_tempo_w		number(15,4) 	:= 0;
ie_zera_tempo_w		varchar2(1);
dt_inicio_real_w	date		:= null;
cd_setor_atendimento_w	number(5);
cd_unidade_basica_w	varchar2(10);
cd_unidade_compl_w	varchar2(10);

					
begin
begin
if	(nvl(nr_cirurgia_p,0) > 0) then

	select	max(b.cd_setor_atendimento),
		max(b.cd_unidade_basica),
		max(b.cd_unidade_compl),
		max(a.dt_termino)
	into	cd_setor_atendimento_w,
		cd_unidade_basica_w,
		cd_unidade_compl_w,
		dt_termino_w
	from	cirurgia a,
		atend_paciente_unidade b
	where	a.dt_entrada_unidade 	= b.dt_entrada_unidade
	and	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_cirurgia 		= nr_cirurgia_p;
	
	if	(dt_termino_w is not null) then
		select	min(dt_inicio_real)
		into	dt_inicio_real_w
		from	cirurgia a,
			atend_paciente_unidade b
		where	a.dt_entrada_unidade 	= b.dt_entrada_unidade
		and	a.nr_atendimento	= b.nr_atendimento
		and	b.cd_setor_atendimento	= cd_setor_atendimento_w
		and	b.cd_unidade_basica	= cd_unidade_basica_w
		and	b.cd_unidade_compl	= cd_unidade_compl_w
		and	a.ie_status_cirurgia	<> 4
		and	a.dt_inicio_real	> dt_termino_w;
		
		if	(dt_inicio_real_w is null) then
			qt_tempo_w := trunc((sysdate - dt_termino_w) * 1440);
		else
			qt_tempo_w := trunc((dt_inicio_real_w - dt_termino_w) * 1440);
		end if;
	end if;	
end if;	
exception
	when others then
	qt_tempo_w := 0;
end;
		
return 	qt_tempo_w;

end obter_min_sala_ociosa_painel;
/