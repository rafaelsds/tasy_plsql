create or replace FUNCTION clipboard_info_concat (
    gen_w IN NUMBER
) RETURN VARCHAR2 AS

    ds_retorno_w   VARCHAR2(2000);
    nur_team_w     VARCHAR2(2000);
    CURSOR c01 IS
    SELECT
        obter_nome_pf(cd_profissional)
    FROM
        pf_profissional
    WHERE
        cd_paciente = gen_w;

BEGIN
    OPEN c01;
    LOOP
        FETCH c01 INTO nur_team_w;
        EXIT WHEN c01%notfound;
        ds_retorno_w := nur_team_w
                        || ';'
                        || ds_retorno_w;
    END LOOP;

    CLOSE c01;
    RETURN substr(ds_retorno_w, 0, length(ds_retorno_w) - 1);
END clipboard_info_concat;
/
