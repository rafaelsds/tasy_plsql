create or replace
function obter_saldo_emprestimo_expurgo(
			nr_seq_conjunto_p		number,
			cd_local_estoque_p		number)
 		    	return number is

qt_saldo_w		cm_expurgo_saldo.qt_saldo%type;

begin

select	nvl(max(qt_saldo),0)
into	qt_saldo_w
from	cm_expurgo_saldo
where	nr_seq_conjunto = nr_seq_conjunto_p
and	cd_local_estoque = cd_local_estoque_p;

if	(qt_saldo_w >= 0) then

	qt_saldo_w := 0;
	
elsif	(qt_saldo_w < 0) then

	qt_saldo_w := (qt_saldo_w * -1);

end if;

return	qt_saldo_w;

end obter_saldo_emprestimo_expurgo;
/