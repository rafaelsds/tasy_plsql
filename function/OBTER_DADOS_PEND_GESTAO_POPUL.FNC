create or replace 
function obter_dados_pend_gestao_popul (	cd_pessoa_fisica_p	varchar2,
											ie_opcao_p			varchar2) 
			return number is

/*Descri��o 				ie_opcao_p
Vacinas pendentes				VP
Gr�vidas						G
Exames pendentes				EP
Receitas pendentes				RP
*/

qt_consultas_agend_w		number(10) := 0;
qt_consultas_realizada_w	number(10) := 0;
qt_retorno_w				number(10) := 0;
dt_nascimento_w				date;

begin

if (ie_opcao_p = 'VP') then

	select	nvl(max(1),0)
	into	qt_retorno_w
	from    vacina b,
		    paciente_vacina e 
	where  e.nr_seq_vacina = b.nr_sequencia 
	and    e.cd_pessoa_fisica = cd_pessoa_fisica_p
	AND		e.dt_cancelamento is null 
	AND	not ((e.dt_vacina is not null 
			AND	e.dt_prevista_execucao is null 
			AND	e.ie_localidade_inf <> 'S') OR (e.dt_vacina is not null 
			AND	e.dt_prevista_execucao is not null 
			AND	e.ie_localidade_inf = 'N'));
		
elsif (ie_opcao_p = 'G') then

	Select  nvl(max(1),0)
	into	qt_retorno_w
	from	atendimento_gravidez a,
			atendimento_paciente b
	WHERE   a.nr_atendimento = b.nr_atendimento
	and		nvl(a.ie_pac_gravida,'N') = 'S'
	and		b.ie_nivel_atencao = 'P'
	and		nvl(a.ie_situacao,'A') = 'A'
	and		b.cd_pessoa_fisica  	= cd_pessoa_fisica_p;
	

elsif (ie_opcao_p = 'EP') then


	Select  nvl(max(1),0)
	into	qt_retorno_w
	from	PEDIDO_EXAME_EXTERNO_ITEM a,
			PEDIDO_EXAME_EXTERNO b,
			atendimento_paciente c
	WHERE   a.nr_seq_pedido = b.nr_sequencia
	and		b.nr_atendimento = c.nr_atendimento
	and		b.ie_nivel_atencao = 'P'
	and		nvl(B.ie_situacao,'A') = 'A'
	and		c.cd_pessoa_fisica  	= cd_pessoa_fisica_p
	and 	not exists(select 1 from exame_lab_resultado x where x.nr_seq_pedido_exam_ext_item = a.nr_sequencia);


elsif (ie_opcao_p = 'RP') then

	select	nvl(max(1),0)
	into	qt_retorno_w
	from 	fa_receita_farmacia a,
			fa_receita_farmacia_item b
	where 	a.nr_sequencia = b.nr_seq_receita
	and 	nvl(b.ie_uso_continuo,'N') = 'S'
	and		a.ie_nivel_atencao = 'P'
	and		a.dt_validade_receita > sysdate
	and 	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and		a.DT_LIBERACAO is not null	
	and		(fim_dia(a.dt_validade_receita) - sysdate) <= 15;
		
end if;

return qt_retorno_w;

end obter_dados_pend_gestao_popul;
/