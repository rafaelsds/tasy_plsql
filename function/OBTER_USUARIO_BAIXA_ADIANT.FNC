Create or Replace
FUNCTION OBTER_USUARIO_BAIXA_ADIANT(	nr_adiantamento_p	number,
					dt_referencia_p		date)
					RETURN varchar2 is

ds_retorno_w	varchar2(255);
nm_usuario_w	varchar2(15);

cursor	c01 is
select	a.nm_usuario
from	adiantamento_baixa_v a
where	a.dt_baixa		<= dt_referencia_p
and	a.nr_adiantamento	= nr_adiantamento_p;

BEGIN

open	c01;
loop
fetch	c01 into
	nm_usuario_w;
exit	when c01%notfound;

	if	(ds_retorno_w	is null) then

		ds_retorno_w	:= nm_usuario_w;

	elsif	(instr(ds_retorno_w || ', ',nm_usuario_w || ', ')	= 0) then

		ds_retorno_w	:= substr(ds_retorno_w || ', ' || nm_usuario_w,1,255);

	end if;

end	loop;
close	c01;

RETURN ds_retorno_w;

END OBTER_USUARIO_BAIXA_ADIANT;
/