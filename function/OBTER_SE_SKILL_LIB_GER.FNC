create or replace
function Obter_se_skill_lib_ger (nr_seq_gerencia_p		number,
								nr_seq_skill_p			number)
 		    	return varchar2 is

ie_liberado_w	varchar2(1);
				
begin

select	nvl(max('S'),'N')
into	ie_liberado_w
from	ger_skill_desenv
where	nr_seq_gerencia		= nr_seq_gerencia_p
and		nr_seq_skill_Desenv	= nr_seq_skill_p;

return	ie_liberado_w;

end Obter_se_skill_lib_ger;
/