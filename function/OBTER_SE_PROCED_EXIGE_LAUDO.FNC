create or replace
function obter_se_proced_exige_laudo(
		cd_procedimento_p	number,
		ie_origem_proced_p	number)
		return varchar2 is

ie_exige_laudo_w	varchar2(1) := 'N';
begin
if	(cd_procedimento_p is not null) and
	(ie_origem_proced_p is not null) then
	begin
	select	nvl(max(ie_exige_laudo),'N')
	into	ie_exige_laudo_w
	from	procedimento
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced_p	= ie_origem_proced_p;
	end;
end if;
return	ie_exige_laudo_w;
end obter_se_proced_exige_laudo;
/