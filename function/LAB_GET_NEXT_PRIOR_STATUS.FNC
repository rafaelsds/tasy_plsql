create or replace
function lab_get_next_prior_status(ie_current_status_p	number,
								   ie_prior_p		varchar2) return number as
								   
ie_status_return_w		number(3);
ds_status_param_value_w	funcao_parametro.vl_parametro%type;
ie_comma_index_w		number(10);
ie_status_w				number(10);
ie_prior_w				varchar2(1);

function get_max_status(ie_current_status_p	number,
						ie_max_status_p		number,
						ie_next_status_p	number,
						ie_prior_p			varchar2) return number AS		
begin
	if (ie_next_status_p is not null and (ie_max_status_p is null or ie_max_status_p <= ie_next_status_p)) then
		if (ie_prior_p = 'S' and ie_next_status_p < ie_current_status_p) then
			return ie_next_status_p;
		elsif (ie_prior_p <> 'S' and ie_next_status_p > ie_current_status_p) then
			return ie_next_status_p;
		end if;
		return ie_max_status_p;
	end if;
	return null;
end; 
				
begin

	ie_prior_w := nvl(ie_prior_p, 'N');

	ds_status_param_value_w := obter_parametro_usuario_js(722, 2);

	ie_comma_index_w := instr(ds_status_param_value_w, ',');

	if (ie_comma_index_w = 0) then
		begin
			ie_status_return_w := to_number(trim(ds_status_param_value_w));
		exception
		when others then
			ie_status_return_w := null;
		end;
		if (ie_prior_w = 'S' and ie_status_return_w is not null and ie_status_return_w >= ie_current_status_p) then
			ie_status_return_w := null;
		elsif (ie_prior_w <> 'S' and ie_status_return_w is not null and ie_status_return_w <= ie_current_status_p) then
			ie_status_return_w := null;
		end if;
	end if;

	while (ie_comma_index_w > 0) loop
		begin
			ie_status_w := to_number(trim(substr(ds_status_param_value_w, 1, ie_comma_index_w-1)));
		exception
		when others then
			ie_status_w := null;
		end;			
		ie_status_return_w := get_max_status(ie_current_status_p, ie_status_return_w, ie_status_w, ie_prior_w);
		if (ie_status_return_w >= ie_current_status_p) then
			exit;
		end if;		
		ds_status_param_value_w := substr(ds_status_param_value_w, ie_comma_index_w+1, length(ds_status_param_value_w));
		ie_comma_index_w := instr(ds_status_param_value_w, ',');
	end loop;

	if (ie_status_return_w is null or ie_status_return_w < ie_current_status_p) then
		if (ds_status_param_value_w is not null and length(trim(ds_status_param_value_w)) > 0) then
			begin
				ie_status_return_w := get_max_status(ie_current_status_p, ie_status_return_w, to_number(trim(ds_status_param_value_w)), ie_prior_w);
			exception
			when others then
				ie_status_return_w := null;
			end;
		end if;
	end if;

	if (ie_status_return_w = 99) then
		ie_status_return_w := null;
	end if;
	return ie_status_return_w;

end lab_get_next_prior_status;
/
