create or replace
function Obter_Lista_Espera_Estagio
		(nr_sequencia_p		number)
		return			varchar2 is

ds_retorno_w		varchar2(100);

begin

if (nr_sequencia_p is not null) then
	select	nvl(ds_estagio, '')           
	into	ds_retorno_w
	from	AG_LISTA_ESPERA_ESTAGIO
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end Obter_Lista_Espera_Estagio;
/