create or replace
function cpoe_visualizar_itens_cururgia (	cd_perfil_p		number,
										nr_sequencia_p		number,
										ie_tipo_item_p		varchar2,
										nr_atendimento_p	number) return varchar2 is

ie_visualizar_cirurgia_w	cpoe_presentation_rule.ie_visualizar_cirurgia%type;
nr_prescricao_w				prescr_medica.nr_prescricao%type;

begin

if	(cd_perfil_p is not null) then

		select 	nvl(max(ie_visualizar_cirurgia),'S')
		into	ie_visualizar_cirurgia_w
		from	(select 	ie_visualizar_cirurgia
			from 	cpoe_presentation_rule
			where 	nvl(cd_perfil, nvl(cd_perfil_p,0)) = nvl(cd_perfil_p,0)
			and	nvl(cd_estabelecimento, nvl(wheb_usuario_pck.get_cd_estabelecimento,0)) = nvl(wheb_usuario_pck.get_cd_estabelecimento,0)
			order by 	cd_perfil, cd_estabelecimento)
		where 	rownum = 1;
		
		if (nvl(ie_visualizar_cirurgia_w,'N') = 'N') then 	
			nr_prescricao_w := cpoe_obter_prescr_original (nr_sequencia_p,ie_tipo_item_p, nr_atendimento_p);
		
			if (nr_prescricao_w is not null) then
				if (obter_cirurgia_prescricao(nr_prescricao_w) > 0) then
					return 'N';
				end if;
			end if;
		
			return 'S';
		else 
			return 'S';
		end if;
end if;

return	'S';

end cpoe_visualizar_itens_cururgia;
/
