create or replace
function obter_seq_rel_laudo(	nr_prescricao_p		number,
				nr_seq_prescr_p		number)
 		    	return number is

nr_seq_relatorio_w	number(10) := 0;
cd_pessoa_fisica_w	varchar2(10);
cd_setor_atendimento_w	number(10);

			
cursor c01 is 
	select	nr_seq_relatorio
	from	regra_layout_rel_laudo
	where	nvl(cd_pessoa_fisica,cd_pessoa_fisica_w) = cd_pessoa_fisica_w
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_w) = cd_setor_atendimento_w
	and	ie_situacao = 'A'
	order	by cd_pessoa_fisica, cd_setor_atendimento, nr_sequencia;
				
begin


select 	max(u.cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	laudo_paciente l,
	usuario u
where	l.nr_prescricao 	= nr_prescricao_p
and	l.nr_seq_prescricao 	= nr_seq_prescr_p
and	u.nm_usuario		= l.nm_usuario_aprovacao
and	((l.nr_seq_superior is null) or (l.nr_seq_superior = 0));


select	nvl(max(cd_setor_atendimento),0)
into	cd_setor_atendimento_w
from	prescr_procedimento
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nr_seq_prescr_p;

open c01;
loop
fetch c01 into
	nr_seq_relatorio_w;
exit when c01%notfound;
	begin
	
	exit;
	
	end;
end loop;
close c01;


return	nr_seq_relatorio_w;

end obter_seq_rel_laudo;
/