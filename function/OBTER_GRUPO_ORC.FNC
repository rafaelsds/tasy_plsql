create or replace
function obter_grupo_orc ( 	cd_centro_custo_p	number)
			return 		number is
				
cd_grupo_centro_w	number(15);

BEGIN

select	max(a.nr_sequencia)
into	cd_grupo_centro_w
from	ctb_cen_grupo_centro a,
	ctb_cen_centro_grupo b
where	a.nr_sequencia = b.nr_seq_grupo
and	b.cd_centro_custo = cd_centro_custo_p;

return cd_grupo_centro_w ;

END obter_grupo_orc;
/