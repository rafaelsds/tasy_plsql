create or replace function GET_TABLE_NAMES_BY_NODE_PEPA(nr_seq_node_pepa_p	number) return STRARRAY is
  array_tabelas_w STRARRAY := STRARRAY();
begin
    if( nr_seq_node_pepa_p = 1137714 ) then --Odontologia
      array_tabelas_w.extend(3);
      array_tabelas_w(1) := 'EVOLUCAO_PACIENTE';
      array_tabelas_w(2) := 'ODONT_PROCEDIMENTO';
      array_tabelas_w(3) := 'ODONT_CONSULTA';
      
    elsif( nr_seq_node_pepa_p = 1121616 ) then --Diagnostico do tumor
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'CAN_LOCO_REGIONAL';
      
    elsif( nr_seq_node_pepa_p = 1131628 ) then --Templates
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'EHR_REGISTRO';
      
    elsif( nr_seq_node_pepa_p = 1122014 ) then --Orientacoes gerais
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'PEP_ORIENTACAO_GERAL';
      
    elsif( nr_seq_node_pepa_p = 1122052 ) then --Consentimentos
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'PEP_PAC_CI';
      
    elsif( nr_seq_node_pepa_p = 1121568 ) then --Loco regional
      array_tabelas_w.extend(4);
      array_tabelas_w(1) := 'CAN_LOCO_REGIONAL';
      array_tabelas_w(2) := 'CAN_FICHA_ADMISSAO';
      array_tabelas_w(3) := 'CAN_FICHA_SEGUIMENTO';
      array_tabelas_w(4) := 'MED_AVALIACAO_PACIENTE';
      
    elsif( nr_seq_node_pepa_p = 1121748 ) then --Solic externa Exame / Proced / Cirurgia
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'PEDIDO_EXAME_EXTERNO';
      
    elsif( nr_seq_node_pepa_p = 1121004 ) then --Escalas e indices
      array_tabelas_w.extend(11);
      array_tabelas_w(1) := 'ESCALA_DISTRESS';
      array_tabelas_w(2) := 'ESCALA_HUMPTY_DUMPTY';
      array_tabelas_w(3) := 'ESCALA_ECOG';
      array_tabelas_w(4) := 'ESCALA_SILVERMAN';
      array_tabelas_w(5) := 'ESCALA_TOXIDADE';
      array_tabelas_w(6) := 'ESCALA_EIF';
      array_tabelas_w(7) := 'ESCALA_EDMONTON';
      array_tabelas_w(8) := 'ESCALA_KARNOFSKY';
      array_tabelas_w(9) := 'ESCALA_EDMONTON_R';
      array_tabelas_w(10) := 'ESCALA_LEE';
      array_tabelas_w(11) := 'ESCALA_EIF_II';
      
    elsif( nr_seq_node_pepa_p = 1121642 ) then --Diagnosticos
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'DIAGNOSTICO_DOENCA';
      
    elsif( nr_seq_node_pepa_p = 1119722 ) then --Exames laboratoriais
      array_tabelas_w.extend(2);
      array_tabelas_w(1) := 'EXAME_LAB_RESULT_IMAGEM';
      array_tabelas_w(2) := 'PRESCR_PROCED_INF_ADIC';
      
    elsif( nr_seq_node_pepa_p = 1119789 ) then --Exames nao laboratoriais
      array_tabelas_w.extend(6);
      array_tabelas_w(1) := 'MED_LAUDO_CDI';
      array_tabelas_w(2) := 'HEM_MANOMETRIA_COMPLETA';
      array_tabelas_w(3) := 'LAUDO_PACIENTE_MEDIDA';
      array_tabelas_w(4) := 'LAUDO_PACIENTE_IMAGEM';
      array_tabelas_w(5) := 'PRESCR_PROCED_INF_ADIC';
      array_tabelas_w(6) := 'LAUDO_PACIENTE_COPIA';
      
    elsif( nr_seq_node_pepa_p = 1119954 ) then --Evolucoes 
      array_tabelas_w.extend(2);
      array_tabelas_w(1) := 'EVOLUCAO_PACIENTE';
      array_tabelas_w(2) := 'LISTA_PROBLEMA_PAC';
      
    elsif( nr_seq_node_pepa_p = 1137149 ) then --Cirurgias
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'CIRURGIA';
      
    elsif( nr_seq_node_pepa_p = 1131556 ) then --Avaliacoes
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'MED_AVALIACAO_PACIENTE';
      
    elsif( nr_seq_node_pepa_p = 1119298 ) then --Sinais vitais e monitorizacao
      array_tabelas_w.extend(12);
      array_tabelas_w(1) := 'ATENDIMENTO_MARCACAO';
      array_tabelas_w(2) := 'CIRCULACAO_EXTRACORPOREA';
      array_tabelas_w(3) := 'ATEND_URO_CHOICE';
      array_tabelas_w(4) := 'ATEND_UROCOLOR';
      array_tabelas_w(5) := 'ATEND_UROANALISE';
      array_tabelas_w(6) := 'ATENDIMENTO_SINAL_VITAL';
      array_tabelas_w(7) := 'ATEND_AVAL_ANALGESIA';
      array_tabelas_w(8) := 'ATENDIMENTO_MONIT_RESP';
      array_tabelas_w(9) := 'ATEND_BIOIMPEDANCIA';
      array_tabelas_w(10) := 'ATEND_INFORMACAO_LEITO';
      array_tabelas_w(11) := 'ATEND_MONIT_HEMOD';
      array_tabelas_w(12) := 'ATEND_ANAL_BIOQ_PORT';
      
    elsif( nr_seq_node_pepa_p = 1137949 ) then --Avaliacao nutricional
      array_tabelas_w.extend(2);
      array_tabelas_w(1) := 'AVAL_NUTRICAO_PED';
      array_tabelas_w(2) := 'AVAL_NUTRICAO';
      
    elsif( nr_seq_node_pepa_p = 1121714 ) then --Parecer medico
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'PARECER_MEDICO_REQ';
      
    elsif( nr_seq_node_pepa_p = 1136403 ) then --Feridas e curativos
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'CUR_FERIDA';
      
    elsif( nr_seq_node_pepa_p = 1121975 ) then --Receitas
      array_tabelas_w.extend(3);
      array_tabelas_w(1) := 'MEDIC_USO_CONTINUO';
      array_tabelas_w(2) := 'MED_RECEITA';
      array_tabelas_w(3) := 'FA_RECEITA_FARMACIA';
      
    elsif( nr_seq_node_pepa_p = 1122033 ) then --Atestados
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'ATESTADO_PACIENTE';
      
    elsif( nr_seq_node_pepa_p = 1171605 ) then --Anamnese
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'ANAMNESE_PACIENTE';
      
    elsif( nr_seq_node_pepa_p = 1120111 ) then --Historico de saude
      array_tabelas_w.extend(24);
      array_tabelas_w(1) := 'EXPLORACAO_FISICA';
      array_tabelas_w(2) := 'PACIENTE_ALERGIA';
      array_tabelas_w(3) := 'HISTORICO_SAUDE_TRATAMENTO';
      array_tabelas_w(4) := 'PACIENTE_OCORRENCIA';
      array_tabelas_w(5) := 'PACIENTE_HIST_SOCIAL';
      array_tabelas_w(6) := 'PACIENTE_TRANSFUSAO';
      array_tabelas_w(7) := 'PLS_DECLARACAO_SEGURADO';
      array_tabelas_w(8) := 'PACIENTE_ANTEC_CLINICO';
      array_tabelas_w(9) := 'PACIENTE_PFLEGEGRAD';
      array_tabelas_w(10) := 'INTERROGATORIO_APARELHO';
      array_tabelas_w(11) := 'PACIENTE_ANTEC_SEXUAIS';
      array_tabelas_w(12) := 'PACIENTE_HABITO_VICIO';
      array_tabelas_w(13) := 'HISTORICO_SAUDE_INTERNACAO';
      array_tabelas_w(14) := 'PACIENTE_REP_PRESCRICAO';
      array_tabelas_w(15) := 'PF_TIPO_DEFICIENCIA';
      array_tabelas_w(16) := 'PACIENTE_AMPUTACAO';
      array_tabelas_w(17) := 'PACIENTE_ACESSORIO';
      array_tabelas_w(18) := 'PACIENTE_EXAME';
      array_tabelas_w(19) := 'HISTORICO_SAUDE_CIRURGIA';
      array_tabelas_w(20) := 'CIH_PAC_FAT_RISCO';
      array_tabelas_w(21) := 'PACIENTE_VACINA';
      array_tabelas_w(22) := 'HISTORICO_SAUDE_MULHER';
      array_tabelas_w(23) := 'PACIENTE_VULNERABILIDADE';
      array_tabelas_w(24) := 'PACIENTE_MEDIC_USO';
      
    elsif( nr_seq_node_pepa_p = 1140898 ) then --Orientacoes de alta
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'ATENDIMENTO_ALTA';
      
    elsif( nr_seq_node_pepa_p = 1121770 ) then --Tratamento oncologico
      array_tabelas_w.extend(9);
      array_tabelas_w(1) := 'RXT_TUMOR';
      array_tabelas_w(2) := 'PACIENTE_PROTOCOLO_MEDIC';
      array_tabelas_w(3) := 'PACIENTE_PROTOCOLO_SOLUC';
      array_tabelas_w(4) := 'PACIENTE_PROTOCOLO_PROC';
      array_tabelas_w(5) := 'PACIENTE_PROTOCOLO_REC';
      array_tabelas_w(6) := 'PACIENTE_PROT_MEDIC_HIST';
      array_tabelas_w(7) := 'PACIENTE_ATENDIMENTO';
      array_tabelas_w(8) := 'PACIENTE_SETOR_LIB';
      array_tabelas_w(9) := 'PACIENTE_PROT_MEDIC_HIST';

    elsif( nr_seq_node_pepa_p = 1140499 ) then --Obstetricia/Nascimentos
      array_tabelas_w.extend(16);
      array_tabelas_w(1) := 'NASCIMENTO';
      array_tabelas_w(2) := 'PARTO_EVENTOS';
      array_tabelas_w(3) := 'PARTO_SOROLOGIA';
      array_tabelas_w(4) := 'POST_NATAL_OBSTERTRICS';
      array_tabelas_w(5) := 'PARTO';
      array_tabelas_w(6) := 'PARTO_PROG_ESP';
      array_tabelas_w(7) := 'MED_PAC_PRE_NATAL';
      array_tabelas_w(8) := 'PRE_NATAL';
      array_tabelas_w(9) := 'NASCIMENTO_IMAGEM';
      array_tabelas_w(10) := 'ATENDIMENTO_GRAVIDEZ';
      array_tabelas_w(11) := 'ENF_TRI_OBSTETRICA';
      array_tabelas_w(12) := 'ENF_TRI_MEDICA';
      array_tabelas_w(13) := 'PRE_PREGNANCY_INFO';
      array_tabelas_w(14) := 'PARTO_PARTICIPANTE';
      array_tabelas_w(15) := 'PARTO_EXAMES_ANTERIORES';
      array_tabelas_w(16) := 'PARTOGRAMA';
      
    elsif( nr_seq_node_pepa_p = 1131396 ) then --Justificativas / Solicitacoes
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'PACIENTE_JUSTIFICATIVA';
      
    elsif( nr_seq_node_pepa_p = 1131267 ) then --Alertas
      array_tabelas_w.extend(3);
      array_tabelas_w(1) := 'ATENDIMENTO_ALERTA';
      array_tabelas_w(2) := 'ALERTA_PACIENTE';
      array_tabelas_w(3) := 'EV_EVENTO_PACIENTE';
      
    elsif( nr_seq_node_pepa_p = 1131515 ) then --Gestao eletronica de documentos GED
      array_tabelas_w.extend(2);
      array_tabelas_w(1) := 'GED_ATENDIMENTO';
      array_tabelas_w(2) := 'ATENDIMENTO_PACIENTE_ANEXO';
      
    elsif( nr_seq_node_pepa_p = 1141060 ) then --Procedimentos
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'PROC_PAC_DESCRICAO';
      
    elsif( nr_seq_node_pepa_p = 1140387 ) then --Lista de problemas
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'LISTA_PROBLEMA_PAC';
      
    elsif( nr_seq_node_pepa_p = 1122111 ) then --Eventos adversos
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'EVENTOS_ADVERSOS_REGISTRO';
      
    elsif( nr_seq_node_pepa_p = 1139208 ) then --Confidencias
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'MED_CONFIDENCIA';
      
    elsif( nr_seq_node_pepa_p = 1139662 ) then --Encaminhamentos
      array_tabelas_w.extend(3);
      array_tabelas_w(1) := 'ATEND_PROGRAMA_SAUDE';
      array_tabelas_w(2) := 'ATEND_ENCAMINHAMENTO';
      array_tabelas_w(3) := 'PACIENTE_HOME_CARE';
      
    elsif( nr_seq_node_pepa_p = 1137463 ) then --CIAP
      array_tabelas_w.extend(1);
      array_tabelas_w(1) := 'CIAP_ATENDIMENTO';
      
    end if;
  return array_tabelas_w;
end GET_TABLE_NAMES_BY_NODE_PEPA;
/