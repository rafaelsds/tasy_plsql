CREATE OR REPLACE
FUNCTION Obter_Regra_Atend_Medico(
		nr_atendimento_p	number,
		cd_medico_p		Varchar2)
		RETURN VARCHAR2 IS

ie_pasta_w			Varchar2(03)	:= null;
cd_setor_atendimento_w	Number(05,0);
ie_clinica_w			Number(05,0);
cd_estabelecimento_w		Number(05,0);
cd_pessoa_fisica_w		Varchar2(10);
qt_atendimento_w		Number(05,0);

cursor c01 is
select ie_pasta
from 	Regra_Atend_medico
where	cd_estabelecimento						= cd_estabelecimento_w	
  and	nvl(cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w
  and	nvl(ie_clinica, ie_clinica_w)				= ie_clinica_w
  and	nvl(cd_medico, cd_medico_p)					= cd_medico_p
  and ((qt_atendimento_w > 1) or (ie_prim_atend = 'N'))
  and	qt_atendimento_w	> 0
order by 
	nvl(cd_setor_atendimento,0),
	nvl(ie_clinica,0),
	nvl(cd_medico,0);

BEGIN

select	max(cd_estabelecimento),
	max(ie_clinica),
	max(cd_setor_atendimento),
	max(cd_pessoa_fisica)
into	cd_estabelecimento_w,
	ie_clinica_w,
	cd_setor_atendimento_w,
	cd_pessoa_fisica_w
from	atendimento_paciente_v
where	nr_atendimento	= nr_atendimento_p;

select count(*)
into	qt_atendimento_w
from	atendimento_paciente
where	cd_pessoa_fisica	= nvl(cd_pessoa_fisica_w,0);

OPEN C01;
LOOP
	FETCH C01 into
		ie_pasta_w;
	exit 	when c01%notfound;
		ie_pasta_w		:= ie_pasta_w;
END LOOP;
CLOSE C01;

RETURN nvl(ie_pasta_w,'E');

END Obter_Regra_Atend_Medico;
/
