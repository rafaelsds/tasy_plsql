create or replace
function obter_se_painel_agenda_perfil(	cd_perfil_p		number,
					cd_agenda_p		number
				      )
 		    	return varchar2 is

ds_retorno_w	varchar2(1) := 'N';
qt_agendas_w	number(10)  := 0;
			
begin

select 	count(*)
into	qt_agendas_w
from	regra_lib_agenda_painel
where	cd_agenda = cd_agenda_p;

if (qt_agendas_w > 0) then

	select 	count(*)
	into	qt_agendas_w
	from	regra_lib_agenda_painel
	where	cd_perfil = cd_perfil_p
	and	cd_agenda = cd_agenda_p;


	if (qt_agendas_w > 0) then
			
		ds_retorno_w := 'S';
		
	end if;

else
	ds_retorno_w := 'S';

end if;

return	ds_retorno_w;

end obter_se_painel_agenda_perfil;
/
