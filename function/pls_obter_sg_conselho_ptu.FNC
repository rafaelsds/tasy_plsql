create or replace
function pls_obter_sg_conselho_ptu (cd_medico_p		varchar2)
 		    	return varchar2 is

sg_conselho_w		varchar2(255);
nr_seq_conselho_w	number(10,0);

begin

select	nvl(max(nr_seq_conselho),0)
into	nr_seq_conselho_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_medico_p;

select	nvl(max(cd_ptu),'')
into	sg_conselho_w
from	conselho_profissional
where	nr_sequencia	= nr_seq_conselho_w;

return	sg_conselho_w;

end pls_obter_sg_conselho_ptu;
/