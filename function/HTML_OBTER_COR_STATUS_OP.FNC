create or replace
function html_obter_cor_status_op(nr_seq_status_op_p	number)
			return number is

/*
This function is used HTML platform
If you need to make any change to this function,
you'll gonna need to change the function "OBTER_COR_STATUS_OP" also,
that is used in Delphi and Java platforms
*/

nr_seq_retorno_w	legenda_ordem_producao_opm.nr_sequencia%type;

begin
if	(nvl(nr_seq_status_op_p,0) > 0) then
	select	nvl(max(lop.nr_sequencia),0)
	into	nr_seq_retorno_w
	from 	status_legenda_op_opm slo,
		legenda_ordem_producao_opm lop 
	where 	slo.nr_seq_status = nr_seq_status_op_p
	and 	slo.nr_seq_legenda = lop.nr_sequencia
	and 	nvl(slo.ie_situacao,'A') = 'A'
	and 	nvl(lop.ie_situacao,'A') = 'A';
end if;

return nr_seq_retorno_w;

end html_obter_cor_status_op;
/