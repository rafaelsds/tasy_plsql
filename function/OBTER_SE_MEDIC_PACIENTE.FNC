create or replace
function obter_se_medic_paciente(	nr_prescricao_p		NUMBER)

					return VARCHAR2 is
ds_retorno_w 	VARCHAR2(1);
			
begin

select 	nvl(max('S'),'N')
into	ds_retorno_w
from	prescr_material 
where	nr_prescricao = nr_prescricao_p
and	ie_medicacao_paciente = 'S'
and	ie_suspenso <> 'S';

return	ds_retorno_w;

end obter_se_medic_paciente;
/

