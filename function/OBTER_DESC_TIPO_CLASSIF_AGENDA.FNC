create or replace
function 	obter_desc_tipo_classif_agenda  (nr_seq_classif_p		number)
					         return varchar2 is

ie_tipo_classif_w		varchar2(01);
ds_retorno_w			varchar2(255);

begin

if	(nvl(nr_seq_classif_p,0) > 0) then
	select	max(ie_tipo_classif)
	into	  ie_tipo_classif_w
	from 	  agenda_paciente_classif
	where	  nr_sequencia	= nr_seq_classif_p;
	
	
	if (ie_tipo_classif_w = 'N') then
		ds_retorno_w := wheb_mensagem_pck.get_texto(308108); -- Normal
	elsif (ie_tipo_classif_w = 'E') then
		ds_retorno_w := wheb_mensagem_pck.get_texto(87462); -- Encaixe
	elsif (ie_tipo_classif_w = 'R') then
		ds_retorno_w := wheb_mensagem_pck.get_texto(308113); -- Retorno Consulta
	elsif (ie_tipo_classif_w = 'C') then
		ds_retorno_w := wheb_mensagem_pck.get_texto(308114); -- Retorno cirurgia
	elsif (ie_tipo_classif_w = 'X') then
		ds_retorno_w := wheb_mensagem_pck.get_texto(308115); -- Retorno Exame
	elsif (ie_tipo_classif_w = 'P') then
		ds_retorno_w := wheb_mensagem_pck.get_texto(308116); -- P�s operat�rio
	end if;
	
end if;

return	ds_retorno_w;

end obter_desc_tipo_classif_agenda;
/