create or replace
function Obter_Se_Avaliacao_Perfil_Vis
		(cd_perfil_p		number,
		nr_seq_avaliacao_p	number,
		cd_setor_atendimento_p	number)
		return varchar2 is

ie_perfil_w			varchar2(01) := 'S';
qt_perfil_w			number(10,0);	
cd_estabelecimento_w		number(10);
nm_usuario_w			varchar2(15);
ie_tipo_evolucao_w		varchar2(15);	
nr_seq_agrupamento_w		number(10,0);

Cursor C01 is
	select	nvl(IE_PERMITE_VISUALIZAR,'S')
	from	med_perfil_avaliacao
	where	nr_seq_tipo_aval	= nr_seq_avaliacao_p
	and	nvl(cd_perfil,cd_perfil_p)		= cd_perfil_p
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_p) = cd_setor_atendimento_p
	and	nvl(cd_estabelecimento,cd_estabelecimento_w)	= cd_estabelecimento_w
	and	nvl(ie_tipo_evolucao,ie_tipo_evolucao_w) = ie_tipo_evolucao_w
	order by nvl(cd_perfil,0),
			nvl(cd_setor_atendimento,0),
			nvl(cd_estabelecimento,0),
			nvl(ie_tipo_evolucao,0);
begin

select	count(*)
into	qt_perfil_w
from	med_perfil_avaliacao
where	nr_seq_tipo_aval	= nr_seq_avaliacao_p;
cd_estabelecimento_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
nm_usuario_w		:= wheb_usuario_pck.get_nm_usuario;

select	nvl(max(ie_tipo_evolucao),'xpto')
into	ie_tipo_evolucao_w
from 	usuario
where	nm_usuario = nm_usuario_w;

select	nvl(max(nr_seq_agrupamento),0)
into	nr_seq_agrupamento_w
from 	setor_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_p;

if	(qt_perfil_w > 0) then
	begin
	ie_perfil_w	:= 'N';
	open C01;
	loop
	fetch C01 into	
		ie_perfil_w;
	exit when C01%notfound;
	end loop;
	close C01;
	end;
end if;

return	ie_perfil_w;

end Obter_Se_Avaliacao_Perfil_Vis;
/