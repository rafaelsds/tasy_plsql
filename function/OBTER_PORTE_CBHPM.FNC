create or replace
function obter_porte_cbhpm
		(dt_referencia_p	date,
		cd_procedimento_p	number,
		ie_origem_proced_p	number)
		return varchar2 is

vl_custo_operacional_w			number(15,2);
vl_medico_w				number(15,2);
vl_anestesista_w			number(15,2);
cd_porte_w				varchar2(10);
tx_porte_w				number(15,4);
qt_uco_w				number(15,4);
nr_porte_anest_w			number(3);
nr_auxiliar_w				number(3);
qt_filme_w				number(15,4);
qt_incidencia_w				number(15,4);
ie_unid_ra_w				varchar2(1);
vl_porte_w				number(15,2);
dt_vigencia_porte_w			date;
dt_vigencia_preco_w			date;
cd_porte_anestesista_w			varchar2(8);
vl_porte_anestesista_w			number(15,2);

begin

obter_preco_cbhpm(null, dt_referencia_p,cd_procedimento_p,ie_origem_proced_p,null,0,null,null,vl_medico_w,
		cd_porte_w,tx_porte_w,qt_uco_w,nr_porte_anest_w,nr_auxiliar_w,
		qt_filme_w,qt_incidencia_w,ie_unid_ra_w,vl_porte_w,dt_vigencia_porte_w,
		dt_vigencia_preco_w,vl_porte_anestesista_w, null, null,null);

return cd_porte_w;

end obter_porte_cbhpm;
/