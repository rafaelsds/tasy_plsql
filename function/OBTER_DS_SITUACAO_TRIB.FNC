create or replace
function obter_ds_situacao_trib(nr_seq_trib_prest_p	number)
 		    	return varchar2 is

ds_situacao_w	varchar2(255);
begin
ds_situacao_w := '';

if	(nvl(nr_seq_trib_prest_p,'0') <> '0') then

	select	max(ds_situacao)
	into	ds_situacao_w
	from	situacao_trib_prest_serv
	where	nr_sequencia= nr_seq_trib_prest_p;
	
end if;

return	ds_situacao_w;

end obter_ds_situacao_trib;
/