create or replace 
function Obter_Se_Col_Igual(
			ds_dono_p		Varchar2,
			nm_tabela_p		Varchar2,		
			nm_integridade_p	Varchar2,
			nm_indice_p		Varchar2,
			ie_opcao_p		Varchar2)
			return varchar2 is

ds_retorno_w		varchar2(1);
qt_ind_w		Number(15,0);
qt_cons_w		Number(15,0);
	
BEGIN

if	(ie_opcao_p = 'D') then
	select 	count(*) 
	into	qt_ind_w
	from	indice_atributo
	where	nm_tabela 	= nm_tabela_p
	and	nm_indice	= nm_indice_p;
	
	select	count(*) 
	into	qt_cons_w	
	from	integridade_referencial
	where	nm_tabela 			= nm_tabela_p
	and	nm_integridade_referencial	= nm_integridade_p;
else
	select 	count(*) 
	into	qt_ind_w
	from 	user_ind_columns
	where	table_name 	= nm_tabela_p
	and	index_name	= nm_indice_p;
	
	select	count(*) 
	into	qt_cons_w	
	from	user_cons_columns
	where	table_name	= nm_tabela_p
	and	constraint_name	= nm_integridade_p;
end if;

if	(qt_ind_w = qt_cons_w) then
	ds_retorno_w		:= 'S';
else
	ds_retorno_w		:= 'S';
end if;
return	ds_retorno_w;

END Obter_se_col_igual;
/
