create or replace function OBTER_STATUS_BOMBA_INFUSAO(NR_PRESCRICAO_P   bomba_infusao.nr_prescricao%type,
                                                      NR_ATENDIMENTO_P  bomba_infusao.nr_atendimento%type,
                                                      NR_SEQ_MAT_CPOE_P bomba_infusao.nr_seq_mat_cpoe%type default null)
  return varchar2 is

  oResult         varchar2(500);
  vStatusDS       number;
  vStatusST       number;
  vStatusCN       number;
  vIeQuestionavel number;

begin

  if (trim(NR_SEQ_MAT_CPOE_P) is not null) then
  
    begin
      with status_pump as
       (select case
                 when bii.ie_status in ('DS') then
                  count(bii.ie_status)
               end ie_status_ds,
               case
                 when bii.ie_status in ('ST') then
                  count(bii.ie_status)
               end ie_status_st,
               case
                 when bii.ie_status in ('IP', 'IS', 'BL', 'TC', 'KV', 'KT') then
                  count(bii.ie_status)
               end ie_status_cn,
               case
                 when nvl(bii.ie_estado_questionavel, 'N') = 'S' then
                  count(bii.ie_estado_questionavel)
               end ie_questionavel
          from bomba_infusao bi, bomba_infusao_interface bii
         where bi.ie_ativo = 'S'
           and bi.nr_seq_mat_cpoe = NR_SEQ_MAT_CPOE_P
           and bi.nr_seq_bomba_interface = bii.nr_sequencia
         group by bii.ie_status, bii.ie_estado_questionavel)
      select sum(nvl(status_pump.ie_status_ds, 0)) ie_status_ds,
             sum(nvl(status_pump.ie_status_st, 0)) ie_status_st,
             sum(nvl(status_pump.ie_status_cn, 0)) ie_status_cn,
             sum(nvl(status_pump.ie_questionavel, 0)) ie_questionavel
        into vstatusds, vstatusst, vstatuscn, viequestionavel
        from status_pump;
    end;
  
  else
  
    with status_pump as
     (select case
               when bii.ie_status in ('DS') then
                count(bii.ie_status)
             end ie_status_ds,
             case
               when bii.ie_status in ('ST') then
                count(bii.ie_status)
             end ie_status_st,
             case
               when bii.ie_status in ('IP', 'IS', 'BL', 'TC', 'KV', 'KT') then
                count(bii.ie_status)
             end ie_status_cn,
             case
               when nvl(bii.ie_estado_questionavel, 'N') = 'S' then
                count(bii.ie_estado_questionavel)
             end ie_questionavel
        from bomba_infusao bi, bomba_infusao_interface bii
       where bi.ie_ativo = 'S'
         and bi.nr_prescricao = NR_PRESCRICAO_P
         and bi.nr_atendimento = NR_ATENDIMENTO_P
         and bi.nr_seq_bomba_interface = bii.nr_sequencia
       group by bii.ie_status, bii.ie_estado_questionavel)
    select sum(nvl(status_pump.ie_status_ds, 0)) ie_status_ds,
           sum(nvl(status_pump.ie_status_st, 0)) ie_status_st,
           sum(nvl(status_pump.ie_status_cn, 0)) ie_status_cn,
           sum(nvl(status_pump.ie_questionavel, 0)) ie_questionavel
      into vstatusds, vstatusst, vstatuscn, viequestionavel
      from status_pump;
  
  end if;

  if (vStatusCN > 0 and vStatusDS = 0 and vStatusST = 0) and
     vIeQuestionavel = 0 then
    oResult := 'CN';
  elsif vStatusDS > 0 and vIeQuestionavel = 0 then
    oResult := 'DS';
  elsif (vStatusST > 0 and vStatusDS = 0 and vStatusCN >= 0) and
        vIeQuestionavel = 0 then
    oResult := 'NI';
  elsif vIeQuestionavel > 0 then
    oResult := 'QN';
  end if;

  return(oResult);

end OBTER_STATUS_BOMBA_INFUSAO;
/
