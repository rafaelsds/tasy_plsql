create or replace 
function	obter_inf_temp_conteudo(	nr_sequencia_p	number)
		return varchar2 is

nr_seq_elem_sup_w	varchar2(255);

begin

select	nr_seq_elem_sup
into	nr_seq_elem_sup_w
from	ehr_template_conteudo
where	nr_sequencia = nr_sequencia_p;

return	nr_seq_elem_sup_w;

end;
/
