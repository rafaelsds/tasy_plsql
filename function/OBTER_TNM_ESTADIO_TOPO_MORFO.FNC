create or replace
function Obter_TNM_Estadio_topo_morfo(cd_pessoa_fisica_p	varchar2,
					cd_estabelecimento_p number,
					nr_sequencia_p       number,
					ie_opcao_p	 varchar2)
					return varchar2 is

ds_retorno_w			Varchar2(20);
cd_estadiamento_w		Varchar2(20);
cd_estadio_outro_w		varchar2(20);
cd_tumor_primario_w		can_loco_regional.cd_tumor_primario%type;
cd_linfonodo_regional_w		can_loco_regional.cd_linfonodo_regional%type;
cd_metastase_distancia_w	can_loco_regional.cd_metastase_distancia%type;
cd_tumor_prim_pat_w		can_loco_regional.cd_tumor_prim_pat%type;
cd_linfonodo_reg_pat_w		can_loco_regional.cd_linfonodo_reg_pat%type;
cd_metastase_dist_pat_w		can_loco_regional.cd_metastase_dist_pat%type;
cd_topografia_w			Varchar2(10);
cd_morfologia_w			Varchar2(10);

begin

select	cd_estadiamento,
	cd_tumor_primario,
	cd_linfonodo_regional,
	cd_metastase_distancia,
	cd_topografia,
	cd_morfologia,
	cd_tumor_prim_pat,
	cd_linfonodo_reg_pat,
	cd_metastase_dist_pat,
	cd_estadio_outro
into	cd_estadiamento_w,
	cd_tumor_primario_w,
	cd_linfonodo_regional_w,
	cd_metastase_distancia_w,
	cd_topografia_w,
	cd_morfologia_w,
	cd_tumor_prim_pat_w,
	cd_linfonodo_reg_pat_w,
	cd_metastase_dist_pat_w,
	cd_estadio_outro_w
from	can_loco_regional
where	nr_sequencia	= 	(select	b.nr_sequencia
				from	can_loco_regional b
				where	b.cd_estabelecimento = cd_estabelecimento_p
					and	b.cd_pessoa_fisica	 = cd_pessoa_fisica_p
					and b.nr_sequencia  	 = nr_sequencia_p);

if	(ie_opcao_p = 'E') then
	ds_retorno_w	:= cd_estadiamento_w;
elsif	(ie_opcao_p = 'T') then
	ds_retorno_w	:= cd_tumor_primario_w;
elsif	(ie_opcao_p = 'N') then
	ds_retorno_w	:= cd_linfonodo_regional_w;
elsif	(ie_opcao_p = 'M') then
	ds_retorno_w	:= cd_metastase_distancia_w;
elsif	(ie_opcao_p = 'pE') then
	ds_retorno_w	:= cd_estadio_outro_w;
elsif	(ie_opcao_p = 'pT') then
	ds_retorno_w	:= cd_tumor_prim_pat_w;
elsif	(ie_opcao_p = 'pN') then
	ds_retorno_w	:= cd_linfonodo_reg_pat_w;
elsif	(ie_opcao_p = 'pM') then
	ds_retorno_w	:= cd_metastase_dist_pat_w;
elsif	(ie_opcao_p = 'TO') then
	ds_retorno_w	:= cd_topografia_w;
elsif	(ie_opcao_p = 'MO') then
	ds_retorno_w	:= cd_morfologia_w;
end if;

return ds_retorno_w;

end Obter_TNM_Estadio_topo_morfo;
/