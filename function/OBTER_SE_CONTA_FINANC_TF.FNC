create or replace
function OBTER_SE_CONTA_FINANC_TF
	(nr_seq_trans_financ_p	number,
	 cd_conta_financ_p	number) return varchar2 is

ds_retorno_w	varchar2(255);
cont_w		number(10,0);
qt_liberado_w	number(10);

begin

select	count(*)
into	qt_liberado_w
from	trans_financ_conta_fin a
where	a.nr_seq_trans_financ	= nr_seq_trans_financ_p;

if	(qt_liberado_w > 0) then

	select	count(*)
	into	cont_w
	from	trans_financ_conta_fin
	where	cd_conta_financ		= cd_conta_financ_p
	and	nr_seq_trans_financ	= nr_seq_trans_financ_p;

end if;

ds_retorno_w			:= 'N';
if	(qt_liberado_w = 0) or (cont_w > 0) then
	ds_retorno_w		:= 'S';
end if;

return	ds_retorno_w;

end OBTER_SE_CONTA_FINANC_TF;
/