CREATE OR REPLACE FUNCTION obter_icone_apap (
    ie_grupo_p    IN  VARCHAR2,
    nm_usuario_p  IN  VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 AS
    ds_caminho_w w_apap_legenda.ds_caminho%TYPE;
BEGIN
    SELECT
        MAX(wal.ds_caminho)
    INTO ds_caminho_w
    FROM
        w_apap_legenda wal
    WHERE 1 = 1
        AND wal.ie_grupo = ie_grupo_p
        AND wal.ie_profissional_medico = nvl2(nm_usuario_p,obter_se_usuario_medico(nm_usuario_p),'N');
    RETURN ds_caminho_w;
END obter_icone_apap;
/
