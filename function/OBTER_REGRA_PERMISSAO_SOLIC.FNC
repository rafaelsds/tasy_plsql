create or replace
function obter_regra_permissao_solic(
			cd_local_estoque_p	number,
			cd_centro_custo_p		number,
			cd_perfil_p		number,
			nm_usuario_p		varchar2,
			cd_material_p		number,
			ie_urgente_p		varchar2,
			dt_solic_compra_p		date,
			cd_setor_entrega_p		number,
			cd_estabelecimento_p	number,
			ie_tipo_servico_p	varchar2)
return varchar2 is


cd_grupo_material_w		number(3);
cd_subgrupo_material_w		number(3);
cd_classe_material_w		number(5);
cd_local_estoque_w		number(10);
cd_centro_custo_w			number(8);
qt_registro_w			number(10);
ie_padronizado_w		varchar2(1);
ie_permissao_w			varchar2(1) := 'S';
ds_retorno_w			varchar2(1) := 'S';

Cursor C01 is
select	nvl(a.ie_permissao,'S')
from	regra_permissao_solic a
where	nvl(cd_centro_custo,nvl(cd_centro_custo_p, 0))		= nvl(cd_centro_custo_p, 0)
and	nvl(cd_local_estoque,nvl(cd_local_estoque_p, 0))	= nvl(cd_local_estoque_p, 0)
and	nvl(cd_perfil,cd_perfil_p)				= cd_perfil_p
and	nvl(nm_usuario_regra,nm_usuario_p) 			= nm_usuario_p
and	nvl(a.cd_grupo_material,cd_grupo_material_w) 		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material,cd_classe_material_w) 		= cd_classe_material_w
and	nvl(a.cd_material,cd_material_p) 			= cd_material_p
and	nvl(a.cd_setor_entrega,nvl(cd_setor_entrega_p,0))		= nvl(cd_setor_entrega_p,0)
and	nvl(a.cd_estabelecimento,nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0)
and	((a.ie_padronizado = ie_padronizado_w) or (nvl(a.ie_padronizado, 'A') = 'A'))
and	((a.ie_urgente = ie_urgente_p) or (nvl(a.ie_urgente, 'A') = 'A'))
and	((ie_dia_semana is null) or
	((ie_dia_semana is not null) and (ie_dia_semana = 9) and  (substr(obter_cod_dia_semana(dt_solic_compra_p), 1,1) in (2,3,4,5,6))) or
	((ie_dia_semana is not null) and (ie_dia_semana <> 9) and (substr(obter_cod_dia_semana(dt_solic_compra_p), 1,1) = ie_dia_semana)))
and	(to_char(hr_inicio,'hh24:mi:ss') < to_char(dt_solic_compra_p,'hh24:mi:ss') or
	hr_inicio is null)
and	(to_char(hr_fim,'hh24:mi:ss') > to_char(dt_solic_compra_p,'hh24:mi:ss') or 
	hr_fim is null)
and	(((dt_inicio is null) or (dt_fim is null)) or
	(to_char(dt_solic_compra_p,'dd/mm') between to_char(dt_inicio,'dd/mm') and to_char(dt_fim,'dd/mm')))
and	(to_char(lpad(nr_dia_fixo_mes,2,0)) = to_char(dt_solic_compra_p,'dd') or 
	nr_dia_fixo_mes is null)
and	((nvl(nr_dia_inicio,0) = 0) or
	(nvl(nr_dia_fim,0) = 0) or
	(to_number(to_char(dt_solic_compra_p,'dd')) between nr_dia_inicio and nr_dia_fim))	
and	(cd_material_p <> 0)
and	((cd_grupo_material is not null) or
	 (cd_subgrupo_material is not null) or
	 (cd_classe_material is not null) or
	 (cd_material is not null) or
	 (ie_padronizado in ('S','N')))
and 	nvl(a.ie_tipo_servico,nvl(ie_tipo_servico_p,'ES')) = nvl(ie_tipo_servico_p,'ES')
order by	nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(nm_usuario_regra,'aaaa'),
		nvl(cd_perfil,0);
begin

if	(nvl(cd_material_p, 0) = 0) then
	select	count(*)
	into	qt_registro_w
	from	regra_permissao_solic
	where	nvl(cd_centro_custo,nvl(cd_centro_custo_p, 0))		= nvl(cd_centro_custo_p, 0)
	and	nvl(cd_local_estoque,nvl(cd_local_estoque_p, 0))	= nvl(cd_local_estoque_p, 0)
	and	nvl(cd_perfil,cd_perfil_p)				= cd_perfil_p
	and	nvl(nm_usuario_regra,nm_usuario_p) 			= nm_usuario_p
	and	nvl(cd_setor_entrega,nvl(cd_setor_entrega_p,0))		= nvl(cd_setor_entrega_p,0)
	and	nvl(cd_estabelecimento,nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0)
	and	((ie_padronizado = ie_padronizado_w) or (nvl(ie_padronizado, 'A') = 'A'))
	and	((ie_urgente = ie_urgente_p) or (nvl(ie_urgente, 'A') = 'A'))
	and	((ie_dia_semana is null) or
		((ie_dia_semana is not null) and (ie_dia_semana = 9) and  (substr(obter_cod_dia_semana(dt_solic_compra_p), 1,1) in (2,3,4,5,6))) or
		((ie_dia_semana is not null) and (ie_dia_semana <> 9) and (substr(obter_cod_dia_semana(dt_solic_compra_p), 1,1) = ie_dia_semana)))
	and	(to_char(hr_inicio,'hh24:mi:ss') < to_char(dt_solic_compra_p,'hh24:mi:ss') or
		hr_inicio is null)
	and	(to_char(hr_fim,'hh24:mi:ss') > to_char(dt_solic_compra_p,'hh24:mi:ss') or
		hr_fim is null)
	and	(((dt_inicio is null) or (dt_fim is null)) or
		(to_char(dt_solic_compra_p,'dd/mm') between to_char(dt_inicio,'dd/mm') and to_char(dt_fim,'dd/mm')))
	and	(to_char(lpad(nr_dia_fixo_mes,2,0)) = to_char(dt_solic_compra_p,'dd') or 
		nr_dia_fixo_mes is null)	
	and	((nvl(nr_dia_inicio,0) = 0) or
		(nvl(nr_dia_fim,0) = 0) or
		(to_number(to_char(dt_solic_compra_p,'dd')) between nr_dia_inicio and nr_dia_fim))		
	and	ie_permissao = 'N'
	and	cd_grupo_material is null
	and	cd_subgrupo_material is null
	and	cd_classe_material is null
	and	cd_material is null; 	

elsif	(nvl(cd_material_p,0) <> 0) then
	begin
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		obter_se_material_padronizado(cd_estabelecimento_p, cd_material)
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		ie_padronizado_w
	from	estrutura_material_v
	where	cd_material	= cd_material_p;

	open C01;
	loop
	fetch C01 into
		ie_permissao_w;
	exit when C01%notfound;
		begin
		ds_retorno_w	:= ie_permissao_w;
		end;
	end loop;
	close C01;
	end;

end if;

if	(qt_registro_w > 0) then
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end obter_regra_permissao_solic;
/
