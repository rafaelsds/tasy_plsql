create or replace
function pls_obter_se_proc_cpt_pac
			(	cd_procedimento_p		Number,
				ie_origem_proced_p		Number,
				ie_opcao_p			Varchar2)
 		    		return Varchar2 is

ie_retorno_w			Varchar2(2);
ds_retorno_w			Varchar2(120);
nr_seq_rol_w			Number(10);

begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_rol_w
from	pls_rol
where	dt_inicio_vigencia	= (select max(dt_inicio_vigencia) from pls_rol);

select	nvl(max(ie_complexidade),'X')
into	ie_retorno_w
from	pls_rol_capitulo	e,
	pls_rol_grupo		d,
	pls_rol_subgrupo	c,
	pls_rol_grupo_proc	b,
	pls_rol_procedimento	a
where	e.nr_seq_rol		= nr_seq_rol_w
and	a.nr_seq_rol_grupo	= b.nr_sequencia
and	a.cd_procedimento	= cd_procedimento_p
and	a.ie_origem_proced	= ie_origem_proced_p
and	b.nr_seq_subgrupo	= c.nr_sequencia
and	c.nr_seq_grupo		= d.nr_sequencia
and	d.nr_seq_capitulo	= e.nr_sequencia
and	nvl(b.ie_situacao,'A')	= 'A'
and	nvl(c.ie_situacao,'A')	= 'A'
and	nvl(d.ie_situacao,'A')	= 'A'
and	nvl(e.ie_situacao,'A')	= 'A';

if	(ie_retorno_w	= 'X') and
	(cd_procedimento_p is not null) then
	ie_retorno_w		:= 'NR';
elsif	(ie_retorno_w	= 'X') then
	ie_retorno_w		:= '';
end if;

if	(ie_opcao_p	= 'C') then
	ds_retorno_w	:= ie_retorno_w;
elsif	(ie_retorno_w	= 'S') then
	ds_retorno_w	:= 'Sim';
elsif	(ie_retorno_w	= 'N') then
	ds_retorno_w	:= 'N�o';
elsif	(ie_retorno_w	= 'NR') then
	ds_retorno_w	:= 'N�o consta no Rol';
else
	ds_retorno_w	:= '';
end if;

return	ds_retorno_w;

end pls_obter_se_proc_cpt_pac;
/