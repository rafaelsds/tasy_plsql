create or replace
function cus_obter_preco_padrao_proc(	cd_estabelecimento_p	number,
				dt_referencia_p		date,
				nr_seq_proc_interno_p	number)
				return number is

cd_estabelecimento_w		number(4)	:= cd_estabelecimento_p;
cd_tabela_custo_w			number(5);
dt_mesano_referencia_w		date;	
ie_atualizado_w			varchar2(1) := 'S';
vl_custo_w			number(15,4);

begin

if	(nr_seq_proc_interno_p is not null) then

	dt_mesano_referencia_w	:= pkg_date_utils.start_of(pkg_date_utils.add_month(dt_referencia_p,-1,0),'MONTH',0);
	
	select	max(decode(ie_mes_calculo,'C',cd_tabela_venda,0))
	into	cd_tabela_custo_w
	from	parametro_custo
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(nvl(cd_tabela_custo_w,0) = 0)	 then
		select	nvl(max(cd_tabela_custo),0)
		into	cd_tabela_custo_w
		from	tabela_custo
		where	cd_tipo_tabela_custo	= 9
		and	cd_estabelecimento	= cd_estabelecimento_w
		and	dt_mes_referencia	= dt_mesano_referencia_w;
	end if;

	select	nvl(max(vl_preco_calculado),0)
	into	vl_custo_w
	from	preco_padrao_proc_v2
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_tabela_custo		= cd_tabela_custo_w
	and	nr_seq_proc_interno	= nr_seq_proc_interno_p
	and	nvl(ie_calcula_conta,'S')	= 'S';
	
end if;

return	vl_custo_w;

end cus_obter_preco_padrao_proc;
/