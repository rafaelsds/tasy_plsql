CREATE OR REPLACE
FUNCTION Obter_fornecedor_venc_cotacao(	nr_cot_compra_p		number,
						nr_item_cot_compra_p		number)	
					RETURN Number IS

nr_seq_fornecedor_w			number(10);
nr_seq_item_fornecedor_w		number(10);
nr_cot_compra_w			number(10);
nr_item_cot_compra_w			number(5);

CURSOR C01 IS
	select	nr_seq_fornecedor
	from	cot_compra_forn_item_tr_v
	where	nr_cot_compra 	= nr_cot_compra_p
	and	nr_item_cot_compra 	= nr_item_cot_compra_p
	order by 
		vl_presente desc, 
		qt_prioridade, 
		vl_preco_liquido desc;

BEGIN

/*	Retirado por Marcus em 24/9/2005 

select	nvl(max(nr_seq_cot_item_forn),'0')
into	nr_seq_item_fornecedor_w
from	cot_compra_item
where	nr_cot_compra		= nr_cot_compra_p
and	nr_item_cot_compra	= nr_item_cot_compra_p;

if	(nr_seq_item_fornecedor_w = 0) then
	open c01;
	loop
	FETCH C01 into
		nr_seq_fornecedor_w;
	exit when c01%notfound;
		nr_seq_fornecedor_w	:= nr_seq_fornecedor_w;
	END LOOP;
	CLOSE C01;
else
	select	nr_seq_cot_forn
	into	nr_seq_fornecedor_w
	from	cot_compra_forn_item
	where	nr_sequencia = nr_seq_item_fornecedor_w;
end if;

*/

select obter_venc_cot_fornec_item(
		nr_cot_compra_p, 
		nr_item_cot_compra_p)
into	nr_seq_item_fornecedor_w
from	dual;

select	nr_seq_cot_forn
into	nr_seq_fornecedor_w
from	cot_compra_forn_item
where	nr_sequencia = nr_seq_item_fornecedor_w;

RETURN	nr_seq_fornecedor_w;

END Obter_fornecedor_venc_cotacao;
/
