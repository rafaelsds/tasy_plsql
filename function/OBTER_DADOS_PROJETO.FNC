create or replace
function obter_dados_projeto(	nr_seq_projeto_p	number,
								ie_tipo_dado_p		varchar2)
							return varchar2 is
					
ds_retorno_w			varchar2(255);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nm_usuario_w			usuario.nm_usuario%type;
dt_inicio_prev_w		varchar2(10);
dt_fim_prev_w			varchar2(10);
pr_consultoria_w		varchar(10);
nr_seq_proj_w			PROJ_CRONOGRAMA.nr_seq_proj%type;	


/*
>> ie_tipo_dado_p

> IT - Indicador de estado do t�rmino do projeto, medido da seguinte forma: 
		Caso falte mais de 30 dias corridos para a data de t�rmino do projeto retornar� N, caso conr�rio retornar� S
> PR - Percentual real de conclus�o do projeto
> OS - Quantidade de OSs pendentes n�o encerradas vinculadas � um projeto
> DI - Data de in�cio prevista do cronograma do projeto
> DF - Data de fim prevista do cronograma do projeto

*/

begin

nr_seq_proj_w := nr_seq_projeto_p;

if	(nr_seq_proj_w is not null) then
	
	select	wheb_usuario_pck.get_cd_estabelecimento,
			wheb_usuario_pck.get_nm_usuario
	into	cd_estabelecimento_w,
			nm_usuario_w
	from	dual;		

	select  PKG_DATE_FORMATERS.TO_VARCHAR(max(nvl(a.DT_INICIO_PREV,sysdate)),'DD/MM/YYYY', PKG_DATE_FORMATERS.GETCALENDAR(cd_estabelecimento_w, nm_usuario_w)),
			PKG_DATE_FORMATERS.TO_VARCHAR(max(nvl(a.DT_FIM_PREV,sysdate)),'DD/MM/YYYY', PKG_DATE_FORMATERS.GETCALENDAR(cd_estabelecimento_w, nm_usuario_w)) ,
			max(a.PR_CONSULTORIA)
	into	dt_inicio_prev_w,
			dt_fim_prev_w,
			pr_consultoria_w
	from    PROJ_CRON_ETAPA a,
			PROJ_CRONOGRAMA b,
			PROJ_PROJETO c
	where   a.nr_seq_cronograma = b.nr_sequencia        
	and     b.nr_seq_proj = c.nr_sequencia
	and     a.nr_seq_superior is null
	and     c.nr_sequencia = nr_seq_proj_w
	and     a.nr_seq_superior is null
	and     b.ie_situacao = 'A'
	and     a.ie_fase = 'S'
	and     b.QT_TOTAL_HORAS = (select  max(x.QT_TOTAL_HORAS)
								from    PROJ_CRONOGRAMA x
								where   x.nr_seq_proj = c.nr_sequencia);
							
	if	(ie_tipo_dado_p = 'IT') then
		
		select 	obter_ultimo_mes_migracao(to_date(dt_inicio_prev_w,'dd/mm/yyyy'),to_date(dt_fim_prev_w,'dd/mm/yyyy'))
		into	ds_retorno_w
		from	dual;
		
	elsif	(ie_tipo_dado_p = 'PR') then	
		ds_retorno_w	:= pr_consultoria_w;
	elsif	(ie_tipo_dado_p = 'OS') then	

		select  count(*)	
		into	ds_retorno_w
		from    PROJ_ORDEM_SERVICO a,
				man_ordem_servico b
		where   a.nr_seq_ordem = b.nr_sequencia
		and     a.nr_seq_proj = nr_seq_proj_w
		and     b.ie_status_ordem <> 3
		and     b.nr_seq_estagio <> 9;

	elsif	(ie_tipo_dado_p = 'DI') then	
		ds_retorno_w	:= dt_inicio_prev_w;
	elsif	(ie_tipo_dado_p = 'DF') then		
		ds_retorno_w	:= dt_fim_prev_w;

	end if;	
	
end if;

return	ds_retorno_w;

end obter_dados_projeto;
/