CREATE OR REPLACE
FUNCTION obter_eme_nome_paciente(	nr_seq_pessoa_p		Number,
					cd_pessoa_fisica_p		Number)
				RETURN VARCHAR IS

ds_retorno_w			Varchar2(100);

BEGIN
if	(cd_pessoa_fisica_p is not null) then
	select	substr(obter_nome_pessoa_fisica(cd_pessoa_fisica_p,null),1,80)
	into	ds_retorno_w
	from	dual;
elsif	(nr_seq_pessoa_p is not null) then
	select	substr(obter_nome_pf_pj(cd_pessoa_fisica, cd_cgc),1,80)
	into	ds_retorno_w
	from	eme_pf_contrato
	where	nr_sequencia = nr_seq_pessoa_p;
end if;
	
RETURN ds_retorno_w;

END obter_eme_nome_paciente;
/