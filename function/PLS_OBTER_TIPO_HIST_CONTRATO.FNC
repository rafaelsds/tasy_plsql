create or replace
function pls_obter_tipo_hist_contrato
			(	nr_seq_tipo_hist_p	number,
				ie_opcao_p		varchar2)
				return varchar2 is

ds_retorno_w			varchar2(255);
ds_tipo_historico_w		varchar2(255);

begin

select	ds_tipo_historico
into	ds_tipo_historico_w
from	pls_tipo_hist_contrato
where	nr_sequencia	= nr_seq_tipo_hist_p;

if	(ie_opcao_p	= 'D') then
	ds_retorno_w	:= ds_tipo_historico_w;
end if;

return	ds_retorno_w;

end pls_obter_tipo_hist_contrato;
/