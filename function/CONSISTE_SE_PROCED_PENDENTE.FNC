create or replace 
function consiste_se_proced_pendente (	cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nr_seq_proc_interno_p	number)
					return			varchar2 is

ie_pendente_w			varchar2(1);
qt_atend_regra_w		number(5,0);
cd_grupo_proc_w			number(15,0);
cd_area_proc_w			number(15,0);
cd_especialidade_proc_w		number(15,0);


begin

/* Pega os dados do procedimento */
select	cd_grupo_proc,
	cd_area_procedimento,
	cd_especialidade
into	cd_grupo_proc_w,
	cd_area_proc_w,
	cd_especialidade_proc_w
from	estrutura_procedimento_v
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

/* Verifica as regras */

select	count(*)
into	qt_atend_regra_w
from	regra_nao_consiste_exec
where	nvl(nr_seq_exame_interno, nvl(nr_seq_proc_interno_p,0))		= nvl(nr_seq_proc_interno_p,0)
and	nvl(cd_area_procedimento, nvl(cd_area_proc_w,0))		= nvl(cd_area_proc_w,0)
and	nvl(cd_grupo_proc, 	  nvl(cd_grupo_proc_w,0))		= nvl(cd_grupo_proc_w,0)
and	nvl(cd_especialidade,     nvl(cd_especialidade_proc_w,0))	= nvl(cd_especialidade_proc_w,0)
and	nvl(cd_procedimento,      nvl(cd_procedimento_p,0))		= nvl(cd_procedimento_p,0)
and	nvl(ie_origem_proced,     nvl(ie_origem_proced_p,0))		= nvl(ie_origem_proced_p,0);

if	(qt_atend_regra_w > 0) then
	ie_pendente_w := 'N';
else
	ie_pendente_w := 'S';
end if;

return ie_pendente_w;

end consiste_se_proced_pendente;
/