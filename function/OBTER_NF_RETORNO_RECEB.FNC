create or replace
function obter_nf_retorno_receb(nr_seq_retorno_p	number)
			return varchar2 is

nr_seq_receb_w		number(10);
nr_nota_fiscal_w	varchar2(255);
ds_retorno_w		varchar2(2000);
			
cursor	c01 is
select	nr_seq_receb
from	convenio_ret_receb
where	nr_seq_retorno	= nr_seq_retorno_p
group by nr_seq_receb;

cursor	c02 is
select	b.nr_nota_fiscal
from	convenio_receb_nf a,
	nota_fiscal b
where	a.NR_SEQ_NOTA_FISCAL	= b.nr_sequencia
and	a.nr_seq_receb		= nr_seq_receb_w
and	b.ie_situacao 		= '1'
group by b.nr_nota_fiscal;
			
begin

ds_retorno_w	:= null;

open C01;
loop
fetch C01 into	
	nr_seq_receb_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_nota_fiscal_w;
	exit when C02%notfound;
		begin
		
		if	(ds_retorno_w is null) then
			ds_retorno_w	:= nr_nota_fiscal_w;
		else		
			ds_retorno_w	:= ds_retorno_w ||', '||nr_nota_fiscal_w;
		end if;
		
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

return ds_retorno_w;

end;
/