CREATE OR REPLACE
FUNCTION Obter_Desc_Exame(	nr_seq_exame_p		NUMBER)
				RETURN VARCHAR2 IS

ds_retorno_w		Varchar2(255);

BEGIN
if	(nr_seq_exame_P	is not null) then
	select	substr(nm_exame,1,255)
	into	ds_retorno_w
	from	exame_laboratorio
	where	nr_seq_exame	= nr_seq_exame_p;
end if;

RETURN ds_retorno_w;

END Obter_Desc_Exame;
/