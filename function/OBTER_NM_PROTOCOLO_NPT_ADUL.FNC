create or replace
function obter_nm_protocolo_npt_adul(   nr_prescricao_p	number,
								 		nr_seq_item_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);				
				
begin

select 	substr(obter_desc_prot_npt(max(nr_seq_protocolo)),1,255)
into	ds_retorno_w
from	nut_pac
where 	nr_sequencia in 	(	select	b.nr_seq_nut_pac
								from	prescr_material b
								where	nr_prescricao = nr_prescricao_p
								and		nr_sequencia  = nr_seq_item_p);

if	(ds_retorno_w is null) then
	
	select	substr(obter_desc_material(max(cd_material)),1,255)
	into	ds_retorno_w
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia  = nr_seq_item_p;
	
end if;								
								
return	ds_retorno_w;

end obter_nm_protocolo_npt_adul;
/