create or replace
function obter_dados_envio_status_pac(nr_seq_status_p		varchar2,
				      nr_seq_agenda_p		number,	
				      ie_opcao_p		varchar2)
			return varchar2 is

ds_retorno_w	varchar2(4000) := '';
nr_seq_regra_w	number(10,0);
nm_paciente_w	varchar2(60);
cd_pessoa_fisica_w	varchar2(10);			
			
begin

select	max(nr_sequencia)
into	nr_seq_regra_w
from	regra_status_pac_email
where	nr_seq_status_pac = nr_seq_status_p;

if	(nr_seq_regra_w is not null) then
	if	(ie_opcao_p = 'T') then
		select	max(ds_titulo)
		into	ds_retorno_w
		from	regra_status_pac_email
		where	nr_sequencia = nr_seq_regra_w;
	elsif	(ie_opcao_p = 'R') then
		select	max(ds_remetente)
		into	ds_retorno_w
		from	regra_status_pac_email
		where	nr_sequencia = nr_seq_regra_w;
	elsif	(ie_opcao_p = 'M') then
		
		select	max(ds_email)
		into	ds_retorno_w
		from	regra_status_pac_email
		where	nr_sequencia = nr_seq_regra_w;
		
		select	max(nm_paciente)
		into	nm_paciente_w
		from	agenda_paciente
		where	nr_sequencia = nr_seq_agenda_p;
		
		ds_retorno_w	:= replace_macro(ds_retorno_w, '@Paciente', nm_paciente_w);	
		
	elsif	(ie_opcao_p = 'A') then
		select	max(ds_anexo)
		into	ds_retorno_w
		from	regra_status_pac_email
		where	nr_sequencia = nr_seq_regra_w;
	elsif	(ie_opcao_p = 'D') then
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	agenda_paciente
		where	nr_sequencia = nr_seq_agenda_p;
		
		select	max(b.ds_email)
		into	ds_retorno_w
		from	pessoa_fisica a,
			compl_pessoa_fisica b
		where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
		and	b.ie_tipo_complemento 	= 1
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
	end if;
end if;

return	ds_retorno_w;

end obter_dados_envio_status_pac;
/