create or replace
function obter_dados_rep_rotina(	nr_sequencia_p	number,
					ie_opcao_p	varchar2)
					return varchar2 is

ds_retorno_w	varchar2(255);
ds_rotina_w	varchar2(255);
					
begin

if	(nr_sequencia_p > 0) then
	select	ds_rotina
	into	ds_rotina_w
	from	rep_rotina
	where	nr_sequencia	=	nr_sequencia_p;
end if;

if	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_rotina_w;
end if;

return	ds_retorno_w;

end obter_dados_rep_rotina;
/