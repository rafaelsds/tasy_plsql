create or replace 
FUNCTION Obter_Proc_Interno(	nr_seq_proc_interno_p	Number,
				ie_opcao_p		varchar2)
				return varchar2 is

ds_resultado_w	varchar2(20);

/* Valores poss�veis para o par�metro IE_OPCAO_P
	- 'CI' - C�digo de integra�ao (CD_INTEGRACAO)
*/

BEGIN

if	(nr_seq_proc_interno_p is not null) then
	if	(ie_opcao_p = 'CI') then		
		select	cd_integracao
		into	ds_resultado_w
		from	proc_interno
		where	nr_sequencia	= nr_seq_proc_interno_p;
	end if;
end if;

return	ds_resultado_w;

END Obter_Proc_Interno;
/