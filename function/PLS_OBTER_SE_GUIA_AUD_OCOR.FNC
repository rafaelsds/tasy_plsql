create or replace
function pls_obter_se_guia_aud_ocor
			(	nr_seq_guia_p		number)
				return number is

nr_retorno_w			number(5);
qt_guia_proc_auditoria_w	number(5) := 0;
qt_guia_mat_auditoria_w		number(5) := 0;
qt_guia_plano_ocorrencia_w	number(5) := 0;
ie_estagio_w			number(2);

begin

select	count(*)
into	qt_guia_proc_auditoria_w
from	pls_ocorrencia_benef	b,
	pls_guia_plano_proc	a
where	a.nr_seq_guia	= nr_seq_guia_p
and	a.nr_sequencia	= b.nr_seq_proc
and	a.ie_status	= 'A';

select	count(*)
into	qt_guia_mat_auditoria_w
from	pls_ocorrencia_benef	b,
	pls_guia_plano_mat	a
where	a.nr_seq_guia	= nr_seq_guia_p
and	a.nr_sequencia	= b.nr_seq_mat
and	a.ie_status	= 'A';

begin
select	ie_estagio
into	ie_estagio_w
from	pls_guia_plano
where	nr_sequencia	= nr_seq_guia_p;
exception
when others then
	ie_estagio_w := 0;
end;

if	(ie_estagio_w = 7) then
	select	count(*)
	into	qt_guia_plano_ocorrencia_w
	from	pls_ocorrencia_benef	b,
		pls_ocorrencia		a
	where	b.nr_seq_guia_plano = nr_seq_guia_p
	and	b.nr_seq_ocorrencia = a.nr_sequencia
	and	a.ie_auditoria = 'S'
	and	nr_seq_proc is null
	and	nr_seq_mat is null;
end if;

nr_retorno_w := qt_guia_proc_auditoria_w + qt_guia_mat_auditoria_w + qt_guia_plano_ocorrencia_w;

return	nr_retorno_w;

end pls_obter_se_guia_aud_ocor;
/
