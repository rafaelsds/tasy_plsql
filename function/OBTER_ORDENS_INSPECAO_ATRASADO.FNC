create or replace
function obter_ordens_inspecao_atrasado(nr_seq_registro_p		number)
 		    	return varchar2 is

ds_ordens_w		varchar2(255);
nr_ordem_compra_w	number(10);


cursor C01 is
	select	distinct nr_ordem_compra
	from	inspecao_recebimento
	where 	obter_dt_prev_oci_inspecao(nr_sequencia) < trunc(sysdate)
	and	dt_entrega_real = trunc(sysdate)
	and	nr_ordem_compra is not null
	and	nr_seq_registro = nr_seq_registro_p;

begin


open C01;
loop
fetch C01 into
	nr_ordem_compra_w;
exit when C01%notfound;
	begin
	ds_ordens_w := substr(ds_ordens_w || nr_ordem_compra_w || chr(13) || chr(10) || ',',1,255);
	end;
end loop;
close C01;

return	ds_ordens_w;

end obter_ordens_inspecao_atrasado;
/