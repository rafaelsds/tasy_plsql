create or replace
function obter_unidade_medida_prior_onc( 	cd_material_p  number,
					cd_estabelecimento_p number)
        return varchar2 is

cd_unidade_medida_w 		varchar2(30);
ie_prioridade_conversao_w	varchar2(2);

begin
if (cd_material_p 	is not null) then

			select  max(a.cd_unidade_medida)
			into 	cd_unidade_medida_w
			from 	material_conversao_unidade a
			where 	a.cd_material = cd_material_p
			and  	a.ie_prioridade = 	(select min(b.ie_prioridade)
							from 	material_conversao_unidade b
							where 	b.cd_material = cd_material_p)
							order by a.ie_prioridade;

		
end if;

return cd_unidade_medida_w;

end obter_unidade_medida_prior_onc;
/
