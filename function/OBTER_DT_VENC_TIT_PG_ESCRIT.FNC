create or replace function obter_dt_venc_tit_pg_escrit(nr_tit_pagar_escrit number) return date is
dt_vencimento_w		date;				
begin

select	max(dt_vencimento_atual)
into	dt_vencimento_w
from	titulo_pagar 
where	nr_titulo	 = nr_tit_pagar_escrit;

return dt_vencimento_w;

end obter_dt_venc_tit_pg_escrit;
/