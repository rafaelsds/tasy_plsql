create or replace
function obter_infusion_duration_cpoe (nr_sequencia_p	in PRESCR_SOLIC_BCO_SANGUE.nr_sequencia%type) 
return varchar2 deterministic is

qt_infusion_w		cpoe_hemoterapia.qt_tempo_inf_etapa%type := null;

begin

	if (nr_sequencia_p is not null) then
	
			
		select	a.qt_tempo_inf_etapa
		into	qt_infusion_w
		from	cpoe_hemoterapia a,
				prescr_solic_bco_sangue b
		where	b.nr_seq_hemo_cpoe = a.nr_sequencia
		and		b.nr_sequencia = nr_sequencia_p;

	end if;

return qt_infusion_w;

end obter_infusion_duration_cpoe;
/
