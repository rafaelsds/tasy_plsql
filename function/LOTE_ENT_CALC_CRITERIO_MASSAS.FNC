create or replace
function lote_ent_calc_criterio_massas(NR_SEQ_PAT_EXAM_REF_p		number,
				nr_seq_exame_padrao_p	number,
				nr_prescricao_p		number,
				ie_opcao_p		number,
				nr_seq_patologia_p	number,
				nr_seq_grupo_pat_p	number)
 		    	return varchar2 is

nr_prioridade_w				number(10);
qt_idade_gest_inicial_w				number(10);
qt_idade_gest_final_w				number(10);
ie_amamentado_w				varchar2(1);
ie_prematuro_w				varchar2(1);
ie_transfudido_w			varchar2(1);
qt_peso_inic_w				number(10,3);
qt_peso_fim_w				number(10,3);
nr_seq_classif_w			number(10);
nr_seq_grau_w				number(10);
ie_gerar_busca_ativa_w		varchar2(1);
ie_gerar_busca_passiva_w	varchar2(1);
ds_mensagem_w				varchar2(4000);
ie_acao_criterio_w			varchar2(1);	

--Variaveis da lote_ent_sec_ficha
lote_qt_idade_gest_w			number(10);
lote_ie_amamentado_w			varchar2(1);
lote_ie_prematuro_w				varchar2(1);
lote_ie_transfundido_w			varchar2(1);
lote_qt_peso_w					number(10,3);
lote_nr_seq_classif_w			number(10);
lote_nr_seq_grau_w				number(10);
lote_ie_gerar_busca_ativa_w		varchar2(1);
lote_ie_gerar_busca_passiva_w	varchar2(1);
lote_ie_mae_veg_w				varchar2(1);
lote_ie_ictericia_w				varchar2(1);
lote_ie_npp_w					varchar2(1);
lote_ie_cor_pf_w				varchar2(10);
lote_ie_gemelar_w				varchar2(1);
lote_qt_media_w					varchar2(10);
lote_qt_gest_w					number(2);
lote_dt_ult_menst_w				date;
lote_ie_tipo_parto_w			varchar2(1);
lote_ie_alim_leite_w			varchar2(1);
ie_classif_lote_ent_w			number(3);
nr_seq_atecaco_w				number(10,0);
lote_cd_convenio_w				number(5);
lote_pessoa_fisica_w			varchar2(10);
lote_medic_uso_w				number(10);
lote_cd_municipio_w				varchar2(6);
lote_ie_tipo_coleta_w			varchar2(1);
lote_ie_data_coleta_w			date;
lote_ie_hora_coleta_w			date;
lote_ie_data_nascimento_w		date;
lote_ie_hora_nascimento_w		date;
	
ds_retorno_w				varchar2(4000);
nr_atendimento_w			number(10);
qt_dias_prev_w				number(10);
ie_tipo_busca_w				varchar2(1);
ds_resultado_sugerido_w		varchar2(255);

nr_seq_resultado_w			number(10);
nr_seq_prescr_w				number(10);
nr_seq_material_w			number(10);
nr_seq_metodo_w				number(10);
ie_corticoide_f_w		lab_valor_padrao_criterio.ie_corticoide%type;
			
begin


/*
ie_opcao_p:
1 - Retorna o DS_MENSAGEM da tabela LOTE_ENT_PATOL_EXAM_CRIT
2 - Retorna o IE_ACAO_CRITERIO (Descri��o) da tabela LOTE_ENT_PATOL_EXAM_CRIT
3 - Retorna o QT_DIAS_PREV da tabela LOTE_ENT_PATOL_EXAM_CRIT
4 - Retorna o tipo de busca que dever� ser realizado ao reconvocar o exame segundo o criterio
5 - Retorna o c�d  IE_ACAO_CRITERIO
6 - Retorna resultado sugerido
7 - Retorna o material crit�rio
8 - Retorna o m�todo crit�rio
*/


--Para buscar o de maior prioridade caso houverem outros cadastrados para o mesmo item
select	nvl(min(a.nr_seq_prioridade),0)
into	nr_prioridade_w
from	LOTE_ENT_PATOL_EXAM_CRIT a,
	LOTE_ENT_PATOL_EXAM_REF b,
	lab_patologia_exame c,
	lab_patologia d
where	a.NR_SEQ_PAT_EXAM_REF = NR_SEQ_PAT_EXAM_REF_p
and	a.nr_seq_pat_exam_ref = b.nr_sequencia
and	c.nr_seq_exame = b.nr_seq_exame
and	d.nr_sequencia = c.nr_seq_patologia
and	c.nr_seq_patologia = b.nr_seq_patologia
and	d.nr_seq_grupo_pat = nr_seq_grupo_pat_p
and	c.nr_seq_patologia = nr_seq_patologia_p
and	a.nr_seq_exame = nr_seq_exame_padrao_p;

select	max(nr_atendimento)
into	nr_atendimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select	max(ie_classif_lote_ent)
into	ie_classif_lote_ent_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;

select	max(nr_seq_resultado)
into	nr_seq_resultado_w
from	exame_lab_resultado
where 	nr_prescricao = nr_prescricao_p;

select	max(nr_sequencia)
into	nr_seq_prescr_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and		nr_seq_exame = nr_seq_exame_padrao_p;

select	max(qt_peso_f),
	max(nr_idade_gest_f),
	max(ie_premat_s_f),
	max(IE_AMAMENTADO_F),
	max(ie_transfusao_f),
	max(nr_seq_grau_parentesco),
	max(substr(obter_classif_atendimento(nr_atendimento_w),1,10)),
	max(IE_MAE_VEG_F),
	max(IE_ICTERICIA_F),
	max(IE_NPP_F),
	max(IE_COR_PF_F),
	max(IE_GEMELAR_F),
	max(substr(lote_ent_obter_result_ant(nr_prescricao_p, nr_seq_prescr_w, nr_seq_resultado_w, nr_seq_exame_padrao_p, 99),1,255)),
	max(QT_GEST_F),
	max(DT_ULT_MENST),
	max(IE_TIPO_PARTO),
	max(IE_ALIM_LEITE_F),
	max(cd_pessoa_fisica),
	max(ie_corticoide_f),
	max(DT_COLETA_FICHA_F),
	max(HR_COLETA_F),
	max(DT_NASCIMENTO_F),
	max(HR_NASCIMENTO_F)
into	lote_qt_peso_w,
	lote_qt_idade_gest_w,
	lote_ie_prematuro_w,
	lote_ie_amamentado_w,
	lote_ie_transfundido_w,
	lote_nr_seq_grau_w,
	lote_nr_seq_classif_w,
	lote_ie_mae_veg_w,
	lote_ie_ictericia_w,
	lote_ie_npp_w,
	lote_ie_cor_pf_w,
	lote_ie_gemelar_w,
	lote_qt_media_w,
	lote_qt_gest_w,
	lote_dt_ult_menst_w,
	lote_ie_tipo_parto_w,
	lote_ie_alim_leite_w,
	lote_pessoa_fisica_w,
	ie_corticoide_f_w,
	lote_ie_data_coleta_w,
	lote_ie_hora_coleta_w,
	lote_ie_data_nascimento_w,
	lote_ie_hora_nascimento_w
from	lote_ent_sec_ficha
where	nr_prescricao = nr_prescricao_p;	

select	max(a.ds_mensagem),
	max(a.ie_acao_criterio),
	max(a.qt_dias_prev),
	max(nvl(a.IE_GERAR_BUSCA_ATIVA,'A')),
	max(a.ds_result_sugerido),
	max(a.nr_seq_material),
	max(a.nr_seq_metodo)
into	ds_mensagem_w,
	ie_acao_criterio_w,
	qt_dias_prev_w,
	ie_tipo_busca_w,
	ds_resultado_sugerido_w,
	nr_seq_material_w,
	nr_seq_metodo_w
from	LOTE_ENT_PATOL_EXAM_CRIT a,
	LOTE_ENT_PATOL_EXAM_REF b,
	lab_patologia_exame c,
	lab_patologia d
where	a.NR_SEQ_PAT_EXAM_REF = NR_SEQ_PAT_EXAM_REF_p
and	a.nr_seq_pat_exam_ref = b.nr_sequencia
and	c.nr_seq_exame = b.nr_seq_exame
and	d.nr_sequencia = c.nr_seq_patologia
and	c.nr_seq_patologia = b.nr_seq_patologia
and	d.nr_seq_grupo_pat = nr_seq_grupo_pat_p
and	d.nr_sequencia = nr_seq_patologia_p
and	b.nr_seq_patologia = nr_seq_patologia_p
and	a.nr_seq_exame = nr_seq_exame_padrao_p
and	((lote_qt_peso_w between a.qt_peso_inic and a.qt_peso_fim) or (nvl(lote_qt_peso_w,'0') = '0'))
and	((lote_qt_idade_gest_w between a.QT_IDADE_GESTACIONAL and a.QT_IDADE_GEST_FIM) or (nvl(lote_qt_idade_gest_w,0) = 0))
and	((lote_ie_prematuro_w = nvl(a.ie_prematuro,lote_ie_prematuro_w)) or (nvl(lote_ie_prematuro_w,'I') = 'I'))
and	((lote_ie_amamentado_w = nvl(a.ie_amamentado,lote_ie_amamentado_w)) or (nvl(lote_ie_amamentado_w,'I') = 'I'))
and	((lote_ie_transfundido_w = nvl(a.ie_transfundido,lote_ie_transfundido_w)) or (nvl(lote_ie_transfundido_w,'I') = 'I'))
and	((lote_nr_seq_grau_w = nvl(a.nr_seq_grau_parentesco,lote_nr_seq_grau_w)) or (nvl(lote_nr_seq_grau_w,0) = 0))
and	((lote_nr_seq_classif_w = nvl(a.nr_seq_classificacao,lote_nr_seq_classif_w)) or (nvl(lote_nr_seq_classif_w,0) = 0))
and 	((lote_ie_mae_veg_w = nvl(a.IE_MAE_VEG,lote_ie_mae_veg_w)) or (nvl(lote_ie_mae_veg_w,'I') = 'I'))
and 	((lote_ie_ictericia_w = nvl(a.IE_ICTERICIA,lote_ie_ictericia_w)) or (nvl(lote_ie_ictericia_w,'I') = 'I'))
and 	((lote_ie_npp_w = nvl(a.IE_NPP,lote_ie_npp_w)) or (nvl(lote_ie_npp_w,'I') = 'I'))
and 	((lote_ie_cor_pf_w = nvl(a.IE_COR_PF,lote_ie_cor_pf_w)) or (nvl(lote_ie_cor_pf_w,'N') = 'N'))
and 	((lote_ie_gemelar_w = nvl(a.IE_GEMELAR,lote_ie_gemelar_w)) or (nvl(lote_ie_gemelar_w,'N') = 'N'))
and	(((a.QT_MINIMA is not null and a.QT_MAXIMA is not null and lote_qt_media_w is not null) and (lote_qt_media_w  between a.QT_MINIMA and a.QT_MAXIMA)) or	(1=1))
and	((lote_qt_gest_w between a.QT_GEST_INI and a.QT_GEST_FIM) or (nvl(lote_qt_gest_w,'0') = '0'))
and	((lote_dt_ult_menst_w is null) or (lote_dt_ult_menst_w between a.DT_ULT_MENST_INI and a.DT_ULT_MENST_FIM))
and	((lote_ie_tipo_parto_w = nvl(a.IE_TIPO_PARTO,lote_ie_tipo_parto_w)) or (nvl(lote_ie_tipo_parto_w,'N') = 'N'))
and 	((lote_ie_alim_leite_w = nvl(a.IE_ALIM_LEITE,lote_ie_alim_leite_w)) or (nvl(lote_ie_alim_leite_w,'I') = 'I'))
and	((ie_classif_lote_ent_w = a.IE_CLASSIF_LOTE_ENT) or (nvl(ie_classif_lote_ent_w,0) = 0))
and	((lote_cd_convenio_w = nvl(a.CD_CONVENIO,lote_cd_convenio_w)) or (nvl(lote_cd_convenio_w,0) = 0))
and	((lote_medic_uso_w = nvl(a.CD_MATERIAL_MED,lote_medic_uso_w)) or (nvl(lote_medic_uso_w,0) = 0))
and 	((lote_cd_municipio_w = nvl(a.CD_MUNICIPIO_IBGE,lote_cd_municipio_w)) or (nvl(lote_cd_municipio_w,'0') = '0'))
and 	((lote_ie_tipo_coleta_w = nvl(a.IE_TIPO_COLETA,lote_ie_tipo_coleta_w)) or (nvl(lote_ie_tipo_coleta_w,'N') = 'N'))
AND		((ie_corticoide_f_w = nvl(ie_corticoide,ie_corticoide_f_w)) OR (NVL(ie_corticoide_f_w,'0') = '0'))
and 	(((lote_ie_data_coleta_w is null) and (nvl(ie_data_coleta, 'N') = 'I')) or (nvl(ie_data_coleta, 'N') = 'N'))
and 	(((lote_ie_hora_coleta_w is null) and (nvl(ie_hora_coleta, 'N') = 'I')) or (nvl(ie_hora_coleta, 'N') = 'N'))
and 	(((lote_ie_data_nascimento_w is null) and (nvl(ie_data_nascimento, 'N') = 'I')) or (nvl(ie_data_nascimento, 'N') = 'N'))
and 	(((lote_ie_hora_nascimento_w is null) and (nvl(ie_hora_nascimento, 'N') = 'I')) or (nvl(ie_hora_nascimento, 'N') = 'N'))
order by b.nr_seq_prioridade, a.nr_seq_prioridade asc;

if 	(ie_opcao_p = 1) 	then
	ds_retorno_w 	:=	ds_mensagem_w;
elsif 	(ie_opcao_p = 2) 	then
	if 	(ie_acao_criterio_w = 'T')	then
		ds_retorno_w	:=	Obter_Desc_Expressao(349095);
	elsif	(ie_acao_criterio_w = 'R')	then
		ds_retorno_w	:=	Obter_Desc_Expressao(349096);
	elsif	(ie_acao_criterio_w = 'N')	then
		ds_retorno_w	:=	Obter_Desc_Expressao(349097);		
	elsif	(ie_acao_criterio_w = 'D')	then
		ds_retorno_w	:=	Obter_Desc_Expressao(349098);		
	end if;
elsif 	(ie_opcao_p = 3) then
	ds_retorno_w := to_char(qt_dias_prev_w);
elsif (ie_opcao_p = 4) then
	ds_retorno_w := ie_tipo_busca_w;
elsif (ie_opcao_p = 5) then
	ds_retorno_w := ie_acao_criterio_w;
elsif (ie_opcao_p = 6) then
	ds_retorno_w := ds_resultado_sugerido_w;
elsif (ie_opcao_p = 7) then
	ds_retorno_w := substr(Obter_Material_Exame_Lab(nr_seq_material_w,null,3),1,255);
elsif (ie_opcao_p = 8) then
	ds_retorno_w := substr(Obter_Metodo_Exame_Lab(nr_seq_metodo_w,2),1,255);	
end if;

return	ds_retorno_w;

end lote_ent_calc_criterio_massas;
/
