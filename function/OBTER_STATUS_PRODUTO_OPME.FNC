create or replace
function obter_status_produto_opme	(nr_lote_producao_p	number,
					ie_opcao_p		varchar2)

/* S - status do lote
     C - Cor */					return varchar2 is

nr_seq_prioridade_w	number(10);
ds_retorno_w		varchar(255);

begin

if (nr_lote_producao_p is not null) then
	select  max(a.nr_seq_prioridade)
	into	nr_seq_prioridade_w
	from    lote_prod_comp_estagio a,
		lote_producao_comp b
	where   a.nr_sequencia = b.nr_seq_estagio
	and     b.nr_lote_producao = nr_lote_producao_p;
end if;	

if	(nr_seq_prioridade_w is not null) and (ie_opcao_p = 'S') then
	select	max(ds_status_lote)
	into	ds_retorno_w
	from	lote_prod_comp_estagio
	where	nr_seq_prioridade = nr_seq_prioridade_w;
	
elsif (nr_seq_prioridade_w is not null) and (ie_opcao_p = 'C') then
	select	max(ds_cor)
	into	ds_retorno_w
	from	lote_prod_comp_estagio
	where	nr_seq_prioridade = nr_seq_prioridade_w;

end if;	

return ds_retorno_w;

end obter_status_produto_opme;
/