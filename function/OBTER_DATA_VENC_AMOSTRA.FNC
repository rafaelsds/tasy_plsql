create or replace
function obter_data_venc_amostra(	nr_seq_armazena_rack_p number, 
                                  nr_seq_amostra_rack_p  number,
                                  ie_acao_p varchar)
                                  return date is
nr_seq_grupo_w          number(10,0);
qt_dias_armazenamento_w number(4,0);
dt_armazenamento_w      date;
begin
  select max(el.nr_seq_grupo)
    into nr_seq_grupo_w
    from lab_soro_exame_amostra lsar
   inner join prescr_procedimento pp on pp.nr_prescricao = lsar.nr_prescricao and pp.nr_sequencia = lsar.nr_seq_prescr
   inner join exame_laboratorio el on el.nr_seq_exame = pp.nr_seq_exame
   where lsar.nr_seq_amostra_rack = nr_seq_amostra_rack_p;    
   
  select max(lsge.qt_dias_armazenamento)
    into qt_dias_armazenamento_w
    from LAB_SORO_ARMAZENA_RACK lsar
   inner join LAB_SORO_ARMAZENAMENTO lsa on lsa.nr_sequencia = lsar.nr_seq_armazenamento_prim 
   inner join LAB_SORO_GRUPO_EXAME lsge on lsge.nr_seq_armazenamento = lsa.nr_sequencia and 
                                           lsge.ie_situacao = 'A' and
                                           lsge.nr_seq_grupo = nr_seq_grupo_w
   where lsar.nr_sequencia = nr_seq_armazena_rack_p;
   
   if (qt_dias_armazenamento_w is null) then
      select nvl(max(QT_DIAS_ARMAZENAMENTO), 0)
        into qt_dias_armazenamento_w      
        from GRUPO_EXAME_LAB
       where nr_sequencia = nr_seq_grupo_w;
   end if;
   
   select min(dt_acao)
     into dt_armazenamento_w   
     from lab_soro_processo_info
    where IE_ACAO = ie_acao_p and
          nr_seq_armazena_rack = nr_seq_armazena_rack_p and
          nr_seq_amostra_rack = nr_seq_amostra_rack_p;
    
return dt_armazenamento_w + qt_dias_armazenamento_w;

end obter_data_venc_amostra;
/