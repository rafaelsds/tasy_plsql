create or replace
function Obter_descricao_turno_nut(nr_Seq_turno_p	number)
 		    	return varchar is
			
ds_retorno_w	varchar2(255);			

begin

if	(nr_Seq_turno_p > 0) then

	select 	max(ds_turno)
	into	ds_retorno_w
	from	nut_turno_lactario
	where	nr_sequencia = nr_Seq_turno_p;
end if;

return	ds_retorno_w;

end Obter_descricao_turno_nut;
/