create or replace
function Obter_motivo_cancel_Proc_Aprov(	nr_seq_processo_p	Number)
		return varchar2 is


ds_motivo_w		varchar2(255);

begin

select	substr(max(obter_motivo_cancel_oc(b.nr_seq_motivo_cancel)),1,255)
into	ds_motivo_w
from	ordem_compra b,
	ordem_compra_item a
where	a.nr_ordem_compra		= b.nr_ordem_compra
and	a.nr_seq_aprovacao	= nr_seq_processo_p;

if	(ds_motivo_w is null) then
	select	substr(max(obter_motivo_cancel_oc(b.nr_seq_motivo_cancel)),1,255)
	into	ds_motivo_w
	from	solic_compra b,
		solic_compra_item a
	where	a.nr_solic_compra		= b.nr_solic_compra
	and	a.nr_seq_aprovacao	= nr_seq_processo_p;
end if;

if	(ds_motivo_w is null) then
	select	substr(max(obter_motivo_cancel_oc(b.nr_seq_motivo_cancel)),1,255)
	into	ds_motivo_w
	from	cot_compra b,
		cot_compra_item a
	where	a.nr_cot_compra		= b.nr_cot_compra
	and	a.nr_seq_aprovacao	= nr_seq_processo_p;
end if;

return ds_motivo_w;

end Obter_motivo_cancel_Proc_Aprov;
/
