create or replace
Function Obter_Seq_Folha_Item_Geap(	nr_sequencia_ant_p	number)
				 	return number is

nr_sequencia_w	number(10);

begin

nr_sequencia_w := nvl(nr_sequencia_ant_p,0) + 1;
if	(mod(nr_sequencia_w,100) = 0) then
	nr_sequencia_w := nr_sequencia_w + 1;
end if;

return nr_sequencia_w;

end;
/