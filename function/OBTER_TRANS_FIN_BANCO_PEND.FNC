create or replace
function OBTER_TRANS_FIN_BANCO_PEND
				(nr_seq_movto_trans_fin_p	in number,
				ie_opcao_p		in varchar2)
				return varchar2 is

nr_seq_trans_financ_w	number(10);
ds_trans_financ_w		varchar2(255);
ds_retorno_w		varchar2(255);

begin

select	max(nr_seq_trans_financ),
	max(substr(OBTER_DESC_TRANS_FINANC(nr_seq_trans_financ),1,255))
into	nr_seq_trans_financ_w,
	ds_trans_financ_w
from	movto_trans_financ
where	nr_sequencia 	= nr_seq_movto_trans_fin_p;

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= to_char(nr_seq_trans_financ_w);
elsif	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_trans_financ_w;
end if;

return ds_retorno_w;

end OBTER_TRANS_FIN_BANCO_PEND;
/