/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Gera as informa��es no relat�rio CPLS - 224 - "OPS - Recibo de Entrega de Cart�o de Dependente Pessoa F�sica"
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  X]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_dependentes_relat
		(	nr_seq_contrato_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(4000);
nm_pessoa_fisica_w	pessoa_fisica.nm_pessoa_fisica%type;

Cursor C01 is
	select	b.nm_pessoa_fisica
	from	pessoa_fisica		b,
		pls_segurado		a
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	a.nr_seq_contrato	= nr_seq_contrato_p
	and	a.nr_seq_titular is not null
	and	((a.dt_rescisao is null) or (a.dt_rescisao > sysdate))
	order by b.nm_pessoa_fisica;
		


begin

open C01;
loop
fetch C01 into	
	nm_pessoa_fisica_w;
exit when C01%notfound;
	begin
	
	ds_retorno_w	:= ds_retorno_w||chr(10)||nm_pessoa_fisica_w;
	
	end;
end loop;
close C01;


return	ds_retorno_w;

end pls_obter_dependentes_relat;
/
