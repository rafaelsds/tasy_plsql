create or replace
function obter_tipo_processo_adep (
				nr_seq_processo_p	number)
				return varchar2 is
				
nr_seq_solucao_w		number(6,0);
nr_seq_procedimento_w	adep_processo.nr_seq_procedimento%type;
ie_tipo_processo_w		varchar2(1);
nr_seq_material_w		adep_processo.nr_seq_material%type;
ie_agrupador_w			prescr_material.ie_agrupador%type;
				
begin
if	(nr_seq_processo_p is not null) then

	select	nvl(max(nr_seq_solucao),0),
			nvl(max(nr_seq_procedimento),0),
			nvl(max(nr_seq_material),0)
	into	nr_seq_solucao_w,
			nr_seq_procedimento_w,
			nr_seq_material_w
	from	adep_processo
	where	nr_sequencia = nr_seq_processo_p;
	
	if	(nr_seq_solucao_w > 0) then
		ie_tipo_processo_w	:= 'S';
	elsif ((nr_seq_procedimento_w > 0) and (nvl(nr_seq_material_w,0) = 0)) then -- aqui
		ie_tipo_processo_w	:= 'P';
	else
	
		select	max(ie_agrupador)
		into	ie_agrupador_w
		from	prescr_mat_hor
		where	nr_seq_processo = nr_seq_processo_p
		and		nr_seq_material = nr_seq_material_w;
		
		if	(ie_agrupador_w = 11) then
			ie_tipo_processo_w	:= 'N';
		else
			ie_tipo_processo_w	:= 'M';		
		end if;
	
		
	end if;

end if;

return ie_tipo_processo_w;

end obter_tipo_processo_adep;
/