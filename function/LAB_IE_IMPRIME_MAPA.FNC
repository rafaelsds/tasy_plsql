create or replace
FUNCTION lab_ie_imprime_mapa(	nr_seq_exame_p 		NUMBER,
								nm_usuario_p 		VARCHAR2)
				RETURN VARCHAR2 IS
ie_formato_w	VARCHAR2(1);

BEGIN
SELECT	DECODE(COUNT(*), 0, 'N', 'S')
INTO	ie_formato_w
FROM	exame_laboratorio
WHERE	nr_seq_exame = nr_seq_exame_p
AND 	ie_imprime_mapa = 'S';

RETURN ie_formato_w;

END lab_ie_imprime_mapa;
/