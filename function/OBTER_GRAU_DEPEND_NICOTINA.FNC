create or replace function OBTER_GRAU_DEPEND_NICOTINA(qt_pontos_p	Number)

		             	return		varchar2 is

ds_dependencia_w		varchar2(100);

begin

if (pkg_i18n.get_user_locale() !=  'en_AU') then 

begin

if	(qt_pontos_p >7) then

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(309116); -- Muito elevado

elsif	(qt_pontos_p > 5) then

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(309117); -- Elevado

elsif	(qt_pontos_p = 5) then

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(309118); -- Medio

elsif	(qt_pontos_p > 2) then

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(309119); -- Baixo

else

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(309120); -- Muito baixo

end if;

end;

else 

begin

if	(qt_pontos_p >= 8) then

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(1112554); -- high dependence

elsif	(qt_pontos_p >= 5 and qt_pontos_p <= 7) then

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(1112553); -- moderate dependence

elsif	(qt_pontos_p >= 3 and qt_pontos_p <= 4) then

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(1112552); -- low to mod dependence

else	

	ds_dependencia_w 	:= wheb_mensagem_pck.get_texto(1112551); -- Low dependence

end if;

end ;

end if;

return ds_dependencia_w;

end OBTER_GRAU_DEPEND_NICOTINA;
/
