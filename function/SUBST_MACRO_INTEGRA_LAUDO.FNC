create or replace
function subst_macro_integra_laudo ( 	ds_texto_p		varchar2,
					nr_atendimento_p	number,
					nr_seq_laudo_p		number,
					nm_usuario_p		varchar2)
				return varchar2 is

ds_retorno_w		varchar2(255);
ds_texto_w		varchar2(255);
ds_texto_alterado_w	varchar2(255);
pos_macro_w		number(10);
ds_macro_w		varchar2(30);
nm_atributo_w		varchar2(50);
pos_fim_macro_w		number(10);
vl_atributo_w		varchar2(255);
ds_resultado_macro_w	varchar2(255);
cd_pessoa_fisica_w	varchar2(14);


begin
ds_texto_w		:= ds_texto_p;
ds_texto_alterado_w	:= ds_texto_w;
select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

if	(ds_texto_w is not null) then
	begin
	WHILE	(ds_texto_w is not null) LOOP
		begin
	
		ds_texto_w	:= replace(replace(ds_texto_w,'\',' '),chr(13) || chr(10),' ');
		
		pos_macro_w	:= instr(ds_texto_w,'@');
		
		if (pos_macro_w > 0) then
			ds_texto_w	:= substr(ds_texto_w,pos_macro_w,length(ds_texto_w));
			pos_fim_macro_w	:= instr(ds_texto_w,' ');
			ds_macro_w	:= Elimina_Caracteres_Especiais(substr(ds_texto_w,1,pos_fim_macro_w -1));
			if (nvl(ds_macro_w,null) is not null) then
				begin
				select	nm_atributo
				into	nm_atributo_w
				from	macro_prontuario
				where	upper(nm_macro) = upper(ds_macro_w);
				exception
				when others then
					nm_atributo_w := null;
				end;
				
				if (nvl(nm_atributo_w,null) is not null) then
					begin
					if (upper(nm_atributo_w) = 'CD_PESSOA_FISICA') then
						vl_atributo_w	:= cd_pessoa_fisica_w;
					elsif (upper(nm_atributo_w) = 'NR_ATENDIMENTO') then
						vl_atributo_w	:= to_char(nr_atendimento_p);
					elsif (upper(nm_atributo_w) = 'CD_PESSOA_USUARIO') then
						select	Obter_Pessoa_Fisica_Usuario(nm_usuario_p, 'C')
						into	vl_atributo_w
						from	dual;
					elsif (upper(nm_atributo_w) = 'NR_LAUDO') then
						vl_atributo_w	:= nr_seq_laudo_p;
					end if;
					ds_resultado_macro_w	:= Substituir_Macro_Texto_Tasy(upper(ds_macro_w), nm_atributo_w, vl_atributo_w);
					
					ds_texto_alterado_w	:=  replace(ds_texto_alterado_w, ds_macro_w, ds_resultado_macro_w);
					end;
				end if;
			end if;
			ds_texto_w	:= substr(ds_texto_w,pos_fim_macro_w, length(ds_texto_w));
		else
			ds_texto_w := '';
		end if;
		
		end;
	END loop;
	end;
end if;

return	ds_texto_alterado_w;

end subst_macro_integra_laudo;
/