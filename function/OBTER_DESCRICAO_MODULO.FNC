create or replace
function Obter_descricao_modulo (nr_seq_modulo_p	number)
				return varchar2 is


ds_retorno_w	varchar2(255);

begin

select 	max(ds_modulo)
into	ds_retorno_w
from 	tre_curso_modulo
where 	nr_sequencia = nr_seq_modulo_p;

return ds_retorno_w;

end Obter_descricao_modulo;
/