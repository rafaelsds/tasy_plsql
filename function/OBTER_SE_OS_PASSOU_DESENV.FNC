CREATE OR REPLACE
FUNCTION Obter_Se_OS_Passou_Desenv(
				nr_ordem_servico_p		Number)
				RETURN Varchar2 IS

ie_retorno_w			Varchar2(15);

BEGIN

ie_retorno_w			:= 'N';

select	nvl(max('S'),'N')
into	ie_retorno_w
from	man_estagio_processo b,
	Man_ordem_serv_estagio a
where	a.nr_seq_ordem		= nr_ordem_servico_p
and	a.nr_seq_estagio	= b.nr_sequencia
and	b.ie_desenv		= 'S';

RETURN ie_retorno_w;

END Obter_Se_OS_Passou_Desenv;
/