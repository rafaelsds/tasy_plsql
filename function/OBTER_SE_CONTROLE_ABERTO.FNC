create or replace
function obter_se_controle_aberto(nr_seq_controle_p	number,
				  nr_atendimento_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1) := 'N';  
qt_controle_w		number(10)  := 0;
			
begin

if 	(nr_seq_controle_p 	> 0) and
	(nr_atendimento_p	> 0) then
	
	select 	count(*)
	into	qt_controle_w
	from	cih_controle_atend
	where	nr_seq_controle = nr_seq_controle_p
	and	nr_atendimento 	= nr_atendimento_p
	and	dt_finalizacao is null;

end if;


if 	(qt_controle_w > 0) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end obter_se_controle_aberto;
/