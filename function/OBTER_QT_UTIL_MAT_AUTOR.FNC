create or replace
function obter_qt_util_mat_autor(	nr_sequencia_p	number,
				nr_atendimento_p	number,
				cd_convenio_p	number,
				cd_material_p	number)
				return number is

qt_autorizada_w			number(11,3)	:= 0;
qt_utilizada_w			number(11,3)	:= 0;
qt_saldo_w			number(11,3)	:= 0;

begin

/* obtem materiais autorizados */
select 	nvl(sum(nvl(a.qt_autorizada,0)),0)
into	qt_autorizada_w
from	material_autorizado a
where	a.nr_sequencia		= nr_sequencia_p;

/* obter materiais lan�ados */
begin
select	nvl(sum(qt_material),0)
into	qt_utilizada_w
from	material_atend_paciente
where	nr_atendimento		= nr_atendimento_p
and	cd_convenio		= cd_convenio_p
and	cd_material		= cd_material_p
and	cd_motivo_exc_conta	is null;
exception when others then
	qt_utilizada_w := 0;
end;

qt_saldo_w :=	nvl(qt_autorizada_w,0) - nvl(qt_utilizada_w,0);

return qt_saldo_w;
end obter_qt_util_mat_autor;
/