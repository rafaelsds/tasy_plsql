create or replace
function obter_maior_preco_brasindice(		cd_estabelecimento_p			number,
					cd_material_p				number,
					dt_vigencia_p				date,
					cd_unidade_medida_param_p			varchar2,
					ie_tipo_preco_p				varchar2)
 		    	return number is


vl_brasindice_w			number(15,4)	:= 0;
cd_unidade_medida_compra_w	varchar2(30);
cd_unidade_medida_estoque_w	varchar2(30);
cd_unidade_medida_consumo_w	varchar2(30);
qt_conv_compra_estoque_w		number(13,4);
qt_conv_estoque_consumo_w	number(13,4);
qt_existe_w			number(10);

cursor c01 is
	select	nvl(max(dividir(b.vl_preco_medicamento, nvl(a.qt_conversao,1))),0)
	from	material_brasindice a,
		brasindice_preco b
	where	a.cd_material		= cd_material_p
	and	a.cd_laboratorio		= b.cd_laboratorio
	and	a.cd_medicamento		= b.cd_medicamento
	and	a.cd_apresentacao		= b.cd_apresentacao
	and	nvl(a.ie_situacao, 'A')	= 'A'
	and	nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_p, 0))	= nvl(cd_estabelecimento_p, 0)
	and	b.ie_tipo_preco			= ie_tipo_preco_p
	and	nvl(b.dt_inicio_vigencia,dt_vigencia_p) <= dt_vigencia_p
	order by b.dt_inicio_vigencia;

begin

/* Adicionei este count pois em alguns clientes, se o material n�o existe na tabela
a consulta demorava mais de 4 minutos */
select	count(*)
into	qt_existe_w
from	material_brasindice a,
	brasindice_preco b
where	a.cd_material		= cd_material_p
and	a.cd_laboratorio		= b.cd_laboratorio
and	a.cd_medicamento		= b.cd_medicamento
and	a.cd_apresentacao		= b.cd_apresentacao
and	nvl(a.ie_situacao, 'A')	= 'A'
and	nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_p, 0))	= nvl(cd_estabelecimento_p, 0)
and	b.ie_tipo_preco			= ie_tipo_preco_p;

if	(qt_existe_w > 0) then
	begin
	
	open C01;
	loop
	fetch C01 into	
		vl_brasindice_w;
	exit when C01%notfound;
		begin
		null;
		end;
	end loop;
	close C01;

	if	(vl_brasindice_w > 0) then
		begin

		select	cd_unidade_medida_compra,
			cd_unidade_medida_estoque,
			cd_unidade_medida_consumo,
			qt_conv_compra_estoque,
			qt_conv_estoque_consumo
		into	cd_unidade_medida_compra_w,
			cd_unidade_medida_estoque_w,
			cd_unidade_medida_consumo_w,
			qt_conv_compra_estoque_w,
			qt_conv_estoque_consumo_w
		from	material
		where	cd_material = cd_material_p;
	
		if	(cd_unidade_medida_param_p = cd_unidade_medida_compra_w) then
			vl_brasindice_w	:= vl_brasindice_w * qt_conv_compra_estoque_w * qt_conv_estoque_consumo_w;
		elsif	(cd_unidade_medida_param_p = cd_unidade_medida_estoque_w) then
			vl_brasindice_w	:= vl_brasindice_w * qt_conv_estoque_consumo_w;
		elsif	(cd_unidade_medida_param_p = cd_unidade_medida_consumo_w) then
			vl_brasindice_w	:= vl_brasindice_w * qt_conv_estoque_consumo_w;
		end if;

		end;
	end if;

	end;
end if;

return	vl_brasindice_w;

end obter_maior_preco_brasindice;
/