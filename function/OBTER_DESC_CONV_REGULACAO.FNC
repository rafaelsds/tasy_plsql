create or replace
function Obter_desc_conv_regulacao(	nr_seq_convenio_p	number )
			return varchar2 is

ds_convenio_w	varchar2(255);

begin

if (nr_seq_convenio_p > 0) then

	Select 	max(obter_desc_convenio(cd_convenio))
	into	ds_convenio_w
	from   	convenio_regulacao
	where   nr_sequencia = nr_seq_convenio_p;
	
end if;

return	ds_convenio_w;

end Obter_desc_conv_regulacao;
/