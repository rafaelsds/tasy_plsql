create or replace
function obter_desc_tempos_cir (nr_seq_tempo_p		number)
				     return varchar is
				     
ds_retorno_w    varchar(255);

begin

if (nr_seq_tempo_p is not null) then

	SELECT max(SUBSTR(ds_tempo,1,150)) ds
	into   ds_retorno_w
	FROM   tempo_cirurgico
	WHERE  nr_sequencia = nr_seq_tempo_p;	
end if;


return	ds_retorno_w;

end obter_desc_tempos_cir;
/