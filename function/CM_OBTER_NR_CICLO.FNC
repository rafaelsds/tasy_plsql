create or replace
function	cm_obter_nr_ciclo(
		nr_seq_ciclo_p		number) return number is

nr_retorno_w		number(15);

begin

select	nr_ciclo
into	nr_retorno_w
from	cm_ciclo
where	nr_sequencia = nr_seq_ciclo_p;

return nr_retorno_w;

end cm_obter_nr_ciclo;
/