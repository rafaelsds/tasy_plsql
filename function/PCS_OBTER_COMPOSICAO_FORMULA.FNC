create or replace
function pcs_obter_composicao_formula(ds_macro_p varchar2)
 		    	return varchar2 is

ds_resultado_w	varchar2(3999);
begin

Select	ds_composicao
into	ds_resultado_w 
from	pcs_formulas
where	upper(ds_macro) = upper(ds_macro_p);

return	ds_resultado_w;

end pcs_obter_composicao_formula;
/