create or replace 
function TISS_OBTER_SE_MAT_CONTA( nr_seq_regra_guia_proc_p	number,
				  nr_interno_conta_p		number)
						return varchar2 is
cd_grupo_material_w	number(10);
cd_subgrupo_material_w	number(10);
cd_classe_material_w	number(10);
cd_material_w		number(10);
cd_grupo_material_ww	number(10);
cd_subgrupo_material_ww	number(10);
cd_classe_material_ww	number(10);
cd_material_ww		number(10);
ds_retorno_w		varchar2(2);

cursor	c01 is
select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material,
	cd_material
from 	tiss_regra_guia_conta_mat
where	nr_seq_regra		= nr_seq_regra_guia_proc_p;

cursor	c02 is
select	distinct cd_material
from 	material_atend_paciente
where	nr_interno_conta	= nr_interno_conta_p
and	cd_motivo_exc_conta	is null;

begin

ds_retorno_w		:= null;

open c01;
loop
fetch c01 into
	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	cd_material_w;
exit when c01%notfound or ds_retorno_w = 'S';
	
	ds_retorno_w		:= 'N';

	open c02;
	loop
	fetch c02 into
		cd_material_ww;
	exit when c02%notfound or ds_retorno_w = 'S';

		select	cd_grupo_material,
			cd_subgrupo_material,
			cd_classe_material
		into	cd_grupo_material_ww,
			cd_subgrupo_material_ww,
			cd_classe_material_ww
		from 	estrutura_material_v
		where	cd_material	= cd_material_ww;

		if	(cd_grupo_material_w is not null) and 
			(cd_grupo_material_w = cd_grupo_material_ww) then
			ds_retorno_w		:= 'S';
		end if;
		if	(cd_subgrupo_material_w is not null) and
			(cd_subgrupo_material_w = cd_subgrupo_material_ww) then
			ds_retorno_w		:= 'S';
		end if;
		if	(cd_classe_material_w is not null) and
			(cd_classe_material_w = cd_classe_material_ww) then
			ds_retorno_w		:= 'S';
		end if;
		if	(cd_material_w is not null) and
			(cd_material_w = cd_material_ww) then
			ds_retorno_w		:= 'S';
		end if;
	end loop;
	close c02;

end loop;
close c01;

return	ds_retorno_w;

end TISS_OBTER_SE_MAT_CONTA;
/