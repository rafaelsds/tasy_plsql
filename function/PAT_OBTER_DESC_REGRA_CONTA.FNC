create or replace
function pat_obter_desc_regra_conta(	nr_sequencia_p		number)
				return varchar2 is

ds_regra_w	varchar2(255) := '';
			
begin

if (nvl(nr_sequencia_p,0) <> 0) then
	select	ds_regra
	into	ds_regra_w
	from	pat_conta_contabil
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_regra_w;

end pat_obter_desc_regra_conta;
/