create or replace
function Obter_carteira_tit_rec (
			nr_seq_carteira_p	number)
 		    	return varchar2 is

ds_carteria_w	varchar2(255);
			
begin

if	(nr_seq_carteira_p is not null) then
	select	substr(cd_carteira || ' - ' || ds_carteira,1,255)
	into	ds_carteria_w
	from	banco_carteira
	where	nr_sequencia = nr_seq_carteira_p;
end if;

return	ds_carteria_w;

end Obter_carteira_tit_rec;
/