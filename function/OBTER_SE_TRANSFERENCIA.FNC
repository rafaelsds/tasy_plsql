CREATE OR REPLACE 
FUNCTION obter_se_transferencia
			(nr_atendimento_p	number)
				return varchar is

ie_transferencia_w	varchar2(1):= 'N';


begin

select	nvl(max(b.ie_transferencia), 'N') ie_transferencia 
into	ie_transferencia_w
from	motivo_alta		b,
	atendimento_paciente	a
WHERE	a.nr_atendimento	= nr_atendimento_p
AND	a.cd_motivo_alta	= b.cd_motivo_alta;

return	ie_transferencia_w;

END obter_se_transferencia;
/