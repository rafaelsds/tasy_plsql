create or replace
function Oncotech_Obter_Se_Pac_Quimio(cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

qt_quimio_w	number;
ie_quimio_w	varchar2(1) := 'N';
			
begin

select count(*)
into	qt_quimio_w
from   atendimento_paciente
where  cd_pessoa_fisica = cd_pessoa_fisica_p
and	ie_tipo_atendimento in (7,8)
and	ie_clinica = '5'
and	nr_seq_classificacao = '5';

if (qt_quimio_w > 0) then
	ie_quimio_w := 'S';
end if;

return	ie_quimio_w;

end Oncotech_Obter_Se_Pac_Quimio;
/
