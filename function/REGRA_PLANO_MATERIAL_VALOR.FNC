create or replace function regra_plano_material_valor(nr_seq_regra_p			number,
				cd_estabelecimento_p	number,
			cd_convenio_p          	number,
			cd_categoria_p         	varchar2,
			dt_vigencia_p          	date,
			cd_material_p          	number,
			cd_tipo_acomodacao_p   	number,
			ie_tipo_atendimento_p  	number,
			cd_setor_atendimento_p 	number,
            cd_plano_p              number,
            nr_atendimento_p        number,
            qt_material_p           number,
            ie_valor_dia_p          varchar2,
            ie_valor_unitario_p     varchar2,
            ie_mat_conta_p          varchar2) return varchar2 is

vl_material_min_w   regra_convenio_plano_mat.vl_material_min%type;
vl_material_max_w   regra_convenio_plano_mat.vl_material_max%type;
ie_valor_w          regra_convenio_plano_mat.ie_valor%type;
ie_valor_dia_w      regra_convenio_plano_mat.ie_valor_dia%type;
ie_valor_unitario_w regra_convenio_plano_mat.ie_valor_unitario%type;
ie_mat_conta_w		regra_convenio_plano_mat.ie_mat_conta%type;
cd_material_w			number(15, 4);
cd_material_preco_w		number(15, 4);
cd_categoria_w			varchar2(10);
vl_referencia_w			number(15,4)	:= 0;
qt_material_w			number(10,0);
ie_aplica_w             varchar2(1)	:= 'S';
vl_referencia_conta_w	number(15,4)	:= 0;

CURSOR C01 IS
select	ie_valor,
	nvl(vl_material_min, 0),
	nvl(vl_material_max, 0),
	nvl(nvl(ie_valor_dia_p,ie_valor_dia),'N'),
	nvl(nvl(ie_valor_unitario_p,ie_valor_unitario),'N'),
    nvl(nvl(ie_mat_conta_p,ie_mat_conta),'N') 
from 	regra_Convenio_Plano_mat
where	nr_sequencia							= nr_seq_regra_p;

begin   
    cd_material_w := cd_material_p;
    ie_aplica_w := 'S';

   open c01;
	loop
	fetch c01 into
		ie_valor_w,
		vl_material_min_w,
		vl_material_max_w,
		ie_valor_dia_w,
		ie_valor_unitario_w,
        ie_mat_conta_w;
	exit when c01%notfound;
        if	(ie_valor_w in ('S','F')) then
            select	max(decode(nvl(ie_mat_conta_w,'N'), 'N', cd_material_w, obter_material_conta(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, cd_material_w, cd_material_w, cd_material_w, cd_plano_p, cd_setor_atendimento_p, sysdate, 0, 0)))
			into	cd_material_preco_w
			from	dual;
        end if;

        if	(nvl(cd_categoria_p,0) = 0) then
            select	min(cd_categoria)
            into	cd_categoria_w
            from	categoria_convenio
            where	cd_convenio	= cd_convenio_p
            and	    ie_situacao	= 'A';
        end if;

        vl_referencia_w := OBTER_PRECO_MATERIAL(cd_estabelecimento_p, cd_convenio_p, nvl(cd_categoria_p,cd_categoria_w), sysdate, cd_material_w, cd_tipo_acomodacao_p, ie_tipo_atendimento_p, cd_setor_atendimento_p,null,0,0);

        select	decode(qt_material_p,0,1,qt_material_p)
		into	qt_material_w
		from	dual;

		if	(nvl(ie_valor_unitario_w,'N') = 'S') then
			qt_material_w	:= 1;
		end if;

        if	(nvl(ie_valor_dia_w,'N') = 'N') then

			if	(ie_valor_w = 'S') then		-- Aplicar regra quando valor fora da faixa
				if	((nvl(qt_material_w,1) * vl_referencia_w) between vl_material_min_w and vl_material_max_w) then
					ie_aplica_w := 'N';
				end if;
			elsif	(ie_valor_w = 'F') then -- Aplicar regra quando valor dentro da faixa

				if	not((nvl(qt_material_w,1) * vl_referencia_w) between vl_material_min_w and vl_material_max_w) then
					ie_aplica_w := 'N';
				end if;
			end if;
		elsif	(nvl(ie_valor_dia_w,'N') = 'S')and
			(cd_material_w is not null) and
			(nr_atendimento_p is not null) then


			begin
			select	nvl(sum(vl_material),0)
			into	vl_referencia_conta_w
			from	material_atend_paciente
			where	cd_material = cd_material_w
			and	nr_atendimento = nr_atendimento_p
			and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atendimento) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
			and	cd_motivo_exc_conta is null;
			exception
			when others then
				vl_referencia_conta_w := 0;
			end;

			if	(ie_valor_w = 'S') then

				if	(((nvl(qt_material_w,1) * vl_referencia_w) + vl_referencia_conta_w) between vl_material_min_w and vl_material_max_w) then
					ie_aplica_w := 'N';
				end if;
			elsif	(ie_valor_w = 'F') then

				if	not(((nvl(qt_material_w,1) * vl_referencia_w) + vl_referencia_conta_w) between vl_material_min_w and vl_material_max_w) then
					ie_aplica_w := 'N';
				end if;
			end if;
		end if;

    end loop;


    return ie_aplica_w;

end regra_plano_material_valor;
/
