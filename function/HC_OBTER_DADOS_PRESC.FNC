create or replace
function hc_obter_dados_presc	(	nr_seq_paciente_p	Number,
					dt_referencia_p		Date,
					nr_seq_agenda_p		number default null,
					ie_opcao_p		Varchar2)
 		    	return Varchar2 is
			
/* ie_opcao_p
'M'	Medicamento - Diluição
'R'	Recomendações
*/
			

nr_atendimento_w	number(10);	
nr_prescricao_w		number(10);	
ds_medicamento_w	varchar2(2000);	
ds_retorno_w		varchar2(4000);	
ds_recomendacao_w	Varchar2(4000) := '';
qt_registros_w		number(10);
dt_agenda_w		date;

Cursor C01 is
	select	substr(obter_desc_material(a.cd_material)||' - '||obter_diluicao_medic(a.nr_sequencia,a.nr_prescricao),1,255),
		substr(a.ds_observacao,1,255)
	from	prescr_material a,
		prescr_medica b,
		prescr_mat_hor c
	where	a.nr_prescricao = b.nr_prescricao
	and	c.nr_prescricao = a.nr_prescricao
	and	c.nr_seq_material = a.nr_sequencia
	and	trunc(dt_referencia_p) between inicio_dia(nvl(b.dt_inicio_prescr,b.dt_prescricao)) and fim_dia(nvl(dt_validade_prescr,b.dt_prescricao))
	and	b.nr_atendimento = nr_atendimento_w
	and	b.nr_prescricao = nvl(nr_prescricao_w,b.nr_prescricao)
	and	c.dt_horario = dt_referencia_p
	and	b.dt_suspensao is null
	and	a.ie_suspenso <> 'S'
	and	c.dt_suspensao is null;
	
Cursor C02 is
	select	substr(obter_desc_material(a.cd_material)||' - '||obter_diluicao_medic(a.nr_sequencia,a.nr_prescricao),1,255),
		substr(a.ds_observacao,1,255)
	from	prescr_material a,
		prescr_medica b,
		prescr_mat_hor c
	where	a.nr_prescricao = b.nr_prescricao
	and	c.nr_prescricao = a.nr_prescricao
	and	c.nr_seq_material = a.nr_sequencia
	and	trunc(dt_referencia_p) between inicio_dia(nvl(b.dt_inicio_prescr,b.dt_prescricao)) and fim_dia(nvl(dt_validade_prescr,b.dt_prescricao))
	and	b.nr_atendimento = nr_atendimento_w
	and	a.ds_observacao is not null
	and	b.nr_prescricao = nvl(nr_prescricao_w,b.nr_prescricao)
	and	c.dt_horario = dt_referencia_p
	and	b.dt_suspensao is null
	and	a.ie_suspenso <> 'S'
	and	c.dt_suspensao is null;
	
begin
if (nr_seq_paciente_p is not null) then
	begin
	
	select	max(a.nr_atendimento)
	into	nr_atendimento_w
	from	paciente_hc_atend a
	where 	a.nr_seq_paciente = nr_seq_paciente_p
	and	a.nr_atendimento is not null;
	
	if	(nr_atendimento_w is null) then
		select	nr_atendimento_origem
		into	nr_atendimento_w
		from	paciente_home_care
		where	nr_sequencia = nr_seq_paciente_p;
		
	end if;
	
	
	
	
	select	max(nr_prescricao),
		max(dt_agenda)
	into	nr_prescricao_w,
		dt_agenda_w
	from	agenda_hc_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	
	select	count(*) 
	into	qt_registros_w
	from	agenda_hc_paciente a
	where	cd_pessoa_fisica = (	select	max(x.cd_pessoa_fisica)
					from	paciente_home_care x
					where	x.nr_sequencia = nr_seq_paciente_p)
	and	dt_agenda = dt_agenda_w;
					
	if	(qt_registros_w > 1) then
		nr_prescricao_w := null;
	end if;
	
	/*where	a.nr_sequencia = (select max(b.nr_sequencia)
					from paciente_hc_atend b
					where	b.nr_seq_paciente = nr_seq_paciente_p
					and	b.nr_atendimento is not null);	*/
	if (ie_opcao_p = 'M') then
		begin
		open C01;
		loop
		fetch C01 into	
			ds_medicamento_w,
			ds_recomendacao_w;
		exit when C01%notfound;
			begin
			ds_retorno_w :=	ds_retorno_w || ' / ' || ds_medicamento_w;
			end;
		end loop;
		close C01;
		
		ds_retorno_w :=	substr(ds_retorno_w,4,255);
		end;
	end if;
	if (ie_opcao_p = 'R') then
		begin
		open C02;
		loop
		fetch C02 into	
			ds_medicamento_w,
			ds_recomendacao_w;
		exit when C02%notfound;
			begin
			ds_retorno_w :=  ds_retorno_w || '/'|| substr(ds_recomendacao_w,1,255);
			end;
		end loop;
		close C02;
		end;
	end if;
	end;
end if;

return	ds_retorno_w;

end hc_obter_dados_presc;
/