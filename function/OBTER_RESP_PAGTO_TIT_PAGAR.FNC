create or replace
function OBTER_RESP_PAGTO_TIT_PAGAR
	(nr_titulo_p	varchar2,
	 ie_opcao_p	varchar2) return varchar2 is

/*
ie_opcao_p

	PF - Pessoa f�sica
	PJ - Pessoa_juridica
	A - Ambas (PF ou PJ)

*/

cd_cgc_w			varchar2(255);
cd_pessoa_fisica_w		varchar2(255);
ds_retorno_w			varchar2(255);

begin

select	max(cd_cgc), 
	max(cd_pessoa_fisica)
into	cd_cgc_w, 
	cd_pessoa_fisica_w
from	titulo_pagar_resp_pagto
where	nr_titulo	= nr_titulo_p;

if	(ie_opcao_p = 'PF') then
	ds_retorno_w	:= cd_pessoa_fisica_w;
elsif	(ie_opcao_p = 'PJ') then
	ds_retorno_w	:= cd_cgc_w;
elsif	(ie_opcao_p = 'PJ') then
	ds_retorno_w	:= nvl(cd_cgc_w, cd_pessoa_fisica_w);
end if;

return	ds_retorno_w;

end OBTER_RESP_PAGTO_TIT_PAGAR;
/
