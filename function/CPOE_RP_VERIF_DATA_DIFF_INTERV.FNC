create or replace function cpoe_rp_verif_data_diff_interv (
                                                                nr_seq_rp_p   in    cpoe_rp.nr_sequencia%type, 
                                                                dt_inicio_rp_p in   cpoe_rp.dt_inicio%type  default null, 
                                                                dt_fim_rp_p in  cpoe_rp.dt_fim%type  default null
                                                            ) return varchar2 is 
fnc_return_w    cpoe_rp.IE_ENABLE_END_DT%type;
qtd_operacao_w    pls_integer;

cursor C001 is 
    select 
        DT_ADMINISTRATION 
    from 
        CPOE_DIFF_DATE_RP 
    where 
        NR_SEQ_CPOE_RP = nr_seq_rp_p 
    order by DT_ADMINISTRATION;

begin 

    qtd_operacao_w := 0;
    
    select 
        count(1)
    into 
        qtd_operacao_w
    from 
        CPOE_DIFF_DATE_RP 
    where 
        NR_SEQ_CPOE_RP = nr_seq_rp_p 
    order by DT_ADMINISTRATION;
    
    
    if (qtd_operacao_w > 0) then 
    
        if ((nr_seq_rp_p is not null) and (dt_inicio_rp_p is not null) and (dt_fim_rp_p is not null)) then 
        
            for record_line in C001 
            loop
                if ((trunc(record_line.DT_ADMINISTRATION) < trunc(dt_inicio_rp_p)) or (trunc(record_line.DT_ADMINISTRATION) > trunc(dt_fim_rp_p))) then 
                    fnc_return_w := 'N';
                else 
                    fnc_return_w := 'S';
                end if;
            end loop;
        
        else
    
            fnc_return_w := 'S';
    
        end if;
    
    else 
    
        fnc_return_w := 'S';
    
    end if;

    return fnc_return_w;

end cpoe_rp_verif_data_diff_interv;
/
