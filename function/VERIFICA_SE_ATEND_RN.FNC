create or replace
function verifica_se_atend_rn( nr_atendimento_p		number)
 		    	return varchar2 is

ie_possui_w	varchar2(1);			
begin

select	nvl(max('S'),'N')
into	ie_possui_w
from   	nascimento
where  	nr_atendimento = nr_atendimento_p
and	nr_atend_rn is not null;

return	ie_possui_w;

end verifica_se_atend_rn;
/