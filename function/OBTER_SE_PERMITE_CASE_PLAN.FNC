create or replace
function obter_se_permite_case_plan
        return varchar2 is

ie_permite_case_planejado_w varchar2(1) := 'S';
cd_estabelecimento_w 		estabelecimento.cd_estabelecimento%type;
cd_perfil_w 				perfil.cd_perfil%type;

cursor c_regra is
    select   1 ordem,  
            nvl(a.ie_case_futuro , 'S') IE_CASE_FUTURO 
    from     parametros_episodio a
    where    a.cd_estabelecimento = cd_estabelecimento_w
    and      a.cd_perfil = cd_perfil_w
    union all
    select   2 ordem, 
            nvl(a.IE_CASE_FUTURO , 'S')
    from     parametros_episodio a
    where    a.cd_estabelecimento is null
    and      a.cd_perfil = cd_perfil_w
    union all
    select   3 ordem, 
            nvl(a.IE_CASE_FUTURO , 'S')
    from     parametros_episodio a
    where    a.cd_estabelecimento = cd_estabelecimento_w
    and      a.cd_perfil is null
    union all
    select   4 ordem, 
            nvl(a.IE_CASE_FUTURO , 'S')
    from     parametros_episodio a
    where    a.cd_estabelecimento is null
    and      a.cd_perfil is null
    order by ordem;
    
begin

if (obter_uso_case = 'S') then
    cd_estabelecimento_w    := obter_estabelecimento_ativo;
    cd_perfil_w             := obter_perfil_ativo;

    for linha_regra in c_regra loop
        ie_permite_case_planejado_w := linha_regra.IE_CASE_FUTURO;
        exit;
    end loop;

end if;
return  ie_permite_case_planejado_w;

end obter_se_permite_case_plan;
/