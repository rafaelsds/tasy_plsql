create or replace 
FUNCTION via_obter_desc_evento(nr_seq_evento_p  NUMBER)
        RETURN VARCHAR2 IS

ds_retorno_w VARCHAR2(255);

BEGIN

SELECT SUBSTR(ds_evento,1,255)
INTO ds_retorno_w
FROM via_evento
WHERE nr_sequencia = nr_seq_evento_p;


RETURN ds_retorno_w;

END via_obter_desc_evento;
/
