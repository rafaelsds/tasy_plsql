/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
create or replace
function pls_obter_se_item_exec_ocorr
			(	nr_seq_execucao_p	number,
				nr_seq_item_p		number,
				ie_tipo_item_p		number)
				return Varchar2 is
			
ds_retorno_w			varchar2(1) := 'N';
qt_ocorrencia_w			number(10)  := 0;

begin

if	(ie_tipo_item_p = 10) then 	
	select	count(1)
	into	qt_ocorrencia_w 
	from	pls_ocorrencia_benef	b,
		pls_ocorrencia		a
	where	b.nr_seq_execucao = nr_seq_execucao_p
	and	b.nr_seq_ocorrencia = a.nr_sequencia
	and	a.ie_auditoria = 'S'
	and	nr_seq_proc	= nr_seq_item_p;
	
elsif	(ie_tipo_item_p = 11) then 	
	select	count(1)
	into	qt_ocorrencia_w 
	from	pls_ocorrencia_benef	b,
		pls_ocorrencia		a
	where	b.nr_seq_execucao = nr_seq_execucao_p
	and	b.nr_seq_ocorrencia = a.nr_sequencia
	and	a.ie_auditoria = 'S'
	and	nr_seq_mat	= nr_seq_item_p;
end if;

if	(qt_ocorrencia_w > 0) then 
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end pls_obter_se_item_exec_ocorr;
/
