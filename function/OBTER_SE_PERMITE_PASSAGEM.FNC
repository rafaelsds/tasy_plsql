create or replace
function obter_se_permite_passagem(nr_atendimento_p	number,
				   dt_passagem_p	date,
				   cd_estabelecimento_p	number)
 		    	return varchar2 is

dt_prim_passagem_w		date;
ie_permite_passagem_inf_w	varchar2(1);	
ie_retorno_w			varchar2(1);
			
begin

select	min(dt_entrada_unidade)
into	dt_prim_passagem_w
from 	atend_paciente_unidade
where   nr_atendimento = nr_atendimento_p;

select	nvl(max(ie_permite_pass_inf_prim_pass),'S')
into	ie_permite_passagem_inf_w
from	parametro_atendimento
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_permite_passagem_inf_w = 'N') and
	(dt_passagem_p < dt_prim_passagem_w) then
	ie_retorno_w := 'N';
else
	ie_retorno_w := 'S';
end if;

return	ie_retorno_w;

end obter_se_permite_passagem;
/