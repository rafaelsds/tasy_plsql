create or replace function man_obter_dados_equip_html5(	nr_seq_equipamento_p	number,
					ie_opcao_p		varchar2)
 		    	return date is

dt_retorno_w			date;

/*ie_opcao_p

IG - In�cio Garantia
FG - Fim Garantia
*/
begin

if	(ie_opcao_p = 'FG') then
	select	max(dt_fim_garantia)
	into	dt_retorno_w
	from	man_equipamento a
	where	nr_sequencia = nr_seq_equipamento_p;

elsif (ie_opcao_p = 'IG') then
	select	max(dt_inicio_garantia)
	into	dt_retorno_w
	from	man_equipamento a
	where	nr_sequencia = nr_seq_equipamento_p;

end if;

return	dt_retorno_w;

end man_obter_dados_equip_html5;

/