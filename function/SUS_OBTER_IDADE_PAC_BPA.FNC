CREATE OR REPLACE
FUNCTION Sus_Obter_Idade_Pac_Bpa
		(	cd_pessoa_fisica_p	Varchar2,
			cd_procedimento_p	Number,
			ie_origem_proced_p	Number,
			dt_atual_p		Date,
			ie_opcao_p		Varchar2)
			return varchar2 is


dt_nascimento_w		Date;
ie_idade_aplica_w	Varchar2(1)	:= 'N';
ds_retorno_w		Varchar2(5)	:= '';


BEGIN

select	dt_nascimento
into	dt_nascimento_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

/*
select	nvl(max(ie_idade_aplica),'N')
into	ie_idade_aplica_w
from	sus_procedimento
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
*/

if	(cd_procedimento_p in (0301010013, 0301010021, 0301010030, 0301010048, 0301010056, 0301010064, 0301010072, 
				0301010080, 0301010099, 0301010102, 0301010110, 0301010129, 0301010137, 0301010153)) then
	ie_idade_aplica_w	:= 'S';
end if;

if	(ie_idade_aplica_w	= 'S') then
	return obter_idade(dt_nascimento_w, dt_atual_p, ie_opcao_p);
elsif	(ie_idade_aplica_w	= 'N') then
	return ds_retorno_w;
end if;

END Sus_Obter_Idade_Pac_Bpa;
/