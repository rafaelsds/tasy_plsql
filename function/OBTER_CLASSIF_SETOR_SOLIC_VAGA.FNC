create or replace
function obter_classif_setor_solic_vaga(nr_atendimento_p	number)
 		    	return varchar2 is
ds_retorno_w	varchar2(255);
vl_param_w		varchar2(255);
nr_sequencia_w	gestao_vaga.nr_sequencia%type;
begin
if (nvl(nr_atendimento_p,0) > 0) then
	begin
		select	max(nr_sequencia)
		into	nr_sequencia_w
		from 	gestao_vaga
		where	nr_atendimento = nr_atendimento_p
		and		cd_setor_desejado is not null
		and		cd_unidade_basica is not null; 					

		if (nvl(nr_sequencia_w,0) > 0) then 
			select	max(obter_valor_dominio(1,obter_classif_setor(cd_setor_desejado)))
			into	ds_retorno_w
			from 	gestao_vaga
			where	nr_sequencia = nr_sequencia_w; 
		end if;
	exception
	when others then
		ds_retorno_w := null;
	end;
	
end if;
return	ds_retorno_w;

end obter_classif_setor_solic_vaga;
/
