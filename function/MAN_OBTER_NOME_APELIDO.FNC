create or replace
function Man_Obter_Nome_Apelido(cd_pessoa_solicitante_p		varchar2)
 		    	return varchar2 is

/*
Esta function ira retornar o nome ou o apelido do solicitante da OS conforme o valor (S ou N) do parametro [68] da funcao Gestao de Ordens de Servico 
OS 1347673
*/				
				
ie_nome_apelido_w	varchar2(255);
ie_param_w			varchar2(1);
				
begin

ie_param_w := nvl(Obter_Valor_Param_Usuario(296,68,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento), 'S');

if(ie_param_w = 'S' )then
	ie_nome_apelido_w := substr(nvl(obter_apelido_pessoa(cd_pessoa_solicitante_p), obter_nome_pf(cd_pessoa_solicitante_p)),1,60);
else
	ie_nome_apelido_w := substr(obter_nome_pf(cd_pessoa_solicitante_p),1,60);
end if;

return	ie_nome_apelido_w;

end Man_Obter_Nome_Apelido;
/
