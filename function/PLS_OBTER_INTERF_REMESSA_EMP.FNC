create or replace
function pls_obter_interf_remessa_emp (
		nr_seq_empresa_p	number)
		return number is

cd_interface_w	number(5,0);

begin
if	(nr_seq_empresa_p is not null) then
	begin
	select	nvl(max(cd_interface_remessa),0)
	into	cd_interface_w
	from	pls_desc_empresa
	where	nr_sequencia = nr_seq_empresa_p;
	end;
end if;
return cd_interface_w;
end pls_obter_interf_remessa_emp;
/