create or replace
Function pls_obter_via_cartao_interc(	nr_seq_segurado_p	number) 
					return number is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter a via do cart�o do benefici�rio
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_via_cartao_w			number(5);
nr_seq_congenere_w		number(10);
ie_tipo_envio_via_w		number(10);

begin

select	max(nvl(nr_via_solicitacao,1))
into	nr_via_cartao_w
from	pls_segurado_carteira
where	nr_seq_segurado	= nr_seq_segurado_p;

	begin
		select	nr_seq_congenere
		into	nr_seq_congenere_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_p;
	exception
	when others then
		nr_seq_congenere_w	:= null;
	end;
	
select	max(ie_tipo_envio_via)
into	ie_tipo_envio_via_w
from	pls_regra_limite_envio_scs
where	(nr_seq_congenere	is not null
or	nr_seq_congenere	= nr_seq_congenere_w);

if	(ie_tipo_envio_via_w	= 2) then
	nr_via_cartao_w	:= 0;
end if;


return	nr_via_cartao_w;

end pls_obter_via_cartao_interc;
/