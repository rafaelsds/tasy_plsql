create or replace
function obter_qt_lib_parcela_neg_cr(nr_seq_parcela_p	number)
					return number is

qt_retorno_w	number(10)	:= null;

begin

if	(nr_seq_parcela_p is not null) then
	select	count(*)
	into	qt_retorno_w
	from	negociacao_cr_parcela_lib a
	where	a.nr_seq_parcela	= nr_seq_parcela_p;
end if;

return	qt_retorno_w;

end obter_qt_lib_parcela_neg_cr;
/