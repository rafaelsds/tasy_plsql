create or replace
function pls_obter_cbos_medico
		(	cd_medico_p		Number)
 		    	return Varchar2 is
			
ds_cbo_w		Varchar2(255);
ds_cbo_ww		Varchar2(255);
			
Cursor C01 is
	select	a.nr_seq_cbo_saude||' - '||b.ds_cbo
	from	medico_especialidade a,
		cbo_saude b
	where	a.nr_seq_cbo_saude = b.nr_sequencia
	and	a.cd_pessoa_fisica = cd_medico_p;
	
begin

open C01;
loop
fetch C01 into	
	ds_cbo_w;
exit when C01%notfound;
	begin
	select	ds_cbo_ww||decode(ds_cbo_ww,'','',', ')||ds_cbo_w
	into	ds_cbo_ww
	from	dual;
	end;
end loop;
close C01;

return	ds_cbo_ww;

end pls_obter_cbos_medico;
/
