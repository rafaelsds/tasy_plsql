create or replace
function com_obter_se_cnpj_ja_cliente(
		nr_seq_cliente_p	number,
		cd_cnpj_p		varchar2,
		ie_produto_p		varchar2)
		return varchar2 is
		
ie_cnpj_ja_cliente_w	varchar2(1) := 'N';
		
begin
if	(nr_seq_cliente_p is not null) and
	(cd_cnpj_p is not null) and
	(ie_produto_p is not null) then
	begin
	select	decode(count(*),0,'N','S')
	into	ie_cnpj_ja_cliente_w
	from	com_cliente
	where	nr_sequencia <> nr_seq_cliente_p
	and	cd_cnpj = cd_cnpj_p
	and	ie_produto = ie_produto_p;
	end;
end if;
return ie_cnpj_ja_cliente_w;
end com_obter_se_cnpj_ja_cliente;
/