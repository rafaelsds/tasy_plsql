create or replace
function obter_nome_medico_externo (	cd_pessoa_fisica_paciente_p	varchar2) return varchar2 is

nm_medico_w               varchar2(255):='';

begin

if(cd_pessoa_fisica_paciente_p is not null)then
	select  max(substr(obter_nome_medico(cd_medico, 'N'),1,255))
	into 	nm_medico_w
	from    pf_medico_externo 	
	where	cd_pessoa_fisica = cd_pessoa_fisica_paciente_p;
end if;

return  nm_medico_w;

end obter_nome_medico_externo;
/