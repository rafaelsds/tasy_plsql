create or replace
function obter_se_vinc_agenda_cons(nr_seq_agenda_p	number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(1);

begin
ds_retorno_w := 'N';
if 	(nr_seq_agenda_p is not null) then

	select  nvl(max('S'),'N')
	into	ds_retorno_w
	from	agenda_consulta
	where	nr_seq_agepaci = nr_seq_agenda_p;
end if;


return	ds_retorno_w;

end obter_se_vinc_agenda_cons;
/