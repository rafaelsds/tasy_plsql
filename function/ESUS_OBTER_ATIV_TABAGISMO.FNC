create or replace
function esus_obter_ativ_tabagismo( nr_sequencia_p		esus_atividade_coletiva.nr_sequencia%type)
				return varchar2 is
				
ds_retorno_w		varchar2(15)	:= 'N';
qt_reg_w		number(10)	:= 0;				
	
begin

begin
select	count(1)
into	qt_reg_w
from	esus_atividade_coletiva
where	nr_sequencia = nr_sequencia_p
and	((nvl(ie_pnct_sessao_1,'X') = 'S') or
	(nvl(ie_pnct_sessao_2,'X') = 'S') or
	(nvl(ie_pnct_sessao_3,'X') = 'S') or
	(nvl(ie_pnct_sessao_4,'X') = 'S'));
exception
when others then
	qt_reg_w := 0;
end;

if	(qt_reg_w > 0) then
	ds_retorno_w := 'S';
end if;
	
return	ds_retorno_w;

end esus_obter_ativ_tabagismo;
/