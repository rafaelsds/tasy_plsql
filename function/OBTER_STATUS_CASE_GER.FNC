create or replace function obter_status_case_ger	(
		nr_sequencia_p	number,
		ie_opcao_p        	varchar2	default	null
	) return varchar2 is
    
ie_status_case_w	varchar2(50);
ie_planned_w		varchar2(1) := 'P';
ie_cancel_w     	varchar2(1) := 'C';
ie_current_w     	varchar2(1) := 'A';
ie_expression_desc_w	varchar2(1) := 'E';
ie_fim_episodio_w       varchar2(1) := 'X';
    
begin
		select  
			case 
				when dt_cancelamento is not null then ie_cancel_w
				when dt_fim_episodio is not null then ie_fim_episodio_w
				when dt_episodio > sysdate then ie_planned_w
				else ie_current_w
			end
		into	ie_status_case_w
		from    episodio_paciente
		where   nr_sequencia = nr_sequencia_p;    

    	if (ie_opcao_p = ie_expression_desc_w) then
			if (ie_status_case_w = ie_planned_w) then
				ie_status_case_w := obter_desc_expressao(497767);
			elsif(ie_status_case_w =  ie_cancel_w) then
				ie_status_case_w := obter_desc_expressao(322155);
			elsif(ie_status_case_w =  ie_fim_episodio_w) then
				ie_status_case_w := obter_desc_expressao(290046);
			else		
				ie_status_case_w := obter_desc_expressao(283952);
			end if;
    	end if;
    
    	return ie_status_case_w;

end obter_status_case_ger;
/