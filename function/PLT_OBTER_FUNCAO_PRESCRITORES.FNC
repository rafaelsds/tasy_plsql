create or replace
function PLT_obter_funcao_prescritores( nr_prescricoes_p	varchar2,
										ie_tipo_item_p		varchar2,
										ie_tipo_retorno_p	varchar2 default 'DS')
							return varchar2 is

type campos is record  (	nr_prescricao		number(14),
							nm_prescritor 		varchar2(255),
							nm_usuario		varchar(15),
							ds_funcao 		varchar(25));
						
type Vetor is table of campos index by binary_integer;	

Vetor_w				Vetor;					
nr_prescricoes_w	varchar2(255);
ds_funcoes_w		varchar2(2000);
nm_usuario_w		varchar2(15);
nm_prescritor_w		varchar2(255);
ie_lista_w			varchar2(1);
ds_separador_w		char(2) := ', ';
i					number(10);
										
procedure ordenar_vetor is
x		number(10);
chave	campos;
y		number(10);
begin
for x in 2..Vetor_w.count loop
	begin
	y := x;
	chave := Vetor_w(x);
	while 	(y > 1) and 
		(chave.nm_prescritor < Vetor_w(y-1).nm_prescritor) loop
		begin
		Vetor_w(y) := Vetor_w(y-1);
		y := y - 1;
		end;
	end loop;	
	Vetor_w(y) := chave;
	end;
end loop;
end;

begin
nr_prescricoes_w	:= nr_prescricoes_p;
i 	:= 0;

if	(ie_tipo_retorno_p = 'CD') then
	ds_separador_w	:= ',';
end if;

while (length(nr_prescricoes_w) > 0) loop 
	begin
	i	:= i+1;
	if	(instr(nr_prescricoes_w,',') > 0)  then
		Vetor_w(i).nr_prescricao	:= somente_numero(substr(nr_prescricoes_w,1,instr(nr_prescricoes_w,',')));
		nr_prescricoes_w	:= substr(nr_prescricoes_w,instr(nr_prescricoes_w,',')+1,40000);
	else
		Vetor_w(i).nr_prescricao	:= somente_numero(nr_prescricoes_w);
		nr_prescricoes_w	:= null;
	end if;
	
	if	(ie_tipo_item_p	= 'E') then
		select	max(substr(obter_nome_pf(cd_prescritor),1,255)),
				max(nm_usuario_nrec)
		into	nm_prescritor_w,
				nm_usuario_w				
		from	pe_prescricao
		where	nr_sequencia = Vetor_w(i).nr_prescricao;
	else
		select	max(substr(obter_nome_pf(cd_prescritor),1,255)),
				max(nm_usuario_original)
		into	nm_prescritor_w,
				nm_usuario_w
		from	prescr_medica
		where	nr_prescricao = Vetor_w(i).nr_prescricao;
	end if;
	
	ie_lista_w := 'N';
	
	for x in 1..Vetor_w.count loop
		begin
		if 	(Vetor_w(x).nm_prescritor = nm_prescritor_w) and 
			(x <> i)	then
			ie_lista_w := 'S';
		end if;
		end;		
	end loop;
		
	if	(ie_lista_w = 'N') then	
		Vetor_w(i).nm_prescritor	:= nm_prescritor_w;
		
		if	(ie_tipo_retorno_p = 'CD') then
			Vetor_w(i).ds_funcao	:= Obter_funcao_usuario(nm_usuario_w);
		elsif	(ie_tipo_retorno_p = 'DS') then
			Vetor_w(i).ds_funcao		:= upper(substr(Obter_funcao_usuario_orig(nm_usuario_w),1,3));
		end if;
	end if;
	end;
end loop;

ordenar_vetor;

for j in 1..Vetor_w.count loop
	begin
	if	(Vetor_w(j).ds_funcao is not null) then
		if 	(nvl(ds_funcoes_w, 'XPTO') = 'XPTO') then
			ds_funcoes_w := Vetor_w(j).ds_funcao;
		else
			ds_funcoes_w := ds_funcoes_w || ds_separador_w || Vetor_w(j).ds_funcao;
		end if;
	end if;
	end;
end loop;										

										
return	ds_funcoes_w;

end PLT_obter_funcao_prescritores;
/