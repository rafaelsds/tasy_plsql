create or replace
Function Obter_Desconto_item_conta
		(nr_seq_matproc_p		number,
		ie_matproc_p			number)
		return number is

vl_desconto_w		number(15,2)	:= 0;
nr_interno_conta_w	number(10,0);
ie_tipo_desconto_w	number(10,0);

begin

if	(ie_matproc_p	= 1) then
	begin
	
	select	max(nr_interno_conta)
	into	nr_interno_conta_w
	from	procedimento_paciente
	where	nr_sequencia	= nr_seq_matproc_p;

	select	nvl(max(ie_tipo_desconto),0)
	into	ie_tipo_desconto_w
	from	conta_paciente_desconto
	where	nr_interno_conta	= nr_interno_conta_w;

	if	(ie_tipo_desconto_w = 7) then
		select	nvl(sum(vl_desconto),0)
		into	vl_desconto_w
		from	conta_paciente_desc_item
		where	nr_seq_proc_pacote	= nr_seq_matproc_p;
	else
		select	nvl(sum(vl_procedimento),0)
		into	vl_desconto_w
		from	proc_paciente_valor
		where	ie_tipo_valor		= 3
		and	nr_seq_procedimento	= nr_seq_matproc_p;
	end if;

	end;
elsif	(ie_matproc_p	= 2) then
	begin

	select	nvl(sum(vl_material),0)
	into	vl_desconto_w
	from	mat_atend_paciente_valor
	where	ie_tipo_valor		= 3
	and	nr_seq_material		= nr_seq_matproc_p;

	end;
end if;

return	vl_desconto_w;

end Obter_Desconto_item_conta;
/