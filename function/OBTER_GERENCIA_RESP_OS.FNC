create or replace
function obter_gerencia_resp_os (
		nr_seq_os_p	number)
		return number is
		
nr_seq_gerencia_w	number(10,0);
		
begin
if	(nr_seq_os_p is not null) then
	begin
	select	max(g.nr_seq_gerencia)
	into	nr_seq_gerencia_w
	from	grupo_desenvolvimento g,
		man_ordem_servico o
	where	g.nr_sequencia = o.nr_seq_grupo_des
	and	o.nr_sequencia = nr_seq_os_p;
	end;
end if;
return nr_seq_gerencia_w;
end obter_gerencia_resp_os;
/
