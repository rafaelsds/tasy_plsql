create or replace
function pls_obter_desc_subestipulante
			(	nr_sequencia_p		Number)
 		    		return Varchar2 is
				
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter nome do subestipulante
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x ] Tasy (Delphi/Java) [ X ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/				
				
ds_retorno_w			Varchar2(255);
cd_pessoa_fisica_w		Varchar2(10);
cd_cgc_w			Varchar2(14);

begin

select	cd_pessoa_fisica,
	cd_cgc
into	cd_pessoa_fisica_w,
	cd_cgc_w
from	pls_sub_estipulante
where	nr_sequencia	= nr_sequencia_p;

if	(cd_pessoa_fisica_w is not null) then
	ds_retorno_w	:= 'PF - ' || substr(obter_nome_pf(cd_pessoa_fisica_w),1,60);
elsif	(cd_cgc_w is not null) then
	ds_retorno_w	:= 'PJ - ' || substr(obter_razao_social(cd_cgc_w),1,80);
end if;

return	ds_retorno_w;

end pls_obter_desc_subestipulante;
/
