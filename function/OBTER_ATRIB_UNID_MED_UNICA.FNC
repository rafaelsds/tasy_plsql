create or replace
function Obter_Atrib_Unid_Med_Unica( nm_tabela_p varchar2,
     nm_atributo_p varchar2,
     dt_referencia_p date,
     ie_opcao_p number)
     return varchar2 is

nr_sequencia_w  number(10);
IE_UNIDADE_MEDIDA_w varchar2(15);
nr_seq_unid_med_w number(10);

ds_resultado_w varchar2(60);

begin

ds_resultado_w := '';

select	max(nr_sequencia)
into	nr_sequencia_w
from	tab_atrib_regra_unid_med
where	nm_tabela = nm_tabela_p
  and	nm_atributo = nm_atributo_p
  and	dt_referencia_p between dt_inicio_vigencia and fim_dia(nvl(dt_final_vigencia,dt_referencia_p));

if	(nr_sequencia_w is not null) and
	(ie_opcao_p <> 0) then
	select	IE_UNIDADE_MEDIDA,
		nr_seq_unid_med
	into	IE_UNIDADE_MEDIDA_w,
		nr_seq_unid_med_w
	from tab_atrib_regra_unid_med
	where nr_sequencia = nr_sequencia_w;

	if	(IE_UNIDADE_MEDIDA_w = 'U') and
		(nr_seq_unid_med_w is not null) then
		select	substr(EHR_Obter_Desc_Unid_Med(nr_seq_unid_med, 'C'),1,60)
		into	ds_resultado_w
		from	tab_atrib_regra_item
		where nr_sequencia = nr_seq_unid_med_w;
	end if;
end if;

return ds_resultado_w;

end;
/