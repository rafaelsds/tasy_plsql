create or replace 
function hsl_obter_solic_desc(
		nr_titulo_p	number,
		nr_seq_baixa_p	number,
		ie_opcao_p	varchar2) return varchar2 is

/* ie_opcao
	C - cpf/cnpj
	D - descricao
	F - C�digo da pessoa fisica
	J - Codigo da pessoa juridica
*/

ds_retorno_w	varchar2(255);
cd_pessoa_w	varchar2(14);
ds_pessoa_w	varchar2(255);
cd_pessoa_fisica_w	varchar2(10);
cd_cgc_w		varchar2(14);

begin

select	max(nvl(a.cd_pessoa_fisica,a.cd_cgc)),
	substr(max(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc)),1,255),
	max(a.cd_pessoa_fisica),
	max(a.cd_cgc)
into	cd_pessoa_w,
	ds_pessoa_w,
	cd_pessoa_fisica_w,
	cd_cgc_w
from	titulo_receber_liq_desc a
where	a.nr_titulo	= nr_titulo_p
and	a.nr_seq_liq	= nr_seq_baixa_p;


if	(ie_opcao_p = 'C')	then
	ds_retorno_w	:= cd_pessoa_w;
elsif	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_pessoa_w;
elsif	(ie_opcao_p = 'F') then
	ds_retorno_w	:= cd_pessoa_fisica_w;
elsif	(ie_opcao_p = 'J') then
	ds_retorno_w	:= cd_cgc_w;
end if;

return ds_retorno_w;

end hsl_obter_solic_desc;
/