create or replace
function Obter_motivo_cancel_checkup(	nr_seq_motivo_p		number,
					cd_estabelecimento_p	number)
 		    	return varchar is

ds_retorno_w	varchar2(100);
			
begin

select	substr(ds_motivo,1,100)
into	ds_retorno_w
from	checkup_motivo_cancel
where	cd_estabelecimento 	= cd_estabelecimento_p
and	nr_sequencia		= nr_seq_motivo_p;

return	ds_retorno_w;

end Obter_motivo_cancel_checkup;
/