create or replace
function pls_obter_notas_contestadas
			(	nr_fatura_p	number	) 
			return varchar2 is 

nr_nota_w	ptu_questionamento.nr_nota%type;
ds_retorno_w	varchar2(255) := null;
			
Cursor C01 is
	select	a.nr_nota
	from	ptu_questionamento a,
		ptu_camara_contestacao b
	where	a.nr_seq_contestacao = b.nr_sequencia
	and	(select		max(c.cd_motivo)
		 from		ptu_motivo_questionamento c
		 where		c.nr_sequencia = a.nr_seq_mot_questionamento) <> 99
	and	b.nr_fatura = nr_fatura_p  
	group by a.nr_nota;

begin

open C01;
loop
fetch C01 into	
	nr_nota_w;
exit when C01%notfound;
	begin
	
	select	ds_retorno_w || decode(ds_retorno_w, null, '', ',') || nr_nota_w
	into	ds_retorno_w
	from 	dual;
	
	end;
end loop;
close C01;

return	ds_retorno_w;

end pls_obter_notas_contestadas;
/