create or replace
function Obter_somas_transacao_cheq(	nr_sequencia_p	number,
					ie_opcao_p	varchar2)
 		    	return number is

			
retorno_w	number(15,2);
vl_retorno_w	number(15,2);

begin

if	(ie_opcao_p	= 'T') then

	if	(nr_sequencia_p	is not null) then
	
		select	max(vl_transacao)
		into	vl_retorno_w
		from	movto_trans_financ
		where	nr_sequencia	= nr_sequencia_p;
		
	end if;
	
elsif	(ie_opcao_p	= 'C') then

	if	(nr_sequencia_p	is not null) then
		
		select	max(vl_cheque)
		into	vl_retorno_w
		from	cheque_cr
		where	nr_seq_cheque	= nr_sequencia_p;
	
	end if;

elsif	(ie_opcao_p	= 'E') then

	if	(nr_sequencia_p	is not null) then
		
		select	max(vl_lancamento)
		into	vl_retorno_w
		from	banco_extrato_lanc
		where	nr_sequencia	= nr_sequencia_p;
	
	end if;

end if;

retorno_w := nvl(vl_retorno_w,0);

return	retorno_w;

end Obter_somas_transacao_cheq;
/
