Create or replace
function adep_obter_um_dosagem_prescr2 (
				nr_prescricao_p	number,
				nr_sequencia_p	number
				)
				return varchar is

qt_dose_w			varchar2(15);
qt_solucao_w			number(15,4);
cd_unidade_medida_dose_w	varchar2(30);
ds_prescr_w			varchar2(15);
ds_prescricao_w			varchar2(60);
ds_dose_diferenciada_w		varchar2(50);
ds_retorno_w			varchar2(80);
nr_agrupamento_w		number(07,1);
qt_reg_w			number(06,0);
nr_seq_w			number(06,0);
ie_via_w			varchar2(05);
ie_agrupador_w			number(2);
qt_dose_especial_w		number(18,6);
hr_dose_especial_w		varchar2(5);

begin

select	converte_fracao_dose(a.cd_unidade_medida_dose, b.qt_dose)  qt_dose,
	a.cd_unidade_medida_dose,
	c.ds_prescricao,
	a.ie_via_aplicacao,
	a.nr_agrupamento,
	a.ie_agrupador
into	qt_dose_w,
	cd_unidade_medida_dose_w,
	ds_prescr_w,
	ie_via_w,
	nr_agrupamento_w,
	ie_agrupador_w	
from  	prescr_mat_hor b,
	intervalo_prescricao c,	
	prescr_material a
where 	a.cd_intervalo 		= c.cd_intervalo
and	a.nr_prescricao 	= b.nr_prescricao
and	a.nr_sequencia		= b.nr_seq_material	 
and 	a.nr_prescricao  	= nr_prescricao_p
and	b.nr_sequencia		= nr_sequencia_p
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';

if	(substr(qt_dose_w,1,1) = ',') then
	qt_dose_w	:= '0' || qt_dose_w;
end if;
qt_dose_w	:= replace(qt_dose_w,'.',',');

select	count(*),
	min(nr_sequencia)
into	qt_reg_w,
	nr_seq_w
from	prescr_material
where	nr_prescricao		= nr_prescricao_p
  and	ie_agrupador		= 1
  and	nr_agrupamento		= nr_agrupamento_w;

if	(qt_reg_w > 1) and
	(nr_sequencia_p <> nr_seq_w) then
	ds_prescricao_w	:= '';
	ie_via_w		:= '';
else
	ds_prescricao_w	:= ds_prescr_w;
end if;

if 	((ie_agrupador_w <> 1) or
	(ie_agrupador_w = 1) and (qt_solucao_w is null)) then
	ds_retorno_w	:= qt_dose_w || ' ' || cd_unidade_medida_dose_w ||
			'   ' ||ds_prescricao_w || '   ' || ie_via_w;
else
	ds_retorno_w	:= ds_prescricao_w || '   ' || ie_via_w;
end if;

return ds_retorno_w;

end adep_obter_um_dosagem_prescr2;
/
