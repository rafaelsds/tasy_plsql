create or replace function	pfcs_get_if_private_room(nr_seq_bed_p	number) return varchar2 is

cd_setor_atendimento_w		setor_atendimento.cd_setor_atendimento%type;
cd_unidade_basica_w		unidade_atendimento.cd_unidade_basica%type;
cd_unidade_compl_w		unidade_atendimento.cd_unidade_compl%type;

qt_bed_same_room_w		number(10);
ie_private_bed_w		varchar2(1);

begin

ie_private_bed_w := 'N';

select	max(cd_unidade_basica),
		max(cd_unidade_compl),
		max(cd_setor_atendimento)
into	cd_unidade_basica_w,
		cd_unidade_compl_w,
		cd_setor_atendimento_w
from	unidade_atendimento a
where	a.nr_seq_interno = nr_seq_bed_p;

select	count(*)
into	qt_bed_same_room_w
from	unidade_atendimento a
where	cd_setor_atendimento = cd_setor_atendimento_w
and		cd_unidade_basica = cd_unidade_basica_w
and		nr_seq_interno <> nr_seq_bed_p
and		ie_situacao = 'A';

if		(qt_bed_same_room_w > 0) then
		ie_private_bed_w := 'N';
else	
		ie_private_bed_w := 'S';
end if;

return ie_private_bed_w;

end;
/
