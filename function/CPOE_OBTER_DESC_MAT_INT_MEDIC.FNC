create or replace
function cpoe_obter_desc_mat_int_medic(nr_sequencia_p	number) return varchar2 is

ds_retorno_p	varchar2(255);

begin

select	substr(obter_desc_material(max(cd_material_interacao)),1,255)
into	ds_retorno_p
from	MATERIAL_INTERACAO_MEDIC
where	nr_sequencia = nr_sequencia_p;


return ds_retorno_p;

end cpoe_obter_desc_mat_int_medic;
/