create or replace
function obter_se_exibe_prescr_gpe (	nr_prescricao_p			number,
					ie_retrograda_p			varchar2, --[81]
					ie_liberacao_p			varchar2,
					ie_lib_farm_p			varchar2,
					ie_sem_atendimento_p		varchar2, --[95]
					cd_setor_atendimento_p		number,
					ie_dispensacao_p		number,
					ie_revisao_p			number,
					ie_itens_p			number,
					ie_consiste_alta_p		varchar2, --[40]
					ie_restringe_estab_p		varchar2, --[71]
					cd_estabelecimento_p		number)
				return varchar2 is

ie_exibe_w			varchar2(1)	:= 'N';
ie_prescr_emergencia_w 		prescr_medica.ie_prescr_emergencia%type;
dt_liberacao_w			prescr_medica.dt_liberacao%type;
dt_liberacao_farmacia_w		prescr_medica.dt_liberacao_farmacia%type;
cd_setor_atendimento_w		prescr_medica.cd_setor_atendimento%type;
cd_pessoa_fisica_w		prescr_medica.cd_pessoa_fisica%type;
nr_atendimento_w		prescr_medica.nr_atendimento%type;
dt_revisao_w			prescr_medica.dt_revisao%type;
cd_estabelecimento_w		prescr_medica.cd_estabelecimento%type;
ie_disp_w			varchar2(1);
ie_itens_w			varchar2(1)	:= 'N';
ds_itens_prescr_w		varchar2(2000);
dt_alta_w			date;

begin

select	nvl(max(ie_prescr_emergencia),'N'),
	max(dt_liberacao),
	max(dt_liberacao_farmacia),
	max(cd_setor_atendimento),
	max(cd_pessoa_fisica),
	max(nr_atendimento),
	max(dt_revisao),
	max(obter_itens_prescr(nr_prescricao,DS_ITENS_PRESCR)),
	max(cd_estabelecimento)
into	ie_prescr_emergencia_w,
	dt_liberacao_w,
	dt_liberacao_farmacia_w,
	cd_setor_atendimento_w,
	cd_pessoa_fisica_w,
	nr_atendimento_w,
	dt_revisao_w,
	ds_itens_prescr_w,
	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	max(dt_alta)
into	dt_alta_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_w;

if	(nr_atendimento_w	is null) and
	(cd_setor_atendimento_w	is null) then
	cd_setor_atendimento_w	:= obter_cd_setor_pf(cd_pessoa_fisica_w);
end if;

if	(ie_dispensacao_p	= 0) then
	select	nvl(max('S'),'N')
	into	ie_disp_w
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and	ie_regra_disp = 'U';
elsif	(ie_dispensacao_p	= 1) then
	select	nvl(max('N'),'S')
	into	ie_disp_w
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and	ie_regra_disp = 'U';
else
	ie_disp_w	:= 'S';
end if;

if	(ie_itens_p	= 0) then
	if	(instr(ds_itens_prescr_w,obter_desc_expressao(293051)/*'Medic'*/) > 0) then
		ie_itens_w	:= 'S';
	end if;
elsif	(ie_itens_p	= 1) then
	if	(instr(ds_itens_prescr_w,'Sol')	> 0) then
		ie_itens_w	:= 'S';
	end if;
elsif	(ie_itens_p	= 2) then
	if	(instr(ds_itens_prescr_w,'Proc') > 0) then
		ie_itens_w	:= 'S';
	end if;
elsif	(ie_itens_p	= 3) then
	if	(instr(ds_itens_prescr_w,'Rec')	> 0) then
		ie_itens_w	:= 'S';
	end if;
elsif	(ie_itens_p	= 4) then
	if	(instr(ds_itens_prescr_w,obter_desc_expressao(298038)/*'Sangue'*/) > 0) then
		ie_itens_w	:= 'S';
	end if;
else
	ie_itens_w	:= 'S';
end if;

if	((ie_retrograda_p = 'S')	or
	 (ie_prescr_emergencia_w	= 'N')) and
	((ie_liberacao_p = 0) or 
	 ((ie_liberacao_p = 1) and (dt_liberacao_w is not null)) or 
	 ((ie_liberacao_p = 2) and (dt_liberacao_w is null))) and
	((ie_lib_farm_p = 0) or 
	 ((ie_lib_farm_p = 1) and (dt_liberacao_farmacia_w is not null)) or 
	 ((ie_lib_farm_p = 2) and (dt_liberacao_farmacia_w is null))) and
	((ie_sem_atendimento_p = 'S') or (nr_atendimento_w is not null)) and
	((cd_setor_atendimento_p is null) or (cd_setor_atendimento_p = cd_setor_atendimento_w)) and
	(ie_disp_w	= 'S') and
	((ie_revisao_p	= 2) or
	 ((ie_revisao_p	= 0) and (dt_revisao_w	is null)) or
	 ((ie_revisao_p = 1) and (dt_revisao_w	is not null))) and
	(ie_itens_w	= 'S') and
	((ie_consiste_alta_p	= 'S') or ((ie_consiste_alta_p	= 'N') and (dt_alta_w is null))) and
	((ie_restringe_estab_p	= 'N') or ((ie_restringe_estab_p = 'S') and (cd_estabelecimento_w	= cd_estabelecimento_p)))
	 then
	ie_exibe_w	:= 'S';
end if;

return	ie_exibe_w;

end obter_se_exibe_prescr_gpe;
/