create or replace
function obter_conta_convenio(	cd_estabelecimento_p	number,		
				cd_convenio_p		number,
				ie_tipo_atendimento_p	number,
				ie_tipo_conta_p		varchar2,
				dt_vigencia_p		date,
				ie_tipo_protocolo_p		number,
				cd_categoria_p		varchar2,
				cd_plano_p		varchar2,
				cd_setor_atendimento_p	number,
				cd_procedimento_p	number	default null,
				ie_origem_proced_p	number	default null,
				nr_seq_trans_financ_p	number	default null)
				return varchar2 is

cd_conta_contabil_w		varchar2(40);
cd_cgc_w			varchar2(14);
cd_empresa_w			number(05,0);
cd_setor_atendimento_w		number(10,0);
cd_grupo_w                				number(15)  := 0;
cd_especialidade_w        				number(15)  := 0;
cd_area_w					number(15)  := 0;
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);

cursor c01 is
select	cd_conta_contabil
from	convenio_conta_contabil
where	cd_convenio		= cd_convenio_p
and	(ie_tipo_atendimento	= ie_tipo_atendimento_p or ie_tipo_atendimento is null)
and	cd_estabelecimento		= cd_estabelecimento_p
and	ie_tipo_conta		= nvl(ie_tipo_conta_p, ie_tipo_conta)
and	(nvl(dt_inicio_vigencia, dt_vigencia_p) <= dt_vigencia_p and nvl(dt_fim_vigencia, dt_vigencia_p) >= dt_vigencia_p)
and	(ie_tipo_protocolo		= ie_tipo_protocolo_p or ie_tipo_protocolo is null)
and	(cd_categoria		= cd_categoria_p or cd_categoria is null)
and	(cd_plano		= cd_plano_p or cd_plano is null)
and	(cd_setor_atendimento	= cd_setor_atendimento_w or cd_setor_atendimento is null)
and	nvl(cd_procedimento,cd_procedimento_w) 		= cd_procedimento_w
and	nvl(decode(nvl(cd_procedimento,0),0,null,ie_origem_proced),ie_origem_proced_w) = ie_origem_proced_w
and	nvl(cd_area_procedimento,cd_area_w)			= cd_area_w	
and	nvl(cd_especialidade,cd_especialidade_w)		= cd_especialidade_w
and	nvl(cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
and	nvl(nr_seq_trans_financ, nvl(nr_seq_trans_financ_p,0))	= nvl(nr_seq_trans_financ_p,0)
order by dt_inicio_vigencia,
	nvl(cd_plano, 0),
	nvl(cd_categoria , 0),
	nvl(cd_setor_atendimento, 0),
	nvl(ie_tipo_atendimento , 0),
	nvl(cd_procedimento,0),
	nvl(cd_grupo_proc,0),
	nvl(cd_especialidade,0),
	nvl(cd_area_procedimento,0),
	nvl(nr_seq_trans_financ,0);


begin

cd_setor_atendimento_w	:= cd_setor_atendimento_p;

begin
cd_procedimento_w		:= nvl(cd_procedimento_p,0);
ie_origem_proced_w		:= nvl(ie_origem_proced_p,0);



select	cd_grupo_proc,
	cd_especialidade,
	cd_area_procedimento
into	cd_grupo_w,
	cd_especialidade_w,
	cd_area_w
from	estrutura_procedimento_v
where	cd_procedimento 	= cd_procedimento_p
and	ie_origem_proced 	= ie_origem_proced_p;
exception when others then
	cd_grupo_w 		:= 0;
	cd_especialidade_w	:= 0;
	cd_area_w		:= 0;
end;

open c01;
loop
fetch c01 into
	cd_conta_contabil_w;
exit when c01%notfound;
end loop;
close c01;

if	(cd_conta_contabil_w is null) then
	begin

	select	cd_cgc
	into	cd_cgc_w
	from	convenio
	where	cd_convenio	= cd_convenio_p;

	select cd_empresa
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_p;

	select obter_conta_contab_pj(cd_empresa_w, cd_estabelecimento_p, cd_cgc_w,'R', dt_vigencia_p)
	into	cd_conta_contabil_w
	from	dual;

	end;
end if;

if	(cd_conta_contabil_w is null) then
	select	cd_conta_contabil
	into	cd_conta_contabil_w
	from	convenio
	where	cd_convenio	= cd_convenio_p;
end if;

return cd_conta_contabil_w;
end obter_conta_convenio;
/