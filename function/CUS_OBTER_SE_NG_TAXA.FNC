create or replace
function cus_obter_se_ng_taxa(	cd_estabelecimento_p	number,
				nr_seq_gng_p		number,
				nr_seq_ng_p		number,
				cd_centro_controle_p	number)
 		    		return varchar2 is

ie_compoe_taxa_custo_w		varchar2(1)	:= 'S';
qt_regra_w			number(10);

begin

select	count(*)
into	qt_regra_w
from	cus_regra_grupo_taxa
where	nr_seq_gng	= nr_seq_gng_p;

if	(qt_regra_w = 0) then
	
	select	nvl(max(ie_compoe_taxa_custo),'S')
	into	ie_compoe_taxa_custo_w
	from	natureza_gasto
	where	nr_sequencia	= nr_seq_ng_p;
else
	ie_compoe_taxa_custo_w			:= 'N';
	
	select	count(*)
	into	qt_regra_w
	from	cus_regra_grupo_taxa a
	where	cd_estabelecimento		= cd_estabelecimento_p
	and	cd_centro_controle		= cd_centro_controle_p
	and	nr_seq_gng		= nr_seq_gng_p
	and	ie_compoe_taxa_custo	= 'S';
	
	if	(qt_regra_w > 0) then
		ie_compoe_taxa_custo_w	:= 'S';
	end if;
end if;

return	ie_compoe_taxa_custo_w;

end cus_obter_se_ng_taxa;
/