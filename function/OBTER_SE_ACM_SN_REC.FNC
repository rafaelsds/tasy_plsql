create or replace
function obter_se_acm_sn_rec (	ds_horarios_p		varchar2,
				ie_se_necessario_p	varchar2)
	return varchar2 is
	
ie_acm_sn_w	varchar2(1) := 'N';
ds_horarios_w	varchar2(2000);
	
begin
if	(ds_horarios_p is not null) then
	begin
	ds_horarios_w	:= padroniza_horario(ds_horarios_p);
	if	((ds_horarios_w = '') or
		 (ds_horarios_w is null)) then
		begin
		ie_acm_sn_w	:= 'S';
		end;
	end if;
	end;
end if;

if	(ie_acm_sn_w		= 'N') and
	(ie_se_necessario_p	= 'S') then
	
	ie_acm_sn_w	:= 'S';
	
end if;	

return ie_acm_sn_w;
end obter_se_acm_sn_rec;
/
