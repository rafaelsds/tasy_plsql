create or replace
function obter_se_pendente_atend_senha(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);			
				
begin

if (nr_sequencia_p is not null) then

	select 	decode(count(*),0,'N','S')
	into 	ds_retorno_w
	from	paciente_senha_fila a,
			fila_espera_senha b
	where	b.nr_sequencia = nvl(a.nr_seq_fila_senha, a.nr_seq_fila_senha_origem)
	and		a.nr_sequencia = nr_sequencia_p 
	and		not exists (select 1 
					from 	atendimentos_senha z
					where	z.NR_SEQ_PAC_SENHA_FILA = a.nr_sequencia
					and          z.dt_inicio_atendimento IS NULL);

end if;

return	ds_retorno_w;

end obter_se_pendente_atend_senha;
/
