create or replace
function verificar_existencia_reg_atend	(nr_atendimento_p	number)
 		    	return varchar2 is

ie_existe_reg_w	varchar2(2) := 'N';

begin
if	(nr_atendimento_p is not null) then
	select	decode(count(*),0,'N','S')
	into	ie_existe_reg_w
	from	prontuario_controle
	where	nr_atendimento = nr_atendimento_p;

end if;

return	ie_existe_reg_w;

end verificar_existencia_reg_atend;
/