create or replace
function Obter_Pessoa_Jur_Tipo_Adic	(cd_cgc_p		varchar2,
					cd_estabelecimento_p	number)
 		    	return number is

cd_tipo_pessoa_w	number(3);

begin

select	nvl(max(cd_tipo_pessoa),0)
into	cd_tipo_pessoa_w
from	pessoa_jur_tipo_adic
where	cd_cgc 	= cd_cgc_p
and	cd_estabelecimento	= cd_estabelecimento_p;

return	cd_tipo_pessoa_w;

end Obter_Pessoa_Jur_Tipo_Adic;
/