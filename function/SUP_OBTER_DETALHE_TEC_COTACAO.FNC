create or replace
function sup_obter_detalhe_tec_cotacao ( nr_cot_compra_p	number,
				    nr_item_p		number)
 		    	return varchar is

qt_existe_w		number(10);
ds_detalhe_tecnico_w	varchar2(4000) := ' ';

begin

select	count(*)
into	qt_existe_w
from	cot_compra_item_detalhe
where	nr_cot_compra = nr_cot_compra_p
and	nr_item_cot_compra = nr_item_p;

if	(qt_existe_w > 0) then
	select	substr(ds_detalhe,1,4000)
	into	ds_detalhe_tecnico_w
	from	cot_compra_item_detalhe
	where	nr_cot_compra = nr_cot_compra_p
	and	nr_item_cot_compra = nr_item_p;
end if;

return	ds_detalhe_tecnico_w;

end sup_obter_detalhe_tec_cotacao;
/