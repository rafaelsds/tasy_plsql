create or replace
function proj_obter_desc_hospedagem(	nr_sequencia_p		number)
				return varchar2 is

ds_retorno_w	varchar2(255);

begin
select	substr(obter_dados_pf_pj(null,a.cd_cnpj_hotel,'N'),1,60)
into	ds_retorno_w
from	via_hotel a
where	a.nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end proj_obter_desc_hospedagem;
/