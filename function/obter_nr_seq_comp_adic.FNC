create or replace
function obter_nr_seq_comp_adic( nr_seq_atend_disp_p number)
 		    	return number is

nr_seq_comp_adic_w number(10);
 			
begin


if (nr_seq_atend_disp_p is not null) then
	
	select 	max(NR_SEQ_COMPLEMENTO)
	into	nr_seq_comp_adic_w
	from 	ATEND_PAC_DISPOSITIVO
	where 	NR_SEQUENCIA = nr_seq_atend_disp_p
	and 	DT_RETIRADA is null; 

end if;

return nr_seq_comp_adic_w;	

end obter_nr_seq_comp_adic;
/
