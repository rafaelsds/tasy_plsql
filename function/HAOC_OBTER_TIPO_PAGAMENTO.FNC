create or replace
function	HAOC_Obter_tipo_pagamento(	tp_pagamento_p		varchar,
						nr_seq_escritutal_p	number)
						return			number is

nr_doc_w	number(10);
nr_docn_w	number(10);
nr_ted_mt_w	number(10);
nr_ted_ot_w	number(10);
nr_cc_ot_w	number(10);
nr_cc_mt_w	number(10);
nr_blq_w	number(10);
nr_blqN_w	number(10);
qt_lote_w	number(10);
qt_pag_con_w	number(10);

begin

--Doc
select	count(*)
into	nr_doc_w
from	titulo_pagar_escrit c,
	titulo_pagar t
where	c.cd_banco		= 341
and	c.ie_tipo_pagamento	= 'DOC'
and	nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

--Ted mesmo titular
select	count(*)
into	nr_ted_mt_w
from	estabelecimento a,
	titulo_pagar_escrit c,
	titulo_pagar t
where	a.cd_cgc		= nvl(t.cd_pessoa_fisica,t.cd_cgc)
and	t.cd_estabelecimento	= a.cd_estabelecimento
and	c.ie_tipo_pagamento	= 'TED'
and	nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

--Ted outro titular
select	count(*)
into	nr_ted_ot_w
from	estabelecimento a,
	titulo_pagar_escrit c,
	titulo_pagar t
where	a.cd_cgc		<> nvl(t.cd_pessoa_fisica,t.cd_cgc)
and	t.cd_estabelecimento	= a.cd_estabelecimento
and	c.ie_tipo_pagamento	= 'TED'
and	nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

--CC outro titular
select	count(*)
into	nr_cc_ot_w
from	estabelecimento a,
	titulo_pagar_escrit c,
	titulo_pagar t
where	a.cd_cgc		<> nvl(t.cd_pessoa_fisica,t.cd_cgc)
and	c.ie_tipo_pagamento	= 'CC'
and	nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

--CC mesmo titular
select	count(*)
into	nr_cc_mt_w
from	estabelecimento a,
	titulo_pagar_escrit c,
	titulo_pagar t
where	a.cd_cgc		= nvl(t.cd_pessoa_fisica,t.cd_cgc)
and	c.ie_tipo_pagamento	= 'CC'
and	nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

--Blq Itau
select	count(*)
into	nr_blq_w
from	titulo_pagar_escrit c,
	titulo_pagar t
where	c.ie_tipo_pagamento	= 'BLQ'
and	SUBSTR(t.nr_bloqueto,1,3) = 341
and	c.nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

--Doc n�o Itau
select	count(*)
into	nr_docn_w
from	titulo_pagar_escrit c,
	titulo_pagar t
where	c.cd_banco		<> 341
and	c.ie_tipo_pagamento	= 'DOC'
and	nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

-- Blq N�o Itau
select	count(*)
into	nr_blqN_w
from	titulo_pagar_escrit c,
	titulo_pagar t
where	c.ie_tipo_pagamento	= 'BLQ'
and	(t.nr_bloqueto is null or SUBSTR(t.nr_bloqueto,1,3) <> 341)
and	c.nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

-- Pagamento de Concessionarias
select	count(*)
into	qt_pag_con_w
from	titulo_pagar_escrit c,
	titulo_pagar t
where	c.ie_tipo_pagamento	= 'PC'
and	c.nr_seq_escrit		= nr_seq_escritutal_p
and	c.nr_titulo		= t.nr_titulo
and	t.ie_situacao		= 'A';

qt_lote_w	:= 0;

if	(nr_doc_w > 0) and (tp_pagamento_p in ('DOC','TEDMT','TEDOT','CCOT','CCMT','BLQ','DOCN','BLQN','PC')) then
	qt_lote_w	:= qt_lote_w + 1;
end if;

if 	(nr_ted_mt_w > 0) and (tp_pagamento_p in ('TEDMT','TEDOT','CCOT','CCMT','BLQ','DOCN','BLQN','PC')) then
	qt_lote_w	:= qt_lote_w + 1;
end if;

if 	(nr_ted_ot_w > 0) and (tp_pagamento_p in ('TEDOT','CCOT','CCMT','BLQ','DOCN','BLQN','PC')) then
	qt_lote_w	:= qt_lote_w + 1;
end if;

if 	(nr_cc_ot_w > 0) and (tp_pagamento_p in ('CCOT','CCMT','BLQ','DOCN','BLQN','PC')) then
	qt_lote_w	:= qt_lote_w + 1;
end if;

if 	(nr_cc_mt_w > 0) and (tp_pagamento_p in ('CCMT','BLQ','DOCN','BLQN','PC')) then
	qt_lote_w	:= qt_lote_w + 1;
end if;

if	(nr_blq_w > 0) and (tp_pagamento_p in ('BLQ','DOCN','BLQN','PC')) then
	qt_lote_w	:= qt_lote_w + 1;
end if;

if	(nr_docn_w > 0) and (tp_pagamento_p in ('DOCN','BLQN','PC')) then
	qt_lote_w	:= qt_lote_w + 1;
end if;

if	(nr_blqN_w > 0) and (tp_pagamento_p in ('BLQN','PC')) then
	qt_lote_w	:= qt_lote_w + 1;
end if;

if	(qt_pag_con_w > 0) and (tp_pagamento_p = 'PC') then
	qt_lote_w	:= qt_lote_w + 1;
end if;

return	qt_lote_w;

end HAOC_Obter_tipo_pagamento;
/