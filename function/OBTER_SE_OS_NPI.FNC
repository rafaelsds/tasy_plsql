create or replace
function obter_se_os_npi(nr_seq_ordem_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);

begin

select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	proj_cronograma e,
		prp_processo_fase p,
		prp_fase_processo f,
		proj_cron_etapa d,
		man_ordem_servico c
where	p.nr_Sequencia = e.nr_seq_processo_fase
and		f.nr_sequencia = p.nr_seq_fase_processo
and		e.nr_sequencia = d.nr_seq_cronograma
and		c.nr_seq_proj_cron_etapa = d.nr_sequencia
and		c.nr_sequencia = nr_seq_ordem_p;

return	ds_retorno_w;

end obter_se_os_npi;
/