create or replace
function obter_se_cotacao_aprov(	dt_aprovacao_p	date,
					nr_cot_compra_p		number,
					IE_FINALIDADE_COTACAO_p	varchar2 default null)
 		    	return varchar2 is

ie_status_w		varchar2(15);
dt_aprovacao_w		date;	
qt_registros_w		number(10);
	
begin


if	(dt_aprovacao_p	is not null) then
	return 'S';
end if;


if	(IE_FINALIDADE_COTACAO_p	= 'F') then
	return 'S';
end if;


select	count(*)
into	qt_registros_w
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_p
and	nr_seq_aprovacao is not null;
	
if	(qt_registros_w > 0) then
	return 'S';
end if;

return	'N';

end obter_se_cotacao_aprov;
/
