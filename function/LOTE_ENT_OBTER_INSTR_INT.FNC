create or replace
function lote_ent_obter_instr_int(	nr_seq_instituicao_p		number,
				cd_tipo_instrucao_p			varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);				

/*
cd_tipo_instrucao:
B - Busca ativa
S - Secretaria
A - Ambos
*/	
	
begin

if 	(nr_seq_instituicao_p is not null) and
	(cd_tipo_instrucao_p is not null) then
	
	select 	substr(max(a.DS_INSTRUCAO),1,255)
	into	ds_retorno_w
	from	lote_ent_inst_instrucao a
	where	a.nr_seq_instituicao = nr_seq_instituicao_p
	and		((cd_tipo_instrucao = cd_tipo_instrucao_p) or (cd_tipo_instrucao = 'A'));

end if;

return	ds_retorno_w;

end lote_ent_obter_instr_int;
/