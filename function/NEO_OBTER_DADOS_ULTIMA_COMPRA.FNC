create or replace
function neo_obter_dados_ultima_compra(
			cd_estabelecimento_p	Number,
			qt_dia_p			Number,
			cd_material_p		number,
			cd_local_estoque_p	Number,
			ie_tipo_p			Varchar2,
			ie_tipo_retorno_p		varchar2)
		return varchar2 is

vl_retorno_w			number(15,4);
nr_sequencia_w			number(10,0);
dt_entrada_saida_w		date;
nr_nota_fiscal_w		varchar2(255);
cd_material_estoque_w	number(6);
ds_retorno_w	varchar2(255);

/*ie_tipo_p
N =  Material da nota, ou seja: Busca a nota com pr�prio material passado no parametro
Sen�o busca sempre do Material estoque, ou seja: Busca a nota que tenha o material de estoque do material passado no parametro
*/

BEGIN
vl_retorno_w		:= null;
nr_nota_fiscal_w	:= null;
dt_entrada_saida_w	:= null;
ds_retorno_w 		:= null;

/*Fabio 20/03 - Separei em 2 select devido a performance pois fazia com nvl no local*/
if	(nvl(cd_local_estoque_p, 0) = 0) then
	select	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	natureza_operacao o,
		operacao_nota p,
		nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_sequencia		= b.nr_sequencia
	and	b.cd_natureza_operacao	= o.cd_natureza_operacao
	and	b.cd_operacao_nf 	= p.cd_operacao_nf
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.dt_atualizacao	>= sysdate - nvl(qt_dia_p,90)
	and	a.cd_material  		= cd_material_p
	and	b.ie_situacao		= '1'
	and	o.ie_entrada_saida	= 'E'
	and	p.ie_ultima_compra	= 'S';

elsif	(nvl(cd_local_estoque_p, 0) > 0) then
	select	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	natureza_operacao o,
		operacao_nota p,
		nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_sequencia		= b.nr_sequencia
	and	b.cd_natureza_operacao	= o.cd_natureza_operacao
	and	b.cd_operacao_nf 	= p.cd_operacao_nf
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_local_estoque	= cd_local_estoque_p
	and	a.dt_atualizacao	>= sysdate - nvl(qt_dia_p,90)
	and	a.cd_material  		= cd_material_p
	and	b.ie_situacao		= '1'
	and	o.ie_entrada_saida	= 'E'
	and	p.ie_ultima_compra	= 'S';
end if;


if	(nr_sequencia_w > 0) then
	begin
	/*Separei em 2 select devido a performance pois fazia com nvl com ie_tipo para o material*/
	if	(nvl(ie_tipo_p, 'N') = 'N') then
		select	nvl(max(dividir((a.vl_total_item_nf - a.vl_desconto -
				a.vl_desconto_rateio + a.vl_frete + a.vl_despesa_acessoria + a.vl_seguro),
			a.qt_item_estoque)),0),
			max(b.dt_entrada_saida),
			max(b.nr_nota_fiscal)
		into	vl_retorno_w,
			dt_entrada_saida_w,
			nr_nota_fiscal_w
		from 	nota_fiscal_item a,
				nota_fiscal b
		where	a.nr_sequencia = b.nr_sequencia
		and	b.nr_sequencia 	= nr_sequencia_w
		and	a.cd_material	= cd_material_p;
	elsif	(nvl(ie_tipo_p, 'N') <> 'N') then
		select	nvl(max(dividir((a.vl_total_item_nf - a.vl_desconto -
				a.vl_desconto_rateio + a.vl_frete + a.vl_despesa_acessoria + a.vl_seguro),
			a.qt_item_estoque)),0),
			max(b.dt_entrada_saida),
			max(b.nr_nota_fiscal)
		into	vl_retorno_w,
			dt_entrada_saida_w,
			nr_nota_fiscal_w
		from 	nota_fiscal_item a,
			nota_fiscal b
		where	a.nr_sequencia = b.nr_sequencia
		and	b.nr_sequencia 		= nr_sequencia_w
		and	a.cd_material_estoque	= cd_material_p;
	end if;
	if (ie_tipo_retorno_p = 'DT') then
		ds_retorno_w := to_char(dt_entrada_saida_w,'dd/mm/yyyy');
	elsif (ie_tipo_retorno_p = 'NF') then
		ds_retorno_w := nr_nota_fiscal_w;
	else
		ds_retorno_w := to_char(vl_retorno_w);
	end if;	
	end;
end if;

return ds_retorno_w;

END neo_obter_dados_ultima_compra;
/
