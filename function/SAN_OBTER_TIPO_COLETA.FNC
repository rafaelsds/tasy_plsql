create or replace
function san_Obter_tipo_coleta(nr_seq_doacao_p	Number)
 		    	return Number is

retorno_w	Number(2);
			
begin

if (nr_seq_doacao_p is not null) then
	
	select	ie_tipo_coleta
	into	retorno_w
	from	san_doacao
	where	nr_sequencia = nr_seq_doacao_p;
	
end if;

return	retorno_w;

end san_Obter_tipo_coleta;
/