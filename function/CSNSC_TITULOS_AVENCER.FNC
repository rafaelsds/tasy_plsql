Create or Replace
FUNCTION CSNSC_TITULOS_AVENCER(	dt_vencimento_w	date)
					RETURN VarChar2 IS

ds_lista_titulo_w	varchar(4000);
nr_titulo_w	number(10);

cursor	c01 is
select	a.nr_titulo
from	titulo_pagar a
where	a.ie_situacao		= 'A'
and	a.dt_vencimento_atual	between trunc(dt_vencimento_w) and fim_dia(dt_vencimento_w)
order by	a.nr_titulo;

BEGIN

open	c01;
loop
fetch	c01 into
	nr_titulo_w;
exit	when c01%notfound;

	if	(ds_lista_titulo_w is null) then
		ds_lista_titulo_w	:= nr_titulo_w;
	elsif	(length(ds_lista_titulo_w) <= 3990) then
		ds_lista_titulo_w	:= ds_lista_titulo_w || ', ' || nr_titulo_w;
	end if;

end	loop;
close	c01;

RETURN ds_lista_titulo_w;

END CSNSC_TITULOS_AVENCER;
/