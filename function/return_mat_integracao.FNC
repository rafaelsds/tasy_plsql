create or replace
function RETURN_MAT_INTEGRACAO(nr_seq_interno_p		number,
                               ie_opcao_p		varchar2) 
				return varchar2 is
          
				
ds_material_int_w		varchar2(200);
cd_material_int_w		varchar2(20);
ds_retorno_w			varchar2(255);

begin

	select	max(b.cd_material_integracao),	
		max(b.ds_material_exame)
	into	cd_material_int_w,
		ds_material_int_w
	from	prescr_procedimento a,
		material_exame_lab b
	where	a.cd_material_exame = b.cd_material_exame
	and	a.nr_seq_interno     = nr_seq_interno_p;
  
  
	if	(ie_opcao_p = 'C') then
		ds_retorno_w := cd_material_int_w;
	elsif	(ie_opcao_p = 'D') then
		ds_retorno_w := ds_material_int_w;
	end if;
  
return ds_retorno_w;

end RETURN_MAT_INTEGRACAO;
/
