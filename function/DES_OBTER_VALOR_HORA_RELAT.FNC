create or replace
function des_obter_valor_hora_relat(	nr_seq_cliente_p	number)
 		    	return number is

vl_hora_w	number(15,2) := 140;

cursor c01 is
select	vl_hora
from	des_regra_cobranca_rel_os
where	(nvl(nr_seq_cliente, nr_seq_cliente_p) = nr_seq_cliente_p)
and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate)
order by nvl(nr_seq_cliente,0);
	
begin

if	(nr_seq_cliente_p > 0) then
	open C01;
	loop
	fetch C01 into	
		vl_hora_w;
	exit when C01%notfound;
		begin
		vl_hora_w := vl_hora_w;
		end;
	end loop;
	close C01;
else
	select	max(vl_hora)
	into	vl_hora_w
	from	des_regra_cobranca_rel_os
	where	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate);
end if;

return	vl_hora_w;

end des_obter_valor_hora_relat;
/
