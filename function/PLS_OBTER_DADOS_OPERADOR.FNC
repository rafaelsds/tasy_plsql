create or replace
function pls_obter_dados_operador
			(	nr_seq_operador_p	number,
				ie_tipo_dado_p		varchar2)
				return varchar2 is
				
ds_retorno_w			varchar2(255);
nm_pessoa_fisica_w		varchar2(255);
cd_pessoa_fisica_w		varchar2(15);
nm_usuario_w			varchar2(255);

begin

select	b.nm_pessoa_fisica,
	a.cd_pessoa_fisica
into	nm_pessoa_fisica_w,
	cd_pessoa_fisica_w
from	pls_operador	a, 
	pessoa_fisica	b
where	a.nr_sequencia		= nr_seq_operador_p
and	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica;

if	(ie_tipo_dado_p = 'M') then
	ds_retorno_w := nm_pessoa_fisica_w;
elsif	(ie_tipo_dado_p = 'C') then
	ds_retorno_w := cd_pessoa_fisica_w;
elsif	(ie_tipo_dado_p = 'U') then
	ds_retorno_w := obter_usuario_pf(cd_pessoa_fisica_w);
elsif	(ie_tipo_dado_p = 'NM') then
	select	c.nm_usuario
	into	nm_usuario_w
	from	pls_operador	a, 
		pessoa_fisica	b,
		usuario		c
	where	a.nr_sequencia	= nr_seq_operador_p
	and	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	b.cd_pessoa_fisica	= c.cd_pessoa_fisica;
	ds_retorno_w := nm_usuario_w;
end if;

return	ds_retorno_w;

end pls_obter_dados_operador;
/