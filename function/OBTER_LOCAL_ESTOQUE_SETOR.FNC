create or replace
function obter_local_estoque_setor(cd_setor_atendimento_p number,
				   cd_estabelecimento_p	  number)
				return number is

cd_local_estoque_w	number(4);
cd_local_estoque_ww	number(4);

begin

select	nvl(max(cd_local_estoque),0)
into	cd_local_estoque_ww
from	setor_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_p
and	cd_estabelecimento   = cd_estabelecimento_p;

if	(cd_local_estoque_ww > 0) then 
	cd_local_estoque_w	:= cd_local_estoque_ww;
end if;

return	cd_local_estoque_w;

end 	obter_local_estoque_setor;
/