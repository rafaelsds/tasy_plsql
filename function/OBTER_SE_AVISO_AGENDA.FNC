create or replace
function obter_se_aviso_agenda	(cd_agenda_p	number,
					dt_aviso_p	date)
					return varchar2 is

ie_aviso_w	varchar2(1);

begin
if	(cd_agenda_p is not null) and
	(dt_aviso_p is not null) then
	select	decode(count(*),0,'N','S')
	into	ie_aviso_w
	from	agenda_aviso
	where	cd_agenda 	= cd_agenda_p
	and	dt_aviso	= dt_aviso_p
	and	ds_aviso	is not null; 
end if;

return ie_aviso_w;

end obter_se_aviso_agenda;
/