create or replace
function obter_motivo_glosa_ret_item(nr_interno_conta_p	number)	return varchar2 is

nr_retorno_w		number(5);
cd_motivo_glosa_w		number(5);

begin

select	max(cd_motivo_glosa)
into	cd_motivo_glosa_w
from 	convenio_retorno_item
where	nr_interno_conta = nr_interno_conta_p;

nr_retorno_w	:= nvl(cd_motivo_glosa_w,0);

return	nr_retorno_w;

end obter_motivo_glosa_ret_item;
/
