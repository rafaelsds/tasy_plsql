create or replace
function VerificaPacienteAtendCiru(nr_cirurgia_p	number,
				   nr_atendimento_p	number)
				return varchar2 is
ie_paciente_w	varchar2(1);

begin

select	nvl(max('S'),'N')
into	ie_paciente_w
from	cirurgia 
where	nr_cirurgia = nr_cirurgia_p
and	nr_atendimento = nr_atendimento_p;

return	ie_paciente_w;

end VerificaPacienteAtendCiru;
/