create or replace function	qua_obter_revisao_doc(nr_seq_documento_p	number)
return varchar2 is

	cd_funcao_gestao_qualidade_w 	number(4) := 4000;
	cd_par_gerar_codigo_revisao_w	number(3) := 227;
	ds_retorno_w			varchar2(20);
	ie_gerar_codigo_revisao_w 	varchar2(1);
	
begin
	ie_gerar_codigo_revisao_w := obter_parametro_funcao(cd_funcao_gestao_qualidade_w,
							cd_par_gerar_codigo_revisao_w,
							wheb_usuario_pck.get_nm_usuario);
	
	if (nvl(ie_gerar_codigo_revisao_w, 'S') = 'S') then
		select max(somente_numero(a.cd_revisao))
		into	ds_retorno_w
		from	qua_doc_revisao a
		where	a.nr_seq_doc = nr_seq_documento_p
		and 	somente_nao_numero(a.cd_revisao) is null;
	else
		select a.cd_revisao 
		into   ds_retorno_w
		from   qua_doc_revisao a 
		where  a.nr_seq_doc = nr_seq_documento_p 
		and    a.nr_sequencia = (select max(qdr.nr_sequencia) from qua_doc_revisao qdr where qdr.nr_seq_doc = nr_seq_documento_p);
		end if;
	return	ds_retorno_w;

end qua_obter_revisao_doc;
/
