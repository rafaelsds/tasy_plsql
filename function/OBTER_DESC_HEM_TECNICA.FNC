create or replace
function Obter_desc_hem_tecnica (nr_seq_tecnica_p		Number)
 		    	return Varchar2 is

ds_retorno_w	Varchar2(255);
				
begin

if	(nr_seq_tecnica_p is not null) then

	select 	max(ds_tecnica)
	into	ds_retorno_w
	from	hem_tecnica
	where	ie_situacao = 'A'
	and 	nr_sequencia = nr_seq_tecnica_p;

end if;

return	ds_retorno_w;

end Obter_desc_hem_tecnica;
/