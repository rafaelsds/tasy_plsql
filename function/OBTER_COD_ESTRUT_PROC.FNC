CREATE OR REPLACE
FUNCTION Obter_Cod_Estrut_Proc
		(cd_area_proced_p	number,
		cd_especial_proced_p	number,
		cd_grupo_proced_p	number,
		cd_procedimento_p	number,
		ie_origem_proced_p	number)
		RETURN number IS

cd_retorno_w		number(20,0);

begin
if	(cd_procedimento_p	is not null) then
	cd_retorno_w		:= cd_procedimento_p;
elsif	(cd_grupo_proced_p	is not null) then
	cd_retorno_w		:= cd_grupo_proced_p;
elsif	(cd_especial_proced_p	is not null) then
	cd_retorno_w		:= cd_especial_proced_p;
elsif	(cd_area_proced_p	is not null) then
	cd_retorno_w		:= cd_area_proced_p;
end if;

RETURN cd_retorno_w;
END Obter_Cod_Estrut_Proc;
/

