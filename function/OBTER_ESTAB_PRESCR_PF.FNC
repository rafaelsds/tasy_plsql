create or replace
function obter_estab_prescr_pf(cd_pessoa_fisica_p 	varchar2)
 		    	return number is

nr_prescricao_w		number(10);			
cd_estabelecimento_w	number(10);
			
begin

select	max(nr_prescricao)
into	nr_prescricao_w
from	prescr_medica
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_w;

return	cd_estabelecimento_w;

end obter_estab_prescr_pf;
/