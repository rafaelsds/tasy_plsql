create or replace
function far_obter_data_continuo(cd_pessoa_fisica_p	Varchar2,
			cd_material_p		Number,
			ie_tipo_retorno_p	Varchar2)
			return Date is

				
qt_medic_cont_w			number(5);
qt_consumo_w			number(14);
qt_dias_w			varchar2(5);
qt_pedido_w			number(5);
dt_ult_compra_w			date;
qt_material_pedido_w		number(14);
qt_uso_w				number(5);
cd_unidade_uso_w			varchar2(30);
qt_periodo_w			number(5);
ie_periodo_w			varchar2(3);
cd_unidade_venda_w		Varchar2(15);
cd_unidade_estoque_w 		Varchar2(15);
qt_conv_estoque_venda_w		number(13,4);
qt_dias_total_w			Number(14);
retorno_w				Date;
regra_w				number(13,4);

begin

select	count(*)
into	qt_medic_cont_w
from	far_medic_uso_continuo
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	cd_material	= cd_material_p;

if	(qt_medic_cont_w > 0) then

	select count(*)
	into	qt_pedido_w
	from	far_pedido a
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	ie_classificacao = 'P'
	and	dt_fechamento is not null
	and	exists 	(select	1
			from	far_pedido_item x
			where	x.nr_seq_pedido = a.nr_sequencia
			and	x.cd_material = cd_material_p);
	
	if	(qt_pedido_w > 0) then	
	
		select	max(cd_unidade_venda),
			max(cd_unidade_medida_estoque),
			max(qt_conv_estoque_venda)
		into	cd_unidade_venda_w,
			cd_unidade_estoque_w,
			qt_conv_estoque_venda_w
		from	material_estab
		where	cd_material = cd_material_p;

		select	max(qt_uso),
			max(cd_unidade_uso),
			max(qt_periodo),
			max(ie_periodo)
		into	qt_uso_w,
			cd_unidade_uso_w,
			qt_periodo_w,
			ie_periodo_w
		from	far_medic_uso_continuo
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	cd_material	= cd_material_p;	
	
		select	sum(b.qt_material),
			max(dt_fechamento)
		into	qt_material_pedido_W,
			dt_ult_compra_w
		from	far_pedido a,
			far_pedido_item b
		where	b.nr_seq_pedido = a.nr_sequencia
		and	b.cd_material = cd_material_p
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_fechamento is not null
		and	dt_fechamento =	(select	max(dt_fechamento)
					from	far_pedido x,
						far_pedido_item y
					where	y.nr_seq_pedido = x.nr_sequencia
					and	y.cd_material = cd_material_p
					and	x.cd_pessoa_fisica = cd_pessoa_fisica_p);

		qt_dias_total_w:= qt_periodo_w;
		
		if (ie_periodo_w = 'M') then
			qt_dias_total_w:= (qt_periodo_w * 30);
		end if;	
		
		if	(cd_unidade_uso_w = cd_unidade_estoque_w) and
			(cd_unidade_uso_w <> cd_unidade_venda_w) then
			qt_material_pedido_W:= (qt_material_pedido_w * qt_conv_estoque_venda_w);
		end if;
	
		regra_w:= (dividir(qt_dias_total_w,qt_uso_w)* qt_material_pedido_W);
		
		if (ie_tipo_retorno_p = 'U') then
			retorno_w:= dt_ult_compra_w;
		elsif (ie_tipo_retorno_p = 'P') then	
			retorno_w:= (dt_ult_compra_w+regra_w);
		end if;
	
	end if;
	
end if;
					
return	retorno_w;

end far_obter_data_continuo;
/
