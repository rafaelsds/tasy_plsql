CREATE OR REPLACE FUNCTION tx_obter_data_exame_meld (
    qt_ponto_meld_p  NUMBER,
    dt_exame_p       DATE
) RETURN DATE IS
    dt_prox_exame_w  DATE;
    sql_w            VARCHAR2(200);
BEGIN		


    BEGIN
        sql_w := 'CALL OBTER_DATA_PROX_EXAM_MD(:1, :2) INTO dt_prox_exame_w';
        EXECUTE IMMEDIATE sql_w
            USING IN qt_ponto_meld_p, IN dt_exame_p, OUT dt_prox_exame_w;
    EXCEPTION
        WHEN OTHERS THEN
            dt_prox_exame_w := NULL;
    END;


    RETURN dt_prox_exame_w;
END;
/