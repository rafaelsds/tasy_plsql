create or replace
function Obter_se_laudo_liberado(nr_seq_laudo_p		number)
 		    	return varchar2 is

ie_liberado_w		varchar2(1);
dt_liberacao_w		date;

begin

if (nr_Seq_laudo_p is not null) then

	select  dt_liberacao
	into	dt_liberacao_w
	from	laudo_paciente
	where	nr_sequencia = nr_seq_laudo_p;

	if (dt_liberacao_w is not null) then
		ie_liberado_w := 'S';
	else		
		ie_liberado_w := 'N';
	end if;

end if;

return	ie_liberado_w;

end Obter_se_laudo_liberado;
/