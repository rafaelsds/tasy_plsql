create or replace
function hem_obter_ramo_segmento (cd_segmento_w		varchar2)
 		    	return varchar2 is
			
ds_retorno_w	Varchar2(3);

begin
ds_retorno_w	:= '';

if	(cd_segmento_w in ('1','2','3','4','16','16a','16b','16c')) then
	ds_retorno_w := 'RCA';
end if;

if	(cd_segmento_w in ('6','7','8','9','9a','10','10a')) then
	ds_retorno_w := 'LAD';
end if;

if	(cd_segmento_w in ('11','12','12a','12b','13','14','14a','14b','15')) then
	ds_retorno_w := 'LCX';
end if;

return	ds_retorno_w;

end hem_obter_ramo_segmento;
/