create or replace
function obter_categoria_glosa_atend(nr_atendimento_p			number)
 		    	return varchar2 is

cd_categoria_glosa_w	varchar2(10);			
			
begin


select  max(cd_categoria_glosa)
into 	cd_categoria_glosa_w
from 	Atend_categoria_convenio a
where	a.nr_atendimento		= nr_atendimento_p
and  	a.dt_inicio_vigencia	= (select    max(dt_inicio_vigencia)
				   from	     Atend_categoria_convenio b
	                           where     nr_atendimento	= nr_atendimento_p);


return	cd_categoria_glosa_w;

end obter_categoria_glosa_atend;
/