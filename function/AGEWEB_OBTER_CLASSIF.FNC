create or replace
function ageweb_obter_classif(cd_especialidade_p	number,
			      qt_idade_p		number,
			      dt_agenda_p		date,
			      cd_profissional_p		varchar)
 		    	return varchar2 is

nr_seq_regra_w		number(10,0);
ie_classif_agenda_w	varchar2(5);			

Cursor C01 is
	select	nr_sequencia
	from	ageweb_regra_classif
	where	((cd_especialidade = cd_especialidade_p) or (cd_especialidade is null))
	and	((cd_pessoa_fisica = cd_profissional_p) or (cd_pessoa_fisica is null))
	and	qt_idade_p between nvl(qt_idade_min,0) and nvl(qt_idade_max,999)
	and	dt_agenda_p between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(dt_inicio_vigencia,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
				    to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(dt_fim_vigencia,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss')
	order by 
		nvl(cd_pessoa_fisica,'0'),
		nvl(cd_especialidade,0);
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_regra_w;
exit when C01%notfound;
	begin
	nr_seq_regra_w := nr_seq_regra_w;
	end;
end loop;
close C01;

select	max(ie_classif_agenda)
into	ie_classif_agenda_w
from	ageweb_regra_classif
where	nr_sequencia = nr_seq_regra_w;

return	ie_classif_agenda_w;

end ageweb_obter_classif;
/
