create or replace
function Obter_desc_alergeno(nr_seq_tipo_p	Number)
          	return varchar2 is

ds_retorno_w	varchar2(80);

begin

select	ds_tipo_alergia
into	ds_retorno_w
from	tipo_alergia
where	nr_sequencia	= nr_seq_tipo_p;

return	ds_retorno_w;

end Obter_desc_alergeno;
/