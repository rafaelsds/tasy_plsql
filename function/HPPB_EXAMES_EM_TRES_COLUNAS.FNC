create or replace
function hppb_exames_em_tres_colunas(nr_atendimento_p		number,
				 ie_opcao_p		varchar)
 		    	return varchar2 is
cd_exame_w		varchar(20);
ds_exame_w		varchar(255);
codigo_w			number(10);
controle_w		number(10);
ds_resultado_1		varchar(255);
ds_resultado_2		varchar(255);
ds_resultado_3		varchar(255);
espaco_w_1		varchar(255) := '';
espaco_w_2		varchar(255) := '';
espaco_w_3		varchar(255) := '';
ds_retorno_w		varchar(255);

/*ie_opcao = 1, 2 ou 3;*/

Cursor C01 is
	SELECT 	a.cd_exame,
		a.nm_exame,
		mod(rownum,3)
	from	(select	distinct 
			c.cd_exame cd_exame,
			c.nm_exame nm_exame
		 from	exame_laboratorio c,
			procedimento_paciente_v d,
			prescr_procedimento a,
			prescr_medica b
		 where	a.nr_prescricao	= d.nr_prescricao(+)
		 and	a.nr_sequencia	= d.nr_sequencia_prescricao(+)
		 and	a.nr_seq_exame	= c.nr_seq_exame
		 and	a.nr_prescricao	= b.nr_prescricao
		 and         b.nr_atendimento     = nr_atendimento_p) a;

begin
	open C01;
	loop
	fetch C01 into
		cd_exame_w,
		ds_exame_w,
		codigo_w;
	exit when C01%notfound;
		begin
			if (codigo_w = 2) then
				ds_resultado_3 := ds_resultado_3||espaco_w_3||cd_exame_w||' - '||ds_exame_w;
				espaco_w_3 := chr(13);
			end if;	
			if (codigo_w = 1) then
				ds_resultado_1 := ds_resultado_1||espaco_w_1||cd_exame_w||' - '||ds_exame_w;
				espaco_w_1 := chr(13);
			end if;
			if (codigo_w = 0) then
				ds_resultado_2 := ds_resultado_2||espaco_w_2||cd_exame_w||' - '||ds_exame_w;
				espaco_w_2 := chr(13);
			end if;
		end;
	end loop;
	close C01;
if (ie_opcao_p = 1) then
	ds_retorno_w := ds_resultado_1;
end if;
if (ie_opcao_p = 2) then
	ds_retorno_w := ds_resultado_2;
end if;
if (ie_opcao_p = 3) then
	ds_retorno_w := ds_resultado_3;
end if;

return	ds_retorno_w;

end hppb_exames_em_tres_colunas;
/