create or replace
function obter_valor_conta_prot(nr_protocolo_p		number,
				dt_parametro_p		date,
				ie_opcao_p		varchar2)
				return number is
/*ie_opcao_p:
ECH	- 	Buscar valor medico quando estrutura dos procedimentos for 71,91,92 e 93.
*/
vl_retorno_w		number(15,3);
vl_medico_w		number(15,3);

begin

if (ie_opcao_p = 'ECH') then
	select	nvl(sum(a.vl_medico),0)
	into	vl_medico_w
	from    procedimento_paciente a,
		conta_paciente b,
		titulo_receber c
	where	b.nr_seq_protocolo	= nr_protocolo_p	
	and	b.nr_seq_protocolo = c.NR_SEQ_PROTOCOLO
	and	c.ie_situacao <> '5'
	and	(c.ie_situacao <> '3' or (c.dt_liquidacao > dt_parametro_p or c.dt_liquidacao is null))
	and	((c.dt_liquidacao is null) or (c.dt_liquidacao > dt_parametro_p))
	and	a.nr_interno_conta = b.nr_interno_conta
	and	b.ie_cancelamento	IS NULL
	and	b.cd_estabelecimento	= obter_estabelecimento_ativo
	and	a.ie_emite_conta in (71, 91, 92, 93);
	
	vl_retorno_w :=	vl_medico_w;
end if;

return	vl_retorno_w;

end obter_valor_conta_prot;
/