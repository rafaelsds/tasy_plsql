create or replace
function Obter_macros_comunic_aval return varchar2 is

ds_retorno_w            varchar2(2000);
ds_enter_w              varchar2(10) := chr(13) || chr(10);

begin

ds_retorno_w    :=      '@Paciente 	= Nome do paciente.' 			|| ds_enter_w ||
			'@Idade		= Idade do paciente.' 			|| ds_enter_w ||
			'@Altura		= Altura do paciente.'		|| ds_enter_w ||
			'@Sexo_abrev 	= Abrev. do sexo do paciente.' 		|| ds_enter_w ||
			'@Sexo	 	= Sexo do paciente.' 			|| ds_enter_w ||
			'@Prontuario 	= Prontu�rio do paciente.' 		|| ds_enter_w ||
			'@Atendimento 	= N�mero do atendimento.' 		|| ds_enter_w ||
			'@Avaliacao 	= Tipo de avalia��o.'	 		|| ds_enter_w ||
			'@Data_atual 	= Data atual.'	 			|| ds_enter_w ||
			'@Data_hr_atual	= Data e hora atual.'			|| ds_enter_w ||
			'@Nascimento 	= Data de nascimento.'	 		|| ds_enter_w ||
			'@Usuario 	= Usu�rio.'	 			|| ds_enter_w ||
			'@Setor		= Setor.'	 			|| ds_enter_w ||
			'@Pac_setor	= Setor do paciente.'	 		|| ds_enter_w ||
			'@Pac_set_atual	= Setor atual do paciente.'	 		|| ds_enter_w ||
			'@CID 		= CID do atendimento.'			|| ds_enter_w ||
			'@Data_entrada 	= Data entrada.'		 	|| ds_enter_w ||
			'@Data_aval	= Data da avalia��o.'		 	|| ds_enter_w ||
			'@Pessoa_lib	= Pessoa que liberou a avalia��o.'	|| ds_enter_w ||
			'@Nr_aval	= N�mero da avalia��o.';

return ds_retorno_w;

end Obter_macros_comunic_aval;
/