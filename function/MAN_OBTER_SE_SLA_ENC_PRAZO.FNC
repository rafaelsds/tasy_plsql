create or replace
function man_obter_se_sla_enc_prazo(
			nr_sequencia_p	number)
		return varchar2 is

cd_estabelecimento_w	number(4);
ie_tempo_w		varchar2(15);
dt_inicio_w		date;
dt_termino_w	date;
qt_min_termino_w	number(15);
ds_retorno_w		varchar2(1);
dt_fim_real_w		date;

begin

/* Utilizada no relatório de SLAs */

if	(nr_sequencia_p is not null) then
	begin

	select	cd_estabelecimento,
		ie_tempo,
		dt_inicio,
		qt_min_termino
	into	cd_estabelecimento_w,
		ie_tempo_w,
		dt_inicio_w,
		qt_min_termino_w
	from	man_ordem_serv_sla
	where	nr_seq_ordem	= nr_sequencia_p;
	
	if	(ie_tempo_w = 'COR') then
		dt_termino_w	:= dt_inicio_w + (qt_min_termino_w / 1440);
	else
		dt_termino_w	:= man_obter_hor_com(cd_estabelecimento_w, dt_inicio_w, qt_min_termino_w);
	end if;

	select	nvl(dt_fim_real,sysdate)
	into	dt_fim_real_w
	from	man_ordem_servico
	where	nr_sequencia = nr_sequencia_p;
	
	if	(dt_termino_w >= dt_fim_real_w) then /* SLA encerrada no prazo*/
		ds_retorno_w := 'S';
	elsif	(dt_termino_w < dt_fim_real_w) then /* SLA encerrada fora do prazo */
		ds_retorno_w := 'N';
	end if;
	
	end;
end if;

return ds_retorno_w;

end man_obter_se_sla_enc_prazo;
/