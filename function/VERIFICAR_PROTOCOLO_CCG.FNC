create or replace
function verificar_protocolo_ccg (	nr_seq_protocolo_p	number,
					nr_prescricao_p		number,
					nr_seq_procedimento_p	number
					) 
				return varchar2 is
				
ie_retorno_w		varchar2(1) := 'S';
qt_glic_inic_w		number(3,0);
qt_glic_fim_w		number(3,0);
qt_glic_inic_aux_w	number(10,0) := null;
qt_glic_fim_aux_w	number(10,0) := null;
					
Cursor C01 is
	select	a.qt_glic_inic,
		a.qt_glic_fim		
	from	prescr_proc_glic a
	where	a.nr_seq_protocolo = nr_seq_protocolo_p
	and	a.nr_seq_procedimento = nr_seq_procedimento_p
	and	a.nr_prescricao = nr_prescricao_p
	and	ie_retorno_w = 'S'
	order by nvl(qt_glic_inic,0);
					
begin

select	nvl(max('S'),'N')
into	ie_retorno_w
from	prescr_proc_glic b
where	b.nr_seq_protocolo = nr_seq_protocolo_p
and	b.nr_seq_procedimento = nr_seq_procedimento_p
and	b.nr_prescricao = nr_prescricao_p
and	b.qt_glic_fim = 999;

open C01;
loop
fetch C01 into	
	qt_glic_inic_w,
	qt_glic_fim_w;
exit when C01%notfound;
	begin
		
	if	(qt_glic_inic_aux_w is null) then 
		begin
		qt_glic_inic_aux_w := qt_glic_inic_w;
		qt_glic_fim_aux_w  := qt_glic_fim_w + 1;
		if	(qt_glic_inic_aux_w <> 0) then
			ie_retorno_w := 'N';
			Exit;		
		end if;		
		
		end;
	else	
		begin			
		if	(qt_glic_fim_aux_w <> qt_glic_inic_w) then
			ie_retorno_w := 'N';
			Exit;
		else
			qt_glic_fim_aux_w := qt_glic_fim_w + 1;
		end if;
		end;
	end if;		
	
	
	end;
end loop;
close C01;

return ie_retorno_w;

end verificar_protocolo_ccg;
/