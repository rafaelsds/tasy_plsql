create or replace
function obter_glosa_procmat_rep(	nr_titulo_p	number)
				return number is

vl_procedimento_w			number(15,2);
vl_material_w			number(15,2);
vl_retorno_w			number(15,2);

begin

if	(nr_titulo_p is not null) then

	select	nvl(sum(somente_positivo(nvl(vl_repasse,0) - nvl(vl_liberado,0))),0) vl
	into	vl_procedimento_w
	from 	procedimento_paciente c,
		procedimento_repasse b,
		titulo_pagar a
	where	a.nr_titulo		= nr_titulo_p
	and	a.nr_repasse_terceiro	= b.nr_repasse_terceiro
	and	b.nr_seq_procedimento	= c.nr_sequencia;
	
	select	nvl(sum(somente_positivo(nvl(vl_repasse,0) - nvl(vl_liberado,0))),0) vl
	into	vl_material_w
	from	material_atend_paciente c,
		material_repasse b,
		titulo_pagar a
	where	a.nr_titulo		= nr_titulo_p
	and	a.nr_repasse_terceiro	= b.nr_repasse_terceiro
	and	b.nr_seq_material	= c.nr_sequencia;

	vl_retorno_w	:= nvl(vl_procedimento_w,0) + nvl(vl_material_w,0);
end if;

return	nvl(vl_retorno_w,0);

end obter_glosa_procmat_rep;
/
