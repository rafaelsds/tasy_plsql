create or replace
function obter_desc_result_sorologia(ie_resultado_p		varchar2)
 		    	return varchar2 is
ds_resultado_w	varchar2(255) := Wheb_mensagem_pck.get_texto(309128); --'N�o testado';
begin
if 	(ie_resultado_p	 = 'P') then
	ds_resultado_w := Wheb_mensagem_pck.get_texto(309130); --'Positivo';
elsif 	(ie_resultado_p	 = 'N') then
	ds_resultado_w := Wheb_mensagem_pck.get_texto(309132); --'Negativo';
elsif 	(ie_resultado_p	 = 'I') then
	ds_resultado_w := Wheb_mensagem_pck.get_texto(309134); --'Indeterminado';
end if;


return	ds_resultado_w;

end obter_desc_result_sorologia;
/