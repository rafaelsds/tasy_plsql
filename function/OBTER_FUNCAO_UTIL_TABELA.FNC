create or replace
function Obter_funcao_util_tabela (nm_tabela_p		varchar2,
								   ie_opcao_p		varchar2)
return varchar2 is

ds_retorno_w	varchar2(255);

begin

if	(ie_opcao_p = 'F') then --Nome da fun��o
	if	(nm_tabela_p = 'ANVISA_VIA_ADMINISTRACAO') then
		ds_retorno_w	:= obter_desc_expressao(293070);--'Medicamento';
	elsif (nm_tabela_p = 'CONSISTENCIA_LIB_PEPO') then
		ds_retorno_w	:= obter_desc_expressao(335345);--'Cadastro dos Itens Peroperat�rios';
	elsif (nm_tabela_p in('FANEP_ITEM', 'INDICADOR_GESTAO', 'FICHA_FINANC_ITEM', 'INDICADOR_GESTAO_ATRIB', 'OFTALMOLOGIA_ITEM',
						'OFT_ACAO', 'PEPO_ACAO', 'PEPO_ITEM', 'PEP_ACAO', 'PRONTUARIO_ITEM', 'PRONTUARIO_PASTA','REGRA_CONSISTE_ONC',
						'REGRA_CONSISTE_PRESCR', 'SAEP_ITEM', 'SUBINDICADOR_GESTAO', 'SUBINDICADOR_GESTAO_ATRIB', 'TASY_LEGENDA',
						'TASY_PADRAO_COR', 'TASY_PADRAO_IMAGEM')) then
		ds_retorno_w	:= obter_desc_expressao(291107);--'Administra��o do sistema';
	elsif (nm_tabela_p in('FUNCAO', 'FUNCAO_PARAMETRO', 'INCONSISTENCIA_ESCRIT', 'MODULO_IMPLANTACAO', 'APLICACAO_TASY','MODULO_TASY')) then
		ds_retorno_w	:= obter_desc_expressao(630768);--'Dicion�rio de Dados';
	elsif (nm_tabela_p = 'EVENTO_TASY') then
		ds_retorno_w	:= obter_desc_expressao(779719);--'Shift+F11/Eventos Tasy';
	elsif (nm_tabela_p = 'INFORMACAO_CONTABIL') then
		ds_retorno_w	:= obter_desc_expressao(780325);--'Shift+F11/Informa��es cont�beis';
	elsif (nm_tabela_p = 'MAN_SEVERIDADE') then
		ds_retorno_w	:= obter_desc_expressao(780174);--'Shift+F11/Severidade dos defeitos';
	elsif (nm_tabela_p = 'PERGUNTA_RECUP_SENHA') then
		ds_retorno_w	:= obter_desc_expressao(780317);--'Shift+F11/Perguntas para recupera��o de senha';
	elsif (nm_tabela_p = 'PE_ESCALA_ITEM') then
		ds_retorno_w	:= obter_desc_expressao(780397);--'Shift+F11/Escalas e �ndices vinculadas � SAE/Itens da escala e �ndice';
	elsif (nm_tabela_p = 'REGRA_CONSISTE_SAE') then
		ds_retorno_w	:= obter_desc_expressao(780473);--'PEP/Cadastros de Enfermagem';
	elsif (nm_tabela_p = 'SAME_OPERACAO') then
		ds_retorno_w	:= obter_desc_expressao(780246);--'Shift+F11/Opera��es realizadas com prontu�rio';
	elsif (nm_tabela_p = 'TIPO_AMPUTACAO') then
		ds_retorno_w	:= obter_desc_expressao(780240);--'Shift+F11/Tipos de amputa��o';
	elsif (nm_tabela_p = 'TIPO_LOCALIZAR_ATRIBUTO') then
		ds_retorno_w	:= obter_desc_expressao(780327);--'Shift+F11/Tipo de localizador/Atributos do tipo localizar';
	elsif (nm_tabela_p in('TIPO_LOTE_CONTABIL', 'TIPO_LOTE_CONTABIL_PARAM')) then
		ds_retorno_w	:= obter_desc_expressao(780329);--'Cadastros de Tipo de Lote Cont�bil';
	else
		ds_retorno_w	:= obter_desc_expressao(630768);--'Dicion�rio de Dados';
	end if;
else
	if	(nm_tabela_p = 'ANVISA_VIA_ADMINISTRACAO') then
		ds_retorno_w	:= obter_desc_expressao(780319);--'Cadastro/Anvisa/Via de administra��o';
	elsif (nm_tabela_p = 'CONSISTENCIA_LIB_PEPO') then
		ds_retorno_w	:= obter_desc_expressao(780387);--'PEPO/Consist�ncia Lib PEPO';
	elsif (nm_tabela_p = 'APLICACAO_TASY') then
		ds_retorno_w	:= obter_desc_expressao(283600);--'Aplica��o';
	elsif (nm_tabela_p = 'FANEP_ITEM') then
		ds_retorno_w	:= obter_desc_expressao(780242);--'Par�metros Assistenciais/Itens FANEP';
	elsif (nm_tabela_p = 'FICHA_FINANC_ITEM') then
		ds_retorno_w	:= obter_desc_expressao(780248);--'Par�metros/Itens Ficha Financeira';
	elsif (nm_tabela_p = 'FUNCAO') then
		ds_retorno_w	:= obter_desc_expressao(304443);--'Fun��es';
	elsif (nm_tabela_p = 'FUNCAO_PARAMETRO') then
		ds_retorno_w	:= obter_desc_expressao(780389);--'Fun��es/Par�metros';
	elsif (nm_tabela_p = 'INCONSISTENCIA_ESCRIT') then
		ds_retorno_w	:= obter_desc_expressao(780381);--'Cadastros/Inconsist�ncia escritural';
	elsif (nm_tabela_p = 'INDICADOR_GESTAO') then
		ds_retorno_w	:= obter_desc_expressao(780177);--'Ind Gest�o/Indicador';
	elsif (nm_tabela_p = 'INDICADOR_GESTAO_ATRIB') then
		ds_retorno_w	:= obter_desc_expressao(780250);--'Ind Gest�o/Dimens�es';
	elsif (nm_tabela_p = 'MODULO_IMPLANTACAO') then
		ds_retorno_w	:= obter_desc_expressao(780399);--'M�dulo Implanta��o/Fun��es';
	elsif (nm_tabela_p = 'MODULO_TASY') then
		ds_retorno_w	:= obter_desc_expressao(283600);--'Aplica��o';
	elsif (nm_tabela_p = 'OFTALMOLOGIA_ITEM') then
		ds_retorno_w	:= obter_desc_expressao(780168);--'Parametros Assistenciais/Itens Oftalmologia/Itens';
	elsif (nm_tabela_p = 'OFT_ACAO') then
		ds_retorno_w	:= obter_desc_expressao(780321);--'Parametros Assistenciais/Itens Oftalmologia/A��o';
	elsif (nm_tabela_p = 'PEPO_ACAO') then
		ds_retorno_w	:= obter_desc_expressao(780391);--'Parametros Assistenciais/Itens PEPO/A��o';
	elsif (nm_tabela_p = 'PEPO_ITEM') then
		ds_retorno_w	:= obter_desc_expressao(780471);--'Parametros Assistenciais/Itens PEPO/Itens';
	elsif (nm_tabela_p = 'PEP_ACAO') then
		ds_retorno_w	:= obter_desc_expressao(780491);--'Parametros Assistenciais/Itens Prontuario/A��o';
	elsif (nm_tabela_p = 'PRONTUARIO_ITEM') then
		ds_retorno_w	:= obter_desc_expressao(780252);--'Parametros Assistenciais/Itens Prontuario';
	elsif (nm_tabela_p = 'PRONTUARIO_PASTA') then
		ds_retorno_w	:= obter_desc_expressao(780393);--'Parametros Assistenciais/Pastas do Prontu�rio';
	elsif (nm_tabela_p = 'REGRA_CONSISTE_ONC') then
		ds_retorno_w	:= obter_desc_expressao(780475);--'Parametros Assistenciais/Quimioterapia/Consist�ncias';
	elsif (nm_tabela_p = 'REGRA_CONSISTE_PRESCR') then
		ds_retorno_w	:= obter_desc_expressao(780179);--'Parametros Assistenciais/Prescri��o/Consist�ncias';
	elsif (nm_tabela_p = 'REGRA_CONSISTE_SAE') then
		ds_retorno_w	:= obter_desc_expressao(689943);--'Consist�ncias';
	elsif (nm_tabela_p = 'SAEP_ITEM') then
		ds_retorno_w	:= obter_desc_expressao(780244);--'Parametros Assistenciais/Itens SAEP';
	elsif (nm_tabela_p = 'SUBINDICADOR_GESTAO') then
		ds_retorno_w	:= obter_desc_expressao(780395);--'Ind Gest�o/Subindicador';
	elsif (nm_tabela_p = 'SUBINDICADOR_GESTAO_ATRIB') then
		ds_retorno_w	:= obter_desc_expressao(780323);--'Ind Gest�o/Subindicador/Atributos subindicador';
	elsif (nm_tabela_p = 'TASY_LEGENDA') then
		ds_retorno_w	:= obter_desc_expressao(780170);--'Par�metros/Cores/Legenda';
	elsif (nm_tabela_p = 'TASY_PADRAO_COR') then
		ds_retorno_w	:= obter_desc_expressao(780254);--'Par�metros/Cores/Padr�o Cor';
	elsif (nm_tabela_p = 'TASY_PADRAO_IMAGEM') then
		ds_retorno_w	:= obter_desc_expressao(780172);--'Par�metros/Imagens';
	elsif (nm_tabela_p = 'TIPO_LOTE_CONTABIL') then
		ds_retorno_w	:= obter_desc_expressao(779721);--'Lote Cont�bil/Par�metros';
	elsif (nm_tabela_p = 'TIPO_LOTE_CONTABIL_PARAM') then
		ds_retorno_w	:= obter_desc_expressao(779721);--'Lote Cont�bil/Par�metros';
	else
		ds_retorno_w	:= obter_desc_expressao(780256);--'EHR/Cadastros';
	end if;
end if;

return	ds_retorno_w;

end Obter_funcao_util_tabela;
/