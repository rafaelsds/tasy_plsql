create or replace 
function obter_se_solic_vaga(cd_pessoa_fisica_p	varchar2 )
				return varchar2 is

ie_possui_reserva_w	varchar2(1);

begin

select	nvl(max('S'),'N')
into	ie_possui_reserva_w
from	gestao_vaga
where	cd_pessoa_fisica	=	cd_pessoa_fisica_p
and	dt_solicitacao 		<= sysdate
and	ie_status in ('P','A')
and	((nr_atendimento is null) or 
	(nr_atendimento is not null and  OBTER_SE_ATENDIMENTO_ALTA(nr_atendimento) = 'N' ));

return	ie_possui_reserva_w;

end obter_se_solic_vaga;
/
