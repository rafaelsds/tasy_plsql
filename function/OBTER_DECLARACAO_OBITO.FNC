create or replace
function Obter_Declaracao_Obito
		(nr_atendimento_p	number)
		return varchar2 is

nr_declaracao_w		varchar2(20);

begin

select	max(nr_declaracao)
into	nr_declaracao_w
from 	declaracao_obito
where	nr_atendimento	= nr_atendimento_p
and	ie_situacao	= 'A';

return	nr_declaracao_w;

end Obter_Declaracao_Obito;
/
