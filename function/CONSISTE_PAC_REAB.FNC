create or replace 
FUNCTION consiste_pac_reab(nr_sequencia_p    NUMBER,
			       dt_fim_tratamento_p    DATE,
					ie_opcao_p    NUMBER)
				    RETURN NUMBER IS
nr_qtd_w	NUMBER := 0;

BEGIN
    IF (ie_opcao_p = 1)	THEN
	--RF1
	SELECT 1
	INTO nr_qtd_w
	FROM RP_PAC_AGEND_INDIVIDUAL i,
		 agenda_consulta a
	WHERE 1 = 1
	AND a.nr_seq_rp_item_ind = i.nr_sequencia
	AND a.nr_atendimento IS NOT NULL
	AND i.nr_sequencia = nr_sequencia_p
	AND a.dt_agenda > dt_fim_tratamento_p;

    ELSIF (ie_opcao_p = 4) or (ie_opcao_p = 2) THEN
	--RF2/RF4
	SELECT 1
	INTO nr_qtd_w
	FROM agenda_consulta a
	WHERE a.nr_seq_rp_mod_item IN (SELECT x.nr_sequencia
					FROM	RP_PAC_MODELO_AGEND_ITEM x
					WHERE x.nr_seq_modelo_pac = nr_sequencia_p)
	AND a.nr_atendimento IS NOT NULL		
	AND a.dt_agenda > dt_fim_tratamento_p;

    ELSIF (ie_opcao_p = 3) THEN
	--RF3
	SELECT 1
	INTO nr_qtd_w
	FROM RP_PAC_MODELO_AGEND_ITEM i,
		  agenda_consulta a
	WHERE 1=1
	AND i.nr_sequencia = a.nr_seq_rp_mod_item
	AND a.nr_atendimento IS NOT NULL
	and i.nr_sequencia = nr_sequencia_p
	AND a.dt_agenda > dt_fim_tratamento_p;
    END IF;


RETURN nr_qtd_w;

END consiste_pac_reab;
/