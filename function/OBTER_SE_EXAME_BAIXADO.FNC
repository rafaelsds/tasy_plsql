create or replace
function Obter_se_exame_baixado(	nr_prescricao_p		number,
									nr_seq_prescr_p		number)
 		    	return varchar2 is

cd_motivo_baixa_w		number(3);

begin

select	cd_motivo_baixa
into	cd_motivo_baixa_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and		nr_sequencia = nr_seq_prescr_p;

if	(cd_motivo_baixa_w = 0) then
	return 'N';
else
	return 'S';
end if;

end Obter_se_exame_baixado;
/