create or replace
function pls_obter_cd_seq_guia
			(	nr_seq_guia_p		Number,
				cd_guia_p	Varchar2)
				return Varchar2 is
	
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter o c�digo ou sequ�ncia da guia
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	
ds_retorno_w			Varchar2(20);
nr_seq_guia_w			pls_guia_plano.nr_sequencia%type;

begin

if	(nr_seq_guia_p	is not null) then
	select	cd_guia
	into	ds_retorno_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_p;
elsif	(cd_guia_p	is not null) then
	select	max(nr_sequencia)
	into	nr_seq_guia_w
	from	pls_guia_plano
	where	cd_guia	= cd_guia_p;
	
	ds_retorno_w := to_char(nr_seq_guia_w);
end if;

return	ds_retorno_w;

end pls_obter_cd_seq_guia;
/