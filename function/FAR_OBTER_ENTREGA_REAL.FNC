create or replace
function	far_obter_entrega_real	(nr_seq_pedido_p		number) return date is

dt_retorno_w		date;

begin

select	dt_entrega_real
into	dt_retorno_w
from	far_rom_entrega_pedido
where	nr_seq_pedido = nr_seq_pedido_p;

return dt_retorno_w;

end far_obter_entrega_real;
/