create or replace
function obter_material_conversao_ext(	
				cd_material_p 	number,
				ie_opcao_p 	varchar2)
				return varchar2 is

result_w 	varchar2(100);
type_w 	varchar(3);
code_w 	varchar(100);

-- 	IE_OPCAO_P
-- 	C 	= CODE
-- 	T 	= TYPE

begin

begin
	if(cd_material_p is not null) and (ie_opcao_p is not null) then
		select	max(nvl(gfc_code, gcr_code)),
			max(decode(gfc_code, null, decode(gcr_code, null, '','GCR'), 'GFC'))
		into	code_w,
			type_w
		from	material_conversao
		where	cd_material = cd_material_p;
			
		case ie_opcao_p
			when 'C' then result_w := code_w;
			when 'T' then result_w := type_w;
			else result_w := '';
		end case;		
		
	end if;
exception when others then
	result_w := '';
end;

return result_w;

end obter_material_conversao_ext;
/

