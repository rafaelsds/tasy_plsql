create or replace
function reg_is_review_pending_user (	nr_seq_revision_p		number,
					nm_usuario_p			varchar2 )
 		    	return varchar2 is

nr_seq_rev_control_w			reg_version_approvers.nr_seq_rev_control%type;
nm_author_w				reg_doc_revisor.nm_usuario_revisor%type;
ie_review_pending_w			varchar2(1);

begin

select	reg_fetch_revision_author(nr_seq_revision_p)
into	nm_author_w
from	dual;

select	decode(count(1), 0, 'N', 'S')
into	ie_review_pending_w
from	reg_version_revision
where	nr_sequencia = nr_seq_revision_p
and	ie_status_revisao in ('U', 'A')
and	(nm_author_w = nm_usuario_p or reg_doc_has_user_delegation(nr_seq_documento, 'C', nm_author_w, nm_usuario_p) = 'S');

if (ie_review_pending_w = 'N') then

	select	max(nr_sequencia)
	into	nr_seq_rev_control_w
	from	reg_revision_control
	where	nr_seq_revisao = nr_seq_revision_p;

	select	decode(count(1), 0, 'N', 'S')
	into	ie_review_pending_w
	from	reg_version_revision vr,
		reg_version_approvers va
	where	vr.nr_sequencia = va.nr_seq_revision
	and	vr.nr_sequencia = nr_seq_revision_p
	and	(va.nm_approver = nm_usuario_p or reg_doc_has_user_delegation(vr.nr_seq_documento, va.ie_approver_type, va.nm_approver, nm_usuario_p) = 'S')
	and	va.nr_seq_rev_control = nr_seq_rev_control_w
	and	(
			(va.ie_approver_type = 'R' and (
								(vr.ie_status_revisao = 'R' and va.ie_aceito is null) or
								(vr.ie_status_revisao = 'S' and va.dt_approved is null)
							)
			) or
			(va.ie_approver_type = 'A' and vr.ie_status_revisao = 'P' and va.ie_aceito is null)
		);

end if;

return ie_review_pending_w;

end reg_is_review_pending_user;
/
