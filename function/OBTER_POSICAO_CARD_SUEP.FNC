create or replace
function obter_posicao_card_suep ( 	nr_seq_suep_p	number,
									nr_seq_item_p	number,
									ie_posicao_p	varchar2)
									return number is
			

			
qt_posicao_w 				number(10):= 0;
qt_posicao_card_x_w			number(10):= 0;
qt_linha_x_w				number(10):= 0;
qt_posicao_card_y_w			number(10):= 0;
qt_posicao_card_atual_x_w	number(10):= 0;
qt_posicao_card_atual_y_w	number(10):= 0;

qt_preenchido_x_0_w			number(10):= 0;
qt_preenchido_x_1_w			number(10):= 0;
qt_preenchido_x_2_w			number(10):= 0;
qt_preenchido_x_3_w			number(10):= 0;

nr_sequencia_w				number(10);
ie_tamanho_card_w			varchar2(1);
ie_posicao_usuario_w		varchar2(1);
nm_usuario_w				varchar2(15);
nr_seq_apres_w				number(10);
ds_item_w					varchar2(255);
ie_tipo_item_w				varchar2(5);


Cursor C01 is
	Select 	nr_sequencia,
			decode(ie_tipo_item,'SO','G','PN','G',nvl(ie_tamanho_card,obter_tamanho_card_suep(ie_tipo_item)))
	from   	item_suep
	where  	nr_seq_suep = nr_seq_suep_p
	order by NR_SEQ_APRES_CARD;


begin


nm_usuario_w := wheb_usuario_pck.get_nm_usuario;


Select 	nvl(max('S'),'N')
into	ie_posicao_usuario_w
from 	suep_posicao_usuario
where	nm_usuario = nm_usuario_w
and		nr_seq_suep = nr_seq_suep_p;


if (ie_posicao_usuario_w = 'S') then

	Select  max(ie_tipo_item)
	into	ie_tipo_item_w
	from   	item_suep
	where 	nr_sequencia = nr_seq_item_p;

	Select 	max(pos_x),
			max(pos_y)
	into	qt_posicao_card_x_w,
			qt_posicao_card_y_w
	from 	suep_posicao_usuario
	where	nm_usuario = nm_usuario_w
	and		nr_seq_suep = nr_seq_suep_p
	and		ie_tipo_item = ie_tipo_item_w;	


else

	open C01;
	loop
	fetch C01 into
		nr_sequencia_w,
		ie_tamanho_card_w;
	exit when C01%notfound;
		begin	
		
		qt_posicao_card_x_w	:= 	qt_posicao_card_atual_x_w;
			
				
		if ( qt_posicao_card_x_w = 0 and qt_preenchido_x_0_w = 1) then		
			
			qt_posicao_card_x_w := qt_posicao_card_x_w + qt_preenchido_x_0_w + qt_preenchido_x_1_w + qt_preenchido_x_2_w + qt_preenchido_x_3_w;
			qt_posicao_card_atual_x_w := qt_posicao_card_x_w;
			qt_preenchido_x_0_w	:= 0;
			qt_preenchido_x_1_w	:= 0;
			qt_preenchido_x_2_w	:= 0;
			qt_preenchido_x_3_w	:= 0;
			
		elsif ( qt_posicao_card_x_w = 1 and qt_preenchido_x_1_w = 1) then	
		
			qt_posicao_card_x_w := qt_posicao_card_x_w + qt_preenchido_x_1_w + qt_preenchido_x_2_w + qt_preenchido_x_3_w;
			qt_posicao_card_atual_x_w := qt_posicao_card_x_w;			
			qt_preenchido_x_1_w	:= 0;
			qt_preenchido_x_2_w	:= 0;
			qt_preenchido_x_3_w	:= 0;
			
		elsif ( qt_posicao_card_x_w = 2 and qt_preenchido_x_2_w = 1) then

			qt_posicao_card_x_w := qt_posicao_card_x_w +  qt_preenchido_x_2_w + qt_preenchido_x_3_w;
			qt_posicao_card_atual_x_w := qt_posicao_card_x_w;			
			qt_preenchido_x_2_w	:= 0;
			qt_preenchido_x_3_w	:= 0;
			
		elsif (qt_posicao_card_x_w = 3 and qt_preenchido_x_3_w = 1) then	
			
			qt_posicao_card_x_w := qt_posicao_card_x_w + qt_preenchido_x_3_w;
			qt_posicao_card_atual_x_w := qt_posicao_card_x_w;			
			qt_preenchido_x_3_w	:= 0;
		
		end if;
		
				
		qt_posicao_card_y_w := trunc( qt_posicao_card_x_w / 4) + qt_linha_x_w;
		
		if ( qt_posicao_card_x_w > 3 or (qt_posicao_card_x_w  = 3 and (ie_tamanho_card_w = 'G' or ie_tamanho_card_w = 'H'))) then
			
			qt_posicao_card_x_w := 0;
			qt_posicao_card_atual_x_w := 0;
			qt_linha_x_w := qt_linha_x_w + 1;
		
		end if;
		
		if ( qt_posicao_card_x_w = 0 and qt_preenchido_x_0_w = 1) then		
			
			qt_posicao_card_x_w := qt_posicao_card_x_w + qt_preenchido_x_0_w + qt_preenchido_x_1_w + qt_preenchido_x_2_w + qt_preenchido_x_3_w;
			qt_posicao_card_atual_x_w := qt_posicao_card_x_w;
			qt_preenchido_x_0_w	:= 0;
			qt_preenchido_x_1_w	:= 0;
			qt_preenchido_x_2_w	:= 0;
			qt_preenchido_x_3_w	:= 0;
			
		elsif ( qt_posicao_card_x_w = 1 and qt_preenchido_x_1_w = 1) then
		
			qt_posicao_card_x_w := qt_posicao_card_x_w + qt_preenchido_x_1_w + qt_preenchido_x_2_w + qt_preenchido_x_3_w;
			qt_posicao_card_atual_x_w := qt_posicao_card_x_w;			
			qt_preenchido_x_1_w	:= 0;
			qt_preenchido_x_2_w	:= 0;
			qt_preenchido_x_3_w	:= 0;
			
		elsif ( qt_posicao_card_x_w = 2 and qt_preenchido_x_2_w = 1) then
		
			qt_posicao_card_x_w := qt_posicao_card_x_w +  qt_preenchido_x_2_w + qt_preenchido_x_3_w;			
			qt_posicao_card_atual_x_w := qt_posicao_card_x_w;
			qt_preenchido_x_2_w	:= 0;
			qt_preenchido_x_3_w	:= 0;
			
		elsif (qt_posicao_card_x_w = 3 and qt_preenchido_x_3_w = 1) then
		
			qt_posicao_card_x_w := qt_posicao_card_x_w + qt_preenchido_x_3_w;			
			qt_posicao_card_atual_x_w := qt_posicao_card_x_w;
			qt_preenchido_x_3_w	:= 0;
		
		end if;


		
		qt_posicao_card_y_w := trunc( qt_posicao_card_x_w / 4) + qt_linha_x_w;
		
		if ( qt_posicao_card_x_w > 3 or ( qt_posicao_card_x_w  = 3 and ( ie_tamanho_card_w = 'G' or ie_tamanho_card_w = 'H'))) then
			
			qt_posicao_card_x_w := 0;
			qt_posicao_card_atual_x_w := 0;
			qt_linha_x_w := qt_linha_x_w + 1;
			qt_posicao_card_y_w := qt_posicao_card_y_w + 1;
		
		end if;
				
		if ( ie_tamanho_card_w = 'P' ) then
		
			qt_posicao_card_atual_x_w := qt_posicao_card_atual_x_w + 1;
					
		elsif ( ie_tamanho_card_w = 'V' ) then
		
			if ( qt_posicao_card_atual_x_w  = 0) then
			
				qt_preenchido_x_0_w := 1;
			
			elsif ( qt_posicao_card_atual_x_w = 1) then
			
				qt_preenchido_x_1_w := 1;
				
			elsif ( qt_posicao_card_atual_x_w = 2) then
			
				qt_preenchido_x_2_w := 1;
				
			elsif ( qt_posicao_card_atual_x_w = 3) then
			
				qt_preenchido_x_3_w := 1;
			
			end if;	
			
			qt_posicao_card_atual_x_w := qt_posicao_card_atual_x_w + 1;		
			
		elsif ( ie_tamanho_card_w = 'G' ) then
		
			if ( qt_posicao_card_atual_x_w  = 0) then
			
				qt_preenchido_x_0_w := 1;
				qt_preenchido_x_1_w := 1;
			
			elsif ( qt_posicao_card_atual_x_w = 1) then
			
				qt_preenchido_x_1_w := 1;
				qt_preenchido_x_2_w := 1;
				
			elsif ( qt_posicao_card_atual_x_w = 2) then
			
				qt_preenchido_x_2_w := 1;
				qt_preenchido_x_3_w := 1;
			
			end if;			
					
			qt_posicao_card_atual_x_w := qt_posicao_card_atual_x_w + 2;
			
		elsif ( ie_tamanho_card_w = 'H' ) then
			
			qt_posicao_card_atual_x_w := qt_posicao_card_atual_x_w + 2;	
			
			
		end if;
		
		if ( nr_sequencia_w = nr_seq_item_p ) then
			goto Fim; 
		end if;
		
		
		end;
	end loop;
	close C01;
	
end if;



<<Fim>>
if ( ie_posicao_p = 'X' ) then

	qt_posicao_w := qt_posicao_card_x_w;
	
elsif ( ie_posicao_p = 'Y' ) then	

	qt_posicao_w := qt_posicao_card_y_w;

end if;

return	qt_posicao_w;

end obter_posicao_card_suep;
/