create or replace
function HD_obter_se_trat_perit (cd_pessoa_fisica_p	number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);
			
begin

select	decode(count(*),0,'N','S')
into	ie_retorno_w
from	paciente_tratamento
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	exists (select 1 from paciente_tratamento 
		where 	ie_tratamento IN ('CAPD','CPI','DPA','DPAV')
		and	cd_pessoa_fisica = cd_pessoa_fisica_p)
and	not exists (select 1 from paciente_tratamento 
		where 	ie_tratamento in ('CO','HD','CH','CHD','CHDF','CHDI','PAF','SCUF','SLED','PT','TX','TR','PL','DDC','IHF','IHDF')
		and	cd_pessoa_fisica = cd_pessoa_fisica_p);

return	ie_retorno_w;

end HD_obter_se_trat_perit;
/
