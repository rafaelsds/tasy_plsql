create or replace
function pls_obter_incosistencia_lib
			(	cd_inconsistencia_p	number)
			return varchar2 is

ds_retorno_w			varchar2(10);
qt_registros_w			number(10);
nr_seq_inconsistencia_w		number(10);

begin

begin
select	decode(ie_situacao,'A','S','I','N'),
	nr_sequencia
into	ds_retorno_w,
	nr_seq_inconsistencia_w
from	sib_inconsistencia
where	cd_inconsistencia	= cd_inconsistencia_p;
exception
when others then
	ds_retorno_w	:= 'N';
	nr_seq_inconsistencia_w	:= null;
end;

if	(ds_retorno_w = 'S') then
	select	count(1)
	into	qt_registros_w
	from	pls_regra_sib_inconsist
	where	nr_seq_inconsistencia	= nr_seq_inconsistencia_w;
	
	if	(qt_registros_w	> 0) then
		if	(nvl(obter_perfil_ativo,0) = 0) then /* Se for pelo portal deve consistir, por�m n�o tem perfil */
			return 'S';
		else
			select	count(1)
			into	qt_registros_w
			from	pls_regra_sib_inconsist
			where	nr_seq_inconsistencia	= nr_seq_inconsistencia_w
			and	cd_perfil		= obter_perfil_ativo;
		end if;
		
		if	(qt_registros_w = 0) then
			return 'N';
		else
			return 'S';
		end if;
	end if;
	return	'S';
end if;

return	ds_retorno_w;

end pls_obter_incosistencia_lib;
/