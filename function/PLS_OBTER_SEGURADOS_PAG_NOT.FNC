create or replace
function pls_obter_segurados_pag_not (	nr_seq_pagador_p		pls_notificacao_pagador.nr_seq_pagador%type,
					nr_seq_notific_pagador_p	pls_notificacao_pagador.nr_sequencia%type)
 		    	return varchar2 is

ds_retorno_w	varchar2(4000);

			
Cursor C01 is
	select	substr(obter_nome_pf(e.cd_pessoa_fisica),1,100) nm_segurado,
		substr(pls_obter_carteira_segurado(e.nr_sequencia),1,255) nr_carteirinha
	from	pls_notificacao_pagador a,
		pls_notificacao_item 	b,
		pls_mensalidade 	c,
		pls_mensalidade_segurado d,
		pls_segurado e
	where	a.nr_sequencia 		= b.nr_seq_notific_pagador
	and	b.nr_seq_mensalidade	= c.nr_sequencia
	and	c.nr_sequencia		= d.nr_seq_mensalidade
	and	d.nr_seq_segurado	= e.nr_sequencia
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	a.nr_sequencia		= nr_seq_notific_pagador_p
	group by
		e.cd_pessoa_fisica,
		e.nr_sequencia
	order by	nm_segurado;
			
			
begin

ds_retorno_w := '';
	
for r_c01_w in C01 loop

	ds_retorno_w := ds_retorno_w || r_c01_w.nm_segurado || ' C�d. do usu�rio: '||r_c01_w.nr_carteirinha||pls_util_pck.enter_w; 		
	
end loop;

return ds_retorno_w	;

end pls_obter_segurados_pag_not;
/