create or replace
function	cm_obter_se_conj_em_transf(
		nr_seq_conjunto_p			number)
			return varchar2 is

ie_retorno_w		varchar2(1);
qt_existe_w		number(10);

begin

select	count(*)
into	qt_existe_w
from	cm_lote_transf_conj a,
	cm_lote_transferencia b
where	a.nr_seq_lote_transf = b.nr_sequencia
and	a.nr_seq_conjunto = nr_seq_conjunto_p
and	b.dt_baixa is null;

if	(qt_existe_w > 0) then
	ie_retorno_w	:= 'S';
else
	ie_retorno_w	:= 'N';
end if;

return	ie_retorno_w;

end cm_obter_se_conj_em_transf;
/