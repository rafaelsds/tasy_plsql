create or replace 
function HD_Obter_Turno_PRC (	cd_pessoa_fisica_p	varchar2,
				ie_opcao_p		varchar2)
				return			varchar2 is
-- C - C�digo
-- D - Descri��o
-- G - Descri��o (Recep��o)

nr_seq_turno_w		number(10);
ds_retorno_w		varchar2(255);
dt_inicio_w		date;
ds_turno_w		varchar2(255);
ie_dias_escala_w	varchar2(1);

Cursor C01 is
	select	distinct substr(obter_descricao_padrao('HD_TURNO','DS_TURNO',d.NR_SEQ_TURNO),1,80)
	from	hd_escala_dialise a,
		hd_escala_dialise_dia d
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and		d.dt_inicio_escala_dia <= sysdate	
	and		a.nr_sequencia = d.nr_seq_escala
	and		d.dt_fim_escala_dia is null
	order by 1;
begin
ds_retorno_w	:= '';
Obter_Param_Usuario(7009 ,114,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_dias_escala_w);

if 	(ie_dias_escala_w = 'N') and
	(ie_opcao_p = 'G') then
	open C01;
	loop
	fetch C01 into	
		ds_turno_w;
	exit when C01%notfound;
		begin		
		ds_retorno_w := ds_retorno_w ||' - '|| ds_turno_w;			
		end;
	end loop;
	close C01;
	ds_retorno_w := substr(ds_retorno_w,4,255);
else
				
	select	max(nr_seq_turno)
	into	nr_seq_turno_w
	from	hd_escala_dialise
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	dt_inicio <= sysdate
	and	dt_fim is null;
end if;
	
if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= nr_seq_turno_w;
elsif	(ie_opcao_p = 'D') or
	((ie_opcao_p = 'G') and 
	(ie_dias_escala_w = 'S'))then	
	select	substr(ds_turno,1,200)
	into	ds_retorno_w
	from	hd_turno
	where	nr_sequencia	= nr_seq_turno_w;		
	
end if;

return ds_retorno_w;

end HD_Obter_Turno_PRC;
/