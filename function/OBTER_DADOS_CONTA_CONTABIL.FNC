create or replace
function obter_dados_conta_contabil(	cd_conta_contabil_p	varchar2,
				cd_estabelecimento_p	number,
				ie_resultado_p		varchar2)
				return varchar2 is

/*ie_resultado_p
CR = Criterio Rateio
CC = Centro Custo
CL = Classificacao
T  = Tipo (Analitica ou T�tulo)
S  = Situacao
DC = Debito/Credito
TG = Tipo do grupo da conta (Resultado, Ativo, Passivo  dominio 1143)
SC = C�digo sistema cont�bil
G = C�digo do grupo da conta
*/

nr_seq_crit_conta_w	varchar2(10);
nr_seq_crit_rateio_w	varchar2(10);
cd_centro_custo_conta_w	varchar2(8);
cd_centro_custo_w		varchar2(8);
vl_resultado_w		varchar2(255);
cd_classificacao_w		varchar2(40);
ie_tipo_w			varchar2(01);
ie_tipo_grupo_w		varchar2(01);
ie_situacao_w		varchar2(01);
cd_grupo_w		number(10);
ie_debito_credito_w		varchar2(1);
cd_sistema_contabil_w	varchar2(255);

begin

select	max(nr_seq_crit_rateio),
	max(cd_centro_custo),
	max(cd_classificacao),
	max(ie_tipo),
	max(ie_situacao),
	max(cd_grupo),
	max(cd_sistema_contabil)
into	nr_seq_crit_conta_w,
	cd_centro_custo_conta_w,
	cd_classificacao_w,
	ie_tipo_w,
	ie_situacao_w,
	cd_grupo_w,
	cd_sistema_contabil_w
from	conta_contabil
where	cd_conta_contabil = cd_conta_contabil_p;

if	(cd_grupo_w > 0) then
	select	nvl(max(ie_tipo),'')
	into	ie_tipo_grupo_w
	from	ctb_grupo_conta
	where	cd_grupo = cd_grupo_w;
end if;	

select	nvl(max(nr_seq_crit_rateio), nr_seq_crit_conta_w),
	nvl(max(cd_centro_custo), cd_centro_custo_conta_w)
into	nr_seq_crit_rateio_w,
	cd_centro_custo_w
from	conta_contabil_estab
where	cd_conta_contabil = cd_conta_contabil_p
and	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_resultado_p = 'CR') then
	vl_resultado_w	:= nr_seq_crit_rateio_w;
elsif	(ie_resultado_p = 'CC') then
	vl_resultado_w	:= cd_centro_custo_w;
elsif	(ie_resultado_p = 'CL') then
	vl_resultado_w	:= substr(ctb_obter_classif_conta(cd_conta_contabil_p, cd_classificacao_w, null),1,40);
elsif	(ie_resultado_p = 'T') then
	vl_resultado_w	:= ie_tipo_w;
elsif	(ie_resultado_p = 'S') then
	vl_resultado_w	:= ie_situacao_w;
elsif	(ie_resultado_p = 'DC') and (cd_grupo_w > 0) then
	select	ie_debito_credito
	into	vl_resultado_w
	from	ctb_grupo_conta
	where	cd_grupo = cd_grupo_w;
elsif	(ie_resultado_p = 'TG') then
	vl_resultado_w	:= ie_tipo_grupo_w;
elsif	(ie_resultado_p = 'SC') then
	vl_resultado_w	:= cd_sistema_contabil_w;
elsif	(ie_resultado_p = 'G') then
	vl_resultado_w	:= nvl(cd_grupo_w,0);
end if;

return vl_resultado_w;

end obter_dados_conta_contabil;
/
