create or replace
function Obter_desc_subtipo_protocolo ( cd_protocolo_p		number,
					nr_seq_protocolo_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);		
			
begin

select	nm_medicacao
into	ds_retorno_w
from   	protocolo_medicacao
where   ie_situacao = 'A'
and     cd_protocolo = cd_protocolo_p
and	nr_sequencia = nr_seq_protocolo_p
order by 1;

return	ds_retorno_w;

end Obter_desc_subtipo_protocolo;
/