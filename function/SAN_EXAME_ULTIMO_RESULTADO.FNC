create or replace
function san_exame_ultimo_resultado(
			nr_sequencia_p		number,
			ie_tipo_lote_p		varchar2,
			ie_tipo_exame_p		varchar2)
 		    	return varchar2 is

/*
IE_TIPO_LOTE_P
'D' - Exames de Doa��o
'R' - Exames da Reserva
'T' - Exames da Transfus�o

IE_TIPO_EXAME_P
'ABO' -  Tipagem sangu�nea, sistema ABO
'RH' - Fator Rh
'PAI' - Pesquisa de Anticorpos Irregulares
'HS' - Hemoglobina S
*/
ds_retorno_w		varchar2(255);
nr_seq_exame_lote_w	san_exame_lote.nr_sequencia%type;
nr_seq_exame_w		san_exame.nr_sequencia%type;

begin

	select	max(nr_sequencia)
	into	nr_seq_exame_lote_w
	from	(select	max(nr_sequencia) nr_sequencia
		from	san_exame_lote
		where	ie_tipo_lote_p	= 'D'
		and	nr_seq_doacao	= nr_sequencia_p
		union
		select	max(nr_sequencia) nr_sequencia
		from	san_exame_lote
		where	ie_tipo_lote_p	= 'R'
		and	nr_seq_reserva	= nr_sequencia_p
		union
		select	max(nr_sequencia) nr_sequencia
		from	san_exame_lote
		where	ie_tipo_lote_p	  = 'T'
		and	nr_seq_transfusao = nr_sequencia_p);
		
	if	(nr_seq_exame_lote_w is not null) then
	
		select	max(nr_sequencia)
		into	nr_seq_exame_w
		from	(select	max(nr_sequencia) nr_sequencia
			from	san_exame
			where	ie_tipo_exame_p = 'ABO'
			and	ie_tipo_sangue	= 'S'
			and	ie_situacao	= 'A'
			union
			select	max(nr_sequencia) nr_sequencia
			from	san_exame
			where	ie_tipo_exame_p = 'RH'
			and	ie_fator_rh	= 'S'
			and	ie_situacao	= 'A'
			union
			select	max(nr_sequencia) nr_sequencia
			from	san_exame
			where	ie_tipo_exame_p = 'PAI'
			and	ie_tipo_exame	= '2'
			and	ie_situacao	= 'A'
			union
			select	max(nr_sequencia) nr_sequencia
			from	san_exame
			where	ie_tipo_exame_p = 'HS'
			and	ie_tipo_exame	= '8'
			and	ie_situacao	= 'A');
		
		if	(nr_seq_exame_w is not null) then

			select 	nvl(decode(b.ie_resultado,'N', to_char(a.vl_resultado), nvl(decode(c.cd_exp_valor_dominio, null, a.ds_resultado, obter_desc_expressao(c.cd_exp_valor_dominio)), a.ds_resultado)), a.ie_resultado)
			into	ds_retorno_w
			from	san_exame b
			left join san_exame_realizado a 
				on  a.nr_seq_exame		= b.nr_sequencia
			left join valor_dominio c 
				on  	b.cd_tipo_resultado 	= c.cd_dominio 
				and 	Upper(a.ds_resultado)	= Upper(c.ds_valor_dominio)
				and 	c.ie_situacao 		= 'A'
			where 	a.nr_seq_exame_lote	= nr_seq_exame_lote_w
			and 	a.nr_seq_exame		= nr_seq_exame_w
			and	a.dt_liberacao is not null;
		
		end if;
		
	end if;

return	ds_retorno_w;

end san_exame_ultimo_resultado;
/