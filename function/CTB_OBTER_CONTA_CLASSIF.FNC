create or replace 
function ctb_obter_conta_classif
			(	cd_classificacao_atual_p	varchar2,
				dt_referencia_p			date,
				cd_estabelecimento_p		number)
				return varchar2 is

cd_conta_contabil_w 	varchar2(20);
cd_empresa_w		empresa.cd_empresa%type;
				
begin

select	max(a.cd_empresa)
into	cd_empresa_w
from	estabelecimento a
where	a.cd_estabelecimento = cd_estabelecimento_p;

select	max(a.cd_conta_contabil)
into	cd_conta_contabil_w
from	conta_contabil a
where	a.cd_classificacao_atual	= cd_classificacao_atual_p
and	a.ie_tipo			= 'A'
and	a.cd_empresa			= cd_empresa_w
and	dt_referencia_p between nvl(a.dt_inicio_vigencia,dt_referencia_p) and nvl(a.dt_fim_vigencia,dt_referencia_p);

return	cd_conta_contabil_w;

end ctb_obter_conta_classif;
/