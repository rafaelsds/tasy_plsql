create or replace 
function obter_des_status_sepse(cd_valor_p varchar2, ie_opcao_p varchar2 default 'D') return varchar2 is

ds_descricao_w 		varchar2(255) := '';
ie_versao_sepse_w   varchar2(10);

/*
ie_opcao_p
D - Descri��o
C - C�digo
*/
										 
begin

select nvl(ie_versao_sepse,'1')
into   ie_versao_sepse_w
from   parametro_medico
where  cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_opcao_p = 'C') then

	if (ie_versao_sepse_w = '2') then
		if (cd_valor_p in ('SC', 'RE')) then
			ds_descricao_w := 'SC,RE';
		elsif (cd_valor_p in ('D','RD','RF','SN')) then
			ds_descricao_w := 'D,RD,RF,SN';
		else 
			ds_descricao_w := cd_valor_p;
		end if;
	else
		ds_descricao_w := cd_valor_p;
	end if;

else

	if (ie_versao_sepse_w = '2') then
		if (cd_valor_p = 'SM') then
			ds_descricao_w := substr(obter_desc_expressao(881624,'Suspeita de infec��o sem disfun��o aguardando equipe'),1,255);
		elsif (cd_valor_p = 'SA') then
			ds_descricao_w := substr(obter_desc_expressao(881626,'Suspeita de infec��o sem disfun��o aguardando a��o m�dica'),1,255);
		elsif (cd_valor_p = 'RN') then
			ds_descricao_w := substr(obter_desc_expressao(301022,'Suspeita de Sepse pendente a��o m�dica'),1,255);
		elsif (cd_valor_p = 'RB') then
			ds_descricao_w := substr(obter_desc_expressao(881628,'Suspeita de Sepse aguardando a��o da equipe'),1,255);
		elsif (cd_valor_p in ('SC', 'RE')) then
			ds_descricao_w := substr(obter_desc_expressao(881630,'Infec��o sem Disfun��o Org�nica'),1,255);
		elsif (cd_valor_p = 'RC') then
			ds_descricao_w := substr(obter_desc_expressao(881826,'Sepse confirmada'),1,255);
		elsif (cd_valor_p in ('D','RD','RF','SN')) then
			ds_descricao_w := substr(obter_desc_expressao(781086,'Sepse descartada'),1,255);
		else 
			ds_descricao_w := substr(obter_valor_dominio(7056,cd_valor_p),1,255);
		end if;
	else 
		ds_descricao_w := substr(obter_valor_dominio(7056,cd_valor_p),1,255);
	end if;
	
end if;

return ds_descricao_w;

end;
/
