create or replace
Function pls_obter_dados_execucao(	nr_seq_execucao_p	number,
					ie_tipo_p		Varchar2) 
					return Varchar2 is
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter dados da execu��o da requisi��o
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	IE_TIPO_P
	
	NP - Nome do Prestador executor
*/

ds_retorno_w		varchar2(255);

begin
if	(nr_seq_execucao_p	is not null) then
	if	(ie_tipo_p	= 'NP') then
		begin
			select	substr(pls_obter_dados_prestador(nr_seq_prestador, 'N'),1,255)
			into	ds_retorno_w
			from	pls_lote_execucao_req
			where	nr_sequencia	= (	select	max(nr_seq_lote_exec)
							from	pls_execucao_requisicao
							where	nr_sequencia = nr_seq_execucao_p);
		exception
		when others then
			ds_retorno_w	:= '';
		end;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_dados_execucao;
/