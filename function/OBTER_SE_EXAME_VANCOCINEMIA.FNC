create or replace
function Obter_se_exame_vancocinemia(	nr_seq_exame_p		number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1) := 'N';
begin

if	(nr_seq_exame_p is not null) then
	select	nvl(max(ie_vancocinemia), 'N')
	into	ie_retorno_w
	from	exame_laboratorio
	where	nr_seq_exame = nr_seq_exame_p;
end if;

return ie_retorno_w;

end Obter_se_exame_vancocinemia;
/