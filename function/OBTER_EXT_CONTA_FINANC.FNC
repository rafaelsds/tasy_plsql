CREATE OR REPLACE FUNCTION OBTER_EXT_CONTA_FINANC (CD_CONTA_FINANC_P VARCHAR2)
			RETURN VARCHAR2 IS

DS_RESULTADO_W			VARCHAR2(100);

BEGIN

IF (CD_CONTA_FINANC_P IS NOT NULL) THEN

	select	max(a.CD_SISTEMA_EXTERNO)
	into	DS_RESULTADO_W
	from	conta_financeira a
	where	a.cd_conta_financ	= cd_conta_financ_p;
	
END IF;

RETURN DS_RESULTADO_W;
END OBTER_EXT_CONTA_FINANC;
/
