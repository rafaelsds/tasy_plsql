create or replace
function obter_dados_receb_conv_mx(	nr_titulo_p		number,
					nr_seq_baixa_p		number,
					nr_bordero_p		number,
					ie_opcao_p		varchar2)
					return varchar2 is
						
ds_retorno_w   		varchar2(255);
cd_pessoa_fisica_w      pessoa_fisica.cd_pessoa_fisica%type;
cd_cgc_w                pessoa_juridica.cd_cgc%type;
nr_seq_pais_w           pais.nr_sequencia%type;
ds_banco_w              banco.ds_banco%type;
cd_cnpj_agencia_w 	pessoa_juridica.cd_cgc%type;
sg_pais_w               pais.sg_pais%type;

nr_seq_receb_w		bordero_tit_rec.nr_seq_receb%type;
nr_seq_conta_banco_w	convenio_receb.nr_seq_conta_banco%type;
nr_seq_retorno_w	convenio_ret_receb.nr_seq_retorno%type;
cd_banco_w		banco_estabelecimento.cd_banco%type;
cd_agencia_bancaria_w	banco_estabelecimento.cd_agencia_bancaria%type;
cd_cgc_agencia_w	agencia_bancaria.cd_cgc_agencia%type;

begin

select 	max(a.cd_cgc),
	max(a.cd_pessoa_fisica)
into	cd_cgc_w,
	cd_pessoa_fisica_w
from	titulo_receber a,
	titulo_receber_liq b
where	a.nr_titulo = b.nr_titulo
and	a.nr_titulo = nr_titulo_p;
		
	if (ie_opcao_p = 'RFCPE') then	--RFC emissor da conta de pagamento
	
		if (cd_cgc_w is not null) then 	
				
			select	max(a.cd_cnpj_agencia)
			into	cd_cnpj_agencia_w
			from	pessoa_juridica_conta a 
			where	a.cd_cgc = cd_cgc_w
			and	a.ie_conta_pagamento = 'N';
			
			if 	(nvl(cd_cnpj_agencia_w,'X') <> 'X') then
			
				select	max(cd_rfc)
				into	ds_retorno_w
				from 	pessoa_juridica
				where	cd_cgc = cd_cnpj_agencia_w;
		
			end if;	
	
		elsif (cd_pessoa_fisica_w is not null) then
		
			select	max(a.cd_cnpj_agencia)
			into	cd_cnpj_agencia_w
			from	pessoa_fisica_conta a 
			where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
			and	a.ie_conta_pagamento = 'N';			
			
			if 	(nvl(cd_cnpj_agencia_w,'X') <> 'X') then
			
				select	max(cd_rfc)
				into	ds_retorno_w
				from 	pessoa_juridica
				where	cd_cgc = cd_cnpj_agencia_w;
				
			end if;	
		end if;
		
	elsif	(ie_opcao_p = 'CPE') then	--Conta de Pagamento emissor
		
		if (cd_cgc_w is not null) then
			
			select	max(b.nr_conta)
			into	ds_retorno_w
			from	pessoa_juridica a,
				pessoa_juridica_conta b,
				titulo_receber c,
				titulo_receber_liq d
			where   a.cd_cgc = b.cd_cgc
			and     a.cd_cgc = c.cd_cgc
			and     c.nr_titulo = d.nr_titulo
			and     c.nr_titulo = nr_titulo_p
			and     d.nr_sequencia = nr_seq_baixa_p
			and	b.ie_conta_pagamento = 'N';
					
		elsif (cd_pessoa_fisica_w is not null)	then		

			select	max(b.nr_conta)
			into	ds_retorno_w
			from	pessoa_fisica a,
				pessoa_fisica_conta b,
				titulo_receber c,
				titulo_receber_liq d
			where   a.cd_pessoa_fisica = b.cd_pessoa_fisica
			and     a.cd_pessoa_fisica = c.cd_pessoa_fisica
			and     c.nr_titulo = d.nr_titulo
			and     c.nr_titulo = nr_titulo_p
			and     d.nr_sequencia = nr_seq_baixa_p
			and	b.ie_conta_pagamento = 'N';
					
		end if;
		
	elsif	(ie_opcao_p = 'RFCPB') then	--RFC da conta beneficiaria
	
		select 	nr_seq_receb
		into	nr_seq_receb_w
		from 	bordero_tit_rec
		where	nr_bordero = nr_bordero_p
		and	nr_titulo = nr_titulo_p;
		
		select 	nr_seq_conta_banco
		into 	nr_seq_conta_banco_w
		from	convenio_receb
		where	nr_sequencia = nr_seq_receb_w;
		
		select 	cd_banco,
			cd_agencia_bancaria
		into	cd_banco_w,
			cd_agencia_bancaria_w
		from	banco_estabelecimento
		where 	nr_sequencia = nr_seq_conta_banco_w;
		
		select 	cd_cgc_agencia
		into	cd_cgc_agencia_w
		from	agencia_bancaria
		where	cd_banco = cd_banco_w
		and	cd_agencia_bancaria = cd_agencia_bancaria_w;
				
		select 	cd_rfc
		into 	ds_retorno_w
		from 	pessoa_juridica 
		where 	cd_cgc = cd_cgc_agencia_w;

	elsif	(ie_opcao_p = 'CPB') then	--Conta de pagamento beneficiaria
		
		select 	nr_seq_receb
		into	nr_seq_receb_w
		from 	bordero_tit_rec
		where	nr_bordero = nr_bordero_p
		and	nr_titulo = nr_titulo_p;
					
		select 	nr_seq_conta_banco
		into 	nr_seq_conta_banco_w
		from	convenio_receb
		where	nr_sequencia = nr_seq_receb_w;
		
		select 	cd_conta
		into	ds_retorno_w
		from	banco_estabelecimento
		where 	nr_sequencia = nr_seq_conta_banco_w;	
		
	end if;	
	
	return	ds_retorno_w;

end obter_dados_receb_conv_mx;
/
