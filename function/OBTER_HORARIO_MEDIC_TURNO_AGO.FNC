create or replace
function obter_horario_medic_turno_ago(	
				nr_prescricao_p		number,
				nr_sequencia_p		number,
				nr_seq_turno_p		number,
				qt_min_agora_p		number)
				return varchar2 is

ds_horario_w		varchar2(15);
ds_resultado_w		varchar2(2000);

Cursor C01 is
select	b.ds_horario
from	prescr_mat_hor b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	a.nr_prescricao		= nr_prescricao_p
and	b.nr_seq_material	= nr_sequencia_p
and	b.nr_seq_turno		= nr_seq_turno_p
and	((b.ie_urgente		= 'S') or 
         (b.dt_horario <= nvl(a.dt_liberacao,a.dt_liberacao_medico) + qt_min_agora_p / 1440))
and	b.dt_suspensao is null
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
order by b.dt_horario;

begin

OPEN C01;
LOOP
FETCH C01 into
	ds_horario_w;
exit when c01%notfound;
	begin
	ds_resultado_w	:= ds_resultado_w ||' '||ds_horario_w;
	end;
END LOOP;
CLOSE C01;

return ds_resultado_w;

end obter_horario_medic_turno_ago;
/