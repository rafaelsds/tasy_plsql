create or replace function obter_nome_item_worklist
			(	nr_sequencia_p		number)
				return varchar2 is

ds_retorno_w		varchar2(100);

begin
if	(nr_sequencia_p is not null) then
	begin
	select DS_ITEM
	into	ds_retorno_w
	from	wl_item
	where	nr_sequencia = nr_sequencia_p;  
 
	end;
end if;

return ds_retorno_w;

end obter_nome_item_worklist;
/