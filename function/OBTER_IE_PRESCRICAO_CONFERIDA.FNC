create or replace
function obter_ie_prescricao_conferida(nr_prescricao_p	number)
			return varchar2 is

ds_retorno_w	hd_prescr_conferida.ie_conferido%type;

begin

select	nvl(max(ie_conferido), 'N')
into 	ds_retorno_w
from	hd_prescr_conferida c
where	c.nr_prescricao = nr_prescricao_p;

ds_retorno_w := nvl(ds_retorno_w, 'N');

return	ds_retorno_w;

end obter_ie_prescricao_conferida;
/