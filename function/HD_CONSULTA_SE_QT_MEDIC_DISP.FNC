create or replace
function hd_consulta_se_qt_medic_disp	(	ds_lista_param_p	varchar2,
						qt_mdedicamento_p	Number)
 		    	return varchar2 is
tam_lista_w		number(10);
ie_pos_virgula_w	number(3);
ie_pos_traco_w		number(10);
lista_inf_w		varchar2(100);
nr_seq_medic_w		number(20);
nulo_w			number(20);
nr_lote_w		number(20);
ds_retorno_w		Varchar2(1) := 'N';
qt_saldo_w		Number(10);
begin
if (ds_lista_param_p is not null) then 
	tam_lista_w		:= length(ds_lista_param_p);
	ie_pos_virgula_w	:= instr(ds_lista_param_p,',');
	ie_pos_traco_w		:= instr(ds_lista_param_p,'-');
	lista_inf_w		:= substr(ds_lista_param_p,1,ie_pos_virgula_w - 1);

	/* obter a sequ�ncia lida */
	if	(ie_pos_virgula_w <> 0) then
		begin
		nulo_w			:= substr(lista_inf_w,1,(ie_pos_traco_w - 1));
		lista_inf_w		:= substr(lista_inf_w,ie_pos_traco_w + 1,100);
		nr_lote_w		:= lista_inf_w;
		end;
	end if;

	if (nvl(nr_lote_w,0) > 0) then
		
		select  substr(hd_obter_qt_saldo_lote(nr_sequencia),1,255) 
		into	qt_saldo_w
		from	hd_lote_medic_ext
		where	nr_sequencia = nr_lote_w;
		
		if (qt_mdedicamento_p > qt_saldo_w) then
			ds_retorno_w := 'N';
		else
			ds_retorno_w := 'S';
		end if;
	end if;
end if;
return	ds_retorno_w;

end hd_consulta_se_qt_medic_disp;
/