create or replace
function Obter_se_exame_lib_exames(nr_atendimento_p  number)
							 return varchar2 IS
qt_exame_w				Number(10);
qt_exame_lib_w			Number(10);
ds_retorno_w			Varchar2(1);
ie_aprov_bioquimico_w	varchar2(3);
ie_status_execucao_w	varchar2(4)	:= '15';
ie_status_atend_w		varchar2(4)	:= '35';
qt_exame_laborat_w		Number(10);
qt_exame_lib_laborat_w	Number(10);
qt_geral_lib_w			Number(10);
qt_geral_w				Number(10);

/*

P - Exame liberado e pendente por fazer
S - Exame liberado e j� feito
N - N�o tem exame liberado para o paciente

*/

begin

-- Exames que N�O s�o de laborat�rio

select	count(*)
into	qt_exame_w
from	prescr_procedimento b,	
		prescr_medica a
where	a.nr_prescricao  = b.nr_prescricao
  and	a.nr_atendimento = nr_atendimento_p
  and	(Obter_se_exame_presc_externo(b.cd_procedimento,b.nr_seq_proc_interno) = 'N')
  and	b.nr_seq_exame is null
  and	(a.dt_liberacao is not null or a.dt_liberacao_medico is not null)
  and   nvl(b.ie_suspenso, 'N') = 'N'
  and   nvl(b.ie_exame_bloqueado,'N') = 'N';

select	count(*)
into	qt_exame_lib_w
from	prescr_procedimento b,	
		prescr_medica a
where	a.nr_prescricao  = b.nr_prescricao
  and	a.nr_atendimento = nr_atendimento_p
  and	b.nr_seq_exame is null
  and   (b.ie_status_execucao > ie_status_execucao_w)
  and	(Obter_se_exame_presc_externo(b.cd_procedimento,b.nr_seq_proc_interno) = 'N')
  and	(a.dt_liberacao is not null or a.dt_liberacao_medico is not null)
  and   nvl(b.ie_suspenso, 'N') = 'N'
  and   nvl(b.ie_exame_bloqueado,'N') = 'N';

-- Exames que N�O s�o de laborat�rio  
  
 -- Exames de laborat�rio 
select	max(nvl(vl_parametro,vl_parametro_padrao))
into	ie_aprov_bioquimico_w
from	funcao_parametro
where	cd_funcao		= 281
and	    nr_sequencia	= 176;

if	(ie_aprov_bioquimico_w = 'S') then
	ie_status_atend_w	:= 30;
end if;

select	count(*)
into	qt_exame_laborat_w
from	prescr_procedimento b,	
		prescr_medica a
where	a.nr_prescricao  = b.nr_prescricao
  and	a.nr_atendimento = nr_atendimento_p
  and	(Obter_se_exame_presc_externo(b.cd_procedimento,b.nr_seq_proc_interno) = 'N')
  and	b.nr_seq_exame is not null
  and	(a.dt_liberacao is not null or a.dt_liberacao_medico is not null)
  and   nvl(b.ie_suspenso, 'N') = 'N'
  and   nvl(b.ie_exame_bloqueado,'N') = 'N';

select	count(*)
into	qt_exame_lib_laborat_w
from	prescr_procedimento b,	
		prescr_medica a
where	a.nr_prescricao  = b.nr_prescricao
  and	(Obter_se_exame_presc_externo(b.cd_procedimento,b.nr_seq_proc_interno) = 'N')
  and	a.nr_atendimento = nr_atendimento_p
  and	b.ie_status_atend >= ie_status_atend_w
  and	b.nr_seq_exame is not null
  and	(a.dt_liberacao is not null or a.dt_liberacao_medico is not null)
  and   nvl(b.ie_suspenso, 'N') = 'N'
  and   nvl(b.ie_exame_bloqueado,'N') = 'N';

-- Exames de laborat�rio 
  
qt_geral_w := qt_exame_laborat_w +  qt_exame_w;
qt_geral_lib_w := qt_exame_lib_laborat_w + qt_exame_lib_w; 
	

if 	(qt_geral_lib_w < qt_geral_w ) and ( qt_geral_lib_w <> 0 ) then
	ds_retorno_w := 'P';
else
	if 	( qt_geral_lib_w = qt_geral_w) and  (qt_geral_w <> 0) then
		ds_retorno_w := 'S';
	else
		ds_retorno_w := 'N';
	end if;
end if;

return	ds_retorno_w;

end	Obter_se_exame_lib_exames;
/
