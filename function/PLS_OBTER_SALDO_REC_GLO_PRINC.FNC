create or replace
function pls_obter_saldo_rec_glo_princ(	nr_seq_conta_proc_p		pls_conta_proc.nr_sequencia%type,
					nr_seq_rec_glosa_proc_p		pls_rec_glosa_proc.nr_sequencia%type)
 		    	return number is	

/*
A ideia dessa function teve base sobre a ideia da function PLS_OBTER_SALDO_REC_GLOSA_PROC
por�m devido a quantidade de lugares onde a function em quest�o � utilizada, principalmente em campos
functions, foi criado essa function que ser� chamada na verifica��o de saldo do item na importa��o de XML
Foi feito os union all pois caso o item n�o possua nr_seq_proc_princ e nenhum item secund�rio ir� retornar
o valor na primeira parte do union all, caso seja um item que n�o possua nr_seq_proc_princ e possua itens
secund�rios ent�o ir� retornar valor tanto na primeira quanto an segunda parte
*/			
vl_saldo_w		number(15,2);
vl_glosa_w		pls_conta_proc.vl_glosa%type;
vl_acatado_w		pls_rec_glosa_proc.vl_acatado%type;

begin
select	sum(vl_glosa)
into	vl_glosa_w
from	(
	select	nvl(sum(vl_glosa), 0) vl_glosa
	from	pls_conta_proc
	where	nr_sequencia		= nr_seq_conta_proc_p
	union all
	select	nvl(sum(vl_glosa), 0) vl_glosa
	from	pls_conta_proc
	where	nr_seq_proc_princ	= nr_seq_conta_proc_p);

select	sum(vl_acatado)
into	vl_acatado_w
from	(
	select	nvl(sum(a.vl_acatado),0) vl_acatado
	from	pls_rec_glosa_proc a,
		pls_conta_proc b
	where	a.nr_seq_conta_proc = b.nr_sequencia
	and	b.nr_sequencia = nr_seq_conta_proc_p
	and	a.nr_sequencia <> nvl(nr_seq_rec_glosa_proc_p, -1)
	union all
	select	nvl(sum(a.vl_acatado),0) vl_acatado
	from	pls_rec_glosa_proc a,
		pls_conta_proc b
	where	a.nr_seq_conta_proc = b.nr_sequencia
	and	b.nr_seq_proc_princ = nr_seq_conta_proc_p
	and	a.nr_sequencia <> nvl(nr_seq_rec_glosa_proc_p, -1));	
	
vl_saldo_w	:= vl_glosa_w - vl_acatado_w;

return	vl_saldo_w;

end pls_obter_saldo_rec_glo_princ;
/
