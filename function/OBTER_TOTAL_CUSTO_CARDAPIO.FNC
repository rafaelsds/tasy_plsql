create or replace
function obter_total_custo_cardapio(
		nr_seq_cardapio_p	number)
		return number is

ie_considerar_pessoas_w	varchar2(1);
total_custo_cardapio_w	number(17,4);
		
begin
if	(nr_seq_cardapio_p is not null) then
	begin
	ie_considerar_pessoas_w := obter_valor_param_usuario(912, 12, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	select	decode(ie_considerar_pessoas_w,'S',sum(a.vl_custo /* * d.qt_pessoa_atend */ ),sum(a.vl_custo)) total_custo_cardapio
	into	total_custo_cardapio_w
	from	nut_rec_real_comp a,
		nut_cardapio_dia d,
		nut_cardapio c,
		nut_receita_real b
	where	b.nr_sequencia   	= a.nr_seq_rec_real
	and	c.nr_sequencia     	= b.nr_seq_cardapio
	and	d.nr_sequencia     	= c.nr_seq_card_dia
	and	b.nr_seq_cardapio	= nr_seq_cardapio_p;
	end;
end if;
return	total_custo_cardapio_w;

end obter_total_custo_cardapio;
/