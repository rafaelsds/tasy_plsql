 create or replace 
 function cpoe_is_material_order_type(cd_material_p material_order_type.cd_material%type,
                                     nr_seq_order_unit_p material_order_type.nr_seq_order_type%type,
                                     cd_estabelecimento_p material_order_type.cd_estabelecimento%type
                                     ) return  varchar is
                                         
 is_material     varchar(1) := 'N';
 
 begin
 
     select  max('S') 
     into    is_material
 	from	cpoe_order_unit a,
 			material_order_type b
     where   a.nr_sequencia = nr_seq_order_unit_p
 	and		a.NR_SEQ_CPOE_TIPO_PEDIDO = b.nr_seq_order_type
 	and		b.cd_material = cd_material_p
     and     nvl(b.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
 	and		b.IE_SITUACAO = 'A';
 
     return is_material;
 
 end cpoe_is_material_order_type;
 /
