create or replace
function obter_conversao_meio_ext_ipe(	cd_cgc_convenio_p	varchar2,
					nm_tabela_p		varchar2,
					nm_atributo_p		varchar2,
					cd_interno_p		varchar2)
				return varchar2 is
cd_externo_w	varchar2(40);
begin

begin
select	max(cd_externo)
into	cd_externo_w
from	conversao_meio_externo
where	cd_cgc 			= cd_cgc_convenio_p
and	upper(nm_tabela) 	= upper(nm_tabela_p)
and	upper(nm_atributo) 	= upper(nm_atributo_p)
and	cd_interno 		= cd_interno_p;
exception
when others then
	cd_externo_w := null;
end;

return	cd_externo_w;

end obter_conversao_meio_ext_ipe;
/