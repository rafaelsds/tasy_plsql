create or replace
function obter_se_servico_check_list(nr_seq_servico_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1);
ie_ignorar_checklist_w	varchar2(1);
			
begin

select 	decode(count(*),0,'N','S')
into	ds_retorno_w
from	sl_check_list_unid
where	nr_seq_sl_unid	= nr_seq_servico_p
and	nvl(obter_status_checklist_unid(nr_sequencia),'F') = 'F';

if	(ds_retorno_w = 'N') then
	
	select	nvl(max(ie_ignorar_checklist),'N')
	into	ie_ignorar_checklist_w
	from	unidade_atendimento a,
		sl_unid_atend b
	where	b.nr_seq_unidade = a.nr_seq_interno
	and	b.nr_sequencia	= nr_seq_servico_p;
	
	if	(ie_ignorar_checklist_w = 'S') then
		ds_retorno_w	:= 'S';
	end if;	
end if;
return	ds_retorno_w;

end obter_se_servico_check_list;
/
