create or replace
function Obter_Desc_Proc_CIH
		(cd_procedimento_p	number,
		nr_atendimento_p		number default 0)
		return varchar2 is

ds_procedimento_w	varchar2(255) 	:= '';
cd_convenio_w		number(10,0);
ie_tipo_atendimento_w	number(05,0);

begin

select 	max(ds_procedimento)
into	ds_procedimento_w
from	procedimento
where	cd_procedimento	= cd_procedimento_p
  and	ie_origem_proced	in (2,3,7);

return	ds_procedimento_w;

end Obter_Desc_Proc_CIH;
/
