create or replace
function obter_tipo_operacao_nota(cd_operacao_nf_p	number)
 		    	return varchar2 is
			
ds_retorno_w			varchar2(1);
ie_tipo_operacao_w		number(1);
ie_transf_w			varchar2(1);
ie_servico_w			varchar2(1);
ie_bonificacao_w		varchar2(1);
ie_devolucao_w			varchar2(1);

begin
if	(nvl(cd_operacao_nf_p,0) > 0) then
	select	ie_tipo_operacao,
		ie_transferencia_estab,
		ie_servico,
		ie_bonificacao,
		ie_devolucao
	into	ie_tipo_operacao_w,
		ie_transf_w,
		ie_servico_w,
		ie_bonificacao_w,
		ie_devolucao_w
	from	operacao_nota
	where	cd_operacao_nf = cd_operacao_nf_p
	order by 1,2,3,4,5;
	
	if (nvl(ie_tipo_operacao_w,0) = 1) then
		ds_retorno_w	:=  'C';
	elsif (ie_servico_w = 'S') then
		ds_retorno_w	:=  'S';
	elsif (ie_transf_w = 'S') then
		ds_retorno_w	:=  'T';
	elsif(ie_bonificacao_w = 'S') then
		ds_retorno_w	:= 'B';
	elsif(ie_devolucao_w = 'S') then
		ds_retorno_w	:= 'D';						
	end if;
end if;	

return	ds_retorno_w;

end obter_tipo_operacao_nota;
/
