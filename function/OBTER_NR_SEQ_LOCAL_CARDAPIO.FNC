create or replace
function Obter_nr_seq_local_cardapio(nr_seq_cardapio_p		number)
 		    	return number is
			
ds_retorno_w	number(10);

begin

if (nr_seq_cardapio_p is not null) then

SELECT		max(nr_seq_local)
into		ds_retorno_w
FROM		nut_cardapio_dia
WHERE		nr_sequencia = nr_seq_cardapio_p;

end if;

return	ds_retorno_w;

end Obter_nr_seq_local_cardapio;
/