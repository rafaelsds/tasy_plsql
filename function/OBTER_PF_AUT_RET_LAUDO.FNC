create or replace
function obter_pf_aut_ret_laudo (nr_atendimento_p	number)
 		    	return varchar2 is

nm_pessoa_fisica_w	varchar2(200);
lista_pessoas_w		varchar2(255);			

Cursor C01 is
	select	nm_pessoa_fisica
	from	atend_autor_entrega
	where	nr_atendimento = nr_atendimento_p
	order by nm_pessoa_fisica;
			
begin

open C01;
loop
fetch C01 into	
	nm_pessoa_fisica_w;
exit when C01%notfound;
	begin
	if	(lista_pessoas_w is not null) then
		lista_pessoas_w := substr(lista_pessoas_w || ', ' || nm_pessoa_fisica_w,1,255);
	else
		lista_pessoas_w := nm_pessoa_fisica_w;
	end if;
	end;
end loop;
close C01;

return	lista_pessoas_w;

end obter_pf_aut_ret_laudo;
/