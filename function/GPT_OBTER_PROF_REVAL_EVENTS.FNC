create or replace
function gpt_obter_prof_reval_events	(nr_seq_cpoe_p	number,
										ie_tipo_item_p	varchar2)
				return number is

cd_profissional_w				gpt_revalidation_events.cd_profissional%type;

begin

if (nr_seq_cpoe_p is not null and ie_tipo_item_p is not null) then

	select	max(cd_profissional)
	into	cd_profissional_w
	from	gpt_revalidation_events
	where	nr_sequencia = gpt_obter_seq_reval_events(nr_seq_cpoe_p, ie_tipo_item_p);

end if;

return cd_profissional_w;

end gpt_obter_prof_reval_events;
/