create or replace
function  lab_nr_repeticoes_exame(nr_prescricao_p		number,
			       nr_seq_prescr_p		number)
			       return number is

ds_contador_w			number;
			
begin

select	count(*)
into	ds_contador_w
from	PRESCR_PROC_EXAME_REPET 
where	nr_prescricao = nr_prescricao_p
and	nr_seq_prescr = nr_seq_prescr_p;

return	ds_contador_w;

end lab_nr_repeticoes_exame;
/