create or replace
function obter_grupo_pj_rel_cre(cd_cgc_p		varchar2)
								return varchar2 is

ds_retorno_w	varchar2(20);
/*OS1455684 - Function utilizada na aba Relat da fun��o Manute��o de T�tulos a Receber*/	
begin

if (nvl(cd_cgc_p,'X') <> 'X') then

	select	substr(max(a.cd_cnpj),1,20)
	into	ds_retorno_w
	from	pessoa_juridica_grupo a
	where	a.cd_cnpj_unidade = cd_cgc_p;

end if;

return	ds_retorno_w;

end obter_grupo_pj_rel_cre;
/