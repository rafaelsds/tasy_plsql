create or replace
function obter_obito_RN( 	dt_obito_p  date,
				dt_nascimento_p  date)
			return number is
qt_dias_w number(10) := 0;
begin

if 	(round((dt_obito_p - dt_nascimento_p)) <= 28) then
	qt_dias_w := 1;
end if;

return qt_dias_w;

end obter_obito_RN;
/