create or replace
function OBTER_SE_INTERV_KIT(	cd_kit_material_p	number,
				cd_material_p	number) return varchar2 is

ds_retorno_w	varchar2(1);
cd_intervalo_w	intervalo_prescricao.cd_intervalo%type;

begin

ds_retorno_w	:= 'N';

if	((cd_kit_material_p is not null) and (cd_material_p is not null)) then

	select	max(cd_intervalo)
	into	cd_intervalo_w
	from	componente_kit
	where	cd_kit_material	= cd_kit_material_p
	and	cd_material	= cd_material_p;

	if	(cd_intervalo_w is not null) then
		ds_retorno_w := 'S';
	end if;

end if;

return ds_retorno_w;

end	OBTER_SE_INTERV_KIT;
/