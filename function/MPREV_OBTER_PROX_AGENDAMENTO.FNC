create or replace
function mprev_obter_prox_agendamento	(nr_seq_participante_p	number,
					ie_opcao_p		varchar2,
					nr_linha_p		number,
					dt_referencia_p		date default sysdate)
							return varchar2 is
ds_retorno_w		varchar2(255)	:= null;
dt_prevista_w		date;
dt_agendamento_w	date;
i			number(10);
ie_forma_atend_prev_w	mprev_partic_ciclo_item_v.ie_forma_atend_prev%type;
nm_grupo_w		mprev_agendamento_turma_v.nm_grupo%type;
nm_turma_w		mprev_agendamento_turma_v.nm_turma%type;

/*				
ie_opcao_p					
'PA' - Data prevista para atendimento (conforme plano)
'AA' - Data agendada para atendimento
'T' - Telemonitoramento
'G' - Reuni�o em Grupo
*/

Cursor c_atend_prev is
	select	a.dt_prevista,
		a.dt_agendamento
	from	mprev_partic_ciclo_item_v a
	where	a.nr_seq_participante = nr_seq_participante_p
	and	a.dt_prevista >= dt_referencia_p 
	and	(a.ie_status <> 'C' or a.ie_status is null)
	and	a.ie_forma_atend_prev = ie_forma_atend_prev_w
	order by
		a.dt_prevista;
		
Cursor c_agenda_grupo is
	select	a.dt_agenda,
		a.nm_grupo,
		a.nm_turma
	from	mprev_agendamento_turma_v a
	where	a.nr_seq_participante = nr_seq_participante_p
	and	a.dt_agenda >= dt_referencia_p 
	and	(a.ie_status_agenda <> 'C' or a.ie_status_agenda is null)
	order by
		a.dt_agenda;
		
begin

if 	(nr_seq_participante_p is not null) and 
	(ie_opcao_p is not null) and 
	(nr_linha_p is not null) then

	if	(ie_opcao_p in ('PA','AA')) then
		/* Buscar atendimentos previstos presenciais */
		ie_forma_atend_prev_w	:= 'P';
		i	:= 1;
		open c_atend_prev;
		loop
		fetch c_atend_prev into	
			dt_prevista_w,
			dt_agendamento_w;
		exit when c_atend_prev%notfound;
			begin
			if	(i = nr_linha_p) then
				if	(ie_opcao_p = 'PA') then
					ds_retorno_w	:= to_char(dt_prevista_w,'dd/mm/yy');
				elsif	(ie_opcao_p = 'AA') then
					ds_retorno_w	:= to_char(dt_agendamento_w,'dd/mm/yy hh24:mi');
				end if;
			end if;
			i	:= i + 1;
			end;
		end loop;
		close c_atend_prev;
	elsif	(ie_opcao_p = 'T') then
		/* Buscar atendimentos previstos por telefone */
		ie_forma_atend_prev_w	:= 'T';
		i	:= 1;
		open c_atend_prev;
		loop
		fetch c_atend_prev into	
			dt_prevista_w,
			dt_agendamento_w;
		exit when c_atend_prev%notfound;
			begin
			if	(i = nr_linha_p) then
				/* No caso da tele, n�o existe agendamento ser� uma lista apenas */
				ds_retorno_w	:= to_char(dt_prevista_w,'mm/yyyy');
			end if;
			i	:= i + 1;
			end;
		end loop;
		close c_atend_prev;		
	elsif	(ie_opcao_p = 'G') then
		i	:= 1;
		open c_agenda_grupo;
		loop
		fetch c_agenda_grupo into	
			dt_agendamento_w,
			nm_grupo_w,
			nm_turma_w;
		exit when c_agenda_grupo%notfound;
			begin
			if	(i = nr_linha_p) then
				ds_retorno_w	:= 	initcap(substr(to_char(dt_agendamento_w,'day'),1,3)) || ' ' ||
							to_char(dt_agendamento_w,'dd/mm/yy') || ' - ' || 
							substr(nm_grupo_w,1,60) || ', ' ||
							substr(nm_turma_w,1,30);
			end if;
			i	:= i + 1;
			end;
		end loop;
		close c_agenda_grupo;
	end if;
end if;

return	ds_retorno_w;

end mprev_obter_prox_agendamento;
/