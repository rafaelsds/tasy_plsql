create or replace
function obter_tipo_interdicao (cd_motivo_interdicao_p	number)
				return varchar2 is

ie_ajuste_acomodacao_w		varchar2(1);

begin

select	nvl(max(ie_ajuste_acomodacao),'N')
into	ie_ajuste_acomodacao_w
from	motivo_interdicao_leito
where	cd_motivo	= cd_motivo_interdicao_p;

return ie_ajuste_acomodacao_w;

end obter_tipo_interdicao;
/