create or replace
function san_obter_motivo_troca (cd_motivo_troca_p	number)
 		    	return varchar2 is

ds_resultado_w		varchar2(255);
			
begin

select	max(ds_motivo)
into	ds_resultado_w
from	san_mot_troca_hemo_solic
where	nr_sequencia = cd_motivo_troca_p;

return	ds_resultado_w;

end san_obter_motivo_troca;
/