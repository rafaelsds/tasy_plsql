create or replace
function obter_se_pessoa_terceiro(	cd_pessoa_fisica_p	varchar2,
					nr_seq_terceiro_p	number )
					return varchar2 is

ds_retorno_w	varchar2(1);
					
begin

select	nvl(max('S'),'N')
into	ds_retorno_w
from	terceiro_pessoa_fisica
where	cd_pessoa_fisica	=	cd_pessoa_fisica_p
and	nr_seq_terceiro		=	nr_seq_terceiro_p;

return	ds_retorno_w;

end obter_se_pessoa_terceiro;
/