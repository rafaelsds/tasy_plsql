create or replace 
function obter_desc_motivo_reserva (	nr_seq_motivo_p	number,
					ie_opcao_p	varchar2)
					return varchar2 is

ds_motivo_w	varchar2(255);
ds_retorno_w	varchar2(255);

begin

if (nvl(nr_seq_motivo_p,0) > 0) then

	select 	ds_motivo
	into	ds_motivo_w
	from	motivo_reserva_unidade
	where	nr_sequencia = nr_seq_motivo_p;

end if;

if (ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_motivo_w;
end if;

return ds_retorno_w;

end obter_desc_motivo_reserva;
/