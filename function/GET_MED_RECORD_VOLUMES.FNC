create or replace
function get_med_record_volumes	(nr_sequencia_p in varchar2)
				return varchar2 is

ds_volume_w varchar2(255)	:= '';
ds_retorno_w varchar2(255)	:= '';

cursor c01 is
	select 	nr_seq_volume
	from 	same_prontuario
	where 	nr_sequencia in
		(select nr_seq_prontuario
		from 	same_solic_pront_envelope
		where 	nr_seq_solic = nr_sequencia_p
			and nr_seq_prontuario is not null)
	and nr_seq_volume is not null
  and ROWNUM < 25;		

begin

open c01;
loop
fetch c01 into
	ds_volume_w;
exit when c01%notfound;
	begin
	ds_retorno_w := ds_retorno_w || ds_volume_w || ', ';
	end;
end loop;
close c01;

ds_retorno_w := substr(ds_retorno_w,1,length(ds_retorno_w)-2);

return	ds_retorno_w;
end	get_med_record_volumes;
/