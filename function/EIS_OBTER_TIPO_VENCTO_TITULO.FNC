create or replace
function	EIS_OBTER_TIPO_VENCTO_TITULO (	nr_titulo_p		number,
						dt_referencia_p		date) return number is

ie_tipo_vencimento_w		number(10);
dt_pagamento_previsto_w		date;
dt_parametro_w			date;

begin

select	dt_pagamento_previsto
into	dt_pagamento_previsto_w
from 	titulo_receber_v
where	nr_titulo	= nr_titulo_p;

dt_parametro_w	:= trunc(dt_referencia_p, 'dd');

if	(dt_pagamento_previsto_w >= dt_parametro_w + 90) then
	ie_tipo_vencimento_w	:= 5;
elsif	(dt_pagamento_previsto_w >= dt_parametro_w + 60) then
	ie_tipo_vencimento_w	:= 4;
elsif	(dt_pagamento_previsto_w >= dt_parametro_w + 30) then
	ie_tipo_vencimento_w	:= 3;
elsif	(dt_pagamento_previsto_w >= dt_parametro_w + 15) then
	ie_tipo_vencimento_w	:= 2;
elsif	(dt_pagamento_previsto_w >= dt_parametro_w) then
	ie_tipo_vencimento_w	:= 1;
elsif	(dt_pagamento_previsto_w >= dt_parametro_w - 15) then
	ie_tipo_vencimento_w	:= 6;
elsif	(dt_pagamento_previsto_w >= dt_parametro_w- 30) then
	ie_tipo_vencimento_w	:= 7;
elsif	(dt_pagamento_previsto_w >= dt_parametro_w- 60) then
	ie_tipo_vencimento_w	:= 8;
elsif	(dt_pagamento_previsto_w >= dt_parametro_w - 90) then
	ie_tipo_vencimento_w	:= 9;
else	
	ie_tipo_vencimento_w	:= 10;
end if;

return ie_tipo_vencimento_w;

end EIS_OBTER_TIPO_VENCTO_TITULO;
/
