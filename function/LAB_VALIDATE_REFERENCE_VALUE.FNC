create or replace function lab_validate_reference_value(	nr_prescricao_p			number,
										nr_seq_exame_p			number,
										nr_seq_material_p		number,
										ie_sexo_p				varchar2,
										dt_nascimento_p			date,
										ie_valor_percent_p		varchar2,
										qt_result_p				number,
										ds_result_p				varchar2,
										cd_estabelecimento_p	number)
										return number is

qt_casas_decimais_dias_w	number(2);
ie_tipo_valor_w				exame_lab_padrao.ie_tipo_valor%type;
qt_dias_w					number(10, 2);
qt_horas_w					number(10, 0);

begin
	ie_tipo_valor_w := 0;

    select	decode(nvl(max(ie_idade_int_val_ref), 'N'), 'N', 2, 0)
	into 	qt_casas_decimais_dias_w
	from	lab_parametro
	where	cd_estabelecimento = cd_estabelecimento_p;

	select 	obter_dias_entre_datas_lab(dt_nascimento_p, sysdate),
			Obter_Hora_Entre_datas(dt_nascimento_p, sysdate)
	into 	qt_dias_w,
			qt_horas_w
	from 	dual;

    if upper(ie_valor_percent_p) in ('P','V','D') then 
        begin
           select	a.ie_tipo_valor
            into	ie_tipo_valor_w   
            from
                (select	ie_tipo_valor
                from	exame_lab_padrao
                where	((ie_sexo = nvl(ie_sexo_p,'0')) or (ie_sexo = '0'))
                and		nr_seq_exame = nr_seq_exame_p
                and		nvl(nr_seq_material,nr_seq_material_p) = nr_seq_material_p
                and		(((nvl((trunc((qt_dias_w / 365.25),qt_casas_decimais_dias_w)),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'A')) or
                        ((nvl(trunc(((qt_dias_w / 365.25) * 12), qt_casas_decimais_dias_w),0) between qt_idade_min and qt_idade_max) and (ie_periodo = 'M')) or
                        (((nvl(qt_dias_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'D')) or
                        (((nvl(qt_horas_w,0)) between qt_idade_min and qt_idade_max) and (ie_periodo = 'H')))
                        
                and	(((ie_valor_percent_p = 'V') and (qt_result_p between qt_minima and qt_maxima)) or
                      ((ie_valor_percent_p = 'P') and (qt_result_p between qt_percent_min and qt_percent_max)) or
                      ((ie_valor_percent_p = 'D') and (upper(ELIMINA_ACENTOS(ds_result_p)) like upper(ELIMINA_ACENTOS(DS_OBSERVACAO)))) )
                
                and		nvl(ie_situacao,'A') = 'A'
                order 	by nr_seq_material, ie_sexo, ie_tipo_valor) a
            where
            rownum = 1;
        exception
            when no_data_found then
                ie_tipo_valor_w := 0;
        end;
    end if;    
    
    return ie_tipo_valor_w;
	
end lab_validate_reference_value;
/