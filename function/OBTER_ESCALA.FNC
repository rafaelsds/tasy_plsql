create or replace
function obter_escala (nr_sequencia_p number)
			return varchar2 is
ds_escala_w VARCHAR2(60);
begin	

ds_escala_w := '';	

if (nr_sequencia_p is not null) then
	select	nvl(max(ds_escala),'')
	into	ds_escala_w
	from	escala 
	where	nr_sequencia = nr_sequencia_p;
end if;

return ds_escala_w;
end;
/