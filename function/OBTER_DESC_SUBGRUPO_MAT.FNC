create or replace
function Obter_desc_subgrupo_mat	(cd_subgrupo_material_p	number) 
				return varchar2 is 


ds_retorno_w		varchar2(255);

begin

if	(cd_subgrupo_material_p is not null) then
	select	ds_subgrupo_material
	into	ds_retorno_w
	from	subgrupo_material
	where	cd_subgrupo_material	= cd_subgrupo_material_p;
end if;

return ds_retorno_w;

end Obter_desc_subgrupo_mat;
/