create or replace 
function Obter_hora_inicio_agenda(	nr_seq_agenda_p	number )
			return date is

dt_agenda_w	date	:= null;

begin

if	(nvl(nr_seq_agenda_p,0) > 0) then
	select	max(hr_inicio)
	into	dt_agenda_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
end if;	

return dt_agenda_w;

end Obter_hora_inicio_agenda;
/