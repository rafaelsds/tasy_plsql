create or replace
function retornar_prescr_atendimento( nr_atendimento_p	number)
						return number is

nr_prescricao_ww	number(14);
nr_prescricao_w   number(14):= 0;

cursor	c01 is
	select	nr_prescricao
	from	   cirurgia
	where	   nr_atendimento = nr_atendimento_p
	and	   dt_inicio_real 	<= sysdate
	and	   dt_inicio_real    > sysdate - 2
	and	   dt_termino 	is null
	and	   ie_status_cirurgia not in (3,4)
	order by dt_inicio_real;

begin
if	(nvl(nr_atendimento_p,0) > 0) then
	open C01;
	loop
	fetch C01 into	
      	   nr_prescricao_ww;
	exit when C01%notfound;
         		begin
         		nr_prescricao_w := nr_prescricao_ww;
         		end;
	end loop;
	close C01;
end if;	

return nr_prescricao_w;

end retornar_prescr_atendimento;
/