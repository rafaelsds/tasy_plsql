create or replace
function cnes_obter_apres_grupo_equip
			(	nr_seq_grupo_p		Number)
				return Number is
			
nr_retorno_w			Number(10);

begin

select	max(nr_seq_apresent)
into	nr_retorno_w
from	cnes_grupo_equipamento
where	nr_sequencia	= nr_seq_grupo_p;

return	nr_retorno_w;

end cnes_obter_apres_grupo_equip;
/