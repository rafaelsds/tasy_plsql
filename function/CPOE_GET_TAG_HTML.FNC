create or replace
function cpoe_get_tag_html	(	ie_tipo_item_p	varchar2 default null,
					nr_sequencia_p	number default null,
					ds_observacao_p	varchar2 default null) return varchar2 as

ds_tag_w varchar2(4000)	:= null;

ds_template_w	varchar2(4000)	:= '<div class="legend-cell-content-tag-cpoe" style="cursor: default;">
                            <div class="legend-cell-tag-cpoe" style="background-color: rgb(255, 233, 171); cursor: default;"
                            uib-tooltip="#@#@MY_TAG_TOOLTIP#@#@" tooltip-class="cpoe-status-tooltip" tooltip-append-to-body="true">
                                #@#@MY_TAG#@#@
                            </div>
			</div>';

cd_material_w		cpoe_material.cd_material%type;
ds_observacao_w		cpoe_material.ds_observacao%type;
ie_administracao_w	cpoe_material.ie_administracao%type;
dt_fim_w		cpoe_material.dt_fim%type;
ie_evento_unico_w	cpoe_material.ie_evento_unico%type;
dt_suspensao_w		cpoe_material.dt_suspensao%type;

	procedure addTag(	ds_informacao_p	varchar2) is
	ds_my_tag_w	varchar2(4000);
	ds_text_w	varchar2(4000)	:= ds_informacao_p;
	begin
	ds_my_tag_w	:= replace(ds_template_w,'#@#@MY_TAG_TOOLTIP#@#@',ds_text_w);
	
	if	(dt_suspensao_w	is not null) then
		ds_text_w	:=  '<div class="cpoe-item-suspenso">'||ds_text_w|| '</div>';
	end if;
	
	ds_tag_w	:= ds_tag_w || replace(ds_my_tag_w,'#@#@MY_TAG#@#@',ds_text_w);	
	end addTag;

	
	function getMessageTag(	nr_seq_texto_p	number) return varchar2 is
	
	begin
	
	return wheb_mensagem_pck.get_texto(nr_seq_texto_p);
	
	end getMessageTag;
	
	procedure addTagTexto(	nr_seq_texto_p	number) is
	begin
	addTag(getMessageTag(nr_seq_texto_p));
	end addTagTexto;

begin

if	(ie_tipo_item_p	in ('SOL','M')) then
	
	select	a.cd_material,
		a.ds_observacao,
		a.ie_administracao,
		a.dt_suspensao,
		a.dt_fim
	into	cd_material_w,
		ds_observacao_w,
		ie_administracao_w,
		dt_suspensao_w,
		dt_fim_w
	from	cpoe_material a
	where	nr_sequencia = nr_sequencia_p;
	
	
	if	(ie_tipo_item_p	= 'SOL') then
		addTagTexto(1135866);
	end if;
	
	if	(Get_Ordered_on_behalf(nr_sequencia_p,'M')	= 'S') then
		addTagTexto(1135865);
	end if;
	
	if	(ie_administracao_w	= 'N') then
		addTagTexto(1135884);
	end if;
	
	if	(ie_evento_unico_w	= 'S') then
		addTagTexto(1136256);
	end if;
	
	if	(get_if_tapering_dose(nr_sequencia_p)	= 'S') then
		addTagTexto(1139373);
		if	(dt_fim_w is null) then
			addTagTexto(1153314);
		end if;
	end if;
	
	if	(get_if_variable_dose(nr_sequencia_p)	= 'S') then
		addTagTexto(1146170);
	end if;
	
	if	(get_if_nurse_initiated(nr_sequencia_p)	= 'S') then
		addTagTexto(1149071);
	end if;
	
	if	(CPOE_OBTER_SE_MAT_TAG(cd_material_w,'ANTCOAG')	= 'S') then
		addTagTexto(1151942);
	end if;
	
	if	(CPOE_OBTER_SE_MAT_TAG(cd_material_w,'INSUL')	= 'S') then
		addTagTexto(1151944);
	end if;
	
	if	(ds_observacao_w is not null) then
		addTagTexto(1139298);
	end if;
else
	if	(ds_observacao_p is not null) then
		addTagTexto(1139298);
	end if;

end if;

return ds_tag_w;

end cpoe_get_tag_html;
/
