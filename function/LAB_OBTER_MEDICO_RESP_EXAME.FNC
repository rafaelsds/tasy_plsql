create or replace function Lab_Obter_Medico_Resp_Exame(
				nr_prescricao_p		number,
				nr_seq_prescr_p		number,
				ie_opcao_p		number)
				return varchar2 is
cd_medico_resp_w 	Varchar2(10);
nm_pessoa_fisica_w 	Varchar2(100);
ds_retorno_w		varchar2(255);
/* ie_opcao_p
   1 - cd_medico_resp
   2 - nm_pessoa_fisica
*/
Begin
select 	max(a.cd_medico_resp)
into 	cd_medico_resp_w
from	exame_lab_result_item	a,
	exame_lab_resultado	b
where 	b.nr_seq_resultado	= a.nr_seq_resultado
and	b.nr_prescricao		= nr_prescricao_p
and	a.nr_seq_prescr		= nr_seq_prescr_p
and	a.nr_seq_material is not null;
if	(ie_opcao_p = 1) then
	ds_retorno_w	:= cd_medico_resp_w;
elsif	(ie_opcao_p = 2) then
	select 	SUBSTR(OBTER_NOME_PF(CD_PESSOA_FISICA),0,100)
	into	nm_pessoa_fisica_w
	from 	pessoa_fisica
	where	cd_pessoa_fisica = cd_medico_resp_w;
	ds_retorno_w := nm_pessoa_fisica_w;
end if;
return ds_retorno_w;
end;
/