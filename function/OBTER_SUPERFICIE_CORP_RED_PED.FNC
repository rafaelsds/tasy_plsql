CREATE OR REPLACE FUNCTION obter_superficie_corp_red_ped (
    qt_peso_p                  NUMBER,
    qt_altura_p                NUMBER,
    qt_porcent_reducao_p       NUMBER,
    cd_pessoa_fisica_p         VARCHAR2,
    nm_usuario_p               VARCHAR2,
    ie_formula_sup_corporea_p  VARCHAR2 DEFAULT NULL
) RETURN NUMBER IS

    qt_resultado_w              NUMBER(15, 4);
    qt_idade_calc_w             VARCHAR2(3);
    qt_idade_paciente_w         NUMBER(3, 0);
    qt_max_result_w             NUMBER(15, 4);
    sql_w                       VARCHAR2(200);
    ie_formula_sup_corporea_w	varchar2(10);
    nr_dec_sc_w                 number(10);
    
    
    PRAGMA autonomous_transaction;
BEGIN
    obter_param_usuario(281, 381, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento,
                       qt_idade_calc_w);
    SELECT
        MAX(qt_max_sc_onc)
    INTO qt_max_result_w
    FROM
        parametro_medico
    WHERE
        cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
        
        
    select nvl(nvl(ie_formula_sup_corporea_p,max(ie_formula_sup_corporea)),'M'),
           max(nr_dec_sc)
    into   ie_formula_sup_corporea_w,
           nr_dec_sc_w
    from   parametro_medico
    where  cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;
        
        

    qt_idade_paciente_w := obter_idade_pf(cd_pessoa_fisica_p, sysdate, 'A');
    
    BEGIN
        sql_w := 'CALL OBTER_SUPERF_CORP_RED_PED_MD(:1, :2, :3, :4, :5, :6, :7, :8) INTO :qt_resultado_w';
        EXECUTE IMMEDIATE sql_w
            USING IN qt_idade_paciente_w, IN qt_idade_calc_w, IN qt_peso_p, IN qt_altura_p, IN ie_formula_sup_corporea_w, IN qt_porcent_reducao_p,
            IN qt_max_result_w, IN nr_dec_sc_w, OUT qt_resultado_w;

    EXCEPTION
        WHEN OTHERS THEN
            qt_resultado_w := NULL;
    END;


    RETURN qt_resultado_w;
END obter_superficie_corp_red_ped;
/
