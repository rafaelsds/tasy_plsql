create or replace
function obter_data_agenda_consulta_pf (cd_pessoa_fisica_p	number)
				   return	date is

dt_agenda_w	date;

begin

if	(cd_pessoa_fisica_p is not null) then
	select	max(dt_agenda)
	into	dt_agenda_w
	from	agenda_consulta
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
end if;

return	dt_agenda_w;

end obter_data_agenda_consulta_pf;
/
