create or replace
function Obter_Se_Perm_Agecons
		(cd_pessoa_agenda_p	varchar2,
		cd_pessoa_usuario_p	varchar2,
		cd_perfil_p		number)
 		return varchar2 is
		
ie_agenda_w	varchar2(01)	:= 'T';

begin

select	nvl(min(a.ie_agenda), 'T')
into	ie_agenda_w
from	agenda b, med_permissao a
where	a.cd_agenda					= b.cd_agenda
and	a.cd_medico_prop				= cd_pessoa_agenda_p
and	nvl(a.cd_pessoa_fisica, cd_pessoa_usuario_p)	= cd_pessoa_usuario_p
and	nvl(a.cd_perfil, cd_perfil_p)			= cd_perfil_p
and	b.ie_situacao					= 'A';

return	ie_agenda_w;

end Obter_Se_Perm_Agecons;
/