create or replace
function ctb_obter_orc_centro_agrup(	cd_empresa_p		number,
				nr_seq_mes_ref_p		number,
				cd_estabelecimento_p		number,
				nr_seq_cenario_p		number,
				cd_centro_custo_p		number,
				cd_conta_contabil_p		varchar2,
				ie_valor_p			varchar2)
 		    		return number is	

cd_classif_centro_w		varchar2(80);
cd_estabelecimento_w		number(10);
qt_regra_w			number(10)	:= 0;
vl_centro_agregado_w		number(15,2)	:= 0;
vl_centro_custo_w		number(15,2)	:= 0;
vl_retorno_w			number(15,2)	:= 0;

begin

select	max(cd_classificacao),
	max(cd_estabelecimento)
into	cd_classif_centro_w,
	cd_estabelecimento_w
from	centro_custo
where	cd_centro_custo	= cd_centro_custo_p;

select	count(*)
into	qt_regra_w
from	centro_custo b,
	centro_custo_orc_estab a
where	a.cd_centro_custo	= b.cd_centro_custo
and	b.cd_classificacao like cd_classif_centro_w||'%';

if	(nr_seq_cenario_p is null) then
	begin
	/* valor do centro de origem */
	select	nvl(sum(decode(ie_valor_p,'O',a.vl_orcado,'R',a.vl_realizado)),0)
	into	vl_centro_custo_w
	from	centro_custo b,
		ctb_orcamento a
	where	a.cd_centro_custo	= b.cd_centro_custo
	and	b.cd_classificacao like cd_classif_centro_w||'%'
	and	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
	and	a.cd_conta_contabil	= cd_conta_contabil_p
	and	b.cd_estabelecimento	= cd_estabelecimento_w;
	

	if	(qt_regra_w > 0) and
		(cd_estabelecimento_p is null) then
		/* soma dos centros agregados */
		select	nvl(sum(decode(ie_valor_p,'O',a.vl_orcado,'R',a.vl_realizado)),0)
		into	vl_centro_agregado_w
		from	centro_custo d,
			estabelecimento c,
			centro_custo_orc_estab b,
			ctb_orcamento a
		where	a.cd_estabelecimento	= c.cd_estabelecimento
		and	a.cd_centro_custo	= b.cd_centro_destino
		and	b.cd_centro_destino	= d.cd_centro_custo
		and	d.cd_classificacao like cd_classif_centro_w||'%'
		and	c.cd_empresa		= cd_empresa_p
		and	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
		and	a.cd_conta_contabil	= cd_conta_contabil_p;

	end if;
	end;
else
	begin
	select	nvl(sum(a.vl_orcado),0)
	into	vl_centro_custo_w
	from	centro_custo b,
		ctb_orc_cen_valor a
	where	a.cd_centro_custo	= b.cd_centro_custo
	and	b.cd_classificacao like cd_classif_centro_w||'%'
	and	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
	and	a.cd_conta_contabil	= cd_conta_contabil_p
	and	a.nr_seq_cenario	= nr_seq_cenario_p
	and	b.cd_estabelecimento	= cd_estabelecimento_w;
	
	if	(qt_regra_w > 0) and
		(cd_estabelecimento_p is null) then
		
		select	nvl(sum(a.vl_orcado),0)
		into	vl_centro_agregado_w
		from	centro_custo d,
			estabelecimento c,
			centro_custo_orc_estab b,
			ctb_orc_cen_valor a
		where	a.cd_estabelecimento	= c.cd_estabelecimento
		and	a.cd_centro_custo	= b.cd_centro_destino
		and	b.cd_centro_destino	= d.cd_centro_custo
		and	d.cd_classificacao like cd_classif_centro_w||'%'
		and	c.cd_empresa		= cd_empresa_p
		and	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
		and	a.nr_seq_cenario	= nr_seq_cenario_p
		and	a.cd_conta_contabil	= cd_conta_contabil_p;
		
	end if;
	end;
end if;
vl_retorno_w	:= vl_centro_custo_w + vl_centro_agregado_w;

return	vl_retorno_w;

end ctb_obter_orc_centro_agrup;
/
