create or replace
FUNCTION obter_prescr_dieta_etiq(nr_seq_mapa_p		NUMBER)
 		    	RETURN NUMBER IS
/*
	Buscar o numero da prescrição da dieta.
*/
nr_prescricao_w 		NUMBER(10);
ie_excluir_w			VARCHAR2(1);
ie_ultima_prescricao_pac_w	VARCHAR2(1);
nr_atend_alta_w			NUMBER(10);
nr_atendimento_w		mapa_dieta.nr_atendimento%TYPE;
ie_liberado_w			VARCHAR2(1);
cd_pessoa_fisica_w		mapa_dieta.cd_pessoa_fisica%TYPE;
dt_dieta_w			mapa_dieta.dt_dieta%TYPE;
cd_refeicao_w			mapa_dieta.cd_refeicao%TYPE;
ie_prescr_liberadas_enferm_w	VARCHAR2(1);
qt_hora_w			NUMBER(5);
dt_final_w			DATE;
varPrescrRN_w			VARCHAR2(1);

BEGIN
IF	(nr_seq_mapa_p IS NOT NULL) THEN

	obter_param_usuario(1000, 9, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, qt_hora_w);
	obter_param_usuario(1000, 19, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, varPrescrRN_w);
	obter_param_usuario(1000, 105, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_ultima_prescricao_pac_w);
	obter_param_usuario(1000, 114, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_prescr_liberadas_enferm_w);

	SELECT	cd_pessoa_fisica,
		dt_dieta,
		cd_refeicao,
		nr_atendimento
	INTO	cd_pessoa_fisica_w,
		dt_dieta_w,
		cd_refeicao_w,
		nr_atendimento_w
	FROM	mapa_dieta
	WHERE	nr_sequencia = nr_seq_mapa_p;

	IF	(TRUNC(dt_dieta_w, 'dd') = TRUNC(SYSDATE, 'dd')) THEN
		dt_final_w	:= SYSDATE;
	ELSE
		dt_final_w	:= fim_dia(dt_dieta_w);
	END IF;

	SELECT	NVL(MAX(a.nr_prescricao),0)
	INTO	nr_prescricao_w
	FROM 	prescr_dieta b,
		prescr_medica a
	WHERE 	a.nr_prescricao 	= b.nr_prescricao
	AND 	a.nr_atendimento	= nr_atendimento_w
	AND 	cd_refeicao_w	= DECODE(NVL(b.ie_refeicao,'T'),'T', cd_refeicao_w, NVL(b.ie_refeicao,'T'))
	AND	((NVL(a.dt_liberacao,a.dt_liberacao_medico) IS NOT NULL AND ie_prescr_liberadas_enferm_w = 'N')
	OR	a.dt_liberacao IS NOT NULL)
	AND 	NVL(b.ie_suspenso,'N') = 'N'
	AND 	a.dt_prescricao	> SYSDATE - (qt_hora_w / 24)
	AND	a.dt_prescricao <= dt_final_w
	AND	((NVL(a.ie_recem_nato,'N') = 'N') OR (a.ie_recem_nato = varPrescrRN_w));

	IF (nr_prescricao_w = 0) AND (ie_ultima_prescricao_pac_w = 'S') THEN

		SELECT	MAX(a.nr_atend_alta)
		INTO	nr_atend_alta_w
		FROM	atendimento_paciente a
		WHERE	a.nr_atendimento = nr_atendimento_w;

		IF	(nr_atend_alta_w IS NOT NULL) THEN

			SELECT	NVL(MAX(a.nr_prescricao),0)
			INTO	nr_prescricao_w
			FROM 	prescr_dieta b,
				prescr_medica a
			WHERE 	a.nr_prescricao		= b.nr_prescricao
			AND	a.nr_atendimento	= nr_atend_alta_w
			AND	obter_tipo_atendimento(a.nr_atendimento) = 3
			AND 	cd_refeicao_w	= DECODE(NVL(b.ie_refeicao,'T'),'T', cd_refeicao_w, NVL(b.ie_refeicao,'T'))
			AND	((NVL(a.dt_liberacao,a.dt_liberacao_medico) IS NOT NULL AND ie_prescr_liberadas_enferm_w = 'N')
			OR	a.dt_liberacao IS NOT NULL)
			AND 	NVL(b.ie_suspenso,'N')	= 'N'
			AND	((NVL(a.ie_recem_nato,'N') = 'N') OR (a.ie_recem_nato = varPrescrRN_w));

		END IF;
	END IF;
END IF;

RETURN	nr_prescricao_w;

END obter_prescr_dieta_etiq;
/