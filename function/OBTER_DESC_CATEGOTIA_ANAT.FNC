create or replace 
Function Obter_Desc_categotia_anat(	cd_cat_anat_p		Varchar2) 
					return varchar2 is

ds_retorno_w	Varchar2(100);

begin

if	(cd_cat_anat_p is not null) then
	select	ds_categoria_anatomica
	into	ds_retorno_w
	from	cido_categoria_anatomica
	where	cd_categoria_anatomica = cd_cat_anat_p;
end if;

return ds_retorno_w;

end Obter_Desc_categotia_anat;
/