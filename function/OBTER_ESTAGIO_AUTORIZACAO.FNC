create or replace
function obter_estagio_autorizacao(nr_seq_gestao_p	number)
						return varchar2 is

ds_retorno_w				varchar2(255);

begin

if	(nr_seq_gestao_p is not null) then

	select	max(b.ds_estagio)
	into	ds_retorno_w
	from	estagio_autorizacao b,
			autorizacao_convenio a
	where	a.nr_seq_estagio = b.nr_sequencia
	and		((b.ie_interno in (90, 70, 60)) or (b.ie_autor_agenda in ('N', 'B')))
	and		a.nr_sequencia	 = ( select	max(c.nr_sequencia)
								 from	autorizacao_convenio c
								 where	c.nr_seq_gestao		= nr_seq_gestao_p);
end if;

return ds_retorno_w;

end;
/