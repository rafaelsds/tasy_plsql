create or replace
function lic_obter_vl_limite_conta(
			dt_referencia_p			date,
			cd_conta_contabil_p		varchar2,
			nr_seq_tipo_compra_p		number,
			nr_seq_mod_compra_p		number,
			cd_estabelecimento_p		number)
 		    return number is

vl_limite_w	number(17,4);			
			
begin

select	nvl(max(a.vl_limite),0)
into	vl_limite_w
from	reg_lic_conta_valor a
where	dt_referencia_p between a.dt_inicio_vigencia and a.dt_fim_vigencia
and	a.ie_situacao = 'A'
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	nvl(a.cd_conta_contabil, nvl(cd_conta_contabil_p, 0))		= nvl(cd_conta_contabil_p,0)
and	nvl(a.nr_seq_tipo_compra, nvl(nr_seq_tipo_compra_p,0))		= nvl(nr_seq_tipo_compra_p,0)
and	nvl(a.nr_seq_mod_compra, nvl(nr_seq_mod_compra_p,0))		= nvl(nr_seq_mod_compra_p,0);

return	vl_limite_w;

end lic_obter_vl_limite_conta;
/