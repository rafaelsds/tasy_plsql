create or replace
function fis_obter_se_brasileiro	(cd_pessoa_fisica_p	varchar2)
					return varchar2 is

ie_brasileiro_w	varchar2(1) := 'N';
cd_nacionalidade_w	varchar2(8);
nr_cpf_w 		pessoa_fisica.nr_cpf%type;

begin
if	(cd_pessoa_fisica_p is not null) then
	select	nvl(max(cd_nacionalidade),'0'),
			NVL(max(nr_cpf),'0')
	into	cd_nacionalidade_w,
			nr_cpf_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	if (nr_cpf_w is not null and nr_cpf_w != '0') then
		ie_brasileiro_w := 'S';
	elsif	(cd_nacionalidade_w = '0') then
		ie_brasileiro_w := 'S';
	else
		select	nvl(max(ie_brasileiro),'N')
		into	ie_brasileiro_w
		from	nacionalidade
		where	cd_nacionalidade = cd_nacionalidade_w;
	end if;
end if;

return ie_brasileiro_w;

end fis_obter_se_brasileiro;
/
