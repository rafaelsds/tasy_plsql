CREATE or replace function HMSL_obter_pct_ocup_conv(cd_convenio_p		number,
				  dt_parametro_p	date,
				  ie_periodo_p		varchar2,
				  cd_estabelecimento_p	number)
 		    	return number is

total_leitos_w	number(10);
			
begin

if	(ie_periodo_p = 'M') then
	begin
	select  SUM(nr_leitos_ocupados) 
	into	total_leitos_w
	FROM 	HMSL_EIS_OCUP_convenio_V
	WHERE  	TRUNC(dt_referencia,'month') = TRUNC(dt_parametro_p,'month')
	and	cd_convenio = cd_convenio_p
	and	cd_estabelecimento = cd_estabelecimento_p
	AND	ie_periodo = 'M';
	end;
end if;

if	(ie_periodo_p = 'D') then
	begin
	select  SUM(nr_leitos_ocupados) 
	into	total_leitos_w
	FROM 	HMSL_EIS_OCUP_convenio_V
	WHERE  	TRUNC(dt_referencia,'dd') = TRUNC(dt_parametro_p,'dd')
	and	cd_convenio = cd_convenio_p
	and	cd_estabelecimento = cd_estabelecimento_p
	AND	ie_periodo = 'D';
	end;
end if;

if	(ie_periodo_p = '6h') then
	begin
	select  SUM(nr_leitos_ocupados) 
	into	total_leitos_w
	FROM 	HMSL_EIS_OCUP_convenio_V
	WHERE  	TRUNC(dt_referencia,'dd') = TRUNC(dt_parametro_p,'dd')
	and	to_char(dt_referencia,'hh24') = '06'
	and	cd_convenio = cd_convenio_p
	and	cd_estabelecimento = cd_estabelecimento_p
	AND	ie_periodo = 'D';
	end;
end if;

return	total_leitos_w;

end HMSL_obter_pct_ocup_conv;

/