create or replace
function pls_obter_desc_lista_municip
			(	cd_municipio_ibge_p		varchar2)
				return varchar2 is

ds_retorno_w			varchar2(4000);
ds_lista_w			varchar2(100);
pos_virgula_w			number(5);
ie_acao_w			varchar2(60);

begin

ds_lista_w	:= cd_municipio_ibge_p || ',';

while (ds_lista_w is not null) loop 
	begin
	
	select  instr(ds_lista_w,',')
	into    pos_virgula_w
	from    dual;
	
	if	(pos_virgula_w > 0) then
		ie_acao_w	:= substr(ds_lista_w,1,pos_virgula_w-1);
		
		if	(ie_acao_w is not null) then
			ds_lista_w	:= substr(ds_lista_w,pos_virgula_w+1,length(ds_lista_w));
			
			if	(ds_retorno_w is null) then
				ds_retorno_w	:= obter_desc_municipio_ibge(ie_acao_w);
			else
				ds_retorno_w	:= ds_retorno_w || ', ' || obter_desc_municipio_ibge(ie_acao_w);
			end if;
		else
			ds_lista_w	:= null;
		end if;
	end if;
	
	end;
end loop;

return	substr(ds_retorno_w,1,255);

end pls_obter_desc_lista_municip;
/
