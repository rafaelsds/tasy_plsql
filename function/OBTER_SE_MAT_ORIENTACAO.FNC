create or replace 
function Obter_se_mat_orientacao(	cd_material_p		number,
									cd_pessoa_fisica_p	varchar2 default null)
			return char is

ie_retorno_w	char(1) := 'N';

BEGIN

if	(cd_pessoa_fisica_p is not null) then
	ie_retorno_w := Obter_se_apres_orient_mat(cd_material_p, cd_pessoa_fisica_p);
end if;

if	(nvl(ie_retorno_w,'S') = 'S') then
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	material
	where	rownum = 1
	and		cd_material	= cd_material_p
	and		nvl(ie_mostrar_orientacao,'S')	= 'S'
	and		ds_orientacao_usuario is not null;
	
	if	(ie_retorno_w = 'N') then

		select	nvl(max('S'),'N')
		into	ie_retorno_w
		from	material_reacao
		where	cd_material	= cd_material_p
		and		rownum = 1
		and		ds_reacao is not null;
	end if;
end if;

return ie_retorno_w;

END Obter_se_mat_orientacao;
/