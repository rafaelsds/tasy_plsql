CREATE OR REPLACE
FUNCTION obter_curva_abc_local(
			cd_material_p		number,
			ie_curva_grupo_p		varchar2,
			cd_local_estoque_p	number,
			dt_mesano_referencia_p	date)
	RETURN Varchar2 IS

ie_curva_w		varchar2(1);
cd_material_w		Number(06,0);

BEGIN

select	nvl(max(cd_material_estoque), cd_material_p)
into	cd_material_w
from 	material
where 	cd_material	= cd_material_p;

select	nvl(max(decode(ie_curva_grupo_p,'S',ie_curva_abc_grupo,ie_curva_abc)),'X')
into	ie_curva_w
from	material_abc_local
where	cd_material	= cd_material_w
and	cd_local_estoque	= cd_local_estoque_p
and	dt_mesano_referencia = (
	select	max(dt_mesano_referencia)
	from	material_abc_local
	where	cd_material		= cd_material_w
	and	cd_local_estoque		= cd_local_estoque_p
	and	dt_mesano_Referencia	<= dt_mesano_referencia_p);

Return ie_curva_w;

end obter_curva_abc_local;
/