create or replace
function Obter_Desc_Plano_Conv(
		cd_convenio_p	Number,
		cd_plano_p	Varchar2)
		return	varchar2 is


ds_plano_w		varchar2(80);

begin
if	(cd_convenio_p	is not  null) and
	(cd_plano_p	is not null) then
	select	max(ds_plano)
	into	ds_plano_w
	from	Convenio_Plano
	where	cd_convenio	= nvl(cd_convenio_p,0)
	and	cd_plano	= nvl(cd_plano_p,'0');
end if;

return	ds_plano_w;

end Obter_Desc_Plano_Conv;
/