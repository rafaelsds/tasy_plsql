create or replace
function obter_aprov_exame_regra(	nr_seq_exame_p		number,
					cd_pessoa_fisica_p	varchar2,
					ds_campo_p		varchar2,
					qt_result_p		number				
				)
 		    	return varchar2 is

nr_prescricao_w		number(10);
nr_seq_prescr_w		number(10);
nr_seq_resultado_w	number(10);

pr_resultado_w		number(9,4);
qt_resultado_w		number(15,4);

pr_minimo_w		number(10,2);
pr_maximo_w		number(10,2);
ie_referencia_w		varchar2(1);

vl_resultado_w		number(15,4);
vl_resultado_min_w	number(15,4);
vl_resultado_max_w	number(15,4);

ie_resultado_w		varchar2(1) := 'N';

nr_seq_grupo_w		number(10);

begin


ie_resultado_w	:= 'N';

if	(cd_pessoa_fisica_p 	is not null) and
	(nr_seq_exame_p 	is not null) then

	-- Busca dados da regra conforme Exame
	select	max(pr_minimo),
		max(pr_maximo),
		max(ie_referencia)
	into	pr_minimo_w,
		pr_maximo_w,
		ie_referencia_w
	from	lab_regra_aprovacao_exame
	where	nr_seq_exame	= nr_seq_exame_p;
	
	if	((pr_minimo_w is null) and (pr_maximo_w is null)) then
		
		select	max(nr_seq_grupo)
		into	nr_seq_grupo_w
		from	exame_laboratorio
		where	nr_seq_exame 	= nr_seq_exame_p;
		
		
		select	max(pr_minimo),
			max(pr_maximo),
			max(ie_referencia)
		into	pr_minimo_w,
			pr_maximo_w,
			ie_referencia_w
		from	lab_regra_aprovacao_exame
		where	nr_seq_grupo	= nr_seq_grupo_w;
		
	end if;
	
	
	if	((pr_minimo_w is not null) and (pr_maximo_w is not null)) then
	
		--Busca a �ltima prescri��o do paciente que possua a o exame
		select	max(e.nr_prescricao)
		into	nr_prescricao_w
		from	prescr_procedimento e,
			(select	nr_prescricao
			from	prescr_medica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_p) p
		where	e.nr_prescricao = p.nr_prescricao
		and	e.nr_seq_exame	= nr_seq_exame_p;
		

		if	(nr_prescricao_w is not null) then
		
			select	max(nr_sequencia)
			into	nr_seq_prescr_w
			from	prescr_procedimento
			where	nr_prescricao 	= nr_prescricao_w
			and	nr_seq_exame	= nr_seq_exame_p;

			select	max(nr_seq_resultado)
			into	nr_seq_resultado_w
			from	exame_lab_resultado
			where	nr_prescricao	= nr_prescricao_w;
			
			if	(nr_seq_resultado_w is not null) then
			
				select	decode(ds_campo_p,'pr_resultado', pr_resultado, qt_resultado)					
				into	vl_resultado_w
				from	exame_lab_result_item
				where	nr_seq_exame 	 = nr_seq_exame_p
				and	nr_seq_resultado = nr_seq_resultado_w;

				
				select	(vl_resultado_w - ((vl_resultado_w)*(pr_minimo_w / 100))),
					(vl_resultado_w + ((vl_resultado_w)*(pr_maximo_w / 100)))	
				into	vl_resultado_min_w,
					vl_resultado_max_w
				from	dual;
				
				
				if	(qt_result_p >= vl_resultado_min_w) and
					(qt_result_p <= vl_resultado_max_w) then
					
					ie_resultado_w := 'S';	
					
				end if;
				
				
			elsif	(ie_referencia_w = 'S') then
		
				ie_resultado_w := 'S';	
					
			end if;
		
		elsif	(ie_referencia_w = 'S') then
		
			ie_resultado_w := 'S';	
			
		end if;
	
	else
	
		ie_resultado_w := 'N';	
		
	end if;

end if;

return	ie_resultado_w;

end obter_aprov_exame_regra;
/