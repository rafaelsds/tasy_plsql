create or replace FUNCTION OBTER_PRESCR_FLEURY_ASYNC 
(
  NR_PRESCRICAO_P IN prescr_procedimento.nr_prescricao%type
) RETURN VARCHAR2 AS 

count_1 number;
count_2 number;
count_3 number;

BEGIN
select count(*)
into count_1
from   prescr_procedimento p,
exame_laboratorio e
where  p.nr_prescricao  = nr_prescricao_p
and    p.nr_seq_exame   = e.nr_seq_exame
and    e.cd_cgc_externo = '60840055000131';

select count(*)
into count_2
from   proc_interno i,
	   prescr_procedimento p ,
       regra_proc_interno_integra t
where  i.ie_tipo in ('AP','APH','APC')
and	   p.nr_prescricao = nr_prescricao_p
and    i.nr_sequencia  = p.nr_seq_proc_interno
and    p.cd_cgc_laboratorio = '60840055000131'
and	   t.nr_seq_proc_interno = i.nr_sequencia
and	   t.ie_tipo_integracao = 3;

select count(*)
into count_3
from   proc_interno i,
	   prescr_procedimento p,
	   regra_proc_interno_integra t
where  i.ie_tipo not in ('AP','APH','APC')
and	   p.nr_prescricao = nr_prescricao_p
and    i.nr_sequencia  = p.nr_seq_proc_interno
and	   t.nr_seq_proc_interno = i.nr_sequencia
and	   t.ie_tipo_integracao = 6;

if(count_1 > 0 or count_2 > 0 or count_3 > 0) then
     return '1';
else 
     return '0';
end if;
END OBTER_PRESCR_FLEURY_ASYNC;
/
