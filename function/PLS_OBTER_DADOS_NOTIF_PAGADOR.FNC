create or replace
function pls_obter_dados_notif_pagador	(	nr_seq_notif_pagador_p		pls_notificacao_pagador.nr_sequencia%type,
						ie_opcao_p			varchar2)
						return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar informa��es das notifica��es dos pagadores
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de ATEN��O:

'CO'	= Cobrador atual
'ECO'	= Grupo de cobran�a atual
'OC'	= Original cobrador

As op��es acima s�o utilizadas exclusivamente na fun��o OPS - Controle de Notifica��es de Atraso
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

ds_retorno_w		varchar2(4000);
nr_seq_cobrador_w	pls_notificacao_pagador.nr_seq_cobrador%type;
nr_seq_grupo_w		grupo_cobranca_membro.nr_seq_grupo%type;
nr_seq_cobranca_w	cobranca.nr_sequencia%type;
qt_grupo_w		pls_integer;

begin
if	(nr_seq_notif_pagador_p is not null) then

	if	(ie_opcao_p = 'CO') then
		select	max(a.nr_sequencia)
		into	nr_seq_cobranca_w
		from	cobranca a
		where	a.ie_status <> 'C'
		and	a.nr_titulo in (select	x.nr_titulo
					from	pls_notificacao_item x
					where	x.nr_seq_notific_pagador = nr_seq_notif_pagador_p);
		
		if	(nr_seq_cobranca_w is not null) then
			select	max(b.nm_pessoa_fisica)
			into	ds_retorno_w
			from	pessoa_fisica	b,
				cobrador	a,
				cobranca	c
			where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
			and	a.nr_sequencia		= c.nr_seq_cobrador
			and	c.nr_sequencia		= nr_seq_cobranca_w;
		end if;
		
	elsif	(ie_opcao_p = 'ECO') then
		select	count(1)
		into	qt_grupo_w
		from	grupo_cobranca_membro
		where	rownum	= 1;
	
		if	(qt_grupo_w > 0) then
			select	max(a.nr_sequencia)
			into	nr_seq_cobranca_w
			from	cobranca a
			where	a.ie_status <> 'C'
			and	a.nr_titulo in (select	x.nr_titulo
						from	pls_notificacao_item x
						where	x.nr_seq_notific_pagador = nr_seq_notif_pagador_p);
				
			if	(nr_seq_cobranca_w is not null) then
				select	max(b.nr_seq_grupo)
				into	nr_seq_grupo_w
				from	grupo_cobranca_membro	b,
					cobrador		a,
					cobranca		c
				where	a.nr_sequencia	= b.nr_seq_cobrador
				and	a.nr_sequencia	= c.nr_seq_cobrador
				and	c.nr_sequencia = nr_seq_cobranca_w;
				
				if	(nr_seq_grupo_w is not null) then
					select	a.ds_grupo
					into	ds_retorno_w
					from	grupo_cobranca	a
					where	a.nr_sequencia	= nr_seq_grupo_w;
				end if;
			end if;
		end if;
		
	elsif	(ie_opcao_p = 'OC') then
		select	nr_seq_cobrador
		into	nr_seq_cobrador_w
		from	pls_notificacao_pagador	a
		where	a.nr_sequencia	= nr_seq_notif_pagador_p;
		
		if	(nr_seq_cobrador_w is not null) then
			select	max(b.nm_pessoa_fisica)
			into	ds_retorno_w
			from	pessoa_fisica	b,
				cobrador	a
			where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
			and	a.nr_sequencia		= nr_seq_cobrador_w;
		end if;
	end if;
	
end if;

return	ds_retorno_w;

end pls_obter_dados_notif_pagador;
/