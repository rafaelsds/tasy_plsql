create or replace
function man_obter_desc_estagio_trad
			(	nr_seq_estagio_p		number,
				nr_seq_localizacao_p		number,
				ie_opcao_p			varchar2)
 		    		return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w			varchar2(255)	:= '';
cd_cnpj_w			man_localizacao.cd_cnpj%type;
nr_seq_idioma_w			pessoa_juridica.nr_seq_idioma%type;
cd_exp_estagio_w		man_estagio_processo.cd_exp_estagio%type;
ds_estagio_w			man_estagio_processo.ds_estagio%type;
cd_exp_explic_estagio_w		man_estagio_processo.cd_exp_explic_estagio%type;
ds_explicacao_w			man_estagio_processo.ds_explicacao%type;
cd_expressao_w			man_estagio_processo.cd_exp_estagio%type;

begin
if	(nr_seq_estagio_p is not null) then
	select	max(a.ds_estagio),
		max(a.cd_exp_estagio),
		max(a.cd_exp_explic_estagio),
		substr(max(a.ds_explicacao),1,255)
	into	ds_estagio_w,
		cd_exp_estagio_w,
		cd_exp_explic_estagio_w,
		ds_explicacao_w
	from	man_estagio_processo	a
	where	a.nr_sequencia	= nr_seq_estagio_p;
	
	if	(ie_opcao_p = 'D') then
		ds_retorno_w	:= ds_estagio_w;
		cd_expressao_w	:= cd_exp_estagio_w;
	elsif	(ie_opcao_p = 'X') then
		ds_retorno_w	:= ds_explicacao_w;
		cd_expressao_w	:= cd_exp_explic_estagio_w;
	end if;
	
	

	if	(nr_seq_localizacao_p is not null) then
		select	max(a.cd_cnpj)
		into	cd_cnpj_w
		from	man_localizacao	a
		where	a.nr_sequencia	= nr_seq_localizacao_p;
		
		if	(cd_cnpj_w is not null) then
			select	max(a.nr_seq_idioma)
			into	nr_seq_idioma_w
			from	pessoa_juridica	a
			where	a.cd_cgc	= cd_cnpj_w;
			
			if	(nr_seq_idioma_w is not null) then
				ds_retorno_w	:= obter_desc_expressao_idioma(	cd_expressao_w,
										ds_retorno_w,
										nr_seq_idioma_w);
			end if;
		end if;
	end if;
end if;

return ds_retorno_w;

end man_obter_desc_estagio_trad;
/