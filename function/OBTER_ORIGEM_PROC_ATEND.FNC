create or replace
function obter_origem_proc_atend(	nr_atendimento_p		number,
					cd_estabelecimento_p		number)
 		    	return number is

cd_convenio_w		number(5);
cd_categoria_w 		varchar2(10);
ie_tipo_atend_w		number(3);
ie_tipo_conv_w		number(2);
ie_origem_proced_w	Number(10,0);

begin

cd_convenio_w 	:= obter_convenio_atendimento(nr_atendimento_p);
cd_categoria_w 	:= obter_categoria_atendimento(nr_atendimento_p);

Select	ie_tipo_atendimento, 
	ie_tipo_convenio
into	ie_tipo_atend_w,
	ie_tipo_conv_w	
from 	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

ie_origem_proced_w := obter_origem_proced_cat(cd_estabelecimento_p, ie_tipo_atend_w, ie_tipo_conv_w, cd_convenio_w, cd_categoria_w);

return	ie_origem_proced_w;

end obter_origem_proc_atend;
/