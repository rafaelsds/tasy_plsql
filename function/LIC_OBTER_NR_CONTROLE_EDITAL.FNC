create or replace
function lic_obter_nr_controle_edital(	nr_seq_licitacao_p		number)
 		    	return varchar2 is

nr_controle_interno_w			varchar2(100);			
			
begin

select	max(nr_controle_interno)
into	nr_controle_interno_w
from	reg_lic_edital
where	nr_seq_licitacao = nr_seq_licitacao_p;

return	nr_controle_interno_w;

end lic_obter_nr_controle_edital;
/