create or replace
function obter_legenda_isolamento(cd_tipo_isolamento_p	number)
 		    	return varchar2 is
			
ds_cor_w	varchar2(100);			

begin

SELECT 	ds_cor_ocupacao
into	ds_cor_w
FROM 	motivo_isolamento 
where 	nr_sequencia = cd_tipo_isolamento_p;

return	ds_cor_w;

end obter_legenda_isolamento;
/