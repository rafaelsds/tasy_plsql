create or replace
function spa_obter_dados_cheque_cr( 	nr_seq_cheque_p		number,
					ie_opcao_p 		varchar2)
					return varchar2 is
ds_retorno_w	varchar2(80) := '';

cd_banco_w		cheque_cr.cd_banco%type;
cd_agencia_bancaria_w	cheque_cr.cd_agencia_bancaria%type;
nr_cheque_w		cheque_cr.nr_cheque%type;
vl_cheque_w		cheque_cr.vl_cheque%type;
nr_conta_w		cheque_cr.nr_conta%type;
ds_banco_w		varchar2(80);

/*
ie_opcao_p
CB 	- CODIGO DO BANCO
DB 	- DESCRICAO DO BANCO
CA 	- CODIGO AGENCIA BANCARIA
C 	- CHEQUE
VC 	- VALOR DO CHEQUE
NC 	- NUMERO DA CONTA
*/

begin

if (nvl(nr_seq_cheque_p,0) > 0) then
	begin
		select 	cd_banco,
			cd_agencia_bancaria, 
			nr_cheque, 
			vl_cheque, 
			nr_conta
		into	cd_banco_w,		
			cd_agencia_bancaria_w,
			nr_cheque_w,		
			vl_cheque_w,
			nr_conta_w
		from   cheque_cr
		where  nr_seq_cheque = nr_seq_cheque_p;
	exception 
	when others then
		return	ds_retorno_w;
	end;

	if (ie_opcao_p = 'CB') then
		ds_retorno_w := cd_banco_w;
	elsif (ie_opcao_p = 'DB') then
		ds_retorno_w := substr(obter_nome_banco(cd_banco_w),1,80);
	elsif (ie_opcao_p = 'CA') then
		ds_retorno_w := cd_agencia_bancaria_w;
	elsif (ie_opcao_p = 'C') then
		ds_retorno_w := nr_cheque_w;
	elsif (ie_opcao_p = 'VC') then
		ds_retorno_w := vl_cheque_w;
	elsif (ie_opcao_p = 'NC') then
		ds_retorno_w := nr_conta_w;
	end if;	
end if;

return	ds_retorno_w;

end spa_obter_dados_cheque_cr;
/