create or replace
function obter_status_anterior_agenda (	nr_seq_agenda_p		number,
					ie_status_agenda_p	varchar)
					return varchar is
					
dt_acao_w		date;
ie_status_agenda_w	varchar2(15);
ie_status_agenda_ww	varchar2(15) := null;

cursor	c01 is					
	select	dt_acao,
		ie_status_agenda
	from	agenda_historico_acao
	where	nr_seq_agenda = nr_seq_agenda_p
	and	ie_status_agenda is not null
	and	dt_acao < (	select	max(dt_acao)
				from	agenda_historico_acao
				where	nr_seq_agenda		= nr_seq_agenda_p
				and	ie_status_agenda	= ie_status_agenda_p)
	order by dt_acao;					

begin

open c01;
loop
fetch c01 into
	dt_acao_w,
	ie_status_agenda_w;
exit when c01%notfound;
	begin
	ie_status_agenda_ww := ie_status_agenda_w;
	end;
end loop;
close c01;

return	ie_status_agenda_ww;

end obter_status_anterior_agenda;
/