create or replace
function hsl_obter_medicos_adic_ag_crm(	nr_seq_agenda_p	number)
					return varchar2 is

crm_w			varchar2(4000);
cd_crm_w		varchar2(260);
nr_ordem_w		number(10,0);

cursor	c01 is
select	2 ordem,
	substr(obter_nome_medico(a.cd_medico, 'CRM'),1,254)
from	proc_interno b,
	agenda_paciente_proc a
where	a.nr_seq_proc_interno = b.nr_sequencia
and	a.nr_sequencia = nr_seq_agenda_p;

begin
if	(nr_seq_agenda_p is not null) then
	open c01;
	loop
	fetch c01	into
			nr_ordem_w,
			cd_crm_w;
	exit when c01%notfound;
		begin
		if	(nvl(length(crm_w),0) < 3930) then
			crm_w := crm_w || cd_crm_w || ' / ';
		end if;
		end;
	end loop;
	close c01;
end if;

return crm_w;

end hsl_obter_medicos_adic_ag_crm;
/