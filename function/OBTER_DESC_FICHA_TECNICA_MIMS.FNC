create or replace function 
Obter_desc_ficha_tecnica_mims(nr_seq_ficha_p	Number)

		      return		varchar2 is

		      ds_retorno_w	varchar2(80);
begin
if (nr_seq_ficha_p > 0) then
	select	substr(ds_substancia,1,80)
	into	ds_retorno_w
	from	imp_medic_ficha_tecnica
	where	nr_sequencia = nr_seq_ficha_p;
end if;

	return ds_retorno_w;

end Obter_desc_ficha_tecnica_mims;
/