create or replace
function ageint_obter_se_exam_adic(nr_seq_ageint_p	number,
				   nr_seq_item_p	number)
 		    	return varchar2 is

ie_existe_adic_w	varchar2(1);			
			
begin

select	decode(count(*),0,'N','S')
into	ie_existe_adic_w
from	agenda_integrada_item a,
	ageint_exame_adic_item b
where	b.nr_seq_item		= a.nr_sequencia
and	a.nr_seq_agenda_int	= nr_seq_ageint_p
and	a.nr_sequencia		= nr_seq_item_p;

return	ie_existe_adic_w;

end ageint_obter_se_exam_adic;
/