CREATE OR REPLACE
FUNCTION obter_saldo_movto_bco_pend(	nr_seq_movto_pend_p	number,
					dt_referencia_p		date)
					RETURN			number IS

vl_credito_w	number(15,2);
vl_baixa_w	number(15,2);
vl_saldo_w	number(15,2);

begin

select	nvl(max(a.vl_credito),0)
into	vl_credito_w
from	movto_banco_pend a
where	a.nr_sequencia	= nr_seq_movto_pend_p;

select	nvl(sum(a.vl_baixa),0)
into	vl_baixa_w
from	movto_banco_pend_baixa a
where	a.nr_seq_movto_pend	= nr_seq_movto_pend_p
and	a.dt_baixa		<= fim_dia(dt_referencia_p);

vl_saldo_w	:= nvl(vl_credito_w,0) - nvl(vl_baixa_w,0);

return vl_saldo_w;

END obter_saldo_movto_bco_pend;
/