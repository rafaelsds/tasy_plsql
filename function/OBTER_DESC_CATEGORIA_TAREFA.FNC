create or replace
function obter_desc_categoria_tarefa
			(	nr_sequencia_p		Number)
 		    		return Varchar2 is

ds_retorno_w			Varchar2(80);

begin

select	max(ds_categoria)
into	ds_retorno_w
from	categoria_tarefa
where	nr_sequencia	= nr_sequencia_p;

return	ds_retorno_w;

end obter_desc_categoria_tarefa;
/