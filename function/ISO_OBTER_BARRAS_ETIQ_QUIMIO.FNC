create or replace
function ISO_obter_barras_etiq_quimio (nr_sequencia_p		number)
 		    	return number is

nr_barras_w	number(10);
begin
	select	to_number(to_char(c.nr_sequencia) || to_char(c.nr_seq_digito))
	into	nr_barras_w
	from	prescr_mat_hor c
	where	c.nr_sequencia 	= 	(select	min(c.nr_sequencia)
					 from	prescr_material b,
						prescr_mat_hor c
					 where	b.nr_prescricao 	= c.nr_prescricao
					 and	b.nr_sequencia 		= c.nr_seq_material
					 and	b.nr_seq_ordem_prod	= nr_sequencia_p);

return	nr_barras_w;

end ISO_obter_barras_etiq_quimio;
/