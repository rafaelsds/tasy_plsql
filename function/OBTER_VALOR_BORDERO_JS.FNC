create or replace
function obter_valor_bordero_js(
		nr_seq_conv_receb_p	number,
		nr_bordero_rec_p	number)
 		return varchar2 is

ds_retorno_w		varchar2(1) := 'N';
qt_convenio_w		number(10,0);
dt_recebimento_w	date;
qt_registro_w		number(10,0);

begin

select	count(*)
into	qt_registro_w
from	bordero_recebimento a
where	a.nr_bordero   = nr_bordero_rec_p
and	a.dt_recebimento is not null;

if	(qt_registro_w is not null) then
	begin
	select	max(a.dt_recebimento)
	into	dt_recebimento_w
	from	bordero_recebimento a
	where	a.nr_bordero   = nr_bordero_rec_p;
	end;
end if;

select	count(*)
into	qt_convenio_w
from	convenio_ret_receb a
where	a.nr_seq_receb = nr_seq_conv_receb_p;

if	(dt_recebimento_w is null) and
	(qt_convenio_w = 0) then
	begin
	ds_retorno_w := 'S';
	end;
end if;

return	ds_retorno_w;
end obter_valor_bordero_js;
/