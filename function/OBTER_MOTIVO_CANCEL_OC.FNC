CREATE OR REPLACE
function	obter_motivo_cancel_oc(nr_sequencia_p number)
          	return varchar2 is
ds_motivo_w 	varchar(80);
BEGIN
	select	ds_motivo
	into	ds_motivo_w
	from	motivo_cancel_sc_oc
	where	nr_sequencia = nr_sequencia_p;
	return	ds_motivo_w;
END	obter_motivo_cancel_oc;
/