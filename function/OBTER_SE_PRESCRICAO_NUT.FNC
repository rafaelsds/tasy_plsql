create or replace
function obter_se_prescricao_nut(nr_prescricao_p	number)
						return char is
ie_retorno_w	char(1)	:= 'N';
			
begin

if (nr_prescricao_p <> 0) then
	select 	nvl(max('S'),'N')
	into	ie_retorno_w
	from	nut_atend_serv_dia_rep
	where	rownum = 1
	and		((nr_prescr_oral = nr_prescricao_p) or
			 (nr_prescr_jejum = nr_prescricao_p) or
			 (nr_prescr_compl = nr_prescricao_p) or
			 (nr_prescr_enteral = nr_prescricao_p) or
			 (nr_prescr_npt_adulta = nr_prescricao_p) or
			 (nr_prescr_npt_neo = nr_prescricao_p) or
			 (nr_prescr_leite_deriv = nr_prescricao_p) or
			 (nr_prescr_npt_ped = nr_prescricao_p));
end if;

return	ie_retorno_w;

end obter_se_prescricao_nut;
/