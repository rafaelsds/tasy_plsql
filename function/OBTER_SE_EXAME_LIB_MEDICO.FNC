create or replace
function Obter_se_exame_lib_medico (	cd_pessoa_fisica_p		varchar2,
										nr_seq_exame_rotina_p	number,
										nr_seq_proc_rotina_p	number)
 		    	return varchar2 is

ie_exibir_w		char(1) := 'S';
			
begin

select	nvl(max('N'),'S')
into	ie_exibir_w
from	regra_exame_rotina_medico
where	rownum = 1
and		cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(ie_exibir_w = 'N') then
	if	(nr_seq_exame_rotina_p > 0) then
		select	nvl(max('N'),'S')
		into	ie_exibir_w
		from	regra_exame_rotina_medico
		where	rownum = 1
		and		cd_pessoa_fisica	= cd_pessoa_fisica_p
		and		nr_seq_exame is not null;
		
		if	(ie_exibir_w = 'N') then
			select	nvl(max('S'),'N')
			into	ie_exibir_w
			from	regra_exame_rotina_medico
			where	rownum = 1
			and		cd_pessoa_fisica	= cd_pessoa_fisica_p
			and		nr_seq_exame	= nr_seq_exame_rotina_p;
		end if;
	elsif	(nr_seq_proc_rotina_p > 0) then
		select	nvl(max('N'),'S')
		into	ie_exibir_w
		from	regra_exame_rotina_medico
		where	rownum = 1
		and		cd_pessoa_fisica	= cd_pessoa_fisica_p
		and		NR_SEQ_PROC_ROTINA is not null;
		
		if	(ie_exibir_w = 'N') then
			select	nvl(max('S'),'N')
			into	ie_exibir_w
			from	regra_exame_rotina_medico
			where	rownum = 1
			and		cd_pessoa_fisica		= cd_pessoa_fisica_p
			and		NR_SEQ_PROC_ROTINA	= nr_seq_proc_rotina_p;
		end if;
	end if;
	
end if;

return	ie_exibir_w;

end Obter_se_exame_lib_medico;
/