create or replace
function obter_ie_questionar_exame(nr_seq_exame_p	number)
 		    	return varchar2 is

nr_seq_exame_w		number(10);
ie_questionar_exame_w	varchar2(1) := 'N';
			
begin

	select	max(nr_seq_exame) 
	into	nr_seq_exame_w
	from	exame_laboratorio
	where	nr_seq_exame = nr_seq_exame_p;
	
	select	nvl(max(ie_questionar_exame),'N')
	into	ie_questionar_exame_w
	from	exame_laboratorio
	where nr_seq_exame = nr_seq_exame_w;

return	ie_questionar_exame_w;

end obter_ie_questionar_exame;
/
