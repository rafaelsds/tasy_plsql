create or replace
function HCVSP_Obter_valor_recebido (nr_interno_conta_p	Number)
					return Number is

vl_recebido_w		Number(15,2):= 0;
vl_recebido_tit_w	Number(15,2):= 0;
nr_titulo_w		NUMBER(10);


cursor c01 is
	SELECT	/*+ index (a titrece_conpaci_fk_i) */
		nr_titulo
	FROM	titulo_receber a
	WHERE	(nr_interno_conta_p <> 0)
	AND	(nr_interno_conta = nr_interno_conta_p);

begin

OPEN c01;
LOOP
FETCH c01 INTO 
	nr_titulo_w;
EXIT WHEN c01%NOTFOUND;
	begin
	vl_recebido_tit_w:= 0;

	select	nvl(sum(vl_recebido),0)
	into	vl_recebido_tit_w
	from	titulo_receber_liq
	where	nr_titulo = nr_titulo_w
	and	cd_tipo_recebimento = 14;

	vl_recebido_w:= vl_recebido_w + vl_recebido_tit_w;
	end;
END LOOP;
CLOSE c01;

return vl_recebido_w;

end HCVSP_Obter_valor_recebido;
/
