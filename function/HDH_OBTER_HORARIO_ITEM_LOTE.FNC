CREATE OR REPLACE function hdh_obter_horario_item_lote(	nr_seq_lote_p	number,
							cd_material_p	number)
   					return varchar2 is

ds_horario_w			varchar2(15);
ds_horarios_w			varchar2(2000);

cursor C01 is
select x.ds_horario_h
from
(select	c.ds_horario ds_horario_h
from	prescr_mat_hor c,
	ap_lote_item b,
	ap_lote a
where	a.nr_sequencia 	= b.nr_seq_lote
and	b.nr_seq_mat_hor = c.nr_sequencia
and	a.nr_prescricao	= c.nr_prescricao
and	c.dt_suspensao is null
and	b.cd_material	= cd_material_p
and	a.nr_sequencia	= nr_seq_lote_p
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
order by c.nr_sequencia)x
group by x.ds_horario_h;

begin

if	(nr_seq_lote_p is not null) and
	(cd_material_p is not null) then
	open C01;
	loop
	fetch C01 into
		ds_horario_w;
	exit when C01%notfound;
		begin
		ds_horarios_w	:= ds_horarios_w || ' ' || ds_horario_w;
		end;
	end loop;
	close C01;

end if;

return	ds_horarios_w;

end hdh_obter_horario_item_lote;
/