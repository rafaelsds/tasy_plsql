create or replace
function obter_dados_seg_entrada_unica
			(	nr_seq_segurado_p	number,
				ie_opcao_p		varchar2,
				ie_tipo_atendimento_p 	number default null)
			return varchar2 is

ds_retorno_w		varchar2(255);
ie_abrangencia_w	varchar2(3);
nr_seq_plano_w		number(10);
ie_acomodacao_w		varchar2(1);
nr_seq_outorgante_w	number(10);
ie_tipo_segurado_w	varchar2(10);
nr_seq_congenere_w	number(10);
ie_tipo_inter_benef_w	varchar2(10);
nr_seq_contrato_w	number(10);
nr_seq_intercambio_w	number(10);
cd_estabelecimento_w	number(4);
ie_tipo_operacao_w	varchar2(3);
cd_cgc_estipulante_w	pessoa_juridica.cd_cgc%type;
nr_seq_plano_categ_w	pls_regra_convenio_categ.nr_seq_plano%type;
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type;

/*  ie_opcao_p
    CI  Cart�o de identifica��o (carteirinha)
    VC  Validade carteirinha
    IV  In�cio vig�ncia
    CV  Conv�nio
    CA  Categoria
    PLC Plano conv�nio
    ER  Empresa de refer�ncia(Estipulante PJ do contrato do benefici�rio)
*/

Cursor C01 is
	select	a.cd_categoria
	from	pls_regra_convenio_categ a
	where	(a.ie_abrangencia = ie_abrangencia_w or a.ie_abrangencia is null)
	and	(a.ie_tipo_operacao = ie_tipo_operacao_w or a.ie_tipo_operacao is null)
	and	(a.ie_tipo_segurado = ie_tipo_segurado_w or a.ie_tipo_segurado is null)
	and	(a.nr_seq_plano = nr_seq_plano_categ_w or a.nr_seq_plano is null)
	and	((nvl(a.ie_tipo_intercambio,'A') = ie_tipo_inter_benef_w) or (nvl(a.ie_tipo_intercambio,'A') = 'A'))
	and	(a.ie_tipo_atendimento = ie_tipo_atendimento_w or a.ie_tipo_atendimento is null)
	and	((a.nr_seq_sca is null) or (exists	(select	1
							from	pls_sca_vinculo	x
							where	x.nr_seq_segurado	= nr_seq_segurado_p
							and	a.nr_seq_sca		= x.nr_seq_plano
							and	sysdate between trunc(x.dt_inicio_vigencia,'dd') and fim_dia(x.dt_fim_vigencia)
							and	x.dt_liberacao is not null)))
	order by nvl(a.nr_seq_sca,0),
		nvl(a.nr_seq_plano,0),
		nvl(a.ie_tipo_atendimento,0),
		nvl(a.ie_abrangencia,' '),
		nvl(a.ie_tipo_operacao,' '),
		nvl(a.ie_tipo_segurado,' '),
		decode(nvl(a.ie_tipo_intercambio,'A'),'A',-1,1);

Cursor C02 is
	select	a.cd_plano
	from	pls_regra_convenio_produto a
	where	(a.ie_acomodacao = ie_acomodacao_w or a.ie_acomodacao is null)
	and	(a.nr_seq_plano = nr_seq_plano_w or a.nr_seq_plano is null)
	and	(a.ie_tipo_atendimento = ie_tipo_atendimento_w or a.ie_tipo_atendimento is null)
	and	((a.nr_seq_grupo_plano is null) or (exists (	select  1
								from	pls_preco_produto y,
									pls_preco_grupo_produto x
								where	y.nr_seq_grupo  = x.nr_sequencia
								and	x.nr_sequencia  = a.nr_seq_grupo_plano
								and	y.nr_seq_plano  = nr_seq_plano_w)))
	and ((a.nr_seq_categ_acomodacao is null) or (exists(	select  1
								from	pls_plano_acomodacao v,
									pls_plano w
								where	v.nr_seq_plano		= w.nr_sequencia
								and	v.nr_seq_categoria	= a.nr_seq_categ_acomodacao
								and	w.nr_sequencia		= nr_seq_plano_w)))
	and ((a.nr_seq_tipo_acomodacao is null) or ( exists(	select  1
								from	pls_plano_acomodacao v,
									pls_plano w
								where   v.nr_seq_plano		= w.nr_sequencia
								and v.nr_seq_tipo_acomodacao	= a.nr_seq_tipo_acomodacao
								and w.nr_sequencia		= nr_seq_plano_w)))
	and	((a.nr_seq_sca is null) or (exists	(select	1
							from	pls_sca_vinculo	x
							where	x.nr_seq_segurado	= nr_seq_segurado_p
							and	a.nr_seq_sca		= x.nr_seq_plano
							and	sysdate between trunc(x.dt_inicio_vigencia,'dd') and fim_dia(x.dt_fim_vigencia)
							and	x.dt_liberacao is not null)))
	order by nvl(a.nr_seq_sca,0),
		nvl(a.ie_tipo_atendimento,0),
		nvl(a.nr_seq_plano,0),
		nvl(a.nr_seq_grupo_plano,0),
		nvl(a.nr_seq_tipo_acomodacao,0),
		nvl(a.nr_seq_categ_acomodacao,0),
		nvl(a.ie_acomodacao,' ');

Cursor C03 is
	select	a.cd_convenio
	from	pls_regra_convenio a
	where	a.nr_seq_outorgante	= nr_seq_outorgante_w
	and	(a.ie_tipo_atendimento = ie_tipo_atendimento_w or a.ie_tipo_atendimento is null)
	and	(a.nr_seq_plano = nr_seq_plano_categ_w or a.nr_seq_plano is null)
	and	((a.nr_seq_sca is null) or (exists	(select	1
							from	pls_sca_vinculo	x
							where	x.nr_seq_segurado	= nr_seq_segurado_p
							and	a.nr_seq_sca		= x.nr_seq_plano
							and	sysdate between trunc(x.dt_inicio_vigencia,'dd') and fim_dia(x.dt_fim_vigencia)
							and	x.dt_liberacao is not null)))
	order by nvl(a.nr_seq_sca,0),
		 nvl(a.nr_seq_plano,0),
		 nvl(a.ie_tipo_atendimento,0);

begin
ds_retorno_w	:= '';

begin
select	nr_seq_contrato,
	nr_seq_intercambio,
	cd_estabelecimento,
	ie_tipo_segurado,
	nr_seq_congenere,
	nr_seq_plano
into	nr_seq_contrato_w,
	nr_seq_intercambio_w,
	cd_estabelecimento_w,
	ie_tipo_segurado_w,
	nr_seq_congenere_w,
	nr_seq_plano_categ_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

exception
when others then
	null;
end;

select	nvl(ie_tipo_atendimento_p, max(ie_tipo_atendimento))
into	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento = (select max(nr_atendimento)
			from	atendimento_paciente
			where	nr_seq_segurado = nr_seq_segurado_p);

if	(cd_estabelecimento_w is null) then
	if	(nr_seq_contrato_w is not null) then
		select	max(cd_estabelecimento)
		into	cd_estabelecimento_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;
	else
		select	max(cd_estabelecimento)
		into	cd_estabelecimento_w
		from	pls_intercambio
		where	nr_sequencia	= nr_seq_intercambio_w;
	end if;
end if;

if	(ie_opcao_p = 'CI') then /* Cart�o de identifica��o */
	select	max(b.cd_usuario_plano)
	into	ds_retorno_w
	from	pls_segurado_carteira b,
		pls_segurado a
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_p;
elsif	(ie_opcao_p = 'VC') then /* Validade cart�o de identifica��o */
	select	to_char(max(b.dt_validade_carteira), 'dd/mm/yyyy')
	into	ds_retorno_w
	from	pls_segurado_carteira b,
		pls_segurado a
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_p;
elsif	(ie_opcao_p = 'IV') then /* In�cio da vig�ncia */
	select	max(b.dt_inicio_vigencia)
	into	ds_retorno_w
	from	pls_segurado_carteira b,
		pls_segurado a
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_p;
elsif	(ie_opcao_p = 'CV') then /* Conv�nio */
	select	max(nr_sequencia)
	into	nr_seq_outorgante_w
	from	pls_outorgante
	where	cd_estabelecimento	= cd_estabelecimento_w;
	
	open C03;
	loop
	fetch C03 into
		ds_retorno_w;
	exit when C03%notfound;
	end loop;
	close C03;
elsif	(ie_opcao_p = 'CA') then /* Categoria */
	select	max(b.ie_abrangencia),
		max(b.ie_tipo_operacao)
	into	ie_abrangencia_w,
		ie_tipo_operacao_w
	from	pls_segurado a,
		pls_plano b
	where	a.nr_seq_plano	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_segurado_p;
	
	if	(nr_seq_congenere_w is not null) then
		ie_tipo_inter_benef_w	:= pls_obter_tipo_intercambio(nr_seq_congenere_w,cd_estabelecimento_w);
	end if;
	
	open C01;
	loop
	fetch C01 into
		ds_retorno_w;
	exit when C01%notfound;
	end loop;
	close C01;
elsif	(ie_opcao_p = 'PLC') then /* Plano conv�nio */
	select	max(b.nr_sequencia),
		max(b.ie_acomodacao)
	into	nr_seq_plano_w,
		ie_acomodacao_w
	from	pls_segurado a,
		pls_plano b
	where	a.nr_seq_plano	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_segurado_p;
	
	open C02;
	loop
	fetch C02 into
		ds_retorno_w;
	exit when C02%notfound;
	end loop;
	close C02;
elsif	(ie_opcao_p = 'ER') then /* Empresa */
	select	max(b.cd_cgc_estipulante)
	into	cd_cgc_estipulante_w
	from	pls_segurado a,
		pls_contrato b
	where	a.nr_seq_contrato	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_p;
	
	select	max(cd_empresa)
	into	ds_retorno_w
	from	empresa_referencia
	where	cd_cgc	= cd_cgc_estipulante_w
	and	nvl(ie_situacao,'A') = 'A';
end if;

return  ds_retorno_w;

end obter_dados_seg_entrada_unica;
/
