create or replace
function obter_se_inf_adic_diag_carta(nr_seq_modelo_p	number)
 		    	return varchar2 is
				
ds_retorno_w	varchar2(1);

begin

select	nvl(max('S'),'N')
into	ds_retorno_w
from	carta_medica_regra a,
		carta_medica_regra_item b,
		carta_medica_modelo c
where	a.nr_sequencia 	= b.nr_seq_regra
and		a.nr_seq_modelo 	= c.nr_sequencia
and   	c.nr_sequencia    = nr_seq_modelo_p
and		nvl(ie_incluir_inf_adic,'N') = 'S'
and		exists	(	select	1
					from	carta_medica_regra_item x
					where	x.nr_seq_regra 	= b.nr_seq_regra
					and	nvl(ie_incluir_inf_adic,'N') = 'S');


return	ds_retorno_w;

end obter_se_inf_adic_diag_carta;
/