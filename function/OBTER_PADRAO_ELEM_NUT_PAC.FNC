create or replace 
function Obter_padrao_elem_nut_pac	(
			nr_sequencia_p		number)
			return Varchar2 is


ds_retorno_w		Varchar2(60);
ds_padrao_w			Varchar2(20);

BEGIN

select	max(replace(qt_min,'.',',')||' - '||replace(qt_max,'.',',')),
		max(ds_padrao)
into	ds_retorno_w,
		ds_padrao_w
from	nut_elemento
where	nr_sequencia	= nr_sequencia_p;

if	(instr(ds_retorno_w,',') = 1) then
	ds_retorno_w	:= '0'||ds_retorno_w;
end if;

ds_retorno_w	:= replace(ds_retorno_w,' ,',' 0,');

if	(ds_padrao_w is not null) or
	(ds_padrao_w <> '') then
	ds_retorno_w	:= ds_padrao_w;
end if;

return ds_retorno_w;

end Obter_padrao_elem_nut_pac;
/