create or replace
function des_obter_pacote_os(	nr_sequencia_p	number,
				ie_opcao_p	number)
				return varchar2 is

ds_retorno_w			varchar2(255);
nr_seq_pacote_w			number(10);

begin

if	(ie_opcao_p = 1) then
	begin
	select	max(nr_seq_pacote)
	into	ds_retorno_w
	from	des_pacote_ordem_serv
	where	nr_seq_ordem_serv	= nr_sequencia_p;
	end;
elsif	(ie_opcao_p = 2) then
	begin
	select	max(nr_seq_pacote)
	into	nr_seq_pacote_w
	from	des_pacote_ordem_serv
	where	nr_seq_ordem_serv	= nr_sequencia_p;
	
	select	to_char(max(dt_entrega),'dd/mm/yyyy')
	into	ds_retorno_w
	from	des_pacote_versao
	where	nr_sequencia	= nr_seq_pacote_w;
	
	end;
end if;

return	ds_retorno_w;

end des_obter_pacote_os;
/