create or replace
function obter_nr_internado_agenda(	cd_pessoa_p			varchar2,
									dt_agenda_p			date default null,
									ie_opcao_p			varchar2 default 'N',
									ie_status_agenda_P	varchar2 default '')
 		    	return varchar2 is

ds_retorno_w 	varchar2(10) := '0';
ie_param_15_w	varchar2(1);	
dt_agenda_w 	date;
				
begin

if(dt_agenda_p is null) then

	
	SELECT MAX(dt_agenda) 
	into dt_agenda_w
	from agenda_hc_paciente
	where cd_pessoa_fisica = cd_pessoa_p
	and trunc(dt_agenda) = trunc(sysdate);
	
else 

	dt_agenda_w:= dt_agenda_p;
	
end if;

if (nvl(ie_opcao_p,'N') = 'PI') then

	obter_param_usuario(867,15,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_param_15_w);

	if ( ie_param_15_w = 'S') then
	
		select	count(*)
		into 	ds_retorno_w
		from    setor_atendimento c, 
				atendimento_paciente b,
				atend_paciente_unidade  a
		where   a.nr_atendimento        = b.nr_atendimento
		and     a.cd_setor_atendimento  = c.cd_setor_atendimento
		and     c.cd_classif_setor      in (3,4,8)
		and     b.cd_pessoa_fisica      = cd_pessoa_p
		and		trunc(nvl(dt_agenda_w,a.dt_entrada_unidade)) between trunc(a.dt_entrada_unidade) and trunc(nvl(a.dt_saida_unidade,dt_agenda_w));
		
	end if;



else

	select	count(*)
	into 	ds_retorno_w
	from    setor_atendimento c, 
			atendimento_paciente b,
			unidade_atendimento a
	where   a.nr_atendimento        = b.nr_atendimento
	and     a.cd_setor_atendimento  = c.cd_setor_atendimento
	and     c.cd_classif_setor      in (3,4,8)
	and     b.cd_pessoa_fisica      = cd_pessoa_p;

end if;

return	ds_retorno_w;

end obter_nr_internado_agenda;
/
