create or replace
function Obter_Medic_prescr_Atend_Ficha(	nr_atendimento_p		number)
 		    	return varchar2 is
ds_retorno_mat_w	varchar2(20000);
ds_retorno_sol_w	varchar2(20000);
ds_retorno_w		varchar2(20000);
ds_solucao_w		varchar2(20000);

ds_material_w	varchar2(20000);

Cursor C01 is
	select	distinct
		nvl(substr(obter_descricao_padrao('MEDIC_FICHA_TECNICA', 'DS_SUBSTANCIA',c.NR_SEQ_FICHA_TECNICA),1,80),obter_desc_mat_generico(a.cd_material))
	from	prescr_material a,
		prescr_medica b,
		material c
	where	a.nr_prescricao		= b.nr_prescricao
	and		c.cd_material = a.cd_material
	and	b.nr_Atendimento	= nr_atendimento_p
	and	nvl(b.dt_liberacao, b.dt_liberacao_medico) is not null
	and	a.ie_suspenso		= 'N'
	and	a.ie_agrupador		= 1
	and	b.IE_ORIGEM_INF		= 1
	and	a.ie_se_necessario = 'N'
	and	a.ie_acm = 'N'
	order by 1;
	
Cursor C02 is
	select	distinct
		a.DS_SOLUCAO
	from	prescr_solucao a,
		prescr_medica b
	where	a.nr_prescricao		= b.nr_prescricao
	and	b.nr_Atendimento	= nr_atendimento_p
	and	nvl(b.dt_liberacao, b.dt_liberacao_medico) is not null
	and	a.ie_suspenso		= 'N'
	and	a.DS_SOLUCAO is not null
	and	b.IE_ORIGEM_INF		= 1
	order by 1;
begin

open C01;
loop
fetch C01 into	
	ds_material_w;
exit when C01%notfound;
	begin
	if	(ds_retorno_mat_w	is null) then
		ds_retorno_mat_w	:= wheb_mensagem_pck.get_texto(308634) || ': '||	ds_material_w; -- Medic
	else
		ds_retorno_mat_w	:= ds_retorno_mat_w||', '||ds_material_w;
	end if;
	end;
end loop;
close C01;


open C02;
loop
fetch C02 into	
	ds_solucao_w;
exit when C02%notfound;
	begin
	if	(ds_retorno_sol_w	is null) then
		ds_retorno_sol_w	:= chr(13)||chr(10)|| wheb_mensagem_pck.get_texto(308635) || ': '|| ds_solucao_w; -- Soluc
	else
		ds_retorno_sol_w	:= ds_retorno_sol_w||', '||ds_solucao_w;
	end if;
	end;
end loop;
close C02;

ds_retorno_w	:= ds_retorno_mat_w;
if	(ds_retorno_sol_w is not null) then
	ds_retorno_w	:= ds_retorno_w||ds_retorno_sol_w;
end if;


return	substr(ds_retorno_w,1,4000);

end Obter_Medic_prescr_Atend_Ficha;
/
