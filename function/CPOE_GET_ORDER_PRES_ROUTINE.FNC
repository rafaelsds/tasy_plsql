create or replace
function cpoe_get_order_pres_routine(	
					cd_perfil_p			number,
					ie_tipo_p			varchar2) return number is
											
nr_seq_apresen_enteral_w		cpoe_presentation_routine.nr_seq_apresen_enteral%type;
nr_seq_apresen_esp_w			cpoe_presentation_routine.nr_seq_apresen_esp%type;
nr_seq_apresen_gas_w			cpoe_presentation_routine.nr_seq_apresen_gas%type;
nr_seq_apresen_ap_w			cpoe_presentation_routine.nr_seq_apresen_anat%type;
nr_seq_apresen_lab_w			cpoe_presentation_routine.nr_seq_apresen_lab%type;
nr_seq_apresen_medic_w			cpoe_presentation_routine.nr_seq_apresen_medic%type;
nr_seq_apresen_nao_lab_w		cpoe_presentation_routine.nr_seq_apresen_nao_lab%type;
nr_seq_apresen_oral_w			cpoe_presentation_routine.nr_seq_apresen_oral%type;
nr_seq_apresen_rec_w			cpoe_presentation_routine.nr_seq_apresen_rec%type;
nr_seq_apresen_sol_w			cpoe_presentation_routine.nr_seq_apresen_sol%type;
nr_seq_apresen_mat_w			cpoe_presentation_routine.nr_seq_apresen_mat%type;
nr_seq_apresen_sup_w			cpoe_presentation_routine.nr_seq_apresen_sup%type;
nr_seq_apresen_milk_w			cpoe_presentation_routine.nr_seq_apresen_milk%type;
nr_seq_apresen_resp_w			cpoe_presentation_routine.nr_seq_apresen_resp%type;
nr_sequencia_w					cpoe_presentation_rule.nr_sequencia%type;
nr_seq_apresent_w				number(15) := 9999999;

/* Opcoes disponiveis	Dominio =  7246 */
		
begin

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	(select nr_sequencia
	from	cpoe_presentation_rule
	where 	nvl(cd_perfil, nvl(cd_perfil_p,0)) = nvl(cd_perfil_p,0)
	and	nvl(cd_estabelecimento, nvl(wheb_usuario_pck.get_cd_estabelecimento,0)) = nvl(wheb_usuario_pck.get_cd_estabelecimento,0)
	order by cd_perfil, cd_estabelecimento)
where 	rownum = 1;

if (nr_sequencia_w > 0) then
	select	max(nvl(nr_seq_apresen_enteral,9999999)),
			max(nvl(nr_seq_apresen_esp,9999999)),
			max(nvl(nr_seq_apresen_gas,9999999)),
      max(nvl(nr_seq_apresen_anat,9999999)),
			max(nvl(nr_seq_apresen_lab,9999999)),
			max(nvl(nr_seq_apresen_medic,9999999)),
			max(nvl(nr_seq_apresen_nao_lab,9999999)),
			max(nvl(nr_seq_apresen_oral,9999999)),
			max(nvl(nr_seq_apresen_rec,9999999)),
			max(nvl(nr_seq_apresen_sol,9999999)),
			max(nvl(nr_seq_apresen_mat,9999999)),
			max(nvl(nr_seq_apresen_sup,9999999)),
			max(nvl(nr_seq_apresen_milk,9999999)),
			max(nvl(nr_seq_apresen_resp,9999999))
	into	nr_seq_apresen_enteral_w,
			nr_seq_apresen_esp_w,
			nr_seq_apresen_gas_w,
      nr_seq_apresen_ap_w,
			nr_seq_apresen_lab_w,
			nr_seq_apresen_medic_w,
			nr_seq_apresen_nao_lab_w,
			nr_seq_apresen_oral_w,
			nr_seq_apresen_rec_w,
			nr_seq_apresen_sol_w,
			nr_seq_apresen_mat_w,
			nr_seq_apresen_sup_w,
			nr_seq_apresen_milk_w,
			nr_seq_apresen_resp_w
	from	cpoe_presentation_routine
	where	nr_seq_rule = nr_sequencia_w;
	
	if (ie_tipo_p = 'DE') then
		return 	nr_seq_apresen_enteral_w;
	elsif (ie_tipo_p = 'DL') then
		return	nr_seq_apresen_milk_w;	
	elsif (ie_tipo_p = 'DO') then
		return	nr_seq_apresen_oral_w;
	elsif (ie_tipo_p = 'DS') then
		return	nr_seq_apresen_sup_w;
	elsif (ie_tipo_p = 'ES') then
		return 	nr_seq_apresen_esp_w;
	elsif (ie_tipo_p = 'EX') then
		return	nr_seq_apresen_lab_w;
	elsif (ie_tipo_p = 'G') then
		return	nr_seq_apresen_gas_w;
  elsif (ie_tipo_p = 'AP') then
		return	nr_seq_apresen_ap_w;
	elsif (ie_tipo_p = 'M') then
		return	nr_seq_apresen_medic_w;
	elsif (ie_tipo_p = 'P') then
		return	nr_seq_apresen_nao_lab_w;
	elsif (ie_tipo_p = 'R') then
		return	nr_seq_apresen_rec_w;
	elsif (ie_tipo_p = 'S') then
		return	nr_seq_apresen_sol_w;
	elsif (ie_tipo_p = 'MA') then
		return	nr_seq_apresen_mat_w;
	elsif (ie_tipo_p = 'TR') then
		return	nr_seq_apresen_resp_w;
	end if;

end if;

return	nr_seq_apresent_w;

end cpoe_get_order_pres_routine;
/
