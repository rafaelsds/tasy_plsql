create or replace 
function EHR_Obter_se_Tipo_Reg_Lib (	nr_seq_tipo_reg_p	number,
					cd_perfil_p		number,
					cd_especialidade_p	number,
					cd_pessoa_fisica_p	varchar2)
					return	varchar2 is

qt_regra_w		number(10);
ds_retorno_w		varchar2(1);
ie_liberar_w		varchar2(1);

Cursor C01 is
	select	ie_liberar
	into	ie_liberar_w
	from	ehr_tipo_reg_lib
	where	nr_seq_tipo_reg	= nr_seq_tipo_reg_p
	and	nvl(cd_perfil,nvl(cd_perfil_p,0))		= nvl(cd_perfil_p,0)
	--and	nvl(cd_especialidade,nvl(cd_especialidade_p,0))	= nvl(cd_especialidade_p,0)
	and	nvl(cd_pessoa_fisica,nvl(cd_pessoa_fisica_p,0))	= nvl(cd_pessoa_fisica_p,0)
	and	((cd_especialidade is null) or (obter_se_especialidade_medico(cd_pessoa_fisica_p,cd_especialidade)	= 'S'))
	order by nvl(cd_pessoa_fisica,0),
		nvl(cd_especialidade,0),
		nvl(cd_perfil,0);

begin

select	count(*)
into	qt_regra_w
from	ehr_tipo_reg_lib
where	nr_seq_tipo_reg	= nr_seq_tipo_reg_p;

if	(qt_regra_w = 0) then
	begin
	ds_retorno_w	:= 'S';
	end;
else
	begin
	ds_retorno_w	:= 'N';
	open C01;
	loop
	fetch C01 into	
		ie_liberar_w;
	exit when C01%notfound;
		begin
		ds_retorno_w	:= ie_liberar_w;
		end;
	end loop;
	close C01;
	end;
end if;

return ds_retorno_w;
	
end EHR_Obter_se_Tipo_Reg_Lib;
/
