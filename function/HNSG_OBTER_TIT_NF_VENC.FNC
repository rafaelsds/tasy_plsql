create or replace
function HNSG_OBTER_TIT_NF_VENC	(nr_seq_nota_fiscal_p	number,
									dt_vencimento_p			date,
									nr_interno_conta_p		number)	return varchar2 is

nr_titulo_w	number(10,0);
nr_nota_fiscal_w	varchar2(255);

begin

select	nr_nota_fiscal
into	nr_nota_fiscal_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nota_fiscal_p;

nr_titulo_w	:= null;

/* Primeiro busca nos t�tulos a pagar */
select	max(nr_titulo)
into	nr_titulo_w
from	titulo_pagar
where	nr_seq_nota_fiscal		= nr_seq_nota_fiscal_p
and	trunc(dt_vencimento_original, 'dd')	= trunc(dt_vencimento_p, 'dd');


/* Se n�o achou procura nos t�tulos a receber pela sequencia da nota */
if	(nr_titulo_w is null) then

	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_seq_nf_saida			= nr_seq_nota_fiscal_p
	and	trunc(dt_vencimento, 'dd')	= trunc(dt_vencimento_p, 'dd');

	/* Se n�o achou, busca ent�o pelo n�mero da nota */
	if	(nr_titulo_w is null) then

		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_nota_fiscal			= nr_nota_fiscal_w
		and	nr_nota_fiscal is not null
		and	nr_seq_nf_saida is null
		and	trunc(dt_vencimento, 'dd')	= trunc(dt_vencimento_p, 'dd');
		
		/* Se n�o achou, busca pelo nr. da conta paciente, em casos do titulo ter sido gerado direto pela mesma */
		if	(nr_titulo_w is null) then
		
			select	max(nr_titulo)
			into	nr_titulo_w
			from	titulo_receber
			where	nr_interno_conta = nr_interno_conta_p
			and		nr_interno_conta is not null;
		
		end if;

	end if;

end if;

return	nr_titulo_w;

end HNSG_OBTER_TIT_NF_VENC;
/