create or replace
function rim_obter_dif_rpa( 
					ie_opcao_p number,
					ie_mes_p varchar2,
					ie_tipo_p varchar2,
					dt_inicial_p date,
					dt_final_p date)
					return number is
/*
D = Diferenša de datas;
P = Qtde pacientes
*/					

qt_retorno_w	number(10,2);							
							
begin	
		
	if (ie_tipo_p = 'D') then
		select  round(sum((dt_saida_recup - dt_saida_recup_prev)*1440),2)
		into 	qt_retorno_w
		from	cirurgia
		where	dt_saida_recup is not null
		and	dt_saida_recup_prev is not null
		and	to_char(trunc(dt_saida_recup_prev,'month'),'mm') = ie_mes_p
		and	(((ie_opcao_p = 1) and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) >= 0) 
			              and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) <= 60))
			  or ((ie_opcao_p = 2) and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) >= 61) 
			              and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) <= 120))
			  or ((ie_opcao_p = 3) and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) >= 121))
		   	  or (ie_opcao_p = 4))
		and	  dt_inicio_prevista between dt_inicial_p and fim_dia(dt_final_p);
	elsif (ie_tipo_p = 'P') then
		select  count(*)
		into	qt_retorno_w
		from	cirurgia
		where	dt_saida_recup is not null
		and	dt_saida_recup_prev is not null
		and	to_char(trunc(dt_saida_recup_prev,'month'),'mm') = ie_mes_p
		and	(((ie_opcao_p = 1) and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) >= 0) 
		                              and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) <= 60))
		                  or ((ie_opcao_p = 2) and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) >= 61) 
			               and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) <= 120))
		                  or ((ie_opcao_p = 3) and (round(((dt_saida_recup - dt_saida_recup_prev)*1440),2) >= 121))
			  or (ie_opcao_p = 4))
		and	dt_inicio_prevista between dt_inicial_p and fim_dia(dt_final_p);
	end if;
	
		return	qt_retorno_w;
		
end	rim_obter_dif_rpa;
/