create or replace
function Obter_justificativa_log(nr_seq_justificativa_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(60);
			
begin

select	max(ds_justificativa)
into	ds_retorno_w
from	tasy_log_justificativa
where	nr_sequencia = nr_seq_justificativa_p;

return	ds_retorno_w;

end Obter_justificativa_log;
/