create or replace
function obter_nome_pf_oculta(
			cd_pessoa_fisica_p	varchar2,
			cd_perfil_p			number,
			nm_usuario_p		varchar2,
			nm_pessoa_fisica_p	varchar2 default null) 
			return varchar2 is

nm_pessoa_w				varchar2(255);
nr_seq_person_name_w	person_name.nr_sequencia%type;

begin
select	max(nm_oculto),
		max(nr_seq_person_name)
into	nm_pessoa_w,
		nr_seq_person_name_w
from	pessoa_fisica_oculta
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and		(cd_perfil = cd_perfil_p
		or	cd_perfil is null)
order by
	nvl(cd_perfil,0);

if (nr_seq_person_name_w is not null) then
	begin
	nm_pessoa_w := substr(pkg_name_utils.get_person_name(nr_seq_person_name_w,null,'full','social,main'),0,255);
	end;
elsif (nm_pessoa_w is null) then
	begin
	if	(nm_pessoa_fisica_p is not null) then
		begin
		nm_pessoa_w := nm_pessoa_fisica_p;
		end;
	else
		begin
		nm_pessoa_w := substr(obter_nome_pf(cd_pessoa_fisica_p), 0, 255);
		end;
	end if;
	end;	
end if;
return nm_pessoa_w;
end	obter_nome_pf_oculta;
/