create or replace
function sup_adiciona_rtf_texto(ds_texto_p		varchar2)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(4000);

begin

if	(instr(ds_texto_p,'\rtf') = 0) then
	ds_retorno_w := '{\rtf1\ansi\deff0{\fonttbl{\f0\fswiss\fcharset0 Arial;}}{\colortbl ;\red0\green0\blue128;}\viewkind4\uc1\pard\cf1\lang1033\f0\fs20' || ds_texto_p || '\par }';
else
	ds_retorno_w := ds_texto_p;
end if;

return	ds_retorno_w;

end sup_adiciona_rtf_texto;
/
