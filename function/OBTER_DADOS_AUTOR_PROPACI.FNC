create or replace
function OBTER_DADOS_AUTOR_PROPACI
		(nr_sequencia_p		number,
		ie_opcao_p		varchar2) return varchar2 is

/*
IE_OPCAO_P
1 - Retorna mensagem de consistencia
2 - Retorna qual a regra
*/


nr_atendimento_w	number(10,0);
cd_convenio_w		number(10,0);
dt_procedimento_w	date;
cd_procedimento_w	number(15,0);
ie_tipo_atendimento_w	number(3,0);
ie_origem_proced_w	number(10,0);
cd_setor_atendimento_w	number(10,0);
qt_procedimento_w	number(15,2);
cd_plano_w		varchar2(10) := '';
ds_erro_w		varchar2(255) := '';
ds_retorno_w		varchar2(255) := '';
ie_regra_w		varchar2(10) := null;
nr_seq_regra_retorno_w	number(10);
nr_seq_proc_interno_w	number(10);
cd_categoria_w		varchar2(10);
cd_estabelecimento_w	number(4);
cd_medico_exec_w	varchar2(10);
ie_glosa_w			regra_ajuste_proc.ie_glosa%type;
nr_seq_regra_preco_w		regra_ajuste_proc.nr_sequencia%type;
begin

begin
select	a.nr_atendimento,
	a.cd_convenio,
	a.dt_procedimento,	
	a.cd_procedimento,
	a.ie_origem_proced,
	a.qt_procedimento,
	b.ie_tipo_atendimento,
	obter_plano_convenio_atend(b.nr_atendimento,'C'),
	a.cd_setor_atendimento,
	a.nr_seq_proc_interno,
	substr(obter_categoria_atendimento(b.nr_atendimento),1,10),
	b.cd_estabelecimento,
	a.cd_medico_executor
into	nr_atendimento_w,
	cd_convenio_w,
	dt_procedimento_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_procedimento_w,
	ie_tipo_atendimento_w,
	cd_plano_w,
	cd_setor_atendimento_w,
	nr_seq_proc_interno_w,
	cd_categoria_w,
	cd_estabelecimento_w,
	cd_medico_exec_w
from	atendimento_paciente b,
	procedimento_paciente a
where	a.nr_atendimento	= b.nr_atendimento
and	a.nr_sequencia		= nr_sequencia_p;


consiste_plano_convenio
		(nr_atendimento_w,
		cd_convenio_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		dt_procedimento_w,
		0,/*qt_procedimento_w,  -- Francisco - 10/10/08 Troquei por 0 por causa da consiste_autorizacao_proc */
		ie_tipo_atendimento_w,
		cd_plano_w,
		null,
		ds_erro_w,
		cd_setor_atendimento_w,
		null,
		ie_regra_w,
		null,
		nr_seq_regra_retorno_w,
		nr_seq_proc_interno_w,
		cd_categoria_w,
		cd_estabelecimento_w,
		null,
		cd_medico_exec_w,
		'',
		ie_glosa_w,
		nr_seq_regra_preco_w);

exception
	when others then
		ds_erro_w		:= null;
		ie_regra_w		:= null;
end;

if	(ie_opcao_p = '1') then
	if	(ie_regra_w = 6) then	-- se consiste na conta
		ds_retorno_w		:= ds_erro_w;
	end if;
elsif	(ie_opcao_p = '2') then
	ds_retorno_w	:= ie_regra_w;
end if;

return	ds_retorno_w;

end OBTER_DADOS_AUTOR_PROPACI;
/
