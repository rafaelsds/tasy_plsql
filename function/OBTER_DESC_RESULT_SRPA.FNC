create or replace 
FUNCTION Obter_desc_result_srpa(	nr_sequencia_p	number)
			return varchar2 is

ds_retorno_w	varchar2(120);

BEGIN

select	ds_resultado
into	ds_retorno_w
from	srpa_item_result
where	nr_sequencia = nr_sequencia_p;

return ds_retorno_w;

END Obter_desc_result_srpa;
/
