create or replace
function obter_se_os_manchester return varchar2 is

ie_teste_negocio_w	varchar2(1 char);
nr_seq_ordem_serv_w	man_ordem_servico.nr_sequencia %type;

begin
	nr_seq_ordem_serv_w := reg_object_log_pck.get_so;

	select	nvl2(max(dt_teste_negocio),'S','N')
	into	ie_teste_negocio_w
	from	man_ordem_servico
	where	nr_sequencia = nr_seq_ordem_serv_w
	and		nr_seq_estagio <> 2
	and		dt_teste_negocio is not null;
	
	return ie_teste_negocio_w;
	
end obter_se_os_manchester;
/