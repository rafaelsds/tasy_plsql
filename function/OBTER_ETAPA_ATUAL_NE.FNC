create or replace
function Obter_Etapa_Atual_NE(	nr_prescricao_p		Number,
				nr_seq_solucao_p	Number)
				return Number is
				
nr_etapa_sol_w		Number(15);

begin

select	nvl(max(nr_etapa_sol),0)
into	nr_etapa_sol_w
from	prescr_mat_hor
where	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S'
and	dt_inicio_horario is not null
and	ie_agrupador = 8
and	nr_seq_material = nr_seq_solucao_p
and	nr_prescricao = nr_prescricao_p;

if	(nr_etapa_sol_w = 0) then
	select	nvl(min(nr_etapa_sol),0)
	into	nr_etapa_sol_w
	from	prescr_mat_hor
	where	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S'
	and	dt_suspensao is null
	and	ie_agrupador = 8
	and	nr_seq_material = nr_seq_solucao_p
	and	nr_prescricao = nr_prescricao_p;
end if;

return	nr_etapa_sol_w;

end Obter_Etapa_Atual_NE;
/
