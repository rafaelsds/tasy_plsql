create or replace
function ageint_obter_orient_exame_age	(cd_procedimento_p		number,
					ie_origem_proced_p		number,
					cd_convenio_p			number,
					nr_seq_proc_interno_p		number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number,
					cd_funcao_p			number)
					return varchar2 is

ds_orientacao_w	varchar2(10000) := '';
ds_orient_convenio_w	varchar2(2000) := '';
ds_orientacao_usuario_w	varchar2(4000) := '';
ie_orientacao_w		varchar2(01);
ie_tipo_orientacao_w	varchar2(02);
ie_forma_orientacao_w	varchar2(1);

begin
select	max(ds_orientacao)
into	ds_orientacao_w
from	procedimento
where	cd_procedimento = cd_procedimento_p
and	ie_origem_proced = ie_origem_proced_p;


Obter_Param_Usuario(820,67,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_tipo_orientacao_w);
Obter_Param_Usuario(869,106,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_forma_orientacao_w);

if	(nvl(nr_seq_proc_interno_p,0) > 0) then
	select	max(ie_orientacao),
		max(ds_orientacao_pac),
		max(ds_orientacao_usuario)
	into	ie_orientacao_w,
		ds_orientacao_w,
		ds_orientacao_usuario_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_p;

	select	max(ds_orientacao)
	into	ds_orient_convenio_w
	from	proc_interno_conv_orient
	where	nr_seq_proc_interno = nr_seq_proc_interno_p
	and	((cd_convenio = cd_convenio_p) or (cd_convenio is null));

	--n_error(-20011,cd_funcao_w || '#@#@');
	if	(ie_forma_orientacao_w	= 'S') then
		if	(ie_orientacao_w = 'C') then
			ds_orientacao_w := ds_orient_convenio_w;
		elsif	(ie_orientacao_w = 'A') and ((ie_tipo_orientacao_w = 'P') or (cd_funcao_p <> 820)) then
			ds_orientacao_w := ds_orientacao_w || chr(13) || chr(10) || ds_orient_convenio_w;
		elsif	(ie_orientacao_w = 'A') and (ie_tipo_orientacao_w = 'U') and (cd_funcao_p = 820) then
			ds_orientacao_w := ds_orientacao_usuario_w || chr(13) || chr(10) || ds_orient_convenio_w;
		elsif	(ie_tipo_orientacao_w = 'U') and (cd_funcao_p = 820) then
			ds_orientacao_w := ds_orientacao_usuario_w;
		elsif	(ie_orientacao_w = 'A') and (ie_tipo_orientacao_w = 'PU') and (cd_funcao_p = 820) then
			ds_orientacao_w := ds_orientacao_w || chr(13) || chr(10) || ds_orient_convenio_w || chr(13) || chr(10) ||ds_orientacao_usuario_w;
		elsif	(ie_tipo_orientacao_w = 'PU') and (cd_funcao_p = 820) then
			ds_orientacao_w := ds_orientacao_w || chr(13) || chr(10) || ds_orientacao_usuario_w;
		end if;
	else
		ds_orientacao_w	:= '';
	end if;
end if;

return substr(ds_orientacao_w,1,4000);

end ageint_obter_orient_exame_age;
/
