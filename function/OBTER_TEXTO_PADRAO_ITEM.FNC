create or replace
function	obter_texto_padrao_item(nr_seq_item_p		number,
					ie_opcao_p		varchar2,
					cd_estabelecimento_p	number default null,
					ie_evolucao_p		varchar2 default null,
					ie_justificativa_p	varchar2 default null,
					cd_especialidade_p	number default null,
					ie_sem_especialidade_p	varchar2 default null
					) return varchar2 is
/*
C - C�digo
*/

nr_sequencia_w		number(10);
cd_perfil_ativo_w	number;

begin

cd_perfil_ativo_w	:= obter_perfil_ativo;

if	(nr_seq_item_p is not null) then
	
	select	max(nvl(nr_sequencia,0))
	into	nr_sequencia_w
	from	texto_padrao
	where	nr_seq_item_pront = nr_seq_item_p
	and	ie_opcao_padrao = 'S'
	and	((nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p) or cd_estabelecimento_p is null)
	and	((nvl(ie_evolucao_clinica,ie_evolucao_p) = ie_evolucao_p) or ie_evolucao_p is null)
	and	((nvl(ie_tipo_justificativa,ie_justificativa_p) = ie_justificativa_p) or ie_justificativa_p is null)
	and	((cd_especialidade = cd_especialidade_p) or ((cd_especialidade is null) and (ie_sem_especialidade_p  = 'S')) or (nvl(cd_especialidade_p,0) = 0))
	and	(Obter_Se_Texto_Padrao_lib(nr_sequencia) = 'S')
	and	(nvl(cd_perfil,cd_perfil_ativo_w) = cd_perfil_ativo_w);
	
end if;

if	(ie_opcao_p = 'C') then

	return nr_sequencia_w;

end if;
	
end obter_texto_padrao_item;
/
