create or replace
function obter_regra_disp_pepo	(	ie_tipo_prescricao_p	number,
					cd_material_p		number
				)
				return varchar2 is
					
					
ie_possui_regra_w	varchar2(1):= 'N';
ie_dispensa_w		varchar2(1):= 'S';
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
ie_tipo_prescricao_w	number(3);
ie_tipo_prescr_w	number(1);
ie_tipo_prescr_ww	number(1);

cursor c01 is
	select	a.ie_tipo_prescr
	from	regra_ajuste_disp_pepo a,
		regra_ajuste_disp_mat b
	where	a.nr_sequencia 		  				= b.nr_seq_regra
	and	nvl(b.cd_material,cd_material_p) 			= cd_material_p
	and	nvl(b.cd_grupo_material,cd_grupo_material_w) 		= cd_grupo_material_w
	and	nvl(b.cd_subgrupo_material,cd_subgrupo_material_w) 	= cd_subgrupo_material_w
	and	nvl(b.cd_classe_material,cd_classe_material_w) 		= cd_classe_material_w
	order by nvl(b.cd_material,99999) desc,
		 nvl(b.cd_grupo_material,99999) desc,
		 nvl(b.cd_subgrupo_material,99999) desc,
		 nvl(b.cd_classe_material,99999) desc,
		 b.nr_sequencia;
		 	
begin
if	(ie_tipo_prescricao_p is not null) then

	select	nvl(max('S'),'N')
	into	ie_possui_regra_w
	from	regra_ajuste_disp_pepo a,
		regra_ajuste_disp_mat b
	where	a.nr_sequencia 		= b.nr_seq_regra;
	

	if	(ie_possui_regra_w = 'S') and (nvl(cd_material_p,0) > 0) then
		ie_dispensa_w := 'N';
		
		cd_grupo_material_w	:= Obter_estrutura_material(cd_material_p,'G');
		cd_subgrupo_material_w	:= Obter_estrutura_material(cd_material_p,'S');
		cd_classe_material_w	:= obter_dados_material(cd_material_p,'CCLA');
		
		open c01;
		loop
		fetch c01 into	
			ie_tipo_prescr_w;
		exit when c01%notfound;
			begin
			ie_tipo_prescr_ww	:= ie_tipo_prescr_w;
			end;
		end loop;
		close c01;
	
		if	(ie_tipo_prescr_ww = ie_tipo_prescricao_p) then
			ie_dispensa_w := 'S';
		end if;
	end if;
end if;	

return	ie_dispensa_w;
	
end obter_regra_disp_pepo;
/	