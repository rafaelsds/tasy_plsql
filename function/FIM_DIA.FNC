create or replace
FUNCTION Fim_Dia(	dt_origem_p		Date)
			RETURN Date deterministic IS

dt_retorno_w		Date;

BEGIN

dt_retorno_w	:= trunc(nvl(dt_origem_p,sysdate),'dd') + 86399/86400;

RETURN dt_retorno_w;

END Fim_dia;
/
