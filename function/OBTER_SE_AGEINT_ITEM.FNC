create or replace
function Obter_Se_Ageint_Item	( 
				cd_agenda_p		Number,
				nr_seq_agenda_int_p	Number,
				cd_estabelecimento_p	Number,
				nr_seq_proc_interno_p	Number,
				cd_especialidade_p	Number,
				cd_medico_p		Varchar2,
				cd_medico_exame_p	number,
				cd_procedimento_p	number,
				ie_origem_proced_p	number,
				nr_seq_item_ageint_p	Number,
				qt_idade_p		Number default null,
				ie_rodizio_especialidade_p		Varchar2 default 'N'
				)
 		    	return Varchar2 is

ds_Retorno_w		Varchar2(1)	:= 'N';

nr_seq_proc_interno_w	Number(10);
cd_especialidade_w	Number(5);
cd_medico_w		Varchar2(10);
ie_tipo_agendamento_w	Varchar2(15);	
cd_tipo_agenda_w	Number(10);
cd_espec_agenda_w	Number(5);
cd_pessoa_fisica_w	Varchar2(10);
cd_convenio_w		Number(5);
cd_categoria_w		Varchar2(10);
nr_seq_regra_w		Number(10);
qt_exclusivo_w		Number(5);
ie_Agenda_w		Varchar2(2);
qt_regra_w		Number(5);
cd_estabelecimento_w	Number(4);
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(10);
cd_area_proced_w	number(8,0);
cd_espec_proced_w	number(8,0);
cd_grupo_proced_w	number(15,0);
cd_plano_w		varchar2(10);
nr_seq_grupo_w		number(10);	
ie_cons_lib_grupo_exames_w	varchar2(1);
nr_seq_forma_org_w	Number(10);                                                  
nr_seq_grupo_sus_w	Number(10);                                                 
nr_seq_subgrupo_w	Number(10);

Cursor C01 is
	select	nvl(nr_sequencia,0)
	from	agenda_regra 
	where	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
	and	cd_agenda = cd_agenda_p
	and	((cd_convenio = cd_convenio_w) or (cd_convenio is null))
	and	((cd_categoria = cd_categoria_w) or (cd_categoria is null))
	and	((cd_area_proc = cd_area_proced_w) or (cd_area_proc is null))
	and	((cd_especialidade = cd_espec_proced_w) or (cd_especialidade is null))
	and	((cd_grupo_proc = cd_grupo_proced_w) or (cd_grupo_proc is null))
	and	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
	and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null)))
	and	((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
	and	((cd_medico = cd_medico_exame_p) or (cd_medico is null))
	and	nvl(qt_idade_p,0) >= nvl(qt_idade_min,0)
	and 	nvl(qt_idade_p,0) <= nvl(qt_idade_max,999)		
	and	cd_municipio_ibge is null	
	and	nr_seq_grupo_ageint is null
	and	nvl(ie_situacao,'A') = 'A'
	order by nvl(cd_convenio,0),		 
		nvl(cd_procedimento,0), 
		nvl(nr_seq_proc_interno,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_proc,0),
		nvl(nr_seq_forma_org,0),
		nvl(nr_seq_subgrupo,0),
		nvl(nr_seq_grupo,0),
		nvl(cd_medico,'0'),
		nvl(cd_perfil,0);

Cursor C04 is                                                                   
	select	a.nr_sequencia                                                          
	from	agenda_regra d,                                                           
		agenda_int_grupo_item b,                                                      
		agenda_int_grupo a,                                                           
		agenda_integrada_item c
	where	a.nr_sequencia	= b.nr_seq_grupo                                          
	and	a.nr_sequencia	= d.nr_seq_grupo_ageint                                     
	and	d.cd_agenda 	= cd_agenda_p                                                 
	and	c.nr_seq_grupo_selec	=  a.nr_sequencia                                     
	--and	((nvl(d.ie_somente_anestesia,'N') = 'S' and ie_anestesia_w = 'S') or (nvl(d.ie_somente_anestesia,'N') = 'N')) Conforme orienta��o do Andr� W. n�o est� sendo consistido a linha comentada
	and	a.ie_situacao  = 'A'                                                       
	and	ds_retorno_w	<> 'S'               
	and	nvl(d.ie_situacao,'A') = 'A'	
	and	c.nr_sequencia = nr_seq_item_ageint_p;
	
Cursor C05 is                                                                   
	select	nr_sequencia                                                            
	from	agenda_regra                                                              
	where	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p     
	and	cd_agenda = cd_agenda_p                                                    
	and	((cd_convenio = cd_convenio_w) or (cd_convenio is null))                   
	and	((cd_categoria = cd_categoria_w) or (cd_categoria is null))                
	and	((cd_plano_convenio = cd_plano_w) or (cd_plano_convenio is null))          
	and	((cd_area_proc = cd_area_proced_w) or (cd_area_proc is null))              
	and	((cd_especialidade = cd_espec_proced_w) or (cd_especialidade is null))     
	and	((cd_grupo_proc = cd_grupo_proced_w) or (cd_grupo_proc is null))           
	and	nvl(nr_seq_forma_org, nr_seq_forma_org_w)	= nr_seq_forma_org_w             
	and	nvl(nr_seq_grupo, nr_seq_grupo_sus_w)		= nr_seq_grupo_sus_w                
	and	nvl(nr_seq_subgrupo, nr_seq_subgrupo_w)		= nr_seq_subgrupo_w               
	and	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))       
	and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null)))                                             
	and	((nr_seq_proc_interno = nr_seq_proc_interno_w) or (nr_seq_proc_interno is null))                                                                           
	and	nr_seq_grupo_ageint	= nr_seq_grupo_w                                       
	and	((cd_medico = cd_medico_p and ie_rodizio_especialidade_p = 'N') or (cd_medico is null) or (ie_rodizio_especialidade_p = 'S'))
	and	nvl(qt_idade_p,0) >= nvl(qt_idade_min,0)
	and 	nvl(qt_idade_p,0) <= nvl(qt_idade_max,999)	                   
	--and	((nvl(ie_somente_anestesia,'N') = 'S' and ie_anestesia_w = 'S') or (nvl(ie_somente_anestesia,'N') = 'N'))  Conforme orienta��o do Andr� W. n�o est� sendo consistido a linha comentada
	and	ie_permite in ('S','N')  
	and	nvl(ie_situacao,'A') = 'A'	
	order by nvl(cd_convenio, 0),                                                           
		nvl(cd_procedimento,0),                                               
		nvl(nr_seq_proc_interno,0),                                                   
		nvl(cd_area_proc,0),                                                          
		nvl(cd_especialidade,0),                                                      
		nvl(cd_grupo_proc,0),                                                         
		nvl(nr_seq_forma_org,0),                                                      
		nvl(nr_seq_subgrupo,0),                                                       
		nvl(nr_seq_grupo,0),                                                          		
		nvl(cd_medico, 0);                                              
	
begin
select	nvl(max(obter_valor_param_usuario(869, 218, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_p)), 'S')
into	ie_cons_lib_grupo_exames_w
from 	dual;

select	cd_convenio, 
	cd_categoria,
	cd_estabelecimento,
	cd_plano
into	cd_convenio_w,
	cd_categoria_w,
	cd_estabelecimento_w,
	cd_plano_w
from	agenda_integrada
where	nr_sequencia	= nr_seq_agenda_int_p;

select	max(cd_tipo_agenda),
	max(cd_especialidade),
	max(cd_pessoa_fisica)
into	cd_tipo_agenda_w,
	cd_espec_agenda_w,
	cd_pessoa_fisica_w
from	agenda
where	cd_agenda	= cd_agenda_p;


if	(nvl(cd_procedimento_p,0)	= 0) or
	(nvl(ie_origem_proced_p,0)	= 0) then
	obter_proc_tab_interno_conv(
				nr_seq_proc_interno_p,
				cd_estabelecimento_p,
				cd_convenio_w,
				cd_categoria_w,
				cd_plano_w,
				null,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				sysdate,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);
else
	cd_procedimento_w	:= cd_procedimento_p;
	ie_origem_proced_w	:= ie_origem_proced_p;
end if;

select	nvl(max(cd_area_procedimento),0),
	nvl(max(cd_especialidade),0),
	nvl(max(cd_grupo_proc),0),                                                     
	nvl(max(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento,ie_origem_proced, 'C', 'F'),'F'),1,10)),0),
	nvl(max(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento,ie_origem_proced, 'C', 'G'),'G'),1,10)),0),                                                                                      
	nvl(max(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento,ie_origem_proced, 'C', 'S'),'S'),1,10)),0)                                                                                                                    
into	cd_area_proced_w,
	cd_espec_proced_w,
	cd_grupo_proced_w,                                                             
	nr_seq_forma_org_w,                                                            
	nr_seq_grupo_sus_w,                                                           
	nr_seq_subgrupo_w  
from	estrutura_procedimento_v
where	cd_procedimento = cd_procedimento_w
and	ie_origem_proced = ie_origem_proced_w;

if	(cd_tipo_agenda_w = 2) then
	select	count(*)
	into	qt_exclusivo_w
	from	agenda_regra
	where	nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	and	cd_agenda 		= cd_agenda_p
	and	((cd_convenio = cd_convenio_w) or (cd_convenio is null))
	and	((cd_medico = cd_medico_exame_p) or (cd_medico is null))
	and	nvl(qt_idade_p,0) >= nvl(qt_idade_min,0)
	and 	nvl(qt_idade_p,0) <= nvl(qt_idade_max,999)	
	and	nr_seq_grupo_ageint is null
	and	ie_permite = 'E'
	and	nvl(ie_situacao,'A') = 'A';
	
	if	(qt_exclusivo_w > 0) then

		select	nvl(max(nr_sequencia),0)
		into	nr_seq_regra_w
		from	agenda_regra
		where	nvl(cd_estabelecimento, cd_estabelecimento_p) 	= cd_estabelecimento_p
		and	cd_agenda 		= cd_agenda_p
		and	((cd_convenio 		= cd_convenio_w) or (cd_convenio is null))
		and	((cd_categoria = cd_categoria_w) or (cd_categoria is null))
		and	((cd_area_proc = cd_area_proced_w) or (cd_area_proc is null))
		and	((cd_especialidade = cd_espec_proced_w) or (cd_especialidade is null))
		and	((cd_grupo_proc = cd_grupo_proced_w) or (cd_grupo_proc is null))
		and	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
		and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null)))
		and	((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
		and	((cd_medico 		= cd_medico_exame_p) or (cd_medico is null))
		and	nvl(qt_idade_p,0) >= nvl(qt_idade_min,0)
		and 	nvl(qt_idade_p,0) <= nvl(qt_idade_max,999)	
		and	nr_seq_grupo_ageint is null
		and	ie_permite = 'E'
		and	nvl(ie_situacao,'A') = 'A';
			
		if	(nr_seq_regra_w = 0) then
			ds_retorno_w	:= 'N';
		else
			ds_retorno_w	:= 'S';
		end if;
	else		
		open C01;
		loop
		fetch C01 into	
			nr_seq_regra_w;
		exit when C01%notfound;
			begin
			nr_seq_regra_w := nr_seq_regra_w;
			end;
		end loop;
		close C01;

			
		
		if	(nr_seq_regra_w > 0) then
		
			select	nvl(max(ie_permite),'S')
			into	ie_agenda_w
			from	agenda_regra
			where	nr_sequencia = nr_seq_regra_w;
		
		
			if	(ie_agenda_w = 'S') then
				ds_retorno_w	:= 'S';
			end if;

			if	(ie_agenda_w in ('N', 'Q')) then
				ds_retorno_w	:= 'N';
			end if;		
		else	
			if (nvl(nr_seq_item_ageint_p,0) > 0) then
				open C04;                                                                     
				loop                                                                          
				fetch C04 into                                                                
					nr_seq_grupo_w;                                                              
				exit when C04%notfound;                                                       
					begin                                                                                                                                         
					open C05;                                                                    
					loop                                                                         
					fetch C05 into                                                               
						nr_seq_regra_w;                                                             
					exit when C05%notfound;                                                      
						begin                                                                       
						if	(ds_retorno_w <> 'S') or (ie_cons_lib_grupo_exames_w = 'N') then                                             
							select	nvl(max(ie_permite),'N')                                            
							into	ds_retorno_w                                                          
							from	agenda_regra                                                          
							where	nr_sequencia	= nr_seq_regra_w;                                       				
						end if;                                                                   
						end;                                                                        
					end loop;                                                                    
					close C05;                                                                   
					end;                                                                         
				end loop;                                                                     
				close C04;            
			end if;
		end if;
		
	end if;

	if	(ds_retorno_w	= 'S') then
		ds_retorno_w	:= ageint_obter_regra_med_exame(cd_medico_exame_p, cd_agenda_p, nr_seq_proc_interno_p, nr_seq_Agenda_int_p, cd_procedimento_p, ie_origem_proced_p);
	end if;
elsif	(cd_tipo_agenda_w in (3,4)) then
	if	(((cd_medico_p		= cd_pessoa_fisica_w) and (ie_rodizio_especialidade_p = 'N')) or (cd_medico_p is null) or (ie_rodizio_especialidade_p = 'S')) and
		((cd_espec_agenda_w	= cd_especialidade_p) or (cd_especialidade_p is null)) then
		ds_retorno_w	:= 'S';
	else
		ds_Retorno_w	:= 'N';
	end if;
end if;

if	(ds_retorno_w	= 'S') then
	return	ds_Retorno_w;
end if;

return	ds_retorno_w;

end Obter_Se_Ageint_Item;
/