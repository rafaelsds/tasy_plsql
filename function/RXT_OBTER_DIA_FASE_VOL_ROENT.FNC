create or replace
function rxt_obter_dia_fase_vol_roent	(	nr_seq_volume_p		number,
						nr_seq_campo_p		number,
						ie_opcao_p		varchar2)
						return number is

nr_seq_dia_w	number(10,0);
 
/*

DT - Dia tratamento
DF - Dia fase

*/

begin

if	(nr_seq_volume_p is not null) and
	(nr_seq_campo_p is not null) then
	begin
	if	(ie_opcao_p = 'DT') then

		select	nvl(max(nr_seq_dia),0) + 1
		into	nr_seq_dia_w
		from	rxt_agenda_fase
		where	nr_seq_volume	= nr_seq_volume_p;

	elsif	(ie_opcao_p = 'DF') then
	
		select	nvl(max(nr_seq_dia_fase),0) + 1
		into	nr_seq_dia_w
		from	rxt_agenda_fase
		where	nr_seq_volume		= nr_seq_volume_p
		and	nr_seq_campo_roentgen	= nr_seq_campo_p;

	end if;
	end;
end if;

return nr_seq_dia_w;

end rxt_obter_dia_fase_vol_roent;
/