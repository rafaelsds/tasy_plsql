create or replace
function OBTER_TITULO_REPASSE
		(nr_repasse_terceiro_p	number) return varchar2 is

ds_lista_titulo_w	varchar2(254) := '';
nr_titulo_w		number(10,0);

cursor c01 is
select	nr_titulo
from	titulo_pagar
where	nr_repasse_terceiro	= nr_repasse_terceiro_p
union
select	nr_titulo
from	repasse_terceiro_venc
where	nr_repasse_terceiro	= nr_repasse_terceiro_p
order	by nr_titulo;

begin

open c01;
loop
fetch c01 into
	nr_titulo_w;
exit when c01%notfound;

	if	(ds_lista_titulo_w is null) then
		ds_lista_titulo_w	:= nr_titulo_w;
	else
		ds_lista_titulo_w	:= substr(ds_lista_titulo_w || ', ' || nr_titulo_w,1,254);
	end if;

end loop;
close c01;

return ds_lista_titulo_w;

end OBTER_TITULO_REPASSE;
/