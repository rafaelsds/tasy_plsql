create or replace
function obter_se_pac_ultimo_leito(nr_atendimento_p		number)
 		    	return varchar2 is
				
dt_entrada_unidade_w	date;
nr_seq_interno_w		number(10,0);
ie_paciente_leito_w		varchar2(1) := 'N';
ie_status_unidade_w		varchar2(3);
nr_atendimento_w		number(10,0);
nr_atepacu_w			number(10,0);

begin
nr_atepacu_w := Obter_Atepacu_paciente(nr_atendimento_p, 'IAA');

if	(nvl(nr_atendimento_p,0) > 0)	then
	select	max(dt_entrada_unidade),
			max(nr_seq_interno)
	into	dt_entrada_unidade_w,
			nr_seq_interno_w
	from 	setor_atendimento b,
			atend_paciente_unidade a
	where	a.nr_atendimento		= nr_atendimento_p
	and		a.cd_setor_atendimento 	= b.cd_setor_atendimento
	and		nvl(a.nr_seq_interno,nr_atepacu_w) = nr_atepacu_w
	and		a.dt_saida_unidade = (select max(x.dt_saida_unidade)
								from	 atend_paciente_unidade x
								where	 x.nr_atendimento = a.nr_atendimento)
	order by	decode(b.cd_classif_setor, 3, 1, 4, 1, 8, 1, 0),
				a.dt_saida_unidade,
				a.dt_entrada_unidade;

	if	(nvl(nr_seq_interno_w,0) > 0)	then
		select 	max(b.ie_status_unidade),
				max(b.nr_atendimento)
		into	ie_status_unidade_w,
				nr_atendimento_w
		from  	unidade_atendimento b,
				atend_paciente_unidade a
		where 	a.nr_atendimento 		= nr_atendimento_p
		and 	a.nr_seq_interno		= nr_seq_interno_w
		and 	a.cd_setor_atendimento	= b.cd_setor_atendimento
		and 	a.cd_unidade_basica		= b.cd_unidade_basica
		and 	a.cd_unidade_compl		= b.cd_unidade_compl;
		
		if	(ie_status_unidade_w = 'P') and
			(nr_atendimento_w <> nr_atendimento_p)	then
			ie_paciente_leito_w := 'S';
		else
			ie_paciente_leito_w	:= 'N';
		end if;
	end if;
end if;	


return	ie_paciente_leito_w;

end obter_se_pac_ultimo_leito;
/