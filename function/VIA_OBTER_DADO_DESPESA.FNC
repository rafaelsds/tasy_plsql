create or replace function via_obter_dado_despesa(
		nr_seq_despesa_p         number,
		ie_field_p 	        varchar2 )      return varchar2 is

        dt_despesa_w            via_despesa.dt_despesa%type;
        nr_seq_classif_desp_w	via_despesa.nr_seq_classif_desp%type;
        ie_responsavel_custo_w  via_despesa.ie_responsavel_custo%type;
        vl_despesa_w            via_despesa.vl_despesa%type;
        nr_seq_forma_pagto_w    via_despesa.nr_seq_forma_pagto%type;
        cd_retorno_w 		Varchar2(80);
        
begin

        SELECT  vd.dt_despesa,
                vd.nr_seq_classif_desp,
                vd.ie_responsavel_custo,
                vd.vl_despesa,
                vd.nr_seq_forma_pagto
        INTO    dt_despesa_w,
                nr_seq_classif_desp_w,
                ie_responsavel_custo_w,
                vl_despesa_w,
                nr_seq_forma_pagto_w
        FROM    via_despesa vd
        WHERE   vd.nr_sequencia = nr_seq_despesa_p;

	if (ie_field_p = 'DT_DESPESA') then
		cd_retorno_w := to_char(dt_despesa_w, 'DD/MM/YYYY');
	elsif (ie_field_p = 'NR_SEQ_CLASSIF_DESP') then
		cd_retorno_w := nr_seq_classif_desp_w;
	elsif (ie_field_p = 'IE_RESPONSAVEL_CUSTO') then
		cd_retorno_w := ie_responsavel_custo_w;
	elsif (ie_field_p = 'VL_DESPESA') then
		cd_retorno_w := vl_despesa_w;
        elsif (ie_field_p = 'NR_SEQ_FORMA_PAGTO') then
                cd_retorno_w := nr_seq_forma_pagto_w;
	end if;

	return cd_retorno_w;

end via_obter_dado_despesa;
/
