create or replace
function obter_cor_barras_atepacfq(
			nr_seq_cor_p		number,
			cd_perfil_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2)
 		    	return varchar2 is

ds_cor_w		varchar2(15) :='';
ds_cor_perfil_w		varchar2(15) :='';
ds_retorno_w		varchar2(15);

begin

select	ds_cor_fundo
into	ds_cor_w
from	tasy_padrao_cor
where	nr_sequencia = nr_seq_cor_p;

select	nvl(max(ds_cor_fundo),'X')
into	ds_cor_perfil_w
from	tasy_padrao_cor_perfil
where	nr_seq_padrao = nr_seq_cor_p
and	cd_estabelecimento = cd_estabelecimento_p
and	((cd_perfil = cd_perfil_p) or (nvl(cd_perfil,0) = 0));

if	(ds_cor_perfil_w <> 'X') then
	ds_retorno_w := ds_cor_perfil_w;
else
	ds_retorno_w := ds_cor_w;
end if;

return	ds_retorno_w;

end obter_cor_barras_atepacfq;
/