create or replace
function elimina_numeros
		(ds_campo_p	varchar2)
		return varchar2 is

ds_retorno_w	varchar2(255);

begin
ds_retorno_w	:= substr(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(ds_campo_p,'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9',''),1,255);

	
return	ds_retorno_w;

end elimina_numeros;
/
