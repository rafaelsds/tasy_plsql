create or replace
function pls_obter_valores_conta_comp
			(	nr_seq_competencia_p		Number,
				ie_tipo_movimentacao_p		Varchar2)
 		    		return Number is
				
vl_retorno_w			Number(15,2)	:= 0;
vl_identificado_w		Number(15,2) 	:= 0;
vl_cobrado_w			Number(15,2)	:= 0;
vl_deferido_w			Number(15,2)	:= 0;
vl_debitos_pendentes_w		Number(15,2)	:= 0;
vl_divida_ativa_w		Number(15,2)	:= 0;
vl_final_w			Number(15,2)	:= 0;
pr_historico_w			pls_processo_competencia.pr_historico%type;

begin

/* Tipos de movimenta��o padr�o, utilizados na ativa��o dos WCPanel
	I = Identificadas
	C = Cobradas
	D = Deferedias
	P = Pendentes
	DP = D�bitos pendentes
	DA = Divida ativa */
if 	(ie_tipo_movimentacao_p in ('I', 'C', 'D', 'P', 'DP', 'DA')) then
	select	nvl(sum(pls_conta_processo_obter_valor(a.nr_seq_conta)), 0)
	into 	vl_retorno_w
	from	pls_processo_contas_comp a
	where	a.nr_seq_competencia 	= nr_seq_competencia_p
	and	a.ie_tipo_movimentacao 	= ie_tipo_movimentacao_p;
/* Tipos de movimenta��o utilizados no WCPanel de resumo
	FI = Valor final (ABIs identificadas - ABIs Cobradas - ABIs Deferidas)
	HC = Valor final * Percentual hist�rico
	TO = Total (HC + D�bitos pendentes + D�vida ativa) */
elsif 	(ie_tipo_movimentacao_p in ('FI', 'HC')) then 
	select	nvl(sum(pls_conta_processo_obter_valor(a.nr_seq_conta)), 0)
	into 	vl_identificado_w
	from	pls_processo_contas_comp a
	where	a.nr_seq_competencia 	= nr_seq_competencia_p
	and	a.ie_tipo_movimentacao 	= 'I';
	
	select	nvl(sum(pls_conta_processo_obter_valor(a.nr_seq_conta)), 0)
	into 	vl_cobrado_w
	from	pls_processo_contas_comp a
	where	a.nr_seq_competencia 	= nr_seq_competencia_p
	and	a.ie_tipo_movimentacao 	= 'C';

	select	nvl(sum(pls_conta_processo_obter_valor(a.nr_seq_conta)), 0)
	into 	vl_deferido_w
	from	pls_processo_contas_comp a
	where	a.nr_seq_competencia 	= nr_seq_competencia_p
	and	a.ie_tipo_movimentacao 	= 'D';

	vl_retorno_w := vl_identificado_w - vl_cobrado_w - vl_deferido_w;

	if	(ie_tipo_movimentacao_p = 'HC') then
		select	pr_historico
		into	pr_historico_w
		from	pls_processo_competencia
		where	nr_sequencia = nr_seq_competencia_p;

		vl_retorno_w := dividir(vl_retorno_w * pr_historico_w, 100);
	end if;
elsif	(ie_tipo_movimentacao_p = 'TO') then
	select	pls_obter_valores_conta_comp(nr_seq_competencia_p, 'HC') vl_total
	into	vl_final_w
	from 	dual;

	select	pls_obter_valores_conta_comp(nr_seq_competencia_p, 'DP') vl_total
	into	vl_debitos_pendentes_w
	from 	dual;

	select	pls_obter_valores_conta_comp(nr_seq_competencia_p, 'DA') vl_total
	into	vl_divida_ativa_w
	from 	dual;

	vl_retorno_w := vl_final_w + vl_debitos_pendentes_w + vl_divida_ativa_w;
end if;

return	vl_retorno_w;

end pls_obter_valores_conta_comp;
/