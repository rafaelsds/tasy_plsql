create or replace 
function get_nr_seq_solucao_agrupada(nr_seq_cpoe_p  prescr_material.nr_seq_mat_cpoe%type)
        return number is

nr_prescricao_vigente_w     prescr_medica.nr_prescricao%type;
nr_seq_solucao_w            prescr_solucao.nr_seq_solucao%type;

begin 

nr_prescricao_vigente_w := obter_prescricao_solucao_cpoe(nr_seq_cpoe_p);

select  max(nr_sequencia_solucao)
into    nr_seq_solucao_w
from    prescr_material
where	nr_seq_mat_cpoe = nr_seq_cpoe_p
and     nr_prescricao = nr_prescricao_vigente_w;

return nr_seq_solucao_w;

end get_nr_seq_solucao_agrupada;
/
