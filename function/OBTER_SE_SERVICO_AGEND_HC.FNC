create or replace
function obter_se_servico_agend_hc( dt_agenda_p date,
				    cd_pessoa_fisica_p varchar2,
				    nr_seq_servico_p number)
				return varchar2 is

ie_retorno_w	varchar2(1);

begin

select	decode(count(*),0,'N','S')
into	ie_retorno_w
from	agenda_home_care a,
	agenda_hc_paciente b,
	hc_servico c
where	b.nr_seq_agenda = a.nr_sequencia
and	a.nr_seq_servico = c.nr_sequencia
and	b.cd_pessoa_fisica = cd_pessoa_fisica_p
and	b.dt_agenda = dt_agenda_p
and     c.nr_sequencia = nr_seq_servico_p;

return ie_retorno_w;

end obter_se_servico_agend_hc;
/