create or replace
function obter_desc_exame_hist_motiv(nr_seq_motivo_hist_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin

if	(nr_seq_motivo_hist_p is not null) then

	select	max(ds_motivo)
	into	ds_retorno_w
	from	exame_historico_motivo
	where	nr_sequencia = nr_seq_motivo_hist_p;

end if;

return	ds_retorno_w;

end obter_desc_exame_hist_motiv;
/