create or replace
function obter_seq_nf_emprestimo(
	nr_emprestimo_p		number)
 	return number is

nr_sequencia_w		number(10);			
			
begin

select	max(a.nr_sequencia)
into	nr_sequencia_w
from	nota_fiscal a,
	nota_fiscal_item b
where	a.nr_sequencia = b.nr_sequencia
and	a.ie_situacao = '1'
and	b.nr_emprestimo = nr_emprestimo_p
and	not exists (
	select	1
	from	emprestimo_material_dev x
	where	x.nr_seq_nota = a.nr_sequencia);

return	nr_sequencia_w;

end obter_seq_nf_emprestimo;
/
