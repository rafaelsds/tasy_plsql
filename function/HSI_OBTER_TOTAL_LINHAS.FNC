create or replace
function HSI_Obter_Total_Linhas	(nr_seq_protocolo_p	Number,
				 nr_interno_conta_p	number)
					return Number is

nr_total_linhas_w	Number(10):= 0;
qt_reg_10_w		Number(10):= 1;
qt_reg_11_w		Number(10):= 0;
qt_reg_12_w		Number(10):= 0;
qt_reg_13_w		Number(10):= 0;
qt_reg_19_w		Number(10):= 1;

begin

/* Registro 11 */
select 	count(*)
into	qt_reg_11_w
from   	w_interf_conta_cab  a
where	ie_tipo_atend_real = 1
and   	nr_seq_protocolo = nr_seq_protocolo_p
and 	nr_interno_conta = nr_interno_conta_p;

/* Registro 12 */
select 	count(*)
into	qt_reg_12_w
from	(
	SELECT 	a.nr_interno_conta,
		a.nr_seq_protocolo,
		substr(x.nr_doc_convenio,1,8) nr_documento,
		Campo_numerico(a.cd_item_convenio) cd_procedimento,
		a.dt_entrada_unidade 	dt_entrada,
		nvl(a.dt_saida_unidade,x.dt_periodo_final) dt_saida,
		sum(a.qt_item) 		qt_procedimento,
		sum(a.vl_total_item) 	vl_procedimento,
		a.cd_item, 
		obter_cpf_pessoa_fisica(a.CD_MEDICO_REQ) nr_cpf_solic
	from	w_interf_conta_cab 	x,
		w_interf_conta_item 	a
	where	a.nr_interno_conta	= x.nr_interno_conta
	and	nvl(a.ie_total_interf,0)	= 5 
	and	ie_tipo_atend_real	= 1
	and 	nvl(a.ie_responsavel_credito,'H') <> 'PRC'
	and	x.nr_seq_protocolo = nr_seq_protocolo_p
	and 	x.nr_interno_conta = nr_interno_conta_p
	group by Campo_numerico(a.cd_item_convenio),
       		a.nr_interno_conta,
		obter_cpf_pessoa_fisica(a.CD_MEDICO_REQ) ,
       		a.nr_seq_protocolo,
		a.cd_item,
   		a.dt_entrada_unidade,
   		nvl(a.dt_saida_unidade,x.dt_periodo_final),
	   	substr(x.nr_doc_convenio,1,8)) t;

/* Registro 13 */
select 	count(*)
into	qt_reg_13_w
from	(
	SELECT 	a.nr_interno_conta,
   		a.nr_seq_protocolo,
	  	substr(x.nr_doc_convenio,1,8) nr_documento,
   		Campo_numerico(a.cd_item_convenio) cd_procedimento,
	   	decode(a.ie_tipo_item,1,sum(a.qt_item),1) qt_procedimento,
	   	sum(decode(a.cd_area_proc,1,(a.vl_honorario + a.vl_custo_oper),	
				4,(a.vl_honorario + a.vl_custo_oper),a.vl_total_item)) 	vl_procedimento,
	   	nvl(a.nr_cpf_executor,a.cd_cgc_hospital) cd_cgc_cpf_exec,
	   	nvl(a.cd_cgc_cpf_resp_cred, a.cd_cgc_hospital) 	cd_cgc_cpf_resp_cred,
	   	decode(a.ie_tipo_item,1, a.dt_item, nvl(x.dt_alta,x.dt_periodo_final)) dt_procedimento,
	   	a.cd_funcao_executor 	cd_atividade_prestador,
	   	a.nr_porte_anestesico 	ie_porte_anestesico,
	   	decode(a.ie_tipo_item,1, a.dt_item, nvl(x.dt_alta,x.dt_periodo_final)) hr_atendimento,
	   	a.pr_funcao_participante * 100 	tx_procedimento,
	   	decode(a.cd_classif_setor,'4','S','N') 	ie_procedimento_uti, 
	   	a.cd_item
	from	w_interf_conta_cab 	x,
		w_interf_conta_item 	a
	where	a.nr_interno_conta	= x.nr_interno_conta
	and	nvl(a.ie_total_interf,0)	<> 5
	and 	nvl(a.ie_responsavel_credito,'H') <> 'PRC'
	and	x.nr_seq_protocolo = nr_seq_protocolo_p
	and 	x.nr_interno_conta = nr_interno_conta_p
	group by Campo_numerico(a.cd_item_convenio),
		a.nr_interno_conta,
		a.nr_seq_protocolo,
		obter_cpf_pessoa_fisica(a.CD_MEDICO_REQ) ,
		nvl(a.nr_cpf_executor,a.cd_cgc_hospital),
		nvl(a.cd_cgc_cpf_resp_cred, a.cd_cgc_hospital),
		decode(a.ie_tipo_item,1, a.dt_item, nvl(x.dt_alta,x.dt_periodo_final)),
		a.cd_funcao_executor,
		a.nr_porte_anestesico,
		a.pr_via_acesso,
		a.pr_funcao_participante,
		a.ie_emite_conta,
		a.cd_classif_setor,
		decode(a.ie_tipo_item,1, a.dt_item, nvl(x.dt_alta,x.dt_periodo_final)),
		a.cd_item,
		a.ie_tipo_item,
		substr(x.nr_doc_convenio,1,8)
	having 	sum(decode(a.ie_tipo_item,1, a.qt_item, a.vl_total_item)) > 0) t;


nr_total_linhas_w:= 	qt_reg_10_w + qt_reg_11_w + qt_reg_12_w + 
			qt_reg_13_w + qt_reg_19_w;

return	nr_total_linhas_w;

end HSI_Obter_Total_Linhas;
/