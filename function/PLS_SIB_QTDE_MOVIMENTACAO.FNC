create or replace
function pls_sib_qtde_movimentacao
			(	nr_seq_lote_p		pls_sib_lote.nr_sequencia%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type)
				return integer is

qt_registro_w		number(10);
ie_tipo_lote_w		pls_sib_lote.ie_tipo_lote%type;
ie_gerar_correcao_w	pls_sib_lote.ie_gerar_correcao%type;
dt_inicio_mov_w		pls_sib_lote.dt_inicio_mov%type;
dt_fim_mov_w		pls_sib_lote.dt_fim_mov%type;

Begin

if	(nr_seq_lote_p is not null) then
	select	ie_tipo_lote,
		nvl(ie_gerar_correcao,'N'),
		trunc(dt_inicio_mov,'dd'),
		fim_dia(dt_fim_mov)
	into	ie_tipo_lote_w,
		ie_gerar_correcao_w,
		dt_inicio_mov_w,
		dt_fim_mov_w
	from	pls_sib_lote
	where	nr_sequencia = nr_seq_lote_p;
	
	if	(ie_tipo_lote_w = 'M') then --Movimenta��o
		select	count(1)
		into	qt_registro_w
		from (	select	nr_seq_agrup
			from (	select	a.nr_seq_segurado nr_seq_agrup
				from	pls_segurado_status a,
					pls_segurado b
				where	a.nr_seq_segurado = b.nr_sequencia
				and	b.ie_tipo_segurado in ('B','R')
				and	b.dt_liberacao is not null
				and	b.dt_cancelamento is null
				and	a.dt_inicial between dt_inicio_mov_w and dt_fim_mov_w
				and	b.cd_estabelecimento = cd_estabelecimento_p
				union
				select	a.nr_seq_segurado nr_seq_agrup
				from	pls_segurado_status a,
					pls_segurado b
				where	a.nr_seq_segurado = b.nr_sequencia
				and	b.ie_tipo_segurado in ('B','R')
				and	b.dt_liberacao is not null
				and	b.dt_cancelamento is null
				and	b.cd_cco is not null
				and	a.dt_final between dt_inicio_mov_w and dt_fim_mov_w
				and	b.cd_estabelecimento = cd_estabelecimento_p
				union
				select	a.nr_seq_segurado nr_seq_agrup
				from	pls_segurado_historico a,
					pls_segurado b
				where	a.nr_seq_segurado = b.nr_sequencia
				and	b.ie_tipo_segurado in ('B','R')
				and	b.dt_liberacao is not null
				and	b.dt_cancelamento is null
				and	b.cd_cco is not null
				and	a.ie_envio_sib = 'S'
				and	a.dt_ocorrencia_sib between dt_inicio_mov_w and dt_fim_mov_w
				and	b.cd_estabelecimento = cd_estabelecimento_p
				union
				select	b.nr_sequencia nr_seq_agrup
				from	pls_pessoa_fisica_sib a,
					pls_segurado b
				where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
				and	b.ie_tipo_segurado in ('B','R')
				and	b.dt_liberacao is not null
				and	b.dt_cancelamento is null
				and	b.cd_cco is not null
				and	(b.dt_rescisao is null or b.dt_rescisao >= dt_fim_mov_w)
				and	a.dt_ocorrencia_sib between dt_inicio_mov_w and dt_fim_mov_w
				and	b.cd_estabelecimento = cd_estabelecimento_p
				union
				select	a.nr_sequencia nr_seq_agrup
				from	pls_sib_reenvio a
				where	a.cd_estabelecimento = cd_estabelecimento_p
				and	a.ie_tipo_movimento is not null
				and	a.nr_seq_lote_sib is null
				and	a.nr_seq_segurado is not null
				and	ie_gerar_correcao_w = 'S' --No lote de Movimenta��o, s� considera corre��es se a op��o estiver marcada no lote
				)
			group by nr_seq_agrup);
	elsif	(ie_tipo_lote_w = 'C') then --Corre��o
		select	count(1)
		into	qt_registro_w
		from	pls_sib_reenvio a
		where	a.cd_estabelecimento = cd_estabelecimento_p
		and	a.ie_tipo_movimento is not null
		and	a.nr_seq_lote_sib is null
		and	a.nr_seq_segurado is not null;
	elsif	(ie_tipo_lote_w in ('I','E')) then --Inclus�o/Exclus�o de todos benefici�rios ativos
		select	count(1)
		into	qt_registro_w
		from	pls_segurado a,
			pls_contrato b
		where	b.nr_sequencia = a.nr_seq_contrato
		and	a.ie_tipo_segurado in ('B','R')
		and	b.cd_estabelecimento = cd_estabelecimento_p
		and	a.dt_liberacao is not null
		and	a.dt_cancelamento is null
		and	(a.dt_rescisao is null or a.dt_rescisao > sysdate)
		and	b.ie_tipo_beneficiario <> 'ENB';
	else
		qt_registro_w	:= 0;
	end if;
end if;

return nvl(qt_registro_w,0);

end pls_sib_qtde_movimentacao;
/