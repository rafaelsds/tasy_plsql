create or replace
function obter_se_inconsiste_laudo_aih(
				cd_procedimento_p	number,
				ie_origem_proced_p	number,
				ie_classificacao_p	number,
				cd_cid_principal_p	varchar2)
				return varchar2 is
				
ds_retorno_w			varchar2(1) := 'N';
qt_cid_compat_w			number(10);
nr_seq_geral_w			number(15);

begin

/* 2 - CID diagnůstico principal incorreto */
if	(nvl(cd_procedimento_p,0) <> 0) and
	(nvl(ie_origem_proced_p,0) <> 0) and
	(nvl(ie_classificacao_p,0) = 1) and
	(nvl(cd_cid_principal_p,'X') <> 'X') then
	begin
	select	count(*)
	into	qt_cid_compat_w
	from	sus_procedimento_cid
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_geral_w
	from	sus_procedimento_cid
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p
	and	cd_doenca_cid		= cd_cid_principal_p;
	
	if	(nr_seq_geral_w	= 0) and
		(qt_cid_compat_w <> 0) then
		ds_retorno_w := 'S';
	end if;
	end;
end if;

return	ds_retorno_w;

end obter_se_inconsiste_laudo_aih;
/