CREATE OR REPLACE
FUNCTION OBTER_TITULAR_CONVENIO (NR_ATENDIMENTO_P	NUMBER, 
				 CD_CONVENIO_p		NUMBER, 
				 IE_OPCAO_P 		VARCHAR)
				 RETURN VARCHAR2 IS


/*	ie_valor_p		C - C�digo
				D - Descri��o
*/

ds_resultado_w		Varchar2(60);
cd_pessoa_titular_w	Varchar2(10);

BEGIN

select	max(P.CD_PESSOA_TITULAR)
into	cd_pessoa_titular_w
from	PESSOA_TITULAR_CONVENIO P,
	ATENDIMENTO_PACIENTE A
where	P.CD_PESSOA_FISICA = A.CD_PESSOA_FISICA
and	A.NR_ATENDIMENTO = NR_ATENDIMENTO_P
and	P.CD_CONVENIO = CD_CONVENIO_P;

if	(upper(IE_OPCAO_P) = 'C') then
	ds_resultado_w := cd_pessoa_titular_w;
else
	if	(cd_pessoa_titular_w is not null) then
		ds_resultado_w := substr(OBTER_NOME_PF(cd_pessoa_titular_w),1,60);
	end if;
end if;

RETURN	ds_resultado_w;

END	OBTER_TITULAR_CONVENIO;
/
