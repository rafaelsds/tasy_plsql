create or replace
function pls_obter_censo_mes(	dt_mes_ref_p		date,
				ie_informacao_p		varchar2,
				cd_estabelecimento_p	number,
				nr_seq_plano_p		number,
				ie_segmentacao_p	varchar2,
				ie_tipo_contratacao_p	varchar2,
				nr_seq_faixa_etaria_p	number,
				ie_regulamentacao_p	varchar2,
				nr_seq_vendedor_canal_p	number,
				nr_seq_contrato_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
qt_itens_w	number(10);

begin

if	(nvl(cd_estabelecimento_p,0) <> 0) then		/* Estabelecimento */
	select	count(*)
	into	qt_itens_w
	from	w_pls_censo
	where	dt_mes_referencia = dt_mes_ref_p
	and	ie_situacao_beneficiario = ie_informacao_p
	and	cd_estabelecimento_p	= cd_estabelecimento;
elsif	(nvl(nr_seq_plano_p,0) <> 0) then		/* Plano */
	select	count(*)
	into	qt_itens_w
	from	w_pls_censo
	where	dt_mes_referencia = dt_mes_ref_p
	and	ie_situacao_beneficiario = ie_informacao_p
	and	nr_seq_plano_p	= nr_seq_plano;
elsif	(nvl(ie_segmentacao_p,'0') <> '0') then		/* Segmentação */
	select	count(*)
	into	qt_itens_w
	from	w_pls_censo
	where	dt_mes_referencia = dt_mes_ref_p
	and	ie_situacao_beneficiario = ie_informacao_p
	and	ie_segmentacao_p	= ie_segmentacao;
elsif	(nvl(ie_tipo_contratacao_p,'0') <> '0') then	/* Tipo contratação */
	select	count(*)
	into	qt_itens_w
	from	w_pls_censo
	where	dt_mes_referencia = dt_mes_ref_p
	and	ie_situacao_beneficiario = ie_informacao_p
	and	ie_tipo_contratacao_p	= ie_tipo_contratacao;
elsif	(nvl(nr_seq_faixa_etaria_p,0) <> 0) then	/* Faixa etária */
	select	count(*)
	into	qt_itens_w
	from	w_pls_censo
	where	dt_mes_referencia = dt_mes_ref_p
	and	ie_situacao_beneficiario = ie_informacao_p
	and	nr_seq_faixa_etaria_p	= nr_seq_faixa_etaria;
elsif	(nvl(ie_regulamentacao_p,'0') <> '0') then	/* Regulamentação */
	select	count(*)
	into	qt_itens_w
	from	w_pls_censo
	where	dt_mes_referencia = dt_mes_ref_p
	and	ie_situacao_beneficiario = ie_informacao_p
	and	ie_regulamentacao_p	= ie_regulamentacao;
elsif	(nvl(nr_seq_vendedor_canal_p,0) <> 0) then	/* Vendedor */
	select	count(*)
	into	qt_itens_w
	from	w_pls_censo
	where	dt_mes_referencia = dt_mes_ref_p
	and	ie_situacao_beneficiario = ie_informacao_p
	and	nr_seq_vendedor_canal_p	= nr_seq_vendedor_canal;
elsif	(nvl(nr_seq_contrato_p,'0') <> '0') then	/* Contrato */
	select	count(*)
	into	qt_itens_w
	from	w_pls_censo
	where	dt_mes_referencia = dt_mes_ref_p
	and	ie_situacao_beneficiario = ie_informacao_p
	and	nr_seq_contrato_p	= nr_seq_contrato;
end if;

ds_retorno_w := to_char(qt_itens_w);

return	ds_retorno_w;

end pls_obter_censo_mes;
/