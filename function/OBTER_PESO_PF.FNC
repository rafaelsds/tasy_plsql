create or replace
function obter_peso_pf( cd_pessoa_fisica_p 	varchar2)
			return number is

qt_retorno_w		number(10);

begin

if	(cd_pessoa_fisica_p is not null) then
	select	max(qt_peso)
	into	qt_retorno_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
end if;
	
return qt_retorno_w;
end obter_peso_pf;
/
