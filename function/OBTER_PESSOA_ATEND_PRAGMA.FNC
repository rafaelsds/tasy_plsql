create or replace
function obter_pessoa_atend_pragma (nr_atendimento_p	number)
							return varchar2 is

pragma autonomous_transaction;

cd_pessoa_atend_w	pessoa_fisica.cd_pessoa_fisica%type;

begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_atend_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

return	cd_pessoa_atend_w;

end obter_pessoa_atend_pragma;
/
