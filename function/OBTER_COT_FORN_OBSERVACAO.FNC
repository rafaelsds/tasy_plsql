CREATE OR REPLACE
function obter_cot_forn_observacao(
		nr_seq_cot_forn_p		number,
		ie_quebra_linha_p		Varchar2) return varchar2 is

cd_material_w		Number(8);
ds_observacao_w		Varchar2(255);
Resultado_w	 	Varchar2(4000);
Quebra_w		Varchar2(10) 	:= chr(13) || chr(10);

cursor  c01 is
select	cd_material,
	ds_observacao
from	cot_compra_forn_item
where	nr_seq_cot_forn = nr_seq_cot_forn_p
and	nvl(ie_situacao,'A') = 'A'
and	ds_observacao is not null;

BEGIN

if	(nvl(ie_quebra_Linha_p,'N') <> 'S') then
	quebra_w		:= '  ';
end if;

OPEN C01;
LOOP
FETCH C01 into	
	cd_material_w,
	ds_observacao_w;
EXIT WHEN C01%NOTFOUND;
	begin

		Resultado_w	:=	Resultado_w || cd_material_w || ' - ' || ds_observacao_w || ', ' || quebra_w;
		
	end;
END LOOP;

RETURN resultado_w;

END obter_cot_forn_observacao;
/