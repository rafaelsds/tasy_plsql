create or replace 
FUNCTION valida_medida_laudo(	nr_sequencia_p	in number)
				return Varchar2 is			

QT_MEDIDA_w	number(15,4);
nr_seq_medida_w	number(10,0);
QT_MAXIMO_w	number(15,4);
QT_MINIMO_w	number(15,4);		
QT_MEDIDA_MAX_w	number(15,4);
QT_MEDIDA_MIN_w	number(15,4);		

begin

select	nr_seq_medida,
	qt_medida,
	qt_maximo,
	qt_minimo
into	nr_seq_medida_w,
	qt_medida_w,
	qt_maximo_w,
	qt_minimo_w
from	laudo_paciente_medida
where	nr_sequencia = nr_sequencia_p;

SELECT	nvl(qt_minimo_w,QT_MEDIDA_MIN),
	nvl(qt_maximo_w,QT_MEDIDA_MAX)
INTO	QT_MEDIDA_MIN_w,
	QT_MEDIDA_MAX_w
FROM	medida_exame_laudo
WHERE   nr_sequencia = nr_seq_medida_w;	

if	(qt_medida_W < QT_MEDIDA_MIN_w) or
	(qt_medida_W > QT_MEDIDA_MAX_w) then
	return	'N';
else
	return	'S';
end if;

end	valida_medida_laudo;
/