create or replace 
function SUS_Obter_Valor (	nr_interno_conta_p	number,
				ie_valor_p		number) 
				return number is
vl_retorno_w	number(15,2);

BEGIN

select	sum(nvl(vl_sadt,0)) + sum(nvl(vl_ato_anestesista,0)) + sum(nvl(b.vl_medico,0)) + sum(nvl(vl_matmed,0))
into	vl_retorno_w
from	sus_valor_proc_paciente b,
	procedimento_paciente a
where	a.nr_sequencia		= b.nr_sequencia
  and	a.cd_motivo_exc_conta	is null
  and	a.nr_interno_conta	= nr_interno_conta_p
/*  and	a.cd_procedimento	= cd_procedimento_p
  and	a.ie_origem_proced	= ie_origem_proced_p*/;

return NVL(vl_retorno_w,0);
end;
/
