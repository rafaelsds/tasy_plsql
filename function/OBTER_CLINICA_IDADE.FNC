create or replace
FUNCTION obter_clinica_idade(	qt_idade_p	NUMBER)
 		    	RETURN NUMBER IS

ie_clinica_w	NUMBER(10);

BEGIN

SELECT	MAX(ie_clinica)
INTO	ie_clinica_w
FROM	regra_clinica_idade
WHERE	qt_idade_p BETWEEN qt_idade_min AND qt_idade_max
AND		(cd_estabelecimento = obter_estabelecimento_ativo or cd_estabelecimento is null);

RETURN	ie_clinica_w;

END obter_clinica_idade;
/
