create or replace
function pls_obter_abran_prest (	nr_seq_prestador_p		pls_prestador.nr_sequencia%type,
					nr_seq_segurado_p		pls_segurado.nr_sequencia%type,	
					nr_seq_outorgante_p		pls_outorgante.nr_sequencia%type,
					nr_seq_plano_p			pls_plano.nr_sequencia%type,
					ie_tipo_segurado_p		pls_segurado.ie_tipo_segurado%type,
					dt_emissao_p			date,
					ie_abrangencia_p		pls_plano.ie_abrangencia%type)
						return varchar2 is 
					
/*compara a abrang�ncia do prestador com a abrang�ncia do produto do benefici�rio. Se for informado a �rea de atua��o utilizar� ela, caso contr�rio seguir� o processo descrito abaixo.

Para a abrang�ncia municipal do produto, o munic�pio do prestador deve ser o mesmo da  cooperativa/operadora do benefici�rio.
J� para uma abrang�ncia estadual, o estado do prestador  deve ser o mesmo da  cooperativa/operadora do benefici�rio.
Para a nacional, ambas devem estar dentro do mesmo pa�s.
Para este tipo de processo  poder�o ser utilizadas todas as abrang�ncias descritas no PTU. Se for utilizada alguma referente a grupos, obrigatoriamente deve ser informado a �rea de atua��o.*/
					
qt_area_atuacao_w		pls_integer;
qt_benef_repas_val_w		pls_integer;
ie_area_coberta_w		varchar2(1);		
ie_tipo_prestador_w		varchar2(2);
cd_cogido_prestador_w		varchar2(22);
nr_seq_pais_prestador_w		pais.nr_sequencia%type;
nr_seq_pais_outorgante_w	pais.nr_sequencia%type;
cd_cgc_outorgante_w		pls_outorgante.cd_cgc_outorgante%type;
sg_estado_prest_w		pls_prestador_area.sg_estado%type;		

/* �reas de atua��o do plano */
Cursor c01(nr_seq_plano_p	pls_plano.nr_sequencia%type) is
	select	a.cd_municipio_ibge,
		a.sg_estado,
		a.nr_seq_regiao
	from	pls_plano_area a
	where	a.nr_seq_plano	= nr_seq_plano_p;

	
/*�rea de atua��o do prestador executor */
Cursor C02(nr_seq_prestador_p	pls_prestador.nr_sequencia%type) is
	select	a.cd_municipio_ibge,
		a.sg_estado,
		a.nr_seq_regiao
	from	pls_prestador_area a
	where	a.nr_seq_prestador	= nr_seq_prestador_p;
	
/* �reas de atua��o da operadora */
Cursor c03(nr_seq_outorgante_p	pls_outorgante.nr_sequencia%type) is
	select	a.cd_municipio_ibge,
		a.sg_uf_municipio,
		a.nr_seq_regiao
	from	pls_regiao_atuacao a
	where	a.nr_seq_operadora	= nr_seq_outorgante_p;
begin

/*Se n�o consistir nada abaixo, ir� retornar que n�o tem abrang�ncia por padr�o. Princ�pio da pior situa��o*/
ie_area_coberta_w	:= 'N';
qt_benef_repas_val_w	:= 1;
-- verifica��o de seguran�a
if	(ie_abrangencia_p is not null and nr_seq_prestador_p is not null and nr_seq_plano_p is not null)	then
	
	if	(ie_tipo_segurado_p = 'C')	then
		qt_benef_repas_val_w	:= 0;
	/*Verifica a validade do repasse de responsabilidade assumida*/
		select	count(1)
		into	qt_benef_repas_val_w
		from 	pls_segurado
		where 	nr_sequencia = nr_seq_segurado_p
		and	dt_emissao_p between dt_contratacao and dt_rescisao
		or	(dt_emissao_p >= dt_contratacao and dt_rescisao is null)
		and 	rownum <= 1;
	
	end if;
	
	/*Verificar se no sistema est� informado a area de atua��o do produto*/
	select	count(1)
	into	qt_area_atuacao_w
	from	pls_plano_area	a
	where	a.nr_seq_plano = nr_seq_plano_p;
	
	if	(qt_benef_repas_val_w > 0)	then
		
		/*Se a abrang�ncia for diferente de nacional*/
		if	(ie_abrangencia_p <> 'N')	then
			/*Se houver �rea de atua��o informada para o plano*/
			if	(qt_area_atuacao_w > 0) then
				
				for r_c01 in C01(nr_seq_plano_p) loop/*informa��es do plano r_C01*/
				

				
					for r_C02 in C02(nr_seq_prestador_p) loop/*informa��es do prestador executor do atendimento r_c02*/
									
						
									
									
						/*Compara o municipio do plano com o municipio do prestador*/
						if	(ie_abrangencia_p = 'M')	then
							if	(r_c01.cd_municipio_ibge = r_c02.cd_municipio_ibge)	then
					
								ie_area_coberta_w  := 'S';
								return ie_area_coberta_w;
							end if;			
						/*Compara a abrang�ncia estadual, estado do plano com o estado do prestador */
						elsif	(ie_abrangencia_p = 'E')	then
							if	(r_c01.sg_estado = r_c02.sg_estado)	then					
								ie_area_coberta_w := 'S';
								return ie_area_coberta_w;							
							/* verifica se o estado cadastrado na �rea de atua��o do plano pertence a regi�o cadastrada na �rea de atua��o do prestador */
							elsif (pls_obter_se_mun_uf_regiao(null, r_c01.sg_estado, r_c02.nr_seq_regiao) = 'S') then
								ie_area_coberta_w := 'S';
								return ie_area_coberta_w;
							/* verifica se o estado cadastrado na �rea de atua��o do prestador pertence a regi�o cadastrada na �rea de atua��o do plano */
							elsif (pls_obter_se_mun_uf_regiao(null, r_c02.sg_estado, r_c01.nr_seq_regiao) = 'S') then
								ie_area_coberta_w := 'S';
								return ie_area_coberta_w;
							/* verifica se o munic�pio cadastrado na �rea de atua��o do plano pertence ao estado cadastrado na �rea de atua��o do prestador */
							elsif ((r_c01.sg_estado is null) and (pls_obter_se_municipio_em_uf(r_c01.cd_municipio_ibge, r_c02.sg_estado)	= 'S')) then
								ie_area_coberta_w := 'S';
								return ie_area_coberta_w;							
							/* verifica se o munic�pio cadastrado na �rea de atua��o do prestador pertence ao estado cadastrado na �rea de atua��o do plano */
							elsif ((r_c02.sg_estado is null) and (pls_obter_se_municipio_em_uf(r_c02.cd_municipio_ibge, r_c01.sg_estado)	= 'S')) then
								ie_area_coberta_w := 'S';
								return ie_area_coberta_w;
							end if;
						elsif	(ie_abrangencia_p = 'GM')	then/*Grupo de m�nicipios*/

							if	(r_c01.nr_seq_regiao = r_c02.nr_seq_regiao) then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;
								
							elsif	(r_c01.sg_estado = r_c02.sg_estado)	then					
								ie_area_coberta_w := 'S';
								return ie_area_coberta_w;

							/*Se o munic�pio do prestador est�  na regi�o do plano*/
							elsif	(pls_obter_se_mun_uf_regiao(r_c02.cd_municipio_ibge, null, r_c01.nr_seq_regiao) = 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;

							/*Se o munic�pio  do plano est� na regi�o do prestador*/
							elsif	(pls_obter_se_mun_uf_regiao(r_c01.cd_municipio_ibge, null, r_c02.nr_seq_regiao) = 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;
								
							/*Se o estado do prestador est�  na regi�o do plano*/
							elsif	(pls_obter_se_mun_uf_regiao(null, r_c02.sg_estado, r_c01.nr_seq_regiao) = 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;

							/*Se o munic�pio do plano est� no estado do prestador*/
							elsif	(pls_obter_se_municipio_em_uf(r_c01.cd_municipio_ibge, r_c02.sg_estado)	= 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;

							/*Se o munic�pio do prestador est� no estado do plano*/
							elsif	(pls_obter_se_municipio_em_uf(r_c02.cd_municipio_ibge, r_c01.sg_estado)	= 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;

							elsif (r_c01.cd_municipio_ibge = r_c02.cd_municipio_ibge) then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;
							end if;
							
						-- se for por grupo de estados. Nesse caso, presume-se que as regi�es estejam cadastradas por estados e n�o por munic�pios.
						elsif	(ie_abrangencia_p = 'GE')	then
							
							-- Se no cadastro do prestador n�o estiver informado a UF e sim a cidade, ent�o busca a UF na qual essa cidade faz parte
							if	(r_c02.sg_estado is null) then
								sg_estado_prest_w := obter_uf_ibge(r_c02.cd_municipio_ibge);
							else
								sg_estado_prest_w := r_c02.sg_estado;
							end if;
							
							if	(r_c01.nr_seq_regiao = r_c02.nr_seq_regiao) then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;
							
							/*Se o estado do munic�pio do plano est� na regi�o do prestador*/
							elsif	(pls_obter_se_mun_uf_regiao(null, obter_uf_ibge(r_c01.cd_municipio_ibge), r_c02.nr_seq_regiao) = 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;

							/*Se o estado do munic�pio do prestador est� na regi�o do plano*/
							elsif	(pls_obter_se_mun_uf_regiao(null, obter_uf_ibge(r_c02.cd_municipio_ibge), r_c01.nr_seq_regiao) = 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;							

							/*Se o  estado da  prestador est� na regi�o do plano*/		
							elsif	(pls_obter_se_mun_uf_regiao(null, sg_estado_prest_w ,r_c01.nr_seq_regiao) = 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;
								
							/*Se  o estado do plano est� na regi�o do perstador*/
							elsif	(pls_obter_se_mun_uf_regiao(null,r_c01.sg_estado ,r_c02.nr_seq_regiao) = 'S') then
								ie_area_coberta_w	:= 'S';
								return ie_area_coberta_w;
							end if;
						end if;
					end loop;
				end loop;
				
			-- Informa��o da operadora que recebeu a conta do prestador
			elsif	(nr_seq_outorgante_p	is not null)	then
				
				for r_c03 in C03(nr_seq_outorgante_p) loop/*informa��es da operadora  outorgante r_c03*/
					for r_C02 in C02(nr_seq_prestador_p) loop/*informa��es da prestador executor do atendimento r_c02*/
						ie_area_coberta_w	:= 'S';
						/*Compara o municipio da operadora com o municipio do prestador*/
						if	(ie_abrangencia_p = 'M')	then
							if	(r_c03.cd_municipio_ibge = r_c02.cd_municipio_ibge)	then
								ie_area_coberta_w  := 'S';
								return ie_area_coberta_w;
							end if;
						/*Compara a abrang�ncia estadual, compara estado da operadora com o estado do prestador */
						elsif	(ie_abrangencia_p = 'E')	then
							if	(r_c03.sg_uf_municipio = r_c02.sg_estado)	then
								ie_area_coberta_w := 'S';
								return ie_area_coberta_w;
							end if;
						end if;
					end loop;
				end loop;
			end if;
		elsif	(ie_abrangencia_p = 'N')	then /*Se a abrang�ncia for nacional*/
			/*Obter se o prestador � PF ou PJ*/
			select	decode(max(a.cd_cgc), null, 'PF', 'PJ')
			into	ie_tipo_prestador_w
			from	pls_prestador	a
			where	a.nr_sequencia = nr_seq_prestador_p;
			
			/*Buscar o CGC do outorgante*/
			select 	max(x.cd_cgc_outorgante)
			into	cd_cgc_outorgante_w
			from	pls_outorgante	x
			where	x.nr_sequencia = nr_seq_outorgante_p;
				
			/*Se o tipo de prestador for pessoa f�sica buscar� o pa�s da tabela PESSOA_FISICA*/
			if	(ie_tipo_prestador_w	= 'PF')	then
				/*Buscar o  do prestador*/
				select	max(x.cd_pessoa_fisica)
				into 	cd_cogido_prestador_w
				from	pls_prestador	x
				where	x.nr_sequencia = nr_seq_prestador_p;
				
			/*Se o tipo de prestador for pessoa jur�dica buscar� o pa�s da tabela PESSOA_JURIDICA*/
			elsif	(ie_tipo_prestador_w	= 'PJ')	then
				select	max(x.cd_cgc)
				into 	cd_cogido_prestador_w
				from	pls_prestador	x
				where	x.nr_sequencia = nr_seq_prestador_p;
				
			end if;
			
			/*Buscar os pa�ses*/
			nr_seq_pais_prestador_w		:= pls_obter_pais_abrang(cd_cogido_prestador_w,ie_tipo_prestador_w);
			nr_seq_pais_outorgante_w	:= pls_obter_pais_abrang(cd_cgc_outorgante_w,'PJ');
			
			
			
			/*Conforme visto com D�cio,  se n�o encontrar valor para o pa�s do prestador n�o ir� validar a glosa DRQUADROS  O.S 604052*/
			if	(nr_seq_pais_prestador_w is  not null)	then
				if	(nr_seq_pais_prestador_w = nr_seq_pais_outorgante_w)	then
					ie_area_coberta_w := 'S';
					return ie_area_coberta_w;
				end if;
			else
				ie_area_coberta_w	:= 'S';
				return ie_area_coberta_w;
			end if;
		end if;

	end if;
end if;
return ie_area_coberta_w;	

end pls_obter_abran_prest;
/
