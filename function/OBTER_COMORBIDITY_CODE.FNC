create or replace
function obter_comorbidity_code(nr_seq_hist_rotina_w	varchar2,
								nm_atributo_w	varchar2)
								return varchar2 is

cd_conversao_w	conversao_meio_externo.cd_externo%type;

BEGIN

select	max(cd_externo)
into	cd_conversao_w
from	conversao_meio_externo
where	nm_tabela	= 'TIPO_HIST_SAUDE_GRUPO'
and	nm_atributo	= nm_atributo_w
and	cd_interno	= nr_seq_hist_rotina_w;
	
return cd_conversao_w;

end obter_comorbidity_code;
/
