create or replace
function Obter_Resul_Ranson(qt_pontuacao_p		number)
 		    	return varchar2 is

begin

if	(qt_pontuacao_p	<=2) then
	return '0.9 %';
elsif	(qt_pontuacao_p	between 3 and 4) then
	return '16 %';
elsif	(qt_pontuacao_p	between 5 and 6) then
	return '40 %';
else
	return '100 %';
end if;
	

end Obter_Resul_Ranson;
/