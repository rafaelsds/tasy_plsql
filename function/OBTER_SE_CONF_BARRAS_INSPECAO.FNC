create or replace
function OBTER_SE_CONF_BARRAS_INSPECAO(		cd_estabelecimento_p		number,
						cd_material_p			number)
 		    	return varchar2 is


cd_grupo_material_w			grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w			subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w			classe_material.cd_classe_material%type;
ie_conferencia_w				regra_conf_barras_inspecao.ie_conferencia%type := 'N';
			
cursor c01 is
select	nvl(ie_conferencia,'S')
from	regra_conf_barras_inspecao
where	cd_estabelecimento = cd_estabelecimento_p
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(cd_material, cd_material_p) 			= cd_material_p or cd_material_p = 0)
order by
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);			
			
begin


select	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v a
where	a.cd_material = cd_material_p;

open C01;
loop
fetch C01 into	
	ie_conferencia_w;
exit when C01%notfound;
	begin
	ie_conferencia_w := ie_conferencia_w;
	end;
end loop;
close C01;

return	ie_conferencia_w;

end obter_se_conf_barras_inspecao;
/
