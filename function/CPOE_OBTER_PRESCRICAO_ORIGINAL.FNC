create or replace
function cpoe_obter_prescricao_original(nr_sequencia_p		number,
										ie_tipo_item_p		varchar2,
										nr_atendimento_p	number)
										return number is

nr_prescr_retorno_w number(15) := 0;
nr_pres_original_w	number(15) := 0;

begin

if	(nr_sequencia_p is not null) and
	(nr_sequencia_p > 0 ) and
	(nr_atendimento_p is not null) and
	(nr_atendimento_p > 0) then
		
	if	(ie_tipo_item_p = 'M') then --Medicamento

		select  nvl(min(a.nr_prescricao),0)
		into	nr_pres_original_w
		from  	prescr_material a
		where  	a.nr_seq_mat_cpoe = nr_sequencia_p;

	elsif	(ie_tipo_item_p = 'N') then --Nutricao

		select	nvl(min(a.nr_prescricao),0)		--Oral
		into	nr_pres_original_w
		from	prescr_dieta a
		where	a.nr_seq_dieta_cpoe = nr_sequencia_p;
		
		if	(nr_pres_original_w is null) or
			(nr_pres_original_w = 0) then

				select	nvl(min(a.nr_prescricao),0)	--Enteral
				into	nr_pres_original_w
				from	prescr_material	a
				where   nr_seq_dieta_cpoe = nr_sequencia_p
				and		a.ie_agrupador = 8;
		end if;		

		if	(nr_pres_original_w is null) or
			(nr_pres_original_w = 0) then

				select	nvl(min(a.nr_prescricao),0) --Jejum
				into	nr_pres_original_w
				from	rep_jejum a
				where	a.nr_seq_dieta_cpoe = nr_sequencia_p;

		end if;				
		
		if	(nr_pres_original_w is null) or
			(nr_pres_original_w = 0) then

				select	nvl(min(a.nr_prescricao),0) --Suplemento
				into	nr_pres_original_w
				from	prescr_material a
				where   a.nr_seq_dieta_cpoe = nr_sequencia_p
				and		a.ie_agrupador = 12;

		end if;
		
		
		if	(nr_pres_original_w is null) or
			(nr_pres_original_w = 0) then

				select	nvl(min(a.nr_prescricao),0) --Leites e Derivados
				into	nr_pres_original_w 
				from	prescr_leite_deriv a
				where   a.nr_seq_dieta_cpoe = nr_sequencia_p;	

		end if;
		
		if	(nr_pres_original_w is null) or
			(nr_pres_original_w = 0) then

				select	nvl(min(a.nr_prescricao),0) --Parenteral Infantil 
				into	nr_pres_original_w
				from	nut_pac a
				where   a.nr_seq_npt_cpoe = nr_sequencia_p
				and 	a.ie_npt_adulta = 'P';
		end if;
		
		if	(nr_pres_original_w is null) or
			(nr_pres_original_w = 0) then

				select	nvl(min(a.nr_prescricao),0)	--Parenteral Adulto
				into	nr_pres_original_w
				from	nut_pac a,
						prescr_medica b		
				where   a.nr_seq_npt_cpoe = nr_sequencia_p
				and 	a.ie_npt_adulta = 'S';

		end if;		

		
	elsif	(ie_tipo_item_p = 'R') then	

		select	nvl(min(a.nr_prescricao),0)		--Recomendacao
		into	nr_pres_original_w
		from	prescr_recomendacao a
		where   a.nr_seq_rec_cpoe = nr_sequencia_p;

	elsif	(ie_tipo_item_p = 'G') then --Gasoterapia

		select	nvl(min(a.nr_prescricao),0)	
		into	nr_pres_original_w
		from	prescr_gasoterapia a
		where   a.nr_seq_gas_cpoe = nr_sequencia_p;

	elsif	(ie_tipo_item_p = 'P') then --Procedimento

		select	nvl(min(a.nr_prescricao),0)	
		into	nr_pres_original_w
		from	prescr_procedimento a
		where   a.nr_seq_proc_cpoe = nr_sequencia_p;

	elsif	(ie_tipo_item_p = 'D') then --Dialise

		select	nvl(min(a.nr_prescricao),0)	
		into	nr_pres_original_w
		from	hd_prescricao a
		where   a.nr_seq_dialise_cpoe = nr_sequencia_p;

	end if;
	
	if	(nr_pres_original_w > 0 ) then		
		nr_prescr_retorno_w := nr_pres_original_w;
	else
		nr_prescr_retorno_w := null;
	end if;
	
end if;	

return	nr_prescr_retorno_w;

end cpoe_obter_prescricao_original;
/
