create or replace function cpoe_obter_qtd_exame_vinculado(nr_seq_proc_interno_p 	number
        ) return INTEGER is

qt_retorno_w		INTEGER := 0;

begin

	select	count(*)
	into	qt_retorno_w
	from	exame_laboratorio
	where	nr_seq_proc_interno = nr_seq_proc_interno_p;

return qt_retorno_w;

end cpoe_obter_qtd_exame_vinculado;
/
