create or replace
function obter_cor_pele_cross(nr_seq_cor_pele_p	number)
 		    	return number is

nr_retorno_w number(3);
				
begin

select	cd_raca_cross
into	nr_retorno_w
from	cor_pele
where	nr_sequencia = nr_seq_cor_pele_p;

return	nr_retorno_w;

end obter_cor_pele_cross;
/