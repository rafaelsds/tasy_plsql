create or replace function obtain_active_cancel_date(nr_seq_lote_p number, cd_resp_type_p varchar2) 
return date 
is 
	dt_review_accepted_cancelled_w ap_lote_review.DT_REVIEW_ACCEPTED%type;
begin
	dt_review_accepted_cancelled_w := null;
	
	if ( nr_seq_lote_p is not null ) then
		if ( nvl(cd_resp_type_p, 'NA') = 'A' ) then
		   select nvl(DT_REVIEW_ACCEPTED, null) into dt_review_accepted_cancelled_w 
		   from ap_lote_review where nr_seq_lote = nr_seq_lote_p;
		elsif ( nvl(cd_resp_type_p, 'NA') = 'C' ) then
		   select nvl(DT_REVIEW_CANCELLED, null) into dt_review_accepted_cancelled_w 
		   from ap_lote_review where nr_seq_lote = nr_seq_lote_p;
		else
		   dt_review_accepted_cancelled_w := null;
		end if;
	end if;
return dt_review_accepted_cancelled_w;
end obtain_active_cancel_date;
/