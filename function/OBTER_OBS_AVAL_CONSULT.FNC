create or replace
FUNCTION obter_obs_aval_consult(
				nr_seq_quesito_p	NUMBER,
				nr_seq_proj_p		NUMBER,
				cd_consultor_p		NUMBER)
 		    	RETURN VARCHAR2 IS

observacoes_w		VARCHAR2(4000);
observacao_w		VARCHAR2(500);


CURSOR C01 IS
	SELECT 	substr(wheb_mensagem_pck.get_texto(300252,	'DT_AVALIACAO=' || b.dt_avaliacao || ';' ||
								'DS_OBSERVACAO=' || a.ds_observacao),1,4000)
	FROM      proj_consultor_aval_quesito	a,
		  proj_consultor_aval	b
	WHERE     b.nr_sequencia	= a.nr_seq_avaliacao
	AND	  cd_consultor		= cd_consultor_p
	AND	  a.ds_observacao IS NOT NULL
	AND	  TO_NUMBER(TO_CHAR(b.dt_avaliacao, 'mm')) > (TO_NUMBER(TO_CHAR(SYSDATE, 'mm'))-5)
	AND	  b.nr_seq_proj = nr_seq_proj_p
	AND	  a.nr_seq_quesito = nr_seq_quesito_p;

BEGIN
OPEN C01;
LOOP
FETCH C01 INTO
	observacao_w;
EXIT WHEN C01%NOTFOUND;
	BEGIN
	observacoes_w	:= SUBSTR(observacao_w || '  ' || observacoes_w,1,4000);
	END;
END LOOP;
CLOSE C01;

RETURN	observacoes_w;

END obter_obs_aval_consult;
/