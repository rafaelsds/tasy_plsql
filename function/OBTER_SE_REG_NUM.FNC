create or replace
function Obter_Se_reg_num(	ie_tipo_numeracao_p	varchar2,
				nr_declaracao_p		number)
 		    	return varchar2 is
			
			
ds_retorno_w	varchar2(10)	:= 'S';
qt_registro_w	number(10);			

begin


select	count(*)
into	qt_registro_w
from	regra_numeracao_declaracao a,
	REGRA_NUMERACAO_DEC_ITEM b
where	a.nr_sequencia	= b.NR_SEQ_REGRA_NUM	
and	a.ie_situacao	= 'A'
and	a.ie_tipo_numeracao	= ie_tipo_numeracao_p;


if	(qt_registro_w	> 0) and
	(nr_declaracao_p is not null) then
	select	count(*)
	into	qt_registro_w
	from	regra_numeracao_declaracao a,
		REGRA_NUMERACAO_DEC_ITEM b
	where	a.nr_sequencia	= b.NR_SEQ_REGRA_NUM	
	and	a.ie_situacao	= 'A'
	and	a.ie_tipo_numeracao	= ie_tipo_numeracao_p
	and	nr_declaracao	= nr_declaracao_p
	and	IE_DISPONIVEL	= 'S';
	
	if	(qt_registro_w	= 0) then
		ds_retorno_W	:= 'N';
	end if;
end if;

return	ds_retorno_w;

end Obter_Se_reg_num;
/
