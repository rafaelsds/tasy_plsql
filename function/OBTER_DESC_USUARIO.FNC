create or replace
function Obter_Desc_Usuario
		(nm_usuario_p		varchar2) 
		return Varchar2 is

ds_retorno_w	varchar2(100);

begin

if	(nm_usuario_p is not null) then
	begin

	select	ds_usuario
	into	ds_retorno_w
	from	usuario
	where	nm_usuario	= nm_usuario_p;

	end;
end if;

return	ds_retorno_w;

end Obter_Desc_Usuario;
/