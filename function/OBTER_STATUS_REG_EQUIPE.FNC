create or replace
function obter_status_reg_equipe(nr_seq_equipe_p	number)
 		    	return varchar2 is

ds_retorno_w			varchar(100) := wheb_mensagem_pck.get_texto(309490); -- Liberada
ie_status_liberada_w		number(10);
ie_status_not_liberada_w	number(10);
nr_sequencia_w			number(10);
ie_situacao_w			varchar2(2);
/*J9 a J12*/
dt_partida_w		date;
dt_chegada_local_w	date;
dt_saida_local_w	date;
dt_chegada_base_w	date;
nr_seq_motivo_parada_w	number(10);
ie_almoco_w		varchar2(2);

begin


select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	eme_reg_resposta a
where	nr_seq_equipe = nr_seq_equipe_p;

select	nvl(max(nr_sequencia),0)
into	ie_status_liberada_w
from	eme_reg_resposta a
where	nr_seq_equipe = nr_seq_equipe_p
and    ((dt_partida is null and dt_chegada_local is null and dt_saida_local is null and dt_chegada_base is null) or
	   (dt_partida is not null and dt_chegada_local is not null and dt_saida_local is not null and dt_chegada_base is null) or
	   (dt_partida is not null and dt_chegada_local is not null and dt_saida_local is not null and dt_chegada_base is not null))
and	nr_sequencia = nr_sequencia_w;

select	max(dt_partida),
	max(dt_chegada_local),
	max(dt_saida_local),
	max(dt_chegada_base)
into	dt_partida_w,		
        dt_chegada_local_w,	
        dt_saida_local_w,	
        dt_chegada_base_w
from	EME_REG_RESPOSTA
where	nr_seq_equipe = nr_seq_equipe_p
and	nr_Sequencia = nr_sequencia_w;	

select	nvl(max(nr_sequencia),0)
into	ie_status_not_liberada_w
from	eme_reg_resposta a
where	nr_seq_equipe = nr_seq_equipe_p
and	   ((dt_partida is not null and dt_chegada_local is null and dt_saida_local is null and dt_chegada_base is null) or
	   (dt_partida is not null and dt_chegada_local is not null and dt_saida_local is null and dt_chegada_base is null))
and	nr_sequencia = nr_sequencia_w;

select	NVL(max(ie_almoco),'N'),
	nvl(max(nr_seq_motivo_parada),0)
into	ie_almoco_w,
	nr_seq_motivo_parada_w
from	pf_equipe
where	nr_sequencia = nr_seq_equipe_p;

select 	ie_situacao 
into	ie_situacao_w
from	pf_equipe
where	nr_sequencia = nr_seq_equipe_p;

if (ie_situacao_w = 'A') then 
	if	(dt_partida_w is not null) and
		(dt_chegada_local_w is null) then
		ds_retorno_w	:= 'J9';
	elsif 	(dt_chegada_local_w is not null) and
		(dt_saida_local_w is null) then
		ds_retorno_w	:= 'J10';
	elsif 	(dt_saida_local_w is not null) and
		(dt_chegada_base_w is null) then
		ds_retorno_w	:= 'J11';
	elsif	(ie_almoco_w = 'S') then
		ds_retorno_w := 'J4';
	elsif	(nr_seq_motivo_parada_w <> 0) then
		ds_retorno_w := 'J7';
	elsif	(dt_chegada_base_w is not null) then
		ds_retorno_w := 'J12';		
	end if;
end if;
	
return	ds_retorno_w;

end obter_status_reg_equipe;
/
