create or replace
function OBTER_CONTA_BANCO (nr_sequencia_p	number) return varchar2 is

ds_conta_w	varchar2(254);

begin
if ( nvl(nr_sequencia_p,0) <> 0 ) then
	select	substr(max(ds_conta),1,254)
	into	ds_conta_w
	from	banco_estabelecimento_v
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_conta_w;

end;
/
