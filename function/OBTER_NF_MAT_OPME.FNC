create or replace
function obter_nf_mat_opme(nr_seq_material_p		number,
			ie_opcao_p		varchar2) return varchar2 is

/*
 N - Nr Nota Fiscal
 D - Data de Emiss�o
*/

nr_nota_fiscal_w		varchar2(255);
dt_emissao_w		date;
ds_retorno_w		varchar2(150);

BEGIN

select	max(nr_nota_fiscal),
	max(dt_emissao)
into	nr_nota_fiscal_w,
	dt_emissao_w
from	mat_atend_paciente_opme
where	nr_seq_material	= nr_seq_material_p;

if	(ie_opcao_p = 'N') then
	ds_retorno_w := substr(nr_nota_fiscal_w,1,150);
elsif	(ie_opcao_p = 'D') then
	ds_retorno_w := dt_emissao_w;
end if;

return	ds_retorno_w;

end obter_nf_mat_opme;
/