create or replace
function obter_intervalo_med_rot(	ie_opcao_p		varchar2,
									cd_intervalo_p	varchar2)
 		    	return varchar2 is

cd_intervalo_w		varchar2(10);

begin
if	(ie_opcao_p = 'ACM') then
	select  max(cd_intervalo)
	into	cd_intervalo_w
	from 	intervalo_prescricao
    where   ie_acm = 'S'
    and     ie_situacao = 'A'
	and     cd_intervalo = cd_intervalo_p
	and     Obter_se_intervalo(cd_intervalo, 1) = 'S';
	
	if	(cd_intervalo_w is null) then
		select  max(cd_intervalo)
		into	cd_intervalo_w
		from 	intervalo_prescricao
		where   ie_acm = 'S'
		and     ie_situacao = 'A'
		and     Obter_se_intervalo(cd_intervalo, 1) = 'S';
	end if;
elsif	(ie_opcao_p = 'SN') then
	select  max(cd_intervalo)
	into	cd_intervalo_w
	from 	intervalo_prescricao
    where   ie_se_necessario = 'S'
    and     ie_situacao = 'A'
	and     cd_intervalo = cd_intervalo_p
	and     Obter_se_intervalo(cd_intervalo, 1) = 'S';
	
	if	(cd_intervalo_w is null) then
		select  max(cd_intervalo)
		into	cd_intervalo_w
		from 	intervalo_prescricao
		where   ie_se_necessario = 'S'
		and     ie_situacao = 'A'
		and     Obter_se_intervalo(cd_intervalo, 1) = 'S';
	end if;
end if;

return	cd_intervalo_w;

end obter_intervalo_med_rot;
/