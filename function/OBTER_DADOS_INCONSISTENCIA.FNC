create or replace
function Obter_Dados_Inconsistencia
		(nr_seq_inconsistencia_p	number,
		cd_estabelecimento_p		number,
		cd_perfil_p			number,
		ie_valor_p			varchar2,
		ie_campo_p			varchar2)
		return	varchar2 is

IE_FECHA_CONTA_w		varchar2(01);
IE_FECHA_ATENDIMENTO_w		varchar2(01);
IE_FECHA_PROTOCOLO_w		varchar2(01);
IE_FECHA_CONTA_estab_w		varchar2(01);
IE_FECHA_ATENDIMENTO_estab_w	varchar2(01);
IE_FECHA_PROTOCOLO_estab_w	varchar2(01);
ie_valor_w			varchar2(01);

begin

select	max(IE_FECHA_CONTA),
	max(IE_FECHA_ATENDIMENTO),
	max(IE_FECHA_PROTOCOLO)
into	IE_FECHA_CONTA_estab_w,
	IE_FECHA_ATENDIMENTO_estab_w,
	IE_FECHA_PROTOCOLO_estab_w
from	inconsistencia_estab
where	cd_estabelecimento	= cd_estabelecimento_p
and	nr_seq_inconsistencia	= nr_seq_inconsistencia_p;


select	max(IE_FECHA_CONTA),
	max(IE_FECHA_ATENDIMENTO),
	max(IE_FECHA_PROTOCOLO)
into	IE_FECHA_CONTA_w,
	IE_FECHA_ATENDIMENTO_w,
	IE_FECHA_PROTOCOLO_w
from	inconsistencia_perfil
where	cd_estabelecimento	= cd_estabelecimento_p
and	cd_perfil		= cd_perfil_p
and	nr_seq_inconsistencia	= nr_seq_inconsistencia_p;


if	(ie_campo_p	= 'IE_FECHA_CONTA') then
	ie_valor_w	:= nvl(nvl(IE_FECHA_CONTA_w,IE_FECHA_CONTA_estab_w), ie_valor_p);
elsif	(ie_campo_p	= 'IE_FECHA_ATENDIMENTO') then
	ie_valor_w	:= nvl(nvl(IE_FECHA_ATENDIMENTO_w, IE_FECHA_ATENDIMENTO_estab_w), ie_valor_p);
elsif	(ie_campo_p	= 'IE_FECHA_PROTOCOLO') then
	ie_valor_w	:= nvl(nvl(IE_FECHA_PROTOCOLO_w, IE_FECHA_PROTOCOLO_estab_w), ie_valor_p);
end if;

return	ie_valor_w;

end Obter_Dados_Inconsistencia;
/