create or replace
function pls_obter_seq_proc_min_concorr(	nr_seq_regra_p		Number,
						nr_seq_guia_p		Number,
						nr_seq_requisicao_p	Number,
						nr_seq_execucao_p	Number)
						return Number is

nr_seq_retorno_w		Number(10) 	:= 0;
cd_proc_concorrente_w		Number(10);
ie_origem_proced_concorr_w	Number(1);
nr_seq_req_proc_w		Number(10);
cd_proc_req_w			Number(10);
ie_orig_proc_req_w		Number(1);
nr_seq_min_w			Number(10)	:= 9999999999;
nr_seq_guia_proc_w		Number(10);
cd_proc_guia_w			Number(10);
ie_orig_proc_guia_w		Number(1);
nr_seq_exec_proc_w		Number(10);
cd_proc_exec_w			Number(10);
ie_orig_proc_exec_w		Number(1);

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced
	from	pls_proc_concorrente
	where	nr_seq_regra 		= nr_seq_regra_p
	and	ie_ocorrencia		= 'S'
	and	ie_situacao		= 'A';
	
Cursor C02 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C03 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_p;
	
Cursor C04 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_execucao_req_item
	where	nr_seq_execucao	= nr_seq_execucao_p;
	
begin

open C01;
loop
fetch C01 into	
	cd_proc_concorrente_w,
	ie_origem_proced_concorr_w;
exit when C01%notfound;
	if 	(nr_seq_guia_p is not null) then
		begin
		
		open C03;
		loop
		fetch C03 into	
			nr_seq_guia_proc_w,
			cd_proc_guia_w,
			ie_orig_proc_guia_w;
		exit when C03%notfound;
			begin
			if 	(cd_proc_concorrente_w		= cd_proc_guia_w) and
				(ie_origem_proced_concorr_w	= ie_orig_proc_guia_w) then
				if 	(nr_seq_min_w > nr_seq_guia_proc_w) then
					nr_seq_min_w 	:= nr_seq_guia_proc_w;
				end if;
			end if;
			end;
		end loop;
		close C03;
		
		end;
	elsif 	(nr_seq_requisicao_p is not null) then
		begin
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_req_proc_w,
			cd_proc_req_w,
			ie_orig_proc_req_w;
		exit when C02%notfound;
			begin
			if 	(cd_proc_concorrente_w		= cd_proc_req_w) and
				(ie_origem_proced_concorr_w	= ie_orig_proc_req_w) then
				if 	(nr_seq_min_w > nr_seq_req_proc_w) then
					nr_seq_min_w 	:= nr_seq_req_proc_w;
				end if;
			end if;
			end;
		end loop;
		close C02;
		
		end;
	elsif	(nr_seq_execucao_p is not null) then

		open C04;
		loop
		fetch C04 into	
			nr_seq_exec_proc_w,
			cd_proc_exec_w,
			ie_orig_proc_exec_w;
		exit when C04%notfound;
			begin
			if 	(cd_proc_concorrente_w		= cd_proc_exec_w) and
				(ie_origem_proced_concorr_w	= ie_orig_proc_exec_w) then
				if 	(nr_seq_min_w > nr_seq_exec_proc_w) then
					nr_seq_min_w 	:= nr_seq_exec_proc_w;
				end if;
			end if;
			end;
		end loop;
		close C04;
	
	end if;
end loop;
close C01;

if 	(nr_seq_min_w = 999999999) then
	nr_seq_retorno_w	:= 0;
else
	nr_seq_retorno_w	:= nr_seq_min_w;
end if;
	
return	nr_seq_retorno_w;

end pls_obter_seq_proc_min_concorr;
/
