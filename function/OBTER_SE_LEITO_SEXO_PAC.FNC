create or replace
function obter_se_leito_sexo_pac( cd_setor_atendimento_p	number,
				  cd_unidade_basica_p		varchar2,
				  cd_unidade_compl_p		varchar2,
				  nr_atendimento_p		number)
				  
 		    	return varchar2 is

ie_sexo_compativel_w	varchar2(1);
ie_sexo_unid_w		varchar2(1);
ie_sexo_fixo_w		varchar2(1);
ie_sexo_pf_w		varchar2(1);

begin

ie_sexo_compativel_w := 'S';

if	(nvl(nr_atendimento_p,0) > 0) then

	select	nvl(ie_sexo_paciente,'L'),
		nvl(ie_sexo_fixo,'N')
	into	ie_sexo_unid_w,
		ie_sexo_fixo_w
	from 	unidade_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_p
	and	cd_unidade_basica = cd_unidade_basica_p
	and	cd_unidade_compl  = cd_unidade_compl_p;
	
	select	a.ie_sexo
	into	ie_sexo_pf_w
	from	pessoa_fisica a,
		atendimento_paciente b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	b.nr_atendimento = nr_atendimento_p;
	
	
	
	if	(((ie_sexo_fixo_w = 'S') and
		 (upper(ie_sexo_unid_w) = upper(ie_sexo_pf_w))) or
		 (ie_sexo_fixo_w = 'N') or
		 (ie_sexo_unid_w in ('L','A'))) then
		
		ie_sexo_compativel_w := 'S';
	else
		ie_sexo_compativel_w := 'N';
	end if;

end if;

return	ie_sexo_compativel_w;

end obter_se_leito_sexo_pac;
/