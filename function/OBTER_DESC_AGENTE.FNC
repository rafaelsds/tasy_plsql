create or replace
function obter_desc_agente(	nr_sequencia_p	number )
			return varchar2 is

ds_agente_w	varchar2(60);

begin

select	ds_agente
into	ds_agente_w
from	agente_anestesico
where	nr_sequencia = nr_sequencia_p;

return	ds_agente_w;

end obter_desc_agente;
/