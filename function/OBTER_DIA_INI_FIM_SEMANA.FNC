create or replace
function obter_dia_ini_fim_semana ( dt_mes_ref_p	date,
				   vl_dominio_p	number)
				   return varchar2 is
			
--vetor
type qt_semana_w is record (dia_w date);
type vetor is table of qt_semana_w index by binary_integer;
			
vetor_w			vetor;
ie_retorno_w		varchar2(60);
i 			number(3)   	:=0;
ind 			number(3)   	:=1;
dt_dia_w 		date 	:= trunc(dt_mes_ref_p,'month');
dt_inicio_semana_w 	date;
dt_fim_semana_w 	date;
ie_dia_sem_w	varchar2(10);

begin

while (dt_dia_w <= last_day(trunc(dt_mes_ref_p,'dd'))) loop
	ie_dia_sem_w	:= obter_cod_dia_semana(dt_dia_w);
/*	if	(philips_param_pck.get_nr_seq_idioma = 2) then -- Espanhol  - M�xico(MX)
		if	(ie_dia_sem_w = 7) then
			ie_dia_sem_w := 1;
		else
			ie_dia_sem_w	:= ie_dia_sem_w + 1;
		end if;
	end if;
*/	
	if (ie_dia_sem_w = 1) then
		begin
		i := i +1;
		vetor_w(i).dia_w := trunc(dt_dia_w);
		end;
	end if;
	dt_dia_w:= trunc(dt_dia_w) +1;
end loop;

ind := 1;	
while	(ind <= i) loop
	if 	(vl_dominio_p = ind) then
		dt_inicio_semana_w  :=  obter_inicio_fim_semana(trunc(vetor_w(ind).dia_w),'I');
		dt_fim_semana_w     :=  obter_inicio_fim_semana(trunc(vetor_w(ind).dia_w),'F');
		exit;
	end if;	
	ind := 	ind + 1;
end loop;

ie_retorno_w	:= 	dt_inicio_semana_w || ' - ' ||
			dt_fim_semana_w;

return	ie_retorno_w;

end obter_dia_ini_fim_semana;
/ 
