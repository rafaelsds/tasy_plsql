create or replace
function obter_se_existe_mes(cd_empresa_p	number,
			     dt_referencia_p	date)
 		    	return varchar2 is
ds_retorno_w	varchar2(1) := 'N';
qt_registros_w	number(10);
begin


select 	count(*)
into	qt_registros_w
from	ctb_mes_ref
where	cd_empresa 	= cd_empresa_p
and	dt_referencia 	= dt_referencia_p;

if (qt_registros_w > 0 ) then
	ds_retorno_w	:= 'S';
end if;


return	ds_retorno_w;

end obter_se_existe_mes;
/