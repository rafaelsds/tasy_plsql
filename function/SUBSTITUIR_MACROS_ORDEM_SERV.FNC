create or replace
function substituir_macros_ordem_serv
			(	ds_texto_p		varchar2,
				nr_seq_ordem_p		number)
				return varchar2 is

dt_atual_w		date;
nr_hora_atual_w		number(10);				
ds_texto_w		varchar2(8000);
nm_solicitante_w	varchar2(255);
ds_periodo_w		varchar2(255);
cd_pessoa_solicitante_w	varchar2(10);
				
begin
ds_texto_w 		:= ds_texto_p;
dt_atual_w		:= sysdate;
nr_hora_atual_w 	:= to_number(to_char(dt_atual_w,'hh24'));

select	max(a.cd_pessoa_solicitante)
into	cd_pessoa_solicitante_w
from	man_ordem_servico	a
where	a.nr_sequencia	= nr_seq_ordem_p;

select	substr(obter_primeiro_nome(obter_nome_pf(cd_pessoa_solicitante_w)),1,120) nm_solicitante
into	nm_solicitante_w
from	dual;

if	(nr_hora_atual_w >= 6) and
	(nr_hora_atual_w < 12) then
	ds_periodo_w	:= wheb_mensagem_pck.get_texto(318817);
elsif	(nr_hora_atual_w >= 12) and
	(nr_hora_atual_w <18) then
	ds_periodo_w	:= wheb_mensagem_pck.get_texto(318818);	
else 
	ds_periodo_w	:= wheb_mensagem_pck.get_texto(318819);
end if;

ds_texto_w	:= substr(replace_macro(ds_texto_w,'@nm_solicitante_os', nm_solicitante_w),1,8000);
ds_texto_w	:= substr(replace_macro(ds_texto_w,'@periodo_dia', ds_periodo_w),1,8000);

return	ds_texto_w;

end substituir_macros_ordem_serv;
/