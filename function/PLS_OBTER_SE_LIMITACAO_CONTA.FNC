create or replace
function pls_obter_se_limitacao_conta	
			(	nr_seq_segurado_p		Number,
				nr_seq_tipo_limitacao_p		Number,
				qt_permitida_p			Number,
				qt_intervalo_p			Number,
				ie_periodo_p			Varchar2,
				qt_solicitada_p			Number,
				nr_seq_conta_p			Number,
				ie_tipo_periodo_p		varchar2)
				return Varchar2 is
				
ie_limitacao_w			Varchar2(1);
dt_conta_w			Date;
nr_seq_titular_w		Number(10);
nr_seq_contrato_w		Number(10);
dt_contratacao_w		Date;
dt_atual_w			Date;
dt_inicio_w			Date;
dt_inicial_w			Date;
dt_final_w			Date;
ie_tipo_incidencia_w		Varchar2(1);
nr_seq_conta_w			Number(10);
nr_seq_segurado_w		Number(10);
nr_seq_conta_proc_w		Number(10);
qt_procedimento_w		Number(12,4);
qt_liberada_w			Number(7,3) := 0;
qt_permitida_w			Number(9,3);

/*DIEGO 29/07/2011 - ESTA FUNCTION FOI SUBSTITUIDA PELA PROCEDURE PLS_SE_LIMITACAO_CONTA.*/

Cursor C01 is
	select	b.nr_sequencia,
		a.nr_sequencia
	from	pls_conta	b,
		pls_segurado 	a
	where	a.nr_sequencia	= b.nr_seq_segurado
	and	a.nr_sequencia	= nr_seq_segurado_p
	and	b.ie_glosa	= 'N'
	and 	nvl(ie_tipo_incidencia_w,'B')	= 'B'
	and	b.dt_atendimento_referencia between dt_inicial_w and fim_dia(dt_final_w)
	union
	select	b.nr_sequencia,
		a.nr_sequencia
	from	pls_conta	b,
		pls_segurado 	a
	where	a.nr_sequencia		= b.nr_seq_segurado
	and	a.nr_seq_contrato	= nr_seq_contrato_w
	and	b.ie_glosa	= 'N'
	and 	nvl(ie_tipo_incidencia_w,'B') = 'T'
	and	b.dt_atendimento_referencia between dt_inicial_w and fim_dia(dt_final_w)
	and	((a.nr_sequencia = nr_seq_segurado_p and a.nr_Seq_titular is null) or (a.nr_seq_titular = nr_seq_segurado_p and pls_obter_se_parente_legal(a.nr_sequencia) = 'S'));
	
Cursor C02 is
	select	pls_obter_regra_limitacao(a.cd_procedimento, null, a.ie_origem_proced, nr_seq_segurado_w, null),
		sum(nvl(qt_procedimento_imp,0))
	from	pls_conta_proc a
	where	nr_seq_conta	= nr_seq_conta_w
	and	nvl(a.ie_glosa,'N')	= 'N' --Diego OS 313903 - Hist�rico 23/05/2011 13:14:43 - Para o caso do procedimento ter sido glosado.
	and	pls_obter_regra_limitacao(a.cd_procedimento, null, a.ie_origem_proced, nr_seq_segurado_w, null) = nr_seq_tipo_limitacao_p
	/*and	a.ie_status in ('L','S')*/
	group	by pls_obter_regra_limitacao(a.cd_procedimento, null, a.ie_origem_proced, nr_seq_segurado_w,null);
			

begin

ie_limitacao_w	:= 'N';
qt_permitida_w	:= nvl(qt_permitida_p,0);

select	nvl(dt_atendimento_referencia,sysdate)
into	dt_conta_w
from	pls_conta
where	nr_sequencia	= nr_seq_conta_p;

if	(nr_seq_segurado_p is not null) then
	select	nr_seq_titular,
		nr_seq_contrato
	into	nr_seq_titular_w,
		nr_seq_contrato_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
	
	if	(ie_periodo_p = 'CO') then
		select	trunc(a.dt_contrato,'dd'),
			trunc(dt_conta_w)
		into	dt_contratacao_w,
			dt_atual_w
		from	pls_contrato a
		where	a.nr_sequencia	= nr_seq_contrato_w;
		
		dt_inicio_w	:= dt_contratacao_w;
		
		if	(ie_tipo_periodo_p = 'M') then
			while	(dt_inicio_w <= dt_atual_w) loop	
				select	add_months(dt_inicio_w,qt_intervalo_p)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	add_months(dt_inicio_w, -qt_intervalo_p),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'S') then
			while	(dt_inicio_w <= dt_atual_w) loop	
				select	dt_inicio_w + (qt_intervalo_p*7)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - (qt_intervalo_p*7),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'D') then
			while	(dt_inicio_w <= dt_atual_w) loop	
				select	dt_inicio_w + qt_intervalo_p
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - qt_intervalo_p,
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;			
		end if;
	elsif	(ie_periodo_p = 'CA') then
		dt_atual_w	:= trunc(dt_conta_w);
		dt_inicio_w	:= trunc(dt_conta_w,'year');
		if	(ie_tipo_periodo_p = 'M') then
			if	(qt_intervalo_p = 1) then
				dt_inicial_w	:= trunc(dt_conta_w,'month');
				dt_final_w	:= last_day(trunc(dt_conta_w,'month'));	
			elsif	(qt_intervalo_p in (2,3,4,6)) then
				dt_inicio_w	:= trunc(dt_conta_w,'year');
				
				while	(dt_inicio_w <= dt_atual_w) loop
					begin
					select	add_months(dt_inicio_w,qt_intervalo_p)
					into	dt_inicio_w
					from	dual;
					end;
				end loop;
				
				select	add_months(dt_inicio_w,-qt_intervalo_p),
					dt_inicio_w -1
				into	dt_inicial_w,
					dt_final_w
				from	dual;
				
			elsif	(qt_intervalo_p = 12) then
				dt_inicial_w	:= trunc(dt_conta_w,'year');
				dt_final_w	:= last_day(add_months(trunc(dt_conta_w,'year'),qt_intervalo_p));
			end if;
		elsif	(ie_tipo_periodo_p = 'S') then
			while	(dt_inicio_w <= dt_atual_w) loop	
				select	dt_inicio_w + (qt_intervalo_p*7)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - (qt_intervalo_p*7),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		elsif	(ie_tipo_periodo_p = 'D') then
			while	(dt_inicio_w <= dt_atual_w) loop	
				select	dt_inicio_w + qt_intervalo_p
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - qt_intervalo_p,
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
		end if;
		
	elsif	(ie_periodo_p = 'A') then
	
		select	trunc(a.dt_contratacao)
		into	dt_inicio_w
		from	pls_segurado a
		where	a.nr_sequencia	= nr_seq_segurado_p;
		
		dt_atual_w := trunc(dt_conta_w);
		
		if	(ie_tipo_periodo_p = 'M') then
		
			while	(dt_inicio_w <= dt_atual_w) loop	
				select	add_months(dt_inicio_w,qt_intervalo_p)
				into	dt_inicio_w 
				from	dual;
			end loop;
			
			select	add_months(dt_inicio_w, -qt_intervalo_p),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
						
		elsif	(ie_tipo_periodo_p = 'S') then
		
			while	(dt_inicio_w <= dt_atual_w) loop	
				select	dt_inicio_w + (qt_intervalo_p*7)
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - (qt_intervalo_p*7),
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;
			
		elsif	(ie_tipo_periodo_p = 'D') then
		
			while	(dt_inicio_w <= dt_atual_w) loop	
				select	dt_inicio_w + qt_intervalo_p
				into	dt_inicio_w
				from	dual;
			end loop;
			
			select	dt_inicio_w - qt_intervalo_p,
				dt_inicio_w -1
			into	dt_inicial_w,
				dt_final_w
			from	dual;			
		end if;	
		
	end if;	
	
	select	ie_tipo_incidencia
	into	ie_tipo_incidencia_w
	from	pls_tipo_limitacao
	where	nr_sequencia	= nr_seq_tipo_limitacao_p;
	
	open c01;
	loop
	fetch c01 into	
		nr_seq_conta_w,
		nr_seq_segurado_w;
	exit when c01%notfound;
		begin
		open c02;
		loop
		fetch c02 into
			nr_seq_conta_proc_w,
			qt_procedimento_w;
		exit when c02%notfound;					
			qt_liberada_w	:= nvl(qt_liberada_w,0) + nvl(qt_procedimento_w,0);
		end loop;
		close c02;
		end;
	end loop;
	close c01;
	/* Alexandre OS 293195 - Foi documentado a soma do campo qt_solicitada_p, pois conforme a OS 276326 e data do hist�rico 10/02/2011, no cursor 
	n�o � tratado o status da conta, e por isso j� trazar , no cursor, a quantidade solicitada do procediemnto da conta que est� sendo consisitida */
	qt_liberada_w	:= qt_liberada_w; --+ nvl(qt_solicitada_p,0);
	
	if	(qt_permitida_w < qt_liberada_w) then
		ie_limitacao_w	:= 'S';
	end if;
end if;

return ie_limitacao_w;

end pls_obter_se_limitacao_conta;
/
