create or replace
function possui_diagnostico_preliminar(	
	nr_atendimento_p            number)
return varchar2 is

ie_existe_w		varchar2(10);
			
begin

select	decode(count(*),0,'N','S')
into	ie_existe_w
from	diagnostico_medico
where	nr_atendimento = nr_atendimento_p
and	ie_tipo_diagnostico = 1
and	trunc(dt_diagnostico) = trunc(sysdate);

return	ie_existe_w;

end possui_diagnostico_preliminar;
/