create or replace
function Obter_se_OS_prior_usuario(	nm_usuario_p		varchar2,
					nr_seq_ativ_prev_p	number,
					nr_seq_ordem_p		number)
 		    	return varchar2 is
			
IE_PRIORIDADE_DESEN_w	number(3);
ie_consiste_w		varchar2(1);
			
begin

if	(nvl(nr_seq_ativ_prev_p,0) > 0) then

	select	nvl(IE_PRIORIDADE_DESEN,0)
	into	IE_PRIORIDADE_DESEN_w
	from	man_ordem_ativ_prev
	where	nr_sequencia = nr_seq_ativ_prev_p;

	select	decode(count(*),0,'N','S')
	into	ie_consiste_w
	from	man_ordem_servico a,
		man_estagio_processo b,
		man_ordem_ativ_prev c
	where	b.nr_sequencia 		= a.nr_seq_estagio
	and	a.nr_sequencia		= c.nr_seq_ordem_serv
	and	(b.ie_desenv    	= 'S' or b.ie_tecnologia = 'S')
	and	c.IE_PRIORIDADE_DESEN > IE_PRIORIDADE_DESEN_w
	and	a.nr_sequencia		<> nr_seq_ordem_p
	and	c.nm_usuario_prev	= nm_usuario_p
	and	c.dt_prevista between trunc(sysdate) and fim_dia(sysdate)
	and	DT_REAL is null;

end if;
	
return	ie_consiste_w;

end Obter_se_OS_prior_usuario;
/
