create or replace
function man_obter_se_causa_dano_lib(	nr_seq_causa_p		number,
					nr_seq_grupo_trab_p	number,
					nr_seq_grupo_planej_p	number,
					nr_seq_tipo_ordem_p	number default 0)
 		    	return varchar2 is

ds_retorno_w	varchar2(1) := 'S';
qt_existe_w	number(10,0);


begin

/*Verifica se existe alguma regra*/
select	count(*)
into	qt_existe_w
from	man_causa_dano_lib
where	nr_seq_causa_dano = nr_seq_causa_p;

if	(qt_existe_w > 0) then
	begin
	select	'S'
	into	ds_retorno_w
	from	man_causa_dano_lib
	where	nr_seq_causa_dano 				= nr_seq_causa_p
	and	(nvl(nr_seq_grupo_trab,nvl(nr_seq_grupo_trab_p,0)) 	= nvl(nr_seq_grupo_trab_p,0))
	and	(nvl(nr_seq_grupo_planej,nvl(nr_seq_grupo_planej_p,0)) 	= nvl(nr_seq_grupo_planej_p,0))
	and	(nvl(nr_seq_tipo_ordem,nvl(nr_seq_tipo_ordem_p,0))	= nvl(nr_seq_tipo_ordem_p,0))
	and	rownum < 2;	
	exception
	when others then
		ds_retorno_w := 'N';
	end;
end if;

return	ds_retorno_w;

end man_obter_se_causa_dano_lib;
/
