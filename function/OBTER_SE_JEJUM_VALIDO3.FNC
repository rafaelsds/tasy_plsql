create or replace
function Obter_se_jejum_valido3(	nr_atendimento_p		number,	
				nr_prescricao_p		number,
				nr_seq_min_p		number,
				ie_tipo_item_p		varchar2)
 		    	return number is

nr_seq_w					number(15,0);
dt_horario_w				date;

cursor c01 is 
select	dt_horario
from	prescr_dieta_hor
where	nr_prescricao 	= nr_prescricao_p
and		ie_tipo_item_p	= 'D'
and		nr_seq_dieta	> nr_seq_min_p
union
select	dt_horario
from	prescr_mat_hor
where	nr_prescricao 	= nr_prescricao_p
and		nr_seq_material > nr_seq_min_p
and		ie_tipo_item_p	= 'M';

begin

open C01;
loop
fetch C01 into	
	dt_horario_w;
exit when C01%notfound;
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_w
	from	prescr_medica b,
			rep_jejum a
	where	b.nr_atendimento	= nr_atendimento_p
	and		b.nr_prescricao		= a.nr_prescricao
	and		nvl(a.ie_suspenso,'N')	<> 'S'
	and		b.dt_suspensao is null
	and		dt_horario_w between nvl(a.dt_inicio, b.dt_inicio_prescr) and nvl(a.dt_fim, b.dt_validade_prescr);
	
	if	(nr_seq_w	> 0) then
		exit;
	end if;
	
end loop;
close C01;

if	(nr_seq_w	= 0) then
	nr_seq_w	:= null;
end if;

return	nr_seq_w;

end Obter_se_jejum_valido3;
/
