create or replace
function obter_fone_prescritor	(nr_prescricao_p	number)
			return varchar2 is

ds_fone_prescr_w	varchar2(255);

begin
if	(nr_prescricao_p is not null) then

	select	decode(Obter_se_medico(cd_prescritor,'M'),'S', obter_fone_pac_agenda(cd_prescritor))
	into	ds_fone_prescr_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;

end if;

return ds_fone_prescr_w;

end obter_fone_prescritor;
/
