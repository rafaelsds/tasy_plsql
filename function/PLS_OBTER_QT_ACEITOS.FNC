create or replace
function pls_obter_qt_aceitos(nr_seq_intercambio_p	number)
 		    	return number is

qt_aceitos_w number(10);
			
begin
if	(nr_seq_intercambio_p is not null) then
	begin
	select	count(*)
	into	qt_aceitos_w
	from	ptu_intercambio_benef	b,
		ptu_intercambio_empresa	a
	where	b.nr_seq_empresa        = a.nr_sequencia
	and	a.nr_seq_intercambio    = nr_seq_intercambio_p
	and	b.ie_status             in ('A','C');
	end;
end if;

return	qt_aceitos_w;

end pls_obter_qt_aceitos;
/