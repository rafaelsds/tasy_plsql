create or replace
function obter_usuario_frasco_pato( nr_seq_frasco_pato_p	number,
			       ie_status_p		number)
 		    	return varchar2 is			
ds_retorno_w	varchar2(30);	

begin

select	nvl(max(a.nm_usuario_nrec),'')
into	ds_retorno_w
from	frasco_pato_loc_status a
where   nr_seq_frasco_pato_loc = nr_seq_frasco_pato_p
and	ie_status	= ie_status_p;

return	ds_retorno_w;

end obter_usuario_frasco_pato;
/