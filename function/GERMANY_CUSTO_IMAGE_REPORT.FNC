create or replace procedure germany_custo_image_report
			(	nr_seq_interno_p		varchar2,
				nr_prescricao_p			varchar2,
        ds_arquivo_imagem_p varchar2,
        place_order_p  varchar2) is 


ie_existe_w	VARCHAR2(1);
nr_seq_imagem_w	laudo_paciente_imagem.nr_seq_imagem%TYPE;
nr_seq_laudo_w		number(10);
nr_seq_prescr_w		number(10);
nr_prescr_w		number(10);
ds_pasta_exp_xml_w	varchar2(2000);
nm_usuario_w		varchar2(255);
nr_seq_empresa_w		number(10);

BEGIN

nm_usuario_w	:= 'CUSTODIAG';
nr_seq_empresa_w	:= 165;

begin

select	max(ds_caminho_entrada)
into	ds_pasta_exp_xml_w
from    empresa_integr_dados a,
	empresa_integracao b,
	sistema_integracao c
where	a.nr_seq_empresa_integr = b.nr_sequencia
and	b.nr_sequencia = c.nr_seq_empresa
and     b.nr_sequencia = nr_seq_empresa_w;


select	
	max(a.nr_sequencia)
into	
	nr_seq_prescr_w
from	prescr_procedimento	a
where	a.nr_prescricao	= nr_prescricao_p
and   a.nr_seq_interno = nr_seq_interno_p;


select	max(a.nr_sequencia)
into	nr_seq_laudo_w
from	laudo_paciente	a
where	a.nr_prescricao	= nr_prescricao_p
and	a.nr_seq_prescricao	= nr_seq_prescr_w;


SELECT	DECODE(COUNT(*),0,'N','S')
INTO 	ie_existe_w
FROM 	laudo_paciente_imagem a
WHERE 	a.nr_sequencia = nr_seq_laudo_w
AND  	a.ds_arquivo_imagem = ds_arquivo_imagem_p;


IF	(ie_existe_w = 'N') THEN
	SELECT	NVL(MAX(a.nr_seq_imagem),0)+1
	INTO 	nr_seq_imagem_w
	FROM 	laudo_paciente_imagem a
	WHERE 	a.nr_sequencia = nr_seq_laudo_w;

	INSERT INTO laudo_paciente_imagem( 
		nr_sequencia,
		nr_seq_imagem,
		ds_arquivo_imagem,
		nm_usuario,
		dt_atualizacao,
    NM_USUARIO_NREC,
    dt_atualizacao_nrec)
	VALUES( nr_seq_laudo_w,
		nr_seq_imagem_w,
		ds_pasta_exp_xml_w||ds_arquivo_imagem_p,
		nm_usuario_w,
		SYSDATE,
    nm_usuario_w,
    sysdate);

	COMMIT;

END IF;

exception

when others then

	gravar_log_cdi(7000,

			substr(nr_seq_interno_p || ' - ' || nr_prescricao_p || '#@#@' || sqlerrm,1,4000),

			'GERMANY');

end;

end germany_custo_image_report;
/