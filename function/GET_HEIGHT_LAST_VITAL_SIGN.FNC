CREATE OR REPLACE
FUNCTION get_height_last_vital_sign(cd_pessoa_fisica_p varchar2)
RETURN varchar2 IS
    result_w varchar2(20 char);
BEGIN    
    select to_char(decode(ie_unid_med_altura, 'cm', qt_altura_cm, qt_altura_m)) || ie_unid_med_altura
    into   result_w
    from   atendimento_sinal_vital	
    where  nr_sequencia = (select nvl(max(nr_sequencia), 0)
                from  atendimento_sinal_vital
                where qt_altura_cm is not null
                and   nvl(ie_situacao, 'A') = 'A'
                and   cd_paciente = cd_pessoa_fisica_p);
    RETURN result_w;
END get_height_last_vital_sign;
/