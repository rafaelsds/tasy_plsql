create or replace 
FUNCTION ctb_obter_situacao_saldo_ecd(	nr_seq_rubrica_p		number,
					cd_registro_p		varchar2,
					dt_vigencia_p		date,
					vl_saldo_p		number)
					return varchar2 is


ie_debito_credito_w				varchar2(1);
ie_situacao_w				varchar2(1);
ie_tipo_grupo_w				varchar2(1);
ie_tipo_w					varchar2(1);
cd_classif_conta_w				varchar2(40);
cd_grupo_w				number(10);
ie_grupo_balanco_ecd_w			ctb_modelo_rubrica.ie_grupo_balanco_ecd%type;
ie_total_w				ctb_modelo_rubrica.ie_total%type;

cursor c01 is
select	a.cd_conta_contabil
from	ctb_modelo_rubrica b,
	ctb_modelo_rubrica_conta a
where	a.nr_seq_rubrica	= b.nr_sequencia
and	b.ie_origem_valor <> 'SR'
and	b.nr_sequencia		= nr_seq_rubrica_p
order by	a.nr_sequencia;

BEGIN

begin
select	ie_total
into	ie_total_w
from	ctb_modelo_rubrica a
where	a.nr_sequencia	= nr_seq_rubrica_p;
exception when others then
	ie_total_w	:= null;
end;


if	(cd_registro_p = 'J100') then
	begin
	select	max(ie_grupo_balanco_ecd)
	into	ie_grupo_balanco_ecd_w
	from	ctb_modelo_rubrica
	where	nr_sequencia	= nr_seq_rubrica_p;
	
	if	(ie_grupo_balanco_ecd_w = 1) then
		ie_situacao_w	:= 'D';
		if	(vl_saldo_p < 0) then
			ie_situacao_w	:= 'C';
		end if;
	elsif	(ie_grupo_balanco_ecd_w = 2) then
		begin
		ie_situacao_w	:= 'C';
		if	(vl_saldo_p < 0) then
			ie_situacao_w	:= 'D';
		end if;
		end;
	end if;
	end;
elsif	(cd_registro_p = 'J150') then
	begin
	ie_situacao_w	:= 'P';
	if	(vl_saldo_p < 0) then
		ie_situacao_w	:= 'N';
	end if;
	end;
end if;


for vet01 in C01 loop
	begin
	/*Tratamento IND_VL do Reg J100 Balan�o Patrimonial */
	if	(cd_registro_p = 'J100') then 
		begin
		ie_situacao_w	:= 'C';

		select	nvl(max(cd_grupo),0)
		into	cd_grupo_w
		from	conta_contabil
		where	cd_conta_contabil	= vet01.cd_conta_contabil;

		if	(cd_grupo_w > 0) then

			select	max(ie_debito_credito)
			into	ie_debito_credito_w
			from	ctb_grupo_conta
			where	cd_grupo = cd_grupo_w;

			if	(vl_saldo_p > 0) then
				ie_situacao_w	:= ie_debito_credito_w;
			else
				ie_situacao_w	:= 'D';
				if	(ie_debito_credito_w = 'D') then
					ie_situacao_w	:= 'C';
				end if;
			end if;
		end if;
		end;
	elsif	(cd_registro_p = 'J150') then /* Deomstra��o de Resultado do Exerc�cio */
		begin
		ie_situacao_w	:= 'P';
		
		select	nvl(max(cd_grupo),0),
			nvl(max(ie_tipo),'A')
		into	cd_grupo_w,
			ie_tipo_w
		from	conta_contabil
		where	cd_conta_contabil	= vet01.cd_conta_contabil;
		
		select	max(ie_debito_credito),
			max(ie_tipo)
		into	ie_debito_credito_w,
			ie_tipo_grupo_w
		from	ctb_grupo_conta
		where	cd_grupo = cd_grupo_w;
		
		if (nvl(ie_total_w, 'X') <> 'S') then
			begin
			if	(ie_tipo_w = 'T') then
				begin
				
					if	(vl_saldo_p > 0) then
						ie_situacao_w	:= 'P';
					else
						ie_situacao_w	:= 'N';
					end if;
					
					if	(ie_tipo_grupo_w = 'C') then /* Tipo de grupo Receitas */
						begin
						ie_situacao_w	:= 'P';
						if	(vl_saldo_p < 0) then
							ie_situacao_w	:= 'N';
						end if;
						end;
					elsif	(ie_tipo_grupo_w = 'D') then /* Tipo de grupo Despesa */
						begin
						ie_situacao_w	:= 'N';
						if	(vl_saldo_p < 0) then
							ie_situacao_w	:= 'P';
						end if;
						end;
					elsif	(ie_tipo_grupo_w = 'R') then /* Se for um grupo geral de Resultado */
						begin
						cd_classif_conta_w	:= substr(ctb_obter_classif_conta(vet01.cd_conta_contabil, cd_classif_conta_w, dt_vigencia_p),1,1);
						if	(cd_classif_conta_w = '3') then
							begin
							ie_situacao_w	:= 'P';
							if	(vl_saldo_p < 0) then
								ie_situacao_w	:= 'N';
							end if;
							end;
						elsif	(cd_classif_conta_w = '4') then
							begin
							ie_situacao_w	:= 'N';
							if	(vl_saldo_p < 0) then
								ie_situacao_w	:= 'P';
							end if;
							end;
						else	
							ie_situacao_w	:= 'P';
							if	(vl_saldo_p < 0) then
								ie_situacao_w	:= 'N';
							end if;
						end if;
						end;
					end if;		
				end;
			elsif	(ie_tipo_w = 'A') then
				begin
				
				if	(ie_tipo_grupo_w = 'C') then /* Tipo de grupo Receitas */
					begin
					ie_situacao_w	:= 'R';
					if	(vl_saldo_p < 0) then
						ie_situacao_w	:= 'D';
					end if;
					end;
				elsif	(ie_tipo_grupo_w = 'D') then /* Tipo de grupo Despesa */
					begin
					ie_situacao_w	:= ie_tipo_grupo_w;
					if	(vl_saldo_p < 0) then
						ie_situacao_w	:= 'R';
					end if;
					end;
				elsif	(ie_tipo_grupo_w = 'R') then /* Se for um grupo geral de Resultado */
					begin
					cd_classif_conta_w	:= substr(ctb_obter_classif_conta(vet01.cd_conta_contabil, cd_classif_conta_w, dt_vigencia_p),1,1);
					if	(cd_classif_conta_w = '3') then
						begin
						ie_situacao_w	:= 'R';
						if	(vl_saldo_p < 0) then
							ie_situacao_w	:= 'D';
						end if;
							
						end;
					elsif	(cd_classif_conta_w = '4') then
						begin
						ie_situacao_w	:= 'D';
						if	(vl_saldo_p < 0) then
							ie_situacao_w	:= 'R';
						end if;
						
						end;
					else	
						ie_situacao_w	:= ie_tipo_grupo_w;
					end if;
					end;
				end if;
						
				end;
			end if;
			end;
		elsif(ie_total_w = 'S') then
			begin
			if	(ie_tipo_w = 'T') then
				begin
				
				ie_situacao_w	:= 'P';
				if	(vl_saldo_p < 0) then
					ie_situacao_w	:= 'N';
				end if;
				
				if	(ie_tipo_grupo_w = 'D') then /* Tipo de grupo Despesa */
					begin
					ie_situacao_w	:= 'N';
					if	(vl_saldo_p < 0) then
						ie_situacao_w	:= 'P';
					end if;
					end;
				elsif	(ie_tipo_grupo_w = 'R') then /* Se for um grupo geral de Resultado */
					begin
					cd_classif_conta_w	:= substr(ctb_obter_classif_conta(vet01.cd_conta_contabil, cd_classif_conta_w, dt_vigencia_p),1,1);
					if	(cd_classif_conta_w = '4') then
						begin
						ie_situacao_w	:= 'N';
						if	(vl_saldo_p < 0) then
							ie_situacao_w	:= 'P';
						end if;
						end;
					end if;
					end;
				end if;
				end;		
			end if;
			end;
		end if;
		end;
	end if;
	
	end;
end loop;

return ie_situacao_w;

END ctb_obter_situacao_saldo_ecd;
/