create or replace
function obter_ie_prescr_vigente(
		nr_prescricao_p		number,
		nr_prescricoes_p	varchar2)
		return varchar2 is

ie_prescr_vigente_w	varchar2(1) := '';
nr_prescr_w		number(10,0);
begin
if	(nr_prescricao_p is not null) or
	(nr_prescricoes_p is not null) then
	begin
	
	if	(nr_prescricao_p is not null) then
		begin
		nr_prescr_w	:= nr_prescricao_p;
		end;
	else
		begin
		nr_prescr_w	:= obter_max_nrprescricao(nr_prescricoes_p);
		end;
	end if;
	
	ie_prescr_vigente_w	:= obter_se_prescr_vigente(nr_prescr_w);
	end;
end if;
return	ie_prescr_vigente_w;
end obter_ie_prescr_vigente;
/