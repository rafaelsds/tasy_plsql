create or replace
function obter_se_inclui_tit_bordero(	nr_bordero_p	number,
					nr_titulo_p	number,
					nm_usuario_p	varchar2) return varchar2 is

ds_retorno_w			varchar2(4000);
ie_permite_inclusao_w		varchar2(1)	:= 'S';
ie_tipo_titulo_w		varchar2(2);
ie_origem_titulo_w		varchar2(10);
nr_seq_trans_financ_bordero_w	number(10);
ie_situacao_w			varchar2(1);
ie_titulo_escrit_w		varchar2(1);
cd_estabelecimento_w		number(4);
qt_pagto_escrit_w		number(10)	:= 0;

Cursor c01 is
select	nvl(ie_permite_inclusao,'S')
from	trans_financ_bordero
where	nr_seq_trans_financ	= nr_seq_trans_financ_bordero_w
and	nvl(ie_tipo_titulo,ie_tipo_titulo_w)		= ie_tipo_titulo_w
and	nvl(ie_origem_titulo,ie_origem_titulo_w)	= ie_origem_titulo_w;

begin

select	nr_seq_trans_financ
into	nr_seq_trans_financ_bordero_w
from	bordero_pagamento
where	nr_bordero	= nr_bordero_p;

select  ie_tipo_titulo,
	ie_origem_titulo,
	ie_situacao,
	cd_estabelecimento
into	ie_tipo_titulo_w,
	ie_origem_titulo_w,
	ie_situacao_w,
	cd_estabelecimento_w
from 	titulo_pagar
where	nr_titulo	= nr_titulo_p;

if	(ie_situacao_w = 'C') then

	ds_retorno_w	:= WHEB_MENSAGEM_PCK.get_texto(819177, 'NR_TITULO_P=' || nr_titulo_p); /* 'O t�tulo #@NR_TITULO_P#@ n�o pode ser inclu�do no border� porque est� cancelado!'  */ 

else

	obter_param_usuario(855,39,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_titulo_escrit_w);

	if	(nvl(ie_titulo_escrit_w,'S') = 'N') then

		select	count(*)
		into	qt_pagto_escrit_w
		from	titulo_pagar_escrit a
		where	a.nr_titulo	= nr_titulo_p;

	end if;

	if	(qt_pagto_escrit_w > 0) then
		ds_retorno_w	:= WHEB_MENSAGEM_PCK.get_texto(819178, 'NR_TITULO_P=' || nr_titulo_p); /* 'O t�tulo #@NR_TITULO_P#@ n�o pode ser inclu�do no border� porque est� em pagamento escritural. Par�metro [39]'  */
	else

		open c01;
		loop
		fetch c01 into
			ie_permite_inclusao_w;
		exit when c01%notfound;
			null;
		end loop;
		close c01;

		if	(ie_permite_inclusao_w = 'N') then
			ds_retorno_w	:= WHEB_MENSAGEM_PCK.get_texto(819178, 'NR_TITULO_P=' || nr_titulo_p); /* 'O t�tulo #@NR_TITULO_P#@ n�o pode ser inclu�do no border� porque o tipo do t�tulo n�o est� liberado para a transa��o do border�.' */ 
		end if;

	end if;

end if;

return	ds_retorno_w;

end obter_se_inclui_tit_bordero;
/