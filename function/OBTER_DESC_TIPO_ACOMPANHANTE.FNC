create or replace
function obter_desc_tipo_acompanhante(nr_sequencia_p		number)
 		    	return varchar2 is

ds_result_w	varchar2(255) := '';
begin
select	DS_TIPO_ACOMPANHANTE
into	ds_result_w
from	tipo_acompanhante
where	nr_sequencia = nr_sequencia_p;	


return	ds_result_w;

end obter_desc_tipo_acompanhante;
/