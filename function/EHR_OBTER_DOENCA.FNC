create or replace 
function EHR_Obter_Doenca(	nr_seq_doenca_p	number)
				return	varchar2 is

ds_retorno_w		varchar2(80);

begin

if	(nr_seq_doenca_p > 0) then
	begin
	select	ds_doenca
	into	ds_retorno_w
	from	ehr_doenca
	where	nr_sequencia	= nr_seq_doenca_p;
	end;
end if;

return ds_retorno_w;
	
end EHR_Obter_Doenca;
/