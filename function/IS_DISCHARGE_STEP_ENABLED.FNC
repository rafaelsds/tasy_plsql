create or replace
function is_discharge_step_enabled(	ie_discharge_level_p	varchar2,
					nm_user_p		varchar2,
					cd_profile_p		number)
 		    	return varchar2 is

ie_step_allowed_w	varchar2(1);

Cursor C01 is
	select	nvl(ie_permite, 'N') ie_allowed
	from	permissao_etapa_alta
	where	((ie_etapa_alta is null) or (ie_etapa_alta = ie_discharge_level_p))
	and	((cd_perfil is null) or (cd_perfil = cd_profile_p))
	and	((nm_usuario is null) or (nm_usuario = nm_user_p))
	order by ie_etapa_alta, nm_usuario, cd_perfil;

begin
-- If there's no rule defined for this level, the user is allowed to do it
ie_step_allowed_w := 'S';

open C01;
loop
fetch C01 into	
	ie_step_allowed_w;
exit when C01%notfound;
	exit;
end loop;
close C01;

return	ie_step_allowed_w;

end is_discharge_step_enabled;  
/
