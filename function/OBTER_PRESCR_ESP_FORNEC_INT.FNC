create or replace
function obter_prescr_esp_fornec_int(	nr_cirurgia_p	number,
					cd_fornec_consig_p varchar2)
				return number is

nr_prescricao_w		number(14);
qt_existe_w		number(10);


/*busca a prescri��o da pasta materiais especiais da gest�o de cirurgias passando o fornecedor processo da integra��o com INPART*/	
begin

select 	count(*)
into	qt_existe_w
from   	prescr_medica p
where  	p.nr_cirurgia = nr_cirurgia_p
and 	p.ie_tipo_prescr_cirur = '1';
	

if	(nvl(qt_existe_w,0) > 0) then

	select 	max(p.nr_prescricao)
	into 	nr_prescricao_w	
	from   	prescr_medica p,
		prescr_material m
	where  	p.nr_cirurgia = nr_cirurgia_p
	and 	p.nr_prescricao = m.nr_prescricao
	and 	m.cd_fornec_consignado = cd_fornec_consig_p
	and 	p.ie_tipo_prescr_cirur = '1'
	and exists (	select 1
			from prescr_material x
			where x.nr_prescricao = p.nr_prescricao);
  
end if;

return nr_prescricao_w;

end obter_prescr_esp_fornec_int;
/