create or replace
function obter_status_ret_conv(	nr_seq_ret_item_p	number)
		return varchar2 is
		
ie_status_retorno_w	varchar2(1);		
		
begin

begin
select	a.ie_status_retorno
into	ie_status_retorno_w
from	convenio_retorno a,
	convenio_retorno_item b
where	b.nr_seq_retorno	= a.nr_sequencia
and	b.nr_sequencia		= nr_seq_ret_item_p
and	rownum 			=1;	
exception
when others then
	ie_status_retorno_w	:= 'R';
end;

return	ie_status_retorno_w;

end obter_status_ret_conv;
/