create or replace 
function converte_numero_octadecimal( nr_valor_p in number ) 
			return varchar2 is
begin
	return to_base(nr_valor_p, 8 );
end converte_numero_octadecimal;
/
