create or replace function obter_adt_a01_varian(nr_seq_rxt_tumor_p number,
												nr_seq_tratamento_p number)
return clob is

cd_pessoa_fisica_w				varchar2(64 char);
family_name_patient_w			varchar2(64 char);
given_name_patient_w			varchar2(64 char);
middleinitialorname_patient_w	varchar2(64 char);
family_name_mother_w			varchar2(64 char);
given_name_mother_w				varchar2(64 char);
middleinitialorname_mother_w	varchar2(64 char);
date_of_birth_w		 			varchar2(30 char);
sex_w		 					varchar2(64 char);
race_w		 					varchar2(64 char);
street_address_w				varchar2(64 char);
zip_code_w		 				varchar2(20 char);
state_adress_w		 			varchar2(64 char);
city_w		 					varchar2(64 char);
country_w            			varchar2(64 char);
telephone_number_w				varchar2(64 char);
telecommunication_usecode_w 	varchar2(10 char);
email_address_w		 			varchar2(64 char);
ie_estado_civil_w				varchar2(15 char);
point_of_care_w 				varchar2(120 char);
room_w 							varchar2(255 char);
bed_w 							varchar2(255 char);
facility_w 						varchar2(120 char);
id_doctor_w 					varchar2(10 char);
family_name_doctor_pep_w 		varchar2(100 char);
given_name_doctor_pep_w 		varchar2(100 char);
middlename_doctor_pep_w 		varchar2(100 char);
visit_number_w 					number(10);
date_time_message_w            	varchar2(14 char);
date_message_w					varchar2(10 char);
hl7_itens_varian_seq_w			number(38);
nr_prontuario_w 				number(10);
trigger_event_w					varchar2(15 char);
qt_pacientes_cadastrados_w 		number(10);
action_triggered_w 				varchar2(20 char);
diagnosis_code_w				varchar2(20 char);

begin
	declare
	json_aux_w   philips_json;
	ds_message_w CLOB;

	begin
	json_aux_w := philips_json();

    select count(nr_sequencia)
        into qt_pacientes_cadastrados_w
        from pf_codigo_externo
        where cd_pessoa_fisica = cd_pessoa_fisica_w
        and nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
        and ie_tipo_codigo_externo = 'ARIA';

    if (qt_pacientes_cadastrados_w > 0) then
    trigger_event_w := 'ADT-A08';
    else
    trigger_event_w := 'ADT-A01';
    end if;

    select decode(trigger_event_w, 'ADT-A01', 'newPatient','alreadyIncludedPatient')
    into action_triggered_w
    from dual;

	select
		a.cd_pessoa_fisica,
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_pf,'sobrenome')), 1, 64) family_name_patient,
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_pf,'nome')), 1, 64) given_name_patient,
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_pf,'restonome')), 1, 64) middleinitialorname_patient,
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_mae_pf,'sobrenome')), 1, 64) family_name_mother,
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_mae_pf,'nome')), 1, 64) given_name_mother,
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_mae_pf,'restonome')), 1, 64) middleinitialorname_mother,
		a.data_nascto_pf Date_Of_Birth,
		sex,
		race,
		substr(elimina_acentuacao(street_address), 1, 64) street_address,
		substr(zip_code, 1, 16) zip_code,
		substr(elimina_acentuacao(state_adress), 1, 64) state_adress,
		substr(elimina_acentuacao(city), 1, 64) city,
		substr(elimina_acentuacao(country), 1, 64) country,
		substr(telephone_number, 1, 64) telephone_number,
		telecommunication_usecode,
		email_address,
		decode(a.ie_estado_civil,1,'S',2,'M',3,'D','U'),
		nr_prontuario,
		substr(street_address, 1, 20) diagnosis_code
	into
		cd_pessoa_fisica_w,
		family_name_patient_w,
		given_name_patient_w,
		middleinitialorname_patient_w,
		family_name_mother_w,
		given_name_mother_w,
		middleinitialorname_mother_w,
		date_of_birth_w,
		sex_w,
		race_w,
		street_address_w,
		zip_code_w,
		state_adress_w,
		city_w,
		country_w,
		telephone_number_w,
		telecommunication_usecode_w,
		email_address_w,
		ie_estado_civil_w,
		nr_prontuario_w,
		diagnosis_code_w
	from	(select b.nr_sequencia,
				b.cd_pessoa_fisica,
				c.nm_pessoa_fisica nome_pf,
				obter_nome_mae_pf(b.cd_pessoa_fisica) nome_mae_pf,
				to_char(obter_data_nascto_pf(b.cd_pessoa_fisica), 'YYYYMMDDHH24MISS') data_nascto_pf,
				nvl(ie_sexo,'I') Sex,
				d.cd_raca_hl7 Race,
				(e.ds_endereco ||','|| e.nr_endereco || decode(ds_complemento, null, '', ',' || e.ds_complemento)) street_address,
				e.cd_cep Zip_Code,
				substr(obter_compl_pf(e.cd_pessoa_fisica, e.ie_tipo_complemento, 'DS_UF'), 1, 50) state_adress,
				e.ds_municipio City,
				substr(obter_nome_pais(e.nr_seq_pais),1,20) Country,
				nvl(obter_telefone_pf(b.cd_pessoa_fisica, 12), obter_telefone_pf(b.cd_pessoa_fisica, 13)) telephone_number,
				'HP' telecommunication_usecode,
				obter_email_pf(b.cd_pessoa_fisica) email_address,
				c.cd_religiao,
				c.ie_estado_civil,
				b.nr_atendimento,
				c.nr_prontuario,
				c.nr_cep_cidade_nasc,
				b.cd_doenca
			from	rxt_tumor b,
                    		pessoa_fisica c
				left join cor_pele d on c.nr_seq_cor_pele = d.nr_sequencia
				left join compl_pessoa_fisica e on (c.cd_pessoa_fisica = e.cd_pessoa_fisica and e.ie_tipo_complemento = 1)
				where b.cd_pessoa_fisica = c.cd_pessoa_fisica) a
				where   a.nr_sequencia = nr_seq_rxt_tumor_p;

	select
		substr(elimina_acentuacao(a.point_of_care),1,255) point_of_care,
		substr(elimina_acentuacao(a.room),1,255),
		substr(elimina_acentuacao(a.bed),1,255),
		substr(elimina_acentuacao(a.facility),1,255),
		cd_medico,
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_medico_pf_pep,'sobrenome')), 1, 64),
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_medico_pf_pep,'nome')), 1, 64),
		substr(elimina_acentuacao(obter_parte_nome_pf(a.nome_medico_pf_pep,'restonome')), 1, 64),
		a.nr_atendimento
	into
		point_of_care_w,
		room_w,
		bed_w,
		facility_w,
		id_doctor_w,
		family_name_doctor_pep_w,
		given_name_doctor_pep_w,
		middlename_doctor_pep_w,
		visit_number_w
	from    (select	b.nr_sequencia,
			obter_desc_setor_atend(obter_setor_atendimento(b.nr_atendimento)) point_of_care,
			obter_unidade_atendimento(b.nr_atendimento, 'A', 'U') room,
			obter_unidade_atendimento(b.nr_atendimento, 'A', 'UC') bed,
			b.cd_estabelecimento facility,
			obter_nome_pf(b.cd_medico) nome_medico_pf_pep,
			obter_nome_pf(c.cd_medico_resp) nome_medico_pf_eup,
			b.nr_atendimento,
			b.cd_medico,
			c.dt_entrada
		from 	rxt_tumor b,
			atendimento_paciente c
		where 	b.nr_atendimento = c.nr_atendimento(+)) a
		where 	a.nr_sequencia = nr_seq_rxt_tumor_p;

	select	to_char(sysdate, 'YYYYMMDDHH24MISS'),
			to_char(sysdate, 'YYYYMMDD')
	into	date_time_message_w,
			date_message_w
	from	dual;

	select	hl7_itens_varian_seq.nextval
	into	hl7_itens_varian_seq_w
	from	dual;

	json_aux_w.put('IDNumber',cd_pessoa_fisica_w);
	json_aux_w.put('Surname',family_name_patient_w);
	json_aux_w.put('GivenName',given_name_patient_w);
	json_aux_w.put('SecondandFurtherGivenName',middleinitialorname_patient_w);
	json_aux_w.put('MotherSurname',family_name_mother_w);
	json_aux_w.put('MotherGivenName',given_name_mother_w);
	json_aux_w.put('MotherSecondandFurtherGivenName',middleinitialorname_mother_w);
	json_aux_w.put('DateTimeofBirth',date_of_birth_w);
	json_aux_w.put('AdministrativeSex',sex_w);
	json_aux_w.put('Race',race_w);
	json_aux_w.put('StreetName',street_address_w);
	json_aux_w.put('ZiporPostalCode',zip_code_w);
	json_aux_w.put('StateorProvince',state_adress_w);
	json_aux_w.put('City',city_w);
	json_aux_w.put('Country',country_w);
	json_aux_w.put('TelephoneNumber',telephone_number_w);
	json_aux_w.put('TelecommunicationUseCode',telecommunication_usecode_w);
	json_aux_w.put('EmailAddress',email_address_w);
	json_aux_w.put('MaritalStatus',ie_estado_civil_w);
	json_aux_w.put('PointofCare',nvl(point_of_care_w,''));
	json_aux_w.put('Room',nvl(room_w,''));
	json_aux_w.put('Bed',nvl(bed_w,''));
	json_aux_w.put('Facility',nvl(facility_w,''));
	json_aux_w.put('AttendingDoctorIDNumber',nvl(id_doctor_w,''));
	json_aux_w.put('AttendingDoctorSurname',nvl (family_name_doctor_pep_w,''));
	json_aux_w.put('AttendingDoctorGivenName',nvl (given_name_doctor_pep_w,''));
	json_aux_w.put('AttendingDoctorSecondandFurtherGivenName',nvl(middlename_doctor_pep_w,''));
	json_aux_w.put('VisitNumberID', nr_seq_tratamento_p);
	json_aux_w.put('DateTimeOfMessage', nvl(date_time_message_w,''));
	json_aux_w.put('DateOfMessage', nvl(date_message_w,''));
	json_aux_w.put('MessageControlID', nvl(hl7_itens_varian_seq_w,''));
	json_aux_w.put('PatientID', nvl(nr_prontuario_w,''));
	json_aux_w.put('triggerEvent', trigger_event_w);
	json_aux_w.put('actionTriggered', action_triggered_w);
	json_aux_w.put('DiagnosisCode', diagnosis_code_w);

	dbms_lob.createtemporary(ds_message_w, TRUE);
	json_aux_w.to_clob(ds_message_w);

	return ds_message_w;
	end;

end obter_adt_a01_varian;
/
