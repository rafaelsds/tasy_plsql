create or replace
function Obter_horarios_suspensos	(nr_atendimento_p	number,
									qt_minutos_anterior_p		number)
 		    	return varchar2 is
				
hr_atual_w	varchar2(5);
ds_retorno_w	varchar2(2000) := null;

cursor c01 is
Select	to_char(b.DT_HORARIO, 'HH24:mi')
from 	prescr_medica a,
	Prescr_mat_hor b
where 	a.nr_prescricao = b.nr_prescricao
and	nvl(b.ie_adep, 'N') <> 'N' 
and	a.nr_atendimento = nr_atendimento_p
and 	b.dt_suspensao is not null
and 	b.dt_suspensao >= sysdate - qt_minutos_anterior_p/1440
order by DT_HORARIO ;

begin

if	(nr_atendimento_p is not null) and
	(qt_minutos_anterior_p is not null) then
	open C01;
	loop
	fetch C01 into	
		hr_atual_w;
	exit when C01%notfound;
		begin
		if	(ds_retorno_w is null) then
			ds_retorno_w := hr_atual_w;
		else
			ds_retorno_w := ds_retorno_w || ',' || hr_atual_w ;
		end if;
		end;
	end loop;
	close C01;
end if;

return	ds_retorno_w;

end Obter_horarios_suspensos;
/