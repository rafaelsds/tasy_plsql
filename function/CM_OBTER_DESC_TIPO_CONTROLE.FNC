create or replace
function	cm_obter_desc_tipo_controle(
		nr_sequencia_p		number) return varchar2 is

ds_retorno_w		varchar2(255);

begin

select	ds_tipo
into	ds_retorno_w
from	cm_tipo_cad_controle
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end cm_obter_desc_tipo_controle;
/