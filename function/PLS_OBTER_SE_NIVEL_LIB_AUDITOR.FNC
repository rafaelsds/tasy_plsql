create or replace
function pls_obter_se_nivel_lib_auditor
			(	nr_seq_ocorrencia_p		number,
				nr_seq_grupo_p			number,
				nm_usuario_p			varchar2,
				ie_consistir_grupo_p		varchar2)
				return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_retorno_w			varchar2(1)	:= 'S';
nr_seq_nivel_lib_w		number(10);
nr_nivel_liberacao_w		number(10);
nr_seq_nivel_lib_aud_w		number(10);
nr_nivel_liberacao_aud_w	number(10);

begin
if	(nvl(nr_seq_ocorrencia_p,0) > 0) and
	(nvl(ie_consistir_grupo_p,'S') = 'S') then
	/*Nivel de libera��o da ocorrencia*/
	select	a.nr_seq_nivel_lib
	into	nr_seq_nivel_lib_w
	from	pls_ocorrencia a
	where	a.nr_sequencia	= nr_seq_ocorrencia_p;

	select	max(nr_nivel_liberacao)
	into	nr_nivel_liberacao_w
	from	pls_nivel_liberacao
	where	nr_sequencia	= nr_seq_nivel_lib_w;

	begin
	select	nvl(a.nr_seq_nivel_lib,b.nr_seq_nivel_lib)
	into	nr_seq_nivel_lib_aud_w
	from	pls_membro_grupo_aud	a,
		pls_grupo_auditor	b	
	where	a.nr_seq_grupo 		= nr_seq_grupo_p
	and	a.nm_usuario_exec 	= nm_usuario_p
	and	a.nr_seq_grupo 		= b.nr_sequencia;
	exception
	when others then
		nr_seq_nivel_lib_aud_w	:= null;
	end;
	
	select	max(nr_nivel_liberacao)
	into	nr_nivel_liberacao_aud_w
	from	pls_nivel_liberacao
	where	nr_sequencia	= nr_seq_nivel_lib_aud_w;
	
	if	(nvl(nr_nivel_liberacao_aud_w,0) < nvl(nr_nivel_liberacao_w,0)) then
		ie_retorno_w	:= 'N';
	end if;
elsif	(nvl(nr_seq_ocorrencia_p,0) > 0) then
	select	max(a.nr_seq_nivel_lib)
	into	nr_seq_nivel_lib_w
	from	pls_ocorrencia a
	where	a.nr_sequencia	= nr_seq_ocorrencia_p
	and	((exists	(select	1
			from	pls_ocorrencia_grupo	b
			where	b.nr_seq_ocorrencia	= a.nr_sequencia
			and	b.nr_seq_grupo		= nr_seq_grupo_p)) or
		(not exists	(select	1
			from	pls_ocorrencia_grupo	b
			where	b.nr_seq_ocorrencia	= a.nr_sequencia)));

	select	max(nr_nivel_liberacao)
	into	nr_nivel_liberacao_w
	from	pls_nivel_liberacao
	where	nr_sequencia	= nr_seq_nivel_lib_w;

	begin
		select	nvl(a.nr_seq_nivel_lib,b.nr_seq_nivel_lib)
		into	nr_seq_nivel_lib_aud_w
		from	pls_membro_grupo_aud	a,
			pls_grupo_auditor	b	
		where	a.nr_seq_grupo 		= nr_seq_grupo_p
		and	a.nm_usuario_exec 	= nm_usuario_p
		and	a.nr_seq_grupo 		= b.nr_sequencia;
	exception
	when others then
		nr_seq_nivel_lib_aud_w	:= null;
	end;
	
	select	max(nr_nivel_liberacao)
	into	nr_nivel_liberacao_aud_w
	from	pls_nivel_liberacao
	where	nr_sequencia	= nr_seq_nivel_lib_aud_w;
	
	if	(nvl(nr_nivel_liberacao_aud_w,0) < nvl(nr_nivel_liberacao_w,0)) then
		ie_retorno_w	:= 'N';
	end if;
end if;

return	ie_retorno_w;

end pls_obter_se_nivel_lib_auditor;
/