create or replace FUNCTION obter_desc_motivo_precaucoes(
   nr_atendimento_p  NUMBER)
   RETURN VARCHAR2 IS

ds_motivo_w varchar(255) := null;
ds_retorno_w   VARCHAR2(4000);

CURSOR C01 IS
  SELECT b.ds_motivo
  FROM  atendimento_precaucao a ,
   motivo_isolamento b
  WHERE  a.nr_atendimento = nr_atendimento_p
  AND   b.nr_sequencia  = a.nr_seq_motivo_isol 
  AND   a.dt_inativacao IS NULL
  AND   ((a.dt_termino IS NULL) OR (SYSDATE BETWEEN NVL(a.DT_INICIO,SYSDATE-1) AND a.DT_TERMINO))
  AND   a.dt_liberacao IS NOT NULL;
 


BEGIN

OPEN C01;
LOOP
FETCH C01 INTO
 ds_motivo_w;
EXIT WHEN C01%NOTFOUND;

  IF (ds_motivo_w is not null) THEN
   ds_retorno_w := ds_retorno_w||', '||ds_motivo_w;
  ELSE
   ds_retorno_w := ds_motivo_w;
  END IF;
END LOOP;
CLOSE C01;
RETURN ds_retorno_w;

END obter_desc_motivo_precaucoes;
/