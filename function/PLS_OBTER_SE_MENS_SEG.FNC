create or replace
function pls_obter_se_mens_seg
			(	nr_seq_segurado_p	number,
				nr_seq_mensalidade_p	number)
				return varchar2 is

ds_retorno_w			varchar2(1)	:= 'S';
dt_adesao_w			date;
dt_mesano_referencia_w		date;
nr_seq_contrato_w		number(10);
dt_limite_movimentacao_w	varchar2(2);
dt_inicio_movimentacao_w	date;
dt_fim_movimentacao_w		date;
nr_seq_regra_w			number(10);
ie_tipo_data_limite_w		varchar2(2);
vl_variacao_mes_w		number(5);
ie_tipo_contrato_w		varchar2(2);
nr_seq_intercambio_w		number(10);
cd_estabelecimento_w		number(4);

begin

select	dt_contratacao,
	nr_seq_contrato,
	nr_seq_intercambio
into	dt_adesao_w,
	nr_seq_contrato_w,
	nr_seq_intercambio_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

select	decode(cd_pf_estipulante,null,'PJ','PF'),
	cd_estabelecimento
into	ie_tipo_contrato_w,
	cd_estabelecimento_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_w;

select	dt_referencia
into	dt_mesano_referencia_w
from	pls_mensalidade
where	nr_sequencia	= nr_seq_mensalidade_p;

if	(nvl(nr_seq_contrato_w,0) <> 0) then
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_mens_contrato
	where	nr_seq_contrato	= nr_seq_contrato_w
	and	ie_tipo_regra	= 'L'
	and	trunc(dt_mesano_referencia_w,'dd')	>= trunc(dt_inicio_vigencia,'dd')
	and	trunc(dt_mesano_referencia_w,'dd')	<= trunc(nvl(dt_fim_vigencia,dt_mesano_referencia_w),'dd');
elsif	(nvl(nr_seq_intercambio_w,0) <> 0) then
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_mens_contrato
	where	nr_seq_intercambio	= nr_seq_intercambio_w
	and	ie_tipo_regra		= 'L'
	and	trunc(dt_mesano_referencia_w,'dd')	>= trunc(dt_inicio_vigencia,'dd')
	and	trunc(dt_mesano_referencia_w,'dd')	<= trunc(nvl(dt_fim_vigencia,dt_mesano_referencia_w),'dd');
end if;

if	(nvl(nr_seq_regra_w,0) = 0) then
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_mens_contrato
	where	nr_seq_contrato	is null
	and	nr_seq_intercambio is null
	and	ie_tipo_regra		= 'L'
	and	cd_estabelecimento	= cd_estabelecimento_w
	and	trunc(dt_mesano_referencia_w,'dd')	>= trunc(dt_inicio_vigencia,'dd')
	and	trunc(dt_mesano_referencia_w,'dd')	<= trunc(nvl(dt_fim_vigencia,dt_mesano_referencia_w),'dd')
	and	((nvl(ie_pessoa_contrato,'A') = ie_tipo_contrato_w) or (ie_pessoa_contrato = 'A'));
end if;

if	(nvl(nr_seq_regra_w,0)	> 0) then
	select	lpad(to_char(nvl(a.dt_limite_movimentacao,'0')),2,0),
		a.ie_tipo_data_limite
	into	dt_limite_movimentacao_w,
		ie_tipo_data_limite_w
	from	pls_regra_mens_contrato a
	where	a.nr_sequencia	= nr_seq_regra_w;
end if;

if	(dt_limite_movimentacao_w <> '00') then
	
	select	decode(ie_tipo_data_limite_w,'C',0,'A',-1)
	into	vl_variacao_mes_w	
	from	dual;
	
	begin
	dt_fim_movimentacao_w		:= add_months(to_date(dt_limite_movimentacao_w || to_char(dt_mesano_referencia_w,'mm/yyyy')),vl_variacao_mes_w);
	exception
	when others then
		dt_fim_movimentacao_w	:= add_months(last_day(dt_mesano_referencia_w),vl_variacao_mes_w);
	end;
	
	dt_inicio_movimentacao_w	:= add_months(dt_fim_movimentacao_w,-1) + 1;

	if	(dt_adesao_w	> dt_fim_movimentacao_w) then
		ds_retorno_w	:= 'N';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_mens_seg;
/