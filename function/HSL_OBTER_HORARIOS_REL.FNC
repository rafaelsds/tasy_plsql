create or replace
function hsl_obter_horarios_rel (
				nr_prescricao_p	number,
				nr_seq_item_p	number,
				ie_tipo_item_p	varchar2,
				dt_inicial_P	date,
				dt_final_p		date)
				return varchar2 is
	
hr_horario_w	varchar2(10);
ds_horarios_w	varchar2(255);
	
cursor c01 is
select	to_char(dt_horario,'hh24:mi')
from	prescr_mat_hor
where	nr_prescricao = nr_prescricao_p
and		nr_seq_material = nr_seq_item_p
and		dt_horario between dt_inicial_p and dt_final_p
order by dt_horario;

cursor c02 is
select	to_char(dt_horario,'hh24:mi')
from	prescr_rec_hor
where	nr_prescricao = nr_prescricao_p
and		nr_seq_recomendacao = nr_seq_item_p
and		dt_horario between dt_inicial_p and dt_final_p
order by dt_horario;

cursor c03 is
select	to_char(dt_horario,'hh24:mi')
from	prescr_proc_hor
where	nr_prescricao = nr_prescricao_p
and		nr_seq_procedimento = nr_seq_item_p
and		dt_horario between dt_inicial_p and dt_final_p
order by dt_horario;

cursor c04 is
select	to_char(dt_horario,'hh24:mi')
from	prescr_dieta_hor
where	nr_prescricao = nr_prescricao_p
and		nr_seq_dieta = nr_seq_item_p
and		dt_horario between dt_inicial_p and dt_final_p
order by dt_horario;
	
begin

if	(ie_tipo_item_p	= 'M') then
	open c01;
	loop
	fetch c01 into
		hr_horario_w;
	exit when c01%notfound;
		ds_horarios_w	:= ds_horarios_w || ' ' || hr_horario_w;
	end loop;
	close c01;
elsif	(ie_tipo_item_p	= 'R') then
	open c02;
	loop
	fetch c02 into
		hr_horario_w;
	exit when c02%notfound;
		ds_horarios_w	:= ds_horarios_w || ' ' || hr_horario_w;
	end loop;
	close c02;
elsif	(ie_tipo_item_p	= 'P') then
	open c03;
	loop
	fetch c03 into
		hr_horario_w;
	exit when c03%notfound;
		ds_horarios_w	:= ds_horarios_w || ' ' || hr_horario_w;
	end loop;
	close c03;
elsif	(ie_tipo_item_p	= 'D') then
	open c04;
	loop
	fetch c04 into
		hr_horario_w;
	exit when c04%notfound;
		ds_horarios_w	:= ds_horarios_w || ' ' || hr_horario_w;
	end loop;
	close c04;
end if;

return ds_horarios_w;

end hsl_obter_horarios_rel;
/
