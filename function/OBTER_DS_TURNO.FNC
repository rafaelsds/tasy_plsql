create or replace function Obter_ds_turno( ie_turno	number)
			     return varchar2 is

ds_turno_w		varchar2(15);

begin

if (ie_turno = 0) then
   ds_turno_w := wheb_mensagem_pck.get_texto(309282); -- Manh�
elsif (ie_turno = 1) then
   ds_turno_w := wheb_mensagem_pck.get_texto(309281); -- Tarde
elsif (ie_turno = 3)then
   ds_turno_w := wheb_mensagem_pck.get_texto(309280); -- Noite
end if;

return	ds_turno_w;

end Obter_ds_turno;
/
