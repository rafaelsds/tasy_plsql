create or replace
function Obter_Se_exibe_dispositivo(	ie_bomba_infusao_p	varchar2,
					ie_tipo_item_p		number)
 		    	return varchar2 is

cont_w		number(15,0);
ie_exibir_w	varchar2(20) := 'S';

begin

select	count(*)
into	cont_w
from	rep_dispositivo
where	nvl(ie_tipo_item,1)	= ie_tipo_item_p;

if	(cont_w	> 0) then
	ie_exibir_w	:= 'N';
	select	count(*)
	into	cont_w
	from	rep_dispositivo
	where	ie_bomba_infusao	= ie_bomba_infusao_p
	and	nvl(ie_tipo_item,1)	= ie_tipo_item_p;
	
	if	(cont_w	> 0) then
		ie_exibir_w	:= 'S';
	end if;	
end if;

return	ie_exibir_w;

end Obter_Se_exibe_dispositivo;
/
