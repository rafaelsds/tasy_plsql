create or replace function man_obter_os_ccb (
		NR_SEQ_ORDEM_SERV_P number)
	return number
is
	ds_retorno_w		man_ordem_servico.nr_sequencia%type;
	nr_seq_projeto_w	proj_projeto.nr_sequencia%type;
	nr_seq_os_tc_os_w	man_ordem_servico.nr_sequencia%type;
begin

	select	proj_obter_projeto_ordem_serv(nr_seq_ordem_serv_p)
	into	nr_seq_projeto_w
	from	dual;
	
	select 	max(nr_seq_service_order)
	into 	nr_seq_os_tc_os_w
	from  	reg_tc_so_dev
	where 	nr_seq_pendency = (	select nr_seq_pendency
								from man_ordem_servico 
								where nr_sequencia = nr_seq_ordem_serv_p);
								
	select	max(coalesce(nr_seq_os_tc_os_w, mos.nr_seq_ordem_serv_pai, to_number(proj_obter_dados_projeto(nr_seq_projeto_w, 'OS'))))
	into	ds_retorno_w
	from	man_ordem_servico mos
	where	mos.nr_sequencia = nr_seq_ordem_serv_p;

	return ds_retorno_w;
end man_obter_os_ccb;
/
