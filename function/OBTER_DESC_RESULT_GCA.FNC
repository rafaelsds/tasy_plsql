create or replace
FUNCTION obter_desc_result_gca(		nr_sequencia_p number)
			return varchar2 is

ds_retorno_w	varchar2(200);

BEGIN

select	ds_resultado
into	ds_retorno_w
from	gca_item_result
where	nr_sequencia	= nr_sequencia_p;

return	ds_retorno_w;

END obter_desc_result_gca;
/