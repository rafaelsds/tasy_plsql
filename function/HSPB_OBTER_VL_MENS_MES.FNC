create or replace function hspb_obter_vl_mens_mes
			(	nr_seq_pagador_p	varchar2,
				dt_referencia_p		varchar2,
				ie_opcao_p		varchar2)
				return number is
ds_retorno_w	number(10,2);

begin

if	(ie_opcao_p = 'JAN') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/01/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'FEV') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/02/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'MAR') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/03/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'ABR') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/04/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'MAI') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/05/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'JUN') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/06/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'JUL') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/07/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'AGO') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/08/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'SET') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/09/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'OUT') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/10/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'NOV') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/11/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;
if	(ie_opcao_p = 'DEZ') then
	select 	nvl(sum(d.vl_recebido)+sum(d.vl_juros)+sum(d.vl_multa), 0.00)
	into	ds_retorno_w
	from 	titulo_receber		f,
		titulo_receber_liq	d,
		pls_mensalidade		a
	where	d.nr_titulo		= f.nr_titulo
	and	a.nr_sequencia		= f.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	trunc(to_date('01/12/' || dt_referencia_p),'mm') = trunc(d.dt_recebimento,'mm');
end if;

return	ds_retorno_w;

end hspb_obter_vl_mens_mes;