create or replace
function OBTER_QT_DIAS_AVISO_ATRASO(
				cd_estabelecimento_p		Number)
			return number is

qt_dias_aviso_atraso_w		Number(15,4);

begin
select	nvl(max(qt_dias_aviso_atraso),60)
into	qt_dias_aviso_atraso_w
from	parametro_compras
where	cd_estabelecimento	= cd_estabelecimento_p;

return qt_dias_aviso_atraso_w;

end OBTER_QT_DIAS_AVISO_ATRASO;
/