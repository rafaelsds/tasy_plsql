create or replace
function validar_atend_regra_triagem (	cd_estabelecimento_p	number,
					ie_clinica_p		number,
					hr_geracao_p		varchar2,
					nr_seq_classificacao_p	number)
				return varchar2 is
					
ie_retorno_w		varchar2(2)	:= 'N';
cd_estabelecimento_w	number(4);
ie_clinica_w		number(5);
ie_possui_regra_w	varchar2(1);
ie_possui_count_w       number(5);
nr_seq_classificacao_w  number(10);

Cursor C01 is
	select	cd_estabelecimento,
		ie_clinica,
		nr_seq_classificacao
	from	atend_regra_triagem
	where	ie_situacao = 'A'
	and	(((hr_inicial is null) or (hr_final is null)) or (to_number(to_char(to_date(hr_geracao_p,'dd/mm/yyyy hh24:mi:ss'),'hh24')||to_char(to_date(hr_geracao_p,'dd/mm/yyyy hh24:mi:ss'),'mi')) between
			to_number(to_char(hr_inicial,'hh24')||to_char(hr_inicial,'mi')) and to_number(to_char(hr_final,'hh24')||to_char(hr_final,'mi'))));
	
begin
Select 	count(*)
into 	ie_possui_count_w
from	atend_regra_triagem
where 	cd_estabelecimento = cd_estabelecimento_p;

select 	decode(count(*),0,'N','S')
into	ie_possui_regra_w
from	atend_regra_triagem
where	cd_estabelecimento = cd_estabelecimento_p
and	((nvl(ie_clinica, ie_clinica_p) = ie_clinica_p) or (ie_clinica_p = 0))
and	((nvl(nr_seq_classificacao, nr_seq_classificacao_p) = nr_seq_classificacao_p) or (nr_seq_classificacao_p = 0));

if	(ie_possui_regra_w = 'N') and (nvl(ie_possui_count_w,0) = 0) then
		ie_retorno_w := 'S';
end if;		

if	(ie_possui_regra_w = 'S') then
	open C01;
	loop
	fetch C01 into
		cd_estabelecimento_w,			
		ie_clinica_w,
		nr_seq_classificacao_w;
	exit when C01%notfound;
		begin
		
		if	(cd_estabelecimento_p = cd_estabelecimento_w) and
			(nr_seq_classificacao_p = nvl(nr_seq_classificacao_w, nr_seq_classificacao_p) or (nr_seq_classificacao_p = 0)) and	
			((NVL(ie_clinica_w, ie_clinica_p) = ie_clinica_p) OR (ie_clinica_p = 0)) then
			ie_retorno_w := 'S';
			
		end if;
		
		end;
		
	end loop;

	close C01;
end if;

return	ie_retorno_w;

end validar_atend_regra_triagem;
/
