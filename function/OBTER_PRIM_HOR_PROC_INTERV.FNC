create or replace
function obter_prim_hor_proc_interv(	nr_atendimento_p	number,
										nr_prescricoes_p	varchar2,
										dt_inicio_prescr_p	date,
										cd_procedimento_p	number,
										ie_origem_proced_p	number,
										cd_intervalo_p		varchar2,
										ie_recalc_horario_p	varchar2,
										ie_regra_prim_hor_p	varchar2)
				return date is

nr_hora_interv_w	number(15,4);
dt_horario_w		date;
dt_horario_ww		date;
dt_ultima_exec_w	date;
dt_final_w			date;
dt_inicio_w 		date;
hr_prim_horario_w	varchar2(5);
nr_prescricoes_w	varchar2(255);

begin
nr_prescricoes_w	:= ' ' || nr_prescricoes_p || ' ';
nr_prescricoes_w	:= replace(nr_prescricoes_w, ',',' ');
nr_prescricoes_w	:= replace(nr_prescricoes_w, '  ',' ');

dt_inicio_w 		:= dt_inicio_prescr_p - 5;
dt_final_w			:= dt_inicio_prescr_p + 5;

if	(ie_recalc_horario_p = 'I') or
	(ie_regra_prim_hor_p = 'H') then
	begin
	if	(nr_atendimento_p is not null) and
		(dt_inicio_prescr_p is not null) and
		(cd_procedimento_p is not null) and
		(ie_origem_proced_p is not null) and
		(cd_intervalo_p is not null) then

		nr_hora_interv_w	:= obter_ocorrencia_intervalo(cd_intervalo_p, 24, 'H');
		if	(somente_numero(nr_hora_interv_w) = 0) then
			nr_hora_interv_w	:= 1;
		end if;
		
		dt_horario_w		:= dt_inicio_prescr_p - nr_hora_interv_w / 24;

		select	max(c.dt_horario)
		into	dt_horario_ww
		from	prescr_proc_hor c,
				prescr_procedimento b,
				prescr_medica a
		where	c.nr_prescricao		= b.nr_prescricao
		and		c.nr_seq_procedimento	= b.nr_sequencia
		and		c.dt_suspensao		is null
		and		Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
		and		nvl(c.ie_situacao,'A')	= 'A'
		and		c.dt_horario		>= dt_horario_w - 220/24
		and		b.dt_suspensao		is null
		and		b.cd_procedimento	= cd_procedimento_p
		and		b.ie_origem_proced	= ie_origem_proced_p
		--and	b.nr_seq_exame is null
		and		b.nr_prescricao		= a.nr_prescricao
		and		a.dt_suspensao		is null
		and		nvl(a.dt_liberacao,a.dt_liberacao_medico)		is not null
		and		nr_prescricoes_w  like	'% ' || a.nr_prescricao || ' %'
		and		a.dt_inicio_prescr between dt_inicio_w and dt_final_w
		and		a.nr_atendimento	= nr_atendimento_p;
		
		if	(dt_horario_ww is not null) then
			dt_horario_ww	:= dt_horario_ww + nr_hora_interv_w / 24;
		end if;

	end if;
	end;
elsif	(ie_recalc_horario_p = 'H') then
	begin
	if	(nr_atendimento_p is not null) and
		(dt_inicio_prescr_p is not null) and
		(cd_procedimento_p is not null) and
		(ie_origem_proced_p is not null) and
		(cd_intervalo_p is not null) then

		nr_hora_interv_w	:= obter_ocorrencia_intervalo(cd_intervalo_p, 24, 'H');
		if	(somente_numero(nr_hora_interv_w) = 0) then
			nr_hora_interv_w	:= 1;
		end if;

		select	min(c.dt_horario)
		into	dt_horario_ww
		from	prescr_proc_hor c,
			prescr_procedimento b,
			prescr_medica a
		where	c.nr_prescricao		= b.nr_prescricao
		and	c.nr_seq_procedimento	= b.nr_sequencia
		and	b.nr_prescricao		= a.nr_prescricao
		and	c.dt_suspensao		is null
		and	nvl(c.ie_situacao,'A')	= 'A'
		and	b.dt_suspensao		is null
		and	b.cd_procedimento	= cd_procedimento_p
		and	b.ie_origem_proced	= ie_origem_proced_p
		and	a.dt_inicio_prescr between dt_inicio_w and dt_final_w
		and	b.nr_seq_exame is null
		and	a.dt_suspensao		is null
		and	nvl(a.dt_liberacao,a.dt_liberacao_medico) is not null
		and	a.nr_atendimento	= nr_atendimento_p
		and	nr_prescricoes_w  like	'% ' || a.nr_prescricao || ' %'
		and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S';
		
		if	(dt_horario_ww is not null) then
			begin
			while	(establishment_timezone_utils.startofday(dt_horario_ww) >= establishment_timezone_utils.startofday(dt_inicio_prescr_p)) loop 
				begin
				dt_horario_ww	:= dt_horario_ww - 1;
				end;
			end loop;
			while	(dt_horario_ww < dt_inicio_prescr_p) loop 
				begin
				dt_horario_ww	:= dt_horario_ww + (nvl(nr_hora_interv_w,1) / 24);
				end;
			end loop;
			end;
		end if;

	end if;
	end;
end if;

return dt_horario_ww;

end obter_prim_hor_proc_interv;
/