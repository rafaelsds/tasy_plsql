create or replace
function hem_obter_seq_manomet(	ie_tipo_p		varchar2)
 		    	return number is

nr_seq_apresent_w	valor_dominio.nr_seq_apresent%type;
begin
if	(ie_tipo_p is not null) then

	select	max(nr_seq_apresent)
	into	nr_seq_apresent_w
	from	valor_dominio
	where	cd_dominio = 6939
	and	vl_dominio = ie_tipo_p;
	
end if;
return	nr_seq_apresent_w;

end hem_obter_seq_manomet;
/