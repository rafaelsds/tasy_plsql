create or replace
function Obter_qt_dose_onco(	cd_material_p		number,
				qt_dose_p		number,
				ie_via_aplicacao_p	varchar2,
				ie_dispensacao_p	varchar2 default 'N',
				cd_intervalo_p		varchar2 default null)
 		    	return  number is
			
			
ie_via_aplicacao_w	varchar2(10);	
qt_retorno_w		number(18,6);
ie_regra_w		varchar2(2);
qt_multiplo_w		number(18,6);
nr_multiplo_w		Number(15,4)	:= 0;
qt_casas_decimais_w	number(10);
qt_resto_w			number(18,6);
cd_grupo_w			number(3)	:= 0;
cd_subgrupo_w			number(3)	:= 0;
cd_classe_w			number(5)	:= 0;
cd_intervalo_w		varchar2(10)	:= nvl(cd_intervalo_p,'XPTO');
cd_estabelecimento_w 	number(10) := wheb_usuario_pck.get_cd_estabelecimento;
		
			
Cursor C01 is
	select	ie_regra,
		nvl(qt_multiplo,1),
		nvl(QT_CASAS_DECIMAIS,0)
	from	regra_disp_oncologia
	where	1=1
	and		nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and 	(((nvl(ie_utiliza_disp,'S')=  'N') and (ie_dispensacao_p = 'N')) or 
		(nvl(ie_utiliza_disp,'S') in ('S','E')))  
	and	nvl(cd_material,cd_material_p)			= cd_material_p
	and 	nvl(cd_classe_material, cd_classe_w)		= cd_classe_w
	and 	nvl(cd_subgrupo_material, cd_subgrupo_w) 		= cd_subgrupo_w
	and 	nvl(cd_grupo_material, cd_grupo_w) 	 	= cd_grupo_w
	and	nvl(ie_via_aplicacao,ie_via_aplicacao_w)	= ie_via_aplicacao_w
	and	nvl(cd_intervalo,cd_intervalo_w)	= cd_intervalo_w
	order by nvl(ie_via_aplicacao,'0'),
			nvl(cd_intervalo,'0'),
	nvl(cd_material,0),
	nvl(cd_classe_material,0),
	nvl(cd_subgrupo_material,0),
	nvl(cd_grupo_material,0);
	

begin
ie_via_aplicacao_w	:= nvl(ie_via_aplicacao_p,'0');

select	nvl(max(a.cd_classe_material),0),
	nvl(max(b.cd_subgrupo_material),0),
	nvl(max(c.cd_grupo_material),0)
into	cd_classe_w,
	cd_subgrupo_w,
	cd_grupo_w
from	material a,
	classe_material b,
	subgrupo_material c
where	a.cd_material          = cd_material_p
and a.cd_classe_material   = b.cd_classe_material
and b.cd_subgrupo_material = c.cd_subgrupo_material;
	
	
open C01;
loop
fetch C01 into	
	ie_regra_w,
	qt_multiplo_w,
	QT_CASAS_DECIMAIS_w;
exit when C01%notfound;
end loop;
close C01;

if	(ie_regra_w	= 'P') then
	qt_retorno_w	:= round(qt_dose_p,QT_CASAS_DECIMAIS_w);
elsif	(ie_regra_w	= 'S') then

	if	((trunc(qt_dose_p / qt_multiplo_w) * qt_multiplo_w) = qt_dose_p) then
		nr_multiplo_w	:= (qt_dose_p / qt_multiplo_w);
	else
		nr_multiplo_w   := trunc(qt_dose_p / qt_multiplo_w) + 1;
	end if;
	qt_retorno_w	:= qt_multiplo_w * nr_multiplo_w;
	
elsif	(ie_regra_w	= 'I') then

	if	((trunc(qt_dose_p / qt_multiplo_w) * qt_multiplo_w) = qt_dose_p) then
		nr_multiplo_w	:= (qt_dose_p / qt_multiplo_w);
	else
		nr_multiplo_w   := trunc(qt_dose_p / qt_multiplo_w);
	end if;
	qt_retorno_w	:= qt_multiplo_w * nr_multiplo_w;
elsif	(ie_regra_w	= 'A') then
	qt_resto_w	:= mod(qt_dose_p,qt_multiplo_w);
	
	if	((qt_resto_w/qt_multiplo_w ) >= 0.5) then
		nr_multiplo_w   := trunc(qt_dose_p / qt_multiplo_w) + 1;
	else
		nr_multiplo_w   := trunc(qt_dose_p / qt_multiplo_w);
	end if;
	qt_retorno_w	:= qt_multiplo_w * nr_multiplo_w;
else
	qt_retorno_w	:= qt_dose_p;
	
end if;


return	qt_retorno_w;

end Obter_qt_dose_onco;
/
