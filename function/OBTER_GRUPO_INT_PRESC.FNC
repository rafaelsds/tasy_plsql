create or replace function obter_grupo_int_presc(
				nr_seq_subgrupo_p 	interv_presc_gruposubgrupo.nr_seq_subgrupo%type )
				return varchar2 is

ds_retorno_w		varchar2(200);
nr_seq_grup_interv_prescr_w number(10);

begin
if (nr_seq_subgrupo_p is not null) then
	begin
		 select  max(nr_seq_grupo_interv_prescr) into nr_seq_grup_interv_prescr_w
        from 	subgrupo_intervalo_prescr
        where nr_sequencia = nr_seq_subgrupo_p;

        select  substr(ds_grupo_interv_prescr,1,200)
        into	ds_retorno_w
        from 	grupo_intervalo_prescr
        where nr_sequencia = nr_seq_grup_interv_prescr_w;
	end;
end if;

return ds_retorno_w;

end obter_grupo_int_presc;
/
