create or replace
function  permite_fornec_cancelar_ocie(
			nr_seq_item_entrega_p	number)
	return 			Varchar2 is

nr_ordem_compra_w	number(10);
dt_prevista_entrega_w	date;
cd_estabelecimento_w	number(4);
qt_dias_cancel_fornec_w	number(10);
ds_retorno_w		varchar2(1) := 'S';

begin

select	nr_ordem_compra,
	dt_prevista_entrega
into	nr_ordem_compra_w,
	dt_prevista_entrega_w
from	ordem_compra_item_entrega
where	nr_sequencia = nr_seq_item_entrega_p;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_w;

select	nvl(max(qt_dias_cancel_fornec), 0)
into	qt_dias_cancel_fornec_w
from	parametro_compras
where	cd_estabelecimento	 = cd_estabelecimento_w;

if	(qt_dias_cancel_fornec_w > 0) and
	((trunc(sysdate,'dd') >= (trunc(dt_prevista_entrega_w, 'dd') - qt_dias_cancel_fornec_w))) then
	ds_retorno_w	:= 'N';
end if;

return	ds_retorno_w;

end 	permite_fornec_cancelar_ocie;
/
