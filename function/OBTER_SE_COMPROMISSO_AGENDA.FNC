create or replace
function obter_se_compromisso_agenda(	dt_agenda_p		date,
					nm_usuario_p		varchar2,
					dt_final_p		Date)
					return varchar2 is

ie_compromisso_w	varchar2(01);
qt_convite_w		number(05,0);
qt_agenda_w		number(05,0);

begin

select	count(*)
into	qt_convite_w
from	agenda_tasy b,
	agenda_tasy_convite a
where	b.nr_sequencia		= a.nr_seq_agenda
and	b.ie_status		= 'M'
and	b.dt_agenda		= dt_agenda_p
and	nm_usuario_convidado	= nm_usuario_p;

select	count(*)
into	qt_agenda_w
from	agenda_tasy
where	ie_status		= 'M'
and	((dt_agenda		between dt_agenda_p and dt_final_p) 
or	(dt_final		between dt_agenda_p and dt_final_p))
and	nm_usuario_agenda	= nm_usuario_p;

ie_compromisso_w		:= 'N';
if	(qt_convite_w > 0) or
	(qt_agenda_w > 0) then
	ie_compromisso_w	:= 'S';
end if;

return	ie_compromisso_w;

end;
/