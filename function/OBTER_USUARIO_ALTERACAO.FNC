create or replace
function obter_usuario_alteracao
			(	nm_usuario_p		Varchar2)
				return Varchar2 is				

nm_usuario_alteracao_w	Varchar2(255);
				
begin

select	substr(obter_nome_usuario(nm_usuario_p), 1,255)
into	nm_usuario_alteracao_w
from	dual;


if	(nm_usuario_alteracao_w is null) then
	begin
		select	substr(pls_obter_dados_segurado(to_number(nm_usuario_p), 'N'), 1,255)
		into	nm_usuario_alteracao_w
		from 	dual;	
	exception
	when others then
		nm_usuario_alteracao_w := nm_usuario_p;
	end;
end if;

if	(nm_usuario_alteracao_w is null) then
	nm_usuario_alteracao_w := nm_usuario_p;
end if;

return	nm_usuario_alteracao_w;

end obter_usuario_alteracao;
/