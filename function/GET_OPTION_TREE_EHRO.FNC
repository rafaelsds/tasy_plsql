create or replace
function get_option_tree_ehro(	ie_param_158_p		varchar2,
								tree_level_p		number,
								nr_atendimento_p	varchar2,
								cd_pessoa_fisica_p	varchar2)
									return varchar2 is

ie_return_w		varchar2(3)	:= null;
									
begin

if	(ie_param_158_p = 'S') then
	ie_return_w := 'C';
            
	if (tree_level_p > 1) then
		ie_return_w := 'P';
	end if;
elsif (nr_atendimento_p > 0) then
	ie_return_w := 'P';   
			
	if (tree_level_p > 1) then
		ie_return_w := 'I';
    end if;
elsif (cd_pessoa_fisica_p is not null) then
	ie_return_w := 'I';
end if;

return	ie_return_w;

end get_option_tree_ehro;
/