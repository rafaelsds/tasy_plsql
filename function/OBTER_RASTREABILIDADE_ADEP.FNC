create or replace
function obter_rastreabilidade_adep( nr_prescricao_p number,
                                     ie_tipo_processo_p rastreabilidade_adep.ie_tipo_processo%type )
            return varchar is

ie_gravar_processo_w varchar2(1 char);
nr_seq_regra_w rastreabilidade_adep.nr_sequencia%type;
ie_gera_log_w varchar2(1 char);

/*
ie_tipo_processo:

'Plano terapeutico': 'PT' 
'Administracao/Reversao do item': 'ADM'
'Aprazamento': 'APR'
'Reaprazamento': 'REA'
'Geracao de area de preparo': 'PL'
'Geracao de processo': 'GP'
'Preparacao de processo': 'PP'
'Processos do GEDIPA': 'PG'
*/

begin

ie_gera_log_w := 'N';

if  (wheb_assist_pck.obter_seq_rastr_adep is not null) then
    select  nvl(max('S'),'N')
    into    ie_gravar_processo_w
    from    rastreabilidade_adep a
    where   a.nr_sequencia = wheb_assist_pck.obter_seq_rastr_adep
    and     a.ie_tipo_processo = ie_tipo_processo_p;

    if  (ie_gravar_processo_w = 'S') then
        return 'S';
    end if;
end if;

select  max(a.nr_sequencia)
into    nr_seq_regra_w
from    rastreabilidade_adep a
where   a.ie_tipo_processo = ie_tipo_processo_p
and     (a.nm_usuario_regra = wheb_usuario_pck.get_nm_usuario or a.nm_usuario_regra is null)
and     (a.cd_perfil = wheb_usuario_pck.get_cd_perfil or a.cd_perfil is null)
and     (a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento or a.cd_estabelecimento is null)
and     sysdate between nvl(a.dt_inicio_vigencia,sysdate - 1) and nvl(a.dt_fim_vigencia,sysdate + 1);

if  (nr_seq_regra_w is not null) then
    wheb_assist_pck.set_regra_rastr_adep(nr_seq_regra_w,nr_prescricao_p);
    ie_gera_log_w   := 'S';
end if;

return ie_gera_log_w;

end obter_rastreabilidade_adep;
/
