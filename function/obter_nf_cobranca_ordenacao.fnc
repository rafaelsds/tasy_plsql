create or replace
function obter_nf_cobranca_ordenacao (nr_titulo_p	number)
 		    	return number is
			
ds_retorno_w		number(38);
nr_nota_fiscal_w	titulo_receber.nr_nota_fiscal%type;

/*Rotina criada para atender a coluna NR_NOTA_FISCAL_ORDENACAO na tabela COBRANCA, coluna function, que tem por objeto trazer a informacao de nr_nota_fiscal do titulo ou NF vinculada ao titulo, 
considerando apenas numeros, e ser possivel ordernar do menor para maior, por isso o return dessa function eh number, se retornar varchar a ordenacao fica incorreta*/

begin

if (nr_titulo_p is not null) then

	select	nvl(max(nr_nota_fiscal),'0')
	into	nr_nota_fiscal_w
	from	titulo_receber
	where	nr_titulo = nr_titulo_p;
	
	if (nr_nota_fiscal_w = '0') then
		select	obter_dados_titulo_receber(nr_titulo, 'NF')
		into	nr_nota_fiscal_w
		from	titulo_receber
		where	nr_titulo = nr_titulo_p;
	end if;
	
end if;

begin

	nr_nota_fiscal_w := somente_numero_char(nr_nota_fiscal_w); --Deixar somente numeros

	if (nr_nota_fiscal_w is not null) then
		ds_retorno_w := to_number(nr_nota_fiscal_w);
	else
		ds_retorno_w := null;
	end if;
	
exception when others then
	ds_retorno_w := null;
end;

return	ds_retorno_w;

end obter_nf_cobranca_ordenacao;
/