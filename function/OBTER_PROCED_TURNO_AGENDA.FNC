create or replace
function obter_proced_turno_agenda(cd_agenda_p		number,
				   dt_agenda_p		date)
				   return number is

cd_procedimento_w	varchar2(10);
nr_seq_proc_interno_w	number(10,0);
ie_dia_semana_w		number(5,0);
			
begin

select	obter_cod_dia_semana(dt_agenda_p)
into	ie_dia_semana_w
from	dual;

select	max(nr_seq_proc_interno)
into	nr_seq_proc_interno_w
from	agenda_horario
where	cd_agenda = cd_agenda_p
and	((dt_dia_semana = ie_dia_semana_w) or ((dt_dia_semana = 9) and (ie_dia_Semana_w not in (7,1))))
and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_agenda_p)))
and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_agenda_p)))
and 	hr_inicial < hr_final
and	nvl(nr_minuto_intervalo,0) > 0
and	to_date(trunc(dt_agenda_p, 'dd') || ' ' || to_char(dt_agenda_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') between
			to_date(trunc(dt_agenda_p, 'dd') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') and
			to_date(trunc(dt_agenda_p, 'dd') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');

return	nr_seq_proc_interno_w;

end obter_proced_turno_agenda;
/