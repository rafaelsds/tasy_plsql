CREATE OR REPLACE
function obter_regra_local_entrega(		cd_estabelecimento_p	number,
					cd_material_p		number,
					ie_regra_p		varchar2,
					ie_tipo_ordem_p		varchar2)
return Number is

cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
qt_regra_w				number(5);
cd_local_entrega_w			number(4);
ie_controlado_w				varchar2(1);
ie_padronizado_w				varchar2(1);
cd_unidade_medida_compra_w		varchar2(30);
cd_perfil_w				perfil.cd_perfil%type;

/* ie_regra_p
	O = Ordem de compras*/

cursor c01 is
select	cd_local_entrega
from	regra_local_compra
where	cd_estabelecimento = cd_estabelecimento_p
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	nvl(cd_perfil, cd_perfil_w)				= cd_perfil_w
and	(nvl(cd_material, cd_material_p) 			= cd_material_p or cd_material_p = 0)
and	((nvl(ie_controlado,'A') = 'A') or (nvl(ie_controlado, 'A') 	= ie_controlado_w))
and	((nvl(ie_padronizado,'A') = 'A') or (nvl(ie_padronizado, 'A') 	= ie_padronizado_w))
and	nvl(cd_unidade_medida, cd_unidade_medida_compra_w)	= cd_unidade_medida_compra_w 
and	ie_ordem_compra = decode(ie_regra_p, 'O', 'S', 'N')
and	nvl(ie_tipo_ordem,ie_tipo_ordem_p) = ie_tipo_ordem_p
order by
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);

begin

cd_perfil_w	:= Obter_Perfil_Ativo;

select	count(*)
into	qt_regra_w
from	regra_local_compra;

if	(qt_regra_w > 0) then
	begin
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		substr(obter_se_medic_controlado(cd_material),1,1) ie_controlado,
		substr(obter_se_material_padronizado(cd_estabelecimento_p, cd_material),1,1) ie_padronizado
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		ie_controlado_w,
		ie_padronizado_w
	from	estrutura_material_v
	where	cd_material = cd_material_p;

	select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,255) cd_unidade_medida_compra
	into	cd_unidade_medida_compra_w
	from	material
	where	cd_material = cd_material_p;

	open c01;
	loop
	fetch c01 into
		cd_local_entrega_w;
	exit when c01%notfound;
	end loop;
	close c01;
	end;
end if;

return cd_local_entrega_w;

end obter_regra_local_entrega;
/