create or replace
function obter_tipo_classif_escala(nr_seq_classif_p	number)
				return varchar2 is

ie_tipo_escala_w	varchar2(10) := 'S';

begin

if	(nr_seq_classif_p is not null) then
	begin
	select	ie_tipo_escala
	into	ie_tipo_escala_w
	from	escala_classif e,
		escala_grupo d,
		escala c
	where 	c.nr_seq_grupo = d.nr_sequencia
	and	d.nr_seq_classif = e.nr_sequencia 
	and	c.nr_sequencia = nr_seq_classif_p
	and	rownum < 2;
	exception
	when others then
		ie_tipo_escala_w := '';
	end;
end if;

return	ie_tipo_escala_w;

end obter_tipo_classif_escala;
/