create or replace
function cm_obter_nome_item_orig(
		nr_seq_item_cont_p		number) return varchar2 is

ds_retorno_w			varchar2(255);
nr_seq_item_w			number(10);
nr_seq_conjunto_cont_w		number(10);
nr_seq_conjunto_w		number(10);
qt_existe_w			number(10);
nr_seq_item_orig_w		number(10);

begin

select	nr_seq_item,
	nr_seq_conjunto
into	nr_seq_item_w,
	nr_seq_conjunto_cont_w
from	cm_item_cont
where	nr_sequencia = nr_seq_item_cont_p;

select	nr_seq_conjunto
into	nr_seq_conjunto_w
from	cm_conjunto_cont
where	nr_sequencia = nr_seq_conjunto_cont_w
and	nvl(ie_situacao,'A') = 'A';

select	count(*)
into	qt_existe_w
from	cm_conjunto_item_sub
where	nr_seq_conjunto = nr_seq_conjunto_w
and	nr_seq_item_sub = nr_seq_item_w
and	dt_final_vigencia is null;

if	(qt_existe_w <> 0) then
	begin

	select	max(nr_seq_item)
	into	nr_seq_item_orig_w
	from	cm_conjunto_item_sub
	where	nr_seq_conjunto = nr_seq_conjunto_w
	and	nr_seq_item_sub = nr_seq_item_w
	and	dt_final_vigencia is null;

	select	nm_item
	into	ds_retorno_w
	from	cm_item
	where	nr_sequencia = nr_seq_item_orig_w;

	end;
end if;

return ds_retorno_w;

end cm_obter_nome_item_orig;
/