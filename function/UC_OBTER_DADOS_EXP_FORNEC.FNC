create or replace function	uc_obter_dados_exp_fornec 	(cd_cgc_p				varchar2,
										cd_pessoa_fisica_p		varchar2,
										ie_opcao_p				varchar2) return varchar2 is

/*
Op��o:
B: Banco
A: Agencia
C: Conta
DC: Digito da Conta
DA: Digito da Ag�ncia
*/

ds_retorno_w			varchar2(255);
cd_banco_w				varchar2(255);
cd_agencia_bancaria_w	varchar2(255);
nr_conta_w				varchar2(255);
nr_digito_conta_w		varchar2(255);
ie_digito_agencia_w		varchar2(255);
count_w					number(10);

begin

ds_retorno_w := '';

if (cd_cgc_p is not null) then
	begin

	select 	count(*)
	into	count_w
	from	pessoa_juridica_conta
	where	cd_cgc = cd_cgc_p;

	if	(count_w > 0 ) then
		begin

		select	cd_banco,
				cd_agencia_bancaria,
				nr_conta,
				nr_digito_conta,
				ie_digito_agencia
		into	cd_banco_w,
				cd_agencia_bancaria_w,
				nr_conta_w,
				nr_digito_conta_w,
				ie_digito_agencia_w
		from 	pessoa_juridica_conta
		where	cd_cgc = cd_cgc_p
		and		cd_banco = (
						select	max(x.cd_banco)
						from 	pessoa_juridica_conta x
						where	x.cd_cgc = cd_cgc_p);

		if (ie_opcao_p = 'B') then
			ds_retorno_w := cd_banco_w;
		elsif (ie_opcao_p = 'A') then
			ds_retorno_w := cd_agencia_bancaria_w;
		elsif (ie_opcao_p = 'C') then
			ds_retorno_w := nr_conta_w;
		elsif (ie_opcao_p = 'DC') then
			ds_retorno_w := nr_digito_conta_w;
		elsif (ie_opcao_p = 'DA') then
			ds_retorno_w := ie_digito_agencia_w;
		end if;

		end;
	end if;

	end;
end if;

if (cd_pessoa_fisica_p is not null) and (ie_opcao_p is null) then
	begin

	select 	count(*)
	into	count_w
	from	pessoa_fisica_conta_ctb
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and		dt_fim_vigencia > sysdate;

	if (count_w > 0) then
		begin

		select	somente_numero(a.cd_conta_contabil)
		into	ds_retorno_w
		from	pessoa_fisica_conta_ctb a
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and		a.dt_fim_vigencia > sysdate
		and		a.nr_sequencia = (select max(x.nr_sequencia)
								  from   pessoa_fisica_conta_ctb x
								  where  x.cd_pessoa_fisica = cd_pessoa_fisica_p
								  and	 x.dt_fim_vigencia > sysdate);

		end;
	end if;

	end;
end if;

return ds_retorno_w;

end uc_obter_dados_exp_fornec;
/