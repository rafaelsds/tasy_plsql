create or replace
function Obter_Origem_conv_atend (nr_atendimento_p	number,
								  ie_opcao_p		varchar)
				return varchar2 is

nr_seq_origem_w		number(10);
ds_retorno_w		varchar2(60);

/*
	ie_opcao_p	:	'C' - C�digo
				'D' - Descri��o		*/

begin

select	max(nvl(nr_seq_origem,0))
into	nr_seq_origem_w
from	atend_categoria_convenio
where	nr_atendimento	= nr_atendimento_p;

if	(nr_seq_origem_w <> 0) then

	if	(upper(ie_opcao_p) = 'C') then
		ds_retorno_w	:= substr(nr_seq_origem_w,1,10);

	elsif	(upper(ie_opcao_p) = 'D') then
		select	ds_origem
		into	ds_retorno_w
		from	convenio_origem_usuario
		where	nr_sequencia	= nr_seq_origem_w;
	end if;
end if;

return	ds_retorno_w;

end Obter_Origem_conv_atend;
/