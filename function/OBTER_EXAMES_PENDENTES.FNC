create or replace
function obter_exames_pendentes(nr_prescricao_p number,
				nm_usuario_p	Varchar2,
				cd_estabelecimento_p Number) return  varchar2 as 
retorno_w varchar2(250) := ' ';
begin
	declare
		nm_exame_w    	varchar2(250);
		Cursor C01 is
			SELECT	f.nm_exame
			FROM	lab_parametro d, 
				prescr_procedimento b, 
				prescr_medica c,
				exame_laboratorio f
			WHERE	b.nr_prescricao = c.nr_prescricao  
			AND	c.cd_estabelecimento = d.cd_estabelecimento 
			AND	b.nr_seq_exame    = f.nr_seq_exame
			AND     obter_ds_status_result_exame(b.nr_prescricao, b.nr_sequencia, nm_usuario_p, cd_estabelecimento_p) = Wheb_mensagem_pck.get_texto(309557) /*'Processando'*/
			AND	b.nr_prescricao = nr_prescricao_p;
		BEGIN
			OPEN C01;
				LOOP
					FETCH C01 into
						nm_exame_w;
					exit when c01%notfound;
					begin						
						if	( retorno_w = ' ')	then
							retorno_w :=  ' ' || Wheb_mensagem_pck.get_texto(309549, 'NM_EXAME_W='||nm_exame_w); --' Exames Pendentes: '||nm_exame_w;
						else
							retorno_w := retorno_w ||' - '|| nm_exame_w;
						end if;
					exception
						when others then
							begin
								dbms_output.put_line(Wheb_mensagem_pck.get_texto(1042279));
							end;
					end;
				END LOOP;
			CLOSE C01;
		END;
	return retorno_w;
end obter_exames_pendentes;
/