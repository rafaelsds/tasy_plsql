create or replace
function obter_setor_mat_auditoria(	nr_seq_interno_p	number,
					nr_seq_item_p		number)
 		    	return number is
				
cd_setor_atendimento_w	number(10) := 0;		

begin

if	(nvl(nr_seq_interno_p,0) <> 0) then
	select	cd_setor_atendimento
	into	cd_setor_atendimento_w
	from	atend_paciente_unidade
	where	nr_seq_interno = nr_seq_interno_p;
end if;

if 	(nvl(cd_setor_atendimento_w,0) = 0) then
	select	cd_setor_atendimento
	into	cd_setor_atendimento_w
	from	material_atend_paciente
	where	nr_sequencia	= nr_seq_item_p;
end if;

return	cd_setor_atendimento_w;

end obter_setor_mat_auditoria;
/