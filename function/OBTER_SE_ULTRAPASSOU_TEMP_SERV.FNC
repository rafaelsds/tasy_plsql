create or replace
function obter_se_ultrapassou_temp_serv	(	nr_sequencia_p	Number,
						nr_seq_unid_p	Number,
						dt_param_p	Date)
 		    	return varchar2 is
			
qt_min_previsto_w	Number(10);
nr_seq_servico_w	Number(10);
dt_inicio_w		Date;
dt_total_w		Date;
retorno_w		varchar2(1) := 'N';
begin

select	nvl(max(nr_seq_servico),0)
into	nr_seq_servico_w
from	sl_unid_atend
where	nr_sequencia  = nr_seq_unid_p;
	
	if (nvl(nr_sequencia_p,0) = 0) then
	
		select	nvl(max(qt_min_previsto),0)
		into	qt_min_previsto_w
		from  	sl_servico
		where	nr_sequencia = nr_seq_servico_w;	
	
	else
		select	nvl(max(qt_min_previsto),0)
		into	qt_min_previsto_w
		from  	sl_servico
		where	nr_sequencia = nr_sequencia_p;
	
	end if;
	
	if	(qt_min_previsto_w	> 0) then
		
		select	max(dt_inicio)
		into	dt_inicio_w
		from	sl_unid_atend
		where	nr_sequencia  = nr_seq_unid_p;
		
		if (dt_inicio_w is not null) then
		
			select	(dt_inicio_w +  qt_min_previsto_w/1440)
			into	dt_total_w
			from 	dual;
		
			if (dt_total_w	< dt_param_p) then
				retorno_w := 'S';
			end if;
		
		end if;
	end if;
	
return	retorno_w;

end obter_se_ultrapassou_temp_serv;
/
