create or replace 
function HSA_obter_forn_ultima_compra(
			cd_material_p			number,
			cd_estabelecimento_p		number)
return varchar2 is

ds_retorno_w		varchar2(14) := '';
nr_seq_ultima_compra_w	number(10,0);
dt_emissao_w		date;

begin

select	max(n.dt_emissao)
into	dt_emissao_w
from	nota_fiscal n,
	nota_fiscal_item i
where	i.cd_material = cd_material_p
and	n.nr_sequencia	= i.nr_sequencia
and	n.ie_situacao = '1'
and	n.dt_atualizacao_estoque is not null
and	n.cd_estabelecimento = cd_estabelecimento_p;

if	(dt_emissao_w is not null) then
	select	nvl(max(n.nr_sequencia),0)
	into	nr_seq_ultima_compra_w
	from	nota_fiscal n,
		nota_fiscal_item i
	where	i.cd_material = cd_material_p
	and	n.nr_sequencia	= i.nr_sequencia
	and	n.ie_situacao = '1'
	and	n.dt_atualizacao_estoque is not null
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_emissao = dt_emissao_w;
end if;

if	(nr_seq_ultima_compra_w > 0) then
	select	b.cd_cgc_emitente
	into	ds_retorno_w
	from	nota_fiscal b
	where	b.nr_sequencia = nr_seq_ultima_compra_w;
else
	select	a.cd_cgc_emitente
	into	ds_retorno_w
	from	nota_fiscal a
	where	a.nr_sequencia = (
		select	max(b.nr_sequencia)
		from	natureza_operacao o,
			operacao_nota p,
			nota_fiscal n,
			nota_fiscal_item b
		where	b.nr_sequencia		= n.nr_sequencia
		and	n.cd_natureza_operacao	= o.cd_natureza_operacao
		and	b.cd_material		= cd_material_p
		and	n.cd_estabelecimento	= cd_estabelecimento_p
		and	o.ie_entrada_saida		= 'E'
		and	n.cd_operacao_nf 		= p.cd_operacao_nf
		and	nvl(p.ie_ultima_compra, 'S') 	= 'S'	
		and	n.ie_acao_nf		= '1'
		and	n.ie_situacao		= '1');
end if;

return	ds_retorno_w;

end HSA_obter_forn_ultima_compra;
/