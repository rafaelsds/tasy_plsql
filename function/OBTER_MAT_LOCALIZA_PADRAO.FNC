create or replace
function obter_mat_localiza_padrao (cd_estabelecimento_p number,
									cd_perfil_p			 number,
									ie_tipo_item_p		 varchar2,
									nm_usuario_p		 varchar2)
 		    	return varchar2 is

nr_seq_regra_w		mat_loc_pad_libe.nr_seq_regra%type;				
ie_tipo_padrao_w	material_localiza_padrao.ie_tipo_padrao%type;
ds_retorno_w		varchar2(5) := '';
ie_existe_libe_w	varchar2(1) := 'N';
ie_liberado_w		varchar2(1) := 'N';	

Cursor C01 is
	select 	nr_sequencia,
			ie_tipo_padrao
	from	material_localiza_padrao
	where	ie_tipo_item = ie_tipo_item_p
	order by nr_sequencia;

begin

open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	ie_tipo_padrao_w;
exit when C01%notfound;
	begin

	select	nvl(max('S'),'N')
	into	ie_existe_libe_w
	from 	mat_loc_pad_libe
	where	nr_seq_regra = nr_seq_regra_w;

	if (ie_existe_libe_w = 'S') then
	
		select 	nvl(max('S'),'N')
		into	ie_liberado_w
		from 	mat_loc_pad_libe
		where	nr_seq_regra = nr_seq_regra_w
		and 	nm_usuario_regra = nm_usuario_p;
		
		if (ie_liberado_w = 'N') then
			
			select 	nvl(max('S'),'N')
			into	ie_liberado_w
			from 	mat_loc_pad_libe
			where	nr_seq_regra = nr_seq_regra_w
			and 	cd_perfil = cd_perfil_p;
			
			if (ie_liberado_w = 'N') then			
				select 	nvl(max('S'),'N')
				into	ie_liberado_w
				from 	mat_loc_pad_libe
				where	nr_seq_regra = nr_seq_regra_w
				and 	cd_estabelecimento = cd_estabelecimento_p;
			end if;		
		end if;
		
		if (ie_liberado_w = 'S') then
		
			ds_retorno_w := ie_tipo_padrao_w;
			exit;
		end if;
	else
	
		ds_retorno_w := ie_tipo_padrao_w;
	end if;
	end;
end loop;
close C01;	

return ds_retorno_w;

end obter_mat_localiza_padrao;
/
