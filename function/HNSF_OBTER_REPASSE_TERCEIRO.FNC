Create or Replace
FUNCTION HNSF_OBTER_REPASSE_TERCEIRO(	dt_inicial_p		date,
					dt_final_p		date,
					nr_seq_terceiro_p	number,
					cd_convenio_p		number,
					ie_tipo_convenio_p	number,
					ie_status_p		varchar2)
					RETURN VarChar2 IS

nr_repasse_terceiro_w		number(10);
ds_lista_repasse_w		varchar2(4000);

cursor	c01 is
select	a.nr_repasse_terceiro
from	repasse_terceiro a
where	a.dt_mesano_referencia	between dt_inicial_p and fim_dia(dt_final_p)
and	(0 = nvl(nr_seq_terceiro_p,0) or a.nr_seq_terceiro = nr_seq_terceiro_p)
and	(0 = nvl(cd_convenio_p,0) or
	exists
	(select	1
	from	repasse_terceiro_item x
	where	x.nr_repasse_terceiro	= a.nr_repasse_terceiro
	and	x.cd_convenio		= cd_convenio_p))
and	(0 = nvl(ie_tipo_convenio_p,0) or
	exists
	(select	1
	from	convenio y,
		repasse_terceiro_item x
	where	x.nr_repasse_terceiro	= a.nr_repasse_terceiro
	and	x.cd_convenio		= y.cd_convenio
	and	y.ie_tipo_convenio	= ie_tipo_convenio_p))
and	('T' = nvl(ie_status_p,'T') or a.ie_status = ie_status_p)
order by	a.nr_repasse_terceiro;

BEGIN

ds_lista_repasse_w	:= '';

open	c01;
loop
fetch	c01 into
	nr_repasse_terceiro_w;
exit	when c01%notfound;

	if	(ds_lista_repasse_w is null) then
		ds_lista_repasse_w	:= nr_repasse_terceiro_w;
	else
		ds_lista_repasse_w	:= ds_lista_repasse_w || ', ' || nr_repasse_terceiro_w;
	end if;

end loop;
close c01;

RETURN ds_lista_repasse_w;

END HNSF_OBTER_REPASSE_TERCEIRO;
/