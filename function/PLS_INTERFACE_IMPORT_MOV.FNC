create or replace
function pls_interface_import_mov(cd_interface_p	varchar2)
 		    	return varchar2 is

ds_retorno_w 		varchar2(1) := 'N';

begin
/* Retorna somente as interfaces de importa��o v�lidas */
/* Lista de interfaces
3055 - COPEL
3029 - Padr�o RN  
*/
if( cd_interface_p in('3055','3029')) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end pls_interface_import_mov;
/