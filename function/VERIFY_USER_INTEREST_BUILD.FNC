CREATE OR REPLACE FUNCTION verify_user_interest_build(
    nr_seq_sche_build_p NUMBER,
    nm_user_p           VARCHAR2)
  RETURN VARCHAR2
IS
  user_is_interested_w VARCHAR2(350) := '';
  counter_w            NUMBER        := 0;
BEGIN
  SELECT COUNT(*)
  INTO counter_w
  FROM build_user_interest
  WHERE nr_seq_sche_build = nr_seq_sche_build_p
  AND nm_user_build       = nm_user_p;
  IF Counter_W            > 0 THEN
    user_is_interested_w := 'Y';
  ELSE
    user_is_interested_w := 'N';
  END IF;
RETURN user_is_interested_w;
END verify_user_interest_build;
/