create or replace function obter_tipo_campo_relatorio(
cd_tipo_p varchar2)

	return varchar2 is
	ds_retorno_w varchar2(12) := 'TEXTBOX';

begin

/*L = soon
N = report name
D = system date
P = page
U = user
C = report code pay*/

if(cd_tipo_p is not null) then
    if(cd_tipo_p in ('L'))then
        ds_retorno_w := 'IMAGEM';
    elsif(cd_tipo_p in ('N','D','P','U', 'C'))then
        ds_retorno_w := 'TEXTBOX';
    end if;
else
    ds_retorno_w := 'TEXTBOX';
end if;

return ds_retorno_w;

end obter_tipo_campo_relatorio;
/