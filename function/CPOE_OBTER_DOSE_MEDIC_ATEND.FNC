create or replace
function cpoe_obter_dose_medic_atend(	nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
										cd_material_p		material.cd_material%type,
										cd_unidade_medida_p	unidade_medida.cd_unidade_medida%type)
									return number is
					
qt_dose_retorno_w		prescr_material.qt_dose%type := 0;

begin	
if	(nr_atendimento_p is not null) then
	select	nvl(sum(obter_dose_convertida(b.cd_material, c.qt_dose, c.cd_unidade_medida, cd_unidade_medida_p)),0)
	into	qt_dose_retorno_w
	from	prescr_mat_hor c,
			prescr_material b,
			prescr_medica a
	where	a.nr_prescricao		= b.nr_prescricao
	and		a.nr_atendimento	= nr_atendimento_p
	and		b.cd_material		= cd_material_p
	and		b.nr_prescricao		= c.nr_prescricao
	and		b.nr_sequencia		= c.nr_seq_material
	and		a.dt_suspensao		is null
	and		nvl(b.ie_suspenso,'N')	= 'N'
	and		c.dt_suspensao		is null
	and		Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S';
end if;

return qt_dose_retorno_w;

end cpoe_obter_dose_medic_atend;
/