create or replace
function Obter_Se_Nota_Entrada_Saida
			(nr_seq_nota_p		number,
			ie_exibir_erro_p	varchar2 default 'N')
			return varchar2 is

ie_retorno_w			varchar2(01);
cd_operacao_estoque_w		number(03,0);
cd_operacao_nota_w		number(04,0);

begin

select	cd_operacao_nf
into	cd_operacao_nota_w
from	nota_fiscal
where	nr_sequencia		= nr_seq_nota_p;


select	nvl(max(cd_operacao_estoque),0)
into	cd_operacao_estoque_w
from	operacao_nota
where	cd_operacao_nf		= cd_operacao_nota_w;

if	(cd_operacao_estoque_w = 0) and (ie_exibir_erro_p = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(265532);
	--'A opera��o da nota n�o est� vinculada com uma opera��o de estoque.'
end if;

select	ie_entrada_saida
into	ie_retorno_w
from	operacao_estoque
where	cd_operacao_estoque	= cd_operacao_estoque_w;

return	ie_retorno_w; 

end Obter_Se_Nota_Entrada_Saida;
/
