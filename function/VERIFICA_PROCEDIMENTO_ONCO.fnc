create or replace function VERIFICA_PROCEDIMENTO_ONCO (
          nr_atendimento_p  in number,
          nr_seq_paciente_p	in number) return varchar2 is
          
  cd_protocolo_w           paciente_setor.cd_protocolo%type;
  nr_seq_medicacao_w       paciente_setor.nr_seq_medicacao%type;
  cd_convenio_w            atend_categoria_convenio.cd_convenio%type;
  nr_seq_proc_w            number;
  nr_agrupamento_proced_w  number;
  ds_registro_w            varchar2(1);
  count_w                  number;
begin

  ds_registro_w := 'N';
  begin
    select atend_categoria_convenio.cd_convenio
      into cd_convenio_w
      from atend_categoria_convenio
     where atend_categoria_convenio.nr_atendimento = nr_atendimento_p;
  exception
    when others then
      cd_convenio_w := null;
  end;  
  
  begin
    select cd_protocolo,
           nr_seq_medicacao
      into cd_protocolo_w,
           nr_seq_medicacao_w
      from paciente_setor
     where nr_seq_paciente = nr_seq_paciente_p;
  exception
    when others then
      cd_protocolo_w := null;
      nr_seq_medicacao_w := null;
  end;  
  
  begin
    select count(1)
      into count_w
      from regra_procedimento_onc
     where ((regra_procedimento_onc.cd_estabelecimento =
           wheb_usuario_pck.get_cd_estabelecimento) or
           (regra_procedimento_onc.cd_estabelecimento is null))
       and nvl(regra_procedimento_onc.cd_protocolo, nvl(cd_protocolo_w, 0)) =
           nvl(cd_protocolo_w, 0)
       and nvl(regra_procedimento_onc.cd_convenio, nvl(cd_convenio_w, 0)) =
           nvl(cd_convenio_w, 0)
       and nvl(regra_procedimento_onc.nr_seq_medicacao,
               nvl(nr_seq_medicacao_w, 0)) = nvl(nr_seq_medicacao_w, 0)
       and NVL(regra_procedimento_onc.ie_situacao,'A') = 'A';
  exception
    when others then
      count_w := 0;
  end;

  if nvl(count_w,0) > 0 then
    ds_registro_w := 'S';
  end if;
  
  return ds_registro_w;
end verifica_procedimento_onco;
/
