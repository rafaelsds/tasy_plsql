create or replace
function obter_desc_grupo_glosa(nr_seq_grupo_p	number)	return varchar2 is

ds_grupo_glosa_w		varchar2(255);

begin

select	max(ds_grupo)
into	ds_grupo_glosa_w
from 	grupo_glosa
where	nr_sequencia = nr_seq_grupo_p;

return	ds_grupo_glosa_w;

end obter_desc_grupo_glosa;
/
