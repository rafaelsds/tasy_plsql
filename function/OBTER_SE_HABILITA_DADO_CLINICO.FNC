create or replace
function obter_se_habilita_dado_clinico	(nr_seq_procedimento_p	number,
						cd_pessoa_fisica_p		varchar2,
						ie_opcao_p 			varchar2)
						return varchar2 is

qt_habilitado_w	number(10,0) 	:= 0;
ie_habilitado_w	varchar2(1)	:= 'N';

begin
if	(nr_seq_procedimento_p	is not null) and
	(cd_pessoa_fisica_p	is not null) then
	if	(nvl(ie_opcao_p,'MUP') = 'MUP') then
		select	nvl(count(*),0)
		into	qt_habilitado_w
		from	hem_medic a
		where	ie_uso_medic	= 'P'
		and	not exists	(select	1
					from	hem_proc_medic y,
						hem_proc x
					where	a.nr_sequencia	= y.nr_seq_medic
					and	y.ie_uso_medic	= 'P'
					and	y.nr_seq_proc		= x.nr_sequencia
					and	x.nr_sequencia	= nr_seq_procedimento_p
					and	x.cd_pessoa_fisica	= cd_pessoa_fisica_p);
	elsif	(nvl(ie_opcao_p,'MUP') = 'MUE') then
		select	nvl(count(*),0)
		into	qt_habilitado_w
		from	hem_medic a
		where	ie_uso_medic	= 'E'
		and	not exists	(select	1
					from	hem_proc_medic y,
						hem_proc x
					where	a.nr_sequencia	= y.nr_seq_medic
					and	y.ie_uso_medic	= 'E'
					and	y.nr_seq_proc		= x.nr_sequencia
					and	x.nr_sequencia	= nr_seq_procedimento_p
					and	x.cd_pessoa_fisica	= cd_pessoa_fisica_p);
	elsif	(nvl(ie_opcao_p,'MUP') = 'E') then
		select	nvl(count(*),0)
		into	qt_habilitado_w
		from	hem_exame a
		where	not exists	(select	1
					from	hem_proc_exame y,
						hem_proc x
					where	a.nr_sequencia	= y.nr_seq_exame
					and	y.nr_seq_proc		= x.nr_sequencia
					and	x.nr_sequencia	= nr_seq_procedimento_p
					and	x.cd_pessoa_fisica	= cd_pessoa_fisica_p);
	elsif	(nvl(ie_opcao_p,'MUP') = 'I') then
		select	nvl(count(*),0)
		into	qt_habilitado_w
		from	hem_dado_clinico a
		where	not exists	(select	1
					from	hem_proc_dado_clinico y,
						hem_proc x
					where	a.nr_sequencia	= y.nr_seq_dado_clinico
					and	y.nr_seq_proc		= x.nr_sequencia
					and	x.nr_sequencia	= nr_seq_procedimento_p
					and	x.cd_pessoa_fisica	= cd_pessoa_fisica_p);
	end if;

if	(nvl(qt_habilitado_w,0) > 0) then
	ie_habilitado_w	:= 'S';
end if;
end if;

return ie_habilitado_w;

end obter_se_habilita_dado_clinico;
/