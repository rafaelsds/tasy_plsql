create or replace function obter_desc_tratamento_agenda (
    nr_sequencia_p number
) return varchar2 is 
    ds_tratamento_w varchar2(100 char) := null;
begin 
    if(nr_sequencia_p is not null) then 
        select substr(rxt_obter_desc_tipo(c.nr_seq_tipo),1,40)
        into ds_tratamento_w
        from rxt_agenda a, rxt_tratamento b, rxt_tumor c
        where a.nr_seq_tratamento = b.nr_sequencia
        and b.nr_seq_tumor = c.nr_sequencia 
        and  a.nr_sequencia = nr_sequencia_p; 

    end if;

    return ds_tratamento_w;
end obter_desc_tratamento_agenda;
/