create or replace
function hd_obter_unid_escala_dia(nr_seq_escala_dia_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(80);			
			
begin
if	(nr_seq_escala_dia_p is not null) then
	select	max(substr(hd_obter_desc_unid_dialise(a.NR_SEQ_UNID_DIALISE),1,80))
	into	ds_retorno_w
	from	hd_escala_dialise a,
		hd_escala_dialise_dia b
	where	a.nr_sequencia = b.nr_seq_escala
	and	b.nr_sequencia = nr_seq_escala_dia_p;
end if;
return	ds_retorno_w;

end hd_obter_unid_escala_dia;
/