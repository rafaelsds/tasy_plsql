create or replace
function	qua_obter_os_revisao_pendente(
		nr_seq_documento_p	number)
			return number is

nr_retorno_w		number(10);

begin

select	nvl(max(nr_sequencia),0)
into	nr_retorno_w
from	man_ordem_servico
where	nr_seq_documento = nr_seq_documento_p
and	ie_tipo_ordem = 12
and	ie_status_ordem <> '3';

return	nr_retorno_w;

end qua_obter_os_revisao_pendente;
/