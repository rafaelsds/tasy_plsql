create or replace
function fis_check_fiscal_xml_data(	nr_seq_project_p	number,
									ie_type_content_p	varchar2,
									query_arguments_p 	varchar2) 
									return varchar2 is

/*
	Type Content	Query Arguments
	R				DS_DESTINATARIO
	T				IE_TIPO_TRANSMISSAO
*/

record_amount_w	number(10) := 0;
response_w		varchar2(1) := 'N';
sql_w			varchar(1000);
arguments_w		varchar(255);

begin

	if ie_type_content_p = 'R' then
		arguments_w := 'ds_destinatario like ' || chr(39) || '%' || query_arguments_p || '%' || chr(39);
	elsif ie_type_content_p = 'T' then
		arguments_w := 'ie_tipo_transmissao = ' || query_arguments_p;
	end if;
	
	sql_w := 'select count(*) from fis_projeto_xml where nr_seq_projeto = ' || nr_seq_project_p || ' and ' || arguments_w;
	
	EXECUTE IMMEDIATE sql_w INTO record_amount_w;
	
	if (record_amount_w > 0) then
		response_w := 'S';
	end if;
	
	return response_w;
	
end fis_check_fiscal_xml_data;
/
