create or replace function lab_obter_sequencia_amostra (nr_prescricao_p		number,
						nr_seq_material_p	number)
						return varchar2 is

nr_sequencia_w	number(10);
ds_retorno_w	varchar2(255);


begin


select	max(a.nr_sequencia)
into	nr_sequencia_w
from 	prescr_proc_material a,
	material_exame_lab b
where 	a.nr_prescricao 				= nr_prescricao_p
and	a.nr_Seq_material				= b.nr_sequencia
and	a.nr_seq_material				= nr_seq_material_p;


ds_retorno_w := nr_sequencia_w;

return ds_retorno_w;

end;
/