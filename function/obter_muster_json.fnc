create or replace function obter_muster_json(nr_sequencia_p	number)
 		    	return clob is
begin

	return telematic_json_pck.get_muster_information_clob(nr_sequencia_p);

end obter_muster_json;
 /