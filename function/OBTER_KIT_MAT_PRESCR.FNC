CREATE OR REPLACE
FUNCTION Obter_Kit_Mat_Prescr(	cd_material_p		number,
				cd_estabelecimento_p	number,
				ie_tipo_atendimento_p	number,
				cd_setor_Atendimento_p	number)
 		    	RETURN number IS

Cursor C01 is
	select	cd_kit
	from	kit_mat_prescricao
	where	cd_material		= cd_material_p
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_p)	= ie_tipo_atendimento_p
	and	nvl(cd_setor_Atendimento,cd_setor_Atendimento_p)= cd_setor_Atendimento_p	
	and	obter_setor_excluir_kit(nr_sequencia,cd_setor_Atendimento_p) = 'N'
	order by 	nvl(ie_tipo_atendimento,0),
			nvl(cd_setor_Atendimento,0);
			
cd_kit_material_w	number(10);
BEGIN

open C01;
loop
fetch C01 into	
	cd_kit_material_w;	
exit when C01%notfound;
end loop;
close C01;

return	nvl(cd_kit_material_w,obter_mat_estabelecimento(cd_estabelecimento_p,0,cd_material_p,'KT'));

END Obter_Kit_Mat_Prescr;
/