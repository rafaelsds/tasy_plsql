create or replace
function obter_desc_perfil_adicional(
		ds_codigo_p	varchar2)
		return varchar2 is

lista_perfis_w		lista_varchar_pck.tabela_varchar;
		
ds_codigo_w	varchar2(4000);
ds_retorno_w	varchar2(32000);
ds_perfil_w	varchar2(40);

begin

ds_codigo_w	:= ds_codigo_p;

if	(ds_codigo_w is not null) then
	begin
	
	lista_perfis_w := obter_lista_string2(ds_codigo_w, ',');
	
	for	i in lista_perfis_w.first..lista_perfis_w.last loop						 						
			begin
			select	obter_desc_perfil(to_number(lista_perfis_w(i)))
			into	ds_perfil_w
			from	dual;

			ds_retorno_w	:= ds_perfil_w ||', '||ds_retorno_w;
			exception
			when others then
				ds_retorno_w := ds_retorno_w; 	
			end;
	end loop;
	end;
end if;	
			
return substr(ds_retorno_w,1,4000);
end obter_desc_perfil_adicional;
/