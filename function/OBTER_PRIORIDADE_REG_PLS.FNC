CREATE OR REPLACE
FUNCTION OBTER_PRIORIDADE_REG_PLS (  NR_SEQUENCIA_P	NUMBER,
									 cd_procedimento_p	number default null,
									 ie_opcao_p		VARCHAR2 default 'A')
				RETURN VARCHAR2 IS

ds_retorno_w		varchar2(255);
nr_Seq_requisicao_w	number(10);
NR_SEQ_PRIORIDADE_w	number(10);

BEGIN
if	(NR_SEQUENCIA_P IS NOT NULL) THEN
	
	if (ie_opcao_p = 'A') then
	
		Select  max(nr_Seq_requisicao)
		into	nr_Seq_requisicao_w
		from	pls_auditoria
		where	nr_sequencia = NR_SEQUENCIA_P;
		
		if ( nr_Seq_requisicao_w > 0) then
		
			Select  max(a.NR_SEQ_PRIORIDADE)
			into	nr_seq_prioridade_w
			from	pls_requisicao_proc a,
					pls_requisicao b
			where	a.nr_Seq_requisicao = b.nr_sequencia
			and		b.nr_sequencia = nr_Seq_requisicao_w
			and		a.cd_procedimento = cd_procedimento_p;
		
			if (nr_seq_prioridade_w is not null) then
			
				Select  obter_prioridade_regulacao(nr_seq_prioridade_w)
				into	ds_retorno_w
				from    dual; 
			
			end if;
		
		end if;
	
	end if;
	
	
end if;

RETURN	ds_retorno_w;

END OBTER_PRIORIDADE_REG_PLS;
/