create or replace
procedure intdisp_suspender_horario(nr_atendimento_p	number,
									nr_prescricao_p		number,
									ie_tipo_evento_p	varchar2,
									nm_usuario_p		varchar2) is 

cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
ie_gerar_kit_w			setor_atendimento.ie_gerar_kit%type;
qt_existe_regra_w		number(10);

begin

if	(ie_tipo_evento_p = 'M') then

	cd_setor_atendimento_w := obter_setor_atendimento(nr_atendimento_p);

	select	nvl(max(ie_gerar_kit), 'S')
	into	ie_gerar_kit_w
	from	setor_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_w;
	
	if	(ie_gerar_kit_w = 'N') then
		
		update	prescr_mat_hor a
		set		dt_suspensao = sysdate,
			nm_usuario_susp = nm_usuario_p
		where	a.nr_seq_superior is not null
		and		a.nr_prescricao = nr_prescricao_p
		and	((a.nr_seq_processo is null) or exists(	select	1 
											from	adep_processo x
											where	x.nr_sequencia = a.nr_seq_processo
											and		x.ie_status_processo = 'G'))
		and	not exists (select	1
						from	prescr_material y
						where	y.nr_prescricao = a.nr_prescricao
						and		y.nr_sequencia = a.nr_seq_material
						and		y.nr_prescricao = nr_prescricao_p
						and		y.nr_sequencia_diluicao is not null)
		and	not exists (select	1 
						from	ap_lote b
						where	b.nr_sequencia = a.nr_sequencia
						and		b.nr_prescricao = nr_prescricao_p
						and		b.ie_status_lote = 'G');
	end if;

end if;

end intdisp_suspender_horario;
/