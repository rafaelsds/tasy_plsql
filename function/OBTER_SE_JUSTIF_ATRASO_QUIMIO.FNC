create or replace
function obter_se_justif_atraso_quimio		(nr_seq_atendimento_p		number)
						return varchar2 is
						
ds_retorno_w		varchar2(1) := 'N';	
ie_dia_seguinte_w		varchar2(1) := null;	
dt_liberacao_w		date;
dt_real_w		date;
nr_seq_regra_w	number(10);						
cd_estabelecimento_w	number(10);

cursor c01 is
select 	nr_sequencia	
from 	regra_justif_atraso
where 	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
order by 	nvl(cd_estabelecimento, 0);

begin

if (nvl(nr_seq_atendimento_p,0) > 0) then
	
	select  max(dt_liberacao),
			max(dt_real),
			max(cd_estabelecimento)
	into    dt_liberacao_w,
		dt_real_w,
		cd_estabelecimento_w
	from 	paciente_atendimento
	where 	nr_seq_atendimento = nr_seq_atendimento_p;
    
	open c01;
	loop
	fetch c01 into
		nr_seq_regra_w;
	exit when c01%notFOUND;	
	end loop;
	close c01;

	if (nvl(nr_seq_regra_w,0) > 0)  then
	
		if 	(trunc(dt_real_w) = trunc(sysdate)) then		
			ie_dia_seguinte_w := 'N';
		elsif  (trunc(dt_real_w) > trunc(sysdate)) then			
			ie_dia_seguinte_w := 'S';
		end if;
		
		if (ie_dia_seguinte_w is not null) then
			select  nvl(max('N'),'S')
			into	ds_retorno_w
			from    regra_justif_atraso
			where   to_char(dt_liberacao_w,'hh24:mi') between to_char(hr_inicio_liberacao,'hh24:mi') and to_char(HR_FIM_LIBERACAO,'hh24:mi')
			and		to_char(dt_real_w,'hh24:mi') between to_char(hr_inicio_tratamento,'hh24:mi') and to_char(hr_fim_tratamento,'hh24:mi')
			and		nvl(ie_dia_seguinte,'N') = ie_dia_seguinte_w
			and 	nr_sequencia = nr_seq_regra_w;
		end if;
		
	end if;
end if;

return	ds_retorno_w;

end obter_se_justif_atraso_quimio;
/