create or replace
function obter_hr_inicio_cirurgia(nr_cirurgia_p number)
 		    	return date is

hr_inicio_w date;

begin
if (nr_cirurgia_p > 0) then
	begin
	select	max(hr_inicio)
	into	hr_inicio_w
	from	agenda_paciente
	where	nr_cirurgia = nr_cirurgia_p
	and	ie_status_agenda <> 'C';
	end;
end if;

return hr_inicio_w;

end obter_hr_inicio_cirurgia;
/
