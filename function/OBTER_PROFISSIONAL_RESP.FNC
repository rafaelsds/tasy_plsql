create or replace 
function obter_profissional_resp(	nr_atendimento_p	number,
					ie_profissional_p	varchar2 )
					return varchar2 is 

cd_pessoa_fisica_w	varchar2(10);

begin

/*
E - Enfermeiro
F - Farmac�utico
M - M�dico
N - Nutricionista
P - Psic�logo
*/

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atend_profissional
where	nr_atendimento		=	nr_atendimento_p
and	ie_profissional		=	ie_profissional_p
and	dt_inicio_vigencia	=	(select	max(dt_inicio_vigencia)
					from	atend_profissional
					where	nr_atendimento		=	nr_atendimento_p
					and	ie_profissional		=	ie_profissional_p);

return	cd_pessoa_fisica_w;

end obter_profissional_resp;
/