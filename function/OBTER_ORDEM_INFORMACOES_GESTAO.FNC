create or replace
function obter_ordem_informacoes_gestao (
		ie_tipo_item_p			varchar2,
		ie_tipo_registro_p		varchar2,
		ie_classif_proced_gestao_p	varchar2)
		return number is
		
nr_seq_ordem_w	number(10,0);
		
begin
if	(ie_tipo_item_p = 'J') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 10;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 20;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p in ('D','DE')) then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 30;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 40;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'S') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 50;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 60;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'SOL') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 70;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 80;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'SNE') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 90;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 100;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'NAN') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 110;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 120;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'NPA') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 130;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 140;
		end;
	end if;	
	end;	
elsif	(ie_tipo_item_p = 'NPN') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 150;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 160;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'HM') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 170;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 180;
		end;
	end if;	
	end;		
elsif	(ie_tipo_item_p = 'M') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 190;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 200;
		end;
	end if;
	end;
elsif	(ie_tipo_item_p = 'MAT') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 210;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 220;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'P') then
	begin
	if	(ie_classif_proced_gestao_p = 'P') then
		begin
		if	(ie_tipo_registro_p = 'T') then
			begin
			nr_seq_ordem_w	:= 230;
			end;
		elsif	(ie_tipo_registro_p = 'R') then
			begin
			nr_seq_ordem_w	:= 240;
			end;
		end if;		
		end;
	elsif	(ie_classif_proced_gestao_p = 'SADT') then
		begin
		if	(ie_tipo_registro_p = 'T') then
			begin
			nr_seq_ordem_w	:= 250;
			end;
		elsif	(ie_tipo_registro_p = 'R') then
			begin
			nr_seq_ordem_w	:= 260;
			end;
		end if;		
		end;
	end if;
	end;
elsif	(ie_tipo_item_p = 'IA') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 270;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 280;
		end;
	end if;
	end;	
elsif	(ie_tipo_item_p in ('C','G')) then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 290;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 300;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'IAG') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 310;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 320;
		end;
	end if;	
	end;
elsif	(ie_tipo_item_p = 'I') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 330;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 340;
		end;
	end if;	
	end;		
elsif	(ie_tipo_item_p = 'L') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 350;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 360;
		end;
	end if;		
	end;
elsif	(ie_tipo_item_p = 'O') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 370;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 380;
		end;
	end if;		
	end;	
elsif	(ie_tipo_item_p = 'R') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 390;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 400;
		end;
	end if;		
	end;
elsif	(ie_tipo_item_p = 'B') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 410;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 420;
		end;
	end if;		
	end;
elsif	(ie_tipo_item_p = 'E') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 430;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 440;
		end;
	end if;		
	end;	
elsif	(ie_tipo_item_p = 'DI') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 450;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 460;
		end;
	end if;		
	end;
elsif	(ie_tipo_item_p = 'LD') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 470;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 480;
		end;
	end if;		
	end;	
elsif	(ie_tipo_item_p in('ME','MD')) then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 490;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 500;
		end;
	end if;		
	end;	
elsif	(ie_tipo_item_p = 'NPP') then
	begin
	if	(ie_tipo_registro_p = 'T') then
		begin
		nr_seq_ordem_w	:= 500;
		end;
	elsif	(ie_tipo_registro_p = 'R') then
		begin
		nr_seq_ordem_w	:= 510;
		end;
	end if;		
	end;	
end if;
return nr_seq_ordem_w;
end obter_ordem_informacoes_gestao;
/
