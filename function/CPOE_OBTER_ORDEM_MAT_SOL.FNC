create or replace 
function cpoe_obter_ordem_mat_sol(
					cd_material_p				cpoe_material.cd_material%type,
					ie_controle_tempo_p			cpoe_material.ie_controle_tempo%type,
					ie_tipo_solucao_p			cpoe_material.ie_tipo_solucao%type,
					ie_via_aplicacao_p			cpoe_material.ie_via_aplicacao%type,
					cd_estabelecimento_p		number,
					cd_perfil_p					number,
					cd_setor_usuario_p			number,
					nm_usuario_p				varchar2,
					ie_funcao_p 				varchar2 default 'C',
					cd_mat_comp1_p				number default null,
					cd_mat_comp2_p				number default null,
					cd_mat_comp3_p				number default null,
					cd_mat_comp4_p				number default null,
					cd_mat_comp5_p				number default null,
					cd_mat_comp6_p				number default null,
					cd_mat_comp7_p				number default null) return number is

cd_material_w 					number(15);					
cd_grupo_material_w				number(15);
cd_subgrupo_material_w			number(15);
cd_classe_material_w			number(15);
					
ie_subgrupo_w				cpoe_regra_ordem_subgrupo.ie_subgrupo%type;
nr_seq_ordem_subgrupo_w		cpoe_regra_ordem_subgrupo.nr_sequencia%type;

nr_seq_apresentacao_w		cpoe_regra_ordem_itens.nr_seq_apresentacao%type;
nr_seq_apresent_aux_w		cpoe_regra_ordem_itens.nr_seq_apresentacao%type;

cursor c01 is 
select	c.nr_sequencia
from	cpoe_regra_ordem_lib_v a,
		cpoe_regra_ordem_grupo b,
		cpoe_regra_ordem_subgrupo c
where	a.nr_seq_regra_ordem = b.nr_seq_regra_ordem
and		c.nr_seq_ordem_grupo = b.nr_sequencia
and		b.ie_grupo = 'M'
and		c.ie_subgrupo = ie_subgrupo_w
and	((a.cd_estabelecimento is null) or (cd_estabelecimento_p = a.cd_estabelecimento))
and	((a.cd_setor_atendimento is null) or (cd_setor_usuario_p = a.cd_setor_atendimento))
and	((a.nm_usuario_regra is null) or (nm_usuario_p = a.nm_usuario_regra))
and	((a.cd_perfil is null) or (cd_perfil_p = a.cd_perfil))
and		nvl(a.ie_situacao,'A') = 'A'
and (a.ie_funcao_cpoe = 'A' or a.ie_funcao_cpoe = nvl(ie_funcao_p, 'C'))
order by	nvl(a.nm_usuario_regra,'') desc,
			nvl(a.cd_setor_atendimento,0),
			nvl(a.cd_perfil,0),
			nvl(a.cd_estabelecimento,0);
			
cursor c02 is 
select	nr_seq_apresentacao
from	cpoe_regra_ordem_itens
where	nr_seq_ordem_subgrupo = nr_seq_ordem_subgrupo_w
and		nvl(cd_material,cd_material_w) = cd_material_w
and		nvl(cd_grupo_material,cd_grupo_material_w) = cd_grupo_material_w
and		nvl(cd_subgrupo_material,cd_subgrupo_material_w) = cd_subgrupo_material_w
and		nvl(cd_classe_material,cd_classe_material_w) = cd_classe_material_w
and	    nvl(ie_tipo_solucao, nvl(ie_tipo_solucao_p,'XPTO')) = nvl(ie_tipo_solucao_p,'XPTO')
order	by 
		nvl(cd_material,0),
		nvl(cd_grupo_material,0),	
		nvl(cd_subgrupo_material,0),
		nvl(cd_classe_material,0),
		nvl(ie_tipo_solucao,'');

cursor c03 is 
select	nr_seq_apresentacao
from	cpoe_regra_ordem_itens
where	nr_seq_ordem_subgrupo = nr_seq_ordem_subgrupo_w
and		nvl(cd_material,cd_material_p) = cd_material_p
and		nvl(cd_grupo_material,cd_grupo_material_w) = cd_grupo_material_w
and		nvl(cd_subgrupo_material,cd_subgrupo_material_w) = cd_subgrupo_material_w
and		nvl(cd_classe_material,cd_classe_material_w) = cd_classe_material_w
and		nvl(ie_via_aplicacao, nvl(ie_via_aplicacao_p,'XPTO')) = nvl(ie_via_aplicacao_p,'XPTO')
order	by 
		nvl(cd_material,0),
		nvl(cd_grupo_material,0),	
		nvl(cd_subgrupo_material,0),
		nvl(cd_classe_material,0),
		nvl(ie_via_aplicacao,'');
		
cursor c04 is
select nvl(cd_material,0),
	   nvl(cd_grupo_material,0),
	   nvl(cd_subgrupo_material,0),
	   nvl(cd_classe_material,0)
from estrutura_material_v
where cd_material in (cd_material_p, cd_mat_comp1_p, cd_mat_comp2_p, cd_mat_comp3_p,
                      cd_mat_comp4_p, cd_mat_comp5_p, cd_mat_comp6_p, cd_mat_comp7_p);
					  
begin

nr_seq_apresentacao_w := 9999;

if (ie_controle_tempo_p = 'S') then
	ie_subgrupo_w := 'SOL';
else
	ie_subgrupo_w := 'M';
end if;

open c01;
loop
fetch c01 into nr_seq_ordem_subgrupo_w;
exit when c01%notfound;
end loop;
close c01;

if (ie_subgrupo_w = 'SOL') then 
	
	open c04;
	loop
	fetch c04 into 	cd_material_w,
					cd_grupo_material_w,
					cd_subgrupo_material_w,
					cd_classe_material_w;
	exit when c04%notfound;

		open c02;
		loop
		fetch c02 into nr_seq_apresent_aux_w;
		exit when c02%notfound;

		if	(nvl(nr_seq_apresent_aux_w,9999) < nr_seq_apresentacao_w) then
			nr_seq_apresentacao_w := nr_seq_apresent_aux_w;
		end if;

		end loop;
		close c02;

	end loop;
	close c04;
	
elsif (ie_subgrupo_w = 'M') then
	
	select	nvl(max(b.cd_grupo_material),0),
			nvl(max(b.cd_subgrupo_material),0),
			nvl(max(b.cd_classe_material),0)
	into	cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w
	from	estrutura_material_v b
	where 	b.cd_material = cd_material_p;	
	
	open c03;
	loop
	fetch c03 into nr_seq_apresentacao_w;
	exit when c03%notfound;
	end loop;
	close c03;
	
end if;

return nr_seq_apresentacao_w;

end cpoe_obter_ordem_mat_sol;
/