create or replace
function hjf_obter_serie_documento(	nr_seq_protocolo_p	number,
					nr_atendimento_p	NUmber)
					return varchar2 is

cd_serie_documento_w		varchar2(4);
ie_tipo_atendimento_w		number(1,0);
dt_entrega_convenio_w		Date;

begin

cd_serie_documento_w:= 'JF';

select 	max(ie_tipo_atendimento)
into	ie_tipo_atendimento_w
from 	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

if	(ie_tipo_atendimento_w = 1) then
	
	cd_serie_documento_w:= cd_serie_documento_w || 'I';

else

	cd_serie_documento_w:= cd_serie_documento_w || 'E';

end if;


select	nvl(max(dt_entrega_convenio),sysdate)
into	dt_entrega_convenio_w
from 	protocolo_convenio
where 	nr_seq_protocolo = nr_seq_protocolo_p;


if	((to_number(to_char(dt_entrega_convenio_w,'dd')) >= 16) and
	(to_number(to_char(dt_entrega_convenio_w,'dd')) <= 31)) then
	
	cd_serie_documento_w:= cd_serie_documento_w || '1';

else
	cd_serie_documento_w:= cd_serie_documento_w || '2';

end if;

return cd_serie_documento_w;

end hjf_obter_serie_documento;
/