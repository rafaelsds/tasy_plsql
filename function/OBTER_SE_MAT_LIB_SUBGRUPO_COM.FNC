create or replace
function obter_se_mat_lib_subgrupo_com(	nr_seq_subgrupo_compras_p	number,
					cd_material_p			number)
return varchar2 is

cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
ie_liberado_w				varchar2(1) := 'N';


cursor c01 is
select	nvl(a.ie_liberado, 'N')
from	sup_estrut_compras a,
	sup_subgrupo_compra b,
	sup_grupo_compra c
where	a.nr_seq_subgrupo_compra = b.nr_sequencia
and	b.nr_seq_grupo_compra = c.nr_sequencia
and	b.nr_sequencia = nr_seq_subgrupo_compras_p
and	nvl(a.cd_grupo_material,cd_grupo_material_w) 		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material,cd_classe_material_w) 		= cd_classe_material_w
and	nvl(a.cd_material,cd_material_p) 			= cd_material_p
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(b.ie_situacao,'A') = 'A'
order by	nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0);

begin

select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v
where	cd_material = cd_material_p;

open C01;
loop
fetch C01 into	
	ie_liberado_w;
exit when C01%notfound;
	begin
	ie_liberado_w := ie_liberado_w;
	end;
end loop;
close C01;

return ie_liberado_w;

end obter_se_mat_lib_subgrupo_com;
/
