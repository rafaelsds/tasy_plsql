create or replace
function obter_se_alerta_pf_pac (cd_pessoa_fisica_p    varchar2,
				 cd_paciente_p          varchar2,
                                 nr_Atendimento_p        number)
                return varchar2 is

ie_alerta_w    varchar2(1) := 'N';

begin
if    (cd_pessoa_fisica_p is not null) then

    select    decode(count(*),0,'N','S')
    into    ie_alerta_w
    from    pf_alerta
    where    cd_pessoa_fisica = cd_pessoa_fisica_p
    and     cd_paciente = cd_paciente_p
    and     nr_atendimento = nr_atendimento_p
    and    dt_liberacao is not null
    and    dt_ciencia is null
    and    sysdate between nvl(DT_INICIO,sysdate) and nvl(DT_FIM,sysdate);

end if;

return ie_alerta_w;

end obter_se_alerta_pf_pac;
/