CREATE OR REPLACE
Function	Obter_Dados_Ultimo_Atend(	cd_pessoa_fisica_p	VarChar2,
							nr_atendimento_p		Number,
							ie_tipo_atend_p		Number,
							ie_opcao_p			VarChar2)
					Return Varchar2 IS

/* ie_opcao_p - NA - n� atendimento
		  - DA - data de alta
		  - DE - data de entrada
		  - TA - tipo de atendimento
		  - ME - M�dico Resp
		  - CO - Conv�nio
		  -DAF - data ultimo atendimento (ficha financeira).
*/

nr_atendimento_w		number(10);
ds_retorno_w		varchar2(255) := '';

BEGIN

if	(nr_atendimento_p is not null) then
	begin
	nr_atendimento_w	:= null;
	if (cd_pessoa_fisica_p is not null) then
		select 	max(nr_atendimento)
		into	nr_atendimento_w
		from 	atendimento_paciente
		where 	nr_atendimento		< nr_atendimento_p
		and	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and 	ie_tipo_atendimento	= nvl(ie_tipo_atend_p, ie_tipo_atendimento);
	end if;
	end;
elsif (cd_pessoa_fisica_p is not null) then
	begin
	if	(ie_tipo_atend_p is null) then
		select nvl(max(nr_atendimento),null)
		into	nr_atendimento_w
		from atendimento_paciente
		where cd_pessoa_fisica		= cd_pessoa_fisica_p;
	else
		select nvl(max(nr_atendimento),null)
		into	nr_atendimento_w
		from atendimento_paciente
		where cd_pessoa_fisica		= cd_pessoa_fisica_p
		and ie_tipo_atendimento	= nvl(ie_tipo_atend_p, ie_tipo_atendimento);
	end if;
	end;
end if;

if	(nr_atendimento_w is not null) then
	if	(ie_opcao_p = 'NA') then
		ds_retorno_w	:= nr_atendimento_w;
	elsif	(ie_opcao_p = 'DA') then
		select to_char(dt_alta, 'dd/mm/yyyy hh24:mi:ss')
		into ds_retorno_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_w;
	elsif	(ie_opcao_p = 'DE') then
		select to_char(dt_entrada, 'dd/mm/yyyy hh24:mi:ss')
		into ds_retorno_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_w;
	elsif	(ie_opcao_p = 'TA') then
		select ie_tipo_atendimento
		into ds_retorno_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_w;
	elsif	(ie_opcao_p = 'ME') then
		select obter_nome_pf(cd_medico_resp)
		into ds_retorno_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_w;
	elsif	(ie_opcao_p = 'CO') then
		select ds_convenio
		into ds_retorno_w
		from atendimento_paciente_v
		where nr_atendimento = nr_atendimento_w;
	elsif	(ie_opcao_p = 'CL') then
		select	substr(obter_valor_dominio(17, ie_clinica),1,255)
		into	ds_retorno_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_w;
	elsif	(ie_opcao_p = 'DAF') then
		select max(nvl(to_char(dt_alta, 'dd/mm/yyyy hh24:mi:ss'), wheb_mensagem_pck.get_texto(301738)))
		into ds_retorno_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_w;
	end if;
end if;

RETURN ds_retorno_w;

END Obter_Dados_Ultimo_Atend;
/