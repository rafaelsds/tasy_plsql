create or replace
function obter_soma_valores_guia_grc(
			nr_interno_conta_p			Number) 
		return number as
vl_saldo		Number(13,4);

begin
select	nvl(sum(obter_valores_guia_grc(x.nr_Seq_lote_hist,x.nr_interno_conta,x.cd_autorizacao,'VP')),0)  
   						+ nvl(sum(obter_valores_guia_grc(x.nr_Seq_lote_hist,x.nr_interno_conta,x.cd_autorizacao,'VG')),0)  
into  vl_saldo
from	lote_audit_hist_guia x  
where	x.nr_interno_conta = nr_interno_conta_p;

return vl_saldo;

end obter_soma_valores_guia_grc;
/