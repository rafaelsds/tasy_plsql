create or replace
function paf_obter_chave(cd_estabelecimento_p	number,
			 ie_tipo_p		varchar2)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(2000);
			
begin

select	decode(ie_tipo_p,'C',cd_chave_publica,cd_chave_privada)
into	ds_retorno_w
from	paf_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

return	ds_retorno_w;

end paf_obter_chave;
/