create or replace
function sus_obter_procconta_rateio_aih
	(	nr_sequencia_p	number)
		return varchar2 is 


vl_medico_w		number(15,2)	:= 0;
qt_ponto_medico_w	number(10,0)	:= 0;
qt_ponto_anest_w	number(10,0)	:= 0;
ds_retorno_w		varchar2(1)	:= 'N';
cd_procedimento_w	number(15);
qt_reg_proc_w		number(10,0)	:= 0;
cd_registro_proc_w	sus_valor_proc_paciente.cd_registro_proc%type := 0;

BEGIN

begin
select	nvl(b.vl_medico,0),
	nvl(b.qt_ato_medico,0),
	nvl(b.qt_ato_anestesista,0),
	nvl(a.cd_procedimento,0),
	nvl(b.cd_registro_proc,0)
into	vl_medico_w,
	qt_ponto_medico_w,
	qt_ponto_anest_w,
	cd_procedimento_w,
	cd_registro_proc_w
from	sus_valor_proc_paciente	b,
	procedimento_paciente	a
where	a.nr_sequencia		= b.nr_sequencia
and	a.nr_sequencia		= nr_sequencia_p
and	a.ie_origem_proced	= 7
and	a.cd_motivo_exc_conta is null;
exception
	when others then
	vl_medico_w		:= 0;
	qt_ponto_medico_w	:= 0;
	qt_ponto_anest_w	:= 0;
	cd_registro_proc_w	:= 0;
end;

if	((qt_ponto_medico_w > 0) or (qt_ponto_anest_w > 0) or (cd_procedimento_w in (802010199))) then
	ds_retorno_w	:= 'S';
end if;

if	(ds_retorno_w = 'S') and
	(cd_registro_proc_w <> 3) then
	begin
	
	select	count(1)
	into	qt_reg_proc_w
	from	sus_procedimento_registro
	where	cd_procedimento = cd_procedimento_w
	and	ie_origem_proced = 7
	and	cd_registro in (3,4);
	
	if	(qt_reg_proc_w > 1) then
		ds_retorno_w	:= 'N';
	end if;
	end;
end if;

return	ds_retorno_w;

end sus_obter_procconta_rateio_aih;
/
