create or replace function mx_obter_obs_item(	nr_sequencia_p in  number) return varchar2 is 

ds_observacao_w 	nota_fiscal_item.ds_observacao%type;
ds_nr_registro_w	varchar2(100);
ds_nr_pedido_w		varchar2(100);		

begin
	
select	max(reg.nr_registro),
		max(adu.nr_pedido)
into	ds_nr_registro_w,
		ds_nr_pedido_w
from	nf_imp_nota_reg_imob reg,
		nf_imp_nota_item_reg_aduan adu,
		nf_imp_nota_item nfi
where	nfi.nr_sequencia = nr_sequencia_p
and		reg.nr_seq_nf_imp_nota_item = nfi.nr_sequencia
and		adu.nr_seq_nf_imp_nota_item = nfi.nr_sequencia;

if (ds_nr_registro_w is not null) then
	ds_observacao_w := obter_desc_expressao(297441) || ': ' || ds_nr_registro_w;
end if;

if (ds_nr_pedido_w is not null) then

	if (ds_observacao_w is null) then
		ds_observacao_w := obter_desc_expressao(295394) || ': ' || ds_nr_pedido_w;
	else
		ds_observacao_w := ds_observacao_w || ' ' || obter_desc_expressao(295394) || ': ' || ds_nr_pedido_w;
	end if;
end if;

return ds_observacao_w;

end mx_obter_obs_item;
/