create or replace
function HD_Obter_Se_Priming_Ideal (	nr_seq_modelo_p		number,
					qt_priming_p		number)
					return			varchar2 is

qt_priming_fabrica_w		number(10,2);
pr_variacao_w			number(6,3);
qt_priming_menor_w		number(10,2);
qt_priming_maior_w		number(10,2);
ie_retorno_w			varchar2(1);

begin

/* Pega os dados do modelo do dialisador */
select	qt_priming_fabrica,
	pr_variacao
into	qt_priming_fabrica_w,
	pr_variacao_w
from	hd_modelo_dialisador
where	nr_sequencia	= nr_seq_modelo_p;

qt_priming_menor_w	:= qt_priming_fabrica_w - (qt_priming_fabrica_w * pr_variacao_w / 100);
qt_priming_maior_w	:= qt_priming_fabrica_w + (qt_priming_fabrica_w * pr_variacao_w / 100);

if	(qt_priming_p >= qt_priming_menor_w) and 
	(qt_priming_p <= qt_priming_maior_w) then
	ie_retorno_w	:= 'S';
else
	ie_retorno_w	:= 'N';
end if;

return ie_retorno_w;

end HD_Obter_Se_Priming_Ideal;
/