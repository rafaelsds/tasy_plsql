create or replace
function	obter_inconsist_incl_benef_js(nr_seq_solic_inclusao_p	number)
	return number is

qt_existe_w	number(10);

begin

select	count(*)
into	qt_existe_w
from	pls_inconsist_incl_benef
where	nr_seq_solic_inclusao = nr_seq_solic_inclusao_p;

return	qt_existe_w;

end obter_inconsist_incl_benef_js;
/