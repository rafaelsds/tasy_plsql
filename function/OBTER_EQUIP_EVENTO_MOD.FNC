create or replace
function Obter_equip_evento_mod (nr_seq_equip_p	number)
				return varchar2 is


ds_retorno_w	varchar2(255);

begin

select 	max(ds_equipamento)
into	ds_retorno_w
from 	tre_equip
where 	nr_sequencia = nr_seq_equip_p;

return ds_retorno_w;

end Obter_equip_evento_mod;
/