create or replace
function Sus_Obter_Ordem_Ident_AIH(ie_identificacao_p		Varchar2)
 		    	return number is

vl_retorno_w	Number(1);			

begin

if	(ie_identificacao_p = '01') then
	vl_retorno_w := 1;
elsif	(ie_identificacao_p = '05') then
	vl_retorno_w := 2;
elsif	(ie_identificacao_p = '03') then
	vl_retorno_w := 3;
elsif	(ie_identificacao_p = '04') then
	vl_retorno_w := 4;
elsif	(ie_identificacao_p = '07') then
	vl_retorno_w := 5;
end if;

return	vl_retorno_w;

end Sus_Obter_Ordem_Ident_AIH;
/