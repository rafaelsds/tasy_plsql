create or replace
function obter_dt_execucao_exames(nr_laudo_p number) return varchar2 is

ds_retorno_w varchar2(50);

begin
	if (nr_laudo_p is not null) then  

		select 	to_char(max(proc_pac.dt_procedimento),'dd/mm/yyyy')
		into    ds_retorno_w
		from	prescr_procedimento prescr_proc, 
			procedimento_paciente proc_pac
		where	prescr_proc.nr_prescricao = proc_pac.nr_prescricao (+)
		and	prescr_proc.nr_sequencia  = proc_pac.nr_sequencia_prescricao (+)
		and	proc_pac.nr_laudo = nr_laudo_p;
	end if;	

  
return ds_retorno_w;

end obter_dt_execucao_exames;
/



