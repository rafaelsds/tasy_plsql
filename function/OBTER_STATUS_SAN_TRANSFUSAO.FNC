create or replace function obter_status_san_transfusao(nr_seq_transfusao_p  number,
                                    nr_seq_reserva_hemo_p number default 0)
                                    return varchar2 is
      
ie_status_w             varchar2(10);
nr_seq_transfusao_w     SAN_TRANSFUSAO.nr_sequencia%type;

begin

if(nr_seq_transfusao_p = 0 and nr_seq_reserva_hemo_p <> 0 )then
  
  select max(nr_sequencia)
    into nr_seq_transfusao_w   
  from san_transfusao
  where nr_seq_reserva = nr_seq_reserva_hemo_p;
  
end if;

if(nr_seq_transfusao_p  is not null or nr_seq_transfusao_w is not null) then
  
  select  max(ie_status)
    into  ie_status_w
  from  san_transfusao
  where  (nr_sequencia = nr_seq_transfusao_p or nr_sequencia = nr_seq_transfusao_w);
    
end if;

return  ie_status_w;

end obter_status_san_transfusao;
/
