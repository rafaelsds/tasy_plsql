create or replace
function sus_obter_dados_proc_generico(	nr_sequencia_p       	number,
					cd_procedimento_p		number,
					ie_opcao_p		varchar2)
					return varchar2 is

cd_procedimento_w	number(15)	:= 0;
ds_procedimento_w		varchar2(255) 	:= '';
nr_sequencia_w		number(10)	:= 0;
ds_retorno_w		varchar2(255)	:= '';

begin

if	(nr_sequencia_p is not null) then
	begin
	begin
	select	max(cd_procedimento),
		max(substr(ds_procedimento,1,255))
	into	cd_procedimento_w,
		ds_procedimento_w
	from	sus_procedimento_generico
	where	nr_sequencia	= nr_sequencia_p;
	exception
	when others then
		cd_procedimento_w := 0;
		ds_procedimento_w := '';
	end;
	end;
else
	begin
	begin
	select	max(nr_sequencia),
		max(substr(ds_procedimento,1,255))
	into	nr_sequencia_w,
		ds_procedimento_w
	from	sus_procedimento_generico
	where	cd_procedimento	= cd_procedimento_p;
	exception
	when others then
		nr_sequencia_w 		:= 0;
		ds_procedimento_w		:= '';
	end;
	end;
end if;
	
if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= cd_procedimento_w;
elsif 	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_procedimento_w;
elsif 	(ie_opcao_p = 'S') then
	ds_retorno_w	:= nr_sequencia_w;
end if;

return ds_retorno_w;

end sus_obter_dados_proc_generico;
/
