create or replace function OBTER_VALOR_CAMPO_TEXTO(ds_texto_p		varchar2,
							ds_campo_p	varchar2,
							ds_prox_campo_p	varchar2)
			return varchar2 is

ds_retorno_w		varchar2(100);

begin
	
	if ( ds_texto_p is null or ds_texto_p = '' ) then
		
		ds_retorno_w := '';
		
	elsif ( ds_campo_p is null or ds_campo_p = '' ) then
		
		ds_retorno_w := '';
		
	elsif ( ds_prox_campo_p is null or ds_prox_campo_p = '' ) then
		
		ds_retorno_w := '';
		
	else
		
		begin
			select	substr(ds_retorno, 0, instr(upper(ds_retorno),upper(ds_prox_campo_p)) - 1)
			into	ds_retorno_w
			from	(select	substr(ds_texto_p, (instr(upper(ds_texto_p),upper(ds_campo_p)) + length(ds_campo_p))) ds_retorno
				from	dual);
		end;
		
	end if;
	
	return nvl(ds_retorno_w,'');
	
end OBTER_VALOR_CAMPO_TEXTO;
/
