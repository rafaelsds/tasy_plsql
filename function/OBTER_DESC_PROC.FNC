CREATE OR REPLACE
FUNCTION Obter_Desc_Proc(
		cd_procedimento_p	Number)
	RETURN VARCHAR IS

ds_retorno_w		Varchar2(255);

BEGIN

if	( cd_procedimento_p is not null ) then
	select 	substr(ds_procedimento,1,255)
	into	ds_retorno_w
	from 	procedimento
	where 	cd_procedimento = cd_procedimento_p;
end if;

RETURN ds_retorno_w;

END Obter_Desc_Proc;
/
