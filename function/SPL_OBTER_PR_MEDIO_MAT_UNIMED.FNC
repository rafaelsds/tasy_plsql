create or replace
function spl_obter_pr_medio_mat_unimed(	cd_material_p		number,
			dt_vigencia_p		date,
			cd_convenio_P		number,
			cd_estabelecimento_p	number,
			ie_tipo_p		varchar2)
 		    	return number is
	
/*
Obejto utilizado em relat�rio do Belvedere.

Ie_tipo_p
UN - Gera a med�a do convenio UNIMED, desconsidera as caegorias que n�o possuem pre�o.
RC - Gera a m�dia para o material, desconsiderandos os convenio 17 e 31,  desconsidera as categorias que n�o possuem pre�o.
*/	
	
vl_retorno_w	number(15,2);
cd_convenio_w	number(5);
cd_categoria_w	varchar2(10);
vl_material_w	number(15,2):=0;
nr_count_w	number(10):=0;


Cursor C01 is
	select	a.cd_convenio,
		a.cd_categoria
	from	categoria_convenio a,
		convenio b
	where	a.cd_convenio	= b.cd_convenio
	and	((a.cd_convenio = cd_convenio_p) or (nvl(cd_convenio_p,0) = 0))
	and	a.cd_convenio = (31)
	and	obter_preco_material(cd_estabelecimento_p,a.cd_convenio,a.cd_categoria,dt_vigencia_p,cd_material_p,0,0,0,0,0,0) > 0
	and 	a.ie_situacao = 'A'
	and	b.ie_situacao = 'A';

Cursor C02 is	
	select	a.cd_convenio,
		a.cd_categoria
	from	categoria_convenio a,
		convenio b
	where	a.cd_convenio	= b.cd_convenio
	and	((a.cd_convenio = cd_convenio_p) or (nvl(cd_convenio_p,0) = 0))
	and 	a.ie_situacao = 'A'
	and	obter_preco_material(cd_estabelecimento_p,a.cd_convenio,a.cd_categoria,dt_vigencia_p,cd_material_p,0,0,0,0,0,0) > 0
	and	a.cd_convenio not in (17,31)
	and	b.ie_situacao = 'A';	
	
begin

	if (ie_tipo_p = 'UN') then
		begin
		open C01;
		loop
		fetch C01 into	
			cd_convenio_w,	
			cd_categoria_w;
		exit when C01%notfound;
			begin
			vl_material_w	:= vl_material_w + obter_preco_material(	cd_estabelecimento_p,
											cd_convenio_w, 
											cd_categoria_w, 
											dt_vigencia_p, 
											cd_material_p, 0, 0, 0, 0, 0, 0);
			nr_count_w:= nr_count_w + 1;
			end;
		end loop;
		close C01;
		end;
	elsif (ie_tipo_p = 'RC') then
		begin
		open C02;
		loop
		fetch C02 into	
			cd_convenio_w,	
			cd_categoria_w;
		exit when C02%notfound;
			begin
			vl_material_w	:= vl_material_w + obter_preco_material(	cd_estabelecimento_p,
											cd_convenio_w, 
											cd_categoria_w, 
											dt_vigencia_p, 
											cd_material_p, 0, 0, 0, 0, 0, 0);
			nr_count_w:= nr_count_w + 1;
			end;
		end loop;
		close C02;	
		end;
	end if;			
	
vl_retorno_w:= dividir(vl_material_w,nr_count_w);

return	vl_retorno_w;

end spl_obter_pr_medio_mat_unimed;
/
