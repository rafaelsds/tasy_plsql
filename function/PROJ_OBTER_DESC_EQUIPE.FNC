create or replace
function proj_obter_desc_equipe(nr_seq_equipe_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
ds_equipe_w		varchar2(255);

begin

select	substr(proj_obter_dados_equipe(nr_seq_equipe_funcao,'D'),1,100)
into	ds_equipe_w
from	proj_equipe
where	nr_sequencia	= nr_seq_equipe_p;

ds_retorno_w	:= ds_equipe_w;

return	ds_retorno_w;

end proj_obter_desc_equipe;
/