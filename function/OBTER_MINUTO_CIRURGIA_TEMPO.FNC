create or replace
function obter_minuto_cirurgia_tempo(nr_cirugia_p	number) 
	return number is

qt_minuto_w	number(6);

begin

select	sum(qt_minuto)
into	qt_minuto_w
from	cirurgia_tempo
where	nr_cirurgia = nr_cirugia_p;

return	qt_minuto_w;

end	obter_minuto_cirurgia_tempo;
/