create or replace
function obter_volume_parcial_sol_bi	(ie_tipo_solucao_p	number,
				nr_prescricao_p	number,
				nr_seq_solucao_p	number)
				return number is

qt_volume_parcial_w	number(15,2);

begin
if	(ie_tipo_solucao_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) then

	if	(ie_tipo_solucao_p = 1) then

		select	nvl(sum(qt_volume_parcial),0)
		into	qt_volume_parcial_w
		from	prescr_solucao_evento
		where	nr_prescricao		= nr_prescricao_p
		and	nr_seq_solucao		= nr_seq_solucao_p
		and	ie_alteracao		= 14
		and	ie_evento_valido		= 'S';
	else
		select	nvl(sum(qt_volume_parcial),0)
		into	qt_volume_parcial_w
		from	prescr_solucao_evento
		where	nr_prescricao		= nr_prescricao_p
		and	nr_seq_material		= nr_seq_solucao_p
		and	ie_alteracao		= 14
		and	ie_evento_valido		= 'S';
	end if;

end if;

return qt_volume_parcial_w;

end obter_volume_parcial_sol_bi;
/
