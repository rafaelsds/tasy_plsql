create or replace
function lic_obter_registro_preco(	nr_seq_licitacao_p		number,
				nr_seq_lic_item_p		number)
 		    	return number is

nr_sequencia_w			number(10);			
			
begin


select	max(a.nr_sequencia)
into	nr_sequencia_w
from	reg_compra a,
	reg_compra_item b
where	a.nr_sequencia	= b.nr_seq_reg_compra
and	a.nr_seq_licitacao	= nr_seq_licitacao_p
and	b.nr_seq_lic_item	= nr_seq_lic_item_p
and	b.dt_cancelamento is null;

return	nr_sequencia_w;

end lic_obter_registro_preco;
/