create or replace
function obter_tipo_classif(nr_sequencia_p		number)
 		    	return varchar2 is

ds_classif_pacient_w	varchar(90);

begin

if	(nr_sequencia_p is not null) then
	select	max(ds_classif_paciente)
	into	ds_classif_pacient_w
	from 	tipo_classificao_paciente
	where 	nr_sequencia = nr_sequencia_p;
end if;

return	ds_classif_pacient_w;

end obter_tipo_classif;
/