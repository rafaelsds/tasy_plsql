Create or Replace
Function ocup_obter_se_pac_cirurgia (	nr_atendimento_p	number)
return Varchar is

ds_resultado_w		Varchar2(1);
qt_quantidade_w		Number(5);

BEGIN

SELECT	COUNT(*)
INTO	qt_quantidade_w
FROM	setor_atendimento b,
		atend_paciente_unidade a,
		cirurgia c
WHERE	a.cd_setor_atendimento = b.cd_setor_atendimento
AND		a.nr_atendimento 	   = c.nr_atendimento	   
AND		a.dt_entrada_unidade   = c.dt_entrada_unidade
AND		c.ie_status_cirurgia NOT IN (3,4) 
AND		b.cd_classif_setor = 2
AND		a.dt_saida_unidade IS NULL
AND		a.nr_atendimento = nr_atendimento_p;


ds_resultado_w	:= 'N';
if	(qt_quantidade_w > 0) then
	ds_resultado_w	:= 'S';
end if;

RETURN ds_resultado_w;
END ocup_obter_se_pac_cirurgia;
/
