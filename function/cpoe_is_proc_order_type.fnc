 create or replace 
 function cpoe_is_proc_order_type(nr_seq_proc_interno_p proc_order_type.nr_seq_proc_interno%type,
                                     nr_seq_order_unit_p proc_order_type.nr_seq_order_type%type,
                                     cd_estabelecimento_p proc_order_type.cd_estabelecimento%type
                                     ) return  varchar is
                                   
 
 is_proc     varchar(1) := 'N';
 
 begin

 
     select  nvl(max('S'),'N') 
     into    is_proc
 	from	cpoe_order_unit a,
 			PROC_ORDER_TYPE b
     where   a.nr_sequencia = nr_seq_order_unit_p
 	and		a.NR_SEQ_CPOE_TIPO_PEDIDO = b.nr_seq_order_type
 	and		b.NR_SEQ_PROC_INTERNO = nr_seq_proc_interno_p
     and    (b.cd_estabelecimento is null or b.cd_estabelecimento  = cd_estabelecimento_p)
 	and		b.IE_SITUACAO = 'A';
 
     return is_proc;
 
 end cpoe_is_proc_order_type;
 /
