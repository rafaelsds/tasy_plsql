create or replace
function obter_ie_tipo_ordem_nf	(nr_ordem_compra_p	number)
				return varchar2 is
			
ie_tipo_ordem_compra_w	varchar2(2);

begin
if	(nr_ordem_compra_p is not null) then
	begin
	select	nvl(ie_tipo_ordem,'N') 
	into	ie_tipo_ordem_compra_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_p;
	end;
end if;	

return	ie_tipo_ordem_compra_w;

end obter_ie_tipo_ordem_nf;
/