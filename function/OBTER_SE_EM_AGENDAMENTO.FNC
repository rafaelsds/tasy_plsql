create or replace
function obter_se_em_agendamento(	dt_inicio_agendamento_p	date)
 		    	return varchar2 is

qt_tempo_em_agenda_w 	number(10);
ie_em_agendamento_w		varchar2(2) := 'S';

begin
begin
	select	max(qt_tempo_em_agenda)
	into 	qt_tempo_em_agenda_w
	from	parametro_agenda
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	if 	(dt_inicio_agendamento_p is not null) and
		(nvl(qt_tempo_em_agenda_w, 0) > 0) then

		if (PKG_DATE_UTILS.get_DiffDate(dt_inicio_agendamento_p,sysdate,'MINUTE') > qt_tempo_em_agenda_w) then
			ie_em_agendamento_w := 'N';
		end if;
			
	end if;
exception
	when others then
		ie_em_agendamento_w := 'N';
end;
return	ie_em_agendamento_w;

end obter_se_em_agendamento;
/