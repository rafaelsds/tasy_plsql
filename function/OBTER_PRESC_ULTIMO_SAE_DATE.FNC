create or replace
function obter_presc_ultimo_sae_date (nr_atendimento_p	varchar2,
									  ie_opcao_p		varchar2)
 		    	return date is
			
/* op��o				*/
/* 	DI = data inicio sae 		*/
/*	DF = data fim sae 		*/
/*	DP = data prescri��o*/
/*	DT= Data Libera��o*/

dt_inicio_w			date;
dt_fim_w			date;
dt_prescricao_w		date;
dt_liberacao_w      date;
ds_retorno_w		date;
nr_sequencia_w		number(10);

begin

SELECT MAX(b.nr_sequencia)
into	nr_sequencia_w
FROM 	pe_prescricao b
where	b.nr_atendimento = nr_atendimento_p
and	b.dt_liberacao is not null
and	b.dt_inativacao is null;

if	(nr_sequencia_w	is not null) then
	SELECT	DT_INICIO_PRESCR,
			DT_VALIDADE_PRESCR,
			dt_prescricao,
			dt_liberacao
	into	dt_inicio_w,
			dt_fim_w,
			dt_prescricao_w,
			dt_liberacao_w
	FROM	pe_prescricao
	WHERE	nr_sequencia = nr_sequencia_w;

	if	(ie_opcao_p = 'DI') then
		ds_retorno_w := dt_inicio_w;
	elsif	(ie_opcao_p = 'DF') then
		ds_retorno_w := dt_fim_W;
	elsif	(ie_opcao_p = 'DP') then
		ds_retorno_w := dt_prescricao_w;
	elsif	(ie_opcao_p = 'DT') then
		ds_retorno_w := dt_liberacao_w;
	end if;
end if;
return	ds_retorno_w;


end obter_presc_ultimo_sae_date;
/
