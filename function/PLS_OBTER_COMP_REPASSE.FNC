create or replace 
function pls_obter_comp_repasse (	dt_referencia_p	date,
					nr_seq_segurado_p	number,
					nr_seq_congenere_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ie_tipo_compartilhamento_w			number(10,0);
ie_tipo_repasse_w				pls_segurado_repasse.ie_tipo_repasse%type;
dt_repasse_w					pls_segurado_repasse.dt_repasse%type;
dt_fim_repasse_w				pls_segurado_repasse.dt_fim_repasse%type;

begin

pls_obter_dados_repasse(			dt_referencia_p,
						nr_seq_segurado_p,
						nr_seq_congenere_p,
						ie_tipo_repasse_w,
						ie_tipo_compartilhamento_w,
						dt_repasse_w,
						dt_fim_repasse_w);

if (ie_opcao_p = 'C') then

	return ie_tipo_compartilhamento_w;

end if;

if (ie_opcao_p = 'R') then

	return ie_tipo_repasse_w;

end if;

end pls_obter_comp_repasse;
/