create or replace
function Obter_CNPJ_Medico(	cd_medico_p		varchar2,
				nr_sequencia_p		number)
 		    	return varchar2 is
			
cd_cgc_w	varchar2(14);

begin

select	max(cd_cgc)
into	cd_cgc_w
from	procedimento_participante
where	cd_pessoa_fisica = cd_medico_p
and	nr_sequencia = nr_sequencia_p;


return	cd_cgc_w;

end Obter_CNPJ_Medico;
/
