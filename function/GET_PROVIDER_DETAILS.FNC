create or replace function get_provider_details (
	cd_medico_p 			varchar2,
	cd_estabelecimento_p 	number,
	cd_setor_atendimento_p 	number)

return varchar2 IS

nr_provider_w varchar2(8);

begin 

select  max(nvl(nr_provider, null)) 
into    nr_provider_w
from    medical_provider_number a
where   a.cd_medico = cd_medico_p
and (a.cd_estabelecimento = cd_estabelecimento_p or a.cd_estabelecimento is null)
and (a.cd_department = cd_setor_atendimento_p or a.cd_department is null);

return nr_provider_w;

end get_provider_details;
/