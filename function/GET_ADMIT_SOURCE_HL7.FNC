create or replace
function get_admit_source_hl7 (
	nr_atendimento_p	number,
	nr_seq_interno_p	number)
	return number is
	
admit_source_w	number(1) := 7;
	
begin
if	(nr_atendimento_p is not null) and
	(nr_seq_interno_p is not null) then
	begin
	select	decode(count(*),0,7,2)
	into	admit_source_w
	from	setor_atendimento s,
		atend_paciente_unidade u
	where	s.cd_setor_atendimento = u.cd_setor_atendimento
	and	s.cd_classif_setor = '1'
	and	u.nr_atendimento = nr_atendimento_p
	and	u.nr_seq_interno < nr_seq_interno_p;
	end;
end if;
return admit_source_w;
end get_admit_source_hl7;
/