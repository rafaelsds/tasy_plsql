create or replace
function obter_seq_apres_medic(cd_protocolo_p	number,
				nr_seq_protocolo_p	number,
				cd_material_p	number)
 		    	return number is
			
nr_seq_apres_w		number(10);

begin

if	(cd_protocolo_p is not null) and (nr_seq_protocolo_p is not null) and
	(cd_material_p is not null)  then
	
	select	max(a.nr_seq_apresentacao)
	into	nr_seq_apres_w
	from	protocolo_medic_material a
	where	cd_protocolo 	 = cd_protocolo_p
	and	nr_sequencia 	 = nr_seq_protocolo_p
	and	cd_material  = cd_material_p;
end if;

return	nr_seq_apres_w;

end obter_seq_apres_medic;
/