create or replace
function obter_nota_credito_lote_hist(nr_seq_lote_p	number)
						return number is

nr_seq_nota_credito_w	number(10);

begin

if	(nr_seq_lote_p is not null) then
	select	max(a.nr_sequencia)
	into	nr_seq_nota_credito_w
	from	nota_credito a
	where	a.nr_seq_lote_audit_hist	= nr_seq_lote_p;
end if;

return nr_seq_nota_credito_w;

end;
/