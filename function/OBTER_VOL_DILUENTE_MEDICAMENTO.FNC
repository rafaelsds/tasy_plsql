create or replace
function obter_vol_diluente_medicamento
		(nr_prescricao_p	number,
		nr_sequencia_p		number)
		return number is


qt_retorno_w		number(18,6);
cd_unidade_medida_w	varchar2(30);
cd_material_w		number(6);
qt_dose_w		number(18,6);

begin

select	cd_material,
	nvl(qt_solucao,qt_dose),
	cd_unidade_medida_dose
into	cd_material_w,
	qt_dose_w,
	cd_unidade_medida_w
from	prescr_material
where	nr_prescricao			= nr_prescricao_p
and		nr_sequencia_diluicao	= nr_sequencia_p
and		ie_agrupador			= 3;

if	(cd_unidade_medida_w = OBTER_UNID_MED_USUA('ml')) then
	qt_retorno_w	:= qt_dose_w;
else
	select	obter_conversao_ml(cd_material_w,qt_dose_w,cd_unidade_medida_w)
	into	qt_retorno_w
	from	dual;	
end if;

return qt_retorno_w;

end obter_vol_diluente_medicamento;
/
