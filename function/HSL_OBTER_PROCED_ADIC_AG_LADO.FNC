create or replace
function hsl_obter_proced_adic_ag_lado(nr_seq_agenda_p	number)
						return varchar2 is

ds_procedimentos_w	varchar2(4000);
ds_proced_w		varchar2(260);
nr_ordem_w		number(10,0);
ie_lado_w		varchar2(255);

cursor	c01 is
select	2 ordem,
	b.cd_procedimento||' - '||b.ds_procedimento,
	decode(a.ie_lado,'D',' Direito','E', 'Esquerdo', 'A', 'Ambos')
from	procedimento b,
	agenda_paciente_proc a
where	a.cd_procedimento = b.cd_procedimento
and	a.ie_origem_proced = b.ie_origem_proced
and	a.nr_sequencia = nr_seq_agenda_p
and	a.nr_seq_proc_interno is null
union
select	2 ordem,
	b.nr_sequencia||' - '||b.ds_proc_exame,
	decode(a.ie_lado,'D',' Direito','E', 'Esquerdo', 'A', 'Ambos')
from	proc_interno b,
	agenda_paciente_proc a
where	a.nr_seq_proc_interno = b.nr_sequencia
and	a.nr_sequencia = nr_seq_agenda_p
order by
	1;

begin
if	(nr_seq_agenda_p is not null) then
	open c01;
	loop
	fetch c01	into
			nr_ordem_w,
			ds_proced_w,
			ie_lado_w;
	exit when c01%notfound;
		begin
		
		if	(nvl(length(ds_procedimentos_w),0) < 3930) then
			ds_procedimentos_w := ds_procedimentos_w || ds_proced_w || ' - ' ||ie_lado_w||' + ';
		end if;
		end;
	end loop;
	close c01;
end if;

return ds_procedimentos_w;

end hsl_obter_proced_adic_ag_lado;
/