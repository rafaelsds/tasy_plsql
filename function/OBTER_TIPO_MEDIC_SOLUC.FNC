create or replace
function obter_tipo_medic_soluc(nr_seq_atendimento_p	number,
				nr_seq_solucao_p	number)
 		    	return varchar2 is

ie_pre_medicacao_w   varchar2(1);
			
begin
select	max(ie_pre_medicacao)
into	ie_pre_medicacao_w 
from 	paciente_atend_soluc x
WHERE  	x.nr_seq_atendimento	 = nr_seq_atendimento_p
and   	x.nr_seq_solucao   	 = nr_seq_solucao_p;
	

return	ie_pre_medicacao_w;

end obter_tipo_medic_soluc;
/
