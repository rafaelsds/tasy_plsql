create or replace
function obter_tipo_lista_simpro_vig(	cd_simpro_p	Number,
				dt_vigencia_p	Date)
 		    	return Varchar2 is

ie_tipo_lista_w	varchar2(1);
			
begin

if	(nvl(cd_simpro_p,0) > 0) then

	select	max(ie_tipo_lista)
	into	ie_tipo_lista_w
	from	simpro_preco
	where	cd_simpro = cd_simpro_p
	and	dt_vigencia = dt_vigencia_p;

end if;

return	ie_tipo_lista_w;

end obter_tipo_lista_simpro_vig;
/