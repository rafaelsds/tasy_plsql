create or replace
function pls_obter_se_cheque_devolucao
			(	nr_seq_segurado_p		Number)
 		    		return varchar2 is
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retorna se � cheque devolvido.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ x]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_fisica_w		varchar2(10);				
ie_retorno_w			varchar2(1)	:= 'N';
nr_seq_pagador_w		number(10);
cd_cgc_w			pls_contrato_pagador.cd_cgc%type;

begin
begin
select	nr_seq_pagador
into	nr_seq_pagador_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;
exception
when others then
	nr_seq_pagador_w := 0;
end;

if	(nr_seq_pagador_w <> 0) then
	select	a.cd_pessoa_fisica,
		a.cd_cgc
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	pls_contrato_pagador a
	where	a.nr_sequencia = nr_seq_pagador_w;
	
	if	(cd_pessoa_fisica_w is not null) then
		select	decode(count(1),0,'N','S')
		into	ie_retorno_w
		from	cheque_cr		a
		where	a.cd_pessoa_fisica 	= cd_pessoa_fisica_w
		and	obter_status_cheque(a.nr_seq_cheque) in (3,5,6,7,10)
		and	rownum <= 1;
	end if;

	if (cd_cgc_w is not null) then
		select	decode(count(1),0,'N','S')
		into	ie_retorno_w
		from	cheque_cr		a
		where	a.cd_cgc 	= cd_cgc_w
		and	obter_status_cheque(a.nr_seq_cheque) in (3,5,6,7,10)
		and	rownum <= 1;
	end if;
end if;

return	ie_retorno_w;

end pls_obter_se_cheque_devolucao;
/