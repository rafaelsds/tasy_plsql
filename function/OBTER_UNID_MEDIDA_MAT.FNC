create or replace
function obter_unid_medida_mat(		cd_estabelecimento_p	number,
					cd_cnpj_p		varchar2,
					cd_material_p		number)
 		    	return varchar2 is

cd_unid_medida_w		varchar2(30);

begin

if	(cd_cnpj_p is null) then
	
	select	nvl(max(cd_unid_medida),0)
	into	cd_unid_medida_w
	from	material
	where	cd_material = cd_material_p;

else
	select	obter_dados_material_fornec(cd_estabelecimento_p, cd_cnpj_p, cd_material_p, 0)
	into	cd_unid_medida_w
	from	dual;
	
	if	(cd_unid_medida_w is null) then
	
		select	nvl(max(cd_unid_medida),0)
		into	cd_unid_medida_w
		from	material
		where	cd_material = cd_material_p;
	end if;
end if;

return	cd_unid_medida_w;

end obter_unid_medida_mat;
/