CREATE OR REPLACE 
FUNCTION Obter_Lab_Externo_Exame(nr_seq_exame_p		number) 
				return varchar2 is

ds_razao_social_w	pessoa_juridica.ds_razao_social%type;

begin

select	b.ds_razao_social
into	ds_razao_social_w
from	pessoa_juridica b,
	exame_laboratorio a
where	a.cd_cgc_externo	= b.cd_cgc
and	a.nr_seq_exame		= nr_seq_exame_p;

return ds_razao_social_w;

end Obter_Lab_Externo_Exame;
/
