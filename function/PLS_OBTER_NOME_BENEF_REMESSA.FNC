create or replace
function pls_obter_nome_benef_remessa
			(	nr_seq_pagador_p		number,
				nr_ordem_p			number)
				return varchar2 is
				
nm_beneficiario_w		varchar2(80);
nm_benef_ret_w			varchar2(80)	:= null;
nr_ordem_w			number(10)	:=0;
ie_ordem_w			number(10);

cursor c01 is
	select	1 ie_ordem,
		substr(b.nm_pessoa_fisica,1,60) nm_pessoa_benef
	from	pls_contrato_pagador c,
		pessoa_fisica	b,
		pls_segurado	a
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	a.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and	c.nr_seq_contrato	= a.nr_seq_contrato
	and	c.nr_sequencia		= nr_seq_pagador_p
	union all
	select	2 ie_ordem,
		substr(b.nm_pessoa_fisica,1,60) nm_pessoa_benef
	from	pessoa_fisica	b,
		pls_segurado	a
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	a.nr_seq_pagador	= nr_seq_pagador_p
	and	not exists	(select	1
				from	pls_contrato_pagador x
				where	x.nr_seq_contrato	= a.nr_seq_contrato
				and	x.cd_pessoa_fisica	= a.cd_pessoa_fisica)
	order by ie_ordem;

begin
open c01;
loop
fetch c01 into
	ie_ordem_w,
	nm_beneficiario_w;
exit when c01%notfound;
	begin
	nr_ordem_w := nr_ordem_w + 1;
	
	if	(nr_ordem_w = nr_ordem_p) then
		nm_benef_ret_w	:=	nm_beneficiario_w;
	end if;
	end;
end loop;
close c01;

return	nm_benef_ret_w;

end pls_obter_nome_benef_remessa;
/