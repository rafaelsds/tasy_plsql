create or replace
function db_get_docx_variable (
	nr_seq_macro_p		db_content_macro.nr_sequencia%type,
	ie_get_version_p	varchar2	default 'N')
return varchar2 is

/*
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Objective: Show the formatted macro for the user
	------------------------------------------------------------------------
	Direct call: 
	[   ] Dictionary objects
	[ X ] Tasy (Delphi/Java/HTML5)
	[   ] Portal
	[   ] Reports
	[   ] Others:
	 -----------------------------------------------------------------------
	Attention points: N/A
	------------------------------------------------------------------------
	References:	
		Tables: db_content_macro
		SQL objects: 
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

nr_seq_macro_w	db_content_macro.nr_sequencia%type;
ds_macro_w	db_content_macro.ds_macro%type;
nr_version_w	db_content_macro.nr_version%type;

ds_retorno_w	varchar2(4000);

begin

	select	cm.nr_sequencia,
		cm.ds_macro,
		cm.nr_version
	into	nr_seq_macro_w,
		ds_macro_w,
		nr_version_w
	from	db_content_macro cm
	where	cm.nr_sequencia = nr_seq_macro_p;

	if	(nr_seq_macro_w is not null) then
		begin
		
			if	(ie_get_version_p = 'S') then
				begin
					select	'${@' || ds_macro_w || '@' || nr_version_w || '}'
					into	ds_retorno_w
					from	dual;
				end;
			else
				begin
					select	'${@' || ds_macro_w || '}'
					into	ds_retorno_w
					from	dual;
				end;
			end if;
		end;
	end if;

	return ds_retorno_w;

end db_get_docx_variable;
/
