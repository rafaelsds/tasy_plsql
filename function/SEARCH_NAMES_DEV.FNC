create or replace function search_names_dev (
			nm_filter_p 		varchar2,
			cd_person_filter_p 	varchar2,
			nr_person_name_p 	number,
			ds_type_name_p 		varchar2 default null,
			ds_filter_name_p	varchar2 default null,
			establishment_p		estabelecimento.cd_estabelecimento%type default null,
			exact_match_p	        varchar2 default null			)
			return person_name_dev_table pipelined is type cursor_ref is ref cursor;	

person_name_dev_r		person_name_dev_row := person_name_dev_row(null,null);
is_name_feature_enabled	boolean;
ds_type_name_w	 		varchar2(50);
ds_filter_name_w 		varchar2(50);
establishment 			number(4)		:= wheb_usuario_pck.get_cd_estabelecimento; 
nm_filtro_w				varchar2(300)	:= padronizar_nome(nm_filter_p);
nm_filtro_validacao_w	varchar2(300);

cd_pessoa_fisica_w 		varchar2(10);
nm_translated_w			varchar2(200);
nm_social_w				varchar2(200);
nm_pessoa_fisica_w 		varchar2(200);
ds_se_nome_social_w 	varchar2(1);



cursor c01 is
	select	b.cd_pessoa_fisica,
			pkg_name_utils.get_person_name(a.nr_sequencia, establishment, ds_type_name_w, ds_filter_name_w),
			pkg_name_utils.get_person_name(a.nr_sequencia, establishment, ds_type_name_w, 'translated')
	from	table(pkg_name_utils.search_names(nm_filter_p, ds_filter_name_w, exact_match_p)) a,
			pessoa_fisica b
	where	b.nr_seq_person_name = a.nr_sequencia;

cursor c02 is 
	select	cd_pessoa_fisica,
			nvl(nm_social, nm_pessoa_fisica)
	from	pessoa_fisica
	where	UPPER(decode(nm_social,null,NVL(nm_pessoa_pesquisa, nm_pessoa_fisica),padronizar_nome(nm_social))) LIKE '%' || nm_filtro_w || '%';

cursor c03 is
 SELECT cd_pessoa_fisica, 
	nm_pessoa_fisica
	 FROM (SELECT PAGING.*,
	    ROW_NUMBER() OVER (ORDER BY FRAMEWORK_ORDER_INDEX) PAGING_RN 
	 FROM (SELECT RETURNED_ROWS.*,
	  NULL FRAMEWORK_ORDER_INDEX
	 FROM (SELECT /*+ first_rows(10000) */ 
	  cd_pessoa_fisica,
	    nm_pessoa_fisica
	  from pessoa_fisica
	  where NM_PESSOA_pesquisa like '%' || nm_filtro_w || '%'
	   ) RETURNED_ROWS) PAGING)
	 WHERE (PAGING_RN >= 1) AND (PAGING_RN <= 10000);
	

cursor c04 is
	select	cd_pessoa_fisica,
			nm_social,
			nm_pessoa_fisica
	from	pessoa_fisica
	where	UPPER(decode(nm_social,null,NVL(nm_pessoa_pesquisa, nm_pessoa_fisica),padronizar_nome(nm_social))) LIKE '%' || nm_filtro_w || '%';
	
cursor c05 is
	select	b.cd_pessoa_fisica,
			pkg_name_utils.get_person_name(a.nr_sequencia, establishment, ds_type_name_w, 'social'),
			pkg_name_utils.get_person_name(a.nr_sequencia, establishment, ds_type_name_w, 'main'),
			pkg_name_utils.get_person_name(a.nr_sequencia, establishment, ds_type_name_w, 'translated')
	from	table(pkg_name_utils.search_names(nm_filter_p, ds_filter_name_w, exact_match_p)) a,
			pessoa_fisica b
	where	b.nr_seq_person_name = a.nr_sequencia;

cursor c06 is
	select	cd_pessoa_fisica,
		nm_social,
		nm_pessoa_fisica
	from	pessoa_fisica
	where	lower(nm_social) like '%' || lower(nm_filter_p) || '%'
	or	lower(nm_pessoa_fisica) like '%' || lower(nm_filter_p) || '%';
	
cursor c07 is
	select	b.cd_pessoa_fisica,
		pkg_name_utils.get_person_name(a.nr_sequencia, establishment, ds_type_name_w, 'main')
	from	table(pkg_name_utils.search_names(nm_filter_p, ds_filter_name_w, exact_match_p)) a,
		pessoa_fisica b
	where	b.nr_seq_person_name = a.nr_sequencia
	and	upper(pkg_name_utils.get_person_name(a.nr_sequencia, establishment, ds_type_name_w, 'translated')) like '%' || upper(nm_filtro_w) || '%';	

begin
 
/* Validation used because of web applications that have no user in the Tasy session. This validation should not be altered or deleted. */
if	( establishment_p is not null ) then
	establishment	:= 	establishment_p;
end if;


ds_type_name_w := ds_type_name_p;
ds_filter_name_w := ds_filter_name_p;
ds_se_nome_social_w := pkg_name_utils.get_social_name_enabled(establishment);

if (nr_person_name_p is not null) then
	begin
	if (ds_filter_name_w = 'both') then
		ds_filter_name_w := null;
	end if;

	if (ds_type_name_w is null) then
		begin
		ds_type_name_w := 'full';
		end;
	end if;

	if (ds_filter_name_w is null) then
		begin
		ds_filter_name_w := 'main';

		if (ds_se_nome_social_w = 'S') then
			begin
			ds_filter_name_w := 'social,' || ds_filter_name_w;
			end;
		end if;
		end;
	end if;

	if (ds_se_nome_social_w = 'T') then
		begin
		select	cd_pessoa_fisica,
				pkg_name_utils.get_person_name(nr_person_name_p, establishment, ds_type_name_w, 'social'),
				pkg_name_utils.get_person_name(nr_person_name_p, establishment, ds_type_name_w, 'main'),
				pkg_name_utils.get_person_name(nr_person_name_p, establishment, ds_type_name_w, 'translated')
		into	cd_pessoa_fisica_w,
				nm_social_w,
				nm_pessoa_fisica_w,
				nm_translated_w
		from	pessoa_fisica
		where	nr_seq_person_name = nr_person_name_p
		and		rownum <= 1;

		person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;

		if (PKG_I18N.get_user_locale = 'ja_JP') then
			begin
                if(nm_translated_w is not null) then
                    person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w || ' (' || nm_translated_w || ')', 1, 200);
                else
                    person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w, 1, 200);
                end if;
			end;
		elsif (nm_social_w is not null) then
			begin
			person_name_dev_r.nm_pessoa_fisica := substr(nm_social_w || ' (' || nm_pessoa_fisica_w || ')', 1, 200);
			end;
		else
			begin
			person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
			end;
		end if;
		end;
	else
		begin
		select	cd_pessoa_fisica,
			pkg_name_utils.get_person_name(nr_person_name_p, establishment, ds_type_name_w, ds_filter_name_w),
			pkg_name_utils.get_person_name(nr_person_name_p, establishment, ds_type_name_w, 'translated')
		into	cd_pessoa_fisica_w,
			nm_pessoa_fisica_w,
			nm_translated_w
		from	pessoa_fisica
		where	nr_seq_person_name = nr_person_name_p
		and		rownum <= 1;
		
		person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;
		person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
		
		if (PKG_I18N.get_user_locale = 'ja_JP') then
			begin
                if(nm_translated_w is not null) then
                    person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w || ' (' || nm_translated_w || ')', 1, 200);
                else
                    person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w, 1, 200);
                end if;
			end;
		elsif (ds_filter_name_w = 'social,main') and (trim(person_name_dev_r.nm_pessoa_fisica) is null) then
                select	pkg_name_utils.get_person_name(nr_person_name_p, establishment, ds_type_name_w, 'main')
                into	person_name_dev_r.nm_pessoa_fisica
                from	pessoa_fisica
                where	nr_seq_person_name = nr_person_name_p
                and		rownum <= 1;    
        end if;
		end;
	end if;
	pipe row(person_name_dev_r);
	end;
elsif (cd_person_filter_p is not null) then
	begin
	if (ds_filter_name_w = 'both') then
		ds_filter_name_w := null;
	end if;
	is_name_feature_enabled := pkg_name_utils.get_name_feature_enabled() = 'S';

	if (is_name_feature_enabled) then
		begin
		if (ds_type_name_w is null) then
			begin
			ds_type_name_w := 'full';
			end;
		end if;

		if (ds_filter_name_w is null) then
			begin
			ds_filter_name_w := 'main';

			if (ds_se_nome_social_w = 'S') then
				begin
				ds_filter_name_w := 'social,' || ds_filter_name_w;
				end;
			end if;
			end;
		end if;

		if (ds_se_nome_social_w = 'T') then
			begin
			select	a.cd_pessoa_fisica,
					pkg_name_utils.get_person_name(a.nr_seq_person_name, establishment, ds_type_name_w, 'social'),
				pkg_name_utils.get_person_name(a.nr_seq_person_name, establishment, ds_type_name_w, 'main'),
				pkg_name_utils.get_person_name(a.nr_seq_person_name, establishment, ds_type_name_w, 'translated')
			into	cd_pessoa_fisica_w,
					nm_social_w,
				nm_pessoa_fisica_w,
				nm_translated_w
			from	pessoa_fisica a
			where	a.cd_pessoa_fisica = cd_person_filter_p
			and		rownum <= 1;

			person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;

			if (PKG_I18N.get_user_locale = 'ja_JP') then
				begin
                    if(nm_translated_w is not null) then
                        person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w || ' (' || nm_translated_w || ')', 1, 200);
                    else
                        person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w, 1, 200);
                    end if;
				end;
			elsif (nm_social_w is not null) then
				begin
				person_name_dev_r.nm_pessoa_fisica := substr(nm_social_w || ' (' || nm_pessoa_fisica_w || ')', 1, 200);
				end;
			else
				begin
				person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
				end;
			end if;
			end;
		else
			begin
			select	a.cd_pessoa_fisica,
				pkg_name_utils.get_person_name(a.nr_seq_person_name, establishment, ds_type_name_w, ds_filter_name_w),
					   pkg_name_utils.get_person_name(a.nr_seq_person_name, establishment, ds_type_name_w, 'translated')
				into   cd_pessoa_fisica_w,
					   nm_pessoa_fisica_w,
					   nm_translated_w
				from   pessoa_fisica a
				where  a.cd_pessoa_fisica = cd_person_filter_p
				and    rownum <=1;
				
				person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;	

			if (PKG_I18N.get_user_locale = 'ja_JP') then
					begin
                    if(nm_translated_w is not null) then
                        person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w || ' (' || nm_translated_w || ')', 1, 200);
                    else
                        person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w, 1, 200);
				end if;				
					end;
				else
					begin
					person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
					end;
				end if;
				end;
		end if;
		pipe row(person_name_dev_r);
		end;
			else	
				begin
		if ( ds_se_nome_social_w = 'S') then
			begin
				select	a.cd_pessoa_fisica,
						nvl(a.nm_social, a.nm_pessoa_fisica)
				into	person_name_dev_r.cd_pessoa_fisica,
						person_name_dev_r.nm_pessoa_fisica
				from	pessoa_fisica a
				where	a.cd_pessoa_fisica = cd_person_filter_p
				and		rownum <= 1;
				end;
		elsif (ds_se_nome_social_w = 'T') then
				begin
				select	a.cd_pessoa_fisica,
						a.nm_social,
						a.nm_pessoa_fisica
				into	cd_pessoa_fisica_w,
						nm_social_w,
						nm_pessoa_fisica_w
				from	pessoa_fisica a
				where	a.cd_pessoa_fisica = cd_person_filter_p
				and		rownum <= 1;

				person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;

				if (nm_social_w is not null) then
					begin
					person_name_dev_r.nm_pessoa_fisica := substr(nm_social_w || ' (' || nm_pessoa_fisica_w || ')', 1, 200);
					end;
				else
					begin
					person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
					end;
				end if;
				end;
		else
				begin
				select	a.cd_pessoa_fisica,
						a.nm_pessoa_fisica
				into	person_name_dev_r.cd_pessoa_fisica,
						person_name_dev_r.nm_pessoa_fisica
				from	pessoa_fisica a
				where	a.cd_pessoa_fisica = cd_person_filter_p
				and		rownum <= 1;
				end;
			end if;
		pipe row(person_name_dev_r);
		end;
	end if;
	end;
else
	begin
	
	if (nm_filter_p is not null) then
		begin
			
		is_name_feature_enabled := pkg_name_utils.get_name_feature_enabled() = 'S';	

		if (is_name_feature_enabled) then
			begin
			if (ds_filter_name_w = 'both') then
				ds_filter_name_w := null;
			end if;
			if (ds_type_name_w is null) then
				begin
				ds_type_name_w := 'full';
				end;
			end if;

			if (ds_filter_name_w is null) then
				begin
				ds_filter_name_w := 'main';

				if (ds_se_nome_social_w = 'S') then
					begin
					ds_filter_name_w := 'social,' || ds_filter_name_w;
					end;
				end if;
				end;
			end if;

			if (ds_se_nome_social_w = 'T') then
				begin
				open c05;
				loop
				fetch c05
				into	cd_pessoa_fisica_w,
						nm_social_w,
						nm_pessoa_fisica_w,
						nm_translated_w;
				exit when c05%notfound;
					begin
					person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;

					if (PKG_I18N.get_user_locale = 'ja_JP') then
						begin
                            if(nm_translated_w is not null) then
                                person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w || ' (' || nm_translated_w || ')', 1, 200);
                            else
                                person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w, 1, 200);
                            end if;
						end;
					elsif (nm_social_w is not null) then
						begin
						person_name_dev_r.nm_pessoa_fisica := substr(nm_social_w || ' (' || nm_pessoa_fisica_w || ')', 1, 200);
						end;
					else
						begin
						person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
						end;
					end if;
					pipe row(person_name_dev_r);
					end;
				end loop;
				close c05;
				end;
			else
				begin
				open c01;
				loop
				fetch c01
				into	cd_pessoa_fisica_w,
					nm_pessoa_fisica_w,
					nm_translated_w;
				exit when c01%notfound;
					begin
					person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;
					
					if (PKG_I18N.get_user_locale = 'ja_JP') then
						begin
                            if(nm_translated_w is not null) then
                                person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w || ' (' || nm_translated_w || ')', 1, 200);
                            else
                                person_name_dev_r.nm_pessoa_fisica := substr(nm_pessoa_fisica_w, 1, 200);
                            end if;
						end;
					else
						begin
						person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
						end;
					end if;
					
					pipe row(person_name_dev_r);
					end;
				end loop;
				close c01;
				end;
			end if;
			end;
		else
		
			begin
			if (ds_filter_name_w = 'both') then
				open C06;
				loop
				fetch C06 into
					cd_pessoa_fisica_w,
					nm_social_w,
					nm_pessoa_fisica_w;
				exit when C06%notfound;
					begin
					person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;
					if  (ds_se_nome_social_w = 'S') then --c02
						person_name_dev_r.nm_pessoa_fisica := nvl(nm_social_w,nm_pessoa_fisica_w);
					elsif (ds_se_nome_social_w = 'T') then --c04
						if (nm_social_w is not null) then
							begin
							person_name_dev_r.nm_pessoa_fisica := substr(nm_social_w || ' (' || nm_pessoa_fisica_w || ')', 1, 200);
							end;
						else
							begin
							person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
							end;
						end if;
					else --c03
						person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
					end if;
					end;
					pipe row(person_name_dev_r);
				end loop;
				close C06;
			elsif  (ds_se_nome_social_w = 'S') then
				begin
								
				if	(nm_filter_p is null) then
					return;
				end if;
				
					open c02;
					loop
					fetch c02
					into	person_name_dev_r.cd_pessoa_fisica,
							person_name_dev_r.nm_pessoa_fisica;
					exit when c02%notfound;
						begin
						pipe row(person_name_dev_r);
						end;
					end loop;
					close c02;				
				end;
			elsif (ds_se_nome_social_w = 'T') then
				begin
								
				open c04;
				loop
				fetch c04
				into	cd_pessoa_fisica_w,
						nm_social_w,
						nm_pessoa_fisica_w;
				exit when c04%notfound;
					begin
					person_name_dev_r.cd_pessoa_fisica := cd_pessoa_fisica_w;

					if (nm_social_w is not null) then
						begin
						person_name_dev_r.nm_pessoa_fisica := substr(nm_social_w || ' (' || nm_pessoa_fisica_w || ')', 1, 200);
						end;
					else
						begin
						person_name_dev_r.nm_pessoa_fisica := nm_pessoa_fisica_w;
						end;
					end if;
					pipe row(person_name_dev_r);
					end;
				end loop;
				close c04;
				end;
			else
				begin
				
				nm_filtro_validacao_w	:= replace(replace(nm_filtro_w,'%',''),' ','');
				if	(nvl(length(nm_filtro_validacao_w),0)	<=2) then
					return;
				end if;
				
				if	(ds_filter_name_p = 'translated') then
				
					open c07;
					loop
					fetch c07
					into	person_name_dev_r.cd_pessoa_fisica,
						person_name_dev_r.nm_pessoa_fisica;
					exit when c07%notfound;
						begin
						pipe row(person_name_dev_r);
						end;
					end loop;
					close c07;
				else
				
					open c03;
					loop
					fetch c03
					into	person_name_dev_r.cd_pessoa_fisica,
							person_name_dev_r.nm_pessoa_fisica;
					exit when c03%notfound;
						begin
						pipe row(person_name_dev_r);
						end;
					end loop;
					close c03;
				end if;
				end;
			end if;
			end;
		end if;
		end;
	else
		begin
		return;
		end;
	end if;
	end;
end if;
end search_names_dev;
/
