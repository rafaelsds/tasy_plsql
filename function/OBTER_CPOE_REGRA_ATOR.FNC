create or replace
FUNCTION obter_cpoe_regra_ator(ds_order_type_p varchar2) return varchar2 is

/* ds_order_type_p - types of acceptable values
D - diet order
E - enteral nutrition
IM - image order
IP - infusion pump order
M - medication order
R - recommendation order
G - gas order
P - procedure order
O - other
*/

cpoe_regra_ator_row_w		CPOE_REGRA_ATOR%rowtype;

BEGIN

if (ds_order_type_p not in ('D', 'E', 'IM', 'IP', 'M', 'R', 'G', 'O', 'P')) then
	return null;
end if;

select *
into cpoe_regra_ator_row_w
from CPOE_REGRA_ATOR
where cd_estabelecimento = obter_estabelecimento_ativo
and rownum = 1
order by nr_sequencia desc;

if (ds_order_type_p = 'D') then
	return cpoe_regra_ator_row_w.IE_DIETETIC_ORDER_TYPE;
elsif (ds_order_type_p = 'E') then
	return cpoe_regra_ator_row_w.IE_ENTERAL_ORDER_TYPE;
elsif (ds_order_type_p = 'IM') then
	return cpoe_regra_ator_row_w.IE_IMAGING_ORDER_TYPE;
elsif (ds_order_type_p = 'IP') then
	return cpoe_regra_ator_row_w.IE_INFUSION_ORDER_TYPE;
elsif (ds_order_type_p = 'M') then
	return cpoe_regra_ator_row_w.IE_MEDICATION_ORDER_TYPE;
elsif (ds_order_type_p = 'R') then
	return cpoe_regra_ator_row_w.IE_RECOMMENDATION_ORDER_TYPE;
elsif (ds_order_type_p = 'G') then
	return cpoe_regra_ator_row_w.IE_GAS_ORDER_TYPE;
elsif (ds_order_type_p = 'P') then
	return cpoe_regra_ator_row_w.IE_PROC_ORDER_TYPE;
elsif (ds_order_type_p = 'O') then
	return cpoe_regra_ator_row_w.IE_OTHER_ORDER_TYPE;
else
	return null;
end if;

exception
	when NO_DATA_FOUND then
		return null;

END obter_cpoe_regra_ator;
/
