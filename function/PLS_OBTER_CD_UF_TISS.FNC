create or replace
function pls_obter_cd_uf_tiss(	sc_uf_tiss_p	varchar2) return varchar2 deterministic is

ds_retorno_w	pls_conta.uf_crm_exec_imp%type	:= null;

begin

case (sc_uf_tiss_p )

	when 'RO' then
		ds_retorno_w	:= '11';
	when 'AC' then
		ds_retorno_w	:= '12';
	when 'AM' then
		ds_retorno_w	:= '13';
	when 'RR' then
		ds_retorno_w	:= '14';
	when 'PA' then
		ds_retorno_w	:= '15';
	when 'AP' then
		ds_retorno_w	:= '16';
	when 'TO' then
		ds_retorno_w	:= '17';
	when 'MA' then
		ds_retorno_w	:= '21';
	when 'PI' then
		ds_retorno_w	:= '22';
	when 'CE' then
		ds_retorno_w	:= '23';
	when 'RN' then
		ds_retorno_w	:= '24';
	when 'PB' then
		ds_retorno_w	:= '25';
	when 'PE' then
		ds_retorno_w	:= '26';
	when 'AL' then
		ds_retorno_w	:= '27';
	when 'SE' then
		ds_retorno_w	:= '28';
	when 'BA' then
		ds_retorno_w	:= '29';
	when 'MG' then
		ds_retorno_w	:= '31';
	when 'ES' then
		ds_retorno_w	:= '32';
	when 'RJ' then
		ds_retorno_w	:= '33';
	when 'SP' then
		ds_retorno_w	:= '35';
	when 'PR' then
		ds_retorno_w	:= '41';
	when 'SC' then
		ds_retorno_w	:= '42';
	when 'RS' then
		ds_retorno_w	:= '43';
	when 'MS' then
		ds_retorno_w	:= '50';
	when 'MT' then
		ds_retorno_w	:= '51';
	when 'GO' then
		ds_retorno_w	:= '52';
	when 'DF' then
		ds_retorno_w	:= '53';
	when 'EX' then
		ds_retorno_w	:= '98';
	else
		ds_retorno_w	:= null;
end case;

return	ds_retorno_w;

end pls_obter_cd_uf_tiss;
/
