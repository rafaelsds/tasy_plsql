create or replace
function ptu_obter_proc_mat_desc
		(	cd_servico_p		Number,
			ie_origem_proced_p	Number,
			ie_tipo_servico_p	Varchar2)
 		    	return Varchar2 is

ds_retorno_w	varchar2(255);
			
begin

if	(ie_tipo_servico_p	= 'P') then
	select	ds_procedimento
	into	ds_retorno_w
	from	procedimento
	where	cd_procedimento		= cd_servico_p
	and	ie_origem_proced	= ie_origem_proced_p;
elsif	(ie_tipo_servico_p	= 'M') then
	select	ds_material
	into	ds_retorno_w
	from	pls_material
	where	cd_material_ops	= cd_servico_p;
end if;

return	ds_retorno_w;

end ptu_obter_proc_mat_desc;
/