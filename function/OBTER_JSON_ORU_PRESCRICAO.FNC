create or replace function obter_json_oru_prescricao(
nr_prescricao_p prescr_procedimento.nr_prescricao%type,
nr_seq_exame_p prescr_procedimento.nr_seq_exame%type,
nr_sequencia_p prescr_procedimento.nr_sequencia%type,
ie_status_atend_p prescr_procedimento.ie_status_atend%type)

return varchar2 is

nr_prescricao_w		varchar2(14);
visit_id_w			varchar2(10);
nr_seq_exame_w		number(10);
exam_date_w			varchar2(30);
dt_prescricao_w		varchar2(30);
dt_envio_w			varchar2(30);
exam_value_w		varchar2(4000);
unit_of_measure_w	varchar2(40);
nm_exame_w			varchar2(80);
exam_code_w			varchar2(20);
patient_id_w		varchar2(10);
patientname_w		varchar2(60);
patientbirthdate_w	varchar2(14);


begin

declare
	json_aux_w   philips_json;
	ds_message_w clob;

begin
	json_aux_w := philips_json();

	begin
		select b.cd_pessoa_fisica,
			   obter_nome_pf(b.cd_pessoa_fisica),
			   to_char(obter_data_nascto_pf(b.cd_pessoa_fisica),'YYYYMMDDHH24MISS'),
			   b.nr_atendimento,
			   b.nr_prescricao,
			   to_char(b.dt_prescricao ,'YYYYMMDDHH24MISS'),
			   elab.nm_exame,
			   to_char(elri.dt_coleta ,'YYYYMMDDHH24MISS'),
			   elab.nr_seq_exame,
			   lee.cd_exame_equip,
			   lab_obter_resultado_exame(elri.nr_seq_resultado,elri.nr_sequencia),
			   lab_obter_unidade_medida(elab.nr_seq_unid_med),
			   to_char(sysdate,'YYYYMMDDHH24MISS')
		into patient_id_w,
			 patientname_w,
			 patientbirthdate_w,
			 visit_id_w,
			 nr_prescricao_w,
			 dt_prescricao_w,
			 nm_exame_w,
			 exam_date_w,
			 nr_seq_exame_w,
			 exam_code_w,
			 exam_value_w,
			 unit_of_measure_w,
			 dt_envio_w
		from prescr_medica b
		inner join lab_exame_equip lee on lee.nr_seq_exame = nr_seq_exame_p
		inner join equipamento_lab el on el.cd_equipamento = lee.cd_equipamento and el.ds_sigla = 'EPIMED'
		inner join exame_lab_resultado elr on elr.nr_prescricao = nr_prescricao_p
		inner join exame_lab_result_item elri on elri.nr_seq_resultado = elr.nr_seq_resultado  and elri.nr_seq_prescr = nr_sequencia_p
		inner join material_exame_lab mel on mel.nr_sequencia = elri.nr_seq_material
		inner join exame_laboratorio elab on elab.nr_seq_exame = nr_seq_exame_p
		where ie_status_atend_p>=35
		and nr_prescricao_p = b.nr_prescricao
		and b.CD_SETOR_ATENDIMENTO in (
			select h.cd_setor_atendimento  
			from SETOR_ATENDIMENTO h 
			where h.IE_EPIMED = 'S' 
			and h.ie_situacao = 'A' 
			and h.cd_setor_atendimento = b.cd_setor_atendimento)
		and	(
			((select abs(nvl(max(j.nr_min_mov_epimed),0)) from parametro_Atendimento j) = 0) 
			or (obter_data_entrada_setor(b.nr_atendimento) >= sysdate - (select abs(nvl(max(j.nr_min_mov_epimed),0)) from parametro_Atendimento j)/1440))
		and  elri.nr_seq_material is not null;
		
		json_aux_w.put('PATIENTID', patient_id_w);
		json_aux_w.put('PATIENTNAME', patientname_w);
		json_aux_w.put('BIRTHDATE', patientbirthdate_w);
		json_aux_w.put('VISIT_ID', visit_id_w);
		json_aux_w.put('NR_PRESCRICAO', nr_prescricao_w);
		json_aux_w.put('DT_PRESCRICAO',dt_prescricao_w);
		json_aux_w.put('EXAM_CODE',exam_code_w);
		json_aux_w.put('EXAM_NAME',nm_exame_w);
		json_aux_w.put('EXAM_VALUE',exam_value_w);
		json_aux_w.put('UNIT_OF_MEASURE',unit_of_measure_w);
		json_aux_w.put('EXAM_DATE',exam_date_w);
		json_aux_w.put('SEND_DATE',dt_envio_w);
			
		dbms_lob.createtemporary(ds_message_w, TRUE);
		json_aux_w.to_clob(ds_message_w);
		
	exception 
		when no_data_found OR too_many_rows then
			ds_message_w := empty_clob();
	end;

	return ds_message_w;
	end;

end obter_json_oru_prescricao;

/