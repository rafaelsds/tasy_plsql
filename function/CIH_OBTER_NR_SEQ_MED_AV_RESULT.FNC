create or replace
function cih_obter_nr_seq_med_av_result (nr_seq_contato_p		number)
				return	number is

nr_sequencia_w	number(10);

begin

if(nr_seq_contato_p is not null) then

	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	med_avaliacao_paciente
	where	nr_seq_contato = nr_seq_contato_p;

end if;

return	nvl(nr_sequencia_w,0);

end cih_obter_nr_seq_med_av_result;
/