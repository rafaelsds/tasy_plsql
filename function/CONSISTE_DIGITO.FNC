create or replace
function consiste_digito(	ie_rotina_p		varchar2,
			cd_codigo_p		varchar2) 
			return boolean is

cd_codigo_w		varchar2(25);
ie_resultado_w		boolean;
cd_digito_w		number(02);
vl_soma_w		number(15,0);
cd_aih_w			varchar2(13);

begin

if	(ie_rotina_p = 'Modulo11') or
	(ie_rotina_p = 'Modulo11_BH') or
	(ie_rotina_p = 'Modulo10') or
	(ie_rotina_p = 'Modulo10X') or
	(ie_rotina_p = 'Modulo10D0') or
	(ie_rotina_p = 'AVIMED') then
	begin
	
	cd_digito_w		:= calcula_digito(ie_rotina_p, 
				substr(cd_codigo_p, 1,  length(cd_codigo_p) - 1));
				
	begin
	ie_resultado_w	:= (cd_digito_w = substr(cd_codigo_p, length(cd_codigo_p),1));
	exception when others then
		ie_resultado_w	:= false;
	end;
	
	end;
elsif	(ie_rotina_p = 'EAN13') then
	begin
	
	begin
	ie_resultado_w	:= (Length(cd_codigo_p) = 13) and
				(calcula_digito(ie_rotina_p, cd_codigo_p) = 
				substr(cd_codigo_p, 13,1)); 
				
	exception when others then
		ie_resultado_w	:= false;
	end;
	
	end;
elsif	(ie_rotina_p = 'IPE') then
	ie_resultado_w	:= consiste_digito_IPE(cd_codigo_p); 
elsif   (ie_rotina_p = 'Bloqueto') then
	begin
	ie_resultado_w	:= (calcula_digito(ie_rotina_p, cd_codigo_p) =
			   (substr(cd_codigo_p,5,1)));
	end;
elsif   (ie_rotina_p = 'AIH') then
	begin
	cd_aih_w		:= cd_codigo_p;
	cd_digito_w		:= calcula_digito(ie_rotina_p,cd_aih_w);
	ie_resultado_w	:= (cd_digito_w = substr(cd_aih_w, length(cd_aih_w),1));
	end;
elsif   (ie_rotina_p = 'Bloqueto_Dig') then
	begin
	ie_resultado_w	:= (calcula_digito('Modulo10', substr(cd_codigo_p,1,9)) =
			   (substr(cd_codigo_p,10,1))) and
			   (calcula_digito('Modulo10', substr(cd_codigo_p,11,10)) =
			   (substr(cd_codigo_p,21,1))) and
			   (calcula_digito('Modulo10', substr(cd_codigo_p,22,10)) =
			   (substr(cd_codigo_p,32,1)));
	end;
elsif	(upper(ie_rotina_p) = 'CARTAOSUS') then

	vl_soma_w	:=  (to_number(substr(cd_codigo_p, 1, 1))  * 15) +
	        	(to_number(substr(cd_codigo_p, 2, 1)) * 14) +
		        (to_number(substr(cd_codigo_p, 3, 1)) * 13) +
	        	(to_number(substr(cd_codigo_p, 4, 1)) * 12) +
	        	(to_number(substr(cd_codigo_p, 5, 1)) * 11) +
		        (to_number(substr(cd_codigo_p, 6, 1)) * 10) +
		        (to_number(substr(cd_codigo_p, 7, 1)) * 9) +
	        	(to_number(substr(cd_codigo_p, 8, 1)) * 8) +
		        (to_number(substr(cd_codigo_p, 9, 1)) * 7) +
		        (to_number(substr(cd_codigo_p, 10, 1)) * 6) +
        		(to_number(substr(cd_codigo_p, 11, 1)) * 5) +
		        (to_number(substr(cd_codigo_p, 12, 1)) * 4) +
		        (to_number(substr(cd_codigo_p, 13, 1)) * 3) +
        		(to_number(substr(cd_codigo_p, 14, 1)) * 2) +
	        	(to_number(substr(cd_codigo_p, 15, 1)) * 1);

	if	((vl_soma_w mod 11) = 0) and
		(to_number(cd_codigo_p) <> 0) then
		ie_resultado_w		:= true;
	else
		ie_resultado_w		:= false;
	end if;

/* Felipe - OS68137 - 13/09/2007 */
elsif	(upper(ie_rotina_p) = 'FIOPREV') then
	begin
	cd_codigo_w	:= substr(cd_codigo_p,1,length(cd_codigo_p)- 1);
	cd_digito_w	:= calcula_digito(ie_rotina_p,cd_codigo_w);
	ie_resultado_w	:= (cd_digito_w = substr(cd_codigo_p, length(cd_codigo_p),1));
	end;
elsif	(upper(ie_rotina_p) = 'PETROBRAS') then
	cd_digito_w := calcula_digito_petrobras(cd_codigo_p);
	ie_resultado_w :=(cd_digito_w = substr(cd_codigo_p,7,1));
elsif	(upper(ie_rotina_p) = 'CABESP') then
	cd_digito_w := Calcula_Digito(upper(ie_rotina_p), substr(cd_codigo_p, 1, length(cd_codigo_p)-2));
	ie_resultado_w :=(cd_digito_w = substr(cd_codigo_p,-2,2));
end if;

return ie_resultado_w;

end consiste_digito;
/