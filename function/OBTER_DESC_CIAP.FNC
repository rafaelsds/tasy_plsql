create or replace
function obter_desc_ciap(
	cd_ciap_p	varchar2)
	return varchar2 is

ds_retorno_w	varchar2(255);

begin

if (cd_ciap_p is not null) then
	select	substr(ds_ciap,1,255)
	into	ds_retorno_w
	from	problema_ciap
	where	cd_ciap = cd_ciap_p
	and	ie_situacao = 'A';
end if;

return	ds_retorno_w;

end obter_desc_ciap;
/
