create or replace
function obter_dados_inconsist_sol(	nr_prescricao_p	number, 
					nr_seq_material_p number)
					return varchar2 is

ds_retorno_w			varchar2(4000);
ds_inconsistencia_w		varchar2(4000);
ds_inconsistencia_farm_w	varchar2(255); 

Cursor C01 is
	SELECT	SUBSTR(obter_dados_inconsist_farm(nr_seq_inconsistencia,'D'),1,255) ds_inconsistencia
	FROM   	prescr_material_incon_farm
	WHERE  	nr_prescricao 	= nr_prescricao_p
	AND	nr_seq_material	= nr_seq_material_p
	and	coalesce(ie_situacao, 'A') = 'A' 
	ORDER  BY  ds_inconsistencia;

begin


if   ((nr_seq_material_p > 0) and 
      (nr_prescricao_p > 0)) then
	open c01;
	loop
	fetch c01 into
		ds_inconsistencia_w;
	exit when c01%NOTFOUND;
		begin
		ds_retorno_w	:= ds_retorno_w||' - '||ds_inconsistencia_w;
		end;
	end loop;
	close c01;
end if;

begin

	select	substr(obter_dados_inconsist_farm(max(a.nr_seq_inconsistencia),'D'),1,255) 
	into	ds_inconsistencia_farm_w	
	from 	prescr_material a
	where 	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_seq_material_p
	and 	ie_agrupador = 4;


exception
when others then
	ds_inconsistencia_farm_w	:= null;	
end;	

if (ds_inconsistencia_farm_w is not null) then	
	ds_retorno_w	:= ds_retorno_w||' '||ds_inconsistencia_farm_w;
end if;

return	ds_retorno_w;

end obter_dados_inconsist_sol;
/