CREATE OR REPLACE FUNCTION OBTER_TIPO_DOC_APROV     (CD_DESC_NODE IN NUMBER, 
                                                    NR_SEQ_ITEM_PRONT_P IN NUMBER) RETURN VARCHAR2 IS 

DS_TIPO_COLUNA_W VARCHAR2(50);
DS_TIPO_TEMPLATE VARCHAR2(50);


BEGIN
    SELECT 
        obter_desc_exp_idioma(CD_DESC_NODE,wheb_usuario_pck.get_nr_seq_idioma,null)
    INTO
        DS_TIPO_COLUNA_W
    FROM DUAL;
    
    IF (CD_DESC_NODE = 307982) THEN
        IF(NR_SEQ_ITEM_PRONT_P = 5) THEN
            SELECT 
                obter_desc_exp_idioma(792013,wheb_usuario_pck.get_nr_seq_idioma,null)
            INTO
                DS_TIPO_TEMPLATE
            FROM DUAL;
            DS_TIPO_COLUNA_W := DS_TIPO_TEMPLATE || ' (' || DS_TIPO_COLUNA_W || ')';
        END IF;
        IF(NR_SEQ_ITEM_PRONT_P = 7) THEN
            SELECT 
                obter_desc_exp_idioma(284113,wheb_usuario_pck.get_nr_seq_idioma,null)
            INTO
                DS_TIPO_TEMPLATE
            FROM DUAL;
            DS_TIPO_COLUNA_W := DS_TIPO_TEMPLATE || ' (' || DS_TIPO_COLUNA_W || ')';
        END IF;
    END IF;

    RETURN INITCAP(DS_TIPO_COLUNA_W);
END OBTER_TIPO_DOC_APROV;
/
