create or replace
function obter_episodio_atend(nr_atendimento_p           number)
                        return varchar2 is

nr_seq_episodio_w               number(10);
nr_seq_episodio_ww		episodio_paciente.nr_episodio%type;

begin

select  nvl(max(nr_seq_episodio),0)
into    nr_seq_episodio_w
from    atendimento_paciente
where   nr_atendimento = nr_atendimento_p;

select  nvl(max(nr_episodio),0)
into    nr_seq_episodio_ww
from    episodio_paciente
where   nr_sequencia = nr_seq_episodio_w;


return  nr_seq_episodio_ww;

end obter_episodio_atend;
/