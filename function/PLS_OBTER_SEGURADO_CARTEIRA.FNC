create or replace
function pls_obter_segurado_carteira
			(	cd_usuario_plano_p		pls_segurado_carteira.cd_usuario_plano%type,
				cd_estabelecimento_p		number)
				return number is

retorno_w			pls_segurado.nr_sequencia%type;
cd_usuario_plano_w		pls_segurado_carteira.cd_usuario_plano%type;

begin
select	replace(replace(cd_usuario_plano_p,'_',''),'.','')
into	cd_usuario_plano_w
from	dual;

select	max(a.nr_sequencia)
into	retorno_w
from	pls_segurado		a,
	pls_segurado_carteira	b
where	b.nr_seq_segurado	= a.nr_sequencia
and	b.cd_estabelecimento	= cd_estabelecimento_p
and	b.cd_usuario_plano	= cd_usuario_plano_w;

return	retorno_w;

end pls_obter_segurado_carteira;
/
