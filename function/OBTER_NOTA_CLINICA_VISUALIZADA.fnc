create or replace function OBTER_NOTA_CLINICA_VISUALIZADA (	 nr_atendimento_p number,
                                                            nm_usuario_p varchar2
                                       )
                    return varchar2 is

cd_evolucao_w               evolucao_paciente.cd_evolucao%type;
nr_notas_nao_visualizadas_w number(10) := 0;
qt_total_w                  number(10) := 0;

cursor c01 is
select b.cd_evolucao
from   evolucao_paciente b
where  b.dt_liberacao is not null
and trunc(b.DT_ATUALIZACAO) >= trunc(sysdate - 3)
and    not exists (select 1 
                   from evo_pac_int_visualizado a 
                   where b.cd_evolucao = a.nr_seq_evo_paciente
                   and a.nm_usuario          = nm_usuario_p)
and     b.nr_atendimento = nr_atendimento_p;

begin

open C01;
	loop
	   fetch C01 into
		 cd_evolucao_w;
	   exit when C01%notfound;
  
     qt_total_w := qt_total_w + 1;
    
	end loop;
close C01;  

return nvl(qt_total_w, 0);

end OBTER_NOTA_CLINICA_VISUALIZADA;
/
