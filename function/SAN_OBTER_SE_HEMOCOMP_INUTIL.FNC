create or replace
function san_obter_se_hemocomp_inutil
			(	nr_seq_reserva_p	number,
				nr_seq_producao_p	varchar2)
				return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se existe hemocomponente inativo para a reserva ou para a reserva de produ��o.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(255)	:= 'N';
ds_lista_w              varchar2(100);
nr_pos_virgula_w	number(10,0);
nr_seq_producao_w		number(10,0);

begin
        
if	(nr_seq_reserva_p is not null) then
	select	decode(count(1),0,'N','S')
	into	ds_retorno_w
	from	san_producao		a,
		san_reserva_prod	b
	where	a.nr_sequencia		= b.nr_seq_producao
	and	b.nr_seq_reserva	= nr_seq_reserva_p
	and	a.nr_seq_inutil is not null;
elsif	(nr_seq_producao_p is not null) then

        ds_lista_w :=  nr_seq_producao_p;	
        while  (ds_lista_w is not null and ds_retorno_w <> 'S') loop
	begin	
		nr_pos_virgula_w := instr(ds_lista_w,',');		
		if	(nvl(nr_pos_virgula_w,0) > 0) then
			begin
			nr_seq_producao_w		:= substr(ds_lista_w,0,nr_pos_virgula_w-1);
			ds_lista_w	:= substr(ds_lista_w,nr_pos_virgula_w+1,length(ds_lista_w));												
			end;
		else
			begin			
			nr_seq_producao_w		:= to_number(ds_lista_w);
			ds_lista_w	:= null;			
			end;
		end if;			
		if	(nvl(nr_seq_producao_w,0) > 0) then
			select	decode(count(1),0,'N','S')
			into	ds_retorno_w
			from	san_producao	a
			where	a.nr_sequencia	= nr_seq_producao_w
			and	a.nr_seq_inutil is not null;
		end if;			
	end;
	end loop; 	
end if;

return ds_retorno_w;

end san_obter_se_hemocomp_inutil;
/