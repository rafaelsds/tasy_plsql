Create or Replace
FUNCTION OBTER_DADOS_ANALISE_ORT(	nr_seq_analise_p	number,
					ie_opcao_p		varchar2)
					RETURN			VarChar2 IS

/*	ie_opcao_p

DCP	Descri��o da chave prim�ria
DUC	Descri��o do usu�rio cria��o
DC	Descri��o do campo
CP	Chave prim�ria

	ie_chave_primaria_w	(iniciar e terminar com ponto-e-v�rgula)

quando for um m�todo:		;M; + nome do form + m�todo pai + ocorr�ncia + m�todo
quando for um componente:	;C; + nome do form + classe do componente + nome do componente
quando for uma tabela:		;T; + identificador da tabela (definir sempre um identificador diferente dos j� existentes) + chave prim�ria da tabela (se for composta, separar com ponto-e-v�rgula)

*/

ds_retorno_w			varchar2(255);
ds_chave_primaria_w		varchar2(255);
ds_usuario_criacao_w		varchar2(255);
ie_chave_primaria_w		varchar2(255);

nm_tabela_origem_w		varchar2(50);
nm_tabela_w			varchar2(50);
nm_atributo_w			varchar2(50);
nr_seq_visao_w			number(10);
nr_seq_objeto_w			number(10);
nr_seq_obj_filtro_w		number(10);
nr_seq_cor_w			number(10);
cd_dominio_w			number(5);
vl_dominio_w			varchar2(15);
cd_funcao_w			number(5);
nr_seq_parametro_w		number(5);
ds_funcao_w			varchar2(80);
nr_seq_origem_w			number(10);
ds_usuario_nrec_w		varchar2(255);
nm_usuario_nrec_w		varchar2(15);
nr_seq_relatorio_w		number(10);
nr_seq_campo_relat_w		number(10);
nr_seq_param_relat_w		number(10);

cd_classif_relat_w		varchar2(4);
cd_relatorio_w			number(5);
nm_dominio_w			varchar2(80);
ds_banda_w			varchar2(30);
ds_campo_relat_w		varchar2(80);
cd_parametro_w			varchar2(50);

ds_campo_w			varchar2(255);
ds_metodo_w			varchar2(255);
ds_classe_objeto_w		varchar2(255);
ds_form_w			varchar2(10);
ds_nome_objeto_w		varchar2(255);
nr_parametro_w			number(10);
nr_ocorrencia_w			number(10);

nr_seq_ind_gestao_w		analise_ortografica.nr_seq_ind_gestao%type;
nr_seq_ind_gestao_atrib_w	analise_ortografica.nr_seq_ind_gestao_atrib%type;
nr_seq_subind_gestao_w		analise_ortografica.nr_seq_subind_gestao%type;
nr_seq_subind_gestao_atrib_w	analise_ortografica.nr_seq_subind_gestao_atrib%type;
ds_indicador_w			indicador_gestao.ds_indicador%type;
ds_ind_gestao_atrib_w		varchar2(255);
ds_subindicador_w		subindicador_gestao.ds_subindicador%type;
ds_grid_w			subindicador_gestao_atrib.ds_grid%type;
ds_tipo_documento_w		valor_dominio.ds_valor_dominio%type;

cd_interface_w			interface.cd_interface%type;
ds_interface_w			interface.ds_interface%type;

nr_seq_referencia_w		analise_ortografica.nr_seq_referencia%type;

cd_tipo_lote_contabil_w		analise_ortografica.cd_tipo_lote_contabil%type;
ds_tipo_lote_contabil_w		tipo_lote_contabil.ds_tipo_lote_contabil%type;

cd_expressao_w			dic_expressao.cd_expressao%type;
ds_expressao_br_w		dic_expressao.ds_expressao_br%type;

BEGIN

if	(nr_seq_analise_p	is not null) then

	select	a.nm_tabela_origem,
		a.nm_tabela,
		a.nm_atributo,
		a.nr_seq_visao,
		decode(nvl(a.nr_seq_objeto,0),0,b.nr_seq_objeto,a.nr_seq_objeto) nr_seq_objeto,
		a.nr_seq_obj_filtro,
		a.nr_seq_cor,
		a.cd_dominio,
		a.vl_dominio,
		a.cd_funcao,
		a.nr_seq_parametro,
		substr(obter_nome_funcao(a.cd_funcao),1,80) ds_funcao,
		a.nr_seq_origem,
		a.nr_seq_relatorio,
		a.nr_seq_campo_relat,
		a.nr_seq_param_relat,
		a.ds_campo,
		a.ds_metodo,
		a.ds_classe_objeto,
		a.ds_form,
		a.ds_nome_objeto,
		a.nr_parametro,
		a.nr_ocorrencia,
		a.nr_seq_ind_gestao,
		a.nr_seq_ind_gestao_atrib,
		a.nr_seq_subind_gestao,
		a.nr_seq_subind_gestao_atrib,
		a.cd_interface,
		a.nr_seq_referencia,
		a.cd_tipo_lote_contabil,
		a.cd_expressao
	into	nm_tabela_origem_w,
		nm_tabela_w,
		nm_atributo_w,
		nr_seq_visao_w,
		nr_seq_objeto_w,
		nr_seq_obj_filtro_w,
		nr_seq_cor_w,
		cd_dominio_w,
		vl_dominio_w,
		cd_funcao_w,
		nr_seq_parametro_w,
		ds_funcao_w,
		nr_seq_origem_w,
		nr_seq_relatorio_w,
		nr_seq_campo_relat_w,
		nr_seq_param_relat_w,
		ds_campo_w,
		ds_metodo_w,
		ds_classe_objeto_w,
		ds_form_w,
		ds_nome_objeto_w,
		nr_parametro_w,
		nr_ocorrencia_w,
		nr_seq_ind_gestao_w,
		nr_seq_ind_gestao_atrib_w,
		nr_seq_subind_gestao_w,
		nr_seq_subind_gestao_atrib_w,
		cd_interface_w,
		nr_seq_referencia_w,
		cd_tipo_lote_contabil_w,
		cd_expressao_w
	from	dic_objeto_filtro b,
		analise_ortografica a
	where	a.nr_seq_obj_filtro	= b.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_analise_p;

	if	(ie_opcao_p	= 'DC') then

		if	(nm_tabela_origem_w	is null) then

			ds_retorno_w	:= ds_campo_w;

		else

			select	nvl(a.ds_label_grid,a.ds_label)
			into	ds_retorno_w
			from	tabela_atributo a
			where	a.nm_atributo	= ds_campo_w
			and	a.nm_tabela	= nm_tabela_origem_w;

		end if;

	elsif	(ie_opcao_p	in ('DCP','DUC','CP')) then

		if	(nm_tabela_origem_w	= 'TABELA_ATRIBUTO') then

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817766) /*Tabela:*/ || ' ' || nm_tabela_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817767) /* Atributo:*/ || ' ' || nm_atributo_w;
			ie_chave_primaria_w	:= ';T;TA;' || nm_tabela_w || ';' || nm_atributo_w || ';';

			select	max(a.nm_usuario_nrec)
			into	nm_usuario_nrec_w
			from	tabela_atributo a
			where	a.nm_atributo	= nm_atributo_w
			and	a.nm_tabela	= nm_tabela_w;

		elsif	(nm_tabela_origem_w	= 'TABELA_VISAO_ATRIBUTO') then

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817766) /*Tabela:*/ || ' ' || nm_tabela_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817768) /*Vis�o:*/ || ' ' || nr_seq_visao_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817767) /* Atributo:*/ || ' ' || nm_atributo_w;
			ie_chave_primaria_w	:= ';T;TVA;' || ';' || nr_seq_visao_w || ';' || nm_atributo_w || ';';

			/* busca o usu�rio da tabela, no caso do campo da vis�o ter sido copiado da tabela */
			select	max(d.nm_usuario_nrec)
			into	nm_usuario_nrec_w
			from	tabela_atributo d,
				tabela_visao c,
				tabela_visao_atributo b,
				analise_ortografica a
			where	d.nm_atributo			= b.nm_atributo
			and	c.nm_tabela			= d.nm_tabela
			and	b.nr_sequencia			= c.nr_sequencia
			and	a.nm_atributo			= b.nm_atributo
			and	a.nr_seq_visao			= b.nr_sequencia
			and	a.nr_sequencia			= nr_seq_origem_w;

		elsif	(nm_tabela_origem_w	= 'DIC_OBJETO') then

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(90775, 'NM_OBJETO=' || nr_seq_objeto_w); /*Objeto:*/
			ie_chave_primaria_w	:= ';T;DO;' || nr_seq_objeto_w || ';';

			select	max(a.nm_usuario_nrec)
			into	nm_usuario_nrec_w
			from	dic_objeto a
			where	a.nr_sequencia	= nr_seq_objeto_w;

		elsif	(nm_tabela_origem_w	= 'DIC_OBJETO_FILTRO') then

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(90775, 'NM_OBJETO=' || nr_seq_objeto_w) /*Objeto:*/ || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817769) /*Objeto filtro:*/ || ' ' || nr_seq_obj_filtro_w;
			ie_chave_primaria_w	:= ';T;DOF;' || nr_seq_objeto_w || ';' || nr_seq_obj_filtro_w || ';';

			select	max(a.nm_usuario_nrec)
			into	nm_usuario_nrec_w
			from	dic_objeto_filtro a
			where	a.nr_sequencia	= nr_seq_obj_filtro_w;

		elsif	(nm_tabela_origem_w	= 'TASY_PADRAO_COR') then

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817771) /*Cor:*/ || ' ' || nr_seq_cor_w;
			ie_chave_primaria_w	:= ';T;TPC;' || nr_seq_cor_w || ';';

			select	max(a.nm_usuario_nrec)
			into	nm_usuario_nrec_w
			from	tasy_padrao_cor a
			where	a.nr_sequencia	= nr_seq_cor_w;

		elsif	(nm_tabela_origem_w	= 'VALOR_DOMINIO') then

			select	max(a.nm_dominio)
			into	nm_dominio_w
			from	dominio a
			where	a.cd_dominio	= cd_dominio_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817772) /*Dom�nio:*/ || ' ' || cd_dominio_w || ' - ' || nm_dominio_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817773) /*Valor:*/ || ' ' || vl_dominio_w;
			ie_chave_primaria_w	:= ';T;VD;' || cd_dominio_w || ';' || vl_dominio_w || ';';

		elsif	(nm_tabela_origem_w	= 'FUNCAO_PARAMETRO') then

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817774) /*Fun��o:*/ || ' ' || cd_funcao_w || ' - ' || ds_funcao_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817775) /*Par�metro:*/ || ' ' || nr_seq_parametro_w;
			ie_chave_primaria_w	:= ';T;FP;' || cd_funcao_w || ';' || nr_seq_parametro_w || ';';

			select	max(a.nm_usuario_nrec)
			into	nm_usuario_nrec_w
			from	funcao_parametro a
			where	a.nr_sequencia	= nr_seq_parametro_w
			and	a.cd_funcao	= cd_funcao_w;

		elsif	(nm_tabela_origem_w	= 'RELATORIO') then

			select	max(a.cd_relatorio),
				max(a.cd_classif_relat)
			into	cd_relatorio_w,
				cd_classif_relat_w
			from	relatorio a
			where	a.nr_sequencia	= nr_seq_relatorio_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817776) /*Relat�rio:*/ || ' ' || cd_classif_relat_w || to_char(cd_relatorio_w,'FM00000');
			ie_chave_primaria_w	:= ';T;R;' || nr_seq_relatorio_w || ';';

		elsif	(nm_tabela_origem_w	= 'BANDA_RELAT_CAMPO') then

			select	max(c.cd_relatorio),
				max(c.cd_classif_relat),
				max(b.ds_banda),
				max(a.ds_campo)
			into	cd_relatorio_w,
				cd_classif_relat_w,
				ds_banda_w,
				ds_campo_relat_w
			from	relatorio c,
				banda_relatorio b,
				banda_relat_campo a
			where	b.nr_seq_relatorio	= c.nr_sequencia
			and	b.nr_seq_relatorio	= nr_seq_relatorio_w
			and	a.nr_seq_banda		= b.nr_sequencia
			and	a.nr_sequencia		= nr_seq_campo_relat_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817776) /*Relat�rio:*/ || ' ' || cd_classif_relat_w || to_char(cd_relatorio_w,'FM00000') || '   '|| 
									WHEB_MENSAGEM_PCK.get_texto(817777) /*Banda*/ || ': ' || ds_banda_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817778) /*Campo:*/ || ' ' || ds_campo_relat_w;
			ie_chave_primaria_w	:= ';T;BRC;' || nr_seq_relatorio_w || ';' || nr_seq_campo_relat_w || ';';

		elsif	(nm_tabela_origem_w	= 'RELATORIO_PARAMETRO') then

			select	max(a.cd_parametro),
				max(b.cd_relatorio),
				max(b.cd_classif_relat)
			into	cd_parametro_w,
				cd_relatorio_w,
				cd_classif_relat_w
			from	relatorio b,
				relatorio_parametro a
			where	b.nr_sequencia		= nr_seq_relatorio_w
			and	a.nr_seq_relatorio	= b.nr_sequencia
			and	a.nr_sequencia		= nr_seq_param_relat_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817776) /*Relat�rio:*/ || ' ' || cd_classif_relat_w || to_char(cd_relatorio_w,'FM00000') || '   ' ||
									WHEB_MENSAGEM_PCK.get_texto(817775) /*Par�metro:*/ || ' ' || cd_parametro_w;
			ie_chave_primaria_w	:= ';T;RP;' || nr_seq_relatorio_w || ';' || nr_seq_param_relat_w || ';';

		elsif	(nm_tabela_origem_w	= 'DOMINIO') then

			select	max(a.nm_dominio)
			into	nm_dominio_w
			from	dominio a
			where	a.cd_dominio	= cd_dominio_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817772) /*Dom�nio:*/ || ' ' || cd_dominio_w || ' - ' || nm_dominio_w;
			ie_chave_primaria_w	:= ';T;D;' || cd_dominio_w || ';';

		elsif	(nm_tabela_origem_w	= 'INDICADOR_GESTAO') then

			select	max(a.ds_indicador),
				max(a.nm_usuario_nrec)
			into	ds_indicador_w,
				nm_usuario_nrec_w
			from	indicador_gestao a
			where	a.nr_sequencia	= nr_seq_ind_gestao_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817779) /*Indicador de gest�o:*/ || ' ' || nr_seq_ind_gestao_w || ' - ' || ds_indicador_w;
			ie_chave_primaria_w	:= ';T;IG;' || nr_seq_ind_gestao_w || ';';

		elsif	(nm_tabela_origem_w	= 'INDICADOR_GESTAO_ATRIB') then

			select	max(nvl(a.ds_dimensao,nvl(a.ds_data,a.ds_informacao))),
				max(b.ds_indicador)
			into	ds_ind_gestao_atrib_w,
				ds_indicador_w
			from	indicador_gestao b,
				indicador_gestao_atrib a
			where	a.nr_seq_ind_gestao	= b.nr_sequencia
			and	a.nr_sequencia		= nr_seq_ind_gestao_atrib_w
			and	a.nr_seq_ind_gestao	= nr_seq_ind_gestao_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817779) /*Indicador de gest�o:*/ || ' ' || nr_seq_ind_gestao_w || ' - ' || ds_indicador_w || '   ' ||
									WHEB_MENSAGEM_PCK.get_texto(817767) /*Atributo:*/ || ' ' || nr_seq_ind_gestao_atrib_w || ' - ' || ds_ind_gestao_atrib_w;
			ie_chave_primaria_w	:= ';T;IGA;' || nr_seq_ind_gestao_w || ';' || nr_seq_ind_gestao_atrib_w || ';';

		elsif	(nm_tabela_origem_w	= 'SUBINDICADOR_GESTAO') then

			select	max(a.ds_subindicador),
				max(a.nm_usuario_nrec)
			into	ds_subindicador_w,
				nm_usuario_nrec_w
			from	subindicador_gestao a
			where	a.nr_sequencia		= nr_seq_subind_gestao_w
			and	a.nr_seq_indicador	= nr_seq_ind_gestao_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817779) /*Indicador de gest�o:*/ || ' ' || nr_seq_ind_gestao_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817780) /*Subindicador:*/ || ' ' || nr_seq_subind_gestao_w || ' - ' || ds_subindicador_w;
			ie_chave_primaria_w	:= ';T;SIG;' || nr_seq_ind_gestao_w || ';' || nr_seq_subind_gestao_w || ';';

		elsif	(nm_tabela_origem_w	= 'SUBINDICADOR_GESTAO_ATRIB') then

			select	max(a.ds_subindicador),
				max(b.ds_grid),
				max(b.nm_usuario_nrec)
			into	ds_subindicador_w,
				ds_grid_w,
				nm_usuario_nrec_w
			from	subindicador_gestao_atrib b,
				subindicador_gestao a
			where	b.nr_sequencia	= nr_seq_subind_gestao_atrib_w
			and	a.nr_sequencia	= b.nr_seq_subindicador
			and	a.nr_sequencia	= nr_seq_subind_gestao_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817779) /*Indicador de gest�o:*/ || ' ' || nr_seq_ind_gestao_w || '   ' ||
									WHEB_MENSAGEM_PCK.get_texto(817780) /*Subindicador:*/ || ' ' || nr_seq_subind_gestao_w || ' - ' || ds_subindicador_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817767) /*Atributo:*/ || ' ' || nr_seq_subind_gestao_atrib_w || ' - ' || ds_grid_w;
			ie_chave_primaria_w	:= ';T;SIGA;' || nr_seq_subind_gestao_w || ';' || nr_seq_subind_gestao_atrib_w || ';';

		elsif	(nm_tabela_origem_w	= 'INTERFACE') then

			select	max(a.ds_interface),
				max(a.nm_usuario_nrec)
			into	ds_interface_w,
				nm_usuario_nrec_w
			from	interface a
			where	a.cd_interface	= cd_interface_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817781) /*Interface:*/ || ' ' || cd_interface_w || ' - ' || ds_interface_w;
			ie_chave_primaria_w	:= ';T;I;' || cd_interface_w || ';';

		elsif	(nm_tabela_origem_w	= 'FUNCAO') then

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817774) /*Fun��o:*/ || ' ' || cd_funcao_w || ' - ' || ds_funcao_w;
			ie_chave_primaria_w	:= ';T;F;' || cd_funcao_w || ';';

			select	max(a.nm_usuario_nrec)
			into	nm_usuario_nrec_w
			from	funcao a
			where	a.cd_funcao	= cd_funcao_w;

		elsif	(nm_tabela_origem_w	= 'TIPO_LOTE_CONTABIL') then

			select	max(a.ds_tipo_lote_contabil),
				max(a.nm_usuario_nrec)
			into	ds_tipo_lote_contabil_w,
				nm_usuario_nrec_w
			from	tipo_lote_contabil a
			where	a.cd_tipo_lote_contabil	= cd_tipo_lote_contabil_w;

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817782) /*Tipo de lote cont�bil:*/ || ' ' || cd_tipo_lote_contabil_w || ' - ' || ds_tipo_lote_contabil_w;
			ie_chave_primaria_w	:= ';T;TLC;' || cd_tipo_lote_contabil_w || ';';

		elsif	(nm_tabela_origem_w	= 'DIC_EXPRESSAO') then
		
			select	max(a.ds_expressao_br),
				max(a.nm_usuario)
			into	ds_expressao_br_w,
				nm_usuario_nrec_w
			from	dic_expressao a
			where	a.cd_expressao	= cd_expressao_w;
			
			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817783) /*Dic Express�o:*/ || ' ' || cd_expressao_w;
			ie_chave_primaria_w	:= ';T;DE;' || cd_expressao_w || ';';
		
		elsif	(nm_tabela_origem_w	= 'ANVISA_FORMA_FORMACEUTICA') or
			(nm_tabela_origem_w	= 'ANVISA_VIA_ADMINISTRACAO') or
			(nm_tabela_origem_w	= 'CLASSIFICACAO_PARAMETRO') or
			(nm_tabela_origem_w	= 'CONSISTENCIA_LIB_PEPO') or
			(nm_tabela_origem_w	= 'EHR_ARQUETIPO') or
			(nm_tabela_origem_w	= 'EHR_CEFALOCAUDAL') or
			(nm_tabela_origem_w	= 'EHR_DOENCA') or
			(nm_tabela_origem_w	= 'EHR_ELEMENTO') or
			(nm_tabela_origem_w	= 'EHR_ELEMENTO_VISUAL') or
			(nm_tabela_origem_w	= 'EHR_ENTIDADE_TASY') or
			(nm_tabela_origem_w	= 'EHR_ESPECIALIDADE') or
			(nm_tabela_origem_w	= 'EHR_GRUPO_ELEMENTO') or
			(nm_tabela_origem_w	= 'EHR_SUBGRUPO_ELEMENTO') or
			(nm_tabela_origem_w	= 'EHR_UNIDADE_MEDIDA') or
			(nm_tabela_origem_w	= 'FANEP_ITEM') or
			(nm_tabela_origem_w	= 'FICHA_FINANC_ITEM') or
			(nm_tabela_origem_w	= 'INCONSISTENCIA') or
			(nm_tabela_origem_w	= 'INCONSISTENCIA_ESCRIT') or
			(nm_tabela_origem_w	= 'MAN_SEVERIDADE') or
			(nm_tabela_origem_w	= 'OFT_ACAO') or
			(nm_tabela_origem_w	= 'OFTALMOLOGIA_ITEM') or
			(nm_tabela_origem_w	= 'PEP_ACAO') or
			(nm_tabela_origem_w	= 'PEPO_ACAO') or
			(nm_tabela_origem_w	= 'PEPO_ITEM') or
			(nm_tabela_origem_w	= 'PERGUNTA_RECUP_SENHA') or
			(nm_tabela_origem_w	= 'PRONTUARIO_ITEM') or
			(nm_tabela_origem_w	= 'PRONTUARIO_PASTA') or
			(nm_tabela_origem_w	= 'REGRA_CONSISTE_ONC') or
			(nm_tabela_origem_w	= 'REGRA_CONSISTE_PRESCR') or
			(nm_tabela_origem_w	= 'REGRA_CONSISTE_SAE') or
			(nm_tabela_origem_w	= 'SAEP_ITEM') or
			(nm_tabela_origem_w	= 'SAME_OPERACAO') or
			(nm_tabela_origem_w	= 'TIPO_AMPUTACAO') or
			(nm_tabela_origem_w	= 'TIPO_LOCALIZAR') or
			(nm_tabela_origem_w	= 'TIPO_LOCALIZAR_ATRIBUTO') or
			(nm_tabela_origem_w	= 'DIC_EXPRESSAO') then

			obter_valor_dinamico_4000_bv(	'select max(nm_usuario_nrec)' ||
							' from ' || nm_tabela_origem_w ||
							' where nr_sequencia = :nr_seq_referencia',
							'nr_seq_referencia=' || nr_seq_referencia_w || ';',
							nm_usuario_nrec_w);

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817784) /*Sequ�ncia:*/ || ' ' || nr_seq_referencia_w;
			ie_chave_primaria_w	:= ';T;' || nm_tabela_origem_w || ';' || nr_seq_referencia_w || ';';

		elsif	(ds_metodo_w		is not null) then

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817785) /*Form:*/ || ' ' || ds_form_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817786) /*M�todo pai:*/ || ' ' || ds_metodo_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817788) /*M�todo origem:*/ || ' ' || ds_campo_w;
			ie_chave_primaria_w	:= ';M;' || ds_form_w || ';' || ds_metodo_w || ';' || nr_ocorrencia_w || ';' || ds_campo_w || ';';

		else

			ds_chave_primaria_w	:= WHEB_MENSAGEM_PCK.get_texto(817785) /*Form:*/ || ' ' || ds_form_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817787) /*Classe:*/ || ' ' || ds_classe_objeto_w || '   ' || 
									WHEB_MENSAGEM_PCK.get_texto(817789) /*Componente:*/ || ' ' || ds_nome_objeto_w;
			ie_chave_primaria_w	:= ';C;' || ds_form_w || ';' || ds_classe_objeto_w || ';' || ds_nome_objeto_w || ';';

		end if;

		if	(ie_opcao_p	= 'DCP') then
			ds_retorno_w	:= ds_chave_primaria_w;
		elsif	(ie_opcao_p	= 'DUC') then

			if	(nm_usuario_nrec_w	is not null) then

				select	substr(nvl(nvl(obter_nome_usuario(nm_usuario_nrec_w),obter_nome_pf_pj(max(a.cd_pessoa_fisica),null)),WHEB_MENSAGEM_PCK.get_texto(817790) /*'Usu�rio n�o cadastrado'*/ ),1,255) || ' (' || nm_usuario_nrec_w || ')' nm_usuario_criacao
				into	ds_usuario_criacao_w
				from	usuario a
				where	a.nm_usuario	= nm_usuario_nrec_w;

				if	(ds_usuario_criacao_w	is null) then

					ds_usuario_criacao_w	:= WHEB_MENSAGEM_PCK.get_texto(817790) /*'Usu�rio n�o cadastrado*/ || ' (' || nm_usuario_nrec_w || ')';

				end if;

			end if;

			ds_retorno_w	:= ds_usuario_criacao_w;

		elsif	(ie_opcao_p	= 'CP') then
			ds_retorno_w	:= ie_chave_primaria_w;
		end if;

	end if;

end if;

RETURN ds_retorno_w;

END OBTER_DADOS_ANALISE_ORT;
/