CREATE OR REPLACE
FUNCTION Obter_AtePacu_Data(		
					nr_atendimento_p		Number,
/*					cd_setor_atendimento_p	Number,*/
					ie_opcao_p			Varchar2,
					dt_referencia_p		date)
					RETURN Number IS

/* UNIDADE DO PACIENTE				*/
/*	P = Primeira Unidade          		*/
/* 	A = Unidade Atual  			*/
/*	PI = Primeira Unidade Interna��o	*/
/*	IA = Interna��o Atual			*/

nr_seq_atepacu_w			Number(10,0);

BEGIN

if	(ie_opcao_p = 'P') then
	select	nvl(min(nr_seq_interno),0)
	into	nr_seq_atepacu_w
	from 	atend_paciente_unidade a
	where	a.nr_atendimento 		= nr_atendimento_p
/*	  and  a.cd_setor_atendimento	= nvl(cd_setor_atendimento_p, a.cd_setor_atendimento)*/
	  and 	dt_entrada_unidade	= 
		(select min(dt_entrada_unidade)
		from atend_paciente_unidade b
		where nr_atendimento 		= nr_atendimento_p);
elsif	(ie_opcao_p = 'A') then
	select	nvl(max(nr_seq_interno),0)
	into	nr_seq_atepacu_w
	from	atend_paciente_unidade a
	where	a.nr_atendimento	= nr_atendimento_p
/*	  and  a.cd_setor_atendimento	= nvl(cd_setor_atendimento_p, a.cd_setor_atendimento)*/
	  and	nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999)	= 
			(select max(nvl(b.dt_saida_unidade, b.dt_entrada_unidade + 9999))
			from atend_paciente_unidade b
			where b.nr_atendimento 	= nr_atendimento_p
			  and b.dt_entrada_unidade <= dt_referencia_p);
elsif	(ie_opcao_p = 'PI') then
	
	select	nvl(min(nr_seq_interno),0)
	into	nr_seq_atepacu_w
	from 	atend_paciente_unidade a
	where	a.nr_atendimento 		= nr_atendimento_p
/*	  and  a.cd_setor_atendimento	= nvl(cd_setor_atendimento_p, a.cd_setor_atendimento)*/
	  and dt_entrada_unidade	= 
		(select min(dt_entrada_unidade)
		from 	Setor_Atendimento y,
			atend_paciente_unidade b
		where 	nr_atendimento 		= nr_atendimento_p
		  and 	b.cd_setor_atendimento	= y.cd_setor_atendimento
		  and 	y.cd_classif_setor in (3,4,8));
elsif	(ie_opcao_p = 'IA') then
	/* Ricardo 23/01/2004 - Inclu� o teste da classifica��o do setor no select do nr_seq_interno 
				(para solucionar dois unidades com a mesma unidade de sa�da) */
	select	nvl(max(nr_seq_interno),0)
	into	nr_seq_atepacu_w
	from 	setor_atendimento b,
		atend_paciente_unidade a
	where	a.nr_atendimento 		= nr_atendimento_p
	  and 	a.cd_setor_atendimento		= b.cd_setor_atendimento 
	  and 	b.cd_classif_setor in (3,4,8)
/*	  and  a.cd_setor_atendimento	= nvl(cd_setor_atendimento_p, a.cd_setor_atendimento)*/
	  and 	nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999)	= 
		(select max(nvl(b.dt_saida_unidade, b.dt_entrada_unidade + 9999))
		from 	Setor_Atendimento y,
			atend_paciente_unidade b
		where nr_atendimento 	= nr_atendimento_p
		  and b.cd_setor_atendimento	= y.cd_setor_atendimento
		  and y.cd_classif_setor in (3,4,8)
		  and	b.dt_entrada_unidade <= dt_referencia_p);

end if;

RETURN nr_seq_atepacu_w;

END Obter_Atepacu_Data;
/