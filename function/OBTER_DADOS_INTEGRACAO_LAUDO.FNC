create or replace
function obter_dados_integracao_laudo (	ds_opcao_p		varchar2,
					ie_valor_p		varchar2 default null)
 		    	return varchar2 is
			
/*
ds_opcao_p

NMU      NM_USUARIO_PACS
DSS      DS_SENHA_PACS
DIR_PAC  DS_DIRETORIO_PACS

*/			
		
ds_retorno_w		varchar2(255);			
cd_estabelecimento_w 	number;

begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

if (ds_opcao_p = 'DIR_PAC') then

	select  max(ds_diretorio_pacs)
	into	ds_retorno_w
	from    parametro_medico
	where   cd_estabelecimento = cd_estabelecimento_w;
	

elsif (ds_opcao_p = 'NMU') then

	select  max(nm_usuario_pixeon)
	into	ds_retorno_w
	from    parametro_integracao_pacs 
	where   cd_estabelecimento  = cd_estabelecimento_w;
	
elsif (ds_opcao_p = 'DSS') then

	select  max(ds_senha_pixeon)
	into	ds_retorno_w
	from    parametro_integracao_pacs 
	where   cd_estabelecimento  = cd_estabelecimento_w;
elsif (ds_opcao_p = 'CPOE_INT') then

	select	max(cd_intervalo_cpoe)
	into	ds_retorno_w
	from	parametro_medico
	where   cd_estabelecimento  = cd_estabelecimento_w;
	
else 
	ds_retorno_w := '';	
end if;

return	ds_retorno_w;

end obter_dados_integracao_laudo;
/
