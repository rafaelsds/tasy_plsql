create or replace
function obter_desc_classe_cpa(nr_seq_classe_p	Number)
 		    	return Varchar2 is

ds_retorno_w	Varchar2(80);
			
begin

select 	max(ds_classe)
into	ds_Retorno_w
from	classe_titulo_pagar
where	nr_sequencia	= nr_seq_classe_p;

return	ds_retorno_w;

end obter_desc_classe_cpa;
/
