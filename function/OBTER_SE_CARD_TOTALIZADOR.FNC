create or replace
function obter_se_card_totalizador( nr_seq_cardapio_p		number,
				   nr_seq_serv_dia_p		number,
				   cd_dieta_p			varchar2,
				   ie_paciente_p		varchar2   )
 		    	return varchar2 is
			
			
cd_dieta_w	 		number(10);
nr_seq_grupo_producao_w		number(10);
ie_conduta_dietot_w		varchar2(1);
ie_retorno_w			varchar2(1) := 'N';

begin

if ( (nr_seq_cardapio_p is not null) and (nr_seq_serv_dia_p is not null) and (ie_paciente_p = 'S')) then


	select	max(cd_dieta),
		max(nr_seq_grupo_producao)
	into	cd_dieta_w,
		nr_seq_grupo_producao_w
	from	nut_cardapio_dia
	where	nr_sequencia = nr_seq_cardapio_p;


	ie_conduta_dietot_w	:= obter_valor_param_usuario(1003,85,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);


	if (ie_conduta_dietot_w = 'S') then
		
		SELECT	coalesce(max('S'),'N')
		into	ie_retorno_w
		from	nut_atend_serv_dia_dieta a,
			nut_atend_serv_dieta b,
			nut_atend_serv_dia c
		WHERE	a.nr_sequencia 		 = b.nr_seq_dieta  
		AND	c.nr_sequencia	 	 = b.nr_seq_servico
		AND	c.nr_sequencia	 	 = nr_seq_serv_dia_p	
		AND	a.cd_dieta		 = cd_dieta_w;
		
		if	(ie_retorno_w = 'N') then
		
			SELECT	coalesce(max('S'),'N')
			into	ie_retorno_w
			from 	nut_grupo_producao_dieta x
			where	x.nr_seq_grupo_producao	= nr_seq_grupo_producao_w
			and	x.cd_dieta = cd_dieta_p
			and	exists ( select	1
					from	nut_atend_serv_dia_dieta a,
						nut_atend_serv_dieta b,
						nut_atend_serv_dia c
					WHERE	a.nr_sequencia 		= b.nr_seq_dieta  
					AND	c.nr_sequencia	 	= b.nr_seq_servico
					AND	c.nr_sequencia	 	= nr_seq_serv_dia_p
					and	a.cd_dieta 		= x.cd_dieta);
		end if;

	else

		select	coalesce(max('S'), 'N')
		into	ie_retorno_w
		from 	nut_atend_serv_dia_rep a,
			prescr_dieta b
		WHERE 	a.nr_prescr_oral 	= b.nr_prescricao  
		AND	a.nr_seq_serv_dia 	= nr_seq_serv_dia_p
		and	b.cd_dieta		= cd_dieta_w;	
			
		if	(ie_retorno_w = 'N') then

			select	coalesce(max('S'), 'N')
			into	ie_retorno_w
			from	nut_grupo_producao_dieta x
			where	x.nr_seq_grupo_producao	= nr_seq_grupo_producao_w
			and	x.cd_dieta = cd_dieta_p
			and	exists (select	1
					from 	nut_atend_serv_dia_rep a,
						prescr_dieta b
					WHERE 	a.nr_prescr_oral 	= b.nr_prescricao  
					AND	a.nr_seq_serv_dia 	= nr_seq_serv_dia_p
					and	b.cd_dieta		= x.cd_dieta);

		end if;
		
	end if;	
end if;


return	ie_retorno_w;

end obter_se_card_totalizador;
/