create or replace
function san_calc_volume_bolsa_padrao (
			nr_seq_derivado_p	number,
			qt_peso_bolsa_p		number,
			qt_peso_bolsa_vazia_p	number,
			nr_seq_conservante_p	number,
			cd_material_p		number,
			ie_tipo_bolsa_p		varchar2 default null)
 		    	return number is

-- Apos alterar esta function, deve ser verificada a SAN_CALC_PESO_HEMO, que faz o oposto desta
			
qt_densidade_w		number(10,3);
qt_volume_w		number(10,3);
qt_peso_bolsa_vazia_w	number(10,3);
begin
if (nr_seq_derivado_p is not null) then

	if (nr_seq_conservante_p is not null and nr_seq_conservante_p > 0) and (cd_material_p is not null) and (ie_tipo_bolsa_p is null) then
		SELECT 	max(qt_densidade_hemo),
			max(qt_peso_bolsa_vazia)
		into	qt_densidade_w,
			qt_peso_bolsa_vazia_w
		FROM   	san_derivado_regra
		WHERE  	nr_seq_derivado 	= nr_seq_derivado_p
		and	nr_seq_conservante 	= nr_seq_conservante_p
		and	cd_material	 	= cd_material_p
		and	ie_tipo_bolsa is null;
	elsif (nr_seq_conservante_p is not null and nr_seq_conservante_p > 0) and (ie_tipo_bolsa_p is null) then
		SELECT 	max(qt_densidade_hemo),
			max(qt_peso_bolsa_vazia)
		into	qt_densidade_w,
			qt_peso_bolsa_vazia_w
		FROM   	san_derivado_regra
		WHERE  	nr_seq_derivado 	= nr_seq_derivado_p
		and	nr_seq_conservante 	= nr_seq_conservante_p
		and	cd_material is null
		and	ie_tipo_bolsa is null;
	elsif (cd_material_p is not null) and (ie_tipo_bolsa_p is null) then
		SELECT 	max(qt_densidade_hemo),
			max(qt_peso_bolsa_vazia)
		into	qt_densidade_w,
			qt_peso_bolsa_vazia_w
		FROM   	san_derivado_regra
		WHERE  	nr_seq_derivado 	= nr_seq_derivado_p
		and	cd_material	 	= cd_material_p
		and	nr_seq_conservante is null
		and	ie_tipo_bolsa is null;
	elsif (ie_tipo_bolsa_p is null) then
		SELECT 	max(qt_densidade_hemo),	
			max(qt_peso_bolsa_vazia)
		into	qt_densidade_w,
			qt_peso_bolsa_vazia_w
		FROM   	san_derivado_regra
		WHERE  	nr_seq_derivado    = nr_seq_derivado_p
		and	cd_material	   is null
		and	nr_seq_conservante is null
		and	ie_tipo_bolsa is null;
	elsif (nr_seq_conservante_p is not null and nr_seq_conservante_p > 0) and (cd_material_p is not null) and (ie_tipo_bolsa_p is not null)  then
		SELECT 	max(qt_densidade_hemo),
			max(qt_peso_bolsa_vazia)
		into	qt_densidade_w,
			qt_peso_bolsa_vazia_w
		FROM   	san_derivado_regra
		WHERE  	nr_seq_derivado 	= nr_seq_derivado_p
		and	nr_seq_conservante 	= nr_seq_conservante_p
		and	cd_material	 	= cd_material_p
		and	ie_tipo_bolsa = ie_tipo_bolsa_p;
	elsif (nr_seq_conservante_p is not null and nr_seq_conservante_p > 0) and (ie_tipo_bolsa_p is not null)  then
		SELECT 	max(qt_densidade_hemo),
			max(qt_peso_bolsa_vazia)
		into	qt_densidade_w,
			qt_peso_bolsa_vazia_w
		FROM   	san_derivado_regra
		WHERE  	nr_seq_derivado    = nr_seq_derivado_p
		and	nr_seq_conservante = nr_seq_conservante_p
		and	cd_material 	   is null
		and	ie_tipo_bolsa 	   = ie_tipo_bolsa_p;
	elsif (cd_material_p is not null) and (ie_tipo_bolsa_p is not null)  then
		SELECT 	max(qt_densidade_hemo),
			max(qt_peso_bolsa_vazia)
		into	qt_densidade_w,
			qt_peso_bolsa_vazia_w
		FROM   	san_derivado_regra
		WHERE  	nr_seq_derivado    = nr_seq_derivado_p
		and	cd_material	   = cd_material_p
		and	nr_seq_conservante is null
		and	ie_tipo_bolsa 	   = ie_tipo_bolsa_p;
	elsif (ie_tipo_bolsa_p is not null) then
		SELECT 	max(qt_densidade_hemo),
			max(qt_peso_bolsa_vazia)
		into	qt_densidade_w,
			qt_peso_bolsa_vazia_w
		FROM   	san_derivado_regra
		WHERE  	nr_seq_derivado    = nr_seq_derivado_p
		and	cd_material	   is null
		and	nr_seq_conservante is null
		and	ie_tipo_bolsa      = ie_tipo_bolsa_p;
	end if;

	if (qt_densidade_w is null) then
		select	nvl(max(qt_densidade_hemo),1)
		into	qt_densidade_w
		from	san_derivado
		where	nr_sequencia = nr_seq_derivado_p;
	end if;

	select	((nvl(qt_peso_bolsa_p,0) - coalesce(qt_peso_bolsa_vazia_p,qt_peso_bolsa_vazia_w,0)) / qt_densidade_w)
	into	qt_volume_w
	from 	dual;

end if;

return	qt_volume_w;

end san_calc_volume_bolsa_padrao;
/