create or replace
function pls_obter_valor_resultado
			(	nr_seq_resultado_p		Number,
				ie_tipo_valor_p			Number)
 		    		return Number is
				
/* IE_TIPO_VALOR_P
	1	|Mensalidade
	2	|Contas m�dicas
	3	|Reembolso
	4	|Comiss�o vendas
	5	|Despesas n�o assist�nciais
	6	|Encargos
	7	|Ressarcimento SUS
	8	|Resultado				
*/				

ie_tipo_valor_w			Varchar2(5);
vl_resultado_w			Number(15,2);
				
begin
         
select	ie_tipo_valor,
	vl_resultado
into	ie_tipo_valor_w,
	vl_resultado_w
from	pls_resultado
where	nr_sequencia	= nr_seq_resultado_p
and	((ie_tipo_valor	= ie_tipo_valor_p) or (ie_tipo_valor_p = 8));

if	(ie_tipo_valor_w	not in (1,11)) then
	vl_resultado_w		:= (abs(vl_resultado_w) * -1);
end if;	
	 
return	vl_resultado_w;

end pls_obter_valor_resultado;
/