create or replace
function OBTER_TOTAL_VINC_TIT_RECEB
			(nr_titulo_p	in number) 
			return number is

vl_retorno_w	number(15,2) := 0;

begin

select	nvl(sum(vl_vinculado),0)
into	vl_retorno_w
from	convenio_receb_tit_rec
where	nr_titulo	= nr_titulo_p;

return 	vl_retorno_w;

end OBTER_TOTAL_VINC_TIT_RECEB;
/