create or replace function get_exam_sequences(nr_atendimento_p	number,
					nr_seq_classif_p	number)
return varchar2 as

ds_exam_seq_w	varchar2(100) := '';
ds_exam_list_w	varchar2(500) := '';
qt_lengt_w	number(4);

cursor c02 is

select  a.nr_sequencia
from	cpoe_procedimento a, proc_interno b
where	a.ie_pre_cons_exam = 'S'
and	a.dt_liberacao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_seq_proc_interno = b.nr_sequencia
and	b.nr_seq_classif = nr_seq_classif_p;

begin
open c02;
loop
fetch	c02 into
	ds_exam_seq_w;
exit	when c02%notfound;
begin
	qt_lengt_w := nvl(length(ds_exam_list_w),0);
	if (qt_lengt_w < 500) then
		ds_exam_list_w	:= ds_exam_list_w || ',' || ds_exam_seq_w;
	end if;
end;
end loop;

ds_exam_list_w := substr(ds_exam_list_w,2,length(ds_exam_list_w));

return ds_exam_list_w;

end get_exam_sequences;
/