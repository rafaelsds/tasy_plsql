create or replace
function pls_obter_se_unimed_superior
			(	nr_seq_congenere_p		number,
				nr_seq_congenere_sup_p		number)
 		    	return varchar2 is

ds_retorno_w			varchar2(1)	:= 'N';
nr_seq_congenere_w		number(10)	:= null;
nr_seq_congenere_atual_w	number(10)	:= null;

begin
select	nr_seq_congenere
into	nr_seq_congenere_w
from	pls_congenere
where	nr_sequencia	 = nr_seq_congenere_p;

if	(nr_seq_congenere_w = nr_seq_congenere_sup_p) then
	ds_retorno_w	:= 'S';
else
	while	(nr_seq_congenere_w is not null) and
		(ds_retorno_w = 'N') loop 
		begin
		select	nr_seq_congenere
		into	nr_seq_congenere_atual_w
		from	pls_congenere
		where	nr_sequencia	 = nr_seq_congenere_w;
		
		nr_seq_congenere_w	:= nr_seq_congenere_atual_w;
		
		if	(nr_seq_congenere_w = nr_seq_congenere_sup_p) then
			ds_retorno_w	:= 'S';	
		end if;
		end;
	end loop;
end if;

return	ds_retorno_w;

end pls_obter_se_unimed_superior;
/