create or replace
function obter_se_existe_segurado_lib(	nr_seq_lote_reajuste_p number)
					return varchar2 is

qt_existe_w	number(10);
ds_retorno_w	varchar2(1);

begin

select	count(*)
into	qt_existe_w
from	pls_segurado_preco
where	nr_seq_lote_reajuste = nr_seq_lote_reajuste_p
and	dt_liberacao is null;

if	(qt_existe_w > 0) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end obter_se_existe_segurado_lib;
/