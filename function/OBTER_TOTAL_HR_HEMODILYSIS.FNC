create or replace FUNCTION Obter_total_hr_hemodilysis(
    nr_atendimento_p NUMBER,
    dt_inicial_p     DATE,
    dt_final_p       DATE)
  RETURN NUMBER
IS
  ie_evento_w        VARCHAR2(15);
  ie_somar_w         VARCHAR2(1);
  qt_minutos_w       NUMBER(10,0) := 0;
  qt_minutos_final_w NUMBER(10,0) := 0;
  dt_evento_w        DATE;
  dt_inicio_w        DATE;
  nr_seq_solucao_w   NUMBER(10);
  nr_prescricao_w    NUMBER(10);
 
  CURSOR c01
  IS
    SELECT c.nr_seq_solucao, a.nr_prescricao 
    FROM hd_prescricao a,
      prescr_solucao c,
      prescr_medica d
    WHERE d.nr_atendimento = nr_atendimento_p
    AND a.nr_prescricao    = d.nr_prescricao
    AND a.nr_sequencia     = c.nr_seq_dialise
    AND c.nr_prescricao    = d.nr_prescricao;
    
  CURSOR c02 (nr_seq_solucao_p number, nr_prescricao_p number)
  IS
    SELECT b.dt_evento,
      b.ie_evento
    FROM hd_prescricao a,
      hd_prescricao_evento b,
      prescr_solucao c,
      prescr_medica d
    WHERE d.nr_atendimento = nr_atendimento_p
    AND a.nr_prescricao    = d.nr_prescricao
    AND a.nr_sequencia     = c.nr_seq_dialise
    AND c.nr_prescricao    = d.nr_prescricao
    AND b.nr_prescricao    = c.nr_prescricao
    AND b.nr_seq_solucao   = c.NR_SEQ_SOLUCAO
    AND b.nr_seq_solucao   = nr_seq_solucao_p
    AND a.nr_prescricao   = nr_prescricao_p
    AND b.dt_evento BETWEEN TRUNC(dt_inicial_p) AND fim_dia(dt_final_p)
    ORDER BY dt_evento;

BEGIN
  IF (nr_atendimento_p IS NOT NULL AND dt_inicial_p IS NOT NULL AND dt_final_p IS NOT NULL) THEN
    OPEN C01;
    LOOP
      FETCH C01 INTO nr_seq_solucao_w,nr_prescricao_w;
      EXIT
    WHEN C01%notfound;
      OPEN C02 (nr_seq_solucao_w,nr_prescricao_w);
      LOOP
      FETCH C02 INTO dt_evento_w, ie_evento_w;
      EXIT
    WHEN C02%notfound;
      IF (ie_evento_w IN ('II')) THEN
        dt_inicio_w                                := dt_evento_w;
        ie_somar_w                                 := 'S';
      elsif (ie_evento_w  IN ('IT')) AND (ie_somar_w = 'S') THEN
        qt_minutos_w                               := qt_minutos_w + obter_min_entre_datas(dt_inicio_w, dt_evento_w, 1);
        ie_somar_w                                 := 'N';
      END IF;
    END LOOP;
    CLOSE C02;
    qt_minutos_final_w := qt_minutos_final_w + qt_minutos_w;
  END LOOP;
CLOSE c01;
END IF;
RETURN qt_minutos_final_w;
END Obter_total_hr_hemodilysis;
/