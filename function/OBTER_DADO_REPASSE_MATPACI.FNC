create or replace
function Obter_Dado_Repasse_MatPaci(	nr_seq_material_p	number,
					nr_seq_terceiro_p		number,
					ie_opcao_p		number) 
					return varchar2 is

ds_retorno_w		varchar2(20);
nr_seq_retorno_w		number(10);
nr_seq_protocolo_w	number(15);
nr_interno_conta_w	number(15);

begin

if	(ie_opcao_p = 0) then
	select	sum(vl_repasse)
	into	ds_retorno_w
	from	material_repasse
	where nr_seq_material		= nr_seq_material_p
	  and nr_seq_terceiro		= nr_seq_terceiro_p
	  and nr_seq_item_retorno is not null
	  and nr_seq_ret_glosa is null;
elsif	(ie_opcao_p = 1) then
	select	sum(vl_repasse)
	into	ds_retorno_w
	from 	material_repasse
	where nr_seq_material		= nr_seq_material_p
	  and nr_seq_terceiro		= nr_seq_terceiro_p
	  and nr_seq_ret_glosa is not null;
elsif	(ie_opcao_p = 2) then
	select	min(nr_seq_item_retorno)
	into	nr_seq_retorno_w
	from 	material_repasse
	where nr_seq_material		= nr_seq_material_p
	  and nr_seq_terceiro		= nr_seq_terceiro_p
	  and nr_seq_item_retorno is not null
	  and nr_seq_ret_glosa is null;

	if	(nr_seq_retorno_w is not null) then
		select	nvl(min(dt_fechamento),null)
		into	ds_retorno_w
		from	convenio_retorno b,
			convenio_retorno_item a
		where	a.nr_seq_retorno= b.nr_sequencia
		  and	a.nr_sequencia	= nr_seq_retorno_w;
	end if;
elsif	(ie_opcao_p = 3) then
	select	min(nr_seq_ret_glosa)
	into	nr_seq_retorno_w
	from 	material_repasse
	where nr_seq_material		= nr_seq_material_p
	  and nr_seq_terceiro		= nr_seq_terceiro_p
	  and nr_seq_ret_glosa is not null;

	if	(nr_seq_retorno_w is not null) then
		select	nvl(min(dt_fechamento),null)
		into	ds_retorno_w
		from	convenio_retorno c,
			convenio_retorno_item b,
			convenio_retorno_glosa a
		where	a.nr_seq_ret_item	= b.nr_sequencia
		  and	b.nr_seq_retorno	= c.nr_sequencia
		  and	a.nr_sequencia		= nr_seq_retorno_w;
	end if;
elsif	(ie_opcao_p = 4) then /* vl_pendente*/
	select	sum(vl_repasse)
	into	ds_retorno_w
	from	material_repasse
	where	nr_seq_material		= nr_seq_material_p
	and	nr_seq_terceiro		= nr_seq_terceiro_p
	and	nr_repasse_terceiro is null;
elsif	(ie_opcao_p = 5) then /* dt_protocolo*/
	select	a.nr_interno_conta
	into	nr_interno_conta_w
	from	material_atend_paciente a
	where	a.nr_sequencia 	= nr_seq_material_p;

	select	b.nr_seq_protocolo
	into	nr_seq_protocolo_w
	from	conta_paciente b
	where	b.nr_interno_conta	= nr_interno_conta_w;

	select	a.dt_mesano_referencia
	into	ds_retorno_w
	from	protocolo_convenio a
	where	a.nr_seq_protocolo	= nr_seq_protocolo_w;
elsif	(ie_opcao_p = 6) then /* dt_protocolo*/
	select	a.nr_interno_conta
	into	nr_interno_conta_w
	from	material_atend_paciente a
	where	a.nr_sequencia 	= nr_seq_material_p;

	select	max(b.nr_seq_retorno)
	into	nr_seq_retorno_w
	from	convenio_retorno_item b
	where	b.nr_interno_conta	= nr_interno_conta_w;

	select	max(c.dt_fechamento)
	into	ds_retorno_w
	from	convenio_retorno c
	where	c.nr_sequencia	= nr_seq_retorno_w;
elsif	(ie_opcao_p = 7) then	/* repasse do procedimento */
	select	sum(vl_repasse)
	into	ds_retorno_w
	from 	material_repasse
	where	nr_seq_material		= nr_seq_material_p
	and	nr_seq_terceiro		= nvl(nr_seq_terceiro_p,nr_seq_terceiro);

end if;

return ds_retorno_w;

end Obter_Dado_Repasse_MatPaci;
/