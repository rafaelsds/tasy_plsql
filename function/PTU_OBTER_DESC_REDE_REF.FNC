create or replace
function ptu_obter_desc_rede_ref(cd_rede_p	varchar2)
						return varchar2 is
						
ds_retorno_w	varchar2(255);

begin

if	(cd_rede_p is not null) then
	select	a.ds_rede
	into	ds_retorno_w
	from	ptu_rede_referenciada a
	where	a.cd_rede	= cd_rede_p;
end if;

return	ds_retorno_w;

end ptu_obter_desc_rede_ref;
/