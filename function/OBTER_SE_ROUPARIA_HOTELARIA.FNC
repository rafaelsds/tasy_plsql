create or replace
function obter_se_rouparia_hotelaria(	nr_seq_unidade_p		number) return varchar2 is

ds_retorno_w	varchar2(1);

begin

SELECT	DECODE(COUNT(1),0,'N','S')
into	ds_retorno_w
FROM	sl_unid_atend x,
		sl_servico y
WHERE	x.nr_seq_unidade	=  	nr_seq_unidade_p
AND		y.nr_sequencia		=	NVL(x.nr_seq_servico_exec,x.nr_seq_servico)
and		x.dt_fim is null
and		x.ie_status_serv <> 'C'
AND		y.IE_EXECUTOR IN ('O','R')
AND		ROWNUM = 1;

return ds_retorno_w;

end obter_se_rouparia_hotelaria;
/