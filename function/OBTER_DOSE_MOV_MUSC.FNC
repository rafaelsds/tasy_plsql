create or replace
function obter_dose_mov_musc(	cd_pessoa_fisica_p		varchar2,
				nr_seq_art_mov_musculo_p	number,
				nr_seq_toxina_p		number default 1)
 		    	return number is
			
qt_retorno_w		number(18,6);			
qt_idade_w		number(5);
qt_peso_w		number(10,3);
ie_forma_calculo_w		varchar2(3);
qt_conversao_w		number(18,6);
nr_seq_toxina_w		number(10);

begin

select	max(obter_idade(dt_nascimento,sysdate,'A'))
into	qt_idade_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

select	nvl(max(qt_peso),0)
into	qt_peso_w
from	atendimento_sinal_vital	
where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
			from	atendimento_sinal_vital
			where	qt_peso is not null
			and	cd_paciente	= cd_pessoa_fisica_p
			and	ie_situacao = 'A'
			and	nvl(IE_RN,'N')	= 'N');


select	max(a.qt_dose_padrao),
	max(ie_forma_calculo),
	max(nr_seq_toxina) 
into	qt_retorno_w,
	ie_forma_calculo_w,
	nr_seq_toxina_w
from 	artic_mov_musculo_regra a 
where 	a.nr_seq_art_mov_musculo = nr_seq_art_mov_musculo_p
and	qt_idade_w between a.qt_idade_min and a.qt_idade_max
and 	qt_peso_w between nvl(a.qt_peso_min,0) and nvl(a.qt_peso_max,999)
and	nvl(a.nr_seq_toxina,nr_seq_toxina_p) = nr_seq_toxina_p;

if	(nr_seq_toxina_w is not null) then

	select  nvl(max(qt_conversao),1)
	into	qt_conversao_w
	from	toxina_botulinica
	where	nr_Sequencia =	nr_seq_toxina_w;

end if;
 /*

if	(upper(ie_forma_calculo_w) = 'KG') then

qt_retorno_w := qt_retorno_w * qt_peso_w * nvl(qt_conversao_w,1);

end if;*/

return	qt_retorno_w;

end obter_dose_mov_musc;
/
