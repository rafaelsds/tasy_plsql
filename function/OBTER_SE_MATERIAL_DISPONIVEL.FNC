create or replace
function obter_se_material_disponivel	(cd_material_p	number)
				return varchar2 is
					
ie_disponivel_w	varchar2(1);
					
begin
if	(cd_material_p is not null) then

	select	nvl(max(ie_disponivel_mercado),'N')
	into	ie_disponivel_w
	from	material
	where	cd_material = cd_material_p;

end if;

return ie_disponivel_w;

end obter_se_material_disponivel;
/