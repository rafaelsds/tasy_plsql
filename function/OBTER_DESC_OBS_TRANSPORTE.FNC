create or replace
function obter_desc_obs_transporte( nr_seq_obs_p	number)
 		    	return varchar2 is
ds_retorno_w	varchar2(255);
begin
if (nvl(nr_seq_obs_p,0) > 0) then
	begin
		select 	ds_obs_geral
		into	ds_retorno_w
		from	gestao_transporte_hist_obs
		where	nr_sequencia = nr_seq_obs_p;	
	exception
	when others then
		ds_retorno_w := '';
	end;
end if;
return	ds_retorno_w;

end obter_desc_obs_transporte;
/