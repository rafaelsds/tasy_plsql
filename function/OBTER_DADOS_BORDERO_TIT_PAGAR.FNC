create or replace
function obter_dados_bordero_tit_pagar(
		nr_sequencia_p	number,
		nr_bordero_p	number,
		nr_titulo_p	number,
		ie_opcao_p	varchar2)
		return number is
		
/*'
retorno:
VCI:	valor border� com impostos
VSI:	valor border� sem impostos
VT:	valor de impostos
VLI:	valor l�quido com impostos
'*/

nr_sequencia_w		number(10);
nr_bordero_w		number(10);
nr_titulo_w		number(10);
vl_bordero_w		number(15,2);
vl_imposto_w		number(15,2);
vl_imposto_baixa_w		number(15,2);
vl_desconto_bordero_w	number(15,2);
vl_juros_bordero_w		number(15,2);
vl_multa_bordero_w		number(15,2);


ie_trib_atualiza_saldo_w	varchar2(1);

ds_retorno_w		varchar2(255);

begin

begin
select	nr_sequencia
into	nr_sequencia_w
from	bordero_tit_pagar
where	nr_sequencia	= nr_sequencia_p;
exception
when others then
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	bordero_tit_pagar
	where	nr_bordero = nr_bordero_p
	and	nr_titulo = nr_titulo_p;
end;

if	(nr_sequencia_w > 0) then
	begin
	select	nr_bordero,
		nr_titulo,
		vl_bordero,
		nvl(vl_desconto_bordero,0),
		nvl(vl_juros_bordero,0),
		nvl(vl_multa_bordero,0)
	into	nr_bordero_w,
		nr_titulo_w,
		vl_bordero_w,
		vl_desconto_bordero_w,
		vl_juros_bordero_w,
		vl_multa_bordero_w
	from	bordero_tit_pagar
	where	nr_sequencia	= nr_sequencia_w;
	exception
	when others then
		nr_bordero_w	:= null;
	end;
else
	begin	
	select	nr_bordero,
		nr_titulo,
		vl_bordero,
		nvl(vl_desconto_bordero,0),
		nvl(vl_juros_bordero,0),
		nvl(vl_multa_bordero,0)
	into	nr_bordero_w,
		nr_titulo_w,
		vl_bordero_w,
		vl_desconto_bordero_w,
		vl_juros_bordero_w,
		vl_multa_bordero_w
	from	titulo_pagar
	where	nr_bordero = nr_bordero_p
	and	nr_titulo = nr_titulo_p;	
	exception
	when others then
		nr_bordero_w := null;
	end;
end if;

if	(nr_bordero_w is not null) then
	begin
	select	nvl(max(b.ie_trib_atualiza_saldo),'S')
	into	ie_trib_atualiza_saldo_w
	from	parametros_contas_pagar b,
		titulo_pagar a
	where	a.cd_estabelecimento	= b.cd_estabelecimento
	and	a.nr_titulo		= nr_titulo_w;
	
	vl_imposto_baixa_w	:= 0;
	vl_imposto_w		:= 0;
	
	if	(ie_trib_atualiza_saldo_w = 'S') then
		begin
		if	(ie_opcao_p in ('VSI', 'VT')) then
			vl_imposto_w	:= nvl(obter_valores_tit_pagar(nr_titulo_w, sysdate, 'VT'),0);
		end if;
		
		if	(ie_opcao_p in ('VSI', 'VCI', 'VLI')) then
			select	nvl(sum(b.vl_imposto), 0)
			into	vl_imposto_baixa_w
			from	titulo_pagar_imposto b,
				titulo_pagar_baixa a
			where	a.nr_titulo	= b.nr_titulo
			and	a.nr_sequencia	= b.nr_seq_baixa
			and	b.ie_pago_prev	= 'V'
			and	a.nr_bordero	= nr_bordero_w
			and	a.nr_titulo	= nr_titulo_w;
		end if;
		end;
	end if;
		
	if	(ie_opcao_p = 'VSI') then
		ds_retorno_w	:= vl_bordero_w + (vl_imposto_w - vl_imposto_baixa_w);
	elsif	(ie_opcao_p = 'VCI') then
		ds_retorno_w	:= vl_bordero_w - vl_imposto_baixa_w;
	elsif	(ie_opcao_p = 'VT') then
		ds_retorno_w	:= vl_imposto_w;
	elsif	(ie_opcao_p = 'VLI') then
		ds_retorno_w	:= vl_bordero_w - vl_imposto_baixa_w + vl_juros_bordero_w - vl_desconto_bordero_w + vl_multa_bordero_w;
	end if;
	end;
end if;

return	ds_retorno_w;

end	obter_dados_bordero_tit_pagar;
/