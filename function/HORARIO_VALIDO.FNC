create or replace 
function Horario_valido
		(hr_base_p	Date,
		hr_inicio_p	Date,
		hr_fim_p	Date)
		return varchar2 is

ds_retorno_w	varchar2(001)	:= 'N';
hr_fim_w	Date;

BEGIN

ds_retorno_w		:= 'N';
hr_fim_w		:= hr_fim_p;
if	(hr_inicio_p > hr_fim_p) then
	hr_fim_w	:= hr_fim_p + 1;
end if;

if	(hr_base_p >= hr_inicio_p) and
	(hr_base_p <= hr_fim_w) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

END Horario_valido;
/