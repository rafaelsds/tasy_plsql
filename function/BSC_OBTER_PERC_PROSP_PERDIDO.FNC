create or replace
function bsc_obter_perc_prosp_perdido(		dt_inicial_p	date,
					dt_final_p		date)
 		    	return number is

pr_retorno_w		number(15,2);
qt_total_out_prosp_w	number(10);
qt_total_prospect_w	number(10);

begin
select	count(*)
into	qt_total_out_prosp_w
from	com_cliente a
where	a.ie_classificacao = 'AO'
and	exists(	select	1
		from	com_cliente_hist x
		where	a.nr_sequencia = x.nr_seq_cliente
		and	x.nr_seq_ativ = 24 -- Fechado comprou de concorrente
		and	x.dt_historico between dt_inicial_p and fim_dia(dt_final_p));

select	count(*)
into	qt_total_prospect_w
from	com_cliente a
where	exists(	select	1
		from	com_cliente_log x
		where	a.nr_sequencia = x.nr_seq_cliente
		and	x.ie_classificacao = 'P' -- prospect
		and	x.dt_log between dt_inicial_p and fim_dia(dt_final_p));

if	(qt_total_prospect_w = 0) then
	pr_retorno_w := 0;
else
	pr_retorno_w := ((qt_total_out_prosp_w * 100) / qt_total_prospect_w);
end if;

return	pr_retorno_w;

end bsc_obter_perc_prosp_perdido;
/