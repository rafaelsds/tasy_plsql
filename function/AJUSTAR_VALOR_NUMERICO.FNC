create or replace
function ajustar_valor_numerico(qt_valor_p	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar(255);
			
begin

if	(substr(trim(qt_valor_p),0,1) = ',' ) then
	
	ds_retorno_w := '0' || trim(qt_valor_p);
end if;
	
if ( substr(trim(qt_valor_p),0,1) <> ',') then
	ds_retorno_w := qt_valor_p;
end if;

return	ds_retorno_w;

end ajustar_valor_numerico;
/

