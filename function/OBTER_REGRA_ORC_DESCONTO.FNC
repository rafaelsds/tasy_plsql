create or replace
function obter_regra_orc_desconto(	vl_orcamento_p	number,
					ie_opcao_p	varchar2)
 		    	return Varchar2 is
			
vl_retorno_w	number(15,2);
qt_reg_w	number(10,0);
pr_desconto_w	number(15,2);
vl_minimo_w	number(15,2);
vl_maximo_w	number(15,2);

begin

select	count(*)
into	qt_reg_w
from	regra_orc_desconto;

if	(nvl(qt_reg_w,0) > 0) then
	
	select	max(pr_desconto),
		max(vl_minimo),
		max(vl_maximo)
	into	pr_desconto_w,
		vl_minimo_w,
		vl_maximo_w
	from	regra_orc_desconto
	where	vl_orcamento_p between vl_minimo and vl_maximo;
	
	if	(ie_opcao_p	= 'DES') then
		vl_retorno_w	:= pr_desconto_w;
	elsif	(ie_opcao_p	= 'MIN') then
		vl_retorno_w	:= vl_minimo_w;
	elsif	(ie_opcao_p	= 'MAX') then
		vl_retorno_w	:= vl_maximo_w;
	end if;
	
end if;

vl_retorno_w	:= nvl(vl_retorno_w,-1);

return	vl_retorno_w;

end obter_regra_orc_desconto;
/