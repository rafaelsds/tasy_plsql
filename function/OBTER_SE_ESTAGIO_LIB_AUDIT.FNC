create or replace
function obter_se_estagio_lib_audit(	nr_seq_auditoria_p	auditoria_conta_paciente.nr_sequencia%type,
				nr_seq_motivo_audit_p	auditoria_motivo.nr_sequencia%type)
 		    	return Varchar2 is

nr_seq_pendencia_w	cta_pendencia.nr_sequencia%type;
nr_seq_estagio_w	cta_pendencia.nr_seq_estagio%type;
qt_regra_w		number(10,0);
qt_regra_motivo_w	number(10,0);
ds_retorno_w		varchar2(1) := 'S';
			
begin

if	(nvl(nr_seq_auditoria_p,0) > 0) then

	select	count(1)
	into	qt_regra_w
	from	auditoria_motivo_estagio
	where	nr_seq_audit_motivo = nr_seq_motivo_audit_p;
	
	if	(qt_regra_w > 0) then

		select	max(nr_sequencia)
		into	nr_seq_pendencia_w
		from	cta_pendencia
		where	nr_seq_auditoria = nr_seq_auditoria_p;
		
		if	(nvl(nr_seq_pendencia_w,0) > 0) then
		
			select	max(nr_seq_estagio)
			into	nr_seq_estagio_w
			from	cta_pendencia
			where	nr_sequencia = nr_seq_pendencia_w;
			
			select	count(1)
			into	qt_regra_motivo_w
			from	auditoria_motivo_estagio
			where	nr_seq_audit_motivo = nr_seq_motivo_audit_p
			and	nr_seq_estagio_pend = nr_seq_estagio_w;
			
			if	(qt_regra_motivo_w = 0) then
				ds_retorno_w	:= 'N';
			end if;
		
		end if;
		
	end if;

end if;

return	ds_retorno_w;

end obter_se_estagio_lib_audit;
/