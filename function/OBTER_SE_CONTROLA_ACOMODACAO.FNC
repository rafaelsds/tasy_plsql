create or replace
function obter_se_controla_acomodacao(nr_sequencia_p		number)
 		    	return varchar2 is
ds_retorno_w varchar2(1);
begin

if (nr_sequencia_p is not null) then
	select nvl(max(ie_controla_ocupacao),'S')
	into	ds_retorno_w
	from	qt_local
	where	nr_sequencia = nr_sequencia_p;
end if;


return	ds_retorno_w;

end obter_se_controla_acomodacao;
/