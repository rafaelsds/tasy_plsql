create or replace function OBTER_VALOR_PAGO_PROT
				(        nr_seq_protocolo_p      number,
         				ie_valor_p              varchar2,
         				dt_saldo_p              date)
         				return number is


vl_pago_w	number(15,2);
vl_amenor_w	number(15,2);
vl_glosado_w	number(15,2);
vl_adicional_w	number(15,2);

/*

Op��es:

P - Pago
AM - Amenor
G - Glosado
m - vl_amaior
*/


begin

if      (ie_valor_p = 'P') then
        select  nvl(sum(vl_pago),0)
        into    vl_pago_w
        from    convenio_retorno b,
                convenio_retorno_item a
        where   a.nr_seq_retorno = b.nr_sequencia
          and   nr_interno_conta in (select x.nr_interno_conta  from conta_paciente x where x.nr_seq_protocolo = nr_seq_protocolo_p)
          and   b.ie_status_retorno = 'F'
          and   b.dt_baixa_cr    <= nvl(dt_saldo_p, sysdate);
	
	return vl_pago_w;
end if;

if      (ie_valor_p = 'AM') then
        select  nvl(sum(vl_amenor),0)
        into    vl_amenor_w
        from    convenio_retorno b,
                convenio_retorno_item a
        where   a.nr_seq_retorno = b.nr_sequencia
          and   nr_interno_conta in (select x.nr_interno_conta  from conta_paciente x where x.nr_seq_protocolo = nr_seq_protocolo_p)
          and   b.ie_status_retorno = 'F'
          and   b.dt_baixa_cr    <= nvl(dt_saldo_p, sysdate);

	return vl_amenor_w;
end if;

if      (ie_valor_p = 'G') then
        select  nvl(sum(vl_glosado),0)
        into    vl_glosado_w
        from    convenio_retorno b,
                convenio_retorno_item a
        where   a.nr_seq_retorno = b.nr_sequencia
          and   nr_interno_conta in (select x.nr_interno_conta from conta_paciente x where x.nr_seq_protocolo = nr_seq_protocolo_p)
          and   b.ie_status_retorno = 'F'
          and   b.dt_baixa_cr    <= nvl(dt_saldo_p, sysdate);

	return vl_glosado_w;
end if;

if      (ie_valor_p = 'M') then
        select  nvl(sum(vl_adicional),0)
        into    vl_adicional_w
        from    convenio_retorno b,
                convenio_retorno_item a
        where   a.nr_seq_retorno = b.nr_sequencia
          and   nr_interno_conta in (select x.nr_interno_conta from conta_paciente x where x.nr_seq_protocolo = nr_seq_protocolo_p)
          and   b.ie_status_retorno = 'F'
          and   b.dt_baixa_cr    <= nvl(dt_saldo_p, sysdate);

	return vl_adicional_w;
end if;

end ;
/

