create or replace
function Obter_Motivo_Alta_sus(	nr_atendimento_p	number)
				return	varchar2 is


cd_motivo_alta_sus_w		number(2);

begin

begin
select	b.cd_motivo_alta_sus
into	cd_motivo_alta_sus_w
from	atendimento_paciente a,
	motivo_alta b
where	a.nr_atendimento = nr_atendimento_p
and	a.cd_motivo_alta = b.cd_motivo_alta;
exception
	when others then
	cd_motivo_alta_sus_w	:= null;
end;

return	cd_motivo_alta_sus_w;

end Obter_Motivo_Alta_sus;
/
