create or replace function obter_se_ultimo_status(	ie_tipo_agenda_p number,
							nr_seq_agenda_p		number,
							ie_evento_p		varchar2,
							ie_agenda_p		varchar2
							)
							return varchar2 is

ie_last_status_w	varchar2(1) := 'N';

begin

if 	(
	(nvl(ie_tipo_agenda_p,0) > 0) 	and
	(nvl(nr_seq_agenda_p,0) > 0) 	and
	(nvl(ie_evento_p,'N') <> 'N') 	and
	(nvl(ie_agenda_p,'N') <> 'N')
	) then

	select	decode(count(1),0,'N','S')
	into 	ie_last_status_w
	from	status_evento_agenda a
	where	(a.cd_estabelecimento = obter_estabelecimento_ativo or exists (	select 1 
										from STATUS_EVENTO_AGENDA_ESTAB x 
										where x.cd_estabelecimento = obter_estabelecimento_ativo
										and a.nr_sequencia 	   = x.nr_seq_status_evento_ag ))	
	and	a.ie_evento = ie_evento_p
	and	rownum = 1	
	and	((a.ie_agenda = ie_agenda_p) or (a.ie_agenda = 'T'))
	and 	nvl(a.ie_status,'0') = substr(obter_dados_agendas(ie_tipo_agenda_p,nr_seq_agenda_p,'SA'),1,255)  
	order 	by	nvl(a.cd_agenda,0),
			nvl(a.cd_perfil,0),
			nvl(a.ie_evolucao_clinica,'0'),
			nvl(a.ie_classif_agenda,'*'),
			nvl(a.cd_convenio,0),
			nvl(a.nr_seq_tipo_atend,0),
			a.ie_status;

end if;

return ie_last_status_w;

end obter_se_ultimo_status;
/
