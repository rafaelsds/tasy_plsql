create or replace
function OBTER_SE_ENTRA_FLUXO
		(cd_empresa_p		number,
		cd_conta_financ_p	number,
		cd_estab_fluxo_p	number,
		cd_estab_item_p		number) return varchar2 is

cd_empresa_w		number(10,0);
cd_estab_exclusivo_w	number(10,0);
ie_entra_fluxo_w	varchar2(1) := 'N';

begin

select	cd_empresa
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento	= cd_estab_item_p;

ie_entra_fluxo_w	:= 'N';
if	(cd_empresa_w = cd_empresa_p) then	-- verificar se a empresa do item a entrar o fluxo � da a mesma do fluxo que est� sendo gerado

	select	cd_estabelecimento
	into	cd_estab_exclusivo_w
	from	conta_financeira
	where	cd_conta_financ		= cd_conta_financ_p;

	if	(cd_estab_exclusivo_w is null) then	-- verificar se a conta financeira � exclusivo de uma empresa
		ie_entra_fluxo_w		:= 'S';
	elsif	(cd_estab_exclusivo_w = nvl(cd_estab_fluxo_p, cd_estab_exclusivo_w)) then -- verificar se o estab exclusivo da cf 
		ie_entra_fluxo_w		:= 'S';					-- � a mesma do fluxo que est� sendo gerado
	end if;
end if;

return	ie_entra_fluxo_w;

end OBTER_SE_ENTRA_FLUXO;
/
