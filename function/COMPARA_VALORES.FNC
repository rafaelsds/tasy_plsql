create or replace
function Compara_valores 			(nr_inicial_p		number,
						 nr_operando_p		varchar,
						 nr_final_p		number)
 		    	return number is

begin

if (nr_operando_p like '=') then
	if (nr_inicial_p = nr_final_p) then
		return 1;
	else
		return 0;
	end if;
elsif (nr_operando_p like '>') then
	if (nr_inicial_p > nr_final_p) then
		return 1;
	else
		return 0;
	end if;
elsif (nr_operando_p like '<') then
	if (nr_inicial_p < nr_final_p) then
		return 1;
	else
		return 0;
	end if;
elsif (nr_operando_p like '>=') then
	if (nr_inicial_p >= nr_final_p) then
		return 1;
	else
		return 0;
	end if;
elsif (nr_operando_p like '<=') then
	if (nr_inicial_p <= nr_final_p) then
		return 1;
	else
		return 0;
	end if;
end if;

end Compara_valores;
/