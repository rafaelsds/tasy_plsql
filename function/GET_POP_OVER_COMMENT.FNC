create or replace FUNCTION get_pop_over_comment (nr_atendimento_p NUMBER DEFAULT NULL
) RETURN VARCHAR2 IS

    rsql VARCHAR2(32767);
    CURSOR c1 IS
               SELECT ds_comment
                 FROM pop_over_table
                WHERE nr_atendimento = nr_atendimento_p
                ;
BEGIN 
    OPEN c1;
    FETCH c1 INTO rsql;
    CLOSE c1;

    RETURN (rsql);

END get_pop_over_comment ;
/
