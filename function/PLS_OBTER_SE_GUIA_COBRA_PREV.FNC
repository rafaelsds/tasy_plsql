create or replace
function pls_obter_se_guia_cobra_prev
			(	nr_seq_guia_p		number,
				nr_seq_segurado_p	number,
				qt_dias_cobranca_prev_p	number)
				return varchar2 is

ds_retorno_w 		varchar2(1) := 'N';
qt_reg_w		number(5);
dt_solicitacao_w	date;

begin

select	dt_solicitacao
into	dt_solicitacao_w
from	pls_guia_plano
where	nr_sequencia = nr_seq_guia_p;

if	(qt_dias_cobranca_prev_p > 0) then
	select	count(*)
	into	qt_reg_w
	from	pls_guia_plano
	where	nr_sequencia	<> nr_seq_guia_p
	and	nr_seq_segurado = nr_seq_segurado_p
	and	ie_cobranca_prevista = 'S'
	and	dt_solicitacao between trunc(dt_solicitacao_w - qt_dias_cobranca_prev_p) and fim_dia(dt_solicitacao_w);

	if	(qt_reg_w > 0) then
		ds_retorno_w := 'S';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_guia_cobra_prev;
/
