create or replace
function mprev_obter_dados_ciclo_partic(nr_seq_partic_ciclo_item_p number,
					ie_opcao_p varchar2)
					return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar diferentes informacoes com relacao ao participante da medicina preventiva.
Usada em campos function do dicionario de dados.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: MedPrev - Programas de Promocao a Saude
[  ]  Objetos do dicionario [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
ds_retorno_w			varchar2(255)	:= null;
ie_opcao_w			varchar2(3);
/*
ie_opcao_p
'DT' - Retorna a data de prevista
'NP' - Nome do programa da medicina preventiva
'RE' - Equipe responsavel pelo programa
'RP' - Profissional responsavel pelo programa
*/
	
begin

ie_opcao_w := upper(ie_opcao_p);

if	(ie_opcao_w = 'DT') then
	begin
	select	to_char(dt_prevista, 'dd/mm/yyyy')
	into	ds_retorno_w
	from	mprev_partic_ciclo_item
	where	ie_status <> 'C'
	and	nr_sequencia = nr_seq_partic_ciclo_item_p;
	exception
	when others then
		ds_retorno_w	:= '';
	end;	
elsif	(ie_opcao_w = 'NP') then
	select 	max(substr(mprev_obter_desc_programa(a.nr_seq_programa),1,255))
	into	ds_retorno_w
	from 	mprev_programa_partic a,
		mprev_prog_partic_modulo b,
		mprev_partic_ciclo_atend c,
		mprev_partic_ciclo_item d
	where	a.nr_sequencia = b.nr_seq_programa_partic
	and	b.nr_sequencia = c.nr_seq_prog_partic_mod
	and	c.nr_sequencia = d.nr_seq_partic_ciclo_atend
	and	d.nr_sequencia = nr_seq_partic_ciclo_item_p;
elsif	(ie_opcao_w = 'RE') then
	select 	max(f.cd_pessoa_responsavel)
	into	ds_retorno_w
	from 	mprev_programa_partic a,
		mprev_prog_partic_modulo b,
		mprev_partic_ciclo_atend c,
		mprev_partic_ciclo_item d,
		mprev_prog_partic_prof e,
		mprev_equipe f
	where	a.nr_sequencia = b.nr_seq_programa_partic
	and	b.nr_sequencia = c.nr_seq_prog_partic_mod
	and	c.nr_sequencia = d.nr_seq_partic_ciclo_atend
	and	e.nr_seq_programa_partic = a.nr_sequencia
	and	e.nr_seq_equipe = f.nr_sequencia
	and	d.nr_sequencia = nr_seq_partic_ciclo_item_p
	and	e.dt_inicio_acomp <= sysdate
	and	(e.dt_fim_acomp > sysdate or e.dt_fim_acomp is null);
elsif	(ie_opcao_w = 'RP') then
	select 	max(e.cd_profissional)
	into	ds_retorno_w
	from 	mprev_programa_partic a,
		mprev_prog_partic_modulo b,
		mprev_partic_ciclo_atend c,
		mprev_partic_ciclo_item d,
		mprev_prog_partic_prof e
	where	a.nr_sequencia = b.nr_seq_programa_partic
	and	b.nr_sequencia = c.nr_seq_prog_partic_mod
	and	c.nr_sequencia = d.nr_seq_partic_ciclo_atend
	and	e.nr_seq_programa_partic = a.nr_sequencia
	and	d.nr_sequencia = nr_seq_partic_ciclo_item_p
	and	e.dt_inicio_acomp <= sysdate
	and	(e.dt_fim_acomp > sysdate or e.dt_fim_acomp is null);
end if;
	
return ds_retorno_w;	

end mprev_obter_dados_ciclo_partic;
/