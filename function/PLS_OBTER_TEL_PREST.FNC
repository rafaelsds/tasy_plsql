create or replace
function pls_obter_tel_prest
			(	cd_pessoa_fisica_p	varchar2,
				cd_cgc_p		varchar2,
				ie_opcao_p		varchar2,
				ie_endereco_p		varchar2,
				nr_seq_compl_pj_p	number default 0,
				nr_seq_compl_pf_p	number default 0,
				NR_SEQ_TIPO_COMPL_ADIC_p number default 0)	
				return varchar2 is

ds_retorno_w		varchar2(40);
qt_compl_w		number(10);
nr_ddi_telefone_w	varchar2(3);
nr_ddd_telefone_w	varchar2(3);
nr_telefone_w		varchar2(40);

begin
if	(cd_pessoa_fisica_p is not null) then
	
	if	(nvl(nr_seq_compl_pf_p,0) <> 0) and 
		(ie_endereco_p = 'PFC') then
		select	max(nr_telefone),
			max(nr_ddi_telefone),
			max(nr_ddd_telefone)
		into	nr_telefone_w,
			nr_ddi_telefone_w,
			nr_ddd_telefone_w
		from	compl_pf_tel_adic
		where	nr_sequencia	= nr_seq_compl_pf_p;
	elsif	(nvl(NR_SEQ_TIPO_COMPL_ADIC_p,0) <> 0)and
		(ie_endereco_p = 'PFA') then
		begin
		select	nr_telefone,
			nr_ddi_telefone,
			nr_ddd_telefone
		into	nr_telefone_w,
			nr_ddi_telefone_w,
			nr_ddd_telefone_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	ie_tipo_complemento	= 9
		and	nr_seq_tipo_compl_adic	= NR_SEQ_TIPO_COMPL_ADIC_p;
		exception
		when others then
			nr_telefone_w		:= '';
			nr_ddi_telefone_w	:= '';
			nr_ddd_telefone_w	:= '';
		end;
	else
	
		select	count(*)
		into	qt_compl_w
		from	compl_pessoa_fisica		a
		where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	a.ie_tipo_complemento = decode(ie_endereco_p, 'PFA', 9, 'PFC', 2, 'PFR', 1);
		
		if	(qt_compl_w > 0) then
			select	a.nr_ddi_telefone,
				a.nr_ddd_telefone,
				a.nr_telefone
			into	nr_ddi_telefone_w,
				nr_ddd_telefone_w,
				nr_telefone_w	
			from	compl_pessoa_fisica		a
			where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
			and	a.nr_sequencia		= (	select	max(a.nr_sequencia) -- PK composta, pode haver v�rios registros, por isso do max - OS 707758 - wcbernardino
								from	compl_pessoa_fisica a
								where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
								and	a.ie_tipo_complemento 	= decode(ie_endereco_p, 'PFA', 9, 'PFC', 2, 'PFR', 1));
		end if;

		
		if	(nr_ddi_telefone_w is null) and (nr_ddd_telefone_w is null) and (nr_telefone_w is null) then
			select	a.nr_ddi_celular,
				a.nr_ddd_celular,
				a.nr_telefone_celular
			into	nr_ddi_telefone_w,
				nr_ddd_telefone_w,
				nr_telefone_w	
			from	pessoa_fisica		a
			where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;
		end if;	
	end if;
	
	if	(ie_opcao_p = 'DIT') then
		ds_retorno_w	:= nr_ddi_telefone_w;
	elsif	(ie_opcao_p = 'DDT') then
		ds_retorno_w	:= nr_ddd_telefone_w;
	elsif	(ie_opcao_p = 'T') then
		ds_retorno_w	:= nr_telefone_w;
	end if;	
else
	if	(nvl(ie_endereco_p, 'PJ') = 'PJ') then
		select	max(a.nr_ddi_telefone),
			max(a.nr_ddd_telefone),
			max(a.nr_telefone)
		into	nr_ddi_telefone_w,
			nr_ddd_telefone_w,
			nr_telefone_w	
		from	pessoa_juridica a
		where	a.cd_cgc		= cd_cgc_p;
	else
		select	max(a.nr_ddd_telefone),
			max(a.nr_telefone)
		into	nr_ddd_telefone_w,
			nr_telefone_w
		from	pessoa_juridica_compl a
		where	cd_cgc			= cd_cgc_p
		and	a.nr_sequencia		= nvl(nr_seq_compl_pj_p, a.nr_sequencia)
		and	a.ie_tipo_complemento	= decode(ie_endereco_p, 'PJA', 6, 'PJC', 1, 'PJF', 2);
	end if;	
	
	if	(ie_opcao_p = 'DIT') then
		ds_retorno_w	:= nr_ddi_telefone_w;
	elsif	(ie_opcao_p = 'DDT') then
		ds_retorno_w	:= nr_ddd_telefone_w;
	elsif	(ie_opcao_p = 'T') then
		ds_retorno_w	:= nr_telefone_w;
	end if;
	
	if	(nr_seq_compl_pj_p = 0) then
		ds_retorno_w 	:= obter_dados_pf_pj(null, cd_cgc_p, ie_opcao_p);
	end if;
end if;

return	ds_retorno_w;

end pls_obter_tel_prest;
/
