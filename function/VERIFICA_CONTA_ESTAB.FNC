create or replace
function verifica_conta_estab( cd_banco_p 		number,
				cd_agencia_bancaria_p	varchar2,
				nr_conta_p		varchar2,
				cd_pessoa_fisica_p	varchar2,
				cd_cgc_p		varchar2,
				cd_estabelecimento_p	number)
					return varchar2 is

qt_regra_w	number(10);
qt_regra_estab_w number(10);
ds_retorno_w	varchar2(1);

begin

ds_retorno_w := 'S';

if (cd_pessoa_fisica_p is null) then

	select	count(*)
	into	qt_regra_w
	from	pessoa_jur_conta_estab
	where	cd_banco = cd_banco_p
	and	cd_agencia_bancaria = cd_agencia_bancaria_p
	and	nr_conta = nr_conta_p
	and	cd_cgc = cd_cgc_p;

	if (qt_regra_w > 0) then

		select	count(*)
		into	qt_regra_estab_w
		from	pessoa_jur_conta_estab
		where	cd_banco = cd_banco_p
		and	cd_agencia_bancaria = cd_agencia_bancaria_p
		and	nr_conta = nr_conta_p
		and	cd_cgc = cd_cgc_p
		and	cd_estabelecimento = cd_estabelecimento_p;

		if (qt_regra_estab_w = 0) then
			ds_retorno_w := 'N';
		end if;
	end if;
elsif (cd_cgc_p is null) then

	select	count(*)
	into	qt_regra_w
	from	pessoa_fis_conta_estab
	where	cd_banco = cd_banco_p
	and	cd_agencia_bancaria = cd_agencia_bancaria_p
	and	nr_conta = nr_conta_p
	and	cd_pessoa_fisica = cd_pessoa_fisica_p;

	if (qt_regra_w > 0) then

		select	count(*)
		into	qt_regra_estab_w
		from	pessoa_fis_conta_estab
		where	cd_banco = cd_banco_p
		and	cd_agencia_bancaria = cd_agencia_bancaria_p
		and	nr_conta = nr_conta_p
		and	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	cd_estabelecimento = cd_estabelecimento_p;

		if (qt_regra_estab_w = 0) then
			ds_retorno_w := 'N';
		end if;
	end if;
end if;	

return	ds_retorno_w;

end verifica_conta_estab;

/