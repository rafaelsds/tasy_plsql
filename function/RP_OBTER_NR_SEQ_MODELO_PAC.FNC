create or replace
function rp_obter_nr_seq_modelo_pac(nr_seq_pac_reab_p		number)
 		    	return varchar2 is

nr_seq_modelo_agendamento_w	number(10,0);	
nr_seq_pac_modelo_w		number(10,0);
ds_retorno_w			varchar2(80);			
			
begin

select	max(nr_sequencia)
into	nr_seq_pac_modelo_w
from	rp_pac_modelo_agendamento 
where	nr_seq_pac_reab = nr_seq_pac_reab_p
and	dt_fim_tratamento is null
and	dt_inativacao is null;

select	max(nr_seq_modelo_agendamento)
into	nr_seq_modelo_agendamento_w
from	rp_pac_modelo_agendamento 
where	nr_sequencia = nr_seq_pac_modelo_w
and	dt_fim_tratamento is null
and	dt_inativacao is null;

if (nr_seq_modelo_agendamento_w is not null) then
	ds_retorno_w := nr_seq_modelo_agendamento_w;
end if;	

return	ds_retorno_w;

end rp_obter_nr_seq_modelo_pac;
/