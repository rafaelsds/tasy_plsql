create or replace
function pls_alterar_texto_mensagem(	ds_texto_p		varchar2,
					ds_texto_ini_p		varchar2,
					ds_texto_fim_p		varchar2,
					ds_frase_p		varchar2,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(4000);
ds_mensagem_w		varchar2(4000);
ds_frase_w		varchar2(255);
nr_parametro1_w		number(10);
nr_paramentro2_w	number(10);
	
-- IE_OPCAO_P
-- UP - frase UPPER
	
begin
/* Obter o primeiro parâmetro para o substr */
nr_parametro1_w := instr(ds_texto_p,ds_texto_ini_p);

/* Obter o segundo parâmetro para o substr */
nr_paramentro2_w := instr(ds_texto_p,ds_texto_fim_p);

/* Frase para substituição */
ds_frase_w := ds_frase_p;

/* Parâmetros IP_OPCAO_P */
if	(upper(ie_opcao_p) = 'UP') then
	ds_frase_w := upper(ds_frase_p);
end if;

/* Trocar frase da mensagem */
select	substr(substr(ds_texto_p,1,nr_parametro1_w - 1) || ds_frase_w || substr(ds_texto_p,nr_paramentro2_w - 1,4000),1,4000)
into	ds_mensagem_w
from	dual;

return	ds_mensagem_w;

end pls_alterar_texto_mensagem;
/