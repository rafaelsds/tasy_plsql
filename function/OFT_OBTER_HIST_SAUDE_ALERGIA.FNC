CREATE OR REPLACE FUNCTION oft_obter_hist_saude_alergia (
    nr_sequencia_p NUMBER
) RETURN VARCHAR2 IS

    ds_retorno_w           VARCHAR2(4000) := NULL;
    ds_classe_w            VARCHAR2(60);
    ds_medic_w             VARCHAR2(100);
    ds_reacao_w            VARCHAR2(100);
    ds_abordagem_w         VARCHAR2(60);
    ds_substancia_w        VARCHAR2(80);
    dt_inicial_w           DATE;
    dt_ultima_w            DATE;
    dt_fim_w               DATE;
    ds_observacao_w        VARCHAR2(4000);
    ds_duracao_w           VARCHAR2(20);
    ds_principio_w         VARCHAR2(100);
    ie_nega_alergias_w     VARCHAR2(10);
    ds_agente_causador_w   VARCHAR2(1000);
BEGIN
    SELECT
        substr(obter_desc_classe_mat(cd_classe_mat), 1, 60),
        substr(obter_desc_material(cd_material), 1, 100),
        substr(obter_desc_reacao_alergica(nr_seq_reacao), 1, 100),
        substr(obter_desc_ficha_tecnica(nr_seq_ficha_tecnica), 1, 80),
        substr(obter_valor_dominio(1337, ie_abordagem), 1, 60),
        dt_inicio,
        dt_ultima,
        substr(ds_observacao, 1, 4000),
        substr(obter_desc_dcb(nr_seq_dcb), 1, 80),
        dt_fim,
        substr(obter_idade(dt_inicio, nvl(dt_fim, sysdate), 'S'), 1, 20),
        ie_nega_alergias,
        substr(obter_desc_alergeno(nr_seq_tipo), 1, 1000) ds_agente_causador
    INTO
        ds_classe_w,
        ds_medic_w,
        ds_reacao_w,
        ds_principio_w,
        ds_abordagem_w,
        dt_inicial_w,
        dt_ultima_w,
        ds_observacao_w,
        ds_substancia_w,
        dt_fim_w,
        ds_duracao_w,
        ie_nega_alergias_w,
        ds_agente_causador_w
    FROM
        paciente_alergia
    WHERE
        nr_sequencia = nr_sequencia_p;

    IF ( ds_agente_causador_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(1024731)
                               || ': '
                               || ds_agente_causador_w
                               || chr(13)
                               || chr(10), 1, 4000); -- Agente causador

    END IF;

    IF ( dt_inicial_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309160)
                               || ' '
                               || to_char(dt_inicial_w, 'dd/mm/yyyy')
                               || chr(13)
                               || chr(10), 1, 4000); -- Desde
    END IF;

    IF ( dt_fim_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309160)
                               || ' '
                               || to_char(dt_inicial_w, 'dd/mm/yyyy')
                               || ' '
                               || wheb_mensagem_pck.get_texto(309164)
                               || ' '
                               || to_char(dt_fim_w, 'dd/mm/yyyy')
                               || '   '
                               || wheb_mensagem_pck.get_texto(309167)
                               || ' '
                               || ds_duracao_w
                               || chr(13)
                               || chr(10), 1, 4000); -- Desde	--ate	--Duracao
    END IF;

    IF ( ie_nega_alergias_w = 'S' ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309170)
                               || chr(13)
                               || chr(10), 1, 4000); --  Nega alergias
    END IF;

    IF ( ds_substancia_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309172)
                               || ': '
                               || ds_substancia_w
                               || chr(13)
                               || chr(10), 1, 4000); -- Substancia
    END IF;

    IF ( ds_principio_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309175)
                               || ': '
                               || ds_principio_w
                               || chr(13)
                               || chr(10), 1, 4000); -- Principio ativo
    END IF;

    IF ( ds_classe_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309177)
                               || ': '
                               || ds_classe_w
                               || chr(13)
                               || chr(10), 1, 4000); -- Classe
    END IF;

    IF ( ds_medic_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309179)
                               || ': '
                               || ds_medic_w
                               || chr(13)
                               || chr(10), 1, 4000); -- Medicamento
    END IF;

    IF ( ds_reacao_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309180)
                               || ': '
                               || ds_reacao_w
                               || chr(13)
                               || chr(10), 1, 4000); -- Reacao
    END IF;

    IF ( ds_abordagem_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309181)
                               || ': '
                               || ds_abordagem_w
                               || chr(13)
                               || chr(10), 1, 4000); -- Abordagem
    END IF;

    IF ( ds_observacao_w IS NOT NULL ) THEN
        ds_retorno_w := substr(ds_retorno_w
                               || wheb_mensagem_pck.get_texto(309183)
                               || ': '
                               || ds_observacao_w
                               || chr(13)
                               || chr(10), 1, 4000);-- Observacao
    END IF;

    RETURN ds_retorno_w;
	
END oft_obter_hist_saude_alergia;
/