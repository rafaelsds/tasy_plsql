create or replace
function Obter_tipo_reserva(nr_seq_reserva_p	Number)
 		    	return Varchar2 is

ie_tipo_reserva_w	varchar2(1);			

/*H - Hospedagem  T - Transporte */
			
begin

select	nvl(max(ie_tipo_reserva),'H')
into	ie_tipo_reserva_w
from 	via_reserva
where 	nr_sequencia = nr_seq_reserva_p;

return	ie_tipo_reserva_w;

end Obter_tipo_reserva;
/