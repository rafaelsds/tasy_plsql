create or replace
function com_obter_ordem_servico_exec(
			dt_inicial_p		varchar2,
			dt_final_p			varchar2,
			ie_classificacao_p		varchar2,
			nm_usuario_exec_p		varchar2,
			cd_setor_usuario_p		number,
			ie_ativ_real_exec_p		varchar2,
			sql_where_p		varchar2,
			nr_seq_ordem_serv_p	number,
			ie_opcao_p		varchar2)
 		    	return varchar2 is
			
/*	Esta function � utilizada no relat�rio CMAN 61156 - Corp
	V -	Verifica se a ordem de servi�o pertence ao executor conforme o par�metro sql_where_p;
	L -	Lista as ordens de servi�o do executor conforme os par�metros;	*/

ds_retorno_w		varchar2(4000);

begin

begin

if	(ie_opcao_p = 'V') then
	ds_retorno_w :=	substr(obter_select_concatenado_bv(
			'select	nvl(max(''S''),''N'')
			from	man_ordem_servico_v a
			where	a.nr_sequencia = ' || nr_seq_ordem_serv_p ||
			replace(replace(replace(replace(sql_where_p,'(','('''),')',''')'),', ',''', '),', ',', '''),null,null),1,1);
elsif	(ie_opcao_p = 'L') then
	ds_retorno_w := substr(obter_select_concatenado_bv(
			'select	a.nr_sequencia
			from	man_ordem_servico_v a
			where	1 = 1 ' ||
			replace(replace(replace(replace(sql_where_p,'(','('''),')',''')'),', ',''', '),', ',', ''') ||
			' and	(((' || '''' || dt_inicial_p || '''' || ') = (''  /  /    '')) or ((trunc(a.dt_ordem_servico)) >= (to_date(' ||
			'''' || dt_inicial_p || '''' || ',''dd/mm/yyyy''))))
			and	(((' || '''' || dt_final_p || '''' || ')  = (''  /  /    '')) or ((trunc(a.dt_ordem_servico)) <= (to_date(' ||
			'''' || dt_final_p || '''' || ',''dd/mm/yyyy''))))
			and	((obter_setor_usuario(a.nm_usuario_exec) = ' || cd_setor_usuario_p || ') or (' || cd_setor_usuario_p || ' = 0))
			and	a.ie_classificacao = ' || '''' || ie_classificacao_p || '''' ||
			' and	a.nm_usuario_exec = ' || '''' || nm_usuario_exec_p || '''' ||
			' and	(' || '''' || ie_ativ_real_exec_p || '''' || ' = ''N''
				or (' || '''' || ie_ativ_real_exec_p || '''' || ' = ''S'' and exists(select	1
									from	man_ordem_serv_ativ x
									where	x.nr_seq_ordem_serv = a.nr_sequencia
									and	x.nm_usuario_exec = a.nm_usuario_exec)))
			order by 1',null,'; '),1,4000);
end if;

exception
when others then
	ds_retorno_w := null;
end;

return	ds_retorno_w;

end com_obter_ordem_servico_exec;
/