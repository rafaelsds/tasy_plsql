create or replace
function GP_obter_nr_seq_prot_pront(nr_atendimento_p		number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar(255);

begin
if (nr_atendimento_p is not null) then

	select	max(c.nr_seq_protocolo)
	into	ds_retorno_w
	from	atendimento_paciente d,
		conta_paciente b,
		protocolo_convenio c
	where	d.nr_atendimento   	= b.nr_atendimento
	and     	b.nr_seq_protocolo 	= c.nr_seq_protocolo
	and     	d.nr_atendimento   	= nr_atendimento_p;

end if;

return	ds_retorno_w;			

end GP_obter_nr_seq_prot_pront;
/