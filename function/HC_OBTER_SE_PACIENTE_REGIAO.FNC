create or replace
function HC_obter_se_paciente_regiao(	CD_PESSOA_FISICA_p	varchar2,
					nr_seq_regiao_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(10);

begin

ds_retorno_w	:= 'S';

if	(CD_PESSOA_FISICA_p is not null) and
	(nvl(nr_seq_regiao_p,0) > 0) then
	
	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	PACIENTE_HOME_CARE a,
		HC_COMO_CHEGAR_PACIENTE b
	where	a.nr_sequencia = b.nr_seq_paciente
	and	CD_PESSOA_FISICA = CD_PESSOA_FISICA_p
	and	nr_seq_regiao = nr_seq_regiao_p;
	
end if;

return	ds_retorno_w;

end HC_obter_se_paciente_regiao;
/