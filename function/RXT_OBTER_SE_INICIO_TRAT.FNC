create or replace
function rxt_obter_se_inicio_trat(	nr_seq_tumor_p	number)
			return 	varchar2 is

ie_retorno_w	varchar2(01);
dt_inicio_trat_w	date;

Cursor C01 is
	select	dt_inicio_trat
	from	rxt_tratamento
	where	nr_seq_tumor	= nr_seq_tumor_p;

begin

ie_retorno_w	:= 'N';
if	(nr_seq_tumor_p is not null) then
	begin
	
	open C01;
	loop
	fetch C01 into	
		dt_inicio_trat_w;
	exit when C01%notfound;
		begin		
		if	(dt_inicio_trat_w is not null) then
			ie_retorno_w	:= 'S';
		else
			ie_retorno_w	:= 'N';
		end if;
		end;
	end loop;
	close C01;

	end;
end if;

return ie_retorno_w;

end rxt_obter_se_inicio_trat;
/