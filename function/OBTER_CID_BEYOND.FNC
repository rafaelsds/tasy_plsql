create or replace
function obter_cid_beyond( nr_atendimento_p	varchar2)	
		           return varchar2 is

qt_registros_w		number(15) := 0;
cd_doenca_cid_w		cid_doenca.cd_doenca_cid%type := null;


begin

if	(nr_atendimento_p is not null) then
	select count(*) qt_itens
	into   qt_registros_w
	from cid_doenca_v a
	where 1 = 1
	and ((exists(select 1 
			     from diagnostico_doenca x 
				 where x.nr_atendimento = nr_atendimento_p
				 and x.cd_doenca =  a.cd_doenca)) or
		 (exists(select 1
			  from lista_problema_pac y
			  where y.cd_pessoa_fisica = obter_pessoa_atendimento(nr_atendimento_p,'C')
			  and y.cd_doenca = a.cd_doenca)));

	if	(qt_registros_w = 1) then
		select max(cd_doenca_cid)
		into   cd_doenca_cid_w
		from cid_doenca_v a
		where 1 = 1
		and ((exists(select 1 
					 from diagnostico_doenca x 
					 where x.nr_atendimento = nr_atendimento_p
					 and x.cd_doenca =  a.cd_doenca)) or
			 (exists(select 1
				  from lista_problema_pac y
				  where y.cd_pessoa_fisica = obter_pessoa_atendimento(nr_atendimento_p,'C')
				  and y.cd_doenca = a.cd_doenca)))
		and rownum = 1;
	end if;
	
end if;

return cd_doenca_cid_w;

end obter_cid_beyond;
/
