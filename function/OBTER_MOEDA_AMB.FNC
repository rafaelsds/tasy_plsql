create or replace
function obter_moeda_amb(cd_edicao_amb_p		number,
			cd_procedimento_p		number)
 		    	return varchar2 is

ds_moeda_w		varchar2(30);
			
begin

select	max(obter_desc_moeda(cd_moeda))
into	ds_moeda_w
from	preco_amb
where	cd_procedimento = cd_procedimento_p
and	cd_edicao_amb = cd_edicao_amb_p;

return	ds_moeda_w;

end obter_moeda_amb;
/