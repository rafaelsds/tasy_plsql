create or replace function obter_regra_fat_onc (nr_sequencia_p can_ordem_item_prescr.nr_sequencia%type) 
return regra_faturamento_onc%rowtype is

r_retorno_w regra_faturamento_onc%rowtype;
cd_estabelecimento_w number := wheb_usuario_pck.get_cd_estabelecimento;
cd_convenio_w number;
nao_utilizado_w varchar2(255 char);
cd_grupo_material_w number;
cd_subgrupo_material_w number;

cursor dados_item_prescrito is
select		item.cd_material,
            nvl(ordem.nr_atendimento, prescricao.nr_atendimento) nr_atendimento,
			material.cd_classe_material,
            material.ie_tipo_material
from		can_ordem_item_prescr item
inner join	material on item.cd_material = material.cd_material
inner join 	can_ordem_prod ordem on item.nr_seq_ordem = ordem.nr_sequencia
inner join  prescr_medica prescricao on ordem.nr_prescricao = prescricao.nr_prescricao
where		item.nr_sequencia = nr_sequencia_p;

begin
    for registro in dados_item_prescrito loop
        obter_convenio_execucao(registro.nr_atendimento, sysdate,
                        cd_convenio_w,
                        nao_utilizado_w,
                        nao_utilizado_w,
                        nao_utilizado_w,
                        nao_utilizado_w);

        cd_grupo_material_w := to_number(obter_dados_material(registro.cd_material, 'CGRU'));
        cd_subgrupo_material_w := to_number(obter_dados_material(registro.cd_material, 'CSUB'));

        begin
            select  r.*
            into    r_retorno_w
            from    (
                select  regra.*
                from    regra_faturamento_onc regra
                where   (regra.cd_material is null or regra.cd_material = registro.cd_material)
                and     (regra.cd_convenio is null or regra.cd_convenio = cd_convenio_w)
                and     (regra.cd_classe_material is null or regra.cd_classe_material = registro.cd_classe_material)
                and     (regra.cd_grupo_material is null or regra.cd_grupo_material = cd_grupo_material_w)
                and     (regra.cd_subgrupo_material is null or regra.cd_subgrupo_material = cd_subgrupo_material_w)
                and     (regra.ie_tipo_material is null or regra.ie_tipo_material = registro.ie_tipo_material)
                and     regra.cd_estabelecimento = cd_estabelecimento_w
                and     (regra.dt_inicio_vigencia is null or sysdate >= regra.dt_inicio_vigencia)
                and     (regra.dt_fim_vigencia is null or sysdate <= regra.dt_inicio_vigencia)
                and     regra.ie_situacao = 'A'
                order by
                    regra.cd_material,
                    regra.cd_convenio,
                    regra.cd_classe_material,
                    regra.cd_grupo_material,
                    regra.cd_subgrupo_material,
                    regra.ie_tipo_material
            ) r where rownum = 1;
        exception when no_data_found then
            null;
        end;
    end loop;

    return r_retorno_w;
end;
/
