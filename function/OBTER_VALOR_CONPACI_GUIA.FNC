create or replace
function obter_valor_conpaci_guia	(nr_interno_conta_p number,
					cd_autorizacao_p varchar2,
					ie_opcao_p  number)
					return number is
vl_guia_w 		number(15,2);
ie_titulo_receber_w	varchar2(1);
/*
1 - valor da guia
*/

begin

select 	obter_valor_conv_estab(cd_convenio_parametro, cd_estabelecimento,'IE_TITULO_RECEBER')
into	ie_titulo_receber_w
from 	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

begin
select  vl_guia
into 	vl_guia_w
from	conta_paciente_guia
where  	cd_autorizacao  = cd_autorizacao_p
and 	nr_interno_conta = nr_interno_conta_p;
exception
	when others then

	if (ie_titulo_receber_w = 'C') then

		select  sum(vl_guia)
		into 	vl_guia_w
		from	conta_paciente_guia
		where	nr_interno_conta = nr_interno_conta_p;	
	
	end if;	
	end;
return 	vl_guia_w;
end;
/
