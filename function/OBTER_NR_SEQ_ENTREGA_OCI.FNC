create or replace
function obter_nr_seq_entrega_oci(		nr_ordem_compra_p		number,
						nr_item_oci_p			number,
						dt_entrega_p			date)
 		    	return number is
			
nr_sequencia_w		ordem_compra_item_entrega.nr_sequencia%type;

begin

select	min(b.nr_sequencia)
into	nr_sequencia_w
from	ordem_compra_item_entrega b,
	ordem_compra_item a
where	a.nr_ordem_compra	= nr_ordem_compra_p
and	a.nr_item_oci		= nr_item_oci_p
and	a.nr_ordem_compra	= b.nr_ordem_compra
and	a.nr_item_oci		= b.nr_item_oci
and	b.dt_cancelamento is null
and	trunc(b.dt_prevista_entrega,'dd') = trunc(dt_entrega_p,'dd');

return	nr_sequencia_w;

end obter_nr_seq_entrega_oci;
/
