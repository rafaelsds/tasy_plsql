create or replace
function cont_obter_tipo_integracao (	nr_seq_log_p		number)
 		    	return varchar2 is

ie_retorno_w		varchar2(1);

nr_seq_informacao_w		log_integracao.nr_seq_informacao%type;
nr_seq_proj_xml_w		informacao_integracao.nr_seq_proj_xml%type;
ds_classe_java_w		informacao_integracao.ds_classe_java%type;
ie_tipo_utilizacao_w	informacao_integracao.ie_tipo_utilizacao%type;
/*
1 - XML - WS - Assíncrono
*/
				
begin

select	max(a.nr_seq_informacao)
into	nr_seq_informacao_w
from	log_integracao a
where	a.nr_sequencia = nr_seq_log_p;

select	max(a.nr_seq_proj_xml),
		max(a.ds_classe_java),
		max(a.ie_tipo_utilizacao)
into	nr_seq_proj_xml_w,
		ds_classe_java_w,
		ie_tipo_utilizacao_w
from	informacao_integracao a
where	a.nr_sequencia = nr_seq_informacao_w;

if	(ie_tipo_utilizacao_w = 'A') and
	(ds_classe_java_w is not null) and
	(nr_seq_proj_xml_w is not null) then
	
	ie_retorno_w	:= '1';

elsif (ie_tipo_utilizacao_w = 'A') and

	(ds_classe_java_w is not null) and

	(nr_seq_proj_xml_w is null) then


	ie_retorno_w	:= '2';
end if;

return	ie_retorno_w;

end cont_obter_tipo_integracao;
/
