create or replace function OBTER_DESC_ESCALA_SAD (QT_SCORE NUMBER)
			      return VARCHAR2 is

DS_RETORNO_W VARCHAR2 (255);
QT_SCORE_W NUMBER;

begin
QT_SCORE_W := nvl(QT_SCORE, 0);

if ((QT_SCORE_W >= 0) AND (QT_SCORE_W <= 4)) then -- Low;
	DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(921281), 1, 255);
elsif ((QT_SCORE_W >= 5) AND (QT_SCORE_W <= 6)) then -- Medium;
	DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(973242), 1, 255);
elsif ((QT_SCORE_W >= 7) AND (QT_SCORE_W <= 10)) then -- High;
	DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(921285), 1, 255);
end if;

return DS_RETORNO_W;

end OBTER_DESC_ESCALA_SAD;
/