create or replace
procedure pls_vincular_senha_lead(	nr_seq_lead_p		number,
				nr_seq_senha_lead_p	number,
				nm_usuario_p		varchar2) is 

nm_pessoa_fisica_w	pls_solicitacao_comercial.nm_pessoa_fisica%type;
cd_pf_vinculado_w		pls_solicitacao_comercial.cd_pf_vinculado%type;

begin

if	(nvl(nr_seq_senha_lead_p,0) <> 0) then
	update	pls_solicitacao_comercial
	set	nr_seq_pac_senha_fila	= nr_seq_senha_lead_p
	where	nr_sequencia		= nr_seq_lead_p;
	
	select	nm_pessoa_fisica,
		cd_pf_vinculado
	into	nm_pessoa_fisica_w,
		cd_pf_vinculado_w
	from	pls_solicitacao_comercial
	where	nr_sequencia	= nr_seq_lead_p;
	
	update	paciente_senha_fila
	set	dt_vinculacao_senha 	= sysdate,
		nm_usuario		= nm_usuario_p,
		nm_paciente		= decode(cd_pf_vinculado_w,null,nm_pessoa_fisica_w,substr(obter_nome_pf(cd_pf_vinculado_w),1,255),null),
		cd_pessoa_fisica 		= cd_pf_vinculado_w
	where	nr_sequencia 		= nr_seq_senha_lead_p;
end if;

commit;

end pls_vincular_senha_lead;
/