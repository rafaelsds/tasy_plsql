create or replace
function obter_proc_tab_interno_hemo	(nr_seq_producao_p	number)
 		    	return number is
			
vl_proc_interno_w	number(10) := null;
cd_procedimento_w	number(10);
nr_seq_proc_interno_w	number(10);
begin
if	(nr_seq_producao_p is not null) then

	select	max(x.cd_procedimento),
		max(x.nr_seq_proc_interno)
	into	cd_procedimento_w,
		nr_seq_proc_interno_w
	from	san_derivado b,
		san_producao a,
		san_derivado_convenio x
	where	1 = 1
	and	a.nr_seq_derivado 		= b.nr_sequencia
	and	x.nr_seq_derivado		= b.nr_sequencia
	and	nvl(x.ie_irradiado,'N')		= nvl(a.ie_irradiado,'N')
	and	nvl(x.ie_lavado,'N')		= nvl(a.ie_lavado,'N')
	and	nvl(x.ie_filtrado,'N')		= nvl(a.ie_filtrado,'N')
	and	nvl(x.ie_aliquotado,'N')	= nvl(a.ie_aliquotado,'N')
	and	nvl(x.ie_aferese,'N')		= nvl(a.ie_aferese,'N')
	and	nvl(x.dt_inicio_vigencia,sysdate)		<= sysdate
	and	a.nr_sequencia					= nr_seq_producao_p;

	vl_proc_interno_w	:= nvl(nr_seq_proc_interno_w, cd_procedimento_w);
end if;	

return	vl_proc_interno_w;

end obter_proc_tab_interno_hemo;
/