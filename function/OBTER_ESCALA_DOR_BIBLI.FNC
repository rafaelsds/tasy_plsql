create or replace
function obter_escala_dor_bibli(cd_escala_dor_p		varchar2)
				return varchar2 is
				
ds_escala_bibliografia_w		varchar2(4000) := '';
ds_escala_bibliografia_pais_w	varchar2(4000);
ds_locale_w						escala_doc_locale.cd_pais%type;
qt_records_w					pls_integer;

begin

ds_locale_w := nvl(nvl(pkg_i18n.get_user_locale,philips_param_pck.get_ds_locale),'pt_BR');

	if (cd_escala_dor_p is not null) then
		begin
		
			select		nvl(substr(obter_desc_expressao(a.cd_exp_informacao),1,4000), a.ds_doc_cliente)
			into			ds_escala_bibliografia_w
			from			escala_dor_documentacao a
			where			a.cd_escala_dor = cd_escala_dor_p;
			
			select			count(*)
			into			qt_records_w
			from			escala_dor_doc_locale a,
							escala_dor_documentacao b
			where 			a.nr_seq_escala_doc = b.nr_sequencia
			and 			b.cd_escala_dor       = cd_escala_dor_p
			and				a.cd_pais          = ds_locale_w;
			  
			  
			if (qt_records_w > 0) then
			  
				select		max(substr(nvl(obter_desc_expressao(a.cd_exp_ref_bibli), a.desc_doc_cliente),1,4000))
				into		ds_escala_bibliografia_pais_w
				from		escala_dor_doc_locale a,
							escala_dor_documentacao b
				where		a.nr_seq_escala_doc = b.nr_sequencia
				and			b.cd_escala_dor       = cd_escala_dor_p
				and			a.cd_pais          = ds_locale_w;
				
			end if; 
		  
		end;
		
	end if;
	
	if (ds_escala_bibliografia_pais_w is not null) then

		ds_escala_bibliografia_w	:= ds_escala_bibliografia_pais_w;
	  
	end if;

return	ds_escala_bibliografia_w;

end obter_escala_dor_bibli;
/