create or replace
function obter_procedimento_aferese(	nr_prescricao_p	number,
					nr_seq_prescr_p	number,
					ie_opcao_p	varchar2)
 		    	return varchar2 is

cd_procedimento_w	number(15);
ds_procedimento_w	varchar2(255);	
nr_seq_proc_interno_w	number(10);
begin

SELECT	max(cd_procedimento), max(nr_seq_proc_interno)
into	cd_procedimento_w,
	nr_seq_proc_interno_w
FROM	prescr_procedimento
WHERE	nr_prescricao = nr_prescricao_p
AND	nr_sequencia = nr_seq_prescr_p;



if (ie_opcao_p = 'D') then

	if	(nvl(nr_seq_proc_interno_w,0) > 0)  then
		select	max(ds_proc_exame)
		into	ds_procedimento_w
		from	proc_interno
		where	nr_sequencia	= nr_seq_proc_interno_w
		and	ie_tipo = 'AT'
		order by ds_proc_exame;
	else
		SELECT	max(substr(a.ds_proc_exame,1,255))
		into	ds_procedimento_w
		FROM   	procedimento b,
			proc_interno a
		WHERE  	a.ie_tipo = 'AT'
		AND    	a.cd_procedimento    = b.cd_procedimento
		AND    	a.ie_origem_proced   = b.ie_origem_proced
		AND	a.cd_procedimento    = cd_procedimento_w   
		ORDER BY a.ds_proc_exame;
end if;

	
end if;

if (ie_opcao_p = 'D') then
	return ds_procedimento_w;
else
	return	cd_procedimento_w;
end if;

end obter_procedimento_aferese;
/