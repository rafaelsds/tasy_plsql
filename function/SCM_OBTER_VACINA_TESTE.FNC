create or replace
function SCM_obter_vacina_teste(	nr_sequencia_p		number,
						nr_atendimento_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(10000) := '';
ds_seq_teste_3_w	varchar2(100);
ds_seq_teste_5_w	varchar2(100);
ds_seq_teste_6_w	varchar2(100);
ds_seq_teste_7_w	varchar2(100);

begin

select	max(decode(x.NR_SEQ_TESTE_VACINA,3,';' || TO_CHAR(x.DT_REGISTRO, 'DD/MM/YYYY HH24:Mi') || ';' || OBTER_DIF_DATA(z.DT_NASCIMENTO,x.DT_REGISTRO,'') || ';',';;;'))
into	ds_seq_teste_3_w
from	atend_vacina_teste x,
		nascimento z
where	x.nr_atendimento = z.nr_atendimento
and		z.nr_sequencia = nr_sequencia_p
and		z.nr_atendimento = nr_atendimento_p
and		ie_situacao = 'A'
and		x.nr_seq_teste_vacina = 3;

select	max(decode(x.NR_SEQ_TESTE_VACINA,5,TO_CHAR(x.DT_REGISTRO, 'DD/MM/YYYY') || ';' || x.DS_LOTE || ';' || NVL(x.DS_OBSERVACAO,';') || ';' ,';;;'))
into	ds_seq_teste_5_w
from	atend_vacina_teste x,
		nascimento z
where	x.nr_atendimento = z.nr_atendimento
and		z.nr_sequencia = nr_sequencia_p
and		z.nr_atendimento = nr_atendimento_p
and		ie_situacao = 'A'
and		x.nr_seq_teste_vacina = 5;

select	max(decode(x.NR_SEQ_TESTE_VACINA,6,TO_CHAR(x.DT_REGISTRO, 'DD/MM/YYYY') || ';' || x.DS_LOTE || ';;' || NVL(x.DS_OBSERVACAO,';') || ';' ,';;;;'))
into	ds_seq_teste_6_w
from	atend_vacina_teste x,
		nascimento z
where	x.nr_atendimento = z.nr_atendimento
and		z.nr_sequencia = nr_sequencia_p
and		z.nr_atendimento = nr_atendimento_p
and		ie_situacao = 'A'
and		x.nr_seq_teste_vacina = 6;

select	max(decode(x.NR_SEQ_TESTE_VACINA,7,TO_CHAR(x.DT_REGISTRO, 'DD/MM/YYYY') || ';' || x.DS_LOTE || ';' || x.DS_OBSERVACAO,';;;'))
into	ds_seq_teste_7_w
from	atend_vacina_teste x,
		nascimento z
where	x.nr_atendimento = z.nr_atendimento
and		z.nr_sequencia = nr_sequencia_p
and		z.nr_atendimento = nr_atendimento_p
and		ie_situacao = 'A'
and		x.nr_seq_teste_vacina = 7;

ds_retorno_w := ds_retorno_w || nvl(ds_seq_teste_3_w,';;;') || nvl(ds_seq_teste_5_w,';;;') || nvl(ds_seq_teste_6_w,';;;;') || nvl(ds_seq_teste_7_w,';;;');

return	ds_retorno_w;

end SCM_obter_vacina_teste;
/