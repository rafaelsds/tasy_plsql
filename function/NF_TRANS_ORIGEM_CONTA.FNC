create or replace 
function nf_trans_origem_conta(nr_sequencia_nf_p number) 
                                                return varchar2 is 
						
nr_sequencia_w			nota_fiscal.nr_sequencia%type; 
nr_sequencia_origem_w  	 	nota_fiscal.nr_sequencia%type; 
nr_conta_origem_w 	        conta_paciente.nr_interno_conta%type;
nr_interno_conta_w		conta_paciente.nr_interno_conta%type;	
nr_atendimento_w		conta_paciente.nr_atendimento%type;

cursor c01 is
select 	nr_conta_origem
from 	matpaci_conta_log
where 	nr_atendimento = nr_atendimento_w
and 	nr_interno_conta = nr_interno_conta_w	
union
select 	nr_conta_origem
from 	propaci_conta_log
where 	nr_atendimento = nr_atendimento_w
and 	nr_interno_conta = nr_interno_conta_w;

begin
	nr_sequencia_w := null;
	
	if   (nr_sequencia_nf_p is not null) then
		
		select	b.nr_interno_conta,
			b.nr_atendimento
		into	nr_interno_conta_w,
			nr_atendimento_w
		from	nota_fiscal a,
			conta_paciente b
		where	a.nr_interno_conta = b.nr_interno_conta
		and	nr_sequencia = nr_sequencia_nf_p
		and	a.nr_interno_conta is not null
		and 	b.nr_atendimento is not null;
		
		if (nr_interno_conta_w is not null and nr_atendimento_w is not null) then
		
			open c01;
			loop
			fetch c01
			into nr_conta_origem_w;
			exit when c01%notfound;
				begin
				   if (nr_conta_origem_w is not null) then
					nr_sequencia_origem_w := obter_nf_conta(nr_conta_origem_w,1);
					
					if (nr_sequencia_origem_w is not null) then
						nr_sequencia_w := nf_canc_origem_conta(nr_sequencia_origem_w);
					end if;
				   end if;
				end;
			end loop;
			close c01;
			
		end if;

	end if; 
	  
return nr_sequencia_w;     
end nf_trans_origem_conta;
/
