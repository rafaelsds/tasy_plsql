create or replace
function get_conta_pac_quimio(		nr_seq_atendimento_p		Number,
									nr_seq_ordem_p				Number)
 		    						return Number is

nr_interno_conta_w	conta_paciente.nr_interno_conta%type;
							
begin

SELECT 	nvl(max(nr_interno_conta),0)
into	nr_interno_conta_w
FROM	paciente_atend_medic b,
		paciente_atendimento e,
		material_atend_paciente c
WHERE	e.nr_seq_atendimento = b.nr_seq_atendimento
and		e.nr_prescricao = c.nr_prescricao
AND	 	b.nr_seq_material = c.nr_sequencia_prescricao					  
and	 	b.nr_seq_atendimento = nr_seq_atendimento_p
and		c.nr_seq_ordem_prod = nr_seq_ordem_p;

return	nr_interno_conta_w;

end get_conta_pac_quimio;
/