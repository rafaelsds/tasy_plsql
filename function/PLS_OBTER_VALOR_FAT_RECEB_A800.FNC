create or replace
function pls_obter_valor_fat_receb_A800
			(	nr_seq_fatura_p		number,
				ie_valor_p		varchar2)
 		    	return number is

vl_retorno_w	number(15,2);

begin

begin
if	(ie_valor_p = 1) then
	select	vl_total_mensalidade
	into	vl_retorno_w
	from	ptu_fatura_pre_total
	where	nr_seq_fatura	= nr_seq_fatura_p;
elsif	(ie_valor_p = 2) then
	select	vl_total_inscricao
	into	vl_retorno_w
	from	ptu_fatura_pre_total
	where	nr_seq_fatura	= nr_seq_fatura_p;
elsif	(ie_valor_p = 3) then
	select	vl_total_ajuste
	into	vl_retorno_w
	from	ptu_fatura_pre_total
	where	nr_seq_fatura	= nr_seq_fatura_p;
elsif	(ie_valor_p = 4) then
	select	vl_tot_mens_original
	into	vl_retorno_w
	from	ptu_fatura_pre_total
	where	nr_seq_fatura	= nr_seq_fatura_p;
elsif	(ie_valor_p = 5) then
	select	vl_tot_insc_original
	into	vl_retorno_w
	from	ptu_fatura_pre_total
	where	nr_seq_fatura	= nr_seq_fatura_p;
elsif	(ie_valor_p = 6) then
	select	qt_total_r802
	into	vl_retorno_w
	from	ptu_fatura_pre_total
	where	nr_seq_fatura	= nr_seq_fatura_p;
end if;
exception
when others then
	vl_retorno_w	:= 0;
end;

return	nvl(vl_retorno_w,0);

end pls_obter_valor_fat_receb_A800;
/