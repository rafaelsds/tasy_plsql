create or replace
function obter_evento_comunic_financ(		nr_sequencia_p	Number,
					cd_evento_p	Varchar2)
					return		varchar2 is

ie_permite_w		varchar2(1);
cd_funcao_w		number(5);

cursor c01 is
select	cd_funcao
from	fin_regra_envio_comunic
where	nr_sequencia	= nr_sequencia_p;

begin

ie_permite_w	:= 'N';

OPEN C01;
LOOP
FETCH C01 into
	cd_funcao_w;
EXIT when c01%notfound;

	if	(ie_permite_w = 'N') then
	
		if	(cd_funcao_w	= 8559) and
			(cd_evento_p	in ('1')) then
			ie_permite_w	:= 'S';
		elsif	(cd_funcao_w	= 89) and
			(cd_evento_p	in ('2')) then
			ie_permite_w	:= 'S';
		elsif	(cd_funcao_w	= 857) and
			(cd_evento_p	in ('3')) then
			ie_permite_w	:= 'S';
		elsif	(cd_funcao_w	= 813) and
			(cd_evento_p	in ('4', '6')) then
			ie_permite_w	:= 'S';
		elsif	(cd_funcao_w	= 815) and
			(cd_evento_p	in ('5')) then
			ie_permite_w	:= 'S';
		end if;
		
	end if;
end loop;
close c01;

return ie_permite_w;

end obter_evento_comunic_financ;
/