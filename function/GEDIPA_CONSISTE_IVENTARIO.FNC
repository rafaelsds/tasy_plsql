create or replace
function gedipa_consiste_iventario(cd_estabelecimento_p		number,
				   cd_material_p		number,
				   cd_local_estoque_p		number)
				   return varchar2 is

ie_bloqueio_inventario_w	varchar2(1);
ds_material_w			varchar2(255);
ds_mensagem_w			varchar2(255) := '';

begin
begin
select	substr(b.ds_material,1,60),
	nvl(a.ie_bloqueio_inventario,'N')
into	ds_material_w,
	ie_bloqueio_inventario_w
from	material b,
	saldo_estoque a
where	b.cd_material		= a.cd_material
and	a.dt_mesano_referencia	= trunc(sysdate,'mm')
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_local_estoque	= cd_local_estoque_p
and	a.cd_material		= cd_material_p;
exception
	when no_data_found then
	begin
	ie_bloqueio_inventario_w := 'N';
	end;
end;
if	(ie_bloqueio_inventario_w = 'S') then
	ds_mensagem_w := WHEB_MENSAGEM_PCK.get_texto(807364, 'DS_MATERIAL=' || ds_material_w);
end if;
	
return	ds_mensagem_w;

end gedipa_consiste_iventario;
/