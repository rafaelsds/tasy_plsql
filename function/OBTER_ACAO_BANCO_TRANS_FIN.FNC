create or replace
function obter_acao_banco_trans_fin(	nr_seq_trans_financ_p	number,
				ie_opcao_p		varchar2)
				return varchar2 is

ie_retorno_w			varchar2(15);

/*
IE_OPCAO_P	
B	= IE_BANCO da Trans financ
BCTB	= Retorna o IE_BANCO da forma cont�bil
	(D = Pagto, C = Recebimento)

*/

begin

select	nvl(max(ie_banco),'N')
into	ie_retorno_w
from	transacao_financeira
where	nr_sequencia	= nr_seq_trans_financ_p;

if	(ie_opcao_p = 'BCTB') then

	select	decode(ie_retorno_w,'D','R','C','P')
	into	ie_retorno_w
	from	dual;
	
end if;

return	ie_retorno_w;

end obter_acao_banco_trans_fin;
/
