create or replace
function agecons_obter_medico_agenda(cd_agenda_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(10);			
			
begin

select	max(cd_pessoa_fisica)
into	ds_retorno_w
from	agenda
where	cd_agenda = cd_agenda_p;

return	ds_retorno_w;

end agecons_obter_medico_agenda;
/