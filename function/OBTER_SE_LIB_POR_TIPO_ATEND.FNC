create or replace
function obter_se_lib_por_tipo_atend(ie_tipo_atend_p	number,
                                    		ie_opcao_p     	number,
                                        		vl_opcao_p     	varchar2)
                      		  return varchar2 is

ds_retorno_w            		varchar2(1) := 'S';
qt_regra_tipo_atend_w   		number(10);
nr_seq_regra_w         		 number(10);

/*
ie_opcao_p
	1 - Tipo conv�nio.
	2 - TISS Tipo Atend
*/
begin

If      (ie_opcao_p = 1) then -- Tipo Conv�nio
        select 	nvl(max(a.nr_sequencia),0)
        into   	nr_seq_regra_w 
        from   	regra_lib_tipo_atend a,
		regra_lib_tipo_atend_conv b
        where  	a.ie_tipo_atendimento	 	= ie_tipo_atend_p
	and	a.nr_sequencia			= b.nr_seq_regra_atend
	and	a.ie_anatomia_patologica	= 'N';

        if      (nr_seq_regra_w > 0) then
                select 	nvl(max(1),0)
                into   	qt_regra_tipo_atend_w
                from    regra_lib_tipo_atend_conv
                where  	nr_seq_regra_w		= nr_seq_regra_atend
                and    	ie_tipo_convenio	= to_number(vl_opcao_p);

                if      (qt_regra_tipo_atend_w > 0) then
                        ds_retorno_w := 'S';
                else    ds_retorno_w := 'N';
                end if;
        end if;
Elsif	(ie_opcao_p = 2) then -- TISS Tipo  Atend
        select 	nvl(max(nr_sequencia),0)
        into   	nr_seq_regra_w
        from   	regra_lib_tipo_atend
        where  	ie_tipo_atendimento		= ie_tipo_atend_p
	and	ie_anatomia_patologica		= 'N';

        if      (nr_seq_regra_w > 0) then
                select 	nvl(max(1),0)
                into   	qt_regra_tipo_atend_w
                from    regra_lib_tipo_atend_tiss
                where  	nr_seq_regra_w	= nr_seq_regra_atend
                and    	ie_tipo_atend_tiss	= to_number(vl_opcao_p);

                if      (qt_regra_tipo_atend_w > 0) then
                        ds_retorno_w := 'S';
                else    ds_retorno_w := 'N';
                end if;
        end if;	
end if;

return  ds_retorno_w;

end obter_se_lib_por_tipo_atend;
/