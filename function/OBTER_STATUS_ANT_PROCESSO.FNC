create or replace 
function Obter_status_ant_processo (
			nr_seq_processo_p	number)
			return varchar2 is

ie_status_w	varchar2(15)	:= 'X';
dt_paciente_w	date;
dt_preparo_w	date;
dt_leitura_w	date;
dt_retirada_w	date;
dt_higienizacao_w	date;
dt_dispensacao_w	date;
dt_processo_w	date;

begin
if	(nr_seq_processo_p is not null) then

	select	max(dt_paciente),
		max(dt_retirada),	
		max(dt_leitura),		
		max(dt_preparo),
		max(dt_higienizacao),
		max(dt_dispensacao),
		max(dt_processo)
	into	dt_paciente_w,
		dt_retirada_w,	
		dt_leitura_w,		
		dt_preparo_w,
		dt_higienizacao_w,
		dt_dispensacao_w,
		dt_processo_w
	from	adep_processo
	where	nr_sequencia = nr_seq_processo_p;

	if	(dt_paciente_w is not null) then
		ie_status_w	:= 'A';
	elsif	(dt_retirada_w is not null) then
		ie_status_w	:= 'R';		
	elsif	(dt_leitura_w is not null) and
		(dt_preparo_w is null) then
		ie_status_w	:= 'L';
	elsif	(dt_leitura_w is not null) and
		(dt_preparo_w is not null) and
		(dt_leitura_w > dt_preparo_w) then
		ie_status_w	:= 'L';
	elsif	(dt_preparo_w is not null) then
		ie_status_w	:= 'P';		
	elsif	(dt_higienizacao_w is not null) then
		ie_status_w	:= 'H';		
	elsif	(dt_dispensacao_w is not null) then
		ie_status_w	:= 'D';
	elsif	(dt_processo_w is not null) then
		ie_status_w	:= 'G';
	end if;
	
	if	(dt_paciente_w is not null) and
		(ie_status_w <> 'A')then
		ie_status_w	:= 'A';
	elsif	(dt_retirada_w is not null)  and
		(ie_status_w <> 'R')then
		ie_status_w	:= 'R';		
	elsif	(dt_leitura_w is not null) and
		(dt_preparo_w is null)  and
		(ie_status_w <> 'L')then
		ie_status_w	:= 'L';
	elsif	(dt_leitura_w is not null) and
		(dt_preparo_w is not null) and
		(dt_leitura_w > dt_preparo_w)  and
		(ie_status_w <> 'L')then
		ie_status_w	:= 'L';
	elsif	(dt_preparo_w is not null)  and
		(ie_status_w <> 'P')then
		ie_status_w	:= 'P';		
	elsif	(dt_higienizacao_w is not null)  and
		(ie_status_w <> 'H')then
		ie_status_w	:= 'H';		
	elsif	(dt_dispensacao_w is not null)  and
		(ie_status_w <> 'D')then
		ie_status_w	:= 'D';
	elsif	(dt_processo_w is not null)  and
		(ie_status_w <> 'G')then
		ie_status_w	:= 'G';
	end if;

end if;

return ie_status_w;

end Obter_status_ant_processo;
/
