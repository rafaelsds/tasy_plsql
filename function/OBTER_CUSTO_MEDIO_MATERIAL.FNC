create or replace
function obter_custo_medio_material(
		cd_estabelecimento_p	number,
		dt_mesano_referencia_p	date,
		cd_material_p		number,
		ie_consignado_p		varchar2  default null)
		return number is

vl_custo_medio_w		number(15,4);
cd_material_w		number(06,0);
ie_custo_medio_ant_w	varchar2(1);
ie_consignado_w		varchar2(1);

Cursor C01 is
	select	nvl(vl_custo_medio,0)
	from	saldo_estoque
	where	cd_material	= cd_material_w
	and	cd_estabelecimento	= nvl(cd_estabelecimento_p, cd_estabelecimento)
	order by	dt_mesano_referencia desc, 
		vl_custo_medio desc;
begin

select	nvl(cd_material_estoque, cd_material),
	ie_consignado
into	cd_material_w,
	ie_consignado_w
from	material
where	cd_material = cd_material_p;

if	(ie_consignado_p = 1) then
	ie_consignado_w := '1';
end if;

if	(ie_consignado_w = '1') then
	begin
	vl_custo_medio_w := obter_custo_medio_consig( cd_estabelecimento_p, null, cd_material_p, null, dt_mesano_referencia_p);
	end;
else
	begin
	select	nvl(max(vl_custo_medio),0)
	into	vl_custo_medio_w
	from	saldo_estoque
	where	dt_mesano_referencia	= ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_mesano_referencia_p)
	and	cd_material		= cd_material_w
	and	cd_estabelecimento	= nvl(cd_estabelecimento_p, cd_estabelecimento);

	if	(vl_custo_medio_w = 0) then
		begin
		select	nvl(max(ie_custo_medio_ant),'N')
		into	ie_custo_medio_ant_w
		from	parametro_estoque
		where	cd_estabelecimento = nvl(cd_estabelecimento_p, cd_estabelecimento);
		
		if	(ie_custo_medio_ant_w = 'S') then
			begin
			open C01;
			loop
			fetch C01 into	
				vl_custo_medio_w;
			exit when ((C01%notfound) or (vl_custo_medio_w > 0));
			end loop;
			close C01;
			end;
		end if;
		end;
	end if;
	end;
end if;

return	vl_custo_medio_w;
end obter_custo_medio_material;
/