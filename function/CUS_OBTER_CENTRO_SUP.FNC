create or replace
function cus_obter_centro_sup(	cd_estabelecimento_p	number,
					cd_centro_controle_p	number)
					return number is

cd_centro_superior_w			number(8);

BEGIN

cd_centro_superior_w			:= null;

if	(cd_estabelecimento_p is not null) and
	(cd_centro_controle_p is not null) then
	
	select	cd_centro_controle_pai
	into	cd_centro_superior_w
	from	centro_controle
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_centro_controle	= cd_centro_controle_p;

end if;

return cd_centro_superior_w;

END cus_obter_centro_sup;
/