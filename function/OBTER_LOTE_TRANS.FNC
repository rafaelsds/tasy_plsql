create or replace
function obter_lote_trans(nr_sequencia_p number)
 		    	return number is

			
ds_retorno_w	number(10);			
begin

select 	max(nr_sequencia)
into	ds_retorno_w
from	nota_fiscal_lote_nfe
where	nr_seq_transmissao = nr_sequencia_p;	

return	ds_retorno_w;

end obter_lote_trans;
/