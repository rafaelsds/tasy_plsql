create or replace
function Obter_Artigo_PF
		(cd_pessoa_fisica_p	Varchar2)
		return Varchar2 is


ie_artigo_w			varchar2(01);
ie_sexo_w			varchar2(01);

begin

select	ie_sexo
into	ie_sexo_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(ie_sexo_w	= 'M') then
	ie_artigo_w		:= 'o';
elsif	(ie_sexo_w	= 'F') then
	ie_artigo_w			:= 'a';
end if;

return ie_artigo_w;

end Obter_Artigo_PF;
/
