create or replace function pfcs_func_tl_device_usage
(
  ds_device_type_p in varchar2,
  ds_filter_type_p in varchar2,
  cd_estabelecimento_p	number
) return number as
nr_retorno_w						number(10);

/*
   Function "pfcs_func_tl_device_usage" is used provide device use, device available and borken device count.
*/

begin
	nr_retorno_w := 0;	
	-- ds_filter_type 'A' is for all avialable devices
	if (ds_filter_type_p = 'A') then
		select	count(*) into nr_retorno_w
		from 	pfcs_device dev,
				estabelecimento estab
		where	(dev.si_status in ('INACTIVE') or dev.si_status is null)
		and		dev.ds_device_type = 'Monitor'
        and     dev.cd_estabelecimento = estab.cd_estabelecimento
        and     estab.cd_estabelecimento = cd_estabelecimento_p;
	-- ds_filter_type 'U' is for all used devices		
	elsif (ds_filter_type_p = 'U') then
		select count(1) into nr_retorno_w
		from	pfcs_service_request sr,
		        pfcs_encounter enc,
                pfcs_patient pat,
                pfcs_encounter_location el,
                pfcs_device dev,
                unidade_atendimento uni,
                setor_atendimento sa
        where	( (sr.si_status = 'COMPLETED' and sr.cd_service = 'E0404') or
                (sr.si_status = 'ACTIVE' and sr.cd_service = 'E0403'))
        and 	sr.nr_seq_encounter = enc.nr_sequencia
        and     dev.si_status = 'ACTIVE'
        and		dev.ds_device_type = 'Monitor'
        and 	pat.nr_sequencia = dev.nr_seq_patient
        and 	pat.ie_active = '1'
        and		enc.nr_seq_patient = pat.nr_sequencia
        and		enc.si_status in ('PLANNED', 'ARRIVED')
        and		el.nr_seq_encounter = enc.nr_sequencia
        and 	uni.nr_seq_location = el.nr_seq_location
        and		uni.cd_setor_atendimento = sa.cd_setor_atendimento
        and 	sa.ie_situacao = 'A'
        and 	uni.ie_situacao = 'A'
        and 	sa.cd_classif_setor in ('1','3','4','9','11','12')
        and     sa.cd_estabelecimento_base = cd_estabelecimento_p;
	-- ds_filter_type B_L_D is for Borken/Lost devices
	elsif (ds_filter_type_p = 'B_L_D') then
		select	count(*) into nr_retorno_w
    	from 	pfcs_device dev,
				estabelecimento estab
    	where	dev.si_status in ('UNKNOWN')
    	and		dev.ds_device_type = 'Monitor'
        and     dev.cd_estabelecimento = estab.cd_estabelecimento
        and     estab.cd_estabelecimento = cd_estabelecimento_p;
    end if;

	return nr_retorno_w;

	exception
		-- If there are no records found
		when no_data_found then
		  return nr_retorno_w;

end pfcs_func_tl_device_usage;
/
