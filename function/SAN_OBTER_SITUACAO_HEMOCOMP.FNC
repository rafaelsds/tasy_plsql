create or replace
function san_obter_situacao_hemocomp(	ie_opcao_p			varchar2,
					nr_seq_transfusao_p		number)
 		    	return varchar2 is
ie_status_w 	varchar2(1);

begin
if (ie_opcao_p = 'I') then
	select  max(ie_irradiado)
	into	ie_status_w
        from	san_producao
	where	nr_seq_transfusao = nr_seq_transfusao_p;


elsif (ie_opcao_p = 'F') then
	select  max(ie_filtrado)
	into	ie_status_w
        from	san_producao
	where	nr_seq_transfusao = nr_seq_transfusao_p;
	
elsif (ie_opcao_p = 'L') then
	select  max(ie_lavado)
	into	ie_status_w
        from	san_producao
	where	nr_seq_transfusao = nr_seq_transfusao_p;
	
elsif (ie_opcao_p = 'A') then
	select  max(ie_aliquotado)
	into	ie_status_w
        from	san_producao
	where	nr_seq_transfusao = nr_seq_transfusao_p;
end if;
	
return	ie_status_w;

end san_obter_situacao_hemocomp;
/