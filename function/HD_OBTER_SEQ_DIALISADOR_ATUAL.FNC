create or replace
function HD_Obter_Seq_Dialisador_Atual (nr_seq_dialise_p	number)
					return			number is
					
nr_seq_dialise_dialis_w		number(10,0);

begin

select	max(nr_sequencia)
into	nr_seq_dialise_dialis_w
from	hd_dialise_dialisador
where	nr_seq_dialise		= nr_seq_dialise_p
and	dt_montagem is not null
and	dt_retirada is null;

return nr_seq_dialise_dialis_w;

end HD_Obter_Seq_Dialisador_Atual;
/