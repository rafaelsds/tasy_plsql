create or replace function obter_result_risco_insuf_renal(nr_sequencia_p	number)
			return varchar2 is

ie_creatinina_w				varchar2(1);
ie_clearance_w				varchar2(1);
qt_risco_leve_moderado_w	number(10);
ds_retorno_w		        varchar2(4000);
sql_w                       varchar2(200);
begin
if	(nr_sequencia_p is not null) then
	select	nvl(ie_creatinina,'N'),
			nvl(ie_clearance,'N'),
			(decode(nvl(ie_clearance_maior,'N'),'S',1,0) +
			decode(nvl(ie_icc,'N'),'S',1,0) +
			decode(nvl(ie_dm,'N'),'S',1,0) +
			decode(nvl(ie_ictericia,'N'),'S',1,0) +
			decode(nvl(ie_desidratacao,'N'),'S',1,0) +
			decode(nvl(ie_rel_ureia_creat,'N'),'S',1,0))
	into	ie_creatinina_w,
			ie_clearance_w,
			qt_risco_leve_moderado_w
	from	escala_risco_insuf_renal
	where	nr_sequencia = nr_sequencia_p;
--INICIO MD 1

    BEGIN
        sql_w := 'CALL OBTER_DS_EXP_RIS_INS_RENAL_MD(:1, :2, :3, :4, :5, :6) INTO :ds_retorno_w';
        EXECUTE IMMEDIATE sql_w
            USING IN ie_creatinina_w ,IN ie_clearance_w, IN qt_risco_leve_moderado_w,
                  IN obter_desc_expressao(283400), IN obter_desc_expressao(347040), 
                  IN obter_desc_expressao(347844), OUT ds_retorno_w; 

    EXCEPTION
        WHEN OTHERS THEN
            ds_retorno_w := NULL;
    END;

    RETURN ds_retorno_w;

else
	return	'';
    --FIM MD 1
end if;

end obter_result_risco_insuf_renal;
/