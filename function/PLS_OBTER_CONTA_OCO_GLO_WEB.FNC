create or replace
function pls_obter_conta_oco_glo_web (  nr_seq_protocolo_p		number,
					nr_seq_ocorrencia_p 		number,
					nr_seq_motivo_glosa_p 		number,
					ie_tipo_p			varchar2)
				   return number is
/*
O - Verifica total de contas com ocorrências no modelo atual de importação.
ON - Verifica o total de contas com ocorrências no novo modelo de importação.
*/
qt_registros_w	number(10);

begin

if	(ie_tipo_p = 'O') then
	select 	count(distinct b.nr_sequencia)
	into	qt_registros_w
	from 	pls_ocorrencia_benef a,
		pls_conta b
	where	a.nr_seq_conta 		= b.nr_sequencia
	and	b.nr_seq_protocolo	= nr_seq_protocolo_p
	and 	a.nr_seq_ocorrencia	= nr_seq_ocorrencia_p;
	
elsif	(ie_tipo_p = 'ON') then
	select 	count(distinct b.nr_sequencia)
	into	qt_registros_w
	from 	pls_ocorrencia_imp a,
		pls_conta_imp b
	where	a.nr_seq_conta 		= b.nr_sequencia
	and	b.nr_seq_protocolo	= nr_seq_protocolo_p
	and 	a.nr_seq_ocorrencia	= nr_seq_ocorrencia_p;
	
else
	select 	count(distinct b.nr_sequencia)
	into	qt_registros_w
	from 	pls_conta_glosa a,
		pls_conta b
	where	a.nr_seq_conta 		= b.nr_sequencia
	and	b.nr_seq_protocolo	= nr_seq_protocolo_p
	and 	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_p;
end if;


return qt_registros_w;

end pls_obter_conta_oco_glo_web;
/