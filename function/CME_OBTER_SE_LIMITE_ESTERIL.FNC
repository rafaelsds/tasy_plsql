create or replace
function cme_obter_se_limite_esteril(nr_seq_conjunto_p	number)
				return varchar2 is

qt_conj_esterilizados_w		number(10);
qt_limite_esterilizacao_w	number(10);
ie_limite_w			varchar2(1);

begin

ie_limite_w	:= 'N';

if	(nr_seq_conjunto_p is not null) then
	begin
	select	nvl(max(qt_limite_esterilizacao),0)
	into	qt_limite_esterilizacao_w
	from	cm_conjunto
	where	nr_sequencia	= nr_seq_conjunto_p
	and	ie_situacao	= 'A';

	if	(qt_limite_esterilizacao_w <> 0) then
		begin
		select	count(*)
		into	qt_conj_esterilizados_w
		from	cm_conjunto_cont
		where	nr_seq_conjunto = nr_seq_conjunto_p
		and	ie_status_conjunto = 3
		and	nvl(ie_situacao,'A') = 'A';

		if	(qt_conj_esterilizados_w >= qt_limite_esterilizacao_w) then
			ie_limite_w	:= 'S';	
		end if;
		end;
	end if;
	end;
end if;

return ie_limite_w;

end cme_obter_se_limite_esteril;
/