create or replace
function obter_motivo_bloq_agecons	(nr_seq_agenda_p	number,
					nr_seq_motivo_p	number)
					return varchar2 is

ds_motivo_obs_w	varchar2(255);
ds_motivo_cad_w	varchar2(255);
ds_motivo_w		varchar2(255);
ds_responsavel_w	varchar2(40);
nm_usuario_status_w	varchar2(15);

begin
if	(nr_seq_agenda_p is not null) then
	/* obter motivo observacao */
	select	max(ds_motivo_status),
		'(' || max(substr(nm_usuario_status,1,15)) || ' em ' || to_char(max(dt_status),'dd/mm/yyyy hh24:mi:ss') || ')',
		max(nm_usuario_status)
	into	ds_motivo_obs_w,
		ds_responsavel_w,
		nm_usuario_status_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_p;

	/* obter motivo cadastro */
	select	max(ds_motivo)
	into	ds_motivo_cad_w
	from	agenda_motivo
	where	nr_sequencia = nr_seq_motivo_p;

	if	(ds_motivo_obs_w is not null) and
		(ds_motivo_cad_w is not null) then
		ds_motivo_w := substr(ds_motivo_obs_w || ' - ' || ds_motivo_cad_w|| ' ' || ds_responsavel_w ,1,255) ;
	elsif	(nm_usuario_status_w is not null) then
		ds_motivo_w := substr(nvl(ds_motivo_obs_w, ds_motivo_cad_w) || ' ' || ds_responsavel_w,1,255);
	end if;
end if;

return ds_motivo_w;

end obter_motivo_bloq_agecons;
/
