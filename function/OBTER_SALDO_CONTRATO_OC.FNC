create or replace
function obter_saldo_contrato_oc(	nr_seq_contrato_p		number)
 		    	return number is

vl_total_contrato_w		number(15,2);
vl_total_ordem_w		number(15,4);
vl_saldo_w		number(15,4);
			
begin
vl_saldo_w	:= 0;

select	nvl(vl_total_contrato,0)
into	vl_total_contrato_w
from	contrato
where	nr_sequencia = nr_seq_contrato_p;

select	nvl(sum(obter_valor_liquido_ordem(nr_ordem_compra)),0)
into	vl_total_ordem_w
from	ordem_compra
where	nr_seq_contrato = nr_seq_contrato_p
and	(nr_seq_motivo_cancel is null)
and	(obter_se_ordem_reprovada(nr_ordem_compra) = 'N')
and	(dt_liberacao is not null);

vl_saldo_w := vl_total_contrato_w - vl_total_ordem_w;

return	vl_saldo_w;

end obter_saldo_contrato_oc;
/
