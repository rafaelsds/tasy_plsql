create or replace
function pls_valida_dia_mes(	dt_ini_ent_p		in	varchar2,
				dt_fim_ent_p		in	varchar2,
				dt_registro_p		in	date,
				ie_tipo_saida_p		in	varchar2) return date is
/*
ie_tipo_saida_p
	I = in�cio
	F = fim
*/

dt_retorno_w		Date;
ds_dia_filtro_w		Varchar2(2);
ds_dia_data_w		Varchar2(2);

begin

if	(ie_tipo_saida_p = 'I') then
	if	(dt_ini_ent_p is null) or (dt_registro_p is null) then
		dt_retorno_w := to_date('01/01/' || to_char(dt_registro_p,'yy'));
	else
		dt_retorno_w := to_date(dt_ini_ent_p || '/' || to_char(dt_registro_p,'yy'));
	end if;
	
elsif	(ie_tipo_saida_p = 'F') then
	if	(dt_fim_ent_p is null) or (dt_registro_p is null) then
		dt_retorno_w := to_date('31/12/' || to_char(dt_registro_p,'yy'));
	else
		-- Pegamos o dia vindo do filtro pois como o filtro n�o � um componente de data o mesmo permite informar por exemplo, '31/04', neste caso pegamos o valor '30'
		ds_dia_filtro_w := substr(dt_fim_ent_p, 1, 2);
		
		-- Pegamos o �ltimo dia do m�s, por exemplo, seguindo o exemplo do coment�rio acima, '30', visto que o m�s '04' tem apenas '30' dias
		ds_dia_data_w	:= to_char(last_day(to_date('01/' || substr(dt_fim_ent_p, 4, 2) || to_char(dt_registro_p,'yy'))), 'dd');
		
		-- Se o dia vindo do filtro for maior que o �ltimo dia do m�s vindo do filtro, por exemplo, '31/04' n�o existe, ent�o substitu�mos a data para o �ltimo dia do m�s, retornando '30/04'
		if	(to_number(ds_dia_filtro_w) > to_number(ds_dia_data_w)) then
			dt_retorno_w := to_date(ds_dia_data_w || '/' || substr(dt_fim_ent_p, 4, 2) || '/' || to_char(dt_registro_p,'yy'));
		else
			dt_retorno_w := to_date(dt_fim_ent_p || '/' || to_char(dt_registro_p,'yy'));
		end if;
	end if;
end if;

return dt_retorno_w;

end pls_valida_dia_mes;
/