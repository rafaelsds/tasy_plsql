create or replace
function pls_obter_se_guia_glosada
			(	nr_seq_guia_p		Number)
 		    		return Varchar2 is

ie_retorno_w			Varchar2(1)	:= 'N';
qt_glosa_w			Number(5)	:= 0;
qt_glosa_guia_w			Number(5)	:= 0;
qt_glosa_guia_proc_w		Number(5)	:= 0;
qt_glosa_guia_mat_w		Number(5)	:= 0;

begin

select	count(*)
into	qt_glosa_guia_w
from	pls_guia_glosa
where	nr_seq_guia = nr_seq_guia_p;

select	count(*)
into	qt_glosa_guia_proc_w
from	pls_guia_glosa
where	nr_seq_guia_proc in (	select	nr_sequencia 
				from 	pls_guia_plano_proc 
				where 	nr_seq_guia = nr_seq_guia_p
				and	nr_seq_motivo_exc is null)
and	(nr_seq_ocorrencia is null
or	nr_seq_ocorrencia in (	select  a.nr_sequencia
				from	pls_ocorrencia a,
					pls_ocorrencia_benef b
				where	a.nr_sequencia = b.nr_seq_ocorrencia
				and	b.nr_seq_guia_plano = nr_seq_guia_p
				and	a.ie_auditoria <> 'S'));				

select	count(*)
into	qt_glosa_guia_mat_w
from	pls_guia_glosa
where	nr_seq_guia_mat in (	select	nr_sequencia 
				from 	pls_guia_plano_mat 
				where 	nr_seq_guia = nr_seq_guia_p
				and	nr_seq_motivo_exc is null)
and	(nr_seq_ocorrencia is null
or	nr_seq_ocorrencia in (	select  a.nr_sequencia
				from	pls_ocorrencia a,
					pls_ocorrencia_benef b
				where	a.nr_sequencia = b.nr_seq_ocorrencia
				and	b.nr_seq_guia_plano = nr_seq_guia_p
				and	a.ie_auditoria <> 'S'));

qt_glosa_w	:= qt_glosa_guia_w + qt_glosa_guia_proc_w + qt_glosa_guia_mat_w;

if	(qt_glosa_w	> 0) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end pls_obter_se_guia_glosada;
/
