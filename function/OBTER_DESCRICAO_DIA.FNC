create or replace
FUNCTION obter_descricao_dia (	dt_dia_p	DATE) RETURN VARCHAR2 IS
nr_seq_messagem_w	NUMBER;
BEGIN

CASE	pkg_date_utils.get_weekday(dt_dia_p)
	WHEN 1 THEN
		nr_seq_messagem_w := 357985;
	WHEN 2 THEN
		nr_seq_messagem_w := 357979;
	WHEN 3 THEN
		nr_seq_messagem_w := 357980;
	WHEN 4 THEN
		nr_seq_messagem_w := 357981;
	WHEN 5 THEN
		nr_seq_messagem_w := 357982;
	WHEN 6 THEN
		nr_seq_messagem_w := 357983;
	WHEN 7 THEN
		nr_seq_messagem_w := 357984;
	ELSE
		RETURN	'';
END CASE;
RETURN wheb_mensagem_pck.get_texto(nr_seq_messagem_w);
END obter_descricao_dia;
/