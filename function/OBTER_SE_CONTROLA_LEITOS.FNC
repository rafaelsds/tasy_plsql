create or replace
function obter_se_controla_leitos(
				cd_setor_atendimento_p		number,
				cd_unidade_basica_p		varchar2,
				cd_unidade_compl_p		varchar2)
 		    	return varchar2 is

			
			
ie_retorno_w			varchar2(2) := 'N';			
qt_existe_w			number(5);
begin


select	nvl(max(ie_mostra_gestao_disp),'N')
into	ie_retorno_w
from	setor_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_p;

if	(ie_retorno_w = 'S') then 
	begin
	select	count(*)
	into	qt_existe_w
	from	unidade_atendimento
	where	ie_mostra_gestao_disp = 'S'
	and	cd_setor_atendimento = cd_setor_atendimento_p;
	
	if	(qt_existe_w > 0) then
		begin
		select	nvl(max(ie_mostra_gestao_disp),'N')
		into	ie_retorno_w
		from	unidade_atendimento
		where	cd_setor_atendimento 	= 	cd_setor_atendimento_p
		and	cd_unidade_basica	=	cd_unidade_basica_p
		and	cd_unidade_compl	=	cd_unidade_compl_p;
		end;
	end if;
	
	end;
end if;

return	ie_retorno_w;

end obter_se_controla_leitos;
/
