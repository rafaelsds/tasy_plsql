create or replace
function verifica_Pac_Atend_Grupo(	cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is
			
ds_grupo_w	varchar2(60);	
nr_seq_grupo_w	number(10);
ds_mensagem_w	varchar2(100);



begin

if	(cd_pessoa_fisica_p is not null) then
	
	begin
	
		select	c.nr_seq_grupo
		into	nr_seq_grupo_w
		from	regra_agenda_grupo_atend c
		where	c.ie_situacao	= 'A'
		and	rownum	= 1;
	exception
		when others then
		nr_seq_grupo_w	:= null;
	end;

	if	(nr_seq_grupo_w is not null) then
		select	max(b.ds_grupo)
		into	ds_grupo_w
		from	pac_grupo_atend a,
			pac_grupo b
		where	a.nr_seq_grupo	= b.nr_sequencia
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	a.nr_seq_grupo in (
					select	c.nr_seq_grupo
					from	regra_agenda_grupo_atend c
					where	c.ie_situacao	= 'A');
	end if;

	if	(ds_grupo_w is not null) then
		ds_mensagem_w	:= substr(wheb_mensagem_pck.get_texto(796787,'DS_GRUPO_W='||ds_grupo_w),1,100);
	end if;

end if;

return	ds_mensagem_w;

end verifica_Pac_Atend_Grupo;
/