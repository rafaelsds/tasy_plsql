create or replace
function Obter_estab_protocolo(nr_seq_protocolo_p number)
 		    	return number is
cd_estab_protocolo_w	number(5):= 0;
begin
select	max(cd_estabelecimento)
into	cd_estab_protocolo_w
from	protocolo_convenio
where	nr_seq_protocolo = nr_seq_protocolo_p;

return	cd_estab_protocolo_w;

end Obter_estab_protocolo;
/