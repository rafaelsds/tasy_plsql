create or replace function obter_se_dieta_prescrita_cpoe(
            	nr_atendimento_p      		number,
            	cd_pessoa_fisica_p    		varchar2
)   return varchar2 is
    
    	ie_dieta_precr_w   varchar2(1);    
    
begin

	select 	decode(count(*), 0, 'N', 'S') 
	into    	ie_dieta_precr_w
    	from    	cpoe_dieta
    	where   	((nr_atendimento   = nr_atendimento_p)
            		or  (cd_pessoa_fisica = cd_pessoa_fisica_p 
            	and 	nr_atendimento is null))
    	and     	dt_liberacao is null
    	and     	dt_suspensao is null
    	and     	(dt_fim is null 	or dt_fim > sysdate);
  
    	return ie_dieta_precr_w;

end obter_se_dieta_prescrita_cpoe;
/