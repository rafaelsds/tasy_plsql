create or replace
function nut_obter_custo_lanche(nr_seq_tipo_lanche_p  number)
 		    	return number is
			
  ds_retorno_w	number(10,2);
  
begin

if	(nr_Seq_tipo_lanche_p is not null) then
	select 	max(qt_custo_lanche)
	into	ds_retorno_w
	from   	nut_tipo_lanche
	where	ie_situacao = 'A'
	and	nr_sequencia = nr_seq_tipo_lanche_p;
end if;

return	ds_retorno_w;

end nut_obter_custo_lanche;
/
