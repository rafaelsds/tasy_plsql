create or replace
function obter_origem_princ_laudo_sus(nr_seq_atend_futuro_p	number)
 		    	return varchar2 is

ie_origem_proced_w	number(10);

begin

select 	max(ie_origem_proced)
into	ie_origem_proced_w
from   	atend_pac_proced_previsto
where  	ie_proc_principal 	= 'S'
and	nr_seq_atend_futuro 	= nr_seq_atend_futuro_p;

return	ie_origem_proced_w;

end obter_origem_princ_laudo_sus;
/