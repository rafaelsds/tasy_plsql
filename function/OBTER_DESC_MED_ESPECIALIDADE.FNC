create or replace
function obter_desc_med_especialidade(	cd_especialidade_p	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);			
	
begin

if	(cd_especialidade_p	is not null) then
	
	select	max(substr(obter_descricao_padrao('ESPECIALIDADE_MEDICA','DS_ESPECIALIDADE',CD_ESPECIALIDADE),1,50))
	into	ds_retorno_w
	from	medico_especialidade
	where	cd_especialidade = cd_especialidade_p;

end if;

return	ds_retorno_w;

end obter_desc_med_especialidade;
/
