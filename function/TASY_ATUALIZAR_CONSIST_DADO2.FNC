create or replace
function TASY_ATUALIZAR_CONSIST_DADO2(nm_owner_origem_p Varchar2) return  varchar2 IS 

nm_tabela_w	varchar2(50);
qt_rows_tabela	number;
qt_rows_tabela_import	number;
ds_retorno	varchar2(255);
ds_inconsistencias_p varchar2(4000);

cursor C01 is
	select	table_name
	from 	user_tables ut,
		tabela_sistema ts
	where	ts.nm_tabela = ut.table_name
	and	ts.ie_exportar = 'S'
	and	upper(ut.table_name) in (	'INDICE',
						'INDICE_ATRIBUTO',
						'INTEGRIDADE_ATRIBUTO',
						'INTEGRIDADE_REFERENCIAL',
						'OBJETO_SISTEMA_PARAM',
						'DIC_OBJETO',
						'DIC_OBJETO_FILTRO',
						'FUNCAO_PARAMETRO',
						'XML_PROJETO',
						'XML_ATRIBUTO',
						'XML_ELEMENTO');

Begin
ds_retorno	:= '';

open C01;
loop
fetch C01 into
	nm_tabela_w;
exit when C01%notfound;
	begin
		begin
			execute immediate 'select (select (count(1) + 25) from ' || nm_tabela_w || '), (select count(1) from ' || nm_owner_origem_p || '.' || nm_tabela_w || ') from dual' 
			into qt_rows_tabela, qt_rows_tabela_import;
		
			if	(qt_rows_tabela) < qt_rows_tabela_import then
				ds_retorno := ds_retorno || nm_tabela_w || ': ' || qt_rows_tabela || ', ' || qt_rows_tabela_import || '.'|| chr(13) || chr(10);
			end if;
		exception
			WHEN OTHERS THEN 
				null;
		end;
	end;
end loop;

close C01;

return ds_retorno;

end Tasy_Atualizar_Consist_Dado2;
/
