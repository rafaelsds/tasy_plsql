create or replace
function Obter_quantidades_OS_HTML5(	nm_usuario_prev_p	varchar2,
										dt_prevista_p		date,
										ie_opcao_p			varchar2)
 		    	return number is

qt_previstas_w		number(10);
qt_min_prev_w		number(10);
qt_min_exec_w		number(10);
qt_retorno_w		number(10);

begin

select	sum(qt_previstas),
		sum(qt_min_prev),
		sum(qt_min_exec)
into	qt_previstas_w,
		qt_min_prev_w,
		qt_min_exec_w
from	(
select	count(*) qt_previstas, -- 'Atividades previstas'
		nvl(sum(a.qt_min_prev),0) qt_min_prev,
		nvl(sum(a.qt_min_exec),0) qt_min_exec
from	man_os_prevista_v a
where	a.nm_usuario_prev = nm_usuario_prev_p
and	nvl(substr(man_obter_se_mostra_os_confid(a.nr_sequencia,nm_usuario_prev_p),1,1),'S') = 'S'
and	a.dt_prevista between trunc(to_date(dt_prevista_p),'dd') and fim_dia(to_date(dt_prevista_p))
and	((ie_opcao_p <> 'FIM') or (a.ie_finalizado = 'S'))
and	(nvl(a.ie_situacao_os,'0') <> '3'
	or exists(	select	1
			from	man_ordem_serv_ativ x 
			where	x.nr_seq_ordem_serv 	= a.nr_sequencia 
			and	x.nm_usuario_exec	= a.nm_usuario_prev
			and	trunc(x.dt_atividade,'dd') = trunc(a.dt_prevista,'dd')))
union
select	count(*) qt_previstas, --'Executadas s/ previs�o'
		nvl(sum(man_obter_min_ordem_prev_usu(a.nr_sequencia,y.nm_usuario_exec, y.dt_atividade)),0) qt_min_prev,
		nvl(sum(qt_minuto),0)
from	man_ordem_serv_ativ y,
		man_ordem_servico_v a
where	a.nr_sequencia 		= y.nr_seq_ordem_serv
and		y.nm_usuario_exec 	= nm_usuario_prev_p
and		nvl(substr(man_obter_se_mostra_os_confid(a.nr_sequencia,nm_usuario_prev_p),1,1),'S') = 'S'
and		y.dt_atividade between trunc(to_date(dt_prevista_p),'dd') and fim_dia(to_date(dt_prevista_p))
and	((ie_opcao_p <> 'FIM') or (obter_se_ativ_real_pendente(a.nr_sequencia,0,y.nm_usuario_exec) = 'S'))
and not exists (select	1
		from	man_ordem_ativ_prev x
		where	x.nr_seq_ordem_serv = a.nr_sequencia
		and	trunc(x.dt_prevista) = trunc(to_date(dt_prevista_p))
		and	x.nm_usuario_prev = nm_usuario_prev_p)
);

if	(ie_opcao_p in('QTP','FIM')) then
	qt_retorno_w	:= qt_previstas_w;
elsif	(ie_opcao_p = 'MINP') then
	qt_retorno_w	:= qt_min_prev_w;
elsif	(ie_opcao_p = 'MINE') then
	qt_retorno_w	:= qt_min_exec_w;
end if;

return	qt_retorno_w;

end Obter_quantidades_OS_HTML5;
/