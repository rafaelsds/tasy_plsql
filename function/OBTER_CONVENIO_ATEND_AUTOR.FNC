CREATE OR REPLACE
FUNCTION Obter_Convenio_Atend_autor(
					nr_atendimento_p			number)
					RETURN varchar2 IS

cd_convenio_w			Number(5,0);
ds_convenio_W			varchar2(255);

BEGIN

select nvl(max(cd_convenio),0)
into cd_convenio_w
from 	Atend_categoria_convenio a
where a.nr_atendimento		= nr_atendimento_p
  and a.dt_inicio_vigencia	= 
	(select max(dt_inicio_vigencia)
	from Atend_categoria_convenio b
	where nr_atendimento	= nr_atendimento_p);
	
if	(cd_convenio_w is not null) then
	begin
	select	ds_convenio
	into	ds_convenio_w
	from	convenio
	where	cd_convenio = cd_convenio_w;
	end;
end if;	

RETURN ds_convenio_w;
END Obter_Convenio_Atend_autor;
/
