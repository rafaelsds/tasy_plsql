create or replace
function Obter_Desc_Moeda
		(cd_moeda_p  number) 
		return varchar2 is 

ds_moeda_w varchar2(30);

begin

select ds_moeda
into ds_moeda_w
from moeda
where cd_moeda = cd_moeda_p;

return ds_moeda_w;

end Obter_Desc_Moeda;
/