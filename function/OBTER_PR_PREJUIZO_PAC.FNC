create or replace
function obter_pr_prejuizo_pac( vl_pacote_p	Number,
				vl_aberto_p	Number)
 		    	return number is

pr_retorno	Number(15,2);
begin

if (vl_pacote_p >= vl_aberto_p) then
  pr_retorno:= 0;
else
  pr_retorno:= abs(((dividir(vl_pacote_p,vl_aberto_p)*100)-100));
end if;

return	pr_retorno;

end obter_pr_prejuizo_pac;
/