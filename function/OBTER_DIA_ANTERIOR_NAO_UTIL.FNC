create or replace
function obter_dia_anterior_nao_util
		(cd_estabelecimento_p	number,
		dt_referencia_p		date) return date is

dt_referencia_w		date;
dt_retorno_w		date;
ie_feriado_w		number(2,0);


begin
dt_referencia_w		:= dt_referencia_p;
dt_retorno_w		:= null;

if	(dt_referencia_w is not null) then
	while	(dt_retorno_w is null) loop
		ie_feriado_w	:= obter_se_feriado(cd_estabelecimento_p, dt_referencia_w);

		if	((ie_feriado_w = 1) or
			(pkg_date_utils.is_business_day(dt_referencia_w) = 0)) then
			dt_retorno_w	:= dt_referencia_w;
		else
			dt_referencia_w	:= dt_referencia_w - 1;
		end if;
	end loop;
end if;

return dt_retorno_w;

end OBTER_DIA_ANTERIOR_NAO_UTIL;
/