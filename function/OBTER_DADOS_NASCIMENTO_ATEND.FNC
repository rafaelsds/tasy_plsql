create or replace
function obter_dados_nascimento_atend(nr_atendimento_p		number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is
ds_retorno_w			varchar2(255);
ie_tipo_nascimento_w		nascimento.ie_tipo_nascimento%type;
/*
'TN' - Tipo de nascimento
*/

begin

begin
select	a.ie_tipo_nascimento
into	ie_tipo_nascimento_w
from	nascimento a
where	a.DT_NASCIMENTO = 	(select max(x.dt_nascimento)
				from	nascimento x
				where	x.nr_atendimento = a.nr_atendimento);
exception
when others then
	ie_tipo_nascimento_w := null;
end;


if	(ie_opcao_p = 'TN') then
	ds_retorno_w := ie_tipo_nascimento_w;
end if;

return	ds_retorno_w;

end obter_dados_nascimento_atend;
/