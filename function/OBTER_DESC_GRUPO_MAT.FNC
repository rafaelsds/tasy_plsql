create or replace
function Obter_desc_grupo_mat	(cd_grupo_material_p	number) 
				return varchar2 is 


ds_retorno_w		varchar2(255);

begin

if	(cd_grupo_material_p is not null) then
	select	ds_grupo_material
	into	ds_retorno_w
	from	grupo_material
	where	cd_grupo_material	= cd_grupo_material_p;
end if;

return ds_retorno_w;

end Obter_desc_grupo_mat;
/