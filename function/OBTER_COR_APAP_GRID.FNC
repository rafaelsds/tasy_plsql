create or replace
function obter_cor_apap_grid(	ds_cor_ref_sv_p		varchar2,
								ds_cor_varias_sv_p	varchar2,
								nr_coluna_p	number)
 		    	return number is

lista_w		dbms_sql.varchar2_table;
lista_2_w	dbms_sql.varchar2_table;
ds_retorno_ref_sv_w			number(10);
ds_retorno_varias_sv_w		number(10);
ds_retorno_w				number(10);
ds_linha_w					varchar2(255);
begin

if	( ds_cor_ref_sv_p	is not null)  then

	lista_w	:= obter_lista_string(ds_cor_ref_sv_p,',');

	if	(lista_w.count	>0) then
		
		for i in lista_w.first..lista_w.last loop
			begin
			ds_linha_w	:= lista_w(i);
			
			if	(substr(ds_linha_w,1,2)	= nr_coluna_p) then
				ds_retorno_ref_sv_w	:= somente_numero(substr(ds_linha_w,3));
			end if;
			
			end;
		end loop;
		
	end if;
	
end if;


if	( ds_cor_varias_sv_p	is not null)  then

	lista_2_w	:= obter_lista_string(ds_cor_varias_sv_p,',');

	if	(lista_2_w.count	>0) then
		
		for i in lista_2_w.first..lista_2_w.last loop
			begin
			ds_linha_w	:= lista_2_w(i);
			
			if	(substr(ds_linha_w,1,2)	= nr_coluna_p) then
				ds_retorno_varias_sv_w	:= somente_numero(substr(ds_linha_w,3));
			end if;
			
			end;
		end loop;
		
	end if;	
	
end if;


if (ds_retorno_varias_sv_w is not null) and (ds_retorno_ref_sv_w is not null) then

	ds_retorno_w := 7040;
	
elsif(ds_retorno_varias_sv_w is not null) then

   ds_retorno_w := ds_retorno_varias_sv_w;
  
elsif(ds_retorno_ref_sv_w is not null) then

   ds_retorno_w := ds_retorno_ref_sv_w;
	
end if;



return	ds_retorno_w;

end obter_cor_apap_grid;
/
