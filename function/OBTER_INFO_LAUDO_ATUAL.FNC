create or replace
function obter_info_laudo_atual(nr_prescricao_p		number,
				nr_seq_prescr_p		number)
 		    	return number is

nr_retorno_w	number(10);
			
begin

if	(nr_prescricao_p is not null) and
	(nr_seq_prescr_p is not null) then

	select	max(nr_sequencia)
	into	nr_retorno_w
	from	prescr_proc_info_laudo
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_prescr = nr_seq_prescr_p;

end if;

return	nr_retorno_w;

end obter_info_laudo_atual;
/