create or replace
function obter_telefone_pf_html5(cd_pessoa_fisica_p	varchar2,
			     ie_tipo_telefone_p	number)
				
				return varchar2 is

ds_telefone_w		varchar2(255);

/*
tipos
1 - residencial
2 - comercial
7 - bip/pager
8 - comercial
9 - contato
10 -contato com ddd
0 - celular (cadastro pessoa f�sica) - rafael em 30/10/2006 os43519
11- fax
12- ddd / celular 
13- ddd / residencial
14- ddi / ddd / celular - espec�fico para Agendamento Coletivo
*/
begin

if	(ie_tipo_telefone_p = 1) then
begin
	select	substr(obter_nr_cel_int(max(p.nr_ddi_telefone), max(p.nr_telefone)),1,255)
	into	ds_telefone_w
	from	compl_pessoa_fisica p
	where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	p.ie_tipo_complemento = 1;
end;
	
elsif	(ie_tipo_telefone_p = 2) then
begin
	select	substr(obter_nr_cel_int(max(p.nr_ddi_telefone), max(p.nr_telefone)),1,255)
	into	ds_telefone_w
	from	compl_pessoa_fisica p
	where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	p.ie_tipo_complemento = 2;
end;
	
elsif	(ie_tipo_telefone_p = 4) then
begin
	select	substr(obter_nr_cel_int(max(p.nr_ddi_telefone), max(p.nr_telefone)),1,255)
	into	ds_telefone_w
	from	compl_pessoa_fisica p
	where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	p.ie_tipo_complemento = 1;
end;

elsif	(ie_tipo_telefone_p = 5) then
begin
	select	substr(obter_nr_cel_int(max(p.nr_ddi_telefone), max(p.nr_telefone)),1,255)
	into	ds_telefone_w
	from	compl_pessoa_fisica p
	where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	p.ie_tipo_complemento = 2;
end;

elsif	(ie_tipo_telefone_p = 8) then
begin
	select	substr(obter_nr_cel_int(max(p.nr_ddi_telefone), max(p.nr_ramal)),1,255)
	into	ds_telefone_w
	from	compl_pessoa_fisica p
	where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	p.ie_tipo_complemento = 2;
end;

elsif	(ie_tipo_telefone_p = 9) then
begin
	select	substr(obter_nr_cel_int(max(nr_ddi_celular), max(nr_telefone_celular)),1,255)
	into	ds_telefone_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
end;

elsif	(ie_tipo_telefone_p = 10) then
begin
	select	substr(obter_nr_cel_int(max(nr_ddi_celular), max(nr_telefone_celular)),1,255)
	into	ds_telefone_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
end;

elsif	(ie_tipo_telefone_p = 7) then
begin
	select	substr(obter_nr_cel_int(max(nr_ddi_celular), max(nr_pager_bip)),1,255)
	into	ds_telefone_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
end;

elsif	(ie_tipo_telefone_p = 0) then
begin
	select	substr(obter_nr_cel_int(max(nr_ddi_celular), max(nr_telefone_celular)),1,255)
	into	ds_telefone_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
end;

elsif	(ie_tipo_telefone_p = 11) then
begin
	select	substr(obter_nr_cel_int(max(nr_ddi_fax), max(p.ds_fax)),1,255)
	into	ds_telefone_w
	from	compl_pessoa_fisica p
	where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
	and		p.ie_tipo_complemento = 1;
end;	

elsif	(ie_tipo_telefone_p = 12) then
begin
	select	substr(obter_nr_cel_int(max(nr_ddi_celular), max(nr_telefone_celular)),1,255)
	into	ds_telefone_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
end;
	
elsif	(ie_tipo_telefone_p = 13) then
begin
	select	substr(obter_nr_cel_int(max(nr_ddi_telefone),  max(nr_ddd_telefone || nr_telefone)),1,255)
	into	ds_telefone_w
	from	compl_pessoa_fisica p
	where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	p.ie_tipo_complemento = 1;
end;	

elsif	(ie_tipo_telefone_p = 14) then
begin
	select	substr(obter_nr_cel_int(max(nr_ddi_celular), max(nr_ddd_celular || nr_telefone_celular)),1,255)
	into	ds_telefone_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
end;	

end if;

return ds_telefone_w;

end obter_telefone_pf_html5;
/