create or replace
function pls_obter_dados_valor_fatura(nr_seq_fatura_p		number,
					ie_opcao_p		varchar2)
					return number is

ie_opcao_w		varchar2(10);
nr_seq_prestador_w	number(10);
nr_seq_conta_w		number(10);
ie_tipo_guia_w		varchar2(2);
ie_tipo_relacao_w	varchar2(2);
vl_item_w		number(15,2) := 0;
vl_retorno_w		number(15,2) := 0;

--	IE_OPCAO
-- CHMC	- Consultas e honor�rios m�dicos cooperados
-- CHMN	- Consultas e honor�rios m�dicos contratados
-- ETC	- Exames e terapias cooperados
-- ETN	- Exames e terapias contratados
-- ETR	- Exames e terapias rede pr�pria
-- IRP	- Interna��es Rede pr�pria
-- IRC	- Interna��es Rede contratada

Cursor C01 is
	select	e.nr_sequencia
	from	pls_conta		e,
		pls_fatura_conta	c,
		pls_fatura_evento	b,
		pls_fatura		a
	where	a.nr_sequencia	= b.nr_seq_fatura
	and	b.nr_sequencia	= c.nr_seq_fatura_evento
	and	e.nr_sequencia	= c.nr_seq_conta
	and	a.nr_sequencia	= nr_seq_fatura_p
	group by
		e.nr_sequencia;
	
begin
ie_opcao_w := upper(ie_opcao_p);

open C01;
loop
fetch C01 into
	nr_seq_conta_w;
exit when C01%notfound;
	begin
	begin
	select	ie_tipo_guia,
		nr_seq_prestador_exec
	into	ie_tipo_guia_w,
		nr_seq_prestador_w
	from	pls_conta
	where	nr_sequencia = nr_seq_conta_w;
	exception
	when others then
		ie_tipo_guia_w := null;
		nr_seq_prestador_w := null;
	end;
	
	-- IE_TIPO_GUIA_W - Dominio 1746
	-- 3               Guia de consulta
	-- 4               Guia de SP/SADT
	-- 5               Guia de Resumo de Interna��o
	-- 6               Guia de Honor�rio Individual
	
	if	(nr_seq_prestador_w is not null) then
		begin
		select	ie_tipo_relacao
		into	ie_tipo_relacao_w
		from	pls_prestador
		where	nr_sequencia = nr_seq_prestador_w;
		exception
		when others then
			ie_tipo_relacao_w := null;
		end;
	end if;
	
	-- IE_TIPO_RELACAO_W - Dominio 1668
	-- C               Cooperado
	-- D              Rede contratualizada direta
	-- F               Fornecedor
	-- I               Rede contratualizada indireta
	-- P               Rede pr�pria
	
	if	(ie_tipo_guia_w is not null) and
		(ie_tipo_relacao_w is not null) then
		
		if	(ie_opcao_w = 'CHMC') then
			if	(ie_tipo_guia_w in ('3','6')) and
				(ie_tipo_relacao_w = 'C') then
				select	sum(b.vl_beneficiario)
				into	vl_item_w
				from	pls_conta_pos_estabelecido	b,
					pls_conta_proc	a
				where	a.nr_sequencia	= b.nr_seq_conta_proc
				and	a.nr_seq_conta	= nr_seq_conta_w;
			end if;
		
		elsif	(ie_opcao_w = 'CHMN') then
			if	(ie_tipo_guia_w in ('3','6')) and
				(ie_tipo_relacao_w in ('F','D','I')) then
				select	sum(b.vl_beneficiario)
				into	vl_item_w
				from	pls_conta_pos_estabelecido	b,
					pls_conta_proc	a
				where	a.nr_sequencia	= b.nr_seq_conta_proc
				and	a.nr_seq_conta	= nr_seq_conta_w;
			end if;
		
		elsif	(ie_opcao_w = 'ETC') then
			if	(ie_tipo_guia_w = '4') and
				(ie_tipo_relacao_w = 'C') then
				select	sum(b.vl_beneficiario)
				into	vl_item_w
				from	pls_conta_pos_estabelecido	b,
					pls_conta_proc	a
				where	a.nr_sequencia	= b.nr_seq_conta_proc
				and	a.nr_seq_conta	= nr_seq_conta_w;
			end if;
		
		elsif	(ie_opcao_w = 'ETN') then
			if	(ie_tipo_guia_w = '4') and
				(ie_tipo_relacao_w in ('F','D','I')) then
				select	sum(b.vl_beneficiario)
				into	vl_item_w
				from	pls_conta_pos_estabelecido	b,
					pls_conta_proc	a
				where	a.nr_sequencia	= b.nr_seq_conta_proc
				and	a.nr_seq_conta	= nr_seq_conta_w;
			end if;
	
		elsif	(ie_opcao_w = 'ETR') then
			if	(ie_tipo_guia_w = '4') and
				(ie_tipo_relacao_w = 'P') then
				select	sum(b.vl_beneficiario)
				into	vl_item_w
				from	pls_conta_pos_estabelecido	b,
					pls_conta_proc	a
				where	a.nr_sequencia	= b.nr_seq_conta_proc
				and	a.nr_seq_conta	= nr_seq_conta_w;
			end if;
	
		elsif	(ie_opcao_w = 'IRP') then
			if	(ie_tipo_guia_w = '5') and
				(ie_tipo_relacao_w = 'P') then
				select	sum(b.vl_beneficiario)
				into	vl_item_w
				from	pls_conta_pos_estabelecido	b,
					pls_conta_proc	a
				where	a.nr_sequencia	= b.nr_seq_conta_proc
				and	a.nr_seq_conta	= nr_seq_conta_w;
			end if;
		
		elsif	(ie_opcao_w = 'IRC') then
			if	(ie_tipo_guia_w = '5') and
				(ie_tipo_relacao_w in ('F','D','I')) then
				select	sum(b.vl_beneficiario)
				into	vl_item_w
				from	pls_conta_pos_estabelecido	b,
					pls_conta_proc	a
				where	a.nr_sequencia	= b.nr_seq_conta_proc
				and	a.nr_seq_conta	= nr_seq_conta_w;
			end if;
		end if;
		
		vl_retorno_w := vl_retorno_w + vl_item_w;
	end if;
	end;
end loop;
close C01;

return	vl_retorno_w;

end pls_obter_dados_valor_fatura;
/