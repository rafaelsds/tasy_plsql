create or replace
function pls_obter_qt_vidas_tab_preco
			(	nr_seq_segurado_p	number,
				ie_calculo_vidas_p	varchar2,
				ie_acao_p		varchar2)
 		    	return number is

qt_vidas_w		number(10);
nr_seq_contrato_w	pls_contrato.nr_sequencia%type;
nr_seq_titular_w	pls_segurado.nr_seq_titular%type;
dt_contratacao_w	pls_segurado.dt_contratacao%type;
dt_contrato_w		pls_contrato.dt_contrato%type;
dt_liberacao_w		pls_segurado.dt_liberacao%type;
dt_aprovacao_w		pls_contrato.dt_aprovacao%type;
begin

select	nr_seq_contrato
into	nr_seq_contrato_w
from	pls_segurado
where	nr_sequencia = nr_seq_segurado_p;

if(nr_seq_contrato_w is null) then
	select	a.nr_seq_intercambio,
		nvl(a.nr_seq_titular,a.nr_sequencia),
		trunc(a.dt_contratacao,'dd'),
		trunc(b.dt_inclusao,'dd'),
		trunc(a.dt_liberacao,'dd'),
		trunc(b.dt_aprovacao,'dd')
	into	nr_seq_contrato_w,
		nr_seq_titular_w,
		dt_contratacao_w,
		dt_contrato_w,
		dt_liberacao_w,
		dt_aprovacao_w
	from	pls_segurado	a,
		pls_intercambio	b
	where	b.nr_sequencia	= a.nr_seq_intercambio
	and	a.nr_sequencia	= nr_seq_segurado_p;
	
	if	(dt_contratacao_w = dt_contrato_w) and
	(nvl(ie_acao_p,'X') = 'C') then
		if	(ie_calculo_vidas_p = 'A') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_intercambio = nr_seq_contrato_w
			and	dt_rescisao is null
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'T') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_intercambio = nr_seq_contrato_w
			and	nr_seq_titular is null
			and	dt_rescisao is null
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'D') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_intercambio = nr_seq_contrato_w
			and	nr_seq_titular is not null
			and	dt_rescisao is null
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'TD') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado a
			where	a.nr_seq_intercambio = nr_seq_contrato_w
			and	dt_cancelamento is null
			and	((a.nr_seq_titular is null) or ((a.nr_seq_titular is not null) and ((select	count(1)
												from	grau_parentesco x
												where	x.nr_sequencia = a.nr_seq_parentesco
												and	x.ie_tipo_parentesco = '1') > 0)))
			and	a.dt_rescisao is null;
		elsif	(ie_calculo_vidas_p = 'F') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_intercambio = nr_seq_contrato_w
			and	dt_cancelamento is null
			and	((nr_sequencia = nr_seq_titular_w) or
				 (nr_seq_titular = nr_seq_titular_w))
			and	dt_rescisao is null;
		end if;
	else
		if	(ie_calculo_vidas_p = 'A') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_intercambio = nr_seq_contrato_w
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate))
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'T') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_intercambio = nr_seq_contrato_w
			and	nr_seq_titular is null
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate))
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'D') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_intercambio = nr_seq_contrato_w
			and	nr_seq_titular is not null
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate))
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'TD') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado a
			where	a.nr_seq_intercambio = nr_seq_contrato_w
			and	a.dt_liberacao is not null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate))
			and	dt_cancelamento is null
			and	((nr_seq_titular is null) or ((nr_seq_titular is not null) and ((select	count(1)
												from	grau_parentesco x
												where	x.nr_sequencia = a.nr_seq_parentesco
												and	x.ie_tipo_parentesco = '1') > 0)));
		elsif	(ie_calculo_vidas_p = 'F') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_intercambio = nr_seq_contrato_w
			and	((nr_sequencia = nr_seq_titular_w) or
				 (nr_seq_titular = nr_seq_titular_w))
			and	dt_liberacao is not null
			and	dt_cancelamento is null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate));
		end if;
	end if;		
else 
	select	a.nr_seq_contrato,
		nvl(a.nr_seq_titular,a.nr_sequencia),
		trunc(a.dt_contratacao,'dd'),
		trunc(b.dt_contrato,'dd'),
		trunc(a.dt_liberacao,'dd'),
		trunc(b.dt_aprovacao,'dd')
	into	nr_seq_contrato_w,
		nr_seq_titular_w,
		dt_contratacao_w,
		dt_contrato_w,
		dt_liberacao_w,
		dt_aprovacao_w
	from	pls_segurado	a,
		pls_contrato	b
	where	b.nr_sequencia	= a.nr_seq_contrato
	and	a.nr_sequencia	= nr_seq_segurado_p;

	if	(dt_contratacao_w = dt_contrato_w) and
		(nvl(ie_acao_p,'X') = 'C') then
		if	(ie_calculo_vidas_p = 'A') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	dt_rescisao is null
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'T') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	nr_seq_titular is null
			and	dt_rescisao is null
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'D') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	nr_seq_titular is not null
			and	dt_rescisao is null
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'TD') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado a
			where	a.nr_seq_contrato = nr_seq_contrato_w
			and	dt_cancelamento is null
			and	((a.nr_seq_titular is null) or ((a.nr_seq_titular is not null) and ((select	count(1)
												from	grau_parentesco x
												where	x.nr_sequencia = a.nr_seq_parentesco
												and	x.ie_tipo_parentesco = '1') > 0)))
			and	a.dt_rescisao is null;
		elsif	(ie_calculo_vidas_p = 'F') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato	= nr_seq_contrato_w
			and	dt_cancelamento is null
			and	((nr_sequencia = nr_seq_titular_w) or
				 (nr_seq_titular = nr_seq_titular_w))
			and	dt_rescisao is null;
		end if;
	else
		if	(ie_calculo_vidas_p = 'A') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate))
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'T') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	nr_seq_titular is null
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate))
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'D') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	nr_seq_titular is not null
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate))
			and	dt_cancelamento is null;
		elsif	(ie_calculo_vidas_p = 'TD') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado a
			where	a.nr_seq_contrato = nr_seq_contrato_w
			and	a.dt_liberacao is not null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate))
			and	dt_cancelamento is null
			and	((nr_seq_titular is null) or ((nr_seq_titular is not null) and ((select	count(1)
												from	grau_parentesco x
												where	x.nr_sequencia = a.nr_seq_parentesco
												and	x.ie_tipo_parentesco = '1') > 0)));
		elsif	(ie_calculo_vidas_p = 'F') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato	= nr_seq_contrato_w
			and	((nr_sequencia = nr_seq_titular_w) or
				 (nr_seq_titular = nr_seq_titular_w))
			and	dt_liberacao is not null
			and	dt_cancelamento is null
			and	((dt_rescisao is null) or (dt_rescisao > sysdate));
		end if;
	end if;
end if;

return	nvl(qt_vidas_w,0);

end pls_obter_qt_vidas_tab_preco;
/
