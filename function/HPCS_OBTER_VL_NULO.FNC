create or replace
function hpcs_obter_vl_nulo(	ds_retorno_p		varchar2,
				ds_valor_p		varchar2 default null)
 		    	return varchar2 is
ds_retorno_w			varchar2(255);
begin

ds_retorno_w	:= ds_valor_p;

if	(nvl(ds_valor_p,'X') = 'X') then
	begin
	ds_retorno_w := ds_retorno_p;
	end;
end if;

return	ds_retorno_w;

end hpcs_obter_vl_nulo;
/
