create or replace
function obter_se_higie_atrasada(dt_alta_p		date,
				dt_saida_real_p		date,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				cd_setor_atendimento_p	number,
				cd_unidade_basica_p	varchar2,
				cd_unidade_compl_p	varchar2,
				nr_atendimento_p	number)
				return varchar2 is

qt_retorno_w		number(10);
qt_minuto_w		number;
ie_atraso_w		varchar2(1);		
dt_higienizado_w	date;

	/*
		Verifica se o leito est� com a higieniza��o atrasada conforme
		par�metro [214] - Quantidade de minutos na pasta alta para atualizar a cor de alerta, caso n�o tenha in�cio de higieniza��o.
		da fun��o Ocupa��o Hospitalar.
	*/	
begin

select	max(dt_inicio_higienizacao)
into	dt_higienizado_w
from	unidade_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_p
and	cd_unidade_basica = cd_unidade_basica_p
and	nvl(cd_unidade_compl,cd_unidade_compl_p) = cd_unidade_compl_p
and	dt_inicio_higienizacao >= dt_alta_p;

if	(dt_alta_p is not null) and
	(dt_saida_real_p is not null) and
	(dt_higienizado_w is null) then
	begin
	select	max(obter_valor_param_usuario(44, 214, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p))
	into	qt_minuto_w
	from	dual;

	select	trunc(((sysdate - dt_saida_real_p) * 1440))
	into 	qt_retorno_w
	from	dual;

	if	(qt_retorno_w > nvl(qt_minuto_w,0)) then
		ie_atraso_w	:= 'S';	
	else
		ie_atraso_w	:= 'N';
	end if;
	end;
else	
	ie_atraso_w := 'N';

end if;

return	ie_atraso_w;

end obter_se_higie_atrasada;
/
