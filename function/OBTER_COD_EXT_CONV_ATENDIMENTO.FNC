CREATE OR REPLACE
FUNCTION Obter_cod_ext_conv_Atendimento(
					nr_atendimento_p			number)
					RETURN Number IS

cd_convenio_w			Number(5,0);
cd_externo_w	convenio.CD_EXTERNO%type;

BEGIN

select 	Obter_Convenio_Atendimento(nr_atendimento_p)
into 	cd_convenio_w
from 	dual;
	
if (cd_convenio_w > 0) then
	select max(CD_EXTERNO)
	into cd_externo_w
	from convenio
	where cd_convenio = cd_convenio_w;
end if;
	

RETURN cd_externo_w;
END Obter_cod_ext_conv_Atendimento;
/
