create or replace
function obter_desc_grupo_ou_pergunta(	nr_seq_grupo_p		number,
					nr_seq_pergunta_p	number)
 		    	return varchar2 is

ds_result_w		varchar2(255);			

begin

if	(nr_seq_grupo_p is not null) then

	select	nvl(max(ds_grupo),'')
	into	ds_result_w
	from	avf_grupo_pergunta
	where	nr_sequencia	= nr_seq_grupo_p;

elsif	(nr_seq_pergunta_p is not null) then

	select	'        ' || ds_pergunta
	into	ds_result_w
	from	avf_pergunta
	where	nr_sequencia = nr_seq_pergunta_p;
end if;

return	ds_result_w;

end obter_desc_grupo_ou_pergunta;
/