create or replace
function Verifica_peso_ciclo( 	nr_seq_paciente_p	number,
					nr_atendimento_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1) := 'N'; 
qt_peso_sv_w		number(10,3);
qt_peso_onc_w		number(10,3);	

cursor c01 is
	select	qt_peso
	from	paciente_atendimento
	where	nr_prescricao is null
	and	nr_seq_paciente = nr_seq_paciente_p;

begin
select	obter_sinal_vital(nr_atendimento_p,'Peso')
into	qt_peso_sv_w
from	dual;

open C01;
loop
fetch C01 into	
	qt_peso_onc_w;
exit when C01%notfound;
	begin
	if (qt_peso_sv_w <> qt_peso_onc_w) then
		ds_retorno_w := 'S';
		exit;
	end if;
	end;
end loop;
close C01;

return	ds_retorno_w;

end Verifica_peso_ciclo;
/
