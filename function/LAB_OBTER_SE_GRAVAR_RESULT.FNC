create or replace 
function lab_obter_se_gravar_result(nr_seq_exame_p number)
					return varchar2 is

nr_seq_grupo_w	number(10);
ds_retorno_w	varchar2(1) := 'N';

begin

select	max(nr_seq_grupo)
into	nr_seq_grupo_w
from	exame_laboratorio
where 	nr_seq_exame = nr_seq_exame_p;

select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	lab_regra_gravar_result
where	nvl(nr_seq_exame,nr_seq_exame_p)	= nr_seq_exame_p
and	nvl(nr_seq_grupo,nr_seq_grupo_w)	= nr_seq_grupo_w;
	
return ds_retorno_w;
	
end;
/