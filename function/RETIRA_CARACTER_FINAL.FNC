create or replace 
function Retira_Caracter_Final
		(ds_linha_p		varchar2,
		ds_caracter_p		varchar2)
		return varchar2 is

ds_retorno_w	varchar2(255);
i		Integer;
Inserir		Boolean;


begin

i	:= 1;
Inserir	:= True;

for	i in 1 .. length(ds_linha_p) loop
	begin
	if	(substr(ds_linha_p,i,1) = ' ') and
		((substr(ds_linha_p,i - 1,1) = ' ') or
		((substr(ds_linha_p,i + 1,1) = ' '))) then
		Inserir	:= False;
	else
		Inserir	:= True;
	end if;

	if	(Inserir = True) then
		ds_retorno_w	:= ds_retorno_w || substr(ds_linha_p,i,1);	
	end if;
	end;
end loop;


return ds_retorno_w;

end Retira_Caracter_Final;
/