create or replace
function pls_obter_vl_total_rel_4534(	dt_inicio_p		varchar2,
					nr_contrato_p		number,
					ie_estipulante_p	varchar2,
					ie_opcao_p		varchar2)
return 			varchar2 is
vl_retorno_w		varchar2(255);
begin
/*
T = Total t�tular + dependentes
TT = Total t�tular
TD = Total dependentes
TA = Total agregados
*/

if (ie_opcao_p = 'T') then
	select	campo_mascara_virgula(to_char(sum(vl_beneficiario)))
	into	vl_retorno_w
	from	(	select	to_number(pls_obter_valor_segurado(e.nr_sequencia,'VCD')) vl_beneficiario
			from	pls_segurado	e,
				pls_contrato	a
			where	a.nr_sequencia	= e.nr_seq_contrato
			and	e.nr_seq_titular is null
			and	(a.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
			and	trunc(e.dt_contratacao, 'month') <= to_date(dt_inicio_p,'mm/yyyy')
			and	(e.dt_rescisao is null or trunc(e.dt_rescisao, 'month') = to_date(dt_inicio_p,'mm/yyyy'))  
			union all
			select	to_number(pls_obter_valor_segurado(e.nr_sequencia,'VCD')) vl_beneficiario
			from	pls_segurado	e,
				pls_contrato	a
			where	a.nr_sequencia		= e.nr_seq_contrato
			and	e.nr_seq_titular in (	select	e.nr_sequencia
							from	pls_segurado		e,
								pls_contrato		a
							where	a.nr_sequencia	= e.nr_seq_contrato
							and	e.nr_seq_titular is null
							and	(a.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
							and	trunc(e.dt_contratacao, 'month') <= to_date(dt_inicio_p,'mm/yyyy')
							and	(e.dt_rescisao is null or trunc(e.dt_rescisao, 'month') = to_date(dt_inicio_p,'mm/yyyy')) )
			and	trunc(e.dt_contratacao, 'month') <= to_date(dt_inicio_p,'mm/yyyy')
			and	(e.dt_rescisao is null or trunc(e.dt_rescisao, 'month') = to_date(dt_inicio_p,'mm/yyyy')) 
			and	(a.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
			and	((ie_estipulante_p = 'PF' and a.cd_pf_estipulante is not null) or
				 (ie_estipulante_p = 'PJ' and a.cd_cgc_estipulante is not null) or
				 (ie_estipulante_p = 'A')));
elsif (ie_opcao_p = 'TT') then
	select	rpad(count(1),8) || lpad(campo_mascara_virgula(to_char(nvl(sum(to_number(pls_obter_valor_segurado(e.nr_sequencia,'VCD'))),0))),20)
	into	vl_retorno_w
	from	pls_segurado	e,
		pls_contrato	a
	where	a.nr_sequencia	= e.nr_seq_contrato
	and	e.nr_seq_titular is null  
	and	trunc(e.dt_contratacao, 'month') <= to_date(dt_inicio_p,'mm/yyyy')
	and	(e.dt_rescisao is null or trunc(e.dt_rescisao, 'month') = to_date(dt_inicio_p,'mm/yyyy'))
	and	(a.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	((ie_estipulante_p = 'PF' and a.cd_pf_estipulante is not null) or
		 (ie_estipulante_p = 'PJ' and a.cd_cgc_estipulante is not null) or
		 (ie_estipulante_p = 'A'));
elsif (ie_opcao_p = 'TD') then
	select	rpad(count(1),8) || lpad(campo_mascara_virgula(to_char(nvl(sum(to_number(pls_obter_valor_segurado(e.nr_sequencia,'VCD'))),0))),20)
	into	vl_retorno_w
	from	pls_segurado		e,
		pls_contrato		a
	where  	a.nr_sequencia		= e.nr_seq_contrato
	and	e.nr_seq_titular in (	select	e.nr_sequencia
					from	pls_segurado	e,
						pls_contrato	a
					where	a.nr_sequencia	= e.nr_seq_contrato
					and	(a.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
					and	e.nr_seq_titular is null  
					and	trunc(e.dt_contratacao, 'month') <= to_date(dt_inicio_p,'mm/yyyy')
					and	(e.dt_rescisao is null or trunc(e.dt_rescisao, 'month') = to_date(dt_inicio_p,'mm/yyyy'))  )
	and	trunc(e.dt_contratacao, 'month') <= to_date(dt_inicio_p,'mm/yyyy')
	and	(e.dt_rescisao is null or trunc(e.dt_rescisao, 'month') = to_date(dt_inicio_p,'mm/yyyy'))  
	and	((ie_estipulante_p = 'PF' and a.cd_pf_estipulante is not null) or
		 (ie_estipulante_p = 'PJ' and a.cd_cgc_estipulante is not null) or
		 (ie_estipulante_p = 'A'))
	and	(select	ie_tipo_parentesco
		 from	grau_parentesco
		 where	nr_sequencia	= e.nr_seq_parentesco) = '1';
elsif (ie_opcao_p = 'TA') then
	select	rpad(count(1),8) || lpad(campo_mascara_virgula(to_char(nvl(sum(to_number(pls_obter_valor_segurado(e.nr_sequencia,'VCD'))),0))),20)
	into	vl_retorno_w
	from	grau_parentesco		f,
		pls_segurado		e,
		pls_contrato		a
	where	a.nr_sequencia		= e.nr_seq_contrato
	and	f.nr_sequencia		= e.nr_seq_parentesco
	and	e.nr_seq_titular in (	select	e.nr_sequencia
					from	pls_segurado	e,
						pls_contrato	a
					where	a.nr_sequencia	= e.nr_seq_contrato
					and	(a.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
					and	e.nr_seq_titular is null  
					and	trunc(e.dt_contratacao, 'month') <= to_date(dt_inicio_p,'mm/yyyy')
					and	(e.dt_rescisao is null or trunc(e.dt_rescisao, 'month') = to_date(dt_inicio_p,'mm/yyyy'))  )
	and	trunc(e.dt_contratacao, 'month') <= to_date(dt_inicio_p,'mm/yyyy')
	and	(e.dt_rescisao is null or trunc(e.dt_rescisao, 'month') = to_date(dt_inicio_p,'mm/yyyy'))  
	and	((ie_estipulante_p = 'PF' and a.cd_pf_estipulante is not null) or
		 (ie_estipulante_p = 'PJ' and a.cd_cgc_estipulante is not null) or
		 (ie_estipulante_p = 'A'))
	and	(select	ie_tipo_parentesco
		 from	grau_parentesco
		 where	nr_sequencia	= e.nr_seq_parentesco) <> '1';
end if;

return vl_retorno_w;

end pls_obter_vl_total_rel_4534;
/
