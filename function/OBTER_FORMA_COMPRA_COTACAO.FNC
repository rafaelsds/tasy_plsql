create or replace
function obter_forma_compra_cotacao(	nr_cot_compra_p			number)
 		    	return number is

nr_seq_forma_compra_w		number(10);			
			
begin

select	max(a.nr_seq_forma_compra)
into	nr_seq_forma_compra_w
from	solic_compra a,
	solic_compra_agrup_v b
where	a.nr_solic_compra = b.nr_solic_compra
and	b.nr_cot_compra = nr_cot_compra_p;

return	nr_seq_forma_compra_w;

end obter_forma_compra_cotacao;
/