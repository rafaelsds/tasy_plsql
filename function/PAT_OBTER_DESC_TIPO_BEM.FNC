create or replace
function pat_obter_desc_tipo_bem(	nr_seq_tipo_p	number)
					return varchar2 is

ds_tipo_bem_w				varchar2(80);

begin

if	(nr_seq_tipo_p is not null) then
	select	ds_tipo
	into	ds_tipo_bem_w
	from	pat_tipo_bem
	where	nr_sequencia = nr_seq_tipo_p;
end if;

return	ds_tipo_bem_w;
end pat_obter_desc_tipo_bem;
/
