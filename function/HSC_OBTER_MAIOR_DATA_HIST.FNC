create or replace
function hsc_obter_maior_data_hist (cd_setor_p  	number,
				    cd_unidade_p  	varchar2,
				    ie_opcao_p	         varchar2)
 		    	return date is

dt_max_historio	date;			
dt_fim_historio date;
			
begin

if 	((cd_setor_p is not null) and (cd_unidade_p is not null)) then
	
	select	max(a.dt_historico)
	into	dt_max_historio
	from   	unidade_atend_hist a,
		unidade_atendimento b
	where  	a.nr_seq_unidade = b.nr_seq_interno
	and    	b.cd_setor_atendimento = cd_setor_p
	and	b.cd_unidade_basica || ' ' || b.cd_unidade_compl = cd_unidade_p
	and	a.ie_status_unidade = 'L';
		
	select	max(a.dt_fim_historico)
	into	dt_fim_historio
	from   	unidade_atend_hist a,
		unidade_atendimento b
	where  	a.nr_seq_unidade = b.nr_seq_interno
	and    	b.cd_setor_atendimento = cd_setor_p
	and	b.cd_unidade_basica || ' ' || b.cd_unidade_compl = cd_unidade_p
	and	dt_historico = dt_max_historio 
	and	a.ie_status_unidade = 'L';

end if;

if	(ie_opcao_p = 'H') then
	return	dt_max_historio;
else
	return dt_fim_historio;
end if;

end hsc_obter_maior_data_hist;
/