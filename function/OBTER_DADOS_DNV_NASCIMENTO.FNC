create or replace
function obter_dados_dnv_nascimento (nr_atendimento_p	number, ie_opcao_p varchar2)
					return Varchar2 is
/* 	DT = Data de Nascimento
	DV  = DNV*/
ds_retorno			varchar2(40);
ds_nascimento_w 	varchar2(40);
ds_dnv_w				varchar2(40);

begin
select	max(to_char(nvl(dt_nascimento,sysdate),'dd/mm/yyyy hh:mm:ss')),
	nvl(max(nr_dnv), Wheb_mensagem_pck.get_texto(799512))
into	ds_nascimento_w,
	ds_dnv_w
from 	nascimento
where 	nr_atendimento = nr_atendimento_p;

if (ie_opcao_p = 'DT') then
	ds_retorno := ds_nascimento_w;
elsif (ie_opcao_p = 'DV') then
	ds_retorno := ds_dnv_w;
end if;

return ds_retorno;

end obter_dados_dnv_nascimento;
/
