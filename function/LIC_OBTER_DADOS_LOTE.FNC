create or replace
function lic_obter_dados_lote(	nr_seq_licitacao_p		number,
			nr_lote_compra_p		varchar2,
			ie_opcao_p		number)
return number is

vl_maximo_edital_w			number(13,4);
qt_item_w			number(13,4);
vl_retorno_w			number(13,4);
			
/*ie_opcao_p
1 - VL_MAXIMO_EDITAL
2 - QT_ITEM */

begin

select	sum(vl_maximo_edital * qt_item),
	sum(qt_item)
into	vl_maximo_edital_w,
	qt_item_w
from	reg_lic_item
where	nr_seq_licitacao 	= nr_seq_licitacao_p
and	nr_lote_compra	= nr_lote_compra_p
and	nvl(ie_lote,'N')	= 'N';

if	(ie_opcao_p = 1) then
	vl_retorno_w := vl_maximo_edital_w;
elsif	(ie_opcao_p = 2) then
	vl_retorno_w := qt_item_w;
end if;

return	vl_retorno_w;

end lic_obter_dados_lote;
/
