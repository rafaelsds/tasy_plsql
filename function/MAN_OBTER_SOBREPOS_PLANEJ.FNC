create or replace
function man_obter_sobrepos_planej(	nr_seq_planej_prev_p	number,
						nr_seq_equipamento_p	number)
						Return Varchar2 is 

dt_emissao_prev_w				Date;
dt_ordem_servico_w				Date;
dt_prevista_w					Date;
dt_ultima_ordem_w				Date;
ie_sobreposicao_w				Varchar2(1);
nr_seq_ordem_w				number(10);	
nr_seq_superior_w				number(10);
qt_dia_freq_w					number(4);
qt_dia_ordem_w				number(3);
qt_dia_sobreposicao_w			number(5);
ie_converter_mes_w				Varchar2(1);
BEGIN

ie_sobreposicao_w			:= 'N';

if	(nr_seq_planej_prev_p is not null) then
	/* Verifica se o planej possui um planejamento superior*/
	select	nvl(max(nr_seq_superior),0),
		nvl(max(qt_dia_sobreposicao),0)
	into	nr_seq_superior_w,
		qt_dia_sobreposicao_w
	from	man_planej_prev
	where	nr_sequencia = nr_seq_planej_prev_p;
	
	if	(nr_seq_superior_w > 0) then
		begin
		/* Ultima ordem de servi�o gerada do planej superior*/	
		select	Nvl(max(nr_sequencia),0)
		into	nr_seq_ordem_w
		from	man_ordem_servico
		where	nr_seq_equipamento	= nr_seq_equipamento_p
		and	nr_seq_planej		= nr_seq_superior_w
		and	ie_tipo_ordem		= 2;

		/* se n�o existir ordem, n�o existe sobreposi��o*/
		if	(nr_seq_ordem_w = 0) then
			ie_sobreposicao_w	:= 'N';
		else	
			/* Se tiver OS, verificar se existir� sobreposi��o*/
			/* Data da ordem e encerramento da ultima OS gerada*/
			select	trunc(dt_ordem_servico),
				Nvl(dt_fim_real,sysdate + 1000)
			into	dt_ordem_servico_w,
				dt_ultima_ordem_w
			from	man_ordem_servico
			where	nr_sequencia = nr_seq_ordem_w;
			
			/* Se a data da ordem gerada estiver dentro do per�odo - existe Sobreposi��o */
			if	(dt_ordem_servico_w > (sysdate - qt_dia_sobreposicao_w)) then
				ie_sobreposicao_w	:= 'S';
			else
				/* Verificar se pode existir uma sobreposi��o futura dentro do per�odo da regra*/
				select	nvl(max(a.qt_dia_gerar_ordem),0),
					nvl(max(c.qt_dia),0),
					nvl(max(c.ie_converter_mes), 'N')
				into	qt_dia_ordem_w,
					qt_dia_freq_w,
					ie_converter_mes_w
				from	man_localizacao d,
					man_freq_planej c,
					man_planej_prev a,
					man_equipamento b
				where	a.nr_seq_tipo_equip	= b.nr_seq_tipo_equip
				and	a.nr_seq_frequencia	= c.nr_sequencia
				and	b.nr_seq_local		= d.nr_sequencia
				and	a.nr_sequencia		= nr_seq_superior_w
				and	b.nr_sequencia		= nr_seq_equipamento_p
				and	nvl(a.dt_inicial,sysdate)	<= (sysdate + qt_dia_sobreposicao_w)
				and 	nvl(a.cd_setor_atendimento, d.cd_setor)	= d.cd_setor
				and	b.cd_estab_contabil	= nvl(a.cd_estabelecimento, b.cd_estab_contabil)
				and	nvl(a.ie_situacao,'A')	= 'A'
				and	nvl(b.ie_situacao,'A')	= 'A'
				and	nvl(a.ie_contador,'N')	= 'N'
				and	((nvl(a.ie_impacto,'T') = 'T') or (nvl(b.cd_impacto, a.ie_impacto) = a.ie_impacto))
				and	(b.nr_sequencia 	= nvl(a.nr_seq_equip, b.nr_sequencia));
				/* Calculando data prevista da gera��o da ordem futura*/
				dt_prevista_w		:= obter_data_prev_futura(dt_ultima_ordem_w, qt_dia_freq_w, ie_converter_mes_w);
				dt_emissao_prev_w	:= dt_prevista_w - qt_dia_ordem_w;

				/* Se a data de emissao da ordem estiver dentro do periodo futuro entao existe sobreposi��o*/
				if	(dt_emissao_prev_w >= sysdate) and
					(dt_emissao_prev_w <= (sysdate + qt_dia_sobreposicao_w)) then
					ie_sobreposicao_w	:= 'S';
				end if;
			end if;
		end if;
		end;		
	end if;		
end if;

return Nvl(ie_sobreposicao_w,'N');

END man_obter_sobrepos_planej;
/
