create or replace
function TX_Obter_desc_motivo_recusa (	nr_seq_motivo_recusa_p		number)
					return	varchar2 is
					
ds_motivo_recusa_w	varchar2(255);

begin

if	(nr_seq_motivo_recusa_p is not null) then
	select	ds_motivo_recusa
	into	ds_motivo_recusa_w
	from 	tx_motivo_recusa_doacao
	where	nr_sequencia = nr_seq_motivo_recusa_p;
end if;

return	ds_motivo_recusa_w;
	
end TX_Obter_desc_motivo_recusa;
/