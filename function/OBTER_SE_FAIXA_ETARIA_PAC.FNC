create or replace
function obter_se_faixa_etaria_pac	(	cd_pessoa_fisica_p	varchar2,
						cd_setor_atendimento_p	number)
					return varchar2 is
			
qt_idade_w		Number(10);
ie_idade_min_w		Number(10);
ie_idade_max_w		Number(10);
ie_cadastro_w		Number(10);
ie_permite_w		varchar2(1) := 'S';
qt_idade_min_w		Number(10);
qt_idade_max_w		Number(10);
qt_idade_atend_w	Number(10);
nr_sequencia_w		Number(10);
nr_sequencia_ww		Number(10);
Cursor C01 is
	select	qt_idade_min, 
		qt_idade_max
	from	faixa_etaria_pac
	where	ie_situacao = 'A';
	
begin

select	count(*)
into	ie_cadastro_w
from	faixa_etaria_pac
where	ie_situacao = 'A';

if (cd_pessoa_fisica_p is not null) and (nvl(ie_cadastro_w,0) > 0) then
	
	begin	
	select	nvl(obter_idade_pf(cd_pessoa_fisica_p, sysdate, 'A'),0)
	into	qt_idade_w
	from	dual;
	
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	faixa_etaria_pac
	where	qt_idade_w between qt_idade_min and	qt_idade_max
	and	ie_situacao = 'A';
	
	exception
	when others then
			
		qt_idade_w := 0;
	end;	
	
	
	if (nvl(qt_idade_w,0) > 0) then
		
		begin
		
		select	nvl(max(obter_idade_pf(obter_pessoa_atendimento(nr_atendimento,'C'), sysdate, 'A')),0)
		into	qt_idade_atend_w
		from	unidade_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_p
		and	dt_entrada_unidade = 	(	select	max(dt_entrada_unidade)
							from	unidade_atendimento
							where	cd_setor_atendimento = cd_setor_atendimento_p
							and	nr_atendimento is not null
							and	nvl(ie_controle_faixa_etaria,'N') = 'S'
						);
						
		select	max(nr_sequencia)
		into	nr_sequencia_ww
		from	faixa_etaria_pac
		where	qt_idade_atend_w between qt_idade_min and qt_idade_max
		and	ie_situacao = 'A';
		
		exception
		when others then
				
			qt_idade_atend_w := 0;
		end;				
		if (nvl(qt_idade_atend_w,0) > 0) then
			if (nvl(nr_sequencia_w,0) <> nvl(nr_sequencia_ww,0)) then
				ie_permite_w := 'N';
			end if;
		end if;
		
	end if;
	
end if;

return	nvl(ie_permite_w,'S');

end obter_se_faixa_etaria_pac;
/
