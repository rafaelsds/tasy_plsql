create or replace
function obter_se_todos_result_digit(nr_seq_resultado_p   number)  
                     return varchar2 is

ie_libera_w  varchar2(1);

begin


select   decode(count(1), 0, 'S', 'N')
into     ie_libera_w 
from     exame_lab_result_item a
where    a.nr_seq_resultado = nr_seq_resultado_p
and      qt_resultado is null
and      pr_resultado is null
and      ds_resultado is null;


return ie_libera_w;

end;
/