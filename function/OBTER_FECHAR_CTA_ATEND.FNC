CREATE OR REPLACE FUNCTION OBTER_FECHAR_CTA_ATEND (	
				NR_INTERNO_CONTA_P	NUMBER)
						RETURN VARCHAR2 IS

ie_fecha_cta_w		varchar2(1)	:= 'S';
ie_fecha_atend_w		varchar2(1)	:= 'S';
ie_fecha_cta_ww		varchar2(1)	:= 'S';
ie_fecha_atend_ww		varchar2(1)	:= 'S';
ds_incons_w			varchar2(2000):= '';
ie_fim_w			boolean	:= False;
ie_pos_w			number(10)	:= 0;
ie_pos_proc_w			number(10)	:= 0;
ie_cont_w			number(10)	:= 0;
cd_incons_w			number(22)	:= 0;
cd_incons_ww			varchar2(50);
qt_incons_w			Number(15,0)	:= 0;
cd_estabelecimento_w		Number(15,0);

BEGIN


select replace(ds_inconsistencia,' ',',') || ',',
	cd_estabelecimento
into	ds_incons_w,
	cd_estabelecimento_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

while not ie_fim_w loop
	begin
	qt_incons_w	:= qt_incons_w + 1;	
	ie_pos_w := instr(ds_incons_w,',');
	if (ie_pos_w > 0) then
		begin
		
		cd_incons_ww	:= substr(ds_incons_w,1,ie_pos_w);
		ie_pos_proc_w	:= instr(cd_incons_ww,'(');
		
		if	(ie_pos_proc_w > 0) then
			cd_incons_ww	:= substr(cd_incons_ww,1,ie_pos_proc_w);
		end if;		

		cd_incons_w 	:= somente_numero(cd_incons_ww);
		ds_incons_w 	:= substr(ds_incons_w,ie_pos_w + 1,length(ds_incons_w));

		ie_fecha_cta_ww		:= '';
		ie_fecha_atend_ww	:= '';
		select	min(nvl(c.ie_fecha_conta,nvl(b.ie_fecha_conta,a.ie_fecha_conta))),
			min(nvl(c.ie_fecha_atendimento,nvl(b.ie_fecha_atendimento,a.ie_fecha_atendimento))),
			count(*)
		into	ie_fecha_cta_ww,
			ie_fecha_atend_ww,
			ie_cont_w
		from	inconsistencia_perfil	c,
			inconsistencia_estab	b,
			inconsistencia		a
		where	cd_estabelecimento_w	= b.cd_estabelecimento (+)
		and	a.nr_sequencia		= b.nr_seq_inconsistencia (+)
		and	a.nr_sequencia		= c.nr_seq_inconsistencia (+)
		and	obter_perfil_ativo	= c.cd_perfil (+)
		and	cd_estabelecimento_w	= c.cd_estabelecimento (+)
		and	a.cd_inconsistencia	= cd_incons_w;
		if	(ie_cont_w = 1) then
			if	(ie_fecha_cta_ww = 'N') then
				ie_fecha_cta_w	:= 'N';
			end if;
			if	(ie_fecha_atend_ww = 'N') then
				ie_fecha_atend_w	:= 'N';
			end if;
		end if;
		end;
	end if;
	ie_fim_w := (ie_pos_w < 1) or (qt_incons_w > 100);
	end;
end loop;	
RETURN ie_fecha_cta_w || ie_fecha_atend_w;
END OBTER_FECHAR_CTA_ATEND;
/
