create or replace function obter_estagio_autor_ageint(	nr_seq_ageint_p		number,
				nr_seq_agenda_cons_p	number,
				nr_seq_agenda_exame_p	number,
				ie_opcao_p		        varchar2,
				nr_seq_pend_quimio_p	number default null)
return varchar2 is

ds_retorno_w		varchar2(4000)	:= null;
nr_sequencia_autor_w	number(10);
nr_seq_estagio_w		number(10);
nr_seq_proc_interno_w	number(10);
ds_estagio_w		varchar2(255);
cd_senha_w		varchar2(20);
ds_observacao_w		varchar2(4000);
nr_seq_atendimento_w	agenda_integrada_item.nr_seq_atendimento%type;

begin

if	(nr_seq_ageint_p is not null) and ((nr_seq_agenda_cons_p is not null) or (nr_seq_agenda_exame_p is not null) or (nr_seq_pend_quimio_p is not null)) then

	if (nr_seq_agenda_exame_p is not null) then
		select	max(a.nr_sequencia)
		into	nr_sequencia_autor_w
		from	autorizacao_convenio a,
			procedimento_autorizado b
		where	a.nr_sequencia	= b.nr_sequencia_autor
		and	b.nr_seq_agenda = nr_seq_agenda_exame_p;
		if	(nr_sequencia_autor_w is null) then
			select	max(nr_sequencia)
			into	nr_sequencia_autor_w
			from	autorizacao_convenio
			where	nr_seq_agenda = nr_seq_agenda_exame_p;
		end if;
	elsif (nr_seq_agenda_cons_p is not null) then
		select	max(nr_sequencia)
		into	nr_sequencia_autor_w
		from	autorizacao_convenio
		where	nr_seq_agenda_consulta = nr_seq_agenda_cons_p;
	end if;

	if (nr_sequencia_autor_w is null) then

		if (nr_seq_agenda_exame_p is not null) then
			select	max(nr_seq_proc_interno)
			into	nr_seq_proc_interno_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int = nr_seq_ageint_p
			and	nr_seq_agenda_exame = nr_seq_agenda_exame_p;
		elsif (nr_seq_agenda_cons_p is not null) then
			select	max(nr_seq_proc_interno)
			into	nr_seq_proc_interno_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int = nr_seq_ageint_p
			and	nr_seq_agenda_cons = nr_seq_agenda_cons_p;
		end if;

		select	max(a.nr_sequencia)
		into	nr_sequencia_autor_w
		from	autorizacao_convenio a, procedimento_autorizado b
		where	a.nr_sequencia = b.nr_sequencia_autor
		and	a.nr_seq_age_integ = nr_seq_ageint_p
		and	b.nr_seq_proc_interno = nr_seq_proc_interno_w;

		/*select	max(nr_sequencia)
		into	nr_sequencia_autor_w
		from	autorizacao_convenio
		where	nr_seq_age_integ = nr_seq_ageint_p;*/
	end if;

	if (nr_sequencia_autor_w is null) then

		select 	max(nr_seq_atendimento)
		into	nr_seq_atendimento_w
		from	agenda_integrada_item
		where	nr_seq_agenda_int = nr_seq_ageint_p
		and	    nr_seq_pend_quimio = nr_seq_pend_quimio_p;

		nr_sequencia_autor_w := obter_qt_nr_seq_autor(nr_seq_atendimento_w);

	end if;

	if	(nr_sequencia_autor_w is not null) then

		select	max(b.nr_sequencia),
			max(b.ds_estagio),
			max(a.cd_senha),
			max(a.ds_observacao)
		into	nr_seq_estagio_w,
			ds_estagio_w,
			cd_senha_w,
			ds_observacao_w
		from	estagio_autorizacao b,
			autorizacao_convenio a
		where	a.nr_seq_estagio	= b.nr_sequencia
		and	a.nr_sequencia	= nr_sequencia_autor_w;

		if	(ie_opcao_p	= 'C') then
			ds_retorno_w	:= nr_seq_estagio_w;
		elsif	(ie_opcao_p	= 'D') then
			ds_retorno_w	:= ds_estagio_w;
		elsif	(ie_opcao_p	= 'OBS') then
			ds_retorno_w	:= ds_observacao_w;
		elsif	(ie_opcao_p	= 'S') then
			ds_retorno_w	:= cd_senha_w;
		end if;
	end if;

end if;

return ds_retorno_w;

end;
/
