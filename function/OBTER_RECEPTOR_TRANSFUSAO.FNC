create or replace
function obter_receptor_transfusao(	nr_seq_transfusao_p 	number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is

cd_pessoa_fisica_w	varchar2(14);
nm_pessoa_fisica_w	varchar2(100);
nr_atendimento_w	number(10);

begin

select	max(nr_atendimento)
into	nr_atendimento_w
from	san_transfusao
where	nr_sequencia	= nr_seq_transfusao_p;

select	max(cd_pessoa_fisica),
	max(obter_nome_pf(cd_pessoa_fisica))
into	cd_pessoa_fisica_w,
	nm_pessoa_fisica_w
from   	atendimento_paciente
where  	nr_atendimento = nr_atendimento_w;

if (ie_opcao_p = 'C') then
	return	cd_pessoa_fisica_w;
else
	return nm_pessoa_fisica_w;
end if;

end obter_receptor_transfusao;
/

