create or replace 
function obter_se_clinica_idade(	qt_idade_p		number,
					ie_clinica_p		number,
					ie_tipo_atendimento_p	number)
 		    	return varchar2 is

ie_clinica_w	number(10);
ie_permite_w	varchar2(1) := 'N';
qt_clinicas_w	number(10);

Cursor 	c01 is
	select	ie_clinica
	from	regra_clinica_idade
	where	qt_idade_p between qt_idade_min and qt_idade_max
	and	((ie_tipo_atendimento = ie_tipo_atendimento_p) or (ie_tipo_atendimento is null))
	and	((cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento) or (cd_estabelecimento is null));
begin

select	count(*)
into	qt_clinicas_w
from	regra_clinica_idade
where	qt_idade_p between qt_idade_min and qt_idade_max
and	((ie_tipo_atendimento = ie_tipo_atendimento_p) or (ie_tipo_atendimento is null))
and	((cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento) or (cd_estabelecimento is null));

If	(qt_clinicas_w = 0) then
	ie_permite_w := 'S';
End if;

open c01;
loop
fetch 	c01 into 
	ie_clinica_w;
exit when c01%notfound;
	If	(ie_permite_w <> 'S') then
		If	(ie_clinica_w = ie_clinica_p) then
			ie_permite_w := 'S';
		Else 	
			ie_permite_w := 'N';
		End if;
	End if;
end loop;
close c01;

return	ie_permite_w;

end obter_se_clinica_idade;
/
