create or replace
function obter_prescricao(
		nr_atendimento_p		number,
		cd_pessoa_fisica_p		varchar2,
		ie_prescr_nao_liberada_p	varchar2,
		nr_prescricao_p			number,
		nr_prescricoes_p		varchar2,
		nr_prescr_titulo_p		number,
		nm_usuario_p			varchar2)
 		    	return number is
			
nr_prescricao_w	number(14,0);

begin

if	(ie_prescr_nao_liberada_p is not null) and
	((nr_atendimento_p is not null) or
	(cd_pessoa_fisica_p is not null)) and
	(nm_usuario_p is not null) then
	begin
	
	if	(ie_prescr_nao_liberada_p = 'S') then
		begin
		
		nr_prescricao_w	:= obter_prescricao_nao_liberada(nr_atendimento_p,cd_pessoa_fisica_p,nm_usuario_p);
		end;	
	else	if 	(nr_prescricao_p is not null) then
			begin
			
			nr_prescricao_w	:= nr_prescricao_p;
			end;
	else	if	(nr_prescricoes_p is not null) and
			(nr_prescricoes_p <> '') then
			begin
			
			nr_prescricao_w	:=	plt_obter_max_nr_prescricao(nr_prescricoes_p);
			end;
	else	if	(nr_prescr_titulo_p is not null) and
			(nr_prescr_titulo_p <> '') then
			begin
			
			nr_prescricao_w	:=	to_number(nr_prescr_titulo_p);
			end;
	       end if;
	    end if;
	  end if;
	end if;
	end;
end if;
return	nr_prescricao_w;
end obter_prescricao;
/