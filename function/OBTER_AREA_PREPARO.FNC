create or replace function obter_area_preparo  (	nr_prescricao_p		number,
				nr_seq_material_p	number) 
				return number is
		
cd_setor_atendimento_w	number(5,0);
nr_seq_material_w		number(6,0);
cd_local_estoque_w	number(4,0);
cd_unidade_medida_w	varchar2(30);
ie_via_aplicacao_w		varchar2(5);
cd_grupo_material_w	number(3,0);
cd_subgrupo_material_w	number(3,0);
cd_classe_material_w	number(5,0);
cd_material_w		number(6,0);
nr_seq_regra_prep_w	number(10,0);
nr_seq_area_prep_w	number(10,0);
nr_atendimento_w	number(10,0);
cd_setor_prescr_w	number(5,0);
cd_local_prescr_w	number(4,0);
cd_estabelecimento_w	number(5,0);
cd_local_estoque_prescr_w number(4,0);
ie_local_estoque_proc_w	varchar2(2);
nr_seq_kit_w		number(10,0);
nr_sequencia_w		number(10,0);
nr_seq_familia_w	number(10,0);
cd_material_estoque_w	number(6,0);
ie_agrupador_w		number(2,0);
ie_gerar_area_sem_disp_w	varchar2(1);
nr_seq_agrup_classif_w		number(10,0);
nr_seq_agrupamento_w		number(10,0);
ie_urgencia_w				varchar2(1);
ie_acm_w					varchar2(1);
ie_se_necessario_w			varchar2(1);
qt_dose_especial_w			number(18,6); 
ie_item_fracionado_w varchar2(1);
qt_dose_w prescr_material.qt_dose%type;
			
cursor c01 is
select	nvl(cd_local_prescr_w,0),
	a.nr_sequencia,
	nvl(a.cd_unidade_medida,'XPTO'),
	nvl(a.ie_via_aplicacao,'XPTO'),
	b.cd_grupo_material,
	b.cd_subgrupo_material,
	b.cd_classe_material,
	b.cd_material,
	nvl(b.nr_seq_familia,0),
	b.cd_material_estoque,
	a.ie_agrupador,
	nvl(ie_urgencia,'N'),
	nvl(a.ie_acm, 'N'),
	nvl(a.ie_se_necessario,'N'),
	nvl(qt_dose_especial,0),
	nvl(Obter_dose_convertida(	a.cd_material,
								a.qt_dose,
								a.cd_unidade_medida_dose,
								obter_dados_material_estab(a.cd_material,cd_estabelecimento_w,'UMS')),0) qt_dose
from	estrutura_material_v b,
	prescr_material a
where	b.cd_material		= a.cd_material
and	((ie_gerar_area_sem_disp_w = 'S') or (nvl(ie_regra_disp,'S') <> 'N'))
and	a.nr_prescricao		= nr_prescricao_p
and	((nvl(nr_seq_material_p,0) = 0) or
	 (a.nr_sequencia	= nr_seq_material_p))
order by
	a.nr_sequencia;
	
cursor c02 is
select	a.nr_sequencia,
	a.nr_seq_area_prep
from	adep_area_prep b,
	adep_regra_area_prep a
where	a.nr_seq_area_prep					= b.nr_sequencia
and obter_se_regra_setor_area(cd_setor_atendimento_w, b.nr_sequencia) = 'S'
and	nvl(b.cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w
and nvl(a.cd_estabelecimento_regra, cd_estabelecimento_w)		= cd_estabelecimento_w
and	nvl(a.cd_local_estoque, cd_local_estoque_w)		= cd_local_estoque_w
and	nvl(a.cd_unidade_medida, cd_unidade_medida_w)		= cd_unidade_medida_w
and	nvl(a.ie_via_aplicacao, ie_via_aplicacao_w)		= ie_via_aplicacao_w
and	nvl(a.cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	nvl(a.cd_material, cd_material_w)			= cd_material_w
and	nvl(nr_seq_familia, nvl(nr_seq_familia_w,0))		= nvl(nr_seq_familia_w,0)
and	nvl(a.cd_controlador_estoque, nvl(cd_material_estoque_w,0))	= nvl(cd_material_estoque_w,0)
and	nvl(a.nr_seq_agrupamento_setor, nvl(nr_seq_agrupamento_w,0)) = nvl(nr_seq_agrupamento_w,0)
and	nvl(b.ie_situacao,'A')				= 'A'
and	nvl(a.ie_situacao,'A')				= 'A'
and	nvl(a.ie_exclusao,'N')				= 'N'
and	nvl(a.nr_seq_agrup_classif, nvl(nr_seq_agrup_classif_w,0)) = nvl(nr_seq_agrup_classif_w,0)
and	(((nvl(a.ie_hemodialise,'S') = 'S') and (ie_agrupador_w = 13)) or
	 ((nvl(a.ie_hemodialise,'N') = 'N') and (ie_agrupador_w <> 13)))
and 	((nvl(ie_dose_especial,'N') = 'N') or ((nvl(ie_dose_especial,'N') = 'S') and (qt_dose_especial_w > 0)))
and		(((nvl(ie_somente_acmsn,'N') = 'G') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S'))) or
		((nvl(ie_somente_acmsn,'N') = 'S') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S'))) or
		((nvl(ie_somente_acmsn,'N') = 'A') and (ie_acm_w = 'S') and (ie_se_necessario_w = 'N')) or
		((nvl(ie_somente_acmsn,'N') = 'C') and (ie_acm_w = 'N') and (ie_se_necessario_w = 'S')) or
		(nvl(ie_somente_acmsn,'N') = 'N'))
and ((nvl(a.ie_somente_fracionados, 'N') = 'S' and ie_item_fracionado_w = 'S') or (nvl(a.ie_somente_fracionados, 'N') = 'N'))
and	not exists (	select	1
			from	adep_area_prep x,
				adep_regra_area_prep z
			where	x.nr_sequencia = b.nr_sequencia
			and	z.nr_seq_area_prep					= x.nr_sequencia
			and obter_se_regra_setor_area(cd_setor_atendimento_w, b.nr_sequencia) = 'S'
			and	nvl(x.cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w
      and nvl(z.cd_estabelecimento_regra, cd_estabelecimento_w)		= cd_estabelecimento_w
			and	nvl(z.cd_local_estoque, cd_local_estoque_w)		= cd_local_estoque_w
			and	nvl(z.cd_unidade_medida, cd_unidade_medida_w)		= cd_unidade_medida_w
			and	nvl(z.ie_via_aplicacao, ie_via_aplicacao_w)		= ie_via_aplicacao_w
			and	nvl(z.cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
			and	nvl(z.cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
			and	nvl(z.cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
			and	nvl(z.cd_material, cd_material_w)			= cd_material_w
			and	nvl(z.nr_seq_familia, nvl(nr_seq_familia_w,0))		= nvl(nr_seq_familia_w,0)
			and	nvl(z.cd_controlador_estoque, nvl(cd_material_estoque_w,0))	= nvl(cd_material_estoque_w,0)
			and	nvl(x.ie_situacao,'A')					= 'A'
			and	nvl(z.ie_situacao,'A')					= 'A'		
			and	nvl(z.ie_exclusao,'N')					= 'S'
			and	nvl(a.nr_seq_agrup_classif, nvl(nr_seq_agrup_classif_w,0)) = nvl(nr_seq_agrup_classif_w,0)
			and	nvl(a.nr_seq_agrupamento_setor, nvl(nr_seq_agrupamento_w,0)) = nvl(nr_seq_agrupamento_w,0)
			and	(((nvl(a.ie_hemodialise,'S') = 'S') and (ie_agrupador_w = 13)) or
				 ((nvl(a.ie_hemodialise,'N') = 'N') and (ie_agrupador_w <> 13)))
			and 	((nvl(ie_dose_especial,'N') = 'N') or ((nvl(ie_dose_especial,'N') = 'S') and (qt_dose_especial_w > 0)))
			and		(((nvl(ie_somente_acmsn,'N') = 'G') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S'))) or
					((nvl(ie_somente_acmsn,'N') = 'S') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S'))) or
					((nvl(ie_somente_acmsn,'N') = 'A') and (ie_acm_w = 'S') and (ie_se_necessario_w = 'N')) or
					((nvl(ie_somente_acmsn,'N') = 'C') and (ie_acm_w = 'N') and (ie_se_necessario_w = 'S')) or
					(nvl(ie_somente_acmsn,'N') = 'N'))
      and ((nvl(z.ie_somente_fracionados, 'N') = 'S' and ie_item_fracionado_w = 'S') or (nvl(z.ie_somente_fracionados, 'N') = 'N')))
order by
  nvl(a.ie_somente_fracionados, 'N'),
	nvl(b.cd_setor_atendimento,0),
	nvl(a.cd_local_estoque,0),
	nvl(a.cd_material,0),
	nvl(a.cd_controlador_estoque,0),
	nvl(a.cd_grupo_material,0),
	nvl(a.cd_subgrupo_material,0),
	nvl(a.cd_classe_material,0),
	nvl(nr_seq_familia,0),
	nvl(a.cd_unidade_medida,'AAAAA'),
	nvl(a.ie_via_aplicacao,'AAAAA'),
	NVL(DECODE(ie_somente_acmsn,'N',NULL,ie_somente_acmsn),'AAAAA');

begin
if	(nr_prescricao_p is not null) then
	
	select	nvl(max(cd_setor_atendimento),0),
		max(nr_atendimento),
		max(cd_estabelecimento),
		max(obter_local_estoque_setor(cd_setor_atendimento, cd_estabelecimento))
	into	cd_setor_atendimento_w,
		nr_atendimento_w,
		cd_estabelecimento_w,
		cd_local_estoque_prescr_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	
	select	nvl(max(nr_seq_agrup_classif),0),
			nvl(max(nr_seq_agrupamento),0)
	into	nr_seq_agrup_classif_w,
			nr_seq_agrupamento_w
	from	setor_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_w; 

	select	nvl(max(ie_local_estoque_proc),'P'),
			nvl(max(ie_gerar_area_sem_disp),'S')
	into	ie_local_estoque_proc_w,
			ie_gerar_area_sem_disp_w
	from	parametros_farmacia
	where	cd_estabelecimento	= cd_estabelecimento_w;

	cd_setor_prescr_w	:= Obter_Unidade_Atendimento(nr_atendimento_w,'IAA','CS');

	if	(ie_local_estoque_proc_w = 'P') then
		cd_local_prescr_w	:= obter_local_estoque_setor(cd_setor_prescr_w, obter_estab_atend(nr_atendimento_w));
	elsif	(ie_local_estoque_proc_w = 'S') then
		cd_local_prescr_w	:= nvl(cd_local_estoque_prescr_w, obter_local_estoque_setor(cd_setor_prescr_w, obter_estab_atend(nr_atendimento_w)));
	end if;
	
	open c01;
	loop
	fetch c01 into	cd_local_estoque_w,
			nr_seq_material_w,
			cd_unidade_medida_w,
			ie_via_aplicacao_w,
			cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w,
			cd_material_w,
			nr_seq_familia_w,
			cd_material_estoque_w,
			ie_agrupador_w,
			ie_urgencia_w,
			ie_acm_w,			
			ie_se_necessario_w,
			qt_dose_especial_w,
			qt_dose_w;
	exit when c01%notfound;
		begin
			
		nr_seq_regra_prep_w	:= 0;
		nr_seq_area_prep_w	:= 0;
		
		if	(ie_local_estoque_proc_w = 'R') then
			cd_local_estoque_w	:= obter_local_estoque_regra_ged(cd_setor_prescr_w,nr_prescricao_p,nr_seq_material_w,null,null,'N');	
		end if;
    
    if ((qt_dose_w - trunc(qt_dose_w)) = 0) then
      ie_item_fracionado_w := 'N';
    else
      ie_item_fracionado_w := 'S';
    end if;
          
		open c02;
		loop
		fetch c02 into	nr_seq_regra_prep_w,
				nr_seq_area_prep_w;
		exit when c02%notfound;
			begin
			nr_seq_area_prep_w := nr_seq_area_prep_w;
			end;
		end loop;
		close c02;
		end;
	end loop;
	close c01;
end if;

return	nr_seq_area_prep_w;


end obter_area_preparo;
/
