create or replace
function san_obter_resultado_procergs(	nr_seq_doacao_p		number,
					ie_tipo_exame_p		number)
 		    	return varchar2 is
-- ie_tipo_exame_p = valor do dom�nio: SAN - Tipo de exame
			
ds_retorno_w		varchar2(3);
nr_seq_exame_lote_w	san_exame_lote.nr_sequencia%type;
begin

if 	(nr_seq_doacao_p is not null) and
	(ie_tipo_exame_p is not null) then

	select	max(nr_sequencia)
	into	nr_seq_exame_lote_w
	from	san_exame_lote
	where	nr_seq_doacao = nr_seq_doacao_p;
	
	if	(nr_seq_exame_lote_w is not null) then
		select	decode(max(upper(a.ds_resultado)), 'POSITIVO', 'POS', 'NEGATIVO', 'NEG', null, 'NAR', ' ')
		into	ds_retorno_w
		from	san_exame_realizado a,
			san_exame b
		where	a.nr_seq_exame_lote = nr_seq_exame_lote_w
		and	a.nr_seq_exame = b.nr_sequencia
		and	b.ie_tipo_exame = ie_tipo_exame_p
		and	rownum = 1;
	end if;	
	
end if;

return	ds_retorno_w;

end san_obter_resultado_procergs;
/