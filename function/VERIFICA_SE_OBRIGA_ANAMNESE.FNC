create or replace
function Verifica_se_obriga_anamnese(nr_prescricao_p		number)
 		    	return varchar2 is

ie_obriga_w			varchar2(1);			
cd_convenio_w			number(5);
ie_tipo_atendimento_w		number(5);
ie_geral_w			varchar2(1);
			
begin

select	max(a.ie_tipo_atendimento),
	Obter_Convenio_Atendimento(max(a.nr_atendimento))
into	ie_tipo_atendimento_w,
	cd_convenio_w	
from	prescr_medica b,
	atendimento_paciente a
where	a.nr_atendimento = b.nr_atendimento
and	b.nr_prescricao  = nr_prescricao_p;

select	decode(count(*),0,'N','S')
into	ie_geral_w
from	regra_obriga_anamnese
where	cd_convenio is null
and	ie_tipo_atendimento is null;

if	(ie_geral_w = 'S') then
	select	max(ie_consistir)
	into	ie_obriga_w
	from	regra_obriga_anamnese
	where	cd_convenio is null
	and	ie_tipo_atendimento is null;
else
	select	nvl(max(ie_consistir),'N')
	into	ie_obriga_w
	from	regra_obriga_anamnese
	where	((cd_convenio = cd_convenio_w) or
		 (cd_convenio is null))
	and	((ie_tipo_atendimento = ie_tipo_atendimento_w) or
		 (ie_tipo_atendimento is null));
end if;

return	ie_obriga_w;

end Verifica_se_obriga_anamnese;
/