create or replace
function ctb_pls_obter_valor_prov_fat(	cd_estabelecimento_p		Number,
					nr_seq_conta_p 			Number,
					nr_seq_pos_estab_contab_p 	Number,
					ie_tipo_valor_p			varchar2)
				return number is

/*
		###
		ie_tipo_valor_p:
			'P' : Provisao
			'A' : Administracao
			'D' : Diferenca
			'R' : Reembolso
			'AJ': Ajuste
		###
*/

ie_status_prov_pagto_w			pls_parametro_contabil.ie_status_prov_pagto%type;
ie_lote_ajuste_fat_w			pls_parametro_contabil.ie_lote_ajuste_fat%type;
ie_lote_taxa_fat_w			pls_parametro_contabil.ie_lote_taxa_fat%type;
ie_lote_reembolso_fat_w			pls_parametro_contabil.ie_lote_reembolso_fat%type;
ie_lote_dif_fat_w			pls_parametro_contabil.ie_lote_dif_fat%type;
ie_lote_taxa_adm_w			pls_parametro_contabil.ie_lote_taxa_adm%type;
vl_provisao_w				pls_conta_pos_estab_contab.vl_provisao%type;
vl_administracao_w			pls_conta_pos_estab_contab.vl_administracao%type;
vl_diferenca_w				pls_conta_pos_estab_contab.vl_provisao%type;
vl_reembolso_w				pls_conta_pos_estab_contab.vl_custo_operacional%type;
vl_contab_ajuste_w			pls_conta_pos_estab_contab.vl_custo_operacional%type;
vl_retorno_w				number(15,2) := 0;

begin

select	nvl(max(ie_status_prov_pagto), 'N'),
	nvl(max(ie_lote_ajuste_fat), 'R'),
	nvl(max(ie_lote_taxa_fat), 'R'),
	nvl(max(ie_lote_reembolso_fat), 'R'),
	nvl(max(ie_lote_dif_fat), 'X'),
	nvl(max(ie_lote_taxa_adm), 'R')
into	ie_status_prov_pagto_w,
	ie_lote_ajuste_fat_w,
	ie_lote_taxa_fat_w,
	ie_lote_reembolso_fat_w,
	ie_lote_dif_fat_w,
	ie_lote_taxa_adm_w
from	pls_parametro_contabil
where	cd_estabelecimento = cd_estabelecimento_p;

begin
select	nvl(e.vl_provisao,0) vl_provisao,
	nvl(e.vl_administracao,0) vl_administracao,
	nvl(to_number(pls_obter_vl_pag_fat_pos(e.nr_sequencia,'T')), 0) vl_diferenca,
	(nvl(e.vl_custo_operacional,0) - nvl(e.vl_administracao,0)) vl_reembolso,
	(nvl(e.vl_custo_operacional,0) - nvl(e.vl_provisao,0)) vl_contab_ajuste
into	vl_provisao_w,
	vl_administracao_w,
	vl_diferenca_w,
	vl_reembolso_w,
	vl_contab_ajuste_w
from	pls_conta_pos_estab_contab 		e,
	pls_conta_pos_estabelecido		d,
	pls_conta				b
where	d.nr_sequencia		= e.nr_seq_conta_pos
and	b.nr_sequencia		= d.nr_seq_conta
and	b.nr_sequencia 		= nr_seq_conta_p
and	e.nr_sequencia 		= nr_seq_pos_estab_contab_p;
exception when others then
vl_provisao_w		:= 0;
vl_administracao_w	:= 0;
vl_diferenca_w		:= 0;
vl_reembolso_w		:= 0;
vl_contab_ajuste_w	:= 0;
end;

if	(ie_tipo_valor_p = 'P') then
	if	(ie_lote_taxa_fat_w not in ('P','A') and ie_lote_reembolso_fat_w not in ('P','A')) then
		vl_retorno_w := vl_provisao_w;
	end if;
elsif	(ie_tipo_valor_p = 'A') then
	if	(ie_lote_taxa_fat_w in ('P','A')) then
		vl_retorno_w := vl_administracao_w;
	end if;
elsif	(ie_tipo_valor_p = 'D') then
	if	(ie_lote_dif_fat_w = 'P' or (ie_lote_dif_fat_w = 'X' and ie_lote_ajuste_fat_w = 'P')) then
		vl_retorno_w := vl_diferenca_w;
	end if;
elsif	(ie_tipo_valor_p = 'R') then
	if	(ie_lote_reembolso_fat_w in ('P','A')) then
		vl_retorno_w := vl_reembolso_w;
	end if;
elsif	(ie_tipo_valor_p = 'AJ') then
	if	(ie_lote_ajuste_fat_w	= 'P') then
		vl_retorno_w := vl_contab_ajuste_w;
	end if;
end if;


return	vl_retorno_w;

end ctb_pls_obter_valor_prov_fat;
/
