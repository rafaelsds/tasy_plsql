create or replace FUNCTION CLIP_GET_TYPE_DEF (
    record_type_p IN VARCHAR2
) RETURN NUMBER AS
    nr_seq_p NUMBER;
BEGIN
    SELECT
        nr_sequencia
    INTO nr_seq_p
    FROM
        clinical_note_settings
    WHERE
        ie_data_type = 'CLIPBOARD'
        AND ie_med_rec_type = record_type_p
        AND ie_default_rule = 'S'
        AND ie_situacao = 'A';

    RETURN nr_seq_p;
END CLIP_GET_TYPE_DEF; 
/
