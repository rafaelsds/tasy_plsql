CREATE OR REPLACE FUNCTION OBTER_DATA_NOTA_CREDITO	(nr_seq_nota_credito_p	NUMBER,
					ie_opcao_p		VARCHAR2)
						RETURN		DATE IS

/* ie_opcao_p
'DT'		Data da nota de cr�dito		dt_nota_credito
'DV'		Data de vencimento		dt_vencimento
*/

ds_retorno_w		DATE;
dt_nota_credito_w	          	DATE;
dt_vencimento_w		DATE;

BEGIN

SELECT	a.dt_nota_credito,
	a.dt_vencimento
INTO	dt_nota_credito_w,
	dt_vencimento_w
FROM	nota_credito a
WHERE	a.nr_sequencia	= nr_seq_nota_credito_p;


IF	(ie_opcao_p	= 'DT') THEN
	ds_retorno_w	:= dt_nota_credito_w;
ELSIF	(ie_opcao_p	= 'DV') THEN
	ds_retorno_w	:= dt_vencimento_w;
END IF;

RETURN ds_retorno_w;

END OBTER_DATA_NOTA_CREDITO;
/
