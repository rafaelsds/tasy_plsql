create or replace
function obter_se_gerar_item_ged	(	ie_tipo_item_p	varchar2,
										nr_prescricao_p	number,
										nr_seq_item_p	number,
										nr_agrupamento_p	number,
										ie_superior_p	varchar2,
										nr_seq_kit_p	number,
										ie_agrupador_p	number)
										return varchar2 is

ie_gerar_adep_w	varchar2(1) := 'S';

begin
if	(nr_agrupamento_p is not null) and
	(ie_tipo_item_p = 'M') and
	(ie_agrupador_p = 1) then
	
	if		(nvl(ie_superior_p,'N') = 'S') then
			ie_gerar_adep_w	:= 'S';
	elsif	(nvl(nr_seq_kit_p,0) > 0) then
			ie_gerar_adep_w	:= 'N';
	else
			select	decode(count(*),0,'S','N')
			into	ie_gerar_adep_w
			from	prescr_material b
			where	b.nr_prescricao		= nr_prescricao_p
			and		b.nr_agrupamento	= nr_agrupamento_p
			and		b.ie_agrupador		= 1
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		exists (	select	1
								from	prescr_material x
								where	x.nr_prescricao 	= b.nr_prescricao
								and		x.nr_sequencia		<> nr_seq_item_p
								and		x.nr_agrupamento	= b.nr_agrupamento
								and		x.ie_agrupador		= 1
								and		x.ie_item_superior		= 'S');
	end if;
end if;

return ie_gerar_adep_w;

end obter_se_gerar_item_ged;
/