create or replace
function rop_obter_qt_min_max_roupa(	cd_estabelecimento_p		number,
				cd_setor_atendimento_p		number,
				nr_seq_roupa_p			number,
				ie_opcao_p			number)
 		    	return number is
/*ie_opcao_p
0 - minimo
1 - maximo */
			
qt_minimo_w		number(15,4);
qt_maximo_w		number(15,4);
qt_retorno_w		number(15,4);
			
begin

select	nvl(max(qt_minimo),0),
	nvl(max(qt_maximo),0)
into	qt_minimo_w,
	qt_maximo_w
from (
	select	nvl(max(qt_minimo),0) qt_minimo,
		nvl(max(qt_maximo),0) qt_maximo
	from	rop_regra_roupa_setor
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_setor_atendimento	= cd_setor_atendimento_p
	and	nr_seq_roupa		= nr_seq_roupa_p
	union
	select	nvl(max(b.qt_minimo),0),
		nvl(max(b.qt_maximo),0)
	from	rop_regra_roupa_setor a,
		rop_regra_roupa_setores b
	where	a.nr_sequencia		= b.nr_seq_regra
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_setor_atendimento	= b.cd_setor_atendimento
	and	b.cd_setor_atendimento	= cd_setor_atendimento_p
	and	a.nr_seq_roupa		= nr_seq_roupa_p);

if	(ie_opcao_p = 0) then
	qt_retorno_w := qt_minimo_w;
elsif	(ie_opcao_p = 1) then
	qt_retorno_w := qt_maximo_w;
end if;

return	qt_retorno_w;

end rop_obter_qt_min_max_roupa;
/
