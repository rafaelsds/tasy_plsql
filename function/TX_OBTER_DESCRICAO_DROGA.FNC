create or replace
function tx_obter_descricao_droga (	nr_seq_droga_p	number)
					return varchar2 is
ds_droga_w	varchar2(255);
begin

if	(nr_seq_droga_p > 0) then

	select 	ds_droga
	into	ds_droga_w
	from	tx_drogas_paciente
	where 	nr_sequencia = nr_seq_droga_p;

end if;

return ds_droga_w;

end tx_obter_descricao_droga;
/