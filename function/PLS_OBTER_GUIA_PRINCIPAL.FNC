create or replace
function pls_obter_guia_principal(	nr_seq_guia_p		pls_guia_plano.nr_sequencia%type,
					nr_seq_requisicao_p	pls_requisicao.nr_sequencia%type,
					nr_seq_execucao_p	pls_execucao_requisicao.nr_sequencia%type)
					return varchar2 is

cd_guia_principal_w			pls_guia_plano.cd_guia_principal%type	:= '';

begin
if	(nr_seq_guia_p is not null) then
	select	nvl(cd_guia_principal, nr_seq_guia_principal)
	into	cd_guia_principal_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_p;
elsif	(nr_seq_requisicao_p is not null) then
	select	nvl(cd_guia_principal, nr_seq_guia_principal)
	into	cd_guia_principal_w
	from	pls_requisicao
	where	nr_sequencia = nr_seq_requisicao_p;
elsif	(nr_seq_execucao_p is not null) then
	select	nvl(b.cd_guia_principal, b.nr_seq_guia_principal)
	into	cd_guia_principal_w
	from	pls_execucao_requisicao a,
		pls_requisicao b
	where	a.nr_seq_requisicao = b.nr_sequencia
	and	a.nr_sequencia = nr_seq_execucao_p;
end if;

return	cd_guia_principal_w;

end pls_obter_guia_principal;
/