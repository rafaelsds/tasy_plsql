create or replace
function obter_qt_pecas_sem_local_lote(	nr_seq_lote_p		number)
 		    	return number is

qt_pecas_w		number(10);			
			
begin

select	count(*)
into	qt_pecas_w
from	rop_roupa
where	nr_seq_lote_roupa = nr_seq_lote_p
and	nr_seq_local is null;

return	qt_pecas_w;

end obter_qt_pecas_sem_local_lote;
/