create or replace
function INTPD_OBTER_ESTAB_FILA(
		NR_SEQ_FILA_P	number)
		return number is

cd_estab_w	intpd_fila_transmissao.cd_estab_documento%type;
		
begin
begin
	
	select nvl(cd_estab_documento,0)
	into cd_estab_w
	from intpd_fila_transmissao
	where nr_Sequencia = NR_SEQ_FILA_P; 
exception
when others then
	cd_estab_w	:=	0;
end;

return	cd_estab_w;

end INTPD_OBTER_ESTAB_FILA;
/