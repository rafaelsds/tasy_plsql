create or replace
function obter_setor_prescricao(nr_prescricao_p	number,
			   	    ie_opcao_p	varchar2)
	return varchar2 as

cd_retorno_w	number(5,0);
ds_retorno_w	varchar(100);

begin

select	max(cd_setor_atendimento),
	substr(obter_nome_setor(max(cd_setor_atendimento)),1,100)
into	cd_retorno_w,
	ds_retorno_w
from	prescr_medica
where	nr_prescricao	=	nr_prescricao_p;

if 	(ie_opcao_p = 'C') then
	return	cd_retorno_w;
else
	return	ds_retorno_w;
end if;

end obter_setor_prescricao;
/