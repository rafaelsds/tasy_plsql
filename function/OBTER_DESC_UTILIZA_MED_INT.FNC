create or replace
function obter_desc_utiliza_med_int(ie_utiliza_medic_intern_p	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin

if (ie_utiliza_medic_intern_p = 'S') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(338113); --Sim. Sem altera��o de dose
elsif (ie_utiliza_medic_intern_p = 'M') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(338114); --Sim. Com altera��o de dose
elsif (ie_utiliza_medic_intern_p = 'N') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(334314); --N�o
else
	ds_retorno_w := '';
end if;

return	ds_retorno_w;

end obter_desc_utiliza_med_int;
/