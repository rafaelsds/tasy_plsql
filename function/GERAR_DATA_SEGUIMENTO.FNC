create or replace
FUNCTION Gerar_Data_Seguimento(nr_sequencia_p	Number)
			return date is

qt_registro_w	number(5,0);
dt_aux_w	date;
	
begin

select	count(*)
into	qt_registro_w
from	can_ficha_seguimento
where	NR_SEQ_FICHA_ADMISSAO = nr_sequencia_p;

if	(qt_registro_w = 0) then
	begin
	
	select	PKG_DATE_UTILS.ADD_MONTH(dt_consulta, 24,0)
	into	dt_aux_w
	from	can_ficha_admissao
	where	nr_sequencia = nr_sequencia_p;
	
	end;
elsif (qt_registro_w > 0)	and 
	(qt_registro_w < 11)	then
	begin	
	
	select	PKG_DATE_UTILS.ADD_MONTH(max(dt_seguimento),12,0)
	into	dt_aux_w
	from	can_ficha_seguimento
	where	NR_SEQ_FICHA_ADMISSAO = nr_sequencia_p;
	
	end;
elsif (qt_registro_w > 10) then
	begin

	select	PKG_DATE_UTILS.ADD_MONTH(max(dt_seguimento),60,0)
	into	dt_aux_w
	from	can_ficha_seguimento
	where	NR_SEQ_FICHA_ADMISSAO = nr_sequencia_p;
		
	end;
end if;

return	dt_aux_w;

END	Gerar_Data_Seguimento;
/
