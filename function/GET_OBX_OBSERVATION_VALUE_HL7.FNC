create or replace
function get_obx_observation_value_hl7 (
	ds_resultado_p	varchar2,
	qt_resultado_p	number,
	pr_resultado_p	number)
	return varchar2 is
	
obx_observation_value_w	varchar2(4000);
	
begin
if	(ds_resultado_p is not null) then
	begin
	obx_observation_value_w := ds_resultado_p;
	end;
elsif	(qt_resultado_p is not null) then
	begin
	obx_observation_value_w := qt_resultado_p;
	end;
elsif	(pr_resultado_p is not null) then
	begin
	obx_observation_value_w := pr_resultado_p;
	end;	
end if;
return obx_observation_value_w;
end get_obx_observation_value_hl7;
/