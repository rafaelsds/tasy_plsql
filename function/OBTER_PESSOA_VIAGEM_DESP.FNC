create or replace
function obter_pessoa_viagem_desp
			(	nr_seq_viagem_desp_p		Number,
				ie_tipo_retorno_p		Varchar2)
 		    		return Varchar2 is
	
ds_retorno_w			Varchar2(80);
cd_pessoa_fisica_w		Varchar2(10);
nm_pessoa_fisica_w		Varchar2(80);

begin

begin
select	b.cd_pessoa_fisica,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,80)
into	cd_pessoa_fisica_w,
	nm_pessoa_fisica_w
from	via_viagem	b,
	via_relat_desp	a
where	a.nr_seq_viagem	= b.nr_sequencia
and	a.nr_sequencia	= nr_seq_viagem_desp_p;
exception
	when others then
	cd_pessoa_fisica_w	:= '';
	nm_pessoa_fisica_w	:= '';
end;


if	(ie_tipo_retorno_p	= 'C') then
	ds_retorno_w		:= cd_pessoa_fisica_w;
elsif	(ie_tipo_retorno_p	= 'N') then
	ds_retorno_w		:= nm_pessoa_fisica_w;
end if;

return	ds_retorno_w;

end obter_pessoa_viagem_desp;
/