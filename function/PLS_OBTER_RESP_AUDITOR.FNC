create or replace
function pls_obter_resp_auditor
		(	nr_seq_resp_auditor_p	Number,
			ie_tipo_p		Number)
 		    	return Varchar2 is
			
/* IE_TIPO_P
	1 - CPF / CGC
	2 - Nome
*/

cd_pf_cgc_w		Varchar2(14);
nm_responsavel_w	Varchar2(80);
ds_retorno_w		Varchar2(80);
ie_tipo_pessoa_w	Varchar2(2);

begin

select	nvl(cd_pessoa_fisica, cd_cgc),
	substr(obter_nome_pf_pj(cd_pessoa_fisica, cd_cgc),1,80)
into	cd_pf_cgc_w,
	nm_responsavel_w
from	pls_responsavel_auditor
where	nr_sequencia	= nr_seq_resp_auditor_p;

if	(length(cd_pf_cgc_w) < 11) then
	ie_tipo_pessoa_w	:= 'PF';
else
	ie_tipo_pessoa_w	:= 'PJ';
end if;

if	(ie_tipo_p	= 1) then
	if	(ie_tipo_pessoa_w = 'PF') then
		ds_retorno_w	:= substr(obter_dados_pf(cd_pf_cgc_w, 'CPF'),1,11);
	elsif	(ie_tipo_pessoa_w = 'PJ') then
		ds_retorno_w	:= cd_pf_cgc_w;
	end if;
else
	if	(ie_tipo_pessoa_w = 'PF') then
		ds_retorno_w	:= substr(obter_nome_pf(cd_pf_cgc_w),1,60);
	elsif	(ie_tipo_pessoa_w = 'PJ') then
		ds_retorno_w	:= substr(obter_razao_social(cd_pf_cgc_w),1,80);
	end if;
end if;

return	ds_retorno_w;

end pls_obter_resp_auditor;
/