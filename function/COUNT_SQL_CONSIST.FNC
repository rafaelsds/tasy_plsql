CREATE OR REPLACE FUNCTION count_sql_consist(nr_seq_sql_p number)
  RETURN NUMBER
 IS
  count_w dar_consist_sql.nr_seq_sql%TYPE;
BEGIN
select count(nr_sequencia) INTO count_w from dar_consist_sql where nr_seq_sql = nr_seq_sql_p;
 RETURN count_w;
END count_sql_consist;
/
