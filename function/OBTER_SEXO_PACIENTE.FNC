create or replace function obter_sexo_paciente (
    nr_atendimento_p integer
) return varchar2 is
    v_sexo_w varchar(20);
begin
    if ( nr_atendimento_p is not null ) then
        select
            MAX(ie_sexo) as sexo
        into v_sexo_w
        from
            pessoa_fisica          pf,
            atendimento_paciente   at
        where
            at.cd_pessoa_fisica = pf.cd_pessoa_fisica
            and at.nr_atendimento = nr_atendimento_p;

    end if;

    return v_sexo_w;
end obter_sexo_paciente;
/
