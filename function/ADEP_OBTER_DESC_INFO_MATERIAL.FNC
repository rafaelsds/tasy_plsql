create or replace
function adep_obter_desc_info_material(	nr_seq_cpoe_p	number,
							ie_tipo_item_p	varchar2,
							dt_filtro_p		date default null,
							qt_horas_adicionais_p number default 0,
							ie_exibir_dt_fim_p in varchar2 default 'N') return varchar2 is

ds_retorno_w		varchar2(2000 char);
dt_suspensao_w		date;
dt_lib_suspensao_w	date;
ie_tipo_proced_w	prescr_procedimento.ie_tipo_proced%type;
pragma autonomous_transaction;
begin

	if (ie_tipo_item_p in ('M','MAT','SOL', 'IA')) then
		select	max(cpoe_obter_desc_info_material(
						cd_material, -- cd_material_p
						cd_mat_comp1, -- cd_mat_comp1_p
						qt_dose_comp1, -- qt_dose_comp1_p
						cd_unid_med_dose_comp1, -- cd_unid_med_dose_comp1_p
						cd_mat_comp2, -- cd_mat_comp2_p
						qt_dose_comp2, -- qt_dose_comp2_p
						cd_unid_med_dose_comp2, -- cd_unid_med_dose_comp2_p
						cd_mat_comp3, -- cd_mat_comp3_p
						qt_dose_comp3, -- qt_dose_comp3_p
						cd_unid_med_dose_comp3, -- cd_unid_med_dose_comp3_p
						cd_mat_dil, -- cd_mat_dil_p
						qt_dose_dil, -- qt_dose_dil_p
						cd_unid_med_dose_dil, -- cd_unid_med_dose_dil_p
						cd_mat_red, -- cd_mat_red_p
						qt_dose_red, -- qt_dose_red_p
						cd_unid_med_dose_red, -- cd_unid_med_dose_red_p
						dt_liberacao, -- dt_liberacao_p
						qt_dose, -- qt_dose_p
						cd_unidade_medida, -- cd_unidade_medida_p
						ie_via_aplicacao, -- ie_via_aplicacao_p
						cd_intervalo, -- cd_intervalo_p
						dt_lib_suspensao, -- dt_lib_suspensao_p
						dt_suspensao, -- dt_suspensao_p
						ie_administracao, -- ie_administracao_p
						nr_seq_ataque, -- nr_seq_ataque_p
						nr_seq_procedimento, -- nr_seq_procedimento_p
						nr_seq_adicional, -- nr_seq_adicional_p
						nr_seq_hemoterapia, -- nr_seq_hemotherapy_p
						ds_dose_diferenciada, -- ds_dose_diferenciada_p
						ds_dose_diferenciada_dil, -- ds_dose_diferenciada_dil_p
						ds_dose_diferenciada_red, -- ds_dose_diferenciada_red_p
						ds_dose_diferenciada_comp1, -- ds_dose_diferenciada_comp1_p
						ds_dose_diferenciada_comp2, -- ds_dose_diferenciada_comp2_p
						ds_dose_diferenciada_comp3, -- ds_dose_diferenciada_comp3_p
						cd_mat_comp4, -- cd_mat_comp4_p
						qt_dose_comp4, -- qt_dose_comp4_p
						cd_unid_med_dose_comp4, -- cd_unid_med_dose_comp4_p
						ds_dose_diferenciada_comp4, -- ds_dose_diferenciada_comp4_p
						cd_mat_comp5, -- cd_mat_comp5_p
						qt_dose_comp5, -- qt_dose_comp5_p
						cd_unid_med_dose_comp5, -- cd_unid_med_dose_comp5_p
						ds_dose_diferenciada_comp5, -- ds_dose_diferenciada_comp5_p
						cd_mat_comp6, -- cd_mat_comp6_p
						qt_dose_comp6, -- qt_dose_comp6_p
						cd_unid_med_dose_comp6, -- cd_unid_med_dose_comp6_p
						ds_dose_diferenciada_comp6, -- ds_dose_diferenciada_comp6_p
						dt_inicio, -- dt_inicio_p
						dt_fim, -- dt_fim_p
						dt_fim_cih, -- dt_fim_cih_p
						nr_dia_util, -- nr_dia_util_p
						ie_antibiotico, -- ie_antibiotico_p
						ie_tipo_solucao, -- ie_tipo_solucao_p
						cd_mat_comp7, -- cd_mat_comp7_p
						qt_dose_comp7, -- qt_dose_comp7_p
						cd_unid_med_dose_comp7, -- cd_unid_med_dose_comp7_p
						ds_dose_diferenciada_comp7, -- ds_dose_diferenciada_comp7_p
						'N', -- ie_oncologia_p
						null, -- cd_mat_recons_p
						null, -- qt_dose_recons_p
						null, -- cd_unid_med_dose_recons_p
						null, -- ds_dose_diferenciada_rec_p
						ds_solucao, -- ds_titulo_p
						null, -- ie_duracao_p
						null, -- ie_ref_calculo_p
						null, -- nr_etapas_p
						null, -- ie_controle_tempo_p
						null, -- qt_dias_solicitado_p
						null, -- qt_dias_liberado_p
						null, -- nr_atendimento_p
						null, -- ie_baixado_por_alta_p
						null, -- dt_alta_medico_p
						'S', -- ie_exibe_dil_p
						'S', -- ie_adep_p
						nr_sequencia, -- nr_seq_cpoe_p
						null, -- ds_horarios_p
						ie_tipo_item_p, -- ie_tipo_item_p
						null, -- ie_reset_atb_p
						null, -- nr_seq_cpoe_anterior_p
						ds_calc_therap_dose, -- ds_calc_therap_dose_p
						ie_exibir_dt_fim_p, -- ie_exibir_dt_fim_p
						qt_final_concentration, -- qt_final_concentration_p
						ie_um_final_conc_pca, -- ie_um_final_conc_pca_p
						null, -- qt_dose_range_min_p
						null, -- qt_dose_range_max_p
						null, -- ie_dose_range_p
						ds_medic_nao_padrao, -- ds_medic_nao_padrao_p
						ie_medicacao_paciente,-- ie_medicacao_paciente_p
						'N',
						qt_dose_dia)), 
						max(dt_suspensao),
						max(dt_lib_suspensao)
		into		ds_retorno_w,
				dt_suspensao_w,
				dt_lib_suspensao_w
		from		cpoe_material
		where	nr_sequencia = nr_seq_cpoe_p;
		
	elsif (ie_tipo_item_p in ('P','C','G','I','L')) then
	
		select 	max(x.ie_tipo_proced)
		into 	ie_tipo_proced_w
		from 	(select	a.ie_tipo_proced,
						dt_atualizacao_nrec
				from 	prescr_procedimento a
				where 	a.nr_seq_proc_cpoe = nr_seq_cpoe_p
				order by dt_atualizacao_nrec desc) x
		where 	rownum = 1;
		
		if (ie_tipo_proced_w in ('AP', 'APC', 'APH')) then
			select	max(cpoe_obter_desc_grid_proc(
						substr(cpoe_obter_desc_proc_interno(nr_seq_proc_interno),1,255),
						null,
						qt_procedimento,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						dt_lib_suspensao,
						dt_suspensao,
						ie_administracao,
						dt_inicio,
						'S')),
					max(dt_suspensao),
					max(dt_lib_suspensao)
			into	ds_retorno_w,
					dt_suspensao_w,
					dt_lib_suspensao_w
			from	cpoe_anatomia_patologica
			where	nr_sequencia = nr_seq_cpoe_p;
		else
			select	max(cpoe_obter_desc_grid_proc(
							substr(cpoe_obter_desc_proc_interno(nr_seq_proc_interno),1,255),
							cd_intervalo,
							qt_procedimento,
							cd_mat_proc1,
							qt_dose_mat1,
							cd_unid_medida_dose_mat1,
							ie_assoc_adep1,
							cd_mat_proc2,
							qt_dose_mat2,
							cd_unid_medida_dose_mat2,
							ie_assoc_adep2,
							cd_mat_proc3,
							qt_dose_mat3,
							cd_unid_medida_dose_mat3,
							ie_assoc_adep3,
							cd_mat_proc4,
							qt_dose_mat4,
							cd_unid_medida_dose_mat4,
							ie_assoc_adep4,
							cd_mat_proc5,
							qt_dose_mat5,
							cd_unid_medida_dose_mat5,
							ie_assoc_adep5,
							dt_lib_suspensao,
							dt_suspensao,
							ie_administracao,
							null,
							null,
							cd_mat_proc6,
							qt_dose_mat6,
							cd_unid_medida_dose_mat6,
							ie_assoc_adep6,
							cd_mat_proc7,
							qt_dose_mat7,
							cd_unid_medida_dose_mat7,
							ie_assoc_adep7,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							'S',
							'S',
							decode(ie_lado,'E',obter_desc_expressao(289476),'D',obter_desc_expressao(486691),'A',obter_desc_expressao(308326),null))),
							max(dt_suspensao),
							max(dt_lib_suspensao)
			into		ds_retorno_w,
					dt_suspensao_w,
					dt_lib_suspensao_w
			from		cpoe_procedimento
			where	nr_sequencia = nr_seq_cpoe_p;
		end if;
	elsif (ie_tipo_item_p in ('D','SNE','S','LD','J','NAN','NPP')) then
		select	max(cpoe_obter_ds_dieta(
						ie_tipo_dieta,
						cd_dieta,
						cd_material,
						cd_mat_prod1,
						nr_seq_tipo,
						dt_liberacao,
						'S',
						dt_lib_suspensao,
						dt_suspensao,
						ie_administracao,
						cd_intervalo,
						cd_unid_med_prod1,
						ie_via_leite1,
						qt_dose_prod1,
						cd_mat_prod2,
						cd_unid_med_prod2,
						qt_dose_prod2,
						cd_mat_prod3,
						cd_unid_med_prod3,
						qt_dose_prod3,
						cd_mat_prod4,
						cd_unid_med_prod4,
						qt_dose_prod4,
						cd_mat_prod5,
						cd_unid_med_prod5,
						qt_dose_prod5,
						ie_forma,
						qt_dose,
						cd_unidade_medida_dose,
						null,
						null,
						'S',
						ie_via_aplicacao,
						nr_sequencia)),
						max(dt_suspensao),
						max(dt_lib_suspensao)
		into		ds_retorno_w,
				dt_suspensao_w,
				dt_lib_suspensao_w
		from		cpoe_dieta
		where	nr_sequencia = nr_seq_cpoe_p;
	
	elsif (ie_tipo_item_p = 'DI') then
		select	max(cpoe_obter_desc_dialise(
						ie_tipo_dialise,
						nr_seq_protocolo,
						dt_lib_suspensao,
						dt_suspensao,
						qt_fluxo_sangue,
						qt_hora_min_sessao,
						'S',
						ie_hemodialise,
						ie_tipo_peritoneal,
						qt_volume_ciclo,
						qt_hora_min_duracao,
						null,
						dt_atualizacao_peso,
						null,
						null,
						'S',
						nr_sequencia)),
						max(dt_suspensao),
						max(dt_lib_suspensao)
		into		ds_retorno_w,
				dt_suspensao_w,
				dt_lib_suspensao_w
		from		cpoe_dialise
		where	nr_sequencia = nr_seq_cpoe_p;
	
	elsif (ie_tipo_item_p = 'O') then
		select	max(cpoe_obter_desc_grid_gas(
						substr(CPOE_Obter_Desc_Gas(nr_seq_gas,dt_liberacao),1,254),
						qt_gasoterapia,
						cpoe_obter_desc_unid_med_gas(ie_unidade_medida),
						substr(obter_valor_dominio(1299, ie_respiracao),1,200),
						substr(obter_desc_intervalo_prescr(cd_intervalo),1,255),
						dt_lib_suspensao,
						dt_suspensao,
						ie_administracao,
						null,
						null,
						null,
						ie_respiracao,
						ie_disp_resp_esp,
						cd_modalidade_vent,
						'S')),
						max(dt_suspensao),
						max(dt_lib_suspensao)
		into		ds_retorno_w,
				dt_suspensao_w,
				dt_lib_suspensao_w
		from		cpoe_gasoterapia
		where	nr_sequencia = nr_seq_cpoe_p;
	
	elsif (ie_tipo_item_p = 'HM') then
		select	max(substr(cpoe_obter_ds_hemoterapia(
						nr_sequencia,
						dt_liberacao,
						'S',
						dt_lib_suspensao,
						dt_suspensao,
						ie_tipo,
						qt_procedimento,
						ie_reserva,
						null,
						null,
						'S'),1,2000)),
						max(dt_suspensao),
						max(dt_lib_suspensao)
		into		ds_retorno_w,
				dt_suspensao_w,
				dt_lib_suspensao_w
		from		cpoe_hemoterapia
		where	nr_sequencia = nr_seq_cpoe_p;
	
	elsif (ie_tipo_item_p = 'E') then
		select	max(cpoe_obter_desc_grid_sae(
						nr_seq_proc,
						cd_intervalo,
						dt_lib_suspensao,
						dt_suspensao,
						ie_administracao,
						'S')),
						max(dt_suspensao),
						max(dt_lib_suspensao)
		into		ds_retorno_w,
				dt_suspensao_w,
				dt_lib_suspensao_w
		from		cpoe_intervencao
		where	nr_sequencia = nr_seq_cpoe_p;
	
	elsif (ie_tipo_item_p = 'R') then
		select	max(cpoe_obter_desc_grid_recom(
						substr(cpoe_obter_desc_recomendacao(cd_recomendacao,dt_liberacao),1,255),
						substr(obter_desc_intervalo_prescr(cd_intervalo),1,255),
						dt_lib_suspensao,
						dt_suspensao,
						ie_administracao,
						ds_observacao,
						null,
						null,
						null,
						'S',
						nr_seq_topografia)),
						max(dt_suspensao),
						max(dt_lib_suspensao)
		into		ds_retorno_w,
				dt_suspensao_w,
				dt_lib_suspensao_w
		from		cpoe_recomendacao
		where	nr_sequencia = nr_seq_cpoe_p;
	end if;
	
	if (dt_lib_suspensao_w is not null) and
		(dt_suspensao_w is not null) and		
		(dt_filtro_p >= dt_suspensao_w) then
		ds_retorno_w	:= '<del> '||ds_retorno_w|| '</del>';
	end if;
	
return	'<div style="line-height: 1.3 !important; padding:5px 0 0 0px !important;">' || ds_retorno_w || '</div>';
end adep_obter_desc_info_material;
/