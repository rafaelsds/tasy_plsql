create or replace
function pls_obter_se_val_apres_resumo( nr_seq_protocolo_p	pls_protocolo_conta.nr_sequencia%type) return varchar2 is 

/*
	Consiste se vl_cobrado_manual est� ok em rela��o ao valor do protocolo 
	na gera��o do demonstrativo do protocolo(Gerar resumo protocolo)
*/

ie_consiste_valor_w		varchar2(1);
ie_tipo_guia_w			pls_protocolo_conta.nr_sequencia%type;
ie_origem_protocolo_w	pls_protocolo_conta.ie_origem_protocolo%type;
ie_apresentacao_w		pls_protocolo_conta.ie_apresentacao%type;
vl_cobrado_manual_w		pls_protocolo_conta.vl_cobrado_manual%type;
vl_procedimento_imp_w	pls_conta_proc.vl_procedimento_imp%type;
vl_retorno_w			pls_conta_proc.vl_procedimento_imp%type;
vl_material_imp_w		pls_conta_mat.vl_material_imp%type;
ds_erro_w				varchar2(255) := '';

begin

	select	ie_tipo_guia,
			ie_origem_protocolo,
			ie_apresentacao,
			nvl(vl_cobrado_manual,0) vl_cobrado_manual
	into	ie_tipo_guia_w,
			ie_origem_protocolo_w,
			ie_apresentacao_w,
			vl_cobrado_manual_w
	from	pls_protocolo_conta
	where	nr_sequencia = nr_seq_protocolo_p;
	
	ie_consiste_valor_w	:= pls_obter_se_cons_vl_apre_prot( ie_origem_protocolo_w, ie_tipo_guia_w, ie_apresentacao_w);
	
	if	(ie_consiste_valor_w = 'S') then
		select	nvl(sum(a.vl_procedimento_imp), 0) -- Ajustado para verificar o mat tamb�m pois �n�o era verificado
		into	vl_procedimento_imp_w		
		from	pls_conta_proc	a,
			pls_conta	b
		where	a.nr_seq_conta		= b.nr_sequencia
		and	b.nr_seq_protocolo	= nr_seq_protocolo_p
		and	a.ie_status		not in ('D','M');
		
		select	nvl(sum(a.vl_material_imp), 0)
		into	vl_material_imp_w
		from 	pls_conta_mat	a,
			pls_conta	b
		where	a.nr_seq_conta		= b.nr_sequencia
		and	b.nr_seq_protocolo	= nr_seq_protocolo_p
		and	a.ie_status		not in ('D','M');
		
		vl_retorno_w	:= nvl(vl_procedimento_imp_w,0) + nvl(vl_material_imp_w,0);	
		
		if	(vl_retorno_w <> vl_cobrado_manual_w ) then
			if	(vl_cobrado_manual_w > 0) then	
				ds_erro_w := wheb_mensagem_pck.get_texto(682603,'VL_PROCOLO=' || vl_cobrado_manual_w || ';' || 'VL_CONTA=' || vl_retorno_w || ';' || 'NR_SEQ_PROT=' || nr_seq_protocolo_p || ';' || 'NR_SEQ_LOTE=' || null);
			else
				--Se chegou aqui � por que h� diverg�ncia de valores e tamb�m uma regra v�lida de consist�ncia de valor e ainda o vl_cobrado_manual est� nulo, ent�o
				--retora um string de controle para alertar usu�rio.
				ds_erro_w := 'ZERADO';
			end if;
		end if;

	end if;
	
return ds_erro_w;

end pls_obter_se_val_apres_resumo;
/