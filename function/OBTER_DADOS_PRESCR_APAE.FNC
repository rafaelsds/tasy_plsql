create or replace
function obter_dados_prescr_apae(	nr_prescricao_p	number )
					return varchar2 is


ds_dieta_w		varchar2(500);
ds_dietas_w		varchar2(500) := null;
ds_medicamento_w	varchar2(500);
ds_medicamentos_w	varchar2(500) := null;
ds_material_w 		varchar2(500);
ds_materiais_w		varchar2(500) := null;
ds_recomendacao_w 	varchar2(2000);
ds_recomendacoes_w	varchar2(500) := null;
ds_procedimento_w	varchar2(500);
ds_procedimentos_w	varchar2(500) := null;
ds_retorno_w		varchar2(2000);


cursor c01 is
	select		obter_desc_expressao(670822)/*'Dieta: '*/ || substr(obter_nome_dieta(cd_dieta),1,40) || chr(13) ||
			decode(ds_observacao,null,null,'Obs.: ') || substr(ds_observacao,1,200)
	from		prescr_dieta
	where		nr_prescricao	= nr_prescricao_p
	and		ie_suspenso <> 'S'
	order by	1;

cursor c02 is
	select		obter_desc_expressao(330076)/*'Medicamento: '*/ || substr(b.ds_material,1,40) || chr(13) ||
			'Dos.: ' || substr(obter_um_dosagem_prescr(a.nr_prescricao, a.nr_sequencia),1,40) || chr(13) ||
			decode(ds_observacao,null,null,'Obs.: ') || substr(ds_observacao,1,200)
	from  		material b, 
			prescr_material a
	where 		a.cd_material    	= b.cd_material
	and 		a.nr_prescricao  	= nr_prescricao_p
	and 		a.nr_sequencia_solucao 	is null
	and 		a.nr_sequencia_proc 	is null
	and 		a.nr_sequencia_diluicao is null
	and 		a.nr_sequencia_dieta 	is null
	and		a.ie_agrupador		= 1
	and		a.ie_suspenso		<> 'S'
	order by 	a.nr_agrupamento, 
	  		a.nr_sequencia;
cursor c04 is
	select		obter_desc_expressao(329898)/*'Recomendação: '*/ || substr(obter_rec_prescricao(nr_sequencia, nr_prescricao),1,255) || chr(13) ||
			decode(ds_observacao,null,null,'Obs.: ') || substr(ds_recomendacao,1,255)
	from		prescr_recomendacao
	where		nr_prescricao	= nr_prescricao_p
	and		ie_suspenso <> 'S';

cursor c05 is
	select		obter_desc_expressao(296423)/*'Proced.: '*/ || substr(obter_desc_prescr_proc(a.cd_procedimento,a.ie_origem_proced, a.nr_seq_proc_interno),1,240) || chr(13) ||
			decode(ds_observacao,null,null,'Obs.: ') || substr(ds_observacao,1,200)		
	from   		prescr_procedimento a
	where  		a.nr_prescricao      	= nr_prescricao_p
	and		obter_se_exibe_proced(a.nr_prescricao,a.nr_sequencia,a.ie_tipo_proced,'O') = 'S'
	and		a.nr_seq_solic_sangue is null
	and		a.ie_suspenso		<> 'S'
	and		a.nr_seq_origem is null
	order by 	a.nr_agrupamento;

begin

OPEN C01;
LOOP
FETCH C01 into
	ds_dieta_w;
exit when c01%notfound;
	begin
	ds_dietas_w		:=	substr(ds_dietas_w || ds_dieta_w || chr(13),1,500);
	end;
END LOOP;
CLOSE C01;

OPEN C02;
LOOP
FETCH C02 into
	ds_medicamento_w;
exit when c02%notfound;
	begin
	ds_medicamentos_w	:=	substr(ds_medicamentos_w || ds_medicamento_w || chr(13),1,500);
	end;
END LOOP;
CLOSE C02;

OPEN C04;
LOOP
FETCH C04 into
	ds_recomendacao_w;
exit when c04%notfound;
	begin
	ds_recomendacoes_w	:=	substr(ds_recomendacoes_w || ds_recomendacao_w || chr(13),1,500);
	end;
END LOOP;
CLOSE C04;

OPEN C05;
LOOP
FETCH C05 into
	ds_procedimento_w;
exit when c05%notfound;
	begin
	ds_procedimentos_w	:=	substr(ds_procedimentos_w || ds_procedimento_w || chr(13),1,500);
	end;
END LOOP;
CLOSE C05;

if	(ds_dietas_w is not null) and (length(ds_retorno_w || ds_dietas_w) < 1995) then
	ds_retorno_w	:= substr(ds_retorno_w || ds_dietas_w || chr(13),1,2000);
end if;
	
if	(ds_medicamentos_w is not null) and (length(ds_retorno_w || ds_medicamentos_w) < 1995) then
	ds_retorno_w	:= substr(ds_retorno_w || ds_medicamentos_w || chr(13),1,2000);
end if;

if	(ds_materiais_w is not null) and (length(ds_retorno_w || ds_materiais_w) < 1995) then
	ds_retorno_w	:= substr(ds_retorno_w || ds_materiais_w || chr(13),1,2000);
end if;

if	(ds_recomendacoes_w is not null) and (length(ds_retorno_w || ds_recomendacoes_w) < 1995)  then
	ds_retorno_w	:= substr(ds_retorno_w || ds_recomendacoes_w || chr(13),1,2000);
end if;

if	(ds_procedimentos_w is not null) and (length(ds_retorno_w || ds_procedimentos_w) < 1995)  then
	ds_retorno_w	:= substr(ds_retorno_w || ds_procedimentos_w || chr(13),1,2000);
end if;

return ds_retorno_w;

end obter_dados_prescr_apae;
/
