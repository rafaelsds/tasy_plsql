create or replace
function obter_hor_min_espera(nr_minutos_p	number) return varchar2 is

ds_tempo_espera_w	varchar2(20);
nr_horas_w		number(10,0);
nr_minutos_w		number(10,0);

begin
nr_minutos_w := 0;
nr_horas_w   := 0;

nr_minutos_w	:= nr_minutos_p;

while	(nr_minutos_w > 60) loop
	nr_horas_w := nr_horas_w + 1;
	nr_minutos_w := nr_minutos_w - 60;
end loop;

ds_tempo_Espera_w	:= lpad(nr_horas_w, 2, '0') || ':' || lpad(nr_minutos_w, 2, '0');

return	ds_tempo_espera_w;

end obter_hor_min_espera;
/ 
