create or replace
function obter_regra_acesso_func(cd_perfil_p	varchar2,
								 nr_seq_regra_p	number)
				return varchar2 is
				
ie_possui_regra_w	varchar2(1);
begin


select 	decode(count(*),0,'S','N')
into	ie_possui_regra_w	
from	atend_regra_acesso_func
where 	cd_perfil = cd_perfil_p;

if	(ie_possui_regra_w = 'N') then

	select 	decode(count(*),0,'N','S')
	into	ie_possui_regra_w
	from	atend_regra_acesso_func 
	where 	(cd_perfil = cd_perfil_p)
	and 	(nr_seq_regra = nr_seq_regra_p);
  
end if;

return	ie_possui_regra_w;

end obter_regra_acesso_func;
/
