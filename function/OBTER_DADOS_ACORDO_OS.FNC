create or replace
function obter_dados_acordo_os
			(	nr_seq_ordem_servico_p		number)
 		    	return varchar2 is

ds_retorno_w			varchar2(255);
nr_seq_acordo_w			number(10);
dt_prevista_acordo_w		date;

begin

select	max(nr_seq_acordo)
into	nr_seq_acordo_w
from	desenv_acordo_os
where	nr_seq_ordem_servico = nr_seq_ordem_servico_p;

if	(nvl(nr_seq_acordo_w,0) > 0) then
	select	max(dt_entrega_prev)
	into	dt_prevista_acordo_w
	from	desenv_acordo_os
	where	nr_seq_ordem_servico = nr_seq_ordem_servico_p
	and	nr_seq_acordo  = nr_seq_acordo_w;
	
	if	(dt_prevista_acordo_w is not null) then
		ds_retorno_w	:= to_char(dt_prevista_acordo_w,'dd/mm/yyyy');
	end if;
end if;

return	ds_retorno_w;

end obter_dados_acordo_os;
/