create or replace
function pls_obter_data_agenda
			(	dt_analise_p		pls_analise_adesao.dt_analise%type,
				cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type)
				return date is

dt_agenda_w			agenda_consulta.dt_agenda%type;
nr_seq_agenda_consulta_w	agenda_consulta.nr_sequencia%type;

begin

select	max(nr_sequencia)
into	nr_seq_agenda_consulta_w
from	agenda_consulta
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	dt_agenda >= trunc(dt_analise_p,'dd')
and	ie_status_agenda <> 'C';

if	(nr_seq_agenda_consulta_w is null) then
	dt_agenda_w	:= null;
else
	select	dt_agenda
	into	dt_agenda_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_consulta_w;
end if;

return	dt_agenda_w;

end pls_obter_data_agenda;
/