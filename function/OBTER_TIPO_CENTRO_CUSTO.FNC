create or replace
function obter_tipo_centro_custo(
				cd_centro_custo_p	number)
			return varchar2 is

ie_tipo_grupo_w	varchar2(1);
begin

select	nvl(max(a.ie_tipo),'X')
into	ie_tipo_grupo_w
from	centro_custo a
where	a.cd_centro_custo = cd_centro_custo_p;

return ie_tipo_grupo_w;

end obter_tipo_centro_custo;
/