CREATE OR REPLACE FUNCTION 
obter_se_dados_problema (	nr_seq_problema_p	number,
							ie_opcao_p			varchar)
							return varchar2 is
							
ds_retorno_w		varchar2(1) := 'N';


Begin

if 	( nr_seq_problema_p is not null ) then

	if (ie_opcao_p = 'P') then

		Select 	max(decode(nvl(qt_registro,0),0,'N','S'))
		into	ds_retorno_w
		from  ( Select count(1) qt_registro
				from   prescr_item_sintoma
				where  nr_seq_problema = nr_seq_problema_p
				union all
				Select count(1) qt_registro
				from   soap_item_sintoma
				where  nr_seq_problema = nr_seq_problema_p
				union all
				Select count(1) qt_registro
				from   atend_encaminhamento_probl
				where  nr_seq_problema = nr_seq_problema_p);
				
	end if;


end if;

return	ds_retorno_w;

end	obter_se_dados_problema;
/