create or replace
function OBTER_VALOR_TRIB_REPASSE
		(nr_repasse_terceiro_p	number,
		ie_tipo_tributo_p		varchar2) return number is

vl_tributo_w	number(15,2);

begin

select	nvl(sum(c.vl_imposto), 0)
into	vl_tributo_w
from	tributo d,
	repasse_terc_venc_trib c,
	repasse_terceiro_venc b,
	repasse_terceiro a
where	d.cd_tributo			= c.cd_tributo
and	d.ie_tipo_tributo		= nvl(ie_tipo_tributo_p,d.ie_tipo_tributo)
and	c.nr_seq_rep_venc		= b.nr_sequencia
and	b.nr_repasse_terceiro	= a.nr_repasse_terceiro
and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;

return	vl_tributo_w;

end OBTER_VALOR_TRIB_REPASSE;
/

