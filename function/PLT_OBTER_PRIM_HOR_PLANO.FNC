create or replace
function PLT_Obter_Prim_Hor_Plano(	nr_atendimento_p	 		Number,
					cd_setor_atendimento_p			Number,
					dt_prescricao_p				date,
					nm_usuario_p				varchar2)
					return date is

dt_prim_horario_w		date;
dt_prim_horario_aux_w		date;
ie_regra_horario_w		varchar2(15);
	
begin

dt_prim_horario_w	:= Obter_Prim_Horario_Prescricao(nr_atendimento_p, cd_setor_atendimento_p, dt_prescricao_p, nm_usuario_p,'P');

return	dt_prim_horario_w;

end PLT_Obter_Prim_Hor_Plano;
/
