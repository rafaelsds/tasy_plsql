create or replace
function lab_obter_tot_pedido_lsf (nr_lote_externo_p	number,
			   ie_opcao_p	varchar2)
			return number is

qt_retorno_w 	number;
begin
qt_retorno_w := 0;

if (nr_lote_externo_p is not null) then
	if (ie_opcao_p = 'P') then
		select 	count(distinct nr_prescricao)
		into 	qt_retorno_w
		from 	prescr_procedimento
		where 	nr_seq_lote_externo = nr_lote_externo_p;
	elsif (ie_opcao_p = 'E') then
		select 	count(nr_seq_exame)
		into 	qt_retorno_w
		from 	prescr_procedimento
		where 	nr_seq_lote_externo = nr_lote_externo_p;
	end if;
end if;
	
return	qt_retorno_w;

end lab_obter_tot_pedido_lsf;
/	