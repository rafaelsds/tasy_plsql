create or replace
function lic_obter_se_item_sc_reg_preco(	nr_solic_compra_p			number,
					nr_item_solic_compra_p		number)
 		    	return varchar2 is

ie_possui_w			varchar2(1) := 'N';
qt_existe_w			number(10);

begin

select	count(*)
into	qt_existe_w
from	reg_lic_compra a,
	reg_lic_compra_item b
where	a.nr_sequencia		= b.nr_seq_reg_lic_compra
and	b.nr_solic_compra		= nr_solic_compra_p
and	b.nr_item_solic_compra	= nr_item_solic_compra_p;

if	(qt_existe_w > 0) then
	ie_possui_w := 'S';
end if;

return	ie_possui_w;

end lic_obter_se_item_sc_reg_preco;
/
