create or replace
function sus_obter_valor_porte_anest(	cd_edicao_amb_p	number,
				nr_porte_anestesico_p	number,	
				dt_inicio_vigencia_p	date,
				ie_opcao_p	varchar2)
				return varchar2 as
				
vl_retorno_w		varchar2(255);
cd_edicao_amb_w		number(6);
nr_porte_anestesico_w	number(2);
dt_inicio_vigencia_w	date;

/*ie_opcao:
V - valor
P - Porte e valor
*/
					
begin

begin
select	campo_mascara_virgula(vl_porte_anestesico)
into	vl_retorno_w
from	porte_anestesico
where	cd_edicao_amb 		= cd_edicao_amb_p
and	nr_porte_anestesico 	= nr_porte_anestesico_p
and	dt_inicio_vigencia		= dt_inicio_vigencia_p;
exception
when others then
	vl_retorno_w := '';
end;

if	(ie_opcao_p = 'P') then
	vl_retorno_w := nr_porte_anestesico_p||' - '||vl_retorno_w;
end if;

return vl_retorno_w;

end sus_obter_valor_porte_anest;
/