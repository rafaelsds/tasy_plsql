create or replace
function obter_obser_setor_atend(nr_atendimento_p		number)
 		    	return varchar2 is

ds_observacoes_w	varchar2(2000);
ds_obs_w	varchar2(2000);			
	
Cursor C01 is
	select	ds_observacao
	from	atend_paciente_unidade
	where	nr_atendimento = nr_atendimento_p
	and	ds_observacao is not null;

	
begin

open C01;
loop
fetch C01 into	
	ds_obs_w;
exit when C01%notfound;
	begin
	
	if	(ds_observacoes_w is null) then
	
		ds_observacoes_w := ds_obs_w;
	else 
		ds_observacoes_w := substr(ds_observacoes_w || ' - ' || ds_obs_w,1,2000);
	end if;
	
	end;
end loop;
close C01;


return	ds_observacoes_w;

end obter_obser_setor_atend;
/