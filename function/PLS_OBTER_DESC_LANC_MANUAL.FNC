create or replace
function pls_obter_desc_lanc_manual
			(	nr_seq_tipo_lancamento_p	Number)
 		    	return Varchar2 is

ds_retorno_w	Varchar2(255);

begin

select	ds_tipo_lancamento
into	ds_retorno_w
from	pls_tipo_lanc_adic
where	nr_sequencia	= nr_seq_tipo_lancamento_p;

return	ds_retorno_w;

end pls_obter_desc_lanc_manual;
/