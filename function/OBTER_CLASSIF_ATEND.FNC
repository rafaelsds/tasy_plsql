create or replace
function Obter_Classif_Atend
		(nr_atendimento_p	number)
		return varchar2 is

ds_classif_w		varchar2(100);

begin

select	b.ds_classif_agenda
into	ds_classif_w
from	agenda_consulta_v2 b,	
	med_atendimento a
where	a.nr_atendimento	= nr_atendimento_p
and	a.nr_seq_agenda		= b.nr_sequencia;

return	ds_classif_w;

end Obter_Classif_Atend;
/