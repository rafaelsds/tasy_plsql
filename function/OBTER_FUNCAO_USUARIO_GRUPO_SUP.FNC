create or replace
function obter_funcao_usuario_grupo_sup(nm_usuario_p		varchar2,
										nr_seq_grupo_p		number,
										nr_seq_gerencia_p	number)
 		    	return varchar2 is

				
ie_funcao_usuario_w		varchar2(15);

begin

select	max(c.ie_funcao_usuario)
into	ie_funcao_usuario_w
from	usuario_grupo_sup c,
		grupo_suporte b
where	b.nr_sequencia = c.nr_seq_grupo
and		b.nr_seq_gerencia_sup = nr_seq_gerencia_p
and		b.nr_sequencia = nr_seq_grupo_p
and		c.nm_usuario_grupo = nm_usuario_p;

return	ie_funcao_usuario_w;

end obter_funcao_usuario_grupo_sup;
/