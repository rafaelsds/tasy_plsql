create or replace 
function obter_config_pepo_graf(ds_campo_p varchar2)
  return varchar2
is
  retorno_w varchar2(1);
  qt_regra_w number(10);
  
begin

if (ds_campo_p = 'V') then 		
	
	retorno_w := 'S';
	
	select	count(*)
	into    qt_regra_w
	from	pepo_configuracao_agente
	where	nm_usuario	= wheb_usuario_pck.get_nm_usuario;
	
	if (nvl(qt_regra_w,0) > 0) then
	
		select	nvl(max('S'),'N')
		into	retorno_w
		from	pepo_configuracao_agente
		where	nm_usuario	=	wheb_usuario_pck.get_nm_usuario
		and	ds_campo	=	ds_campo_p
		and	ie_opcao_padrao	=	'S';
	end if;
else
	select	nvl(max('S'),'N')
	into	retorno_w
	from	pepo_configuracao_agente
	where	nm_usuario	=	wheb_usuario_pck.get_nm_usuario
	and	ds_campo	=	ds_campo_p
	and	ie_opcao_padrao	=	'S';
	
end if;

return retorno_w;

end obter_config_pepo_graf;
/
