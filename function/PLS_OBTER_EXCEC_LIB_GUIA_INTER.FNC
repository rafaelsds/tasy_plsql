create or replace
function pls_obter_excec_lib_guia_inter
			(	cd_procedimento_p	Number,
				ie_origem_proced_p	Number,
				cd_area_procedimento_p	Number,
				cd_grupo_proc_p		Number,
				cd_especialidade_p	Number,
				dt_solicitacao_p	Date,
				ie_tipo_contrato_p	Varchar2)
				return Number is

nr_seq_regra_excecao_w		Number(10);				
				
begin

select	max(nr_sequencia)
into	nr_seq_regra_excecao_w
from	pls_excecao_lib_inter
where	((cd_procedimento	is null)	or (cd_procedimento		= cd_procedimento_p))
and	((ie_origem_proced	is null)	or (ie_origem_proced		= ie_origem_proced_p))
and	((cd_grupo_proc		is null)	or (cd_grupo_proc		= cd_grupo_proc_p))
and	((cd_especialidade	is null)	or (cd_especialidade		= cd_especialidade_p))
and	((cd_area_procedimento	is null)	or (cd_area_procedimento	= cd_area_procedimento_p))
and	((ie_tipo_contrato	is null)	or (ie_tipo_contrato		= ie_tipo_contrato_p))
and	trunc(dt_solicitacao_p)	between trunc(dt_inicio_vigencia)		and trunc(fim_dia(nvl(dt_fim_vigencia,sysdate)))
order by cd_area_procedimento,
	 cd_especialidade,
	 cd_grupo_proc,
	 cd_procedimento;

return	nr_seq_regra_excecao_w;

end pls_obter_excec_lib_guia_inter;
/