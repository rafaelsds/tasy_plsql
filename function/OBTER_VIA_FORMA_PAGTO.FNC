create or replace
function Obter_Via_Forma_Pagto
	(nr_seq_forma_p		number)
	return varchar2 is

ds_forma_pagto_w		varchar2(100);
	
begin

select	max(ds_forma_pagamento)
into	ds_forma_pagto_w
from	via_forma_pagamento
where	nr_sequencia	= nr_seq_forma_p;

return	ds_forma_pagto_w;

end Obter_Via_Forma_Pagto;
/