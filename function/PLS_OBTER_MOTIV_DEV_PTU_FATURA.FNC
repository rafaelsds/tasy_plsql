create or replace
function pls_obter_motiv_dev_ptu_fatura(nr_seq_motivo_p		number,
				ie_opcao_p		varchar2)
				return varchar2 is
					
ds_retorno_w		varchar2(255);

/*IE_OPCAO_P
D - Descri��o do motivo */ 

begin
if	(ie_opcao_p = 'D') then
	select	substr(ds_motivo,1,255)
	into	ds_retorno_w
	from	ptu_motivo_devolucao_a500
	where 	ie_situacao	= 'A'
	and	nr_sequencia	= nr_seq_motivo_p
	order by ds_motivo;
end if;

return	ds_retorno_w;

end pls_obter_motiv_dev_ptu_fatura;
/