create or replace
function obter_macro_alerta_evento
 		    	return varchar2 is

ds_retorno_w		varchar2(4000);
begin

ds_retorno_w	:=  substr(obter_desc_expressao(649422)/*'@paciente = Nome do paciente'||chr(13)||
						'@nascimento = Dt de nascimento'||chr(13)||
						'@pac_abreviado = Nome do paciente abreviado'||chr(13)||
						'@quarto = Unidade b�sica + compl'||chr(13)||
						'@setor = Setor atendimento'||chr(13)||
						'@atendimento = N� atendimento'||chr(13)||
						'@ds_convenio = Descri��o Conv�nio'||chr(13)||
						'@ramal = N� ramal unidade'||chr(13)||
						'@telefone = N� telefone setor'||chr(13)||
						'@agenda = Descri��o da agenda'||chr(13)||
						'@dtinicioagenda = Hora de inicio da agenda'||chr(13)||
						'@procagenda = Procedimento da agenda'||chr(13)||
						'@dtcancelagenda = Dt de cancelamento da agenda'||chr(13)||
						'@dt_agendamento = Dt da agenda inicial'||chr(13)||
						'@medicoagenda = M�dico do agendamento' || chr(13) ||
						'@data_atual = Dt atual sem horario)' || chr(13) ||
						'@data_hora_atual= Dt atual (com horario)' || chr(13) ||
						'@cd_cid= C�digo do CID' || chr(13) ||
						'@ds_cid= Descri��o do CID' || chr(13) ||
						'@ie_setor_ant_pa= Define se o setor anterior � de PA' || chr(13) ||
						'@opme= Opme da agenda' || chr(13) ||
						'@qtopme= Quantidade do Opme da agenda' || chr(13) ||
						'@autorizacaoopme= Status da autoriza��o do opme' || chr(13) ||
						'@listaopme= Listagem das opme''s da agenda'|| chr(13) ||
						'@fornec_opme= Fornecedor da OPME'|| chr(13) ||
						'@dt_transferencia= Dt da transfer�ncia do agendamento' || chr(13) ||
						'@agedestino= Agenda para qual o agendamento foi transferido' || chr(13) ||
						'@equipsolicitado= Equip solicitado para o empr�stimo' || chr(13) ||
						'@estabeequipsolicitado= Estab do equipamento solicitado para o empr�stimo' || chr(13) ||
						'@estabeequipsolicitante= Estab solicitante que realizou o empr�stimo do equipamento' || chr(13) ||
						'@estabelecimentocme= Estabelecimento do cme' || chr(13) ||
						'@procadicagenda= Proced adicionais da agenda' || chr(13) ||
						'@reserva= N�mero da reserva do agendamento' || chr(13) ||
						'@cmesolicitado= CME solicitado para o empr�stimo'|| chr(13) ||
						'@obs_classif_pessoa=Observa��o da classifica��o da PF'|| chr(13) ||
						'@tipo_atendimento = Tipo de atendimento' || chr(13) ||
						'@convenio = Conv�nio do atendimento' || chr(13) ||
						'@plano = Plano do conv�nio do atendimento' || chr(13) ||
						'@categoria = Categoria do conv�nio do atendimento' || chr(13) ||
						'@classif_pf = Classifica��es da pessoa f�sica' || chr(13) ||
						'@motivo_cancel = Motivo cancelamento' || chr(13) ||
						'@obsagendamento = Observa��o do agendamento' || chr(13) ||
						'@usuarioramal = Ramal do usu�rio' || chr(13) ||
						'@nomeusuario = Nome do usu�rio ' || chr(13) ||
						'@dtorigemtransf = Dt origem do agendamento ' || chr(13) ||
						'@convenio	= Conv�nio' || chr(13) ||
						'@ds_setor_ant	= Setor anterior'  || chr(13) ||
						'@ds_setor_n_ant_pass  = �ltimo setor n�o sendo passagem de setor'   || chr(13) ||
						'@cd_unid_basica_compl_ant = Unidade anterior' || chr(13) ||
						'@dt_baixa_tit_pagar = Dt de baixa do t�tulo' || chr(13) ||
						'@statusagenda = Status da agenda cir�rgica' || chr(13) ||
						'@obs_alta = Observa��o da alta' || chr(13) ||
						'@vl_baixa_tit_pagar = Valor de baixa do t�tulo' || chr(13) ||
						'@set_unidade = Setor da unidade do servi�o de leito'|| chr(13) ||
						'@descpontosmust = Descri��o pontos Escala Must' || chr(13) ||
						'@pontosmust  = Pontua��o Escala Must'|| chr(13) ||
						'@Estagio_Mat_Espec = Est�gio da autoriza��o de materiais especiais'|| chr(13) ||
						'@ds_exame_autor   = C�d. e desc. dos exames autorizados' || chr(13) ||
						'@inicio_vigencia_autor = In�cio vig�ncia da autoriza��o conv�nio ' || chr(13) ||
						'@sequencia_autor = N� seq da autoriza��o conv�nio ' || chr(13) ||
						'@ds_estagio_autor = Descri��o do est�gio da autoriza��o conv�nio ' || chr(13) ||
						'@nr_atend_autor = N� do atendimento da autoriza��o conv�nio ' || chr(13) ||
						'@tipo_guia_autor = Tipo guia da autoriza��o conv�nio ' || chr(13) ||
						'@senha_autor =Senha da autoriza��o conv�nio ' || chr(13) ||
						'@conteudo_mat_autor = C�d, descri��o,qtde solic,qtde autorizada,forncedor do material na autoriza��o ' || chr(13) ||
						'@fim_vigencia_autor = Fim vig�ncia da autoriza��o conv�nio '|| chr(13) ||
						'@pontos_mst = Pontua��o escala MST'|| chr(13) ||
						'@desc_mst = Descri��o pontua��o escala MST'|| chr(13) ||
						'@dt_prevista = Dt prevista na "Gest�o de Vagas"'|| chr(13) ||
						'@dt_alta_prevista = Dt alta prevista' || chr(13) ||
						'@pac_biobanco = Pac. Biobanco' || chr(13) ||
						'@prescricao = N� da prescri��o' || chr(13) ||
						'@exame = Descri��o do exame laboratorial' || chr(13) ||
						'@dt_aprovacao = dt de aprova��o do resultado' || chr(13) ||
						'@result_inaceitavel = Resultado do exame laboratorial(Valor inaceit�vel)' || chr(13) ||
						'@usuario_liberador = Pessoa que liberou/Aprovou o resultado do exame laboratorial' || chr(13) ||
						'@result_anterior_inac = Resultado anterior ao valor inaceit�vel' || chr(13) ||
						'@med_atend_prim = Primeiro nome do m�dico respos�vel do atendimento'|| chr(13) ||
						'@med_resp = Nome do respos�vel do atendimento'|| chr(13) ||
						'@especialidade_dest = Especialidade origem parecer' || chr(13) ||
						'@especialidade_dest_prof = Especialidade dest parecer' || chr(13) ||
						'@tipo_parecer = Tipo parecer' || chr(13) ||
						'@equipe_parecer = Equipe dest parecer'|| chr(13)*/ ||
						'@ds_exame_solicitacao = Exame solicitado' || chr(13) ||
						'@ds_proc_solicitacao = Procedimento solicitado' || chr(13) ||
						'@qt_exame_solicitacao = Qtd. Exame solicitado' || chr(13) ||
						'@ds_exame_sem_cad_solic = Exame sem cadastro' || chr(13) ||
						'@lista_equip_agenda = Equipamentos da agenda' || chr(13) ||
						'@submotivo_alta = Submotivo alta' || chr(13) ||
						'@servico_pac	= Servi�o para o paciente' || chr(13) ||
						'@nm_avaliador_aval = Nome do m�dico que fez a �ltima avalia��o'  || chr(13) ||
						'@dt_avaliacao = Data da �ltima avalia��o' || chr(13) ||
						'@endereco_compelto = Endere�o completo do paciente'  || chr(13) ||
						'@seq_instituicao = Sequ�ncia da institui��o' || chr(13) ||
						'@razao_social = Raz�o social' || chr(13) ||
						'@nr_telefone = Telefone da instituicao' || chr(13) ||
						'@nr_ramal = Ramal da institui��o' || chr(13) ||
						'@ds_instituicao = Descri��o da institui��o' || chr(13) ||
						'@cd_cnes = C�digo CNES da instiui��o' || chr(13) ||
						'@nr_adiant_pago = N�mero(s) do(s) adiantamento(s) pago(s) vinculado(s) ao(s) t�tulo(s)' || chr(13) ||
						'@vl_adiant_tit_pagar = Valor(es) do(s) adiantamento(s) vinculado(s) ao(s) titulo(s)' || chr(13) ||
						'@nm_fornec_adiant_pago = Fornecedor do(s) adiantamento(s)' || chr(13) ||
						obter_desc_expressao(782579)/*'@medicamento = Medicamento solicitado'*/ || 
						'@nr_ordem_compra_adiant = Ordem(s) de compra vinculada ao(s) adiantamento(s)',1,4000);

return	ds_retorno_w;

end obter_macro_alerta_evento;
/
