create or replace
function man_obter_se_recurso_alocado(	nm_recurso_p		varchar2,
					dt_referencia_p		varchar2,
					ie_opcao_p		varchar2,
					nr_seq_proj_p		number   )
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
ds_projeto_w		varchar2(255) := '';
cd_recurso_w		varchar2(10);
ie_consiste_w		varchar2(1);
qt_horas_dia_w		number(15,2);
dt_referencia_w		date;

begin
	ds_retorno_w := 'N';

	if (obter_se_usuario_analista(nm_recurso_p, 'S') <> 'S') and (obter_se_usuario_analista(nm_recurso_p, 'M') <> 'S') and (obter_se_usuario_analista(nm_recurso_p, 'N') <> 'S') then
	begin
		
		dt_referencia_w := to_date(dt_referencia_p,'dd/mm/yyyy');
		select	nvl(max(a.cd_pessoa_fisica), 0)
		into	cd_recurso_w
		from	usuario a
		where	a.nm_usuario = nm_recurso_p;
			
		select		decode(count(*),0,'N','S')
		into		ie_consiste_w
		from		proj_cron_etapa_equipe a,
				proj_cron_etapa b,
				proj_cronograma c,
				proj_projeto d
		where		b.nr_sequencia = a.nr_seq_etapa_cron
		and		c.nr_sequencia = b.nr_seq_cronograma
		and		d.nr_sequencia = c.nr_seq_proj
		and		d.nr_seq_estagio = 12
		and		trunc(b.dt_inicio_prev) = trunc(dt_referencia_w)
		and		b.pr_etapa < 100
		and		a.cd_programador = cd_recurso_w
		and		d.nr_sequencia <> nr_seq_proj_p;
	 
		
		if	(ie_opcao_p = 'P') then
			if(ie_consiste_w = 'S') then
				select		substr((d.nr_sequencia || ' - ' ||d.ds_titulo),1,255)
				into		ds_projeto_w
				from		proj_cron_etapa_equipe a,
						proj_cron_etapa b,
						proj_cronograma c,
						proj_projeto d
				where		b.nr_sequencia = a.nr_seq_etapa_cron
				and		c.nr_sequencia = b.nr_seq_cronograma
				and		d.nr_sequencia = c.nr_seq_proj
				and		d.nr_seq_estagio = 12
				and		trunc(b.dt_inicio_prev) = trunc(dt_referencia_w)
				and		b.pr_etapa < 100
				and		a.cd_programador = cd_recurso_w
				and		d.nr_sequencia <> nr_seq_proj_p
				group by	d.nr_sequencia,d.ds_titulo;
			end if;
			ds_retorno_w := ds_projeto_w;
		elsif	(ie_opcao_p = 'A') then
		begin
			if	(ie_consiste_w = 'S') then
			begin
				select		sum(b.qt_hora_prev)			
				into		qt_horas_dia_w
				from		proj_cron_etapa_equipe a,
						proj_cron_etapa b,
						proj_cronograma c,
						proj_projeto d
				where		b.nr_sequencia = a.nr_seq_etapa_cron
				and		c.nr_sequencia = b.nr_seq_cronograma
				and		d.nr_sequencia = c.nr_seq_proj
				and		d.nr_seq_estagio = 12
				and		trunc(b.dt_inicio_prev) = trunc(dt_referencia_w)
				and		b.pr_etapa < 100
				and		a.cd_programador = cd_recurso_w
				and		d.nr_sequencia <> nr_seq_proj_p;

				if	(qt_horas_dia_w < 4) then
					ie_consiste_w := 'N';
				end if;
			end;
			end if;
		
			ds_retorno_w := ie_consiste_w;
		end;
		end if;
	end;
	end if;

return	ds_retorno_w;

end man_obter_se_recurso_alocado;
/