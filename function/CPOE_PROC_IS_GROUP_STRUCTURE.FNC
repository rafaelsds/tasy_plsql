create or replace
function cpoe_proc_is_group_structure(
				nr_seq_reference_p	pi_estrutura.nr_seq_estrut_sup%type) 
			return varchar2 is 

is_group_structure_w	varchar2(1);
	
begin

if (nr_seq_reference_p is not null) then

	select	decode(count(1), 0, 'N', 'S')
	into	is_group_structure_w
	from	pi_estrutura
	where	nr_seq_estrut_sup = nr_seq_reference_p
	and		nvl(si_group_appointment, 'N') = 'S';

end if;

return is_group_structure_w;

end cpoe_proc_is_group_structure;
/
