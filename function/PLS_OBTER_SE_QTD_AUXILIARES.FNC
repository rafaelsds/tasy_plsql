create or replace
function pls_obter_se_qtd_auxiliares
			(	nr_seq_conta_proc_p	Number)
				return varchar2 is

nr_auxiliares_w			number(10);
qt_participantes_w		number(10);
ds_retorno_w			varchar2(1) := 'S';	
cd_guia_referencia_w		varchar2(20);
cd_guia_w			varchar2(20);
ie_tipo_guia_w			varchar2(2);
qt_reg_resumo_w			number(10);
qt_reg_honorario_w		number(10);
nr_seq_conta_referencia_w	number(10);
nr_seq_conta_w			number(10);
cd_auxiliar_resumo_w		number(10);
cd_auxiliar_honorario_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_conta_proc_w		number(10);
				
begin

/*Obter dados necess�rios*/
select	a.nr_auxiliares,
	a.cd_procedimento,
	a.ie_origem_proced,
	b.nr_sequencia,
	b.cd_guia_referencia, 
	b.cd_guia,
	b.ie_tipo_guia,
	b.nr_seq_conta_referencia,
	b.nr_seq_segurado
into	nr_auxiliares_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_conta_w,
	cd_guia_referencia_w,
	cd_guia_w,
	ie_tipo_guia_w,
	nr_seq_conta_referencia_w,
	nr_seq_segurado_w
from	pls_conta	b,
	pls_conta_proc	a
where	a.nr_sequencia	= nr_seq_conta_proc_p
and	a.nr_seq_conta	= b.nr_sequencia; 

if	(nvl(nr_auxiliares_w,0) > 0) then --S� ser� consistido se existe n�mero de participantes definidos no procedimento
		
	/* Verificar quantos participantes existem na guia de Resumo de interna��o  que n�o s�o cooperados e possuem graus de participa��o como auxiliares*/
	select	count(*)
	into	qt_reg_resumo_w
	from	pls_conta		c,
		pls_conta_proc		b,
		pls_proc_participante	a
	where	a.nr_seq_conta_proc	= b.nr_sequencia
	and	b.nr_seq_conta 		= c.nr_sequencia
	and	c.cd_guia_referencia	= cd_guia_referencia_w
	and	c.nr_seq_segurado	= nr_seq_segurado_w
	--and	pls_obter_se_cooperado(a.cd_medico,null) = 'N'
	and	pls_obter_se_grau_auxiliar(a.nr_seq_grau_partic) = 'S'
	and	c.ie_tipo_guia = '5';

	/* Obter a quantidade de participantes da conta de honorario */
	select	count(*)
	into	qt_reg_honorario_w
	from	pls_conta		c,
		pls_conta_proc		b
	where	b.nr_seq_conta		= c.nr_sequencia
	and	pls_obter_se_grau_auxiliar(c.nr_seq_grau_partic) = 'S'
	and	c.cd_guia_referencia	= cd_guia_referencia_w
	and	c.nr_seq_segurado	= nr_seq_segurado_w
	and	c.ie_tipo_guia = '6';	

	/* Quantidade total de participantes */
	qt_participantes_w	:= qt_reg_resumo_w + qt_reg_honorario_w;	
	
	if	(qt_participantes_w > nr_auxiliares_w)  then	
		
		select	nvl(max(to_number(cd_tiss)),0)
		into	cd_auxiliar_resumo_w
		from	pls_conta_proc		b,
			pls_proc_participante	a,
			pls_grau_participacao	p,
			pls_conta		c
		where	a.nr_seq_conta_proc	= b.nr_sequencia
		and	c.nr_sequencia		= b.nr_seq_conta
		and	a.nr_seq_grau_partic	= p.nr_sequencia		
		--and	pls_obter_se_cooperado(a.cd_medico,null) = 'N'
		and	p.ie_auxiliar		= 'S'
		and	c.cd_guia_referencia	= cd_guia_referencia_w
		and	c.nr_seq_segurado	= nr_seq_segurado_w
		and	c.ie_tipo_guia 		= '5';		
		
		select	nvl(max(to_number(cd_tiss)),0)
		into	cd_auxiliar_honorario_w
		from	pls_conta		c,
			pls_conta_proc		b,
			pls_grau_participacao	p
		where	b.nr_seq_conta		= c.nr_sequencia
		and	c.nr_seq_grau_partic	= p.nr_sequencia
		and	p.ie_auxiliar		= 'S'
		and	c.cd_guia_referencia	= cd_guia_referencia_w
		and	c.nr_seq_segurado	= nr_seq_segurado_w
		and	c.ie_tipo_guia 		= '6';	
		
		if	(ie_tipo_guia_w = '5') then			
			if	(cd_auxiliar_resumo_w > cd_auxiliar_honorario_w) then				
				
				select	max(b.nr_sequencia)
				into	nr_seq_conta_proc_w
				from	pls_conta_proc		b,
					pls_proc_participante	a,
					pls_grau_participacao	p,
					pls_conta		c
				where	a.nr_seq_conta_proc	= b.nr_sequencia
				and	c.nr_sequencia		= b.nr_seq_conta
				and	a.nr_seq_grau_partic	= p.nr_sequencia		
				--and	pls_obter_se_cooperado(a.cd_medico,null) = 'N'
				and	p.ie_auxiliar		= 'S'
				and	c.cd_guia_referencia	= cd_guia_referencia_w
				and	c.nr_seq_segurado	= nr_seq_segurado_w
				and	c.ie_tipo_guia 		= '5'
				and	p.cd_tiss		= cd_auxiliar_resumo_w;
				
				if	(nvl(nr_seq_conta_proc_w,0) = nr_seq_conta_proc_p) then
					ds_retorno_w	:= 'N';
				end if;
			end if;
		elsif	(ie_tipo_guia_w = '6') then									
			if	(cd_auxiliar_honorario_w > cd_auxiliar_resumo_w) then
							
				/*Verificar se trata da mesma conta, sendo que pode existir mais de uma guia de Hi em um atendimento*/
				select	max(b.nr_sequencia)
				into	nr_seq_conta_proc_w
				from	pls_conta		c,
					pls_conta_proc		b,
					pls_grau_participacao	p
				where	b.nr_seq_conta		= c.nr_sequencia
				and	c.nr_seq_grau_partic	= p.nr_sequencia
				and	p.ie_auxiliar		= 'S'
				and	c.cd_guia_referencia	= cd_guia_referencia_w
				and	c.nr_seq_segurado	= nr_seq_segurado_w
				and	c.ie_tipo_guia 		= '6'
				and	to_number(p.cd_tiss)	= cd_auxiliar_honorario_w;
			
				if	(nvl(nr_seq_conta_proc_w,0) = nr_seq_conta_proc_p) then
					ds_retorno_w	:= 'N';
				end if;
			end if;		
		end if;
		
	end if;	
	
end if;

return	ds_retorno_w;

end pls_obter_se_qtd_auxiliares;
/