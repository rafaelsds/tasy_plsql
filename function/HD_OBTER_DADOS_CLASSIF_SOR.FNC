create or replace
function hd_obter_dados_classif_sor(
				nr_seq_classif_p	number,
				ie_opcao_p		varchar2)
 		    	return varchar2 is

ds_Retorno_w		varchar2(90);
ds_classificacao_w	varchar2(90);	
	
begin

select	max(ds_classificacao)
into	ds_classificacao_w
from	hd_classificacao_sorologia
where	nr_sequencia	= nr_seq_classif_p;

if	(ie_opcao_p	= 'D') then
	ds_retorno_w	:= ds_classificacao_w;
end if;

return	ds_Retorno_w;

end hd_obter_dados_classif_sor;
/