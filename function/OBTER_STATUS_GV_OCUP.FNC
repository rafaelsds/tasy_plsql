create or replace
function obter_status_gv_ocup (	nr_seq_interno_p	number,
				nr_atendimento_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);
vl_param_w		varchar2(255);
nr_sequencia_w		gestao_vaga.nr_sequencia%type;
begin

if (nvl(nr_atendimento_p,0) > 0) then
	Obter_param_Usuario(44, 319, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, vl_param_w);
	begin
		if (vl_param_w is not null) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from 	gestao_vaga
			where	nr_atendimento = nr_atendimento_p
			and 	obter_se_contido_char(ie_status, vl_param_w) = 'N';
		else 
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from 	gestao_vaga
			where	nr_atendimento = nr_atendimento_p; 					
		end if;	
		if (nvl(nr_sequencia_w,0) > 0) then 
			select	max(obter_desc_status_gv(ie_status,'D'))
			into	ds_retorno_w
			from 	gestao_vaga
			where	nr_sequencia = nr_sequencia_w; 
			
		end if;
	exception
	when others then
		ds_retorno_w := null;
	end;
end if;
return	ds_retorno_w;

end obter_status_gv_ocup;
/
