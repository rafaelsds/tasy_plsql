create or replace
function obter_se_wjpanel_ultimo_nivel (nr_seq_objeto_p	number)
	return varchar2 is

ie_ultimo_nivel_w varchar2(1) := 'S';

begin
if	(nr_seq_objeto_p is not null) then
	begin
	select	decode(count(*),0,'S','N')
	into	ie_ultimo_nivel_w
	from	dic_objeto
	where	nr_seq_obj_sup = nr_seq_objeto_p
	and	ie_tipo_objeto = 'P';
	end;
end if;
return ie_ultimo_nivel_w;
end obter_se_wjpanel_ultimo_nivel;
/