CREATE OR REPLACE FUNCTION rxt_tratamento_ag_autorizacao(
    nr_seq_tratamento_p rxt_tratamento.nr_sequencia%TYPE
) RETURN VARCHAR2 IS

nr_count_w NUMBER(3);

BEGIN

    SELECT COUNT(nr_sequencia)
    INTO nr_count_w
    FROM rxt_tratamento tratamento
    WHERE tratamento.nr_sequencia = nr_seq_tratamento_p
    AND tratamento.dt_liberacao IS NULL
    AND EXISTS (
        SELECT 1
        FROM autorizacao_convenio
        WHERE nr_seq_rxt_tratamento = tratamento.nr_sequencia
    )
    AND obter_se_tratamento_autorizado(tratamento.nr_sequencia) = 'N';

    IF (nr_count_w > 0) THEN
        RETURN 'S';
    END IF;

    RETURN 'N';

END rxt_tratamento_ag_autorizacao;
/