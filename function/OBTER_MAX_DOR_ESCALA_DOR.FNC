create or replace
function obter_max_dor_escala_dor(dt_sinal_vital_p date
					)
 		    	return number is

ds_retorno_w		number(10);
nr_seq_result_dor_w	varchar2(10);
			
begin

select max(obter_desc_result_dor(nr_seq_result_dor,'I'))
into	nr_seq_result_dor_w
from 	w_eis_escala_dor		
where 	trunc(dt_sinal_vital) = trunc(dt_sinal_vital_p);

select  count(*)
into	ds_retorno_w
from 	w_eis_escala_dor
where 	trunc(dt_sinal_vital) = trunc(dt_sinal_vital_p)
and	nr_seq_result_dor = to_number(nr_seq_result_dor_w);


return	ds_retorno_w;

end obter_max_dor_escala_dor;
/
