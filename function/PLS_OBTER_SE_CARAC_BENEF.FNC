create or replace
function pls_obter_se_carac_benef (	nr_seq_segurado_p	number,
					nr_seq_contrato_p	number,
					dt_solicitacao_p	date,
					nr_seq_caracteristica_p	number)
					return 			varchar2 is

ds_retorno_w	varchar2(1) := 'N';

cursor C01 is
	select	nr_seq_caracteristica
	from	pls_seg_caracteristica
	where	nr_seq_segurado = nr_seq_segurado_p
	and	trunc(dt_solicitacao_p) between trunc(nvl(dt_inicio_vigencia, sysdate)) and trunc(nvl(dt_fim_vigencia, sysdate));
	
cursor C02 is
	select	nr_seq_caracteristica
	from	pls_contrato_caracterist
	where	nr_seq_contrato	= nr_seq_contrato_p
	and	trunc(dt_solicitacao_p) between trunc(nvl(dt_inicio_vigencia, sysdate)) and trunc(nvl(dt_fim_vigencia, sysdate));
begin

if (nr_seq_segurado_p is not null) then
	for r_c01_w in C01 loop
		if (nr_seq_caracteristica_p = r_c01_w.nr_seq_caracteristica) then
			ds_retorno_w := 'S';
			exit;		
		end if;
	end loop;
elsif (nr_seq_contrato_p is not null) then
	for r_c02_w in C02 loop
		if (nr_seq_caracteristica_p = r_c02_w.nr_seq_caracteristica) then
			ds_retorno_w := 'S';
			exit;		
		end if;
	end loop;
end if;

return	ds_retorno_w;

end pls_obter_se_carac_benef;
/