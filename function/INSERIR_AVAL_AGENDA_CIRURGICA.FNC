create or replace	
procedure inserir_aval_agenda_cirurgica(	nr_sequencia_p		number,
					cd_pessoa_fisica_p 	varchar2,
					cd_medico_p		varchar2,
					nr_seq_tipo_avaliacao_p 	number,
					nm_usuario_p		varchar2) is 
begin

if 	(cd_pessoa_fisica_p is not null) then

	insert into med_avaliacao_paciente(	nr_sequencia,
						cd_pessoa_fisica,
						nr_seq_agenda_pac,
						cd_medico,
						nr_seq_tipo_avaliacao,
						dt_avaliacao,
						dt_atualizacao,
						nm_usuario)
				values	  (	med_avaliacao_paciente_seq.nextval,
						cd_pessoa_fisica_p,
						nr_sequencia_p,
						cd_medico_p,
						nr_seq_tipo_avaliacao_p,
						sysdate,
						sysdate,
						nm_usuario_p);
						
end if;

commit;

end inserir_aval_agenda_cirurgica;
/