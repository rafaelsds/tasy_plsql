create or replace
function EO_OBTER_DESC_LOTE(nr_sequencia_p NUMBER)
 		    	return VARCHAR2	 is

ds_retorno_w		Varchar2(255);				

begin

if (nr_sequencia_p is not null) then
	
	select	max(DS_LOTE_FORNEC)
	into	ds_retorno_w
	from	MATERIAL_LOTE_FORNEC
	where 	1 = 1
	and nr_sequencia = nr_sequencia_p;

end if;	

return	ds_retorno_w;

end EO_OBTER_DESC_LOTE;
/