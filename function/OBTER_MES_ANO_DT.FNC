create or replace
function obter_mes_ano_dt(dt_referencia_p		date,
		       ie_opcao_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(10);

/* Op��o
     M - M�s
     A - Ano
*/
	
begin

if	(ie_opcao_p = 'M') then
	select	PKG_DATE_UTILS.extract_field('MONTH', dt_referencia_p)
	into	ds_retorno_w
	from	dual;
elsif 	(ie_opcao_p = 'A') then
	select	PKG_DATE_UTILS.extract_field('YEAR',dt_referencia_p)
	into	ds_retorno_w
	from	dual;
end if;	

return	ds_retorno_w;

end obter_mes_ano_dt;
/
