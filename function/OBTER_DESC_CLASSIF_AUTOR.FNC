create or replace 
function Obter_Desc_classif_autor
		(nr_sequencia_p	number)
		return varchar2 is

ds_classificacao_w	varchar2(255);

begin

select	max(ds_classificacao)
into	ds_classificacao_w
from 	classif_autorizacao
where 	nr_sequencia = nr_sequencia_p;

return	ds_classificacao_w;

end  Obter_Desc_classif_autor;
/