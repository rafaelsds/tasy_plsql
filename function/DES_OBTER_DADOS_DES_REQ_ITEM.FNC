create or replace
function des_obter_dados_des_req_item(	nr_seq_req_item_p		number,
					ie_opcao_p			varchar2)
						return varchar2 is
						
ds_retorno_w		varchar2(255)	:= null;

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter dados do requisito 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

begin

if	(nr_seq_req_item_p is not null) then
	/* N�mero */
	if	(ie_opcao_p = 'NR') then
		select	a.nr_requisito
		into	ds_retorno_w
		from	des_requisito_item a
		where	a.nr_sequencia	= nr_seq_req_item_p;
	/* T�tulo */
	elsif	(ie_opcao_p = 'TI') then
		select	substr(a.ds_titulo,1,255)
		into	ds_retorno_w
		from	des_requisito_item a
		where	a.nr_sequencia	= nr_seq_req_item_p;
	/* Tipo do Requisito */
	elsif	(ie_opcao_p = 'TR') then
		select	substr(a.ie_tipo_requisito,1,255)
		into	ds_retorno_w
		from	des_requisito_item a
		where	a.nr_sequencia	= nr_seq_req_item_p;
	end if;
end if;

return	ds_retorno_w;

end des_obter_dados_des_req_item;
/
