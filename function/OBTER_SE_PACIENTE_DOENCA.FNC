create or replace
function obter_se_paciente_doenca(	cd_doenca_p			varchar2,
									nr_atendimento_p 	number)
 		    	return varchar2 is

nr_doenca_w		number(5);
ds_retorno_w	varchar2(2);

begin

select	count(*)
into	nr_doenca_w
from	diagnostico_doenca
where	cd_doenca = cd_doenca_p
and		nr_atendimento = nr_atendimento_p
and		dt_liberacao is not null
and		dt_inativacao is null;


if	(nr_doenca_w > 0) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
	
end if;

return	ds_retorno_w;

end obter_se_paciente_doenca;
/