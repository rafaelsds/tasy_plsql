CREATE OR REPLACE FUNCTION OBTER_VIA_APLICACAO_AGENT(nr_seq_agente_p  number)
					RETURN VARCHAR2 IS
ie_via_aplicacao_w	varchar2(5) :='';
BEGIN
if (nr_seq_agente_p is not null) then
	select	 nvl(max(IE_VIA_APLICACAO),'')
	into	ie_via_aplicacao_w	
	from	CIRURGIA_AGENTE_ANESTESICO
	where	nr_sequencia	= nr_seq_agente_p
	and	ie_situacao	= 'A';	
end if;

RETURN	ie_via_aplicacao_w;

END	OBTER_VIA_APLICACAO_AGENT;
/