create or replace
function man_obter_sla_detail(ie_tipo_sla_p			number,
							ie_tipo_sla_termino_p	number)
 		    	return varchar2 deterministic is

ds_retorno_w varchar2(5);
				
begin
	/* 1 contrato com multa*/
	/* Customer Start Finish with Penalty'*/	
	if ie_tipo_sla_p = 1 and ie_tipo_sla_termino_p = 1 then
		return 'CSFP';
	end if;	

	/* 1 Contrato com multa*/
	/* Customer Only Finish with Penalty'*/
    if ie_tipo_sla_p = 1 and ie_tipo_sla_termino_p in (1, 3) then
		return 'COSP';
    end if;
	
    /* 2 Contrato sem multa*/
	/* Customer Start Finish No Penalty'*/
	if ie_tipo_sla_p = 2 and ie_tipo_sla_termino_p = 2 then
		return 'CSFNP';
	end if;	

    /* 2 Contrato sem multa*/
	/* Customer Only Start No Penalty'*/
	if ie_tipo_sla_p = 2 and ie_tipo_sla_termino_p = 3 then
		return 'COSNP';
    end if;
	
    /*  3 Sem Contrato */
	/* Philips Start Finish No Penalty'*/
	if ie_tipo_sla_p = 3 and ie_tipo_sla_termino_p = 3 then
		return 'PSFNP';
	end if;
	
return	ds_retorno_w;

end man_obter_sla_detail;
/