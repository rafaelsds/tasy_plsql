create or replace 
function obter_versao_tuss(	nr_sequencia_p        number,
							ie_opcao_p			varchar2)
                                                return varchar2 is

ds_retorno_w			varchar2(120);

begin

if	(ie_opcao_p	= 'CD') then

	select	max(cd_versao)
	into	ds_retorno_w
	from	tuss_servico
	where	nr_sequencia = nr_sequencia_p;
	
elsif (ie_opcao_p	= 'INF') then

	select	max(ds_informacao)
	into	ds_retorno_w
	from	tuss_servico
	where	nr_sequencia = nr_sequencia_p;
	
end if;

return ds_retorno_w;

end obter_versao_tuss;
/
