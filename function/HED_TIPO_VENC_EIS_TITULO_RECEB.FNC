create or replace
function hed_tipo_venc_eis_titulo_receb(
			dt_vencimento_p	date,
			dt_parametro_p	date)
 		    	return varchar2 is

ie_retorno_w		varchar2(15);
dt_parametro_w		date := trunc(dt_parametro_p,'dd');
			
begin
if	(dt_vencimento_p >= dt_parametro_w + 365) then /* Vencer acima de 365 dias */
	ie_retorno_w	:= '13';
elsif	(dt_vencimento_p >= dt_parametro_w + 180) then /* Vencer de 181 a 365 dias */
	ie_retorno_w	:= '11';
elsif	(dt_vencimento_p >= dt_parametro_w + 90) then /* Vencer de 91 a 180 dias */
	ie_retorno_w	:= '5';
elsif	(dt_vencimento_p >= dt_parametro_w + 60) then /* Vencer de 61 a 90 dias */
	ie_retorno_w	:= '4';
elsif	(dt_vencimento_p >= dt_parametro_w + 30) then /* Vencer de 31 a 60 dias */
	ie_retorno_w	:= '3';
elsif	(dt_vencimento_p >= dt_parametro_w + 15) then /* Vencer de 16 a 30 dias */
	ie_retorno_w	:= '2';
elsif	(dt_vencimento_p >= dt_parametro_w) then /* Vencer em 15 dias */
	ie_retorno_w	:= '1';
elsif	(dt_vencimento_p >= dt_parametro_w - 15) then /* Vencido em 15 dias */ 
	ie_retorno_w	:= '6';
elsif	(dt_vencimento_p >= dt_parametro_w - 30) then /* Vencido de 16 a 30 dias */
	ie_retorno_w	:= '7';
elsif	(dt_vencimento_p >= dt_parametro_w - 60) then /* Vencido de 31 a 60 dias */
	ie_retorno_w	:= '8';
elsif	(dt_vencimento_p >= dt_parametro_w - 90) then /* Vencido de 61 a 90 dias */
	ie_retorno_w	:= '9';
elsif	(dt_vencimento_p >= dt_parametro_w - 180) then /* Vencido de 91 a 180 dias */
	ie_retorno_w	:= '12';
elsif	(dt_vencimento_p >= dt_parametro_w - 365) then /* Vencido de 181 a 365 dias */
	ie_retorno_w	:= '14';
else
	ie_retorno_w	:= '10';
end if;

return	ie_retorno_w;

end hed_tipo_venc_eis_titulo_receb;
/