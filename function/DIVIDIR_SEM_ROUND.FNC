create or replace
FUNCTION Dividir_sem_round(	Dividendo_p		Number,
				Divisor_p		Number)
				RETURN number IS

Resultado_w		Number;

BEGIN
if	(nvl(divisor_p,0) = 0) then
	Resultado_w		:= 0;
else
	Resultado_w		:= Dividendo_p / divisor_p;
end if;
RETURN Resultado_w;
END Dividir_sem_round;
/