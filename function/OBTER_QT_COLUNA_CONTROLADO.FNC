create or replace
function Obter_Qt_Coluna_Controlado(
			ie_coluna_desejada_p		Number,
			ie_entrada_saida_p		Varchar2,
			ie_tipo_requisicao_p		Varchar2,
			ie_coluna_controlado_p		Number,
			cd_acao_p			Number,
			qt_estoque_p			Number) 
			return Number  deterministic is

qt_coluna_w		Number(15,4);
nr_coluna_w		Number(10,0);
qt_estoque_w		Number(15,4);

/* Coluna
	1 - Entrada
	2 - Saida
	3 - Perda
	4 - Transferência
*/

BEGIN

nr_coluna_w		:= 0;
qt_coluna_w		:= 0;
qt_estoque_w		:= nvl(qt_estoque_p,0);

if	(nvl(ie_coluna_controlado_p,0) in (1,2,3,4,11,12,13,14)) then
	nr_coluna_w		:= ie_coluna_controlado_p;
elsif	(ie_tipo_requisicao_p	in ('2','21')) then
	nr_coluna_w		:= 4;
elsif	(ie_entrada_saida_p 	= 'E') then
	nr_coluna_w		:= 1;
else			
	nr_coluna_w		:= 2;
end if;	

if	(cd_acao_p = '2') then
	begin

	if	(nr_coluna_w = 11) then
		nr_coluna_w	:= 2;
	elsif	(nr_coluna_w in (12,13,14)) then
		nr_coluna_w	:= 1;
	else
		qt_estoque_w		:= qt_estoque_w * -1;
	end if;

	end;
else	
	begin

	if	(nr_coluna_w = 11) then
		nr_coluna_w	:= 1;
	elsif	(nr_coluna_w = 12) then
		nr_coluna_w	:= 2;
	elsif	(nr_coluna_w = 13) then
		nr_coluna_w	:= 3;
	elsif	(nr_coluna_w = 14) then
		nr_coluna_w	:= 4;
	end if;

	end;
end if;

if	(nr_coluna_w = 4) and
	(ie_entrada_saida_p = 'E') then
	qt_estoque_w		:= qt_estoque_w * -1;
end if;

if	(ie_coluna_desejada_p = nr_coluna_w) then
	qt_coluna_w		:= qt_estoque_w;
end if;

return qt_coluna_w;

end;
/