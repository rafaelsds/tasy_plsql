create or replace
function tx_obter_ult_result_data_lab	
					(cd_pessoa_fisica_p	varchar2,
					seq_grid_p		number,
					nr_seq_exame_p		number) 
					return varchar2 is
nr_seq_resultado_w		number(10);
ds_resultado_w			varchar2(90)	:= '-';
nr_seq_resultado_prescr_w	number(10);
dt_referencia_w			date;			
begin

select 	dt_referencia
into	dt_referencia_w
from	TX_EXAME_GRID_CONTROLE
where 	nr_sequencia = seq_grid_p;

select 	nvl(max(c.nr_seq_resultado),0)
into	nr_seq_resultado_w
from	exame_laboratorio d,
	exame_lab_resultado a,
	exame_lab_result_item c
where	a.nr_seq_resultado 	= c.nr_seq_resultado
and	d.nr_seq_exame 		= c.nr_seq_exame
and	trunc(nvl(c.dt_digitacao,a.dt_resultado),'month') = trunc(dt_referencia_w,'month')
--and	c.dt_digitacao		<= dt_referencia_p
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
and	c.nr_seq_exame		= nr_seq_exame_p
and	(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado)) is not null;

select 	nvl(max(c.nr_seq_resultado),0)
into	nr_seq_resultado_prescr_w
from	exame_laboratorio d,
	exame_lab_resultado a,
	exame_lab_result_item c,
	prescr_medica b
where	a.nr_seq_resultado 	= c.nr_seq_resultado
and	d.nr_seq_exame 		= c.nr_seq_exame
and	trunc(nvl(c.dt_digitacao,a.dt_resultado),'month') = trunc(dt_referencia_w,'month')
and	b.cd_pessoa_fisica	= cd_pessoa_fisica_p
and	c.nr_seq_exame		= nr_seq_exame_p
and	b.nr_prescricao		= a.nr_prescricao
and	(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado)) is not null;

if	(nr_seq_resultado_prescr_w > nr_seq_resultado_w) then
	nr_seq_resultado_w	:= nr_seq_resultado_prescr_w;
end if;


if	(nr_seq_resultado_w > 0) then

	select	max(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado))
	into	ds_resultado_w
	from	exame_laboratorio d,
		exame_lab_result_item c
	where	c.nr_seq_resultado 	= nr_seq_resultado_w
	and	c.nr_seq_exame		= nr_seq_exame_p
	and	d.nr_seq_exame 		= c.nr_seq_exame;
	
end if;

return ds_resultado_w;

end tx_obter_ult_result_data_lab;
/