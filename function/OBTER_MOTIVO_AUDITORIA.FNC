Create or Replace 
FUNCTION	Obter_motivo_auditoria(nr_sequencia_p number)
			Return Varchar2 IS

ds_motivo_w		Varchar2(80);

BEGIN

begin
select	nvl(max(ds_motivo_auditoria),Wheb_mensagem_pck.get_texto(799244))
into	ds_motivo_w
from 	auditoria_motivo
where 	nr_sequencia = nr_sequencia_p;
exception
	when others then
		ds_motivo_w:= '';	
end;

RETURN ds_motivo_w;
END Obter_motivo_auditoria;
/