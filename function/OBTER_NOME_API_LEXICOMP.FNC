create or replace function obter_nome_api_lexicomp ( cd_api_p in alerta_api.cd_api%type ) 
										return varchar2 is

	ds_description_w varchar2(255 char) := '';

begin
	if (cd_api_p is not null) then
		case cd_api_p
			when 'A' then
				ds_description_w := obter_texto_tasy(1128085, null);
			when 'B' then
				ds_description_w := obter_texto_tasy(1132505, null);
			when 'C' then
				ds_description_w := obter_texto_tasy(1128087, null);
			when 'D' then
				ds_description_w := obter_texto_tasy(1132470, null);
			when 'E' then
				ds_description_w := obter_texto_tasy(1132517, null);
			when 'F' then
				ds_description_w := obter_texto_tasy(1132518, null);
			when 'G' then
				ds_description_w := obter_texto_tasy(1132519, null);
			when 'H' then
				ds_description_w := obter_texto_tasy(1132520, null);
			when 'I' then
				ds_description_w := obter_texto_tasy(1132521, null);
			when 'DDC' then
				ds_description_w := obter_texto_tasy(1132505, null);
			when 'PE' then
				ds_description_w := obter_texto_tasy(728068, null);
			else 
				ds_description_w := '';
		end case;
	end if;

	return ds_description_w;

end obter_nome_api_lexicomp;
/