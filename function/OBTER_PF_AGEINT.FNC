create or replace
function Obter_Pf_Ageint(
			nr_seq_ageint_p	Number)
 		    	return Varchar2 is

ds_retorno_w	Varchar2(10);
			
begin

select	max(cd_pessoa_fisica)
into	ds_retorno_w
from	agenda_integrada
where	nr_sequencia	= nr_seq_ageint_p;

return	ds_retorno_w;

end Obter_Pf_Ageint;
/