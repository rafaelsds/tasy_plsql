create or replace
function upc_obter_hora_entre_datas(dt_inicial_p		date,
				dt_final_p		date)
 		    	return number is
nr_horas_w		number(15,5);
nr_horas_inuteis_w	number(15,5):= 0;
nr_horas_uteis_w	number(15,5);
dt_aux_w		date;
dt_inicio_aux_w		date;
dt_fim_aux_w		date;
dt_final_w		date;
qt_w			number(15,5) := 0;
qt_horas_dif_w		number(15,4) := 0;
qt_minuto_w		number(15,4) := 0;
cont			number(10,0) := 0;
begin
dt_aux_w	:= dt_inicial_p;
qt_minuto_w	:= obter_min_entre_datas(dt_inicial_p,dt_final_p, 1);
for i in 1..qt_minuto_w loop
	begin
	cont := cont + 1;
	if	(substr(obter_se_dia_util(trunc(dt_aux_w + cont/1440), wheb_usuario_pck.get_cd_estabelecimento),1,2) = 'N') or
		(substr(obter_se_dia_util(trunc(dt_aux_w + cont/1440), wheb_usuario_pck.get_cd_estabelecimento),1,2) = 'S')and
		(dt_aux_w + cont/1440 not between to_date(to_char(dt_aux_w + cont/1440,'dd/mm/yyyy') || ' ' || '07:00:00', 'dd/mm/yyyy hh24:mi:ss') and
					to_date(to_char(dt_aux_w + cont/1440,'dd/mm/yyyy') || ' ' ||'18:59:59', 'dd/mm/yyyy hh24:mi:ss'))then
		qt_horas_dif_w	:= qt_horas_dif_w + 1;

	end if;

	end;
end loop;

nr_horas_w := qt_horas_dif_w / 60;
return	nvl(nr_horas_w,0);

end UPC_OBTER_HORA_ENTRE_DATAS;
/
