create or replace
function pls_obter_operadora_estab(	cd_estabelecimento_p	number)
				return number is

nr_sequencia_w	number(10);

begin

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	pls_outorgante 
where	cd_estabelecimento = cd_estabelecimento_p;

return	nr_sequencia_w;

end pls_obter_operadora_estab;
/