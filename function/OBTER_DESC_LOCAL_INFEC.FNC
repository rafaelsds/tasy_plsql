create or replace
function obter_desc_local_infec (nr_seq_local_infeccao_p number)
				return varchar2 is

ds_resultado_w			varchar2(255);

begin

if (nr_seq_local_infeccao_p is not null) then
	select 	substr(to_char(dt_infeccao,'dd/mm/yyyy hh24:mi:ss')||' - '||
	substr(obter_desc_topografia(cd_topografia),1,255)||' - '||
	substr(obter_desc_caso_infeccao(cd_caso_infeccao),1,255)||' - '||
	substr(obter_nome_clinica(cd_clinica),1,255),1,255)
	into ds_resultado_w
from	cih_local_infeccao
where	nr_sequencia = nr_seq_local_infeccao_p;
end if;

return ds_resultado_w;

end obter_desc_local_infec;
/