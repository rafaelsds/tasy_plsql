create or replace function obter_qtd_entubacao(nr_atendimento_p in atendimento_paciente.nr_atendimento%type)
   return number is
   qt_atendimento_w number;
begin
   select count(a.nr_atendimento)
     into qt_atendimento_w
     from ATEND_PAC_DISPOSITIVO a
    where a.nr_atendimento = nr_atendimento_p
    group by a.nr_atendimento;

   return(qt_atendimento_w);
   
end obter_qtd_entubacao;
/
