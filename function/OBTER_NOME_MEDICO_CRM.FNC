create or replace
function	obter_nome_medico_crm(
		nr_crm_p		varchar2) return varchar is

ds_retorno_w		varchar2(255);

begin

select	substr(max(nm_guerra),1,255)
into	ds_retorno_w
from	medico
where	nr_crm = nr_crm_p;

return	ds_retorno_w;

end obter_nome_medico_crm;
/