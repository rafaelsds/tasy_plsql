create or replace
function obter_desc_recurso_agenda (	nr_seq_recurso_p	number)
					return	varchar2 is
ds_retorno_w	varchar2(40);

begin

if	(nr_seq_recurso_p is not null) then
	select	max(ds_recurso)
	into	ds_retorno_w
	from	recurso_agenda
	where	nr_sequencia = nr_seq_recurso_p;
end if;

return ds_retorno_w;

end obter_desc_recurso_agenda;
/