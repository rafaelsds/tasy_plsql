create or replace
function obter_cor_forma_entrega ( nr_seq_forma_laudo_p	number)
 		    	return varchar2 is

ds_Retorno_w	varchar2(80);
			
begin

select	max(ds_cor)
into	ds_retorno_w
from	forma_entrega_laudo
where	nr_Sequencia	= nr_seq_forma_laudo_p;

return	ds_Retorno_w;

end obter_cor_forma_entrega;
/
