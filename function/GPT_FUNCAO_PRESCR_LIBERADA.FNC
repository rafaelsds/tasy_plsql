create or replace
function gpt_funcao_prescr_liberada ( cd_perfil_p		number,
				      ie_prescr_uausrio_p	number) return varchar2 is

				      
ie_possui_regra_w		varchar(1):='N';
ie_funcao_medico_lib_w	gpt_presentation_rule.ie_funcao_medico_lib%type;
ie_funcao_enf_lib_w		gpt_presentation_rule.ie_funcao_enf_lib%type;
ie_funcao_farm_lib_w	gpt_presentation_rule.ie_funcao_farm_lib%type;
ie_funcao_fisio_lib_w	gpt_presentation_rule.ie_funcao_fisio_lib%type;
ie_funcao_nutric_lib_w	gpt_presentation_rule.ie_funcao_nutric_lib%type;
ie_funcao_odont_lib_w	gpt_presentation_rule.ie_funcao_odont_lib%type;
ie_funcao_tec_enf_w	gpt_presentation_rule.ie_funcao_tec_enf%type;


										
begin

if	(cd_perfil_p is not null) then	
		select max(nvl('S','N'))
		into	ie_possui_regra_w
		from 	gpt_presentation_rule
		where 	((cd_perfil = cd_perfil_p) or (cd_perfil is null));
	
		if (nvl(ie_possui_regra_w,'N') = 'S') then				
			select	max(nvl(ie_funcao_medico_lib,'S')),
				max(nvl(ie_funcao_enf_lib,'S')),
				max(nvl(ie_funcao_farm_lib,'S')),
				max(nvl(ie_funcao_fisio_lib,'S')),
				max(nvl(ie_funcao_nutric_lib,'S')),
        max(nvl(ie_funcao_tec_enf, 'S')),
                max(nvl(ie_funcao_odont_lib,'S'))
			into	ie_funcao_medico_lib_w,
				ie_funcao_enf_lib_w,
				ie_funcao_farm_lib_w,
				ie_funcao_fisio_lib_w,
				ie_funcao_nutric_lib_w,
        ie_funcao_tec_enf_w,
                ie_funcao_odont_lib_w
			from 	gpt_presentation_rule
			where 	((cd_perfil = cd_perfil_p) or (cd_perfil is null));
				
			if	(ie_prescr_uausrio_p = 1) then --Medico
				return nvl(ie_funcao_medico_lib_w,'N');				
			elsif (ie_prescr_uausrio_p = 8) then -- farmaceutico 				
				return nvl(ie_funcao_farm_lib_w,'N');				
			elsif (ie_prescr_uausrio_p = 3) then -- enfermeiro 					
				return nvl(ie_funcao_enf_lib_w,'N');				
			elsif (ie_prescr_uausrio_p = 4) then -- nutricionista
				return nvl(ie_funcao_nutric_lib_w,'N');				
			elsif (ie_prescr_uausrio_p = 10) then -- fisioterapeuta
				return nvl(ie_funcao_fisio_lib_w,'N');	
            elsif (ie_prescr_uausrio_p = 12) then -- odontologista
				return nvl(ie_funcao_odont_lib_w,'N');
      elsif (ie_prescr_uausrio_p = 13) then
        return nvl(ie_funcao_tec_enf_w, 'N');
			else 
				return 'N';					
			end if;		
		end if;	
end if;										

return 	'S';
								
end gpt_funcao_prescr_liberada;
/
