create or replace
function roteiro_obter_data_referencia (dt_inicio_tratamento_p	date,
										dt_final_tratamento_p	date,
										dt_referencia_p      	date,
								        ie_referencia_p       	varchar2,
										ie_tipo_referencia_p	varchar2)
return varchar2 is

ds_retorno_w 	varchar2(1);
dt_final_tratamento_w date;

begin
ds_retorno_w := '';

dt_final_tratamento_w := nvl(dt_final_tratamento_p,'20/12/2099');

if (ie_tipo_referencia_p = 'I')	 then
	if (ie_referencia_p = 'M') and
	   (dt_inicio_tratamento_p <= (PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)+7)) and
	   (not (dt_final_tratamento_w < PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'A') and	      
	      (dt_inicio_tratamento_p <= fim_mes(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'year',0))) and
		  (not (dt_final_tratamento_w <  PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'year',0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'S') and
	      (dt_inicio_tratamento_p <=  fim_mes(dt_referencia_p)) and
		  (not (dt_final_tratamento_w < PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'T') and
	      (dt_inicio_tratamento_p <= fim_mes(dt_referencia_p)) and
		  (not (dt_final_tratamento_w <  PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'B')and
	      (dt_inicio_tratamento_p <= (PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)+7)) and
		  (not (dt_final_tratamento_w < PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0))) then
		ds_retorno_w := 'X';
		
	elsif (ie_referencia_p = 'SR')and
		  (not (dt_final_tratamento_w <= fim_mes(PKG_DATE_UTILS.start_of(TO_DATE(dt_inicio_tratamento_p),'year',0)))) then
		ds_retorno_w := 'X';
		
	end if;

elsif (ie_tipo_referencia_p = 'D') then

	if (ie_referencia_p = 'M') and
	   (dt_inicio_tratamento_p <= PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)+21)  and
	   (not (dt_final_tratamento_w < PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)+7))then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'A') and
	      (dt_inicio_tratamento_p <=  fim_mes(PKG_DATE_UTILS.ADD_MONTH((PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'year',0)),10,0))) and
		  (not (dt_final_tratamento_w <  PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'year',0),1,0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'S') and
	      (dt_inicio_tratamento_p <=  fim_mes(PKG_DATE_UTILS.ADD_MONTH((PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)),4,0))) and
		  (not (dt_final_tratamento_w <  PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0),1,0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'T') and
	      (dt_inicio_tratamento_p <=  fim_mes(PKG_DATE_UTILS.ADD_MONTH((PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)),1,0))) and
		  (not (dt_final_tratamento_w <  PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0),1,0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'B') and
	      (dt_final_tratamento_w < PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0) + 7) and
	      (dt_inicio_tratamento_p <=  PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0),2,0) + 21) then
		ds_retorno_w := 'X';
		
	elsif (ie_referencia_p = 'SR') and
		  (dt_final_tratamento_w >= fim_mes(PKG_DATE_UTILS.start_of(TO_DATE(dt_inicio_tratamento_p),'year',0))) and
		  (dt_final_tratamento_w >=	PKG_DATE_UTILS.start_of(sysdate-30, 'ddd', 0)) then
		ds_retorno_w := 'X';
		
	end if;
	
elsif (ie_tipo_referencia_p = 'F') then
	if (ie_referencia_p = 'M') and
	   (dt_inicio_tratamento_p <= fim_mes(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0))) and
	   (not (dt_final_tratamento_w < PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)+20)) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'A') and
	      (dt_inicio_tratamento_p <=  fim_mes(PKG_DATE_UTILS.ADD_MONTH((PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'year',0)),11,0))) and
		  (not (dt_final_tratamento_w < PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'year',0),11,0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'S') and
	      (dt_final_tratamento_w  <  PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0),5,0)) and
	      (dt_inicio_tratamento_p <=  fim_mes(PKG_DATE_UTILS.ADD_MONTH((PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)),5,0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'T') and
	      (dt_final_tratamento_w  <  PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0),2,0)) and
	      (dt_inicio_tratamento_p <=  fim_mes(PKG_DATE_UTILS.ADD_MONTH((PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0)),2,0))) then
		ds_retorno_w := 'X';

	elsif (ie_referencia_p = 'B')and
	      (dt_final_tratamento_w  <  PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0),2,0) + 20) and
	      (dt_inicio_tratamento_p <=  fim_mes(PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(TO_DATE(dt_referencia_p),'month',0),2,0))) then
		ds_retorno_w := 'X';
		
	elsif (ie_referencia_p = 'SR')and
	      (dt_final_tratamento_w >= sysdate) then
		ds_retorno_w := 'X';		
	end if;
end if;
return ds_retorno_w;

end roteiro_obter_data_referencia;
/