CREATE OR REPLACE
FUNCTION Obter_desc_hem_segmento_princ	(nr_sequencia_p		number)
					RETURN VARCHAR2 IS

ds_retorno_w	VARCHAR2(80);

BEGIN

if	(nr_sequencia_p is not null) then
	select	ds_segmento
	into	ds_retorno_w
	from	hem_segmento_princ
	where	nr_sequencia = nr_sequencia_p;
end if;

RETURN	ds_retorno_w;

END Obter_desc_hem_segmento_princ;
/
