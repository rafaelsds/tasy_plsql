Create or Replace
FUNCTION HCBCS_OBTER_PARCELA_TITULO(	nr_titulo_p	number)
						RETURN VarChar2 IS

ds_resultado_w		varchar2(255);
nr_interno_conta_w	number(10);
qt_total_w		number(10);
qt_parcela_w		number(10);

BEGIN

select	max(nr_interno_conta)
into	nr_interno_conta_w
from	titulo_receber
where	ie_situacao	= '1'
and	nr_titulo	= nr_titulo_p;

if	(nr_interno_conta_w is not null) then
	select	count(*)
	into	qt_total_w
	from	titulo_receber
	where	ie_situacao	= '1'
	and	nr_interno_conta	= nr_interno_conta_w;

	select	count(*)
	into	qt_parcela_w
	from	titulo_receber
	where	ie_situacao	= '1'
	and	nr_interno_conta	= nr_interno_conta_w
	and	nr_titulo		> nr_titulo_p;


	ds_resultado_w	:= to_char(qt_total_w - qt_parcela_w) || '/' || to_char(qt_total_w);
else
	ds_resultado_w	:= '1/1';
end if;

RETURN ds_resultado_w;

END HCBCS_OBTER_PARCELA_TITULO;
/