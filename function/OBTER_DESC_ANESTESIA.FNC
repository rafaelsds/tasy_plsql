create or replace
function obter_desc_anestesia(	nr_seq_tipo_anest_p		number)
				return varchar2 is

ds_retorno_w		varchar2(255);

begin
if (nr_seq_tipo_anest_p is not null) then
	select	substr(DS_ANESTESIA,1,255)
	into	ds_retorno_w
	from	cat_anestesia
	where	nr_sequencia	= nr_seq_tipo_anest_p;
end if;

return	ds_retorno_w;

end obter_desc_anestesia;
/