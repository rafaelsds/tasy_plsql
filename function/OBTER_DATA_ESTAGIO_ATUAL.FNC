create or replace
function Obter_data_estagio_atual(nr_seq_pendencia_p		Number)
 		    	return Date is

nr_seq_hist_w		Number(10,0);
dt_retorno_w		Date;
			
begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_hist_w
from 	cta_pendencia_hist
where 	nr_seq_pend = nr_seq_pendencia_p;

if	(nr_seq_hist_w > 0) then

	select 	dt_inicio_estagio
	into	dt_retorno_w
	from 	cta_pendencia_hist
	where 	nr_sequencia = nr_seq_hist_w;
	
else
	
	select 	dt_pendencia
	into	dt_retorno_w
	from 	cta_pendencia
	where 	nr_sequencia = nr_seq_pendencia_p;
	
end if;

return	dt_retorno_w;

end Obter_data_estagio_atual;
/