create or replace function obter_restricao_pepo(	nr_seq_pepo_p		number,
				nr_atendimento_pepo_p	number,
				cd_pessoa_fisica_pepo_p	varchar2,
				nr_cirurgia_p		number,
				nr_atendimento_p	number,
				cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

nr_seq_pepo_w	pepo_cirurgia.nr_sequencia%type;

begin

if	(nr_seq_pepo_p is not null) then

	if	(nr_cirurgia_p is not null) then

		select	max(nr_seq_pepo)
		into	nr_seq_pepo_w
		from	cirurgia
		where	nr_cirurgia = nr_cirurgia_p;

		if	(nr_seq_pepo_p = nr_seq_pepo_w) then
			return 'S';
		else
			return 'N';
		end if;

	elsif	(nr_atendimento_p is not null) then
		if	(nr_atendimento_pepo_p = nr_atendimento_p) then
			return 'S';
		else
			return 'N';
		end if;
	elsif	(cd_pessoa_fisica_p is not null) then
		if	(cd_pessoa_fisica_pepo_p = cd_pessoa_fisica_p) then
			return 'S';
		else
			return 'N';
		end if;
	else
		return 'N';
	end if;
else
	return 'N';
end if;

end obter_restricao_pepo;
/