create or replace
function cad_obter_escala_medico(	nr_seq_escala_p		number,
				nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin
select	substr(a.ds_escala,1,255)
into	ds_retorno_w
from	escala a,
		escala_diaria b,
		medico_plantao c
where	a.nr_sequencia = b.nr_seq_escala
and	b.nr_sequencia = c.nr_seq_escala_diaria
and	c.nr_seq_escala_diaria = nr_seq_escala_p
and	c.nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end cad_obter_escala_medico;
/