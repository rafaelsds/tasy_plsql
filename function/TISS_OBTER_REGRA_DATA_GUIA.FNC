create or replace
function TISS_OBTER_REGRA_DATA_GUIA
				(cd_estabelecimento_p	in number,
				cd_convenio_p		in number,
				ie_tiss_tipo_guia_p	in varchar2,
				ie_tipo_atend_tiss_p	in varchar2,
				ie_clinica_p		in number)
				return varchar2 is

ds_retorno_w	varchar2(255)	:= null;
ie_data_w	varchar2(255);

cursor	c01 is
select	ie_data
from 	tiss_regra_data_guia
where	cd_estabelecimento					= cd_estabelecimento_p
and 	nvl(cd_convenio,nvl(cd_convenio_p,0))			= nvl(cd_convenio_p,0)
and	nvl(ie_clinica,nvl(ie_clinica_p,0))			= nvl(ie_clinica_p,0)
and 	((ie_tipo_data = 'EC' and ie_tiss_tipo_guia_p	= '3') or
	 (ie_tipo_data = 'ESADT' and ie_tiss_tipo_guia_p = '4'))
and	nvl(ie_tipo_atend_tiss,nvl(ie_tipo_atend_tiss_p,'X'))	= nvl(ie_tipo_atend_tiss_p,'X')
order by nvl(cd_convenio,0),
	nvl(ie_tipo_atend_tiss,'X'),
	nvl(ie_clinica,0);	

begin

if	(ie_tiss_tipo_guia_p is not null) then
	open c01;
	loop
	fetch c01 into
		ie_data_w;
	exit when c01%notfound;
		ds_retorno_w	:= ie_data_w;
	end loop;
	close c01;		

end if;

return ds_retorno_w;

end TISS_OBTER_REGRA_DATA_GUIA;
/