create or replace
function nut_obter_cardapio_padrao(	nr_atendimento_p	Number,
					nr_seq_servico_p	Number,
					nr_seq_servico_dia_p	Number,
					dt_referencia_p		Date)
 		    	return Number is

nr_prescr_oral_w	number(14);
nr_seq_cardapio_w	number(10);
ds_parametro_120_w  	varchar2(1);
				
begin
if	((nvl(nr_seq_servico_dia_p,0) > 0) and (nvl(nr_atendimento_p,0) > 0)) then
	
	ds_parametro_120_w := obter_valor_param_usuario(1003,120,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);
						
	select	max(nr_prescr_oral)
	into	nr_prescr_oral_w
	from	nut_atend_serv_dia_rep b,
		nut_atend_serv_dia c
	where	b.nr_seq_serv_dia   	= c.nr_sequencia	
	and	c.nr_atendimento 	= nr_atendimento_p
	and	c.nr_sequencia		= nr_seq_servico_dia_p
	and	c.dt_servico between inicio_dia(dt_referencia_p) and fim_dia(dt_referencia_p);

	begin
		select	DISTINCT(a.nr_sequencia)
		into	nr_seq_cardapio_w
		from	nut_cardapio_dia a,
			nut_local_refeicao c,
			prescr_dieta b
		where	a.nr_seq_local = c.nr_sequencia
		and	b.nr_prescricao = nr_prescr_oral_w
		and	c.ie_local_paciente = 'S'
		and 	(c.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento or c.cd_estabelecimento is null)
		and	((a.cd_dieta = b.cd_dieta ) or exists(	select	1
								from	nut_grupo_producao x,
									nut_grupo_producao_dieta k
								where	k.nr_seq_grupo_producao	= x.nr_sequencia
								and	x.nr_sequencia 		= a.nr_seq_grupo_producao
								and	k.cd_dieta = b.cd_dieta))
		and	a.nr_seq_servico = nr_seq_servico_p
		and	nvl(a.ie_cardapio_padrao,'N') = 'S'
		and	((ds_parametro_120_w = 'N' and (dt_referencia_p between a.dt_vigencia_inicial and a.dt_vigencia_final) and obter_se_cardapio_dia(dt_referencia_p, a.ie_semana, a.ie_dia_semana) = 'S')
    or(ds_parametro_120_w = 'S' and nvl(a.nr_seq_cycle,0) > 0 and get_cycle_day(dt_referencia_p, a.nr_seq_cycle) = a.nr_seq_cycle_day));
	
	exception
	when others then
		nr_seq_cardapio_w := null;
	end;
end if;
       
return	nr_seq_cardapio_w;

end nut_obter_cardapio_padrao;
/
