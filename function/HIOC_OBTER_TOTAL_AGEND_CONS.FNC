create or replace
function hioc_obter_total_agend_cons(	cd_agenda_p		NUMBER,
					dt_agenda_P		DATE,
					ie_opcao_p		VARCHAR2)
					RETURN NUMBER IS

qt_retorno_w	NUMBER(10,0);

BEGIN
/*CATE 365 - HIOC - Agenda de Cons. por Classif.(Classif. Consulta) */
if	(ie_opcao_p = 'A') then
	SELECT	COUNT(*)
	INTO	qt_retorno_w
	FROM	agenda_consulta a
	WHERE	a.cd_agenda = cd_agenda_p
	AND	a.dt_agenda BETWEEN TRUNC(dt_agenda_p) AND TRUNC(dt_agenda_p) + 86399/86400
	AND	a.ie_status_agenda not in ('L', 'C', 'I', 'F') 
	AND	a.ie_status_agenda = 'E'
	AND	a.ie_classif_agenda IN ('N73','N62','N1','N2','56','N9','N3','N','N75','N12');
elsif (ie_opcao_p = 'T') then
	SELECT	COUNT(*)
	INTO	qt_retorno_w
	FROM	agenda_consulta a
	WHERE	a.dt_agenda BETWEEN TRUNC(dt_agenda_p) AND TRUNC(dt_agenda_p) + 86399/86400
	AND	((a.cd_agenda = cd_agenda_p) OR (cd_agenda_p = '0'))
	AND	a.ie_status_agenda not in ('L', 'C', 'I', 'F')
	AND	a.ie_status_agenda = 'E'
	AND	a.ie_classif_agenda IN ('N73','N62','N1','N2','56','N9','N3','N','N75','N12');
elsif (ie_opcao_P = 'X') then
	SELECT	COUNT(*)
	INTO	qt_retorno_w
	FROM	agenda_consulta a
	WHERE	a.dt_agenda BETWEEN TRUNC(dt_agenda_p,'mm') AND last_day(dt_agenda_p) + 86399/86400
	AND	((a.cd_agenda = cd_agenda_p) OR (cd_agenda_p = '0'))
	AND	a.ie_status_agenda not in ('L', 'C', 'I', 'F')
	AND	a.ie_status_agenda = 'E'
	AND	a.ie_classif_agenda IN ('N73','N62','N1','N2','56','N9','N3','N','N75','N12');
end if;
return	qt_retorno_w;

end hioc_obter_total_agend_cons;
/