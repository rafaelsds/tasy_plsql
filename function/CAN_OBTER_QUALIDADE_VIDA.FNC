create or replace
function Can_obter_qualidade_vida(nr_sequencia_p		number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);

begin
if (nr_sequencia_p is not null) then

	select	max(ds_qualidade)
	into	ds_retorno_w
	from   	can_qualidade_vida
	where	nr_sequencia = nr_sequencia_p;
	
end if;

return	ds_retorno_w;

end Can_obter_qualidade_vida;
/