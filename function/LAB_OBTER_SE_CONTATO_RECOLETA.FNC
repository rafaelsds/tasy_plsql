create or replace
function lab_obter_se_contato_recoleta (nr_prescricao_p number)
        return varchar2 is
ds_retorno_w varchar2(1);
begin
 select decode(count(*),0,'N','S')
 into ds_retorno_w
 from prescr_contato_recoleta
 where nr_prescricao = nr_prescricao_p;
return ds_retorno_w;
end lab_obter_se_contato_recoleta;
/
