create or replace 
function obter_se_fluxo_caixa_regra
			(	nr_seq_regra_p			number,
				dt_referencia_p			date,
				cd_estabelecimento_p		number,
				ie_opcao_p			varchar2)
			 return varchar2 is

/*ie_opcao_p
VL - Valor de dedu��o		- Retorna o Valor
E - Se exclui ou n�o a regra	- Retorna "S" ou "N" */

ds_retorno_w			varchar2(255);
ie_acao_w			varchar2(255);
ie_regra_exclusao_w		varchar2(100);
ie_origem_tit_pagar_w		varchar2(100);
ie_origem_tit_rec_w		varchar2(100);
cd_pessoa_fisica_w		varchar2(100);
ie_regra_data_w			varchar2(100);
cd_cgc_w			varchar2(100);
ie_somente_dia_util_w		varchar2(1);
vl_minimo_w			number(15,2);
vl_maximo_w			number(15,2);
vl_saldo_total_w		number(15,2);
vl_saldo_titulo_w		number(15,2);
nr_seq_classe_tit_pagar_w	number(15);
qt_dia_inicial_w		number(10,0);
qt_dia_final_w			number(10,0);
cont_w				number(10,0);
dt_referencia_w			date;
vl_fixo_w			number(15,2);
ie_regra_origem_w		varchar2(3);
cd_conta_financ_w		number(10);
dt_inicial_w			date;
dt_final_w			date;

cursor c01 is
select	ie_regra_exclusao,
	ie_origem_tit_pagar,
	ie_origem_tit_rec,
	cd_pessoa_fisica,
	cd_cgc,
	qt_dia_inicial,
	qt_dia_final,
	ie_regra_data,
	vl_minimo,
	vl_maximo,
	ie_acao,
	nr_seq_classe_tit_pagar,
	ie_somente_dia_util
from	regra_fluxo_caixa_exc
where	nr_seq_regra		= nr_seq_regra_p;

begin

ds_retorno_w		:= 'S';
vl_saldo_total_w	:= 0;

select	max(a.vl_fixo),
	max(a.ie_regra_origem),
	max(a.cd_conta_financ)
into	vl_fixo_w,
	ie_regra_origem_w,
	cd_conta_financ_w
from	regra_fluxo_caixa a
where	a.nr_sequencia	= nr_seq_regra_p;

if	(ie_somente_dia_util_w = 'S') then
	dt_referencia_w	:= obter_proximo_dia_util(cd_estabelecimento_p, dt_referencia_p);
else
	dt_referencia_w	:= dt_referencia_p;
end if;

open C01;
loop
fetch C01 into
	ie_regra_exclusao_w,
	ie_origem_tit_pagar_w,
	ie_origem_tit_rec_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	qt_dia_inicial_w,
	qt_dia_final_w,
	ie_regra_data_w,
	vl_minimo_w,
	vl_maximo_w,
	ie_acao_w,
	nr_seq_classe_tit_pagar_w,
	ie_somente_dia_util_w;
exit when c01%notfound or ds_retorno_w = 'N';
	
	dt_inicial_w := obter_data_valida(qt_dia_inicial_w, dt_referencia_w);
	dt_final_w := obter_data_valida(qt_dia_final_w, dt_referencia_w);

	if	(ie_regra_exclusao_w = 'TP') then
		
		select	count(*),
			nvl(sum(a.vl_saldo_titulo),0)
		into	cont_w,
			vl_saldo_titulo_w
		from	titulo_pagar a
		where	a.cd_estabelecimento	= cd_estabelecimento_p
		and	a.ie_origem_titulo		= nvl(ie_origem_tit_pagar_w, a.ie_origem_titulo)
		and	nvl(a.cd_pessoa_fisica, 'X')	= nvl(cd_pessoa_fisica_w, nvl(a.cd_pessoa_fisica, 'X'))
		and	nvl(a.cd_cgc, 'X') 		= nvl(cd_cgc_w, nvl(a.cd_cgc, 'X'))
		and	a.vl_titulo			between nvl(vl_minimo_w, a.vl_titulo) and nvl(vl_maximo_w, a.vl_titulo)
		and	nvl(a.nr_seq_classe,0)	= nvl(nr_seq_classe_tit_pagar_w, nvl(a.nr_seq_classe, 0))
		and	exists (select 	1
				from 	titulo_pagar_classif x
				where 	x.nr_titulo = a.nr_titulo
				and	x.nr_seq_conta_financ = cd_conta_financ_w)
		and	(((ie_regra_data_w	= 'E') and (a.dt_emissao between dt_inicial_w and dt_final_w))
			or ((ie_regra_data_w	= 'V') and (a.dt_vencimento_atual	between dt_inicial_w and dt_final_w))
			);
		
	elsif	(ie_regra_exclusao_w = 'TR') then
		
		select	count(*),
			nvl(sum(a.vl_saldo_titulo),0)
		into	cont_w,
			vl_saldo_titulo_w
		from	titulo_receber a
		where	a.cd_estabelecimento	= cd_estabelecimento_p
		and	a.ie_origem_titulo	= nvl(ie_origem_tit_rec_w, a.ie_origem_titulo)
		and	nvl(a.cd_pessoa_fisica, 'X') = nvl(cd_pessoa_fisica_w, nvl(a.cd_pessoa_fisica, 'X'))
		and	nvl(a.cd_cgc, 'X') 	= nvl(cd_cgc_w, nvl(a.cd_cgc, 'X'))
		and	a.vl_titulo		between nvl(vl_minimo_w, a.vl_titulo) and nvl(vl_maximo_w, a.vl_titulo)
		and	exists (select 	1
				from 	titulo_receber_classif x
				where 	x.nr_titulo = a.nr_titulo
				and	x.cd_conta_financ = cd_conta_financ_w)
		and	(((ie_regra_data_w	= 'E') and (a.dt_emissao between dt_inicial_w and dt_final_w))
			or ((ie_regra_data_w	= 'V') and (a.dt_pagamento_previsto between dt_inicial_w and dt_final_w))
			);
		
	end if;

	vl_saldo_total_w	:= vl_saldo_total_w + vl_saldo_titulo_w;

	if	(ie_acao_w		= 'M') and
		(ie_regra_origem_w	= 'VF') and
		(vl_saldo_total_w	> vl_fixo_w) then

		vl_saldo_total_w	:= vl_fixo_w;

	end if;
	
	if	(ie_opcao_p = 'E') and 
		(ie_acao_w = 'E') then
		ds_retorno_w		:= 'S';
		if	(cont_w > 0) then
			ds_retorno_w	:= 'N';
		end if;
	end if;
end loop;
close C01;

if	(ie_opcao_p = 'VL') then
	ds_retorno_w	:= vl_saldo_total_w;
end if;

return	ds_retorno_w;

end obter_se_fluxo_caixa_regra;
/