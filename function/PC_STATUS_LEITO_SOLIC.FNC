create or replace
function pc_status_leito_solic(nr_atendimento_p	number)
 		    	return varchar2 is
ds_retorno_w	varchar2(255);
nr_sequencia_w	gestao_vaga.nr_sequencia%type;
begin
if (nvl(nr_atendimento_p,0) > 0) then
	
	begin
	        select	max(nr_sequencia)
		into	nr_sequencia_w
		from 	gestao_vaga
		where	nr_atendimento = nr_atendimento_p
		and	obter_classif_setor(cd_setor_desejado) in(3,4)
		and	cd_setor_desejado is not null
		and	cd_unidade_basica is not null
		and 	ie_status <> 'F'; 
		
	if (nvl(nr_sequencia_w,0) > 0) then 
        	select	max(obter_valor_dominio(82,b.ie_status_unidade))
		into	ds_retorno_w
		from 	gestao_vaga a,
			unidade_atendimento b
		where	a.nr_sequencia = nr_sequencia_w
		and	a.cd_setor_desejado = b.cd_setor_atendimento
		and	a.cd_unidade_basica = b.cd_unidade_basica
		and	a.cd_unidade_compl    = b.cd_unidade_compl; 
	end if;	
	exception
	when others then
		ds_retorno_w := null;
	end;
	
end if;
return	ds_retorno_w;

end pc_status_leito_solic;
/