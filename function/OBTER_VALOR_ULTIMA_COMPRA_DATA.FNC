create or replace
function Obter_Valor_Ultima_Compra_data(
			cd_estabelecimento_p		Number,
			qt_dia_p				Number,
			cd_material_p			number,
			cd_local_estoque_p		Number,
			ie_nota_estoque_p			Varchar2,
			dt_referencia_p			date)
			return number is

vl_retorno_w		number(15,3);
nr_sequencia_w		Number(10,0);

BEGIN

vl_retorno_w		:= 0;

if	(ie_nota_estoque_p = 'N') then
	select	
		max(a.nr_sequencia)
	into	nr_sequencia_w
	from	natureza_operacao o,
		operacao_nota p,
		nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_sequencia		= b.nr_sequencia
	and	b.cd_natureza_operacao	= o.cd_natureza_operacao
	and	o.ie_entrada_saida		= 'E'
	and	b.ie_situacao		= 1
	and	cd_material  		= cd_material_p
	and	b.cd_operacao_nf = p.cd_operacao_nf
	and	nvl(p.ie_ultima_compra, 'S') = 'S'	
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.dt_atualizacao	between (dt_referencia_p - nvl(qt_dia_p,90)) and dt_referencia_p
	and	((cd_local_estoque_p is null) or (cd_local_estoque = cd_local_estoque_p))
	and	exists (
		select 1
		from	nota_fiscal b
		where	a.nr_sequencia	= b.nr_sequencia
		and	b.ie_situacao	= '1');
else
	select	
		max(a.nr_sequencia)
	into	nr_sequencia_w
	from	natureza_operacao o,
		operacao_nota p,
		nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_sequencia		= b.nr_sequencia
	and	b.cd_natureza_operacao	= o.cd_natureza_operacao
	and	o.ie_entrada_saida		= 'E'
	and	b.ie_situacao		= 1
	and	cd_material_estoque	= cd_material_p
	and	b.cd_operacao_nf = p.cd_operacao_nf
	and	nvl(p.ie_ultima_compra, 'S') = 'S'
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.dt_atualizacao between (dt_referencia_p - nvl(qt_dia_p,90)) and dt_referencia_p
	and	((cd_local_estoque_p is null) or (cd_local_estoque = cd_local_estoque_p))
	and	exists (
		select 1 
		from	nota_fiscal b
		where	a.nr_sequencia	= b.nr_sequencia
		and	b.ie_situacao	= '1');
end if;

if	(nr_sequencia_w > 0) then
	select	nvl(max(dividir((a.vl_total_item_nf - a.vl_desconto -
			vl_desconto_rateio + vl_frete + a.vl_despesa_acessoria + a.vl_seguro),
		a.qt_item_estoque)),0)
	into	vl_retorno_w
	from 	nota_fiscal_item a
	where	nr_sequencia 		= nr_sequencia_w
	and	(((ie_nota_estoque_p = 'N') and (cd_material = cd_material_p)) or
		((ie_nota_estoque_p <> 'N') and (cd_material_estoque = cd_material_p)));
end if;

return	vl_retorno_w;

END Obter_Valor_Ultima_Compra_data;
/