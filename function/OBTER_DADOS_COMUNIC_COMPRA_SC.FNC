create or replace
function obter_dados_comunic_compra_sc(	nr_solic_compra_p			number,
					cd_evento_p			number,
					nr_seq_regra_evento_p		number,
					ie_tipo_retorno_p			Varchar2)
return 			varchar2 is 

nm_usuario_w			varchar2(15) := '';
ds_titulo_w			varchar2(80) := '';
ds_comunicacao_w			varchar2(2000) := '';
qt_regra_comunic_w		number(6);
ds_item_ww			varchar2(2000);
ds_item_w			varchar2(2000);
ds_observacao_w			solic_compra.ds_observacao%type;
cd_estabelecimento_w		number(5);
dt_entrega_w			varchar2(2000);
nm_pessoa_solic_w			varchar2(100);

cursor c01 is
select	substr(a.cd_material || ' - ' || obter_desc_material(a.cd_material),1,2000) ds_material
from	solic_compra_item a,
	estrutura_material_v e
where	a.cd_material = e.cd_material
and	a.nr_solic_compra = nr_solic_compra_p;

begin

select	b.cd_estabelecimento,
	substr(obter_nome_pf_pj(b.cd_pessoa_solicitante,null),1,100) nm_pessoa_solic,
	b.nm_usuario,
	b.ds_observacao
into	cd_estabelecimento_w,
	nm_pessoa_solic_w,
	nm_usuario_w,
	ds_observacao_w
from	solic_compra b
where	b.nr_solic_compra = nr_solic_compra_p;

select	count(*)
into	qt_regra_comunic_w
from	regra_envio_comunic_compra a,
	regra_envio_comunic_evento b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_funcao = 267
and	b.cd_evento = cd_evento_p
and	b.ie_situacao = 'A'
and	a.cd_estabelecimento = cd_estabelecimento_w;
	
open c01;
loop
fetch c01 into
	ds_item_w;
exit when c01%notfound;
	ds_item_ww	:= substr(ds_item_ww || ds_item_w || chr(13),1,2000);
end loop;
close c01;

select	max(b.ds_titulo),
	max(b.ds_comunicacao) ds_comunicacao
into	ds_titulo_w,
	ds_comunicacao_w
from	regra_envio_comunic_compra a,
	regra_envio_comunic_evento b
where	a.nr_sequencia = b.nr_seq_regra
and	b.cd_evento = cd_evento_p
and	a.cd_estabelecimento = cd_estabelecimento_w
and	b.nr_sequencia = nr_seq_regra_evento_p;

ds_comunicacao_w := substr(replace_macro(ds_comunicacao_w, '@usuario', nm_usuario_w), 1, 2000);
ds_comunicacao_w := substr(replace_macro(ds_comunicacao_w, '@nr_solic_compra',nr_solic_compra_p), 1, 2000);
ds_comunicacao_w := substr(replace_macro(ds_comunicacao_w, '@nm_solicitante', nm_pessoa_solic_w), 1, 2000);
ds_comunicacao_w := substr(replace_macro(ds_comunicacao_w, '@data', sysdate), 1, 2000);
ds_comunicacao_w := substr(replace_macro(ds_comunicacao_w, '@itens', ds_item_ww), 1, 2000);
ds_comunicacao_w := substr(replace_macro(ds_comunicacao_w, '@motivo', ''), 1, 2000);
ds_comunicacao_w := substr(replace_macro(ds_comunicacao_w, '@observacao', ds_observacao_w), 1, 2000);

if	(ie_tipo_retorno_p = 'M') then
	return ds_comunicacao_w;
elsif	(ie_tipo_retorno_p = 'T') then
	return ds_titulo_w;
end if;

end obter_dados_comunic_compra_sc;
/