create or replace
function obter_vl_fat_cli( nr_seq_cliente_p		number,
						dt_mes_ref_p			date)
 		    	return number is

vl_nota_w	number(17,4);
				
				
begin

select	sum(nvl(vl_nota,0))
into	vl_nota_w
from	(
select	sum(d.vl_atual) vl_nota
from	com_cliente b,
		com_cli_neg_lic c,
		com_cli_neg_lic_item d,
		com_modalidade_lic e
where	b.nr_sequencia = c.nr_seq_cliente
and		c.nr_sequencia = d.NR_SEQ_NEG_LIC
and		d.NR_SEQ_MOD_LICENC = e.nr_sequencia
and		e.ie_modalidade = 'CDU'
and		exists (select 1 from com_cliente_gestor a where a.nr_seq_cliente = b.nr_sequencia)
and		trunc(c.dt_assinatura,'yyyy') = trunc(dt_mes_ref_p,'yyyy')
and		trunc(d.DT_INICIO_VENC_MANUT,'month') <= (trunc(dt_mes_ref_p,'month'))
and		b.nr_sequencia = nr_Seq_cliente_p
union all
select	sum(d.vl_parcela) vl_nota
from	com_cliente b,
		com_cli_neg_lic c,
		com_cli_neg_lic_parc d
where	b.nr_sequencia = c.nr_seq_cliente
and		c.nr_sequencia = d.NR_SEQ_NEGOC_LIC
and		d.ie_origem_valor = '1'
and		trunc(c.dt_assinatura,'yyyy') = trunc(dt_mes_ref_p,'yyyy')
and		trunc(d.DT_vencimento,'month') = (trunc(dt_mes_ref_p,'month'))
and		exists (select 1 from com_cliente_gestor a where a.nr_seq_cliente = b.nr_sequencia)
and		b.nr_sequencia = nr_Seq_cliente_p
union all /* Valores de lut */
select	sum(d.vl_atual) vl_nota
from	com_cliente b,
		com_cli_neg_lic c,
		com_cli_neg_lic_item d,
		com_modalidade_lic e
where	b.nr_sequencia = c.nr_seq_cliente
and		c.nr_sequencia = d.NR_SEQ_NEG_LIC
and		d.NR_SEQ_MOD_LICENC = e.nr_sequencia
and		e.ie_modalidade = 'LUT'
and		exists (select 1 from com_cliente_gestor a where a.nr_seq_cliente = b.nr_sequencia)
and		trunc(c.dt_assinatura,'yyyy') = trunc(dt_mes_ref_p,'yyyy')
and		trunc(d.DT_INICIO_VENC,'month') <= (trunc(dt_mes_ref_p,'month'))
and		b.nr_sequencia = nr_Seq_cliente_p);

return	vl_nota_w;

end obter_vl_fat_cli;
/