create or replace
function EHR_Obter_Elemento_Visual(	nr_seq_elemento_p	Number)
				return varchar2 is

ds_retorno_w	varchar2(80);	
begin

if	(nr_seq_elemento_p > 0) then
	begin
	select	obter_desc_expressao(CD_EXP_ELEMENTO, nm_elemento)
	into	ds_retorno_w
	from	ehr_elemento_visual
	where	nr_sequencia	= nr_seq_elemento_p;
	end;
end if;


return	ds_retorno_w;

end EHR_Obter_Elemento_Visual;
/