create or replace
function obter_motivo_cancel_agenda2	(nr_seq_agenda_p	number)
						return varchar2 is

cd_motivo_w		agenda_paciente.cd_motivo_cancelamento%type;
ds_retorno_w	varchar2(255);

begin
if	(nr_seq_agenda_p is not null) then

	select	nvl(max(cd_motivo_cancelamento),'N')
	into	cd_motivo_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;

	if	(cd_motivo_w <> 'N') then
	
		select	max(ds_motivo)
		into	ds_retorno_w
		from	agenda_motivo_cancelamento
		where	cd_motivo = cd_motivo_w;
	end if;
end if;

return ds_retorno_w;

end obter_motivo_cancel_agenda2;
/
