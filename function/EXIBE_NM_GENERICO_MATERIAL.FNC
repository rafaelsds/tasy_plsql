create or replace
function	exibe_nm_generico_material(	cd_material_p	number, 
				nm_usuario_p	varchar2)	return	varchar2  is

ds_material	material.ds_material%type;
param		varchar2(2);

begin

select	nvl(max(decode(vl_parametro,'S','S','N')), 'N')
into	param 
from	funcao_param_usuario
where	cd_funcao = 42 
and	nm_usuario_param = nm_usuario_p 
and	nr_sequencia = 17;
	
if	(param = 'S')then
	ds_material := obter_desc_material(obter_material_generico(cd_material_p)); 	
else 
	ds_material := obter_desc_material(cd_material_p);
end if;
  
return ds_material;

end exibe_nm_generico_material;
/