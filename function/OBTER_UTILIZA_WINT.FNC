create or replace FUNCTION OBTER_UTILIZA_WINT(cd_convenio_p	number,
					cd_estabelecimento_p	number)
					return Varchar2 is

ie_utiliza_wint_w	varchar2(1):='N';

BEGIN

begin
	select	ie_utiliza_wint
	into	ie_utiliza_wint_w
	from 	CONVENIO_ESTABELECIMENTO
	where cd_convenio = cd_convenio_p
    and cd_estabelecimento = cd_estabelecimento_p;
end;

RETURN	ie_utiliza_wint_w;

END	OBTER_UTILIZA_WINT;
/
