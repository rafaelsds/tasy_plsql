create or replace
function custo_medio_material_meses(	cd_estabelecimento_p		number,
						qt_meses_p		number,
						cd_material_p		number) return number is

vl_custo_medio_w		number(15,4);
vl_custo_w			number(15,4);
vl_custo_soma_w			number(15,4);
dt_mesano_referencia_w		date;
qt_divide_w			number(3);

cursor C01 is
	select	max(vl_custo_medio),
		dt_mesano_referencia
	from	saldo_estoque
	where	cd_estabelecimento = cd_estabelecimento_p
	and	dt_mesano_referencia between pkg_date_utils.add_month(pkg_date_utils.start_of(sysdate,'MONTH',0), - qt_meses_p,0) and pkg_date_utils.start_of(sysdate,'MONTH',0)
	and	cd_material = cd_material_p
	group by dt_mesano_referencia;

begin
vl_custo_soma_w		:= 0;
qt_divide_w		:= 0;

open c01;
loop
fetch c01 into
	vl_custo_w,
	dt_mesano_referencia_w;
exit when c01%notfound;
	begin
	vl_custo_soma_w	:= vl_custo_soma_w + vl_custo_w;
	qt_divide_w	:= qt_divide_w + 1;
	end;
end loop;
close c01;

if	(vl_custo_soma_w > 0) and
	(qt_divide_w > 0) then
	vl_custo_medio_w	:= dividir(vl_custo_soma_w, qt_divide_w);
end if;

return	vl_custo_medio_w;

end custo_medio_material_meses;
/