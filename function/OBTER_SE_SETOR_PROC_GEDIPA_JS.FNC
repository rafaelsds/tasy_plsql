create or replace
function obter_se_setor_proc_gedipa_js (nr_atendimento_p	number)
					return varchar2 is
					
ie_setor_processo_gedipa_w	varchar2(1) := 'N';
					
begin
if	(nr_atendimento_p is not null) then

	ie_setor_processo_gedipa_w := obter_se_setor_processo_gedipa(obter_unidade_atendimento(nr_atendimento_p,'IAA','CS'));

end if;

return ie_setor_processo_gedipa_w;

end obter_se_setor_proc_gedipa_js;
/