create or replace
function obter_prof_conta_checkup(nr_seq_checkup_etapa_p		number)
 		    	return varchar2 is

nm_medico_executor_w	varchar(255);			
begin

select max(substr(obter_nome_pf(b.cd_pessoa_fisica),1,255))
into   nm_medico_executor_w
from   procedimento_paciente a,
       pessoa_fisica b
where  a.nr_seq_checkup_etapa = nr_seq_checkup_etapa_p
and    a.cd_medico_executor = b.cd_pessoa_fisica;

if	(nm_medico_executor_w	is null) then
	select max(substr(obter_nome_pf(b.cd_pessoa_fisica),1,255))
	into   nm_medico_executor_w
	from   procedimento_paciente a,
	       pessoa_fisica b
	where  a.nr_seq_checkup_etapa = nr_seq_checkup_etapa_p
	and    a.CD_PESSOA_FISICA = b.cd_pessoa_fisica;
end if;	


return	nm_medico_executor_w;

end obter_prof_conta_checkup;
/
