create or replace
function obter_se_material_lado	(cd_material_p	number)
				return varchar2 is
				
ie_lado_w	varchar2(15);
					
begin
if	(cd_material_p is not null) then

	select	max(ie_exige_lado)
	into	ie_lado_w
	from	material
	where	cd_material = cd_material_p;

end if;

return ie_lado_w;

end obter_se_material_lado;
/