create or replace
function hsl_qtd_unid_higienizada_turno(dt_inicial_p		date,
					cd_estabelecimento_p	number,
					nr_seq_escala_p		number,
					ie_opcao_p		varchar2)
 		    	return number is

qt_retorno_w	number(10,0);
			
begin

/* Opções
	THM - Total de higienizações manhã
	THT - Total de higienizações tarde
	THN - Total de higienizações noite
	THD - Total de higienizações diário
	TCE - Total de check-list efetuado
	TI    - Total de Inadequações
	THH - Total horas higienização
	
*/

if	(ie_opcao_p = 'THM') then
	
	select	count(*)
	into	qt_retorno_w
	from    unidade_atendimento a,
		sl_unid_atend b
	where   b.nr_seq_unidade = a.nr_seq_interno
	and     b.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(b.dt_inicio) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(b.dt_fim) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	to_char(b.dt_fim,'hh24:mi:ss') between '07:00:00' and '11:59:59'
	and	b.cd_executor_fim_serv  in (select	a.cd_pessoa_fisica
				            from 	escala_diaria_adic a,
							escala_diaria b
					    where	a.nr_seq_escala_diaria = b.nr_sequencia
					    and		b.nr_seq_escala = nr_seq_escala_p
					    and		trunc(b.dt_inicio) = dt_inicial_p);	

elsif	(ie_opcao_p = 'THT') then
	
	select	count(*)
	into	qt_retorno_w
	from    unidade_atendimento a,
		sl_unid_atend b
	where   b.nr_seq_unidade = a.nr_seq_interno
	and     b.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(b.dt_inicio) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(b.dt_fim) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	to_char(b.dt_fim,'hh24:mi:ss') between '12:00:00' and '17:59:59'
	and	b.cd_executor_fim_serv  in (select	a.cd_pessoa_fisica
				            from 	escala_diaria_adic a,
							escala_diaria b
					    where	a.nr_seq_escala_diaria = b.nr_sequencia
					    and		b.nr_seq_escala = nr_seq_escala_p
					    and		trunc(b.dt_inicio) = dt_inicial_p);	

elsif	(ie_opcao_p = 'THN') then
	
	select	count(*)
	into	qt_retorno_w
	from    unidade_atendimento a,
		sl_unid_atend b
	where   b.nr_seq_unidade = a.nr_seq_interno
	and     b.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(b.dt_inicio) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(b.dt_fim) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	to_char(b.dt_fim,'hh24:mi:ss') between '18:00:00' and '06:59:59'
	and	b.cd_executor_fim_serv  in (select	a.cd_pessoa_fisica
				            from 	escala_diaria_adic a,
							escala_diaria b
					    where	a.nr_seq_escala_diaria = b.nr_sequencia
					    and		b.nr_seq_escala = nr_seq_escala_p
					    and		trunc(b.dt_inicio) = dt_inicial_p);	

elsif	(ie_opcao_p = 'THD') then
	
	select	count(*)
	into	qt_retorno_w
	from    unidade_atendimento a,
		sl_unid_atend b
	where   b.nr_seq_unidade = a.nr_seq_interno
	and     b.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(b.dt_inicio) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(b.dt_fim) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	b.cd_executor_fim_serv  in (select	a.cd_pessoa_fisica
				            from 	escala_diaria_adic a,
							escala_diaria b
					    where	a.nr_seq_escala_diaria = b.nr_sequencia
					    and		b.nr_seq_escala = nr_seq_escala_p
					    and		trunc(b.dt_inicio) = dt_inicial_p);	

elsif	(ie_opcao_p = 'TCE') then
	
	select	count(*)
	into	qt_retorno_w
	from    unidade_atendimento a,
		sl_unid_atend b,
		sl_check_list_unid c
	where   b.nr_seq_unidade = a.nr_seq_interno
	and	c.nr_seq_sl_unid = b.nr_sequencia
	and     b.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(b.dt_inicio) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(b.dt_fim) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(c.dt_liberacao) = dt_inicial_p
	and	b.cd_executor_fim_serv  in (select	a.cd_pessoa_fisica
				            from 	escala_diaria_adic a,
							escala_diaria b
					    where	a.nr_seq_escala_diaria = b.nr_sequencia
					    and		b.nr_seq_escala = nr_seq_escala_p
					    and		trunc(b.dt_inicio) = dt_inicial_p);	
	
elsif	(ie_opcao_p = 'TI') then
	
	select	sum(sl_obter_itens_nao_confor(c.nr_sequencia))
	into	qt_retorno_w
	from    unidade_atendimento a,
		sl_unid_atend b,
		sl_check_list_unid c
	where   b.nr_seq_unidade = a.nr_seq_interno
	and	c.nr_seq_sl_unid = b.nr_sequencia
	and     b.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(b.dt_inicio) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(b.dt_fim) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(c.dt_liberacao) = dt_inicial_p
	and	b.cd_executor_fim_serv  in (select	a.cd_pessoa_fisica
				            from 	escala_diaria_adic a,
							escala_diaria b
					    where	a.nr_seq_escala_diaria = b.nr_sequencia
					    and		b.nr_seq_escala = nr_seq_escala_p
					    and		trunc(b.dt_inicio) = dt_inicial_p);	
					    
elsif	(ie_opcao_p = 'THH') then

	select	(sum((b.dt_fim - b.dt_inicio) * 1440) / 60)
	into	qt_retorno_w
	from    unidade_atendimento a,
		sl_unid_atend b
	where   b.nr_seq_unidade = a.nr_seq_interno
	and     b.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(b.dt_inicio) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	trunc(b.dt_fim) between dt_inicial_p and fim_dia(dt_inicial_p)
	and	b.cd_executor_fim_serv  in (select	a.cd_pessoa_fisica
				            from 	escala_diaria_adic a,
							escala_diaria b
					    where	a.nr_seq_escala_diaria = b.nr_sequencia
					    and		b.nr_seq_escala = nr_seq_escala_p
					    and		trunc(b.dt_inicio) = dt_inicial_p);
	
end if;

return	qt_retorno_w;

end hsl_qtd_unid_higienizada_turno;
/