create or replace 
FUNCTION OBTER_DATA_INI_TRAT(cd_pessoa_fisica_p number)
			return varchar2 is

dt_retorno_w	varchar2(255);

begin

SELECT 	to_char(MIN(dt_inicio), 'yyyy-mm-dd') --Fixo para o M�xico
into 	dt_retorno_w
FROM 	DIAGNOSTICO_DOENCA DD, 
		cid_doenca CD, 
		atendimento_paciente AP 
WHERE 	DD.nr_atendimento = AP.nr_atendimento 
AND 	AP.cd_pessoa_fisica =  cd_pessoa_fisica_p
AND 	UPPER(trim(DD.cd_doenca)) = UPPER(trim(CD.cd_doenca_cid)) 
AND 	CD.ie_doenca_cronica_mx IN ('DIABT', 'OBSDA', 'HIPAS', 'LIPDM', 'MTABO')
AND 	DD.dt_inicio IS NOT NULL
AND 	DD.dt_liberacao IS NOT NULL AND DD.dt_inativacao IS NULL;

return dt_retorno_w;

END OBTER_DATA_INI_TRAT;
/