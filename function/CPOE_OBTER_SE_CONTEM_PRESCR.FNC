create or replace
function cpoe_obter_se_contem_prescr(	nr_sequencia_p 	number,
				ie_tipo_item_p	varchar2) return varchar2 is

ds_retorno_w		varchar2(100);

begin

if	(ie_tipo_item_p = 'M' or ie_tipo_item_p = 'MAT' or ie_tipo_item_p = 'SOL') then
	select 	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_material
	where	nr_seq_mat_cpoe = nr_sequencia_p;
elsif	(ie_tipo_item_p = 'SNE' or ie_tipo_item_p = 'S') then
 	select 	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_material
	where	nr_seq_dieta_cpoe = nr_sequencia_p;
elsif (ie_tipo_item_p = 'J') then
	select 	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	rep_jejum
	where	nr_seq_dieta_cpoe = nr_sequencia_p;
elsif (ie_tipo_item_p = 'LD') then
	select 	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_leite_deriv
	where	nr_seq_dieta_cpoe = nr_sequencia_p;
elsif (ie_tipo_item_p = 'NA' or ie_tipo_item_p = 'NP') then
	select 	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	nut_pac
	where	nr_seq_npt_cpoe = nr_sequencia_p;
elsif (ie_tipo_item_p = 'D') then
	select 	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_dieta
	where	nr_seq_dieta_cpoe = nr_sequencia_p;
elsif (ie_tipo_item_p = 'R') then
	select	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_recomendacao
	where	nr_seq_rec_cpoe = nr_sequencia_p;
elsif (ie_tipo_item_p = 'P') then
	select	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_procedimento
	where	nr_seq_proc_cpoe = nr_sequencia_p;		
elsif (ie_tipo_item_p = 'O') then
	select	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_gasoterapia
	where	nr_seq_gas_cpoe = nr_sequencia_p;	
elsif (ie_tipo_item_p = 'HM') then
	select	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_solic_bco_sangue
	where	nr_seq_hemo_cpoe = nr_sequencia_p;	
elsif (ie_tipo_item_p = 'DI') then	
	select	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	hd_prescricao
	where	nr_seq_dialise_cpoe = nr_sequencia_p;	
end if;

return ds_retorno_w;

end cpoe_obter_se_contem_prescr;
/
