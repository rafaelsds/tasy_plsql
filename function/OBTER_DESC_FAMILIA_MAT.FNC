create or replace
function Obter_Desc_Familia_Mat(nr_seq_familia_p	number)
				return			varchar2 is

ds_familia_w		varchar2(255);

begin

select	ds_familia
into	ds_familia_w
from	material_familia
where	nr_sequencia	= nr_seq_familia_p;

return ds_familia_w;

end Obter_Desc_Familia_Mat;
/