create or replace
function sl_obter_itens_nao_confor(nr_sequencia_p	number)
 		    	return number is
qt_itens_w	number(5);
begin

SELECT COUNT(*)
into qt_itens_w
FROM sl_chelist_Item_v
WHERE  NR_SEQ_CHECKLIST = nr_sequencia_p
AND ie_resultado = 'N';


return	qt_itens_w;

end sl_obter_itens_nao_confor;
/