create or replace 
FUNCTION Obter_desc_motivo_transf_pac(
			nr_sequencia_p	Number)
			return varchar2 is

ds_retorno_w	Varchar2(60);

BEGIN

if	(nr_sequencia_p is not null) then
	select	ds_motivo
	into	ds_retorno_w
	from	motivo_transf_pac
	where	nr_sequencia	= nr_sequencia_p;
end if;


return	ds_retorno_w;

END Obter_desc_motivo_transf_pac;
/
