CREATE OR REPLACE 
FUNCTION HSR_OBTER_SE_ALTA_ADMINISTR (dt_alta_p		   date,
				      dt_periodo_final_p   date)
			return varchar2 is

ie_retorno_w		varchar2(1):= 'N';

BEGIN

if (dt_periodo_final_p < nvl(dt_alta_p, sysdate)) then
	ie_retorno_w:= 'S';
else
	ie_retorno_w:= 'N';
end if;

return ie_retorno_w;

END HSR_OBTER_SE_ALTA_ADMINISTR;
/