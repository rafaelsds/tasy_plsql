create or replace
function mprev_desc_patologia(nr_seq_diagnostico_p			number)
 		    	return varchar is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar a descri��o da patologia pelo c�digo passado.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: MedPrev - Programas de Promo��o a Sa�de
[ x ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
ds_retorno_w	varchar2(255);

begin

if 	(nr_seq_diagnostico_p is not null) then
	select 	ds_diagnostico
	into	ds_retorno_w
	from 	diagnostico_interno
	where	nr_sequencia = nr_seq_diagnostico_p;
end if;

return	ds_retorno_w;

end mprev_desc_patologia;
/