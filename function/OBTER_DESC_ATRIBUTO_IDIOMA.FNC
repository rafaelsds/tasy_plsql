create or replace
function obter_desc_atributo_idioma(
				nm_tabela_p	varchar2,
				nm_atributo_p	varchar2,
				nr_seq_idioma_p	number)
 		    	return varchar2 is
ds_retorno_w	varchar2(4000);
begin

if	(nr_seq_idioma_p is not null) and (nr_seq_idioma_p > 0) then
	select	max(ds_traducao)
	into	ds_retorno_w
	from	tab_atributo_desc_idioma
	where	nm_tabela = nm_tabela_p
	and	nm_atributo = nm_atributo_p
	and	nr_seq_idioma = nr_seq_idioma_p;
	
	if	(ds_retorno_w is not null) and (ds_retorno_w <> ' ') then
		return ds_retorno_w;
	end if;
end if;

select	ds_atributo
into	ds_retorno_w
from	tabela_atributo
where	nm_tabela = nm_tabela_p
and	nm_atributo = nm_atributo_p;

return	ds_retorno_w;

end obter_desc_atributo_idioma;
/