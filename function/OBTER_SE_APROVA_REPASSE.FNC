Create or Replace
FUNCTION OBTER_SE_APROVA_REPASSE(	nr_seq_terceiro_p	number,
					nr_repasse_terceiro_p	number,
					dt_referencia_p		date,
					nm_usuario_p		varchar2,
					cd_perfil_p		number)

						RETURN VarChar2 IS

ie_aprova_repasse_w	varchar2(1)	:= 'N';
qt_regra_w		number(10);
vl_repasse_w		number(10);
nr_seq_regra_w		number(10);
qt_permissao_w		number(10);
qt_usuario_w		number(10);
qt_perfil_w		number(10);

cursor	c01 is
select	a.nr_sequencia
from	regra_aprovacao_repasse a
where	vl_repasse_w	between nvl(vl_minimo,vl_repasse_w) and nvl(vl_maximo,vl_repasse_w)
and	nvl(a.nr_seq_terceiro,nvl(nr_seq_terceiro_p,0))	= nvl(nr_seq_terceiro_p,0)
and	dt_referencia_p	between nvl(dt_inicio_vigencia,dt_referencia_p) and nvl(dt_fim_vigencia,dt_referencia_p)
order by	nvl(a.nr_seq_terceiro,0);

BEGIN

select	count(*)
into	qt_regra_w
from	regra_aprovacao_repasse a
where	nvl(a.nr_seq_terceiro,nvl(nr_seq_terceiro_p,0))	= nvl(nr_seq_terceiro_p,0)
and	dt_referencia_p	between nvl(dt_inicio_vigencia,dt_referencia_p) and nvl(dt_fim_vigencia,dt_referencia_p);

if	(qt_regra_w	= 0) then

	ie_aprova_repasse_w	:= 'S';

else

	select	nvl(obter_valor_repasse(nr_repasse_terceiro_p,'R'),0)
	into	vl_repasse_w
	from	dual;

	open	c01;
	loop
	fetch	c01 into
		nr_seq_regra_w;
	exit	when c01%notfound;
	end	loop;
	close	c01;

	select	count(*)
	into	qt_permissao_w
	from	regra_aprovacao_repas_usu a
	where	a.nr_seq_regra		= nr_seq_regra_w;

	if	(qt_permissao_w	= 0) then

		ie_aprova_repasse_w	:= 'S';

	else

		select	count(*)
		into	qt_usuario_w
		from	regra_aprovacao_repas_usu a
		where	a.nm_usuario_lib	= nm_usuario_p
		and	a.nr_seq_regra		= nr_seq_regra_w;

		if	(qt_usuario_w	> 0) then

			ie_aprova_repasse_w	:= 'S';

		else

			select	count(*)
			into	qt_perfil_w
			from	regra_aprovacao_repas_usu a
			where	a.cd_perfil_lib		= cd_perfil_p
			and	a.nr_seq_regra		= nr_seq_regra_w;

			if	(qt_perfil_w	> 0) then

				ie_aprova_repasse_w	:= 'S';

			end if;

		end if;

	end if;

end if;

RETURN ie_aprova_repasse_w;

END OBTER_SE_APROVA_REPASSE;
/