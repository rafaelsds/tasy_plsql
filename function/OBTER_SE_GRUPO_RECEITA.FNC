create or replace
function obter_se_grupo_receita(nr_grupo_receita_p number)
 		    	return varchar2 is
				
ie_grupo_receita_w	varchar2(1) := 'N';
				
begin

if (nvl(nr_grupo_receita_p,0) > 0) then

select	nvl(max('S'),'N')
into ie_grupo_receita_w
from oft_grupo_receita
where nr_grupo_receita = nr_grupo_receita_p;
	
end if;

return	ie_grupo_receita_w;

end obter_se_grupo_receita;
/