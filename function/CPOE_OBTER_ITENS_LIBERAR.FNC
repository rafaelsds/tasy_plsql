create or replace 
function cpoe_obter_itens_liberar(
					ds_campo_p    varchar2)
			return varchar2 is

ds_campo_w    	varchar2(3000);
i	     		integer;

begin

if	(ds_campo_p is not null) then
  begin

  ds_campo_w := replace(ds_campo_w, ' ', '');

  for i in 1..length(ds_campo_p) loop
    begin
		if 	(substr(ds_campo_p, i, 1) in ('1','2','3','4','5','6','7','8','9','0',';')) then
			ds_campo_w := ds_campo_w || substr(ds_campo_p, i, 1);
		end if;
    end;
  end loop;

  end;
end if;

ds_campo_w := replace(ds_campo_w, ';', ',');

return ds_campo_w;

end cpoe_obter_itens_liberar;
/
