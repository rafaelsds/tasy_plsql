CREATE OR REPLACE
function obter_usuario_deleg_proc_aprov(
			nr_sequencia_p		number,
			nr_seq_proc_aprov_p	number)
	RETURN Varchar2 IS

nm_usuario_w		Varchar2(60);
ds_Resultado_w	 	Varchar2(2000);
cd_pessoa_fisica_w	varchar2(10);
nm_usuario_deleg_w	varchar2(15);

cursor  c01 is
select	nm_usuario
from	pessoas_processo_aprovacao_v
where	nr_sequencia = nr_sequencia_p
and	nr_seq_proc_aprov	= nr_seq_proc_aprov_p;

BEGIN

open C01;
loop
fetch C01 into	
	nm_usuario_w;
exit when C01%notfound;
	begin
	
	select	obter_pessoa_fisica_usuario(nm_usuario_w,'C')
	into	cd_pessoa_fisica_w
	from	dual;
		
	select	obter_usuario_pessoa(obter_pessoa_delegacao(cd_pessoa_fisica_w, 'AC', sysdate))
	into	nm_usuario_deleg_w
	from	dual;
	
	if	(nm_usuario_deleg_w is not null) then
		begin
		if	(ds_Resultado_w is null) then
			ds_Resultado_w	:= nm_usuario_deleg_w;
		else
			ds_Resultado_w	:= ds_Resultado_w || ', ' || nm_usuario_deleg_w;
		end if;
		end;
	end if;	
	end;
end loop;
close C01;

RETURN ds_Resultado_w;

END obter_usuario_deleg_proc_aprov;
/
