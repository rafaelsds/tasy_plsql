create or replace
function pls_obter_nr_conselho_web
			(	ie_conselho_prof_tiss_p			Varchar2)
				return Number is

nr_sequencia_w		Number(10);
ds_retorno_w		Number(10);

begin

select  nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from 	conselho_profissional 
where   ie_conselho_prof_tiss = ie_conselho_prof_tiss_p;

ds_retorno_w := nr_sequencia_w;

return	ds_retorno_w;

end pls_obter_nr_conselho_web;
/
