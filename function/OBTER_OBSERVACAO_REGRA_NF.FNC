create or replace
function obter_observacao_regra_nf(nr_interno_conta_p number,
                                   nr_seq_protocolo_p number,
				   nr_seq_lote_protocolo_p number)
return varchar2 is

cd_convenio_w 		number;
cd_estabelecimento_w 	number;
ds_conteudo_w		varchar2(4000);
ds_lotes_protocolo_w 	varchar(4000);
qt_idade_w		varchar(20);
nr_protocolo_lotes_w 	protocolo_convenio.nr_protocolo%type;
nr_interno_conta_w 	conta_paciente.nr_interno_conta%type;
nm_paciente_w 		pessoa_fisica.nm_pessoa_fisica%type;
nr_atendimento_w 		conta_paciente.nr_atendimento%type;
dt_nascimento_w 		pessoa_fisica.dt_nascimento%type;
nr_cpf_w 		pessoa_fisica.nr_cpf%type;
cd_medico_resp_w 		pessoa_fisica.nm_pessoa_fisica%type;
nr_protocolo_w 		protocolo_convenio.nr_protocolo%type;
nr_seq_protocolo_w 	protocolo_convenio.nr_seq_protocolo%type;
nr_seq_doc_convenio_w	protocolo_convenio.nr_seq_doc_convenio%type;

cursor c01 is
select distinct nr_protocolo
  from protocolo_convenio
 where nr_seq_lote_protocolo = nr_seq_lote_protocolo_p;
	 
begin

if (nr_interno_conta_p > 0) then
	select cp.nr_interno_conta, 
		cp.nr_atendimento,
		pf.nm_pessoa_fisica nm_paciente, 
		pf.nr_cpf, 
		pf.dt_nascimento, 
		obter_idade(pf.dt_nascimento, sysdate, 'A') qt_idade,
		cp.cd_convenio_parametro, 
		cp.cd_estabelecimento,
		obter_nome_medico_eup(ap.cd_medico_resp, cp.cd_estabelecimento) cd_medico_resp
	into nr_interno_conta_w, 
		nr_atendimento_w, 
		nm_paciente_w, 
		nr_cpf_w, 
		dt_nascimento_w, 
		qt_idade_w, 
		cd_convenio_w, 
		cd_estabelecimento_w,
		cd_medico_resp_w
	from conta_paciente cp,
		pessoa_fisica pf,
		atendimento_paciente ap
	where pf.cd_pessoa_fisica = ap.cd_pessoa_fisica
	and 	ap.nr_atendimento   = cp.nr_atendimento	
	and 	cp.nr_interno_conta = nr_interno_conta_p;

	select 	ds_observacao			
	into 	ds_conteudo_w
	from 	parametro_nfs_observacao                 
	where 	cd_estabelecimento = cd_estabelecimento_w
	and 	cd_convenio = cd_convenio_w
	and 	ie_origem_nf = 'C';

	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nr_interno_conta', nr_interno_conta_w);
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nm_paciente', substr(nm_paciente_w,1,255));
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nr_atendimento', nr_atendimento_w);
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nr_cpf', nr_cpf_w);
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@dt_nascimento', to_char(dt_nascimento_w,'dd/mm/yyyy'));
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@qt_idade', qt_idade_w);	   
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@medic_resp_atend', substr(cd_medico_resp_w,1,255));
end if;

if (nr_seq_protocolo_p > 0) then
	select pc.nr_seq_protocolo, 
		pc.nr_protocolo, 
		pc.nr_seq_doc_convenio,
	       	pc.cd_convenio, 
	       	pc.cd_estabelecimento
	  into nr_seq_protocolo_w, 
	  	nr_protocolo_w, 
		nr_seq_doc_convenio_w,
	       	cd_convenio_w, 
		cd_estabelecimento_w
	  from protocolo_convenio pc
	 where pc.nr_seq_protocolo = nr_seq_protocolo_p;
	 
	select ds_observacao			
	  into ds_conteudo_w
	  from parametro_nfs_observacao                 
	 where cd_estabelecimento = cd_estabelecimento_w
	   and cd_convenio = cd_convenio_w
           and ie_origem_nf = 'P';

	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nr_seq_protocolo', nr_seq_protocolo_w);
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nr_protocolo',  nr_protocolo_w);
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nr_doc_convenio', nr_seq_doc_convenio_w);
end if;	   
  
if (nr_seq_lote_protocolo_p > 0) then
	open c01;
	loop
	fetch c01 into
		nr_protocolo_lotes_w;
	exit when c01%notfound;
		begin

		ds_lotes_protocolo_w := ds_lotes_protocolo_w || ' ' || nr_protocolo_lotes_w || ' ,'; 

		end;
	end loop;
	close c01;
	
	select max (cd_convenio), 
		max (cd_estabelecimento)
	  into cd_convenio_w, 
	  	cd_estabelecimento_w
	  from protocolo_convenio pc
	 where pc.nr_seq_lote_protocolo = nr_seq_lote_protocolo_p;

	select ds_observacao			
	  into ds_conteudo_w
	  from parametro_nfs_observacao                 
	 where cd_estabelecimento = cd_estabelecimento_w
	   and cd_convenio = cd_convenio_w
           and ie_origem_nf = 'L';

	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nr_seq_lote_protocolo', nr_seq_lote_protocolo_p);
	ds_conteudo_w	:= replace_macro(ds_conteudo_w,'@nr_protocolos_lote', '(' || substr(ds_lotes_protocolo_w, 2, length(ds_lotes_protocolo_w) -3) || ')');
end if;	   

return 	ds_conteudo_w;

end obter_observacao_regra_nf;
/