create or replace
function retirar_inconsistencia (ds_inconsistencia_p       varchar2,
                            cd_inconsistencia_p    varchar2)
                           return varchar2 is

ds_retorno_w       varchar2(2000);

begin

ds_retorno_w       := substr(ds_inconsistencia_p,1,255);

if (instr(ds_retorno_w, cd_inconsistencia_p) = 1) then
   ds_retorno_w    := substr(ds_retorno_w, length(cd_inconsistencia_p) + 2, 255);
elsif      (instr(ds_retorno_w, ' ' || cd_inconsistencia_p || ' ') > 0) then
   ds_retorno_w    := replace(ds_retorno_w, ' '|| cd_inconsistencia_p || ' ', ' ');
end if;

return substr(ds_retorno_w,1,255);

end;
/