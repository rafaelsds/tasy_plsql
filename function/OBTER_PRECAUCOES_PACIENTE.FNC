create or replace
function obter_precaucoes_paciente(cd_pessoa_fisica_p varchar2)
	return varchar2 is

ds_retorno_w varchar2(4000);
ds_auxiliar_w cih_precaucao.ds_precaucao%type;
nr_seq_precaucao_atend_w number;
nr_seq_precaucao_w number;
nr_Seq_motivo_precaucao_w number;
nr_atendimento_w atendimento_paciente.nr_atendimento%type;

Cursor C02 is
	select nr_atendimento
	from atendimento_paciente
	where cd_pessoa_fisica = cd_pessoa_fisica_p
	and dt_cancelamento is null
	order by dt_atualizacao_nrec desc;

Cursor C01 is
	select a.nr_sequencia
	from atendimento_precaucao a ,
	cih_precaucao b
	WHERE a.nr_atendimento = nr_atendimento_w
	AND b.nr_sequencia = a.nr_seq_precaucao
	AND a.dt_inativacao IS NULL
	AND a.dt_termino IS NULL
	AND a.dt_final_precaucao IS NULL
	AND a.dt_liberacao IS NOT NULL
	AND a.ie_situacao = 'A'
	ORDER BY NVL(b.nr_seq_prioridade,0),a.nr_sequencia;

begin

nr_seq_precaucao_atend_w := 0;

open C02;
loop
fetch C02 into
nr_atendimento_w;
exit when C02%notfound;
begin

	open C01;
	loop
	fetch C01 into
		nr_seq_precaucao_atend_w;
	exit when C01%notfound;
		if (nr_seq_precaucao_atend_w > 0) then

			select max(nr_seq_precaucao),
			max(nr_seq_motivo_isol)
			into nr_seq_precaucao_w,
			nr_Seq_motivo_precaucao_w
			from atendimento_precaucao
			where nr_sequencia = nr_seq_precaucao_atend_w;

			select max(ds_precaucao)
			into ds_auxiliar_w
			from cih_precaucao
			where nr_sequencia = nr_seq_precaucao_w;

			if (nvl(ds_retorno_w,'1') not like ('%'||ds_auxiliar_w||'%')) then
				if (ds_retorno_w is null) then
					ds_retorno_w := ds_auxiliar_w;
				else
					ds_retorno_w := ds_retorno_w||'; '|| ds_auxiliar_w;
				end if;
			end if;
		end if;
	end loop;
	close C01;
	end;
end loop;
close C02;


return ds_retorno_w;


end obter_precaucoes_paciente;
/