CREATE OR REPLACE
FUNCTION Obter_Atecaco_Atend_Tipo_conv(nr_atendimento_p		Number)
		RETURN Number IS

nr_seq_atecaco_w			Number(10,0);

BEGIN

select	nvl(max(nr_seq_interno),0)
into	nr_seq_atecaco_w
from 	convenio b,
	atend_categoria_convenio a
where	a.nr_atendimento 		= nr_atendimento_p
  and 	a.cd_convenio		= b.cd_convenio
  and	b.ie_tipo_convenio		<> 3;

RETURN nr_seq_atecaco_w;

END Obter_Atecaco_Atend_Tipo_conv;
/
