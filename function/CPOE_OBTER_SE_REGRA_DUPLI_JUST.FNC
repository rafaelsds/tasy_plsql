create or replace
function cpoe_obter_se_regra_dupli_just(ie_tipo_item_p			varchar2,
									cd_perfil_p				number,
									cd_setor_atendimento_p	number) 
				return varchar2 is
				
ds_retorno_w		varchar2(1) := 'S';
ie_existe_regra_w	varchar2(1);
nr_seq_regra_w		number(10);

begin

select	max(b.nr_sequencia) 
into	nr_seq_regra_w
from	cpoe_regra_consistencia c,
		cpoe_regra_duplicidade b
where	c.nr_sequencia = b.nr_seq_regra
and		nvl(c.cd_perfil, cd_perfil_p) = cd_perfil_p
and		nvl(c.cd_setor_atendimento, cd_setor_atendimento_p) = cd_setor_atendimento_p;


if (nr_seq_regra_w is not null) then
	select 	nvl(max('N'), 'S')
	into 	ds_retorno_w
	from	cpoe_regra_duplicidade b
	where	b.nr_sequencia = nr_seq_regra_w
	and		((ie_consiste_dupl_sol = 'A' and ie_tipo_item_p in ('MAT','MCOMP1', 'MCOMP2', 'MCOMP3','MCOMP4', 'MCOMP5', 'MCOMP6')) or
			(ie_consiste_dupl_exa = 'A' and ie_tipo_item_p = 'P') or
			(ie_consiste_dupl_nut = 'A' and ie_tipo_item_p in ('E','J','L','O','S')) or
			(ie_consiste_dupl_gas = 'A' and ie_tipo_item_p = 'G') or
			(ie_consiste_dupl_rec = 'A' and ie_tipo_item_p = 'R') or 
			(ie_consiste_dupl_hem = 'A' and ie_tipo_item_p in ('HBC', 'HP')) or
			(ie_consiste_dupl_dia = 'A' and ie_tipo_item_p in ('DI', 'DP')) or
			(ie_consiste_dupl_mat = 'A' and ie_tipo_item_p = 'M')or
			(ie_consiste_dupl_ana = 'N' and ie_tipo_item_p = 'AP'));
else
	ds_retorno_w := 'S';
end if;

return ds_retorno_w;

end cpoe_obter_se_regra_dupli_just;
/
