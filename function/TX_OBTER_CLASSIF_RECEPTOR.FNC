create or replace
function TX_Obter_Classif_Receptor (	nr_seq_receptor_p		number)
					return				varchar2 is
					
ie_retorno_w		varchar2(15);
qt_reg_efetivo_w	number(5,0);
qt_reg_doador_w		number(5,0);

begin

select	count(*)
into	qt_reg_efetivo_w
from	tx_doador_potencial
where	nr_seq_receptor		= nr_seq_receptor_p
and	ie_doador_efetivo	= 'S';

select	count(*)
into	qt_reg_doador_w
from	tx_doador_potencial
where	nr_seq_receptor		= nr_seq_receptor_p;


if	(qt_reg_efetivo_w > 0) then
	ie_retorno_w	:= 'CDE';
elsif	(qt_reg_doador_w > 0) then
	ie_retorno_w	:= 'CDP';
else
	ie_retorno_w	:= 'SDP';
end if;

return	ie_retorno_w;
	
end TX_Obter_Classif_Receptor;
/