create or replace
function Obter_se_exibe_indicacao(
				nr_sequencia_p			number,
				cd_perfil_p				number) return varchar2 is

ie_retorno_w			varchar2(1) := 'S';
ie_existe_regra_w		varchar2(1);
	
cursor c01 is
Select	nvl(a.ie_visualizar,'S')
from	san_indicacao_regra a,
		san_indicacao b
where	a.nr_seq_regra = b.nr_sequencia(+)
and		nvl(b.ie_situacao,'A')	= 'A'
and		b.nr_sequencia = nr_sequencia_p 
and		(a.cd_perfil = cd_perfil_p or a.cd_perfil is null)
order by a.cd_perfil;
	
begin

select decode(count(*),0,'N','S')
into	ie_existe_regra_w
from	san_indicacao_regra
where	cd_perfil = cd_perfil_p;


	
if (ie_existe_regra_w = 'S') then
	open C01;
	loop
	fetch C01 into	
		ie_retorno_w;
	exit when C01%notfound;
		ie_retorno_w	:= ie_retorno_w;
	end loop;
	close C01;
else
	ie_retorno_w := 'S';
end if;	
	
return	ie_retorno_w;

end Obter_se_exibe_indicacao;
/