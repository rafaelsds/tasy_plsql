create or replace
function ageint_obter_lista_espera_reab(nr_seq_item_p		number)
 		    	return number is

nr_seq_lista_w	number(10);			
begin
if	(nr_seq_item_p is not null) then
	select	max(nr_seq_lista) 
	into	nr_seq_lista_w
	from	agenda_integrada_item
	where	nr_sequencia = nr_seq_item_p;
end if;
return	nr_seq_lista_w;

end ageint_obter_lista_espera_reab;
/