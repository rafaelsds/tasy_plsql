create or replace
function permite_inserir_amamentacao(nr_atendimento_p number, nr_seq_nascimento_p number) return varchar is
			
ds_retorno_w varchar2(1) := 'N';	
qtd_registros_inativados_w number;		

begin

	select count(*) 
	  into qtd_registros_inativados_w
	  from nascimento_amamentacao 
	 where nr_atendimento = nr_atendimento_p
	   and ie_situacao <> 'I'
	   and nr_seq_nascimento = nr_seq_nascimento_p;
	   
	if (qtd_registros_inativados_w > 0) then
	  ds_retorno_w := 'S';
	end if;

return	ds_retorno_w;

end permite_inserir_amamentacao;
/