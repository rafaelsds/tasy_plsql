create or replace
function	sup_obter_media_comp_aprov_v2(
			cd_material_p			number,
			cd_estabelecimento_p	number,
			ie_tipo_p				varchar2)
		return number is

cd_grupo_material_w		number(3);
cd_subgrupo_material_w		number(10);
cd_classe_material_w		number(10);
vl_retorno_w			number(15,2);

begin

	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from	estrutura_material_v
	where	cd_material = cd_material_p;

	if((ie_tipo_p =  'C') or (ie_tipo_p = 'M')) then
		select	a.vl_media_compra
		into vl_retorno_w
		from	sup_media_compra_aprov a,
			sup_media_compra_aprov_es b
		where	a.nr_sequencia = b.nr_seq_regra
		and	a.cd_estabelecimento = cd_estabelecimento_p
		and	b.cd_classe_material = cd_classe_material_w
		and	a.dt_inicio_vigencia = 	(select	max(x.dt_inicio_vigencia)
						from	sup_media_compra_aprov x,
							sup_media_compra_aprov_es y
						where	x.nr_sequencia = y.nr_seq_regra
						and	x.cd_estabelecimento = cd_estabelecimento_p
						and	y.cd_classe_material = cd_classe_material_w
						and	trunc(x.dt_inicio_vigencia) <= trunc(sysdate))
		order by 
			nvl(b.cd_classe_material, 0),
			nvl(b.cd_subgrupo_material, 0),
			nvl(b.cd_grupo_material, 0);

	elsif (ie_tipo_p =  'G') then
		select	a.vl_media_compra
		into vl_retorno_w
		from	sup_media_compra_aprov a,
			sup_media_compra_aprov_es b
		where	a.nr_sequencia = b.nr_seq_regra
		and	a.cd_estabelecimento = cd_estabelecimento_p
		and	b.cd_grupo_material = cd_grupo_material_w
		and	a.dt_inicio_vigencia = 	(select	max(x.dt_inicio_vigencia)
						from	sup_media_compra_aprov x,
							sup_media_compra_aprov_es y
						where	x.nr_sequencia = y.nr_seq_regra
						and	x.cd_estabelecimento = cd_estabelecimento_p
						and	y.cd_grupo_material	= cd_grupo_material_w
						and	trunc(x.dt_inicio_vigencia) <= trunc(sysdate))
		order by 
			nvl(b.cd_classe_material, 0),
			nvl(b.cd_subgrupo_material, 0),
			nvl(b.cd_grupo_material, 0);
			
	elsif (ie_tipo_p =  'S') then
		select	a.vl_media_compra
		into vl_retorno_w
		from	sup_media_compra_aprov a,
			sup_media_compra_aprov_es b
		where	a.nr_sequencia = b.nr_seq_regra
		and	a.cd_estabelecimento = cd_estabelecimento_p
		and	b.cd_subgrupo_material = cd_subgrupo_material_w
		and	a.dt_inicio_vigencia = 	(select	max(x.dt_inicio_vigencia)
						from	sup_media_compra_aprov x,
							sup_media_compra_aprov_es y
						where	x.nr_sequencia = y.nr_seq_regra
						and	x.cd_estabelecimento = cd_estabelecimento_p
						and	y.cd_subgrupo_material	= cd_subgrupo_material_w
						and	trunc(x.dt_inicio_vigencia) <= trunc(sysdate))
		order by 
			nvl(b.cd_classe_material, 0),
			nvl(b.cd_subgrupo_material, 0),
			nvl(b.cd_grupo_material, 0);

	end if;

return	vl_retorno_w;

end sup_obter_media_comp_aprov_v2;
/