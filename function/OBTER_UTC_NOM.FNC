create or replace 
function obter_utc_nom(dt_referencia_p timestamp default sysdate)
return varchar2 is

ie_utc_w	varchar2(15);

begin

ie_utc_w	:= obter_utc();

return replace(ie_utc_w,':','');
  
end obter_utc_nom;
/