create or replace
function Obter_Se_Atrib_Estrut_Conta
		(cd_convenio_p		number,
		nm_atributo_p		varchar2,
		cd_estrutura_p		number)
		return varchar2 is

ie_atrib_w	varchar2(05)	:= 'N';

begin

select 	nvl(max('S'), 'N')
into	ie_atrib_w
from   	convenio_estrutura_conta b, 
	convenio_estrut_atrib a 
where  	a.nr_sequencia = b.nr_sequencia 
and    	b.cd_estrutura = cd_estrutura_p
and    	b.cd_convenio  = cd_convenio_p
and    	a.nm_atributo  = nm_atributo_p;

if	(ie_atrib_w = 'N') then
	select 	nvl(max('S'), 'N')
	into	ie_atrib_w
	from   	convenio_estrutura_conta b, 
		convenio_estrut_atrib a 
	where  	a.nr_sequencia = b.nr_sequencia 
	and    	b.cd_estrutura = cd_estrutura_p
	and	b.cd_convenio is null
	and    	a.nm_atributo  = nm_atributo_p;
end if;

return	ie_atrib_w;

end Obter_Se_Atrib_Estrut_Conta;
/