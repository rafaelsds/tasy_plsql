create or replace
function obter_desc_checklist(nr_sequencia_p		number)
 		    	return varchar2 is
ds_retorno_w    varchar2(255);
begin
if (nr_sequencia_p is not null) then

	select 	max(nm_checklist)
	into	ds_retorno_w
	from	CHECKLIST_PROCESSO
	where	nr_sequencia = nr_sequencia_p;

end if;


return	ds_retorno_w;

end obter_desc_checklist;
/