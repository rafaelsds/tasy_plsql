create or replace
function obter_data_passagem_atend(	nr_atendimento_p	Number,
				ie_tipo_gerar_p		Varchar2)
 		    	return Date is

dt_retorno_w	date;
qt_registro_w	number(10,0);		
			
begin

if	(ie_tipo_gerar_p = 'S') then
	dt_retorno_w := sysdate;
else
	select	max(dt_entrada)
	into	dt_retorno_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	qt_registro_w := 1;
	
	while (qt_registro_w > 0) loop
	
		dt_retorno_w := dt_retorno_w + 1/86400;
		
		select	count(*)
		into	qt_registro_w
		from	atend_paciente_unidade
		where	nr_atendimento = nr_atendimento_p
		and	dt_entrada_unidade = dt_retorno_w;
		
	end loop;
	
end if;

return	dt_retorno_w;

end obter_data_passagem_atend;
/