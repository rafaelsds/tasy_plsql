create or replace
function obter_se_diluente_onc_material (
		nr_seq_paciente_p		number,
		nr_seq_material_p		number,
		nr_seq_interno_p		number)
		return varchar2 is
		
ie_via_aplicacao_w		varchar2(5);
cd_setor_atendimento_w	number(5);
nr_seq_agrupamento_w	number(10);
ds_retorno_w			varchar2(1) := 'S';

begin

select	max(ie_via_aplicacao)
into	ie_via_aplicacao_w
from	paciente_protocolo_medic 
where	nr_seq_paciente = nr_seq_paciente_p
and		nr_seq_material = nr_seq_material_p;

select	max(cd_setor_atendimento)
into	cd_setor_atendimento_w
from	paciente_setor
where	nr_seq_paciente = nr_seq_paciente_p;

if	(ie_via_aplicacao_w is not null) then	

	select	decode(count(1),0,'N','S') 
	into	ds_retorno_w
	from	material_diluicao
	where	nr_seq_interno = nr_seq_interno_p
	and		(ie_via_aplicacao = ie_via_aplicacao_w or ie_via_aplicacao is null)
	and		ie_reconstituicao = 'N';

end if;

if	(ds_retorno_w = 'S') and 
	(cd_setor_atendimento_w is not null) then	
	
	select	obter_agrupamento_setor(cd_setor_atendimento_w) 
	into	nr_seq_agrupamento_w
	from 	dual;
	
	select	decode(count(1),0,'N','S') 
	into	ds_retorno_w
	from	material_diluicao
	where	nr_seq_interno = nr_seq_interno_p
	and		(cd_setor_atendimento = cd_setor_atendimento_w or cd_setor_atendimento is null);

	if	(ds_retorno_w = 'S') and 
		(nr_seq_agrupamento_w is not null) then	
	
		select	decode(count(1),0,'N','S') 
		into	ds_retorno_w
		from	material_diluicao
		where	nr_seq_interno = nr_seq_interno_p
		and		(nr_seq_agrupamento = nr_seq_agrupamento_w or nr_seq_agrupamento is null);
	
	end if;

end if;

return ds_retorno_w;

end obter_se_diluente_onc_material;
/