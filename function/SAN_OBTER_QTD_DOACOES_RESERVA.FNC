create or replace
function san_Obter_qtd_doacoes_reserva(nr_seq_reserva_p		NUMBER)
 		    	return NUMBER is

qt_doacoes_w		number(5);
		

begin

if (nr_seq_reserva_p is not null) then
	
	select  count(*)
	into	qt_doacoes_w
	from	san_doacao
	where	nr_seq_reserva   = nr_seq_reserva_p;
end if;

return	qt_doacoes_w;

end san_Obter_qtd_doacoes_reserva;
/