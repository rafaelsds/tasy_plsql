create or replace
function obter_se_exige_cpf_js(cd_pessoa_fisica_p	varchar2,
				cd_pessoa_responsavel_p	varchar2,
				ie_exige_cpf_conv_p	varchar2)
 		    	return varchar2 is

ie_exige_cpf_w		varchar2(1) := 'N';	
qt_idade_pf_w		number(10);	
cd_pessoa_fisica_w      pessoa_fisica.cd_pessoa_fisica%type;
nr_cpf_w		pessoa_fisica.nr_cpf%type;
ie_brasileiro_w		varchar2(1);
			
begin

-- ie_exige_cpf_conv_p param 501 da EUP

cd_pessoa_fisica_w := cd_pessoa_fisica_p;

if	(ie_exige_cpf_conv_p = 'S') then

	qt_idade_pf_w:= Obter_Idade_PF(cd_pessoa_fisica_p, sysdate, 'A');
	if	(nvl(qt_idade_pf_w,0) < 18) and
		(cd_pessoa_responsavel_p is not null) then
		cd_pessoa_fisica_w := cd_pessoa_responsavel_p;
	end if;
	
	select	max(trim(nvl(nr_cpf,obter_compl_pf(cd_pessoa_fisica,3,'C'))))
	into	nr_cpf_w
        from   	pessoa_fisica                                              
        where 	cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	ie_brasileiro_w := obter_se_brasileiro(cd_pessoa_fisica_w);
	
	if	(nr_cpf_w is null) and
		(ie_brasileiro_w = 'S') then
		ie_exige_cpf_w := 'S';
	end if;
	
end if;

return	ie_exige_cpf_w;

end obter_se_exige_cpf_js;
/
