create or replace
function pls_obter_prest_exec_monit (	nr_seq_prestador_exec_p		pls_prestador.nr_sequencia%type,
					nr_seq_prest_inter_p		pls_prestador_intercambio.nr_sequencia%type,
					cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type,
					cd_cgc_p			pessoa_juridica.cd_cgc%type)
 		    	return varchar2 is
			
ds_retorno_w			varchar2(20);
cd_cgc_w			pessoa_juridica.cd_cgc%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;

begin
cd_pessoa_fisica_w	:= cd_pessoa_fisica_p;
cd_cgc_w		:= cd_cgc_p;

if	(nr_seq_prestador_exec_p is not null) then
	
	select	cd_cgc,
		cd_pessoa_fisica
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_prestador_exec_p;
	
elsif	(nr_seq_prest_inter_p is not null) then
	
	select	nvl(nr_cpf,cd_cgc_intercambio)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_inter_p;
end if;

if	(ds_retorno_w is null) then
	if	(cd_pessoa_fisica_w is not null) then
		select	nr_cpf
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	else
		ds_retorno_w := cd_cgc_w;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_prest_exec_monit;
/
