create or replace
function obter_se_precaucao_lib(nr_seq_precaucao_p	number)
 		    	return varchar2 is

qt_reg_w	number(10);

begin

if	(nr_seq_precaucao_p is not null) then
	select	count(*)
	into	qt_reg_w
	from	cih_precaucao_lib
	where	nr_seq_precaucao = nr_seq_precaucao_p;
	
	if	(qt_reg_w > 0) then
		select	count(*)
		into	qt_reg_w
		from	cih_precaucao_lib
		where	nr_seq_precaucao = nr_seq_precaucao_p
		and	cd_perfil = obter_perfil_ativo;
		
		if	(qt_reg_w = 0) then
			return 'N';
		end if;
	end if;
end if;

return	'S';

end obter_se_precaucao_lib;
/