create or replace
function	obter_se_proc_autorizado(	nr_seq_agenda_p	number)
						return varchar is

ie_autorizado_w	varchar2(1):='N';

begin

if	(nvl(nr_seq_agenda_p,0) > 0) then
	select 	decode(count(*),0,'N','S')
	into	ie_autorizado_w
	from   	autorizacao_convenio a,
		estagio_autorizacao b
	where  	a.nr_seq_estagio    	= b.nr_sequencia
	and	b.ie_interno	   	= 10
	and	a.ie_tipo_autorizacao	= 3
	and	a.nr_seq_agenda		= nr_seq_agenda_p;
end if;

return	ie_autorizado_w;

end obter_se_proc_autorizado;
/