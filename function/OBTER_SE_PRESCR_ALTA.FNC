create or replace
function obter_se_prescr_alta(	nr_prescricoes_p		varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(1) := 'N';
	
begin

if	(nr_prescricoes_p is not null) then
	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	prescr_medica
	where	obter_se_contido(nr_prescricao, nr_prescricoes_p) = 'S'
	and	dt_suspensao is null
	and	nvl(ie_prescricao_alta,'N') = 'S';
end if;

return	ds_retorno_w;

end obter_se_prescr_alta;
/
