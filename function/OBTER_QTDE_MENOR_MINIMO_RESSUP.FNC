create or replace
function obter_qtde_menor_minimo_ressup(cd_material_p		Number,
					cd_estabelecimento_p	Number)
 		    	return Number is
			
cd_material_estoque_w			Number(10);
qt_estoque_minimo_w			Number(15,4);	
qt_Estoque_w				Number(15,4);
qt_faltante_w				Number(15,4) := 0;

begin

SELECT	a.cd_material_estoque,
	b.qt_estoque_minimo
INTO	cd_material_estoque_w,
	qt_estoque_minimo_w
FROM	material a,
	material_estab b
WHERE	a.cd_material = b.cd_material
AND	b.cd_Estabelecimento = cd_estabelecimento_p
AND	a.cd_material = cd_material_p;

SELECT	SUM(qt_estoque)
INTO	qt_estoque_w
FROM	saldo_estoque
WHERE	cd_estabelecimento = cd_estabelecimento_p
AND	cd_material = cd_material_estoque_w
AND	dt_mesano_referencia = TRUNC(SYSDATE,'mm');

if	(qt_estoque_w < qt_Estoque_minimo_w) then
	qt_faltante_W := qt_Estoque_minimo_w - qt_estoque_w;
end if;

return	qt_faltante_w;

end obter_qtde_menor_minimo_ressup;
/