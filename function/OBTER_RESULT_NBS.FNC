create or replace
function Obter_Result_nbs(qt_pontuacao_p		number)
 		    	return varchar2 is

begin

if	(qt_pontuacao_p	= -10) then
	return	'20 semanas';
elsif	(qt_pontuacao_p	= -9) then
	return	'20 semanas + 3 dias';
elsif	(qt_pontuacao_p	= -8) then
	return	'20 semanas + 6 dias';
elsif	(qt_pontuacao_p	= -7) then
	return	'21 semanas + 1 dia';
elsif	(qt_pontuacao_p	= -6) then
	return	'21 semanas + 4 dias';
elsif	(qt_pontuacao_p	= -5) then
	return	'22 semanas ';
elsif	(qt_pontuacao_p	= -4) then
	return	'22 semanas + 3 dias';
elsif	(qt_pontuacao_p	= -3) then
	return	'22 semanas + 6 dias';
elsif	(qt_pontuacao_p	= -2) then
	return	'23 semanas + 1 dia';
elsif	(qt_pontuacao_p	= -1) then
	return	'23 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 0) then
	return	'24 semanas';
elsif	(qt_pontuacao_p	= 1) then
	return	'24 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 2) then
	return	'24 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 3) then
	return	'25 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 4) then
	return	'25 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 5) then
	return	'26 semanas';
elsif	(qt_pontuacao_p	= 6) then
	return	'26 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 7) then
	return	'26 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 8) then
	return	'27 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 9) then
	return	'27 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 10) then
	return	'28 semanas';
elsif	(qt_pontuacao_p	= 11) then
	return	'28 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 12) then
	return	'28 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 13) then
	return	'29 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 14) then
	return	'29 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 15) then
	return	'30 semanas';
elsif	(qt_pontuacao_p	= 16) then
	return	'30 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 17) then
	return	'30 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 18) then
	return	'31 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 19) then
	return	'31 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 20) then
	return	'32 semanas';
elsif	(qt_pontuacao_p	= 21) then
	return	'32 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 22) then
	return	'32 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 23) then
	return	'33 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 24) then
	return	'33 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 25) then
	return	'34 semanas';
elsif	(qt_pontuacao_p	= 26) then
	return	'34 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 27) then
	return	'34 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 28) then
	return	'35 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 29) then
	return	'35 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 30) then
	return	'36 semanas';
elsif	(qt_pontuacao_p	= 31) then
	return	'36 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 32) then
	return	'36 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 33) then
	return	'37 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 34) then
	return	'37 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 35) then
	return	'38 semanas';
elsif	(qt_pontuacao_p	= 36) then
	return	'38 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 37) then
	return	'38 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 38) then
	return	'39 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 39) then
	return	'39 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 40) then
	return	'40 semanas';
elsif	(qt_pontuacao_p	= 41) then
	return	'40 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 42) then
	return	'40 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 43) then
	return	'41 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 44) then
	return	'41 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 45) then
	return	'42 semanas';
elsif	(qt_pontuacao_p	= 46) then
	return	'42 semanas + 3 dias';
elsif	(qt_pontuacao_p	= 47) then
	return	'42 semanas + 6 dias';
elsif	(qt_pontuacao_p	= 48) then
	return	'43 semanas + 1 dia';
elsif	(qt_pontuacao_p	= 49) then
	return	'43 semanas + 4 dias';
elsif	(qt_pontuacao_p	= 50) then
	return	'44 semanas';
else
	return '';	
end if;
end Obter_Result_nbs;
/