create or replace function obter_nome_traduzido_estab
 		    	return varchar2 is				
ds_retorno_w		varchar2(255);

begin
ds_retorno_w	:= ''; 

select	nvl(max(ie_translated_name),'N')
into	ds_retorno_w
from	establishment_locale
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

return	ds_retorno_w;

end obter_nome_traduzido_estab;
/
