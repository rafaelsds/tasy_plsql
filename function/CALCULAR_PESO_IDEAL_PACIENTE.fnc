create or replace function calcular_peso_ideal_paciente(nr_seq_paciente_p paciente_atendimento.nr_seq_paciente%type,
					nr_seq_atendimento_p 	paciente_atendimento.nr_seq_atendimento%type,
					qt_fator_correcao_p 	number,
					ie_opcao_calculo_p 	varchar2)
					return float is
					
qt_peso_ideal_w		float := 0.0;
nr_seq_paciente_w	number;
qt_peso_w			number(10,3);
cd_pessoa_fisica_w  varchar(10);
qt_altura_w     	number;

begin
	nr_seq_paciente_w := nr_seq_paciente_p;

	if (nr_seq_paciente_w is null and nr_seq_atendimento_p is not null) then
		select 	max(nr_seq_paciente)
		into 	nr_seq_paciente_w
		from 	paciente_atendimento
		where 	nr_seq_atendimento = nr_seq_atendimento_p;
	end if;

	select 	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from 	paciente_setor
	where 	nr_seq_paciente = nr_seq_paciente_w;

	if (nr_seq_atendimento_p is not null) then
		select 	obter_peso_ciclo(nr_seq_atendimento_p),
			max(qt_altura)
		into 	qt_peso_w,
			qt_altura_w
		from 	paciente_atendimento
		where 	nr_seq_atendimento = nr_seq_atendimento_p;

	else
		select 	obter_peso_protocolo(nr_seq_paciente_w),
			max(qt_altura)
		into 	qt_peso_w,
			qt_altura_w
		from 	paciente_setor
		where 	nr_seq_paciente = nr_seq_paciente_w;
	end if;

	if (lower(ie_opcao_calculo_p) = 'peso_ideal') then
		select 	obter_peso_ideal_pac(cd_pessoa_fisica_w,
			qt_altura_w)
		into 	qt_peso_ideal_w
		from 	dual;

	else 
		select 	obter_peso_ideal_ajust_pac(cd_pessoa_fisica_w,
			qt_peso_w,
			qt_fator_correcao_p,
			qt_altura_w)
		into 	qt_peso_ideal_w
		from 	dual;
	end if;

return qt_peso_ideal_w;

end calcular_peso_ideal_paciente;
/ 
