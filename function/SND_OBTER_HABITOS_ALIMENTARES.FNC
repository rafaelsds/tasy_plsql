create or replace
function snd_obter_habitos_alimentares	(cd_pessoa_fisica_p	varchar2,
					ie_opcao_p		varchar2)
				return varchar2 is

/* ie_opcao_p -> eventuais futuras customizações */

ds_vicio_w		varchar2(80);
ds_retorno_w		varchar2(2000) := '';
ds_string_retorno_w	varchar2(2000) := '';

cursor C01 is
select	substr(obter_descricao_padrao('MED_TIPO_VICIO','DS_VICIO',a.nr_seq_tipo),1,254)
into	ds_retorno_w
from   	paciente_habito_vicio a
where  	a.cd_pessoa_fisica = cd_pessoa_fisica_p
and	a.dt_fim is null
and    	upper(a.ie_classificacao) = upper('C') ;

begin
open c01;
	loop
		fetch c01 into
			ds_vicio_w;
	exit when c01%notfound;
		if	length(ds_vicio_w) > 0 then
			ds_retorno_w := ds_retorno_w || ds_vicio_w || ', ';
		end if;
	end loop;
close C01;

if	length(trim(substr((ds_retorno_w),1,length(ds_retorno_w)-2) || '.' )) > 1 then
	ds_string_retorno_w := trim(substr((ds_retorno_w),1,length(ds_retorno_w)-2) || '.' );
else
	ds_string_retorno_w := null;
end if;

return ds_string_retorno_w;

--tratamento em pontuaçao final da string e usar separador ' , ' apenas se houver informaçao.
end snd_obter_habitos_alimentares;
/