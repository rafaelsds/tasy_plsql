create or replace 
function Consistir_solucao_etapa_rep(
				nr_prescricao_p	number,
				nr_seq_solucao_p	number) return varchar2 is

qt_existe_w	number(10,0) := 0;
ie_calc_aut_w	varchar2(1);
nr_etapas_w	number(3,0);
retorno_w		varchar2(1) := 'N';
									
begin

select	count(*)
into	qt_existe_w
from	prescr_material
where	nr_prescricao = nr_prescricao_p
and	nr_sequencia_solucao = nr_seq_solucao_p
and	ie_agrupador = 4;

select	nvl(ie_calc_aut,'N'),
	nvl(nr_etapas,0)
into	ie_calc_aut_w,
	nr_etapas_w
from	prescr_solucao
where	nr_prescricao = nr_prescricao_p
and	nr_seq_solucao = nr_seq_solucao_p;

if	(qt_existe_w <> 0) and (ie_calc_aut_w = 'S') and (nr_etapas_w = 0) then
	
	retorno_w := 'S';

end if;

return retorno_w; 

end Consistir_solucao_etapa_rep;
/