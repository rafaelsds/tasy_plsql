create or replace
function san_verif_tipo_doacao_auto_dir	(nr_seq_tipo_doacao_p	number)
 		    	return varchar2 is

ds_tipo_doacao_w	varchar2(1);
			
begin
if	(nr_seq_tipo_doacao_p is not null) then
	
	select	nvl(x.ie_classif_doacao, null)
	into	ds_tipo_doacao_w
	from	san_tipo_doacao x
	where	x.nr_sequencia = nr_seq_tipo_doacao_p;

end if;

return	ds_tipo_doacao_w;

end san_verif_tipo_doacao_auto_dir;
/