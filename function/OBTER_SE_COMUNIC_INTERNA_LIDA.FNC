create or replace 
function Obter_Se_Comunic_Interna_Lida
		(nr_sequencia_p	number,
		 nm_usuario_p	varchar2)
		 return varchar2 is

ds_retorno_w	varchar2(001)	:= 'N';
begin

select nvl(max('S'),'N')
into ds_retorno_w
from comunic_interna_lida
where nr_sequencia = nr_sequencia_p
  and nm_usuario = nm_usuario_p;

return	ds_retorno_w;

end Obter_Se_Comunic_Interna_Lida;
/