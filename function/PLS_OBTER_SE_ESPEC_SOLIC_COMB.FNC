create or replace
function pls_obter_se_espec_solic_comb
			(	nr_seq_guia_proc_p		number,
				nr_seq_req_proc_p	number,
				nr_seq_exec_proc_p	number,
				nr_seq_proc_espec_p	number,
				cd_especialidade_p	number	)
				return varchar2 is

ie_retorno_w			varchar2(1) := 'N';
cd_procedimento_w		procedimento.cd_procedimento%type;
ie_origem_proced_w		procedimento.ie_origem_proced%type;
cd_area_w			area_procedimento.cd_area_procedimento%type;
cd_especialidade_w		especialidade_proc.cd_especialidade%type;
cd_grupo_w			procedimento.cd_grupo_proc%type;
ie_origem_w			procedimento.ie_origem_proced%type;
nr_seq_regra_proc_espec_w	Number(10) := 0;
qt_regra_valida_w		Number(10);

Cursor c01 is
	select	a.ie_liberado
	from	pls_oc_proc_espec_regra	a,
		pls_oc_proc_especialidade b
	where	a.nr_seq_regra = b.nr_sequencia
	and	b.nr_sequencia = nr_seq_proc_espec_p
	and	((a.cd_procedimento is null)	or ((a.cd_procedimento 		= cd_procedimento_w) and (ie_origem_proced = ie_origem_w)))
	and	(a.cd_grupo_proc is null 	or a.cd_grupo_proc 		= cd_grupo_w)
	and	(a.cd_especialidade is null	or a.cd_especialidade		= cd_especialidade_w)
	and	(a.cd_area_procedimento is null	or a.cd_area_procedimento	= cd_area_w)
	and	a.ie_situacao		 	= 'A'
	and	b.cd_espec_medica		= cd_especialidade_p
	order by
		nvl(a.cd_procedimento,0),
		nvl(a.cd_grupo_proc,0),
		nvl(a.cd_especialidade,0),
		nvl(a.cd_area_procedimento,0);
begin
if 	( nr_seq_guia_proc_p is not null) then
	begin
		select	cd_procedimento,
			ie_origem_proced
		into	cd_procedimento_w,
			ie_origem_proced_w
		from	pls_guia_plano_proc
		where	nr_sequencia  = nr_seq_guia_proc_p ;
	exception
	when others then
		cd_procedimento_w	:= null;
	end;
elsif	(nr_seq_req_proc_p is not null) then
	begin
		select	cd_procedimento,
			ie_origem_proced
		into	cd_procedimento_w,
			ie_origem_proced_w
		from	pls_requisicao_proc
		where	nr_sequencia  = nr_seq_req_proc_p;
	exception
	when others then
		cd_procedimento_w	:= null;
	end;
elsif	(nr_seq_exec_proc_p is not null) then
	begin
		select	cd_procedimento,
			ie_origem_proced
		into	cd_procedimento_w,
			ie_origem_proced_w
		from	pls_execucao_req_item
		where	nr_sequencia  = nr_seq_exec_proc_p;
	exception
	when others then
		cd_procedimento_w	:= null;
	end;
end if;

if	(cd_procedimento_w > 0) then
	/*Obter area, grupo, especialidade do procedimentos  */
	pls_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, cd_area_w,
			      cd_especialidade_w, cd_grupo_w, ie_origem_w);

	for r_c01_w in c01 loop
		/* Se n�o estiver liberado na regra, da� gera a ocorr�ncia */
		if	(r_c01_w.ie_liberado = 'N') then
			ie_retorno_w	:= 'S';
		else
			ie_retorno_w	:= 'N';
		end if;

	end loop;
end if;

return	ie_retorno_w;

end pls_obter_se_espec_solic_comb;
/