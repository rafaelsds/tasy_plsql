create or replace
function OBTER_ESCADAS_BARTHEL(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w 	varchar2(255);
			
begin

select 	DECODE(IE_ESCADAS,0,'Incapaz',5,'Precisa de ajuda(verbal f�sica ou de suporte)',10,'Independente')
into	ds_retorno_w
from	escala_barthel
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end OBTER_ESCADAS_BARTHEL;
/