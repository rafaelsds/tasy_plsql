create or replace
function obter_url_opme_inpart(
		cd_estabelecimento_p	number)
		return varchar2 is
ds_url_opme_w	varchar2(120) := '';
begin
if	(cd_estabelecimento_p is not null) then
	begin
	select	max(nvl(ds_url_opme,''))
	into	ds_url_opme_w
	from	parametro_compras
	where	cd_estabelecimento = cd_estabelecimento_p;
	end;
end if;
return ds_url_opme_w;
end obter_url_opme_inpart;
/