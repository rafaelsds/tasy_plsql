create or replace
function obter_estabelecimento_cotacao
			(	nr_cot_cotacao_p	Number)
 		    		return Number is

cd_estabelecimento_w	Number(4);
				
begin

select	nvl(cd_estabelecimento, 1)
into	cd_estabelecimento_w
from	cot_compra 
where	nr_cot_compra = nr_cot_cotacao_p;

return	cd_estabelecimento_w;

end obter_estabelecimento_cotacao;
/