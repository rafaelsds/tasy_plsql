create or replace
function obter_resultado_escala_berg(qt_pontos_p	number)
 		    	return varchar2 is

ds_resultado_w	varchar2(255);

begin
if	(qt_pontos_p < 45) then
	ds_resultado_w := Wheb_mensagem_pck.get_texto(309704); --'Maior probabilidade de queda';
elsif	(qt_pontos_p >= 45) then
	ds_resultado_w := Wheb_mensagem_pck.get_texto(309706); --'Menor probabilidade de queda';
end if;

return	ds_resultado_w;

end obter_resultado_escala_berg;
/