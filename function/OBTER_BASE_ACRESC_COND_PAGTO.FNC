create or replace
function obter_base_acresc_cond_pagto(	cd_condicao_pagamento_p	number,
					vl_original_p		number)
							return number is

vl_retorno_w		number(15,2);
tx_fracao_parcela_w	number(15,4);
vl_base_juros_w		number(15,2)	:= 0;
vl_parcela_w		number(15,2);

Cursor c01 is
select	tx_fracao_parcela
from	parcela a
where	a.cd_condicao_pagamento	= cd_condicao_pagamento_p
and	nvl(a.ie_base_juros,'N')= 'S';

begin

open c01;
loop
fetch c01 into
	tx_fracao_parcela_w;
exit when c01%notfound;
	vl_parcela_w	:= (vl_original_p * tx_fracao_parcela_w) / 100;

	vl_base_juros_w	:= vl_base_juros_w + vl_parcela_w;
end loop;
close c01;

vl_retorno_w	:= vl_base_juros_w;

return vl_retorno_w;

end obter_base_acresc_cond_pagto;
/