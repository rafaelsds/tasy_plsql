create or replace
function obter_regra_motivo_atend(	cd_material_p		number,
				nr_seq_tipo_baixa_p	number) 
				return varchar2 is

qt_existe_w		number(20);
cd_classe_material_w	number(5);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
nr_sequencia_w		regra_motivo_atend_prescr.nr_sequencia%type;
ie_retorno_w		varchar2(1) := 'S';

begin

select	count(1)
into	qt_existe_w
from	motivo_baixa_atend_prescr b,
	regra_motivo_atend_prescr a
where	a.nr_sequencia = b.nr_seq_mot_atend_presc
and	((a.cd_estabelecimento is null) or (a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento));

if	(qt_existe_w > 0) then

	select	max(cd_grupo_material),
		max(cd_subgrupo_material),
		max(cd_classe_material)
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from	estrutura_material_v
	where	cd_material = cd_material_p;
	
	select	nvl(max(nr_sequencia), 0)
	into	nr_sequencia_w
	from	regra_motivo_atend_prescr
	where	nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
	and	nvl(cd_subgrupo_material, cd_subgrupo_material_w) = cd_subgrupo_material_w
	and	nvl(cd_classe_material, cd_classe_material_w) = cd_classe_material_w
	and	nvl(cd_material, cd_material_p) = cd_material_p
	and	((cd_estabelecimento is null) or (cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento));
	
	if	(nr_sequencia_w > 0) then
		
		select	count(1)
		into	qt_existe_w
		from	motivo_baixa_atend_prescr
		where	nr_seq_mot_atend_presc = nr_sequencia_w;
		
		if	(qt_existe_w > 0 and nr_seq_tipo_baixa_p is not null) then
			
			select	nvl(max('S'),'N')
			into	ie_retorno_w
			from	motivo_baixa_atend_prescr
			where	nr_seq_mot_atend_presc = nr_sequencia_w
			and	nr_seq_tipo_baixa = nr_seq_tipo_baixa_p;

		end if;
	end if;
end if;

return ie_retorno_w;

end obter_regra_motivo_atend;
/