create or replace
function san_obter_cd_sangue_isbt(	ie_tipo_sangue_p	varchar2,
					ie_fator_rh_p		varchar2,
					nr_seq_tipo_doacao_p	Number)
 		    	return Varchar2 is
cd_sangue_isbt_w	Varchar2(2);

Cursor C01 is
	select	cd_sangue_isbt
	from	san_grupo_sanguineo_isbt
	where	nvl(ie_tipo_sangue, ie_tipo_sangue_p) = ie_tipo_sangue_p
	and	nvl(ie_fator_rh, ie_fator_rh_p) = ie_fator_rh_p
	and	nvl(nr_seq_tipo_doacao, nr_seq_tipo_doacao_p) = nr_seq_tipo_doacao_p
	order by nr_seq_tipo_doacao desc, ie_fator_rh desc, ie_tipo_sangue desc;	
		
begin
	
open C01;
loop
fetch C01 into	
	cd_sangue_isbt_w;
exit when C01%notfound;
	begin
	null;
	end;
end loop;
close C01;

return	cd_sangue_isbt_w;

end san_obter_cd_sangue_isbt;
/
