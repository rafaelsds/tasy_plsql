create or replace function obter_nr_fases_pend_prot_radio(nr_seq_tratamento_p number)
							return number is


qt_fases_pendentes_prot_w number(10) := 0;
nr_seq_volume_trat_w number(10);
qt_fase_pend_volume number(10);

cursor c01 is
	select nr_sequencia
	from rxt_volume_tratamento
	where nr_seq_tratamento = nr_seq_tratamento_p;

begin

if	(nr_seq_tratamento_p is not null) then

open c01;
	loop
	fetch c01 into nr_seq_volume_trat_w;
	exit when c01%notfound;
		begin
		
		select count(*)
		into qt_fase_pend_volume
		from rxt_fase_tratamento
		where dt_lib_seg_fisico is null
		and dt_aprovacao_medico is null
		and nr_seq_volume_tratamento = nr_seq_volume_trat_w;
		
		qt_fases_pendentes_prot_w := qt_fases_pendentes_prot_w + qt_fase_pend_volume;
		
		end;
	end loop;
	close c01;

end if;

return	qt_fases_pendentes_prot_w;

end obter_nr_fases_pend_prot_radio;
/