create or replace
function pls_obter_nota_mensalidade(nr_seq_mensalidade_p	number)
						return number is
nr_seq_nota_fiscal_w	number(10);

begin

if	(nr_seq_mensalidade_p is not null) then

	select	max(nr_sequencia)
	into	nr_seq_nota_fiscal_w
	from	nota_fiscal
	where	nr_seq_mensalidade	= nr_seq_mensalidade_p;
end if;

return nr_seq_nota_fiscal_w;

end;
/