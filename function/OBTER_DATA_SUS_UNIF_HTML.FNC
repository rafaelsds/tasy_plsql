create or replace
function obter_data_sus_unif_html(		nr_interno_conta_p	number,
				ie_opcao_p	varchar2) 
				return Date is

/*
I - Data de inicio da validade
F - Data final da validade
T - Data competencia da apac
*/

dt_inicio_validade_w	Date;
dt_fim_validade_w		Date;
dt_competencia_w        Date;

begin

if	(nr_interno_conta_p is not null) then
	select	dt_inicio_validade,
			dt_fim_validade,
            dt_competencia
	into	dt_inicio_validade_w,
			dt_fim_validade_w,
            dt_competencia_w
	from	sus_apac_unif
	where	nr_interno_conta = nr_interno_conta_p;
	
	if		(ie_opcao_p = 'I') then
			return dt_inicio_validade_w;
	elsif	(ie_opcao_p = 'F') then
			return dt_fim_validade_w;
	elsif	(ie_opcao_p = 'T') then
			return dt_competencia_w;            
	end if;	
end if;

end obter_data_sus_unif_html;
/
