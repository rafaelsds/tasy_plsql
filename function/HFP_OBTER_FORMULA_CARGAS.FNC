create or replace
function hfp_obter_formula_cargas(ds_formula_p	varchar2,
				nr_seq_musc_p	number,
				qt_valor_p	varchar2)
				return varchar2 is

ds_formula_w		varchar2(2000);
ie_formula_ok_w		number(5);
ds_formula_aux_w	varchar2(2000);
nr_seq_musc_w		number(10);
ie_tamanho_w		number(5);
i			number(5);
qt_valor_atual_w	varchar2(255);
ds_resultado_w		varchar2(255);

begin

qt_valor_atual_w := replace(qt_valor_p, ',', '.');

ds_formula_w	:= ds_formula_p;
ie_formula_ok_w	:= instr(ds_formula_w, '@');

if	(ie_formula_ok_w > 0) then

	ds_formula_aux_w 	:= ds_formula_w;	
	
	while (instr(ds_formula_aux_w, '@') > 0) loop
	
		ie_formula_ok_w := instr(ds_formula_aux_w, '@');
		ie_tamanho_w := 0;

		for i in ie_formula_ok_w..length(ds_formula_aux_w) + 1 loop
		
			if	((substr(ds_formula_aux_w, i, 1) in (' ','/','*','+','-','(',')','@',',')) or
				(i = length(ds_formula_aux_w) + 1)) and
				(i > (ie_formula_ok_w + 1)) then
				
				ie_tamanho_w := i;
				exit;
				
			end if;
			
		end loop;

		begin

		nr_seq_musc_w	:= to_number(substr(ds_formula_aux_w, ie_formula_ok_w + 1, ie_tamanho_w - (ie_formula_ok_w + 1)));
		
		exception
			when others then
				nr_seq_musc_w	:= to_number(replace(substr(ds_formula_aux_w, ie_formula_ok_w + 1, ie_tamanho_w - (ie_formula_ok_w +  1)),',','.'));
		end;

		ds_formula_aux_w 	:= replace((replace(ds_formula_aux_w, '@' || nvl(nr_seq_musc_w,nr_seq_musc_p), qt_valor_atual_w)),' ','');
		ds_formula_w 	 	:= ds_formula_aux_w;

	end loop;
	
end if;	

ie_formula_ok_w := instr(ds_formula_w, '@');

if (nvl(ie_formula_ok_w,0) = 0) then

	obter_valor_dinamico_char_bv('select '||ds_formula_w||' from dual',null, ds_resultado_w);

	if	(ds_resultado_w is null) or
		(ds_resultado_w = '') then
		obter_valor_dinamico('select ' || replace(ds_formula_w, ',', '.') || ' from dual', ds_resultado_w);
	end if;
end if;
		
return ds_resultado_w;

end;
/
