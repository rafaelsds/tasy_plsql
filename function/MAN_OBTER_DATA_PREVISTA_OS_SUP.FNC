create or replace
function man_obter_data_prevista_os_sup ( nr_seq_ordem_serv_p		number	)
 		    	return date is

dt_prevista_w			date;

begin

select	max(a.dt_prevista)
into	dt_prevista_w
from	man_ordem_ativ_prev a
where	a.nr_seq_ordem_serv = nr_seq_ordem_serv_p
and	a.nm_usuario_prev = (	select	max(c.nm_usuario_exec)
				from	usuario_grupo_sup b,
					man_ordem_servico_exec c
				where	b.nm_usuario_grupo = c.nm_usuario_exec
				and	c.nr_seq_ordem = nr_seq_ordem_serv_p
				and	c.dt_fim_execucao is null
			);

return	dt_prevista_w;

end man_obter_data_prevista_os_sup;
/