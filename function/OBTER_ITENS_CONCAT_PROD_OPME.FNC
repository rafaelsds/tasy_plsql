create or replace
function obter_itens_concat_prod_opme(nr_lote_producao_p	number,
				      nr_sequencia_p		number)
 		    	return varchar2 is

ds_itens_w	varchar2(254);
ds_retorno	varchar2(4000);

cursor c01 is
	select	substr(obter_nm_documento_opme(a.nr_seq_documento),1,255)
	from	comp_documento_opme a
	where	1 = 1
	and  	a.nr_lote_producao  = nr_lote_producao_p
	and	a.nr_seq_componente = nr_sequencia_p
	order by 1;

begin
open c01;
loop
fetch c01 into
	ds_itens_w;
exit when c01%notfound;
	begin
	if ds_retorno is null then
		ds_retorno := ds_itens_w;
	else
		ds_retorno := ds_retorno || ', ' || ds_itens_w;
	end if;
	end;
end loop;
close c01;

return	ds_retorno;

end obter_itens_concat_prod_opme;
/
