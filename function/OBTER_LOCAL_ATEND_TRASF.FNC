create or replace function obter_local_atend_trasf(	nr_ordem_compra_p		number,
                                                    nr_item_oci_p           number default null)
					return number is

cd_local_transf_w	number(4);

begin

    if ( OBTER_FUNCAO_ATIVA() = 221 ) then
    select cd_local_estoque
    into cd_local_transf_w
    from ordem_compra_item
    where	nr_item_oci = nvl(nr_item_oci_p,0)
    and nr_ordem_compra = nvl(nr_ordem_compra_p,0);

        if (cd_local_transf_w is null) then
        cd_local_transf_w := null;
        select	cd_local_transf
        into	cd_local_transf_w
        from	ordem_compra
        where	nr_ordem_compra = nvl(nr_ordem_compra_p,0);

        end if;

    else 

    select	cd_local_transf
    into	cd_local_transf_w
    from	ordem_compra
    where	nr_ordem_compra = nvl(nr_ordem_compra_p,0);

    end if;

return	cd_local_transf_w;

end obter_local_atend_trasf;
/