create or replace 
function Obter_Encaixe_classificacao(dt_encaixe_p			date,
									cd_agenda_p				number,
									nr_seq_classif_agenda_p	number,
									cd_tipo_agenda_p		number,
									cd_classificacao_p		varchar2,
									dt_geracao_encaixe_p	date,
                  cd_convenio_p number default null,
									cd_medico_p number default null)
					return	varchar2 is

ie_bloquear_w				varchar2(2)	:= 'N';
nr_sequencia_w				number(10);
ds_classificacao_agenda_w	varchar2(15);
ie_dia_semana_w				boolean;
qt_registros_w				number(1) := 0;
qt_existe_regra_w			number(1) := 0;
ie_tipo_turno_w				varchar2(1); -- N=Normal E=Especial
cd_classificacao_w			varchar2(15);
ie_val_classif_lib_w		varchar2(1) := 'S';
cd_estabelecimento_w		number(10);
dt_geracao_enc_w			date;

begin
select 	nvl(max(1),0)
into	qt_existe_regra_w
from 	agenda_turno_classif_lib;

if (qt_existe_regra_w = 0) then
	goto final;
end if;

	if (cd_tipo_agenda_p = 2) then
		begin
			select 	'E',
				nr_sequencia, 
				nr_seq_classif_agenda
			into 	ie_tipo_turno_w,
				nr_sequencia_w,
				ds_classificacao_agenda_w
			from 	agenda_horario_esp
			where 	cd_agenda = cd_agenda_p
			and		dt_agenda = trunc(dt_geracao_encaixe_p)
			and 	dt_encaixe_p between to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and 	to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and 	rownum = 1;
		exception
		when no_data_found then
			nr_sequencia_w := null;
		end;
		if (nr_sequencia_w is null) then
			begin
				select 	'N',
					nr_sequencia, 
					nr_seq_classif_agenda
				into 	ie_tipo_turno_w,
					nr_sequencia_w,
					ds_classificacao_agenda_w
				from 	agenda_horario
				where 	cd_agenda = cd_agenda_p
				and 	trunc(nvl(dt_inicio_vigencia,sysdate)) <= trunc(sysdate) 
				and		trunc(nvl(dt_final_vigencia,sysdate)) >= trunc(sysdate)
				and 	dt_encaixe_p between to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
				and 	to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
				and 	((pkg_date_utils.get_weekday(dt_geracao_encaixe_p) = dt_dia_semana) or ((dt_dia_semana = '9') and (pkg_date_utils.is_business_day(dt_geracao_encaixe_p) = 1)))
				and 	rownum = 1;
			exception
			when no_data_found then
				nr_sequencia_w := null;
			end;
		end if;

		if (nvl(nr_sequencia_w,0) > 0) then
			if (ie_tipo_turno_w = 'N') then
				select 	count(1)
				into	qt_registros_w
				from 	agenda_turno_classif_lib
				where 	nr_seq_horario = nr_sequencia_w;
			else
				select 	count(1)
				into	qt_registros_w
				from 	agenda_turno_classif_lib
				where 	nr_seq_hor_esp = nr_sequencia_w;
			end if;
		end if;

		ds_classificacao_agenda_w := obter_classif_regra_alteracao(2, cd_agenda_p, dt_encaixe_p, ds_classificacao_agenda_w);

		if (qt_registros_w = 0)
		or ((ds_classificacao_agenda_w is null) and (nr_seq_classif_agenda_p is null))
		or (ds_classificacao_agenda_w = nr_seq_classif_agenda_p) then
			goto final;
		end if;

		if (ie_tipo_turno_w = 'N') then
			select 	count(1)
			into	qt_registros_w
			from 	agenda_turno_classif_lib
			where 	nr_seq_horario = nr_sequencia_w
			and 	nr_seq_classif_agenda = nr_seq_classif_agenda_p;
		else
			select 	count(1)
			into	qt_registros_w
			from 	agenda_turno_classif_lib
			where 	nr_seq_hor_esp = nr_sequencia_w
			and 	nr_seq_classif_agenda = nr_seq_classif_agenda_p;
		end if;

		if (qt_registros_w = 0) then
			ie_bloquear_w := 'S';
		end if;

	ELSIF (cd_tipo_agenda_p = 3) then
		begin
			select 	'E',
				nr_sequencia, 
				ie_classif_agenda
			into 	ie_tipo_turno_w,
				nr_sequencia_w,
				ds_classificacao_agenda_w
			from 	agenda_turno_esp
			where 	cd_agenda = cd_agenda_p
			and		trunc(dt_geracao_encaixe_p) between trunc(dt_agenda) and fim_dia(trunc(nvl(DT_AGENDA_FIM,dt_agenda)))
			and 	dt_encaixe_p between to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and 	to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and 	((pkg_date_utils.get_weekday(dt_geracao_encaixe_p) = ie_dia_semana) or ((ie_dia_semana = '9') and (pkg_date_utils.is_business_day(dt_geracao_encaixe_p) = 1)) or (ie_dia_semana is null))
			and rownum = 1;
		exception
		when no_data_found then
			nr_sequencia_w := null;
		end;

		if (nr_sequencia_w is null) then
			begin
				select 	'N',
					nr_sequencia, 
					ie_classif_agenda
				into 	ie_tipo_turno_w,
					nr_sequencia_w,
					ds_classificacao_agenda_w
				from 	agenda_turno
				where 	cd_agenda = cd_agenda_p
				and 	trunc(nvl(dt_inicio_vigencia,sysdate)) <= trunc(sysdate) 
				and	trunc(nvl(dt_final_vigencia,sysdate)) >= trunc(sysdate)
				and 	dt_encaixe_p between to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
				and 	to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
				and 	((pkg_date_utils.get_weekday(dt_geracao_encaixe_p) = ie_dia_semana) or ((ie_dia_semana = '9') and (pkg_date_utils.is_business_day(dt_geracao_encaixe_p) = 1)))
				and 	rownum = 1;
			exception
			when no_data_found then
				nr_sequencia_w := null;
			end;
		end if;
		
		if (nvl(nr_sequencia_w,0) > 0) then
			if (ie_tipo_turno_w = 'N') then
				select 	count(1)
				into	qt_registros_w
				from 	agenda_turno_classif_lib
				where 	nr_seq_turno = nr_sequencia_w;
			else
				select 	count(1)
				into	qt_registros_w
				from 	agenda_turno_classif_lib
				where 	nr_seq_turno_esp = nr_sequencia_w;
			end if;
		end if;
		
		cd_classificacao_w	:= nvl(cd_classificacao_p,'E');	
		
		ds_classificacao_agenda_w := obter_classif_regra_alteracao(3, cd_agenda_p, dt_encaixe_p, ds_classificacao_agenda_w);

		if (qt_registros_w = 0)
		or ((ds_classificacao_agenda_w is null) and (cd_classificacao_w is null))
		or (ds_classificacao_agenda_w = cd_classificacao_w) then
			goto final;
		end if;
		
		if (ie_tipo_turno_w = 'N') then
			select 	count(1)
			into	qt_registros_w
			from 	agenda_turno_classif_lib
			where 	nr_seq_turno= nr_sequencia_w
			and 	ie_classif_agenda = cd_classificacao_w;
		else
			select 	count(1)
			into	qt_registros_w
			from 	agenda_turno_classif_lib
			where 	nr_seq_turno_esp = nr_sequencia_w
			and 	ie_classif_agenda = cd_classificacao_w;
		end if;
		
		if (qt_registros_w = 0) then
			ie_bloquear_w := 'S';
		end if;
    
    if (ie_bloquear_w = 'S') then

      select cd_estabelecimento
      into cd_estabelecimento_w
      from agenda
      where cd_agenda = cd_agenda_p;

      dt_geracao_enc_w := to_date(to_char(dt_geracao_encaixe_p,'ddmmyyyy')||' '||to_char(dt_encaixe_p,'hh24miss'),'ddmmyyyy hh24miss');
      ie_val_classif_lib_w := ageint_valida_regra_classif(cd_estabelecimento_w,cd_convenio_p,cd_agenda_p,cd_medico_p,dt_geracao_enc_w,ds_classificacao_agenda_w,cd_classificacao_p);

      if (ie_val_classif_lib_w = 'S') then
        ie_bloquear_w := 'N';
      end if;

    end if;	
		
	ELSIF (cd_tipo_agenda_p = 5) then
		begin
			select 	'N',
				nr_sequencia, 
				ie_classif_agenda
			into 	ie_tipo_turno_w,
				nr_sequencia_w,
				ds_classificacao_agenda_w
			from 	agenda_turno
			where 	cd_agenda = cd_agenda_p
			and 	trunc(nvl(dt_inicio_vigencia,sysdate)) <= trunc(sysdate) 
			and	trunc(nvl(dt_final_vigencia,sysdate)) >= trunc(sysdate)
			and 	dt_encaixe_p between to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and 	to_date(to_char(dt_encaixe_p,'dd/mm/yyyy')||' '||to_char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and 	((pkg_date_utils.get_weekday(dt_geracao_encaixe_p) = ie_dia_semana) or ((ie_dia_semana = '9') and (pkg_date_utils.is_business_day(dt_geracao_encaixe_p) = 1)))
			and 	rownum = 1;
		exception
		when no_data_found then
			nr_sequencia_w := null;
		end;

		if (nvl(nr_sequencia_w,0) > 0) then
			select 	count(1)
			into	qt_registros_w
			from 	agenda_turno_classif_lib
			where 	nr_seq_turno = nr_sequencia_w;
		end if;
		
		if (qt_registros_w = 0) then
			goto final;
		end if;
		
		cd_classificacao_w	:= cd_classificacao_p;
		
		if	(cd_classificacao_p is null) and
			(nvl(nr_sequencia_w,0) > 0) then
			select nvl(max(cd_classificacao), 'E')
			into	cd_classificacao_w
			from 	agenda_turno_classif
			where	nr_seq_turno	= nr_sequencia_w;
		end if;		
		
		ds_classificacao_agenda_w := obter_classif_regra_alteracao(5, cd_agenda_p, dt_encaixe_p, ds_classificacao_agenda_w);

		if (qt_registros_w = 0)
		or ((ds_classificacao_agenda_w is null) and (cd_classificacao_w is null))
		or (ds_classificacao_agenda_w = cd_classificacao_w) then
			goto final;
		end if;

		select 	count(1)
		into	qt_registros_w
		from 	agenda_turno_classif_lib
		where 	nr_seq_turno= nr_sequencia_w
		and 	ie_classif_agenda = cd_classificacao_w;
	
		if (qt_registros_w = 0) then
			ie_bloquear_w := 'S';
		end if;
	
	end if;
	
<<final>>
return	ie_bloquear_w;
END	Obter_Encaixe_classificacao;
/
