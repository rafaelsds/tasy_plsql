create or replace
function obter_data_final_servico_pad(	nr_seq_hc_pad_cad_p		number,
									cd_pessoa_fisica_p      number  ) return date is
									
dt_final_w			date;
dt_retorno_w           date;

begin

select max(a.dt_final)
into dt_final_w
from hc_pad_servico a,
     hc_pad_controle b,
     paciente_home_care c
where a.nr_seq_controle = b.nr_sequencia
and b.nr_seq_paciente = c.nr_sequencia
and b.nr_seq_hc_pad_cad = nr_seq_hc_pad_cad_p
and c.cd_pessoa_fisica = cd_pessoa_fisica_p;


if(dt_final_w is not null) then

	dt_retorno_w := dt_final_w;

end if;

return	dt_retorno_w;

end obter_data_final_servico_pad;
/