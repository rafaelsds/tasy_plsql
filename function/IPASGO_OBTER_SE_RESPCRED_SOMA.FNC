create or replace
function ipasgo_obter_se_respcred_soma(	ie_responsavel_credito_p	procedimento_paciente.ie_responsavel_credito%type)
 		    			return varchar2 is
			
ds_retorno_w		varchar2(15) 	:= 'N';
qt_resp_soma_w		number(10)	:= 0;

begin

select	count(1)
into	qt_resp_soma_w
from	regra_honorario
where	cd_regra = ie_responsavel_credito_p
and	ie_repassa_medico = 'N'
and	ie_entra_conta <> 'S'
and	rownum = 1;

if	(qt_resp_soma_w > 0) then
	ds_retorno_w := 'S';
end if;

return	nvl(ds_retorno_w,'N');

end ipasgo_obter_se_respcred_soma;
/