create or replace
function obter_se_solic_nao_integrado(	nr_solic_compra_p	in number) 
 		    	return varchar2 is
			
			

qt_itens_solic_w			number(5);
qt_itens_integrados_w			number(5);
qt_itens_nao_integrados_w		number(5);
ie_solic_possui_n_integrado_w		varchar(1) := 'N';	--Verifica se a solicita��o possui integrados e n�o integrados na mesma solicita��o;
			
begin

select	count(*)
into	qt_itens_solic_w
from	solic_compra_item
where 	nr_solic_compra = nr_solic_compra_p;

select	count(*)
into	qt_itens_integrados_w
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p
and	obter_se_integra_sc_item(nr_solic_compra_p,cd_material) = 'S';

select	count(*)
into	qt_itens_nao_integrados_w
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p
and	obter_se_integra_sc_item(nr_solic_compra_p,cd_material) = 'N';


if	(qt_itens_nao_integrados_w > 0) and
	(qt_itens_integrados_w > 0) then
	ie_solic_possui_n_integrado_w := 'S';
end if;

return	ie_solic_possui_n_integrado_w;

end obter_se_solic_nao_integrado;
/