create or replace
function cpoe_get_default_solubility(
							cd_material_p	material.cd_material%type) return number is

nr_seq_solubilidade_w		nutricao_leite_deriv_solub.nr_sequencia%type;

cursor c01 is
	select	a.nr_sequencia
	from	nutricao_leite_deriv_solub a,
			nutricao_leite_deriv b
	where	b.nr_sequencia = a.nr_seq_produto
	and		nvl(a.ie_situacao,'A') = 'A'
	and		b.cd_material = cd_material_p
	and		nvl(ie_padrao,'N') = 'S'
	and 	nvl(b.ie_situacao,'A') = 'A'
	order by a.nr_sequencia;
		
begin

if (cd_material_p is not null) then
	open c01;
	loop
	fetch c01 into nr_seq_solubilidade_w;
	exit when c01%notfound;
	end loop;
	close c01;
end if;

return nr_seq_solubilidade_w;

end cpoe_get_default_solubility;
/