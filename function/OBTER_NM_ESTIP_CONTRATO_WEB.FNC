create or replace
function obter_nm_estip_contrato_web
			(	nr_seq_segurado_p				number)
 		    		return Varchar2 is

nr_seq_congenere_w			number(10);
nr_seq_intercambio_w		number(10);
nr_seq_contrato_w			number(10);			
nm_estipulante_w				varchar2(255);

begin

begin
	select	nr_seq_congenere,
			nr_seq_intercambio,
			nr_seq_contrato
	into		nr_seq_congenere_w,
			nr_seq_intercambio_w,
			nr_seq_contrato_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;
exception 
when others then
	nr_seq_congenere_w := null;
	nr_seq_intercambio_w := null;
	nr_seq_contrato_w := null;
end;

if	(nr_seq_contrato_w is not null) then
	nm_estipulante_w := pls_obter_dados_contrato(nr_seq_contrato_w,'E');
elsif	(nr_seq_intercambio_w is not null) then
	nm_estipulante_w := pls_obter_dados_contrato_inter(nr_seq_intercambio_w,null,'E');
elsif	(nr_seq_congenere_w is not null) then
	nm_estipulante_w := pls_obter_nome_congenere(nr_seq_congenere_w);
end if;

return	nm_estipulante_w;

end obter_nm_estip_contrato_web;
/