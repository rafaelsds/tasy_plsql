create or replace 
function Obter_Tipo_Acomod_Data	(
			nr_atendimento_p		number,
			dt_referencia_p		date)
			return number is


cd_tipo_acomodacao_w		number(10,0)	:= null;

begin

if	(nvl(nr_atendimento_p,0) > 0) then
	select	nvl(max(cd_tipo_acomodacao),0)
	into	cd_tipo_acomodacao_w
	from 	atend_paciente_unidade a
	where	a.nr_atendimento 		= nr_atendimento_p
	and	nr_seq_interno	= 
		(select	max(nvl(nr_seq_interno,0))
		from 	atend_paciente_unidade b
		where	nr_atendimento 	= nr_atendimento_p
		and	nvl(b.dt_saida_unidade, b.dt_entrada_unidade + 9999) <= dt_referencia_p);
end if;
	
return cd_tipo_acomodacao_w;

end Obter_Tipo_Acomod_Data;
/