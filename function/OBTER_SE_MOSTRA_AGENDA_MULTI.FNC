create or replace
function Obter_Se_Mostra_Agenda_Multi ( nr_seq_agenda_p			number,
					ie_tipo_profissional_p		varchar2)
					return				varchar2 is

ie_retorno_w		varchar2(1)	:= 'N';
qt_registro_w		number(10,0)	:= 0;

begin

begin
select	count(*)
into	qt_registro_w
from	agenda_consulta_prof
where	nr_seq_agenda		= nr_seq_agenda_p
and	ie_tipo_profissional	= ie_tipo_profissional_p;
exception
	when others then
	qt_registro_w		:= 0;
end;

if	(qt_registro_w > 0) then

	ie_retorno_w		:= 'S';

end if;

return ie_retorno_w;

end Obter_Se_Mostra_Agenda_Multi;
/