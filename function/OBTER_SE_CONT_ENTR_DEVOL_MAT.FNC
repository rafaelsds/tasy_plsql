create or replace
function obter_se_cont_entr_devol_mat (		cd_material_p	number)
						return varchar2 is

ie_retorno_w			varchar2(1);
cd_classe_material_w		NUMBER(5);
cd_subgrupo_material_w		NUMBER(3);
cd_grupo_material_w		NUMBER(3);
cd_perfil_ativo_w		number(10);

begin

cd_perfil_ativo_w	:= substr(obter_perfil_ativo,1,10);

if	(nvl(cd_material_p,0) > 0) then

	SELECT	MAX(cd_classe_material),
		MAX(cd_subgrupo_material),
		MAX(cd_grupo_material)
	INTO	cd_classe_material_w,
		cd_subgrupo_material_w,
		cd_grupo_material_w
	FROM	estrutura_material_v
	WHERE	cd_material	= cd_material_p;
	
end if;

select	nvl(max('S'),'N')
into	ie_retorno_w
from	controle_entrega_devol_mat
where	nvl(cd_classe_material,nvl(cd_classe_material_w,0))  	   = nvl(cd_classe_material_w,0)
and	nvl(cd_subgrupo_material,nvl(cd_subgrupo_material_w,0))    = nvl(cd_subgrupo_material_w,0)
and	nvl(cd_grupo_material,nvl(cd_grupo_material_w,0))	   = nvl(cd_grupo_material_w,0)
and	nvl(cd_perfil_ativo,nvl(cd_perfil_ativo_w,0))	  	   = nvl(cd_perfil_ativo_w,0)
and	nvl(cd_material,nvl(cd_material_p,0))			   = nvl(cd_material_p,0);

return	ie_retorno_w;

end obter_se_cont_entr_devol_mat;
/