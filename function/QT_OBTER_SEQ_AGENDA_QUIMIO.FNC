create or replace function qt_obter_seq_agenda_quimio (nr_seq_atendimento_p paciente_atendimento.nr_seq_atendimento%type) 
                                                          return agenda_quimio.nr_sequencia%type is

nr_sequencia_w agenda_quimio.nr_sequencia%type;

begin

if (nr_seq_atendimento_p is not null) then

  select max (nr_sequencia)
  into nr_sequencia_w
  from agenda_quimio 
  where nr_seq_atendimento = nr_seq_atendimento_p 
  and dt_cancelada is null;

end if;

return nr_sequencia_w;

end qt_obter_seq_agenda_quimio;
/
