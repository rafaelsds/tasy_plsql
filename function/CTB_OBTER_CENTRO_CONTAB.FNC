create or replace
function ctb_obter_centro_contab(	cd_estab_origem_p	number,
					cd_centro_origem_p	number,
					cd_estab_destino_p	number)
				return number is
				
cd_centro_retorno_w		number(10);

begin

select	nvl(max(b.cd_centro_destino), cd_centro_origem_p)
into	cd_centro_retorno_w
from	centro_custo a,
	regra_centro_estab_contab b
where	a.cd_centro_custo	= b.cd_centro_custo
and	a.cd_estabelecimento	= cd_estab_origem_p
and	b.cd_centro_custo	= cd_centro_origem_p;

return	cd_centro_retorno_w;

end ctb_obter_centro_contab;
/