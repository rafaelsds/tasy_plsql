create or replace
function pls_obter_regra_bloq_opme
			(	cd_guia_p	Varchar2)
				return Varchar2 is

dt_solicitacao_w		Date;

ie_tipo_guia_w			Varchar2(4);

nr_seq_prestador_w		Number(10);
nr_seq_regra_w			Number(10);
nr_seq_guia_w			Number(10);
nr_retorno_w			Varchar2(10)	:= '0';

begin

select	max(nr_sequencia)
into	nr_seq_guia_w
from	pls_guia_plano
where	cd_guia = cd_guia_p;

if	(nr_seq_guia_w	is not null) then
	select	ie_tipo_guia,
		nr_seq_prestador,
		dt_solicitacao
	into	ie_tipo_guia_w,
		nr_seq_prestador_w,
		dt_solicitacao_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_w;
	
	if	(pls_obter_se_controle_estab('RE') = 'S') then
		select	max(nr_sequencia)
		into	nr_seq_regra_w
		from	pls_bloqueio_solic_mat_med
		where	(ie_tipo_guia		is null	or ie_tipo_guia		= ie_tipo_guia_w)
		and	(nr_seq_prestador	is null	or nr_seq_prestador	= nr_seq_prestador_w)
		and	(nr_seq_grupo_prestador	is null	or pls_obter_se_prestador_grupo(nr_seq_grupo_prestador, nr_seq_prestador_w) = 'S')
		and	dt_solicitacao_w	between (dt_inicio_vigencia) 	and (fim_dia(nvl(dt_fim_vigencia, sysdate)))
		and 	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;
	else
		select	max(nr_sequencia)
		into	nr_seq_regra_w
		from	pls_bloqueio_solic_mat_med
		where	(ie_tipo_guia		is null	or ie_tipo_guia		= ie_tipo_guia_w)
		and	(nr_seq_prestador	is null	or nr_seq_prestador	= nr_seq_prestador_w)
		and	(nr_seq_grupo_prestador	is null	or pls_obter_se_prestador_grupo(nr_seq_grupo_prestador, nr_seq_prestador_w) = 'S')
		and	dt_solicitacao_w	between (dt_inicio_vigencia) 	and (fim_dia(nvl(dt_fim_vigencia, sysdate)));
	end if;
	
	if	(nr_seq_regra_w	is not null) then
		nr_retorno_w	:= nr_seq_regra_w;
	end if;
end if;

return	nr_retorno_w;

end pls_obter_regra_bloq_opme;
/
