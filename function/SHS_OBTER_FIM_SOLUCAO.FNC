create or replace
function shs_obter_fim_solucao(
					nr_atendimento_p	number,
					nr_prescricao_p		number,
					nr_seq_solucao_p	number,
					ie_tipo_solucao_p	varchar2) return date is
					
dt_evento_w		date;
begin

select	max(dt_evento)
into	dt_evento_w
from	adep_solucao_v
where	nr_atendimento 	= nr_atendimento_p
and		ie_tipo_solucao = ie_tipo_solucao_p
and		nr_prescricao = nr_prescricao_p
and		nr_seq_solucao = nr_seq_solucao_p
and		((nvl(ie_status,'N') = 'I') or
		 (nvl(ie_status,'N') = 'T'))
and		nr_seq_evento = obter_evento_adep_solucao(ie_tipo_solucao,nr_prescricao,nr_seq_solucao,'T');

return dt_evento_w;

end shs_obter_fim_solucao;
/