create or replace
function Obter_competencia_apac(nr_interno_conta_p	number) 
				return Date is

dt_competencia_w	Date;

begin

begin
select	dt_competencia
into	dt_competencia_w
from	sus_apac_movto
where	nr_interno_conta = nr_interno_conta_p;
exception
	when others then
		dt_competencia_w	:= null;
end;

return	dt_competencia_w;

end;
/