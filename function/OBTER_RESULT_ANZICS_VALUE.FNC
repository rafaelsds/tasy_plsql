create or replace function obter_result_anzics_value (	NR_SEQ_ATEND_ANZICS_P	number,
                                                       NR_SEQ_ANZICS_ITEM_P number)

					return varchar is

ds_retorno_w	varchar(255);



begin


if	(NR_SEQ_ATEND_ANZICS_P is not null and  NR_SEQ_ANZICS_ITEM_P is not null ) then

select	max(ds_value) into ds_retorno_w
from	atend_anzics_result
where	NR_SEQ_ATEND_ANZICS = NR_SEQ_ATEND_ANZICS_P
and	NR_SEQ_ANZICS_ITEM = NR_SEQ_ANZICS_ITEM_P;

  
end if;

return ds_retorno_w;


end obter_result_anzics_value;
/