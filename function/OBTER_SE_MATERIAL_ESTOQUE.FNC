create or replace
function obter_se_material_estoque(
			cd_estabelecimento_p	number,
			cd_estabelecimento_base_p	number,
			cd_material_p		number) 
			return varchar is

ds_retorno_w	Varchar2(1);
begin
select	nvl(max(ie_material_estoque),'N')
into	ds_retorno_w
from	material_estab
where	cd_material = cd_material_p
and	cd_estabelecimento = cd_estabelecimento_p;

return ds_retorno_w;
end obter_se_material_estoque;
/