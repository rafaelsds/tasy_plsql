create or replace
function pls_obter_dado_acao_fat_geral (	nr_seq_regra_p	in pls_regra_tit_fat_geral.nr_sequencia%type,
						ie_opcao_p	in varchar2) return varchar2 as
/*	ie_opcao_p
		VCN	- Vincular o A580 em um titulo de c�mara de compensa��o
*/
						
ds_retorno_w	varchar2(500) :='';
begin

if	(ie_opcao_p = 'VCN') then

	select	nvl(max(a.ie_consiste_tit_camara), 'N')
	into	ds_retorno_w
	from	pls_regra_tit_fat_geral	a
	where	a.nr_sequencia			= nr_seq_regra_p;
end if;

return ds_retorno_w;
	
end pls_obter_dado_acao_fat_geral;
/