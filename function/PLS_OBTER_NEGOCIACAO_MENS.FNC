create or replace
function pls_obter_negociacao_mens
			(	nr_titulo_p		number)
				return number is 

nr_retorno_w			number(10);
	
begin

select	max(a.nr_seq_negociacao)
into	nr_retorno_w
from	negociacao_cr		b,
	titulo_rec_negociado	a
where	a.nr_seq_negociacao	= b.nr_sequencia
and	b.ie_status		= 'L'
and	a.nr_titulo 		= nr_titulo_p;	

return	nr_retorno_w;

end pls_obter_negociacao_mens;
/