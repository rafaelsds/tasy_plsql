create or replace
function obter_bonificacao_oci(	nr_ordem_compra_p		number,
				nr_item_oci_p			number,
				cd_estabelecimento_p		number)
				return varchar2 is

ie_bonificacao_w		varchar2(1) := 'N';
nr_cot_compra_w			number(10);
nr_item_cot_compra_w		number(5);
qt_registros_w			number(5);

cursor c01 is
select	b.nr_cot_compra,
	b.nr_item_cot_compra
from	ordem_compra_item b,
	ordem_compra a
where	b.nr_ordem_compra = nr_ordem_compra_p
and	b.nr_item_oci = nr_item_oci_p
and	b.nr_ordem_compra = a.nr_ordem_compra
and	a.cd_estabelecimento = cd_estabelecimento_p;

begin

open c01;
loop
fetch c01 into	
	nr_cot_compra_w,
	nr_item_cot_compra_w;
exit when c01%notfound;
	begin

	select	count(*)
	into	qt_registros_w
	from	cot_compra_forn a,
		cot_compra_forn_item b,
		cot_compra_forn_item_bon c
	where	a.nr_sequencia	= b.nr_seq_cot_forn
	and	b.nr_sequencia	= c.nr_seq_cot_item_forn
	and	a.nr_cot_compra	= nr_cot_compra_w
	and	b.nr_item_cot_compra = nr_item_cot_compra_w
	and	obter_oc_item_cotacao(nr_cot_compra_w, nr_item_cot_compra_w, c.cd_material) = nr_ordem_compra_p;

	if	(qt_registros_w > 0) then
		ie_bonificacao_w := 'S';
	end if;
	end;
end loop;
close c01;

return	ie_bonificacao_w;

end obter_bonificacao_oci;
/