create or replace
function hc_obter_equipe_agenda (	nr_seq_agenda_p		Number)
 		    	return Number is
			
nr_seq_equipe_w		Number(10);

begin

if	( nr_seq_agenda_p is not null) then
	begin
	
	select	max(nr_seq_equipe_hc)
	into	nr_seq_equipe_w
	from	hc_agenda_prof
	where	nr_seq_agenda = nr_seq_agenda_p ;	
	
	end;
end if;
	
return	nr_seq_equipe_w;

end hc_obter_equipe_agenda;
/