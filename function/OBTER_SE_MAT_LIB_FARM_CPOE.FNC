create or replace
function obter_se_mat_lib_farm_cpoe(nr_seq_mat_cpoe_p cpoe_material.nr_sequencia%type)
			return varchar2 is

ie_liberado_w		varchar2(1 char);

begin
	begin
		select	decode(dt_liberacao_farm, null, 'N', 'S')
		into	ie_liberado_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_mat_cpoe_p
		and		(nvl(dt_lib_suspensao, dt_suspensao) is null or nvl(dt_lib_suspensao, dt_suspensao) > sysdate);
	exception
		when no_data_found then
			ie_liberado_w := null;
	end;

	return	ie_liberado_w;
end obter_se_mat_lib_farm_cpoe;
/
