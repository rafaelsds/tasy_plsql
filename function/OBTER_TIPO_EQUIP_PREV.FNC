CREATE OR REPLACE
FUNCTION Obter_Tipo_Equip_Prev(
					nr_sequencia_p	Number)
					RETURN Number IS

qt_prev_w				Number(15,0);
cd_estabelecimento_w			number(5,0) := Wheb_usuario_pck.get_cd_estabelecimento;
BEGIN

select count(*)
into qt_prev_w
from man_planej_prev
where nr_seq_tipo_equip = nr_sequencia_p
and   ((cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento is null));

RETURN qt_prev_w;
END Obter_Tipo_Equip_Prev;
/
