create or replace
function man_tipo_evento_regra(	nr_sequencia_p		number)
				return varchar2 is

ie_evento_w	varchar2(2);

begin
select	ie_evento
into	ie_evento_w
from	man_regra_envio_comunic
where	nr_sequencia = nr_sequencia_p;

return ie_evento_w;
end man_tipo_evento_regra;
/