create or replace
function obter_desc_curta_acomod(cd_tipo_acomod_p		number)
 		    	return varchar2 is

retorno_w		varchar2(255);
			
begin

select	max(DS_CURTA_ACOMOD)
into	retorno_w
from	TIPO_ACOMODACAO
where	cd_tipo_acomodacao = cd_tipo_acomod_p;

return	retorno_w;

end obter_desc_curta_acomod;
/