create or replace
function sup_obter_desc_tipo_aval_mat(nr_sequencia_p	number)
 		    	return varchar2 is

ds_tipo_avaliacao_w		mat_tipo_avaliacao.ds_tipo_avaliacao%type;

begin

ds_tipo_avaliacao_w := '';

if	(nr_sequencia_p > 0) then
	select	substr(ds_tipo_avaliacao,1,80)
	into	ds_tipo_avaliacao_w
	from	mat_tipo_avaliacao
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_tipo_avaliacao_w;

end sup_obter_desc_tipo_aval_mat;
/
