create or replace
function pls_obter_medico_conselho
			(	nr_conselho_p		Varchar2,
				nr_seq_conselho_p	Varchar2)
				return Varchar2 is

cd_retorno_w			Varchar2(255);

begin

select	max(a.cd_pessoa_fisica)
into	cd_retorno_w
from	pessoa_fisica		a,
	medico			b
where	a.nr_seq_conselho	= nr_seq_conselho_p
and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.nr_crm 		= nr_conselho_p
and	b.ie_situacao		<> 'I';

return	cd_retorno_w;

end pls_obter_medico_conselho;
/