create or replace
function obter_consumo_dias( cd_estabelecimento_p		number,
			cd_material_p			number,
			dt_mesano_referencia_p		date,
			dt_inicial_p			date,
			qt_dias_passados_p		number)
		return number is

ds_retorno_w			number(18,4);

begin

select	sum(qt_consumo) qt_movimento
into	ds_retorno_w
from  	movimento_estoque_v a,
      	material m
where 	a.cd_material_estoque = m.cd_material_estoque
and 	m.cd_material          = cd_material_p
and 	dt_mesano_referencia   = dt_mesano_referencia_p
and 	a.cd_estabelecimento   = cd_estabelecimento_p
and 	cd_centro_custo     is not null
and 	dt_movimento_estoque   between trunc(dt_inicial_p,'dd') - qt_dias_passados_p and dt_inicial_p;

return  ds_retorno_w;

end obter_consumo_dias;
/