create or replace
function obter_se_item_disp(cd_estabelecimento_p	in usuario.cd_estabelecimento%type,
							cd_material_p			in material.cd_material%type)
					return varchar2 is

ie_retorno_w		varchar2(1 char);
ie_disp_mercado_w	material.ie_disponivel_mercado%type;

begin

	ie_retorno_w := 'S';

	if (cd_material_p is not null) then

		ie_disp_mercado_w := nvl(obter_se_material_disponivel(cd_material_p), 'S');

		if (ie_disp_mercado_w = 'N') then

			ie_retorno_w := 'N';

		elsif (ie_disp_mercado_w = 'E') then

			if (nvl(obter_se_material_padronizado(cd_estabelecimento_p, cd_material_p), 'N') = 'S') then

				if (nvl(obter_saldo_disp_estoque(cd_estabelecimento_p, cd_material_p, null, sysdate), 0) = 0) then

					ie_retorno_w := 'N';

				end if;

			end if;

		end if;

	end if;

	return ie_retorno_w;

end obter_se_item_disp;
/
