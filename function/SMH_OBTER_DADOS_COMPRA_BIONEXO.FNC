create or replace
function SMH_obter_dados_compra_bionexo(	nr_cot_compra_p			number,
						nr_item_cot_compra_p		number,
						cd_cnpj_p			varchar2,
						ie_opcao_p			varchar2)
 		    	return Varchar2 is

/*
ie_opcao_p
1 - Identifica se � compra Direta ou pela Bionexo
2 - Descricao do tipo de integra��o de envio
3 - Descri��o do tipo de integra��o de recebimento*/			
			
ds_retorno_w			varchar2(255);
qt_registros_w			number(10);	
	
begin

if	(ie_opcao_p = '1') then

	select	count(*)
	into	qt_registros_w
	from	cot_compra_forn a,
		cot_compra_forn_item b
	where	a.nr_sequencia = b.nr_seq_cot_forn
	and	a.nr_cot_compra = nr_cot_compra_p
	and	b.nr_item_cot_compra = nr_item_cot_compra_p
	and	a.cd_cgc_fornecedor = cd_cnpj_p
	and	b.nr_id_integracao is not null;

	if	(qt_registros_w > 0) then
		ds_retorno_w := 'Bionexo';
	else
		ds_retorno_w := 'Direta';
	end if;
elsif	(ie_opcao_p = '2') then
	select	substr(obter_valor_dominio(2585,IE_TIPO_INTEGRACAO_ENVIO),1,255)
	into	ds_retorno_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_p;
elsif	(ie_opcao_p = '3') then
	select	substr(obter_valor_dominio(2585,IE_TIPO_INTEGRACAO_RECEB),1,255)
	into	ds_retorno_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_p;
end if;

return	ds_retorno_w;

end SMH_obter_dados_compra_bionexo;
/