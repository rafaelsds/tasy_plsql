create or replace
function consiste_titulo_nf(nr_sequencia_nf_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1) := 'N';
nr_titulo_w		titulo_pagar.nr_titulo%type;
ie_consistido_w		varchar2(1) := 'N';
vl_saldo_titulo_w	titulo_receber.vl_saldo_titulo%type;
vl_titulo_w		titulo_receber.vl_titulo%type;
count_w			number(10);

Cursor C01 is -- Cursor para buscar todos os t�tulos a receber que est�o vinculados a essa NF
	select	a.nr_titulo
	from	titulo_receber a
	where	a.nr_seq_nf_saida = nr_sequencia_nf_p;
begin

if (nr_sequencia_nf_p > 0) then 

	open C01;
	loop
	fetch C01 into	
		nr_titulo_w;
	exit when C01%notfound;
		begin
		
		if ( ie_consistido_w = 'N' ) then
		
			select	a.vl_saldo_titulo,
				a.vl_titulo
			into	vl_saldo_titulo_w,
				vl_titulo_w
			from	titulo_receber a
			where	a.nr_titulo = nr_titulo_w;
	
			if ( vl_saldo_titulo_w <> vl_titulo_w ) then -- Consiste pois saldo � diferente do valor do titulo.
				ds_retorno_w 	:= 'S';
				ie_consistido_w := 'S';
			else  
				select	count(*)
				into	count_w
				from	titulo_receber_liq a
				where   a.nr_titulo = nr_titulo_w
				and     a.vl_recebido > 0
				and     not exists ( select 1
						     from   titulo_receber_liq x
						     where  x.nr_seq_liq_origem	= a.nr_sequencia
						     and    x.nr_titulo		= a.nr_titulo )
				and     a.nr_seq_liq_origem is null;
				
				if (count_w > 0) then --Possui baixas que n�o est�o estornadas, deve consistir

					ds_retorno_w 	:= 'S';
					ie_consistido_w := 'S';
				end if;
			end if;
		
		end if;
		
		end;
	end loop;
	close C01;
	
end if;

return	ds_retorno_w;

end consiste_titulo_nf;
/