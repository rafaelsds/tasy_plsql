create or replace
function obter_desc_qualidade_refeicao	(nr_seq_qualidade_p	number)
						return varchar2 is

ds_qualidade_w	varchar2(60);
begin
if	(nr_seq_qualidade_p is not null) then
	select	ds_qualidade
	into	ds_qualidade_w
	from	adep_qualidade_refeicao
	where	nr_sequencia = nr_seq_qualidade_p;
end if;

return ds_qualidade_w;

end obter_desc_qualidade_refeicao;
/