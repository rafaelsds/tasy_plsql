create or replace
function obter_desc_escala_dor(	cd_escala_dor_p		varchar2) return varchar2 is

ds_retorno_w		varchar2(255);

begin

if	(cd_escala_dor_p	is not null) then

	ds_retorno_w	:= substr(obter_valor_dominio(1298,cd_escala_dor_p),1,60);

	if	(ds_retorno_w is null) then
		select	ds_escala_dor
		into	ds_retorno_w
		from	escala_dor
		where	cd_escala_dor	= cd_escala_dor_p;
	end if;


end if;

return	ds_retorno_w;

end obter_desc_escala_dor;
/
