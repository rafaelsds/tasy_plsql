create or replace
function obter_sql_painel_datas(	nr_seq_indicador_p	number,
					dt_inicio_p		date,
					dt_fim_p		date)
					return	varchar2 is

nm_atributo_w		varchar2(85);
ds_sql_informacao_w	varchar2(2000);
ds_sql_dimensao_w	varchar2(85);
ds_dimensao_w		varchar2(85);
ds_sql_w		varchar2(4000);
ds_tabela_visao_w	varchar2(50);
ds_order_w		varchar2(100);
ds_sql_where_w		varchar2(2000);
ds_data_w		varchar2(200);
qt_atributo_w		Number(03,0) := 0;
IE_CLASSIFICACAO_ORDEM_w varchar2(1);
group_by_w		varchar2(500);

cursor C01 is
	select		nm_atributo,
			IE_CLASSIFICACAO_ORDEM
	from		indicador_gestao_atrib
	where		nr_seq_ind_gestao	=	nr_seq_indicador_p
	and		ds_informacao is not null;

begin

OPEN C01;
LOOP
FETCH C01 into
	nm_atributo_w,
	IE_CLASSIFICACAO_ORDEM_w;
exit when C01%notfound;
	begin
	   if (IE_CLASSIFICACAO_ORDEM_w <> 'T') then  
		ds_sql_informacao_w	:=	ds_sql_informacao_w || ', ' || 'sum(' || nm_atributo_w || ') ' || nm_atributo_w;
	   end if;
	   
	   if (IE_CLASSIFICACAO_ORDEM_w = 'T') then  
		ds_sql_informacao_w	:=	ds_sql_informacao_w || ', ' || nm_atributo_w || ' ' || nm_atributo_w;
		group_by_w := group_by_w || ', ' || nm_atributo_w;
	   end if;
	   
	end;
END LOOP;
CLOSE C01;

select	nm_atributo,
	decode(ie_classificacao_ordem, 'D', '1'||decode(ie_tipo_ordem_classif,'A', ' Asc ', ' Desc ') ||',2' , '2 '||decode(ie_tipo_ordem_classif,'A', ' Asc ', ' Desc ') ||',1')
into	ds_dimensao_w,
	ds_order_w
from	indicador_gestao_atrib
where	nr_seq_ind_gestao	=	nr_seq_indicador_p
and	ds_dimensao is not null;


select	ds_tabela_visao,
	ds_sql_where
into	ds_tabela_visao_w,
	ds_sql_where_w
from	indicador_gestao
where	nr_sequencia		=	nr_seq_indicador_p;

select	count(*)
into	qt_atributo_w
from	user_tab_columns
where	upper(table_name) = upper(trim(ds_tabela_visao_w))
and	column_name = 'DT_REFERENCIA';

if	(qt_atributo_w > 0) then
	ds_data_w	:= ' and dt_referencia between  to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') and ' || 
						       'to_date(' || chr(39) || to_char(dt_fim_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ')';
	ds_sql_where_w	:= ds_sql_where_w || ds_data_w;
end if;

if	(substr(ds_dimensao_w,1,3) = 'DT_') then
	ds_sql_dimensao_w	:=	ds_dimensao_w;	
else
	ds_sql_dimensao_w	:=	'substr(' || ds_dimensao_w || ',1,255)'|| ds_dimensao_w;
end if;	

ds_sql_w	:= 	' select '	|| ds_sql_dimensao_w	|| ds_sql_informacao_w || 
			' from ' 	|| ds_tabela_visao_w	|| 
			' where 1 = 1 ' || ds_sql_where_w	||  
			' group by ' 	|| ds_dimensao_w 	|| group_by_w || chr(13) || 
			' order by ' 	|| ds_order_w;


return ds_sql_w;

end obter_sql_painel_datas;
/