create or replace
function OBTER_CHEQUE_TITULO_PAGAR	(nr_titulo_p	number) return varchar2 is

nr_cheque_w		varchar2(20);
ds_cheques_w		varchar2(254);

Cursor c01 is
	select	distinct a.nr_cheque
	from	cheque b,
		CHEQUE_BORDERO_TITULO a
	where	a.nr_seq_cheque	= b.nr_sequencia
	and	b.dt_cancelamento is null
	and	a.nr_titulo	= nr_titulo_p;

begin

open c01;
loop
fetch c01 into
	nr_cheque_w;
exit	when c01%notfound;

	if	(length(ds_cheques_w || nr_cheque_w) < 250) then
		if	(ds_cheques_w is not null) then
			ds_cheques_w := ds_cheques_w || ',';
		end if;
		ds_cheques_w := substr(ds_cheques_w || nr_cheque_w,1,254);
	end if;	

end loop;
close c01;

return	substr(ds_cheques_w,1,254);

end;
/
