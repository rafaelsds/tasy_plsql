create or replace
function obter_qt_oci_trans_nota(
			nr_ordem_compra_p	number,
			nr_item_oci_p		number,
			ie_entrada_saida_p	varchar2)
		return number is

qt_material_w		number(13,4);
ie_unidade_medida_w	varchar2(3);
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;

begin
select	decode(b.cd_unidade_medida_compra,substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_w,'UME'),1,30), 'UME','UMP')
into	ie_unidade_medida_w
from	ordem_compra_item b,
	material a
where	a.cd_material = b.cd_material
and	b.nr_ordem_compra = nr_ordem_compra_p
and	b.nr_item_oci = nr_item_oci_p;

if (ie_entrada_saida_p = 'S') then
	select	nvl(sum(a.qt_item_nf),0)
	into	qt_material_w
	from	nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_ordem_compra = nr_ordem_compra_p
	and		a.nr_item_oci	= nr_item_oci_p
	and		b.nr_sequencia = a.nr_sequencia
	and		b.ie_situacao = '1'	
	and		obter_se_nota_entrada_saida(a.nr_sequencia) = 'S';
else	
	select	nvl(sum(a.qt_item_nf),0)
	into	qt_material_w
	from	nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_ordem_compra = nr_ordem_compra_p
	and		a.nr_item_oci	= nr_item_oci_p
	and		b.nr_sequencia = a.nr_sequencia
	and		b.ie_situacao = '1'		
	and		obter_se_nota_entrada_saida(a.nr_sequencia) = 'E';
end if;

return	qt_material_w;

end obter_qt_oci_trans_nota;
/
