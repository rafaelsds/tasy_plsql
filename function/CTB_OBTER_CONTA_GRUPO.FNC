create or replace
FUNCTION ctb_obter_conta_grupo(	cd_conta_contabil_p	VARCHAR2)
				RETURN VARCHAR2 IS

cd_grupo_w		NUMBER(10);
vl_resultado_w		number(10);

BEGIN

SELECT	MAX(cd_grupo)
INTO	cd_grupo_w
FROM	conta_contabil
WHERE	cd_conta_contabil = cd_conta_contabil_p;


vl_resultado_w	:= NVL(cd_grupo_w,0);

RETURN vl_resultado_w;

END ctb_obter_conta_grupo;
/