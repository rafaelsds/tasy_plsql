create or replace
function Obter_Setor_Exec_Item
		(nr_seq_item_p		number,
		ie_tipo_item_p		number)
		return varchar2 is

ds_setor_atendimento_w		varchar2(100);

begin

if	(ie_tipo_item_p		= 1) then
	select	substr(obter_nome_setor(cd_setor_atendimento),1,100)
	into	ds_setor_atendimento_w
	from	material_atend_paciente
	where	nr_sequencia	= nr_seq_item_p;
else
	select	substr(obter_nome_setor(cd_setor_atendimento),1,100)
	into	ds_setor_atendimento_w
	from	procedimento_paciente
	where	nr_sequencia	= nr_seq_item_p;
end if;

return	ds_setor_atendimento_w;

end Obter_Setor_Exec_Item;
/