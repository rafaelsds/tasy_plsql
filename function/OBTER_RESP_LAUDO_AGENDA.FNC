create or replace
function Obter_resp_laudo_agenda(
		nr_seq_agenda_p	number,
		nr_atendimento_p	Number) 
		return Varchar2 is

ds_retorno_w		Varchar2(60);
nr_sequencia_w	Number(10,0);
dt_liberacao_w	date;

BEGIN


if	(nr_atendimento_p is not null) then
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	laudo_paciente a
	where	nr_atendimento	= nr_atendimento_p
	and	nr_prescricao		= 
		(select max(a.nr_prescricao)
		from	prescr_procedimento b,
			prescr_medica a
		where	a.nr_prescricao	= b.nr_prescricao
		and	a.nr_atendimento	= nr_atendimento_p
		and	b.cd_procedimento	= 
			(select cd_procedimento
			from agenda_paciente
			where	nr_sequencia	= nr_seq_agenda_p));
	select	max(dt_liberacao)
	into	dt_liberacao_w
	from	laudo_paciente
	where	nr_sequencia	= nr_sequencia_w;

	if	(nr_sequencia_w is not null) then
		select	max(substr(obter_nome_pf(b.cd_pessoa_fisica),1,60))
		into	ds_retorno_w	
		from	laudo_paciente a,
				pessoa_fisica b
		where	a.cd_medico_Resp	= b.cd_pessoa_fisica		
		and		a.nr_sequencia		= nr_sequencia_w;
	end if;	
	
end if;

return ds_retorno_w;

END;
/