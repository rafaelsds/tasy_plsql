create or replace
function Obter_se_jejum_valido (dt_inicio_p	date,
				dt_prim_p	date,
				dt_fim_p	date)
 		    	return varchar2 is

ie_valido_w	varchar2(1) := 'S';
			
begin

if	(dt_inicio_p >= dt_prim_p) and
	(dt_inicio_p <= dt_fim_p) then
	ie_valido_w	:= 'S';
else
	ie_valido_w	:= 'N';
end if;

return	ie_valido_w;

end Obter_se_jejum_valido;
/