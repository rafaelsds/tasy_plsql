create or replace
function pls_obter_tipo_acomod_segurado
			(	nr_seq_segurado_p		number)
				return number is

nr_seq_acomodacao_w		number(10);
nr_seq_plano_w			number(10);
begin
select	max(b.nr_sequencia)
into	nr_seq_plano_w
from	pls_segurado	a,
	pls_plano	b
where	b.nr_sequencia	= a.nr_seq_plano
and	a.nr_sequencia	= nr_seq_segurado_p;

select	pls_obter_dados_produto(nr_seq_plano_w,'AP')
into	nr_seq_acomodacao_w
from	dual;

return	nr_seq_acomodacao_w;

end pls_obter_tipo_acomod_segurado;
/