create or replace
function obter_protocolo_pac_atend(nr_seq_atendimento_p		number)
 		    	return varchar2 is

ds_protocolo_w	varchar2(255);

begin
if 	(nr_seq_atendimento_p	 is not null) then 
	SELECT	MAX(NVL(SUBSTR(obter_desc_protocolo_medic(a.nr_seq_medicacao, a.cd_protocolo),1,50), ds_protocolo_livre))
	INTO    ds_protocolo_w
	FROM	paciente_setor a,
		paciente_atendimento b
	WHERE	a.nr_seq_paciente	= b.nr_seq_paciente
	AND 	b.nr_seq_paciente 	= nr_seq_atendimento_p
	AND     NVL(a.ie_suporte, 'N') 	= 'N';
end if;

return	ds_protocolo_w;

end obter_protocolo_pac_atend;
/