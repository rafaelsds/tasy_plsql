create or replace
function obter_se_exame_complementar (	nr_prescricao_p	number,
					nr_seq_prescr_p number)
 		    	return varchar2 is
ie_retorno_w	varchar2(1) := 'N';
begin

select  decode(count(*),0,'N','S')
into	ie_retorno_w
from	prescr_proc_exam_compl
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_prescr	= nr_seq_prescr_p;

return	ie_retorno_w;

end obter_se_exame_complementar;
/