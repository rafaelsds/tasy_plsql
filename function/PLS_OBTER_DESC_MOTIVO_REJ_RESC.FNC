create or replace
function pls_obter_desc_motivo_rej_resc
			(	nr_seq_motivo_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	max(ds_motivo)
into	ds_retorno_w
from	pls_motivo_rejeicao_resc
where	nr_sequencia	= nr_seq_motivo_p;

return	ds_retorno_w;

end pls_obter_desc_motivo_rej_resc;
/