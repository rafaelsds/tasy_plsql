create or replace
function obter_item_xsd
			(nr_seq_projeto_p		xml_projeto.nr_sequencia%type,
			 nm_item_p				xsd_item.nm_item%type) return varchar2 is

nr_seq_item_w	xsd_item.nr_sequencia%type;

begin

select	max(nr_sequencia)
into	nr_seq_item_w
from	xsd_item
where	nm_item 			= nm_item_p
and		nr_seq_projeto_xml	= nr_seq_projeto_p;

return	nr_seq_item_w;

end obter_item_xsd;
/
