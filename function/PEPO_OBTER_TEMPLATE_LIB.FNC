create or replace
function pepo_obter_template_lib(	nr_seq_item_pepo_p	number )
					return number is

nr_sequencia_w		number(10,0);
nr_seq_template_w	number(10,0);
					
begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	perfil_item_pepo
where	nr_seq_item_pepo	=	nr_seq_item_pepo_p
and	cd_perfil		=	obter_perfil_ativo;

if	(nr_sequencia_w > 0) then
	select	max(nr_seq_template)
	into	nr_seq_template_w
	from	perfil_item_pepo_template
	where	nr_seq_item_perfil	=	nr_sequencia_w;
end if;

return	nr_seq_template_w;

end pepo_obter_template_lib;
/