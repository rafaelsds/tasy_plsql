create or replace
function obter_atendimento_pt(
		nr_atendimento_p		number,
		cd_pessoa_fisica_p		varchar2,
		ie_prescr_nao_liberada_p	varchar2,
		nr_prescricao_p			number,
		nr_prescricoes_p		varchar2,
		nr_prescr_titulo_p		number,
		nm_usuario_p			varchar2)
 		    	return number is

nr_prescricao_w		number(14,0);
nr_atendimento_w	number(10,0);

begin
if	(nm_usuario_p is not null) then
	begin
	
	nr_prescricao_w	:=	plt_obter_prescricao(nr_prescricao_p,nr_prescricoes_p,nr_prescr_titulo_p,nm_usuario_p,cd_pessoa_fisica_p,'U');

	if	(nr_atendimento_p > 0) then
		begin
		
		nr_atendimento_w := nr_atendimento_p;
		end;
	
	else if	(nr_prescricao_w > 0) then
		begin
		
		select	nvl(max(nr_atendimento),0)
		into	nr_atendimento_w
		from	prescr_medica
		where	nr_prescricao = nr_prescricao_w;
		
		if	(nr_atendimento_w = 0) then
			begin
			
			select	nvl(max(nr_atendimento),0)
			into	nr_atendimento_w
			from	atendimento_paciente
			where	cd_pessoa_fisica = cd_pessoa_fisica_p
			and	nvl(obter_setor_atendimento(nr_atendimento),0) > 0
			and	dt_alta is null;			
			end;
		end if;		
		end;
	else if	(cd_pessoa_fisica_p <> '') then
		begin
		
		select	nvl(max(nr_atendimento),0)
		into	nr_atendimento_w
		from	atendimento_paciente
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	nvl(obter_setor_atendimento(nr_atendimento),0) > 0
		and	dt_alta is null;
		end;
	end if;
	end if;
	end if;	
	end;
end if;
return	nr_atendimento_w;
end obter_atendimento_pt;
/