CREATE OR REPLACE
FUNCTION san_obter_entidade_atend( nr_atendimento_p NUMBER)
        RETURN NUMBER IS

cd_setor_atendimento_w NUMBER(5);
nr_sequencia_w  NUMBER(10);

BEGIN

IF (nr_atendimento_p IS NOT NULL) THEN

 cd_setor_atendimento_w := NVL(obter_setor_atendimento(nr_atendimento_p),0);

 SELECT  MAX(nr_sequencia)
 INTO nr_sequencia_w
 FROM san_entidade
 WHERE  cd_setor_atendimento = cd_setor_atendimento_w;

END IF;

RETURN nr_sequencia_w;

END san_obter_entidade_atend;
/