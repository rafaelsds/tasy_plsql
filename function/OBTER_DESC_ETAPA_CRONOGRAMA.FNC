CREATE OR REPLACE
FUNCTION Obter_desc_etapa_cronograma	(nr_sequencia_p		number)
					RETURN VARCHAR2 IS

ds_retorno_w	VARCHAR2(100);

BEGIN

if	(nr_sequencia_p is not null) then
	select	a.ds_etapa
	into	ds_retorno_w
	from	proj_etapa_cronograma a
	where	a.nr_sequencia = nr_sequencia_p;
end if;

RETURN	ds_retorno_w;

END Obter_desc_etapa_cronograma;
/