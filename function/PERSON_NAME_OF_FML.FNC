create or replace FUNCTION PERSON_NAME_OF_FML(
    NR_SEQUENCIA_P IN PESSOA_FISICA.NR_SEQ_PERSON_NAME%type,
    option_p       IN VARCHAR DEFAULT 'FULLN' )
  RETURN VARCHAR2
IS
  ds_retorno_w VARCHAR2( 60 );
  first_name   VARCHAR2( 60 );
  middle_Name  VARCHAR2( 60 );
  last_name    VARCHAR2( 60 );
BEGIN
  IF( NR_SEQUENCIA_P IS NOT NULL )THEN
    BEGIN
      SELECT DISTINCT DS_GIVEN_NAME,
        DS_COMPONENT_NAME_1,
        DS_FAMILY_NAME
      INTO first_name,
        middle_Name,
        last_name
      FROM person_name
      WHERE NR_SEQUENCIA = NR_SEQUENCIA_P
      AND ds_type	=	'main' ;
      IF(option_p        = 'FN')THEN
        ds_retorno_w    := first_name;
      elsif( option_p    = 'MN' ) THEN
        ds_retorno_w    := middle_Name;
      elsif( option_p    = 'LN' ) THEN
        ds_retorno_w    := last_name;
      END IF;
    END;
  END IF;
RETURN ds_retorno_w;
END PERSON_NAME_OF_FML;
/