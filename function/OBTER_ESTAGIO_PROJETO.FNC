create or replace
function obter_estagio_projeto (nr_seq_projeto_p	number)
	return varchar2 is
	
ds_estagio_w	varchar2(255);	
	
begin
if	(nr_seq_projeto_p is not null) then
	begin
	select	max(ds_estagio)
	into	ds_estagio_w
	from	proj_estagio e,
		proj_projeto p
	where	e.nr_sequencia = p.nr_seq_estagio
	and	p.nr_sequencia = nr_seq_projeto_p;
	end;
end if;
return ds_estagio_w;
end obter_estagio_projeto;
/