create or replace
function pls_obter_qt_prot_lote(	ds_hash_p			varchar2,
					nr_seq_prestador_p		number,
					nr_protocolo_prestador_p	varchar2,
					nr_seq_usu_prestador_p		number,
					nr_seq_perfil_web_p		number,
					cd_estabelecimento_p		number)
					return number is

qt_protocolo_integrado_w	Number(10) := 0;
nr_seq_lote_w			Number(10) := 0;
ie_permite_duplicidade_w	Varchar2(1) := 'N';

begin

if	(nr_seq_usu_prestador_p is not null) then
	ie_permite_duplicidade_w := pls_obter_param_web(1249, 15, cd_estabelecimento_p,
							nr_seq_usu_prestador_p, nr_seq_perfil_web_p, null,
							null,'P',null,null);
end if;


if	(ie_permite_duplicidade_w = 'N') then
	select	max(nr_sequencia)
	into	nr_seq_lote_w
	from	pls_lote_protocolo_conta
	where	nr_seq_prestador = nr_seq_prestador_p
	and	ie_tipo_lote = 'P'
	and	dt_geracao_analise is null;

	if (nr_seq_lote_w is not null) then
		select  count(1)
		into	qt_protocolo_integrado_w
		from    pls_protocolo_conta
		where   ds_hash = ds_hash_p
		and     nr_protocolo_prestador = nr_protocolo_prestador_p
		and	nr_seq_lote_conta = nr_seq_lote_w;
	end if;
end if;

return qt_protocolo_integrado_w;

end pls_obter_qt_prot_lote;
/
