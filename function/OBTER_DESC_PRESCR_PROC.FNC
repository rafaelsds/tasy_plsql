create or replace
function Obter_Desc_Prescr_Proc
		(cd_procedimento_p	number,
		ie_origem_proced_p	number,
		nr_seq_proc_interno_p	number,
		ie_desc_nao_informado_p	varchar2 default 'S') 
		return varchar2 is

ds_procedimento_w	procedimento.ds_procedimento%TYPE;
nr_seq_interno_w	number(10,0);

begin

if	(nvl(nr_seq_proc_interno_p,0) > 0)  then
	select	max(ds_proc_exame)
	into	ds_procedimento_w
	from	proc_interno
	where	nr_sequencia	= nr_seq_proc_interno_p;
else
	select	obter_descricao_procedimento(cd_procedimento_p, ie_origem_proced_p)
	into	ds_procedimento_w
	from 	dual;
end if;

if	(ie_desc_nao_informado_p = 'N') and (ds_procedimento_w = OBTER_DESC_EXPRESSAO(612231)) then -- 612231: 'N�o encontrado'
	ds_procedimento_w := null;
end if;	
	

return	ds_procedimento_w;

end Obter_Desc_Prescr_Proc;
/
