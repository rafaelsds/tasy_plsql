create or replace 
function obter_inicio_quimio_grid_gpt(nr_atendimento_p	number,
				cd_pessoa_fisica_p	varchar2) return date is
				
nr_sequencia_w			gpt_hist_analise_plano.nr_sequencia%type;
dt_inicio_analise_w 	gpt_hist_analise_plano.dt_inicio_analise%type;
dt_fim_analise_w		gpt_hist_analise_plano.dt_fim_analise%type;
dt_prescricao_w			date;
begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	gpt_hist_analise_plano
where	((nr_atendimento = nr_atendimento_p) or ((cd_pessoa_fisica = cd_pessoa_fisica_p and nr_atendimento is null and nvl(nr_atendimento_p,0) = 0)))
and		ie_tipo_usuario = 'E';

if	(nr_sequencia_w is not null) and
	(nr_sequencia_w > 0) then
	select 	max(dt_inicio_analise),
			max(dt_fim_analise)
	into	dt_inicio_analise_w,
			dt_fim_analise_w
	from 	gpt_hist_analise_plano
	where	nr_sequencia = nr_sequencia_w;
end if;

if	(dt_fim_analise_w is not null) then
	dt_prescricao_w := dt_fim_analise_w;
elsif	(dt_inicio_analise_w is not null) then
	dt_prescricao_w := dt_inicio_analise_w;
else
	dt_prescricao_w := trunc(sysdate,'dd') - 2;
end if;

return dt_prescricao_w;

end obter_inicio_quimio_grid_gpt;
/