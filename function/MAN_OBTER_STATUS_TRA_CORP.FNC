create or
replace function Man_obter_status_tra_corp(nm_table_name_p	 	 varchar2, 
											nm_primary_key_name_p varchar2, 
											nr_primary_key_value_p varchar2)
return varchar2
is
--'A' = 927336 - Aguardando tradu��o 
--'T' = 927338 - Tradu��o realizada 
--'N' = 927454 - Tradu��o n�o configurada

nm_atributo_exp_w	varchar2(100); 
qt_traducao_w		number(5); 
ds_sql_dinamico		varchar2(500); 
cd_exp_w			number(10); 

cursor c1 is 
select	nm_atributo_exp 
from	corp_tabelas_traducao 
where	nm_tabela = nm_table_name_p
and		nvl(ie_situacao, 'A') <> 'I';
	
begin 
	open c1;
		loop 
			fetch c1 into nm_atributo_exp_w;
				if(nm_atributo_exp_w is null) then
					return 'N';
				end if;
				ds_sql_dinamico := 'select ' 
								|| nm_atributo_exp_w 
								|| ' from ' 
								|| nm_table_name_p 
								|| ' where ' 
								|| nm_primary_key_name_p 
								|| ' = ' 
								|| nr_primary_key_value_p;
			
				execute immediate ds_sql_dinamico into cd_exp_w;
			
				select	Count(*)
				into	qt_traducao_w
				from	dic_expressao_idioma
				where	ds_locale in ( 'en_US', 'pt_BR', 'es_MX' ) 
				and		cd_expressao = cd_exp_w; 
			
				if ( qt_traducao_w < 3 ) then 
					return 'A'; 
				end if;
			exit when c1%notfound;			
		end loop;
	close c1;

	return 'T'; 
end man_obter_status_tra_corp;
/ 