create or replace
function obter_se_existe_prot_prescr(
							cd_protocolo_p	number,
							nr_prescricao_p number) return varchar2 is
ie_existe_prot_w	varchar2(1) := 'N';
begin
if	(cd_protocolo_p is not null) and 
	(nr_prescricao_p is not null) then
	/* Verifica se existe o protocolo no cadastro da prescri��o */
	select	nvl(max('S'),'N')
	into		ie_existe_prot_w
	from		prescr_medica
	where	nr_prescricao	= nr_prescricao_p
	and		cd_protocolo	= cd_protocolo_p;
	/* Se n�o existe na prescri��o verifica nos materiais/medicamentos da prescri��o */
	if 	(ie_existe_prot_w = 'N') then
		select	nvl(max('S'),'N')
		into		ie_existe_prot_w
		from		prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and		cd_protocolo	= cd_protocolo_p;
		/* Se n�o existe nos materiais/medicamentos verifica nos procedimentos */
		if	(ie_existe_prot_w = 'N') then
			select	nvl(max('S'),'N')
			into		ie_existe_prot_w
			from		prescr_procedimento
			where	nr_prescricao	= nr_prescricao_p
			and		cd_protocolo	= cd_protocolo_p;
		end if;
	end if;
end if;

return ie_existe_prot_w;

end obter_se_existe_prot_prescr;
/