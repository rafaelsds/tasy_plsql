create or replace
function obter_se_prescricao_quimio(nr_prescricao_p		number)
 		    	return varchar2 is

ie_quimio_w	varchar2(1) := 'N';

begin

select 	nvl(max('S'),'N') 
into	ie_quimio_w
from	paciente_atendimento
where	nr_prescricao = nr_prescricao_p;


return	ie_quimio_w;

end obter_se_prescricao_quimio;
/