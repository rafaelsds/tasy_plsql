create or replace
function	obter_qtd_baixa_mensalidade(	nr_sequencia_p	number)
						return varchar2 is

ie_retorno_w	varchar2(1);

begin

if	(nvl(nr_sequencia_p,0) > 0) then

	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	titulo_receber_liq c,
		titulo_receber b,
		pls_mensalidade a
	where	a.nr_sequencia = b.nr_seq_mensalidade
	and	b.nr_titulo = c.nr_titulo
	and	a.nr_sequencia = nr_sequencia_p;

end if;

return	ie_retorno_w;

end obter_qtd_baixa_mensalidade;
/