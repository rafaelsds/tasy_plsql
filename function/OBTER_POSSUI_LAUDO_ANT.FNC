CREATE OR REPLACE
FUNCTION obter_possui_laudo_ant(cd_pessoa_fisica_p	varchar2)
				RETURN VARCHAR2 IS

ds_retorno_w	varchar2(1)	:= 'N';
qt_laudo_w		number(10);

BEGIN
	
select	count(*)
into		qt_laudo_w
from   	laudo_paciente		b,
		atendimento_paciente	a
where		a.nr_atendimento		= b.nr_atendimento	   
and		a.cd_pessoa_fisica	= cd_pessoa_fisica_p
and		b.dt_liberacao is not null;

if		(qt_laudo_w	> 0) then
		ds_retorno_w	:=	'S';

end if;

RETURN ds_retorno_w;

END obter_possui_laudo_ant;
/