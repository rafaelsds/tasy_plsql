create or replace
function   dl_obter_desc_tipo_item(nr_seq_tipo_item_p Number)
				return varchar2 is

ds_tipo_item_w	varchar2(255);

begin

select	obter_valor_dominio(3131,ie_tipo_item)
into	ds_tipo_item_w
from	dl_tipo_item
where	nr_sequencia = nr_seq_tipo_item_p;

return	ds_tipo_item_w;

end dl_obter_desc_tipo_item;
/