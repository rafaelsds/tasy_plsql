create or replace 
function lab_obter_se_exame_lib (	nr_seq_exame_p		number,
				cd_estabelecimento_p	number)
				return varchar2 is

nr_seq_grupo_w		number(10);
ie_possui_regra_w	varchar2(1);
ie_exame_liberado_w	varchar2(1);

begin
ie_exame_liberado_w	:= 'S';
if (nr_seq_exame_p > 0) then
	select 	max(nr_seq_grupo)
	into	nr_seq_grupo_w
	from 	exame_laboratorio
	where 	nr_seq_exame = nr_seq_exame_p;

	select 	nvl(max('S'), 'N')
	into	ie_possui_regra_w
	from	EXAME_LAB_ESTAB
	where 	nvl(nr_seq_exame,nr_seq_exame_p) = nr_seq_exame_p
	and	nvl(nr_seq_grupo,nr_seq_grupo_w) = nr_seq_grupo_w
	and	rownum <= 1;

	if (ie_possui_regra_w = 'S') then

		ie_exame_liberado_w	:= 'N';

		select 	nvl(max('S'), 'N')
		into	ie_exame_liberado_w
		from	EXAME_LAB_ESTAB
		where 	nvl(nr_seq_exame,nr_seq_exame_p) = nr_seq_exame_p
		and	nvl(nr_seq_grupo,nr_seq_grupo_w) = nr_seq_grupo_w
		and	cd_estabelecimento = cd_estabelecimento_p
		and	rownum <= 1;

	end if;
end if;
return ie_exame_liberado_w;

end lab_obter_se_exame_lib;
/
