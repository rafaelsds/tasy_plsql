create or replace 
function OBTER_SE_CHEQUE_TITULO
		(nr_seq_cheque_p	number,
		 nr_titulo_p		number) return varchar2 is

ie_retorno_w		varchar2(100);
cont_w			number(10,0);
nr_seq_caixa_w		number(15,0);
nr_seq_lote_w		number(15,0);

cursor c01 is
select	a.nr_seq_caixa,
	a.nr_seq_lote
from	movto_trans_financ a
where	a.nr_seq_cheque		= nr_seq_cheque_p
and	a.nr_seq_caixa		is not null
and	a.nr_seq_lote		is not null
order 	by a.dt_transacao;


begin

select	count(*)
into	cont_w
from	cheque_cr
where	nr_seq_cheque		= nr_seq_cheque_p
and	nr_titulo		= nr_titulo_p;

ie_retorno_w		:= 'N';
if	(cont_w > 0) then
	ie_retorno_w	:= 'S';
else
	open c01;
	loop
	fetch c01 into
		nr_seq_caixa_w,
		nr_seq_lote_w;
	exit when c01%notfound;
		null;
	end loop;
	close c01;

	select	count(*)
	into	cont_w
	from	movto_trans_financ
	where	nr_seq_titulo_receber	= nr_titulo_p
	and	nr_seq_caixa		= nr_seq_caixa_w
	and	nr_seq_lote		= nr_seq_lote_w;

	if	(cont_w > 0) then
		ie_retorno_w		:= 'S';
	end if;
end if;

return ie_retorno_w;

end OBTER_SE_CHEQUE_TITULO;
/
