create or replace
function obter_vaga_hist_status_aprov(nr_seq_gestao_vaga_p	number)
									return varchar2 is
ie_retorno_w		varchar2(1) := 'N'; 									
									
begin

select	decode(count(*),0,'N','S')
into	ie_retorno_w
from	gestao_vaga_hist_status	
where	ie_status = 'P'
and 	nr_seq_gestao_vaga = nr_seq_gestao_vaga_p;

return	ie_retorno_w;

end obter_vaga_hist_status_aprov;
/