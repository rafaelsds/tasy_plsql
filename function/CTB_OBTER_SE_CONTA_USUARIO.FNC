create or replace
function ctb_obter_se_conta_usuario(	cd_empresa_p		number,
				cd_conta_contabil_p	varchar2,
				nm_usuario_p		varchar2)
				return varchar2 is

cd_conta_contabil_w		varchar2(20);
qt_registro_w			number(10);
qt_regra_w			number(10)	:= 0;
ie_permite_w			varchar2(1);
ie_atualizacao_w			varchar2(1);
cd_perfil_w			number(5);

cursor c01 is
select	nvl(ie_atualizacao,'N')
from	ctb_lib_orc_conta
where	cd_empresa					= cd_empresa_p
and	((nm_usuario_lib				= nm_usuario_p) or (nm_usuario_lib is null))
and	((cd_perfil 		= cd_perfil_w) or (cd_perfil is null))
and	((((nvl(ie_conta_inferior,'N') = 'N') and (nvl(cd_conta_contabil,cd_conta_contabil_w)	= cd_conta_contabil_w)) or
	((ie_conta_inferior = 'S') and (substr(ctb_obter_se_conta_classif(cd_conta_contabil_w,obter_dados_conta_contabil(cd_conta_contabil,null,'CL')),1,1) = 'S'))))
order	by 	nm_usuario_lib desc, cd_perfil	desc;

begin

cd_perfil_w		:= wheb_usuario_pck.get_cd_perfil;

ie_permite_w		:= 'S';
cd_conta_contabil_w	:= nvl(cd_conta_contabil_p,0);

select	count(*)
into	qt_regra_w
from	ctb_lib_orc_conta
where	cd_empresa = cd_empresa_p;

if	(qt_regra_w > 0) then

	ie_permite_w	:= 'N';

	/* Matheus comentei este retirei o if em 25/09/2008 porque � necessario somente o cursor
	select	count(*)
	into	qt_registro_w
	from	ctb_lib_orc_conta
	where	cd_empresa				= cd_empresa_p
	and	nvl(cd_conta_contabil,cd_conta_contabil_w)	= cd_conta_contabil_w
	and	nm_usuario_lib				= nm_usuario_p;*/

	/* se tiver regra, verifica se pode ou n�o atualizar o or�amento de todas ou de uma conta*/
	open c01;
	loop
	fetch c01 into
		ie_atualizacao_w;
	exit when c01%notfound;
		begin
		ie_permite_w	:= ie_atualizacao_w;
		end;
	end loop;
	close c01;	
	
end if;
return ie_permite_w;

end ctb_obter_se_conta_usuario;
/