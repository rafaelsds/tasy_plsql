create or replace
function obter_nome_avf_tipo_avaliacao(	nr_sequencia_p		number)
return varchar2 is

ds_tipo_avaliacao_w		varchar2(255);
			
begin

select	ds_tipo_avaliacao
into	ds_tipo_avaliacao_w
from	avf_tipo_avaliacao
where	nr_sequencia = nr_sequencia_p;

return	ds_tipo_avaliacao_w;

end obter_nome_avf_tipo_avaliacao;
/