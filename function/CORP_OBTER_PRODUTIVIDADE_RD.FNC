create or replace
function corp_obter_produtividade_rd(	dt_referencia_p		date,
					nr_seq_gerencia_p	number)
					return number is
					

dt_inicial_w				date;
dt_final_w				date;
qt_os_cliente_teste_w			number(10);
qt_funcionario_w			number(10);
qt_retorno_w				number(15,4);

begin

dt_inicial_w	:= trunc(dt_referencia_p,'mm');
dt_final_w	:= fim_mes(dt_referencia_p);

/* Quantidade de funcionarios - igual ao que � buscado na tela Indicadores Desenvolvimento */
select	count(1)
into	qt_funcionario_w
from	usuario_grupo_des b,
	grupo_desenvolvimento a
where	a.nr_sequencia	= b.nr_seq_grupo
and	a.ie_situacao	= 'A'
and	a.nr_seq_gerencia	= nvl(nr_seq_gerencia_p, a.nr_seq_gerencia);

/* Quantidade de OS em Cliente-teste */

select	count(*)
into	qt_os_cliente_teste_w
from	grupo_desenvolvimento a,
	man_ordem_servico b
where	a.nr_sequencia	= b.nr_seq_grupo_des
and	a.nr_seq_gerencia	= nvl(nr_seq_gerencia_p, a.nr_seq_gerencia)
and	exists	(select	1
		from	man_ordem_serv_estagio y,
			man_estagio_processo x
		where	x.nr_sequencia	= y.nr_seq_estagio
		and	y.nr_seq_ordem	= b.nr_sequencia
		and	x.ie_desenv	= 'S')
and	exists	(select	1
		from	man_ordem_serv_estagio y,
			man_estagio_processo x
		where	x.nr_sequencia	= y.nr_seq_estagio
		and	y.nr_seq_ordem	= b.nr_sequencia
		and	x.nr_sequencia in(2,791)
		and	y.dt_atualizacao between dt_inicial_w and dt_final_w
		and	exists(	select	1
				from	usuario_grupo_des w,
					grupo_desenvolvimento z
				where	z.nr_sequencia	= w.nr_seq_grupo
				and	y.nm_usuario	= w.nm_usuario_grupo
				and	z.nr_seq_gerencia	= nvl(nr_seq_gerencia_p, z.nr_sequencia)));


qt_retorno_w	:= dividir_sem_round(qt_os_cliente_teste_w, qt_funcionario_w);

return qt_retorno_w;

end corp_obter_produtividade_rd;
/