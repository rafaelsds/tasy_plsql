create or replace
function pls_obter_se_analises_tela
			(	nr_seq_analise_p	Number,
				cd_guia_p		Varchar2)
 		    		return Varchar2 is
				
/*Esta function funciona especificamente para obter a qt de analises na tela da fun��o
    OPS - An�lise de Contas M�dicas
    
    S - Existe mais de uma an�lise
    N - Existe somente uma an�ise*/				
				
ds_retorno_w			Varchar2(1) := 'S';

begin

begin
select	'N'
into	ds_retorno_w
from	pls_resumo_conta_v
where	((nr_guia = cd_guia_p) or (nvl(cd_guia_p,0) = '0'))
and	((nr_seq_analise = nr_seq_analise_p) or (nvl(nr_seq_analise_p,0) = 0))
and	nvl(nr_seq_conta,0) > 0
group by nr_seq_analise;
exception
when others then
	ds_retorno_w := 'S';
end;

return	ds_retorno_w;

end pls_obter_se_analises_tela;
/