create or replace
function obter_se_possui_foto(
		cd_pessoa_fisica_p		varchar2,
		nr_seq_atend_visita_p	number)
 	return varchar2 is

ie_possui_foto_w	varchar2(1);

begin

ie_possui_foto_w	:= 'N';

if	(cd_pessoa_fisica_p is not null) then
	select	decode( count(*), 0, 'N', 'S')
	into	ie_possui_foto_w
	from	pessoa_fisica_foto
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
end if;

if	((nr_seq_atend_visita_p is not null) and
	(ie_possui_foto_w = 'N')) then
	select	decode( count(*), 0, 'N', 'S')
	into	ie_possui_foto_w
	from	atendimento_visita_foto
	where	nr_seq_atend_visita	= nr_seq_atend_visita_p;
end if;

return	ie_possui_foto_w;
end obter_se_possui_foto;
/
