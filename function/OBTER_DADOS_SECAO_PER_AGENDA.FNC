create or replace
function obter_dados_secao_per_agenda(nr_seq_agenda_p		number,
					ie_diario_p		varchar2,
					ie_final_semana_p	varchar2,
					qt_intervalo_p		number,
					ie_opcao_p		varchar2,
					ie_next_day_p varchar2 default 'N')
					return date is

qt_dia_w		number(3);
qt_total_secao_w	number(3,0);
nr_secao_w	number(3,0);
dt_agenda_w	date;
dt_atual_w	date;
cd_dia_semana_w	varchar2(1);
ds_retorno_w	date;
ie_gerar_dia_w	varchar2(1)  := 'S';
qt_feriado_dia_w	number(10);
cd_agenda_w	number(10);
ie_agenda_feriado_w	varchar(1);
ie_bloqueio_w	varchar2(1);
ie_existe_turno_w	varchar2(1);
cd_estabelecimento_w		number(4,0);	
ie_existe_livre_w  varchar2(1);
ie_dia_gerado_w  varchar2(1);

/*
D - Data t�rmino
*/

begin

if	(nr_seq_agenda_p is not null) then
	begin
	/* obter dados agenda */
	select	max(nvl(a.nr_secao,1)),
		max(a.dt_agenda),
		max(nvl(a.qt_total_secao,0)),
		max(b.cd_estabelecimento)
	into	nr_secao_w,
		dt_agenda_w,
		qt_total_secao_w,
		cd_estabelecimento_w
	from	agenda_consulta a,
		agenda b
	where	a.cd_agenda = b.cd_agenda
	and	a.nr_sequencia = nr_seq_agenda_p;
	
	/* Gerar hor�rios por semana ou diariamente */
	if	(ie_diario_p = 'S') then
		qt_dia_w	:= 1;
	elsif	(ie_diario_p = 'N') and (qt_intervalo_p > 0) then
		qt_dia_w	:= qt_intervalo_p;
	elsif	(ie_diario_p = 'N') then
		qt_dia_w	:= 7;
	end if;

	--nr_secao_w	:= 1;
	dt_atual_w	:= dt_agenda_w;
	if	(qt_total_secao_w > 0) then
		while	(nr_secao_w < qt_total_secao_w) loop
			begin
			if	(ie_next_day_p = 'S') and
				(ie_gerar_dia_w = 'N') then
				dt_atual_w	:= dt_atual_w + 1;
			else
				dt_atual_w	:= dt_atual_w + qt_dia_w;
			end if;
                                                ie_gerar_dia_w	:= 'S';	
			
			select	substr(Obter_Cod_Dia_Semana(dt_atual_w),1,1)
			into	cd_dia_semana_w
			from	dual;			

			if	(cd_dia_semana_w = 1) and
				(ie_final_semana_p = 'N') then
				dt_atual_w	:= dt_atual_w + 1;
			elsif	(cd_dia_semana_w = 7) and
				(ie_final_semana_p = 'N') then
				dt_atual_w	:= dt_atual_w + 2;
			end if;
			
			
			--VALIDAR SE EXISTE FERIADO NO MEIO DO PER�ODO DE GERA��O DOS DIAS DA AGENDA
			select	nvl(max(obter_se_feriado(cd_estabelecimento_w, dt_atual_w)), 0)
			into	qt_feriado_dia_w
			from	dual;
			
			--BUSCAR C�D. DA AGENDA PARA VERIFICAR SE � PERMITIDO O AGENDAMENTO EM DIAS QUE S�O FERIADO
			select	nvl(max(cd_agenda), 0)
			into	cd_agenda_w
			from	agenda_consulta
			where	nr_sequencia	= nr_seq_agenda_p;
			
			if	(cd_agenda_w > 0)then
				begin
				
				select 	nvl(max(ie_feriado),'N')
				into	ie_agenda_feriado_w
				from	agenda
				where	cd_agenda = cd_agenda_w;
				
				end;			
			end if;
			
			/* validar hor�rio x bloqueio */
			select	decode(count(*),0,'N','S')
			into	ie_bloqueio_w
			from	agenda_bloqueio
			where	cd_agenda = cd_agenda_w
			and	dt_final >= trunc(dt_atual_w)
			and	dt_inicial <= trunc(dt_atual_w)
			AND	(TO_CHAR(hr_final_bloqueio,'hh24:mi:ss') >= TO_CHAR(dt_atual_w,'hh24:mi') 
				OR hr_final_bloqueio IS NULL)
			AND	(TO_CHAR(hr_inicio_bloqueio, 'hh24:mi:ss') <= TO_CHAR(dt_atual_w,'hh24:mi') 
				OR hr_inicio_bloqueio IS NULL)
			and	((ie_dia_semana is null) or (ie_dia_semana = cd_dia_semana_w));
			
			/*
			VIG�NCIAS DO TURNO:
			*/
			select	decode(count(*),0,'N','S')
			into	ie_existe_turno_w
			from   	agenda_turno a,
				agenda_turno_classif c
			where  	c.nr_seq_turno = a.nr_sequencia
			AND 	a.cd_agenda       	= 	cd_agenda_w						
			and    	((a.ie_dia_semana   	= 	cd_dia_semana_w) 
				or (a.ie_dia_semana = 9))
			AND	TO_CHAR(hr_final, 'hh24:mi:ss') >= TO_CHAR(dt_atual_w,'hh24:mi:ss')
			AND	TO_CHAR(hr_inicial, 'hh24:mi:ss') <= TO_CHAR(dt_atual_w,'hh24:mi:ss')
			AND	((qt_feriado_dia_w > 0 AND NVL(c.ie_gera_feriado,'S') = 'S' AND ie_agenda_feriado_w = 'S')
				OR (qt_feriado_dia_w = 0));
				
			/* validar se existe horario */
			select	decode(count(*),0,'N','S')
			into	ie_existe_livre_w
			from	agenda_consulta
			where	cd_agenda = cd_agenda_w
			and		dt_agenda = dt_atual_w
			and 	ie_status_agenda = 'L';
			
			/*Validar se dia foi gerado*/
			select	decode(count(*),0,'N','S')
			into	ie_dia_gerado_w
			from	agenda_consulta
			where	cd_agenda = cd_agenda_w
			and		dt_agenda = dt_atual_w;
			
			if  (ie_existe_turno_w = 'N') then
				ie_gerar_dia_w	:= 'N';					
			elsif (ie_existe_livre_w = 'N' and ie_dia_gerado_w = 'S') then
				ie_gerar_dia_w	:= 'N';
			elsif (ie_bloqueio_w = 'S') then
				ie_gerar_dia_w	:= 'N';
			elsif	(qt_feriado_dia_w > 0) and	(ie_agenda_feriado_w = 'N') then
				ie_gerar_dia_w	:= 'N';	
			end if;				
			
			if	(ie_gerar_dia_w	= 'S') then
				nr_secao_w	:= nr_secao_w + 1;
			end if;
	
			
			end;
		end loop;
	end if;

	if	(ie_opcao_p = 'D') then
		ds_retorno_w	:= dt_atual_w;
	end if;
	end;
end if;
	
return ds_retorno_w;

end obter_dados_secao_per_agenda;
/