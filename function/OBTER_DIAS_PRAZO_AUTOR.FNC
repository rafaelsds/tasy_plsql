create or replace
function obter_dias_prazo_autor(nr_seq_agenda_p	number)
				return varchar2 is

ds_retorno_w		varchar2(255);
qt_dias_prazo_w		number(10);

begin

if	(nr_seq_agenda_p is not null) then
	select 	max(qt_dias_prazo)
	into 	qt_dias_prazo_w
	from 	autorizacao_convenio
	where 	nr_seq_agenda = nr_seq_agenda_p;

	if	(qt_dias_prazo_w > 0) then
		ds_retorno_w		:= to_char(qt_dias_prazo_w)|| ' ' || Wheb_mensagem_pck.get_texto(309125); --' dias';
	else
		ds_retorno_w		:= Wheb_mensagem_pck.get_texto(307465); --'N�o informado';
	end if;
		
end if;

return	ds_retorno_w;

end	obter_dias_prazo_autor;
/