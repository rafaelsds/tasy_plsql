create or replace
function tre_desc_certificado(nr_seq_certificado_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(90);			
			
begin

if	(nr_seq_certificado_p is not null) then
	
	select	substr(max(ds_certificado),1,90)
	into	ds_retorno_w
	from	tre_certificado
	where	nr_sequencia	= nr_seq_certificado_p;
	
end if;

return	ds_retorno_w;

end tre_desc_certificado;
/