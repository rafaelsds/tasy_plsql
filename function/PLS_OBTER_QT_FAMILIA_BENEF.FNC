create or replace
function pls_obter_qt_familia_benef
		(	nr_seq_segurado_p	number,
			nr_seq_titular_p	number,
			ie_opcao_p		varchar2,
			dt_verificar_ativo_p	date)
 		    	return number is
			
/*
ie_opcao_p
L - Liberado
N - N�o liberado
*/
			
qt_registros_w		number(10);

begin

if	(nr_seq_titular_p is null) then
	if	(ie_opcao_p = 'L') then
		select	count(1)
		into	qt_registros_w
		from	pls_segurado
		where	nr_seq_titular	= nr_seq_segurado_p
		and	dt_liberacao is not null
		and	((dt_rescisao is null) or (dt_rescisao > dt_verificar_ativo_p));
	elsif	(ie_opcao_p = 'N') then
		select	count(1)
		into	qt_registros_w
		from	pls_segurado
		where	nr_seq_titular	= nr_seq_segurado_p;
	end if;
	qt_registros_w	:= nvl(qt_registros_w,0) + 1;

elsif	(nr_seq_titular_p is not null) then
	if	(ie_opcao_p = 'L') then
		select	count(1)
		into	qt_registros_w
		from	pls_segurado
		where	nr_seq_titular	= nr_seq_titular_p
		and	nr_sequencia	<> nr_seq_segurado_p
		and	dt_liberacao is not null
		and	((dt_rescisao is null) or (dt_rescisao > dt_verificar_ativo_p));
	elsif	(ie_opcao_p = 'N') then
		select	count(1)
		into	qt_registros_w
		from	pls_segurado
		where	nr_seq_titular	= nr_seq_titular_p
		and	nr_sequencia	<> nr_seq_segurado_p;
	end if;
	qt_registros_w	:= nvl(qt_registros_w,0) + 2;
end if;

return	qt_registros_w;

end pls_obter_qt_familia_benef;
/
