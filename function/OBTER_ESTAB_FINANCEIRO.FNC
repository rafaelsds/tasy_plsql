create or replace
function OBTER_ESTAB_FINANCEIRO(cd_estabelecimento_p	number)
 		    	return number is

cd_estab_financeiro_w	number(4)	:= null;
			
begin

select	max(cd_estab_financeiro)
into	cd_estab_financeiro_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

return	cd_estab_financeiro_w;

end OBTER_ESTAB_FINANCEIRO;
/