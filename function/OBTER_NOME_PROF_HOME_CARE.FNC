create or replace
function obter_nome_prof_home_care(nr_seq_profissional_p number)
				return varchar2 is

ds_retorno_w		varchar2(100);

begin

if	(nr_seq_profissional_p > 0) then

	select	substr(obter_nome_pf(cd_pessoa_fisica),1,100)
	into	ds_retorno_w
	from	hc_profissional
	where	nr_sequencia = nr_seq_profissional_p;

end if;

return ds_retorno_w;

end obter_nome_prof_home_care;
/