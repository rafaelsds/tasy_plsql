CREATE OR REPLACE
FUNCTION Obter_data_alta_Atendimento(nr_atendimento_p		number)
					RETURN DATE IS

ds_retorno_w		date;

BEGIN
if	(nr_atendimento_p is not null) then
	select	nvl(a.dt_alta,sysdate)
	into	ds_retorno_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_p;
end if;

RETURN	ds_retorno_w;

END	Obter_data_alta_Atendimento;
/
