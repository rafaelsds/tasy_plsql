create or replace
function paf_obter_proximo_cf_item
 		    	return number is

nr_cupom_fiscal_item_w	number(10);
			
begin

select	paf_cupom_fiscal_item_seq.nextval
into	nr_cupom_fiscal_item_w
from	dual;

return	nr_cupom_fiscal_item_w;

end paf_obter_proximo_cf_item;
/
