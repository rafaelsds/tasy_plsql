CREATE OR REPLACE
function obter_se_permite_grupo_compra(	nr_solic_compra_p		number,
					cd_material_p		number)
return varchar2 is

nr_seq_grupo_compra_w			number(10);
nr_seq_grupo_item_solic_w			number(10);
cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
ie_permite_w				varchar2(1) := 'S';
ie_liberado_w				varchar2(1);
qt_regra_w				number(10);
qt_itens_solic_compra_w			number(10);
ds_lista_grupo_compra_w			varchar2(255);
ds_lista_grupo_item_solic_w			varchar2(4000);
cd_material_w				number(6);

cursor c01 is
select	distinct
	a.cd_material
from	solic_compra_item a
where	a.nr_solic_compra = nr_solic_compra_p
and	a.cd_material <> cd_material_p;

cursor c02 is
select	c.nr_sequencia nr_seq_grupo
from	sup_estrut_compras a,
	sup_subgrupo_compra b,
	sup_grupo_compra c
where	a.nr_seq_subgrupo_compra = b.nr_sequencia
and	b.nr_seq_grupo_compra = c.nr_sequencia
and	((a.cd_grupo_material = obter_estrutura_material(cd_material_w,'G')) or
	(a.cd_subgrupo_material = obter_estrutura_material(cd_material_w,'S')) or
	(a.cd_classe_material = obter_estrutura_material(cd_material_w,'C')) or
	(a.cd_material = cd_material_w))
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(b.ie_situacao,'A') = 'A';

cursor c03 is
select	distinct
	c.nr_sequencia nr_seq_grupo
from	sup_estrut_compras a,
	sup_subgrupo_compra b,
	sup_grupo_compra c
where	a.nr_seq_subgrupo_compra = b.nr_sequencia
and	b.nr_seq_grupo_compra = c.nr_sequencia
and	nvl(a.cd_grupo_material, cd_grupo_material_w)     	= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(a.cd_material, cd_material_p)			= cd_material_p or cd_material_p = 0)
and	ds_lista_grupo_item_solic_w is not null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(b.ie_situacao,'A') = 'A';

begin

select	count(*)
into	qt_regra_w
from	sup_estrut_compras;

select	count(*)
into	qt_itens_solic_compra_w
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p;

if	(qt_regra_w > 0) and
	(qt_itens_solic_compra_w > 0) then
	begin	
	open C01;
	loop
	fetch C01 into	
		cd_material_w;
	exit when C01%notfound;
		begin		
		open C02;
		loop
		fetch C02 into	
			nr_seq_grupo_item_solic_w;
		exit when C02%notfound;
			begin
			ds_lista_grupo_item_solic_w := substr(ds_lista_grupo_item_solic_w || nr_seq_grupo_item_solic_w || ',', 0, 4000);
			end;
		end loop;
		close C02;		
		end;
	end loop;
	close C01;
	end;
		
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from	estrutura_material_v
	where	cd_material = cd_material_p;	
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_grupo_compra_w;
	exit when C03%notfound;
		begin
		if	(obter_se_contido(nr_seq_grupo_compra_w,ds_lista_grupo_item_solic_w) = 'S') then
			ie_permite_w := 'S';
		else
			ie_permite_w := 'N';
		end if;
		end;
	end loop;
	close C03;	
end if;

return ie_permite_w;

end obter_se_permite_grupo_compra;
/
