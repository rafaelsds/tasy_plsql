create or replace
function obter_obs_tipo_isolamento_cid
				(cd_doenca_cid_p varchar2,
				ie_opcao_p	 number,
				ie_opcao_ret_p	varchar2) 
				 return varchar2 is

ds_resultado_w		varchar2(4000);
ds_tipo_isolamento_w	varchar2(80);
ds_observacao_w		varchar2(2000);
ie_quebra_w		varchar2(1);

Cursor c01 is
	select	distinct
		ds_tipo_isolamento,
		ds_observacao
	from	cih_doenca_infecto a,
		cih_tipo_isolamento b
	where	a.nr_seq_tipo_isolamento = b.nr_sequencia
	and	cd_doenca_cid	= cd_doenca_cid_p;

begin
	
ie_quebra_w	:= 'N';
OPEN C01;
LOOP
FETCH C01 into
		ds_tipo_isolamento_w,
		ds_observacao_w;
exit when c01%notfound;

	if (ie_opcao_p = 0) then
		if (ie_quebra_w = 'S') then
			ds_resultado_w := ds_resultado_w || ' - ';
		end if;
	else
		if (ie_quebra_w = 'S') then
			ds_resultado_w := ds_resultado_w || chr(10);
		end if;
	end if;
	
	if	(ie_opcao_ret_p = 1) then
		ds_resultado_w := ds_resultado_w || ds_tipo_isolamento_w;
	else
		ds_resultado_w := ds_resultado_w || ds_observacao_w;
	end if;

	ie_quebra_w := 'S';

END LOOP;
CLOSE C01;

return ds_resultado_w;
end;
/
