create or replace
function obter_desc_mot_dev_item(nr_sequencia_p	number)
 		    	return varchar2 is

ds_retorno_w varchar2(255);
			
begin

select	ds_motivo
into	ds_retorno_w
from	motivo_dev_doc_item
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end obter_desc_mot_dev_item;
/