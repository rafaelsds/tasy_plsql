create or replace function get_patient_type(
    nr_atendimento_p   number,
    cd_pessoa_fisica_p varchar2 default	null)
  return varchar2
as
  ds_return_w varchar2(10);
begin
  select count(*)
  into ds_return_w
  from gestao_vaga
  where ie_status     in ('R')
  and ie_solicitacao  in ('I','TI')
  and ((nr_atendimento = nr_atendimento_p)
  or (cd_pessoa_fisica = cd_pessoa_fisica_p
  and nr_atendimento  is null));
  if(ds_return_w       = '0') then
    select decode(ie_tipo_atendimento,1,'IN','OP')
    into ds_return_w
    from atendimento_paciente
    where ((nr_atendimento = nr_atendimento_p)
    or (cd_pessoa_fisica   = cd_pessoa_fisica_p
    and nr_atendimento    is null));
    return ds_return_w;
  else 
   ds_return_w := 'IN';
    return ds_return_w;
  end if;
end get_patient_type;
/