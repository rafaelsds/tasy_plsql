create or replace
function obter_se_agenda_escala (
		cd_agenda_p			number)
 		    	return varchar2 is

qt_escala_w	number(5);
ds_retorno_w	varchar2(1) := 'N';

begin

select	count(*)
into	qt_escala_w
from	agenda_escala
where	cd_agenda = cd_agenda_p;

if	(qt_escala_w > 0) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end obter_se_agenda_escala;
/