create or replace
function Obter_Valor_Audit_lib(	nr_seq_auditoria_p	number,
				nr_interno_conta_p	number,
				dt_liberacao_p		date,
				vl_auditoria_lib_p	number)
 		    	return number is
			
			
vl_item_w		number(15,2);
vl_total_w		number(15,2):=0;
qt_ajuste_w		number(15,3);
qt_original_w		number(15,3);
qt_dif_w		number(15,3);
ie_tipo_w		number(1,0);

vl_preco_w		number(15,2);
vl_unitario_w		number(15,4);
nr_interno_conta_w	number(10,0);

tx_proc_original_w	number(15,4);
tx_proc_ajuste_w	number(15,4);
tx_dif_w		number(15,4);
ie_tipo_auditoria_w	varchar2(1);
ie_tipo_item_w		varchar2(1);
nr_seq_audit_ext_w	number(10,0);
nr_seq_item_w		number(10,0);
nr_sequencia_w		number(10,0);
qt_procedimento_w	number(10,0);

cursor c01 is
	select	nvl(a.vl_material,0),
		b.qt_ajuste,
		b.qt_original,
		1,
		a.vl_unitario,
		null,
		null,
		'I',
		null,
		null
	from	material_atend_paciente a,
		auditoria_matpaci b
	where	a.nr_sequencia	    = b.nr_seq_matpaci
	and	b.nr_seq_auditoria  = nr_seq_auditoria_p
	and 	a.nr_interno_conta  = nr_interno_conta_p
	and	a.cd_motivo_exc_conta is null
	and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia
	union all
	select	nvl(a.vl_procedimento,0),
		b.qt_ajuste,
		b.qt_original,
		2,
		null,
		b.tx_proc_original,
		b.tx_proc_ajuste,
		'I',
		null,
		a.nr_sequencia
	from	procedimento_paciente a,
		auditoria_propaci b
	where	a.nr_sequencia	    = b.nr_seq_propaci 
	and 	a.nr_interno_conta  = nr_interno_conta_p
	and	b.nr_seq_auditoria  = nr_seq_auditoria_p
	and	a.cd_motivo_exc_conta is null
	and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia
	union all
	select	nvl(a.vl_material,0),
		b.qt_ajuste,
		b.qt_original,
		1,
		a.vl_unitario,
		null, 
		null,
		'E',
		b.nr_sequencia,
		a.nr_sequencia
	from	material_atend_paciente a,
		auditoria_externa_item  c,
		auditoria_externa 	b
	where	a.nr_sequencia	    = c.nr_seq_item
	and 	c.nr_seq_audit_ext  = b.nr_sequencia
	and	b.nr_seq_auditoria  = nr_seq_auditoria_p
	and 	a.nr_interno_conta  = nr_interno_conta_p
	and 	b.ie_tipo_item = 'M'
	and	a.cd_motivo_exc_conta is null
	and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia
	union all
	select	nvl(a.vl_procedimento,0),
		b.qt_ajuste,
		b.qt_original,
		2,
		null,
		null,
		null,
		'E',
		b.nr_sequencia,
		a.nr_sequencia
	from	procedimento_paciente   a,
		auditoria_externa_item  c,
		auditoria_externa 	b
	where	a.nr_sequencia	    = c.nr_seq_item
	and 	c.nr_seq_audit_ext  = b.nr_sequencia
	and 	a.nr_interno_conta  = nr_interno_conta_p
	and 	b.ie_tipo_item = 'P'
	and	b.nr_seq_auditoria  = nr_seq_auditoria_p
	and	a.cd_motivo_exc_conta is null
	and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia;
	
	
/*Cursor C02 is
	select	c.nr_seq_item
	from	auditoria_externa_item  c,
		auditoria_externa 	b
	where	c.nr_seq_audit_ext  = b.nr_sequencia
	and 	b.nr_seq_auditoria  = nr_seq_auditoria_p	
	and 	c.nr_seq_audit_ext  = nr_seq_audit_ext_w
	order by 1;*/
	
Cursor C02 is
	select	c.nr_seq_item
	from	material_atend_paciente a,
		auditoria_externa_item  c,
		auditoria_externa 	b
	where	a.nr_sequencia	    = c.nr_seq_item
	and 	c.nr_seq_audit_ext  = b.nr_sequencia
	and	b.nr_seq_auditoria  = nr_seq_auditoria_p
	and 	c.nr_seq_audit_ext  = nr_seq_audit_ext_w
	and 	b.ie_tipo_item = 'M'
	and	a.cd_motivo_exc_conta is null
	and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia
	and	a.vl_material > 0	
	union all
	select	c.nr_seq_item
	from	procedimento_paciente   a,
		auditoria_externa_item  c,
		auditoria_externa 	b
	where	a.nr_sequencia	    = c.nr_seq_item
	and 	c.nr_seq_audit_ext  = b.nr_sequencia
	and 	c.nr_seq_audit_ext  = nr_seq_audit_ext_w
	and 	b.ie_tipo_item = 'P'
	and	b.nr_seq_auditoria  = nr_seq_auditoria_p
	and	a.cd_motivo_exc_conta is null
	and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia
	and 	a.vl_procedimento > 0;
	
begin

if (dt_liberacao_p is not null and vl_auditoria_lib_p is not null) then
	return vl_auditoria_lib_p;
end if;

open C01;
loop
fetch C01 into	
	vl_item_w,
	qt_ajuste_w,
	qt_original_w,
	ie_tipo_w,
	vl_unitario_w,
	tx_proc_original_w,
	tx_proc_ajuste_w,
	ie_tipo_auditoria_w,
	nr_seq_audit_ext_w,
	nr_sequencia_w;
exit when C01%notfound;
	begin		
	vl_total_w	:= vl_total_w + vl_item_w;
	
	if	(ie_tipo_auditoria_w = 'I') then -- Auditoria Interna
	
		if 	(qt_ajuste_w is not null) then
			qt_dif_w:= qt_ajuste_w - qt_original_w;
		
			if	(ie_tipo_w = 1) then
					
				vl_total_w	:=  vl_total_w + (vl_unitario_w * qt_dif_w);
			
			elsif	(ie_tipo_w = 2) then
				
				if	(qt_original_w = 0) and (vl_item_w = 0) then					
					select 	nvl(max(obter_preco_procedimento (a.cd_estabelecimento, a.cd_convenio_parametro, a.cd_categoria_parametro, nvl(b.dt_conta, b.dt_procedimento), 
									      b.cd_procedimento, b.ie_origem_proced, 0, 0, 0, b.cd_medico_executor, b.ie_funcao_medico, null,null, 0,0,'P')),0)
					into	vl_item_w
					from	procedimento_paciente b,
					 	conta_paciente a
					where 	a.nr_interno_conta = nr_interno_conta_p
					and 	a.nr_interno_conta = b.nr_interno_conta
					and 	b.nr_sequencia = nr_sequencia_w;
					
					vl_preco_w:= vl_item_w;
				else
		
					vl_preco_w	:=  dividir(vl_item_w, qt_original_w);
				
				end if;				
				
				vl_total_w	:=  vl_total_w + (vl_preco_w * qt_dif_w);
		
			end if;
		
		elsif	(tx_proc_ajuste_w is not null) then
			tx_dif_w:= tx_proc_ajuste_w - tx_proc_original_w;
				
			vl_total_w:= vl_total_w + (vl_item_w * tx_dif_w / 100);
		end if;
		
	elsif	(ie_tipo_auditoria_w = 'E') then -- Auditoria Externa
	
		if 	(qt_ajuste_w is not null) then
			qt_dif_w:= qt_ajuste_w - qt_original_w;
		
			open C02;
			loop
			fetch C02 into	
				nr_seq_item_w;
			exit when C02%notfound;
				begin
				nr_seq_item_w:= nr_seq_item_w;
				end;
			end loop;
			close C02;
			
			if	(nr_seq_item_w = nr_sequencia_w) then
		
				if	(ie_tipo_w = 1) then
					
					vl_total_w	:=  vl_total_w + (vl_unitario_w * qt_dif_w);
			
				elsif	(ie_tipo_w = 2) then
										
					if	(vl_item_w < 0) then
						vl_item_w:= vl_item_w * -1;
					end if;
					
					select	a.qt_procedimento
					into	qt_procedimento_w
					from	procedimento_paciente a
					where	a.nr_sequencia = nr_sequencia_w;
					
					vl_preco_w	:=  dividir(vl_item_w, qt_procedimento_w); /* troquei o qt_original_w pelo qt_procedimento_w e adicionei o 
												select para buscar a qtd do procedimento, OS 186694*/
					vl_total_w	:=  vl_total_w + (vl_preco_w * qt_dif_w);
		
				end if;
				
			end if;
		
		end if;
	
	end if;
	
	end;
end loop;
close C01;

return	vl_total_w;

end Obter_Valor_Audit_lib;
/
