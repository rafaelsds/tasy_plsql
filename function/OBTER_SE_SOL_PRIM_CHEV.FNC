create or replace
function	obter_se_sol_prim_chev(
					nr_prescricao_p		number,
					nr_seq_item_p		number,
					nr_horario_evento_p	number,
					ie_tipo_solucao_p	number,
					nm_usuario_p		varchar2,
					ie_acao_p		number) return varchar2 is

ds_retorno_w			varchar2(1) := 'N';
ie_dialise_w			varchar2(1);
nr_etapa_sne_w			number(3);
nr_etapa_atual_sol_w	prescr_mat_hor.nr_etapa_sol%type;
nr_etapa_atual_w		prescr_solucao.nr_etapas%type;
nr_seq_ultimo_evento_w	hd_prescricao_evento.nr_sequencia%type;
nr_seq_horario_w		prescr_mat_hor.nr_sequencia%type;

begin

if 	(ie_tipo_solucao_p = 1) then
	if	(nr_horario_evento_p is not null) and
		(nr_horario_evento_p > 0)	then
		
		select	nvl(max(nr_etapa_sol),0)
		into	nr_etapa_atual_sol_w
		from	prescr_mat_hor
		where	nr_sequencia = nr_horario_evento_p;
				
		if	(nr_etapa_atual_sol_w is null) then
			nr_etapa_atual_sol_w := nr_horario_evento_p;
		end if;
		
	else
		nr_etapa_atual_sol_w := obter_etapa_atual(nr_prescricao_p, nr_seq_item_p);
	end if;
	
	if	(nr_etapa_atual_sol_w = 0) then
		nr_etapa_atual_sol_w := obter_etapa_atual(nr_prescricao_p, nr_seq_item_p);
	end if;	
	
	--Quando for reinicio
	if	((nvl(ie_acao_p,0) > 0) and (nvl(nr_etapa_atual_sol_w,0) > 0) and (ie_acao_p = 3)) then
		nr_etapa_atual_sol_w	:= nr_etapa_atual_sol_w +1;
	end if;
	
	--Tratamento criado para verificar se a etapa que esta sendo efetuada a acao nao esta suspensa
	select	min(a.nr_etapa_sol)
	into	nr_etapa_atual_w
	from	prescr_mat_hor a
	where	a.nr_prescricao = nr_prescricao_p
	and		a.nr_seq_solucao = nr_seq_item_p
	and		a.nr_etapa_sol >= nr_etapa_atual_sol_w
	and		a.dt_suspensao is null
	and		nvl(a.ie_horario_especial,'N') <> 'S'
	and		a.dt_inicio_horario is null;
	
	select 	nvl(max('S'), 'N')
	into 	ie_dialise_w
	from	prescr_solucao a,
			hd_prescricao b
	where 	a.nr_seq_dialise	= b.nr_sequencia
	and 	a.nr_prescricao 	= nr_prescricao_p
	and		a.nr_seq_solucao 	= nr_seq_item_p;
	
	if (ie_dialise_w = 'S') then
	
		select 	max(nr_sequencia)
		into	nr_seq_ultimo_evento_w
		from	hd_prescricao_evento a
		where 	a.nr_prescricao 	= nr_prescricao_p
		and 	a.nr_seq_solucao	= nr_seq_item_p;
	
		select	nvl(max('S'),'N')
		into	ds_retorno_w
		from	prescr_solucao a,
				hd_prescricao_evento b
		where 	a.nr_prescricao 	= b.nr_prescricao
		and		a.nr_seq_solucao 	= b.nr_seq_solucao
		and		a.nr_prescricao 	= nr_prescricao_p
		and		a.nr_seq_solucao 	= nr_seq_item_p
		and		b.ie_evento 		= 'PC'
		and		b.nr_sequencia		= nr_seq_ultimo_evento_w;
		
	else
	
		select	nvl(max('S'),'N')
		into	ds_retorno_w
		from	prescr_solucao	a,
				prescr_solucao_evento b
		where	a.nr_prescricao 	= b.nr_prescricao
		and		a.nr_seq_solucao 	= b.nr_seq_solucao
		and		b.nr_prescricao 	= nr_prescricao_p
		and		b.nr_seq_solucao 	= nr_seq_item_p
		and		b.ie_tipo_solucao 	= ie_tipo_solucao_p
		and		b.nr_etapa_evento 	= nr_etapa_atual_w
		and		b.ie_alteracao 		= 37
		and		nvl(b.ie_evento_valido,'S') = 'S';
		
	end if;
	
elsif	(ie_tipo_solucao_p = 2) then	
	
	nr_etapa_sne_w	:= obter_etapa_atual_sne(nr_prescricao_p,nr_seq_item_p);
	
	if	(nr_etapa_sne_w = 0) then
		nr_etapa_sne_w	:= nr_etapa_sne_w + 1;
	end if;
	
	select	min(nr_etapa_sol)
	into	nr_etapa_atual_w
	from	prescr_mat_hor
	where	nr_prescricao	= nr_prescricao_p
	and		nr_seq_material	= nr_seq_item_p
	and		nr_etapa_sol	>= nr_etapa_sne_w
	and		dt_suspensao is null
	and		nvl(ie_horario_especial,'N') <> 'S'
	and		dt_inicio_horario is null;

	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	prescr_material a,
			prescr_solucao_evento b
	where	a.nr_prescricao		= b.nr_prescricao
	and		a.nr_sequencia		= b.nr_seq_material
	and		b.nr_prescricao		= nr_prescricao_p
	and		b.nr_seq_material	= nr_seq_item_p
	and		b.ie_tipo_solucao	= ie_tipo_solucao_p
	and		b.nr_etapa_evento	= nr_etapa_atual_w
	and		b.ie_alteracao		= 37
	and		nvl(b.ie_evento_valido,'S') = 'S';

elsif	(ie_tipo_solucao_p = 3) then	--Hemoterapia
	
	nr_etapa_sne_w	:= OBTER_ETAPA_ATUAL_PROC(nr_prescricao_p,nr_seq_item_p);

	if	(nr_etapa_sne_w = 0) then
		nr_etapa_sne_w	:= nr_etapa_sne_w + 1;
	end if;

	select	min(a.nr_etapa)
	into	nr_etapa_sne_w
	from	prescr_proc_hor a
	where	a.nr_prescricao = nr_prescricao_p
	and	a.nr_seq_procedimento = nr_seq_item_p
	and	a.nr_etapa >= nr_etapa_sne_w
	and	a.dt_suspensao is null
	and	nvl(a.ie_horario_especial,'N') <> 'S'
	and	a.dt_inicio_horario is null;

	select  max(nr_sequencia)
	into	nr_seq_horario_w
	from    prescr_proc_hor
	where   nr_prescricao 	= nr_prescricao_p
	and     nr_seq_procedimento = nr_seq_item_p
	and     nr_etapa 	= nr_etapa_sne_w;

	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	prescr_procedimento a,
		prescr_solucao_evento b
	where	a.nr_prescricao 	= b.nr_prescricao
	and	a.nr_sequencia 		= b.nr_seq_procedimento
	and	b.nr_prescricao 	= nr_prescricao_p
	and	b.nr_seq_procedimento 	= nr_seq_item_p
	and	b.ie_tipo_solucao 	= ie_tipo_solucao_p
	and	((nvl(nr_seq_horario_w,0) = 0) or (b.nr_etapa_evento = nr_etapa_sne_w))
	and	b.ie_alteracao 		= 37
	and	nvl(b.ie_evento_valido,'S') = 'S';
	
	
end if;
return ds_retorno_w;

end obter_se_sol_prim_chev;
/
