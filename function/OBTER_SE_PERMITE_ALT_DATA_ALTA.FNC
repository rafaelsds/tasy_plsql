create or replace
function Obter_se_permite_alt_data_alta(cd_motivo_alta_p number)
 		    	return varchar2 is

ds_retorno_w	varchar2(10) := 'S';

begin

select	nvl(max(ie_alt_data_medica),'S')
into	ds_retorno_w
from	motivo_alta
where	cd_motivo_alta = cd_motivo_alta_p;


return	ds_retorno_w;

end Obter_se_permite_alt_data_alta;
/
