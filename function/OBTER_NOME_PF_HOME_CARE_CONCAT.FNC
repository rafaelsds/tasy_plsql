create or replace
function obter_nome_pf_home_care_concat( nr_seq_p number,
					 dt_parametro_ini_p	date,
					 dt_parametro_fim_p	date)
				return varchar2 is
				
ds_retorno_w varchar2(4000) := '';
nm_pessoa_w varchar2(255);

Cursor C01 is
select 	distinct
	obter_nome_pf(x.cd_pessoa_fisica)
from 	paciente_home_care x,
	hc_pac_equipamento y
where 	x.dt_final is null
and 	y.nr_seq_paciente = x.nr_sequencia
and 	y.nr_seq_equip_control is not null
and 	y.nr_seq_equip_control = nr_seq_p
and	((dt_parametro_ini_p    between dt_inicio_utilizacao and dt_fim_utilizacao) or 
	 (dt_parametro_fim_p    between dt_inicio_utilizacao and dt_fim_utilizacao) or
	 (dt_inicio_utilizacao between dt_parametro_ini_p and dt_parametro_fim_p) or
	 (dt_fim_utilizacao    between dt_parametro_ini_p and dt_parametro_fim_p) or
	 (dt_fim_utilizacao is null))
order by 1;

begin

if (nr_seq_p is not null) then
	open C01;
	loop
	fetch C01 into
		nm_pessoa_w;
	exit when C01%notfound;
	begin
	ds_retorno_w :=  ds_retorno_w || nm_pessoa_w || ','|| chr(13) || chr(10);
	end;
	end loop;
	close C01;
end if;

if ( ds_retorno_w is not null) then
	ds_retorno_w := substr( ds_retorno_w ,1,length( ds_retorno_w )-1);
end if;
ds_retorno_w:= wheb_mensagem_pck.get_texto(802874) || ' ' || chr(13) || chr(10) || ds_retorno_w;
return ds_retorno_w;

end obter_nome_pf_home_care_concat;
/
