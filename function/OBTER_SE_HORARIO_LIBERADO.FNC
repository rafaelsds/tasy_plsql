create or replace
function Obter_se_horario_liberado(	dt_lib_horario_p	date,
									dt_horario_p		date)
     return char deterministic is
begin

if	(dt_lib_horario_p is not null) then
	return 'S';
end if;

return 'N';

end Obter_se_horario_liberado;
/
