create or replace 
function Obter_Valor_Lancto_Automatico
		(cd_estabelecimento_p		number,
		cd_procedimento_p			number,
		ie_origem_proced_p			number,
		cd_convenio_p				number,
		cd_categoria_p			varchar2,
		dt_vigencia_p				date,
		ie_tipo_atendimento_p		number,
		nr_seq_exame_p			number)
		return number is

vl_lancamento_w		number(15,4) := 0;
nr_seq_lancto_w		number(10,0);
cd_proc_lancto_w		number(15,0);
ie_origem_lancto_w		number(10,0);
cd_material_lancto_w		number(06,0);
qt_lancamento_w		number(15,4);
vl_material_w			number(15,6);
vl_procedimento_w		number(15,6);
vl_outros_w			number(15,6);
dt_ult_vigencia_w		date;
cd_tab_preco_mat_w		number(10,0);
ie_origem_preco_w		number(10,0);
cd_edicao_amb_w		number(10,0);
nr_sequencia_w		number(10,0) := 0;
cd_tipo_acomodacao_w		number(10,0) := 0;
cd_setor_atendimento_w	number(10,0) := 0;
qt_idade_w			number(10,0) := 0;
cd_funcao_medico_w		number(10,0) := 0;

cd_usuario_convenio_w	Varchar2(40);
cd_plano_w			Varchar2(20);
ie_clinica_w			Number(15,0);
cd_empresa_ref_w		Number(15,0);
nr_seq_bras_preco_w		number(10,0);
nr_seq_mat_bras_w		number(10,0);
nr_seq_conv_bras_w		number(10,0);
nr_seq_conv_simpro_w		number(10,0);
nr_seq_mat_simpro_w		number(10,0);
nr_seq_simpro_preco_w		number(10,0);
nr_seq_ajuste_mat_w		number(10,0);
ie_situacao_w  varchar2(1);

cursor	c01 is
	select	cd_procedimento, 
		ie_origem_proced, 
		cd_material, 
		qt_lancamento + 1
	from	regra_lanc_aut_pac
	where	nr_seq_regra	= nr_seq_lancto_w
	and 	nvl(ie_adic_orcamento,'N') = 'N';  

begin

select	nvl(max(nr_sequencia), 0)
into	nr_seq_lancto_w
from	regra_lanc_automatico
where	cd_estabelecimento		= cd_estabelecimento_p
and	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced		= ie_origem_proced_p
and	(cd_convenio is null or cd_convenio = cd_convenio_p)
and	nvl(cd_categoria, cd_categoria_p)	= cd_categoria_p
and	nr_seq_exame			= nr_seq_exame_p;

if	(nr_seq_lancto_w > 0) then
	begin
	open	c01;
	loop
	fetch	c01 into
		cd_proc_lancto_w,
		ie_origem_lancto_w,
		cd_material_lancto_w,
		qt_lancamento_w;
	exit	when	c01%notfound;
		begin

		if	(cd_material_lancto_w is not null) then
			begin

      
      select 	nvl(max(ie_situacao),'A')
      into	ie_situacao_w
      from	material
      where	cd_material	 = cd_material_lancto_w;
      
      if	(ie_situacao_w = 'A') then
        define_preco_material
				(cd_estabelecimento_p,
				cd_convenio_p,
				cd_categoria_p,
				dt_vigencia_p,
				cd_material_lancto_w,
				0, ie_tipo_atendimento_p,
				0, '', 0, 0, cd_plano_w, null, null, null, null, null, null, null,
				vl_material_w,
				dt_ult_vigencia_w,
				cd_tab_preco_mat_w,
				ie_origem_preco_w,
				nr_seq_bras_preco_w,
				nr_seq_mat_bras_w,
				nr_seq_conv_bras_w,
				nr_seq_conv_simpro_w,
				nr_seq_mat_simpro_w,
				nr_seq_simpro_preco_w,
				nr_seq_ajuste_mat_w);
			
        vl_lancamento_w	:= vl_lancamento_w + (qt_lancamento_w * vl_material_w);							

        end if;
			end;
		end if;

		if	(cd_proc_lancto_w is not null) then
			begin
		
      select 	nvl(max(ie_situacao),'A')
      into	ie_situacao_w
      from	procedimento
      where	cd_procedimento	 = cd_proc_lancto_w
      and	ie_origem_proced = ie_origem_lancto_w;
		
      if	(ie_situacao_w = 'A') then
        select	obter_preco_procedimento
				(cd_estabelecimento_p,
				cd_convenio_p,
				cd_categoria_p,
				dt_vigencia_p,
				cd_proc_lancto_w,
				ie_origem_lancto_w,
				0, 0, 0, null, null, 
				cd_usuario_convenio_w, cd_plano_w, ie_clinica_w, cd_empresa_ref_w,
				'P')
        into	vl_procedimento_w
        from	dual;




        vl_lancamento_w	:= vl_lancamento_w + (qt_lancamento_w * vl_procedimento_w); 
      end if;      
			end;
		end if;
		end;	
	end loop;
	close	c01;
	end;
end if;


if	(vl_lancamento_w = 0) then
	select	obter_preco_procedimento
		(cd_estabelecimento_p,
		cd_convenio_p,
		cd_categoria_p,
		dt_vigencia_p,
		cd_procedimento_p,
		ie_origem_proced_p,
		0, 0, 0, null, null, 
		cd_usuario_convenio_w, cd_plano_w, ie_clinica_w, cd_empresa_ref_w,
		'P')
	into	vl_lancamento_w
	from	dual;
end if;

return	vl_lancamento_w;

end Obter_Valor_Lancto_Automatico;
/
