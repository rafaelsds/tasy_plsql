create or replace function GET_CHECKSUM_ACTIVATION( cd_key_p Varchar2)
        return Varchar is
 
ds_caracter_w  Varchar2(1);
ds_retorno_w  Varchar2(255);
ds_Char_Table_w  Varchar2(255);
i   integer  := 0;
j   integer  := 0;
vl_value_w  Number(10);
vl_total_w  Number(10);
vl_final_w  Number(10);
tamanho    Number(15);

begin
ds_retorno_w := '';
ds_Char_Table_w := '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ*';
vl_total_w := 0;
vl_final_w := 0;

if (cd_key_p is not null) then

 tamanho := length(cd_key_p);

 for i in 1..tamanho loop
  begin
  ds_caracter_w := substr(cd_key_p, i,1);
  j := 0;
  while (length(ds_Char_Table_w) > j) loop
   begin
   j := j + 1;
   if (substr( ds_Char_Table_w, j,1) = to_char(ds_caracter_w)) then
    begin
    vl_value_w := trunc(j-1);
    vl_total_w := trunc((vl_value_w * (power(2, tamanho - i+1))));

    vl_final_w := vl_final_w + vl_total_w;
    end;
   end if;
   end;
  end loop;
  end;
 end loop;

 select substr(ds_Char_Table_w,mod(38 - mod(vl_final_w,37),37)+1,1)
 into ds_retorno_w
 from dual;
end if;

return ds_retorno_w;

end GET_CHECKSUM_ACTIVATION;
/