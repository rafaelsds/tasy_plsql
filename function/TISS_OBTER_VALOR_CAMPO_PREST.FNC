create or replace
function TISS_OBTER_VALOR_CAMPO_PREST(nr_seq_procedimento_p	in number,
					nr_seq_campo_prest_p	in number) 
						return	varchar2 is

ds_retorno_w	varchar2(255);

begin

select	max(ds_campo)
into	ds_retorno_w
from	proc_paciente_tiss_prest
where	nr_seq_procedimento	= nr_seq_procedimento_p
and	nr_seq_campo_prest	= nr_seq_campo_prest_p;

ds_retorno_w	:= nr_seq_campo_prest_p||';'||ds_retorno_w;

return	ds_retorno_w;

end TISS_OBTER_VALOR_CAMPO_PREST;
/
