create or replace function obter_texto_long(ds_tabela_p      varchar2,
                                            ds_campo_long_p  varchar2,
                                            ds_campo_chave_p varchar2,
                                            ds_valor_chave_p number) return varchar2 is

   cursor_w       binary_integer;
   sql_w          varchar2(100);
   value_w        varchar2(32760);
   clob_w         clob;
   value_length_w integer := 32760;
   length_w       integer := 0;

   in_html        boolean := false;
   caracter_w     varchar2(1);
   clob_tmp_w     clob;

 begin

   sql_w := 'select ' || ds_campo_long_p || ' from ' || ds_tabela_p;

   if trim(ds_campo_chave_p) is not null and
      trim(ds_valor_chave_p) is not null
   then
     sql_w := sql_w || ' where ' || ds_campo_chave_p || ' = ' || ds_valor_chave_p;
   end if;

   cursor_w := dbms_sql.open_cursor;
   dbms_sql.parse(cursor_w, sql_w, dbms_sql.native);
   dbms_sql.define_column_long(cursor_w, 1);

   if dbms_sql.execute_and_fetch(cursor_w) = 1 then
     loop
       dbms_sql.column_value_long(cursor_w,
                                  1,
                                  32760,
                                  length_w,
                                  value_w,
                                  value_length_w);
       clob_w   := clob_w || value_w;
       length_w := length_w + 32760;
       exit when value_length_w < 32760;
     end loop;
   end if;

   if dbms_sql.is_open(cursor_w) then
     dbms_sql.close_cursor(cursor_w);
   end if;

   clob_w := trim(replace(replace(rtrim(rtrim(ltrim(ltrim(clob_w,
                                                          chr(13) || chr(10)),
                                                    chr(10)),
                                              chr(13) || chr(10)),
                                         chr(10)),
                                   chr(160),
                                   ' '),
                           chr(9),
                           ' '));

  if clob_w is not null then

    if dbms_lob.substr(clob_w, 1, 1) = '{' then --rtf

      clob_w := trim(ltrim(ltrim(regexp_replace(regexp_replace(clob_w,'(\\par |\\par)'), '(\\.* |\\.*|{.*|}.*)'), chr(13) || chr(10)), chr(10)));

    end if;

    if dbms_lob.substr(clob_w, 1, 1) = '<' then

      for i in 1 .. dbms_lob.getlength( clob_w ) loop

        caracter_w := substr( clob_w, i, 1 );

        if in_html then

          if caracter_w = '>' then

            in_html := false;

            if clob_tmp_w is not null then
              clob_tmp_w := clob_tmp_w || ' ';
            end if;

          end if;

        elsif caracter_w = '<' then

          in_html := true;

        end if;

        if not in_html and caracter_w <> '>' then

          clob_tmp_w := clob_tmp_w || caracter_w;

        end if;

      end loop;

      clob_w := rtrim(replace(clob_tmp_w, chr(38)||'#x200B;'), chr(10));

    end if;

    clob_w := trim(ltrim(regexp_replace(trim(replace(replace(clob_w, chr(13) || chr(10), ' '), chr(10), ' ')), '[[:space:]]{2,}', ' '), '.'));
    
  end if;

  return dbms_lob.substr(clob_w, 32767, 1);

end obter_texto_long;
/
