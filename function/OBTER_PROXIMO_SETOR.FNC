create or replace
function obter_proximo_setor(	nr_seq_interno_p	number)
					return number is

cd_setor_atendimento_w	number(5);
cd_proximo_setor_w	number(5);
nr_atendimento_w		number(15);
nr_seq_interno_w		number(15);

BEGIN

/* Obter o setor do par�metro*/

select	max(cd_setor_atendimento),
	max(nr_atendimento)
into	cd_setor_atendimento_w,
	nr_atendimento_w
from	atend_paciente_unidade
where	nr_seq_interno = nr_seq_interno_p;

/* Obter a pr�xima movimenta��o do pac no atendimento apos o atual */

select	nvl(min(nr_seq_interno),0)
into	nr_seq_interno_w
from	atend_paciente_unidade
where	nr_atendimento	= nr_atendimento_w
and	nr_seq_interno	> nr_seq_interno_p;

if	(nr_seq_interno_w > 0) then
	select	nvl(a.cd_setor_atendimento,cd_setor_atendimento_w)
	into	cd_proximo_setor_w
	from	atend_paciente_unidade a
	where	nr_atendimento	= nr_atendimento_w
	and	nr_seq_interno	= nr_seq_interno_w;
else
	cd_proximo_setor_w	:= 0;
end if;

return cd_proximo_setor_w;

END obter_proximo_setor;
/