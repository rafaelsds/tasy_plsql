create or replace
function obter_se_audit_periodo(nr_sequencia_p	number)
 		    	return varchar2 is

qt_audit_periodo_w	number(10,0);
nr_interno_conta_w	number(10,0);
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;	
			
			
begin

select 	nvl(max(nr_interno_conta),0),
	max(dt_periodo_inicial),
	max(dt_periodo_final)
into	nr_interno_conta_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w
from 	auditoria_conta_paciente
where 	nr_sequencia = nr_sequencia_p;

select 	count(*)
into	qt_audit_periodo_w
from 	auditoria_conta_paciente
where 	nr_interno_conta = nr_interno_conta_w
and 	nr_sequencia <> nr_sequencia_p
and 	dt_periodo_inicial = dt_periodo_inicial_w
and 	dt_periodo_final = dt_periodo_final_w;

if	(qt_audit_periodo_w > 0) then
	return	'S';
else
	return  'N';
end if;

end obter_se_audit_periodo;
/