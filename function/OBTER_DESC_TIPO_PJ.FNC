CREATE OR REPLACE FUNCTION
OBTER_DESC_TIPO_PJ	(CD_TIPO_PESSOA_P	NUMBER,
			IE_OPCAO_P		VARCHAR2)
			RETURN VARCHAR2 IS

cd_tipo_pj_w			NUMBER(03,0);
ds_tipo_pj_w			VARCHAR2(254);
ds_retorno_w			VARCHAR2(255);

/* IE_OPCAO_P

'C' - CODIGO
'D' - DESCRICAO
*/


begin

select	cd_tipo_pessoa,
	ds_tipo_pessoa
into	cd_tipo_pj_w,
	ds_tipo_pj_w
from	tipo_pessoa_juridica
where	cd_tipo_pessoa = cd_tipo_pessoa_p;

if (ie_opcao_p = 'C') then
	ds_retorno_w := cd_tipo_pj_w;
elsif (ie_opcao_p = 'D') then
	ds_retorno_w := ds_tipo_pj_w;	
end if;
return ds_retorno_w;

END OBTER_DESC_TIPO_PJ;
/