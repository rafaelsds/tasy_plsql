create or replace
function obter_fonte_panorama ( cd_perfil_p		number)
				return varchar2 is

ds_retorno_w		varchar2(255);
qt_tamanho_fonte_w	regra_perm_panorama.qt_tamanho_fonte%type;

begin

select	max(qt_tamanho_fonte)
into 	qt_tamanho_fonte_w
from	regra_perm_panorama
where	cd_perfil = cd_perfil_p;
	
if (qt_tamanho_fonte_w is null) then  -- se n�o encontrou a regra para o perfil, busca uma regra sem perfil definido.
	select	max(qt_tamanho_fonte)
	into 	qt_tamanho_fonte_w
	from	regra_perm_panorama
	where	cd_perfil is null;
end if;

if (qt_tamanho_fonte_w is null) then
	ds_retorno_w	:= '12'; -- valor padr�o do Panorama.
else
	ds_retorno_w	:= qt_tamanho_fonte_w;
end if;

return	ds_retorno_w;

end obter_fonte_panorama;
/