create or replace
function lab_consitir_val_interf_aprov	(nr_prescricao_p	number,
										nr_seq_prescr_p		number)
					return number is
qt_exames_consist_w	number(5);
nr_seq_result_w		number(10);
qt_itens_w			number(10);
vl_resultado_w 		number(15,4);
nr_seq_regra_w		number(10);
ds_regra_w			varchar2(255);
qt_resultado_desc_w varchar2(15);

Cursor C01 is
	select 	b.nr_sequencia,
			b.ds_resultado,
			nvl(a.qt_resultado,0)
	from 	lab_exame_valor_interf b,
			exame_lab_result_item a
	where	((a.nr_seq_exame = b.nr_seq_exame) or (b.nr_seq_grupo = obter_grupo_exame_lab(a.nr_seq_exame)))
	and		a.nr_seq_resultado = nr_seq_result_w
	and		a.nr_seq_prescr = nr_seq_prescr_p
	and		a.qt_resultado is not null
	and		(b.ds_resultado like '%>%' or b.ds_resultado like '%<%' or b.ds_resultado like '%=%' or b.ds_resultado like '%between%');
	
begin

select 	count(*) 
into	qt_itens_w
from	lab_exame_valor_interf;

qt_exames_consist_w	:= 0;

if	(qt_itens_w > 0) then

	select 	max(nr_seq_resultado)
	into	nr_seq_result_w
	from	exame_lab_resultado
	where	nr_prescricao_p = nr_prescricao;

	--tratamento para verificar apenas os exames que est�o no formato. OS 538327 (ajustado na OS 557338 para 'exists' pois a liga��o com a exame_lab_format_item n�o retornava valor para os analitos)
	SELECT  count(*)
	INTO	qt_exames_consist_w
	FROM 	exame_lab_result_item a,
			lab_exame_valor_interf b
	WHERE	a.nr_seq_prescr = nr_seq_prescr_p
	AND	((a.nr_seq_exame = b.nr_seq_exame) OR (b.nr_seq_grupo = obter_grupo_exame_lab(a.nr_seq_exame)))
	AND	a.qt_resultado	= b.qt_resultado
	AND	a.nr_seq_resultado = nr_seq_result_w
	AND	EXISTS (SELECT 1 
			FROM   exame_lab_format_item x
			WHERE  x.nr_seq_exame = a.nr_seq_exame);
	
	if	(qt_exames_consist_w = 0) then
		
		select 	count(*)
		into	qt_exames_consist_w
		from 	exame_lab_result_item a,
				lab_exame_valor_interf b,
				exame_laboratorio c
		where	nr_seq_prescr = nr_seq_prescr_p
		and		((a.nr_seq_exame = b.nr_seq_exame) or (b.nr_seq_grupo = obter_grupo_exame_lab(a.nr_seq_exame)))
		and		a.ds_resultado	= b.ds_resultado
		and		a.nr_Seq_exame = c.nr_seq_exame
		and		ie_formato_resultado in ('SM','D','DL','SDM','MS','S')
		and		a.nr_seq_resultado = nr_seq_result_w;	
	
		if (qt_exames_consist_w = 0) then
			--tratamento para regra com simbolo de > , < e =
			open c01;
			loop
			fetch c01 into 	nr_seq_regra_w,
							ds_regra_w,
							qt_resultado_desc_w;
			exit when c01%notfound;
				
				obter_valor_dinamico('select 1 from dual where ' || qt_resultado_desc_w || ds_regra_w, vl_resultado_w);
				
				if (vl_resultado_w is null) or (vl_resultado_w = 0) then
					obter_valor_dinamico('select 1 from dual where ' || qt_resultado_desc_w || replace(ds_regra_w, ',', '.'), vl_resultado_w);
				end if;

				if (nvl(vl_resultado_w,0) > 0) then
					qt_exames_consist_w := vl_resultado_w;
					exit;
				end if;
				
			end loop;
			close c01;
			
		end if;
	end if;
end if;	
	
return qt_exames_consist_w;

end;
/
