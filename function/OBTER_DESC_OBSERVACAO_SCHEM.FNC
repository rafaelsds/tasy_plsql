create or replace
function obter_desc_observacao_schem(nr_seq_obj_sch_p	OBJETO_SCHEMATIC_PARAM.nr_seq_obj_sch%type,
													  cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
													  cd_perfil_p	perfil.cd_perfil%type)
return varchar2 is

ds_retorno_w		obj_schem_perfil.ds_observacao%type;
nr_seq_regra_w	objeto_schematic_param.nr_sequencia%type;
begin

begin
select	nr_sequencia
into	nr_seq_regra_w
from	objeto_schematic_param
where	nr_seq_obj_sch = nr_seq_obj_sch_p;
exception
when others then
	nr_seq_regra_w	:=	0;	
end;

if	(nr_seq_regra_w > 0) then
	begin
		select	ds_observacao
		into	ds_retorno_w
		from	obj_schem_perfil
		where	nr_seq_obj_schem_param = nr_seq_regra_w
		and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
		and	cd_perfil = cd_perfil_p;
	exception
	when others then
		begin
			select	ds_observacao
			into	ds_retorno_w
			from	obj_schem_estab
			where	nr_seq_obj_schem_param = nr_seq_regra_w
			and	cd_estabelecimento = cd_estabelecimento_p;
		exception
		when others then
			select	ds_observacao
			into	ds_retorno_w
			from	objeto_schematic_param
			where	nr_sequencia = nr_seq_regra_w;
		end;
	end;
end if;

return	ds_retorno_w;

end obter_desc_observacao_schem;
/