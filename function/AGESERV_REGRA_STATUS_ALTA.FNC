create or replace
function  ageserv_regra_status_alta(nr_atendimento_p		number,
				    ie_status_agenda_p		varchar2)
 		    	return varchar2 is

qt_regra_w		number(10,0);
dt_alta_w		date;
qt_contas_w		number(10,0);
ie_retorno_w		varchar2(1) := 'N';
				    
begin

if	(nr_atendimento_p > 0) then
	select	count(*)
	into	qt_regra_w
	from	agenda_status_alta
	where	ie_status_agenda = ie_status_agenda_p;

	if	(qt_regra_w > 0) then
		select	max(dt_alta)
		into	dt_alta_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;
			
		select	count(*)
		into	qt_contas_w
		from	conta_paciente
		where	nr_atendimento 	 = nr_atendimento_p
		and	ie_status_acerto = 1;
		
		if	(dt_alta_w is not null) and
			(qt_contas_w = 0) then
			ie_retorno_w	:= 'S';
		end if;
	end if;
end if;

return	ie_retorno_w;

end ageserv_regra_status_alta;
/