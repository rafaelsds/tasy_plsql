create or replace
function obter_resultado_jh_frat( qt_pontos_p	Number)
							return varchar2 is

ds_retorno_w		Varchar2(100);

begin
ds_retorno_w	:= '';
if	(qt_pontos_p between 0 and 5) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308203); -- Baixo risco
elsif	(qt_pontos_p between 6 and 13) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309297); -- Risco moderado
elsif	(qt_pontos_p > 13) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308205); -- Alto risco
end if;

return ds_retorno_w;

end obter_resultado_jh_frat;
/