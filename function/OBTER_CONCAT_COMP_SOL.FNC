create or replace
function obter_Concat_comp_sol	(nr_prescricao_p	number,
					nr_seq_solucao_p	number)
					return varchar2 is

nr_seq_comp_w		number(10,0);
ds_componente_w		varchar2(100);
ds_reduzida_w		varchar2(100);
ds_material_w       varchar2(100);

cursor c01 is
select	substr(nvl(b.ds_abrev_solucao,b.ds_reduzida), 1, 100)
from	prescr_material a,
		material b,
		prescr_medica c
where	a.nr_prescricao 		= c.nr_prescricao
and		a.nr_prescricao 		= nr_prescricao_p
and		a.nr_sequencia_solucao 	= nr_seq_solucao_p
and		a.cd_material			= b.cd_material
and 	not exists (SELECT c.nr_prescricao 
					FROM prescr_material c
					WHERE c.nr_prescricao	= a.nr_prescricao
					and	c.nr_sequencia = a.nr_seq_substituto)
and		nvl(c.cd_funcao_origem,924) <> 2314;			

cursor c02 is
select	substr(nvl(b.ds_abrev_solucao,b.ds_reduzida), 1, 100)
from	prescr_material a,
		material b,
		prescr_medica c
where	a.nr_prescricao 		= c.nr_prescricao 
and		a.nr_prescricao 		= nr_prescricao_p
and		a.nr_sequencia_solucao 	= nr_seq_solucao_p
and		a.cd_material			= b.cd_material
and 	not exists (SELECT c.nr_prescricao 
					FROM prescr_material c
					WHERE c.nr_prescricao	= a.nr_prescricao
					and	c.nr_sequencia = a.nr_seq_substituto)
and		nvl(c.cd_funcao_origem,924) = 2314				
order by ordena_item_cpoe(a.nr_seq_mat_cpoe, a.cd_material);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) then
	
	open C01;
	loop
	fetch C01 into	
		ds_reduzida_w;
	exit when C01%notfound;
		if	(ds_componente_w is not null) then
			ds_componente_w	:= substr(ds_componente_w || ' + ' || ds_reduzida_w,1,100);
		else
			ds_componente_w	:= substr(ds_reduzida_w,1,100);
		end if;
	end loop;
	close C01;
	
	
	open C02;
	loop
	fetch C02 into	
		ds_reduzida_w;
	exit when C02%notfound;
		if	(ds_componente_w is not null) then
			ds_componente_w	:= substr(ds_componente_w || ' + ' || ds_reduzida_w,1,100);
		else
			ds_componente_w	:= substr(ds_reduzida_w,1,100);
		end if;
	end loop;
	close C02;
	
    select  substr(max(obter_desc_material(a.cd_material)), 1, 100)
    into    ds_material_w
    from    prescr_material a
    where   a.nr_prescricao         = nr_prescricao_p
    and     a.nr_sequencia_solucao 	= nr_seq_solucao_p;
    
end if;

return nvl(ds_componente_w, ds_material_w);

end obter_Concat_comp_sol;
/
