create or replace
function sus_obter_dados_param_aih (	cd_estabelecimento_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(100);					
cd_diretor_clinico_w	varchar2(10);
cd_medico_autorizador_w varchar2(10);
/*
Op��es
DC - Diretor cl�nico
MA - M�dico autorizador
*/

begin

select	cd_diretor_clinico,
	cd_medico_autorizador
into	cd_diretor_clinico_w,
	cd_medico_autorizador_w
from	SUS_PARAMETROS_AIH
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_opcao_p = 'DC') then
	ds_retorno_w := cd_diretor_clinico_w;
elsif	(ie_opcao_p = 'MA') then
	ds_retorno_w := cd_medico_autorizador_w;
end if;

return	ds_retorno_w;

end sus_obter_dados_param_aih;
/