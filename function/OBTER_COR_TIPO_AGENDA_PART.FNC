create or replace
function obter_cor_tipo_agenda_part (	nr_seq_tipo_agenda_p	number)
					return	varchar2 is
ds_retorno_w	varchar2(40);

begin

if	(nr_seq_tipo_agenda_p is not null) then
	select	max(ds_cor)
	into	ds_retorno_w
	from	tipo_agenda_tasy
	where	nr_sequencia = nr_seq_tipo_agenda_p;
end if;

return ds_retorno_w;

end obter_cor_tipo_agenda_part;
/
