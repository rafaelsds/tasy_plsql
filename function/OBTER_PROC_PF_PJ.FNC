create or replace
function obter_proc_pf_pj(nr_sequencia_p		number)
 		    	return number is

ie_tipo_nota_w		nota_fiscal.ie_tipo_nota%type;
cd_estabelecimento_w	nota_fiscal.cd_estabelecimento%type;
cd_pessoa_fisica_w	nota_fiscal.cd_pessoa_fisica%type;
cd_cgc_emitente_w	nota_fiscal.cd_cgc_emitente%type;
cd_procedimento_w	nota_fiscal_item.cd_procedimento%type;

begin

select	max(ie_tipo_nota),
	max(cd_estabelecimento),
	max(cd_pessoa_fisica),
	max(cd_cgc_emitente)
into	ie_tipo_nota_w,
	cd_estabelecimento_w,
	cd_pessoa_fisica_w,
	cd_cgc_emitente_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

if	(ie_tipo_nota_w = 'EF') then
	begin
	
	select	max(cd_procedimento)
	into	cd_procedimento_w
        from    pessoa_fis_conta_nf
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;

	end;
else
	begin
	
	select  max(cd_procedimento)
	into	cd_procedimento_w
        from    mat_lib_estrut_fornec
        where   cd_cnpj_fornec = cd_cgc_emitente_w
        and     cd_estabelecimento = cd_estabelecimento_w;
	       
	end;
end if;

return	cd_procedimento_w;

end obter_proc_pf_pj;
/