create or replace
function Obter_se_bloqueio_atendimento(	nr_atendimento_p	number)
					return varchar2 is

ds_retorno_w			varchar2(1) := 'N';					
dt_entrada_w			Date;
cd_convenio_w			Number(10);
cd_estabelecimento_w	Number(14,0);
ie_tipo_atendimento_w	Varchar2(255);
qt_reg_w				Number(10);
qt_horas_w				Number(14,0);				
				
begin



if	(nr_atendimento_p is not null) and
	(nr_atendimento_p > 0) then
	
	select	count(1)
	into	qt_reg_w
	from	regra_bloqueio_atendimento;
	
	if(qt_reg_w	> 0)then

		cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
		
		Select 	ie_tipo_atendimento,
				dt_entrada,
				nvl(obter_convenio_atendimento(nr_atendimento),0)
		into	ie_tipo_atendimento_w,
				dt_entrada_w,
				cd_convenio_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;
			
		qt_horas_w := ((sysdate - dt_entrada_w)*24);
		
		select	count(*)
		into	qt_reg_w
		from	regra_bloqueio_atendimento
		where	ie_tipo_atendimento = ie_tipo_atendimento_w
		and		nvl(cd_convenio,cd_convenio_w) = cd_convenio_w
		and		nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
		and		qt_horas_w between qt_hora_inicial and qt_hora_final;

		if	(qt_reg_w > 0) then

			ds_retorno_w := 'S';

		end if;
	end if;
	
end if;

return	ds_retorno_w;

end Obter_se_bloqueio_atendimento;
/
