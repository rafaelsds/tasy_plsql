create or replace
function pls_status_solic_carteira
			(	nr_seq_carteira_p	number,
				ie_retorno_p		varchar2 default 'D')
 		    		return varchar2 is

ds_retorno_w				varchar2(120);
ie_sit_carteira_segurado_w		varchar2(1);
ie_situacao_carteira_emissao_w		varchar2(1);
ie_situacao_lote_w			varchar2(1);
nr_seq_lote_w				number(10);
nr_seq_lote_emissao_w			number(10);
dt_entrega_beneficiario_w		date;
dt_envio_lote_w				date;
qt_solicitacao_pendente_w		number(10);
vl_dominio_w				varchar2(10);

begin

select	count(*)
into	qt_solicitacao_pendente_w
from	pls_solic_carteira_adic
where	nr_seq_carteira	= nr_seq_carteira_p
and	ie_status	= 0; --Pendente

if	(qt_solicitacao_pendente_w > 0) then
	if	(ie_retorno_p = 'C') then
		ds_retorno_w	:= 1;
	elsif	(ie_retorno_p = 'D') then
		ds_retorno_w	:= obter_valor_dominio(2587, 1); --1 - Analise
	end if;	
else
	-- Obter a situacao da carteira do segurado
	select	nvl(max(ie_situacao), 'D'),
		max(nr_seq_lote_emissao)
	into	ie_sit_carteira_segurado_w,
		nr_seq_lote_emissao_w
	from	pls_segurado_carteira
	where	nr_sequencia	= nr_seq_carteira_p;

	-- Obter o ultimo lote da carteirinha
	select	nvl(max(b.nr_seq_lote),0)
	into	nr_seq_lote_w
	from	pls_carteira_emissao	b,
		pls_segurado_carteira	a
	where	a.nr_sequencia		= b.nr_seq_seg_carteira
	and	a.nr_sequencia		= nr_seq_carteira_p;

	-- Obter o status e o recebimento da carteira pela grafica
	select	nvl(max(ie_situacao), 'P'),
		max(dt_entrega_beneficiario)
	into	ie_situacao_carteira_emissao_w,
		dt_entrega_beneficiario_w
	from	pls_carteira_emissao
	where	nr_seq_seg_carteira	= nr_seq_carteira_p
	and	nr_seq_lote		= nr_seq_lote_w;

	select	max(dt_envio),
		nvl(max(ie_situacao), 'D')
	into	dt_envio_lote_w,
		ie_situacao_lote_w
	from	pls_lote_carteira
	where	nr_sequencia	= nr_seq_lote_w;

	if	(ie_sit_carteira_segurado_w = 'P') and
		(nr_seq_lote_emissao_w is null) then
		vl_dominio_w	:= '1'; -- Analise
	elsif	(dt_entrega_beneficiario_w is not null) then
		vl_dominio_w	:= '4'; -- Recebida
	elsif	(ie_situacao_carteira_emissao_w = 'D') and
		(dt_entrega_beneficiario_w is null) then
		vl_dominio_w	:= '3'; -- Disponivel para entrega
	elsif	(ie_situacao_carteira_emissao_w = 'P') then
		vl_dominio_w	:= '2'; -- Lote gerado
	elsif	(ie_sit_carteira_segurado_w = 'P') then
		vl_dominio_w	:= '1'; -- Analise
	end if;
	
	if	(ie_retorno_p = 'C') then
		ds_retorno_w	:= vl_dominio_w;
	elsif	(ie_retorno_p = 'D') then
		ds_retorno_w	:= obter_valor_dominio(2587, vl_dominio_w);
	end if;
end if;

return ds_retorno_w;

end pls_status_solic_carteira;
/