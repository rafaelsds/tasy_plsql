create or replace
function obter_se_possui_regra_gedipa(cd_material_p	number,
									  cd_grupo_material_p number,
									  cd_subgrupo_material_p number,
									  cd_classe_material_p number)
								return varchar2 is

ie_resultado_w	varchar2(1) := 'N';
qtd_w			number(10);
			
begin

	select	count(*)
	into	qtd_w
	from 	regra_lib_hor_medic_rep x 
	where   ((x.cd_material = cd_material_p) 
	or       (x.cd_grupo_material  = cd_grupo_material_p) 
	or       (x.cd_subgrupo_material = cd_subgrupo_material_p) 
	or       (x.cd_classe_material  = cd_classe_material_p))
	and 	x.ie_situacao = 'A';

	if (qtd_w > 0) then
		ie_resultado_w := 'S';
	end if;

	return	ie_resultado_w;

end obter_se_possui_regra_gedipa;
/
