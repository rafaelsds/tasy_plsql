create or replace
function pls_processo_obter_valor
			(	nr_seq_processo_p		Number,
				ie_tipo_valor_p			Number)
 		    		return Number is

/* IE_TIPO_VALOR_P
	1 - Valor procedimento das contas
	2 - Valor deferido das contas
	3 - Valor pendente das contas
	4 - Valor a ressarcir das contas
	5 - Valor pago
*/
				
vl_retorno_w			Number(15,2)	:= 0;
vl_deferido_w			Number(15,2)	:= 0;
vl_pendente_w			Number(15,2)	:= 0;
vl_ressarcir_w			Number(15,2)	:= 0;
vl_pago_w			Number(15,2)	:= 0;

begin

if	(ie_tipo_valor_p	= 1) then
	select	nvl(sum(vl_procedimento),0)
	into	vl_retorno_w
	from	pls_processo_procedimento	b,
		pls_processo_conta		a
	where	a.nr_sequencia			= b.nr_seq_conta
	and	a.nr_seq_processo		= nr_seq_processo_p;
elsif	(ie_tipo_valor_p	in (2,3,4)) then
	select	nvl(sum(a.vl_deferido),0),
		nvl(sum(a.vl_pendente),0),
		nvl(sum(a.vl_ressarcir),0)
	into	vl_deferido_w,
		vl_pendente_w,
		vl_ressarcir_w
	from	pls_processo_conta	a
	where	a.nr_seq_processo	= nr_seq_processo_p;
	
	if	(ie_tipo_valor_p	= 2) then
		vl_retorno_w		:= vl_deferido_w;
	elsif	(ie_tipo_valor_p	= 3) then
		vl_retorno_w		:= vl_pendente_w;
	elsif	(ie_tipo_valor_p	= 4) then
		vl_retorno_w		:= vl_ressarcir_w;
	end if;
elsif	(ie_tipo_valor_p	= 5) then
	select	sum(obter_valor_pago_tit_pagar(a.nr_titulo))
	into	vl_pago_w
	from	titulo_pagar a
	where	a.nr_seq_processo = nr_seq_processo_p;
	
	vl_retorno_w	:= vl_pago_w;
end if;

return	vl_retorno_w;

end pls_processo_obter_valor;
/