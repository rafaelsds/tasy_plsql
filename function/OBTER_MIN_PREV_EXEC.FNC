CREATE OR REPLACE
FUNCTION Obter_Min_Prev_Exec(
			nr_sequencia_p	Number,
			nm_usuario_p		Varchar2)
			RETURN Number IS

qt_min_prev_w			Number(15,0);

BEGIN

qt_min_prev_w	:= 0;

select nvl(sum(nvl(qt_min_prev,0)),0)
into	qt_min_prev_w
from 	man_ordem_servico_exec
where	nr_seq_ordem		= nr_sequencia_p
and	nm_usuario_exec	= nm_usuario_p;

RETURN qt_min_prev_w;

END Obter_Min_Prev_Exec;
/
