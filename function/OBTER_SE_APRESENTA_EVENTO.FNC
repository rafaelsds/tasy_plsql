create or replace FUNCTION OBTER_SE_APRESENTA_EVENTO(nr_seq_evento_p number, cd_funcao_ativa_p number) 
RETURN VARCHAR2 is

ie_aprensenta_w varchar2(1);
ie_funcao_registro_w varchar2(5);
BEGIN

begin
  select decode(ie_funcao_registro, null, 'N', 'S', 'S')
  into ie_funcao_registro_w
  from qua_evento
  where nr_sequencia = nr_seq_evento_p;
  
exception
	when no_data_found then
	begin
		ie_funcao_registro_w	:= 'N';
	end;
end;

if (ie_funcao_registro_w = 'S') then
  if (cd_funcao_ativa_p = obter_funcao_ativa) then
    ie_aprensenta_w := 'S';
  else 
    ie_aprensenta_w := 'N';
  end if;
else
  ie_aprensenta_w := 'S';
end if;

RETURN ie_aprensenta_w;
END OBTER_SE_APRESENTA_EVENTO;
/