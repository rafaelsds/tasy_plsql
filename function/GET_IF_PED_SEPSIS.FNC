create or replace 
function get_if_ped_sepsis(	nr_atendimento_p    number)
							return varchar2 is

cd_estabelecimento_w        number(4);
ie_finaliza_manual_w        varchar2(1);
qt_reg_sepsis_w             number(10) := 0;
dt_alta_w                   date;
qt_horas_sepsis_w           number(10);
existe_liberacao_sepsis_w   varchar2(1);
ie_sepse_ped				varchar2(1) := 'N'; 

begin
	if 	(nvl(nr_atendimento_p,0) > 0) then

		Select 	max(dt_alta)
		into	dt_alta_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;
		
		if	(dt_alta_w is null) then		    
				
				select obter_dados_atendimento(nr_atendimento_p,'EST'),
					   obter_se_sepse_liberada(nr_atendimento_p)
				into   cd_estabelecimento_w,
					   existe_liberacao_sepsis_w
				from	dual;

			if	(existe_liberacao_sepsis_w <> 'N' ) then

				Select  max(QT_HORAS_SEPSIS)
				into	qt_horas_sepsis_w
				from 	parametro_medico
				where	cd_estabelecimento = cd_estabelecimento_w;			

				--- SEPSE PEDIATRICA
				if (existe_liberacao_sepsis_w = 'P') then
				
					ie_finaliza_manual_w := obter_se_sepse_liberada(nr_atendimento_p, 'F');
					
					if (ie_finaliza_manual_w = 'S') then
						Select  count(*)
						into	qt_reg_sepsis_w
						from	escala_sepse_infantil
						where 	nr_atendimento = nr_atendimento_p
						and     dt_fim_protocolo is null
						and	    nvl(ie_situacao,'A') = 'A'
						and     dt_liberacao is not null;
						
					elsif (nvl(qt_horas_sepsis_w,0) > 0) then
					
						Select  count(*)
						into	qt_reg_sepsis_w
						from	escala_sepse_infantil
						where 	nr_atendimento = nr_atendimento_p
						and	dt_atualizacao_nrec between (sysdate  - (1/24 * qt_horas_sepsis_w)) and sysdate
						and	nvl(ie_situacao,'A') = 'A'
						and     dt_liberacao is not null;
						
					end if;
					
					if (qt_reg_sepsis_w > 0) then 
						ie_sepse_ped := 'S';					
					end if;
					
				end if;
			end if;
		else 
			ie_sepse_ped := 'S';
		end if;
	end if;
	return ie_sepse_ped;
end get_if_ped_sepsis;
/	
