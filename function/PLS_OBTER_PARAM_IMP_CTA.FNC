create or replace
function pls_obter_param_imp_cta (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					ie_opcao_p		varchar2) return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Retorna o parametro solicitado pela op��o

	Op��es
	FV - Forma de valida��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

	
Altera��es:
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
ds_retorno_w	varchar2(1000) := '';

begin

if	(ie_opcao_p = 'FV') then

	select	nvl(max(ie_forma_validacao),'P')
	into	ds_retorno_w
	from	pls_param_importacao_conta
	where	cd_estabelecimento	= cd_estabelecimento_p;
end if;

return ds_retorno_w;

end pls_obter_param_imp_cta;
/
