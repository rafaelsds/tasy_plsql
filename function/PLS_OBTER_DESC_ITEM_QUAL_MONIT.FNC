create or replace
function pls_obter_desc_item_qual_monit (cd_item_p		pls_material.cd_material_ops%type)
 		    	return varchar2 is
			
ds_retorno_w		pls_material.ds_material%type;
			
begin

select	max(ds_procedimento)
into	ds_retorno_w
from	procedimento
where	cd_procedimento = cd_item_p
and	ie_situacao = 'A';

if	(ds_retorno_w is null) then

	select	max(ds_procedimento)
	into	ds_retorno_w
	from	procedimento
	where	cd_procedimento = cd_item_p;

	if	(ds_retorno_w is null) then
		select	max(ds_material)
		into	ds_retorno_w
		from 	pls_material
		where	cd_material_ops = cd_item_p
		and	ie_situacao = 'A';
		
		if	(ds_retorno_w is null) then
			
			select	max(ds_material)
			into	ds_retorno_w
			from 	pls_material
			where	cd_material_ops = cd_item_p;
			
			if	(ds_retorno_w is null) then
				
				select	max(ds_material)
				into	ds_retorno_w
				from 	pls_material
				where	cd_material = cd_item_p
				and	ie_situacao = 'A';
				
				if	(ds_retorno_w is null) then
					
					select	max(ds_material)
					into	ds_retorno_w
					from 	pls_material
					where	cd_material = cd_item_p;
				end if;
			end if;
		end if;
	end if;
end if;


return	ds_retorno_w;

end pls_obter_desc_item_qual_monit;
/
