create or replace
function obter_se_paciente_doc_exame(cd_pessoa_fisica_p	varchar2) return number is
nr_seq_agenda_int_w number(10);			
begin

SELECT	max(a.nr_seq_agenda_int)
INTO	nr_seq_agenda_int_w
FROM	agenda_integrada_item a,
	agenda_paciente b
WHERE	a.nr_seq_agenda_exame = b.nr_sequencia
AND	b.cd_pessoa_fisica = cd_pessoa_fisica_p
AND	b.dt_agenda = TRUNC(SYSDATE);

return	nvl(nr_seq_agenda_int_w,0);

end obter_se_paciente_doc_exame;
/