CREATE OR REPLACE FUNCTION obter_desc_resp_credito_cir (
    nr_cirurgia_p    cirurgia.nr_cirurgia%TYPE,
    nr_sequencia_p   NUMBER
) RETURN VARCHAR2 IS
    ds_retorno_w   VARCHAR2(80);
    ie_funcao_w    VARCHAR2(1);
BEGIN
    SELECT  nvl(MAX(ie_funcao), 'C')
    INTO    ie_funcao_w
    FROM
        (
            SELECT  MAX('A') ie_funcao
            FROM    anestesia_descricao
            WHERE   nr_cirurgia = nr_cirurgia_p
            AND     nr_sequencia = nr_sequencia_p
            UNION
            SELECT  MAX('C') ie_funcao
            FROM    cirurgia_descricao
            WHERE   nr_cirurgia = nr_cirurgia_p
            AND     nr_sequencia = nr_sequencia_p
        );

    IF ( nr_cirurgia_p IS NOT NULL AND ie_funcao_w = 'A' ) THEN
        SELECT  substr(MAX(ds_regra), 1, 80)
        INTO    ds_retorno_w
        FROM    regra_honorario    a,
                resp_credito_cir   b
        WHERE   a.cd_regra = b.ie_responsavel_credito
        AND     b.nr_cirurgia = nr_cirurgia_p
        AND     nvl(b.ie_funcao, 'C') = 'A';

    ELSIF ( nr_cirurgia_p IS NOT NULL ) THEN
        SELECT  substr(MAX(ds_regra), 1, 80)
        INTO ds_retorno_w
        FROM    regra_honorario    a,
                resp_credito_cir   b
        WHERE   a.cd_regra = b.ie_responsavel_credito
        AND     b.nr_cirurgia = nr_cirurgia_p
        AND     nvl(b.ie_funcao, 'C') <> 'A';

    END IF;

    RETURN ds_retorno_w;
END obter_desc_resp_credito_cir;
/