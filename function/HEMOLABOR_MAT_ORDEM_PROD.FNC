create or replace 
FUNCTION hemolabor_mat_ordem_prod
(
			nr_seq_ordem_p	Number,
			ie_opcao_p	number)
			return varchar2 is

ds_material_w		Varchar2(100);	
cd_unid_med_w		Varchar2(30);
qt_dose_w		Number(15,3);
qt_item_w		number(10);
ds_item_aux_w		varchar2(255);
x			number(10);
ds_mat_retorno_w	varchar2(255);
ds_mat_proc_w		varchar2(2000);

Cursor C01 is
	select	substr(Obter_Dados_Material(b.cd_material,'D'),1,100),
		qt_dose,
		cd_unidade_medida
	from	can_ordem_item_prescr b,
		material a
	where	b.nr_seq_ordem		= nr_seq_ordem_p
	and	a.cd_material		= b.cd_material
	order by b.nr_sequencia;

BEGIN


open C01;
loop
	fetch C01 into 
		ds_material_w,
		qt_dose_w,
		cd_unid_med_w;
	exit when C01%notfound;
	begin
		ds_mat_proc_w	:= ds_mat_proc_w || substr(rpad(ds_material_w, 40, ' '),1,40) || '  '||qt_dose_w ||' '||cd_unid_med_w|| '@';	
	end;
end loop;
close C01;

x 		:= 1;
qt_item_w	:= 0;
ds_item_aux_w	:= '';

while	(x <= length(ds_mat_proc_w)) loop
	if	(substr(ds_mat_proc_w,x,1) <> '@') then			
		ds_item_aux_w	:= ds_item_aux_w || substr(ds_mat_proc_w,x,1);
	end if;
	
	if	(substr(ds_mat_proc_w,x,1) = '@') then
		qt_item_w	:= qt_item_w + 1;
		if	(ie_opcao_p	= qt_item_w) then
			ds_mat_retorno_w := ds_item_aux_w;
			exit;
		elsif	(ie_opcao_p	> qt_item_w) then
			ds_item_aux_w	 := '';
			ds_mat_retorno_w := '';
		end if;
		ds_item_aux_w	:= '';
	end if;

	x := x + 1;
end loop;

ds_mat_proc_w	:= ds_mat_retorno_w;

return	ds_mat_proc_w;

END hemolabor_mat_ordem_prod;
/