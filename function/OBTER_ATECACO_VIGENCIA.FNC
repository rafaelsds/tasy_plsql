CREATE OR REPLACE
FUNCTION Obter_Atecaco_vigencia(		
					nr_atendimento_p		Number)
					RETURN Number IS

nr_seq_atecaco_w			Number(10,0);

BEGIN

select	nvl(max(nr_seq_interno),0)
into	nr_seq_atecaco_w
from 	atend_categoria_convenio a
where	a.nr_atendimento 		= nr_atendimento_p
and 	dt_inicio_vigencia= 
	(select min(dt_inicio_vigencia)
	from atend_categoria_convenio b
	where nr_atendimento 		= nr_atendimento_p)
and	dt_final_vigencia = 
	(select max(dt_final_vigencia)
	from atend_categoria_convenio b
	where nr_atendimento 		= nr_atendimento_p);

RETURN nr_seq_atecaco_w;

END Obter_Atecaco_vigencia;
/