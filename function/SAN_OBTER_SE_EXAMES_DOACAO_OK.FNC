create or replace
function san_obter_se_exames_doacao_ok(	nr_seq_doacao_p		number,
					ie_opcao_p		number)
 		    	return number is

qt_exames_w		number(10) := 0;
nr_seq_exame_lote_w	number(10);
ie_apto_amostra_w	varchar2(1);
			
cursor c01 is			
	select	nr_sequencia
	from	san_exame_lote
	where	nr_seq_doacao	= nr_seq_doacao_p;			
				
begin

select	nvl(max(ie_apto_amostra),'N')
into	ie_apto_amostra_w
from	san_doacao
where	nr_sequencia = nr_seq_doacao_p;

if (ie_apto_amostra_w = 'N') then

	open c01;
	loop
	fetch c01 into	
		nr_seq_exame_lote_w;
	exit when c01%notfound;
		begin
		
		qt_exames_w := qt_exames_w + nvl(San_Obter_Se_Exames_Ok(nr_seq_exame_lote_w,ie_opcao_p),0);
		
		end;
	end loop;
	close c01;
end if;

return	qt_exames_w;

end san_obter_se_exames_doacao_ok;
/