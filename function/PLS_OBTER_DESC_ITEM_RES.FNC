create or replace
function pls_obter_desc_item_res
		(	nr_sequencia_p	number)
 		return varchar2 is
			
ds_item_despesa_w		varchar2(255);
nr_seq_res_conta_princ_w	number(10);

begin

select	max(nr_seq_res_conta_princ)
into	nr_seq_res_conta_princ_w
from	pls_resumo_conta_v
where	nr_sequencia	= nr_sequencia_p;

if	(nvl(nr_seq_res_conta_princ_w,0) > 0) then
	select	max(ds_item_despesa)
	into	ds_item_despesa_w
	from	pls_resumo_conta_v
	where	nr_sequencia	= nr_seq_res_conta_princ_w;
else
	select	max(ds_item_despesa)
	into	ds_item_despesa_w
	from	pls_resumo_conta_v
	where	nr_sequencia	= nr_sequencia_p;
end if;

return	ds_item_despesa_w;

end pls_obter_desc_item_res;
/
