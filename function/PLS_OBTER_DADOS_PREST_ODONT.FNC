create or replace
function pls_obter_dados_prest_odont(	nr_seq_prestador_p		number,
				ie_opcao_p		varchar2)
				return varchar is
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter dados do prestador com rela��o a odontologia
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

IE_OPCAO_P
PO - Retorna se tem o tipo prestador odontol�gico

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_retorno_w	varchar(1) := 'N';

begin

if	(nr_seq_prestador_p is not null) then
	if	(ie_opcao_p = 'PO') then
		select	decode(count(*),1,'S','N') 
		into	ie_retorno_w
		from	pls_prestador a
		where	nr_sequencia	= nr_seq_prestador_p
		and	exists(	select	1	
				from	pls_tipo_prestador p
				where	p.nr_sequencia = a.nr_seq_tipo_prestador
				and	ie_odontologico  = 'S');
	end if;
end if;

return	ie_retorno_w;
    
end pls_obter_dados_prest_odont;
/