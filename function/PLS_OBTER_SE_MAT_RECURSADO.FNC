create or replace 
function pls_obter_se_mat_recursado (	nr_seq_conta_mat_p		pls_conta_mat.nr_sequencia%type,
					vl_recusado_p			pls_rec_glosa_mat.vl_recursado%type) 
								return varchar2 is

qt_recurso_w		pls_integer;
ie_retorno_w		varchar2(1) := 'S';
vl_recursado_w		pls_rec_glosa_mat.vl_recursado%type;
								
begin

select	count(1)
into	qt_recurso_w
from	pls_rec_glosa_mat
where	nr_seq_conta_mat = nr_seq_conta_mat_p;

if	(qt_recurso_w = 0) then
	select	count(1)
	into	qt_recurso_w
	from	pls_rec_glosa_mat_imp
	where	nr_seq_material = nr_seq_conta_mat_p;
end if;

if	(qt_recurso_w > 0) then
	
	ie_retorno_w := 'N';
	
	select	sum(vl_recusado)
	into	vl_recursado_w
	from	(
		select	nvl(sum(a.vl_recursado),0) vl_recusado
		from	pls_rec_glosa_mat a
		where	a.nr_seq_conta_mat = nr_seq_conta_mat_p
		and	a.ie_status = '1'
		union all
		select	nvl(sum(a.vl_acatado),0) vl_recusado
		from	pls_rec_glosa_mat a
		where	a.nr_seq_conta_mat = nr_seq_conta_mat_p
		and	a.ie_status in ('2','3')
		union all
		select	nvl(sum(a.vl_recursado),0)
		from	pls_rec_glosa_mat_imp a
		where	a.nr_seq_material = nr_seq_conta_mat_p
		and	not exists (	select	1
					from	pls_rec_glosa_mat x
					where	x.nr_seq_conta_mat_imp = a.nr_sequencia));
	
	if	(vl_recursado_w < vl_recusado_p) then
		ie_retorno_w := 'S';
	end if;
end if;

return ie_retorno_w;

end pls_obter_se_mat_recursado;
/
