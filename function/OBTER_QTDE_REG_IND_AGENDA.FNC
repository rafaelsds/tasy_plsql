create or replace
function obter_qtde_reg_ind_agenda		 ( nr_seq_agenda_p NUMBER)
						  return number is


qt_registros_p		number(10);


begin

select	count(*)
into    qt_registros_p
from    pessoa_fisica b,
        profissional_agenda a
where   b.cd_pessoa_fisica 	= a.cd_profissional
and     a.nr_seq_agenda 	= nr_seq_agenda_p
order by nm_pessoa_fisica desc;

return	qt_registros_p;

end obter_qtde_reg_ind_agenda;
/