create or replace function cpoe_obter_alta_medica(
	ie_baixado_por_alta_p varchar2 default null,
	dt_alta_medico_p date default null,
	dt_suspensao_p date default null
)
return varchar2 is
ie_alta_medica_w varchar2(1 char);
begin 
if ((ie_baixado_por_alta_p = 'S') and (nvl(dt_alta_medico_p, sysdate) <= sysdate) and (dt_suspensao_p is null)) then
	ie_alta_medica_w := 'S';
else
	ie_alta_medica_w := 'N';
end if;

return ie_alta_medica_w;

end cpoe_obter_alta_medica;
/