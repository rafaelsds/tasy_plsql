create or replace
function pls_obter_desc_rede_ref_prod
			(	cd_rede_p		varchar2,
				cd_estabelecimento_p	number)
 		    	return varchar2 is
			
ds_retorno_w			varchar2(255) := '';

begin
	
	if	(cd_rede_p is not null) then		
		select	max(ds_rede)
		into	ds_retorno_w
		from	ptu_rede_referenciada
		where	cd_estabelecimento = cd_estabelecimento_p
		and	cd_rede		   = cd_rede_p;		
	end if;

return	ds_retorno_w;

end pls_obter_desc_rede_ref_prod;
/