create or replace
function pls_obter_desc_tipo_acrescimo
			(nr_seq_tipo_acrescimo_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	max(ds_acrescimo)
into	ds_retorno_w
from	pls_tipo_acrescimo
where	nr_sequencia	= nr_seq_tipo_acrescimo_p;

return	ds_retorno_w;

end pls_obter_desc_tipo_acrescimo;
/