create or replace
function pls_obter_plano_preco_scpa
			(	nr_protocolo_ans_p		Varchar2,
				cd_codigo_scpa_p		Varchar2)
 		    		return Number is

nr_seq_plano_w			Number(10);
nr_protocolo_w			Varchar2(20);
cd_codigo_scpa_w		Varchar2(20);

begin

nr_protocolo_w		:= nr_protocolo_ans_p;
nr_protocolo_w		:= elimina_caracteres_especiais(nr_protocolo_w);
nr_protocolo_w		:= replace(nr_protocolo_w,'/','');

cd_codigo_scpa_w	:= cd_codigo_scpa_p;
cd_codigo_scpa_w	:= elimina_caracteres_especiais(cd_codigo_scpa_w);
cd_codigo_scpa_w	:= replace(cd_codigo_scpa_w,'/','');

select	nvl(max(nr_sequencia),0)
into	nr_seq_plano_w
from	pls_plano
where	elimina_caracteres_especiais(replace(nr_protocolo_ans,'/','')) = nr_protocolo_w
and	nvl(elimina_caracteres_especiais(replace(cd_scpa,'/','')),'0') = nvl(cd_codigo_scpa_w,'0');

return	nr_seq_plano_w;

end pls_obter_plano_preco_scpa;
/
