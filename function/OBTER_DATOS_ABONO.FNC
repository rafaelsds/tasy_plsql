create or replace
FUNCTION OBTER_DATOS_ABONO
        (nr_nota_fiscal_p NUMBER, ie_opcao_p NUMBER) RETURN VARCHAR2 IS

ie_retorno_w	VARCHAR2(255);
ds_metodo_pago_w VARCHAR2(255);
ds_forma_pago_w		VARCHAR2(255);

/* 1 - M�todo de Pago (Efectivo, Tarjeta, TEF, Cheque)
   2 - Forma de Pago (Parcialidad, Una solo Exhibici�n)
*/

BEGIN

SELECT 	'PAGO EN ' || UPPER(obter_desc_tipo_recebimento(a.cd_tipo_recebimento)),
	      DECODE(a.nr_seq_nota_fiscal_parc, NULL, 'PAGO EN UNA SOLO EXHIBICION', 'PAGO EN PARCIALIDAD')
INTO    ds_metodo_pago_w,
        ds_forma_pago_w
FROM 	titulo_receber_liq a,
      caixa_receb b
WHERE	a.nr_seq_caixa_rec = b.nr_sequencia
AND obter_dados_titulo_receber(a.nr_titulo,'NS') = nr_nota_fiscal_p
AND a.nr_sequencia = (SELECT MAX(nr_sequencia) FROM titulo_receber_liq WHERE obter_dados_titulo_receber(nr_titulo,'NFT') = nr_nota_fiscal_p);

IF(ie_opcao_p = 1) THEN
    ie_retorno_w := ds_metodo_pago_w;
END IF;

IF(ie_opcao_p = 2) THEN
    ie_retorno_w := ds_forma_pago_w;
END IF;

RETURN	ie_retorno_w;

END OBTER_DATOS_ABONO;
/