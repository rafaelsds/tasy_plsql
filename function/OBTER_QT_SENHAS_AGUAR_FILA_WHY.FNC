create or replace 
function obter_qt_senhas_aguar_fila_why(nr_seq_fila_p    number,
                    dt_inicio_p     date,
                    dt_fim_p        date) 
				return number is

qt_senha_fila_w    number(10);            

begin

select count(*)
into   qt_senha_fila_w
from   paciente_senha_fila
where  dt_inicio_atendimento is null
and    dt_fim_atendimento is null
and    dt_inutilizacao is null
and    nr_seq_fila_senha = nr_seq_fila_p
and    dt_entrada_fila >= dt_inicio_p
and    dt_entrada_fila <= dt_fim_p;

return    qt_senha_fila_w;

end obter_qt_senhas_aguar_fila_why;
/

