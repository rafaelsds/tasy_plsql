CREATE OR REPLACE 
FUNCTION OBTER_STATUS_PAGTO_CHEQUE	(NR_SEQ_CHEQUE_P	NUMBER)
					RETURN varchar2 IS
dt_pagamento_w		date;
dt_prev_pagamento_w	date;
dt_compensacao_w	date;
dt_vencimento_w		date;
nr_seq_concil_w		number(10);
ds_status_w		varchar2(255);

BEGIN

select	max(a.dt_pagamento),
	max(a.dt_prev_pagamento),
	max(a.dt_compensacao),
	max(a.dt_vencimento)
into	dt_pagamento_w,
	dt_prev_pagamento_w,
	dt_compensacao_w,
	dt_vencimento_w
from	cheque a
where	a.nr_sequencia	= nr_seq_cheque_p;

if	(dt_compensacao_w is not null) and (dt_pagamento_w is not null) then
	select	nvl(max(a.nr_seq_concil),-1)
	into	nr_seq_concil_w
	from	movto_trans_financ a
	where	a.nr_seq_cheque	= nr_seq_cheque_p;
end if;

if	(dt_vencimento_w is not null) and (dt_vencimento_w < sysdate) and (dt_compensacao_w is null) then
	ds_status_w	:= WHEB_MENSAGEM_PCK.get_texto(819233); /*'Cheque vencido'*/
elsif	(nr_seq_concil_w = -1) then
	ds_status_w	:= WHEB_MENSAGEM_PCK.get_texto(819234); /*'Pendente de conciliação'*/
elsif	(dt_prev_pagamento_w is not null) and (dt_pagamento_w is null) and (sysdate > dt_prev_pagamento_w) then
	ds_status_w	:= WHEB_MENSAGEM_PCK.get_texto(819235); /*'Pagamento vencido'*/
end if;

RETURN ds_status_w;

END OBTER_STATUS_PAGTO_CHEQUE;
/