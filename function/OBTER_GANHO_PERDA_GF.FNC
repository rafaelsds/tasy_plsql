create or replace
function obter_ganho_perda_gf(nr_cirurgia_p		number,
				data_p			date)
 		    	return number is


qt_total_w	number(15,2);

begin

select	sum(decode(ie_perda_ganho,'G',decode(nvl(ie_volume_ocorrencia,'V'),'O', qt_ocorrencia, qt_volume),
	decode(nvl(ie_volume_ocorrencia,'V'),'O', qt_ocorrencia, qt_volume) * -1)) y
into	qt_total_w
from   	grupo_perda_ganho c,
	tipo_perda_ganho b,
	atendimento_perda_ganho a
where	c.nr_sequencia = b.nr_seq_grupo
and	nvl(a.ie_situacao,'A') = 'A'
and    	b.nr_sequencia = a.nr_seq_tipo
and    	ie_exibir_grafico = 'S'
and	nr_cirurgia = nr_cirurgia_p
and	dt_medida <= data_p;


return	qt_total_w;

end obter_ganho_perda_gf;
/