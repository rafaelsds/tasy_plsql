create or replace
function Obter_Perc_Faturar_Pacote
		(nr_seq_pacote_p	number)
		return number is


pr_afaturar_w	number(15,4)	:= 0;

begin

select	nvl(max(pr_afaturar), 0)
into	pr_afaturar_w
from	atendimento_pacote
where	NR_SEQ_PROCEDIMENTO	= nr_seq_pacote_p;

return	pr_afaturar_w;

end Obter_Perc_Faturar_Pacote;
/