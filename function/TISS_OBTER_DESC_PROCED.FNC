create or replace
function TISS_OBTER_DESC_PROCED (nr_seq_procedimento_p	number,
				nr_seq_partic_p		number,
				ds_campo_p		varchar2) return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	max(vl_campo)
into	ds_retorno_w
from	proc_paciente_tiss
where	nr_seq_procedimento	= nr_seq_procedimento_p
and	ds_campo		= ds_campo_p
and	nvl(nr_seq_partic, 0)	= nvl(nr_seq_partic_p,0);

return	ds_retorno_w;

end TISS_OBTER_DESC_PROCED;
/
