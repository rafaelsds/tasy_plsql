create or replace
function SUS_Obter_Se_Laudo_Apac_ret(nr_apac_p		number)
 		    	return varchar2 is
ie_retorno_w	varchar2(1);
begin

select	nvl(max('S'),'N')
into	ie_retorno_w
from	sus_laudo_paciente
where	nr_apac	= nr_apac_p
and	DT_RETORNO_SECR is not null;

return	ie_retorno_w;

end SUS_Obter_Se_Laudo_Apac_ret;
/