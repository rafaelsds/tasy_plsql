create or replace function nfse_obter_dados_pf_pj(	cd_pessoa_fisica_p	varchar2,
				cd_cgc_p		varchar2,
				ie_opcao_p		varchar2)
				return varchar2 is

ds_razao_social_w		pessoa_juridica.ds_razao_social%type;
cd_cep_w				pessoa_juridica.cd_cep%type;
ds_endereco_w			pessoa_juridica.ds_endereco%type;
ds_bairro_w				pessoa_juridica.ds_bairro%type;
sg_estado_w				pessoa_juridica.sg_estado%type;
ds_retorno_w			varchar2(255);
ds_complemento_w		pessoa_juridica.ds_complemento%type;
nr_telefone_w			pessoa_juridica.nr_telefone%type;
nr_endereco_w			pessoa_juridica.nr_endereco%type;
ds_email_w				pessoa_juridica.ds_email%type;
nr_inscricao_municipal_w	pessoa_juridica.nr_inscricao_municipal%type;
nr_ddd_telefone_w		pessoa_juridica.nr_ddd_telefone%type;
cd_municipio_ibge_dv_w	varchar2(255);
cd_municipio_ibge_w		pessoa_juridica.cd_municipio_ibge%type;
cd_municipio_nfe_w		sus_municipio.cd_municipio_nfe%type;
retorno_w				varchar2(255);
begin
if	(cd_pessoa_fisica_p is not null)	then
	retorno_w:= nfse_obter_compl_pf(cd_pessoa_fisica_p,ie_opcao_p);
else
	select	a.ds_razao_social,
		a.cd_cep,
		a.ds_endereco,
		a.ds_bairro,
		a.sg_estado,
		a.ds_complemento,
		a.nr_telefone,
		a.nr_endereco,
		a.ds_email,
		a.nr_inscricao_municipal,
		a.cd_municipio_ibge,
		nr_ddd_telefone
	into	ds_razao_social_w,
		cd_cep_w,
		ds_endereco_w,
		ds_bairro_w,
		sg_estado_w,
		ds_complemento_w,
		nr_telefone_w,
		nr_endereco_w,
		ds_email_w,
		nr_inscricao_municipal_w,
		cd_municipio_ibge_w,
		nr_ddd_telefone_w
	from	tipo_pessoa_juridica b,
		pessoa_juridica a,
		cns_tipo_logradouro c
	where	a.cd_tipo_pessoa		= b.cd_tipo_pessoa
	and	a.nr_seq_tipo_logradouro	= c.nr_sequencia(+)
	and	a.cd_cgc			= cd_cgc_p;

	if	(ie_opcao_p = 'N')	then
		retorno_w:= ds_razao_social_w;
	elsif	(ie_opcao_p = 'T')	then
		retorno_w:= substr(somente_numero(nr_telefone_w),1,15);
	elsif	(ie_opcao_p = 'UF')	then
		retorno_w:= sg_estado_w;
	elsif	(ie_opcao_p = 'CEP') then
        retorno_w:= substr(cd_cep_w,1,8);
	elsif	(ie_opcao_p = 'B')	then
		retorno_w:= ds_bairro_w;
	elsif	(ie_opcao_p = 'EN')	then
		retorno_w:= ds_endereco_w;
	elsif   (ie_opcao_p = 'NR') then
		retorno_w:=  substr(somente_numero(nr_endereco_w),1,5);
	elsif   (ie_opcao_p = 'CO') then
		retorno_w:= ds_complemento_w;
	elsif   (ie_opcao_p = 'M') then
		begin
			if	ds_email_w is null then
				select	max(ds_email)
				into	ds_email_w
				from	pessoa_juridica_compl
				where	ie_tipo_complemento = 7
				and	cd_cgc = cd_cgc_p;
			end if;
		retorno_w:= ds_email_w;
		end;
	elsif   (ie_opcao_p = 'MC') then
		begin
			select	max(ds_email)
			into	ds_email_w
			from	pessoa_juridica_compl
			where	ie_tipo_complemento = 7
			and	cd_cgc = cd_cgc_p;
		retorno_w:= ds_email_w;
		end;
	elsif	(ie_opcao_p = 'IM') then
		retorno_w:= nr_inscricao_municipal_w;
	elsif	(ie_opcao_p = 'CDM') then
		retorno_w:= cd_municipio_ibge_w;
	elsif	(ie_opcao_p = 'CDMDV') then
		begin
		if	(cd_municipio_ibge_w is not null) then
			cd_municipio_ibge_dv_w := substr(cd_municipio_ibge_w || substr(calcula_digito('MODULO10',cd_municipio_ibge_w),1,1),1,255);
		end if;
		retorno_w:= cd_municipio_ibge_dv_w;
		end;
	elsif	(ie_opcao_p = 'CDMNF') then
		begin
		select	cd_municipio_nfe
		into	cd_municipio_nfe_w
		from	sus_municipio
		where	cd_municipio_ibge = cd_municipio_ibge_w;

		retorno_w := cd_municipio_nfe_w;
		end;
	elsif	(ie_opcao_p = 'DDT') then
		retorno_w:= nr_ddd_telefone_w;
	elsif (ie_opcao_p = 'DM') then
		retorno_w := obter_desc_municipio_ibge(cd_municipio_ibge_w);
	end if;
end if;

retorno_w := tiss_eliminar_caractere(elimina_acentuacao(retorno_w));

RETURN retorno_w;

end;
/
