create or replace
function Obter_Doencas(	cd_pessoa_fisica_p varchar2)
          	return varchar2 is		
		
ds_doenca_w 	varchar(80);
ds_retorno_w	varchar(4000) := null;
ds_agente_w		varchar(4000) := null;

cursor C01 is
   select  distinct ds_doenca ds_agente
   from    PACIENTE_ANTEC_CLINICO
   where   cd_pessoa_fisica = cd_pessoa_fisica_p
   order 	by 1;
   
begin
open c01;
loop
fetch	c01 into
	ds_doenca_w;
exit	when c01%notfound;
	if (ds_doenca_w  is not null)then
	ds_retorno_w := ds_retorno_w || ds_doenca_w || ', ';
	end if;
end loop;
close C01;

if (substr((ds_retorno_w),1,length(ds_retorno_w)-2) is not null) then
	ds_retorno_w := substr((ds_retorno_w),1,length(ds_retorno_w)-2) || '.';
else
	ds_retorno_w := substr((ds_retorno_w),1,length(ds_retorno_w)-2);	
end if;

return	substr(ds_retorno_w,1,255); 

--tratamento em pontuašao final da string e usar separador ' , ' apenas se houver informašao.
end	Obter_Doencas;
/