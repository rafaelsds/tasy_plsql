create or replace
function pls_obter_crm_dep_guia	(	nr_seq_conta_p	pls_conta.nr_sequencia%type)
				return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter o Nr_crm do executor/participante dependo do tipo da guia.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [ X ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ds_retorno_w	varchar2(255);
ie_tipo_guia_w	pls_conta.ie_tipo_guia%type;
nr_crm_w	pls_conta.nr_crm_exec%type;

begin
--pego o tipo da guia da conta
select	max(ie_tipo_guia),
	max(nr_crm_exec)
into	ie_tipo_guia_w,
	nr_crm_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_p;

if	(ie_tipo_guia_w is not null) then

	if 	(ie_tipo_guia_w = 3) then
		ds_retorno_w := nr_crm_w;
	elsif	(ie_tipo_guia_w = 6) then
		select	max(nvl(substr(obter_crm_medico(c.cd_medico), 1, 255), a.nr_crm_exec))
		into	nr_crm_w
		from	pls_conta a,
			pls_conta_proc b,
			pls_proc_participante c
		where	a.nr_sequencia = b.nr_seq_conta
		and	b.nr_sequencia = c.nr_seq_conta_proc
		and	a.nr_sequencia = nr_seq_conta_p;

		ds_retorno_w := nr_crm_w;
	elsif	(ie_tipo_guia_w in(4, 5)) then
		select	max(substr(obter_crm_medico(b.cd_medico), 1, 255))
		into	nr_crm_w
		from	pls_conta_proc a,
			pls_proc_participante b
		where	a.nr_sequencia = b.nr_seq_conta_proc
		and	a.nr_seq_conta = nr_seq_conta_p;	
		
		ds_retorno_w := nr_crm_w;
	end if;
end if;
return	ds_retorno_w;

end pls_obter_crm_dep_guia;
/
