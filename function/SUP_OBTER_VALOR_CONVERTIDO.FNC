create or replace
function sup_obter_valor_convertido(	cd_material_p			number,
				vl_material_p			number,
				cd_unidade_medida_p		varchar2,
				cd_unidade_medida_retorno_p	varchar2)
 		    		return number is
				
/*Op��es cd_unidade_medida_retorno_p:
UME = Unidade Medida Estoque
UMC = Unidade Medida Consumo
UMP = Unidade Medida Compra*/

cd_unidade_medida_compra_w	varchar2(30);
cd_unidade_medida_estoque_w	varchar2(30);
cd_unidade_medida_consumo_w	varchar2(30);
qt_conv_compra_estoque_w		number(13,4);
qt_conv_estoque_consumo_w	number(13,4);				
vl_retorno_w			number(18,4);
cd_estabelecimento_w		number(4) := wheb_usuario_pck.get_cd_estabelecimento;

begin
select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMC'),1,30) cd_unidade_medida_compra,
	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UME'),1,30) cd_unidade_medida_estoque,
	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
	qt_conv_compra_estoque,
	qt_conv_estoque_consumo	
into	cd_unidade_medida_compra_w,
	cd_unidade_medida_estoque_w,
	cd_unidade_medida_consumo_w,
	qt_conv_compra_estoque_w,
	qt_conv_estoque_consumo_w
from 	material
where	cd_material = cd_material_p;

/*Converte primeiro para valor de estoque*/
if	(cd_unidade_medida_p = cd_unidade_medida_estoque_w) then
	vl_retorno_w := vl_material_p;
elsif	(cd_unidade_medida_p = cd_unidade_medida_consumo_w) then
	vl_retorno_w := vl_material_p * qt_conv_estoque_consumo_w;
elsif	(cd_unidade_medida_p = cd_unidade_medida_compra_w) then
	vl_retorno_w := dividir(vl_material_p, qt_conv_compra_estoque_w);
else
	vl_retorno_w := vl_material_p;
end if;

/*Converte o valor para a unidade de medida desejada, se for  de estoque n�o converte, pois j� est� convertido*/
if	(cd_unidade_medida_retorno_p = 'UMC') then
	vl_retorno_w := dividir(vl_retorno_w, qt_conv_estoque_consumo_w);
elsif	(cd_unidade_medida_retorno_p = 'UMP') then
	vl_retorno_w := vl_retorno_w * qt_conv_compra_estoque_w;
end if;

return	vl_retorno_w;
end sup_obter_valor_convertido;
/