create or replace function obter_nut_atend_serv_dia(	nr_atendimento_p 	number,
					cd_setor_atendimento_p	number,
					dt_referencia_p		date,
					cd_dieta_p		number,
					nr_seq_servico_p	number,
					cd_acompanhante_p	number, 
					nr_seq_acompanhante_p	number,
					nr_seq_servico_acomp_p	number,
					nr_seq_grupo_producao_p	number
					)
 		    	return number DETERMINISTIC is
			
nr_sequencia_w		number(10);
ie_serv_nao_liberado_w	varchar2(1);	
ie_conduta_dietot_w	varchar2(1);

begin

ie_conduta_dietot_w	:= obter_valor_param_usuario(1003,85,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);
ie_serv_nao_liberado_w	:= obter_valor_param_usuario(1003,86,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);

if (cd_acompanhante_p = 0) and (nvl(nr_seq_acompanhante_p,0) = 0) then --Paciente
	
	if (ie_conduta_dietot_w = 'S') then
	
		select	nvl(max(a.nr_sequencia),0)
		into	nr_sequencia_w
		from	nut_atend_serv_dia a
		where	a.nr_seq_servico	= nr_seq_servico_p	
		and	a.nr_atendimento	= nr_atendimento_p
		and     a.cd_setor_atendimento	= cd_setor_atendimento_p
		and	(a.dt_liberacao is not null or ie_serv_nao_liberado_w = 'S')
		and     pkg_date_utils.start_of(a.dt_servico, 'dd')	= pkg_date_utils.start_of(dt_referencia_p, 'dd')
		and 	((cd_dieta_p = 0) 
			or (	exists (select	1
					from	nut_atend_serv_dieta b,
						nut_atend_serv_dia_dieta c
					where	a.nr_sequencia 	= b.nr_seq_servico
					and	c.nr_sequencia 	= b.nr_seq_dieta
					and	c.dt_suspensao is null
					and	(c.cd_dieta  = cd_dieta_p or nut_obter_se_grupo_producao(nr_seq_grupo_producao_p,a.nr_sequencia,'C') = 'S'))
			or	exists (select	1
					from	nut_atend_serv_dia_dieta c
					where	c.nr_seq_servico = a.nr_sequencia
					and	c.dt_suspensao is null
					and	(c.cd_dieta  = cd_dieta_p or nut_obter_se_grupo_producao(nr_seq_grupo_producao_p,a.nr_sequencia,'C') = 'S'))));
			/*or (not exists (select	1
					from	nut_atend_serv_dieta b,
						nut_atend_serv_dia_dieta c
					where	a.nr_sequencia 	= b.nr_seq_servico
					and	c.nr_sequencia 	= b.nr_seq_dieta
					and	c.dt_suspensao is null)
			and not	exists (select	1
					from	nut_atend_serv_dia_dieta c
					where	c.nr_seq_servico = a.nr_sequencia
					and	c.dt_suspensao is null))));*/
		
	else
	
		select	nvl(max(p.nr_sequencia),0)
		into	nr_sequencia_w
		from   	nut_atend_serv_dia p,
			nut_atend_serv_dia_rep b
		where   b.nr_seq_serv_dia	= p.nr_sequencia
		and	p.nr_seq_servico	= nr_seq_servico_p
		and 	((cd_dieta_p = 0) 
			or (	exists (select	1
					from	prescr_dieta x
					where	x.nr_prescricao = b.nr_prescr_oral
					and	(x.cd_dieta  = cd_dieta_p or nut_obter_se_grupo_producao(nr_seq_grupo_producao_p,x.nr_prescricao,'P') = 'S'))  
			or not 	exists (select	1
					from	prescr_dieta x
					where	x.nr_prescricao = b.nr_prescr_oral)))	
		and	p.nr_atendimento	= nr_atendimento_p
		and     p.cd_setor_atendimento	= cd_setor_atendimento_p
		and	(p.dt_liberacao is not null or ie_serv_nao_liberado_w = 'S')
		and     pkg_date_utils.start_of(p.dt_servico, 'dd')	= pkg_date_utils.start_of(dt_referencia_p, 'dd');
		
	end if;
else	
	begin
	if (cd_acompanhante_p <> 0) then -- Acompanhante com o codigo de pessoa
	
		select	nvl(max(a.nr_sequencia),0)
		into	nr_sequencia_w
		from   	nut_atend_serv_dia a,
			nut_atend_acompanhante b
		where   a.nr_sequencia		= b.nr_seq_atend_serv_dia
		and	a.nr_seq_servico	= nr_seq_servico_p
		and 	((cd_dieta_p = 0) 
			or (	exists (select	1
					from	nut_atend_acomp_dieta x
					where	x.nr_seq_atend_acomp = b.nr_sequencia
					and	(x.cd_dieta  = cd_dieta_p or nut_obter_se_grupo_producao(nr_seq_grupo_producao_p,x.nr_seq_atend_acomp,'A') = 'S'))  
			or not exists ( select	1
			 		from  	nut_atend_acomp_dieta x
					where 	x.nr_seq_atend_acomp = b.nr_sequencia)))	
		and	a.nr_atendimento	= nr_atendimento_p
		and     a.cd_setor_atendimento	= cd_setor_atendimento_p
		and	(a.dt_liberacao is not null or ie_serv_nao_liberado_w = 'S')
		and     pkg_date_utils.start_of(a.dt_servico, 'dd')	= pkg_date_utils.start_of(dt_referencia_p, 'dd')
		and	b.cd_pessoa_fisica	= to_char(cd_acompanhante_p);
		
		
	else   -- Acompanhante sem codigo de pessoa
	
		select	nvl(max(a.nr_sequencia),0)
		into	nr_sequencia_w
		from   	nut_atend_serv_dia a,
			nut_atend_acompanhante b
		where   a.nr_sequencia		= b.nr_seq_atend_serv_dia
		and	a.nr_seq_servico	= nr_seq_servico_p 
		and 	((cd_dieta_p = 0) 
			or (	exists (select	1
					from	nut_atend_acomp_dieta x
					where	x.nr_seq_atend_acomp = b.nr_sequencia
					and	(x.cd_dieta  = cd_dieta_p or nut_obter_se_grupo_producao(nr_seq_grupo_producao_p,x.nr_seq_atend_acomp,'A') = 'S'))  
			or not exists ( select	1
			 		from  	nut_atend_acomp_dieta x
					where 	x.nr_seq_atend_acomp = b.nr_sequencia)))
		and	a.nr_atendimento	= nr_atendimento_p
		and     a.cd_setor_atendimento	= cd_setor_atendimento_p
		and	(a.dt_liberacao is not null or ie_serv_nao_liberado_w = 'S')
		and     pkg_date_utils.start_of(a.dt_servico, 'dd')	= pkg_date_utils.start_of(dt_referencia_p, 'dd');
		
	end  if;
	end;
	
end if;


return	nr_sequencia_w;

end obter_nut_atend_serv_dia;
/
