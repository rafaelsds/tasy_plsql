create or replace
function pls_obter_msg_reajuste_benef
			(	nr_seq_mensalidade_p		number,
				nr_ordem_p			number)
				return varchar2 is
				
ds_mensagem_reajuste_w		varchar2(2000);
ds_mensagem_reajuste_ret_w	varchar2(2000);
nr_sequencia_w			number(10);
nr_ordem_w			number(10)	:=0;
nr_seq_contrato_w		number(10);
nr_seq_segurado_w		number(10);
ie_ordem_w			number(10);

cursor c01 is
	select	1 ie_ordem,
		a.nr_sequencia
	from	pls_contrato_pagador c,
		pessoa_fisica	b,
		pls_segurado	a
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	a.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and	c.nr_seq_contrato	= a.nr_seq_contrato
	and	a.nr_seq_contrato	= nr_seq_contrato_w
	union all
	select	2 ie_ordem,
		a.nr_sequencia
	from	pessoa_fisica	b,
		pls_segurado	a
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	a.nr_seq_contrato	= nr_seq_contrato_w
	and	not exists	(select	1
				from	pls_contrato_pagador x
				where	x.nr_seq_contrato	= a.nr_seq_contrato
				and	x.cd_pessoa_fisica	= a.cd_pessoa_fisica)
	order by ie_ordem;

begin

select	max(nr_seq_contrato)
into	nr_seq_contrato_w
from	pls_mensalidade a
where	a.nr_sequencia	= nr_seq_mensalidade_p;

open c01;
loop
fetch c01 into
	ie_ordem_w,
	nr_seq_segurado_w;
exit when c01%notfound;
	begin
	nr_ordem_w := nr_ordem_w + 1;
	
	if	(nr_ordem_w = nr_ordem_p) then
		select	max(substr(b.ds_mensagem_reajuste,1,2000)) ds_mensagem_reajuste
		into	ds_mensagem_reajuste_w
		from	pls_mensalidade_seg_item b,
			pls_mensalidade_segurado a
		where	a.nr_sequencia	= b.nr_seq_mensalidade_seg
		and	a.nr_seq_mensalidade	= nr_seq_mensalidade_p
		and	a.nr_seq_segurado	= nr_seq_segurado_w;
	
		ds_mensagem_reajuste_ret_w :=	ds_mensagem_reajuste_w;
	end if;
	end;
end loop;
close c01;

return	ds_mensagem_reajuste_ret_w;

end pls_obter_msg_reajuste_benef;
/