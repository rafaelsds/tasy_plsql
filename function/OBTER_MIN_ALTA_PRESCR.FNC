create or replace
function obter_min_alta_prescr(ie_tipo_atendimento_p	number)
 		    	return number is

qt_min_nova_prescr_w	number(5);

begin

select	nvl(max(qt_min_nova_prescr),0)
into	qt_min_nova_prescr_w
from	regra_cons_min_prescr_alta
where	ie_tipo_atendimento = ie_tipo_atendimento_p;

return	qt_min_nova_prescr_w;

end obter_min_alta_prescr;
/