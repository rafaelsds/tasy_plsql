create or replace
function obter_se_pac_consulta_dia(	cd_agenda_p		number,
				nr_seq_agenda_p		number,
				dt_agenda_p		date)
				return varchar2 is

cd_agenda_w		number(10,0);
dt_agenda_w		date;
cd_pessoa_fisica_w	varchar2(10);
ie_agenda_dia_w		varchar2(1);
ie_biometria_status_w	varchar2(10);
nm_usuario_w		varchar2(15);

begin

ie_agenda_dia_w := 'N';
if	(nr_seq_agenda_p is not null) and
	(dt_agenda_p is not null) then
	begin

	nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
	
	obter_param_usuario(821,423,obter_perfil_ativo,nm_usuario_w,wheb_usuario_pck.get_cd_estabelecimento,ie_biometria_status_w);
	
	if	(nvl(ie_biometria_status_w,'N') = 'S') then
	
		select	decode(count(*),0,'N','S')
		into	ie_agenda_dia_w
		from	agenda_consulta
		where	trunc(dt_agenda,'dd') = trunc(dt_agenda_p)
		and		cd_agenda = cd_agenda_p
		and		ie_status_agenda = 'O'
		and		dt_consulta is not null
		and		nr_sequencia <> nr_seq_agenda_p
		and		nm_usuario = nm_usuario_w;
	
	
	else
	
		select	decode(count(*),0,'N','S')
		into	ie_agenda_dia_w
		from	agenda_consulta
		where	trunc(dt_agenda,'dd') = trunc(dt_agenda_p)
		and	cd_agenda = cd_agenda_p
		and	ie_status_agenda = 'O'
		and	dt_consulta is not null
		and	nr_sequencia <> nr_seq_agenda_p;
	
	end if;
	
	end;	
end if;

return ie_agenda_dia_w;

end obter_se_pac_consulta_dia;
/