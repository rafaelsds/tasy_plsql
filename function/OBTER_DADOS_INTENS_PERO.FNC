create or replace
function obter_dados_intens_pero(	nr_seq_material_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

nr_seq_agente_w		number(10,0);
ds_retorno_w		varchar2(255);
					
begin

select	nr_seq_agente 
into	nr_seq_agente_w
from 	agente_anest_material 
where	nr_sequencia = nr_seq_material_p;

if	(ie_opcao_p = 'DTD') then
	ds_retorno_w := obter_dados_agente(nr_seq_agente_w,'DTD');
elsif	(ie_opcao_p = 'UM') then
	ds_retorno_w := substr(obter_dados_agente(nr_seq_agente_w,'UM'),1,100);
end if;

return	ds_retorno_w;

end obter_dados_intens_pero;
/