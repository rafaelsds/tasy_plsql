create or replace
function obter_gestao_vaga_local(	cd_setor_atendimento_p		number,
					cd_unidade_basica_p 		varchar2,
					cd_unidade_compl_p			varchar2)
					return varchar2 is

dt_retorno_w 		varchar2(255);				
cd_paciente_reserva_w	varchar2(10);
nm_pac_reserva_w	varchar2(255);
				
begin

dt_retorno_w := null;

select 	max(cd_paciente_reserva),
	max(nm_pac_reserva)
into	cd_paciente_reserva_w,
	nm_pac_reserva_w
from 	unidade_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_p
and	cd_unidade_basica = cd_unidade_basica_p
and	cd_unidade_compl = cd_unidade_compl_p;


if	(cd_paciente_reserva_w is not null) or (nm_pac_reserva_w is not null) then

	select	max(nvl(obter_nome_setor(CD_SETOR_ATUAL),obter_valor_dominio(1492,IE_LOCAL_AGUARDA)))
	into	dt_retorno_w
	from 	gestao_vaga	
	where	cd_setor_desejado 	= cd_setor_atendimento_p
	and		cd_unidade_basica 	= cd_unidade_basica_p
	and		cd_unidade_compl 	= cd_unidade_compl_p 
	and 	ie_status in ('A','R');
end if;


return	dt_retorno_w;

end obter_gestao_vaga_local;
/
