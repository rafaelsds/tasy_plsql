CREATE OR REPLACE 
function OBTER_SE_FUNCAO_TASY	(ds_form_p	varchar2)
				return		varchar2 is

ie_funcao_tasy_w	varchar2(1)	:= 'N';
ds_form_w		varchar2(10);

begin

if	(ds_form_p	is not null) then

	ds_form_w	:= upper(replace(ds_form_p,'_',null));

	select	nvl(max('S'),'N')
	into	ie_funcao_tasy_w
	from	funcao a
	where	upper(replace(a.ds_form,'_',null))	= ds_form_w;

end if;

return	ie_funcao_tasy_w;

end OBTER_SE_FUNCAO_TASY;
/