create or replace
function obter_ultima_doacao_sangue( cd_pessoa_fisica_p	varchar2,
				nr_seq_doacao_p		number)
 		    	return date is
				
dt_ultima_doacao_w 	date;

begin				
				
if (cd_pessoa_fisica_p is not null and nr_seq_doacao_p is not null) then

	select	max(dt_doacao)
	into	dt_ultima_doacao_w
	from 	san_doacao
	where 1 = 1
	and	cd_pessoa_fisica 	= cd_pessoa_fisica_p
	and	nr_sequencia <> nr_seq_doacao_p;

end if;	
	
return	dt_ultima_doacao_w;

end obter_ultima_doacao_sangue;
/

