create or replace
function gpi_obter_perc_proj(	nr_sequencia_p	number) 
			return number is

dt_cancelamento_w	date;
qt_cron_w		number(10);
pr_real_w			number(15,2);
pr_projeto_w		number(15,2)	:= 0;

begin

select	count(*),
	nvl(sum(pr_real),0)
into	qt_cron_w,
	pr_real_w
from	gpi_cronograma
where	nr_seq_projeto	= nr_sequencia_p
and	dt_cancelamento is null;

pr_projeto_w		:= pr_real_w;

if	(pr_real_w <> 0) then
	pr_projeto_w	:= (dividir(pr_real_w,(qt_cron_w * 100)) * 100);
end if;

return pr_projeto_w;

end gpi_obter_perc_proj;
/