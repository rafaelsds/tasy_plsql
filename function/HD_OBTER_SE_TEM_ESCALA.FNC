create or replace
function hd_obter_se_tem_escala(	cd_pessoa_fisica_p	varchar2,
					nr_seq_escala_p		number)
					return varchar2 is
ds_retorno_w		varchar2(1) := 'N';

begin

	select	decode(count(*), 0, 'N', 'S')
	into	ds_retorno_w
	from	hd_escala_dialise
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	dt_inicio <= sysdate
	and	dt_fim is null
	and	nr_seq_escala = nr_seq_escala_p;

return	ds_retorno_w;
end hd_obter_se_tem_escala;
/	
