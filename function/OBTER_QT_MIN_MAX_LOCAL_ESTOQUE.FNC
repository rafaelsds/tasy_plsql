create or replace
function obter_qt_min_max_local_estoque(	cd_local_estoque_p		number,
					ie_opcao_p			varchar2)
 		    	return number is

/*
ie_opcao_p
I - Minimo
A - Maximo*/

qt_retorno_w				number(5);
qt_dia_ajuste_minimo_w			number(5);
qt_dia_ajuste_maximo_w			number(5);

begin

select	qt_dia_ajuste_minimo,
	qt_dia_ajuste_maximo
into	qt_dia_ajuste_minimo_w,
	qt_dia_ajuste_maximo_w
from	local_Estoque
where	cd_local_estoque = cd_local_estoque_p;

if	(ie_opcao_p = 'I') then
	qt_retorno_w := qt_dia_ajuste_minimo_w;
elsif	(ie_opcao_p = 'A') then
	qt_retorno_w := qt_dia_ajuste_maximo_w;
end if;

return	qt_retorno_w;

end obter_qt_min_max_local_estoque;
/