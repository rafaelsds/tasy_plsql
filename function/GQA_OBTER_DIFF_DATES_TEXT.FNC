create or replace function GQA_OBTER_DIFF_DATES_TEXT(date1_p date, date2_p date, tipo_p varchar2)
  return varchar2 is

ds_retorno_w          varchar2(100) := null;
str_hora          varchar2(20);
str_minuto        varchar2(20);
str_dia          varchar2(20);

int_dia          number(10);
int_hora          number(10);
int_minuto        number(10);

diff number(10, 10);
begin
  if(date1_p is null or date2_p is null or tipo_p is null) then
    return null;
  end if;
  
  str_dia := obter_desc_expressao(500514);
  str_hora := obter_desc_expressao(487631);
  str_minuto := obter_desc_expressao(883687);

  if(tipo_p = 'DHM') then --DIAS, HORAS e MINUTOS apenas 
    select 
           trunc(difference                     ) as days,
           trunc(mod( difference * 24,       24)) as hours,
           trunc(mod( difference * 24 * 60,  60)) as minutes
    into 
            int_dia,
            int_hora,
            int_minuto
    from   (select abs(date1_p - date2_p) as difference from dual);
    
    int_dia := nvl(int_dia, 0);
    int_hora := nvl(int_hora, 0);
    int_minuto := nvl(int_minuto, 0);
    
    if(int_dia > 0) then
      ds_retorno_w :=  ds_retorno_w || TO_CHAR(int_dia) || str_dia || ' ';
    end if;
    if(int_dia > 0 or int_hora > 0) then
      ds_retorno_w :=  ds_retorno_w || TO_CHAR(int_hora) || str_hora || ' ';
    end if;

    ds_retorno_w :=  ds_retorno_w || TO_CHAR(int_minuto) || str_minuto || ' ';
  end if;

  return ds_retorno_w;
end GQA_OBTER_DIFF_DATES_TEXT;
/
