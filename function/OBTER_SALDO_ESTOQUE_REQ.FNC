create or replace
function obter_saldo_estoque_req(
	cd_estabelecimento_p	number,
	cd_material_p		number,
	cd_local_estoque_p	number,
	cd_fornecedor_p		varchar2,
	dt_mesano_referencia_p	date,
	ie_consignado_p		varchar2,
	ie_generico_p		varchar2) 
	return number is 
	
qt_estoque_w		number(17,4) := 0;
qt_estoque_ww		number(17,4) := 0;

cd_material_estoque_w	material.cd_material%type;
cd_material_generico_w	material.cd_material%type;

cd_material_w		material.cd_material%type;

Cursor C01 is
select	a.cd_material 
from	material a
where	a.cd_material_generico = cd_material_p
and	a.cd_material_estoque <> cd_material_estoque_w
and	nvl(ie_generico_p,'N') = 'S'
and	1 = 2 /*N�o considerar o saldo dos comerciais do gen�rico, de acordo com o hist�rico da OS 640069 em 23/10/2013 15:42:03*/
union
select	a.cd_material 
from	material a
where	a.cd_material = cd_material_generico_w
and	a.cd_material_estoque <> cd_material_estoque_w
and	nvl(ie_generico_p,'N') = 'S' /*OS 640069*/
union
select	a.cd_material 
from	material a
where	a.cd_material = cd_material_p;

begin
begin
select	cd_material_estoque,
	cd_material_generico
into	cd_material_estoque_w,
	cd_material_generico_w
from	material
where	cd_material	= cd_material_p;		
exception
when others then
	cd_material_estoque_w := null;
end;

open C01;
loop
fetch C01 into	
	cd_material_w;
exit when C01%notfound;
	begin
	if	(ie_consignado_p = 'N') then
		obter_saldo_estoque(cd_estabelecimento_p, cd_material_w, cd_local_estoque_p, dt_mesano_referencia_p, qt_estoque_ww);
	elsif	(ie_consignado_p = 'S') then
		qt_estoque_ww := obter_saldo_estoque_consig(cd_estabelecimento_p, cd_fornecedor_p, cd_material_w, cd_local_estoque_p);
	end if;
	
	qt_estoque_w := qt_estoque_w + nvl(qt_estoque_ww,0);
	end;
end loop;
close C01;

return qt_estoque_w;

end obter_saldo_estoque_req;
/