create or replace
function Obter_totais_fat_med(nr_sequencia_p	number,
			      nr_atendimento_p	number,	
			      ie_tipo_p		number)
 		    	return number is

vl_retorno_w		number(15,2);
vl_total_w		number(15,2);
vl_liquido_w		number(15,2);
			
begin

/*
1 - Valor total
2 - Valor L�quido
3 - Valor total do atendimento
4 - Valor L�quido do atendimento
5 - Valor total de terceiros
6- Percentual descontos
*/

select 	nvl(sum((nvl(a.vl_procedimento,0) * a.qt_fatur)),0),
	nvl(sum((nvl(a.vl_procedimento,0) * qt_fatur) - ((nvl(vl_procedimento,0) * qt_fatur) * pr_desconto / 100)),0)
into	vl_total_w,
	vl_liquido_w
from 	pep_med_fatur		b,
	pep_med_fatur_proc 	a
where 	b.nr_sequencia = a.nr_seq_fatur
and 	b.nr_sequencia = nr_sequencia_p
and 	b.nr_atendimento = nr_atendimento_p;

if	(ie_tipo_p = 1) then
	vl_retorno_w:= vl_total_w;
elsif	(ie_tipo_p = 2) then
	vl_retorno_w:= vl_liquido_w;
elsif	(ie_tipo_p in (3,4)) then
	
	select 	nvl(sum((nvl(a.vl_procedimento,0) * a.qt_fatur)),0),
		nvl(sum((nvl(a.vl_procedimento,0) * qt_fatur) - ((nvl(vl_procedimento,0) * qt_fatur) * pr_desconto / 100)),0)
	into	vl_total_w,
		vl_liquido_w
	from 	pep_med_fatur		b,
		pep_med_fatur_proc 	a
	where 	b.nr_sequencia = a.nr_seq_fatur
	and 	b.nr_atendimento = nr_atendimento_p;
	
	if	(ie_tipo_p = 3) then
		vl_retorno_w:= vl_total_w;
	elsif	(ie_tipo_p = 4) then
		vl_retorno_w:= vl_liquido_w;
	end if;

elsif	(ie_tipo_p = 5) then
	select 	nvl(sum((nvl(a.vl_procedimento,0) * a.qt_fatur)),0)
	into	vl_retorno_w
	from 	pep_med_fatur b,
		pep_med_fatur_proc a
	where 	b.nr_sequencia = a.nr_seq_fatur
	and	b.nr_seq_conta_terceiro = nr_sequencia_p;

elsif	(ie_tipo_p = 6) then
	select	nvl(sum(pr_desconto),0)
	into	vl_retorno_w
	from 	pep_med_fatur b,
		pep_med_fatur_proc a
	where 	b.nr_sequencia = a.nr_seq_fatur
and 	b.nr_sequencia = nr_sequencia_p
and 	b.nr_atendimento = nr_atendimento_p;
end if;

return	vl_retorno_w;

end Obter_totais_fat_med;
/
