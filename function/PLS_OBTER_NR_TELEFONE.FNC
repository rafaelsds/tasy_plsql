create or replace
function pls_obter_nr_telefone(
	nr_ddi_telefone_p varchar2,
	nr_ddd_telefone_p varchar2,
	nr_telefone_p varchar2
) return varchar2 is

ds_retorno_w varchar2(255);
nr_ddi_telefone_w varchar2(255);			

begin
	if (nr_telefone_p is not null) then
		if	(substr(nr_ddi_telefone_p,1,1) = '+') then
			nr_ddi_telefone_w := nr_ddi_telefone_p;
		else
			nr_ddi_telefone_w := '+' || nr_ddi_telefone_p; 	
		end if;
		select decode(nr_ddi_telefone_p, null, 
            nr_telefone_p, 
            nr_ddi_telefone_w || ' ' || ' (' || nr_ddd_telefone_p || ') ' || 
            case
                when length(nr_telefone_p) = 9 then
                    substr(nr_telefone_p, 1, 5) || '-' || substr(nr_telefone_p, 6, 9)
                else
                    substr(nr_telefone_p, 1, 4) || '-' || substr(nr_telefone_p, 5, 8)
                end)
		into ds_retorno_w
		from dual;
	end if;	
	return	ds_retorno_w;
end pls_obter_nr_telefone;
/
