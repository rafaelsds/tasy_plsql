create or replace 
function sus_obter_desc_financiamento(nr_sequencia_p	sus_procedimento.nr_seq_financiamento%type)				    	
return varchar2 is  


ds_retorno_w sus_financiamento.ds_financiamento%type;

begin 
	if(nr_sequencia_p is not null) then
	
		begin
			select	ds_financiamento
			into	ds_retorno_w
			from	sus_financiamento
			where	nr_sequencia = nr_sequencia_p;
			exception
				when others then
					ds_retorno_w := '';
		end;
		
	else
		begin
			ds_retorno_w := '';
		end;
	end if;
	
return	ds_retorno_w;  

end sus_obter_desc_financiamento; 
/
