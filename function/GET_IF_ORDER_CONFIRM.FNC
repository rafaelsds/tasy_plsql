create or replace
function get_if_order_confirm(	nr_atendimento_p	number)
 		    	return varchar2 is

ds_return_w		varchar2(1);			
			
begin

select	nvl(cpoe_item_pend_lib_nur_enf(NR_ATENDIMENTO_P, 'A', 'E', 'N'),'N')
into	ds_return_w
from	dual;

return	ds_return_w;

end get_if_order_confirm;
/

