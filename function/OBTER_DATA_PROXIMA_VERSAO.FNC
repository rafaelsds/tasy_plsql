create or replace
function obter_data_proxima_versao(	dt_referencia_p date)
		return date is
	

ie_dia_w			number(1);
dt_referencia_w		date;
dt_versao_w		date;
begin

dt_referencia_w	:= nvl(dt_referencia_p,sysdate);

select	pkg_date_utils.get_weekday(dt_referencia_w)
into	ie_dia_w
from	dual;

select	pkg_date_utils.start_of(next_day(dt_referencia_w, 3), 'DD', 0)
into	dt_versao_w
from	dual;

if	(ie_dia_w >= 4) then
	
	if	(ie_dia_w = 4) and
		(pkg_date_utils.extract_field('HOUR', dt_referencia_w, 0) >= 18) then
		dt_versao_w	:= dt_versao_w + 7;
	end if;
	if	(ie_dia_w > 4) then
		dt_versao_w	:= dt_versao_w + 7;
	end if;
elsif	(ie_dia_w in (1,2)) then
	dt_versao_w	:= dt_versao_w + 7;
end if;

if	(dt_referencia_w <= pkg_date_utils.get_date(2012, 1, 11, 0)) then
	dt_versao_w := pkg_date_utils.get_date(2012, 1, 17, 0);
end if;

return	dt_versao_w;

end obter_data_proxima_versao;
/