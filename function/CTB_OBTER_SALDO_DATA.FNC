create or replace 
FUNCTION CTB_Obter_Saldo_Data(
			cd_estabelecimento_p		Number,
			cd_conta_contabil_p		Varchar2,
			cd_centro_custo_p		Number,
			dt_referencia_p		Date)
			return Number is

vl_saldo_w		Number(15,2);
vl_debito_w		Number(15,2);
vl_credito_w		Number(15,2);
dt_referencia_w	Date;
nr_seq_saldo_w	Number(10,0);

BEGIN

vl_saldo_w		:= 0;
dt_referencia_w	:= add_months(trunc(dt_referencia_p,'month'),-1);
select	max(nr_sequencia)
into	nr_seq_saldo_w
from	ctb_mes_ref
where	dt_referencia	= dt_referencia_w;

select	/*+ index(a ctbsald_uk) */
	nvl(max(vl_saldo),0)
into	vl_saldo_w
from	ctb_saldo a
where	nr_seq_mes_ref		= nr_seq_saldo_w
and	cd_conta_contabil		= cd_conta_contabil_p
and	cd_estabelecimento		= cd_estabelecimento_p
and	nvl(cd_centro_custo,0)	= nvl(cd_centro_custo_p,0);

select	nvl(ctb_obter_movto_data(
	cd_estabelecimento_p,
	cd_conta_contabil_p,
	cd_centro_custo_p,
	trunc(dt_referencia_p,'month'),
	dt_referencia_p,
	'D'),0)
into	vl_debito_w
from	dual;

select	nvl(ctb_obter_movto_data(
	cd_estabelecimento_p,
	cd_conta_contabil_p,
	cd_centro_custo_p,
	trunc(dt_referencia_p,'month'),
	dt_referencia_p,
	'C'),0)
into	vl_credito_w
from	dual;

vl_saldo_w		:= vl_saldo_w - vl_debito_w + vl_credito_w;

return vl_saldo_w;

END CTB_Obter_Saldo_Data;
/