create or replace
function OBTER_TRANSPORTE_BARTHEL(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w 	varchar2(255);
			
begin

select 	DECODE(IE_TRANSPORTE,0,'Incapaz n�o tem equilibrio para sentar',5,'Grande ajuda (uma ou duas pessoas f�sica) pode sentar',10,'Pequena ajuda (verbal ou f�sica)',15,'Independente')
into	ds_retorno_w
from	escala_barthel
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end OBTER_TRANSPORTE_BARTHEL;
/