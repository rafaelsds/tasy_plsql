create or replace
function obter_se_evento_finaliz_proc(	nr_sequencia_p	number)
					return varchar2 is


	
ds_retorno_w		varchar2(15);
	
begin
		
if	(nr_sequencia_p is not null) then
	select	nvl(max(IE_FIM_PROCESSO_CIRURGICO),'N')
	into	ds_retorno_w
	from	evento_cirurgia
	where	nr_sequencia	=	nr_sequencia_p;
end if;

return 	ds_retorno_w;


end obter_se_evento_finaliz_proc;
/
