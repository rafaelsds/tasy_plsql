create or replace
function pls_obter_valores_mensalidade	(nr_seq_lote_p			number,
					nr_seq_mensalidade_p		number,
					ie_opcao_p			varchar2)
					return number is

/*	'1' - Pre�o pr�-estabelecido
	'2' - Taxa de inscri��o
	'3' - Co-participa��o
	'4' - Reajuste - Varia��o de custo
	'5' - Reajuste - Mudan�a de faixa-et�ria
	'6' - Pre�o p�s-estabelecido por custo operacional
	'7' - Pre�o p�s-estabelecido por rateio
	'8' - Pre�o misto
	'17' - Taxa de emiss�o de boleto
	'A' - Lan�amentos adicionais	
	'C' - Total cancelado
	'TR' - T�tulo - Total recebido	*/
	

vl_retorno_w			number(15,2)	:= 0;
vl_item_w			number(15,2)	:= 0;
vl_total_item_w			number(15,2)	:= 0;
nr_seq_mensalidade_w		number(10);
nr_seq_mensalidade_seg_w	number(10);
vl_cancelado_W			number(15,2)	:= 0;
vl_taxa_boleto_w		number(15,2)	:= 0;
ie_passou_w			varchar2(1) 	:= 'N';
vl_total_recebido_w		number(15,2)	:= 0;

cursor c01 is
	select	nr_sequencia
	from	pls_mensalidade
	where	(nr_seq_lote	= nvl(nr_seq_lote_p,0)
	or	nr_sequencia	= nvl(nr_seq_mensalidade_p,0));

cursor c02 is
	select	nr_sequencia
	from	pls_mensalidade_segurado
	where	nr_seq_mensalidade	= nr_seq_mensalidade_w;

begin

if	(nvl(nr_seq_lote_p,0) > 0) or
	(nvl(nr_seq_mensalidade_p,0) > 0) then
	
	if (ie_opcao_p = 'TR') then			
		select	((nvl(sum(b.vl_recebido),0) + nvl(sum(b.vl_juros),0) + nvl(sum(b.vl_multa),0) + nvl(sum(b.vl_rec_maior),0)) + nvl(sum(b.vl_glosa),0)) 
		into	vl_total_recebido_w
		from	pls_mensalidade		c,
			titulo_receber_liq	b,
			titulo_receber		a
		where	a.nr_titulo = b.nr_titulo(+)
		and	a.nr_seq_mensalidade = c.nr_sequencia
		and   	nvl(b.ie_lib_caixa, 'S') = 'S'
		and	c.nr_sequencia = nr_seq_mensalidade_p;
		
		vl_retorno_w := vl_total_recebido_w;
	else 
	if (ie_opcao_p = 'C') then
		select	sum(vl_mensalidade)
		into	vl_cancelado_W
		from	pls_mensalidade
		where	(nr_seq_lote = nvl(nr_seq_lote_p,0)
		or	nr_sequencia = nvl(nr_seq_mensalidade_p,0))
		and	ie_cancelamento = 'C';
		
		vl_retorno_w	:= vl_cancelado_w;
	else 
		vl_item_w		:= 0;
		vl_total_item_w		:= 0;
		open c01;
		loop
		fetch c01 into	
			nr_seq_mensalidade_w;
		exit when c01%notfound;
			open c02;
			loop
			fetch c02 into	
				nr_seq_mensalidade_seg_w;
			exit when c02%notfound;
				if (ie_opcao_p = 'A') then
					select	nvl(sum(vl_adicional),0)
					into	vl_item_w
					from	pls_mensalidade_seg_adic
					where	nr_seq_mensalidade_seg	= nr_seq_mensalidade_seg_w;
				else
					select	nvl(sum(vl_item),0)
					into	vl_item_w
					from	pls_mensalidade_seg_item
					where	nr_seq_mensalidade_seg	= nr_seq_mensalidade_seg_w
					and	ie_tipo_item		= ie_opcao_p;
					
					if	(ie_opcao_p = '17') then
						select	nvl(sum(b.vl_taxa_boleto),0)
						into	vl_taxa_boleto_w
						from	pls_mensalidade_seg_item	b,
							pls_mensalidade_segurado	a
						where	a.nr_sequencia			= b.nr_seq_mensalidade_seg
						and	a.nr_sequencia			= nr_seq_mensalidade_seg_w;
						
						if	(vl_taxa_boleto_w = 0) and
							(ie_passou_w = 'N')then
							ie_passou_w := 'S';
							select	nvl(sum(b.vl_item),0)
							into	vl_taxa_boleto_w
							from	pls_mensalidade_seg_item	b,
								pls_mensalidade			a
							where	a.nr_sequencia			= b.nr_seq_mensalidade
							and	a.nr_sequencia			= nr_seq_mensalidade_w;
						end if;
						
						vl_item_w	:= nvl(vl_item_w,0) + nvl(vl_taxa_boleto_w,0);
					end if;
				end if;
				
				vl_total_item_w		:= vl_total_item_w + vl_item_w;
			end loop;
			close c02;
		end loop;
		close c01;
	
		vl_retorno_w	:= nvl(vl_total_item_w,0);

	end if;
	end if;
end if;

return	nvl(vl_retorno_w,0);

end pls_obter_valores_mensalidade;
/