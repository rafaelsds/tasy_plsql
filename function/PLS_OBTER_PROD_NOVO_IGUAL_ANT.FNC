create or replace
function pls_obter_prod_novo_igual_ant
			(	nr_seq_segurado_p		number,
				nr_seq_contrato_p		number,
				ie_rescindi_atual_p		varchar2,
				dt_contratacao_nova_p	date)
				return varchar2 is
			
ds_retorno_w			varchar2(10);
qt_registros_w			number(10);
nr_seq_plano_novo_w		number(10);
nr_seq_plano_ant_w		number(10);
ie_regulamentacao_novo_w		varchar2(10);
nr_protocolo_ans_novo_w		varchar2(20);
ie_regulamentacao_ant_w		varchar2(10);
nr_protocolo_ans_ant_w		varchar2(20);
dt_rescisao_w			date;

begin
ds_retorno_w	:= 'N';

select	count(*)
into	qt_registros_w
from	pls_contrato_plano
where	nr_seq_contrato	= nr_seq_contrato_p
and	ie_situacao	= 'A';

if	(qt_registros_w = 1) then
	select	nr_seq_plano
	into	nr_seq_plano_novo_w
	from	pls_contrato_plano
	where	nr_seq_contrato	= nr_seq_contrato_p
	and	ie_situacao	= 'A';
	
	select	pls_obter_produto_benef(nr_sequencia,dt_contratacao_nova_p),
		dt_rescisao
	into	nr_seq_plano_ant_w,
		dt_rescisao_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
	
	select	ie_regulamentacao,
		nr_protocolo_ans
	into	ie_regulamentacao_novo_w,
		nr_protocolo_ans_novo_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_novo_w;
	
	select	ie_regulamentacao,
		nr_protocolo_ans
	into	ie_regulamentacao_ant_w,
		nr_protocolo_ans_ant_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_ant_w;
	
	if	(ie_regulamentacao_novo_w = 'P') and
		(ie_regulamentacao_ant_w = 'P') then
		if	(trim(nr_protocolo_ans_novo_w) = trim(nr_protocolo_ans_ant_w)) then
			/*aaschlote - Caso o beneficiario esteja ativo e n�o for rescindido, ou a possuir rescis�o futura com ades�o inferior, ent�o consiste,*/
			if	((dt_rescisao_w is null) and
				(ie_rescindi_atual_p = 'N') or
				(dt_rescisao_w is not null) and
				(dt_rescisao_w	> dt_contratacao_nova_p)) then
				ds_retorno_w	:= 'S';
			end if;
		end if;	
	end if;
end if;

return	ds_retorno_w;

end pls_obter_prod_novo_igual_ant;
/