create or replace
function obter_intervalo_min(	qt_minuto_p	number)
			return varchar2 is

ds_retorno_w	varchar2(100);

begin

if	(qt_minuto_p > 0) and
	(qt_minuto_p <= 60) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308369, 'HR=' || '01'); -- At� 01 hora
elsif	(qt_minuto_p > 60) and
	(qt_minuto_p <= 120) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '02'); -- At� 02 horas
elsif	(qt_minuto_p > 120) and
	(qt_minuto_p <= 180) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '03'); -- At� 03 horas
elsif	(qt_minuto_p > 180) and
	(qt_minuto_p <= 240) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '04'); -- At� 04 horas
elsif	(qt_minuto_p > 240) and
	(qt_minuto_p <= 300) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '05'); -- At� 05 horas
elsif	(qt_minuto_p > 300) and
	(qt_minuto_p <= 360) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '06'); -- At� 06 horas
elsif	(qt_minuto_p > 360) and
	(qt_minuto_p <= 420) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '07'); -- At� 07 horas
elsif	(qt_minuto_p > 420) and
	(qt_minuto_p <= 480) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '08'); -- At� 08 horas
elsif	(qt_minuto_p > 480) and
	(qt_minuto_p <= 540) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '09'); -- At� 09 horas
elsif	(qt_minuto_p > 540) and
	(qt_minuto_p <= 600) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '10'); -- At� 10 horas
elsif	(qt_minuto_p > 600) and
	(qt_minuto_p <= 660) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '11'); -- At� 11 horas
elsif	(qt_minuto_p > 660) and
	(qt_minuto_p <= 720) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '12'); -- At� 12 horas
elsif	(qt_minuto_p > 720) and
	(qt_minuto_p <= 780) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '13'); -- At� 13 horas
elsif	(qt_minuto_p > 780) and
	(qt_minuto_p <= 840) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '14'); -- At� 14 horas
elsif	(qt_minuto_p > 840) and
	(qt_minuto_p <= 900) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '15'); -- At� 15 horas
elsif	(qt_minuto_p > 900) and
	(qt_minuto_p <= 960) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '16'); -- At� 16 horas
elsif	(qt_minuto_p > 960) and
	(qt_minuto_p <= 1020) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '17'); -- At� 17 horas
elsif	(qt_minuto_p > 1020) and
	(qt_minuto_p <= 1080) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '18'); -- At� 18 horas
elsif	(qt_minuto_p > 1080) and
	(qt_minuto_p <= 1140) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '19'); -- At� 19 horas
elsif	(qt_minuto_p > 1140) and
	(qt_minuto_p <= 1200) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '20'); -- At� 20 horas
elsif	(qt_minuto_p > 1200) and
	(qt_minuto_p <= 1260) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '21'); -- At� 21 horas
elsif	(qt_minuto_p > 1260) and
	(qt_minuto_p <= 1320) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '22'); -- At� 22 horas
elsif	(qt_minuto_p > 1320) and
	(qt_minuto_p <= 1380) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '23'); -- At� 23 horas
elsif	(qt_minuto_p > 1380) and
	(qt_minuto_p <= 1440) then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308366, 'HR=' || '24'); -- At� 24 horas
elsif	(qt_minuto_p = 0) then
	ds_retorno_w	:= '0';
end if;

/* At� 1 hora, at� 2 horas, at� 3 horas, at� 4 h, at� 5 h, at� 6 horas e acima de 6 horas. */	

return	ds_retorno_w;

end obter_intervalo_min;
/
