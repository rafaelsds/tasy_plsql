create or replace
function obter_se_moeda_forn_estrang(nr_seq_cot_forn_p	cot_compra_forn.nr_sequencia%type)
 		    	return varchar2 is
				
retorno_w 	varchar2(1) := 'N';
cd_moeda_w	cot_compra_forn.cd_moeda%type;

begin

  select	cd_moeda
  into	cd_moeda_w
  from	cot_compra_forn
  where	nr_sequencia = nr_seq_cot_forn_p;

  if (cd_moeda_w <> wheb_moeda_pck.get_moeda_padrao(wheb_usuario_pck.get_cd_estabelecimento)) then
    retorno_w := 'S';
  end if;

  return retorno_w;

end obter_se_moeda_forn_estrang;
/
