create or replace
function obter_se_proced_isolamento(	nr_prescricao_p		number,
					nr_sequencia_p		number) return varchar2 is

ds_retorno_w	varchar2(1) := 'N';
begin
if	(nvl(nr_prescricao_p,0) > 0) and
	(nvl(nr_sequencia_p,0) > 0) then

	select	nvl(max(ie_gera_isolamento),'N')
	into	ds_retorno_w
	from	prescr_procedimento b,
		proc_interno		a
	where	a.nr_sequencia = b.nr_seq_proc_interno
	and	nvl(a.ie_gera_isolamento,'N') = 'S'
	and	b.nr_sequencia = nr_sequencia_p
	and	b.nr_prescricao	= nr_prescricao_p
	and	b.nr_seq_proc_interno is not null;

	if	(ds_retorno_w = 'N') then
		select	nvl(max(ie_gera_isolamento),'N')
		into	ds_retorno_w
		from	prescr_procedimento b,
			exame_laboratorio a
		where	a.nr_seq_exame = b.nr_seq_exame
		and	nvl(a.ie_gera_isolamento,'N') = 'S'
		and	b.nr_sequencia = nr_sequencia_p
		and	b.nr_prescricao	= nr_prescricao_p;
	end if;

end if;

return	ds_retorno_w;

end obter_se_proced_isolamento;
/
