create or replace FUNCTION matrixv7_get_nr_seq_prescr (
    nr_prescricao_p   prescr_procedimento.nr_prescricao%TYPE,
    cd_exame_p        lab_exame_equip.cd_exame_equip%TYPE
) RETURN prescr_procedimento.nr_sequencia%TYPE IS
    nr_seq_prescr_w     prescr_procedimento.nr_sequencia%TYPE;
BEGIN
    BEGIN
        SELECT
            a.nr_sequencia
        INTO
            nr_seq_prescr_w
        FROM
            prescr_procedimento   a,
            lab_exame_equip       b,
            exame_laboratorio     c,
            equipamento_lab       d
        WHERE
            a.nr_prescricao = nr_prescricao_p
            AND b.cd_exame_equip = cd_exame_p
            AND a.nr_seq_exame = b.nr_seq_exame
            AND d.cd_equipamento = b.cd_equipamento
            AND b.nr_seq_exame = c.nr_seq_exame
            AND d.ds_sigla = 'MATRIX'
            AND nvl(a.ie_suspenso, 'N') = 'N'
            AND nvl(c.ie_situacao, 'A') = 'A';

    EXCEPTION
        WHEN too_many_rows THEN
            --Ha mais de um exame na prescricao com o codigo #@CD_EXAME#@. E necessario verificar o cadastro do exame. O resultado nao sera importado para esse exame.
            wheb_mensagem_pck.exibir_mensagem_abort(
                nr_seq_mensagem_p => 1097835,
                vl_macros_p => 'CD_EXAME=' || cd_exame_p
            );
        WHEN no_data_found THEN
            --Nao foi localizado um exame com codigo #@CD_EXAME#@ na prescricao #@NR_PRESCRICAO#@. E necessario verificar o cadastro do exame. O resultado nao sera importado para esse exame.
            wheb_mensagem_pck.exibir_mensagem_abort(
                nr_seq_mensagem_p => 1118417,
                vl_macros_p => 'CD_EXAME=' || cd_exame_p || ';NR_PRESCRICAO=' || nr_prescricao_p
            );
    END;
    
    RETURN nr_seq_prescr_w;
END matrixv7_get_nr_seq_prescr;
/