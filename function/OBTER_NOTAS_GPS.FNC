create or replace
function obter_notas_gps (	nr_seq_gps_p	number)
 		    	return varchar2 is

nr_nota_fiscal_w 	varchar2(255);
retorno_w 	varchar2(2000);
contador_w 	number(10) := 0;
qt_registro_w 	number(10);

cursor c01 is
	select 	n.nr_nota_fiscal 
	from 	titulo_pagar t,
		nota_fiscal n, 
		gps g, 
		gps_titulo p 
	where 	t.nr_seq_nota_fiscal = n.nr_sequencia(+)
	and   	p.nr_seq_gps = g.nr_sequencia 
	and   	p.nr_titulo = t.nr_titulo 
	and  	g.nr_sequencia = nr_seq_gps_p;

begin

select 	count(*)
into	qt_registro_w
from 	titulo_pagar t,
	nota_fiscal n, 
	gps g, 
	gps_titulo p 
where 	t.nr_seq_nota_fiscal = n.nr_sequencia(+)
and   	p.nr_seq_gps = g.nr_sequencia 
and   	p.nr_titulo = t.nr_titulo 
and  	g.nr_sequencia = nr_seq_gps_p;

if (qt_registro_w > 0) then

	open c01;
	loop
	fetch c01 into	
		nr_nota_fiscal_w;
	exit when c01%notfound;
		begin
		
		if (contador_w = 0) then
			retorno_w := nr_nota_fiscal_w;
		elsif (contador_w <> qt_registro_w) then
			retorno_w := retorno_w || ', ' || nr_nota_fiscal_w;
		elsif (contador_w = qt_registro_w) then
			retorno_w := retorno_w || nr_nota_fiscal_w;
		end if;

		contador_w := contador_w + 1;
	
		end;
	end loop;
	close c01;
end if;

return	retorno_w;

end obter_notas_gps;
/