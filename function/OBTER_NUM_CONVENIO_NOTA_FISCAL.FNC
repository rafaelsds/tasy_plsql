create or replace
function obter_num_convenio_nota_fiscal(nr_seq_nota_fiscal_p		number)
 		    	return varchar2 is

ds_convenio_ano_w 	 varchar2(20);

begin

select 	max(a.ds_convenio_ano)
into 	ds_convenio_ano_w
from 	projeto_recurso a,
	nota_fiscal_item b
where 	a.nr_sequencia = b.nr_seq_proj_rec
and 	b.nr_sequencia = nr_seq_nota_fiscal_p;

return	ds_convenio_ano_w;

end obter_num_convenio_nota_fiscal;
/
