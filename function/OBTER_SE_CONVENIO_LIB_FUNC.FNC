create or replace
FUNCTION obter_se_convenio_lib_func
		(cd_estabelecimento_p	NUMBER,
		cd_convenio_p		NUMBER,		
		cd_funcao_p		NUMBER,
		ie_opcao_p 		VARCHAR2,
		dt_referencia_p		DATE)
		RETURN VARCHAR2 IS

ie_liberado_w		VARCHAR2(01)	:= 'S';
qt_regra_w		NUMBER(10,0);
cd_perfil_w		NUMBER(10);
qt_registro_w		number(10);
ie_considera_tab_w 	varchar2(1);
ie_conv_estab_w		varchar2(1);
/*QT - Se existe convenio cadastrado....
*/

BEGIN
cd_perfil_w	:= NVL(obter_perfil_ativo,0);
begin
	SELECT	nvl(max(1),0)
	INTO	qt_regra_w
	FROM	CONVEIO_ESTAB_FUNC_LIB a
	WHERE	a.cd_estabelecimento	= cd_estabelecimento_p
	AND		NVL(a.IE_PERMITE_CONVENIO,'S')	= 'N'
	AND		a.cd_funcao			= cd_funcao_p
	AND		NVL(a.cd_perfil,cd_perfil_w)	= cd_perfil_w
	AND		((NVL(a.dt_inicio_regra,nvl(dt_referencia_p,sysdate))		<= nvl(dt_referencia_p,sysdate)))
	and		rownum = 1;
exception
when others then
	qt_regra_w := 0;
end;

begin
	select 	nvl(max(1),0)
	into 	qt_registro_w
	from 	convenio_estabelecimento b
	where 	b.cd_estabelecimento = cd_estabelecimento_p
	and 	b.cd_convenio = cd_convenio_p
	and		rownum = 1;

exception
when others then
	qt_registro_w := 0;
end;

IF 	ie_opcao_p = 'QT' THEN
	RETURN qt_regra_w;
END IF;	

IF	(qt_regra_w	<> 0) THEN

	SELECT	DECODE(COUNT(*),0,'S','N')
	INTO	ie_liberado_w
	FROM	CONVEIO_ESTAB_FUNC_LIB
	WHERE	cd_convenio		= cd_convenio_p
	AND	cd_funcao		= cd_funcao_p
	AND	NVL(IE_PERMITE_CONVENIO,'S')	= 'N'
	AND	cd_estabelecimento	= cd_estabelecimento_p
	AND	NVL(cd_perfil,cd_perfil_w)	= cd_perfil_w
	AND	((NVL(dt_inicio_regra,nvl(dt_referencia_p,sysdate))		<= nvl(dt_referencia_p,sysdate)));
END IF;
	
if (cd_funcao_p = 869) then

	select nvl(max(IE_CONSISTE_CONV_ESTAB),'N')
	into ie_conv_estab_w
	from parametro_agenda_integrada 
	where nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

	if  (ie_conv_estab_w = 'S') and 
	(qt_registro_w = 0) and 
	(ie_liberado_w = 'S') then
		ie_liberado_w := 'N';
	end if;
else
	Obter_Param_Usuario(916, 669, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_p, ie_considera_tab_w);

	if  (ie_considera_tab_w = 'S') and 
	(qt_registro_w = 0) and 
	(ie_liberado_w = 'S') then
		ie_liberado_w := 'N';
	end if;
end if;


RETURN	ie_liberado_w;

END obter_se_convenio_lib_func;
/
