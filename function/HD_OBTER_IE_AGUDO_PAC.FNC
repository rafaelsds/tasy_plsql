create or replace
function hd_obter_ie_agudo_pac(cd_pessoa_fisica_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);			
			
begin

if (cd_pessoa_fisica_p is not null) then	

	select		max(ie_paciente_agudo)
	into		ds_retorno_w
	from		hd_pac_renal_cronico
	where		cd_pessoa_fisica = cd_pessoa_fisica_p;

end if;

return	ds_retorno_w;

end hd_obter_ie_agudo_pac;
/