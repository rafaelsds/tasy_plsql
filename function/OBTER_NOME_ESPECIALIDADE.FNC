create or replace
function Obter_Nome_Especialidade
		(cd_especialidade_p		number)
		return			varchar2 is

ds_especialidade_w		varchar2(100);

begin

begin
	select	ds_especialidade
	into	ds_especialidade_w
	from	especialidade_medica
	where	cd_especialidade	= cd_especialidade_p;
exception
when others then
	ds_especialidade_w := '';
end;

return	ds_especialidade_w;

end Obter_Nome_Especialidade;
/