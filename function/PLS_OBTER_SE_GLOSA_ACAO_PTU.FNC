create or replace
function pls_obter_se_glosa_acao_ptu	(	nr_seq_guia_p		Number,
						nr_seq_requisicao_p	Number,
						nr_seq_complemento_p	Number)
						return Varchar2 is

cd_inconsistencia_w				Number(4);

ie_tipo_transacao_w				Varchar2(2);
ie_retorno_w					Varchar2(2)	:= 'N';

nr_seq_motivo_glosa_w				Number(10);
nr_seq_procedimento_w				Number(10);
nr_seq_material_w				Number(10);
nr_seq_inconsist_w				Number(10);

Cursor C01 is
	select	nr_seq_motivo_glosa
	from	pls_guia_glosa
	where	nr_seq_guia		= nr_seq_guia_p
	and	ie_tipo_transacao_w	= 'G'
	union
	select	nr_seq_motivo_glosa
	from	pls_requisicao_glosa
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	ie_tipo_transacao_w	= 'R';

Cursor C02 is
	select	nr_sequencia
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p
	and	(nr_seq_compl_ptu	= nr_seq_complemento_p	or nr_seq_complemento_p is null)
	union
	select	nr_sequencia
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	(nr_seq_compl_ptu	= nr_seq_complemento_p	or nr_seq_complemento_p is null);
	
Cursor C03 is
	select	nr_sequencia
	from	pls_guia_plano_mat
	where	nr_seq_guia		= nr_seq_guia_p
	and	(nr_seq_compl_ptu	= nr_seq_complemento_p	or nr_seq_complemento_p is null)
	union
	select	nr_sequencia
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	(nr_seq_compl_ptu	= nr_seq_complemento_p	or nr_seq_complemento_p is null);

Cursor C04 is
	select	nr_seq_motivo_glosa
	from	pls_guia_glosa
	where	(nr_seq_guia_proc	= nr_seq_procedimento_w or nr_seq_procedimento_w 	is null)
	and	(nr_seq_guia_mat	= nr_seq_material_w 	or nr_seq_material_w 		is null)
	and	ie_tipo_transacao_w	= 'G'
	union
	select	nr_seq_motivo_glosa
	from	pls_requisicao_glosa
	where	(nr_seq_req_proc	= nr_seq_procedimento_w or nr_seq_procedimento_w 	is null)
	and	(nr_seq_req_mat		= nr_seq_material_w 	or nr_seq_material_w 		is null)
	and	ie_tipo_transacao_w	= 'R';

Cursor C05 is
	select	a.nr_seq_inconsistencia
	from	ptu_intercambio_consist a
	where	((nr_seq_requisicao	= nr_seq_requisicao_p or nr_seq_requisicao is null)
	and	((nr_seq_procedimento	= nr_seq_procedimento_w and nr_seq_procedimento is not null) or (nr_seq_procedimento_w is null and nr_seq_procedimento is null))
	and	((nr_seq_material	= nr_seq_material_w and nr_seq_material is not null) or (nr_seq_material_w is null and nr_seq_material is null)))
	union
	select	y.nr_sequencia
	from	pls_acao_glosa_tiss	x,
		ptu_inconsistencia	y,
		pls_requisicao_glosa	z
	where	x.nr_seq_inconsis_scs	= y.nr_sequencia
	and	x.nr_seq_motivo_glosa	= z.nr_seq_motivo_glosa
	and	((z.nr_seq_requisicao	= nr_seq_requisicao_p or z.nr_seq_requisicao is null)
	and	((z.nr_seq_req_proc	= nr_seq_procedimento_w and z.nr_seq_req_proc is not null) or (nr_seq_procedimento_w is null and z.nr_seq_req_proc is null))
	and	((z.nr_seq_req_mat	= nr_seq_material_w and z.nr_seq_req_mat is not null) or (nr_seq_material_w is null and z.nr_seq_req_mat is null)))
	union
	select	v.nr_sequencia
	from	pls_ocorrencia_scs	u,
		ptu_inconsistencia	v,
		pls_ocorrencia_benef	w
	where	u.nr_seq_inconsis_scs	= v.nr_sequencia
	and	u.nr_seq_ocorrencia	= w.nr_seq_ocorrencia
	and	((w.nr_seq_requisicao	= nr_seq_requisicao_p or w.nr_seq_requisicao is null)
	and	((w.nr_seq_proc	= nr_seq_procedimento_w and w.nr_seq_proc is not null) or (nr_seq_procedimento_w is null and w.nr_seq_proc is null))
	and	((w.nr_seq_mat	= nr_seq_material_w and w.nr_seq_mat is not null) or (nr_seq_material_w is null and w.nr_seq_mat is null)));

begin

if	(nr_seq_guia_p	is not null) then
	ie_tipo_transacao_w	:= 'G';
elsif	(nr_seq_requisicao_p	is not null) then
	ie_tipo_transacao_w	:= 'R';
end if;
	
open C01;
loop
fetch C01 into	
	nr_seq_motivo_glosa_w;
exit when C01%notfound;
	begin
	
	begin
	select	b.cd_inconsistencia
	into	cd_inconsistencia_w
	from	pls_acao_glosa_tiss	a,
		ptu_inconsistencia	b
	where	a.nr_seq_inconsis_scs	= b.nr_sequencia
	and	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w;
	exception
	when others then
		cd_inconsistencia_w	:= 0;
	end;
	
	if	(nvl(cd_inconsistencia_w,0)	<> 0) then
		ie_retorno_w	:= 'S';
	end if;
	
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_seq_procedimento_w;
exit when C02%notfound;
	begin
	open C04;
	loop
	fetch C04 into	
		nr_seq_motivo_glosa_w;
	exit when C04%notfound;
		begin
		begin
			select	b.cd_inconsistencia
			into	cd_inconsistencia_w
			from	pls_acao_glosa_tiss	a,
				ptu_inconsistencia	b
			where	a.nr_seq_inconsis_scs	= b.nr_sequencia
			and	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w;
		exception
		when others then
			cd_inconsistencia_w	:= 0;
		end;
		
		if	(nvl(cd_inconsistencia_w,0)	<> 0) then
			ie_retorno_w	:= 'S';
		end if;
		end;
	end loop;
	close C04;
	end;

	if	(ie_retorno_w	= 'N') then
		open C05;
		loop
		fetch C05 into	
			nr_seq_inconsist_w;
		exit when C05%notfound;
			begin
			begin
				select	cd_inconsistencia
				into	cd_inconsistencia_w
				from	ptu_inconsistencia
				where	nr_sequencia	= nr_seq_inconsist_w;
			exception
			when others then
				cd_inconsistencia_w	:= 0;
			end;
			
			if	(nvl(cd_inconsistencia_w,0)	<> 0) then
				ie_retorno_w	:= 'S';
			end if;
			end;
		end loop;
		close C05;
	end if;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_material_w;
exit when C03%notfound;
	begin
	open C04;
	loop
	fetch C04 into	
		nr_seq_motivo_glosa_w;
	exit when C04%notfound;
		begin
		begin
			select	b.cd_inconsistencia
			into	cd_inconsistencia_w
			from	pls_acao_glosa_tiss	a,
				ptu_inconsistencia	b
			where	a.nr_seq_inconsis_scs	= b.nr_sequencia
			and	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w;
		exception
		when others then
			cd_inconsistencia_w	:= 0;
		end;
		
		if	(nvl(cd_inconsistencia_w,0)	<> 0) then
			ie_retorno_w	:= 'S';
		end if;
		end;
	end loop;
	close C04;
	
	if	(ie_retorno_w	= 'N') then
		open C05;
		loop
		fetch C05 into	
			nr_seq_inconsist_w;
		exit when C05%notfound;
			begin
			begin
				select	cd_inconsistencia
				into	cd_inconsistencia_w
				from	ptu_inconsistencia
				where	nr_sequencia	= nr_seq_inconsist_w;
			exception
			when others then
				cd_inconsistencia_w	:= 0;
			end;
			
			if	(nvl(cd_inconsistencia_w,0)	<> 0) then
				ie_retorno_w	:= 'S';
			end if;
			end;
		end loop;
		close C05;
	end if;
	end;
end loop;
close C03;

return	ie_retorno_w;

end pls_obter_se_glosa_acao_ptu;
/