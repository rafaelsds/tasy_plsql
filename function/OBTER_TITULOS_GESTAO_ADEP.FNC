create or replace
function obter_titulos_gestao_adep (ie_tipo_item_p			VARCHAR2,
									ie_classif_proced_gestao_p	VARCHAR2,
									cd_perfil_p			number default null)
return VARCHAR2 is

ds_titulo_w	VARCHAR2(80);
cd_perfil_w	number(10);
ie_tipo_item_w	varchar2(40);
ie_param_cpoe_35_w varchar2(2);

begin

cd_perfil_w	:= nvl(cd_perfil_p,obter_perfil_ativo);

case	ie_tipo_item_p
	when 'DI'  then ie_tipo_item_w := '1';
	when 'D'   then ie_tipo_item_w := '2';
	when 'O'   then ie_tipo_item_w := '3';
	when 'HM'  then ie_tipo_item_w := '4';
	when 'J'   then ie_tipo_item_w := '5';
	when 'MAT' then ie_tipo_item_w := '6';
	when 'M'   then ie_tipo_item_w := '7';
	when 'NAN' then ie_tipo_item_w := '8';
	when 'NPN' then ie_tipo_item_w := '9';
	when 'NPP' then ie_tipo_item_w := '10';
	when 'P'   then ie_tipo_item_w := '12';
	when 'R'   then ie_tipo_item_w := '13';
	when 'SNE' then ie_tipo_item_w := '14';
	when 'S'   then ie_tipo_item_w := '15';
	when 'SOL' then ie_tipo_item_w := '16';
	when 'G'   then ie_tipo_item_w := '18';
	when 'C'   then ie_tipo_item_w := '18';
	when 'I'   then ie_tipo_item_w := '19'; 
	when 'L'   then ie_tipo_item_w := '20';
	when 'LD'  then ie_tipo_item_w := '21';
	when 'IAH' then ie_tipo_item_w := '22';
	when 'E'   then ie_tipo_item_w := '23';
	else ie_tipo_item_w := '';
end case;

if (ie_tipo_item_p <> 'SUSP') then
	select	substr(max(ds_item),1,80)
	into	ds_titulo_w
	from	plt_nomenclatura
	where	((cd_perfil is null) or (cd_perfil = cd_perfil_w))
	and	ie_tipo_item = ie_tipo_item_w 
	and	nvl(ie_classif_proced_gestao_p,'XPTO') <> 'SADT';
else
	ds_titulo_w := null;
end if;	

if	(ds_titulo_w is null) then
	if	(ie_tipo_item_p = 'J') then
		begin
		ds_titulo_w	:= obter_desc_expressao(304828) || Plt_Obter_Qtd_Item_liberado('J',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p in('D','DE')) then
		begin
		ds_titulo_w	:= obter_desc_expressao(287913) || Plt_Obter_Qtd_Item_liberado(ie_tipo_item_p,cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p in('ME','MD')) then
		begin
		ds_titulo_w	:= obter_desc_expressao(287725)	|| Plt_Obter_Qtd_Item_liberado(ie_tipo_item_p,cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'S') then
		begin
		ds_titulo_w	:= obter_desc_expressao(298960)|| Plt_Obter_Qtd_Item_liberado('S',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'SOL') then
		begin
		ds_titulo_w	:= obter_desc_expressao(298694)	|| Plt_Obter_Qtd_Item_liberado('SOL',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'SNE') then
		begin
		ds_titulo_w	:= obter_desc_expressao(314222)	|| Plt_Obter_Qtd_Item_liberado('SNE',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'NAN') then
		begin
      Obter_param_Usuario(2314, 35, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_param_cpoe_35_w);
      if (ie_param_cpoe_35_w = 'S') then
          ds_titulo_w	:= obter_desc_expressao(1028131);
      else
          ds_titulo_w	:= obter_desc_expressao(305331) || Plt_Obter_Qtd_Item_liberado('NAN',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
      end if;
		end;
	elsif	(ie_tipo_item_p = 'NPA') then
		begin
		ds_titulo_w	:= obter_desc_expressao(305331) || Plt_Obter_Qtd_Item_liberado('NPA',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'NPN') then
		begin
		ds_titulo_w	:= obter_desc_expressao(305334)	|| Plt_Obter_Qtd_Item_liberado('NPN',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'NPP') then
		begin
		ds_titulo_w	:= obter_desc_expressao(305336) || Plt_Obter_Qtd_Item_liberado('NPP',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'HM') then
		begin
		ds_titulo_w	:= obter_desc_expressao(291262) || Plt_Obter_Qtd_Item_liberado('HM',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'IAH') then
		begin
			ds_titulo_w	:= obter_desc_expressao(751620) || Plt_Obter_Qtd_Item_liberado('IAH',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;		
	elsif	(ie_tipo_item_p = 'M') then
		begin
		ds_titulo_w	:= obter_desc_expressao(293085) || Plt_Obter_Qtd_Item_liberado('M',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'MAT') then
		begin
		ds_titulo_w	:= obter_desc_expressao(292949) || Plt_Obter_Qtd_Item_liberado('MAT',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'DI') then
		begin
		ds_titulo_w	:= obter_desc_expressao(287725) || Plt_Obter_Qtd_Item_liberado('DI',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'P') then
		begin
		if	(ie_classif_proced_gestao_p = 'P') then
			begin
			ds_titulo_w	:= obter_desc_expressao(296717)	|| Plt_Obter_Qtd_Item_liberado('P',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
			end;
		elsif	(ie_classif_proced_gestao_p = 'SADT') then
			begin
			ds_titulo_w	:= obter_desc_expressao(327306)|| Plt_Obter_Qtd_Item_liberado('SADT',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
		end if;
		end;
	elsif	(ie_tipo_item_p = 'IA') then
		begin
		ds_titulo_w	:= obter_desc_expressao(489491)	|| Plt_Obter_Qtd_Item_liberado('IA',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p in ('C','G')) then
		begin
		ds_titulo_w	:= obter_desc_expressao(494765) || Plt_Obter_Qtd_Item_liberado(ie_tipo_item_p,cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'IAG') then
		begin
		ds_titulo_w	:= obter_desc_expressao(350668)	|| Plt_Obter_Qtd_Item_liberado('IAG',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'I') then
		begin
		ds_titulo_w	:= obter_desc_expressao(292249) || Plt_Obter_Qtd_Item_liberado('I',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'L') then
		begin
		ds_titulo_w	:= obter_desc_expressao(314959) || Plt_Obter_Qtd_Item_liberado('L',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'O') then
		begin
		ds_titulo_w	:= obter_desc_expressao(290567)	|| Plt_Obter_Qtd_Item_liberado('O',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'R') then
		begin
		ds_titulo_w	:= obter_desc_expressao(314282)	|| Plt_Obter_Qtd_Item_liberado('R',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'B') then
		begin
		ds_titulo_w	:= obter_desc_expressao(333347) || Plt_Obter_Qtd_Item_liberado('B',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'E') then
		begin
		ds_titulo_w	:= obter_desc_expressao(966978) || Plt_Obter_Qtd_Item_liberado('E',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;
	elsif	(ie_tipo_item_p = 'LD') then
		begin
		ds_titulo_w	:= obter_desc_expressao(314294) || Plt_Obter_Qtd_Item_liberado('LD',cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
		end;	
	elsif	(ie_tipo_item_p = 'SUSP') then
		begin
		ds_titulo_w	:= obter_desc_expressao(307771);
		end;
	elsif	(ie_tipo_item_p = 'NANNPT') then
		begin
		ds_titulo_w	:= obter_desc_expressao(1028131);
		end;
	end if;
	
else
	ds_titulo_w:= ds_titulo_w || ' ' ||Plt_Obter_Qtd_Item_liberado(ie_tipo_item_p,cd_perfil_w,wheb_usuario_pck.get_nm_usuario);
end if;
	
return upper(ds_titulo_w);

end obter_titulos_gestao_adep;
/
