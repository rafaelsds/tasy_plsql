create or replace
function obter_desc_kit_estoque_mat (	cd_kit_material_p	number,
					nr_seq_kit_estoque_p	number)
					return varchar2 is

ds_retorno_w	VARCHAR2(80);					
					
begin

if	(nvl(cd_kit_material_p,0) > 0) then

	ds_retorno_w := substr(obter_desc_kit(cd_kit_material_p),1,80);
	 		
	
elsif (nvl(nr_seq_kit_estoque_p,0) > 0) then
	
	ds_retorno_w := substr(obter_desc_kit_estoque(nr_seq_kit_estoque_p),1,80);

end if;

return	ds_retorno_w;

end obter_desc_kit_estoque_mat;
/