create or replace
function remove_html_from_text(
					ds_texto_p 		varchar2)
				return varchar2 is

ds_sql_w			varchar2(2000);
ds_return_w			varchar2(3200) := '';

begin

if (ds_texto_p is not null) then
	
	ds_sql_w :=   'select remove_formatacao_rtf_html(:ds_texto_p) from dual';
    
	execute immediate
			ds_sql_w
	into	ds_return_w
	using	ds_texto_p;
	
end if;

return ds_return_w;

end remove_html_from_text;
/
