create or replace
function obter_local_estoque_regra_ged(	cd_setor_atendimento_p	number,
										nr_prescricao_p			number default null,
										nr_seq_material_p		number default null,
										ie_opcao_p				varchar2 default null,
										dt_horario_p			date default null,
										ie_dose_especial_p      varchar2 default 'N')
										return number is

/*
ie_opcao_p
S = Sequencia da regra.
*/

cd_local_estoque_w			number(4,0)	:= null;
cd_grupo_material_w			number(3) 	:= null;
cd_subgrupo_material_w		number(3) 	:= null;
cd_classe_material_w		number(5) 	:= null;
cd_material_w				number(6) 	:= null;
nr_seq_familia_w			number(10) 	:= null;
cd_material_estoque_w		number(3) 	:= null;
ie_via_aplicacao_w			varchar2(5) := null;
cd_setor_prescricao_w		number(5) 	:= null;
cd_local_estoque_prescr_w	number(4,0) := null;
ie_urgencia_w				varchar2(1);
ie_acm_w					varchar2(1);
ie_se_necessario_w			varchar2(1);
nr_sequencia_w				number(10);
nr_retorno_w				number(10);
qt_minuto_w					number(10);
qt_dose_especial_w			number(18,6);
ie_dose_espec_agora_w		varchar2(1);
ie_classif_urgente_w		prescr_mat_hor.ie_classif_urgente%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

cursor C01 is
	select	a.cd_local_estoque,
			a.nr_sequencia
	from	regra_local_disp_gedipa a
	where	nvl(a.cd_setor_atendimento, nvl(cd_setor_atendimento_p,0))	= nvl(cd_setor_atendimento_p,0)
  and   nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_w,0))	= nvl(cd_estabelecimento_w,0)
	and 	nvl(a.cd_setor_prescricao,nvl(cd_setor_prescricao_w,0))		= nvl(cd_setor_prescricao_w,0)
	and		nvl(a.cd_grupo_material, nvl(cd_grupo_material_w,0))		= nvl(cd_grupo_material_w,0)
	and		nvl(a.cd_subgrupo_material, nvl(cd_subgrupo_material_w,0))	= nvl(cd_subgrupo_material_w,0)
	and		nvl(a.cd_classe_material, nvl(cd_classe_material_w,0))		= nvl(cd_classe_material_w,0)
	and		nvl(a.cd_material, nvl(cd_material_w,0))					= nvl(cd_material_w,0)
	and		nvl(nr_seq_familia, nvl(nr_seq_familia_w,0))				= nvl(nr_seq_familia_w,0)
	--and		qt_minuto_w between nvl(qt_min_inicio,-999999999) and nvl(qt_max_inicio,999999999)
	and		nvl(a.ie_via_aplicacao, nvl(ie_via_aplicacao_w,'XX'))		= nvl(ie_via_aplicacao_w,'XX')
	and		((nvl(ie_agora,'N') = 'N') or ((ie_agora = 'S') and (ie_urgencia_w = 'S')))
	and		(((nvl(ie_somente_acmsn,'N') = 'G') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S'))) or
			((nvl(ie_somente_acmsn,'N') = 'S') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S'))) or
			((nvl(ie_somente_acmsn,'N') = 'A') and (ie_acm_w = 'S') and (ie_se_necessario_w = 'N')) or
			((nvl(ie_somente_acmsn,'N') = 'C') and (ie_acm_w = 'N') and (ie_se_necessario_w = 'S')) or
			(nvl(ie_somente_acmsn,'N') = 'N'))
	and 	((nvl(ie_dose_especial,'N') = 'N') or ((nvl(ie_dose_especial,'N') = 'S') and (ie_dose_especial_p = 'S')))
	and     ((nvl(ie_tipo_lote,'X') = 'X') or
	         (ie_tipo_lote = 'A' and ie_classif_urgente_w = 'A') or
			 (ie_tipo_lote = 'N' and ie_classif_urgente_w = 'N') or
			 (ie_tipo_lote = 'E' and ie_classif_urgente_w = 'E'))
	order by nvl(a.cd_material,0),
			nvl(ie_agora,'AAAAA'),
			nvl(cd_setor_atendimento,0000000),			
			nvl(a.cd_grupo_material,0),
			nvl(a.cd_subgrupo_material,0),
			nvl(a.cd_classe_material,0),
			nvl(nr_seq_familia,0),
			nvl(a.ie_via_aplicacao,'AAAAA'),
			nvl(a.ie_dose_especial,'AAAA'),
			nvl(ie_tipo_lote,'AAAA');

begin

qt_minuto_w := nvl(Obter_Min_Entre_Datas(sysdate,dt_horario_p,1),0);

if	(nvl(nr_prescricao_p,0) > 0) and
	(nvl(nr_seq_material_p,0) > 0) then

	select	max(nvl(a.ie_via_aplicacao,'XPTO')),
			max(b.cd_grupo_material),
			max(b.cd_subgrupo_material),
			max(b.cd_classe_material),
			max(b.cd_material),
			max(nvl(b.nr_seq_familia,0)),
			max(nvl(to_number(substr(obter_setor_prescricao(a.nr_prescricao,'C'),1,5)),0)),
			max(obter_local_estoque_setor(c.cd_setor_atendimento, c.cd_estabelecimento)),
			max(nvl(ie_urgencia,'N')),
			max(nvl(a.ie_acm, 'N')),
			max(nvl(a.ie_se_necessario,'N')),
			max(nvl(qt_dose_especial,0)),
			max(ie_dose_espec_agora),
      max(c.cd_estabelecimento)
	into	ie_via_aplicacao_w,
			cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w,
			cd_material_w,
			nr_seq_familia_w,
			cd_setor_prescricao_w,
			cd_local_estoque_prescr_w,
			ie_urgencia_w,
			ie_acm_w,
			ie_se_necessario_w,
			qt_dose_especial_w,
			ie_dose_espec_agora_w,
      cd_estabelecimento_w
	from	prescr_medica c,
			estrutura_material_v b,
			prescr_material a
	where	b.cd_material		= a.cd_material
	and		a.nr_prescricao		= nr_prescricao_p
	and		a.nr_prescricao		= c.nr_prescricao
	and 	nr_sequencia 		= nr_seq_material_p;

elsif	(nvl(nr_prescricao_p,0) > 0) then

	select	max(obter_local_estoque_setor(cd_setor_atendimento, cd_estabelecimento)),
			max(cd_setor_atendimento),
      max(cd_estabelecimento)
	into	cd_local_estoque_prescr_w,
			cd_setor_prescricao_w,
      cd_estabelecimento_w
	from	prescr_medica
	where	nr_prescricao	= nr_prescricao_p;

end if;

if (nr_prescricao_p is not null) and
	((nr_seq_material_p is not null) and (nr_seq_material_p > 0)) and
	(dt_horario_p is not null) then

	select	max(nvl(ie_classif_urgente,'X'))
	into 	ie_classif_urgente_w
	from 	prescr_mat_hor
	where 	nr_prescricao 	= nr_prescricao_p
	and 	nr_seq_material	= nr_seq_material_p
	and 	dt_horario 		= dt_horario_p;

end if;
open C01;
loop
fetch C01 into
	cd_local_estoque_w,
	nr_sequencia_w;
exit when C01%notfound;
	begin
	cd_local_estoque_w	:= cd_local_estoque_w;
	nr_sequencia_w		:= nr_sequencia_w;
	end;
end loop;
close C01;

nr_retorno_w := nvl(cd_local_estoque_w,cd_local_estoque_prescr_w);

if	(nvl(ie_opcao_p,'X') = 'S') then
	nr_retorno_w := nvl(nr_sequencia_w,0);
end if;

return nr_retorno_w;

end obter_local_estoque_regra_ged;
/