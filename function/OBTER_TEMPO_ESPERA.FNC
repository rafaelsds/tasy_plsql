create or replace
function obter_tempo_espera(	nr_seq_agecons_p	number,
								ie_opcao_p 			varchar2) 
								return varchar2 is

dt_retorno_w	varchar2(255);
	
/*
Tempo prevista de espera (minutos) - 'P'
Tempo real de espera (minutos) - 'R'
*/

begin

if (nr_seq_agecons_p is not null) and 
	(ie_opcao_p is not null) then
if (ie_opcao_p = 'P') then
	select 	max(pls_obter_dif_data_horario(a.dt_entrada, b.dt_agenda))
	into 	dt_retorno_w
	from 	atendimento_paciente a,
			agenda_consulta b
	where 	a.nr_atendimento = b.nr_atendimento
	and 	b.nr_sequencia = nr_seq_agecons_p;
	
elsif (ie_opcao_p = 'R') then 	
	select 	max(pls_obter_dif_data_horario(a.dt_entrada, b.dt_inicio_prescr))
	into 	dt_retorno_w
	from 	atendimento_paciente a,
			prescr_medica b
	where 	a.nr_atendimento = b.nr_atendimento
	and 	b.nr_seq_agecons = nr_seq_agecons_p;
end if;
end if;
return dt_retorno_w;

end obter_tempo_espera;
/
