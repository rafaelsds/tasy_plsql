create or replace
function gpt_get_release_status(nm_tabela_p		varchar2,
								nr_sequencia_p	number,
								si_param_1_p	varchar2)
								return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_item_w	varchar2(3);
dt_release_w	date;
si_option_w		varchar2(3);
si_return_w		varchar2(3);

begin

ie_tipo_item_w	:= cpoe_obter_tipo_item(nm_tabela_p,nr_sequencia_p);

if	(si_param_1_p = 'E') then
	si_option_w	:= 'LE';
else
	si_option_w	:= 'F';
end if;

dt_release_w	:= cpoe_obter_dado_prescr(nr_sequencia_p,ie_tipo_item_w,si_option_w);

if	(dt_release_w is not null) then
	/* Released */
	si_return_w	:= '0';
else
	/* Pending */
	si_return_w	:= '1';
end if;


return	si_return_w;

end gpt_get_release_status;
/