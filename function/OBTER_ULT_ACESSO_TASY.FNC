create or replace
function obter_ult_acesso_tasy(	nm_usuario_p		varchar2)
 		    	return date is

dt_retorno_w		date;

begin
select	max(a.dt_acesso)
into	dt_retorno_w
from	tasy_log_acesso a
where	a.nr_sequencia = (	select	max(x.nr_sequencia)
			from	tasy_log_acesso x
			where	x.nm_usuario = nm_usuario_p
			and	x.ie_result_acesso = 'N'
			and	dt_acesso > sysdate - 60);

return	dt_retorno_w;

end obter_ult_acesso_tasy;
/
