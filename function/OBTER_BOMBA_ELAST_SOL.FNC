create or replace
function obter_bomba_elast_sol(
			nr_prescricao_p		number,
			nr_seq_solucao_p	number) 
			return number is 
pragma autonomous_transaction;
begin

return Obter_bomba_elastomerica(nr_prescricao_p, nr_seq_solucao_p,'C');

end obter_bomba_elast_sol;
/