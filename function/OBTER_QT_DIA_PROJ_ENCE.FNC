create or replace
function obter_qt_dia_proj_ence(dt_encerramento_p		date)
				return number is

ds_retorno_w		number(10);				
begin

select  nvl(obter_dias_entre_datas(dt_encerramento_p,sysdate),0)
into	ds_retorno_w  
from 	dual;

return	ds_retorno_w;

end obter_qt_dia_proj_ence;
/