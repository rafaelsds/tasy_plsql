create or replace
function Obter_Dados_ProPaci
		(IE_OPCAO_P			varchar2,
		 NR_SEQ_PROCEDIMENTO_P		number)
		return varchar2 is
/*	CP	-	RETORNA C�DIGO DO PROCEDIMENTO
	DP	-	RETORNA DATA DO PROCEDIMENTO
	ME	-	RETORNA C�DIGO DO M�DICO EXECUTOR
	NC	-	RETORNA N�MERO DA CIRURGIA
	IC	-	RETORNA NR_INTERNO_CONTA
	KIT	-	REORNA O KIT MATERIAL
	DS	- 	Retorna o DS_PROCEDIMENTO
	U	- 	Unidade Medida
	R	-	Respons�vel pelo cr�dito do procedimento
	TC	-	C�digo do Procedimento TUSS
	TD	-	Descri��o do procedimento TUSS
	NG	-	N�mero da guia
	NR	-	N�mero do repasse
	O	- 	Origem do procedimento
	FM	-	Descri��o da fun��o do m�dico
	EM	-	Descri��o da Especialidade
	ATO	- 	N�mero do Ato Ipasgo
*/

DT_PROCEDIMENTO_W		VARCHAR2(25);
CD_PROCEDIMENTO_W		VARCHAR2(15);
CD_MEDICO_EXECUTOR_W		VARCHAR2(10);
NR_CIRURGIA_W			VARCHAR2(10);
NR_INTERNO_CONTA_W		VARCHAR2(10);
IE_ORIGEM_PROCED_W		NUMBER(10);
CD_KIT_MATERIAL_W		number(5);
CD_UNIDADE_MEDICA_W		varchar2(20);
ds_procedimento_w		PROCEDIMENTO.DS_PROCEDIMENTO%type;
ie_responsavel_credito_w	varchar2(5);
cd_procedimento_tuss_w		number(15);
ds_procedimento_tuss_w		PROCEDIMENTO.DS_PROCEDIMENTO%type;
dt_procedimento_f_w		varchar2(80);
nr_doc_convenio_w		varchar2(255);
nr_repasse_terceiro_w		number(10);
ie_funcao_medico_w		procedimento_paciente.ie_funcao_medico%type;
ds_funcao_w			funcao_medico.ds_funcao%type;
cd_especialidade_w		procedimento_paciente.cd_especialidade%type;
ds_especialidade_w		especialidade_medica.ds_especialidade%type;
nr_ato_ipasgo_w			procedimento_paciente.nr_ato_ipasgo%type;

begin
begin
SELECT	A.DT_PROCEDIMENTO,
	A.CD_PROCEDIMENTO,
	A.CD_MEDICO_EXECUTOR,
	A.NR_CIRURGIA,
	A.NR_INTERNO_CONTA,
	A.IE_ORIGEM_PROCED,
	substr(obter_unid_med_Servico(cd_convenio, cd_categoria, cd_procedimento, ie_origem_proced, obter_estab_atend(nr_atendimento)),1,10),
	obter_descricao_procedimento(a.cd_procedimento,a.ie_origem_proced),
	ie_responsavel_credito,
	cd_procedimento_tuss,
	obter_descricao_procedimento(a.cd_procedimento_tuss,8),
	to_char(A.DT_PROCEDIMENTO,'dd/mm/yyyy hh24:mi:ss'),
	a.nr_doc_convenio,
	ie_funcao_medico,
	cd_especialidade,
	nr_ato_ipasgo
INTO	DT_PROCEDIMENTO_W,
	CD_PROCEDIMENTO_W,
	CD_MEDICO_EXECUTOR_W,
	NR_CIRURGIA_W,
	NR_INTERNO_CONTA_W,
	IE_ORIGEM_PROCED_W,
	CD_UNIDADE_MEDICA_W,
	ds_procedimento_w,
	ie_responsavel_credito_w,
	cd_procedimento_tuss_w,
	ds_procedimento_tuss_w,
	dt_procedimento_f_w,
	nr_doc_convenio_w,
	ie_funcao_medico_w,
	cd_especialidade_w,
	nr_ato_ipasgo_w
FROM	procedimento_paciente a
WHERE	NR_SEQUENCIA = NR_SEQ_PROCEDIMENTO_P;
exception
when others then
	IE_ORIGEM_PROCED_W	:= IE_ORIGEM_PROCED_W;
end;

if	IE_OPCAO_P = 'DP'	then
	return DT_PROCEDIMENTO_W;
elsif	IE_OPCAO_P = 'DPF'	then
	return dt_procedimento_f_w;
elsif	IE_OPCAO_P = 'CP'	then
	return CD_PROCEDIMENTO_W;
elsif	IE_OPCAO_P = 'ME'	then
	return CD_MEDICO_EXECUTOR_W;
elsif	IE_OPCAO_P = 'NC'	then
	return NR_CIRURGIA_W;
elsif	IE_OPCAO_P = 'IC'	then
	return NR_INTERNO_CONTA_W;
elsif	(IE_OPCAO_P	= 'DS') then
	return ds_procedimento_w;
elsif	IE_OPCAO_P = 'KIT'	then
	SELECT	CD_KIT_MATERIAL
	INTO	CD_KIT_MATERIAL_W
	FROM	PROCEDIMENTO
	WHERE	CD_PROCEDIMENTO = CD_PROCEDIMENTO_W
	AND	IE_ORIGEM_PROCED = IE_ORIGEM_PROCED_W;
	return	CD_KIT_MATERIAL_W;
elsif	(ie_opcao_p	= 'U') then
	return	 CD_UNIDADE_MEDICA_W;
elsif	(ie_opcao_p	= 'R') then
	return	ie_responsavel_credito_w;
elsif	(ie_opcao_p	= 'TC') then
	return	cd_procedimento_tuss_w;
elsif	(ie_opcao_p	= 'TD') then
	return	ds_procedimento_tuss_w;
elsif	(ie_opcao_p	= 'NG') then
	return 	nr_doc_convenio_w;
elsif	(ie_opcao_p	= 'NR') then
	select	max(a.nr_repasse_terceiro)
	into	nr_repasse_terceiro_w
	from	procedimento_repasse a
	where	a.nr_seq_procedimento	= nr_seq_procedimento_p;

	return	nr_repasse_terceiro_w;
elsif	(ie_opcao_p	= 'O') then
	return 	IE_ORIGEM_PROCED_W;
elsif	(ie_opcao_p	= 'FM') then
	select	max(ds_funcao)
	into	ds_funcao_w
	from	funcao_medico
	where	cd_funcao = ie_funcao_medico_w;

	return 	ds_funcao_w;
elsif	(ie_opcao_p	= 'EM') then
	select	max(ds_especialidade)
	into	ds_especialidade_w
	from	especialidade_medica
	where	cd_especialidade = cd_especialidade_w;

	return 	ds_especialidade_w;
elsif	(ie_opcao_p	= 'ATO') then
	return 	nr_ato_ipasgo_w;
end if;
end;
/