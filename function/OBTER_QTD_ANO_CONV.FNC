create or replace
function obter_qtd_ano_conv(	dt_referencia_p		date,
				ie_opcao_p		number,
				ie_tipo_atendimento_p	number)
 		    	return number is

qtd_retorno_w		number(10,2);			
qtd_contas_total_w	number(10);
qtd_contas_conv_w	number(10);

begin

select	sum(nr_pac_dia) 
into	qtd_contas_conv_w
from	W_HMJ_REL_FILANTROPIA
where	trunc(dt_referencia,'year') = trunc(sysdate,'year')
and	cd_convenio in (	select	cd_convenio
							from	W_HMJ_REL_FILANTROPIA
							where	dt_referencia between trunc(dt_referencia_p,'month') and last_day(dt_referencia_p));

select	sum(nr_pac_dia)
into	qtd_contas_total_w
from	W_HMJ_REL_FILANTROPIA
where	trunc(dt_referencia,'year') = trunc(sysdate,'year');

if	(ie_opcao_p = 1) then
	qtd_retorno_w := qtd_contas_conv_w;
elsif	(ie_opcao_p = 2) then
	qtd_retorno_w := round(qtd_contas_conv_w * 100 / qtd_contas_total_w,2);
end if;

return	qtd_retorno_w;

end obter_qtd_ano_conv;
/
