create or replace
function obter_sigla_exame_fleury_diag(
			nr_prescricao_p         number,
                    		nr_seq_prescr_p         number
				)
 		    	return varchar2 is

cd_sigla_w		varchar2(20) := null;
nr_seq_proc_interno_w	number(10);
cd_estabelecimento_w	prescr_medica.cd_estabelecimento%type;
ie_tipo_atend_w		number(4);
			
begin

select	max(p.nr_seq_proc_interno)	
into	nr_seq_proc_interno_w
from	prescr_procedimento p,
	proc_interno i
where	p.nr_seq_proc_interno 	= i.nr_sequencia
and	p.nr_prescricao	 	= nr_prescricao_p
and	p.nr_sequencia   	= nr_seq_prescr_p
and	p.nr_seq_exame is null
and	i.ie_tipo 		not in ('AP','APH','APC');

if	(nr_seq_proc_interno_w is not null) then

	select	max(nvl(b.cd_estabelecimento,0)),
			max(nvl(a.ie_tipo_atendimento,0))
	into	cd_estabelecimento_w,
			ie_tipo_atend_w	
	from	atendimento_paciente a,
			prescr_medica b
	where	a.nr_atendimento = b.nr_atendimento
	and		b.nr_prescricao = nr_prescricao_p;
	
	select	--max(cd_integracao)
			max(laudo_obter_cd_integracao(nr_prescricao_p, nr_seq_prescr_p, 6))
	into	cd_sigla_w
	from	regra_proc_interno_integra
	where	nr_seq_proc_interno 	= nr_seq_proc_interno_w
	and		nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and		nvl(ie_tipo_atendimento, ie_tipo_atend_w) = ie_tipo_atend_w
	and	ie_tipo_integracao 	= 6;

end if;

return	cd_sigla_w;

end obter_sigla_exame_fleury_diag;
/