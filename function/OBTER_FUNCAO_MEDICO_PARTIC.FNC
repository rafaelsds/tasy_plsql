create or replace function obter_funcao_medico_partic (nr_seq_propaci_p	procedimento_participante.nr_sequencia%type,
						nr_seq_partic_p		procedimento_participante.nr_seq_partic%type)
						return varchar2 is
			
ie_funcao_w	procedimento_participante.ie_funcao%type;
			
begin

	if	(nvl(nr_seq_propaci_p,0) > 0) and
		(nvl(nr_seq_partic_p,0) > 0) then
		
		select	a.ie_funcao
		into	ie_funcao_w
		from	procedimento_participante a
		where	a.nr_sequencia = nr_seq_propaci_p
		and	a.nr_seq_partic = nr_seq_partic_p;
		
	end if;

	return ie_funcao_w;

end obter_funcao_medico_partic;

/
