create or replace function obter_execucao( nr_atendimento_p number )
  return varchar2 is
  existe number;
BEGIN
  select count(1)
    into existe
    from imagem_pac_prot_externo
   where nr_atendimento = nr_atendimento_p;
  if existe > 0 then
     RETURN obter_desc_expressao(718223); -- Externo;
   else
     RETURN obter_desc_expressao(697085); -- Interno
   end if;
   
END obter_execucao;
/