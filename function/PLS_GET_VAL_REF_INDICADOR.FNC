create or replace
function pls_get_val_ref_indicador (
    nr_sequencia_p pls_indicador_cred_prest.nr_sequencia%type,
    nr_valor_p pls_indicador_cred_prest.vl_maximo%type
) return varchar2 is
ds_referencia_w pls_indicador_cred_prest.nr_sequencia%type;
ds_resultado_w varchar(10);

begin
    select max(nr_sequencia)
    into ds_referencia_w
    from pls_indicador_cred_prest 
    where nr_sequencia = nr_sequencia_p 
        and vl_minimo <= nr_valor_p 
        and vl_maximo >= nr_valor_p;

    if (ds_referencia_w is null) then
        ds_resultado_w := obter_expressao_idioma(928855);
    else
        ds_resultado_w := obter_expressao_idioma(928857);
    end if;
    return ds_resultado_w;
end pls_get_val_ref_indicador;
/
