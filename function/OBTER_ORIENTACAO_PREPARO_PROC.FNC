create or replace FUNCTION obter_orientacao_preparo_proc(nr_seq_proc_interno_p IN NUMBER,
                                       cd_estabelecimento_p  IN NUMBER,
				       nr_seq_topografia_p   IN NUMBER DEFAULT NULL,
				       ie_tipo_atendimento_p IN NUMBER DEFAULT NULL) RETURN varchar2 IS

TYPE lista_macros IS TABLE OF VARCHAR2(255) INDEX BY PLS_INTEGER;
					   
  return_w       varchar2(6000) := '';
  ds_orientacao_ageint_w       varchar2(4000) := '';
  nr_row         NUMBER := 0;
  ds_separador_w VARCHAR2(3) := chr(10);
  qt_valor_w	AGEINT_ORIENT_PREP_MACRO.vl_macro%type := 0;
  lista_macros_w	lista_macros;
  ind	number(10);
  ds_lista_macro_w varchar2(4000) := ageint_obter_macro_prep('S');
  ds_macro_w varchar2(255) := '';

BEGIN

	ind := 1;
	while (length(ds_lista_macro_w) > 0) loop
		ds_macro_w := substr(ds_lista_macro_w, 1, instr(ds_lista_macro_w,';')-1);
		ds_lista_macro_w := substr(ds_lista_macro_w, instr(ds_lista_macro_w,';')+1);
		lista_macros_w(ind) := ds_macro_w;
		ind := ind+1;
	end loop;

   FOR i IN (SELECT  ds_orientacao_preparo,
                    nr_sequencia AS nr_row
              FROM (SELECT aop.nr_sequencia,
                           aopr.nr_seq_orient_preparo,
                           aop.ds_orientacao_preparo
                      FROM ageint_orient_preparo    aop,
                           ageint_orient_prep_regra aopr
                     WHERE aop.nr_sequencia = aopr.nr_seq_orient_preparo
                       AND nvl(aopr.nr_seq_proc_interno, nr_seq_proc_interno_p) = nr_seq_proc_interno_p
                       AND nvl(aopr.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
		       AND nvl(aopr.nr_seq_topografia, nvl(nr_seq_topografia_p,0)) = nvl(nr_seq_topografia_p,0)
		       --AND nvl(aopr.ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0)) = nvl(ie_tipo_atendimento_p,0)
                       AND aop.ie_situacao = 'A'
                     ORDER BY nvl(aopr.nr_seq_apres,999), aop.nr_sequencia)) LOOP
    IF nr_row  <> i.nr_row THEN
		ds_orientacao_ageint_w := i.ds_orientacao_preparo;
		if (ds_orientacao_ageint_w is not null) then
			ind := lista_macros_w.last;
			while (ind > 0) loop
				select nvl(max(vl_macro),0)
				into qt_valor_w
				from AGEINT_ORIENT_PREP_MACRO a
				where ie_macro = ind
				and a.nr_seq_orient_prep_regra in 
								(select nr_sequencia
								from AGEINT_ORIENT_PREP_REGRA x
								where nvl(x.cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
								and nvl(x.nr_seq_proc_interno,nr_seq_proc_interno_p) = nr_seq_proc_interno_p
								AND nvl(x.nr_seq_topografia, nvl(nr_seq_topografia_p,0)) = nvl(nr_seq_topografia_p,0)
								--AND nvl(x.ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0)) = nvl(ie_tipo_atendimento_p,0)
								);
				ds_orientacao_ageint_w := replace_macro_clob(ds_orientacao_ageint_w, lista_macros_w(ind), qt_valor_w);
				ind := ind-1;
			end loop;
		end if;
        return_w := return_w || ds_orientacao_ageint_w || ds_separador_w;
        nr_row := i.nr_row ;
    END IF;

  END LOOP;

  RETURN return_w ;
END obter_orientacao_preparo_proc;
/