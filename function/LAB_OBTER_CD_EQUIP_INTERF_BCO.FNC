create or replace function Lab_obter_cd_equip_interf_bco(nr_seq_exame_p		NUMBER,
					ds_sigla_p		VARCHAR2,
					ie_opcao_p		VARCHAR2,
					ie_tipo_atendimento_p	NUMBER,
					cd_estabelecimento_p	NUMBER)
 		    	RETURN VARCHAR2 IS

cd_exame_equip_integr_w		VARCHAR2(20);
Resultado_w	 		VARCHAR2(50);
DS_SIGLA_EQUIP_w		VARCHAR2(20);

BEGIN
SELECT 	NVL(MAX(b.cd_exame_equip),NULL),
	MAX(DS_SIGLA_EQUIP)
INTO 	cd_exame_equip_integr_w,
	DS_SIGLA_EQUIP_w
FROM equipamento_lab a,
	exame_lab_equip_regra c,
     lab_exame_equip b
WHERE a.cd_equipamento	= b.cd_equipamento
 AND b.nr_sequencia	= c.nr_seq_exame_equip
 AND b.nr_seq_exame 	= nr_seq_exame_p
 AND a.ds_sigla 	= ds_sigla_p
 AND b.ie_tipo_atendimento = NVL(ie_tipo_atendimento_p,b.ie_tipo_atendimento)
 AND c.cd_estabelecimento = cd_estabelecimento_p
 AND c.ie_dia_semana	= TO_NUMBER(TO_CHAR(SYSDATE, 'd'))
 AND SYSDATE	>= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy') || ' ' || c.ds_hora_inicio || ':00','dd/mm/yyyy hh24:mi:ss')
 AND SYSDATE	<= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy') || ' ' || c.ds_hora_fim || ':00','dd/mm/yyyy hh24:mi:ss')
 AND DS_FORMA_ENVIO_EQUIP = 'BCO';

 IF	(cd_exame_equip_integr_w IS NULL) THEN

	SELECT 	NVL(MAX(b.cd_exame_equip),NULL),
		MAX(DS_SIGLA_EQUIP)
	INTO 	cd_exame_equip_integr_w,
		DS_SIGLA_EQUIP_w
	FROM equipamento_lab a,
		exame_lab_equip_regra c,
	     lab_exame_equip b
	WHERE a.cd_equipamento	= b.cd_equipamento
	 AND b.nr_sequencia	= c.nr_seq_exame_equip
	 AND b.nr_seq_exame 	= nr_seq_exame_p
	 AND a.ds_sigla 	= ds_sigla_p
	 AND b.ie_tipo_atendimento IS NULL
	 AND c.cd_estabelecimento = cd_estabelecimento_p
	 AND DS_FORMA_ENVIO_EQUIP = 'BCO'
	 AND c.ie_dia_semana	= TO_NUMBER(TO_CHAR(SYSDATE, 'd'))
	 AND SYSDATE	>= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy') || ' ' || c.ds_hora_inicio || ':00','dd/mm/yyyy hh24:mi:ss')
	 AND SYSDATE	<= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy') || ' ' || c.ds_hora_fim || ':00','dd/mm/yyyy hh24:mi:ss')  ;

	IF	(cd_exame_equip_integr_w IS NULL) THEN
		SELECT 	NVL(MAX(b.cd_exame_equip),NULL),
			MAX(DS_SIGLA_EQUIP)
		INTO 	cd_exame_equip_integr_w,
			DS_SIGLA_EQUIP_w
		FROM 	equipamento_lab a,
			lab_exame_equip b
		WHERE a.cd_equipamento	= b.cd_equipamento
		AND b.nr_seq_exame 	= nr_seq_exame_p
		AND DS_FORMA_ENVIO_EQUIP = 'BCO'
		AND b.ie_tipo_atendimento = NVL(ie_tipo_atendimento_p,b.ie_tipo_atendimento)
		AND a.ds_sigla 	= ds_sigla_p;

		IF	(cd_exame_equip_integr_w IS NULL) THEN
			SELECT 	NVL(MAX(b.cd_exame_equip),NULL),
				MAX(DS_SIGLA_EQUIP)
			INTO 	cd_exame_equip_integr_w,
				DS_SIGLA_EQUIP_w
			FROM 	equipamento_lab a,
				lab_exame_equip b
			WHERE a.cd_equipamento	= b.cd_equipamento
			AND DS_FORMA_ENVIO_EQUIP = 'BCO'
			AND b.nr_seq_exame 	= nr_seq_exame_p
			AND b.ie_tipo_atendimento IS NULL
			AND a.ds_sigla 	= ds_sigla_p;
		END IF;
	END IF;
END IF;

IF	(ie_opcao_p	= 'SE') THEN
	Resultado_w	:= DS_SIGLA_EQUIP_w;
END IF;

RETURN	Resultado_w;

END Lab_obter_cd_equip_interf_bco;
/