create or replace
function Obter_Se_prescr_hor
		(nr_prescricao_p	number,
		nr_seq_turno_p		number)
		return varchar2 is


ie_retorno_w		varchar2(01)	:= 'N';
nr_sequencia_w		number(10,0);

cursor	c01 is
	select	a.nr_sequencia
	from	prescr_material a
	where	nr_prescricao	= nr_prescricao_p;

begin

open	c01;
loop
fetch	c01 into
	nr_sequencia_w;
exit	when	c01%notfound;
	begin

	if	(ie_retorno_w	= 'N') then
		select	nvl(max('S'), 'N')
		into	ie_retorno_w
		from	prescr_mat_hor
		where	nr_prescricao		= nr_prescricao_p
		and	nr_seq_material		= nr_sequencia_w
		and	nr_seq_turno		= nr_seq_turno_p
		and	dt_disp_farmacia	is null
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
	end if;

	end;
end loop;
close c01;

return	ie_retorno_w;

end Obter_Se_prescr_hor;
/