create or replace
function obter_se_tipo_guia_conta
		(ie_tipo_atendimento_p		number,
		ie_tipo_guia_p			varchar2)
		return varchar2 is

ie_liberado_w	varchar2(01)	:= 'S';
qt_reg_w		number(10,0);

begin

select	count(*)
into	qt_reg_w
from 	tipo_guia_conta;

if	(qt_reg_w	> 0) then
	begin

	select	nvl(max('S'), 'N')
	into	ie_liberado_w
	from	tipo_guia_conta
	where	ie_tipo_atendimento	= ie_tipo_atendimento_p
	and	ie_tipo_guia	= ie_tipo_guia_p;

	end;
end if;

return	ie_liberado_w;

end obter_se_tipo_guia_conta;
/