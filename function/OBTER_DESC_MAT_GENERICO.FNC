create or replace
function	obter_desc_mat_generico(cd_material_p		number) return varchar2 is

ds_retorno_w		varchar2(255);

begin

select	substr(b.ds_material,1,255)
into	ds_retorno_w
from	material a,
		estrutura_material_v b
where	a.cd_material = cd_material_p
and		b.cd_material = nvl(a.cd_material_generico,a.cd_material);

return ds_retorno_w;

end obter_desc_mat_generico;
/
