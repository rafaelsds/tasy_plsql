create or replace
function pls_obter_nome_grupo_auditor
		(	nr_seq_grupo_p		Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(120);			

begin

select	nvl(max(nm_grupo_auditor),'')
into	ds_retorno_w
from	pls_grupo_auditor
where	nr_sequencia	= nr_seq_grupo_p;

return	ds_retorno_w;

end pls_obter_nome_grupo_auditor;
/