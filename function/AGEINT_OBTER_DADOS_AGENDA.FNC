create or replace
function ageint_obter_dados_agenda( nr_seq_agenda_cons_p		number,
				    nr_seq_agenda_exa_p			number,	
				    nr_seq_quimio_p			number,
				    nr_seq_item_p			number,
				    nr_seq_lab_p			number,
				    ie_retorno_p			varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
dt_data_agenda_w	varchar2(20);
ie_status_w		varchar2(10);
nr_sequencia_w		varchar2(10);
cd_profissional_w	varchar2(10);
nr_seq_pend_quimio_w	number(10,0);
cd_estab_ageint_w	number(4,0);

/*
S =  nr_seq_status
DI =  dt_inicio_agendamento
SE = nr_sequencia
EI = estab agenda integrada
*/
			
begin

ds_retorno_w := '';
dt_data_agenda_w := '';
ie_status_w := '';

if (nr_seq_agenda_cons_p is not null) and (nr_seq_agenda_cons_p <> 0) then
	select 	max(nr_seq_status),
		max(to_char(dt_inicio_agendamento,'dd/mm/yyyy')),
		max(a.nr_sequencia),
		max(a.cd_profissional),
		max(a.cd_estabelecimento)
	into	ie_status_w,
		dt_data_agenda_w,
		nr_sequencia_w,
		cd_profissional_w,
		cd_estab_ageint_w
	from	agenda_integrada a,
		agenda_integrada_item b
	where	a.nr_sequencia = b.nr_seq_agenda_int
	and	b.nr_seq_agenda_cons = nr_seq_agenda_cons_p;
elsif ((nr_seq_agenda_exa_p is not null) and (nr_seq_agenda_exa_p <> 0)) then
	select 	max(nr_seq_status),
		max(to_char(dt_inicio_agendamento,'dd/mm/yyyy')),
		max(a.nr_sequencia),
		max(a.cd_profissional),
		max(a.cd_estabelecimento)
	into	ie_status_w,
		dt_data_agenda_w,
		nr_sequencia_w,
		cd_profissional_w,
		cd_estab_ageint_w
	from	agenda_integrada a,
		agenda_integrada_item b
	where	a.nr_sequencia = b.nr_seq_agenda_int
	and	b.nr_seq_agenda_exame = nr_seq_agenda_exa_p;
elsif ((nr_seq_quimio_p is not null) and (nr_seq_quimio_p <> 0)) then
	
	select	max(b.nr_seq_pend_agenda)
	into	nr_seq_pend_quimio_w
	from	agenda_quimio a,
		paciente_atendimento b
	where	a.nr_seq_atendimento = b.nr_seq_atendimento
	and	a.nr_sequencia = nr_seq_quimio_p;
	
	if	(nr_seq_pend_quimio_w > 0) then
	select 	max(nr_seq_status),
		max(to_char(dt_inicio_agendamento,'dd/mm/yyyy')),
		max(a.nr_sequencia),
		max(a.cd_profissional),
		max(a.cd_estabelecimento)
	into	ie_status_w,
		dt_data_agenda_w,
		nr_sequencia_w,
		cd_profissional_w,
		cd_estab_ageint_w
	from	agenda_integrada a,
		agenda_integrada_item b
	where	a.nr_sequencia = b.nr_seq_agenda_int
	and	b.nr_seq_pend_quimio = nr_seq_pend_quimio_w;
elsif ((nr_seq_item_p is not null) and (nr_seq_item_p <> 0)) then

	select 	max(nr_seq_status),
		max(to_char(dt_inicio_agendamento,'dd/mm/yyyy')),
		max(a.nr_sequencia),
		max(a.cd_profissional),
		max(a.cd_estabelecimento)
	into	ie_status_w,
		dt_data_agenda_w,
		nr_sequencia_w,
		cd_profissional_w,
		cd_estab_ageint_w
	from	agenda_integrada a,
		agenda_integrada_item b,
		agenda_quimio c
	where	a.nr_sequencia = b.nr_seq_agenda_int
	and	c.nr_seq_ageint_item = b.nr_sequencia
	and	b.nr_sequencia = nr_seq_item_p;	
		
	end if;
elsif ((nr_seq_lab_p is not null) and (nr_seq_lab_p <> 0)) then
	select 	max(nr_seq_status),
		max(to_char(dt_inicio_agendamento,'dd/mm/yyyy')),
		max(a.nr_sequencia),
		max(a.cd_profissional),
		max(a.cd_estabelecimento)
	into	ie_status_w,
		dt_data_agenda_w,
		nr_sequencia_w,
		cd_profissional_w,
		cd_estab_ageint_w
	from	agenda_integrada a,
		ageint_exame_lab b
	where	b.nr_seq_ageint = a.nr_sequencia
	and	b.nr_sequencia = nr_seq_lab_p;	
end if;

if (ie_retorno_p = 'S') then
	ds_retorno_w := ie_status_w;
elsif (ie_retorno_p = 'DI') then
	ds_retorno_w := dt_data_agenda_w;
elsif (ie_retorno_p = 'SE') then
	ds_retorno_w := nr_sequencia_w;
elsif (ie_retorno_p = 'EI') then
	ds_retorno_w := cd_estab_ageint_w;
elsif	(ie_Retorno_p	= 'P') then
	ds_retorno_w	:= cd_profissional_w;
end if;

return	ds_retorno_w;

end ageint_obter_dados_agenda;
/
