create or replace
function	obter_dados_cpi_solic(	nr_seq_solic_p	number,
					ie_opcao_p	varchar2)
		return varchar2 is

ds_retorno_w		varchar2(20);

/*

DTD - Data de Desejada

*/

begin

select	to_char(dt_desejada,'dd/mm/yyyy hh24:mi:ss')
into	ds_retorno_w
from	same_cpi_solic
where	nr_sequencia = nr_seq_solic_p;

return	ds_retorno_w;

end	obter_dados_cpi_solic;
/