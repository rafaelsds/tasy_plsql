create or replace
function obter_se_dente_protese_odont(
					nr_seq_area_p		number,
					nr_seq_dente_p		number,
					ie_protese_sup_p	varchar2,
					ie_protese_inf_p	varchar2)
				return varchar2 is

ds_retorno_w 	varchar2(1) := 'N';
nr_seq_dente_w	area_imagem_item.nr_seq_dente%type := nr_seq_dente_p;
				
begin

if (nr_seq_dente_w is null) then
	select	max(nr_seq_dente)
	into	nr_seq_dente_w
	from	area_imagem_item
	where	nr_seq_area = nr_seq_area_p;
end if;

if (nr_seq_dente_w is not null) then

	select	nvl(max('S'),'N') 
	into	ds_retorno_w
	from	dual
	where	((ie_protese_sup_p = 'S' and (nr_seq_dente_w between 10 and 29 or nr_seq_dente_w between 50 and 66)) or
			(ie_protese_inf_p = 'S' and  (nr_seq_dente_w between 30 and 49 or nr_seq_dente_w between 70 and 86)));
			
end if;
		
return ds_retorno_w;

end obter_se_dente_protese_odont;
/
