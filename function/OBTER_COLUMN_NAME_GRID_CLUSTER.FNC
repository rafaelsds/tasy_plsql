create or replace
function	obter_column_name_grid_cluster(
			nm_tabela_p	varchar2,
			nr_sequencia_p	number)
return	varchar2 is

nm_coluna_w	varchar2(30);

begin

select	column_name
into	nm_coluna_w
from	user_tab_columns
where	table_name = nm_tabela_p
and	substr(column_name,4,length(column_name)) = nr_sequencia_p
and	column_id > 3;

return	nm_coluna_w;

end;
/