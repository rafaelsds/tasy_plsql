create or replace
function obter_desc_lote_lp(	nr_lote_producao_p	number,
				cd_material_p		number)
				return varchar2 is

ds_todos_lote_fornec_w	varchar2(255);
ds_lote_fornec_w	varchar2(20);
				
Cursor C01 is
	select	distinct Obter_dados_lote_fornec(nr_seq_lote_fornec,'D')
	from	lp_item_util
	where	nr_lote_producao	=	nr_lote_producao_p
	and	cd_material		=	cd_material_p;
				
begin

open C01;
loop
fetch C01 into	
	ds_lote_fornec_w;
exit when C01%notfound;
	begin
	if	(ds_todos_lote_fornec_w is null) then
		ds_todos_lote_fornec_w	:= ds_lote_fornec_w;
	else	
		ds_todos_lote_fornec_w	:= ds_todos_lote_fornec_w || ', ' || ds_lote_fornec_w;
	end if;
	end;
end loop;
close C01;

return	ds_todos_lote_fornec_w;

end obter_desc_lote_lp;
/