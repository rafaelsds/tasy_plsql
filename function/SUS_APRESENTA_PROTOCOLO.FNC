create or replace
function sus_apresenta_protocolo (	nr_seq_protocolo_p		number,
				ie_tipo_financiamento_p	varchar2,
				ie_complexidade_p		varchar2)
				return varchar2 is
					
ds_retorno_w		varchar2(15);
ie_tipo_financ_w		varchar2(15);
ie_complexidade_w		varchar2(15);

begin

select	nvl(max(ie_tipo_financ_sus),'0'),
	nvl(max(ie_complexidade_sus),'0')
into	ie_tipo_financ_w,
	ie_complexidade_w
from	protocolo_convenio
where	nr_seq_protocolo = nr_seq_protocolo_p;

if	(ie_tipo_financ_w <> '0') and
	(ie_tipo_financ_w = ie_tipo_financiamento_p) and
	(ie_complexidade_w <> '0') and
	(ie_complexidade_w = ie_complexidade_p) then
	ds_retorno_w := 'S';
elsif	(ie_tipo_financ_w <> '0') and
	(ie_tipo_financ_w = ie_tipo_financiamento_p) and
	(ie_complexidade_w = '0') then
	ds_retorno_w := 'S';
elsif	(ie_tipo_financ_w = '0') and
	(ie_complexidade_w <> '0') and
	(ie_complexidade_w = ie_complexidade_p) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end sus_apresenta_protocolo;
/