create or replace
function obter_horas_min(  dt_inicial_p    date,
        dt_final_p      date)
           return varchar2 is

ds_retorno_w     varchar2(40) := '0';      
qt_horas_w      varchar2(40) := '0';
qt_minutos_w      varchar2(40) := '0';
      
begin

with t0 as (
select 
  dt_inicial_p 	data_inicial, 
  dt_final_p 	data_final
from dual),
t1 as (
select data_final - data_inicial diferenca from t0
)
select 
  trunc(diferenca)*24 + trunc((diferenca - trunc(diferenca))*24) horas,
  trunc((((diferenca - trunc(diferenca))*24) - trunc((diferenca - trunc(diferenca))*24))*60) minutos
  into 	qt_horas_w, 
		qt_minutos_w
from t1;

ds_retorno_w := qt_horas_w || 'h ' || qt_minutos_w || 'm '; 

return  ds_retorno_w;

end obter_horas_min;
/  
