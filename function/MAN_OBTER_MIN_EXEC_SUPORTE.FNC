create or replace
function Man_obter_min_exec_suporte (nr_seq_ordem_serv_p	number)
 		    	return number is

retorno_w	number(10);

begin

select 	sum(qt_minuto)
into	retorno_w
from	man_ordem_serv_ativ a,
		usuario b,
		man_ordem_servico c
where	a.nm_usuario_exec 		= b.nm_usuario
and		a.nr_seq_ordem_serv 	= c.nr_sequencia
and		b.cd_setor_atendimento 	= 4
and		c.nr_sequencia			= nr_seq_ordem_serv_p;

return	retorno_w;

end Man_obter_min_exec_suporte;
/
