create or replace
function obter_se_albubina_just(	nr_prescricao_p	number,
					nr_sequencia_p	number)
					return varchar is

cd_material_w		number(6,0);
ie_possui_regra_w	varchar2(1);
ie_albubina_justi_w	varchar2(1) := 'S';
nr_dia_util_w		number(10,0);
qt_dia_util_w		number(5,0);
dt_inicial_w		date;
dt_final_w		date;
nr_atendimento_w	number(10,0);

begin

select	a.cd_material,
	nvl(a.nr_dia_util,0),
	b.nr_atendimento,
	dt_inicio_prescr
into	cd_material_w,
	nr_dia_util_w,
	nr_atendimento_w,
	dt_final_w
from	prescr_medica b,
	prescr_material a
where	b.nr_prescricao = a.nr_prescricao
and	b.nr_prescricao = nr_prescricao_p
and	a.nr_prescricao	= nr_prescricao_p
and	a.nr_sequencia	= nr_sequencia_p;	

select	nvl(max('S'),'N')
into	ie_possui_regra_w
from	regra_medic_espec_rep
where	cd_material	=	cd_material_w
and	ie_regra = 'AH';

if	(ie_possui_regra_w = 'S') then
	dt_inicial_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate) - nr_dia_util_w;
--	dt_final_w	:= sysdate;

	select	nvl(sum(qt_dia_util),0)
	into	qt_dia_util_w
	from	prescr_medica_albumina a,
		prescr_medica_albu_indic b,
		prescr_material c,
		prescr_medica d
	where	a.nr_sequencia = b.nr_seq_prescr_albu
	and	a.nr_prescricao = c.nr_prescricao
	and	a.nr_seq_material = c.nr_sequencia
	and	c.nr_prescricao = d.nr_prescricao
	and	c.cd_material = cd_material_w
	and	c.ie_agrupador = 1
	and	c.dt_suspensao is null
	and	d.dt_suspensao is null
	and	nvl(d.dt_liberacao,d.dt_liberacao_medico) is not null
	and	d.dt_prescricao between dt_inicial_w and dt_final_w
	and	d.nr_atendimento = nr_atendimento_w;
	
	if	(qt_dia_util_w = 0) then
		begin
		select	nvl(max('S'),'N')
		into	ie_albubina_justi_w
		from	prescr_medica_albumina a,
			prescr_medica_albu_indic b
		where	a.nr_sequencia = b.nr_seq_prescr_albu
		and	a.nr_prescricao = nr_prescricao_p
		and	a.nr_seq_material = nr_sequencia_p;
		end;
	else
		begin
		if	(nr_dia_util_w > qt_dia_util_w) then
			select	nvl((qt_dia_util_w + nvl(max(QT_DIA_UTIL),0)),0)
			into	qt_dia_util_w
			from	prescr_medica_albumina a,
				prescr_medica_albu_indic b
			where	a.nr_sequencia = b.nr_seq_prescr_albu
			and	a.nr_prescricao = nr_prescricao_p
			and	a.nr_seq_material = nr_sequencia_p;
			
			if	(nr_dia_util_w > qt_dia_util_w) then
				ie_albubina_justi_w	:= 'N';
			else
				ie_albubina_justi_w	:= 'S';
			end if;
		else
			ie_albubina_justi_w	:= 'S';
		end if;
		end;
	end if;
end if;

return	ie_albubina_justi_w;

end obter_se_albubina_just;
/
