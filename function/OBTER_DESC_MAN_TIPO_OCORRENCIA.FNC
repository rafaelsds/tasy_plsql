create or replace
function obter_desc_man_tipo_ocorrencia(nr_seq_tipo_ocorr_p		MAN_TIPO_OCORRENCIA.nr_sequencia%type)
 		    	return varchar2 is
			
expressao_w	dic_expressao.ds_expressao_br%type;

begin

select  SUBSTR(obter_desc_expressao(cd_exp_tipo_ocorrencia , ds_tipo_ocorrencia),1,255) ds
into	expressao_w
from 	man_tipo_ocorrencia
where	nr_sequencia = nr_seq_tipo_ocorr_p;


return	expressao_w;

end obter_desc_man_tipo_ocorrencia;
/