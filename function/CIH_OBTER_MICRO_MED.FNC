create or replace
function cih_obter_micro_med(	cd_microorganismo_p	number,
				cd_medicamento_p	number,
				nr_ficha_ocorrencia_p	number,
				dt_inicial_p		date,
				dt_final_p		date,
				ie_resultado_cultura_p	Number,
				nr_seq_cultura_p	Number default 0)
 		    	return varchar2 is
			
ds_retorno_w 	varchar2(255);

begin

select	count(*)
into	ds_retorno_w
from  	cih_ficha_ocorrencia f,	
	cih_cultura_medic b,
	cih_cultura a
where	a.dt_coleta between trunc(dt_inicial_p) and fim_dia(dt_final_p)
and	a.cd_microorganismo	= cd_microorganismo_p
and	(f.nr_ficha_ocorrencia	= nr_ficha_ocorrencia_p or nr_ficha_ocorrencia_p = 0)
and	b.cd_medicamento	= cd_medicamento_p
and	a.nr_seq_cultura	= nr_seq_cultura_p
and	a.nr_ficha_ocorrencia	= f.nr_ficha_ocorrencia
and  	a.nr_ficha_ocorrencia	= b.nr_ficha_ocorrencia
and   	a.nr_seq_cultura	= b.nr_seq_cultura
and	b.ie_resultado_cultura	= decode(ie_resultado_cultura_p,0,b.ie_resultado_cultura,ie_resultado_cultura_p);

return	ds_retorno_w;

end cih_obter_micro_med;
/