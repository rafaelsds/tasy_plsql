create or replace
function obter_medico_executor(nr_atendimento_p		number)
 		    	return varchar2 is
		
nm_medico_w		varchar2(255) := null;	
ds_retorno_w		varchar2(255);
cd_medico_agenda_w	number(10);
		
Cursor C01 is
	select	obter_nome_medico(a.cd_medico_exec,'N')
	from	prescr_procedimento a,
		prescr_medica b
	where	b.nr_atendimento = nr_atendimento_p
	and	a.nr_prescricao  = b.nr_prescricao
	and	a.cd_medico_exec is not null;

begin

open C01;
loop
fetch C01 into	
	nm_medico_w;
exit when C01%notfound;
	begin
	
	ds_retorno_w  := nm_medico_w;
	
	end;
end loop;
close C01;

if	(nm_medico_w is null) then

	select	nvl(max(cd_medico_exec),0)
	into	cd_medico_agenda_w
	from 	agenda_paciente
	where	nr_atendimento  = nr_atendimento_p
	and	cd_agenda   = 2 ;
	
	if   (cd_medico_agenda_w > 0 ) then
		
		select obter_nome_medico(cd_medico_agenda_w, 'N')
		into	nm_medico_w
		from 	dual;
		
	end if;


end if;

ds_retorno_w := nm_medico_w;

return	ds_retorno_w;

end obter_medico_executor;
/