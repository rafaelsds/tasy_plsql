create or replace
function rp_vigencia_lista_espera(cd_pessoa_fisica_p	varchar2,
				  nm_usuario_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);			
dt_inclusao_w	varchar2(255);
qt_dias_w	number(10);

begin
obter_param_usuario(9091,3, obter_perfil_ativo, nm_usuario_p, 0, qt_dias_w);
ds_retorno_w := 'S';
if (cd_pessoa_fisica_p is not null) and (cd_pessoa_fisica_p > 0 ) then
	dt_inclusao_w := rp_obter_dados_lista_espera(cd_pessoa_fisica_p,'DTI');
	if (dt_inclusao_w is not null) and (qt_dias_w > 0) then
		if (to_date(dt_inclusao_w,'dd/mm/yyyy') < (trunc(sysdate) - qt_dias_w)) then
				ds_retorno_w := 'N';
			end if;
	end if;
end if;

return	ds_retorno_w;

end rp_vigencia_lista_espera;
/