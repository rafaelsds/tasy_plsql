CREATE OR REPLACE FUNCTION OBTER_DT_AGENDAMENTO_PACIENTE(nr_sequencia_p         NUMBER)
  RETURN DATE IS
 
  dt_agendamento_w DATE;
  
BEGIN

  IF (NVL(nr_sequencia_p,0) > 0) THEN
    SELECT MAX(dt_agendamento)
    INTO dt_agendamento_w
    FROM agenda_paciente
    WHERE nr_sequencia = nr_sequencia_p;
  END IF;

RETURN dt_agendamento_w;

END OBTER_DT_AGENDAMENTO_PACIENTE;
/