create or replace FUNCTION cpoe_obter_inicio_interv_horas (
    cd_intervalo_p      VARCHAR2,
    dt_inicio_base_p    DATE,
    dt_inicio_prescr_p  DATE
) RETURN DATE IS

    ie_interv_24_w     intervalo_prescricao.ie_24_h%TYPE;
    qt_operacao_w      intervalo_prescricao.qt_operacao%TYPE;
    qt_horas_interv_w  NUMBER(18, 6);
    dt_inicio_w        DATE := dt_inicio_base_p;
    sql_w              VARCHAR2(200);
BEGIN

    IF (
        cd_intervalo_p IS NOT NULL
        AND dt_inicio_base_p IS NOT NULL
    ) THEN
        SELECT
            nvl(MAX(ie_24_h), 'N'),
            MAX(qt_operacao)
        INTO
            ie_interv_24_w,
            qt_operacao_w
        FROM
            intervalo_prescricao
        WHERE
                cd_intervalo = cd_intervalo_p
            AND nvl(ie_operacao, 'X') = 'H';

        qt_horas_interv_w := obter_ocorrencia_intervalo(cd_intervalo_p, 24, 'H');

        
        BEGIN
            sql_w := 'CALL obter_cpoe_dt_ini_int_hrs_md(:1, :2, :3, :4, :5) INTO :dt_inicio_w';
            EXECUTE IMMEDIATE sql_w
                USING IN ie_interv_24_w, IN dt_inicio_base_p, IN qt_horas_interv_w, IN qt_operacao_w, IN dt_inicio_prescr_p, OUT dt_inicio_w;
        EXCEPTION
            WHEN OTHERS THEN
                dt_inicio_w := NULL;
        END;

    END IF;

    RETURN dt_inicio_w;
EXCEPTION
    WHEN OTHERS THEN
        RETURN dt_inicio_base_p;
END cpoe_obter_inicio_interv_horas;
/
