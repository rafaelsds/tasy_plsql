create or replace
function Obter_desc_resultados_prm( nr_sequencia_p 	number,
				ie_opcao_p		varchar)
 		    	return varchar2 is
/*
�rea de origem	'A'
Tipo de problema	'T'
Origem Ocorr�ncia	'O'
Fator		'F'
Tipo de evento	'E'
*/			
ds_retorno_w	varchar2(255);
	
begin

If	(ie_opcao_p = 'A') then

	select  substr(ds_area,1,255)
	into 	ds_retorno_w
	from   	prm_area_origem
	where  	ie_situacao = 'A'
	and    	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'T') then

	select  substr(ds_problema,1,255)
	into 	ds_retorno_w
	from	prm_tipo_problema
	where	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'E') then

	select  substr(ds_tipo_evento,1,255)
	into 	ds_retorno_w
	from	qua_tipo_evento
	where	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'F') then

	select  substr(ds_fator,1,255)
	into 	ds_retorno_w
	from	prm_fator_contribuinte
	where	nr_sequencia = nr_sequencia_p;
	
elsif 	(ie_opcao_p = 'O') then

	select  substr(ds_origem,1,255)
	into 	ds_retorno_w
	from	prm_origem_ocorrencia
	where	nr_sequencia = nr_sequencia_p;	

end if;
return	ds_retorno_w;

end Obter_desc_resultados_prm;
/
