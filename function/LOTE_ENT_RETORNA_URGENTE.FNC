create or replace
function lote_ent_retorna_urgente (nr_seq_ficha_p		number,
								nr_seq_exame_p			number)
								return varchar2 is

ds_retorno_w		varchar2(1);
nr_seq_prescr_w		number(10);
nr_prescricao_w		number(10);
ie_tipo_valor_w		varchar2(10);
				
begin

select 	max(nr_prescricao)
into	nr_prescricao_w
from	lote_ent_sec_ficha
where	nr_sequencia = nr_seq_ficha_p;

select	max(a.nr_sequencia)
into	nr_seq_prescr_w
from	prescr_procedimento a,
		lote_ent_sec_ficha b
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_prescricao = nr_prescricao_w
and		a.nr_seq_exame = nr_seq_exame_p;

if 	(nr_prescricao_w is not null) and
	(nr_seq_exame_p is not null) and
	(nr_seq_prescr_w is not null) then

	SELECT	max(nvl(a.ie_urgencia,'N'))
	into	ds_retorno_w
	FROM	prescr_procedimento a
	WHERE	a.nr_sequencia = nr_seq_prescr_w
	AND		a.nr_seq_exame 		= nr_seq_exame_p
	AND 	a.nr_prescricao 	= nr_prescricao_w;

end if;
	
return	ds_retorno_w;

end lote_ent_retorna_urgente;
/