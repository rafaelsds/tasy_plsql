create or replace
function PLS_OBTER_SE_DOSSIE_ITEM
		(cd_perfil_p	number,
		 nr_seq_item_p	number) return varchar2 is

ie_retorno_w		varchar2(20);
cont_w			number(10,0);

begin

select	count(*)
into	cont_w
from	pls_dossie_item_perfil
where	cd_perfil	= cd_perfil_p
and	nr_seq_item	= nr_seq_item_p;

ie_retorno_w	:= 'N';
if	(cont_w > 0) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end PLS_OBTER_SE_DOSSIE_ITEM;
/
