create or replace
function pls_obter_se_municipio_em_uf
			(	cd_municipio_ibge_p	Varchar2,
				sg_estado_p		Varchar2)
				return Varchar2 is

ie_retorno_w		Varchar2(1);
sg_estado_w		sus_municipio.ds_unidade_federacao%type;
			
begin

ie_retorno_w	:= 'N';

if	(cd_municipio_ibge_p	is not null) and (sg_estado_p	is not null) then
	sg_estado_w	:= obter_uf_ibge(cd_municipio_ibge_p);
	
	if	(sg_estado_w	= sg_estado_p) then
		ie_retorno_w	:= 'S';
	end if;
end if;

return	ie_retorno_w;

end pls_obter_se_municipio_em_uf;
/