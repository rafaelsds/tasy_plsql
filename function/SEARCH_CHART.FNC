create or replace
function search_chart(	nr_cirurgia_p		number,
								nr_seq_pepo_p		number,
								code_group_p		varchar)	
			return get_chart_table pipelined is type cursor_ref is ref cursor;
			
	
get_values_r		get_chart_row := get_chart_row(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

ie_union_w 				number(10);
name_w					varchar2(255);
comercial_name_w			varchar2(255);
ie_medic_nao_inform_w			varchar2(15);
description_w				varchar2(255);
color_w					varchar2(15);
style_w 				varchar2(30);
shape_w 				varchar2(30);
qt_size_w 				number(5);
fill_color_w 				varchar2(15);
shaded_w 				varchar2(15);
fixed_scale_w 				number(2);
init_date_w 				date;
end_date_w 				date;
code_chart_w 				number(10);
seq_apresentacao_w 			number(10);
nr_cirurgia_w 				number(10);
nr_seq_pepo_w 				number(10);
nm_usuario_w 				varchar2(15);
nm_tabela_w				varchar2(50);
nm_atributo_w 				varchar2(50);
ie_sinal_vital_w 			varchar2(15);
nr_seq_superior_w 			number(10);
end_of_administration_w 		varchar2(15);
cd_material_w 				number(6);
cd_unid_medida_adm_w 			varchar2(30);
ie_modo_adm_w 				varchar2(15);
nr_seq_agente_w 			number(10);
ds_dosagem_w 				varchar2(15);
ie_tipo_w					varchar2(15);
ie_configuravel_w		pepo_config_chart_html.ie_configuravel%type;
nr_sequencia_w			pepo_config_chart_html.nr_sequencia%type;
ie_tipo_dosagem_w		agente_anestesico.ie_tipo_dosagem%type;
qt_total_dosage_w		number(15, 4);
ie_forma_farmaceutica_w		agente_anestesico.ie_forma_farmaceutica%type;
ds_concatenado_w		varchar2(255);

cursor	vital_signs is
	select	distinct /* Necessary because of Blood pressure fields that will repeat the same vital sign */
				1 ie_union, 
				substr(obter_desc_expressao(e.cd_exp_grafico,null), 0, 255) name,
				substr((select max(ds_unid_med_sv) from pepo_sv r where r.ie_sinal_vital = c.ie_sinal_vital), 0, 255) description,
				substr(nvl(e.ds_cor_grafico_html,'#E5BAFF'), 0, 15) color,	
				substr(nvl(e.ie_estilo_grafico_html,'points'), 0, 30) style,
				substr(e.ie_desenho_grafico, 0, 30) shape,
				e.qt_tamanho_grafico qt_size,
				substr(decode(e.ie_transparente,'S','transparent',nvl(e.ds_cor_grafico_html,'#E5BAFF')), 0, 15) fill_color, /* White */
				obter_data_grafico_pepo('I',d.nr_cirurgia) init_date,
				obter_data_grafico_pepo('F',d.nr_cirurgia) end_date,
				d.nr_sequencia code_chart,
				d.nr_seq_apresentacao seq_apresentacao,
				d.nr_cirurgia nr_cirurgia,
				d.nr_seq_pepo nr_seq_pepo,
				substr(d.nm_usuario, 0, 15) nm_usuario,
				substr(c.ie_sinal_vital, 0, 15),
				e.nr_sequencia,
				e.ie_configuravel
	from		pepo_config_chart_html e,
				pepo_item_grafico d,
				pepo_sinal_vital c
	where		c.ie_sinal_vital		= e.ie_sinal_vital
	and			d.nr_seq_sinal_vital 	= c.nr_sequencia
	and			d.ie_grafico 			= 'S'
	and			nvl(c.ie_situacao,'A') 	= 'A'
	and			c.ie_sinal_vital NOT IN ('QTSEGMENTOST','QTNITROSO')
	and			(((nvl(nr_cirurgia_p,0) > 0) and (d.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_seq_pepo_p,0) > 0) and (d.nr_seq_pepo = nr_seq_pepo_p)));

cursor	pepo_config_cliente is
	select	substr(nvl(ds_cor_grafico_html,'#E5BAFF'), 0, 15) color,	
		substr(nvl(ie_estilo_grafico_html,'points'), 0, 30) style,
		substr(ie_desenho_grafico, 0, 30) shape
	from	pepo_config_chart_html_cli
	where	nr_seq_pepo_config		= nr_sequencia_w
	order by nr_sequencia;

	
cursor	anesthesic_agents is
	/* Anesthesic Agents - Medications - Hidroeletric Teraphy  */
	select 	distinct
				2 ie_union, 
				substr(c.ds_agente ,0,255) name,
				substr(obter_desc_material(cd_material),0,255) comercial_name,
				substr(c.ie_medic_nao_inform, 0, 15),
				--substr(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,''), 0, 255) description,
				substr(get_type_chart_surgery_2(d.nr_sequencia,d.qt_dose,d.qt_dose_ataque,d.qt_halog_ins,d.qt_velocidade_inf,d.ie_aplic_bolus,b.ie_tipo_dosagem,b.cd_unid_medida_adm,d.ie_modo_registro,b.ie_modo_adm,''),1,255) description,
				substr(nvl(c.ds_cor_grafico,'#008C23'), 0, 15) color,	
				--substr(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'style'), 0, 30) style,
				substr(get_type_chart_surgery_2(d.nr_sequencia,d.qt_dose,d.qt_dose_ataque,d.qt_halog_ins,d.qt_velocidade_inf,d.ie_aplic_bolus,b.ie_tipo_dosagem,b.cd_unid_medida_adm,d.ie_modo_registro,b.ie_modo_adm,'style'),1,30) style,
				substr('circle', 0, 30) shape,
				--to_number(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'qt_size')) qt_size,
				to_number(get_type_chart_surgery_2(d.nr_sequencia,d.qt_dose,d.qt_dose_ataque,d.qt_halog_ins,d.qt_velocidade_inf,d.ie_aplic_bolus,b.ie_tipo_dosagem,b.cd_unid_medida_adm,d.ie_modo_registro,b.ie_modo_adm,'qt_size')) qt_size,
				substr(nvl(c.ds_cor_grafico,'#008C23'), 0, 15) fill_color, 
				substr('true', 0, 15) shaded,
				0 fixed_scale,
				obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
				obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,	  
				a.nr_sequencia CODE_CHART, 		
				decode(nvl(pkg_i18n.get_user_locale, 'pt_BR'),'en_AU',nvl(nvl(c.nr_seq_apresent_graf,c.nr_seq_apresentacao),a.nr_seq_apresentacao),a.nr_seq_apresentacao) seq_apresentacao,
				a.nr_cirurgia nr_cirurgia,
				a.nr_seq_pepo nr_seq_pepo,
				substr(a.nm_usuario, 0, 15) nm_usuario,
				substr('CIRURGIA_AGENTE_ANESTESICO', 0, 50) nm_tabela,
				--substr('DS_AGENTE' || '_' || get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'style'), 0, 50) nm_atributo,
				substr('DS_AGENTE' || '_' || get_type_chart_surgery_2(d.nr_sequencia,d.qt_dose,d.qt_dose_ataque,d.qt_halog_ins,d.qt_velocidade_inf,d.ie_aplic_bolus,b.ie_tipo_dosagem,b.cd_unid_medida_adm,d.ie_modo_registro,b.ie_modo_adm,'style'), 0, 50) nm_atributo,
				b.nr_sequencia nr_seq_superior,
				substr(obter_se_fim_administracao(b.nr_sequencia), 0, 15) end_of_administration,
				b.cd_material,
				substr(b.cd_unid_medida_adm, 0, 30),
				substr(obter_modo_medicamento_cirur(b.nr_sequencia), 0, 15) ie_modo_adm,
				c.nr_sequencia nr_seq_agente,
				substr(b.ie_tipo_dosagem, 0, 15) ds_dosagem,
				b.ie_tipo,
				b.ie_tipo_dosagem,
				c.ie_forma_farmaceutica
	from 		pepo_item_grafico a, 
				cirurgia_agente_anest_ocor d,
				cirurgia_agente_anestesico b, 
				agente_anestesico c
	where 	a.nr_seq_cirur_agente = b.nr_sequencia
	and 		b.nr_seq_agente = c.nr_sequencia
	and 		b.nr_sequencia = d.nr_seq_cirur_agente(+)
	and		b.ie_tipo in (1,2,3)
	and		a.ie_grafico = 'S' 
	and 		nvl(b.ie_situacao,'A') = 'A'
	and		nvl(d.ie_situacao,'A') = 'A'
	and		(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)));
	
cursor	blood_components is
	select	distinct
				3 ie_union,
				substr(c.ds_derivado, 0, 255) name,
				substr(obter_volume_hemocomponente(NR_SEQ_DERIVADO,'D'),1,255) description,
				substr('#A81923', 0, 15) color,	
				substr('line', 0, 30) style,
				2 qt_size,
				substr('#FFF', 0, 15) fill_color, /* White */
				substr('true', 0, 15) shaded,
				0 fixed_scale,
				obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
				obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,
				a.nr_sequencia CODE_CHART, 
				a.nr_seq_apresentacao seq_apresentacao,
				b.nr_cirurgia nr_cirurgia,
				b.nr_seq_pepo nr_seq_pepo,
				substr(a.nm_usuario, 0, 15) nm_usuario,
				substr('CIRURGIA_AGENTE_ANESTESICO', 0, 50) nm_tabela,
				substr('DS_AGENTE_HEMO', 0, 50) nm_atributo,
				b.nr_sequencia nr_seq_superior,
				substr(obter_se_fim_administracao(b.nr_sequencia), 0, 15) end_of_administration,
				b.cd_material,
				substr(b.cd_unid_medida_adm, 0, 30),
				substr('H', 0, 15) ie_modo_adm
	from 		pepo_item_grafico a, 
				cirurgia_agente_anestesico b, 
				san_derivado c 
	where 	a.nr_seq_cirur_agente = b.nr_sequencia
	and 		c.nr_sequencia = b.nr_seq_derivado(+)
	and 		nvl(b.ie_situacao,'A') = 'A'
	and		a.ie_grafico = 'S'
	and		(((nvl(nr_cirurgia_p,0) > 0) and (b.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_seq_pepo_p,0) > 0) and (b.nr_seq_pepo = nr_seq_pepo_p)));
	
/* Hidro balance - Wins and losses */
cursor	hidro_balance is
	select 	distinct
				4 ie_union,
				substr(c.ds_tipo, 0, 255) name,
				substr('ml', 0, 255) description,
				substr('#3064FF', 0, 15) color,	
				substr('points', 0, 30) style,
				10 qt_size,
				substr(decode(obter_tipo_perda_ganho(b.nr_seq_tipo,'C'),'G','#3064FF','P','transparent'), 0, 15) fill_color, /* White */
				substr('true', 0, 15) shaded,
				0 fixed_scale,
				obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
				obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,
				a.nr_sequencia CODE_CHART, 
				nvl(a.nr_seq_apresentacao,0) seq_apresentacao,
				b.nr_cirurgia nr_cirurgia,
				b.nr_seq_pepo nr_seq_pepo,
				substr(a.nm_usuario, 0, 15) nm_usuario,
				substr('ATENDIMENTO_PERDA_GANHO', 0, 50) nm_tabela,
				substr('DS_TIPO', 0, 50) nm_atributo,
				b.nr_seq_tipo nr_seq_superior
	from 		pepo_item_grafico a, 
				tipo_perda_ganho c,
				atendimento_perda_ganho b
	where 	b.nr_seq_tipo = c.nr_sequencia
	and 		b.nr_sequencia = a.nr_seq_atend_pg(+)
	and		c.ie_exibir_grafico = 'S'
	and 		nvl(b.ie_situacao,'A') = 'A'
	and		nvl(a.ie_grafico,'S') = 'S'
	and		(((nvl(nr_cirurgia_p,0) > 0) and (b.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_seq_pepo_p,0) > 0) and (b.nr_seq_pepo = nr_seq_pepo_p)))
	union all
	/* Hidro balance - Hidro balance */
	select 	distinct
				5 ie_union,
				substr(obter_desc_expressao(284223,null), 0, 255) name,
				substr('ml', 0, 255) description,
				substr('#3064FF', 0, 15) color,	
				substr('line', 0, 30) style,
				3 qt_size,
				null fill_color, /* White */
				substr('true', 0, 15) shaded,
				0 fixed_scale,
				obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
				obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,
				a.nr_sequencia CODE_CHART, 
				nvl(a.nr_seq_apresentacao,999) seq_apresentacao,
				b.nr_cirurgia nr_cirurgia,
				b.nr_seq_pepo nr_seq_pepo,
				substr(a.nm_usuario, 0, 15) nm_usuario,
				substr('ATENDIMENTO_PERDA_GANHO', 0, 50) nm_tabela,
				substr('BALANCO', 0, 50) nm_atributo,
				null nr_seq_superior
	from 		pepo_item_grafico a, 
				tipo_perda_ganho c,
				atendimento_perda_ganho b
	where 	b.nr_seq_tipo = c.nr_sequencia
	and 		b.nr_sequencia = a.nr_seq_atend_pg(+)
	and		c.ie_exibir_grafico = 'S'
	and 		nvl(b.ie_situacao,'A') = 'A'
	and		nvl(a.ie_grafico,'S') = 'S'
	and		(((nvl(nr_cirurgia_p,0) > 0) and (b.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_seq_pepo_p,0) > 0) and (b.nr_seq_pepo = nr_seq_pepo_p)));
	
cursor	events_graph is
	select	91 ie_union,
				substr(obter_desc_expressao(295156,null), 0, 255) name,
				substr(obter_desc_expressao(295156,null), 0, 255) description,
				substr('#8EC200', 0, 15) color,	
				substr('line', 0, 30) style,
				substr('#FFF', 0, 15) fill_color,
				10 fixed_scale,
				obter_data_etapa_evento(nr_cirurgia_p,nr_seq_pepo_p,1) init_date,
				obter_data_etapa_evento(nr_cirurgia_p,nr_seq_pepo_p,6) end_date,
				-1 CODE_CHART, 
				1 seq_apresentacao,
				nr_cirurgia_p nr_cirurgia,
				nr_seq_pepo_p nr_seq_pepo,
				substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
				substr('DS_PACIENTE', 0, 50) nm_atributo
	from		cirurgia a
	where		(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)))
	/* Events graph - Anesthesia */
	union
	select	92 ie_union,
				substr(obter_desc_expressao(283468,null), 0, 255) name,
				substr(obter_desc_expressao(283468,null), 0, 255) description,
				substr('#FACD52', 0, 15) color,	
				substr('line', 0, 30) style,
				substr('#FFF', 0, 15) fill_color, /* White */
				10 fixed_scale,
				obter_data_etapa_evento(nr_cirurgia_p,nr_seq_pepo_p,2) init_date,
				obter_data_etapa_evento(nr_cirurgia_p,nr_seq_pepo_p,5) end_date,
				-2 CODE_CHART, 
				2 seq_apresentacao,
				nr_cirurgia_p nr_cirurgia,
				nr_seq_pepo_p nr_seq_pepo,
				substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
				substr('DS_ANESTESIA', 0, 50) nm_atributo
	from		cirurgia a
	where		(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)))
	union
	/* Events graph - Procedure */
	select	93 ie_union,
				--substr(obter_desc_expressao(720319,null), 0, 255) name,
				--substr(obter_desc_expressao(720319,null), 0, 255) description,
				substr(obter_exame_agenda(cd_procedimento_princ, ie_origem_proced, nr_seq_proc_interno),1,40) name,
				substr(obter_exame_agenda(cd_procedimento_princ, ie_origem_proced, nr_seq_proc_interno),1,40) description,
				substr('#F7727B', 0, 15) color,	
				substr('line', 0, 30) style,
				substr('#FFF', 0, 15) fill_color, /* White */
				10 fixed_scale,
				obter_data_etapa_evento(nr_cirurgia,nr_seq_pepo,3) init_date,
				obter_data_etapa_evento(nr_cirurgia,nr_seq_pepo,4) end_date,
				-3 CODE_CHART, 
				3 seq_apresentacao,
				a.nr_cirurgia,
				a.nr_seq_pepo,
				substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
				substr('DS_PROCEDIMENTO', 0, 50) nm_atributo
	from		cirurgia a
	where		(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)))
	union
	--Fanep
	select 	91 ie_union,
				substr(obter_desc_expressao(295156,null), 0, 255) name,
				substr(obter_desc_expressao(295156,null), 0, 255) description,
				substr('#8EC200', 0, 15) color,	
				substr('line', 0, 30) style,
				substr('#FFF', 0, 15) fill_color, /* White */
				10 fixed_scale,
				obter_data_etapa_evento(null,nr_sequencia,1) init_date,
				obter_data_etapa_evento(null,nr_sequencia,6) end_date,
				-1 CODE_CHART, 
				1 seq_apresentacao,
				null nr_cirurgia,
				a.nr_sequencia nr_seq_pepo,
				substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
				substr('DS_PACIENTE', 0, 50) nm_atributo
	from		pepo_cirurgia a
	where		not exists (select 1 from cirurgia b where b.nr_seq_pepo = a.nr_sequencia)
	and		a.nr_sequencia = nr_seq_pepo_p
	/* Events graph - Anesthesia */
	union
	--Fanep
	select 	92 ie_union,
				substr(obter_desc_expressao(283468,null), 0, 255) name,
				substr(obter_desc_expressao(283468,null), 0, 255) description,
				substr('#FACD52', 0, 15) color,	
				substr('line', 0, 30) style,
				substr('#FFF', 0, 15) fill_color, /* White */
				10 fixed_scale,
				obter_data_etapa_evento(null,nr_sequencia,2) init_date,
				obter_data_etapa_evento(null,nr_sequencia,5) end_date,
				-2 CODE_CHART, 
				2 seq_apresentacao,
				null nr_cirurgia,
				a.nr_sequencia nr_seq_pepo,
				substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
				substr('DS_ANESTESIA', 0, 50) nm_atributo
	from		pepo_cirurgia a
	where		not exists (select 1 from cirurgia b where b.nr_seq_pepo = a.nr_sequencia)
	and		a.nr_sequencia = nr_seq_pepo_p
	union
	/* Events graph - Procedure */
	--Fanep
	select 	93 ie_union,
				substr(obter_desc_expressao(720319,null), 0, 255) name,
				substr(obter_desc_expressao(720319,null), 0, 255) description,
				substr('#F7727B', 0, 15) color,	
				substr('line', 0, 30) style,
				substr('#FFF', 0, 15) fill_color, /* White */
				10 fixed_scale,
				obter_data_etapa_evento(null,nr_sequencia,3) init_date,
				obter_data_etapa_evento(null,nr_sequencia,4) end_date,
				-3 CODE_CHART, 
				3 seq_apresentacao,
				null nr_cirurgia,
				nr_sequencia nr_seq_pepo,
				substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
				substr('DS_PROCEDIMENTO', 0, 50) nm_atributo
	from		pepo_cirurgia a
	where		not exists (select 1 from cirurgia b where b.nr_seq_pepo = a.nr_sequencia)
	and		a.nr_sequencia = nr_seq_pepo_p;

	cursor	current_therapies is
	/* Anesthesic Agents - Medications - Hidroeletric Teraphy  */
	select 	distinct
				2 ie_union, 
				substr(decode(pkg_i18n.get_user_locale,'en_AU',decode(get_total_dose(b.nr_sequencia,b.ie_tipo_dosagem) ,0,'',' - ' || obter_desc_expressao(288275)|| get_total_dose(b.nr_sequencia,b.ie_tipo_dosagem)  || '(ml)'),''),0,255) name,
                nvl(substr(b.ds_solucao,0,255), substr(obter_desc_material(cd_material) || decode(pkg_i18n.get_user_locale,'en_AU',decode(get_total_dose(b.nr_sequencia,b.ie_tipo_dosagem) ,0,'',' - ' || obter_desc_expressao(288275)|| get_total_dose(b.nr_sequencia,b.ie_tipo_dosagem)  || '(ml)'),''),0,255)) comercial_name,
				--substr(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,''), 0, 255) description,
				substr(get_type_chart_surgery_2(d.nr_sequencia,d.qt_dose,d.qt_dose_ataque,d.qt_halog_ins,d.qt_velocidade_inf,d.ie_aplic_bolus,b.ie_tipo_dosagem,b.cd_unid_medida_adm,d.ie_modo_registro,b.ie_modo_adm,''),1,255) description,
				substr('#008C23', 0, 15) color,	
				--substr(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'style'), 0, 30) style,
				substr(get_type_chart_surgery_2(d.nr_sequencia,d.qt_dose,d.qt_dose_ataque,d.qt_halog_ins,d.qt_velocidade_inf,d.ie_aplic_bolus,b.ie_tipo_dosagem,b.cd_unid_medida_adm,d.ie_modo_registro,b.ie_modo_adm,'style'),1,30) style,
				substr('circle', 0, 30) shape,
				--to_number(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'qt_size')) qt_size,
				to_number(get_type_chart_surgery_2(d.nr_sequencia,d.qt_dose,d.qt_dose_ataque,d.qt_halog_ins,d.qt_velocidade_inf,d.ie_aplic_bolus,b.ie_tipo_dosagem,b.cd_unid_medida_adm,d.ie_modo_registro,b.ie_modo_adm,'qt_size')) qt_size,
				substr('#008C23', 0, 15) fill_color, 
				substr('true', 0, 15) shaded,
				0 fixed_scale,
				obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
				obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,	  
				a.nr_sequencia CODE_CHART, 		
				decode(nvl(pkg_i18n.get_user_locale, 'pt_BR'),'en_AU',a.nr_seq_apresentacao) seq_apresentacao,
				a.nr_cirurgia nr_cirurgia,
				a.nr_seq_pepo nr_seq_pepo,
				substr(a.nm_usuario, 0, 15) nm_usuario,
				substr('CIRURGIA_AGENTE_ANESTESICO', 0, 50) nm_tabela,
				--substr('DS_AGENTE' || '_' || get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'style'), 0, 50) nm_atributo,
				substr('DS_AGENTE' || '_' || get_type_chart_surgery_2(d.nr_sequencia,d.qt_dose,d.qt_dose_ataque,d.qt_halog_ins,d.qt_velocidade_inf,d.ie_aplic_bolus,b.ie_tipo_dosagem,b.cd_unid_medida_adm,d.ie_modo_registro,b.ie_modo_adm,'style'), 0, 50) nm_atributo,
				b.nr_sequencia nr_seq_superior,
				substr(obter_se_fim_administracao(b.nr_sequencia), 0, 15) end_of_administration,
				b.cd_material,
				substr(b.cd_unid_medida_adm, 0, 30),
				substr(obter_modo_medicamento_cirur(b.nr_sequencia), 0, 15) ie_modo_adm,
				substr(b.ie_tipo_dosagem, 0, 15) ds_dosagem,
				b.ie_tipo
	from 		pepo_item_grafico a, 
				cirurgia_agente_anest_ocor d,
				cirurgia_agente_anestesico b
	where 	a.nr_seq_cirur_agente = b.nr_sequencia
	and 		b.nr_sequencia = d.nr_seq_cirur_agente(+)
	and		b.ie_tipo in (2,3)
	and		a.ie_grafico = 'S' 
	and 		nvl(b.ie_situacao,'A') = 'A'
	and		nvl(d.ie_situacao,'A') = 'A'
	and     b.ie_terapia_atual = 'S'
	and		((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p));
	
begin

if	(code_group_p = 'SV') then
	open vital_signs;
	loop
	fetch vital_signs into	
		ie_union_w,
		name_w,
		description_w,
		color_w,
		style_w,
		shape_w,
		qt_size_w,
		fill_color_w,
		init_date_w,
		end_date_w,
		code_chart_w,
		seq_apresentacao_w,
		nr_cirurgia_w,
		nr_seq_pepo_w,
		nm_usuario_w,
		ie_sinal_vital_w,
		nr_sequencia_w,
		ie_configuravel_w;
	exit when vital_signs%notfound;
		begin
		if (description_w is null) then
         case	ie_sinal_vital_w
            when	'QTRV' then
               description_w := obter_desc_expressao(935055,'dyna.seg.cm-5');
            when	'TXRV' then
               description_w := obter_desc_expressao(935055,'dyna.seg.cm-5');   
            when	'PAM' then
               description_w := obter_desc_expressao(792358,'mmHg');   
            when	'ICA' then
               description_w := obter_desc_expressao(935052,'l/min/m2');      
            when	'QTFREQVENT' then
               description_w := obter_desc_expressao(935060,'ciclos/min');  
            when	'PAI' then
               description_w := obter_desc_expressao(792358,'mmHg');
            when	'PANI' then
               description_w := obter_desc_expressao(792358,'mmHg');			   
            else   
               description_w := null;
         end case;   
      end if;         
               
			if (ie_configuravel_w = 'S') then
				for r_pepo_config_cliente in pepo_config_cliente loop
					begin
						color_w	:= r_pepo_config_cliente.color;
						style_w	:= r_pepo_config_cliente.style;
						shape_w	:= r_pepo_config_cliente.shape;
					end;
				end loop;
			end if;

		get_values_r :=  get_chart_row(code_group_p,					--code_group
												 ie_union_w,			--ie_union
												 name_w,				--name						
												 null,					--comercial_name			
												 null,					--ie_medic_nao_inform	
												 description_w,			--description				
												 color_w,				--color						
												 style_w,				--style 					
												 shape_w,				--shape 					
												 qt_size_w,				--qt_size 					
												 fill_color_w,			--fill_color 				
												 null,					--shaded 					
												 null,					--fixed_scale 			
												 init_date_w,			--init_date 				
												 end_date_w,			--end_date 				
												 code_chart_w,			--CODE_CHART 				
												 seq_apresentacao_w,	--seq_apresentacao 		
												 nr_cirurgia_w,			--nr_cirurgia 			
												 nr_seq_pepo_w,			--nr_seq_pepo 			
												 nm_usuario_w,			--nm_usuario 				
												 null,					--nm_tabela				
												 null,					--nm_atributo 			
												 ie_sinal_vital_w,		--ie_sinal_vital 		
												 null,					--nr_seq_superior 		
												 null,					--end_of_administration 
												 null,					--cd_material 			
												 null,					--cd_unid_medida_adm 	
												 null,					--ie_modo_adm 			
												 null,					--nr_seq_agente 			
												 null,					--ds_dosagem 				
												 null);					--ie_tipo
		pipe row(get_values_r);										 
		end;
	end loop;
	close vital_signs;
elsif (code_group_p = 'AG') then
	open anesthesic_agents;
	loop
	fetch anesthesic_agents into	
		ie_union_w, 
		name_w,
		comercial_name_w,
		ie_medic_nao_inform_w,
		description_w,
		color_w,
		style_w,
		shape_w,
		qt_size_w,
		fill_color_w, 
		shaded_w,
		fixed_scale_w,
		init_date_w,
		end_date_w,
		CODE_CHART_w,
		seq_apresentacao_w,
		nr_cirurgia_w,
		nr_seq_pepo_w,
		nm_usuario_w,
		nm_tabela_w,
		nm_atributo_w,
		nr_seq_superior_w,
		end_of_administration_w,
		cd_material_w,
		cd_unid_medida_adm_w,
		ie_modo_adm_w,
		nr_seq_agente_w,
		ds_dosagem_w,
		ie_tipo_w,
		ie_tipo_dosagem_w,
		ie_forma_farmaceutica_w;
	exit when anesthesic_agents%notfound;
		begin
		
		if	(pkg_i18n.get_user_locale()  = 'en_AU') and
			(ie_forma_farmaceutica_w <> 'G') then
			qt_total_dosage_w	:= get_total_dose(nr_seq_superior_w,ie_tipo_dosagem_w);
			
			if	(qt_total_dosage_w	> 0) then
				ds_concatenado_w	:=  ' - '||obter_desc_expressao(288275) || ' '||cpoe_dose_formatada(qt_total_dosage_w) || '('||obter_desc_expressao(703346)||')';
				name_w			:=  substr(name_w ||ds_concatenado_w,1,255);
				comercial_name_w	:= substr(comercial_name_w ||ds_concatenado_w,1,255);
			end if;
			
		end if;
		
		get_values_r := 	get_chart_row(code_group_p,						--code_group
												 ie_union_w,				--ie_union
												 name_w,					--name						
												 comercial_name_w,			--comercial_name			
												 ie_medic_nao_inform_w,		--ie_medic_nao_inform	
												 description_w,				--description				
												 color_w,					--color						
												 style_w,					--style 					
												 shape_w,					--shape 					
												 qt_size_w,					--qt_size 					
												 fill_color_w,				--fill_color 				
												 shaded_w,					--shaded 					
												 fixed_scale_w,				--fixed_scale 
												 init_date_w,				--init_date 				
												 end_date_w,				--end_date 				
												 code_chart_w,				--CODE_CHART 				
												 seq_apresentacao_w,		--seq_apresentacao 		
												 nr_cirurgia_w,				--nr_cirurgia 			
												 nr_seq_pepo_w,				--nr_seq_pepo 			
												 nm_usuario_w,				--nm_usuario 				
												 nm_tabela_w,				--nm_tabela				
												 nm_atributo_w,				--nm_atributo 			
												 null,						--ie_sinal_vital 		
												 nr_seq_superior_w,			--nr_seq_superior 		
												 end_of_administration_w,	--end_of_administration 
												 cd_material_w,				--cd_material 			
												 cd_unid_medida_adm_w,		--cd_unid_medida_adm 	
												 ie_modo_adm_w,   			--ie_modo_adm 			
												 nr_seq_agente_w,			--nr_seq_agente 			
												 ds_dosagem_w,				--ds_dosagem 				
												 ie_tipo_w);				--ie_tipo
		pipe row(get_values_r);	
		
		end;
	end loop;
	close anesthesic_agents;
elsif (code_group_p = 'SH') then
	open blood_components;
	loop
	fetch blood_components into	
		ie_union_w,
		name_w,
		description_w,
		color_w,
		style_w,
		qt_size_w,
		fill_color_w,
		shaded_w,
		fixed_scale_w,
		init_date_w,
		end_date_w,
		CODE_CHART_w,
		seq_apresentacao_w,
		nr_cirurgia_w,
		nr_seq_pepo_w,
		nm_usuario_w,
		nm_tabela_w,
		nm_atributo_w,
		nr_seq_superior_w,
		end_of_administration_w,
		cd_material_w,
		cd_unid_medida_adm_w,
		ie_modo_adm_w;
	exit when blood_components%notfound;
		begin
		get_values_r :=  get_chart_row(code_group_p,						--code_group
												 ie_union_w,				--ie_union
												 name_w,					--name						
												 null,						--comercial_name			
												 null,						--ie_medic_nao_inform	
												 description_w,				--description				
												 color_w,					--color						
												 style_w,					--style 					
												 null,						--shape 					
												 qt_size_w,					--qt_size 					
												 fill_color_w,				--fill_color 				
												 shaded_w,					--shaded 					
												 fixed_scale_w,				--fixed_scale 
												 init_date_w,				--init_date 				
												 end_date_w,				--end_date 				
												 code_chart_w,				--CODE_CHART 				
												 seq_apresentacao_w,		--seq_apresentacao 		
												 nr_cirurgia_w,				--nr_cirurgia 			
												 nr_seq_pepo_w,				--nr_seq_pepo 			
												 nm_usuario_w,				--nm_usuario 				
												 nm_tabela_w,				--nm_tabela				
												 nm_atributo_w,				--nm_atributo 			
												 null,						--ie_sinal_vital 		
												 nr_seq_superior_w,			--nr_seq_superior 		
												 end_of_administration_w,	--end_of_administration 
												 cd_material_w,				--cd_material 			
												 cd_unid_medida_adm_w,		--cd_unid_medida_adm 	
												 ie_modo_adm_w,   			--ie_modo_adm 			
												 null,						--nr_seq_agente 			
												 null,						--ds_dosagem 				
												 null);						--ie_tipo
		pipe row(get_values_r);	
		
		end;
	end loop;
	close blood_components;
elsif (code_group_p = 'BH') then
	open hidro_balance;
	loop
	fetch hidro_balance into	
		ie_union_w,
		name_w,
		description_w,
		color_w,
		style_w,
		qt_size_w,
		fill_color_w,
		shaded_w,
		fixed_scale_w,
		init_date_w,
		end_date_w,
		CODE_CHART_w, 
		seq_apresentacao_w,
		nr_cirurgia_w,
		nr_seq_pepo_w,
		nm_usuario_w,
		nm_tabela_w,
		nm_atributo_w,
		nr_seq_superior_w;
	exit when hidro_balance%notfound;
		begin
		get_values_r :=  get_chart_row(code_group_p,					--code_group
												 ie_union_w,			--ie_union
												 name_w,				--name						
												 null,					--comercial_name			
												 null,					--ie_medic_nao_inform	
												 description_w,			--description				
												 color_w,				--color						
												 style_w,				--style 					
												 null,					--shape 					
												 qt_size_w,				--qt_size 					
												 fill_color_w,			--fill_color 				
												 shaded_w,				--shaded 					
												 fixed_scale_w,			--fixed_scale 
												 init_date_w,			--init_date 				
												 end_date_w,			--end_date 				
												 code_chart_w,			--CODE_CHART 				
												 seq_apresentacao_w,	--seq_apresentacao 		
												 nr_cirurgia_w,			--nr_cirurgia 			
												 nr_seq_pepo_w,			--nr_seq_pepo 			
												 nm_usuario_w,			--nm_usuario 				
												 nm_tabela_w,			--nm_tabela				
												 nm_atributo_w,			--nm_atributo 			
												 null,					--ie_sinal_vital 		
												 nr_seq_superior_w,		--nr_seq_superior 		
												 null,					--end_of_administration 
												 null,					--cd_material 			
												 null,					--cd_unid_medida_adm 	
												 null,   				--ie_modo_adm 			
												 null,					--nr_seq_agente 			
												 null,					--ds_dosagem 				
												 null);					--ie_tipo
		pipe row(get_values_r);	
		
		end;
	end loop;
	close hidro_balance;
elsif (code_group_p = 'NAV') then
	open events_graph;
	loop
	fetch events_graph into	
		ie_union_w,
		name_w,
		description_w,
		color_w,
		style_w,
		fill_color_w,
		fixed_scale_w,
		init_date_w,
		end_date_w,
		CODE_CHART_w, 
		seq_apresentacao_w,
		nr_cirurgia_w,
		nr_seq_pepo_w,
		nm_tabela_w,
		nm_atributo_w;
	exit when events_graph%notfound;
		begin
		get_values_r :=  get_chart_row(code_group_p,					--code_group
												 ie_union_w,			--ie_union
												 name_w,				--name						
												 null,					--comercial_name			
												 null,					--ie_medic_nao_inform	
												 description_w,			--description				
												 color_w,				--color						
												 style_w,				--style 					
												 null,					--shape 					
												 null,					--qt_size 					
												 fill_color_w,			--fill_color 				
												 null,					--shaded 					
												 fixed_scale_w,			--fixed_scale 
												 init_date_w,			--init_date 				
												 end_date_w,			--end_date 				
												 code_chart_w,			--CODE_CHART 				
												 seq_apresentacao_w,	--seq_apresentacao 		
												 nr_cirurgia_w,			--nr_cirurgia 			
												 nr_seq_pepo_w,			--nr_seq_pepo 			
												 null,					--nm_usuario 				
												 nm_tabela_w,			--nm_tabela				
												 nm_atributo_w,			--nm_atributo 			
												 null,					--ie_sinal_vital 		
												 null,					--nr_seq_superior 		
												 null,					--end_of_administration 
												 null,					--cd_material 			
												 null,					--cd_unid_medida_adm 	
												 null,   				--ie_modo_adm 			
												 null,					--nr_seq_agente 			
												 null,					--ds_dosagem 				
												 null);					--ie_tipo
		pipe row(get_values_r);	
		
		end;
	end loop;
	close events_graph;
elsif (code_group_p = 'TA') then
	open current_therapies;
	loop
	fetch current_therapies into	
		ie_union_w, 
		name_w,
		comercial_name_w,
		description_w,
		color_w,
		style_w,
		shape_w,
		qt_size_w,
		fill_color_w, 
		shaded_w,
		fixed_scale_w,
		init_date_w,
		end_date_w,
		CODE_CHART_w,
		seq_apresentacao_w,
		nr_cirurgia_w,
		nr_seq_pepo_w,
		nm_usuario_w,
		nm_tabela_w,
		nm_atributo_w,
		nr_seq_superior_w,
		end_of_administration_w,
		cd_material_w,
		cd_unid_medida_adm_w,
		ie_modo_adm_w,
		ds_dosagem_w,
		ie_tipo_w;
	exit when current_therapies%notfound;
		begin
		get_values_r := 	get_chart_row(code_group_p,						--code_group
												 ie_union_w,				--ie_union
												 name_w,					--name						
												 comercial_name_w,			--comercial_name			
												 ie_medic_nao_inform_w,		--ie_medic_nao_inform	
												 description_w,				--description				
												 color_w,					--color						
												 style_w,					--style 					
												 shape_w,					--shape 					
												 qt_size_w,					--qt_size 					
												 fill_color_w,				--fill_color 				
												 shaded_w,					--shaded 					
												 fixed_scale_w,				--fixed_scale 
												 init_date_w,				--init_date 				
												 end_date_w,				--end_date 				
												 code_chart_w,				--CODE_CHART 				
												 seq_apresentacao_w,		--seq_apresentacao 		
												 nr_cirurgia_w,				--nr_cirurgia 			
												 null,				--nr_seq_pepo 			
												 nm_usuario_w,				--nm_usuario 				
												 nm_tabela_w,				--nm_tabela				
												 nm_atributo_w,				--nm_atributo 			
												 null,						--ie_sinal_vital 		
												 nr_seq_superior_w,			--nr_seq_superior 		
												 end_of_administration_w,	--end_of_administration 
												 cd_material_w,				--cd_material 			
												 cd_unid_medida_adm_w,		--cd_unid_medida_adm 	
												 ie_modo_adm_w,   			--ie_modo_adm 			
												 null,			--nr_seq_agente 			
												 ds_dosagem_w,				--ds_dosagem 				
												 ie_tipo_w);				--ie_tipo
		pipe row(get_values_r);	
		
		end;
	end loop;
	close current_therapies;
end if;

end search_chart;
/