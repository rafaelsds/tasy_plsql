create or replace function 
obter_nfse_conta_protocolo (nr_seq_protocolo_p		number,
			nr_interno_conta_p		number)
			return varchar2 is	

nr_nota_w		varchar2(255);
ds_nota_w		varchar2(4000) := ' ';

cursor c01 is
select	a.nr_nfe_imp
from	nota_fiscal a, 
		operacao_nota o
where	(nr_seq_protocolo_p 	<> 0) 
and	(a.nr_seq_protocolo 	= nr_seq_protocolo_p)
and	a.ie_situacao = '1'
and	(nr_interno_conta_p	= a.nr_interno_conta or
	nr_interno_conta_p	= 0)
and a.cd_operacao_nf = o.cd_operacao_nf
and	o.ie_servico = 'S'
and	o.ie_nf_eletronica = 'S'
and	o.ie_operacao_fiscal = 'S'
union
select	a.nr_nfe_imp
from	nota_fiscal a,
		operacao_nota o
where	nr_interno_conta_p	= a.nr_interno_conta
and	a.ie_situacao = '1'
and	nr_interno_conta_p	<> 0
and a.cd_operacao_nf = o.cd_operacao_nf
and	o.ie_servico = 'S'
and	o.ie_nf_eletronica = 'S'
and	o.ie_operacao_fiscal = 'S'
union
select	a.nr_nfe_imp
from	nota_fiscal a,
		protocolo_convenio b,
		operacao_nota o
where	a.nr_seq_lote_prot	= b.nr_seq_lote_protocolo
and	nr_seq_protocolo_p	<> 0
and	a.ie_situacao = '1'
and	b.nr_seq_protocolo	= nr_seq_protocolo_p
and a.cd_operacao_nf = o.cd_operacao_nf
and	o.ie_servico = 'S'
and	o.ie_nf_eletronica = 'S'
and	o.ie_operacao_fiscal = 'S'
order by 1;

begin
open c01;
loop
fetch c01 into nr_nota_w;
	exit when c01%notfound;
	if	(length(ds_nota_w) < 3600) then
		ds_nota_w := ds_nota_w || nr_nota_w ||', ';
	end if;
end loop;
close c01;

return trim(substr(ds_nota_w,1,length(ds_nota_w) - 2));

end obter_nfse_conta_protocolo;
/
