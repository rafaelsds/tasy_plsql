CREATE OR REPLACE FUNCTION ageint_obter_plano_lib_categ(cd_convenio_p        NUMBER,
                                                        cd_categoria_p       VARCHAR2,
                                                        cd_plano_p           VARCHAR2,
                                                        cd_estabelecimento_p NUMBER,
                                                        dt_atendimento_p     DATE)
  RETURN VARCHAR2 IS

  ie_liberado_w VARCHAR2(01) := 'S';
  ie_parametro  VARCHAR2(01) := 'S';

BEGIN
  SELECT nvl(MAX(pa.ie_ativa_categ_plano), 'N')
    INTO ie_parametro
    FROM parametro_agenda pa
   WHERE pa.cd_estabelecimento = cd_estabelecimento_p;

  --  obter_param_usuario(869, 187, obter_perfil_ativo, obter_usuario_ativo, cd_estabelecimento_p, ie_parametro);

  IF ie_parametro = 'S' AND cd_plano_p is not NULL THEN
    ie_liberado_w := obter_plano_lib_categoria(cd_convenio_p,
                                               cd_categoria_p,
                                               cd_plano_p,
                                               cd_estabelecimento_p,
                                               dt_atendimento_p);
  END IF;
  RETURN ie_liberado_w;

END ageint_obter_plano_lib_categ;
/