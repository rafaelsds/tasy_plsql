create or replace
function pls_obter_se_grupo_uf
			(	nr_seq_grupo_p		Number,
				uf_p			Varchar2)
				return varchar2 is

ie_retorno_w		Varchar2(1)	:= 'N';
qt_regra_w		pls_integer;

begin

select	COUNT(1)
into	qt_regra_w
from	pls_preco_uf		a,
	pls_preco_grupo_uf	b
where	a.nr_seq_grupo		= b.nr_sequencia
and	a.sg_uf			= uf_p
and	b.nr_sequencia		= nr_seq_grupo_p
and	b.ie_situacao		= 'A';

if	(qt_regra_w > 0) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end pls_obter_se_grupo_uf;
/
