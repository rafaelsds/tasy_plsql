create or replace 
function obter_estab_cot_compra(	nr_cot_compra_p	number)
			return number is

cd_estabelecimento_w		number(5);

begin

select	nvl(max(cd_estabelecimento),null)
into	cd_estabelecimento_w
from	cot_compra
where	nr_cot_compra = nr_cot_compra_p;

return cd_estabelecimento_w;

end;
/