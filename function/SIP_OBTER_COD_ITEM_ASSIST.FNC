create or replace
function sip_obter_cod_item_assist
			(	nr_sequencia_p		Number)
				return Varchar2 is
				
cd_item_w			Varchar2(10);

begin

select	max(cd_item)
into	cd_item_w
from	sip_item_assistencial
where	nr_sequencia	= nr_sequencia_p;

return	cd_item_w;

end sip_obter_cod_item_assist;
/