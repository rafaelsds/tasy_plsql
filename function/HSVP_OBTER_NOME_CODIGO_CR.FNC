create or replace
function hsvp_obter_nome_codigo_cr(cd_pessoa_fisica_p varchar2,
			Ie_opcao_p  varchar2)
			return varchar2 is

nm_pessoa_fisica_w	varchar2(60);
cd_cr_w			varchar2(20);
ie_sexo_w		varchar2(1);
cd_medico_w		varchar2(10);
nr_seq_conselho_w	number(10);
sg_conselho_w		varchar(60);
ds_retorno_w		varchar2(100);

begin

select	nvl(max(a.nm_pessoa_fisica),''),
	nvl(max(nvl(b.nr_crm, a.ds_codigo_prof)),''),
	nvl(max(a.ie_sexo),'M'),
	nvl(max(b.cd_pessoa_fisica),null),
	nvl(max(a.NR_SEQ_CONSELHO),0)
into	nm_pessoa_fisica_w,
	cd_cr_w,
	ie_sexo_w,
	cd_medico_w,
	nr_seq_conselho_w
from	medico b,
	pessoa_fisica a
where	a.cd_pessoa_fisica = b.cd_pessoa_fisica(+)
and	a.cd_pessoa_fisica = cd_pessoa_fisica_p;

if	(nm_pessoa_fisica_w is not null) then
	if	(ie_opcao_p = 'NC') then
		ds_retorno_w := nm_pessoa_fisica_w || ' (';
	elsif (ie_sexo_w = 'M') then
		ds_retorno_w := '';
	else
		ds_retorno_w := '';
	end if;
	ds_retorno_w := ds_retorno_w || nm_pessoa_fisica_w || ' (';
	if	(cd_medico_w is not null) and (ie_opcao_p <> 'NCI') then
		ds_retorno_w := ds_retorno_w || 'CRM  ' || cd_cr_w ||')';
	elsif	(ie_opcao_p = 'NC') or (ie_opcao_p = 'NCI') then
		begin
		if	(nr_seq_conselho_w > 0) then
			select	a.sg_conselho
			into 	sg_conselho_w
			from 	conselho_profissional a
			where	a.nr_sequencia = nr_seq_conselho_w;
		end if;
		ds_retorno_w := ds_retorno_w || sg_conselho_w || cd_cr_w ||')';
		end;
	else
		ds_retorno_w := ds_retorno_w || 'CRF  ' || cd_cr_w ||')';
	end if;
else
	ds_retorno_w := '';
end if;

return ds_retorno_w;

end hsvp_obter_nome_codigo_cr;
/