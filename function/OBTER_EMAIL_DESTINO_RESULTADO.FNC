create or replace
function obter_email_destino_resultado(	nr_prescricao_p		number,
					cd_estabelecimento_p	number)
 		    	return varchar2 is

ds_email_w	varchar2(255);			
			
begin

select 	max(ds_email)
into	ds_email_w
from   	pessoa_juridica_estab a,
	convenio b,
	prescr_medica c
where  	a.cd_cgc = b.cd_cgc
and    	b.cd_convenio = obter_convenio_atendimento(obter_atendimento_prescr(c.nr_prescricao)) 
and    	c.nr_prescricao = nr_prescricao_p
and    	a.cd_estabelecimento = cd_estabelecimento_p;

return	ds_email_w;

end obter_email_destino_resultado;
/