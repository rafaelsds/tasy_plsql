create or replace
Function oft_obter_formulario	(	nr_seq_tipo_consulta_p		number)
											return	number is
	

nr_sequencia_w			oft_formulario_regra.nr_sequencia%type;
nr_sequencia_ww		oft_formulario_regra.nr_sequencia%type;
cd_perfil_w				perfil.cd_perfil%type	:= wheb_usuario_pck.get_cd_perfil;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type	:= wheb_usuario_pck.get_cd_estabelecimento;
	
cursor	c01 is							
	select 	b.nr_sequencia
	from   	oft_formulario a,
				oft_formulario_regra b
	where  	a.nr_sequencia 		   								   = b.nr_seq_formulario
	and	   nvl(a.nr_seq_tipo_consulta,nr_seq_tipo_consulta_p) = nr_seq_tipo_consulta_p
	and	   nvl(b.cd_perfil,cd_perfil_w) 							   = cd_perfil_w
	and	   nvl(b.cd_estabelecimento,cd_estabelecimento_w) 	   = cd_estabelecimento_w
	and		b.dt_liberacao is not null
	and		nvl(a.ie_situacao,'A') = 'A'
	and		nvl(b.ie_situacao,'A') = 'A'
	order by nvl(b.cd_perfil,0),
				nvl(b.cd_estabelecimento,0);

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	nr_sequencia_ww := nr_sequencia_w;
	end;
end loop;
close C01;
	
return nr_sequencia_ww;

end oft_obter_formulario;
/
