 Create or Replace
Function obter_protocolos_retorno(
			cd_estabelecimento_p	Number,
			cd_convenio_p		Number,
			nr_seq_retorno_p	Number)
			return Varchar2 is

nr_seq_protocolo_w	number(10);
nr_seq_prot_adic_w	convenio_retorno.nr_seq_prot_adic%type;
ds_protocolos_w		Varchar2(2000) := '';

cursor c01 is
	select	nvl(nr_seq_protocolo,0),
		nvl(nr_seq_prot_adic,'0')
	from	convenio_retorno
	where cd_estabelecimento = cd_estabelecimento_p
	  and cd_convenio	 = cd_convenio_p
	  and (nr_seq_protocolo is not null or nr_seq_prot_adic is not null)
	  and decode(nvl(nr_seq_retorno_p,0),0,nr_sequencia,nr_seq_retorno_p) = nr_sequencia
	order by nr_sequencia desc;

BEGIN

open c01;
loop
	fetch c01 into	nr_seq_protocolo_w,
			nr_seq_prot_adic_w;
	exit when c01%notfound;

	ds_protocolos_w := substr(ds_protocolos_w || ';' || nr_seq_protocolo_w || ';' || nr_seq_prot_adic_w,1,1990);

end loop;
close c01;

ds_protocolos_w := ds_protocolos_w || ';';

RETURN ds_protocolos_w;

END obter_protocolos_retorno;
/