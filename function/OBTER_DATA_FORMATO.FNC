create or replace function obter_data_formato(  dt_horario_p 			in date,
												cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
												nm_usuario_p 			in usuario.nm_usuario%type,
												cd_formato_p 			in locale_formats.id_mask%type default 'timestamp')
					return date is

/*
exemplo formatos:
  longDate
  mediumDate
  short
  shortDate
  shortDayMonth
  shortThreeLetterMonth
  shortTime
  timestamp

  Os formatos podem ser encontrados na tabela locale_formats
*/

ds_mascara_w varchar2(100 char);

begin

if (dt_horario_p is not null
	and cd_estabelecimento_p is not null
	and nm_usuario_p is not null) then
	
	ds_mascara_w := pkg_date_formaters.localize_mask(nvl(cd_formato_p, 'timestamp'), pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_p, nm_usuario_p));

	return to_date(to_char(dt_horario_p, ds_mascara_w), ds_mascara_w);
	
end if;

return null;

end obter_data_formato;
/