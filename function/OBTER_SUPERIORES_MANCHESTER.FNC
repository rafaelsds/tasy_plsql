create or replace 
function obter_superiores_manchester(nr_seq_fluxo_p number, ie_leitura_obrigatoria_p varchar2) return varchar2 is

ds_superiores_w varchar2(255) := '';
ds_leitura_obr_w varchar2(255);

cursor c01 is
	select	ie_classificacao_risco||'/'||nr_seq_item||':'||nvl(b.qt_valor_maximo,0)||','||nvl(b.qt_valor_minimo,0) valores,
			case when b.ie_leitura_obrigatoria in ('PS','PD') then b.ie_leitura_obrigatoria||'=' else '' end identificador
	from    manchester_fluxograma_item a,
			manchester_item b
	where   nr_seq_fluxo = nr_seq_fluxo_p
	and     nvl(a.ie_versao,'1') = '2'
	and     nvl(b.ie_versao,'1') = '2'
	and     b.nr_sequencia = a.nr_seq_item
	and     obter_se_contido_char(b.ie_leitura_obrigatoria,ds_leitura_obr_w) = 'S' 
	and     b.ie_leitura_obrigatoria is not null
	order by nr_seq_apresentacao;

begin

if (nr_seq_fluxo_p > 0) and (ie_leitura_obrigatoria_p <> ' ') then

	if (ie_leitura_obrigatoria_p = 'PS') then
		ds_leitura_obr_w := 'PS,PD'; 
	else
		ds_leitura_obr_w := ie_leitura_obrigatoria_p;
	end if;

	for itens in c01 loop
		ds_superiores_w := ds_superiores_w || itens.identificador || itens.valores || ';';
	end loop;

end if;

return ds_superiores_w;

end obter_superiores_manchester;

/