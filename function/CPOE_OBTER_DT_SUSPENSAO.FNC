create or replace
function cpoe_obter_dt_suspensao (nr_sequencia_p number,
								  ie_tipo_item_p varchar2)
								 return date is

dt_suspensao_w		date;				

begin

if	(nvl(nr_sequencia_p,0) > 0) then
	
	if (ie_tipo_item_p = 'N') then
		
		select	max(dt_suspensao)
		into	dt_suspensao_w
		from	cpoe_dieta
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;
	
	elsif (ie_tipo_item_p = 'M') then
	
		select	max(dt_suspensao)
		into	dt_suspensao_w
		from	cpoe_material
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;	
	
	elsif (ie_tipo_item_p = 'P') then

		select	max(dt_suspensao)
		into	dt_suspensao_w
		from	cpoe_procedimento
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;	
		
	elsif (ie_tipo_item_p = 'G') then
	
		select	max(dt_suspensao)
		into	dt_suspensao_w
		from	cpoe_gasoterapia
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;		
	
	elsif (ie_tipo_item_p = 'R') then

		select	max(dt_suspensao)
		into	dt_suspensao_w
		from	cpoe_recomendacao
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;		
	
	elsif (ie_tipo_item_p = 'D') then
	
		select	max(dt_suspensao)
		into	dt_suspensao_w
		from	cpoe_dialise
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;			
	
	elsif (ie_tipo_item_p = 'H') then
	
		select	max(dt_suspensao)
		into	dt_suspensao_w
		from	cpoe_hemoterapia
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;
	
	elsif (ie_tipo_item_p = 'AP') then
	
		select	max(dt_suspensao)
		into	dt_suspensao_w
		from	cpoe_anatomia_patologica
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;	
	
	end if;
end if;


return	nvl(dt_suspensao_w,null);

end cpoe_obter_dt_suspensao;
/