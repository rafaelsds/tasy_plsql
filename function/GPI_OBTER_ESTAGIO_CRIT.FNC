create or replace 
FUNCTION GPI_Obter_estagio_Crit(nr_sequencia_p number)
					return number is
					
qt_w		number(10);

BEGIN
					
	select  count(*)
	Into	qt_w	
	from	gpi_estagio a,
		gpi_lib_estagio b
	where   a.nr_sequencia = b.nr_Seq_Estagio
	and	a.nr_sequencia = nr_sequencia_p;	
		

return qt_w;

END GPI_Obter_estagio_Crit;
/