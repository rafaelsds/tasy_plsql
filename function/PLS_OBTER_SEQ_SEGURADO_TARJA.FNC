create or replace
function pls_obter_seq_segurado_tarja
			(	cd_usuario_plano_p	Varchar2,
				cd_estabelecimento_p	Number,
				ie_restringe_estab_p	Varchar2)
 		    	return varchar2 is

nr_seq_segurado_w	number(10);
ie_tipo_repasse_w	varchar2(1);
cd_estabelecimento_w	number(4);
qt_regra_eventual_w	number(10);

begin

if	(ie_restringe_estab_p = 'S') then
	cd_estabelecimento_w	:= cd_estabelecimento_p;
else
	cd_estabelecimento_w	:= null;
end if;

/* 1� Busca pelo NR_CARTAO_INTERCAMBIO - Somente retornar se tiver ativo, sen�o deve ser criado usu�rio eventual */
select	max(b.nr_sequencia)
into	nr_seq_segurado_w
from	pls_segurado		b,
	pls_segurado_carteira	a
where	a.nr_seq_segurado	= b.nr_sequencia
and	b.ie_situacao_atend	= 'A'
and	((a.cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento_w is null))
and	a.nr_cartao_intercambio	= cd_usuario_plano_p;

/* 2� Busca pelo CD_USUARIO_PLANO - Se existir algum cadastrado com a carteira, tem que retornar */
if	(nr_seq_segurado_w is null) then
	select	max(b.nr_sequencia)
	into	nr_seq_segurado_w
	from	pls_segurado		b,
		pls_segurado_carteira	a
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	((a.cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento_w is null))
	and	a.cd_usuario_plano	= cd_usuario_plano_p;
	
	if	(nr_seq_segurado_w is not null) then
		select	max(ie_tipo_repasse)
		into	ie_tipo_repasse_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
		
		if	(ie_tipo_repasse_w is not null) then
			select	count(1)
			into	qt_regra_eventual_w
			from	pls_regra_usuario_repasse
			where	ie_tipo_repasse	= ie_tipo_repasse_w
			and	ie_gerar_usuario_eventual = 'S'
			and	ie_situacao = 'A';
			
			if	(qt_regra_eventual_w > 0) then
				nr_seq_segurado_w	:= null;
			end if;
		end if;
	end if;
end if;

return	nr_seq_segurado_w;

end pls_obter_seq_segurado_tarja;
/