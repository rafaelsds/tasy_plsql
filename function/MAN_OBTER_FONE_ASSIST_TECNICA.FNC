create or replace
function man_obter_fone_assist_tecnica	(cd_cnpj_p varchar2)
				return varchar2 is

ds_telefone_w	varchar2(80);

begin

if	(cd_cnpj_p is not null) then

	select	max(ds_telefone)
	into	ds_telefone_w
	from	assistencia_tecnica
	where	cd_cgc = cd_cnpj_p;

end if;

return ds_telefone_w;

end man_obter_fone_assist_tecnica;
/