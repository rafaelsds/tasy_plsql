create or replace function obter_desc_man_tipo_executor(nr_sequencia_p number)
    return varchar2 is

ds_retorno_w  varchar2(80);

begin
  if (nr_sequencia_p is not null) then
    begin
      select substr(obter_desc_expressao(cd_exp_executor, ds_executor),1,80)
      into ds_retorno_w
      from man_tipo_executor
      where nr_sequencia = nr_sequencia_p;
    end;
  end if;

return ds_retorno_w;
end obter_desc_man_tipo_executor;
/