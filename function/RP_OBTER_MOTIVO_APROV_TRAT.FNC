create or replace
function rp_obter_motivo_aprov_trat(nr_sequencia_p	number,
				    ds_motivo_p		varchar2 default '')
 		    	return varchar2 is
ds_retorno_w	varchar2(255);			

begin

if (nr_sequencia_p <> 0) and (nr_sequencia_p is not null) then
	select 	substr(ds_motivo,1,255)
	into	ds_retorno_w
	from	rp_motivo_aprov_trat
	where	nr_sequencia = nr_sequencia_p;
else
	select 	max(nr_sequencia)
	into	ds_retorno_w
	from	rp_motivo_aprov_trat
	where	upper(ds_motivo) = upper(ds_motivo_p);
end if;


return	ds_retorno_w;

end rp_obter_motivo_aprov_trat;
/