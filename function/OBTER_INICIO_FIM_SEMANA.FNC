create or replace
function obter_inicio_fim_semana(	dt_referencia_p		date,
					ie_opcao_p		varchar2)
				return varchar2 is

ds_retorno_w		varchar2(100);
ie_dia_sem_w		varchar2(10);
/* I - Para obter a data de inicio da semana */
/* F - Para obter a data de fim da semana    */

begin
ie_dia_sem_w	:= obter_cod_dia_semana(to_date(dt_referencia_p));
/*if	(philips_param_pck.get_nr_seq_idioma = 2) then -- Espanhol  - M�xico(MX)
	if	(ie_dia_sem_w = 7) then
		ie_dia_sem_w := 1;
	else
		ie_dia_sem_w	:= ie_dia_sem_w + 1;
	end if;
end if;*/

if	(ie_opcao_p = 'I') then	
	select (to_date(dt_referencia_p) - ie_dia_sem_w+1)
	into	ds_retorno_w
	from dual;
elsif	(ie_opcao_p = 'F') then	
	select (to_date(dt_referencia_p) - ie_dia_sem_w+7)
	into	ds_retorno_w
	from dual;	
end if;

return ds_retorno_w;
	
end obter_inicio_fim_semana;
/
