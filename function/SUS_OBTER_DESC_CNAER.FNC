CREATE OR REPLACE
FUNCTION Sus_Obter_Desc_Cnaer
		(	cd_atividade_cnaer_p	Number)
			return Varchar2 is


ds_retorno_w		Varchar2(200);


BEGIN

select	nvl(max(ds_atividade_cnaer),'')
into	ds_retorno_w
from	sus_atividade_cnaer
where	cd_atividade_cnaer	= cd_atividade_cnaer_p;

return	ds_retorno_w;

END Sus_Obter_Desc_Cnaer;
/