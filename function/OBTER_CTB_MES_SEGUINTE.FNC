Create or Replace
Function obter_ctb_mes_seguinte(
				nr_seq_mes_ref_p	Number,
				qt_mes_seguinte_p	Number)
				Return Number IS

cd_empresa_w			ctb_mes_ref.cd_empresa%type;
nr_seq_retorno_w		Number(10,0);

BEGIN


if	(nr_seq_mes_ref_p is not null) and
	(qt_mes_seguinte_p is not null) then

	begin
	select	a.cd_empresa
	into	cd_empresa_w
	from	ctb_mes_ref a
	where	a.nr_sequencia	= nr_seq_mes_ref_p;
	exception when others then
		cd_empresa_w	:= obter_empresa_estab(wheb_usuario_pck.get_cd_estabelecimento);
	end;
	
	
	select	min(a.nr_sequencia)
	into	nr_seq_retorno_w
	from	ctb_mes_ref a
	where	trunc(a.dt_referencia) 	= (	select	trunc(pkg_date_utils.add_month(b.dt_referencia,qt_mes_seguinte_p, 0))
						from	ctb_mes_ref b
						where	nr_sequencia	= nr_seq_mes_ref_p)
	and	a.cd_empresa		= cd_empresa_w;
end if;

RETURN nr_seq_retorno_w;
END obter_ctb_mes_seguinte;
/