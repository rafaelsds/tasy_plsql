create or replace
function pls_obter_se_prest_rede_atend
			(	nr_seq_prestador_p	number,
				nr_seq_rede_p		number)
				return varchar2 is

ie_permite_w			varchar2(2)	:= 'N';
nr_seq_classificacao_w		number(10);
nr_seq_tipo_prestador_w		number(10);
cd_prestador_w			Varchar2(30);
ie_tipo_rede_min_ptu_w		pls_rede_atend_regra.ie_tpo_rede_ptu_prest%type;
ie_prestador_alto_custo_w	pls_prestador.ie_prestador_alto_custo%type;

Cursor C01 is
	select	ie_permite
	from	pls_prestador_rede_atend
	where	((nr_seq_prestador = nvl(nr_seq_prestador_p,nr_seq_prestador)) or (nr_seq_prestador is null))
	and	((nr_seq_classif_prestador = nvl(nr_seq_classificacao_w,nr_seq_classif_prestador)) or (nr_seq_classif_prestador is null))
	and	((nr_seq_tipo_prestador = nvl(nr_seq_tipo_prestador_w,nr_seq_tipo_prestador)) or (nr_seq_tipo_prestador is null))
	and	nr_seq_rede_atend	= nr_seq_rede_p
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate)
	order by
		nvl(nr_seq_prestador,0),
		nvl(nr_seq_classif_prestador,0),
		nvl(nr_seq_tipo_prestador,0);
		
Cursor C02 is
	select	ie_permite
	from	pls_rede_atend_regra
	where	((nr_seq_prestador = nvl(nr_seq_prestador_p,nr_seq_prestador)) or (nr_seq_prestador is null))
	and	((nr_seq_classif_prestador = nvl(nr_seq_classificacao_w,nr_seq_classif_prestador)) or (nr_seq_classif_prestador is null))
	and	((nr_seq_tipo_prestador = nvl(nr_seq_tipo_prestador_w,nr_seq_tipo_prestador)) or (nr_seq_tipo_prestador is null))
	and	((cd_prestador is null) or (cd_prestador = nvl(cd_prestador_w,'0')))
	and	((ie_alto_custo_prest is null) or (ie_alto_custo_prest	in ('A', nvl(ie_prestador_alto_custo_w, 'N'))))
	and	((ie_tpo_rede_ptu_prest is null) or (ie_tpo_rede_ptu_prest in ('N', ie_tipo_rede_min_ptu_w)))
	and	nr_seq_rede	= nr_seq_rede_p
	order by
		nvl(cd_prestador,0),
		nvl(nr_seq_prestador,0),
		nvl(nr_seq_classif_prestador,0),
		nvl(nr_seq_tipo_prestador,0),
		nvl(ie_congenere_inferior,'X'),
		nvl(cd_procedimento,0) desc,
		nvl(nr_seq_congenere,0) desc;

begin
select	max(nr_seq_classificacao),
	max(nr_seq_tipo_prestador),
	max(cd_prestador),
	decode(max(ie_tipo_rede_min_ptu), 1, 'B', 2, 'E', 3, 'M', 'N'),
	max(ie_prestador_alto_custo)
into	nr_seq_classificacao_w,
	nr_seq_tipo_prestador_w,
	cd_prestador_w,
	ie_tipo_rede_min_ptu_w,
	ie_prestador_alto_custo_w
from	pls_prestador
where	nr_sequencia	= nr_seq_prestador_p;

open C01;
loop
fetch C01 into	
	ie_permite_w;
exit when C01%notfound;
	begin
	null;
	end;
end loop;
close C01;

if	(ie_permite_w = 'N') then
	open C02;
	loop
	fetch C02 into	
		ie_permite_w;
	exit when C02%notfound;
		begin
		null;
		end;
	end loop;
	close C02;
end if;


return	ie_permite_w;

end pls_obter_se_prest_rede_atend;
/
