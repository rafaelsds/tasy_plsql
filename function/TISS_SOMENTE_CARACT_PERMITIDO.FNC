create or replace
function tiss_somente_caract_permitido(ds_conteudo_p		varchar2,
					ie_opcao_p		varchar2 )
						return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retonar apenas os caracteres permitidos
-------------------------------------------------------------------------------------------------------------------
OPS - Controle de Contesta��es
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ]  Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: IE_OPCAO_P

A	- Alfab�tico de A � Z, mai�sculas e min�sculas e brancos
N	- Num�rico de 0 � 9
AN	- Alfab�tico de A � Z mai�sculas e min�sculas e Num�rico de 0 � 9 e brancos 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
						
ds_retorno_w		varchar2(4000);
ds_conteudo_w		varchar2(4000);

begin
ds_conteudo_w := trim(ds_conteudo_p);

if	(ie_opcao_p = 'A') then
	ds_conteudo_w := elimina_acentuacao(ds_conteudo_w);
	ds_conteudo_w := replace(replace(replace(replace(ds_conteudo_w,'�','N'),'�','n'),'�','AE'),'�','ae');
	ds_retorno_w := regexp_replace(ds_conteudo_w, '[^a-zA-Z ]');

elsif	(ie_opcao_p = 'N') then
	ds_retorno_w := regexp_replace(ds_conteudo_w, '[^0-9]');

elsif	(ie_opcao_p = 'AN') then
	ds_conteudo_w := elimina_acentuacao(ds_conteudo_w);
	ds_conteudo_w := replace(replace(ds_conteudo_w,'�','N'),'�','n');
	ds_retorno_w := regexp_replace(ds_conteudo_w, '[^a-zA-Z0-9 ]');
end if;

return	ds_retorno_w;

end tiss_somente_caract_permitido;
/
