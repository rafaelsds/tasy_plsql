create or replace FUNCTION list_desc_quebr (    
    nr_prescricao_p   NUMBER
) RETURN VARCHAR2 IS

    retorno_list   VARCHAR2(4000) := '';
    CURSOR list_desc_cursor IS
        select obter_desc_procedimento(b.cd_procedimento,b.ie_origem_proced) || ' - ' || a.ds_informacao as ds_informacao
        from prescr_proced_inf_adic a,
            prescr_procedimento b
        where b.nr_prescricao = a.nr_prescricao
        and a.nr_prescricao = nr_prescricao_p
        order by a.nr_sequencia;

         list_desc_record   list_desc_cursor%rowtype;

 BEGIN
    OPEN list_desc_cursor;
    FETCH list_desc_cursor INTO list_desc_record;
    WHILE list_desc_cursor%found LOOP
        IF list_desc_record.ds_informacao is not null  THEN
           retorno_list := SUBSTR(list_desc_record.ds_informacao||chr(10)||retorno_list, 1, 4000) ;
        END IF;
        FETCH list_desc_cursor INTO list_desc_record;
    END LOOP;

    CLOSE list_desc_cursor;
    RETURN retorno_list;
END list_desc_quebr;
/
