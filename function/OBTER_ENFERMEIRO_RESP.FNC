CREATE OR REPLACE
FUNCTION Obter_enfermeiro_resp(
			nr_atendimento_p	Number,
			ie_opcao_p		varchar2)	
			RETURN VarChar2 IS

ds_Retorno_w		VarChar2(100);
cd_pessoa_fisica_w	varchar2(10);
nm_pessoa_w		varchar2(100);

BEGIN

select	a.cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atend_enfermagem_resp a
where	a.nr_sequencia	= (	select	max(b.nr_sequencia)
				from	atend_enfermagem_resp b
				where	b.nr_atendimento	= nr_atendimento_p);

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= cd_pessoa_fisica_w;
else
	ds_retorno_w	:= obter_nome_pf(cd_pessoa_fisica_w); 
end if;

RETURN ds_retorno_w;

END Obter_enfermeiro_resp;
/
