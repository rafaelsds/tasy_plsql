create or replace
function cus_obter_capac_centro(	cd_estabelecimento_p	number,
				nr_seq_tabela_p		number,
				cd_centro_controle_p	number,
				nr_sequencia_nivel_p	number)
				return number is

nr_seq_tabela_capac_w		tabela_custo.nr_sequencia%type;
cd_tipo_tabela_custo_w		tipo_tabela_custo.cd_tipo_tabela_custo%type;
qt_disponibilidade_w		capac_centro_controle.qt_disponibilidade%type;
cd_empresa_w				empresa.cd_empresa%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

begin

select	nvl(cd_estabelecimento,0),
	cd_empresa,
	cd_tipo_tabela_custo
into	cd_estabelecimento_w,
	cd_empresa_w,
	cd_tipo_tabela_custo_w
from	tabela_custo
where	nr_sequencia	= nr_seq_tabela_p;

if	(cd_tipo_tabela_custo_w = 2) then
	nr_seq_tabela_capac_w	:= nvl(nr_seq_tabela_p,0);
elsif	(cd_tipo_tabela_custo_w = 7) then
	select	nvl(max(nr_seq_tabela_param),0)
	into	nr_seq_tabela_capac_w
	from	tabela_parametro
	where	nr_seq_tabela	 	= nr_seq_tabela_p
	and		nr_seq_parametro	= 2;
end if;
	
if	(nr_seq_tabela_capac_w <> 0) then
	begin
	select	nvl(qt_disponibilidade,0)
	into	qt_disponibilidade_w
	from	capac_centro_controle
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	nr_seq_tabela		= nr_seq_tabela_capac_w
	and	cd_centro_controle	= cd_centro_controle_p
	and	nr_sequencia_nivel	= nr_sequencia_nivel_p;
	exception when others then
		qt_disponibilidade_w	:= 0;
	end;
end if;

return	qt_disponibilidade_w;

end cus_obter_capac_centro;
/