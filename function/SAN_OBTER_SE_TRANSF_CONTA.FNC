create or replace
function san_obter_se_transf_conta(nr_seq_transfusao_p	number,
							cd_setor_atendimento_p		number,
							nr_atendimento_p			number)
 		    	return varchar is

ds_retorno_w	varchar2(1);
				
begin

select 	coalesce(max('S'), 'N')
into	ds_retorno_w
from	procedimento_paciente 
where 	nr_atendimento = nr_atendimento_p
and 	cd_setor_atendimento = cd_setor_atendimento_p
and 	ds_observacao like '%'||nr_seq_transfusao_p||'%';

return	ds_retorno_w;

end san_obter_se_transf_conta;
/