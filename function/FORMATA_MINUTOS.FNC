create or replace
function Formata_minutos(nr_minutos_p	number)
 		    	return varchar2 is

qt_dias_w	number(10);			
qt_horas_w	number(10);
qt_minutos_w	number(10);
	
ds_horario_w	varchar2(15);
	
begin
qt_horas_w := 0;
qt_dias_w  := 0;

qt_minutos_w := nr_minutos_p;
while	(qt_minutos_w >= 60) loop
	qt_horas_w   := qt_horas_w + 1;
	qt_minutos_w := qt_minutos_w - 60;
end loop;

while	(qt_horas_w >= 24) loop
	qt_dias_w  := qt_dias_w + 1;
	qt_horas_w := qt_horas_w - 24;
end loop;

if	(qt_dias_w > 0) then
	ds_horario_w := ds_horario_w || qt_dias_w || ' d ';	
end if;

if	(qt_horas_w > 0) then
	ds_horario_w := ds_horario_w || lpad(qt_horas_w, 2, '0');
	
	if	(qt_minutos_w > 0) then
		ds_horario_w := ds_horario_w || ':' || lpad(qt_minutos_w, 2, '0');
	else	
		ds_horario_w := ds_horario_w || 'h';
	end if;	
else	
	if	(qt_minutos_w > 0) then
		ds_horario_w := ds_horario_w || '00:' || lpad(qt_minutos_w, 2, '0');
	end if;
end if;

return	ds_horario_w;

end Formata_minutos;
/
