create or replace
function  pls_obter_dados_cd_prest_web
			(	cd_prestador_p		varchar2,
				ie_opcao_p		Varchar2)
 		    		return Varchar2 is
				
ds_retorno_w			Varchar2(255)	:= null;
nr_seq_prestador_w		Number(10);
cd_pessoa_fisica_w		Varchar2(10);
cd_cgc_w			Varchar2(50);

/*
 Tipos
	 NM  - Nome prestador
	SQ - Sequ�ncia
*/

begin

select	max(nr_sequencia),
	max(cd_cgc),
	max(cd_pessoa_fisica)
into	nr_seq_prestador_w,
	cd_cgc_w,
	cd_pessoa_fisica_w	
from	pls_prestador
where	cd_prestador = cd_prestador_p;

if	(ie_opcao_p = 'NM') then
	select	max(a.nm_interno)
	into	ds_retorno_w
	from	pls_prestador a
	where	nr_sequencia	= nr_seq_prestador_w;

	if	(ds_retorno_w is null) then
		if	(cd_cgc_w is not null)	then
			select	ds_razao_social
			into	ds_retorno_w
			from	pessoa_juridica
			where	cd_cgc	= cd_cgc_w;
		elsif	(cd_pessoa_fisica_w is not null)	then
			select	nm_pessoa_fisica
			into	ds_retorno_w
			from	pessoa_fisica
			where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		end if;
	end if;
elsif 	(ie_opcao_p = 'SQ') then
	ds_retorno_w := nr_seq_prestador_w;
end if;

return	ds_retorno_w;

end  pls_obter_dados_cd_prest_web;
/
