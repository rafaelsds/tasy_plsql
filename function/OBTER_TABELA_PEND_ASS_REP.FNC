create or replace
function obter_tabela_pend_ass_rep( 
						nr_sequencia_p		number,
						nr_prescricao_p		number,
						nm_usuario_p		varchar2) return varchar2 is
						
nr_seq_projeto_w		number(15);
nr_seq_dieta_w 			number(15);
nr_seq_proced_w			number(15);
nr_seq_material_w		number(15);
nr_prescricao_w			number(15);
nr_seq_interno_w		number(15);
nr_seq_recomendacao_w	number(15);
nr_seq_solucao_w		number(15);
nr_seq_nut_pac_w		number(15);
nr_seq_nut_prot_w		number(15);
nr_seq_gasoterapia_w	number(15);
nm_tabela_w				varchar2(255);
						
begin

select	max(nr_prescricao),
		nvl(max(nr_seq_dieta_rep),0),
		nvl(max(nr_seq_proced_rep),0),
		nvl(max(nr_seq_material_rep),0),
		nvl(max(nr_seq_recomendacao),0),
		nvl(max(nr_seq_solucao),0),
		nvl(max(nr_seq_nut_pac),0),
		nvl(max(nr_seq_nut_prot),0),
		nvl(max(nr_seq_gasoterapia),0)
into	nr_prescricao_w,
		nr_seq_dieta_w,
		nr_seq_proced_w,
		nr_seq_material_w,
		nr_seq_recomendacao_w,
		nr_seq_solucao_w,
		nr_seq_nut_pac_w,
		nr_seq_nut_prot_w,
		nr_seq_gasoterapia_w
from	pep_item_pendente
where	nr_sequencia = nr_sequencia_p;

if	(nvl(nr_prescricao_w,0) > 0) and 
	(nr_seq_dieta_w = 0) and
	(nr_seq_proced_w = 0) and
	(nr_seq_recomendacao_w = 0) and
	(nr_seq_solucao_w = 0) and
	(nr_seq_nut_pac_w = 0) and
	(nr_seq_nut_prot_w = 0) and
	(nr_seq_gasoterapia_w = 0) and	
	(nr_seq_material_w = 0) then
	nm_tabela_w := 'PRESCR_MEDICA';
elsif	(nr_seq_material_w > 0) and
		(nr_seq_dieta_w	= 0) and
		(nr_seq_recomendacao_w = 0) and
		(nr_seq_solucao_w = 0) and
		(nr_seq_nut_pac_w = 0) and
		(nr_seq_nut_prot_w = 0) and
		(nr_seq_gasoterapia_w = 0) and
		(nr_seq_proced_w = 0) then
	nm_tabela_w := 'PRESCR_MATERIAL';
elsif 	(nr_seq_dieta_w > 0) and
		(nr_seq_material_w = 0) and
		(nr_seq_recomendacao_w = 0) and
		(nr_seq_solucao_w = 0) and
		(nr_seq_nut_pac_w = 0) and
		(nr_seq_nut_prot_w = 0) and
		(nr_seq_gasoterapia_w = 0) and
		(nr_seq_proced_w = 0) then	
	nm_tabela_w := 'PRESCR_DIETA';
elsif 	(nr_seq_proced_w > 0) and
		(nr_seq_material_w = 0) and
		(nr_seq_recomendacao_w = 0) and
		(nr_seq_solucao_w = 0) and
		(nr_seq_nut_pac_w = 0) and
		(nr_seq_nut_prot_w = 0) and
		(nr_seq_gasoterapia_w = 0) and
		(nr_seq_dieta_w = 0) then
	nm_tabela_w := 'PRESCR_PROCEDIMENTO';
elsif 	(nr_seq_recomendacao_w > 0) and
		(nr_seq_material_w = 0) and
		(nr_seq_proced_w = 0) and
		(nr_seq_solucao_w = 0) and
		(nr_seq_nut_pac_w = 0) and
		(nr_seq_nut_prot_w = 0) and
		(nr_seq_gasoterapia_w = 0) and
		(nr_seq_dieta_w = 0) then
	nm_tabela_w := 'PRESCR_RECOMENDACAO';
elsif 	(nr_seq_solucao_w > 0) and
		(nr_seq_material_w = 0) and
		(nr_seq_proced_w = 0) and
		(nr_seq_recomendacao_w = 0) and
		(nr_seq_nut_pac_w = 0) and
		(nr_seq_nut_prot_w = 0) and
		(nr_seq_gasoterapia_w = 0) and
		(nr_seq_dieta_w = 0) then
	nm_tabela_w := 'PRESCR_SOLUCAO';
elsif 	(nr_seq_gasoterapia_w > 0) and
		(nr_seq_material_w = 0) and
		(nr_seq_proced_w = 0) and
		(nr_seq_recomendacao_w = 0) and
		(nr_seq_nut_pac_w = 0) and
		(nr_seq_nut_prot_w = 0) and
		(nr_seq_solucao_w = 0) and
		(nr_seq_dieta_w = 0) then
	nm_tabela_w := 'PRESCR_GASOTERAPIA';
elsif 	(nr_seq_nut_pac_w > 0) and
		(nr_seq_material_w = 0) and
		(nr_seq_proced_w = 0) and
		(nr_seq_recomendacao_w = 0) and
		(nr_seq_gasoterapia_w = 0) and
		(nr_seq_nut_prot_w = 0) and
		(nr_seq_solucao_w = 0) and
		(nr_seq_dieta_w = 0) then
	nm_tabela_w := 'NUT_PACIENTE';
elsif 	(nr_seq_nut_prot_w > 0) and
		(nr_seq_material_w = 0) and
		(nr_seq_proced_w = 0) and
		(nr_seq_recomendacao_w = 0) and
		(nr_seq_gasoterapia_w = 0) and
		(nr_seq_nut_pac_w = 0) and
		(nr_seq_solucao_w = 0) and
		(nr_seq_dieta_w = 0) then
	nm_tabela_w := 'NUT_PAC';
end if;

return nm_tabela_w;
	
end obter_tabela_pend_ass_rep;
/