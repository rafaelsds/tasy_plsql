create or replace 
function obter_tit_rec_controle_pes (nr_sequencia_p	number)
			return varchar2 is

ds_retorno_w		varchar2(2000) := '';
nr_titulo_w		number(10,0);

cursor c01 is
select	nr_titulo
from	titulo_receber
where	nr_seq_controle_pessoa = nr_sequencia_p;

begin

open c01;
loop
	fetch c01 into
		nr_titulo_w;
	exit when c01%notfound;

	ds_retorno_w := ds_retorno_w || nr_titulo_w || ',';

end loop;
close c01;

return substr(ds_retorno_w,1,length(ds_retorno_w)-1);

end obter_tit_rec_controle_pes;
/