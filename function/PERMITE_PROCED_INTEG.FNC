create or replace
function permite_proced_integ	( nr_seq_interno_p		number,
				  nr_seq_sistema_integ_p	number )
 		    	return varchar2 is
			
			
ds_retorno_w	varchar2(1);			

begin

select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	prescr_procedimento a,
	procedimento b,
	proc_interno_integracao c,
	prescr_medica d
where	a.cd_procedimento  = b.cd_procedimento
and	a.ie_origem_proced = b.ie_origem_proced
and	d.nr_prescricao	   = a.nr_prescricao
and	nvl(c.nr_seq_proc_interno, nvl(a.nr_seq_proc_interno,0)) = nvl(a.nr_seq_proc_interno,0)
and	nvl(c.cd_procedimento, nvl(a.cd_procedimento,0)) = nvl(a.cd_procedimento,0)
and	nvl(c.ie_origem_proced, nvl(a.ie_origem_proced,0)) = nvl(a.ie_origem_proced,0)
and	nvl(c.cd_tipo_procedimento, nvl(b.cd_tipo_procedimento,0)) = nvl(b.cd_tipo_procedimento,0)
and	nvl(c.cd_estabelecimento, nvl(d.cd_estabelecimento,0)) = nvl(d.cd_estabelecimento,0)
and	a.nr_seq_interno = nr_seq_interno_p
and	c.nr_seq_sistema_integ 	= nr_seq_sistema_integ_p;

return	ds_retorno_w;

end permite_proced_integ;
/