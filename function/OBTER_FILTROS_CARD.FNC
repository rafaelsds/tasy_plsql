create or replace
function obter_filtros_card ( nr_seq_item_p		number)
 		    	return varchar2 is
			
ds_filtros_card_w			varchar2(2000) := '';		
ie_formato_busca_w			varchar2(5);
qt_dias_busca_w				number(3);
qt_consultas_w				number(3);
ie_restringir_tipo_atend_w	varchar2(5);
ds_formato_busca_w			varchar2(255);
IE_TIPO_ITEM_w				varchar2(5);
ds_restringir_tipo_atend_w	varchar2(255);

begin

select	max(IE_FORMATO_BUSCA),
	max(QT_DIAS_BUSCA),
	max(QT_CONSULTAS),
	max(IE_RESTRINGIR_TIPO_ATEND),
		max(IE_TIPO_ITEM)
into	ie_formato_busca_w,
	qt_dias_busca_w,
	qt_consultas_w,
	ie_restringir_tipo_atend_w,
		IE_TIPO_ITEM_w
from	item_suep
where	nr_sequencia = nr_seq_item_p;


If  (ie_formato_busca_w is null) then


	Select 	obter_busca_padrao_suep(IE_TIPO_ITEM_w)
	into	ie_formato_busca_w
	from 	dual;


end if;



if (ie_formato_busca_w = 'A') then

	ds_formato_busca_w := obter_desc_expressao(283863);

elsif (ie_formato_busca_w = 'P') then

	ds_formato_busca_w := obter_desc_expressao(295829);

elsif (ie_formato_busca_w = 'C') then


	ds_formato_busca_w := obter_desc_expressao(668979);

end if;



if	(ds_formato_busca_w is not null) then
	ds_filtros_card_w := '<div>'||'<strong>'||obter_desc_expressao(791226) || ': '|| '</strong>' ||ds_formato_busca_w;
end if;

if	(qt_dias_busca_w is not null) then
	ds_filtros_card_w := ds_filtros_card_w || '<br>' ||'<strong>'||obter_desc_expressao(791299) || ': '|| '</strong> ' ||qt_dias_busca_w;
end if;

if	(qt_consultas_w is not null) then
	ds_filtros_card_w := ds_filtros_card_w || '<br>' ||'<strong>'||obter_desc_expressao(791303) || ': '|| '</strong> ' ||qt_consultas_w;
end if;

if	(ie_restringir_tipo_atend_w is not null) then

	if	(ie_restringir_tipo_atend_w = 'S') then
	
		ds_restringir_tipo_atend_w := obter_desc_expressao(327113);
		
	else
	
		ds_restringir_tipo_atend_w := obter_desc_expressao(327114);
		
	end if;
	

	ds_filtros_card_w := ds_filtros_card_w || '<br>' || '<strong>'||obter_desc_expressao(791307) || ': '|| '</strong> ' || ds_restringir_tipo_atend_w;
end if;


if	(ds_filtros_card_w = '') then
	ds_filtros_card_w := obter_desc_expressao(543333);
else
	
	ds_filtros_card_w := ds_filtros_card_w ||'</div>';

end if;

return	ds_filtros_card_w;

end obter_filtros_card ;
/
