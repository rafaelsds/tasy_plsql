create or replace
function obter_desc_motivo_desconto( cd_motivo_desconto_p number)
      return varchar2 is
	
ds_motivo_w  varchar2(200);

begin

select nvl(max(ds_motivo), '')
into ds_motivo_w
from  motivo_desconto
where nr_sequencia = cd_motivo_desconto_p;

return ds_motivo_w;

end obter_desc_motivo_desconto;
/
