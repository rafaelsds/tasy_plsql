create or replace
function pls_obter_desc_tipo_pf 
			(	cd_pessoa_fisica_p	Varchar2)
				return Varchar2 is
				
ds_retorno_w			Varchar2(255);
ie_funcionario_w		Varchar2(2);
qt_registros_w			Number(10);

begin

if	(nvl(cd_pessoa_fisica_p,'X') <> 'X') then
	
	
	select	count(1)
	into	qt_registros_w
	from	pls_cooperado
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

	if	(qt_registros_w	> 0) then
		ds_retorno_w	:= 'Cooperado';
	elsif	(qt_registros_w	= 0) then
		select	count(1)
		into 	qt_registros_w
		from	Medico
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p;


		if	(qt_registros_w = 0) then
			select	ie_funcionario,
				1
			into	ie_funcionario_w,
				qt_registros_w
			from 	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
			
			ie_funcionario_w := nvl(ie_funcionario_w,'N');
			
			if	(ie_funcionario_w = 'S') then
				ds_retorno_w	:= 'Funcionário';
			elsif	(ie_funcionario_w = 'N') then
				if	(qt_registros_w	> 0) then
					ds_retorno_w	:= 'Beneficiário';
				else
					ds_retorno_w	:= 'Outros';
				end if;
			end if;
		elsif	(qt_registros_w <> 0) then
			ds_retorno_w	:= 'Médico';
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_desc_tipo_pf;
/
