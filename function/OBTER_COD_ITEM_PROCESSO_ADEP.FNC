create or replace
function obter_cod_item_processo_adep	(nr_prescricao_p	number,
					nr_seq_material_p	number,
					nr_seq_horario_p	number)
					return number is

cd_material_w	number(6,0);

begin

if	(nr_seq_horario_p is not null) then

	select	to_number(obter_dados_horario(nr_seq_horario_p,'CI'))
	into	cd_material_w
	from	dual;

elsif	(nr_prescricao_p is not null) and
	(nr_seq_material_p is not null) then

	select	to_number(obter_dados_material_prescr(nr_prescricao_p,nr_seq_material_p,'CD_MATERIAL'))
	into	cd_material_w
	from	dual;

end if;

return cd_material_w;

end obter_cod_item_processo_adep;
/