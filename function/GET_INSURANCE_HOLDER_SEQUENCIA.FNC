create or replace 
function get_insurance_holder_sequencia(cd_pessoa_fisica_p 	number,
					ie_insurance_type_p   	number)
					return number is

nr_sequencia_w number := null;
cd_convenio_w  number := null;

begin
if	(cd_pessoa_fisica_p is not null and ie_insurance_type_p is not null) then
	begin
		select	max(cd_convenio)
		into	cd_convenio_w
		from	convenio
		where	ie_tipo_convenio 	= ie_insurance_type_p;
	
		select	nr_sequencia
		into	nr_sequencia_w
		from	(select	nr_sequencia
			from	pessoa_titular_convenio a
			where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	a.cd_convenio        = cd_convenio_w
			and	(a.dt_inicio_vigencia <= sysdate or a.dt_inicio_vigencia is null)
			order by dt_fim_vigencia desc)
		where rownum <= 1;
	exception
		when	no_data_found then
			nr_sequencia_w := null;
	end;
	return nr_sequencia_w;
end if;
return nr_sequencia_w;
end get_insurance_holder_sequencia;
/
