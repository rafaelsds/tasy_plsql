create or replace
function obter_se_oc_proj_rec(	nr_ordem_compra_p		number)
 		    	return varchar2 is

ie_oc_proj_rec_w		varchar2(1) := 'N';
qt_registros_w			number(10);
			
begin

select	count(*)
into	qt_registros_w
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_seq_proj_rec > 0;

if	(qt_registros_w > 0) then
	ie_oc_proj_rec_w := 'S';
end if;

return	ie_oc_proj_rec_w;

end obter_se_oc_proj_rec;
/