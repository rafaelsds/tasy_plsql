create or replace
function obter_valor_itens_cta_pend(	nr_seq_pend_p		number)
					return number is

vl_retorno_w		number(15,2) := 0;

begin
	select	nvl(sum(a.vl_item),0) vl_item
		into	vl_retorno_w
		from	cta_pendencia_item a
		where	a.nr_seq_pend = nr_seq_pend_p;

return	vl_retorno_w;

end obter_valor_itens_cta_pend;
/