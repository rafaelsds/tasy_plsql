create or replace
function pls_obter_desc_regra_recalculo
		(	nr_seq_regra_p		Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(255);

begin

select	max(ds_regra)
into	ds_retorno_w
from	pls_regra_lote_recalculo
where	nr_sequencia	= nr_seq_regra_p;

return	ds_retorno_w;

end pls_obter_desc_regra_recalculo;
/