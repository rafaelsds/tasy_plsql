create or replace
function pls_obter_msg_requisicao(	nr_seq_requisicao_p		number)
					return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Buscar informações da observação da requisição e das mensagens das requisições
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [ X ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(4000);
ds_mensagem_w		varchar2(255);


Cursor C01 is
	select	substr(ds_mensagem,1,255)
	from	pls_mensagem_requisicao
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

begin

select	substr(ds_observacao,1,255)
into	ds_retorno_w
from	pls_requisicao
where	nr_sequencia	= nr_seq_requisicao_p;

ds_retorno_w	:= ds_retorno_w||chr(13);

open C01;
loop
fetch C01 into	
	ds_mensagem_w;
exit when C01%notfound;
	begin	
	ds_retorno_w	:= ds_retorno_w||chr(13)||ds_mensagem_w;	
	end;
end loop;
close C01;

return	ds_retorno_w;

end pls_obter_msg_requisicao;
/