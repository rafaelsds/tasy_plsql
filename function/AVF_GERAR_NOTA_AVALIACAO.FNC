create or replace
function avf_gerar_nota_avaliacao(
				nr_sequencia_p	number)
 		    		return number is

nr_seq_pergunta_w			number(10);
nr_seq_nota_item_w		number(10);
nr_seq_tipo_nota_w		number(10);
vl_nota_maxima_w			number(9,4);
nr_seq_grupo_pergunta_w		number(10,0);
pr_peso_grupo_w			number(5,2);
qt_perguntas_w			number(10)	:= 0;
vl_media_w			number(15,4)	:= 0;
vl_media_acum_w			number(15,4)	:= 0;
vl_media_final_w			number(15,4)	:= 0;
vl_nota_w			number(9,4);

ie_pontuacao_pergunta_w		varchar2(1) 	:= 'N';
ie_apresentacao_media_w		varchar2(1)	:= '1';
cd_estabelecimento_w		number(10)	:= wheb_usuario_pck.get_cd_estabelecimento;
nm_usuario_w			varchar2(15)	:= wheb_usuario_pck.get_nm_usuario;
pr_peso_pergunta_w		avf_pergunta.pr_peso_pergunta%type;

Cursor C01 is
	select	a.nr_sequencia,
		b.nr_seq_nota_item,
		a.nr_seq_tipo_nota
	from	avf_resultado_item b,
		avf_pergunta a
	where	a.nr_sequencia	= b.nr_seq_pergunta
	and	b.nr_seq_resultado	= nr_sequencia_p
	and	b.nr_seq_pergunta is not null
	and	b.nr_seq_nota_item is not null
	and	substr(avf_obter_se_nota_se_aplica(b.nr_seq_nota_item),1,1) = 'S';

Cursor C02 is
	select	a.nr_sequencia,
		nvl(a.pr_peso_grupo,100)
	from	avf_resultado_item b,
		avf_grupo_pergunta a
	where	a.nr_sequencia	= b.nr_seq_grupo_pergunta
	and	b.nr_seq_resultado	= nr_sequencia_p;
	
Cursor C03 is
	select	a.nr_sequencia,
		b.nr_seq_nota_item,
		a.pr_peso_pergunta
	from	avf_resultado_item b,
		avf_pergunta a
	where	a.nr_sequencia		= b.nr_seq_pergunta
	and	b.nr_seq_resultado		= nr_sequencia_p
	and	a.nr_seq_grupo_pergunta	= nr_seq_grupo_pergunta_w
	and	b.nr_seq_pergunta is not null
	and	b.nr_seq_nota_item is not null
	and	substr(avf_obter_se_nota_se_aplica(b.nr_seq_nota_item),1,1) = 'S';

begin

/* Verifica se a gera��o da nota ser� pelo processo j� existente ou ir� considerar os pontos de cada pergunta  */
obter_param_usuario(6, 73, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_pontuacao_pergunta_w);
/*Forma de apresenta��o da m�dia da avaliacao*/
obter_param_usuario(6, 76, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_apresentacao_media_w);

begin
select	to_number(ie_apresentacao_media_w)
into	ie_apresentacao_media_w
from	dual;
exception
when others then
	ie_apresentacao_media_w := '1';
end;

if	(nvl(ie_pontuacao_pergunta_w, 'N') = 'N') then
	begin
	if	(ie_apresentacao_media_w in ('0','1','2')) then
		begin		
		open C01;
		loop
		fetch C01 into	
			nr_seq_pergunta_w,
			nr_seq_nota_item_w,
			nr_seq_tipo_nota_w;
		exit when C01%notfound;
			begin		
			qt_perguntas_w := qt_perguntas_w + 1;
			
			select	nvl(max(vl_tipo_nota),0)
			into	vl_nota_w
			from	avf_tipo_nota_item
			where	nr_sequencia = nr_seq_nota_item_w;
			
			select	nvl(max(vl_tipo_nota),0)
			into	vl_nota_maxima_w
			from	avf_tipo_nota_item
			where	nr_seq_tipo_nota = nr_seq_tipo_nota_w;
			
			if	(ie_apresentacao_media_w = '1') then		
				vl_media_w	:= dividir(vl_nota_w, dividir(vl_nota_maxima_w, 100));
			elsif	(ie_apresentacao_media_w = '0') then
				vl_media_w	:= dividir(dividir(vl_nota_w, dividir(vl_nota_maxima_w, 100)),10);
			elsif	(ie_apresentacao_media_w = '2') then
				vl_media_w	:= vl_nota_w;
			end if;
			
			vl_media_acum_w		:= vl_media_acum_w + vl_media_w;
			end;
		end loop;
		close C01;
		
		if	(ie_apresentacao_media_w = '2') then
			vl_media_final_w := vl_media_acum_w;
		else
			vl_media_final_w := dividir(vl_media_acum_w, qt_perguntas_w);
		end if;
		end;
	elsif	(ie_apresentacao_media_w = '3') then
		begin		
		open C02;
		loop
		fetch C02 into	
			nr_seq_grupo_pergunta_w,
			pr_peso_grupo_w;
		exit when C02%notfound;
			begin
			qt_perguntas_w	:= 0;
			vl_media_acum_w	:= 0;
			
			open C03;
			loop
			fetch C03 into	
				nr_seq_pergunta_w,
				nr_seq_nota_item_w,
				pr_peso_pergunta_w;
			exit when C03%notfound;
				begin
				qt_perguntas_w := qt_perguntas_w + 1;
			
				select	nvl(max(vl_tipo_nota),0)
				into	vl_nota_w
				from	avf_tipo_nota_item
				where	nr_sequencia = nr_seq_nota_item_w;

				vl_media_acum_w	:= vl_media_acum_w + (vl_nota_w * dividir(nvl(pr_peso_pergunta_w,100),100));
				end;
			end loop;
			close C03;
			
			vl_media_acum_w		:= dividir(dividir(vl_media_acum_w, qt_perguntas_w) * pr_peso_grupo_w,100);
			vl_media_final_w	:= vl_media_final_w + vl_media_acum_w;
			end;
		end loop;
		close C02;
		end;
	end if;	
	end;
elsif	(nvl(ie_pontuacao_pergunta_w, 'N') = 'S') then
	begin
	select	nvl(sum(qt_pontos),0) qt_pontos
	into	vl_media_w
	from	avf_pergunta_tipo a,
		avf_resultado_item b
	where	a.nr_seq_pergunta	= b.nr_seq_pergunta
	and	a.nr_seq_tipo_nota_item	= b.nr_seq_nota_item
	and	b.nr_seq_resultado	= nr_sequencia_p;

	vl_media_final_w := vl_media_w;
	end;
end if;

return	vl_media_final_w;

end avf_gerar_nota_avaliacao;
/