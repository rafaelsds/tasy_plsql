CREATE OR REPLACE
FUNCTION Obter_Regra_Funcao_Partic_Cir(
					cd_estabelecimento_p	Number,
					cd_convenio_p		number,
					cd_categoria_p		Varchar2,
					cd_procedimento_p	Number,
					ie_origem_proced_p	Number,
					dt_conta_p		Date,
					nr_seq_proc_p		Number,
					cd_funcao_p		Varchar2)
					RETURN VARCHAR2 IS

ie_auxiliar_w			Varchar2(01);
ie_anestesista_w		Varchar2(01);
nr_auxiliares_w			Number(03,0);
nr_aux_partic_w			Number(03,0);
qt_porte_anestesico_w		Number(03,0);
ie_regra_w			Varchar2(01);
cd_edicao_amb_w			Number(06,0);
ie_prioridade_edicao_w		varchar2(01);
VL_CH_HONORARIOS_W		NUMBER(15,4) := 1;
VL_CH_CUSTO_OPER_W		NUMBER(15,4) := 1;
VL_M2_FILME_W			NUMBER(15,4) := 0;
dt_inicio_vigencia_w		date;
tx_ajuste_geral_w		number(15,4);
ie_instrumentador_w		varchar2(01);
ie_gerar_instrumentador_w	varchar2(01);

ie_aux_cbhpm_porte_cir_w	varchar2(01);
ie_aux_amb_porte_cir_w		varchar2(01);
nr_cirurgia_w			number(10,0);
nr_interno_conta_w		number(10,0);
cd_edicao_ajuste_w		number(6);
ie_edicao_ajuste_partic_cir_w	varchar2(1);
ie_ind_equipe_sus_w		Number(1);
nr_seq_cbhpm_edicao_w		number(10,0);
ie_origem_proced_edicao_w	number(10,0);
dt_procedimento_w			procedimento_paciente.dt_procedimento%type;


BEGIN

select	max(nr_cirurgia),
	max(nr_interno_conta),
	max(dt_procedimento)
into	nr_cirurgia_w,
	nr_interno_conta_w,
	dt_procedimento_w
from	procedimento_paciente
where	nr_sequencia 	= nr_seq_proc_p;


select nvl(max(ie_auxiliar),'N'),
	nvl(max(ie_anestesista),'N'),
	nvl(max(ie_instrumentador),'N'),
	nvl(max(ie_ind_equipe_sus),0)
into	ie_auxiliar_w,
	ie_anestesista_w,
	ie_instrumentador_w,
	ie_ind_equipe_sus_w
from 	funcao_medico	
where cd_funcao	= cd_funcao_p;

select	nvl(max(ie_prioridade_edicao_amb), 'N'),
	nvl(max(ie_gerar_instrumentador),'S'),
	nvl(max(ie_aux_cbhpm_porte_cir),'N'),
	nvl(max(ie_aux_amb_porte_cir),'N'),
	nvl(max(ie_edicao_ajuste_partic_cir),'N')
into	ie_prioridade_edicao_w,
	ie_gerar_instrumentador_w,
	ie_aux_cbhpm_porte_cir_w,
	ie_aux_amb_porte_cir_w,
	ie_edicao_ajuste_partic_cir_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

/*      obter a edicao do convenio  */
if	(ie_prioridade_edicao_w	= 'N') then
	begin
	select 	nvl(max(cd_edicao_amb),0)
	into   	cd_edicao_amb_w
	from 	convenio_amb
	where 	(cd_estabelecimento     = cd_estabelecimento_p)
	and 	(cd_convenio            = cd_convenio_p)
	and 	(cd_categoria           = cd_categoria_p)
	and 	(nvl(ie_situacao,'A')	= 'A')
	and 	(dt_inicio_vigencia     =
      		(select max(dt_inicio_vigencia)
       		from 	convenio_amb a
       		where 	(a.cd_estabelecimento  = cd_estabelecimento_p)
         	and 	(a.cd_convenio         = cd_convenio_p)
         	and 	(a.cd_categoria        = cd_categoria_p)
	 	and 	(nvl(a.ie_situacao,'A')= 'A')
         	and 	(a.dt_inicio_vigencia <=  dt_conta_p)));
	end;
else
	Obter_Edicao_Proc_Conv
		(cd_estabelecimento_p,
		cd_convenio_p,
		cd_categoria_p,
		dt_conta_p,
		cd_procedimento_p,
		cd_edicao_amb_w,
		vl_ch_honorarios_W,
		vl_ch_custo_oper_w,
		vl_m2_filme_w,
		dt_inicio_vigencia_w,
		tx_ajuste_geral_w,
		nr_seq_cbhpm_edicao_w);
end if;

cd_edicao_ajuste_w	:= Obter_Dados_Ajuste_proc(	cd_estabelecimento_p,
							cd_convenio_p,
							cd_categoria_p,
							cd_procedimento_p,
							ie_origem_proced_p,
							0,
							dt_conta_p,
							1);
if	(nvl(cd_edicao_ajuste_w,0)	> 0) and
	(ie_edicao_ajuste_partic_cir_w	= 'S') then
	cd_edicao_amb_w	:= cd_edicao_ajuste_w;
end if;

select	nvl(max(ie_origem_proced),ie_origem_proced_p)
into	ie_origem_proced_edicao_w
from	edicao_amb
where	cd_edicao_amb	= cd_edicao_amb_w;


if	((ie_origem_proced_p	= 5) and
	(cd_edicao_amb_w 	in (2004,2005))) or (ie_origem_proced_edicao_w = 5) then
	begin
	select nvl(max(a.nr_auxiliar),0),
	 	 nvl(max(a.nr_porte_anest), 0)
	into	 nr_auxiliares_w,
	 	 qt_porte_anestesico_w
	from	 cbhpm_preco a
	where	 a.cd_procedimento	= cd_procedimento_p
	and	 a.ie_origem_proced	= ie_origem_proced_p
	and	 nvl(a.dt_vigencia,sysdate - 3650) =
			(select max(nvl(b.dt_vigencia,sysdate - 3650))
				from cbhpm_preco b
				where	b.cd_procedimento		= cd_procedimento_p
				and	b.ie_origem_proced	= ie_origem_proced_p
				and	nvl(b.dt_vigencia,sysdate - 3650)	<= dt_conta_p);

	if	(ie_aux_cbhpm_porte_cir_w	= 'S') then

		select	nvl(max(Obter_Aux_Maior_Porte_Cir(nr_cirurgia_w, nr_interno_conta_w, dt_conta_p, 
				ie_origem_proced_p, null, null)), nr_auxiliares_w)
		into	nr_auxiliares_w
		from	dual;
	end if;



	end;
else
	begin
	select nvl(max(a.nr_auxiliares),0),
	 	 nvl(max(a.qt_porte_anestesico), 0)
	into	 nr_auxiliares_w,
	 	 qt_porte_anestesico_w
	from 	 preco_amb a
	where	 a.cd_edicao_amb		= cd_edicao_amb_w
  	and	 a.cd_procedimento	= cd_procedimento_p
  	and	 a.ie_origem_proced	= ie_origem_proced_p
  	and    nvl(a.dt_inicio_vigencia,sysdate - 3650)	=
	 	(select max(nvl(b.dt_inicio_vigencia,sysdate - 3650))
			from preco_amb b
			where b.cd_edicao_amb		= cd_edicao_amb_w
			and	b.cd_procedimento		= cd_procedimento_p
			and	b.ie_origem_proced	= ie_origem_proced_p
			and	nvl(b.dt_inicio_vigencia,sysdate - 3650)	<= dt_conta_p);

	

	if	(ie_aux_amb_porte_cir_w	= 'S') then

		select	nvl(max(Obter_Aux_Maior_Porte_Cir(nr_cirurgia_w, nr_interno_conta_w, dt_conta_p, ie_origem_proced_p, 	

			cd_edicao_amb_w, null)), nr_auxiliares_w)
		into	nr_auxiliares_w
		from	dual;
	end if;

	end;
end if;


ie_regra_w	:= 'N';
if	(ie_anestesista_w	= 'S') and
	((qt_porte_anestesico_w	<> 0) and (ie_origem_proced_p not in (2,3,7))) then
	select 	count(*)
	into 	nr_aux_partic_w
	from 	funcao_medico b,
			cirurgia_participante a
	where	a.nr_seq_procedimento	= nr_seq_proc_p
      and   a.ie_funcao				= b.cd_funcao
	  and   ie_anestesista			= 'S';
	if	(nr_aux_partic_w <= 2) then
		ie_regra_w	:= 'S';	
	end if;
/*	Pode ter um auxiliar de anestesista
	if	(nr_aux_partic_w <= 0) then
		ie_regra_w	:= 'S';	
	end if;
*/
end if;
if	(ie_auxiliar_w	= 'S') and
	((nr_auxiliares_w > 0) and (ie_origem_proced_p not in (2,3,7))) then
	begin
	select 	count(*)
	into 	nr_aux_partic_w
	from 	funcao_medico b,
			cirurgia_participante a
	where	a.nr_seq_procedimento	= nr_seq_proc_p
    and   	a.ie_funcao				= b.cd_funcao
	and   	ie_auxiliar				= 'S';
	if	(nr_aux_partic_w < nr_auxiliares_w) then
		ie_regra_w	:= 'S';	
	end if;
	end;
end if;

/* Quando for SUS */
if	(((ie_anestesista_w = 'S') or (ie_auxiliar_w = 'S')) and
	(ie_origem_proced_p in (2,3,7))) then
	select	nvl(max(ie_gerar_partic_cirurg), 'N')
	into	ie_regra_w
	from	sus_parametros_aih
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(Sus_validar_regra(3, cd_procedimento_p, ie_origem_proced_p,dt_procedimento_w) = 0) or
		((ie_ind_equipe_sus_w = 6) and
		(Sus_Obter_Se_Detalhe_Proc(cd_procedimento_p,ie_origem_proced_p,'001',dt_procedimento_w)	= 0)) then
		ie_regra_w	:= 'N';
	end if;
end if;

if	(ie_regra_w = 'N') and (ie_instrumentador_w = 'S') and
	(ie_gerar_instrumentador_w = 'S') then	
	ie_regra_w:= 'S';
end if;

RETURN ie_regra_w;
END Obter_Regra_Funcao_Partic_Cir;
/
