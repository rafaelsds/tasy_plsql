create or replace
function Obter_se_agrup_audit_ext( nr_seq_regra_audit_ext_p	number,
				   nr_sequencia_p 		number,
				   ie_tipo_item_p		varchar2)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(1):= 'S';
cd_material_w		number(6,0);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
ie_agrupar_w		varchar2(1);
cd_grupo_material_w     number(3);
cd_subgrupo_material_w  number(3);
cd_classe_material_w    number(5);

cd_area_procedimento_w  number(15,0);
cd_especialidade_w      number(15,0);
cd_grupo_proc_w         number(15,0);

Cursor C01 is
	select	nvl(ie_agrupar,'S')
	from	regra_audit_ext_crit_mat
	where	nr_seq_regra_audit = nr_seq_regra_audit_ext_p
	and 	ie_situacao = 'A'
	and 	nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
	and 	nvl(cd_subgrupo_material, cd_subgrupo_material_w) = cd_subgrupo_material_w
	and 	nvl(cd_classe_material, cd_classe_material_w) = cd_classe_material_w
	and 	nvl(cd_material, cd_material_w) = cd_material_w
	order by nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0);
		
Cursor C02 is
	select	nvl(ie_agrupar,'S')
	from	regra_audit_ext_crit_proc
	where	nr_seq_regra_audit = nr_seq_regra_audit_ext_p
	and 	ie_situacao = 'A'
	and 	nvl(cd_area_procedimento, cd_area_procedimento_w) = cd_area_procedimento_w
	and 	nvl(cd_especialidade, cd_especialidade_w) = cd_especialidade_w
	and 	nvl(cd_grupo_proc, cd_grupo_proc_w) = cd_grupo_proc_w
	and 	nvl(cd_procedimento, cd_procedimento_w) = cd_procedimento_w	
	order by nvl(cd_procedimento,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0);

begin

if	(ie_tipo_item_p = 'P') then

	select 	max(cd_procedimento),
		max(ie_origem_proced)
	into	cd_procedimento_w,
		ie_origem_proced_w
	from 	procedimento_paciente
	where 	nr_sequencia = nr_sequencia_p;
	
	select 	cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc
	into	cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w
	from 	estrutura_procedimento_v
	where	cd_procedimento = cd_procedimento_w
	and 	ie_origem_proced = ie_origem_proced_w;
	
	open C02;
	loop
	fetch C02 into	
		ie_agrupar_w;
	exit when C02%notfound;
		begin
		ds_retorno_w:= ie_agrupar_w;
		end;
	end loop;
	close C02;

elsif	(ie_tipo_item_p = 'M') then

	select 	max(cd_material)
	into	cd_material_w	
	from 	material_atend_paciente
	where 	nr_sequencia = nr_sequencia_p;
	
	select 	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from 	estrutura_material_v
	where 	cd_material = cd_material_w;
	
	open C01;
	loop
	fetch C01 into	
		ie_agrupar_w;
	exit when C01%notfound;
		begin
		ds_retorno_w:= ie_agrupar_w;
		end;
	end loop;
	close C01;
	
end if;

return	ds_retorno_w;

end Obter_se_agrup_audit_ext;
/