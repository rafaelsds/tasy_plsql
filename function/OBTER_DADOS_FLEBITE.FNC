create or replace
function obter_dados_flebite( 	ie_opcao_p	varchar,
			dt_inicio_p	date,
			dt_final_p		date)
			return Number is
/*
Total de AVP-Dia		'TD'
Total de AVP-Inseridos	'TI'
Total de pacientes com flebite	'TF'
Total de Flebites		'TE'
*/
			
ds_retorno_w	Number(10);
	
begin

If	(ie_opcao_p = 'TD') then
	
	if (dt_final_p is null) then
		select 	count(*)
		into	ds_retorno_w
		from   	atend_pac_dispositivo a,
			dispositivo b
		where  	a.nr_seq_dispositivo = b.nr_sequencia
		and    	b.ie_classif_disp_niss = 'CVC'
		and    	trunc(a.dt_instalacao) <= dt_inicio_p
		and    	nvl(a.dt_retirada,dt_inicio_p) >= dt_inicio_p;
	else
		select 	count(*)
		into	ds_retorno_w
		from   	atend_pac_dispositivo a,
			dispositivo b
		where  	a.nr_seq_dispositivo = b.nr_sequencia
		and    	b.ie_classif_disp_niss = 'CVC'
		and    	trunc(a.dt_instalacao) <= dt_inicio_p
		and    	nvl(a.dt_retirada,dt_final_p) >= dt_final_p;
	end if;

elsif	(ie_opcao_p = 'TI') then

	select 	count(*)
	into	ds_retorno_w
	from   	atend_pac_dispositivo a,
		dispositivo b
	where  	a.nr_seq_dispositivo = b.nr_sequencia
	and    	b.ie_classif_disp_niss = 'CVC'
	and    	trunc(a.dt_instalacao) between dt_inicio_p and nvl(dt_final_p,dt_inicio_p);
	
	
elsif	(ie_opcao_p = 'TF') then

	select 	count(distinct a.nr_atendimento)
	into	ds_retorno_w
	from	qua_evento_paciente a,
 		qua_tipo_evento b,
		qua_evento c
	where  	b.ie_tipo_evento = 'AF'
	and	b.nr_sequencia = c.nr_seq_tipo
	and 	a.nr_seq_evento = c.nr_sequencia
	and	trunc(dt_evento) between dt_inicio_p and nvl(dt_final_p,dt_inicio_p);
	
elsif	(ie_opcao_p = 'TE') then

	select 	count(*)
	into	ds_retorno_w
	from	qua_evento_paciente b,
		qua_evento_flebite e
	where	e.nr_seq_evento = b.nr_sequencia
	and	b.dt_inativacao is null
	and	e.ie_origem = '1'
	and    	trunc(b.dt_evento) between dt_inicio_p and nvl(dt_final_p,dt_inicio_p);
		
end if;

return	ds_retorno_w;

end obter_dados_flebite;
/