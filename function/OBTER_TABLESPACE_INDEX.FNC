CREATE OR REPLACE
FUNCTION Obter_Tablespace_Index(nm_objeto_p	varchar2 default null)
RETURN VARCHAR2 IS

nm_tablespace_w	Varchar2(40);
nm_user_w	Varchar2(40);
ie_configurar_w	varchar2(1);

BEGIN

EXECUTE IMMEDIATE 'BEGIN :a := wheb_usuario_pck.get_configurar_tablespace; END;' USING IN OUT ie_configurar_w;
		
if	(nm_objeto_p is not null) and
	(ie_configurar_w = 'S') then
	OBTER_VALOR_DINAMICO_CHAR_BV('select obter_nome_tablespace(:NM_OBJETO,:IE_TIPO) from dual',
			'NM_OBJETO=' || nm_objeto_p || ';' ||
			'IE_TIPO=' || 'INDICE' || ';', nm_tablespace_w);
	if	(nm_tablespace_w is null) then
		select	Obter_Tablespace_Index(null)
		into	nm_tablespace_w
		from	dual;
	end	if;
else
	OBTER_VALOR_DINAMICO_CHAR_BV(
		'select nvl(vl_parametro,vl_parametro_padrao) '||
		'from 	funcao_parametro '||
		'where	nr_sequencia = 95 ' ||
		'and 	cd_funcao = 0 ','',nm_tablespace_w);
		
	if	( nm_tablespace_w is null ) then
	
		select	username
		into	nm_user_w
		from	user_users;
	
		select	max(tablespace_name)
		into	nm_tablespace_w
		from	user_tablespaces
		where	tablespace_name 	= nm_user_w || '_INDEX';
	
		if	(nm_tablespace_w is null) then
			nm_tablespace_w	:= 'TASY_INDEX';
		end if;
	end if;
end	if;

RETURN nm_tablespace_w;

END Obter_Tablespace_Index;
/
