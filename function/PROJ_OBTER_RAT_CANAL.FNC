create or replace
function proj_obter_rat_canal(	cd_pessoa_fisica_p		varchar2,
			dt_ano_p			varchar2,
			ie_mes_p			varchar2,
			ie_interno_p		varchar2)
 		    	return number is

vl_retorno_w	number(15,2);
dt_referencia_w	date;

begin
dt_referencia_w	:= to_date('01/'||ie_mes_p||'/'||dt_ano_p,'dd/mm/yyyy');

if	(ie_interno_p = 'S') then
	begin
	select	(nvl((sum(nvl(c.qt_min_ativ,0)) - sum(nvl(c.qt_min_intervalo,0))),0)/60) qt_total_rat_ativ
	into	vl_retorno_w
	from	proj_projeto a,
		proj_rat b,
		proj_rat_ativ c
	where	a.nr_sequencia = b.nr_seq_proj
	and	b.nr_sequencia = c.nr_seq_rat
	and	a.nr_seq_classif = 15
	and	b.cd_executor = cd_pessoa_fisica_p
	and	c.dt_inicio_ativ between dt_referencia_w and fim_mes(dt_referencia_w);
	end;
elsif	(ie_interno_p = 'N') then
	begin
	select	(nvl((sum(nvl(c.qt_min_ativ,0)) - sum(nvl(c.qt_min_intervalo,0))),0)/60) qt_total_rat_ativ
	into	vl_retorno_w
	from	proj_projeto a,
		proj_rat b,
		proj_rat_ativ c
	where	a.nr_sequencia = b.nr_seq_proj
	and	b.nr_sequencia = c.nr_seq_rat
	and	a.nr_seq_classif <> 15
	and	b.cd_executor = cd_pessoa_fisica_p
	and	c.dt_inicio_ativ between dt_referencia_w and fim_mes(dt_referencia_w);
	end;
end if;

return	vl_retorno_w;

end proj_obter_rat_canal;
/