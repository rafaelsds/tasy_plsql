create or replace
function obter_tipo_devol_mat_prescr(
		nr_prescricao_p		number,
		nr_seq_prescr_p		number,
		nr_devolucao_p		number,
		nr_seq_item_devol_p	number)
	return varchar2 is


ie_tipo_baixa_estoque_w		varchar2(002);
ds_tipo_devolucao_w		varchar2(100);

begin
if (nvl(nr_seq_prescr_p, 0) > 0) and
		(nvl(nr_devolucao_p, 0) > 0) and
		(nvl(nr_seq_item_devol_p, 0) > 0) then

	select	max(ie_tipo_baixa_estoque)
	into	ie_tipo_baixa_estoque_w
	from	item_devolucao_material_pac
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia_prescricao	= nr_seq_prescr_p
	and	nr_devolucao		= nr_devolucao_p
	and	nr_sequencia		= nr_seq_item_devol_p;
	
/*Se tem a sequencia, � porque o item foi prescrito, ent�o podemos buscar por este valor*/
elsif	(nvl(nr_seq_prescr_p, 0) > 0) then
	select	max(ie_tipo_baixa_estoque)
	into	ie_tipo_baixa_estoque_w
	from	item_devolucao_material_pac
	where	nr_prescricao			= nr_prescricao_p
	and	nr_sequencia_prescricao		= nr_seq_prescr_p;
/*Se n�o tem sequencia do item... quer dizer que n�o foi prescrito, ent�o buscamos pelo numero da devolu��o*/
elsif	(nvl(nr_seq_prescr_p, 0) = 0) and
	(nvl(nr_devolucao_p, 0) > 0) and
	(nvl(nr_seq_item_devol_p, 0) > 0) then
	select	max(ie_tipo_baixa_estoque)
	into	ie_tipo_baixa_estoque_w
	from	item_devolucao_material_pac
	where	nr_devolucao		= nr_devolucao_p
	and	nr_sequencia		= nr_seq_item_devol_p;
end if;

if	(ie_tipo_baixa_estoque_w is not null) then
	begin

	select	max(ds_tipo_baixa)
	into	ds_tipo_devolucao_w
	from 	tipo_baixa_prescricao
	where 	ie_prescricao_devolucao	= 'D'
	and	cd_tipo_baixa		= ie_tipo_baixa_estoque_w;
	end;
end if;


return	ds_tipo_devolucao_w;

end obter_tipo_devol_mat_prescr;
/
