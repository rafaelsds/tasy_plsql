create or replace
function obter_dados_trans_financ(	nr_seq_trans_fin_p		number,
				ie_opcao_p				varchar2)
				return varchar2 is

ds_retorno_w	varchar2(250);

/*
N - Nome da Transa��o Financeira
TS - Tipo saldo da transa��o financeira
*/

begin


if	(nr_seq_trans_fin_p is not null) then
	begin
	
	if	(ie_opcao_p = 'N') then
		begin
		
		select	ds_transacao
		into	ds_retorno_w
		from	transacao_financeira
		where	nr_sequencia	= nr_seq_trans_fin_p;
		
		end;
	end if;
	
	if	(ie_opcao_p = 'TS') then
		begin
		
		select	ie_saldo_caixa
		into	ds_retorno_w
		from	transacao_financeira
		where	nr_sequencia	= nr_seq_trans_fin_p;
		
		end;
	end if;
		
		
	end;
end if;

return	ds_retorno_w;

end;
/