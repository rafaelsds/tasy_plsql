create or replace
function sac_obter_se_existe_setor_adic(nr_seq_responsavel_p		number)
 		    	return varchar2 is

qt_setor_w		NUMBER(15,2) := 0;
qt_setor_adic_w NUMBER(15,2) := 0;
ie_setor_adic_w	varchar2(1) := '';

begin

select	count(cd_setor_atendimento)
into	qt_setor_w
from	sac_responsavel
where	nr_sequencia = nr_seq_responsavel_p;


select	count(cd_setor_atendimento)
into	qt_setor_adic_w
from	sac_responsavel_setor_adic
where	nr_seq_responsavel = nr_seq_responsavel_p;

if	((nvl(qt_setor_w,0) + nvl(qt_setor_adic_w,0)) <> 0) then
	ie_setor_adic_w := 'S';
else
	ie_setor_adic_w := 'N';
end if;


return	ie_setor_adic_w;

end sac_obter_se_existe_setor_adic;
/