create or replace
function Obter_Se_Pend_Fechar_conta(nr_seq_tipo_p		number)
 		    	return Varchar2 is

ds_retorno_w	varchar2(1);			
			
begin

select	nvl(max(ie_fechar_conta),'N')
into	ds_retorno_w
from	cta_tipo_pend b
where	b.nr_sequencia = nr_seq_tipo_p;

return	ds_retorno_w;

end Obter_Se_Pend_Fechar_conta;
/