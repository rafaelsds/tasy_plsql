create or replace
function obter_desc_disp_adjunto(nr_seq_disp_p		Number)
 		    	return Varchar2 is

ds_retorno_w	Varchar2(255);
begin

if	(nr_seq_disp_p is not null) then

	select	ds_dispositivo
	into	ds_retorno_w
	from	hem_disp_adjunto
	where 	nr_sequencia = nr_seq_disp_p;

end if;

return	ds_retorno_w;

end obter_desc_disp_adjunto;
/