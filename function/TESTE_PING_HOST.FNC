create or replace
function teste_ping_host ( 	p_host_name  varchar2,
				p_port 	     number)
return varchar2 is
tcpconnection utl_tcp.connection; 
begin
/*Se retornar "S" o sistema conseguiu pingar no IP passado*/
begin
	tcpconnection := utl_tcp.open_connection (p_host_name, p_port);
	utl_tcp.close_connection (tcpconnection);
	return 'S';
exception
when utl_tcp.network_error then

if (upper(sqlerrm) like '%HOST%') then 
		return 'N';
	elsif (upper (sqlerrm) like '%LISTENER%') then 
		return 'S';
	else
		return 'N';
	end if;
end;

end teste_ping_host;
/