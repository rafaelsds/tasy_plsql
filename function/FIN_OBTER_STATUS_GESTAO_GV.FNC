create or replace
function fin_obter_status_gestao_gv(	nr_seq_gv_p		number,
				ie_opcao_p		varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(50) := wheb_mensagem_pck.get_texto(803053);
ie_retorno_w		varchar2(5) := '1';
ie_etapa_viagem_w		varchar2(10);
dt_receb_fin_w		date;
dt_aprov_fin_w		date;
dt_receb_proj_w		date;

begin
select	max(a.ie_etapa_viagem),
	max(a.dt_receb_fin),
	max(a.dt_aprov_fin),
	max(a.dt_receb_proj)
into	ie_etapa_viagem_w,
	dt_receb_fin_w,
	dt_aprov_fin_w,
	dt_receb_proj_w
from	via_viagem a
where	a.nr_sequencia = nr_seq_gv_p;

if	(nvl(dt_aprov_fin_w,null) is not null) then
	ie_retorno_w := '6';
elsif	(nvl(dt_receb_fin_w,null) is not null) then
	ie_retorno_w := '5';
elsif	(nvl(ie_etapa_viagem_w,'X') = '5') then
	ie_retorno_w := '4';
elsif	(nvl(dt_receb_proj_w,null) is not null) then
	ie_retorno_w := '3';
elsif	(nvl(ie_etapa_viagem_w,'X') = '8') then
	ie_retorno_w := '2';
else	ie_retorno_w := '1';
end if;

if	(ie_opcao_p = 'C') then
	ds_retorno_w := ie_retorno_w;
elsif	(ie_retorno_w = '1') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(803053);
elsif	(ie_retorno_w = '2') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(803054);
elsif	(ie_retorno_w = '3') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(803055);
elsif	(ie_retorno_w = '4') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(803056);
elsif	(ie_retorno_w = '5') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(803057);
elsif	(ie_retorno_w = '6') then
	ds_retorno_w := wheb_mensagem_pck.get_texto(803058);
end if;

return	ds_retorno_w;

end fin_obter_status_gestao_gv;
/