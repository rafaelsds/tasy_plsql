create or replace
function  obter_medico_alta(nr_atendimento_p	varchar2)
				return varchar2 is

ds_retorno_w				pessoa_fisica.ds_codigo_prof%type;
nm_usuario_w				atendimento_paciente.nm_usuario_alta_medica%type;
ie_usuario_alta_medico_w	varchar2(1);
				
begin

if (nr_atendimento_p is not null) then
	
	select	obter_se_usuario_medico(nvl(a.nm_usuario_alta_medica, a.nm_usuario_alta)),
			nvl(a.nm_usuario_alta_medica, a.nm_usuario_alta)
	into	ie_usuario_alta_medico_w,
			nm_usuario_w
	from	atendimento_paciente a 
	where 	a.nr_atendimento = nr_atendimento_p;
	
	if (ie_usuario_alta_medico_w = 'S') then
	
		select 	a.ds_codigo_prof
		into	ds_retorno_w
		from	pessoa_fisica a
		where	a.cd_pessoa_fisica = Obter_Pf_Usuario(nm_usuario_w, 'C');
	
	end if;
	
end if;


return	ds_retorno_w;

end obter_medico_alta;
/