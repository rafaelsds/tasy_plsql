create or replace
function obter_se_rep_cpoe(nr_prescricao_p		Number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);				
				
begin

select	nvl(max('S'),'N')
into	ds_retorno_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p
and		cd_funcao_origem = 2314;

return	ds_retorno_w;

end obter_se_rep_cpoe;
/
	