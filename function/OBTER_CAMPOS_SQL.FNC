create or replace 
function obter_campos_SQL(ds_comando_sql_p	varchar2)
return varchar2 is

atributos_sql_w		dbms_sql.desc_tab;
nm_atributo_sql_w	varchar2(80);
nr_atributos_sql_w	pls_integer;
ie_tipo_dado_w		number(10);
ds_tipo_dado_w		varchar2(10);
ds_retorno_w		varchar2(2000);
cursor_w			integer;
qt_decimais_w		number(10);

begin
	ds_retorno_w := '';

	if	(ds_comando_sql_p is not null) then
		begin
			cursor_w	:= dbms_sql.open_cursor;
			dbms_sql.parse(cursor_w, ds_comando_sql_p, dbms_sql.native);
			dbms_sql.describe_columns(cursor_w, nr_atributos_sql_w, atributos_sql_w);
			
			for i in 1 .. nr_atributos_sql_w loop
				begin
					nm_atributo_sql_w	:= atributos_sql_w(i).col_name;
					ie_tipo_dado_w		:= atributos_sql_w(i).col_type;
					qt_decimais_w		:= nvl(atributos_sql_w(i).col_scale,0);

					if (ie_tipo_dado_w = 12) then 
						ds_tipo_dado_w := 'date';
					elsif  (ie_tipo_dado_w = 2) then 
						if (qt_decimais_w > 0) then 
							ds_tipo_dado_w := 'float';
						else 
							ds_tipo_dado_w := 'long';
						end if;
					else 
						ds_tipo_dado_w := 'string';
					end if;

					ds_retorno_w := ds_retorno_w || nm_atributo_sql_w || '-' || ds_tipo_dado_w || ',';
				end;
			end loop;
			dbms_sql.close_cursor(cursor_w);
		end;
	end if;	

	ds_retorno_w := substr(ds_retorno_w,1,length(ds_retorno_w)-1);					

return ds_retorno_w;  	

end obter_campos_SQL;
/
