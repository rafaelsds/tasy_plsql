create or replace function get_display_new_surgery (	cd_pessoa_fisica_p	varchar2,
																		nr_atendimento_p		number,
																		nr_cirurgia_p			number,
																		nr_prescricao_p		number)	return varchar2 is
																		
ie_display_w				varchar2(1):= 'N';																		
ie_mostra_cancelada_w		varchar2(1):= 'N';																	

begin

obter_param_usuario(872, 367, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_mostra_cancelada_w);

if(philips_param_pck.get_cd_pais in (7, 14, 5)) then
	ie_display_w := 'S';
else
	ie_display_w := 'N';
end if;

return	ie_display_w;

end get_display_new_surgery;
/
