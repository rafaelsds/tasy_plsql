create or replace
function mprev_obter_qt_canc_agend( 	dt_inicial_p	date,
					dt_final_p	date,
					cd_agenda_p	varchar2	)
 		    	return number is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Obter dados de um agendamento da agenda da Medicina Preventiva.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicionario [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
qt_retorno_w		number(10);

begin

if	( dt_inicial_p is not null)	or 
		( dt_final_p is not null)	or
			( cd_agenda_p is not null)	then

select count(1)
into	qt_retorno_w
from (
	select  *
	from	mprev_agendamento a
	where	a.cd_agenda	= cd_agenda_p
	and	a.ie_status_agenda = 'C'
	and	dt_inicial_p between a.dt_agenda and a.dt_final_agenda
	and 	a.dt_final_agenda > dt_inicial_p
	union
	select  *
	from	mprev_agendamento a
	where	a.cd_agenda	= cd_agenda_p
	and	a.ie_status_agenda = 'C'
	and	dt_final_p between a.dt_agenda and a.dt_final_agenda
	and 	a.dt_agenda > dt_final_p
);

end if;

return	qt_retorno_w;

end mprev_obter_qt_canc_agend;
/
