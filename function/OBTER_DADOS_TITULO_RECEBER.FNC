create or replace function obter_dados_titulo_receber
			(	nr_titulo_p		number,
				ie_tipo_dado_p		varchar2)
		return varchar2 is
/*
	'N'  - Nome Pessoa Fisica/Juridica
	'PFC' - Codigo Pessoa Fisica
	'PJC' - Codigo Pessoa Juridica
	'P'  - Valor pago do titulo
	'V'  - Valor atual do titulo, casos em que houve alteracao de valor
	'U'  - Data da ultima baixa
	'NF' - Nota fiscal do protocolo
	'O'  - Titulo que originou este titulo
	'J'  - Valor do juros
	'M'  - Valor da multa
	'R'  - Valor recebido
	'S'  - Descricao da Situacao
	'D'  - Data de vencimento
	'B'  - Data p/ Baixa CR	
	'VT' - Valor do titulo
	'NA' - Numero do atendimento
	'SN' - Sequencia da nota fiscal
	'CO' - Convenio
	'T'  - Tipo titulo
	'NE' - Nota fiscal eletronica
	'PP' - Data pagamento previsto
	'L'  - Data de Liquidacao
	'VA' - Valor da alteracao do titulo
	'NFT' - Nota fiscal do titulo
	'VD' - Valor do titulo - Valor desdobrado
	'GA' - Guia do atendimento
	'SI' - Situacao
	'PA' - Paciente

	'VDS'- Valor de descontos
	'VLL' - Valor liquido do titulo (sem impostos)
	'DC' - Data Referencia Conta Paciente
	'VDP' - Valor de desconto previsto
	'VNC' - Valor das notas de credito pendentes de baixa
	'VAT' - Variacao ativa
	'VPA' - Variacao passiva
	'VP' - Valor de perdas
	'VM' - Valor amaior
	'VGR' - Valor glosado fora recurso de glosas
	'VG' - valor glosado
	'DV' - Data de vencimento atual
	'DOC' - Nr documento
	'SEA' - Setor entrada atendimento
	'TPJ' - Tipo Pessoa
	'PARC' - Numero da parcela do titulo a receber da conta paciente
	'DEP' - Data de entrada do paciente
	'VATR' - Valor alterado transferido
	'NFP' - Nota fiscal do protocolo
	'VL' - Valor liquido
	'VTD' - Valor titulo desdobramento
	'CS' - Codigo de serie
	'PDJ' - Percentual diario de juros
	'TXM' - Taxa de multa
	'NIC' - Numero interno da conta
	'TDFB' Descricao da transacao financeira da baixa
	'CF' Conta Financeira
	'DCL' Descricao da classe do titulo
	'VRP' Valor de recuperacao de perda
	'TPR' Tipo perda
	'NTE' Numero do Titulo Externo
	'DAV' Data alteracao vencimento
	
	Plano de saude
	'ME' - Mensalidade
	'ES' - Estipulante
	'CT' - Contrato
	'LM' - Lote mensalidade
	'MM' - Mes mensalidade
	'PM' - Parcela mensalidade
	'DE' - Data emissao
	'DEF' - Data emissao formatada
	'EP' - Empresa pagador
	'PPS' - Sequencia do pagador do Plano de Saude
	'OBS' - Observacao da mensalidade
	'OAV' - Observacao da alteracao de vencimento
	'RCC' - Responsavel pela conta na agenda cirurgica
	'PR' - Produto da mensalidade
	'CA' - Carteirinha beneficiario
	'MCC' - Motivo cancelamento contrato
	'NRC' - Numero do contrato
	'VLG' - Valor de glosa nao aceita
	'DRP' - Data de rescisao do pagador
	'NP' - Numero do Protocolo
	'TP' - Tipo portador
	'NTP' - Nome portador
	'CDO' - Codigo operadora
	'DRPC' - Data de rescisao do pagador ou do contrato
	'OT' - Origem do titulo
*/

ds_retorno_w			VARCHAR2(255)	:= null;
nm_pessoa_w			VARCHAR2(80);
cd_pessoa_fisica_w		varchar2(10);
cd_pessoa_juridica_w		varchar2(14);
nr_nota_fiscal_w		varchar2(255);
vl_pago_w			NUMBER(15,2);
vl_titulo_w			NUMBER(15,2);
vl_alteracao_w			NUMBER(15,2);
dt_recebimento_w		DATE;
dt_baixa_cr_w			date;
vl_juros_w			NUMBER(15,2);
vl_descontos_w			NUMBER(15,2);
vl_multa_w			NUMBER(15,2);
vl_recebido_w			NUMBER(15,2);
vl_transferido_w			NUMBER(15,2) := 0;

ds_situacao_w			VARCHAR2(255);		/* Jacson OS 47302 */
vl_saldo_titulo_w		NUMBER(15,2);
vl_titulo_desdob_w		NUMBER(15,2);

dt_vencimento_w			DATE;
nr_atendimento_w		number(10);
nr_seq_nf_saida_w		number(10);
cd_convenio_w			number(5);
ie_tipo_titulo_w		varchar2(5);
nr_interno_conta_w		number(10);
nr_seq_protocolo_w		number(10);
nr_nfe_imp_w			varchar2(255)	:= '';
dt_pagamento_prev_w		date;
dt_liquidacao_w			date;
ie_situacao_w			varchar2(1);
nm_paciente_w			varchar2(80);

nr_seq_mensalidade_w		number(10);
nr_seq_contrato_w		number(10);
nr_seq_lote_mens_w		number(10);
dt_ref_mens_w			date;
ds_estipulante_w		varchar2(80);
ie_tit_rec_canc_w		varchar2(255);
nr_parcela_w			number(10);
vl_tributo_w			number(15,2);
dt_ref_con_paci_w		date;
vl_desc_previsto_w		number(15,2);
dt_emissao_w			date;
ie_desc_prev_w			varchar2(1);
cd_estabelecimento_w		number(4);
vl_nota_credito_w		number(15,2);
qt_baixa_w			number(10,0);
qt_total_w			number(10,0);
qt_parcela_w			number(10,0);
vl_variacao_w			number(15,2);
vl_perdas_w			number(15,2);
vl_amaior_w			number(15,2);
nr_documento_w			varchar2(255);
dt_alteracao_w			date;
dt_entrada_w			date;
nr_seq_plano_w			number(10);
nr_seq_pagador_w		number(10);
cd_tipo_taxa_juro_w		number(15);
cd_tipo_taxa_multa_w		number(15);
pr_juros_w			number(15,4)	:= 0;
tx_juros_w			number(15,4);
pr_multa_w			number(15,4)	:= 0;
tx_multa_w			number(15,4);
ie_tipo_taxa_w			varchar2(255);
nr_seq_alter_vencto_w	alteracao_vencimento.nr_sequencia%type;
dt_alteracao_vencto_w	alteracao_vencimento.dt_alteracao%type;
vl_liquido_w			titulo_receber.vl_titulo%type;

BEGIN

IF	(ie_tipo_dado_p = 'N') THEN

	SELECT	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,80) nm_pessoa
	INTO	nm_pessoa_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w	:= nm_pessoa_w;

ELSIF	(ie_tipo_dado_p = 'PFC') THEN

	SELECT	a.cd_pessoa_fisica
	INTO	cd_pessoa_fisica_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w	:= cd_pessoa_fisica_w;

ELSIF	(ie_tipo_dado_p = 'PJC') THEN

	SELECT	a.cd_cgc
	INTO	cd_pessoa_juridica_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w	:= cd_pessoa_juridica_w;

ELSIF	(ie_tipo_dado_p = 'P') THEN

	SELECT	a.vl_titulo - a.vl_saldo_titulo vl_pago
	INTO	vl_pago_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w := vl_pago_w;

ELSIF	(ie_tipo_dado_p = 'S') THEN

	SELECT	substr(obter_valor_dominio(710, a.ie_situacao),1,200)
	INTO	ds_situacao_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w := ds_situacao_w;

ELSIF	(ie_tipo_dado_p = 'D') THEN

	SELECT	a.dt_vencimento
	INTO	dt_vencimento_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w := to_char(dt_vencimento_w,'dd/mm/yyyy');
	
ELSIF	(ie_tipo_dado_p = 'VS') THEN

	SELECT	a.vl_saldo_titulo
	INTO	vl_saldo_titulo_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w := vl_saldo_titulo_w;

ELSIF	(ie_tipo_dado_p = 'VT') THEN

	SELECT	a.vl_titulo
	INTO	vl_titulo_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w := vl_titulo_w;

ELSIF	(ie_tipo_dado_p = 'V') THEN

	SELECT	a.vl_titulo
	INTO	vl_titulo_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	SELECT	NVL(SUM(DECODE(ie_aumenta_diminui, 'A', vl_alteracao, vl_alteracao * -1)),0)
	INTO	vl_alteracao_w
	FROM	alteracao_valor
	WHERE	nr_titulo	= nr_titulo_p;

	ds_retorno_w		:= vl_titulo_w + vl_alteracao_w;

ELSIF	(ie_tipo_dado_p = 'O') THEN
	BEGIN
	select	max(nr_titulo)
	INTO	ds_retorno_w
	FROM	titulo_receber_desdob
	WHERE	nr_titulo_dest = nr_titulo_p;
	END;

ELSIF	(ie_tipo_dado_p = 'NF') THEN
	SELECT	nvl(max(somente_numero(b.nr_nota_fiscal)),null)
	INTO	nr_nota_fiscal_w
	FROM	titulo_receber	a,
		nota_fiscal	b
	WHERE	a.nr_titulo		= nr_titulo_p
	AND	a.nr_seq_protocolo	= b.nr_seq_protocolo;

	ds_retorno_w			:= nr_nota_fiscal_w;
	
ELSIF	(ie_tipo_dado_p = 'U') THEN
	BEGIN
	SELECT	MAX(DT_RECEBIMENTO)
	INTO	dt_recebimento_w
	FROM	titulo_receber_liq
	WHERE	nr_titulo	= nr_titulo_p
	and	nvl(ie_lib_caixa, 'S')	= 'S';

	ds_retorno_w		:= to_char(dt_recebimento_w,'dd/mm/yyyy');
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			ds_retorno_w		:= NULL;
	END;

ELSIF	(ie_tipo_dado_p = 'DEP') THEN
	BEGIN

	dt_entrada_w	:= null;
	
	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	titulo_receber
	where	nr_titulo		= nr_titulo_p;

	if	(nr_atendimento_w is null) then

		select	max(nr_interno_conta)
		into	nr_interno_conta_w
		from	titulo_receber
		where	nr_titulo		= nr_titulo_p;

		if	(nr_interno_conta_w is not null) then

			select	max(nr_atendimento)
			into	nr_atendimento_w
			from	conta_paciente
			where	nr_atendimento	= nr_atendimento_w;

			select	max(dt_entrada)
			into	dt_entrada_w
			from	atendimento_paciente
			where	nr_atendimento	= nr_atendimento_w;

		end if;

	else	

		select	max(dt_entrada)
		into	dt_entrada_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_w;

	end if;

	ds_retorno_w		:= to_char(dt_entrada_w,'dd/mm/yyyy');
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			ds_retorno_w		:= NULL;
	END;

ELSIF	(ie_tipo_dado_p = 'B') THEN
	begin
	select	max(r.dt_baixa_cr)
	into	dt_baixa_cr_w
	from	convenio_retorno r,
		convenio_retorno_item i
	where	r.nr_sequencia	= i.nr_seq_retorno
	and	i.nr_titulo	= nr_titulo_p;

	ds_retorno_w		:= to_char(dt_baixa_cr_w,'dd/mm/yyyy');
	exception
		when no_data_found then
			ds_retorno_w	:= null;
	end;

ELSIF	(ie_tipo_dado_p = 'J') THEN

	select	nvl(sum(vl_juros),0)
	into	vl_juros_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_p;

	ds_retorno_w		:= vl_juros_w;
ELSIF	(ie_tipo_dado_p = 'VDS') THEN

	select	nvl(sum(vl_descontos),0)
	into	vl_descontos_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_p;

	ds_retorno_w		:= vl_descontos_w;
ELSIF	(ie_tipo_dado_p = 'M') THEN

	select	nvl(sum(vl_multa),0)
	into	vl_multa_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_p;

	ds_retorno_w		:= vl_multa_w;

ELSIF	(ie_tipo_dado_p = 'R') THEN

	select	nvl(sum(vl_recebido),0)
	into	vl_recebido_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_p;

	ds_retorno_w		:= vl_recebido_w;
ELSIF	(ie_tipo_dado_p = 'NA') THEN

	SELECT	a.nr_atendimento
	INTO	nr_atendimento_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= nr_atendimento_w;
ELSIF	(ie_tipo_dado_p = 'SN') THEN

	SELECT	a.nr_seq_nf_saida
	INTO	nr_seq_nf_saida_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= nr_seq_nf_saida_w;
ELSIF	(ie_tipo_dado_p	= 'CO') THEN

	SELECT	nvl(a.cd_convenio_conta, obter_convenio_tit_rec(a.nr_titulo))
	INTO	cd_convenio_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= cd_convenio_w;
ELSIF	(ie_tipo_dado_p = 'T') THEN

	SELECT	a.ie_tipo_titulo
	INTO	ie_tipo_titulo_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= ie_tipo_titulo_w;
ELSIF	(ie_tipo_dado_p	= 'NE') THEN

	SELECT	a.nr_interno_conta,
		a.nr_seq_protocolo,
		a.nr_seq_nf_saida
	INTO	nr_interno_conta_w,
		nr_seq_protocolo_w,
		nr_seq_nf_saida_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_nf_saida_w is not null) then

		select	max(nr_nfe_imp)
		into	nr_nfe_imp_w
		from	nota_fiscal
		where	nr_sequencia = nr_seq_nf_saida_w;
	elsif	(nr_interno_conta_w is not null) then

		select	max(nr_nfe_imp)
		into	nr_nfe_imp_w
		from	nota_fiscal
		where	nr_interno_conta = nr_interno_conta_w;

	elsif	(nr_seq_protocolo_w is not null) then

		select	max(nr_nfe_imp)
		into	nr_nfe_imp_w
		from	nota_fiscal
		where	nr_seq_protocolo = nr_seq_protocolo_w;
	end if;
	ds_retorno_w	:= nr_nfe_imp_w;


ELSIF	(ie_tipo_dado_p = 'PP') THEN

	SELECT	a.dt_pagamento_previsto
	INTO	dt_pagamento_prev_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= to_char(dt_pagamento_prev_w,'dd/mm/yyyy');
ELSIF	(ie_tipo_dado_p = 'L') THEN

	SELECT	a.dt_liquidacao
	INTO	dt_liquidacao_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= to_char(dt_liquidacao_w,'dd/mm/yyyy');
ELSIF	(ie_tipo_dado_p = 'VA') THEN
	SELECT	NVL(SUM(DECODE(ie_aumenta_diminui, 'A', vl_alteracao, vl_alteracao * -1)),0)
	INTO	vl_alteracao_w
	FROM	alteracao_valor
	WHERE	nr_titulo	= nr_titulo_p;
	ds_retorno_w		:= vl_alteracao_w;
ELSIF	(ie_tipo_dado_p = 'NFT') then
	select	nr_seq_nf_saida,
		nr_nota_fiscal
	into	nr_seq_nf_saida_w,
		nr_nota_fiscal_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;

	if	(nr_seq_nf_saida_w is not null) and
		(nr_nota_fiscal_w is null) then
		select	max(somente_numero(nr_nota_fiscal))
		into	nr_nota_fiscal_w
		from	nota_fiscal
		where	nr_sequencia	= nr_seq_nf_saida_w;
	end if;
	
	ds_retorno_w	:= nr_nota_fiscal_w;	

elsif	(ie_tipo_dado_p = 'VATR') then
	SELECT	NVL(SUM(DECODE(ie_aumenta_diminui, 'A', vl_alteracao, vl_alteracao * -1)),0)
	INTO	vl_alteracao_w
	FROM	alteracao_valor
	WHERE	nr_titulo	= nr_titulo_p;

	SELECT	a.ie_situacao
	INTO	ie_situacao_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(ie_situacao_w = '5') then
		select	nvl(sum(a.vl_titulo),0)
		into	vl_transferido_w
		from	titulo_receber a
		where	nr_titulo	in
			(
			select	nr_titulo_dest
			FROM	titulo_receber_desdob
			WHERE	nr_titulo 	= nr_titulo_p);
	end if;

	ds_retorno_w		:= vl_alteracao_w + vl_transferido_w;
ELSIF	(ie_tipo_dado_p = 'VD') then

	SELECT	a.vl_titulo
	INTO	vl_titulo_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	SELECT	NVL(SUM(DECODE(ie_aumenta_diminui, 'A', vl_alteracao, vl_alteracao * -1)),0)
	INTO	vl_alteracao_w
	FROM	alteracao_valor
	WHERE	nr_titulo	= nr_titulo_p;

	select	nvl(sum(vl_titulo), 0)
	into	vl_titulo_desdob_w
	from	titulo_receber_desdob
	where	nr_titulo	= nr_titulo_p;

	ds_retorno_w		:= vl_titulo_w + vl_alteracao_w - vl_titulo_desdob_w;
ELSIF	(ie_tipo_dado_p = 'GA') then

	SELECT	a.nr_atendimento
	INTO	nr_atendimento_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	select	max(nr_doc_convenio)
	into	ds_retorno_w
	from	atend_categoria_convenio
	where	nr_atendimento					= nr_atendimento_w
	and	obter_atecaco_atendimento(nr_atendimento)	= nr_seq_interno;

ELSIF	(ie_tipo_dado_p = 'VDE') THEN
	SELECT	NVL(SUM(vl_titulo), 0)
	INTO	vl_titulo_desdob_w
	FROM	titulo_receber_desdob
	WHERE	nr_titulo	= nr_titulo_p;

	ds_retorno_w		:= vl_titulo_desdob_w;
ELSIF	(ie_tipo_dado_p = 'SI') then

	SELECT	a.ie_situacao
	INTO	ie_situacao_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= ie_situacao_w;
ELSIF	(ie_tipo_dado_p = 'PA') then

	SELECT	substr(obter_pessoa_atendimento(a.nr_atendimento,'N'),1,200) nm_paciente
	INTO	nm_paciente_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= nm_paciente_w;








ELSIF	(ie_tipo_dado_p = 'ME') then

	SELECT	a.nr_seq_mensalidade
	INTO	nr_seq_mensalidade_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w		:= nr_seq_mensalidade_w;
elsif	(ie_tipo_dado_p = 'ES') then

	SELECT	a.nr_seq_mensalidade
	INTO	nr_seq_mensalidade_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
		select	max(nr_seq_contrato)
		into	nr_seq_contrato_w
		from	pls_mensalidade
		where	nr_sequencia	= nr_seq_mensalidade_w;

		if	(nr_seq_contrato_w is not null) then
			select	substr(obter_nome_pf_pj(cd_pf_estipulante,CD_CGC_ESTIPULANTE),1,80)
			into	ds_estipulante_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_w;
		end if;
	end if;
	
	ds_retorno_w	:= ds_estipulante_w;
elsif	(ie_tipo_dado_p = 'CT') then

	SELECT	a.nr_seq_mensalidade
	INTO	nr_seq_mensalidade_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
		select	max(nr_seq_contrato)
		into	nr_seq_contrato_w
		from	pls_mensalidade
		where	nr_sequencia	= nr_seq_mensalidade_w;
	end if;

	ds_retorno_w	:= nr_seq_contrato_w;
elsif	(ie_tipo_dado_p = 'LM') then

	SELECT	a.nr_seq_mensalidade
	INTO	nr_seq_mensalidade_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
		select	max(nr_seq_lote)
		into	nr_seq_lote_mens_w
		from	pls_mensalidade
		where	nr_sequencia	= nr_seq_mensalidade_w;
	end if;

	ds_retorno_w	:= nr_seq_lote_mens_w;

elsif	(ie_tipo_dado_p = 'MM') then

	SELECT	a.nr_seq_mensalidade
	INTO	nr_seq_mensalidade_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
		select	max(dt_referencia)
		into	dt_ref_mens_w
		from	pls_mensalidade
		where	nr_sequencia	= nr_seq_mensalidade_w;
	end if;

	ds_retorno_w	:= dt_ref_mens_w;

elsif	(ie_tipo_dado_p = 'PM') then

	SELECT	a.nr_seq_mensalidade
	INTO	nr_seq_mensalidade_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
		select	max(nr_parcela)
		into	nr_parcela_w
		from	pls_mensalidade
		where	nr_sequencia	= nr_seq_mensalidade_w;
	end if;

	ds_retorno_w	:= nr_parcela_w;

elsif	(ie_tipo_dado_p = 'VLL') then
	select	a.vl_titulo,
		a.cd_estabelecimento
	into	vl_titulo_w,
		cd_estabelecimento_w
	from	titulo_receber a
	where	a.nr_titulo		= nr_titulo_p;

	select	nvl(max(a.ie_desc_prev),'N')
	into	ie_desc_prev_w
	from	parametro_contas_receber a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;

	select	nvl(sum(decode(b.ie_soma_diminui, 'S' , a.vl_tributo, 'D' , a.vl_tributo * -1,0)),0)
	into	vl_tributo_w
	from	titulo_receber_trib	a,
		tributo			b
	where	a.cd_tributo		= b.cd_tributo
	and	a.nr_seq_nota_fiscal is null
	and	a.nr_seq_mens_trib is null
	and	(a.ie_origem_tributo = 'D' or nvl(b.ie_incide_conta,'N') = 'N')
	and	a.nr_titulo		= nr_titulo_p;
	
	vl_liquido_w			:= vl_titulo_w + vl_tributo_w;

	if	(ie_desc_prev_w = 'S') then
	
		select	nvl(a.vl_desc_previsto,0)
		into	vl_desc_previsto_w
		from	titulo_receber a
		where	a.nr_titulo		= nr_titulo_p;

		vl_liquido_w		:= vl_liquido_w - vl_desc_previsto_w;
	end if;

	ds_retorno_w := vl_liquido_w;
	
elsif	(ie_tipo_dado_p = 'DC') then
	
	select	max(dt_mesano_referencia)
	into	dt_ref_con_paci_w
	from	conta_paciente a,
		titulo_receber b
	where	b.nr_titulo		= nr_titulo_p
	and	a.nr_interno_conta	= b.nr_interno_conta;

	if	(dt_ref_con_paci_w is null) then
		select	max(dt_mesano_referencia)
		into	dt_ref_con_paci_w
		from	conta_paciente a,
			titulo_receber b
		where	b.nr_titulo		= nr_titulo_p
		and	a.nr_seq_protocolo	= b.nr_seq_protocolo;		
	end if;

	ds_retorno_W	:= dt_ref_con_paci_w;

ELSIF	(ie_tipo_dado_p = 'DOC') THEN

	SELECT	a.nr_documento
	INTO	nr_documento_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w := nr_documento_w;

elsif	(ie_tipo_dado_p = 'VDP') then

	select	a.vl_desc_previsto
	into	vl_desc_previsto_w
	from	titulo_receber a
	where	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w	:= vl_desc_previsto_w;
elsif	(ie_tipo_dado_p = 'DE') then
	select	a.dt_emissao
	into	dt_emissao_w
	from	titulo_receber a
	where	a.nr_titulo		= nr_titulo_p;

	ds_retorno_w	:= to_char(dt_emissao_w,'dd/mm/yyyy');
elsif	(ie_tipo_dado_p	= 'VNC') then
	/* select	count(*)
	into	qt_baixa_w
	from	nota_credito_baixa a
	where	a.nr_titulo	= nr_titulo_p;

	if	(qt_baixa_w	> 0) then
		select	nvl(sum(a.vl_baixa),0)
		into	vl_nota_credito_w
		from	nota_credito_baixa a
		where	a.nr_titulo	= nr_titulo_p;
	end if; */

	if	(nvl(vl_nota_credito_w,0) = 0) then
		select	sum(a.vl_nota_credito)
		into	vl_nota_credito_w
		from	titulo_receber_nc a
		where	a.nr_titulo_rec	= nr_titulo_p
		and	a.nr_seq_liq	is null;
	end if;

	ds_retorno_w	:= nvl(vl_nota_credito_w, 0);
elsif	(ie_tipo_dado_p	= 'VAT') then

	select	nvl(sum(a.vl_rec_maior),0)
	into	vl_variacao_w
	from	titulo_receber_liq a
	where	a.nr_titulo		= nr_titulo_p
	and	a.cd_tipo_recebimento	= 17;

	ds_retorno_w	:= nvl(vl_variacao_w,0);
elsif	(ie_tipo_dado_p	= 'VPA') then

	select	nvl(sum(a.vl_descontos),0)
	into	vl_variacao_w
	from	titulo_receber_liq a
	where	a.nr_titulo		= nr_titulo_p
	and	a.cd_tipo_recebimento	= 15;

	ds_retorno_w	:= nvl(vl_variacao_w,0);
elsif	(ie_tipo_dado_p	= 'VP') then

	select	nvl(sum(a.vl_perdas),0)
	into	vl_perdas_w
	from	titulo_receber_liq a
	where	a.nr_titulo	= nr_titulo_p;

	ds_retorno_w	:= nvl(vl_perdas_w,0);
elsif	(ie_tipo_dado_p = 'VM') then

	select	nvl(sum(a.vl_rec_maior),0)
	into	vl_amaior_w
	from	titulo_receber_liq a
	where	a.nr_titulo	= nr_titulo_p;

	ds_retorno_w	:= nvl(vl_amaior_w,0);
elsif	(ie_tipo_dado_p = 'EP') then

	select	substr(pls_obter_dados_empresa(max(d.nr_seq_empresa),'D'),1,250)
	into	ds_retorno_w
	from	titulo_receber		a,
		pls_mensalidade		b,
		pls_contrato_pagador	c,
		pls_contrato_pagador_fin d
	where	a.nr_seq_mensalidade	= b.nr_sequencia
	and	b.nr_seq_pagador	= c.nr_sequencia
	and	d.nr_seq_pagador	= c.nr_sequencia
	and	b.dt_referencia	between nvl(d.dt_inicio_vigencia,b.dt_referencia) and nvl(d.dt_fim_vigencia,b.dt_referencia)
	and	a.nr_titulo		= nr_titulo_p;

elsif	(ie_tipo_dado_p	= 'VGR') then

	select	sum(a.vl_glosa)
	into	ds_retorno_w
	from	titulo_receber_liq a
	where	a.nr_titulo		= nr_titulo_p
	and	a.nr_seq_lote_hist_guia	is null;

elsif	(ie_tipo_dado_p	= 'VG') then

	select	sum(a.vl_glosa)
	into	ds_retorno_w
	from	titulo_receber_liq a
	where	a.nr_titulo		= nr_titulo_p;

elsif	(ie_tipo_dado_p = 'DV') then

	select	max(a.dt_vencimento)
	into	dt_vencimento_w
	from	alteracao_vencimento a
	where	a.nr_titulo	= nr_titulo_p;

	if	(dt_vencimento_w is null) then
		select	max(a.dt_vencimento)
		into	dt_vencimento_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_p;
	end if;

	ds_retorno_w	:= to_char(dt_vencimento_w,'dd/mm/yyyy');
	
elsif	(ie_tipo_dado_p = 'PPS') then
	select	a.nr_seq_pagador
	into	ds_retorno_w
	from	titulo_receber	a
	where	a.nr_titulo	= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'OBS') then
	select	b.ds_observacao
	into	ds_retorno_w
	from	titulo_receber		a,
		pls_mensalidade		b
	where	a.nr_seq_mensalidade	= b.nr_sequencia
	and	a.nr_titulo		= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'OAV') then
	select	max(dt_alteracao)
	into	dt_alteracao_w
	from	alteracao_vencimento a
	where	a.nr_titulo	= nr_titulo_p;
	
	select	max(a.ds_observacao)
	into	ds_retorno_w
	from	alteracao_vencimento a 
	where	a.nr_titulo = nr_titulo_p
	and	a.dt_alteracao = dt_alteracao_w;
elsif	(ie_tipo_dado_p = 'SEA') then

	SELECT	a.nr_atendimento
	INTO	nr_atendimento_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(nr_atendimento_w is not null) then

		select	max(cd_setor_atendimento)
		into	ds_retorno_w
		from	atend_paciente_unidade
		where	nr_atendimento	= nr_atendimento_w
		and	nr_seq_interno 	= Obter_Atepacu_paciente(nr_atendimento_w, 'A');
	end if;
elsif	(ie_tipo_dado_p = 'TPJ') then
	
	select	MAX(obter_desc_tipo_pj(b.cd_tipo_pessoa,'D'))
	into	ds_retorno_w
	from	pessoa_juridica b,
		titulo_receber a
	where	a.cd_cgc		= b.cd_cgc
	and	a.nr_titulo	= nr_titulo_p;

elsif	(ie_tipo_dado_p = 'PARC') then

	select	max(nr_interno_conta)
	into	nr_interno_conta_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;

	if	(nr_interno_conta_w is not null) then
		select	count(*)
		into	qt_total_w
		from	titulo_receber
		where	nr_interno_conta	= nr_interno_conta_w;

		select	count(*)
		into	qt_parcela_w
		from	titulo_receber
		where	nr_interno_conta	= nr_interno_conta_w
		and	nr_titulo		> nr_titulo_p;


		ds_retorno_w	:= to_char(qt_total_w - qt_parcela_w) || '/' || to_char(qt_total_w);
	else
		ds_retorno_w	:= '1/1';
	end if;

elsif	(ie_tipo_dado_p = 'RCC') then

	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;

	if	(nr_atendimento_w is not null) then
		select	max(IE_RESPONSAVEL_CONTA)
		into	ds_retorno_w
		from	AGENDA_PACIENTE_INF_ADIC b,
			AGENDA_PACIENTE a
		where	a.nr_atendimento	= nr_atendimento_w
		and	a.nr_sequencia		= b.nr_seq_agenda;
	end if;

ELSIF	(ie_tipo_dado_p = 'NFP') THEN
	SELECT	somente_numero(max(b.nr_nota_fiscal))
	INTO	nr_nota_fiscal_w
	FROM	titulo_receber	a,
		nota_fiscal	b
	WHERE	a.nr_seq_protocolo	= b.nr_seq_protocolo
	and	a.nr_titulo		= nr_titulo_p;

	if	(nvl(nr_nota_fiscal_w,'0') = '0') then
		select	somente_numero(max(c.nr_nota_fiscal))
		into	nr_nota_fiscal_w
		from	nota_fiscal c,
			conta_paciente b,
			titulo_receber a
		where	b.nr_interno_conta	= c.nr_interno_conta
		and	a.nr_seq_protocolo	= b.nr_seq_protocolo
		and	a.nr_titulo		= nr_titulo_p;

		if	(nvl(nr_nota_fiscal_w,'0') = '0') then
			select	somente_numero(max(a.nr_nota_fiscal))
			into	nr_nota_fiscal_w
			from	titulo_receber a
			where	a.nr_titulo	= nr_titulo_p;

			if	(nvl(nr_nota_fiscal_w,'0') = '0') then
				select	somente_numero(max(c.nr_nota_fiscal))
				into	nr_nota_fiscal_w
				from	nota_fiscal c,
					protocolo_convenio b,
					titulo_receber a
				where	b.nr_seq_lote_protocolo	= c.nr_seq_lote_prot
				and	a.nr_seq_protocolo	= b.nr_seq_protocolo
				and	a.nr_titulo		= nr_titulo_p;
			end if;
		end if;
	end if;

	if	(nr_nota_fiscal_w = '0') then
		nr_nota_fiscal_w	:= null;
	end if;

	ds_retorno_w			:= nr_nota_fiscal_w;
elsif	(ie_tipo_dado_p = 'PR') then
	select	a.nr_seq_mensalidade
	into	nr_seq_mensalidade_w
	from	titulo_receber a
	where	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
		select	max(nr_seq_plano)
		into	nr_seq_plano_w
		from	pls_mensalidade_segurado
		where	nr_seq_mensalidade	= nr_seq_mensalidade_w;
	else
		select	max(nr_seq_pagador)
		into	nr_seq_pagador_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_p;
		
		if	(nr_seq_pagador_w is not null) then
			select	max(a.nr_seq_plano)
			into	nr_seq_plano_w
			from	pls_contrato_plano	a,
				pls_contrato		c,
				pls_contrato_pagador	b
			where	c.nr_sequencia		= b.nr_seq_contrato
			and	a.nr_seq_contrato	= c.nr_sequencia
			and	b.nr_sequencia		= nr_seq_pagador_w;
		end if;
	end if;
	
	if	(nr_seq_plano_w is not null) then
		select	substr(ds_plano,1,80)
		into	ds_retorno_w
		from	pls_plano
		where	nr_sequencia	= nr_seq_plano_w;
	end if;
elsif	(ie_tipo_dado_p = 'VL') then
	select	max(a.vl_titulo)
	into	vl_titulo_w
	from	titulo_receber a
	where	a.nr_titulo	= nr_titulo_p;

	select	sum(decode(b.ie_soma_diminui,'S',a.vl_tributo,'D',a.vl_tributo * -1,0))
	into	vl_tributo_w
	from	tributo b,
		titulo_receber_trib a
	where	a.nr_titulo		= nr_titulo_p
	and	a.cd_tributo		= b.cd_tributo
	and	a.nr_seq_nota_fiscal	is null
	and	(a.ie_origem_tributo = 'D' or nvl(b.ie_incide_conta,'N') = 'N');

	ds_retorno_w	:= vl_titulo_w + vl_tributo_w;
elsif	(ie_tipo_dado_p = 'CA') then
	select	nvl(pls_carteira_pag_se_seg(a.nr_seq_pagador,a.cd_pessoa_fisica),' ')
	into	ds_retorno_w
	from    titulo_receber a
	where  	a.nr_titulo		= nr_titulo_p;
	
elsif	(ie_tipo_dado_p = 'VTD') then
	select	nvl(sum(b.vl_titulo), 0)
	into	ds_retorno_w
	from	titulo_receber_desdob b
	where	b.nr_titulo = nr_titulo_p;
	
elsif	(ie_tipo_dado_p = 'PRR') then
	select	a.nr_seq_mensalidade
	into	nr_seq_mensalidade_w
	from	titulo_receber a
	where	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
		select	max(nr_seq_plano)
		into	nr_seq_plano_w
		from	pls_mensalidade_segurado
		where	nr_seq_mensalidade	= nr_seq_mensalidade_w;
	else
		select	max(nr_seq_pagador)
		into	nr_seq_pagador_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_p;
		
		if	(nr_seq_pagador_w is not null) then
			select	max(a.nr_seq_plano)
			into	nr_seq_plano_w
			from	pls_contrato_plano	a,
				pls_contrato		c,
				pls_contrato_pagador	b
			where	c.nr_sequencia		= b.nr_seq_contrato
			and	a.nr_seq_contrato	= c.nr_sequencia
			and	b.nr_sequencia		= nr_seq_pagador_w;
		end if;
	end if;
	
	if	(nr_seq_plano_w is not null) then
    --729339 = -regulamentado
		select	substr(obter_valor_dominio(2157,ie_regulamentacao),1,80) || decode(ie_regulamentacao,'A',null,obter_desc_expressao(729339))
		into	ds_retorno_w
		from	pls_plano
		where	nr_sequencia	= nr_seq_plano_w;
	end if;
	
	if	(ds_retorno_w is null) then
    --729343 = Nao pertencente a contrato de plano de saude
		ds_retorno_w	:= obter_desc_expressao(729343);
	end if;
elsif	(ie_tipo_dado_p = 'ECC') then
	select	a.nr_seq_mensalidade
	into	nr_seq_mensalidade_w
	from	titulo_receber a
	where	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
    --289591 = Estipulante PF / 289592 = Estipulante PJ
		select	decode(max(c.cd_cgc_estipulante),null, obter_desc_expressao(289591),obter_desc_expressao(289592))
		into	ds_retorno_w
		from	pls_contrato		c,
			pls_contrato_pagador	b,
			pls_mensalidade		a
		where	c.nr_sequencia		= b.nr_seq_contrato
		and	a.nr_seq_pagador	= b.nr_sequencia
		and	a.nr_sequencia		= nr_seq_mensalidade_w;
	else
		select	max(nr_seq_pagador)
		into	nr_seq_pagador_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_p;
		
		if	(nr_seq_pagador_w is not null) then
      --289591 = Estipulante PF / 289592 = Estipulante PJ
			select	decode(max(c.cd_cgc_estipulante),null,obter_desc_expressao(289591),obter_desc_expressao(289592))
			into	ds_retorno_w
			from	pls_contrato		c,
				pls_contrato_pagador	b
			where	c.nr_sequencia	= b.nr_seq_contrato
			and	b.nr_sequencia	= nr_seq_pagador_w;
		end if;
	end if;
	
	if	(ds_retorno_w is null) then
    --729343 = Nao pertencente a contrato de plano de saude
		ds_retorno_w	:= obter_desc_expressao(729343);
	end if;
elsif	(ie_tipo_dado_p = 'CS') then
	select	cd_serie
	into	ds_retorno_w
	from	titulo_receber
	where	nr_titulo = nr_titulo_p;
elsif	(ie_tipo_dado_p = 'MCC') then
	select	max(d.nr_seq_motivo_rescisao)
	into	ds_retorno_w
	from    pls_contrato         	d, 
		pls_contrato_pagador 	c,
		pls_mensalidade      	b,
		titulo_receber		a
	where   b.nr_sequencia  	= a.nr_seq_mensalidade
	and	b.nr_seq_pagador	= c.nr_sequencia
	and	c.nr_seq_contrato	= d.nr_sequencia
	and	a.nr_titulo 		= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'PDJ') then
	select	cd_tipo_taxa_juro,
		NVL(tx_juros,0)
	into	cd_tipo_taxa_juro_w,
		tx_juros_w
	from	titulo_receber
	where	nr_titulo		= nr_titulo_p;
	
	select	ie_tipo_taxa
	into	ie_tipo_taxa_w
	from	tipo_taxa
	where	cd_tipo_taxa	= cd_tipo_taxa_juro_w;
	
	if	(ie_tipo_taxa_w = 'A') then
		pr_juros_w	:= dividir_sem_round(tx_juros_w, 365);
	elsif	(ie_tipo_taxa_w = 'M') then
		pr_juros_w	:= dividir_sem_round(tx_juros_w, 30);
	elsif	(ie_tipo_taxa_w = 'D') then
		pr_juros_w	:= tx_juros_w;
	end if;
	
	ds_retorno_w := pr_juros_w;
elsif	(ie_tipo_dado_p = 'TXM') then
	select	NVL(tx_multa,0)
	into	tx_multa_w
	from	titulo_receber
	where	nr_titulo		= nr_titulo_p;
	
	ds_retorno_w := tx_multa_w;

elsif	(ie_tipo_dado_p = 'NRC') then

	SELECT	a.nr_seq_mensalidade
	INTO	nr_seq_mensalidade_w
	FROM	titulo_receber a
	WHERE	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_mensalidade_w is not null) then
		select	max(b.nr_seq_contrato)
		into	nr_seq_contrato_w
		from	pls_mensalidade		a,
			pls_contrato_pagador	b
		where	a.nr_seq_pagador	= b.nr_sequencia
		and	a.nr_sequencia		= nr_seq_mensalidade_w;

		if	(nr_seq_contrato_w is not null) then
			select	nr_contrato
			into	ds_retorno_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_w;
		end if;
	end if;
elsif	(ie_tipo_dado_p = 'NIC') then
	select	max(nr_interno_conta)
	into	nr_interno_conta_w
	from	titulo_receber
	where	nr_titulo = nr_titulo_p;
	
	ds_retorno_w := nr_interno_conta_w;
elsif	(ie_tipo_dado_p = 'DTFB') then
	select	substr(max(ds_transacao),1,255)
	into	ds_retorno_w
	from	titulo_receber a,
		transacao_financeira b
	where	A.nr_seq_trans_fin_baixa = b.nr_sequencia
	and	a.nr_titulo = nr_titulo_p;
	
elsif	(ie_tipo_dado_p = 'CF') then
	select  max(cd_conta_financ)
	into	ds_retorno_w
	from	titulo_receber_classif
	where	nr_titulo = nr_titulo_p;
elsif	(ie_tipo_dado_p = 'DCL') then
	select  max(ds_classe)
	into	ds_retorno_w
	from	titulo_receber a,
		CLASSE_TITULO_RECEBER b
	where	a.NR_SEQ_CLASSE = b.nr_sequencia
	and	a.nr_titulo = nr_titulo_p;
elsif	(ie_tipo_dado_p = 'VRP') then
	select  sum(b.vl_baixa)
	into	ds_retorno_w
	from	perda_contas_receber a,
		perda_contas_receb_baixa b,
		fin_tipo_baixa_perda c
	where	a.nr_sequencia = b.nr_seq_perda
	and	b.nr_seq_tipo_baixa = c.nr_sequencia
	and	c.ie_tipo_consistencia = 0
	and	a.nr_titulo = nr_titulo_p;
elsif	(ie_tipo_dado_p = 'NP') then
	select  max(a.nr_seq_protocolo)
	into	ds_retorno_w
	from	titulo_receber a
	where	a.nr_titulo = nr_titulo_p;	
elsif	(ie_tipo_dado_p = 'VLG') then	
	select	nvl(sum(a.vl_amenor),0)
	into	ds_retorno_w
	from	convenio_retorno_item a
	where	a.nr_titulo	= nr_titulo_p
	and	a.nr_seq_retorno	= 
			(	select	max(b.nr_seq_retorno) 
				from 	convenio_retorno_item b 
				where 	b.nr_titulo 		= nr_titulo_p 
				and 	a.nr_interno_conta 	= b.nr_interno_conta 
				and 	a.cd_autorizacao 	= b.cd_autorizacao);
				
elsif	(ie_tipo_dado_p = 'DRP') then	
	select	max(nr_seq_pagador)
	into	nr_seq_pagador_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;
	
	if	(nr_seq_pagador_w is not null) then
		select	max(dt_rescisao),
			max(nr_seq_contrato)
		into	ds_retorno_w,
			nr_seq_contrato_w
		from	pls_contrato_pagador
		where  	nr_sequencia	= nr_seq_pagador_w;
		
		if	(ds_retorno_w is not null) then
			select	max(dt_rescisao_contrato)
			into	ds_retorno_w
			from	pls_contrato
			where	nr_sequencia = nr_seq_contrato_w;
		end if;
	end if;
elsif	(ie_tipo_dado_p = 'TP') then
	select	obter_valor_dominio(703,max(cd_tipo_portador))
	into	ds_retorno_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'NTP') then
	select	b.ds_portador
	into	ds_retorno_w
	from	titulo_receber a,
		portador b
	where	a.cd_portador	= b.cd_portador
	and	nr_titulo	= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'DEF') then
	select	to_char(dt_emissao,'dd/mm/yyyy')
	into	ds_retorno_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'NTE') then
	select	nr_titulo_externo
	into	ds_retorno_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;	
elsif	(ie_tipo_dado_p = 'TPR') then
	select	substr(max(d.ds_tipo_baixa),1,255)
	into	ds_retorno_w
	from	titulo_receber a,
		perda_contas_receber b,
		perda_contas_receb_baixa c,
		fin_tipo_baixa_perda d
	where	a.nr_titulo	= b.nr_titulo
	and	c.nr_seq_perda	= b.nr_sequencia
	and	c.nr_seq_tipo_baixa = d.nr_sequencia
	and	a.nr_titulo	= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'CDO') then
	select	max(a.cd_cooperativa)
	into	ds_retorno_w
	from	pls_congenere	a,
		titulo_receber	b
	where	a.cd_cgc	= b.cd_cgc
	and	b.nr_titulo	= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'DRPC') then	
	select	max(nr_seq_pagador)
	into	nr_seq_pagador_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;
	
	if	(nr_seq_pagador_w is not null) then
		select	max(dt_rescisao),
			max(nr_seq_contrato)
		into	ds_retorno_w,
			nr_seq_contrato_w
		from	pls_contrato_pagador
		where  	nr_sequencia	= nr_seq_pagador_w;
		
		if	(ds_retorno_w is null) then
			select	max(dt_rescisao_contrato)
			into	ds_retorno_w
			from	pls_contrato
			where	nr_sequencia = nr_seq_contrato_w;
		end if;
	end if;
elsif	(ie_tipo_dado_p = 'OT') then	
	select	ie_origem_titulo
	into	ds_retorno_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;
elsif	(ie_tipo_dado_p = 'DAV') then

	select	max(a.nr_sequencia)
	into	nr_seq_alter_vencto_w
	from	alteracao_vencimento a
	where	a.nr_titulo = nr_titulo_p;
	
	if ( nvl(nr_seq_alter_vencto_w,0) <> 0) then
	
		select	max(a.dt_alteracao)
		into	dt_alteracao_vencto_w
		from	alteracao_vencimento a
		where	a.nr_titulo 	= nr_titulo_p
		and		a.nr_sequencia	= nr_seq_alter_vencto_w;
		
		begin
			ds_retorno_w := to_char(dt_alteracao_vencto_w,'dd/mm/yyyy');
		exception when others then
			ds_retorno_w := null;
		end;
	
	end if;

end if;

return ds_retorno_w;

end obter_dados_titulo_receber;
/