create or replace
function pls_obter_se_titular_obito
			(	nr_seq_segurado_p	number,
				ie_tipo_obito_p		varchar2,
				ie_tipo_retorno_p	varchar2)
			return varchar2 is
			
/*ie_tipo_obito_p
M - Manter no contrato atual
N  - Gerar novo contrato
*/

/*ie_tipo_retorno_p
P - Possui beneficio
S - Retorna a sequencia da regra
Q - Quantidade de anos de validade
*/
			
ds_retorno_w		varchar2(10);
qt_registros_w		number(10);
dt_contrato_w		pls_contrato.dt_contrato%type;
nr_seq_contrato_w	number(10);
nr_seq_intercambio_w	number(10);

begin

select	nr_seq_contrato,
	nr_seq_intercambio
into	nr_seq_contrato_w,
	nr_seq_intercambio_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

if	(nr_seq_contrato_w is not null) then
	select dt_contrato
	into dt_contrato_w
	from pls_contrato
	where nr_sequencia = nr_seq_contrato_w;
	
elsif	(nr_seq_intercambio_w is not null) then
	select dt_inclusao
	into dt_contrato_w
	from pls_intercambio
	where nr_sequencia = nr_seq_intercambio_w;	
else
	dt_contrato_w	:= sysdate;
end if;

select	count(*)
into	qt_registros_w
from	PLS_SCA_REGRA_OBITO	c,
	pls_sca_vinculo		b,
	pls_plano		a
where	b.nr_seq_plano		= a.nr_sequencia
and	c.nr_seq_sca		= a.nr_sequencia
and	b.nr_seq_segurado	= nr_seq_segurado_p
and	((b.dt_fim_vigencia is null) or (b.dt_fim_vigencia > sysdate))
and	b.dt_liberacao is not null
and	dt_contrato_w between trunc(nvl(c.dt_contrato_inicial, dt_contrato_w),'dd') and fim_dia(nvl(c.dt_contrato_final, dt_contrato_w))
and	c.ie_acao_seguro	= ie_tipo_obito_p;


if	(ie_tipo_retorno_p = 'P') then
	if	(qt_registros_w > 0) then
		ds_retorno_w	:= 'S';
	else
		ds_retorno_w	:= 'N';
	end if;	
elsif	(ie_tipo_retorno_p = 'S') then
	if	(qt_registros_w > 0) then
		select	to_char(max(c.nr_sequencia))	
		into	ds_retorno_w
		from	PLS_SCA_REGRA_OBITO	c,
			pls_sca_vinculo		b,
			pls_plano		a	
		where	b.nr_seq_plano		= a.nr_sequencia
		and	c.nr_seq_sca		= a.nr_sequencia
		and	b.nr_seq_segurado	= nr_seq_segurado_p
		and	((b.dt_fim_vigencia is null) or (b.dt_fim_vigencia > sysdate))
		and	b.dt_liberacao is not null
		and	dt_contrato_w between trunc(nvl(c.dt_contrato_inicial, dt_contrato_w),'dd') and fim_dia(nvl(c.dt_contrato_final, dt_contrato_w))
		and	c.ie_acao_seguro	= ie_tipo_obito_p;
	end if;
elsif	(ie_tipo_retorno_p = 'Q') then
	if	(qt_registros_w > 0) then
	select	to_char(max(c.QT_VALIDADE_CONTRATO))
		into	ds_retorno_w
		from	PLS_SCA_REGRA_OBITO	c,
			pls_sca_vinculo		b,
			pls_plano		a
		where	b.nr_seq_plano		= a.nr_sequencia
		and	c.nr_seq_sca		= a.nr_sequencia
		and	b.nr_seq_segurado	= nr_seq_segurado_p
		and	((b.dt_fim_vigencia is null) or (b.dt_fim_vigencia > sysdate))
		and	b.dt_liberacao is not null
    and	dt_contrato_w between trunc(nvl(c.dt_contrato_inicial, dt_contrato_w),'dd') and fim_dia(nvl(c.dt_contrato_final, dt_contrato_w))
		and	c.ie_acao_seguro	= ie_tipo_obito_p;
	end if;
end if;
	
return	ds_retorno_w;

end pls_obter_se_titular_obito;
/
