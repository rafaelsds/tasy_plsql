create or replace
function LAB_Obter_Equipamento(cd_equipamento_p	number) 
					return varchar2 is

ds_retorno_w	varchar2(50);

begin

select nvl(max(ds_equipamento),0)
into	ds_retorno_w
from equipamento_lab
where cd_equipamento = cd_equipamento_p;

return ds_retorno_w;

end;
/