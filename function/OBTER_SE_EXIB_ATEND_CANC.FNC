create or replace
function obter_se_exib_atend_canc(cd_estabelecimento_p		number)
 		    	return varchar2 is

ie_cancelado_censo_w	varchar2(1);

begin

select	nvl(max(ie_cancelado_censo),'S')
into	ie_cancelado_censo_w
from	parametro_atendimento
where	cd_estabelecimento = cd_estabelecimento_p;

return	ie_cancelado_censo_w;

end obter_se_exib_atend_canc;
/