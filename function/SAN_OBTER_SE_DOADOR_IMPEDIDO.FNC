create or replace
function san_obter_se_doador_impedido( cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);
begin

if (cd_pessoa_fisica_p is not null) then

	select 	decode(count(*),0,'N','S')
	into	ie_retorno_w
	from 	san_impedimento c, 
		san_doacao_impedimento b, 
		san_doacao a 
	where 	a.nr_sequencia 		= b.nr_seq_doacao  
	and 	b.nr_seq_impedimento 	= c.nr_sequencia 
	and 	a.cd_pessoa_fisica 	= cd_pessoa_fisica_p
	and 	a.ie_avaliacao_final 	<> 'A'
	and	c.ie_definitivo = 'S';
	
	
	if (ie_retorno_w = 'N') then
	
		select 	decode(count(*),0,'N','S')
		into	ie_retorno_w
		from 	san_impedimento c, 
			san_questionario b, 
			san_doacao a 
		where 	a.nr_sequencia 		= b.nr_seq_doacao  
		and 	b.nr_seq_impedimento 	= c.nr_sequencia 
		and 	a.cd_pessoa_fisica 	= cd_pessoa_fisica_p
		and 	a.ie_avaliacao_final 	<> 'A'
		and	c.ie_definitivo = 'S'
		and     nvl(b.ie_impede_doacao,'N') = 'S' ;

	end if;
	
	if (ie_retorno_w = 'N') then
	
		select 	decode(count(*),0,'N','S')
		into	ie_retorno_w
		from 	san_impedimento_sorologia b, 
			san_doacao a 
		where 	a.nr_sequencia 		= b.nr_seq_doacao 
		and 	a.cd_pessoa_fisica 	= cd_pessoa_fisica_p
		and 	a.ie_avaliacao_final 	<> 'A'
		and	nvl(b.ie_inapto_definitivo,'N') = 'S';
		
	end if;

end if;

return	ie_retorno_w;

end san_obter_se_doador_impedido;
/
