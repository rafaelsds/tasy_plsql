create or replace
function via_obter_se_obriga_compl(nr_seq_classif_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);			
begin

select	nvl(ie_obriga_complemento,'N')
into	ds_retorno_w
from	via_classif_desp
where	nr_sequencia = nr_seq_classif_p;


return	ds_retorno_w;

end via_obter_se_obriga_compl;
/