create or replace 
function Obter_Cod_Proc_Interno_conv	(nr_seq_proc_interno_p	NUMBER, 
					cd_convenio_p		NUMBER, 
					cd_categoria_p		NUMBER, 
					dt_vigencia_p		date, 
					cd_estabelecimento_p	NUMBER) 
					return NUMBER is 
 
cd_edicao_amb_w		NUMBER(6,0); 
cd_procedimento_w	NUMBER(15,0);
dt_vigencia_w		date;
 
begin

dt_vigencia_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(dt_vigencia_p, sysdate));
 
select	obter_edicao_amb(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, dt_vigencia_p)
into	cd_edicao_amb_w 
from 	dual; 
 
select	nvl(max(a.cd_procedimento),0) 
into	cd_procedimento_w 
from	proc_interno_conv a 
where	a.nr_seq_proc_interno 			= nr_seq_proc_interno_p 
and	nvl(a.cd_convenio, cd_convenio_p)	= cd_convenio_p 
and	nvl(a.cd_edicao_amb, nvl(cd_edicao_amb_w,0))	= nvl(cd_edicao_amb_w,0)
and	dt_vigencia_w between nvl(a.dt_inicio_vigencia, dt_vigencia_w) and nvl(a.dt_final_vigencia, dt_vigencia_w); 


if	(cd_procedimento_w = 0) then 
	select	b.cd_procedimento 
	into	cd_procedimento_w 
	from	proc_interno b 
	where	b.nr_sequencia = nr_seq_proc_interno_p; 
end if; 
 
 
return	cd_procedimento_w; 
 
end	Obter_Cod_Proc_Interno_conv;
/