create or replace
Function Obter_Desconto_MatProc
		(nr_seq_matproc_p		number,
		ie_matproc_p			varchar2)
		return number is

vl_desconto_w		number(15,2)	:= 0;
nr_interno_conta_w	number(10,0);
ie_tipo_desconto_w	number(10,0);

nr_seq_proc_pacote_w	number(10,0);
nr_seq_item_w		number(10,0);
ie_proc_mat_w		number(1);
vl_desc_proc_w		number(15,2);
vl_desc_mat_w		number(15,2);


Cursor C01 is
	select	nr_sequencia,
		1 ie_proc_mat
	from	procedimento_paciente
	where	nr_seq_proc_pacote = nvl(nr_seq_proc_pacote_w,0)
	and 	nr_interno_conta = nr_interno_conta_w
	union all
	select	nr_sequencia,
		2 ie_proc_mat
	from	material_atend_paciente
	where	nr_seq_proc_pacote = nvl(nr_seq_proc_pacote_w,0)	
	and 	nr_interno_conta = nr_interno_conta_w
	order by ie_proc_mat;

begin

if	(ie_matproc_p	= 'P') then
	begin
	
	select	max(nr_interno_conta),
		max(nr_seq_proc_pacote)
	into	nr_interno_conta_w,
		nr_seq_proc_pacote_w
	from	procedimento_paciente
	where	nr_sequencia	= nr_seq_matproc_p;

	select	nvl(max(ie_tipo_desconto),0)
	into	ie_tipo_desconto_w
	from	conta_paciente_desconto
	where	nr_interno_conta	= nr_interno_conta_w;
	
	if	(ie_tipo_desconto_w = 7) then
		select	nvl(sum(vl_desconto),0)
		into	vl_desconto_w
		from	conta_paciente_desc_item
		where	nr_seq_proc_pacote	= nr_seq_matproc_p;
	else
		
		if	(nvl(nr_seq_proc_pacote_w,0) = nr_seq_matproc_p) then
		
			vl_desc_mat_w	:= 0;
			vl_desc_proc_w	:= 0;
			vl_desconto_w	:= 0;
		
			open C01;
			loop
			fetch C01 into	
				nr_seq_item_w,
				ie_proc_mat_w;
			exit when C01%notfound;
				begin

				if	(ie_proc_mat_w = 1) then
				
					select	nvl(sum(vl_procedimento),0)
					into	vl_desc_proc_w
					from	proc_paciente_valor
					where	ie_tipo_valor		= 3
					and	nr_seq_procedimento	= nr_seq_item_w;
					
					vl_desconto_w:= vl_desconto_w + vl_desc_proc_w;
				
				else				
					select	nvl(sum(vl_material),0)
					into	vl_desc_mat_w
					from	mat_atend_paciente_valor
					where	ie_tipo_valor		= 3
					and	nr_seq_material		= nr_seq_item_w;
					
					vl_desconto_w:= vl_desconto_w + vl_desc_mat_w;
	
				end if;
				
				end;					
			end loop;
			close C01;
		
		else
	
			select	nvl(sum(vl_procedimento),0)
			into	vl_desconto_w
			from	proc_paciente_valor
			where	ie_tipo_valor		= 3
			and	nr_seq_procedimento	= nr_seq_matproc_p;
			
		end if;
	end if;

	end;
elsif	(ie_matproc_p	= 'M') then
	begin

	select	nvl(sum(vl_material),0)
	into	vl_desconto_w
	from	mat_atend_paciente_valor
	where	ie_tipo_valor		= 3
	and	nr_seq_material		= nr_seq_matproc_p;

	end;
end if;

return	vl_desconto_w;

end Obter_Desconto_MatProc;
/