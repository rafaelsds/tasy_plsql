create or replace
function lab_converte_path_storage(	ds_arquivo_p	varchar2)
					return varchar2 is

ds_retorno_w			varchar2(256);
cd_uuid_aux_w			file_storage_path.cd_uuid%type;
cd_uuid_w				file_storage_path.cd_uuid%type;
ds_path_aux_w			varchar2(256);
ds_path_w				file_storage_path.ds_path%type;
ds_uploaded_file_name_w		varchar2(256);
ie_separator_w			varchar2(1);
		
begin

if (instr(ds_arquivo_p, 'tasy-storage') > 0) then	
	ds_path_aux_w	:= substr(ds_arquivo_p, instr(ds_arquivo_p,'/',1,2), instr(substr(ds_arquivo_p, instr(ds_arquivo_p, '/',1,2),length(ds_arquivo_p)),'/',-1,1));	
	
	cd_uuid_aux_w	:= replace(substr(ds_path_aux_w, instr(ds_path_aux_w,'/',1,1), instr(substr(ds_path_aux_w, instr(ds_path_aux_w, '/',1,1),length(ds_path_aux_w)),'/',1,2)),'/','');
	
	cd_uuid_w	:= substr(cd_uuid_aux_w, instr(cd_uuid_aux_w,'.',1,1) + 1, length(cd_uuid_aux_w));
	
	if (instr(ds_path_aux_w,'/',1,3) > 0) then
		ds_path_aux_w	:= substr(ds_path_aux_w, instr(ds_path_aux_w,'/',1,2), instr(substr(ds_path_aux_w, instr(ds_path_aux_w, '/',1,2),length(ds_path_aux_w)),'/',-1,1)); 
		
	else
		ds_path_aux_w	:= null;
	end if;
	
	ds_uploaded_file_name_w	:= substr(ds_arquivo_p, instr(ds_arquivo_p,'/',-1,1) + 1,length(ds_arquivo_p));
	ds_uploaded_file_name_w	:= substr(ds_uploaded_file_name_w, 1, instr(ds_uploaded_file_name_w, '?',-1,1) -1);
	
	begin
		select	ds_path
		into	ds_path_w
		from	file_storage_path
		where	cd_uuid	= cd_uuid_w;
	exception
	when others then
		ds_path_w	:= null;
	end;
	
	if (ds_path_w is not null) then	
		if (instr(ds_path_w, '\') > 0) then --'
			ie_separator_w	:= '\'; --'
			ds_path_aux_w	:= replace(ds_path_aux_w, '/', '\'); --'
		else
			ie_separator_w	:= '/';
			ds_path_aux_w	:= replace(ds_path_aux_w, '/', '\'); --'
		end if;
		
		if (ds_path_aux_w is null) then
			ds_retorno_w	:= ds_path_w || ie_separator_w || ds_uploaded_file_name_w;		
		else
			ds_retorno_w	:= ds_path_w || ds_path_aux_w || ds_uploaded_file_name_w;
		end if;
	end if;
else 
	ds_retorno_w	:= ds_arquivo_p; 	
end if;

return	ds_retorno_w;

end lab_converte_path_storage;
/
