create or replace
function Obter_Prof_Cirurgia_Func (	nr_cirurgia_p	number,
					ie_funcao_p	varchar2) 
					return	varchar2 is

ds_profissional_w	varchar2(4000):= '';
nm_profissional_w	varchar2(60):='';

/* 'C' - Circulante
'A' - Auxiliar
'I - Instrumentador 
O - Outros */

cursor c01 is
	select	obter_initcap(substr(obter_nome_pf(cd_pessoa_fisica),1,60)) nm_profissional
	from	cirurgia_participante a, funcao_medico b
	where	a.nr_cirurgia = nr_cirurgia_p
	and 	a.ie_funcao = to_char(b.cd_funcao)
	and	IE_INSTRUMENTADOR	= 'S'
	and	ie_funcao_p		= 'I'
	union
	select	obter_initcap(substr(obter_nome_pf(cd_pessoa_fisica),1,60)) nm_profissional
	from	cirurgia_participante a, funcao_medico b
	where	a.nr_cirurgia 	= nr_cirurgia_p
	and 	a.ie_funcao 	= to_char(b.cd_funcao)
	and	IE_INSTRUMENTADOR	= 'N'
	and	ie_medico		= 'N'
	and	IE_ANESTESISTA 		= 'N'
	and	IE_AUXILIAR    		= 'N'
	and	ie_funcao_p		= 'C'
	union
	select	obter_initcap(substr(obter_nome_pf(cd_pessoa_fisica),1,60)) nm_profissional
	from	cirurgia_participante a, funcao_medico b
	where	a.nr_cirurgia = nr_cirurgia_p
	and 	a.ie_funcao = to_char(b.cd_funcao)
	and	IE_INSTRUMENTADOR	= 'N'
	and	(ie_medico		= 'S' or IE_ANESTESISTA 		= 'S' or IE_AUXILIAR    		= 'S')
	and	ie_funcao_p		= 'O'
	order by	1 desc;

begin

if	(nvl(nr_cirurgia_p,0) > 0) then
	open c01;
	loop

		fetch c01 into	nm_profissional_w;
			exit when c01%notfound;

		if	(nm_profissional_w is not null) then 
			if	(ds_profissional_w is null) then
				ds_profissional_w	:=	nm_profissional_w;
			else
				ds_profissional_w	:=	nm_profissional_w || ', ' || ds_profissional_w;
			end if;
		end if;

	end loop;
	close c01;
end if;	

return	ds_profissional_w;

end Obter_Prof_Cirurgia_Func;
/