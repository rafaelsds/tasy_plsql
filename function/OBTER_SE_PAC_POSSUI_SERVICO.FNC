create or replace
function obter_se_pac_possui_servico(
		nr_atendimento_p	number,
		nr_seq_servico_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1) := 'N';

begin

if ((nr_atendimento_p is not null) and (nr_seq_servico_p is not null)) then
	begin
	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	atendimento_servico a
	where	a.nr_atendimento = nr_atendimento_p
	and	a.nr_seq_servico = nr_seq_servico_p;
	end;
end if;

return	ds_retorno_w;

end obter_se_pac_possui_servico;
/