create or replace
function obter_dados_medic_uso_continuo (nr_seq_medicamento_p	number,
					ie_opcao_p		varchar,
					ie_dado_p		varchar2)
					return			varchar2 is

qt_dose_w			varchar2(15);
cd_unidade_medida_dose_w	varchar2(30);
ds_prescricao_w			varchar2(60);
ie_via_w			varchar2(05);
ds_retorno_w			varchar2(100);
nr_seq_motivo_susp_w		number(10);
nr_seq_medic_subst_w		number(10);
ds_material_w			varchar2(100);
DS_MOTIVO_SUSP_w		varchar2(100);
	
begin

if	(ie_opcao_p = 'N') then

	select	a.qt_dose,
		a.cd_unidade_medida,
		b.ds_prescricao,
		a.ie_via_aplicacao,
		a.nr_seq_motivo_susp,
		a.nr_seq_medic_subst
	into	qt_dose_w,
		cd_unidade_medida_dose_w,
		ds_prescricao_w,
		ie_via_w,
		nr_seq_motivo_susp_w,
		nr_seq_medic_subst_w
	from	intervalo_prescricao b,
		medic_uso_continuo a
	where	a.nr_sequencia		= nr_seq_medicamento_p
	and	a.cd_intervalo	= b.cd_intervalo(+);

elsif	(ie_opcao_p = 'H') then
	select	a.qt_dose,
		a.cd_unidade_medida,
		b.ds_prescricao,
		a.ie_via_aplicacao,
		a.nr_seq_motivo_susp,
		a.nr_seq_medic_subst
	into	qt_dose_w,
		cd_unidade_medida_dose_w,
		ds_prescricao_w,
		ie_via_w,
		nr_seq_motivo_susp_w,
		nr_seq_medic_subst_w
	from	intervalo_prescricao b,
		medic_uso_continuo_hist a
	where	a.nr_sequencia		= nr_seq_medicamento_p
	and	a.cd_intervalo	= b.cd_intervalo(+);
end if;

if	(ie_dado_p = 'D') then
	ds_retorno_w	:= qt_dose_w|| ' ' ||cd_unidade_medida_dose_w||'   '||ds_prescricao_w||'   '||ie_via_w;

elsif	(ie_dado_p = 'MA') then

	select	max(substr(obter_desc_material(a.cd_material),1,100))
	into	ds_material_w
	from	medic_uso_continuo a
	where	a.nr_sequencia = nr_seq_medic_subst_w;
	
	ds_retorno_w	:= ds_material_w;

elsif	(ie_dado_p = 'S') then

	select	max(DS_MOTIVO_SUSP)
	into	DS_MOTIVO_SUSP_w
	from	MOTIVO_SUSP_MEDIC_USO_CONT
	where 	nr_sequencia = nr_seq_motivo_susp_w;

	ds_retorno_w	:= DS_MOTIVO_SUSP_w;

end if;

return	ds_retorno_w;

end obter_dados_medic_uso_continuo;
/