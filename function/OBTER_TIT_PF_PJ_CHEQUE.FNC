create or replace
function OBTER_TIT_PF_PJ_CHEQUE(nr_seq_cheque_p		number)
			     return varchar2 is

ds_retorno_w		varchar(3999);
nr_titulos_w		varchar(255);
cd_pessoa_fisica_w	varchar(10);
cd_cgc_w		varchar(14);
ds_auxiliar_w		varchar(3999);	

Cursor C01 is -- Buscar titulos vinculados ao cheque
	select x.* 	
	from
		(
		select	a.nr_titulo
		from	cheque_bordero_titulo b,
			titulo_pagar a
		where	a.nr_titulo	= b.nr_titulo
		and	b.nr_seq_cheque	= nr_seq_cheque_p
		union
		select	a.nr_titulo
		from	cheque_bordero_titulo b,
			titulo_pagar a
		where	a.nr_bordero	= b.nr_bordero
		and	b.nr_seq_cheque	= nr_seq_cheque_p
		union
		select	a.nr_titulo
		from	titulo_pagar a
		where	exists
			(select	1
			 from	adiantamento_pago_cheque z,
				titulo_pagar_adiant y
			 where	y.nr_titulo		= a.nr_titulo
			 and	y.nr_adiantamento	= z.nr_adiantamento
			 and	z.nr_seq_cheque		= nr_seq_cheque_p)
		union
		select	a.nr_titulo
		from	titulo_pagar a
		where	exists
			(select	1
			from	adiantamento_pago_cheque z
			where	z.nr_seq_cheque		= nr_seq_cheque_p
			and	z.nr_adiantamento	= a.nr_adiant_pago)
		) x
		order by 1;
		
Cursor C02 is -- Buscar PF/PJ dos titulos
	select  cd_pessoa_fisica,
		cd_cgc
	from	titulo_pagar
	where	nr_titulo = nr_titulos_w;
		
begin

open C01;
loop
fetch C01 into	
	nr_titulos_w;
exit when C01%notfound;
	begin
	
	if (nr_titulos_w is not null) then
	
		open C02;
		loop
		fetch C02 into	
			cd_pessoa_fisica_w,
			cd_cgc_w;
		exit when C02%notfound;
			begin
			ds_retorno_w := ds_retorno_w || nr_titulos_w ||' - '|| SUBSTR(obter_nome_pf_pj(cd_pessoa_fisica_w,cd_cgc_w),1,80)||', ';	
			end;
		end loop;
		close C02;
	
	end if;
	
	end;
end loop;
close C01;

return	substr(ds_retorno_w,1,3999);

end OBTER_TIT_PF_PJ_CHEQUE;
/