CREATE OR REPLACE
FUNCTION obter_motivo_audit_item	(nr_sequencia_p	 number,
					nr_atendimento_p number,
					ie_item_p        varchar2)
				RETURN VARCHAR2 IS

/* ie_item_p              P - Procedimento     M - Material*/
				
ds_retorno_w	Varchar2(100);

BEGIN

if (ie_item_p = 'P') then
  
    select	max(a.nr_seq_motivo)
    into	ds_retorno_w
    from	auditoria_propaci a,
		auditoria_conta_paciente b
    where	a.nr_seq_propaci		= nr_sequencia_p
    and		a.nr_seq_auditoria 		= b.nr_sequencia
    and		b.nr_atendimento		= nr_atendimento_p;
    
elsif (ie_item_p = 'M') then

    select	max(a.nr_seq_motivo)
    into	ds_retorno_w
    from	auditoria_matpaci a,
		auditoria_conta_paciente b
    where	a.nr_seq_matpaci		= nr_sequencia_p
    and		a.nr_seq_auditoria 		= b.nr_sequencia
    and		b.nr_atendimento		= nr_atendimento_p;
    
end if;

RETURN	ds_retorno_w;

END	obter_motivo_audit_item;
/
