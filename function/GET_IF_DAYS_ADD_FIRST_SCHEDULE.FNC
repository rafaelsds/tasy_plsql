create or replace FUNCTION GET_IF_DAYS_ADD_FIRST_SCHEDULE(nr_atendimento_p	 		number,
			 cd_estabelecimento_p			number,
			 dt_prescricao_p			date,
			 nm_usuario_p				varchar2,
			 cd_perfil_p				number,
			 ie_funcao_p				varchar2,
       ie_via_aplicacao_p		varchar2) RETURN varchar2 IS

nm_usuario_w			usuario.nm_usuario%type;
cd_perfil_w				perfil.cd_perfil%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
cd_setor_atend_prescr_w		setor_atendimento.cd_setor_atendimento%type;
ie_regra_horario_w    prescr_horario_setor.ie_regra_horario%type;
si_has_rule_w varchar2(1) := 'N';

cursor C01 is
	select	ie_regra_horario
	from	prescr_horario_setor
	where	nvl(cd_setor_atendimento, nvl(cd_setor_atend_prescr_w,0)) = nvl(cd_setor_atend_prescr_w,0)
	and	nvl(nm_usuario_regra, nvl(nm_usuario_w, 0)) = nvl(nm_usuario_w, 0)
	and	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and	nvl(cd_perfil, cd_perfil_w) = cd_perfil_w
	and	((nvl(ie_funcao_p,'A') = nvl(ie_funcao,'A')) or
		(nvl(ie_funcao,'A') = 'A'))
	and	dt_prescricao_p between obter_prim_hor_prescr_regra(dt_prescricao_p, 
						nvl(hr_inicio,pkg_date_utils.get_dateTime(1900, 1, 1, 00, 00, 01)), 
						nvl(hr_fim,pkg_date_utils.get_dateTime(1900,01,01,23,59,59)), 'I') and
					obter_prim_hor_prescr_regra(dt_prescricao_p, 
						nvl(hr_inicio,pkg_date_utils.get_dateTime(1900, 1, 1, 00, 00, 01)), 
						nvl(hr_fim,pkg_date_utils.get_dateTime(1900,01,01,23,59,59)), 'F')
	and	nvl(ie_via_aplicacao, nvl(ie_via_aplicacao_p, 0)) = nvl(ie_via_aplicacao_p, 0)
	order by nvl(nr_seq_prioridade,0),
		nvl(cd_setor_atendimento,99999999) desc,
		nvl(cd_perfil,9999999) desc,
		nvl(nm_usuario_regra, 'XXXX') desc,
		hr_fixa desc;

begin

cd_perfil_w				:=  nvl(cd_perfil_p, wheb_usuario_pck.get_cd_perfil);
nm_usuario_w			:=  nvl(nm_usuario_p, wheb_usuario_pck.get_nm_usuario);
cd_estabelecimento_w	:=  nvl(cd_estabelecimento_p, wheb_usuario_pck.get_cd_estabelecimento);

cd_setor_atend_prescr_w := cpoe_obter_setor_atend_prescr(nr_atendimento_p,cd_estabelecimento_w,cd_perfil_w,nm_usuario_w);

FOR C01_w IN C01 LOOP
  ie_regra_horario_w := C01_w.ie_regra_horario;
END LOOP;

if	(ie_regra_horario_w = 'D1') or		-- Sunday according interval first time
  (ie_regra_horario_w = 'D2') or		-- Monday according interval first time
  (ie_regra_horario_w = 'D3') or		-- Tuesday according interval first time
  (ie_regra_horario_w = 'D4') or		-- Wednesday according interval first time
  (ie_regra_horario_w = 'D5') or		-- Thursday according interval first time
  (ie_regra_horario_w = 'D6') or		-- Friday according interval first time
  (ie_regra_horario_w = 'D7') then		-- Saturday according interval first time

  si_has_rule_w := 'S';

end if;

return si_has_rule_w;

end;
/
