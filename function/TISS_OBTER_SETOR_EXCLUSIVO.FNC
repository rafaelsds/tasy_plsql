create or replace 
function TISS_OBTER_SETOR_EXCLUSIVO
				(cd_procedimento_p	in number,
				ie_origem_proced_p	in number)
				return number is

ds_retorno_w	number(5);	

begin

select	max(cd_setor_exclusivo)
into	ds_retorno_w
from 	procedimento
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

return ds_retorno_w;

end TISS_OBTER_SETOR_EXCLUSIVO;
/