create or replace 
function obter_se_prescr_item_susp(nr_prescricao_p	number) return varchar2 is
qt_susp_w		number(15);
ds_retorno_w	varchar2(1);
begin
select 	sum(qt_susp)
into		qt_susp_w
from		(	select	count(*) qt_susp
			from		material a,
					prescr_material b
			where	a.cd_material	= b.cd_material
			and		b.ie_suspenso	= 'S'
			and		b.ie_agrupador in (1,4,8,12)
			and		b.nr_prescricao	= nr_prescricao_p
			union
			select	count(*) qt_susp
			from		procedimento a,
					prescr_procedimento b
			where	a.cd_procedimento	= b.cd_procedimento
			and		a.ie_origem_proced	= b.ie_origem_proced
			and		b.ie_suspenso	= 'S'
			and		b.nr_prescricao	= nr_prescricao_p);
ds_retorno_w := 'N';
if	(qt_susp_w > 0) then
	ds_retorno_w := 'S';
end if;

return ds_retorno_w;

end 	obter_se_prescr_item_susp;
/