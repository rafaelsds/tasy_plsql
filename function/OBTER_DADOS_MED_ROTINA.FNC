create or replace
function	obter_dados_med_rotina( 
						cd_protocolo_p		number,
						nr_seq_material_p	number,
						ie_opcao_p			varchar2) return varchar2 is

ie_intervalo_w					varchar2(1);
ie_unidade_medida_w				varchar2(1);
ie_obriga_justificativa_w		varchar2(1);
ie_dados_atb_w					varchar2(1);
ie_dados_medicamento_w			varchar2(1);
ie_questionar_obs_w				varchar2(1);
ie_retorno_w					varchar2(1);

begin

select	nvl(max(a.ie_intervalo),'N'),
		nvl(max(a.ie_unidade_medida),'N'),
		nvl(max(decode(a.ie_justificativa,'S',a.ie_justificativa,obter_dados_medic_atb_var(b.cd_material,wheb_usuario_pck.get_cd_estabelecimento,b.ie_obriga_justificativa,'OJ',null,null,null))),'N'),
		nvl(max(a.ie_dados_atb),'N'),
		nvl(max(a.ie_dados_medicamento),'N'),
		nvl(max(a.ie_questionar_obs),'N')
into		ie_intervalo_w,
		ie_unidade_medida_w,
		ie_obriga_justificativa_w,
		ie_dados_atb_w,
		ie_dados_medicamento_w,
		ie_questionar_obs_w	
from 	protocolo_medic_material a,
		material b
where	a.cd_material = b.cd_material
and		nvl(a.ie_medic_rotina,'N') = 'S'
and		a.cd_protocolo = cd_protocolo_p
and		a.nr_seq_material = nr_seq_material_p;

if	(ie_opcao_p = 'INT') then
	ie_retorno_w := ie_intervalo_w;
elsif	(ie_opcao_p = 'UMD') then
	ie_retorno_w := ie_unidade_medida_w;
elsif	(ie_opcao_p = 'JUS') then
	ie_retorno_w := ie_obriga_justificativa_w;
elsif	(ie_opcao_p = 'ATB') then
	ie_retorno_w := ie_dados_atb_w;
elsif	(ie_opcao_p = 'MED') then
	ie_retorno_w := ie_dados_medicamento_w;
elsif	(ie_opcao_p = 'OBS') then
	ie_retorno_w := ie_questionar_obs_w;
end if;

return ie_retorno_w;

end obter_dados_med_rotina;
/
