create or replace
function obter_valor_icmsst_nf( nr_sequencia_p		number)
 		    	return number is

retorno_w		number(13,2);
icms_w		number(13,2);
						
begin

select	nvl(sum(a.vl_tributo),0)
into	icms_w
from	nota_fiscal_item_trib a,
	tributo b
where	a.cd_tributo 		= b.cd_tributo
and	a.nr_sequencia		= nr_sequencia_p
and	b.ie_tipo_tributo = 'ICMSST'
and   	a.ie_rateio = 'N';

retorno_w	:= icms_w;

return	retorno_w;

end obter_valor_icmsst_nf;
/
