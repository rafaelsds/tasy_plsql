create or replace
function obter_conta_bandeira_cartao(	nr_sequencia_p	number)
 		    		return varchar2 is
			
cd_conta_contabil_w		varchar2(20);

begin

if	(nr_sequencia_p is not null) then
	
	select	max(cd_conta_contabil)
	into	cd_conta_contabil_w
	from	bandeira_cartao_cr
	where	nr_sequencia	= nr_sequencia_p;
	
end if;

return	cd_conta_contabil_w;

end obter_conta_bandeira_cartao;
/