create or replace 
Function Obter_Lab_Execucao_Etapa_HTML5(	nr_prescricao_p	number,
					nr_seq_prescr_p	number,
					ie_etapa_p	varchar2,
					ie_opcao_p	varchar2) 
					return varchar2 is

/* ie_opcao_p
	'U' - nm_usuario
	'D' - dt_atualizacao
	'S' - sequencia
*/

nm_usuario_w		varchar2(15);
dt_atualizacao_w	date;
nr_sequencia_w		number(10);

begin

nm_usuario_w := '';
dt_atualizacao_w := null;

select nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from 	prescr_proc_etapa
where	nr_prescricao		= nr_prescricao_p
  and	nr_seq_prescricao	= nr_Seq_prescr_p
  and	ie_etapa		= ie_etapa_p;

if (nr_sequencia_w > 0) then
	select	nvl(max(nm_usuario),''),
		nvl(max(dt_atualizacao), null)
	into	nm_usuario_w,
		dt_atualizacao_w
	from 	prescr_proc_etapa
	where	nr_prescricao		= nr_prescricao_p
	  and	nr_seq_prescricao	= nr_Seq_prescr_p
	  and	ie_etapa		= ie_etapa_p
	  and 	nr_sequencia		= nr_sequencia_w;
end if;

if	(ie_opcao_p = 'U') then
	return nm_usuario_w;
elsif	(ie_opcao_p = 'D') then
	return dt_atualizacao_W;
elsif	(ie_opcao_p = 'S') then
	return nr_sequencia_w;
end if;

end;
/