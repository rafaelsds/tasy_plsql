create or replace
function obter_cod_usuario_conv(	nr_atendimento_p		number,
				cd_convenio_p		number)
			
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);			

begin

if 	(nr_atendimento_p is not null) and
	(cd_convenio_p is not null) then

	select 		max(a.cd_usuario_convenio)
	into		ds_retorno_w
	from 		atend_categoria_convenio a
	where 		a.nr_atendimento = nr_atendimento_p
	and   		a.cd_convenio = cd_convenio_p
	and   		a.dt_inicio_vigencia = (select  max(b.dt_inicio_vigencia)
						from 	atend_categoria_convenio b
						where 	b.nr_atendimento = a.nr_atendimento
						and     b.cd_convenio = a.cd_convenio);

end if;

return	ds_retorno_w;

end obter_cod_usuario_conv;
/