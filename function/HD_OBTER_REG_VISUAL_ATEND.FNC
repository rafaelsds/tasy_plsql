create or replace
function hd_obter_reg_visual_atend(	nr_atendimento_p	number)
 		    	return varchar2 is
				
ie_permite_w	Varchar2(1);
begin
if	(nvl(nr_atendimento_p,0) > 0) then

	select	nvl(max('S'),'N')
	into	ie_permite_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_p
	and	((not exists (	select 1 from hd_regra_visual_atend y where nvl(y.ie_situacao,'A') = 'A'))
		or (exists (	select	1 
			from	paciente_tratamento z ,
					hd_regra_visual_atend y
			where	a.cd_pessoa_fisica = z.cd_pessoa_fisica
			and		a.ie_tipo_atendimento = nvl(y.ie_tipo_atendimento,a.ie_tipo_atendimento)
			and		z.ie_paciente_agudo is not null 
			and		nvl(y.ie_situacao,'A') = 'A'
			and		((z.ie_paciente_agudo = y.ie_tipo_paciente) or (y.ie_tipo_paciente = 'A'))
			and		z.dt_final_tratamento is null)));
		
end if;
return	ie_permite_w;

end hd_obter_reg_visual_atend;
/
