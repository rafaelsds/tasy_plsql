create or replace
function obter_evento_regra(	nr_seq_evento_p  number,
				cd_tipo_procedimento_p number)
				return number is

nr_seq_evento_conv_w number(10);

cursor c01 is
	select a.nr_sequencia
	from	evento_convenio_regra b,
		evento_convenio a
	where a.nr_sequencia      = b.nr_seq_evento_conv
	   and a.nr_seq_evento      = nr_seq_evento_p
	   and nvl(b.cd_tipo_procedimento, cd_tipo_procedimento_p)  = cd_tipo_procedimento_p
	   and	a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	order by nvl(b.cd_tipo_procedimento, 0);
begin

open c01;
loop
	fetch c01 into nr_seq_evento_conv_w;
	exit when c01%notfound;
end loop;
close c01;

return nr_seq_evento_conv_w;

end;
/
