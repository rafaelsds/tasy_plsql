create or replace
function Obter_interv_acm_sn_agora_lib( cd_estabelecimento_p	number,
										ie_intervalo_p			number,
										ie_opcao_p				number,
										ie_item_p				varchar2,
										cd_item_p				varchar2,
										cd_intervalo_p			varchar2,
										nr_prescricao_p			number,
										ie_origem_proced_p		varchar2 default null,
										nr_seq_proc_interno_p	number default null,
										ie_prescr_emergencia_p 	varchar2 default null,
										cd_setor_atendimento_p	number default null)
 		    	return varchar2 is

			
/*
------------IE_INTERVALO_P
1 - Agora
2 - Se necess�rio
3 - ACM
4 - Exceto Agora
5 - Exceto Se necess�rio
6 - Exceto ACM

------------IE_OPCAO_P
0/null	- Verifica maior do tipo		MAX(cd_intervalo)
1	- Verifica se exibe		S / N
2	- Trazer maior intervalo que possa ser exibido para o item

------------IE_ITEM_P
1 - Medicamentos
2 - Materiais
P - Procedimentos
5 - Medicamentos associados ao procedimentos
B - Banco de sangue
R - Recomenda��es
8 - SNE
9 - NPTs
12 - Suplemento oral
13 - Dieta Oral
14 - Hemodi�lise
15 - Solu��es
16 - Lact�rio
*/

cd_intervalo_w				intervalo_prescricao.cd_intervalo%type;
ie_agora_w					varchar2(1) := 'S';
ie_acm_w					varchar2(1) := 'S';
ie_sn_w						varchar2(1) := 'S';
ie_prescr_emergencia_w		char(1) := 'N';
ds_where_especif_w			varchar2(10000);
ie_executa_sql_w			boolean	:= true;
cd_setor_atendimento_w		setor_atendimento.cd_setor_atendimento%type;
ie_desconsidera_interv_w	varchar2(1) := 'N';

Cursor C01 is
	select	max(cd_intervalo)
	from	intervalo_prescricao
	where	coalesce(ie_situacao,'A') = 'A'
	and		Obter_se_intervalo_estab(cd_intervalo, cd_estabelecimento_p) = 'S'
	and		coalesce(obter_se_exibe_intervalo(nr_prescricao_p,cd_intervalo, null),'S') = 'S'
	and		Obter_se_interv_regra(cd_intervalo, ie_item_p) = 'S'
	and		Obter_se_intervalo(cd_intervalo, ie_item_p) = 'S'
	and 	(((ie_item_p = '1') and
			  (Obter_se_intervalo_material(cd_item_p,cd_intervalo) = 'S')) or
			 ((ie_item_p = 'P') and
			  (obter_se_intervalo_proc(cd_item_p, ie_origem_proced_p, nr_seq_proc_interno_p, cd_intervalo, cd_setor_atendimento_w)  = 'S')) or
			 (ie_item_p not in ('1','P')))
	and		(((ie_item_p in ('1', '2', '8', '12')) and
			  (Obter_se_intervalo_lib_mat(cd_item_p, cd_intervalo) = 'S')) or
			 (ie_item_p not in ('1', '2', '8', '12')))
	and		(((ie_item_p in ('1','15')) and
			  (((ie_agora_w = 'S') and
			    (((coalesce(ie_agora,'N') <> 'S') or (Obter_Menor_Intervalo_agora(1, nr_prescricao_p) = cd_intervalo)))) or
			   (ie_agora_w = 'N'))) or
			 (ie_item_p not in ('1','15')))
	and		(((ie_prescr_emergencia_w <> 'N') and
			  (coalesce(ie_so_retrograda,'N') = 'N')) or
			 (ie_prescr_emergencia_w = 'N'))
	and		(((cd_estabelecimento_p is null) and
			  (cd_estabelecimento is null)) or
			 ((cd_estabelecimento_p is not null) and
			  (nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p)))
	and		(((ie_sn_w = 'S') and (nvl(ie_se_necessario,'N') = 'S')) or
			 ((ie_acm_w = 'S') and (nvl(ie_acm,'N') = 'S')) or
			 ((ie_agora_w = 'S') and (nvl(ie_agora,'N') = 'S')) or
			 ((ie_agora_w = ie_acm_w) and (ie_acm_w = ie_sn_w)))
	and		((ie_desconsidera_interv_w = 'S') or 
			 (((cd_intervalo_p is not null) and
			   (((not (coalesce(ie_opcao_p,0) = 0)) and
			     (cd_intervalo = cd_intervalo_p)) or
			    (coalesce(ie_opcao_p,0) = 0))) or
			  (cd_intervalo_p is null)));

begin

ie_prescr_emergencia_w := ie_prescr_emergencia_p;
cd_setor_atendimento_w := cd_setor_atendimento_p;

if	(ie_item_p is not null) then
	if	(coalesce(ie_intervalo_p,0) > 0) then
		if	(ie_intervalo_p = 1) then
			ie_acm_w	:= 'N';
			ie_sn_w		:= 'N';
		elsif	(ie_intervalo_p = 2) then
			ie_agora_w	:= 'N';
			ie_acm_w	:= 'N';
		elsif	(ie_intervalo_p = 3) then
			ie_agora_w	:= 'N';
			ie_sn_w		:= 'N';
		elsif	(ie_intervalo_p = 4) then
			ie_agora_w	:= 'N';
		elsif	(ie_intervalo_p = 5) then
			ie_acm_w	:= 'N';
		elsif	(ie_intervalo_p = 6) then
			ie_sn_w		:= 'N';
		end if;
	end if;
	
	if	(nr_prescricao_p is not null) and
		((ie_prescr_emergencia_w is null) or
		 (cd_setor_atendimento_w is null)) then
		select	coalesce(max(ie_prescr_emergencia),'N'),
				max(cd_setor_atendimento)
		into	ie_prescr_emergencia_w,
				cd_setor_atendimento_w
		from	prescr_medica
		where	nr_prescricao = nr_prescricao_p;
	end if;

	cd_intervalo_w	:= null;

	if	(ie_item_p = '1') and
		(cd_item_p is null) then
		ie_executa_sql_w	:= false;
	end if;
	
	-- Define se ser� executada a consulta
	if	(ie_executa_sql_w) then
		open C01;
		loop
		fetch C01 into	
			cd_intervalo_w;
		exit when C01%notfound;
			begin
				cd_intervalo_w := cd_intervalo_w;
			end;
		end loop;
		close C01;
	end if;
	
	if	(coalesce(ie_opcao_p,0) = 0) then
		return cd_intervalo_w;
	elsif	(ie_opcao_p = 1) then
		if	(cd_intervalo_w is not null) then
			return 'S';
		else
			return 'N';
		end if;
	elsif	(ie_opcao_p = 2) then
	
		if	(ie_executa_sql_w) and
			(cd_intervalo_w is null) then
			ie_desconsidera_interv_w := 'S';
			open c01;
			loop
			fetch c01 into	
				cd_intervalo_w;
			exit when c01%notfound;
				begin
					cd_intervalo_w := cd_intervalo_w;
				end;
			end loop;
			close c01;
		end if;
		
		return cd_intervalo_w;
	end if;
	
end if;

return '';

end Obter_interv_acm_sn_agora_lib;
/