create or replace
function obter_estagio_revisao_proc_int(nr_seq_proc_interno_p	number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is

ds_estagio_w	estagio_revisao_proc_int.ds_estagio%type;		
begin

begin
select	a.ds_estagio
into	ds_estagio_w
from	estagio_revisao_proc_int a,
	proc_interno_revisao b
where	a.nr_sequencia 		= b.nr_seq_estagio
and	b.nr_seq_proc_interno   = nr_seq_proc_interno_p
and	b.dt_liberacao	is null
and	rownum < 2;
exception
when others then
	ds_estagio_w := null;
end;


return	ds_estagio_w;

end obter_estagio_revisao_proc_int;
/