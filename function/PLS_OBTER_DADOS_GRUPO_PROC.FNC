create or replace
function pls_obter_dados_grupo_proc (	nr_seq_grupo_p		pls_regra_ocor_grupo_serv.nr_sequencia%type,
					ie_opcao_p		varchar2)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Obter dados do grupo de procedimento cadastrado em "OPS - Glosas e Ocorrências" 
	pasta "Regras / Ocorrências / Grupo procedimento"
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
ie_opcao_p

'DS' - Descrição do grupo
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ds_retorno_w	varchar2(255);
begin

if	(nvl(ie_opcao_p,'X') = 'DS') then
	select	ds_grupo
	into	ds_retorno_w
	from	pls_regra_ocor_grupo_serv
	where	nr_sequencia = nr_seq_grupo_p;
end if;


return	ds_retorno_w;

end pls_obter_dados_grupo_proc;
/
