create or replace
function pls_obter_dados_inc_hist (nr_seq_historico_p		number)
 		    	return varchar2 is
ds_retorno_w		varchar2(4000);
			
begin

select	ds_observacao
into	ds_retorno_w
from	pls_pessoa_inc_hist
where	nr_sequencia	= nr_seq_historico_p;

return	ds_retorno_w;

end pls_obter_dados_inc_hist;
/