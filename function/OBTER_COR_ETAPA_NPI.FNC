create or replace
function obter_cor_etapa_npi(nr_seq_etapa_processo_p		Number)
 		    	return Number is

retorno_w	Number(10);
dt_fim_prev_w	date;
ie_execucao_w	varchar2(1);
pr_etapa_w      Number(10);

begin

select	nvl(max(to_number(nvl(p.ie_status_etapa,'2'))),'2'),
	max(trunc(p.dt_fim_prev)),
	max(p.pr_etapa)
into	retorno_w,
	dt_fim_prev_w,
	pr_etapa_w
from	proj_cron_etapa p,
	prp_processo_etapa c
where	c.nr_sequencia = p.nr_seq_processo_etapa
and	c.nr_sequencia = nr_Seq_etapa_processo_p;


select	decode(count(*), 0, 'N', 'S')
into	ie_execucao_w
from	proj_cron_etapa p,
	prp_processo_etapa c,
	man_ordem_servico o,
	man_ordem_serv_ativ a
where	c.nr_sequencia = p.nr_seq_processo_etapa
and	p.nr_sequencia = o.nr_seq_proj_cron_etapa
and	o.nr_sequencia = a.nr_seq_ordem_serv
and	c.nr_sequencia = nr_Seq_etapa_processo_p;

if  (pr_etapa_w = 100) then
     retorno_w := 4;
end if;     

if (retorno_w in (2,3) and dt_fim_prev_w < trunc(sysdate)) then
    retorno_w := 5;
elsif (retorno_w in (2,3) and ie_execucao_w = 'S') then
       retorno_w := 3;
end if;

return	retorno_w;

end obter_cor_etapa_npi;
/
