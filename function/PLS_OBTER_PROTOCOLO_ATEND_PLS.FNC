create or replace
function pls_obter_protocolo_atend_pls(	nr_seq_tabela_origem_p		number,
					ie_origem_protocolo_p		number)
					return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter o nr_protocolo da pls_protocolo_atendimento

-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [   ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
/* ie_origem_protocolo_p
	1 - OPS - Autorizações
	2 - OPS - Requisições para autorização
	3 - Atendimento (call center)
	4 - Boletim de ocorrência
*/

nr_protocolo_w		pls_protocolo_atendimento.nr_protocolo%type;

begin

if (ie_origem_protocolo_p = 1) then
	select	max(nr_protocolo)
	into	nr_protocolo_w
	from	pls_protocolo_atendimento
	where 	nr_seq_guia	= nr_seq_tabela_origem_p;

elsif (ie_origem_protocolo_p = 2) then
	select	max(nr_protocolo)
	into	nr_protocolo_w
	from	pls_protocolo_atendimento
	where 	nr_seq_requisicao	= nr_seq_tabela_origem_p;
end if; 

return	nr_protocolo_w;

end pls_obter_protocolo_atend_pls;
/