create or replace
function obter_max_toxidade	( 	nr_atendimento_p	Number,
					dt_avalicao_p 		Date
					)
 		    	return number is
			
ie_toxidade_w	Number;

begin

Select 	max(a.ie_toxidade)
into	ie_toxidade_w
from	escala_mucosite a,
	atendimento_paciente b
where	a.nr_atendimento = b.nr_atendimento
and	a.nr_atendimento = nr_atendimento_p
and	trunc(dt_avaliacao) = trunc(dt_avalicao_p)
and	((upper(obter_desc_classif_atend(b.nr_seq_classificacao)) like '%ONCOLOGIA%') or
	(upper(obter_desc_classif_atend(b.nr_seq_classificacao)) like '%HEMATOLOGIA%') or
	(upper(obter_desc_classif_atend(b.nr_seq_classificacao)) like '%TMO%') or
	(upper(obter_desc_classif_atend(b.nr_seq_classificacao)) like '%MEDULA%')); 

return  ie_toxidade_w;

end obter_max_toxidade;
/