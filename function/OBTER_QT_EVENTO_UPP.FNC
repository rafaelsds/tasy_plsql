create or replace
function obter_qt_evento_UPP(	nr_sequencia_p	number)
			return number is
			
qt_retorno_w 		number(10);
dt_evento_min_w		qua_evento_paciente.dt_evento%type;
nr_atendimento_w 	qua_evento_paciente.nr_atendimento%type;
dt_evento_w 		qua_evento_paciente.dt_evento%type;

begin
	SELECT	a.nr_atendimento
			,a.dt_evento
	INTO 	nr_atendimento_w
			,dt_evento_w
	FROM 	qua_evento_paciente a
	WHERE 	a.nr_sequencia = nr_sequencia_p;
	
	SELECT 	min(dt_evento)
	into 	dt_evento_min_w
	FROM 	qua_evento_paciente
	WHERE 	nr_atendimento = nr_atendimento_w
	and		nr_seq_evento = 56;
	
	SELECT	count(*) qt_evento
	INTO 	qt_retorno_w
	FROM 	qua_evento_paciente b
	WHERE 	b.nr_atendimento = nr_atendimento_w
	and 	nr_sequencia = nr_sequencia_p
	and 	dt_evento = dt_evento_w
	and		dt_evento = dt_evento_min_w;

	return qt_retorno_w;

end obter_qt_evento_UPP;
/