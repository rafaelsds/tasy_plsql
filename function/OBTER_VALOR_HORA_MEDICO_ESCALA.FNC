create or replace
function obter_valor_hora_medico_escala
		(	cd_medico_p	number) 
			return Number is

vl_retorno_w 		number(15,2);			

Cursor C01 is
	select	vl_hora
	from	escala_valor_hora_medico
	where	cd_medico = cd_medico_p
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_final_vigencia, sysdate)
	order by dt_inicio_vigencia; --Se existir duas deste m�dico no periodo ser� obtida a mais recente
			
begin

open C01;
loop
fetch C01 into	
	vl_retorno_w;
exit when C01%notfound;	
end loop;
close C01;

return	vl_retorno_w;

end obter_valor_hora_medico_escala;
/
