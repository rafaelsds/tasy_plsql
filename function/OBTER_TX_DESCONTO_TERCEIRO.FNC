create or replace
function OBTER_TX_DESCONTO_TERCEIRO
			(nr_repasse_terceiro_p	in number)
			return number is
dt_referencia_w	date;
vl_retorno_w	number(15,4);
			
begin

select	dt_mesano_referencia
into	dt_referencia_w
from	repasse_terceiro	
where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

select	nvl(max(tx_desconto),0) * -1
into	vl_retorno_w
from	regra_desc_repasse a
where	trunc(a.dt_mesano_ref,'month') = trunc(dt_referencia_w,'month');

return 	vl_retorno_w;

end OBTER_TX_DESCONTO_TERCEIRO;
/