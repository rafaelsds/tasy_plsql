create or replace
function obter_apres_exame_padrao(nr_seq_exame_p	number)
				return varchar2 is

nr_seq_apresent_w	number(10,0);

begin

if	(nr_seq_exame_p > 0) then
	select	nr_seq_apresent
	into	nr_seq_apresent_w
	from	med_exame_padrao
	where	nr_sequencia = nr_seq_exame_p;
end if;
	
return	nr_seq_apresent_w;

end  obter_apres_exame_padrao;
/