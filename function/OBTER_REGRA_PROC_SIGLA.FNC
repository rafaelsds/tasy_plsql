create or replace
function OBTER_REGRA_PROC_SIGLA(cd_procedimento_p	number,
				ie_origem_proc_p					varchar2,
				cd_tipo_procedimento_p				number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(100) := '';

begin
if (cd_tipo_procedimento_p = '') then
	select 	max(ds_sigla)
	into 	ds_retorno_w
	from 	regra_sigla_proc
	where 	CD_PROCEDIMENTO = cd_procedimento_p 
	and	IE_ORIGEM_PROCED = ie_origem_proc_p;
else
	select 	max(substr(ds_sigla,1,100))
	into 	ds_retorno_w
	from 	regra_sigla_proc
	where 	cd_tipo_procedimento = cd_tipo_procedimento_p;

	if ((ds_retorno_w = '') or (ds_retorno_w is null)) then
		select 	max(ds_sigla)
		into 	ds_retorno_w
		from 	regra_sigla_proc
		where 	CD_PROCEDIMENTO = cd_procedimento_p 
		and	IE_ORIGEM_PROCED = ie_origem_proc_p;	
	end if;
	
end if;
	
return	ds_retorno_w;

end OBTER_REGRA_PROC_SIGLA;
/
