create or replace
function obter_se_copia_justif(	qt_total_dias_lib_p		number,
								nr_dia_util_p			number,
								ie_dias_util_medic_p	varchar2)
 		    	return varchar2 is

ie_maior_w		varchar2(10) := 'S';
nr_dia_util_w	number(10);
				
begin

nr_dia_util_w	:= nr_dia_util_p +1;

if	(ie_dias_util_medic_p = 'O') then
	nr_dia_util_w	:= nr_dia_util_w +1;	
end if;

if	(ie_dias_util_medic_p = 'S') then
	ie_maior_w	:= Obter_se_maior(qt_total_dias_lib_p, nr_dia_util_w);
	if	((nr_dia_util_w > qt_total_dias_lib_p) and ((nr_dia_util_w - qt_total_dias_lib_p) < 2)) then
		ie_maior_w := 'S';
	elsif ((nr_dia_util_w > qt_total_dias_lib_p) and ((nr_dia_util_w - qt_total_dias_lib_p) >= 2)) then
		ie_maior_w := 'N';
	end if;
else
	ie_maior_w		:= Obter_se_maior(qt_total_dias_lib_p, nr_dia_util_w);
end if;

return	ie_maior_w;

end obter_se_copia_justif;
/
