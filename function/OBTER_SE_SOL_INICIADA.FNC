create or replace
function obter_se_sol_iniciada (
			nr_prescricao_p		number,
			nr_seq_solucao_p	number)
			return varchar2 is

ie_retorno_w	varchar2(1) := 'N';
			
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) then
	begin
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	prescr_mat_hor
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_solucao	= nr_seq_solucao_p
	and	dt_inicio_horario is not null
	and	dt_fim_horario is null
	and	dt_interrupcao is null;
		
	end;
end if;

return ie_retorno_w; 

end obter_se_sol_iniciada;
/