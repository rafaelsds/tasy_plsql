create or replace
function obter_valores_tit_proj_rec(	nr_seq_proj_rec_p			number,
				ie_opcao_p			varchar2)
 		    	return number is

			
/*
ie_opcao_p
	VT - Valor do t�tulo
	VL - Valor o t�tulo liquidado	
*/			
			
vl_retorno_w			titulo_pagar_baixa.vl_baixa%type := 0;
vl_baixa_w			titulo_pagar_baixa.vl_baixa%type := 0;
vl_baixa_ww			titulo_pagar_baixa.vl_baixa%type := 0;
vl_baixa_liq_w			titulo_pagar_baixa.vl_baixa%type := 0;
vl_baixa_liq_ww			titulo_pagar_baixa.vl_baixa%type := 0;
nr_titulo_w			titulo_pagar.nr_titulo%type;

cursor c01 is
select	distinct t.nr_titulo
from	titulo_pagar t,
	titulo_pagar_baixa b,
	nota_fiscal n
where	t.nr_seq_nota_fiscal in	(	select	i.nr_sequencia
					from	nota_fiscal_item i
					where	i.nr_seq_proj_rec = nr_seq_proj_rec_p)
and	n.nr_sequencia = t.nr_seq_nota_fiscal
and 	t.nr_titulo = b.nr_titulo;
			
begin


open C01;
loop
fetch C01 into	
	nr_titulo_w;
exit when C01%notfound;
	begin
	
	select	nvl(sum(a.vl_baixa),0)
	into	vl_baixa_w
	from	titulo_pagar_baixa a
	where	a.nr_titulo = nr_titulo_w;
	
	vl_baixa_ww := vl_baixa_ww + vl_baixa_w;
	
	
	select	nvl(sum(a.vl_baixa),0)
	into	vl_baixa_liq_w
	from	titulo_pagar_baixa a,
		titulo_pagar b
	where	a.nr_titulo = b.nr_titulo
	and	a.nr_titulo = nr_titulo_w
	and	b.ie_situacao = 'L';
	
	vl_baixa_liq_ww := vl_baixa_liq_ww + vl_baixa_liq_w;
	
	end;
end loop;
close C01;


if	(ie_opcao_p = 'VT') then
	vl_retorno_w := vl_baixa_ww;
elsif	(ie_opcao_p = 'VL') then
	vl_retorno_w := vl_baixa_liq_ww;
end if;

return	vl_retorno_w;

end obter_valores_tit_proj_rec;
/