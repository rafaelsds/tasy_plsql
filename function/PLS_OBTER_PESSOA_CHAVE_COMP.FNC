create or replace
function pls_obter_pessoa_chave_comp
			(	nr_sequencia_p		number,
				ie_opcao_p		varchar2)
 		    		return varchar2 is

ds_retorno_w		varchar2(255);
ds_aux_w		varchar2(100);
cd_pessoa_fisica_w	varchar2(14);
ds_chave_composta_w	varchar(500);
ds_chave_simples_w	Varchar2(255);
nm_tabela_w		Varchar2(30);

begin

select	ds_chave_composta,
	ds_chave_simples,
	nm_tabela
into	ds_chave_composta_w,
	ds_chave_simples_w,
	nm_tabela_w
from	tasy_solic_alt_campo
where	nr_sequencia	= nr_sequencia_p;

if	(nm_tabela_w	= 'PESSOA_FISICA') then
	ds_retorno_w	:= ds_chave_simples_w;
elsif	(nm_tabela_w	= 'COMPL_PESSOA_FISICA') then	
	select	substr(ds_chave_composta_w,instr(ds_chave_composta_w,('CD_PESSOA_FISICA=')) + 17,length(ds_chave_composta_w))
	into	ds_aux_w
	from	dual;

	select	substr(ds_aux_w,1,instr(ds_aux_w,'#@#@') -1)
	into	ds_retorno_w
	from	dual;
end if;

return	ds_retorno_w;

end pls_obter_pessoa_chave_comp;
/