create or replace
function obter_lote_medic_producao(	nr_lote_producao_p	number,
					cd_material_p		number)
					return number is

nr_seq_lote_fornec_w	number(10,0) := null;
					
begin

if	(obter_se_permite_lote_dif(cd_material_p) = 'N') then
	select	max(nr_seq_lote_fornec)
	into	nr_seq_lote_fornec_w
	from	lp_item_util
	where	nr_lote_producao	=	nr_lote_producao_p
	and	cd_material		=	cd_material_p;
end if;

return	nr_seq_lote_fornec_w;

end obter_lote_medic_producao;
/