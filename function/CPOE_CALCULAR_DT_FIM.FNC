create or replace 
function CPOE_Calcular_DT_FIM(dt_inicio_p date, 
    ds_horarios_p varchar2,
    ie_tipo_item_p varchar2,
    cd_intervalo_p varchar2 default null,
    ie_evento_unico_p varchar2 default null)
    return date is
	
ie_param8_cpoe_w		varchar2(1);
dt_fim_w cpoe_procedimento.dt_fim%type;
qt_dias_adicionar_w		number(10) := 0;
ie_operacao_w	intervalo_prescricao.ie_operacao%type;
qt_operacao_w	intervalo_prescricao.qt_operacao%type;

begin

  obter_param_usuario(2314, 8, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_param8_cpoe_w);

  if (ie_param8_cpoe_w = 'F' and ie_tipo_item_p = 'P') then
     select max(a.ie_operacao),
           max(a.qt_operacao)
      into ie_operacao_w,
           qt_operacao_w
      from intervalo_prescricao a
     where a.cd_intervalo = cd_intervalo_p; 
  
     if ( ((ie_operacao_w = 'H' and qt_operacao_w >= 24) or (ie_operacao_w <> 'H' and qt_operacao_w <= 1))
         or ie_evento_unico_p = 'S' ) then
        dt_fim_w := fim_dia(dt_inicio_p);
     else
        dt_fim_w := fim_dia(dt_inicio_p + 1);
    end if;
  else
      dt_fim_w := (dt_inicio_p + 1) - 1/1440;
  end if;

  return trunc(dt_fim_w,'mi');

end CPOE_Calcular_DT_FIM;
/
