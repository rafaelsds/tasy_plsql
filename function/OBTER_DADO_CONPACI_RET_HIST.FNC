create or replace
Function OBTER_DADO_CONPACI_RET_HIST (	nr_sequencia_p	number,
					nm_campo_p	varchar2)
					return varchar2 is

ds_retorno_w		varchar2(100);

begin

if	(upper(nm_campo_p) = 'DT_HISTORICO') then

	select	to_char(dt_historico, 'dd/mm/yyyy hh24:mi:ss')
	into 	ds_retorno_w
	from 	conta_paciente_ret_hist
	where 	nr_sequencia 	= nr_sequencia_p;
	
elsif 	(upper(nm_campo_p) = 'DS_HISTORICO') then

	select 	max(b.ds_historico)
	into 	ds_retorno_w
	from	hist_audit_conta_paciente b,
		conta_paciente_ret_hist a
	where 	a.nr_seq_hist_audit	= b.nr_sequencia
	and 	a.nr_sequencia 		= nr_sequencia_p;
	
elsif 	(upper(nm_campo_p) = 'NR_SEQ_HISTORICO') then

	select	max(b.nr_sequencia)
	into 	ds_retorno_w
	from	hist_audit_conta_paciente b,
		conta_paciente_ret_hist a
	where 	a.nr_seq_hist_audit 	= b.nr_sequencia
	and 	a.nr_sequencia 		= nr_sequencia_p;
end if;

return ds_retorno_w;

end;
/