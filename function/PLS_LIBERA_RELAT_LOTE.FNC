create or replace
function pls_libera_relat_lote
            (nr_seq_lote_p        Number,
            nr_seq_prestador_p    Number)
            return varchar2 is

ds_retorno_w        Varchar2(2);
nr_seq_prestador_w    Number(10);
            
begin

    
select    nvl(max(nr_sequencia),0)
into    nr_seq_prestador_w    
from    pls_pag_prest_vencimento
where    nr_seq_pag_prestador = (select    max(nr_sequencia)
                from    pls_pagamento_prestador
                where     nr_seq_lote        = nr_seq_lote_p
                and    nr_seq_prestador     = nr_seq_prestador_p);

if    (nr_seq_prestador_w = 0) then
    ds_retorno_w := 'N';
else
    ds_retorno_w := 'S';
end if;

return ds_retorno_w;

end pls_libera_relat_lote;
/