create or replace
function nut_obter_dietas_serv_def(nr_seq_serv_dia_p	number)
				return varchar2 is
				
ds_dieta_w	varchar2(2000);
ds_retorno_w	varchar2(2000) := null;
ie_prescr_hor_w	varchar2(1);

Cursor C01 is
        SELECT 	c.nm_dieta 
	FROM   	nut_atend_serv_dia_rep d, 
		prescr_medica a, 
		prescr_dieta b, 
		dieta c, 
		nut_atend_serv_dia x, 
		cpoe_dieta y,
		prescr_dieta_hor z,
		nut_servico f,
		nut_servico_horario e
	WHERE  	b.nr_prescricao = a.nr_prescricao 
	AND 	c.cd_dieta = b.cd_dieta 
	AND 	d.nr_prescr_oral = a.nr_prescricao 
	AND 	y.nr_sequencia(+) = b.nr_seq_dieta_cpoe 
	AND 	d.nr_seq_serv_dia = nr_seq_serv_dia_p 
	AND 	x.nr_sequencia = d.nr_seq_serv_dia
	AND   	z.nr_prescricao = a.nr_prescricao
	AND   	f.nr_sequencia = x.nr_seq_servico
	AND   	e.nr_seq_servico = f.nr_sequencia
	AND   	z.nr_seq_dieta = b.nr_sequencia
	AND 	(((b.ie_suspenso = 'N' or b.ie_suspenso is null) and y.nr_sequencia is null)
		or (y.dt_lib_suspensao IS NULL OR y.dt_lib_suspensao > x.dt_servico) 
		AND x.dt_servico BETWEEN PKG_DATE_UTILS.get_Time(y.dt_inicio, e.ds_horarios) and PKG_DATE_UTILS.get_Time(Nvl(y.dt_fim, x.dt_servico), e.ds_horarios_fim))
	UNION 
	SELECT 	Obter_desc_expressao(304828) 
	FROM   	nut_atend_serv_dia_rep d, 
		nut_atend_serv_dia a, 
		rep_jejum c, 
		prescr_medica e 
	WHERE  	e.nr_prescricao = c.nr_prescricao 
	AND 	a.nr_sequencia = d.nr_seq_serv_dia 
	AND 	d.nr_prescr_jejum = c.nr_prescricao 
	AND 	a.nr_sequencia = nr_seq_serv_dia_p 
	AND 	(nvl(c.ie_suspenso,'N') = 'N' and	a.dt_servico BETWEEN c.dt_inicio and nvl(c.dt_fim,e.dt_validade_prescr))
	ORDER  BY 1; 
	
Cursor C02 is
        SELECT 	c.nm_dieta 
	FROM   	nut_atend_serv_dia_rep d, 
		prescr_medica a, 
		prescr_dieta b, 
		dieta c, 
		nut_atend_serv_dia x, 
		cpoe_dieta y 
	WHERE  	b.nr_prescricao = a.nr_prescricao 
	AND 	c.cd_dieta = b.cd_dieta 
	AND 	d.nr_prescr_oral = a.nr_prescricao 
	AND 	y.nr_sequencia(+) = b.nr_seq_dieta_cpoe 
	AND 	d.nr_seq_serv_dia = nr_seq_serv_dia_p 
	AND 	x.nr_sequencia = d.nr_seq_serv_dia 
	AND 	(((b.ie_suspenso = 'N' or b.ie_suspenso is null) and y.nr_sequencia is null)
		OR ((y.dt_lib_suspensao IS NULL OR y.dt_lib_suspensao > x.dt_servico) 
		AND x.dt_servico BETWEEN y.dt_inicio AND Nvl(y.dt_fim, x.dt_servico))) 
	UNION 
	SELECT 	Obter_desc_expressao(304828) 
	FROM   	nut_atend_serv_dia_rep d, 
		nut_atend_serv_dia a, 
		rep_jejum c, 
		prescr_medica e 
	WHERE  	e.nr_prescricao = c.nr_prescricao 
	AND 	a.nr_sequencia = d.nr_seq_serv_dia 
	AND 	d.nr_prescr_jejum = c.nr_prescricao 
	AND 	a.nr_sequencia = nr_seq_serv_dia_p 
	AND 	(nvl(c.ie_suspenso,'N') = 'N' and	a.dt_servico BETWEEN c.dt_inicio and nvl(c.dt_fim,e.dt_validade_prescr))
	ORDER  BY 1; 

begin

obter_param_usuario(1003, 125, somente_numero(wheb_usuario_pck.get_cd_perfil), wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento,1), ie_prescr_hor_w);

if	( ie_prescr_hor_w = 'S' ) then
	open C01;
	loop
	fetch C01 into	
		ds_dieta_w;
	exit when C01%notfound;
		if	(ds_retorno_w is null) then
			begin
			ds_retorno_w := ds_dieta_w;
			end;
		else
			ds_retorno_w := ds_retorno_w||', '||ds_dieta_w;
		end if;
        
	end loop;
	close C01;
else
	open C02;
	loop
	fetch C02 into	
		ds_dieta_w;
	exit when C02%notfound;
		if	(ds_retorno_w is null) then
			begin
			ds_retorno_w := ds_dieta_w;
			end;
		else
			ds_retorno_w := ds_retorno_w||', '||ds_dieta_w;
		end if;
        
	end loop;
	close C02;
end if;

return	ds_retorno_w;

end nut_obter_dietas_serv_def;
/
