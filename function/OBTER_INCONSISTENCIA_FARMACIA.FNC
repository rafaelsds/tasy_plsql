create or replace
function obter_inconsistencia_farmacia(	nr_prescricao_p			number,
										nr_sequencia_p			number)
								return varchar2 is

ds_inconsistencia_w			varchar2(2000);
nr_seq_inconsistencia_w		number(10);
ds_inconsistencia_aux_w		inconsistencia_med_farm.ds_inconsistencia%type;

cursor	c01 is
select	nr_seq_inconsistencia
from	prescr_material_incon_farm
where	nr_prescricao		= nr_prescricao_p
and		nr_seq_material		= nr_sequencia_p
and		nvl(ie_situacao,'A')	= 'A';

begin

select	max(nr_seq_inconsistencia)
into	nr_seq_inconsistencia_w
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and		nr_sequencia	= nr_sequencia_p;

if	(nr_seq_inconsistencia_w	is not null) then
	select	max(ds_inconsistencia)
	into	ds_inconsistencia_w
	from	inconsistencia_med_farm
	where	nr_sequencia	=	nr_seq_inconsistencia_w;
else

	open C01;
	loop
	fetch C01 into	
		nr_seq_inconsistencia_w;
	exit when C01%notfound;
		begin
		
		select	max(ds_inconsistencia)
		into	ds_inconsistencia_aux_w
		from	inconsistencia_med_farm
		where	nr_sequencia	=	nr_seq_inconsistencia_w;		
		
		if	(ds_inconsistencia_w	is null) then
			ds_inconsistencia_w	:= ds_inconsistencia_aux_w;
		else
			ds_inconsistencia_w	:= substr(ds_inconsistencia_w||';'||ds_inconsistencia_aux_w,1,2000);
		end if;
		
		end;
	end loop;
	close C01;

end if;

return	ds_inconsistencia_w;

end obter_inconsistencia_farmacia;
/