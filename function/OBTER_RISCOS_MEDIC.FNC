create or replace
function Obter_riscos_medic(	cd_material_p	Number)
 		    	return Varchar2 is

ds_risco_w	varchar(255);
ds_retorno_w	varchar2(1000) := obter_desc_expressao(308398)||': ';

Cursor C01 is
select	substr(obter_descricao_padrao('RISCO_MEDICAMENTO','DS_RISCO',nr_seq_risco),1,255)
from	medicamento_risco
where	cd_material = cd_material_p
and	  	nvl(ie_situacao, 'A') = 'A'
order by 1;
			
begin

open C01;
loop
fetch C01 into	
	ds_risco_w;
exit when C01%notfound;
	ds_retorno_w := ds_retorno_w || ds_risco_w || ', ';
end loop;
close C01;

if	(ds_retorno_w = obter_desc_expressao(308398)||': ') then
	ds_retorno_w := '';
end if;

return	substr(ds_retorno_w,1,length(trim(ds_retorno_w))-1);

end Obter_riscos_medic;
/