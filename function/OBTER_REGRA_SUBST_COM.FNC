create or replace
function obter_regra_subst_com(	nr_prescricao_p	number,                               
								cd_material_p	number,
								nr_atendimento_cpoe_p number default null) 
				return number is

cd_material_ret_w	material.cd_material%type;
qt_regra_w		number(15);
cd_material_generico_w	material.cd_material%type;
qt_material_generico_w	number(15);
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
cd_convenio_w		convenio.cd_convenio%type;


begin

cd_material_ret_w	:= 0;

select	max(nr_atendimento)
into	nr_atendimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

if	(nvl(nvl(nr_atendimento_w, nr_atendimento_cpoe_p), 0) > 0) then

	cd_convenio_w := obter_convenio_atendimento(nvl(nr_atendimento_w,nr_atendimento_cpoe_p));

	if	(nvl(cd_convenio_w, 0) > 0) then

		select	count(*)
		into	qt_regra_w
		from	rep_regra_subst_com
		where	cd_convenio		= cd_convenio_w
		and	nvl(ie_situacao, 'A')	<> 'I';

		if	(qt_regra_w > 0) then

			select	max(cd_material_generico)
			into	cd_material_generico_w
			from	material
			where	cd_material		= cd_material_p
			and	ie_tipo_material	= 2
			and	cd_material_generico	is not null;

			if	(nvl(cd_material_generico_w, 0) > 0) then

				select	count(*)
				into	qt_material_generico_w
				from	material
				where	cd_material		= cd_material_generico_w
				and	ie_tipo_material	= 3
				and	ie_situacao	 	= 'A'
				and	ie_prescricao		= 'S';

				if	(qt_material_generico_w > 0) then

					cd_material_ret_w	:= cd_material_generico_w;

				end if;

			end if;

		end if;

	end if;

end if;

return	cd_material_ret_w;

end	obter_regra_subst_com;
/
