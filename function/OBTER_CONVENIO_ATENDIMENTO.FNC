CREATE OR REPLACE
FUNCTION Obter_Convenio_Atendimento(
					nr_atendimento_p			number)
					RETURN Number IS

cd_convenio_w			Number(5,0);

BEGIN

select nvl(max(cd_convenio),0)
into cd_convenio_w
from 	Atend_categoria_convenio a
where a.nr_atendimento		= nr_atendimento_p
  and a.dt_inicio_vigencia	= 
	(select max(dt_inicio_vigencia)
	from Atend_categoria_convenio b
	where nr_atendimento	= nr_atendimento_p);

RETURN cd_convenio_w;
END Obter_Convenio_Atendimento;
/
