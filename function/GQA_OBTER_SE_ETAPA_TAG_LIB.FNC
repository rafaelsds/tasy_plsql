create or replace function gqa_obter_se_etapa_tag_lib(ie_funcao_p varchar2, nr_seq_protocolo_p number)
  return varchar2 is

resposta             varchar2(1) := 'S';
qtd                  number(10);
nr_sequencia_atual_w gqa_protocolo_etapa_pac.nr_sequencia%type;
nr_seq_etapa_atual_w gqa_protocolo_etapa_pac.nr_seq_etapa%type;
begin
  begin
    select nr_sequencia, nr_seq_etapa into nr_sequencia_atual_w, nr_seq_etapa_atual_w from (
        select dt_inicio, nr_sequencia, nr_seq_etapa from gqa_protocolo_etapa_pac 
        where nr_seq_prot_pac = nr_seq_protocolo_p and dt_inicio is not null
        order by dt_inicio desc
    ) where rownum = 1;
  exception when no_data_found then
    nr_sequencia_atual_w := null;
    nr_seq_etapa_atual_w := null;
  end;
  
  if(nr_sequencia_atual_w is not null and nr_seq_etapa_atual_w is not null) then
    select count(1) into qtd from gqa_pendencia_regra_tag where nr_seq_gqa_regra = nr_seq_etapa_atual_w;
    
    if(qtd = 0) then --se nao tiver nada cadastrado para esse protocolo, retorna sim.
      return 'S';
    else --tem cadastrado, fazer consistencias.
      select  decode(count(1),0,'N','S')
        into  resposta
        from  gqa_pendencia_regra_tag
        where nr_seq_gqa_regra = nr_seq_etapa_atual_w and nvl(ie_funcao,ie_funcao_p) = ie_funcao_p
        and    (cd_perfil is null
        or    cd_perfil = wheb_usuario_pck.get_cd_perfil)
        and    (cd_estabelecimento is null
        or    cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento)
        and    (cd_setor_atendimento is null
        or    cd_setor_atendimento = wheb_usuario_pck.get_cd_setor_atendimento)
        and    (cd_especialidade is null
        or    cd_especialidade = obter_especialidade_pf(obter_pf_usuario(wheb_usuario_pck.get_nm_usuario, 'C')));

      return resposta;
    end if;
  else
    return 'S';    
  end if;
end gqa_obter_se_etapa_tag_lib;
/
