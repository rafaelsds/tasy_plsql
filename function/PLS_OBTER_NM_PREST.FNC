create or replace
function pls_obter_nm_prest(	cd_prestador_p	varchar2)
					return	varchar2 is
					
/* Function usada para atualizar o campo descritivo de um WDLG com o nome do prestador, 
recebe como par�metro o c�digo que retorna do localizador*/

ds_retorno_w			varchar2(255);
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;

begin

begin
select	max(nr_sequencia)
into	nr_seq_prestador_w
from	pls_prestador
where	cd_prestador = cd_prestador_p;
exception when no_data_found then
	nr_seq_prestador_w	:= 0;
end;

if	(nr_seq_prestador_w > 0) then
	ds_retorno_w	:= substr(pls_obter_dados_prestador(nr_seq_prestador_w,'N'),1,255);
else
	ds_retorno_w	:= '';
end if;
	
return	ds_retorno_w;

end pls_obter_nm_prest;
/