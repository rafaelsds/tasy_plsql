create or replace
function sup_obter_se_desdobra_req(	nr_requisicao_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1) := 'N';
ie_desdobrar_w		varchar2(1);
qt_existe_w		number(10);

cd_material_w		number(6);
cd_local_estoque_w	number(4);
cd_operacao_estoque_w	number(3);
cd_estabelecimento_w	number(4);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
nr_seq_familia_w	number(10);
cd_local_destino_w	requisicao_material.cd_local_estoque_destino%type;
ie_controlado_w		varchar2(1);

cursor	c01 is
	select	b.cd_material,
		e.cd_grupo_material,
		e.cd_subgrupo_material,
		e.cd_classe_material,
		e.nr_seq_familia,
		a.cd_local_estoque,
		a.cd_operacao_estoque,
		a.cd_estabelecimento,
		a.cd_local_estoque_destino,
		substr(obter_se_medic_controlado(b.cd_material),1,1)
	from	requisicao_material a,
		item_requisicao_material b,
		estrutura_material_v e
	where	a.nr_requisicao = nr_requisicao_p
	and	a.nr_requisicao = b.nr_requisicao
	and	b.cd_material = e.cd_material;
		
begin

open C01;
loop
fetch C01 into	
	cd_material_w,
	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	nr_seq_familia_w,
	cd_local_estoque_w,
	cd_operacao_estoque_w,
	cd_estabelecimento_w,
	cd_local_destino_w,
	ie_controlado_w;
exit when C01%notfound;
	begin
	
	select	max(ie_desdobrar)
	into	ie_desdobrar_w
	from(	select	ie_desdobrar
		from	regra_desdobramento_req
		where	ie_situacao = 'A'
		and	cd_estabelecimento = cd_estabelecimento_w
		and	nvl(cd_material,cd_material_w) = cd_material_w
		and	nvl(cd_grupo_material,cd_grupo_material_w) = cd_grupo_material_w
		and	nvl(cd_subgrupo_material,cd_subgrupo_material_w) = cd_subgrupo_material_w
		and	nvl(cd_classe_material,cd_classe_material_w) = cd_classe_material_w
		and	nvl(cd_operacao_estoque,cd_operacao_estoque_w) = cd_operacao_estoque_w
		and	nvl(cd_local_estoque,cd_local_estoque_w) = cd_local_estoque_w
		and	nvl(nr_seq_familia,nvl(nr_seq_familia_w,0)) = nvl(nr_seq_familia_w,0)
		and	nvl(cd_local_destino,nvl(cd_local_destino_w,0)) = nvl(cd_local_destino_w,0)
		and	((nvl(ie_controlado,'A') = 'A') or (nvl(ie_controlado, 'A') = ie_controlado_w))
		order by cd_material,
			nr_seq_familia,
			cd_classe_material,
			cd_subgrupo_material,
			cd_grupo_material,
			cd_local_estoque,
			cd_local_destino,
			ie_controlado desc,
			cd_operacao_estoque)
	where	rownum = 1;
	
	if	(ie_desdobrar_w = 'S') then
		ds_retorno_w := 'S';
		exit;
	end if;
	
	end;
end loop;
close C01;

return ds_retorno_w;

end sup_obter_se_desdobra_req;
/