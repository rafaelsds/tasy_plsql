create or replace
function obter_se_atend_conta_aberto	(nr_atendimento_p	number)
						return varchar2 is

qt_conta_atend_w	number(10,0);
ie_conta_aberto_w	varchar2(1) := 'S';
ie_perm_conta_def_w	varchar2(1);
begin

Obter_param_Usuario(866, 173, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_perm_conta_def_w);

if	(nr_atendimento_p is not null) then
	if	((obter_funcao_ativa <> 866 and obter_funcao_ativa <> 281) or (ie_perm_conta_def_w = 'N')) then
		/* obter se atend conta */
		select	count(*)
		into	qt_conta_atend_w
		from	conta_paciente
		where	nr_atendimento = nr_atendimento_p;

		/* obter se conta aberto atend */
		if	(qt_conta_atend_w > 0) then
			select	decode(count(*),0,'N','S')
			into	ie_conta_aberto_w
			from	conta_paciente
			where	nr_atendimento = nr_atendimento_p
			and	ie_status_acerto = 1;
		else
			ie_conta_aberto_w := 'S';
		end if;
	end if;
end if;

return ie_conta_aberto_w;

end obter_se_atend_conta_aberto;
/
