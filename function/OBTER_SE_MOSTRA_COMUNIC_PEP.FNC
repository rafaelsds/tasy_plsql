Create or replace 
function Obter_Se_Mostra_Comunic_pep(	nr_sequencia_p	Number,
					nm_usuario_p	Varchar2)
				return varchar2 is

ie_mostra_w		varchar2(001)	:= 'S';

nm_usuario_w		Varchar2(15);
nm_usuario_ww		Varchar2(15);
nm_usuario_www		Varchar2(255);
ie_geral_w		Varchar2(01);
nm_usuario_dest_w		Varchar2(4000);
ds_setor_adicional_w	varchar2(2000);
ie_gerencial_w		Varchar2(01);
nr_seq_classif_w		Number(10,0);
ds_perfil_adic_w		Varchar2(4000);
cd_perfil_w		Number(05,0);
ie_pessoal_w		Varchar2(01);
cd_setor_w		Number(05,0);
cd_estab_w		Number(05,0);
cont_w			Number(05,0);
ie_Usuario_Dest_w		Varchar2(01);
cd_setor_ww		Number(05,0);
cd_estab_ww		Number(05,0);
cd_posicao_w		Varchar2(10) := ' ';
qt_posicao_w		Number(10,0);
ds_grupo_w		varchar2(255);
nr_seq_grupo_w		number(10,0);
nm_usuario_oculto_w	Varchar2(15);
qt_controle_w		number(10);
BEGIN

select	cd_setor_atendimento,
	cd_estabelecimento
into	cd_setor_ww,
	cd_estab_ww
from 	Usuario
where	nm_usuario		= nm_usuario_p;

select	nm_usuario,
	nvl(ie_geral,'N'),
	nm_usuario_destino,
	nvl(ie_gerencial,'N'),
	cd_perfil,
	nvl(nr_seq_classif,0),
	ds_perfil_adicional,
	ds_setor_adicional,
	cd_setor_destino,
	cd_estab_destino,
	ds_grupo,
	nm_usuario_oculto
into	nm_usuario_w,
	ie_geral_w,
	nm_usuario_dest_w,
	ie_gerencial_w,
	cd_perfil_w,
	nr_seq_classif_w,
	ds_perfil_adic_w,
	ds_setor_adicional_w,
	cd_setor_w,
	cd_estab_w,
	ds_grupo_w,
	nm_usuario_oculto_w
from 	comunic_interna
where	nr_sequencia	= nr_sequencia_p;


ie_Usuario_Dest_w		:= 'N';
qt_posicao_w			:= 99999;
if	(nm_usuario_dest_w is not null) and
	(nm_usuario_dest_w like '%' || nm_usuario_p  || '%') then
	nm_usuario_dest_w		:= replace(nm_usuario_dest_w, ' ', '') || ',';
	nm_usuario_ww			:= replace(nm_usuario_p,' ', '');
	qt_controle_w			:= 0;
	while	(qt_posicao_w <> 0) and
			(ie_usuario_dest_w <> 'S') and 
			(qt_controle_w < 500) loop
		qt_controle_w := qt_controle_w + 1;
		qt_posicao_w := instr(nm_usuario_dest_w,',');
		if	(qt_posicao_w <> 0) then
			nm_usuario_www	:= substr(nm_usuario_dest_w, 1, qt_posicao_w -1);			
			nm_usuario_dest_w	:= substr(nm_usuario_dest_w, qt_posicao_w + 1, 2000);			
			if	(nm_usuario_www = nm_usuario_ww) then
				ie_usuario_dest_w	:= 'S';
			end if;
		end if;
	end loop;
end if;

ie_mostra_w			:= 'N';
ie_pessoal_w			:= ie_mostra_w;
if	(nm_usuario_p	= nm_usuario_w) then
	ie_mostra_w		:= 'S';
	ie_pessoal_w		:= ie_mostra_w;
elsif	(nm_usuario_p	= nm_usuario_oculto_w) then
	ie_mostra_w		:= 'S';
	ie_pessoal_w		:= ie_mostra_w;
elsif	(ie_Usuario_Dest_w	= 'S') then
	ie_mostra_w		:= 'S';
	ie_pessoal_w		:= ie_mostra_w;
elsif	(ie_geral_w		= 'S') then
	ie_mostra_w		:= 'S';
elsif	((ds_perfil_adic_w is not null) or (ds_setor_adicional_w is not null) or (ds_grupo_w is not null)) and
	(ie_mostra_w <> 'S') then

	begin
	if	(ds_perfil_adic_w is not null) and
		(ie_mostra_w <> 'S') then
		begin
		qt_controle_w := 0;
		while	(cd_posicao_w <> '0') and
				(ie_mostra_w <> 'S') and 
				(qt_controle_w < 500) loop
			begin
			qt_controle_w := qt_controle_w + 1;
			cd_posicao_w := instr(ds_perfil_adic_w,',');
			cd_perfil_w	:= to_number(substr(ds_perfil_adic_w,1,cd_posicao_w-1));			

			select	nvl(max('S'),'N')	
			into	ie_mostra_w
			from	Usuario_perfil
			where	nm_usuario	= nm_usuario_p
			and	cd_perfil	= cd_perfil_w;
			
			ds_perfil_adic_w	:= substr(ds_perfil_adic_w,to_number(cd_posicao_w) + 1,length(ds_perfil_adic_w));
			
			end;
		end loop;

		end;
	end if;

	if	(ds_setor_adicional_w is not null) and
		(ie_mostra_w <> 'S') then
		begin
		cd_posicao_w	:= ' ';
		qt_controle_w	:= 0;
		while	(cd_posicao_w <> '0') and
				(ie_mostra_w <> 'S') and 
				(qt_controle_w < 500) loop
			begin
			qt_controle_w := qt_controle_w + 1;
			cd_posicao_w := instr(ds_setor_adicional_w,',');
			cd_setor_w	:= to_number(substr(ds_setor_adicional_w,1,cd_posicao_w-1));			

			select	nvl(max('S'),'N')
			into	ie_mostra_w
			from 	Usuario
			where	nm_usuario		= nm_usuario_p
			and	cd_setor_atendimento	= cd_setor_w;
			
			ds_setor_adicional_w	:= substr(ds_setor_adicional_w,to_number(cd_posicao_w) + 1,length(ds_setor_adicional_w));
			
			end;
		end loop;
	
		end;
	end if;

	if	(ds_grupo_w is not null) and 		-- Edgar 15/02/2005, OS 15007, fiz rotina abaixo
		(ie_mostra_w <> 'S') then

		cd_posicao_w	:= ' ';
		qt_controle_w 	:= 0;
		while	(cd_posicao_w <> '0') and
				(ie_mostra_w <> 'S') and
				(qt_controle_w < 500) loop
			qt_controle_w 	:= qt_controle_w + 1;
			cd_posicao_w	:= instr(ds_grupo_w,',');
			if	(somente_numero(ds_grupo_w) > 0) then -- Francisco 14/08/2005 OS 39156 
				if	(nvl(cd_posicao_w,'0') = '0') then	-- Edgar 11/08/2005 OS 22009 tratamento se n�o tiver v�rgula
					nr_seq_grupo_w	:= to_number(ds_grupo_w);
				else
					nr_seq_grupo_w	:= to_number(substr(ds_grupo_w,1,cd_posicao_w-1));
				end if;
			end if;

			select	nvl(max('S'),'N')
			into	ie_mostra_w
			from 	usuario_grupo
			where	nm_usuario_grupo	= nm_usuario_p
			and	nr_seq_grupo		= nr_seq_grupo_w;
			
			ds_grupo_w	:= nvl(substr(ds_grupo_w,to_number(cd_posicao_w) + 1,length(ds_grupo_w)), ' ');
			select	decode(ds_grupo_w,' ','0',ds_grupo_w)
			into	ds_grupo_w
			from	dual;
			/*Fabio OS_22072 23/08/2005*/

		end loop;
	end if;

	end;
end if;

/* Inclui este select para ver se o usu�rio tem o estabelecimento da mensagem liberado */
select	count(*)
into	cont_w
from	USUARIO_ESTABELECIMENTO
where	nm_usuario_param	= nm_usuario_p
and	cd_estabelecimento	= cd_estab_w;

if	(ie_pessoal_w	= 'N') and
	(ie_mostra_w	= 'S') and (cont_w = 0) and
	(nvl(cd_estab_w,cd_estab_ww) <> cd_estab_ww) then
	ie_mostra_w		:= 'N';
end if;

if	(ie_mostra_w	= 'S') then
	select	nvl(max('N'),'S')
	into	ie_mostra_w
	from	comunic_interna_lida
	where	nr_sequencia	= nr_sequencia_p
	and	nm_usuario	= nm_usuario_p;
end if;

return	nvl(ie_mostra_w,'N');

end Obter_Se_Mostra_Comunic_pep;
/