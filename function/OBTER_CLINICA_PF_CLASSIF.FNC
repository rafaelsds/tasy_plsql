create or replace	
function Obter_clinica_PF_Classif(
			cd_funcao_p		number,
			cd_agenda_p		number,
			cd_pessoa_fisica_p		varchar2,
			dt_referencia_p		date,
			cd_estabelecimento_p	number)
 		    	return varchar2 is

qt_regra_w		number(10);
qt_regra_sem_classif_w	number(10,0);
ds_retorno_w		varchar2(80);
nr_seq_classif_w		number(10);
ie_clinica_w		number(5);
qt_perm_w		number(10);
nr_seq_classif_ww		number(10);
ds_classificacao_w		varchar2(80);
ds_classif_retorno_w	varchar2(80);
qt_regra_agenda_w		number(10);
cd_perfil_w		number(5);
qt_especialidade_w		number(10);
cd_especialidade_w	varchar2(80);
ie_perm_espec_w		varchar2(1);
cd_especialidade_regra_w	 number(10);
nr_Seq_Classif_pf_W	number(10);
ds_classif_esp_ret_w	varchar2(80);
ie_consiste_classif_w	varchar2(1);

Cursor C01 is
	select	ie_clinica,
		nr_sequencia
	from	pessoa_classif
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	((dt_referencia_p >= dt_inicio_vigencia) or (dt_inicio_vigencia is null))
	and	((Dt_referencia_p <= dt_final_vigencia) or (dt_final_vigencia is null))
	and	((cd_estabelecimento = cd_estabelecimento_p) or (cd_estabelecimento is null))
	order by ie_clinica;
	
Cursor C02 is
	select	cd_especialidade
	from	pessoa_classif_espec
	where	nr_seq_classif_pf	= nr_Seq_Classif_pf_W
	and	((dt_referencia_p >= dt_inicio_vigencia) or (dt_inicio_vigencia is null))
	and	((Dt_referencia_p <= dt_fim_vigencia) or (dt_fim_vigencia is null))
	and	ie_perm_espec_w		= 'N'
	order by nr_sequencia;
	
begin

cd_perfil_w := Obter_perfil_Ativo;


select	max(cd_especialidade)
into	cd_especialidade_w
from	agenda
where	cd_agenda	= cd_agenda_p;

	
	open C01;
	loop 
	fetch C01 into	
		ie_clinica_w,
		nr_Seq_Classif_pf_W;
	exit when C01%notfound;
		begin
		ie_clinica_w			:=	ie_clinica_w;
		
			if	(cd_especialidade_w is not null)then
				select	nvl(max(ie_classif_pf_agenda), 'N')
				into	ie_consiste_classif_w
				from	especialidade_medica
				where	cd_especialidade	= cd_especialidade_w;
			end if;	
			ie_perm_espec_w	:=	'N';
			
			select	count(*)
			into	qt_especialidade_w
			from	pessoa_classif_espec
			where	nr_seq_classif_pf	= nr_Seq_Classif_pf_W;
			
			if	(ie_consiste_classif_w	= 'S') and
				(qt_especialidade_w	> 0) and
				(cd_especialidade_w > 0)	then
				open C02;
				loop
				fetch C02 into	
					cd_especialidade_regra_w;
				exit when C02%notfound;
					begin
					if	(cd_especialidade_regra_w	= cd_Especialidade_w) then
						ie_perm_espec_w	:= 'S';
					end if;
					end;
				end loop;
				close C02;
			else
				ie_perm_espec_w	:= 'S';
			end if;			
		end;
	end loop;
	close C01;
	

if	(((ie_perm_espec_w = 'S') or
	(qt_especialidade_w = 0)) and
	(ie_clinica_w <> 0)) then
	begin
	ds_retorno_w	:=	ie_clinica_w;
	end;
end if;

return	ds_retorno_w;

end Obter_clinica_PF_Classif;
/ 
