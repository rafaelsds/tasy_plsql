create or replace
function obter_se_prescr_dieta(nr_prescricao_p		number)
 		    	return varchar2 is

ie_dieta_w		varchar2(1);
				
begin

select	decode(sum(qt),0,'N','S')
into	ie_dieta_w
from	(select	count(nr_sequencia) qt
		from	prescr_dieta
		where	nr_prescricao	= nr_prescricao_p
		union all	
		select	count(nr_sequencia) qt
		from	rep_jejum
		where	nr_prescricao	= nr_prescricao_p
		union all
		select	count(nr_sequencia) qt
		from	prescr_leite_deriv
		where	nr_prescricao	= nr_prescricao_p);

return	ie_dieta_w;

end obter_se_prescr_dieta;
/