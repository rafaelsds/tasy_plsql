create or replace
function obter_dados_pre_natal(	nr_sequencia_p	Number,					
				ie_opcao_p	Varchar2)
					return Number is



					
qt_peso_prim_consulta_w	Number(6,3);

begin

if	(nr_sequencia_p is not null) then

	if	(ie_opcao_p	= 'PP') then
		select	max(qt_peso_prim_consulta)
		into	qt_peso_prim_consulta_w
		from	med_pac_pre_natal
		where	nr_sequencia	= nr_sequencia_p;
		
		return qt_peso_prim_consulta_w;
	
	end if;
	
end if;

return null;

end obter_dados_pre_natal;
/
