create or replace 
function TISS_OBTER_SE_PROC_CONTA(nr_seq_regra_guia_proc_p	number,
				  nr_interno_conta_p	number)
						return varchar2   is

cd_area_procedimento_w	number(15);
cd_especialidade_w	number(15);
cd_grupo_proc_w		number(15);
cd_procedimento_w	number(15);
cd_tipo_procedimento_w	number(3);
ie_origem_proced_w	number(10);
cd_grupo_proc_ww		number(15);
cd_especialidade_ww	number(15);
cd_area_procedimento_ww	number(15);
cd_tipo_procedimento_ww	number(3);
cd_procedimento_ww	number(15);
ie_origem_proced_ww	number(10);
ds_retorno_w		varchar2(15);
qt_regra_w		number(10);

cursor c01 is
select	cd_area_procedimento,
	cd_especialidade,
	cd_grupo_proc,
	cd_procedimento,
	cd_tipo_procedimento,
	ie_origem_proced
from	tiss_regra_guia_conta a
where	a.nr_seq_regra		= nr_seq_regra_guia_proc_p
order by	nvl(cd_area_procedimento,0),
	nvl(cd_especialidade,0),
	nvl(cd_grupo_proc,0),
	nvl(cd_procedimento,0),
	nvl(cd_tipo_procedimento,0);

cursor c02 is
select	distinct cd_procedimento,
	ie_origem_proced
from	procedimento_paciente
where	nr_interno_conta	= nr_interno_conta_p
and	cd_motivo_exc_conta	is null;

begin

ds_retorno_w		:= null;
qt_regra_w	:= 0;

if	(nr_seq_regra_guia_proc_p is not null) then

	begin
	select	1
	into	qt_regra_w
	from	tiss_regra_guia_conta
	where	nr_seq_regra	= nr_seq_regra_guia_proc_p
	and	rownum		= 1;
	exception
	when others then
		qt_regra_w	:= 0;
	end;
end if;

if	(qt_regra_w > 0) then	

	open c01;
	loop
	fetch c01 into
		cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		cd_procedimento_w,
		cd_tipo_procedimento_w,
		ie_origem_proced_w;
	exit when c01%notfound or ds_retorno_w = 'S';

		ds_retorno_w		:= 'N';

		open c02;
		loop
		fetch c02 into
			cd_procedimento_ww,
			ie_origem_proced_ww;
		exit when c02%notfound or ds_retorno_w = 'S';
							
			select	cd_grupo_proc,
				cd_especialidade,
				cd_area_procedimento,
				cd_tipo_procedimento
			into	cd_grupo_proc_ww,
				cd_especialidade_ww,
				cd_area_procedimento_ww,
				cd_tipo_procedimento_ww
			from	estrutura_procedimento_v
			where	cd_procedimento		= cd_procedimento_ww
			and	ie_origem_proced	= ie_origem_proced_ww;

			if	(cd_tipo_procedimento_w is not null) and
				(cd_tipo_procedimento_w = cd_tipo_procedimento_ww) then
				ds_retorno_w		:= 'S';
			end if;
			if	(cd_area_procedimento_w is not null) and 
				(cd_area_procedimento_w = cd_area_procedimento_ww) then
				ds_retorno_w		:= 'S';
			end if;
			if	(cd_especialidade_w is not null) and
				(cd_especialidade_w = cd_especialidade_ww) then
				ds_retorno_w		:= 'S';
			end if;
			if	(cd_grupo_proc_w is not null) and
				(cd_grupo_proc_w = cd_grupo_proc_ww) then
				ds_retorno_w		:= 'S';
			end if;
			if	(cd_procedimento_w is not null) and
				(cd_procedimento_w = cd_procedimento_ww) then
				ds_retorno_w		:= 'S';
			end if;

		end loop;
		close c02;

	end loop;
	close c01;
end if;

return	ds_retorno_w;

end TISS_OBTER_SE_PROC_CONTA;
/