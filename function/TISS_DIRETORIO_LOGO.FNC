create or replace 
function TISS_DIRETORIO_LOGO(	ds_dir_logo_comp_p varchar2,
				ds_dir_padrao_p 	 varchar2 ) return varchar2 is
				
ds_dir_logo_comp_w		varchar2(255);
ds_dir_padrao_w			varchar(255);
ds_dir_com_w			varchar(255);
ds_dir_pad_w			varchar(255);
ds_posicao_w			number(10);

begin

ds_dir_logo_comp_w 	:= ds_dir_logo_comp_p;
ds_dir_com_w		:= ds_dir_logo_comp_p;
ds_dir_padrao_w		:= ds_dir_padrao_p;
ds_dir_pad_w		:= ds_dir_padrao_p;

if	(instr(ds_dir_logo_comp_w,chr(92)) > 0) then		
	ds_posicao_w := length(ds_dir_logo_comp_w);			
	if	(chr(92) <> substr(ds_dir_logo_comp_w,ds_posicao_w,ds_posicao_w)) then
		ds_dir_com_w := ds_dir_logo_comp_w || chr(92);
	end if;		
elsif	(instr(ds_dir_logo_comp_w,chr(47)) > 0) then
	ds_posicao_w := length(ds_dir_logo_comp_w);		
	if	(chr(47) <> substr(ds_dir_logo_comp_w,ds_posicao_w,ds_posicao_w)) then
		ds_dir_com_w := ds_dir_logo_comp_w || chr(47);
	end if;
end if;

if	(instr(ds_dir_padrao_w,chr(92)) > 0) then 
		ds_posicao_w := length(ds_dir_padrao_w);
	if	(chr(92) <> substr(ds_dir_padrao_w,ds_posicao_w,ds_posicao_w)) then
		ds_dir_pad_w := ds_dir_padrao_w || chr(92);
	end if;		
elsif	(instr(ds_dir_padrao_w,chr(47)) > 0) then 
	ds_posicao_w := length(ds_dir_padrao_w);
	if	(chr(47) <> substr(ds_dir_padrao_w,ds_posicao_w,ds_posicao_w)) then
		ds_dir_pad_w := ds_dir_padrao_w || chr(47);
	end if;		
end if;

return nvl(ds_dir_com_w,ds_dir_pad_w);

end TISS_DIRETORIO_LOGO;
/
