create or replace
function obter_se_regra_elem_cont(nr_sequencia_p number) 
return	varchar2 is

qt_reg_w	 number(3);

begin

select	count(*)
into	qt_reg_w
from	EHR_TEMP_CONTEUDO_REGRA	
where	NR_SEQ_TEMP_SUP = nr_sequencia_p;

if	(qt_reg_w > 0) then
	return	'S';
else
	return	'N';
end	if;

end;
/