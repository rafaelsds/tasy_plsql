create or replace 
function OBTER_CONTA_PAG_PESSOA (cd_pessoa_fisica_p	varchar2,
				 cd_cgc_p		varchar2,
				cd_estabelecimento_p	number) return varchar2 is
nr_conta_w	varchar2(50);

cursor c01 is
select	nr_conta
from	pessoa_fisica_conta a
where	(not exists
	(select	1
	from	pessoa_fis_conta_estab x
	where	x.nr_conta		= a.nr_conta
	and	x.cd_agencia_bancaria	= a.cd_agencia_bancaria
	and	x.cd_banco		= a.cd_banco
	and	x.cd_pessoa_fisica	= a.cd_pessoa_fisica) or
	exists
	(select	1
	from	pessoa_fis_conta_estab x
	where	x.cd_estabelecimento	= cd_estabelecimento_p
	and	x.nr_conta		= a.nr_conta
	and	x.cd_agencia_bancaria	= a.cd_agencia_bancaria
	and	x.cd_banco		= a.cd_banco
	and	x.cd_pessoa_fisica	= a.cd_pessoa_fisica))
and	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	ie_conta_pagamento	= 'S'
and	ie_situacao		= 'A'
union
select	nr_conta
from	pessoa_juridica_conta a
where	(not exists
	(select	1
	from	pessoa_jur_conta_estab x
	where	x.nr_conta		= a.nr_conta
	and	x.cd_agencia_bancaria	= a.cd_agencia_bancaria
	and	x.cd_banco		= a.cd_banco
	and	x.cd_cgc		= a.cd_cgc) or
	exists
	(select	1
	from	pessoa_jur_conta_estab x
	where	x.cd_estabelecimento	= cd_estabelecimento_p
	and	x.nr_conta		= a.nr_conta
	and	x.cd_agencia_bancaria	= a.cd_agencia_bancaria
	and	x.cd_banco		= a.cd_banco
	and	x.cd_cgc		= a.cd_cgc))
and	cd_cgc			= cd_cgc_p
and	ie_conta_pagamento	= 'S'
and	ie_situacao		= 'A';

begin

open c01;
loop
fetch c01 into
	nr_conta_w;
exit when c01%notfound;
	nr_conta_w	:= nr_conta_w;
end loop;
close c01;

return nr_conta_w;

end OBTER_CONTA_PAG_PESSOA;
/