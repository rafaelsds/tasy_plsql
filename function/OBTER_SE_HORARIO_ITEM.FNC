create or replace
function Obter_se_horario_item(ds_horarios_p		varchar2,
				cd_material_p		number)
 		    	return varchar2 is

ie_horario_item_w	varchar2(1) := 'S';
ds_horarios_w		varchar2(2000);
hr_inicio_w		varchar2(5);
hr_fim_w		varchar2(5);
ds_hora_w		varchar2(10);
k			number(10,0);

cursor c01 is
select	hr_inicio,
	hr_final
from	material_horario_padrao
where	cd_material	= cd_material_p;

begin

if	(ds_horarios_p	<> 'ACM') and
	(ds_horarios_p	<> 'SN') then
	
	open c01;
	loop
	fetch c01 into
		hr_inicio_w,
		hr_fim_w;
	exit when c01%notfound;

		ds_horarios_w	:= trim(ds_horarios_p);
		ds_horarios_w	:= ds_horarios_w ||' ';

		while	ds_horarios_w is not null LOOP
			begin
			select	instr(ds_horarios_w, ' ') 
			into	k
			from 	dual;
			
			if	(k > 1) and
				(substr(ds_horarios_w, 1, k -1) is not null) then
				begin
				ds_hora_w		:= substr(ds_horarios_w, 1, k-1);
				ds_hora_w		:= replace(ds_hora_w, ' ','');
				ds_horarios_w		:= substr(ds_horarios_w, k + 1, 2000);
				
				if	(instr(ds_hora_w,':') = 0) then
					ds_hora_w	:= ds_hora_w || ':00';
				end if;
							
				if	(ds_hora_w < hr_inicio_w) or
					(ds_hora_w > hr_fim_w) then
					ie_horario_item_w := 'N';
					exit;
				end if;

				end;
			elsif	(ds_horarios_w is not null) then
				ds_horarios_w		:= '';
			end if;			
			end;
		END LOOP;
		
	end loop;
	close c01;
end if;

return	ie_horario_item_w;

end Obter_se_horario_item;
/
