create or replace
procedure pls_gerar_arq_lote_mov(  	NR_SEQ_MOV_OPERADORA_P  	pls_mov_benef_operadora.nr_sequencia%type,
					DS_ARQUIVO_P  			pls_mov_benef_operadora.ds_arquivo%type,
					NM_USUARIO_P			varchar2	) is
begin

if    (NR_SEQ_MOV_OPERADORA_P is not null) then
	update  pls_mov_benef_operadora
	set     ds_arquivo     		= DS_ARQUIVO_P,
		dt_atualizacao		= sysdate,
		nm_usuario		= NM_USUARIO_P
	where   nr_sequencia     	= NR_SEQ_MOV_OPERADORA_P;

	commit;
end if;

end pls_gerar_arq_lote_mov;
/
