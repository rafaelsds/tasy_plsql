create or replace
function Obter_richtext_base(	ds_conteudo_p		varchar2,
				ie_alinhamento_p	varchar2,
				ds_fonte_p		varchar2,
				qt_tamanho_p		varchar2)
 		    	return varchar2 is

ie_alinhamento_w	varchar2(1);
ds_conteudo_w		varchar2(3500);
ds_retorno_w		varchar2(4000);
qt_tamanho_w		number(3) := qt_tamanho_p * 2;

			
begin

if	(ie_alinhamento_p = 'E') then
	ie_alinhamento_w := 'l';
elsif	(ie_alinhamento_p = 'D') then
	ie_alinhamento_w := 'r';
elsif	(ie_alinhamento_p = 'C') then
	ie_alinhamento_w := 'c';
else	ie_alinhamento_w := 'j';
end if;



ds_conteudo_w := replace(ds_conteudo_p,chr(10),chr(13) || chr(10) || '\par ');

ds_retorno_w := '{\rtf1\ansi\ansicpg1252\deff0{\fonttbl{\f0\fnil\fcharset0 ' || ds_fonte_p || ';}}' 	|| chr(13) || chr(10) ||
		'{\colortbl ;\red0\green0\blue;}' 							|| chr(13) || chr(10) ||
			'\viewkind4\uc1\pard\q' || ie_alinhamento_w || '\cf1\lang1046\f0\fs' || qt_tamanho_w || ' ' || 
			ds_conteudo_w || ' \par }' || chr(13) || chr(10);
		 
return	ds_retorno_w;

end Obter_richtext_base;
/