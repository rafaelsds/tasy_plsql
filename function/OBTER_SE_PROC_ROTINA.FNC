create or replace
function Obter_se_proc_rotina(	cd_medico_p  	number,
				ie_tipo_atendimento_p 	number,
				cd_setor_atendimento_p	number,
				cd_classif_setor_pac_p	number,
				ie_clinica_p		number,
				cd_setor_prescr_p	number,
				cd_convenio_p		number,
				nr_sequencia_p		number,
				cd_estabelecimento_p	number,
				cd_perfil_p		number,
				ie_tipo_convenio_p	number default null) 
				return varchar is


ds_retorno_w			char(1);
cd_perfil_w			perfil.cd_perfil%type;

Cursor C01 is
select	nvl(ie_permite, 'N')
from	procedimento_rotina_lib a
where	nr_seq_proc_rotina 		= nr_sequencia_p
and	nvl(cd_perfil, cd_perfil_w)	= cd_perfil_w
and	nvl(nvl(cd_estabelecimento, cd_estabelecimento_p), 999)	= nvl(nvl(cd_estabelecimento_p, cd_estabelecimento),999)
and	(exists(	select	b.cd_especialidade
				from	medico_especialidade b
				where	b.cd_especialidade = a.cd_especialidade
				and	b.cd_pessoa_fisica = cd_medico_p) or (a.cd_especialidade is null))
and	((ie_tipo_atendimento	= ie_tipo_atendimento_p)  or (ie_tipo_atendimento is null)) 
and	((cd_setor_atendimento  = cd_setor_atendimento_p) or (cd_setor_atendimento is null))
and	((ie_tipo_convenio	= ie_tipo_convenio_p) or (ie_tipo_convenio is null))
and	((cd_classif_setor_pac  = cd_classif_setor_pac_p) or (cd_classif_setor_pac is null))
and	((ie_clinica		= ie_clinica_p)		or (ie_clinica is null))  
and	((cd_setor_prescr	= cd_setor_prescr_p)	or (cd_setor_prescr is null))  
and	((cd_convenio		= cd_convenio_p)	or (cd_convenio is null))
and 	((nr_seq_agrupamento	= obter_agrupamento_setor(cd_setor_prescr_p)) or (nr_seq_agrupamento is null))
order by nvl(cd_especialidade,0),
	nvl(ie_tipo_atendimento,0),
	nvl(cd_setor_atendimento,0),
	nvl(cd_classif_setor_pac,0),
	nvl(cd_perfil,0),
	nvl(ie_clinica,0),
	nvl(cd_setor_prescr,0),
	nvl(cd_convenio,0),
	nvl(ie_tipo_convenio,0),
	nr_sequencia;

begin

select	decode(count(*),0,'S','N') 
into	ds_retorno_w
from	procedimento_rotina_lib
where 	nr_seq_proc_rotina = nr_sequencia_p;

if (cd_perfil_p is null) then
	cd_perfil_w := obter_perfil_ativo;
else
	cd_perfil_w := cd_perfil_p;
end if;

if	(ds_retorno_w = 'N') then
	open C01;
	loop
	fetch C01 into	
		ds_retorno_w;
	exit when C01%notfound;
		ds_retorno_w := ds_retorno_w;	
		
	end loop;
	close C01;
end if;	  
 
return	ds_retorno_w;

end Obter_se_proc_rotina;
/