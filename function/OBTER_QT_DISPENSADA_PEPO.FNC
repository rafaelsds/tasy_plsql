create or replace
function obter_qt_dispensada_pepo	(nr_prescricao_p	number,
					cd_material_p		number,
					nr_seq_lote_fornec_p	number,
					ie_operacao_p		varchar)
					return	number is
					
qt_dispensado_w	number(18,6);					
				
begin

select	nvl(sum(b.qt_dispensacao),0)
into	qt_dispensado_w
from	cirurgia a,
	cirurgia_agente_disp b
where	a.nr_cirurgia			= b.nr_cirurgia
and	a.nr_prescricao 		= nr_prescricao_p
and	b.ie_operacao			= ie_operacao_p
and	b.cd_material			= cd_material_p
and	nvl(b.nr_seq_lote_fornec,0)	= nvl(nr_seq_lote_fornec_p,0);

return	qt_dispensado_w;
				
end obter_qt_dispensada_pepo;
/