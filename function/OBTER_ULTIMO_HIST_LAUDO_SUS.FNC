CREATE OR REPLACE 
FUNCTION Obter_ultimo_hist_laudo_sus
			(	nr_seq_laudo_p	number)
				return varchar is

ds_retorno_w	varchar2(255);

BEGIN

select	substr(ds_historico,1,255)
into	ds_retorno_w
from	sus_laudo_paciente_hist
where	nr_sequencia = (	select	max(nr_sequencia) 
				from 	sus_laudo_paciente_hist 
				where 	nr_seq_laudo	= nr_seq_laudo_p);

RETURN ds_retorno_w;

END Obter_ultimo_hist_laudo_sus;
/