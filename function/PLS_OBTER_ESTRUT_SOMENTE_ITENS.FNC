create or replace
function pls_obter_estrut_somente_itens
                        (       nr_seq_estrutura_p      	number,
                                nr_seq_guia_p           	number,
                                nr_seq_conta_p          	number,
				nr_seq_proc_p			number,
				nr_seq_mat_p			number,
				cd_procedimento_p		number,
				ie_origem_proced_p		number)
                                return varchar2 is

nr_seq_conta_proc_w		Number(10);
nr_seq_guia_proc_w		Number(10);
ie_estrutura_w			varchar2(1)	:= 'N';
ie_retorno_w                    Varchar2(1)     := 'N';
cd_procedimento_w               Number(15) := 0;
cd_area_procedimento_w          Number(15) := 0;
cd_especialidade_w              Number(15) := 0;
cd_grupo_proc_w                 Number(15) := 0;
ie_origem_proced_w              Number(10);
ie_origem_proced_ww             Number(10);
nr_seq_estrut_mat_w		Number(10);
nr_seq_material_w		Number(10);
nr_seq_conta_mat_w		Number(10);
nr_seq_regra_estrutura_w	Number(10);
nr_seq_guia_mat_w		Number(10);
qt_itens_estrutura_w		Number(10);
qt_itens_procs_mats_w		Number(10) 	:= 0;
ie_existe_fora_estrut_w		varchar2(1)	:= 'N';
cd_guia_w			varchar2(20);
cd_guia_referencia_w		varchar2(20);
nr_seq_segurado_w		Number(10);
cd_area_w			number(15);
cd_especialidade_ww		number(15);
cd_grupo_w			number(15);
ie_origem_proced_out_w		number(15);
ie_existe_pacote_w		varchar2(1)	:= 'N';

Cursor C01 is
        select  nr_sequencia,
		cd_procedimento,
                ie_origem_proced
        from    pls_guia_plano_proc
        where   nr_seq_guia = nr_seq_guia_p
        order by 1;

Cursor C02 is
        select  a.nr_sequencia,
		a.cd_procedimento,
                a.ie_origem_proced
        from    pls_conta_proc a,
		pls_conta b
        where   a.nr_seq_conta = b.nr_sequencia
	and	b.nr_seq_segurado = nr_seq_segurado_w
	and	((a.nr_seq_conta = nr_seq_conta_p)
	or	 (b.cd_guia = cd_guia_referencia_w)
	or	 (b.cd_guia_referencia = cd_guia_w))
        order by 1;

Cursor C04 is
        select  a.nr_sequencia,
		a.nr_seq_material
        from    pls_conta_mat a,
		pls_conta b
        where   a.nr_seq_conta = b.nr_sequencia
	and	b.nr_seq_segurado = nr_seq_segurado_w
	and	((a.nr_seq_conta = nr_seq_conta_p)
	or	 (b.cd_guia = cd_guia_referencia_w)
	or	 (b.cd_guia_referencia = cd_guia_w))
        order by 1;        

Cursor C05 is
        select  nr_sequencia,
		nr_seq_material
        from    pls_guia_plano_mat
        where   nr_seq_guia = nr_seq_guia_p
        order by 1;

begin
/* Defini��o; Somente itens da regra
     -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    Para a ocorr�ncia ser v�lida deve haver todos os procedimentos informados na estrutura, e no campo procedimento se houver, mais procedimentos que n�o est�o na regra.

   No caso de uma regra Autoriza��o / Conta � gerado  ocorr�ncia na guia/conta por haver mais do que os procedimentos cadastrados em regra

   No caso de uma regra Itens � gerado ocorr�ncia nos Itens que n�o est�o cadastrados na regra.

    OS 309863 - Diego - 25/04/2011  - Ajustado para que o sistema verifique e considere como parte de estruturas os materiais cadastrados na pasta estruturas;
			- Ajustado tamb�m para que o sistema somente verifique os campos Procedimentos e materiais.
			Isto foi realizado porque ao cadastrar uma ar�a na estrutura por exemplo esta se dizendo que todos os procedimento da �rea fazem parte da estrutura.
			Assim para ser gerado a ocorr�ncia por somente os itens das regras a conta teria que apresentar todos os procedimentos da �rea, o que seria inviavel.

    OS 309863 - Diego - 18/06/2011 - Ajustado para que seja necess�rio haver todos os procedimentos e materiais da regra para a ocorr�ncia se tornar v�lida.

    Retorno = 'S' Se deve gerar ocorr�ncia
	'N' Se n�o feve gerar ocorrencia

   Obs: O procedimento da regra (cd_procedimento_p) funciona como uma adendo da estrutura ou seja
             Caso exista funciona como se fosse parte da estrutura
*/

/*Verificado quantas regras existem nas estrutura (valendo somente procedimento e material)*/
select	count(*)
into	qt_itens_estrutura_w
from	pls_ocorrencia_estrut_item
where	((cd_procedimento is not null) or (nr_seq_material is not null))
and	nr_seq_estrutura = nr_seq_estrutura_p
and	ie_estrutura = 'S';

if	(cd_procedimento_p is not null) then
	qt_itens_estrutura_w := qt_itens_estrutura_w + 1;
end if;

if	(nvl(nr_seq_conta_p,0) > 0) then
	select	nr_seq_segurado,
		cd_guia,
		cd_guia_referencia
	into	nr_seq_segurado_w,
		cd_guia_w,
		cd_guia_referencia_w
	from	pls_conta
	where	nr_sequencia = nr_seq_conta_p;
	/* OS 466061 - Alterado tratamento para quando for conta m�dica */
	/*Obtem os procedimentos da conta.*/
	open C02;
	loop
	fetch C02 into
		nr_seq_conta_proc_w,
		cd_procedimento_w,
		ie_origem_proced_w;
	exit when C02%notfound;
		begin
		pls_obter_estrut_proc(cd_procedimento_w,ie_origem_proced_w, cd_area_w, cd_especialidade_ww, cd_grupo_w, ie_origem_proced_out_w);
		
		select  decode(count(nr_sequencia),0,'N','S')
		into	ie_estrutura_w
		from    pls_ocorrencia_estrut_item a
		where   nr_seq_estrutura        = nr_seq_estrutura_p
		and     nvl(a.cd_procedimento,cd_procedimento_w) 	= cd_procedimento_w
		and     nvl(a.ie_origem_proced,ie_origem_proced_out_w) 	= ie_origem_proced_out_w
		and	nvl(a.cd_grupo_proc,cd_grupo_w) = cd_grupo_w
		and	nvl(a.cd_especialidade,cd_especialidade_ww) = cd_especialidade_ww
		and	nvl(a.cd_area_procedimento,cd_area_w) = cd_area_w
		and	ie_estrutura =  'S';

		/*Se a regra selecionda diz que este procedimento n�o faz parte da estrtura*/
		if	(nvl(ie_estrutura_w,'N') = 'N') then
			/*Se este procedimento n�o faz parte da regra � verificado se este procedimento faz parte da regra(vide observa��o no inicio da rotina)*/
			--if	((cd_procedimento_w  = cd_procedimento_p) and
			--	 (ie_origem_proced_w = ie_origem_proced_p))then
				qt_itens_procs_mats_w := qt_itens_procs_mats_w + 1;
			--else
			ie_existe_fora_estrut_w := 'S';
			--end if;
		else
			ie_existe_pacote_w	:= 'S';
			qt_itens_procs_mats_w	:= qt_itens_procs_mats_w + 1;
		end if;
		end;
	end loop;
	close C02;

	/*Obtem os mats da conta*/
	open C04;
	loop
	fetch C04 into
		nr_seq_conta_mat_w,
		nr_seq_material_w;
	exit when C04%notfound;
		begin
		select	max(a.nr_seq_estrut_mat)
		into	nr_seq_estrut_mat_w
		from	pls_material	a
		where	a.nr_sequencia	= nr_seq_material_w;
		
		select  decode(count(nr_sequencia),0,'N','S')
		into	ie_estrutura_w
		from    pls_ocorrencia_estrut_item a
		where   nr_seq_estrutura        = nr_seq_estrutura_p
		and	nvl(a.nr_seq_material,nr_seq_material_w)	= nr_seq_material_w
		and	nvl(a.nr_seq_estrut_mat,nr_seq_estrut_mat_w)	= nr_seq_estrut_mat_w
		and	((a.nr_seq_estrut_mat is not null) or (a.nr_seq_material is not null))
		and	ie_estrutura =  'S';

		/*Se a regra selecionda diz que este procedimento n�o faz parte da estrtura*/
		if	(nvl(ie_estrutura_w,'N') = 'N') then
			ie_existe_fora_estrut_w := 'S';
		else
			ie_existe_pacote_w	:= 'S';
			qt_itens_procs_mats_w	:= qt_itens_procs_mats_w + 1;
		end if;
		end;
	end loop;
	close C04;
elsif	(nvl(nr_seq_guia_p,0) > 0) then
	/*Obtem os procedimentos da guia.*/
	open C01;
	loop
	fetch C01 into
		nr_seq_guia_proc_w,
		cd_procedimento_w,
		ie_origem_proced_w;
	exit when C01%notfound;
		begin
		/*Obtem se o procedimento faz parte da estrutura*/
		select  decode(count(nr_sequencia),0,'N','S')
		into	ie_estrutura_w
		from    pls_ocorrencia_estrut_item a
		where   nr_seq_estrutura        = nr_seq_estrutura_p
		and     a.cd_procedimento 	= cd_procedimento_w
		and     a.ie_origem_proced 	= ie_origem_proced_w
		and	ie_estrutura =  'S';

		/*Se a regra selecionda diz que este procedimento n�o faz parte da estrtura*/
		if	(nvl(ie_estrutura_w,'N') = 'N') then
			/*Se este procedimento n�o faz parte da regra � verificado se este procedimento faz parte da regra(vide observa��o no inicio da rotina)*/
			if	((cd_procedimento_w  = cd_procedimento_p) and
				(ie_origem_proced_w = ie_origem_proced_p))then
				qt_itens_procs_mats_w := qt_itens_procs_mats_w + 1;
			else
				ie_existe_fora_estrut_w := 'S';
			end if;
		else
			qt_itens_procs_mats_w := qt_itens_procs_mats_w + 1;
		end if;
		end;
	end loop;
	close C01;

	/*Obtem os mats da conta*/
	open C05;
	loop
	fetch C05 into
		nr_seq_guia_mat_w,
		nr_seq_material_w;
	exit when C05%notfound;
		begin
		select  decode(count(nr_sequencia),0,'N','S')
		into	ie_estrutura_w
		from    pls_ocorrencia_estrut_item a
		where   nr_seq_estrutura        = nr_seq_estrutura_p
		and	nr_seq_material		= nr_seq_material_w
		and	ie_estrutura =  'S';

		/*Se a regra selecionda diz que este procedimento n�o faz parte da estrtura*/
		if	(nvl(ie_estrutura_w,'N') = 'N') then
			ie_existe_fora_estrut_w := 'S';
		else
			qt_itens_procs_mats_w := qt_itens_procs_mats_w + 1;
		end if;
		end;
	end loop;
	close C05;
end if;

if	(nvl(nr_seq_guia_p,0) > 0) then
	if	(qt_itens_procs_mats_w = qt_itens_estrutura_w) and
		(ie_existe_fora_estrut_w = 'S') then
		ie_retorno_w := 'S';
	end if;
else
	if	(ie_existe_fora_estrut_w = 'S') and
		(ie_existe_pacote_w = 'S') then
		ie_retorno_w := 'S';
	end if;
end if;

if	(ie_retorno_w = 'S') then
	if	(nvl(nr_seq_guia_p,0) > 0) then
		if	(nvl(nr_seq_proc_p,0) > 0) then
			select  cd_procedimento,
				ie_origem_proced
			into	cd_procedimento_w,
				ie_origem_proced_w
			from	pls_guia_plano_proc
			where   nr_seq_guia	= nr_seq_guia_p
			and	nr_sequencia	= nr_seq_proc_p;
		elsif	(nvl(nr_seq_mat_p,0) > 0) then
			select  nr_seq_material
			into	nr_seq_material_w
			from    pls_guia_plano_mat
			where   nr_seq_guia	= nr_seq_guia_p
			and	nr_sequencia	= nr_seq_mat_p;
		end if;
	elsif	(nvl(nr_seq_conta_p,0) > 0) then
		if	(nvl(nr_seq_proc_p,0) > 0) then
			select 	cd_procedimento,
				ie_origem_proced
			into	cd_procedimento_w,
				ie_origem_proced_w
			from    pls_conta_proc
			where   nr_seq_conta	= nr_seq_conta_p
			and	nr_sequencia	= nr_seq_proc_p;
		elsif	(nvl(nr_seq_mat_p,0) > 0) then
			select  nr_seq_material
			into	nr_seq_material_w
			from    pls_conta_mat
			where   nr_seq_conta	= nr_seq_conta_p
			and	nr_sequencia	= nr_seq_mat_p;
		end if;
	end if;
	
	if	(nvl(nr_seq_guia_p,0) > 0) then
		if	(nvl(nr_seq_proc_p,0) > 0) then
			select  decode(count(nr_sequencia),0,'S','N')
			into	ie_retorno_w
			from    pls_ocorrencia_estrut_item a
			where   nr_seq_estrutura        = nr_seq_estrutura_p
			and     a.cd_procedimento 	= cd_procedimento_w
			and     a.ie_origem_proced 	= ie_origem_proced_w
			and	ie_estrutura =  'S';

			if	((cd_procedimento_w  = cd_procedimento_p) and
				 (ie_origem_proced_w = ie_origem_proced_p))then
				ie_retorno_w := 'N';
			end if;
		elsif	(nvl(nr_seq_mat_p,0) > 0) then
			select  decode(count(nr_sequencia),0,'S','N')
			into	ie_retorno_w
			from    pls_ocorrencia_estrut_item a
			where   nr_seq_estrutura        = nr_seq_estrutura_p
			and	nr_seq_material		= nr_seq_material_w
			and	ie_estrutura =  'S';
		end if;
	else
		if	(nvl(nr_seq_proc_p,0) > 0) then
			pls_obter_estrut_proc(cd_procedimento_w,ie_origem_proced_w, cd_area_w, cd_especialidade_ww, cd_grupo_w, ie_origem_proced_out_w);
			
			select  decode(count(nr_sequencia),0,'S','N')
			into	ie_retorno_w
			from    pls_ocorrencia_estrut_item a
			where   nr_seq_estrutura        = nr_seq_estrutura_p
			and     nvl(a.cd_procedimento,cd_procedimento_w) 	= cd_procedimento_w
			and     nvl(a.ie_origem_proced,ie_origem_proced_out_w) 	= ie_origem_proced_out_w
			and	nvl(a.cd_grupo_proc,cd_grupo_w) = cd_grupo_w
			and	nvl(a.cd_especialidade,cd_especialidade_ww) = cd_especialidade_ww
			and	nvl(a.cd_area_procedimento,cd_area_w) = cd_area_w
			and	ie_estrutura =  'S';
		elsif	(nvl(nr_seq_mat_p,0) > 0) then
			select	max(a.nr_seq_estrut_mat)
			into	nr_seq_estrut_mat_w
			from	pls_material	a
			where	a.nr_sequencia	= nr_seq_material_w;
			
			select  decode(count(nr_sequencia),0,'S','N')
			into	ie_retorno_w
			from    pls_ocorrencia_estrut_item a
			where   nr_seq_estrutura        = nr_seq_estrutura_p
			and	nvl(a.nr_seq_material,nr_seq_material_w)	= nr_seq_material_w
			and	nvl(a.nr_seq_estrut_mat,nr_seq_estrut_mat_w)	= nr_seq_estrut_mat_w
			and	((a.nr_seq_estrut_mat is not null) or (a.nr_seq_material is not null))
			and	ie_estrutura =  'S';
		end if;
	end if;
end if;

return  ie_retorno_w;

end pls_obter_estrut_somente_itens;
/