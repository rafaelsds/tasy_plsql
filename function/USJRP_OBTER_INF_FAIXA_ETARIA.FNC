create or replace
function usjrp_obter_inf_faixa_etaria
			(	qt_idade_inicial_p	number,
				qt_idade_final_p	number,
				dt_inicio_p		varchar2,
				dt_fim_p		varchar2,
				nr_contrato_p		number,
				nr_seq_plano_p		number,
				nr_titulo_p		number,
				nr_contrato_in_p	number,
				nr_contrato_fi_p	number,
				nr_familia_in_p		number,
				nr_familia_fi_p		number,
				ie_opcao_p		varchar2)
				return number is
				
/* ie_opcao_p
	A - Fat. Anterior
	I - Mov Incl.
	E - Mov. Excl.
	B - Atual
	C - Valor da coparticipa��o por faixa et�ria;
	T - Valor da mensalidade por faixa et�ria;
	U - Valor unit�rio por faixa et�ria;
	D - Valor de desconto por faixa et�ria;
	EA - Eventos de acr�scimo
	QMR -  Quantidade de mensalidades retroativas
	VUMR - Valor unit�rio das mensalidades retroativas
	VMR - Valor total de mensalidades retroativas
*/
				
ds_retorno_w			number(15,2);
ie_mes_cobranca_reaj_reg_w	varchar2(1);

begin

if	(ie_opcao_p = 'A') then

	select	count(a.nr_sequencia) 
	into	ds_retorno_w
	from	pls_segurado_carteira		e,
		pls_contrato			d,
		pls_plano			c,
		pessoa_fisica			b,
		pls_segurado			a
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	a.nr_seq_plano 		= c.nr_sequencia 
	and	a.nr_seq_contrato 	= d.nr_sequencia 
	and	a.nr_Sequencia 		= e.nr_seq_segurado
	and	c.nr_sequencia		= nr_seq_plano_p
	and	a.dt_contratacao <= to_date(dt_inicio_p,'dd/mm/yyyy') - 1 
	and	(d.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	trunc(months_between(sysdate, b.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));
	
elsif	(ie_opcao_p = 'I') then

	select	count(a.nr_sequencia) 
	into	ds_retorno_w
	from	pls_segurado_carteira		e,
		pls_contrato			d,
		pls_plano			c,
		pessoa_fisica			b,
		pls_segurado			a
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	a.nr_seq_plano 		= c.nr_sequencia 
	and	a.nr_seq_contrato 	= d.nr_sequencia 
	and	a.nr_Sequencia 		= e.nr_seq_segurado
	and	c.nr_sequencia		= nr_seq_plano_p
	and	a.dt_contratacao between to_date(dt_inicio_p,'dd/mm/yyyy') - 1 and to_date(dt_fim_p,'dd/mm/yyyy') - 1
	and	(d.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	trunc(months_between(sysdate, b.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));
	
elsif	(ie_opcao_p = 'E') then

	select	count(a.nr_sequencia) 
	into	ds_retorno_w
	from	pls_segurado_carteira		e,
		pls_contrato			d,
		pls_plano			c,
		pessoa_fisica			b,
		pls_segurado			a
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	a.nr_seq_plano 		= c.nr_sequencia 
	and	a.nr_seq_contrato 	= d.nr_sequencia
	and	a.nr_sequencia 		= e.nr_seq_segurado	
	and	c.nr_sequencia		= nr_seq_plano_p
	and	(a.dt_rescisao between to_date(dt_inicio_p,'dd/mm/yyyy') - 1 and to_date(dt_fim_p,'dd/mm/yyyy') - 1 or
		 a.dt_cancelamento between to_date(dt_inicio_p,'dd/mm/yyyy') - 1 and to_date(dt_fim_p,'dd/mm/yyyy') - 1)
	and	(d.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	trunc(months_between(sysdate, b.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));
	
elsif	(ie_opcao_p = 'B') then

	select	count(a.nr_sequencia) 
	into	ds_retorno_w
	from	pls_segurado_carteira		e,
		pls_contrato			d,
		pls_plano			c,
		pessoa_fisica			b,
		pls_segurado			a
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	a.nr_seq_plano 		= c.nr_sequencia 
	and	a.nr_seq_contrato 	= d.nr_sequencia
	and	a.nr_sequencia 		= e.nr_seq_segurado	
	and	c.nr_sequencia		= nr_seq_plano_p
	and	a.dt_rescisao is null
	and	a.dt_cancelamento is null
	and	d.nr_contrato = nr_contrato_p
	and	trunc(months_between(sysdate, b.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));
	
elsif	(ie_opcao_p = 'C') then

	select	sum(b.vl_coparticipacao) 
	into	ds_retorno_w
	from	titulo_receber			h,
		pls_contrato			g,
		pls_plano			f,
		pls_segurado_carteira		e,
		pessoa_fisica			d,
		pls_segurado			c,
		pls_mensalidade_segurado	b,
		pls_mensalidade			a
	where	a.nr_sequencia		= b.nr_seq_mensalidade
	and	b.nr_seq_segurado 	= c.nr_sequencia
	and	c.cd_pessoa_fisica 	= d.cd_pessoa_fisica
	and	c.nr_sequencia 		= e.nr_seq_segurado
	and	c.nr_seq_plano 		= f.nr_sequencia 
	and	c.nr_seq_contrato 	= g.nr_sequencia 
	and	a.nr_sequencia		= h.nr_seq_mensalidade
	and	f.nr_sequencia		= nr_seq_plano_p
	and	h.nr_titulo = nr_titulo_p 
	and	a.dt_referencia between to_date(dt_inicio_p,'dd/mm/yyyy') and to_date(dt_fim_p,'dd/mm/yyyy')
	and	(g.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	trunc(months_between(sysdate, d.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));
	
elsif	(ie_opcao_p = 'T') then

	select	sum(b.vl_mensalidade) 
	into	ds_retorno_w
	from	titulo_receber			h,
		pls_contrato			g,
		pls_plano			f,
		pls_segurado_carteira		e,
		pessoa_fisica			d,
		pls_segurado			c,
		pls_mensalidade_segurado	b,
		pls_mensalidade			a
	where	a.nr_sequencia		= b.nr_seq_mensalidade
	and	b.nr_seq_segurado 	= c.nr_sequencia
	and	c.cd_pessoa_fisica 	= d.cd_pessoa_fisica
	and	c.nr_sequencia 		= e.nr_seq_segurado
	and	c.nr_seq_plano 		= f.nr_sequencia 
	and	c.nr_seq_contrato 	= g.nr_sequencia 
	and	f.nr_sequencia		= nr_seq_plano_p
	and	a.nr_sequencia		= h.nr_seq_mensalidade
	and	h.nr_titulo = nr_titulo_p 
	and	a.dt_referencia between to_date(dt_inicio_p,'dd/mm/yyyy') and to_date(dt_fim_p,'dd/mm/yyyy')
	and	(g.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	trunc(months_between(sysdate, d.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));
	
elsif	(ie_opcao_p = 'U') then

	select	nvl(max(ie_mes_cobranca_reaj_reg),'P')
	into	ie_mes_cobranca_reaj_reg_w
	from	pls_parametros
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	select	sum(h.vl_preco_atual) 
	into	ds_retorno_w
	from	pls_segurado_preco		h,
		pls_contrato			g,
		pls_plano			f,
		pls_segurado_carteira		e,
		pessoa_fisica			d,
		pls_segurado			c
	where	c.cd_pessoa_fisica 	= d.cd_pessoa_fisica
	and	c.nr_sequencia 		= e.nr_seq_segurado
	and	c.nr_seq_plano 		= f.nr_sequencia 
	and	c.nr_seq_contrato 	= g.nr_sequencia 
	and	c.nr_sequencia 		= h.nr_seq_segurado
	and	f.nr_sequencia		= nr_seq_plano_p
	and	(g.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	trunc(months_between(sysdate, d.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	h.nr_sequencia = (	select	max(x.nr_sequencia)			
					from	pls_segurado_preco x
					where	x.nr_seq_segurado = c.nr_sequencia
					and	(((trunc(sysdate,'month') >= trunc(x.dt_reajuste,'month'))
					and	((x.cd_motivo_reajuste <> 'E') or (ie_mes_cobranca_reaj_reg_w = 'M'))) 
					or	(((x.cd_motivo_reajuste = 'E') and (trunc(sysdate,'month') >= trunc(add_months(x.dt_reajuste,1), 'month')))
					and	(ie_mes_cobranca_reaj_reg_w = 'P')))	)
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));	

elsif	(ie_opcao_p = 'D') then

	select	nvl(max(ie_mes_cobranca_reaj_reg),'P')
	into	ie_mes_cobranca_reaj_reg_w
	from	pls_parametros
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	select	sum(h.vl_desconto) 
	into	ds_retorno_w
	from	pls_segurado_preco		h,
		pls_contrato			g,
		pls_plano			f,
		pls_segurado_carteira		e,
		pessoa_fisica			d,
		pls_segurado			c
	where	c.cd_pessoa_fisica 	= d.cd_pessoa_fisica
	and	c.nr_sequencia 		= e.nr_seq_segurado
	and	c.nr_seq_plano 		= f.nr_sequencia 
	and	c.nr_seq_contrato 	= g.nr_sequencia 
	and	c.nr_sequencia 		= h.nr_seq_segurado
	and	f.nr_sequencia		= nr_seq_plano_p
	and	(g.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	trunc(months_between(sysdate, d.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	h.nr_sequencia = (	select	max(x.nr_sequencia)			
					from	pls_segurado_preco x
					where	x.nr_seq_segurado = c.nr_sequencia
					and	(((trunc(sysdate,'month') >= trunc(x.dt_reajuste,'month'))
					and	((x.cd_motivo_reajuste <> 'E') or (ie_mes_cobranca_reaj_reg_w = 'M'))) 
					or	(((x.cd_motivo_reajuste = 'E') and (trunc(sysdate,'month') >= trunc(add_months(x.dt_reajuste,1), 'month')))
					and	(ie_mes_cobranca_reaj_reg_w = 'P')))	)
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));	
					
elsif	(ie_opcao_p = 'EA') then

	select	sum(h.vl_item) 
	into	ds_retorno_w
	from	titulo_receber			i,
		pls_mensalidade_seg_item	h,
		pls_contrato			g,
		pls_plano			f,
		pls_segurado_carteira		e,
		pessoa_fisica			d,
		pls_segurado			c,
		pls_mensalidade_segurado	b,
		pls_mensalidade			a
	where	a.nr_sequencia		= b.nr_seq_mensalidade
	and	b.nr_seq_segurado 	= c.nr_sequencia
	and	c.cd_pessoa_fisica 	= d.cd_pessoa_fisica
	and	c.nr_sequencia 		= e.nr_seq_segurado
	and	c.nr_seq_plano 		= f.nr_sequencia 
	and	c.nr_seq_contrato 	= g.nr_sequencia 
	and	h.nr_seq_mensalidade_seg = b.nr_sequencia
	and	a.nr_sequencia 		= i.nr_seq_mensalidade
	and	i.nr_titulo = nr_titulo_p
	and	h.ie_tipo_mensalidade 	= 'A'
	and	f.nr_sequencia		= nr_seq_plano_p
	and	a.dt_referencia between to_date(dt_inicio_p,'dd/mm/yyyy') and to_date(dt_fim_p,'dd/mm/yyyy')
	and	(g.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));
	
elsif	(ie_opcao_p = 'QMR') then

	select	count(*)
	into	ds_retorno_w
	from	pls_segurado_carteira		h,
		pls_mensalidade_seg_item	g,
		pls_contrato			f,
		pls_segurado			e,
		pls_mensalidade_segurado	d,
		pls_mensalidade			c,
		pls_regra_mens_contrato		b,
		titulo_receber			a
	where	a.nr_seq_mensalidade 	= c.nr_sequencia
	and	c.nr_sequencia 		= d.nr_seq_mensalidade
	and	d.nr_seq_segurado 	= e.nr_sequencia
	and	e.nr_seq_contrato 	= f.nr_sequencia
	and	d.nr_sequencia  	= g.nr_seq_mensalidade_seg
	and	e.nr_sequencia 		= h.nr_seq_segurado
	and	a.nr_titulo = nr_titulo_p 
	and	b.nr_sequencia = (	select	max(x.nr_sequencia)
					from	pls_regra_mens_contrato	x
					where	x.nr_seq_contrato is null
					and	x.nr_seq_intercambio is null
					and	x.ie_tipo_regra		= 'L'
					and	trunc(c.dt_referencia,'dd') >= trunc(x.dt_inicio_vigencia,'dd')
					and	trunc(c.dt_referencia,'dd') <= trunc(nvl(x.dt_fim_vigencia,c.dt_referencia),'dd')	)
	and	g.ie_tipo_mensalidade = 'N'
	and	g.ie_tipo_item = '1'
	and	nvl(g.vl_item,0) <> 0
	and	e.nr_seq_plano = nr_seq_plano_p
	and	c.dt_referencia between to_date(dt_inicio_p,'dd/mm/yyyy') and to_date(dt_fim_p,'dd/mm/yyyy')
	and	(f.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	(substr(h.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(h.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));
	
elsif	(ie_opcao_p = 'VUMR') then

	select	nvl(max(ie_mes_cobranca_reaj_reg),'P')
	into	ie_mes_cobranca_reaj_reg_w
	from	pls_parametros
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	select	sum(h.vl_preco_atual) 
	into	ds_retorno_w
	from	pls_segurado_preco		h,
		pls_contrato			g,
		pls_plano			f,
		pls_segurado_carteira		e,
		pessoa_fisica			d,
		pls_segurado			c
	where	c.cd_pessoa_fisica 	= d.cd_pessoa_fisica
	and	c.nr_sequencia 		= e.nr_seq_segurado
	and	c.nr_seq_plano 		= f.nr_sequencia 
	and	c.nr_seq_contrato 	= g.nr_sequencia 
	and	c.nr_sequencia 		= h.nr_seq_segurado
	and	f.nr_sequencia		= nr_seq_plano_p
	and	(g.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	trunc(months_between(sysdate, d.dt_nascimento) / 12) between qt_idade_inicial_p and qt_idade_final_p
	and	h.nr_sequencia = (	select	max(x.nr_sequencia)			
					from	pls_segurado_preco x
					where	x.nr_seq_segurado = c.nr_sequencia
					and	(((trunc(sysdate,'month') >= trunc(x.dt_reajuste,'month'))
					and	((x.cd_motivo_reajuste <> 'E') or (ie_mes_cobranca_reaj_reg_w = 'M'))) 
					or	(((x.cd_motivo_reajuste = 'E') and (trunc(sysdate,'month') >= trunc(add_months(x.dt_reajuste,1), 'month')))
					and	(ie_mes_cobranca_reaj_reg_w = 'P')))	)
	and	exists(	select	1
			from	pls_mensalidade_seg_item	u,
				pls_segurado			t,
				pls_mensalidade_segurado	s,
				pls_mensalidade			z,
				pls_regra_mens_contrato		y,
				titulo_receber			x
			where	x.nr_seq_mensalidade = z.nr_sequencia
			and	z.nr_sequencia = s.nr_seq_mensalidade
			and	s.nr_seq_segurado = c.nr_sequencia
			and	u.nr_seq_mensalidade_seg = s.nr_sequencia
			and	x.nr_titulo = nr_titulo_p
			and	y.nr_sequencia = (	select	max(m.nr_sequencia)
							from	pls_regra_mens_contrato	m
							where	m.nr_seq_contrato is null
							and	m.nr_seq_intercambio is null
							and	m.ie_tipo_regra		= 'L'
							and	trunc(z.dt_referencia,'dd') >= trunc(m.dt_inicio_vigencia,'dd')
							and	trunc(z.dt_referencia,'dd') <= trunc(nvl(m.dt_fim_vigencia,z.dt_referencia),'dd')	)
			and	u.ie_tipo_mensalidade = 'N'
			and	u.ie_tipo_item = '1'
			and	nvl(u.vl_item,0) <> 0
			and	z.dt_referencia between to_date(dt_inicio_p,'dd/mm/yyyy') and trunc(to_date(dt_fim_p,'dd/mm/yyyy')))
	and	(substr(e.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(e.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));		

elsif	(ie_opcao_p = 'VMR') then	

	select	sum(d.vl_mensalidade)
	into	ds_retorno_w
	from	pls_segurado_carteira		h,
		pls_mensalidade_seg_item	g,
		pls_contrato			f,
		pls_segurado			e,
		pls_mensalidade_segurado	d,
		pls_mensalidade			c,
		pls_regra_mens_contrato		b,
		titulo_receber			a
	where	a.nr_seq_mensalidade 	= c.nr_sequencia
	and	c.nr_sequencia 		= d.nr_seq_mensalidade
	and	d.nr_seq_segurado 	= e.nr_sequencia
	and	e.nr_seq_contrato 	= f.nr_sequencia
	and	d.nr_sequencia		= g.nr_seq_mensalidade_seg
	and	e.nr_sequencia 		= h.nr_seq_segurado
	and	a.nr_titulo = nr_titulo_p
	and	b.nr_sequencia = (	select	max(x.nr_sequencia)
					from	pls_regra_mens_contrato	x
					where	x.nr_seq_contrato is null
					and	x.nr_seq_intercambio is null
					and	x.ie_tipo_regra		= 'L'
					and	trunc(c.dt_referencia,'dd') >= trunc(x.dt_inicio_vigencia,'dd')
					and	trunc(c.dt_referencia,'dd') <= trunc(nvl(x.dt_fim_vigencia,c.dt_referencia),'dd')	)
	and	g.ie_tipo_mensalidade = 'N'
	and	g.ie_tipo_item = '1'
	and	nvl(g.vl_item,0) <> 0
	and	e.nr_seq_plano = nr_seq_plano_p
	and	c.dt_referencia between to_date(dt_inicio_p,'dd/mm/yyyy') and to_date(dt_fim_p,'dd/mm/yyyy')
	and	(f.nr_contrato = nr_contrato_p or nvl(nr_contrato_p,0) = 0)
	and	(substr(h.cd_usuario_plano,5,4) between nr_contrato_in_p and nr_contrato_fi_p or (nvl(nr_contrato_in_p,0) = 0 and nvl(nr_contrato_fi_p,0) = 0))
	and	(substr(h.cd_usuario_plano,9,6) between nr_familia_in_p and nr_familia_fi_p or (nvl(nr_familia_in_p,0) = 0 and nvl(nr_familia_fi_p,0) = 0));	
	
end if;	

if	(nvl(ds_retorno_w,0) = 0) then
	ds_retorno_w := 0;
end if;

return	ds_retorno_w;

end usjrp_obter_inf_faixa_etaria;
/