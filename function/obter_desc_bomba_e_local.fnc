create or replace function obter_desc_bomba_e_local(nr_prescricao_p  number,
                                                    nr_atendimento_p number)
  return varchar2 is

  ds_desc_w varchar2(500);

  nr_seq_equipamento_w bomba_infusao.nr_seq_equipamento%type;
  nr_seq_canal_bomba_w bomba_infusao.nr_seq_canal_bomba%type;

  ds_canal_bomba_w man_equipamento_bomba.ds_descricao%type;

begin

  select a.nr_seq_equipamento, a.nr_seq_canal_bomba
    into nr_seq_equipamento_w, nr_seq_canal_bomba_w
    from bomba_infusao a
   where a.ie_ativo = 'S'
     and a.nr_prescricao = nr_prescricao_p
     and a.nr_atendimento = nr_atendimento_p
     and a.nr_sequencia =
         (select max(b.nr_sequencia)
            from bomba_infusao b
           where a.nr_seq_equipamento = b.nr_seq_equipamento
             and nvl(a.NR_SEQ_CANAL_BOMBA, 0) = nvl(b.NR_SEQ_CANAL_BOMBA, 0))
     and rownum = 1;

  if (nr_seq_equipamento_w is not null) then
  
    if (nr_seq_canal_bomba_w is not null) then
      select ' - ' || a.ds_descricao
        into ds_canal_bomba_w
        from man_equipamento_bomba a
       where a.nr_sequencia = nr_seq_canal_bomba_w
         and a.nr_equipamento = nr_seq_equipamento_w;
    end if;
  
    select a.ds_equipamento || ds_canal_bomba_w || ' - ' desc_bomba
      into ds_desc_w
      from man_equipamento a
     where a.nr_sequencia = nr_seq_equipamento_w;
  
  end if;

  return(ds_desc_w);

end obter_desc_bomba_e_local;

/
