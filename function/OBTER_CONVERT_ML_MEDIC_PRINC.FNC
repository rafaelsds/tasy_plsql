create or replace
function obter_convert_ml_medic_princ( nr_prescricao_p  NUMBER,
          nr_seq_superior_p NUMBER)
          return NUMBER is

qt_dose_ml_w NUMBER(18,6);

begin
/*FUNCTION CRIADA POR JCURBANO, DOCUMENTADA POR FALTA DE DOCUMENTAÇÃO NO DICIONÁRIO DE OBJETOS*/
select max(obter_conversao_ml(cd_material, qt_dose, cd_unidade_medida_dose))
into qt_dose_ml_w
from prescr_material
where nr_prescricao = nr_prescricao_p
and  nr_sequencia = nr_seq_superior_p;

return qt_dose_ml_w;

end obter_convert_ml_medic_princ;
/