create or replace function obter_desc_escala_copd(	qt_score_p	number)

					return varchar is

ds_retorno_w	varchar2(100);



begin



if	(qt_score_p >= 0 and qt_score_p <= 5) then

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(1070797); -- None
  
elsif (qt_score_p >= 6 and qt_score_p <= 9) then

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309772); -- Low

elsif (qt_score_p >= 10 and qt_score_p <= 20) then

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(309092); -- Medium

elsif (qt_score_p >= 21 and qt_score_p <= 30) then

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(1046944); -- High
  
elsif (qt_score_p >= 31 ) then

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(1070363); -- Very high

end if;


return ds_retorno_w;


end obter_desc_escala_copd;
/