create or replace
function Obter_Seq_acao_caso_teste
		(cd_caso_teste_p		number)
 		return number is

nr_seq_acao_caso_teste_w		caso_teste_acao.nr_ordem_execucao%TYPE;		
		
begin

select	nvl(max(nr_ordem_execucao),0)+5 
into	nr_seq_acao_caso_teste_w
from	caso_teste_acao 
where	NR_SEQ_CASO_TESTE = cd_caso_teste_p;

return	nr_seq_acao_caso_teste_w;

end Obter_Seq_acao_caso_teste;
/
