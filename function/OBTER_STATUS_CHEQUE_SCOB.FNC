create or replace
function OBTER_STATUS_CHEQUE_SCOB (nr_seq_cheque_p		number,
				   dt_parametro_p		date) return varchar2 is

/*	Edgar 15/01/2009, OS 123662, function para posi��o cont�bil do cheque sem considerar a cobran�a

	1 - � DEPOSITAR
	2 - DEPOSITADO
	3 - DEVOLVIDO
	4 - REAPRESENTADO
	5 - SEGUNDA DEVOLUCAO
	6 - DEVOLUCAO PACIENTE
	8 - VENDIDO PARA TERCEIROS
	9 - SEGUNDA REAPRESENTA��O
	10 - TERCEIRA DEVOLU�AO

*/

dt_parametro_w			date;
dt_registro_w			date;
dt_deposito_w			date;
dt_devolucao_banco_w		date;
dt_reapresentacao_w		date;
dt_seg_devolucao_w		date;
dt_seq_reapresentacao_w		date;
dt_terc_devolucao_w		date;
dt_devolucao_w			date;
dt_venda_terc_w			date;
ie_status_cheque_w		number(10,0);


begin

dt_parametro_w		:= trunc(dt_parametro_p,'dd');
SELECT		nvl(trunc(dt_registro,'dd'),dt_parametro_w + 1),
		nvl(trunc(dt_deposito,'dd'),dt_parametro_w + 1),
		nvl(trunc(dt_devolucao_banco,'dd'),dt_parametro_w + 1),
		nvl(trunc(dt_reapresentacao,'dd'),dt_parametro_w + 1),
		nvl(trunc(dt_seg_devolucao,'dd'),dt_parametro_w + 1),
		nvl(trunc(dt_devolucao,'dd'),dt_parametro_w + 1),
		nvl(trunc(dt_venda_terc,'dd'),dt_parametro_w + 1),
		nvl(trunc(dt_seg_reapresentacao, 'dd'),dt_parametro_w + 1),
		nvl(trunc(dt_terc_devolucao,'dd'),dt_parametro_w + 1)
INTO		dt_registro_w,
		dt_deposito_w,
		dt_devolucao_banco_w,
		dt_reapresentacao_w,
		dt_seg_devolucao_w,
		dt_devolucao_w,
		dt_venda_terc_w,
		dt_seq_reapresentacao_w,
		dt_terc_devolucao_w
FROM		cheque_cr
WHERE		nr_seq_cheque	= nr_seq_cheque_p;


ie_status_cheque_w		:= 1;

if	(dt_parametro_w	>= dt_registro_w) then	-- � depositar
	ie_status_cheque_w	:= 1;
end if;

if	(dt_parametro_w	>= dt_deposito_w) then	-- depositado
	ie_status_cheque_w	:= 2;
end if;

if	(dt_parametro_w	>= dt_devolucao_banco_w) then	-- devolvido
	ie_status_cheque_w	:= 3;
end if;

if	(dt_parametro_w	>= dt_reapresentacao_w) then	-- reapresentado
	ie_status_cheque_w	:= 4;
end if;

if	(dt_parametro_w	>= dt_seg_devolucao_w) then	-- segunda devolu��o
	ie_status_cheque_w	:= 5;
end if;

if	(dt_parametro_w	>= dt_seq_reapresentacao_w) then	-- segunda reapresenta��o
	ie_status_cheque_w	:= 9;
end if;

if	(dt_parametro_w	>= dt_terc_devolucao_w) then	-- terceira devolu��o
	ie_status_cheque_w	:= 10;
end if;

if	(dt_parametro_w	>= dt_devolucao_w) then		-- devolu��o ao paciente
	ie_status_cheque_w	:= 6;
end if;

if	(dt_parametro_w	>= dt_venda_terc_w) then		-- vendido para terceiros
	ie_status_cheque_w	:= 8;
end if;

return	ie_status_cheque_w;

end OBTER_STATUS_CHEQUE_SCOB;
/