create or replace
function obter_desc_grupo_receita(nr_sequencia_p number)
 		    	return varchar2 is
				
ds_grupo_receita_w	varchar2(255);
				
begin

if (nvl(nr_sequencia_p,0) > 0) then

select substr(ds_grupo_receita,1,255)
into   ds_grupo_receita_w
from   grupo_receita
where nr_sequencia = nr_sequencia_p;
	
end if;

return	ds_grupo_receita_w;

end obter_desc_grupo_receita;
/