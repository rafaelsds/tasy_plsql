create or replace
function obter_cor_pep_perfil_pac (	nr_sequencia_p	number)
					return varchar2 is

ds_retorno_w	varchar2(255);

begin

if	( nr_sequencia_p is not null ) then
	select	max(ds_cor)
	into	ds_retorno_w
	from	pep_perfil_paciente
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end obter_cor_pep_perfil_pac;
/
