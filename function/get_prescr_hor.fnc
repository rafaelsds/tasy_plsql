create or replace function get_prescr_hor(	nr_atendimento_p	  number,
                                            cd_pessoa_fisica_p	varchar2)
                                            return varchar2 is

dt_prescr_mat_hor_w	        varchar2(4000);
dt_prescr_gasoterapia_hor_w	varchar2(4000);
dt_prescr_rec_hor_w	        varchar2(4000);
dt_prescr_proc_hor_w	      varchar2(4000);
dt_prescr_dieta_hor_w	      varchar2(4000);
dt_prescr_mat_hor_ww	        varchar2(4000);
dt_prescr_gasoterapia_hor_ww	varchar2(4000);
dt_prescr_rec_hor_ww	        varchar2(4000);
dt_prescr_proc_hor_ww	      varchar2(4000);
dt_prescr_dieta_hor_ww	      varchar2(4000);

begin
if	(nr_atendimento_p  is not null or cd_pessoa_fisica_p  is not null) then

	select to_char(min(dt_horario), 'dd/mm/yyyy hh24:mi:ss') dt_horario
	into dt_prescr_mat_hor_w
	from prescr_mat_hor
	where nr_prescricao in ( select	nr_prescricao
							from	prescr_medica
							where	nr_atendimento = nr_atendimento_p
							or		cd_pessoa_fisica = cd_pessoa_fisica_p
							and		dt_liberacao is not null
							and		nr_seq_motivo_susp is null
	)
	and dt_horario >= sysdate;

	select to_char(min(dt_horario), 'dd/mm/yyyy hh24:mi:ss') dt_horario
	into dt_prescr_gasoterapia_hor_w
	from prescr_gasoterapia_hor
	where nr_prescricao in ( select	nr_prescricao
							from	prescr_medica
							where	nr_atendimento = nr_atendimento_p
							or		cd_pessoa_fisica = cd_pessoa_fisica_p
							and		dt_liberacao is not null
							and		nr_seq_motivo_susp is null
	)
	and dt_horario >= sysdate;

	select to_char(min(dt_horario), 'dd/mm/yyyy hh24:mi:ss') dt_horario
	into dt_prescr_rec_hor_w
	from prescr_rec_hor
	where nr_prescricao in ( select	nr_prescricao
							from	prescr_medica
							where	nr_atendimento = nr_atendimento_p
							or		cd_pessoa_fisica = cd_pessoa_fisica_p
							and		dt_liberacao is not null
							and		nr_seq_motivo_susp is null
	)
	and dt_horario >= sysdate;

	select to_char(min(dt_horario), 'dd/mm/yyyy hh24:mi:ss') dt_horario
	into dt_prescr_proc_hor_w
	from prescr_proc_hor
	where nr_prescricao in ( select	nr_prescricao
							from	prescr_medica
							where	nr_atendimento = nr_atendimento_p
							or		cd_pessoa_fisica = cd_pessoa_fisica_p
							and		dt_liberacao is not null
							and		nr_seq_motivo_susp is null
	)
	and dt_horario >= sysdate;

	select to_char(min(dt_horario), 'dd/mm/yyyy hh24:mi:ss') dt_horario
	into dt_prescr_dieta_hor_w
	from prescr_dieta_hor
	where nr_prescricao in ( select	nr_prescricao
							from	prescr_medica
							where	nr_atendimento = nr_atendimento_p
							or		cd_pessoa_fisica = cd_pessoa_fisica_p
							and		dt_liberacao is not null
							and		nr_seq_motivo_susp is null
	)
	and dt_horario >= sysdate;

	select max(dt_prescr_mat_hor_w)
	into dt_prescr_mat_hor_ww 
	from dual 
	where to_date(dt_prescr_mat_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_gasoterapia_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss')) 
	and to_date(dt_prescr_mat_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_rec_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
	and to_date(dt_prescr_mat_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_proc_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
	and to_date(dt_prescr_mat_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_dieta_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'));

	if (dt_prescr_mat_hor_ww is not null) then

		return	dt_prescr_mat_hor_ww;

	else
		
		select max(dt_prescr_gasoterapia_hor_w)
		into dt_prescr_gasoterapia_hor_ww 
		from dual 
		where to_date(dt_prescr_gasoterapia_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_mat_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss')) 
		and to_date(dt_prescr_gasoterapia_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_rec_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
		and to_date(dt_prescr_gasoterapia_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_proc_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
		and to_date(dt_prescr_gasoterapia_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_dieta_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'));

		if (dt_prescr_gasoterapia_hor_ww is not null) then
		
			return	dt_prescr_gasoterapia_hor_ww;
		
		else
			
			select max(dt_prescr_rec_hor_w)
			into dt_prescr_rec_hor_ww 
			from dual 
			where to_date(dt_prescr_rec_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_mat_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss')) 
			and to_date(dt_prescr_rec_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_gasoterapia_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
			and to_date(dt_prescr_rec_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_proc_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
			and to_date(dt_prescr_rec_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_dieta_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'));
			
			if (dt_prescr_rec_hor_ww is not null) then
			
				return	dt_prescr_rec_hor_ww;

			else 
				
				select max(dt_prescr_proc_hor_w)
				into dt_prescr_proc_hor_ww 
				from dual 
				where to_date(dt_prescr_proc_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_mat_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss')) 
				and to_date(dt_prescr_proc_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_gasoterapia_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
				and to_date(dt_prescr_proc_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_rec_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
				and to_date(dt_prescr_proc_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_dieta_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'));

				if (dt_prescr_proc_hor_ww is not null) then
				
					return	dt_prescr_proc_hor_ww;
				
				else
					
					select max(dt_prescr_dieta_hor_w)
					into dt_prescr_dieta_hor_ww 
					from dual 
					where to_date(dt_prescr_dieta_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_mat_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss')) 
					and to_date(dt_prescr_dieta_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_gasoterapia_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
					and to_date(dt_prescr_dieta_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_rec_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'))
					and to_date(dt_prescr_dieta_hor_w, 'dd/mm/yyyy hh24:mi:ss') <= nvl(to_date(dt_prescr_proc_hor_w, 'dd/mm/yyyy hh24:mi:ss'), to_date('01/01/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'));
					
					if (dt_prescr_dieta_hor_ww is not null) then
			
						return	dt_prescr_dieta_hor_ww;
			
					else

						return	null;

					end if;
					
				end if;
				
			end if;
		
		end if;
		
	end if;

end if;

end	get_prescr_hor;
/
