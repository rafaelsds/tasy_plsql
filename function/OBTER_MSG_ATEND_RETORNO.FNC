create or replace
function obter_msg_atend_retorno (nr_seq_regra_p	number)
 		    	return varchar2 is

ds_mensagem_w	varchar2(4000);

begin

select	max(ds_mensagem)
into	ds_mensagem_w
from	regra_atend_retorno
where	nr_sequencia = nr_seq_regra_p;

return	ds_mensagem_w;

end obter_msg_atend_retorno;
/