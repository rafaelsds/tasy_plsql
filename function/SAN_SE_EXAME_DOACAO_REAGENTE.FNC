create or replace
function san_se_exame_doacao_reagente(nr_seq_doacao_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);
ie_continua_w	varchar2(1);
begin
ds_retorno_w := 'N';

select	decode(count(*),0,'S','N') 
into	ie_continua_w
from	san_exame_realizado a,
	san_exame_lote b
where	a.nr_seq_exame_lote = b.nr_sequencia
and	b.nr_seq_doacao     = nr_seq_doacao_p
and	((san_obter_se_exame_auxiliar(a.nr_seq_exame_lote,a.nr_seq_exame,wheb_usuario_pck.get_cd_estabelecimento,'S') = 'S'
	and	san_obter_destino_exame(a.nr_seq_exame,3) = 'S')
	or	(san_obter_se_exame_auxiliar(a.nr_seq_exame_lote,a.nr_seq_exame,wheb_usuario_pck.get_cd_estabelecimento,'O') = 'S'
	and	san_obter_destino_exame(a.nr_seq_exame,0) = 'S'))
and	b.nr_amostra = 1	
and	dt_liberacao is null;

if	(ie_continua_w = 'S') then
	select	decode(count(*),0,'N','S') 
	into	ds_retorno_w
	from	san_exame_realizado a,
		san_exame_lote b
	where	a.nr_seq_exame_lote = b.nr_sequencia
	and	b.nr_seq_doacao = nr_seq_doacao_p
	and	b.nr_amostra = 1
	and 	san_obter_se_exame_auxiliar(a.nr_seq_exame_lote,a.nr_seq_exame,wheb_usuario_pck.get_cd_estabelecimento,'S') = 'S'
	and	upper(a.ds_resultado) in ('REAGENTE','POSITIVO','INDETERMINADO');	
end if;

return	ds_retorno_w;

end san_se_exame_doacao_reagente;
/
