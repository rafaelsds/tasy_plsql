CREATE OR REPLACE
function obter_regra_solic_material(	cd_material_p           number,
					cd_estabelecimento_p	number,
				ie_retorno_p            varchar2)
return varchar2 is

/* ie_retorno_p
	C = Aviso chegada
	A = Aviso aprova��o
	U = Urgente
	L = Permite Liberar
	I - Permite incluir  */

cd_perfil_w		number(5);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
qt_regra_w		number(5);
ie_aviso_chegada_w	varchar2(1);
ie_aviso_aprov_oc_w	varchar2(1);
ie_urgente_w		varchar2(1);
ie_permite_liberar_w	varchar2(1);
ie_permite_incluir_w		varchar2(1);
ie_consignado_w			varchar2(1);

cursor c01 is
select	ie_aviso_chegada,
	ie_aviso_aprov_oc,
	ie_urgente,
	ie_permite_liberar,
	ie_permite_incluir
from	regra_solicitacao_material
where	cd_estabelecimento = cd_estabelecimento_p
and	nvl(cd_perfil, cd_perfil_w) = cd_perfil_w
and	nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
and	nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w) = cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w) = cd_classe_material_w
and	((nvl(ie_consignado, ie_consignado_w) = ie_consignado_w) or (ie_consignado is null))
and	(nvl(cd_material, cd_material_p) = cd_material_p or cd_material_p = 0)
order by
	nvl(ie_consignado,0),
	nvl(cd_perfil,0),
	nvl(cd_grupo_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_material, 0);


begin

select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material,
	ie_consignado
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	ie_consignado_w
from	estrutura_material_v
where	cd_material = cd_material_p;

cd_perfil_w := obter_perfil_ativo;

select	count(*)
into	qt_regra_w
from	regra_solicitacao_material;

if	(qt_regra_w > 0) then
	begin
	open c01;
	loop
	fetch c01 into
		ie_aviso_chegada_w,
                ie_aviso_aprov_oc_w,
                ie_urgente_w,
		ie_permite_liberar_w,
		ie_permite_incluir_w;
	exit when c01%notfound;
	end loop;
	close c01;
	end;
end if;

if	(ie_retorno_p = 'C') then
	return ie_aviso_chegada_w;
elsif	(ie_retorno_p = 'A') then
	return  ie_aviso_aprov_oc_w;
elsif	(ie_retorno_p = 'U') then
	return  ie_urgente_w;
elsif	(ie_retorno_p = 'L') then
	return  ie_permite_liberar_w;
elsif	(ie_retorno_p = 'I') then
	return  ie_permite_incluir_w;	
end if;

end     obter_regra_solic_material;
/
