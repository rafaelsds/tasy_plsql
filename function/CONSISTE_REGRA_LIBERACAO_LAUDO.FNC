create or replace
function Consiste_regra_liberacao_laudo(nr_sequencia_p	number)
 		    	return varchar2 is
			
ds_mensagem_w		varchar2(255);
ie_tipo_grupo_w 	varchar2(15);
ie_libera_w		varchar2(1);

cursor c01 is 
 	select	c.ie_tipo_grupo
	from 	regra_lib_laudo_que_item c,
		regra_lib_laudo_questao b,
		laudo_paciente a
	where 	b.nr_seq_proc_interno = obter_nr_seq_proc_interno(a.nr_prescricao,a.nr_seq_prescricao)
	and 	c.nr_seq_regra_lib = b.nr_sequencia
	and	b.ie_situacao = 'A'
	and	a.nr_sequencia = nr_sequencia_p
	and	((nvl(c.ie_exame_complementar,'N') = 'N') or
		( exists (select 1 from laudo_pac_exame_compl where nr_seq_laudo = a.nr_sequencia)));
		
begin

ds_mensagem_w := '';

open C01;
loop
fetch C01 into
	ie_tipo_grupo_w;
exit when C01%notfound;
	begin
	
	if 	(nr_sequencia_p is not null) then 
	
		select 	nvl(max('S'),'N')	   
		into	ie_libera_w
		from   	laudo_paciente x,
			laudo_questao_item i, 
			laudo_grupo_questao g, 
			grupo_questao_laudo o,
			prescr_proc_peca p,    
			tipo_amostra_patologia d
		where 	x.nr_sequencia 		= g.nr_seq_laudo
		and   	i.nr_seq_laudo_grupo   	= g.nr_sequencia
		and   	g.nr_seq_peca 		= p.nr_sequencia
		and   	p.nr_seq_amostra_princ 	= d.nr_sequencia
		and   	g.nr_seq_grupo_questao 	= o.nr_sequencia
		and	ie_texto_grupo		= ie_tipo_grupo_w	
		and   	x.nr_sequencia 		= nr_sequencia_p
		and	i.dt_liberacao is not null;
		
	end if;
	
	if	(ie_libera_w = 'N') then 
		ds_mensagem_w := obter_desc_expressao(779453);
		exit;
	end if;	
	end;
end loop;
close C01;

return	ds_mensagem_w;

end Consiste_regra_liberacao_laudo;
/