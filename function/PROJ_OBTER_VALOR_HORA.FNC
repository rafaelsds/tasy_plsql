create or replace
function proj_obter_valor_hora	(nr_seq_proj_p number,
				ie_recurso_p varchar2	)
return number is
vl_retorno_w		proj_hora_cobrar.vl_hora%type;
begin

select	max(vl_hora)
into	vl_retorno_w
from	proj_hora_cobrar
where	nr_seq_proj = nr_seq_proj_p
and 	sysdate between dt_inicio and dt_fim
and 	ie_recurso = ie_recurso_p
and		cd_executor is null;

return	vl_retorno_w;

end PROJ_OBTER_VALOR_HORA;
/