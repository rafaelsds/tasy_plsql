create or replace
function obter_dados_qua_doc_local(	nr_seq_doc_p	number,
				ie_retorno_p	varchar2)
				return varchar2 is

ds_setor_w		varchar2(80);
ds_localizacao_eletronica_w	varchar2(255);
ds_retorno_w		varchar2(255);

begin
/*
NS = Nome do setor de atendimento
DL = Descrição da localização eletrônica
*/

select	max(substr(obter_nome_setor(cd_setor_atendimento),1,80))
into	ds_setor_w
from	qua_doc_local
where	nr_seq_doc = nr_seq_doc_p
and	ie_forma_armazenamento <> 'E';

select	max(ds_localizacao_eletronica)
into	ds_localizacao_eletronica_w
from	qua_doc_local
where	nr_seq_doc = nr_seq_doc_p
and	ie_forma_armazenamento <> 'F';

if	(ie_retorno_p = 'NS') then
	ds_retorno_w := ds_setor_w;
elsif	(ie_retorno_p = 'DL') then
	ds_retorno_w := ds_localizacao_eletronica_w;
end if;

return	ds_retorno_w;

end obter_dados_qua_doc_local;
/
