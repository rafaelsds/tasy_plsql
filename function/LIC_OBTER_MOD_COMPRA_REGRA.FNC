create or replace
function lic_obter_mod_compra_regra(	cd_conta_contabil_p		varchar2,
				cd_estabelecimento_p		number)
 		    	return number is

nr_seq_mod_compra_w			number(10);
			
begin

select	nvl(max(a.nr_seq_mod_compra),0)
into	nr_seq_mod_compra_w
from	reg_lic_conta_valor a
where	(trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd') and trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd'))
and	a.ie_situacao = 'A'
and	a.cd_conta_contabil	= cd_conta_contabil_p
and	a.cd_estabelecimento	= cd_estabelecimento_p;

return	nr_seq_mod_compra_w;

end lic_obter_mod_compra_regra;
/