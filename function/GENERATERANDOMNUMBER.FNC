create or replace function generateRandomNumber return varchar2 is
   
w		varchar2(1);
a		varchar2(10);
b		varchar2(1);
x		varchar2(10);
y		varchar2(10);
z		varchar2(2);
c		varchar2(6);
locationId 	varchar2(10);  -- Location Id
t		varchar2(10);
result_w 	varchar2(50); 

begin


select	substr('abcdef', dbms_random.value(1,6), 1) ||
	substr('abcdef', dbms_random.value(1,6), 1) 
into a
from dual;

select	Lpad(round(dbms_random.value(1,9)),1,'0')
into	w
from	dual; 

select	substr('abcdef', dbms_random.value(1,6), 1) 
into 	x 
from 	dual; 

select	lpad(round(dbms_random.value(1, 9)),1,'0')
into	b	
from	dual; 

select	substr('abcdef', dbms_random.value(1,6), 1) ||
	substr('abcdef', dbms_random.value(1,6), 1)
into	y
from 	dual; 

select	lpad(round(dbms_random.value(6, 999999)),6,'0')
into 	c 
from	dual;  

select	substr('abcdef', dbms_random.value(1,6), 1) 
into	t
from 	dual;  

select	lpad(round(dbms_random.value(3, 99)),2,'0')
into 	z	 
from	dual; 

select  max(CD_LOCATION)
into 	locationId
from 	ECLIPSE_PARAMETERS;

result_w	:=( locationId || a || to_char(w) || x || to_char(b) || y || to_char(c) || t || to_char(z)  ) ;
return	result_w ;
end generateRandomNumber;
/
