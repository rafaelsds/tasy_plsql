CREATE OR REPLACE function TASY.oncomed_obter_med_pac(nr_soro_p varchar2)
return varchar2 is

ds_retorno_w             varchar2(200);
ds_resultado_w           number(1);
dt_ent_setor_w           number(1);
ie_existe_soro_w         number(1);
nr_sequencia_w           number(10);
begin  
if (nr_soro_p is null)then
    ds_retorno_w := '***Favor informar o c�digo.***';
else
        nr_sequencia_w := substr((nr_soro_p), instr((nr_soro_p), '#')+1);

        select count(*)
        into   ie_existe_soro_w
        from   can_ordem_prod
        where  nr_Sequencia = nr_sequencia_w
        and     nr_prescricao = substr((nr_soro_p), 1, instr((nr_soro_p), '#')-1);

        if(ie_existe_soro_w = 1) then--OK
            
            select      count(*)
            into        ds_resultado_w
            from       med_avaliacao_result r,
                         med_avaliacao_paciente p
            where     r.nr_seq_avaliacao = p.nr_sequencia 
            and         p.nr_seq_tipo_avaliacao = 692 
            and         r.ds_resultado = nr_soro_p 
            and         p.dt_liberacao is not null;     
            
                 if(ds_resultado_w = 0) then --OK    
                          
                    select  count(*)
                    into    dt_ent_setor_w
                    from    can_ordem_prod
                    where   nr_sequencia = nr_sequencia_w 
                    and     dt_entrega_setor is not null;  

                      if(dt_ent_setor_w = 0) then      
                        ds_retorno_w := 'ERRO: Este soro ainda n�o foi dispensado pela Farm�cia.';                
                      else
                        select  nr_sequencia||' - '||Obter_mat_ordem_prod_rotulo(nr_sequencia)||' - '||obter_nome_pf(cd_pessoa_fisica)
                        into    ds_retorno_w
                        from    can_ordem_prod
                        where   nr_sequencia = nr_sequencia_w; 
                      end if;
                else        
                      ds_retorno_w := 'ERRO: Este soro j� foi entregue para a Enfermagem.';
                end if;
        else
                ds_retorno_w := '***ERRO -  O soro n�o foi encontrado. Passe novamente.***';
        end if;
end if;

Return ds_retorno_w;

end oncomed_obter_med_pac;
/