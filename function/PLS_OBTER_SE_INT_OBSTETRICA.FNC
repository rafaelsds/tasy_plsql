create or replace
function pls_obter_se_int_obstetrica(	ie_tipo_guia_p		pls_conta.ie_tipo_guia%type,
					ie_gestacao_p		pls_conta.ie_gestacao%type,
					ie_aborto_p		pls_conta.ie_aborto%type,
					ie_parto_normal_p		pls_conta.ie_parto_normal%type,
					ie_complicacao_puerperio_p	pls_conta.ie_complicacao_puerperio%type,
					ie_complicacao_neonatal_p	pls_conta.ie_complicacao_neonatal%type,
					ie_parto_cesaria_p		pls_conta.ie_parto_cesaria%type,
					ie_baixo_peso_p			pls_conta.ie_baixo_peso%type,
					ie_atend_rn_sala_parto_p	pls_conta.ie_atend_rn_sala_parto%type,
					ie_transtorno_p			pls_conta.ie_transtorno%type)
 		    	return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se a conta se refere a uma interna��o obst�trica
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_retorno_w	varchar2(1)	:= 'N'; 

begin

if	(ie_tipo_guia_p = 5) then
	if	((nvl(ie_gestacao_p,'N') = 'S') or ( nvl(ie_aborto_p,'N') = 'S') or 
		( nvl(ie_parto_normal_p,'N') = 'S') or  (nvl(ie_complicacao_puerperio_p,'N') = 'S') or 
		( nvl(ie_complicacao_neonatal_p,'N') = 'S') or (nvl(ie_parto_cesaria_p,'N') = 'S') or
		(nvl(ie_baixo_peso_p,'N') = 'S') or (nvl(ie_atend_rn_sala_parto_p,'N') = 'S') or 
		(nvl(ie_transtorno_p,'N') = 'S')) then
		ie_retorno_w	:= 'S';		
	end if;
end if;


return	ie_retorno_w;

end ;
/