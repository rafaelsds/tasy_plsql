create or replace
function obter_se_area_proc_lib_perfil(cd_area_procedimento_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1):= 'N';
qt_regra_w	number(10);
			
begin

select	count(*)
into	qt_regra_w
from	regra_area_proc_lib
where	cd_area_procedimento = cd_area_procedimento_p;

if	(qt_regra_w > 0) then

	select	count(*)
	into	qt_regra_w
	from	regra_area_proc_lib
	where	cd_area_procedimento = cd_area_procedimento_p
	and	cd_perfil = obter_perfil_ativo;
	
	if (qt_regra_w > 0) then
	ds_retorno_w:= 'S';
	end if;
	

else
	ds_retorno_w:= 'S';
end if;



return	ds_retorno_w;

end obter_se_area_proc_lib_perfil;
/