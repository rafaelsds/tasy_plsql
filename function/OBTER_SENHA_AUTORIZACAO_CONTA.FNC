create or replace
function obter_senha_autorizacao_conta(nr_interno_conta_p	Number)
 		    	return Varchar2 is
			
cd_autorizacao_w	varchar2(20);
nr_atendimento_w	number(10,0);
cd_senha_w		varchar2(20);
			
begin

if	(nvl(nr_interno_conta_p,0) > 0) then

	select	max(cd_autorizacao),
		max(nr_atendimento)
	into	cd_autorizacao_w,
		nr_atendimento_w
	from	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;
	
	if	(cd_autorizacao_w is not null) then
		
		select	max(cd_senha)
		into	cd_senha_w
		from	autorizacao_convenio
		where	cd_autorizacao = cd_autorizacao_w
		and	nr_atendimento = nr_atendimento_w;
		
	end if;

end if;

return	cd_senha_w;

end obter_senha_autorizacao_conta;
/