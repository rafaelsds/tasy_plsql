create or replace
function obter_se_consist_cobertura(cd_convenio_p	number,
				    cd_plano_p		varchar2)
				return varchar2 is

ds_retorno_w	varchar2(10);	
		
begin
select	nvl(max('S'), 'N')
into	ds_retorno_w
from	regra_consist_cobertura
where	nvl(cd_convenio, nvl(cd_convenio_p, 0)) = nvl(cd_convenio_p, 0)
and	nvl(cd_plano, nvl(cd_plano_p, 'XPTO')) 	= nvl(cd_plano_p, 'XPTO');

return	ds_retorno_w;

end obter_se_consist_cobertura;
/