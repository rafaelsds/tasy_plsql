create or replace
function adep_obter_restricao_valor_dom	(cd_dominio_p		dominio.cd_dominio%type)
 		    	return varchar2 is
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
cd_perfil_w				perfil.cd_perfil%type;
cd_retorno_w			varchar2(2000 char);
vl_dominio_regra_w		restricao_valor_dom.vl_dominio%type;
ie_valor_existe_w		varchar2(1 char);

cursor cRegraValores is
select	a.vl_dominio cd_retorno
from	restricao_valor_dom a
where	((a.cd_estabelecimento = cd_estabelecimento_w) or (a.cd_estabelecimento is null))
and		((a.cd_perfil = cd_perfil_w) or (a.cd_perfil is null))
and		a.cd_dominio = cd_dominio_p
order by
	nvl(a.cd_estabelecimento,99999),
	nvl(a.cd_perfil,99999),
	a.nr_sequencia;
	
cursor cValidaDominio is
select 	x.cd_registro cd_registro	
from    table(lista_pck.obter_lista_char(vl_dominio_regra_w)) x;

begin
cd_perfil_w := obter_perfil_ativo;
cd_estabelecimento_w := obter_estabelecimento_ativo;
for cRegraValores_w in cRegraValores loop
	vl_dominio_regra_w := cRegraValores_w.cd_retorno;
	for cValidaDominio_w in cValidaDominio loop
		select	nvl(max('S'), 'N')
		into	ie_valor_existe_w
		from	valor_dominio a
		where	a.cd_dominio = cd_dominio_p
		and		a.vl_dominio = cValidaDominio_w.cd_registro;
		if (ie_valor_existe_w = 'S') then
			cd_retorno_w := cd_retorno_w || cValidaDominio_w.cd_registro || ',';
		end if;
	end loop;
	exit;
end loop;

return	cd_retorno_w;

end adep_obter_restricao_valor_dom;
/
