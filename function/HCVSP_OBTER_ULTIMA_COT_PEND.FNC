create or replace
function HCVSP_obter_ultima_cot_pend(	cd_material_p		number,
					cd_estabelecimento_p	number)
 		    	return number is

nr_cot_compra_w		number(10);

begin


select	max(a.nr_cot_compra)
into	nr_cot_compra_w
from	cot_compra a,
	cot_compra_item b
where	a.nr_cot_compra = b.nr_cot_compra
and	a.cd_estabelecimento = cd_estabelecimento_p
and	b.cd_material = cd_material_p
and	a.nr_seq_motivo_cancel is null
and	a.dt_geracao_ordem_compra is null
and	a.dt_cot_compra >= sysdate - 365;

return	nr_cot_compra_w;

end HCVSP_obter_ultima_cot_pend;
/