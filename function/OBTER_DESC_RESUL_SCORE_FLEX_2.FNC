create or replace
function obter_desc_resul_score_flex_2( qt_pontos_p		Number,
					nr_seq_escala_p		Number)
 		    	return varchar2 is

ds_retorno_w	varchar2(2000) := null;			
			
begin
if	(qt_pontos_p is not null) then
	begin
	
	select 	ds_resultado
	into	ds_retorno_w	 
	from   	eif_escala_ii_result
	where   qt_pontos_p between qt_pontos_min and qt_pontos_max
	and	nr_seq_escala = nr_seq_escala_p;
	
	exception
			when others then 
				ds_retorno_w	:= null;
	end;
end if;
	
return	ds_retorno_w;

end obter_desc_resul_score_flex_2;
/