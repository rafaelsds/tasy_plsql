CREATE OR REPLACE
FUNCTION LATAM_GET_USER_PARAM (
        nr_sequencia_p LATAM_CONFIGURACAO.NR_SEQUENCIA%type
) RETURN VARCHAR2 IS

nr_sequencia_w      LATAM_CONFIGURACAO.NR_SEQUENCIA%type;
vl_padrao_w         LATAM_CONFIGURACAO.VL_PADRAO%type;
vl_parametro_w      LATAM_CONFIG_PARAM.VL_PARAMETRO%type;

BEGIN

SELECT MAX(a.NR_SEQUENCIA),
       MAX(UPPER(NVL(a.VL_PADRAO, 'S')))
INTO   nr_sequencia_w,
       vl_padrao_w
FROM   LATAM_CONFIGURACAO a
WHERE  a.nr_sequencia = nr_sequencia_p;

IF ( nr_sequencia_w is null ) THEN
    RETURN 'S';
END IF;

SELECT MAX(a.VL_PARAMETRO)
INTO   vl_parametro_w
FROM   latam_config_param a
WHERE  a.nr_seq_configuracao = nr_sequencia_w
AND    a.cd_perfil           = obter_perfil_ativo
AND    a.vl_parametro        IS NOT NULL
AND    UPPER(a.vl_parametro) IN ('S', 'N');

RETURN COALESCE(vl_parametro_w, vl_padrao_w, 'S');

END LATAM_GET_USER_PARAM;
/
