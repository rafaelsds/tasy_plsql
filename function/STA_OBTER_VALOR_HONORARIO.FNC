CREATE OR REPLACE
FUNCTION STA_OBTER_VALOR_HONORARIO(	nr_repasse_terceiro_p	number)
					return number is		

vl_procedimento_w	number(15,2);
vl_participante_w	number(15,2);
vl_material_w		number(15,2);
vl_custo_op_w		number(15,2);
vl_materiais_W		number(15,2);

Begin

select	SUM(w.vl_medico),
	SUM(w.vl_custo_operacional),
	SUM(w.vl_materiais)
into	vl_procedimento_w,
	vl_custo_op_w,
	vl_materiais_w
from	procedimento_paciente w,
	procedimento_repasse u
where	u.nr_seq_procedimento	= w.nr_sequencia
and	u.nr_repasse_terceiro	= nr_repasse_terceiro_p
and	u.nr_seq_partic		is null;

select	SUM(x.vl_participante)
into	vl_participante_w
from	procedimento_participante x,
	procedimento_repasse u
where	u.nr_seq_procedimento	= x.nr_sequencia
and	u.nr_seq_partic		= x.nr_seq_partic
and	u.nr_repasse_terceiro	= nr_repasse_terceiro_p;

select	SUM(y.vl_material)
into	vl_material_w
from	material_atend_paciente y,
	material_repasse x
where	x.nr_seq_material	= y.nr_sequencia
and	x.nr_repasse_terceiro	= nr_repasse_terceiro_p;

return	(nvl(vl_procedimento_w,0) + nvl(vl_custo_op_w,0) + nvl(vl_materiais_w,0) + nvl(vl_participante_w,0) + nvl(vl_material_w,0));

end sta_obter_valor_honorario;
/
