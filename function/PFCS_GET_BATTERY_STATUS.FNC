create or replace function pfcs_get_battery_status
(
  nr_seq_patient_p		varchar2
) return varchar as

ds_battery_status 	varchar(15) := null;
ds_battery 			varchar(15) := null;

low_battery_code 	varchar(15) := 'D0401';
empty_battery_code	varchar(15) := 'D0402';
ds_active_status	varchar2(15) := 'ACTIVE';

begin

select cd_flag into ds_battery_status from (
	select 	cd_flag
	from 	pfcs_patient_flag ppf
	where 	ppf.si_status = ds_active_status
			and (ppf.cd_flag = low_battery_code or ppf.cd_flag = empty_battery_code)
			and ppf.nr_seq_patient = nr_seq_patient_p
			and ppf.period_end is null
			order by ppf.dt_atualizacao desc
            )
    where rownum = 1;
  
  
  if (ds_battery_status = low_battery_code) then
		ds_battery := 'Low';
  elsif (ds_battery_status = empty_battery_code) then
		ds_battery := 'Empty';
  else
		ds_battery := null;
  end if; 
	
return ds_battery;

exception

-- If there will be no record
when no_data_found then
  return ds_battery;
  
end pfcs_get_battery_status;
/
