create or replace
function obter_dados_fila_triagem(
			nr_atendimento_p		number,
			ie_opcao_p			varchar2)
 		    	return varchar2 is

/*
DS - Descri��o fila triagem do pronto atendimento
*/			

ds_retorno_w		varchar2(255) := '';

begin

if	(nr_atendimento_p is not null) then
	
	if	(ie_opcao_p = 'DS') then
	
	Select  max(b.ds_fila)
	into	ds_retorno_w	
	from	triagem_pronto_atend a,
			fila_espera_senha b
	where	a.nr_atendimento = nr_atendimento_p
	and		a.NR_SEQ_PAC_FILA = b.nr_sequencia;
	
	end if;

end if;

return	ds_retorno_w;

end obter_dados_fila_triagem;
/