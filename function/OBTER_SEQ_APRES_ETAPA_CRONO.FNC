create or replace
function Obter_seq_apres_etapa_crono(nr_seq_etapa_p 			number)
 		    	return number is
nr_seq_apresent_w	number(10);
begin
select	nvl(max(NR_SEQ_APRESENT),999)
into	nr_seq_apresent_w
from	PROJ_ETAPA_CRONOGRAMA
where	nr_sequencia	= nr_seq_etapa_p;

return	nr_seq_apresent_w;

end Obter_seq_apres_etapa_crono;
/