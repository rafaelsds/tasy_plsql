create or replace
function Obter_desc_agrup_setor(NR_SEQ_AGRUP_CLASSIF_p		number)
 		    	return varchar is
			
ds_agrupamento_w 	varchar2(255);

begin

select  nvl(max(ds_agrupamento),wheb_mensagem_pck.get_texto(311189))
into	ds_agrupamento_w
from	agrupamento_setor b
where   b.nr_sequencia = NR_SEQ_AGRUP_CLASSIF_p;

return	ds_agrupamento_w;

end Obter_desc_agrup_setor;
/