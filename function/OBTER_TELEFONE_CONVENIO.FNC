create or replace 
function Obter_Telefone_Convenio (cd_convenio_p	number)
				return varchar2 is

ds_retorno_w			varchar2(40);

begin

if	(nvl(cd_convenio_p, 0) <> 0) then
	select	max(nr_telefone_autor)
	into	ds_retorno_w
	from	convenio
	where	cd_convenio		= cd_convenio_p;
end if;

return	ds_retorno_w;

end	Obter_Telefone_Convenio;
/
