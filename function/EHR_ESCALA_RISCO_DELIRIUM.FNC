create or replace
function ehr_escala_risco_delirium (	nr_seq_reg_elemento_p	number,
					nr_seq_entidade_p	number,
					nr_seq_elemento_p	number)
				return varchar2 is

ds_retorno_w			varchar2(20000);
ds_retorno_ww			varchar2(20000);

nr_sequencia_w			number(10);
nm_atributo_w			varchar2(255);
ds_comando_w			varchar2(2000);
ds_comando_ww			varchar2(2000);
ds_label_w			varchar2(60);
ds_quebra_w			varchar2(30);
ds_separador_w			varchar2(30);
c001				Integer;	
qt_cursor_w			number(10);

cursor c01 is
select	nr_sequencia
from	escala_risco_delirium
where	nr_seq_reg_elemento	= nr_seq_reg_elemento_p;

Cursor C02 is
select	nvl(ds_function,nm_atributo),
	ds_label,
	decode(ie_quebra_linha,'S',' chr(13) ||',null),
	ds_separador
from	ehr_template_cont_ret
where	nr_seq_elemento	= nr_seq_elemento_p
order by nr_seq_apres;

begin

open C02;
loop
fetch C02 into	
	nm_atributo_w,
	ds_label_w,
	ds_quebra_w,
	ds_separador_w;
exit when C02%notfound;
	begin
	if	(ds_comando_w is not null) then
		ds_comando_w	:= ds_comando_w ||'||'||chr(13);
	end if;
	ds_comando_w	:= ds_comando_w ||' decode('||nm_atributo_w||',null,null,'||ds_quebra_w ||chr(39) || ds_label_w ||chr(39) ||'|| '|| nm_atributo_w ||'||'|| chr(39) || ds_separador_w ||chr(39) ||')';
	end;
end loop;
close C02;

ds_comando_w	:= 'Select '||ds_comando_w ||' ds '||chr(13) ||'From escala_risco_delirium ';


open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	ds_comando_ww	:= ds_comando_w || chr(13) || 'where nr_sequencia = '||to_char(nr_sequencia_w);
	
	c001 := dbms_sql.open_cursor;
	dbms_sql.parse(c001, ds_comando_ww, dbms_sql.native);
	dbms_sql.define_column(c001, 1, ds_retorno_ww, 2000);
	qt_cursor_w	:= dbms_sql.execute(c001); 
	qt_cursor_w	:= dbms_sql.fetch_rows(c001);
	dbms_sql.column_value(c001, 1, ds_retorno_ww);
	dbms_sql.close_cursor(c001);
	ds_retorno_w	:= ds_retorno_w ||ds_retorno_ww ||chr(13);
	end;
end loop;
close C01;


return substr(ds_retorno_w,1,2000);

end ehr_escala_risco_delirium;
/
