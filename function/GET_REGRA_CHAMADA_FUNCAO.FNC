create or replace function get_regra_chamada_funcao
                               (p_cd_estabelecimento regra_chamada_acesso.cd_estabelecimento%type,
                                p_cd_perfil          regra_chamada_acesso.cd_perfil%type,
                                p_nm_usuario_regra   regra_chamada_acesso.nm_usuario_regra%type,
                                p_cd_funcao_tasy     regra_chamada_params.cd_funcao_tasy%type)
             return number is

Cursor c_verifica_regra is
select count(*) from
 (select '1' tp_acesso_usuario
      from regra_chamada_externa rce,
           regra_chamada_params  rcp,
           regra_chamada_acesso  rca
     where rce.nr_sequencia = rca.nr_seq_regra
       and rce.nr_sequencia = rcp.nr_seq_regra
       and rca.nm_usuario_regra = p_nm_usuario_regra
       and rcp.cd_funcao_tasy   = p_cd_funcao_tasy
   union
    select
           '2' tp_acesso_perfil
      from regra_chamada_externa rce,
           regra_chamada_acesso rca,
           regra_chamada_params  rcp
     where rce.nr_sequencia = rca.nr_seq_regra
       and rce.nr_sequencia = rcp.nr_seq_regra
       and rca.cd_perfil      = p_cd_perfil
       and rcp.cd_funcao_tasy = p_cd_funcao_tasy
   union
    select '3' tp_acesso_estabelecimento
      from regra_chamada_externa rce, regra_chamada_acesso rca, regra_chamada_params  rcp
     where rce.nr_sequencia = rca.nr_seq_regra
       and rce.nr_sequencia = rcp.nr_seq_regra
       and rca.cd_estabelecimento = p_cd_estabelecimento
       and rcp.cd_funcao_tasy     = p_cd_funcao_tasy
       ) qt_registro;

   w_conta_funcao  number (5) := 0;
   
Begin
  open  c_verifica_regra;
  fetch c_verifica_regra into w_conta_funcao;
  close c_verifica_regra;
  --
  if w_conta_funcao > 0 then
     return 1;
  else
     return 0;
  end if;
end get_regra_chamada_funcao;
/
