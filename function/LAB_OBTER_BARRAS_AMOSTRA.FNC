create or replace
function lab_obter_barras_amostra(		nr_prescricao_p  		number,
					nr_seq_prescr_p  		number,
					nr_seq_prescr_proc_mat_p	number,
					ie_amostra_ant_p 		varchar2)
 		    			return varchar is
nr_seq_material_w			number(10);
nr_seq_grupo_w			number(10);
nr_seq_origem_w			number(10);
dt_liberacao_w			date;
dt_prescricao_w			date;
dt_liberacao_medico_w		date;
nr_seq_lab_w			varchar2(20);
nr_seq_grupo_imp_w		number(10);

nr_seq_prescr_proc_mat_w		number(10);
cd_barras_w			varchar2(255);
nr_seq_frasco_w			number(10);
ie_grupo_imp_amostra_seq_w	varchar2(1);
cd_estabelecimento_w		number(4);
ie_agrupa_amostra_horario_w	lab_parametro.ie_agrupa_amostra_horario%type;
ie_urgencia_w				prescr_procedimento.ie_urgencia%type;
dt_prev_execucao_w			prescr_procedimento.dt_prev_execucao%type;
dt_prior_codigo_barras_w varchar2(2);

begin
select	c.nr_seq_material,
	b.nr_seq_grupo,
	b.nr_seq_grupo_imp,
	nvl(a.nr_seq_origem,0),
	k.dt_liberacao,
	k.dt_prescricao,
	k.dt_liberacao_medico,
	a.nr_seq_lab,
	a.nr_seq_frasco,
	nvl(k.cd_estabelecimento,1),
	nvl(a.ie_urgencia,'N'),
	a.dt_prev_execucao
into	nr_seq_material_w,
	nr_seq_grupo_w,
	nr_seq_grupo_imp_w,
	nr_seq_origem_w,
	dt_liberacao_w,
	dt_prescricao_w,
	dt_liberacao_medico_w,
	nr_seq_lab_w,
	nr_seq_frasco_w,
	cd_estabelecimento_w,
	ie_urgencia_w,
	dt_prev_execucao_w
from	exame_lab_material c,
	material_exame_lab d,
	exame_laboratorio b,
	prescr_procedimento a,
	prescr_medica k
where	k.nr_prescricao		= a.nr_prescricao
and	a.nr_prescricao 		= a.nr_prescricao
and	a.nr_seq_exame 		= b.nr_seq_exame
and	a.nr_seq_exame	 	is not null
and	d.nr_sequencia 		= Obter_Mat_Exame_Lab_prescr(a.nr_prescricao, a.nr_sequencia, 1) 
and	c.nr_seq_material 		= d.nr_sequencia
and	c.nr_seq_exame 		= a.nr_seq_exame
and	a.nr_prescricao		= nr_prescricao_p
and	a.nr_sequencia		= nr_Seq_prescr_p
and	(d.ie_volume_tempo 	= 'S' or nvl(c.qt_coleta,1) > 0);

select	nvl(max(ie_gerar_padrao_grupo_imp),'N'),
		nvl(max(ie_agrupa_amostra_horario),'N')
into 	ie_grupo_imp_amostra_seq_w,
		ie_agrupa_amostra_horario_w
from 	lab_parametro
where 	cd_estabelecimento = cd_estabelecimento_w;

if (ie_agrupa_amostra_horario_w = 'U') then
			
	select 	nvl(max(a.NR_SEQUENCIA),0)
	into	nr_seq_prescr_proc_mat_w
	from 	prescr_proc_material a,
			prescr_proc_mat_item b,
			prescr_procedimento c
	where	a.nr_sequencia = b.nr_seq_prescr_proc_mat 
	AND 	c.nr_sequencia = b.nr_seq_prescr  
	and		c.nr_prescricao = b.nr_prescricao
	and 	a.nr_prescricao = nr_prescricao_p
	and 	a.nr_seq_material = nr_seq_material_w
   and   ((nr_seq_prescr_proc_mat_p is not null) or (b.nr_seq_prescr = nr_seq_prescr_p))
	and		nvl(c.ie_urgencia,'N') = ie_urgencia_w
	and		a.nr_sequencia    = nvl(nr_seq_prescr_proc_mat_p,a.nr_sequencia)
	and		((NVL(a.nr_seq_frasco,0)	= NVL(nr_seq_frasco_w,0)) or (ie_amostra_ant_p not in ('AM11F','AM10F')))
	and 	(((a.nr_seq_grupo   = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or 
			((a.nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S')));
	
elsif (ie_agrupa_amostra_horario_w = 'S') then

	select 	nvl(max(a.NR_SEQUENCIA),0)
	into	nr_seq_prescr_proc_mat_w
	from 	prescr_proc_material a,
			prescr_proc_mat_item b,
			prescr_procedimento c
	where	a.nr_sequencia = b.nr_seq_prescr_proc_mat 
	AND 	c.nr_sequencia = b.nr_seq_prescr  
	and		c.nr_prescricao = b.nr_prescricao
	and 	a.nr_prescricao = nr_prescricao_p
	and 	a.nr_seq_material = nr_seq_material_w
	AND 	c.dt_prev_execucao = dt_prev_execucao_w
	and		a.nr_sequencia    = nvl(nr_seq_prescr_proc_mat_p,a.nr_sequencia)
   and   ((nr_seq_prescr_proc_mat_p is not null) or (b.nr_seq_prescr = nr_seq_prescr_p))
	and		((NVL(a.nr_seq_frasco,0)	= NVL(nr_seq_frasco_w,0)) or (ie_amostra_ant_p not in ('AM11F','AM10F')))
	and 	(((a.nr_seq_grupo   = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or 
			((a.nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S')));
	
else

	select 	nvl(max(NR_SEQUENCIA),0)
	into	nr_seq_prescr_proc_mat_w
	from 	prescr_proc_material a
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_seq_material = nr_seq_material_w
	and	nr_sequencia    = nvl(nr_seq_prescr_proc_mat_p,nr_sequencia)
	and	((NVL(nr_seq_frasco,0)	= NVL(nr_seq_frasco_w,0)) or (ie_amostra_ant_p not in ('AM11F','AM10F')))
	and 	(((nr_seq_grupo   = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or 
		((nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S')));

end if;

if	(nr_seq_prescr_proc_mat_w > 0) then
  dt_prior_codigo_barras_w := lab_obter_valor_parametro(722,373);

	select 	cd_barras
	into	cd_barras_w
	from	prescr_proc_material
	where	nr_sequencia = nr_seq_prescr_proc_mat_w;

	if	(length(cd_barras_w) > 10) then
		if	(ie_amostra_ant_p = 'DGS6') then
			cd_barras_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,6,0);
		end if;
		if	(ie_amostra_ant_p = 'DGS5') then
			cd_barras_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,5,0);
		end if;
		if	(ie_amostra_ant_p = 'DGIS') then
			cd_barras_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_grupo_imp_w,2,0) || lpad(nr_seq_lab_w,3,0);
		end if;
		if	(ie_amostra_ant_p = 'DS4GI') then
			cd_barras_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmm') || lpad(nr_seq_lab_w,4,0) || lpad(nr_seq_grupo_imp_w,2,0);
		end if;
		if	(ie_amostra_ant_p = 'DAGS4') then  
      if (dt_prior_codigo_barras_w = 'LE') then
        cd_barras_w := to_char(nvl(dt_liberacao_w,nvl(dt_liberacao_medico_w,dt_prescricao_w)),'ddmmyy') || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,4,0);      
      else
        cd_barras_w := to_char(nvl(dt_liberacao_medico_w,nvl(dt_liberacao_w,dt_prescricao_w)),'ddmmyy') || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,4,0);
      end if;  
		end if;
	elsif	(ie_amostra_ant_p = 'AMO10') then
		cd_barras_w := lpad(cd_barras_w,10,0);		
	elsif	(ie_amostra_ant_p = 'AM10F') then
		cd_barras_w := lpad(cd_barras_w,10,0);	
	elsif	(ie_amostra_ant_p = 'EAM10') then
		cd_barras_w := lpad(substr(cd_estabelecimento_w,1,2),2,0)||lpad(cd_barras_w,10,0);		
	elsif	(ie_amostra_ant_p = 'AMO11') then
		cd_barras_w := lpad(cd_barras_w,11,0);
	elsif	(ie_amostra_ant_p = 'AM11F') then
		cd_barras_w := lpad(cd_barras_w,11,0);
	elsif	(ie_amostra_ant_p = 'AMO13') then
		cd_barras_w := lpad(cd_barras_w,13,0);		
	elsif	(ie_amostra_ant_p <> 'WEB') then		
		cd_barras_w := lpad(cd_barras_w,9,0);
	end if;
end if;

return cd_barras_w;

end lab_obter_barras_amostra;
/