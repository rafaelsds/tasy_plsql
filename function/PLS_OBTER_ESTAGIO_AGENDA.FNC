create or replace
function pls_obter_estagio_agenda
			(	dt_analise_p		pls_analise_adesao.dt_analise%type,
				cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type)
				return varchar2 is

/*
Retorno:
EM	Entrevista marcada
FE	Faltou na entrevista
EP	Marcacao da entrevista pendente
ER	Entrevista realizada
*/

ie_status_agenda_w		agenda_consulta.ie_status_agenda%type;
nr_seq_agenda_consulta_w	agenda_consulta.nr_sequencia%type;
ds_retorno_w			varchar2(2);

begin

select	max(nr_sequencia)
into	nr_seq_agenda_consulta_w
from	agenda_consulta
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	dt_agenda >= trunc(dt_analise_p,'dd')
and	ie_status_agenda <> 'C';

if	(nr_seq_agenda_consulta_w is null) then
	ds_retorno_w	:= 'EP';	-- Marcacao da entrevista pendente
else
	select	nvl(ie_status_agenda, 'X')
	into	ie_status_agenda_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_consulta_w;

	if	(ie_status_agenda_w = 'E') then
		ds_retorno_w	:= 'ER';	-- Entrevista realizada
	else
		select	max(nr_sequencia)
		into	nr_seq_agenda_consulta_w
		from	agenda_consulta
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_agenda >= sysdate
		and	ie_status_agenda <> 'C';
		
		if	(nr_seq_agenda_consulta_w is not null) then
			ds_retorno_w	:= 'EM';	-- Entrevista marcada
		else
			ds_retorno_w	:= 'FE';	-- Faltou na entrevista
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_estagio_agenda;
/