create or replace
function obter_resultado_obs_triagem(
	nr_atendimento_p	number)
 		    	return varchar2 is

ds_observacao_w	varchar2(255);

begin

select	substr(ds_observacao,1,255)
into	ds_observacao_w
from	triagem_pronto_atend
where	nr_sequencia = (select max(nr_sequencia) from triagem_pronto_atend where nr_atendimento = nr_atendimento_p);

return	ds_observacao_w;

end obter_resultado_obs_triagem;
/