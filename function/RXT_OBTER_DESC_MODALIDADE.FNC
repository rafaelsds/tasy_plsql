create or replace
function rxt_obter_desc_modalidade(nr_seq_modalidade_p		number)
 		    	return varchar2 is
ds_retorno_w	varchar2(100);
begin
if	(nr_seq_modalidade_p is not null) then

	select	max(ds_modalidade)
	into	ds_retorno_w
	from	rxt_tipo_modalidade
	where	nr_sequencia = nr_seq_modalidade_p;

end if;	


return	ds_retorno_w;

end rxt_obter_desc_modalidade;
/