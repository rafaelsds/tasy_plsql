create or replace
function get_custom_algorithm_name(
			nr_sequencia_p	number)
		return varchar2 is

ds_name_w varchar2(255) := '';

begin
if (nr_sequencia_p is not null) then
	begin
	select	substr(max(ds_name),1,255)
	into	ds_name_w
	from	custom_algorithm
	where	nr_sequencia = nr_sequencia_p;
	end;
end if;
return ds_name_w;
end get_custom_algorithm_name;
/