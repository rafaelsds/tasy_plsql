create or replace
function fis_obter_coaseguro_deducible	(nr_interno_conta_p	number,
					ie_tipo_p	varchar2,
					nr_sequencia_nf_p nota_fiscal.nr_sequencia%type default null)
					return number is

vl_retorno_w		number(15,4) := null;
vl_coaseguro1_w		number(15,4);
vl_coaseguro2_w		number(15,4);
vl_coaseg_nivel_hosp_w	number(15,4);
vl_deducible_w		number(15,4);
qt_processamento_w	number(10,0);
qt_nf_credito_w		number(10,0) := 0;

begin

/*Caso a nota fiscal for de credito nao deve levar valor OS 2156739*/
if (nvl(nr_sequencia_nf_p,0) > 0) then

	SELECT 	nvl(count(a.nr_seq_nf_gerada),0)
	INTO	qt_nf_credito_w
	FROM	NF_CREDITO a,
			NOTA_FISCAL b
	WHERE	a.nr_seq_nf_gerada = b.nr_sequencia
	AND		b.nr_sequencia = nr_sequencia_nf_p
	AND		b.nr_sequencia_ref is not null;
	
end if;

if ((nr_interno_conta_p is not null) and (ie_tipo_p is not null) and (nvl(qt_nf_credito_w,0) = 0)) then

	/*Verificar se a conta ainda esta processada da nova forma*/
	select	count(*)
	into	qt_processamento_w
	from 	conta_pac_deducao_conv
	where 	nr_seq_conta_orig = nr_interno_conta_p
	and 	dt_processamento is not null;	
	
	if	(nvl(qt_processamento_w,0) > 0) then
	
		if	(ie_tipo_p = 'C') then
			select	nvl(sum(nvl(a.vl_rateio,0)),0) vl
			into	vl_retorno_w
			from	conta_pac_ded_conv_item a,
				conta_pac_deducao_conv b
			where	a.nr_seq_deducao_conv = b.nr_sequencia
			and	b.nr_seq_conta_orig = nr_interno_conta_p
			and	b.ie_tipo_calculo <> 'D';
		elsif	(ie_tipo_p = 'D') then
			select	nvl(sum(nvl(a.vl_rateio,0)),0) vl
			into	vl_retorno_w
			from	conta_pac_ded_conv_item a,
				conta_pac_deducao_conv b
			where	a.nr_seq_deducao_conv = b.nr_sequencia
			and	b.nr_seq_conta_orig = nr_interno_conta_p
			and	b.ie_tipo_calculo = 'D';
		end if;
	
	else

		if ie_tipo_p = 'C' then
			select  nvl(max(a.vl_procedimento),0)
			into	vl_coaseguro1_w
			from 	procedimento_paciente a,
				conta_paciente b
			where 	a.nr_interno_conta = b.nr_interno_conta
			and 	a.nr_interno_conta = nr_interno_conta_p
			and exists 	(select	1
					from  	conv_regra_calculo_conta x
					where   x.cd_proc_calculo = a.cd_procedimento
					and 	x.ie_situacao = 'A'
					and 	x.ie_tipo_calculo = 'N' --Coaseguro
					and 	x.cd_convenio = b.cd_convenio_parametro
					and 	b.cd_estabelecimento = x.cd_estabelecimento );
					
			select  nvl(max(a.vl_procedimento),0)
			into	vl_coaseguro2_w
			from 	procedimento_paciente a,
				conta_paciente b
			where 	a.nr_interno_conta = b.nr_interno_conta
			and 	a.nr_interno_conta = nr_interno_conta_p
			and exists 	(select	1
					from  	conv_regra_calculo_conta x
					where   x.cd_proc_calculo = a.cd_procedimento
					and 	x.ie_situacao = 'A'
					and 	x.ie_tipo_calculo = 'H' --Coaseguro
					and 	x.cd_convenio = b.cd_convenio_parametro
					and 	b.cd_estabelecimento = x.cd_estabelecimento );
					
			select  nvl(max(a.vl_procedimento),0)
			into	vl_coaseg_nivel_hosp_w
			from 	procedimento_paciente a,
				conta_paciente b
			where 	a.nr_interno_conta = b.nr_interno_conta
			and 	a.nr_interno_conta = nr_interno_conta_p
			and exists 	(select	1
					from  	conv_regra_calculo_conta x
					where   x.cd_proc_calculo = a.cd_procedimento
					and 	x.ie_situacao = 'A'
					and 	x.ie_tipo_calculo = 'A' --Coaseguro Nivel Hosp
					and 	x.cd_convenio = b.cd_convenio_parametro
					and 	b.cd_estabelecimento = x.cd_estabelecimento );
					
			vl_retorno_w := (vl_coaseguro1_w + vl_coaseguro2_w + vl_coaseg_nivel_hosp_w) * -1;
		end if;
		
		if ie_tipo_p = 'D' then	  
			select  nvl(max(a.vl_procedimento),0)
			into	vl_deducible_w
			from 	procedimento_paciente a,
				conta_paciente b
			where 	a.nr_interno_conta = b.nr_interno_conta
			and 	a.nr_interno_conta = nr_interno_conta_p
			and exists 	(select	1
					from  	conv_regra_calculo_conta x
					where   x.cd_proc_calculo = a.cd_procedimento
					and 	x.ie_situacao = 'A'
					and 	x.ie_tipo_calculo = 'D' --Deducible
					and 	x.cd_convenio = b.cd_convenio_parametro
					and 	b.cd_estabelecimento = x.cd_estabelecimento );
					
			vl_retorno_w := vl_deducible_w * -1;
		end if;
		
	end if;
	
end if;	

return	vl_retorno_w;

end fis_obter_coaseguro_deducible;
/