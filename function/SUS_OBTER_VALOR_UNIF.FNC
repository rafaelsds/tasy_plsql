create or replace 
function Sus_Obter_Valor_Unif (	nr_interno_conta_p	number,
					ie_valor_p		number) 
				return number is
				
vl_retorno_w		procedimento_paciente.vl_procedimento%type := 0;
ie_arred_sp_sisaih_w	sus_parametros_aih.ie_arred_sp_sisaih%type := 'N';

begin

ie_arred_sp_sisaih_w := nvl(sus_parametros_aih_pck.get_ie_arred_sp_sisaih,'X');

if	(ie_arred_sp_sisaih_w = 'X') then
	begin
	
	begin
	select	nvl(b.ie_arred_sp_sisaih,'N')
	into	ie_arred_sp_sisaih_w
	from	conta_paciente a,
		sus_parametros_aih b
	where	a.nr_interno_conta	= nr_interno_conta_p
	and	b.cd_estabelecimento	= a.cd_estabelecimento
	and	rownum = 1;
	exception
	when others then
		ie_arred_sp_sisaih_w := 'N';
	end;
	
	sus_parametros_aih_pck.set_ie_arred_sp_sisaih(ie_arred_sp_sisaih_w);
	
	end;
end if;

if	(ie_arred_sp_sisaih_w		= 'N') then
	begin
	
	select	sum((nvl(b.vl_sadt,0) + nvl(b.vl_ato_anestesista,0) + nvl(b.vl_medico,0) + nvl(b.vl_matmed,0))) vl_procedimento 
	into	vl_retorno_w
	from	sus_valor_proc_paciente b,
		procedimento_paciente a
	where	a.nr_sequencia		= b.nr_sequencia
	and	a.cd_motivo_exc_conta	is null
	and	a.nr_interno_conta	= nr_interno_conta_p
	and	a.ie_origem_proced	= 7;

	end;
else
	begin
	
	select	sum((nvl(b.vl_sadt,0) + nvl(a.vl_medico,0) + nvl(b.vl_matmed,0) + nvl((select nvl(sum(nvl(p.vl_participante,0)),0) from procedimento_participante p where p.nr_sequencia = a.nr_sequencia),0))) vl_procedimento
	into	vl_retorno_w
	from	sus_valor_proc_paciente b,
		procedimento_paciente a
	where	a.nr_sequencia		= b.nr_sequencia
	and	a.cd_motivo_exc_conta	is null
	and	a.nr_interno_conta	= nr_interno_conta_p
	and	a.ie_origem_proced	= 7;
	
	end;
end if;

return nvl(vl_retorno_w,0);

end sus_obter_valor_unif;
/
