create or replace
function proj_Obter_informacao_os
		(dt_parametro_p				date,
		ie_tipo_inf_p			varchar2,
		ie_tipo_valor_p			varchar2)
 		return number is

/*
ie_tipo_inf_p
QTI - Quantidade total de Insatisfa��o no m�s
PRI - Percentual de insatisfa��o
ENS - Quantidade total de OS encerrada com satisfa��o
*/

qt_os_encerrada_classif_w	number(15,4);
qt_os_insatisf_w		number(15,4);
dt_ref_mes_w	date;
dt_fim_mes_w	date;
	
begin

dt_ref_mes_w			:= PKG_DATE_UTILS.start_of(dt_parametro_p, 'MONTH', 0);
dt_fim_mes_w			:= PKG_DATE_UTILS.end_of(dt_ref_mes_w, 'MONTH', 0);

if	(ie_tipo_valor_p	= 'A') then
	dt_ref_mes_w		:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_parametro_p, 'MONTH', 0), -11, 0);
	dt_fim_mes_w		:= PKG_DATE_UTILS.start_of(dt_parametro_p, 'MONTH', 0);
end if;

/*Retorna a quantidade de OS encerrada com Grau de Satisfa��o*/
if	( ie_tipo_inf_p in ('PRI','ENS')) then
	select   count(*)
	into	 qt_os_encerrada_classif_w
	from    man_ordem_servico a
	where   a.nr_grupo_trabalho = 82
	and	a.ie_status_ordem = 3
	and	a.ie_grau_satisfacao in ('E','O','B','P','R')
	and	a.dt_fim_real between dt_ref_mes_w and dt_fim_mes_w;

end if;
/*Retorna a quantidade de OS encerrada com Grau de Satisfa��o Irregular*/
if	(ie_tipo_inf_p in ('QTI','PRI')) then

	select	count(*)
	into	qt_os_insatisf_w
	from	man_ordem_servico a
	where	a.ie_status_ordem = 3
	and	a.nr_grupo_trabalho = 82
	and	a.ie_grau_satisfacao in ('P','R')
	and	a.dt_fim_real between dt_ref_mes_w and dt_fim_mes_w;
end if;

if	(ie_tipo_inf_p = 'QTI') then
	return	qt_os_insatisf_w;
elsif	(ie_tipo_inf_p = 'PRI') then
	return dividir(qt_os_insatisf_w, qt_os_encerrada_classif_w)*100;
elsif	(ie_tipo_inf_p = 'ENS') then
	return	qt_os_encerrada_classif_w;
end if;

end proj_Obter_informacao_os;
/
