create or replace 
FUNCTION CTB_Obter_Conta_Apres_ANS(
				cd_plano_p		Varchar2,
				nr_seq_plano_ans_p	number default 0)
				return 		varchar2 is

cd_classificacao_w		Varchar2(80);
ds_conta_w			Varchar2(180);
ds_conta_ww			Varchar2(254);

begin

select		max(cd_classificacao),
		max(ds_conta)
into		cd_classificacao_w,
		ds_conta_w
from		ctb_plano_ans
where		nvl(cd_plano,0)		= nvl(cd_plano_p,cd_plano)
and		((nr_sequencia 		= nr_seq_plano_ans_p) or (nr_seq_plano_ans_p = 0));

ds_conta_ww	:= SUBSTR(lpad(' ', length(cd_classificacao_w)-1) || ds_conta_w,1,100);

return ds_conta_ww;

end CTB_Obter_Conta_Apres_ANS;
/