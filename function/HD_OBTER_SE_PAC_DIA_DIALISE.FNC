create or replace
function hd_obter_se_pac_dia_dialise(	cd_pessoa_fisica_p	varchar2,
					ie_dia_semana_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is
nr_seq_escala_w		number(10,0);
ds_retorno_w		varchar2(1) := 'N';

begin

select 	hd_obter_escala_prc(cd_pessoa_fisica_p, 'C')
into	nr_seq_escala_w
from 	dual;

if	(nvl(nr_seq_escala_w,0) > 0) then

	select 	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	hd_escala_dia_semana
	where	nr_seq_escala = nr_seq_escala_w
	and	ie_dia_semana = ie_dia_semana_p;

end if;
return	ds_retorno_w;
end hd_obter_se_pac_dia_dialise;
/