create or replace
function LIC_OBTER_DESC_FORNEC (nr_seq_fornec_p	number) return varchar2 is

ds_nome_abrev_w		pessoa_juridica.ds_nome_abrev%type;
nm_fantasia_w		pessoa_juridica.nm_fantasia%type;
ds_razao_social_w	pessoa_juridica.ds_razao_social%type;
ds_retorno_w		varchar2(254);


begin
Select	ds_nome_abrev,
	nm_fantasia,
	ds_razao_social
into	ds_nome_abrev_w,
	nm_fantasia_w,
	ds_razao_social_w
from	pessoa_juridica
where	cd_cgc = (select	cd_cgc_fornec
		  from		lic_fornec
		  where		nr_sequencia = nr_seq_fornec_p);
if (ds_nome_abrev_w is not null) then
	ds_retorno_w := ds_nome_abrev_w;
elsif (nm_fantasia_w is not null) then
	ds_retorno_w := nm_fantasia_w;
else
	ds_retorno_w := ds_razao_social_w;
end if;
return ds_retorno_w;
end;
/
