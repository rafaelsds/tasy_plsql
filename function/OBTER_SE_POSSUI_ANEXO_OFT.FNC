create or replace
function obter_se_possui_anexo_oft    (cd_pessoa_fisica_p		number)
				       return varchar2 is
 
ds_item_w   		varchar2(255);
nr_seq_angio_retino_w   number(10);
nr_seq_biometria_w	number(10);
nr_seq_campimetria_w 	number(10);
nr_seq_cerastocopia_w 	number(10); 
nr_seq_microscopia_w 	number(10);
nr_seq_oct_w 		number(10);
nr_seq_tomografia_w	number(10); 
nr_seq_topografia_w	number(10);
nr_seq_conduta_w	number(10);	
nr_seq_consulta_w	number(10);
nr_seq_fundoscopia_w number(10);
nr_seq_biomicroscopia_w number(10);
nr_seq_oft_exame_externo_w number(10);
nr_seq_paquimetria_w number(10);
nr_seq_ultrassonografia_w  number(10);
nr_seq_mapeamento_w  number(10);
nr_seq_aberrometria_w number(10);
ie_ceratoscopia_w	varchar2(1);	
ie_tipo_registro_w   varchar2(15);	
ie_Altera_pasta_cerat_comp_w	varchar2(1);		   
	  
begin

Obter_Param_Usuario(3010,125,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_altera_pasta_cerat_comp_w);

select 	max(nr_seq_angio_retino), 
	max(nr_seq_biometria), 
	max(nr_seq_campimetria), 
	max(nr_seq_cerastocopia),  
	max(nr_seq_microscopia), 
	max(nr_seq_oct), 
	max(nr_seq_tomografia), 
	max(nr_seq_topografia),
	max(ie_ceratoscopia),
   max(nr_seq_fundoscopia), 
   max(nr_seq_biomicroscopia), 
   max(nr_seq_oft_exame_externo), 
   max(nr_seq_paquimetria), 
   max(nr_seq_ultrassonografia), 
   max(nr_seq_mapeamento), 
   max(nr_seq_aberrometria)
into	nr_seq_angio_retino_w, 
	nr_seq_biometria_w, 
	nr_seq_campimetria_w, 
	nr_seq_cerastocopia_w,  
	nr_seq_microscopia_w, 
	nr_seq_oct_w, 
	nr_seq_tomografia_w, 
	nr_seq_topografia_w,
	ie_ceratoscopia_w,
   nr_seq_fundoscopia_w, 
   nr_seq_biomicroscopia_w, 
   nr_seq_oft_exame_externo_w, 
   nr_seq_paquimetria_w, 
   nr_seq_ultrassonografia_w, 
   nr_seq_mapeamento_w, 
   nr_seq_aberrometria_w
from    oft_imagem_exame
where   nr_seq_consulta  in (select c.nr_sequencia 
                                       from     atendimento_paciente b, 
                                                oft_consulta c 
                                        where   c.nr_atendimento = b.nr_atendimento 
                                        and     b.cd_pessoa_fisica = cd_pessoa_fisica_p 
													 and		c.dt_cancelamento is null
                                        union                                         
                                        select  a.nr_sequencia       
                                        from    oft_consulta a       
                                        where   a.cd_pessoa_fisica = cd_pessoa_fisica_p
													 and		a.dt_cancelamento is null);
					
select 	max(nr_seq_conduta)
into	nr_seq_conduta_w
from    oft_anexo
where   nr_seq_consulta  in (select c.nr_sequencia 
                                       from     atendimento_paciente b, 
                                                oft_consulta c 
                                        where   c.nr_atendimento = b.nr_atendimento 
                                        and     b.cd_pessoa_fisica = cd_pessoa_fisica_p
													 and		c.dt_cancelamento is null	
                                        union                                         
                                        select  a.nr_sequencia       
                                        from    oft_consulta a       
                                        where   a.cd_pessoa_fisica = cd_pessoa_fisica_p
													 and		a.dt_cancelamento is null);
					
select 	max(nr_seq_consulta)
into	nr_seq_consulta_w
from    oft_anexo
where   nr_seq_consulta  in (select c.nr_sequencia 
                                       from     atendimento_paciente b, 
                                                oft_consulta c 
                                        where   c.nr_atendimento = b.nr_atendimento 
                                        and     b.cd_pessoa_fisica = cd_pessoa_fisica_p 
													 and		c.dt_cancelamento is null
                                        union                                         
                                        select  a.nr_sequencia       
                                        from    oft_consulta a       
                                        where   a.cd_pessoa_fisica = cd_pessoa_fisica_p
													 and		a.dt_cancelamento is null);
				

if (nr_seq_angio_retino_w is not null) then
   select   nvl(max(ie_tipo_registro),'A')
   into     ie_tipo_registro_w
   from     oft_angio_retino
   where    nr_sequencia = nr_seq_angio_retino_w;
   if (ie_tipo_registro_w = 'A') then
      ds_item_w := '71' || ',';
   else   
      ds_item_w := '298' || ',';
   end if;   
end if;

if (nr_seq_biometria_w is not null) then
	ds_item_w := ds_item_w || '78' || ',';	
end if;

if (nr_seq_campimetria_w is not null) then
	ds_item_w := ds_item_w || '171' || ',';
end if;

if (nr_seq_cerastocopia_w is not null) then
	ds_item_w := ds_item_w || '175' || ',';
end if;

if (nr_seq_microscopia_w is not null) then
	ds_item_w := ds_item_w || '80' || ',';
end if;

if (nr_seq_oct_w is not null) then
	ds_item_w := ds_item_w || '177' || ',';
end if;

if (nr_seq_tomografia_w is not null) then
	ds_item_w := ds_item_w || '173' || ',';
end if;

if ((ie_Altera_pasta_cerat_comp_w = 'N') and
   (nr_seq_topografia_w is not null)) then
	ds_item_w := ds_item_w || '168' || ',';
end if;

if ((ie_Altera_pasta_cerat_comp_w = 'S') and
    (ie_ceratoscopia_w = 'S')) then
    ds_item_w := ds_item_w || '168' || ',';
end if;

if (nr_seq_conduta_w is not null) then
	ds_item_w := ds_item_w || '24' || ',';
end if;

if (nr_seq_consulta_w is not null) then
	ds_item_w := ds_item_w || '31' || ',';
end if;

if (nr_seq_fundoscopia_w is not null) then
	ds_item_w := ds_item_w || '9' || ',';
end if;

if (nr_seq_biomicroscopia_w is not null) then
	ds_item_w := ds_item_w || '10' || ',';
end if;

if (nr_seq_oft_exame_externo_w is not null) then
	ds_item_w := ds_item_w || '6' || ',';
end if;

if (nr_seq_paquimetria_w is not null) then
	ds_item_w := ds_item_w || '84' || ',';
end if;

if (nr_seq_ultrassonografia_w is not null) then
	ds_item_w := ds_item_w || '277' || ',';
end if;

if (nr_seq_mapeamento_w is not null) then
	ds_item_w := ds_item_w || '276' || ',';
end if;

if (nr_seq_aberrometria_w is not null) then
	ds_item_w := ds_item_w || '302' || ',';
end if;
   

ds_item_w := SUBSTR(ds_item_w,1,LENGTH(ds_item_w)-1);

return ds_item_w;

end obter_se_possui_anexo_oft;
/