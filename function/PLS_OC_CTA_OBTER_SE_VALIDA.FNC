create or replace
function pls_oc_cta_obter_se_valida(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_seq_conta_p		number,
					nr_seq_conta_proc_p	number,
					nr_seq_conta_mat_p	number)
						return pls_tipos_ocor_pck.ret_valid is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se a conta ou item se encaixam na regra de valida��o associada
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
ATOMIZA��O. N�o ficar fazendo tratamentos e selects internos, apenas chamar a procedure
de cada valida��o aqui
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

retorno_w	pls_tipos_ocor_pck.ret_valid; 

begin
retorno_w.nr_sequencia		:= null;
retorno_w.ie_aplicavel		:= 'N';
retorno_w.ie_gera_ocorrencia	:= 'S';

if	(dados_regra_p.nr_sequencia > 0) then
	/* Somente filtros */
	if	(dados_regra_p.ie_validacao = 1) then
		retorno_w.nr_sequencia		:= null;
		retorno_w.ie_aplicavel		:= 'N';
		retorno_w.ie_gera_ocorrencia	:= 'S';
	/* Validar sexo exclusivo do procedimento */
	elsif	(dados_regra_p.ie_validacao = 2) then
		retorno_w	:= pls_oc_cta_obter_val_cart(dados_regra_p,nr_seq_conta_p);
	/* Validar carteirinha */
	elsif	(dados_regra_p.ie_validacao = 3) then
		retorno_w	:= pls_oc_cta_obter_val_sexo_exc(dados_regra_p,nr_seq_conta_proc_p);
	end if;
end if;

return	retorno_w;

end pls_oc_cta_obter_se_valida;
/