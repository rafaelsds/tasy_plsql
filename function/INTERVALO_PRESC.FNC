create or replace function INTERVALO_PRESC(nr_atendimento_p	number) return varchar is
		
intervalo_desc_w  intervalo_prescricao.ds_intervalo%TYPE;
ie_refeicao_w  atend_previsao_alta.ie_refeicao%TYPE;

begin
    select max(ie_refeicao)
    into ie_refeicao_w
    from atend_previsao_alta
    where nr_atendimento = nr_atendimento_p;
    
    SELECT max(ds_intervalo)
    into intervalo_desc_w
    FROM intervalo_prescricao
    WHERE ie_operacao = 'F'
    AND REGEXP_LIKE (ds_horarios ,'^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$')
    and ds_horarios = ie_refeicao_w;

return intervalo_desc_w;

end INTERVALO_PRESC;
/
