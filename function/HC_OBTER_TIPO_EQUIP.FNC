create or replace
function hc_obter_tipo_equip(nr_sequencia_p		number)
 		    	return varchar2 is


ie_param_84_w		varchar2(1);
ds_tipo_equip_w		varchar2(255);
			
begin

Obter_Param_Usuario(867, 84, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_param_84_w);

if (ie_param_84_w = 'C') or (ie_param_84_w = 'A') then
	
	select	max(ds_tipo_equip)
	into	ds_tipo_equip_w
	from	hc_pac_equipamento a,
		man_equipamento b,
		man_tipo_equipamento c
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_seq_equip_control = b.nr_sequencia
	and	b.nr_seq_tipo_equip = c.nr_sequencia;
	
elsif (ie_param_84_w = 'E') then

	select	substr(obter_valor_dominio(125, ie_tipo_equipamento),1,255)
	into	ds_tipo_equip_w
	from	hc_pac_equipamento a,
		equipamento b
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_seq_equip_hosp = b.cd_equipamento;
	
end if;

return	ds_tipo_equip_w;

end hc_obter_tipo_equip;
/