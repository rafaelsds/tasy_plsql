create or replace
function obter_desc_bsc_indicador(	nr_seq_indicador_p	number)
				return varchar2 is

nm_indicador_w			varchar2(255);
								 
begin

if	 (nr_seq_indicador_p is not null) then

	 select	substr(nvl(max(obter_desc_expressao(CD_EXP_NM_INDICADOR,nm_indicador)),''),1,255)
	 into	nm_indicador_w
	 from	bsc_indicador
	 where	nr_sequencia = nr_seq_indicador_p;
	 
end if;

return nm_indicador_w;

end obter_desc_bsc_indicador;
/