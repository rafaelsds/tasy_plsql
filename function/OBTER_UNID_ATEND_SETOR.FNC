create or replace 
function obter_unid_atend_setor(
			nr_atendimento_p		number,
			cd_setor_atendimento_p		number,
			ie_informacao_p			Varchar2)
			return varchar2 is


/* Informacao						
	U = Unidade basica - complementar
	UB = Unidade b�sica
	UC = Unidade complementar
	DT = Data entrada unidade
	CS = C�digo Setor	
*/

cd_setor_atendimento_w		number(10,0);
ds_unidade_w			varchar2(255);
cd_unidade_basica_w		varchar2(010);
cd_unidade_compl_w		varchar2(010);
ds_setor_atendimento_w		varchar2(100);
nr_seq_interno_w		number(10);
dt_entrada_unidade_w		varchar2(20);

begin

if	(nvl(nr_atendimento_p,0) > 0) then
	begin

	select	max(nvl(b.nr_seq_interno,0))
	into	nr_seq_interno_w
	from	setor_atendimento a,
		atend_paciente_unidade b
	where	b.nr_atendimento	= nr_atendimento_p
	and	b.cd_setor_atendimento	= a.cd_setor_atendimento
	and	b.cd_setor_atendimento	= cd_setor_atendimento_p;

	if	(nr_seq_interno_w > 0) then
		begin
		
		select	cd_setor_atendimento,
			cd_unidade_basica,
			cd_unidade_compl,
			to_char(dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss')
		into	cd_setor_atendimento_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w,
			dt_entrada_unidade_w
		from	atend_paciente_unidade
		where	nr_seq_interno	= nr_seq_interno_w;

		if	(ie_informacao_p = 'U') then
			ds_unidade_w	:= cd_unidade_basica_w || ' ' || cd_unidade_compl_w;
		elsif	(ie_informacao_p = 'UB') then
			ds_unidade_w	:= cd_unidade_basica_w;	
		elsif	(ie_informacao_p = 'UC') then
			ds_unidade_w	:= cd_unidade_compl_w;
		elsif	(ie_informacao_p = 'DT') then
			ds_unidade_w	:= dt_entrada_unidade_w;
		elsif	(ie_informacao_p = 'CS') then
			ds_unidade_w	:= cd_setor_atendimento_w;
		end if;

		end;
	end if;

	end;
end if;
	
return ds_unidade_w;

end obter_unid_atend_setor;
/