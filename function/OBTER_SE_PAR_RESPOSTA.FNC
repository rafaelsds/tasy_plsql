create or replace
function obter_se_par_resposta(	nr_parecer_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(5);
qt_reg_w	number(15);
begin

select	COUNT(*)
into	QT_REG_W
from	parecer_medico
where	nr_parecer = nr_parecer_p
and		dt_liberacao is not null
and		ie_situacao = 'A';

if	(qt_reg_w	= 0) then
	return 'N';
else
	return 'S';
end if;

end obter_se_par_resposta;
/
