create or replace
function obter_ultima_ordem_entregue(	cd_estabelecimento_p	number,
				cd_material_p		number,
				qt_dias_p			number)
return number is

nr_ordem_compra_w			number(10);
dt_real_entrega_w				date;

cursor c01 is
select	distinct
	a.nr_ordem_compra,
	c.dt_real_entrega
from	ordem_compra_item_entrega c,
	ordem_compra_item b,
	ordem_compra a
where	a.nr_ordem_compra = b.nr_ordem_compra
and	b.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_item_oci = c.nr_item_oci
and	a.cd_estabelecimento = cd_estabelecimento_p
and	b.cd_material = cd_material_p
and	trunc(c.dt_real_entrega,'dd') between trunc(sysdate,'dd') - nvl(qt_dias_p,90) and trunc(sysdate,'dd')
and	a.nr_seq_motivo_cancel is null
and	c.dt_cancelamento is null
and	c.dt_real_entrega is not null
order by	c.dt_real_entrega desc;

begin

open C01;
loop
fetch C01 into	
	nr_ordem_compra_w,
	dt_real_entrega_w;
exit when C01%notfound;
	begin
	nr_ordem_compra_w := nr_ordem_compra_w;
	exit;
	end;
end loop;
close C01;

return	nr_ordem_compra_w;

end obter_ultima_ordem_entregue;
/