create or replace function ie_informacao_sensivel(nm_tabela_p varchar2, nm_atributo_p	varchar2) return varchar2 is
ie_informacao_sensivel_w		varchar2(1);
begin

ie_informacao_sensivel_w := 'N';

if	((nm_tabela_p is not null) and (nm_atributo_p is not null)) then

    select	nvl(max(IE_INFORMACAO_SENSIVEL), 'N')
    into    ie_informacao_sensivel_w
    from    tabela_atributo
    where	nm_tabela = nm_tabela_p
    and     nm_atributo = nm_atributo_p;
    
end if;

return	ie_informacao_sensivel_w;

end ie_informacao_sensivel;

/