create or replace function        OBTAIN_ADMIN_INSTR_COUNT( nr_sequencia_p number)
return varchar2
is

  ds_retorno_w varchar2( 1 ) := 'N';
  nr_admin_ins_count_w number:= 0;

begin

if(nr_sequencia_p is not null) then
 select count(*)
 into   nr_admin_ins_count_w
 from   CPOE_HEMOTERAPIA cm , blood_admin_instructions bai
 where  bai.CD_CPOE_ITEM_SEQ = cm.nr_sequencia
        and bai.cd_cpoe_item_seq= nr_sequencia_p;

    if(nr_admin_ins_count_w > 0) then
        ds_retorno_w := 'S';
    end if;

end if; 

return ds_retorno_w;

end OBTAIN_ADMIN_INSTR_COUNT;
/

