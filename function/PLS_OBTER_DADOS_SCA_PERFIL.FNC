/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter o Produto e tabela de pre�o SCA que foi utilizada na simula��o coletiva do perfil, seperando os dados por ponto e virgula
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_dados_sca_perfil
			(	nr_seq_proposta_p	number)
				return varchar2 is
			
ds_retorno_w			varchar2(255);
nr_seq_simul_perfil_w		number(10);
nr_seq_plano_sca_w		number(10);
nr_seq_tabela_sca_w		number(10);

Cursor C01 is
	select	nr_seq_plano,
		nr_seq_tabela
	from	pls_sca_vinculo
	where	nr_seq_simulacao_perfil	= nr_seq_simul_perfil_w;

begin

select	NR_SEQ_SIMUL_PERFIL
into	nr_seq_simul_perfil_w
from	pls_proposta_adesao
where	nr_sequencia	= nr_seq_proposta_p;

if	(nr_seq_simul_perfil_w is null) then
	return '';
end if;

open C01;
loop
fetch C01 into	
	nr_seq_plano_sca_w,
	nr_seq_tabela_sca_w;
exit when C01%notfound;
	begin
	
	ds_retorno_w	:= ds_retorno_w||'#'||nr_seq_plano_sca_w||';'||nr_seq_tabela_sca_w||';';
	
	end;
end loop;
close C01;

return	ds_retorno_w;

end pls_obter_dados_sca_perfil;
/
