create or replace
function Obter_se_presc_proc_bolsa(nr_seq_proc_bolsa_p	number)
 		    	return varchar2 is
				
ie_retorno_w	varchar2(10);
begin

select	decode(count(1),0,'N','S') ie_prescr_proc_bolsa
into	ie_retorno_w
from	prescr_proc_bolsa b,
	prescr_proc_hor c
where	b.nr_prescricao = c.nr_prescricao
and	b.nr_seq_procedimento = c.nr_seq_procedimento
and	b.nr_sequencia = nr_seq_proc_bolsa_p
and	obter_status_hor_sol_adep(c.dt_inicio_horario,c.dt_fim_horario,c.dt_suspensao,c.dt_interrupcao,c.ie_dose_especial,null,null,c.nr_prescricao,null) not in ('N','S')
and	nvl(b.ie_bipado, 'N') = 'S';

return	ie_retorno_w;

end Obter_se_presc_proc_bolsa;
/
