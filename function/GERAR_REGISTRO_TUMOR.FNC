create or replace
function Gerar_registro_tumor(	cd_pessoa_fisica_p	varchar2,
				dt_registro_p		Date,
				cd_estabelecimento_p	number)
				return varchar2 is

ds_retorno_w	Varchar2(20);
ano_w		Varchar2(4);
qt_registro_w	varchar2(10);
qt_tumor_w	Number(3);
cd_cnes_w	varchar2(11);
nr_registro_w	varchar2(4);

Cursor C01 is
	select	substr(nr_registro,16,4)
	from	can_ficha_admissao
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	to_char(dt_preench_ficha,'yyyy') = ano_w;

begin

ano_w	:= to_char(dt_registro_p,'yyyy');

OPEN C01;
LOOP
	FETCH C01 INTO
		nr_registro_w;
	exit when c01%notfound;
	nr_registro_w	:= nr_registro_w;
END LOOP;
CLOSE C01;

qt_registro_w	:= nr_registro_w;

if	(nr_registro_w is null) then
	select	lpad(count(*) +1,4,'0')
	into	qt_registro_w
	from	can_ficha_admissao
	where	to_char(dt_preench_ficha,'yyyy') = ano_w;
end if;

select	lpad(max(cd_cnes),11,'0')
into	cd_cnes_w
from	sus_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

if (cd_cnes_w is null) then
	select	lpad(max(cd_cnes_hospital),11,'0')
	into	cd_cnes_w
	from	sus_parametros_apac
	where	cd_estabelecimento	= cd_estabelecimento_p;
end if;


select	count(*) + 1
into	qt_tumor_w
from	can_ficha_admissao
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	to_char(dt_preench_ficha,'yyyy') = ano_w;

ds_retorno_w	:= cd_cnes_w || ano_w || qt_registro_w ||qt_tumor_w;

return ds_retorno_w;

end Gerar_registro_tumor;
/
