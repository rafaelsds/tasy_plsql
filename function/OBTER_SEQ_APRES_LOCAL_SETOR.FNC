create or replace
function obter_seq_apres_local_setor	(nr_sequencia_p	number)
 		    	return number is

ds_retorno_w	number(5);

begin
select	nvl(max(nr_seq_apres),0)
into	ds_retorno_w
from	pa_local
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end obter_seq_apres_local_setor;
/