create or replace
function obter_origem_proc_prescr	(nr_prescricao_p	number,
					nr_seq_proced_p	number)
					return number is

ie_origem_proced_w	number(1,0);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_proced_p is not null) then
	select	max(ie_origem_proced)
	into	ie_origem_proced_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_seq_proced_p;
end if;

return ie_origem_proced_w;

end obter_origem_proc_prescr;
/