create or replace
function pls_obter_nome_operadora_estab
		(	cd_estabelecimento_p		Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(255);

begin

begin
select	substr(obter_razao_social(cd_cgc_outorgante),1,255)
into	ds_retorno_w
from	pls_outorgante 
where	cd_estabelecimento	= cd_estabelecimento_p;
exception
when others then
	ds_retorno_w	:= '';
end;

return	ds_retorno_w;

end pls_obter_nome_operadora_estab;
/
