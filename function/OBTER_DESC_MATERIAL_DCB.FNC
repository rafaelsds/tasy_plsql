CREATE OR REPLACE
FUNCTION Obter_Desc_Material_dcb(
					cd_material_p	Number)
					RETURN VARCHAR IS

ds_retorno_w			Varchar2(255);

BEGIN

select	substr(nvl(max(a.ds_dcb),obter_desc_material(cd_material_p)),1,255)
into	ds_retorno_w
from	material c,
	dcb_medic_controlado a,
	medic_ficha_tecnica d
where	c.nr_seq_ficha_tecnica	= d.nr_sequencia
and	a.nr_sequencia 		= d.nr_seq_dcb
and	c.cd_material 		= cd_material_p;

RETURN ds_retorno_w;

END Obter_Desc_Material_dcb;
/
