create or replace
function obter_desc_ecog(	nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(240);				
			
begin

select	max(ds_nivel)
into	ds_retorno_w
from	nivel_capac_funcional_ecog
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end obter_desc_ecog;
/