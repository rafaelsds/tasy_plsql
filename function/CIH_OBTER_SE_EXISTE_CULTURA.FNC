create or replace
function cih_obter_se_existe_cultura( nr_ficha_ocorrencia_p		number,
				nr_seq_cultura_p			number)
 		    	return number is
qt_retorno_w	number;
begin
if (nr_ficha_ocorrencia_p is not null) and (nr_seq_cultura_p is not null) then
	begin
	select	count(*)
	into	qt_retorno_w
	from   	CIH_CULTURA_MEDIC 
	where  	nr_ficha_ocorrencia = nr_ficha_ocorrencia_p
	and	nr_seq_cultura = nr_seq_cultura_p;
	end;
end if;
return	qt_retorno_w;

end cih_obter_se_existe_cultura;
/