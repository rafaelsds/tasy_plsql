CREATE OR REPLACE
FUNCTION Obter_Data_Result_Exame(	
				nr_prescricao_p		Number,
				nr_seq_prescricao_p	Number)
				RETURN Date IS

dt_aprovacao_w		Date;

BEGIN

begin
select	max(a.dt_resultado) dt_resultado
into	dt_aprovacao_w
from	exame_lab_result_item b,
	exame_lab_resultado a
where	a.nr_prescricao		= nr_prescricao_p
and	a.nr_seq_resultado	= b.nr_seq_resultado
and	b.nr_seq_prescr		= nr_seq_prescricao_p;
exception
	when others then
	dt_aprovacao_w	:= null;
end;

RETURN dt_aprovacao_w;

END Obter_Data_Result_Exame;
/