CREATE OR REPLACE FUNCTION OBTER_VERSION (NR_SEQUENCIA_P NUMBER,
                                          TYPE_P VARCHAR2)
                                          RETURN NUMBER AS
NR_VERSAO_W NUMBER(10);
BEGIN

SELECT NVL(NR_VERSAO,1)
  INTO NR_VERSAO_W
  FROM PROTOCOLO_INTEGRADO
  WHERE NR_SEQUENCIA = NR_SEQUENCIA_P;

RETURN NR_VERSAO_W;

END OBTER_VERSION;

/
