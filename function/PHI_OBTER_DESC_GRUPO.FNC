create or replace function
phi_obter_desc_grupo (
	nr_seq_grupo_p		grupo_desenvolvimento.nr_sequencia%type,
	nr_seq_idioma_p		idioma.nr_sequencia%type default null
) return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Objective: 
-------------------------------------------------------------------------------------------------------------------
Direct call: 
[  ]  Dictionary objects [  ] Tasy (Delphi/Java/HTML5) [  ] Portal [  ]  Reports [ ] Others:
 ------------------------------------------------------------------------------------------------------------------
Attention points: 
-------------------------------------------------------------------------------------------------------------------
References: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(4000 char) := null;
nr_seq_idioma_w		idioma.nr_sequencia%type;

begin

if	(phi_is_base_philips = 'S') then
	begin
	
		nr_seq_idioma_w := coalesce(nr_seq_idioma_p, wheb_usuario_pck.get_nr_seq_idioma, 5);
	
		select	substr(coalesce(obter_desc_exp_idioma(gd.cd_exp_grupo, nr_seq_idioma_w), gd.ds_grupo), 1, 4000)
		into	ds_retorno_w
		from	grupo_desenvolvimento gd
		where	gd.nr_sequencia = nr_seq_grupo_p;
	end;
end if;

	return ds_retorno_w;
	
end phi_obter_desc_grupo;
/
