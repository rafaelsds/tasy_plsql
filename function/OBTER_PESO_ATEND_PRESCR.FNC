create or replace 
function obter_peso_atend_prescr ( nr_atendimento_p	number)
				return number is

nr_prescricao_w	number(14,0);
qt_peso_w	number(6,3);
ds_retorno_W	varchar2(150);

begin

select	max(nr_prescricao)
into	nr_prescricao_w
from	prescr_medica
where	nr_atendimento	=	nr_atendimento_p
and	qt_peso is not null;

select	nvl(qt_peso,0)
into	qt_peso_w
from	prescr_medica
where	nr_prescricao	=	nr_prescricao_w;

return	qt_peso_w;

end obter_peso_atend_prescr;
/