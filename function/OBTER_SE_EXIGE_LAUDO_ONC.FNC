create or replace
function obter_se_exige_laudo_onc( nr_seq_paciente_p 	number,
									nr_atendimento_p	number)
		return varchar2 is

cd_protocolo_w				number(10);	
nr_seq_prot_medicacao_w 	number(6);	
ie_exige_laudo_sus_w 		varchar2(1);
nr_seq_laudo_sus_w 			number(10);
cd_pessoa_fisica_w 			number(10) := 0;
qt_laudo_prenchido_w 		number(10) := 0;
ie_verifica_Laudo_SUS_w		varchar2(3);
ie_verifica_convenio_sus_w	varchar2(3);
ie_tipo_convenio_w			number(2);	
	
ie_retorno_w 				varchar2(1) := 'N';
	
begin


obter_param_usuario(281,1464,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_verifica_Laudo_SUS_w);
obter_param_usuario(281,996,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_verifica_convenio_sus_w);



select	max(cd_protocolo), 					--1
		nvl(max(nr_seq_medicacao),0),				--2
		nvl(max(nr_seq_laudo_sus),0),				--3
		max(cd_pessoa_fisica)				--4
into	cd_protocolo_w,					--1
		nr_seq_prot_medicacao_w,		--2
		nr_seq_laudo_sus_w,				--3
		cd_pessoa_fisica_w 				--4
from	paciente_setor
where	nr_seq_paciente = nr_seq_paciente_p;


if	(nr_seq_prot_medicacao_w > 0) then

	select	UPPER(nvl(max(ie_exigir_laudo_sus),'N')) 
	into	ie_exige_laudo_sus_w
	from	protocolo_medicacao pm 
	where 	pm.cd_protocolo 	=	cd_protocolo_w
	and		pm.nr_sequencia 	=	nr_seq_prot_medicacao_w;
	
	if (ie_verifica_Laudo_SUS_w = 'S') and
		(nvl(nr_atendimento_p,0) > 0 ) then
		
		if 	(ie_verifica_convenio_sus_w = 'N') then
		
			select 	max(IE_TIPO_CONVENIO) 
			into	ie_tipo_convenio_w
			from   	atendimento_paciente
			where	nr_Atendimento = nr_atendimento_p;
			
			if 		(ie_tipo_convenio_w <> 3) then
		
					ie_exige_laudo_sus_w := 'N';
		
			end if;
		
		end if;
		
	
		select	count(*)
		into	qt_laudo_prenchido_w
		from	sus_laudo_paciente
		where	nr_atendimento	=	nr_atendimento_p;	
	
	
	elsif	(cd_pessoa_fisica_w is not null)	then
		
		select	count(*)
		into	qt_laudo_prenchido_w
		from	sus_laudo_paciente
		where	cd_pessoa_fisica	=	cd_pessoa_fisica_w;
		
	end if;
			 
	if	(ie_exige_laudo_sus_w = 'S')	then
		
		if	(nr_seq_laudo_sus_w = 0
				and	qt_laudo_prenchido_w	>	0 
				and ie_verifica_Laudo_SUS_w <> 'N')	then
			ie_retorno_w := 'V';
		elsif	(qt_laudo_prenchido_w	=	0)	then
			ie_retorno_w := 'P';
		elsif	(nr_seq_laudo_sus_w	>	0)	then
			ie_retorno_w := 'N';
		end if;
	else
		ie_retorno_w := 'N';
	end if;
	
end if;

return ie_retorno_w;

end obter_se_exige_laudo_onc;
/