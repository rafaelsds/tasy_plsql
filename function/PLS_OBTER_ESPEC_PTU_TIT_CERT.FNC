create or replace
function pls_obter_espec_ptu_tit_cert(	cd_especialidade_p	especialidade_medica.cd_especialidade%type,
					nr_versao_ptu_p		varchar2)
						return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_retorno_w		varchar2(255);
nr_versao_ptu_w		espec_med_ptu_tit_cert.nr_versao_ptu%type;

begin
if	(nr_versao_ptu_p is not null) then
	select	max(cd_ptu)
	into	cd_retorno_w
	from	espec_med_ptu_tit_cert
	where	cd_especialidade	= cd_especialidade_p
	and	(nr_versao_ptu		= nr_versao_ptu_p or nr_versao_ptu is null);
else
	select	max(nr_versao_ptu)
	into	nr_versao_ptu_w
	from	espec_med_ptu_tit_cert
	where	cd_especialidade	= cd_especialidade_p;
	
	if	(nr_versao_ptu_w is not null) then
		select	max(cd_ptu)
		into	cd_retorno_w
		from	espec_med_ptu_tit_cert
		where	cd_especialidade	= cd_especialidade_p
		and	nr_versao_ptu		= nr_versao_ptu_w;
	else
		select	max(cd_ptu)
		into	cd_retorno_w
		from	espec_med_ptu_tit_cert
		where	cd_especialidade	= cd_especialidade_p;
	end if;
end if;
	
return	cd_retorno_w;

end pls_obter_espec_ptu_tit_cert;
/