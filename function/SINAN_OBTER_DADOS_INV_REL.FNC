create or replace
function sinan_obter_dados_inv_rel(	nr_seq_notificacao_p number,
					nr_linha_p	number,
					ie_domiciliar_p	varchar2,
					ie_opcao_p	varchar2)
 		    	return varchar2 is
nm_contato_w		varchar2(255);
qt_idade_w		number(2);
ie_sintomatico_w	varchar2(3);
ds_observacao_w		varchar2(255);
nr_linha_w		number(2);
ds_retorno_w		varchar2(255);
ie_tipo_contato_w	varchar2(20);
begin
select 	NM_CONTATO,
	QT_IDADE,
	IE_SINTOMATICO,
	DS_OBSERVACAO,
	IE_TIPO_CONTATO,
	linha
into	nm_contato_w,		
        qt_idade_w,	
        ie_sintomatico_w,
        ds_observacao_w,
	ie_tipo_contato_w,
        nr_linha_w	
from (
	select	a.NM_CONTATO NM_CONTATO,
		a.QT_IDADE QT_IDADE,
		decode(a.IE_SINTOMATICO,'1','Sim','2','N�o') IE_SINTOMATICO,
		a.DS_OBSERVACAO DS_OBSERVACAO,
		decode(a.ie_tipo_contato,'1',wheb_mensagem_pck.get_texto(803080),'2',upper(wheb_mensagem_pck.get_texto(803081)),'3',wheb_mensagem_pck.get_texto(803082),'4',wheb_mensagem_pck.get_texto(803083)) ie_tipo_contato,
		rownum linha
	from  	SINAN_INVESTIGACAO a,
		notificacao_sinan c 
	where	a.nr_seq_notificacao = c.nr_sequencia
	and	c.nr_sequencia = nr_seq_notificacao_p
	and	a.ie_domiciliar = ie_domiciliar_p
	order by 1)
where linha = nr_linha_p;

if	(ie_opcao_p = 'N') then
	ds_retorno_w := nm_contato_w;
elsif   (ie_opcao_p = 'I') then	
	ds_retorno_w := qt_idade_w;
elsif	(ie_opcao_p = 'S') then	
	ds_retorno_w := ie_sintomatico_w;
elsif	(ie_opcao_p = 'O') then
	ds_retorno_w := ds_observacao_w;
elsif	(ie_opcao_p = 'TC') then
	ds_retorno_w := ie_tipo_contato_w;

end if;
	
return	ds_retorno_w;

end sinan_obter_dados_inv_rel;
/
