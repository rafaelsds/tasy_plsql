create or replace 
function obter_desc_leito_unid_compl(nr_seq_unidade_p	number)return varchar2 is

ds_unidade_basica_w	varchar2(255);	
ds_unidade_compl_w	varchar2(255);				

begin
select	max(cd_unidade_basica),
        	max(cd_unidade_compl)
into	ds_unidade_basica_w,
	ds_unidade_compl_w
from	unidade_atendimento
where	nr_seq_interno	= nr_seq_unidade_p
and     	ie_status_unidade <> 'L';

if (trim(ds_unidade_compl_w) is not null) then
    	return ds_unidade_basica_w || ' - ' || ds_unidade_compl_w;
else
    	return ds_unidade_basica_w;
end if;

end obter_desc_leito_unid_compl;
/