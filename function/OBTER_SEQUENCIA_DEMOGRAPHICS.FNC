create or replace
function obter_sequencia_demographics(	nr_seq_item_p	in	number)
										return varchar2 is

ds_return_w			varchar2(255 char);
ds_ie_tipo_inf_w	varchar2(20 char);
ie_first_value		varchar2(1 char) := 1;

cursor	c01 is
select		ie_tipo_inf
from		informacao_suep
where		nr_seq_item = nr_seq_item_p
order by	nr_seq_apres asc;

begin

for r_c01 in c01 loop
	if (r_c01.ie_tipo_inf is not null) then

		CASE r_c01.ie_tipo_inf
			WHEN  'ID' THEN ds_ie_tipo_inf_w := 'CD_PERSON_ID';
			WHEN 'CPF' THEN ds_ie_tipo_inf_w := 'NR_CPF';
			WHEN 'MRS' THEN ds_ie_tipo_inf_w := 'DS_ESTADO_CIVIL';
			WHEN 'HMP' THEN ds_ie_tipo_inf_w := 'NR_TELEFONE_CELULAR';
			WHEN 'WKP' THEN ds_ie_tipo_inf_w := 'NR_TELEFONE';
			WHEN 'ADR' THEN ds_ie_tipo_inf_w := 'DS_ENDERECO';
			WHEN 'CTY' THEN ds_ie_tipo_inf_w := 'DS_MUNICIPIO';
			WHEN 'STA' THEN ds_ie_tipo_inf_w := 'SG_ESTADO';
			WHEN 'CTR' THEN ds_ie_tipo_inf_w := 'CD_PAIS';
			WHEN 'ZIP' THEN ds_ie_tipo_inf_w := 'CD_CEP';
			WHEN 'RAC' THEN ds_ie_tipo_inf_w := 'DS_RACA';
			WHEN 'PFS' THEN ds_ie_tipo_inf_w := 'DS_PROFISSAO';
			WHEN 'INS' THEN ds_ie_tipo_inf_w := 'DS_CONVENIO';
			WHEN 'MDC' THEN ds_ie_tipo_inf_w := 'NM_MEDICO';
			ELSE null;
		END CASE;

		if (ie_first_value = 1) then
			ds_return_w := ds_return_w || ds_ie_tipo_inf_w;
			ie_first_value := 0;
		else
			ds_return_w := ds_return_w || ',' || ds_ie_tipo_inf_w;
		end if;
	end if;
end loop;

return ds_return_w;

end obter_sequencia_demographics;
/
