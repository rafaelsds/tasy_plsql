create or replace
function pls_obter_desc_material_regra
		(	nr_seq_material_p	Number)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(255)	:= '';
qt_regra_w		number(10);

begin
select	count(1)
into	qt_regra_w
from 	pls_material_restricao x
where 	x.nr_seq_material	= nr_seq_material_p
and 	nvl(x.ie_bloqueia_autor,'N') = 'S'
and	sysdate	between	dt_inicio_vigencia and nvl(dt_fim_vigencia, sysdate);

if	(qt_regra_w = 0) then
	begin
		select	ds_material
		into	ds_retorno_w
		from	pls_material
		where	nr_sequencia	= nr_seq_material_p;
	exception
	when no_data_found then
		ds_retorno_w	:= '';
	end;
else
	ds_retorno_w	:= '';
end if;

return	ds_retorno_w;

end pls_obter_desc_material_regra;
/
