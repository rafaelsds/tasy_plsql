create or replace
function obter_alerta_sv_peso(	cd_pessoa_fisica_p		Varchar2)
				return Varchar2 is
				
ds_retorno_w	Varchar2(255) := '';
nr_sequencia_w	atendimento_sinal_vital.nr_sequencia%type;

begin

	select	coalesce(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	atendimento_sinal_vital
	where	cd_paciente = cd_pessoa_fisica_p
	and	coalesce(qt_peso,0) > 0;
		
	if	(nr_sequencia_w > 0) then
		ds_retorno_w	:= substr(obter_alerta_sinal_vital(nr_sequencia_w),1,255);		
	end if;
	
return	ds_retorno_w;

end obter_alerta_sv_peso;
/