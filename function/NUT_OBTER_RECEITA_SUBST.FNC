create or replace function nut_obter_receita_subst (	cd_pessoa_fisica_p	varchar2,
					nr_seq_servico_p	number,
					nr_seq_receita_old_p	number)
 		    	return number DETERMINISTIC is
			
nr_seq_receita_new_w	number;
nr_sequencia_w		nut_regra_subst_receita.nr_sequencia%type;

begin

if	(nr_seq_receita_old_p is not null) then

	select	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	nut_regra_subst_receita a
	where	a.nr_seq_servico = nr_seq_servico_p
	and	a.nr_seq_receita_old = nr_seq_receita_old_p
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	sysdate between COALESCE(a.dt_inicio, sysdate) and COALESCE(a.dt_fim, sysdate);
	
	if	(nr_sequencia_w is null) then
	
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	nut_regra_subst_receita a
		where	a.nr_seq_servico is null
		and	a.nr_seq_receita_old = nr_seq_receita_old_p
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	sysdate between COALESCE(a.dt_inicio, sysdate) and COALESCE(a.dt_fim, sysdate);
	
	end if;
	
	select	max(a.nr_seq_receita_new)
	into	nr_seq_receita_new_w
	from	nut_regra_subst_receita a
	where	a.nr_sequencia = nr_sequencia_w;

end if;

return	nr_seq_receita_new_w;

end nut_obter_receita_subst;
/