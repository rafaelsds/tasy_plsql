create or replace 
Function Obter_Saldo_Inicial_Fluxo(
				cd_estabelecimento_p	number,
				dt_referencia_p	Date,
				cd_empresa_p		number) 
				return number as

vl_inicial_w			number(15,2);

BEGIN

select	sum(nvl(vl_saldo,0))
into	vl_inicial_w
from	banco_saldo b,
	banco_estabelecimento a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.ie_fluxo_caixa	= 'S'
and	a.nr_sequencia	= b.nr_seq_conta
and	b.dt_referencia	= trunc(dt_referencia_p,'month');

return nvl(vl_inicial_w,0);

end Obter_Saldo_Inicial_Fluxo;
/