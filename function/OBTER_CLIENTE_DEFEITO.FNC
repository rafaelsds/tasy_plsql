create or replace
function obter_cliente_defeito(nr_seq_ordem_p		varchar2)
 		    	return varchar2 is

ie_doc_cliente_w	varchar2(1):= 'N';
ie_origem_erro_w	varchar2(4);
ie_possui_doc_w		varchar2(1);
			
begin
if	(nvl(nr_seq_ordem_p,0) > 0) then
	begin
	select	decode(count(*),0,'N','S')
	into	ie_possui_doc_w
	from	man_doc_erro
	where	nr_seq_ordem = nr_seq_ordem_p
	and 	dt_liberacao is not null;

	if	(nvl(ie_possui_doc_w,'N') = 'S')	then
		select  max(ie_origem_erro)
		into 	ie_origem_erro_w
		from 	man_doc_erro
		where 	nr_seq_ordem = nr_seq_ordem_p;
	end if;
	
	
	if(ie_origem_erro_w  = 'C') then
		ie_doc_cliente_w:= 'S';
	else
		ie_doc_cliente_w:= 'N';
	end if;
	end;
end if;

return	ie_doc_cliente_w;

end obter_cliente_defeito;
/