create or replace 
FUNCTION obter_se_item_alterada(dt_atualizacao_p date, dt_atualizacao_nrec_p date) RETURN Varchar2 IS

begin

if (dt_atualizacao_p > dt_atualizacao_nrec_p) then
	return 'U';
else
	return 'I';
end if;

END obter_se_item_alterada;
/