create or replace
function obter_info_w_item_prescr(nr_prescricao_p	number,
								  nr_seq_item_p 	number,	
								  ie_origem_inf_p	varchar2,
								  tp_informacao_p	varchar2)
 		    	return varchar2 is

resultado_w	varchar2(30);

begin

if (tp_informacao_p = 'D') then

	select	max(qt_dose)
	into	resultado_w
	from 	w_item_prescr 
	where  	nr_prescricao	= nr_prescricao_p
	and		nr_seq_item		= nr_seq_item_p
	and		ie_origem_inf	= ie_origem_inf_p;

elsif (tp_informacao_p = 'U') then

	select	max(cd_unidade_medida)
	into	resultado_w
	from 	w_item_prescr 
	where  	nr_prescricao	= nr_prescricao_p
	and		nr_seq_item		= nr_seq_item_p
	and		ie_origem_inf	= ie_origem_inf_p;

end if;

return	resultado_w;

end obter_info_w_item_prescr;
/