create or replace
function pls_obter_dados_item_notif
			(	nr_seq_item_p		number,
				ie_opcao_p		varchar2)
				return varchar2 is

/*	ie_opcao_p
	VJ - Valor de juros
	VM - Valor de multa
	VP - Valor a pagar
	DL - Data de liquida��o
	S - Situa��o do t�tulo
	DM - Data mensalidade
	QD - Varia��o de dias entre a data do lote e a data de vencimento do t�tulo
	VS - Valor do saldo do t�tulo 
	DRM - Data Refer�ncia da Mensalidade*/
	
ds_retorno_w			varchar2(255);
ds_situacao_w			varchar2(50);
ie_data_base_w			varchar2(10);
vl_juros_w			number(15,2);
vl_multa_w			number(15,2);
vl_saldo_titulo_w		number(15,2);
nr_titulo_w			number(10);
nr_seq_mensalidade_w		number(10);
nr_seq_regra_w			number(10);
dt_liquidacao_w			date;
dt_ref_mensalidade_w		date;
dt_lote_w			date;
dt_pagamento_previsto_w		date;
dt_ref_mensal_segur_w		date;

begin
select	max(a.dt_lote),
	max(a.nr_seq_regra)
into	dt_lote_w,
	nr_seq_regra_w
from	pls_notificacao_lote	a,
	pls_notificacao_pagador	b,
	pls_notificacao_item	c
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_notific_pagador
and	c.nr_sequencia	= nr_seq_item_p;

select	nr_titulo,--substr(pls_obter_titulo_mensalidade(nr_seq_mensalidade,null),1,255),
	nr_seq_mensalidade
into	nr_titulo_w,
	nr_seq_mensalidade_w
from	pls_notificacao_item
where	nr_sequencia	= nr_seq_item_p;

select 	nvl(max(ie_data_base),'VA')
into   	ie_data_base_w
from    pls_notificacao_regra
where   nr_sequencia    = nr_seq_regra_w
and     ie_situacao     = 'A';

/*select	obter_juros_multa_titulo(nr_titulo,sysdate,'R','J'),
	obter_juros_multa_titulo(nr_titulo,sysdate,'R','M'),
	vl_saldo_titulo,
	dt_liquidacao,
	substr(obter_valor_dominio(710,ie_situacao),1,50),
	dt_pagamento_previsto
into	vl_juros_w,
	vl_multa_w,
	vl_saldo_titulo_w,
	dt_liquidacao_w,
	ds_situacao_w,
	dt_pagamento_previsto_w
from	titulo_receber
where	nr_titulo	= nr_titulo_w;

select	trunc(dt_referencia,'month')
into	dt_ref_mensalidade_w
from	pls_mensalidade
where	nr_sequencia	= nr_seq_mensalidade_w;*/

if	(ie_opcao_p = 'VJ') then
	select	obter_juros_multa_titulo(nr_titulo,sysdate,'R','J')
	into	vl_juros_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
	
	ds_retorno_w	:= to_char(vl_juros_w);
elsif	(ie_opcao_p = 'VM') then
	select	obter_juros_multa_titulo(nr_titulo,sysdate,'R','M')
	into	vl_multa_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
	
	ds_retorno_w	:= to_char(vl_multa_w);
elsif	(ie_opcao_p = 'VP') then
	select	obter_juros_multa_titulo(nr_titulo,sysdate,'R','J'),
		obter_juros_multa_titulo(nr_titulo,sysdate,'R','M'),
		vl_saldo_titulo
	into	vl_juros_w,
		vl_multa_w,
		vl_saldo_titulo_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
	
	ds_retorno_w	:= to_char(vl_saldo_titulo_w + vl_multa_w + vl_juros_w);
elsif	(ie_opcao_p = 'DL') then
	select	dt_liquidacao
	into	dt_liquidacao_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
	
	ds_retorno_w	:= to_char(dt_liquidacao_w,'dd/mm/yyyy');
elsif	(ie_opcao_p = 'S') then
	select	substr(obter_valor_dominio(710,ie_situacao),1,50)
	into	ds_situacao_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
	
	ds_retorno_w	:= ds_situacao_w;
elsif	(ie_opcao_p = 'DM') then
	select	trunc(dt_referencia,'month')
	into	dt_ref_mensalidade_w
	from	pls_mensalidade
	where	nr_sequencia	= nr_seq_mensalidade_w;
	
	ds_retorno_w	:= to_char(dt_ref_mensalidade_w,'dd/mm/yyyy');
elsif	(ie_opcao_p = 'QD') then
	select	decode(ie_data_base_w,'VA',dt_pagamento_previsto,dt_vencimento)
	into	dt_pagamento_previsto_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
	
	ds_retorno_w	:= to_char(dt_lote_w - dt_pagamento_previsto_w);
elsif	(ie_opcao_p = 'VS') then
	select	vl_saldo_titulo
	into	vl_saldo_titulo_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
	
	ds_retorno_w	:= to_char(vl_saldo_titulo_w);
elsif	(ie_opcao_p = 'DRM') then	
	select	trunc(b.dt_mesano_referencia,'month')
	into	dt_ref_mensal_segur_w
	from	pls_lote_mensalidade	b,
		pls_mensalidade		a
	where	b.nr_sequencia		= nr_seq_lote
	and	a.nr_sequencia		= nr_seq_mensalidade_w;	
	
	ds_retorno_w	:= to_char(dt_ref_mensal_segur_w,'dd/mm/yyyy');	
end if;

return	ds_retorno_w;

end pls_obter_dados_item_notif;
/