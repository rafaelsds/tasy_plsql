create or replace
function obter_status_anterior_presc (  nr_prescricao_p		number,
					nr_sequencia_p		number,
					ie_status_exec_p	varchar2)
 		    	return varchar2 is
			
ie_status_exec_w	varchar2(10);
ie_status_w		varchar2(10);
dt_ultimo_status_w	date;	

Cursor C01 is
	select  ie_status_exec
	from	prescr_proc_status
	where   nr_prescricao = nr_prescricao_p
	and	nr_seq_prescr = nr_sequencia_p
	and	ie_status_exec < ie_status_exec_p
	order   by dt_atualizacao_nrec;
	
begin

open C01;
loop
fetch C01 into	
	ie_status_w;
exit when C01%notfound;
	begin	
	ie_status_exec_w := ie_status_w;
	end;
end loop;
close C01;


return	ie_status_exec_w;

end obter_status_anterior_presc;
/