create or replace
function Obter_Se_Diag_Sae_Lib	(	nr_seq_sae_p	number,
					nr_seq_diagnostico_p	number)
 		    	return varchar2 is
ds_retorno_w		varchar2(10)	:= 'S';
qt_reg_w		number(10);
cd_setor_Atendimento_w	number(10);
qt_idade_w		number(10);
ie_sexo_w		varchar2(10);
cd_estabelecimento_w	number(10);
begin

select	count(*)
into	qt_reg_w
from	PE_DIAGNOSTICO_REGRA
where	nr_seq_diagnostico	= nr_seq_diagnostico_p;

if	(qt_reg_w	> 0) then
	
	select	nvl(cd_setor_atendimento,0),
		nvl(obter_idade(dt_nascimento,sysdate,'A'),0),
		nvl(ie_sexo,'I')
	into	cd_setor_Atendimento_w,
		qt_idade_w,
		ie_sexo_w
	from	pe_prescricao a,
		pessoa_fisica b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nr_sequencia		= nr_seq_sae_p;
	
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
	
	select	count(*)
	into	qt_reg_w
	from	PE_DIAGNOSTICO_REGRA
	where	nr_seq_diagnostico	= nr_seq_diagnostico_p
	and	qt_idade_w between nvl(qt_idade_min,0) and nvl(qt_idade_max,999)
	and	nvl(ie_sexo,ie_sexo_w)				= ie_sexo_w
	and	nvl(cd_setor_atendimento,cd_setor_Atendimento_w) = cd_setor_Atendimento_w
	and	nvl(cd_estabelecimento,cd_Estabelecimento_w) = cd_estabelecimento_w;
	
	if	(qt_reg_w	= 0) then
		ds_retorno_w	:= 'N';
	end if;
end if;

return	ds_retorno_w;

end Obter_Se_Diag_Sae_Lib;
/