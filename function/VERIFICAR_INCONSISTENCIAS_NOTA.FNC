create or replace
function verificar_inconsistencias_nota
			(	nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w			varchar2(4000);
ds_inconsistencia_w		varchar2(255) default '';

cursor c01 is
select 	nvl(a.ds_consistencia,'XX')
from 	nota_fiscal_consist a
where 	a.nr_seq_nota = nr_sequencia_p
and 	a.ie_forma_consistencia = 'S';

begin

	open c01;
	loop
	fetch c01 into
		ds_inconsistencia_w;
	exit when c01%notfound;
		begin
		if	(ds_inconsistencia_w <> 'XX') then
			ds_retorno_w := ds_retorno_w || ds_inconsistencia_w || chr(10);
		end if;
		end;
	end loop;
	close c01;

return	ds_retorno_w;

end verificar_inconsistencias_nota;
/