create or replace
Function obter_recibo_caixa_receb( nr_seq_caixa_receb_p number) 
					return number is

ds_retorno_w number(15);

begin

	select 	max(nr_recibo)
	into	ds_retorno_w
	from	caixa_receb
	where	nr_sequencia = nr_seq_caixa_receb_p;
	
return	ds_retorno_w;

end obter_recibo_caixa_receb;
/