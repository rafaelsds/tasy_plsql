create or replace FUNCTION tws_get_consultation_details(

    nr_sequencia_p         NUMBER,

    ie_consultation_type_p VARCHAR,

    ie_type_p              VARCHAR,
    
    cd_estabelecimento_p NUMBER)

  RETURN VARCHAR2

IS

  ds_result_w           VARCHAR2(255) := '';

  ds_result_w_1           VARCHAR2(255) := '';

  ie_tipo_agendamento_w VARCHAR2(1);

  nr_seq_proc_interno_w VARCHAR2(255);

  /* S - Description status

  V - Dominio value status

  D - Consultation Date

  T - Consultation Type

  HR - Time of Consultation

  TD - Type Description

  CD - Cancellation Date

  DS - Notification description
  
  ED - End Date 
  
  DU - Duration */

BEGIN

  IF ( ie_consultation_type_p = 'AC' ) THEN

    BEGIN

      SELECT a.cd_tipo_agenda

      INTO ie_tipo_agendamento_w

      FROM agenda a

      INNER JOIN agenda_consulta ac

      ON ac.cd_agenda    = a.cd_agenda

      WHERE ac.nr_sequencia = nr_sequencia_p;

      IF (ie_type_p         = 'D') THEN

        SELECT dt_Agenda

        INTO ds_result_w

        FROM agenda_consulta

        WHERE nr_Sequencia = nr_sequencia_p;

        elsif (ie_type_p      = 'DS') THEN

        

         BEGIN

            IF (ie_tipo_agendamento_w = 3) THEN

                SELECT initcap(Obter_nome_pessoa_fisica(a.CD_PESSOA_FISICA, NULL))

                INTO ds_result_w

                FROM agenda_consulta ac ,

                agenda a

                WHERE ac.cd_agenda  = a.cd_agenda

                AND ac.nr_Sequencia = nr_sequencia_p;

            ELSE

                SELECT nvl (c.ds_especialidade,'') INTO ds_result_w

                FROM   especialidade_medica c ,agenda_consulta ac , agenda a

                WHERE  c.cd_especialidade = a.cd_especialidade  and  ac.cd_agenda  = a.cd_agenda

                AND ac.nr_Sequencia = nr_sequencia_p;

            END IF;

          END;

        elsif (ie_type_p      = 'HR') THEN

          SELECT PKG_DATE_FORMATERS.TO_VARCHAR(DT_AGENDA,'shortTime',pkg_date_formaters.getUserLanguageTag(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO,WHEB_USUARIO_PCK.GET_NM_USUARIO), NULL) hr_agenda_inicio

          INTO ds_result_w

          FROM agenda_consulta

          WHERE nr_Sequencia = nr_sequencia_p;

        elsif (ie_type_p     = 'S') THEN

          BEGIN

            IF (ie_tipo_agendamento_w = 3) THEN

              SELECT tws_get_expression_desc_idioma(8452,tws_status_appointment_domain(nr_sequencia_p, 'CO'),NULL,cd_estabelecimento_p)

              INTO ds_result_w

              FROM dual;

            ELSE

              SELECT tws_get_expression_desc_idioma(8452,tws_status_appointment_domain(nr_sequencia_p, 'SE'),NULL,cd_estabelecimento_p)

              INTO ds_result_w

              FROM dual;

            END IF;

          END;

        elsif (ie_type_p ='V') THEN

          BEGIN

            IF (ie_tipo_agendamento_w = 3) THEN

              SELECT tws_status_appointment_domain(nr_sequencia_p, 'CO')

              INTO ds_result_w

              FROM dual;

            ELSE

              SELECT tws_status_appointment_domain(nr_sequencia_p, 'SE')

              INTO ds_result_w

              FROM dual;

            END IF;

          END;

        elsif (ie_type_p ='T') THEN

          ds_result_w   := ie_tipo_agendamento_w;

        elsif (ie_type_p ='TD') THEN

          SELECT tws_get_expression_desc_idioma(2772,ie_tipo_agendamento_w,NULL,cd_estabelecimento_p)

          INTO ds_result_w

          FROM dual;

        elsif (ie_type_p ='CD') THEN

          SELECT TO_CHAR(dt_cancelamento,'dd-MM-yyyy hh24:mi:ss')

          INTO ds_result_w

          FROM agenda_consulta

          WHERE nr_sequencia = nr_sequencia_p;
          
       elsif (ie_type_p ='DU') THEN
        
         select nr_minuto_duracao 
         
         INTO ds_result_w
         
         from agenda_consulta
         
         WHERE nr_sequencia =nr_sequencia_p;
         
       elsif (ie_type_p ='ED') THEN
        
         select pkg_date_formaters.To_varchar(( dt_agenda + ( 1 / 1440 * nr_minuto_duracao ) ),  'short', pkg_date_formaters.Getuserlanguagetag  (wheb_usuario_pck.get_cd_estabelecimento,  wheb_usuario_pck.get_nm_usuario), NULL) into ds_result_w from agenda_consulta  where nr_sequencia=nr_sequencia_p;
         
        END IF;

      END;

    elsif (ie_consultation_type_p = 'AP') THEN

      BEGIN

        SELECT a.cd_tipo_agenda

        INTO ie_tipo_agendamento_w 

        FROM agenda a

        INNER JOIN agenda_paciente ac

        ON ac.cd_agenda    = a.cd_agenda

        WHERE ac.nr_sequencia = nr_sequencia_p;

        

        SELECT ac.nr_seq_proc_interno

        INTO nr_seq_proc_interno_w 

        FROM agenda a

        INNER JOIN agenda_paciente ac

        ON ac.cd_agenda    = a.cd_agenda

        WHERE ac.nr_sequencia = nr_sequencia_p;

        

        

        IF (ie_type_p         = 'D') THEN

          SELECT dt_Agenda

          INTO ds_result_w

          FROM agenda_paciente

          WHERE nr_Sequencia = nr_sequencia_p;

          

        elsif (ie_type_p      = 'DS') THEN

         

         IF (ie_tipo_agendamento_w = 1  OR ie_tipo_agendamento_w = 2) THEN

          IF (nr_seq_proc_interno_w IS NOT NULL) THEN 

         select substr(max(ds_proc_exame),1,240) into ds_result_w_1 from proc_interno pi, agenda_paciente ap where ap.nr_seq_proc_interno = pi.nr_sequencia and ap.nr_sequencia = nr_sequencia_p; 

         

          ELSE 

         select max(ds_procedimento) INTO ds_result_w_1  from procedimento p , agenda_paciente ap where  p.cd_procedimento = ap.cd_procedimento and p.ie_origem_proced = ap.ie_origem_proced

         and  ap.nr_Sequencia = nr_sequencia_p;

        

        end if;

        end if;

        

         ds_result_w := ds_result_w_1;



        elsif (ie_type_p  = 'HR') THEN

          SELECT PKG_DATE_FORMATERS.TO_VARCHAR(hr_inicio,'shortTime',pkg_date_formaters.getUserLanguageTag(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO,WHEB_USUARIO_PCK.GET_NM_USUARIO), NULL) hr_agenda_inicio

          INTO ds_result_w

          FROM agenda_paciente

          WHERE nr_Sequencia = nr_sequencia_p;

        elsif (ie_type_p     = 'S') THEN

          BEGIN

            IF (ie_tipo_agendamento_w = 2) THEN

              SELECT tws_get_expression_desc_idioma(8452,tws_status_appointment_domain(nr_sequencia_p, 'EX'),NULL,cd_estabelecimento_p)

              INTO ds_result_w

              FROM dual;

            ELSE

              SELECT tws_get_expression_desc_idioma(8452,tws_status_appointment_domain(nr_sequencia_p, 'SU'),NULL,cd_estabelecimento_p)

              INTO ds_result_w

              FROM dual;

            END IF;

          END;

        elsif (ie_type_p ='V') THEN

          BEGIN

            IF (ie_tipo_agendamento_w = 2) THEN

              SELECT tws_status_appointment_domain(nr_sequencia_p, 'EX')

              INTO ds_result_w

              FROM dual;

            ELSE

              SELECT tws_status_appointment_domain(nr_sequencia_p, 'SU')

              INTO ds_result_w

              FROM dual;

            END IF;

          END;

        elsif (ie_type_p ='T') THEN

          ds_result_w   := ie_tipo_agendamento_w;

        elsif (ie_type_p ='TD') THEN

          SELECT tws_get_expression_desc_idioma(2772,ie_tipo_agendamento_w,NULL,cd_estabelecimento_p)

          INTO ds_result_w

          FROM dual;

        elsif (ie_type_p ='CD') THEN

          SELECT TO_CHAR(dt_cancelamento,'dd-MM-yyyy hh24:mi:ss')

          INTO ds_result_w

          FROM agenda_paciente

          WHERE nr_sequencia = nr_sequencia_p;
       
        elsif (ie_type_p ='DU') THEN
        
         select nr_minuto_duracao 
         
         INTO ds_result_w
         
         from agenda_paciente
         
         WHERE nr_sequencia =nr_sequencia_p;
         
         elsif (ie_type_p ='ED') THEN
         
        select  Substr(To_char(Obter_final_agendamento(nr_sequencia_p), 'dd/MM/yyyy hh24:mi'), 1, 20) into ds_result_w from dual;   
         
       END IF;
      END;

    elsif (ie_consultation_type_p = 'Q') THEN

      BEGIN

       select ie_status_agenda into ie_tipo_agendamento_w  FROM agenda_quimio WHERE nr_Sequencia = nr_sequencia_p;

        IF (ie_type_p         = 'D') THEN

          SELECT dt_Agenda

          INTO ds_result_w

          FROM agenda_quimio

          WHERE nr_Sequencia = nr_sequencia_p;

        elsif (ie_type_p         = 'DS') THEN

          SELECT wsuite_util_pck.Get_wsuite_expression(297210)

          INTO ds_result_w

          FROM dual;

        elsif (ie_type_p     = 'HR') THEN

          SELECT PKG_DATE_FORMATERS.TO_VARCHAR(DT_AGENDA,'shortTime',pkg_date_formaters.getUserLanguageTag(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO,WHEB_USUARIO_PCK.GET_NM_USUARIO), NULL) hr_agenda_inicio

          INTO ds_result_w

          FROM agenda_quimio

          WHERE nr_Sequencia = nr_sequencia_p;

        elsif (ie_type_p     = 'S') THEN

          SELECT tws_get_expression_desc_idioma(8452,tws_status_appointment_domain(nr_sequencia_p, 'CH'),NULL,cd_estabelecimento_p)

          INTO ds_result_w

          FROM dual;

        elsif (ie_type_p ='V') THEN

          SELECT tws_status_appointment_domain(nr_sequencia_p, 'CH')

          INTO ds_result_w

          FROM dual;

        elsif (ie_type_p ='T') THEN

          ds_result_w   := ie_tipo_agendamento_w;

        elsif (ie_type_p ='TD') THEN

          SELECT tws_get_expression_desc_idioma(2772,ie_tipo_agendamento_w,NULL,cd_estabelecimento_p)

          INTO ds_result_w

          FROM dual;

        elsif (ie_type_p ='CD') THEN

          SELECT TO_CHAR(dt_cancelada,'dd-MM-yyyy hh24:mi:ss')

          INTO ds_result_w

          FROM agenda_quimio

          WHERE nr_sequencia = nr_sequencia_p;
          
      elsif (ie_type_p ='DU') THEN
        
         select nr_minuto_duracao 
         
         INTO ds_result_w
         
         from agenda_quimio
         
         WHERE nr_sequencia =nr_sequencia_p;
         
      elsif (ie_type_p ='ED') THEN    

       select pkg_date_formaters.To_varchar(( dt_agenda + ( 1 / 1440 * nr_minuto_duracao ) ), 'short', pkg_date_formaters.Getuserlanguagetag  (wheb_usuario_pck.get_cd_estabelecimento,  wheb_usuario_pck.get_nm_usuario), NULL) 
       into ds_result_w from agenda_quimio  where nr_sequencia=nr_sequencia_p;
       
        END IF;

      END;

    END IF;

    RETURN ds_result_w;

  END tws_get_consultation_details;
/