create or replace
function obter_se_regra_curta_duracao(	cd_perfil_p			number,
										cd_medico_p			varchar2,
										cd_especialidade_p	number)
 		    		return varchar2 is

ie_retorno_w 		char(1) := 'N';
cd_especialidade_w	medico_especialidade.cd_especialidade%type := 0;
cd_perfil_w			perfil.cd_perfil%type	:= 0;
				
begin

select	nvl(max('S'),'N')
into	ie_retorno_w
from	regra_plt_curta_duracao
where	rownum = 1;

if	(ie_retorno_w = 'S') then
	
	if	(cd_especialidade_p is not null) then
		cd_especialidade_w	:= cd_especialidade_p;
	elsif	(cd_medico_p is not null) then
		cd_especialidade_w	:= obter_especialidade_medico2(cd_medico_p);
	end if;
	
	cd_perfil_w		:= nvl(cd_perfil_p,0);
	cd_especialidade_w	:= nvl(cd_especialidade_w,0);
	
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	regra_plt_curta_duracao
	where	nvl(cd_perfil,cd_perfil_w)	= cd_perfil_w
	and		nvl(cd_especialidade,cd_especialidade_w) = cd_especialidade_w
	and		rownum = 1;
	
end if;

return ie_retorno_w;

end obter_se_regra_curta_duracao;
/