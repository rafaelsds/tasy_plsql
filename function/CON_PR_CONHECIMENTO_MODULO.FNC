create or replace
function con_pr_conhecimento_modulo(cd_consultor_p		number,
				nr_seq_conhecimento_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(15);			
			
begin

select	round(((sum(b.pr_conhecimento))/(count(b.cd_funcao)*100))*100,2)
into	ds_retorno_w
from	com_cons_gest_con_mod a,
	com_cons_gest_con_fun b,
	com_canal_consultor c
where	a.nr_sequencia = b.nr_seq_conhecimento
and	c.nr_sequencia = a.nr_seq_consultor
and	b.nr_seq_conhecimento 	= nr_seq_conhecimento_p
and	c.cd_pessoa_fisica 	= cd_consultor_p;

return	ds_retorno_w;

end con_pr_conhecimento_modulo;
/