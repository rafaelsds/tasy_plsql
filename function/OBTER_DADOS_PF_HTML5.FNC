create or replace function obter_dados_pf_html5
			(	cd_pessoa_fisica_p		varchar2,
				ie_opcao_p			varchar2)
				return date is


/* IE_OPCAO_P 

DN - Data de nascimento
*/
ds_retorno_w pessoa_fisica.dt_nascimento%type;

begin

if	(nvl(cd_pessoa_fisica_p,'0') <> '0') then
begin
	if	(ie_opcao_p	= 'DN') then
  
  begin
		select	max(dt_nascimento)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
  end;
end if;
end;
end if;

return ds_retorno_w;

end obter_dados_pf_html5;

/