create or replace
function pfcs_get_pa_period return date is

dt_base_w		date;
ie_period_pa_w		pfcs_general_rule.ie_period_pa%type;

begin

	select	ie_period_pa
	into	ie_period_pa_w
	from	pfcs_general_rule
	where rownum = 1;

	if (ie_period_pa_w = 'D') then
		select trunc(sysdate) into dt_base_w from dual;
	elsif (ie_period_pa_w = 'U') then
		select sysdate - 1 into dt_base_w from dual;
	end if;

	return dt_base_w;

end;
/
