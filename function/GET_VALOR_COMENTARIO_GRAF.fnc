create or replace function get_valor_comentario_graf( nr_cirurgia_p number,
									nr_seq_registro_p number,
									nm_tabela_p varchar2,
									nm_campo_p varchar2)
 		    	return varchar is
				
ds_resultado_w varchar2(1000);

cursor	c_observ_registro is
select 	substr(obter_nome_pf(b.cd_profissional)
		|| ' - ' || 
		b.ds_observao,1,1000) ds_observao
from 	revisao_cirurgia a,
		revisao_cirurgia_obs b 
where 	a.nr_seq_origem 	= nr_seq_registro_p
and 	upper(a.ie_tabela_referencia) = upper(nm_tabela_p)
and 	a.nr_cirurgia = nr_cirurgia_p
and 	a.nr_sequencia = b.nr_seq_revisao		
and 	a.dt_inativacao is null
and 	nvl(a.ie_situacao, 'A') =  'A'
and 	nvl(b.ie_situacao, 'A') =  'A'
and 	b.dt_liberacao is not null;

cursor	c_observ_registro_campo is
select 	substr(obter_nome_pf(b.cd_profissional)
		|| ' - ' || 
		b.ds_observao,1,1000) ds_observao
from 	revisao_cirurgia a,
		revisao_cirurgia_obs b 
where 	a.nr_seq_origem 	= nr_seq_registro_p
and 	upper(a.ie_tabela_referencia) = upper(nm_tabela_p)
and 	a.nr_cirurgia = nr_cirurgia_p
and 	a.ie_campo_ref_valor = nm_campo_p
and 	a.nr_sequencia = b.nr_seq_revisao		
and 	a.dt_inativacao is null
and 	nvl(a.ie_situacao, 'A') =  'A'
and 	nvl(b.ie_situacao, 'A') =  'A'
and 	b.dt_liberacao is not null;


begin
if (nm_campo_p is null) then
ds_resultado_w:= null;
<<read_c_observ_registro>>
for r_observ_registro in c_observ_registro
	loop	
	ds_resultado_w:= ds_resultado_w || r_observ_registro.ds_observao || '<br>';
	end loop read_c_observ_registro;
else
  <<read_c_observ_registro_campo>>
  for r_observ_registro_campo in c_observ_registro_campo
	loop	
	ds_resultado_w:= ds_resultado_w || r_observ_registro_campo.ds_observao || '<br>';
	end loop read_c_observ_registro_campo;
end if;	
	
return	ds_resultado_w;

end get_valor_comentario_graf;
/