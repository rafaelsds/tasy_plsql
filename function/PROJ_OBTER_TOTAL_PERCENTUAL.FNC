create or replace
function proj_obter_total_percentual
			(	nr_seq_projeto_p		Number)
				return Number is
				
pr_retorno_w			Number(15,2)	:= 0;

begin

/*select	nvl(avg(b.pr_etapa),0)
into	pr_retorno_w
from	proj_cron_etapa	b,
	proj_cronograma	a
where	a.nr_sequencia	= b.nr_seq_cronograma
and	a.nr_seq_proj	= nr_seq_projeto_p
and	a.ie_tipo_cronograma = 'E'
and	dt_aprovacao is not null; */

select	nvl(max(PR_REALIZACAO),0)
into	pr_retorno_w
from	proj_cronograma	a
where a.nr_seq_proj	= nr_seq_projeto_p
and	a.ie_tipo_cronograma = 'E'
and	dt_aprovacao is not null
and	dt_aprovacao = (select max(dt_aprovacao) from proj_cronograma w 
			where w.nr_seq_proj	= nr_seq_projeto_p
			and	w.ie_tipo_cronograma = 'E'
			and	w.dt_aprovacao is not null);


return	pr_retorno_w;

end proj_obter_total_percentual;
/
