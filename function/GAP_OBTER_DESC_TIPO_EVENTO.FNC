create or replace
function gap_obter_desc_tipo_evento(	nr_seq_tipo_evento_p	number)
				return varchar2 is

ds_retorno_w			varchar2(100);

begin

if	(nr_seq_tipo_evento_p is not null) then
	
	select	nvl(max(ds_tipo),'')
	into	ds_retorno_w
	from	gap_tipo_evento
	where	nr_sequencia	= nr_seq_tipo_evento_p;
	
end if;

return	ds_retorno_w;

end gap_obter_desc_tipo_evento;
/