create or replace
function hsm_obter_cbo_medico(cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is
			
nr_seq_cbo_saude_w	number(10);	
cd_cbo_w		varchar(30);

begin

if	(cd_pessoa_fisica_p is not null) then

	select	nvl(max(nr_seq_cbo_saude),0)
	into	nr_seq_cbo_saude_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	if	(nr_seq_cbo_saude_w > 0) then
	
		select	substr(cd_cbo,1,30)
		into	cd_cbo_w
		from	cbo_saude
		where	nr_sequencia = nr_seq_cbo_saude_w;
	
	end if;

end if;

return	cd_cbo_w;

end hsm_obter_cbo_medico;
/