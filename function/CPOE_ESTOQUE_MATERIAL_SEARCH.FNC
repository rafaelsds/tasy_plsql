CREATE OR replace FUNCTION cpoe_estoque_material_search(
cd_material_p          material_order_type.cd_material%TYPE,
nm_usuario_p           VARCHAR2,
cd_setor_atendimento_p NUMBER,
ismedicineinstock_p    VARCHAR2)
RETURN VARCHAR
IS
  nr_count_w NUMBER(3);
BEGIN
    IF ( ismedicineinstock_p = 'S' ) THEN
      SELECT Count(*)
      INTO   nr_count_w
      FROM   movimento_estoque
      WHERE  cd_local_estoque IN((SELECT cd_local_estoque
                                  FROM   setor_atendimento
                                  WHERE
             cd_setor_atendimento IN( cd_setor_atendimento_p )
                                  UNION
                                  SELECT cd_local_estoque
                                  FROM   setor_local
                                  WHERE
             cd_setor_atendimento IN( cd_setor_atendimento_p )))
             AND cd_material = cd_material_p;

      IF ( nr_count_w = 0 ) THEN
        RETURN 'N';
      END IF;
    END IF;

    RETURN 'S';
end;
/
