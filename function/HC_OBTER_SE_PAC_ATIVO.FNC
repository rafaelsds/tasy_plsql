create or replace
function hc_obter_se_pac_ativo( nr_seq_paciente_hc_p	number,
				cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);

begin
ds_retorno_w	:= 'A';
if	(nr_seq_paciente_hc_p is not null) then
	select	max(ie_situacao)
	into	ds_retorno_w
	from	paciente_home_care
	where	nr_sequencia = nr_seq_paciente_hc_p;
elsif	(cd_pessoa_fisica_p is not null) then
	select	nvl(max(ie_situacao),'A')
	into	ds_retorno_w
	from	paciente_home_care
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_inicio = (	select	max(dt_inicio)
				from	paciente_home_care
				where	cd_pessoa_fisica = cd_pessoa_fisica_p);
end if;	

return	ds_retorno_w;

end hc_obter_se_pac_ativo;
/
