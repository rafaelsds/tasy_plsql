create or replace
function	obter_se_material_far_com 	(cd_material_p		number,
				cd_estabelecimento_p	number) return varchar2 is

ds_retorno_w		varchar2(1);

begin

select	max(nvl(ie_farmacia_com,'N'))
into	ds_retorno_w
from	material_estab
where	cd_material = cd_material_p
and	cd_estabelecimento 	= cd_estabelecimento_p;

return ds_retorno_w;

end obter_se_material_far_com;
/