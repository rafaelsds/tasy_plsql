create or replace 
FUNCTION rop_obter_dados_roupa(	nr_sequencia_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(80);
nr_seq_lote_roupa_w	number(10);
cd_barra_roupa_w	varchar2(40);

/*
L - Sequ�ncia do Lote
C - C�digo de Barras
*/

begin
if	(nr_sequencia_p is not null) then
	select	nr_seq_lote_roupa,
		cd_barra_roupa
	into	nr_seq_lote_roupa_w,
		cd_barra_roupa_w
	from	rop_roupa
	where	nr_sequencia = nr_sequencia_p;

	if	(ie_opcao_p = 'L') then
		ds_retorno_w	:= nr_seq_lote_roupa_w;
	elsif	(ie_opcao_p = 'C') then
		ds_retorno_w	:= cd_barra_roupa_w;
	end if;
end if;

return ds_retorno_w;

end rop_obter_dados_roupa;
/