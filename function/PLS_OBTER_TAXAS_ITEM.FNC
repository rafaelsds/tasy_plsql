create or replace
function pls_obter_taxas_item
			(	nr_seq_taxa_p		pls_taxa_item.nr_sequencia%type,
				ie_tipo_p		varchar2)
				return 		Number is
				
tx_w			Number(9,3);

begin

if	(ie_tipo_p = 'M') then

	select	max(tx_material)
	into	tx_w
	from	pls_taxa_item
	where	nr_sequencia	= nr_seq_taxa_p;

elsif	(ie_tipo_p = 'CO') then

	select	max(tx_custo_operacional)
	into	tx_w
	from	pls_taxa_item
	where	nr_sequencia	= nr_seq_taxa_p;

elsif	(ie_tipo_p = 'MED') then

	select	max(tx_medico)
	into	tx_w
	from	pls_taxa_item
	where	nr_sequencia	= nr_seq_taxa_p;

end if;

return	nvl(tx_w,100);

end pls_obter_taxas_item;
/
 