create or replace
function obter_se_amamenta(nr_atendimento_p	number) return varchar2 is

ds_retorno_w varchar2(1);

begin

if(obter_sexo_pf(obter_pessoa_atendimento(nr_atendimento_p, 'C'), 'C') = 'F')then
	select 	nvl(max('S'),'N')
	into	ds_retorno_w
	from	nascimento_amamentacao
	where	nr_atendimento = nr_atendimento_p
	and		dt_liberacao is not null
	and		dt_inativacao is null;
else
	ds_retorno_w := 'N';
end if;

return ds_retorno_w;

end obter_se_amamenta;
/
