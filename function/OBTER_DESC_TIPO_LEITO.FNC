CREATE OR REPLACE
FUNCTION Obter_Desc_Tipo_Leito(	cd_tipo_leito_p	number)
				return varchar2 is

ds_retorno_w	varchar2(80);

BEGIN

select	ds_tipo_leito
into	ds_retorno_w
from	sus_tipo_leito
where	cd_tipo_leito	= cd_tipo_leito_p;

Return  ds_retorno_w;

END Obter_Desc_Tipo_Leito;
/