create or replace
function Unimed_obter_data (nr_atendimento_p  number,
			dt_alta_p date)
 		    	return varchar2 is

dt_retorno_w	date;

begin
if (nvl(nr_atendimento_p,0) > 0) then
	if (dt_alta_p is not null) then
		select 	dt_alta
		into	dt_retorno_w
		from	atendimento_paciente
		where 	nr_atendimento =  nr_atendimento_p;
	end if;

	if (dt_alta_p is null) then
		select 	max(dt_saida_unidade)
		into	dt_retorno_w
		from 	atend_paciente_unidade
		where	nr_atendimento =  nr_atendimento_p
		and	cd_setor_atendimento = 13;
	end if;
end if;

return	dt_retorno_w;

end Unimed_obter_data;
/
