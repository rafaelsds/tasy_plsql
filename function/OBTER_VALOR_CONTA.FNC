create or replace
function  obter_valor_conta(	nr_interno_conta_p	number,
					ie_valor_p			integer)
				   	return number is

/*	ie_valor_p		0 - vl_total
				1 - vl_procedimento
				2 - vl_material
				3 - vl_medico
				4 - vl_custo_operacional
				5 - vl_custo_original_proced
				6 - vl_custo_original_material
				7 - vl_diaria
*/

ie_status_acerto_w		number;
ie_proc_mat_w			number;
vl_medico_w			number(15,2);
vl_anestesista_w		number(15,2);
vl_filme_w			number(15,2);
vl_auxiliares_w			number(15,2);
vl_custo_operacional_w		number(15,2);
vl_item_w			number(15,2);
vl_original_proced_w		number(15,2);
vl_original_material_w		number(15,2);
vl_diaria_w			number(15,2);
nr_seq_proc_pacote_w		number(10,0);
ie_tipo_convenio_w		number(2);
ie_tipo_atendimento_w		number(2);

vl_resultado_w		number(15,2) := 0;

cursor C01 is
	select 	ie_proc_mat,
			nvl(nr_seq_proc_pacote,0),
			nvl(sum(vl_medico),0),
			nvl(sum(vl_anestesista),0),
			nvl(sum(vl_filme),0),
			nvl(sum(vl_auxiliares),0),
			nvl(sum(vl_custo_operacional),0),
			nvl(sum(vl_item),0),
			nvl(sum(decode(ie_proc_mat,1,vl_original,0)),0) vl_orig_proced,
			nvl(sum(decode(ie_proc_mat,2,vl_original,0)),0) vl_orig_mat,
			nvl(sum(decode(ie_proc_mat,1,decode(tp_item,3,vl_item,0),0)),0) vl_diaria
	from 	conta_paciente_valor_v
	where 	nr_interno_conta = nr_interno_conta_p
	  and 	ie_status_acerto_w = 1
--	  and 	qt_item <> 0			Edgar 18/03/2005, OS 16232, qdo glosa parcial fic com qt zero
	  and 	cd_motivo_exc_conta is null
	and	nvl(nr_seq_proc_pacote,0) <> nr_sequencia
	group by ie_proc_mat,
		nvl(nr_seq_proc_pacote,0)
	union
	select 	decode(cd_material,null,1,2),
			nvl(nr_seq_proc_pacote,0),
			nvl(sum(vl_medico),0),
			nvl(sum(vl_anestesista),0),
			nvl(sum(vl_Materiais),0),
			nvl(sum(vl_auxiliares),0),
			nvl(sum(vl_custo_operacional),0),
			nvl(sum(vl_procedimento + vl_material),0),
			nvl(sum(decode(cd_material,null,vl_original,0)),0) vl_orig_proced,
			nvl(sum(decode(cd_material,null,0,vl_original)),0) vl_orig_mat,
			nvl(sum(decode(ie_diaria,'S',vl_procedimento,0)),0) vl_diaria
	from 	conta_paciente_Resumo
	where nr_interno_conta = nr_interno_conta_p
	  and ie_status_acerto_w = 2
	group by decode(cd_material,null,1,2),
		nvl(nr_seq_proc_pacote,0)
	order by 1;

begin

select ie_status_acerto
into	ie_status_acerto_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

open C01;
loop
	fetch C01 into	ie_proc_mat_w,
				nr_seq_proc_pacote_w,
				vl_medico_w,
				vl_anestesista_w,
				vl_filme_w,
				vl_auxiliares_w,
				vl_custo_operacional_w,
				vl_item_w,
				vl_original_proced_w,
				vl_original_material_w,
				vl_diaria_w;
	exit when C01%notfound;


	if 	(ie_proc_mat_w = 1) then
		if 	(ie_valor_p = 0) then
			vl_resultado_w := vl_resultado_w + vl_item_w;
		elsif (ie_valor_p = 1) then
			vl_resultado_w := vl_item_w;
		elsif (ie_valor_p = 3) then
			vl_resultado_w := vl_medico_w + vl_anestesista_w + vl_auxiliares_w;
		elsif (ie_valor_p = 4) then
			vl_resultado_w := vl_custo_operacional_w;
		elsif (ie_valor_p = 5) then
			begin
			if 	(nvl(nr_seq_proc_pacote_w,0) <> 0) then
				vl_resultado_w := vl_original_proced_w;
			else
				vl_resultado_w := vl_item_w;				
			end if;
			end;
		elsif (ie_valor_p = 7) then
			vl_resultado_w := vl_diaria_w;
		end if;

	else
		if 	(ie_valor_p = 0) then
			vl_resultado_w := vl_resultado_w + vl_item_w;
		elsif 	(ie_valor_p = 2) then
			vl_resultado_w := vl_item_w;
		elsif 	(ie_valor_p = 6) then
			begin
			if 	(nvl(nr_seq_proc_pacote_w,0) <> 0) then
				vl_resultado_w := vl_original_material_w;
			else
				vl_resultado_w := vl_item_w;				
			end if;
			end;
		end if;
	end if;
end loop;
close C01;

/* Felipe - 17/08/2007 - OS - 65674 - Existem conv�nios do tipo SUS que deve ter o valor baseado no calculo SUS */
/*
Comentado para a OS 528339 Geliard
begin
select	a.ie_tipo_convenio,
	c.ie_tipo_atendimento
into	ie_tipo_convenio_w,
	ie_tipo_atendimento_w
from	atendimento_paciente	c,
	conta_paciente		b,
	convenio		a
where	a.cd_convenio		= b.cd_convenio_parametro
and	b.nr_atendimento	= c.nr_atendimento
and	b.nr_interno_conta	= nr_interno_conta_p;
exception
	when others then
	ie_tipo_convenio_w	:= 0;
	ie_tipo_atendimento_w	:= 0;
end;

if	(ie_tipo_convenio_w	= 3) and
	(ie_tipo_atendimento_w	= 1) then

if	(ie_tipo_convenio_w	= 10) and 
	(ie_tipo_atendimento_w	= 1) then
	select	sum(nvl(vl_sadt,0)) + sum(nvl(vl_ato_anestesista,0)) + sum(nvl(b.vl_medico,0)) + sum(nvl(vl_matmed,0))
	into	vl_resultado_w
	from	sus_valor_proc_paciente	b,
		procedimento_paciente 	a
	where	a.nr_sequencia		= b.nr_sequencia
	and	a.cd_motivo_exc_conta	is null
	and	a.nr_interno_conta	= nr_interno_conta_p;
end if;*/ 

return nvl(vl_resultado_w,0);

end obter_valor_conta;
/
