create or replace
function obter_dt_prev_retirada_cobr(
				nr_seq_cobranca_p	number)	return date is

dt_retirada_w		date;
nr_seq_orgao_w		number;
dt_inclusao_w		date;
qt_mes_retirada_w	number;

begin

dt_retirada_w	:= null;

select	nvl(c.dt_vencimento,nvl(d.dt_vencimento,a.dt_inclusao)),
	NR_SEQ_ORGAO
into	dt_inclusao_w,
	nr_seq_orgao_w
from	cobranca_orgao a,
	cobranca b,
	titulo_receber c,
	cheque_cr d
where	a.nr_seq_cobranca = b.nr_sequencia
and	b.nr_titulo = c.nr_titulo(+)
and	b.nr_seq_cheque = d.nr_seq_cheque(+)
and	a.nr_sequencia	= nr_seq_cobranca_p;


select  nvl(qt_mes_retirada,0)
into    qt_mes_retirada_w
from    orgao_cobranca
where   nr_sequencia	= nr_seq_orgao_w;


if	(qt_mes_retirada_w <> 0) then
	dt_retirada_w	:= pkg_date_utils.add_month(dt_inclusaO_w, qt_mes_retirada_w,0); 		
end if;

return dt_retirada_w;

end obter_dt_prev_retirada_cobr;
/
