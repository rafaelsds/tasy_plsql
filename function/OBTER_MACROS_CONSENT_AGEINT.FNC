create or replace
function obter_macros_consent_ageint
 		    	return Varchar2 is

ds_macro_w		Varchar2(2000);
ds_enter_w		varchar2(10) := chr(13) || chr(10);
				
begin
ds_macro_w	:= 	'@ds_estabelecimento = ' ||wheb_mensagem_pck.get_texto(684072) || ds_enter_w || --Descri��o do estabelecimento
			'@ds_agenda = ' ||wheb_mensagem_pck.get_texto(684073) || ds_enter_w || --Descri��o da agenda
			'@nm_paciente = '||wheb_mensagem_pck.get_texto(684074) || ds_enter_w ||--Nome do paciente
			'@nm_responsavel = '||wheb_mensagem_pck.get_texto(684079)|| ds_enter_w ||--Nome do respons�vel
			'@nm_medico = '||wheb_mensagem_pck.get_texto(684075) || ds_enter_w || --Nome do m�dico
			'@ds_exame = '||wheb_mensagem_pck.get_texto(684076) || ds_enter_w ||--Descri��o do exame
			'@dt_hr_agendamento = '||wheb_mensagem_pck.get_texto(684077) || ds_enter_w ||--Data e hora do agendamento
			'@dt_agendamento = '||wheb_mensagem_pck.get_texto(684078) || ds_enter_w ||--Data do agendamento
			'@hr_agendamento = '||wheb_mensagem_pck.get_texto(687451) || ds_enter_w ||--Hora agendamento
			'@ds_lado = '||wheb_mensagem_pck.get_texto(687452)|| ds_enter_w ||-- Lado
			'@ds_convenio = '||wheb_mensagem_pck.get_texto(687453) || ds_enter_w || --Conv�nio
			'@cidade_estab = '||wheb_mensagem_pck.get_texto(791150) || ds_enter_w || --Cidade do estabelecimento
			'@prontuario = '||wheb_mensagem_pck.get_texto(791147); --Prontu�rio

return	ds_macro_w;

end obter_macros_consent_ageint;
/