create or replace
function consiste_processamento_conta(nr_interno_conta_p number)
		return varchar2 is

nr_interno_conta_w	conta_paciente.nr_interno_conta%type;
ie_status_acerto_w	conta_paciente.ie_status_acerto%type;

cd_estabelecimento_w	conta_paciente.cd_estabelecimento%type;
cd_convenio_w		conta_paciente.cd_convenio_parametro%type;
dt_periodo_inicial_w	conta_paciente.dt_periodo_inicial%type;
nr_seq_conta_des_w	conta_paciente.nr_interno_conta%type;
ie_status_destino_w	conta_paciente.ie_status_acerto%type;
nr_seq_regra_deducao_w	conv_regra_deducao.nr_sequencia%type;

ds_retorno_w		varchar2(1);

cursor c01 is
select	a.nr_interno_conta	
from	conta_log_processamento a,
		conta_paciente b
where	a.nr_interno_conta_base = nr_interno_conta_p
and		a.nr_interno_conta = b.nr_interno_conta
and		b.ie_cancelamento is null;

begin

ds_retorno_w	:= 'N';

select	max(cd_estabelecimento),
	max(cd_convenio_parametro),
	max(dt_periodo_inicial)
into	cd_estabelecimento_w,
	cd_convenio_w,
	dt_periodo_inicial_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

nr_seq_regra_deducao_w	:= nvl(obter_regra_deducao_conv(cd_estabelecimento_w, cd_convenio_w, null, dt_periodo_inicial_w),0);

if	(nr_seq_regra_deducao_w > 0) then

	select	max(nr_seq_conta_des)
	into	nr_seq_conta_des_w
	from	conta_pac_deducao_conv
	where	nr_seq_conta_orig = nr_interno_conta_p
	and	dt_processamento is not null;
	
	if	(nvl(nr_seq_conta_des_w,0) > 0) then	
		ds_retorno_w	:= 'P';
	
		select	ie_status_acerto
		into	ie_status_acerto_w
		from	conta_paciente
		where	nr_interno_conta = nr_interno_conta_p;
		
		select	max(ie_status_acerto)
		into	ie_status_destino_w
		from	conta_paciente
		where	nr_interno_conta = nr_seq_conta_des_w;
		
		if	((ie_status_acerto_w = 2) or (ie_status_destino_w = 2)) then
			ds_retorno_w	:= 'D';
		end if;		
	end if;

else

	open c01;
	loop
		fetch	c01
		into	nr_interno_conta_w;
		exit when c01%notfound;
		ds_retorno_w	:= 'P';
		
		select	ie_status_acerto
		into	ie_status_acerto_w
		from	conta_paciente
		where	nr_interno_conta = nr_interno_conta_w;
		
		if	(ie_status_acerto_w = 2) then
			ds_retorno_w	:= 'D';
		end if;
		
		exit when ds_retorno_w = 'D';
	end loop;
	close c01;
	
end if;

return  ds_retorno_w;
end consiste_processamento_conta;
/
