create or replace FUNCTION lab_obter_versao_lab_UsinaDap (ds_laboratorio_p		VARCHAR2)
					RETURN VARCHAR2 IS
ds_retorno_w	  VARCHAR2(10);
i		  INTEGER;
nr_posicao_w	  INTEGER;
ds_laboratorio_w  VARCHAR2(10);
BEGIN
ds_retorno_w	:= '';

IF 	(ds_laboratorio_p IS NOT NULL) THEN
	BEGIN
	nr_posicao_w := INSTR(ds_laboratorio_p,'^');
	ds_laboratorio_w := SUBSTR(ds_laboratorio_p,nr_posicao_w+1,LENGTH(ds_laboratorio_p));
	FOR	i IN INSTR(ds_laboratorio_w,'^') .. LENGTH(ds_laboratorio_w) LOOP
		BEGIN
			IF (NOT (SUBSTR(ds_laboratorio_p,i+1,1) = '^')) THEN
				ds_retorno_w	:= ds_retorno_w||SUBSTR(ds_laboratorio_p,i+1,1);
			ELSE
				EXIT;
			END IF;
		END;
	END LOOP;
	END;
END IF;

RETURN	ds_retorno_w;

END lab_obter_versao_lab_UsinaDap;
/