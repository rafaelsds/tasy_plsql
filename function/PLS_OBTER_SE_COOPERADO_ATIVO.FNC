create or replace
function pls_obter_se_cooperado_ativo(  cd_pessoa_fisica_p		varchar2,
					dt_referencia_p			varchar2,
					ds_param1			varchar2,
					nr_seq_conta_p			pls_conta.nr_sequencia%type default null)
 		    	return varchar2 is

ie_cooperado_w	  		varchar2(1) := 'N'; 
ie_situacao_w			varchar2(3);
nr_seq_situacao_w		number(10);
nr_seq_cooperado_w		number(10);
dt_inclusao_w			date;
dt_exclusao_w			date;
dt_referencia_w			date;
ie_ativo_data_w			varchar2(3)	:= 'N';

begin

begin
	dt_referencia_w	:= dt_referencia_p;
		
exception
when others then 
	wheb_mensagem_pck.exibir_mensagem_abort(296634, 'NR_SEQ_CONTA='||nr_seq_conta_p);
end;

if	(dt_referencia_w is null) then
	dt_referencia_w	:= sysdate;
end if;

/* Verificar primeiro se a pessoa f�sica est� cadastrada como cooperado */
select	max(nr_sequencia)
into	nr_seq_cooperado_w
from 	pls_cooperado	
where 	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	ie_status		= 'A';


if 	(nr_seq_cooperado_w is not null)	then
	/* Buscar a �ltima altera��o de situa��o */
	select	max(a.nr_sequencia)
	into	nr_seq_situacao_w
	from 	pls_cooperado_situacao	a
	where 	a.nr_seq_cooperado	= nr_seq_cooperado_w
	and 	a.dt_alteracao	<= dt_referencia_w;
	
	if	(nr_seq_situacao_w is not null) then
		select 	max(ie_situacao)
		into	ie_ativo_data_w
		from 	pls_cooperado_situacao
		where 	nr_sequencia = nr_seq_situacao_w;
	/* Se n�o houve nenhuma altera��o de situa��o, tem que confirmar se ent� entre inclus�o e exclus�o */
	else
		select  dt_inclusao,
			dt_exclusao,
			nvl(ie_status,'A')
		into	dt_inclusao_w,
			dt_exclusao_w,
			ie_situacao_w
		from 	pls_cooperado
		where 	nr_sequencia	= nr_seq_cooperado_w;
		
		if	(dt_referencia_w >= dt_inclusao_w) and
			(dt_referencia_w < nvl(dt_exclusao_w,dt_referencia_w+1)) then
			ie_ativo_data_w	:= 'A';
		end if;
	end if;
end if;

if	(ie_ativo_data_w = 'A') then
	ie_cooperado_w	:= 'S';
end if;

return	ie_cooperado_w;

end pls_obter_se_cooperado_ativo;
/