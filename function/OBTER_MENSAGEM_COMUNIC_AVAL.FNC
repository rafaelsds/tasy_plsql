create or replace function obter_mensagem_comunic_aval (	nr_seq_avaliacao_p	number,
					cd_declaracao_p	varchar2)
					return			varchar2 is

ds_retorno_w			varchar2(4000);
nm_pessoa_fisica_w		varchar2(60);
nm_medico_w				varchar2(60);
ds_senha_w				varchar2(20);

cd_pessoa_fisica_w		varchar2(10);
cd_medico_w				varchar2(10);
nr_seq_tipo_avaliacao_w	number(10,0);
ds_observacao_w			varchar2(255);
ds_tipo_avaliacao_w		varchar2(60);

begin
select	cd_pessoa_fisica,
		cd_medico,
		nr_seq_tipo_avaliacao,
		ds_observacao
into	cd_pessoa_fisica_w,
		cd_medico_w,
		nr_seq_tipo_avaliacao_w,
		ds_observacao_w
from	med_avaliacao_paciente
where	nr_sequencia		= nr_seq_avaliacao_p;

select	SUBSTR(OBTER_NOME_PF(CD_PESSOA_FISICA),0,60),
		ds_senha
into	nm_pessoa_fisica_w,
		ds_senha_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w;

select	SUBSTR(OBTER_NOME_PF(CD_PESSOA_FISICA),0,60)
into	nm_medico_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_medico_w;

select	ds_tipo
into	ds_tipo_avaliacao_w
from	med_tipo_avaliacao
Where	nr_sequencia = nr_seq_tipo_avaliacao_w;

select	ds_declaracao
into	ds_retorno_w
from	declaracao
where	upper(cd_declaracao) = upper(cd_declaracao_p);

ds_retorno_w	:= replace(ds_retorno_w,'@paciente',nm_pessoa_fisica_w);
ds_retorno_w	:= replace(ds_retorno_w,'@usuario',cd_pessoa_fisica_w);
ds_retorno_w	:= replace(ds_retorno_w,'@senha',ds_senha_w);
ds_retorno_w	:= replace(ds_retorno_w,'@avaliador',nm_medico_w);
ds_retorno_w	:= replace(ds_retorno_w,'@tipoavaliacao',ds_tipo_avaliacao_w);
ds_retorno_w	:= replace(ds_retorno_w,'@observacao',ds_observacao_w);

return ds_retorno_w;

end obter_mensagem_comunic_aval;
/