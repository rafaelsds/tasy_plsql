create or replace
function obter_local_acomodacao(
				nr_prescricao_p 	number)
				return 		varchar2	is

ds_retorno_w 	varchar2(100);

begin
select	max(substr(obter_desc_local_qt(NR_SEQ_LOCAL,'C'),1,60))
into	ds_retorno_w
from	paciente_atendimento
where	nr_prescricao	= nr_prescricao_p;

return  	ds_retorno_w;
end obter_local_acomodacao;
/