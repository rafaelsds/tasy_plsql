create or replace
function pls_obter_motivo_susp_seg
			(nr_seq_segurado_p	number)
			return varchar2 is
						
ds_retorno_w	varchar2(255)	:= null;

begin

if	(nr_seq_segurado_p is not null) then
	select	substr(obter_descricao_padrao_pk('PLS_MOTIVO_SUSP_SEG','DS_MOTIVO','NR_SEQUENCIA',nr_seq_motivo_susp),1,255)
	into	ds_retorno_w
	from	pls_segurado_suspensao a
	where	a.nr_seq_segurado	= nr_seq_segurado_p
	and	a.dt_fim_suspensao	is null;
end if;

return	ds_retorno_w;

end pls_obter_motivo_susp_seg;
/