create or replace 
function verifica_resposta_parecer_pend(	nm_usuario_p	varchar2, 
						nr_parecer_p 	number default null)
						return varchar2 is

ds_retorno		varchar2(1);
cd_pessoa_usuario_w 	varchar2(10);

begin

cd_pessoa_usuario_w := OBTER_PESSOA_FISICA_USUARIO(nm_usuario_p, 'C');	
				
select	nvl(max('S'),'N') 
into	ds_retorno 
from	PARECER_MEDICO_REQ a 
where 	(nr_parecer_p is not null and a.nr_parecer = nr_parecer_p )
or ( a.nr_parecer	not in (select b.nr_parecer 
		    from   PARECER_MEDICO b 
		    where  b.nr_parecer = a.nr_parecer	
		    and    b.dt_liberacao is not null
			and    b.dt_inativacao is null)
and	((a.CD_PESSOA_PARECER is not null 
      and a.CD_PESSOA_PARECER = cd_pessoa_usuario_w)
	or (a.nr_seq_equipe_dest is not null 
        and a.nr_seq_equipe_dest in (select c.nr_seq_equipe 
							 from 	pf_equipe_partic c 
							 where	c.nr_seq_equipe = a.nr_seq_equipe_dest 
							 and	c.cd_pessoa_fisica = cd_pessoa_usuario_w))))
and a.dt_liberacao is not null
and a.dt_inativacao is null;

return	ds_retorno;

end verifica_resposta_parecer_pend;
/
