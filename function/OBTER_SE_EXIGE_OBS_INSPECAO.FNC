create or replace
function obter_se_exige_obs_inspecao(	nr_sequencia_p		number)
return varchar2 is

ie_exige_w			varchar2(1);
			
begin

select	nvl(max(IE_OBSERVACAO),'N')
into	ie_exige_w
from	inspecao_nao_conf
where	nr_sequencia = nr_sequencia_p;

return	ie_exige_w;

end obter_se_exige_obs_inspecao;
/
