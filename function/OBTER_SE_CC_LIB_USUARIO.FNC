create or replace
function obter_se_cc_lib_usuario(	nm_usuario_p			varchar2,
				cd_centro_custo_p			number)
 		    	return varchar2 is

ie_liberado_w			varchar2(1) := 'N';			
qt_existe_w			number(10);
begin


select	count(*)
into	qt_existe_w
from	Usuario_Setor_v a,
	setor_atendimento b
where	a.cd_setor_atendimento	= b.cd_setor_atendimento
and	b.cd_centro_custo		= cd_centro_custo_p
and	a.nm_usuario		= nm_usuario_p;

if	(qt_existe_w > 0) then
	ie_liberado_w := 'S';
end if;

return	ie_liberado_w;

end obter_se_cc_lib_usuario;
/