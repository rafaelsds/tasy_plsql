create or replace
function san_obter_se_prod_lavado(	nr_seq_producao_p	number)
				return varchar is
				
ie_retorno_w			varchar2(1)	:= 'N';

begin


select	decode(count(*), 0, 'N', 'S')
into	ie_retorno_w
from	san_producao_lavagem
where	nr_seq_producao		= nr_seq_producao_p;

return	ie_retorno_w;

end san_obter_se_prod_lavado;
/