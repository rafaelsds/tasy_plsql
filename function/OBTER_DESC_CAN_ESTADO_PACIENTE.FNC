create or replace
FUNCTION OBTER_DESC_CAN_ESTADO_PACIENTE(NR_SEQUENCIA_P  NUMBER)
        RETURN VARCHAR2 IS

ds_estado_w VARCHAR2(255);

BEGIN
SELECT ds_estado
INTO ds_estado_w
FROM can_estado_paciente
WHERE nr_sequencia = nr_sequencia_p;


RETURN ds_estado_w;

END OBTER_DESC_CAN_ESTADO_PACIENTE;
/