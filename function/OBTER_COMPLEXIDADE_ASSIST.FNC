CREATE OR REPLACE
FUNCTION obter_complexidade_assist
		(nr_atendimento_p	number,
		cd_setor_atendimento_p	number,
		ie_opcao_p		varchar2,
		ie_campo_p		varchar2 default '1')
		return varchar2 is

ds_descricao_w			varchar2(255)	:= null;
ie_complexidade_assist_w		varchar2(10);
qt_pontuacao_w			number(10);
ds_escala_dini			varchar2(255);
nr_sequencia_w			number(10);
dt_avaliacao_w			date;
nr_seq_escala_man_w		number(10);
nr_seq_score_flex_w		number(10);
nr_seq_escala_news_w		number(10);
ie_parametro3_w  		varchar2(1);
begin

/* ATENCAO: AO ADICIONAR NOVA ESCALA DE COMPLEXIDADE ASSISTENCIAL, DEVE SER ALTERADA A PROCEDURE
CONSISTE_REGRA_SAE E ADICIONADA A ESCALA PARA CONSISTENCIA.	*/

/* Pega o tipo de complexidade assistencial do setor */

if	(nvl(ie_campo_p,'1')	= '1') then
select	nvl(max(ie_complexidade_assit), 'E'),
		max(nr_seq_score_flex_ca1)
into	ie_complexidade_assist_w,
		nr_seq_score_flex_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_p;
elsif	(nvl(ie_campo_p,'1')	= '2') then
select	max(IE_COMPLEXIDADE_ASSIT2),
		max(nr_seq_score_flex_ca2)
into	ie_complexidade_assist_w,
		nr_seq_score_flex_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_p;
elsif	(nvl(ie_campo_p,'1')	= '3') then
select	max(IE_COMPLEXIDADE_ASSIT3),
		max(nr_seq_score_flex_ca3)
into	ie_complexidade_assist_w,
		nr_seq_score_flex_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_p;
elsif	(nvl(ie_campo_p,'1')	= '4') then
select	max(IE_COMPLEXIDADE_ASSIT4),
		max(nr_seq_score_flex_ca4)
into	ie_complexidade_assist_w,
		nr_seq_score_flex_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_p;
end if;

if	(ie_complexidade_assist_w	is not null) then

if	(ie_complexidade_assist_w = 'E') then
select	obter_dados_gca(nr_atendimento_p, ie_opcao_p)
into	ds_descricao_w
from	dual;
elsif	(ie_complexidade_assist_w = 'NT') then

if ( ie_opcao_p = 'PC') then

	select	max(a.qt_pto_total),
			max(a.dt_avaliacao)
	into	qt_pontuacao_w,
			dt_avaliacao_w
	from	escala_ntiss a
	where	a.nr_sequencia = ( select 	max(b.nr_sequencia)
							   from 	escala_ntiss b
							   where  	b.nr_atendimento = nr_atendimento_p
							   and		b.dt_liberacao is not null
							   and		b.dt_inativacao is null);

else

	select	obter_pontuacao_ntiss(nr_atendimento_p)
	into	ds_descricao_w
	from	dual;

end if;



elsif	(ie_complexidade_assist_w = 'T') then

if ( ie_opcao_p = 'PC') then

	select	max(a.qt_pontuacao),
			max(a.dt_avaliacao),
			max(substr(obter_classif_tiss(a.qt_pontuacao),1,255))
	into	qt_pontuacao_w,
			dt_avaliacao_w,
			ds_descricao_w
	from	tiss_interv_terapeutica a
	where	a.nr_sequencia	=	(select	max(b.nr_sequencia)
								 from	tiss_interv_terapeutica b
								 where	b.nr_atendimento	= nr_atendimento_p
								 and	b.dt_liberacao is not null
								 and	b.dt_inativacao is null);

else

	select	obter_dados_tiss28(nr_atendimento_p, ie_opcao_p)
	into	ds_descricao_w
	from	dual;

end if;

elsif	(ie_complexidade_assist_w	= 'DT') then

if	(ie_opcao_p	in ('P','G','PC')) then

	select	max(qt_pontuacao),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	escala_downton a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	escala_downton x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);

end if;

qt_pontuacao_w := ds_descricao_w;

if	(ie_opcao_p	in ('G','PC')) and
	(ds_descricao_w is not null) then
	ds_descricao_w	:= substr(obter_descr_escala_downton(ds_descricao_w),1,100);
end if;
elsif	(ie_complexidade_assist_w	= 'ES') then

select	max(qt_score),
		max(dt_avaliacao)
into	qt_pontuacao_w,
		dt_avaliacao_w
from	escala_stratify a
where	nr_sequencia	= (	select	max(x.nr_sequencia)
				from	escala_stratify x
				where	x.nr_atendimento	= nr_atendimento_p
				and	x.dt_liberacao is not null
				and	x.dt_inativacao is null);
if	(ie_opcao_p	in ('P','G','PC')) then
	ds_descricao_w	:= qt_pontuacao_w;
end if;
if	(ie_opcao_p	= 'CT') and
	(qt_pontuacao_w	>= 9) then
	ds_descricao_w	:= obter_cor_cadastro(5036,obter_perfil_Ativo,wheb_usuario_pck.get_cd_estabelecimento,2);
end if;
if	(ie_opcao_p	= 'CD') and
	(qt_pontuacao_w	>= 9) then
	ds_descricao_w	:= obter_cor_cadastro(5036,obter_perfil_Ativo,wheb_usuario_pck.get_cd_estabelecimento,1);
end if;

if	(ie_opcao_p	in ('G','PC')) and
	(ds_descricao_w is not null)then
	ds_descricao_w	:= substr(obter_desc_escala_stratify(ds_descricao_w),1,100);
	end if;
elsif	(ie_complexidade_assist_w	= 'AP') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(qt_apache_ii),
			max(dt_apache)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	apache a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	apache x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);


end if;
elsif	(ie_complexidade_assist_w	= 'TR') then

if	(ie_opcao_p	in ('P','G','PC')) then

	select	max(qt_escore),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	atend_timi_risk a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	atend_timi_risk x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;
elsif	(ie_complexidade_assist_w	= 'GR') then

if	(ie_opcao_p	in ('P','G','PC')) then

	select	max(substr(obter_desc_grace_ii(y.nr_sequencia,'R6P'),1,255)),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	escala_grace_ii y
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
								from	escala_grace_ii x
								where	x.nr_atendimento	= nr_atendimento_p
								and		x.dt_liberacao is not null
								and		x.dt_inativacao is null);

end if;
elsif	(ie_complexidade_assist_w	= 'SN') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_SNAP_II),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_SNAPII_SNAPPEII a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
								from	ESCALA_SNAPII_SNAPPEII x
								where	x.nr_atendimento	= nr_atendimento_p
								and		x.dt_liberacao is not null
								and		x.dt_inativacao is null);
end if;
elsif	(ie_complexidade_assist_w	= 'SE') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_SNAPPE_II),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_SNAPII_SNAPPEII a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_SNAPII_SNAPPEII x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;
elsif	(ie_complexidade_assist_w	= 'SA') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_SAPS3 a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_SAPS3 x
					where	x.nr_atendimento	= nr_atendimento_p
					and	nvl(x.ie_tipo_SAPS3,'ADM') = '28D'
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;
elsif	(ie_complexidade_assist_w	= 'SP') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_SAPS3 a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_SAPS3 x
					where	x.nr_atendimento	= nr_atendimento_p
					and	nvl(x.ie_tipo_SAPS3,'ADM') = 'PIR'
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;
elsif	(ie_complexidade_assist_w	= 'B') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTO),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ATEND_ESCALA_BRADEN a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ATEND_ESCALA_BRADEN x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;

if	(ie_opcao_p	= 'G') and
	(ds_descricao_w is not null)then
	ds_descricao_w	:= substr(obter_resultado_braden(TO_NUMBER(ds_descricao_w)),1,100) || ' - '||to_char(DT_AVALIACAO_w,'dd/mm/yyyy hh24:mi:ss');

elsif (ie_opcao_p	= 'PC') and
	(ds_descricao_w is not null)then

	qt_pontuacao_w := ds_descricao_w;

	ds_descricao_w	:= substr(obter_resultado_braden(TO_NUMBER(ds_descricao_w)),1,100);

end if;
elsif	(ie_complexidade_assist_w	= 'BQ') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(qt_ponto),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ATEND_ESCALA_BRADEN_Q a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ATEND_ESCALA_BRADEN_Q x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;

qt_pontuacao_w := ds_descricao_w;

if	(ie_opcao_p	in ('G','PC')) and
	(ds_descricao_w is not null)then
	ds_descricao_w	:= substr(obter_resultado_braden_q(TO_NUMBER(ds_descricao_w)),1,100);
	end if;
elsif	(ie_complexidade_assist_w	= 'MT') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	escala_must a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	escala_must x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;

qt_pontuacao_w := ds_descricao_w;

if	(ie_opcao_p	in ('G', 'PC')) and
	(ds_descricao_w is not null)then
	ds_descricao_w	:= substr(Obter_result_must(ds_descricao_w),1,100);
	end if;

elsif	(ie_complexidade_assist_w	= 'MO') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(qt_pontuacao),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	escala_morse a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
								from	escala_morse x
								where	x.nr_atendimento	= nr_atendimento_p
								and		x.dt_liberacao is not null
								and		x.dt_inativacao is null);
end if;

if	( ie_opcao_p	= 'G') and
	( ds_descricao_w is not null) then
	ds_descricao_w	:= SubStr(Obter_desc_escala_morse(ds_descricao_w), 1, 20) || ' - '||to_char(dt_avaliacao_w,'dd/mm/yyyy hh24:mi:ss');

elsif ( ie_opcao_p	= 'PC') and
	  ( ds_descricao_w is not null) then

	qt_pontuacao_w := ds_descricao_w;

	ds_descricao_w	:= SubStr(Obter_desc_escala_morse(ds_descricao_w), 1, 20);

end if;
elsif	(ie_complexidade_assist_w	= 'SF') then

if	(ie_opcao_p	in ('P','G','PC')) then


	select	substr(max(obter_resultado_escala_eif(a.nr_sequencia,'T')),1,255),
			max(nr_sequencia),
			max(dt_avaliacao)
	into	ds_descricao_w,
			nr_sequencia_w,
			dt_avaliacao_w
	from	ESCALA_EIF a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
								from	ESCALA_EIF x
								where	x.nr_atendimento	= nr_atendimento_p
								and		(nr_seq_score_flex_w is null or x.nr_seq_escala = nr_seq_score_flex_w)
								and		x.dt_liberacao is not null
								and		x.dt_inativacao is null);
end if;

qt_pontuacao_w := ds_descricao_w;

if	(ie_opcao_p	in ('G','PC')) and
	(ds_descricao_w is not null) then
	ds_descricao_w	:= substr(obter_resultado_escala_eif(nr_sequencia_w,'D'),1,255);
	end if;
elsif	(ie_complexidade_assist_w	= 'SFII') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(nr_seq_escala),
			max(qt_pontos),
			max(dt_avaliacao)
	into	nr_sequencia_w,
			qt_pontuacao_w,
			dt_avaliacao_w
	from	ESCALA_EIF_II a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_EIF_II x
					where	x.nr_atendimento	= nr_atendimento_p
					and		(nr_seq_score_flex_w is null or x.nr_seq_escala = nr_seq_score_flex_w)
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;

if	(ie_opcao_p	in ('G','PC')) then
	ds_descricao_w	:= substr(obter_desc_resul_score_flex_2(qt_pontuacao_w,nr_sequencia_w),1,255);
end if;
elsif	(ie_complexidade_assist_w	= 'N') then

if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_NAS a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_NAS x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;
elsif 	(ie_complexidade_assist_w	= 'M') then

select	max(qt_pontuacao),
		max(dt_avaliacao)
into	qt_pontuacao_w,
		dt_avaliacao_w
from	escala_mews a
where	nr_sequencia	= (	select	max(x.nr_sequencia)
				from	escala_mews x
				where	x.nr_atendimento	= nr_atendimento_p
				and	x.dt_liberacao is not null
				and	x.dt_inativacao is null);
if	(ie_opcao_p	in ('P','G','PC')) then
	ds_descricao_w	:= qt_pontuacao_w;
elsif	(ie_opcao_p	= 'CT') and
	(qt_pontuacao_w	>=3) then
	ds_descricao_w	:= obter_cor_cadastro(2250,obter_perfil_Ativo,wheb_usuario_pck.get_cd_estabelecimento,2);
elsif	(ie_opcao_p	= 'CD') and
	(qt_pontuacao_w	>= 3) then
	ds_descricao_w	:= obter_cor_cadastro(2250,obter_perfil_Ativo,wheb_usuario_pck.get_cd_estabelecimento,1);
end if;
elsif	(ie_complexidade_assist_w	= 'S') then
if	(ie_opcao_p	in ('P','G','PC')) then
  select	max(QT_PONTUACAO)||'  ( '|| obter_desc_expressao(560996)||' '||PR_RISCO||'% )'
	into	ds_descricao_w
	from	ESCALA_SAPS3 a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_SAPS3 x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null)
	 group by pr_risco;
end if;
elsif	(ie_complexidade_assist_w	= 'P') then
if	(ie_opcao_p	in ('P','G','PC')) then
  select	max(qt_pto_total),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_PRISM a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_PRISM x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
			  and	x.dt_inativacao is null);
end if;
elsif	(ie_complexidade_assist_w	= 'ICC') then
if	(ie_opcao_p	in ('P','G','PC')) then
  select	max(QT_PONTUACAO) || ' (' || obter_desc_expressao(296055) || ' ' ||max(QT_PONTUACAO_IDADE) || ')',
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_CHARLSON a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_CHARLSON x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
				and	x.dt_inativacao is null)
		group by QT_PONTUACAO;
end if;
elsif	(ie_complexidade_assist_w	= 'CRB') then
if	(ie_opcao_p	in ('P','G','PC')) then
  select	max(QT_PONTUACAO) || ' (' || substr(obter_resultado(max(qt_pontuacao)),1,50) || ')',
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_CRB65 a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_CRB65 x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
				and	x.dt_inativacao is null)
		group by QT_PONTUACAO;
end if;
elsif	(ie_complexidade_assist_w	= 'SO') then
if	(ie_opcao_p	in ('P','G','PC')) then
  select	max(qt_pontuacao),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_SOFA a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_SOFA x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;
elsif	(ie_complexidade_assist_w	= 'RT') then
if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(IE_RISCO),
			max(DT_AVALIACAO)
	into	ds_descricao_w,
			DT_AVALIACAO_w
	from	ESCALA_TEV a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_TEV x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
	if	(ie_opcao_p	= 'G') and
		(ds_descricao_w is not null)then
		ds_descricao_w	:= substr(obter_valor_dominio(2900,ds_descricao_w),1,255) || ' - '||to_char(DT_AVALIACAO_w,'dd/mm/yyyy hh24:mi:ss');

	elsif(ie_opcao_p	= 'PC') and
		(ds_descricao_w is not null)then
		ds_descricao_w	:= substr(obter_valor_dominio(2900,ds_descricao_w),1,255);

	end if;
end if;
elsif 	(ie_complexidade_assist_w	= 'D') then
if	(ie_opcao_p in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			MAX(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_DINI a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_dini x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);

	qt_pontuacao_w := ds_descricao_w;

	if	(ie_opcao_p	in ('G','PC')) and
		(ds_descricao_w is not null)then
		ds_descricao_w	:= substr(Obter_result_dini('S',ds_descricao_w),1,100);
	end if;
elsif	(ie_opcao_p	= 'CD') then
	select	max(QT_PONTUACAO)
	into	ds_descricao_w
	from	ESCALA_DINI a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_dini x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
	if	(ds_descricao_w	is not null) then
		ds_descricao_w	:= substr(Obter_result_dini('CORF',ds_descricao_w),1,100);
	end if;
elsif	(ie_opcao_p	= 'CT') then
	ds_descricao_w	:= null;
Else
	select	obter_result_dini('S', max(QT_PONTUACAO))
	into	ds_descricao_w
	from	ESCALA_DINI a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_dini x
					where	x.nr_atendimento = nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
End if;

elsif	(ie_complexidade_assist_w	= 'JH') then
if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			MAX(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_FRAT a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_FRAT x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);

	qt_pontuacao_w := ds_descricao_w;

	if	(ie_opcao_p	in ('G','PC')) and
		(ds_descricao_w is not null)then
		ds_descricao_w	:= substr(obter_resultado_jh_frat(ds_descricao_w),1,100);
	end if;

end if;

elsif	(ie_complexidade_assist_w	= 'CI') then
if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(substr(obter_result_CAM_ICU(ie_alt_estado_mental,ie_comport_anormal,ie_focar_atencao,ie_distraido,null,null,'S',ie_pedra_flutuam,ie_peixe_mar,ie_quilograma,ie_martelo_madeira,ie_agitado,ie_letagico,ie_estuporoso,ie_camatoso),1,100)),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	escala_cam_icu
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_CAM_ICU x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);
end if;

elsif	(ie_complexidade_assist_w	= 'KZ') then
if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_KATZ a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_KATZ x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);

	qt_pontuacao_w := ds_descricao_w;

	if	(ie_opcao_p	in ('G','PC')) and
		(ds_descricao_w is not null)then
		ds_descricao_w	:= substr(obter_descr_escala_katz(ds_descricao_w),1,100);
	end if;

end if;

elsif	(ie_complexidade_assist_w	= 'DV') then
if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(qt_pontuacao),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	ESCALA_IDV a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	ESCALA_IDV x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);

	qt_pontuacao_w := ds_descricao_w;

	if	(ie_opcao_p	in ('G','PC')) and
		(ds_descricao_w is not null)then
		ds_descricao_w	:= substr(Obter_Result_IDV(ds_descricao_w),1,100);
	end if;

end if;

elsif	(ie_complexidade_assist_w	= 'PW') then
if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			max(dt_avaliacao)
	into	ds_descricao_w,
			dt_avaliacao_w
	from	escala_pews a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	escala_pews x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);

	qt_pontuacao_w := ds_descricao_w;

	if	(ie_opcao_p	in ('G','PC')) and
		(ds_descricao_w is not null)then
		ds_descricao_w	:= substr(Obter_Result_PEWS(ds_descricao_w),1,100);
	end if;

end if;
elsif	(ie_complexidade_assist_w	= 'MNAL') then
if	(ie_opcao_p	in ('P','G','PC')) then
	select	max(QT_PONTUACAO),
			max(nr_sequencia),
			max(dt_avaliacao)
	into	ds_descricao_w,
			nr_seq_escala_man_w,
			dt_avaliacao_w
	from	escala_man a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	escala_man x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);

	qt_pontuacao_w := ds_descricao_w;

	if	(ie_opcao_p	in ('G','PC')) and
		(ds_descricao_w is not null)then
		ds_descricao_w	:= substr(Obter_Result_Man(ds_descricao_w, nr_seq_escala_man_w),1,100);
	end if;

end if;

elsif	(ie_complexidade_assist_w	= 'NE') then
if	(ie_opcao_p	in ('P','G','PC', 'CD')) then
	select	max(QT_PONTUACAO),
		max(nr_sequencia),
		max(dt_avaliacao),
		max(obter_parametro_3_news(nr_sequencia))
	into	ds_descricao_w,
		nr_seq_escala_news_w,
		dt_avaliacao_w,
		ie_parametro3_w
	from	escala_news a
	where	nr_sequencia	= (	select	max(x.nr_sequencia)
					from	escala_news x
					where	x.nr_atendimento	= nr_atendimento_p
					and	x.dt_liberacao is not null
					and	x.dt_inativacao is null);

	qt_pontuacao_w := ds_descricao_w;

	if	(ie_opcao_p	= 'G') and
	(ds_descricao_w is not null) then
		ds_descricao_w	:= substr(Wheb_mensagem_pck.get_texto(1113184)|| ' ' || obter_nivel_clinico_news(ds_descricao_w, nr_seq_escala_news_w) || ' ('||ds_descricao_w||') - '|| to_char(dt_avaliacao_w,'dd/mm/yyyy hh24:mi'),1,255);

	elsif (ie_opcao_p	= 'PC') and
		(ds_descricao_w is not null) then

		qt_pontuacao_w := ds_descricao_w;

		ds_descricao_w	:= substr(obter_nivel_clinico_news(ds_descricao_w, nr_seq_escala_news_w),1,100);
	elsif (ie_opcao_p = 'CD') then
		if (ds_descricao_w >= 7) then
			ds_descricao_w :=  obter_cor_cadastro(8785, obter_perfil_Ativo,wheb_usuario_pck.get_cd_estabelecimento,1);

		elsif (ds_descricao_w in( 5, 6 ) or upper(ie_parametro3_w) = 'S') then
			ds_descricao_w :=  obter_cor_cadastro(8784, obter_perfil_Ativo,wheb_usuario_pck.get_cd_estabelecimento,1);

		elsif (ds_descricao_w in( 0, 1, 2, 3, 4 )) then
			ds_descricao_w :=  obter_cor_cadastro(8783, obter_perfil_Ativo,wheb_usuario_pck.get_cd_estabelecimento,1);
		end if;
	end if;

end if;

end if;

end if;

if  ( ie_opcao_p = 'PC') and
( ds_descricao_w is not null) and
( dt_avaliacao_w is not null)then


if ( qt_pontuacao_w is not null) then

	ds_descricao_w := substr(ds_descricao_w || ' (' || to_char(qt_pontuacao_w),1,255);

	if (qt_pontuacao_w = 1) then
		ds_descricao_w := substr(ds_descricao_w || ' ' || Wheb_mensagem_pck.get_texto(411506) || ')',1,255);
	else
		ds_descricao_w := substr(ds_descricao_w || ' ' || Wheb_mensagem_pck.get_texto(411518) || ')',1,255);
	end if;

end if;

ds_descricao_w	:= substr(ds_descricao_w || ' - '||to_char(dt_avaliacao_w,'dd/mm/yyyy hh24:mi'),1,255);


end if;

RETURN ds_descricao_w;

END obter_complexidade_assist;
/
