create or replace
function obter_ds_grupo_manutencao(nr_seq_grupo_p		number)
 		    	return varchar2 is

ds_retorno_w varchar2(255);
				
begin

if(nr_seq_grupo_p is not null) then
begin
	select	substr(ds_grupo,1,255)
	into 	ds_retorno_w
	from	hd_grupo_motivo_man
	where	nr_sequencia = nr_seq_grupo_p;
end;
end if;

return	ds_retorno_w;

end obter_ds_grupo_manutencao;
/