create or replace
function obter_modulo_funcao
			(	cd_funcao_p		Number)
 		    		return Number is
				
nr_seq_retorno_w		Number(10);

begin

select	nvl(max(nr_seq_modulo),0)
into	nr_seq_retorno_w
from	funcao
where	cd_funcao	= cd_funcao_p;

return	nr_seq_retorno_w;

end obter_modulo_funcao;
/