create or replace
function Obter_se_transfusao_iniciada(nr_seq_transfusao_p	number)
					return varchar2 is

ds_retorno_w	varchar2(1);
					
begin

if	(nvl(nr_seq_transfusao_p,0) > 0) then

	select 	decode(count(*),0,'N','S') 
	into	ds_retorno_w
	from	san_producao
	where	nr_seq_transfusao = nr_seq_transfusao_p
	and	dt_utilizacao is not null;
end if;

return	ds_retorno_w;

end Obter_se_transfusao_iniciada;
/