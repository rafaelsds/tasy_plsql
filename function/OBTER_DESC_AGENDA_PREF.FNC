create or replace 
Function Obter_Desc_agenda_pref(	nr_seq_preferencia_p	Number) 
				return varchar2 is

ds_retorno_w	Varchar2(80);

begin

if	(nr_seq_preferencia_p is not null) then
	select	ds_preferencia
	into	ds_retorno_w
	from	agenda_preferencia
	where	nr_sequencia	= nr_seq_preferencia_p;
end if;

return ds_retorno_w;

end Obter_Desc_agenda_pref;
/