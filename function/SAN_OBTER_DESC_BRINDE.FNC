create or replace
function san_obter_desc_brinde (nr_seq_brinde_p		number)
 		    	return varchar2 is

ds_brinde_w	varchar2(255);
begin
if	(nr_seq_brinde_p is not null) then
	select	substr(obter_desc_material(x.cd_material),1,255)
	into	ds_brinde_w
	from	san_regra_brinde x
	where	x.nr_sequencia =  nr_seq_brinde_p;

end if;

return	ds_brinde_w;

end san_obter_desc_brinde;
/