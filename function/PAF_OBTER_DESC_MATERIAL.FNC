CREATE OR REPLACE
FUNCTION paf_Obter_Desc_Material(
		cd_material_p	Number)
	RETURN VARCHAR IS

ds_retorno_w		Varchar2(255);

BEGIN

if	( cd_material_p is not null ) then
	select 	substr(ds_material,1,255)
	into	ds_retorno_w
	from 	material
	where 	cd_material = cd_material_p;
end if;

RETURN ds_retorno_w;

END paf_Obter_Desc_Material;
/
