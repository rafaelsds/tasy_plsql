create or replace
function OBTER_SE_FLUXO_LOTE_MES ( 
				dt_referencia_p		date,
				ie_classif_fluxo_p	varchar2,
				nr_seq_lote_fluxo_p	number,
				cd_estabelecimento_p number,
				cd_empresa_p 		number,
				ie_restringe_estab_p varchar2 )
 		    	return varchar2 is

ie_possui_mes_w		varchar2(1);
dt_inicial_w		date;
dt_final_w		date;

begin


dt_inicial_w	:= trunc(dt_referencia_p,'month');
dt_final_w	:= fim_mes(dt_referencia_p);

select	nvl(max('S'),'N')
into	ie_possui_mes_w
from	fluxo_caixa_lote
where	dt_mesano_referencia	between dt_inicial_w and dt_final_w
and	ie_classif_fluxo	= ie_classif_fluxo_p
and	nr_sequencia		<> nr_seq_lote_fluxo_p
and	cd_empresa	= cd_empresa_p
and	( (ie_restringe_estab_p = 'N') or
      (ie_restringe_estab_p = 'E' and cd_estabelecimento = cd_estabelecimento_p) or
      (ie_restringe_estab_p = 'F' and (cd_estabelecimento = cd_estabelecimento_p or obter_estab_financeiro(cd_estabelecimento_p) = cd_estabelecimento)) );

return	ie_possui_mes_w;

end OBTER_SE_FLUXO_LOTE_MES;
/
