create or replace
FUNCTION manchester_obter_ultimo_disc (nr_fluxo_P NUMBER) RETURN NUMBER IS

nr_seq_retorno		NUMBER(10);

BEGIN


SELECT	MAX(b.nr_seq_apresentacao) ie_ultimo_item
INTO	nr_seq_retorno
FROM  	manchester_fluxograma a,
		manchester_fluxograma_item b
WHERE	a.nr_sequencia = nr_fluxo_p
AND		a.nr_sequencia = b.nr_seq_fluxo;


RETURN nr_seq_retorno;

END manchester_obter_ultimo_disc;
/