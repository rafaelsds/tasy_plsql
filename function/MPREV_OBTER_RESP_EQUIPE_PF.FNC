create or replace
function mprev_obter_resp_equipe_pf(	cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type)
					return  varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter o respons�vel da equipe da pessoa fisica. 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_responsavel_w	pessoa_fisica.cd_pessoa_fisica%type;

begin

if	(cd_pessoa_fisica_p is not null) then
	/*Verifica se a pessoa � respons�vel por alguma equipe*/
	select	max(a.cd_pessoa_responsavel)
	into	cd_pessoa_responsavel_w
	from	mprev_equipe a
	where	a.cd_pessoa_responsavel = cd_pessoa_fisica_p;

	if	(cd_pessoa_responsavel_w is null) then
		/*Se a pessoa n�o for respons�vel por alguma equipe vai buscar o respons�vel pela equipe que ele pertence.*/
		select 	max(a.cd_pessoa_responsavel)
		into	cd_pessoa_responsavel_w
		from	mprev_equipe a,
			mprev_equipe_profissional b
		where	b.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	a.nr_sequencia = b.nr_seq_equipe
		and	b.dt_exclusao is null;	
	end if;
end if;

return	cd_pessoa_responsavel_w;

end mprev_obter_resp_equipe_pf;
/
