CREATE OR REPLACE FUNCTION psa_email_from_tasy_user_email ( p_ds_email   IN usuario.ds_email%TYPE ) RETURN VARCHAR2
  AS 

/*
 1- It removes multiplos email separeted by ; (semicolon),considering only the first one.
 2- Remove empty spaces and any character after the 60th character.
 */
BEGIN
  RETURN substr(
    trim(substr(
      p_ds_email,
      1,
      CASE(instr(
        p_ds_email,
        ';'
      ) )
        WHEN 0 THEN length(p_ds_email)
        ELSE instr(
          p_ds_email,
          ';'
        ) - 1
      END
    ) ),
    1,
    60
  );
END psa_email_from_tasy_user_email;
/