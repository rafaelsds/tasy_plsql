create or replace
function pls_obter_se_proc_atend
			(	cd_procedimento_p		number,
				ie_origem_proced_p		number,
				nr_seq_proc_tipo_atend_p	number)
				return varchar2 is
				
ds_retorno_w		varchar2(1);
cd_area_proc_w		Number(30);
cd_grupo_proc_w		Number(30);
cd_especialidade_w	Number(30);
ie_origem_proced_w	Number(30);
				
begin

-- jjung OS 491904 07/09/2012 : obter a estrutura do procedimento
pls_obter_estrut_proc(	cd_procedimento_p, ie_origem_proced_p, cd_area_proc_w,
				cd_especialidade_w, cd_grupo_proc_w, ie_origem_proced_w);
				
select	decode(count(1),0,'N','S')
into	ds_retorno_w
from	pls_oc_proc_atend_regra
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p
and	nr_seq_regra		= nr_seq_proc_tipo_atend_p;

/* jjung OS 491904 07/09/2012: Se n�o existir regra para o procedimento, verifica o grupo,  
Se n�o existir regra para o grupo, verifica a especialidade, Se n�o existir regra para a especialidade, verifica para a area */
if	(nvl(ds_retorno_w,'N') = 'N') then
	select	decode(nvl(count(1),0),0,'N','S')
	into	ds_retorno_w
	from	pls_oc_proc_atend_regra
	where	cd_grupo_proc		= cd_grupo_proc_w
	and	nr_seq_regra		= nr_seq_proc_tipo_atend_p;
end if;

if	(nvl(ds_retorno_w,'N') = 'N') then
	select	decode(nvl(count(1),0),0,'N','S')
	into	ds_retorno_w
	from	pls_oc_proc_atend_regra
	where	cd_especialidade	= cd_especialidade_w
	and	nr_seq_regra		= nr_seq_proc_tipo_atend_p;
end if;

if	(nvl(ds_retorno_w,'N') = 'N') then
	select	decode(nvl(count(1),0),0,'N','S')
	into	ds_retorno_w
	from	pls_oc_proc_atend_regra
	where	cd_area_procedimento	= cd_area_proc_w
	and	nr_seq_regra		= nr_seq_proc_tipo_atend_p;
end if;

return	ds_retorno_w;

end pls_obter_se_proc_atend;
/
