create or replace
function obter_convenio_ageturno_wlcb(	cd_convenio_p 	number,
					cd_agenda_p 	number,
					dt_encaixe_p 	varchar2,
					hr_encaixe_p 	varchar2)
return varchar2 is
			
qt_regra_conv_w 	number(10,0);	
ds_retorno_w		varchar2(1)	:= 'N';
ie_dia_semana_ww	number(1,0);
nr_seq_turno_w		number(10,0);

begin

select 	count(*)
into	qt_regra_conv_w
from	AGENDA_TURNO_CONV a
where	a.cd_agenda 	= cd_agenda_p
and	a.cd_convenio	= cd_convenio_p;

if 	(qt_regra_conv_w > 0) then --se convenio existir na agenda da tabela AGENDA_TURNO_CONV...
	select	obter_cod_dia_semana(dt_encaixe_p)
	into	ie_dia_semana_ww
	from	dual;
	
	select 	max(a.nr_seq_turno)
	into	nr_seq_turno_w
	from	agenda_turno_conv a,
		agenda_turno b
	where 	b.cd_agenda 	= a.cd_agenda
	and	b.nr_sequencia	= a.nr_seq_turno
	and	b.ie_dia_semana = ie_dia_semana_ww
	and	to_date('30/12/1899 '||hr_encaixe_p,'dd/mm/yyyy hh24:mi:ss') between b.hr_inicial and b.hr_final
	and	a.cd_agenda 	= cd_agenda_p
	and	a.cd_convenio	= cd_convenio_p
	and	(dt_encaixe_p >= b.dt_inicio_vigencia	or b.dt_inicio_vigencia is null) 
	and 	(dt_encaixe_p <= b.dt_final_vigencia	or b.dt_final_vigencia is null);
	
	if (nr_seq_turno_w is null) then --se vigencia is null entao pega dias de trabalho....
		select 	max(a.nr_seq_turno)
		into	nr_seq_turno_w
		from	agenda_turno_conv a,
			agenda_turno b
		where 	b.cd_agenda 	= a.cd_agenda
		and	b.nr_sequencia	= a.nr_seq_turno
		and 	b.ie_dia_semana = 9
		and	to_date('30/12/1899 '||hr_encaixe_p,'dd/mm/yyyy hh24:mi:ss') between b.hr_inicial and b.hr_final
		and	a.cd_agenda 	= cd_agenda_p
		and	a.cd_convenio	= cd_convenio_p;
	end if;

end if;	


/* Turnos especiais */

select 	count(*)
into	qt_regra_conv_w
from	agenda_turno_esp_conv a
where	a.cd_agenda 	= cd_agenda_p
and	a.cd_convenio	= cd_convenio_p;

if 	(qt_regra_conv_w > 0 ) then --se convenio existir na agenda da tabela AGENDA_TURNO_CONV...
	
	select 	max(a.nr_seq_turno_esp)
	into	nr_seq_turno_w
	from	agenda_turno_esp_conv a,
		agenda_turno_esp b
	where 	b.cd_agenda 	= a.cd_agenda
	and	b.nr_sequencia	= a.nr_seq_turno_esp
	and	a.cd_agenda 	= cd_agenda_p
	and	a.cd_convenio	= cd_convenio_p
	and	to_date('30/12/1899 '||hr_encaixe_p,'dd/mm/yyyy hh24:mi:ss') between b.hr_inicial and b.hr_final
	and	(dt_encaixe_p >= b.dt_agenda		or b.dt_agenda 	is null) 
	and 	(dt_encaixe_p <= b.dt_agenda_fim	or b.dt_agenda_fim is null);
	
end if;	


if 	nr_seq_turno_w is not null  then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end obter_convenio_ageturno_wlcb;
/
