create or replace
function obter_contato_emergencia(	cd_pessoa_fisica_p	varchar2,
					ie_opcao_p		varchar2)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(4000);
nm_contato_w	varchar2(255);
nr_telefone_w	varchar2(255);

begin

if	(upper(ie_opcao_p)	= 'NF') then --Nome + Telefone
	nm_contato_w	:= obter_compl_pf(cd_pessoa_fisica_p,20,'N');
	nr_telefone_w	:= obter_compl_pf(cd_pessoa_fisica_p,20,'T');
	ds_retorno_w	:= nm_contato_w;
	if	(nr_telefone_w is not null) then
	
		if	(ds_retorno_w	is not null) then
			ds_retorno_w := ds_retorno_w ||' - ';
		end if;
		
		ds_retorno_w	:= ds_retorno_w || nr_telefone_w;
	end if;
	
end if;


return	ds_retorno_w;

end obter_contato_emergencia;
/