create or replace
function mprev_obter_ciclo_atual_part(	nr_seq_prog_partic_p	number,
				dt_referencia_p		date	default	sysdate)
				return number is

nr_seq_partic_mod_w		mprev_prog_partic_modulo.nr_sequencia%type;
qt_retorno_w			number := 0;

begin

if (nr_seq_prog_partic_p is not null) then
begin
	select	nr_sequencia
	into	nr_seq_partic_mod_w
	from 	mprev_prog_partic_modulo
	where 	dt_referencia_p between dt_entrada and nvl(dt_saida,dt_referencia_p)
	and 	nr_seq_programa_partic = nr_seq_prog_partic_p;
exception
when others then
	nr_seq_partic_mod_w := 0;
end;		

	select	count(nr_sequencia)
	into	qt_retorno_w
	from 	mprev_partic_ciclo_atend
	where 	nr_seq_prog_partic_mod = nr_seq_partic_mod_w;

end if;

return	qt_retorno_w;

end mprev_obter_ciclo_atual_part;
/