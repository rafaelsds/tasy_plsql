Create or replace
Function Obter_Valor_Trans_Financ	(nr_sequencia_p		number,
					ie_opcao_p		varchar2)
					return number is

vl_transacao_w		number(15,2);

begin

if	(ie_opcao_p	= 'D') then
	select	nvl(a.vl_transacao,0)
	into	vl_transacao_w
	from	transacao_financeira b,
		movto_trans_financ a
	where	a.nr_seq_trans_financ	= b.nr_sequencia
	  and	a.nr_sequencia		= nr_sequencia_p
	  and	b.ie_caixa		in ('D', 'A', 'T');
end if;

if	(ie_opcao_p	= 'E') then
	select	nvl(a.vl_transacao,0)
	into	vl_transacao_w
	from	transacao_financeira b,
		movto_trans_financ a
	where	a.nr_seq_trans_financ	= b.nr_sequencia
	  and	a.nr_sequencia		= nr_sequencia_p
	  and	b.ie_saldo_caixa	= 'E'
	  and	b.ie_caixa		<> 'D';
end if;

if	(ie_opcao_p	= 'S') then
	select	nvl(a.vl_transacao,0)
	into	vl_transacao_w
	from	transacao_financeira b,
		movto_trans_financ a
	where	a.nr_seq_trans_financ	= b.nr_sequencia
	  and	a.nr_sequencia		= nr_sequencia_p
	  and	b.ie_saldo_caixa	= 'S'
	  and	b.ie_caixa		<> 'D';
end if;

return	vl_transacao_w;

end Obter_Valor_Trans_Financ;
/