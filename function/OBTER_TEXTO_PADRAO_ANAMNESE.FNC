create or replace
function	obter_texto_padrao_anamnese(	cd_profissional_p	varchar2,
					nr_atendimento_p	number)
				return number is

nr_seq_texto_w		number(10);
cd_especialidade_w	number(10);
ie_tipo_atendimento_w	number(10)	:= 0;
ie_clinica_w		number(10)	:= 0;
cd_setor_atendimento_w	number(10)	:= 0;
nm_usuario_w		varchar2(50);
cd_estabelecimento_w	number(10);

Cursor C01 is
	select	nr_seq_texto
	into	nr_seq_texto_w
	from	anamnese_texto_padrao
	where	nvl(cd_especialidade,cd_especialidade_w)	= cd_especialidade_w
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)	= ie_tipo_atendimento_w
	and	nvl(ie_clinica,ie_clinica_w)			= ie_clinica_w
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_w)= cd_setor_atendimento_w
	and	nvl(nm_usuario_config,nm_usuario_w)		= nm_usuario_w
	and	nvl(cd_estabelecimento,cd_estabelecimento_w)	= cd_estabelecimento_w
	order by nvl(nm_usuario_config,'AAAAAAA'),
		nvl(ie_tipo_atendimento, 0),
		 nvl(cd_setor_atendimento, 0),
		 nvl(ie_clinica,0),
		 nvl(cd_estabelecimento, 0);
begin
cd_especialidade_w	:= nvl(obter_especialidade_medico(cd_profissional_p,'C'),0);
nm_usuario_w		:= nvl(wheb_usuario_pck.get_nm_usuario,'AAAAAA');
cd_estabelecimento_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

if	(nvl(nr_atendimento_p,0)	> 0) then
	select	ie_tipo_atendimento,
		nvl(ie_clinica,0),
		cd_setor_atendimento
	into	ie_tipo_atendimento_w,
		ie_clinica_w,
		cd_setor_atendimento_w
	from	resumo_atendimento_paciente_v
	where	nr_atendimento	= nr_atendimento_p;
end if;

open C01;
loop
fetch C01 into	
	nr_seq_texto_w;
exit when C01%notfound;
end loop;
close C01;


return nr_seq_texto_w;
	
end obter_texto_padrao_anamnese;
/
