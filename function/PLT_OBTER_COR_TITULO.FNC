create or replace
function plt_obter_cor_titulo(	ie_tipo_item_p		varchar2,
					nr_seq_regra_p		number)
 		    	return varchar2 is

ds_cor_w			varchar2(20);
ie_tipo_item_w		varchar2(15);
			
begin

select	case	ie_tipo_item_p
	when 'DI'  then '1'
	when 'D'   then '2'
	when 'O'   then '3'
	when 'HM'  then '4'
	when 'J'   then '5'
	when 'MAT' then '6'
	when 'M'   then '7'
	when 'NAN' then '8'
	when 'NPN' then '9'
	when 'NPP' then '10'
	when 'P'   then '12'
	when 'R'   then '13'
	when 'SNE' then '14'
	when 'S'   then '15'
	when 'SOL' then '16' 
	when 'G'   then '18' 
	when 'C'   then '18' 
	when 'I'   then '19' 
	when 'L'   then '20' 
	when 'LD'  then '21' else ''
	end	
into	ie_tipo_item_w
from	dual;	

if	(ie_tipo_item_w is not null) then
	select	max(DS_COR_TITULO)
	into	ds_cor_w
	from	plt_regra_ordem_item
	where	ie_tipo_item = ie_tipo_item_w
	and	nr_seq_grupo = nr_seq_regra_p;
end if;

return	ds_cor_w;

end plt_obter_cor_titulo;
/