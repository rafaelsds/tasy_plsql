create or replace
function obter_se_houve_reacao_trans( 	cd_pessoa_fisica_p  number,
				nr_seq_producao_p  number)
      return varchar2 is

ie_resultado_w  varchar2(2) := 'N';
nr_seq_evento_w	number(10);

begin

if (cd_pessoa_fisica_p is not null) then

select  decode(count(*),0,'N','S')
into	ie_resultado_w
from    san_transfusao c,
		san_trans_reacao b,
		san_producao a
where  	a.nr_sequencia  = b.nr_seq_producao
and		c.nr_sequencia  = a.nr_seq_transfusao
and     b.nr_seq_producao  = nvl(nr_seq_producao_p, b.nr_seq_producao)
and     c.cd_pessoa_fisica = cd_pessoa_fisica_p;

if (ie_resultado_w = 'N') then	
	
	OBTER_PARAM_USUARIO(450,339,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,nr_seq_evento_w);
	
	select  decode(count(*),0,'N','S')
	into 	ie_resultado_w
	from  	QUA_EVENTO_PACIENTE a,
			atendimento_paciente b				 
	where 	a.nr_atendimento = b.nr_atendimento
	and		a.dt_liberacao is not null
	and	  	a.nr_seq_evento = nr_seq_evento_w
	and	  	b.cd_pessoa_fisica = cd_pessoa_fisica_p;	

end if;

end if;

return  ie_resultado_w;

end obter_se_houve_reacao_trans;
/
