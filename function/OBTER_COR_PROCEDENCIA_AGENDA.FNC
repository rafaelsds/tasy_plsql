create or replace
function obter_cor_procedencia_agenda(cd_procedencia_p	number,
				      ie_cor_p		varchar2)
 		    	return varchar2 is

ds_cor_w	varchar2(10);
ds_cor_fundo_w	varchar2(10);
ds_cor_fonte_w 	varchar2(10);			
			
begin

if	(cd_procedencia_p > 0) then

	select	max(ds_cor_fundo),
		max(ds_cor_fonte)
	into	ds_cor_fundo_w,
		ds_cor_fonte_w
	from	procedencia	
	where 	cd_procedencia	= cd_procedencia_p;
	
	if	(ie_cor_p = 'B') then
		ds_cor_w := ds_cor_fundo_w;
	elsif	(ie_cor_p = 'F') then
		ds_cor_w := ds_cor_fonte_w;
	end if;

end if;

return	ds_cor_w;

end obter_cor_procedencia_agenda;
/