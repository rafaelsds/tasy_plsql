create or replace
function pls_obter_cbo_saude_ptu(	nr_seq_cbo_saude_p		number,
					nr_versao_ptu_p			varchar2,
					ie_tipo_transacao_p		varchar2,
					cd_interface_p			number)
					return number is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	NO PARAMETRO "NR_VERSAO_PTU_P" INFORMAR APENAS:
		040	041	050	060
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

cd_cbo_ptu_w		number(10);
nr_versao_ptu_w		varchar2(10);

begin
nr_versao_ptu_w := nvl( nr_versao_ptu_p, ptu_obter_versao_dominio( ie_tipo_transacao_p, cd_interface_p));

if	(nr_seq_cbo_saude_p is not null) then
	if	(nr_versao_ptu_w is not null) then
		select	max(cd_cbo_ptu)
		into	cd_cbo_ptu_w
		from	cbo_saude_ptu
		where	nr_seq_cbo_saude	= nr_seq_cbo_saude_p
		and	nr_versao_ptu		= nr_versao_ptu_w;
	end if;
	
	if	(cd_cbo_ptu_w is null) then
		select	max(cd_cbo_ptu)
		into	cd_cbo_ptu_w
		from	cbo_saude
		where	nr_sequencia	= nr_seq_cbo_saude_p;
	end if;
end if;

return	cd_cbo_ptu_w;

end pls_obter_cbo_saude_ptu;
/