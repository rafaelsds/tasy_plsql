create or replace
function  OBTER_GLOSA_PARTICIPANTE
		(nr_seq_procedimento_p		number,
		nr_seq_material_p		number,
		cd_pessoa_fisica_p		varchar2,
		ie_opcao_p			varchar2) return number is

/*
ie_opcao_p

'G'	glosa
'A'	a maior

*/


vl_retorno_w	number(15,2)	:= 0;
vl_glosa_w	number(15,2);
vl_amaior_w	number(15,2);

begin

if	(nvl(nr_seq_procedimento_p,0) > 0) then
	select	nvl(sum(a.VL_GLOSA),0),
		nvl(sum(a.vl_amaior),0)
	into	vl_glosa_w,
		vl_amaior_w
	from	convenio_retorno_glosa a
	where	(a.cd_pessoa_fisica is null or a.cd_pessoa_fisica = cd_pessoa_fisica_p)
	and	NR_SEQ_PROPACI	= nr_seq_procedimento_p;
else
	select	nvl(sum(a.VL_GLOSA),0),
		nvl(sum(a.vl_amaior),0)
	into	vl_glosa_w,
		vl_amaior_w
	from	convenio_retorno_glosa a
	where	(a.cd_pessoa_fisica is null or a.cd_pessoa_fisica = cd_pessoa_fisica_p)
	and	NR_SEQ_MATPACI	= nr_seq_material_p;
end if;

if	(ie_opcao_p	= 'A') then
	vl_retorno_w	:= vl_amaior_w;
elsif	(ie_opcao_p	= 'G') then
	vl_retorno_w	:= vl_glosa_w;
end if;

return vl_retorno_w;

end OBTER_GLOSA_PARTICIPANTE;
/