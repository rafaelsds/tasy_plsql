create or replace
function obter_meta_bsc_sla (nr_seq_indicador_p		Number,
											dt_referencia_p				Date) 
	return Number is

qt_meta_w			Number(15,2);

begin

select	nvl(max(a.qt_meta),95)
into		qt_meta_w
from		bsc_ind_inf a 
where 	a.nr_seq_indicador = nr_seq_indicador_p 
and 		to_date(bsc_obter_periodo_inf(a.nr_seq_indicador, a.nr_sequencia, a.cd_ano, a.cd_periodo),'mm/yyyy') = trunc(dt_referencia_p,'month');

return qt_meta_w;

end obter_meta_bsc_sla;
/
