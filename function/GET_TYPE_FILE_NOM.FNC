create or replace
function get_type_file_nom(
			dt_referencia_p	date,
			ie_type_info_p	varchar2)
			return varchar2 is

ds_return_w varchar2(1) := 'N';
start_date_w date;
end_date_w date;
first_time_w varchar2(1) := 'N';

begin
if	(dt_referencia_p is not null) and
	(ie_type_info_p is not null) then
	begin
	start_date_w := pkg_date_utils.start_of(dt_referencia_p,'MONTH');
	end_date_w := pkg_date_utils.end_of(dt_referencia_p,'MONTH');
	
	select 	decode(count(*),0,'S','N')
	into	first_time_w
	from	nom_log_convenio_mx
	where 	dt_envio between start_date_w and end_date_w;
	
	if	(ie_type_info_p = 'F') then -- F = First time in this month
		begin
		if (first_time_w = 'S') then
			begin
			select  decode(count(*), 0,'N','S')
			into	ds_return_w
			from	atendimento_paciente ap,
					atend_categoria_convenio b
			where	ap.dt_entrada between start_date_w and end_date_w
			and		ap.nr_atendimento = b.nr_atendimento
			and 	b.cd_plano_convenio is not null;
			end;
		end if;
		end;		
	elsif (ie_type_info_p = 'N') and
		(first_time_w = 'N') then
		begin
		select	decode(count(*),0,'N','S')
		into	ds_return_w
		from	atendimento_paciente ap,
				atend_categoria_convenio b
		where	ap.dt_entrada between start_date_w and end_date_w
		and		ap.nr_atendimento = b.nr_atendimento
		and 	b.cd_plano_convenio is not null
		and 	not exists (
				select 	1
				from 	nom_log_convenio_mx x
				where	x.cd_pessoa_fisica = ap.cd_pessoa_fisica
				and		x.dt_envio between start_date_w and end_date_w);
		end;
	elsif (first_time_w = 'N') then
		begin
		select	decode(count(*),0,'N','S')
		into	ds_return_w
		from	atendimento_paciente ap,
				atend_categoria_convenio b,
				nom_log_convenio_mx x
		where	ap.dt_entrada between start_date_w and end_date_w
		and		ap.nr_atendimento = b.nr_atendimento
		and 	b.cd_plano_convenio is not null
		and		x.cd_pessoa_fisica = ap.cd_pessoa_fisica
		and		x.dt_envio between start_date_w and end_date_w
		and		(b.cd_convenio <> x.cd_convenio or b.dt_final_vigencia <> x.dt_fim_vigencia);
		end;
	end if;
	end;
end if;
return ds_return_w;
end get_type_file_nom;
/