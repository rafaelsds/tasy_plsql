create or replace
function obter_setor_profissional(cd_profissional_p		varchar2)
 		    	return number is
cd_setor_atendimento_w	number(5);
begin
cd_setor_atendimento_w := 0;

if	(cd_profissional_p is not null) then
	select	nvl(max(cd_setor_atendimento),0)
	into	cd_setor_atendimento_w
	from	usuario
	where	cd_pessoa_fisica = cd_profissional_p;

end if;
return	cd_setor_atendimento_w;

end obter_setor_profissional;
/