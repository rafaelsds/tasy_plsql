create or replace
function pls_obter_dados_prest_inter	(	nr_seq_prest_int_p 	pls_prestador_intercambio.nr_sequencia%type,
						ie_opcao_p		varchar2)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se a conta se encaixa nos filtros espec�ficos de conta para
as ocorr�ncias combinadas de conta m�dica.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ x]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ds_retorno_w		varchar2(4000);
nr_seq_cbo_saude_w	cbo_saude.nr_sequencia%type;
ds_municipio_w		sus_municipio.ds_municipio%type;
ds_unidade_federacao_w	sus_municipio.ds_unidade_federacao%type;

/*	ie_opcao_p
	'N'      - -Nome
	'CPF'  - -CPF
	'CGC' - -CGC
	'CNES -- CD_CNES
	'IBGE'--CD_MUNICIPIO_IBGE
	MC - Municipio completo
	CDPF - C�digo pessoa f�sica
*/	
begin

ds_retorno_w := '';

if	(ie_opcao_p = 'N') then
	select	max(nm_prestador)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif 	(ie_opcao_p = 'CPF') then
	select 	max(nr_cpf)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'CGC') then
	select 	max(cd_cgc_intercambio)
	into	ds_retorno_w
	from 	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'CNES') then
	select 	max(cd_cnes)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'IBGE') then
	select	max(cd_municipio_ibge)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'DTS') then
	select	max(dt_inicio_servico)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'DTC') then
	select	max(dt_contratualizacao)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'IRO') then
	select	max(ie_relacao_operadora)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'ITC') then
	select	max(ie_tipo_contratualizacao)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'CCL') then
	select	max(cd_classificacao)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'CAI') then
	select	max(cd_ans_int)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'DSV') then
	select	max(ie_disponibilidade_serv)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'IUE') then
	select	max(ie_urgencia_emergencia)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'MC') then

	select	max(a.ds_municipio),
		max(a.ds_unidade_federacao)
	into	ds_municipio_w,
		ds_unidade_federacao_w
	from	sus_municipio			a,
		pls_prestador_intercambio	b
	where	b.cd_municipio_ibge	= a.cd_municipio_ibge
	and	b.nr_sequencia		= nr_seq_prest_int_p;
	
	ds_retorno_w := ds_municipio_w;
	
	if	(ds_unidade_federacao_w is not null) then
	
		ds_retorno_w := ds_retorno_w || ' - ' || ds_unidade_federacao_w;
	end if;
	
elsif	(ie_opcao_p = 'CBO') then
	select	max(nr_seq_cbo_saude)
	into	nr_seq_cbo_saude_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;

	select	max(cd_cbo)
	into	ds_retorno_w
	from	cbo_saude
	where	nr_sequencia	= nr_seq_cbo_saude_w;
	
elsif	(ie_opcao_p = 'CBOS') then
	select	max(nr_seq_cbo_saude)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
	
elsif	(ie_opcao_p = 'CDPF') then
	select	max(cd_pessoa_fisica)
	into	ds_retorno_w
	from	pls_prestador_intercambio
	where	nr_sequencia = nr_seq_prest_int_p;
else	
	ds_retorno_w := null;
end if;
	
return	ds_retorno_w;

end pls_obter_dados_prest_inter;
/