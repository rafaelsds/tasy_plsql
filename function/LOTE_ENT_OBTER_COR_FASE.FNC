create or replace
function lote_ent_obter_cor_fase(
						nr_seq_status_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(10);				
				
begin

select	max	(ds_cor)
into	ds_retorno_w
from	lote_ent_cor_status_busca
where	nr_seq_status	= nr_seq_status_p;

return	ds_retorno_w;

end lote_ent_obter_cor_fase;
/
