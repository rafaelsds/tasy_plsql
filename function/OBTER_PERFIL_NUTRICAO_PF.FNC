create or replace
function Obter_perfil_nutricao_pf(	cd_pessoa_fisica_p	varchar2,
					ie_opcao_p		varchar2)
 		    	return varchar2 is

nr_seq_perfil_pf_w	number(10);
ds_retorno_w		varchar2(80);			
			
			
begin

select	nvl(max(nr_seq_nut_perfil), 0)
into	nr_seq_perfil_pf_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

if	(ie_opcao_p = 'C') then
	ds_retorno_w := nr_seq_perfil_pf_w;
else
	select	ds_perfil
	into	ds_retorno_w
	from	nut_perfil
	where	nr_sequencia = nr_seq_perfil_pf_w;
end if;


return	ds_retorno_w;

end Obter_perfil_nutricao_pf;
/