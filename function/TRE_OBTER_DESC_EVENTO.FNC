create or replace
function tre_obter_desc_evento(nr_seq_evento_p		number)
 		    	return varchar2 is

ds_evento_w	varchar2(80);			
			
begin
if	(nr_seq_evento_p is not null) then
	select	ds_evento
	into	ds_evento_w
	from	tre_evento
	where	nr_sequencia = nr_seq_evento_p;
end if;
return	ds_evento_w;

end tre_obter_desc_evento;
/