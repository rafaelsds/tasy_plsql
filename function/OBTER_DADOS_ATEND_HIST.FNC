create or replace 
function obter_dados_atend_hist	(nr_seq_unidade_p	number,
				ie_opcao_p		varchar2) 
			return date is

ds_retorno_w		date;

/*ie_opcao_p
DU - Data do ultimo atendimento
DN - Data da nova internação

*/

begin

if	(ie_opcao_p = 'DN') then
	begin
	
	select	dt_historico
	into	ds_retorno_w
	from	unidade_atend_hist
	where	nr_sequencia in (select	max(b.nr_sequencia)
				from    unidade_atend_hist b
				where   b.nr_seq_unidade = nr_seq_unidade_p
				and	b.dt_fim_historico is null
				and	b.ie_status_unidade = 'P');
	
	end;
elsif	(ie_opcao_p = 'DU') then
	begin
	
	select	dt_fim_historico
	into	ds_retorno_w
	from	unidade_atend_hist
	where	nr_sequencia in (select	max(b.nr_sequencia)
				from    unidade_atend_hist b
				where   b.nr_seq_unidade = nr_seq_unidade_p
				and	b.dt_fim_historico is not null
				and	b.ie_status_unidade = 'P');
	
	end;
end if;

return ds_retorno_w ;

end obter_dados_atend_hist;
/