create or replace
function pls_obter_preco_procedimento
				(cd_estabelecimento_p			number,
				nr_seq_prestador_p			number,
				dt_vigencia_p				date,
				cd_procedimento_p			number,
				ie_origem_proced_p			number,
				nr_seq_tipo_acomodacao_p		number,
				nr_seq_tipo_atendimento_p		number,
				nr_seq_clinica_p			number,
				nr_seq_categoria_p			number,
				nr_seq_plano_p				number,
				ie_opcao_p				varchar2)
				return number is

/*
	P - pre�o procedimento
*/

vl_procedimento_w		number(15,2);
vl_custo_operacional_w		number(15,2);
vl_anestesista_w		number(15,2);
vl_medico_w			number(15,2);
vl_auxiliares_w			number(15,2);
vl_filme_w			number(15,2);
vl_retorno_w			number(15,2);
cd_tabela_servico_w		number(4);
nr_seq_regra_w			number(10);
ie_classificacao_w		varchar2(1);
dt_vigencia_w			date;
cd_edicao_amb_w			number(6);
ie_valor_informado_w		Varchar2(1)	:= 'N';
cd_porte_anestesico_w		Varchar2(10);
ds_retorno_w			Varchar2(255);
nr_aux_regra_w			number(10);
ie_tipo_vinculo_w		Varchar2(10);
nr_seq_classificacao_w		Number(10);
nr_seq_regra_autogerado_w	Number(10);
dt_procedimento_w		date	:= null;
cd_moeda_autogerado_w		Number(3);
vl_ch_honorarios_w		cotacao_moeda.vl_cotacao%type;
vl_ch_custo_oper_w		cotacao_moeda.vl_cotacao%type;
vl_ch_custo_filme_w		cotacao_moeda.vl_cotacao%type;
vl_ch_anestesista_w		cotacao_moeda.vl_cotacao%type;
dados_regra_preco_proc_w	pls_cta_valorizacao_pck.dados_regra_preco_proc;
dados_regra_preco_servico_w	pls_cta_valorizacao_pck.dados_regra_preco_servico;
begin

select	max(ie_classificacao)
into	ie_classificacao_w
from	procedimento
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

if	(ie_classificacao_w = '1') then

	begin

	pls_define_preco_proc(cd_estabelecimento_p, nr_seq_prestador_p, nr_seq_categoria_p,
			dt_vigencia_p, null, cd_procedimento_p,
			ie_origem_proced_p, nr_seq_tipo_acomodacao_p, nr_seq_tipo_atendimento_p,
			nr_seq_clinica_p, nr_seq_plano_p, 'P',
			0, 0, null,
			'N', null, '',
			'','','',
			'', '', null,
			'', '', '', 
			'','A', 'X',
			null, null, dt_procedimento_w,
			null, null,null,
			null, null, dados_regra_preco_proc_w);
			
	vl_procedimento_w		:= dados_regra_preco_proc_w.vl_procedimento;
	vl_custo_operacional_w		:= dados_regra_preco_proc_w.vl_custo_operacional;
	vl_anestesista_w		:= dados_regra_preco_proc_w.vl_anestesista;
	vl_medico_w			:= dados_regra_preco_proc_w.vl_medico;
	vl_filme_w			:= dados_regra_preco_proc_w.vl_filme; 
	vl_auxiliares_w			:= dados_regra_preco_proc_w.vl_auxiliares;
	nr_seq_regra_w			:= dados_regra_preco_proc_w.nr_sequencia;
	cd_edicao_amb_w			:= dados_regra_preco_proc_w.cd_edicao_amb; 
	cd_porte_anestesico_w		:= dados_regra_preco_proc_w.cd_porte_anestesico;
	nr_aux_regra_w			:= dados_regra_preco_proc_w.nr_auxiliares;
	nr_seq_regra_autogerado_w	:= dados_regra_preco_proc_w.nr_seq_regra_autogerado;
	cd_moeda_autogerado_w		:= dados_regra_preco_proc_w.cd_moeda_autogerado; 
	vl_ch_honorarios_w		:= dados_regra_preco_proc_w.vl_ch_honorarios; 
	vl_ch_custo_oper_w		:= dados_regra_preco_proc_w.vl_ch_custo_oper; 
	vl_ch_custo_filme_w		:= dados_regra_preco_proc_w.vl_ch_custo_filme; 
	vl_ch_anestesista_w		:= dados_regra_preco_proc_w.vl_ch_anestesista;
				
	end;
else
	begin
	
	begin
	select	ie_tipo_vinculo,
		nr_seq_classificacao
	into	ie_tipo_vinculo_w,
		nr_seq_classificacao_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;
	exception
	when others then
		ie_tipo_vinculo_w	:= null;
		nr_seq_classificacao_w	:= null;
	end;
	
	pls_define_preco_servico(cd_estabelecimento_p, nr_seq_prestador_p, dt_vigencia_p,
			cd_procedimento_p, ie_origem_proced_p, 'P',
			null, 'N', null,
			nr_seq_plano_p,
			'', '', '',
			0, nr_seq_classificacao_w, nr_seq_categoria_p,
			'N', ie_tipo_vinculo_w, '',
			'', null, null, 
			null, null, nr_seq_clinica_p, 
			nr_seq_tipo_atendimento_p, nr_seq_tipo_acomodacao_p,null, 
			null, dados_regra_preco_servico_w);
	vl_procedimento_w	:= 	dados_regra_preco_servico_w.vl_servico;	
	end;
end if;

if	(ie_opcao_p	= 'P') then
	vl_retorno_w	:= vl_procedimento_w;
end if;

return vl_retorno_w;

end pls_obter_preco_procedimento;
/