create or replace FUNCTION GET_VOLUME_PARCIAL_INTERFACE(nr_seq_mat_cpoe_p 		NUMBER default 0,
                                                        nr_seq_agent_p 		NUMBER default 0)
 		    	RETURN NUMBER IS
                
qt_volume_w	                 bomba_infusao_interface.qt_volume%type;
qt_volume_inicial_w	         bomba_infusao_interface.qt_volume_inicial%type;
qt_bolus_w	                 bomba_infusao_interface.qt_bolus%type;
qt_ult_volume_w	             bomba_infusao_interface.qt_ult_volume%type;
soma_total_volume_w	         bomba_infusao_interface.qt_volume%type default 0;

cursor c01 is
select 
    nvl(bii.qt_volume, 0) qt_volume,
    nvl(bii.qt_volume_inicial, 0) qt_volume_inicial,
    nvl(bii.qt_bolus, 0) qt_bolus,
    nvl(bii.qt_ult_volume, 0) qt_ult_volume
from 
    bomba_infusao bi, bomba_infusao_interface bii
where 
    bi.nr_seq_bomba_interface = bii.nr_sequencia
and (nr_seq_mat_cpoe = nr_seq_mat_cpoe_p OR bi.nr_seq_agente = nr_seq_agent_p)
and ie_ativo = 'S';

BEGIN

open C01;
loop
fetch C01 into
	qt_volume_w,
    qt_volume_inicial_w,
    qt_bolus_w,
    qt_ult_volume_w;
exit when C01%notfound;
	begin
        soma_total_volume_w := soma_total_volume_w + 
        (qt_volume_w - qt_volume_inicial_w + qt_bolus_w + qt_ult_volume_w);
        if (soma_total_volume_w < 0) then
        soma_total_volume_w := 0;
        end if;
	end;
end loop;
close C01;

RETURN	soma_total_volume_w;

END GET_VOLUME_PARCIAL_INTERFACE;
/
