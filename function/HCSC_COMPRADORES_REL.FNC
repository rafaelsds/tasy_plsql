create or replace
function hcsc_compradores_rel
 		    	return varchar2 is

cd_compradores_w			varchar2(255);			

begin

select	nvl(cd_compradores,'X')
into	cd_compradores_w
from	hscs_dados_rel_controle;

return	cd_compradores_w;

end hcsc_compradores_rel;
/