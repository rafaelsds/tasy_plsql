create or replace
function obter_hist_alter_pac_atend(nr_prescricao_p		number)
 		    	return number is

nr_retorno_w			number(10);
nr_seq_atendimento_w	number(10);
ie_possui_hist_w		varchar2(1);				
				
begin
nr_retorno_w	:=	0;

if (nr_prescricao_p is not null) then

	select 	nvl(max(nr_seq_atendimento),0)
	into	nr_seq_atendimento_w
	from	paciente_atendimento
	where	nr_prescricao	=	nr_prescricao_p;
	
	if (nr_seq_atendimento_w > 0) then
	
		select  nvl(max('S'),'N')
		into	ie_possui_hist_w
		from	paciente_prot_medic_hist
		where	nr_seq_atendimento = nr_seq_atendimento_w;
		
		if (ie_possui_hist_w = 'S') then
		
			nr_retorno_w 	:= 	nr_seq_atendimento_w;
		
		end if;
	
	end if;
	
end if;


return	nr_retorno_w;

end obter_hist_alter_pac_atend;
/