create or replace
function hspb_obter_benef_permanencia
				(cd_estabelecimento_p	number,
				 nr_seq_plano_p		number,
				 ie_tipo_contrato_p	varchar2,
				 dt_inicio_p		date,
				 dt_fim_p		date,
				 ie_opcao_p		varchar2)
				return number is
				
/*
ie_opcao_p
	'A' = Perman�ncia de at� 3 meses
	'B' = Perman�ncia de 4 a 6 meses
	'C' = Perman�ncia de 7 a 9 meses
	'D = Perman�ncia de 10 a 12 meses
	'E' = Perman�ncia acima de 12 meses
	'T' = Total
*/

ds_retorno_w	number(10);

begin

if	(ie_opcao_p is not null) then

	select	count(*)
	into	ds_retorno_w
	from	pls_motivo_cancelamento d, 
		pessoa_fisica		c,
		pls_segurado		b,
		pls_contrato		a
	where	a.nr_sequencia = b.nr_seq_contrato
	and	b.cd_pessoa_fisica = c.cd_pessoa_fisica
	and	b.nr_seq_motivo_cancelamento = d.nr_sequencia
	and	(b.nr_seq_plano = nr_seq_plano_p or nvl(nr_seq_plano_p,0) = 0)
	and	((ie_tipo_contrato_p = 'PF' and a.cd_pf_estipulante is not null) or 
		 (ie_tipo_contrato_p = 'PJ' and a.cd_cgc_estipulante is not null) or 
		 (ie_tipo_contrato_p = 'A'))
	and	d.cd_estabelecimento = cd_estabelecimento_p
	and	b.dt_contratacao between decode(to_date(dt_inicio_p,'dd/mm/yy'), null, b.dt_contratacao, to_date(dt_inicio_p,'dd/mm/yy')) and decode(to_date(dt_fim_p,'dd/mm/yy'), null, b.dt_contratacao, to_date(dt_fim_p,'dd/mm/yy'))
	and	((ie_opcao_p = 'A' and nvl(months_between(trunc(b.dt_cancelamento,'month'), trunc(b.dt_contratacao,'month')),0) <= 3) or
		 (ie_opcao_p = 'B' and nvl(months_between(trunc(b.dt_cancelamento,'month'), trunc(b.dt_contratacao,'month')),0) >= 4 and nvl(months_between(trunc(b.dt_cancelamento,'month'), trunc(b.dt_contratacao,'month')),0) <= 6) or
		 (ie_opcao_p = 'C' and nvl(months_between(trunc(b.dt_cancelamento,'month'), trunc(b.dt_contratacao,'month')),0) >= 7 and nvl(months_between(trunc(b.dt_cancelamento,'month'), trunc(b.dt_contratacao,'month')),0) <= 9) or
		 (ie_opcao_p = 'D' and nvl(months_between(trunc(b.dt_cancelamento,'month'), trunc(b.dt_contratacao,'month')),0) >= 10 and nvl(months_between(trunc(b.dt_cancelamento,'month'), trunc(b.dt_contratacao,'month')),0) <= 12) or
		 (ie_opcao_p = 'E' and nvl(months_between(trunc(b.dt_cancelamento,'month'), trunc(b.dt_contratacao,'month')),0) >= 13) or
		 (ie_opcao_p = 'T'));
		 
end if;

return	ds_retorno_w;

end hspb_obter_benef_permanencia;
/