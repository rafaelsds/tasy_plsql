create or replace
function pls_obter_regra_intercambio
			(	nr_seq_conta_p		number,
				ie_cobranca_pagamento_p	varchar2)
			return number is 

qt_dias_envio_conta_w		number(10);
nr_seq_intercambio_w		number(10)	:= null;
cd_estabelecimento_w		number(4);
ie_envia_w			varchar2(1)	:= 'S';
qt_atraso_w			number(10);
qt_prestador_a400_w		number(5);
ie_consiste_prest_w		varchar2(1)	:= 'N';
nr_seq_plano_w			number(10);
ie_seguro_obito_w		varchar2(1);
ie_pcmso_w			varchar2(10);
nr_sequencia_w			number(10)	:= null;
nr_seq_fatura_w			number(10);
nr_seq_segurado_w		number(10);
dt_conta_w			date;
nr_seq_congenere_w		number(10);
cd_unimed_origem_w		varchar2(20);
nr_seq_grupo_coop_w		number(15);
dt_recebimento_fatura_w		date;
dt_postagem_w			date;
dt_atendimento_w		date;

Cursor c01 is
	select	a.nr_sequencia
	from	pls_regra_intercambio	a
	where	dt_postagem_w	between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_conta_w)
	and	((pls_obter_tipo_intercambio(nr_seq_congenere_w,cd_estabelecimento_w) = a.ie_tipo_intercambio) or (a.ie_tipo_intercambio = 'A'))
	and	nvl(a.nr_seq_congenere_sup,nvl(a.nr_seq_congenere,nvl(nr_seq_congenere_w,0)))= nvl(nr_seq_congenere_w,0)
	and	nvl(ie_cobranca_pagamento,nvl(ie_cobranca_pagamento_p,'P')) = nvl(ie_cobranca_pagamento_p,'P')
	and	(a.nr_seq_intercambio = nr_seq_intercambio_w or a.nr_seq_intercambio is null)
	and	(a.nr_seq_grupo_coop_seg = nr_seq_grupo_coop_w or a.nr_seq_grupo_coop_seg is null)
	and	(a.nr_seq_plano is null or a.nr_seq_plano = nr_seq_plano_w)	
	and	(a.nr_seq_grupo_congenere is null or
		exists (select	1
			from	pls_cooperativa_grupo	x
			where	x.nr_seq_grupo 		= a.nr_seq_grupo_congenere
			and	x.nr_seq_congenere 	= nr_seq_congenere_w))
	and	((ie_seguro_obito_w = 'B' and nvl(a.ie_beneficio_obito,'N') = 'S') or ie_seguro_obito_w = 'N')
	and	((ie_pcmso	= ie_pcmso_w) or (ie_pcmso = 'N'))
	and 	((a.ie_tipo_data is null) or 
		((a.ie_tipo_data = 'R') and ((dt_recebimento_fatura_w - dt_atendimento_w) <= a.qt_dias_envio_taxa)) or
		((a.ie_tipo_data = 'P')	and ((dt_postagem_w - dt_atendimento_w) <= a.qt_dias_envio_taxa)));

begin

if	(nr_seq_conta_p is not null) then
	select	a.dt_atendimento_referencia,
		a.nr_seq_segurado,
		a.nr_seq_fatura,
		nvl(dt_alta,dt_atendimento)
	into	dt_conta_w,
		nr_seq_segurado_w,
		nr_seq_fatura_w,
		dt_atendimento_w
	from	pls_conta a
	where	a.nr_sequencia	= nr_seq_conta_p;
	
	if	(nr_seq_fatura_w is not null) then
		select	a.cd_unimed_origem,
			a.dt_recebimento_fatura,
			a.dt_postagem
		into	cd_unimed_origem_w,
			dt_recebimento_fatura_w,
			dt_postagem_w
		from	ptu_fatura a
		where	a.nr_sequencia	= nr_seq_fatura_w;
		
		if	(cd_unimed_origem_w is not null) then
			select	max(a.nr_sequencia)
			into	nr_seq_congenere_w
			from	pls_congenere a
			where	a.cd_cooperativa	= cd_unimed_origem_w;
			
			if	(nr_seq_congenere_w is null) then
				select	max(a.nr_sequencia)
				into	nr_seq_congenere_w
				from	pls_congenere a
				where	somente_numero(a.cd_cooperativa)	= somente_numero(cd_unimed_origem_w);
			end if;
		end if;
	end if;

	/* Quantidade de dias envio*/
	begin
	select	max(nr_seq_intercambio),
		max(cd_estabelecimento),
		max(nr_seq_plano),
		max(ie_pcmso),
		max(nr_seq_grupo_coop)
	into	nr_seq_intercambio_w,
		cd_estabelecimento_w,
		nr_seq_plano_w,
		ie_pcmso_w,
		nr_seq_grupo_coop_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	exception
	when others then
		nr_seq_intercambio_w	:= null;
	end;

	if	(ie_pcmso_w is null) then
		ie_pcmso_w	:= 'N';
	end if;

	begin
	select	nvl(ie_seguro_obito,'N')
	into	ie_seguro_obito_w
	from	pls_plano
	where	nr_sequencia = nr_seq_plano_w;
	exception
	when others then
		ie_seguro_obito_w := 'N';
	end;
	
	nr_sequencia_w	:= null;
	open c01;
	loop
	fetch c01 into	
		nr_sequencia_w;
	exit when c01%notfound;
		begin
		null;
		end;
	end loop;
	close c01;
end if;

return nr_sequencia_w;

end pls_obter_regra_intercambio;
/
