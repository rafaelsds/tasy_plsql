create or replace
function obter_desc_itens_processo_adep(	nr_seq_processo_p	number,
											ie_opcao_p			varchar2)
 		    	return varchar2 is

ds_itens_w		varchar2(4000) := '';
ds_item_w		varchar2(255);
				
Cursor C01 is
	select	distinct (substr(obter_dados_material(cd_material, 'DR'),1,30) || '| ')
	from	prescr_mat_hor
	where	nr_seq_processo = nr_seq_processo_p
	order by 1;
	
begin

if	(nr_seq_processo_p is not null) then
	if	(ie_opcao_p = 'MDR') then
		open C01;
		loop
		fetch C01 into	
			ds_item_w;
		exit when C01%notfound;
			begin
			ds_itens_w	:= substr(ds_itens_w || ds_item_w,1,4000);
			end;
		end loop;
		close C01;
	end if;
end if;


return	ds_itens_w;

end obter_desc_itens_processo_adep;
/