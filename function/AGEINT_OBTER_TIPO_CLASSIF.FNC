create or replace
function ageint_obter_tipo_classif(nr_seq_tipo_classif_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(90);			
			
begin

select	max(ds_classif_paciente)
into	ds_retorno_w
from	tipo_classificao_paciente
where	nr_sequencia = nr_seq_tipo_classif_p;

return	ds_retorno_w;

end ageint_obter_tipo_classif;
/