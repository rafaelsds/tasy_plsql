create or replace
function Obter_informacao_os_pessoa (	nm_usuario_p		varchar2,
					ie_opcao_p		varchar2,
					dt_inicial_p		date,
					dt_final_p		date)
 		    			return number is

/*

T	- total de OS com registro de atividades realizadas do usu�rio, no per�odo
CA 	- Total de OS com registro de atividades realizadas do usu�rio, classificadas com complexidade Alta, Muito alta ou Alto risco.
CAE	- Total de OS encerrada pelo usu�rio, classificadas com complexidade Alta, Muito alta ou Alto risco.
CAEU - Total de OS encerrada, classificadas com complexidade Alta, Muito alta ou Alto risco. que o usu�rio teve atividade realizada e esta como executor.
ECPE	- Total de OS's encerradas com atividade realizada pelo usu�rio e com o mesmo como executor da OS.
HRAD	- Horas adicionais registradas pelo usu�rio
EANA	- Total de OS Encerradas no per�odo e que tiveram atividade registrada por um analista., e que ele esteja como executor previsto da OS.
ENC		- Total de OS Encerrada pelo usu�rio no per�odo
DEF		- Total de Defeitos documentados e liberados em nome do usu�rio no m�s
DEFAP	- Total de Defeitos documentados e liberados em nome de qualquer usu�rio do grupo do analista, dividido pela quantidade de pessoas do grupo no m�s.
*/

qt_os_w				number(10);
dt_inicial_w			date;
dt_final_w			date;
dt_entrada_w			date;
dt_saida_w			date;
qt_min_extra_w			number(15,4);
qt_min_realizados_w		number(10);
qt_min_intervalo_w		number(10);
qt_pessoas_grupo_w		number(10);
nr_seq_grupo_w			number(10);
qt_tota_pf_grupos_w		number(10);
qt_total_os_grupos_w		number(10);
ds_dia_semana_w			varchar2(10);

cursor c01 is
select	dt_entrada,
	nvl(dt_saida, fim_dia(dt_entrada)),
	qt_min_intervalo
from	usuario_controle
where	nm_usuario	= nm_usuario_p
 and	dt_entrada  between dt_inicial_w and fim_dia(dt_final_w);

cursor c02 is
select	nr_seq_grupo
from	usuario_grupo_des
where	nm_usuario_grupo = nm_usuario_p;
 
begin

dt_inicial_w	:= trunc(dt_inicial_p,'month');
dt_final_w	:= last_day(dt_final_p) + 86399/86400;

if	(ie_opcao_p = 'T') then

	select	count(distinct b.nr_seq_ordem_serv)
	into	qt_os_w
	from	man_ordem_serv_ativ b
	where	dt_atividade between dt_inicial_w AND dt_final_w
	and	nm_usuario_exec	= nm_usuario_p;

elsif	(ie_opcao_p = 'CA') then
	select	count(distinct b.nr_seq_ordem_serv)
	into	qt_os_w
	from	man_ordem_servico a,
		man_ordem_serv_ativ b
	where	a.nr_sequencia	= b.nr_seq_ordem_serv
	and	a.nr_seq_complex in(4,5,6)
	and	b.dt_atividade between dt_inicial_w AND dt_final_w
	and	b.nm_usuario_exec = nm_usuario_p;
	
elsif	(ie_opcao_p = 'CAE') then
	select	count(distinct x.nr_seq_ordem_serv)
	into	qt_os_w
	from	w_avaliacao_usuario x,
		man_ordem_servico a
	where	a.nr_sequencia	= x.NR_SEQ_ORDEM_SERV
	and	a.nr_seq_complex in(4,5,6)
	and	x.ie_referencia	= 'E'
	and	x.nm_usuario_cor = 'TASY'
	and	x.dt_referencia between dt_inicial_w AND dt_final_w
	and	x.nm_usuario = nm_usuario_p;
	
elsif	(ie_opcao_p = 'ENC') then
	select	count(distinct x.nr_seq_ordem_serv)
	into	qt_os_w
	from	w_avaliacao_usuario x,
		man_ordem_servico a
	where	a.nr_sequencia	= x.NR_SEQ_ORDEM_SERV
	and	x.ie_referencia	= 'E'
	and	x.nm_usuario_cor = 'TASY'
	and	x.dt_referencia between dt_inicial_w AND dt_final_w
	and	x.nm_usuario = nm_usuario_p;

elsif	(ie_opcao_p = 'CAEU') then
	select	count(distinct a.nr_sequencia)
	into	qt_os_w
	from	man_ordem_servico m,
		os_encerrada_gerencia_v a
	where	a.dt_fim_real between dt_inicial_w and dt_final_w
	and	a.nr_sequencia	= m.nr_sequencia
	and	m.nr_seq_complex in(4,5,6)
	and	exists (	select	1
				from	man_ordem_serv_ativ b
				where	a.nr_sequencia		= b.nr_seq_ordem_serv
				and	b.nm_usuario_exec	= nm_usuario_p)
	and	exists	(	select	1
				from	man_ordem_servico_exec c
				where	a.nr_sequencia		= c.nr_seq_ordem
				and	c.nm_usuario_exec	= nm_usuario_p);
				
elsif	(ie_opcao_p = 'EANA') then
	select	count(distinct a.nr_seq_ordem_serv)
	into	qt_os_w
	from	man_ordem_servico b,
		man_ordem_serv_ativ a
	where	b.nr_sequencia 		= a.nr_seq_ordem_serv
	and	a.nm_usuario_exec 	= nm_usuario_p
	and	b.ie_status_ordem	= 3
	and	exists (select	1
				from	man_ordem_servico_exec x
				where	x.nr_seq_ordem = b.nr_sequencia
				and	x.nm_usuario_exec = nm_usuario_p)
	and	b.dt_fim_real between dt_inicial_w and dt_final_w;
	
elsif	(ie_opcao_p = 'DEF') then
	select	count(distinct x.nr_seq_ordem_serv)
	into	qt_os_w
	from	w_avaliacao_usuario x
	where	x.ie_referencia	= 'ER'
	and	x.nm_usuario_cor = 'TASY'
	and	x.dt_referencia between dt_inicial_w AND dt_final_w
	and	x.nm_usuario = nm_usuario_p;
	
elsif	(ie_opcao_p = 'DEFAP') then
	
	select	count(distinct x.nr_seq_ordem_serv)
	into	qt_total_os_grupos_w
	from	w_avaliacao_usuario x
	where	x.ie_referencia	= 'ER'
	and	x.nm_usuario_cor = 'TASY'
	and	x.dt_referencia between dt_inicial_w AND dt_final_w
	and	x.nm_usuario in (	select	distinct a.nm_usuario_grupo
					from	usuario_grupo_des a
					where	nr_seq_grupo in (select	distinct b.nr_seq_grupo
								from	usuario_grupo_des b
								where	b.nm_usuario_grupo = nm_usuario_p));
	
	select	count(distinct a.nm_usuario_grupo)
	into	qt_tota_pf_grupos_w
	from	usuario_grupo_des a
	where	a.nr_seq_grupo	in (	select	distinct b.nr_seq_grupo
					from	usuario_grupo_des b
					where	b.nm_usuario_grupo = nm_usuario_p);
	
	qt_os_w	:= dividir(qt_total_os_grupos_w, qt_tota_pf_grupos_w);

elsif	(ie_opcao_p = 'ECPE') then
	select	count(distinct a.nr_sequencia)
	into	qt_os_w
	from	os_encerrada_gerencia_v a
	where	a.dt_fim_real between dt_inicial_w and dt_final_w
	and	exists (	select	1
				from	MAN_ORDEM_SERV_ATIV b
				where	a.nr_sequencia		= b.nr_seq_ordem_serv
				and	b.nm_usuario_exec	= nm_usuario_p)
	and	exists	(	select	1
				from	MAN_ORDEM_SERVICO_EXEC c
				where	a.nr_sequencia		= c.nr_seq_ordem
				and	c.nm_usuario_exec	= nm_usuario_p);
	
elsif	(ie_opcao_p = 'HRAD') then
	
	qt_min_extra_w	:= 0;
	open C01;
	loop
	fetch C01 into	
		dt_entrada_w,
		dt_saida_w,
		qt_min_intervalo_w;
	exit when C01%notfound;
		
		qt_min_realizados_w		:= Obter_Min_Entre_Datas(dt_entrada_w, dt_saida_w, 1) - qt_min_intervalo_w;
		ds_dia_semana_w			:= substr(obter_dia_semana(dt_entrada_w),1,3);
		
		if	(ds_dia_semana_w in ('S�b','Dom')) then
			qt_min_extra_w	:= qt_min_extra_w + qt_min_realizados_w;
		else
			qt_min_extra_w	:= qt_min_extra_w + (qt_min_realizados_w - 525);
		end if;
		
	end loop;
	close C01;
	
	qt_min_extra_w	:= qt_min_extra_w / 60; --Transformar em horas
	
end if;

if	(ie_opcao_p = 'HRAD') then
	return	qt_min_extra_w;
elsif	(ie_opcao_p in('T','CA','CAE','EANA','ECPE','CAEU', 'ENC','DEF','DEFAP')) then
	return	qt_os_w;
end if;

end Obter_informacao_os_pessoa;
/
