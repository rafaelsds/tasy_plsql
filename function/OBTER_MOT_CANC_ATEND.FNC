create or replace
function obter_mot_canc_atend(nr_atendimento_p	number)
 		    	return number is

nr_seq_motivo_cancelamento_w	number(10);					
			
begin

select	nvl(max(nr_seq_motivo),0)
into	nr_seq_motivo_cancelamento_w
from	atendimento_cancelamento
where	nr_atendimento = nr_atendimento_p;


return	nr_seq_motivo_cancelamento_w;

end obter_mot_canc_atend;
/