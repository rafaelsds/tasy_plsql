create or replace
function pls_obter_seq_saude(	cd_cbo_p	varchar2	)
				return number is

ds_retorno_w	varchar2(80);

begin
select	max(nr_sequencia)
into	ds_retorno_w
from	cbo_saude
where	cd_cbo = cd_cbo_p
and	ie_situacao = 'A';


return ds_retorno_w;

end pls_obter_seq_saude;
/
