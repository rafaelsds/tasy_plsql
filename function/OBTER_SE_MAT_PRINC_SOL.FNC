create or replace
function obter_se_mat_princ_sol(nr_prescricao_p		prescr_material.nr_prescricao%type,
								nr_seq_solucao_p	prescr_material.nr_sequencia_solucao%type,
								nr_seq_material_p	prescr_material.nr_sequencia%type)
 		    	return varchar2 is

cd_material_w	prescr_material.cd_material%type;
ie_principal_w	varchar2(1 char);

begin

cd_material_w := obter_medic_principal_sol(nr_prescricao_p, nr_seq_solucao_p);

select	nvl(max('S'),'N')
into	ie_principal_w
from	prescr_material a
where	a.nr_prescricao = nr_prescricao_p
and		(a.nr_sequencia_solucao = nr_seq_solucao_p or
			obter_se_medic_composto(a.nr_prescricao, a.nr_sequencia, a.nr_agrupamento) = 'S')
and		a.nr_sequencia = nr_seq_material_p
and		(nr_seq_solucao_p is null or a.cd_material = cd_material_w);

return ie_principal_w;

end obter_se_mat_princ_sol;
/
