create or replace
function gpi_obter_classif_estrut(	nr_sequencia_p	number)
				return Varchar2 is

type campos is record(	nr_seq_apres		number(10));

type vetor is table of campos index by binary_integer;
estrutura_w			vetor;
i				integer		:= 0;
nr_sequencia_w			number(10)	:= nr_sequencia_p;
nr_seq_apres_w			number(10);
nr_seq_superior_w			number(10);
qt_nivel_w			number(10)	:= 1;
cd_classificacao_w			varchar2(255);

begin
/* Obter a sequencia de apresentação da etapa*/
select	max(nr_seq_superior),
	nvl(max(nr_sequencia),1)
into	nr_seq_superior_w,
	nr_seq_apres_w
from	gpi_estrutura
where	nr_sequencia		= nr_sequencia_w;

estrutura_w(i).nr_seq_apres	:= nr_seq_apres_w;

/* Obter as sequencias de apresentação superiores*/
while	(nr_seq_superior_w is not null) loop
	begin
	nr_sequencia_w		:= nr_seq_superior_w;
	i			:= i + 1;
	
	select	max(nr_seq_superior),
		nvl(max(nr_sequencia),1)
	into	nr_seq_superior_w,
		nr_seq_apres_w
	from	gpi_estrutura
	where	nr_sequencia	= nr_sequencia_w;
	
	estrutura_w(i).nr_seq_apres	:= nr_seq_apres_w;
	
	end;
end loop;
/* Define a classificação da etapa */
if	(estrutura_w.Count = 1) then
	cd_classificacao_w	:= trim(to_char(estrutura_w(i).nr_seq_apres,'009'));
elsif	(estrutura_w.Count > 1) then
	begin
	for i in REVERSE 0..estrutura_w.Count-1 loop
		begin
		cd_classificacao_w	:= substr(cd_classificacao_w || trim(to_char(estrutura_w(i).nr_seq_apres,'009')) || '.',1,255);
		end;
	end loop;
	end;
end if;
/* se o ultimo caracter for um ponto, retirar da variavel */
if	(substr(cd_classificacao_w,length(cd_classificacao_w),1) = '.') then
	cd_classificacao_w	:= substr(cd_classificacao_w,1,length(cd_classificacao_w)-1);
end if;

return	cd_classificacao_w;

end gpi_obter_classif_estrut;
/