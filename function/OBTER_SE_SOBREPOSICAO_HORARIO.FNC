create or replace
function obter_se_sobreposicao_horario	(cd_agenda_p	number,
					dt_agenda_p	date,
					nr_min_duracao_p	number)
					return varchar2 is

ie_sobreposicao_w		varchar2(1) := 'N';
hr_param_w			date;
nr_seq_horario_w		number(10,0);
hr_agenda_w			date;
ds_lista_status_w 		varchar2(255)	:= null;
ie_sobreposicao_encaixe_w	varchar2(1) := 'S';

begin

select	nvl(max(ie_sobreposicao_encaixe),'S')
into	ie_sobreposicao_encaixe_w
from	parametro_agenda
where	cd_estabelecimento = obter_estabelecimento_ativo;

if	(cd_agenda_p is not null) and
	(dt_agenda_p is not null) and
	(nr_min_duracao_p is not null) then

	if	(wheb_usuario_pck.get_cd_funcao = 871) then /*so ira buscar o parametro se estiver na funcao Gestao da Agenda cirurgica*/
		Obter_Param_Usuario(871, 820, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ds_lista_status_w);	
	end if;
	
	/* obter horario parametro */
	hr_param_w := dt_agenda_p + nr_min_duracao_p / 1440;

	/* obter horario seguinte */
	if (nvl(ds_lista_status_w,'XPTO') = 'XPTO') then
		select	min(hr_inicio)
		into	hr_agenda_w
		from	agenda_paciente
		where	cd_agenda = cd_agenda_p
		and	dt_agenda = trunc(dt_agenda_p,'dd')
		and	hr_inicio > dt_agenda_p
		and	ie_status_agenda not in ('C','L','II')
		and	((ie_sobreposicao_encaixe_w = 'N' and nvl(ie_encaixe,'N') <> 'S' ) or ie_sobreposicao_encaixe_w = 'S');
	else	
		select	min(hr_inicio)
		into	hr_agenda_w
		from	agenda_paciente
		where	cd_agenda = cd_agenda_p
		and		dt_agenda = trunc(dt_agenda_p,'dd')
		and		hr_inicio > dt_agenda_p
		and 	obter_se_contido_char(ie_status_agenda,ds_lista_status_w || ',L') = 'N';
	end if;	

	/* verificar sobreposicao */
	if	(hr_agenda_w is not null) then

		if	( hr_param_w > hr_agenda_w ) then
		
			ie_sobreposicao_w := 'S';
		else
			ie_sobreposicao_w := 'N';

		end if;
	else
		ie_sobreposicao_w := 'N';
	end if;

end if;

return ie_sobreposicao_w;

end obter_se_sobreposicao_horario;
/