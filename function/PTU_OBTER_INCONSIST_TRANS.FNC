create or replace
function ptu_obter_inconsist_trans
			(	nr_seq_guia_p		Number,
				nr_seq_requisicao_p	Number,
				nr_seq_consulta_p	Number,
				cd_transacao_p		Varchar2,
				nr_qt_reg_p		Number,
				nr_seq_pedido_p		Number,
				cd_servico_p		Number)
				return Number is

cd_inconsistencia_w		Number(4)	:= 0;
qt_registro_w			Number(2);
nr_seq_inconsist_w		Number(10);
count_registro_w		Number(10) := 1;
ie_tipo_w			Varchar2(2);
nr_seq_servico_w		Number(10) := 0;

Cursor C01 is
	select	'P',
		a.nr_sequencia
	from	pls_guia_plano_proc a
	where	a.nr_seq_guia	= nr_seq_guia_p
	and	a.cd_procedimento	= cd_servico_p
	and	exists	(	select	1
				from	ptu_intercambio_consist x
				where	x.nr_seq_procedimento	= a.nr_sequencia
				and	x.nr_seq_guia		= nr_seq_guia_p)
	union
	select	'M',
		a.nr_sequencia
	from	pls_guia_plano_mat a
	where	a.nr_seq_guia	= nr_seq_guia_p
	and	substr(pls_obter_seq_codigo_material(a.nr_seq_material,''),1,255)	= cd_servico_p
	and	exists	(	select	1
				from	ptu_intercambio_consist x
				where	x.nr_seq_material	= a.nr_sequencia
				and	x.nr_seq_guia		= nr_seq_guia_p);
		
Cursor C02 is
	select	distinct(nr_seq_inconsistencia)
	from	ptu_intercambio_consist
	where	(nr_seq_guia		= nr_seq_guia_p 	or nr_seq_guia_p 		is null)
	and	(nr_seq_requisicao	= nr_seq_requisicao_p	or nr_seq_requisicao_p		is null)
	and	(nr_seq_procedimento	= nr_seq_servico_w 	or nr_seq_servico_w 		is null)
	and	(ie_tipo_w		= 'P'			or ie_tipo_w			is null)
	and	cd_transacao		= cd_transacao_p;

Cursor C03 is
	select	distinct(nr_seq_inconsistencia)
	from	ptu_intercambio_consist
	where	(nr_seq_guia		= nr_seq_guia_p 	or nr_seq_guia_p 		is null)
	and	(nr_seq_requisicao	= nr_seq_requisicao_p	or nr_seq_requisicao_p		is null)
	and	(nr_seq_material	= nr_seq_servico_w 	or nr_seq_servico_w 		is null)
	and	(ie_tipo_w		= 'M'			or ie_tipo_w			is null)
	and	cd_transacao		= cd_transacao_p;
	
Cursor C04 is
	select	distinct(nr_seq_inconsistencia)
	from	ptu_intercambio_consist
	where	((nr_seq_consulta_benef	= nr_seq_consulta_p)
	or	(nr_seq_consulta_prest	= nr_seq_consulta_p))
	and	cd_transacao		= cd_transacao_p;

Cursor C05 is
	select	'P',
		a.nr_sequencia
	from	pls_requisicao		b,
		pls_requisicao_proc 	a
	where	a.nr_seq_requisicao	= nr_seq_requisicao_p
	and	a.nr_seq_requisicao	= b.nr_sequencia
	and	((a.cd_procedimento	= cd_servico_p)	
	or	(exists	(	select	1
				from	ptu_pedido_autorizacao	x,
					ptu_pedido_aut_servico	y
				where	x.nr_sequencia		= y.nr_seq_pedido
				and	x.nr_seq_requisicao	= b.nr_sequencia
				and	y.cd_servico_consersao	= a.cd_procedimento
				and	y.cd_servico		= cd_servico_p))
	or 	(a.cd_procedimento_ptu	= cd_servico_p))
	and	exists	(	select	1
				from	ptu_intercambio_consist x
				where	x.nr_seq_procedimento	= a.nr_sequencia
				and	x.nr_seq_requisicao	= nr_seq_requisicao_p)
	and	a.ie_status	in ('N','G')
	and	b.ie_estagio	in (6,7)
	union
	select	'M',
		a.nr_sequencia
	from	pls_requisicao		b,
		pls_requisicao_mat 	a
	where	a.nr_seq_requisicao	= nr_seq_requisicao_p
	and	a.nr_seq_requisicao	= b.nr_sequencia
	and	((substr(pls_obter_seq_codigo_material(a.nr_seq_material,''),1,255)	= cd_servico_p)
	or	(exists	(	select	1
				from	ptu_pedido_autorizacao	x,
					ptu_pedido_aut_servico	y
				where	x.nr_sequencia		= y.nr_seq_pedido
				and	x.nr_seq_requisicao	= b.nr_sequencia
				and	y.cd_servico_consersao	= substr(pls_obter_seq_codigo_material(a.nr_seq_material,''),1,255)
				and	y.cd_servico		= cd_servico_p))
	or 	(a.cd_material_ptu	= cd_servico_p))
	and	exists	(	select	1
				from	ptu_intercambio_consist x
				where	x.nr_seq_material	= a.nr_sequencia
				and	x.nr_seq_requisicao	= nr_seq_requisicao_p)
	and	a.ie_status	in ('N','G')
	and	b.ie_estagio	in (6,7);

begin

open C01;
loop
fetch C01 into	
	ie_tipo_w,
	nr_seq_servico_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_inconsist_w;
	exit when C02%notfound;
		begin
		
		select	count(distinct(nr_seq_inconsistencia))
		into	qt_registro_w
		from	ptu_intercambio_consist
		where	(nr_seq_guia		= nr_seq_guia_p 	or nr_seq_guia_p 		is null)
		and	(nr_seq_procedimento	= nr_seq_servico_w 	or nr_seq_servico_w 		is null)
		and	cd_transacao		= cd_transacao_p
		and	ie_tipo_w		= 'P';
		
		select	max(cd_inconsistencia)
		into	cd_inconsistencia_w
		from	ptu_inconsistencia
		where	nr_sequencia	= nr_seq_inconsist_w;
		
		if	(nr_qt_reg_p	<= qt_registro_w) then
			if	(count_registro_w  =  nr_qt_reg_p) then		
				return	cd_inconsistencia_w;
			else
				count_registro_w := count_registro_w  + 1;
			end if;
		else
			cd_inconsistencia_w	:= 0;
		end if;
		
		end;
	end loop;
	close C02;
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_inconsist_w;
	exit when C03%notfound;
		begin
		
		select	count(distinct(nr_seq_inconsistencia))
		into	qt_registro_w
		from	ptu_intercambio_consist
		where	(nr_seq_guia		= nr_seq_guia_p 	or nr_seq_guia_p 		is null)
		and	(nr_seq_material	= nr_seq_servico_w 	or nr_seq_servico_w 		is null)
		and	cd_transacao		= cd_transacao_p
		and	ie_tipo_w		= 'M';
		
		select	max(cd_inconsistencia)
		into	cd_inconsistencia_w
		from	ptu_inconsistencia
		where	nr_sequencia	= nr_seq_inconsist_w;
		
		if	(nr_qt_reg_p	<= qt_registro_w) then
			if	(count_registro_w  =  nr_qt_reg_p) then		
				return	cd_inconsistencia_w;
			else
				count_registro_w := count_registro_w  + 1;
			end if;
		else
			cd_inconsistencia_w	:= 0;
		end if;
		
		end;
	end loop;
	close C03;
	
	end;
end loop;
close C01;

open C05;
loop
fetch C05 into	
	ie_tipo_w,
	nr_seq_servico_w;
exit when C05%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_inconsist_w;
	exit when C02%notfound;
		begin
		
		select	count(distinct(nr_seq_inconsistencia))
		into	qt_registro_w
		from	ptu_intercambio_consist
		where	(nr_seq_requisicao	= nr_seq_requisicao_p	or nr_seq_requisicao_p		is null)
		and	(nr_seq_procedimento	= nr_seq_servico_w 	or nr_seq_servico_w 		is null)
		and	cd_transacao		= cd_transacao_p
		and	ie_tipo_w		= 'P';
		
		select	max(cd_inconsistencia)
		into	cd_inconsistencia_w
		from	ptu_inconsistencia
		where	nr_sequencia	= nr_seq_inconsist_w;
		
		if	(nr_qt_reg_p	<= qt_registro_w) then
			if	(count_registro_w  =  nr_qt_reg_p) then		
				return	cd_inconsistencia_w;
			else
				count_registro_w := count_registro_w  + 1;
			end if;
		else
			cd_inconsistencia_w	:= 0;
		end if;
		
		end;
	end loop;
	close C02;
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_inconsist_w;
	exit when C03%notfound;
		begin
		
		select	count(distinct(nr_seq_inconsistencia))
		into	qt_registro_w
		from	ptu_intercambio_consist
		where	(nr_seq_requisicao	= nr_seq_requisicao_p	or nr_seq_requisicao_p		is null)
		and	(nr_seq_material	= nr_seq_servico_w 	or nr_seq_servico_w 		is null)
		and	cd_transacao		= cd_transacao_p
		and	ie_tipo_w		= 'M';
		
		select	max(cd_inconsistencia)
		into	cd_inconsistencia_w
		from	ptu_inconsistencia
		where	nr_sequencia	= nr_seq_inconsist_w;
		
		if	(nr_qt_reg_p	<= qt_registro_w) then
			if	(count_registro_w  =  nr_qt_reg_p) then		
				return	cd_inconsistencia_w;
			else
				count_registro_w := count_registro_w  + 1;
			end if;
		else
			cd_inconsistencia_w	:= 0;
		end if;
		
		end;
	end loop;
	close C03;
	
	end;
end loop;
close C05;

if	(((nr_seq_guia_p	is null) or (nr_seq_requisicao_p	is null)) and (nr_seq_pedido_p	is not null)) then
	nr_seq_servico_w	:= nr_seq_pedido_p;
	open C02;
	loop
	fetch C02 into	
		nr_seq_inconsist_w;
	exit when C02%notfound;
		begin
		
		select	count(distinct(nr_seq_inconsistencia))
		into	qt_registro_w
		from	ptu_intercambio_consist
		where	(nr_seq_guia		= nr_seq_guia_p 	or nr_seq_guia_p 		is null)
		and	(nr_seq_requisicao	= nr_seq_requisicao_p	or nr_seq_requisicao_p		is null)
		and	(nr_seq_procedimento	= nr_seq_servico_w 	or nr_seq_servico_w 		is null)
		and	cd_transacao		= cd_transacao_p;
		
		select	max(cd_inconsistencia)
		into	cd_inconsistencia_w
		from	ptu_inconsistencia
		where	nr_sequencia	= nr_seq_inconsist_w;
		
		if	(nr_qt_reg_p	<= qt_registro_w) then
			if	(count_registro_w  =  nr_qt_reg_p) then		
				return	cd_inconsistencia_w;
			else
				count_registro_w := count_registro_w  + 1;
			end if;
		else
			cd_inconsistencia_w	:= 0;
		end if;
		
		end;
	end loop;
	close C02;

	open C03;
	loop
	fetch C03 into	
		nr_seq_inconsist_w;
	exit when C03%notfound;
		begin
		
		select	count(distinct(nr_seq_inconsistencia))
		into	qt_registro_w
		from	ptu_intercambio_consist
		where	(nr_seq_guia		= nr_seq_guia_p 	or nr_seq_guia_p 		is null)
		and	(nr_seq_requisicao	= nr_seq_requisicao_p	or nr_seq_requisicao_p		is null)
		and	(nr_seq_material	= nr_seq_servico_w 	or nr_seq_servico_w 		is null)
		and	cd_transacao		= cd_transacao_p;
		
		select	max(cd_inconsistencia)
		into	cd_inconsistencia_w
		from	ptu_inconsistencia
		where	nr_sequencia	= nr_seq_inconsist_w;
		
		if	(nr_qt_reg_p	<= qt_registro_w) then
			if	(count_registro_w  =  nr_qt_reg_p) then		
				return	cd_inconsistencia_w;
			else
				count_registro_w := count_registro_w  + 1;
			end if;
		else
			cd_inconsistencia_w	:= 0;
		end if;
		
		end;
	end loop;
	close C03;

end if;

open C04;
loop
fetch C04 into	
	nr_seq_inconsist_w;
exit when C04%notfound;
	begin
	
	select	count(distinct(nr_seq_inconsistencia))
	into	qt_registro_w
	from	ptu_intercambio_consist
	where	((nr_seq_consulta_benef	= nr_seq_consulta_p)
	or	(nr_seq_consulta_prest	= nr_seq_consulta_p))
	and	cd_transacao		= cd_transacao_p;
	
	select	max(cd_inconsistencia)
	into	cd_inconsistencia_w
	from	ptu_inconsistencia
	where	nr_sequencia	= nr_seq_inconsist_w;
	
	if	(nr_qt_reg_p	<= qt_registro_w) then
		if	(count_registro_w  =  nr_qt_reg_p) then		
			return	cd_inconsistencia_w;
		else
			count_registro_w := count_registro_w  + 1;
		end if;
	else
		cd_inconsistencia_w	:= 0;
	end if;
	
	end;
end loop;
close C04;

return	cd_inconsistencia_w;

end ptu_obter_inconsist_trans;
/