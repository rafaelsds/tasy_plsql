create or replace
function ctb_obter_justi_orc_periodo(
				cd_empresa_p			number,
				cd_centro_custo_p		number,
				cd_conta_contabil_p		varchar2,
				dt_inicial_p		date,
				dt_final_p		date)
		return 	varchar2 is

nr_seq_mes_ref_w		number(10);
ds_justificativa_w		varchar2(2000);

begin

select	nvl(max(ds_justificativa),'')
into	ds_justificativa_w
from	ctb_orcamento
where	cd_centro_custo		= cd_centro_custo_p
and	cd_conta_contabil	= nvl(cd_conta_contabil_p,cd_conta_contabil)
and	nvl(ds_justificativa,'X') 	<> 'X'
and	to_date(ctb_obter_mes_ref(nr_seq_mes_ref)) between dt_inicial_p and dt_final_p
and	nr_sequencia  = (select max(nr_sequencia)
			 from   ctb_orcamento
			 where	cd_centro_custo		= cd_centro_custo_p
			 and	cd_conta_contabil	= nvl(cd_conta_contabil_p,cd_conta_contabil)
			 and	nvl(ds_justificativa,'X') 	<> 'X'
			 and	to_date(ctb_obter_mes_ref(nr_seq_mes_ref)) between dt_inicial_p and dt_final_p);

return	substr(ds_justificativa_w,1,2000);

end ctb_obter_justi_orc_periodo;
/