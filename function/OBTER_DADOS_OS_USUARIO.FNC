create or replace
function Obter_dados_OS_usuario( nm_usuario_des_p		varchar2,
				dt_referencia_p			date,
				ie_opcao_p			varchar2)
 		    	return number is

qt_retorno_w		number(15,4);
qt_ordens_trab_w	number(10);
cd_pessoa_fisica_w	varchar2(255);

begin

if	(ie_opcao_p = 'QTOS') then
	select	count(distinct a.nr_sequencia)
	into	qt_retorno_w
	from	man_ordem_servico a
	where	exists (select 1
			from	man_ordem_serv_ativ b
			where	a.nr_sequencia		= b.nr_seq_ordem_serv
			and	trunc(b.dt_atividade,'year') = trunc(dt_referencia_p,'year')
			and	b.nm_usuario_exec	= nm_usuario_des_p);
elsif	(ie_opcao_p = 'PRDEF') then

	select	count(distinct a.nr_sequencia)
	into	qt_ordens_trab_w
	from	man_ordem_servico a
	where	exists (select 1
			from	man_ordem_serv_ativ b
			where	a.nr_sequencia		= b.nr_seq_ordem_serv
			and	trunc(b.dt_atividade,'year') = trunc(dt_referencia_p,'year')
			and	b.nm_usuario_exec	= nm_usuario_des_p);

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	usuario
	where	nm_usuario = nm_usuario_des_p;

	select	count(distinct a.nr_sequencia)
	into	qt_retorno_w
	from	man_doc_erro a,
		man_ordem_servico b
	where	b.nr_sequencia		= a.nr_seq_ordem
	and	a.dt_liberacao is not null
	and	trunc(b.dt_ordem_servico,'year') = trunc(dt_referencia_p,'year')
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	a.ie_origem_erro	= 'W'
	and	nvl(a.ie_recorrente,'N') = 'N';
	
	qt_retorno_w	:= dividir(qt_retorno_w *100, qt_ordens_trab_w);
	
elsif	(ie_opcao_p = 'QTDEF') then

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	usuario
	where	nm_usuario = nm_usuario_des_p;

	select	count(distinct a.nr_sequencia)
	into	qt_retorno_w
	from	man_doc_erro a,
		man_ordem_servico b
	where	b.nr_sequencia		= a.nr_seq_ordem
	and	a.dt_liberacao is not null
	and	trunc(b.dt_ordem_servico,'year') = trunc(dt_referencia_p,'year')
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	a.ie_origem_erro	= 'W'
	and	nvl(a.ie_recorrente,'N') = 'N';
end if;

return	qt_retorno_w;

end Obter_dados_OS_usuario;
/