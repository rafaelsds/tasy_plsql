create or replace 
function pls_obter_permite_bio_cirur(	cd_medico_p      	pessoa_fisica.cd_pessoa_fisica%type)
					return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Validar se o medico esta apto a fazer checkin/checkout na execucao cirurgica
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [ x] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */	

ds_retorno_w			varchar2(1)	:= 'S';
	
begin

--Verifica se o medico nao esta com participacao cirurgica em andamento.
select	decode(count(1),0,'S','N')
into	ds_retorno_w
from	pls_controle_biometria_med
where	cd_medico	= cd_medico_p
and	dt_check_in	is not null
and	dt_check_out	is null;

return  ds_retorno_w;

end pls_obter_permite_bio_cirur;
/