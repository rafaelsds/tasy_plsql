create or replace
function obter_se_empresa_reenserida(	nr_seq_fornec_p			number,
					nr_Seq_lic_item_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1);
qt_registro_w		number(10);
			
begin

select	count(*)
into	qt_registro_w
from	reg_lic_fornec_lance
where	nr_seq_fornec = nr_seq_fornec_p
and	nr_seq_lic_item = nr_seq_lic_item_p
and	nvl(ie_reenserida,'N') = 'S';

if	(qt_registro_w = 0) then
	ds_retorno_w := 'N';
else
	ds_retorno_w := 'S';
end if;


return	ds_retorno_w;

end obter_se_empresa_reenserida;
/