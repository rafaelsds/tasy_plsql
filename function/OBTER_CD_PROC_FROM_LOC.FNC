CREATE OR REPLACE
FUNCTION Obter_cd_proc_from_loc(cd_procedimento_loc_p	varchar2,
				ie_origem_proced_p	number)	
RETURN VarChar2 IS

cd_procedimento_w		procedimento.cd_procedimento%TYPE;

BEGIN

if	(cd_procedimento_loc_p is not null) then
	
	begin
	select 	max(cd_procedimento)
	into 	cd_procedimento_w
	from 	procedimento
	where 	cd_procedimento_loc = cd_procedimento_loc_p
	  and 	ie_origem_proced = nvl(ie_origem_proced_p,ie_origem_proced)
	  and 	rownum = 1;
	end;
end if;
RETURN cd_procedimento_w;

END Obter_cd_proc_from_loc; 
/