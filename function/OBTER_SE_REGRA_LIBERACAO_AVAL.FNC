create or replace
function obter_se_regra_liberacao_aval (nr_seq_tipo_p		number,
					cd_funcao_p		number,
					ie_opcao_p		varchar2) 
					return varchar2 is

ds_retorno_w		varchar2(1) := 'N';
ie_gera_evolucao_w	varchar2(1) := 'N';
					
begin

if	(ie_opcao_p = 'EV') then
	select	max(ie_gerar_evolucao)
	into	ie_gera_evolucao_w
	from	med_tipo_avaliacao
	where	nr_sequencia = nr_seq_tipo_p;

	if	(ie_gera_evolucao_w = 'R') then
		select	nvl(max(ie_gera_evolucao),'N')
		into	ds_retorno_w
		from	med_avaliacao_liberacao
		where	nvl(ie_situacao,'A')	= 'A'
		and	nr_seq_tipo		= nr_seq_tipo_p
		and	cd_funcao		= cd_funcao_p;
	elsif	(ie_gera_evolucao_w = 'A') and (cd_funcao_p in (281,872)) then
		ds_retorno_w := 'S';
	elsif	(ie_gera_evolucao_w = 'S') and (cd_funcao_p = 281) then
		ds_retorno_w := 'S';
	elsif	(ie_gera_evolucao_w = 'C') and (cd_funcao_p = 872) then
		ds_retorno_w := 'S';
	end if;
end if;	

return	ds_retorno_w;	

end obter_se_regra_liberacao_aval;
/