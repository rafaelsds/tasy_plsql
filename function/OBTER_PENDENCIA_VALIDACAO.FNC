create or replace
function obter_pendencia_validacao(nr_seq_pendencia_p		number,
					ie_tipo_validacao_p	varchar2)
 		    	return varchar2 is

ie_retorno_w		varchar2(1) := 'N';
qt_pre_validacao_w	Integer;
qt_interno_w		Integer;
qt_externo_w		Integer;

begin

select 	nvl(sum(case when ie_validation_type  = 'P' then 1 else 0 end),0) qt_pre_validacao,
	nvl(sum(case when ie_validation_type  = 'I' then 1 else 0 end),0) qt_interno,
	nvl(sum(case when ie_validation_type  = 'E' then 1 else 0 end),0) qt_externo
into	qt_pre_validacao_w,
	qt_interno_w,
	qt_externo_w
from 	reg_validation_people
where 	nr_seq_ct_validation = nr_seq_pendencia_p;

if (ie_tipo_validacao_p = 'PV') then
	if (qt_pre_validacao_w > 0) then
		if (qt_externo_w = 0 and qt_interno_w = 0) then
			ie_retorno_w := 'S';
		end if;
	end if;
elsif (ie_tipo_validacao_p = 'PP') then
	if (qt_pre_validacao_w = 0) then
		ie_retorno_w := 'S';
	end if;
end if;


return	ie_retorno_w;

end obter_pendencia_validacao;
/