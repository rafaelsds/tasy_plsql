create or replace
function obter_desc_label_relatorio(cd_expressao_p	number,
				ds_expressao_p	varchar2 default null)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(4000) := null;

begin
  ds_retorno_w := TRIM(OBTER_DESC_EXPRESSAO(cd_expressao_p, NVL(ds_expressao_p, ''))) || ':';
  return	ds_retorno_w;
end obter_desc_label_relatorio;
/