create or replace
function obter_se_generico_atepaclp(cd_mat_barras_p		number,
  cd_material_p	number)
  return varchar2 is

ie_generico_w	varchar2(1) := 'N';
cd_material_w material.cd_material%type;
cd_perfil_w number(10);
ie_generico_liberado_w varchar2(1);
ie_generico_igual_liberado_w varchar2(1);

Cursor C01 is
select  a.cd_material_generico cd_material
from  material a
where a.cd_material = cd_mat_barras_p
and a.cd_material_generico is not null
union
select a.cd_material
from  material a
where cd_material_generico =
  (select nvl(cd_material_generico, cd_material)
  from  material
  where cd_material = cd_mat_barras_p)
and a.cd_material <> cd_mat_barras_p
and a.cd_material <> a.cd_material_generico;

begin	

cd_perfil_w			:= obter_perfil_ativo;
ie_generico_liberado_w 	:= nvl(obter_valor_param_usuario(7029, 13, cd_perfil_w, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'N');
ie_generico_igual_liberado_w 	:= nvl(obter_valor_param_usuario(7029, 77, cd_perfil_w, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'N');

if (ie_generico_liberado_w = 'S') then

  if (ie_generico_igual_liberado_w = 'S') then
    ie_generico_w := nvl(substr(obter_se_generico_igual(cd_mat_barras_p, cd_material_p),1,1), 'N');
  end if;

  open C01;
  loop
  fetch C01 into
    cd_material_w;
  exit when C01%notfound;
    begin
    if (cd_material_w = cd_material_p) then
      ie_generico_w := 'S';
    end if;
    end;
  end loop;
  close C01;
end if;

return	ie_generico_w;

end obter_se_generico_atepaclp;
/
