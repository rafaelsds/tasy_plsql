create or replace
function obter_dias_horas_trab_corp(	ie_opcao_p		varchar2,
			nm_usuario_periodo_p	varchar2,
			dt_periodo_p		date,
			qt_horas_dia_p		number,
			cd_estabelecimento_p	number)
 		    	return number is

qt_retorno_w		number(10,2);
qt_dias_uteis_w		number(10);
dt_inicio_mes_w		date;
dt_fim_mes_w		date;
qt_dias_falta_w		number(10,2);
dt_inicio_filtro_w	date;
dt_final_filtro_w	date;

cursor c01 is
	select	a.dt_inicio,
		a.dt_fim
	from	ausencia_tasy a
	where	a.nm_usuario_ausente = nm_usuario_periodo_p
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	'S' = case when	((a.dt_inicio >= dt_inicio_mes_w) and (a.dt_fim <= dt_fim_mes_w)) then 'S'	/* faltas somente dentro do m�s */
			when ((a.dt_inicio <= dt_inicio_mes_w) and (a.dt_fim >= dt_inicio_mes_w)) then 'S'	/* Falta desde antes do inicio do m�s at� meses sequintes */
			when ((a.dt_inicio >= dt_inicio_mes_w) and (a.dt_inicio <= dt_fim_mes_w)) then 'S'	/* Falta iniciou dentro do m�s */
			when ((a.dt_fim >= dt_inicio_mes_w) and (a.dt_fim <= dt_fim_mes_w)) then 'S'		/* Falta terminou dentro do m�s */
			else 'N' end
	order by
		1;

c01_w		c01%rowtype;

begin

dt_inicio_mes_w := trunc(dt_periodo_p,'mm');
dt_fim_mes_w	:= fim_mes(dt_inicio_mes_w);

/* Quantidade de dias uteis dentro do m�s informado */
select	obter_dias_uteis_periodo(dt_inicio_mes_w,dt_fim_mes_w,cd_estabelecimento_p)
into	qt_dias_uteis_w
from	dual;

qt_dias_falta_w := 0;
open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin

	/* Pegar data de in�cio sempre dentro do m�s */
	if	(c01_w.dt_inicio < dt_inicio_mes_w) then
		begin
		dt_inicio_filtro_w := dt_inicio_mes_w;
		end;
	else
		begin
		dt_inicio_filtro_w := c01_w.dt_inicio;
		end;
	end if;

	/* Pegar data de final sempre dentro do m�s */
	if	(c01_w.dt_fim > dt_fim_mes_w) then
		begin
		dt_final_filtro_w := dt_fim_mes_w;
		end;
	else
		begin
		dt_final_filtro_w := c01_w.dt_fim;
		end;
	end if;


	qt_dias_falta_w := qt_dias_falta_w + obter_dias_uteis_periodo(dt_inicio_filtro_w,dt_final_filtro_w,cd_estabelecimento_p);
	end;
end loop;
close c01;

/* Descontar os dias de falta */
qt_dias_uteis_w := (qt_dias_uteis_w - qt_dias_falta_w);

if	(ie_opcao_p = 'D') then
	begin
	qt_retorno_w := qt_dias_uteis_w;
	end;
elsif	(ie_opcao_p = 'H') then
	begin
	qt_retorno_w := (qt_dias_uteis_w * qt_horas_dia_p);
	end;
end if;

return	qt_retorno_w;

end obter_dias_horas_trab_corp;
/