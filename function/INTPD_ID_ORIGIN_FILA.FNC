CREATE OR REPLACE function intpd_id_origin_fila(	nr_seq_fila_trans_p	varchar2)
				return varchar2 is

ds_retorno_w 	varchar2(255);

begin

select	ds_id_origin
into ds_retorno_w
from 	intpd_eventos_sistema
where 	nr_sequencia = (select	max(nr_seq_evento_sistema)
			from 	intpd_fila_transmissao
			where 	nr_sequencia = nr_seq_fila_trans_p);

return	ds_retorno_w;

end intpd_id_origin_fila;
/