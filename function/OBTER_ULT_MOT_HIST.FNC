create or replace
function obter_ult_mot_hist(nr_sequencia_p number)
        return varchar2 is

ds_retorno_w	varchar2(255);
nr_seq_aux_w	number(10);

begin

if	(nr_sequencia_p is not null) and
	(nr_sequencia_p > 0) then
	begin
	
	select	max(b.nr_sequencia)
	into	nr_seq_aux_w
	from	gestao_vaga_hist b
	where	b.nr_seq_gestao = nr_sequencia_p
	and	dt_liberacao is not null;
	
	if (nr_seq_aux_w > 0) then
		begin
		
			select	substr(obter_descricao_padrao('GESTAO_VAGA_MOTIVO_HIST','DS_MOTIVO',NR_SEQ_MOTIVO_HISTORICO),1,255)
			into	ds_retorno_w
			from	gestao_vaga_hist a
			where	a.nr_sequencia = nr_seq_aux_w;
			
		end;
	end if;
	
	end;
end if;

return ds_retorno_w;

end obter_ult_mot_hist;
/