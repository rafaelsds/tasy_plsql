create or replace
function lab_obter_desc_movimentacao (nr_seq_mot_movimentacao_p lab_soro_mot_movimentacao.nr_sequencia%type)
 		    	return varchar2 is
				
ds_retorno_w	varchar2(255);

begin

if (nr_seq_mot_movimentacao_p is not null) then
	select  ds_movimentacao
	into	ds_retorno_w
	from    lab_soro_mot_movimentacao
	where   nr_sequencia = nr_seq_mot_movimentacao_p;
end if;

return	ds_retorno_w;

end lab_obter_desc_movimentacao;
/