create or replace
function obter_tempo_proced_agenda(	nr_seq_proc_interno_p	number,
					cd_medico_p		varchar2,
					cd_agenda_p		number,
					ie_opcao_p		varchar2)
					return number is

qt_min_cirugia_w	number(10,0) := null;
qt_min_higienizacao_w	number(10,0);
cd_estabelecimento_w	number(4);

/*
ie_opcao_p
TH - Tempo + Higienização
H - Higienização
T - Tempo
*/

begin

select	nvl(max(cd_estabelecimento),0)
into	cd_estabelecimento_w
from	agenda
where	cd_agenda = cd_agenda_p;

select	max(qt_min_cirugia)
into	qt_min_cirugia_w
from	proc_interno_tempo
where	nr_seq_proc_interno		= nr_seq_proc_interno_p
and	nvl(cd_medico,nvl(cd_medico_p,'X')) = nvl(cd_medico_p,'X')
and	nvl(cd_estabelecimento,cd_estabelecimento_w)	= cd_estabelecimento_w;

if	(qt_min_cirugia_w is null) then
	select	nvl(max(qt_min_cirurgia),0)
	into	qt_min_cirugia_w
	from	proc_interno
	where	nr_sequencia		= nr_seq_proc_interno_p;
end if;

select	nvl(max(qt_min_higienizacao),0)
into	qt_min_higienizacao_w
from	proc_interno
where	nr_sequencia			= nr_seq_proc_interno_p;

if	(ie_opcao_p = 'T') then
	return qt_min_cirugia_w;
elsif	(ie_opcao_p = 'H') then
	return qt_min_higienizacao_w;
else
	return qt_min_cirugia_w + qt_min_higienizacao_w;
end if;	
	
end obter_tempo_proced_agenda;
/