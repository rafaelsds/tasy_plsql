CREATE OR REPLACE FUNCTION PFCS_GET_ALGORITHM_VISIBILITY(
    cd_algorithm_p varchar2,
    cd_profile_p number default null,
    cd_establishment_p number default null,
    nm_user_p varchar2 default null
) RETURN VARCHAR2 AS

/*
    Get algorithm visibility based on username, profile and/or establishment following the standard restriction order used on TASY
    cd_algorithm_p:
        - SIM: SIMULATION
        - LOS: REMAINING LOS
        - FF: FREQUENT FLYER
        - EDI: TRS
        - LACE: READMISSION RISK

    RETURN 'Y' (yes) or 'N' (no)
*/

ie_visible_w varchar2(1) := 'Y';

BEGIN
    select nvl(max(v.ie_visibility), 'Y')
    into ie_visible_w
    from (
        select sett.ie_visibility,
            sett.cd_estabelecimento cd_establishment,
            sett.cd_perfil cd_profile,
            sett.nm_usuario_regra nm_user
        from pfcs_algorithm_settings sett,
            pfcs_algorithm alg
        where sett.nr_seq_pfcs_algorithm = alg.nr_sequencia
            and alg.cd_algorithm = cd_algorithm_p
            and sett.nr_seq_pfcs_algorithm = alg.nr_sequencia
            and nvl(sett.nm_usuario_regra, nm_user_p) = nm_user_p
            and nvl(sett.cd_perfil, cd_profile_p) = cd_profile_p
            and nvl(sett.cd_estabelecimento, cd_establishment_p) = cd_establishment_p
        order by sett.nm_usuario_regra, sett.cd_perfil, sett.cd_estabelecimento asc
    ) v where rownum = 1;

    return ie_visible_w;
END PFCS_GET_ALGORITHM_VISIBILITY;
/
