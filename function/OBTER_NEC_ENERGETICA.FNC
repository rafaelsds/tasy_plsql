create or replace 
function obter_nec_energetica(qt_peso_atual_p number,
							  qt_altura_p number,
							  qt_fator_ativ_fisica_p number,
							  nr_atendimento_p number) return number is

ds_result_w	number(10,3);
cd_pf_w number(10);
ds_sexo_w varchar(1);
idade_ano_w number(10);
idade_mes_w number(10);
qt_altura_w number(7,3);

begin


if(qt_altura_p > 0 and qt_fator_ativ_fisica_p > 0 and qt_peso_atual_p > 0)then


		select cd_pessoa_fisica
		into cd_pf_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_p;

		ds_sexo_w := obter_sexo_pf(cd_pf_w, 'C');
		idade_ano_w := Obter_Idade_PF(cd_pf_w, sysdate, 'A');
		idade_mes_w := Obter_Idade_PF(cd_pf_w, sysdate, 'M');
		qt_altura_w := qt_altura_p/100;
		

		if(ds_sexo_w = 'M')then
		
			if(idade_ano_w = 0 and idade_mes_w <= 3)then
				ds_result_w := (89 * qt_peso_atual_p) - 100 + 175;
				
			elsif (idade_ano_w = 0 and idade_mes_w <= 6) then
				ds_result_w := (89 * qt_peso_atual_p) - 100 + 56;
				
			elsif (idade_ano_w = 0 and idade_mes_w <= 12) then
				ds_result_w := (89 * qt_peso_atual_p) - 100 + 22;
				
			elsif (idade_ano_w = 0 and idade_mes_w <= 12) then
				ds_result_w := (89 * qt_peso_atual_p) - 100 + 20;
				
			elsif (idade_ano_w >= 3 and idade_ano_w <= 8) then
				ds_result_w := 88.5 - (61.9 * idade_ano_w) + qt_fator_ativ_fisica_p * ((26.7 * qt_peso_atual_p) + (903 * qt_altura_w)) + 20;
				
			elsif (idade_ano_w <= 18) then
				ds_result_w := 88.5 - (61.9 * idade_ano_w) + qt_fator_ativ_fisica_p * ((26.7 * qt_peso_atual_p) + (903 * qt_altura_w)) + 25;
			end if;
		else
			if(idade_ano_w = 0 and idade_mes_w <= 3)then
				ds_result_w := (89 * qt_peso_atual_p) - 100 + 175;
				
			elsif (idade_ano_w = 0 and idade_mes_w <= 6) then
				ds_result_w := (89 * qt_peso_atual_p) - 100 + 56;
				
			elsif (idade_ano_w = 0 and idade_mes_w <= 12) then
				ds_result_w := (89 * qt_peso_atual_p) - 100 + 22;
				
			elsif (idade_ano_w = 0 and idade_mes_w <= 12) then
				ds_result_w := (89 * qt_peso_atual_p) - 100 + 20;
				
			elsif (idade_ano_w >= 3 and idade_ano_w <= 8) then
				ds_result_w := 135.3 - (30.8 * idade_ano_w) + qt_fator_ativ_fisica_p * ((10.0 * qt_peso_atual_p) + (934 * qt_altura_w)) + 20;
				
			elsif (idade_ano_w <= 18) then
				ds_result_w := 135.3 - (30.8 * idade_ano_w) + qt_fator_ativ_fisica_p * ((10.0 * qt_peso_atual_p) + (934 * qt_altura_w)) + 25;
			end if;
			
		end if;
		
	end if;

return ds_result_w;

end obter_nec_energetica;
/