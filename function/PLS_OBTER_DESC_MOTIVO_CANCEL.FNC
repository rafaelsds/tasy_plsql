create or replace
function pls_obter_desc_motivo_cancel
			(	nr_seq_motivo_p		Number)
 		    		return Varchar2 is

ds_retorno_w			Varchar2(255);

begin

select	nvl(max(ds_motivo_cancelamento),'')
into	ds_retorno_w
from	pls_motivo_cancelamento
where	nr_sequencia	= nr_seq_motivo_p;

return	ds_retorno_w;

end pls_obter_desc_motivo_cancel;
/