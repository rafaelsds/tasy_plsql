create or replace
function existe_result_parcial(nr_prescricao_p	varchar2,
			       nr_seq_prescr_p	varchar2)
 		    	return varchar2 is
qt_registros	number(10);
ds_retorno_w	varchar2(10);
begin
ds_retorno_w := 'N';

if (nr_prescricao_p is not null) then
	select	count(*) 
	into	qt_registros
	from	exame_lab_result_parcial
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_prescr = nr_seq_prescr_p;
	
	if (qt_registros > 0) then
		ds_retorno_w := 'S';
	else
		ds_retorno_w := 'N';
	end if;
	
end if;

return	ds_retorno_w;

end existe_result_parcial;
/