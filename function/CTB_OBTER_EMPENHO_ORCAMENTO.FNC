create or replace
function ctb_obter_empenho_orcamento(
				nr_seq_mes_ref_p		number,
				cd_conta_contabil_p	varchar2,
				cd_centro_custo_p		number,
				cd_estabelecimento_p	number,
				dt_inicial_p date default null,
				dt_final_p date default null)
				return	number is

cd_empresa_w			number(10);
dt_referencia_w			date;
nr_seq_regra_w			number(10);
vl_empenho_ant_w		number(15,2) :=0;
vl_empenho_w			number(15,2) :=0;
vl_ordem_w			number(15,2);
vl_solicitacao_w			number(15,2);
qt_regra_w			number(10);
nr_mes_w			number(2);
nr_ano_w				number(4);
dt_final_w			date;
dt_inicial_w			date;

cursor c01 is
select	a.nr_sequencia,
	a.ie_periodo
from	ctb_orc_regra_empenho a
where	a.cd_empresa	= cd_empresa_w
and	nvl(a.cd_estab_exclusivo, cd_estabelecimento_p)	= cd_estabelecimento_p
and	substr(obter_se_periodo_vigente(a.dt_inicio_vigencia, nvl(a.dt_fim_vigencia, sysdate), dt_referencia_w),1,1) = 'S';

vet01	C01%RowType;

cursor	c02 is
select	a.cd_item
from	ctb_orc_regra_empenho_comp a
where	a.nr_seq_regra_emp	= nr_seq_regra_w;

vet02	C02%RowType;


BEGIN

cd_empresa_w	:= obter_empresa_estab(cd_estabelecimento_p);

select	pkg_date_utils.start_of(dt_referencia,'MONTH',0)
into	dt_referencia_w
from	ctb_mes_ref
where	nr_sequencia = nr_seq_mes_ref_p;

select	count(*)
into	qt_regra_w
from	ctb_orc_regra_empenho a
where	a.cd_empresa	= cd_empresa_w
and	nvl(a.cd_estab_exclusivo, cd_estabelecimento_p)	= cd_estabelecimento_p
and	substr(obter_se_periodo_vigente(a.dt_inicio_vigencia, nvl(a.dt_fim_vigencia, sysdate), dt_referencia_w),1,1) = 'S';

if	(qt_regra_w = 0) then
	begin


	select	nvl(sum(a.vl_item_liquido),0)
	into	vl_ordem_w
	from	ordem_compra b,
		ordem_compra_item a
	where	a.nr_ordem_compra = b.nr_ordem_compra
	and	pkg_date_utils.start_of(b.dt_ordem_compra,'MONTH',0) = dt_referencia_w
	and	b.dt_aprovacao is not null
	and	b.dt_baixa is null
	and	a.cd_conta_contabil = cd_conta_contabil_p
	and	a.cd_centro_custo = cd_centro_custo_p
	and	b.cd_estabelecimento = cd_estabelecimento_p;

	select	round(nvl(sum(a.vl_unit_previsto),0),2)
	into	vl_solicitacao_w
	from	solic_compra b,
		solic_compra_item a
	where	a.nr_solic_compra = b.nr_solic_compra
	and	pkg_date_utils.start_of(b.dt_solicitacao_compra,'MONTH',0) = dt_referencia_w
	and	b.dt_autorizacao is not null
	and	b.dt_baixa is null
	and	b.cd_conta_contabil = cd_conta_contabil_p
	and	b.cd_centro_custo = cd_centro_custo_p
	and	b.cd_estabelecimento = cd_estabelecimento_p;

	vl_empenho_w	:= nvl(vl_ordem_w,0) + nvl(vl_solicitacao_w,0);
	end;
else
	begin

	vl_empenho_w	:= 0;
	
	open C01;
	loop
	fetch C01 into	
		vet01;
	exit when C01%notfound;
		begin
		nr_seq_regra_w	:= vet01.nr_sequencia;
		nr_mes_w	:= to_number(pkg_date_utils.extract_field('MONTH', dt_referencia_w));
		nr_ano_w	:= to_number(pkg_date_utils.extract_field('YEAR', dt_referencia_w));
		
		if	(dt_inicial_p is null) and
			(dt_final_p is null) then
			begin
			if	(vet01.ie_periodo = 'M') then
				begin
				dt_inicial_w	:= pkg_date_utils.start_of(dt_referencia_w,'MONTH',0);
				dt_final_w	:= fim_mes(dt_referencia_w);
				end;
			elsif	(vet01.ie_periodo = 'A') then
				begin
				dt_inicial_w	:= pkg_date_utils.start_of(dt_referencia_w,'YEAR',0);
				dt_final_w	:= fim_ano(dt_referencia_w);
				end;
			elsif	(vet01.ie_periodo = 'S') then
				begin
				dt_final_w	:= fim_dia(pkg_date_utils.end_of(pkg_date_utils.get_date(nr_ano_w,(round((nr_mes_w+2)/6)*6),1),'MONTH',0));
				dt_inicial_w	:= pkg_date_utils.start_of(pkg_date_utils.add_month(dt_final_w,-5,0),'MONTH',0);
				end;
			elsif 	(vet01.ie_periodo = 'T') then
				begin
				dt_final_w	:= fim_dia(pkg_date_utils.end_of(pkg_date_utils.get_date(2016,(round((6+1)/3)*3),1), 'MONTH', 0));
				dt_inicial_w	:= pkg_date_utils.start_of(pkg_date_utils.add_month(dt_final_w,-2,0),'MONTH',0);
				end;
			elsif	(vet01.ie_periodo = 'B') and
				(nr_mes_w <= 2) then
				begin
				dt_final_w	:= fim_dia(pkg_date_utils.end_of(pkg_date_utils.get_date(nr_ano_w, (round((nr_mes_w+0.5)/2)*2),1), 'MONTH', 0));
				dt_inicial_w	:= pkg_date_utils.start_of(pkg_date_utils.add_month(dt_final_w,-1,0),'MONTH',0);
				end;
			end if;
			end;
		else
			dt_inicial_w	:= dt_inicial_p;
			dt_final_w	:= dt_final_p;
		end if;
		open C02;
		loop
		fetch C02 into	
			vet02;
		exit when C02%notfound;
			begin
			
			if	(vet02.cd_item	= '1') then
				begin
				select	round(nvl(sum(a.vl_unit_previsto),0),2)
				into	vl_solicitacao_w
				from	solic_compra b,
					solic_compra_item a
				where	a.nr_solic_compra	= b.nr_solic_compra
				and	a.cd_conta_contabil	= cd_conta_contabil_p
				and	b.cd_centro_custo 	= cd_centro_custo_p
				and	b.cd_estabelecimento 	= cd_estabelecimento_p
				and	b.dt_solicitacao_compra between dt_inicial_w and dt_final_w
				and	b.dt_autorizacao is not null
				and	b.dt_baixa is null
				and	b.nr_seq_motivo_cancel is null
				and	not exists(	select	1
							from	nota_fiscal_item y
							where	y.nr_solic_compra	= b.nr_solic_compra
							and	y.nr_item_solic_compra	= a.nr_item_solic_compra);
				
			
				end;
			elsif	(vet02.cd_item	= '2') then
				begin
				select	nvl(sum(c.vl_rateio),0)
				into	vl_ordem_w
				from	nota_fiscal_item_rateio c,
					nota_fiscal_item a,
					nota_fiscal b
				where	a.nr_sequencia		= b.nr_sequencia
				and	a.nr_sequencia		= c.nr_seq_nota
				and	a.nr_item_nf		= c.nr_item_nf
				and	a.cd_conta_contabil	= cd_conta_contabil_p
				and	c.cd_centro_custo 	= cd_centro_custo_p
				and	b.cd_estabelecimento 	= cd_estabelecimento_p
				and	b.dt_entrada_saida between dt_inicial_w and dt_final_w
				and	b.dt_atualizacao_estoque is not null
				and	b.ie_situacao		= '1';
				
				select	nvl(sum(a.vl_liquido),0)
				into	vl_solicitacao_w
				from	nota_fiscal_item a,
					nota_fiscal b
				where	a.nr_sequencia		= b.nr_sequencia
				and	b.cd_estabelecimento 	= cd_estabelecimento_p
				and	b.dt_entrada_saida between dt_inicial_w and dt_final_w
				and	b.dt_atualizacao_estoque is not null
				and	a.cd_conta_contabil	= cd_conta_contabil_p
				and	a.cd_centro_custo 	= cd_centro_custo_p
				and	b.ie_situacao		= '1'
				and	not exists(	select	1
							from	nota_fiscal_item_rateio c
							where	c.nr_seq_nota	= a.nr_sequencia
							and	c.nr_seq_nota	= b.nr_sequencia
							and	c.nr_item_nf	= a.nr_item_nf);
				
				vl_solicitacao_w	:= vl_solicitacao_w + vl_ordem_w;
				
				end;
			elsif	(vet02.cd_item	= '3') then
				begin
				select	nvl(sum(b.vl_titulo),0)
				into	vl_solicitacao_w
				from	titulo_pagar a,
					titulo_pagar_classif b
				where	a.nr_titulo		= b.nr_titulo
				and	b.cd_conta_contabil	= cd_conta_contabil_p
				and	b.cd_centro_custo	= cd_centro_custo_p
				and	a.cd_estabelecimento	= cd_estabelecimento_p
				and	a.dt_emissao between dt_inicial_w and dt_final_w
				and	a.nr_seq_nota_fiscal is null
				and	a.ie_situacao		<> 'C';
				
				end;
			elsif	(vet02.cd_item	= '4') then	
				begin
				
				select	nvl(sum(a.qt_estoque * obter_custo_medio_material(b.cd_estabelecimento, dt_referencia_w, a.cd_material)),0)
				into	vl_solicitacao_w
				from	requisicao_material b,
					item_requisicao_material a
				where	a.nr_requisicao		= b.nr_requisicao
				and	a.cd_conta_contabil	= cd_conta_contabil_p
				and	b.cd_centro_custo 	= cd_centro_custo_p
				and	b.cd_estabelecimento 	= cd_estabelecimento_p
				and	b.dt_solicitacao_requisicao between dt_inicial_w and dt_final_w
				and	b.dt_liberacao is not null;
				end;
			end if;
			vl_empenho_w	:= vl_empenho_w	+ vl_solicitacao_w;
			end;
		end loop;
		close C02;
		
		end;
	end loop;
	close C01;
	end;
end if;

return	vl_empenho_w;

END ctb_obter_empenho_orcamento;
/