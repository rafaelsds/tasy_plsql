create or replace function obter_info_equip_man(	nr_seq_equipamento_p	number,
														ie_info_p varchar2)
														return varchar2 is
dt_select_w			date;
ds_select_w			varchar2(255);

return_w				varchar2(255) := '';	
										
begin

/*
FG 	- Informa��o se fim garantia
DE 	- Informa��o se � descarte
DDE - Descri��o status descarte
SM 	- Informa��o se n�o tem possibilidade de manuten��o
DSM - Descri��o dos esquipamentos sem possibilidade de manuten��o
DA  - Informa��o se equipamento danificado
DDA - Descri��o do dano
*/

if (ie_info_p = 'FG') then

	select	max(dt_fim_garantia)
	into	dt_select_w
	from	man_equipamento a
	where	nr_sequencia = nr_seq_equipamento_p;
	
	if (sysdate > dt_select_w) then
		 return_w := 'S';
	end if;
	
elsif(ie_info_p = 'DE') or (ie_info_p = 'DDE') then
	
	select 	nvl(max(c.ie_descarte),'N'),
			nvl(max(c.ds_status),'')
	into 	return_w,
			ds_select_w
	from	equipamento a,
			man_equipamento b,
			man_equip_status c
	where 	a.cd_equipamento = nr_seq_equipamento_p
	and 	a.nr_seq_equipamento_man = b.nr_sequencia
	and 	b.nr_seq_status = c.nr_sequencia
	and		c.ie_descarte = 'S';
	
	if (ie_info_p = 'DDE') then
		return_w := ds_select_w;
	end if;
	
elsif(ie_info_p = 'SM') or (ie_info_p = 'DSM') then

	select 	PKG_DATE_UTILS.ADD_MONTH(b.dt_aquisicao, b.qt_tempo_vida_util),
			a.ds_equipamento
	into 	dt_select_w, 
			ds_select_w
	from	equipamento a,
			man_equipamento b
	where 	a.cd_equipamento = nr_seq_equipamento_p
	and 	a.nr_seq_equipamento_man = b.nr_sequencia;
	
	if (sysdate > dt_select_w) and (ie_info_p = 'SM') then
		return_w := 'S';
	elsif (sysdate > dt_select_w) and (ie_info_p = 'DSM') then
		return_w := ds_select_w;
	end if;

elsif(ie_info_p = 'DA') then
	select 	decode(max(1),1,'S','N') 
	into 	return_w
	from 	equipamento a,
			man_equipamento b,
			man_tipo_equipamento c,
			man_planej_prev d,
			man_causa_dano e
	where 	a.cd_equipamento = nr_seq_equipamento_p
	and 	a.nr_seq_equipamento_man = b.nr_sequencia
	and 	b.nr_seq_tipo_equip  = c.nr_sequencia
	and		d.nr_seq_tipo_equip  = c.nr_sequencia
	and 	d.nr_seq_causa_dano = e.nr_sequencia;
	
elsif(ie_info_p = 'DDA') then	
	select 	nvl(max(e.ds_causa), '') 
	into 	return_w
	from 	equipamento a,
			man_equipamento b,
			man_tipo_equipamento c,
			man_planej_prev d,
			man_causa_dano e
	where 	a.cd_equipamento = nr_seq_equipamento_p
	and 	a.nr_seq_equipamento_man = b.nr_sequencia
	and 	b.nr_seq_tipo_equip  = c.nr_sequencia
	and		d.nr_seq_tipo_equip  = c.nr_sequencia
	and 	d.nr_seq_causa_dano = e.nr_sequencia
	and 	e.ie_situacao = 'A';
end if;
	
return return_w;
	
end obter_info_equip_man;
/