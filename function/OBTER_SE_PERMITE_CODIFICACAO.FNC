create or replace FUNCTION OBTER_SE_PERMITE_CODIFICACAO (CD_DOENCA_CID_P Varchar2)
				return Varchar2 is

ds_retorno_w			Varchar2(2 char);

BEGIN

if ((CD_DOENCA_CID_P is not null ) and (nvl(pkg_i18n.get_user_locale, 'pt_BR') in ('de_AT'))) then
	select nvl2(max(DS_DOENCA_CID), 'N', 'S')
    into ds_retorno_w
	from CID_DOENCA
	where cd_doenca_cid = upper( CD_DOENCA_CID_P )
	and (ie_codificacao = 'N')
	order by 1;
else
	ds_retorno_w := 'S';
end if;

return ds_retorno_w;

END OBTER_SE_PERMITE_CODIFICACAO;
/
