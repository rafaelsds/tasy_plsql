create or replace
function hnsg_obter_data_etapa_presc(nr_prescricao_p		number,
				nr_seq_prescr_p		number,
				vl_dominio_p		number)
 		    	return date is


data_etapa_w	date;

			
begin

select 	max(a.dt_atualizacao)
into	data_etapa_w
from	prescr_proc_etapa a
where	a.nr_prescricao = nr_prescricao_p
and	a.nr_seq_prescricao  = nr_seq_prescr_p
and	a.ie_etapa 	= vl_dominio_p;

return	data_etapa_w;

end hnsg_obter_data_etapa_presc;
/