create or replace
function obter_se_quest_aval_lib_usu(	nr_seq_questionario_p		number,
				nm_usuario_p			varchar2)
	return varchar2 is

ie_liberado_w		varchar2(1) := 'N';
nr_seq_mat_aval_w		number(10);
cd_pessoa_usuario_w	varchar2(10);
cd_pessoa_resp_aval_w	varchar2(10);
cd_pessoa_resp_ques_w	varchar2(10);
qt_registro_w		number(10);
ie_permite_resp_setor_w 	varchar2(1);
			
begin

ie_permite_resp_setor_w := obter_valor_param_usuario(4100, 20, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo);

select	obter_pessoa_fisica_usuario(nm_usuario_p,'C')
into	cd_pessoa_usuario_w	
from	dual;

select	nr_seq_mat_aval, cd_resp_quest
into	nr_seq_mat_aval_w, cd_pessoa_resp_ques_w
from	mat_aval_quest
where	nr_sequencia = nr_seq_questionario_p;

if	(cd_pessoa_resp_ques_w = cd_pessoa_usuario_w) then
	ie_liberado_w := 'S';
else 
	if	(nr_seq_mat_aval_w > 0) then
	
		select	cd_pessoa_resp
		into	cd_pessoa_resp_aval_w
		from	mat_avaliacao
		where	nr_sequencia = nr_seq_mat_aval_w;
	
		if	(cd_pessoa_resp_aval_w = cd_pessoa_usuario_w) then
			ie_liberado_w := 'S';
		else
			if (ie_permite_resp_setor_w = 'S') then
				select	count(*)
				into	qt_registro_w
				from	mat_avaliacao_setor
				where	nr_seq_mat_aval 		= nr_seq_mat_aval_w
				and	cd_pessoa_avaliadora 	= cd_pessoa_usuario_w;
			end if;
		
			if	(qt_registro_w > 0) then
				ie_liberado_w := 'S';
			end if;
		end if;
	end if;
end if;

return	ie_liberado_w;

end obter_se_quest_aval_lib_usu;
/