CREATE OR REPLACE FUNCTION fis_mascara_telefone ( p_telefone VARCHAR2) RETURN VARCHAR2 IS 

v_telefone           VARCHAR2(20); 
v_telefone_formatado VARCHAR2(20); 
 
BEGIN  
    v_telefone            := elimina_caracteres_telefone(p_telefone);
    IF(LENGTH(v_telefone)> 10)THEN
      v_telefone_formatado :=  SUBSTR(v_telefone,(LENGTH(v_telefone)-9),LENGTH(v_telefone) );
    ELSE 
      v_telefone_formatado  := LPAD(v_telefone,10,0);
    END IF;
   
    RETURN(v_telefone_formatado); 
END fis_mascara_telefone;
/