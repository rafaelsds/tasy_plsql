create or replace
function Obter_Desc_Local_Refeicao	(	nr_seq_local_ref_p	number)
										return varchar2 is

ds_local_ref_w	varchar2(90)	:= '';

begin

if	(nr_seq_local_ref_p is not null) then
	begin

	select	ds_local_ref
	into	ds_local_ref_w
	from	tipo_local_refeicao
	where	nr_sequencia	= nr_seq_local_ref_p;

	end;
end if;

return	ds_local_ref_w;

end Obter_Desc_Local_Refeicao;
/