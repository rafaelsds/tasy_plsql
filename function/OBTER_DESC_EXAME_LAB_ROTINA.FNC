create or replace
function Obter_desc_exame_lab_rotina(nr_seq_exame_p	number)
 		    	return varchar2 is

ds_prescricao_w	varchar2(255);
			
begin

select	max(ds_prescricao)
into	ds_prescricao_w
from   	exame_lab_rotina y
where	nr_seq_exame	= nr_seq_exame_p;
	
return	ds_prescricao_w;

end Obter_desc_exame_lab_rotina;
/