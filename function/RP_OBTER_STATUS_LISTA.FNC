create or replace
function rp_obter_status_lista(nr_seq_lista_p  number,
				ie_opcao_p  varchar2)
				return varchar2 is
ds_retorno_w varchar2(255);
begin

if (nr_seq_lista_p is not null) then
	if (ie_opcao_p = 'D') then
	
		select	decode(ie_status,'A',wheb_mensagem_pck.get_texto(803027),'T',wheb_mensagem_pck.get_texto(803030),'C',wheb_mensagem_pck.get_texto(309479),'B',wheb_mensagem_pck.get_texto(795187),'')
		into	ds_retorno_w
		from	RP_LISTA_ESPERA_MODELO
		where	nr_sequencia = nr_seq_lista_p;
		
	elsif (ie_opcao_p = 'C') then
	
		select	max(ie_status)
		into	ds_retorno_w
		from	RP_LISTA_ESPERA_MODELO
		where	nr_sequencia = nr_seq_lista_p;
		
	end if;
end if;

return ds_retorno_w;

end rp_obter_status_lista;
/
