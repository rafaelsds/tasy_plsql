create or replace
function	cm_obter_desc_motivo_cancel(
		nr_seq_motivo_cancel_p		number)
		return varchar2 is

ds_retorno_w		varchar2(255);

begin

select	ds_motivo
into	ds_retorno_w
from	cm_motivo_cancel_ciclo
where	nr_sequencia = nr_seq_motivo_cancel_p;

return	ds_retorno_w;

end cm_obter_desc_motivo_cancel;
/