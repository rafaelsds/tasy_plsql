create or replace
function tre_obter_ultima_data_presenca (nr_seq_evento_p	Number)
 		    	return date is

dt_inicio_real_w	date;
dt_fim_real_w		date;
qt_carga_horaria_w	number(13,2);
hr_inicial_w		date;
ht_final_w		date;
ie_dia_semana_w		number(1);
dt_atual_w		date;
ie_dia_atual_w		number(1);
nr_seq_agenda_w		number(10);
dt_alteracao_w		date;

Cursor C01 is
	select	to_date(to_Char(c.hr_inicial,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
		to_date(to_Char(c.hr_final,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),		
		to_number(c.ie_dia_semana)
	from	tre_agenda_turno c
	where	c.nr_seq_agenda_tre = nr_seq_agenda_w
	and	((dt_atual_w between c.dt_inicio_vigencia and fim_dia(c.dt_fim_vigencia)) or ((c.dt_inicio_vigencia is null) or (c.dt_fim_vigencia is null)));

begin


if	( nr_seq_evento_p is not null) then		
	begin

	select	a.dt_inicio_real,
                a.dt_fim_real,
                nvl(a.qt_carga_horaria,0),
                a.nr_seq_agenda
	into	dt_inicio_real_w,
		dt_fim_real_w,
		qt_carga_horaria_w,
		nr_seq_agenda_w
	from	tre_evento a 
	where	a.nr_sequencia = nr_seq_evento_p
	and     rownum = 1;

	select	max(dt_alteracao)
	into	dt_alteracao_w
	from	tre_alteracao_dia_aula
	where	nr_seq_agenda = nr_seq_agenda_w
	and	dt_liberacao is not null
	and	ie_acao = 'A';



	dt_atual_w := dt_inicio_real_w;
	while	(qt_carga_horaria_w > 0) and (dt_atual_w < dt_fim_real_w ) loop

		select pkg_date_utils.get_WeekDay(dt_atual_w) --Busca como dia da semana
		into	ie_dia_atual_w
		from 	dual;

		open C01;
		loop
		fetch C01 into	
			hr_inicial_w,
			ht_final_w,
			ie_dia_semana_w ;
		exit when C01%notfound;
			begin	-- compara os dias da semana. Se ie_dia_semana = 9, traz apenas os dias de trabalho
			if ((ie_dia_atual_w = ie_dia_semana_w) or ((ie_dia_semana_w = 9) and (pkg_date_utils.IS_BUSINESS_DAY(dt_atual_w) = 1))) and (obter_se_feriado(obter_estabelecimento_ativo, dt_atual_w) = 0) then			
			
				qt_carga_horaria_w := qt_carga_horaria_w - to_number(((ht_final_w - hr_inicial_w)*24));
			end if;
			end;
		end loop;
		close C01;

		dt_atual_w := trunc(dt_atual_w) + 1;		

		end loop;
		end;
	end if;

if	(dt_alteracao_w > dt_atual_w) then	
	dt_atual_w := trunc(dt_alteracao_w+1);
end if;

return	dt_atual_w-1;

end tre_obter_ultima_data_presenca;
/
