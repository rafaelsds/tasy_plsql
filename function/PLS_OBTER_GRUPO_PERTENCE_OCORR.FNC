create or replace
function pls_obter_grupo_pertence_ocorr
			(	nr_seq_ocorrencia_p	number,
				nr_seq_grupo_p		number)
				return varchar2 is
				
ie_permissao_w			varchar2(1);
qt_grupo_w			number(5);
ds_retorno_w			varchar2(1) := 'N';
qt_ocorrencia_informativa_w	number(5);

begin

if	(nvl(nr_seq_ocorrencia_p,0) <> 0) then
	select	max(ie_permissao)
	into	ie_permissao_w
	from	pls_grupo_auditor
	where	nr_sequencia = nr_seq_grupo_p;	

	if	(ie_permissao_w in('T','R')) then
		ds_retorno_w := 'S';
	end if;		  

	if	(ds_retorno_w <> 'S') then
		select	count(1)
		into	qt_ocorrencia_informativa_w
		from	pls_ocorrencia
		where	nr_sequencia	= nr_seq_ocorrencia_p
		and	ie_auditoria	= 'N'
		and	ie_situacao	= 'A';
		
		if	(qt_ocorrencia_informativa_w = 0) then
			select  count(1)
			into	qt_grupo_w
			from 	pls_ocorrencia_grupo
			where 	nr_seq_ocorrencia 	= nr_seq_ocorrencia_p
			and	nr_seq_grupo		= nr_seq_grupo_p 
			and	ie_situacao = 'A';

			if	(qt_grupo_w > 0) then
				ds_retorno_w := 'S';
			end if;		
		else
			ds_retorno_w := 'S';
		end if;
	end if;
else
	ds_retorno_w := 'S';
end if;


return	ds_retorno_w;

end pls_obter_grupo_pertence_ocorr;
/
