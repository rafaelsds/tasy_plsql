create or replace
function Sus_Obter_Se_Cid_Compativel(
					cd_doenca_cid_p		Varchar2,
					cd_procedimento_p	Number)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(1)	:= 'N';
qt_proc_compat_w	Number(5)	:= 0;
			
begin

select	count(*)
into	qt_proc_compat_w
from	sus_procedimento_cid
where	cd_procedimento	= cd_procedimento_p
and	cd_doenca_cid	= cd_doenca_cid_p;

if	(qt_proc_compat_w > 0) then
	begin
	ds_retorno_w	:= 'S';
	end;
end if;

return	ds_retorno_w;

end Sus_Obter_Se_Cid_Compativel;
/