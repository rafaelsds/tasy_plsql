create or replace
function obter_dt_imposto_tit_pagar(nr_titulo_p		number, 
					cd_tributo_p	number)
 		    	return date is

dt_retorno_w		date;
			
begin

select 	to_char(max(dt_imposto),'DD/MM/YYYY')
into 	dt_retorno_w
from 	titulo_pagar_imposto
where	nr_titulo = nr_titulo_p
and	cd_tributo = cd_tributo_p;

return	dt_retorno_w;

end obter_dt_imposto_tit_pagar;
/