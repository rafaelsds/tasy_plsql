create or replace
function obter_nome_nao_padrao_mat(cd_material_p		material.cd_material%type,
				   nr_atendimento_p		atendimento_paciente.nr_atendimento%type)
 		    	return varchar2 is

ds_material_w 	material.ds_material%type := '';
			
begin

if ((cd_material_p is not null) and (nr_atendimento_p is not null)) then

	select 	max(substr(ds_medic_nao_padrao,1,255))
	into 	ds_material_w
	from 	cpoe_material
	where 	cd_material = cd_material_p
	and	nr_atendimento = nr_atendimento_p;
	
end if;

return	ds_material_w;

end obter_nome_nao_padrao_mat;
/