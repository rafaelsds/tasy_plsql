create or replace
function mprev_obter_dados_partic(	nr_seq_partic_p		mprev_participante.nr_sequencia%type,
					dt_referencia_p		date,
					ie_opcao_p		varchar2,
					cd_pf_partic_p		mprev_participante.cd_pessoa_fisica%type default null
					)
					return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar diferentes informacoes com relacao ao participante da medicina preventiva.
Usada em campos function do dicionario de dados.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: MedPrev - Programas de Promocao a Saude
[  ]  Objetos do dicionario [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:

ie_opcao_p
'PR' = Retorna os programas em que o participante esta ativo
'CR' = Retorna o numero da carteirinha do participante
'CN' = Retorna as campanhas ativas do participante
'DI' = Retorna a data de inclusao do partucipante no programa.
'I' - Retorna a idade da pessoa
'S'- Retorna o sexo da pessoa
'T' - Retorna telefone da pessoa
'AD' - Area de atendimento domiciliar.
'TA' - Tipo de atendimento
'PF' - Codigo pessoa fisica
'TRC' - Numero telefone residencial completo. Com DDD.
'TCC'- Numero celular completo com DDD. 
'FA' - Forma de atendimento. "Cadastro do participante, pasta Forma de atendimento".
'NC'  = Retorna o nome do cuidador.
'NCL' = Retorna uma lista de nomes de cuidadores.
'GA' = Retorna os grupos de atendimento em que o participante esta dentro
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(4000);
cd_pessoa_fisica_w	mprev_participante.cd_pessoa_fisica%type;
ie_opcao_w		varchar2(3);
nr_seq_segurado_w	pls_segurado.nr_sequencia%type;
ie_origem_dados_w	varchar2(1);
cd_convenio_w		convenio.cd_convenio%type;
nr_seq_partic_w		mprev_participante.nr_sequencia%type;

cursor C01(	nr_seq_partic_pc	mprev_participante.nr_sequencia%type) is
	select	c.nm_campanha
	from	mprev_campanha_partic p,
		mprev_campanha c
	where	p.nr_seq_participante = nr_seq_partic_pc
	and	p.dt_inclusao <= sysdate
	and	(p.dt_exclusao is null or p.dt_exclusao > sysdate)
	and	c.nr_sequencia = p.nr_seq_campanha;
	
cursor C02(	nr_seq_partic_pc	mprev_participante.nr_sequencia%type) is
	select	c.nm_programa
	from	mprev_programa_partic p,
		mprev_programa c
	where	p.nr_seq_participante = nr_seq_partic_pc
	and	p.dt_inclusao <= sysdate
	and	(p.dt_exclusao is null or p.dt_exclusao > sysdate)
	and	c.nr_sequencia = p.nr_seq_programa;

cursor C03(	nr_seq_partic_pc	mprev_participante.nr_sequencia%type) is
	select  cd_pessoa_cuidador
	from    mprev_partic_cuidador
        where   nr_seq_participante = nr_seq_partic_pc
	and     dt_referencia_p between inicio_dia(dt_inicio) and fim_dia(nvl(dt_fim,sysdate+999));

cursor C04(	nr_seq_partic_pc	mprev_participante.nr_sequencia%type) is
	select	substr(mprev_obter_dados_grupo_turma(a.nr_seq_turma, 'NG'),1,255) || ' - '|| substr(mprev_obter_dados_grupo_turma(a.nr_seq_turma, 'N'),1,255) nm_turma
	from	mprev_grupo_turma_partic a
	where	a.nr_seq_participante = nr_seq_partic_pc
	and		sysdate between pkg_date_utils.start_of(a.dt_entrada, 'DAY') and pkg_date_utils.end_of(nvl(a.dt_saida, sysdate), 'DAY');

begin

if (nr_seq_partic_p is null and cd_pf_partic_p is not null) then
	cd_pessoa_fisica_w := cd_pf_partic_p;

	select	max(nr_sequencia)
	into	nr_seq_partic_w
	from	mprev_participante
	where	cd_pessoa_fisica = cd_pf_partic_p;
elsif (cd_pf_partic_p is null and nr_seq_partic_p is not null) then
	nr_seq_partic_w := nr_seq_partic_p;

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	mprev_participante
	where	nr_sequencia = nr_seq_partic_p;
end if;

ie_opcao_w := upper(ie_opcao_p);

-- Campanhas ativas do participante
if	(ie_opcao_w = 'CN')then

	for	rw_c01_w in C01(nr_seq_partic_p) loop

		if	(ds_retorno_w is not null) then
		
			ds_retorno_w := substr(ds_retorno_w || ', ' || rw_c01_w.nm_campanha, 1, 255);
			exit when length (ds_retorno_w) >= 254;
		else
			ds_retorno_w := substr(rw_c01_w.nm_campanha, 1, 255);
		end if;
	end loop;
	
-- Numero da carteirinha do participante
elsif	(ie_opcao_w = 'CR') then

		ie_origem_dados_w := mprev_obter_origem_dados;
		
		if	(ie_origem_dados_w = 'O') then
			nr_seq_segurado_w	:= mprev_obter_benef_partic(null,cd_pessoa_fisica_w);
			if	(nr_seq_segurado_w is not null) then
				ds_retorno_w	:= substr(pls_obter_dados_segurado(nr_seq_segurado_w,'C'), 1, 255);
			end if;
		elsif	(ie_origem_dados_w = 'P') then
			cd_convenio_w	:= obter_convenio_estab(wheb_usuario_pck.get_cd_estabelecimento);
			ds_retorno_w	:= substr(obter_dados_titular_convenio(cd_pessoa_fisica_w,cd_convenio_w,'CUV'), 1, 255);
		end if;
	
-- Programas em que o participante esta ativo
elsif	(ie_opcao_w = 'PR') then
	for	rw_c02_w in C02(nr_seq_partic_w) loop

		if	(ds_retorno_w is not null) then

			ds_retorno_w := substr(ds_retorno_w || ', ' || rw_c02_w.nm_programa, 1, 255);
			exit when length (ds_retorno_w) >= 254;

		else
			ds_retorno_w := substr(rw_c02_w.nm_programa, 1, 255);
		end if;
	end loop;
	
-- Data de inclusao do partucipante no programa.
elsif	(ie_opcao_w = 'DI') then

	select	to_char(min(x.dt_inclusao), 'dd/mm/yyyy')
	into	ds_retorno_w
	from	mprev_programa_partic x
	where	x.nr_seq_participante = nr_seq_partic_p;
	
-- Idade
elsif	(ie_opcao_w = 'I') then

	ds_retorno_w	:= obter_dados_pf(cd_pessoa_fisica_w,'I');
	
-- Sexo	
elsif	(ie_opcao_w = 'S') then

	ds_retorno_w	:= obter_dados_pf(cd_pessoa_fisica_w,'SE');
-- Telefone	
elsif	(ie_opcao_w = 'T') then

	ds_retorno_w	:= obter_telefone_pf(cd_pessoa_fisica_w,1);
	
-- Area atendimento domicilio.
elsif	(ie_opcao_w = 'AD') then
	
	select	substr(pls_admin_cursor.obter_desc_cursor (
		cursor (select	area.ds_area
			from	mprev_partic_tipo_atend atend
			,	mprev_area_atend_domic area
			where   atend.nr_seq_participante = nr_seq_partic_p
			and     atend.dt_inicio <= sysdate 
			and     (atend.dt_fim is null or atend.dt_fim >= sysdate)
			and	area.nr_sequencia = atend.nr_seq_area_atendimento
		)
		,	','
	), 1, 255)
	into	ds_retorno_w
	from	dual;
elsif	(ie_opcao_w = 'TA') then
	select	obter_valor_dominio(5987, nvl(max(ie_tipo_atendimento), 'P'))
	into	ds_retorno_w
	from	mprev_partic_tipo_atend
	where	nr_seq_participante	= nr_seq_partic_p
	and	dt_inicio	<= dt_referencia_p
	and 	(dt_fim	>= dt_referencia_p or dt_fim is null);
-- Retorna o codigo da pessoa fisica
elsif	(ie_opcao_w = 'PF') then
	select	cd_pessoa_fisica
	into	ds_retorno_w
	from	mprev_participante a
	where	a.nr_sequencia	= nr_seq_partic_p;
--Retorna telefone residencial compelato com numero de DDD.
elsif	(ie_opcao_w = 'TRC') then
	ds_retorno_w	:= obter_telefone_pf(cd_pessoa_fisica_w,4);
--Retorna numero do telefone celular com DDD.
elsif	(ie_opcao_w = 'TCC') then
	ds_retorno_w	:= obter_telefone_pf(cd_pessoa_fisica_w,12);
--FA - Forma de atendimento. "Cadastro do participante, pasta Forma de atendimento".
elsif	(ie_opcao_w = 'FA') then
	select	decode(a.ie_tipo_atendimento,'P','L',a.ie_tipo_atendimento)
	into	ds_retorno_w
	from	mprev_partic_tipo_atend a
	where	a.nr_seq_participante = nr_seq_partic_p
	and	trunc(dt_inicio) <= trunc(dt_referencia_p)
	and	(trunc(dt_fim) >= trunc(dt_referencia_p) or dt_fim is null)
	and	rownum	= 1;
--'NC' = Retorna o nome do cuidador.
elsif	(ie_opcao_w = 'NC') then
	select	max(obter_nome_pf(cd_pessoa_cuidador))
	into	ds_retorno_w
	from	mprev_partic_cuidador a
	where	a.nr_seq_participante = nr_seq_partic_p
	and	sysdate between dt_inicio and nvl(dt_fim,sysdate);
	
elsif (ie_opcao_w = 'NCL') then
	for	rw_c0c3_w in C03(nr_seq_partic_p) loop

		if	(ds_retorno_w is not null) then
		
			ds_retorno_w := substr(ds_retorno_w || '/ ' || obter_nome_pf(rw_c0c3_w.cd_pessoa_cuidador), 1, 255);
			exit when length (ds_retorno_w) >= 254;
			
		else
			ds_retorno_w := substr(obter_nome_pf(rw_c0c3_w.cd_pessoa_cuidador), 1, 255);
		end if;
	end loop;

-- Grupos de atendimento em que o participante esta dentro
elsif	(ie_opcao_w = 'GA') then
	for	rw_c04_w in C04(nr_seq_partic_w) loop
		if	(ds_retorno_w is not null) then
			ds_retorno_w := substr(ds_retorno_w || ', ' || rw_c04_w.nm_turma, 1, 4000);
			exit when length (ds_retorno_w) >= 3999;
		else
			ds_retorno_w := substr(rw_c04_w.nm_turma, 1, 4000);
		end if;
	end loop;
end if;

return	ds_retorno_w;

end mprev_obter_dados_partic;
/
