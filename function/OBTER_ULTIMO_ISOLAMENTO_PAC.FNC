create or replace
function obter_ultimo_isolamento_pac(	cd_pessoa_fisica_p		varchar2,
					nm_usuario_p			varchar2)
 		    	return number is

nr_atendimento_w	number(10);
nr_atend_isol_w		number(10);
qt_dias_isolamento_w	number(10);
			
qt_isolamento_w		number(10);
			
cursor c01 is
	select	nr_atendimento
	from	atendimento_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;			
			
begin

obter_param_usuario(916, 400, obter_perfil_ativo, nm_usuario_p, 0, qt_dias_isolamento_w);


if	(cd_pessoa_fisica_p is not null) then

	open c01;
	loop
	fetch c01 into
		nr_atendimento_w;
	exit when c01%notfound;
		begin
		
		select 	max(b.nr_atendimento)
		into	qt_isolamento_w
		from	motivo_isolamento a,
			motivo_isolamento_atend b
		where	a.nr_sequencia = b.nr_seq_motivo_isol
		and	b.nr_atendimento	= nr_atendimento_w
		and	nvl(ie_gerar_alerta,'N') = 'S'
		and 	(sysdate - b.dt_inicio_isolamento ) <= qt_dias_isolamento_w;
		
		
		if (qt_isolamento_w > 0) then
			
			nr_atend_isol_w := nr_atendimento_w;
		
		end if;
		
		
		end;
	end loop;
	close c01;
	
	
	
end if;


return	nr_atend_isol_w;

end obter_ultimo_isolamento_pac;
/
