create or replace
function Obter_Horarios_suspensos_REP(nr_prescricao_p       number,
                                  nr_seq_material_p     number)
                        return varchar2 is

ds_horarios_w           varchar2(255);
dt_horario_w            varchar2(255);

cursor c01 is
select  to_char(dt_horario,'hh24:mi')
from    prescr_mat_hor
where   nr_prescricao   = nr_prescricao_p
and     nr_seq_material = nr_seq_material_p
and     dt_suspensao is not null
and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

begin

open C01;
loop
fetch C01 into
        dt_horario_w;
exit when C01%notfound;
        ds_horarios_w   := ds_horarios_w || ' ' || dt_horario_w;
end loop;
close C01;

return  ds_horarios_w;

end Obter_Horarios_suspensos_REP;
/