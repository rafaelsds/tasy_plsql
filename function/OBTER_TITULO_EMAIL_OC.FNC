create or replace
function obter_titulo_email_oc(	nr_ordem_compra_p		number,
				ie_opcao_p			number)
 		    	return varchar2 is

/*ie_opcao
0               Ordem de compra + Nr ordem
1               Ordem de compra + Nr ordem + Nm fornecedor*/

ds_fornecedor_w			varchar2(100);
ds_retorno_w			varchar2(255);
	
begin

select	substr(obter_nome_pf_pj(cd_pessoa_fisica, cd_cgc_fornecedor),1,100)
into	ds_fornecedor_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

if	(ie_opcao_p = 0) then
	ds_retorno_w := substr( wheb_mensagem_pck.get_texto(298461) || ' ' || nr_ordem_compra_p,1,255);
elsif	(ie_opcao_p = 1) then
	ds_retorno_w := substr( wheb_mensagem_pck.get_texto(298461) || ' ' || nr_ordem_compra_p || ' - ' || ds_fornecedor_w,1,255);
end if;

return	ds_retorno_w;

end obter_titulo_email_oc;
/