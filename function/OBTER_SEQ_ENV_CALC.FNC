create or replace
function obter_seq_env_calc(	nr_seq_envio_convenio_p Number,
				cd_convenio_p           Number)
				return Varchar2 is
ds_retorno_w    Varchar2(10);

begin

select	mod(nr_seq_envio_convenio_p, nvl(nr_multiplo_envio ,0))
into	ds_retorno_w
from	convenio
where	cd_convenio = cd_convenio_p;

return	ds_retorno_w;

end obter_seq_env_calc;
/