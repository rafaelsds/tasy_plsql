create or replace
function Obter_se_generico_regra_interv( nr_seq_regra_p  number,
					cd_material_p  number) return varchar2 is
ds_retorno_w 	varchar2(1);

begin

select	nvl(max('S'),'N')
into	ds_retorno_w
from	regra_interv_sol_generico
where	nr_seq_regra = nr_seq_regra_p
and	cd_material = cd_material_p;

return ds_retorno_w;

end Obter_se_generico_regra_interv;
/
