Create or replace
Function Obter_data_reap_hist (	nr_sequencia_p	number)
					return date is

qt_dias_reapresentacao_w	number(3);
cd_convenio_w			number(5);
dt_historico_w			date;
dt_resultado_w			date;
cd_estabelecimento_w		number(10);
qt_dias_reap_estab_w		number(10);

begin

cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

select	b.dt_historico
into	dt_historico_w
from	hist_audit_conta_paciente a,
	conta_paciente_ret_hist b
where	a.nr_sequencia		= b.nr_seq_hist_audit
and	a.ie_acao		= 1
and	b.nr_sequencia		= nr_sequencia_p;

dt_resultado_w	:= null;

if	(dt_historico_w is not null) then

	select	a.cd_convenio
	into	cd_convenio_w
	from	conta_paciente_retorno a,
		conta_paciente_ret_hist b
	where	a.nr_sequencia	= b.nr_seq_conpaci_ret
	and	b.nr_sequencia	= nr_sequencia_p;
	
	select	nvl(max(a.qt_dias_reapre),0)
	into	qt_dias_reap_estab_w
	from	convenio_estabelecimento a
	where	a.cd_convenio		= cd_convenio_w
	and	a.cd_estabelecimento	= cd_estabelecimento_w;

	select	nvl(nvl(qt_dias_reap_estab_w,qt_dias_reapresentacao),0)
	into	qt_dias_reapresentacao_w
	from	convenio
	where	cd_convenio	= cd_convenio_w;

	dt_resultado_w	:= dt_historico_w + qt_dias_reapresentacao_w;
end if;

return	dt_resultado_w;

end Obter_data_reap_hist;
/