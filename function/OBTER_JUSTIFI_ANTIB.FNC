create or replace
function Obter_Justifi_Antib(
                nr_atendimento_p                Number,
                cd_material_p                   Number)
                return varchar2 is



ds_retorno_w                    varchar2(2000)  := null;
cd_material_generico_w          number;
nr_seq_ficha_tecnica_w          number;

Begin

select	max(nr_seq_ficha_tecnica),
	max(cd_material_generico)
into	nr_seq_ficha_tecnica_w,
	cd_material_generico_w
from	material
where	cd_material = cd_material_p;

SELECT  MAX(a.ds_justificativa)
into	ds_retorno_w
FROM    prescr_material a,
	prescr_medica b
WHERE	a.nr_prescricao   = b.nr_prescricao
aND	b.nr_atendimento  = nr_atendimento_p
AND 	a.ie_suspenso     <> 'S'
and   	a.nr_dia_util 	= 1
and	b.dt_prescricao between sysdate - 20 and sysdate
and   	nvl(b.dt_liberacao_medico, b.dt_liberacao) is not null
and	a.cd_material     in
			(       select  cd_material_generico_w
				from    dual
				union
				select  cd_material_p
				from    dual
				union
				select  cd_material
				from    material
				where   cd_material_generico    = cd_material_generico_w
				union
				select  cd_material
				from    material
				where   nr_seq_ficha_tecnica    = nr_seq_ficha_tecnica_w);




return substr(ds_retorno_w,1,2000);

end Obter_Justifi_Antib;
/
