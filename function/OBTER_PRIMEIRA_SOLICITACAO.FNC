create or replace
function obter_primeira_solicitacao(nr_sequencia_p	number)
 		    	return varchar2 is

ds_retorno_w			varchar2(255);
ie_status_w 			varchar2(2);
cd_unidade_basica_w		varchar2(255);
cd_unidade_compl_w		varchar2(255);
cd_setor_desejado_w		number(10);
nr_sequencia_w			number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_paciente_reserva_w		varchar2(10);

begin	

begin
	select 	max(cd_setor_atendimento),
		max(cd_unidade_basica),
		max(cd_unidade_compl),
		max(obter_pessoa_atendimento(nr_atendimento,'C')),
		max(cd_paciente_reserva)
	into	cd_setor_desejado_w,
		cd_unidade_basica_w,
		cd_unidade_compl_w,
		cd_pessoa_fisica_w,
		cd_paciente_reserva_w
	from   	ocupacao_unidade_v
	where  	nr_seq_interno = nr_sequencia_p;
	
	if (cd_paciente_reserva_w is not null) then
		select	substr(obter_desc_status_gv(a.IE_STATUS,'D'),1,255)
		into	ds_retorno_w
		from	gestao_vaga a
		where	a.dt_prevista = (	select min(b.dt_prevista)
						from 	gestao_vaga b
						where 	trunc(b.dt_prevista) >= trunc(sysdate)
						and	b.cd_pessoa_fisica  = cd_paciente_reserva_w
						and   	b.cd_unidade_basica = cd_unidade_basica_w
						and   	b.cd_unidade_compl  = cd_unidade_compl_w
						and   	b.cd_setor_desejado = cd_setor_desejado_w);
	end if;
exception
when others then
	ds_retorno_w := '';
end;
							 


return	ds_retorno_w;

end obter_primeira_solicitacao;
/
