create or replace
function obter_desc_exame_lab_cod(	cd_exame_p		varchar2)
						return varchar2 is

ds_retorno_w	varchar2(255)	:= '';
			
begin

if	(cd_exame_p is not null)then
	begin
	
	select   max(nm_Exame)
	into	 ds_retorno_w
	from     exame_laboratorio
	where    upper(cd_exame)   = upper(cd_exame_p)
	and      lab_obter_se_exame_lib(nr_seq_exame, wheb_usuario_pck.get_cd_estabelecimento) = 'S'
	and      ie_solicitacao    = 'S';
	
	end;
end if;

return	ds_retorno_w;

end obter_desc_exame_lab_cod;
/