create or replace
function pls_obter_indicacao
		(	nr_seq_pessoa_proposta_p	pls_proposta_beneficiario.nr_sequencia%type,
			ie_opcao_p 			varchar2 )
 		    	return varchar2 is

/* ie_opcao_p:
	* CB - C�digo do benefici�rio de indica��o
	* DB - Descri��o do benefici�rio de indica��o
	* CF - C�digo da pessoa f�sica de indica��o
	* DF - Descri��o da pessoa f�sica de indica��o
	* CJ - C�digo da pessoa jur�dica de indica��o
	* DJ - Descri��oo da pessoa jur�dica de indica��o
*/

ds_retorno_w		varchar2(255) := null;
nr_seq_segurado_w	pls_indicacao_venda.nr_seq_segurado%type;
cd_pessoa_fisica_w	pls_indicacao_venda.cd_pessoa_fisica%type;
cd_cgc_w		pls_indicacao_venda.cd_cgc%type;
			
begin

if	(nr_seq_pessoa_proposta_p is not null) then
	begin
	select	nr_seq_segurado,
		cd_pessoa_fisica,
		cd_cgc
	into	nr_seq_segurado_w,
		cd_pessoa_fisica_w,
		cd_cgc_w
	from	(select	b.nr_seq_segurado,
			b.cd_pessoa_fisica,
			b.cd_cgc		
		from	pls_proposta_beneficiario	a,
			pls_indicacao_venda		b
		where	b.nr_seq_segurado_prop 	= a.nr_sequencia
		and	a.nr_sequencia 		= nr_seq_pessoa_proposta_p
		union all
		select	b.nr_seq_segurado,
			b.cd_pessoa_fisica,
			b.cd_cgc
		from	pls_proposta_beneficiario	a,
			pls_indicacao_venda		b,
			pls_proposta_adesao		c
		where	b.nr_seq_proposta 	= c.nr_sequencia
		and	a.nr_seq_proposta 	= c.nr_sequencia
		and	a.nr_sequencia 		= nr_seq_pessoa_proposta_p
		and	not exists     (select	1
					from	pls_proposta_beneficiario	x,
						pls_indicacao_venda		y
					where	y.nr_seq_segurado_prop	= x.nr_sequencia
					and	x.nr_sequencia = a.nr_sequencia));
	exception
	when others then
		nr_seq_segurado_w	:= null;
		cd_pessoa_fisica_w	:= null;
		cd_cgc_w		:= null;
	end;

	if	(ie_opcao_p = 'CB') then -- CB - C�digo do benefici�rio de indica��o
		
		ds_retorno_w := nr_seq_segurado_w;
		
	elsif	(ie_opcao_p = 'DB') then -- DB - Descri��o do benefici�rio de indica��o
		if	(nr_seq_segurado_w is not null) then	
			ds_retorno_w := substr(pls_obter_dados_segurado(nr_seq_segurado_w,'N'),1,255);
		end if;
	elsif	(ie_opcao_p = 'CF') then -- CF - C�digo da pessoa f�sica de indica��o

		ds_retorno_w := cd_pessoa_fisica_w;

	elsif	(ie_opcao_p = 'DF') then -- DF - Descri��o da pessoa f�sica de indica��o
		if	(cd_pessoa_fisica_w is not null) then	
			ds_retorno_w := substr(obter_nome_pf(cd_pessoa_fisica_w),1,255);
		end if;	
	elsif	(ie_opcao_p = 'CJ') then -- CJ - C�digo da pessoa jur�dica de indica��o

		ds_retorno_w := cd_cgc_w;
		
	elsif	(ie_opcao_p = 'DJ') then -- DJ - Descri��oo da pessoa jur�dica de indica��o
		if	(cd_cgc_w is not null) then	
			ds_retorno_w := substr(obter_nome_pf_pj(null, cd_cgc_w),1,255);
		end if;	
	end if;		
end if;	

return	ds_retorno_w;

end pls_obter_indicacao;
/