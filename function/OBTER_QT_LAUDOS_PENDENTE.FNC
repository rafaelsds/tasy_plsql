create or replace 
function OBTER_QT_LAUDOS_PENDENTE
	(nr_atendimento_p		number,
	nr_seq_laudo_fora_p		number,
	nr_seq_proc_fora_p		number)	return number is

/*

Function criada para a OS 19904
nr_seq_laudo_fora_p = 	verificar se tem laudos pendentes que n�o sejam este laudo
nr_seq_proc_fora_p = 	verificar se tem procedimentos sem laudos ou com laudos pendentes que n�o sejam deste procedimento

*/

qt_laudo_pendente_w		number(5,0);
qt_proc_sem_laudo_w		number(5,0);
retorno_w			number(5,0);
nr_sequencia_prescricao_w	number(10);
nr_prescricao_w			number(10);
qt_proc_sem_exec_w		number(10);
ie_opcao_w			varchar(2);


begin

	select  nvl(max(ie_laudo_pendente),'L')
	into    ie_opcao_w
	from    parametro_cdi;

if	(ie_opcao_w = 'L') then
	begin
	
	select	count(*)
	into	qt_laudo_pendente_w
	from	laudo_paciente
	where	nr_atendimento	= nr_atendimento_p
	and	dt_laudo	is null
	and	nr_sequencia	<> nvl(nr_seq_laudo_fora_p, -1);

	select	count(*)
	into	qt_proc_sem_laudo_w
	from	procedimento_paciente c,
		prescr_procedimento b,
		prescr_medica a
	where	a.nr_atendimento	= nr_atendimento_p
	and	a.nr_prescricao		= b.nr_prescricao
	and	c.nr_sequencia_prescricao	= b.nr_sequencia
	and	c.nr_prescricao		= b.nr_prescricao
	and	c.nr_sequencia		<> nvl(nr_seq_proc_fora_p, -1)
	and	not exists 	(select	1
				from	laudo_paciente y,
					procedimento_paciente x
				where	y.nr_seq_proc		= x.nr_sequencia
				and	x.nr_sequencia_prescricao = b.nr_sequencia
				and	x.nr_prescricao		= b.nr_prescricao);
				
	end;

	retorno_w	:= qt_laudo_pendente_w + qt_proc_sem_laudo_w;
	
else 

	select	count(*)
	into	qt_proc_sem_exec_w
	from	prescr_procedimento b,
		prescR_medica a
	where	b.nr_prescricao = a.nr_prescricao
	and	a.nr_atendimento = nr_atendimento_p
	and	b.nr_seq_exame is null
	and	nvl(b.ie_suspenso,'N') <> 'S'
	and	not exists 	(select	1
				from	procedimento_paciente x
				where	x.nr_sequencia_prescricao 	= b.nr_sequencia
				and	x.nr_prescricao			= b.nr_prescricao);	
				
	retorno_w	:= qt_proc_sem_exec_w;
		
end if;

return	retorno_w;

end OBTER_QT_LAUDOS_PENDENTE;
/
