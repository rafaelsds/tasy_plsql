create or replace
function pat_obter_se_regra_valor (
        nr_seq_regra_valor_p pat_regra_valor_conta.nr_sequencia%type,
        vl_valor_p number,
        dt_data_p date )
return varchar2 is

ds_retorno_w varchar2(1);

begin

begin
select  nvl(max('S'),'N')
into  ds_retorno_w
from  pat_regra_valor_conta_item a
where a.nr_seq_regra_valor = nr_seq_regra_valor_p
and   vl_valor_p between a.vl_minimo and nvl(a.vl_maximo,vl_valor_p)
and   dt_data_p between a.dt_inicio and nvl(a.dt_final,dt_data_p);
exception
    when others then
        ds_retorno_w := 'N';
end;

return ds_retorno_w;
end;
/
