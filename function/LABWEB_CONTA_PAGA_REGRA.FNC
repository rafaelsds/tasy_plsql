create or replace function Labweb_conta_paga_regra (nr_atendimento_p	varchar2,
                                                    nr_prescricao_p number DEFAULT null,
                                                    nr_seq_prescr_p number default null)
					return varchar2 is

ds_retorno_w		varchar2(1);


begin

	if (nr_prescricao_p is  null 
        or nr_seq_prescr_p is null)then
		select 	decode(count(*),'0','N','S')
		into	ds_retorno_w
		from 	conta_paciente a
		where	a.nr_atendimento = nr_atendimento_p
		and 	(a.ie_status_acerto = 2 and nvl(nr_seq_protocolo,0) <> 0);

		if	(ds_retorno_w = 'N') then

			select 	decode(count(*),'0','N','S')
			into	ds_retorno_w
			from 	labweb_conv_cobranca a,
				conta_paciente b
			where 	nvl(a.ie_situacao,'A') = 'A'
			and	a.cd_convenio = b.cd_convenio_parametro
			and	a.cd_convenio = Obter_Convenio_Atendimento(nr_atendimento_p);

		end if;
	else 
		select 	decode(count(*),'0','N','S')
		into	ds_retorno_w
		from 	conta_paciente a,
				procedimento_paciente b
		where	a.nr_atendimento 			= nr_atendimento_p
		and 	a.ie_status_acerto 			= 2 
		and 	a.nr_interno_conta 			= b.nr_interno_conta
		and		b.nr_prescricao 			= nr_prescricao_p
		and		b.nr_sequencia_prescricao	= nr_seq_prescr_p;

	end if;

	return	ds_retorno_w;

end Labweb_conta_paga_regra;
/
