create or replace 
FUNCTION Obter_Atributo_Compl_Hist(nr_seq_atributo_p	Varchar2)
				return varchar2 is

nm_atributo_w			Varchar2(254);

begin

select	max(nm_atributo)
into	nm_atributo_w
from	Atributo_Compl_hist
where	nr_sequencia		= nr_seq_atributo_p;

return nm_atributo_w;

end Obter_Atributo_Compl_Hist;
/
