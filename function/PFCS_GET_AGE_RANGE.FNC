create or replace function PFCS_GET_AGE_RANGE(dt_birthdate_p	date)

					return varchar2 is

qt_idade_w number(10) := 0;

begin

if (dt_birthdate_p is not null) then

qt_idade_w := round((sysdate - dt_birthdate_p) / 365);

end if;

if	(qt_idade_w > 125) then

	return '> 125';

elsif	(qt_idade_w > 120) then

	return '121 125';

elsif	(qt_idade_w > 115) then

	return '116 120';

elsif	(qt_idade_w > 110) then

	return '111 115';

elsif	(qt_idade_w > 105) then

	return '106 - 110';

elsif	(qt_idade_w > 100) then

	return '101 - 105';

elsif	(qt_idade_w > 95) then

	return '96 - 100';

elsif	(qt_idade_w > 90) then

	return '91 - 95';

elsif	(qt_idade_w > 85) then

	return '86 - 90';

elsif	(qt_idade_w > 80) then

	return '81 - 85';

elsif	(qt_idade_w > 75) then

	return '76 - 80';

elsif	(qt_idade_w > 70) then

	return '71 - 75';

elsif	(qt_idade_w > 65) then

	return '66 - 70';

elsif	(qt_idade_w > 60) then

	return '61 - 65';

elsif	(qt_idade_w > 55) then

	return '56 - 60';

elsif	(qt_idade_w > 50) then

	return '51 - 55';

elsif	(qt_idade_w > 45) then

	return '46 - 50';

elsif	(qt_idade_w > 40) then

	return '41 - 45';

elsif	(qt_idade_w > 35) then

	return '36 - 40';

elsif	(qt_idade_w > 30) then

	return '31 - 35';

elsif	(qt_idade_w > 25) then

	return '26 - 30';

elsif	(qt_idade_w > 20) then

	return '21 - 25';

elsif	(qt_idade_w > 15) then

	return '16 - 20';

elsif	(qt_idade_w > 10) then

	return '11 - 15';

elsif	(qt_idade_w > 5) then

	return '06 - 10';

elsif	(qt_idade_w >= 0) then

	return '00 - 05';

else

	return '0';

end if;


end PFCS_GET_AGE_RANGE;
/