create or replace 
function obter_desc_hemoterapia(nr_prescricao_p			prescr_medica.nr_prescricao%type,
								nr_sequencia_p			prescr_procedimento.nr_sequencia%type,
								ie_tipo_proced_p		prescr_procedimento.ie_tipo_proced%type,
								cd_procedimento_p		prescr_procedimento.cd_procedimento%type,
								ie_origem_proced_p		prescr_procedimento.ie_origem_proced%type,
								nr_seq_proc_interno_p	prescr_procedimento.nr_seq_proc_interno%type,
								nr_seq_derivado_p		prescr_procedimento.nr_seq_derivado%type)
								return varchar2 is
ds_retorno_w varchar2(50);
begin 

ds_retorno_w := substr(Obter_desc_san_derivado(nr_seq_derivado_p),1,50);

if (nr_seq_derivado_p is null) and
   (obter_se_exibe_proced(nr_prescricao_p,nr_sequencia_p,ie_tipo_proced_p,'PH') = 'S') then
	ds_retorno_w := substr(Obter_Desc_Prescr_Proc(cd_procedimento_p,ie_origem_proced_p, nr_seq_proc_interno_p),1,50);
end if;

return ds_retorno_w;

end obter_desc_hemoterapia;
/