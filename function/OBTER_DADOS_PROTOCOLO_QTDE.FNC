create or replace
function obter_dados_protocolo_qtde (	nr_seq_protocolo_p		number,
					ie_opcao_p			varchar2)
					return number is
qt_retorno_w 		number(15,0) := 0;

begin
if (nvl(nr_seq_protocolo_p,0) > 0) then 
	if (ie_opcao_p = 'TR') then
		select 	count(1)
		into	qt_retorno_w
		from 	titulo_receber 
		where 	nr_seq_protocolo = nr_seq_protocolo_p;
	elsif (ie_opcao_p = 'NF') then
		select 	count(1)
		into	qt_retorno_w
		from 	nota_fiscal 
		where 	nr_seq_protocolo = nr_seq_protocolo_p;
	end if;
end if;

return	qt_retorno_w;

end obter_dados_protocolo_qtde;
/