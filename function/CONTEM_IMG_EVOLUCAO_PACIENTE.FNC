create or replace FUNCTION CONTEM_IMG_EVOLUCAO_PACIENTE 
(
  CD_EVOLUCAO_P IN VARCHAR2 
) RETURN VARCHAR2 AS 
vl_retorno_w		Varchar2(4000);
BEGIN
            select case when consulta.nr_imagem > 0 then 'S'
            else  'N' end contem_imagem
            INTO vl_retorno_w
            from( select distinct count(ds_arquivo) nr_imagem
            from    evolucao_paciente_imagem epi
            where epi.cd_evolucao = CD_EVOLUCAO_P) consulta;
            
    
return	nvl(vl_retorno_w,' ');             
END CONTEM_IMG_EVOLUCAO_PACIENTE;
/
