Create or replace
function adep_obter_um_dosagem_glic (	nr_prescricao_p	number,
										nr_sequencia_p	number,
										ie_acm_p	varchar2,
										ie_sn_p		varchar2)
							return varchar is

qt_dose_w					varchar2(15);
qt_solucao_w				number(15,4);
ds_prescr_w					varchar2(15);
ds_prescricao_w				varchar2(60);
ds_retorno_w				varchar2(80);
qt_reg_w					number(06,0);
nr_seq_w					number(06,0);
ie_via_w					varchar2(50);
qt_dose_especial_w			number(18,6);
hr_dose_especial_w			varchar2(5);

cd_unidade_medida_dose_w	prescr_material.cd_unidade_medida_dose%type;
nr_agrupamento_w			prescr_material.nr_agrupamento%type;
ie_agrupador_w				prescr_material.ie_agrupador%type;

begin

select	converte_fracao_dose(max(a.cd_unidade_medida_dose), max(b.qt_dose)) qt_dose,
		max(a.cd_unidade_medida_dose),
		substr(obter_desc_intervalo(max(a.cd_intervalo)),1,15),
		substr(obter_desc_via(max(a.ie_via_aplicacao)),1,50),
		max(a.nr_agrupamento),
		max(a.ie_agrupador)
into	qt_dose_w,
		cd_unidade_medida_dose_w,
		ds_prescr_w,
		ie_via_w,
		nr_agrupamento_w,
		ie_agrupador_w	
from  	prescr_mat_hor b,
		prescr_material a
where 	a.nr_sequencia		= b.nr_seq_material	 
and		a.nr_prescricao 	= b.nr_prescricao
and 	a.nr_prescricao  	= nr_prescricao_p
and		Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
and		b.nr_sequencia		= nr_sequencia_p;

if	(substr(qt_dose_w,1,1) = ',') then
	qt_dose_w	:= '0' || qt_dose_w;
end if;

qt_dose_w	:= replace(qt_dose_w,'.',',');

select	count(*),
		min(nr_sequencia)
into	qt_reg_w,
		nr_seq_w
from	prescr_material
where	ie_agrupador		= 1
and		nr_agrupamento		= nr_agrupamento_w
and		nr_prescricao		= nr_prescricao_p;

if	(qt_reg_w > 1) and
	(nr_sequencia_p <> nr_seq_w) then
	ds_prescricao_w	:= '';
	ie_via_w		:= '';
else
	ds_prescricao_w	:= ds_prescr_w;
end if;

if 	(ie_agrupador_w is not null) then
	ds_retorno_w	:= substr(qt_dose_w || ' ' || cd_unidade_medida_dose_w || '   ' ||ds_prescricao_w || '   ' || ie_via_w,1,60);
else
	ds_retorno_w	:= substr(ds_prescricao_w || '   ' || ie_via_w,1,60);
end if;

return ds_retorno_w;

end adep_obter_um_dosagem_glic;
/