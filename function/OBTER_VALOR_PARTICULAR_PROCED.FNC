create or replace
function obter_valor_particular_proced(
				cd_Estabelecimento_p	Number,
				cd_procedimento_p	Number,
				ie_origem_proced_p	Number,
				ie_tipo_atendimento_p	Number,
				cd_medico_p		Varchar2,
				ie_clinica_p		Number,
				nr_seq_proc_interno_p	Number,
				nr_seq_exame_p		Number)
 		    	return Number is

cd_convenio_w		Number(5);			
cd_categoria_w		Varchar2(10);
vl_procedimento_w	Number(15,2);
vl_Retorno_w		Number(15,2);

begin

obter_convenio_particular(cd_estabelecimento_p, cd_convenio_w, cd_categoria_w);

select	obter_preco_proced(
				cd_estabelecimento_p,
				cd_convenio_w,
				cd_categoria_w,
				sysdate,
				cd_procedimento_p,
				ie_origem_proced_p,
				null,
				ie_tipo_atendimento_p,
				null,
				cd_medico_p,
				null,
				null,
				null,
				ie_clinica_p,
				null,
				'P',
				nr_seq_exame_p,
				nr_seq_proc_interno_p)
into	vl_procedimento_w
from	dual;

vl_retorno_w	:= vl_procedimento_w;

return	vl_Retorno_w;

end obter_valor_particular_proced;
/