CREATE OR REPLACE FUNCTION Obter_estoque_minimo_derivado (nr_seq_derivado_p number,
							Ie_fator_rh_p 	  varchar2,
							ie_tipo_sanguineo_p varchar2,
							ie_opcao_p number )
							return varchar2 is
resultado_w		varchar2(40);
qt_estoque_minimo_w	number(10);


cursor	c01 is
	select 	nvl(a.qt_estoque_minimo,0)
	from   	san_derivado_estoque a
	where  	a.nr_seq_derivado = nr_seq_derivado_p
	and	(nvl(a.ie_fator_rh, ie_fator_rh_p) 		= ie_fator_rh_p or ie_fator_rh_p is null)
	and	(nvl(a.ie_tipo_sanguineo, ie_tipo_sanguineo_p) = ie_tipo_sanguineo_p or ie_tipo_sanguineo_p is null)
	order by nvl(nr_seq_derivado_p,0), nvl(ie_fator_rh_p,0), nvl(ie_tipo_sanguineo_p,0) ;
		
begin
OPEN C01;
LOOP
FETCH C01 into	
		qt_estoque_minimo_w;
EXIT when C01%notfound;
END LOOP;
CLOSE C01;

resultado_w :=	qt_estoque_minimo_w;

return resultado_w;
							

end;
/