create or replace
function odont_desc_especialidade(nr_seq_especialidade_p		number)
 		    	return Varchar2 is

ds_especialidade_w	varchar2(80);			
			
begin
if	(nr_seq_especialidade_p is not null) then
	select	max(ds_especialidade)
	into	ds_especialidade_w
	from	especialidade_odontologica
	where	nr_sequencia = nr_seq_especialidade_p;
end if;

return	ds_especialidade_w;

end odont_desc_especialidade;
/