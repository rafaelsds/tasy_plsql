create or replace
function obter_interv_evento_sol	(ie_tipo_solucao_p	number,
					nr_prescricao_p	number,
					nr_seq_solucao_p	number,
					nr_seq_evento_p	number,
					dt_evento_p		date,
					ie_evento_p		number,
					ie_evento_valido_p	varchar2)
					return varchar2 is

ds_intervalo_w	varchar2(100);
nr_seq_evento_w	number(10,0);
dt_evento_w		date;

begin
if	(ie_tipo_solucao_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) and
	(nr_seq_evento_p is not null) and
	(dt_evento_p is not null) and
	(ie_evento_p is not null) and
	(ie_evento_valido_p is not null) then

	if	(ie_tipo_solucao_p = 1) then -- solucoes

		if	(ie_evento_p <> 6) and
			(ie_evento_valido_p = 'S') then

			select	nvl(max(nr_sequencia),0)
			into	nr_seq_evento_w
			from	prescr_solucao_evento
			where	ie_tipo_solucao = ie_tipo_solucao_p
			and	nr_prescricao = nr_prescricao_p
			and	nr_seq_solucao = nr_seq_solucao_p
			and	nr_sequencia < nr_seq_evento_p
			and	ie_alteracao <> 6
			and	ie_evento_valido = 'S';

			if	(nr_seq_evento_w > 0) then

				select	dt_alteracao
				into	dt_evento_w
				from	prescr_solucao_evento
				where	nr_sequencia = nr_seq_evento_w;
	
				if	(dt_evento_w is not null) then

					select	obter_dif_data(dt_evento_w,dt_evento_p,'')
					into	ds_intervalo_w
					from	dual;
				end if;
			end if;
		end if;


	elsif	(ie_tipo_solucao_p = 2) then -- sne

		if	(ie_evento_p <> 6) and
			(ie_evento_valido_p = 'S') then

			select	nvl(max(nr_sequencia),0)
			into	nr_seq_evento_w
			from	prescr_solucao_evento
			where	ie_tipo_solucao = ie_tipo_solucao_p
			and	nr_prescricao = nr_prescricao_p
			and	nr_seq_material = nr_seq_solucao_p
			and	nr_sequencia < nr_seq_evento_p
			and	ie_alteracao <> 6
			and	ie_evento_valido = 'S';

			if	(nr_seq_evento_w > 0) then

				select	dt_alteracao
				into	dt_evento_w
				from	prescr_solucao_evento
				where	nr_sequencia = nr_seq_evento_w;
	
				if	(dt_evento_w is not null) then

					select	obter_dif_data(dt_evento_w,dt_evento_p,'')
					into	ds_intervalo_w
					from	dual;
				end if;
			end if;
		end if;


	elsif	(ie_tipo_solucao_p = 3) then -- hemocomponentes

		if	(ie_evento_p <> 6) and
			(ie_evento_valido_p = 'S') then

			select	nvl(max(nr_sequencia),0)
			into	nr_seq_evento_w
			from	prescr_solucao_evento
			where	ie_tipo_solucao = ie_tipo_solucao_p
			and	nr_prescricao = nr_prescricao_p
			and	nr_seq_procedimento = nr_seq_solucao_p
			and	nr_sequencia < nr_seq_evento_p
			and	ie_alteracao <> 6
			and	ie_evento_valido = 'S';

			if	(nr_seq_evento_w > 0) then

				select	dt_alteracao
				into	dt_evento_w
				from	prescr_solucao_evento
				where	nr_sequencia = nr_seq_evento_w;
	
				if	(dt_evento_w is not null) then

					select	obter_dif_data(dt_evento_w,dt_evento_p,'')
					into	ds_intervalo_w
					from	dual;
				end if;
			end if;
		end if;


	elsif	(ie_tipo_solucao_p = 4) then -- npt adulta

		if	(ie_evento_p <> 6) and
			(ie_evento_valido_p = 'S') then

			select	nvl(max(nr_sequencia),0)
			into	nr_seq_evento_w
			from	prescr_solucao_evento
			where	ie_tipo_solucao = ie_tipo_solucao_p
			and	nr_prescricao = nr_prescricao_p
			and	nr_seq_nut = nr_seq_solucao_p
			and	nr_sequencia < nr_seq_evento_p
			and	ie_alteracao <> 6
			and	ie_evento_valido = 'S';

			if	(nr_seq_evento_w > 0) then

				select	dt_alteracao
				into	dt_evento_w
				from	prescr_solucao_evento
				where	nr_sequencia = nr_seq_evento_w;
	
				if	(dt_evento_w is not null) then

					select	obter_dif_data(dt_evento_w,dt_evento_p,'')
					into	ds_intervalo_w
					from	dual;
				end if;
			end if;
		end if;


	elsif	(ie_tipo_solucao_p = 5) then -- npt neo

		if	(ie_evento_p <> 6) and
			(ie_evento_valido_p = 'S') then

			select	nvl(max(nr_sequencia),0)
			into	nr_seq_evento_w
			from	prescr_solucao_evento
			where	ie_tipo_solucao = ie_tipo_solucao_p
			and	nr_prescricao = nr_prescricao_p
			and	nr_seq_nut_neo = nr_seq_solucao_p
			and	nr_sequencia < nr_seq_evento_p
			and	ie_alteracao <> 6
			and	ie_evento_valido = 'S';

			if	(nr_seq_evento_w > 0) then

				select	dt_alteracao
				into	dt_evento_w
				from	prescr_solucao_evento
				where	nr_sequencia = nr_seq_evento_w;
	
				if	(dt_evento_w is not null) then

					select	obter_dif_data(dt_evento_w,dt_evento_p,'')
					into	ds_intervalo_w
					from	dual;
				end if;
			end if;
		end if;
	end if;
end if;

return ds_intervalo_w;

end obter_interv_evento_sol;
/