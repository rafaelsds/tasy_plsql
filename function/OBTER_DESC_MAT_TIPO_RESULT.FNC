create or replace
function obter_desc_mat_tipo_result(nr_sequencia_p	number)
 		    	return varchar2 is

ds_resultado_w		mat_tipo_result.ds_resultado%type;

begin

ds_resultado_w := '';

if	(nr_sequencia_p > 0) then
	select	substr(ds_resultado,1,2000)
	into	ds_resultado_w
	from	mat_tipo_result
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_resultado_w;

end obter_desc_mat_tipo_result;
/
