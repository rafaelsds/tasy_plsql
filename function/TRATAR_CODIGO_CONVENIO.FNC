create or replace
function tratar_codigo_convenio(	cd_codigo_usuario_p	varchar2,
					ie_regra_p		varchar2 )
					return	varchar2 is

cd_codigo_retorno_w	varchar2(30);

begin

cd_codigo_retorno_w  := cd_codigo_usuario_p;

if  (ie_regra_p in ('1','2','3')) then
    cd_codigo_retorno_w   :=    substr(cd_codigo_usuario_p,2,17);   
end if;

if	(ie_regra_p in ('4')) then
	cd_codigo_retorno_w	:=	substr(cd_codigo_usuario_p,3,16); 	
end if;

return	cd_codigo_retorno_w; 

end tratar_codigo_convenio;
/