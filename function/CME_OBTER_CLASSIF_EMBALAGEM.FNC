create or replace 
function CME_Obter_Classif_Embalagem
		(nr_seq_embalagem_p	number) 
		return varchar2 is

ds_retorno_w	varchar2(100);

begin

select	ds_classificacao
into	ds_retorno_w
from	cm_classif_embalagem
where	nr_sequencia	= nr_seq_embalagem_p;

return ds_retorno_w;

end CME_Obter_Classif_Embalagem;
/
