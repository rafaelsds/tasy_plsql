Create or Replace
Function obter_se_material_padronizado(	cd_estabelecimento_p		number,
						cd_material_p			number) 
return Varchar2 is

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ds_retorno_w			Varchar2(1);

begin

cd_estabelecimento_w	:= cd_estabelecimento_p;

if 	(cd_estabelecimento_w is null) then
	cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
end if;

begin
select	ie_padronizado
into	ds_retorno_w
from	material_estab
where	cd_material = cd_material_p
and		cd_estabelecimento = cd_estabelecimento_w;

exception
when others then
	begin
	select	ie_padronizado
	into	ds_retorno_w
	from	material
	where	cd_material = cd_material_p;
	exception
	when others then
		ds_retorno_w := null;
	end;
end;

return ds_retorno_w;
end obter_se_material_padronizado;
/
