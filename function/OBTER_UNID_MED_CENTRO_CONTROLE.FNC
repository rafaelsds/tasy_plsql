create or replace 
function obter_unid_med_centro_controle(	cd_centro_controle_p number,
						cd_estabelecimento_p number)
						return varchar2 is

ds_resultado_w			varchar2(100);

begin

if	(cd_centro_controle_p is not null) and
	(cd_estabelecimento_p is not null) then
	select	cd_unidade_medida
	into	ds_resultado_w
	from	centro_controle
	where	cd_centro_controle = cd_centro_controle_p
	and	cd_estabelecimento = cd_estabelecimento_p;
end if;

return ds_resultado_w;

end obter_unid_med_centro_controle;
/