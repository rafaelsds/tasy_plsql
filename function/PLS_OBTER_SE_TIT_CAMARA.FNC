create or replace
function pls_obter_se_tit_camara
		(nr_titulo_pagar_p	titulo_pagar.nr_titulo%type,
		nr_titulo_receber_p	titulo_receber.nr_titulo%type) return varchar2 is
		
ds_retorno_w	varchar2(255) 	:= 'N';
cont_w		number(10,0) 	:= 0;

begin 

if	(nr_titulo_pagar_p is not null) then
	select	count(1)
	into	cont_w
	from	pls_titulo_lote_camara
	where	nr_titulo_pagar = nr_titulo_pagar_p;
elsif	(nr_titulo_receber_p is not null) then
	select	count(1)
	into	cont_w
	from	pls_titulo_lote_camara
	where	nr_titulo_receber = nr_titulo_receber_p;
end if;

if	(cont_w > 0) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end pls_obter_se_tit_camara;
/