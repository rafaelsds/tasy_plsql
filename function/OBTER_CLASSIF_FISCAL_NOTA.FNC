create or replace
function obter_classif_fiscal_nota(	nr_sequencia_p		Number,
						ie_desc_codigo_p		varchar2)
			return varchar2 is

/*ie_desc_codigo_p
C = Codigo
D = Descri��o
CD = C�digo - Descri��o
*/

cd_classificacao_w		Varchar2(15);
ds_classificacao_w		Varchar2(150);
ds_retorno_w			Varchar2(150);

begin

select	cd_classificacao,
	ds_classificacao
into	cd_classificacao_w,
	ds_classificacao_w
from	classif_fiscal_prest_serv
where	nr_sequencia = nr_sequencia_p;

if	(ie_desc_codigo_p = 'C') then
	ds_retorno_w	:= cd_classificacao_w;
elsif	(ie_desc_codigo_p = 'D') then
	ds_retorno_w	:= ds_classificacao_w;
elsif	(ie_desc_codigo_p = 'CD') then
	ds_retorno_w	:= cd_classificacao_w || ' - ' || ds_classificacao_w;
end if;

return ds_retorno_w;
	
end obter_classif_fiscal_nota;
/