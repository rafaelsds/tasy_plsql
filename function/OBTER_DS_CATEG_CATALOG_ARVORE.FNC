CREATE OR REPLACE function obter_ds_categ_catalog_arvore (	nr_sequencia_p	number)
			return varchar2 is

ds_retorno_w		varchar2(4000) := '';

begin

select cd_categoria || ' - ' || ds_descricao_categoria
into ds_retorno_w
from catalogo_codif_categ
where nr_sequencia = nr_sequencia_p;

return ds_retorno_w;

end obter_ds_categ_catalog_arvore;
/