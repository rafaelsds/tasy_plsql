create or replace
function man_obter_min_exec_desenv_sup( nr_seq_os_p	number)
 		    	return number is

ds_retorno_w	number(10,0);

begin
select	sum(nvl(a.qt_minuto,0))
into	ds_retorno_w
from	man_ordem_serv_ativ a
where	a.nr_seq_ordem_serv       = nr_seq_os_p
and	a.nr_seq_funcao not in (31, 32, 311,11,551,132,132)
and	exists (select 1 from usuario_grupo_des u
		where u.nm_usuario_grupo = a.nm_usuario_exec);

/* 142|Conex�o
 462|Orienta��o do desenvolvimento
  22|Suporte
 */
return	nvl(ds_retorno_w,0);

end man_obter_min_exec_desenv_sup;
/	