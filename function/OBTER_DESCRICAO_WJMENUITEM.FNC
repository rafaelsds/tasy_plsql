create or replace
function obter_descricao_wjmenuitem (
		ds_texto_p	varchar2,
		ie_atalho_p	varchar2,
		ie_ctrl_p	varchar2,
		ie_shift_p	varchar2,
		ie_alt_p	varchar2)
		return varchar2 is
	
ds_descricao_w	varchar2(255);
	
begin
if	(ds_texto_p is not null) then
	begin
	ds_descricao_w	:= ds_texto_p;
	if	(ie_atalho_p is not null) then
		begin
		if	(nvl(ie_ctrl_p,'N') = 'N') and
			(nvl(ie_shift_p,'N') = 'N') and
			(nvl(ie_alt_p,'N') = 'N') then
			begin
			ds_descricao_w	:= ds_descricao_w || ' ' || ie_atalho_p;
			end;
		elsif	(nvl(ie_ctrl_p,'N') = 'S') and
			(nvl(ie_shift_p,'N') = 'S') and
			(nvl(ie_alt_p,'N') = 'S') then
			begin
			ds_descricao_w	:= ds_descricao_w || ' Ctrl+Shift+Alt' || ie_atalho_p;
			end;
		elsif	(nvl(ie_ctrl_p,'N') = 'S') and
			(nvl(ie_shift_p,'N') = 'S') and
			(nvl(ie_alt_p,'N') = 'N') then
			begin
			ds_descricao_w	:= ds_descricao_w || ' Ctrl+Shift+' || ie_atalho_p;
			end;			
		elsif	(nvl(ie_ctrl_p,'N') = 'S') and
			(nvl(ie_shift_p,'N') = 'N') and
			(nvl(ie_alt_p,'N') = 'S') then
			begin
			ds_descricao_w	:= ds_descricao_w || ' Ctrl+Alt+' || ie_atalho_p;
			end;
		elsif	(nvl(ie_ctrl_p,'N') = 'S') and
			(nvl(ie_shift_p,'N') = 'N') and
			(nvl(ie_alt_p,'N') = 'N') then
			begin
			ds_descricao_w	:= ds_descricao_w || ' Ctrl+' || ie_atalho_p;
			end;
		elsif	(nvl(ie_ctrl_p,'N') = 'N') and
			(nvl(ie_shift_p,'N') = 'S') and
			(nvl(ie_alt_p,'N') = 'S') then
			begin
			ds_descricao_w	:= ds_descricao_w || ' Shift+Alt' || ie_atalho_p;
			end;
		elsif	(nvl(ie_ctrl_p,'N') = 'N') and
			(nvl(ie_shift_p,'N') = 'S') and
			(nvl(ie_alt_p,'N') = 'N') then
			begin
			ds_descricao_w	:= ds_descricao_w || ' Shift+' || ie_atalho_p;
			end;
		elsif	(nvl(ie_ctrl_p,'N') = 'N') and
			(nvl(ie_shift_p,'N') = 'N') and
			(nvl(ie_alt_p,'N') = 'S') then
			begin
			ds_descricao_w	:= ds_descricao_w || ' Alt+' || ie_atalho_p;
			end;									
		end if;
		end;
	end if;
	end;
end if;
return ds_descricao_w;
end obter_descricao_wjmenuitem;
/