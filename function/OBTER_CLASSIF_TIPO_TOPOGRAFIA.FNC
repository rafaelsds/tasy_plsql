create or replace
function obter_classif_tipo_topografia(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);

begin
select  max(a.ds_classificacao)
into	ds_retorno_w
from    cih_classif_topografia a	
where	a.nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end obter_classif_tipo_topografia;
/
