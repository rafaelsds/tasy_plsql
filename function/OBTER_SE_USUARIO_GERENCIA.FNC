create or replace
function OBTER_SE_USUARIO_GERENCIA	(nm_usuario_p		varchar2,
					nr_seq_gerencia_p	number)
					return	varchar2 is
		
ie_retorno_w	varchar2(1) := 'N';
		
begin

select	nvl(max('S'),'N')
into	ie_retorno_w
from	usuario_grupo_des c,
	grupo_desenvolvimento b,
	gerencia_wheb a
where	upper(c.nm_usuario_grupo)	= upper(nm_usuario_p)
and	b.nr_sequencia		= c.nr_seq_grupo
and	a.nr_sequencia		= b.nr_seq_gerencia
and	a.nr_sequencia		= nr_seq_gerencia_p;

return	ie_retorno_w;

end OBTER_SE_USUARIO_GERENCIA;
/