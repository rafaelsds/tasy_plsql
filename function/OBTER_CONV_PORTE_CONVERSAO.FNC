create or replace
function Obter_Conv_Porte_Conversao (	cd_convenio_p		number,
					nr_cirurgia_p		number)
 		    	return number is

ie_porte_w		varchar2(1);
qt_porte_anest_w	number(3,0)	:= 99;
			
begin

if	(nr_cirurgia_p is not null) and
	(cd_convenio_p is not null) then
	
	select	ie_porte
	into	ie_porte_w
	from	cirurgia
	where	nr_cirurgia	= nr_cirurgia_p;
	
	if	(ie_porte_w is not null) then
		
		select	max(qt_porte_anest)
		into	qt_porte_anest_w
		from	conv_porte_conversao
		where	cd_convenio 	= cd_convenio_P
		and	ie_porte	= ie_porte_w;
	
	end if;
	
end if;

return	qt_porte_anest_w;

end Obter_Conv_Porte_Conversao;
/