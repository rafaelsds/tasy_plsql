create or replace
function obter_dados_dime_estab	(	cd_estabelecimento_p		number,
					ie_opcao_p			varchar2 )
					return varchar2 is

/* OP��ES
TD - Tipo declara��o
RA - Regime apura��o
PE - Porte empresa
CO - Declara��o consolidada
CE - Declara��o centralizada
TC - Transfer�ncia de cr�ditos
CP - Cr�dito presumido
IF - Incentivos fiscais
MO - Movimento
ST - Substituto tribut�rio
EC - Escrita cont�bil
TR - N�mero trabalhadores
*/			
					
ie_tipo_declaracao_w 	varchar2(1);
ie_regime_apuracao_w 	varchar2(1);
ie_porte_empresa_w 	varchar2(1); 
ie_consolidada_w 		varchar2(1); 
ie_centralizada_w 		varchar2(1); 
ie_transf_creditos_w 	varchar2(1); 
ie_credito_presu_w 		varchar2(1); 
ie_incentivos_fiscais_w 	varchar2(1); 
ie_movimento_w 		varchar2(1); 
ie_subst_tributario_w 	varchar2(1); 
ie_escrita_contabil_w 	varchar2(1);
nr_trabalhadores_w 	varchar2(10);

begin

select	ie_tipo_declaracao, 
	ie_regime_apuracao, 
	ie_porte_empresa, 
	ie_consolidada, 
	ie_centralizada, 
	ie_transf_creditos, 
	ie_credito_presu, 
	ie_incentivos_fiscais, 
	ie_movimento, 
	ie_subst_tributario, 
	ie_escrita_contabil, 
	to_char(nr_trabalhadores)
into	ie_tipo_declaracao_w,
	ie_regime_apuracao_w, 
	ie_porte_empresa_w, 
	ie_consolidada_w, 
	ie_centralizada_w, 
	ie_transf_creditos_w,
	ie_credito_presu_w, 
	ie_incentivos_fiscais_w,
	ie_movimento_w,
	ie_subst_tributario_w,
	ie_escrita_contabil_w, 
	nr_trabalhadores_w
from	dime_estab
where	cd_estabelecimento = cd_estabelecimento_p;

if (ie_opcao_p = 'TD') then
	return ie_tipo_declaracao_w;
elsif (ie_opcao_p = 'RA') then
	return ie_regime_apuracao_w;
elsif (ie_opcao_p = 'PE') then
	return ie_porte_empresa_w;
elsif (ie_opcao_p = 'CO') then
	return ie_consolidada_w;
elsif (ie_opcao_p = 'CE') then
	return ie_centralizada_w;
elsif (ie_opcao_p = 'TC') then
	return ie_transf_creditos_w;
elsif (ie_opcao_p = 'CP') then
	return ie_credito_presu_w;
elsif (ie_opcao_p = 'IF') then
	return ie_incentivos_fiscais_w;
elsif (ie_opcao_p = 'MO') then
	return ie_movimento_w;
elsif (ie_opcao_p = 'ST') then
	return ie_subst_tributario_w;
elsif (ie_opcao_p = 'EC') then	
	return ie_escrita_contabil_w;
elsif (ie_opcao_p = 'TR') then
	return nr_trabalhadores_w;
else
	return '';
end if;

end obter_dados_dime_estab;
/