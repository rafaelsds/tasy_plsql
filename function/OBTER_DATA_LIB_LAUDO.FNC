CREATE OR REPLACE
FUNCTION Obter_Data_Lib_laudo(	
				nr_prescricao_p		Number,
				nr_seq_prescricao_p	Number)
				RETURN Date IS

dt_liberacao_w		Date;

BEGIN

begin
select	max(dt_liberacao)
into	dt_liberacao_w
from	laudo_paciente
where	nr_prescricao		= nr_prescricao_p
and	nr_seq_prescricao	= nr_seq_prescricao_p;
exception
	when others then
	dt_liberacao_w	:= null;
end;

RETURN dt_liberacao_w;

END Obter_Data_Lib_laudo;
/