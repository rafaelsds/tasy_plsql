create or replace
function latam_obter_dados_gap(nr_seq_gap_p	number,
							   ie_opcao_p	varchar2)
 		    	return number is
ds_retorno_w	number(15,2);
begin

if (ie_opcao_p = 'T') then
	select	count(*)
	into	ds_retorno_w
	from	LATAM_REQUISITO
	WHERE  	nr_seq_gap    = nr_Seq_gap_p;
elsif (ie_opcao_p = 'N') then
	SELECT 	count(*)
	into	ds_retorno_w
	FROM   LATAM_REQUISITO
	WHERE  	nr_seq_gap    = nr_Seq_gap_p
	and		IE_SITUACAO in ('D','P');
elsif (ie_opcao_p = 'V') then	
	SELECT 	nvl(SUM(vl_Custo),0)
	into	ds_retorno_w
	FROM   	LATAM_REQUISITO
	WHERE  	nr_seq_gap    = nr_Seq_gap_p;
elsif (ie_opcao_p = 'H') then		
	SELECT SUM(QT_TEMPO_HORAS)
	into	ds_retorno_w
	FROM   LATAM_REQUISITO
	WHERE  	nr_seq_gap    = nr_Seq_gap_p;
elsif (ie_opcao_p = 'TG') then		
	SELECT SUM(QT_TEMPO_HORAS) + SUM(nvl(QT_HORAS_DESIGN,0)) + SUM(nvl(QT_HORAS_TEC,0))
	into	ds_retorno_w
	FROM   LATAM_REQUISITO
	WHERE  	nr_seq_gap    = nr_Seq_gap_p;
		
end if;


return	ds_retorno_w;

end latam_obter_dados_gap;
/