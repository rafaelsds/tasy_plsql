create or replace
function obter_ds_material_especial (
				nr_sequencia_p		number,
				nr_prescricao_p		number)
				return varchar2 is
				
ds_descricao_w		varchar2(255);

begin

if (nr_sequencia_p is not null) and (nr_prescricao_p is not null) then
	select	ds_material_especial
	into	ds_descricao_w
	from	prescr_procedimento
	where	nr_sequencia = nr_sequencia_p
	and	nr_prescricao = nr_prescricao_p;
end if;
	
return	ds_descricao_w;

end obter_ds_material_especial;
/