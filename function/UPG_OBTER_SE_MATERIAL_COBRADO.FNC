create or replace
function upg_obter_se_material_cobrado(cd_material_p number)
 		    	return Varchar2 is

ds_retorno_w Varchar2(1);
qt_cobrado_w	number(5);
			
begin

select	count(*)
into	qt_cobrado_w
from 	regra_ajuste_material
where 	cd_convenio in (2,5,6,8)
and	cd_material = cd_material_p
and	ie_glosa = 'K';

if (qt_cobrado_w > 0) then
	ds_retorno_w:= 'N';
else 
	ds_retorno_w:= 'S';
end if;

return	ds_retorno_w;

end upg_obter_se_material_cobrado;
/