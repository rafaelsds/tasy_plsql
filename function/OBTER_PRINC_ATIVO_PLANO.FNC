create or replace
function obter_princ_ativo_plano(cd_material_p 	number,
								 ie_opcao_p		varchar2)
				return varchar2 is

ds_princ_ativo_princ_w	varchar2(1000);
ds_concetracao_princ_w	varchar2(1000);
ds_princ_ativo_sec_w	varchar(1000);
ds_concetracao_sec_w	varchar(1000);
ds_princ_ativo_w		varchar(255);
ds_concentracao_w		varchar(255);

cursor	c01 is
select 	substr(obter_descricao_padrao('MEDIC_FICHA_TECNICA', 'DS_SUBSTANCIA',NR_SEQ_MEDIC_FICHA_TECNICA),1,80),
		QT_CONVERSAO_MG || CD_UNID_MED_CONCETRACAO
from 	material_princ_ativo
where 	cd_material = cd_material_p;

begin

	open C01;
	loop
	fetch C01 into	
		ds_princ_ativo_w,
		ds_concentracao_w;
	exit when C01%notfound;
		begin	
			if (ds_princ_ativo_w is not null) then
				ds_princ_ativo_sec_w := ds_princ_ativo_sec_w || ' + ' || ds_princ_ativo_w;
			end if;
			if (ds_concentracao_w is not null) then
				ds_concetracao_sec_w := ds_concetracao_sec_w || ' + ' || ds_concentracao_w;
			end if;
		end;
	end loop;
	close C01;
	
	select 	substr(obter_descricao_padrao('MEDIC_FICHA_TECNICA', 'DS_SUBSTANCIA',NR_SEQ_FICHA_TECNICA),1,80),
			QT_CONVERSAO_MG || CD_UNID_MED_CONCETRACAO
	INTO	ds_princ_ativo_princ_w,
			ds_concetracao_princ_w
	from 	material a
	where 	cd_material = cd_material_p;

	if(ie_opcao_p = 'P')then
		if (ds_princ_ativo_sec_w is not null) then
			return ds_princ_ativo_princ_w || ' + ' || substr(ds_princ_ativo_sec_w, 4, 1000);
		else
			return ds_princ_ativo_princ_w;
		end if;
	else
		if (ds_concetracao_sec_w is not null) then
			return ds_concetracao_princ_w || ' ' || substr(ds_concetracao_sec_w, 2, 1000);
		else
			return ds_concetracao_princ_w;
		end if;
	end if;
	
	return null;
	
end obter_princ_ativo_plano;
/
