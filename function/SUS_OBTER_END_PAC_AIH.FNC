CREATE OR REPLACE
FUNCTION Sus_Obter_End_Pac_Aih
		(	nr_aih_p		Number,
			nr_seq_aih_p		Number,
			ie_tipo_dado_p		varchar2) 
			return Varchar2 is


/* IE_TIPO_DADO_P
	C - CEP
	MI - MUNICIPIO IBGE
	E - ENDERECO
*/


cd_pessoa_fisica_w	Varchar2(10);
cd_cep_w		Varchar2(15);
cd_municipio_ibge_w	varchar2(6);
ds_endereco_w		Varchar2(40);


BEGIN

select	b.cd_pessoa_fisica,
	a.cd_cep,
	a.cd_municipio_ibge,
	a.ds_endereco
into	cd_pessoa_fisica_w,
	cd_cep_w,
	cd_municipio_ibge_w,
	ds_endereco_w
from	atendimento_paciente	b,
	sus_aih			a
where	a.nr_atendimento	= b.nr_atendimento
and	a.nr_aih		= nr_aih_p
and	a.nr_sequencia		= nr_seq_aih_p;

select	nvl(cd_cep, cd_cep_w) cd_cep,
        nvl(cd_municipio_ibge, nvl(cd_municipio_ibge_w, ' ')) cd_municipio_ibge,
	nvl(ds_endereco, ds_endereco_w) ds_endereco
into	cd_cep_w,
	cd_municipio_ibge_w,
	ds_endereco_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_w
and	ie_tipo_complemento	= 1;

if	(ie_tipo_dado_p	= 'C') then
	return	cd_cep_w;
elsif	(ie_tipo_dado_p	= 'MI') then
	return	cd_municipio_ibge_w;
elsif	(ie_tipo_dado_p	= 'E') then
	return	ds_endereco_w;
end if;


END Sus_Obter_End_Pac_Aih;
/