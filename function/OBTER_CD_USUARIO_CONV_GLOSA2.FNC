create or replace function 
	obter_cd_usuario_conv_glosa2 (	nr_atendimento_p	number,
					ie_opcao_p		number)
						return varchar2 is	

cd_usuario_ibcm_w		varchar2(60);
cd_usuario_ipe_w		varchar2(60);

/*
ie_opcao_p	
1 - IBCM
2 - IPE
*/

begin
select	nvl(max(cd_usuario_conv_glosa),0),
	nvl(max(cd_usuario_convenio),0)
into	cd_usuario_ibcm_w,
	cd_usuario_ipe_w
from	atend_categoria_convenio
where	nr_atendimento 	= nr_atendimento_p
and 	dt_inicio_vigencia	= (	select max(x.dt_inicio_vigencia)
					from atend_categoria_convenio x
					where x.nr_atendimento	= nr_atendimento_p);
					
if	(ie_opcao_p = 1) then
	return 	cd_usuario_ibcm_w;
else
	return 	cd_usuario_ipe_w;
end if;
	
end obter_cd_usuario_conv_glosa2;
/
