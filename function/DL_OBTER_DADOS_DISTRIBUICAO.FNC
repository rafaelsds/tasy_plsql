create or replace
function dl_obter_dados_distribuicao(
                        nr_seq_distribuicao_p   number,
                        ie_opcao_p              varchar2)
                        return varchar2 is

ds_retorno_w    varchar2(255);

/*      ie_opcao_p
	A - nr adiantamento
	T - nr titulo pagar
*/

begin
ds_retorno_w    := '';

if      (ie_opcao_p = 'A') then
        select  max(nr_adiantamento)
        into    ds_retorno_w
        from    adiantamento_pago
        where   nr_seq_distribuicao    = nr_seq_distribuicao_p;
elsif   (ie_opcao_p = 'T') then
        select  max(nr_titulo)
        into    ds_retorno_w
        from    titulo_pagar
        where   nr_seq_distribuicao    = nr_seq_distribuicao_p;
end if;

return  ds_retorno_w;

end dl_obter_dados_distribuicao;
/
