create or replace
function Sus_Consiste_Habilitacao(cd_procedimento_p		Number,
			ie_origem_proced_p		Number,
			nr_dias_p			Number,
			cd_estabelecimento_p		Number)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(1) := 'N';
cd_habilitacao_w	Number(4);
dt_final_vigencia_w	Date;
			
begin

select	max(cd_habilitacao)
into	cd_habilitacao_w
from	sus_proced_habilitacao
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

begin
select	max(dt_final_vigencia)
into	dt_final_vigencia_w
from	sus_habilitacao_hospital
where	cd_habilitacao		= cd_habilitacao_w
and	cd_estabelecimento	= cd_estabelecimento_p;
exception
	when others then
	dt_final_vigencia_w	:= null;
end;

if	(dt_final_vigencia_w is not null) and
	((dt_final_vigencia_w - sysdate) <= nr_dias_p) then
	begin
	ds_retorno_w	:= 'S';
	end;
end if;

return	ds_retorno_w;

end Sus_Consiste_Habilitacao;
/