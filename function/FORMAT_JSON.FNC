create or replace
function format_json    (   ds_nome_p   varchar2,
                            ds_json_p   Clob)
 		    	return Clob is

begin

return	'{ "' || ds_nome_p || '" : [' || substr(ds_json_p,1,length(ds_json_p)-1) || ']}';

end format_json;
/
