create or replace
function obter_desc_escala_SADD(qt_pontos_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255) := '';

begin
if	(qt_pontos_p >= 1) and
	(qt_pontos_p < 	10) then
	ds_retorno_w := wheb_mensagem_pck.get_texto(307934); -- DependÍncia leve
elsif 	(qt_pontos_p >= 10) and
	(qt_pontos_p < 	20) then
	ds_retorno_w := wheb_mensagem_pck.get_texto(307935); -- DependÍncia moderada
elsif 	(qt_pontos_p >= 20) and
	(qt_pontos_p <= 45) then
	ds_retorno_w := wheb_mensagem_pck.get_texto(307936); -- DependÍncia Grave
else
	ds_retorno_w := wheb_mensagem_pck.get_texto(307937); -- Sem dependÍncia
end if;

return	ds_retorno_w;

end obter_desc_escala_SADD;
/