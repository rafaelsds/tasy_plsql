create or replace
function obter_tabulacao_etapa_npi(nr_seq_cronograma_p	number,
			nr_sequencia_p		number)
 		    	return varchar2 is

level_w		Number(5);
ds_atividade_w	Varchar(255);
result_w		Varchar(1000);

begin

select	nvl(max(ordem),0),
	max(ds_atividade)
into	level_w,
	ds_atividade_w
from	(select	z.nr_sequencia,
		z.nr_seq_superior,
		level ordem,
		z.ds_atividade
	from	(select	x.nr_sequencia,
			x.nr_seq_superior,
			ds_atividade
		from	proj_cron_etapa x
		where	x.nr_seq_cronograma = nr_seq_cronograma_p
		order by x.nr_seq_superior
	) z
	start with z.nr_seq_superior is null
	connect by prior z.nr_sequencia = z.nr_seq_superior)
where	nr_sequencia = nr_sequencia_p;

select	lpad(ds_atividade_w,length(ds_atividade_w)+((level_w-1)*5),' ')
into	result_w
from	dual;

return	result_w;

end obter_tabulacao_etapa_npi;
/