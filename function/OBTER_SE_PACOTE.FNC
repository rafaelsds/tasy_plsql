create or replace
function obter_se_pacote (nr_sequencia_p	number,
			  nr_seq_proc_pacote_p	number)
					return varchar2 deterministic is

ie_pacote_w	varchar2(1):=  'A';

begin

if	(nr_seq_proc_pacote_p is not null) and 
	(nr_sequencia_p = nr_seq_proc_pacote_p) then
	ie_pacote_w:= 'S';
elsif	(nr_seq_proc_pacote_p is not null) and 
	(nr_sequencia_p <> nr_seq_proc_pacote_p) then
	ie_pacote_w:= 'N';
end if;

return ie_pacote_w;

end obter_se_pacote;
/
