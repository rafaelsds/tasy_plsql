create or replace
function obter_se_result_exame_mat(
						nr_atendimento_p		number,
						cd_material_p			number,
						nr_seq_lab_result_p		number,
						nr_seq_resultado_p		number) return varchar2 is 

qt_resultado_w				exame_lab_result_item.qt_resultado%type;
ds_resultado_w				exame_lab_result_item.ds_resultado%type;
pr_resultado_w				exame_lab_result_item.pr_resultado%type;
nr_seq_exame_w				exame_lab_result_item.nr_seq_exame%type;
nr_seq_resultado_w			exame_lab_result_item.nr_seq_resultado%type;
dt_resultado_w				date;
cd_subgrupo_material_w		number(10);
cd_grupo_material_w			number(10);
cd_classe_material_w		number(10);
ie_retorno_w				varchar2(1) := 'N';
nr_seq_ficha_tecnica_w		mat_exame_lab_result.nr_seq_ficha_tecnica%type;
					
cursor c01 is
select	distinct b.qt_resultado,
		b.ds_resultado,
		b.pr_resultado,
		b.nr_seq_exame
from	exame_lab_result_item b
where	b.nr_sequencia = nr_seq_lab_result_p
and		b.nr_seq_resultado = nr_seq_resultado_p
and		exists  ( select 1
				  from	exame_lab_resultado a
				  where a.nr_atendimento = nr_atendimento_p
				  and	a.nr_seq_resultado = b.nr_seq_resultado
				  and	a.dt_resultado = (	
							select	max(c.dt_resultado)
							from	exame_lab_resultado c
							where	c.nr_seq_resultado = a.nr_seq_resultado
							and		c.nr_atendimento = a.nr_atendimento
							and		c.dt_resultado > sysdate -7));
begin

if (cd_material_p is not null) then
	

	select	nvl(max(b.cd_subgrupo_material),0),
			nvl(max(b.cd_grupo_material),0),
			nvl(max(b.cd_classe_material),0),
			nvl(max(b.nr_seq_ficha_tecnica),0)
	into	cd_subgrupo_material_w,
			cd_grupo_material_w,
			cd_classe_material_w,
			nr_seq_ficha_tecnica_w
	from	estrutura_material_v b
	where 	b.cd_material  = cd_material_p;

	open c01;
	loop
	fetch c01 into	qt_resultado_w,
					ds_resultado_w,
					pr_resultado_w,
					nr_seq_exame_w;
	exit when c01%notfound;
		begin
		
		if (qt_resultado_w is not null) then
			select	nvl(max('S'),'N')
			into	ie_retorno_w
			from	mat_exame_lab_result
			where	nr_seq_exame = nr_seq_exame_w
			and		ie_formato_resultado = 'V'
			and		nvl(cd_material,cd_material_p) = cd_material_p
			and		nvl(cd_classe_material, cd_classe_material_w) = cd_classe_material_w
			and		nvl(cd_subgrupo_material, cd_subgrupo_material_w) = cd_subgrupo_material_w
			and		nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
			and		nvl(nr_seq_ficha_tecnica, nr_seq_ficha_tecnica_w) = nr_seq_ficha_tecnica_w
			and		qt_resultado_w between qt_minima and qt_maxima;
		elsif (pr_resultado_w is not null) then
			select	nvl(max('S'),'N')
			into	ie_retorno_w
			from	mat_exame_lab_result
			where	nr_seq_exame = nr_seq_exame_w
			and		ie_formato_resultado = 'P'
			and		nvl(cd_material,cd_material_p) = cd_material_p
			and		nvl(cd_classe_material, cd_classe_material_w) = cd_classe_material_w
			and		nvl(cd_subgrupo_material, cd_subgrupo_material_w) = cd_subgrupo_material_w
			and		nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
			and		nvl(nr_seq_ficha_tecnica, nr_seq_ficha_tecnica_w) = nr_seq_ficha_tecnica_w
			and		pr_resultado_w between qt_percent_min and qt_percent_max;
		elsif (ds_resultado_w is not null) then
			select	nvl(max('S'),'N')
			into	ie_retorno_w
			from	mat_exame_lab_result
			where	nr_seq_exame = nr_seq_exame_w
			and		ie_formato_resultado = 'D'
			and		nvl(cd_material,cd_material_p) = cd_material_p
			and		nvl(cd_classe_material, cd_classe_material_w) = cd_classe_material_w
			and		nvl(cd_subgrupo_material, cd_subgrupo_material_w) = cd_subgrupo_material_w
			and		nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
			and		nvl(nr_seq_ficha_tecnica, nr_seq_ficha_tecnica_w) = nr_seq_ficha_tecnica_w
			and		ds_resultado = ds_resultado_w;
		end if;
		
		if (ie_retorno_w = 'S') then
			exit;
		end if;
		
		end;
	end loop;
	close c01;
end if;

return ie_retorno_w;

end obter_se_result_exame_mat;
/