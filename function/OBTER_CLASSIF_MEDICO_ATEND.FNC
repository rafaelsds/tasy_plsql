CREATE OR REPLACE
FUNCTION Obter_Classif_medico_atend(nr_atendimento_p 	Number,
				  cd_medico_p	Varchar2)	
				   	RETURN Varchar2 IS

nr_seq_classif_medico_w		Number(10)	:= null;
ds_especialidade_w		Varchar2(255)	:= '';

Cursor C01 is
select	nr_seq_classif_medico
from	atendimento_troca_medico
where	cd_medico_atual = cd_medico_p
and 	nr_atendimento = nr_atendimento_p
order by nr_sequencia;

BEGIN

OPEN C01;
LOOP
FETCH C01 into
	nr_seq_classif_medico_w;
exit when c01%notfound;
	nr_seq_classif_medico_w	:= nr_seq_classif_medico_w;
END LOOP;
CLOSE C01;

if 	(nr_seq_classif_medico_w is not null) then

	select	ds_classificacao
	into	ds_especialidade_w
	from	medico_classif_atend
	where	nr_sequencia	= nr_seq_classif_medico_w;

end if;

RETURN ds_especialidade_w;

END Obter_Classif_medico_atend;
/
