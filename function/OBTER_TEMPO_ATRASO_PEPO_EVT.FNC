create or replace
function obter_tempo_atraso_pepo_evt( nr_cirurgia_p		number,
				  nr_seq_evento_dif_p	number,
				  qt_tempo_limite_p number,
				  nr_seq_evento_p number)
				return number is

ie_tempo_atraso_w	number(10);
dt_registro_inic_w	date;
dt_registro_final_w	date;

begin

if 	(nvl(qt_tempo_limite_p,0) > 0) then

	select	max(dt_registro)
	into 	dt_registro_inic_w
	from   	evento_cirurgia_paciente
	where	((nr_cirurgia   = nr_cirurgia_p) or 
		 (nr_seq_pepo = nr_cirurgia_p)) 	
	and	nr_seq_evento = nr_seq_evento_p
	and	nvl(ie_situacao,'A') = 'A';
	
	select	max(dt_registro)
	into 	dt_registro_final_w
	from   	evento_cirurgia_paciente
	where	((nr_cirurgia   = nr_cirurgia_p) or 
		 (nr_seq_pepo = nr_cirurgia_p))
	and	nr_seq_evento = nr_seq_evento_dif_p
	and	nvl(ie_situacao,'A') = 'A';	
	
	ie_tempo_atraso_w := Obter_Min_Entre_Datas(dt_registro_inic_w,dt_registro_final_w,1) - qt_tempo_limite_p;

end if;

return	ie_tempo_atraso_w;

end obter_tempo_atraso_pepo_evt;

/