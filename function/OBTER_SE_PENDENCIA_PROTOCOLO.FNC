CREATE OR REPLACE FUNCTION obter_se_pendencia_protocolo(NR_SEQUENCIA_P IN NUMBER)
  RETURN VARCHAR IS
  
  return_w VARCHAR(1);
BEGIN
  SELECT decode(count(1),0,'N','S')
    INTO return_w
    FROM gqa_pendencia_regra t
   WHERE t.ie_evento = '9'
     AND t.ie_situacao = 'A'
     AND t.nr_seq_pendencia = NR_SEQUENCIA_P;
     
  RETURN return_w;
END obter_se_pendencia_protocolo;
/
