create or replace
function obter_horario_apraz_bolsa(	cd_perfil_p		Number,
					nr_prescricao_p		Number,
					nr_seq_procedimento_p	Number)
 		    	return date is
qt_seg_margem_w		Number(3);
qt_min_apraz_w		Number(3);
qt_seg_dif_w		Number(10);
qt_bolsas_w		Number(10);
cd_setor_prescricao_w	Number(5);
dt_ultimo_reg_w		Date;
dt_horario_w		Date;
nr_seq_derivado_w   prescr_procedimento.nr_seq_derivado%type;

Cursor C01 is
select	qt_minutos_margem,
	qt_minutos_apraz
from	regra_aprazamento_bolsa
where	nvl(cd_perfil,cd_perfil_p) = cd_perfil_p
and	nvl(cd_setor_atendimento,cd_setor_prescricao_w) = cd_setor_prescricao_w
and	nvl(nr_seq_derivado,nr_seq_derivado_w) = nr_seq_derivado_w
order by nvl(cd_perfil,0),
     nvl(nr_seq_derivado,0),
	 qt_minutos_margem;
			
begin

select nr_seq_derivado
into nr_seq_derivado_w
from prescr_procedimento
where nr_prescricao = nr_prescricao_p
and nr_sequencia = nr_seq_procedimento_p;

select	max(cd_setor_atendimento)
into	cd_setor_prescricao_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

open C01;
loop
fetch C01 into	
	qt_seg_margem_w,
	qt_min_apraz_w;
exit when C01%notfound;
end loop;
close C01;

select	max(dt_atualizacao_nrec),
	max(dt_horario)
into	dt_ultimo_reg_w,
	dt_horario_w
from	prescr_proc_hor
where	nr_prescricao = nr_prescricao_p
and	nr_seq_procedimento = nr_seq_procedimento_p;

select	count(*)
into	qt_bolsas_w
from	prescr_proc_bolsa
where	nr_prescricao = nr_prescricao_p
and	nr_seq_procedimento = nr_seq_procedimento_p
and	dt_cancelamento is null;

if	(qt_bolsas_w = 1) then
	select	max(dt_atualizacao_nrec)
	into	dt_ultimo_reg_w
	from	prescr_proc_bolsa
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_procedimento = nr_seq_procedimento_p
	and	dt_cancelamento is null;
end if;

qt_seg_dif_w := ceil((sysdate - dt_ultimo_reg_w) * 86400);

if	(qt_seg_dif_w <= qt_seg_margem_w) then
	dt_horario_w := dt_horario_w + qt_min_apraz_w/1440;
else	
	dt_horario_w := sysdate;	
end if;

return	trunc(dt_horario_w,'mi');

end obter_horario_apraz_bolsa;
/
