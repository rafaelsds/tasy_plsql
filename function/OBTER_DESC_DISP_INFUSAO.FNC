create or replace
function obter_desc_disp_infusao(ie_bomba_infusao_p		varchar2)
 		    	return varchar2 is

ds_retorno_w varchar2(255);
begin

if (ie_bomba_infusao_p is not null) then

	select 	max(ds_valor_dominio)
	into	ds_retorno_w
	from	valor_dominio
	where	cd_dominio	 = 1537
	and		vl_dominio 	 = ie_bomba_infusao_p;		

end if; 


return	ds_retorno_w;

end obter_desc_disp_infusao;
/
