create or replace function	ie_encounter_inconsistancy(nr_atendimento_p number)
				return	varchar2 is

ds_msg_w		varchar2(200) := '';
nr_classif_x_count_w	number := '';

begin

if	(nr_atendimento_p is not null)	then

	select 	count(*) 
	into 	nr_classif_x_count_w
	from 	enc_x_classif_x_category a
	where	a.ie_encounter_type =
			(select	max(b.ie_tipo_atendimento)
			from	atendimento_paciente b
			where	nr_atendimento = nr_atendimento_p)
	and	a.ie_situacao = 'A';


	if	(nr_classif_x_count_w > 0)	then 
		select	decode(count(*), 0, substr(obter_desc_expressao(954798), 0, 200), '')
		into    ds_msg_w
		from    atendimento_paciente a,
			enc_x_classif_x_category b,
			atend_categoria_convenio c
		where   a.nr_atendimento		= nr_atendimento_p
		and     a.ie_tipo_atendimento		= b.ie_encounter_type
		and     (b.ie_classification is null or a.nr_seq_classificacao = b.ie_classification)
		and     c.nr_atendimento(+)		= a.nr_atendimento
		and     (b.ie_category is null or c.nr_seq_patient_category = b.ie_category)
		and     b.ie_situacao 			= 'A';

	end if;
end if;
return	ds_msg_w;

end ie_encounter_inconsistancy;
/