create or replace
function Obter_preco_proc_Int_atend(	nr_atendimento_p		number,
					dt_conta_p			date,
					cd_procedimento_p		Number,
					ie_origem_proced_p		Number,
					nr_seq_proc_interno_p		number,
					ie_opcao_p			Varchar2)
 		    	return number is
vl_resultado_w			Number(15,2);
ie_tipo_atendimento_w		number(10);
cd_tipo_acomodacao_w		number(10);
cd_setor_Atendimento_w		number(10);
cd_convenio_w			number(10);
cd_categoria_w			varchar2(20);
cd_plano_w			varchar2(20);
cd_estabelecimento_w		number(10);
ie_clinica_w			number(10);
begin
select	ie_tipo_atendimento,
	CD_TIPO_ACOMOD_CONV,
	cd_setor_atendimento,
	cd_convenio,
	cd_categoria,
	cd_plano_convenio,
	cd_estabelecimento,
	ie_clinica
into	ie_tipo_atendimento_w,
	cd_tipo_acomodacao_w,
	cd_setor_Atendimento_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w,
	cd_estabelecimento_w,
	ie_clinica_w
from	atendimento_paciente_v
where	nr_atendimento	= nr_atendimento_p;

vl_resultado_w	:= obter_preco_proc_interno(	cd_estabelecimento_w,
						cd_convenio_w,
						cd_categoria_w,
						dt_conta_p,
						cd_procedimento_p,
						ie_origem_proced_p,
						nr_seq_proc_interno_p,
						cd_tipo_acomodacao_w,
						ie_tipo_atendimento_w,
						cd_setor_atendimento_w,
						null,
						null,
						null,
						cd_plano_w,
						ie_clinica_w,
						null,
						ie_opcao_p);

return	vl_resultado_w;

end Obter_preco_proc_Int_atend;
/