create or replace
FUNCTION HNSG_Dados_Prescr_Etiq(nr_prescricao_p NUMBER,
					 ie_reimprimir_p VARCHAR2,
					 ie_opcao_p 	VARCHAR2,
					 ds_setor_p VARCHAR2)
 		    	RETURN VARCHAR2 IS

/* ie_opcao_p
			'G' - Grupo
			'M' - Material*/
ds_temp_w 	  VARCHAR2(255):=NULL;
ds_retorno_w  VARCHAR2(255):=NULL;

CURSOR C01 IS
SELECT  DISTINCT
	DECODE(ie_opcao_p,'G',d.ds_grupo_exame_lab,'M',e.ds_material_exame)
FROM    atendimento_paciente g,
	material_exame_lab e,
	grupo_exame_lab d,
	exame_laboratorio c,
	prescr_proc_material h,
	prescr_procedimento b,
	prescr_medica a
WHERE   a.nr_prescricao         = b.nr_prescricao
AND     b.nr_seq_exame          = c.nr_seq_exame
AND     c.nr_seq_grupo          = d.nr_sequencia
AND     a.nr_atendimento        = g.nr_atendimento
AND	a.nr_prescricao	   	= h.nr_prescricao
AND	h.nr_seq_material	= e.nr_sequencia
AND	b.cd_material_exame	= e.cd_material_exame
AND     a.nr_prescricao         =  nr_prescricao_p
and	e.nr_sequencia		= 89 /* somente trazer sobre o material SORO 89 ate o momento*/
AND	('S' = ie_reimprimir_p 	OR (b.ie_status_atend >= 25 AND b.dt_atualizacao > SYSDATE - 0.0004))
AND	obter_se_contido_char(b.cd_setor_atendimento,ds_setor_p) = 'S';
BEGIN
OPEN c01;
	LOOP
		FETCH 	c01
		INTO	ds_temp_w;
		EXIT WHEN c01%NOTFOUND;
	   	ds_retorno_w := ds_retorno_w || ds_temp_w||', ';
	END LOOP;
CLOSE c01;
RETURN trim(SUBSTR(ds_retorno_w,1,LENGTH(ds_retorno_w)-2));
END HNSG_Dados_Prescr_Etiq;
/