create or replace
function Restringe_visualizacao( cd_estab_atend_p   varchar2)
					return varchar2 is

ie_lib_visualizacao_w	varchar2(1) := 'N';
ie_restringe_estab_w	varchar2(1);
ie_existe_regra_w	varchar2(1);

begin

obter_param_usuario(916, 381, obter_perfil_ativo, Wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_restringe_estab_w);

if (cd_estab_atend_p = wheb_usuario_pck.get_cd_estabelecimento) then
	ie_lib_visualizacao_w := 'S';
elsif (ie_restringe_estab_w = 'R') and (ie_lib_visualizacao_w = 'N') then

	select	decode(count(*),0,'S','N')
	into	ie_lib_visualizacao_w
	from	regra_estab_eup
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and	nvl(ie_situacao,'A') = 'A';

	if	(ie_lib_visualizacao_w = 'N') then
		
		select 	decode(count(*),0,'N','S')
		into	ie_lib_visualizacao_w
		from	regra_estab_eup a,
			regra_estab_eup_lib b
		where	a.nr_sequencia = b.nr_seq_regra
		and	a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
		and	b.cd_estabelecimento = cd_estab_atend_p;
		
	end if;
	
end if;

return	ie_lib_visualizacao_w;

end Restringe_visualizacao;
/
