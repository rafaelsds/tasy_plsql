create or replace
function Ageint_obter_lista_email(nr_seq_ageint_p		number,
				  cd_estabelecimento_p		number)
 		    	return varchar2 is

ds_email_destino_w	varchar2(255);
cd_medico_w		varchar2(10);
qt_regra_w		number(10,0);
ds_retorno_w		varchar2(4000);
ie_tipo_compl_corresp_w	number(2,0);

Cursor C01 is
	select	distinct b.cd_medico_req
	from	agenda_integrada_item a,
		agenda_consulta b
	where	a.nr_seq_agenda_cons = b.nr_sequencia
	and	a.nr_seq_agenda_int = nr_seq_ageint_p;
			
begin

open C01;
loop
fetch C01 into	
	cd_medico_w;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_regra_w
	from	ageint_texto_padrao_proc
	where	cd_pessoa_fisica = cd_medico_w
	and	nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
	and	ie_tipo_agenda = 'C';
	
	if	(qt_regra_w > 0) then
	
		select	max(ie_tipo_compl_corresp)
		into	ie_tipo_compl_corresp_w
		from	medico
		where	cd_pessoa_fisica = cd_medico_w; 
		
		select	max(b.ds_email)
		into	ds_email_destino_w
		from	pessoa_fisica a,
			compl_pessoa_fisica b
		where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
		and	b.ie_tipo_complemento 	= ie_tipo_compl_corresp_w
		and	a.cd_pessoa_fisica	= cd_medico_w;
		
		ds_retorno_w	:= ds_retorno_w || ds_email_destino_w ||', ';
	end if;
	
	end;
end loop;
close C01;

ds_retorno_w	:= substr(ds_retorno_w,1,length(ds_retorno_w)-2);

return	ds_retorno_w;

end Ageint_obter_lista_email;
/