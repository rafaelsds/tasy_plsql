create or replace
function Obter_Se_Entre_Horario	(	dt_referencia_p	date,
					hr_inicio_p	date,
					hr_final_p	date)
 		    	return varchar2 deterministic is
ds_retorno_w	varchar2(10)	:= 'N';

dt_inicio_w	date;
dt_final_w	date;

begin

dt_inicio_w	:= to_date(to_char(dt_referencia_p,'dd/mm/yyyy')||' '|| to_char(hr_inicio_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

dt_final_w	:= to_date(to_char(dt_referencia_p,'dd/mm/yyyy')||' '|| to_char(hr_final_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

if	(dt_final_w	< dt_inicio_w) then
	dt_final_w	:= dt_final_w + 1;
end if;


if	(dt_referencia_p	>= dt_inicio_w) and
	(dt_referencia_p	<= dt_final_w) then
	ds_retorno_w	:= 'S';
end if;


return	ds_retorno_w;

end Obter_Se_Entre_Horario;
/