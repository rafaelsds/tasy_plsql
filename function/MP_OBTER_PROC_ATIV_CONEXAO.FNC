create or replace
function mp_obter_proc_ativ_conexao(nr_seq_proc_obj_p	number)
						return varchar2 is

ds_retorno_w			varchar2(255)	:= null;
nr_seq_proc_obj_destino_w	number(10);
nr_seq_proc_obj_origem_w	number(10);
nr_seq_processo_destino_w	number(10);
nm_processo_w			varchar2(255);
nm_objeto_w			varchar2(255);
nr_seq_conexao_w		number(10);

begin

if	(nr_seq_proc_obj_p is not null) then
	select	max(nr_seq_proc_obj_destino)
	into	nr_seq_proc_obj_destino_w
	from	mp_proc_obj_ref
	where	nr_seq_proc_obj_origem	= nr_seq_proc_obj_p;

	if	(nr_seq_proc_obj_destino_w is not null) then
		select	b.nm_processo,
			a.nm_objeto
		into	nm_processo_w,
			nm_objeto_w
		from	mp_processo b,
			mp_processo_objeto a
		where	a.nr_seq_processo	= b.nr_sequencia
		and	a.nr_sequencia		= nr_seq_proc_obj_destino_w;

		ds_retorno_w	:= replace(replace(nm_processo_w,chr(13),null),chr(10),null) || '/' || chr(13) || replace(replace(nm_objeto_w,chr(13),null),chr(10),null);
	else
		/* Obter fluxo que est� ligada no destino */
		select	max(a.nr_sequencia)
		into	nr_seq_conexao_w
		from	mp_processo_objeto a
		where	a.nr_seq_obj_origem	= nr_seq_proc_obj_p;
		
		if	(nr_seq_conexao_w is not null) then
			/* Obter atividade liga a conex�o */
			select	max(a.nr_seq_obj_destino)
			into	nr_seq_proc_obj_destino_w
			from	mp_processo_objeto a
			where	a.nr_sequencia	= nr_seq_conexao_w;
			
			if	(nr_seq_proc_obj_destino_w is not null) then
			
				/*Obter o objeto origem */
				select	max(a.nr_seq_proc_obj_origem)
				into	nr_seq_proc_obj_origem_w
				from	mp_proc_obj_ref a
				where	a.nr_seq_proc_obj_destino	= nr_seq_proc_obj_destino_w;
				
				if	(nr_seq_proc_obj_origem_w is not null) then
					/* Obter fluxo do objeto origem */
					select	max(a.nr_sequencia)
					into	nr_seq_conexao_w
					from	mp_processo_objeto a
					where	a.nr_seq_obj_destino	= nr_seq_proc_obj_origem_w;
					
					if	(nr_seq_conexao_w is not null) then
						/* Obter atividade ligada a conex�o */
						select	max(a.nr_seq_obj_origem)
						into	nr_seq_proc_obj_origem_w
						from	mp_processo_objeto a
						where	a.nr_sequencia	= nr_seq_conexao_w;
						
						if	(nr_seq_proc_obj_origem_w is not null) then
							select	b.nm_processo,
								a.nm_objeto
							into	nm_processo_w,
								nm_objeto_w
							from	mp_processo b,
								mp_processo_objeto a
							where	a.nr_seq_processo	= b.nr_sequencia
							and	a.nr_sequencia		= nr_seq_proc_obj_origem_w;
		
							ds_retorno_w	:= replace(replace(nm_processo_w,chr(13),null),chr(10),null) || '/' || chr(13) || replace(replace(nm_objeto_w,chr(13),null),chr(10),null);
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
end if;

return	ds_retorno_w;

end;
/