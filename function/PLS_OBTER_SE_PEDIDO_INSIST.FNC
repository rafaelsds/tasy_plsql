create or replace
function pls_obter_se_pedido_insist(	nr_seq_requisicao_p		pls_requisicao.nr_sequencia%type)
					return 	varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter se dever� habilitar a op��o para realiza��o de pedido de insist�ncia no Portal Web.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ds_retorno_w		varchar2(1)	:= 'N';
ie_estagio_w		pls_requisicao.ie_estagio%type;
ie_tipo_processo_w	pls_requisicao.ie_tipo_processo%type;
ie_tipo_intercambio_w	pls_requisicao.ie_tipo_intercambio%type;
qt_transacao_w		pls_integer;

begin
select	ie_estagio,
	ie_tipo_processo,
	ie_tipo_intercambio
into	ie_estagio_w,
	ie_tipo_processo_w,
	ie_tipo_intercambio_w
from	pls_requisicao
where	nr_sequencia	= nr_seq_requisicao_p;

--Reprovada
if	(ie_estagio_w	= 7) and
	(ie_tipo_processo_w	= 'I') and
	(ie_tipo_intercambio_w	= 'I') then
	select	count(1)
	into	qt_transacao_w
	from	ptu_pedido_insistencia
	where  	nr_seq_requisicao = nr_seq_requisicao_p;
	
	--Pedido de insist�ncia
	if (qt_transacao_w = 0) then
		select	count(1)
		into	qt_transacao_w
		from	ptu_resposta_autorizacao
		where	nr_seq_requisicao = nr_seq_requisicao_p;

		--Resposta de autoriza��o
		if (qt_transacao_w > 0) then
			select	count(1)
			into	qt_transacao_w
			from	ptu_resposta_auditoria
			where	nr_seq_requisicao = nr_seq_requisicao_p;
			
			--Resposta de auditoria
			if (qt_transacao_w < 2) then
				ds_retorno_w	:= 'S';
			end if;
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_pedido_insist;
/
