create or replace
function obter_se_medic_atraso_html(					ie_tipo_item_p	varchar2,
																				nr_atendimento_p	varchar2,
																				nr_prescricao_p	number,
																				ie_pendente_liberacao_p varchar2,
																				nr_seq_cpoe_p number,
																				cd_item_p varchar2, 
																				qt_minutos_p number,
																				dt_filtro_p date,
																				nr_seq_item_p number,
																				qt_hora_adicional_p number,
																				qt_item_p number, 
																				cd_intervalo_p varchar2) 
return varchar2 is
										
ds_retorno_w	varchar2(1) := 'N';
				
		
begin

if(ie_tipo_item_p = 'M') then

		if 	(nr_seq_cpoe_p is not null) and
			(ie_tipo_item_p is not null) and
			(qt_minutos_p is not null) then
			
			
			
					SELECT  nvl(max('S'),'N') 
					into	ds_retorno_w
					FROM	 prescr_mat_hor a,
								prescr_material b  
					WHERE  ie_tipo_item_p = 'M'   
					AND	 a.nr_seq_material = b.nr_sequencia
					AND	 a.cd_material = b.cd_material   
					AND	 b.nr_prescricao = a.nr_prescricao   					   
					AND	(nr_seq_cpoe_p IS NOT NULL  AND	b.nr_seq_mat_cpoe = nr_seq_cpoe_p)   
					AND	 a.nr_atendimento = nr_atendimento_p   
					AND	 a.dt_horario < (SYSDATE - (qt_minutos_p / 1440))   
					AND	 a.dt_horario between  dt_filtro_p 	AND	(dt_filtro_p  + (qt_hora_adicional_p / 24))    
					AND	 a.dt_fim_horario IS NULL    
					AND	 a.dt_interrupcao IS NULL    
					AND	 a.dt_lib_horario IS NOT NULL    
					AND	 ie_pendente_liberacao_p IS NULL    
					AND	 a.dt_recusa IS NULL    
					AND	 a.dt_suspensao IS NULL  
					AND  a.ie_agrupador = 1		
					AND  b.qt_dose = qt_item_p
					AND  b.cd_intervalo = cd_intervalo_p
					AND	 nvl(a.ie_horario_especial,'N') = 'N';	
			

		elsif(cd_item_p is not null) then 	
		
			
					SELECT nvl(max('S'),'N')
					into	ds_retorno_w
					FROM	prescr_mat_hor a,
						    prescr_material b  
					WHERE  ie_tipo_item_p = 'M'  
					AND	 a.nr_seq_material = b.nr_sequencia 
					AND	 a.cd_material = b.cd_material  
					AND	 b.nr_prescricao = a.nr_prescricao  					 					
					AND	 (b.cd_material = cd_item_p)   
					AND	 a.nr_atendimento = nr_atendimento_p   
					AND	 a.dt_horario < (SYSDATE - (qt_minutos_p / 1440))   
					AND	 a.dt_horario between  dt_filtro_p 	AND	(dt_filtro_p  + (qt_hora_adicional_p / 24))    
					AND	 a.dt_fim_horario IS NULL    
					AND	 a.dt_interrupcao IS NULL    
					AND	 a.dt_lib_horario IS NOT NULL    
					AND	 ie_pendente_liberacao_p IS NULL    
					AND	 a.dt_recusa IS NULL    
					AND	 a.dt_suspensao IS NULL    
					AND  a.ie_agrupador = 1		
					AND  b.qt_dose = qt_item_p
					AND  b.cd_intervalo = cd_intervalo_p
					AND	 b.nr_prescricao = nr_prescricao_p
					AND	 nvl(a.ie_horario_especial,'N') = 'N';
 		end if;

elsif(ie_tipo_item_p = 'SOL') then		
		
		if 	(nr_seq_cpoe_p is not null) and
			(ie_tipo_item_p is not null) and
			(qt_minutos_p is not null) then
			
			
				SELECT nvl(max('S'),'N')
				into	ds_retorno_w
				FROM  	prescr_mat_hor a, 
						prescr_material b,
						prescr_medica c
        		WHERE	ie_tipo_item_p = 'SOL'   
				AND  	a.nr_seq_material = b.nr_sequencia 
		  	AND		a.cd_material = b.cd_material 
		  	AND		b.nr_prescricao = a.nr_prescricao
				AND		b.nr_prescricao = c.nr_prescricao					
		   	AND   (nr_seq_cpoe_p IS NOT NULL AND b.nr_seq_mat_cpoe = nr_seq_cpoe_p) 
		 		AND   a.nr_atendimento = nr_atendimento_p 
		 		AND 	a.dt_horario < (SYSDATE - (qt_minutos_p / 1440))  
				AND	  a.dt_horario between  dt_filtro_p 	AND	(dt_filtro_p  + (qt_hora_adicional_p / 24))    
				AND		NVL(obter_status_solucao_prescr(1, a.nr_prescricao, a.nr_seq_solucao),'N') in ('N','P')				
		  	AND 	a.dt_fim_horario IS NULL    
		  	AND 	a.dt_interrupcao IS NULL    
		   	AND 	a.dt_lib_horario IS NOT NULL    
		   	AND   ie_pendente_liberacao_p IS NULL    
		   	AND 	a.dt_recusa IS NULL    
		   	AND 	a.dt_suspensao IS NULL    
		   	AND 	NVL(a.ie_horario_especial,'N') = 'N' 
				AND   a.ie_agrupador = 4				
				AND   a.nr_seq_solucao = nr_seq_item_p;
				
			

		elsif(cd_item_p is not null) then
		
				SELECT  nvl(max('S'),'N')
				into	ds_retorno_w
				FROM	prescr_mat_hor a, 
						prescr_material b,
						prescr_medica c	 
				WHERE 	ie_tipo_item_p = 'SOL' 
				AND  	a.nr_seq_material = b.nr_sequencia
				AND		a.cd_material = b.cd_material 
				AND		b.nr_prescricao = a.nr_prescricao
				AND		b.nr_prescricao = c.nr_prescricao				
				AND  	(b.cd_material = cd_item_p) 
				AND   a.nr_atendimento = nr_atendimento_p 
				AND 	a.dt_horario < (SYSDATE - (qt_minutos_p / 1440))  
				AND	  a.dt_horario between  dt_filtro_p 	AND	(dt_filtro_p  + (qt_hora_adicional_p / 24))    
				AND		NVL(obter_status_solucao_prescr(1, a.nr_prescricao, a.nr_seq_solucao),'N') in ('N','P')
				AND 	a.dt_fim_horario IS NULL   
				AND 	a.dt_interrupcao IS NULL   
				AND 	a.dt_lib_horario IS NOT NULL   
				AND   ie_pendente_liberacao_p IS NULL   
				AND 	a.dt_recusa IS NULL   
				AND 	a.dt_suspensao IS NULL   
				AND 	NVL(a.ie_horario_especial,'N') = 'N'
				AND   a.ie_agrupador = 4
				AND   a.nr_seq_solucao = nr_seq_item_P;	
		
		end if;
		

end if;		

return ds_retorno_w;

end obter_se_medic_atraso_html;
/
