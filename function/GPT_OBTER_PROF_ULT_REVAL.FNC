create or replace
function gpt_obter_prof_ult_reval			(nr_seq_cpoe_p 		number,
											ie_tipo_item_p 		varchar2,
											nr_atendimento_p 	number)
				return varchar2 is

ds_retorno_w				pessoa_fisica.nm_pessoa_fisica%type;

begin
	if 	(nr_atendimento_p is not null) and  
		(nr_seq_cpoe_p is not null) and 
		(ie_tipo_item_p is not null) then 
		
		select	obter_nome_pessoa_fisica(cd_profissional, null)
		into	ds_retorno_w
		from	gpt_revalidation_events
		where	nr_atendimento = nr_atendimento_p
		and     nr_sequencia = gpt_obter_seq_reval_events(nr_seq_cpoe_p, ie_tipo_item_p);
		
	end if;
	
	return ds_retorno_w;

end gpt_obter_prof_ult_reval;
/