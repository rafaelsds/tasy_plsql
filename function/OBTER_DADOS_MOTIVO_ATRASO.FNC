create or replace
function obter_dados_motivo_atraso(	nr_sequencia_p	number,
					ie_opcao_p	varchar2)
					return varchar2 is

/*
G - Grupo
D_Descri��o
*/

ds_grupo_atraso_w	varchar2(255);
ds_retorno_w		varchar2(255);
ds_motivo_w		varchar2(100);
	
begin

select	max(ds_grupo_atraso),
	max(ds_motivo)
into	ds_grupo_atraso_w,
	ds_motivo_w
from	grupo_atraso_cirurgia a,
	cir_motivo_atraso b
where	b.nr_seq_grupo = a.nr_sequencia(+)
and	b.nr_sequencia = nr_sequencia_p;

if	(ie_opcao_p = 'G') then
	ds_retorno_w	:= ds_grupo_atraso_w;
elsif	(ie_opcao_p = 'D') then 
	ds_retorno_w	:= ds_motivo_w;
end if;

return	ds_retorno_w;

end obter_dados_motivo_atraso;
/
