create or replace
function hc_obter_nome_agenda (nr_seq_agenda_p	Number)
 		    	return Varchar2 is

nm_agenda_w	Varchar2(100);
			
begin
if (nr_seq_agenda_p is not null) then
	begin
	
	select	max(nm_agenda)
	into	nm_agenda_w
	from	agenda_home_care
	where	nr_sequencia = nr_seq_agenda_p;	
	
	end;
end if;


return	nm_agenda_w;

end hc_obter_nome_agenda;
/