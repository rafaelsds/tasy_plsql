create or replace
function obter_etapas_adep_sol(	ie_tipo_solucao_p	number,
								nr_prescricao_p		number,
								nr_seq_solucao_p	number)
						return number is

nr_etapas_w					number(3) := 0;
ie_alteracao_w				number(3);
ie_alteracao_aux_w			number(3);
nr_seq_motivo_w				number(10);
nr_sequencia_w				number(10);
nr_seq_aux_w				number(10);
ie_etapa_w					varchar2(1);
ie_inst_sol_sem_interromp_w	varchar2(1);

cursor c01 is
select	ie_alteracao,
		nr_seq_motivo,
		nr_sequencia
from	prescr_solucao_evento
where	nvl(ie_evento_valido,'S') = 'S'
and		((ie_alteracao in (1,2,35)) or
		((ie_inst_sol_sem_interromp_w = 'N') and (ie_alteracao = 35)))
and		ie_tipo_solucao = ie_tipo_solucao_p
and		nr_seq_solucao = nr_seq_solucao_p
and		nr_prescricao = nr_prescricao_p;

begin
if	(ie_tipo_solucao_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) then

	Obter_Param_Usuario(1113,518, Obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_inst_sol_sem_interromp_w);
	
	if	(ie_tipo_solucao_p = 1) then -- solucoes

		open C01;
		loop
		fetch C01 into	
			ie_alteracao_w,
			nr_seq_motivo_w,
			nr_sequencia_w;
		exit when C01%notfound;
			begin
			ie_etapa_w	:= 'S';
			if	(ie_alteracao_w	= 2) and
				(nvl(obter_se_motivo_troca_frasco(nr_seq_motivo_w),'N') <> 'S') then
				ie_etapa_w	:= 'N';
			elsif	(ie_alteracao_w = 35) then
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_aux_w
				from	prescr_solucao_evento
				where	nvl(ie_evento_valido,'S') = 'S'
				and		ie_tipo_solucao	= ie_tipo_solucao_p
				and		nr_sequencia	< nr_sequencia_w
				and		nr_seq_solucao	= nr_seq_solucao_p
				and		nr_prescricao	= nr_prescricao_p;				
												
				select	nvl(max(ie_alteracao),0)
				into	ie_alteracao_aux_w
				from	prescr_solucao_evento
				where	((ie_alteracao	<> 2) or
						 (obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S'))
				and		nr_sequencia	= nr_seq_aux_w;
												
				if	(ie_alteracao_aux_w	in (2, 37, 9, 5, 13)) then
					ie_etapa_w	:= 'N';
				else
					select	nvl(min(nr_sequencia),0)
					into	nr_seq_aux_w
					from	prescr_solucao_evento
					where	nvl(ie_evento_valido,'S') = 'S'
					and		ie_tipo_solucao	= ie_tipo_solucao_p
					and		nr_seq_solucao	= nr_seq_solucao_p
					and		nr_prescricao	= nr_prescricao_p
					and		nr_sequencia	> nr_sequencia_w;
															
					select	nvl(max(ie_alteracao),0)
					into	ie_alteracao_aux_w
					from	prescr_solucao_evento
					where	((ie_alteracao	<> 2) or
							 (obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S'))
					and		nr_sequencia	= nr_seq_aux_w;
											
					if	(ie_alteracao_aux_w	= 1) then
						ie_etapa_w	:= 'N';
						
					else 
		
						select	nvl(max(nr_sequencia),0)
						into	nr_seq_aux_w
						from	prescr_solucao_evento
						where	nvl(ie_evento_valido,'S') = 'S'
						and		ie_tipo_solucao	= ie_tipo_solucao_p
						and		nr_seq_solucao	= nr_seq_solucao_p
						and		nr_prescricao	= nr_prescricao_p
						and		nr_sequencia	< nr_sequencia_w
						and		cd_funcao 		= 88;
															
						select	nvl(max(ie_alteracao),0)
						into	ie_alteracao_aux_w
						from	prescr_solucao_evento
						where	nr_sequencia	= nr_seq_aux_w;
											
						if	(ie_alteracao_aux_w	= 1) then
							ie_etapa_w	:= 'N';					
						end if;											
						
					end if;	
						
				end if;
				
			end if;

			if	(ie_etapa_w	= 'S') then
				nr_etapas_w	:= nr_etapas_w + 1;
			end if;			
			
			end;
		end loop;
		close C01;
		
	elsif	(ie_tipo_solucao_p = 2) then -- suporte nutricional enteral
		
		select	count(1)
		into	nr_etapas_w
		from	prescr_solucao_evento
		where	nvl(ie_evento_valido,'S') = 'S'
		and		((ie_alteracao in (1,12)) or 
				 ((ie_alteracao = 2) and (obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S'))) --Ivan e Oraci em 04/01/2008 OS78263
		and		ie_alteracao in (1,2,12)
		and		ie_tipo_solucao = ie_tipo_solucao_p
		and		nr_seq_material = nr_seq_solucao_p
		and		nr_prescricao = nr_prescricao_p;


	elsif	(ie_tipo_solucao_p = 3) then -- hemocomponentes
		
		select	sum(nr_etapas)
		into	nr_etapas_w
		from (
			select	decode(count (1), 0, 0, 1) nr_etapas
			from	prescr_solucao_evento
			where	nvl(ie_evento_valido, 'S') = 'S'
			and		ie_alteracao = 1
			and		ie_tipo_solucao = ie_tipo_solucao_p
			and		nr_seq_procedimento = nr_seq_solucao_p
			and		nr_prescricao = nr_prescricao_p
			union all
			select	count(1) nr_etapas
			from	prescr_solucao_evento
			where	nvl(ie_evento_valido, 'S') = 'S'
			and		((ie_alteracao = 2 and obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S') or --Ivan e Oraci em 04/01/2008 OS78263
						(ie_alteracao = 4 and cd_funcao = 88))
			and		ie_alteracao in (2,4)
			and		ie_tipo_solucao = ie_tipo_solucao_p
			and		nr_seq_procedimento = nr_seq_solucao_p
			and		nr_prescricao = nr_prescricao_p
			);

	elsif	(ie_tipo_solucao_p = 4) then -- npt adulta
		
		select	count(1)
		into	nr_etapas_w
		from	prescr_solucao_evento
		where	nvl(ie_evento_valido,'S') = 'S'
		and		((ie_alteracao = 1) or 
				 ((ie_alteracao = 2) and (obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S'))) --Ivan e Oraci em 04/01/2008 OS78263
		and		ie_alteracao in (1,2)
		and		ie_tipo_solucao = ie_tipo_solucao_p
		and		nr_seq_nut = nr_seq_solucao_p
		and		nr_prescricao = nr_prescricao_p;

	elsif	(ie_tipo_solucao_p = 5) then -- npt neonatal
		
		select	count(1)
		into	nr_etapas_w
		from	prescr_solucao_evento
		where	nvl(ie_evento_valido,'S') = 'S'
		and		((ie_alteracao = 1) or 
				 ((ie_alteracao = 2) and (obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S'))) --Ivan e Oraci em 04/01/2008 OS78263
		and		ie_alteracao in (1,2)
		and		ie_tipo_solucao = ie_tipo_solucao_p
		and		nr_seq_nut_neo = nr_seq_solucao_p
		and		nr_prescricao = nr_prescricao_p;

	elsif	(ie_tipo_solucao_p = 6) then -- npt adulta2
		
		select	count(1)
		into	nr_etapas_w
		from	prescr_solucao_evento
		where	nvl(ie_evento_valido,'S') = 'S'
		and		((ie_alteracao = 1) or 
				 ((ie_alteracao = 2) and (obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S'))) --Ivan e Oraci em 04/01/2008 OS78263
		and		ie_alteracao in (1,2)
		and		ie_tipo_solucao = ie_tipo_solucao_p
		and		nr_seq_nut_neo = nr_seq_solucao_p
		and		nr_prescricao = nr_prescricao_p;
		
	elsif	(ie_tipo_solucao_p = 7) then -- npt pediatrica
		
		select	count(1)
		into	nr_etapas_w
		from	prescr_solucao_evento
		where	nvl(ie_evento_valido,'S') = 'S'
		and		((ie_alteracao = 1) or 
				 ((ie_alteracao = 2) and (obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S'))) --Ivan e Oraci em 04/01/2008 OS78263
		and		ie_alteracao in (1,2)
		and		ie_tipo_solucao = ie_tipo_solucao_p
		and		nr_seq_nut_neo = nr_seq_solucao_p
		and		nr_prescricao = nr_prescricao_p;	
		
	end if;
end if;

return nr_etapas_w;

end obter_etapas_adep_sol;
/
