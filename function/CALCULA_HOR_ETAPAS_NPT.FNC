create or replace 
function Calcula_hor_etapas_npt(nr_prescricao_p           number,
								ie_gerar_npt_fases_p	  varchar2)
                                 return varchar2 is
															
i                         number(10) := 1;
ds_retorno_w              varchar2(200) := '';
qt_fase_npt_w             number(15);
dt_prim_horario_w		  date;
hr_prim_horario_w         date;
qt_hora_w                 date;
Mascara_w                 varchar2(10);
nr_horas_intervalo_w      number(10);
qt_tempo_etapa_w          nut_pac.qt_tempo_etapa%type;
qt_hora_inf_w             nut_pac.qt_hora_inf%type;
ie_acm_w				  varchar2(1);

begin

select dt_primeiro_horario
into   dt_prim_horario_w	
from   prescr_medica
where  nr_prescricao = nr_prescricao_p;


select	qt_fase_npt,
	nvl(qt_tempo_etapa,0),
	qt_hora_inf,
	decode(hr_prim_horario, null, dt_prim_horario_w, pkg_date_utils.get_time(sysdate, hr_prim_horario, 0)),
	ie_acm
into  	qt_fase_npt_w,
	qt_tempo_etapa_w,
	qt_hora_inf_w,
	hr_prim_horario_w,
	ie_acm_w
from    nut_pac
where   nr_sequencia    = (select  min(nr_sequencia)
                              from    nut_pac
                              where   nr_prescricao   = nr_prescricao_p);

if	(qt_tempo_etapa_w > 0) then
	nr_horas_intervalo_w := qt_tempo_etapa_w;
else
	nr_horas_intervalo_w    := dividir(qt_hora_inf_w,qt_fase_npt_w);
end if;

if    (pkg_date_utils.extract_field('MINUTE', hr_prim_horario_w, 0) <> 0) then
	Mascara_w	:= 'hh24:mi';
else
	Mascara_w	:= 'hh24';
end if;

qt_hora_w := hr_prim_horario_w;

if	(nvl(ie_gerar_npt_fases_p,'N') = 'S') and
	(ie_acm_w <> 'S') then
	for i in 1.. qt_fase_npt_w loop
		begin
		ds_retorno_w 	:= 	ds_retorno_w || ' ' || wheb_mensagem_pck.get_texto(307054, 'NR_PRIM_HOR=' ||  to_char(trunc(qt_hora_w,'mi'), Mascara_w)) || ' ' || -- das #@NR_PRIM_HOR#@ �s
					to_char(trunc(qt_hora_w,'mi') + (nr_horas_intervalo_w / 24), Mascara_w) || ';';
		qt_hora_w       :=  trunc(qt_hora_w,'mi') + nr_horas_intervalo_w/24;
		end;
	end loop;
else
	ds_retorno_w := to_char(trunc(qt_hora_w,'mi'), Mascara_w);
end if;	

return ds_retorno_w;

end Calcula_hor_etapas_npt;
/