create or replace
function hd_obter_se_regra_atend (nr_atendimento_p	 number)
 		    	return varchar2 is

qt_regra_w		varchar2(10);
nr_atendimento_w	number(10);
nr_seq_interno_w	number(10);
cd_convenio_w		number(10);
nr_seq_queixa_w		number(10);

ds_retorno_w		varchar2(1)  := 'S';
			
begin

if (nr_atendimento_p is not null) then

	select	count(*)
	into	qt_regra_w
	from	hd_regra_atend_dialise a
	where	ie_situacao = 'A';
	
	if (qt_regra_w > 0) then
		
		select	max(a.nr_seq_interno)
		into	nr_seq_interno_w
		from	atend_categoria_convenio a
		where	a.nr_atendimento = nr_atendimento_p;
		
		select	max(cd_convenio)
		into	cd_convenio_w
		from	atend_categoria_convenio a
		where	a.nr_seq_interno = nr_seq_interno_w;
		
		select	max(nr_seq_queixa)
		into	nr_seq_queixa_w
		from	atendimento_paciente a
		where	a.nr_atendimento = nr_atendimento_p;
		
		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	hd_regra_atend_dialise a
		where	a.ie_situacao = 'A'
		and	a.cd_convenio = cd_convenio_w
		and	nr_seq_queixa_w = nvl(a.nr_seq_queixa,nr_seq_queixa_w);
		
	end if;	
	
end if;


return	ds_retorno_w;

end hd_obter_se_regra_atend;
/