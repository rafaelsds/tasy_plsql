create or replace
function obter_fabric_material( cd_material_p  varchar2, cd_marca_p varchar2)
	return number is

nr_seq_fabric  mat_fabricante.nr_sequencia%type;

begin
	if (cd_marca_p is null) then
		select	nr_seq_fabric
		into	nr_seq_fabric
		from	material
		where cd_material = cd_material_p;
		
		if(nr_seq_fabric is null) then 
			select 	nvl(max(cd_fabricante), '0')
			into	nr_seq_fabric
			from 	material_marca
			where 	cd_material = cd_material_p
			and		cd_fabricante is not null;
		end if;
	else
		select 	nvl(max(cd_fabricante), '0')
		into	nr_seq_fabric
		from 	material_marca
		where 	cd_material = cd_material_p
		and		nr_sequencia = cd_marca_p;
		
		if(nr_seq_fabric is null) then 
			select	nr_seq_fabric
			into	nr_seq_fabric
			from	material
			where cd_material = cd_material_p;
		end if;
		
	end if;	
	
return nr_seq_fabric;

end obter_fabric_material;
/