create or replace
function nut_obter_custo_gen( cd_material_p		number,
				qt_componente_p		number,
				qt_rendimento_p		number,
				qt_componentes_p	number,
				qt_conversao_p		number,
				ie_opcao_p		varchar2,
				nr_seq_genero_p		number)
 		    	return number is

qt_dose_convertida_w		number(18,6);
qt_material_w			number(18,6);
cd_unidade_medida_estoque_w	varchar2(30);
vl_custo_w			number(18,6);
qt_custo_total_w		number(18,6);
qt_custo_result_w		number(18,6);
cd_material_estoque_w		number(10);
ie_conv_nut_w			varchar2(1);
cd_estabelecimento_w		number(4);
ie_resultParametro_w            varchar2(1);
cd_unid_med_genero_w		varchar2(30);
qt_conversao_w			nut_genero_alim.qt_conversao%type;

begin
if	(qt_conversao_p = 0) then
	qt_conversao_w	:= 1;
else
	qt_conversao_w	:= qt_conversao_p;
end if;

if	(cd_material_p is not null) then
	
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
	
	obter_param_usuario(1003,84,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_conv_nut_w);
	obter_param_usuario(1003,112,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_resultParametro_w);	
	
	select 	max(substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UME'),1,30)) cd_unidade_medida_estoque
	into	cd_unidade_medida_estoque_w
	from	material
	where 	cd_material = cd_material_p;
	
	select	nvl(max(cd_unidade_medida),'G')
	into	cd_unid_med_genero_w
	from	nut_genero_alim
	where	nr_sequencia = nr_seq_genero_p;
	
	if	(ie_opcao_p = 'U') then	--Valor unit
		qt_material_w := (qt_componente_p/NVL(qt_rendimento_p,qt_componente_p));
		if	(ie_conv_nut_w = 'S') then
			if	(upper(cd_unidade_medida_estoque_w) <> upper(cd_unid_med_genero_w)) then
				qt_dose_convertida_w := qt_material_w / qt_conversao_w;
			else
				qt_dose_convertida_w := qt_material_w;
			end if;
			if (ie_resultParametro_w = 'M') then
			   vl_custo_w 	  := obter_custo_medio_material(cd_estabelecimento_w,TRUNC(SYSDATE,'mm'),cd_material_p);
			else 
			   vl_custo_w 	  := nut_obter_custo_ult_compra_gen(cd_estabelecimento_w,cd_material_p);
			end if;
			
			qt_custo_result_w := qt_dose_convertida_w * vl_custo_w;
		else
			qt_custo_result_w := nut_obter_custo_material(cd_material_p, qt_material_w, cd_unid_med_genero_w);
		end if;
	elsif	(ie_opcao_p = 'T') then	--Valor total
		qt_material_w := (qt_componente_p/NVL(qt_rendimento_p,qt_componente_p));
		if	(ie_conv_nut_w = 'S') then		
			if	(upper(cd_unidade_medida_estoque_w) <> upper(cd_unid_med_genero_w)) then		
				qt_dose_convertida_w := qt_material_w / qt_conversao_w;								
			else	
				qt_dose_convertida_w := qt_material_w;
			end if;		
			if (ie_resultParametro_w = 'M') then
			   vl_custo_w   := obter_custo_medio_material(cd_estabelecimento_w,TRUNC(SYSDATE,'mm'),cd_material_p);
			else 
			   vl_custo_w   := nut_obter_custo_ult_compra_gen(cd_estabelecimento_w, cd_material_p);
			end if;
			qt_custo_result_w := (qt_dose_convertida_w * vl_custo_w) * qt_componentes_p;
		else 
			qt_custo_result_w := nut_obter_custo_material(cd_material_p,qt_material_w, cd_unid_med_genero_w) * qt_componentes_p;
		end if;		
	elsif	(ie_opcao_p = 'C') then --Custo Total card�pio
		qt_material_w := (qt_componente_p/NVL(qt_rendimento_p,qt_componente_p));
		if	(ie_conv_nut_w = 'S') then		
			if	(upper(cd_unidade_medida_estoque_w) <> upper(cd_unid_med_genero_w)) then		
				qt_dose_convertida_w := qt_material_w / qt_conversao_w;								
			else	
				qt_dose_convertida_w := qt_material_w;
			end if;		
			if (ie_resultParametro_w = 'M') then
			   vl_custo_w   := obter_custo_medio_material(cd_estabelecimento_w,TRUNC(SYSDATE,'mm'),cd_material_p);
			else 
			   vl_custo_w   := nut_obter_custo_ult_compra_gen(cd_estabelecimento_w,cd_material_p);
			end if;
			qt_custo_result_w := qt_dose_convertida_w * vl_custo_w;
		else 
			qt_custo_result_w := nut_obter_custo_material(cd_material_p,qt_material_w, cd_unid_med_genero_w);
		end if;	
	end if;
end if;


return	qt_custo_result_w;

end nut_obter_custo_gen;
/