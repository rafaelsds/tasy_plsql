create or replace function OBTER_SE_CONTRATO_DATA(nr_seq_contrato_p	number,
						dt_referencia_p		date,
						ie_periodo_p		varchar2,
						nr_seq_regra_p		number default null) return Varchar2 is

ds_retorno_w		varchar2(1) := 'N';
cd_estabelecimento_w	number(4);
dt_primeiro_vencto_w	date;
ie_tratar_fim_semana_w	varchar2(1);
dt_referencia_w		date;

begin
ds_retorno_w	:= 'N';

select	cd_estabelecimento
into	cd_estabelecimento_w
from	contrato
where	nr_sequencia	= nr_seq_contrato_p;

select	ie_tratar_fim_semana
into	ie_tratar_fim_semana_w
from	parametro_fluxo_caixa
where	cd_estabelecimento	= cd_estabelecimento_w;

select	min(b.dt_primeiro_vencto)
into	dt_primeiro_vencto_w
from	contrato_regra_pagto b
where	decode(ie_periodo_p,'D',dt_referencia_p,PKG_DATE_UTILS.start_of(dt_referencia_p, 'MONTH', 0)) between 
		nvl(b.dt_inicio_vigencia, pkg_date_utils.get_date(1900, 1, 1, 0)) and
		nvl(b.dt_final_vigencia, pkg_date_utils.get_date(2099, 1, 1, 0))
and	decode(ie_periodo_p,'D',PKG_DATE_UTILS.extract_field('DAY', decode(ie_tratar_fim_semana_w,'S',obter_proximo_dia_util(cd_estabelecimento_w,
		PKG_DATE_UTILS.GET_DATE(PKG_DATE_UTILS.extract_field('DAY', nvl(b.dt_primeiro_vencto,dt_referencia_p)), dt_referencia_p) ),b.dt_primeiro_vencto)),0) = decode(ie_periodo_p,'D',PKG_DATE_UTILS.extract_field('DAY', dt_referencia_p),0)
and	(nr_seq_regra_p is null or b.nr_sequencia = nr_seq_regra_p)
and	b.dt_primeiro_vencto is not null
and	b.nr_seq_contrato	= nr_seq_contrato_p;

/* Quando n�o encontrar o primeiro vencimento n�o pode entrar pois sen�o vai acabar assumindo a data refer�ncia e retornando S incorretamente */
if	(dt_primeiro_vencto_w is not null) then
	if 	(ie_tratar_fim_semana_w = 'S') then
			
		/* Se o dia do primeiro vencimento n�o existir para o m�s de refer�ncia utiliza o �ltimo dia do m�s */
		begin
		dt_referencia_w	 := obter_proximo_dia_util(cd_estabelecimento_w,
			PKG_DATE_UTILS.GET_DATE(PKG_DATE_UTILS.extract_field('DAY', dt_primeiro_vencto_w), dt_referencia_p) );
		exception
			when others then
			dt_referencia_w	:= PKG_DATE_UTILS.GET_DATETIME(PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0), nvl(dt_referencia_p, PKG_DATE_UTILS.get_Time('00:00:00')));
		end;
		
		if 	(ie_periodo_p	 = 'D') and
			(PKG_DATE_UTILS.start_of(dt_referencia_w,'DD',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'DD',0)) then
			ds_retorno_w	:= 'S';
		elsif	(ie_periodo_p	 = 'M') and
			(PKG_DATE_UTILS.start_of(dt_referencia_w, 'MONTH', 0) = PKG_DATE_UTILS.start_of(dt_referencia_p, 'MONTH', 0)) then
			ds_retorno_w	:= 'S';
		end if;	
		
		/* Caso a procedure que gera o contrato gerar para o m�s seguinte */
		begin
		dt_referencia_w	 := obter_proximo_dia_util(cd_estabelecimento_w,
			PKG_DATE_UTILS.GET_DATE(PKG_DATE_UTILS.extract_field('DAY', dt_primeiro_vencto_w),
			PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p,-1, 0) ) );
		exception
			when others then
			dt_referencia_w	:= obter_proximo_dia_util(cd_estabelecimento_w,PKG_DATE_UTILS.GET_DATETIME(PKG_DATE_UTILS.end_of(PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p,-1,0), 'MONTH', 0), nvl(dt_referencia_p, PKG_DATE_UTILS.get_Time('00:00:00')))); 	
		end; 
		if 	(ie_periodo_p	 = 'D') and
			(PKG_DATE_UTILS.start_of(dt_referencia_w,'DD',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'DD',0)) then
			ds_retorno_w	:= 'S';
		elsif	(ie_periodo_p	 = 'M') and
			(PKG_DATE_UTILS.start_of(dt_referencia_w, 'MONTH', 0) = PKG_DATE_UTILS.start_of(dt_referencia_p, 'MONTH', 0)) then
			ds_retorno_w	:= 'S';
		end if;	
	else
		begin
			dt_referencia_w	:=  PKG_DATE_UTILS.GET_DATE(PKG_DATE_UTILS.extract_field('DAY', dt_primeiro_vencto_w), dt_referencia_p);
		exception when others then
			dt_referencia_w	:= PKG_DATE_UTILS.GET_DATETIME(PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0), nvl(dt_referencia_p, PKG_DATE_UTILS.get_Time('00:00:00')));
		end;
		
		if 	(ie_periodo_p	 = 'D') and
			(PKG_DATE_UTILS.start_of(dt_referencia_w,'DD',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'DD',0)) then
			ds_retorno_w	:= 'S';
		elsif	(ie_periodo_p	 = 'M') and
			(PKG_DATE_UTILS.start_of(dt_referencia_w, 'MONTH', 0) = PKG_DATE_UTILS.start_of(dt_referencia_p, 'MONTH', 0)) then
			ds_retorno_w	:= 'S';
		end if;			
	end if; 
end if;

return	ds_retorno_w;

end;
/
