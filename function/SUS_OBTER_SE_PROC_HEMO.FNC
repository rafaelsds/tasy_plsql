create or replace
function sus_obter_se_proc_hemo(nr_sequencia_p		procedimento_paciente.nr_sequencia%type)
				return varchar2 is
				
ds_retorno_w		varchar2(1)	:= 'N';
qt_reg_hemo_w		number(10)	:= 0;

begin

select	count(1)
into	qt_reg_hemo_w
from	san_exame_realizado
where   nr_seq_propaci  = nr_sequencia_p
and	rownum = 1;

if	(qt_reg_hemo_w = 0) then
	begin
	select	count(1)
	into	qt_reg_hemo_w
	from	san_producao
	where   nr_seq_propaci  = nr_sequencia_p
	and	rownum = 1;
	
	if	(qt_reg_hemo_w = 0) then
		begin
		select	count(1)
		into	qt_reg_hemo_w
		from	san_reserva_prod
		where   nr_seq_propaci  = nr_sequencia_p
		and	rownum = 1;
		
		if	(qt_reg_hemo_w > 0) then
			ds_retorno_w := 'S';				
		end if;		
		end;
	else
		ds_retorno_w := 'S';	
	end if;	
	end;
else
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end sus_obter_se_proc_hemo;
/