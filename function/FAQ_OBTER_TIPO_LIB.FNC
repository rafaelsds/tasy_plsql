create or replace
function faq_obter_tipo_lib(	nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin
select	nvl(max(ds_tipo_lib),'')
into	ds_retorno_w
from	com_faq_tipo_lib
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end faq_obter_tipo_lib;
/