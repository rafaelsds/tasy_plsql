create or replace
function obter_se_integra_LWSDEG_VFR(	nr_prescricao_p		number,
					cd_estabelecimento_p	number
					)
 		    	return varchar2 is

ie_integra_w		varchar2(1) := 'N';

begin

if	(nr_prescricao_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ie_integra_w
	from	prescr_procedimento b,
		exame_laboratorio c,
		lab_exame_equip d,
		equipamento_lab	e,
		material_exame_lab g
	where	b.nr_prescricao = nr_prescricao_p
	and	b.nr_seq_exame = c.nr_seq_exame
	and	c.nr_seq_exame = d.nr_seq_exame
	and	d.cd_equipamento = e.cd_equipamento
	and	g.cd_material_exame = b.cd_material_exame
	and	g.nr_sequencia = nvl(d.nr_seq_material,g.nr_sequencia)
	and	b.nr_seq_lab is not null
	and	b.nr_seq_exame is not null
	and	b.dt_integracao is null
	and	b.cd_motivo_baixa = 0
	and 	e.ds_sigla	= 'LWSDEG';

end if;

return	ie_integra_w;

end obter_se_integra_LWSDEG_VFR;
/