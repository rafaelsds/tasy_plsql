create or replace
function get_desc_inconsistency
			(	nr_seq_inconsistency_p	Number)
				return Varchar2 is

ds_inconsistency_w	Varchar2(255);

begin
select	max(ds_inconsistency)
into	ds_inconsistency_w
from	eclipse_inconsistency
where	nr_sequencia	= nr_seq_inconsistency_p;

return ds_inconsistency_w;

end get_desc_inconsistency;
/
