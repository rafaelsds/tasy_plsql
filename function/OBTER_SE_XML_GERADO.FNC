create or replace
function Obter_se_xml_gerado (nr_seq_protocolo_p	Number)
					return varchar2 is
qt_xml_gerado_w	number(8):= 0;
ie_retorno_w	varchar2(1):= 'N';

begin

begin
select 	count(*)
into	qt_xml_gerado_w
from	tiss_log
where 	nr_seq_protocolo = nr_seq_protocolo_p;
exception
	when others then
	qt_xml_gerado_w:= 0;
end;

if	(qt_xml_gerado_w > 0) then
	ie_retorno_w:= 'S';
else
	ie_retorno_w:= 'N';
end if;

return ie_retorno_w;

end Obter_se_xml_gerado;
/