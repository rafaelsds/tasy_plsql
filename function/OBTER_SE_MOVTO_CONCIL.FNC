create or replace
function obter_se_movto_concil(nr_sequencia_p	number,
				dt_referencia_p	date)
				return varchar2 is

ie_retorno_w		varchar2(1)	:= 'N';
nr_seq_concil_w		number(10);
dt_concil_w		date;
ie_conciliacao_w	varchar2(1)	:= 'N';

begin

select	max(a.nr_seq_concil),
	max(trunc(b.dt_atualizacao,'dd')),
	max(ie_conciliacao)
into	nr_seq_concil_w,
	dt_concil_w,
	ie_conciliacao_w
from	concil_banc_movto b,
	movto_trans_financ a
where	a.nr_seq_concil	= b.nr_sequencia(+)
and	a.nr_sequencia	= nr_sequencia_p;


if	(nr_seq_concil_w is null) and
	(ie_conciliacao_w = 'N') then
	ie_retorno_w	:= 'N';
elsif	(nr_seq_concil_w is not null) or
	(ie_conciliacao_w = 'S') then
	if	(dt_concil_w <= dt_referencia_p) then
		ie_retorno_w	:= 'S';
	else
		ie_retorno_w	:= 'N';
	end if;
end if;

return ie_retorno_w;

end;
/