create or replace
function pls_obter_dados_tabela_preco
			(	nr_seq_tabela_p		Number,
				ie_tipo_dado_p		Varchar2)
 		    		return Varchar2 is
				
/*	IE_TIPO_DADO_P
	'N' - Nome
	'DP' - Descri��o do produto
	'TB' - Tabela base
	'NC' - N�mero do contrato
	'EC' - Estipulante do contrato
	'SQ' Seq contrato
	'DR' - Dt. reajuste do contrato
*/

ds_retorno_w			Varchar2(255)	:= '';
nr_contrato_w			number(10);

begin

if	(ie_tipo_dado_p	= 'N') then
	select	nm_tabela
	into	ds_retorno_w
	from	pls_tabela_preco
	where	nr_sequencia = nr_seq_tabela_p;
elsif	(ie_tipo_dado_p	= 'DP') then
	select	b.ds_plano
	into	ds_retorno_w
	from	pls_tabela_preco a,
		pls_plano b
	where	a.nr_seq_plano	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_tabela_p;
elsif	(ie_tipo_dado_p	= 'TB') then
	select	max(ie_tabela_base)
	into	ds_retorno_w
	from	pls_tabela_preco
	where	nr_sequencia = nr_seq_tabela_p;
elsif	(ie_tipo_dado_p	= 'NC') then
	select	max(nr_contrato)
	into	ds_retorno_w
	from	pls_tabela_preco
	where	nr_sequencia = nr_seq_tabela_p;
elsif	(ie_tipo_dado_p	= 'EC') then
	select	substr(obter_nome_pf_pj(b.cd_pf_estipulante,b.cd_cgc_estipulante),1,255)
	into	ds_retorno_w
	from	pls_tabela_preco a,
		pls_contrato b
	where	a.nr_contrato	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_tabela_p;
elsif	(ie_tipo_dado_p	= 'SQ') then
	select	max(nr_contrato)
	into	nr_contrato_w
	from	pls_tabela_preco
	where	nr_sequencia = nr_seq_tabela_p;
	
	if	(nr_contrato_w is not null) then
		select	nr_sequencia
		into	ds_retorno_w
		from	pls_contrato
		where	nr_contrato	= nr_contrato_w;
	else
		ds_retorno_w := '';
	end if;
elsif	(ie_tipo_dado_p	= 'DR') then
	select	max(nr_contrato)
	into	nr_contrato_w
	from	pls_tabela_preco
	where	nr_sequencia = nr_seq_tabela_p;
	
	select	max(nr_contrato)
	into	nr_contrato_w
	from	pls_tabela_preco
	where	nr_sequencia = nr_seq_tabela_p;
	
	if	(nr_contrato_w is not null) then
		select	to_char(dt_reajuste,'dd/mm/yyyy')
		into	ds_retorno_w
		from	pls_contrato
		where	nr_sequencia	= nr_contrato_w;
	else
		ds_retorno_w := '';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_dados_tabela_preco;
/
