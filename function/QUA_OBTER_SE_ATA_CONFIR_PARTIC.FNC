create or replace
function qua_obter_se_ata_confir_partic(
				nr_seq_ata_p	number)
				return varchar2 is
				
ds_retorno_w			varchar2(1) := 'N';
qt_partic_ata_w			number(10,0);
qt_partic_ciente_ata_w		number(10,0);

begin


select	count(*)
into	qt_partic_ata_w
from	proj_ata_participante
where	cd_pessoa_participante is not null
and	nr_seq_ata = nr_seq_ata_p;

if	(qt_partic_ata_w > 0) then
	begin
	select	count(*)
	into	qt_partic_ciente_ata_w
	from	proj_ata_participante
	where	cd_pessoa_participante is not null
	and	dt_ciente_ata is not null
	and	nr_seq_ata = nr_seq_ata_p;
	
	if	(qt_partic_ata_w = qt_partic_ciente_ata_w) then
		ds_retorno_w := 'S';
	elsif	(qt_partic_ata_w > qt_partic_ciente_ata_w) and
		(qt_partic_ciente_ata_w > 0) then
		ds_retorno_w := 'P';
	end if;
	end;
end if;

return	ds_retorno_w;

end qua_obter_se_ata_confir_partic;
/