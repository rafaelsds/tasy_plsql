create or replace 
FUNCTION hemolabor_volume_ordem_prod
(
			nr_seq_ordem_p	Number)
			return number is

cd_material_w		number(6,0);	
cd_unid_med_w		Varchar2(30);
qt_dose_w		Number(15,3);
qt_volume_final_w	number(14,4);
qt_dose_conv_w		number(14,4);


Cursor C01 is
	select	b.cd_material,
		qt_dose,
		cd_unidade_medida
	from	can_ordem_item_prescr b,
		material a
	where	b.nr_seq_ordem		= nr_seq_ordem_p
	and	a.cd_material		= b.cd_material;

BEGIN
qt_volume_final_w := 0;

open C01;
loop
	fetch C01 into
		cd_material_w,
		qt_dose_w,
		cd_unid_med_w;
	exit when C01%notfound;
	begin
	if	(upper(cd_unid_med_w) <> 'ML') then
		begin
		select obter_conversao_ml(cd_material_w, qt_dose_w, cd_unid_med_w) into qt_dose_conv_w from dual;
		qt_volume_final_w := qt_volume_final_w + qt_dose_conv_w;
		end;
	elsif	(upper(cd_unid_med_w) = 'ML') then
		qt_volume_final_w := qt_volume_final_w +  qt_dose_w;
	end if;	
	end;
end loop;
close C01;

return	qt_volume_final_w;

END hemolabor_volume_ordem_prod;
/