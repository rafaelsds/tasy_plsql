create or replace
function obter_paciente_presenca (nr_seq_curso_p	number,
				nr_seq_modulo_p		number,
				cd_pessoa_fisica_p	number,
				nr_seq_evento_p		number,
				dt_presenca_p		date)
				return varchar2 is

ds_retorno_w	varchar2(1);
ie_presente_w	varchar2(1);

begin

select	b.ie_presente
into	ie_presente_w
from	tre_inscrito a,
	tre_inscrito_presenca b,
	tre_evento c,
	tre_curso d,
	tre_evento_modulo e
where	a.nr_sequencia	= b.nr_seq_inscrito
and	c.nr_sequencia	= a.nr_seq_evento
and	c.nr_seq_curso	= d.nr_sequencia(+)
and	c.nr_sequencia = e.nr_seq_evento(+)
and	((d.nr_sequencia	= nr_seq_curso_p) or (nr_seq_curso_p = 0))
and	((c.nr_sequencia	= nr_seq_evento_p) or (nr_seq_evento_p = 0))
and	((e.nr_sequencia = nr_seq_modulo_p) or (nr_seq_modulo_p = 0))
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
and	to_char(b.dt_presenca, 'dd/mm/yyyy') = to_char(dt_presenca_p, 'dd/mm/yyyy');


if	ie_presente_w = 'S'	then
	ds_retorno_w := 'P';
elsif	ie_presente_w = 'N'	then
	ds_retorno_w := 'F';
end if;

return	ds_retorno_w;

end obter_paciente_presenca;
/
