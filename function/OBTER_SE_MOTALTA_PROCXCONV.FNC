create or replace
function Obter_Se_MotAlta_procXConv	(	nr_atendimento_p	Number,
						cd_motivo_alta_p	Number)
 		    	return varchar2 is

ds_retorno_w		Varchar2(1) := 'S';
qt_existe_w		Number(10);
cd_procedimento_w	Number(20);
ie_origem_proced_w	Number(20);
cd_convenio_w		Number(20);

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		cd_convenio
	from	regra_mot_alta_conv_proc
	where	ie_situacao = 'A'
	and	cd_motivo_alta = cd_motivo_alta_p;
	
begin
if (nvl(cd_motivo_alta_p,0) > 0) and (nvl(nr_atendimento_p,0) > 0) then

	select 	count(*)
	into	qt_existe_w
	from	regra_mot_alta_conv_proc
	where	ie_situacao = 'A'
	and	cd_motivo_alta = cd_motivo_alta_p;

	if (qt_existe_w > 0) then
		ds_retorno_w := 'N';
		
		open C01;
		loop
		fetch C01 into	
			cd_procedimento_w,
			ie_origem_proced_w,
			cd_convenio_w;
		exit when C01%notfound;
			begin
			
			if (ds_retorno_w = 'N') then
			
				select 	decode(count(*),0,'N','S')
				into	ds_retorno_w
				from	procedimento_paciente
				where	cd_procedimento = cd_procedimento_w
				and	ie_origem_proced = ie_origem_proced_w
				and	(obter_convenio_atendimento(nr_atendimento) = cd_convenio_w or cd_convenio_w is null)
				and	nr_atendimento = nr_atendimento_p;
				
			end if;
			end;
		end loop;
		close C01;				
	end if;
	
end if;

return	ds_retorno_w;

end Obter_Se_MotAlta_procXConv;
/