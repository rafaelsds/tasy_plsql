create or replace
function Obter_procedimento_associado ( nr_sequencia_proc_p	Number,
					nr_prescricao_p		Number)
					return Number is

/* Retorna 0 (Zero) caso o procedimento ainda n�o tenha sido executado,
   Retorna o nr_sequencia da Procedimento_paciente caso j� tenha sido executado*/

nr_sequencia_w			number(10):= 0;
qt_executado_w			number(8):= 0;

begin

select 	count(*)
into	qt_executado_w
from 	procedimento_paciente
where 	nr_prescricao = nr_prescricao_p
and 	nr_sequencia_prescricao = nr_sequencia_proc_p;

if	(qt_executado_w > 0) then
	select 	max(nr_sequencia)
	into	nr_sequencia_w
	from 	procedimento_paciente
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_sequencia_prescricao = nr_sequencia_proc_p;
else
	nr_sequencia_w:= 0;
end if;	

return nr_sequencia_w;

end Obter_procedimento_associado;
/