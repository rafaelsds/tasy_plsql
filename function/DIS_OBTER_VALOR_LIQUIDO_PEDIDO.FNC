create or replace
function dis_obter_valor_liquido_pedido	(nr_pedido_p	number)
				return Number is

vl_total_pedido_w	number(13,2);

begin
select	vl_total_pedido
into	vl_total_pedido_w
from	dis_pedido
where	nr_sequencia = nr_pedido_p;

return vl_total_pedido_w;
end dis_obter_valor_liquido_pedido;
/