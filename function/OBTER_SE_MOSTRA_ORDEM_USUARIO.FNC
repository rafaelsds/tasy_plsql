create or replace
function obter_se_mostra_ordem_usuario(	nr_ordem_compra_p		number,
					nm_usuario_p			varchar2)
 		    	return varchar2 is

ie_mostra_w			varchar2(1) := 'N';
ie_possui_cc_local_w		varchar2(1) := 'N';
cd_centro_custo_w			number(10);
cd_local_estoque_w		number(04);

cursor c01 is
select	distinct
	a.cd_centro_custo,
	a.cd_local_estoque
from	ordem_compra_item a
where	a.nr_ordem_compra = nr_ordem_compra_p
and	(a.cd_centro_custo is not null or a.cd_local_estoque is not null);

begin

open C01;
loop
fetch C01 into
	cd_centro_custo_w,
	cd_local_estoque_w;
exit when C01%notfound;
	begin

	ie_possui_cc_local_w	:= 'S';

	if	(ie_mostra_w = 'N') then
				
		if	(ie_mostra_w = 'N') and
			(cd_centro_custo_w is not null) and
			(obter_se_cc_lib_usuario(nm_usuario_p, cd_centro_custo_w) = 'S') then
			ie_mostra_w := 'S';
		end if;
		
		if	(ie_mostra_w = 'N') and
			(cd_local_estoque_w is not null) and
			(obter_se_local_lib_usuario(nm_usuario_p, cd_local_estoque_w) = 'S') then
			ie_mostra_w := 'S';
		end if;
	end if;
	end;
end loop;
close C01;

/*tratamento para mostrar a ordem, caso ela n�o tenha nenhum centro de custo ou local de estoque. Ou seja, dai essa ordem pode mostrar*/
if	(ie_possui_cc_local_w = 'N') then
	ie_mostra_w := 'S';
end if;

return	ie_mostra_w;

end obter_se_mostra_ordem_usuario;
/