create or replace
function obter_se_proc_edicao2
		(cd_procedimento_p	number,
		ie_origem_proced_p	number,
		cd_edicao_amb_p		number)
		return varchar2 is

ie_liberado_w	varchar2(01)	:= 'N';

begin

select	nvl(max('S'), 'N') 
into	ie_liberado_w
from	preco_amb
where	cd_edicao_amb	= cd_edicao_amb_p
and	cd_procedimento	= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

return	ie_liberado_w;

end obter_se_proc_edicao2;
/