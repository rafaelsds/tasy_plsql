create or replace
function obter_data_tev_profilaxia(  	nr_atendimento_p		number,
										ie_risco_alto_p 		Varchar2 default null,
										ie_risco_baixo_p 		Varchar2 default null,
										ie_risco_intermed_p 	Varchar2 default null,
										ie_risco_nao_aplica_p 	Varchar2 default null)	
										return Date is
			
ds_retorno_w		Date := Sysdate;
nr_seq_tev_min_w	Number(10);
dt_avaliacao_tev_w	Date;
qt_proc_tev_w		NUmber(10); 
qt_mat_tev_w		NUmber(10);
qt_recomend_tev_w	NUmber(10);
			
begin

select	nvl(min(nr_sequencia),0)
into	nr_seq_tev_min_w
from	escala_tev
where	nr_atendimento	= nr_atendimento_p
and		(((ie_risco  = ie_risco_alto_p) and (ie_risco_alto_p is not null)) or	 
		((ie_risco  = ie_risco_baixo_p) and (ie_risco_baixo_p is not null)) or
		((ie_risco  = ie_risco_intermed_p) and (ie_risco_intermed_p is not null)) or
		((ie_risco  = ie_risco_nao_aplica_p) and (ie_risco_nao_aplica_p is not null)) or
		((ie_risco_alto_p is null) and (ie_risco_baixo_p is null) and (ie_risco_intermed_p is null) and (ie_risco_nao_aplica_p is null)))
and	dt_inativacao is null;


if	 (nr_seq_tev_min_w > 0 ) and (nr_atendimento_p > 0)then

		Select	max(dt_avaliacao)
		into	dt_avaliacao_tev_w
		from	escala_tev
		where	nr_sequencia = 	nr_seq_tev_min_w;		
			
		ds_retorno_w	:= dt_avaliacao_tev_w;
			
end if;

return	ds_retorno_w;

end obter_data_tev_profilaxia;
/
