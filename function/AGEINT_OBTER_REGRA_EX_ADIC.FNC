create or replace
function Ageint_Obter_Regra_Ex_Adic(
			nr_seq_proc_interno_p		number,
			cd_estabelecimento_p		number,
			cd_agenda_p			number,
			cd_convenio_p			number,
			cd_categoria_p			varchar2,
			cd_plano_p			varchar2)
 		    	return number is

vl_retorno_w		number(10)	:= 0;
qt_exclusivo_w		number(10);
nr_seq_regra_w		number(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
cd_area_proced_w	number(8);
cd_espec_proced_w	number(8);
cd_grupo_proced_w	number(15);
			
begin
if	(nr_seq_proc_interno_p	> 0) then
	obter_proc_tab_interno_conv(
					nr_seq_proc_interno_p,
					cd_estabelecimento_p,
					cd_convenio_p,
					cd_categoria_p,
					cd_plano_p,
					null,
					cd_procedimento_w,
					ie_origem_proced_w,
					null,
					sysdate,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);

	select	nvl(max(cd_area_procedimento),0),
		nvl(max(cd_especialidade),0),
		nvl(max(cd_grupo_proc),0)
	into	cd_area_proced_w,
		cd_espec_proced_w,
		cd_grupo_proced_w
	from	estrutura_procedimento_v
	where	cd_procedimento = cd_procedimento_w
	and	ie_origem_proced = ie_origem_proced_w; 

	select	count(*)
	into	qt_exclusivo_w
	from	agenda_cons_regra_proc
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	nvl(cd_agenda,cd_agenda_p) = cd_agenda_p
	and	((cd_convenio = cd_convenio_p) or (cd_convenio is null))
	and	nvl(ie_permite,'S') = 'E';

	if	(qt_exclusivo_w > 0) then

		select	nvl(max(nr_sequencia),0)
		into	vl_retorno_w
		from	agenda_cons_regra_proc
		where	cd_estabelecimento 	= cd_estabelecimento_p
		and	nvl(cd_agenda,cd_agenda_p) = cd_agenda_p
		and	((cd_convenio 		= cd_convenio_p) or (cd_convenio is null))
		and	((cd_categoria = cd_categoria_p) or (cd_categoria is null))
		and	((cd_plano_convenio = cd_plano_p) or (cd_plano_convenio is null))
		and	((cd_area_procedimento = cd_area_proced_w) or (cd_area_procedimento is null))
		and	((cd_especialidade = cd_espec_proced_w) or (cd_especialidade is null))
		and	((cd_grupo_proc = cd_grupo_proced_w) or (cd_grupo_proc is null))
		and	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
		and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null)))
		and	((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
		and	nvl(ie_permite,'S') = 'E';
	else
		select	nvl(max(nr_sequencia),0)
		into	vl_retorno_w
		from	agenda_cons_regra_proc 
		where	cd_estabelecimento = cd_estabelecimento_p
		and	nvl(cd_agenda,cd_agenda_p) = cd_agenda_p
		and	((cd_convenio = cd_convenio_p) or (cd_convenio is null))
		and	((cd_categoria = cd_categoria_p) or (cd_categoria is null))
		and	((cd_plano_convenio = cd_plano_p) or (cd_plano_convenio is null))
		and	((cd_area_procedimento = cd_area_proced_w) or (cd_area_procedimento is null))
		and	((cd_especialidade = cd_espec_proced_w) or (cd_especialidade is null))
		and	((cd_grupo_proc = cd_grupo_proced_w) or (cd_grupo_proc is null))
		and	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
		and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null)))
		and	((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
		and	nvl(ie_permite,'S')	= 'S';
	end if;
end if;

return	vl_retorno_w;

end Ageint_Obter_Regra_Ex_Adic;
/
