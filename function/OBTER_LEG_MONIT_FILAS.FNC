create or replace
function obter_leg_monit_filas(	qt_media_atendimento_p	number,
				qt_tempo_medio_p	number,
				QT_TEMPO_MAXIMO_p	number)
 		    	return varchar2 is
ie_retorno_w	varchar2(10);
begin

if	(qt_media_atendimento_p	is not null) and
	(qt_tempo_medio_p	is not null) and
	(qt_tempo_maximo_p	is not null) and
	(qt_tempo_medio_p	> 0) and
	(qt_tempo_maximo_p	> 0) then
	
	if	(qt_media_atendimento_p	>= QT_TEMPO_MEDIO_p) and
		(qt_media_atendimento_p	< QT_TEMPO_MAXIMO_p) then
		ie_retorno_w	:= 'ME';
	elsif	(qt_media_atendimento_p	>= QT_TEMPO_MAXIMO_p) then
		ie_retorno_w	:= 'MA';
	end if;
	
	
end if;


return	ie_retorno_w;

end obter_leg_monit_filas;
/