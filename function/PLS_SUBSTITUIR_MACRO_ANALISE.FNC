create or replace
function pls_substituir_macro_analise
			(	ds_macro_p	varchar2,
				nm_atributo_p	varchar2,
				vl_atributo_p	number)
				return varchar2 is

cd_procedimento_w		number(15);
nr_seq_material_w		number(10);
ds_resultado_w			varchar2(255);

begin

if	(upper(nm_atributo_p)		= 'NR_SEQ_SEGURADO') then
	
	if	(ds_macro_p	= '@BENEFICIARIO') then
		ds_resultado_w := To_char(vl_atributo_p);
	end if;
elsif	(upper(nm_atributo_p)		= 'NR_SEQ_ITEM') then
	
	select	a.cd_procedimento,
		a.nr_seq_material
	into	cd_procedimento_w,
		nr_seq_material_w
	from	pls_auditoria_item a
	where	nr_sequencia = vl_atributo_p;
	
	if	(ds_macro_p	= '@PROCEDIMENTO') then
		ds_resultado_w	:= To_char(cd_procedimento_w);
	elsif	(ds_macro_p	= '@MATERIAL') then
		ds_resultado_w	:= To_char(nr_seq_material_w);
	end if;		
end if;

return	ds_resultado_w;

end pls_substituir_macro_analise;
/
