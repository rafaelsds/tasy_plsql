create or replace
function Obter_se_exame_bloq_result (nr_prescricao_p	number,
				nr_seq_prescr_p	number)
				return varchar2 is

IE_RESTRINGE_RESULTADO_w	varchar2(1);

begin

select 	nvl(max(IE_RESTRINGE_RESULTADO),'N')
into	IE_RESTRINGE_RESULTADO_w
from 	exame_lab_resultado b,
	exame_lab_result_item a
where	a.nr_seq_resultado = b.nr_seq_resultado
and	a.nr_seq_material is not null
and	b.nr_prescricao = nr_prescricao_p
and	a.nr_seq_prescr = nr_seq_prescr_p;



return	IE_RESTRINGE_RESULTADO_w;

end Obter_se_exame_bloq_result;
/