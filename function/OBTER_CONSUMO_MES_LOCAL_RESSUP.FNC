create or replace
function obter_consumo_mes_local_ressup(	ie_tipo_p				varchar2,
				cd_estabelecimento_p		number,
				cd_material_p			number,
				dt_mesano_referencia_p		date,
				cd_local_estoque_p		number)
				return number is

ds_retorno_w			number(18,4);
qt_consumo_w			number(18,4);
vl_consumo_w			number(18,2);
dt_mesano_referencia_w		date;
ie_consumo_ressup_w		varchar2(1);

begin

if	(dt_mesano_referencia_p is null) then
	dt_mesano_referencia_w	:= trunc(sysdate,'mm');
else
	dt_mesano_referencia_w	:= trunc(dt_mesano_referencia_p,'mm');
end if;

select	nvl(ie_consumo_ressup,'S')
into	ie_consumo_ressup_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

select	sum(nvl(a.qt_consumo,0)) qt_estoque,
	sum(nvl(a.vl_consumo,0)) vl_consumo
into	qt_consumo_w,
	vl_consumo_w
from	movto_estoque_operacao_v a,
	operacao_estoque o
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_material		= cd_material_p
and	o.cd_operacao_estoque	= a.cd_operacao_estoque
and	trunc(a.dt_mesano_referencia,'mm') = trunc(dt_mesano_referencia_w,'mm')
and	((ie_consumo_ressup_w = 'S') or
	((ie_consumo_ressup_w = 'N') and (nvl(o.ie_desconsidera_cons_ressup,'N') = 'N')))
and	a.cd_local_estoque = cd_local_estoque_p;

if	(ie_tipo_p = 'Q') then
	ds_retorno_w := qt_consumo_w;
elsif	(ie_tipo_p = 'V') then
	ds_retorno_w := vl_consumo_w;
end if;

return  ds_retorno_w;

end obter_consumo_mes_local_ressup;
/
