create or replace
function tx_obter_dados_doador_orgao (	nr_seq_doador_orgao_p		number,
					ie_opcao_p			varchar2)
					return				varchar2 is
					
nm_orgao_w		varchar2(80);
nm_doador_w		varchar2(80);
ds_resultado_w		varchar2(255);

begin

select	substr(tx_obter_nome_orgao(nr_seq_orgao),1,80),
	substr(tx_obter_dados_doador(nr_seq_doador, 'N'),1,80)
into	nm_orgao_w,
	nm_doador_w
from	tx_doador_orgao
where	nr_sequencia	= nr_seq_doador_orgao_p;

if	(ie_opcao_p = 'D') then
	ds_resultado_w	:= nm_doador_w;
elsif	(ie_opcao_p = 'O') then
	ds_resultado_w	:= nm_orgao_w;
elsif	(ie_opcao_p = 'DO') then
	ds_resultado_w	:= nm_doador_w||' - '||nm_orgao_w;
end if;

return	ds_resultado_w;

end tx_obter_dados_doador_orgao;
/