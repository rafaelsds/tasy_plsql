create or replace 
function Obter_Clinica_Atend(	nr_atendimento_p	number,
								ie_opcao_p			varchar2)
		return varchar2 is

ie_clinica_w	atendimento_paciente.ie_clinica%type;
begin

if	(nr_atendimento_p	is not null) then
	select	max(ie_clinica)
	into	ie_clinica_w
	from 	atendimento_paciente
	where 	nr_atendimento	= nr_atendimento_p;
	
	if	(ie_opcao_p	= 'C') then
		return ie_clinica_w;
	else
		return obter_valor_dominio(17, ie_clinica_w);
	end if;
end if;

return 	'';

end Obter_Clinica_Atend;
/