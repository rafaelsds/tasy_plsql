CREATE OR REPLACE
FUNCTION obter_desc_familia_material(  	cd_material_p		Number)
					return VARCHAR2 IS

ds_retorno_w				Varchar2(255);

BEGIN

select	b.ds_familia
into	ds_retorno_w
from	material_familia b,
	material a
where	a.nr_seq_familia	= b.nr_sequencia
and	a.cd_material		= cd_material_p;
	
return ds_retorno_w;

END obter_desc_familia_material;
/