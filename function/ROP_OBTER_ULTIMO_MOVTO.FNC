create or replace
function	rop_obter_ultimo_movto(nr_seq_roupa_p	number)
return date is

dt_liberacao_w			date;

begin

select	max(a.dt_liberacao)
into	dt_liberacao_w
from	rop_lote_movto a,
	rop_movto_roupa b
where	a.nr_sequencia = b.nr_seq_lote
and	b.nr_seq_roupa = nr_seq_roupa_p;

return	dt_liberacao_w;

end	rop_obter_ultimo_movto;
/