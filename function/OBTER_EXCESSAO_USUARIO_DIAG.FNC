create or replace 
function obter_excessao_usuario_diag ( 	nr_sequencia_p 	in cpoe_rule_consist_diag.nr_sequencia%type,
					nm_usuario_p 	in usuario.nm_usuario%type) return varchar is

ds_retorno_w	varchar2(1 char);

begin

ds_retorno_w := 'S';

select 	nvl(max('N'),'S')
into 	ds_retorno_w
from 	cpoe_rule_consist_user
where	nr_seq_cpoe_rule = nr_sequencia_p;

if (ds_retorno_w = 'N') then

	select 	nvl(max('N'), 'S')
	into	ds_retorno_w
	from 	cpoe_rule_consist_user
	where 	nm_usuario_regra = nm_usuario_p
	and	nr_seq_cpoe_rule = nr_sequencia_p;
end if;

return ds_retorno_w;

end obter_excessao_usuario_diag;
/