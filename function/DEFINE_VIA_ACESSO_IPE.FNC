create or replace
function Define_via_acesso_ipe
		(nr_interno_conta_p		number,
		cd_item_p				number,
		pr_via_acesso_p			number)
		return number is

/* 00 - �nica ou principal				Quando existe somente um procedimento */
/* 31 - Mesma via de honor�rio anterior		duas cirurgias diferentes c/mesma via */
/* 32 - vias diferentes					duas cirurgias diferentes c/via diferente */
/* 33 - cirurgia bilateral				mesma cirurgia Ex:(perna esquerda e direita) */

ie_vias_acesso_ipe_w	number(2);
qt_cirurgias_w		number(5);


begin
/* Inicializa com padr�o */
ie_vias_acesso_ipe_w	:= 00;

select count(*)
into	 qt_cirurgias_w
from	 estrutura_procedimento_v a,
	 procedimento_paciente b
where	 b.nr_interno_conta	= nr_interno_conta_p
and	 b.cd_procedimento	= a.cd_procedimento
and	 b.ie_origem_proced	= a.ie_origem_proced	
and	 a.cd_area_procedimento	= 4
and	 b.cd_procedimento_convenio <> '16000005';

if	 (qt_cirurgias_w	= 1) then
	 ie_vias_acesso_ipe_w	:= 00;
else
	 begin
	 select count(*)
	 into	 qt_cirurgias_w
	 from	 procedimento_paciente b
	 where b.nr_interno_conta	= nr_interno_conta_p	
	 and	 b.cd_procedimento	= cd_item_p;
	 if	 (qt_cirurgias_w	> 1) then
	 	 ie_vias_acesso_ipe_w	:= 33;
	 else
		 begin
		 if	(pr_via_acesso_p	= 100) then
	 	 	ie_vias_acesso_ipe_w	:= 32;
		 else
	 	 	ie_vias_acesso_ipe_w	:= 31;
		 end if;
		 end;
	 end if;
	end;
end if;

return ie_vias_acesso_ipe_w;

end;
/
