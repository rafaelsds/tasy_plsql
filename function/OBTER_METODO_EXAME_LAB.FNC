create or replace
function Obter_Metodo_Exame_Lab(nr_seq_metodo_p        number,
                                ie_opcao_p              number)
                                return varchar2 is
nr_sequencia_w             number(10);
ds_metodo_exame_w        varchar2(255);
ds_retorno_w       varchar2(255);
/* ie_opcao_p
   1 - nr_sequencia
   2 - ds_metodo_exame
*/
begin
ds_retorno_w := '';
if	(nr_seq_metodo_p is not null) then
	select	nr_sequencia,
		ds_metodo
	into	nr_sequencia_w,
		ds_metodo_exame_w
	from metodo_exame_lab
	where nr_sequencia = nvl(nr_seq_metodo_p,nr_sequencia);

	if (ie_opcao_p = 1) then
	   ds_retorno_w := nr_sequencia_w;
	elsif      (ie_opcao_p = 2) then
	   ds_retorno_w := ds_metodo_exame_w;
	end if;
end if;

return ds_retorno_w;
end;
/
