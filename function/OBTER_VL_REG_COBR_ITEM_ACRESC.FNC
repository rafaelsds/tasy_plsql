create or replace
function obter_vl_reg_cobr_item_acresc(	nr_seq_registro_p	number,
					ie_atual_prevista_p	varchar2)
 		    	return number is

vl_retorno_w		number(15,2) := 0;	
dt_prev_pagto_w		date;		
nr_seq_cheque_w		number(10);
nr_titulo_w		number(10);
dt_referencia_w		date;
			
begin
if	(nr_seq_registro_p is not null) then
	select	sysdate,
		b.nr_seq_cheque,
		b.nr_titulo,
		nvl(c.dt_prev_pagto,sysdate)
	into	dt_prev_pagto_w,
		nr_seq_cheque_w,
		nr_titulo_w,
		dt_prev_pagto_w
	from	registro_cobranca c,
		cobranca b,
		registro_cobr_item a
	where 	a.nr_seq_cobranca	= b.nr_sequencia
	and	a.nr_seq_registro	= c.nr_sequencia
	and	a.nr_sequencia		= nr_seq_registro_p;
	
	if	(ie_atual_prevista_p = 'A') then
		dt_referencia_w	:= sysdate;
	elsif	(ie_atual_prevista_p = 'P') then	
		dt_referencia_w	:= dt_prev_pagto_w;
	end if;	
	
	if	(nr_titulo_w is not null) then
		vl_retorno_w := nvl(obter_juros_multa_titulo(nr_titulo_w, dt_referencia_w, 'R', 'A'),0);
	elsif	(nr_seq_cheque_w is not null) then
		vl_retorno_w := nvl(obter_juro_multa_cheque(nr_seq_cheque_w, dt_referencia_w, 'R', 'A'),0);
	end if;	
end if;	

return	vl_retorno_w;

end obter_vl_reg_cobr_item_acresc;
/