create or replace
function obter_horas_ativ_rat(	nr_seq_atividade_p		number,
			nr_seq_rat_p		number	)
 		    	return number is
	
qt_horas_w 	number;	

begin

select 	sum(a.qt_min_ativ)/60
into 	qt_horas_w
from	proj_rat_ativ a,
	proj_cron_etapa b,
	proj_rat c
where 	a.nr_seq_etapa_cron = b.nr_sequencia
and	a.nr_seq_rat = c.nr_sequencia
and	c.nr_sequencia = nr_seq_rat_p
and	b.nr_sequencia = nr_seq_atividade_p;

return	qt_horas_w;

end obter_horas_ativ_rat;
/