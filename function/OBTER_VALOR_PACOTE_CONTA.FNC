create or replace
function obter_valor_pacote_conta(	nr_interno_conta_p		number,
					cd_setor_atendimento_p		number,
					nr_atendimento_p		number)
			return number is
			
vl_retorno_w		number(15,2) := 0;
nr_seq_proc_pacote_w	number(10);	


Cursor C01 is
	select	nvl(nr_seq_proc_pacote,0)
	from	procedimento_paciente
	where	nr_interno_conta = nr_interno_conta_p
	group by nr_seq_proc_pacote;

Cursor C02 is
	select	nvl(nr_seq_proc_pacote,0)
	from	procedimento_paciente
	where	cd_setor_atendimento = cd_setor_atendimento_p
	and 	nr_atendimento = nr_atendimento_p
	and 	nr_interno_conta is not null
	group by nr_seq_proc_pacote;
	
Cursor C03 is
	select	nvl(nr_seq_proc_pacote,0)
	from	procedimento_paciente 
	where	nr_atendimento = nr_atendimento_p
	and 	nr_interno_conta is not null
	group by nr_seq_proc_pacote;
	

		
begin
if (nr_interno_conta_p is not null) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_proc_pacote_w;
	exit when C01%notfound;
		begin
		if (nr_seq_proc_pacote_w > 0) then
			vl_retorno_w:= vl_retorno_w + nvl(obter_valor_pacote(nr_seq_proc_pacote_w),0);
		end if;
		end;
	end loop;
	close C01;
elsif (cd_setor_atendimento_p is not null) then
	open C02;
	loop
	fetch C02 into	
		nr_seq_proc_pacote_w;
	exit when C02%notfound;
		begin
		if (nr_seq_proc_pacote_w > 0) then
			vl_retorno_w:= vl_retorno_w +  nvl(obter_valor_pacote(nr_seq_proc_pacote_w),0);
		end if;
		end;
	end loop;
	close C02;
elsif (nr_atendimento_p is not null) then
	open C03;
	loop
	fetch C03 into	
		nr_seq_proc_pacote_w;
	exit when C03%notfound;
		begin
		if (nr_seq_proc_pacote_w > 0) then
			vl_retorno_w:= vl_retorno_w +  nvl(obter_valor_pacote(nr_seq_proc_pacote_w),0);
		end if;
		end;
	end loop;
	close C03;
end if;

return	vl_retorno_w;

end obter_valor_pacote_conta;
/