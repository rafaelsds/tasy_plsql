Create or Replace
Function Obter_Cod_Procedimento_Pac (nr_sequencia_p		number)
				return number is
cd_procedimento_w	number(15);

begin

begin
select	nvl(max(cd_procedimento),0)
into	cd_procedimento_w
from 	procedimento_paciente
where 	nr_sequencia	= nr_sequencia_p;
exception
	when others then
	cd_procedimento_w:= 0;
end;

return cd_procedimento_w;

end Obter_Cod_Procedimento_Pac;
/