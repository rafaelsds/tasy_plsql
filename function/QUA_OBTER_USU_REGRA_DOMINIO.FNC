create or replace
function Qua_Obter_Usu_Regra_Dominio(
				nr_seq_regra_p	number,
				ie_usuario_p	varchar)
				return varchar is

ds_usuario_dominio_w		varchar2(254) := null;

begin
if	(nr_seq_regra_p is not null) then
	begin
	if	(substr(qua_tipo_evento_regra(nr_seq_regra_p),1,10) in (1,8,9,10,11,12,13,14,16,19,21,22,26,27,28,29,31,33,34,35,36)) then
		begin
		select	ds_valor_dominio
		into	ds_usuario_dominio_w
		from	valor_dominio
		where	cd_dominio = 1677
		and	vl_dominio = ie_usuario_p;
		end;
	elsif	(substr(qua_tipo_evento_regra(nr_seq_regra_p),1,10) in (2,3,4,5,6,7,17,18,20,21,23,25,37)) then
		begin
		select	ds_valor_dominio
		into	ds_usuario_dominio_w
		from	valor_dominio
		where	cd_dominio = 1678
		and	vl_dominio = ie_usuario_p;
		end;
	elsif	(substr(qua_tipo_evento_regra(nr_seq_regra_p),1,10) in (15,21)) then
		begin
		select	ds_valor_dominio
		into	ds_usuario_dominio_w
		from	valor_dominio
		where	cd_dominio = 2308
		and	vl_dominio = ie_usuario_p;
		end;
	elsif	(substr(qua_tipo_evento_regra(nr_seq_regra_p),1,10) in (24,30)) then
		begin
		select	ds_valor_dominio
		into	ds_usuario_dominio_w
		from	valor_dominio
		where	cd_dominio = 4318
		and	vl_dominio = ie_usuario_p;
		end;
	elsif	(substr(qua_tipo_evento_regra(nr_seq_regra_p),1,10) in (39)) then
		begin
		select	ds_valor_dominio
		into	ds_usuario_dominio_w
		from	valor_dominio
		where	cd_dominio = 1678
		and	vl_dominio = ie_usuario_p;
		end;
	
	end if;
	end;
end if;

return ds_usuario_dominio_w;

end Qua_Obter_Usu_Regra_Dominio;
/

