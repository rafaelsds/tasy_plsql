create or replace
function	agenda_color_mm_status (cd_agenda_p number,
			      dt_agenda_p date)
	return varchar is

ie_color_w		varchar2(10) := '';
qt_all_w        number(10);
qt_avail_w      number(10);
qt_booked_w     number(10);
qt_holiday_w    number(10);

begin

select   count(1) 
into     qt_holiday_w
from     feriado 
where    cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento 
and      pkg_date_utils.start_of(dt_feriado, 'dd') = dt_agenda_p;

if (qt_holiday_w > 0) then
    ie_color_w := 'YELLOW';
    goto finalize;
end if;

select  count(1)
into    qt_all_w
from    agenda_consulta
where   cd_agenda = cd_agenda_p
and     pkg_date_utils.start_of(dt_agenda, 'dd') = dt_agenda_p;

if (qt_all_w = 0) then
    goto finalize;
end if;

select  count(1)
into    qt_avail_w
from    agenda_consulta
where   cd_agenda = cd_agenda_p
and     pkg_date_utils.start_of(dt_agenda, 'dd') = dt_agenda_p
and     ie_status_agenda = 'L';

if (qt_avail_w > 0) then
    ie_color_w := 'BLUE';
    goto finalize;
end if;

select  count(1)
into    qt_booked_w
from    agenda_consulta
where   cd_agenda = cd_agenda_p
and     pkg_date_utils.start_of(dt_agenda, 'dd') = dt_agenda_p
and     ie_status_agenda <> 'L';

if (qt_all_w = qt_booked_w and qt_avail_w = 0) then
    ie_color_w := 'RED';
end if;

<<finalize>>

return ie_color_w;

end agenda_color_mm_status;
/
