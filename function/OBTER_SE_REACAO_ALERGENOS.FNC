create or replace
function obter_se_reacao_alergenos( 	nr_seq_alergenos_p	number,
										nr_seq_reacao_p     number)
 		    	return varchar2 is

ds_retorno_w         varchar(255) := 'S';
qt_reg_w			number(10);
cd_perfil_w			number(10) := obter_perfil_ativo;
begin

if 	(nvl(nr_seq_reacao_p,0)  > 0) and
	(nvl(nr_seq_alergenos_p,0) > 0) then

	select 	count(*)
	into 	qt_reg_w
	from 	tipo_alergia_reacao
	where	nr_seq_tipo = nr_seq_alergenos_p;
	

	if (qt_reg_w > 0) then
	
	   select	count(*)
	   into     qt_reg_w
	   from  	tipo_alergia_reacao
	   where	nr_seq_tipo = nr_seq_alergenos_p
	   and		nr_seq_reacao = nr_seq_reacao_p;
	   
	  if	(qt_reg_w = 0) then
	  
			ds_retorno_w := 'N';
			
	  end if;
	   
	end if;
	
end if;

return	ds_retorno_w;

end obter_se_reacao_alergenos;
/
