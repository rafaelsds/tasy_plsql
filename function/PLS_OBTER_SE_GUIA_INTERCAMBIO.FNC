create or replace
function pls_obter_se_guia_intercambio
			(	cd_guia_p		pls_guia_plano.cd_guia %type,
				nr_seq_guia_p		pls_guia_plano.nr_sequencia%type)
				return varchar2 is

ie_tipo_processo_w	pls_guia_plano.ie_tipo_processo%type;
begin

if	(cd_guia_p is not null)	then
	begin
		select	ie_tipo_processo
		into	ie_tipo_processo_w
		from	pls_guia_plano
		where 	cd_guia = cd_guia_p;
	exception
	when others then
		ie_tipo_processo_w := null;
	end;
elsif	(nr_seq_guia_p is not null)	then
	begin
		select	ie_tipo_processo
		into	ie_tipo_processo_w
		from	pls_guia_plano
		where	nr_sequencia = nr_seq_guia_p;
	exception
	when others then
		ie_tipo_processo_w := null;
	end;
end if;

if	(ie_tipo_processo_w = 'I') then
	return 'S';
else
	return 'N';
end if;

end pls_obter_se_guia_intercambio;
/
