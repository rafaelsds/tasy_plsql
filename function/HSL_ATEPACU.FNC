create or replace FUNCTION HSL_ATEPACU (
nr_atendimento_p NUMBER,
ie_opcao_p NUMBER)
RETURN NUMBER IS

/* UNIDADE DO PACIENTE*/
/*1 = Primeira Unidade */
/*2 = Unidade Atual  */

nr_seq_atepacu_w NUMBER(10,0);
ie_tipo_atendimento_w NUMBER;

--Obs: Function buscada do banco e documentada devido ao erro no relatório do Java

BEGIN

SELECT ie_tipo_atendimento
INTO ie_tipo_atendimento_w
FROM atendimento_paciente
WHERE nr_atendimento = nr_atendimento_p;



IF (ie_opcao_p = 1) THEN
    IF (ie_tipo_atendimento_w = 1) THEN
        SELECT MIN(nr_seq_interno)
        INTO nr_seq_atepacu_w
        FROM atend_paciente_unidade u,Setor_atendimento s
        WHERE u.nr_atendimento=nr_atendimento_p AND
              u.cd_setor_atendimento=s.cd_setor_atendimento AND s.ie_ocup_hospitalar='S' AND
              u.dt_entrada_unidade=(SELECT MIN(x.dt_entrada_unidade) FROM atend_paciente_unidade x,Setor_atendimento y
                                    WHERE x.nr_atendimento=u.nr_atendimento AND
       (x.ie_passagem_setor='N' OR x.ie_passagem_setor IS NULL) AND
                                          x.cd_setor_atendimento=y.cd_setor_atendimento AND y.ie_ocup_hospitalar='S');
    ELSE
        SELECT MIN(nr_seq_interno)
        INTO nr_seq_atepacu_w
        FROM atend_paciente_unidade u
        WHERE   u.nr_atendimento=nr_atendimento_p AND
        u.dt_entrada_unidade=(SELECT MIN(x.dt_entrada_unidade) FROM atend_paciente_unidade x
                                    WHERE x.nr_atendimento=u.nr_atendimento AND
           (x.ie_passagem_setor='N' OR x.ie_passagem_setor IS NULL));

        IF NVL(nr_seq_atepacu_w,0)=0 THEN
            SELECT MAX(nr_seq_interno)
            INTO nr_seq_atepacu_w
            FROM atend_paciente_unidade u
            WHERE   u.nr_atendimento=nr_atendimento_p AND
            u.dt_entrada_unidade=(SELECT MIN(x.dt_entrada_unidade) FROM atend_paciente_unidade x
                                  WHERE x.nr_atendimento=u.nr_atendimento);
        END IF;
    END IF;

ELSIF (ie_opcao_p = 2) THEN

    IF (ie_tipo_atendimento_w = 1) THEN
        SELECT MAX(nr_seq_interno)
        INTO nr_seq_atepacu_w
        FROM atend_paciente_unidade u,Setor_atendimento s
        WHERE u.nr_atendimento=nr_atendimento_p AND
              u.cd_setor_atendimento=s.cd_setor_atendimento AND s.ie_ocup_hospitalar='S' AND
              u.dt_entrada_unidade=(SELECT MAX(x.dt_entrada_unidade) FROM atend_paciente_unidade x,Setor_atendimento y
                                    WHERE x.nr_atendimento=u.nr_atendimento AND
       (x.ie_passagem_setor='N' OR x.ie_passagem_setor IS NULL) AND
                                          x.cd_setor_atendimento=y.cd_setor_atendimento AND y.ie_ocup_hospitalar='S');
    ELSE
        SELECT MAX(nr_seq_interno)
        INTO nr_seq_atepacu_w
        FROM atend_paciente_unidade u
        WHERE   u.nr_atendimento=nr_atendimento_p AND
        u.dt_entrada_unidade=(SELECT MAX(x.dt_entrada_unidade) FROM atend_paciente_unidade x
                                    WHERE x.nr_atendimento=u.nr_atendimento AND
        (x.ie_passagem_setor='N' OR x.ie_passagem_setor IS NULL));

        IF NVL(nr_seq_atepacu_w,0)=0 THEN
            SELECT MAX(nr_seq_interno)
            INTO nr_seq_atepacu_w
            FROM atend_paciente_unidade u
            WHERE   u.nr_atendimento=nr_atendimento_p AND
            u.dt_entrada_unidade=(SELECT MAX(x.dt_entrada_unidade) FROM atend_paciente_unidade x
                                  WHERE x.nr_atendimento=u.nr_atendimento);
        END IF;
    END IF;

END IF;


RETURN nr_seq_atepacu_w;

END HSL_ATEPACU;
/