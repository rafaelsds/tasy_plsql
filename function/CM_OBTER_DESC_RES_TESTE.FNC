create or replace
function	cm_obter_desc_res_teste(
		nr_seq_teste_p		number,
		nr_seq_resultado_p	number) return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	ds_resultado
into	ds_retorno_w
from	cm_ciclo_tipo_teste_res
where	nr_seq_tipo_teste = nr_seq_teste_p
and	nr_sequencia = nr_seq_resultado_p;

return ds_retorno_w;

end cm_obter_desc_res_teste;
/