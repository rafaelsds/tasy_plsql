create or replace
function obter_desc_evento_sol	(nr_seq_evento_p	number)
					return varchar2 is

ds_retorno_w	varchar2(240);
ie_evento_w	number(3,0);
ds_infusao_w	varchar2(22);
ds_evento_w	varchar2(214);

begin
if	(nr_seq_evento_p is not null) then
	select	max(ie_alteracao) ie_evento,
		max(substr(obter_valor_dominio(1883,ie_forma_infusao),1,22)) ds_forma_infusao,
		max(substr(obter_valor_dominio(1662,ie_alteracao),1,214)) ds_evento
	into	ie_evento_w,
		ds_infusao_w,
		ds_evento_w
	from	prescr_solucao_evento
	where	nr_sequencia = nr_seq_evento_p;

	if	(ie_evento_w = 1) and
		(ds_infusao_w is not null) then
		ds_retorno_w := ds_evento_w || ' (' || ds_infusao_w || ') ';
	else
		ds_retorno_w := ds_evento_w;
	end if;
end if;

return ds_retorno_w;

end obter_desc_evento_sol;
/