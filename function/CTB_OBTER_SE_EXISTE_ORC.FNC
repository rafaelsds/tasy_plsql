create or replace
function ctb_obter_se_existe_orc(cd_estabelecimento_p	number,
				 nr_seq_mes_ref_p	number,
				 cd_centro_custo_p	number,
				 cd_conta_contabil_p	varchar2)
				return varchar2 is

ie_retorno_w	varchar2(1)	:= 'S';

begin

select	nvl(max('S'),'N')
into	ie_retorno_w
from	dual
where	exists(	select	1
		from	ctb_orcamento
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	nr_seq_mes_ref		= nr_seq_mes_ref_p
		and	cd_centro_custo		= cd_centro_custo_p
		and	cd_conta_contabil	= cd_conta_contabil_p);

return	ie_retorno_w;

end ctb_obter_se_existe_orc;
/