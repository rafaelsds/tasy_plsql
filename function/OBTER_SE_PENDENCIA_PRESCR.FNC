create or replace
function Obter_se_pendencia_prescr(
					nr_sequencia_p		number,
					ie_funcao_p			varchar2) return varchar2 is
					
ie_retorno_w	varchar2(1) := 'N';					

begin

if (ie_funcao_p = 'PEP') then
select	nvl(max('S'),'N')
into	ie_retorno_w
from	pep_item_pendente
where	nr_sequencia = nr_sequencia_p
and		(((nr_prescricao is null) or 
		  (nr_prescricao is not null)) and
		 ((nr_seq_material_rep is null) and
		  (nr_seq_proced_rep is null) and
		  (nr_seq_dieta_rep is null) and
		  (nr_seq_nut_pac is null) and
		  (nr_seq_nut_prot is null) and
		  (nr_seq_recomendacao is null) and
		  (nr_seq_solucao is null) and
		  (nr_seq_gasoterapia is null) and
		  (nr_seq_horario is null) and
		  (nr_seq_item_adep is null) and
		  (nr_etapa_adep is null) and
		  (ie_tipo_item_adep is null)));
end if;

return ie_retorno_w;		  
		  
end Obter_se_pendencia_prescr;
/