create or replace
function consiste_titulo_pagar_baixa(	nr_titulo_p	number,
				nr_seq_baixa_p  	number)
 		    	return number is
qt_registros_w	number(10,0);
begin
if	(nr_titulo_p is not null) and
	(nr_seq_baixa_p is not null) then
	begin
	select	count(*)         
	into	qt_registros_w
	from    	titulo_pagar_baixa a
	where   	a.nr_titulo = nr_titulo_p
	and     	a.nr_seq_baixa_origem = nr_seq_baixa_p;
	end;
end if;
return	qt_registros_w;
end consiste_titulo_pagar_baixa;
/