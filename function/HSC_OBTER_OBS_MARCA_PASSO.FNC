create or replace
function HSC_obter_obs_Marca_passo (cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is
/* Trocar pala sequencia do acess�rio Marca passo no paciente (Tabela ACESSORIO_PACIENTE)
 * Conforme verificado na base curso, o �nico e com c�digo 1 � o Marca passo
 */
NR_SEQ_ACESS_MARCA_PASSO_W	number(10)	:= 1;

ds_retono_w		varchar2(255);
nr_seq_marca_passo_w	number(10);
		
begin

Select	MAX(nr_sequencia)
into	nr_seq_marca_passo_w
from 	paciente_acessorio
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and 	nr_seq_acessorio	= NR_SEQ_ACESS_MARCA_PASSO_W
and	dt_inativacao	is null
and 	((dt_fim is null) or (sysdate < dt_fim))
and 	((dt_inicio is null) or (sysdate > dt_inicio))
and 	nvl(ie_situacao,'A') = 'A';

if	(nr_seq_marca_passo_w is not null) then
	Select	ds_observacao 
	into	ds_retono_w
	from 	paciente_acessorio
	where	nr_Sequencia	= nr_seq_marca_passo_w;
end if;
return	ds_retono_w;

end HSC_obter_obs_Marca_passo;
/