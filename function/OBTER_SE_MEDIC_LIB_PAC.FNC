create or replace
function Obter_se_medic_lib_pac(	cd_pessoa_fisica_p		varchar2,
									cd_material_p			number,
									nr_prescricao_p			number)
					return varchar2 is

qt_lib_w				number(10);
qt_dias_tratamento_w	number(15,4);
qt_prescricao_w			number(15,4);
nm_usuario_w			varchar2(50);
ie_usa_liberacao_w		varchar2(50);
cd_estabelecimento_w	number(15,0);
dt_inicio_validade_w	date;

begin

select	max(nm_usuario),
		max(cd_estabelecimento)
into	nm_usuario_w,
		cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

obter_param_usuario(47, 1, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_usa_liberacao_w);

select	count(1)
into	qt_lib_w
from	lib_material_paciente
where	((ie_usa_liberacao_w = 'N') or 
		 (dt_liberacao is not null))
and		cd_material = cd_material_p
and		cd_pessoa_fisica	= cd_pessoa_fisica_p
and		dt_suspenso	is null;

if	(qt_lib_w > 0) then

	if	(nvl(nr_prescricao_p,0) > 0) then
		Select	max(qt_dias_tratamento),
				max(nvl(dt_inicio_validade,dt_atualizacao))
		into	qt_dias_tratamento_w,
				dt_inicio_validade_w
		from	lib_material_paciente
		where	((ie_usa_liberacao_w = 'N') or 
				 (dt_liberacao is not null))
		and		dt_suspenso	is null
		and		cd_material	= cd_material_p
		and		cd_pessoa_fisica = cd_pessoa_fisica_p;

		select	nvl(max(obter_dias_entre_datas(dt_inicio_validade_w, a.dt_prescricao)), -1)
		into	qt_prescricao_w
		from	prescr_material b,
				prescr_medica a
		where	b.cd_material = cd_material_p
		and		a.nr_prescricao = b.nr_prescricao
		and		trunc(a.dt_prescricao)	>= trunc(dt_inicio_validade_w)
		and		a.nr_prescricao		= nr_prescricao_p;
	
		if	(qt_prescricao_w = -1) then
			select	nvl(max(obter_dias_entre_datas(dt_inicio_validade_w, a.dt_prescricao)), -1)
			into	qt_prescricao_w
			from	prescr_medica a
			where	trunc(a.dt_prescricao)	>= trunc(dt_inicio_validade_w)
			and		a.nr_prescricao		= nr_prescricao_p;
		end if;
	
		if	(qt_prescricao_w >= qt_dias_tratamento_w) then
			return 'N';
		else
			return 'S';
		end if;
	else
		return 'S';
	end if;
else
	return 'N';
end if;

end Obter_se_medic_lib_pac;
/
