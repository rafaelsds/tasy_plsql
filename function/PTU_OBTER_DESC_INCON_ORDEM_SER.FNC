create or replace
function ptu_obter_desc_incon_ordem_ser
			(	nr_seq_resp_ordem_serv_p	Number,
				nr_seq_item_ord_serv_p		Number)
				return Varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter a inconsist�ncia gerada pra a transa��o de ordem de servi�o.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
ds_retorno_w	Varchar2(255);

begin

select	substr(y.cd_inconsistencia||' - '||y.ds_inconsistencia,1,255)
into	ds_retorno_w
from	ptu_intercambio_consist x,
	ptu_inconsistencia y
where	y.nr_sequencia  	= x.nr_seq_inconsistencia
and	x.nr_seq_ordem_serv	= nr_seq_resp_ordem_serv_p
and	(x.nr_seq_procedimento	= nr_seq_item_ord_serv_p
or	x.nr_seq_material  	= nr_seq_item_ord_serv_p)
and	cd_transacao    	= '00807';

return	ds_retorno_w;

end ptu_obter_desc_incon_ordem_ser;
/