create or replace
function obter_agendas_lib_user_ageserv(
		ds_agenda_lista_p		varchar2,
		nm_usuario_p		varchar2,
		cd_estabelecimento_p	number)
 	return varchar2 is

ie_estab_user_w	varchar2(1);
ie_perfil_usuario_w	varchar2(1);
ie_setor_user_w	varchar2(1);
ds_sql_w		varchar2(4000);
ds_parametros_w	varchar2(2000);
ds_retorno_w	varchar2(2000);

begin

ds_sql_w		:= '';
ds_parametros_w	:= '';

ds_sql_w	:= ds_sql_w || ' select	a.cd_agenda ';
ds_sql_w	:= ds_sql_w || ' from	agenda a ';
ds_sql_w	:= ds_sql_w || ' where	a.ie_situacao = '|| chr(39) || 'A' || chr(39);

/* Agenda de Servicos - Parametro [22] - Exibir somente as agendas do estabelecimento do usuario */
obter_param_usuario(866, 22, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_estab_user_w);

if	(ie_estab_user_w = 'S') then
	begin
	ds_sql_w		:= ds_sql_w || ' and a.cd_estabelecimento = :cd_estabelecimento_p ';
	ds_parametros_w	:= ds_parametros_w || 'cd_estabelecimento_p=' || cd_estabelecimento_p;
	end;
else
	begin
	ds_sql_w		:= ds_sql_w || ' and a.cd_estabelecimento in (select x.cd_estabelecimento from usuario_estabelecimento x where x.nm_usuario_param = :nm_usuario_p) '; 
	ds_parametros_w	:= ds_parametros_w || 'nm_usuario_p=' || nm_usuario_p;
	end;
end if;

/* Agenda de Servicos - Parametro [87] - Exibir somente as agendas do perfil do usuario */
obter_param_usuario(866, 87, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_perfil_usuario_w);

if	(ie_perfil_usuario_w = 'S') then
	begin
	ds_sql_w		:= ds_sql_w || ' and a.cd_perfil_exclusivo in (select x.cd_perfil from usuario_perfil x where x.nm_usuario = :nm_usuario_p) ';
	ds_parametros_w	:= ds_parametros_w || ';nm_usuario_p=' || nm_usuario_p;
	end;
end if;

/* Agenda de Servicos - Parametro [46] - Forma de exibir as agendas */
obter_param_usuario(866, 46, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_setor_user_w);

if	(ie_setor_user_w = 'S') then
	begin
	ds_sql_w		:= ds_sql_w || ' and a.cd_setor_exclusivo in (select x.cd_perfil from usuario_perfil x where x.nm_usuario = :nm_usuario_p) ';
	ds_parametros_w	:= ds_parametros_w || ';nm_usuario_p=' || nm_usuario_p;
	end;
elsif	(ie_setor_user_w = 'N') then
	ds_sql_w		:= ds_sql_w || ' and a.cd_setor_exclusivo is null ';
end if;

ds_sql_w		:= ds_sql_w || ' and substr(obter_se_contido(a.cd_agenda, ' || chr(39) || ds_agenda_lista_p || chr(39) || '), 1,1) = ' || chr(39) || 'S' || chr(39);

ds_retorno_w	:= obter_select_concatenado_bv(ds_sql_w, ds_parametros_w, ',');

return	ds_retorno_w;
end obter_agendas_lib_user_ageserv;
/
