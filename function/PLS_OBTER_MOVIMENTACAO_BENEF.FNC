create or replace
function pls_obter_movimentacao_benef
		(	nr_seq_segurado_p	number)
 		    	return varchar2 is
			
ie_tipo_retorno_w	varchar2(1);
dt_contratacao_w	date;
dt_rescisao_w		date;

begin

select	dt_contratacao,
	dt_rescisao
into	dt_contratacao_w,
	dt_rescisao_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

if	(trunc(dt_contratacao_w,'Month') = trunc(sysdate,'Month')) then
	ie_tipo_retorno_w	:= 'I';
elsif	(dt_rescisao_w is null) then
	ie_tipo_retorno_w	:= 'A';
elsif	(dt_rescisao_w is not null) then
	ie_tipo_retorno_w	:= 'E';
end if;	

return	ie_tipo_retorno_w;

end pls_obter_movimentacao_benef;
/
