create or replace
function Obter_Se_Unidade_Temp
		(cd_setor_atendimento_p		number,
		dt_referencia_p			date,
		cd_unidade_basica_p		varchar2,
		cd_unidade_compl_p		varchar2)
		return varchar2 is

ie_temporario_w		varchar2(01) := 'N';
ie_temp_hist_w		varchar2(01);
nr_seq_interno_w	number(10,0);

cursor	c01 is
	select	ie_temporario
	from	unidade_atend_hist
	where	nr_seq_unidade		= nr_seq_interno_w
	and	dt_historico		between trunc(dt_referencia_p, 'dd') and trunc(dt_referencia_p, 'dd') + 86399/86400 
	order by dt_historico;
	

begin

select	nvl(max(nr_seq_interno), 0),
	nvl(max(ie_temporario), 'N') 
into	nr_seq_interno_w,
	ie_temporario_w
from	unidade_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_p
and	cd_unidade_basica	= cd_unidade_basica_p
and	cd_unidade_compl	= cd_unidade_compl_p;

open	c01;
loop
fetch	c01 into
	ie_temp_hist_w;
exit 	when c01%notfound;
	begin

	ie_temp_hist_w	:= ie_temp_hist_w;

	end;
end loop;
close c01;

return	nvl(ie_temp_hist_w, ie_temporario_w);

end Obter_Se_Unidade_Temp;
/