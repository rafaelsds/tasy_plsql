create or replace
function mprev_obter_dados_grupo_atend(nr_seq_grupo_atend_p	number,
					ie_opcao_p		varchar2)
										return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar diferentes informa��es com rela��o ao grupo de atendimento da medicina preventiva.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

ie_opcao_p :
'NM' = Nome do grupo de atendimento
'QT' = Retorna a quantidade de turmas do grupo de atendimento
'QP' = Retorna a quantidade de participantes do grupo
'DI' = Data de inicio �ltima turma
'DT' = Data de t�rmino �ltima turma
'ST' = Status do grupo. (Ativo, Inativo)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */									
ds_retorno_w	varchar2(255);
begin

if	(upper(ie_opcao_p) = 'NM')then
	select	x.nm_grupo
	into	ds_retorno_w
	from	mprev_grupo_coletivo x
	where	x.nr_sequencia = nr_seq_grupo_atend_p;

elsif	(upper(ie_opcao_p) = 'QT')then
	select	count(nr_sequencia)
	into	ds_retorno_w
	from	mprev_grupo_col_turma x
	where	x.nr_seq_grupo_coletivo = nr_seq_grupo_atend_p;

elsif	(upper(ie_opcao_p) = 'QP')then
	select	sum(mprev_obter_dados_equipe(x.nr_seq_equipe, sysdate, 'QT')) 
	into	ds_retorno_w
	from	mprev_grupo_col_equipe x
	where	x.nr_seq_grupo_coletivo = nr_seq_grupo_atend_p;
	
elsif	(upper(ie_opcao_p) = 'DI')then
	select	dt_inicio 
	into	ds_retorno_w
	from	mprev_grupo_col_turma 
	where	nr_sequencia = (	select	max(nr_sequencia) 
				from	mprev_grupo_col_turma 
				where	nr_seq_grupo_coletivo = nr_seq_grupo_atend_p);

elsif	(upper(ie_opcao_p) = 'DT')then	
	select	dt_termino 
	into	ds_retorno_w
	from	mprev_grupo_col_turma 
	where	nr_sequencia = (	select	max(nr_sequencia) 
				from	mprev_grupo_col_turma 
				where	nr_seq_grupo_coletivo = nr_seq_grupo_atend_p);	
				
elsif	(upper(ie_opcao_p) = 'ST') then
	select	max(ie_situacao)
	into	ds_retorno_w
	from	mprev_grupo_coletivo 
	where	nr_sequencia = nr_seq_grupo_atend_p;				
end if;

return	ds_retorno_w;

end mprev_obter_dados_grupo_atend;
/
