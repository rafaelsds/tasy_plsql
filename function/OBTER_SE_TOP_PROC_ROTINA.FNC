create or replace
function obter_se_top_proc_rotina(
				nr_seq_proc_rotina_p	number) return varchar2 is
ie_topografia_w	varchar2(1) := 'N';
begin

select	nvl(max(ie_topografia),'N')
into		ie_topografia_w
from		procedimento_rotina
where	nr_sequencia = nr_seq_proc_rotina_p;

return ie_topografia_w;

end obter_se_top_proc_rotina;
/
