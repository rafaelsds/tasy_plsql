create or replace
function	obter_diluicao(	nr_seq_producao_p	number,
							nr_sequencia_p 		number) 				
							return number is

qt_po_w	number(20, 2) := null;

nr_seq_solubilidade_w number(20);

qt_volume_w number(20);

qt_fator_diluicao_w number(20);

qt_vol_agua_w number(20);

begin

	SELECT	p.nr_seq_solubilidade,
			b.qt_dose
	into 	nr_seq_solubilidade_w,
			qt_volume_w
	FROM	prescr_medica a,
			prescr_material b,
			nut_prod_lac_item d,
			prescr_mat_hor e,
			nut_prod_principal p
	WHERE b.nr_prescricao = d.nr_prescricao 
	AND	b.nr_sequencia = d.nr_seq_material
	AND	d.nr_seq_mat_hor = e.nr_sequencia
	AND	d.nr_seq_prod_princ = nr_seq_producao_p
	AND d.nr_sequencia = nr_sequencia_p
	AND d.nr_seq_prod_princ = p.nr_sequencia 
	AND	a.nr_prescricao = b.nr_prescricao;

	if ((nr_seq_solubilidade_w is not null) and ( qt_volume_w is not null)) then

		SELECT	MAX(QT_FATOR_DILUICAO),
				MAX(QT_VOLUME_TOTAL)
		into 	qt_fator_diluicao_w,
				qt_vol_agua_w
		FROM  	NUTRICAO_LEITE_DERIV_SOLUB
		WHERE 	nr_sequencia = nr_seq_solubilidade_w;



		qt_po_w := (qt_fator_diluicao_w * qt_volume_w)/ qt_vol_agua_w;


end if;

return	qt_po_w;

end	obter_diluicao;
/