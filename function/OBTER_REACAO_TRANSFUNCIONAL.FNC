create or replace
function obter_reacao_transfuncional(	cd_pessoa_fisica_p	varchar2,
					ie_opcao_p		varchar2 )
					return varchar2 is

/*
ie_opcao_p
DPF - Descri��o localizando por pessoa f�sica
*/

ds_tipo_reacao_w	varchar2(100);
nr_seq_evento_w		number(10);

begin

if	(ie_opcao_p = 'DPF') then
	begin
	select	substr(obter_descricao_padrao('SAN_TIPO_REACAO','DS_TIPO_REACAO',nr_seq_reacao),1,100)
	into	ds_tipo_reacao_w
	from	prescr_solic_bco_sangue
	where	nr_sequencia	=	(	select	max(nr_sequencia)
						from	prescr_solic_bco_sangue a,
							prescr_medica b
						where	a.nr_prescricao		=	b.nr_prescricao
						and	b.cd_pessoa_fisica	=	cd_pessoa_fisica_p
						and	nr_seq_reacao is not null );
	exception
	when others then
		ds_tipo_reacao_w	:= null;
	end;
	
	if	(ds_tipo_reacao_w	is null) then
	
		OBTER_PARAM_USUARIO(450,339,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,nr_seq_evento_w);
		
		select  substr(max(ds_evento),1,100)
		into 	ds_tipo_reacao_w
		from  	QUA_EVENTO_PACIENTE a,
				atendimento_paciente b				 
		where 	a.nr_atendimento = b.nr_atendimento
		and		a.dt_liberacao is not null
		and	  	a.nr_seq_evento = nr_seq_evento_w
		and	  	b.cd_pessoa_fisica = cd_pessoa_fisica_p;
	end if;	
	
end if;

return	ds_tipo_reacao_w;

end obter_reacao_transfuncional;
/
