create or replace 
FUNCTION obter_sinal_vital_RN(
			nr_atendimento_p	number,
			ie_sinal_p		varchar2)
			return number is

vl_retorno_w		Number(15,4);
nr_atend_alta_w		number(10);

BEGIN

if	(nr_atendimento_p > 0) and
	(ie_sinal_p is not null) then
	begin
		
	if	(ie_sinal_p = 'Peso') then
		select	max(qt_peso)
		into	vl_retorno_w
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_peso is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A');
	elsif	(ie_sinal_p = 'FC') then
		select	max(qt_freq_cardiaca)
		into	vl_retorno_w
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_freq_cardiaca is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A');
	elsif	(ie_sinal_p = 'FR') then
		select	max(qt_freq_resp)
		into	vl_retorno_w
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_freq_resp is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A');
	elsif	(ie_sinal_p = 'PAMIN') then
		select	max(qt_pa_diastolica)
		into	vl_retorno_w
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_pa_diastolica is not null
					and	qt_pa_sistolica is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A');
	elsif	(ie_sinal_p = 'PAMAX') then
		select	max(qt_pa_sistolica)
		into	vl_retorno_w
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_pa_sistolica is not null
					and	qt_pa_diastolica is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A');
	elsif	(ie_sinal_p = 'SC') then
		select	max(qt_superf_corporia)
		into	vl_retorno_w
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_superf_corporia is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A');
	elsif	(ie_sinal_p = 'IMC') then
		select	max(qt_imc)
		into	vl_retorno_w
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_imc is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A');
	elsif	(ie_sinal_p = obter_desc_expressao(283402)/*'Altura'*/) then
		select	max(qt_altura_cm)
		into	vl_retorno_w
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_altura_cm is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A');
	elsif	(ie_sinal_p = 'Temp') then
		select	max(qt_temp)
		into	vl_retorno_w
		from	atendimento_sinal_vital
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_temp is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A'
					and	nvl(IE_RN,'N')	= 'N');
	elsif	(ie_sinal_p = 'ESCDOR') then
		select	max(qt_escala_dor)
		into	vl_retorno_w
		from	atendimento_sinal_vital
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_escala_dor is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A'
					and	nvl(IE_RN,'N')	= 'N');
	elsif	(ie_sinal_p = 'PAM') then
		select	max(qt_pam)
		into	vl_retorno_w
		from	atendimento_sinal_vital
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_pam is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A'
					and	nvl(IE_RN,'N')	= 'N');					
	elsif	(ie_sinal_p = 'GC') then
		select	max(qt_glicemia_capilar)
		into	vl_retorno_w
		from	atendimento_sinal_vital
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_glicemia_capilar is not null
					and	nr_atendimento	= nr_atendimento_p
					and	ie_situacao = 'A'
					and	nvl(IE_RN,'N')	= 'N');										
	end if;
	end;

	if	(vl_retorno_w is null) then

		select 	max(nr_atend_alta)
		into	nr_atend_alta_w
		from 	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;
		
		if	(nr_atend_alta_w is not null) then
			begin
				
			if	(ie_sinal_p = 'Peso') then
				select	max(qt_peso)
				into	vl_retorno_w
				from	atendimento_sinal_vital	
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_peso is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'FC') then
				select	max(qt_freq_cardiaca)
				into	vl_retorno_w
				from	atendimento_sinal_vital	
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_freq_cardiaca is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'FR') then
				select	max(qt_freq_resp)
				into	vl_retorno_w
				from	atendimento_sinal_vital	
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_freq_resp is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'PAMIN') then
				select	max(qt_pa_diastolica)
				into	vl_retorno_w
				from	atendimento_sinal_vital	
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_pa_diastolica is not null
							and	qt_pa_sistolica is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'PAMAX') then
				select	max(qt_pa_sistolica)
				into	vl_retorno_w
				from	atendimento_sinal_vital	
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_pa_sistolica is not null
							and	qt_pa_diastolica is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'SC') then
				select	max(qt_superf_corporia)
				into	vl_retorno_w
				from	atendimento_sinal_vital	
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_superf_corporia is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'IMC') then
				select	max(qt_imc)
				into	vl_retorno_w
				from	atendimento_sinal_vital	
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_imc is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = obter_desc_expressao(283402)/*'Altura'*/) then
				select	max(qt_altura_cm)
				into	vl_retorno_w
				from	atendimento_sinal_vital	
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_altura_cm is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'Temp') then
				select	max(qt_temp)
				into	vl_retorno_w
				from	atendimento_sinal_vital
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_temp is not null
							and	nr_atendimento	= nr_atend_alta_w
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'ESCDOR') then
				select	max(qt_escala_dor)
				into	vl_retorno_w
				from	atendimento_sinal_vital
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_escala_dor is not null
							and	nr_atendimento	= nr_atendimento_p
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');
			elsif	(ie_sinal_p = 'PAM') then
				select	max(qt_pam)
				into	vl_retorno_w
				from	atendimento_sinal_vital
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_pam is not null
							and	nr_atendimento	= nr_atendimento_p
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');							
			elsif	(ie_sinal_p = 'GC') then
				select	max(qt_glicemia_capilar)
				into	vl_retorno_w
				from	atendimento_sinal_vital
				where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
							from	atendimento_sinal_vital
							where	qt_glicemia_capilar is not null
							and	nr_atendimento	= nr_atendimento_p
							and	ie_situacao = 'A'
							and	nvl(IE_RN,'N')	= 'N');														
			end if;
			end;
		
		end if;
	end if;

end if;

return vl_retorno_w;

END obter_sinal_vital_RN;
/
