create or replace
function obter_se_atend_prescr_vig(	nr_atendimento_p		Number)
 		    	return Varchar2 is

ie_prescr_vig_w		char(1) := 'N';
			
begin

if	(nr_atendimento_p is not null) then
	
	select	nvl(max('S'),'N')
	into	ie_prescr_vig_w
	from	prescr_medica
	where	rownum = 1
	and		nr_atendimento = nr_atendimento_p
	and		sysdate between dt_inicio_prescr and dt_validade_prescr;
	
end if;

return	ie_prescr_vig_w;

end obter_se_atend_prescr_vig;
/