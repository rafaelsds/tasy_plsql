create or replace
function pls_obter_se_mat_ativo(
				nr_seq_material_p	Varchar2,
				dt_material_p		Date)
 		    	return Varchar2 is

/*
ds_retorno_w:

S - Material ativo
N - Material inativo
D- Material fora da data da regra de vig�ncia
*/
			
ds_retorno_w		Varchar2(1)	:= 'S';
qt_material_ativo_w	number(10);
begin

select	count(1)
into	qt_material_ativo_w
from	pls_material
where	nr_sequencia = nr_seq_material_p;

if	(qt_material_ativo_w > 0)	then
	begin
	select	'S'
	into	ds_retorno_w
	from	pls_material
	where	ie_situacao = 'A'
	and	nr_sequencia = nr_seq_material_p
	and	dt_inclusao is not null;
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;

	if	(ds_retorno_w = 'S') then
		begin
		select	'S'
		into	ds_retorno_w
		from	pls_material
		where	nr_sequencia = nr_seq_material_p
		and	(dt_material_p is null or
			trunc(dt_material_p) between trunc(dt_inclusao) and trunc(nvl(dt_exclusao,dt_material_p+1))
			);
		exception
		when others then
			ds_retorno_w	:= 'N';
		end;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_mat_ativo;
/
