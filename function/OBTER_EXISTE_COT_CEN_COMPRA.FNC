create or replace
function obter_existe_cot_cen_compra(nr_Seq_central_compra_p	number)
 		    	return varchar2 is

qt_existe_w	number(5);
ds_retorno_w 	varchar2(1);

begin
ds_retorno_w := 'N';

select	count(*)
into	qt_existe_w
from	sup_central_compra_item b,
	cot_compra_solic_agrup a
where	a.nr_seq_central_compra_item = b.nr_sequencia
and	b.nr_Seq_sup_central_compra = nr_Seq_central_compra_p;

if	(qt_existe_w = 0) then
	select	count(*)
	into	qt_existe_w
	from	sup_central_compra_item b,
		cot_compra_item a
	where	a.nr_seq_central_compra_item = b.nr_sequencia
	and	b.nr_Seq_sup_central_compra = nr_Seq_central_compra_p
	and	not exists (
			select	1
			from	cot_compra_solic_agrup x
			where	a.nr_cot_compra = x.nr_cot_compra
			and	a.nr_item_cot_compra = x.nr_item_cot_compra);
end if;

if	(qt_existe_w > 0) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;
end obter_existe_cot_cen_compra;
/