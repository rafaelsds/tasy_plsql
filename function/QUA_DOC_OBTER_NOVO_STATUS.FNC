create or replace
function qua_doc_obter_novo_status(nr_seq_doc_p		number)
 		    	return varchar2 is
				
ds_status_w			varchar2(1); 
qt_reg_doc_rev_w	number(10);
dt_validacao_w		date;
dt_aprovacao_w		date;
				
begin

select	ie_status,
		dt_validacao,
		dt_aprovacao
into	ds_status_w,
		dt_validacao_w,
		dt_aprovacao_w
from	qua_documento
where	nr_sequencia = nr_seq_doc_p;

select	count(*) qt_doc_revisao
into	qt_reg_doc_rev_w
from	qua_doc_revisao
where	nr_seq_doc = nr_seq_doc_p
and	dt_aprovacao is null;

if (qt_reg_doc_rev_w > 0) then
	ds_status_w := 'R';
elsif (dt_aprovacao_w is not null) then
	ds_status_w := 'D';
elsif (dt_validacao_w is not null) then
	ds_status_w := 'V';
elsif (dt_validacao_w is not null) then
	ds_status_w := 'E';
else
	ds_status_w := 'P';
end if;

return	ds_status_w	;

end qua_doc_obter_novo_status;
/