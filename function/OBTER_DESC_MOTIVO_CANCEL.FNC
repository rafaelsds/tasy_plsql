create or replace
function obter_desc_motivo_cancel( nr_seq_motivo_p number)
 		    	return varchar2 is


ds_motivo_cancelamento_w	varchar2(80);
begin

if (nr_seq_motivo_p is not null) then

	select	max(ds_motivo_cancelamento)
	into	ds_motivo_cancelamento_w
	from	motivo_cancelamento_vacina
	where 	nr_sequencia 	= nr_seq_motivo_p;

end if;


return	ds_motivo_cancelamento_w;

end obter_desc_motivo_cancel;
/