create or replace
function consistir_idade_paciente_proc(	cd_pessoa_fisica_p	varchar2,
					cd_procedimento_p	number,
					ie_origem_proced_p	number)
					return varchar2 is

qt_idade_w		number(10);
qt_idade_min_w		number(4,0);
qt_idade_max_w		number(4,0);

/*
	S - Idade est� dentro do per�odo min / max permitida para o Procedimento
	N - Idade est� fora do per�odo min / max permitida para o Procedimento
*/

begin

if	(ie_origem_proced_p = 7) then

	begin
	select 	somente_numero(SUS_CONVERTE_IDADE_ANOS(qt_idade_minima,'D')),
		somente_numero(SUS_CONVERTE_IDADE_ANOS(qt_idade_maxima,'D'))
	into	qt_idade_min_w,
		qt_idade_max_w
	from 	sus_procedimento
	where 	cd_procedimento = cd_procedimento_p
	and 	ie_origem_proced = ie_origem_proced_p;
	exception
		when others then
			qt_idade_min_w	:= null;
			qt_idade_max_w	:= null;
	end;

else

	begin
	select	qt_idade_minima_sus,
		qt_idade_maxima_sus
	into	qt_idade_min_w,
		qt_idade_max_w
	from 	procedimento 
	where	cd_procedimento  = cd_procedimento_p
	and	ie_origem_proced = ie_origem_proced_p;

	exception
		when others then
			qt_idade_min_w	:= null;
			qt_idade_max_w	:= null;
	end;
	
end if;

if	((qt_idade_min_w is not null) or 
	(qt_idade_max_w is not null)) then

	begin
	select	obter_idade(dt_nascimento,sysdate,'A')
	into	qt_idade_w
	from	pessoa_fisica
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
	exception
	when others then
		qt_idade_w	:= null;
	end;

	if	(qt_idade_w is not null) then
	
		if	((qt_idade_w 	>  nvl(qt_idade_min_w,0)) or (qt_idade_w = qt_idade_min_w and qt_idade_w = 0)) and		
			(qt_idade_w	<  nvl(qt_idade_max_w,999)) then
			return	'S';
		else	
			return	'N';
		end	if;
	else
		return	'S';	
	end	if;
else
	return	'S';
end	if;

end	consistir_idade_paciente_proc;
/
