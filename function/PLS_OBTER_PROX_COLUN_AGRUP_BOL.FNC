/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter o n�mero da pr�xima coluna do registro de agrupamento
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_prox_colun_agrup_bol
		(	nr_seq_estrut_agrup_p	number)
 		    	return number is
			
nr_coluna_w		number(10);

begin

select	max(NR_COLUNA_AGRUP)
into	nr_coluna_w
from	PLS_ESTRUT_BANDA_BOL_INFO
where	NR_SEQ_ESTRUT_AGRUP	= nr_seq_estrut_agrup_p
and	NR_SEQ_COMPOSICAO is null;

if	(nr_coluna_w is null) then
	nr_coluna_w	:= 0;
end if;

return	nr_coluna_w+1;

end pls_obter_prox_colun_agrup_bol;
/
