create or replace
function OBTER_SEQ_INT_PRESCR_PROCMAT(	nr_prescricao_p		number,
						nr_sequencia_prescricao_p	number,
						ie_opcao_p		varchar2)
									return number is

/*
ie_opcao_p
'P'	- prescr_procedimento
'M'	- prescr_material
*/

nr_seq_interno_w	number(15);

begin

if	(ie_opcao_p	= 'P') then

	select	max(a.nr_seq_interno)
	into	nr_seq_interno_w
	from	prescr_procedimento a
	where	a.nr_prescricao	= nr_prescricao_p
	and	a.nr_sequencia	= nr_sequencia_prescricao_p;

elsif	(ie_opcao_p	= 'M') then

	select	max(a.nr_seq_interno)
	into	nr_seq_interno_w
	from	prescr_material a
	where	a.nr_prescricao	= nr_prescricao_p
	and	a.nr_sequencia	= nr_sequencia_prescricao_p;

end if;

return	nr_seq_interno_w;

end	OBTER_SEQ_INT_PRESCR_PROCMAT;
/