create or replace
function Obter_Destino_Conjunto
		(nr_seq_conj_cont_p	number)
		return	varchar2 is

ds_retorno_w		varchar2(150);
cd_setor_dest_w		number(05,0);
cd_pessoa_fisica_w	varchar2(10);
cd_cgc_w		varchar2(14);
cd_local_estoque_w	number(04,0);

begin

select	cd_local_estoque,
	cd_setor_atend_dest,
	cd_pessoa_fisica,
	cd_cgc
into	cd_local_estoque_w,
	cd_setor_dest_w,
	cd_pessoa_fisica_w,
	cd_cgc_w
from	cm_conjunto_cont
where	nr_sequencia	= nr_seq_conj_cont_p
and	nvl(ie_situacao,'A') = 'A';


if	(cd_pessoa_fisica_w is not null) then
	select	substr(obter_nome_pessoa_fisica(cd_pessoa_fisica_w, null),1,100)
	into	ds_retorno_w
	from	dual;
elsif	(cd_cgc_w is not null) then
	select	substr(obter_razao_social(cd_cgc_w),1,100)
	into	ds_retorno_w
	from	dual;
elsif	(cd_setor_dest_w is not null) then
	select	substr(obter_nome_setor(cd_setor_dest_w),1,100)
	into	ds_retorno_w
	from	dual;
elsif	(cd_local_estoque_w is not null) then
	select	substr(obter_desc_local_estoque(cd_local_estoque_w),1,100)
	into	ds_retorno_w
	from	dual;
end if;

return	ds_retorno_w;

end Obter_Destino_Conjunto;
/