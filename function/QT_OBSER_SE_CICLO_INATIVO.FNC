create or replace
function qt_obser_se_ciclo_inativo(nr_seq_paciente_p	number)
 		    	return varchar2 is
			
ie_inativo_w	varchar2(1);
ie_status_w	varchar2(1);
begin

select	max(ie_status)
into	ie_status_w
from	paciente_setor
where	nr_seq_paciente = nr_seq_paciente_p;

if	(ie_status_w = 'I') then
	ie_inativo_w	:= 'S';
else
	ie_inativo_w	:= 'N';
end if;

return	ie_inativo_w;

end qt_obser_se_ciclo_inativo;
/