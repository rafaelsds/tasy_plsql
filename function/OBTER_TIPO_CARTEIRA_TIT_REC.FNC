create or replace
function obter_tipo_carteira_tit_rec(nr_titulo_p	number)
					return varchar2 is

ie_tipo_carteira_w	varchar2(5)	:= null;

begin

if	(nr_titulo_p is not null) then

	select	ie_tipo_carteira
	into	ie_tipo_carteira_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_p;
end if;

return ie_tipo_carteira_w;

end obter_tipo_carteira_tit_rec;
/