create or replace
function get_description_score_flex_ii(nr_sequencia_p	Number)
 		    	return varchar2 is

ds_retorno_w	varchar2(2000) := null;	
qt_pontos_w     varchar2(50);
nr_seq_escala_w     varchar2(50);
			
begin

	select	nr_seq_escala,
			qt_pontos 
	into 	nr_seq_escala_w,
			qt_pontos_w
	from 	escala_eif_ii
	where 	nr_sequencia = nr_sequencia_p;	
	
if	(qt_pontos_w is not null) then
	begin
	
	select 	ds_resultado
	into	ds_retorno_w	 
	from   	eif_escala_ii_result
	where   qt_pontos_w between qt_pontos_min and qt_pontos_max
	and	nr_seq_escala = nr_seq_escala_w;
	
	exception
			when others then 
				ds_retorno_w	:= null;
	end;
end if;
	
return	ds_retorno_w;

end get_description_score_flex_ii;
/