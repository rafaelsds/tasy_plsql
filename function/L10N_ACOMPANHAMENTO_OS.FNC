	create or replace
	procedure l10n_acompanhamento_os(dt_atual_p	date) is

	nr_sequencia_w	number(10);
	ds_dano_breve_w	varchar2(255);
	ds_estagio_w	varchar2(255);
	dt_estagio_w	date;
	ds_gerencia_w	varchar2(255);
	nr_seq_grupo_des_w number(10);
	nr_seq_estagio_w	number(10);

	cursor c01 is
	select  a.nr_sequencia,
			a.ds_dano_breve,
			c.ds_estagio,
		   (select max(dt_atualizacao)  
			from man_ordem_serv_estagio b 
			where a.nr_sequencia = b.nr_seq_ordem 
			) dt_estagio,
			corp.obter_nome_gerencia(d.nr_seq_gerencia) ds_gerencia,
			nr_seq_grupo_des,
			a.nr_seq_estagio
	from corp.man_ordem_servico a
	join corp.man_estagio_processo c
	on 	 a.nr_seq_estagio = c.nr_sequencia
	join corp.grupo_desenvolvimento d
	on 	 a.nr_seq_grupo_des = d.nr_sequencia
	where nr_seq_localizacao=6854
	and ie_classificacao_cliente in ('E')
	union
	select a.nr_sequencia,
			a.ds_dano_breve,
			c.ds_estagio,
		   (select max(dt_atualizacao)  
			from man_ordem_serv_estagio b 
			where a.nr_sequencia = b.nr_seq_ordem 
			) dt_estagio,
			corp.obter_nome_gerencia(d.nr_seq_gerencia) ds_gerencia,
			nr_seq_grupo_des,
			a.nr_seq_estagio
	from corp.man_ordem_servico a
	join corp.man_estagio_processo c
	on 	 a.nr_seq_estagio = c.nr_sequencia
	join corp.grupo_desenvolvimento d
	on 	 a.nr_seq_grupo_des = d.nr_sequencia
	where nr_seq_tipo_ordem = 307
	and ie_classificacao_cliente in ('E');
	/*
	cursor c02 is
	select distinct a.nr_sequencia,a.ds_dano_breve, b.ds_estagio,
	c.*,
	(select decode(nvl(max(1),''),1,'yes','') from corp.proj_ordem_servico p where p.nr_seq_proj = 8824 and p.nr_seq_ordem = a.nr_sequencia) phase_1,
	d.ds_tipo
	from corp.man_ordem_servico a
	join corp.man_estagio_processo b
		 on a.nr_seq_estagio = b.nr_sequencia
	join corp.man_tipo_ordem_servico d
	on a.nr_seq_tipo_ordem = d.nr_sequencia
	left join (
		select b.ds_atividade sprint,a.ds_atividade workflow,a.nr_seq_sub_proj project, f.ds_atividade requirement,
		f.pr_etapa pr, f.qt_hora_prev effort, f.dt_inicio_prev, f.dt_fim_prev due_date, f.nr_sequencia cronograma,
		f.qt_hora_real,
		corp.obter_pr_prev_crono (f.dt_inicio_prev,f.dt_fim_prev) pr_previsto
		from corp.proj_cron_etapa a
		join corp.proj_cron_etapa b
		on a.nr_seq_superior = b.nr_sequencia
		join corp.proj_cronograma c
		on c.nr_seq_proj = a.nr_seq_sub_proj
		join corp.proj_cron_etapa d
		on c.nr_sequencia =d.nr_seq_cronograma
		and d.nr_seq_superior is null
		join corp.proj_cron_etapa e
		on e.nr_seq_superior = d.nr_sequencia
		join corp.proj_cron_etapa f
		on f.nr_seq_superior = e.nr_sequencia
		where a.nr_seq_superior in (
		  select nr_sequencia
		  from corp.proj_cron_etapa a
		  where ds_atividade like 'dev project%')) c
		  on a.nr_seq_proj_cron_etapa = c.cronograma
	where nr_seq_localizacao = 6854
	and (((ie_desenv='s') or (ie_tecnologia = 's') or (ie_design='s') or (ie_testes = 's')) or ((a.nr_seq_estagio in (2,9)) and (c.cronograma is not null)))
	and nr_seq_tipo_ordem not in (293)
	and	c.sprint is null 
	order by 4,2 */


	begin

	open c01;
	loop
	fetch c01 into	
		nr_sequencia_w,
		ds_dano_breve_w,
		ds_estagio_w,
		dt_estagio_w,
		ds_gerencia_w,
		nr_seq_grupo_des_w,
		nr_seq_estagio_w;
	exit when c01%notfound;
		begin
			insert into location_defect (nr_seq_ordem,
										 ds_dano_breve,
										 ds_estagio,
										 dt_estagio,
										 ds_gerencia,
										 nr_seq_grupo_desc,
										 dt_atualizacao,
										 nr_seq_estagio)
			values						(nr_sequencia_w,
										 ds_dano_breve_w,
										 ds_estagio_w,
										 dt_estagio_w,
										 ds_gerencia_w,
										 nr_seq_grupo_des_w,
										 dt_atual_p,
										 nr_seq_estagio_w);
		end;
	end loop;
	close c01;


	commit;

	end l10n_acompanhamento_os;
	/