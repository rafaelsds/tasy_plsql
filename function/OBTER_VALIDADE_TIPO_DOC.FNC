create or replace
function obter_validade_tipo_doc(nr_seq_tipo_doc_p	Number)
	return Number is 
	
qt_tempo_validade_w Number(10,0) := 0;

begin
if	(nr_seq_tipo_doc_p is not null) then
	begin
	select 	max(qt_tempo_validade) qt_tempo_validade
	into 	qt_tempo_validade_w
	from 	tipo_documento_fornec 
	where 	nr_sequencia = nr_seq_tipo_doc_p;
	end;
end if;
return qt_tempo_validade_w;
end obter_validade_tipo_doc;
/