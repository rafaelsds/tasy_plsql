create or replace
function hd_habilita_desfazer_dialise(nr_seq_dialise_p		number)
 		    	return varchar2 is
qt_registro_w	number(10);
begin
select	count(*)
into	qt_registro_w
from	hd_dialise_dialisador
where	nr_sequencia = hd_obter_dados_dialise(nr_seq_dialise_p,'DS')
and	dt_retirada is not null;

if	(qt_registro_w > 0) then
	return 'N';
end if;

select	count(*)
into	qt_registro_w
from	hd_controle
where	nr_seq_dialise = nr_seq_dialise_p;

if	(qt_registro_w > 0) then
	return 'N';
end if;

select	count(*)
into	qt_registro_w
from	hd_dialise_evento
where	nr_seq_dialise = nr_seq_dialise_p;

if	(qt_registro_w > 0) then
	return 'N';
end if;

select	count(*)
into	qt_registro_w
from	hd_dialise_concentrado
where	nr_seq_dialise = nr_seq_dialise_p;

if	(qt_registro_w > 0) then
	return 'N';
end if;

return	'S';

end hd_habilita_desfazer_dialise;
/