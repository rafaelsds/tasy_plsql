CREATE OR REPLACE
FUNCTION Obter_Per_consulta_preco(	cd_procedimento_p		number,
					ie_origem_proced_p		number,
					cd_convenio_p			number,
					ie_opcao_p			number,
					nm_usuario_p			varchar2)
 		    	return number is
pr_retorno_w		number(15,4);
pr_procedimento_w	number(15,4);
Cursor C01 is
	select	dividir(nvl(sum(pr_particular),0),count(*))
	from	w_consulta_preco_proc a
	where	a.nm_usuario		= nm_usuario_p
	and	pr_particular		<> 0
	and	a.cd_convenio		= cd_convenio_p;
BEGIN

if	(ie_opcao_p		= 1) then

	select	dividir(nvl(sum(pr_particular),0),count(*))
	into	pr_retorno_w
	from	w_consulta_preco_proc a
	where	a.nm_usuario		= nm_usuario_p
	and	a.cd_procedimento	= cd_procedimento_p
	and	a.ie_origem_proced	= ie_origem_proced_p
	and	pr_particular		<> 0
	and	a.cd_convenio		= cd_convenio_p;

elsif	(ie_opcao_p		= 2) then

	select	dividir(nvl(sum(pr_particular),0),count(*))
	into	pr_retorno_w
	from	w_consulta_preco_proc a
	where	a.nm_usuario		= nm_usuario_p
	and	pr_particular		<> 0
	and	a.cd_convenio		= cd_convenio_p;
end if;
return	pr_retorno_w;

END Obter_Per_consulta_preco;
/