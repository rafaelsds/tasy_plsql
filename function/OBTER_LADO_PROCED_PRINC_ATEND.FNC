create or replace
function obter_lado_proced_princ_atend(	nr_atendimento_p	number,
					ie_opcao_p		varchar2 )
					return varchar2 is

ie_tipo_atendimento_w		Number(15,0);
cd_procedimento_w		Number(15,0);
ie_origem_proced_w		Number(10,0);
ie_lado_w			varchar2(1);			

BEGIN

select	ie_tipo_atendimento
into	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento		= 	nr_atendimento_p;

obter_proc_princ_interno(nr_atendimento_p, ie_tipo_atendimento_w, cd_procedimento_w, ie_origem_proced_w, 0);

select	max(ie_lado)
into	ie_lado_w
from	prescr_procedimento a,
	prescr_medica b
where	a.nr_prescricao		=	b.nr_prescricao
and	a.cd_procedimento	=	cd_procedimento_w
and	a.ie_origem_proced	=	ie_origem_proced_w
and	b.nr_atendimento	=	nr_atendimento_p;

if	(ie_opcao_p = 'D') and
	(ie_lado_w is not null) then
	return obter_valor_dominio(1372, ie_lado_w);
else
	return ie_lado_w;
end if; 

end obter_lado_proced_princ_atend;
/