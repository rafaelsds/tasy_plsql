Create or replace
FUNCTION get_info_rule_days_dut_eup	(	cd_convenio_p			in 	regra_dias_autor_dut.cd_convenio%type,
										ie_tipo_atendimento_p	in	regra_dias_autor_dut.ie_tipo_atendimento%type,
										dt_vigencia_p			in	regra_dias_autor_dut.dt_inicio_vigencia%type,
										cd_procedimento_p		in	regra_dias_autor_dut.cd_procedimento%type,
										ie_origem_proced_p		in	regra_dias_autor_dut.ie_origem_proced%type,
										nr_seq_proc_interno_p	in	regra_dias_autor_dut.nr_seq_proc_interno%type
										)
return varchar2 is

ds_retorno_w			varchar2(10) := 'N';
ie_tipo_autorizacao_w	regra_dias_autor_dut.ie_tipo_autorizacao%type;
nm_usuario_resp_w		regra_dias_autor_dut.nm_usuario_resp%type;
qt_dias_autorizacao_w	regra_dias_autor_dut.qt_dias_autorizacao%type;
nr_sequencia_w			regra_dias_autor_dut.nr_sequencia%type;
ds_informacao_dut_w		regra_dias_autor_dut.ds_informacao_dut%type;

begin

	get_info_rule_days_dut(
		cd_convenio_p,
		ie_tipo_atendimento_p,
		dt_vigencia_p,
		cd_procedimento_p,
		ie_origem_proced_p,
		nr_seq_proc_interno_p,
		ie_tipo_autorizacao_w,
		nm_usuario_resp_w,
		qt_dias_autorizacao_w,
		nr_sequencia_w,
		ds_informacao_dut_w
		);

	if(nr_sequencia_w is not null) then
		ds_retorno_w := 'S';
	end if;


return ds_retorno_w;	

END get_info_rule_days_dut_eup;
/