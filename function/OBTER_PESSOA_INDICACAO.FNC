create or replace
function Obter_pessoa_indicacao(	cd_pessoa_fisica_p	varchar2,
					ie_opcao_p		varchar2) 
		             		return			varchar2 is


/* 
  ie_opcao_p
    C -> Codigo pessoa indica��o
    N -> Nome pessoa indica��o
*/

cd_pessoa_indicacao_w	varchar2(10);
ds_retorno_w			varchar2(254);

begin

select	i.cd_pessoa_fisica
into	cd_pessoa_indicacao_w
from	pessoa_fisica_indicacao i
where	i.cd_pessoa_indicada	= cd_pessoa_fisica_p
and	i.nr_sequencia		= (	select	max(j.nr_sequencia)
						from 	pessoa_fisica_indicacao j
						where 	j.cd_pessoa_indicada 	= i.cd_pessoa_indicada);

if	(ie_opcao_p =	'C') then
	ds_retorno_w 	:= cd_pessoa_indicacao_w;
elsif	(ie_opcao_p =	'N') then
	ds_retorno_w	:= substr(obter_nome_pf(cd_pessoa_indicacao_w),1,100);
end if;

return	ds_retorno_w;

end Obter_pessoa_indicacao;
/