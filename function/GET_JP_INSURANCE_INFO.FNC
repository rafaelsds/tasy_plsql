create or replace function get_jp_insurance_info (
    nr_sequencia_p    number,
    ie_informacao_p   number
) return varchar2 is
    
    /*
     1 -- Outpatient self-pay rate
     2 -- Insurance pattern code
     3 -- Insruance name in Japan
    */

    ds_return_w              varchar2(50) := '';
    ds_insurance_name_jp_w   nais_insurance.ds_insurance_name_japanese%type;
    cd_insurance_pattern_w   nais_insurance.cd_insurance_pattern%type;
    qt_outpatient_rate_w     nais_insurance.qt_outpatient_rate%type;
begin
    select
        ds_insurance_name_japanese,
        cd_insurance_pattern,
        qt_outpatient_rate
    into
        ds_insurance_name_jp_w,
        cd_insurance_pattern_w,
        qt_outpatient_rate_w
    from
        nais_insurance
    where
        nr_sequencia = nr_sequencia_p;

    if ( ie_informacao_p = 1 ) then
        ds_return_w := qt_outpatient_rate_w;
    elsif ( ie_informacao_p = 2 ) then
        ds_return_w := cd_insurance_pattern_w;
    elsif ( ie_informacao_p = 3 ) then
        ds_return_w := ds_insurance_name_jp_w;
    end if;

    return ds_return_w;
end get_jp_insurance_info;
/