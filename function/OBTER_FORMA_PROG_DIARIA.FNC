create or replace function obter_forma_prog_diaria(
    cd_funcao_p    in ultima_forma_prog_diaria.cd_funcao%type,
    nm_usuario_p   in ultima_forma_prog_diaria.nm_usuario%type
) return ultima_forma_prog_diaria.ie_forma_apresentacao%type deterministic is

ie_forma_apresentacao_w     ultima_forma_prog_diaria.ie_forma_apresentacao%type;

begin

    select nvl(max(ie_forma_apresentacao),'S')
    into ie_forma_apresentacao_w
    from ultima_forma_prog_diaria
    where cd_funcao = cd_funcao_p
    and nm_usuario = nm_usuario_p;


return	ie_forma_apresentacao_w;
end obter_forma_prog_diaria;
/