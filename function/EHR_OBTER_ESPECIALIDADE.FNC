create or replace 
function EHR_Obter_Especialidade(	nr_seq_especialidade_p	number)
					return	varchar2 is

ds_retorno_w		varchar2(80);

begin

if	(nr_seq_especialidade_p > 0) then
	begin
	select	ds_especialidade
	into	ds_retorno_w
	from	ehr_especialidade
	where	nr_sequencia	= nr_seq_especialidade_p;
	end;
end if;

return ds_retorno_w;
	
end EHR_Obter_Especialidade;
/