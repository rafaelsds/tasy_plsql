create or replace 
function rop_obter_local_roupa(	cd_barra_roupa_p	varchar2)
return number is

ds_retorno_w		number(10);
nr_seq_local_w		number(10);

begin
if	(cd_barra_roupa_p is not null) then
	select	max(nr_seq_local)
	into	ds_retorno_w
	from	rop_roupa
	where	cd_barra_roupa = cd_barra_roupa_p
	and	ie_situacao = 'A';
end if;

return ds_retorno_w;

end rop_obter_local_roupa;
/