create or replace
function obter_se_setor_exec_lab_loc(	cd_setor_atendimento_p		Number,
				cd_estabelecimento_p		Number,
				cd_procedimento_p		number,
				ie_origem_proced_p		Number,
				nr_seq_exame_p			number,
				cd_setor_prescr_p		number)
				return varchar2 is

ds_retorno_w			varchar2(1) := 'S';
qt_setor_adic_w			Number(10,0);
qt_setor_exclusivo_w		Number(10,0);
qt_setor_interno_w		number(10,0);
ie_outro_estab_w		varchar2(1);
cd_classif_setor_w		Varchar2(2);
cd_estabelecimento_base_w	Number(4,0);

begin
if	(nvl(cd_setor_atendimento_p,0) > 0) then
	select	nvl(vl_parametro, vl_parametro_padrao)
	into	ie_outro_estab_w
	from	funcao_parametro
	where	cd_funcao	= 0
	and	nr_sequencia	= 60;

	select	cd_classif_setor,
		cd_estabelecimento_base
	into	cd_classif_setor_w,
		cd_estabelecimento_base_w
	from	setor_atendimento
	where	cd_setor_atendimento	= cd_setor_atendimento_p;

	if	(cd_classif_setor_w in (6,7)) or
		((ie_outro_estab_w = 'N') and (cd_estabelecimento_base_w <> cd_estabelecimento_p)) then
		ds_retorno_w	:= 'N';
	end if;

	if	(ds_retorno_w = 'S') then
		select	count(*)
		into	qt_setor_exclusivo_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p
		and	cd_setor_exclusivo is not null;

		select	count(*)
		into	qt_setor_adic_w
		from	procedimento_setor_atend
		where	cd_procedimento		= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p
		and	cd_estabelecimento	= cd_estabelecimento_p
		and	nvl(cd_setor_origem, nvl(cd_setor_prescr_p,0)) = nvl(cd_setor_prescr_p,0);

		select	count(*)
		into	qt_setor_interno_w
		from	exame_lab_setor
		where	nr_seq_exame	= nr_seq_exame_p
		and	nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p;

		if	(qt_setor_exclusivo_w > 0) or
			(qt_setor_adic_w > 0) or
			(qt_setor_interno_w > 0) then
			ds_retorno_w	:= 'N';
	
			select	count(*)
			into	qt_setor_exclusivo_w
			from	procedimento
			where	cd_procedimento		= cd_procedimento_p
			and	ie_origem_proced	= ie_origem_proced_p
			and	cd_setor_exclusivo	= cd_setor_atendimento_p;

			select	count(*)
			into	qt_setor_adic_w
			from	procedimento_setor_atend
			where	cd_procedimento		= cd_procedimento_p
			and	ie_origem_proced	= ie_origem_proced_p
			and	cd_estabelecimento	= cd_estabelecimento_p
			and	cd_setor_atendimento	= cd_setor_atendimento_p
			and	nvl(cd_setor_origem, nvl(cd_setor_prescr_p,0)) = nvl(cd_setor_prescr_p,0);

			select	count(*)
			into	qt_setor_interno_w
			from	exame_lab_setor
			where	nr_seq_exame	= nr_seq_exame_p
			and	cd_setor_atendimento	= cd_setor_atendimento_p
			and	nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p;

			if	(qt_setor_exclusivo_w > 0) or
				(qt_setor_adic_w > 0) or
				(qt_setor_interno_w > 0) then
				ds_retorno_w	:= 'S';
			end if;
		end if;
	end if;
end if;

return	ds_retorno_w;

end obter_se_setor_exec_lab_loc;
/
