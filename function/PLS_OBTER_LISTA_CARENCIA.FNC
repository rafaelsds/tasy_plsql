create or replace
function pls_obter_lista_carencia(	nr_seq_segurado_p	number,
					nr_seq_contrato_p	number,
					nr_seq_plano_p		number,
					ie_opcao_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(4000)	:= '';
nr_seq_carencia_w	number(10);
ds_carencia_w		varchar2(255) := '';
dt_validade_w		varchar2(100);
cont_w			number(5)	:= 1;

Cursor C01 is
	select	x.nr_sequencia,
		upper(substr(obter_descricao_padrao('PLS_TIPO_CARENCIA','DS_CARENCIA',x.nr_seq_tipo_carencia),1,200)) ds_carencia,
		'Isento' dt_validade
	from	pls_carencia x
	where	x.nr_seq_segurado	= nr_seq_segurado_p
	and	(nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0)) < trunc(sysdate,'dd')
	union
	select	x.nr_sequencia,
		upper(substr(obter_descricao_padrao('PLS_TIPO_CARENCIA','DS_CARENCIA',x.nr_seq_tipo_carencia),1,200)) ds_carencia,
		to_char((nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0))) dt_validade
	from	pls_carencia x
	where	x.nr_seq_segurado	= nr_seq_segurado_p
	and	(nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0)) >= trunc(sysdate,'dd')
	union
	select	x.nr_sequencia,
		upper(substr(obter_descricao_padrao('PLS_TIPO_CARENCIA','DS_CARENCIA',x.nr_seq_tipo_carencia),1,255)) ds_carencia,
		'Isento' dt_validade
	from	pls_carencia x
	where	x.nr_seq_contrato	= nr_seq_contrato_p
	and	not exists	(select	1
				from	pls_carencia x
				where	x.nr_seq_segurado	= nr_seq_segurado_p
				and	pls_obter_se_cpt(x.nr_seq_tipo_carencia)	= 'N')
	and	(nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0)) < trunc(sysdate,'dd')
	union
	select	x.nr_sequencia,
		upper(substr(obter_descricao_padrao('PLS_TIPO_CARENCIA','DS_CARENCIA',x.nr_seq_tipo_carencia),1,255)) ds_carencia,
		to_char((nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0))) dt_validade
	from	pls_carencia x
	where	x.nr_seq_contrato	= nr_seq_contrato_p
	and	not exists	(select	1
				from	pls_carencia x
				where	x.nr_seq_segurado	= nr_seq_segurado_p
				and	pls_obter_se_cpt(x.nr_seq_tipo_carencia)	= 'N')
	and	(nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0)) >= trunc(sysdate,'dd')
	union
	select	x.nr_sequencia,
		upper(substr(obter_descricao_padrao('PLS_TIPO_CARENCIA','DS_CARENCIA',x.nr_seq_tipo_carencia),1,255)) ds_carencia,
		'Isento' dt_validade
	from	pls_carencia x
	where	nr_seq_plano		= nr_seq_plano_p
	and	not exists	(select	1
			from	pls_carencia x
			where	x.nr_seq_segurado	= nr_seq_segurado_p
			and	pls_obter_se_cpt(x.nr_seq_tipo_carencia)	= 'N')
	and	not exists	(select	1
				from	pls_carencia x
				where	x.nr_seq_contrato	= nr_seq_contrato_p
				and	pls_obter_se_cpt(x.nr_seq_tipo_carencia)	= 'N')
	and	(nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0)) < trunc(sysdate,'dd')
	union
	select	x.nr_sequencia,
		upper(substr(obter_descricao_padrao('PLS_TIPO_CARENCIA','DS_CARENCIA',x.nr_seq_tipo_carencia),1,255)) ds_carencia,
		to_char((nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0))) dt_validade
	from	pls_carencia x
	where	nr_seq_plano		= nr_seq_plano_p
	and	not exists	(select	1
				from	pls_carencia x
				where	x.nr_seq_segurado	= nr_seq_segurado_p
				and	pls_obter_se_cpt(x.nr_seq_tipo_carencia)	= 'N')
	and	not exists	(select	1
				from	pls_carencia x
				where	x.nr_seq_contrato	= nr_seq_contrato_p
				and	pls_obter_se_cpt(x.nr_seq_tipo_carencia)	= 'N')
	and	(nvl(x.dt_inicio_vigencia,to_date(substr(pls_obter_dados_segurado(nr_seq_segurado_p,'D'),1,10),'dd/mm/yyyy')) + nvl(x.qt_dias,0)) >= trunc(sysdate,'dd')
	order	by 1;
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_carencia_w,
	ds_carencia_w,
	dt_validade_w;
exit when C01%notfound;
	if (cont_w = ie_opcao_p) then
		ds_retorno_w	:= ds_retorno_w	|| substr(ds_carencia_w || '                                                  ',1,40) || dt_validade_w || chr(10);
	end if;
	cont_w := cont_w + 1;
end loop;
close C01;
	

return	ds_retorno_w;

end pls_obter_lista_carencia;
/