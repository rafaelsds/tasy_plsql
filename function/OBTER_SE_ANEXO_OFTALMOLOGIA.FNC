create or replace
function obter_se_anexo_oftalmologia		(nr_atendimento_p		number,
						 cd_pessoa_fisica_p		varchar2)
						 return varchar2 is
						 
qt_anexos_oft_w         number(10) := 0;
ie_retorno_w	        varchar2(1):= 'N';


begin

if	(nr_atendimento_p is not null) or (cd_pessoa_fisica_p is not null) then
	SELECT	SUM(valor1) 
	into 		qt_anexos_oft_w
	FROM   	(	SELECT  	COUNT(*) valor1  
					FROM    	oft_anexo a, 
								oft_consulta b  
					WHERE   	a.nr_seq_consulta  = b.nr_sequencia 
					and		b.dt_cancelamento is null
					AND     	b.nr_atendimento   = nr_atendimento_p 
					UNION ALL 
					SELECT  	COUNT(*) valor1 
					FROM    	oft_anexo a, 
								oft_consulta b, 
								atendimento_paciente c
					WHERE   	a.nr_seq_consulta  = b.nr_sequencia  
					AND     	b.nr_atendimento   = c.nr_atendimento  
					and		b.dt_cancelamento is null
					AND     	c.cd_pessoa_fisica = cd_pessoa_fisica_p 
					UNION ALL 
					SELECT  	COUNT(*) valor1 
					FROM    	oft_anexo x, 
								oft_consulta y 
					WHERE   	x.nr_seq_consulta  = y.nr_sequencia 
					AND     	y.cd_pessoa_fisica = cd_pessoa_fisica_p
					and		y.dt_cancelamento is null);
end if;					

if	 (qt_anexos_oft_w > 0)  then
		ie_retorno_w	:=	'S';
end if;

return	ie_retorno_w;

end obter_se_anexo_oftalmologia;
/