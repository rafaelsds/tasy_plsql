create or replace
function lote_ent_obter_ds_status_fase(nr_seq_status_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
				
begin

if (nr_seq_status_p is not null) then

	/*select	substr(obter_valor_dominio(5586, nr_seq_status_p),1,255)
	into	ds_retorno_w
	from	dual;*/
	
	select 	max(substr(ds_status,1,255))
	into	ds_retorno_w
	from	lote_ent_status_busca	
	where	nr_sequencia = nr_seq_status_p;
	
end if;

return	ds_retorno_w;

end lote_ent_obter_ds_status_fase;
/
