create or replace
function pls_obter_processo_requisicao
			(	nr_seq_requisicao_p		number)
				return varchar2 is
	
ds_retorno_w			varchar2(2);

begin


begin
	select	ie_tipo_processo
	into	ds_retorno_w
	from	pls_requisicao
	where	nr_sequencia = nr_seq_requisicao_p;
exception
when others then
	ds_retorno_w := null;
end;

return	ds_retorno_w;

end pls_obter_processo_requisicao;
/