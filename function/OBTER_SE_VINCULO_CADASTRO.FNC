create or replace
function Obter_Se_Vinculo_Cadastro
			(cd_medico_p		varchar2)
 		    return varchar2 is
			
ie_permite_cadastro_w		varchar2(2);
qt_reg_w					number(10,0);
ie_vinculo_medico_w			number(10,0);

begin

ie_permite_cadastro_w		:= 'S';

select 	count(*)
into	qt_reg_w
from	REGRA_VINCULO_CADASTRO_PF;

if	(qt_reg_w > 0) then
	begin

	select	max(ie_vinculo_medico)
	into	ie_vinculo_medico_w
	from	medico
	where	cd_pessoa_fisica	= cd_medico_p;
	
	select 	nvl(max(IE_PERMITE_MANUTENCAO), 'S')
	into	ie_permite_cadastro_w
	from	REGRA_VINCULO_CADASTRO_PF
	where	ie_vinculo_medico	= ie_vinculo_medico_w;

	end;
end if;

return	ie_permite_cadastro_w;

end Obter_Se_Vinculo_Cadastro;
/