create or replace
function obter_desc_tipo_doc_item
			(	nr_seq_tipo_doc_item_p		number)
				return varchar2 is

ds_descricao_w			varchar2(100);

begin
if	(nr_seq_tipo_doc_item_p is not null) then
	select 	max (ds_tipo_item) 
	into 	ds_descricao_w
	from 	tipo_doc_item 
	where 	nr_sequencia = nr_seq_tipo_doc_item_p;	

end if; 

return ds_descricao_w;

end obter_desc_tipo_doc_item;
/
	