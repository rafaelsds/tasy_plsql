create or replace
function pls_obter_contrato_ops_inter
			(	nr_seq_contrato_p	number,
				nr_seq_intercambio_p	number)
 		    	return Varchar2 is

ds_retorno_w	varchar2(255);

begin

if	(nvl(nr_seq_contrato_p,0) <> 0) then
	begin
	select	substr(obter_nome_pf_pj(cd_pf_estipulante, cd_cgc_estipulante),1,255)
	into	ds_retorno_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_p;
	exception
	when others then
		ds_retorno_w	:= '';
	end;
elsif	(nvl(nr_seq_intercambio_p,0) <> 0) then
	begin
	select	substr(obter_nome_pf_pj(cd_pessoa_fisica, cd_cgc),1,200)
	into	ds_retorno_w
	from	pls_intercambio
	where	nr_sequencia	= nr_seq_intercambio_p;
	exception
	when others then
		ds_retorno_w	:= '';
	end;
else
	ds_retorno_w	:= '';
end if;

return	ds_retorno_w;

end pls_obter_contrato_ops_inter;
/