create or replace
function rxt_obter_desc_classif_ac	(nr_seq_classif_p	number)
				return varchar2 is

ds_classificacao_w	varchar2(40);

begin
if	(nr_seq_classif_p is not null) then

	select	max(ds_classificacao)
	into	ds_classificacao_w
	from	rxt_classif_acessorio
	where	nr_sequencia = nr_seq_classif_p;

end if;

return ds_classificacao_w;

end rxt_obter_desc_classif_ac;
/