create or replace
function obter_tipo_atributo(	nr_sequencia_p	number,
				nm_atributo_p	varchar2)
 		    	return varchar2 is
			
ds_valor_dominio_w	varchar2(15);
ie_tipo_atributo_w	varchar2(10);	
begin



select 	max(ie_tipo_atributo)
into	ie_tipo_atributo_w
from 	tabela_atributo
where 	nm_tabela  = (	Select max(b.nm_tabela) 
			from  tabela_visao b
			where b.nr_sequencia = nr_sequencia_p)
and 	nm_atributo = nm_atributo_p;

if 	(ie_tipo_atributo_w is not null) then 
	select	max(ds_valor_dominio)
	into	ds_valor_dominio_w
	from 	valor_dominio
	where 	cd_dominio = 85
	and 	vl_dominio = ie_tipo_atributo_w;
end if;

return	ds_valor_dominio_w;

end obter_tipo_atributo;
/