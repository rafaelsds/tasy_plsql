create or replace
function obter_qt_episodio_status(nr_sequencia_p        number)
                        return number is

nr_seq_episodio_w               number(10);

begin

select count(*)
into nr_seq_episodio_w
from episodio_paciente_status 
where NR_SEQ_EPISODIO = nr_sequencia_p;

return  nr_seq_episodio_w;

end obter_qt_episodio_status;
/
