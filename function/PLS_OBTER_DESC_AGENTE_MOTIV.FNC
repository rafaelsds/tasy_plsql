create or replace
function pls_obter_desc_agente_motiv(nr_seq_agente_motivador_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);

begin

select	ds_agente_motivador
into	ds_retorno_w
from	pls_agente_motivador
where	nr_sequencia = nr_seq_agente_motivador_p;

return	ds_retorno_w;

end pls_obter_desc_agente_motiv;
/