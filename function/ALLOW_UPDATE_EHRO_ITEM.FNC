create or replace
function allow_update_ehro_item(nr_cirurgia_p		number, 
								nr_seq_pepo_p		number,
								nr_atendimento_p 	number) 
						return varchar2 is

ie_permite_alterar_w 		varchar2(1) := 'S';
ie_perm_real_w				varchar2(1)	:= '';
ie_permite_inserir_info_w	varchar2(1)	:= '';
qt_min_apos_alta_w			number(10)	:= 0;
dt_atendimento_w			date		:= null;

begin

ie_perm_real_w 				:= obter_valor_param_usuario(872, 119, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
ie_permite_inserir_info_w	:= obter_valor_param_usuario(872, 90, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
qt_min_apos_alta_w			:= to_number(nvl(obter_valor_param_usuario(872, 131, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento), 0));

if	(ie_perm_real_w = 'N') then
	if	(nvl(nr_cirurgia_p,0) > 0) then
		select	nvl(max('S'),'N')
		into	ie_permite_alterar_w
		from	cirurgia 
		where	nr_cirurgia = nr_cirurgia_p 
		and		ie_status_cirurgia not in (3,4);
	else	
		select	nvl(max('S'),'N')
		into	ie_permite_alterar_w
		from	cirurgia 
		where	nr_seq_pepo = nr_seq_pepo_p 
		and		ie_status_cirurgia in (3,4);
	end if;
end if;	
	
if	(ie_permite_inserir_info_w = 'N' and ie_permite_alterar_w = 'S') then
	begin
	dt_atendimento_w := TO_DATE(obter_dados_atendimento(nr_atendimento_p, 'DA'), 'dd/mm/yyyy hh24:mi:ss');
	if	(dt_atendimento_w is not null) and
		(sysdate >= (dt_atendimento_w + (qt_min_apos_alta_w/1440))) then
		begin
			ie_permite_alterar_w := 'N';
		end;
	end if;
	end;
end if;						

return ie_permite_alterar_w;

end allow_update_ehro_item;
/
