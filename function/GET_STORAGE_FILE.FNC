create or replace function get_storage_file(ds_storage in varchar2) return varchar2 is 
    posicaoIniCaminho number(4);
    posicaofimcaminho number(4);
    posicaofimnome    number(4);
    lengthw           number(4);
    storagew          varchar2(4200 CHAR);
    ds_path           varchar2(4200 CHAR);
    cduuid            varchar2(4200 CHAR);
    nomeArq           varchar2(4200 CHAR);
begin 
   select instr(ds_storage, 'tasy-storage://') 
     into posicaoinicaminho
     from dual;

   if (posicaoinicaminho > 0) then
      select instr(ds_storage, '.') 
        into posicaoinicaminho
       from dual;

      select length(ds_storage) into lengthw from dual;

      select substr(ds_storage,posicaoinicaminho + 1, lengthw)
        into storagew 
      from dual;         

      select instr(storagew, '/') 
        into posicaofimcaminho
      from dual;

      select substr(storagew,0,posicaofimcaminho)
        into cduuid
      from dual;

      cduuid := replace(cduuid,'/','');

      select a.ds_path
        into ds_path
        from file_storage_path a
       where cd_uuid = cduuid;

       select length(storagew) into lengthw from dual;
      select substr(storagew, posicaofimcaminho, lengthw) 
         into storagew
       from dual;        

      select instr(storagew, '?') 
        into posicaofimnome
      from dual;

      select substr(storagew, 2, posicaofimnome -2) 
         into  nomeArq
       from dual;               

       return ds_path ||'\'||nomeArq;
   end if;
   return null;
end get_storage_file;
/
