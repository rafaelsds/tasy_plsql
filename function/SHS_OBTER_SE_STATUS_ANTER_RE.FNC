create or replace
function shs_obter_se_status_anter_re (  nr_sequencia_p	number)
					return varchar is
					
ie_status_agenda_w	varchar2(15);

begin

SELECT		MAX(NVL('S','N'))
into		ie_status_agenda_w
FROM		AGENDA_PACIENTE
WHERE		nr_sequencia = nr_sequencia_p
AND			EXISTS(SELECT		ie_status_agenda
			FROM			agenda_historico_acao
			WHERE			NR_SEQ_AGENDA = nr_sequencia_p
			AND			ie_status_agenda = 'RE');				

return	ie_status_agenda_w;

end shs_obter_se_status_anter_re;
/