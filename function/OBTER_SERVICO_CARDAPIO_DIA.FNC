create or replace
function obter_servico_cardapio_dia(nr_seq_cardapio_dia_p	number)
 		    	return number is

vl_retorno_w	number(10,0);
			
begin

if	(nr_seq_cardapio_dia_p > 0) then
	
	select	max(b.nr_seq_servico)
	into	vl_retorno_w
	from	nut_cardapio a,
		nut_cardapio_dia b
	where	b.nr_sequencia = a.nr_seq_card_dia
	and	a.nr_sequencia = nr_seq_cardapio_dia_p; 

end if;

return	vl_retorno_w;

end obter_servico_cardapio_dia;
/