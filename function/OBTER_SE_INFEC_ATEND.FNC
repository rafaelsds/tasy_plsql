create or replace
function obter_se_infec_atend(nr_atendimento_p		number)
 		    	return varchar2 is

cd_pessoa_fisica_w	varchar2(60);
nr_atendimento_w	number;
qt_tmp_w		number;
qt_infec_w		number;

begin

if	(nr_atendimento_p > 0) then

	
	select	count(*)
	into	qt_tmp_w
	from	cih_ficha_ocorrencia f,
		cih_local_infeccao i
	where	f.nr_ficha_ocorrencia	= i.nr_ficha_ocorrencia
	and	f.nr_atendimento	= nr_atendimento_p;
	
	
	if	(qt_tmp_w	>0) then
		return 'S';
	end if;


end if;


return	'N';

end obter_se_infec_atend;
/
