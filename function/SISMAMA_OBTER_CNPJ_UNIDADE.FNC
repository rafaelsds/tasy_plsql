Create Or Replace 
Function Sismama_Obter_Cnpj_Unidade(Nr_Seq_Unidade_Saude_P		Number)
				Return Varchar2 As 
				
cd_cnpj_w	varchar2(14);

Begin

Select 	nvl(max(Cd_Cnpj),'0')
Into	Cd_Cnpj_W
From 	Sus_Unidade_Saude
Where	nr_sequencia = nr_seq_unidade_saude_p;

Return	Cd_Cnpj_W;

end Sismama_Obter_Cnpj_Unidade;
/