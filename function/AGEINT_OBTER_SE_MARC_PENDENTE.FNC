create or replace
function Ageint_Obter_Se_Marc_Pendente(
			nm_usuario_p	varchar2)
 		    	return varchar2 is


qt_marc_pendente_w	number(10)	:= 0;
cd_profissional_w	varchar2(10);
nr_Seq_Ageint_w		number(10);
ds_retorno_W		varchar2(1)	:= 'N';
			
Cursor C01 is
	select	nr_sequencia
	from	agenda_integrada
	where	dt_inicio_Agendamento between trunc(sysdate) and trunc(sysdate) +  + 86399/86400
	and	cd_profissional	= cd_profissional_w;
			
begin

select 	max(cd_pessoa_fisica)
into	cd_profissional_W
from	usuario
where	nm_usuario	= nm_usuario_p;

open C01;
loop
fetch C01 into	
	nr_Seq_Ageint_w;
exit when C01%notfound;
	begin
	
	if	(qt_marc_pendente_w = 0) then
		select	count(*)
		into	qt_marc_pendente_w
		from	ageint_marcacao_usuario
		where	nr_seq_ageint			= nr_seq_Ageint_w
		and	nvl(ie_gerado,'N') 		= 'N'
		and	nvl(ie_horario_auxiliar,'N')	= 'N'
		and	nm_usuario			= nm_usuario_p;
	end if;
	
	end;
end loop;
close C01;

if	(qt_marc_pendente_w	> 0) then
	ds_retorno_W	:= 'S';
end if;

return	ds_retorno_W;

end Ageint_Obter_Se_Marc_Pendente;
/