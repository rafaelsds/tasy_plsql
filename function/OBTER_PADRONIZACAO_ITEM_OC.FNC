create or replace
function obter_padronizacao_item_oc(	cd_estabelecimento_p	number,
					ie_tipo_ordem_p		varchar2,
					cd_material_p			number,
					ie_opcao_p			varchar2)
return varchar2 is						

/*ie_opcao_p
L	- IE_LIBERADO
C	- IE_EXIGE_CENTRO_CUSTO*/

cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
ie_liberado_w				varchar2(1) := 'S';
ie_exige_centro_custo_w			varchar2(1) := 'N';
ds_retorno_w				varchar2(1);

cursor c01 is
select	nvl(ie_liberado,'S'),
	nvl(ie_exige_centro_custo,'N')
from	regra_padronizacao_oc a,
	regra_padronizacao_oc_item b
where	a.nr_sequencia = b.nr_seq_regra
and	a.ie_tipo_ordem = ie_tipo_ordem_p
and	a.cd_estabelecimento = cd_estabelecimento_p
and	nvl(b.cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(b.cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(b.cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(b.cd_material, cd_material_p) 			= cd_material_p or cd_material_p = 0)
order by
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);
	
begin

select	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v a
where	a.cd_material = cd_material_p;

open C01;
loop
fetch C01 into	
	ie_liberado_w,
	ie_exige_centro_custo_w;
exit when C01%notfound;
	begin
	ie_liberado_w				:= ie_liberado_w;
	ie_exige_centro_custo_w			:= ie_exige_centro_custo_w;
	end;
end loop;
close C01;

if	(ie_opcao_p = 'L') then
	ds_retorno_w := ie_liberado_w;
elsif	(ie_opcao_p = 'C') then
	ds_retorno_w := ie_exige_centro_custo_w;
end if;

return ds_retorno_w;

end obter_padronizacao_item_oc;
/
