create or replace
function	ca_obter_status_atividade(
			nr_sequencia_p		number)
		return varchar2 is

/* IE_RETORNO
P = Pendente
A = Pendente em atraso
C = Cancelado
R = Realizado
*/

ie_status_w		varchar2(15);
ie_retorno_w		varchar2(15);
dt_prevista_w		date;

begin

select	ie_status,
	trunc(nvl(dt_prevista,sysdate))
into	ie_status_w,
	dt_prevista_w
from	ca_controle_atividade
where	nr_sequencia = nr_sequencia_p;

if	(ie_status_w = 'P') and
	(dt_prevista_w < trunc(sysdate)) then
	ie_retorno_w	:= 'A';
else
	ie_retorno_w	:= ie_status_w;
end if;

return	ie_retorno_w;

end ca_obter_status_atividade;
/