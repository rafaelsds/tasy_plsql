create or replace
FUNCTION hsc_LAB_OBTER_FORMATO_ITEM ( nr_seq_formato_p  number,
                                    nr_linha_p        number,
                                    nr_prescricao_p   number,
                                    linha_p               number)
                                Return varchar2 is


cursor  C01 is
        select  distinct
                substr(b.nm_exame,1,40),
                j.nr_linha,
                decode(c.ie_urgencia, 'S', ' (Urg�nte)', '')
        from    exame_lab_format_item j,
                exame_laboratorio b,
                prescr_procedimento c
        where   j.nr_seq_exame  = b.nr_seq_exame
        and     j.nr_seq_formato        = nr_seq_formato_p
        and     c.nr_prescricao = nr_prescricao_p
        and     c.qt_procedimento >= linha_p
                and             hsc_obter_se_exame_result(c.nr_prescricao, c.nr_sequencia) = 'N'
        order by 2,1;


ds_retorno_w            VARCHAR2(254);
ds_exame_w              VARCHAR2(200);
nr_linha_w              NUMBER(02,0);
qt_registros_w          NUMBER(02,0) := 0;

ie_urgente_w            varchar2(12);


begin

open    c01;
loop
fetch   c01 into
        ds_exame_w,
        nr_linha_w,
        ie_urgente_w;
exit    when c01%notfound;

        begin

        qt_registros_w := qt_registros_w + 1;

        if (qt_registros_w = nr_linha_p) then
                ds_retorno_w := ds_exame_w||ie_urgente_w;
                exit;
        end if;
        end;
end loop;

return ds_retorno_w;

end hsc_LAB_OBTER_FORMATO_ITEM;
/