create or replace 
function verificar_cota_munic_agepac
				(	
				cd_agenda_p		number,
				dt_agenda_p		date,
				cd_pessoa_fisica_p	varchar2,
				cd_convenio_p	number,
				cd_procedimento_p	number,
				ie_origem_proced_p	number,
				nr_seq_proc_interno_p	number,
				cd_medico_exec_p	varchar2,
				cd_estabelecimento_p	number,
				ie_proximo_mes_p	Varchar2
				) return varchar2 is

ie_regra_w		varchar2(01)	:= 'S';
dt_agenda_w		date;
cd_municipio_ibge_w	varchar2(06);
cd_area_proced_w	number(8,0);
cd_espec_proced_w	number(8,0);
cd_grupo_proced_w	number(15,0);
qt_saldo_w		number(5,0);
qt_existe_w		number(10,0);
nr_seq_forma_org_w	Number(10);
nr_seq_grupo_w		Number(10);
nr_seq_subgrupo_w	Number(10);
nr_Seq_regra_w		Number(10);
qt_Regra_w		Number(5);

Cursor C01 is
	select	nvl(nr_sequencia,0)
	from	agenda_regra
	where	cd_agenda		= cd_agenda_p
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	((cd_convenio		= cd_convenio_p) or (cd_convenio is null))
	and	((cd_area_proc		= cd_area_proced_w) or (cd_area_proc is null))
	and	((cd_especialidade	= cd_espec_proced_w) or (cd_especialidade is null))
	and	((cd_grupo_proc		= cd_grupo_proced_w) or (cd_grupo_proc is null))
	and	nvl(nr_seq_forma_org, nr_seq_forma_org_w)	= nr_seq_forma_org_w 	
	and	nvl(nr_seq_grupo, nr_seq_grupo_w)		= nr_seq_grupo_w 	
	and	nvl(nr_seq_subgrupo, nr_seq_subgrupo_w)		= nr_seq_subgrupo_w
	and	((cd_procedimento	= cd_procedimento_p) or (cd_procedimento is null))
	and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_p) or (ie_origem_proced is null)))
	and	((nr_seq_proc_interno	= nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
	and	((cd_medico		= cd_medico_exec_p) or (cd_medico is null))
	and	cd_municipio_ibge	= cd_municipio_ibge_w
	and	qt_regra		> 0
	order by	nvl(cd_procedimento,0),
		nvl(nr_seq_proc_interno,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_proc,0),
		nvl(nr_seq_forma_org,0),
		nvl(nr_seq_subgrupo,0),
		nvl(nr_seq_grupo,0),
		nvl(cd_medico,'0');
			
begin

select	pkg_date_utils.start_of(decode(ie_proximo_mes_p, 'S', pkg_date_utils.add_month(dt_agenda_p,1,0), dt_agenda_p), 'MONTH',0),
	substr(obter_compl_pf(cd_pessoa_fisica_p, 1,'CDM'),1,6)
into	dt_agenda_w,
	cd_municipio_ibge_w
from	dual;
	
/* obter informa��o do procedimento */
select	nvl(max(cd_area_procedimento),0),
	nvl(max(cd_especialidade),0),
	nvl(max(cd_grupo_proc),0),
	nvl(max(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento, ie_origem_proced, 'C', 'F'),'F'),1,10)),0),
	nvl(max(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento, ie_origem_proced, 'C', 'G'),'G'),1,10)),0),
	nvl(max(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento, ie_origem_proced, 'C', 'S'),'S'),1,10)),0)
into	cd_area_proced_w,
	cd_espec_proced_w,
	cd_grupo_proced_w,
	nr_seq_forma_org_w,
	nr_seq_grupo_w,
	nr_seq_subgrupo_w
from	estrutura_procedimento_v
where	cd_procedimento	= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
	
	
open C01;
loop
fetch C01 into	
	nr_seq_regra_w;
exit when C01%notfound;
	begin
	nr_seq_regra_w	:= nr_seq_regra_w;		
	end;
end loop;
close C01;
/* Verifica se possui regra conforme os dados da agenda */
	
/* Se possuir regra */
if	(nr_seq_regra_w > 0) then
	begin
	
	select	nvl(max(qt_regra),0)
	into	qt_regra_w
	from	agenda_regra
	where	nr_sequencia = nr_seq_regra_w;
	
	select	count(*)
	into	qt_existe_w
	from	agenda_paciente_cota_munic
	where	cd_convenio		= cd_convenio_p
	and	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p
	and	nr_seq_proc_interno	= nr_seq_proc_interno_p
	and	cd_municipio_ibge 	= cd_municipio_ibge_w
	and	dt_mes_referencia	= dt_agenda_w
	and	ie_situacao		= 'A';
		
	if	(qt_existe_w > 0) then
		begin
		/* Verifica o saldo do m�s que est� sendo agendado */
		select	nvl(sum(qt_agendada),0)
		into	qt_saldo_w
		from	agenda_paciente_cota_munic
		where	cd_convenio		= cd_convenio_p
		and	cd_procedimento		= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p
		and	nr_seq_proc_interno	= nr_seq_proc_interno_p
		and	cd_municipio_ibge 	= cd_municipio_ibge_w
		and	dt_mes_referencia	= dt_agenda_w
		and	ie_situacao		= 'A';
		
		/* Se saldo maior que zero, n�o apresentar� os dados da cota por munic�pio*/
		if	(qt_saldo_w >= qt_regra_w) then
			ie_regra_w := 'N';
		end if;
		end;
	else
		ie_regra_w := 'S';
	end if;
	end;
else
	ie_Regra_w	:= 'I';
end if;

return ie_regra_w;

end verificar_cota_munic_agepac;
/