create or replace
function obter_data_limite_dep_ident(nr_seq_deposito_p		number)
					return date is

ds_retorno_w	date;
					
begin
select	nvl(dt_limite_deposito,sysdate)
into	ds_retorno_w
from	deposito_identificado
where	nr_sequencia = nr_seq_deposito_p;

return	ds_retorno_w;

end obter_data_limite_dep_ident;
/