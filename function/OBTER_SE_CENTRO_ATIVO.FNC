create or replace
function obter_se_centro_ativo(	cd_centro_controle_p	number,
								cd_estabelecimento_p number)
 		    	return varchar2 is

ds_retorno_w	varchar2(2);
			
begin
select	max(nvl(ie_situacao,0))
into	ds_retorno_w
from 	centro_controle
where	cd_centro_controle = cd_centro_controle_p
and		cd_estabelecimento = cd_estabelecimento_p;

return	ds_retorno_w;

end obter_se_centro_ativo;
/