CREATE OR REPLACE
FUNCTION  ctb_obter_rel_bal_tipo_custo (	cd_estab_p		NUMBER,
					mes_p				VARCHAR,
					ano_ref_p			NUMBER,
					ie_normal_encerramento_p	VARCHAR2,
					cd_empresa_p		NUMBER,
					cd_classificacao_p		VARCHAR2,
					cd_conta_contabil_p	VARCHAR2,
					nr_nivel_p		VARCHAR2,
					nm_usuario_p		VARCHAR2,
					ie_tipo_custo_P		VARCHAR) RETURN NUMBER IS


ie_normal_encerramento_w	VARCHAR2(1);
cd_empresa_w		NUMBER(4);
cd_conta_contabil_w	VARCHAR2(20);
cd_classificacao_w		VARCHAR2(40);
cd_grupo_w		NUMBER(10);
ds_gerencial_w		VARCHAR2(255);

ie_tipo_w			VARCHAR2(1);
ie_tipo_conta_w		VARCHAR2(1);
ie_tipo_custo_w		VARCHAR2(5);
ie_nivel_w		NUMBER(10);
ie_centro_custo_w		VARCHAR2(1);
ie_gerar_w		VARCHAR2(1);
ie_superior_w		VARCHAR2(1);

vl_saldo_w		NUMBER(15,2);
qt_conta_w		NUMBER(10);

BEGIN

qt_conta_w	:= 0;


SELECT	a.ie_tipo_conta,
	a.cd_conta_contabil,
	a.cd_grupo,
	a.cd_classificacao,
	a.ds_conta_apres ds_gerencial
INTO	ie_tipo_conta_w,
	cd_conta_contabil_w,
	cd_grupo_w,
	cd_classificacao_w,
	ds_gerencial_w
FROM	ctb_balancete_v2 a
WHERE	a.cd_empresa		= NVL(cd_empresa_p, a.cd_empresa)
AND	a.cd_estabelecimento	= NVL(cd_estab_p, a.cd_estabelecimento)
AND	a.ie_normal_encerramento	= ie_normal_encerramento_p
AND	a.dt_referencia BETWEEN ctb_obter_mes_ref(ano_ref_p) AND fim_ano(ctb_obter_mes_ref(ano_ref_p))
AND	((TO_CHAR(a.dt_referencia, 'mm') = mes_p) OR (mes_p = 'T'))
AND	a.ie_nivel	<= NVL(nr_nivel_p, a.ie_nivel)
AND	a.cd_conta_contabil = cd_conta_contabil_p
AND	EXISTS(	SELECT	1
		FROM	centro_custo y,
			ctb_saldo x
		WHERE	x.cd_centro_custo	= y.cd_centro_custo
		AND	x.nr_seq_mes_ref	= a.nr_seq_mes_ref
		AND	a.dt_referencia BETWEEN ctb_obter_mes_ref(ano_ref_p) AND fim_ano(ctb_obter_mes_ref(ano_ref_p))
		AND	y.ie_tipo_custo	IS NOT NULL
		AND	y.ie_tipo_custo	= ie_tipo_custo_p
		AND	ctb_obter_se_classif_sup(x.cd_classificacao, a.cd_classificacao) = 'S')
AND	(SUBSTR(obter_se_conta_vigente(a.cd_conta_contabil, ctb_obter_mes_ref(a.nr_seq_mes_ref)),1,1) = 'S')
GROUP BY	a.cd_conta_contabil,
		a.cd_classificacao,
	a.ds_conta_apres,
	a.ie_tipo_conta,
	a.cd_grupo
ORDER BY	cd_classificacao, ds_conta_apres;



		ie_gerar_w		:= 'S';
		vl_saldo_w		:= 0;

		IF	(NVL(cd_conta_contabil_p, 'X') <> 'X') THEN

			ie_gerar_w	:= SUBSTR(ctb_obter_se_elemento_contido(cd_conta_contabil_w, cd_conta_contabil_p), 1, 1);
		END IF;


		IF	(ie_gerar_w = 'S') THEN


			SELECT	MAX(ie_tipo)
			INTO	ie_tipo_conta_w
			FROM	conta_contabil
			WHERE	cd_conta_contabil	= cd_conta_contabil_w;


			/*somente se anal�tica, pois se for t�tulo futuramente ser� acumulada*/
			IF	(ie_tipo_conta_w <> 'T') THEN

				SELECT	SUM(a.vl_saldo) vl_saldo
				INTO	vl_saldo_w
				FROM	centro_custo b,
					ctb_balancete_v2 a
				WHERE	a.cd_centro_custo		= b.cd_centro_custo
				AND	a.dt_referencia BETWEEN ctb_obter_mes_ref(ano_ref_p) AND fim_ano(ctb_obter_mes_ref(ano_ref_p))
				AND	((TO_CHAR(a.dt_referencia, 'mm') = mes_p) OR (mes_p = 'T'))
				AND	a.cd_conta_contabil		= cd_conta_contabil_w
				AND	a.cd_classificacao		= cd_classificacao_w
				AND	a.ie_normal_encerramento	= ie_normal_encerramento_p
				AND	b.ie_tipo_custo			= ie_tipo_custo_p;

			ELSE

				SELECT	SUM(a.vl_saldo) vl_saldo
				INTO	vl_saldo_w
				FROM	centro_custo b,
					ctb_balancete_v2 a
				WHERE	a.cd_centro_custo		= b.cd_centro_custo
				AND	a.dt_referencia BETWEEN ctb_obter_mes_ref(ano_ref_p) AND fim_ano(ctb_obter_mes_ref(ano_ref_p))
				AND	((TO_CHAR(a.dt_referencia, 'mm') = mes_p) OR (mes_p = 'T'))
				AND	SUBSTR(ctb_obter_se_classif_sup(a.cd_classificacao, cd_classificacao_w), 1, 1) = 'S'
				AND	a.ie_normal_encerramento	= ie_normal_encerramento_p
				AND	b.ie_tipo_custo			= ie_tipo_custo_p;

			END IF;

		END IF;

		IF	(ie_gerar_w = 'S') THEN
			BEGIN


			vl_saldo_w	:= NVL(vl_saldo_w, 0);


			END;
		END IF;


RETURN vl_saldo_w;

END ctb_obter_rel_bal_tipo_custo;
/