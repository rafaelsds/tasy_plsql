create or replace
function sus_obter_desc_modalidade (cd_modalidade_p number)
		return varchar2 is
ds_retorno_w	Varchar2(30);

begin
begin
select	ds_modalidade
into	ds_retorno_w
from	sus_modalidade
where	cd_modalidade	= cd_modalidade_p;
exception
	when others then
		ds_retorno_w	:= null;
end;

return ds_retorno_w;
end sus_obter_desc_modalidade;
/