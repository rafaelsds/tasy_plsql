create or replace
function Obter_cor_Visitante (	nr_sequencia_p	number)
				return 		varchar2 is

ds_cor_w	varchar2(80);

begin

select	max(substr(ds_cor,1,80))
into	ds_cor_w
from	visitante
where	nr_sequencia	= nr_sequencia_p;

return ds_cor_w;

end Obter_cor_Visitante;
/
