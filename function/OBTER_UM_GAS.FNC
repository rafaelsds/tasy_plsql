create or replace
function Obter_UM_gas(	nr_seq_gas_p		number,
			ie_disp_resp_esp_p	varchar2)
 		    	return varchar2 is

ie_unidade_medida_w	varchar2(50);

begin

select	max(ie_unidade_medida)
into	ie_unidade_medida_w
from	regra_unidade_gas
where	nr_seq_gas		= nr_seq_gas_p
and	ie_disp_resp_esp	= ie_disp_resp_esp_p;

return	ie_unidade_medida_w;

end Obter_UM_gas;
/
