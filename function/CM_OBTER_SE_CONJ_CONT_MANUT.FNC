create or replace
function	cm_obter_se_conj_cont_manut(
			nr_seq_conj_cont_p	number)
		return varchar2 is

ie_retorno_w		varchar2(1) := 'N';
qt_existe_w		number(10);
nr_seq_conjunto_w	cm_conjunto_cont.nr_seq_conjunto%type;

begin

if	(nvl(nr_seq_conj_cont_p,0) <> 0) then
	begin

	select	nr_seq_conjunto
	into	nr_seq_conjunto_w
	from	cm_conjunto_cont
	where	nr_sequencia = nr_seq_conj_cont_p;
	
	select	count(*)
	into	qt_existe_w
	from	cm_manutencao_conj
	where	nr_seq_conjunto = nr_seq_conjunto_w
	and	dt_retorno is null;

	if	(qt_existe_w > 0) then
		ie_retorno_w	:= 'S';
	else
		ie_retorno_w	:= 'N';
	end if;

	end;
end if;

return	ie_retorno_w;

end cm_obter_se_conj_cont_manut;
/