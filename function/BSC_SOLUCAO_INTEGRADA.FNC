create or replace
function bsc_solucao_integrada(	dt_inicio_p		date,
				dt_fim_p		date)
 		    	return number is
qt_retorno_w	number(10);

begin
select	count(i.cd_cnpj)
into	qt_retorno_w
from (	select	count(*) qt,
		cd_cnpj
	from	com_cliente
	where	ie_situacao = 'A'
	and	ie_classificacao = 'C'
	and	dt_revisao_prevista between dt_inicio_p and fim_dia(dt_fim_p)
	group by	cd_cnpj) i
where	i.qt > 1;

return	qt_retorno_w;

end bsc_solucao_integrada;
/