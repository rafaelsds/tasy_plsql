create or replace
function via_obter_aprov_sub(	cd_pessoa_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
qt_diretor_w	number(10);
qt_gerente_w	number(10);

begin
select	count(*)
into	qt_diretor_w
from	PESSOA_FISICA_DELEGACAO a
where	CD_PESSOA_SUBSTITUTA = cd_pessoa_p
and	IE_OBJETIVO = 'AV'
and DT_LIMITE >= sysdate;

select	count(*)
into	qt_gerente_w
from	depto_gerencia_philips a
where	obter_cd_pessoa_substituta(obter_cd_pessoa_responsavel(a.nr_seq_gerencia)) = cd_pessoa_p;

if	(cd_pessoa_p = '0') then
	ds_retorno_w := cd_pessoa_p;
elsif	(qt_diretor_w > 0) then
	ds_retorno_w := cd_pessoa_p;
elsif	(qt_gerente_w > 0) then
	ds_retorno_w := cd_pessoa_p;
else
	ds_retorno_w := 'X';
end if;
/*
	begin
	select	nvl(max(a.cd_aprov_sub),cd_pessoa_p)
	into	ds_retorno_w
	from	fin_gv_pend_aprov a
	where	a.cd_pessoa_aprov = cd_pessoa_p;
	end;
end if;*/

return	ds_retorno_w;

end via_obter_aprov_sub;
/
