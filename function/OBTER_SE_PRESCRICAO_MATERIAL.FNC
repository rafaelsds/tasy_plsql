create or replace
function obter_se_prescricao_material(	nr_prescricao_p	number,
					cd_material_p 	number)
 		    	return varchar2 is

ds_retorno_w 	varchar2(1);
			
begin

select	nvl(max('S'),'N')
into	ds_retorno_w
from	material b,
		prescr_material a
where	a.cd_material = b.cd_material
and		a.nr_prescricao = nr_prescricao_p
and		((b.cd_material = cd_material_p) or (b.cd_material_estoque = cd_material_p));

return	ds_retorno_w;

end obter_se_prescricao_material;
/