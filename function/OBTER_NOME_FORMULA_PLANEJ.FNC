create or replace
function	obter_nome_formula_planej(
		nr_sequencia_p			number)
		return varchar2 is

ds_retorno_w		varchar2(255);

begin

select	nm_formula
into	ds_retorno_w
from	sup_formula_planej_compra
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end obter_nome_formula_planej;
/