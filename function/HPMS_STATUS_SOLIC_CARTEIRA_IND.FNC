create or replace
function hpms_status_solic_carteira_ind
			(	nr_seq_carteira_p	number)
 		    		return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter o status da solicita��o da carteira
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w				varchar2(1);
ie_sit_carteira_segurado_w		varchar2(1);
ie_situacao_carteira_emissao_w		varchar2(1);
ie_situacao_lote_w			varchar2(1);
nr_seq_lote_w				number(10);
nr_seq_lote_emissao_w			number(10);
dt_entrega_beneficiario_w		date;
dt_envio_lote_w				date;

begin
/* Obter a situa��o da carteira do segurado */
select	nvl(max(ie_situacao), 'D'),
	max(nr_seq_lote_emissao)
into	ie_sit_carteira_segurado_w,
	nr_seq_lote_emissao_w
from	pls_segurado_carteira
where	nr_sequencia	= nr_seq_carteira_p;

/* Obter o �ltimo lote da carteirinha */
select	nvl(max(b.nr_seq_lote),0)
into	nr_seq_lote_w
from	pls_carteira_emissao	b,
	pls_segurado_carteira	a
where	a.nr_sequencia		= b.nr_seq_seg_carteira
and	a.nr_sequencia		= nr_seq_carteira_p;

/* Obter o status e o recebimento da carteira pela grafica */
select	nvl(max(ie_situacao), 'P'),
	max(dt_entrega_beneficiario)
into	ie_situacao_carteira_emissao_w,
	dt_entrega_beneficiario_w
from	pls_carteira_emissao
where	nr_seq_seg_carteira	= nr_seq_carteira_p
and	nr_seq_lote		= nr_seq_lote_w;

select	max(dt_envio),
	nvl(max(ie_situacao), 'D')
into	dt_envio_lote_w,
	ie_situacao_lote_w
from	pls_lote_carteira
where	nr_sequencia	= nr_seq_lote_w;

if	(dt_entrega_beneficiario_w is not null) then
	ds_retorno_w	:= '4';
elsif	(ie_situacao_carteira_emissao_w = 'D') and
	(dt_entrega_beneficiario_w is null) then
	ds_retorno_w	:= '3';
elsif	(ie_situacao_carteira_emissao_w = 'P') then
	ds_retorno_w	:= '2';
elsif	(ie_sit_carteira_segurado_w = 'P') then
	ds_retorno_w	:= '1';
end if;

return ds_retorno_w;

end hpms_status_solic_carteira_ind;
/
