create or replace
function Obter_ds_aplicacao(nr_sequencia_p number)
 		    	return varchar2 is
ds_aplicacao_w varchar2(255);
begin

select 	substr(ds_aplicacao,1,255)
into 	ds_aplicacao_w
from	aplicacao_software
where 	nr_sequencia = nr_sequencia_p;

return	ds_aplicacao_w;

end Obter_ds_aplicacao;
/