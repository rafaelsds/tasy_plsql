create or replace
function lic_obter_se_gera_lance(	nr_sequencia_p		number)
return varchar2 is

			
nr_seq_mod_compra_w		number(10);			
ie_gera_lance_w			varchar2(1);

begin

select	nvl(nr_seq_mod_compra,0)
into	nr_seq_mod_compra_w
from	reg_licitacao
where	nr_sequencia = nr_sequencia_p;

if	(nr_seq_mod_compra_w <> 0) then
	select	nvl(ie_gera_lances,'S')
	into	ie_gera_lance_w
	from	reg_lic_mod_compra
	where	nr_sequencia = nr_seq_mod_compra_w;
end if;

return	ie_gera_lance_w;

end lic_obter_se_gera_lance;
/