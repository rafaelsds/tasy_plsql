create or replace
function obter_tipo_elem_mat(
						nr_seq_nut_pac_p		number,
						nr_seq_nut_pac_prod_p	number) return varchar2 is

ds_retorno_w	varchar2(10);
begin

select	max(d.ie_tipo_elemento)
into	ds_retorno_w
from	nut_pac_elem_mat a,
		nut_elem_material b,
		nut_pac_elemento c,
		nut_elemento d
where	a.cd_material 		= b.cd_material
and		a.nr_seq_nut_pac 	= c.nr_seq_nut_pac
and		b.nr_seq_elemento 	= c.nr_seq_elemento
and		b.nr_seq_elemento 	= d.nr_sequencia
and		c.nr_seq_elemento 	= d.nr_sequencia
and		a.nr_seq_nut_pac 	= nr_seq_nut_pac_p
and		a.nr_sequencia 		= nr_seq_nut_pac_prod_p;

return ds_retorno_w;

end obter_tipo_elem_mat;
/