create or replace
function Obter_se_atend_dt_nutricao(nr_atendimento_p	number,
				dt_prescricao_p		date)
 		    	return varchar2 is
ie_retorno_w	varchar2(1) := 'N';
begin

if	(Nr_atendimento_p is not null) then
	Select	nvl(max('S'), 'N')
	into	ie_retorno_w
	from	prescr_dieta a,
		Prescr_medica b
	where	a.nr_prescricao = b.nr_prescricao
	and	b.nr_atendimento = nr_atendimento_p
	and 	trunc(b.dt_prescricao) = trunc(dt_prescricao_p);
end if;

return	ie_retorno_w;

end Obter_se_atend_dt_nutricao;
/