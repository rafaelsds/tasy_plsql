create or replace function obter_se_proc_terap_resp_pend(nr_atendimento_p	varchar2)
	return varchar2 is

	ie_proc_terap_resp_pend_w		varchar2(1 char);

begin

	select	nvl(max('S'),'N')
	into	ie_proc_terap_resp_pend_w
	from	prescr_medica a,
			prescr_procedimento x,
			procedimento y,
			proc_interno c
	where	a.nr_prescricao = x.nr_prescricao
	and		x.cd_procedimento = y.cd_procedimento
	and		x.ie_origem_proced = y.ie_origem_proced
	and		x.cd_procedimento = c.cd_procedimento
	and		x.ie_origem_proced = c.ie_origem_proced
	and		a.nr_atendimento = nr_atendimento_p
	and		a.dt_inicio_prescr > sysdate - 7
	and		c.ie_tipo in ('PTR')
	and		obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,establishment_timezone_utils.startOfDay(sysdate-5),establishment_timezone_utils.startOfDay(sysdate+1)) = 'S'
	and		x.dt_suspensao is null
	and		a.dt_suspensao is null
	and		x.nr_seq_exame is null
	and		x.nr_seq_solic_sangue is null
	and		x.nr_seq_derivado is null
	and		x.nr_Seq_origem is null
	and		x.nr_seq_exame_sangue is null
	and		exists	(	select	1
				from	prescr_proc_hor	y
				where	y.nr_prescricao	= x.nr_prescricao
				and		x.nr_sequencia	= y.nr_seq_procedimento
				and		nvl(y.ie_situacao,'A') = 'A'
				and		Obter_se_horario_liberado(y.dt_lib_horario, y.dt_horario) = 'S'
				and		y.dt_suspensao is null
				and		y.dt_fim_horario is null)
	and		rownum = 1;

	return	ie_proc_terap_resp_pend_w;

end obter_se_proc_terap_resp_pend;
/
