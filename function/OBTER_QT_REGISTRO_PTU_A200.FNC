create or replace
function obter_qt_registro_ptu_a200
			(	nr_sequencia_p		number,
				ie_opcao_p		varchar2)
				return number is

qt_total_r102_w		number(7);
qt_alterados_w		number(7);
qt_plano_w		number(7);
qt_recusados_w		number(7);
qt_inclusao_w		number(7);
qt_exclusao_w		number(7);
qt_total_w		number(7);

begin

select	nvl(count(*),0)
into	qt_total_r102_w
from	ptu_retorno_mov_benef
where	nr_seq_retorno_mov	= nr_sequencia_p;

select	nvl(count(*),0)
into	qt_inclusao_w
from	ptu_retorno_mov_benef
where	nr_seq_retorno_mov	= nr_sequencia_p
and	cd_inconsistencia	= 3201;

select	nvl(count(*),0)
into	qt_alterados_w
from	ptu_retorno_mov_benef
where	nr_seq_retorno_mov	= nr_sequencia_p
and	cd_inconsistencia	= 3202;

select	nvl(count(*),0)
into	qt_exclusao_w
from	ptu_retorno_mov_benef
where	nr_seq_retorno_mov	= nr_sequencia_p
and	cd_inconsistencia	= 3203;

select	nvl(count(*),0)
into	qt_plano_w
from	ptu_retorno_mov_benef
where	nr_seq_retorno_mov	= nr_sequencia_p
and	cd_inconsistencia	= 3204;

qt_recusados_w	:= qt_total_r102_w - qt_inclusao_w - qt_alterados_w - qt_exclusao_w - qt_plano_w;

if	(ie_opcao_p	= '102') then
	qt_total_w	:= qt_total_r102_w;
elsif	(ie_opcao_p	= 'QT_ALTERADOS') then
	qt_total_w	:= qt_alterados_w;
elsif	(ie_opcao_p	= 'QT_PLANO') then
	qt_total_w	:= qt_plano_w;
elsif	(ie_opcao_p	= 'QT_RECUSADOS') then
	qt_total_w	:= qt_recusados_w;
elsif	(ie_opcao_p	= 'QT_INCLUSAO') then
	qt_total_w	:= qt_inclusao_w;
elsif	(ie_opcao_p	= 'QT_EXCLUSAO') then
	qt_total_w	:= qt_exclusao_w;
end if;

return	qt_total_w;

end obter_qt_registro_ptu_a200;
/