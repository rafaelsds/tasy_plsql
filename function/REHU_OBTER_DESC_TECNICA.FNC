create or replace
function rehu_obter_desc_tecnica ( nr_seq_tecnica_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);

begin

select	ds_tecnica
into	ds_retorno_w
from	rehu_tecnica_utilizada
where	nr_sequencia = nr_seq_tecnica_p;

return	ds_retorno_w;

end rehu_obter_desc_tecnica;
/