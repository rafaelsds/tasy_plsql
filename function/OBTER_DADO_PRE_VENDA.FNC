create or replace
function obter_dado_pre_venda(
		nr_pre_venda_p	number,
		ie_tipo_dado_p	varchar2)
 		return varchar2 is
ds_retorno_w		varchar2(4000);
begin
if 	(nr_pre_venda_p is not null) and
	(ie_tipo_dado_p is not null) then
	begin
	if (ie_tipo_dado_p = 'NOV') then
		begin
		select	nr_ordem_venda
		into	ds_retorno_w
		from	pre_venda
		where	nr_sequencia = nr_pre_venda_p;
		end;
	elsif (ie_tipo_dado_p = 'NPJ') then
		begin
		select obter_nome_pf_pj(null, cd_cnpj_empresa)
		into ds_retorno_w
		from pre_venda
		where nr_sequencia = nr_pre_venda_p;
		end;
	elsif (ie_tipo_dado_p = 'AGR') then
		begin
		select ie_agrupada
		into ds_retorno_w
		from pre_venda
		where nr_sequencia = nr_pre_venda_p;
		end;
	end if;	
	end;
end if;

return	ds_retorno_w;
end obter_dado_pre_venda;
/