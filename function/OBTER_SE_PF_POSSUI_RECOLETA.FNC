create or replace
function obter_se_pf_possui_recoleta (	cd_pessoa_p	Varchar2)
 		    	return Varchar2 is

ie_possui_recoleta_w	varchar2(1);			
begin

begin

select 	nvl(decode(a.nr_seq_doacao_amostra,null,'N','S'),'N')
into	ie_possui_recoleta_w
from	san_doacao a
where	a.nr_sequencia = (	select 	max(b.nr_sequencia)
				from	san_doacao b
				where	b.cd_pessoa_fisica = cd_pessoa_p);
exception
	when others then
		ie_possui_recoleta_w	:= 'N';
end;

return	ie_possui_recoleta_w;

end obter_se_pf_possui_recoleta;
/