create or replace
function pls_obter_dados_declarac_saude(	nr_seq_declaracao_segurado_p		pls_decl_segurado_web.nr_seq_declaracao_segurado%type,
					ie_opcao_p				Varchar2)
					return Varchar2 is

/*	ie_opcao_p
	DT - Data envio
	DP - Data preenchimento
	UE - Usuario envio	
*/		

dt_envio_w		pls_decl_segurado_web.dt_geracao_hash%type;			
dt_preenchimento_w	pls_decl_segurado_web.dt_utilizacao_hash%type;
nm_usuario_w		pls_decl_segurado_web.nm_usuario%type;
ds_retorno_w		varchar2(255);

begin

select	max(x.dt_geracao_hash),
	max(x.dt_utilizacao_hash),
	max(x.nm_usuario)
into	dt_envio_w,
	dt_preenchimento_w,
	nm_usuario_w
from	pls_decl_segurado_web x	
where	x.nr_sequencia = (	select	max(a.nr_sequencia)
				from	pls_decl_segurado_web a,
					pls_declaracao_segurado b
				where 	a.nr_seq_declaracao_segurado = b.nr_sequencia
				and	b.nr_sequencia = nr_seq_declaracao_segurado_p);

if	(ie_opcao_p	= 'DT') then
	ds_retorno_w	:= to_char(dt_envio_w,'dd/mm/yyyy hh24:mi');
elsif	(ie_opcao_p	= 'DP') then
	ds_retorno_w	:= to_char(dt_preenchimento_w,'dd/mm/yyyy hh24:mi');
elsif	(ie_opcao_p	= 'UE') then
	ds_retorno_w	:= nm_usuario_w;	
end if;	
	
return	ds_retorno_w ;

end pls_obter_dados_declarac_saude;
/