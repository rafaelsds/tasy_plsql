create or replace
function obter_qt_evolucoes_nao_lib (
			nm_usuario_p varchar2) 
			return number is

qt_evo_nao_lib_w number(10,0) := 0;
			
begin

if	(nm_usuario_p is not null) then
	begin
	select	count(*) 
	into	qt_evo_nao_lib_w
	from	evolucao_paciente
	where	nm_usuario = nm_usuario_p
	and	dt_evolucao > sysdate - 30
	and	nr_cirurgia is not null
	and	ie_evolucao_clinica not in ('AE','CO')
	and	dt_liberacao is null;
	end;
end if;

return qt_evo_nao_lib_w;

end obter_qt_evolucoes_nao_lib;
/