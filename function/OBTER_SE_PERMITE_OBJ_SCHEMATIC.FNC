create or replace
function OBTER_SE_PERMITE_OBJ_SCHEMATIC(ie_tipo_obj_atual_p		varchar2,
					ie_tipo_obj_novo_p		varchar2,
					nr_seq_tipo_sch_atual_p		number default null,
					nr_seq_tipo_sch_novo_p		number default null,
					nr_seq_bo_p			number default null)
					return varchar2 is
					
ie_permite_w		varchar2(10) := 'N';
nr_seq_regra_w		number(10);
qt_regra_w		number(10);
nr_seq_regra_obj_w	number(10);
nr_seq_tipo_sch_atual_w	number(10);
nr_seq_tipo_sch_novo_w	number(10);
					
begin

if	(nr_seq_tipo_sch_novo_p = '0') then
	nr_seq_tipo_sch_novo_w	:= null;
else
	nr_seq_tipo_sch_novo_w	:= to_number(nr_seq_tipo_sch_novo_p);
end if;

select	max(nr_sequencia)
into	nr_seq_regra_w
from	REGRA_SCHEMATIC
where	ie_situacao 	= 'A';
--and	dt_liberacao 	is not null;

if	(nr_seq_regra_w > 0) then

	select	max(nr_sequencia)
	into	nr_seq_regra_obj_w
	from	regra_schematic_obj a
	where	a.nr_seq_regra		= nr_seq_regra_w
	and	a.ie_tipo_objeto	= ie_tipo_obj_atual_p
	and	a.nr_seq_tipo_schematic	= nr_seq_tipo_sch_atual_p;
	
	if	(nvl(nr_seq_regra_obj_w,0) > 0) and
		(nvl(nr_seq_tipo_sch_novo_w,0) > 0) then
		select	count(*)
		into	qt_regra_w
		from	regra_schematic_obj_filho
		where	nr_seq_regra_obj	= nr_seq_regra_obj_w
		and	ie_tipo_objeto		= ie_tipo_obj_novo_p
		and	nvl(nr_seq_tipo_schematic,nr_seq_tipo_sch_novo_w)	= nr_seq_tipo_sch_novo_w;
		
	else
	
		select	count(*)
		into	qt_regra_w
		from	regra_schematic_obj a
		where	a.nr_seq_regra		= nr_seq_regra_w
		and	a.ie_tipo_objeto	= ie_tipo_obj_atual_p
		and	(a.nr_seq_tipo_schematic is null or a.nr_seq_tipo_schematic = nr_seq_tipo_sch_atual_p)
		and	exists	(select	1
				from	regra_schematic_obj_filho b
				where	b.nr_seq_regra_obj	= a.nr_sequencia
				and	b.ie_tipo_objeto	= ie_tipo_obj_novo_p
				and	nvl(b.nr_seq_tipo_schematic,nvl(nr_seq_tipo_sch_novo_w,0)) = nvl(nr_seq_tipo_sch_novo_w,nvl(b.nr_seq_tipo_schematic,0)));	
	end if;
	
	if	(qt_regra_w > 0) then
		ie_permite_w	:= 'S';
	end if;

end if;

--Habilitar o navegador de breadcrumb de DBPanel apenas para o scehmatic 6/6 e 12
if	(nr_seq_tipo_sch_atual_p > 0) and
	(nr_seq_tipo_sch_atual_p not in (22,23)) and
	(ie_tipo_obj_atual_p = 'SCH') and
	(ie_tipo_obj_novo_p = 'MN') then
	ie_permite_w	:= 'N';
end if;

if	(ie_tipo_obj_atual_p = 'WDLG') and
	(ie_tipo_obj_novo_p = 'C') and 
	(nr_seq_tipo_sch_novo_w is null or nr_seq_tipo_sch_novo_w = 90)then
	ie_permite_w	:= 'S';
end if;

if	(ie_tipo_obj_atual_p = 'FBO') and
	(ie_tipo_obj_novo_p = 'C') and 
	(nvl(nr_seq_tipo_sch_atual_p,0) = 0)then
	ie_permite_w	:= 'S';
end if;

if	(ie_tipo_obj_atual_p = 'WSCB') and
	(ie_tipo_obj_novo_p = 'C') and 
	(nr_seq_tipo_sch_novo_w is null or nr_seq_tipo_sch_novo_w = 98)then
	ie_permite_w	:= 'S';
end if;

if	(ie_tipo_obj_atual_p = 'WAE') and
	(ie_tipo_obj_novo_p = 'C') and 
	(nr_seq_tipo_sch_novo_w is null or nr_seq_tipo_sch_novo_w = 112)then
	ie_permite_w	:= 'S';
end if;

if	(ie_tipo_obj_atual_p = 'FSCH') and
	(ie_tipo_obj_novo_p = 'FSCH') then
	ie_permite_w	:= 'S';
end if;

--Impedir que sejam adicionados pain�is dentro de BO
if	(nvl(nr_seq_bo_p,0) > 0) and
	(ie_tipo_obj_novo_p = 'P') then
	ie_permite_w := 'N';
end if;

return ie_permite_w;

end OBTER_SE_PERMITE_OBJ_SCHEMATIC;
/