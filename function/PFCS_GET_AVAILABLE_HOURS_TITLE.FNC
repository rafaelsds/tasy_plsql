create or replace
function pfcs_get_available_hours_title
			(	nr_sequencia_p		number,
        cd_expressao_p		number)
				return varchar2 is
   
		 			 
ds_retorno_w			varchar2(4000)	:= null;
cd_idioma_w             number          := null;
expressao_idioma_w      varchar2(4000)	:= null;
full_expressao_idioma_w varchar2(4000)	:= null;
ds_informacao_w         varchar2(4000)	:= null;

begin
begin
select philips_param_pck.get_nr_seq_idioma into cd_idioma_w from dual;
end;

begin
select obter_desc_expressao_idioma(cd_expressao_p, null, cd_idioma_w) into expressao_idioma_w from dual;
end;

begin
select obter_desc_expressao_idioma((select cd_exp_informacao from dic_objeto where nr_sequencia = nr_sequencia_p), null, cd_idioma_w) into full_expressao_idioma_w from dual;
end;

begin
select
replace(full_expressao_idioma_w, trim(lower(expressao_idioma_w)), ' ') || qt_time || ' ' ||lower(expressao_idioma_w) 
into ds_retorno_w
from pfcs_time_available_hours;
return ds_retorno_w;
end;

end pfcs_get_available_hours_title;
/