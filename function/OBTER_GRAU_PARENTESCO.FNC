create or replace
function obter_grau_parentesco(
	cd_pessoa_fisica_p	varchar2,
	cd_pessoa_refer_p	varchar2)
 	return number is

ds_retorno_w	number(10);

begin

if (cd_pessoa_fisica_p is not null) then
	select	max(a.nr_seq_grau_parentesco)
	into	ds_retorno_w
	from	pf_familia a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.cd_pessoa_familia = cd_pessoa_refer_p;
end if;

return	ds_retorno_w;

end obter_grau_parentesco;
/