create or replace
function obter_desc_peca_principal( nr_prescricao_p number,
				    nr_seq_prescr_p number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);

begin
	select nvl(max(a.ds_amostra),'')
	into   ds_retorno_w
	from   tipo_amostra_patologia a,
	       prescr_proc_peca b	   
	where  a.nr_sequencia = b.nr_seq_amostra_princ
	and    b.nr_prescricao = nr_prescricao_p
	and    b.nr_seq_prescr  = nr_seq_prescr_p
	and    b.ie_peca_principal = 'S';

return	ds_retorno_w;

end obter_desc_peca_principal;
/