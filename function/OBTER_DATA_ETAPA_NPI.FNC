create or replace
function obter_data_etapa_npi(	nr_seq_etapa		number)
				return number is

qt_retorno_w		number(10);

begin
select	nvl(max(a.qt_dias),0)
into	qt_retorno_w
from	prp_regra_tempo_etapa a
where	a.nr_seq_etapa_processo = nr_seq_etapa;

return	qt_retorno_w;

end obter_data_etapa_npi;
/