create or replace function obter_qt_pend_paciente(nr_seq_reserva_p	number) return number is

qt_pendencia_pac_w	number(3);

begin
if (nr_seq_reserva_p is not null) then
	select 	count(1)
	into 	qt_pendencia_pac_w
	from 	san_reserva_item
	where 	nr_seq_reserva = nr_seq_reserva_p
	and		ie_pend_paciente = 'S';
end if;

return qt_pendencia_pac_w;

end obter_qt_pend_paciente;
/


