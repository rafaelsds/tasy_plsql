create or replace
function pls_obter_auditor_grupo
				(	nr_seq_grupo_membro_p	Number,
					ie_tipo_p		varchar2) 
				return varchar2 is

/*	ie_tipo_p
	A - Para Retornar nome do auditor
	G - para retornar nome do grupo
e
*/

nr_seq_grupo_w		number(10);
ds_retorno_w		varchar2(255);


begin

ds_retorno_w  	:= '';

if	(nr_seq_grupo_membro_p is not null) then

	select	nr_seq_grupo,
		nm_usuario_exec
	into 	nr_seq_grupo_w,
		ds_retorno_w
	from	pls_membro_grupo_aud
	where	nr_sequencia = nr_seq_grupo_membro_p;

	if	(ie_tipo_p = 'G') then
		select 	nm_grupo_auditor
		into	ds_retorno_w
		from	pls_grupo_auditor
		where	nr_sequencia = nr_seq_grupo_w;
	end if;
end if;

return ds_retorno_w;

end pls_obter_auditor_grupo;
/
