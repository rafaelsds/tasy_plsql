create or replace
function obter_ds_evolucao_paciente(	cd_evolucao_p	number)
 		    	return varchar2 is

nr_seq_tasy_conversao_rtf_w	tasy_conversao_rtf.nr_sequencia%type;
ds_retorno_w			varchar2(2000);

pragma autonomous_transaction;
begin

converte_rtf_string('select ds_evolucao from evolucao_paciente where cd_evolucao = :cd_evolucao_p', to_char(cd_evolucao_P), 'Tasy', nr_seq_tasy_conversao_rtf_w);

select	dbms_lob.substr( ds_texto_clob, 2000, 1 )
into	ds_retorno_w
from	tasy_conversao_rtf
where	nr_sequencia = nr_seq_tasy_conversao_rtf_w;

return	ds_retorno_w;

end obter_ds_evolucao_paciente;
/
