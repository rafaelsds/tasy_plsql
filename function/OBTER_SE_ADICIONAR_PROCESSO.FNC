create or replace
function obter_se_adicionar_processo (
				nr_atendimento_p	number,
				dt_horario_p		date,
				cd_local_estoque_p	number)
				return varchar2 is

ie_adicionar_w	varchar2(1) := 'N';

begin
if	(nr_atendimento_p is not null) and
	(dt_horario_p is not null) then

	select	decode(count(*), 0, 'N', 'S')
	into	ie_adicionar_w
	from	adep_pend_v
	where	nr_atendimento		= nr_atendimento_p
	and	dt_horario			= dt_horario_p
	and	ie_tipo_item		in ('M', 'MAP')
	and	nvl(cd_local_estoque,0)	= nvl(cd_local_estoque_p,0)
	and	nr_seq_processo		is null;	

end if;

return ie_adicionar_w;

end obter_se_adicionar_processo;
/