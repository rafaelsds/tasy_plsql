create or replace
function Fa_obter_data_retorno_item(	nr_seq_medic_item_p	Number)
					return Date is

qt_gerado_w		Number(5);
qt_dispensar_w		Number(5);
nr_dias_util_w		Number(4);
dt_periodo_inicial_w	Date;
dt_prevista_retorno_w	Date;
dt_retorno_w		Date;
					
begin

if (nr_seq_medic_item_p is not null) then

	select	a.qt_gerado,
		a.qt_dispensar,
		b.nr_dias_util,
		b.dt_periodo_inicial,
		b.dt_prevista_retorno
	into	qt_gerado_w,
		qt_dispensar_w,
		nr_dias_util_w,
		dt_periodo_inicial_w,
		dt_prevista_retorno_w
	from	fa_entrega_medicacao_item a,
		fa_entrega_medicacao b
	where	a.nr_seq_fa_entrega = b.nr_sequencia
	and	a.nr_sequencia = nr_seq_medic_item_p;

	if (qt_gerado_w > 0) and (qt_dispensar_w > 0) then
		dt_retorno_w := trunc(dt_periodo_inicial_w + (qt_dispensar_w/(qt_gerado_w/nr_dias_util_w)) - 1);
	else
		dt_retorno_w := trunc(dt_prevista_retorno_w);
	end if;
	
end if;

return	dt_retorno_w;

end Fa_obter_data_retorno_item;
/