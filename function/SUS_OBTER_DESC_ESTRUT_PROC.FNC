CREATE OR REPLACE
FUNCTION Sus_Obter_Desc_Estrut_Proc
                        (       nr_sequencia_p       Number,
				ie_opcao_p		varchar2,
                                ie_estrutura_p          varchar2)
                                return varchar2 is


/* IE_OPCAO_P
        C  - C�digo
	D  - Descri��o
	CD - C�digo + Descri��o
*/

/* IE_ESTRUTURA_P
        F - Forma organizacao
        S - SubGrupo
        G - Grupo       
*/


cd_forma_organizacao_w          Number(6);
ds_forma_organizacao_w          varchar2(100);
cd_subgrupo_w                   Number(4);
ds_subgrupo_w                   varchar2(100);
cd_grupo_w                      Number(2);
ds_grupo_w                      Varchar2(100);


BEGIN

/* ESSA FUNCTION � UTILIZADA NA TABELA SUS_PROCEDIMENTO NO DICIONARIO DE DADOS */
if      (ie_estrutura_p = 'F') then
        select	max(ds_forma_organizacao),
		max(cd_forma_organizacao)
	into	ds_forma_organizacao_w,
		cd_forma_organizacao_w
	from	sus_forma_organizacao
	where	nr_sequencia	= nr_sequencia_p;
elsif   (ie_estrutura_p = 'S') then
        select	max(ds_subgrupo),
		max(cd_subgrupo)
	into	ds_subgrupo_w,
		cd_subgrupo_w
	from	sus_subgrupo
	where	nr_sequencia	= nr_sequencia_p;
elsif   (ie_estrutura_p = 'G') then
	select	max(ds_grupo),
		max(cd_grupo)
	into	ds_grupo_w,
		cd_grupo_w
	from	sus_grupo
	where	nr_sequencia	= nr_sequencia_p;
end if;

if	(ie_opcao_p = 'D') then
	if      (ie_estrutura_p = 'F') then
		return ds_forma_organizacao_w;
	elsif   (ie_estrutura_p = 'S') then
		return ds_subgrupo_w;
	elsif   (ie_estrutura_p = 'G') then
		return ds_grupo_w;
	end if;
elsif	(ie_opcao_p = 'C') then
	if      (ie_estrutura_p = 'F') then
		return cd_forma_organizacao_w;
	elsif   (ie_estrutura_p = 'S') then
		return cd_subgrupo_w;
	elsif   (ie_estrutura_p = 'G') then
		return cd_grupo_w;
	end if;
elsif	(ie_opcao_p = 'CD') then
	if      (ie_estrutura_p = 'F') then
		return cd_forma_organizacao_w || ' - ' || ds_forma_organizacao_w;
	elsif   (ie_estrutura_p = 'S') then
		return cd_subgrupo_w || ' - ' || ds_subgrupo_w;
	elsif   (ie_estrutura_p = 'G') then
		return cd_grupo_w || ' - ' || ds_grupo_w;
	end if;
end if;

END Sus_Obter_Desc_Estrut_Proc;
/
