create or replace function Obter_pontuacao_stroke( qt_pontuacao_p	number)
 		    	return varchar as

ds_resultado_w		varchar2(10);
			
begin
IF 	(qt_pontuacao_p BETWEEN -10 AND  2) THEN
	ds_resultado_w := 'AVCh';
ELSIF	(qt_pontuacao_p BETWEEN 2 AND 10) THEN
	ds_resultado_w := 'AVCi';
END IF;

return	ds_resultado_w;

end Obter_pontuacao_stroke;
/