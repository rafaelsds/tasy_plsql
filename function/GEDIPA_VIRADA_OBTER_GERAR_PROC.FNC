create or replace
function gedipa_virada_obter_gerar_proc (
					dt_virada_p		date,
					ie_processo_atual_p	varchar2,					
					nr_seq_ap_lote_p		number)
					return varchar2 is
					
ie_gerar_processo_w	varchar2(1) := 'N';
ie_gedipa_w		varchar2(1);
					
begin
if	(dt_virada_p is not null) then

	if	((trunc(sysdate,'dd') - trunc(dt_virada_p,'dd')) < 2) then

		if	(ie_processo_atual_p = 'AP_LOTE') then

			if	(nr_seq_ap_lote_p is not null) then
			
				select	decode(count(*),0,'N','S')
				into	ie_gerar_processo_w
				from	ap_lote
				where	nr_sequencia	= nr_seq_ap_lote_p
				and	((dt_impressao	is null) or
					 (dt_impressao	> dt_virada_p));
			else
				ie_gerar_processo_w	:= 'S';
			end if;
		else
			ie_gerar_processo_w	:= 'S';
		end if;
		
	else
		ie_gerar_processo_w	:= 'S';
	end if;
	
	select	nvl(max(ie_gedipa),'S')
	into	ie_gedipa_w
	from	prescr_mat_hor
	where	nr_seq_lote	=	nr_seq_ap_lote_p
	and	dt_lib_horario is not null;
	
	if	(ie_gerar_processo_w = 'S') and (ie_gedipa_w = 'N') then
		ie_gerar_processo_w := 'N';
	end if; 

end if;

return ie_gerar_processo_w;

end gedipa_virada_obter_gerar_proc;
/