create or replace
function rxt_obter_nome_campo_prot	(nr_seq_campo_p	number)
				return varchar2 is

ds_campo_w	varchar2(80);

begin
if	(nr_seq_campo_p is not null) then

	select	max(ds_campo)
	into	ds_campo_w
	from	rxt_campo_protocolo
	where	nr_sequencia = nr_seq_campo_p;

end if;

return ds_campo_w;

end rxt_obter_nome_campo_prot;
/