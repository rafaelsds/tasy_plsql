create or replace
function obter_status_hor_prescr	(nr_seq_horario_p	number,
				ie_tipo_item_p	varchar2)
				return varchar2 is

dt_adm_w		date;
dt_susp_w		date;
ie_dose_esp_w		varchar2(1);
ie_status_w		varchar2(1);
ie_status_processo_w	varchar2(15);

begin
if	(nr_seq_horario_p is not null) and
	(ie_tipo_item_p is not null) then
	if	(ie_tipo_item_p in ('M','S','MAT')) then
		select	max(dt_fim_horario),
			max(dt_suspensao),
			nvl(max(ie_dose_especial),'N'),
			--nvl(max(obter_status_processo_adep(nr_seq_processo)),'X')
			nvl(max(obter_status_processo_area(nr_seq_processo,nr_seq_area_prep)),'X') /* OS 125881 */
		into	dt_adm_w,
			dt_susp_w,
			ie_dose_esp_w,
			ie_status_processo_w
		from	prescr_mat_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

		if	(dt_adm_w is null) and
			(dt_susp_w is null) then
			ie_status_w := 'N';
		
			if	(ie_status_processo_w = 'R') then
				ie_status_w := 'R';
			elsif	(ie_status_processo_w = 'D') then
				ie_status_w := 'D';
			elsif	(ie_status_processo_w = 'H') then
				ie_status_w := 'H';
			elsif	((ie_status_processo_w = 'P') or (ie_status_processo_w = 'L')) then
				ie_status_w := 'P';
			elsif	(ie_dose_esp_w = 'S') then
				ie_status_w := 'E';
			end if;
			
		elsif	(dt_adm_w is not null) and
			(dt_susp_w is null) then
			ie_status_w := 'A';
		elsif	(dt_adm_w is null) and
			(dt_susp_w is not null) then
			ie_status_w := 'S';
		end if;
	elsif	(ie_tipo_item_p = 'P') then
		select	max(dt_fim_horario),
			max(dt_suspensao),
			nvl(max(ie_dose_especial),'N')
		into	dt_adm_w,
			dt_susp_w,
			ie_dose_esp_w
		from	prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

		if	(dt_adm_w is null) and
			(dt_susp_w is null) then
			ie_status_w := 'N';
			if	(ie_dose_esp_w = 'S') then
				ie_status_w := 'E';
			end if;
		elsif	(dt_adm_w is not null) and
			(dt_susp_w is null) then
			ie_status_w := 'A';
		elsif	(dt_adm_w is null) and
			(dt_susp_w is not null) then
			ie_status_w := 'S';
		end if;
	elsif	(ie_tipo_item_p = 'R') then
		select	max(dt_fim_horario),
			max(dt_suspensao)
		into	dt_adm_w,
			dt_susp_w
		from	prescr_rec_hor
		where	nr_sequencia	= nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';	
		if	(dt_adm_w is null) and
			(dt_susp_w is null) then
			ie_status_w := 'N';
		elsif	(dt_adm_w is not null) and
			(dt_susp_w is null) then
			ie_status_w := 'A';
		elsif	(dt_adm_w is null) and
			(dt_susp_w is not null) then
			ie_status_w := 'S';
		end if;
	elsif	(ie_tipo_item_p = 'E') then
		select	max(dt_fim_horario),
			max(dt_suspensao)
		into	dt_adm_w,
			dt_susp_w
		from	pe_prescr_proc_hor
		where	nr_sequencia	= nr_seq_horario_p;	
		if	(dt_adm_w is null) and
			(dt_susp_w is null) then
			ie_status_w := 'N';
		elsif	(dt_adm_w is not null) and
			(dt_susp_w is null) then
			ie_status_w := 'A';
		elsif	(dt_adm_w is null) and
			(dt_susp_w is not null) then
			ie_status_w := 'S';
		end if;
	elsif	(ie_tipo_item_p = 'B') then
		select	max(dt_fim_horario),
			max(dt_suspensao)
		into	dt_adm_w,
			dt_susp_w
		from	prescr_ordem_hor
		where	nr_sequencia = nr_seq_horario_p
		and		ie_horario_especial = 'N';

		if	(dt_adm_w is null) and
			(dt_susp_w is null) then
			ie_status_w := 'N';
		elsif	(dt_adm_w is not null) and
			(dt_susp_w is null) then
			ie_status_w := 'A';
		elsif	(dt_adm_w is null) and
			(dt_susp_w is not null) then
			ie_status_w := 'S';
		end if;
	elsif	(ie_tipo_item_p = 'D') then
		select	max(dt_fim_horario),
			max(dt_suspensao)
		into	dt_adm_w,
			dt_susp_w
		from	prescr_dieta_hor
		where	nr_sequencia	= nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';	
		if	(dt_adm_w is null) and
			(dt_susp_w is null) then
			ie_status_w := 'N';
		elsif	(dt_adm_w is not null) and
			(dt_susp_w is null) then
			ie_status_w := 'A';
		elsif	(dt_adm_w is null) and
			(dt_susp_w is not null) then
			ie_status_w := 'S';
		end if;
	end if;		
end if;

return ie_status_w;

end obter_status_hor_prescr;
/