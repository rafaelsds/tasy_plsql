create or replace
FUNCTION man_obter_tipo_hist_classif(	nr_seq_ordem_p		NUMBER,
				ie_opcao_p		VARCHAR2)
 		    		RETURN VARCHAR2 IS

nr_seq_tipo_w		NUMBER(10);
ds_retorno_w		VARCHAR2(80);

BEGIN
IF	(nr_seq_ordem_p > 0) THEN
	BEGIN
	SELECT	b.nr_seq_tipo_hist
	INTO	nr_seq_tipo_w
	FROM	man_classificacao b,
		man_ordem_servico a
	WHERE	a.nr_seq_classif = b.nr_sequencia
	AND	a.nr_sequencia = nr_seq_ordem_p;

	IF	(ie_opcao_p = 'C') THEN
		ds_retorno_w := nr_seq_tipo_w;
	ELSIF	(ie_opcao_p = 'D') THEN
		SELECT	ds_tipo
		INTO	ds_retorno_w
		FROM	man_tipo_hist
		WHERE	nr_sequencia = nr_seq_tipo_w;
	END IF;
	END;
END IF;
RETURN	ds_retorno_w;

END man_obter_tipo_hist_classif;
/