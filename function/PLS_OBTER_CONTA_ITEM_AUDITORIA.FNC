create or replace
function pls_obter_conta_item_auditoria
			(	nr_seq_conta_p		Number	)
				return Varchar2 is

ie_retorno_w			Varchar2(1)	:= 'N';
qt_registro_1_w			number(10);
qt_registro_2_w			number(10);
qt_registro_w			number(10);
ie_origem_conta_w		Varchar2(3);

qt_registro_pre_w		Number(10);

begin

begin
select	ie_origem_conta
into	ie_origem_conta_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_p;
exception
when others then
	ie_origem_conta_w	:= '';
end;

ie_origem_conta_w	:= nvl(ie_origem_conta_w,'0');

select	count(1)
into	qt_registro_1_w
from	pls_ocorrencia		b,
	pls_ocorrencia_benef	a
where	a.nr_seq_conta		= nr_seq_conta_p
and	a.nr_seq_ocorrencia	= b.nr_sequencia
and	b.ie_auditoria_conta	= 'S';

select	count(1)
into	qt_registro_pre_w
from	pls_ocorrencia		b,
	pls_ocorrencia_benef	a
where	a.nr_seq_conta		= nr_seq_conta_p
and	a.nr_seq_ocorrencia	= b.nr_sequencia
and	b.ie_auditoria_conta	= 'S'
and	(b.ie_pre_analise = 'S');

select	count(1)
into	qt_registro_2_w
from	pls_ocorrencia		b,
	pls_ocorrencia_benef	a
where	a.nr_seq_conta		= nr_seq_conta_p
and	a.nr_seq_ocorrencia	= b.nr_sequencia
and	exists (select	1	/*Diego OS 331561*/
		from	pls_ocorrencia_grupo x
		where	x.nr_seq_ocorrencia				= b.nr_sequencia
		and	(x.ie_conta_medica = 'S' or x.ie_conta_medica is null)
		and	(x.ie_origem_conta = ie_origem_conta_w or x.ie_origem_conta is null)
		);

qt_registro_w	:= nvl(qt_registro_1_w,0) + nvl(qt_registro_2_w,0);

if	(qt_registro_w	> 0) then
	ie_retorno_w	:= 'S';
end if;

if	(nvl(qt_registro_pre_w,0) = qt_registro_w) and
	(ie_origem_conta_w <> 'A')then
	ie_retorno_w	:= 'N';
end if;

return	ie_retorno_w;

end pls_obter_conta_item_auditoria;
/
