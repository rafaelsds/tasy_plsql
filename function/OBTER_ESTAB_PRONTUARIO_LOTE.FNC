create or replace
function Obter_Estab_Prontuario_Lote (	nr_seq_solic_p		Number)
					return Number is

					
nr_atendimento_w	Number(10);
cd_pessoa_fisica_w	Varchar2(10);
dt_periodo_inicial_w	Date;
dt_periodo_final_w	Date;	
nr_seq_local_w		Number(10);
cd_estabelecimento_w	Number(4);		
					
begin

if (nr_seq_solic_p is not null) then
	select	nvl(nr_atendimento,0),
		cd_pessoa_fisica,
		dt_periodo_inicial,
		dt_periodo_final
	into	nr_atendimento_w,
		cd_pessoa_fisica_w,
		dt_periodo_inicial_w,
		dt_periodo_final_w
	from	same_solic_pront
	where	nr_sequencia = nr_seq_solic_p;

	select	max(nr_seq_local)
	into	nr_seq_local_w
	from	same_prontuario
	where	(nr_atendimento		= nr_atendimento_w or nr_atendimento_w = 0)
	and	cd_pessoa_fisica	= nvl(cd_pessoa_fisica_w, cd_pessoa_fisica)
	and	(dt_periodo_inicial_w	>= dt_periodo_inicial or dt_periodo_inicial_w is null)
	and	(dt_periodo_final_w	<= nvl(dt_periodo_final,sysdate));

	if (nr_seq_local_w is not null) then
		select	nvl(max(cd_estabelecimento),0)
		into	cd_estabelecimento_w
		from	same_local
		where 	nr_sequencia = nr_seq_local_w;
	else
		cd_estabelecimento_w := 0;
	end if;
	
end if;

return	cd_estabelecimento_w;

end Obter_Estab_Prontuario_Lote;
/
