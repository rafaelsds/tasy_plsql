CREATE OR REPLACE
FUNCTION Cns_Obter_Municipio_Digitacao
			(	cd_estabelecimento_p	Number,
				ie_tipo_p		Varchar)
				return Varchar2 is

/* IE_TIPO_P
	C - Codigo
	D - Descricao
*/

cd_cep_w	Varchar2(15);
ds_retorno_w	Varchar2(100);

BEGIN

select	nvl(max(b.cd_cep),'')
into	cd_cep_w
from	pessoa_juridica	b,
	estabelecimento a
where	cd_estabelecimento = cd_estabelecimento_p
and	a.cd_cgc = b.cd_cgc;

ds_retorno_w	:= obter_municipio_ibge(campo_numerico(cd_cep_w));

if	(ie_tipo_p	= 'D') then
	ds_retorno_w	:= obter_desc_municipio_ibge(ds_retorno_w);
end if;

return	ds_retorno_w;

END Cns_Obter_Municipio_Digitacao;
/