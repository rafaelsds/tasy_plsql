create or replace
function obter_qt_material_solic_compra(	nr_solic_compra_p		number,
					cd_material_p		number)
return number is

qt_material_w			number(13,4);

begin

select	nvl(sum(qt_material),0)
into	qt_material_w
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p
and	cd_material = cd_material_p;

return	qt_material_w;

end obter_qt_material_solic_compra;
/