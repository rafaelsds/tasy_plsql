create or replace
function obter_seq_interno_diag(nr_atendimento_p	number,
								cd_doenca_p			varchar2)
return number is

nr_seq_interno_w number(10);

begin

	select 	max(a.nr_seq_interno)
	into	nr_seq_interno_w
	from	diagnostico_doenca a
	where	a.cd_doenca = cd_doenca_p
	and		a.nr_atendimento in (   select  nr_atendimento
									from    atendimento_paciente
									where   cd_pessoa_fisica = OBTER_PESSOA_ATENDIMENTO(nr_atendimento_p, 'C'));

return nr_seq_interno_w;

end obter_seq_interno_diag;
/