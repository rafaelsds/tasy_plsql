create or replace
function san_obter_inapto_concatenado(nr_seq_doacao_p		number)
 		    	return Varchar2 is

ds_motivo_inapto_w		varchar2(90);			
ds_motivo_inapto_ant_w		varchar2(4000);	
Cursor C01 is
	select distinct	ds_motivo_inaptidao
	from   san_questionario a,
	       san_motivo_inaptidao b
	where  a.nr_seq_motivo_inaptidao = b.nr_sequencia
	and    a.nr_seq_doacao = nr_seq_doacao_p
	order by 1;
begin
if	(nr_seq_doacao_p is not null) then
	ds_motivo_inapto_ant_w := '';
	open C01;
	loop
	fetch C01 into	
		ds_motivo_inapto_w;
	exit when C01%notfound;
		begin
		ds_motivo_inapto_ant_w := ds_motivo_inapto_ant_w||', '||ds_motivo_inapto_w;
		end;
	end loop;
	close C01;	
	
	ds_motivo_inapto_ant_w := substr(ds_motivo_inapto_ant_w,2,4000); 
	
end if;	
	
return	ds_motivo_inapto_ant_w;

end  san_obter_inapto_concatenado;
/