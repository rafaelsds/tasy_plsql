create or replace
function pls_obter_se_tit_pag_escrit(nr_titulo_p	number)
				return varchar2 is 

cd_estabelecimento_w		number(5);
ie_retorno_w			varchar2(1)	:= 'S';
ie_exige_nf_pag_escrit_w	varchar2(1);
nr_seq_nota_fiscal_w		number(10);
nr_seq_pls_pag_prest_w		varchar2(10);

begin

select	max(nr_seq_pls_pag_prest),
	max(nr_seq_nota_fiscal),
	max(cd_estabelecimento)	
into	nr_seq_pls_pag_prest_w,
	nr_seq_nota_fiscal_w,
	cd_estabelecimento_w	
from	titulo_pagar
where	nr_titulo	= nr_titulo_p;

if	(nr_seq_pls_pag_prest_w is not null) then

	/* Verifica o par�metro */
	select	nvl(max(ie_exige_nf_pag_escrit),'N')
	into	ie_exige_nf_pag_escrit_w
	from	pls_parametro_pagamento
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(ie_exige_nf_pag_escrit_w = 'S') then
		if	(nr_seq_nota_fiscal_w is null) then
			ie_retorno_w	:= 'N';
		end if;
	end if;
end if;

return	ie_retorno_w;

end pls_obter_se_tit_pag_escrit;
/