create or replace
function Obter_ult_cirurgia_graf(	nr_atendimento_p	number,
									ie_param_p			varchar2,
									nm_usuario_p		varchar2)
				return varchar2 is

nr_cirurgia_retorno_w 	number(10);
				
begin

if (ie_param_p = 'G') then

	Select 	max(a.nr_cirurgia)
	into	nr_cirurgia_retorno_w
	from   	pepo_sv_cirurgia a,
			cirurgia c     
	where  	a.nr_cirurgia is not null
	and    	c.nr_cirurgia = a.nr_cirurgia 
	and    	c.nr_atendimento = nr_atendimento_p
	and    	a.nm_usuario = nm_usuario_p;
	
else

	Select 	max(a.nr_cirurgia)
	into	nr_cirurgia_retorno_w
	from 	pepo_item_grafico a,
			cirurgia_agente_anestesico b,
			cirurgia d        
	where 	a.nr_seq_cirur_agente = b.nr_sequencia
	 and	a.nr_cirurgia is not null
	 and    d.nr_cirurgia = a.nr_cirurgia
	 and    d.nr_atendimento = nr_atendimento_p
	 and 	a.nr_seq_cirur_agente is not null
	 and    	a.nm_usuario = nm_usuario_p
	 and	a.ie_grafico = 'S' 
	 and 	nvl(b.ie_situacao,'A') = 'A'; 

end if;


return	nr_cirurgia_retorno_w;

end Obter_ult_cirurgia_graf;
/