create or replace function obter_espec_troca_medico(nr_atendimento_p number,
				cd_medico_resp_p 	varchar2 )
				return varchar2 is

ds_retorno_w		varchar2(60);

begin
if (cd_medico_resp_p is not null) then

	ds_retorno_w := Obter_Espec_medico_atend(nr_atendimento_p, cd_medico_resp_p,'C');

	if(ds_retorno_w is not null) then
        return ds_retorno_w;
	else
		return obter_especialidade_medico(cd_medico_resp_p,'C');
    end if;

else

    return '';
    
end if;

end obter_espec_troca_medico;
/