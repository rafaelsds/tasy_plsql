create or replace
function obter_se_possui_painel(	nr_seq_indicador_p	number,
					nm_usuario_p		varchar2,
					cd_perfil_p		number)
					return varchar2 is

qt_registro_w	number(5,0);

begin

if	(((cd_perfil_p is not null) and (cd_perfil_p <> 0)) and ((nm_usuario_p is not null) and (nm_usuario_p = 'x'))) then
	select	count(*)
	into	qt_registro_w
	from	indicador_gestao_usuario
	where	nr_seq_indicador	=	nr_seq_indicador_p
	and	cd_perfil		= 	cd_perfil_p;
else
	select	count(*)
	into	qt_registro_w
	from	indicador_gestao_usuario
	where	nr_seq_indicador	=	nr_seq_indicador_p
	and	nm_usuario		=	nm_usuario_p;
end if;

if	(qt_registro_w > 0) then
	return 'S';
else
	return 'N';
end if;

end obter_se_possui_painel;
/