create or replace
function obter_desc_programa_pac(cd_pessoa_fisica_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(4000);
nr_seq_participante_w  number(10);
count_w	number(10) := 0;
ds_pessoa_pac_config_w	varchar2(1);
ds_visualizar_alerta_w	varchar2(1);
ds_medicina_preve_w	varchar2(255) := 'MEPREV';


Cursor C01 is
SELECT (SELECT x.nm_programa 
        FROM   mprev_programa x 
        WHERE  x.nr_sequencia = a.nr_seq_programa) ds_nome 
FROM   mprev_programa_partic a, 
       mprev_participante b 
WHERE  a.nr_seq_participante = b.nr_sequencia 
       AND b.cd_pessoa_fisica = (SELECT Max(cd_pessoa_fisica) 
                                 FROM   mprev_participante 
                                 WHERE  nr_sequencia = nr_seq_participante_w) 
       AND Trunc(Nvl(a.dt_exclusao, sysdate)) >= Trunc(sysdate) 
UNION ALL 
SELECT (SELECT x.nm_campanha 
        FROM   mprev_campanha x 
        WHERE  x.nr_sequencia = a.nr_seq_campanha) ds_nome 
FROM   mprev_campanha_partic a, 
       mprev_participante b 
WHERE  a.nr_seq_participante = b.nr_sequencia 
       AND Trunc(Nvl(a.dt_exclusao, sysdate)) >= Trunc(sysdate) 
       AND b.cd_pessoa_fisica = (SELECT Max(cd_pessoa_fisica) 
                                 FROM   mprev_participante 
                                 WHERE  nr_sequencia = nr_seq_participante_w) 
UNION ALL 
SELECT (SELECT x.nm_grupo 
        FROM   mprev_grupo_controle x 
        WHERE  x.nr_sequencia = a.nr_seq_grupo_controle) ds_nome 
FROM   mprev_grupo_cont_pessoa a 
WHERE  a.cd_pessoa_fisica = (SELECT Max(cd_pessoa_fisica) 
                             FROM   mprev_participante 
                             WHERE  nr_sequencia = nr_seq_participante_w) 
       AND Trunc(Nvl(a.dt_exclusao, sysdate)) >= Trunc(sysdate)
order by ds_nome; 

begin

ds_retorno_w := '<b> <font size="3"> ' || obter_desc_expressao(596757) || ' </font></b>' ;

if( cd_pessoa_fisica_p is not null )then

	select	nvl(max('S'), 'N')
	into	ds_pessoa_pac_config_w
	from	pessoa_paciente_config
	where	ie_campo_paciente = ds_medicina_preve_w;

	if( ds_pessoa_pac_config_w = 'S' )then
		select	max(a.nr_sequencia)
		into 	nr_seq_participante_w
		from	mprev_participante a
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
	
		if( nr_seq_participante_w is not null) then
		
			select	nvl(max('S'), 'N')
			into	ds_visualizar_alerta_w
			from	pessoa_paciente_config
			where	ie_campo_paciente = ds_medicina_preve_w
			and		((cd_pessoa_fisica = obter_pf_usuario_ativo or cd_pessoa_fisica is null)
			and 	(cd_perfil = obter_perfil_ativo or cd_perfil is null)
			and		(cd_estabelecimento = obter_estabelecimento_ativo or cd_estabelecimento is null)
			and		(obter_se_especialidades_medico(cd_especialidade, '(' || obter_especialidade_medico_cod(obter_pf_usuario_ativo) || ')') = 'S' or cd_especialidade is null)
			and		(cd_funcao = obter_funcao_ativa or cd_funcao is null));
		
		
			if( ds_visualizar_alerta_w = 'S')then
				for program_prev in C01 loop
					ds_retorno_w :=  ds_retorno_w || '<br/>' || '<br/>' || program_prev.ds_nome;
					count_w := count_w + 1;
				end loop;
			end if;

		end if;
	end if;

end if;

if(nvl( count_w, 0) <= 0)then
	ds_retorno_w := null;
end if;

if (ds_retorno_w is null) and 
	(pkg_i18n.get_user_locale() = 'ja_JP') and 
	(obter_parametro_funcao_padrao(281, 1609, wheb_usuario_pck.get_nm_usuario()) = 'S') and
	(wheb_usuario_pck.get_cd_funcao() = 281)  then

	ds_retorno_w := obter_desc_expressao(1038081);

end if;


return	ds_retorno_w ;

end obter_desc_programa_pac;
/