create or replace
function pls_obter_se_itens_aud_pos
			(	nr_seq_analise_p		Number,
				nr_seq_grupo_atual_p		Number,
				nm_usuario_p			Varchar2)
			return Varchar2 is

/*
Obter se todas entre todas as ocorr�ncias da an�lise existem ocorr�ncia pendentes.
	S - Existe
	S - N�o existe
*/
			
ds_retorno_w			Varchar2(1) := 'N';

Cursor C01 is
	select	'S'
	from	pls_analise_conta_item a
	where	a.nr_seq_analise = nr_seq_analise_p
	and	a.ie_status = 'P'
	and	a.ie_tipo = 'O'	
	order by 1;
	
begin

open C01;
loop
fetch C01 into	
	ds_retorno_w;
exit when C01%notfound;	
end loop;
close C01;

return	ds_retorno_w;

end pls_obter_se_itens_aud_pos;
/