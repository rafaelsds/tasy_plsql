create or replace 
function sepaco_Obter_saldo_Dia_medic(	dt_saldo_p	  		date,
				cd_estabelecimento_p		number,
				cd_local_estoque_p		Varchar2,
				cd_material_p			Number)
		return number IS

dt_fim_w			Date;
dt_inicio_mes_w		Date;
dt_mes_anterior_w		Date;
qt_saldo_w		Number(22,4);
qt_saldo_dia_w		Number(22,4);
dt_movimento_w		Date;
qt_estoque_w		Number(22,4);
qt_entrada_w		Number(22,4);
qt_saida_w		Number(22,4);
qt_perda_w		Number(22,4);
qt_transf_w		Number(22,4);

cursor C01  is
	select	/*+ index(a MOVESTO_I8) */
		PKG_DATE_UTILS.start_of(a.dt_movimento_estoque, 'ddd', 0) dt_movimento_estoque,
		sum(decode(a.cd_acao,'1', qt_estoque, qt_estoque * -1)) qt_estoque,
		sum(obter_qt_coluna_controlado(1, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_entrada,
		sum(obter_qt_coluna_controlado(2, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_saida,
		sum(obter_qt_coluna_controlado(3, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_perda,
		sum(obter_qt_coluna_controlado(4, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_transf
	from	operacao_estoque b,
		movimento_estoque a
	where	a.dt_movimento_estoque between  dt_inicio_mes_w and dt_fim_w
	and	a.cd_estabelecimento		= cd_estabelecimento_p
	and	a.cd_material_estoque		= cd_material_p
	and	a.cd_operacao_estoque		= b.cd_operacao_estoque
	and	b.ie_tipo_requisicao <> '2'
	and	((cd_local_estoque_p is null) or (obter_se_contido(a.cd_local_estoque, cd_local_estoque_p) = 'S'))
	group by PKG_DATE_UTILS.start_of(a.dt_movimento_estoque, 'ddd', 0)
	order by 1;

BEGIN
dt_inicio_mes_w	:= PKG_DATE_UTILS.start_of(dt_saldo_p, 'mm',0);
dt_fim_w		:= PKG_DATE_UTILS.start_of(dt_saldo_p,'dd',0);
dt_mes_anterior_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_inicio_mes_w,-1,0);

select	sum(qt_estoque)
into	qt_saldo_w
from	saldo_Estoque
where	cd_estabelecimento		= cd_estabelecimento_p
and	cd_material 		= cd_material_p
and	dt_mesano_referencia 	= dt_mes_anterior_w
and	((cd_local_estoque_p is null) or (obter_se_contido(cd_local_estoque, cd_local_estoque_p) = 'S'));


OPEN C01;
LOOP
FETCH C01 into
	dt_movimento_w,
	qt_estoque_w,
	qt_entrada_w,
	qt_saida_w,
	qt_perda_w,
	qt_transf_w;
EXIT when C01%notfound;
	qt_saldo_w		:= nvl(qt_saldo_w,0) +
				qt_entrada_w -
				qt_saida_w -
				qt_perda_w -
				qt_transf_w;
END LOOP;
CLOSE C01;

qt_saldo_dia_w	:= nvl(qt_saldo_w,0);
return qt_saldo_dia_w;

END sepaco_Obter_saldo_Dia_medic;
/