create or replace function obter_hint_graf_niss_adep	(nr_seq_grafico_p	number)
					return varchar2 is

ds_hint_w		varchar2(4000);

ds_titulo_w		varchar2(100);
ds_dispositivo_w	varchar2(100);
ds_topografia_w		varchar2(60);
ds_lado_w		varchar2(10);
dt_instalacao_w		varchar2(255);
dt_retirada_w		varchar2(255);
nm_responsavel_w	varchar2(60);
dt_retirada_prev_w	varchar2(255);
ds_setor_atendimento_w	varchar2(100);
ds_calibre_w		varchar2(255);
ie_sucesso_w        varchar2(1);

begin
if	(nr_seq_grafico_p is not null) then

	/* obter dados dispositivo */
	select	ds_titulo,
		substr(obter_nome_dispositivo(nr_seq_dispositivo),1,80) ds_dispositivo,
		substr(obter_desc_topografia_dor(nr_seq_topografia),1,60) ds_topografia,
		substr(obter_valor_dominio(1372,ie_lado),1,10) ds_lado,
		PKG_DATE_FORMATERS.TO_VARCHAR(dt_instalacao, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.GET_NM_USUARIO) dt_instalacao,
		PKG_DATE_FORMATERS.TO_VARCHAR(dt_retirada, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.GET_NM_USUARIO) dt_instalacao,
		substr(obter_nome_pf(nvl(cd_profissional,substr(obter_dados_usuario_opcao(nm_usuario_nrec,'C'),1,10))),1,60) nm_responsavel,
		PKG_DATE_FORMATERS.TO_VARCHAR(dt_retirada_prev, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.GET_NM_USUARIO),
		substr(obter_dados_dispositivo(nr_sequencia,'S'),1,100),
		substr(obter_calibre(nr_seq_calibre),1,100) , 
		nvl(ie_sucesso,'S')
	into	ds_titulo_w,
		ds_dispositivo_w,
		ds_topografia_w,
		ds_lado_w,
		dt_instalacao_w,
		dt_retirada_w,
		nm_responsavel_w,
		dt_retirada_prev_w,
		ds_setor_atendimento_w,
		ds_calibre_w, 
		ie_sucesso_w
	from	atend_pac_dispositivo
	where	nr_sequencia = nr_seq_grafico_p;

	/* montar hint */
	ds_hint_w := substr(wheb_mensagem_pck.get_texto(309126) || ': ',1,4000); -- Dispositivo

	if	(ds_titulo_w is not null) then
		ds_hint_w := substr(ds_hint_w || ds_titulo_w || ' - ' || ds_dispositivo_w,1,4000) || ' ';
	else
		ds_hint_w := substr(ds_hint_w || ds_dispositivo_w,1,4000) || ' ';
	end if;

	if	(ds_topografia_w is not null) then
		ds_hint_w := substr(ds_hint_w || chr(10) || wheb_mensagem_pck.get_texto(309127) || ': ' || ds_topografia_w,1,4000) || ' '; -- Topografia
	end if;

	if	(ds_lado_w is not null) then
		ds_hint_w := substr(ds_hint_w || chr(10) || wheb_mensagem_pck.get_texto(309129) || ': ' || ds_lado_w,1,4000)|| ' '; -- Lado
	end if;

	if (ie_sucesso_w = 'S') then
	   ds_hint_w := substr(ds_hint_w || chr(10) || wheb_mensagem_pck.get_texto(309131) || ': ' || dt_instalacao_w || ' ' || wheb_mensagem_pck.get_texto(309133) || ' ' || nm_responsavel_w,1,4000)|| ' '; -- Instalado em -- por
	else 
	   ds_hint_w := substr(ds_hint_w || chr(10) || wheb_mensagem_pck.get_texto(309135) || ': ' || dt_instalacao_w || ' ' || wheb_mensagem_pck.get_texto(309133) || ' ' || nm_responsavel_w,1,4000)|| ' '; -- Tentativa de instalação em -- por
	end if ;

	if	(dt_retirada_prev_w is not null) and
		(dt_retirada_w is null) then
		   ds_hint_w	:= substr(ds_hint_w || chr(10) || wheb_mensagem_pck.get_texto(309137) || ': ' || dt_retirada_prev_w,1,4000)|| ' '; -- O prazo de validade do dispositivo expira em
	end	if;
	
	if	(ds_setor_atendimento_w is not null) then
	    if (ie_sucesso_w = 'S') then
			ds_hint_w := substr(ds_hint_w || chr(10) || wheb_mensagem_pck.get_texto(309136) || ' ' || ds_setor_atendimento_w,1,4000)|| ' '; -- Instalado no setor
		else 
			ds_hint_w := substr(ds_hint_w || chr(10) || wheb_mensagem_pck.get_texto(309139) || ' ' || ds_setor_atendimento_w,1,4000)|| ' '; -- Setor da tentativa
		end if;
	end if;
	
	if	(ds_calibre_w is not null) then
		ds_hint_w := substr(ds_hint_w || chr(10) || wheb_mensagem_pck.get_texto(309140) || ': ' ||ds_calibre_w,1,4000)|| ' '; -- Calibre
	end if;
end if;

return ds_hint_w;

end obter_hint_graf_niss_adep;
/