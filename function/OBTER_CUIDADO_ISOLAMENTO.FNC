create or replace
function obter_cuidado_isolamento(nr_atendimento_p number,
    ie_opcao_p   varchar2)
        return varchar2 is

ds_precaucao_w   varchar2(4000);
nr_seq_precaucao_atend_w  number(10);
nr_seq_precaucao_w  number(10);

begin

select nvl(max(nr_Sequencia),0)
into nr_seq_precaucao_atend_w
from atendimento_precaucao
where nr_atendimento = nr_atendimento_p
and dt_termino is null;

if (nr_seq_precaucao_atend_w > 0) then
 select  nr_seq_precaucao
 into nr_seq_precaucao_w
 from atendimento_precaucao
 where nr_sequencia = nr_seq_precaucao_atend_w;

 select  DS_CUIDADO
 into ds_precaucao_w
 from cih_precaucao
 where nr_sequencia = nr_seq_precaucao_w;
end if;

if (upper(ie_opcao_p) = 'C') then
 return nr_seq_precaucao_w;
else
 return substr(ds_precaucao_w,1,4000);
end if;

end obter_cuidado_isolamento;
/