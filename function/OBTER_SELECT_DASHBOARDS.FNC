create or replace function Obter_select_dashboards(	ds_comando_p		Varchar2,
					ds_parametros_p		Varchar2,
                           		ds_separador_p		Varchar2,
								nr_colum_p 			number) return varchar2 as

c001				  	Integer;		
retorno_w				Integer;
ds_retorno_w				varchar2(4000);
ds_valor_w				varchar2(2000);

type lista is RECORD (
	nm VARCHAR2(2000),  
	vl long );
type myArray is table of lista index by binary_integer;

/*Contem os parametros do SQL*/
ar_parametros_w myArray;

ds_param_atual_w 	VARCHAR2(2000);   
ds_parametros_w 	VARCHAR2(2000);
nr_pos_separador_w	NUMBER(10);
qt_parametros_w		NUMBER(10);
qt_contador_w		number(10);


ds_sep_bv_w		varchar2(10);
qt_tam_seq_w		number(2);


BEGIN
	ds_sep_bv_w := obter_separador_bv;
	if	(instr(ds_parametros_p,ds_sep_bv_w) = 0 ) then
		ds_sep_bv_w := ';';
	end if;
	qt_tam_seq_w := length(ds_sep_bv_w);
	ds_parametros_w    := ds_parametros_p;
	nr_pos_separador_w := instr(ds_parametros_w,ds_sep_bv_w);
	qt_parametros_w	   := 0;
	while	(nr_pos_separador_w > 0 ) loop
		begin
		qt_parametros_w := qt_parametros_w + 1;
		ds_param_atual_w  := substr(ds_parametros_w,1,nr_pos_separador_w-1);
		ds_parametros_w   := substr(ds_parametros_w,nr_pos_separador_w+qt_tam_seq_w,length(ds_parametros_w));
		nr_pos_separador_w := instr(ds_param_atual_w,'=');
		ar_parametros_w(qt_parametros_w).nm := upper(substr(ds_param_atual_w,1,nr_pos_separador_w-1));
		ar_parametros_w(qt_parametros_w).vl := substr(ds_param_atual_w,nr_pos_separador_w+1,length(ds_param_atual_w));
		nr_pos_separador_w := instr(ds_parametros_w,ds_sep_bv_w);
		if	(qt_parametros_w > 1000) then
			nr_pos_separador_w := 0;
		end if;
		end;
	end loop;
	nr_pos_separador_w := instr(ds_parametros_w,'=');
	if	( nr_pos_separador_w > 0 ) then
		qt_parametros_w := qt_parametros_w +1;
		ds_param_atual_w := ds_parametros_w;
		ar_parametros_w(qt_parametros_w).nm := upper(substr(ds_param_atual_w,1,nr_pos_separador_w-1));
		ar_parametros_w(qt_parametros_w).vl := substr(ds_param_atual_w,nr_pos_separador_w+1,length(ds_param_atual_w));		
	end if;

	begin
	C001 := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(C001, ds_comando_p, dbms_sql.Native);
	DBMS_SQL.DEFINE_COLUMN(C001, nr_colum_p, ds_valor_w,2000);
	for contador_w in 1..ar_parametros_w.count loop
		DBMS_SQL.BIND_VARIABLE(C001, ar_parametros_w(contador_w).nm, ar_parametros_w(contador_w).vl,255);
	end loop;

	retorno_w := DBMS_SQL.execute(c001); 

	while	( DBMS_SQL.FETCH_ROWS(C001) > 0 ) loop

		DBMS_SQL.COLUMN_VALUE(C001, nr_colum_p, ds_valor_w );
		if (ds_retorno_w is not null ) then
			ds_retorno_w := ds_retorno_w || ds_separador_p;
		end if;

		ds_retorno_w := substr(ds_retorno_w || ds_valor_w,1,4000);

	end loop;
	DBMS_SQL.CLOSE_CURSOR(C001);
	exception
	when others then
		DBMS_SQL.CLOSE_CURSOR(C001);
	end;

return ds_retorno_w;

end Obter_select_dashboards; 
/
