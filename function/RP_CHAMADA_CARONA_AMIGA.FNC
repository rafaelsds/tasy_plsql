create or replace
function rp_chamada_carona_amiga( cd_estabelecimento_p	number)
 		    	return varchar2 is
ds_retorno_w	varchar2(1);
begin
select	max(nvl(ie_chamada_agenda,'N'))
into	ds_retorno_w
from	rp_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

return	ds_retorno_w;

end rp_chamada_carona_amiga;
/
