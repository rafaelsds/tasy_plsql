create or replace
function Vivalle_Obter_horario_format(ds_hora_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);			
			
begin

if	(ds_hora_p = 0) then
	ds_retorno_w := '00:00 - 00:59';
elsif	(ds_hora_p = 1) then
	ds_retorno_w := '01:00 - 01:59';
elsif	(ds_hora_p = 2) then
	ds_retorno_w := '02:00 - 02:59';
elsif	(ds_hora_p = 3) then
	ds_retorno_w := '03:00 - 03:59';
elsif	(ds_hora_p = 4) then
	ds_retorno_w := '04:00 - 04:59';
elsif	(ds_hora_p = 5) then
	ds_retorno_w := '05:00 - 05:59';
elsif	(ds_hora_p = 6) then
	ds_retorno_w := '06:00 - 06:59';
elsif	(ds_hora_p = 7) then
	ds_retorno_w := '07:00 - 07:59';
elsif	(ds_hora_p = 8) then
	ds_retorno_w := '08:00 - 08:59';
elsif	(ds_hora_p = 9) then
	ds_retorno_w := '09:00 - 09:59';
elsif	(ds_hora_p = 10) then
	ds_retorno_w := '10:00 - 10:59';
elsif	(ds_hora_p = 11) then
	ds_retorno_w := '11:00 - 11:59';
elsif	(ds_hora_p = 12) then
	ds_retorno_w := '12:00 - 12:59';
elsif	(ds_hora_p = 13) then
	ds_retorno_w := '13:00 - 13:59';
elsif	(ds_hora_p = 14) then
	ds_retorno_w := '14:00 - 14:59';
elsif	(ds_hora_p = 15) then
	ds_retorno_w := '15:00 - 15:59';
elsif	(ds_hora_p = 16) then
	ds_retorno_w := '16:00 - 16:59';
elsif	(ds_hora_p = 17) then
	ds_retorno_w := '17:00 - 17:59';
elsif	(ds_hora_p = 18) then
	ds_retorno_w := '18:00 - 18:59';
elsif	(ds_hora_p = 19) then
	ds_retorno_w := '19:00 - 19:59';
elsif	(ds_hora_p = 20) then
	ds_retorno_w := '20:00 - 20:59';
elsif	(ds_hora_p = 21) then
	ds_retorno_w := '21:00 - 21:59';
elsif	(ds_hora_p = 22) then
	ds_retorno_w := '22:00 - 22:59';
elsif	(ds_hora_p = 23) then
	ds_retorno_w := '23:00 - 23:59';
end if;	

return	ds_retorno_w;

end Vivalle_Obter_horario_format;
/