create or replace
function obter_exame_lab_proc_int	(nr_seq_proc_interno_p	number)
				return number is
												
nr_seq_exame_w	number(10,0);
												
begin
if	(nr_seq_proc_interno_p is not null) then

	select	max(nr_seq_exame_lab)
	into	nr_seq_exame_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_p;

end if;

return nr_seq_exame_w;

end obter_exame_lab_proc_int;
/