create or replace
function obter_desc_nervo(nr_seq_nervo_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin
select	max(ds_nervo)
into	ds_retorno_w
from	nervo
where	nr_sequencia = nr_seq_nervo_p;

return	ds_retorno_w;

end obter_desc_nervo;
/