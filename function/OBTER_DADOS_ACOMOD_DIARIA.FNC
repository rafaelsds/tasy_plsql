create or replace
function Obter_dados_acomod_diaria(nr_atendimento_p	number,
				   nr_seq_interno_p	number,
				   ie_tipo_p		varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
cd_tipo_acomodacao_w	number(4,0);
			
begin

if	(ie_tipo_p = 'A') then --Acomodação
	
	select	nvl(max(cd_tipo_acomodacao),0)
	into	cd_tipo_acomodacao_w
	from 	atend_acomod_diaria
	where 	nr_atendimento = nr_atendimento_p
	and 	nr_seq_interno = nr_seq_interno_p;
	
	if	(cd_tipo_acomodacao_w > 0) then
		
		select	substr(nvl(max(ds_tipo_acomodacao),Wheb_mensagem_pck.get_texto(800043)),1,255)
		into	ds_retorno_w
		from 	tipo_acomodacao
		where 	cd_tipo_acomodacao = cd_tipo_acomodacao_w;
		
	end if;
	
end if;

return	ds_retorno_w;

end Obter_dados_acomod_diaria;
/