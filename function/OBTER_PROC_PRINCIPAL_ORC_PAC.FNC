create or replace
function Obter_proc_principal_orc_pac (	nr_seq_orcamento_p	number)
 		    	return varchar2 is

ds_procedimento_w	varchar2(255);
ds_procedimentos_w	varchar2(500) := '';

Cursor C01 is
select	substr(obter_desc_procedimento(a.cd_procedimento,a.ie_origem_proced),1,255)
from	orcamento_paciente_proc a
where	nr_sequencia_orcamento = nr_seq_orcamento_p
and	ie_procedimento_principal = 'S';

begin

open C01;
loop
fetch C01 into	
	ds_procedimento_w;
exit when C01%notfound;
	begin
	ds_procedimentos_w := ds_procedimentos_w || ds_procedimento_w || ' / ';
	
	end;
end loop;
close C01;

return	substr(ds_procedimentos_w,1,length(ds_procedimentos_w) - 3);

end Obter_proc_principal_orc_pac;
/