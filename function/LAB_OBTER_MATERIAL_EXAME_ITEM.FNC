create or replace
FUNCTION lab_obter_material_exame_item (nr_prescricao_p		VARCHAR2,
					nr_seq_prescr_p		VARCHAR2,
					ie_opcao_p			VARCHAR2) RETURN VARCHAR2 IS


cd_material_exame_w		VARCHAR2(20);
nr_seq_material_w		VARCHAR2(20);
ds_retorno_w			VARCHAR2(20);

BEGIN

SELECT  MAX(d.cd_material_exame),
	MAX(d.nr_sequencia)
INTO 	cd_material_exame_w,
	nr_seq_material_w
FROM	exame_lab_resultado b,
	material_exame_lab d,
	exame_lab_result_item c,
	prescr_procedimento a
WHERE 	a.nr_prescricao = b.nr_prescricao
AND	a.nr_sequencia = c.nr_seq_prescr
AND	b.nr_seq_resultado = c.nr_seq_resultado
AND	c.nr_seq_material = d.nr_sequencia
AND	a.nr_prescricao = nr_prescricao_p
AND	a.nr_sequencia  = nr_seq_prescr_p
AND	c.nr_seq_material IS NOT NULL;

IF	(cd_material_exame_w IS NULL) THEN

	SELECT  MAX(d.cd_material_exame),
		MAX(d.nr_sequencia)
	INTO 	cd_material_exame_w,
		nr_seq_material_w
	FROM	material_exame_lab d,
		prescr_procedimento a
	WHERE 	a.cd_material_exame = d.cd_material_exame
	AND	a.nr_prescricao = nr_prescricao_p
	AND	a.nr_sequencia  = nr_seq_prescr_p;

END IF;	  

IF	(ie_opcao_p = 'C') THEN
	ds_retorno_w := cd_material_exame_w;
ELSIF	(ie_opcao_p = 'S') THEN
	ds_retorno_w := nr_seq_material_w;
END IF;

RETURN	ds_retorno_w;

END lab_obter_material_exame_item;
/