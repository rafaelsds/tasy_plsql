create or replace
function pls_obter_se_gera_juro_mult
		(	cd_estabelecimento_p	number)
 		    	return varchar2 is

ie_gerar_juro_mult_tit_w	varchar2(1);
			
begin

select	nvl(ie_gerar_juro_mult_tit_repasse,'N')
into	ie_gerar_juro_mult_tit_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

return	ie_gerar_juro_mult_tit_w;

end pls_obter_se_gera_juro_mult;
/