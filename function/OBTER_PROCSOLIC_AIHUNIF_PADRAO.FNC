CREATE OR REPLACE
FUNCTION Obter_Procsolic_Aihunif_Padrao
				(	nr_interno_conta_p	number)
				return number is


nr_atendimento_w	number(10);
nr_aih_w		number(13);
cd_procedimento_w	number(15)	:= 0;

BEGIN

select	nr_atendimento
into	nr_atendimento_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

select	nvl(max(nr_aih),0) nr_aih
into	nr_aih_w
from	sus_aih_unif
where 	nr_interno_conta 	= nr_interno_conta_p;

if	(nr_aih_w	= 0) then
	select	nvl(max(nr_aih),0)
	into	nr_aih_w
	from	sus_aih_unif
	where	nr_atendimento	= nr_atendimento_w;
end if;


if	(nr_aih_w	> 0) then
	begin
	select	nvl(max(cd_procedimento_solic),0)	
	into	cd_procedimento_w
	from	sus_laudo_paciente
	where	nr_aih			= nr_aih_w
	and	nr_atendimento		= nr_atendimento_w
	and	ie_tipo_laudo_sus	= 1;

	if	(cd_procedimento_w	= 0) then
		begin
		select	nvl(max(cd_procedimento_solic),0)
		into	cd_procedimento_w
		from	sus_laudo_paciente
		where	nr_aih			= nr_aih_w
		and	nr_atendimento	 	= nr_atendimento_w
		and	ie_tipo_laudo_sus	= 0;		
		
		if	(cd_procedimento_w	= 0) then
			begin
			select	nvl(max(cd_procedimento_solic),0)
			into	cd_procedimento_w
			from	sus_aih_unif
			where	nr_aih	=	nr_aih_w;
			end;
		end if;
		end;
	end if;
	end;
else
	begin
	select	nvl(max(cd_procedimento_solic),0)	
	into	cd_procedimento_w
	from	sus_laudo_paciente
	where	nr_atendimento		= nr_atendimento_w
	and	ie_tipo_laudo_sus	= 1;

	if	(cd_procedimento_w	= 0) then
		begin
		select	nvl(max(cd_procedimento_solic),0)
		into	cd_procedimento_w
		from	sus_laudo_paciente
		where	nr_atendimento	 	= nr_atendimento_w
		and	ie_tipo_laudo_sus	= 0;
		end;
	end if;		
	end;
end if;

RETURN	cd_procedimento_w;

END Obter_Procsolic_Aihunif_Padrao;
/