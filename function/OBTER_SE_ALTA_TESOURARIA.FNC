create or replace
function obter_se_alta_tesouraria(	cd_setor_atendimento_p		number,
					cd_unidade_basica_p		varchar2,
					cd_unidade_compl_p		varchar2)
 		    	return varchar2 is

cd_setor_w	number(10);
ds_retorno_w	varchar2(255);
ie_exibe_status_w varchar2(1);
nr_seq_unidade_w  number(10);
			
begin

obter_param_usuario(44, 172, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_exibe_status_w);

if (ie_exibe_status_w = 'S') then
	SELECT max(nr_seq_interno)
	into nr_seq_unidade_w
	FROM unidade_atendimento
	where cd_setor_atendimento = cd_setor_atendimento_p
	AND cd_unidade_basica = cd_unidade_basica_p
	AND cd_unidade_compl = cd_unidade_compl_p;

	SELECT	distinct nvl(max(b.cd_setor_atendimento),0)
	into cd_setor_w
	FROM atend_paciente_unidade b,
		atendimento_paciente a
	WHERE	a.nr_atendimento = b.nr_atendimento
	AND 	obter_conta_particular_aberta(a.nr_atendimento) IS NOT NULL
	AND	a.dt_alta	IS NOT NULL 
	AND	a.dt_saida_real	IS NULL
	AND	a.dt_alta_tesouraria IS NULL
	AND b.nr_sequencia =  (SELECT MAX(nr_sequencia) FROM atend_paciente_unidade WHERE nr_atendimento = a.nr_atendimento)
	AND b.cd_setor_atendimento = cd_setor_atendimento_p
	AND b.cd_unidade_basica = cd_unidade_basica_p
	AND b.cd_unidade_compl = cd_unidade_compl_p
	and a.nr_atendimento = (select max(nr_atendimento) from unidade_atend_hist where dt_fim_historico is null and ie_status_unidade = 'A' 
	and nr_seq_unidade = nr_seq_unidade_w);
end if;

if (cd_setor_w > 0) and (ie_exibe_status_w = 'S') then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end obter_se_alta_tesouraria;
/
