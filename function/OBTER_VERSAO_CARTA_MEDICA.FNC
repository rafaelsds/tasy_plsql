create or replace function obter_versao_carta_medica( nr_sequencia_p		number)
 		    	return varchar2 is
				
ds_retorno_w			Varchar2(100);		
nr_versao_w				number(5);		
nr_seq_carta_mae_w      number(10);
begin

select max(nr_seq_carta_mae)
into   nr_seq_carta_mae_w
from   carta_medica
where  nr_sequencia = nr_sequencia_p;

select	nvl(count(nr_versao),0)
into	nr_versao_w
from   	carta_medica a
where	nr_seq_carta_mae	= nr_seq_carta_mae_w
and		ie_preliminar = 'S'
and		nr_sequencia <= nr_sequencia_p;

ds_retorno_w := nr_versao_w;

select	nvl(count(nr_versao),0)
into	nr_versao_w
from   	carta_medica a
where	nr_seq_carta_mae = nr_seq_carta_mae_w
and		ie_preliminar in ('N', 'A')
and		nr_sequencia <= nr_sequencia_p;


if (pkg_i18n.get_user_locale <>  'de_AT') then
		if (nr_versao_w > 0) then
		ds_retorno_w := ds_retorno_w ||'.'||nr_versao_w;
		end if;
end if;

return	ds_retorno_w;

end obter_versao_carta_medica;
/
