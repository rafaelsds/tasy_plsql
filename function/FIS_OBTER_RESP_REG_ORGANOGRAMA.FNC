CREATE OR REPLACE
FUNCTION fis_obter_resp_reg_organograma(nr_sequencia_p IN NUMBER,
                                        cd_pessoa_fisica_p IN VARCHAR2,
                                        ie_option_table_p IN VARCHAR2)
                                    RETURN VARCHAR2 IS

cd_pessoa_fisica_w usuario.cd_pessoa_fisica%TYPE;
response_w VARCHAR2(1) := 'S';

BEGIN
   IF(ie_option_table_p = 'DEPARTAMENTO_PHILIPS') THEN
        BEGIN
            SELECT max(nvl(dp.cd_pessoa_fisica, 'T'))
            INTO cd_pessoa_fisica_w
            FROM departamento_philips dp
            WHERE dp.nr_sequencia = nr_sequencia_p
            AND (dp.cd_pessoa_fisica = cd_pessoa_fisica_p OR dp.cd_pessoa_fisica is null);
        EXCEPTION WHEN no_data_found THEN
            cd_pessoa_fisica_w := null;
        END;
    END IF;

    IF(ie_option_table_p = 'GERENCIA_WHEB') THEN
        BEGIN
            SELECT max(nvl(g.cd_responsavel, 'T'))
            INTO cd_pessoa_fisica_w
            FROM gerencia_wheb g
            WHERE g.nr_sequencia = nr_sequencia_p
            AND (g.cd_responsavel = cd_pessoa_fisica_p OR g.cd_responsavel is null);
        EXCEPTION WHEN no_data_found THEN
            cd_pessoa_fisica_w := null;
        END;
    END IF;

    IF (cd_pessoa_fisica_w is not null) THEN
        response_w := 'S';
    ELSE
        response_w := 'N';
    END IF;

RETURN response_w;

END fis_obter_resp_reg_organograma;
/
