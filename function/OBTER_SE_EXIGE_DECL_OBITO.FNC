create or replace
function obter_se_exige_decl_obito(nr_atendimento_p	Number)
 		    	return Varchar2 is

cd_setor_atendimento_w	Number(5);
ds_retorno_w		Varchar2(1) := 'N';
			
begin

cd_setor_atendimento_w	:= obter_setor_atendimento(nr_Atendimento_p);

select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	regra_decl_obito_setor
where	cd_setor_atendimento	= cd_setor_atendimento_w
and	ie_situacao		= 'A';

return	ds_retorno_w;

end obter_se_exige_decl_obito;
/