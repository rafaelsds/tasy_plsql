create or replace
function pls_obter_obervacao_titulo
			(	nr_titulo_p		Number)
 		    	return Varchar2 is

ds_retorno_w	Varchar2(255);

begin

begin
select	a.ds_observacao_titulo
into	ds_retorno_w
from	titulo_receber		a,
	pls_mensalidade		b,
	pls_contrato_pagador	c,
	pls_contrato_pagador_fin d
where	a.nr_seq_mensalidade	= b.nr_sequencia
and	b.nr_seq_pagador	= c.nr_sequencia
and	d.nr_seq_pagador	= c.nr_sequencia
and	a.nr_titulo		= nr_titulo_p
and	a.dt_emissao between d.dt_inicio_vigencia and fim_dia(nvl(d.dt_fim_vigencia,a.dt_emissao))
and	d.ie_destacar_reajuste	= 'S';
exception
when others then
	ds_retorno_w	:= null;
end;

if	(ds_retorno_w is not null) then
	ds_retorno_w	:= 'Percentual de Reajuste 6,73% conforme Oficio n�1065/2010/GGEFP/DIPRO, Protocolo n�33902.127824/2010-00 de 20/07/2010 entre 05/2010 e 04/2011';
end if;

return	ds_retorno_w;

end pls_obter_obervacao_titulo;
/