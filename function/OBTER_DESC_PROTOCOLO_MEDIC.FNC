create or replace 
FUNCTION Obter_desc_protocolo_medic(
			nr_sequencia_p		Number,
			cd_protocolo_p		Number)
			return Varchar2 is

ds_retorno_w	Varchar2(255);

BEGIN

if	(cd_protocolo_p is not null) and
	(nr_sequencia_p is not null) then
	select	nm_medicacao
	into	ds_retorno_w
	from	protocolo_medicacao
	where	cd_protocolo	= cd_protocolo_p
	and	nr_sequencia	= nr_sequencia_p;
end if;

return ds_retorno_w;

END Obter_desc_protocolo_medic;
/
