create or replace
function Ageint_Obter_Se_Turno_Exame (
			nr_seq_proc_interno_p		number,
			cd_agenda_p			number,
			cd_medico_p			varchar2,
			dt_agenda_p			date,
			cd_estabelecimento_p		number,
			nr_seq_grupo_selec_p		number,
			cd_paciente_p		varchar2,
			nr_seq_ageint_item_p			number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1)	:= 'S';
dt_dia_semana_w	number(10);
qt_Regra_w	number(10);
nr_seq_regra_w	number(10);
ie_sexo_w		varchar2(1)	:= 'A';
qt_idade_pac_w		number(5);
nr_seq_proc_interno_w	ageint_exame_adic_item.nr_seq_proc_interno%type;
qt_regra_adic_w	number(10,0);
nr_seq_regra_w_adic_w	number(10,0);

Cursor C01 is
	select	nr_sequencia
	from	ageint_turno_exame
	where	(((nr_seq_proc_interno	= nr_seq_proc_interno_p) and (nr_seq_grupo_ageint is null)) or
			((nr_seq_grupo_ageint 	= nr_seq_grupo_selec_p) and (nr_seq_grupo_selec_p is not null)))
	and		nvl(cd_agenda, cd_agenda_p)		= cd_agenda_p
	and		(nvl(cd_pessoa_fisica, cd_medico_p)	= cd_medico_p or (cd_medico_p is null and cd_pessoa_fisica is null))
	and		((dt_inicial_vigencia	<= trunc(dt_agenda_p)) or (dt_inicial_vigencia is null))
	and		((dt_final_vigencia	>= trunc(dt_agenda_p)) or (dt_final_vigencia is null))
	and		dt_agenda_p between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
							and to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') - 1 / 1440
			and	((dt_dia_semana	= dt_dia_semana_w) or (dt_dia_semana	= 9 and dt_dia_semana_w	not in (1,7)))
	and		nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	and		((nvl(ie_sexo, 'A') = ie_sexo_w) or (nvl(ie_sexo,'A') = 'A'))
	and		(((qt_idade_pac_w >= nvl(qt_idade_min, qt_idade_pac_w)) and (qt_idade_pac_w <= nvl(qt_idade_max, qt_idade_pac_w))) or (qt_idade_pac_w is null))
	and		nvl(ie_consiste_marcacao,'N') = 'N'
	and		((qt_dias_validade is null) or (dt_agenda_p >=(trunc(sysdate)+qt_dias_validade)))
	order 	by nvl(cd_pessoa_fisica,'0'),
			dt_dia_semana;
			
Cursor C02 is
	select	nr_seq_proc_interno
	from	ageint_exame_adic_item
	where	nr_seq_item	= nr_seq_ageint_item_p
	order by nr_seq_proc_interno;	

Cursor C03 is
	select	nr_sequencia
	from	ageint_turno_exame
	where	(((nr_seq_proc_interno	= nr_seq_proc_interno_w) and (nr_seq_grupo_ageint is null)) or
			((nr_seq_grupo_ageint 	= nr_seq_grupo_selec_p) and (nr_seq_grupo_selec_p is not null)))
	and		nvl(cd_agenda, cd_agenda_p)		= cd_agenda_p
	and		(nvl(cd_pessoa_fisica, cd_medico_p)	= cd_medico_p or (cd_medico_p is null and cd_pessoa_fisica is null))
	and		((dt_inicial_vigencia	<= trunc(dt_agenda_p)) or (dt_inicial_vigencia is null))
	and		((dt_final_vigencia	>= trunc(dt_agenda_p)) or (dt_final_vigencia is null))
	and		dt_agenda_p between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
							and to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') - 1 / 1440
			and	((dt_dia_semana	= dt_dia_semana_w) or (dt_dia_semana	= 9 and dt_dia_semana_w	not in (1,7)))
	and		nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	and		((nvl(ie_sexo, 'A') = ie_sexo_w) or (nvl(ie_sexo,'A') = 'A'))
	and		(((qt_idade_pac_w >= nvl(qt_idade_min, qt_idade_pac_w)) and (qt_idade_pac_w <= nvl(qt_idade_max, qt_idade_pac_w))) or (qt_idade_pac_w is null))
	and		nvl(ie_consiste_marcacao,'N') = 'N'
	and		((qt_dias_validade is null) or (dt_agenda_p >=(trunc(sysdate)+qt_dias_validade)))
	order 	by nvl(cd_pessoa_fisica,'0'),
			dt_dia_semana;	
			
begin

if	(cd_paciente_p is not null) then
	select	nvl(max(ie_sexo), 'A'),
			nvl(max(trunc(trunc(months_between(sysdate, dt_nascimento)) / 12)),0)
			--max(obter_idade(dt_nascimento, sysdate, 'A'))
	into	ie_sexo_w,
			qt_idade_pac_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_paciente_p;
end if;

if	((nvl(nr_seq_proc_interno_p,0)	> 0) or (nvl(nr_seq_grupo_selec_p,0)	> 0)) then
	begin
	select	max(1)
	into	qt_regra_w
	from	ageint_turno_exame
	where	(((nr_seq_proc_interno	= nr_seq_proc_interno_p) and (nr_seq_grupo_ageint is null)) or
			((nr_seq_grupo_ageint 	= nr_seq_grupo_selec_p) and (nr_seq_grupo_selec_p is not null)))
	and		nvl(cd_agenda, cd_agenda_p)		= cd_agenda_p
	and		nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	and		((dt_inicial_vigencia	<= trunc(dt_agenda_p)) or (dt_inicial_vigencia is null))
	and		((dt_final_vigencia	>= trunc(dt_agenda_p)) or (dt_final_vigencia is null))
	and		((qt_dias_validade is null) or (dt_agenda_p >= (trunc(sysdate)+qt_dias_validade)))
	and		nvl(ie_consiste_marcacao,'N') = 'N';
	exception
	when no_data_found then
		qt_regra_w := 0;
	end;
		
	if	(qt_regra_w	> 0) then
		dt_dia_semana_w	:= obter_cod_dia_semana(dt_agenda_p);

		open C01;
		loop
		fetch C01 into
			nr_seq_regra_w;
		exit when C01%notfound;
			begin
			nr_seq_regra_w	:= nr_seq_regra_w;
			end;
		end loop;
		close C01;
		if	(nr_seq_regra_w	> 0) then
			ds_retorno_w	:= 'S';
		else
			ds_retorno_w	:= 'N';
		end if;
	end if;
	
end if;

open C02;
loop
fetch C02 into	
	nr_seq_proc_interno_w;
exit when C02%notfound;
	begin
	if	((nvl(nr_seq_proc_interno_w,0)	> 0) 
	or 	(nvl(nr_seq_grupo_selec_p,0)	> 0)) 
	and 	(ds_retorno_w <> 'N') then
		begin
		
		select	max(1)
		into	qt_regra_adic_w
		from	ageint_turno_exame
		where	(((nr_seq_proc_interno	= nr_seq_proc_interno_w) and (nr_seq_grupo_ageint is null)) or
				((nr_seq_grupo_ageint 	= nr_seq_grupo_selec_p) and (nr_seq_grupo_selec_p is not null)))
		and		nvl(cd_agenda, cd_agenda_p)		= cd_agenda_p
		and		nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
		and		((dt_inicial_vigencia	<= trunc(dt_agenda_p)) or (dt_inicial_vigencia is null))
		and		((dt_final_vigencia	>= trunc(dt_agenda_p)) or (dt_final_vigencia is null))
		and		((qt_dias_validade is null) or (dt_agenda_p >= (trunc(sysdate)+qt_dias_validade)))
		and		nvl(ie_consiste_marcacao,'N') = 'N';
		exception
		when no_data_found then
			qt_regra_adic_w := 0;
		end;
		
		if	(qt_regra_adic_w	> 0) then
			dt_dia_semana_w	:= obter_cod_dia_semana(dt_agenda_p);
			nr_seq_regra_w_adic_w := 0;
			open C03;
			loop
			fetch C03 into
				nr_seq_regra_w_adic_w;
			exit when C03%notfound;
				begin
				nr_seq_regra_w_adic_w	:= nr_seq_regra_w_adic_w;
				end;
			end loop;
			close C03;
			if	(nr_seq_regra_w_adic_w	> 0) then
				ds_retorno_w	:= 'S';
			else
				ds_retorno_w	:= 'N';
			end if;		
		end if;
		
	end if;
	end;
end loop;
close C02;

return	ds_retorno_w;

end Ageint_Obter_Se_Turno_Exame;
/