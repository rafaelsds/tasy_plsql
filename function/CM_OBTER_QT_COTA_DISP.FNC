create or replace
function	cm_obter_qt_cota_disp(
		nr_requisicao_p		number,
		nr_seq_item_req_p		number,
		qt_conjunto_p		number) return number is

qt_regra_w			number(10);
qt_conjunto_regra_w		number(10);
ie_periodo_w			varchar2(1);
cd_setor_atendimento_w		number(5);
cd_estabelecimento_w		number(4);
qt_conj_atendido_w		number(15);
qt_retorno_w			number(10);
nr_seq_conjunto_w		number(10);

begin

select	nr_seq_conjunto
into	nr_seq_conjunto_w
from	cm_requisicao_item
where	nr_sequencia = nr_seq_item_req_p;

select	cd_setor_atendimento,
	cd_estabelecimento
into	cd_setor_atendimento_w,
	cd_estabelecimento_w
from	cm_requisicao
where	nr_sequencia = nr_requisicao_p;

select	count(*),
	nvl(max(qt_conjunto),0),
	nvl(max(ie_periodo),'D')
into	qt_regra_w,
	qt_conjunto_regra_w,
	ie_periodo_w
from	cm_conjunto_cota
where	nr_seq_conjunto 		= nr_seq_conjunto_w
and	cd_estabelecimento 	= cd_estabelecimento_w
and	cd_setor_atendimento	= cd_setor_atendimento_w
and	ie_situacao = 'A';

if	(qt_regra_w > 0) then
	begin

	if	(ie_periodo_w = 'D') then
		begin
		
		select	count(*)
		into	qt_conj_atendido_w
		from	cm_requisicao_conj a,
			cm_conjunto_cont b,
			cm_requisicao_item c,
			cm_requisicao d
		where	trunc(a.dt_atendimento) = trunc(sysdate)
		and	b.nr_sequencia = a.nr_seq_conj_real
		and	c.nr_sequencia = a.nr_seq_item_req
		and	d.nr_sequencia = c.nr_seq_requisicao
		and	nvl(b.ie_situacao,'A') = 'A'
		and	b.nr_seq_conjunto = nr_seq_conjunto_w
		and	d.cd_setor_atendimento = cd_setor_atendimento_w
		and	d.cd_estabelecimento = cd_estabelecimento_w;

		if	((qt_conjunto_regra_w - (qt_conj_atendido_w + nvl(qt_conjunto_p,0))) < 1) then
			qt_retorno_w	:= 0;
		else
			qt_retorno_w	:= (qt_conjunto_regra_w - (qt_conj_atendido_w + nvl(qt_conjunto_p,0)));
		end if;

		end;
	elsif	(ie_periodo_w = 'M') then
		begin

		select	count(*)
		into	qt_conj_atendido_w
		from	cm_requisicao_conj a,
			cm_conjunto_cont b,
			cm_requisicao_item c,
			cm_requisicao d
		where	trunc(a.dt_atendimento,'mm') = trunc(sysdate,'mm')
		and	b.nr_sequencia = a.nr_seq_conj_real
		and	c.nr_sequencia = a.nr_seq_item_req
		and	d.nr_sequencia = c.nr_seq_requisicao
		and	nvl(b.ie_situacao,'A') = 'A'
		and	b.nr_seq_conjunto = nr_seq_conjunto_w
		and	d.cd_setor_atendimento = cd_setor_atendimento_w
		and	d.cd_estabelecimento = cd_estabelecimento_w;

		if	((qt_conjunto_regra_w - (qt_conj_atendido_w + nvl(qt_conjunto_p,0))) < 1) then
			qt_retorno_w	:= 0;
		else
			qt_retorno_w	:= (qt_conjunto_regra_w - (qt_conj_atendido_w + nvl(qt_conjunto_p,0)));
		end if;

		end;
	end if;
	end;
end if;

return qt_retorno_w;

end cm_obter_qt_cota_disp;
/