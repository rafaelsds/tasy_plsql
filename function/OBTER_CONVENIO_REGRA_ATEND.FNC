create or replace
function obter_convenio_regra_atend(		cd_medico_p		varchar2,
					cd_convenio_p		number,
					ie_tipo_atendimento_p	number,
					cd_estabelecimento_p	number,
					ie_atend_exec_p		varchar2,
					cd_categoria_p		varchar2,
					cd_plano_convenio_p	varchar2)
					return varchar2 is

ie_atendimento_w		varchar2(1);
ie_execucao_w		varchar2(1);
ie_exec_partic_w		varchar2(1);	
ie_tipo_atendimento_w	number(3);
ie_retorno_w		varchar2(1) := 'S';
ie_existe_w		number(10);
ie_credenciado_w		varchar2(01);

cursor c01 is
	select	ie_atendimento,
		ie_tipo_atendimento
	from	convenio_regra_medico
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio		= cd_convenio_p
	and	ie_atendimento	= 'S'
	and	(ie_tipo_atendimento	= ie_tipo_atendimento_p or ie_tipo_atendimento is null)
	order by ie_tipo_atendimento desc;

cursor c02 is
	select	ie_execucao,
		ie_tipo_atendimento
	from	convenio_regra_medico
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio		= cd_convenio_p
	and	ie_execucao		= 'S'
	and	(ie_tipo_atendimento	= ie_tipo_atendimento_p or ie_tipo_atendimento is null)
	order by ie_tipo_atendimento desc;
	
cursor c03 is
	select	ie_exec_partic,
		ie_tipo_atendimento
	from	convenio_regra_medico
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio		= cd_convenio_p
	and	ie_exec_partic		= 'S'
	and	(ie_tipo_atendimento	= ie_tipo_atendimento_p or ie_tipo_atendimento is null)
	order by ie_tipo_atendimento desc;

begin

select	obter_se_medico_credenciado(cd_estabelecimento_p, cd_medico_p, cd_convenio_p, null, null,cd_categoria_p,null,cd_plano_convenio_p,null,null,null,null)
into	ie_credenciado_w
from	dual;

if	(ie_atend_exec_p = 'A') then
	open c01;
		loop
		fetch c01 into
			ie_atendimento_w,
			ie_tipo_atendimento_w;
		exit when c01%notfound;
		
		if	(ie_tipo_atendimento_w is null) or
			(ie_tipo_atendimento_w = ie_tipo_atendimento_p) then
			if	(ie_credenciado_w = 'N') then
				ie_retorno_w	:= 'N';
			end if;
		end if;

		end loop;
	close c01;	

elsif	(ie_atend_exec_p = 'E') then
	open c02;
		loop
		fetch c02 into
			ie_execucao_w,
			ie_tipo_atendimento_w;
		exit when c02%notfound;
		
		if	(ie_tipo_atendimento_w is null) or
			(ie_tipo_atendimento_w = ie_tipo_atendimento_p) then
			if	(ie_credenciado_w = 'N') then
				ie_retorno_w	:= 'N';
			end if;
		end if;
		
		end loop;
	close c02;	
	
elsif	(ie_atend_exec_p = 'P') then
	open c03;
		loop
		fetch c03 into	
			ie_exec_partic_w,
			ie_tipo_atendimento_w;
		exit when c03%notfound;
		if	(ie_tipo_atendimento_w is null) or
			(ie_tipo_atendimento_w = ie_tipo_atendimento_p) then
			if	(ie_credenciado_w = 'N') then
				ie_retorno_w	:= 'N';
			end if;
		end if;
			
		end loop;
	close c03;
	
end if;


return ie_retorno_w;

end obter_convenio_regra_atend;
/