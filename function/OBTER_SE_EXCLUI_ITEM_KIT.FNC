create or replace
function Obter_se_exclui_item_kit(	nr_prescricao_p			number,
									nr_seq_recomendacao_p	number,
									cd_recomendacao_p		number,
									cd_material_p			number,
									cd_kit_p				number,
									nr_seq_rec_cpoe_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1);
			
begin

select	nvl(max(ie_excluir),'N')
into	ds_retorno_w
from	w_kit_modificado
where	((nr_prescricao = nr_prescricao_p) or
		 ((nr_prescricao is null) and
		  (nr_seq_rec_cpoe = nr_seq_rec_cpoe_p)))
and	cd_recomendacao = cd_recomendacao_p
and	cd_material = cd_material_p
and	cd_kit = cd_kit_p;

return	ds_retorno_w;

end Obter_se_exclui_item_kit;
/
