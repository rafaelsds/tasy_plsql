create or replace
function Obter_se_possui_hor_adm(	nr_prescricao_p		number,
					nr_seq_item_p		number,
					ie_tipo_item_p		varchar2)
					return varchar2 is

ie_possui_w		varchar2(1)	:= 'N';
			
begin

if	(ie_tipo_item_p	in ('IAG','IA','MAT','M','SNE','S','LD')) then

	select	nvl(max('S'),'N')
	into	ie_possui_w
	from	prescr_mat_hor
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_material = nr_seq_item_p
	and	dt_fim_horario	is not null;
	
elsif	(ie_tipo_item_p	in ('G','C','HM','I','P','L')) then

	select	nvl(max('S'),'N')
	into	ie_possui_w
	from	prescr_proc_hor
	where	nr_prescricao 		= nr_prescricao_p
	and	nr_seq_procedimento 	= nr_seq_item_p
	and	dt_fim_horario		is not null;	
	
elsif	(ie_tipo_item_p	in ('DI','SOL')) then

	select	nvl(max('S'),'N')
	into	ie_possui_w
	from	prescr_mat_hor
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_solucao	= nr_seq_item_p
	and	dt_fim_horario	is not null;
	
elsif	(ie_tipo_item_p	in ('D')) then

	select	nvl(max('S'),'N')
	into	ie_possui_w
	from	prescr_dieta_hor
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_dieta	= nr_seq_item_p
	and	dt_fim_horario	is not null;
	
elsif	(ie_tipo_item_p	in ('R')) then

	select	nvl(max('S'),'N')
	into	ie_possui_w
	from	prescr_rec_hor
	where	nr_prescricao		= nr_prescricao_p
	and	nr_seq_recomendacao	= nr_seq_item_p
	and	dt_fim_horario		is not null;
	
end if;	

return	ie_possui_w;

end Obter_se_possui_hor_adm;
/