Create or replace
function obter_tuss_brasindice(     	cd_apresentacao_p       varchar2,
                                        cd_laboratorio_p        varchar2,
                                        cd_medicamento_p        varchar2)
                                return varchar2 is

ds_retorno_w		varchar2(15);
qt_registro_w		number(5);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin
cd_estabelecimento_w := obter_estabelecimento_ativo;

begin
select	1
into	qt_registro_w
from	brasindice_preco
where	cd_medicamento		= cd_medicamento_p
and	cd_laboratorio		= cd_laboratorio_p
and	cd_apresentacao		= cd_apresentacao_p
and	cd_estabelecimento	= cd_estabelecimento_w;
exception
when others then
	qt_registro_w := 0;
end;

if	(qt_registro_w = 0) then
	select  nvl(max(cd_tuss),0)
	into    ds_retorno_w
	from    brasindice_preco
	where   cd_medicamento  = cd_medicamento_p
	and     cd_laboratorio  = cd_laboratorio_p
	and     cd_apresentacao = cd_apresentacao_p
	and	dt_inicio_vigencia	= (	select	max(dt_inicio_vigencia)
						from	brasindice_preco
						where	cd_medicamento		= cd_medicamento_p
						and	cd_laboratorio		= cd_laboratorio_p
						and	cd_apresentacao		= cd_apresentacao_p);
else
	select  nvl(max(cd_tuss),0)
	into    ds_retorno_w
	from    brasindice_preco
	where   cd_medicamento  = cd_medicamento_p
	and     cd_laboratorio  = cd_laboratorio_p
	and     cd_apresentacao = cd_apresentacao_p
	and	nvl(cd_estabelecimento, cd_estabelecimento_w)	= cd_estabelecimento_w
	and	dt_inicio_vigencia	= (	select	max(dt_inicio_vigencia)
						from	brasindice_preco
						where	cd_medicamento		= cd_medicamento_p
						and	cd_laboratorio		= cd_laboratorio_p
						and	cd_apresentacao		= cd_apresentacao_p
						and	nvl(cd_estabelecimento, cd_estabelecimento_w)	= cd_estabelecimento_w);
end if;

return ds_retorno_w;
end;
/