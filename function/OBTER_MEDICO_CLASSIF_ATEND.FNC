create or replace
function obter_medico_classif_atend(	nr_seq_classif_medico_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(80);

begin
if	(nr_seq_classif_medico_p is not null) then

	select	max(ds_classificacao)
	into	ds_retorno_w
	from	medico_classif_atend
	where	nr_sequencia = nr_seq_classif_medico_p;


end if;

return	ds_retorno_w;

end obter_medico_classif_atend;
/