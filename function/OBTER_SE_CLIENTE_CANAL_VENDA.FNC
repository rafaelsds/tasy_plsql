create or replace
function obter_se_cliente_canal_venda (
		nr_seq_cliente_p	number)
		return varchar2 is
		
ie_canal_venda_w	varchar2(1) := 'N';
		
begin
if	(nr_seq_cliente_p is not null) then
	begin
	select	decode(count(*),0,'N','S')
	into	ie_canal_venda_w
	from	com_canal_cliente
	where	nr_seq_cliente = nr_seq_cliente_p
	and	ie_tipo_atuacao = 'V'
	and	dt_fim_atuacao is null;	
	end;
end if;
return ie_canal_venda_w;
end obter_se_cliente_canal_venda;
/