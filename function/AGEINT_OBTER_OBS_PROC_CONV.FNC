create or replace
function Ageint_Obter_Obs_Proc_Conv	(cd_convenio_p			number,
					nr_seq_proc_interno_p		number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number,
					cd_funcao_p			number,
					cd_categoria_p			varchar2,
					cd_plano_p			varchar2,
					cd_pessoa_fisica_p		varchar2)
					return varchar2 is

ds_orientacao_w	varchar2(4000) := '';
ds_orient_convenio_w	varchar2(2000) := '';
ds_orientacao_usuario_w	varchar2(2000) := '';
ie_orientacao_w		varchar2(01);
ie_tipo_orientacao_w	varchar2(01);
qt_idade_w		number(5);

begin

Obter_Param_Usuario(820,67,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_tipo_orientacao_w);

if	(nvl(nr_seq_proc_interno_p,0) > 0) then
	select	max(ie_orientacao),
		max(ds_orientacao_pac),
		max(ds_orientacao_usuario)
	into	ie_orientacao_w,
		ds_orientacao_w,
		ds_orientacao_usuario_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_p;

	qt_idade_w	:= obter_idade_pf(cd_pessoa_fisica_p, sysdate,'A');
	
	select	max(ds_orientacao)
	into	ds_orient_convenio_w
	from	proc_interno_conv_orient
	where	nr_seq_proc_interno = nr_seq_proc_interno_p
	and	((cd_convenio = cd_convenio_p) or (cd_convenio is null))
	and	nvl(cd_categoria, cd_categoria_p) 	= cd_categoria_p
	and	nvl(cd_plano, cd_plano_p)		= cd_plano_p
	and	((((qt_idade_w >= qt_idade_min) or (qt_idade_min is null))
	and	((qt_idade_w <= qt_idade_max) or (qt_idade_max is null)))
	or	(cd_pessoa_fisica_p is null));

	--ation_error(-20011,cd_funcao_w || '#@#@');
	if	(ie_orientacao_w = 'C') then
		ds_orientacao_w := ds_orient_convenio_w;
	elsif	(ie_orientacao_w = 'A') and ((ie_tipo_orientacao_w = 'P') or (cd_funcao_p <> 820)) then
		ds_orientacao_w := ds_orientacao_w || chr(13) || chr(10) || ds_orient_convenio_w;
	elsif	(ie_orientacao_w = 'A') and (ie_tipo_orientacao_w = 'U') and (cd_funcao_p = 820) then
		ds_orientacao_w := ds_orientacao_usuario_w || chr(13) || chr(10) || ds_orient_convenio_w;
	elsif	(ie_tipo_orientacao_w = 'U') and (cd_funcao_p = 820) then
		ds_orientacao_w := ds_orientacao_usuario_w;
	end if;
end if;

return ds_orientacao_w;

end Ageint_Obter_Obs_Proc_Conv;
/
