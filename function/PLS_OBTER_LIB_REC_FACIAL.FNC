create or replace
function pls_obter_lib_rec_facial(	nr_seq_segurado_p		number,
					nr_seq_usuario_web_p		number,
					ie_tipo_liberacao_p		varchar2,
					ie_tipo_guia_p			varchar2,
					ie_funcao_liberada_p		number,
					nr_seq_prestador_p		number)  
					return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Validar se existe regra de liberacao para reconhecimento facial.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ x ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [ ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_regra_lib_w	pls_regra_lib_req_web.nr_sequencia%type;	
nr_contrato_w		varchar2(255);
dt_inicio_vigencia_w	varchar2(255);
nr_seq_seg_carteira_w	pls_segurado_carteira.nr_sequencia%type;
dt_emissao_w		date;
ie_data_valida_w	varchar2(1);
nr_seq_congenere_w	pls_congenere.nr_sequencia%type;
nr_seq_intercambio_w	pls_intercambio.nr_sequencia%type;
nr_seq_plano_w		pls_plano.nr_sequencia%type;	
ie_tipo_segurado_w	pls_segurado.ie_tipo_segurado%type;
ie_preco_w		varchar2(255);
ds_retorno_w		varchar2(1)	:= 'N';

begin

begin
	select	max(nr_sequencia)
	into	nr_seq_seg_carteira_w
	from	pls_segurado_carteira
	where	nr_seq_segurado = nr_seq_segurado_p;
	
	select	dt_solicitacao
	into	dt_emissao_w
	from	pls_segurado_carteira
	where	nr_sequencia = nr_seq_seg_carteira_w
	and	ie_situacao  = 'P'
	and	nr_seq_regra_via is not null;
	
	if	(dt_emissao_w is not null) then
		ie_data_valida_w	:= 'S';
		dt_emissao_w		:= trunc(dt_emissao_w);
	end if;	
exception
when others then
	dt_emissao_w := null;
end;

begin
	select	nr_seq_congenere,
		nr_seq_intercambio,
		ie_tipo_segurado,
		pls_obter_dados_segurado(nr_sequencia, 'NC'),
		pls_obter_dados_segurado(nr_sequencia, 'IV'),
		(	select	ie_preco	
			from	pls_plano
			where	nr_sequencia	= nr_seq_plano)
	into	nr_seq_congenere_w,
		nr_seq_intercambio_w,		
		ie_tipo_segurado_w,
		nr_contrato_w,
		dt_inicio_vigencia_w,
		ie_preco_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
exception
when others then
	nr_seq_congenere_w	:= 0;
	nr_seq_intercambio_w	:= 0;
end;

if	(ie_funcao_liberada_p is not null) then
	select 	max(nr_sequencia)
	into	nr_seq_regra_lib_w
	from	pls_regra_lib_req_web
	where 	ie_situacao					= 'A'
	and	ie_tipo_liberacao				= ie_tipo_liberacao_p
	and	((nr_seq_segurado 		is null)	or (nr_seq_segurado		= nr_seq_segurado_p))
	and	((nr_seq_contrato 		is null)	or (nr_seq_contrato		= nr_contrato_w))
	and	((nr_seq_usuario_web		is null)	or (nr_seq_usuario_web		= nr_seq_usuario_web_p))
	and	((ie_tipo_guia 			is null) 	or (ie_tipo_guia 		= ie_tipo_guia_p))
	and	((ie_funcao_liberada 		is null) 	or (ie_funcao_liberada		= ie_funcao_liberada_p))
	and	((nr_seq_congenere 		is null)	or (nr_seq_congenere		= nr_seq_congenere_w))
	and	((ie_preco			is null)	or (ie_preco			= ie_preco_w))
	and	((dt_ant_emissao_carteira	is null)	or (dt_ant_emissao_carteira	>= to_date(dt_inicio_vigencia_w)))
	and	((nr_seq_contrato_int		is null)	or (nr_seq_contrato_int		= nr_seq_intercambio_w))
	and	((qt_dias_emissao		is null)	or ((ie_data_valida_w = 'S') and (nvl(dt_emissao_w,sysdate) + qt_dias_emissao >= sysdate)))
	and	sysdate between dt_inicio_vigencia 		and nvl(dt_fim_vigencia,sysdate)
	and	((ie_tipo_segurado is null) or (ie_tipo_segurado = ie_tipo_segurado_w))
	and	((nr_seq_prestador	is null)	or ((nr_seq_prestador_p is not null) and (nr_seq_prestador = nr_seq_prestador_p)));
	
	if	(nr_seq_regra_lib_w is not null) then
		ds_retorno_w :=	'S';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_lib_rec_facial;
/