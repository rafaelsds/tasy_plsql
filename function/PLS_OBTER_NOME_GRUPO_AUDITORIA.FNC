create or replace
function pls_obter_nome_grupo_auditoria
		(	nr_seq_grupo_auditoria_p		Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(120);			
nr_seq_grupo_w		number(10);

begin

select	nr_seq_grupo
into	nr_seq_grupo_w
from	PLS_AUDITORIA_GRUPO
where	nr_sequencia = nr_seq_grupo_auditoria_p;

select	nvl(max(nm_grupo_auditor),'')
into	ds_retorno_w
from	pls_grupo_auditor
where	nr_sequencia	= nr_seq_grupo_w;


return	ds_retorno_w;

end pls_obter_nome_grupo_auditoria;
/