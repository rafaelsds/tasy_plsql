create or replace
function obter_cotacao_moeda_fin (	cd_moeda_p	number,
					dt_cotacao_p	date)
	return number is
	
vl_cotacao_w	cotacao_moeda.vl_cotacao%type;
qt_cotacao_w	number(15,4);
	
begin
	if (cd_moeda_p is not null) then
		begin
					select 	a.vl_cotacao
					into 	vl_cotacao_w
					from 	cotacao_moeda a
					where 	a.cd_moeda = cd_moeda_p
					and 	to_char(a.dt_cotacao,'dd/mm/yyyy') = to_char(nvl(dt_cotacao_p,sysdate),'dd/mm/yyyy')
					and 	a.dt_cotacao = (select max(b.dt_cotacao)
											from cotacao_moeda b
											where b.cd_moeda = cd_moeda_p
											and to_char(b.dt_cotacao,'dd/mm/yyyy') = to_char(nvl(dt_cotacao_p,sysdate),'dd/mm/yyyy'));
		end;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(339594);
	end if;
	
	return vl_cotacao_w;
	
end obter_cotacao_moeda_fin;
/