CREATE OR REPLACE FUNCTION OBTER_NRS_SEQ_INTERNO_LAUDO(
      nr_laudo_p INTEGER)
    RETURN VARCHAR2
  IS
    ds_retorno_w     VARCHAR2(4000);
    nr_seq_interno_w VARCHAR2(255);
    CURSOR c01
    IS
      SELECT prescr_proc.nr_seq_interno
      FROM procedimento_paciente proc_pac,
        prescr_procedimento prescr_proc
      WHERE prescr_proc.nr_prescricao = proc_pac.nr_prescricao
      AND prescr_proc.nr_sequencia    = proc_pac.nr_sequencia_prescricao
      AND proc_pac.nr_laudo           = nr_laudo_p;
    nr_seq_interno_linha_w c01%ROWTYPE;
  BEGIN
    OPEN c01;
    LOOP
      FETCH c01 INTO nr_seq_interno_linha_w;
      EXIT
    WHEN c01%NOTFOUND;
      BEGIN
        ds_retorno_w := ds_retorno_w || nr_seq_interno_linha_w.nr_seq_interno || ',' ;
      END;
    END LOOP;
    CLOSE c01;
    RETURN ds_retorno_w;
  END OBTER_NRS_SEQ_INTERNO_LAUDO;
/