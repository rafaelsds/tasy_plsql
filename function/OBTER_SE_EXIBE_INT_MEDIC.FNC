create or replace 
function obter_se_exibe_int_medic(	nr_prescricao_p		number,
					ds_severidades_p	varchar2,
					cd_material_p		Number) return varchar2 is
						
ds_retorno_w	char(1) := 'N';
begin

if	(nr_prescricao_p is not null) then
	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	prescr_medica_interacao
	where	rownum = 1
	and	nr_prescricao	= nr_prescricao_p
	and	obter_se_valor_contido(ie_Severidade,ds_severidades_p) = 'S'
	and	((cd_material_p is null) or (cd_material_p in (cd_material_interacao, cd_material)));
	
	
	if	(ds_retorno_w = 'N') then
		
		if	(cd_material_p is null) then
	
				select nvl(max('S'),'N')
				into	 ds_retorno_w
				from	 prescr_material a,
						 prescr_dieta b
				where  a.nr_prescricao = b.nr_Prescricao
				and	 a.nr_prescricao = nr_Prescricao_p
				and	 a.dt_suspensao is null
				and	 exists( select 1 
									from medic_interacao_alimento c
									where c.cd_material = a.cd_material
									and	((c.cd_dieta is null) or 
									       (c.cd_dieta = b.cd_dieta)));
		else
			select nvl(max('S'),'N')
			into	 ds_retorno_w
			from	 medic_interacao_alimento a
			where	 cd_material	= cd_material_p
			and	 ((cd_dieta is null) or (exists (select	1
													from	prescr_dieta b
													where	b.nr_Prescricao	= nr_prescricao_p
													and	b.dt_suspensao is null
													and		b.cd_dieta		= a.cd_dieta)));
		end if;
	end if;
end if;

return ds_retorno_w;

end obter_se_exibe_int_medic;
/
