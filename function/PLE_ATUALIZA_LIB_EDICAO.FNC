Create or Replace
Function ple_atualiza_lib_edicao(
			nr_seq_edicao_p		Number,
			cd_perfil_p		Number,
			nm_usuario_lib_p	Varchar2)
			return Varchar2 is

ie_atualizacao_w	Varchar2(1);

BEGIN

select	nvl(max(ie_atualizacao),'S')
into	ie_atualizacao_w
from	ple_edicao_liberacao
where	nr_seq_edicao = nr_seq_edicao_p
and	((cd_perfil = cd_perfil_p) or
	(nm_usuario_lib = nm_usuario_lib_p));

RETURN ie_atualizacao_w;

END ple_atualiza_lib_edicao;
/