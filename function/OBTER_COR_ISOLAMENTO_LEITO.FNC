create or replace
function obter_cor_isolamento_leito (cd_perfil_p		number,
					cd_estabelecimento_p	number)
 		    	return varchar2 is
				
ds_cor_fundo_w		varchar2(50);
ds_cor_fonte_w		varchar2(50);
ds_cor_selecao_w	varchar2(50);
begin

tasy_obter_cor(10361, cd_perfil_p, cd_estabelecimento_p, ds_cor_fundo_w, ds_cor_fonte_w, ds_cor_selecao_w);

return converte_Cor_Delphi_RGB(ds_cor_fundo_w);

end obter_cor_isolamento_leito;
/