create or replace function obter_metodo_pago(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(2000);
nr_count_w	number(10);
cd_tipo_recebimento_w   varchar2(10);

cursor C01 is
select  adiciona_zeros_esquerda(b.cd_tipo_recebimento,2)
from	nota_fiscal a,
        fis_metodo_pagamento b
where	a.nr_sequencia = b.nr_seq_nota
and     a.nr_sequencia = nr_sequencia_p
order by b.nr_sequencia;

begin

ds_retorno_w := '';
nr_count_w := 0;

open	c01;
loop
fetch	c01 into
	cd_tipo_recebimento_w;
exit	when c01%notfound;
	begin
	nr_count_w := nr_count_w + 1;

	if (nr_count_w = 1) then
		ds_retorno_w := ds_retorno_w || to_char(cd_tipo_recebimento_w);
	else
		ds_retorno_w := ds_retorno_w ||','|| to_char(cd_tipo_recebimento_w);
	end if;

	end;
	end loop;
	close c01;

return	ds_retorno_w;

end obter_metodo_pago;
/