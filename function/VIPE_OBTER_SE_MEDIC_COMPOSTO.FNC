create or replace
function vipe_obter_se_medic_composto(	nr_prescricao_p	number,
					nr_seq_material_p	number,
					nr_agrupamento_p	number,
					ie_opcao_p	varchar2)
					return 		varchar2 is
					
ie_medic_composto_w	varchar2(1);
ds_retorno_w		varchar2(255);
nr_seq_ficha_tecnica_w	number(10,0);
cont_w			number(10,0);
					
begin 
if	(nr_prescricao_p is not null) and
	(nr_seq_material_p is not null) and
	(nr_agrupamento_p is not null) then
	
	if	(ie_opcao_p = 'A') then
		select	decode(count(*),0,'','S')
		into	ie_medic_composto_w
		from	prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	<> nr_seq_material_p
		and	nr_agrupamento	= nr_agrupamento_p
		and	ie_agrupador	= 1
		and	cd_kit_material is null
		and	nr_seq_substituto is null;
		
		ds_retorno_w	:= ie_medic_composto_w;
	elsif	(ie_opcao_p = 'P') then
		select	max(a.nr_seq_ficha_tecnica)
		into	nr_seq_ficha_tecnica_w
		from	material a,
			prescr_material b
		where	a.cd_material	= b.cd_material
		and	b.nr_prescricao	= nr_prescricao_p
		and	b.nr_sequencia	= nr_seq_material_p;
		
		select	decode(count(*),0,'','P')
		into	ie_medic_composto_w
		from	material b,
			prescr_material a
		where	a.cd_material		= b.cd_material
		and	a.nr_prescricao		= nr_prescricao_p
		and	a.nr_sequencia		<> nr_seq_material_p
		and	a.nr_agrupamento	= nr_agrupamento_p
		and	b.nr_seq_ficha_tecnica	= nr_seq_ficha_tecnica_w
		and	a.ie_agrupador		= 1
		and	a.cd_kit_material 	is null;
		
		if	(ie_medic_composto_w is null) then
			select	decode(count(*),0,'','S')
			into	ie_medic_composto_w
			from	prescr_material
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	<> nr_seq_material_p
			and	nr_agrupamento	= nr_agrupamento_p
			and	ie_agrupador	= 1
			and	cd_kit_material is null;
		end if;
		
		ds_retorno_w	:= ie_medic_composto_w;
	elsif	(ie_opcao_p = 'D') then
		ds_retorno_w	:= '';
		select	max(a.nr_seq_ficha_tecnica)
		into	nr_seq_ficha_tecnica_w
		from	material a,
			prescr_material b
		where	a.cd_material	= b.cd_material
		and	b.nr_prescricao	= nr_prescricao_p
		and	b.nr_sequencia	= nr_seq_material_p;

		if (obter_se_medic_do_paciente(nr_prescricao_p,nr_seq_material_p) = 'S') then
		
			select	count(*)
			into	cont_w
			from	material b,
				prescr_material a
			where	a.cd_material		= b.cd_material
			and	a.nr_prescricao		= nr_prescricao_p
			and	a.nr_sequencia		<> nr_seq_material_p
			and	a.nr_agrupamento	= nr_agrupamento_p
			and	b.nr_seq_ficha_tecnica	= nr_seq_ficha_tecnica_w
			and	a.ie_agrupador		= 1
			and	a.cd_kit_material 	is null;
			
			if	(cont_w > 0) then
				select	substr(max(nvl(decode(a.ds_medic_nao_padrao,null,null,a.ds_medic_nao_padrao || ' ' || wheb_mensagem_pck.get_texto(494867)), b.ds_reduzida)),1,254)
				into	ds_retorno_w
				from	material b,
					prescr_material a
				where	a.cd_material		= b.cd_material
				and	a.nr_prescricao		= nr_prescricao_p
				and	a.nr_sequencia		= nr_seq_material_p;
			end if;
			
			if	(ds_retorno_w is null) then
				select	substr(max(decode(a.ds_medic_nao_padrao,null,null,a.ds_medic_nao_padrao || ' ' || wheb_mensagem_pck.get_texto(494867))),1,254)
				into	ds_retorno_w
				from	material b,
					prescr_material a
				where	a.cd_material		= b.cd_material
				and	a.nr_prescricao		= nr_prescricao_p
				and	a.nr_sequencia		= nr_seq_material_p;

				if	(ds_retorno_w is null) then
					select	substr(max(nvl(decode(nvl(a.ie_medicacao_paciente, 'N') , 'N',null, b.ds_material || ' ' || wheb_mensagem_pck.get_texto(494867)), b.ds_material)),1,254)
					into	ds_retorno_w
					from	material b,
						prescr_material a
					where	a.cd_material		= b.cd_material
					and	a.nr_prescricao		= nr_prescricao_p
					and	a.nr_sequencia		= nr_seq_material_p;
				end if;
			end if;
		
		else
		
			select	substr(max(nvl(a.ds_medic_nao_padr, b.ds_material)),1,254)
			into	ds_retorno_w
			from	material b,
					prescr_material a
			where	a.cd_material	= b.cd_material
			and		a.nr_prescricao	= nr_prescricao_p
			and		a.nr_sequencia	= nr_seq_material_p;
		
		end if;
	end if;
end if;
ds_retorno_w	:= substr(ds_retorno_w,1,240);
return ds_retorno_w;

end VIPE_OBTER_SE_MEDIC_COMPOSTO;
/