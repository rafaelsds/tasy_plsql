Create or Replace
Function  Obter_Sim_Valor_Conta(nr_interno_conta_p	number,
				nr_seq_sim_p		number,
				ie_opcao_p		number)
				   	return number is

/* ie_opcao 		0 - Valor Conta
			1 - Valor Procedimentos
			2 - Valor Materiais
			3 - Valor Custo
*/


vl_item_w			number(15,2);
vl_procedimento_w		number(15,2);
vl_material_w			number(15,2);
vl_resultado_w			number(15,2) := 0;
vl_custo_w			number(15,2);
vl_custo_ant_w			number(15,2);
ie_exibir_custos		varchar2(1);

BEGIN

ie_exibir_custos := obter_valor_param_usuario(1800, 9, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

select 	nvl(sum(vl_material + vl_procedimento),0),
	nvl(sum(vl_procedimento),0),
	nvl(sum(vl_material),0),
	nvl(sum(vl_custo),0),
	nvl(sum(vl_custo_resumo),0)
into	vl_item_w,
	vl_procedimento_w,
	vl_material_w,
	vl_custo_w,
	vl_custo_ant_w
from    sim_valor
where   nr_interno_conta = nr_interno_conta_p
and 	nr_seq_sim	 = nr_seq_sim_p;

if	ie_opcao_p = 0 then
		vl_resultado_w := vl_item_w;
elsif	ie_opcao_p = 1 then
	vl_resultado_w := vl_procedimento_w;
elsif	ie_opcao_p = 2 then
	vl_resultado_w := vl_material_w;
elsif	ie_opcao_p = 3 then
	if (ie_exibir_custos = 'S') then
		vl_resultado_w := vl_custo_w;
	else 
		vl_resultado_w := 0; 
	end if;
elsif	ie_opcao_p = 4 then
	if (ie_exibir_custos = 'S') then
		vl_resultado_w := vl_custo_ant_w; 
	else
		vl_resultado_w := 0;
	end if;
end if;

return nvl(vl_resultado_w,0);

end Obter_Sim_Valor_Conta;
/
