create or replace
function obter_tipo_publicacao (nr_seq_tipo_public_p	number)
	return varchar2 is

ds_tipo_publicacao_w	varchar2(80);

begin
if	(nr_seq_tipo_public_p is not null) then
	begin
	select	max(ds_tipo_publicacao)
	into	ds_tipo_publicacao_w
	from	tipo_publicacao
	where	nr_sequencia = nr_seq_tipo_public_p;
	end;
end if;
return ds_tipo_publicacao_w;
end obter_tipo_publicacao;
/