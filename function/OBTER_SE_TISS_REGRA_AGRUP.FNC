create or replace
function obter_se_tiss_regra_agrup(nr_seq_procedimento_p number)
				return varchar2 is

    cd_grupo_proc_w          tiss_regra_agrup.cd_grupo_proc%type;
    cd_area_procedimento_w   tiss_regra_agrup.cd_area_procedimento%type;
    cd_especialidade_w       tiss_regra_agrup.cd_especialidade%type;
    cd_procedimento_w        tiss_regra_agrup.cd_procedimento%type;
    cd_estabelecimento_w     number(10);
    cd_convenio_w            number(10);
    ie_tipo_atendimento_w    tiss_regra_agrup.ie_tipo_atendimento%type;
    ie_tipo_atend_conta_w    tiss_regra_agrup.ie_tipo_atend_conta%type;
    ie_agrupar_w             tiss_regra_agrup.ie_agrupar%type;

cursor c01 is
select	ie_agrupar
from	tiss_regra_agrup
where	cd_estabelecimento						= cd_estabelecimento_w
and	cd_convenio						= cd_convenio_w
and	nvl(cd_procedimento, nvl(cd_procedimento_w, -1))			= nvl(cd_procedimento_w, -1)
and	nvl(cd_grupo_proc, nvl(cd_grupo_proc_w, -1))			= nvl(cd_grupo_proc_w, -1)
and	nvl(cd_area_procedimento, nvl(cd_area_procedimento_w, -1))	= nvl(cd_area_procedimento_w, -1)
and	nvl(cd_especialidade, nvl(cd_especialidade_w, -1))		= nvl(cd_especialidade_w, -1)
and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_w, 0))		= nvl(ie_tipo_atendimento_w, 0)
and	nvl(ie_tipo_atend_conta, nvl(ie_tipo_atend_conta_w, 0))		= nvl(ie_tipo_atend_conta_w, 0)
order by nvl(cd_procedimento,0),
	nvl(cd_grupo_proc,0),
	nvl(cd_especialidade,0),
	nvl(cd_area_procedimento,0),
	nvl(ie_tipo_atend_conta,0),
	nvl(ie_tipo_atendimento,0);

begin

select	cd_procedimento,
	obter_grupo_procedimento(a.cd_procedimento, a.ie_origem_proced, 'C'),
	obter_area_procedimento(a.cd_procedimento, a.ie_origem_proced),
	obter_especialidade_proced(a.cd_procedimento, a.ie_origem_proced),
	b.cd_estabelecimento,
	b.cd_convenio_parametro,
	b.ie_tipo_atend_conta,
	c.ie_tipo_atendimento
into	cd_procedimento_w,
	cd_grupo_proc_w,
	cd_area_procedimento_w,
	cd_especialidade_w,
	cd_estabelecimento_w,
	cd_convenio_w,
	ie_tipo_atend_conta_w,
	ie_tipo_atendimento_w
from	conta_paciente b,
	procedimento_paciente a,
	atendimento_paciente c
where	b.nr_interno_conta	= a.nr_interno_conta
and	b.nr_atendimento	= c.nr_atendimento
and	a.nr_sequencia		= nr_seq_procedimento_p;

open c01;
loop
fetch c01 into
	ie_agrupar_w;
exit when c01%notfound;

end loop;
close c01;

if	(nvl(ie_agrupar_w, 'S') = 'S') then
	return	'S';
else
	return	'N';
end if;

end	obter_se_tiss_regra_agrup;
/