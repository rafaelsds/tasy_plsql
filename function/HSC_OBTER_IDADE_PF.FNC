create or replace
FUNCTION hsc_Obter_Idade_pf(dt_nascimento_p		date,
						dt_atual_p			date,
						ie_opcao_p			Varchar2)
		     RETURN 	Varchar2 IS

/* ie_opcao_p
	Customizacao
*/

ds_idade_w			Varchar2(50);
dt_rotina_w			Date;
dt_atual_w			Date;
dt_nascimento_w			Date;
qt_anos_w			Number(05,0)	:= 0;
qt_meses_w			Number(15,0)	:= 0;
qt_meses_ww			Number(15,0)	:= 0;
qt_dias_w			Number(10,0)	:= 0;
ds_dia_w			Varchar2(3);
ie_dia_zero_w			varchar2(1);

BEGIN

if	(dt_nascimento_p is null) then
	ds_idade_w := '';
else

	if	(dt_atual_p is null) then
		dt_atual_w		:= sysdate;
	else
		dt_atual_w		:= dt_atual_p;
	end if;

	if	(dt_nascimento_p is null) then
		dt_nascimento_w	:= sysdate;
	else
		dt_nascimento_w	:= dt_nascimento_p;
	end if;

	Obter_Param_Usuario(0,90,0,'',0,ie_dia_zero_w);
	if	(ie_dia_zero_w = 'S') then
		dt_nascimento_w	:= dt_nascimento_w - 1;
	end if;

	begin

	select	trunc(months_between(dt_atual_w, dt_nascimento_w))
	into	qt_meses_w
	from	dual;

	if	(qt_meses_w > 11) then
		qt_anos_w		:= trunc(qt_meses_w / 12);
	end if;
	qt_dias_w			:= to_number(to_char(dt_atual_w,'dd') - to_char(dt_nascimento_w,'dd'));
	if	(qt_dias_w < 0) then
		ds_dia_w		:= to_char(dt_nascimento_w,'dd');

		if	(to_char(last_day(add_months(dt_atual_w,-1)),'dd') < ds_dia_w) then
			ds_dia_w := to_char(last_day(add_months(dt_atual_w,-1)),'dd');
		end if;

		ds_dia_w := ds_dia_w || '/';

		qt_dias_w		:= obter_dias_entre_datas(to_date(ds_dia_w||to_char(add_months(dt_atual_w,-1),'mm/yyyy'),'dd/mm/yyyy'),dt_atual_w);
	end if;
	qt_meses_ww			:= qt_meses_w;
	qt_meses_w			:= qt_meses_w - (qt_anos_w * 12);
	end;


	if	((((((dt_atual_p-dt_nascimento_p)*1440)/60)/24)/360) > 1) then
		
			begin
			ds_idade_w			:= '';
			if	(qt_anos_w = 1) then
				ds_idade_w 		:= '1a';
			end if;
			if	(qt_anos_w > 1) then
				ds_idade_w 		:= qt_anos_w || 'a';
			end if;
			if 	(qt_meses_w > 0) then
				begin
				if	(qt_anos_w > 0) then
					ds_idade_w 	:= ds_idade_w || ' ';
				end if;
				if	(qt_meses_w = 1) then
					ds_idade_w	:= ds_idade_w || '1m';
				end if;
				if	(qt_meses_w > 1) then
					ds_idade_w	:= ds_idade_w || qt_meses_w || 'm';
				end if;
				end;
			end if;
			if	(qt_meses_w < 1) and (qt_anos_w < 1) then
				ds_idade_w	:= ds_idade_w || qt_meses_w || 'm';
			end if;
			end;
		else
			begin
			ds_idade_w			:= '';
			if 	(qt_meses_w > 0) then
				begin
				if	(qt_meses_w = 1) then
					ds_idade_w	:= ds_idade_w || '1m';
				end if;
				if	(qt_meses_w > 1) then
					ds_idade_w	:= ds_idade_w || qt_meses_w || 'm';
				end if;
				end;
			end if;
			if	(qt_meses_w < 1) then
				ds_idade_w	:= ds_idade_w || qt_meses_w || 'm';
			end if;
			if	(qt_dias_w > 0) then
				ds_idade_w	:= ds_idade_w ||' '||qt_dias_w||'d';
			end if;
			end;
		end if;
end if;

return ds_idade_w;
END hsc_Obter_Idade_pf;
/