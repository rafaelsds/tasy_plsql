create or replace 
FUNCTION com_obter_dados_produto(	nr_sequencia_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(80);
ds_produto_w		varchar2(80);

/* D - Descri��o */

begin
if	(nr_sequencia_p is not null) then
	select	ds_produto
	into	ds_produto_w
	from	com_canal_produto
	where	nr_sequencia = nr_sequencia_p;

	if	(ie_opcao_p = 'D') then
		ds_retorno_w := ds_produto_w;
	end if;
end if;

return ds_retorno_w;

end com_obter_dados_produto;
/