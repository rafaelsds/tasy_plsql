create or replace
function pls_format_dt_fim_relat_02425(tipo_data_p		number,
					data_p		date)
					return Date is
			
retorno_w	Date;			

/*
tipo_data_p:
1 e 4  -  retorna �ltimo dia do m�s    recebe XX/XX/XXXX
2 e 5  -  retorna �ltimo dia do ano    recebe 01/01/XXXX
*/

begin

if	(tipo_data_p in (1,4)) then --M�s/Per�odo M�s
	retorno_w := last_day(data_p);
elsif	(tipo_data_p in (2,5)) then --Anual/ Per�odo Anual
	retorno_w := (data_p + 364);
end if;

return	retorno_w;
end pls_format_dt_fim_relat_02425;
/