create or replace
function pls_mens_obter_qtd_pagador
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number)
 		    		return number is

qt_retorno_w			Number(10)	:= 0;
nr_contrato_w			number(10);
nr_dia_inicial_venc_w		Number(2);
nr_dia_final_venc_w		Number(2);
nr_seq_pagador_mens_w		Number(10);
nr_contrato_principal_w		Number(10);
nr_seq_grupo_contrato_w		Number(10);
nr_seq_empresa_w		Number(10);
nr_seq_forma_cobranca_lote_w	Number(10);
dt_mesano_referencia_w		date;
cd_banco_lote_w			number(5);
ie_tipo_lote_w			varchar2(2);
nr_contrato_intercambio_w	number(10);
dt_mesano_referencia_fimdia_w	date;
cd_tipo_portador_w		number(5);
cd_portador_w			number(10);
nr_seq_grupo_preco_w		number(10);
qt_w				number(1);
ie_tipo_contrato_intercambio_w	varchar2(2);
ie_pagador_beneficio_obito_w	varchar2(2);
nr_seq_grupo_inter_w		number(10);
ie_endereco_boleto_lote_w	varchar2(4);
ie_tipo_pessoa_pagador_w	varchar2(2);
nr_seq_classif_benef_w		pls_classificacao_benef.nr_sequencia%type;

Cursor C01 is /* Pagador ***Responsável por fazer o insert na pls_mensalidade *** */
	select	1
	from	pls_contrato_grupo		g,
		pls_grupo_contrato		f,
		pls_desc_empresa		e,
		pls_contrato_pagador_fin	d,
		pls_contrato			c,
		pls_segurado 			b,
		pls_contrato_pagador 		a,
		pls_preco_contrato		h,
		pls_preco_grupo_contrato	i
	where	a.nr_seq_contrato	= c.nr_sequencia
	and	b.nr_seq_pagador	= a.nr_sequencia
	and	d.nr_seq_pagador(+)	= a.nr_sequencia
	and	d.nr_seq_empresa	= e.nr_sequencia(+)
	and	g.nr_seq_grupo		= f.nr_sequencia(+)
	and	g.nr_seq_contrato(+)	= c.nr_sequencia
	and	h.nr_seq_contrato(+)	= c.nr_sequencia
	and	h.nr_seq_grupo		= i.nr_sequencia(+)
	and	c.cd_estabelecimento	= cd_estabelecimento_p
	and	((a.nr_sequencia	= nr_seq_pagador_mens_w and	nr_seq_pagador_mens_w is not null) or nr_seq_pagador_mens_w is null)
	and	((nr_dia_inicial_venc_w is null) or (d.dt_dia_vencimento >= nr_dia_inicial_venc_w))
		and	((nr_dia_final_venc_w is null) or (d.dt_dia_vencimento <= nr_dia_final_venc_w))
	and	(nr_contrato_w is null or ((c.nr_sequencia = nr_contrato_w) or (c.nr_contrato_principal = nr_contrato_w)))
	and	((f.nr_sequencia = nr_seq_grupo_contrato_w) or (nr_seq_grupo_contrato_w is null))
	and	((i.nr_sequencia = nr_seq_grupo_preco_w) or (nr_seq_grupo_preco_w is null))
	and	((cd_banco_lote_w is null) or (d.cd_banco = cd_banco_lote_w))
	and	((nr_seq_forma_cobranca_lote_w is null) or (d.nr_seq_forma_cobranca = nr_seq_forma_cobranca_lote_w))
	and	((e.nr_seq_empresa_superior = nr_seq_empresa_w or e.nr_sequencia = nr_seq_empresa_w) or	(nr_seq_empresa_w is null))
	and	((d.cd_tipo_portador = cd_tipo_portador_w) or (cd_tipo_portador_w is null))
	and	((d.cd_portador = cd_portador_w) or (cd_portador_w is null))
	and	nvl(d.dt_inicio_vigencia,dt_mesano_referencia_fimdia_w) <= dt_mesano_referencia_fimdia_w and ((d.dt_fim_vigencia >= dt_mesano_referencia_fimdia_w) or (d.dt_fim_vigencia is null))
	and	nr_contrato_intercambio_w is null
	and	((ie_tipo_lote_w	= 'A') or (ie_tipo_lote_w	= 'CO')) /* aaschlote 26/10/2010 - OS - 256840 Gerar mensalidade apenas dos tipos de lote Ambos ou Contratos da Operadora*/
	and	((ie_pagador_beneficio_obito_w = 'S' and a.nr_seq_regra_obito is not null) or (ie_pagador_beneficio_obito_w = 'N'))
	and	((a.ie_endereco_boleto = ie_endereco_boleto_lote_w) or (ie_endereco_boleto_lote_w is null))
	and	((ie_tipo_pessoa_pagador_w = 'A') or (ie_tipo_pessoa_pagador_w = 'PF' and a.cd_pessoa_fisica is not null) or (ie_tipo_pessoa_pagador_w = 'PJ' and a.cd_cgc is not null))
	and	((b.nr_seq_classificacao = nr_seq_classif_benef_w) or (nr_seq_classif_benef_w is null))
	UNION ALL
	/* ITEM */
	select	1
	from	pls_contrato_grupo		g,
		pls_grupo_contrato		f,
		pls_desc_empresa		e,
		pls_contrato_pagador_fin	d,
		pls_contrato			c,
		pls_segurado 			b,
		pls_contrato_pagador 		a,
		pls_pagador_item_mens		h,
		pls_preco_contrato		i,
		pls_preco_grupo_contrato	j
	where	a.nr_seq_contrato	= c.nr_sequencia
	and	b.nr_seq_pagador	= h.nr_seq_pagador
	and	d.nr_seq_pagador(+)	= a.nr_sequencia
	and	d.nr_seq_empresa	= e.nr_sequencia(+)
	and	g.nr_seq_grupo		= f.nr_sequencia(+)
	and	g.nr_seq_contrato(+)	= c.nr_sequencia
	and	h.nr_seq_pagador_item	= a.nr_sequencia
	and	i.nr_seq_contrato(+)	= c.nr_sequencia
	and	i.nr_seq_grupo		= j.nr_sequencia(+)
	and	c.cd_estabelecimento	= cd_estabelecimento_p
	and	((a.nr_sequencia	= nr_seq_pagador_mens_w and	nr_seq_pagador_mens_w is not null) or nr_seq_pagador_mens_w is null)
	and	((nr_dia_inicial_venc_w is null) or (d.dt_dia_vencimento >= nr_dia_inicial_venc_w))
		and	((nr_dia_final_venc_w is null) or (d.dt_dia_vencimento <= nr_dia_final_venc_w))
	and	(nr_contrato_w is null or ((c.nr_sequencia = nr_contrato_w) or (c.nr_contrato_principal = nr_contrato_w)))
	and	((f.nr_sequencia = nr_seq_grupo_contrato_w) or (nr_seq_grupo_contrato_w is null))
	and	((j.nr_sequencia = nr_seq_grupo_preco_w) or (nr_seq_grupo_preco_w is null))
	and	((cd_banco_lote_w is null) or (d.cd_banco = cd_banco_lote_w))
	and	((nr_seq_forma_cobranca_lote_w is null) or (d.nr_seq_forma_cobranca = nr_seq_forma_cobranca_lote_w))
	and	((e.nr_seq_empresa_superior = nr_seq_empresa_w or e.nr_sequencia = nr_seq_empresa_w) or	(nr_seq_empresa_w is null))
	and	((d.cd_tipo_portador = cd_tipo_portador_w) or (cd_tipo_portador_w is null))
	and	((d.cd_portador = cd_portador_w) or (cd_portador_w is null))
	and	nvl(d.dt_inicio_vigencia,dt_mesano_referencia_fimdia_w) <= dt_mesano_referencia_fimdia_w and ((d.dt_fim_vigencia >= dt_mesano_referencia_fimdia_w) or (d.dt_fim_vigencia is null))
	and	nr_contrato_intercambio_w is null
	and	((ie_tipo_lote_w	= 'A') or (ie_tipo_lote_w	= 'CO'))
	and	((ie_pagador_beneficio_obito_w = 'S' and a.nr_seq_regra_obito is not null) or (ie_pagador_beneficio_obito_w = 'N'))
	and	((a.ie_endereco_boleto = ie_endereco_boleto_lote_w) or (ie_endereco_boleto_lote_w is null))
	and	((ie_tipo_pessoa_pagador_w = 'A') or (ie_tipo_pessoa_pagador_w = 'PF' and a.cd_pessoa_fisica is not null) or (ie_tipo_pessoa_pagador_w = 'PJ' and a.cd_cgc is not null))
	and	((b.nr_seq_classificacao = nr_seq_classif_benef_w) or (nr_seq_classif_benef_w is null))
	/* FIM ITEM */
	union all
	select	1
	from	pls_sca_vinculo			h,
		pls_contrato_grupo		g,
		pls_grupo_contrato		f,
		pls_desc_empresa		e,
		pls_contrato_pagador_fin	d,
		pls_contrato			c,
		pls_segurado 			b,
		pls_contrato_pagador 		a,
		pls_preco_contrato		i,
		pls_preco_grupo_contrato	j
	where	a.nr_seq_contrato	= c.nr_sequencia
	and	a.nr_sequencia		= h.nr_seq_pagador
	and	b.nr_sequencia		= h.nr_seq_segurado
	and	d.nr_seq_pagador(+)	= a.nr_sequencia
	and	d.nr_seq_empresa	= e.nr_sequencia(+)
	and	g.nr_seq_grupo		= f.nr_sequencia(+)
	and	g.nr_seq_contrato(+)	= c.nr_sequencia
	and	i.nr_seq_contrato(+)	= c.nr_sequencia
	and	i.nr_seq_grupo		= j.nr_sequencia(+)
	and	c.cd_estabelecimento	= cd_estabelecimento_p
	and	((a.nr_sequencia	= nr_seq_pagador_mens_w and	nr_seq_pagador_mens_w is not null) or nr_seq_pagador_mens_w is null)
	and	((nr_dia_inicial_venc_w is null) or (d.dt_dia_vencimento >= nr_dia_inicial_venc_w))
		and	((nr_dia_final_venc_w is null) or (d.dt_dia_vencimento <= nr_dia_final_venc_w))
	and	(nr_contrato_w is null or ((c.nr_sequencia = nr_contrato_w) or (c.nr_contrato_principal = nr_contrato_w)))
	and	((f.nr_sequencia = nr_seq_grupo_contrato_w) or (nr_seq_grupo_contrato_w is null))
	and	((j.nr_sequencia = nr_seq_grupo_preco_w) or (nr_seq_grupo_preco_w is null))
	and	(cd_banco_lote_w is null or d.cd_banco = cd_banco_lote_w)
	and	(nr_seq_forma_cobranca_lote_w is null or d.nr_seq_forma_cobranca = nr_seq_forma_cobranca_lote_w)
	and	((e.nr_seq_empresa_superior = nr_seq_empresa_w or e.nr_sequencia = nr_seq_empresa_w) or	(nr_seq_empresa_w is null))
	and	((d.cd_tipo_portador = cd_tipo_portador_w) or (cd_tipo_portador_w is null))
	and	((d.cd_portador = cd_portador_w) or (cd_portador_w is null))
	and	nvl(d.dt_inicio_vigencia,dt_mesano_referencia_fimdia_w) <= dt_mesano_referencia_fimdia_w and ((d.dt_fim_vigencia >= dt_mesano_referencia_fimdia_w) or (d.dt_fim_vigencia is null))
	and	nr_contrato_intercambio_w is null
	and	((ie_tipo_lote_w	= 'A') or (ie_tipo_lote_w	= 'CO')) /* aaschlote 26/10/2010 - OS - 256840 Gerar mensalidade apenas dos tipos de lote Ambos ou Contratos da Operadora*/
	and	((ie_pagador_beneficio_obito_w = 'S' and a.nr_seq_regra_obito is not null) or (ie_pagador_beneficio_obito_w = 'N'))
	and	((a.ie_endereco_boleto = ie_endereco_boleto_lote_w) or (ie_endereco_boleto_lote_w is null))
	and	((ie_tipo_pessoa_pagador_w = 'A') or (ie_tipo_pessoa_pagador_w = 'PF' and a.cd_pessoa_fisica is not null) or (ie_tipo_pessoa_pagador_w = 'PJ' and a.cd_cgc is not null))
	and	((b.nr_seq_classificacao = nr_seq_classif_benef_w) or (nr_seq_classif_benef_w is null))
	union all
	select	1
	from	pls_desc_empresa		e,
		pls_contrato_pagador_fin	d,
		pls_intercambio			c,
		pls_segurado 			b,
		pls_contrato_pagador 		a,
		pls_preco_contrato		f,
		pls_preco_grupo_contrato	g
	where	a.nr_sequencia		= b.nr_seq_pagador
	and	b.nr_seq_intercambio	= c.nr_sequencia
	and	d.nr_seq_pagador	= a.nr_sequencia
	and	d.nr_seq_empresa	= e.nr_sequencia(+)
	and	f.nr_seq_intercambio(+)	= c.nr_sequencia
	and	f.nr_seq_grupo		= g.nr_sequencia(+)
	and	c.cd_estabelecimento	= cd_estabelecimento_p
	and	((a.nr_sequencia	= nr_seq_pagador_mens_w and	nr_seq_pagador_mens_w is not null) or nr_seq_pagador_mens_w is null)
	and	((nr_dia_inicial_venc_w is null) or (d.dt_dia_vencimento >= nr_dia_inicial_venc_w))
		and	((nr_dia_final_venc_w is null) or (d.dt_dia_vencimento <= nr_dia_final_venc_w))
	and	((nr_contrato_intercambio_w is null) or (c.nr_sequencia = nr_contrato_intercambio_w))
	and	((nr_seq_grupo_inter_w is null) or (c.nr_seq_grupo_intercambio = nr_seq_grupo_inter_w))
	and	((ie_tipo_contrato_intercambio_w is null) or (c.ie_tipo_contrato = ie_tipo_contrato_intercambio_w))
	and	((g.nr_sequencia = nr_seq_grupo_preco_w) or (nr_seq_grupo_preco_w is null))
	and	(cd_banco_lote_w is null or d.cd_banco = cd_banco_lote_w)
	and	(nr_seq_forma_cobranca_lote_w is null or d.nr_seq_forma_cobranca = nr_seq_forma_cobranca_lote_w)
	and	((e.nr_seq_empresa_superior = nr_seq_empresa_w or e.nr_sequencia = nr_seq_empresa_w) or	(nr_seq_empresa_w is null))
	and	((d.cd_tipo_portador = cd_tipo_portador_w) or (cd_tipo_portador_w is null))
	and	((d.cd_portador = cd_portador_w) or (cd_portador_w is null))
	and	d.dt_inicio_vigencia <= dt_mesano_referencia_fimdia_w and ((d.dt_fim_vigencia >= dt_mesano_referencia_fimdia_w) or (d.dt_fim_vigencia is null))
	and	nr_contrato_w is null
	and	((ie_tipo_lote_w	= 'A') or (ie_tipo_lote_w	= 'CI')) /* aaschlote 26/10/2010 - OS - 256840 Gerar mensalidade apenas dos tipos de lote Ambos ou Contratos da Intercâmbio*/
	and	((ie_pagador_beneficio_obito_w = 'S' and a.nr_seq_regra_obito is not null) or (ie_pagador_beneficio_obito_w = 'N'))
	and	((a.ie_endereco_boleto = ie_endereco_boleto_lote_w) or (ie_endereco_boleto_lote_w is null))
	and	((ie_tipo_pessoa_pagador_w = 'A') or (ie_tipo_pessoa_pagador_w = 'PF' and a.cd_pessoa_fisica is not null) or (ie_tipo_pessoa_pagador_w = 'PJ' and a.cd_cgc is not null))
	and	((b.nr_seq_classificacao = nr_seq_classif_benef_w) or (nr_seq_classif_benef_w is null))
	union all
	select	1
	from	pls_desc_empresa		e,
		pls_contrato_pagador_fin	d,
		pls_contrato_pagador 		a
	where	d.nr_seq_pagador	= a.nr_sequencia
	and	d.nr_seq_empresa	= e.nr_sequencia(+)
	and	nr_contrato_intercambio_w is not null
	and	((ie_tipo_lote_w	<> 'A') and (ie_tipo_lote_w	<> 'CI'))
	and	((a.ie_endereco_boleto = ie_endereco_boleto_lote_w) or (ie_endereco_boleto_lote_w is null))
	and	a.nr_seq_pagador_compl in (	select	a.nr_sequencia
						from	pls_contrato_pagador	a,
							pls_intercambio		b
						where	a.nr_seq_pagador_intercambio = b.nr_sequencia
						and	((nr_contrato_intercambio_w is null) or (b.nr_sequencia = nr_contrato_intercambio_w))
						and	((nr_seq_grupo_inter_w is null) or (b.nr_seq_grupo_intercambio = nr_seq_grupo_inter_w))
						and	((ie_tipo_contrato_intercambio_w is null) or (b.ie_tipo_contrato = ie_tipo_contrato_intercambio_w)))
	and	((ie_tipo_pessoa_pagador_w = 'A') or (ie_tipo_pessoa_pagador_w = 'PF' and a.cd_pessoa_fisica is not null) or (ie_tipo_pessoa_pagador_w = 'PJ' and a.cd_cgc is not null));

begin

select	nr_seq_contrato,
	nr_seq_pagador,
	nr_dia_inicial_venc,
	nr_dia_final_venc,
	nr_seq_grupo_contrato,
	cd_banco,
	nr_seq_empresa,
	nr_seq_forma_cobranca,
	trunc(dt_mesano_referencia, 'month'),
	ie_tipo_lote,
	nr_seq_contrato_inter,
	cd_tipo_portador,
	cd_portador,
	nr_seq_grupo_preco,
	ie_tipo_contrato,
	nr_seq_grupo_inter,
	nvl(ie_pagador_beneficio_obito,'N'),
	ie_endereco_boleto,
	nvl(ie_tipo_pessoa_pagador,'A'),
	nr_seq_classif_benef
into	nr_contrato_w,
	nr_seq_pagador_mens_w,
	nr_dia_inicial_venc_w,
	nr_dia_final_venc_w,
	nr_seq_grupo_contrato_w,
	cd_banco_lote_w,
	nr_seq_empresa_w,
	nr_seq_forma_cobranca_lote_w,
	dt_mesano_referencia_w,
	ie_tipo_lote_w,
	nr_contrato_intercambio_w,
	cd_tipo_portador_w,
	cd_portador_w,
	nr_seq_grupo_preco_w,
	ie_tipo_contrato_intercambio_w,
	nr_seq_grupo_inter_w,
	ie_pagador_beneficio_obito_w,
	ie_endereco_boleto_lote_w,
	ie_tipo_pessoa_pagador_w,
	nr_seq_classif_benef_w
from	pls_lote_mensalidade
where	nr_sequencia	= nr_seq_lote_p;

dt_mesano_referencia_fimdia_w	:= fim_dia(dt_mesano_referencia_w);

qt_retorno_w	:= 0;

open C01;
loop
fetch C01 into
	qt_w;
exit when C01%notfound;
	begin
	qt_retorno_w	:= qt_retorno_w + 1;
	end;
end loop;
close C01;

return	qt_retorno_w;

end pls_mens_obter_qtd_pagador;
/