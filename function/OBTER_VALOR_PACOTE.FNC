create or replace
function OBTER_VALOR_PACOTE(nr_seq_propaci_p	number) return number is

VL_PROCEDIMENTO_w	number(15,2);

begin
select	nvl(VL_PROCEDIMENTO,0)
into	VL_PROCEDIMENTO_w
from	procedimento_paciente
where	nr_sequencia = nr_seq_propaci_p;
return	VL_PROCEDIMENTO_w;
end;
/