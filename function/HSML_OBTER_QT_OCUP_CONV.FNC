create or replace 
FUNCTION hsml_obter_qt_ocup_conv(dt_parametro_p  DATE,
				ie_opcao_p varchar2,
				  cd_convenio_p number)
        RETURN NUMBER IS

qt_ocup_conv_w NUMBER(10,4);

BEGIN

IF (ie_opcao_p = 'M') THEN
 BEGIN
	SELECT  SUM(nr_leitos_ocupados)*100
	into 	qt_ocup_conv_w
	FROM 	HMSL_EIS_OCUP_convenio_V a
	WHERE 	ie_periodo = 'M'
	and	a.cd_convenio = cd_convenio_p
	and	trunc(dt_parametro_p,'month') = trunc(dt_referencia,'month');	
 END;
END IF;
IF (ie_opcao_p = 'D') THEN
 BEGIN
	SELECT  SUM(nr_leitos_ocupados)*100
	into    qt_ocup_conv_w
	FROM    HMSL_EIS_OCUP_convenio_V a
	WHERE   ie_periodo = 'D'
	and	a.cd_convenio = cd_convenio_p
	and	trunc(dt_parametro_p,'dd') = trunc(dt_referencia,'dd');
 END;
END IF;
IF (ie_opcao_p = '6h') THEN
 BEGIN
	SELECT  SUM(nr_leitos_ocupados)*100
	into    qt_ocup_conv_w
	FROM    HMSL_EIS_OCUP_convenio_V a
	WHERE   ie_periodo = 'D'
	and	a.cd_convenio = cd_convenio_p
	and	trunc(dt_parametro_p,'dd') = trunc(dt_referencia,'dd')
	and	to_char(dt_referencia,'hh24') = '06';
 END;
END IF;

RETURN qt_ocup_conv_w;

END hsml_obter_qt_ocup_conv;
