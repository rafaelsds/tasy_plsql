Create or Replace
FUNCTION OBTER_VALOR_GUIA_GRG	(nr_interno_conta_p	number,
				cd_autorizacao_p	varchar2,
				ie_opcao_p		varchar2)
					RETURN		number is

/* ie_opcao_p
'VG'	- Valor glosa
'VP'	- Valor pago
*/

vl_retorno_w	number(15,2)	:= 0;
vl_glosa_w	number(15,2);
vl_pago_w	number(15,2);
ie_param_266_w	varchar2(1) := 'N';
BEGIN
obter_param_usuario(27,266,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_param_266_w);
	select	nvl(sum(b.vl_glosa),0),
		nvl(sum(b.vl_pago),0)
	into	vl_glosa_w,
		vl_pago_w
	from	lote_audit_hist_item b,
		lote_audit_hist_guia a
	where	a.nr_interno_conta	= nr_interno_conta_p
	and	a.cd_autorizacao	= cd_autorizacao_p
	and	a.nr_sequencia		= b.nr_seq_guia
	and	a.dt_baixa_glosa	is not null;

if	(ie_opcao_p	= 'VG') then
	vl_retorno_w	:= vl_glosa_w;
elsif	(ie_opcao_p	= 'VP') and
	(Nvl(ie_param_266_w,'N') = 'N') then
	vl_retorno_w	:= vl_pago_w;
end if;

RETURN vl_retorno_w;

END OBTER_VALOR_GUIA_GRG;
/