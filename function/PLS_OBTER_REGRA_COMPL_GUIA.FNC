create or replace
function pls_obter_regra_compl_guia  return Varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter quantidade de dias na regra para o complemento da guia no portal
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

dt_dia_final_w		Number(5);
qt_mes_w			Number(5);
dia_atual_w			Varchar2(2);
mes_atual_w			Varchar2(2);
dt_retorno_w		Varchar2(10);
qt_dias_mes_w		Number(5);

begin

select	to_char(sysdate - 60,'dd/mm/yyyy')
into	dt_retorno_w
from	dual;

if	(pls_obter_se_controle_estab('RE') = 'S')then
	begin
		select	nvl(qt_mes_complemento, 0),
			nvl(dt_dia_complemento, 0)
		into	qt_mes_w,
			dt_dia_final_w
		from	pls_regra_compl_guia_web
		where	ie_situacao = 'A'
		and 	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
		and	rownum = 1;
	exception
	when others then
		dt_dia_final_w := 0;
		qt_mes_w := 0;
	end;
else
	begin
		select	nvl(qt_mes_complemento, 0),
			nvl(dt_dia_complemento, 0)
		into	qt_mes_w,
			dt_dia_final_w
		from	pls_regra_compl_guia_web
		where	ie_situacao = 'A'
		and		rownum = 1;
	exception
	when others then
		dt_dia_final_w := 0;
		qt_mes_w := 0;
	end;
end if;	
	
if	(qt_mes_w > 0 and dt_dia_final_w > 0)then
	
	qt_dias_mes_w := obter_qt_dias_mes(sysdate);
	
	if	(dt_dia_final_w >  qt_dias_mes_w) then
		dt_dia_final_w := qt_dias_mes_w;
	end if;
	
	if	(to_number(dia_atual_w) > dt_dia_final_w ) then
		qt_mes_w := qt_mes_w - 1;
		if(qt_mes_w > 0) then
			dt_retorno_w := '01/'||to_char(add_months(sysdate, - qt_mes_w), 'mm/yyyy');
		else
			dt_retorno_w := '01/'||to_char(sysdate, 'mm/yyyy');
		end if;
	else
		dt_retorno_w := '01/'||to_char(add_months(sysdate, - qt_mes_w), 'mm/yyyy');
	end if;
else
	dt_retorno_w := sysdate - 60;
end if;

return	dt_retorno_w;

end pls_obter_regra_compl_guia;
/
