create or replace
function obter_dados_ev_pac_atend(	nr_seq_evento_p		number,
					ie_opcao_p		varchar2,
					nr_atendimento_p	number,
					cd_pessoa_fisica_p	varchar2)
					return varchar2 is

ds_retorno_w		varchar2(2000);
ds_titulo_w		varchar2(100);
ds_mensagem_w		varchar2(2000);
nm_paciente_w		varchar2(60);
ds_unidade_w		varchar2(60);
ds_setor_atendimento_w	varchar2(60);
nr_ramal_w		varchar2(10);
nr_telefone_w		varchar2(40);
ds_tipo_atend_w		varchar2(60);
ds_plano_conv_w		varchar2(60);
ds_categoria_conv_w	varchar2(60);
ds_convenio_w		varchar2(60);
					
begin

select	max(substr(obter_nome_pf(cd_pessoa_fisica_p),1,60)),
	max(substr(obter_unidade_atendimento(nr_atendimento_p,'A','U'),1,60)),
	max(substr(obter_unidade_atendimento(nr_atendimento_p,'A','RA'),1,60)),
	max(substr(obter_unidade_atendimento(nr_atendimento_p,'A','TL'),1,60)),
	max(substr(obter_unidade_atendimento(nr_atendimento_p,'A','S'),1,60)),
	max(substr(obter_nome_tipo_atend(obter_tipo_atendimento(nr_atendimento_p)),1,60))
into	nm_paciente_w,
	ds_unidade_w,
	nr_ramal_w,
	nr_telefone_w,
	ds_setor_atendimento_w,
	ds_tipo_atend_w
from	dual;
if	(nr_seq_evento_p > 0) then
	select	ds_titulo,
		substr(ds_mensagem,1,2000)
	into	ds_titulo_w,
		ds_mensagem_w
	from	ev_evento
	where	nr_sequencia = nr_seq_evento_p;
end if;

select	substr(obter_dados_categ_conv(nr_atendimento_p, 'DC'), 1, 100),
	substr(obter_dados_categ_conv(nr_atendimento_p, 'DC'), 1, 100),
	substr(Obter_Nome_Convenio(obter_convenio_atendimento(nr_atendimento_p)), 1, 60)
into	ds_plano_conv_w,
	ds_categoria_conv_w,
	ds_convenio_w
from	atend_categoria_convenio
where	nr_seq_interno	= obter_atecaco_atendimento(nr_atendimento_p);	

if	(ie_opcao_p = 'TI') then

	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@paciente',nm_paciente_w),1,4000);
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@quarto',ds_unidade_w),1,4000);
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@setor',ds_setor_atendimento_w),1,4000);
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@atendimento',nr_atendimento_p),1,4000);
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@ramal',nr_ramal_w),1,4000);
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@telefone',nr_telefone_w),1,4000);
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@tipo_atendimento', ds_tipo_atend_w),1,4000);
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@convenio', ds_convenio_w),1,4000);
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@categoria', ds_categoria_conv_w),1,4000);	
	ds_titulo_w	:= substr(replace_macro(ds_titulo_w,'@plano', ds_plano_conv_w),1,4000);

	ds_retorno_w	:= ds_titulo_w;
elsif	(ie_opcao_p = 'ME') then

	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@paciente',nm_paciente_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@quarto',ds_unidade_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@setor',ds_setor_atendimento_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@atendimento',nr_atendimento_p),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@ramal',nr_ramal_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@telefone',nr_telefone_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@tipo_atendimento', ds_tipo_atend_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@convenio', ds_convenio_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@categoria', ds_categoria_conv_w),1,4000);	
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@plano', ds_plano_conv_w),1,4000);

	ds_retorno_w	:= ds_mensagem_w;
end if;

return	ds_retorno_w;

end obter_dados_ev_pac_atend;
/