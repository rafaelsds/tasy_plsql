create or replace
function ageCons_obter_bloq_estagio_pls
		(nr_seq_agenda_p	number)
 		return varchar2 is

qt_bloq_w	number(2);
ds_retorno_w	varchar2(1);
begin

select	count(*)
into	qt_bloq_w
from	agenda_bloq_estagio_aut;

if	(qt_bloq_w > 0) then
	begin
	select	count(*)
	into	qt_bloq_w
	from	agenda_bloq_estagio_aut
	where	ie_estagio = (	select	max(b.ie_estagio)
				from	autorizacao_convenio a,
					pls_guia_plano b
				where	a.cd_autorizacao = b.cd_guia
				and	a.nr_seq_agenda_consulta = nr_seq_agenda_p );
	end;
end if;

if	(qt_bloq_w > 0) then
	ds_retorno_w	:= 'S';
else
	ds_retorno_w	:= 'N';
end if;

return	ds_retorno_w;

end ageCons_obter_bloq_estagio_pls;
/