create or replace
function adep_concat_dil_obs		(cd_matmed_p	varchar2,
				nr_prescricoes_p	varchar2,
				qt_hora_prescr_p 	number,
				ds_diluicao_p	varchar2)
				return varchar2 is

ds_dil_obs_w	varchar2(2000);

ds_obs_w	varchar2(2000) := null;

ds_obs_ww	varchar2(2000);
ie_medico_w	varchar2(1);

ie_prof_w	varchar2(1);

cursor c01 is
select	a.ds_observacao,
	obter_se_medico(a.cd_pessoa_fisica,'M') ie_medico
from	prescr_medica b,
	prescr_mat_alteracao a
where	b.nr_prescricao = a.nr_prescricao
and	b.dt_validade_prescr > sysdate - qt_hora_prescr_p/24
and	a.ie_tipo_item = 'M'
and	a.cd_item = cd_matmed_p
and	substr(obter_se_contido(a.nr_prescricao,nr_prescricoes_p),1,1) = 'S'
order by
	ie_medico desc,
	a.dt_alteracao desc;

begin
if	(cd_matmed_p is not null) and
	(nr_prescricoes_p is not null) and
	(ds_diluicao_p is not null) then

	open c01;
	loop
	fetch c01 into	ds_obs_ww,
			ie_medico_w;
	exit when c01%notfound;
		begin
		
		if	(ds_obs_w is null) then
			if	(ie_medico_w = 'S') then
				ds_obs_w := substr('-MED: ' || ds_obs_ww,1,2000);
			else
				ds_obs_w := substr('-ENF: ' || ds_obs_ww,1,2000);
			end if;
			ie_prof_w := ie_medico_w;

		else

			if	(ie_prof_w = ie_medico_w) then
				ds_obs_w := substr(ds_obs_w || '; ' || ds_obs_ww,1,2000);
			elsif	(ie_medico_w = 'S') then
				ds_obs_w := substr(ds_obs_w || '; -MED: ' || ds_obs_ww,1,2000);
			elsif	(ie_medico_w = 'N') then
				ds_obs_w := substr(ds_obs_w || '; -ENF: ' || ds_obs_ww,1,2000);
			end if;
			ie_prof_w := ie_medico_w;
		end if;		

		end;
	end loop;
	close c01;

	ds_dil_obs_w := substr(ds_diluicao_p || chr(10) || ds_obs_w,1,2000);


elsif	(ds_diluicao_p is not null) then

	ds_dil_obs_w	:= ds_diluicao_p;

end if;

return ds_dil_obs_w;

end adep_concat_dil_obs;
/