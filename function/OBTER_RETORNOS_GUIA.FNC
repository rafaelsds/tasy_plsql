create or replace
function OBTER_RETORNOS_GUIA
		(nr_interno_conta_p	number,
		cd_autorizacao_p	varchar2,
		nr_seq_retorno_p	number) 
		return varchar2 is

ds_retorno_w		varchar2(2000);
nr_seq_retorno_w	number(10);


cursor c01 is
select	b.nr_sequencia nr_sequencia
from	convenio_retorno_item a,
	convenio_retorno b
where	a.nr_seq_retorno	= b.nr_sequencia
and	b.ie_status_retorno	= 'F'
and	a.nr_interno_conta	= nr_interno_conta_p
and	a.cd_autorizacao	= cd_autorizacao_p
and	b.nr_sequencia		<> nr_seq_retorno_p
group by b.nr_sequencia
order by nr_sequencia;

begin

ds_retorno_w		:= '';
open c01;
loop
fetch c01 into
	nr_seq_retorno_w;
exit when c01%notfound;
	ds_retorno_w	:= ds_retorno_w || nr_seq_retorno_w || ', ';
	
end loop;
close c01;

if	(instr(ds_retorno_w, ', ') > 0) then
	ds_retorno_w		:= substr(ds_retorno_w, 1, length(ds_retorno_w) - 2);
end if;

return	ds_retorno_w;

end OBTER_RETORNOS_GUIA;
/