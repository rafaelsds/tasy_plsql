create or replace
function HD_obter_ultimo_diagnostico(cd_pessoa_fisica_P	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(10);

begin

select a.cd_doenca
into	ds_retorno_w
from   (select   a.cd_doenca, 
	   			 trunc(a.dt_diagnostico) dt_diagnostico
		from     diagnostico_medico   b,
	diagnostico_doenca   a,
         atendimento_paciente x
where    a.nr_atendimento     = x.nr_atendimento
and      x.cd_pessoa_fisica   = cd_pessoa_fisica_p
and		 a.nr_atendimento       = b.nr_atendimento
and		 a.dt_diagnostico       = b.dt_diagnostico	
order by dt_diagnostico desc) a
where  rownum = 1;

return	ds_retorno_w;

end HD_obter_ultimo_diagnostico;
/