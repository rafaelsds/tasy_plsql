create or replace
function obter_se_contrato_em_revisao(nr_seq_contrato_p	number)
		return varchar2 is

ie_retorno_w		varchar2(1);
qt_existe_w		number(10);

begin

select	count(*)
into	qt_existe_w
from	contrato a
where	a.nr_sequencia = nr_seq_contrato_p
and	a.ie_situacao = 'A'
and	a.dt_revisao is not null
and	nvl(a.qt_dias_revisao,0) > 0
and	trunc(trunc(a.dt_revisao,'dd') + nvl(a.qt_dias_revisao,0)) <= trunc(sysdate,'dd');

if	(qt_existe_w > 0) then
	ie_retorno_w	:= 'S';
else
	ie_retorno_w	:= 'N';
end if;

return	ie_retorno_w;

end obter_se_contrato_em_revisao;
/
