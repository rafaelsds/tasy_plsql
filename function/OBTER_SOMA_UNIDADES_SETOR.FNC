create or replace 
function obter_soma_unidades_setor
		(cd_estabelecimento_p	number,
		cd_classif_setor_p	number)
			return number is

qt_unidades_setor_w	number(4,0);

begin

select	count(*) - sum(decode(b.ie_temporario, 'S', 1, 0))
into	qt_unidades_setor_w
from	setor_atendimento c,
     	unidade_atendimento b
where 	b.cd_setor_atendimento     	= c.cd_setor_atendimento
and	b.ie_situacao              	= 'A'
and	c.cd_classif_setor 		in (3,4)
and	c.cd_estabelecimento_base	 	= cd_estabelecimento_p
and 	(cd_classif_setor_p is null or c.cd_classif_setor		= cd_classif_setor_p);
return	qt_unidades_setor_w;	

end obter_soma_unidades_setor;
/
