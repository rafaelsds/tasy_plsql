create or replace
function interlink_obter_cod_mat_tiss
	(cd_estabelecimento_p	number,
	cd_material_p		number,
	nr_atendimento_p	Number)
	return varchar2 is
	
	
ds_retorno_w			varchar2(255) := '';
cd_material_brasindice_w	varchar2(255) := '';
cd_laboratorio_w		varchar2(255) := '';
cd_medicamento_w		varchar2(255) := '';
cd_apresentacao_w		varchar2(255) := '';

begin

cd_material_brasindice_w	:= obter_codigo_brasindice(cd_estabelecimento_p, cd_material_p, sysdate, Obter_Convenio_Atendimento(nr_atendimento_p));
cd_laboratorio_w		:= substr(cd_material_brasindice_w,1,3);
cd_medicamento_w		:= substr(cd_material_brasindice_w,5,5);
cd_apresentacao_w		:= substr(cd_material_brasindice_w,11,4);
ds_retorno_w			:= tiss_obter_cod_brasindice(cd_apresentacao_w, cd_laboratorio_w, cd_medicamento_w);
	

return ds_retorno_w;

end interlink_obter_cod_mat_tiss;
/
