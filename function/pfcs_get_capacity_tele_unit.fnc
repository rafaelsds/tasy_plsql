create or replace function pfcs_get_capacity_tele_unit(	cd_establishment_p in number,
														cd_department_p in varchar2) return number as 
nr_unit_tl_capacity_w	setor_atendimento.nr_unit_tl_capacity%type;

begin
	select	sum(nvl(sa.nr_unit_tl_capacity,0))
	  into	nr_unit_tl_capacity_w
	   from	setor_atendimento sa
	  where	sa.cd_estabelecimento_base = cd_establishment_p
		and	sa.cd_classif_setor in ('1','3','4','9','11','12')
		and	sa.ie_situacao = 'A'
		and	((cd_department_p is null) or (sa.ds_setor_atendimento = cd_department_p));

	return nr_unit_tl_capacity_w;

end pfcs_get_capacity_tele_unit;
/
