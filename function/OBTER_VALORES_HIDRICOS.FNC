create or replace 
function obter_valores_hidricos(nr_atendimento_p	number,
				ie_exige_lib_p		varchar2,
				ie_opcao_p		varchar2) return number is

qt_ganho_w	number(15,2);
qt_perda_w	number(15,2);
qt_diferenca_w	number(15,2);
qt_retorno_w	number(15,2);
/*
G - Ganho
P - Perda
D - Diferenša
*/
begin
if (nvl(nr_atendimento_p,0) > 0) then

	select	sum(decode(a.ie_perda_ganho,'G', c.qt_volume,0)) qt_ganho,
		sum(decode(a.ie_perda_ganho,'P', c.qt_volume,0)) qt_perda,
		sum(decode(a.ie_perda_ganho,'G', c.qt_volume,0) - decode(a.ie_perda_ganho,'P', c.qt_volume,0)) qt_diferenca
	into	qt_ganho_w,
		qt_perda_w,
		qt_diferenca_w
	from	tipo_perda_ganho b,
		grupo_perda_ganho a,
		atendimento_perda_ganho c
	where	c.nr_atendimento = nr_atendimento_p
	and	b.nr_sequencia = c.nr_seq_tipo
	and	b.nr_seq_grupo = a.nr_sequencia
	and     nvl(b.ie_soma_bh,'S') = 'S'
	and     ((ie_exige_lib_p = 'N') or (c.dt_liberacao is not null))
	and     nvl(c.ie_situacao,'A') = 'A';
	
	if	(ie_opcao_p = 'G') then
		qt_retorno_w := qt_ganho_w;
	elsif	(ie_opcao_p = 'P') then
		qt_retorno_w := qt_perda_w;
	elsif	(ie_opcao_p = 'D') then
		qt_retorno_w := qt_diferenca_w;
	end if;
end if;

return nvl(qt_retorno_w,0);

end obter_valores_hidricos;
/