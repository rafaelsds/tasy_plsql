create or replace
function user_can_view_sensitive_info(
		nm_usuario_p 		varchar2,
		cd_estabelecimento_p	number)
		return 			varchar2 is

ie_retorno_w	varchar2(1);
qt_grupos_w	number(10);
	
begin
select 	nvl(max(ie_can_view_sensitive_info), 'Y')
into 	ie_retorno_w
from 	usuario
where 	nm_usuario = nm_usuario_p
and	ie_situacao = 'A';

if (ie_retorno_w = 'N') then

	select 	count(*)
	into 	qt_grupos_w
	from 	usuario_grupo u,
		grupo_usuario g
	where 	u.nr_seq_grupo = g.nr_sequencia
	and	u.ie_situacao = 'A'
	and	g.ie_situacao = 'A'
	and	u.nm_usuario_grupo = nm_usuario_p
	and	g.cd_estabelecimento = cd_estabelecimento_p
	and	nvl(g.ie_can_view_sensitive_info, 'Y') = 'Y';

	if (qt_grupos_w > 0) then

		select 	nvl(max(g.ie_can_view_sensitive_info), 'Y')
		into 	ie_retorno_w
		from 	usuario_grupo u,
			grupo_usuario g
		where 	u.nr_seq_grupo = g.nr_sequencia
		and	u.ie_situacao = 'A'
		and	g.ie_situacao = 'A'
		and	u.nm_usuario_grupo = nm_usuario_p
		and	g.cd_estabelecimento = cd_estabelecimento_p
		and	nvl(g.ie_can_view_sensitive_info, 'Y') = 'Y';

	end if;
end if;

return	ie_retorno_w;

end user_can_view_sensitive_info;
/
