create or replace
function obter_cor_ativ_agenda(nr_seq_atividade_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);			
			
begin

begin

select	ds_cor_atividade
into	ds_retorno_w
from	atividade_agenda
where	nr_sequencia = 	nr_seq_atividade_p;

exception
when others then
	ds_retorno_w := '';
end;


return	ds_retorno_w;

end obter_cor_ativ_agenda;
/