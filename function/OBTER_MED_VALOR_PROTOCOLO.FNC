create or replace
function Obter_Med_Valor_Protocolo
		(nr_seq_protocolo_p	number)
		return number is


vl_total_item_w		number(15,2);

begin

select	nvl(sum(qt_item * vl_item),0)
into	vl_total_item_w
from	med_faturamento
where	nr_seq_protocolo	= nr_seq_protocolo_p;

return	vl_total_item_w; 

end Obter_Med_Valor_Protocolo;
/