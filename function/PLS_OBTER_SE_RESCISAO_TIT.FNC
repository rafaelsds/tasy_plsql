create or replace
function pls_obter_se_rescisao_tit
			(	nr_titulo_p	number,
				ie_opcao_p	varchar2)
 		    		return varchar2 is

/* ie_opcao_p
	'I' = Inadimplencia
	'O' = Outros motivos
	'A' = Ativos
*/
/* Function que retorna se o t�tulo est� aberto e o contrato rescindido por inadimplencia*/
ds_retorno_w			varchar2(2) := 'N';
ie_situacao_w			varchar2(1);
dt_rescisao_w			date;
nr_seq_motivo_rescisao_w	number(10);
ie_inadimplencia_w		varchar2(1);

begin

select	max(c.dt_rescisao_contrato),
	max(b.ie_situacao),
	max(c.nr_seq_motivo_rescisao)
into	dt_rescisao_w,
	ie_situacao_w,
	nr_seq_motivo_rescisao_w
from	pls_mensalidade a,
	titulo_receber b,
	pls_contrato c
where	a.nr_sequencia	= b.nr_seq_mensalidade
and	c.nr_sequencia	= a.nr_seq_contrato
and	b.nr_titulo	= nr_titulo_p;

select	max(ie_inadimplencia)
into	ie_inadimplencia_w
from	pls_motivo_cancelamento
where	nr_sequencia	= nr_seq_motivo_rescisao_w;

if	((ie_inadimplencia_w = 'S') and 
	(dt_rescisao_w is not null) and
	(ie_opcao_p = 'I')) then
	ds_retorno_w := 'S';
elsif	((ie_inadimplencia_w = 'N') and 
	(dt_rescisao_w is not null) and
	(ie_opcao_p = 'O')) then
	ds_retorno_w := 'S';
elsif	((dt_rescisao_w is null) and
	(ie_opcao_p = 'A')) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end pls_obter_se_rescisao_tit;
/