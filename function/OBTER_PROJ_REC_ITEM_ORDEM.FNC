create or replace
function obter_proj_rec_item_ordem(	nr_ordem_compra_p		number,
				nr_item_oci_p			number,
				ie_opcao_p			number)
 		    	return varchar2 is

/*ie_opcao_p
0 = Codigo projeto
1 = Descricao projeto*/			

nr_seq_proj_rec_w				number(10);
ds_projeto_w				varchar2(100);

begin

select	nr_seq_proj_rec,
	substr(obter_desc_proj_recurso(nr_seq_proj_rec),1,100) ds_projeto
into	nr_seq_proj_rec_w,
	ds_projeto_w
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p;

if	(ie_opcao_p = 0) then
	return nr_seq_proj_rec_w;
elsif	(ie_opcao_p = 1) then
	return ds_projeto_w;
end if;

end obter_proj_rec_item_ordem;
/