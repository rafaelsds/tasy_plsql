create or replace
function obter_tipo_param_log(nm_usuario_atual_p		varchar2,
							nm_usuario_old_p varchar2,
							cd_perfil_atual_p varchar2,
							cd_perfil_old_p varchar2,
							cd_estab_atual_p varchar2,
							cd_estab_old_p varchar2)
 		    	return varchar2 is

ds_tipo_param_w varchar2(15);

begin

if((nm_usuario_atual_p is not null)
	or (nm_usuario_old_p is not null)) then
ds_tipo_param_w:= obter_desc_expressao(724442);--'Usu�rio';
elsif((cd_perfil_atual_p is not null)
	or (cd_perfil_old_p is not null)) then
ds_tipo_param_w:= obter_desc_expressao(295451);--'Perfil';
elsif((cd_estab_atual_p is not null)
	or (cd_estab_old_p is not null)) then
ds_tipo_param_w:= obter_desc_expressao(289496);--'Estabelecimento';
end if;

return	ds_tipo_param_w;

end obter_tipo_param_log;
/