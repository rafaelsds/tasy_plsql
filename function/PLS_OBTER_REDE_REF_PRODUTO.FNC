create or replace
function pls_obter_rede_ref_produto
			(	nr_seq_plano_p		number,
				cd_estabelecimento_p	number,
				ie_opcao_p		varchar2)
 		    	return varchar2 is
			
/*
ie_opcao_p
C - C�digo
D - Descri��o
DT - Descri��o do tipo de rede MIN
*/			
			
ds_retorno_w			varchar2(255);
ie_abrangencia_w		varchar2(255);
ie_segmentacao_w		varchar2(255);
ie_acomodacao_w			varchar2(255);
ie_acomodacao_aux_w		varchar2(255);
cd_rede_w			varchar2(255);
ie_regulamentacao_w		varchar2(255);
cd_rede_refer_ptu_w		pls_plano.cd_rede_refer_ptu%type;

Cursor C01 is
	select	a.cd_rede
	into	cd_rede_w
	from	ptu_rede_referenciada a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	((a.ie_tipo_segmentacao	= ie_segmentacao_w and a.ie_tipo_segmentacao is not null) or
		(a.ie_tipo_segmentacao is null))
	and	((a.ie_abrangencia	= ie_abrangencia_w and a.ie_abrangencia is not null) or
		(a.ie_abrangencia is null))
	and	((a.ie_tipo_acomodacao	= ie_acomodacao_w and nvl(a.ie_tipo_acomodacao,'NA') <> 'NA') or 
		(nvl(a.ie_tipo_acomodacao,'NA') = 'NA'))
	and	((a.ie_regulamentacao	= ie_regulamentacao_w and a.ie_regulamentacao is not null) or
		(a.ie_regulamentacao is null))
	and	a.ie_situacao		= 'A'
	and	((exists	(select	1
				from	pls_plano_rede_atend x
				where	x.nr_seq_plano = nr_seq_plano_p
				and	x.nr_seq_rede = a.nr_seq_rede)) or (a.nr_seq_rede is null))
	order by decode(a.ie_abrangencia,'','-1','1'),
		decode(a.ie_tipo_segmentacao,'','-1','1'),
		decode(a.ie_tipo_acomodacao,'','-1','1'),
		decode(a.ie_regulamentacao,'','-1','1');

begin

if	(nr_seq_plano_p is not null) then
	select	decode(ie_abrangencia,'E','ES','GM','RB','GE','RA','N','NA','M','MU'),
		ie_segmentacao,
		ie_regulamentacao,
		ie_acomodacao,
		cd_rede_refer_ptu
	into	ie_abrangencia_w,
		ie_segmentacao_w,
		ie_regulamentacao_w,
		ie_acomodacao_aux_w,
		cd_rede_refer_ptu_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_p;

	ie_acomodacao_w	:= pls_obter_dados_cart_unimed(null,nr_seq_plano_p,'DA',0);
	ie_acomodacao_w	:= substr(ie_acomodacao_w,1,1);
	
	if	(ie_acomodacao_w is null) then
		select	decode(ie_acomodacao_aux_w,'I','A','C','E','NA')
		into	ie_acomodacao_w
		from	dual;
	end if;
	
	open C01;
	loop
	fetch C01 into	
		cd_rede_w;
	exit when C01%notfound;
	end loop;
	close C01;
	
	if	(cd_rede_w is not null) then	
		if	(ie_opcao_p = 'C') then
			ds_retorno_w := cd_rede_w;
		elsif	(ie_opcao_p = 'D') then
			select	ds_rede
			into	ds_retorno_w
			from	ptu_rede_referenciada
			where	cd_rede	= cd_rede_w;
		end if;
	end if;
	
	if	(cd_rede_refer_ptu_w is not null) then
		if	(ie_opcao_p = 'DT') then
			select	substr(obter_valor_dominio(3259, ie_tipo_rede_min),1,30)
			into	ds_retorno_w
			from	ptu_rede_referenciada
			where	cd_rede	= cd_rede_refer_ptu_w;
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_rede_ref_produto;
/
