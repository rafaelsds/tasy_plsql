create or replace
function Obter_qt_antimicrobiano_inter(
			nr_atendimento_p	number)
			return Varchar2 is

qt_prescrito_w	number(10)	:= 0;
qt_total_w	number(10)	:= 0;
ds_retorno_w	Varchar2(255);

begin

select	count(	distinct (c.cd_material))
into	qt_prescrito_w
from	material c,
	prescr_material b,
	prescr_medica a
where	a.nr_atendimento	= nr_atendimento_p
and	a.nr_prescricao		= b.nr_prescricao
and	b.cd_material		= c.cd_material
and	a.dt_liberacao is not null
and	nvl(c.ie_controle_medico,0) <> 0;

select	count(	distinct (c.cd_material))
into	qt_total_w
from	material c,
	material_atend_paciente a
where	a.nr_atendimento	= nr_atendimento_p
and	a.cd_material		= c.cd_material
and	nvl(c.ie_controle_medico,0) <> 0;

ds_retorno_w	:= wheb_mensagem_pck.get_texto(309499) || ': '|| to_char(qt_prescrito_w) ||' ' || wheb_mensagem_pck.get_texto(309500) || ': '||to_char(qt_total_w); -- Prescrito	-- Total

return ds_retorno_w;

end Obter_qt_antimicrobiano_inter;
/