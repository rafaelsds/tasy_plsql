create or replace
procedure pls_incluir_documentacao_reaj
		(	nr_seq_prog_reaj_p	number,
			nr_seq_relatorio_p	number,
			ds_arquivo_p		varchar,
			ds_documentacao_p	varchar,
			nm_usuario_p		varchar2) is 

qt_registros_w 	pls_integer;
			
begin

select	count(1)
into	qt_registros_w
from	pls_prog_reaj_documentacao
where	nr_seq_prog_reaj = nr_seq_prog_reaj_p;

if	(qt_registros_w = 0) then -- O n�mero m�ximo de registros � 1

	insert into pls_prog_reaj_documentacao (
			nr_sequencia, nr_seq_lote, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_prog_reaj, nr_seq_relatorio, ds_arquivo, ds_documentacao)
		values(	pls_prog_reaj_documentacao_seq.NextVal, null, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			nr_seq_prog_reaj_p, nr_seq_relatorio_p, ds_arquivo_p, ds_documentacao_p);
			
end if;

commit;

end pls_incluir_documentacao_reaj;
/