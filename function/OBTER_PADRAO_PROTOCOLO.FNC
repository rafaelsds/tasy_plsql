create or replace
function obter_padrao_protocolo(cd_pessoa_p		varchar2,
								cd_perfil_p		number,
								ie_opcao_p		varchar2)
 		    	return number is

nr_seq_retorno_w		number(10);
cd_protocolo_w			number(10);
cd_tipo_protocolo_w		number(10);
				
cursor c01 is
select	cd_protocolo,
		cd_tipo_protocolo
from	rep_protocolo_padrao
where	nvl(cd_perfil,cd_perfil_p)	= cd_perfil_p
and		nvl(cd_pessoa_fisica,cd_pessoa_p) = cd_pessoa_p
order by nvl(cd_perfil,0),
		nvl(cd_pessoa_fisica,0);

begin

open C01;
loop
fetch C01 into	
	cd_protocolo_w,
	cd_tipo_protocolo_w;
exit when C01%notfound;
	null;
end loop;
close C01;

if	(ie_opcao_p = 'T') then
	nr_seq_retorno_w	:= cd_tipo_protocolo_w;
elsif	(ie_opcao_p = 'P') then
	nr_seq_retorno_w	:= cd_protocolo_w;
end if;

return	nr_seq_retorno_w;

end obter_padrao_protocolo;
/