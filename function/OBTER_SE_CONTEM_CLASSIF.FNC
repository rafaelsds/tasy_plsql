create or replace
function obter_se_contem_classif(	cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type,
					ds_lista_p		varchar2) return varchar2 is

ds_retorno_w	varchar2(1);

begin

ds_retorno_w := 'N';

if (cd_pessoa_fisica_p is not null) then

	for item in (	select  pc.nr_seq_classif nr_sequencia
			from    pessoa_classif pc
			where   (pc.dt_inicio_vigencia <= sysdate 
			and     (pc.dt_final_vigencia is null or pc.dt_final_vigencia >= sysdate))
			and     pc.cd_pessoa_fisica = cd_pessoa_fisica_p) loop

		if (ds_retorno_w is null or ds_retorno_w = 'N') then

			select	obter_se_contido_char(item.nr_sequencia, ds_lista_p)
			into	ds_retorno_w
			from dual;

		end if;
	
	end loop;

end if;

return ds_retorno_w;

end;
/