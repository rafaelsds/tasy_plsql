create or replace FUNCTION OBTER_USUARIO_COSIGN  (NM_USUARIO_P IN VARCHAR2, 
                                                     NR_SEQUENCIA_P IN NUMBER) 
                                RETURN VARCHAR2 IS 
qt_registros number;
 -- verifica se usuario pode assinar
BEGIN

    select 
         count(*) into qt_registros
    from 
        documento_aprov_prof a
    where
        a.nr_sequencia = NR_SEQUENCIA_P
    and
        ((obter_codigo_usuario(NM_USUARIO_P) = a.cd_pessoa_fisica
    OR
        (select NR_SEQ_FUNCAO_PF from PESSOA_FISICA x where x.CD_PESSOA_FISICA = (obter_codigo_usuario(NM_USUARIO_P))) = a.nr_seq_funcao_pf));
        
    return qt_registros;
END OBTER_USUARIO_COSIGN;
/