create or replace
function obter_se_servico_acomp (nr_seq_atend_serv_dia_p		number)
				return Varchar2 is

ds_retorno_w	varchar2(1);

begin



select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	nut_atend_acompanhante
where	nr_seq_atend_serv_dia = nr_seq_atend_serv_dia_p;


return	ds_retorno_w;

end obter_se_servico_acomp;
/