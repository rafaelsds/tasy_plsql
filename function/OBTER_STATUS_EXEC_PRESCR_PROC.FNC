create or replace
function Obter_Status_exec_prescr_proc	(	nr_prescricao_p	number,
						nr_seq_prescricao_p	number)
 		    	return varchar2 is
ie_status_execucao_w	varchar2(50);
begin

if	(nr_prescricao_p	is not null) and
	(nr_seq_prescricao_p	is not null) then
	select	max(ie_status_execucao)
	into	ie_status_execucao_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_seq_prescricao_p;
end if;

return	ie_status_execucao_w;

end Obter_Status_exec_prescr_proc;
/