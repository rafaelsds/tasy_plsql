create or replace
function obter_se_obriga_campo_proc 	(nr_prescricao_p	number,
					nr_seq_exame_p		number,
					nr_seq_proc_interno_p	number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					dt_prev_execucao_p	date,
					ds_justificativa_p	varchar2,
					cd_material_exame_p	varchar2,
					cd_setor_atendimento_p	number,
					cd_intervalo_p		varchar2,
					ie_campo_p		number)
 		    	return varchar2 is
			
/*
Junior - OS 493496
Op��o ie_campo_p 999 para verificar se apresenta tela para informar justificativa (somente se ie_campo_obriga da tabela = 6 - Justificativa)
*/
			
ie_retorno_w	varchar2(1) := 'N';
/* procedimento */
cd_procedimento_w		procedimento.cd_procedimento%type;
ie_origem_proced_w		procedimento.ie_origem_proced%type;
cd_material_exame_w		varchar2(20);
dt_prev_execucao_w		date;
ie_proced_lado_w		varchar2(15);
ie_lado_w			varchar2(15);
ie_situacao_proced_w		varchar2(1);
nr_seq_exame_w			number(10);
cd_especialidade_proced_w	number(15,0);
cd_setor_proced_w		number(5,0);
ie_classificacao_w		varchar2(1);
qt_procedimento_w		number(8,3);
nr_seq_proc_interno_w		number(10,0);
ds_dado_clinico_w		varchar2(2000);
ds_resumo_clinico_w		varchar2(2000);
nr_seq_grupo_exame_w		number(10,0);
cd_area_procedimento_w		number(15,0);
cd_grupo_procedimento_w		number(15,0);
nr_seq_subgrupo_w		number(10,0);
dt_prev_execucao_mi_w		date;
nr_seq_prot_glic_w		number(10,0);
ie_setor_exec_proced_w		varchar2(1);
ie_proced_agora_w		varchar2(1);
cd_setor_atendimento_w		number(5,0);
ie_consiste_medic_proc_w	varchar2(1);
ie_consiste_prescr_proc_w	varchar2(1);
cd_estab_prescr_w		number(5,0);
ie_consiste_exame_dia_w		varchar2(1);
cd_convenio_w			number(5,0);
cd_categoria_w			varchar2(10);
cd_tipo_acomodacao_w		number(4,0);
ie_tipo_atendimento_w		number(3,0);
cd_setor_prescricao_w		number(5,0);
cd_tipo_convenio_w		Number(2);
cd_tipo_procedimento_w		number(10,0);
cd_perfil_w			number(15);

begin

cd_procedimento_w := cd_procedimento_p;
ie_origem_proced_w := ie_origem_proced_p;

if ( ( nr_seq_proc_interno_p is not null) and
     ( (nvl(cd_procedimento_p, 0) = 0) or ie_origem_proced_p is null ) ) then
    select  cd_procedimento,
            ie_origem_proced
    into    cd_procedimento_w,
            ie_origem_proced_w
    from    proc_interno
    where   nr_sequencia = nr_seq_proc_interno_p;

end if;

cd_perfil_w := wheb_usuario_pck.get_cd_perfil;

select	obter_especialidade_proced(cd_procedimento_w,ie_origem_proced_w),
        a.cd_estabelecimento,
        obter_tipo_atendimento(a.nr_atendimento),
        obter_grupo_exame_lab(nr_seq_exame_p),
        c.cd_area_procedimento,
        c.cd_grupo_proc,
        nvl(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento_w,ie_origem_proced_w,'C','S'),'S'),1,10),0),
        a.cd_setor_atendimento,
        c.cd_tipo_procedimento,
        Obter_Tipo_Convenio(obter_convenio_atendimento(a.nr_atendimento))
into	cd_especialidade_proced_w,
        cd_estab_prescr_w,
        ie_tipo_atendimento_w,
        nr_seq_grupo_exame_w,
        cd_area_procedimento_w,
        cd_grupo_procedimento_w,
        nr_seq_subgrupo_w,
        cd_setor_prescricao_w,
        cd_tipo_procedimento_w,
        cd_tipo_convenio_w
from	Estrutura_Procedimento_v c,
	    prescr_medica a		
where	cd_procedimento_w	= c.cd_procedimento
and	    ie_origem_proced_w	= c.ie_origem_proced
and	    a.nr_prescricao		= nr_prescricao_p;
	
select	decode(count(*),0,'N','S')
into	ie_retorno_w
from	regra_indic_clinica_obrig
where	nvl(nr_seq_exame_lab,nvl(nr_seq_exame_p,0))		= nvl( nr_seq_exame_p,0)
and	    nvl(nr_seq_grupo_lab,nvl(nr_seq_grupo_exame_w,0))	= nvl( nr_seq_grupo_exame_w,0)
and	    nvl(nr_seq_exame_interno,nvl(nr_seq_proc_interno_p,0))	= nvl( nr_seq_proc_interno_p,0)
and	    nvl(cd_area_procedimento,nvl(cd_area_procedimento_w,0))	= nvl( cd_area_procedimento_w,0)
and	    nvl(cd_especialidade,nvl(cd_especialidade_proced_w,0))	= nvl( cd_especialidade_proced_w,0)
and	    nvl(cd_grupo_proc,nvl(cd_grupo_procedimento_w,0))	= nvl( cd_grupo_procedimento_w,0)
and	    nvl(cd_procedimento,nvl(cd_procedimento_w,0))		= nvl( cd_procedimento_w,0)
and	    nvl(ie_origem_proced,nvl(ie_origem_proced_w,0))		= nvl( ie_origem_proced_w,0)
and	    nvl(cd_intervalo,nvl(cd_intervalo_p,0))			= nvl( cd_intervalo_p,0)
and	    nvl(nr_seq_subgrupo,nr_seq_subgrupo_w)			= nr_seq_subgrupo_w
and	    nvl(cd_perfil,nvl(cd_perfil_w,0))			= nvl(cd_perfil_w,0)
and	    nvl(cd_setor_prescricao,nvl(cd_setor_prescricao_w,0))	= nvl( cd_setor_prescricao_w,0)
and	    nvl(cd_setor_executor,nvl(cd_setor_atendimento_p,0))	= nvl( cd_setor_atendimento_p,0)
and	    nvl(cd_tipo_procedimento, nvl(cd_tipo_procedimento_w,0)) = nvl( cd_tipo_procedimento_w,0)
and	    nvl(ie_tipo_convenio, nvl(cd_tipo_convenio_w,0))	= nvl( cd_tipo_convenio_w,0)
and	    Obter_Regra_obrig_proc(nr_sequencia, ie_tipo_atendimento_w, cd_material_exame_p ) = 'S'
and	    cd_estabelecimento					= cd_estab_prescr_w
and	    ((hr_prev_inicial	is null) or
	     (to_char(dt_prev_execucao_p,'hh24:mi')	>= substr(hr_prev_inicial,1,5)))
and	    ((hr_prev_final		is null) or
	     (to_char( dt_prev_execucao_p,'hh24:mi')	<= substr(hr_prev_final,1,5)))
and	    ((nvl(ie_campo_obriga,'0') = to_char(ie_campo_p)) or
	     ((ie_campo_p = 999) and (ie_campo_obriga = '6') and (nvl(ie_justif_rotina,'N') = 'S')));
return	ie_retorno_w;

end obter_se_obriga_campo_proc;
/
