create or replace
function Obter_bolsa_hemocomponente(nr_prescricao_p		number,
				nr_sequencia_p		number)
 		    	return varchar2 is

cont_w		number(15,0);
nr_bolsa_w	varchar2(50);

begin

select	max(nr_bolsa)
into	nr_bolsa_w
from	prescr_proc_bolsa
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_procedimento = nr_sequencia_p;

return	nr_bolsa_w;

end Obter_bolsa_hemocomponente;
/
