create or replace
function obter_dados_mat_conv_conta(cd_material_p		number,
				  ie_tipo_p	Varchar2,
				  cd_convenio_p number,
				  cd_estabelecimento_p number)
 		    	return varchar2 is

ds_retorno_w varchar2(255);
cd_material_conta_w number(10);
		
/* C - C�digo do material conta
*/		
begin

select	cd_material_conta	
into	cd_material_conta_w
from	material_convenio
where	cd_convenio = cd_convenio_p
and	cd_material = cd_material_p
and	cd_estabelecimento = cd_estabelecimento_p;

if (ie_tipo_p = 'C') then
	ds_retorno_w := cd_material_conta_w;
end if;	

return	ds_retorno_w;

end obter_dados_mat_conv_conta;
/
