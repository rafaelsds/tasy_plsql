create or replace
function obter_inf_doador( nr_seq_doador_p	number,
			   ie_opcao_p		varchar2)
 		    	return varchar2 is

cd_pessoa_fisica_w	varchar2(14);
nm_pessoa_fisica_w	varchar2(100);			
begin

select	max(cd_pessoa_fisica),
	max(obter_nome_pf(cd_pessoa_fisica))
into	cd_pessoa_fisica_w,
	nm_pessoa_fisica_w
from   	san_doacao
where  	nr_sequencia = nr_seq_doador_p;

if (ie_opcao_p = 'C') then
	return	cd_pessoa_fisica_w;
else
	return nm_pessoa_fisica_w;
end if;

end obter_inf_doador;
/

     