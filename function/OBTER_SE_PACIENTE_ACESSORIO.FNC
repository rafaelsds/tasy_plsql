create or replace
function obter_se_paciente_acessorio(	cd_pessoa_fisica_p		varchar2,
					cd_procedimento_p		number,
					ie_origem_proced_p		number)
 		    	return varchar2 is

nr_seq_acessorio_w	number(10,0);
cont_w			number(10,0);
ie_retorno_w		varchar2(10) := 'N';

cursor c01 is
select	b.nr_seq_acessorio
from	acessorio_pac_proced b
where	b.cd_procedimento	= cd_procedimento_p
and	b.ie_origem_proced	= ie_origem_proced_p;

begin

open C01;
loop
fetch C01 into	
	nr_seq_acessorio_w;
exit when C01%notfound;
	
	select	count(*)
	into	cont_w
	from	paciente_acessorio
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	nr_seq_acessorio	= nr_seq_acessorio_w
	and	dt_liberacao		is not null;
	
	if	(cont_w > 0) then
		ie_retorno_w	:= 'S';
		exit;
	end if;
	
end loop;
close C01;

return	ie_retorno_w;

end obter_se_paciente_acessorio;
/
