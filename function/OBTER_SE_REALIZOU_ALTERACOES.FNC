CREATE OR REPLACE FUNCTION OBTER_SE_REALIZOU_ALTERACOES(wNrSeqOrdemServ NUMBER) RETURN VARCHAR2 IS
    resp NUMBER;  
BEGIN
    SELECT Sum(cont) cont INTO resp
    FROM (
          SELECT Count(1) cont FROM tasy.log_alt_ordem_servico@whebl02_orcl 
          WHERE nr_seq_os = wNrSeqOrdemServ
          UNION
          SELECT Count(1) cont FROM tasy.reg_Service_order_pr@whebl02_orcl
          WHERE nr_service_order = wNrSeqOrdemServ
          UNION
          SELECT Count(1) cont FROM tasy.REG_OBJECT_LOG@whebl02_orcl 
          WHERE nr_service_order = wNrSeqOrdemServ
          UNION
          SELECT Count(1) cont FROM man_commit_git
          WHERE nr_seq_ordem_serv = wNrSeqOrdemServ
         );

  IF resp > 0 THEN
    RETURN 'S';
  ELSE
    RETURN 'N';
  END IF;
END OBTER_SE_REALIZOU_ALTERACOES;
/
