create or replace
function pls_obter_dados_camara_comp(	nr_seq_lote_p		number,
					ie_opcao_p		varchar2	) return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

ds_retorno_w		varchar2(255) := null;
vl_saldo_w		titulo_receber.vl_saldo_titulo%type;
nr_titulo_pag_w		titulo_pagar.nr_titulo%type;
nr_titulo_rec_w		titulo_receber.nr_titulo%type;
tx_administrativa_w	pls_camara_compensacao.tx_administrativa%type;
ie_incidencia_taxa_w	pls_camara_compensacao.ie_incidencia_taxa%type;
vl_receber_camara_w	titulo_receber.vl_saldo_titulo%type := 0;
ie_considera_tx_baixa_w	pls_camara_compensacao.ie_considera_tx_baixa%type;

begin

if	(ie_opcao_p = 'SFCC') then
	select	max(nr_sequencia)
	into	ds_retorno_w
	from	ptu_camara_compensacao
	where	nr_seq_lote_camara = nr_seq_lote_p;

elsif	(ie_opcao_p = 'DS') or
	(ie_opcao_p = 'DI') then -- Descri��o saldo
	select	sum(vl_saldo)
	into	vl_saldo_w
	from	(select	sum(b.vl_saldo_titulo) vl_saldo
		from	titulo_receber b,
			pls_titulo_lote_camara a
		where	a.nr_titulo_receber	= b.nr_titulo
		and	a.nr_seq_lote_camara	= nr_seq_lote_p
		union all
		select	sum(b.vl_saldo_titulo)*-1 vl_saldo
		from	titulo_pagar b,
			pls_titulo_lote_camara a
		where	a.nr_titulo_pagar	= b.nr_titulo
		and	a.nr_seq_lote_camara	= nr_seq_lote_p);

	select	pls_obter_titulo_camara_comp(b.nr_sequencia,'R'),
		pls_obter_titulo_camara_comp(b.nr_sequencia,'P'),
		nvl(a.tx_administrativa,1),
		nvl(a.ie_incidencia_taxa,'T'),
		nvl(a.ie_considera_tx_baixa,'N')
	into	nr_titulo_rec_w,
		nr_titulo_pag_w,
		tx_administrativa_w,
		ie_incidencia_taxa_w,
		ie_considera_tx_baixa_w
	from	pls_lote_camara_comp	b,
		pls_camara_compensacao	a
	where	a.nr_sequencia		= b.nr_seq_camara
	and	b.nr_sequencia		= nr_seq_lote_p;

	if	(ie_considera_tx_baixa_w = 'S') then
		if	(ie_incidencia_taxa_w = 'T') then
			select	nvl(sum(b.vl_saldo_titulo),0)
			into	vl_receber_camara_w
			from	titulo_receber 		b,
				pls_titulo_lote_camara	a
			where	a.nr_titulo_receber	= b.nr_titulo
			and	a.nr_seq_lote_camara	= nr_seq_lote_p
			and	b.ie_origem_titulo	= '13'; -- Origem OPS - Faturamento

		elsif	(ie_incidencia_taxa_w = 'N') then
			select	nvl(sum(c.vl_total_ndc),0)
			into	vl_receber_camara_w
			from	pls_fatura		c,
				titulo_receber 		b,
				pls_titulo_lote_camara	a
			where	a.nr_titulo_receber	= b.nr_titulo
			and	c.nr_titulo		= b.nr_titulo
			and	a.nr_seq_lote_camara	= nr_seq_lote_p;

		elsif	(ie_incidencia_taxa_w = 'F') then
			select	nvl(sum(c.vl_fatura),0)
			into	vl_receber_camara_w
			from	pls_fatura		c,
				titulo_receber 		b,
				pls_titulo_lote_camara	a
			where	a.nr_titulo_receber	= b.nr_titulo
			and	c.nr_titulo		= b.nr_titulo
			and	a.nr_seq_lote_camara	= nr_seq_lote_p;
		end if;
		vl_saldo_w := vl_saldo_w - (vl_receber_camara_w * tx_administrativa_w/100);
	end if;

	if	(vl_saldo_w = 0) then
		if	(nr_titulo_rec_w is not null) then
			ds_retorno_w := 'Credor (T�tulo gerado)';

		elsif	(nr_titulo_pag_w is not null) then
			ds_retorno_w := 'Devedor (T�tulo gerado)';
		end if;

		if	(ie_opcao_p = 'DI') then
			if	(nr_titulo_rec_w is not null) then
				ds_retorno_w := 'C';

			elsif	(nr_titulo_pag_w is not null) then
				ds_retorno_w := 'D';
			end if;
		end if;
	end if;

	if	(vl_saldo_w > 0) then
		ds_retorno_w := 'Credor (Em aberto)';
	elsif	(vl_saldo_w < 0) then
		ds_retorno_w := 'Devedor (Em aberto)';
	end if;

	if	(ie_opcao_p = 'DI') then
		if	(vl_saldo_w > 0) then
			ds_retorno_w := 'C';

		elsif	(vl_saldo_w < 0) then
			ds_retorno_w := 'D';
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_dados_camara_comp;
/