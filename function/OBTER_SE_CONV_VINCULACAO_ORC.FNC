create or replace
function Obter_se_conv_vinculacao_orc(cd_convenio_p		number,
				      cd_estabelecimento_p	number)
				return varchar2 is

ie_liberado_w	varchar2(1);				
				
begin

ie_liberado_w:= 'S';

select 	nvl(max(ie_permite_vinc_orc),'S')
into	ie_liberado_w
from 	convenio_estabelecimento
where 	cd_convenio = cd_convenio_p
and 	cd_estabelecimento = cd_estabelecimento_p;

return	ie_liberado_w;

end Obter_se_conv_vinculacao_orc;
/