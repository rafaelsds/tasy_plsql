create or replace 
function Obter_indice_Constraint(
				ds_dono_p		Varchar2,
				nm_tabela_p		Varchar2,		
				nm_integridade_p	Varchar2,
				ie_opcao_p		Varchar2)
				return varchar2 is

ds_retorno_w		varchar2(254);
ds_virgula_w		Varchar2(01);
nr_posicao_w		Number(15,0);
nm_atributo_w		Varchar2(50);
nm_indice_w		Varchar2(50);
qt_coluna_w		Number(15,0);

Cursor C01 is
	select	nm_indice
	from	Indice a
	where	upper(ie_opcao_p)	= 'D'
	and	nm_tabela		= nm_tabela_p
	and	exists(
		select	1
		from	indice_atributo b,
			integridade_atributo c
		where	a.nm_tabela	= b.nm_tabela
		and	a.nm_tabela	= c.nm_tabela
		and	c.nm_integridade_referencial	= nm_integridade_p
		and	b.nm_indice	= a.nm_indice
		and	b.nm_atributo	= c.nm_atributo
		and	b.nr_sequencia= c.ie_sequencia_criacao) 
	and	obter_se_col_igual(upper(ds_dono_p), nm_tabela_p, nm_integridade_p,
		a.nm_indice, ie_opcao_p) = 'S'
	union all
	select	index_name	
	from	user_indexes a
	where	ie_opcao_p		= 'B'
	and	table_owner		= upper(ds_dono_p)
	and	table_name		= nm_tabela_p
	and	exists(
		select	1
		from	user_ind_columns b,
			user_cons_columns c
		where	a.table_name		= b.table_name
		and	a.table_name		= c.table_name
		and	c.constraint_name	= nm_integridade_p
		and	a.index_name		= b.index_name
		and	b.column_name		= c.column_name
		and	b.column_position	= c.position)
	and	obter_se_col_igual(upper(ds_dono_p), nm_tabela_p, nm_integridade_p,
		a.index_name, ie_opcao_p) = 'S'
		order by 1;
	
BEGIN

ds_retorno_w		:= '';
ds_virgula_w		:= '';

OPEN C01;
LOOP
FETCH C01 into
	nm_indice_w;
exit when c01%notfound;
	ds_retorno_w	:= ds_retorno_w || ds_virgula_w || nm_indice_w;
	ds_virgula_w	:= ',';
END LOOP;
CLOSE C01;

return	ds_retorno_w;

END Obter_Indice_Constraint;
/