create or replace
function hd_obter_desc_item_controle	(nr_seq_item_controle_p		number,
					ie_opcao_p			number)
					return varchar2 is
					
ds_item_w	varchar2(255);

begin

if	(nvl(nr_seq_item_controle_p,0) > 0) then

	select 	substr(max(ds_item),1,80)
	into	ds_item_w
	from	hd_item_controle_exame
	where	nr_sequencia = nr_seq_item_controle_p;

end if;

return	ds_item_w;

end hd_obter_desc_item_controle;
/