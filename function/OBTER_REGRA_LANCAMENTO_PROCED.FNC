create or replace
function Obter_Regra_Lancamento_Proced
		(nr_seq_proced_p		number,
		nr_seq_evento_p			number)
		return number is

nr_atendimento_w		number(10,0);
cd_convenio_w			number(05,0);
cd_categoria_w		varchar2(10);
dt_execucao_w			date;
cd_medico_executor_w		varchar2(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10,0);
cd_area_proced_w		number(15);
cd_especialidade_proced_w	number(15);
cd_grupo_proced_w		number(15);
ie_tipo_atendimento_w	number(03,0);
cd_estabelecimento_w		number(04,0);
cd_edicao_amb_w		number(06,0);
cd_setor_atendimento_w	number(05,0);
nr_seq_regra_w		number(10,0);
ie_tipo_convenio_w	number(2);
nr_seq_proc_interno_w	number(10,0);

cd_material_w		material.cd_material%type;
nr_seq_familia_w	material.nr_seq_familia%type;
cd_grupo_material_w	grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w	subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w	classe_material.cd_classe_material%type;

cursor	c01 is
	select	a.nr_sequencia
	from 	regra_Lanc_Automatico a
	where	a.cd_estabelecimento						= cd_estabelecimento_w
	and	nvl(a.cd_convenio, cd_convenio_w)				= cd_convenio_w
	and	nvl(a.cd_categoria, cd_categoria_w)			= cd_categoria_w
	and	nvl(a.ie_tipo_convenio, ie_tipo_convenio_w)		= ie_tipo_convenio_w
	and	(nvl(a.ie_tipo_atendimento,nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,0))
	and 	(nvl(a.nr_seq_evento, nr_seq_evento_p)			= nr_seq_evento_p or
		nr_seq_evento_p is null)
	and	(nvl(a.cd_setor_atendimento, nvl(cd_setor_atendimento_w,0))	= nvl(cd_setor_atendimento_w,0))
	and	(nvl(a.cd_medico, nvl(cd_medico_executor_w,'0'))		= nvl(cd_medico_executor_w,'0'))
	and	(nvl(a.cd_procedimento,cd_procedimento_w) 		= cd_procedimento_w or
		cd_procedimento_w is null)
	and	(nvl(a.ie_origem_proced,ie_origem_proced_w) 		= ie_origem_proced_w or
		ie_origem_proced_w is null)
	and	(nvl(a.cd_area_procedimento,cd_area_proced_w)		= cd_area_proced_w or
		cd_area_proced_w is null)
	and	(nvl(a.cd_especialidade_proc,cd_especialidade_proced_w)	= cd_especialidade_proced_w or
		cd_especialidade_proced_w is null)
	and	(nvl(a.cd_grupo_proc,cd_grupo_proced_w)			= cd_grupo_proced_w or
		cd_grupo_proced_w is null)
	and	(nvl(a.cd_edicao_amb,cd_edicao_amb_w)			= cd_edicao_amb_w or
		cd_edicao_amb_w is null)
	and	(nvl(a.nr_seq_proc_interno, nr_seq_proc_interno_w) 	= nr_seq_proc_interno_w)
	and	nvl(a.cd_material,nvl(cd_material_w,0)) = nvl(cd_material_w,0)
	and	nvl(a.cd_grupo_material,nvl(cd_grupo_material_w,0)) = nvl(cd_grupo_material_w,0)
	and	nvl(a.cd_subgrupo_material,nvl(cd_subgrupo_material_w,0)) = nvl(cd_subgrupo_material_w,0)
	and	nvl(a.cd_classe_material,nvl(cd_classe_material_w,0)) = nvl(cd_classe_material_w,0)
	and	nvl(a.nr_seq_familia,nvl(nr_seq_familia_w,0)) = nvl(nr_seq_familia_w,0)
	and	ie_situacao					= 'A'
	order by
			nvl(a.cd_medico,0),
			nvl(a.cd_setor_atendimento,0),
			nvl(a.ie_tipo_atendimento,0),
			nvl(a.ie_tipo_convenio,0),
			nvl(a.cd_convenio,0),
			nvl(a.cd_edicao_amb,0),
			nvl(a.cd_procedimento,0),
			nvl(a.cd_grupo_proc,0),
			nvl(a.cd_especialidade_proc,0),
			nvl(a.cd_area_procedimento,0);
begin

if	(nr_seq_evento_p  = 132) then
	nr_seq_proc_interno_w	:= 0;
	
	select	a.cd_material,
		a.nr_atendimento,
		a.cd_convenio,
		a.cd_categoria,
		a.dt_atendimento,
		a.cd_setor_atendimento,
		b.cd_grupo_material,
		b.cd_subgrupo_material,
		b.cd_classe_material,
		b.nr_seq_familia,
		obter_tipo_convenio(a.cd_convenio)
	into	cd_material_w,
		nr_atendimento_w,
		cd_convenio_w,
		cd_categoria_w,
		dt_execucao_w,
		cd_setor_atendimento_w,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		nr_seq_familia_w,
		ie_tipo_convenio_w
	from	material_atend_paciente a,
		estrutura_material_v b
	where	a.cd_material = b.cd_material
	and	a.nr_sequencia		= nr_seq_proced_p;
else
	select	a.cd_procedimento,
		a.ie_origem_proced,
		a.nr_atendimento,
		a.cd_medico_executor,
		a.cd_convenio,
		a.cd_categoria,
		a.dt_procedimento,
		a.cd_setor_atendimento,
		b.cd_area_procedimento,
		b.cd_especialidade,
		b.cd_grupo_proc,
		obter_tipo_convenio(a.cd_convenio),
		nvl(a.nr_seq_proc_interno,0)
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_atendimento_w,
		cd_medico_executor_w,
		cd_convenio_w,
		cd_categoria_w,
		dt_execucao_w,
		cd_setor_atendimento_w,
		cd_area_proced_w,
		cd_especialidade_proced_w,
		cd_grupo_proced_w,
		ie_tipo_convenio_w,
		nr_seq_proc_interno_w
	from	procedimento_paciente a,
		estrutura_procedimento_v b
	where	a.nr_sequencia		= nr_seq_proced_p
	and	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced	= b.ie_origem_proced;
end if;

select	ie_tipo_atendimento,
	cd_estabelecimento
into	ie_tipo_atendimento_w,
	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento		= nr_atendimento_w;

select	max(cd_edicao_amb)
into	cd_edicao_amb_w
from	convenio_amb
where	(cd_estabelecimento	= cd_estabelecimento_w)
  and	(cd_convenio		= cd_convenio_w)
  and	(cd_categoria		= cd_categoria_w)
  and	(nvl(ie_situacao,'A')	= 'A')
  and	(dt_inicio_vigencia	=
	(select max(dt_inicio_vigencia)
	from	convenio_amb a
	where	(a.cd_estabelecimento	= cd_estabelecimento_w)
	and	(a.cd_convenio		= cd_convenio_w)
	and	(a.cd_categoria		= cd_categoria_w)
	and	(nvl(a.ie_situacao,'A')	= 'A')
	and	(a.dt_inicio_vigencia	<= dt_execucao_w)));


open	c01;
loop
fetch	c01 into nr_seq_regra_w;
exit	when c01%notfound;
end loop;
close c01;

return	nr_seq_regra_w;

end Obter_Regra_Lancamento_Proced;
/
