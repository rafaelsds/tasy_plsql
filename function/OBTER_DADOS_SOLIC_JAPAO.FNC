create or replace function obter_dados_solic_japao (
			vl_atributo_entrada_p	number,
			ds_macro_p		varchar2)
RETURN VARCHAR2 AS 
vl_retorno_w		Varchar2(4000);
BEGIN
 	if	(ds_macro_p = '@TODAY') then
		select	SYSDATE INTO vl_retorno_w
		FROM DUAL;    
    elsif	(ds_macro_p	= '@TOMORROW') then
		select SYSDATE + 1
		into	vl_retorno_w
		from DUAL;
    elsif	(ds_macro_p	= '@YESTERDAY') then
		select SYSDATE - 1
		into	vl_retorno_w
		from DUAL;
    elsif (ds_macro_p	= '@BLOODTYPEABO') then
		select IE_TIPO_SANGUE  into vl_retorno_w 
        from PESSOA_FISICA PF, ATENDIMENTO_PACIENTE AP 
        where PF.CD_PESSOA_FISICA = AP.CD_PESSOA_FISICA
        AND AP.NR_ATENDIMENTO =vl_atributo_entrada_p;   
    elsif (ds_macro_p	= '@BLOODTYPERH') then
		select IE_FATOR_RH  into vl_retorno_w 
        from PESSOA_FISICA PF, ATENDIMENTO_PACIENTE AP 
        where PF.CD_PESSOA_FISICA = AP.CD_PESSOA_FISICA
        AND AP.NR_ATENDIMENTO = vl_atributo_entrada_p;              
    elsif	(ds_macro_p	= '@EXAMDATE') then
		select max(dt_agenda)  into vl_retorno_w 
        FROM AGENDA_PACIENTE
        where nr_atendimento = vl_atributo_entrada_p;    
    elsif	(ds_macro_p	= '@EXAMKACD') then
        WITH MAX_DATA AS (select nr_atendimento, dt_agenda
        from AGENDA_PACIENTE
        WHERE NR_ATENDIMENTO = vl_atributo_entrada_p),
        SETOR AS (
        select DISTINCT APC.nr_atendimento, CD_SETOR_ATENDIMENTO, MD.DT_AGENDA
        from AGENDA_PACIENTE APC, MAX_DATA MD, PROCEDIMENTO pr
        WHERE APC.NR_ATENDIMENTO = MD.NR_ATENDIMENTO
        AND APC.DT_AGENDA = MD.DT_AGENDA
        and APC.cd_procedimento = pr.cd_procedimento)
       SELECT XMLAGG(XMLELEMENT(E,SE.CD_SETOR_ATENDIMENTO||' - ')).EXTRACT('//text()').getClobVal() RESULTADO
        into vl_retorno_w
        FROM SETOR SE;  
    elsif	(ds_macro_p	= '@EXAMNAME') then
        SELECT XMLAGG(XMLELEMENT(E,CONSULTA.RESULTADO||' - ')).EXTRACT('//text()').getClobVal() 
        INTO vl_retorno_w
        FROM 
        (SELECT DISTINCT TRIM(pr.ds_procedimento) RESULTADO, TO_CHAR(HR_INICIO,'HH:MI:SS') HR_INICIO
        from AGENDA_PACIENTE APC, PROCEDIMENTO pr
        WHERE APC.NR_ATENDIMENTO = vl_atributo_entrada_p
        AND APC.DT_AGENDA IN (SELECT MAX(APS.dt_agenda)
        from AGENDA_PACIENTE APS
        WHERE APS.NR_ATENDIMENTO = APC.NR_ATENDIMENTO)
        and APC.cd_procedimento = pr.cd_procedimento
        ORDER BY 2) CONSULTA;
    elsif	(ds_macro_p	= '@EXAMTIME') then
        SELECT XMLAGG(XMLELEMENT(E,CONSULTA.HR_INICIO||' - ')).EXTRACT('//text()').getClobVal() 
        INTO vl_retorno_w
        FROM
        (select DISTINCT TRIM(pr.ds_procedimento),TO_CHAR(HR_INICIO,'HH:MI:SS') HR_INICIO
        from AGENDA_PACIENTE APC,  PROCEDIMENTO pr
        WHERE APC.NR_ATENDIMENTO = vl_atributo_entrada_p
        AND APC.DT_AGENDA IN (select MAX(dt_agenda)
        from AGENDA_PACIENTE
        WHERE NR_ATENDIMENTO = APC.NR_ATENDIMENTO)
        and APC.cd_procedimento = pr.cd_procedimento
        ORDER BY 2) CONSULTA;  
    elsif (ds_macro_p	LIKE '@EXAMUSER%') then
        WITH MAX_DATA AS (select nr_atendimento, dt_agenda
        from AGENDA_PACIENTE
        WHERE NR_ATENDIMENTO = vl_atributo_entrada_p),
        EXAME AS (
        select DISTINCT APC.nr_atendimento,  CD_MEDICO, 
        obter_nome_pf(CD_MEDICO) medico_solicitante_desc, MD.DT_AGENDA
        from AGENDA_PACIENTE APC, MAX_DATA MD, PROCEDIMENTO pr
        WHERE APC.NR_ATENDIMENTO = MD.NR_ATENDIMENTO
        AND APC.DT_AGENDA = MD.DT_AGENDA
        and APC.cd_procedimento = pr.cd_procedimento)
        SELECT XMLAGG(XMLELEMENT(E,DECODE (ds_macro_p, '@EXAMUSERID', CD_MEDICO, medico_solicitante_desc)
        ||' - ')).EXTRACT('//text()').getClobVal()
        INTO vl_retorno_w
        FROM EXAME E;
    elsif	(ds_macro_p	= '@CURINLEAVEPRES') then
	with max_dt as (
        select a.nr_atendimento, max(Obter_dados_prescricao(a.nr_prescricao,'D')) dt_presc
        from prescr_medica a 
        where a.nr_atendimento = vl_atributo_entrada_p
        and a.ie_prescricao_alta = 'S'
        group by a.nr_atendimento)
        select distinct Obter_dados_prescricao(a.nr_prescricao,'D') || ' - ' ||
                Obter_dados_prescricao(a.nr_prescricao,'NP') || ' - ' ||
                a.DS_JUSTIFICATIVA dados_prec
                into vl_retorno_w
                from prescr_medica a, max_dt mad
                where ie_prescricao_alta = 'S'
                and a.nr_atendimento = mad.nr_atendimento
                and Obter_dados_prescricao(a.nr_prescricao,'D') = mad.dt_presc
                and a.nr_atendimento = vl_atributo_entrada_p;
    elsif	(ds_macro_p	= '@CURINDISEASE') then  
                SELECT SUBSTR(OBTER_DESC_CID(OBTER_CID_ATENDIMENTO(NR_ATENDIMENTO,'P')),1,100) ds_cid
                into vl_retorno_w
                FROM ATENDIMENTO_PACIENTE
                WHERE NR_ATENDIMENTO = vl_atributo_entrada_p
                and ie_tipo_atendimento = 1;          
    elsif	(ds_macro_p	= '@USERSECTEL') then
                WITH max_data AS ( select nr_atendimento, max(dt_entrada) dt_entrada from atendimento_paciente apu
                where apu.nr_atendimento = vl_atributo_entrada_p
                group by nr_atendimento)
                select distinct NR_TELEFONE into vl_retorno_w
                from setor_atendimento sa, atendimento_paciente apu, max_data md
                where apu.nr_atendimento = vl_atributo_entrada_p
                and md.nr_atendimento = apu.nr_atendimento
                and apu.dt_entrada = md.dt_entrada
                and sa.cd_setor_atendimento = apu.cd_setor_usuario_atend;
    elsif	(ds_macro_p	= '@USERSECFAX') then
                SELECT NR_DDI_FAX || ' ' ||  NR_DDD_FAX || ' ' ||c.nr_fax FAX INTO vl_retorno_w
                from empresa a,
                estabelecimento b,
                pessoa_juridica c,
                atendimento_paciente ap
                where a.cd_empresa = b.cd_empresa
                and b.CD_CGC = c.cd_cgc
                and b.cd_estabelecimento = ap.cd_estabelecimento
                and ap.nr_atendimento = vl_atributo_entrada_p;              
    elsif	(ds_macro_p	= '@SALAPACIENTE') then
                    SELECT DS_SETOR_ATENDIMENTO ||' - '|| CD_UNIDADE_BASICA SALA 
                    INTO vl_retorno_w
                    FROM ATENDIMENTO_PACIENTE_V
                    WHERE NR_ATENDIMENTO = vl_atributo_entrada_p;
    elsif	(ds_macro_p	= '@ATTETINGDOCTORS') then
                select substr(obter_nome_pf(max(cd_medico_resp)),1,60)	
                    into 	vl_retorno_w
                    from	atendimento_paciente
                    where	nr_atendimento = vl_atributo_entrada_p;
    elsif	(ds_macro_p	= '@ATTETINGNURSES') then
                select distinct PF.NM_PESSOA_FISICA
                    into 	vl_retorno_w
                    from	atendimento_paciente APC, PESSOA_FISICA PF
                    where CD_PESSOA_RESPONSAVEL IS NOT NULL
                    AND PF.CD_PESSOA_FISICA = APC.CD_PESSOA_RESPONSAVEL
                    AND obter_cargo_PF(APC.CD_PESSOA_RESPONSAVEL, 'C') = TO_CHAR(441)
                    AND APC.NR_ATENDIMENTO = vl_atributo_entrada_p;
    elsif	(ds_macro_p	= '@FAMILYHISTORY') then
                WITH HIST_FAMILIAR AS (
                select A.CD_PESSOA_FISICA,DS_PARENTESCO || ': ' 
                || a.DT_ATUALIZACAO || ' - ' || decode(nm_parente, null , '',nm_parente|| ' - ') ||  DS_DOENCA AS HISTORICO
                    from
                            GRAU_PARENTESCO g, 
                            PACIENTE_ANTEC_CLINICO a,
                            ATENDIMENTO_PACIENTE AP
                    where 	a.CD_PESSOA_FISICA = AP.CD_PESSOA_FISICA
                    AND     AP.NR_ATENDIMENTO = TO_CHAR(vl_atributo_entrada_p)
                    and		g.NR_SEQUENCIA = a.NR_SEQ_PARENTESCO
                    and		a.IE_PACIENTE in ('S','N')
                    and DS_DOENCA is not null)
                SELECT XMLAGG(XMLELEMENT(E,HISTORICO||' / ')).EXTRACT('//text()').getClobVal()
                INTO vl_retorno_w
                from HIST_FAMILIAR;
    elsif	(ds_macro_p	= '@INFECTIONS') then
                WITH INFECCOES AS (SELECT DISTINCT DS_CASO_INFECCAO FROM
                ATENDIMENTO_PACIENTE APC,
                CIH_FICHA_OCORRENCIA CFO,
                CIH_CASO_INFECCAO CCI
                WHERE APC.NR_ATENDIMENTO = CFO.NR_ATENDIMENTO
                AND CFO.CD_CASO_INFECCAO = CCI.CD_CASO_INFECCAO
                AND APC.NR_ATENDIMENTO = vl_atributo_entrada_p)
                SELECT XMLAGG(XMLELEMENT(E,DS_CASO_INFECCAO||' - ')).EXTRACT('//text()').getClobVal() RESULTADO
                INTO vl_retorno_w
                from INFECCOES;               
    elsif	(ds_macro_p	LIKE '@HOS%') then
                SELECT
                CASE  ds_macro_p WHEN '@HOSPNAME' THEN  a.nm_razao_social
                WHEN '@HOSPCEONAME' THEN f.NM_PESSOA_FISICA
                WHEN '@HOSPCEO' THEN a.cd_titular
                WHEN '@HOSPADDR' THEN c.ds_endereco ||' '|| c.ds_bairro ||' - '|| c.ds_municipio ||' - '|| c.sg_estado
                WHEN '@HOSPFAX' THEN NR_DDI_FAX || ' ' ||  NR_DDD_FAX || ' ' ||c.nr_fax
                WHEN '@HOSPTEL' THEN NR_DDI_TELEFONE || ' ' ||NR_DDD_TELEFONE || ' ' || c.nr_telefone
                ELSE CD_CEP END AS INFO_HOSP into vl_retorno_w
                
                from empresa a,
                estabelecimento b,
                pessoa_juridica c,
                pessoa_fisica f,
                atendimento_paciente ap
                where a.cd_empresa = b.cd_empresa
                and b.CD_CGC = c.cd_cgc
                and f.cd_pessoa_fisica = a.cd_titular
                and b.cd_estabelecimento = ap.cd_estabelecimento
                and ap.nr_atendimento = vl_atributo_entrada_p;
    elsif	(ds_macro_p	LIKE '@CURINLEAVEGDATE') then
                SELECT TO_CHAR(dt_alta, 'YYYY')||obter_desc_expressao_idioma(776480,null,12)
                       ||TO_CHAR(dt_alta, 'MM')||obter_desc_expressao_idioma(293215,null,12)
                       ||TO_CHAR(dt_alta, 'DD')||obter_desc_expressao_idioma(287656,null,12)
                as dt_alta
                INTO vl_retorno_w
                FROM atendimento_paciente
                WHERE nr_atendimento = vl_atributo_entrada_p;
    elsif	(ds_macro_p	LIKE '@CURINADMGDATE') then
                SELECT TO_CHAR(DT_INTERNACAO, 'YYYY')||obter_desc_expressao_idioma(776480,null,12)
                       ||TO_CHAR(DT_INTERNACAO, 'MM')||obter_desc_expressao_idioma(293215,null,12)
                       ||TO_CHAR(DT_INTERNACAO, 'DD')||obter_desc_expressao_idioma(287656,null,12)
                AS DT_INTERNACAO 
                INTO vl_retorno_w
                FROM (
                SELECT CASE obter_classif_setor_min(a.nr_atendimento) WHEN 3 THEN a.dt_entrada
                       ELSE DECODE(a.ie_tipo_atendimento, 1, apu.dt_entrada_unidade, 
                       a.dt_entrada)
                       END DT_INTERNACAO
                FROM atendimento_paciente a, atend_paciente_unidade apu
                where a.nr_atendimento = apu.nr_atendimento(+)
                and a.nr_atendimento = vl_atributo_entrada_p
                ) CONSULTA;    
    elsif	(ds_macro_p	LIKE '@NOMEPACIENTEKANA') then
                SELECT PF.NM_PESSOA_FISICA INTO vl_retorno_w
                FROM PESSOA_FISICA PF, ATENDIMENTO_PACIENTE AP
                WHERE PF.CD_PESSOA_FISICA = AP.CD_PESSOA_FISICA
                AND AP.NR_ATENDIMENTO = vl_atributo_entrada_p;
    elsif	(ds_macro_p	LIKE '@SYSGDATE') then
                SELECT TO_CHAR(SYSDATE, 'YYYY')||obter_desc_expressao_idioma(776480,null,12)
                       ||TO_CHAR(SYSDATE, 'MM')||obter_desc_expressao_idioma(293215,null,12)
                       ||TO_CHAR(SYSDATE, 'DD')||obter_desc_expressao_idioma(287656,null,12) as dt_atual
                INTO vl_retorno_w
                FROM dual;     
    end if;
 

    
return	nvl(vl_retorno_w,' ');  
    
END obter_dados_solic_japao;
   
/
