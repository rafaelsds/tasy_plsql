create or replace
FUNCTION Obter_Valor_Macro_Avaliacao(nr_seq_avaliacao_p	NUMBER,
									 ds_macro_p			VARCHAR2,
									 nm_usuario_p		VARCHAR2)
RETURN VARCHAR2 IS

ds_doenca_w						VARCHAR2(32000);
ds_resultado_w					VARCHAR2(4000) := '';
cd_pessoa_fisica_w				VARCHAR2(010);
nm_pessoa_fisica_w				VARCHAR2(060);
ds_idade_w						VARCHAR2(020);
ds_amd_w						VARCHAR2(30);
dt_nascimento_w					DATE;
qt_altura_cm_w					NUMBER(04,1);
ie_sexo_w						VARCHAR2(001);
ds_sexo_w						VARCHAR2(040);
nr_atendimento_w				NUMBER(10,0);
cd_doenca_w						VARCHAR2(10);
dt_entrada_w					DATE;
ds_macro_w						VARCHAR2(255) := UPPER(substr(ds_macro_p,1,255));
ds_convenio_w					VARCHAR2(255);
nm_medico_w						VARCHAR2(200);
ds_diagnostico_w				VARCHAR2(240);
ds_desc_cid_w					VARCHAR2(240);
ds_setor_pac_w					VARCHAR2(240);
nr_prontuario_w					NUMBER(10);
ds_tipo_atend_w					VARCHAR2(240);
cd_medico_referido_w			VARCHAR2(10);
cd_medico_atendimento_w			atendimento_paciente.cd_medico_atendimento%type;
ds_agente_alerg_w				VARCHAR2(2000);
ds_tipo_reacao_w				VARCHAR2(2000);
ds_observacao_w					VARCHAR2(2000);
ie_confirmacao_w				VARCHAR2(3);
ds_doenca_prev_w				VARCHAR2(80);
ds_medic_uso_w					VARCHAR2(2000);
cd_usuario_convenio_w			VARCHAR2(60);
dt_entrada_unidade_w			DATE;
dt_alta_w						VARCHAR2(240);
dt_inicio_cir_w					DATE;
dt_fim_cir_w					DATE;
dt_aplicacao_w					DATE ;
		
ds_endereco_w					VARCHAR2(255);
ds_municipio_w					VARCHAR2(255);
ds_tel_rend_w					VARCHAR2(255);
ds_bairro_w						VARCHAR2(255);
ds_estado_w						VARCHAR2(255);
ds_tel_com_cel_w				VARCHAR2(255);
ds_cep_w						VARCHAR2(255);
ds_conv_ageint_w				VARCHAR2(255);
cd_cart_ageint_w				VARCHAR2(30);
ds_categ_ageint_w				VARCHAR2(255);
nm_med_sol_ageint_w				varchar2(60);
nr_endereco_w					number(5);
ds_complemento_w				varchar2(40);
ds_topografia_radioterapia_w	varchar2(255);
cd_cido_radioterapia_w			varchar2(10);
cd_doenca_radioterapia_w		varchar2(10);
cd_medico_radioterapia_w		varchar2(10);
cd_fisico_radioterapia_w		varchar2(10);
nr_seq_atend_checklist_w		number(10);
cd_ultima_doenca_def_w	        varchar2(10);
cd_ultima_doenca_pre_w			varchar2(10);
nr_cirurgia_w					cirurgia.nr_cirurgia%type;
dt_diagnostico_w				diagnostico_doenca.dt_diagnostico%type;
nr_seq_gestao_vaga_w			gestao_vaga.nr_sequencia%type;
ds_locale_w			varchar2(255);

CURSOR c01 IS
	SELECT	NVL(NVL(NVL(NVL(NVL(NVL(obter_estrut_princ_ativo(nr_seq_ficha_tecnica),obter_desc_dcb(nr_seq_dcb)),obter_desc_dcb_mat(cd_material,'D')),obter_desc_material(cd_material)),obter_desc_classe_mat(cd_classe_mat)),ds_medic_nao_cad),DECODE(ie_nega_alergias,'N',SUBSTR(obter_desc_alergeno(nr_seq_tipo),1,80),OBTER_DESC_EXPRESSAO(346740))) ds_agente_alerg,
		obter_desc_reacao_alergica(nr_seq_reacao) ds_reacao_alerg,
		ds_observacao,
		ie_confirmacao
	FROM	paciente_alergia a
	WHERE	dt_liberacao IS NOT NULL
	AND	dt_inativacao IS NULL
	AND	((dt_fim IS NULL) OR (SYSDATE BETWEEN NVL(dt_inicio,SYSDATE-1) AND dt_fim + 86399/86400))
	AND	cd_pessoa_fisica = cd_pessoa_fisica_w
	ORDER BY	ds_agente_alerg,
			ds_reacao_alerg;

CURSOR C02 IS
	SELECT	SUBSTR(NVL(SUBSTR(obter_desc_pe_doenca(NR_SEQ_DOENCA),1,80),DS_DOENCA),1,80)
	FROM	paciente_antec_clinico
	WHERE	cd_pessoa_fisica	= cd_pessoa_fisica_w
	AND	dt_inativacao	IS NULL
	AND	dt_fim IS NULL
	AND	((DS_DOENCA	IS NOT NULL) OR (NR_SEQ_DOENCA IS NOT NULL));

CURSOR c03 IS
	SELECT	NVL(a.ds_medicamento,SUBSTR(obter_desc_material(a.cd_material),1,100))
	FROM	paciente_medic_uso a,
		hist_saude_reconciliacao b
	WHERE	b.nr_seq_pac_hist = a.nr_sequencia
	AND	b.nr_atendimento = nr_atendimento_w
	AND	a.dt_liberacao IS NOT NULL
	AND	a.dt_inativacao IS NULL
	AND	a.dt_fim IS NULL;

CURSOR c04 IS
	SELECT	obter_desc_cid_doenca(cd_doenca)
	FROM	diagnostico_doenca
	WHERE	nr_atendimento = nr_atendimento_w;
	
Cursor C05 is
	select	substr(obter_desc_topografia_dor(a.nr_seq_topografia),1,60)
	from	rxt_tumor_topografia a,
		rxt_tumor b
	where   a.nr_seq_tumor = b.nr_sequencia
	and  	b.nr_seq_checklist = nr_seq_atend_checklist_w;
	
cursor busca_macro_cliente is
	select	m.ds_macro ds_macro_cliente,
			f.ds_macro ds_macro_philips
	from	funcao_macro f,
			funcao_macro_cliente m
	where	f.nr_sequencia = m.nr_seq_macro
	and		(f.ds_macro = ds_macro_p)
	and		m.ds_locale = ds_locale_w
	and		f.cd_funcao = 909
	union
	select	m.ds_macro ds_macro_cliente,
			f.ds_macro ds_macro_philips
	from	funcao_macro f,
			funcao_macro_cliente m
	where	f.nr_sequencia = m.nr_seq_macro
	and		(m.ds_macro = ds_macro_p)
	and		m.ds_locale = ds_locale_w
	and		f.cd_funcao = 909
	union
	select	f.ds_macro ds_macro_cliente,
			f.ds_macro ds_macro_philips
	from	funcao_macro f
	where	f.ds_macro = ds_macro_p
	and		f.cd_funcao = 909
	and		ds_locale_w = 'pt_BR';
	
cursor cod_procedimento is
	select 	substr(cd_procedimento_princ,1,240) cd_procedimento_w,
		1 ie_tipo_w
	from   	cirurgia
	where  	nr_cirurgia = nr_cirurgia_w
	union
	select 	substr(a.cd_procedimento,1,240) cd_procedimento_w,
		2 ie_tipo_w
	from   	prescr_procedimento a,
		cirurgia b
	where  	a.nr_prescricao = b.nr_prescricao
	and	b.nr_cirurgia 	= nr_cirurgia_w
	and	not exists (	select 	1 
				from 	cirurgia x
				where	x.nr_cirurgia 		= nr_cirurgia_w
				and	x.cd_procedimento_princ	= a.cd_procedimento
				and	x.ie_origem_proced	= a.ie_origem_proced)
	order by 2;
	
cursor ds_procedimento_cirurgia is
	select 	substr(obter_exame_agenda(cd_procedimento_princ, ie_origem_proced, nr_seq_proc_interno),1,240) ds_procedimento_w,
		1 ie_tipo
	from   	cirurgia
	where  	nr_cirurgia = nr_cirurgia_w
	union
	select 	substr(obter_desc_prescr_proc(a.cd_procedimento,a.ie_origem_proced, a.nr_seq_proc_interno),1,240) ds_procedimento_w,
		2 ie_tipo
	from   	prescr_procedimento a,
		cirurgia b
	where  	a.nr_prescricao = b.nr_prescricao
	and	b.nr_cirurgia 	= nr_cirurgia_w
	and	not exists (	select 	1 
				from 	cirurgia x
				where	x.nr_cirurgia 		= nr_cirurgia_w
				and	x.cd_procedimento_princ	= a.cd_procedimento
				and	x.ie_origem_proced	= a.ie_origem_proced)
	order by 2;

BEGIN
ds_locale_w	:= obtain_user_locale(nm_usuario_p);
if	(ds_macro_p is null) then
	return null;
end if;


if (ds_macro_p is not null) then
	BEGIN
	
		for bmc in busca_macro_cliente loop
		
			if (upper(bmc.ds_macro_cliente) = upper(ds_macro_p)) then
				SELECT	UPPER(substr(max(ds_macro_philips),1,255))
				INTO	ds_macro_w
				FROM	TABLE(search_macros(null, UPPER(substr(bmc.ds_macro_philips,1,255)), null, 'R'))
				WHERE	ROWNUM <= 1;
				
				if	(ds_macro_w is not null) then
					exit;
				end if;
			end if;
		end loop;
		
		if (ds_macro_w is null) 	then
			ds_macro_w := UPPER(substr(ds_macro_p,1,255));
		end if;
	END;
end if;

if	(substr(ds_macro_w,1,1) <> '@') then
	return null;
end if;

if (wheb_usuario_pck.get_cd_funcao = 1002) then
	begin
	select nvl(max(nr_seq_gestao_vaga), 0)
	into nr_seq_gestao_vaga_w
	from avaliacao_gestao_vaga
	where nr_sequencia = nr_seq_avaliacao_p;

	SELECT	cd_pessoa_fisica,
		nr_atendimento,
		obter_cid_atendimento(nr_atendimento, 'P'),
		obter_data_entrada(nr_atendimento),
		SUBSTR(Obter_Dados_Atendimento(nr_atendimento,'NC'),1,40),
		SUBSTR(Obter_Dados_Atendimento(nr_atendimento,'NMR'),1,200),
		SUBSTR(Obter_Diagnostico_atendimento(nr_atendimento),1,240),
		SUBSTR(Obter_Desc_CID_Doenca(obter_cid_atendimento(nr_atendimento, 'P')),1,240),
		SUBSTR(obter_data_diag_atend(nr_atendimento, 0),1,240),
		SUBSTR(obter_setor_atepacu(Obter_AtePacu_Paciente(nr_atendimento,'P'),1),1,240),
		SUBSTR(obter_valor_dominio(12,OBTER_TIPO_ATENDIMENTO(nr_atendimento)),1,240),
		SUBSTR(Obter_Dados_Atendimento(nr_atendimento,'DA'),1,40),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'EN'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'CI'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'T'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'B'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'UF'),''),1,255),
		SUBSTR(NVL(obter_dados_pf(cd_pessoa_fisica,'TCD'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'CEP'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'NR'),''),1,6),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'CO'),''),1,40)
	INTO	cd_pessoa_fisica_w,
		nr_atendimento_w,
		cd_doenca_w,
		dt_entrada_w,
		ds_convenio_w,
		nm_medico_w,
		ds_diagnostico_w,
		ds_desc_cid_w,
		dt_diagnostico_w,
		ds_setor_pac_w,
		ds_tipo_atend_w,
		dt_alta_w,
		ds_endereco_w,
		ds_municipio_w,
		ds_tel_rend_w,
		ds_bairro_w,
		ds_estado_w,
		ds_tel_com_cel_w,
		ds_cep_w,
		nr_endereco_w,
		ds_complemento_w
	FROM	gestao_vaga
	WHERE	nr_sequencia	= nr_seq_gestao_vaga_w;
	end;
else
	begin
	SELECT	cd_pessoa_fisica,
		nr_atendimento,
		obter_cid_atendimento(nr_atendimento, 'P'),
		obter_data_entrada(nr_atendimento),
		SUBSTR(Obter_Dados_Atendimento(nr_atendimento,'NC'),1,40),
		SUBSTR(Obter_Dados_Atendimento(nr_atendimento,'NMR'),1,200),
		SUBSTR(Obter_Diagnostico_atendimento(nr_atendimento),1,240),
		SUBSTR(Obter_Desc_CID_Doenca(obter_cid_atendimento(nr_atendimento, 'P')),1,240),
		SUBSTR(obter_data_diag_atend(nr_atendimento, 0),1,240),
		SUBSTR(obter_setor_atepacu(Obter_AtePacu_Paciente(nr_atendimento,'P'),1),1,240),
		SUBSTR(obter_valor_dominio(12,OBTER_TIPO_ATENDIMENTO(nr_atendimento)),1,240),
		SUBSTR(Obter_Dados_Atendimento(nr_atendimento,'DA'),1,40),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'EN'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'CI'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'T'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'B'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'UF'),''),1,255),
		SUBSTR(NVL(obter_dados_pf(cd_pessoa_fisica,'TCD'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'CEP'),''),1,255),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'NR'),''),1,6),
		SUBSTR(NVL(obter_compl_pf(cd_pessoa_fisica,1,'CO'),''),1,40)
	INTO	cd_pessoa_fisica_w,
		nr_atendimento_w,
		cd_doenca_w,
		dt_entrada_w,
		ds_convenio_w,
		nm_medico_w,
		ds_diagnostico_w,
		ds_desc_cid_w,
		dt_diagnostico_w,
		ds_setor_pac_w,
		ds_tipo_atend_w,
		dt_alta_w,
		ds_endereco_w,
		ds_municipio_w,
		ds_tel_rend_w,
		ds_bairro_w,
		ds_estado_w,
		ds_tel_com_cel_w,
		ds_cep_w,
		nr_endereco_w,
		ds_complemento_w
	FROM	med_avaliacao_paciente
	WHERE	nr_sequencia	= nr_seq_avaliacao_p;
	end;
end if;


SELECT substr(obter_nome_pf(cd_pessoa_fisica),1,60),
	obter_idade(dt_nascimento, SYSDATE, 'A'),
	dt_nascimento,
	qt_altura_cm,
	ie_sexo,
	obter_idade(dt_nascimento, SYSDATE, 'S'),
	nr_prontuario
INTO	nm_pessoa_fisica_w,
	ds_idade_w,
	dt_nascimento_w,
	qt_altura_cm_w,
	ie_sexo_w,
	ds_amd_w,
	nr_prontuario_w
FROM	pessoa_fisica
WHERE	cd_pessoa_fisica	= cd_pessoa_fisica_w;

SELECT	ds_valor_dominio
INTO	ds_sexo_w
FROM	valor_dominio_v
WHERE	cd_dominio		= 4
AND	vl_dominio		= ie_sexo_w;


SELECT max(a.cd_doenca)
into   cd_ultima_doenca_def_w
FROM   diagnostico_doenca a
WHERE  a.nr_atendimento = nr_atendimento_w
AND    ie_tipo_diagnostico = 2
AND    a.dt_diagnostico =  (SELECT MAX(dt_diagnostico) 
	   		    FROM   diagnostico_doenca
			    WHERE  nr_atendimento = a.nr_atendimento 
			    AND    ie_tipo_diagnostico = 2);
			    
SELECT max(a.cd_doenca)
into   cd_ultima_doenca_pre_w
FROM   diagnostico_doenca a
WHERE  a.nr_atendimento = nr_atendimento_w
AND    ie_tipo_diagnostico = 1
AND    a.dt_diagnostico =  (SELECT MAX(dt_diagnostico) 
	   		    FROM   diagnostico_doenca
			    WHERE  nr_atendimento = a.nr_atendimento 
			    AND    ie_tipo_diagnostico = 1);	
	    			    

SELECT MAX(cd_medico_referido),
	   max(cd_medico_atendimento)
	--max(cd_usuario_convenio)
INTO	cd_medico_referido_w,
		cd_medico_atendimento_w
	--cd_usuario_convenio_w
FROM   atendimento_paciente
WHERE  nr_atendimento = nr_atendimento_w;


SELECT	MAX(cd_usuario_convenio)
INTO	cd_usuario_convenio_w
FROM	atendimento_paciente_v
WHERE	nr_atendimento = nr_atendimento_w;


SELECT	MAX(dt_entrada_unidade)
INTO	dt_entrada_unidade_w
FROM	atend_paciente_unidade
WHERE	nr_atendimento = nr_atendimento_w;

SELECT 	MAX(dt_inicio_real),
	MAX(dt_termino)
INTO	dt_inicio_cir_w,
	dt_fim_cir_w
FROM	cirurgia
WHERE	nr_atendimento = nr_atendimento_w;

BEGIN
SELECT 	SUBSTR(obter_desc_convenio(MAX(CD_CONVENIO)),1,255),
	SUBSTR(obter_categoria_convenio(MAX(CD_CONVENIO),MAX(CD_CATEGORIA)),1,255),
	SUBSTR(MAX(CD_USUARIO_CONVENIO),1,255),
	substr(obter_nome_pf(max(cd_medico_solicitante)),1,60)
INTO	ds_conv_ageint_w,
	ds_categ_ageint_w,
	cd_cart_ageint_w,
	nm_med_sol_ageint_w
FROM	agenda_integrada
WHERE	nr_sequencia = (	SELECT	MAX(nr_sequencia)
				FROM	agenda_integrada
				WHERE	cd_pessoa_fisica = cd_pessoa_fisica_w);

EXCEPTION
WHEN OTHERS THEN
	BEGIN
	ds_conv_ageint_w	:= '';
	cd_cart_ageint_w	:= '';
	ds_categ_ageint_w	:= '';
	END;
END;
		

select	  max(nr_seq_atend_checklist)
into	  nr_seq_atend_checklist_w
from	  med_avaliacao_paciente
where	  nr_sequencia = nr_seq_avaliacao_p;

select	substr(max(a.cd_medico),1,10),
	substr(max(a.cd_cido),1,10),      
	substr(max(a.cd_doenca),1,10),
	substr(max(a.cd_fisico),1,10)
into	cd_medico_radioterapia_w,
	cd_cido_radioterapia_w,
	cd_doenca_radioterapia_w,
	cd_fisico_radioterapia_w
from	rxt_tumor a
where   a.nr_seq_checklist = nr_seq_atend_checklist_w;

IF	(ds_macro_w = '@NOME') THEN
	ds_resultado_w	:= nm_pessoa_fisica_w;
ELSIF	(ds_macro_w = '@IDADE') THEN
	ds_resultado_w	:= ds_idade_w;
ELSIF	(ds_macro_w = '@NASCIMENTO') THEN
	ds_resultado_w	:= TO_CHAR(dt_nascimento_w, 'dd/mm/yyyy');
ELSIF	(ds_macro_w = '@ALTURA') THEN
	IF (NVL(qt_altura_cm_w,0) > 0 ) THEN
	ds_resultado_w	:= TO_CHAR(qt_altura_cm_w);
	ELSE
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'Altura');
	END IF;
ELSIF	(ds_macro_w = '@SEXO_NUM') THEN
	IF	(ie_sexo_w	= 'F') THEN
		ds_resultado_w	:= 1;
	ELSE
		ds_resultado_w	:= 2;
	END IF;

ELSIF	(ds_macro_w = '@SEXO_ABREV') THEN
	ds_resultado_w	:= ie_sexo_w;
ELSIF	(ds_macro_w = '@SEXO') THEN
	ds_resultado_w	:= ds_sexo_w;
ELSIF	(ds_macro_w = '@DATA') THEN
	ds_resultado_w	:= TO_CHAR(SYSDATE, 'dd/mm/yyyy');
ELSIF	(ds_macro_w = '@DATA_HORA') THEN
	ds_resultado_w	:= TO_CHAR(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
ELSIF	(ds_macro_w = '@NR_ATENDIMENTO') THEN
	ds_resultado_w	:= nr_atendimento_w;
ELSIF	(ds_macro_w = '@CID') THEN
	ds_resultado_w	:= cd_doenca_w;
ELSIF	(upper(ds_macro_p) = '@CD_ULTIMO_CID_DEF') then
	ds_resultado_w := cd_ultima_doenca_def_w;
ELSIF	(upper(ds_macro_p) = '@CD_ULTIMO_CID_PRE') then
	ds_resultado_w := cd_ultima_doenca_pre_w;	
ELSIF	(ds_macro_w = '@DATA_ENTRADA') THEN
	ds_resultado_w	:= TO_CHAR(dt_entrada_w, 'dd/mm/yyyy hh24:mi:ss');
ELSIF	(ds_macro_w = '@USUARIO') THEN
	ds_resultado_w	:= nm_usuario_p;
ELSIF	(ds_macro_w = '@NM_PF_USUARIO') THEN
	ds_resultado_w	:= substr(Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'N'),1,255);
ELSIF	(ds_macro_w = '@SETOR') THEN
	ds_resultado_w	:= SUBSTR(obter_nome_setor(obter_setor_usuario(nm_usuario_p)),1,60);
ELSIF	(ds_macro_w = '@CONVENIO') THEN
	ds_resultado_w	:= ds_convenio_w;
ELSIF	(ds_macro_w = '@MEDICO') THEN
	ds_resultado_w	:= nm_medico_w;
ELSIF	(ds_macro_w = '@DIAGNOSTICO') THEN
	ds_resultado_w	:= ds_diagnostico_w;
ELSIF	(ds_macro_w = '@DESC_CID') THEN
	ds_resultado_w	:= ds_desc_cid_w;
ELSIF	(ds_macro_w = '@SETOR_PAC') THEN
	ds_resultado_w	:= ds_setor_pac_w;
ELSIF	(ds_macro_w = '@PRONTUARIO') THEN
	ds_resultado_w	:= nr_prontuario_w;
ELSIF	(ds_macro_w = '@TIPO_ATENDIMENTO') THEN
	ds_resultado_w	:= ds_tipo_atend_w;
ELSIF  	(ds_macro_w	= '@PESO') THEN
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'Peso');
ELSIF   (ds_macro_w = '@MEDICO_REFERIDO') THEN
	ds_resultado_w := obter_nome_pf(cd_medico_referido_w);
ELSIF	(ds_macro_w = '@CODIGO_CARTEIRINHA') THEN
	ds_resultado_w	:= cd_usuario_convenio_w;
ELSIF	(ds_macro_w = '@TELEFONE') THEN
	ds_resultado_w	:= obter_telefone_pf(cd_pessoa_fisica_w,0);
ELSIF   (ds_macro_w = '@FONE_MEDIC_REFERIDO') 	THEN
	ds_resultado_w := obter_telefone_pf(cd_medico_referido_w,0);
ELSIF   (ds_macro_w = '@DATA_ENTRADA_UNIDAD') 	THEN
	ds_resultado_w := TO_CHAR(dt_entrada_unidade_w, 'dd/mm/yyyy hh24:mi:ss');
ELSIF   (ds_macro_w = '@DATA_ALTA') 	THEN
	ds_resultado_w := dt_alta_w;
ELSIF   (ds_macro_w = '@DATA_INICIO_CIR') 	THEN
	ds_resultado_w := dt_inicio_cir_w;
ELSIF   (ds_macro_w = '@DATA_FIM_CIR') 	THEN
	ds_resultado_w := dt_fim_cir_w;
ELSIF	(ds_macro_w = '@LEITO_PACIENTE') THEN
	ds_resultado_w := obter_Unidade_Atendimento(nr_atendimento_w,'A','U');
ELSIF	(ds_macro_w = '@MUNICIPIO') THEN
	ds_resultado_w := obter_compl_pf(cd_pessoa_fisica_w,1,'CI');
ELSIF	(ds_macro_w	= '@MAE') THEN
	ds_resultado_w	:= NVL(TO_CHAR(obter_compl_pf(cd_pessoa_fisica_w,5,'N')),OBTER_DESC_EXPRESSAO(327119));
ELSIF	(ds_macro_w	= '@PAI') THEN
	ds_resultado_w	:= NVL(TO_CHAR(obter_compl_pf(cd_pessoa_fisica_w,4,'N')),OBTER_DESC_EXPRESSAO(327119));
ELSIF	(ds_macro_w	= '@SETOR_ATENDIMENTO') THEN
	ds_resultado_w	:= NVL(SUBSTR(obter_nome_Setor(obter_setor_Atendimento(nr_atendimento_w)),1,255),OBTER_DESC_EXPRESSAO(327119));

ELSIF	(ds_macro_w	= '@ALERGIAS') THEN
		OPEN C01;
		LOOP
			FETCH C01 INTO
				ds_agente_alerg_w,
				ds_tipo_reacao_w,
				ds_observacao_w,
				ie_confirmacao_w;
			EXIT WHEN C01%NOTFOUND;

			IF	(ie_confirmacao_w = 'S') THEN
				ds_resultado_w 	:= ds_resultado_w || OBTER_DESC_EXPRESSAO(298982)||':  ';
			END IF;

			ds_resultado_w	:= ds_resultado_w || ds_agente_alerg_w;

			IF	(ds_tipo_reacao_w IS NOT NULL) THEN
				ds_resultado_w	:= ds_resultado_w || ' (' || ds_tipo_reacao_w || ')';
			END IF;

			IF	(ds_observacao_w IS NOT NULL) THEN
				ds_resultado_w	:= ds_resultado_w || ' - ' || ds_observacao_w;
			END IF;

			ds_resultado_w	:= ds_resultado_w || CHR(13) || CHR(10);
		END LOOP;
		CLOSE C01;

ELSIF	(ds_macro_w	= '@DOENCAS_PREVIAS') THEN

		OPEN C02;
		LOOP
		FETCH C02 INTO
			ds_doenca_prev_w;
		EXIT WHEN C02%NOTFOUND;
			BEGIN
			ds_resultado_w	:= SUBSTR(ds_resultado_w || ds_doenca_prev_w || CHR(13) || CHR(10),1,2000);
			END;
		END LOOP;
		CLOSE C02;

ELSIF	(ds_macro_w = '@MEDIC_USO_HS') THEN
		OPEN c03;
		LOOP
			FETCH c03 INTO
				ds_medic_uso_w;
			EXIT WHEN c03%NOTFOUND;
			IF	(ds_resultado_w IS NULL) THEN
				ds_resultado_w	:= ds_medic_uso_w;
			ELSE
				ds_resultado_w	:= ds_resultado_w || ', ' || ds_medic_uso_w;
			END IF;
		END LOOP;
		CLOSE c03;

ELSIF	(ds_macro_w	= '@TODOS_DIAGNOSTICOS') THEN
		BEGIN
		OPEN c04;
		LOOP
			FETCH c04 INTO
				ds_doenca_w;
			EXIT WHEN c04%NOTFOUND;
			ds_resultado_w	:= SUBSTR(ds_resultado_w,1,1000) || SUBSTR(ds_doenca_w,1,1000) || CHR(13) || CHR(10);
		END LOOP;
		CLOSE c04;
		END;
ELSIF	(ds_macro_w	= '@MESES_APLICACAO_ESP') THEN
		SELECT	MAX(DT_APLICACAO)
		INTO	dt_aplicacao_w
		FROM	ATENDIMENTO_TOXINA a,
				atendimento_paciente b
		WHERE	a.nr_atendimento	= b.nr_atendimento
		AND		b.cd_pessoa_fisica = cd_pessoa_fisica_w;
		ds_resultado_w	:= '0';
		IF	(dt_aplicacao_w	IS NOT NULL) THEN
			ds_resultado_w	:=  TRUNC(MONTHS_BETWEEN(SYSDATE,dt_aplicacao_w),2);
		END IF;
ELSIF	(ds_macro_w = '@ENDERECO') THEN
	ds_resultado_w	:= ds_endereco_w;
ELSIF	(ds_macro_w = '@DS_ENDERECO') THEN
	ds_resultado_w	:= ds_endereco_w;	
ELSIF	(ds_macro_w = '@DS_MUNICIPIO') THEN
	ds_resultado_w	:= ds_municipio_w;
ELSIF	(ds_macro_w = '@DS_MUNICIPIO') THEN
	ds_resultado_w	:= ds_municipio_w;	
ELSIF	(ds_macro_w = '@DS_TEL_REND') THEN
	ds_resultado_w	:= ds_tel_rend_w;
ELSIF	(ds_macro_w = '@DS_BAIRRO') THEN
	ds_resultado_w	:= ds_bairro_w;
ELSIF	(ds_macro_w = '@NR_ENDERECO_RES') THEN
	ds_resultado_w	:= nr_endereco_w;	
ELSIF	(ds_macro_w = '@DS_COMPLEMENTO_RES') THEN
	ds_resultado_w	:= ds_complemento_w;		
ELSIF	(ds_macro_w = '@DS_ESTADO') THEN
	ds_resultado_w	:= ds_estado_w;
ELSIF	(ds_macro_w = '@DS_TEL_CEL') THEN
	ds_resultado_w	:= ds_tel_com_cel_w;
ELSIF	(ds_macro_w = '@DS_CEP') THEN
	ds_resultado_w	:= ds_cep_w;
ELSIF	(ds_macro_w = '@CONV_AGEINT') THEN
	ds_resultado_w	:= ds_conv_ageint_w;
ELSIF	(ds_macro_w = '@CATEG_AGEINT') THEN
	ds_resultado_w	:= ds_categ_ageint_w;
ELSIF	(ds_macro_w = '@CART_AGEINT') THEN
	ds_resultado_w	:= cd_cart_ageint_w;
elsif	(ds_macro_w = '@MED_SOL_AGEINT') then
	ds_resultado_w	:= nm_med_sol_ageint_w;
elsif	(ds_macro_w = '@QT_PERI_ABDOMINAL') then
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'QT_ABD');
elsif	(ds_macro_w = '@QT_PERI_QUADRIL') then
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'QT_QDR');
elsif	(ds_macro_w	= '@TEMPERATURA') then
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'Temp');
elsif	(ds_macro_w	= '@PA_MIN') then
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'PAMIN');
elsif	(ds_macro_w	= '@PA_MAX') then
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'PAMAX');
elsif	(ds_macro_w	= '@PAM') then
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'PAM');
elsif	(ds_macro_w	= '@IMC') then
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'IMC');
elsif	(ds_macro_w = '@TOPOGRAFIA_RADIOTER') then
	begin
	open C05;
	loop
	fetch C05 into	
		ds_topografia_radioterapia_w;
	exit when C05%notfound;
		begin
		if	(ds_resultado_w is null) then
			ds_resultado_w	:= substr(ds_topografia_radioterapia_w,1,255);
		else
			ds_resultado_w	:= substr(ds_resultado_w || ', ' || ds_topografia_radioterapia_w,1,255);
		end if;
		end;
	end loop;
	close C05;
	end;
elsif	(ds_macro_w = '@CID0_RADIOTERAPIA') then
	ds_resultado_w := substr(obter_desc_cido_topografia(cd_cido_radioterapia_w),1,100); 
elsif	(ds_macro_w = '@CID10_RADIOTERAPIA') then
	ds_resultado_w := substr(obter_desc_cid_doenca(cd_doenca_radioterapia_w),1,100);
elsif	(ds_macro_w = '@MEDICO_RADIOTERAPIA') then	
	ds_resultado_w := obter_nome_pf(cd_medico_radioterapia_w);
elsif	(ds_macro_w = '@FISICO_RADIOTERAPIA') then
	ds_resultado_w := obter_nome_pf(cd_fisico_radioterapia_w);
elsif	(ds_macro_w = '@VALOR_ESCALA_DOR') then
	ds_resultado_w := obter_sinal_vital(nr_atendimento_w,'ESCDOR');
elsif	(ds_macro_w	= '@ESCALA_BARTHEL') then
		begin
		select  max(substr(pkg_date_formaters.to_varchar(dt_avaliacao,'shortDate',1,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario),1,255)||' - '||a.qt_pontuacao||' - '||substr(obter_desc_escala_barthel(a.qt_pontuacao),1,255)) ds_resultado_w
		into	ds_resultado_w
		from    escala_barthel a
		where   a.nr_sequencia = (select max(b.nr_sequencia)
					  from   escala_barthel b
					  where  nr_atendimento = nr_atendimento_w
					  and	 b.dt_liberacao is not null
					  and	 b.dt_inativacao is null);
		end;
elsif (ds_macro_w	= '@PROCEDIMENTO_CIRURGIA') then
	for ds_procedimento_cirurgia_w in ds_procedimento_cirurgia loop
		if	(ds_resultado_w is null) then
			ds_resultado_w	:= ds_procedimento_cirurgia_w.ds_procedimento_w;
		else
			ds_resultado_w	:= ds_resultado_w || chr(13) || chr(10) || ds_procedimento_cirurgia_w.ds_procedimento_w;
		end if;	
	end loop;
elsif	(ds_macro_w = '@COD_PROCEDIMENTO_CIRURGIA') then
	--cod_procedimento
	for cod_procedimento_w in cod_procedimento loop
		if	(ds_resultado_w is null) then
			ds_resultado_w	:= cod_procedimento_w.cd_procedimento_w;
		else
			ds_resultado_w	:= ds_resultado_w || chr(13) || chr(10) || cod_procedimento_w.cd_procedimento_w;
		end if;	
	end loop;
elsif	(ds_macro_w	= '@CRM') then
	ds_resultado_w	:= wheb_mensagem_pck.get_texto(307217) || ' ' || obter_nome_medico(nvl(cd_medico_referido_w,cd_medico_atendimento_w),'CRM'); -- CRM
elsif	(ds_macro_w	= '@DIAGNOSTICO_CID') then
begin
	if (obtain_user_locale(wheb_usuario_pck.get_nm_usuario) = 'de_DE' and obter_uso_case(wheb_usuario_pck.get_nm_usuario) = 'S') then
		select	max(dm.dt_diagnostico)
		into	dt_diagnostico_w
		from	diagnostico_medico dm
		where	dm.nr_atendimento	in (select a.nr_atendimento 
									from atendimento_paciente a, 
										 episodio_paciente b 
									where a.nr_seq_episodio = b.nr_sequencia 
									and b.nr_sequencia = obter_episodio_atendimento(nr_atendimento_w))
		and		exists (select	1
						from	diagnostico_doenca dd
						where	dd.dt_diagnostico = dm.dt_diagnostico
						and		ie_tipo_diagnostico = 2
						and		nvl(ie_situacao,'A') = 'A'
						and		dt_liberacao is not null)
		and		dm.ie_tipo_diagnostico = 2;
		
		select	max(dd.cd_doenca)
		into	ds_resultado_w
		from	diagnostico_doenca dd
		where	dd.nr_atendimento	in (select a.nr_atendimento 
									from atendimento_paciente a, 
										 episodio_paciente b 
									where a.nr_seq_episodio = b.nr_sequencia 
									and b.nr_sequencia = obter_episodio_atendimento(nr_atendimento_w))
		and		dd.dt_diagnostico	= dt_diagnostico_w
		and		dd.ie_classificacao_doenca = 'P';
		
	else
		select	obter_cid_atendimento(nr_atendimento_w,'P')
		into	ds_resultado_w
		from	dual;
	end if;
end;
elsif	(ds_macro_w = '@NM_CIRURGIAO_ULT_CIRUR_ATEND') then
	ds_resultado_w	:=	Obter_dados_ult_cirurgia_atend(nr_atendimento_w, 'NM');
elsif	(ds_macro_w = '@NM_ANESTESIS_ULT_CIRUR_ATEND') then
	ds_resultado_w	:=	Obter_dados_ult_cirurgia_atend(nr_atendimento_w, 'NA');
elsif	(ds_macro_w	= '@NM_MEDICO_EXTERNO') then
	select	max(nm_medico)
	into	ds_resultado_w
	from	(	select	substr(obter_nome_pessoa_fisica(cd_medico,null),1,200) nm_medico
				from	pf_medico_externo a
				where 	cd_pessoa_fisica = cd_pessoa_fisica_w
				and		nr_seq_tipo_medico = 1
				order	by a.nr_sequencia desc)
	where	rownum < 2;
elsif (ds_macro_w	= '@NR_CASE') then
	select 	max(nr_seq_episodio)
	into   	ds_resultado_w
	from	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_w;
elsif	(ds_macro_w	= '@SAUDACAO_CARTA') then
	if (obter_sexo_pf(cd_pessoa_fisica_w, 'C') = 'M') then
		ds_resultado_w := obter_desc_expressao(890062);
	else
		ds_resultado_w := obter_desc_expressao(890060);
	end if;
elsif (ds_macro_w	= '@TITULO_ACADEMICO') then
	select 	max(b.ds_forma_tratamento)
	into	ds_resultado_w
	from	pessoa_fisica a, PF_FORMA_TRATAMENTO b
	where 	cd_pessoa_fisica = cd_pessoa_fisica_w
	and 	a.nr_seq_forma_trat = b.nr_sequencia;
elsif (ds_macro_w = '@ESCALA_APACHE_II') then
	begin
	select  max(QT_APACHE_II)
	into	ds_resultado_w
	from    APACHE a
	where   a.nr_sequencia = (select max(b.nr_sequencia)
				  from   APACHE b
				  where  b.nr_atendimento = nr_atendimento_w
				  and	b.dt_liberacao is not null
				  and	b.dt_inativacao is null);
	end;
elsif 	(ds_macro_w = '@BRADEN') then
	begin
	select  max(qt_ponto||' - '||substr(obter_resultado_braden(qt_ponto),1,255))
	into	ds_resultado_w
	from    atend_escala_braden a
	where   a.nr_sequencia = (select max(b.nr_sequencia)
				  from   atend_escala_braden b
				  where  b.nr_atendimento = nr_atendimento_w
				  and	b.dt_liberacao is not null
				  and	b.dt_inativacao is null);
	end;
elsif 	(ds_macro_w = '@ESCALA_CAPRINI') then
	begin
	--Tratamento especial alemanha. Not deve ser usado no Java / Delphi e com uma resolution do OS 1824270
	select  max(substr(pkg_date_formaters.to_varchar(a.dt_avaliacao,'shortDate',1,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario),1,255)||' - '||a.qt_pontuacao||' - '||substr(obter_valor_dominio(8636,a.ie_risco),1,255)) || '\n'||
			max(ds_resultado)
	into	ds_resultado_w
	from    escala_caprini a
	where   a.nr_sequencia = (select max(b.nr_sequencia)
							  from   escala_caprini b
							  where  b.nr_atendimento = nr_atendimento_w
							  and	b.dt_liberacao is not null
							  and	b.dt_inativacao is null);
	end;
elsif 	(ds_macro_w = '@ESCALA_BARTHEL_ESTENDIDA') then 
	begin
	select  max(substr(pkg_date_formaters.to_varchar(a.dt_avaliacao,'shortDate',1,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario),1,255)||' - '||a.qt_pontuacao||' - '||substr(obter_desc_extended_barthel(a.qt_pontuacao),1,100))
	into	ds_resultado_w
	from    escala_barthel_modificada a
	where   a.nr_sequencia = (select max(b.nr_sequencia)
				  from   escala_barthel_modificada b
				  where  b.nr_atendimento = nr_atendimento_w
				  and	b.dt_liberacao is not null
				  and	b.dt_inativacao is null);
	end;
elsif 	(ds_macro_w = '@ESCALA_EARQ') then
	begin
	select  max(substr(pkg_date_formaters.to_varchar(a.dt_avaliacao,'shortDate',1,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario),1,255)||' - '||a.qt_pontuacao||' - '||substr(obter_descr_escala_earq(a.qt_pontuacao),1,255))
	into	ds_resultado_w
	from    escala_earq a
	where   a.nr_sequencia = (select max(b.nr_sequencia)
				  from   escala_earq b
				  where  b.nr_atendimento = nr_atendimento_w
				  and	b.dt_liberacao is not null
				  and	b.dt_inativacao is null);
	end;
elsif 	(ds_macro_w = '@GLASGOW') then
	begin
	select  max(qt_glasgow||' - '||substr(obter_resultado_glasgow(qt_glasgow),1,100))
	into	ds_resultado_w
	from    atend_escala_indice a
	where   a.nr_sequencia = (select max(b.nr_sequencia)
				  from   atend_escala_indice b
				  where  b.nr_atendimento = nr_atendimento_w
				  and	b.dt_liberacao is not null
				  and	b.dt_inativacao is null);
	end;
elsif (ds_macro_w = '@ESCALA_KARNOFSKY') then
	begin
	select  max(a.ie_KARNOFSKY||' - '|| substr(obter_valor_dominio(1531,a.ie_KARNOFSKY),1,255))
	into	ds_resultado_w
	from     ESCALA_KARNOFSKY a
	where   a.nr_sequencia = (select max(b.nr_sequencia)
				  from   ESCALA_KARNOFSKY b
				  where  b.nr_atendimento = nr_atendimento_w
				  and	b.dt_liberacao is not null
				  and	b.dt_inativacao is null);
	end;
elsif 	(ds_macro_w = '@ESCALA_NRS_2002') then
	begin
	select  max(substr(pkg_date_formaters.to_varchar(a.dt_avaliacao,'shortDate',1,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario),1,255)||' - '||a.qt_pontuacao||' - '||substr(Obter_Resul_nrs(a.qt_pontuacao),1,255))
	into	ds_resultado_w
	from    escala_nrs a
	where   a.nr_sequencia = (select max(b.nr_sequencia)
				  from   escala_nrs b
				  where  b.nr_atendimento = nr_atendimento_w
				  and	b.dt_liberacao is not null
				  and	b.dt_inativacao is null);
	end;
elsif	(ds_macro_w = '@LAST_PULS_SCORE') then
	begin
	select  max(NVL(TO_CHAR(a.dt_avaliacao,'dd/mm/yyyy'), ' ') ||' - '|| a.IE_PULS || ' - ' || obter_valor_dominio(8637,a.IE_PULS))
		into	ds_resultado_w
		from    ESCALA_PULS a
		where   a.nr_sequencia = (select max(b.nr_sequencia)
					  from   ESCALA_PULS b
					  where  b.nr_atendimento = nr_atendimento_w
					  and	b.dt_liberacao is not null
					  and	b.dt_inativacao is null);
	end;
elsif (ds_macro_w = '@ESC_SAPS') then
	begin
	SELECT max(wheb_mensagem_pck.get_texto(307232) || ' ' || a.QT_PONTUACAO || ', ' || wheb_mensagem_pck.get_texto(307223) || ' ' || a.PR_RISCO || '%' ||  DECODE(a.PR_RISCO_AMERICA_SUL,NULL,'', ', '||wheb_mensagem_pck.get_texto(307234)||' ' || a.PR_RISCO_AMERICA_SUL || '%')) -- Pontuario ..., Risco ...% Risco America do Sul
	into	ds_resultado_w
	FROM    ESCALA_SAPS3 a
	WHERE   a.nr_sequencia = (SELECT MAX(b.nr_sequencia)
				  FROM   ESCALA_SAPS3 b
				  WHERE  b.nr_atendimento = nr_atendimento_w
				  and	b.dt_liberacao is not null
				  and	b.dt_inativacao is null);
	END;
elsif	(ds_macro_w	= '@PRONOME') then

		ds_resultado_w	:= obter_pronome_pf(cd_pessoa_fisica_w, 'N');

elsif	(ds_macro_w	= '@DATAEXT') then

		ds_resultado_w	:= to_char(sysdate, 'dd/mm/yyyy');

elsif	(ds_macro_w = '@ARTIG_PRONOME') then

		ds_resultado_w	:= obter_pronome_pf(cd_pessoa_fisica_w, 'S');

elsif	(ds_macro_w	= '@PRIMEIRO_NOME') then

		ds_resultado_w	:= obter_nome_sobrenome_pessoa(cd_pessoa_fisica_w, 'P');

elsif	(ds_macro_w	= '@SOBRENOME') then

		ds_resultado_w	:= obter_nome_sobrenome_pessoa(cd_pessoa_fisica_w, 'S');

elsif	(ds_macro_w	= '@ARTIGO') then

		ds_resultado_w	:= obter_artigo_pf(cd_pessoa_fisica_w);
END IF;

RETURN	ds_resultado_w;

END;
/
