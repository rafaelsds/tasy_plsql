create or replace
function hcc_obter_tipo_documento(	nr_sequencia_p		number)
 		    	return varchar2 is

ie_tipo_documento_w		varchar2(1) := 'A';
ie_tipo_recolhimento_w		varchar2(1);
cd_cgc_w			varchar2(14);
ie_tipo_tributacao_w		pessoa_juridica.ie_tipo_tributacao%type;
cd_tributo_w			number(3);
qt_existe_w			number(10);

cursor c01 is
select	a.cd_tributo
from	nota_fiscal_trib a,
	tributo b
where	a.cd_tributo = b.cd_tributo
and	nr_sequencia = nr_sequencia_p
and	b.ie_tipo_tributo = 'ISS';

begin

ie_tipo_documento_w := 'A';

select	hcc_obter_tipo_recolhimento(nr_sequencia),
	cd_cgc
into	ie_tipo_recolhimento_w,
	cd_cgc_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

/*Se o tipo de recolhimento for Retido na fonte, automaticamente o tipo de documento ser� A - Servi�os*/
if	(ie_tipo_recolhimento_w = 'R') then
	ie_tipo_documento_w := 'A'; /*A - Servi�os*/
else
	begin
	select	max(ie_tipo_tributacao)
	into	ie_tipo_tributacao_w
	from	pessoa_juridica
	where	cd_cgc = cd_cgc_w;	
	
	if	(ie_tipo_tributacao_w = 0) then
	
		open C01;
		loop
		fetch C01 into	
			cd_tributo_w;
		exit when C01%notfound;
			begin
			
			select	count(*)
			into	qt_existe_w
			from	tributo_conta_pagar
			where	cd_tributo = cd_tributo_w
			and	cd_pessoa_juridica = cd_cgc_w;
			
			if	(qt_existe_w = 0) then
				ie_tipo_documento_w := 'C';
			else
				ie_tipo_documento_w := 'S';
			end if;		
			
			end;
		end loop;
		close C01;	
	end if;
	end;
end if;

return	ie_tipo_documento_w;

end hcc_obter_tipo_documento;
/
