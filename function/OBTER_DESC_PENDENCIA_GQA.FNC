create or replace 
function obter_desc_pendencia_gqa (ie_tipo_pendencia_p number) return varchar2 is

ds_pendencia_w varchar2(200);

begin

  if (ie_tipo_pendencia_p = 1) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(367599); -- Diagnóstico
  elsif (ie_tipo_pendencia_p = 2) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(367604); -- Exames de laboratório
  elsif (ie_tipo_pendencia_p = 3) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(367606); -- Sinais vitais 
  elsif (ie_tipo_pendencia_p = 4) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(367607); -- Escalas e índices
  elsif (ie_tipo_pendencia_p = 5) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(367608); -- Curativos
  elsif (ie_tipo_pendencia_p = 6) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(367609); -- Protocolos Assistenciais
  elsif (ie_tipo_pendencia_p = 7) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(367610); -- Eventos
  elsif (ie_tipo_pendencia_p = 8) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(367611); -- Classificação de risco
  elsif (ie_tipo_pendencia_p = 9) then
     ds_pendencia_w := wheb_mensagem_pck.get_texto(1060535); -- CIAP
  end if;
  
return ds_pendencia_w;

end obter_desc_pendencia_gqa;
/