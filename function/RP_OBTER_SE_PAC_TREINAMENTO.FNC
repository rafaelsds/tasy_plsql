create or replace
function rp_obter_se_pac_treinamento(nr_seq_pac_reab_p		number)
 		    	return varchar2 is
			
ie_retorno_w	varchar2(1);

begin

if	(nr_seq_pac_reab_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ie_retorno_w
	from	rp_tipo_pac_reab a,
		rp_tipo_paciente b
	where	a.nr_tipo_pac_reab = b.nr_sequencia
	and	nvl(ie_tipo_treinamento,'N') = 'S'
	and	sysdate between a.dt_inicio_tipo and nvl(dt_fim_tipo,sysdate)
	and	a.nr_seq_pac_reab = nr_seq_pac_reab_p;
end if;

return	ie_retorno_w;

end rp_obter_se_pac_treinamento;
/