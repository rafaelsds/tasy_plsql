create or replace
function HN_Obter_turno_data(dt_referencia_p		Date)
			     return varchar2 is
			     
ds_turno_w		varchar2(15);			     

begin

if (( to_char(dt_referencia_p,'hh24:mi') >= '07:01') and (to_char(dt_referencia_p,'hh24:mi') <= '13:00')) then
   ds_turno_w := 'M';
elsif (( to_char(dt_referencia_p,'hh24:mi') >= '13:01') and (to_char(dt_referencia_p,'hh24:mi') <= '19:00')) then
   ds_turno_w := 'T';
elsif ((( to_char(dt_referencia_p,'hh24:mi') >= '19:01') and (to_char(dt_referencia_p,'hh24:mi') <= '23:59')) or
	(( to_char(dt_referencia_p,'hh24:mi') >= '00:01') and (to_char(dt_referencia_p,'hh24:mi') <= '07:00')))then
   ds_turno_w := 'N';
end if;

return	ds_turno_w;

end HN_Obter_turno_data;
/