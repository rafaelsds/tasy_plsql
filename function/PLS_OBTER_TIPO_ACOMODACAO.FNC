create or replace
function PLS_OBTER_TIPO_ACOMODACAO (nr_seq_tipo_acomod_p	number) return varchar2 is

ds_retorno_w	varchar2(30);

begin

select	max(cd_tiss)
into	ds_retorno_w
from	pls_tipo_acomodacao
where	nr_sequencia	= nr_seq_tipo_acomod_p;

return ds_retorno_w;

end PLS_OBTER_TIPO_ACOMODACAO;
/
