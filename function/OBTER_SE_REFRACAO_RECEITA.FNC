create or replace
function obter_se_refracao_receita		( nr_seq_consulta_p	number)
						  return varchar2 is
						
qt_oft_refracao_w	number(10,0);
ds_retorno_w		varchar2(1) := 'N';						

begin

if ( nr_seq_consulta_p is not null) then
	SELECT	COUNT(*)
	INTO	qt_oft_refracao_w
	FROM	oft_refracao
	WHERE	nr_seq_consulta = nr_seq_consulta_p
	AND     ((ie_receita_dinamica = 'S') OR (ie_receita_estatica = 'S'))
   AND     NVL(ie_situacao,'A') = 'A';
end if;

if	(qt_oft_refracao_w > 0) then
	 ds_retorno_w := 'S';
else
	 ds_retorno_w := 'N';
end if;


return	ds_retorno_w;

end obter_se_refracao_receita;
/
