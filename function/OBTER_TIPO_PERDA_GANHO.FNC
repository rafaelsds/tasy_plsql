Create or Replace
function obter_tipo_perda_ganho(nr_seq_tipo_p	number,
				ie_opcao_p	varchar2)
				return Varchar2 is


ds_retorno_w		varchar2(60);
ie_perda_ganho_w	varchar2(20);
ds_grupo_w		varchar2(40);
ds_cor_w		varchar2(60);

begin

select	
  decode(b.ie_perda_ganho,
    'P',obter_desc_expressao(295434,'Perda'),
    'G',obter_desc_expressao(653120,'Ganho'),
    'J',obter_desc_expressao(1032688,'Ganho Insensivel'),
    'I',obter_desc_expressao(1032686,'Perda Insensivel')
  ),
	b.ie_perda_ganho,
	substr(b.ds_grupo,1,40),
	a.DS_COR
into	ds_retorno_w,
	ie_perda_ganho_w,
	ds_grupo_w,
	ds_cor_w
from	grupo_perda_ganho b,
	tipo_perda_ganho a
where	a.nr_sequencia	= nr_seq_tipo_p
and	a.nr_seq_grupo	= b.nr_sequencia;

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= ie_perda_ganho_w;
elsif	(ie_opcao_p = 'G') then
	ds_retorno_w	:= ds_grupo_w;
elsif	(ie_opcao_p = 'COR') then
	ds_retorno_w	:= ds_cor_w;
end if;

return ds_retorno_w;

end obter_tipo_perda_ganho;
/
