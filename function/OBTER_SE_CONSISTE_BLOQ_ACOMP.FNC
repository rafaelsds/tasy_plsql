create or replace
function obter_se_consiste_bloq_acomp ( nr_atendimento_p	number,
					cd_pessoa_fisica_p	varchar2,
					nm_pessoa_bloqueio_p	varchar2,
					nr_identidade_p		varchar2)
 		    	return varchar2 is

qt_regra_w	number(10) 	:= 0;
ds_retorno_w	varchar2(1)	:= 'N';
			
begin
if	(cd_pessoa_fisica_p is not null) then
	select 	count(*) 
	into	qt_regra_w
	from 	atendimento_acomp_bloq
	where 	nr_atendimento 		= nr_atendimento_p
	and 	cd_pessoa_fisica 	= cd_pessoa_fisica_p;
end if;

if	(qt_regra_w = 0) then
	select	count(*)
	into	qt_regra_w
	from 	atendimento_acomp_bloq
	where 	nr_atendimento	 	= 	nr_atendimento_p
	and	nm_pessoa_fisica	= 	nm_pessoa_bloqueio_p
	and	((nr_identidade		= 	nr_identidade_p and nr_identidade_p is not null) or 
		(nr_identidade		is null and nr_identidade_p is null));
end if;

if	(qt_regra_w	> 0) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end obter_se_consiste_bloq_acomp;

/