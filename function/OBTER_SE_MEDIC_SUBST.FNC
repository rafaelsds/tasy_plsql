create or replace
function obter_se_medic_subst(nr_sequencia_p  number, 
							  nr_prescricao_p number) 
							  return Varchar2 is

ie_medic_subs_w varchar(1);

pragma autonomous_transaction;
begin

	select	nvl(max('S'),'N')
	into	ie_medic_subs_w
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and		nr_seq_substituto = nr_sequencia_p; 

	return ie_medic_subs_w;

end obter_se_medic_subst;
/