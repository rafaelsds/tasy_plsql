create or replace
function obter_descricao_justificativa(	nr_cirurgia_p	number,
					cd_material_p	number ) 
					return varchar2 is

ds_justificativa_w	varchar2(2000);

begin

select	max(ds_justificativa)
into	ds_justificativa_w
from	cirurgia_agente_disp
where	nr_cirurgia	=	nr_cirurgia_p
and	cd_material	=	cd_material_p;	

if	(ds_justificativa_w is null) then
	select	max(ds_justificativa)
	into	ds_justificativa_w
	from	cirurgia_agente_disp
	where	nr_cirurgia	=	nr_cirurgia_p
	and	cd_material in	(	select	cd_material
					from	material
					where	cd_material_generico = cd_material_p);
end if;

return	ds_justificativa_w;

end obter_descricao_justificativa;
/