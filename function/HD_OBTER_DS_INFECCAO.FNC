create or replace
function hd_obter_ds_infeccao(nr_seq_tipo_infeccao_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);			

begin
if (nr_seq_tipo_infeccao_p is not null) then

	select	max(ds_infeccao)
	into	ds_retorno_w
	from	hd_tipo_infeccao
	where	nr_sequencia = nr_seq_tipo_infeccao_p;
	
end if;

return	ds_retorno_w;

end hd_obter_ds_infeccao;
/