Create or replace
Function Man_Obter_Min_Sla_OS_estagio(	nr_seq_ordem_p	Number,
					ie_tempo_p	Varchar2,
					nr_seq_estagio_p	number)
				return number is

qt_min_real_os_w		Number(15);
dt_ordem_w		Date;
dt_inicio_w		Date;
dt_fim_w			Date;
cd_estabelecimento_w	Number(15,0);
dt_estagio_w		date;
nr_seq_estagio_w		number(10,0) := nr_seq_estagio_p;
nr_seq_estagio_ww		number(10,0);

BEGIN

begin
qt_min_real_os_w		:= 0;

Select	a.dt_ordem_servico,
	nvl(a.dt_inicio_real, a.dt_ordem_servico),
	nvl(a.dt_fim_real, sysdate),
	b.cd_estabelecimento,
	nr_seq_estagio
into	dt_ordem_w,
	dt_inicio_w,
	dt_fim_w,
	cd_estabelecimento_w,
	nr_seq_estagio_ww
from	man_localizacao b,
	man_ordem_servico a
where	a.nr_sequencia		= nr_seq_ordem_p
and	a.nr_seq_localizacao	= b.nr_sequencia;

if	(nvl(nr_seq_estagio_ww,0) > 0) and
	(nvl(nr_seq_estagio_ww,0) <> nr_seq_estagio_w) then
	nr_seq_estagio_w := nr_seq_estagio_ww;
end if;

select 	max(dt_atualizacao)
into	dt_estagio_w
from	man_ordem_serv_estagio
where	nr_seq_estagio 	= nr_seq_estagio_p
and	nr_seq_ordem	= nr_seq_ordem_p;

if	(ie_tempo_p = 'COM') then
	select 	Man_Obter_min_Com(cd_estabelecimento_w, dt_estagio_w, sysdate)
	into	qt_min_real_os_w
	from	dual;			
else
	qt_min_real_os_w	:= (dt_inicio_w - dt_estagio_w) * 1440;
end if;

exception
	when others then
	qt_min_real_os_w		:= 0;
end;
Return qt_min_real_os_w;

END Man_Obter_Min_Sla_OS_estagio;
/