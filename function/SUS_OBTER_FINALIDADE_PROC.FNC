create or replace
function Sus_Obter_Finalidade_Proc(	cd_procedimento_p		Number,
					ie_origem_proced_p		Number)
 		    	return Varchar2 is

cd_forma_organizacao_w	Number(6);
ds_retorno_w		Varchar2(3);
			
begin

cd_forma_organizacao_w	:= sus_obter_estrut_proc(cd_procedimento_p, ie_origem_proced_p, 'C', 'F');

if	(cd_forma_organizacao_w = 030402) then
	ds_retorno_w	:= 2;
elsif	(cd_forma_organizacao_w = 030403) then
	ds_retorno_w	:= 8;
elsif	(cd_forma_organizacao_w = 030404) then
	ds_retorno_w	:= 3;
elsif	(cd_forma_organizacao_w = 030405) then
	ds_retorno_w	:= 5;
elsif	(cd_forma_organizacao_w = 030406) then
	ds_retorno_w	:= 1;
else
	ds_retorno_w	:= null;
end if;

return	ds_retorno_w;

end Sus_Obter_Finalidade_Proc;
/