CREATE OR REPLACE
FUNCTION Obter_Via_Aplicacao	(ie_via_aplicacao_p	varchar2,
					ie_opcao_p		varchar2)
					RETURN VARCHAR2 IS

/*
D - Descri��o
V - Via
*/

ds_via_aplicacao_w	varchar2(80);
ds_retorno_w		varchar2(80);
ie_via_aplicacao_w	varchar2(5);

BEGIN
if	(ie_via_aplicacao_p is not null) then
	select	ds_via_aplicacao,
			ie_via_aplicacao
	into	ds_via_aplicacao_w,
			ie_via_aplicacao_w
	from	via_aplicacao
	where	ie_via_aplicacao	= ie_via_aplicacao_p
	and	ie_situacao		= 'A';

	if	(nvl(ie_opcao_p,'D') = 'D') then
		ds_retorno_w	:= ds_via_aplicacao_w;
	end if;
	
	if	(nvl(ie_opcao_p,'V') = 'V') then
		ds_retorno_w	:= ie_via_aplicacao_w;
	end if;
end if;

RETURN	ds_retorno_w;

END	Obter_Via_Aplicacao;
/
