create or replace
function obter_seq_icone_api(	nr_prescricao_p		number,
								nr_seq_material_p	number) return varchar2 is

ds_retorno_w		varchar2(4000 char);
nr_seq_icone_w		number(10);
ds_API_available_w	varchar2(255 char);

--when 'A'    then 'Drug interactions';
--when 'B' 	then 'Drug Disease ICD10';
--when 'C'    then 'Duplicate Therapy';
--when 'D'    then 'Drug Dose Contraindications';
--when 'E'    then 'Route ContraIndications';
--when 'F'    then 'Age Contraindications';
--when 'G'    then 'Genre ContraIndications';					
--when 'H'    then 'Pregnancy ContraIndications';
--when 'I'    then 'Lactation ContraIndications';
					
cursor c01 is
	select 	a.cd_api, 
			count(a.nr_sequencia) qt_total_item,
			CASE a.cd_api
				WHEN 'A' THEN 10 	-- Drug
				WHEN 'C' THEN 20 	-- Duplication
				WHEN 'D' THEN 30 	-- Drug Dose
				WHEN 'B' THEN 40  	-- Drug Disease
				WHEN 'DDC' THEN 40	-- Drug Disease
				WHEN 'E' THEN 50 	-- Route
				WHEN 'F' THEN 60	-- Age
				WHEN 'G' THEN 70	-- Gender
				WHEN 'H' THEN 80	-- Pregnancy
				WHEN 'I' THEN 90	-- Lactation
				WHEN 'PE' THEN 110	-- Patient Education
				ELSE 999
			END nr_order_type			
	from 	alerta_api a,
			table(lista_pck.obter_lista_char(ds_API_available_w)) x
	where 	x.cd_registro = a.cd_api
	and		a.nr_prescricao 	= nr_prescricao_p
	and 	a.nr_seq_material 	= nr_seq_material_p
	and 	a.ie_status_requisicao is null
	group by cd_api,
			 CASE a.cd_api WHEN 'A' THEN 10 WHEN 'C' THEN 20 WHEN 'D' THEN 30 WHEN 'B' THEN 40 WHEN 'DDC' THEN 40 WHEN 'E' THEN 50 WHEN 'F' THEN 60 WHEN 'G' THEN 70 WHEN 'H' THEN 80 WHEN 'I' THEN 90 WHEN 'PE' THEN 110 ELSE 999 END
	order by nr_order_type;

begin

if	(nr_prescricao_p	is not null) and
	(nr_seq_material_p	is not null) then
	
	select max(get_list_API_available(a.nr_atendimento, a.cd_pessoa_fisica, 'N'))
	  into ds_API_available_w
	  from prescr_medica a
	 where a.nr_prescricao = nr_prescricao_p;
	
	for c01_w in c01
	loop
		if c01_w.qt_total_item > 0 then
			case c01_w.cd_api
				when 'A'    then nr_seq_icone_w := 1025;
				when 'B' 	then nr_seq_icone_w := 1026;
				when 'C'    then nr_seq_icone_w := 1027;
				when 'D'    then nr_seq_icone_w := 1028;
				when 'E'    then nr_seq_icone_w := 1029;
				when 'F'    then nr_seq_icone_w := 1030;
				when 'G'    then nr_seq_icone_w := 4131;
				when 'H'    then nr_seq_icone_w := 1031;
				when 'I'    then nr_seq_icone_w := 1032;
			else nr_seq_icone_w := 0;
			end case;
		
			if nr_seq_icone_w > 0 then
				if (ds_retorno_w is not null) then
					ds_retorno_w := ds_retorno_w || ',' || nr_seq_icone_w;
				else
					ds_retorno_w := nr_seq_icone_w;
				end if;
			end if;
		end if;
	end loop;

end if;

return ds_retorno_w;

end obter_seq_icone_api;
/
