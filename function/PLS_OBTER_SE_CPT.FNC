create or replace
function pls_obter_se_cpt	(nr_seq_tipo_carencia_p		number)
				return varchar2 is

ie_cpt_w		varchar2(1);

begin

select	nvl(ie_cpt,'N')
into	ie_cpt_w
from	pls_tipo_carencia
where	nr_sequencia	= nr_seq_tipo_carencia_p;

return ie_cpt_w;

end pls_obter_se_cpt;
/