create or replace
function obter_classif_risco_triagem	(nr_prescricao_p	number)
					 return number is

nr_seq_prioridade_w	number(10);
nr_sequencia_w		number(10):= null;
ie_classif_triagem_w	varchar2(1);

begin
select	nvl(max(ie_forma_classif_triagem), 'A') 
into	ie_classif_triagem_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if (ie_classif_triagem_w = 'A') then
	select	max(c.nr_seq_prioridade)
	into	nr_seq_prioridade_w
	from	pe_diagnostico a,
		pe_prescr_diag b,
		triagem_classif_risco c 
	where	a.nr_sequencia   = b.nr_seq_diag 
	and	a.nr_seq_classif = c.nr_sequencia 
	and	b.nr_seq_prescr  = nr_prescricao_p;
else
	select	min(c.nr_seq_prioridade)
	into	nr_seq_prioridade_w
	from	pe_diagnostico a,
		pe_prescr_diag b,
		triagem_classif_risco c 
	where	a.nr_sequencia   = b.nr_seq_diag 
	and	a.nr_seq_classif = c.nr_sequencia 
	and	b.nr_seq_prescr  = nr_prescricao_p;
end if;

if (nr_seq_prioridade_w is not null) then
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	triagem_classif_risco
	where	nr_seq_prioridade = nr_seq_prioridade_w
	and     nvl(ie_situacao,'A') = 'A';
end if;	
return nr_sequencia_w;
end obter_classif_risco_triagem;
/