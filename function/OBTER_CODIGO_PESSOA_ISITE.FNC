create or replace
function obter_codigo_pessoa_isite(cd_pessoa_fisica_p   varchar2)
 		    	return varchar2 is

cd_pessoa_retorno_w		varchar2(20);
cd_pessoa_fisica_w		varchar2(20);
ie_pessoa_alt_w			varchar2(2);
ie_origem_pessoa_fisica_w	varchar2(10);

begin

select 	nvl(max(ie_utiliza_pessoa_alternativa),'N'),
	nvl(max(ie_origem_pessoa_fisica),1)
into	ie_pessoa_alt_w,
	ie_origem_pessoa_fisica_w
from	empresa_integr_dados
where	nr_seq_empresa_integr = 4;


cd_pessoa_fisica_w := cd_pessoa_fisica_p;

if (ie_origem_pessoa_fisica_w = '2') then

	cd_pessoa_fisica_w := obter_select_concatenado_bv(' select SF_RETORNA_ID_PAC_TASY(:nr_prontuario) 
							    from dual',
				'nr_prontuario='||obter_prontuario_pf(cd_pessoa_fisica_p,wheb_usuario_pck.get_cd_estabelecimento),
							  '');
	if (cd_pessoa_fisica_w is null) then
		cd_pessoa_fisica_w := cd_pessoa_fisica_p;
	end if;

	
end if;



if	((ie_pessoa_alt_w = 'S') and (length(cd_pessoa_fisica_w) <= 6))	then

	cd_pessoa_retorno_w := 'M' || lpad(cd_pessoa_fisica_w,6, '0');
	
else
	
	cd_pessoa_retorno_w := cd_pessoa_fisica_w;
	
end if;

return	cd_pessoa_retorno_w;

end obter_codigo_pessoa_isite;
/
