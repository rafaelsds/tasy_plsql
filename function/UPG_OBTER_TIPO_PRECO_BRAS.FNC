create or replace
function upg_obter_tipo_preco_bras(cd_material_p		number,
				         ie_tipo_p	varchar2)
 		    	return varchar2 is

ie_tipo_preco_w	varchar2(3);		
ds_retorno_w	varchar2(255);
			
begin
select	a.ie_tipo_preco
into	ie_tipo_preco_w
from	brasindice_preco a, 
	material_brasindice b
where	a.cd_laboratorio = b.cd_laboratorio 
and	a.cd_medicamento = b.cd_medicamento 
and	a.cd_apresentacao = b.cd_apresentacao
and	b.cd_material = cd_material_p	  
and	a.dt_inicio_vigencia = (select max(x.dt_inicio_vigencia) from brasindice_preco x, material_brasindice y 
				where x.cd_laboratorio = y.cd_laboratorio 
				and x.cd_medicamento = y.cd_medicamento 
				and x.cd_apresentacao = y.cd_apresentacao
				and y.cd_material = b.cd_material);

if	(ie_tipo_p = 'T') then
	ds_retorno_w := ie_tipo_preco_w;
end if;		
			
return	ds_retorno_w;

end upg_obter_tipo_preco_bras;
/
