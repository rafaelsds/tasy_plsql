create or replace
function pls_obter_resp_credito_prot
			(	nr_seq_protocolo_p	Number,
				ie_tipo_retorno_p	Varchar2)
				return Varchar2 is
				
/* IE_TIPO_RETORNO_P
	C - C�digo
	N - Nome
*/
				
ds_retorno_w			Varchar2(255);
ie_tipo_resp_credito_w		Varchar2(10);
cd_pessoa_fisica_w		Varchar2(10);
nr_seq_segurado_w		Number(10);
cd_pf_retorno_w			Varchar2(10);
cd_pj_retorno_w			Varchar2(14);
nr_contrato_principal_w		Number(10);
nr_seq_subestipulante_w		Number(10);
nr_seq_titular_w		Number(10);

begin

/* Obter o respons�vel cr�dito do protocolo - Dom 2261*/
begin
select	ie_tipo_resp_credito,
	cd_pessoa_fisica,
	nr_seq_segurado
into	ie_tipo_resp_credito_w,
	cd_pessoa_fisica_w,
	nr_seq_segurado_w
from	pls_protocolo_conta
where	nr_sequencia	= nr_seq_protocolo_p;
exception
when others then
	ie_tipo_resp_credito_w	:= '';
	cd_pessoa_fisica_w	:= '';
end;

/* Obter dados do contrato */
select	nvl(b.nr_contrato_principal,0),
	a.nr_seq_subestipulante
into	nr_contrato_principal_w,
	nr_seq_subestipulante_w
from	pls_contrato	b,
	pls_segurado	a
where	a.nr_sequencia		= nr_seq_segurado_w
and	a.nr_seq_contrato	= b.nr_sequencia;

if	(ie_tipo_resp_credito_w = 'B') then
	/* Benefici�rio */
	select	max(cd_pessoa_fisica)
	into	cd_pf_retorno_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	cd_pj_retorno_w	:= '';
elsif	(ie_tipo_resp_credito_w = 'P') then
	/* Pagador */
	select	a.cd_pessoa_fisica,
		a.cd_cgc
	into	cd_pf_retorno_w,
		cd_pj_retorno_w
	from	pls_contrato_pagador	a,
		pls_segurado		b
	where	a.nr_sequencia	= b.nr_seq_pagador
	and	b.nr_sequencia	= nr_seq_segurado_w;
elsif	(ie_tipo_resp_credito_w = 'E') then
	/* Estipulante do benefici�rio */
	select	b.cd_cgc_estipulante,
		b.cd_pf_estipulante
	into	cd_pj_retorno_w,
		cd_pf_retorno_w
	from	pls_contrato	b,
		pls_segurado	a
	where	a.nr_sequencia		= nr_seq_segurado_w
	and	a.nr_seq_contrato	= b.nr_sequencia;
elsif	(ie_tipo_resp_credito_w = 'CP') and 
	(nr_contrato_principal_w > 0) then
	/* Contratante principal */
	select	cd_cgc_estipulante,
		cd_pf_estipulante
	into	cd_pj_retorno_w,
		cd_pf_retorno_w
	from	pls_contrato
	where	nr_sequencia	= nr_contrato_principal_w;
elsif	(ie_tipo_resp_credito_w = 'SE') then
	/* Subestipulante */
	begin
	select	cd_pessoa_fisica,
		cd_cgc
	into	cd_pf_retorno_w,
		cd_pj_retorno_w
	from	pls_sub_estipulante
	where	nr_sequencia	= nr_seq_subestipulante_w;
	exception
		when others then
		cd_pf_retorno_w	:= '';
		cd_pj_retorno_w	:= '';
	end;
elsif	(ie_tipo_resp_credito_w = 'T') then
	/* Titular */
	select	nvl(nr_seq_titular, 0)
	into	nr_seq_titular_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	if	(nr_seq_titular_w	<> 0) then
		select	cd_pessoa_fisica
		into	cd_pf_retorno_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_titular_w;
	elsif	(nr_seq_titular_w	= 0) then
		select	cd_pessoa_fisica
		into	cd_pf_retorno_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
	end if;
	cd_pj_retorno_w	:= '';
elsif	(ie_tipo_resp_credito_w = 'PI') then
	/* Pessoa f�sica identificada */
	cd_pf_retorno_w	:= cd_pessoa_fisica_w;	
	cd_pj_retorno_w	:= '';
end if;

if	(ie_tipo_retorno_p = 'C') then
	if	(nvl(cd_pf_retorno_w,'X') <> 'X') then
		ds_retorno_w	:= cd_pf_retorno_w;
	elsif	(nvl(cd_pj_retorno_w,'X') <> 'X') then
		ds_retorno_w	:= cd_pj_retorno_w;		
	end if;
elsif	(ie_tipo_retorno_p = 'N') then
	ds_retorno_w	:= obter_nome_pf_pj(cd_pf_retorno_w, cd_pj_retorno_w);
end if;

return	ds_retorno_w;

end pls_obter_resp_credito_prot;
/