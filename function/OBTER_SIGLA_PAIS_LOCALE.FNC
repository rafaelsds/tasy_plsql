create or replace
function Obter_Sigla_Pais_Locale(nm_usuario_p		varchar2)
 		    	return Varchar2 is

tag_pais_w	varchar2(15);	
cd_estabelicimento_w	number;			

begin

select	max(ds_locale) 
into	tag_pais_w
from	user_locale 
where	nm_user = nm_usuario_p;

if(tag_pais_w is null)then
	select	max(cd_estabelecimento) 
	into 	cd_estabelicimento_w
	from	usuario 
	where	nm_usuario = nm_usuario_p;

	select 	max(ds_locale) 
	into	tag_pais_w
	from	establishment_locale 
	where	cd_estabelecimento = cd_estabelicimento_w;
end if;

return	nvl(tag_pais_w, 'pt_BR');

end Obter_Sigla_Pais_Locale;
/
