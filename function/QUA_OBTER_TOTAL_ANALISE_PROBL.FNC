create or replace
function qua_obter_total_analise_probl(	nr_seq_problema_p	number)
 		    	return number is

ds_retorno_w	number(10,0) := 0;
qt_valor_w	number(05,0);

cursor c01 is
select	nvl(qt_valor,1)
from	qua_analise_problema_crit
where	nr_seq_analise = nr_seq_problema_p
and	nvl(qt_valor,0) > 0;

begin
if	(nr_seq_problema_p > 0) then
	open C01;
	loop
	fetch C01 into	
		qt_valor_w;
	exit when C01%notfound;
		begin
		if	(ds_retorno_w = 0) then
			ds_retorno_w := 1;
		end if;
		
		ds_retorno_w := ds_retorno_w * qt_valor_w;
		end;
	end loop;
	close C01;
end if;	

return	ds_retorno_w;

end qua_obter_total_analise_probl;
/