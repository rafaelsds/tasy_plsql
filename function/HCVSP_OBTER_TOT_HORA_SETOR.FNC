create or replace
function HCVSP_obter_tot_hora_setor(	dt_inicio_p		date, 
					dt_fim_p		date,
					ie_clinica_p		varchar2,
					cd_setor_atend_p 	number,
					cd_medico_p	 	number,
					ie_tipo_convenio_p	number)
				
 		    	return Varchar2 is
				
dt_cursor_w		varchar2(30);
qt_minutos_w		number(10,0) := 0;
qt_segundos_w		number(10,0) := 0;
qt_horas_w		number(10,0) := 0;
qt_minutos_tot_w		number(10,0) := 0;
qt_segundos_tot_w		number(10,0) := 0;
qt_horas_tot_w		number(10,0) := 0;
ds_retorno_w		varchar2(255);


/*ie_opcao_p
U - Total uuario dia;
TR - Total grupo dia;
TG - Total gerencia dia;
*/

cursor c01 is
select  distinct
        obter_dif_data(a.dt_entrada_unidade, a.dt_saida_unidade,'')
from   ATEND_PACIENTE_UNIDADE a,
       atendimento_paciente b
where  a.nr_atendimento = b.nr_atendimento
and    dt_entrada between dt_inicio_p and dt_fim_p
and    ((a.cd_setor_atendimento = cd_setor_atend_p ) or (cd_setor_atend_p = '0'))
and    ((b.ie_clinica = ie_clinica_p) or (ie_clinica_p = '0'))
and    ((b.cd_medico_resp = cd_medico_p) or (cd_medico_p = '0'))
and	((b.ie_tipo_convenio = ie_tipo_convenio_p) or (ie_tipo_convenio_p = '0'))
order by 1;


begin
open c01;
loop
fetch c01 into
	dt_cursor_w;
exit when c01%notfound;
	begin
	if	(substr(dt_cursor_w, (length(dt_cursor_w) - 2),1) = ':') and
		(substr(dt_cursor_w, (length(dt_cursor_w) - 5),1) = ':') then
		begin
		qt_minutos_w		:= (substr(dt_cursor_w, (length(dt_cursor_w) - 4),2));
		qt_segundos_w		:= (substr(dt_cursor_w, (length(dt_cursor_w) - 1),2));
		qt_horas_w		:= (substr(dt_cursor_w, 1, instr(dt_cursor_w,':') - 1));
		qt_minutos_tot_w		:= qt_minutos_tot_w	+ qt_minutos_w;
		qt_segundos_tot_w		:= qt_segundos_tot_w + qt_segundos_w;
		qt_horas_tot_w		:= qt_horas_tot_w	+ qt_horas_w;
		end;
	end if;
	end;
end loop;
close c01;


if	(qt_segundos_tot_w > 60) then
	while (qt_segundos_tot_w >= 60) loop
		begin
		qt_segundos_tot_w	:= qt_segundos_tot_w - 60;
		qt_minutos_tot_w	:= qt_minutos_tot_w	+ 1;
		end;
	end loop;
end if;

if	(qt_minutos_tot_w > 60) then
	while (qt_minutos_tot_w >= 60) loop
		begin
		qt_minutos_tot_w	:= qt_minutos_tot_w - 60;
		qt_horas_tot_w	:= qt_horas_tot_w + 1;
		end;
	end loop;
end if;


ds_retorno_w :=  qt_horas_tot_w || ' h ' || qt_minutos_tot_w || ' m ' || qt_segundos_tot_w || ' s ';

return	ds_retorno_w;

end  HCVSP_obter_tot_hora_setor;
/