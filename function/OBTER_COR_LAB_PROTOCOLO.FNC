create or replace
function obter_cor_lab_protocolo(	cd_protocolo_p	Number)
				return Varchar2 is

ds_cor_w		Varchar2(255);

begin

select	max(ds_cor)
into	ds_cor_w
from	lab_protocolo_cor
where	cd_protocolo	= cd_protocolo_p;

return	ds_cor_w;

end obter_cor_lab_protocolo;
/
