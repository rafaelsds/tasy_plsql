create or replace
function pls_obter_cgc_congenere
			(	nr_sequencia_p		number)
				return varchar2 is

ds_razao_social_w		varchar2(255);
nr_sequencia_w			number(10);
	
begin

select	nr_sequencia,
	substr(obter_nome_pf_pj(null, cd_cgc),1,254)
into	nr_sequencia_w,
	ds_razao_social_w
from	pls_congenere
where	nr_sequencia	= nr_sequencia_p;

return	ds_razao_social_w;

end pls_obter_cgc_congenere;
/