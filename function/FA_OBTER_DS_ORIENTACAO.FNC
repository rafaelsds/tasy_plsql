create or replace
function fa_obter_ds_orientacao(nr_sequencia_p number)
				    return varchar2 is

ds_retorno_w	varchar2(60);
			
begin

if (nr_sequencia_p is not null) then 
	select max(ds_orientacao)
	into ds_retorno_w
	from fa_orientacao_medic
	where nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end fa_obter_ds_orientacao;
/