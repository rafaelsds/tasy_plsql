create or replace
function mprev_obter_seq_ciclo_ant(	nr_seq_prog_partic_mod_p	number)
					return number is

nr_seq_prog_part_mod_ant_w	mprev_prog_partic_modulo.nr_sequencia%type;
nr_seq_ciclo_ant_w		mprev_prog_partic_modulo.nr_sequencia%type;
nr_retorno_w			mprev_prog_partic_modulo.nr_sequencia%type;
nr_seq_programa_partic_w	mprev_programa_partic.nr_sequencia%type;
nr_seq_modulo_w			mprev_modulo_atend.nr_sequencia%type;
nr_seq_agrupamento_w		mprev_modulo_atend.nr_seq_agrupamento%type;
nr_seq_participante_w		mprev_participante.nr_sequencia%type;

begin

if	(nr_seq_prog_partic_mod_p is not null) then
	
	select 	b.nr_seq_participante,
		c.nr_seq_modulo,
		d.nr_seq_agrupamento
	into	nr_seq_participante_w,
		nr_seq_modulo_w,
		nr_seq_agrupamento_w
	from	mprev_modulo_atend d,
		mprev_programa_modulo c,
		mprev_programa_partic b,
		mprev_prog_partic_modulo a
	where	a.nr_seq_programa_partic	= b.nr_sequencia
	and	a.nr_seq_prog_modulo		= c.nr_sequencia
	and	c.nr_seq_modulo			= d.nr_sequencia
	and	a.nr_sequencia 			= nr_seq_prog_partic_mod_p;
	
	/*Verifica se  o modulo que esta sendo gerado ja foi gerado para pegar a sequencia da mprev_partic_ciclo_atend */
	select  max(a.nr_sequencia)
	into	nr_seq_prog_part_mod_ant_w
	from	mprev_programa_partic c,
		mprev_programa_modulo b,
		mprev_prog_partic_modulo a
	where	a.nr_seq_prog_modulo	= b.nr_sequencia
	and	c.nr_sequencia		= a.nr_seq_programa_partic
	and	b.nr_sequencia 		= nr_seq_modulo_w
	and	c.nr_seq_participante	= nr_seq_participante_w
	and	a.nr_sequencia <> nr_seq_prog_partic_mod_p
	and	a.dt_cancelamento is null;
		
	if	(nr_seq_prog_part_mod_ant_w is null) and
		(nvl(nr_seq_agrupamento_w, 0) > 0) then
		/*Verifica se  o modulo que esta sendo gerado pertence ao mesmo agrupamento dos que ja foram gerados */
		select  max(a.nr_sequencia)
		into	nr_seq_prog_part_mod_ant_w
		from	mprev_modulo_atend d,
			mprev_programa_partic c,
			mprev_programa_modulo b,
			mprev_prog_partic_modulo a
		where	a.nr_seq_prog_modulo	= b.nr_sequencia
		and	c.nr_sequencia		= a.nr_seq_programa_partic
		and	b.nr_seq_modulo		= nr_seq_modulo_w
		and	d.nr_seq_agrupamento	= nr_seq_agrupamento_w
		and	c.nr_seq_participante	= nr_seq_participante_w
		and	a.nr_sequencia <> nr_seq_prog_partic_mod_p
		and	a.dt_cancelamento is null;
	end if;
	
	if	(nr_seq_prog_part_mod_ant_w is not null) then
		--Pega o ultimo registro do ciclo do atendimento do modulo anterior
		select	max(nr_sequencia)
		into	nr_seq_ciclo_ant_w
		from	mprev_partic_ciclo_atend
		where	nr_seq_prog_partic_mod = nr_seq_prog_part_mod_ant_w;
	end if;
	
end if;

nr_retorno_w	:= nr_seq_ciclo_ant_w;

return	nr_retorno_w;

end mprev_obter_seq_ciclo_ant;
/
