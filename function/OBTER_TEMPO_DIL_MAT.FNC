create or replace
function obter_tempo_dil_mat(	nr_seq_paciente_p		number,
								cd_material_p		number,
								cd_diluente_p		number,
								ie_via_aplicacao_p	varchar2 default null)
								return number is

qt_minuto_aplicacao_w	number(4);
cd_estabelecimento_w	number(10);
qt_idade_w				number(10);
qt_peso_w				number(10);
cd_pessoa_fisica_w		varchar2(10);

begin
select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	paciente_setor
where	nr_seq_paciente = nr_seq_paciente_p;

cd_estabelecimento_w 	:= obter_estabelecimento_ativo;
qt_idade_w 				:= obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A');
qt_peso_w				:= obter_peso_pf(cd_pessoa_fisica_w);

select	max(qt_minuto_aplicacao)
into	qt_minuto_aplicacao_w
from	material_diluicao
where	cd_material 	= cd_material_p
and		cd_diluente 	= cd_diluente_p
and		cd_estabelecimento	= cd_estabelecimento_w
and		qt_idade_w between nvl(qt_idade_min,0) and nvl(qt_idade_max,999)
and		qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999)
and		((ie_via_aplicacao_p is null) or (ie_via_aplicacao = ie_via_aplicacao_p));

return	qt_minuto_aplicacao_w;

end obter_tempo_dil_mat;
/