create or replace
function	qua_obter_se_existe_valid_doc(
			nr_seq_documento_p		number)
		return varchar2 is

qt_existe_w	number(10);
ie_retorno_w	varchar2(1);

begin

ie_retorno_w	:= 'N';

select	count(*)
into	qt_existe_w
from	qua_documento
where	nr_sequencia = nr_seq_documento_p
and	cd_pessoa_validacao is not null;

if	(qt_existe_w = 0) then
	begin

	select	count(*)
	into	qt_existe_w
	from	qua_doc_validacao
	where	nr_seq_doc = nr_seq_documento_p
	and	((cd_pessoa_validacao is not null) or
		(cd_cargo is not null));

	if	(qt_existe_w > 0) then
		ie_retorno_w	:= 'S';
	end if;

	end;
else
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end qua_obter_se_existe_valid_doc;
/