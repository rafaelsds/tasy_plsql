create or replace
function obter_imp_tit_pag_especif
		(nr_titulo_p	number) 
		return number is

vl_imposto_w	number(15,2);

begin

	select	nvl(sum(b.vl_imposto),0)
	into	vl_imposto_w
	from	titulo_pagar_imposto b,
		titulo_pagar a,
		tributo t
	where	a.nr_titulo = b.nr_titulo
	and	b.cd_tributo = t.cd_tributo
	and	a.nr_titulo = nr_titulo_p
	and	(t.IE_TIPO_TRIBUTO = 'PIS'
	or 	t.IE_TIPO_TRIBUTO  = 'COFINS' 
	or 	t.IE_TIPO_TRIBUTO = 'CSLL' 
	or 	t.IE_TIPO_TRIBUTO = 'IR' 
	or 	t.IE_TIPO_TRIBUTO  = 'ISS');

return	vl_imposto_w;

end;
/