create or replace
function get_soap_radiology(nr_atendimento_p number,
							 ie_record_type_p		varchar2,
							 ie_soap_type_p		varchar2,
							 ie_stage_p		number,
							 dt_exam_exec_p		date,
							 ds_proc_exame_p		varchar2,
							 cd_exam_type_p		varchar2,
							 ds_comments_p		varchar2,
							 cd_body_part_p		varchar2) return varchar2 is

	ds_content_w		varchar2(255);

begin
  
	ds_content_w  := '<b>' || obter_desc_expressao (345437) || ' ' || ds_proc_exame_p || '</b></br>';
    ds_content_w  := ds_content_w || '<b>' || obter_desc_expressao (302881) || ': ' || cd_exam_type_p || '</b></br>';
	ds_content_w  := ds_content_w || '<b>' || obter_desc_expressao (310282) || ': ' || cd_body_part_p || '</b></br>';
	ds_content_w  := ds_content_w || '<b>' || obter_desc_expressao (285523) || ': ' || ds_comments_p || '</b></br>';
	ds_content_w  := ds_content_w || '<b>' || obter_desc_expressao (286865) || ': ' || dt_exam_exec_p || '</b></br>';
  
	return ds_content_w;
end get_soap_radiology;
/
