create or replace
function obter_se_elem_cliente(	nr_seq_template_p	number)
 		    	return number is

qt_reg_w	number(10);

begin

select	count(*)
into	qt_reg_w
from	ehr_template_conteudo a,
	ehr_elemento b
where	b.nr_sequencia = a.nr_seq_elemento
and	b.ie_origem = 'C'
and	a.nr_seq_template = nr_seq_template_p;


return	qt_reg_w;

end obter_se_elem_cliente;
/