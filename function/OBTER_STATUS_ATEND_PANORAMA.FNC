create or replace function obter_status_atend_panorama(nr_seq_interno_p number) return number is
        
cursor c_status_atend is
select  *
from    ocupacao_unidade_v o
where   nr_seq_interno = nr_seq_interno_p
order by o.nr_seq_apresent, o.cd_unidade;

nr_status_w            		number(5);
cd_perfil_w					number(5);
cd_estabelecimento_w        number(5);
cd_exp_titulo_w             number(10);
nr_seq_texto_titulo_w       number(10);
ie_legenda_ap_w				varchar2(1);
ie_alta_medico_w			varchar2(1);
ie_alta_medico_prioridade_w	varchar2(1);
nm_usuario_w				varchar2(15);
ds_imagem_hover_w			varchar2(100);
ds_imagem_padrao_w			varchar2(100);
ds_alerta_w             	varchar2(10000) := null;
ie_sexo_w               	ocupacao_unidade_v.ie_sexo%type;
ie_acompanhante_w       	ocupacao_unidade_v.ie_acompanhante%type;
ie_paciente_isolado_w   	wocupacao_leito.ie_paciente_isolado%type;
dt_previsto_alta_w			ocupacao_unidade_v.dt_previsto_alta%type;
nr_status_atend_w			number(5);
		
begin
nm_usuario_w 		 := wheb_usuario_pck.get_nm_usuario;
cd_perfil_w  		 := wheb_usuario_pck.get_cd_perfil;
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

for r_status_atend in c_status_atend loop

obter_param_usuario(44, 119, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_alta_medico_w);
obter_param_usuario(44, 148, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_legenda_ap_w);
obter_param_usuario(44, 213, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_alta_medico_prioridade_w);

	if (r_status_atend.cd_pessoa_fisica is null) then
	  ie_sexo_w := r_status_atend.ie_sexo_paciente;
	else
	  ie_sexo_w := r_status_atend.ie_sexo;
	end if;

	if (ie_legenda_ap_w = 's') then
		dt_previsto_alta_w := r_status_atend.dt_previsto_alta;
	else
		dt_previsto_alta_w := null;
	end if;

	select max(substr(obter_status_isolamento_ocup(cd_estabelecimento_w, r_status_atend.nr_atendimento, r_status_atend.ie_acompanhante, nm_usuario_w), 1, 1))
	into   ie_paciente_isolado_w
	from   dual;

	select nvl(max(to_number(obter_imagem_leito_ocup_hosp(
								obter_status_leito_ocupacao(ie_paciente_isolado_w,
															substr(obter_classif_etaria(cd_estabelecimento_w, r_status_atend.cd_pessoa_fisica, 'c'), 1, 3),
															ie_sexo_w,
															r_status_atend.ie_status_unidade,
															r_status_atend.ie_temporario,
															r_status_atend.dt_alta_medico,
															ie_alta_medico_w,
															r_status_atend.nr_atend_alta,
															r_status_atend.ie_radiacao,
															r_status_atend.ie_interditado_radiacao,
															dt_previsto_alta_w,
															obter_se_alta_tesouraria(r_status_atend.cd_setor_atendimento, r_status_atend.cd_unidade_basica, r_status_atend.cd_unidade_compl),
															null,
															r_status_atend.nr_seq_interno,
															r_status_atend.dt_saida_temporaria,
															r_status_atend.dt_retorno_saida_temporaria)))), 0)
		into nr_status_atend_w
		from dual;
	
	if(nr_status_atend_w = 278) then
        nr_status_atend_w := obter_status_alta_panora(r_status_atend.nr_atendimento, obter_perfil_ativo());
    end if;

	return nr_status_atend_w;

end loop;

return 0;

end obter_status_atend_panorama;
/