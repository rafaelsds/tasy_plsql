create or replace
function hsc_obter_se_imprime (nr_prescricao_p	number)
 		    	return varchar2 is

ie_imprime_w	varchar2(1) := 'N';
qt_w		number(10);
			
begin

select 	count(*)
into	qt_w
from 	pessoa_fisica f,
	prescr_medica a
where	a.nr_prescricao 	= nr_prescricao_p
and	a.cd_pessoa_fisica	= f.cd_pessoa_fisica
and	a.nr_atendimento	is null
--and	Obter_Convenio_Atendimento(a.nr_atendimento) = 5002
and	exists (	select	distinct 1
		from	prescr_material z
		where	z.nr_prescricao = a.nr_prescricao
		--and	z.cd_convenio = 5002
		and	z.ie_agrupador = 1
		and	nvl(z.NR_DIA_UTIL,0) in (0,1)
		and	obter_ctrl_antimicrobiano_mat(z.cd_material) > 0
	        and	cd_material in(20080, 20081, 41053, 2730,2735,41052, 2734, 2731, 67866, 94729, 67863, 94730, 67864, 78562, 78563,70732,70733));
		
if	(qt_w = 0) then
	select	count(*)
	into	qt_w
	from 	pessoa_fisica f,
		categoria_convenio o,
		convenio v,
		atend_categoria_convenio c,
		atendimento_paciente b,
		prescr_medica a
	where	a.nr_prescricao 	= nr_prescricao_p
	and   	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_atendimento	is not null
	and	b.nr_atendimento	= c.nr_atendimento
	and	v.cd_convenio	= o.cd_convenio
	--and	o.cd_convenio	= 5002
	and	o.cd_convenio	= c.cd_convenio
	and	o.cd_categoria	= c.cd_categoria
	and	b.cd_pessoa_fisica	= f.cd_pessoa_fisica
	and	c.nr_seq_interno 	= obter_atecaco_atendimento(a.nr_atendimento)
	and	exists (	select	distinct 1
			from	prescr_material z
			where	z.nr_prescricao = a.nr_prescricao
			and	nvl(z.NR_DIA_UTIL,0) in (0,1)
			and	z.ie_agrupador = 1
			and	obter_ctrl_antimicrobiano_mat(z.cd_material) > 0
			and	cd_material in(20080, 20081, 41053, 2730,2735,41052, 2734, 2731, 67866, 94729, 67863, 94730, 67864, 78562, 78563,70732,70733));

end if;

if	(qt_w > 0) then
	ie_imprime_w	:= 'S';
end if;

return	ie_imprime_w;

end hsc_obter_se_imprime;
/
