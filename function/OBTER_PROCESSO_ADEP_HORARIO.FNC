create or replace
function obter_processo_adep_horario(nr_seq_horario_p number)
 		    	return varchar2 is

nr_seq_processo_w	varchar2(20);

begin

if (nr_seq_horario_p is not null) then

	select	max(nr_seq_processo)
	into	nr_seq_processo_w
	from	prescr_mat_hor
	where	nr_sequencia = nr_seq_horario_p;

end if;

return nvl(nr_seq_processo_w,'');

end obter_processo_adep_horario;
/
