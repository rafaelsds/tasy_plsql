create or replace
function obter_medico_convenio(	
					cd_estabelecimento_p	number,
					cd_pessoa_fisica_p		varchar2,
					cd_convenio_p		number,
					cd_prestador_p		varchar2 	default null,
					cd_especialidade_p		number	default null,
					cd_categoria_p		varchar2	default null,
					cd_setor_atendimento_p	number	default null,
					dt_referencia_p		date	default	sysdate,
					ie_tipo_atendimento_p	number	default null,
					ie_funcao_medico_p	varchar2	default null,
					ie_carater_inter_sus_p	varchar2	default null)
					return varchar2 is

cd_medico_convenio_w		varchar2(15);
dt_referencia_w			date;

cursor c01 is
	select	cd_medico_convenio
	from	medico_convenio
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	cd_convenio	= cd_convenio_p
	and	((cd_estabelecimento = cd_estabelecimento_p) or (cd_estabelecimento is null))
	and 	((nvl(cd_prestador,'0')	= nvl(cd_prestador_p, '0')) or (cd_prestador is null))
	and 	((nvl(cd_especialidade,0) = nvl(cd_especialidade_p,0)) or (cd_especialidade is null))
	and 	((cd_categoria is null) or (nvl(cd_categoria,'0')  = nvl(cd_categoria_p,'0')))
	and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_p,0)) = nvl(cd_setor_atendimento_p,0)
	and	dt_referencia_w between nvl(dt_inicio_vigencia,dt_referencia_w) and fim_dia(nvl(dt_final_vigencia,dt_referencia_w))
	and	(nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0)) = nvl(ie_tipo_atendimento_p,0) or (ie_tipo_atendimento_p is null))
	and	((nvl(ie_funcao_medico, nvl(ie_funcao_medico_p,'0')) = nvl(ie_funcao_medico_p, '0')) or (nvl(ie_funcao_medico_p,'0') = '0'))
	and	nvl(ie_carater_inter_sus, nvl(ie_carater_inter_sus_p,'0')) = nvl(ie_carater_inter_sus_p,'0')
	order by 
		nvl(cd_estabelecimento,0),	
		nvl(cd_especialidade,0),
		somente_numero(nvl(cd_prestador,'0')),
		somente_numero(nvl(cd_categoria,'0')),
		nvl(cd_setor_atendimento,0),
		somente_numero(nvl(ie_funcao_medico, '0')),
		somente_numero(nvl(ie_carater_inter_sus,'0'));

begin

cd_medico_convenio_w	:= '';
dt_referencia_w		:= nvl(dt_referencia_p,sysdate);

open c01;
loop
fetch c01 into
	cd_medico_convenio_w;
exit when c01%notfound;
	cd_medico_convenio_w		:= cd_medico_convenio_w;
end loop;
close c01;


return cd_medico_convenio_w;

end obter_medico_convenio;
/
