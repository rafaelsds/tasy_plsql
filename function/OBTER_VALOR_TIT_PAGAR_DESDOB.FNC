create or replace
function OBTER_VALOR_TIT_PAGAR_DESDOB
		(nr_titulo_p	number) return number is

vl_titulo_w		number(15,2);
vl_desdobrado_w		number(15,2);
vl_retorno_w		number(15,2);

begin

select	vl_titulo
into	vl_titulo_w
from	titulo_pagar
where	nr_titulo	= nr_titulo_p;

select	nvl(sum(vl_titulo),0)
into	vl_desdobrado_w
from	titulo_pagar_desdob
where	nr_titulo_dest is not null
and	nr_titulo = nr_titulo_p;
	
vl_retorno_w	:= vl_titulo_w - vl_desdobrado_w;

return	nvl(vl_retorno_w,0);

end OBTER_VALOR_TIT_PAGAR_DESDOB;
/