create or replace
function obter_via_aplic_medic(cd_material_p number)
					return varchar2 is

ie_via_aplicacao_w	varchar2(5);

begin

if	(cd_material_p is not null) then
	begin
	select	nvl(max(ie_via_aplicacao),'')
	into	ie_via_aplicacao_w
	from	material
	where	cd_material = cd_material_p;
	end;
end if;

return	ie_via_aplicacao_w;

end obter_via_aplic_medic;
/