create or replace
function vivale_obter_proc_paciente(nr_atendimento_p	number,
				 ie_opcao_proced_p	varchar,
				 ie_opcao_p	varchar)
 		    	return varchar2 is
			
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
nr_seq_proc_interno_w	number(10);
nr_seq_exame_w		number(10);

nr_primeiro_proc_w	number(10);
nr_segundo_proc_w	number(10);

ds_prim_proc_w		varchar2(255);
ds_seg_proc_w		varchar2(255);

ds_retorno_w		varchar2(255);

begin

/* opcao proced op��o do procedimento p = primeiro, s segundo
op��o d = descri��o c= codigo
*/

if	(nr_atendimento_p is not null) then

		select	nvl(min(nr_sequencia),0)
		into	nr_primeiro_proc_w
		from	procedimento_paciente
		where	nr_atendimento = nr_atendimento_p;


	if	(ie_opcao_proced_p = 'P') and 
		(nr_primeiro_proc_w <> 0) then
	
		select	cd_procedimento,
			ie_origem_proced,
			nr_seq_proc_interno,
			nr_seq_exame
		into	cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			nr_seq_exame_w
		from	procedimento_paciente
		where	nr_atendimento = nr_atendimento_p
		and	nr_sequencia = nr_primeiro_proc_w;
		
		if	(ie_opcao_p = 'C') then
		
			ds_retorno_w := cd_procedimento_w;
		elsif	(ie_opcao_p = 'D') then
			
			select	substr(Obter_Desc_Prescr_Proc_exam(cd_procedimento_w, ie_origem_proced_w,nr_seq_proc_interno_w,nr_seq_exame_w),1,255)
			into	ds_prim_proc_w
			from	dual;
			
			ds_retorno_w := ds_prim_proc_w;
			
		end if;	
	
	elsif	(ie_opcao_proced_p = 'S') and 
		(nr_primeiro_proc_w <> 0) then
	
		select	min(nr_sequencia)
		into	nr_segundo_proc_w
		from	procedimento_paciente
		where	nr_atendimento = nr_atendimento_p
		and	nr_sequencia <> nr_primeiro_proc_w;
		
		select	cd_procedimento,
			ie_origem_proced,
			nr_seq_proc_interno,
			nr_seq_exame
		into	cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			nr_seq_exame_w
		from	procedimento_paciente
		where	nr_atendimento = nr_atendimento_p
		and	nr_sequencia = nr_segundo_proc_w;
		
		if	(ie_opcao_p = 'C') then
		
			ds_retorno_w := cd_procedimento_w;
			
		elsif	(ie_opcao_p = 'D') then
			
			select	substr(Obter_Desc_Prescr_Proc_exam(cd_procedimento_w, ie_origem_proced_w,nr_seq_proc_interno_w,nr_seq_exame_w),1,255)
			into	ds_seg_proc_w
			from	dual;
			
			ds_retorno_w := ds_seg_proc_w;
			
		end if;	
	
	end if;
	
end if;

return	ds_retorno_w;

end vivale_obter_proc_paciente;
/