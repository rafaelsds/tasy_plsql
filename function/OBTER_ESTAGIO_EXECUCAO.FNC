create or replace
function Obter_estagio_execucao(	ie_opcao_p	varchar2 )
				return varchar2 is

ds_retorno_w 	varchar2(255);
				
				
begin

if	(ie_opcao_p = 'P') then
		
		ds_retorno_w := substr(obter_desc_expressao(737627),1,255);
	
elsif	(ie_opcao_p = 'Y') then

		ds_retorno_w := substr(obter_desc_expressao(667691),1,255);
		
elsif	(ie_opcao_p = 'E') then

		ds_retorno_w := substr(obter_desc_expressao(315754),1,255);

elsif	(ie_opcao_p = 'R') then

		ds_retorno_w := substr(obter_desc_expressao(892917),1,255);
		
elsif	(ie_opcao_p = 'V') then
		
		ds_retorno_w := substr(obter_desc_expressao(892919),1,255);
		
elsif	(ie_opcao_p = 'L') then

		ds_retorno_w := substr(obter_desc_expressao(301227),1,255);		

elsif	(ie_opcao_p = 'A') then

		ds_retorno_w := substr(obter_desc_expressao(892921),1,255);

elsif	(ie_opcao_p = 'F') then

		ds_retorno_w := substr(obter_desc_expressao(892921),1,255);	
		
elsif	(ie_opcao_p = 'D') then

		ds_retorno_w := substr(obter_desc_expressao(654443),1,255);	
	
end if;	

return	ds_retorno_w;

end Obter_estagio_execucao;
/