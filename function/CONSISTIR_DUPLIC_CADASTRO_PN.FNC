create or replace
function consistir_duplic_cadastro_pn(	cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type,
										dt_nascimento_p			pessoa_fisica.dt_nascimento%type,
										nm_mae_p				compl_pessoa_fisica.nm_contato_pesquisa%type,
										nr_identidade_p			pessoa_fisica.nr_identidade%type,
										ds_given_name_p			person_name.ds_given_name%type,
										ds_family_name_p 		person_name.ds_family_name%type,
										ds_component_name_1_p 	person_name.ds_component_name_1%type,
										ds_component_name_2_p 	person_name.ds_component_name_2%type,
										ds_component_name_3_p 	person_name.ds_component_name_3%type) 
										return varchar2 is

/* variaveis do paciente */
dt_nasc_param_w		date;
nm_mae_param_w		varchar2(60);

/* variaveis do paciente duplicidade */
cd_pessoa_duplic_w		varchar2(10);
nm_pessoa_duplic_w		varchar2(60);
nr_pront_duplic_w		number(10, 0);
nr_seq_person_name_w 	person_name.nr_sequencia%type;

/* mensagem de retorno */
ds_consistencia_w	varchar2(255);
ds_duplicidade_w	varchar2(255);

ie_somente_nome_w	varchar2(255);

cursor duplicated_names is
	select	1 record_order, /* Check patient name */
			max(pf.cd_pessoa_fisica) cd_pessoa_fisica
	from	pessoa_fisica pf,
			person_name pn
	where	pf.nr_seq_person_name 						= pn.nr_sequencia
	and		pf.cd_pessoa_fisica							<> cd_pessoa_fisica_p
	and	 	upper(nvl(pn.ds_given_name, ' '))			= upper(nvl(ds_given_name_p, ' '))
	and	  	upper(nvl(pn.ds_family_name, ' '))			= upper(nvl(ds_family_name_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_1, ' '))		= upper(nvl(ds_component_name_1_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_2, ' '))		= upper(nvl(ds_component_name_2_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_3, ' '))		= upper(nvl(ds_component_name_3_p, ' '))
	and		nvl(nr_identidade_p, '0')					= '0'
	and		nvl(ie_somente_nome_w, 'N')					= 'N'
	and		nvl(pf.ie_status_usuario_event, 'A')		<> 'I'
	group by 1
	union
	select	2 record_order, /* Check patient name and date of birth */
			max(pf.cd_pessoa_fisica) cd_pessoa_fisica
	from	pessoa_fisica pf,
			person_name pn
	where	pf.nr_seq_person_name 						= pn.nr_sequencia
	and		pf.cd_pessoa_fisica							<> cd_pessoa_fisica_p
	and		pf.dt_nascimento							= dt_nasc_param_w
	and	 	upper(nvl(pn.ds_given_name, ' '))			= upper(nvl(ds_given_name_p, ' '))
	and	  	upper(nvl(pn.ds_family_name, ' '))			= upper(nvl(ds_family_name_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_1, ' '))		= upper(nvl(ds_component_name_1_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_2, ' '))		= upper(nvl(ds_component_name_2_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_3, ' '))		= upper(nvl(ds_component_name_3_p, ' '))
	and		nvl(nr_identidade_p, '0')					= '0'
	and		nvl(pf.ie_status_usuario_event, 'A')		<> 'I'
	group by 2
	union
	select	3 record_order, /* Check patient name and mother name */
			max(pf.cd_pessoa_fisica) cd_pessoa_fisica
	from	compl_pessoa_fisica cpf,
			pessoa_fisica pf,
			person_name pn
	where	pf.nr_seq_person_name 						= pn.nr_sequencia
	and		cpf.ie_tipo_complemento						= 5
	and		cpf.cd_pessoa_fisica						= pf.cd_pessoa_fisica
	and		pf.cd_pessoa_fisica							<> cd_pessoa_fisica_p
	and	 	upper(nvl(pn.ds_given_name, ' '))			= upper(nvl(ds_given_name_p, ' '))
	and	  	upper(nvl(pn.ds_family_name, ' '))			= upper(nvl(ds_family_name_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_1, ' '))		= upper(nvl(ds_component_name_1_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_2, ' '))		= upper(nvl(ds_component_name_2_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_3, ' '))		= upper(nvl(ds_component_name_3_p, ' '))
	and		cpf.nm_contato_pesquisa						= nm_mae_param_w
	and		nvl(pf.ie_status_usuario_event, 'A')		<> 'I'
	group by 3
	union
	select	4 record_order, /* Check name, date of birth and id*/
			max(pf.cd_pessoa_fisica) cd_pessoa_fisica
	from	pessoa_fisica pf,
			person_name pn
	where	pf.nr_seq_person_name 						= pn.nr_sequencia
	and		pf.cd_pessoa_fisica							<> cd_pessoa_fisica_p
	and	 	upper(nvl(pn.ds_given_name, ' '))			= upper(nvl(ds_given_name_p, ' '))
	and	  	upper(nvl(pn.ds_family_name, ' '))			= upper(nvl(ds_family_name_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_1, ' '))		= upper(nvl(ds_component_name_1_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_2, ' '))		= upper(nvl(ds_component_name_2_p, ' '))
	and	  	upper(nvl(pn.ds_component_name_3, ' '))		= upper(nvl(ds_component_name_3_p, ' '))
	and		pf.dt_nascimento							= dt_nasc_param_w
	and		pf.nr_identidade							= nr_identidade_p
	and		nvl(pf.ie_status_usuario_event, 'A')		<> 'I'
	group by 4
	order by 1, 2;

begin

ds_duplicidade_w := '';

obter_param_usuario(5, 162, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_somente_nome_w);

if (cd_pessoa_fisica_p is not null) then
	
	dt_nasc_param_w		:= dt_nascimento_p;
	nm_mae_param_w		:= substr(padronizar_nome(nm_mae_p), 1, 60);
	
	for duplicated_name in duplicated_names loop
		/* obter dados do paciente duplicidade */
		
		select	max(pf.nr_prontuario),
				max(pf.nr_seq_person_name)
		into	nr_pront_duplic_w,
				nr_seq_person_name_w
		from	pessoa_fisica pf
		where	pf.cd_pessoa_fisica	= duplicated_name.cd_pessoa_fisica;
		
		nm_pessoa_duplic_w := pkg_name_utils.get_person_name(nr_seq_person_name_w, wheb_usuario_pck.get_cd_estabelecimento, 'full', 'main', pkg_i18n.get_user_locale);

		/* identificar consistencia */
		if (duplicated_name.record_order = 1) then
			-- 'Nome: '		|| nm_pessoa_duplic_w || ' Cod.: '|| cd_pessoa_duplic_w || ' Prontuario: ' || nr_pront_duplic_w;
			ds_consistencia_w :=  Wheb_mensagem_pck.get_texto(306499, 'NM_PESSOA_DUPLIC_W='||nm_pessoa_duplic_w||';CD_PESSOA_DUPLIC_W='||duplicated_name.cd_pessoa_fisica||';NR_PRONT_DUPLIC_W='||nr_pront_duplic_w);  
		elsif (duplicated_name.record_order = 2) then
		    -- 'Nome e data nasc: ' || nm_pessoa_duplic_w || ' Cod.: '|| cd_pessoa_duplic_w || ' Prontuario: ' || nr_pront_duplic_w; 
			ds_consistencia_w :=  Wheb_mensagem_pck.get_texto(306505, 'NM_PESSOA_DUPLIC_W='||nm_pessoa_duplic_w||';CD_PESSOA_DUPLIC_W='||duplicated_name.cd_pessoa_fisica||';NR_PRONT_DUPLIC_W='||nr_pront_duplic_w);
		elsif (duplicated_name.record_order = 3) then
			-- 'Nome e nome mae: '	|| nm_pessoa_duplic_w || ' Cod.: '|| cd_pessoa_duplic_w || ' Prontuario: ' || nr_pront_duplic_w;
			ds_consistencia_w := Wheb_mensagem_pck.get_texto(306507, 'NM_PESSOA_DUPLIC_W='||nm_pessoa_duplic_w||';CD_PESSOA_DUPLIC_W='||duplicated_name.cd_pessoa_fisica||';NR_PRONT_DUPLIC_W='||nr_pront_duplic_w);
		elsif (duplicated_name.record_order = 4) then
		    -- 'Nome, data nasc. e identidade: ' || nm_pessoa_duplic_w || 'Cod.: '|| cd_pessoa_duplic_w || ' Prontuario: ' || nr_pront_duplic_w;
			ds_consistencia_w := Wheb_mensagem_pck.get_texto(306509, 'NM_PESSOA_DUPLIC_W='||nm_pessoa_duplic_w||';CD_PESSOA_DUPLIC_W='||duplicated_name.cd_pessoa_fisica||';NR_PRONT_DUPLIC_W='||nr_pront_duplic_w);
		end if;

		/* montar mensagem retorno */
		if ((nvl(length(ds_duplicidade_w), 0) + nvl(length(ds_consistencia_w), 0)) < 250) then
			ds_duplicidade_w := ds_duplicidade_w || ds_consistencia_w || CHR(13) || CHR(10);
		end if;
	end loop;
end if;

return ds_duplicidade_w;

end consistir_duplic_cadastro_pn;
/