create or replace
function obter_medida_referencia_laudo
		(nr_seq_laudo_med_p		number)
		return varchar2 is

ds_refer_medida_w		varchar2(80);
nr_sequencia_w			number(10);
qt_idade_minima_w		number(3);
qt_idade_maxima_w		number(3);
qt_peso_minimo_w		number(3);
qt_peso_maximo_w		number(3);
ds_medida_ref_w			varchar2(80);
nr_seq_laudo_w			number(10);
cd_pessoa_w			varchar2(10);
dt_nascimento_w			date;
qt_idade_w			number(5);
qt_peso_w			number(5);
nr_atendimento_w		number(15);
dt_laudo_w			date;
ie_sexo_w			varchar2(1);

nr_prescricao_w			number(10);

nr_seq_med_refer_w		number(10);
vl_minimo_adic_w		number(15,4);
vl_maximo_adic_w		number(15,4);
nr_seq_med_adic_w		number(10);
ds_refer_med_ant_w 		varchar2(80);
vl_exame_adic_w			number(15,4);

ie_tipo_medida_w		varchar2(15);


cursor c01 is
	select	nr_sequencia,
		qt_idade_minima,
		qt_idade_maxima,
		qt_peso_minimo,
		qt_peso_maximo,
		ds_medida_referencia
	from	medida_exame_refer
	where	nr_seq_medida = nr_sequencia_w
	and	(ie_sexo = ie_sexo_w or ie_sexo = 'A')
	and	(nvl(ie_tipo_medida,1) = nvl(nvl(ie_tipo_medida_w, ie_tipo_medida) ,1))	  
	order by ie_sexo;

	
cursor c02 is
	select	vl_minimo,
		vl_maximo,
		nr_seq_med_adic
	from	medida_exame_ref_adic
	where	nr_seq_med_refer = nr_seq_med_refer_w;

begin

select	b.ds_medida_referencia,
	b.nr_sequencia,
	a.nr_seq_laudo
into	ds_refer_medida_w,
	nr_sequencia_w,
	nr_seq_laudo_w
from	medida_exame_laudo b,
	laudo_paciente_medida a
where	a.nr_seq_medida		= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_laudo_med_p;


select	nr_atendimento,
	dt_laudo,
	nr_prescricao,
	ie_tipo_medida
into	nr_atendimento_w,
	dt_laudo_w,
	nr_prescricao_w,
	ie_tipo_medida_w
from	laudo_paciente
where	nr_sequencia = nr_seq_laudo_w;


select	obter_pessoa_atendimento(nr_atendimento_w, 'C')
into	cd_pessoa_w
from	dual;

select	dt_nascimento,
	nvl(ie_sexo,'A')
into	dt_nascimento_w,
	ie_sexo_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_w;

select	obter_idade(dt_nascimento_w,dt_laudo_w,'A')
into	qt_idade_w
from	dual;

select	nvl(obter_peso_prescr_sinal(nr_prescricao_w),0)
into	qt_peso_w
from	dual;	


ds_refer_med_ant_w := '';

open c01;
loop
fetch c01 into
	nr_seq_med_refer_w,
	qt_idade_minima_w,
	qt_idade_maxima_w,
	qt_peso_minimo_w,
	qt_peso_maximo_w,
	ds_medida_ref_w;
exit when c01%notfound;

	if	((qt_idade_w >= qt_idade_minima_w) and (qt_idade_w <= qt_idade_maxima_w)) and
		(((qt_peso_minimo_w is null) and (qt_peso_maximo_w is null)) or
		 ((qt_peso_w >= nvl(qt_peso_minimo_w,0)) and (qt_peso_w <= nvl(qt_peso_maximo_w,9999)))) then		 
		ds_refer_medida_w := ds_medida_ref_w;
	end if;
	

	open c02;
	loop
	fetch c02 into
		vl_minimo_adic_w,
		vl_maximo_adic_w,
		nr_seq_med_adic_w;
	exit when c02%notfound;

		select 	valor_medida_laudo(nr_seq_med_adic_w, nr_seq_laudo_w, 'V')
		into	vl_exame_adic_w
		from 	dual;

		if	((qt_idade_w >= qt_idade_minima_w) and (qt_idade_w <= qt_idade_maxima_w)) and
			(((qt_peso_minimo_w is null) and (qt_peso_maximo_w is null)) or
			 ((qt_peso_w >= nvl(qt_peso_minimo_w,0)) and (qt_peso_w <= nvl(qt_peso_maximo_w,9999)))) and
			((vl_exame_adic_w >= vl_minimo_adic_w) and (vl_exame_adic_w <= vl_maximo_adic_w)) then

			ds_refer_medida_w := ds_medida_ref_w;
		else
			ds_medida_ref_w := ds_refer_med_ant_w;
			ds_refer_medida_w := ds_medida_ref_w;
		end if;


		if	((qt_idade_w >= qt_idade_minima_w) and (qt_idade_w <= qt_idade_maxima_w)) and
			(((qt_peso_minimo_w is null) and (qt_peso_maximo_w is null)) or
			 ((qt_peso_w >= nvl(qt_peso_minimo_w,0)) and (qt_peso_w <= nvl(qt_peso_maximo_w,9999)))) then
			 
			ds_refer_med_ant_w := ds_refer_medida_w;
		end if;


	end loop;
	close c02;

	if	((qt_idade_w >= qt_idade_minima_w) and (qt_idade_w <= qt_idade_maxima_w)) and
		(((qt_peso_minimo_w is null) and (qt_peso_maximo_w is null)) or
		 ((qt_peso_w >= nvl(qt_peso_minimo_w,0)) and (qt_peso_w <= nvl(qt_peso_maximo_w,9999)))) then
		 
		ds_refer_med_ant_w := ds_refer_medida_w;
	end if;


end loop;
close c01;


return	ds_refer_medida_w;

end obter_medida_referencia_laudo;
/