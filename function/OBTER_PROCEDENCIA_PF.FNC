create or replace
function obter_procedencia_pf	(cd_pessoa_fisica_p	varchar2)
					return number is

nr_seq_proced_w	number(10,0);
cd_procedencia_w	number(5,0);

begin
if	(cd_pessoa_fisica_p is not null) then
	/* obter registro */
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_proced_w
	from	pf_procedencia
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

	/* obter valor registro */
	if	(nr_seq_proced_w > 0) then
		select	cd_procedencia
		into	cd_procedencia_w
		from	pf_procedencia
		where	nr_sequencia = nr_seq_proced_w;
	end if;
end if;

return cd_procedencia_w;

end obter_procedencia_pf;
/