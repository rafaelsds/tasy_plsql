create or replace
function Obter_se_pac_regra_evento(	nr_prescricao_p		number,
									nr_sequencia_p		number,
									ie_proc_mat_p		varchar2)
 		    	return char is

ie_regra_w		char(1) := 'N';
			
begin

if	(nr_prescricao_p is not null) then
	if 	(ie_proc_mat_p	= 'P') then
		select	nvl(max('S'),'N')
		into	ie_regra_w
		from	prescr_procedimento a
		where	rownum = 1
		and		a.nr_prescricao = nr_prescricao_p
		and		obter_se_proc_rec_alerta(a.nr_seq_proc_interno,nr_sequencia_p,a.cd_procedimento,a.ie_origem_proced, a.nr_seq_exame) = 'S';
	elsif	(ie_proc_mat_p	= 'M') then
		select	nvl(max('S'),'N')
		into	ie_regra_w
		from	prescr_material a
		where	rownum = 1
		and		nr_prescricao = nr_prescricao_p
		and		obter_se_mat_rec_alerta(a.cd_material,nr_sequencia_p) = 'S';
	elsif	(ie_proc_mat_p	= 'D') then
		select	nvl(max('S'),'N')
		into	ie_regra_w
		from	prescr_dieta a
		where	rownum = 1
		and		nr_prescricao = nr_prescricao_p
		and		obter_se_dieta_rec_alerta(a.cd_dieta,nr_sequencia_p) = 'S';
	elsif	(ie_proc_mat_p	= 'R') then
		select 	nvl(max('S'),'N')
		into 	ie_regra_w
		from 	prescr_recomendacao a
		where 	rownum = 1
		and 	nr_prescricao = nr_prescricao_p
		and 	obter_se_regra_rec_alerta(a.cd_recomendacao,nr_sequencia_p) = 'S';
	end if;
end if;

return	ie_regra_w;

end Obter_se_pac_regra_evento;
/