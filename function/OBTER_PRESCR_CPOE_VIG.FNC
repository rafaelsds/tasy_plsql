create or replace
function obter_prescr_cpoe_vig(	nr_sequencia_p		number,
								ie_tipo_item_p		varchar2,
								nr_atendimento_p	number default null,
								cd_pessoa_fisica_p	varchar2 default null,
								dt_filtro_p			date default null,
								qt_horas_adic_p		number default null)
				return varchar2 is

nr_prescricao_w		varchar2(2000);
nr_prescricao_ww     number(14);
nr_atendimento_w	number(10)	:= nr_atendimento_p;
cd_pessoa_fisica_w	varchar2(10)	:= cd_pessoa_fisica_p;
qt_horas_w    		number(14);


cursor c01 is
	select distinct a.nr_prescricao
		from	prescr_medica a,
				prescr_material b			
		where   ((a.nr_atendimento = nr_atendimento_w) or (a.cd_pessoa_fisica = cd_pessoa_fisica_w and nr_atendimento is null))		
		and     ((ie_tipo_item_p in ('M','SOL','MAT','IAG','IA') and a.nr_prescricao = b.nr_prescricao and	b.nr_seq_mat_cpoe = nr_sequencia_p))		
		and		exists (	select 	1 
							from 	prescr_mat_hor x 
							where 	b.nr_prescricao = x.nr_prescricao
							and		x.nr_seq_material = b.nr_sequencia
							and		x.dt_horario between dt_filtro_p and (dt_filtro_p + qt_horas_w/24));			
cursor c02 is
	select distinct a.nr_prescricao
		from	prescr_medica a,
				prescr_material b			
		where   ((a.nr_atendimento = nr_atendimento_w) or (a.cd_pessoa_fisica = cd_pessoa_fisica_w and nr_atendimento is null))		
		and     (ie_tipo_item_p in ('SNE','S','LD') and a.nr_prescricao = b.nr_prescricao and b.nr_seq_dieta_cpoe = nr_sequencia_p)		
		and		exists (	select 	1 
							from 	prescr_mat_hor x 
							where 	b.nr_prescricao = x.nr_prescricao
							and		x.nr_seq_material = b.nr_sequencia
							and		x.dt_horario between dt_filtro_p and (dt_filtro_p + qt_horas_w/24));
		
cursor c03 is
	select distinct a.nr_prescricao
		from	prescr_medica a,			
				prescr_dieta c
		where  ((a.nr_atendimento = nr_atendimento_w) or (a.cd_pessoa_fisica = cd_pessoa_fisica_w and nr_atendimento is null))		
		and     ((ie_tipo_item_p = 'D' and a.nr_prescricao = c.nr_prescricao and c.nr_seq_dieta_cpoe = nr_sequencia_p))		
		and		exists (	select 	1 
							from 	prescr_dieta_hor x 
							where 	c.nr_prescricao = x.nr_prescricao
							and		x.nr_seq_dieta = c.nr_sequencia
							and		x.dt_horario between dt_filtro_p and (dt_filtro_p + qt_horas_w/24));
		
cursor c04 is
	select distinct a.nr_prescricao
		from	prescr_medica a,				
				rep_jejum d
		where   ((a.nr_atendimento = nr_atendimento_w) or (a.cd_pessoa_fisica = cd_pessoa_fisica_w and nr_atendimento is null))
		and     (ie_tipo_item_p = 'J' and a.nr_prescricao = d.nr_prescricao and d.nr_seq_dieta_cpoe = nr_sequencia_p)		
		and 	((dt_inicio between dt_filtro_p and (dt_filtro_p + qt_horas_w/24)) or (dt_inicio between dt_filtro_p   and (dt_filtro_p + qt_horas_w/24)));
		
	
cursor c05 is
	select distinct a.nr_prescricao
		from	prescr_medica a,			
				prescr_procedimento e
		where   ((a.nr_atendimento = nr_atendimento_w) or (a.cd_pessoa_fisica = cd_pessoa_fisica_w and nr_atendimento is null))
		and     (ie_tipo_item_p in ('P','HM','AP') and a.nr_prescricao = e.nr_prescricao and e.nr_seq_proc_cpoe = nr_sequencia_p)		
		and		exists (	select 	1 
							from 	prescr_proc_hor x 
							where 	e.nr_prescricao = x.nr_prescricao
							and		x.nr_seq_procedimento = e.nr_sequencia
							and		x.dt_horario between dt_filtro_p and (dt_filtro_p + qt_horas_w/24));
		
cursor c06 is
	select distinct a.nr_prescricao
		from	prescr_medica a,				
				prescr_gasoterapia f
		where   ((a.nr_atendimento = nr_atendimento_w) or (a.cd_pessoa_fisica = cd_pessoa_fisica_w and nr_atendimento is null))
		and     (ie_tipo_item_p = 'O' and a.nr_prescricao = f.nr_prescricao and f.nr_seq_gas_cpoe = nr_sequencia_p)		
		and		exists (	select 	1 
							from 	prescr_gasoterapia_hor x 
							where 	f.nr_prescricao = x.nr_prescricao
							and		x.nr_seq_gasoterapia = f.nr_sequencia
							and		x.dt_horario between dt_filtro_p and (dt_filtro_p + qt_horas_w/24));
		
cursor c07 is
	select distinct a.nr_prescricao
		from	prescr_medica a,			
				prescr_recomendacao g
		where   ((a.nr_atendimento = nr_atendimento_w) or (a.cd_pessoa_fisica = cd_pessoa_fisica_w and nr_atendimento is null))
		and     (ie_tipo_item_p = 'R' and a.nr_prescricao = g.nr_prescricao and g.nr_seq_rec_cpoe = nr_sequencia_p)		
		and		exists (	select 	1 
							from 	prescr_rec_hor x 
							where 	g.nr_prescricao = x.nr_prescricao
							and		x.nr_seq_recomendacao = g.nr_sequencia
							and		x.dt_horario between dt_filtro_p and (dt_filtro_p + qt_horas_w/24));	

		
cursor c08 is
	select distinct a.nr_prescricao
		from	prescr_medica a,				
				prescr_solucao h
		where   ((a.nr_atendimento = nr_atendimento_w) or (a.cd_pessoa_fisica = cd_pessoa_fisica_w and nr_atendimento is null))
		and     (ie_tipo_item_p = 'DI' and a.nr_prescricao = h.nr_prescricao and h.nr_seq_dialise_cpoe = nr_sequencia_p)	
		and		exists (	select 	1 
							from 	prescr_mat_hor x 
							where 	x.nr_prescricao = h.nr_prescricao
							and 	x.nr_seq_solucao = h.nr_seq_solucao
							and		x.dt_horario between dt_filtro_p and (dt_filtro_p + qt_horas_w/24));	

begin

if	(nr_atendimento_w	is null) and
	(cd_pessoa_fisica_w is null) then

	if	(ie_tipo_item_p in ('D','SNE','S','J','LD')) then
		select	max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	cpoe_dieta
		where	nr_sequencia	= nr_sequencia_p;
	elsif	(ie_tipo_item_p in ('M','SOL','MAT')) then
		select	max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	cpoe_material
		where	nr_sequencia	= nr_sequencia_p;
	elsif	(ie_tipo_item_p in ('P')) then	
		select	max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	cpoe_procedimento
		where	nr_sequencia	= nr_sequencia_p;
	elsif	(ie_tipo_item_p in ('AP')) then	
		select	max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	cpoe_anatomia_patologica
		where	nr_sequencia	= nr_sequencia_p;	
	elsif	(ie_tipo_item_p in ('O')) then	
		select	max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	cpoe_gasoterapia
		where	nr_sequencia	= nr_sequencia_p;	
	elsif	(ie_tipo_item_p in ('R')) then	
		select	max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	cpoe_recomendacao
		where	nr_sequencia	= nr_sequencia_p;
	elsif	(ie_tipo_item_p in ('HM')) then		
		select	max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	cpoe_hemoterapia
		where	nr_sequencia	= nr_sequencia_p;	
	elsif	(ie_tipo_item_p in ('DI')) then
		select	max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	cpoe_dialise
		where	nr_sequencia	= nr_sequencia_p;	
	end if;


end if;

qt_horas_w	:=	nvl(qt_horas_adic_p,24);

if	(ie_tipo_item_p	= 'D') then
	nr_prescricao_w := '';
	open c03;
	loop
	fetch c03 into nr_prescricao_ww;
	exit when c03%notfound;
		begin
		if	(nr_prescricao_w is null) then
			nr_prescricao_w:= nr_prescricao_ww ||',';
		else
			nr_prescricao_w:= nr_prescricao_w || nr_prescricao_ww||',';
		end if;
	end;
	end loop;
	close c03;

	
elsif	(ie_tipo_item_p	in ('SNE','S','LD')) then
	nr_prescricao_w := '';
	open c02;
	loop
	fetch c02 into nr_prescricao_ww;
	exit when c02%notfound;
		begin
		if	(nr_prescricao_w is null) then
			nr_prescricao_w:= nr_prescricao_ww ||',';
		else
			nr_prescricao_w:= nr_prescricao_w || nr_prescricao_ww||',';
		end if;
	end;
	end loop;
	close c02;
	
elsif	(ie_tipo_item_p	= 'J') then
	nr_prescricao_w := '';
	open c04;
	loop
	fetch c04 into nr_prescricao_ww;
	exit when c04%notfound;
		begin
		if	(nr_prescricao_w is null) then
			nr_prescricao_w:= nr_prescricao_ww ||',';
		else
			nr_prescricao_w:= nr_prescricao_w || nr_prescricao_ww||',';
		end if;
	end;
	end loop;
	close c04;	
	
elsif	(ie_tipo_item_p	in ('M','SOL','MAT','IAG','IA')) then
	nr_prescricao_w := '';
	open c01;
	loop
	fetch c01 into nr_prescricao_ww;
	exit when c01%notfound;
		begin
		if	(nr_prescricao_w is null) then
			nr_prescricao_w:= nr_prescricao_ww ||',';
		else
			nr_prescricao_w:= nr_prescricao_w || nr_prescricao_ww||',';
		end if;
	end;
	end loop;
	close c01;
	
elsif	(ie_tipo_item_p	in ('P','HM','AP')) then
	nr_prescricao_w := '';
	open c05;
	loop
	fetch c05 into nr_prescricao_ww;
	exit when c05%notfound;
		begin
		if	(nr_prescricao_w is null) then
			nr_prescricao_w:= nr_prescricao_ww ||',';
		else
			nr_prescricao_w:= nr_prescricao_w || nr_prescricao_ww||',';
		end if;
	end;
	end loop;
	close c05;
	
elsif	(ie_tipo_item_p	= 'O') then	
	nr_prescricao_w := '';
	open c06;
	loop
	fetch c06 into nr_prescricao_ww;
	exit when c06%notfound;
		begin
		if	(nr_prescricao_w is null) then
			nr_prescricao_w:= nr_prescricao_ww ||',';
		else
			nr_prescricao_w:= nr_prescricao_w || nr_prescricao_ww||',';
		end if;
	end;
	end loop;
	close c06;
	
elsif	(ie_tipo_item_p	= 'R') then
	nr_prescricao_w := '';
	open c07;
	loop
	fetch c07 into nr_prescricao_ww;
	exit when c07%notfound;
		begin
		if	(nr_prescricao_w is null) then
			nr_prescricao_w:= nr_prescricao_ww ||',';
		else
			nr_prescricao_w:= nr_prescricao_w || nr_prescricao_ww||',';
		end if;
	end;
	end loop;
	close c07;	
	
elsif	(ie_tipo_item_p	= 'DI') then
	nr_prescricao_w := '';
	open c08;
	loop
	fetch c08 into nr_prescricao_ww;
	exit when c08%notfound;
		begin
		if	(nr_prescricao_w is null) then
			nr_prescricao_w:= nr_prescricao_ww ||',';
		else
			nr_prescricao_w:= nr_prescricao_w || nr_prescricao_ww||',';
		end if;
	end;
	end loop;
	close c08;


end if;

if (nr_prescricao_w is null) then
	nr_prescricao_w := '0';
end if;

return	nr_prescricao_w;

end obter_prescr_cpoe_vig;
/
