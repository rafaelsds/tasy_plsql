CREATE OR REPLACE
FUNCTION  Obter_Saldo_Tit_Receber_port(	
					nr_titulo_p		number,
				    	dt_referencia_p		Date)
					RETURN Number IS

/* Function criada especificamente para a OS 102557 */

dt_referencia_w			Date;
dt_contabil_w			Date;
ie_situacao_w			varchar2(1);
vl_titulo_w			Number(15,2);
vl_saldo_Data_w			Number(15,2);
vl_recebido_w			Number(15,2);
vl_alteracao_w			Number(15,2);
vl_desdobrado_w			Number(15,2);
vl_tributo_w			Number(15,2);
dt_liquidacao_w			date;

BEGIN

dt_referencia_w			:= trunc(dt_referencia_p, 'dd');

begin
select	vl_titulo,
	dt_contabil,
	ie_situacao,
	dt_liquidacao
into	vl_titulo_w,
	dt_contabil_w,
	ie_situacao_w,
	dt_liquidacao_w
from	Titulo_receber
where	nr_titulo		= nr_titulo_p;
exception
when others then
	vl_titulo_w		:= 0;
end;

select	nvl(sum(decode(ie_aumenta_diminui, 'A', VL_ALTERACAO, VL_ALTERACAO * -1)),0)
into	vl_alteracao_w
from	ALTERACAO_VALOR
where	nr_titulo	= nr_titulo_p
and	DT_ALTERACAO	< dt_referencia_w;

select	nvl(sum(VL_TRIBUTO),0)
into	vl_tributo_w
from	titulo_receber_trib
where	nr_titulo		= nr_titulo_p
and	nvl(ie_origem_tributo, 'C') = 'D';

if 	(ie_situacao_w in ('3','5')) and 
	(dt_liquidacao_w <= dt_referencia_w) then
	vl_saldo_data_w	:= 0;
else
	select nvl(sum(vl_recebido + vl_glosa + vl_descontos + nvl(vl_perdas,0)),0)
	into	vl_recebido_w
	from 	titulo_receber_liq
	where	nr_titulo			= nr_titulo_p
	and	nvl(ie_lib_caixa, 'S')		= 'S'
	and	dt_recebimento	< dt_referencia_w;

	if	(nvl(dt_contabil_w,dt_referencia_w) > dt_referencia_w) then
		vl_saldo_data_w	:= 0;
	else 
		vl_saldo_data_w	:= vl_titulo_w - vl_recebido_w;
	end if;

	vl_Saldo_Data_w	:= vl_Saldo_Data_w + vl_alteracao_w - vl_tributo_w;

end if;


RETURN nvl(vl_Saldo_Data_w,0);

END  Obter_Saldo_Tit_Receber_port;
/