CREATE OR REPLACE
FUNCTION OBTER_ESTAB_ATEND(
					NR_ATENDIMENTO_P number)
					RETURN NUMBER IS

cd_retorno_w		Number(4,0) := null;

BEGIN
if	(nr_atendimento_p	is not null) and
	(nr_atendimento_p  > 0) then
	begin
	select	cd_estabelecimento
	into	cd_retorno_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	end;
end if;

RETURN cd_retorno_w;
END OBTER_ESTAB_ATEND;
/