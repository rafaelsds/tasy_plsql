create or replace
function pls_validar_padrao
		(	ds_string_p		Varchar2,
			ds_padrao_p		Varchar2,
			ds_case_sensitive_p	varchar2 default 'N')
 		    	return varchar2 is

--	ds_padrao_p			
--	A ou a		Somente caracter alfa (Sem ser num�rico)
--	0(zero)		Somente caracter num�rico
--	#_ 		Alfanum�rico

--	#1 - #9		Quantidade de vez que o caracter anterior deve aparecer
--	#N		Quantidade de vez que o caracter anterior deve aparecer � igual a 1 at� infinito

--	#0		Caracter 0
--	#A ou #a		Caracter a

--	Demais caracteres ser�o reconhecidos como caracteres obrigat�rios

/*	Exemplos v�lidos:
		V�lidar padr�o data:	ds_padrao_p: '00/00/0000'ou '0#2/0#2/0#4'
		V�lidar padr�o hora:	ds_padrao_p: '00:00:00'ou '0#2/0#2/0#2'
		
		Validar padr�o de 5 caracters num�ricos terminando em z : 		ds_padrao_p: 00000z ou 0#5z
		Validar padr�o de 15 caracters num�ricos terminando em z : 		ds_padrao_p: 000000000000000z ou 0#90#6z
		
		Validar padr�o de 5 caracters num�ricos terminando em Z maisculo : 	ds_padrao_p: 00000Z ou 0#5Z
								ds_case_sensitive_p = 'S'
								
		Validar se a string possue s� Caracteres sem num�ricos : 		ds_padrao_p: A#N 
		(Sem limite de quantidade)
		
		Validar se a string possue s� num�ricos sem  Caracteres: 		ds_padrao_p: 0#N 
		(Sem limite de quantidade)
		
		Validar se a string inicia com z e depois possue s� 
		num�ricos sem  Caracteres: 				ds_padrao_p: z0#N 
		(Sem limite de quantidade)
		
*/
			
ie_retorno_w		Varchar2(1) := 'N';
ds_caracter_w		Varchar2(1);
ds_regex_w		Varchar2(255);
i			integer;
	
/*

*/
	
begin

i := 0;
while (i <= length(ds_padrao_p)) loop 
	begin
	i := i + 1;
	
	ds_caracter_w := substr(ds_padrao_p,i,1);
	
	if	(upper(ds_caracter_w) = 'A') then
		ds_regex_w := ds_regex_w || '\D';
	elsif	(ds_caracter_w = '0') then
		ds_regex_w := ds_regex_w || '\d';	
	elsif	(ds_caracter_w = '#') then
	
		i := i + 1;
		ds_caracter_w := substr(ds_padrao_p,i,1);
		
		if	(ds_caracter_w = '_') then
			ds_regex_w := ds_regex_w || '.';
		elsif	(ds_caracter_w in ('1', '2', '3', '4', '5', '6', '7', '8', '9')) then
			ds_regex_w := ds_regex_w || '{'||ds_caracter_w||'}';			
		elsif	(ds_caracter_w = '0') then
			ds_regex_w := ds_regex_w || '0';
		elsif	(ds_caracter_w = 'A') then
			ds_regex_w := ds_regex_w || 'A';
		elsif	(ds_caracter_w = 'a') then
			ds_regex_w := ds_regex_w || 'a';
		elsif	(upper(ds_caracter_w) = 'N') then
			ds_regex_w := ds_regex_w || '+';
		end if;
		
	else
		ds_regex_w := ds_regex_w || ds_caracter_w;
	end if;
	
	
	end;
end loop;

if	(ds_case_sensitive_p = 'S') then
	if	(OWA_PATTERN.MATCH(ds_string_p, '^'||ds_regex_w||'$', 'g')) then
		ie_retorno_w := 'S';
	end if;
else
	if	(OWA_PATTERN.MATCH(ds_string_p, '^'||ds_regex_w||'$', 'i')) then
		ie_retorno_w := 'S';
	end if;
end if;

return	ie_retorno_w;

end pls_validar_padrao;
/