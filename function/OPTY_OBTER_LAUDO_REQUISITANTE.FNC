create or replace 
function OPTY_obter_laudo_requisitante(	nr_seq_proc_p number,
					ie_tipo_estrutura_p number)
					return number is
 
qtd_w		number(10);
resultado_w	number(10) := 1;

begin

if	(nr_seq_proc_p is not null) then
	begin
	
	select	count(p.nr_sequencia)
	into	qtd_w
	from	procedimento_paciente p,
		laudo_paciente l
	where	p.nr_sequencia = l.nr_seq_proc
	and	p.cd_medico_req = l.cd_medico_resp
	and	p.nr_sequencia = nr_seq_proc_p;

	if 	(qtd_w > 0) then
		resultado_w := 0;
	end if;
	
	end;
end if;
 
return resultado_w;

end OPTY_obter_laudo_requisitante;
/