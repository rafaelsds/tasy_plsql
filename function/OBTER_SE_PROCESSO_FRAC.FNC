create or replace
function obter_se_processo_frac (nr_seq_processo_p	varchar2)
				return varchar2 is
				
ie_frac_w	varchar2(1) := 'N';
begin
if	(nr_seq_processo_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ie_frac_w
	from	adep_processo_frac
	where	nr_seq_processo = nr_seq_processo_p;

end if;

return ie_frac_w;

end obter_se_processo_frac;
/