create or replace
function obter_pontuacao_ntiss	(nr_atendimento_p		number)
 		    	return number is

qt_pontuacao_w		number(20);			
begin

select	max(a.qt_pto_total)
into	qt_pontuacao_w
from	escala_ntiss a
where	a.nr_sequencia = ( select max(b.nr_sequencia)
			   from	  escala_ntiss b
			   where  b.nr_atendimento = nr_atendimento_p);

return	qt_pontuacao_w;

end obter_pontuacao_ntiss;
/