create or replace
function paf_requisito_vii_item_7_mat(cd_material_p	number,
			cd_estabelecimento_p	number,
			cd_barra_material_p 	varchar)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(2000);			
cd_cgc_w	varchar2(20);

begin

select 	cd_cgc
into	cd_cgc_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

select	'P2' ||
	substr(lpad(cd_cgc_w,14,'0'),1,14) ||
	substr(lpad(nvl(c.cd_barra_material,a.cd_material),14,'0'),1,14) ||
	substr(rpad(a.ds_material,50,' '),1,50) ||
	substr(rpad(nvl(b.cd_unidade_venda,' '),06,' '),1,6) ||
	'T' ||
	decode(b.ie_tipo_imposto,'I','T','S') ||
	nvl(b.ie_situacao_tributaria,'N') ||
	lpad(nvl((	select	pr_aliquota
		from	paf_aliquotas
		where	nr_sequencia = b.nr_icms
		and	b.ie_tipo_imposto = 'I'
		union all
		select	pr_aliquota
		from	paf_aliquotas
		where	nr_sequencia = b.nr_iss
		and	b.ie_tipo_imposto = 'S'),0),4,'0') ||
	lpad(replace(campo_mascara(nvl(paf_Campo_Mascara_virgula(round(far_obter_preco_mat2(cd_material_p,cd_estabelecimento_p, 0),2)),0),2),'.',''),12,0)
into	ds_retorno_w	
from	material a,
	material_estab b,
	material_cod_barra c
where	a.cd_material = b.cd_material
and	a.cd_material = cd_material_p
and	a.cd_material = c.cd_material(+)
and	nvl(c.cd_barra_material,'0') = nvl(cd_barra_material_p,'0')
and	b.cd_estabelecimento = cd_estabelecimento_p;

return	ds_retorno_w;

end paf_requisito_vii_item_7_mat;
/