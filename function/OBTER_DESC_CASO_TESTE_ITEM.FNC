create or replace
function obter_desc_caso_teste_item(
			nr_seq_item_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);

begin

if	(nvl(nr_seq_item_p,0) > 0) then
	select	a.ds_caso_teste
	into	ds_retorno_w
	from	teste_caso_teste_item b,
		teste_caso_teste a
	where	a.nr_sequencia = b.nr_seq_caso_teste
	and	b.nr_sequencia = nr_seq_item_p;
end if;

return	ds_retorno_w;

end obter_desc_caso_teste_item;
/