create or replace
function cpoe_obter_se_mat_plano_conv(
							nr_atendimento_p			number,
							cd_material_p				number,
							cd_setor_atendimento_p		number,
							cd_convenio_p				number,
							cd_estabelecimento_p		number) return varchar2 is 

cd_plano_convenio_w			varchar2(10);							
qt_material_w				number(18,6);	
ie_retorno_w				varchar2(15) := 'S';
ie_consiste_glosa_conv_w	varchar2(15);
ie_erro_autorizacao_w		varchar2(255);
ie_bloqueia_agenda_w		varchar2(1);
ie_regra_plano_w			number(2,0);
nr_seq_regra_plano_w		number(10,0);
begin

select	nvl(ie_consiste_glosa_conv,'N')
into	ie_consiste_glosa_conv_w
from	parametro_medico
where	nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p;

if (ie_consiste_glosa_conv_w <> 'N') then

	qt_material_w := 1;
	cd_plano_convenio_w := substr(obter_dados_categ_conv(nr_atendimento_p,'P'),1,10);
	
	consiste_mat_plano_convenio(
						coalesce(cd_convenio_p,0),
						coalesce(cd_plano_convenio_w,'0'),
						cd_material_p,
						nr_atendimento_p,
						cd_setor_atendimento_p,
						ie_erro_autorizacao_w,
						ie_bloqueia_agenda_w,
						ie_regra_plano_w,
						nr_seq_regra_plano_w,
						qt_material_w,
						sysdate,
						null,
						cd_estabelecimento_p,
						null,
						null);		

		if	((ie_erro_autorizacao_w is not null) or
			 (ie_regra_plano_w = 7)) then
			if	(ie_regra_plano_w in (1,2,7)) then
					ie_retorno_w	:= ie_consiste_glosa_conv_w;
			end if;			
		end if;
			
end if;

return nvl(ie_retorno_w,'S');

end cpoe_obter_se_mat_plano_conv;
/