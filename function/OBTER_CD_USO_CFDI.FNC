create or replace
function obter_cd_uso_cfdi
			( nr_sequencia_p		number )
 		    	return varchar2 is

retorno_w		varchar2(255);

begin
select  nvl(b.cd_uso_cfdi,'P01') cd_uso_cfdi
INTO retorno_w
from nota_fiscal a,
     fis_uso_cfdi b 
where a.nr_sequencia = nr_sequencia_p 
	and     a.nr_sequencia = b.nr_seq_nota(+);

return	retorno_w;

end obter_cd_uso_cfdi;
/
