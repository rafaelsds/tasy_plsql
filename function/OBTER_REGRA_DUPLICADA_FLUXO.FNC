Create or Replace
FUNCTION OBTER_REGRA_DUPLICADA_FLUXO(	dt_inicial_p	date,
					dt_final_p	date)
					RETURN		VarChar2 IS

ds_retorno_w		varchar2(4000);
nr_seq_regra_w		conta_financ_regra.nr_sequencia%type;
nr_seq_regra_igual_w	conta_financ_regra.nr_sequencia%type;
ds_conta_financ_w	conta_financeira.ds_conta_financ%type;
ds_conta_financ_igual_w	conta_financeira.ds_conta_financ%type;

cursor	c01 is
SELECT	a.nr_sequencia,
	x.ds_conta_financ
FROM	conta_financeira x,
	CONTA_FINANC_REGRA a
WHERE	a.dt_atualizacao	between nvl(dt_inicial_p,a.dt_atualizacao) and fim_dia(nvl(dt_final_p,sysdate))
and	a.nr_seq_conta_financ	= x.cd_conta_financ
and	EXISTS
	(SELECT	1
	FROM	CONTA_FINANC_REGRA b,
		conta_financeira y
	WHERE	b.nr_seq_conta_financ		= y.cd_conta_financ
	and	a.NR_SEQUENCIA			<> b.nr_Sequencia
	and	nvl(a.IE_ENTRADA_SAIDA,'0')	= nvl(b.IE_ENTRADA_SAIDA,'0')
	and	nvl(a.CD_GRUPO_MATERIAL,0)	= nvl(b.CD_GRUPO_MATERIAL,0)
	and	nvl(a.CD_SUBGRUPO_MATERIAL,0)	= nvl(b.CD_SUBGRUPO_MATERIAL,0)
	and	nvl(a.CD_CLASSE_MATERIAL,0)	= nvl(b.CD_CLASSE_MATERIAL,0)
  and	nvl(a.NR_SEQ_FAMILIA,0)	= nvl(b.NR_SEQ_FAMILIA,0)
	and	nvl(a.CD_AREA_PROCEDIMENTO,0)	= nvl(b.CD_AREA_PROCEDIMENTO,0)
	and	nvl(a.CD_ESPECIALIDADE,0)	= nvl(b.CD_ESPECIALIDADE,0)
	and	nvl(a.CD_GRUPO_PROC,0)		= nvl(b.CD_GRUPO_PROC,0)
	and	nvl(a.CD_CONVENIO,0)		= nvl(b.CD_CONVENIO,0)
	and	nvl(a.CD_SETOR_ATENDIMENTO,0)	= nvl(b.CD_SETOR_ATENDIMENTO,0)
	and	nvl(a.CD_TIPO_PESSOA,0)		= nvl(b.CD_TIPO_PESSOA,0)
	and	nvl(a.CD_CGC,'0')		= nvl(b.CD_CGC,'0')
	and	nvl(a.CD_MATERIAL,0)		= nvl(b.CD_MATERIAL,0)
	and	nvl(a.CD_PROCEDIMENTO,0)	= nvl(b.CD_PROCEDIMENTO,0)
	and	nvl(a.IE_ORIGEM_PROCED,0)	= nvl(b.IE_ORIGEM_PROCED,0)
	and	nvl(a.CD_CENTRO_CUSTO,0)	= nvl(b.CD_CENTRO_CUSTO,0)
	and	nvl(a.IE_CLINICA,0)		= nvl(b.IE_CLINICA,0)
	and	nvl(a.CD_OPERACAO_NF,0)		= nvl(b.CD_OPERACAO_NF,0)
	and	nvl(a.IE_FATURAMENTO,'0')	= nvl(b.IE_FATURAMENTO,'0')
	and	nvl(a.IE_TIPO_ATENDIMENTO,0)	= nvl(b.IE_TIPO_ATENDIMENTO,0)
	and	nvl(a.CD_CATEGORIA,'0')		= nvl(b.CD_CATEGORIA,'0')
	and	nvl(a.NR_SEQ_PROJ_REC,0)	= nvl(b.NR_SEQ_PROJ_REC,0)
	and	nvl(a.IE_TIPO_CONVENIO,0)	= nvl(b.IE_TIPO_CONVENIO,0)
	and	nvl(a.NR_SEQ_PRODUTO,0)		= nvl(b.NR_SEQ_PRODUTO,0)
	and	nvl(a.IE_NOTA_CREDITO,'0')	= nvl(b.IE_NOTA_CREDITO,'0')
	and	nvl(a.CD_PESSOA_FISICA,'0')	= nvl(b.CD_PESSOA_FISICA,'0')
	and	nvl(a.DS_OBSERVACAO,'0')	= nvl(b.DS_OBSERVACAO,'0')
	and	nvl(a.NR_SEQ_GRUPO,0)		= nvl(b.NR_SEQ_GRUPO,0)
	and	nvl(a.NR_SEQ_SUBGRUPO,0)	= nvl(b.NR_SEQ_SUBGRUPO,0)
	and	nvl(a.NR_SEQ_FORMA_ORG,0)	= nvl(b.NR_SEQ_FORMA_ORG,0)
	and	nvl(a.IE_TIPO_FINANCIAMENTO,'0')= nvl(b.IE_TIPO_FINANCIAMENTO,'0')
	and	nvl(a.IE_COMPLEXIDADE,'0')	= nvl(b.IE_COMPLEXIDADE,'0')
	and	nvl(a.IE_ORIGEM_TIT_REC,'0')	= nvl(b.IE_ORIGEM_TIT_REC,'0')
	and	nvl(a.IE_ORIGEM_TIT_PAGAR,'0')	= nvl(b.IE_ORIGEM_TIT_PAGAR,'0')
	and	nvl(a.NR_SEQ_CLASSE_TP,0)	= nvl(b.NR_SEQ_CLASSE_TP,0)
	and	nvl(a.NR_SEQ_CLASSE_TR,0)	= nvl(b.NR_SEQ_CLASSE_TR,0)
	and	nvl(a.CD_LOCAL_ESTOQUE,0)	= nvl(b.CD_LOCAL_ESTOQUE,0)
	and	nvl(a.NR_SEQ_TRANS_FIN,0)	= nvl(b.NR_SEQ_TRANS_FIN,0)
	and	nvl(a.CD_CONTA_CONTABIL,'0')	= nvl(b.CD_CONTA_CONTABIL,'0')
	and	nvl(a.CD_ESTABELECIMENTO,0)	= nvl(b.CD_ESTABELECIMENTO,0)
	and	nvl(x.CD_EMPRESA,0)		= nvl(y.CD_EMPRESA,0));

BEGIN

open	c01;
loop
fetch	c01 into
	nr_seq_regra_w,
	ds_conta_financ_w;
exit	when c01%notfound;

	select	max(a.nr_sequencia),
		max(x.ds_conta_financ)
	into	nr_seq_regra_igual_w,
		ds_conta_financ_igual_w
	from	conta_financeira x,
		CONTA_FINANC_REGRA a
	WHERE	a.nr_seq_conta_financ	= x.cd_conta_financ
	and	EXISTS
		(SELECT	1
		FROM	CONTA_FINANC_REGRA b,
			conta_financeira y
		WHERE	b.nr_seq_conta_financ		= y.cd_conta_financ
		and	a.NR_SEQUENCIA			<> b.nr_Sequencia
		and	nvl(a.IE_ENTRADA_SAIDA,'0')	= nvl(b.IE_ENTRADA_SAIDA,'0')
		and	nvl(a.CD_GRUPO_MATERIAL,0)	= nvl(b.CD_GRUPO_MATERIAL,0)
		and	nvl(a.CD_SUBGRUPO_MATERIAL,0)	= nvl(b.CD_SUBGRUPO_MATERIAL,0)
		and	nvl(a.CD_CLASSE_MATERIAL,0)	= nvl(b.CD_CLASSE_MATERIAL,0)
		and	nvl(a.NR_SEQ_FAMILIA,0)	= nvl(b.NR_SEQ_FAMILIA,0)
		and	nvl(a.CD_AREA_PROCEDIMENTO,0)	= nvl(b.CD_AREA_PROCEDIMENTO,0)
		and	nvl(a.CD_ESPECIALIDADE,0)	= nvl(b.CD_ESPECIALIDADE,0)
		and	nvl(a.CD_GRUPO_PROC,0)		= nvl(b.CD_GRUPO_PROC,0)
		and	nvl(a.CD_CONVENIO,0)		= nvl(b.CD_CONVENIO,0)
		and	nvl(a.CD_SETOR_ATENDIMENTO,0)	= nvl(b.CD_SETOR_ATENDIMENTO,0)
		and	nvl(a.CD_TIPO_PESSOA,0)		= nvl(b.CD_TIPO_PESSOA,0)
		and	nvl(a.CD_CGC,'0')		= nvl(b.CD_CGC,'0')
		and	nvl(a.CD_MATERIAL,0)		= nvl(b.CD_MATERIAL,0)
		and	nvl(a.CD_PROCEDIMENTO,0)	= nvl(b.CD_PROCEDIMENTO,0)
		and	nvl(a.IE_ORIGEM_PROCED,0)	= nvl(b.IE_ORIGEM_PROCED,0)
		and	nvl(a.CD_CENTRO_CUSTO,0)	= nvl(b.CD_CENTRO_CUSTO,0)
		and	nvl(a.IE_CLINICA,0)		= nvl(b.IE_CLINICA,0)
		and	nvl(a.CD_OPERACAO_NF,0)		= nvl(b.CD_OPERACAO_NF,0)
		and	nvl(a.IE_FATURAMENTO,'0')	= nvl(b.IE_FATURAMENTO,'0')
		and	nvl(a.IE_TIPO_ATENDIMENTO,0)	= nvl(b.IE_TIPO_ATENDIMENTO,0)
		and	nvl(a.CD_CATEGORIA,'0')		= nvl(b.CD_CATEGORIA,'0')
		and	nvl(a.NR_SEQ_PROJ_REC,0)	= nvl(b.NR_SEQ_PROJ_REC,0)
		and	nvl(a.IE_TIPO_CONVENIO,0)	= nvl(b.IE_TIPO_CONVENIO,0)
		and	nvl(a.NR_SEQ_PRODUTO,0)		= nvl(b.NR_SEQ_PRODUTO,0)
		and	nvl(a.IE_NOTA_CREDITO,'0')	= nvl(b.IE_NOTA_CREDITO,'0')
		and	nvl(a.CD_PESSOA_FISICA,'0')	= nvl(b.CD_PESSOA_FISICA,'0')
		and	nvl(a.DS_OBSERVACAO,'0')	= nvl(b.DS_OBSERVACAO,'0')
		and	nvl(a.NR_SEQ_GRUPO,0)		= nvl(b.NR_SEQ_GRUPO,0)
		and	nvl(a.NR_SEQ_SUBGRUPO,0)	= nvl(b.NR_SEQ_SUBGRUPO,0)
		and	nvl(a.NR_SEQ_FORMA_ORG,0)	= nvl(b.NR_SEQ_FORMA_ORG,0)
		and	nvl(a.IE_TIPO_FINANCIAMENTO,'0')= nvl(b.IE_TIPO_FINANCIAMENTO,'0')
		and	nvl(a.IE_COMPLEXIDADE,'0')	= nvl(b.IE_COMPLEXIDADE,'0')
		and	nvl(a.IE_ORIGEM_TIT_REC,'0')	= nvl(b.IE_ORIGEM_TIT_REC,'0')
		and	nvl(a.IE_ORIGEM_TIT_PAGAR,'0')	= nvl(b.IE_ORIGEM_TIT_PAGAR,'0')
		and	nvl(a.NR_SEQ_CLASSE_TP,0)	= nvl(b.NR_SEQ_CLASSE_TP,0)
		and	nvl(a.NR_SEQ_CLASSE_TR,0)	= nvl(b.NR_SEQ_CLASSE_TR,0)
		and	nvl(a.CD_LOCAL_ESTOQUE,0)	= nvl(b.CD_LOCAL_ESTOQUE,0)
		and	nvl(a.NR_SEQ_TRANS_FIN,0)	= nvl(b.NR_SEQ_TRANS_FIN,0)
		and	nvl(a.CD_CONTA_CONTABIL,'0')	= nvl(b.CD_CONTA_CONTABIL,'0')
		and	nvl(a.CD_ESTABELECIMENTO,0)	= nvl(b.CD_ESTABELECIMENTO,0)
		and	nvl(x.CD_EMPRESA,0)		= nvl(y.CD_EMPRESA,0)
		and	b.nr_sequencia			= nr_seq_regra_w);

	if	(ds_retorno_w	is null) or
		(length(ds_retorno_w) < 3700) then

		ds_retorno_w	:=	ds_retorno_w ||
					chr(13) || chr(10) || chr(13) || chr(10) ||
					/*'Conta financeira: ' || ds_conta_financ_w || '. Regra: ' || nr_seq_regra_w || chr(13) || chr(10) ||
					'Conta financeira: ' || ds_conta_financ_igual_w || '. Regra: '|| nr_seq_regra_igual_w;*/
					wheb_mensagem_pck.get_texto(305547,'DS_CONTA_FINANC_W='||ds_conta_financ_w||';NR_SEQ_REGRA_W='||nr_seq_regra_w) ||chr(13)||chr(10)||
					wheb_mensagem_pck.get_texto(305547,'DS_CONTA_FINANC_W='||ds_conta_financ_igual_w||';NR_SEQ_REGRA_W='||nr_seq_regra_igual_w);

	end if;

end	loop;
close	c01;

if	(ds_retorno_w	is not null) then
	--'Foram identificadas regras duplicadas na pasta Conta Financeira/Conta Financeira!'
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(305548) || chr(13) || chr(10) || ds_retorno_w;

end if;

RETURN ds_retorno_w;

END OBTER_REGRA_DUPLICADA_FLUXO;
/