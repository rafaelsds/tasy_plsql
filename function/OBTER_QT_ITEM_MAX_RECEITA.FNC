create or replace 
function Obter_qt_item_max_receita(cd_lista_p varchar2,	
			          cd_medicamento_p varchar2)
			return number is

qt_item_max_w	 number(10);

begin

select      nvl(max(qt_item_max),999)
into	qt_item_max_w
from	regra_geracao_receita
where	cd_lista 	             = cd_lista_p
and	cd_medicamento  = cd_medicamento_p;

return qt_item_max_w;

end Obter_qt_item_max_receita;
/
