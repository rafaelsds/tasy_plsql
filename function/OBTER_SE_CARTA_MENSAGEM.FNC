create or replace
function obter_se_carta_mensagem(nr_seq_carta_p		number,
								 nr_seq_carta_mae_p	number)
 		    	return varchar2 is

ie_mensagem_w		varchar2(1);
nm_usuario_w	varchar2(15) := wheb_usuario_pck.get_nm_usuario;
				
begin

select	nvl(max('S'),'N')
into	ie_mensagem_w
from	carta_comunicacao
where  	nr_seq_carta in (	SELECT nr_sequencia
							FROM	carta_medica 
							WHERE nr_seq_carta_mae = nr_seq_carta_mae_p
							union all
							SELECT 	nvl(nr_seq_carta_mae,nr_sequencia)
							FROM	carta_medica 
							WHERE nr_sequencia = nr_seq_carta_p)
and		nvl(nm_destinatario,nm_usuario_w) = nm_usuario_w 
and		dt_liberacao is not null
and		dt_inativacao is null
and		ie_leitura	= 'N'
order by dt_atualizacao_nrec desc ;

return	ie_mensagem_w;

end obter_se_carta_mensagem;
/