create or replace
function obter_dados_tipo_alerta(	nr_sequencia_p	number,
				ie_opcao_p	varchar)
				return varchar2 is
			
ds_retorno_w			varchar2(200) := null;
ds_texto_padrao_w			tipo_alerta_atend.ds_texto_padrao%type;
ie_allow_free_text_w		tipo_alerta_atend.ie_allow_free_text%type;
nr_seq_default_option_w		tipo_alerta_atend.nr_seq_default_option%type;

begin

if	(ie_opcao_p = 'C') then

	select 	max(ds_cor)
	into 	ds_retorno_w
	from 	tipo_alerta_atend
	where 	nr_sequencia = nr_sequencia_p;

elsif	(ie_opcao_p = 'T') then

	select	max(ds_tipo_alerta)
	into	ds_retorno_w
	from	tipo_alerta_atend
	where	nr_sequencia = nr_sequencia_p;

elsif	(ie_opcao_p = 'CORFUNDO') then

	select	max(ds_cor_fundo)
	into	ds_retorno_w
	from	tipo_alerta_atend
	where	nr_sequencia = nr_sequencia_p;

elsif	(ie_opcao_p = 'TAMANHOFONTE') then

	select	max(qt_tamanho_fonte)
	into	ds_retorno_w
	from	tipo_alerta_atend
	where	nr_sequencia = nr_sequencia_p;

elsif	(ie_opcao_p = 'TEXTO') then	

	select	nvl(ie_allow_free_text, 'Y'),
		nr_seq_default_option,
		ds_texto_padrao
	into	ie_allow_free_text_w,
		nr_seq_default_option_w,
		ds_texto_padrao_w
	from	tipo_alerta_atend
	where	nr_sequencia = nr_sequencia_p ;

	if (ie_allow_free_text_w = 'Y') then
		return 	ds_texto_padrao_w;
	else
		ds_retorno_w := nr_seq_default_option_w;
	end if;
end if;	

return ds_retorno_w;

end obter_dados_tipo_alerta;
/
