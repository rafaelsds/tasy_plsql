create or replace
function obter_operacao_nota(	nr_sequencia_p		number)
				return number is

cd_operacao_nf_w	number(4);

begin

select	nvl(cd_operacao_nf,0)
into	cd_operacao_nf_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

return	cd_operacao_nf_w;

end obter_operacao_nota;
/