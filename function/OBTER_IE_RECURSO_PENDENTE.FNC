create or replace
function obter_ie_recurso_pendente(
		nr_seq_lote_p		number)
		return varchar2 is

ie_recurso_pend_w		varchar2(1);

begin

select NVL(MAX('S'),'N')
into ie_recurso_pend_w
        from   lote_audit_hist x,
              TISS_REC_GLOSA_PROT_IMP y
        where  x.nr_seq_lote_audit = nr_seq_lote_p
        and    x.nr_sequencia = y.nr_seq_lote_hist
        and    x.nr_analise = (select  max(w.nr_analise) from lote_audit_hist w where w.nr_seq_lote_audit = x.nr_seq_lote_audit);

return ie_recurso_pend_w;
end obter_ie_recurso_pendente;
/