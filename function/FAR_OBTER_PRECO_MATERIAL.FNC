create or replace 
function 	far_obter_preco_material(
		cd_material_p 		number,
		cd_estabelecimento_p 	number,
		nr_seq_contrato_conv_p	number)
				return number is

cd_tab_preco_mat_w 		number(4);
vl_preco_w 			number(13,4);
vl_preco_venda_w			number(13,4);
vl_preco_minimo_w			number(13,4);
tx_ajuste_w			number(15,4);
tx_ajuste_minimo_w		number(15,4);
cd_grupo_material_w		number(3);
cd_subgrupo_material_w		number(3);
cd_classe_material_w 		number(5);
tx_ajuste_contrato_conv		number(15,4);

cursor c01 is
select	cd_tab_preco_mat,
	nvl(tx_ajuste,0),
	nvl(tx_ajuste_minimo,0)
from	far_regra_preco_mat a
where	nvl(a.cd_grupo_material,cd_grupo_material_w) 		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material,cd_classe_material_w) 		= cd_classe_material_w
and	nvl(a.cd_material,cd_material_p) 			= cd_material_p
and	a.cd_estabelecimento = cd_estabelecimento_p
and	exists (select 1 from preco_material x where x.cd_material = cd_material_p and x.cd_tab_preco_mat = a.cd_tab_preco_mat)
order by 	nvl(cd_material,0),
	nvl(cd_classe_material,0),
	nvl(cd_subgrupo_material,0),
	nvl(cd_grupo_material,0);
	
Cursor C02 is
	select	nvl(a.TX_AJUSTE,0)
	from	FAR_CONTRATO_PRECO a
	where	a.nr_seq_contrato_conv = nr_seq_contrato_conv_p
	and	nvl(a.cd_grupo_material,cd_grupo_material_w) 		= cd_grupo_material_w
	and	nvl(a.cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
	and	nvl(a.cd_classe_material,cd_classe_material_w) 	= cd_classe_material_w
	and	nvl(a.cd_material,cd_material_p) = cd_material_p
	order by nvl(cd_material,0) asc,
		nvl(cd_classe_material,0) asc,
		nvl(cd_subgrupo_material,0) asc,
		nvl(cd_grupo_material,0) asc;

begin
select 	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from 	estrutura_material_v
where	cd_material = cd_material_p;

open c01;
loop
fetch c01 into
	cd_tab_preco_mat_w,
	tx_ajuste_w,
	tx_ajuste_minimo_w;
exit when c01%notfound;
end loop;
close c01;

if (nr_seq_contrato_conv_p is not null) then
	begin
	open C02;
	loop
	fetch C02 into	
		tx_ajuste_contrato_conv;
	exit when C02%notfound;
	end loop;
	close C02;
	end;
end if;

select 	vl_preco_venda
into	vl_preco_w
from 	preco_material a
where 	cd_material = cd_material_p
and	cd_tab_preco_mat = cd_tab_preco_mat_w
and	ie_situacao = 'A'
and	trunc(dt_inicio_vigencia) =	(select	max(x.dt_inicio_vigencia)
				from	preco_material x
				where	x.cd_tab_preco_mat = a.cd_tab_preco_mat
				and	x.cd_material = a.cd_material
				and	x.ie_situacao = 'A'
				and	trunc(x.dt_inicio_vigencia) <= trunc(sysdate));

vl_preco_venda_w := nvl(vl_preco_w,0) + (dividir((vl_preco_w * nvl(tx_ajuste_w,0)),100));
vl_preco_minimo_w := nvl(vl_preco_w,0) + (dividir((vl_preco_w * nvl(tx_ajuste_minimo_w,0)),100));
vl_preco_w := nvl(vl_preco_venda_w,0) - (dividir((vl_preco_venda_w * nvl(tx_ajuste_contrato_conv,0)),100));

if	(vl_preco_w < vl_preco_minimo_w) then
	vl_preco_w	:= vl_preco_minimo_w;
end if;

return nvl(vl_preco_w,0);

end far_obter_preco_material;
/