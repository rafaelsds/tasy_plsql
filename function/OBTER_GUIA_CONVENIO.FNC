CREATE OR REPLACE 
FUNCTION OBTER_GUIA_CONVENIO	(nr_atendimento_p		number)
				RETURN VARCHAR2 IS

nr_doc_convenio_w	VARCHAR2(20);

BEGIN

SELECT	max(nr_doc_convenio)
INTO	nr_doc_convenio_w
FROM	atend_categoria_convenio
WHERE	nr_seq_interno = obter_atecaco_atendimento(nr_atendimento_p);

RETURN	nr_doc_convenio_w;

END	obter_guia_convenio;
/