create or replace
FUNCTION es_obter_dados_proj (IE_OPCAO_P VARCHAR2,
 		    	        cd_projeto_p  number)	return varchar2 is

ds_retorno_w		varchar2(60);
qt_horas_w      varchar2(60);
pr_concluido_w  varchar2(60);
previstas_w     varchar2(60);
realizado_w     varchar2(60);
saldo_w         varchar2(60);
qt_horas_ad_w   varchar2(60);

/*
H	- % Horas consumidas
P	- % Conclusao projeto
 T	- Horas Previstas
 R	- Horas Realizadas
 S	- Horas Saldo
 A	- Horas Adicionais
 */
 
 begin
select 	distinct(' ' || (dividir(nvl(b.qt_hora_real,0), nvl(b.qt_hora_prev,0))* 100)  || '% horas ') qt_horas,
	b.pr_etapa || '% implantado' pr_concluido,
	Campo_Mascara_virgula_casas( b.qt_hora_prev, 1) qt_total_horas,
    Campo_Mascara_virgula_casas( b.qt_hora_real, 1) qt_horas_realizado,
    Campo_Mascara_virgula_casas( b.qt_hora_saldo, 1) qt_horas_saldo
into qt_horas_w, pr_concluido_w, previstas_w, realizado_w, saldo_w
from    proj_cron_etapa b,
         proj_cronograma a
where  a.nr_sequencia = b.nr_seq_cronograma
and  a.ie_cobrado = 'S'
and  a.nr_seq_proj  = cd_projeto_p
and  b.nr_seq_superior is null
and  rownum = 1;

if	(nvl(ie_opcao_p, 'H') = 'H') then
ds_retorno_w	:= qt_horas_w;
elsif	(nvl(ie_opcao_p, 'P') = 'P') then
ds_retorno_w	:= pr_concluido_w;
elsif	(nvl(ie_opcao_p, 'T') = 'T') then
ds_retorno_w	:= previstas_w;
elsif	(nvl(ie_opcao_p, 'R') = 'R') then
ds_retorno_w	:= realizado_w;
elsif	(nvl(ie_opcao_p, 'S') = 'S') then
ds_retorno_w	:= saldo_w;
elsif	(nvl(ie_opcao_p, 'A') = 'A') then
select	sum(qt_n_adicionais) qt_realizadas
into qt_horas_ad_w
from(
select	sum(to_number(dividir(a.qt_min_ativ, 60))) qt_adicionais,
sum(to_number(dividir(decode(a.ie_atividade_extra, 'S', a.qt_min_ativ, 0), 60))) qt_n_adicionais,
nvl(d.qt_horas_saldo,0) qt_saldo
from	proj_rat b,
proj_rat_ativ a,
proj_cron_etapa x,
proj_cronograma d,
proj_projeto c
where	b.nr_sequencia = a.nr_seq_rat
and	d.nr_sequencia  = x.nr_seq_cronograma
and	x.nr_sequencia  = a.nr_seq_etapa_cron
and	d.nr_seq_proj = c.nr_sequencia
and	c.nr_seq_cliente = b.nr_seq_cliente
and	c.nr_sequencia = cd_projeto_p
and	d.ie_cobrado = 'S'
group by	d.qt_horas_saldo)
where rownum = 1
group by qt_saldo;

ds_retorno_w	:= qt_horas_ad_w ;
end if;
	
RETURN	ds_retorno_w;

END es_obter_dados_proj;
/