CREATE OR REPLACE FUNCTION OBTER_SE_MEDICO_ESPECIALIDADE (NR_SEQ_REGRA_P	Number,
							  CD_MEDICO_ATEND_P	Varchar2)
					RETURN VARCHAR2 IS

DS_RETORNO_W			VARCHAR2(1) := 'S';
CD_ESPEC_MEDICO_ATEND_W		NUMBER(5);
NR_COUNT_W			NUMBER := 0;

BEGIN

Select	nvl(max(cd_espec_medic_atend),0)
into	cd_espec_medico_atend_w
from	regra_lanc_automatico
where	nr_sequencia = NR_SEQ_REGRA_P;

if	(cd_espec_medico_atend_w > 0) then

	Select	count(*)
	into	nr_count_w
	from	medico_especialidade
	where	cd_pessoa_fisica = CD_MEDICO_ATEND_P
	and	cd_especialidade = CD_ESPEC_MEDICO_ATEND_W;

	if	(nr_count_w = 0) then
		ds_retorno_w	:= 'N';
	end if;
end if;

RETURN DS_RETORNO_W;

END OBTER_SE_MEDICO_ESPECIALIDADE;
/