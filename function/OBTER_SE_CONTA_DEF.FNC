create or replace
function Obter_Se_Conta_Def(nr_atendimento_p	number)
 		    	return varchar2 is
			
ie_status_w	varchar2(1) := 'N';
qt_contas_def_w	number(10);

begin

if	(nvl(nr_atendimento_p,0) > 0) then

	select	count(*)
	into	qt_contas_def_w
	from	conta_paciente
	where	nr_atendimento	= nr_atendimento_p
	and	ie_status_acerto	= 2;
	
	if	(qt_contas_def_w > 0) then
		ie_status_w	:= 'S';
	end if;

end if;

return	ie_status_w;

end Obter_Se_Conta_Def;
/