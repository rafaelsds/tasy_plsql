create or replace
function pls_obter_se_espec_solic_oc
			(	nr_seq_proc_p		number,
				nr_seq_proc_espec_p	number,
				cd_medico_p		number	)
				return varchar2 is

ds_retorno_w			varchar2(1) := 'S';
cd_procedimento_w		Number(10);
ie_origem_proced_w		Number(10);
cd_area_w			Number(10);
cd_especialidade_w		Number(10);
cd_grupo_w			Number(10);
ie_origem_w			Number(10);
nr_seq_regra_proc_espec_w	Number(10) := 0;
qt_regra_valida_w		Number(10);
ie_ocorrencia_w			pls_controle_estab.ie_ocorrencia%type := pls_obter_se_controle_estab('GO');
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
	
Cursor c02 is
	select	a.nr_sequencia,
		nvl(a.ie_liberado,'S')
	from	pls_oc_proc_espec_regra	a,
		pls_oc_proc_especialidade b
	where	a.nr_seq_regra = b.nr_sequencia
	and	((cd_procedimento is null) or ((cd_procedimento = nvl(cd_procedimento_w,0)) and (ie_origem_proced = nvl(ie_origem_w,0))))
	and	nvl(cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
	and	nvl(cd_especialidade, cd_especialidade_w)	= cd_especialidade_w
	and	nvl(cd_area_procedimento, cd_area_w) 		= cd_area_w	
	and	a.nr_seq_regra		 			= nr_seq_proc_espec_p
	and	a.ie_situacao		 			= 'A'
	and	exists	(	select	x.cd_especialidade
				from	medico_especialidade x
				where	x.cd_pessoa_fisica = cd_medico_p
				and	x.cd_especialidade = b.cd_espec_medica
				and	rownum <= 1)
	order by
		nvl(a.cd_procedimento,0), 
		nvl(a.cd_grupo_proc,0),
		nvl(a.cd_especialidade,0),
		nvl(a.cd_area_procedimento,0);
			
begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

/*Obter procedimento da conta*/
begin
select	nvl(a.cd_procedimento,0),
	nvl(a.ie_origem_proced,0)
into	cd_procedimento_w,
	ie_origem_proced_w
from	pls_conta_proc	a,
	pls_conta	b
where	a.nr_seq_conta	= b.nr_sequencia
and	a.nr_sequencia  = nr_seq_proc_p
and	ie_tipo_despesa = 1; /* Regra v�lida apenas para procedimentos */
exception
when others then
	cd_procedimento_w	:= 0;
end;

if	(cd_procedimento_w > 0) then
	/*Obter area, grupo, especialidade do procedimentos  */
	pls_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, cd_area_w,
			      cd_especialidade_w, cd_grupo_w, ie_origem_w);
		
	/*Verificar entre as especialidade do m�dico solic/exec se existe alguma com regra que se encaixa*/
	open c02;
	loop
	fetch c02 into
		nr_seq_regra_proc_espec_w,
		ds_retorno_w;
	exit when c02%notfound;
	end loop;
	close c02;

	if	(ds_retorno_w	= 'N') then
		if	(ie_ocorrencia_w = 'S') and
			(cd_estabelecimento_w is not null) then
			select	count(1)
			into	qt_regra_valida_w
			from	pls_oc_proc_espec_regra	a,
				pls_oc_proc_especialidade b
			where	a.nr_seq_regra = b.nr_sequencia
			and	((cd_procedimento is null) or ((cd_procedimento = nvl(cd_procedimento_w,0)) and (ie_origem_proced = nvl(ie_origem_w,0))))
			and	nvl(cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
			and	nvl(cd_especialidade, cd_especialidade_w)	= cd_especialidade_w
			and	nvl(cd_area_procedimento, cd_area_w) 		= cd_area_w	
			and	a.ie_situacao		 			= 'A'
			and	a.ie_liberado					= 'S'
			and	exists	 (	select	x.cd_especialidade
						from	medico_especialidade x
						where	x.cd_pessoa_fisica = cd_medico_p
						and	x.cd_especialidade = b.cd_espec_medica
						and	rownum <= 1)
			and	rownum <= 1;
		else
			select	count(1)
			into	qt_regra_valida_w
			from	pls_oc_proc_espec_regra	a,
				pls_oc_proc_especialidade b
			where	a.nr_seq_regra = b.nr_sequencia
			and	((cd_procedimento is null) or ((cd_procedimento = nvl(cd_procedimento_w,0)) and (ie_origem_proced = nvl(ie_origem_w,0))))
			and	nvl(cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
			and	nvl(cd_especialidade, cd_especialidade_w)	= cd_especialidade_w
			and	nvl(cd_area_procedimento, cd_area_w) 		= cd_area_w	
			and	a.ie_situacao		 			= 'A'
			and	a.ie_liberado					= 'S'
			and	b.cd_estabelecimento 				= cd_estabelecimento_w
			and	exists	 (	select	x.cd_especialidade
						from	medico_especialidade x
						where	x.cd_pessoa_fisica = cd_medico_p
						and	x.cd_especialidade = b.cd_espec_medica
						and	rownum <= 1)
			and	rownum <= 1;
		end if;

		if	(qt_regra_valida_w > 0) then
			ds_retorno_w	:= 'S';
		end if;
	end if;
	
else
	ds_retorno_w	:= 'S';	
end if;

/*
ds_retorno_w;
-------------------
N - N�o Liberado - gera oc
S - Liberado - N�o gera oc
*/

return	ds_retorno_w;

end pls_obter_se_espec_solic_oc;
/
