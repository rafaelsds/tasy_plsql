create or replace
function obter_usu_orig_assinatura(	nr_atendimento_p		number,
				cd_setor_atendimento_p	number)
				return varchar2 is

qt_itens_w		number(10);
nm_usuario_original_w	varchar2(15);
cd_medico_w		varchar2(10);
qt_conta_w		number(10) := 0;
nm_usuario_original_ww	varchar2(15) := '0';
qt_lancamento_w		number(10);

Cursor C01 is
	select	sum(qt_itens) qt_itens,
		nm_usuario_original
	from	(
		select	count(*) qt_itens,
			nm_usuario_original
		from	material_atend_paciente a,
			estrutura_material_v b
		where	a.cd_material = b.cd_material
		and	((b.cd_subgrupo_material = 47) or (a.cd_material  in (149117, 149119,190840,190838,190839,46415,46416,46417,46359,46388,46389,46410,46411,46375,46356,46360,46363,46399,46387,140943,46420,46421,46422,136136,137830,137833,45508,45510,45512,45513,45515,128985,128986)))
		and	((b.cd_classe_material = 145) or (a.cd_material  in (149117, 149119,190840,190838,190839,46415,46416,46417,46359,46388,46389,46410,46411,46375,46356,46360,46363,46399,46387,140943,46420,46421,46422,136136,137830,137833,45508,45510,45512,45513,45515,128985,128986)))
		and	a.cd_motivo_exc_conta is null
		and	nr_atendimento = nr_atendimento_p
		and	(cd_setor_atendimento = cd_setor_atendimento_p or cd_setor_atendimento_p = 0)
		group by	nm_usuario_original)
	group by nm_usuario_original;
	
begin

open C01;
loop
fetch C01 into	
	qt_itens_w,
	nm_usuario_original_w;
exit when C01%notfound;
	begin
		select	count(*)
		into	qt_lancamento_w
		from	usuario a,
			usuario_assinatura b
		where	a.nm_usuario = b.nm_usuario_atual
		and	b.nm_usuario_atual = nm_usuario_original_w;	
		
		if (qt_lancamento_w > 0) then
		
			if	(qt_itens_w > qt_conta_w) then
				nm_usuario_original_ww := nm_usuario_original_w;
			end if;
			
			qt_conta_w := qt_itens_w;
		
		end if;
	
	end;
end loop;
close C01;

select 	nvl(max(cd_pessoa_fisica),'0')
into	cd_medico_w
from 	usuario
where	nm_usuario = nm_usuario_original_ww
and	ie_situacao = 'A';

return	cd_medico_w;

end obter_usu_orig_assinatura;
/
