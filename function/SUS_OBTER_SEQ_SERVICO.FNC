CREATE OR REPLACE
FUNCTION sus_obter_seq_servico
                        (       cd_servico_p       Number)
                                return Number is




nr_sequencia_w          Number(10);


BEGIN


select	max(nr_sequencia)
into	nr_sequencia_w
from	sus_servico
where	cd_servico	= cd_servico_p;

return nr_sequencia_w;

END sus_obter_seq_servico;
/