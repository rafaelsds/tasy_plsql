create or replace
function HMC_verifica_caracter(nr_prescricao_p   number,
								nr_seq_solucao_p number)
									return varchar2 is
			
ds_urgencia_w		varchar2(1);
ds_retorno_w 		varchar2(10) := 'N';
ds_controlado_w 	varchar2(1);
ds_aleatorio_w 		varchar2(1);


begin

select 	nvl(max('S'),'N')
into 	ds_urgencia_w
from 	prescr_solucao a,
		prescr_material c
WHERE	c.nr_sequencia = a.nr_seq_solucao
AND 	c.nr_prescricao = a.nr_prescricao
and		obter_se_intervalo_agora(a.cd_intervalo) = 'S'
AND		a.nr_seq_solucao  = nr_seq_solucao_p
and		a.nr_prescricao = nr_prescricao_p;


select 	nvl(max('S'),'N')
into 	ds_controlado_w
From 	prescr_solucao a,
		prescr_material c
WHERE	c.nr_sequencia = a.nr_seq_solucao
AND 	c.nr_prescricao = a.nr_prescricao
AND		obter_se_medic_controlado(c.cd_material)  = 'S'
and		a.nr_seq_solucao  = nr_seq_solucao_p
and 	a.nr_prescricao = nr_prescricao_p;



if	(ds_urgencia_w = 'S') and
	(ds_controlado_w = 'S') then 
	ds_retorno_w := 'T';
elsif (ds_urgencia_w = 'S') then
	ds_retorno_w := 'A';
elsif (ds_controlado_w = 'S') then
	ds_retorno_w := 'C';
else
	ds_retorno_w := 'N';
end if;


return	ds_retorno_w;

end HMC_verifica_caracter;
/