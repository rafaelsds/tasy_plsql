create or replace
function Obter_se_questiona_extensao(cd_perfil_p		number,
									cd_setor_atendimento_p	number,
									nr_atendimento_p number) return varchar2 is

			
hr_inicio_w		varchar2(5);
hr_fim_w		varchar2(5);
dt_inicio_w		date;
dt_fim_w		date;
ie_questiona_w	varchar2(1) := 'N';
ie_questionar_w	periodo_questiona_extensao.ie_questionar%type;
qt_prescr_w		number(3) := 0;
qt_prescr_lib_w	number(3) := 0;
hr_corte_w		date;
hr_corte_c_w	varchar2(20);


cursor c01 is
select	hr_inicio,
		hr_fim,
		nvl(ie_questionar,'N')
from	periodo_questiona_extensao
where	nvl(cd_setor_atendimento,cd_setor_atendimento_p) = cd_setor_atendimento_p
and		nvl(cd_perfil,cd_perfil_p) = cd_perfil_p;
	
begin

if	(cd_setor_atendimento_p > 0) then
	select	count(*)
	into	qt_prescr_w
	from	prescr_medica
	where	nr_atendimento = nr_atendimento_p
	and		cd_setor_atendimento = cd_setor_atendimento_p
	and		rownum = 1;

	if (qt_prescr_w > 0) then 

		select	nvl(Max(substr(PKG_DATE_FORMATERS.TO_VARCHAR(hr_inicio_prescricao,'shorTime', obter_estabelecimento_ativo, obter_usuario_ativo),12,19)),'00:00:00')
		into	hr_corte_c_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_p;

		hr_corte_w := to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || hr_corte_c_w, 'dd/mm/yyyy hh24:mi:ss');

		if (sysdate between hr_corte_w and (hr_corte_w + 1)) then
			hr_corte_w := hr_corte_w + 1;
		end if;

		select	count(*)
		into	qt_prescr_lib_w
		from	prescr_medica
		where	nr_atendimento = nr_atendimento_p
		and		cd_setor_atendimento = cd_setor_atendimento_p
		and		dt_inicio_prescr between hr_corte_w and (hr_corte_w + 1)
		and		nvl(dt_liberacao, dt_liberacao_medico) is not null
		and		dt_suspensao is null
		and		rownum = 1;
		
	end if;

end if;	
open C01;
loop
fetch C01 into	
	hr_inicio_w,
	hr_fim_w,
	ie_questionar_w;
exit when C01%notfound;
	begin
	
	if (ie_questionar_w = 'N' 
		or (ie_questionar_w in ('F','A') and (qt_prescr_lib_w > 0))
		or (ie_questionar_w in ('P','A') and (qt_prescr_w = 0))) then
	
		dt_inicio_w	:= to_date(to_char(sysdate,'dd/mm/yyyy ') 	|| substr(hr_inicio_w,1,5) || ':00','dd/mm/yyyy hh24:mi:ss');
		dt_fim_w 	:= to_date(to_char(sysdate,'dd/mm/yyyy ') 	|| substr(hr_fim_w,1,5)    || ':00','dd/mm/yyyy hh24:mi:ss');
		
		
		if	(dt_fim_w < dt_inicio_w) then
			dt_fim_w	:= dt_fim_w + 1;
		end if;
		if	(sysdate between dt_inicio_w and dt_fim_w)	then
			ie_questiona_w := 'S';
		end if;
		
	end if;
	
	end;
end loop;
close C01;

return	ie_questiona_w;

end Obter_se_questiona_extensao;
/
