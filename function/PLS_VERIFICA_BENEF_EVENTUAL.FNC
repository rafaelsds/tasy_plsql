create or replace
function pls_verifica_benef_eventual
			(	cd_usuario_plano_p	Varchar2,
				ie_tipo_verificacao_p	Varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number)
				return Varchar2 is
/*
ie_tipo_verificacao_p
B = Verifica se o benefici�rio eventual que esta sendo criado � da pr�pria operadora
D = Verifica se o digito verificador da carteirinha do benefici�rio � v�lido
*/
			
cd_cooperativa_w		Varchar2(30);

ie_retorno_w			Varchar2(2)	:= 'N';
ie_digito_valido_w		Varchar2(2);
ie_benef_operadora_atual_w	Varchar2(2);

nr_seq_emissor_w		Number(10);
nr_seq_carteira_seg_w		Number(10);

qt_coop_estab_w			Number(4);

begin
if	(ie_tipo_verificacao_p	= 'B') then
	select	max(nr_seq_emissor)
	into	nr_seq_emissor_w
	from	pls_parametros
	where	cd_estabelecimento	= cd_estabelecimento_p;

	select	max(nr_sequencia)
	into	nr_seq_carteira_seg_w
	from	pls_segurado_carteira
	where	cd_usuario_plano	= cd_usuario_plano_p
	and	cd_estabelecimento	= cd_estabelecimento_p;

	if	(nvl(nr_seq_carteira_seg_w,0) = 0) then	/* Carteira do benefici�rio n�o existe na base */
		select	pls_obter_campos_carteira(cd_usuario_plano_p,nr_seq_emissor_w,'CM')
		into	cd_cooperativa_w
		from	dual;
		
		if	(nvl(cd_cooperativa_w,'0') <> '0') then
			select	count(*)
			into	qt_coop_estab_w
			from	pls_congenere  a,
				pls_outorgante b
			where	b.cd_cgc_outorgante	= a.cd_cgc
			and	a.cd_cooperativa	= cd_cooperativa_w
			and	a.cd_estabelecimento	= cd_estabelecimento_p;
			
			if	(qt_coop_estab_w	> 0) then
				ie_benef_operadora_atual_w	:= 'S';
			elsif	(qt_coop_estab_w 	= 0) then
				ie_benef_operadora_atual_w	:= 'N';
			end if;
			ie_retorno_w	:= ie_benef_operadora_atual_w;
		end if;
	end if;
elsif	(ie_tipo_verificacao_p	= 'D') then
	if	(pls_calcula_digito_carteira(substr(cd_usuario_plano_p, 1, length(cd_usuario_plano_p) -1)) <>  
		substr(cd_usuario_plano_p, length(cd_usuario_plano_p), 1)) then
		ie_digito_valido_w	:= 'N';
	else
		ie_digito_valido_w	:= 'S';
	end if;
	ie_retorno_w	:= ie_digito_valido_w;
end if;

return	ie_retorno_w;

end pls_verifica_benef_eventual;
/