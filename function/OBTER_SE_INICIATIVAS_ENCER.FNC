create or replace
function obter_se_iniciativas_encer (nr_sequencia_p		number)
							return varchar2 is
ds_retorno_w varchar2(1);

begin

select 	decode(count(*),0,'S','N')
into	ds_retorno_w
from	man_ordem_servico a
where	a.nr_seq_obj_bsc = nr_sequencia_p
and		((trunc(a.dt_fim_real) >= trunc(sysdate)) or (a.dt_fim_real is null));

return	ds_retorno_w;

end obter_se_iniciativas_encer ;
/