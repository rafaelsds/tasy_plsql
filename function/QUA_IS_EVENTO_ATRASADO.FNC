create or replace
function qua_is_evento_atrasado (	nr_seq_evento_p		number	)
		return varchar2 is
		
ds_retorno_w		varchar2(2);

begin

select	decode(x.cont, 0, 'N', 'S')
into	ds_retorno_w
from	(	select 	count(*) cont
		from	qua_evento_paciente a,
			qua_gravidade_evento b
		where	a.nr_seq_gravidade = b.nr_sequencia
		and	a.nr_sequencia = nr_seq_evento_p
		and	a.ie_status <> '5'
		and	(sysdate - a.dt_cadastro) > b.qt_dias	) x;

return ds_retorno_w;
		
end qua_is_evento_atrasado;
/