create or replace
function san_obter_volume_retirar(nr_prescricao_p	number)
 		    	return number is

qt_volume_retirar_w	 cpoe_hemoterapia.qt_volume_retirar%type;
nr_seq_cpoe_w		prescr_solic_bco_sangue.nr_seq_hemo_cpoe%type;

begin

if	(nvl(nr_prescricao_p,0) > 0) then

	select max(nr_seq_hemo_cpoe)
	into	nr_seq_cpoe_w
	from prescr_solic_bco_sangue
	where nr_prescricao = nr_prescricao_p;

	if	(nvl(nr_seq_cpoe_w,0) > 0) then
	
		select	max(qt_volume_retirar)
		into	qt_volume_retirar_w
		from	cpoe_hemoterapia
		where	nr_sequencia = nr_seq_cpoe_w;

	end if;	
	
end if;

return	nvl(qt_volume_retirar_w,0);

end san_obter_volume_retirar;
/