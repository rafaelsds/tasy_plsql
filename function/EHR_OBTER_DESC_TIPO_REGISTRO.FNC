create or replace
function ehr_obter_desc_tipo_registro(nr_sequencia_p	Number)
				return varchar2 is

ds_tipo_registro_w	varchar2(80);

begin
if	(nr_sequencia_p	is not null)then
	select	ds_tipo_registro
	into	ds_tipo_registro_w  
	from	ehr_tipo_registro
	where	nr_sequencia	= nr_sequencia_p;	
end if;
return	ds_tipo_registro_w;

end	ehr_obter_desc_tipo_registro;
/