create or replace
function obter_senha_pf(dt_nascimento_p	date default null)	return varchar2 is

ie_regra_w	varchar2(3);
ds_senha_w	varchar2(20);

begin
/* obter valor par�metro */
select	nvl(nvl(vl_parametro, vl_parametro_padrao),'XX')
into	ie_regra_w
from	funcao_parametro
where	cd_funcao	= 0
and	nr_sequencia	= 65;

/* gerar senha regra */
if	(ie_regra_w = 'NS') then
	select	to_char(atendimento_senha_seq.nextval)
	into	ds_senha_w
	from 	dual;
elsif	(ie_regra_w = 'NA') then
	ds_senha_w	:= obter_numero_randomico;
elsif	(ie_regra_w = 'LN') then
	ds_senha_w	:= obter_senha_letra_numero(6);
elsif	(ie_regra_w = 'DS' and dt_nascimento_p is not null) then
	ds_senha_w := replace(dt_nascimento_p,'/','');
end if;

return ds_senha_w;

end obter_senha_pf;
/
