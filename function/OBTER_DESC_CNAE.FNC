create or replace
function obter_desc_cnae(nr_sequencia_p		number)
 		    	return varchar2 is

ds_cnae_w		varchar2(255);
			
begin

select	ds_cnae
into	ds_cnae_w
from	pls_cnae
where	nr_sequencia = nr_sequencia_p;

return	ds_cnae_w;

end obter_desc_cnae;
/