create or replace 
FUNCTION Obter_setor_ge(
			ie_opcao_p		Varchar2,
			nr_prescricao_p		Number,
			nr_atendimento_p	Number,
			ie_retorno_p		varchar2)
			return Varchar is

ds_retorno_w		Varchar(100);
nr_seq_unidade_w	NUMBER(10);
cd_setor_w			number(10);

/* ie_opcao_p

N - N�o traz nenhum setor
S - Setor atual do paciente
P - Setor da prescricao
E - Setor de entrega prescricao

*/


/* ie_retorno_p

D - Descri��o

C - C�digo

*/

BEGIN

if	(ie_opcao_p = 'N') then
	ds_retorno_w	:= '';
elsif	(ie_opcao_p = 'S') then

	select 	Obter_AtePacu_Paciente(nr_atendimento_p,'IAA')
	into	nr_seq_unidade_w
	from	dual;
	
	
	select	max(ds_setor_atendimento || ' (' || a.cd_unidade_basica || '-' || cd_unidade_compl || ')'),
			max(b.cd_setor_atendimento)
	into	ds_retorno_w,
			cd_setor_w
	from 	atend_paciente_unidade a,
		setor_atendimento b	
	where	a.cd_setor_atendimento = b.cd_setor_atendimento
	and	a.nr_seq_interno = nr_seq_unidade_w;


	--select	substr(obter_setor_atepacu(nr_seq_unidade_w,1),1,100)
	--into	ds_retorno_w
	--from	dual;
	
	
	
elsif	(ie_opcao_p = 'P') then
	select	substr(obter_setor_prescricao(nr_prescricao_p,'D'),1,100),
			substr(obter_setor_prescricao(nr_prescricao_p,'C'),1,100)
	into	ds_retorno_w,
			cd_setor_w
	from	dual;
elsif	(ie_opcao_p = 'E') then
	select	substr(obter_setor_entrega_prescr(nr_prescricao_p,'D'),1,100),
			substr(obter_setor_entrega_prescr(nr_prescricao_p,'C'),1,100)
	into	ds_retorno_w,
			cd_setor_w
	from	dual;
elsif	(ie_opcao_p = 'CC') then
	
	select 	max(Obter_AtePacu_Paciente(nr_atendimento_p,'IAC'))
	into	nr_seq_unidade_w
	from	dual;	
	
	select	max(ds_setor_atendimento || ' (' || a.cd_unidade_basica || '-' || cd_unidade_compl || ')'),
			max(b.cd_setor_atendimento)
	into	ds_retorno_w,
			cd_setor_w
	from 	atend_paciente_unidade a,
		setor_atendimento b	
	where	a.cd_setor_atendimento = b.cd_setor_atendimento
	and	a.nr_seq_interno = nr_seq_unidade_w;
end if;


if	(ie_retorno_p = 'C') then
	ds_retorno_w := cd_setor_w;
end if;

return	ds_retorno_w;

END Obter_setor_ge;
/
