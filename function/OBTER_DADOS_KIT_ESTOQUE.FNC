create or replace
function obter_dados_kit_estoque(	nr_sequencia_p	in 	number,
				ie_retorno_p	in	varchar2)
				return varchar2 is

/*ie_retorno_p
'CR' = Nr Cirurgia
CK = C�digo do kit material
DL = Data de libera��o da solicita��o do kit*/

nr_cirurgia_w		number(10);
cd_kit_material_w		number(10);
ds_retorno_w		varchar2(255);
nr_seq_solic_kit_w		number(10);

begin
begin
select	nr_cirurgia,
	cd_kit_material,
	nr_seq_solic_kit
into	nr_cirurgia_w,
	cd_kit_material_w,
	nr_seq_solic_kit_w
from	kit_estoque
where	nr_sequencia = nr_sequencia_p;
exception
when others then
	begin
	nr_cirurgia_w		:= null;
	cd_kit_material_w	:= null;
	nr_seq_solic_kit_w	:= null;
	end;
end;

if	(ie_retorno_p = 'CR') then
	ds_retorno_w := nr_cirurgia_w;
elsif	(ie_retorno_p = 'CK') then
	ds_retorno_w := cd_kit_material_w;
elsif	(ie_retorno_p = 'DL') and
	(nr_seq_solic_kit_w is not null)then
	select	to_char(dt_liberacao, 'dd/mm/yyyy')
	into	ds_retorno_w
	from	solic_kit_material
	where	nr_sequencia = nr_seq_solic_kit_w;
end if;

return	ds_retorno_w;
end obter_dados_kit_estoque;
/