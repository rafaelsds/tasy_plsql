create or replace
function obter_se_gerente_lider_grupo(	nm_usuario_p 		varchar2,
					nr_seq_grupo_p		number,
					nr_seq_gerencia_p	number)
 		    	return varchar2 is

ie_existe_w 		varchar2(1)  := 'N';
cd_pessoa_fisica_w 	number(10);

begin

if (nm_usuario_p is not null) then
	begin

	select 	max(OBTER_PF_USUARIO(nm_usuario_p,'C'))
	into 	cd_pessoa_fisica_w 
	from 	dual;

	select 	decode(count(*), 0, 'N', 'S')
	into	ie_existe_w
	from 	grupo_desenvolvimento
	where 	nm_usuario_lider = nm_usuario_p
	and   	nr_sequencia = nr_seq_grupo_p;
	
	if (ie_existe_w = 'N') then
		begin
	
		SELECT 	DECODE(COUNT(*),0,'N','S')
		into 	ie_existe_w
		FROM 	dual
		WHERE 	Obter_Gerencia_grupo_desen(nr_seq_grupo_p, 'C') = nr_seq_gerencia_p
		AND 	OBTER_RESPONSAVEL_GERENCIA(nr_seq_gerencia_p) = cd_pessoa_fisica_w;
		
		end;
		
	end if;
	
	end;
	
end if;
	
return	ie_existe_w;

end obter_se_gerente_lider_grupo;
/