create or replace
function obter_se_protocolo_proc_int(cd_protocolo_p	    number,
				     cd_procedimento_p  number,
   				     ie_origem_proced_p number)
 		    	return varchar2 is
ds_retorno_w	varchar2(1);			
begin

select  decode(count(*),0,'N','S')
into	ds_retorno_w
from 	protocolo_proc_interno x,                 
	proc_interno z                          
where   x.nr_seq_proc_interno = z.nr_sequencia  
and   	x.cd_protocolo = cd_protocolo_p
and 	z.cd_procedimento =  cd_procedimento_p
and 	z.ie_origem_proced  = ie_origem_proced_p;

return	ds_retorno_w;

end obter_se_protocolo_proc_int;
/