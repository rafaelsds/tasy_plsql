create or replace
function spdm_obter_diags_paciente(nr_atendimento_p number)
				return varchar is

ds_retorno_w	varchar2(4000);
cd_aux_w		varchar2(10);
ds_aux_w		varchar2(4000);

cursor c01 is
	select substr(obter_desc_cid(CD_DOENCA),1,200),
		   cd_doenca
	from   diagnostico_doenca w 
	where  w.nr_atendimento = nr_atendimento_p;
	
	
begin
open c01;
loop
fetch	c01 into ds_aux_w,
				 cd_aux_w;
		exit when c01%notfound;
		
		begin
			if(ds_aux_w is not null) then
				ds_retorno_w := ds_retorno_w ||cd_aux_w|| ' - '||ds_aux_w||', ';
			end if;
		end;
end loop;
close c01;
				
return trim(' ' from ds_retorno_w);
end spdm_obter_diags_paciente;
/