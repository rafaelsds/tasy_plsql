create or replace
function pls_obter_bloq_cad_prest_web (	nr_seq_usuario_web_p	pls_usuario_web.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) return varchar2 is
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Verificar se para o perfil do usu�rio web, existe alguma regra de atualiza��o de cadastro 
	do prestador, e se o usu�rio se enquadra em uma delas..
	
	Ao se enquadrar, ser� comparado se a data de referencia ultrapassa o prazo em dias, em compara��o
	com a data atual. Se ultrapassar, ser� retornado 'S' - Sim, sen�o 'N' - N�o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ X] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:	Essa rotina deve ser utilzada no login do portal, portanto um cuidado especial 
		com rela��o a performance deve ser tomado.
		
		Primeiro � verificado se existe alguma regra cadastrada para o estabelecimento,
		onde a busca utiliza um campo indexado, otimizando a busca. Caso n�o encontre nenhum,
		o restante da fun��o n�o � executada. Isto otimiza para os clientes que n�o queriam 
		utilizar essa regra.
		
		Se for encontrado alguma regra, � ent�o verificado nelas se existe algum prazo v�lido. 
		Se n�o possuir, a function n�o chega a fazer a busca na parte mais pesada da rotina, verificar
		as altera��es no prestador.
		
		A busca no prestador � mais pesada, devido a foma de liga��o da tasy_solic_alt_campo 
		com o prestador, atualmente � necess�rio alguns "OR", e compara��o de texto.... o que � pesado.
		Caso a situa��o n�o seja sustent�vel, ser� necess�rio realizar alguma altera��o
		na forma de relacionar a altera��o da documenta��o com o prestador, para ganhar performance.
		
		A altera��o deste relacionamento n�o foi feito na cria��o desta fun��o, pois fugia muito
		do escopo da OS, caso necess�rio, ser� aberto uma os de performance, pois a forma como
		est� hoje, j� � bem antiga,  e a tabela deve ser utilizada na aplica��o principal.. por isso ser�
		necessario muito cuidado ao alterar ela.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 
ds_retorno_w		varchar2(1) := 'N';
qt_regras_w		pls_integer;
nr_dias_prazo_w		pls_regra_atual_cad_prest.nr_prazo%type := null;
ie_data_referencia_w	pls_regra_atual_cad_prest.ie_data_referencia%type := null;
dt_ultima_atz_w		tasy_solic_alteracao.dt_analise%type;

-- Curso levando as regras elegiveis, j� ordenado
cursor	c01 (	nr_seq_usuario_web_pc	pls_usuario_web.nr_sequencia%type,
		cd_estabelecimento_pc	estabelecimento.cd_estabelecimento%type) is
	select	a.nr_prazo,
		a.ie_data_referencia
	from	pls_regra_atual_cad_prest	a,
		pls_perfil_web			b,
		pls_usuario_web			c
	where	b.nr_sequencia			= a.nr_seq_perfil_web
	and	c.nr_seq_perfil_web		= b.nr_sequencia
	-- apenas regras para perfil
	and	a.nr_seq_prestador		is null
	and	c.nr_sequencia			= nr_seq_usuario_web_pc
	and	a.cd_estabelecimento		= cd_estabelecimento_pc
	-- Apenas se for para notificar no portal
	and	a.ie_solic_atual_cad		= 'S'
	-- vigencia
	and	trunc(sysdate, 'DD') between a.dt_inicio_vigencia and fim_dia(a.dt_fim_vigencia)
	union all
	select	a.nr_prazo,
		a.ie_data_referencia
	from	pls_regra_atual_cad_prest	a,
		pls_prestador_usuario_web	b
	where	b.nr_seq_prestador		= a.nr_seq_prestador
	and	b.nr_seq_usuario		= nr_seq_usuario_web_pc
	-- apenas regras para prestador
	and	a.nr_seq_perfil_web		is null
	and	a.cd_estabelecimento		= cd_estabelecimento_pc
	-- vigencia
	and	trunc(sysdate, 'DD') between a.dt_inicio_vigencia and fim_dia(a.dt_fim_vigencia)
	-- Apenas se for para notificar no portal
	and	a.ie_solic_atual_cad		= 'S'
	union all
	select	a.nr_prazo,
		a.ie_data_referencia
	from	pls_regra_atual_cad_prest	a
	-- apenas regras "Geral"
	where	a.nr_seq_perfil_web		is null
	and	a.nr_seq_prestador		is null
	and	a.cd_estabelecimento		= cd_estabelecimento_pc
	-- vigencia
	and	trunc(sysdate, 'DD') between a.dt_inicio_vigencia and fim_dia(a.dt_fim_vigencia)
	-- Apenas se for para notificar no portal
	and	a.ie_solic_atual_cad		= 'S'
	order by nr_prazo desc;
	
	
begin

-- Primeiro, verifica se existe qualquer regra, por quest�es de performance
select	count(1)
into	qt_regras_w
from	pls_regra_atual_cad_prest
where	cd_estabelecimento	= cd_estabelecimento_p;

-- se existir, prossegue com a regra
if	(qt_regras_w > 0) then

	-- Abre as regras, e deixa apenas a com prazo mais restrito
	for r_c01_w in c01(nr_seq_usuario_web_p, cd_estabelecimento_p) loop
		
		nr_dias_prazo_w 	:= r_c01_w.nr_prazo;
		ie_data_referencia_w	:= r_c01_w.ie_data_referencia;
		
	end loop; -- fim cursor regras
	
	-- Se possuir um prazo e tipo de data definido, � buscado a ultima atualiza��o de documenta��o
	-- O tipo de Data tem um tratamento especial na busca, quando for:
	--	SI	- Solicita��o / Inclus�o
	-- Ser� considerado primeiro a data de Solicita��o(SI), se n�o existir ser� ent�o avaliado a data de Inclus�o (DI)
	--	DA	- Analise
	--	DS	- Solicita��o	
	-- Ser� considerado que deve existir uma solicita��o de atualiza��o de cadastro
	-- Quando for 
	--	DI	- Inclus�o
	-- Ser� considerado a data de inclus�o dos prestadores, e n�o pode existir solicita��es de atualiza��o de cadastro.
	
	
	if	(nr_dias_prazo_w is not null) then
	
		dt_ultima_atz_w := null;
		-- Foi fracionado em IF, para respeitar a ordena��o solicitada pelo cliente
		
		-- Primeiro verifica se � data de solicita��o, analise ou solicita��o / inclus�o
		if	(ie_data_referencia_w in ('DA', 'DS', 'SI')) then
		
			select	max(t.dt_referencia)
			into	dt_ultima_atz_w
			from (	select	decode(ie_data_referencia_w, 'DA', d.dt_analise, 'DS', d.dt_atualizacao_nrec, 'SI', d.dt_atualizacao_nrec, 'DI', sysdate) dt_referencia
				from	pls_prestador_usuario_web	a,
					pls_prestador			b,
					tasy_solic_alt_campo		c,
					tasy_solic_alteracao		d
				where	b.nr_sequencia		= a.nr_seq_prestador
				-- Forma de FK j� existente na tabela, para ligar a solicita��o de altera��o no prestador, � necess�rio manter desta forma... qualquer duvida, consultar o coment�rio no cabe�alho da function
				and	((c.ds_chave_simples	= b.cd_pessoa_fisica)
					or (substr(c.ds_chave_composta,1,length(c.ds_chave_composta)-1)= 'CD_PESSOA_FISICA='||b.cd_pessoa_fisica||'#@#@IE_TIPO_COMPLEMENTO=')
					or (c.ds_chave_composta='CD_PESSOA_FISICA='||b.cd_pessoa_fisica))
				
				and	d.nr_sequencia			= c.nr_seq_solicitacao
				and     d.ie_tipo_solicitacao		= 'P'
				and	a.nr_seq_usuario		= nr_seq_usuario_web_p
				and	a.ie_situacao			= 'A'
				and	nvl(b.dt_exclusao, sysdate)	>= sysdate
				union all
				select	decode(ie_data_referencia_w, 'DA', d.dt_analise, 'DS', d.dt_atualizacao_nrec, 'SI', d.dt_atualizacao_nrec, 'DI', sysdate) dt_referencia
				from	pls_prestador_usuario_web	a,
					pls_prestador			b,
					tasy_solic_alt_campo		c,
					tasy_solic_alteracao		d
				where	b.nr_sequencia		= a.nr_seq_prestador
				-- Forma de FK j� existente na tabela, para ligar a solicita��o de altera��o no prestador, � necess�rio manter desta forma... qualquer duvida, consultar o coment�rio no cabe�alho da function
				and	((c.ds_chave_simples	= b.cd_cgc)
					or (substr(c.ds_chave_composta,1,length(c.ds_chave_composta)-1)= 'CD_CGC='||b.cd_cgc||'#@#@IE_TIPO_COMPLEMENTO=')
					or (c.ds_chave_composta='CD_CGC='||b.cd_cgc))
				
				and	d.nr_sequencia			= c.nr_seq_solicitacao
				and     d.ie_tipo_solicitacao		= 'P'
				and	a.nr_seq_usuario		= nr_seq_usuario_web_p
				and	a.ie_situacao			= 'A'
				and	nvl(b.dt_exclusao, sysdate)	>= sysdate) t;
		
		end if;
		
		-- Agora verifica se a regra � pela data de inclus�o, ou � solicita��o/ inclus�o e n�o achou nenhuma data de solicitacao
		if	((ie_data_referencia_w = 'DI') or
			 (ie_data_referencia_w = 'SI' and dt_ultima_atz_w is null)) then
		
			select	max(t.dt_referencia)
			into	dt_ultima_atz_w
			from (	-- Quando deve verificar a data de inclus�o do prestador
				select	decode(ie_data_referencia_w, 'DA', sysdate, 'DS', sysdate, 'SI', b.dt_cadastro, 'DI', b.dt_cadastro) dt_referencia
				from	pls_prestador_usuario_web	a,
					pls_prestador			b
				where	b.nr_sequencia		= a.nr_seq_prestador
				-- Faz a restri��o para o prestador elegivel n�o possuir nenhuma solicita��o de atualiza��o de dados
				and	not exists (	select	1
							from	tasy_solic_alt_campo		c,
								tasy_solic_alteracao		d
							where	d.nr_sequencia		= c.nr_seq_solicitacao
							and     d.ie_tipo_solicitacao	= 'P'
							and	((c.ds_chave_simples	= b.cd_pessoa_fisica)
								or (substr(c.ds_chave_composta,1,length(c.ds_chave_composta)-1)= 'CD_PESSOA_FISICA='||b.cd_pessoa_fisica||'#@#@IE_TIPO_COMPLEMENTO=')
								or (c.ds_chave_composta='CD_PESSOA_FISICA='||b.cd_pessoa_fisica)))
								
				and	not exists (	select	1
							from	tasy_solic_alt_campo		c,
								tasy_solic_alteracao		d
							where	d.nr_sequencia		= c.nr_seq_solicitacao
							and     d.ie_tipo_solicitacao	= 'P'
							and	((c.ds_chave_simples	= b.cd_cgc)
								or (substr(c.ds_chave_composta,1,length(c.ds_chave_composta)-1)= 'CD_CGC='||b.cd_cgc||'#@#@IE_TIPO_COMPLEMENTO=')
								or (c.ds_chave_composta='CD_CGC='||b.cd_cgc)))
				and	a.nr_seq_usuario		= nr_seq_usuario_web_p
				and	a.ie_situacao			= 'A'
				and	nvl(b.dt_exclusao, sysdate)	>= sysdate) t;
		end if;
		
		-- se a data de referencia, somada com o prazo em dias, for inferior a data atual, ent�o o retorno deve ser 'S'
		if	((trunc(dt_ultima_atz_w, 'DD') + nvl(nr_dias_prazo_w,0)) < trunc(sysdate, 'DD')) then
		
			ds_retorno_w := 'S';
		else
		
			ds_retorno_w := 'N';
		end if; 
		
	end if; -- Fim se encontrou algum prazo valido
	
end if; -- Fim existe alguma regra

return ds_retorno_w;

end pls_obter_bloq_cad_prest_web;
/
