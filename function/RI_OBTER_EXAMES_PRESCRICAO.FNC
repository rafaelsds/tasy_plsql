CREATE OR REPLACE
FUNCTION Ri_obter_Exames_Prescricao(	
					nr_prescricao_p		number,
					cd_setor_atendimento_p	number,
					ie_opcao_p			Varchar2,
					ie_quebra_linha_p		Varchar2)
					RETURN Varchar2 IS

ds_tipo_proc_w				Varchar2(40);
ds_tipo_ant_w				Varchar2(40)	:= 'xxx';
cd_procedimento_w			Number(15);
ds_procedimento_w 			procedimento.ds_procedimento%TYPE;
qt_procedimento_w			Number(8,3);
Resultado_w	 			Varchar2(2000);
Quebra_w				Varchar2(10) 	:= chr(13) || chr(10);
nr_atendimento_w			Number(10,0);
cd_convenio_w				Number(05,0);
nr_seq_conversao_w			number(10) 		:= 0;
ie_origem_proced_w			Number(10,0);
cd_item_convenio_w			varchar2(20) 	:= '';
cd_grupo_w				varchar2(10) 	:= '';
nr_sequencia_w				number(6,0);
qt_reg_w				number(5,0)	:= 0;
nr_seq_exame_w				Number(10,0);
cd_exame_w				Varchar2(20);
nm_exame_urgente_w			Varchar(80);
nm_exame_nao_urgente_w			Varchar(80);
ie_urgencia_w				Varchar(1);
qt_lengt_w				Number(10);
dt_prev_execucao_w			date;
ds_lado_w				Varchar2(60);
nr_seq_proc_interno_w			number(10,0);
cd_plano_w				varchar2(10);
ie_lado_w				varchar2(1);
cd_tipo_acomod_conv_w			number(4);
qt_idade_w				number(15,0);
cd_pessoa_fisica_w			varchar2(10);
ie_sexo_w				varchar2(1);
cd_empresa_ref_w			number(10,0);
ie_carater_inter_sus_w			varchar2(2);
nr_seq_proc_w				number(10,0);
nr_seq_proc_pacote_w			number(10,0);
nr_seq_pacote_w				number(10,0);

cursor  c01 is
	SELECT  a.nr_sequencia,
		a.cd_procedimento,
		a.ie_origem_proced,
		SUBSTR(obter_desc_prescr_proc(b.cd_procedimento, b.ie_origem_proced, a.nr_seq_proc_interno),1,240),
		a.qt_procedimento,
		a.nr_seq_exame,
		SUBSTR(obter_valor_dominio(95, b.cd_tipo_procedimento),1,40) ds_tipo_proc,
		a.ie_urgencia,
		a.dt_prev_execucao,
		substr(obter_valor_dominio(1372,a.ie_lado),1,60),
		a.ie_lado,
		a.nr_seq_proc_interno
	FROM	procedimento b,
		prescr_procedimento a
	WHERE a.cd_procedimento		= b.cd_procedimento
	AND	a.ie_origem_proced	= b.ie_origem_proced
	AND 	a.nr_prescricao		= nr_prescricao_p
	AND 	(a.cd_setor_atendimento = cd_setor_atendimento_p) 
	order by 4, 2;
BEGIN

/* Op��es
	CQDL	- C�digo/ Quantidade/ Descri��o/ Lado
	CQD	- C�digo/ Quantidade/ Descri��o
	CQDE	- C�digo/ Quantidade/ Descri��o Executados (n�o exclu�dos) 
	CQDC	- C�digo/ Quantidade/ Descri��o do convenio
	D	- Descri��o
	C	- C�digo
	TCR	- Tipo com Repeti��o
	TSR	- Tipo sem Repeti��o
	EI	- Codigo de integra��o do exame
	EU	- Exame Laborat�rio Urgente
	ENU	- Exame Laborat�rio N�o-Urgente	
	DCD	- Data prevista de execucao / Quantidade/ Descricao
	CDLCI	- C�digo / Descri��o / Lado / C�digo interno
*/
if	(nvl(ie_quebra_Linha_p,'N') <> 'S') then
	quebra_w		:= ' ';
end if;
if	(ie_opcao_p = 'CQDC') then
	select	nr_atendimento
	into	nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	select obter_convenio_atendimento(nr_atendimento_w)
	into	cd_convenio_w
	from	dual;
end if;
OPEN C01;
LOOP
FETCH C01 into	
	nr_sequencia_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	ds_procedimento_w,
	qt_procedimento_w,
	nr_seq_exame_w,
	ds_tipo_proc_w,
	ie_urgencia_w,
	dt_prev_execucao_w,
	ds_lado_w,
	ie_lado_w,
	nr_seq_proc_interno_w;
EXIT WHEN C01%NOTFOUND;
	begin
	qt_lengt_w	:= nvl(length(Resultado_w),0);
	if	(ie_opcao_p = 'CQD') then
		if	(qt_lengt_w < 1700) then
			Resultado_w	:= 	
			Resultado_w || '(' || cd_procedimento_w || ')' || '  ' ||
	               	to_char(qt_procedimento_w, '9990') || ' - ' ||
			ds_procedimento_w || quebra_w;
		end if;
	elsif	(ie_opcao_p = 'CQDL') then
		if	(qt_lengt_w < 1700) then
			Resultado_w	:= 	
			Resultado_w || '(' || cd_procedimento_w || ')' || '  ' ||
	               	to_char(qt_procedimento_w, '9990') || ' - ' ||
			ds_procedimento_w ||' - Lado:'|| ds_lado_w || quebra_w;
		end if;
	elsif	(ie_opcao_p = 'CQDE') then
		begin
		select count(*)
		into	qt_reg_w
		from 	procedimento_paciente
		where	nr_prescricao		= nr_prescricao_p
		and	nr_sequencia_prescricao	= nr_sequencia_w
		and	cd_motivo_exc_conta 	is null;		

		if	(qt_reg_w	> 0) and
			(qt_lengt_w < 1700) then
			Resultado_w	:= 	
			Resultado_w || '(' || cd_procedimento_w || ')' || '  ' ||
        	       	to_char(qt_procedimento_w, '9990') || ' - ' ||
			ds_procedimento_w || quebra_w;
		end if;
		end;

	elsif	(ie_opcao_p = 'CQDC') and
		(qt_lengt_w < 1700) then
		begin

		select 	max(cd_plano_convenio),
			nvl(max(cd_empresa),0)
		into	cd_plano_w,
			cd_empresa_ref_w
		from 	atend_categoria_convenio
		where 	nr_atendimento = nr_atendimento_w
		and 	cd_convenio	= cd_convenio_w;
		
		select 	max(cd_tipo_acomodacao)
		into	cd_tipo_acomod_conv_w
		from 	atend_categoria_convenio
		where 	nr_atendimento = nr_atendimento_w
		and 	cd_convenio = cd_convenio_w;
		
		select	max(cd_pessoa_fisica),
			max(ie_carater_inter_sus)
		into	cd_pessoa_fisica_w,
			ie_carater_inter_sus_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_w;
		
		begin
		select	max(obter_idade(dt_nascimento, nvl(dt_obito,sysdate),'DIA')),
			max(ie_sexo)
		into	qt_idade_w,
			ie_sexo_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		exception
		when others then
			qt_idade_w	:= 0;
			ie_sexo_w	:= '';
		end;
		
		nr_seq_pacote_w	:= 0;


		converte_proc_convenio(
			null,
			cd_convenio_w,null,
    			cd_procedimento_w,
			ie_origem_proced_w,
			null, null, null,dt_prev_execucao_w,
    			cd_item_convenio_w,
			cd_grupo_w,
			nr_seq_conversao_w,
			cd_setor_atendimento_p, null, nr_seq_proc_interno_w, 'A',cd_plano_w,null,0, null,
			cd_tipo_acomod_conv_w,
			qt_idade_w,
			ie_sexo_w,
			cd_empresa_ref_w,
			ie_carater_inter_sus_w,
			nr_seq_pacote_w);
		if	(nr_seq_conversao_w <> 0) then
			select nvl(ds_proc_convenio,ds_procedimento_w)
			into	ds_procedimento_w
			from conversao_Proc_convenio
			where nr_sequencia	= nr_seq_conversao_w;
		end if;
		if	(cd_item_convenio_w = '0') then
			cd_item_convenio_w	:= null;
		end if;
		Resultado_w	:= 	
		Resultado_w || '(' || nvl(cd_item_convenio_w, cd_procedimento_w) || ')' || '  ' ||
               	to_char(qt_procedimento_w, '9990') || ' - ' ||
		ds_procedimento_w || quebra_w;
		end;
	elsif	(ie_opcao_p = 'D') and
		(qt_lengt_w < 1700) then
		Resultado_w	:= Resultado_w || ds_procedimento_w || quebra_w;
	elsif	(ie_opcao_p = 'C') and
		(qt_lengt_w < 1700) then
		Resultado_w	:= Resultado_w || cd_procedimento_w || quebra_w;
	elsif	(ie_opcao_p = 'TCR') and
		(ds_tipo_proc_w is not null) and
		(qt_lengt_w < 1700) then
		Resultado_w	:= Resultado_w || ds_tipo_proc_w || quebra_w;
	elsif	(ie_opcao_p = 'TSR') and
		(ds_tipo_proc_w is not null) and
		(ds_tipo_proc_w <> ds_tipo_ant_w) and
		(qt_lengt_w < 1700) then
		Resultado_w	:= Resultado_w || ds_tipo_proc_w || quebra_w;
	elsif	(ie_opcao_p = 'EI') and
		(qt_lengt_w < 1700) then
		select	nvl(CD_EXAME_INTEGRACAO,CD_EXAME)
		into	cd_exame_w
		from	exame_laboratorio
		where	nr_seq_exame	= nr_seq_exame_w;
		Resultado_w	:= Resultado_w || cd_exame_w || quebra_w;
	elsif	(ie_opcao_p = 'EU') and
		(qt_lengt_w < 1700) then
		select 	max(a.nm_exame)
		into	nm_exame_urgente_w
		from	exame_laboratorio a
		where	a.nr_seq_exame	= nr_seq_exame_w
		and	ie_urgencia_w 	= 'S';
		if	(nm_exame_urgente_w is not null) then
			Resultado_w	:= Resultado_w || nm_exame_urgente_w || quebra_w;
		end if;
	elsif	(ie_opcao_p = 'ENU') and
		(qt_lengt_w < 1700) then	
		select 	max(a.nm_exame)
		into	nm_exame_nao_urgente_w
		from	exame_laboratorio a
		where	a.nr_seq_exame	= nr_seq_exame_w
		and	ie_urgencia_w 	= 'N';
		if	(nm_exame_nao_urgente_w is not null) then
			Resultado_w	:= Resultado_w || nm_exame_nao_urgente_w || quebra_w;
		end if;
	elsif	(ie_opcao_p = 'DQD') and
		(qt_lengt_w < 1700) then
		Resultado_w	:= 	
		Resultado_w || '(' || to_char(dt_prev_execucao_w,'dd/mm/yyyy hh24:mi:ss') || ')' || '  ' ||
               	to_char(qt_procedimento_w, '9990') || ' - ' ||
		ds_procedimento_w || quebra_w;
	elsif	(ie_opcao_p = 'CDLCI') and
		(qt_lengt_w < 1700) then
			Resultado_w	:= 	
			Resultado_w || rpad(substr('(' || cd_procedimento_w || ')' || '  ' ||	             
			ds_procedimento_w,1,40),40) || ' ' || rpad(nvl(ie_lado_w,' '),4) || ' ' || nr_seq_proc_interno_w || quebra_w;
	end if;

	ds_tipo_ant_w	:= ds_tipo_proc_w;
	end;
END LOOP;

RETURN resultado_w;

END Ri_obter_Exames_Prescricao;
/
