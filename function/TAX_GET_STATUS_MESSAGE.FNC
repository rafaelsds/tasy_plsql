create or replace 
function tax_get_status_message(sequence_p	varchar2) 
								return varchar2 is

ie_valid_certificate_w		tax_certificate.ie_valid_certificate%type;
ie_chains_generated_w		tax_certificate.ie_chains_generated%type;
ie_communication_wsi_w		tax_certificate.ie_communication_wsi%type;
ie_installed_certificate_w	tax_certificate.ie_installed_certificate%type;
ie_recorded_certificate_w	tax_certificate.ie_recorded_certificate%type;
status_text_code_w			number(10) := 0;
status_text_w				varchar2(255);

begin
	
	select 	max(ie_valid_certificate),
			max(ie_chains_generated),
			max(ie_communication_wsi),
			max(ie_installed_certificate),
			max(ie_recorded_certificate)
	into 	ie_valid_certificate_w,
			ie_chains_generated_w,
			ie_communication_wsi_w,
			ie_installed_certificate_w,
			ie_recorded_certificate_w
	from	tax_certificate
	where	nr_sequencia = sequence_p
	and		(ie_valid_certificate is null or ie_valid_certificate <> 'N')
	and 	(ie_chains_generated is null or ie_chains_generated <> 'N')
	and 	(ie_communication_wsi is null or ie_communication_wsi <> 'N')
	and 	(ie_installed_certificate is null or ie_installed_certificate <> 'N')
	and 	(ie_recorded_certificate is null or ie_recorded_certificate <> 'N');
	
	if (ie_valid_certificate_w is null) then
		status_text_code_w := 967869;
	elsif (ie_chains_generated_w is null) then
		status_text_code_w := 967871;
	elsif (ie_communication_wsi_w is null) then
		status_text_code_w := 967873;
	elsif (ie_installed_certificate_w is null) then
		status_text_code_w := 967875;
	elsif (ie_recorded_certificate_w is null) then
		status_text_code_w := 967877;
	end if;
	
	if (status_text_code_w <> 0) then 
		status_text_w := expressao_pck.obter_desc_expressao(status_text_code_w);
	end if;
	
	return status_text_w;
	
end tax_get_status_message;
/
	