create or replace
function mprev_obter_dados_tele(	nr_seq_partic_ciclo_item_p 	varchar2, 
									ie_opcao_p 					varchar2) 
									return varchar2 is
									
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar diferentes informacaes com relacao as informacoes de telemonitoramento
Usada em campos function do dicionario de dados.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: MedPrev - Telemonitoramento
[  ]  Objetos do dicionario [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:

ie_opcao_p
'AG' = obtem a data e hora da ultima sugestao de agendamento realizada.
'QT' = obtem a quantidade de tentativas realizadas para o atendimento
'DI' = obtem a data de inicio do ultimo atendimento
'DF' = obtem a data de finalizacao do ultimo atendimento
'NA' = Numero do atendimento. Retorno a nr_sequencia do ultimo atendimento
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_sequencia_w		number;
ds_retorno_w		varchar2(255);
ie_opcao_w			varchar2(3);
begin

ie_opcao_w := upper(ie_opcao_p);

--ultima sugestao de agendamento - 'AG'
if	(ie_opcao_w = 'AG')then
	select 	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	mprev_tele_sug_contato a
	where	a.nr_seq_partic_ciclo_item = nr_seq_partic_ciclo_item_p;
	
	
	select 	max(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(a.dt_sugerida, hr_sugerida)) dt_hr_sugerida
	into	ds_retorno_w
	from 	mprev_tele_sug_contato a
	where 	a.nr_seq_partic_ciclo_item = nr_seq_partic_ciclo_item_p
	and		a.nr_sequencia = nr_sequencia_w;

-- Quantidade de tentativas - 'QT'
elsif (ie_opcao_w = 'QT') then

	select	count(1)
	into 	ds_retorno_w
	from	pls_atendimento a
	where	a.nr_seq_mprev_part_ciclo_item = nr_seq_partic_ciclo_item_p
	and	not exists	(select	1
					 from	pls_evento_ocorrencia y,
						pls_atendimento_evento x
					 where	x.nr_seq_evento = y.nr_sequencia
					 and	x.nr_seq_atendimento = a.nr_sequencia
					 and	y.ie_acao_evento = '24');

-- Data de inicio do ultimo atendimento
elsif (ie_opcao_w = 'DI') then

	select	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_ultimo_contato_sucesso)
	into	ds_retorno_w
	from 	mprev_partic_ciclo_item
	where 	nr_sequencia = nr_seq_partic_ciclo_item_p;

-- Data de finalizacao do ultimo do atendimento
elsif (ie_opcao_w = 'DF') then

	select	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_execucao)
	into	ds_retorno_w
	from 	mprev_partic_ciclo_item
	where 	nr_sequencia = nr_seq_partic_ciclo_item_p;

-- Retorna a nr_sequencia do ultimo atendimento
elsif (ie_opcao_w = 'NA') then

	select	max(a.nr_sequencia)
	into	ds_retorno_w	
	from	pls_atendimento a
	where 	a.nr_seq_mprev_part_ciclo_item = nr_seq_partic_ciclo_item_p;
	
end if;

return	ds_retorno_w;

end mprev_obter_dados_tele;
/