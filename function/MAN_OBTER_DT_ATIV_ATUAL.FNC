create or replace
function man_obter_dt_ativ_atual(	nr_seq_ordem_p		number,
				nm_usuario_exec_p		varchar2)
				return date is

dt_atividade_w	date;

begin

select	max(dt_atividade)
into	dt_atividade_w
from	man_ordem_serv_ativ
where	nr_seq_ordem_serv		= nr_seq_ordem_p
and	dt_fim_atividade is null
and	nm_usuario_exec		= nm_usuario_exec_p
and	dt_atividade between trunc(sysdate) and fim_dia(sysdate);

return dt_atividade_w;

end man_obter_dt_ativ_atual;
/
