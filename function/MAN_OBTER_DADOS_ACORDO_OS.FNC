create or replace
function man_obter_dados_acordo_os 	(	nr_seq_ordem_p		number,
						ie_tipo_dado_p		varchar)
 		    	return varchar2 is

/*
NR - Numero do acordo;
DS - Descri��o do acordo;
*/

ds_retorno_w	varchar2(4000);
			
begin

if	(ie_tipo_dado_p = 'DS') then
	begin
		select	da.ds_titulo ds_acordo
		into	ds_retorno_w
		from	desenv_acordo da,
			desenv_acordo_os os
		where	os.nr_seq_ordem_servico = nr_seq_ordem_p
		and	os.nr_seq_acordo = da.nr_sequencia
		and	rownum <= 1
		order by da.nr_sequencia;
	end;
end if;

if	(ie_tipo_dado_p = 'NR') then
	begin
		select	max(da.nr_sequencia) nr_seq_acordo
		into	ds_retorno_w
		from	desenv_acordo da,
			desenv_acordo_os os
		where	os.nr_seq_ordem_servico = nr_seq_ordem_p
		and	os.nr_seq_acordo = da.nr_sequencia
		and 	rownum <= 1
		order by da.nr_sequencia;
	end;
end if;

return	ds_retorno_w;

end man_obter_dados_acordo_os;
/