create or replace
function obter_se_regra_permanencia(	cd_estabelecimento_p		number,
					cd_procedimento_p		number,
					ie_origem_proced_p		number)
 		    	return varchar2 is
			
nr_seq_forma_org_w		Number(10) := 0;
nr_seq_grupo_w			Number(10) := 0;
nr_seq_subgrupo_w		Number(10) := 0;
qt_registro_w			number(10);

begin

begin
select	nr_seq_forma_org,
	nr_seq_grupo,
	nr_seq_subgrupo
into	nr_seq_forma_org_w,
	nr_seq_grupo_w,
	nr_seq_subgrupo_w
from	sus_estrutura_procedimento_v a
where	a.cd_procedimento	= cd_procedimento_p
and	a.ie_origem_proced	= ie_origem_proced_p;
exception
	when others then
	null;
end;


select	count(*)
into	qt_registro_w
from	sus_regra_permanencia a
where	a.cd_estabelecimento	 = cd_estabelecimento_p
and	nvl(a.cd_procedimento,cd_procedimento_p) 		= cd_procedimento_p
and	((cd_procedimento is null) or (nvl(a.ie_origem_proced,ie_origem_proced_p)= ie_origem_proced_p))
and	nvl(a.nr_seq_grupo,nr_seq_grupo_w)			= nr_seq_grupo_w	
and	nvl(a.nr_seq_subgrupo,nr_seq_subgrupo_w)		= nr_seq_subgrupo_w
and	nvl(a.nr_seq_forma_org,nr_seq_forma_org_w)		= nr_seq_forma_org_w
and	ie_situacao = 'A';

if	(qt_registro_w	= 0) then
	return	'S';
else
	return 'N';
end if;

end obter_se_regra_permanencia;
/
