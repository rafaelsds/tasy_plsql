create or replace
function pls_obter_se_regra_cbo(ie_tipo_guia_p			pls_conta.ie_tipo_guia%type,
				nr_seq_prestador_p		pls_conta.nr_seq_prestador_exec%type,
				nr_seq_tipo_atendimento_p	pls_conta.nr_seq_tipo_atendimento%type)
				return varchar2 is

ie_grupo_prestador_w		Varchar2(1) := 'S';
nr_seq_regra_w			pls_regra_cbo.nr_sequencia%type;
ie_retorno_w			Varchar2(1)	:= 'N';
ie_ocorrencia_w			pls_controle_estab.ie_ocorrencia%type := 'N';
cd_estbalecimento_w		estabelecimento.cd_estabelecimento%type;

Cursor C01 (	ie_ocorrencia_pc	pls_controle_estab.ie_ocorrencia%type,
		cd_estbalecimento_pc	estabelecimento.cd_estabelecimento%type) is
	select	nr_seq_grupo_prestador,
		nr_sequencia,
		ie_opcao
	from	pls_regra_cbo
	where	((ie_tipo_guia = ie_tipo_guia_p) or (ie_tipo_guia is null))
	and	((nr_seq_tipo_atendimento = nr_seq_tipo_atendimento_p) or (nr_seq_tipo_atendimento is null))
	and	((nr_seq_prestador = nr_seq_prestador_p) or (nr_seq_prestador is null))
	and	ie_ocorrencia_pc = 'N'
	union all
	select	nr_seq_grupo_prestador,
		nr_sequencia,
		ie_opcao
	from	pls_regra_cbo
	where	((ie_tipo_guia = ie_tipo_guia_p) or (ie_tipo_guia is null))
	and	((nr_seq_tipo_atendimento = nr_seq_tipo_atendimento_p) or (nr_seq_tipo_atendimento is null))
	and	((nr_seq_prestador = nr_seq_prestador_p) or (nr_seq_prestador is null))
	and	ie_ocorrencia_pc = 'S'
	and	cd_estabelecimento = cd_estbalecimento_pc;
begin

ie_ocorrencia_w := pls_obter_se_controle_estab('GO');
cd_estbalecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

for C01_w in C01(ie_ocorrencia_w, cd_estbalecimento_w) loop
	begin
	ie_grupo_prestador_w := 'S';
	
	if	(c01_w.nr_seq_grupo_prestador is not null) then
		ie_grupo_prestador_w := pls_obter_se_grupo_prestador(nr_seq_prestador_P, c01_w.nr_seq_grupo_prestador);
	end if;
	
	if	(ie_grupo_prestador_w= 'S') and
		(c01_w.ie_opcao = 'GO')then
		nr_seq_regra_w := c01_w.nr_sequencia;
		exit;
	end if;
	end;
end loop;

if	(nr_seq_regra_w is not null) then
	ie_retorno_w := 'S';
end if;

return	ie_retorno_w;

end pls_obter_se_regra_cbo;
/
