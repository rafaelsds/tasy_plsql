create or replace 
function OBTER_PF_DESTINO
		(nm_usuario_destino_p	varchar2) return varchar2 is

cd_pessoa_fisica_w		varchar2(255);
nm_usuario_destino_w		varchar2(4000);
ds_lista_pf_w			varchar2(4000);

cursor c01 is
select	cd_pessoa_fisica
from	usuario
where	nm_usuario_destino_w like '%   ' || nm_usuario || '   %'
and	trim(nm_usuario_destino_w)	is not null;

begin

nm_usuario_destino_w	:= substr('   ' || replace(nm_usuario_destino_p, ',', '   ') || '   ',1,4000);

open c01;
loop
fetch c01 into
	cd_pessoa_fisica_w;
exit when c01%notfound;
	if	(ds_lista_pf_w is null) then
		ds_lista_pf_w	:= substr(obter_nome_pf(cd_pessoa_fisica_w),1,4000);
	else
		ds_lista_pf_w	:= substr(ds_lista_pf_w || ', ' || obter_nome_pf(cd_pessoa_fisica_w), 1,4000);
	end if;
end loop;
close c01;

return	ds_lista_pf_w;

end OBTER_PF_DESTINO;
/
