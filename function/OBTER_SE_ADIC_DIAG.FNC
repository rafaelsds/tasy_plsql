create or replace
function obter_se_adic_diag(cd_doenca_p				varchar2,
							ie_classif_doenca_p 	varchar2,
							ie_lado_p     			varchar2,
							ie_tipo_diagnostico_p	varchar2,
							nr_seq_episodio_p  		number,
							ie_diag_tratamento_p	varchar2 default 'N',
							ie_diag_referencia_p	varchar2 default 'N') 	

return varchar2 is

		ie_permite_diag_w varchar2(1);	

begin	

	select 	nvl(max('N'),'S')
	into	ie_permite_diag_w
	from 	diagnostico_doenca
	where	nvl(cd_doenca,0) = nvl(cd_doenca_p, 0)
	and 	nvl(ie_classificacao_doenca, 0) = nvl(ie_classif_doenca_p,  0)
	and 	nvl(ie_lado,0) = nvl(ie_lado_p,0)
	and 	nvl(ie_tipo_diagnostico, 0) = nvl(ie_tipo_diagnostico_p,  0)
	and		dt_liberacao is not null
	and		dt_inativacao is null
	and		((ie_diag_tratamento_p = 'S' and IE_DIAG_TRATAMENTO = 'S') or (ie_diag_referencia_p = 'S' and ie_diag_referencia  = 'S') or (ie_diag_tratamento_p = 'N' and ie_diag_referencia_p = 'N') )
	and 	nr_atendimento in (	select 	a.nr_atendimento 
								from   	atendimento_paciente a, 
										episodio_paciente b 
								where  	a.nr_Seq_episodio = b.nr_Sequencia 
								and 	b.nr_sequencia = nr_seq_episodio_p);

return ie_permite_diag_w;

end obter_se_adic_diag;
/
