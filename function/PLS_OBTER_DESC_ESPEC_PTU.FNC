create or replace
function pls_obter_desc_espec_ptu(	cd_ptu_p		number)
					return Varchar2 is
				
ds_retorno_w		varchar2(255);
qt_registro_w		number(10) := 0;

begin
if	(cd_ptu_p is not null) then
	select	count(*)
	into	qt_registro_w
	from	especialidade_medica
	where	cd_ptu = cd_ptu_p;

	select	substr(max(ds_especialidade),1,255)
	into	ds_retorno_w
	from	especialidade_medica
	where	cd_ptu	= cd_ptu_p;
end if;

/* Se houver mais de uma especialidade com mesmo c�digo PTU, ent�o informar aquele que esta com o IE_PTU_A500 marcado,
     sen�o continua buscando pelo MAX da descri��o da especialidade. */  
if	(qt_registro_w > 1) then
	select	nvl(substr(max(ds_especialidade),1,255),ds_retorno_w)
	into	ds_retorno_w
	from	especialidade_medica
	where	cd_ptu		= cd_ptu_p
	and	ie_ptu_a500	= 'S';
end if;

return	ds_retorno_w;

end pls_obter_desc_espec_ptu;
/