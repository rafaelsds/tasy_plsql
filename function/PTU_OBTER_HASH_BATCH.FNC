create or replace
function ptu_obter_hash_batch
			(	nr_seq_projeto_p	xml_projeto.nr_sequencia%type)
				return varchar2 is

ie_retorno_w	varchar2(5) := 'N';

begin
-- Adicionado o projeto XML do arquivo A510/A520
if	(nr_seq_projeto_p in (101666,102337,102348,102589,102594,102592, 102900, 102897, 102904, 102641, 102933, 102747, 102837)) then
	ie_retorno_w := 'S';
end if;

return	ie_retorno_w;

end ptu_obter_hash_batch;
/
