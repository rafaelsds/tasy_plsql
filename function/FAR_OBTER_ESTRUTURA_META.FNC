create or replace
function	far_obter_estrutura_meta	(nr_sequencia_p		number)
				return varchar2 is

ds_retorno_w		varchar2(255);
cd_material_w		number(10);
cd_classe_w		number(5);
cd_subgrupo_w		number(3);
cd_grupo_w		number(3);

begin

select	wheb_mensagem_pck.get_Texto(314167), /*'Todos os materiais/medicamentos',*/
	nvl(cd_material,0),
	nvl(cd_classe_material,0),
	nvl(cd_subgrupo_material,0),
	nvl(cd_grupo_material,0)
into	ds_retorno_w,
	cd_material_w,
	cd_classe_w,
	cd_subgrupo_w,
	cd_grupo_w
from	far_meta_estrutura
where	nr_sequencia = nr_sequencia_p;

if 	(cd_material_w <> 0) then
	begin
	select	substr(ds_material,1,255)
	into	ds_retorno_w
	from	estrutura_material_v
	where 	cd_material = cd_material_w;
	end;
elsif	(cd_classe_w <> 0) then
	begin
	select	substr(ds_classe_material,1,255)
	into	ds_retorno_w
	from	classe_material
	where	cd_classe_material = cd_classe_w;
	end;
elsif	(cd_subgrupo_w <> 0) then
	begin
	select	substr(ds_subgrupo_material,1,255)
	into	ds_retorno_w
	from	subgrupo_material
	where	cd_subgrupo_material = cd_subgrupo_w;
	end;
elsif	(cd_grupo_w <> 0) then
	begin
	select	substr(ds_grupo_material,1,255)
	into	ds_retorno_w
	from	grupo_material
	where	cd_grupo_material = cd_grupo_w;
	end;
end if;

return ds_retorno_w;

end far_obter_estrutura_meta;
/
