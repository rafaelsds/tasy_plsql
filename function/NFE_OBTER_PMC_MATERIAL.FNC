create or replace
function nfe_obter_pmc_material(cd_estabelecimento_p		number,
				dt_periodo_p			date,
				cd_material_p			number,
				ie_lista_material_p		varchar2,
				nr_seq_classif_p		number)
 		    	return varchar2 is
ds_retorno_w	varchar2(255);
lista_pos_w	varchar2(100);
lista_neg_w	varchar2(100);
lista_neu_w	varchar2(100);
preco_custo_w	number(10,2);
preco_total_w	number(15,2);


begin

select 	nvl(pr_lista_positiva,0),
	nvl(pr_lista_negativa,0),
	nvl(pr_lista_neutra,0)
into	lista_pos_w,
	lista_neg_w,
	lista_neu_w
from	lista_ncm
where	nr_sequencia = nr_seq_classif_p;

preco_custo_w := obter_custo_medio_material(cd_estabelecimento_p,dt_periodo_p,cd_material_p);

if (ie_lista_material_p = '0') then
	preco_total_w	:= (preco_custo_w + (preco_custo_w * lista_pos_w)/100);
elsif (ie_lista_material_p = '1') then
	preco_total_w	:= (preco_custo_w * (preco_custo_w * lista_neg_w)/100);
elsif (ie_lista_material_p = '2') then
	preco_total_w	:= (preco_custo_w * (preco_custo_w * lista_neu_w)/100);
end if;	

ds_retorno_w	:= to_char(preco_total_w,'FM99990.0099');

return	ds_retorno_w;

end nfe_obter_pmc_material;
/
