create or replace
function Obter_UM_comum(nr_seq_ficha_p		number)
 		    	return varchar2 is

cd_unid_medida_comum_w	varchar2(2000) := null;
qt_materiais_w	number(10);
qt_materiais_ww number(10);
cd_unid_medida_w	varchar2(30);
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;

cursor c01 is
SELECT 	COUNT(*),
	a.cd_unidade_medida_consumo
FROM(	SELECT 	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
		cd_material
	FROM	material
	WHERE 	cd_material IN	(SELECT	d.cd_material
				FROM	material d
				WHERE	d.nr_seq_ficha_tecnica = nr_seq_ficha_p)
	UNION
	SELECT	cd_unidade_medida,
			cd_material
	FROM	material_conversao_unidade
	WHERE	cd_material IN (SELECT	d.cd_material
				FROM	material d
				WHERE	d.nr_seq_ficha_tecnica = nr_seq_ficha_p)
	--GROUP BY cd_unidade_medida,
	--		cd_material
	ORDER BY 2	
	) a
GROUP BY a.cd_unidade_medida_consumo
having count(*) = qt_materiais_w
ORDER BY 1 DESC;

begin

if	(nr_seq_ficha_p is not null) then
	begin
	
	select	count(*) 
	into 	qt_materiais_w
	from 	material 
	where 	nr_seq_ficha_tecnica = nr_seq_ficha_p;
	
	open C01;
	loop
	fetch C01 into	
		qt_materiais_ww,
		cd_unid_medida_w;
	exit when C01%notfound;
		begin
		cd_unid_medida_comum_w := cd_unid_medida_comum_w || cd_unid_medida_w || '#0' ;
		end;
	end loop;
	close C01;
	
	end;
end if;

return	cd_unid_medida_comum_w;

end Obter_UM_comum;
/