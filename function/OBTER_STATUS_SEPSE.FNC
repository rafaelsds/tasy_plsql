create or replace
function obter_status_sepse(	nr_seq_escala_p		number)
 		    	return varchar2 is

qt_reg_deflagrador_w	number(10);
ie_status_w				varchar2(3);
qt_geral_w				number(10);
ie_versao_sepse_w       varchar2(10);

begin

select nvl(ie_versao_sepse,'1')
into   ie_versao_sepse_w
from   parametro_medico
where  cd_estabelecimento = obter_estabelecimento_ativo;
		
select	count(*)
into	qt_reg_deflagrador_w
from	escala_sepse_item
where	nr_seq_escala = nr_seq_escala_p
and		ie_resultado = 'S'
and		((ie_versao_sepse_w = '1' and nr_seq_atributo in (8,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26))
or		(ie_versao_sepse_w = '2' and nr_seq_atributo in (41,42,51,59,60,61,62,65,66,72,76,77,95,102,103)));

select	count(*)
into	qt_geral_w
from	escala_sepse_item
where	nr_seq_escala = nr_seq_escala_p
and		ie_resultado = 'S'
and		((ie_versao_sepse_w = '1' and nr_seq_atributo in (4,5,6,7,8,9,11,17))
or		(ie_versao_sepse_w = '2' and nr_seq_atributo in (47,48,49,50,54,55,56,75)));


if	(qt_reg_deflagrador_w > 0) then
	ie_status_w := 'RC';
elsif (qt_geral_w > 0) then
	ie_status_w := 'SD';
else
	ie_status_w := 'RD';
end if;
	

return ie_status_w;

end obter_status_sepse;
/
