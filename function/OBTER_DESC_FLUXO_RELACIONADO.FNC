CREATE OR REPLACE
FUNCTION obter_desc_fluxo_relacionado (nr_seq_fluxo_relacionado_p NUMBER)
						RETURN VARCHAR2 IS

ds_retorno_w		VARCHAR2(255);

BEGIN
IF (nr_seq_fluxo_relacionado_p IS NOT NULL)  THEN
	SELECT	nm_fluxograma
	INTO	ds_retorno_w
	FROM	manchester_fluxograma
	WHERE	nr_sequencia = nr_seq_fluxo_relacionado_p;
END IF;

RETURN ds_retorno_w;

END obter_desc_fluxo_relacionado;
/