create or replace
function obter_minutos_hora(	ds_hora_p varchar2)
				return number is

qt_minutos_w	number(5,0) := null;
				
begin

if	(ds_hora_p is not null) then
	qt_minutos_w := (campo_numerico(substr(lpad(ds_hora_p,5,'0'),1,2)) * 60)  + (campo_numerico(substr(lpad(ds_hora_p,5,'0'),4,5)));
end if;

return	qt_minutos_w;

end obter_minutos_hora;
/