create or replace
function obter_desc_item_grupo_ageint (nr_seq_grupo_p			number,
										nm_curto_p				varchar2,
										ds_proc_exame_p			varchar2,
										cd_pessoa_fisica_p		varchar2,
										ds_especialidade_P		varchar2,
										ds_grupo_proc_p			varchar2,
										cd_convenio_p			number,
										cd_categoria_p			varchar2,
										cd_plano_p				varchar2,
										nr_seq_proc_interno_p	number,
										cd_estabelecimento_p	number,
										qt_idade_p				number,
										ie_tipo_atendimento_p	number,
										ie_sexo_p				varchar2,
										nr_seq_ageint_p			number,
										dt_nascimento_p			date,
										nm_usuario_p			varchar2,
										dt_agendamento_p		date,
										cd_usuario_convenio_p	varchar2,
										cd_medico_p				varchar2,
										cd_especialidade_p		number,
										ie_buscar_valor_p	varchar2 default 'N')
 		    	return varchar2 is

ie_tipo_agendamento_w	agenda_int_grupo.ie_tipo_agendamento%type;
vl_total_w				varchar2(30);
vl_item_w				number(15,2) := 0;
vl_item_assoc_w			number(15,2) := 0;
ie_tipo_convenio_w		convenio.ie_tipo_convenio%type;
ds_item_w				varchar2(255);
nr_seq_proc_interno_w	proc_int_proc_prescr.nr_seq_proc_int_adic%type;
ie_tipo_item_w			agenda_int_grupo.ie_tipo_agendamento%type; /*Corrigido para pegar o tamanho do campo da tabela*/
ie_apres_valor_exame_w	varchar2(1);
ie_apres_valor_cons_w	varchar2(1);
ie_trazer_valor_w	varchar2(1) := 'N';

--Vetor usado na ageint_gerar_proc_Assoc;
cursor c01 is
	select	nvl(nr_seq_proc_int_adic,0)
	from	proc_int_proc_prescr
	where	nr_seq_proc_interno	= nr_seq_proc_interno_p
	and 	nvl(cd_convenio,cd_convenio_p) = cd_convenio_p
	and		(cd_convenio_excluir is null or cd_convenio_excluir <> cd_convenio_p)
	and		Obter_conv_excluir_proc_assoc(nr_sequencia, cd_convenio_p)	= 'S'
	and		nvl(ie_somente_agenda_cir,'N') = 'N'
	and		(((qt_idade_p is null) or (qt_idade_min is null and qt_idade_max is null)) or
			((qt_idade_p is not null) and (qt_idade_p between nvl(qt_idade_min,qt_idade_p) and
			nvl(qt_idade_max,qt_idade_p))))
	and 	Obter_se_proc_interno_ativo(nr_seq_proc_int_adic) = 'A';

begin

select	max(y.ie_tipo_agendamento)
into	ie_tipo_item_w
from	agenda_int_grupo y
where	y.nr_sequencia = nr_seq_grupo_p;

select 	max( ie_apres_valor_exame ),
	max( ie_apres_valor_cons )
into	ie_apres_valor_exame_w,
	ie_apres_valor_cons_w
from 	parametro_agenda_integrada
where 	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

select 	max(ie_tipo_convenio) 
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio = cd_convenio_p;

if (ie_tipo_convenio_w = 1) and
	(((ie_tipo_item_w = 'C') and ( ie_apres_valor_cons_w = 'S' )) or
	((ie_tipo_item_w = 'E') and ( ie_apres_valor_exame_w = 'S' ))) then
        --Obter valor caso seja particular
	ie_trazer_valor_w := 'S';
	if (ie_buscar_valor_p = 'S') then
		vl_item_w := ageint_obter_valor_item(	
											cd_convenio_p,
											cd_categoria_p, 
											cd_plano_p,
											nr_seq_proc_interno_p,
											cd_estabelecimento_p, 
											qt_idade_p, 
											ie_tipo_atendimento_p, 
											ie_sexo_p, 
											nr_seq_ageint_p, 
											NULL, 
											NULL,
											dt_nascimento_p, 
											nm_usuario_p, 
											NULL, 
											dt_agendamento_p, 
											cd_usuario_convenio_p, 
											cd_medico_p,
											cd_especialidade_p);
												
												
		--Busca valor de itens associados.
		open c01;
		loop
		fetch c01 into
			nr_seq_proc_interno_w;
		exit when c01%notfound;
			begin
				vl_item_assoc_w := vl_item_assoc_w + ageint_obter_valor_item(	
													cd_convenio_p,
													cd_categoria_p, 
													cd_plano_p,
													nr_seq_proc_interno_w,
													cd_estabelecimento_p, 
													qt_idade_p, 
													ie_tipo_atendimento_p, 
													ie_sexo_p, 
													nr_seq_ageint_p, 
													NULL, 
													NULL,
													dt_nascimento_p, 
													nm_usuario_p, 
													NULL, 
													dt_agendamento_p, 
													cd_usuario_convenio_p, 
													cd_medico_p,
													cd_especialidade_p);
			
			end;
		end loop;
		close c01;
		
		vl_total_w := TO_CHAR(vl_item_w + vl_item_assoc_w,'999g999g999g999g990d00');
	end if;
else
	vl_total_w := null;
end if;

--Monta a descri��o do item
select	max(ie_tipo_agendamento)
into	ie_tipo_agendamento_w
from	agenda_int_grupo
where	nr_sequencia = nr_seq_grupo_p;

if (nm_curto_p is not null) then
	ds_item_w := nm_curto_p;
elsif 	((ie_tipo_agendamento_w <> 'C') and 
		(ds_proc_exame_p is not null)) then
	ds_item_w := ds_proc_exame_p;
elsif (cd_pessoa_fisica_p is not null) then
	ds_item_w := obter_nome_pf(cd_pessoa_fisica_p);
elsif (ds_especialidade_p is not null) then
	ds_item_w := ds_especialidade_p;
else 
	ds_item_w := ds_grupo_proc_p;
end if;

if (ie_trazer_valor_w = 'S') then
	if (ie_buscar_valor_p = 'S') then
		if 	(vl_item_w is not null) then
			ds_item_w := substr(ds_item_w || ' - '|| wheb_mensagem_pck.get_texto(1034795) ||' '|| vl_total_w,1,255);
		end if;
	else
		ds_item_w := substr(ds_item_w || ' - '|| wheb_mensagem_pck.get_texto(1034795) || ' $$v',1,255);
	end if;
end if;

return	ds_item_w;

end obter_desc_item_grupo_ageint;
/
