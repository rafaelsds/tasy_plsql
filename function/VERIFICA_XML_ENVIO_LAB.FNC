create or replace
function Verifica_XML_Envio_Lab(nr_lote_ext_p number)
 		    	return number is 
  nr_seq_xml_lab_envio_w number(10);

begin

select max(nr_seq_xml_lab_envio)
  into nr_seq_xml_lab_envio_w
  from pessoa_juridica_estab pje
  inner join lab_lote_externo lle on lle.cd_cgc = pje.cd_cgc and lle.cd_estabelecimento = pje.cd_estabelecimento
  inner join pessoa_juridica pj on pj.cd_cgc = lle.cd_cgc
    where lle.nr_sequencia = nr_lote_ext_p;      
        
return	nr_seq_xml_lab_envio_w;

end Verifica_XML_Envio_Lab;
/