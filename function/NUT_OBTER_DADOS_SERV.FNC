create or replace
function nut_obter_dados_serv	(dt_servico_p		date,
				nr_Atendimento_p		number,
				cd_setor_atendimento_p	number,
				nr_seq_servico_p	number,
				ie_opcao_p		varchar2)
 			    	return varchar2 is

ds_resultado_w		varchar2(255);
nr_prescricao_w		number(10);

begin

SELECT	MAX(b.nr_prescricao)
INTO	ds_resultado_w
FROM  	nut_atend_serv_dia a,
	prescr_medica b
WHERE	a.nr_Atendimento = b.nr_atendimento
AND	nvl(b.dt_liberacao,b.dt_liberacao_medico) IS NOT NULL
AND	a.dt_Servico BETWEEN b.dt_inicio_prescr AND b.dt_validade_prescr
and	b.dT_suspensao is null
AND	a.nr_Seq_Servico = nr_Seq_Servico_p
AND	a.nr_atendimento = nr_Atendimento_p
AND	a.cd_setor_Atendimento = cd_setor_Atendimento_p
AND	a.dt_servico BETWEEN TRUNC(dt_servico_p) AND TRUNC(dt_servico_p) + 86399/86400
AND	EXISTS(	SELECT	1
		FROM  	prescr_dieta x
		WHERE	NVL(x.ie_suspenso,'N') = 'N'
		AND	x.nr_prescricao =  b.nr_prescricao
		UNION
		SELECT	1
		FROM	prescr_material x
		WHERE	x.ie_agrupador IN(8,12)
		AND	NVL(x.ie_suspenso,'N') = 'N'
		AND	x.nr_prescricao =  b.nr_prescricao);


if (ie_opcao_p = 'D') then

	select	max(dt_prescricao)
	into	ds_resultado_w
	from	prescr_medica
	where	nr_prescricao = ds_resultado_w;
	
elsif (ie_opcao_p = 'MJ') then

	select	substr(obter_nome_pf(max(cd_medico)),1,100)
	into	ds_resultado_w
	from	prescr_medica
	where	nr_prescricao = ds_resultado_w;
end if;
		

return	ds_resultado_w;

end nut_obter_dados_serv;
/