create or replace
function obter_schematic_path(	nr_seq_obj_schematic_p	number,
				nr_seq_opcao_mouse_p	number default null)
				return varchar2 is

ds_path_w			varchar2(2000);
ds_objeto_w			varchar2(2000);
ie_opcao_mouse_w		varchar2(1);
ie_filho_tab_w			varchar2(1) := 'N';
ie_wdlg_entrada_w		varchar2(1);

nr_seq_obj_superior_w		objeto_schematic.nr_seq_obj_sup%type;
ie_tipo_objeto_w		objeto_schematic.ie_tipo_objeto%type;
ie_tipo_componente_w		objeto_schematic.ie_tipo_componente%type;
ie_componente_pai_w		objeto_schematic.ie_tipo_componente%type;
nr_seq_obj_schematic_w		objeto_schematic.nr_sequencia%type;
nr_seq_dic_objeto_wdlg_w	objeto_schematic.nr_seq_dic_objeto%type;
nr_seq_obj_schem_wdlg_w		objeto_schematic.nr_sequencia%type;
nr_seq_dic_objeto_w		objeto_schematic.nr_seq_dic_objeto%type;
nr_seq_visao_w			objeto_schematic.nr_seq_visao%type;
ds_menu_item_w			dic_expressao.ds_expressao_br%type;
nr_seq_opcao_mouse_w		dic_objeto.nr_sequencia%type;
nr_seq_mi_sup_w			dic_objeto.nr_seq_mi_sup%type;


	procedure carregar_valores_objeto(nr_sequencia_p	number) is
	begin
	select	max(nr_seq_obj_sup),
		nvl(max(obter_desc_expressao(cd_exp_desc_obj, '')), max(ds_objeto)),
		max(ie_tipo_objeto),
		max(ie_tipo_componente),
		max(nr_seq_dic_objeto),
		max(nr_seq_visao)
	into	nr_seq_obj_superior_w,
		ds_objeto_w,
		ie_tipo_objeto_w,
		ie_tipo_componente_w,
		nr_seq_dic_objeto_w,
		nr_seq_visao_w
	from	objeto_schematic
	where	nr_sequencia = nr_sequencia_p;
	end;
	
	procedure carrega_opcao_mouse(nr_sequencia_p	number) is
	begin
	
	select	max(nr_seq_obj_sup),
		max(nr_seq_mi_sup),
		max(obter_desc_expressao(cd_exp_texto, ''))
	into	nr_seq_obj_superior_w,
		nr_seq_mi_sup_w,
		ds_menu_item_w
	from	dic_objeto
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ds_menu_item_w is not null) then
		if	(ds_path_w is null) then
			if	(nr_seq_mi_sup_w is not null) then
				ds_path_w := ds_menu_item_w;
			else
				ds_path_w := obter_desc_expressao(491786, '') || ' ' || ds_menu_item_w;
			end if;
		else
			ds_path_w := obter_desc_expressao(491786, '') || ' ' || ds_menu_item_w || ' >> ' || ds_path_w;
		end if;
	end if;
	end;

begin

ds_path_w := '';
nr_seq_obj_schematic_w := nr_seq_obj_schematic_p;
nr_seq_opcao_mouse_w := nr_seq_opcao_mouse_p;
if	(nr_seq_opcao_mouse_p is not null) then
	ie_opcao_mouse_w := 'S';
else
	ie_opcao_mouse_w := 'N';
end if;

if	(ie_opcao_mouse_w = 'N') then
	carregar_valores_objeto(nr_seq_obj_schematic_w);

	ie_wdlg_entrada_w := 'N';
	-- se for um WDLG, a function tenta descobrir onde ele � chamado
	if	(ie_tipo_componente_w = 'WDLG') then
		--ds_path_w := ds_objeto_w;

		ie_wdlg_entrada_w := 'S';

		select	max(b.nr_seq_dic_objeto),
			max(b.nr_seq_objeto)
		into	nr_seq_dic_objeto_wdlg_w,
			nr_seq_obj_schem_wdlg_w
		from	obj_schematic_evento_acao a,
			OBJ_SCHEMATIC_EVENTO b
		where	a.nr_seq_obj_wdlg = nr_seq_obj_schematic_w
		and	b.nr_sequencia = a.nr_seq_obj_evento;

		if	(nr_seq_dic_objeto_wdlg_w is not null) then
			nr_seq_opcao_mouse_w := nr_seq_dic_objeto_wdlg_w;
			nr_seq_obj_schematic_w := null;
			ie_opcao_mouse_w := 'S'; -- para entrar no if abaixo		
		elsif	(nr_seq_obj_schem_wdlg_w is not null) then
			nr_seq_obj_schematic_w := nr_seq_obj_schem_wdlg_w;

			carregar_valores_objeto(nr_seq_obj_schematic_w);
		end if;
	end if;
end if;

-- Se for op��o de mouse
if	(ie_opcao_mouse_w = 'S') then

	carrega_opcao_mouse(nr_seq_opcao_mouse_w);

	if	(nr_seq_mi_sup_w is not null) then
		carrega_opcao_mouse(nr_seq_mi_sup_w);
	end if;

	if	(nr_seq_obj_superior_w is not null) then

		if	(nr_seq_obj_schematic_w is null) then
			select	max(nr_sequencia)
			into	nr_seq_obj_schematic_w
			from	objeto_schematic
			where	nr_seq_dic_objeto = nr_seq_obj_superior_w;
		end if;

		if	(nr_seq_obj_schematic_w is not null) then
			carregar_valores_objeto(nr_seq_obj_schematic_w);
		end if;

	end if;	
end if;

while 	(nr_seq_obj_superior_w is not null) loop 

	if	(ie_tipo_componente_w = 'WPOPUP') then
		null; --ds_objeto_w := obter_desc_expressao(309655, '');
	elsif	(ie_tipo_componente_w = 'WDBP') then

		select	max(obter_desc_expressao(a.cd_exp_titulo, ''))
		into	ds_objeto_w
		from	tabela_visao a
		where	a.nr_sequencia = nr_seq_visao_w;		

	elsif	(ie_tipo_componente_w in ('WCP', 'WDF')) then

		select	max(obter_desc_expressao(a.cd_exp_texto, ds_objeto_w))
		into	ds_objeto_w
		from	dic_objeto a
		where	a.nr_sequencia = nr_seq_dic_objeto_w;

	elsif	(ie_tipo_componente_w = 'WDLG') then

		select	max(obter_desc_expressao(cd_exp_desc_obj, ''))
		into	ds_objeto_w
		from	objeto_schematic
		where	nr_sequencia = nr_seq_obj_superior_w;

	elsif	(ie_tipo_componente_w = 'WF') then

		ds_objeto_w := obter_desc_expressao(290098, '');

	end if;

	if	(nvl(ie_tipo_objeto_w, 'X') <>  'MR') and
		(nvl(ie_tipo_objeto_w, 'X') <>  'R') and
		(nvl(ie_tipo_objeto_w, 'X') <>  'P') and
		(nvl(ie_tipo_objeto_w, 'X') <>  'SCH') and
		(nvl(ie_tipo_componente_w, 'X') <>  'WP') and
		(nvl(ie_tipo_componente_w, 'X') <>  'WPOPUP') and
		(ds_objeto_w is not null) then

		if	(ds_path_w is null) then
			ds_path_w := substr(ds_objeto_w, 1, 2000);
		elsif	(ie_filho_tab_w = 'S') then
			ds_path_w := substr(ds_objeto_w || ' ' || ds_path_w, 1, 2000);
		else
			ds_path_w := substr(ds_objeto_w || '  >  ' || ds_path_w, 1, 2000);
		end if;

	end if;

	select	max(ie_tipo_componente)
	into	ie_componente_pai_w
	from	objeto_schematic
	where	nr_sequencia = nr_seq_obj_superior_w;

	if	(ie_componente_pai_w = 'WDLG') and
		(ie_wdlg_entrada_w = 'N') then
		ds_path_w := substr(obter_schematic_path(nr_seq_obj_superior_w) || '  >  ' || ds_path_w, 1, 2000);
	end if;

	if	(nvl(ie_tipo_objeto_w, 'X') in ('T','IT')) then
		ie_filho_tab_w := 'S';
	else
		ie_filho_tab_w := 'N';
	end if;

	carregar_valores_objeto(nr_seq_obj_superior_w);

end loop;

return	ds_path_w;

end obter_schematic_path;
/