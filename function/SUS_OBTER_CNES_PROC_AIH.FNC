create or replace 
function sus_obter_cnes_proc_aih (	cd_procedimento_p	number,
					ie_origem_proced_p	number,
					cd_estabelecimento_p	number)
					return varchar2 is

ds_retorno_w		varchar2(10) := '';
qt_registro_w		number(20);
nr_seq_grupo_w		number(10);
nr_seq_subgrupo_w	number(10);
nr_seq_forma_org_w	number(10);

cursor regra_cnes is
        select	nvl(cd_cnes,'') cd_cnes
        from	sus_regra_cnes_proc_aih
        where	(nvl(nr_seq_grupo, nr_seq_grupo_w)			= nr_seq_grupo_w)
        and	(nvl(nr_seq_subgrupo, nr_seq_subgrupo_w)		= nr_seq_subgrupo_w)
        and	(nvl(nr_seq_forma_org, nr_seq_forma_org_w)		= nr_seq_forma_org_w)
        and	(nvl(cd_procedimento, cd_procedimento_p)		= cd_procedimento_p)
        and	(nvl(ie_origem_proced, ie_origem_proced_p)		= ie_origem_proced_p)
        and	cd_estabelecimento	= cd_estabelecimento_p
        and	ie_situacao		= 'A'
        order by
                nvl(cd_procedimento, 0),
                nvl(nr_seq_forma_org, 0),
                nvl(nr_seq_subgrupo, 0),
                nvl(nr_seq_grupo, 0);

regra_cnes_w    regra_cnes%rowtype;

begin

/* Obter a estrutura do procedimento*/
begin
select	nr_seq_grupo,
	nr_seq_subgrupo,
	nr_seq_forma_org
into	nr_seq_grupo_w,
	nr_seq_subgrupo_w,
	nr_seq_forma_org_w
from	sus_estrutura_procedimento_v
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
exception
	when others then
	nr_seq_grupo_w		:= 0;
	nr_seq_subgrupo_w	:= 0;
	nr_seq_forma_org_w	:= 0;
end;

open regra_cnes;
loop
fetch   regra_cnes into	
        regra_cnes_w;
exit when regra_cnes%notfound;
        begin
        ds_retorno_w := regra_cnes_w.cd_cnes;
        end;        
end loop;
close regra_cnes;	

return	ds_retorno_w;

end sus_obter_cnes_proc_aih;
/