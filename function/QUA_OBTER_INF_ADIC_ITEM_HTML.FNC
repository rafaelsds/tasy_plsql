create or replace
function qua_obter_inf_adic_item_html(	nr_seq_evento_p		number,
				nr_seq_item_p			number,
				ie_tipo_campo_p			varchar2)
				return varchar2 is

ds_retorno_w	varchar2(2000);

begin


select	max(decode(ie_tipo_campo_p,'N',to_char(nr_inf_adic),'A',ds_inf_adic,'D',dt_inf_adic))
into	ds_retorno_w
from	QUA_EV_PAC_INF_ADIC a,
		QUA_EVENTO_INF_ADIC b
where	a.nr_seq_evento	= nr_seq_evento_p
and	   a.NR_SEQ_INF_ADIC	= nr_seq_item_p
and	b.nr_sequencia = a.NR_SEQ_INF_ADIC;

return		ds_retorno_w;

end qua_obter_inf_adic_item_html;
/