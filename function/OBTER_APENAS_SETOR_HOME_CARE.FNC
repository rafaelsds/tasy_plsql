create or replace
function obter_apenas_setor_home_care	(	cd_setor_atendimento_p	number,
							nr_atendimento_p	number,
							ie_tipo_p		number)
 		    	return varchar2 is
ds_retorno_w			varchar2(1) := 'S';
cd_classif_setor_atend_w	number(15);
cd_classif_setor_w		varchar2(5);
ie_transf_entre_setor_hc_w	varchar2(1);
begin
obter_param_usuario(3111, 265, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_transf_entre_setor_hc_w);
if 	(nvl(cd_setor_atendimento_p,0) > 0)	and
	(nvl(nr_atendimento_p,0) > 0) 		and
	(nvl(ie_tipo_p,0) > 0) 			and
	(ie_transf_entre_setor_hc_w = 'S')	then

	select	max(obter_classif_setor(obter_setor_atendimento(nr_atendimento_p)))
	into	cd_classif_setor_atend_w
	from	dual;
	
	if (nvl(cd_classif_setor_atend_w,0) = 8) then
	
		select	max(cd_classif_setor)
		into	cd_classif_setor_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_p;
		ds_retorno_w := 'N';	
		if (cd_classif_setor_w = 8) then
			
			ds_retorno_w := 'S';
			
		end if;
	end if;
end if;

return	ds_retorno_w;

end obter_apenas_setor_home_care;
/