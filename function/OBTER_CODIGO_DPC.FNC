create or replace
function obter_codigo_dpc	(nr_atendimento_p	number,
        dt_referencia_p patient_dpc.dt_start_dpc%type default null)
				return varchar2 is
					
cd_dpc_w varchar2(300);

begin
select max(b.cd_dpc || '-' || b.nm_dpc)
into cd_dpc_w
from patient_dpc a, dpc_score b
where b.nr_sequencia = a.nr_seq_dpc_score
and a.nr_atendimento = nr_atendimento_p
and (dt_referencia_p is null or (trunc(dt_referencia_p,'dd') between trunc(a.dt_start_dpc,'dd') and nvl(trunc(a.dt_end_dpc,'dd'),trunc(dt_referencia_p,'dd'))));

return cd_dpc_w;

end obter_codigo_dpc;
/
