CREATE OR REPLACE
FUNCTION Obter_Quartis_Relatorio
			(dt_inicial_p	date,
			 dt_final_p	date,
			 ie_opcao_p	varchar,
			 ie_tipo_p	varchar)
			 return number is

/*	
ie_opcao_p
 MD - Matrícula / Diagnóstico
 DT - Diagnóstico / Tratamento
 MT - Matrícula / Tratamento
*/


/*
ie_tipo_p
 P - Primeiro quartis
 T - Terceiro quartis
*/


vl_primeiro_w	number(10);
vl_terceiro_w	number(10);
vl_result_w	number(10);


BEGIN

if	(ie_opcao_p = 'MD') then

	select	dividir((25 * count(trunc((dt_preench_ficha) - (dt_diagnostico)))), 100) vl_primeiro,
		dividir((75 * count(trunc((dt_preench_ficha) - (dt_diagnostico)))), 100) vl_terceiro
	into	vl_primeiro_w,
		vl_terceiro_w
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_p;

	if	(ie_tipo_p = 'P') then
	
		select	max(qt_item)
		into	vl_result_w
		from	(select	trunc((dt_preench_ficha) - (dt_diagnostico)) qt_item
		 	from	can_ficha_admissao
		 	where	dt_diagnostico between dt_inicial_p and dt_final_p
		 	order by 1)
		where	rownum <= vl_primeiro_w;
	
	elsif	(ie_tipo_p = 'T') then
		
		select	max(qt_item)
		into	vl_result_w
		from	(select	trunc((dt_preench_ficha) - (dt_diagnostico)) qt_item
		 	from	can_ficha_admissao
		 	where	dt_diagnostico between dt_inicial_p and dt_final_p
		 	order by 1)
		where	rownum <= vl_terceiro_w;

	end if;
		

elsif	(ie_opcao_p = 'DT') then

	select	dividir((25 * count(trunc((dt_diagnostico) - (dt_inicio_trat)))), 100) vl_primeiro,
		dividir((75 * count(trunc((dt_diagnostico) - (dt_inicio_trat)))), 100) vl_terceiro
	into	vl_primeiro_w,
		vl_terceiro_w
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_p;

	if	(ie_tipo_p = 'P') then

		select	max(qt_item)
		into	vl_result_w
		from	(select	trunc((dt_diagnostico) - (dt_inicio_trat)) qt_item
			from	can_ficha_admissao
		 	where	dt_diagnostico between dt_inicial_p and dt_final_p
	 		order by 1)
		where	rownum <= vl_primeiro_w;
	
	elsif	(ie_tipo_p = 'T') then
		
		select	max(qt_item)
		into	vl_result_w
		from	(select	trunc((dt_diagnostico) - (dt_inicio_trat)) qt_item
			from	can_ficha_admissao
		 	where	dt_diagnostico between dt_inicial_p and dt_final_p
		 	order by 1)
		where	rownum <= vl_terceiro_w;

	end if;


elsif	(ie_opcao_p = 'MT') then

	select	dividir((25 * count(trunc((dt_preench_ficha) - (dt_inicio_trat)))), 100) vl_primeiro,
		dividir((75 * count(trunc((dt_preench_ficha) - (dt_inicio_trat)))), 100) vl_terceiro
	into	vl_primeiro_w,
		vl_terceiro_w
	from	can_ficha_admissao
	where	dt_diagnostico between dt_inicial_p and dt_final_P;

	if	(ie_tipo_p = 'P') then

		select	max(qt_item)
		into	vl_result_w
		from	(select trunc((dt_preench_ficha) - (dt_inicio_trat)) qt_item
			from	can_ficha_admissao
			where	dt_diagnostico between dt_inicial_p and dt_final_p
			order by 1)
		where	rownum <= vl_primeiro_w;

	elsif	(ie_tipo_p = 'T') then

		select	max(qt_item)
		into	vl_result_w
		from	(select trunc((dt_preench_ficha) - (dt_inicio_trat)) qt_item
			from	can_ficha_admissao
			where	dt_diagnostico between dt_inicial_p and dt_final_p
			order by 1)
		where	rownum <= vl_terceiro_w;

	end if;


end if;

RETURN vl_result_w;

END Obter_Quartis_Relatorio;
/
