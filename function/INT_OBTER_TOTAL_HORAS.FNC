create or replace function int_obter_total_horas(nr_seq_integracao_p  number)
        return number is

nr_horas_total_w number(15,2);

begin

select  sum(qt_horas)
into nr_horas_total_w
from int_etapas
where nr_seq_integracao = nr_seq_integracao_p;

return nr_horas_total_w;

end int_obter_total_horas;
 /