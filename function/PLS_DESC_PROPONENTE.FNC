create or replace
function pls_desc_proponente(
	nr_seq_proponente_p pls_solic_credenciame_user.nr_sequencia%type
) return varchar2 is

ds_proponente_w pls_solic_credenciame_user.nm_pessoa%type;

begin
	if (nr_seq_proponente_p is not null) then
		select max(nm_pessoa)
		into ds_proponente_w
		from pls_solic_credenciame_user
		where nr_sequencia = nr_seq_proponente_p;
	end if;
	return ds_proponente_w;
end pls_desc_proponente;
/
