create or replace
function pls_qt_benef_sexo_faixa_etaria
			(	qt_idade_inicial_p	number,
				qt_idade_final_p	number,
				ie_tipo_data_p		varchar2,
				dt_inicial_p		date,
				dt_final_p		date,
				ie_situacao_p		varchar2,
				cd_estabelecimento_p	number,
				ie_opcao_sexo_p		varchar2)
				return number is

/*
ie_opcao_sexo_p:
	M - Masculino
	F - Feminino
	T - Total (Masculino + Feminino)
*/

qt_beneficiarios_w		number(10);
qt_benef_total_w		number(10);
qt_benef_feminino_w		number(10);
qt_benef_masculino_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
qt_idade_benef_w		number(10);
nr_seq_segurado_w		number(10);
dt_nascimento_w			date;

Cursor C01 is
	select	b.dt_nascimento
	from	pessoa_fisica	b,
		pls_segurado	a
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	ie_tipo_segurado	in ('A','B','R')
	and	(pls_obter_dados_segurado(a.nr_sequencia,'CS')	= ie_situacao_p)
	and	((ie_tipo_data_p = 'C' and dt_contratacao between dt_inicial_p and fim_dia(dt_final_p)) or
		(ie_tipo_data_p	 = 'A' and dt_liberacao between dt_inicial_p and fim_dia(dt_final_p)) or
		(ie_tipo_data_p	 = 'N'))
	and	a.cd_estabelecimento	= cd_estabelecimento_p;
	
Cursor C02 is
	select	b.dt_nascimento
	from	pessoa_fisica	b,
		pls_segurado	a
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	ie_tipo_segurado	in ('A','B','R')
	and	(pls_obter_dados_segurado(a.nr_sequencia,'CS')	= ie_situacao_p)
	and	((ie_tipo_data_p = 'C' and a.dt_contratacao between dt_inicial_p and fim_dia(dt_final_p)) or
		(ie_tipo_data_p	 = 'A' and a.dt_liberacao between dt_inicial_p and fim_dia(dt_final_p)) or
		(ie_tipo_data_p	 = 'N'))
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.ie_sexo = 'F';

Cursor C03 is
	select	b.dt_nascimento
	from	pessoa_fisica	b,
		pls_segurado	a
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	ie_tipo_segurado	in ('A','B','R')
	and	(pls_obter_dados_segurado(a.nr_sequencia,'CS')	= ie_situacao_p)
	and	((ie_tipo_data_p = 'C' and a.dt_contratacao between dt_inicial_p and fim_dia(dt_final_p)) or
		(ie_tipo_data_p	 = 'A' and a.dt_liberacao between dt_inicial_p and fim_dia(dt_final_p)) or
		(ie_tipo_data_p	 = 'N'))
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.ie_sexo = 'M';	
	
begin
qt_beneficiarios_w	:= 0;

if	(ie_opcao_sexo_p = 'T') then
	begin
	open C01;
	loop
	fetch C01 into	
		dt_nascimento_w;
	exit when C01%notfound;
		begin
	
		qt_idade_benef_w	:= trunc(months_between(sysdate, dt_nascimento_w) / 12);
		
		if	(qt_idade_benef_w between qt_idade_inicial_p and qt_idade_final_p) then
			qt_beneficiarios_w	:= qt_beneficiarios_w + 1;
		end if;	
	
		end;
	end loop;
	close C01;
	end;
end if;


if	(ie_opcao_sexo_p = 'F') then
	begin
	open C02;
	loop
	fetch C02 into	
		dt_nascimento_w;
	exit when C02%notfound;
		begin
	
		qt_idade_benef_w	:= trunc(months_between(sysdate, dt_nascimento_w) / 12);
	
		if	(qt_idade_benef_w between qt_idade_inicial_p and qt_idade_final_p) then
			qt_beneficiarios_w	:= qt_beneficiarios_w + 1;
		end if;	
	
		end;
	end loop;
	close C02;
	end;
end if;

if	(ie_opcao_sexo_p = 'M') then
	begin
	open C03;
	loop
	fetch C03 into	
		dt_nascimento_w;
	exit when C03%notfound;
		begin
		
		qt_idade_benef_w	:= trunc(months_between(sysdate, dt_nascimento_w) / 12);
	
		if	(qt_idade_benef_w between qt_idade_inicial_p and qt_idade_final_p) then
			qt_beneficiarios_w	:= qt_beneficiarios_w + 1;
		end if;	
	
		end;
	end loop;
	close C03;
	end;
end if;

return	qt_beneficiarios_w;

end pls_qt_benef_sexo_faixa_etaria;
/
