create or replace
function rp_obter_desc_mot_licenca(nr_sequencia_p	number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);			

begin
ds_retorno_w := '';
if (nr_sequencia_p <> 0) and (nr_sequencia_p is not null) then
	select 	substr(ds_motivo_licenca,1,255)
	into	ds_retorno_w
	from	rp_motivo_licenca
	where	nr_sequencia = nr_sequencia_p;
end if;



return	ds_retorno_w;

end rp_obter_desc_mot_licenca;
/