create or replace
function	cm_calcular_validade_conj	(nr_seq_equip_p		number,
				nr_seq_embalagem_p	number) return date is

dt_retorno_w		date;
nr_seq_metodo_w		number(10);
qt_dias_val_w		number(5);
qt_hor_val_w		number(5);

begin

select	nr_seq_classif
into	nr_seq_metodo_w
from  	cm_equipamento
where	nr_sequencia = nr_seq_equip_p;

select	nvl(max(nvl(qt_dia_validade,0)),0),
	nvl(max(nvl(qt_hor_validade,0)),0)	
into	qt_dias_val_w,
	qt_hor_val_w	
from	cm_validade
where	nr_seq_embalagem 	= nr_seq_embalagem_p
and	nr_seq_metodo		= nr_seq_metodo_w;

if (qt_dias_val_w > 0 or qt_hor_val_w > 0) then
	begin
	select	to_date(to_char(sysdate + qt_dias_val_w + (qt_hor_val_w / 24),'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
	into	dt_retorno_w
	from	dual;
	end;
end if;

return dt_retorno_w;

end cm_calcular_validade_conj;
/