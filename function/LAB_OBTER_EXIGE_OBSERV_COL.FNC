create or replace
function lab_obter_exige_observ_col (nr_prescricao_p		number,
				nr_seq_prescr_p		varchar2)
				return varchar2 is

ie_retorno_w	varchar2(1);			
			
begin

select  decode(count(*),0,'N','S')
into	ie_retorno_w
from	prescr_medica e,
	exame_lab_material d,
	prescr_procedimento a
where	a.nr_prescricao = e.nr_prescricao
and	a.nr_seq_exame = d.nr_seq_exame
and	d.nr_seq_material = Obter_Material_Exame_Lab(null, a.cd_material_exame, 1)
and	a.nr_prescricao = nr_prescricao_p
and	obter_se_contido(a.nr_sequencia ,(nr_seq_prescr_p)) = 'S'
and	((nvl(d.ie_exige_obs_coleta,'N') = 'S') or (nvl(d.ie_exige_obs_coleta,'N') = 'E'));

return	ie_retorno_w;

end lab_obter_exige_observ_col;
/
