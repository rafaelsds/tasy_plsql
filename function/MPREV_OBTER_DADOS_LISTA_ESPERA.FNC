create or replace
function mprev_obter_dados_lista_espera(	nr_seq_lista_p	number,
						ie_opcao_p	varchar2)
						return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar diferentes informa��es da lista de espera da medicina preventiva.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

ie_opcao_p :
'DP' = Descri��o da Prioridade de atendimento
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */									
ds_retorno_w	varchar2(255);
ie_prioridade_w	mprev_grupo_turma_espera.ie_prioridade%type;

begin

if	(upper(ie_opcao_p) = 'DP')then
	select	nvl(ie_prioridade,1)
	into	ie_prioridade_w
	from	mprev_grupo_turma_espera
	where	nr_sequencia = nr_seq_lista_p;
	
	if	(ie_prioridade_w = 1) then
		--Normal
		ds_retorno_w	:= substr(obter_texto_dic_objeto(345383, wheb_usuario_pck.get_nr_seq_idioma, null),1,255);
	else
		--Urgente
		ds_retorno_w	:= substr(obter_texto_dic_objeto(345384, wheb_usuario_pck.get_nr_seq_idioma, null),1,255);
	end if;

end if;

return	ds_retorno_w;

end mprev_obter_dados_lista_espera;
/
