create or replace
function Obter_Medico_Laudo_Repasse
		(nr_seq_proc_p		number)
		return varchar2 is

cd_medico_w		varchar2(10);
nr_seq_proc_princ_w	number(10);
nr_atendimento_w	number(10);
nr_sequencia_w		number(10);

begin

select	max(cd_medico_resp)
into	cd_medico_w
from	laudo_paciente
where	nr_seq_proc	= nr_seq_proc_p;

if	(nvl(cd_medico_w,'0') = '0') then
	select	max(a.nr_seq_proc_princ)
	into	nr_seq_proc_princ_w
	from	procedimento_paciente  a
	where	a.nr_sequencia	= nr_seq_proc_p;
	
	select	max(cd_medico_resp)
	into	cd_medico_w
	from	laudo_paciente
	where	nr_seq_proc	= nr_seq_proc_princ_w;
end if;

return	cd_medico_w;


end Obter_Medico_Laudo_Repasse;
/