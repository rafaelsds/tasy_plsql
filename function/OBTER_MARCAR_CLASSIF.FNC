create or replace
function obter_marcar_classif(ds_atributo_p             	varchar2)
return varchar2 is

ie_marcado_w		varchar2(1 char);
ds_atrib_dominio_w		varchar2(5);

begin
	if (ds_atributo_p = 'IE_DIAG_PRINC_EPISODIO') then
	    	ds_atrib_dominio_w := 'DPE';
	elsif (ds_atributo_P = 'IE_DIAG_REFERENCIA') then
		ds_atrib_dominio_w := 'UBE';
	elsif (ds_atributo_P = 'IE_DIAG_TRATAMENTO') then
		ds_atrib_dominio_w := 'BEH';
	elsif (ds_atributo_P = 'IE_DIAG_ADMISSAO') then
		ds_atrib_dominio_w := 'AUF';
	elsif (ds_atributo_P = 'IE_DIAG_ALTA') then
		ds_atrib_dominio_w := 'ENT';
	elsif (ds_atributo_P = 'IE_DIAG_CIRURGIA') then
		ds_atrib_dominio_w := 'OPD';
	elsif (ds_atributo_P = 'IE_DIAG_OBITO') then
		ds_atrib_dominio_w := 'TOD';
	elsif (ds_atributo_P = 'IE_DIAG_PRE_CIR') then
		ds_atrib_dominio_w := 'POD';
	elsif (ds_atributo_P = 'IE_DIAG_PRINC_DEPART') then
		ds_atrib_dominio_w := 'FAC';
	elsif (ds_atributo_P = 'IE_DIAG_TRAT_CERT') then
		ds_atrib_dominio_w := 'BHS';
	elsif (ds_atributo_P = 'IE_DIAG_CRONICO') then
		ds_atrib_dominio_w := 'FAL';
	elsif (ds_atributo_P = 'IE_DIAG_TRAT_ESPECIAL') then
		ds_atrib_dominio_w := 'ASV';		
	end if;
	
	select  	nvl(max(ie_valor_padrao), 'N')
	into	ie_marcado_w
	from    	classificacao_diagnostico
	where   	ie_classificacao_diagnostico = ds_atrib_dominio_w;
	
return ie_marcado_w;
end obter_marcar_classif;
/