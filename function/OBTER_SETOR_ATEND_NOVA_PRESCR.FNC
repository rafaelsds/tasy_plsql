create or replace
function obter_setor_atend_nova_prescr(
		ie_pac_usu_p		varchar2,
		nr_atendimento_p	number,
		cd_setor_atendimento_p	number,
		cd_perfil_p		number)
 		return number is

cd_setor_atendimento_w	number(5,0);
ie_registro_w		varchar2(1);
begin

if	(ie_pac_usu_p	= 'R') then
	begin
	cd_setor_atendimento_w := to_number(obter_setor_prescr_regra(cd_perfil_p));
	end;
elsif	(ie_pac_usu_p	= 'U') and
	(nr_atendimento_p > 0) then
	begin
	select	decode(nvl(count(*),0),0,'N','S')
	into	ie_registro_w
	from	setor_atendimento a
	where	a.cd_setor_atendimento in (
			select	b.cd_setor_atendimento
			from	atend_paciente_unidade b
			where	b.nr_atendimento	= nr_atendimento_p
			and	b.cd_setor_atendimento	= cd_setor_atendimento_p);
	
	if	(ie_registro_w = 'S') then
		begin
		cd_setor_atendimento_w	:= cd_setor_atendimento_p;
		end;
	end if;
	end;
end if;
if	(cd_setor_atendimento_w	is null) and
	(nr_atendimento_p > 0) then
	begin
	select	nvl(obter_unidade_atendimento(nr_atendimento_p,'IA','CS'),obter_unidade_atendimento(nr_atendimento_p,'A','CS'))
	into	cd_setor_atendimento_w
	from	dual;
	end;
end if;
return	cd_setor_atendimento_w;
end obter_setor_atend_nova_prescr;
/