create or replace
FUNCTION Obter_quant_acomp_atendimento(nr_atendimento_p		NUMBER,
				   cd_pessoa_fisica_p		varchar2)
					RETURN number IS

qt_acompanhante_w			VARCHAR2(255);

BEGIN

SELECT	count(*)
into	qt_acompanhante_w
FROM	atendimento_acompanhante
WHERE	nr_atendimento	= nr_atendimento_p
and	((cd_pessoa_fisica <> cd_pessoa_fisica_p) or (cd_pessoa_fisica is null))
AND	((cd_pessoa_fisica IS NOT NULL) OR
	(nm_acompanhante IS NOT NULL))
AND	dt_saida IS NULL;


RETURN qt_acompanhante_w;

END Obter_quant_acomp_atendimento;
/
