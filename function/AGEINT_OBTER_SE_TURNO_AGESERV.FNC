create or replace
function Ageint_Obter_Se_Turno_Ageserv(
				dt_agenda_p	date,
				cd_agenda_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);
ie_dia_semana_w	number(1);
			
begin

ie_dia_semana_w	:= obter_cod_dia_semana(dt_agenda_p);


select	decode(count(*), 0, 'N', 'S')
into	ds_retorno_w
from	agenda_turno
where	cd_agenda	= cd_agenda_p
and	((dt_agenda_p	>= dt_inicio_vigencia) or (dt_inicio_vigencia is null))
and	((dt_agenda_p	<= dt_final_vigencia) or (dt_final_vigencia is null))
and	((ie_dia_semana	= ie_dia_semana_w) or (ie_dia_semana = 9 and ie_dia_semana_w not in (1,7)))
and	dt_agenda_p <= to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');

ds_retorno_w := 'S';

return	ds_retorno_w;

end Ageint_Obter_Se_Turno_Ageserv;
/
