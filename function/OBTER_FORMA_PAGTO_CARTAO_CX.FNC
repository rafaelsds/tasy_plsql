create or replace
function obter_forma_pagto_cartao_cx(nr_seq_caixa_receb_p	number,
				     nr_seq_forma_pagto_p	number)
 		    	return varchar2 is

nr_seq_caixa_w	number(10);
ds_retorno_w	varchar2(1) := 'S';			
qt_registro_w	number(10);
begin

select	max(b.nr_seq_caixa)
into	nr_seq_caixa_w
from	caixa_receb a,
	caixa_saldo_diario b
where	a.nr_seq_saldo_caixa = b.nr_sequencia
and	a.nr_sequencia = nr_seq_caixa_receb_p;

if (nr_seq_caixa_w is not null) then
	select	count(*)
	into	qt_registro_w
	from	forma_pagto_caixa
	where	nr_seq_caixa = nr_seq_caixa_w
	and		ie_situacao	= 'A';
	
	if (qt_registro_w > 0) then
		select count(*)
		into	qt_registro_w
		from	forma_pagto_caixa
		where	nr_seq_forma_pagto = nr_seq_forma_pagto_p
		and	nr_seq_caixa = nr_seq_caixa_w
		and 	ie_situacao = 'A';
		
		if (qt_registro_w > 0) then
			ds_retorno_w := 'S';
		else
			ds_retorno_w := 'N';
		end if;
	end if;
end if;

return	ds_retorno_w;

end obter_forma_pagto_cartao_cx;
/
