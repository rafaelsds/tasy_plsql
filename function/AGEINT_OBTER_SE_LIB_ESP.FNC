create or replace
function Ageint_Obter_Se_Lib_Esp
			(nr_seq_proc_interno_p		number,
			nr_seq_ageint_p			number,
			cd_agenda_p			number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1)	:= 'N';
cd_agenda_w		number(10);
nr_seq_proc_interno_w	number(10);			
qt_exame_w		number(10);			
			
Cursor C01 is
	select	a.cd_agenda,
		a.nr_seq_proc_interno
	from	ageint_lib_exame_pri a,
		ageint_lib_exames b
	where	b.nr_seq_proc_interno	= nr_seq_proc_interno_p
	and	a.nr_sequencia		= b.nr_ageint_lib_exame
	and	a.cd_agenda		= cd_agenda_p
	and	ds_retorno_w	= 'N';
			
begin

open C01;
loop
fetch C01 into	
	cd_Agenda_w,
	nr_seq_proc_interno_w;
exit when C01%notfound; 
	begin
	if	(cd_agenda_w is not null) and
		(nr_seq_proc_interno_w is not null) then
		select	count(*)
		into	qt_exame_w
		from	agenda_integrada_item
		where	nr_seq_proc_interno	= nr_seq_proc_interno_w
		and	nr_seq_agenda_int	= nr_seq_ageint_p;
		
		if	(qt_exame_w	> 0) then
			ds_retorno_w	:= 'S';
		end if;
	end if;
	end;
end loop;
close C01;

return	ds_retorno_w;

end Ageint_Obter_Se_Lib_Esp;
/