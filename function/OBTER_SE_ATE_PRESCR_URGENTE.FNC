create or replace
function obter_se_ate_prescr_urgente (	nr_atendimento_p	number,
					dt_inicial_p		date,
					dt_final_p		date,
					cd_pessoa_fisica_p      pessoa_fisica.cd_pessoa_fisica%type)
					return varchar2 is

ie_urgente_w		varchar2(1);

begin

if (nr_atendimento_p is not null) then
	select	decode(count(nr_prescricao),0,'N','S')
	into	ie_urgente_w
	from	prescr_medica
	where	nr_atendimento = nr_atendimento_p
	and	dt_liberacao is not null
	and	nvl(ie_lib_farm,'N') = 'S'
	and	dt_liberacao_farmacia is null
	and	obter_se_prioridade_revisao(nr_prescricao)  = 'S'
	and	dt_prescricao between dt_inicial_p and dt_Final_p
	and rownum = 1;
else 
	select	decode(count(nr_prescricao),0,'N','S')
	into	ie_urgente_w
	from	prescr_medica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p 
	and nr_atendimento is null 
	and	dt_liberacao is not null
	and	nvl(ie_lib_farm,'N') = 'S'
	and	dt_liberacao_farmacia is null
	and	obter_se_prioridade_revisao(nr_prescricao)  = 'S'
	and	dt_prescricao between dt_inicial_p and dt_Final_p
	and rownum = 1;
end if;
	
return	ie_urgente_w;

end obter_se_ate_prescr_urgente;
/
