create or replace
function obter_desc_area_mercado(nr_sequencia_p	number)
 		    	return varchar2 is

nm_area_w	varchar2(255);
			
begin

if	(nr_sequencia_p	is not null) then
	select	substr(obter_desc_expressao(cd_exp_area, ds_area),1,255)
	into	nm_area_w
	from	mkt_area 
	where	nr_sequencia = nr_sequencia_p;

end if;

return	nm_area_w;

end obter_desc_area_mercado;
/