CREATE OR REPLACE
FUNCTION Obter_Setor_Atend_nao_cancel(		
					nr_atendimento_p		Number)
					RETURN Number IS


cd_setor_atendimento_w	Number(5,0);
dt_cancelamento_w date;

BEGIN

select	max(dt_cancelamento)
into	dt_cancelamento_w
from	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

if (dt_cancelamento_w is null) then
	select	max(cd_setor_atendimento)
	into	cd_setor_atendimento_w
	from	atend_paciente_unidade
	where	nr_seq_interno = (
		select Obter_Atepacu_paciente(nr_atendimento_p, 'A')
		from dual);	
end if;

RETURN cd_setor_atendimento_w;

END Obter_Setor_Atend_nao_cancel;
/