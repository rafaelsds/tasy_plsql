create or replace
function obter_desc_EIF(	nr_seq_escala_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	max(DS_ESCALA)
into	ds_retorno_w
from	eif_escala
where	nr_sequencia = nr_seq_escala_p;

return	ds_retorno_w;

end obter_desc_eif;
/
