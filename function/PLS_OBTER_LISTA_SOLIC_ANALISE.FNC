create or replace
function pls_obter_lista_solic_analise
			(nr_seq_analise_p		number)
			return varchar2 is

ds_solicitacao_w	varchar2(2000);
ds_solicitacao_ww	varchar2(2000);

Cursor C01 is
	select	ds_tipo_solicitacao
	from	pls_analise_tipo_solic		b,
		pls_analise_solicitacao		a
	where	a.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_tipo_solic	= b.nr_sequencia
	and	a.dt_conclusao is not null;

begin
open C01;
loop
fetch C01 into	
	ds_solicitacao_w;
exit when C01%notfound;
	begin
	ds_solicitacao_ww := ds_solicitacao_w||', '||ds_solicitacao_ww;
	ds_solicitacao_w	:= '';
	end;
end loop;
close C01;

return substr(ds_solicitacao_ww,1,length(ds_solicitacao_ww)-2);

end pls_obter_lista_solic_analise;
/
