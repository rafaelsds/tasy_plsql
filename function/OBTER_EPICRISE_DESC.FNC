create or replace
function obter_epicrise_desc(	nr_sequencia_p number,
								ie_tipo_info_p varchar2)
 		    				return varchar2 is

ds_epicrise_w	varchar2(255);
				
begin

ds_epicrise_w := '';

if (nr_sequencia_p is not null and
	nr_sequencia_p > 0) then
	
	if (ie_tipo_info_p = 'ME') then
		select 	max(SUBSTR(obter_desc_material(cd_material),1,254))		
		into 	ds_epicrise_w
		from 	cpoe_material
		where 	nr_sequencia = nr_sequencia_p;
	elsif (ie_tipo_info_p = 'RE') then
		select 	max(SUBSTR(cpoe_obter_desc_recomendacao(cd_recomendacao, dt_liberacao), 1, 254))		
		into 	ds_epicrise_w
		from 	cpoe_recomendacao
		where 	nr_sequencia = nr_sequencia_p;
	end if;
end if;	

return ds_epicrise_w;

end obter_epicrise_desc;
/
