create or replace
function pls_obter_qtd_vidas_mes
			(	dt_mes_referencia_p	Date,
				ie_tipo_retorno_p		Varchar2)
				return Number is
				
/* ie_tipo_retorno_p
	A - Ativos
	AP - Ativos pr� - estabelecido
	ACO - Ativos custo operacional
	
*/
				
qt_vidas_w			Number(10)	:= 0;

begin

if	(ie_tipo_retorno_p	= 'A') then
	select	count(1)
	into	qt_vidas_w
	from	w_pls_benef_movto_mensal
	where	trunc(dt_referencia,'month')	= trunc(dt_mes_referencia_p,'month');
elsif	(ie_tipo_retorno_p	= 'AP') then
	select	count(*)
	into	qt_vidas_w
	from	pls_plano		c,
		pls_segurado		b,
		pls_segurado_historico	a
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	pls_obter_produto_benef(b.nr_sequencia,dt_mes_referencia_p) = c.nr_sequencia
	and	c.ie_preco		= '1'
	and	a.ie_tipo_historico in ('2','22')
	and	a.dt_historico	=	(select	max(x.dt_historico)
					from	pls_segurado_historico	x
					where	trunc(x.dt_historico,'month') <= trunc(dt_mes_referencia_p,'month')
					and	x.ie_tipo_historico in ('2','22','1','5')
					and 	a.nr_seq_segurado = x.nr_seq_segurado);
elsif	(ie_tipo_retorno_p	= 'ACO') then
	select	count(*)
	into	qt_vidas_w
	from	pls_plano		c,
		pls_segurado		b,
		pls_segurado_historico	a
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	pls_obter_produto_benef(b.nr_sequencia,dt_mes_referencia_p) = c.nr_sequencia
	and	c.ie_preco		= '3'
	and	a.ie_tipo_historico in ('2','22')
	and	a.dt_historico	=	(select	max(x.dt_historico)
					from	pls_segurado_historico	x
					where	trunc(x.dt_historico,'month') <= trunc(dt_mes_referencia_p,'month')
					and	x.ie_tipo_historico in ('2','22','1','5')
					and 	a.nr_seq_segurado = x.nr_seq_segurado);
end if;

return	qt_vidas_w;

end pls_obter_qtd_vidas_mes;
/