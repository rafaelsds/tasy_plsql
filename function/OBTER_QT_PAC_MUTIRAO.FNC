create or replace
function Obter_Qt_Pac_Mutirao(nr_seq_mutirao_p		number)
 		    	return number is

qt_pac_mutirao_w	number(10);

begin

if(nr_seq_mutirao_p is not null)then

	select  count(cd_pessoa_fisica)
	into 	qt_pac_mutirao_w
	from    paciente_mutirao
	where   nr_seq_mutirao = nr_seq_mutirao_p
	and		dt_exclusao is null;

end if;

return	nvl(qt_pac_mutirao_w, 0);

end Obter_Qt_Pac_Mutirao;
/