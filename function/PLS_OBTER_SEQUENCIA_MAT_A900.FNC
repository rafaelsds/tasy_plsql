create or replace
function pls_obter_sequencia_mat_a900 (	nr_seq_material_p	pls_material.nr_sequencia%type,
					dt_vigencia_p		date,
					ie_alc_p		pls_mat_unimed_trib.id_alc%type)
 		    	return pls_material_unimed.nr_sequencia%type is
			
nr_seq_mat_unimed_w	pls_material_unimed.nr_sequencia%type;
dt_vig_ini_w		pls_conta_mat.dt_atendimento%type;
dt_vig_fim_w		pls_conta_mat.dt_atendimento%type;

begin

if	(dt_vigencia_p is not null) then
	dt_vig_ini_w := trunc(dt_vigencia_p);
	dt_vig_fim_w := fim_dia(dt_vigencia_p);
end if;	

select	max(nr_seq_material_unimed)
into	nr_seq_mat_unimed_w
from	pls_material
where	nr_sequencia	= nr_seq_material_p;

if	( nr_seq_mat_unimed_w is null) then

	if	(dt_vigencia_p is not null) then
		select	max(b.nr_sequencia)
		into	nr_seq_mat_unimed_w
		from	pls_material_a900	a,
			pls_material_unimed	b
		where	a.nr_seq_material	= nr_seq_material_p
		and	b.cd_material		= a.cd_material_a900
		and 	((a.dt_fim_vigencia is null) or (a.dt_fim_vigencia > dt_vig_fim_w))
		and	a.dt_inicio_vigencia < dt_vig_ini_w
		and	exists	(select	1
				from	pls_mat_unimed_trib	c
				where	c.nr_seq_mat_unimed	= b.nr_sequencia
				and	((c.dt_inicio_vigencia is null) or (dt_vig_fim_w > c.dt_inicio_vigencia))
				and	c.id_alc = ie_alc_p);
	else
		select	max(b.nr_sequencia)
		into	nr_seq_mat_unimed_w
		from	pls_material_a900	a,
			pls_material_unimed	b
		where	a.nr_seq_material	= nr_seq_material_p
		and	b.cd_material		= a.cd_material_a900;
	end if;
end if;

return	nr_seq_mat_unimed_w;

end pls_obter_sequencia_mat_a900;
/
