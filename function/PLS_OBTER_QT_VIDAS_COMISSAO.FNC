create or replace
function pls_obter_qt_vidas_comissao
		(	nr_seq_contrato_p		number,
			nr_seq_intercambio_p		number,
			dt_mesano_referencia_p		date,
			ie_acao_contrato_p		varchar2,
			nr_contrato_principal_p		number,
			ie_referencia_vidas_p		varchar2,
			ie_considerar_ativos_p		varchar2)
 		    	return number is

qt_vidas_w			pls_integer;
qt_vidas_cont_princ_w		pls_integer;
nr_seq_grupo_intercambio_w	pls_intercambio.nr_seq_grupo_intercambio%type;
nr_seq_grupo_w			pls_contrato_grupo.nr_seq_grupo%type;
nr_contrato_principal_w		pls_contrato.nr_contrato_principal%type;

/*
Criado �ndice : 
*/
begin

qt_vidas_w := 0;

if	(ie_referencia_vidas_p = 'C') then -- C = Contrato
	--Contrato
	if	(nr_seq_contrato_p is not null) then
		--Verifica apenas ativos
		if	(ie_considerar_ativos_p = 'S') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado	a
			where	a.nr_seq_contrato = nr_seq_contrato_p
			and	((dt_rescisao is null) or
				  (dt_rescisao is not null and dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
				  (dt_rescisao is not null and dt_reativacao is not null and dt_reativacao > dt_rescisao and dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
		--verifica todos
		elsif	(ie_considerar_ativos_p = 'N') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado	a
			where	a.nr_seq_contrato = nr_seq_contrato_p;
		end if;
	--Interc�mbio
	elsif	(nr_seq_intercambio_p is not null) then
		--Verifica apenas ativos
		if	(ie_considerar_ativos_p = 'S') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado	a
			where	a.nr_seq_intercambio = nr_seq_intercambio_p
			and	((dt_rescisao is null) or
				  (dt_rescisao is not null and dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
				  (dt_rescisao is not null and dt_reativacao is not null and dt_reativacao > dt_rescisao and dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
		--verifica todos
		elsif	(ie_considerar_ativos_p = 'N') then
			select	count(1)
			into	qt_vidas_w
			from	pls_segurado	a
			where	a.nr_seq_intercambio = nr_seq_intercambio_p;
		end if;
	
	end if;
elsif	(ie_referencia_vidas_p = 'G') then -- G = Grupo contratual
	if	(nr_seq_contrato_p is not null) then
		if	(nr_contrato_principal_p is not null) then
			--Verifica apenas ativos
			if	(ie_considerar_ativos_p = 'S') then
				select	count(1)
				into	qt_vidas_cont_princ_w
				from	pls_contrato	b,
					pls_segurado 	a
				where	b.nr_sequencia	= a.nr_seq_contrato
				and	b.nr_sequencia	= nr_contrato_principal_p
				and	((a.dt_rescisao is null) or
					(a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
					(a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
					
				select	count(1)
				into	qt_vidas_w
				from	pls_contrato	b,
					pls_segurado 	a
				where	b.nr_sequencia	= a.nr_seq_contrato
				and	b.nr_contrato_principal = nr_contrato_principal_p
				and	((a.dt_rescisao is null) or
					 (a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
					 (a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
			--verifica todos
			elsif	(ie_considerar_ativos_p = 'N') then
				select	count(1)
				into	qt_vidas_cont_princ_w
				from	pls_contrato	b,
					pls_segurado 	a
				where	b.nr_sequencia	= a.nr_seq_contrato
				and	b.nr_sequencia	= nr_contrato_principal_p;
				
				select	count(1)
				into	qt_vidas_w
				from	pls_contrato	b,
					pls_segurado 	a
				where	b.nr_sequencia	= a.nr_seq_contrato
				and	b.nr_contrato_principal = nr_contrato_principal_p;
			end if;
			
			qt_vidas_w	:= qt_vidas_w + qt_vidas_cont_princ_w;
		else
			select	max(nr_contrato_principal)
			into	nr_contrato_principal_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_p;
			
			--Verifica pelo contrato principal
			if	(nr_contrato_principal_w is not null) then
				--Verifica apenas ativos
				if	(ie_considerar_ativos_p = 'S') then
					select	count(1)
					into	qt_vidas_cont_princ_w
					from	pls_contrato	b,
						pls_segurado 	a
					where	b.nr_sequencia	= a.nr_seq_contrato
					and	b.nr_sequencia	= nr_contrato_principal_w
					and	((a.dt_rescisao is null) or
						  (a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
						  (a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
					
					select	count(1)
					into	qt_vidas_w
					from	pls_contrato	b,
						pls_segurado 	a
					where	b.nr_sequencia	= a.nr_seq_contrato
					and	b.nr_contrato_principal = nr_contrato_principal_w
					and	((a.dt_rescisao is null) or
						  (a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
						  (a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
				--verifica todos
				elsif	(ie_considerar_ativos_p = 'N') then
					select	count(1)
					into	qt_vidas_cont_princ_w
					from	pls_contrato	b,
						pls_segurado 	a
					where	b.nr_sequencia	= a.nr_seq_contrato
					and	b.nr_sequencia	= nr_contrato_principal_w;
					
					select	count(1)
					into	qt_vidas_w
					from	pls_contrato	b,
						pls_segurado 	a
					where	b.nr_sequencia	= a.nr_seq_contrato
					and	b.nr_contrato_principal = nr_contrato_principal_w;
				end if;
				
				qt_vidas_w	:= qt_vidas_w + qt_vidas_cont_princ_w;
			else
				--Verifica apenas ativos
				if	(ie_considerar_ativos_p = 'S') then
					select	count(1)
					into	qt_vidas_cont_princ_w
					from	pls_contrato	b,
						pls_segurado 	a
					where	b.nr_sequencia	= a.nr_seq_contrato
					and	b.nr_sequencia	= nr_seq_contrato_p
					and	((a.dt_rescisao is null) or
						  (a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
						  (a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
					
					select	count(1)
					into	qt_vidas_w
					from	pls_contrato	b,
						pls_segurado 	a
					where	b.nr_sequencia	= a.nr_seq_contrato
					and	b.nr_contrato_principal	= nr_seq_contrato_p
					and	((a.dt_rescisao is null) or
						  (a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
						  (a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
				--verifica todos
				elsif	(ie_considerar_ativos_p = 'N') then
					select	count(1)
					into	qt_vidas_cont_princ_w
					from	pls_contrato	b,
						pls_segurado 	a
					where	b.nr_sequencia	= a.nr_seq_contrato
					and	b.nr_sequencia	= nr_seq_contrato_p;

					select	count(1)
					into	qt_vidas_w
					from	pls_contrato	b,
						pls_segurado 	a
					where	b.nr_sequencia	= a.nr_seq_contrato
					and	b.nr_contrato_principal	= nr_seq_contrato_p;
				end if;
				
				qt_vidas_w	:= qt_vidas_w + qt_vidas_cont_princ_w;
			end if;
		end if;
	--Verifica para interc�mbio
	elsif	(nr_seq_intercambio_p is not null) then
		select	max(nr_seq_grupo_intercambio)
		into	nr_seq_grupo_intercambio_w
		from	pls_intercambio
		where	nr_sequencia = nr_seq_intercambio_p;
		
		--Verifica apenas ativos
		if	(ie_considerar_ativos_p = 'S') then
			select	count(1)
			into	qt_vidas_w
			from	pls_intercambio	b,
				pls_segurado 	a
			where	a.nr_seq_intercambio = b.nr_sequencia
			and	((b.nr_sequencia in (nr_seq_intercambio_p, nr_seq_grupo_intercambio_w)) or (b.nr_seq_grupo_intercambio in (nr_seq_intercambio_p, nr_seq_grupo_intercambio_w)))
			and	((a.dt_rescisao is null) or
				  (a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
				  (a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
		--verifica todos
		elsif	(ie_considerar_ativos_p = 'N') then
			select	count(1)
			into	qt_vidas_w
			from	pls_intercambio	b,
				pls_segurado 	a
			where	a.nr_seq_intercambio = b.nr_sequencia
			and	((b.nr_sequencia in (nr_seq_intercambio_p, nr_seq_grupo_intercambio_w)) or (b.nr_seq_grupo_intercambio in (nr_seq_intercambio_p, nr_seq_grupo_intercambio_w)));
		end if;
	end if;
elsif	(ie_referencia_vidas_p = 'R') then -- R = Grupo relacion. cliente
	if	(nr_seq_contrato_p is not null) then
		begin
		select	max(nr_seq_grupo)
		into	nr_seq_grupo_w
		from	pls_contrato_grupo
		where	nr_seq_contrato = nr_seq_contrato_p;
		exception
		when others then
			nr_seq_grupo_w	:= null;
		end;
		
		if	(nr_seq_grupo_w is not null) then
			--Verifica apenas ativos
			if	(ie_considerar_ativos_p = 'S') then
				select	count(1) qt_beneficiarios
				into	qt_vidas_w
				from	pls_segurado		a,
					pls_contrato		b,
					pls_contrato_grupo	d,
					pls_grupo_contrato	c
				where	a.nr_seq_contrato	= b.nr_sequencia
				and	d.nr_seq_contrato	= b.nr_sequencia
				and	d.nr_seq_grupo		= c.nr_sequencia
				and	c.nr_sequencia		= nr_seq_grupo_w
				and	((a.dt_rescisao is null) or
					  (a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
					  (a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
			--verifica todos
			elsif	(ie_considerar_ativos_p = 'N') then
				select	count(1) qt_beneficiarios
				into	qt_vidas_w
				from	pls_segurado		a,
					pls_contrato		b,
					pls_contrato_grupo	d,
					pls_grupo_contrato	c
				where	a.nr_seq_contrato	= b.nr_sequencia
				and	d.nr_seq_contrato	= b.nr_sequencia
				and	d.nr_seq_grupo		= c.nr_sequencia
				and	c.nr_sequencia		= nr_seq_grupo_w;
			end if;
		end if;
	elsif	(nr_seq_intercambio_p is not null) then
		begin
		select	max(nr_seq_grupo)
		into	nr_seq_grupo_w
		from	pls_contrato_grupo
		where	nr_seq_intercambio = nr_seq_intercambio_p;
		exception
		when others then
			nr_seq_grupo_w	:= null;
		end;
		
		if	(nr_seq_grupo_w is not null) then
			--Verifica apenas ativos
			if	(ie_considerar_ativos_p = 'S') then
				select	count(1) qt_beneficiarios
				into	qt_vidas_w
				from	pls_segurado		a,
					pls_intercambio		b,
					pls_contrato_grupo	d,
					pls_grupo_contrato	c
				where	a.nr_seq_intercambio	= b.nr_sequencia
				and	d.nr_seq_intercambio	= b.nr_sequencia
				and	d.nr_seq_grupo		= c.nr_sequencia
				and	c.nr_sequencia		= nr_seq_grupo_w
				and	((a.dt_rescisao is null) or
					  (a.dt_rescisao is not null and a.dt_rescisao > trunc(dt_mesano_referencia_p,'dd')) or
					  (a.dt_rescisao is not null and a.dt_reativacao is not null and a.dt_reativacao > a.dt_rescisao and a.dt_reativacao < trunc(dt_mesano_referencia_p,'dd')));
			--verifica todos
			elsif	(ie_considerar_ativos_p = 'N') then
				select	count(1) qt_beneficiarios
				into	qt_vidas_w
				from	pls_segurado		a,
					pls_intercambio		b,
					pls_contrato_grupo	d,
					pls_grupo_contrato	c
				where	a.nr_seq_intercambio	= b.nr_sequencia
				and	d.nr_seq_intercambio	= b.nr_sequencia
				and	d.nr_seq_grupo		= c.nr_sequencia
				and	c.nr_sequencia		= nr_seq_grupo_w;
			end if;
		end if;
	end if;
end if;	

return	qt_vidas_w;

end pls_obter_qt_vidas_comissao;
/