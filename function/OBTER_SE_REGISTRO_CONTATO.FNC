create or replace
function obter_se_registro_contato( 	cd_pessoa_fisica_p 	varchar2,
					cd_estabelecimento_p	number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is
			
nr_seq_tipo_doc_w	number(10);
nr_seq_forma_cont_w 	number(10);
ie_retorno_w		varchar2(1);			

begin

select	max(nr_seq_forma_contato_soro),      
	max(nr_seq_tipo_doc_soro)   
into	nr_seq_forma_cont_w,
	nr_seq_tipo_doc_w
from	san_parametro
where	cd_estabelecimento = cd_estabelecimento_p;

select	decode(count(*),0,'N','S')
into	ie_retorno_w
from	pessoa_fisica_contato
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

return	ie_retorno_w;

end obter_se_registro_contato;
/