create or replace
function obter_qt_apae_agenda(	nr_seq_agenda_p	number)
					return number is
qt_existe_w	number(15,0);

begin

select 	count(*) 
into	qt_existe_w
from 	aval_pre_anestesica 
where 	nr_seq_agenda = nr_seq_agenda_p;

return qt_existe_w;

end obter_qt_apae_agenda;
/