create or replace function get_weekday_name(nr_cpoe_week_seq_p number)
					return varchar2 is
					
ie_segunda_w		VARCHAR2(1);
ie_terca_w			VARCHAR2(1);
ie_quarta_w			VARCHAR2(1);
ie_quinta_w			VARCHAR2(1);
ie_sexta_w			VARCHAR2(1);
ie_sabado_w			VARCHAR2(1);
ie_domingo_w		VARCHAR2(1);
ds_day_name_w		varchar2(30);

begin

select 	NVL(a.ie_segunda, 'N'),
		NVL(a.ie_terca, 'N'),
		NVL(a.ie_quarta, 'N'),
		NVL(a.ie_quinta, 'N'),
		NVL(a.ie_sexta, 'N'),
		NVL(a.ie_sabado, 'N'),
		NVL(a.ie_domingo, 'N')
into	ie_segunda_w,
        ie_terca_w,	
        ie_quarta_w,
        ie_quinta_w,	
        ie_sexta_w,
        ie_sabado_w,
        ie_domingo_w
from 	cpoe_weekday_proc a
where	nr_sequencia = nr_cpoe_week_seq_p;

	if	(ie_segunda_w	='S') then
		ds_day_name_w	:= obter_desc_expressao(298104); -- Monday
	elsif	(ie_terca_w	='S') then
		ds_day_name_w	:= obter_desc_expressao(299301);--Tuesday
	elsif	(ie_quarta_w	='S') then
		ds_day_name_w	:= obter_desc_expressao(297137);-- Wednesday
	elsif	(ie_quinta_w	='S') then
		ds_day_name_w	:= obter_desc_expressao(297213);-- Thursday
	elsif (ie_sexta_w	='S') then
		ds_day_name_w	:= obter_desc_expressao(298487); --Friday
	elsif	(ie_sabado_w	='S') then
		ds_day_name_w	:= obter_desc_expressao(297960); --Saturday
	elsif (ie_domingo_w	='S') then
		ds_day_name_w	:= obter_desc_expressao(288200); --Sunday
	end if;

return trim(ds_day_name_w);

end get_weekday_name;
/