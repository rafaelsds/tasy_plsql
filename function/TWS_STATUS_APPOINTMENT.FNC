create or replace FUNCTION tws_status_appointment(
    nr_sequence_p NUMBER,
    ie_type_p     VARCHAR2)
  RETURN VARCHAR2
IS
  /*
  ie_type_p:
  EX - Exams - agenda_paciente
  CO - Consultation - agenda_consulta
  SE - Services - agenda_consulta
  CH - Chemoteraphy - agenda_quimio
  SU - Surgery - agenda_paciente
  */
  ie_status_w                VARCHAR2(2);
  ds_retorno_w               VARCHAR2(50);
  qt_registros_confirmacao_w NUMBER(1);
BEGIN
  IF (ie_type_p = 'EX') OR (ie_type_p = 'SU') THEN
    SELECT MAX(ie_status_agenda)
    INTO ie_status_w
    FROM agenda_paciente
    WHERE nr_sequencia = nr_sequence_p;
  elsif (ie_type_p     = 'CH') THEN
    SELECT MAX(ie_status_agenda)
    INTO ie_status_w
    FROM agenda_quimio
    WHERE nr_sequencia = nr_sequence_p;
  elsif (ie_type_p     = 'CO') OR (ie_type_p = 'SE') THEN
    SELECT MAX(ie_status_agenda)
    INTO ie_status_w
    FROM agenda_consulta
    WHERE nr_sequencia = nr_sequence_p;
  END IF;
IF (ie_status_w = 'E') OR (ie_status_w = 'AD') THEN
  --Done
  ds_retorno_w    := SUBSTR(WSUITE_UTIL_PCK.GET_WSUITE_EXPRESSION(654443),1,50);
elsif (ie_status_w = 'C') OR (ie_status_w = 'S') THEN
  --Canceled
  ds_retorno_w    := SUBSTR(WSUITE_UTIL_PCK.GET_WSUITE_EXPRESSION(322155),1,50);
elsif (ie_status_w = 'F') OR (ie_status_w = 'I') THEN
  --Lost
  ds_retorno_w := SUBSTR(WSUITE_UTIL_PCK.GET_WSUITE_EXPRESSION(305599),1,50);
ELSE
  BEGIN
    IF (ie_type_p = 'EX') OR (ie_type_p = 'SU') THEN
      SELECT COUNT(1)
      INTO qt_registros_confirmacao_w
      FROM agenda_paciente
      WHERE nr_sequencia = nr_sequence_p
        --and sysdate  > dt_agenda; modified as part of missing value mismatch--
      AND sysdate    > to_Date(pkg_date_formaters.To_varchar(hr_inicio, 'short', pkg_date_formaters.Getuserlanguagetag(wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario), NULL),'dd/mm/yyyy hh24:mi');
    elsif (ie_type_p = 'CH') THEN
      SELECT COUNT(1)
      INTO qt_registros_confirmacao_w
      FROM agenda_quimio
      WHERE nr_sequencia = nr_sequence_p
        --AND sysdate        > dt_agenda; modified as part of missing value mismatch--
      AND sysdate    > to_Date(pkg_date_formaters.To_varchar(dt_agenda, 'short', pkg_date_formaters.Getuserlanguagetag(wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario), NULL),'dd/mm/yyyy hh24:mi');
    elsif (ie_type_p = 'CO') OR (ie_type_p = 'SE') THEN
      SELECT COUNT(1)
      INTO qt_registros_confirmacao_w
      FROM agenda_consulta
      WHERE nr_sequencia = nr_sequence_p
        --AND sysdate        > dt_agenda;  modified as part of missing value mismatch--
      AND sysdate > to_Date(pkg_date_formaters.To_varchar(dt_agenda, 'short', pkg_date_formaters.Getuserlanguagetag(wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario), NULL),'dd/mm/yyyy hh24:mi');
    END IF;
    IF (qt_registros_confirmacao_w > 0) THEN
      --Lost
      ds_retorno_w := SUBSTR(WSUITE_UTIL_PCK.GET_WSUITE_EXPRESSION(305599),1,50);
    ELSE
      BEGIN
        qt_registros_confirmacao_w := 0;
        IF (ie_type_p               = 'EX') OR (ie_type_p = 'SU') THEN
          SELECT COUNT(1)
          INTO qt_registros_confirmacao_w
          FROM agenda_paciente
          WHERE nr_sequencia  = nr_sequence_p
          AND ie_autorizacao IN ('PA', 'PZ', 'P', 'PN');
        elsif (ie_type_p      = 'CH') THEN
          SELECT COUNT(1)
          INTO qt_registros_confirmacao_w
          FROM agenda_quimio a,
            paciente_atendimento b
          WHERE a.nr_seq_atendimento                                                            = b.nr_seq_atendimento
          AND a.nr_sequencia                                                                    = nr_sequence_p
          AND obter_autorizacao_quimio(b.nr_seq_atendimento, b.nr_ciclo, b.ds_dia_ciclo, 'NR') IN ('1', '5');
        elsif (ie_type_p                                                                        = 'CO') OR (ie_type_p = 'SE') THEN
          SELECT COUNT(1)
          INTO qt_registros_confirmacao_w
          FROM agenda_consulta
          WHERE nr_sequencia  = nr_sequence_p
          AND ie_autorizacao IN ('PA', 'PZ', 'P', 'PN');
        END IF;
        IF (qt_registros_confirmacao_w > 0) THEN
          --Waiting for insurance approved
          ds_retorno_w := SUBSTR(WSUITE_UTIL_PCK.GET_WSUITE_EXPRESSION(764124),1,50);
        ELSE
          BEGIN
            qt_registros_confirmacao_w := 0;
            IF (ie_type_p               = 'EX') OR (ie_type_p = 'SU') THEN
              SELECT COUNT(1)
              INTO qt_registros_confirmacao_w
              FROM agenda_paciente
              WHERE nr_sequencia = nr_sequence_p
              AND ie_autorizacao = 'N';
            elsif (ie_type_p     = 'CH') THEN
              SELECT COUNT(1)
              INTO qt_registros_confirmacao_w
              FROM agenda_quimio a,
                paciente_atendimento b
              WHERE a.nr_seq_atendimento                                                           = b.nr_seq_atendimento
              AND a.nr_sequencia                                                                   = nr_sequence_p
              AND obter_autorizacao_quimio(b.nr_seq_atendimento, b.nr_ciclo, b.ds_dia_ciclo, 'NR') = '90';
            elsif (ie_type_p                                                                       = 'CO') OR (ie_type_p = 'SE') THEN
              SELECT COUNT(1)
              INTO qt_registros_confirmacao_w
              FROM agenda_consulta
              WHERE nr_sequencia = nr_sequence_p
              AND ie_autorizacao = 'N';
            END IF;
            IF (qt_registros_confirmacao_w > 0) THEN
              --Not approved by insurance
              ds_retorno_w := SUBSTR(WSUITE_UTIL_PCK.GET_WSUITE_EXPRESSION(764128),1,50);
            ELSE
              BEGIN
                qt_registros_confirmacao_w := 0;
                IF (ie_type_p               = 'EX') OR (ie_type_p = 'SU') THEN
                  SELECT COUNT(1)
                  INTO qt_registros_confirmacao_w
                  FROM agenda_paciente
                  WHERE nr_sequencia  = nr_sequence_p
                  AND dt_confirmacao IS NOT NULL;
                elsif (ie_type_p      = 'CH') THEN
                  SELECT COUNT(1)
                  INTO qt_registros_confirmacao_w
                  FROM agenda_quimio
                  WHERE nr_sequencia  = nr_sequence_p
                  AND dt_confirmacao IS NOT NULL;
                elsif (ie_type_p      = 'CO') OR (ie_type_p = 'SE') THEN
                  SELECT COUNT(1)
                  INTO qt_registros_confirmacao_w
                  FROM agenda_consulta
                  WHERE nr_sequencia  = nr_sequence_p
                  AND dt_confirmacao IS NOT NULL;
                END IF;
                IF (qt_registros_confirmacao_w > 0) THEN
                  --Confirmed by patient
                  ds_retorno_w := SUBSTR(WSUITE_UTIL_PCK.GET_WSUITE_EXPRESSION(764126),1,50);
                ELSE
                  BEGIN
                    qt_registros_confirmacao_w := 0;
                    IF (ie_type_p               = 'EX') OR (ie_type_p = 'SU') THEN
                      SELECT COUNT(1)
                      INTO qt_registros_confirmacao_w
                      FROM agenda_paciente
                      WHERE nr_sequencia  = nr_sequence_p
                      AND dt_confirmacao IS NULL;
                    elsif (ie_type_p      = 'CH') THEN
                      SELECT COUNT(1)
                      INTO qt_registros_confirmacao_w
                      FROM agenda_quimio
                      WHERE nr_sequencia  = nr_sequence_p
                      AND dt_confirmacao IS NULL;
                    elsif (ie_type_p      = 'CO') OR (ie_type_p = 'SE') THEN
                      SELECT COUNT(1)
                      INTO qt_registros_confirmacao_w
                      FROM agenda_consulta
                      WHERE nr_sequencia  = nr_sequence_p
                      AND dt_confirmacao IS NULL;
                    END IF;
                    IF (qt_registros_confirmacao_w > 0) THEN
                      --Approved
                      ds_retorno_w := SUBSTR(WSUITE_UTIL_PCK.GET_WSUITE_EXPRESSION(283709),1,50);
                    END IF;
                  END;
                END IF;
              END;
            END IF;
          END;
        END IF;
      END;
    END IF;
  END;
END IF;
RETURN ds_retorno_w;
END tws_status_appointment;

/