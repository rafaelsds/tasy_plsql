create or replace
function esus_valida_exames_paciente(	cd_procedimento_p		esus_ficha_pro_proced.cd_procedimento%type)
					return varchar2 is

ds_retorno_w			varchar2(1) := 'S';
qt_regra_w			number(10) := 0;

begin

select	count(1)
into	qt_regra_w
from	esus_regra_exame_pac_cam a,
	esus_regra_exame_pac_pro b
where	b.nr_seq_regra_exame = a.nr_sequencia
and	a.ie_situacao = 'A'
and	b.cd_procedimento = nvl(cd_procedimento_p,0)
and	rownum = 1;

if	(qt_regra_w > 0) then
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end esus_valida_exames_paciente;
/
