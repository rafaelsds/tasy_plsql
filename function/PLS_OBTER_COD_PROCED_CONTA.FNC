create or replace
function pls_obter_cod_proced_conta(nr_seq_conta_proc_p		number,
				cd_procedimento_p		varchar2)
				return varchar is
				
ds_retorno_w		varchar2(30)	:= null;

begin
if	(nr_seq_conta_proc_p is not null) then
	select	max(cd_procedimento)
	into	ds_retorno_w
	from	pls_conta_proc
	where	nr_sequencia	= nr_seq_conta_proc_p;
elsif	(cd_procedimento_p is not null) then
	select	max(nr_sequencia)
	into	ds_retorno_w
	from	pls_conta_proc
	where	cd_procedimento	= cd_procedimento_p;
end if;
	
return ds_retorno_w;

end pls_obter_cod_proced_conta;
/