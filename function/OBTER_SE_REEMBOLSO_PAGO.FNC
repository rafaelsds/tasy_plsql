create or replace
function obter_se_reembolso_pago(	nr_seq_protocolo_conta_p	number,
				dt_liquidacao_p		varchar2)
				return varchar2 is

qt_registros_w	number(10);
retorno_w		varchar2(1);

begin

select 	count(*)
into	qt_registros_w
from	titulo_pagar t
where	t.nr_seq_reembolso = nr_seq_protocolo_conta_p
and	to_char(t.dt_liquidacao, 'YYYY') = dt_liquidacao_p;

if (qt_registros_w > 0) then
	retorno_w := 'S';
else
	retorno_w := 'N';
end if;

return	retorno_w;

end obter_se_reembolso_pago;
/