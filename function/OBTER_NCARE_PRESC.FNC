create or replace FUNCTION obter_ncare_presc (
    CD_PESSOA_FISICA_p   varchar2,
  DT_ATUALIZACAO_p     date,
    start_time_p date,
    end_time_p date
) RETURN varchar2 IS
    ds_others_w varchar2(2500);
    begin
  select   rtrim( XMLAGG(XMLELEMENT (E,prescript, chr(10))).EXTRACT('//text()').getclobval(), ',')
  into ds_others_w
  from (SELECT DISTINCT ds_horario   || ' '  ||substr(obter_desc_material(c.cd_material),1,50)||
     decode((CD_MAT_DIL||CD_MAT_RECONS||CD_MAT_RED||CD_MAT_COMP1||CD_MAT_SOLUC1),null,null,', ')||
rpad(substr(rpad(obter_desc_material(c.CD_MAT_DIL),20,',')
||rpad(obter_desc_material(c.CD_MAT_RECONS),20,',')
||rpad(obter_desc_material(c.CD_MAT_RED),20,',')
||rpad(obter_desc_material(c.CD_MAT_COMP1),20,',')
||rpad(obter_desc_material(c.CD_MAT_SOLUC1),20),1,20),23,'...')
             || ' ' ||c.QT_DOSE||' '||(select DS_UNIDADE_MEDIDA|| ' ' from UNIDADE_MEDIDA um where  um.CD_UNIDADE_MEDIDA=c.CD_UNIDADE_MEDIDA)||
             (select DS_INTERVALO from INTERVALO_PRESCRICAO ip where  ip.CD_INTERVALO= c.cd_intervalo)
            prescript
FROM
    material_order_type   mo,
    cpoe_tipo_pedido      ctp,
    prescr_mat_hor ph ,
    prescr_material b ,
    cpoe_material c
WHERE
    mo.ie_situacao = 'A'
    and  c.nr_sequencia= b.nr_seq_mat_cpoe
    and b.nr_sequencia = ph.nr_seq_material
    and b.nr_prescricao = ph.nr_prescricao
    AND ctp.nr_sequencia = mo.nr_seq_order_type
    AND nr_seq_sub_grp = 'PR'
    AND c.CD_PESSOA_FISICA = CD_PESSOA_FISICA_p
    and c.DT_LIBERACAO is not null
    and c.DT_SUSPENSAO is null
    AND c.cd_material = mo.cd_material
    and NR_SEQ_SUB_GRP ='PR'
      and ph.ds_horario >=  to_Char(start_time_p,'hh24:mi:ss')
    and ph.ds_horario <= to_Char(end_time_p,'hh24:mi:ss')

   and to_date(c.DT_INICIO,'dd-mm-yy')=to_date(DT_ATUALIZACAO_p,'dd-mm-yy'))
;
 RETURN ds_others_w;

END obter_ncare_presc;
/
