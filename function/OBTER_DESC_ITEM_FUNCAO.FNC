create or replace
function obter_desc_item_funcao( nr_sequencia_p	number,
								  cd_funcao_p	number)
				return varchar2 is
				
ds_retorno_w		varchar2(255) := '';				
				
begin


if (nr_sequencia_p > 0) and
	(cd_funcao_p > 0) then
	
	select  max(substr(obter_desc_expressao(cd_exp_desc_item,ds_item),1,255)) 
	into	ds_retorno_w		
	from 	funcao_item
	where 	cd_funcao = cd_funcao_p
	and		nr_sequencia = nr_sequencia_p;	
	
end if;


return  ds_retorno_w;
 
end obter_desc_item_funcao;
/