CREATE OR REPLACE FUNCTION cpoe_dose_formatada  (  vl_campo_p    number)
           return varchar2 IS

ds_retorno_w    varchar2(30) := '';
vl_campo_w    varchar2(30);
vl_novo_campo_w    varchar2(30);
i      number(10)  := 0;
ds_decimais_w    varchar2(20)  := '';
qt_casa_w    number(10);
ds_mascara_w    varchar2(40)  := '999,999,999,999,999,999,990';
sql_w           varchar(500);

begin

if  (vl_campo_p is not null) then
  begin


  begin
      sql_w := 'CALL OBTER_DECIMAIS_MD(:1) INTO :ds_decimais_w';
      EXECUTE IMMEDIATE sql_w USING IN vl_campo_p,
                                    OUT ds_decimais_w;
       

  exception
    when others then
      ds_decimais_w := null;
  end; 
  
  ds_mascara_w  := ds_mascara_w ||ds_decimais_w;

  if  (nvl(philips_param_pck.get_nr_seq_idioma,1) <> 1) then
    vl_campo_w  := replace(replace(replace(replace(to_char(vl_campo_p, ds_mascara_w), ',', 'X'),'.', '.'), 'X', ','),' ','');
  else
    vl_campo_w  := replace(replace(replace(replace(to_char(vl_campo_p, ds_mascara_w), ',', 'X'),'.', ','), 'X', '.'),' ','');
  end if;

  end;
end if;

ds_retorno_w  := vl_campo_w;

RETURN ds_retorno_w;
END cpoe_dose_formatada;
/