create or replace
function Obter_desc_leito_unid(nr_seq_interno_p		number)
 		    	return varchar2 is

ds_leito_w			varchar2(50);

begin

select	cd_unidade_basica || '-' || cd_unidade_compl
into	ds_leito_w
from	unidade_atendimento
where	nr_seq_interno = nr_seq_interno_p;

return	ds_leito_w;

end Obter_Desc_leito_unid;
/