create or replace
function obter_consumo_local_periodo(	cd_estabelecimento_p	number,
				cd_local_estoque_p	number,
				cd_material_p		number,
				dt_inicial_p		date,
				dt_final_p			date,
				ie_tipo_p			varchar2)
				return number is

vl_retorno_w		number(18,4);
qt_consumo_w		number(18,4);
qt_dias_w		number(10);
cd_material_estoque_w	number(6);


dt_inicial_w		date;
dt_final_w		date;
dt_mesano_inicio_w	date;
dt_mesano_fim_w		date;

begin
dt_mesano_inicio_w	:= trunc(dt_inicial_p,'mm');
dt_mesano_fim_w		:= trunc(dt_final_p,'mm');
qt_dias_w		:= obter_dias_entre_datas(dt_inicial_p, dt_final_p)+1;
dt_inicial_w		:= trunc(dt_inicial_p,'dd');
dt_final_w		:= (trunc(nvl(dt_final_p, sysdate),'dd') + 86399/86400);

select	cd_material_estoque
into	cd_material_estoque_w
from	material
where	cd_material = cd_material_p;

select	nvl(sum(a.qt_consumo),0)
into	qt_consumo_w
from	movimento_estoque_v2 a
where	dt_processo between dt_inicial_w and dt_final_w
and	a.cd_material_estoque	= cd_material_estoque_w
and	a.dt_mesano_referencia between dt_mesano_inicio_w and dt_mesano_fim_w
and	(a.cd_local_estoque 	= cd_local_estoque_p or nvl(cd_local_estoque_p,0) = 0)
and	a.cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_tipo_p = 'Q') then
	vl_retorno_w := qt_consumo_w;
elsif	(ie_tipo_p = 'M') then
	vl_retorno_w := dividir(qt_consumo_w,qt_dias_w);
end if;

return  vl_retorno_w;

end obter_consumo_local_periodo;
/