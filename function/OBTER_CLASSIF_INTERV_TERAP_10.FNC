create or replace 
function obter_classif_interv_terap_10
			(	qt_pontuacao_p	number)
				return varchar2 is

ds_retorno_w	varchar2(255);

begin
if	(qt_pontuacao_p > 60) then
	ds_retorno_w	:= obter_desc_expressao(777993);
elsif	(qt_pontuacao_p	>= 35) and
	(qt_pontuacao_p <= 60) then
	ds_retorno_w	:= obter_desc_expressao(777998);
elsif	(qt_pontuacao_p >= 20) and
	(qt_pontuacao_p <= 35)then
	ds_retorno_w	:= obter_desc_expressao(778000);
elsif	(qt_pontuacao_p >= 0) and
	(qt_pontuacao_p <= 19) then
	ds_retorno_w	:= obter_desc_expressao(778002);
end if;

return ds_retorno_w;

end obter_classif_interv_terap_10;
/
