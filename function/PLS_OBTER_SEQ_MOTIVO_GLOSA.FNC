create or replace
function pls_obter_seq_motivo_glosa
			(	cd_motivo_glosa_p	Varchar2)
				return Integer is
				
nr_seq_retorno_w		Number(10);

begin

select	max(nr_sequencia)
into	nr_seq_retorno_w
from	tiss_motivo_glosa
where	cd_motivo_tiss	= cd_motivo_glosa_p;

/*Diego 13/10/2011 - Tratamento realizado para o caso de haver mais de uma glosa com o mesmo c�digo preferir sempre a glosa que � tratada pelo plano.*/

if	(nvl(nr_seq_retorno_w,0) = 0) then
	select	max(nr_sequencia)
	into	nr_seq_retorno_w
	from	tiss_motivo_glosa
	where	cd_motivo_tiss	= cd_motivo_glosa_p;
end if;
	
return	nr_seq_retorno_w;

end pls_obter_seq_motivo_glosa;
/