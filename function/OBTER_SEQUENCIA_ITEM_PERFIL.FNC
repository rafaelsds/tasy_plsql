create or replace
function obter_sequencia_item_perfil(cd_perfil_p		number,
					   nr_seq_item_p	number)
 		    	return number is

qtde_w	number(4);
nr_retorno_w	perfil_item_pepo.nr_sequencia%type;
begin

with counter as (
select	count(*) qtde
from		perfil_item_pront
where	cd_perfil = cd_perfil_p
and		nr_sequencia = nr_seq_item_p
union
select	count(*) qtde
from		perfil_item_pepo
where	cd_perfil = cd_perfil_p
and		nr_sequencia = nr_seq_item_p)
select max(a.qtde) 
into	qtde_w
from counter a;

if	(qtde_w = 0) then
	nr_retorno_w	:= null;
else
	nr_retorno_w	:= nr_seq_item_p;
end if;

return	nr_retorno_w;

end obter_sequencia_item_perfil;
/