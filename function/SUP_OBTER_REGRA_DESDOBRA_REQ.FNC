create or replace
function sup_obter_regra_desdobra_req(	cd_material_p		number,
					cd_local_estoque_p	number,
					cd_local_destino_p	number,
					cd_operacao_estoque_p	number,
					cd_estabelecimento_p	number)
 		    	return number is

nr_seq_regra_w		regra_desdobramento_req.nr_sequencia%type;
qt_existe_w		number(10);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
nr_seq_familia_w	number(10);
ie_desdobrar_w		regra_desdobramento_req.ie_desdobrar%type;	
ie_controlado_w		varchar2(1);	
	
begin

select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material,
	nr_seq_familia,
	substr(obter_se_medic_controlado(cd_material),1,1)
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	nr_seq_familia_w,
	ie_controlado_w
from	estrutura_material_v
where	cd_material = cd_material_p;

select	max(nr_sequencia),
	max(ie_desdobrar)
into	nr_seq_regra_w,
	ie_desdobrar_w
from(	select	nr_sequencia,
		ie_desdobrar
	from	regra_desdobramento_req
	where	ie_situacao = 'A'
	and	cd_estabelecimento = cd_estabelecimento_p
	and	nvl(cd_material,cd_material_p) = cd_material_p
	and	nvl(cd_grupo_material,cd_grupo_material_w) = cd_grupo_material_w
	and	nvl(cd_subgrupo_material,cd_subgrupo_material_w) = cd_subgrupo_material_w
	and	nvl(cd_classe_material,cd_classe_material_w) = cd_classe_material_w
	and	nvl(cd_operacao_estoque,cd_operacao_estoque_p) = cd_operacao_estoque_p
	and	nvl(cd_local_estoque,cd_local_estoque_p) = cd_local_estoque_p
	and	nvl(nr_seq_familia,nvl(nr_seq_familia_w,0)) = nvl(nr_seq_familia_w,0)
	and	nvl(cd_local_destino,nvl(cd_local_destino_p,0)) = nvl(cd_local_destino_p,0)
	and	((nvl(ie_controlado,'A') = 'A') or (nvl(ie_controlado, 'A') = ie_controlado_w))
	order by cd_material,
		nr_seq_familia,
		cd_classe_material,
		cd_subgrupo_material,
		cd_grupo_material,
		cd_local_estoque,
		cd_local_destino,
		ie_controlado desc,
		cd_operacao_estoque)
where	rownum = 1;

if 	(ie_desdobrar_w <> 'S') then
	nr_seq_regra_w := null;
end if;

return	nr_seq_regra_w;

end sup_obter_regra_desdobra_req;
/