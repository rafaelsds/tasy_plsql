create or replace
function Obter_Se_Etapa_suspensa(nr_prescricao_p	Number,
				nr_seq_solucao_p	Number,
				nr_etapa_p		number,
				nr_seq_horario_p	number)
				return varchar2 is
				
ie_alteracao_w		Number(3,0);
qt_reg_w		Number(3,0) := 0;
ie_suspensa_w		varchar2(15) := 'N';
dt_suspensao_w		date;

Cursor C01 is
	select	ie_alteracao
	from	prescr_solucao_evento
	where	ie_tipo_solucao = 1
	and	nr_prescricao = nr_prescricao_p
	and	nr_seq_solucao = nr_seq_solucao_p	
	and	(((ie_alteracao = 1) or ((ie_alteracao = 2) and (obter_se_motivo_troca_frasco(nr_seq_motivo) = 'S'))) or
		(ie_alteracao = 12))
	and	ie_evento_valido = 'S';

begin

select	max(dt_suspensao)
into	dt_suspensao_w
from	prescr_mat_hor
where	nr_sequencia	= nr_seq_horario_p
and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

if	(dt_suspensao_w is not null) then
	ie_suspensa_w	:= 'S';
else
	open C01;
	loop
	fetch C01 into
		ie_alteracao_w;
	exit when C01%notfound;

		if	(ie_alteracao_w in (1,2,12)) then
			qt_reg_w := qt_reg_w + 1;
		end if;
		
		if	(qt_reg_w	= nr_etapa_p) and
			(ie_alteracao_w = 12) then
			ie_suspensa_w	:= 'S';
			exit;
		end if;	

	end loop;
	close C01;
	
end if;

return	ie_suspensa_w;

end Obter_Se_Etapa_suspensa;
/