create or replace
function eis_obter_unid_bc	(	nr_seq_interno_p	Number )
 		    	return varchar2 is

cd_unidade_basica_w	varchar2(15);
cd_unidade_compl_w	varchar2(15);
ds_retorno_w		varchar2(50);

begin

if (nvl(nr_seq_interno_p,0) > 0) then

	select	substr(max(cd_unidade_basica),1,15),
		substr(max(cd_unidade_compl),1,15)
	into	cd_unidade_basica_w,
		cd_unidade_compl_w
	from	unidade_atendimento
	where	nr_seq_interno = nr_seq_interno_p;
	
	ds_retorno_w	:= substr(cd_unidade_basica_w||' / '||cd_unidade_compl_w,1,50);
end if;

return	ds_retorno_w;

end eis_obter_unid_bc;
/