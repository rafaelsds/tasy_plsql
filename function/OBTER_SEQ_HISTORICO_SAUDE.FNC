create or replace
function Obter_Seq_Historico_saude(ie_tipo_p		varchar2)
 		    	return number is

nr_seq_apresent_w	number(10);

begin

select	max(nr_seq_apresent)
into	nr_seq_apresent_w
from	REGRA_HISTORICO_SAUDE
where	ie_tipo_historico	= ie_tipo_p;



return	nr_seq_apresent_w;

end Obter_Seq_Historico_saude;
/