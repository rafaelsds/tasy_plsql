create or replace
function pcs_obter_dados_emprestimo_ref(nr_emprestimo_p		number,
					ie_opcao_p	varchar2)
 		    	return varchar2 is

/*
ie_opcao_p
1 -  Estabelecimento
2 - Local de estoque
*/	

cd_estabelecimento_w	emprestimo.cd_estabelecimento%type;
cd_local_estoque_w	local_estoque.cd_local_estoque%type;
ds_retorno_w		varchar2(255);
				
begin

if (ie_opcao_p = '1') then

	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from 	emprestimo
	where 	nr_emprestimo = nr_emprestimo_p;

elsif (ie_opcao_p = '2') then

	select	cd_local_estoque
	into	cd_local_estoque_w
	from 	emprestimo
	where 	nr_emprestimo = nr_emprestimo_p;	
		
end if;	

if (ie_opcao_p = '1') then
	ds_retorno_w := substr(to_char(cd_estabelecimento_w),1,255);
elsif (ie_opcao_p = '2') then
	ds_retorno_w := substr(to_char(cd_local_estoque_w),1,255);	
end if;


return	ds_retorno_w;

end pcs_obter_dados_emprestimo_ref;
/