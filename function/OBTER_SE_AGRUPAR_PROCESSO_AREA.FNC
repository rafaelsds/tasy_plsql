create or replace
function obter_se_agrupar_processo_area (
					nr_processo_origem_p	number,
					nr_processo_destino_p	number,
					nr_seq_area_prep_p	number)
					return varchar2 is
					
ie_agrupar_w		varchar2(1);
ie_existe_origem_w	varchar2(1);
ie_existe_destino_w	varchar2(1);
ie_status_origem_w	varchar2(1);
ie_status_destino_w	varchar2(1);
					
begin
if	(nr_processo_origem_p is not null) and
	(nr_processo_destino_p is not null) and
	(nr_seq_area_prep_p is not null) then
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_origem_w
	from	adep_processo_area
	where	nr_seq_processo = nr_processo_origem_p;
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_destino_w
	from	adep_processo_area
	where	nr_seq_processo = nr_processo_origem_p;	
	
	if	(ie_existe_origem_w = 'S') and
		(ie_existe_destino_w = 'S') then
		
		ie_status_origem_w	:= obter_status_processo_area(nr_processo_origem_p, nr_seq_area_prep_p);
		ie_status_destino_w	:= obter_status_processo_area(nr_processo_destino_p, nr_seq_area_prep_p);		
		
		if	(ie_status_origem_w = ie_status_destino_w) then
			ie_agrupar_w	:= 'S';
		else
			ie_agrupar_w	:= 'N';
		end if;
		
	else
		ie_agrupar_w	:= 'S';
	end if;
	
end if;

return ie_agrupar_w;

end obter_se_agrupar_processo_area;
/