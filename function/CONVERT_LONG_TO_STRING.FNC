create or replace function convert_long_to_string(
   ds_nome_campo_p  varchar2,
   ds_nome_tabela_p varchar2,
   ds_condicao_p  varchar2
   )
        return varchar2 is

ds_retorno_w  varchar2(32000);
qt_lenght_w  number;
cursor_w pls_integer := dbms_sql.open_cursor;
fetch_w  pls_integer;

begin

dbms_sql.parse(cursor_w,'select '||ds_nome_campo_p||' from '||ds_nome_tabela_p||' where '||ds_condicao_p, dbms_sql.native);
dbms_sql.define_column_long(cursor_w,1);

fetch_w := dbms_sql.execute_and_fetch(cursor_w);

dbms_sql.column_value_long(cursor_w,1,32000,0, ds_retorno_w, qt_lenght_w );
dbms_sql.close_cursor(cursor_w) ;

return ds_retorno_w;

end convert_long_to_string;
/