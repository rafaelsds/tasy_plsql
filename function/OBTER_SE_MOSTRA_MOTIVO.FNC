create or replace
function obter_se_mostra_motivo(	nr_seq_dispositivo_p	number,
				nr_seq_motivo_ret_p	number)
				return varchar2 is

ds_retorno_w 	varchar2(1):= 'S';
qt_registro_w	number(10);

begin

select	count(*)
into	qt_registro_w
from	lib_motivo_retirada_disp
where	nr_seq_dispositivo = nr_seq_dispositivo_p;

if	(qt_registro_w > 0) then
	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	lib_motivo_retirada_disp
	where	nr_seq_dispositivo 	= nr_seq_dispositivo_p
	and	nr_seq_motivo_ret	= nr_seq_motivo_ret_p;
end if;	

return	ds_retorno_w;

end obter_se_mostra_motivo;
/		