create or replace
function obter_setor_padrao_rn (	nr_atendimento_p Number,
									nm_usuario_p 	 varchar2
									)
				return Number is

cd_estabelecimento_w		Number(10);
ds_regra_exibicao_setor_w	varchar2(255);
ie_clinica_w				Number(10);
ie_tipo_atendimento_w		Number(10);
cd_convenio_w				Number(10);
cd_categoria_w				Number(10);
cd_setor_atendimeto_w		Number(10);
		

begin

if	(nr_atendimento_p is not null) then

	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
	obter_param_usuario(281,624,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ds_regra_exibicao_setor_w);
	
	Select 	max(a.ie_tipo_atendimento),
			max(a.ie_clinica),
			max(b.cd_convenio),
			max(b.cd_categoria)
	into	ie_tipo_atendimento_w,
			ie_clinica_w,
			cd_convenio_w,
			cd_categoria_w	
	from	atendimento_paciente a,
			ATEND_CATEGORIA_CONVENIO b
	where	a.nr_atendimento = nr_atendimento_p
	and		a.nr_atendimento = b.nr_atendimento;

	
	SELECT   max(cd_setor_atendimento)
	into	 cd_setor_atendimeto_w
	FROM     SETOR_ATENDIMENTO
	where    ie_situacao = 'A'
	and      cd_estabelecimento = cd_estabelecimento_w
	and      Obter_Se_Setor_Regra_Entrada(cd_setor_atendimento, ie_clinica_w,ie_tipo_atendimento_w, ds_regra_exibicao_setor_w, 0, cd_convenio_w, cd_categoria_w	,  nm_usuario_p) = 'S'
	and      substr(obter_se_cosns_setor_unid_conv(cd_setor_atendimento, nr_atendimento_p),1,1) = 'S'
	and	     nvl( IE_SETOR_RN,'S') = 'S'
	and		 nvl(IE_SETOR_PADRAO_RN,'N') = 'S';
	

end if;

return cd_setor_atendimeto_w;

end obter_setor_padrao_rn;
/