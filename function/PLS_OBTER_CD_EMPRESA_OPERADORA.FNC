create or replace
function pls_obter_cd_empresa_operadora
                        (       nr_seq_intercambio_p    Number,
				cd_operadora_empresa_p	varchar2)
                        Return Varchar2 is

ds_retorno_w    Varchar2(255);

begin
	
if	(nr_seq_intercambio_p is not null)	then
	
	select  max(cd_operadora_empresa)
	into    ds_retorno_w
	from    pls_intercambio
	where	nr_sequencia = nr_seq_intercambio_p;

elsif	(cd_operadora_empresa_p is not null)	then
	
	select	max(nr_sequencia)
	into	ds_retorno_w
	from	pls_intercambio
	where	cd_operadora_empresa 	= cd_operadora_empresa_p;
end if;

return  ds_retorno_w;

end pls_obter_cd_empresa_operadora;
/