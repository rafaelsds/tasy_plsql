create or replace
function Obter_regra_mensagem_agecons (cd_agenda_p		number,
				      ie_regra_apres_p		varchar2,
				      ie_tipo_p			varchar2,
				      cd_pessoa_fisica_p	varchar2,
					  ie_classificacao_p	varchar2 default null,
					  cd_convenio_p		number default null,
					  ie_tipo_agenda_p 	varchar2 default null)
 		    	return varchar2 is

qt_regra_w		number(10,0);
nr_seq_regra_msg_w	number(10,0);
cd_medico_w		varchar2(10);
ds_mensagem_w		varchar2(4000);
ds_titulo_w		varchar2(80);
qt_idade_w		number(10,0);

ds_todas_mensagens_w	varchar2(10000);

ds_retorno_w		varchar2(10000) := '';

Cursor C01 is
	select	nr_sequencia
	from	agenda_regra_mensagem
	where	nvl(cd_medico, nvl(cd_medico_w, '0')) = nvl(cd_medico_w, '0')
	and	ie_regra_apres			= ie_regra_apres_p
	and	((qt_idade_w between nvl(qt_idade_min, qt_idade_w) and nvl(qt_idade_max, qt_idade_w)) or (qt_idade_w is null))
	and	nvl(ie_agenda, nvl(ie_tipo_agenda_p,'0')) = nvl(ie_tipo_agenda_p,'0')
	and	nvl(cd_convenio, nvl(cd_convenio_p,0)) = nvl(cd_convenio_p,0)
	and	nvl(cd_agenda, nvl(cd_agenda_p, 0)) = nvl(cd_agenda_p, 0)
	and 	nvl(ie_grupo_proced,'N') = 'N'
	and nvl(nr_seq_proc_interno, 0) = 0;
	
Cursor C02 is
	select	substr((ds_mensagem),1,4000),
		(ds_titulo_mensagem)
	from	agenda_regra_mensagem
	where	nr_sequencia	= nr_seq_regra_msg_w
	and	nvl(ie_classif_agenda,nvl(ie_classificacao_p,'0')) = nvl(ie_classificacao_p,'0')
	and	nvl(ie_agenda, nvl(ie_tipo_agenda_p,'0')) = nvl(ie_tipo_agenda_p,'0')
	and	nvl(cd_convenio, nvl(cd_convenio_p,0)) = nvl(cd_convenio_p,0)
	and	nvl(cd_agenda, nvl(cd_agenda_p, 0)) = nvl(cd_agenda_p, 0)
	and nvl(ie_grupo_proced,'N') = 'N';
	
begin

/* 	Regra Apresentação 
	S - Salvar agendamento
	C - Confirmação do agendamento      */
	
/*	Tipo
	T - Título
	M - Mensagem 	*/

select	count(*)
into	qt_regra_w
from	agenda_regra_mensagem
where	nvl(ie_agenda, 'C') = 'C'
and 	nvl(ie_grupo_proced,'N') = 'N';

if	(qt_regra_w > 0) and
	(cd_agenda_p is not null) and
	(ie_regra_apres_p is not null) then

	if	(cd_pessoa_fisica_p is not null) then
		select	to_number(obter_idade(dt_nascimento, sysdate, 'A'))
		into	qt_idade_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	else
		qt_idade_w := null;
	end if;
	
	select	cd_pessoa_fisica
	into	cd_medico_w
	from	agenda
	where	cd_agenda = cd_agenda_p;
	
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_regra_msg_w;
	exit when C01%notfound;
		begin
		open C02;
		loop
		fetch C02 into	
			ds_mensagem_w,
			ds_titulo_w;
		exit when C02%notfound;
			begin
			ds_todas_mensagens_w := substr(ds_mensagem_w||' '||ds_todas_mensagens_w,1,10000);
			end;
		end loop;
		close C02;
		
		end;
	end loop;
	close C01;
		
	if	(ie_tipo_p = 'T') then
		ds_retorno_w	:= substr(ds_titulo_w,1,4000);
	elsif	(ie_tipo_p = 'M') then
		ds_retorno_w	:= substr(ds_todas_mensagens_w,1,4000);
	end if;
		
end if;

return	ds_retorno_w;

end Obter_regra_mensagem_agecons;
/
