create or replace
function pls_obter_qtd_coop_existentes(nr_seq_lote_p	number)
 		    	return number is

vl_existentes_w		number(10) := 0;

begin
	select  count(*)
	into	vl_existentes_w	
	from    pls_w_import_cad_unimed
	where   ie_existente   = 'S'
	and     nr_seq_lote    = nr_seq_lote_p;

return	vl_existentes_w;

end pls_obter_qtd_coop_existentes;
/