create or replace
function plt_obter_prescricao_item(nr_prescricoes_p	Varchar2)
 		    	return number is

nr_prescricao_w			number(14);			
nr_prescricoes_w		varchar2(4000);
tam_lista_w			number(10,0);
ie_pos_virgula_w		number(3,0);
			
begin

if	(nr_prescricoes_p is not null) then
	nr_prescricoes_w := nr_prescricoes_p;
	while nr_prescricoes_w is not null loop
		begin
		tam_lista_w		:= length(nr_prescricoes_w);
		ie_pos_virgula_w	:= instr(nr_prescricoes_w,',');
		if	(ie_pos_virgula_w <> 0) then
			if (nr_prescricao_w is not null) then
				if (to_number(substr(nr_prescricoes_w,1,(ie_pos_virgula_w - 1))) > nr_prescricao_w) then
					nr_prescricao_w		:= to_number(substr(nr_prescricoes_w,1,(ie_pos_virgula_w - 1)));
				end if;	
			else
				nr_prescricao_w		:= to_number(substr(nr_prescricoes_w,1,(ie_pos_virgula_w - 1)));
			nr_prescricoes_w	:= substr(nr_prescricoes_w,(ie_pos_virgula_w + 1),tam_lista_w);	
			end if;
		else
			nr_prescricao_w 	:= to_number(nr_prescricoes_w);
			nr_prescricoes_w	:= '';
		end if;
		end;
	end loop;
end if;

return	nr_prescricao_w;

end plt_obter_prescricao_item;
/