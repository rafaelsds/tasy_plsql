create or replace
function existe_motivo_adm_adep(	cd_perfil_p				number,
									cd_estabelecimento_p	number,
									nm_usuario_p			varchar2,
									ie_opcao_p				varchar2,
									nr_seq_regra_p			number)
 		    					return varchar2 is

ds_retorno_w	char(1);
								
begin

select	nvl(max('S'), 'N')
into	ds_retorno_w
from	regra_adep_motivo_adm a,
		adep_motivo_admnistracao b
where	b.nr_sequencia = a.nr_seq_regra
and		nvl(a.cd_perfil,cd_perfil_p)	= cd_perfil_p
and		nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
and		nvl(a.nm_usuario_regra,nm_usuario_p) = nm_usuario_p
and     ((ie_opcao_p = 'SOL' and b.ie_solucao = 'S') or (ie_opcao_p = 'MED' and b.ie_medicamento = 'S'));

if	(ie_opcao_p = 'SOL') then

     if	(ds_retorno_w = 'N') then
	
	    select	nvl(max('S'), 'N')
		into	ds_retorno_w
		from	adep_motivo_admnistracao
		where	nr_sequencia not in (	select	b.nr_seq_regra
										from	regra_adep_motivo_adm b)
		and		ie_solucao = 'S';
	
	end if;

	return	ds_retorno_w;
	
elsif (ie_opcao_p = 'MED') then

    if	(ds_retorno_w = 'N') then
	
	    select	nvl(max('S'), 'N')
		into	ds_retorno_w
		from	adep_motivo_admnistracao
		where	nr_sequencia not in (	select	b.nr_seq_regra
										from	regra_adep_motivo_adm b)
		and		ie_medicamento = 'S';
	
	end if;

	return	ds_retorno_w;	
	
else

	select	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	regra_adep_motivo_adm a		
	where	nvl(a.cd_perfil,cd_perfil_p)	= cd_perfil_p
	and		nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
	and		nvl(a.nm_usuario_regra,nm_usuario_p) = nm_usuario_p
	and		a.nr_seq_regra = nr_seq_regra_p;
	
	if	(ds_retorno_w = 'N') then
	
	    select	nvl(max('N'), 'S')
		into	ds_retorno_w
		from	regra_adep_motivo_adm
		where	nr_seq_regra = nr_seq_regra_p;
	
	end if;

	return	ds_retorno_w;
	
end if;

end existe_motivo_adm_adep;
/
