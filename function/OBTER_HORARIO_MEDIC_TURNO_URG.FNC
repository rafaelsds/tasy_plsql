create or replace
function obter_horario_medic_turno_urg(	
				nr_prescricao_p		number,
				nr_sequencia_p		number,
				nr_seq_turno_p		number,
				qt_hora_urgente_p	number,
				qt_hora_antes_p		Number,
				qt_min_agora_p		Number)
				return varchar2 is

ds_horario_w		varchar2(15);
ds_resultado_w		varchar2(2000);

Cursor C01 is
select	b.ds_horario
from	prescr_mat_hor b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	a.nr_prescricao		= nr_prescricao_p
and	b.nr_seq_material	= nr_sequencia_p
and	b.nr_seq_turno		= nr_seq_turno_p
and	b.dt_horario		<= Obter_data_fim_turno(nr_prescricao_p,nr_seq_turno_p,qt_hora_antes_p)
and	b.dt_emissao_farmacia is null
--and	(nvl(a.dt_liberacao,a.dt_liberacao_medico) + qt_hora_urgente_p /24) > b.dt_horario
and	b.dt_horario between (nvl(a.dt_liberacao,a.dt_liberacao_medico) + qt_min_agora_p / 1440) 
	and (nvl(a.dt_liberacao_farmacia,(nvl(a.dt_liberacao,a.dt_liberacao_medico))) + qt_hora_urgente_p /24)
and	b.ie_urgente		= 'N'
and	b.dt_suspensao is null
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
order by b.dt_horario;

begin

OPEN C01;
LOOP
FETCH C01 into
	ds_horario_w;
exit when c01%notfound;
	begin
	ds_resultado_w	:= ds_resultado_w ||' '||ds_horario_w;
	end;
END LOOP;
CLOSE C01;

return ds_resultado_w;

end obter_horario_medic_turno_urg;
/