create or replace
function OBTER_SE_MEDICO_SOCIO
	(cd_estabelecimento_p	number,
	 cd_medico_p		varchar2) return varchar2 is

ie_socio_w		varchar2(100);
cd_empresa_w		number(4,0);

begin

select	nvl(max(cd_empresa),0)
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

select	nvl(max(ie_socio),'N')
into	ie_socio_w
from	medico_estabelecimento
where	cd_pessoa_fisica				= cd_medico_p
and	nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
and	nvl(cd_empresa, cd_empresa_w)			= cd_empresa_w;

return	ie_socio_w;

end OBTER_SE_MEDICO_SOCIO;
/
