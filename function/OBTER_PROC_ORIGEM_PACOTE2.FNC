CREATE OR REPLACE
FUNCTION Obter_Proc_Origem_Pacote2(nr_seq_proc_pacote_p number,
        nr_atendimento_p number)
      RETURN NUMBER IS
 
 cd_procedimento_w  Number(15);
 
BEGIN
begin
 select cd_procedimento
 into  cd_procedimento_w
 from  procedimento_paciente
 where nr_atendimento = nr_atendimento_p 
 and nr_sequencia = (
  select nr_seq_proc_origem
  from  atendimento_pacote
  where  nr_seq_procedimento = nr_seq_proc_pacote_p
  and  nr_atendimento = nr_atendimento_p );
 exception
   when others then
   cd_procedimento_w := null;
 end;
 
RETURN cd_procedimento_w;
END Obter_Proc_Origem_Pacote2;
/
