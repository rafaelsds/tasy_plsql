create or replace
function obter_se_dose_esp_ago(	nr_prescricao_p	number,
				cd_material_p	number)
			return varchar2 is

qt_existe_w	number(10);					
ie_dose_especial_agora_w varchar2(1) := 'N';

begin
select count(*)
into	qt_existe_w
from	prescr_material
where	nr_prescricao = nr_prescricao_p
and	cd_material = cd_material_p
and	ie_dose_espec_agora = 'S';

if	(qt_existe_w = 0) then

	select count(*)
	into	qt_existe_w
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and	cd_material = cd_material_p
	and	ie_urgencia = 'S';
end if;
	
if	(qt_existe_w > 0) then 
	ie_dose_especial_agora_w := 'S';
end if;
	
return ie_dose_especial_agora_w;

end obter_se_dose_esp_ago;
/

