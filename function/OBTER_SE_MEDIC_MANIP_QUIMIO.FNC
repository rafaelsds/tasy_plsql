create or replace
function obter_se_medic_manip_quimio(nr_seq_ordem_p	number)
				return varchar2	is

ie_medic_manip_w	varchar2(1) := 'N';
qt_dose_w		number(15,3);
qt_dose_ww		number(15,3);
cd_material_w		number(6);
cd_material_ww		number(6);
ie_possui_medic_w	varchar2(1);
cd_unidade_medida_w 	varchar2(5);
cd_unidade_medida_real_w varchar2(30); 
nr_seq_ficha_tecnica_w	number(10);
nr_seq_item_prescr_w	number(10);	
qt_dispensados_w	number(10);
ie_existe_w		varchar2(1);
nr_seq_prescricao_w 	number(10);
nr_prescricao_w			number(10);	
ie_tipo_manipulacao_w	varchar2(1);
cursor c01 is 
	select	a.cd_material,
		a.qt_dose,
		a.cd_unidade_medida ,
		nr_seq_ficha_tecnica,
		nr_sequencia
	from 	material b,
		can_ordem_item_prescr a
	where 	b.cd_material = a.cd_material
	and 	nr_seq_ordem = nr_seq_ordem_p;

cursor c02 is 
	select 	b.nr_seq_ficha_tecnica,
		b.cd_material
	from 	material b,
		w_preparo_quimio_lote c 
	where 	b.cd_material = c.cd_material
	and 	nr_seq_ordem = nr_seq_ordem_p
	and 	(substr(obter_tipo_material(c.cd_material,'C'),1,1) <> '1');
	
Cursor C03 is
	select 		d.nr_seq_prescricao,	
				b.nr_prescricao			
	from		can_ordem_item_prescr d,
				can_ordem_prod b
	where 		d.nr_seq_ordem = b.nr_sequencia
	and			b.nr_sequencia = nr_seq_ordem_p
	and			ie_tipo_manipulacao_w = 'T';
pragma autonomous_transaction;
begin

ie_medic_manip_w := 'N';
Obter_Param_Usuario(3130,218,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_tipo_manipulacao_w);

open C01;
loop
fetch C01 into	
	cd_material_w,
	qt_dose_w,
	cd_unidade_medida_w,
	nr_seq_ficha_tecnica_w,
	nr_seq_item_prescr_w;
exit when C01%notfound;
	begin
	
	select	max(a.cd_Material),
		sum(a.qt_dose_real),
		max(a.cd_unidade_medida_real)
	into	cd_material_ww,
		qt_dose_ww,
		cd_unidade_medida_real_w
	from 	material b,
		can_ordem_prod_mat a
	where	b.cd_material  = a.cd_material
	and	nr_seq_ordem =  nr_seq_ordem_p
	and 	a.nr_seq_item_prescr = nr_seq_item_prescr_w
	and 	((a.cd_material  = cd_material_w) or (b.nr_seq_ficha_tecnica = nr_seq_ficha_tecnica_w));

	select	nvl(max('N'),'S')
	into	ie_possui_medic_w 	
	from 	material b,
		can_ordem_prod_mat a
	where	b.cd_material  = a.cd_material
	and 	a.nr_seq_ordem =  nr_seq_ordem_p
	and 	((a.cd_material  = cd_material_w) or (b.nr_seq_ficha_tecnica = nr_seq_ficha_tecnica_w));
	
	if	(ie_possui_medic_w is null) or 
		(ie_possui_medic_w = 'S') then 
		delete from can_ordem_prod_mat where nr_seq_ordem = nr_seq_ordem_p;
		commit;
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(194958);  
		ie_medic_manip_w := 'N';		
	end if;	
	
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_seq_ficha_tecnica_w,	
	cd_material_w;
exit when C02%notfound;
	begin
	select	nvl(max('S'),'N') 
	into 	ie_existe_w
	from 	material c,
		prescr_material a
	where	a.cd_material = c.cd_material
	and 	a.nr_prescricao = (select 	max(b.nr_prescricao) 
				   from		can_ordem_item_prescr d,
						can_ordem_prod b
				   where 	d.nr_seq_ordem = b.nr_sequencia
				   and		b.nr_sequencia = nr_seq_ordem_p
				   and		a.nr_sequencia = d.nr_seq_prescricao)
	and 	((a.cd_material  = cd_material_w) or (c.nr_seq_ficha_tecnica = nr_seq_ficha_tecnica_w));
	
	if 	(ie_existe_w = 'N') then 
		delete from can_ordem_prod_mat where nr_seq_ordem = nr_seq_ordem_p;
		commit;
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(194957); 
	end if;
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_prescricao_w,
	nr_prescricao_w	;
exit when C03%notfound;

	begin
	select	count(1) 
	into	qt_dispensados_w
	from 	material b,
			prescr_material a
	where	a.cd_material = b.cd_material 
	and 	b.ie_mistura = 'S'
	and		a.nr_seq_kit = nr_seq_prescricao_w
	and 	a.nr_prescricao = nr_prescricao_w
	and 	not exists (	select 	1 
							from 	w_preparo_quimio_lote c 
							where 	c.nr_seq_ordem = nr_seq_ordem_p
							and  	c.cd_material = a.cd_material);
							
	if (nvl(qt_dispensados_w,0) = 0) then
		select	count(1) 
		into	qt_dispensados_w
		from 	material b,
				prescr_material a
		where	a.cd_material = b.cd_material 
		and 	b.ie_mistura = 'S'
		and		a.nr_seq_mat_disp = nr_seq_prescricao_w 
		and 	nr_prescricao_original = nr_prescricao_w
		and 	not exists (	select 	1 
								from 	w_preparo_quimio_lote c 
								where 	c.nr_seq_ordem = nr_seq_ordem_p
								and  	c.cd_material = a.cd_material);
	end if;
	
	if 	(nvl(qt_dispensados_w,0) > 0) then 
		delete from can_ordem_prod_mat where nr_seq_ordem = nr_seq_ordem_p;
		commit;
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(194956);
	end if;
	end;
end loop;
close c03;		

return	ie_medic_manip_w;

end obter_se_medic_manip_quimio;
/