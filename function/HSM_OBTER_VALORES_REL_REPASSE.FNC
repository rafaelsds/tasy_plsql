create or replace
function HSM_obter_valores_rel_repasse(	nr_repasse_terceiro_p		number,
					ie_opcao_p			varchar2)
 		    	return number is

vl_retorno_w	number(15,2):=0;

/*
ie_opcao
CD - com devolu��o
SD - sem devolu��o
*/
			
begin

if	(nvl(nr_repasse_terceiro_p,0) > 0) then

	if	(nvl(ie_opcao_p,'X') = 'CD') then
		select	sum(vl_repasse)
		into	vl_retorno_w
		from 	repasse_terceiro_item 
		where 	nr_repasse_terceiro = nr_repasse_terceiro_p 
		and 	ds_observacao like '%DEVOLU��O DE IMPOSTOS%';
	elsif	(nvl(ie_opcao_p,'X') = 'SD') then
		select 	sum(vl_repasse) 
		into	vl_retorno_w
		from 	repasse_terceiro_item 
		where 	nr_repasse_terceiro = nr_repasse_terceiro_p 
		and 	ds_observacao not like '%DEVOLU��O DE IMPOSTOS%';
	end if;
	
end if;

return	vl_retorno_w;

end HSM_obter_valores_rel_repasse;
/
