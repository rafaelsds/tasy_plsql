create or replace
function pls_obter_dados_matriz_ecarta
			(	nr_seq_matriz_p		number,
				ie_opcao_p		varchar2)
			return varchar2 is

ds_retorno_w			varchar2(255);

/*
N = Nome da matriz
*/

begin

if	(ie_opcao_p = 'N') then
	select	ds_matriz
	into	ds_retorno_w
	from	pls_ecarta_matriz
	where	nr_sequencia = nr_seq_matriz_p;
end if;

return	ds_retorno_w;

end pls_obter_dados_matriz_ecarta;
/