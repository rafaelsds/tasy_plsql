create or replace
function obter_se_masc_prot_lib(	nr_sequencia_p		number,
					cd_perfil_p		number,
					cd_estabelecimento_p	number,
					cd_setor_atendimento_p	number)
 		    	return varchar2 is
ds_retorno_w	varchar2(1) := 'S';
qt_registro_w	number(10);
begin

select	count(*)
into	qt_registro_w
from	med_mascara_prot_regra
where	nr_seq_protocolo = nr_sequencia_p;

if	(qt_registro_w > 0) then
	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	med_mascara_prot_regra
	where	nr_seq_protocolo 				= nr_sequencia_p
	and	nvl(cd_perfil,cd_perfil_p)			= cd_perfil_p
	and	nvl(cd_estabelecimento,cd_estabelecimento_p) 	= cd_estabelecimento_p
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_p)= cd_setor_atendimento_p;
	
end if;

return	ds_retorno_w;

end obter_se_masc_prot_lib;
/