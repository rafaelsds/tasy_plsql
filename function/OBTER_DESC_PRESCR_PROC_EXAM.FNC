create or replace
function Obter_Desc_Prescr_Proc_exam
		(cd_procedimento_p	number,
		ie_origem_proced_p	number,
		nr_seq_proc_interno_p	number,
		nr_seq_exame_p		number) 
		return varchar2 is

ds_procedimento_w		varchar2(255);
nr_seq_interno_w		number(10,0);

ie_ordem_converte_proc_w	varchar2(2):= 'EP';
ie_converte_proc_conv_w		varchar2(1);

begin

begin
select	nvl(ie_converte_proc_conv,'N'),
		nvl(ie_ordem_converte_proc,'EP')
into	ie_converte_proc_conv_w,
	ie_ordem_converte_proc_w
from	parametro_faturamento
where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;
exception
	when others then
		ie_converte_proc_conv_w:= 'N';
		ie_ordem_converte_proc_w:= 'EP';
end;

select	nvl(max(nr_seq_proc_interno_p),0)
into	nr_seq_interno_w
from	dual;

if	(ie_converte_proc_conv_w = 'S') and 
	(nvl(ie_ordem_converte_proc_w,'EP') = 'PE') then -- Proc. Interno / Exame de laboratório
		
	if	(nvl(nr_seq_exame_p,0) > 0) then
		
		select	NM_EXAME
		into	ds_procedimento_w
		from	exame_laboratorio
		where	NR_SEQ_EXAME	= nr_seq_exame_p;
		
	end if;
		
	if	(nr_seq_proc_interno_p is not null) and
		(nvl(nr_seq_interno_w,0) > 0) then
		
		select	ds_proc_exame
		into	ds_procedimento_w
		from	proc_interno
		where	nr_sequencia	= nr_seq_interno_w;
		
	end if;	
else
	if	(nr_seq_proc_interno_p is not null) and
		(nvl(nr_seq_interno_w,0) > 0) then
		select	ds_proc_exame
		into	ds_procedimento_w
		from	proc_interno
		where	nr_sequencia	= nr_seq_interno_w;	
	end if;	

	if	(nvl(nr_seq_exame_p,0) > 0) then
		select	NM_EXAME
		into	ds_procedimento_w
		from	exame_laboratorio
		where	NR_SEQ_EXAME	= nr_seq_exame_p;
	end if;	
	
end if;

if (ds_procedimento_w is null) then 
	select	obter_descricao_procedimento(cd_procedimento_p, ie_origem_proced_p)
	into	ds_procedimento_w
	from 	dual;
end if;

return	ds_procedimento_w;

end Obter_Desc_Prescr_Proc_exam;
/
