create or replace
function obter_status_radio_agenda(nr_sequencia_p	rxt_tratamento.nr_sequencia%type)
					return varchar2 is

ie_status_agenda_w rxt_agenda.ie_status_agenda%type;
qt_count_w      number(5);
ds_retorno_w    varchar2(20 char)default 'N';

begin

if	(nr_sequencia_p is not null) then
	select	count(*)
	into    qt_count_w
	from    rxt_tratamento a,
		rxt_agenda b
	where	nr_sequencia_p = a.nr_sequencia
	and     a.nr_sequencia = b.nr_seq_tratamento
	and     ie_status_agenda = 'AP';

if (qt_count_w > 0) then
    ds_retorno_w := 'S';
end if;

end if;

return ds_retorno_w;

end obter_status_radio_agenda;
/
