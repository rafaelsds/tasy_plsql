create or replace
function obter_se_processo_disp(nr_seq_horario_p		number,
				nr_seq_processo_p		number) return varchar2 is
				
ds_retorno_w	varchar2(1) := 'S';

begin

if 	(nr_seq_horario_p is not null) and
	(nr_seq_processo_p is not null) then

	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	prescr_mat_hor
	where 	nr_seq_processo	=	nr_seq_processo_p
	and	nr_sequencia	=	nr_seq_horario_p
	and	nvl(qt_dispensar_hor,0) > 0;
end if;


return	ds_retorno_w;

end obter_se_processo_disp;
/