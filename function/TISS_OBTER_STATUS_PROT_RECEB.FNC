create or replace
function TISS_OBTER_STATUS_PROT_RECEB(nr_seq_prot_receb_p	in number)
				return varchar2 is

ds_retorno_w	varchar2(255);
qt_glosa_w	number(10);
				
begin

ds_retorno_w	:= null;

if	(nvl(nr_seq_prot_receb_p,0) > 0) then

	begin
	select	1
	into	qt_glosa_w
	from	tiss_situacao_prot_glosa a,
		tiss_situacao_prot_guia b
	where	a.nr_seq_prot_guia	= b.nr_sequencia
	and	b.nr_seq_prot_receb	= nr_seq_prot_receb_p
	and	rownum = 1;
	exception
	when others then
		qt_glosa_w	:= 0;
	end;

	if	(qt_glosa_w = 0) then

		begin
		select	1
		into	qt_glosa_w
		from	tiss_situacao_prot_glosa a,
			tiss_situacao_prot_guia b,
			tiss_sit_prot_guia_item c
		where	a.nr_seq_prot_guia	= b.nr_sequencia
		and	c.nr_seq_prot_guia	= b.nr_sequencia
		and	c.nr_sequencia 		= a.nr_seq_prot_guia_item	
		and	b.nr_seq_prot_receb	= nr_seq_prot_receb_p
		and	rownum = 1;
		exception
		when others then
			qt_glosa_w	:= 0;
		end;

	end if;
	
	if	(qt_glosa_w = 0) then
		ds_retorno_w	:= wheb_mensagem_pck.get_texto(334740);--Protocolo recebido
	elsif	(qt_glosa_w > 0) then
		ds_retorno_w	:= wheb_mensagem_pck.get_texto(334741);--Protocolo recebido c/ glosa
	end if;
end if;

return	ds_retorno_w;

end TISS_OBTER_STATUS_PROT_RECEB;
/