create or replace
function permite_justif_insp_oc(	nr_seq_regra_p			number,					
				cd_perfil_p			number,
				cd_setor_atendimento_p		number,
				nm_usuario_p			varchar2)
 		    	return varchar2 is

ie_permite_justif_regra_w	varchar2(15) := 'S';
ie_permite_justif_w		varchar2(1) := 'S';
qt_registros_w			number(10);

begin

select	nvl(ie_permite_justif,'S')
into	ie_permite_justif_regra_w
from	regra_diverg_ins_oc
where	nr_sequencia = nr_seq_regra_p;

if	(ie_permite_justif_regra_w = 'N') then
	ie_permite_justif_w := 'N';
elsif	(ie_permite_justif_regra_w = 'S') then
	ie_permite_justif_w := 'S';
elsif	(ie_permite_justif_regra_w = 'D') then
	select	count(*)
	into	qt_registros_w
	from	regra_diverg_ins_oc_just
	where	nr_seq_regra = nr_seq_regra_p;
	
	if	(qt_registros_w = 0) then
		ie_permite_justif_w := 'S';
	else
		select	count(*)
		into	qt_registros_w
		from	regra_diverg_ins_oc_just
		where	nvl(cd_perfil_regra, cd_perfil_p)		= cd_perfil_p
		and	nvl(cd_setor_regra, cd_setor_atendimento_p)	= cd_setor_atendimento_p
		and	nvl(nm_usuario_regra, nm_usuario_p)		= nm_usuario_p
		and	nvl(ie_permite_justif,'S') = 'N'
		and	nr_seq_regra = nr_seq_regra_p;
			
		if	(qt_registros_w > 0) then
			ie_permite_justif_w := 'N';
		else
			ie_permite_justif_w := 'S';
		end if;
	end if;
end if;	
	
return	ie_permite_justif_w;

end permite_justif_insp_oc;
/
