create or replace
function obter_campo_disp_tipo_doc(nr_seq_tipo_doc_p	number,
					ie_campo_p		varchar2)
					return varchar2 is 

ds_retorno_p		varchar2(1) := 'N';
ie_cart_convenio_w	tipo_documentacao.ie_cart_convenio%type;
					
begin

select	nvl(ie_cart_convenio,'X')
into	ie_cart_convenio_w
from	tipo_documentacao
where	nr_sequencia = nr_seq_tipo_doc_p;

if	(nvl(ie_cart_convenio_w, 'X') = 'S') then
	begin
	if	(nvl(ie_campo_p, 'X') in ('1','2','3','4','5','6','7','8')) then
		ds_retorno_p := 'S';
	end if;
	end;
end if;

return	ds_retorno_p;

end obter_campo_disp_tipo_doc;
/