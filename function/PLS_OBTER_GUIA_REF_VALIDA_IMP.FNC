create or replace
function pls_obter_guia_ref_valida_imp(	cd_guia_ref_p		pls_conta_imp.cd_guia_principal%type,
					nr_seq_conta_p		pls_conta_imp.nr_sequencia%type,
					ie_tipo_guia_p		pls_protocolo_conta_imp.ie_tipo_guia%type,
					ie_exige_ref_guia_p	pls_oc_cta_val_guia_tiss.ie_exige_ref_guia%type,
					ie_val_guia_tiss_p	pls_oc_cta_val_guia_tiss.ie_val_guia_tiss%type)
 		    	return Varchar2 is
--O retorno aqui se da ao contrario devido ao fato de quando estiver como 'N' n�o sera gerada a ocorr�ncia e quando estiver como 'S' ir� gerar a mesma
ie_retorno_w	Varchar2(1)	:= 'N';
qt_guia_w	pls_integer;
qt_tot_guia_w	pls_integer;
begin

if 	(ie_val_guia_tiss_p = 'S') then
	if	(ie_tipo_guia_p	= 3) then
		--Se for guio de consulta somente ser� valido se estiver refer�nciando a pr�pria guia caso contr�rio ser� inv�lido
		select	count(qt_guia)
		into	qt_guia_w
		from	(
			select	1 qt_guia
			from	pls_conta	
			where	ie_status in ('A', 'F', 'L', 'P', 'U')
			and	nr_sequencia	!= nr_seq_conta_p
			and	((cd_guia = cd_guia_ref_p) or (cd_guia_imp	= cd_guia_ref_p))
			union all
			select	1 qt_guia
			from	pls_conta_imp	
			where	nr_sequencia	!= nr_seq_conta_p
			and	cd_guia_principal = cd_guia_ref_p
		) sub;
	elsif	(ie_tipo_guia_p = 4) then
		--Se for guia de SP/SADT, poder� referenciar guia de consulta SP/SADT e Resumo de interna��o
		select	count(qt_guia)
		into	qt_guia_w
		from	(
			select	1 qt_guia
			from	pls_conta	
			where	ie_status in ('A', 'F', 'L', 'P', 'U')
			and	nr_sequencia	!= nr_seq_conta_p
			and	ie_tipo_guia	in ('3','6')
			and	((cd_guia = cd_guia_ref_p) or (cd_guia_imp	= cd_guia_ref_p))
			union all
			select	1 qt_guia
			from	pls_conta_imp conta,
				pls_protocolo_conta_imp	prot	
			where	prot.nr_sequencia	= conta.nr_seq_protocolo
			and	conta.nr_sequencia	!= nr_seq_conta_p
			and	prot.ie_tipo_guia	in ('3','6')
			and	conta.cd_guia_principal = cd_guia_ref_p
		) sub;
	elsif	(ie_tipo_guia_p in (5,6)) then
		--Quando for guia de resumo de interna��o ou honor�rio individual poder� referenciar apenas guia de Resumo de interna��o
		select	count(qt_guia)
		into	qt_guia_w
		from	(
			select	1 qt_guia
			from	pls_conta	
			where	ie_status in ('A', 'F', 'L', 'P', 'U')
			and	nr_sequencia	!= nr_seq_conta_p
			and	ie_tipo_guia	in ('3','4','6')
			and	((cd_guia = cd_guia_ref_p) or (cd_guia_imp	= cd_guia_ref_p))
			union all
			select	1 qt_guia
			from	pls_conta_imp conta,
				pls_protocolo_conta_imp	prot	
			where	prot.nr_sequencia	= conta.nr_seq_protocolo
			and	conta.nr_sequencia	!= nr_seq_conta_p
			and	prot.ie_tipo_guia	in ('3','4','6')
			and	conta.cd_guia_principal = cd_guia_ref_p
		) sub;
	end if;
end if;

if	(ie_exige_ref_guia_p	= 'S') then
	select	count(qt_tot_guia)
	into	qt_tot_guia_w
	from	(
		select	1 qt_tot_guia
		from	pls_conta	
		where	ie_status in ('A', 'F', 'L', 'P', 'U')
		and	nr_sequencia	!= nr_seq_conta_p
		and	((cd_guia = cd_guia_ref_p) or (cd_guia_imp	= cd_guia_ref_p))
		union all
		select	1 qt_tot_guia
		from	pls_conta_imp	
		where	nr_sequencia	!= nr_seq_conta_p
		and	cd_guia_principal = cd_guia_ref_p
	) sub;
end if;

if	((qt_guia_w 		> 0) and 
	(ie_val_guia_tiss_p 	= 'S')) 
	or
	((qt_tot_guia_w		= 0) and
	(ie_exige_ref_guia_p	= 'S')) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end pls_obter_guia_ref_valida_imp;
/