create or replace
function lab_Obter_leito(nr_seq_interno_p		number)
 		    	return varchar2 is

ds_leito_w			varchar2(50);

begin

SELECT	cd_unidade_basica
INTO	ds_leito_w
FROM	atend_paciente_unidade
WHERE	nr_seq_interno =  nr_seq_interno_p;

return	ds_leito_w;

end lab_Obter_leito;
/