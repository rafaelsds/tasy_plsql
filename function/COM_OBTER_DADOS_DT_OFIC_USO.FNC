create or replace
function com_obter_dados_dt_ofic_uso(	nr_seq_cliente_p	number,
				ie_opcao_p	varchar2)
 		    	return date is

/* ie_opcao_p
DO - Data de oficializa��o de uso
DG - Data de gera��o
DP - Data de previs�o de uso
*/

dt_oficializacao_w	date;
dt_geracao_w		date;
dt_prev_ofic_w		date;

begin
select	max(a.dt_oficializacao),
	max(a.dt_geracao),
	max(a.dt_prev_ofic)
into	dt_oficializacao_w,
	dt_geracao_w,
	dt_prev_ofic_w
from	com_cli_ofic_uso a
where	a.nr_sequencia = (	select	max(x.nr_sequencia)
				from	com_cli_ofic_uso x
				where	x.nr_seq_cliente = nr_seq_cliente_p);

if	(nvl(ie_opcao_p,'X') = 'DO') then
	return dt_oficializacao_w;
elsif	(nvl(ie_opcao_p,'X') = 'DG') then
	return dt_geracao_w;
elsif	(nvl(ie_opcao_p,'X') = 'DP') then
	return dt_prev_ofic_w;
end if;

end com_obter_dados_dt_ofic_uso;
/