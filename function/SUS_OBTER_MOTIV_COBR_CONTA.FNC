create or replace
function sus_obter_motiv_cobr_conta (	nr_interno_conta_p	number)
 		    			return number is
			
cd_retorno_w		number(10) := null;

begin

begin
select	cd_motivo_cobranca
into	cd_retorno_w
from	sus_dados_aih_conta
where	nr_interno_conta = nr_interno_conta_p;
exception
when others then
	cd_retorno_w := null;
end;

return	cd_retorno_w;

end sus_obter_motiv_cobr_conta;
/