create or replace
function obter_valor_preco_simpro(cd_estabelecimento_p		Number,
				 cd_convenio_p			Number,
				 cd_material_p			Number,
				 cd_simpro_p			Number,
				 tx_pfb_p			Number,
				 tx_pmc_p			Number,
				 dt_base_p			Date,
				 ie_tipo_preco_conv_p		varchar2,
				 tx_pmc_neg_p			Number,
				 tx_pmc_pos_p			Number,
				 tx_pfb_neg_p			Number,
				 tx_pfb_pos_p			Number,
				 ie_dividir_indice_pmc_p	Varchar2,
				 ie_dividir_indice_pfb_p	Varchar2,
				 nr_seq_marca_p			Number)
 		    	return Number is

ie_div_indice_pmc_w	varchar2(1);
ie_div_indice_pfb_w	varchar2(1);
ie_fora_linha_w		varchar2(1);
ie_tipo_convenio_w	number(2,0);
cd_simpro_w		Number(15,0);
qt_conversao_w		number(15,4);
dt_fora_linha_w		date;
vl_preco_w		number(15,4) := 0;
nr_seq_mat_simpro_w	number(10,0);
dt_vigencia_w		Date;
ie_tipo_preco_w		Varchar2(01);
vl_pfb_w		Number(15,2);
vl_pmc_w		Number(15,2);
ie_tipo_lista_w		varchar2(1);
nr_seq_simpro_preco_w	number(10,0);
cd_estab_simpro_w	estabelecimento.cd_estabelecimento%type;

Cursor c01 is
	select	nvl(a.qt_conversao,1),
		nvl(a.cd_simpro,0),
		b.dt_fora_linha,
		a.nr_sequencia
	from	simpro_cadastro b,
		material_simpro a
	where	a.cd_simpro						= b.cd_simpro
	and	a.cd_material						= cd_material_p
	and 	a.cd_simpro						= cd_simpro_p
	and	nvl(a.nr_seq_marca, nvl(nr_seq_marca_p, 0))	= nvl(nr_seq_marca_p, 0)
	and	nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_p, 0))	= nvl(cd_estabelecimento_p, 0)
	and	nvl(a.cd_convenio, nvl(cd_convenio_p,0)) 		= nvl(cd_convenio_p,0)
	and	nvl(a.ie_tipo_convenio, nvl(ie_tipo_convenio_w,0))	= nvl(ie_tipo_convenio_w,0)
	and 	dt_base_p between nvl(a.dt_vigencia, dt_base_p) and nvl(a.dt_final_vigencia, dt_base_p)
	and	nvl(a.ie_situacao,'A')					= 'A'
	order by 
		nvl(nr_seq_marca_p, 0),
		nvl(a.cd_estabelecimento, 0),		
		nvl(a.cd_convenio,0),
		nvl(a.ie_tipo_convenio,0),
		nvl(a.dt_vigencia, dt_base_p - 5000);
			
begin


ie_div_indice_pmc_w	:= nvl(ie_dividir_indice_pmc_p,'N');
ie_div_indice_pfb_w	:= nvl(ie_dividir_indice_pfb_p,'N');
cd_estab_simpro_w 	:= wheb_usuario_pck.get_cd_estabelecimento;

select	nvl(max(ie_fora_linha_simpro),'S')
into	ie_fora_linha_w	
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_p;

if	(nvl(cd_convenio_p,0) > 0) then
	select	nvl(max(ie_tipo_convenio),0)
	into	ie_tipo_convenio_w
	from	convenio
	where	cd_convenio = cd_convenio_p;
end if;

qt_conversao_w	:= 1;
cd_simpro_w	:= 0;
dt_fora_linha_w	:= sysdate + 1000;


open C01;
loop
fetch C01 into	
	qt_conversao_w,
	cd_simpro_w,
	dt_fora_linha_w,
	nr_seq_mat_simpro_w;
exit when C01%notfound;
	begin
	qt_conversao_w		:= qt_conversao_w;
	cd_simpro_w		:= cd_simpro_w;
	dt_fora_linha_w		:= dt_fora_linha_w;
	nr_seq_mat_simpro_w	:= nr_seq_mat_simpro_w;
	end;
end loop;
close C01;
		
if	(cd_simpro_w > 0) then

	dt_vigencia_w	:= sysdate - 2000;
	
	select 	max(dt_vigencia)
	into	dt_vigencia_w
	from 	simpro_preco
	where 	cd_simpro = cd_simpro_w
	and 	dt_vigencia <= nvl(dt_base_p,sysdate)
	and     nvl(cd_estabelecimento, nvl(cd_estab_simpro_w, 0)) = nvl(cd_estab_simpro_w, 0);

	if 	((ie_fora_linha_w = 'S') or 
		 ((dt_fora_linha_w is null) or (nvl(dt_base_p,sysdate) <= dt_fora_linha_w))) then
	
		select 	nvl(max(vl_preco_fabrica),0),
			nvl(max(vl_preco_venda),0),
			nvl(max(ie_tipo_preco),'X'),
			nvl(max(ie_tipo_lista),'T'),
			max(nr_sequencia)
		into 	vl_pfb_w,
			vl_pmc_w,
			ie_tipo_preco_w,
			ie_tipo_lista_w,
			nr_seq_simpro_preco_w
		from 	simpro_preco
		where 	cd_simpro	= cd_simpro_w
		and 	dt_vigencia	= dt_vigencia_w
		and     nvl(cd_estabelecimento, nvl(cd_estab_simpro_w, 0)) = nvl(cd_estab_simpro_w, 0);
		
		if	(nvl(ie_tipo_preco_conv_p, 'C') = 'C') then
		
			if	(ie_tipo_preco_w = 'F') then
				if	(ie_div_indice_pfb_w = 'S') then
					if	(ie_tipo_lista_w in ('T','Q')) then
						vl_preco_w	:= dividir(vl_pfb_w,tx_pfb_p);
					elsif	(ie_tipo_lista_w = 'S') then
						vl_preco_w	:= dividir(vl_pfb_w,tx_pfb_pos_p);
					elsif	(ie_tipo_lista_w = 'N') then
						vl_preco_w	:= dividir(vl_pfb_w,tx_pfb_neg_p);
					else
						vl_preco_w	:= vl_pfb_w;
					end if;
				else
					if	(ie_tipo_lista_w in ('T','Q')) then
						vl_preco_w	:= vl_pfb_w * tx_pfb_p;
					elsif	(ie_tipo_lista_w = 'S') then
						vl_preco_w	:= vl_pfb_w * tx_pfb_pos_p;
					elsif	(ie_tipo_lista_w = 'N') then
						vl_preco_w	:= vl_pfb_w * tx_pfb_neg_p;
					else
						vl_preco_w	:= vl_pfb_w;
					end if;
				end if;
			else
				if	(ie_div_indice_pmc_w = 'S') then
					if	(ie_tipo_lista_w in ('T','Q')) then
						vl_preco_w	:= dividir(vl_pmc_w,tx_pmc_p);
					elsif	(ie_tipo_lista_w = 'S') then
						vl_preco_w	:= dividir(vl_pmc_w,tx_pmc_pos_p);
					elsif	(ie_tipo_lista_w = 'N') then
						vl_preco_w	:= dividir(vl_pmc_w,tx_pmc_neg_p);
					else
						vl_preco_w	:= vl_pmc_w;
					end if;
				else
					if	(ie_tipo_lista_w in ('T','Q')) then
						vl_preco_w	:= vl_pmc_w * tx_pmc_p;
					elsif	(ie_tipo_lista_w = 'S') then
						vl_preco_w	:= vl_pmc_w * tx_pmc_pos_p;
					elsif	(ie_tipo_lista_w = 'N') then
						vl_preco_w	:= vl_pmc_w * tx_pmc_neg_p;
					else
						vl_preco_w	:= vl_pmc_w;
					end if;
				end if;
			end if;
		else
			begin

			if	(nvl(ie_tipo_preco_conv_p, ie_tipo_preco_w) = 'F') then
				if	(ie_div_indice_pfb_w = 'S') then
					if	(ie_tipo_lista_w in ('T','Q')) then
						vl_preco_w	:= dividir(vl_pfb_w,tx_pfb_p);
					elsif	(ie_tipo_lista_w = 'S') then
						vl_preco_w	:= dividir(vl_pfb_w,tx_pfb_pos_p);
					elsif	(ie_tipo_lista_w = 'N') then
						vl_preco_w	:= dividir(vl_pfb_w,tx_pfb_neg_p);
					else
						vl_preco_w	:= vl_pfb_w;
					end if;

				else
					if	(ie_tipo_lista_w in ('T','Q')) then
						vl_preco_w	:= vl_pfb_w * tx_pfb_p;
					elsif	(ie_tipo_lista_w = 'S') then
						vl_preco_w	:= vl_pfb_w * tx_pfb_pos_p;
					elsif	(ie_tipo_lista_w = 'N') then
						vl_preco_w	:= vl_pfb_w * tx_pfb_neg_p;
					else
						vl_preco_w	:= vl_pfb_w;
					end if;
				end if;
			else	
				if	(ie_div_indice_pmc_w = 'S') then
					if	(ie_tipo_lista_w in ('T','Q')) then
						vl_preco_w	:= dividir(vl_pmc_w,tx_pmc_p);
					elsif	(ie_tipo_lista_w = 'S') then
						vl_preco_w	:= dividir(vl_pmc_w,tx_pmc_pos_p);
					elsif	(ie_tipo_lista_w = 'N') then
						vl_preco_w	:= dividir(vl_pmc_w,tx_pmc_neg_p);
					else
						vl_preco_w	:= vl_pmc_w;
					end if;
				else
					if	(ie_tipo_lista_w in ('T','Q')) then
						vl_preco_w	:= vl_pmc_w * tx_pmc_p;
					elsif	(ie_tipo_lista_w = 'S') then
						vl_preco_w	:= vl_pmc_w * tx_pmc_pos_p;
					elsif	(ie_tipo_lista_w = 'N') then
						vl_preco_w	:= vl_pmc_w * tx_pmc_neg_p;
					else
						vl_preco_w	:= vl_pmc_w;
					end if;
				end if;
			end if;


			end;
		end if;
		vl_preco_w		:= dividir(vl_preco_w, qt_conversao_w);
	end if;
end if;

return	vl_preco_w;

end obter_valor_preco_simpro;
/
