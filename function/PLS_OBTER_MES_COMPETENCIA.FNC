create or replace
function pls_obter_mes_competencia
			(	nr_seq_competencia_p	number,
				cd_estabelecimento_p	number)
				return Varchar2 is

ds_retorno_w			Varchar2(10);

begin
select	to_char(dt_mes_competencia ,'mm/yyyy')
into	ds_retorno_w
from	pls_competencia
where	nr_sequencia = nr_seq_competencia_p
and	cd_estabelecimento	= cd_estabelecimento_p;

return	ds_retorno_w;

end pls_obter_mes_competencia;
/
