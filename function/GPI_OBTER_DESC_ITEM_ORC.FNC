create or replace
function gpi_obter_desc_item_orc(	nr_seq_orc_item_p		number,
				ie_opcao_p		number)
				return varchar2 is


cd_centro_custo_w		gpi_orc_item.cd_centro_custo%type;
cd_material_w			gpi_orc_item.cd_material%type;
ds_atividade_w			gpi_cron_etapa.nm_etapa%type;
ds_conta_w			gpi_plano.ds_conta%type;
ds_centro_custo_w		centro_custo.ds_centro_custo%type;
ds_estrutura_w			gpi_cron_etapa.nm_etapa%type;
ds_fornecedor_w			gpi_orc_item.ds_fornecedor%type;
ds_material_w			material.ds_material%type;
ds_modelo_w			gpi_orc_item.ds_modelo%type;
ds_retorno_w			varchar2(255);
ds_sep_w			varchar2(3);
nr_seq_etapa_w			gpi_orc_item.nr_seq_etapa%type;
vl_orcado_w			gpi_orc_Item.vl_orcado%type;
ds_retorno_final_w		varchar2(255);
ds_subs_retorno_w		varchar2(255);

begin

ds_sep_w	:= '/';

select	max(cd_material),
	max(substr(gpi_obter_desc_conta(nr_seq_plano),1,80)),
	max(nr_seq_etapa),
	max(cd_centro_custo),
	max(vl_orcado),
	max(ds_modelo),
	max(ds_fornecedor)
into	cd_material_w,
	ds_conta_w,
	nr_seq_etapa_w,
	cd_centro_custo_w,
	vl_orcado_w,
	ds_modelo_w,
	ds_fornecedor_w
from	gpi_orc_item a
where	a.nr_sequencia	= nr_seq_orc_item_p;

if	(cd_material_w is not null) then
	ds_material_w	:= substr(obter_desc_material(cd_material_w),1,255);
end if;

if	(nr_seq_etapa_w is not null) then
	ds_estrutura_w	:= substr(gpi_obter_dados_etapa(nr_seq_etapa_w,'E'),1,255);
	ds_atividade_w	:= substr(gpi_obter_desc_etapa(nr_seq_etapa_w),1,100);
end if;

if	(cd_centro_custo_w is not null) then
	ds_centro_custo_w	:= substr(obter_desc_centro_custo(cd_centro_custo_w),1,255);
end if;

if	(ie_opcao_p = 1) then

	begin
	
	if	(ds_atividade_w is not null) then
		ds_retorno_w 		:= substr(ds_atividade_w,1,255);
		ds_retorno_final_w 	:= substr(ds_retorno_w,1,255);
	end if;
	
	if	(ds_conta_w is not null) then
		ds_retorno_w 		:= substr(ds_sep_w || substr(ds_conta_w,1,255),1,255);
		ds_retorno_final_w 	:= substr(substr(ds_retorno_final_w,1,255) || substr(ds_retorno_w,1,255),1,255);
		
	end if;
	
	if	(ds_centro_custo_w is not null) then
		ds_retorno_w 		:= substr(ds_sep_w || substr(ds_centro_custo_w,1,255),1,255);
		ds_retorno_final_w 	:= substr(substr(ds_retorno_final_w,1,255) || substr(ds_retorno_w,1,255),1,255);
			
	end if;	
	
	if	(ds_fornecedor_w is not null) then
		ds_retorno_w 		:= substr(ds_sep_w || substr(ds_fornecedor_w,1,255),1,255);
		ds_retorno_final_w 	:= substr(substr(ds_retorno_final_w,1,255) || substr(ds_retorno_w,1,255),1,255);
				
	end if;
	if	(ds_modelo_w is not null) then
		ds_retorno_w 		:= substr(ds_sep_w || substr(ds_modelo_w,1,255),1,255);
		ds_retorno_final_w 	:= substr(substr(ds_retorno_final_w,1,255) || substr(ds_retorno_w,1,255),1,255);
	
	end if;
	
	if (campo_mascara_virgula(vl_orcado_w) is not null) then
		ds_retorno_w 		:= substr(ds_sep_w || substr(campo_mascara_virgula(vl_orcado_w),1,255),1,255);
		ds_retorno_final_w 	:= substr(substr(ds_retorno_final_w,1,255) || substr(ds_retorno_w,1,255),1,255);
	end if;
	
	if	((ds_atividade_w is null) 	and 
		(ds_conta_w is null) 		and 
		(ds_centro_custo_w is null) 	and 
		(ds_fornecedor_w is null)   	and 
		(ds_modelo_w is null) 	    	and 
		(campo_mascara_virgula(vl_orcado_w) is null)) then
		ds_retorno_final_w := ' ';
	end if;
	
	if	(substr(ds_retorno_final_w,1,1) = '/') then
		ds_retorno_final_w := substr(ds_retorno_final_w,2,255);
	end if;
							
	end;
end if;

return ds_retorno_final_w;

end gpi_obter_desc_item_orc;
/