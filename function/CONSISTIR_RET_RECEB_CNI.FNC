create or replace
function CONSISTIR_RET_RECEB_CNI
			(nr_seq_retorno_p	number,
			nr_seq_receb_p		number,
			ie_opcao_p		varchar2) 
			return varchar2 is

/*ie_opcao_p
R	Data de referÍncia do protocolo
L	Data do lote dos protocolos
*/

ds_retorno_w		varchar2(10);
dt_referencia_w		date;
count_w			number(10);
			
begin
if	(ie_opcao_p = 'L') then
	select	min(a.dt_mesano_referencia)
	into	dt_referencia_w
	from	lote_protocolo a,
		protocolo_convenio b,
		conta_paciente c,
		convenio_retorno_item d
	where	a.nr_sequencia		= b.nr_seq_lote_protocolo
	and	b.nr_seq_protocolo	= c.nr_seq_protocolo
	and	c.nr_interno_conta	= d.nr_interno_conta
	and	d.nr_seq_retorno	= nr_seq_retorno_p;
end if;

if	(ie_opcao_p = 'R') or
	(dt_referencia_w is null) then
	
	select	min(b.dt_mesano_referencia)
	into	dt_referencia_w
	from	protocolo_convenio b,
		conta_paciente c,
		convenio_retorno_item d
	where	b.nr_seq_protocolo	= c.nr_seq_protocolo
	and	c.nr_interno_conta	= d.nr_interno_conta
	and	d.nr_seq_retorno	= nr_seq_retorno_p;
	
end if;

if	(dt_referencia_w is not null) then

	begin
	select	1
	into	count_w
	from	movto_banco_pend_baixa a,
		movto_banco_pend b
	where	a.nr_seq_movto_pend		= b.nr_sequencia
	and	a.nr_seq_conv_receb		= nr_seq_receb_p
	and	trunc(b.dt_credito,'month')	< trunc(dt_referencia_w,'month')
	and	rownum = 1;
	exception
	when others then
		count_w	:= 0;
	end;
end if;	

if	(count_w > 0) then
	ds_retorno_w	:= 'S';
else
	ds_retorno_w	:= 'N';
end if;

return ds_retorno_w;

end CONSISTIR_RET_RECEB_CNI;
/