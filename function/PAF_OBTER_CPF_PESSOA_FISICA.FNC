create or replace
function paf_Obter_Cpf_Pessoa_Fisica
		(cd_pessoa_fisica_p	varchar2)
		return varchar2 is

nr_cpf_w		varchar2(11);

begin

if	(cd_pessoa_fisica_p is not null) then
	begin
	
	select	nr_cpf
	into	nr_cpf_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

	end;
end if;


return nr_cpf_w;

end paf_Obter_Cpf_Pessoa_Fisica;
/
