create or replace
function obter_data_lib_proced	(cd_agenda_p			number,
				cd_profissional_p		varchar2,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				cd_pessoa_fisica_p		varchar2,
				nr_seq_proc_interno_p		number,
				nr_seq_agenda_p			number)
				return date is

nr_seq_tempo_w		number(10,0);
qt_minuto_adic_w		number(15);
qt_anos_w		number(5);
nr_seq_agenda_w		number(10,0);
dt_liberada_w		date := (sysdate - 365);

dt_agenda_w		date;
ie_dia_semana_w		number(001,0);

begin

if (nr_seq_agenda_p is not null) then

	select	max(hr_inicio)
	into	dt_agenda_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	
	if (dt_agenda_w is not null) then 
		ie_dia_semana_w	:= obter_cod_dia_semana(dt_agenda_w);
	else
		ie_dia_semana_w	:= null;
	end if;
	
end if;

if	((cd_procedimento_p is not null) and
	 (ie_origem_proced_p is not null)) or
	(nr_seq_proc_interno_p is not null) then
	
	begin
	select	to_number(obter_idade(dt_nascimento, sysdate, 'A'))
	into	qt_anos_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	exception
	when others then
		qt_anos_w := 0;
	end;
	
	
	if	(nr_seq_proc_interno_p > 0) then
	
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_tempo_w
		from	tempo_procedimento
		where	nr_seq_proc_interno = nr_seq_proc_interno_p
		and	cd_agenda = cd_agenda_p
		and	nvl(cd_pessoa_fisica, nvl(cd_profissional_p,'0'))	= nvl(cd_profissional_p,'0')
		and	qt_anos_w  between nvl(qt_idade_minima,0) and nvl(qt_idade_maxima,999)
		and	nvl(qt_minuto,0) > 0
		and	((ie_dia_semana = ie_dia_semana_w) or ((ie_dia_semana = 9) and (ie_dia_semana_w not in (1,7))) or (ie_dia_semana is null));
		
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_agenda_w
		from	agenda_paciente
		where	nr_seq_proc_interno = nr_seq_proc_interno_p
		and	cd_agenda = cd_agenda_p
		and	nr_sequencia <> nr_seq_agenda_p;
		
	elsif	(cd_procedimento_p > 0) and
		(ie_origem_proced_p > 0) then
		
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_tempo_w
		from	tempo_procedimento
		where	cd_procedimento = cd_procedimento_p
		and	ie_origem_proced = ie_origem_proced_p
		and	cd_agenda	= cd_agenda_p
		and	nvl(cd_pessoa_fisica, nvl(cd_profissional_p,'0'))	= nvl(cd_profissional_p,'0')
		and	qt_anos_w  between nvl(qt_idade_minima,0) and nvl(qt_idade_maxima,999)
		and	nvl(qt_minuto,0) > 0
		and	((ie_dia_semana = ie_dia_semana_w) or ((ie_dia_semana = 9) and (ie_dia_semana_w not in (1,7))) or (ie_dia_semana is null));
		
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_agenda_w
		from	agenda_paciente
		where	cd_procedimento = cd_procedimento_p
		and	ie_origem_proced = ie_origem_proced_p
		and	cd_agenda = cd_agenda_p
		and	nr_sequencia <> nr_seq_agenda_p;
	end if;

	if (nr_seq_tempo_w > 0) and
	   (nr_seq_agenda_w > 0) then
	
		select	NVL(qt_minuto_adic,0)
		into	qt_minuto_adic_w
		from	tempo_procedimento
		where	nr_sequencia = nr_seq_tempo_w;
		
		IF	(qt_minuto_adic_w > 0) THEN
			select	hr_inicio + ((nr_minuto_duracao + qt_minuto_adic_w)/1440)			
			into	dt_liberada_w
			from	agenda_paciente
			where	nr_sequencia = nr_seq_agenda_w;
		END IF;
	end if;
end if;

return dt_liberada_w;

end obter_data_lib_proced;
/
