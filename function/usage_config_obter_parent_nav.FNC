CREATE OR REPLACE FUNCTION usage_config_obter_parent_nav(
                nr_sequencia_p NUMBER)
        RETURN NUMBER
IS
        retorno_w NUMBER(10);
BEGIN
        SELECT  *
        INTO    retorno_w
        FROM
                (SELECT aa.nr_seq_obj_sup
                FROM    objeto_schematic bb,
                        objeto_schematic aa
                WHERE   aa.nr_seq_obj_sup                  = bb.nr_sequencia(+)
                    AND ((bb.ie_tipo_objeto               IN ('TVN','T','IT','DDM','VMI')
                     OR (bb.ie_tipo_objeto                 = 'MN'
                    AND bb.ie_tipo_obj_navegador           = 'TG'))
                     OR (aa.ie_tipo_objeto                 = 'T'
                    AND bb.ie_tipo_objeto                  = 'MN'
                    AND bb.ie_tipo_obj_navegador           = 'T')
                     OR (aa.ie_tipo_objeto                 = 'DDM'
                    AND bb.ie_tipo_objeto                  = 'MN'
                    AND bb.ie_tipo_obj_navegador           = 'DDM'))
                        CONNECT BY nocycle aa.nr_sequencia = prior aa.nr_seq_obj_sup
                        START WITH aa.nr_sequencia         = nr_sequencia_p
                ORDER BY level
                )
        WHERE   1      =1
            AND rownum = 1;
        RETURN retorno_w;
END usage_config_obter_parent_nav;
/