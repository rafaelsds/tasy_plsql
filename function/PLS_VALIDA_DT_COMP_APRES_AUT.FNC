create or replace
function pls_valida_dt_comp_apres_aut (	qt_meses_permitido_p		number,
					ie_comp_fechada_p		varchar2 default 'S',
					dt_competencia_p		pls_lote_apres_automatica.dt_mes_competencia%type)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Validar a data de competencia do lote de apresentação automática
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ds_retorno_w	varchar2(1);
count_w			integer;

begin
ds_retorno_w := 'S';

if	(dt_competencia_p is not null) then
	if	(trunc(dt_competencia_p,'year') < to_date('01/01/2000')) then
		ds_retorno_w := 'A';
	elsif	(trunc(dt_competencia_p,'month') > trunc(add_months(sysdate,qt_meses_permitido_p),'month')) then
		ds_retorno_w := 'N';
	elsif  	((ie_comp_fechada_p = 'N') and (trunc(dt_competencia_p,'month') < trunc(sysdate,'month')) and (dt_competencia_p < sysdate)) then
		
		select	count(1)
		into	count_w
		from	fin_mes_ref
		where	dt_referencia = dt_competencia_p
		and		dt_fechamento is not null;
		
		if (count_w > 0) then
			ds_retorno_w := 'D';
		end if;

	end if;	
end if;

return	ds_retorno_w;

end pls_valida_dt_comp_apres_aut;
/