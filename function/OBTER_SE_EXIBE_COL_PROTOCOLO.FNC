create or replace function obter_se_exibe_col_protocolo(
     nr_prescricao_p number) return varchar2 is

ds_retorno_w varchar2(4000) := null;

begin
select decode (count(1),0, obter_desc_expressao(327114),obter_desc_expressao(327113))
into ds_retorno_w
from prescr_procedimento
where nr_prescricao = nr_prescricao_p
and cd_protocolo is not null
and nr_Seq_exame is not null;

return ds_retorno_w;

end obter_se_exibe_col_protocolo;
/
