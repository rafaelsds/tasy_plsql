create or replace
function Obter_Se_unid_regra
		(nr_seq_regra_p			number,
		 nr_seq_unidade_p			number)
		return varchar2 is

ie_liberado_w			varchar2(01)	:= 'S';
qt_regra_w			number(10,0);

begin

select	count(*)
into	qt_regra_w
from	sl_regra_servico_unid
where	nr_seq_regra_servico = nr_seq_regra_p;

if	(qt_regra_w <> 0) then
	
	select	nvl(max('S'), 'N')
	into	ie_liberado_w
	from	sl_regra_servico_unid
	where	nr_seq_regra_servico	= nr_seq_regra_p
	and	nr_seq_unidade		= nr_seq_unidade_p;

end if;

return	ie_liberado_w;

end Obter_Se_unid_regra;
/
