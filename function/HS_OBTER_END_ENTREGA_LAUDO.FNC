create or replace
function hs_obter_end_entrega_laudo(nr_seq_forma_laudo_p	number,
					cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is
			
ie_tipo_complemento_w	number(2);
ds_end_entrega_w	varchar2(300);
			
begin

ds_end_entrega_w := '';

if	(nvl(nr_seq_forma_laudo_p,0) > 0) and 
	(cd_pessoa_fisica_p is not null) then

	select	nvl(max(ie_tipo_complemento),0)
	into	ie_tipo_complemento_w
	from	forma_entrega_laudo
	where	nr_sequencia = nr_seq_forma_laudo_p;
	
	if	(ie_tipo_complemento_w > 0) then
	
		select	substr(endereco,1,300)
		into	ds_end_entrega_w
		from	(select ds_endereco 	|| 
		         decode(nr_endereco,null,'  ',', '|| to_char(nr_endereco)) || ' ' || 
			 ds_complemento || ' ' || 
			 ds_bairro || ' ' || 
			 ds_municipio || ' ' ||  
			 sg_estado || ' ' ||
			 decode(nr_ddd_telefone,null,'' ||to_char(nr_telefone),to_char(nr_ddd_telefone)||'-'||nr_telefone)
			  endereco 
			from	compl_pessoa_fisica a
			where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	a.ie_tipo_complemento = ie_tipo_complemento_w
			order	by a.nr_sequencia desc);

	
	end if;

end if;

return	ds_end_entrega_w;

end hs_obter_end_entrega_laudo;
/