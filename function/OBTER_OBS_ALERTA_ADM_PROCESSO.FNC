create or replace
function Obter_obs_alerta_adm_processo(	nr_seq_processo_p	number,
										cd_tipos_obs_adm_p	varchar2 ,
										ie_tipo_solucao_p	number )
 		    	return varchar2 is

ds_observacoes_w	varchar2(4000);
ds_obs_w			varchar2(4000);
dt_atualizacao_w	date;
nr_prescricao_w		prescr_medica.nr_prescricao%type;
nr_seq_solucao_w	number(15);

cursor cSolucoes is
	select	distinct b.ds_observacao, b.dt_atualizacao
	from	prescr_solucao_evento b
	where	b.nr_prescricao = nr_prescricao_w
	and		b.ie_alteracao = 9
	and		b.ie_tipo_solucao = ie_tipo_solucao_p
	and		coalesce(b.nr_seq_solucao, b.nr_seq_procedimento, b.nr_seq_material, b.nr_seq_nut, b.nr_seq_nut_neo) = nr_seq_solucao_w
	and		b.nr_seq_tipo_obs is not null
	and		obter_se_valor_contido(b.nr_seq_tipo_obs, cd_tipos_obs_adm_p) = 'S'	
	and     nvl(ie_mostra_adep,'S') = 'S'
	and		nvl(ie_evento_valido,'S') = 'S'
	order by b.dt_atualizacao desc;
	
cursor cItens is
	select	distinct b.ds_observacao, b.dt_atualizacao
	from	prescr_mat_hor a,
			prescr_mat_alteracao b
	where	a.nr_prescricao = b.nr_prescricao
	and		b.ie_alteracao = 11
	and		a.nr_sequencia = coalesce(b.nr_seq_horario,a.nr_sequencia)
	and		a.nr_seq_material = coalesce(b.nr_seq_prescricao,a.nr_seq_material)
	and		a.nr_seq_processo = nr_seq_processo_p
	and		b.nr_seq_tipo_obs is not null
	and		obter_se_valor_contido(b.nr_seq_tipo_obs, cd_tipos_obs_adm_p) = 'S'
	and     nvl(ie_mostra_adep,'S') = 'S'
	and		nvl(ie_evento_valido,'S') = 'S'
	order by b.dt_atualizacao desc;

begin

/*
Tipo de solucao:
1 - SOL
2 - SNE
3 - HM
4 - NPA
5 - NPN
6 - NAN
7 - NPP
*/

if	(nr_seq_processo_p is not null) and
	(cd_tipos_obs_adm_p is not null) then
	select	coalesce(max(nr_prescricao),0),
			coalesce(max(nr_seq_solucao),0)
	into	nr_prescricao_w,
			nr_seq_solucao_w
	from	adep_processo
	where	nr_sequencia = nr_seq_processo_p;

		if	(nr_seq_solucao_w > 0) and
			(nr_prescricao_w > 0) and 
			(ie_tipo_solucao_p > 0) then
			open cSolucoes;
			loop
			fetch cSolucoes into	
				ds_obs_w,
				dt_atualizacao_w;
			exit when cSolucoes%notfound;
				begin
				ds_observacoes_w	:= substr(ds_observacoes_w || '*' || ds_obs_w || chr(10),1,2000);
				end;
			end loop;
			close cSolucoes;
		else
			open cItens;
			loop
			fetch cItens into	
				ds_obs_w,
				dt_atualizacao_w;
			exit when cItens%notfound;
				begin
				ds_observacoes_w	:= substr(ds_observacoes_w || '*' || ds_obs_w || chr(10),1,2000);
				end;
			end loop;
			close cItens;
		end if;
end if;

return	ds_observacoes_w;

end Obter_obs_alerta_adm_processo;
/