create or replace
function obter_se_prim_cid_princ(	nr_atendimento_p	number,
					nr_seq_interno_p	number)
					return varchar2 is

ie_retorno_w		varchar2(1)	:=	'N';
nr_seq_interno_w	number(10);

begin

if	(nr_atendimento_p > 0) and
	(nr_seq_interno_p > 0) then
	begin
	
	select	min(nr_seq_interno)
	into	nr_seq_interno_w
	from	diagnostico_doenca
	where  	ie_classificacao_doenca	= 'P'
	and	nr_atendimento		= nr_atendimento_p;

	if	(nr_seq_interno_p = nr_seq_interno_w) then 
		ie_retorno_w	:= 'S';
	end if;
	end;
end if;
	
return	ie_retorno_w;

end obter_se_prim_cid_princ;
/