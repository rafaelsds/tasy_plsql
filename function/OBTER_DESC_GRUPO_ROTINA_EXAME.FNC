create or replace
function obter_desc_grupo_rotina_exame(nr_seq_grupo_p in number) return varchar2 is 

ds_retorno_w	varchar2(255);
begin
if	(nr_seq_grupo_p is not null) then 
	if  (nr_seq_grupo_p > 0) then
		select	max(ds_grupo)
		into 	ds_retorno_w
		from	grupo_rotina_exame
		where	nr_sequencia = nr_seq_grupo_p;
	else
		ds_retorno_w := Wheb_mensagem_pck.get_texto(308981); --'Sem Grupo';
	end if;
end if;
return ds_retorno_w;
end obter_desc_grupo_rotina_exame;
/
