create or replace
function mdc_obter_pontuacao_escala(    ie_escala_p         number,
                                        nr_sequencia_p      number) return number is
nm_tabela_w         varchar2(255);
ds_retorno_w        number;

begin

nm_tabela_w := obter_tabela_escala(ie_escala_p);

case nm_tabela_w
	when 'ESCALA_SAPS' then
		select max(qt_saps) 
        into ds_retorno_w 
        from escala_saps 
        where nr_sequencia = nr_sequencia_p;
	when 'TISS_INTERV_TERAPEUTICA_10' then
		select max(qt_pontuacao)
        into ds_retorno_w 
        from tiss_interv_terapeutica_10
        where nr_sequencia = nr_sequencia_p;
end case;

return ds_retorno_w;

end mdc_obter_pontuacao_escala;
/