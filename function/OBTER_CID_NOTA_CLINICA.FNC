CREATE OR REPLACE
FUNCTION OBTER_CID_NOTA_CLINICA(		
					cd_pessoa_Fisica_p		Number,
					dt_evolucao_w		date)
					RETURN Varchar2 IS

dt_diagnostico_w		date;
cd_diagnostico_w		Varchar2(240);
nr_atendimento_w		number(15);
cd_pessoa_fisica_w		number(15);

BEGIN

select	max(dt_diagnostico),
		max(nr_atendimento),
		max(cd_doenca)
into 	dt_diagnostico_w,
		nr_atendimento_w,
		cd_diagnostico_w
from 	diagnostico_doenca a
where	a.dt_diagnostico < dt_evolucao_w
and 	OBTER_PESSOA_ATENDIMENTO(nr_atendimento,'C') = cd_pessoa_fisica_p
and 	DT_LIBERACAO is not null
and		DT_INATIVACAO is null;

RETURN cd_diagnostico_w;

END OBTER_CID_NOTA_CLINICA;
/
