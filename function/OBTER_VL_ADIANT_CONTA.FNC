create or replace
function Obter_Vl_Adiant_Conta
		(nr_interno_conta_p		number,
		ie_vinculado_p			varchar2) 
		return number is

vl_adiantamento_w	number(15,4)	:= 0;

begin

if	(ie_vinculado_p	= 'N') then
	select	sum(VL_ADIANTAMENTO)
	into	vl_adiantamento_w
	from	CONTA_PAC_ADIANT_V
	where	nr_interno_conta	= nr_interno_conta_p;
else
	select	sum(VL_ADIANTAMENTO)
	into	vl_adiantamento_w
	from	conta_paciente_adiant
	where	nr_interno_conta	= nr_interno_conta_p;
end if;

return	vl_adiantamento_w;

end Obter_Vl_Adiant_Conta;
/