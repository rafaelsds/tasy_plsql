create or replace
function obter_qtd_prpop_canceladas
 		    	return number is
qt_prop_canceladas_w	number(10);			
begin

select	count(*)
into	qt_prop_canceladas_w
from	com_cliente_proposta
where	nm_usuario_cancelamento is not null;

return	qt_prop_canceladas_w;

end obter_qtd_prpop_canceladas;
/