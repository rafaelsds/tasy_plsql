create or replace
function obter_se_regra_tipo_doc_lib(nr_seq_regra_p	number)
 		    	return varchar2 is

cd_perfil_w		perfil.cd_perfil%type := obter_perfil_ativo;
ds_retorno_w		varchar2(1) := 'S';
qt_registro_w		number(10);

begin
if	(nr_seq_regra_p > 0) then
	select	count(*)
	into	qt_registro_w
	from	regra_tipo_doc_obr_pf_lib
	where	nr_seq_regra_tipo_doc = nr_seq_regra_p;
	
	if	(qt_registro_w > 0) then
		begin
		select	'S'
		into	ds_retorno_w
		from	regra_tipo_doc_obr_pf_lib
		where	nr_seq_regra_tipo_doc 	= nr_seq_regra_p
		and	cd_perfil 		= cd_perfil_w
		and	rownum < 2;
		
		exception
		when others then
			ds_retorno_w := 'N';
		end;
	end if;
end if;

return	ds_retorno_w;

end obter_se_regra_tipo_doc_lib;
/
