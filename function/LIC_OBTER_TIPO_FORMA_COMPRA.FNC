create or replace
function lic_obter_tipo_forma_compra(	nr_seq_forma_compra_p			number)
 		    	return varchar2 is

ie_forma_compra_w			varchar2(15);			
			
begin

if	(nvl(nr_seq_forma_compra_p,0) <> 0) then
	begin
	select	ie_forma_compra
	into	ie_forma_compra_w
	from	reg_lic_forma_compra
	where	nr_sequencia = nr_seq_forma_compra_p;
	end;
end if;
return	ie_forma_compra_w;

end lic_obter_tipo_forma_compra;
/