CREATE OR REPLACE
FUNCTION Obter_Hora_Atraso_Laudo(		
					nr_seq_laudo_p	Number)
					RETURN Number IS


dt_prev_entrega_w		Date;
dt_liberacao_w		Date;
qt_atraso_w			Number;

BEGIN

qt_atraso_w			:= 0;
select	dt_prev_entrega,
		dt_liberacao
into		dt_prev_entrega_w,
		dt_liberacao_w
from		Laudo_paciente
where		nr_sequencia	= nr_seq_laudo_p;

if	(dt_liberacao_w is not null) and
	(dt_prev_entrega_w is not null) and
	((dt_liberacao_w - dt_prev_entrega_w) < 100) and
	(dt_liberacao_w > dt_prev_entrega_w) then
	qt_atraso_w	:= round((dt_liberacao_w - dt_prev_entrega_w) * 24);
end if;

if	(qt_atraso_w is null) then
	qt_atraso_w := 0;
end if;

RETURN qt_atraso_w;

END Obter_hora_atraso_laudo;
/

