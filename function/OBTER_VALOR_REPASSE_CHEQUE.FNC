create or replace 
function OBTER_VALOR_REPASSE_CHEQUE
		(nr_seq_proc_repasse_p	number,
		nr_seq_mat_repasse_p	number,
		nr_seq_cheque_p		number) return number is

-- Function para retornar o valor de repasse do item proporcional ao valor do cheque

nr_interno_conta_w		number(10,0);
vl_item_repasse_w		number(15,2);
vl_tot_proc_repasse_w		number(15,2);
vl_total_repasse_w		number(15,2);
vl_cheque_w			number(15,2);
vl_proporcional_w		number(15,2);

begin

if	(nvl(nr_seq_proc_repasse_p,0) > 0) then

	select	b.nr_interno_conta
	into	nr_interno_conta_w
	from	procedimento_paciente b,
		procedimento_repasse a
	where	a.nr_seq_procedimento	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_proc_repasse_p;

	select	vl_repasse
	into	vl_item_repasse_w
	from	procedimento_repasse
	where	nr_sequencia		= nr_seq_proc_repasse_p;
else
	select	b.nr_interno_conta
	into	nr_interno_conta_w
	from	material_atend_paciente b,
		material_repasse a
	where	a.nr_seq_material	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_mat_repasse_p;

	select	vl_repasse
	into	vl_item_repasse_w
	from	material_repasse
	where	nr_sequencia		= nr_seq_mat_repasse_p;

end if;

select	nvl(sum(b.vl_repasse),0)
into	vl_tot_proc_repasse_w
from	procedimento_repasse b,
	procedimento_paciente a
where	a.nr_sequencia		= b.nr_seq_procedimento
and	a.nr_interno_conta	= nr_interno_conta_w;

select	nvl(sum(b.vl_repasse),0) + vl_tot_proc_repasse_w
into	vl_total_repasse_w
from	material_repasse b,
	material_atend_paciente a
where	a.nr_sequencia		= b.nr_seq_material
and	a.nr_interno_conta	= nr_interno_conta_w;

select	vl_cheque
into	vl_cheque_w
from	cheque_cr
where	nr_seq_cheque	= nr_seq_cheque_p;


vl_proporcional_w		:= dividir_sem_round(vl_item_repasse_w, vl_total_repasse_w) * vl_cheque_w;

return	vl_proporcional_w;

end OBTER_VALOR_REPASSE_CHEQUE;
/
