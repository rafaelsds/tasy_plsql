create or replace
function obter_tipo_repasse(nr_seq_tipo_p	number)
		return varchar2 is
				
ie_retorno		varchar2(255);	

begin

	select	SubStr(ds_tipo_repasse,1,255) 
	into	ie_retorno
	from	tipo_repasse
	where	nr_sequencia	= nr_seq_tipo_p;

return	ie_retorno;

end obter_tipo_repasse;
/