create or replace
function	obter_controle_medic(
						cd_material_p			number,
						cd_estabelecimento_p	number,
						ie_controle_medico_p	number,
						ie_tipo_atendimento_p	number,
						cd_setor_atendimento_p	number,
						ie_clinica_p			number) return number is
					
ie_controle_medico_w	number(2);
begin

select	max(ie_controle_medico)
into	ie_controle_medico_w
from	rep_regra_controle_atb
where	cd_material = cd_material_p
and	cd_estabelecimento = cd_estabelecimento_p
and		nvl(ie_tipo_atendimento,nvl(ie_tipo_atendimento_p,0))	= nvl(ie_tipo_atendimento_p,0)
and		nvl(cd_setor_atendimento,nvl(cd_setor_atendimento_p,0)) = nvl(cd_setor_atendimento_p,0)
and		nvl(ie_clinica,nvl(ie_clinica_p,0)) 						= nvl(ie_clinica_p,0);

if	(ie_controle_medico_w is null) then
	ie_controle_medico_w := ie_controle_medico_p;
end if;

return ie_controle_medico_w;

end obter_controle_medic;
/
