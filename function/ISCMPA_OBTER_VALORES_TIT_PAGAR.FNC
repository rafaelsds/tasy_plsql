create or replace
function iscmpa_obter_valores_tit_pagar
		(nr_titulo_p	number,
		dt_referencia_p	date,
		ie_opcao_p	varchar2) return number is

/* ie_opcao_p

'D' vl_descontos
'OD' vl_outras_deducoes
'J' vl_juros
'M' vl_multa
'OA' vl_outros_acrescimos
'P' vl_pago
'B' vl_baixa
'JM' vl_juros + vl_multa
'VO' valor original
'VT' valor tributos
'VOT' valor original + altera��o de valor + tributos
'VA' valor abatimento
'VPSA' valor pago sem abatimento
'GP' Glosa principal
'GA' Glosa auxiliar
'VG' Valor da glosa
*/

vl_retorno_w			number(15,2);
vl_descontos_w			number(15,2);
vl_outras_deducoes_w		number(15,2);
vl_juros_w			number(15,2);
vl_multa_w			number(15,2);
vl_outros_acrescimos_w		number(15,2);
vl_pago_w			number(15,2);
vl_baixa_w			number(15,2);
vl_original_w			number(15,2);
vl_imposto_w			number(15,2);
vl_alteracao_w			number(15,2);
ie_trib_atualiza_saldo_w	varchar2(1);
vl_abatimento_w			number(15,2);
vl_glosa_ap_w			number(15,2);
vl_glosa_w			number(15,2);
vl_desconto_w			number(15,2);
vl_glosa_tit_w			number(15,2);

begin
select 	max(vl_titulo)
into	vl_original_w
from	titulo_pagar
where	nr_titulo	= nr_titulo_p
and	trunc(dt_emissao,'dd')	<= nvl(dt_referencia_p, dt_emissao);

begin
select	sum(nvl(a.vl_descontos,0)),
	sum(nvl(a.vl_outras_deducoes,0)),
	sum(nvl(a.vl_juros,0)),
	sum(nvl(a.vl_multa,0)),
	sum(nvl(a.vl_outros_acrescimos,0)),
	sum(nvl(a.vl_pago,0)),
	sum(nvl(a.vl_baixa,0)),
	sum(decode(b.ie_tipo_consistencia,3,nvl(a.vl_pago,0),0))
into	vl_descontos_w,
	vl_outras_deducoes_w,
	vl_juros_w,
	vl_multa_w,
	vl_outros_acrescimos_w,
	vl_pago_w,
	vl_baixa_w,
	vl_abatimento_w
from	tipo_baixa_cpa b,
	titulo_pagar_baixa a
where	a.cd_tipo_baixa		= b.cd_tipo_baixa(+)
and	a.nr_titulo		= nr_titulo_p
and	trunc(a.dt_baixa,'dd')	<= nvl(dt_referencia_p, a.dt_baixa);
exception
	when others then
		vl_descontos_w		:= 0;
		vl_outras_deducoes_w	:= 0;
		vl_juros_w		:= 0;
		vl_multa_w		:= 0;
		vl_outros_acrescimos_w	:= 0;
		vl_pago_w		:= 0;
		vl_baixa_w		:= 0;
end;

select	nvl(sum(a.vl_glosa_ato_coop_princ),0) vl_glosa_ap,
	nvl(sum(a.vl_glosa_ato_coop_aux),0) vl_glosa,
	nvl(sum(a.vl_glosa),0)
into	vl_glosa_ap_w,
	vl_glosa_w,
	vl_glosa_tit_w
from	titulo_pagar_baixa a
where	nr_titulo = nr_titulo_p;

select	nvl(max(b.ie_trib_atualiza_saldo),'S')
into	ie_trib_atualiza_saldo_w
from	parametros_contas_pagar b,
	titulo_pagar a
where	a.nr_titulo		= nr_titulo_p
and	a.cd_estabelecimento	= b.cd_estabelecimento;

begin
select	nvl(sum(b.vl_imposto), 0)
into	vl_imposto_w
from	titulo_pagar_imposto b,
	titulo_pagar a
where	b.nr_titulo			= nr_titulo_p
and	a.nr_titulo			= b.nr_titulo
and	b.ie_pago_prev		= 'V'
and	(a.nr_seq_nota_fiscal is null or
	not exists
	(select	1
	from	nota_fiscal_trib x
	where	x.cd_tributo	= b.cd_tributo
	and	x.nr_sequencia	= a.nr_seq_nota_fiscal))
and	a.nr_repasse_terceiro	is null
and	IE_TRIB_ATUALIZA_SALDO_w	= 'S';
exception
	when others then
	vl_imposto_w	:= 0;
end;

vl_retorno_w		:= 0;
if	(ie_opcao_p = 'D') then
	vl_retorno_w		:= vl_descontos_w;
elsif	(ie_opcao_p = 'OD') then
	vl_retorno_w		:= vl_outras_deducoes_w;
elsif	(ie_opcao_p = 'J') then
	vl_retorno_w		:= vl_juros_w;
elsif	(ie_opcao_p = 'M') then
	vl_retorno_w		:= vl_multa_w;
elsif	(ie_opcao_p = 'OA') then
	vl_retorno_w		:= vl_outros_acrescimos_w;
elsif	(ie_opcao_p = 'P') then
	vl_retorno_w		:= vl_pago_w;
elsif	(ie_opcao_p = 'B') then
	vl_retorno_w		:= vl_baixa_w;
elsif	(ie_opcao_p = 'JM') then
	vl_retorno_w		:= vl_juros_w + vl_multa_w;
elsif	(ie_opcao_p = 'VO') then
	vl_retorno_w		:= vl_original_w;
elsif	(ie_opcao_p = 'VT') then
	vl_retorno_w		:= vl_imposto_w;
elsif	(ie_opcao_p = 'VOT') then
	select 	max(vl_titulo)
	into	vl_original_w
	from	titulo_pagar
	where	nr_titulo	= nr_titulo_p;

	select	nvl(sum(vl_anterior - vl_alteracao),0)
	into	vl_alteracao_w
	from	titulo_pagar_alt_valor
	where	nr_titulo	= nr_titulo_p
	and	dt_alteracao	< nvl(dt_referencia_p, dt_alteracao + 1);

	vl_retorno_w		:= (vl_original_w - vl_alteracao_w);
elsif	(ie_opcao_p = 'VA') then
	vl_retorno_w		:= vl_abatimento_w;
elsif	(ie_opcao_p = 'VPSA') then
	vl_retorno_w		:= nvl(vl_pago_w,0) - nvl(vl_abatimento_w,0);
elsif	(ie_opcao_p = 'GP') then
	vl_retorno_w		:= vl_glosa_ap_w;
elsif	(ie_opcao_p = 'GA') then
	vl_retorno_w		:= vl_glosa_w;
elsif	(ie_opcao_p = 'VG') then
	vl_retorno_w		:= vl_glosa_tit_w;
end if;

return	vl_retorno_w;

end iscmpa_obter_valores_tit_pagar;
/