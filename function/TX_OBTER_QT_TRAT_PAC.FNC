create or replace 
function TX_obter_qt_trat_pac (	nr_seq_receptor_p	number,
				cd_pessoa_fisica_p	varchar2,
				cd_estabelecimento_p	number)
				return			number is

qt_tratamento_w		number(15);

begin

qt_tratamento_w	:= 0;

if	(nr_seq_receptor_p is not null) and
	(cd_pessoa_fisica_p is not null) then

	select	count(*)
	into	qt_tratamento_w
	from	paciente_tratamento a
	where	a.dt_final_tratamento is null
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.nr_seq_receptor = nr_seq_receptor_p
	and	substr(tx_obter_se_paciente_ativo(a.nr_seq_receptor),1,1) = 'S';

	if	(qt_tratamento_w > 1) then
		qt_tratamento_w	:= 1;
	end if;

end if;

return qt_tratamento_w;
	
end TX_obter_qt_trat_pac;
/