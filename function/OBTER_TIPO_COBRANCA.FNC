create or replace
function obter_tipo_cobranca (nr_sequencia_p	number) return varchar2 is

ds_tipo_cobranca_w	varchar2(254);

begin

select	ds_tipo_cobranca
into	ds_tipo_cobranca_w
from	tipo_cobranca
where	nr_sequencia = nr_sequencia_p;

return	ds_tipo_cobranca_w;

end;
/