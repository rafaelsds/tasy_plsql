create or replace 
function get_if_medicine_class_cb(
				nr_seq_intern_p		number,
				cd_class_medicine_p	varchar2)
			return varchar2 is

nr_encounter_w			atendimento_paciente.nr_atendimento%type;
dt_entry_encounter_w	atendimento_paciente.dt_entrada%type;
cd_setor_atend_w		setor_atendimento.cd_setor_atendimento%type;
qt_count_w				number(10);

begin

if (nr_seq_intern_p is not null) and
	(cd_class_medicine_p is not null) then

	select 	max(nr_atendimento)
	into	nr_encounter_w
	from	w_pan_paciente
	where	nr_seq_interno = nr_seq_intern_p;

	if (nr_encounter_w is not null) then

		select	count(*)
		into	qt_count_w
		from	adep_pend_prev_v
		where	nr_atendimento	= nr_encounter_w
		and 	obter_classe_material(cd_item) in (select regexp_substr(cd_class_medicine_p,'[^,]+', 1, level) from dual connect by regexp_substr(cd_class_medicine_p, '[^,]+', 1, level) is not null)
		and 	dt_horario between sysdate -2 and sysdate +3
		and		ie_tipo_item in ('M', 'MAT');

	end if;

	if (qt_count_w > 0) then
		return 'S';
	else
		return 'N';
	end if;

end if;

return 'N';

end get_if_medicine_class_cb;
/
