create or replace
function obter_dado_clin_exame_result(	nr_seq_resultado_p	number,
					nr_seq_exame_p		number,
					nr_sequencia_p		number)
 		    	return varchar2 is
			
ds_dado_clinico_w	varchar2(255);

begin

select	max(substr(b.ds_dado_clinico,1,255)) ds_dado_clinico
into	ds_dado_clinico_w
from 	exame_laboratorio c,
        exame_lab_resultado e,
        exame_lab_result_item d,
        prescr_procedimento b
where 	b.nr_prescricao 	= e.nr_prescricao
and 	e.nr_seq_resultado 	= d.nr_seq_resultado
and 	b.nr_sequencia 		= d.nr_seq_prescr
and 	d.nr_seq_exame 		= c.nr_seq_exame
and	d.nr_seq_resultado 	= nr_seq_resultado_p
and	d.nr_seq_exame 		= nr_seq_exame_p
and	d.nr_sequencia 		= nr_sequencia_p;

return	ds_dado_clinico_w;

end obter_dado_clin_exame_result;
/