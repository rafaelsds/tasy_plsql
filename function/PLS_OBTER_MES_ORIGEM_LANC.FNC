create or replace
function pls_obter_mes_origem_lanc
			(	nr_seq_lancamento_p	pls_segurado_mensalidade.nr_sequencia%type)
 		    	return date is

dt_retorno_w		date;
nr_seq_item_origem_w	pls_segurado_mensalidade.nr_seq_item_origem%type;
nr_seq_reaj_retro_w	pls_segurado_mensalidade.nr_seq_reaj_retro%type;

begin

select	nr_seq_item_origem,
	nr_seq_reaj_retro,
	dt_competencia_origem
into	nr_seq_item_origem_w,
	nr_seq_reaj_retro_w,
	dt_retorno_w
from	pls_segurado_mensalidade
where	nr_sequencia	= nr_seq_lancamento_p;

if	(dt_retorno_w is null) then
	if	(nr_seq_item_origem_w is not null) then
		select	b.dt_mesano_referencia
		into	dt_retorno_w
		from	pls_mensalidade_seg_item	a,
			pls_mensalidade_segurado	b
		where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
		and	a.nr_sequencia	= nr_seq_item_origem_w;
	elsif	(nr_seq_reaj_retro_w is not null) then
		select	dt_referencia_reajuste
		into	dt_retorno_w
		from	pls_reajuste_cobr_retro
		where	nr_sequencia	= nr_seq_reaj_retro_w;
	end if;
end if;

return	dt_retorno_w;

end pls_obter_mes_origem_lanc;
/