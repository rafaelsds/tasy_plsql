create or replace
function obter_desc_pergunta_os
			(	nr_seq_pergunta_p		Number)
	 		    	return Varchar2 is
				
ds_retorno_w			Varchar2(255);

begin

select	nvl(max(ds_pergunta),'')
into	ds_retorno_w
from	pergunta_os
where	nr_sequencia	= nr_seq_pergunta_p;

return	ds_retorno_w;

end obter_desc_pergunta_os;
/
