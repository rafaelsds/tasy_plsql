create or replace
function obter_se_usuario_ausente(	nm_usuario_p	varchar2,
				dt_dia_p		date)
 		    	return varchar2 is

ds_retorno_w	varchar2(15) := 'N';

begin
select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	ausencia_tasy
where	nm_usuario_ausente = nm_usuario_p
and	nvl(substr(obter_se_data_periodo(nvl(dt_dia_p,sysdate), dt_inicio, dt_fim),1,1),'N') = 'S';

return	ds_retorno_w;

end obter_se_usuario_ausente;
/