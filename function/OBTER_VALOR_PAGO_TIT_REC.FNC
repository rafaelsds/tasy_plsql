create or replace
function obter_valor_pago_tit_rec(nr_titulo_p		number)
 		    	return number is

vl_calculado_w		number(15,4);
vl_recebido_w		number(15,2);
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
vl_outros_acrescimos_w	number(15,2);
vl_rec_maior_w		number(15,2);
vl_descontos_w		number(15,2);
					
begin

select	nvl(sum(vl_recebido),0),
	nvl(sum(vl_juros),0),
	nvl(sum(vl_multa),0),
	nvl(sum(vl_outros_acrescimos),0),
	nvl(sum(vl_rec_maior),0),
	nvl(sum(vl_descontos),0)
into	vl_recebido_w,
	vl_juros_w,
	vl_multa_w,
	vl_outros_acrescimos_w,
	vl_rec_maior_w,
	vl_descontos_w
from	titulo_receber_liq
where 	nr_titulo = nr_titulo_p;

vl_calculado_w := nvl(vl_recebido_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0) + nvl(vl_outros_acrescimos_w,0) + nvl(vl_rec_maior_w,0);

return	vl_calculado_w;

end obter_valor_pago_tit_rec;
/
