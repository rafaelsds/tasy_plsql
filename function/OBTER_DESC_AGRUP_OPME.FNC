create or replace
function	obter_desc_agrup_opme(	nr_seq_opme_p number)
					return varchar is
ds_protocolo_w	varchar2(255);

begin

select	max(a.ds_protocolo)
into	ds_protocolo_w
from   	agenda_pac_opme_agrup a,
	agenda_pac_opme b
where  	a.nr_sequencia = b.nr_seq_agrup
and	b.nr_sequencia = nr_seq_opme_p;

return 	ds_protocolo_w;

end obter_desc_agrup_opme;
/