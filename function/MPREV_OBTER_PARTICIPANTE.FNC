create or replace
function mprev_obter_participante(cd_pessoa_fisica_p number)
 		    	return number is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verifica se a pessoa fisica esta na medicina preventiva
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:  Localizar Pessoas
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_participante_w number;

begin
	
if	(cd_pessoa_fisica_p is not null) then
	begin
	select 	nr_sequencia
	into	nr_seq_participante_w
	from 	mprev_participante
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
	exception
		when others then
			nr_seq_participante_w := null;
	end;
end if;

return	nr_seq_participante_w;

end mprev_obter_participante;
/