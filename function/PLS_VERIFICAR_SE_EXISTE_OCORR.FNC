create or replace function pls_verificar_se_existe_ocorr(	nr_seq_guia_p			Number,
								nr_seq_requisicao_p		Number,
								nr_seq_execucao_p		Number,
								nr_seq_proc_p			Number,
								nr_seq_mat_p			Number,
								nr_seq_ocorrencia_p		Number)
								return Varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Verificar se j� existe a ocorr�ncia gerada nos itens/cabe�alho nas guias,
requisi��es e execu��es.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(1);
qt_ocorrencia_w		number(10);

begin

if	(nr_seq_guia_p 	is not null) then
	if	(nr_seq_proc_p is not null) then
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_guia_plano 	= nr_seq_guia_p
		and	nr_seq_proc		= nr_seq_proc_p
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;

	elsif	(nr_seq_mat_p is not null) then
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_guia_plano 	= nr_seq_guia_p
		and	nr_seq_mat		= nr_seq_mat_p
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;
		
	else
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_guia_plano 	= nr_seq_guia_p		
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;	
	end if;

elsif	(nr_seq_requisicao_p 	is not null) then
	if	(nr_seq_proc_p is not null) then
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_requisicao 	= nr_seq_requisicao_p
		and	nr_seq_proc		= nr_seq_proc_p
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;

	elsif	(nr_seq_mat_p is not null) then
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_mat		= nr_seq_mat_p
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;
		
	else
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_requisicao	= nr_seq_requisicao_p		
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;	
	end if;

elsif	(nr_seq_execucao_p 	is not null) then
	if	(nr_seq_proc_p is not null) then
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_execucao 	= nr_seq_execucao_p
		and	nr_seq_proc		= nr_seq_proc_p
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;

	elsif	(nr_seq_mat_p is not null) then
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_execucao 	= nr_seq_execucao_p
		and	nr_seq_mat		= nr_seq_mat_p
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;
	
	else
		select	count(1)
		into    qt_ocorrencia_w
		from	pls_ocorrencia_benef
		where	nr_seq_execucao 	= nr_seq_execucao_p		
		and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;
	end if;
end if;

if	(qt_ocorrencia_w > 0) then
	ds_retorno_w	:= 'S';
else
	ds_retorno_w 	:= 'N';
end if;

return	ds_retorno_w;

end pls_verificar_se_existe_ocorr;
/

