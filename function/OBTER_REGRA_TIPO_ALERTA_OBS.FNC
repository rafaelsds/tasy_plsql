create or replace
function obter_regra_tipo_alerta_obs	(cd_perfil_p		perfil.cd_perfil%type)
 		    	return varchar2 is

ie_retorno_w	adep_tipo_observacao.ie_alerta%type;

cursor cRegras is
select	a.ie_alerta,
		b.cd_perfil cd_perfil
from	adep_tipo_observacao a,
		regra_adep_tipo_obs b
where	a.nr_sequencia = b.nr_seq_regra
and		b.cd_perfil = cd_perfil_p
union
select	a.ie_alerta,
		99999 cd_perfil
from	adep_tipo_observacao a
where	not exists(	select	1
					from	regra_adep_tipo_obs b
					where 	a.nr_sequencia = b.nr_seq_regra)
order by cd_perfil;

begin

for cRegras_w in cRegras loop
	ie_retorno_w := cRegras_w.ie_alerta;
	exit;
end loop;

return	ie_retorno_w;

end obter_regra_tipo_alerta_obs;
/
