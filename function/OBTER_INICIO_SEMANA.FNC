create or replace
function Obter_inicio_semana
		(dt_referencia_p		date)
		return date is

ie_dia_semana_w		number(02);
dt_inicio_semana_w	date;
		
begin

ie_dia_semana_w := pkg_date_utils.get_WeekDay(dt_referencia_p);

dt_inicio_semana_w	:= (trunc(dt_referencia_p, 'dd') - ie_dia_semana_w) + 1;

return	dt_inicio_semana_w;

end Obter_inicio_semana;
/
