create or replace
function OBTER_DESC_COLUNA_FLUXO_AGRUP (
			nr_seq_agrupador_p	number,
			nr_coluna_p		number)
 		    	return varchar2 is

ds_coluna_w	fluxo_caixa_agrup_coluna.ds_coluna%type;
qt_cont_w	number(10) := 1;

Cursor C01 is
select	a.ds_coluna
from	fluxo_caixa_agrup_coluna a
where	a.nr_seq_agrupador = nr_seq_agrupador_p
order by nr_ordem_apres;

begin

open C01;
loop
fetch C01 into	
	ds_coluna_w;
exit when C01%notfound or qt_cont_w = nr_coluna_p;
	begin
	qt_cont_w	:= qt_cont_w + 1;
	end;
end loop;
close C01;

return	ds_coluna_w;

end OBTER_DESC_COLUNA_FLUXO_AGRUP;
/