create or replace
function Obter_Regra_envio_email_compra(
				ie_tipo_mensagem_p	Number,
				cd_estabelecimento_p	Number,
				cd_material_p		number)
		return varchar2 is


cd_grupo_material_w		number(03,0);
cd_subgrupo_w			number(03,0);
cd_classe_material_w		number(05,0);
nr_regras_w			number(05,0);
qt_existe_w			number(05,0);
ie_retorno_w			varchar2(1);

begin

ie_retorno_w		:= 'S';

select	count(*)
into	nr_regras_w
from	regra_envio_email_compra
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(nr_regras_w	> 0) then
	begin
	ie_retorno_w		:= 'N';
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_material_w,
		cd_subgrupo_w,
		cd_classe_material_w
	from	estrutura_material_v
	where 	cd_material	= cd_material_p;

	select	count(*)
	into	qt_existe_w
	from	regra_envio_email_compra
	where	cd_estabelecimento					= cd_estabelecimento_p
	and	nvl(cd_grupo_material, cd_grupo_material_w)	= cd_grupo_material_w
	and	nvl(cd_subgrupo_material, cd_subgrupo_w)		= cd_subgrupo_w
	and	nvl(cd_classe_material, cd_classe_material_w)	= cd_classe_material_w
	and	nvl(cd_material, cd_material_p)			= cd_material_p
	and	ie_tipo_mensagem					= ie_tipo_mensagem_p;

	if	(qt_existe_w > 0) then
		ie_retorno_w		:= 'S';
	end if;
	end;
end if;

return ie_retorno_w;

end Obter_Regra_envio_email_compra;
/