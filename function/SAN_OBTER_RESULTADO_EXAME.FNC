create or replace
function san_obter_resultado_exame(	nr_seq_exame_lote_p	number,
					nr_seq_exame_p		number)
 		    	return varchar2 is
			
ds_resultado_w	varchar2(255);
			
begin
if	(nr_seq_exame_lote_p is not null) and 
	(nr_seq_exame_p is not null) then
	
	select	max(b.ds_valor)
		into ds_resultado_w
	from	san_exame_realizado x,
		san_exame a,
		san_exame_valor b
	where	a.nr_sequencia = x.nr_seq_exame
	and 	x.nr_seq_exame = b.nr_seq_exame
	and 	x.nr_seq_exame = nr_seq_exame_p
	and 	x.nr_seq_exame_lote = nr_seq_exame_lote_p
	and 	x.ds_resultado = to_char(b.nr_sequencia)
	and 	a.cd_tipo_resultado IS NULL;
	
	if	( ds_resultado_w is null ) then
	
		select	max(x.ds_resultado)
			into	ds_resultado_w
		from	san_exame_realizado x
		where	x.nr_seq_exame = nr_seq_exame_p
		and	x.nr_seq_exame_lote = nr_seq_exame_lote_p;
		
	end if;
	
end if;

return	ds_resultado_w;

end san_obter_resultado_exame;
/
