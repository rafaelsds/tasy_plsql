create or replace
function sup_obter_se_kit_consistido(
				nr_seq_kit_estoque_p	number)
 		    	return varchar2 is

qt_componentes_w	number(10);
qt_nao_consistidos_w	number(10);
ds_retorno_w		varchar2(1) := 'N';
		
begin

select  count(*)
into	qt_nao_consistidos_w
from    kit_estoque_comp a
where   a.nr_seq_kit_estoque = nr_seq_kit_estoque_p
and     a.ie_gerado_barras not in ('S','E');

select  count(*)
into	qt_componentes_w
from    kit_estoque_comp a
where   a.nr_seq_kit_estoque = nr_seq_kit_estoque_p
and	a.ie_gerado_barras not in ('E');

if	(qt_componentes_w > 0) and
	(qt_nao_consistidos_w = 0) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end sup_obter_se_kit_consistido;
/
