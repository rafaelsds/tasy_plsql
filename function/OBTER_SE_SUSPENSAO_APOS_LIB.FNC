create or replace
function obter_se_suspensao_apos_lib(	nr_prescricao_p		number,
					dt_liberacao_p		date,
					nr_sequencia_p		number,
					ie_tipo_p		varchar)
					return varchar is

dt_suspensao_w	date;	
dt_liberacao_w	date;
ds_retorno_w	varchar2(1):='N';

begin

dt_liberacao_w	:= dt_liberacao_p;


if	(ie_tipo_p = 'M') OR 
	(ie_tipo_p = 'S') OR
	(ie_tipo_p = '2') then
	select	max(dt_suspensao)
	into	dt_suspensao_w
	from	prescr_material
	where	nr_prescricao 	= nr_prescricao_p
	and	((nr_sequencia	= nr_sequencia_p) or 
		 (nr_sequencia_diluicao = nr_sequencia_p));
elsif	(ie_tipo_p = 'D') then
	select	max(dt_suspensao)
	into	dt_suspensao_w
	from	prescr_dieta
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia 	= nr_sequencia_p;
elsif	(ie_tipo_p = 'R') then
	select	max(dt_suspensao)
	into	dt_suspensao_w
	from	prescr_recomendacao
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia 	= nr_sequencia_p;
elsif	(ie_tipo_p = 'P') then
	select	max(dt_suspensao)
	into	dt_suspensao_w
	from	prescr_procedimento
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia 	= nr_sequencia_p;
elsif	(ie_tipo_p = 'G') then
	select	max(dt_suspensao)
	into	dt_suspensao_w
	from	prescr_gasoterapia
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia 	= nr_sequencia_p;
elsif	(ie_tipo_p = '1') then
	if	(dt_liberacao_w is null) then
		select	max(dt_liberacao)
		into	dt_liberacao_w
		from 	prescr_medica
		where	nr_prescricao = nr_prescricao_p;	
	end if;	
	
	select	max(dt_suspensao)
	into	dt_suspensao_w
	from	prescr_solucao
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_seq_solucao 	= nr_sequencia_p;
elsif	(ie_tipo_p = '4') then
	select	max(dt_suspensao)
	into	dt_suspensao_w
	from	nut_paciente
	where	nr_sequencia = nr_sequencia_p;
elsif	(ie_tipo_p = '5') then
	select	max(dt_suspensao)
	into	dt_suspensao_w
	from	nut_pac
	where	nr_sequencia = nr_sequencia_p;
end if;

if	(dt_suspensao_w is not null) and
	(dt_suspensao_w > dt_liberacao_w) then
	ds_retorno_w := 'S';
end if;	
	
return	ds_retorno_w;	

end obter_se_suspensao_apos_lib;
/