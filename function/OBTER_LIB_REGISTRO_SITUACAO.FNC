create or replace
function obter_lib_registro_situacao (
		ie_liberacao_p		varchar2,
		ie_situacao_registro_p	varchar2)
		return varchar2 is

ie_liberacao_w	varchar2(1) := 'S';

begin
if	(ie_situacao_registro_p <> 'T') then
	begin
	if	(ie_liberacao_p = 'N') then
		begin
		if	(ie_situacao_registro_p = 'A') then
			begin
			ie_liberacao_w := 'S';
			end;
		else
			begin
			ie_liberacao_w := 'N';
			end;
		end if;
		end;
	elsif	(ie_liberacao_p = 'I') then
		begin
		if	(ie_situacao_registro_p = 'I') then
			begin
			ie_liberacao_w := 'S';
			end;
		else
			begin
			ie_liberacao_w := 'N';
			end;
		end if;
		end;
	elsif	(ie_liberacao_p = 'A') then
		begin
		if	(ie_situacao_registro_p = 'A') then
			begin
			ie_liberacao_w := 'S';
			end;
		else
			begin
			ie_liberacao_w := 'N';
			end;	
		end if;
		end;
	end if;
	end;
else
	begin
	ie_liberacao_w := 'S';
	end;
end if;
return ie_liberacao_w;
end obter_lib_registro_situacao;
/