create or replace
function ple_obter_status_acao(	nr_seq_ordem_serv_p		number)
				
return varchar2 is

ds_status_ordem_w		varchar2(255);
dt_validacao_pe_w		date;
dt_fim_previsto_w		date;
ie_status_ordem_w		varchar2(1);
			
begin

select	ie_status_ordem,
		dt_validacao_pe,
		dt_fim_previsto
into	ie_status_ordem_w,
		dt_validacao_pe_w,
		dt_fim_previsto_w
from	man_ordem_servico
where	nr_sequencia	= nr_seq_ordem_serv_p;


if	(ie_status_ordem_w = '1') then

	ds_status_ordem_w	:= obter_desc_expressao(283054);--'Aberta';
end if;

if	(ie_status_ordem_w = '2') then

	ds_status_ordem_w	:= obter_desc_expressao(296477);--'Processo';
end if;
    
if	(ie_status_ordem_w = '3') then

	ds_status_ordem_w	:= obter_desc_expressao(303118);--'Conclu�da';
end if;

if	(ie_status_ordem_w = '3') and
	(dt_validacao_pe_w is not null) then
	
	ds_status_ordem_w	:= obter_desc_expressao(331746);--'Validado';
end if;	

if	(dt_fim_previsto_w is not null) and
	(dt_fim_previsto_w < trunc(sysdate)) and
	(ie_status_ordem_w <> '3') then
	
	ds_status_ordem_w	:= obter_desc_expressao(302650);--'Atrasada';
	
end if;

return	ds_status_ordem_w;

end 	ple_obter_status_acao;
/