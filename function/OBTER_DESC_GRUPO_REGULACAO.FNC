create or replace
function obter_desc_grupo_regulacao( nr_seq_grupo_p	number)
				return varchar is

ds_regulacao_w		varchar2(255);

begin

if	( nr_seq_grupo_p is not null) then

	Select 	max(ds_regulacao)
	into	ds_regulacao_w
	from	grupo_regulacao
	where	nr_sequencia = nr_seq_grupo_p;
		
end if;

return ds_regulacao_w;

end obter_desc_grupo_regulacao;
/
