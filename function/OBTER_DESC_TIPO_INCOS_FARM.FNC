create or replace
function obter_desc_tipo_incos_farm		(nr_seq_tipo_p	number)
						return varchar2 is
			
ds_retorno_w	varchar2(200);

begin

if (nvl(nr_seq_tipo_p,0) > 0) then
	select 	max(ds_tipo)
	into 	ds_retorno_w
	from 	tipo_inconsistencia_farm
	where 	nr_sequencia = nr_seq_tipo_p;
end if;

return ds_retorno_w;

end obter_desc_tipo_incos_farm;
/