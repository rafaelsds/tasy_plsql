create or replace
function obter_desc_recebimento(cd_tipo_recebimento_p		varchar2)
 		    			return varchar2 is
						
						
ds_tipo_recebimento_w	 tipo_recebimento.ds_tipo_recebimento%type;						

begin

select obter_valor_dominio(708,ie_tipo_consistencia) ds
into   ds_tipo_recebimento_w
from   tipo_recebimento
where  cd_tipo_recebimento = cd_tipo_recebimento_p;


return wheb_mensagem_pck.get_texto(996094, 'MACRO=' || ds_tipo_recebimento_w);

end obter_desc_recebimento;
/