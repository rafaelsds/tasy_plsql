create or replace
function hdjb_obter_dados_tiss_diag (	nr_atendimento_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

/* ie_opcao_p= '1'  - TISS Tipo Doen�a
    	     = '2'  - TISS Tempo Doen�a
	     = '3'  - TISS Unidade Tempo */
	
ie_tipo_doenca_w	varchar2(2);
ie_tipo_tempo_doenca_w	varchar2(2);
qt_tempo_doenca_w	number(15);
ds_retorno_w		varchar2(255);

begin

begin
select	ie_tipo_doenca,
	ie_unidade_tempo,
	qt_tempo
into	ie_tipo_doenca_w,
	ie_tipo_tempo_doenca_w,
	qt_tempo_doenca_w
from	diagnostico_medico
where	nr_atendimento = nr_atendimento_p
and 	dt_diagnostico = (select max(dt_diagnostico)
			  from   diagnostico_medico
			  where  nr_atendimento = nr_atendimento_p);
exception
	when others then
	ie_tipo_doenca_w:= ' ';
	ie_tipo_tempo_doenca_w:= ' ';
	qt_tempo_doenca_w:= 0;
end;

if	(ie_opcao_p = '1') then
	ds_retorno_w:= ie_tipo_doenca_w;
elsif	(ie_opcao_p = '2') then
	ds_retorno_w:= ie_tipo_tempo_doenca_w;
elsif	(ie_opcao_p = '3') then
	ds_retorno_w:= to_char(qt_tempo_doenca_w);
end if;

return ds_retorno_w;

end hdjb_obter_dados_tiss_diag;
/