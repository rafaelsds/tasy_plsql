CREATE	OR	REPLACE
function	CPOE_CHECAR_CIH_ALTERACAO(	nr_sequencia_p				in number,
										cd_material_p				in number,
										ie_via_aplicacao_p			in varchar2,
										cd_intervalo_p				in varchar2,
										qt_dias_liberado_p			in number,
										qt_dias_solicitado_p		in number,
										qt_dose_p					in number,
										cd_unidade_medida_p			in varchar2,
										cd_mat_comp1_p				in number,
										qt_dose_comp1_p             in number,
										cd_unid_med_dose_comp1_p    in varchar2,
										cd_mat_comp2_p              in number,
										qt_dose_comp2_p             in number,
										cd_unid_med_dose_comp2_p    in varchar2,
										cd_mat_comp3_p              in number,
										qt_dose_comp3_p             in number,
										cd_unid_med_dose_comp3_p    in varchar2)
return varchar2	is	

cd_material_w				cpoe_material.cd_material%type;
ie_via_aplicacao_w			cpoe_material.ie_via_aplicacao%type;
cd_intervalo_w				cpoe_material.cd_intervalo%type;
qt_dias_liberado_w			cpoe_material.qt_dias_liberado%type;
qt_dias_solicitado_w		cpoe_material.qt_dias_solicitado%type;
qt_dose_w					cpoe_material.qt_dose%type;
cd_unidade_medida_w			cpoe_material.cd_unidade_medida%type;
cd_mat_comp1_w				cpoe_material.cd_mat_comp1%type;
qt_dose_comp1_w          	cpoe_material.qt_dose_comp1%type;
cd_unid_med_dose_comp1_w 	cpoe_material.cd_unid_med_dose_comp1%type;
cd_mat_comp2_w           	cpoe_material.cd_mat_comp2%type;
qt_dose_comp2_w          	cpoe_material.qt_dose_comp2%type;
cd_unid_med_dose_comp2_w 	cpoe_material.cd_unid_med_dose_comp2%type;
cd_mat_comp3_w           	cpoe_material.cd_mat_comp3%type;
qt_dose_comp3_w          	cpoe_material.qt_dose_comp3%type;
cd_unid_med_dose_comp3_w 	cpoe_material.cd_unid_med_dose_comp3%type;

ie_alterado_w				varchar2(1 char);


function ieAntiMicroAlterado(cd_mat_orig_p		in number, 
							qt_dos_orig_p 		in number, 
							cd_unid_med_orig_p	in varchar2, 
							cd_mat_p			in number, 
							qt_dos_p			in number, 
							cd_unid_med_p		in varchar2) 
 return varchar2 is
 ie_retornow_w		varchar2(1 char);
 begin
	ie_retornow_w := 'N';
	if(	OBTER_SE_ANTIMICROBIANO(cd_mat_orig_p) = 0) then
		ie_retornow_w := 'N';
	elsif (	cd_mat_orig_p 		<> cd_mat_p or
			qt_dos_orig_p 		<> qt_dos_p or
			cd_unid_med_orig_p	<> cd_unid_med_p) then
		ie_retornow_w := 'S';
	end if;
	return ie_retornow_w;
end ieAntiMicroAlterado;

BEGIN
	ie_alterado_w := 'N';
	
	select	max(cd_material	),				
			max(ie_via_aplicacao),			
			max(cd_intervalo),				
			max(qt_dias_liberado),			
			max(qt_dias_solicitado),		
			max(qt_dose),					
			max(cd_unidade_medida),			
			max(cd_mat_comp1),				
			max(qt_dose_comp1),          	
			max(cd_unid_med_dose_comp1), 	
			max(cd_mat_comp2),           	
			max(qt_dose_comp2),          	
			max(cd_unid_med_dose_comp2), 	
			max(cd_mat_comp3),           	
			max(qt_dose_comp3),          	
			max(cd_unid_med_dose_comp3)
	into	cd_material_w,
			ie_via_aplicacao_w,			
			cd_intervalo_w,				
			qt_dias_liberado_w,
			qt_dias_solicitado_w,		
			qt_dose_w,					
			cd_unidade_medida_w,			
			cd_mat_comp1_w,				
			qt_dose_comp1_w,          	
			cd_unid_med_dose_comp1_w, 	
			cd_mat_comp2_w,           	
			qt_dose_comp2_w,          	
			cd_unid_med_dose_comp2_w, 	
			cd_mat_comp3_w,           	
			qt_dose_comp3_w,
			cd_unid_med_dose_comp3_w
	from	cpoe_material
	where	nr_sequencia	=	nr_sequencia_p;
	
	
	if(	ie_via_aplicacao_w		<> ie_via_aplicacao_p	or
		cd_intervalo_w			<> cd_intervalo_p		or
		qt_dias_liberado_w		<> qt_dias_liberado_p	or
		qt_dias_solicitado_w	<> qt_dias_solicitado_p ) then
			ie_alterado_w := 'S';
	end if;
	
	if(ie_alterado_w <> 'S' and ieAntiMicroAlterado(cd_material_w, qt_dose_w, cd_unidade_medida_w, cd_material_p, qt_dose_p, cd_unidade_medida_p) = 'N' and
	  ieAntiMicroAlterado(cd_mat_comp1_w, qt_dose_comp1_w, cd_unid_med_dose_comp1_w, cd_mat_comp1_p, qt_dose_comp1_p, cd_unid_med_dose_comp1_p) = 'N' and
	  ieAntiMicroAlterado(cd_mat_comp2_w, qt_dose_comp2_w, cd_unid_med_dose_comp2_w, cd_mat_comp2_p, qt_dose_comp2_p, cd_unid_med_dose_comp2_p) = 'N' and
	  ieAntiMicroAlterado(cd_mat_comp3_w, qt_dose_comp3_w, cd_unid_med_dose_comp3_w, cd_mat_comp3_p, qt_dose_comp3_p, cd_unid_med_dose_comp3_p) = 'N') then
		ie_alterado_w :=  'N';
	else
		ie_alterado_w :=  'S';
	end if;
		
return ie_alterado_w;

END	CPOE_CHECAR_CIH_ALTERACAO;
/