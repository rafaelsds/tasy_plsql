CREATE OR REPLACE
FUNCTION obter_qt_diarias_feriado
		(dt_inicial_p		date,
		dt_final_p		date,
		cd_estabelecimento_p	number,
		cd_setor_atendimento_p	number,
		ie_tipo_p		varchar2)
		return number is

/* Fun��o criada especificamente para as OS 16867 e 31063*/

/* IE_TIPO_P
 S - SUS (16867)
 G - Geral (31063)
*/

dt_aux_w	date;
qt_diarias_w	number(10)	:= 0;
qt_pacientes_w	number(10)	:= 0;
ie_dia_semana_w	varchar2(01);
ie_feriado_w	varchar2(01);

BEGIN

dt_aux_w	:= trunc(dt_inicial_p, 'dd');

while	(dt_aux_w <= dt_final_p) loop
	begin

	select	pkg_date_utils.get_WeekDay(dt_aux_w)
	into	ie_dia_semana_w
	from	dual;

	select	nvl(max('S'), 'N')
	into	ie_feriado_w
	from 	feriado 
	where 	dt_feriado = dt_aux_w
	and	cd_estabelecimento = cd_estabelecimento_p;


	If	(ie_feriado_w = 'N') and
		(ie_dia_semana_w in ('2', '3', '4', '5', '6')) then
		begin
		
		if	(cd_setor_atendimento_p	<> 0) then
			select	sum(1)
			into	qt_pacientes_w   
			from  	paciente_internado_v2
			where	dt_saida_interno	>= trunc(dt_aux_w) + 83699/86400   	
			and	dt_entrada_unidade 	<= trunc(dt_aux_w) + 83699/86400 
			and	cd_setor_atendimento	= cd_setor_atendimento_p;
		
		elsif	(cd_setor_atendimento_p	= 0) then
			if	(ie_tipo_p	= 'S') then
				select	sum(1)
				into	qt_pacientes_w   
				from  	paciente_internado_v2
				where	dt_saida_interno	>= trunc(dt_aux_w) + 83699/86400   	
				and	dt_entrada_unidade	<= trunc(dt_aux_w) + 83699/86400 
				and	cd_setor_atendimento	in (8,20,22,23);
			
			elsif	(ie_tipo_p	= 'G') then
				select	sum(1)
				into	qt_pacientes_w   
				from  	paciente_internado_v2
				where	dt_saida_interno	>= trunc(dt_aux_w) + 83699/86400   	
				and    	dt_entrada_unidade	<= trunc(dt_aux_w) + 83699/86400 
				and	cd_setor_atendimento	in (6,8,9,20,22,23);
			end if;
		end if;

		qt_diarias_w	:= qt_diarias_w + qt_pacientes_w;
		
		end;


	end if;
		
	dt_aux_w	:= dt_aux_w + 1;
	
	end;
end loop;

RETURN qt_diarias_w;

END obter_qt_diarias_feriado;
/
