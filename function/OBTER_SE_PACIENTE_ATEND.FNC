create or replace
function obter_se_paciente_atend (	cd_pessoa_fisica_p	varchar2,
									cd_agenda_p			number,
									dt_agenda_p			date)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
ie_possui_atend_w	varchar2(1);
ds_agenda_w		varchar2(255);
	
			
begin
ie_possui_atend_w := 'N';
	
select	decode(count(*),0,'N','S')
into	ie_possui_atend_w
from	agenda_consulta
where	ie_status_agenda = 'O'
and 	cd_pessoa_fisica = cd_pessoa_fisica_p
and		trunc(dt_agenda,'dd') = trunc(dt_agenda_p)
and		cd_agenda <> cd_agenda_p;

if (ie_possui_atend_w = 'S') then
	begin
	select	substr(obter_desc_agenda(max(cd_agenda)),1,255)
	into	ds_agenda_w
	from	agenda_consulta
	where	ie_status_agenda = 'O'
	and 	cd_pessoa_fisica = cd_pessoa_fisica_p
	and		trunc(dt_agenda) = trunc(dt_agenda_p)
	and		cd_agenda <> cd_agenda_p;
	
	ds_retorno_w := obter_texto_tasy(256120, wheb_usuario_pck.get_nr_seq_idioma) || ' ' || ds_agenda_w;
	end;
else
	ds_retorno_w := '';
end if;

return	ds_retorno_w;

end obter_se_paciente_atend;
/