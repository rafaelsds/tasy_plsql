create or replace function OBTER_RESULT_ESCALA_SIST_NUT(
  ie_dietoterapia_p varchar2,
  ie_fator_risco_p varchar2
) return varchar2 is

  ds_retorno_w varchar2(100);
  sql_w varchar2(250);
begin
  ds_retorno_w	:= '';
  begin
    sql_w := 'CALL OBTER_RESULT_ESC_SIST_NUT_MD(:1, :2) INTO :ds_retorno_w';
    EXECUTE IMMEDIATE sql_w 
      USING IN ie_dietoterapia_p,
            IN ie_fator_risco_p,
            OUT ds_retorno_w;
  exception
    when others then 
      ds_retorno_w := null;
    end;
  return ds_retorno_w;
end OBTER_RESULT_ESCALA_SIST_NUT;
/
