create or replace function obter_hr_real_etapa_prog( nr_seq_prog_p number,dt_inicio_prev_p date ) return number is
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter hora real de execu��o de uma etapa do projeto
---------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [ ]  Relat�rios [ ] Outros: 
 --------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

hr_retorno_w number(15,2);
pr_realizacao_w	number(15,2);

begin


select  max(p.pr_realizacao)
into 	pr_realizacao_w
from    proj_programa pr,
		proj_projeto p,
		proj_cronograma c, 
		proj_cron_etapa e
where	pr.nr_sequencia = p.nr_seq_programa
and	p.nr_sequencia = c.nr_seq_proj
and     c.nr_sequencia = e.nr_seq_cronograma
and     c.ie_situacao  = 'A'
and 	pr.nr_sequencia = nr_seq_prog_p
and  	not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  e.nr_sequencia);

if(nvl(pr_realizacao_w,0) < 100 
	and dt_inicio_prev_p > trunc(sysdate))then
	goto final;
end if;

	select	max(QT_HORA_REAL)
	into	hr_retorno_w
	from	dashboard_pmo_prog_graf 
	where 	nr_seq_prog = nr_seq_prog_p
	and	trunc(dt_referencia) = trunc(dt_inicio_prev_p);
	
	if 	(hr_retorno_w is null) then 
		hr_retorno_w := 0;
	end if;
	
  
  <<final>>  
  return  hr_retorno_w;

end obter_hr_real_etapa_prog;
/