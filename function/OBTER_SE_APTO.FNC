create or replace function
OBTER_SE_APTO	(ie_avaliacao_final_p	Varchar2,
		 ie_opcao_p			varchar2)
		 return number is

ds_retorno_w	number(02,0);


BEGIN

if (ie_avaliacao_final_p = ie_opcao_p) then
	ds_retorno_w := 1;
else
	ds_retorno_w := 0;
end if;


return ds_retorno_w;

END OBTER_SE_APTO;
/