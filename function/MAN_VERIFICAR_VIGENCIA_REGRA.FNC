create or replace
function man_verificar_vigencia_regra(	dt_ordem_p	DATE,
										dt_inicio_p	DATE,
										dt_fim_p	DATE)
 		    	RETURN VARCHAR2 IS

ds_retorno_w	VARCHAR2(1);
dt_ordem_w		DATE;
dt_inicio_w		DATE;
dt_fim_w		DATE;

BEGIN

IF 	(dt_inicio_p IS NULL) OR
	(dt_fim_p IS NULL) THEN
	ds_retorno_w := 'S';
ELSE
	SELECT	pkg_date_utils.get_time(HOUR, MINUTE)
	INTO	dt_ordem_w
	FROM	(	SELECT	pkg_date_utils.extract_field('HOUR', NVL(dt_ordem_p, SYSDATE)) HOUR,
				pkg_date_utils.extract_field('MINUTE', NVL(dt_ordem_p, SYSDATE)) MINUTE
				FROM	dual);


	SELECT	pkg_date_utils.get_time(HOUR, MINUTE)
	INTO	dt_inicio_w
	FROM	(	SELECT	pkg_date_utils.extract_field('HOUR', NVL(dt_inicio_p, pkg_date_utils.get_time(00, 00))) HOUR,
				pkg_date_utils.extract_field('MINUTE', NVL(dt_inicio_p, pkg_date_utils.get_time(00 ,00))) MINUTE
				FROM	dual);

	SELECT	pkg_date_utils.get_time(HOUR, MINUTE)
	INTO	dt_fim_w
	FROM	(	SELECT	pkg_date_utils.extract_field('HOUR', NVL(dt_fim_p, pkg_date_utils.get_time(23, 59))) HOUR,
				pkg_date_utils.extract_field('MINUTE', NVL(dt_fim_p, pkg_date_utils.get_time(23, 59))) MINUTE
				FROM	dual);

	SELECT	NVL(MAX('S'), 'N')
	INTO	ds_retorno_w
	FROM 	dual
	WHERE 	dt_ordem_w BETWEEN dt_inicio_w AND dt_fim_w;
END IF;

RETURN	ds_retorno_w;

END man_verificar_vigencia_regra;
/