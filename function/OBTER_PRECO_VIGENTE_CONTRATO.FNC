create or replace
function obter_preco_vigente_contrato(		cd_cnpj_p		varchar2,
					cd_pessoa_fisica_p		varchar2,
					cd_material_p		number)
return number is

vl_pagto_w		number(17,4);

begin

select	min(a.vl_pagto)
into	vl_pagto_w
from	contrato_regra_nf a,
	contrato b
where	a.nr_seq_contrato = b.nr_sequencia
and	a.cd_material = cd_material_p
and	(((cd_cnpj_p is not null) and (b.cd_cgc_contratado = cd_cnpj_p)) or
	((cd_pessoa_fisica_p is not null) and (b.cd_pessoa_contratada = cd_pessoa_fisica_p)))
and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')));

return	vl_pagto_w;

end	obter_preco_vigente_contrato;
/