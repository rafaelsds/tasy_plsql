create or replace
function obter_seq_tipo_devol(nr_sequencia_p	Number)
 		    	return number is

nr_seq_tipo_w	number(10);			
			
begin


select 	max(nr_seq_tipo)
into	nr_seq_tipo_w
from  	fatur_motivo_devol
where 	nvl(ie_situacao,'A') = 'A'
and 	nr_sequencia = nr_sequencia_p;

return	nr_seq_tipo_w;

end obter_seq_tipo_devol;
/