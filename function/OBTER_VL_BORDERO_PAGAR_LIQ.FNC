create or replace
function OBTER_VL_BORDERO_PAGAR_LIQ(nr_bordero_p number)
					return number is

vl_bordero_novo_w	number(18,4);
vl_bordero_antigo_w	number(18,4);
/*criei essa function para obter o vl liquido, que dava problema em alguns relat�rios. A function obter_valor_bordero_pagar traz o vl_titulo da tabela titulo_pagar_w.  N�o alterei essa ja existente para nao dar problema onde ja utilizam essa function*/
begin

select	nvl(sum(vl_bordero),0)
into	vl_bordero_novo_w
from	bordero_tit_pagar
where	nr_bordero	= nr_bordero_p;

select	nvl(sum(vl_liquido_bordero),0)
into	vl_bordero_antigo_w
from	titulo_pagar_v
where	nr_bordero	= nr_bordero_p;

return	vl_bordero_novo_w + vl_bordero_antigo_w;

end	OBTER_VL_BORDERO_PAGAR_LIQ;
/

