create or replace
function Obter_paciente_cracha(	nr_controle_p		varchar2) return varchar2 is 

nr_atendimento_w	number(15);

begin

if	(nr_controle_p is not null) then
	select	max(b.nr_atendimento)
	into	nr_atendimento_w
	from	unidade_atendimento b,
		unidade_atend_cracha a
	where	a.cd_setor_atendimento 	= b.cd_setor_atendimento
	and	a.cd_unidade_basica	= b.cd_unidade_basica
	and	b.nr_atendimento	is not null
	and	a.cd_unidade_compl	= b.cd_unidade_compl
	and	a.cd_cracha		= nr_controle_p;

end if;

return nr_atendimento_w;

end Obter_paciente_cracha;
/
