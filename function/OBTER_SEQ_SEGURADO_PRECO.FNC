create or replace
function obter_seq_segurado_preco
			(	nr_seq_segurado_p		number)
				return number is

nr_seq_seg_preco_w		number(10);

ie_regulamentacao_w		varchar2(1);
ie_mes_cobranca_reaj_w		varchar2(1);
ie_mes_cobranca_reaj_regulam_w	varchar2(1);
ie_mes_cobranca_reaj_cont_w	varchar2(1);
ie_mes_cobranca_reajuste_w	varchar2(1);

begin

select	max(b.ie_mes_cobranca_reaj),
	max(c.ie_regulamentacao)
into	ie_mes_cobranca_reaj_cont_w,
	ie_regulamentacao_w
from	pls_segurado	a,
	pls_contrato	b,
	pls_plano	c
where	a.nr_seq_contrato	= b.nr_sequencia
and	a.nr_seq_plano		= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_segurado_p;

ie_regulamentacao_w		:= nvl(ie_regulamentacao_w,'P');

if	(nvl(ie_mes_cobranca_reaj_cont_w,'R') = 'R') then /* Se no contrato estiver "Conforme regra", pega pela regulamentação */
	select	max(ie_mes_cobranca_reaj),
		max(ie_mes_cobranca_reaj_reg)
	into	ie_mes_cobranca_reaj_w,
		ie_mes_cobranca_reaj_regulam_w
	from	pls_parametros
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
	
	if	(ie_regulamentacao_w = 'R') then
		ie_mes_cobranca_reajuste_w	:= ie_mes_cobranca_reaj_w;
	else
		ie_mes_cobranca_reajuste_w	:= ie_mes_cobranca_reaj_regulam_w;
	end if;
else
	ie_mes_cobranca_reajuste_w	:= nvl(ie_mes_cobranca_reaj_cont_w,'P');
end if;
ie_mes_cobranca_reajuste_w	:= nvl(ie_mes_cobranca_reajuste_w,'P');

select	max(a.nr_sequencia)
into	nr_seq_seg_preco_w
from	pls_segurado_preco a,
	pls_segurado b
where	a.nr_seq_segurado = b.nr_sequencia
and	b.nr_sequencia = nr_seq_segurado_p
and	(((PKG_DATE_UTILS.start_of(sysdate, 'MONTH', 0) >= PKG_DATE_UTILS.start_of(a.dt_reajuste, 'MONTH', 0))
and	((a.cd_motivo_reajuste <> 'E') or (ie_mes_cobranca_reajuste_w = 'M'))) 
or	(((a.cd_motivo_reajuste = 'E') and (PKG_DATE_UTILS.start_of(sysdate, 'MONTH', 0) >= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(a.dt_reajuste,1,0), 'MONTH', 0)))
and	(ie_mes_cobranca_reajuste_w = 'P')));

return	nr_seq_seg_preco_w;

end obter_seq_segurado_preco;
/