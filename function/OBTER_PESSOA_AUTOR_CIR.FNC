create or replace
function OBTER_PESSOA_AUTOR_CIR
			(nr_atendimento_p	number,
			 nr_seq_agenda_p	number) return varchar2 is 

cd_pessoa_fisica_w	varchar2(255);

begin

if	(nr_atendimento_p is not null) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_p;
else
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	agenda_paciente
	where	nr_sequencia	= nr_seq_agenda_p;
end if;

return	cd_pessoa_fisica_w;

end OBTER_PESSOA_AUTOR_CIR;
/
