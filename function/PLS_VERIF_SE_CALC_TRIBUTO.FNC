create or replace
function pls_verif_se_calc_tributo(	nr_seq_lote_pgto_p	pls_lote_pagamento.nr_sequencia%type) return varchar2 is

nr_contador_w	pls_integer;
ds_retorno_w	varchar2(500);
dt_lote_pgto_w	date;

cursor c01(	nr_seq_lote_pgto_pc	pls_lote_pagamento.nr_sequencia%type) is
	select	distinct e.cd_tributo cd_tributo, 
		e.ie_tipo_tributo
	from	pls_pagamento_item a,
		pls_pagamento_prestador b,
		pls_evento_tributo c,
		pls_lote_pagamento d,
		tributo e
	where	d.nr_sequencia = nr_seq_lote_pgto_pc
	and	b.nr_seq_lote = d.nr_sequencia
	and	a.nr_seq_pagamento = b.nr_sequencia
	and	c.nr_seq_evento = a.nr_seq_evento
	and	c.ie_situacao = 'A'
	and	d.dt_mes_competencia between trunc(nvl(c.dt_inicio_vigencia, d.dt_mes_competencia), 'month') and last_day(nvl(c.dt_fim_vigencia, d.dt_mes_competencia))
	and	e.cd_tributo = c.cd_tributo
	and	e.ie_conta_pagar = 'S'
	and	e.ie_situacao = 'A';

begin
ds_retorno_w := null;

if	(nr_seq_lote_pgto_p is not null) then

	select	max(dt_mes_competencia)
	into 	dt_lote_pgto_w
	from	pls_lote_pagamento
	where	nr_sequencia = nr_seq_lote_pgto_p;

	for r_c01_w in c01(nr_seq_lote_pgto_p) loop

		nr_contador_w := 1;

		-- quando for para calcular o tributo para o prestador e n�o houver cadastro de c�lculo do tributo
		-- para o per�odo de gera��o do lote ent�o aborta a gera��o e apresenta uma mensagem ao usu�rio - OS981172
		if	(r_c01_w.ie_tipo_tributo in ('INSS', 'IR')) then

			select	count(1)
			into	nr_contador_w
			from	tributo_conta_pagar
			where	cd_tributo = r_c01_w.cd_tributo
			and	dt_lote_pgto_w between dt_inicio_vigencia_ref and dt_fim_vigencia_ref;
			
			-- o IR temos dois lugares para cadastrar o c�lculo, desta forma caso tenha registro na tabela tributo_conta_pagar
			-- ainda precisamos verificar na tabela regra_calculo_irpf (tabela de pessoa fisica)
			if	(r_c01_w.ie_tipo_tributo = 'IR') and
				(nr_contador_w > 0) then
				
				select	count(1)
				into	nr_contador_w
				from	regra_calculo_irpf
				where	dt_lote_pgto_w between dt_inicio_vigencia_ref and dt_fim_vigencia_ref;
			end if;
		end if;

		if	(nr_contador_w = 0) then
			
			ds_retorno_w := ' Existe(m) cadastro(s) de tributos sem c�lculo cadastrado para o per�odo do lote de pagamento. Tributo: ' || 
						r_c01_w.cd_tributo || ' - ' || r_c01_w.ie_tipo_tributo || '.';
			-- Se encontrar algum � o suficiente para apresentar a mensagem ao usu�rio, ent�o sai do cursor
			exit;
		end if;
	end loop;
end if;
return ds_retorno_w;

end pls_verif_se_calc_tributo;
/