create or replace
FUNCTION ehr_ent_inf_monit_resp (	nr_seq_elemento_p number, 
									cd_pessoa_fisica_p number, 
									nr_atendimento_p number, 
									ie_inf_entidade_p varchar2, 
									ie_atendimento_p varchar2) 
									RETURN varchar2 IS ds_retorno_w varchar2(20000);

ds_retorno_ww varchar2(20000);

nr_sequencia_w number(10);

nm_atributo_w varchar2(255);

ds_comando_w varchar2(2000);

ds_comando_ww varchar2(2000);

ds_label_w varchar2(60);

ds_quebra_w varchar2(30);

ds_separador_w varchar2(30);

c001 Integer;

qt_cursor_w number(10);


CURSOR c01 IS
SELECT nr_sequencia
FROM atendimento_monit_resp a
WHERE nr_atendimento = nr_atendimento_p
  AND ie_atendimento_p = 'S'
  AND nr_sequencia IN
    (SELECT max(x.nr_sequencia)
     FROM atendimento_monit_resp x
     WHERE nr_atendimento = nr_atendimento_p
       AND ie_inf_entidade_p = 'U'
       AND x.dt_inativacao IS NULL
     UNION SELECT x.nr_sequencia
     FROM atendimento_monit_resp x
     WHERE nr_atendimento = nr_atendimento_p
       AND x.dt_inativacao IS NULL
       AND ie_inf_entidade_p = 'T')
UNION
SELECT nr_sequencia
FROM atendimento_monit_resp a
WHERE nr_atendimento IN
    (SELECT b.nr_atendimento
     FROM atendimento_paciente b
     WHERE b.cd_pessoa_fisica = cd_pessoa_fisica_p)
  AND ie_atendimento_p = 'N'
  AND nr_sequencia IN
    (SELECT max(x.nr_sequencia)
     FROM atendimento_monit_resp x
     WHERE x.nr_atendimento IN
         (SELECT b.nr_atendimento
          FROM atendimento_paciente b
          WHERE b.cd_pessoa_fisica = cd_pessoa_fisica_p)
       AND ie_inf_entidade_p = 'U'
       AND x.dt_inativacao IS NULL
     UNION SELECT x.nr_sequencia
     FROM atendimento_monit_resp x
     WHERE x.nr_atendimento IN
         (SELECT b.nr_atendimento
          FROM atendimento_paciente b
          WHERE b.cd_pessoa_fisica = cd_pessoa_fisica_p)
       AND x.dt_inativacao IS NULL
       AND ie_inf_entidade_p = 'T')
ORDER BY nr_sequencia;


CURSOR C02 IS
SELECT nvl(ds_function,nm_atributo),
       ds_label,
       decode(ie_quebra_linha,'S',' chr(13) ||chr(10) ||',NULL),
       ds_separador
FROM ehr_template_cont_ret
WHERE nr_seq_elemento = nr_seq_elemento_p
ORDER BY nr_seq_apres;

BEGIN OPEN C02;

LOOP FETCH C02 INTO nm_atributo_w,
                    ds_label_w,
                    ds_quebra_w,
                    ds_separador_w;

exit WHEN C02%NOTFOUND;

BEGIN 

IF (ds_comando_w IS NOT NULL) THEN 
	ds_comando_w := ds_comando_w ||'||'||chr(13);
END IF;

ds_comando_w := ds_comando_w ||' decode('||nm_atributo_w||',null,null,'||ds_quebra_w ||chr(39) || ds_label_w ||chr(39) ||'|| '|| nm_atributo_w ||'||'|| chr(39) || ds_separador_w ||chr(39) ||')';

END;

END LOOP;

CLOSE C02;

ds_comando_w := 'Select '||ds_comando_w ||' ds '||chr(13) ||'From atendimento_monit_resp ';

OPEN C01;

	LOOP FETCH C01 INTO nr_sequencia_w;

		exit WHEN C01%NOTFOUND;

			BEGIN 
			ds_comando_ww := ds_comando_w || chr(13) || 'where nr_sequencia = '||to_char(nr_sequencia_w);

			c001 := dbms_sql.open_cursor;

			dbms_sql.parse(c001, ds_comando_ww, dbms_sql.native);

			dbms_sql.define_column(c001, 1, ds_retorno_ww, 2000);

			qt_cursor_w := dbms_sql.execute(c001);

			qt_cursor_w := dbms_sql.fetch_rows(c001);

			dbms_sql.column_value(c001, 1, ds_retorno_ww);

			dbms_sql.close_cursor(c001);

			ds_retorno_w := ds_retorno_w ||ds_retorno_ww ||chr(13);

			END;

	END LOOP;

CLOSE C01;

RETURN substr(ds_retorno_w,1,2000);

END ehr_ent_inf_monit_resp;
/