create or replace
function obter_se_proc_hemoc_lib(	Nr_seq_regra_p		Number,
					Nr_seq_solic_sangue_p	Number,
					Nm_usuario_p		varchar2,
					ie_funcao_prescritor_p	varchar2)
 		    	return Varchar2 is
			
ie_espec_lib_w		Varchar2(1) := 'S';
cd_pessoa_fisica_w	Varchar2(10);
qt_reg_w		number(10);
begin

select	nvl(max(cd_pessoa_fisica),0)
into	cd_pessoa_fisica_w
from	usuario
where	nm_usuario = nm_usuario_p;


Select	count(*)
into	qt_reg_w
from	proced_derivado_regra
where	nr_seq_regra = nr_seq_regra_p;

if	(qt_reg_w > 0) then
	Select 	decode(count(*),0,'N','S')
	into	ie_espec_lib_w
	from	proced_derivado_regra
	where	nr_seq_regra = nr_seq_regra_p
	and	((ie_funcao_prescritor is null) or (ie_funcao_prescritor_p = ie_funcao_prescritor))
	and 	((cd_especialidade is null) or (obter_se_especialidade_medico(cd_pessoa_fisica_w, cd_especialidade) = 'S'));
end if;

return	ie_espec_lib_w;

end Obter_se_proc_hemoc_lib;
/