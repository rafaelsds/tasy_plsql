CREATE OR REPLACE 
FUNCTION VERIFY_COMMIT_SO(nr_seq_ordem_servico_p    number)
                          RETURN VARCHAR2 IS

ie_situacao_w               VARCHAR2(1) := 'N';
qt_registro_commit_w        number(10);
qt_registro_ajuste_versao_w number(10);

BEGIN
    select count(*)
    into   qt_registro_commit_w
    from   MAN_COMMIT_GIT a
    where  UPPER(a.ds_projeto) NOT IN ('DEV','TASY-SCHEDULER-AGENT', 'TASY-INTERFACES', 'TWS-BACKEND','TWS-FRONTEND','TASY-PLSQL-OBJECTS')
    AND    LENGTH(a.ds_branch) IN (9, 19, 20, 21, 22)
    and    a.nr_seq_ordem_serv = nr_seq_ordem_servico_p;

    select count(*)
    into   qt_registro_ajuste_versao_w
    from   tasy.ajuste_versao@orcl a
    where  a.DT_LIBERACAO_SERVICE_PACK is not null
    and    a.ie_situacao = 'A'
    and    a.nr_seq_ordem_serv = nr_seq_ordem_servico_p;
    
    if (qt_registro_commit_w > 0 or qt_registro_ajuste_versao_w > 0) then
        ie_situacao_w := 'S';
    end if;
    
    return ie_situacao_w;
END VERIFY_COMMIT_SO;
/