create or replace
function pls_obter_regra_mat_proc(	nr_seq_conta_mat_p	number,
					nr_seq_conta_proc_p	number)
 		    	return number is
			
nr_seq_regra_w		pls_conta_proc.nr_seq_regra%type;

begin

if (nr_seq_conta_mat_p is not null) then

select	nr_seq_regra_copartic
into	nr_seq_regra_w
from	pls_conta_mat
where	nr_sequencia = nr_seq_conta_mat_p;

elsif (nr_seq_conta_proc_p is not null) then

select 	nr_seq_regra_copartic
into	nr_seq_regra_w
from	pls_conta_proc
where	nr_sequencia = nr_seq_conta_proc_p;

end if;


return	nr_seq_regra_w;

end pls_obter_regra_mat_proc;
/