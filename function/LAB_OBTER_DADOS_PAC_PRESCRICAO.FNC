create or replace function LAB_OBTER_DADOS_PAC_PRESCRICAO (
                            nr_prescricao_p NUMBER,
                            cd_pessoa_fisica_p pessoa_fisica.cd_pessoa_fisica%type)

                            return varchar2 is

cd_estabelecimento_w number(10);
ie_envia_telef_cel_ws_w varchar2(1);

begin

begin
    select cd_estabelecimento
    into cd_estabelecimento_w
    from prescr_medica a
    where a.nr_prescricao = nr_prescricao_p;
exception when NO_DATA_FOUND then
    gravar_log_lab_pragma(20920, 'invalid parameters to function call; nr_prescricao_p' || nr_prescricao_p, 'tasy');
    raise_application_error(-20920, 'invalid parameters to function call; nr_prescricao_p' || nr_prescricao_p);
end;

begin
    select ie_envia_telef_cel_ws
    into ie_envia_telef_cel_ws_w
    from lab_parametro a
    where a.cd_estabelecimento = cd_estabelecimento_w;
exception when NO_DATA_FOUND then
    gravar_log_lab_pragma(20920, 'invalid parameters to function call; nr_prescricao_p' || nr_prescricao_p || '; cd_estabelecimento_w' || cd_estabelecimento_w, 'tasy');
    raise_application_error(-20920, 'invalid parameters to function call; nr_prescricao_p' || nr_prescricao_p || '; cd_estabelecimento_w' || cd_estabelecimento_w);
end;

if (ie_envia_telef_cel_ws_w = 'S') then
    return OBTER_DADOS_PF(cd_pessoa_fisica_p, 'TC');
end if;

return OBTER_COMPL_PF(cd_pessoa_fisica_p, 1, 'T');

end LAB_OBTER_DADOS_PAC_PRESCRICAO;
/
