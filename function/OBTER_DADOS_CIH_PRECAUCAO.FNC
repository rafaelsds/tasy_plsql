create or replace
function obter_dados_cih_precaucao(	nr_sequencia_p		number,
									ie_opcao_p			varchar2)
 		    	return varchar2 is

ie_amostra_w		varchar2(1);				
ie_motivo_w			varchar2(1);				
ie_varios_motivos_w	varchar2(1);				
				
begin

select  nvl(max(ie_amostra),'S') ie_amostra,
        nvl(max(ie_motivo),'S') ie_motivo,
        nvl(max(ie_varios_motivos),'S') ie_varios_motivos
into	ie_amostra_w,
		ie_motivo_w,
		ie_varios_motivos_w
from    cih_precaucao
where nr_sequencia = nr_sequencia_p;

if (ie_opcao_p = 'A') then
	return	ie_amostra_w;
elsif(ie_opcao_p = 'M') then
	return	ie_motivo_w;
elsif(ie_opcao_p = 'VM') then
	return	ie_varios_motivos_w;
end if;

end obter_dados_cih_precaucao;
/