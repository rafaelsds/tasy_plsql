create or replace
function Obter_Volume_Diluicao(	NR_SEQUENCIA_P		NUMBER,
				NR_PRESCRICAO_P	 	NUMBER) return varchar2 is

cd_material_w			Number(06,0);
cd_medic_w			Number(06,0);
qt_dose_w			Number(18,6);
qt_dose_ml_w			Number(18,6);
cd_unid_med_dose_w		Varchar2(30) := '';
ds_material_w			Varchar2(100);
ds_material_ww			Varchar2(100);
ds_reduzida_w			Varchar2(100);
ds_diluir_w			Varchar2(255) := '';
ds_rediluir_w			Varchar2(255) := '';
ds_retorno_w			Varchar2(2000) := '';
ds_reconstituir_w		Varchar2(2000) := '';
ie_agrupador_w			number(02,0);
qt_dose_mat_w			Number(18,6);
qt_dose_mat_str_w		Varchar2(20) := '';
cd_unid_med_dose_mat_w		Varchar2(30) := '';
qt_solucao_w			Number(15,3) := 0;
qt_solucao_str_w		Varchar2(10) :='';
qt_solucao_ww			Number(15,3) := 0;
qt_min_aplicacao_w		Number(4,0)  := 0;
qt_min_aplicacao_ant_w		Number(4,0)  := 0;
qt_hora_aplicacao_ant_w		Number(3,0)  := 0;
qt_hora_aplicacao_w		Number(3,0)  := 0;
ds_min_aplicacao_w		Varchar2(255) := '';
cd_volume_final_w		Number(15,3) := 0;
mascara_w			Number(01,0) := 0;
qt_dose_str_w			Varchar2(20) := '';
cd_volume_final_str_w		Varchar2(20) := '';
qt_diluicao_w			Number(18,6) := 0;
cd_diluente_w			Number(6,0)  := 0;
ds_diluente_w			Varchar2(100) := '';
qt_diluicao_str_w		Varchar2(20) := '';
ds_intervalo_w			Varchar2(50) := '';
cd_unidade_medida_w		varchar2(30);
ds_conv_ml_w			Varchar2(20) := '';
qt_conv_ml_w			Number(18,6);
cd_estabelecimento_w		number(4);
ie_descr_redu_dil_w		varchar2(1);

ds_aux_diluir_w			varchar2(30);
ie_via_aplicacao_w		Varchar2(5);
cd_intervalo_w			varchar2(7);
qt_ml_diluente_w		number(18,6);
ds_volume_w			varchar2(100);
ds_tempo_w			varchar2(100);
qt_dose_medic_ml_w		number(18,6);
ds_aplicar_w			varchar2(200);
ds_diluicao_edit_w		varchar2(2000);
ie_aplicar_w			varchar2(1);
ds_dose_diluicao_w		varchar2(100);
qt_dose_unid_med_cons_w		number(18,6);
qt_vol_adic_reconst_w		number(18,6);


cursor	c01 is
	select	a.cd_material,
		a.qt_dose,
		nvl(a.qt_vol_adic_reconst,0),
		a.qt_min_aplicacao,
		a.qt_hora_aplicacao,
		a.cd_unidade_medida_dose,
		a.ie_agrupador,
		a.qt_solucao,
		obter_conversao_ml(a.cd_material,a.qt_dose,a.cd_unidade_medida_dose),
		substr(b.ds_material,1,100),
		substr(b.ds_reduzida,1,100)
	from 	material b,
		prescr_material a
	where 	((a.nr_sequencia_diluicao = nr_sequencia_p) or (a.nr_sequencia = nr_sequencia_p))
	  and 	a.nr_prescricao = nr_prescricao_p
	  and	a.cd_material = b.cd_material
	  and	a.ie_agrupador IN (3,7,9)
	order by a.ie_agrupador desc;

BEGIN

select	b.qt_dose,
	b.qt_min_aplicacao,
	b.qt_hora_aplicacao,
	b.cd_unidade_medida_dose,
	b.qt_solucao,
	b.cd_unidade_medida,
	b.cd_material,
	a.cd_estabelecimento,
	b.cd_intervalo,
	b.ie_via_aplicacao
into	qt_dose_mat_w,
	qt_min_aplicacao_w,
	qt_hora_aplicacao_w,
	cd_unid_med_dose_mat_w,
	qt_solucao_ww,
	cd_unidade_medida_w,
	cd_medic_w,
	cd_estabelecimento_w,
	cd_intervalo_w,
	ie_via_aplicacao_w
from 	prescr_material b,
	prescr_medica a
where 	a.nr_prescricao	= b.nr_prescricao
and	b.nr_sequencia	= nr_sequencia_p
and	a.nr_prescricao	= nr_prescricao_p
and	b.ie_agrupador	in (1, 8, 14);

select	nvl(max(ie_aplicar),'N')
into	ie_aplicar_w
from	parametro_medico
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(cd_intervalo_w is not null) then
	select	max(ds_prescricao)
	into	ds_intervalo_w
	from	intervalo_prescricao
	where	cd_intervalo	= cd_intervalo_w;
	
	if	(ie_via_aplicacao_w is not null) then
		ds_intervalo_w	:= ds_intervalo_w ||' '||ie_via_aplicacao_w;
	end if;
end if;

select	nvl(max(ie_descr_redu_dil),'N')
into	ie_descr_redu_dil_w
from	parametro_medico
where	cd_estabelecimento	= cd_estabelecimento_w;

select	obter_conversao_ml(cd_medic_w,qt_dose_mat_w,cd_unid_med_dose_mat_w)
into	qt_conv_ml_w
from	dual;

if	(qt_conv_ml_w > 0) and
	(upper(cd_unid_med_dose_mat_w) <> upper(obter_unid_med_usua('ML'))) then
	if	(qt_conv_ml_w >= 1) then
		ds_conv_ml_w	:= to_char(qt_conv_ml_w)||' '||obter_unid_med_usua('ml');
	else
		ds_conv_ml_w	:= '0'||to_char(qt_conv_ml_w)||' '||obter_unid_med_usua('ml');
	end if;
end if;

qt_dose_mat_str_w	:= qt_dose_mat_w;

open c01;
loop
fetch c01 into
	cd_material_w,
	qt_dose_w,
	qt_vol_adic_reconst_w,
	qt_min_aplicacao_ant_w,
	qt_hora_aplicacao_ant_w,
	cd_unid_med_dose_w,
	ie_agrupador_w,
	qt_solucao_w,
	qt_dose_ml_w,
	ds_material_ww,
	ds_reduzida_w;
exit when c01%notfound;
	begin
	
	if 	(cd_material_w is not null) and
		(qt_dose_w is not null) and
		(cd_unid_med_dose_w is not null) and
		(ie_agrupador_w = 9) then
		begin
	
		select	obter_conversao_ml(cd_material_w,qt_dose_w + qt_vol_adic_reconst_w,cd_unid_med_dose_w)
		into	qt_conv_ml_w
		from	dual;
		
		if	(upper(cd_unid_med_dose_mat_w) = upper(obter_unid_med_usua('ML'))) then
			begin
			qt_conv_ml_w	:= qt_dose_mat_w;
			end;
		else
			begin
			qt_dose_unid_med_cons_w	:= obter_conversao_unid_med_cons(cd_medic_w,cd_unid_med_dose_mat_w,qt_dose_mat_w);
			qt_conv_ml_w		:= qt_dose_unid_med_cons_w * qt_conv_ml_w;
			end;
		end if;
		
		if	(qt_conv_ml_w > 0) then
			if	(qt_conv_ml_w >= 1) then
				ds_conv_ml_w	:= to_char(qt_conv_ml_w)||' '||obter_unid_med_usua('ml');
			else
				ds_conv_ml_w	:= '0'||to_char(qt_conv_ml_w)||' '||obter_unid_med_usua('ml');
			end if;
		end if;
	
		select 	decode(ie_descr_redu_dil_w,'S',ds_reduzida_w,ds_material_ww)
		into	ds_material_w
		from 	dual;
		
		if	(qt_dose_w < 1) or
			(mod(qt_dose_w, trunc(qt_dose_w)) <> 0) then
			mascara_w	:= 0;
		else
			mascara_w	:= 1;
		end if;

		Select 	decode(mascara_w, 0, campo_mascara(qt_dose_w, 2), campo_mascara(qt_dose_w, 0))
		into	qt_dose_str_w
		from 	dual;
		
		ds_reconstituir_w := 	ds_reconstituir_w ||wheb_mensagem_pck.get_texto(306838) ||' ' ||cd_unidade_medida_w|| -- Reconstituir cada
				' ' || wheb_mensagem_pck.get_texto(306840) || ' '||replace(qt_dose_str_w,'.',',')||' '||cd_unid_med_dose_w|| -- em
				' ' || wheb_mensagem_pck.get_texto(306844) || ' '||ds_material_w || chr(13) || chr(10); -- de
		end;
	end if;

	if 	(cd_material_w is not null) and
		(qt_dose_w is not null) and
		(cd_unid_med_dose_w is not null) and
		(ie_agrupador_w = 3) then
		begin
		qt_ml_diluente_w	:= qt_dose_ml_w;
		select 	decode(ie_descr_redu_dil_w,'S',ds_reduzida_w,ds_material_ww)
		into	ds_material_w
		from 	dual;
		
		ds_aux_diluir_w	:= wheb_mensagem_pck.get_texto(306852); --  do medicamento
		if	(ds_reconstituir_w is not null) then
			ds_aux_diluir_w	:= wheb_mensagem_pck.get_texto(306853); --  da reconstitui��o
		end if;
		
		ds_dose_diluicao_w	:= replace(qt_dose_ml_w,'.',',') ||' '||obter_unid_med_usua('ml');
		if	(qt_dose_ml_w = 0) then
			begin
			ds_dose_diluicao_w	:= replace(qt_dose_w,'.',',') ||' '||cd_unid_med_dose_w;
			end;
		end if;
		
		if	(ds_conv_ml_w is null) and
			(ds_diluir_w is null) then
			ds_diluir_w := 	ds_diluir_w || wheb_mensagem_pck.get_texto(306860) || ' '||replace(qt_dose_mat_str_w,'.',',')||' '||cd_unid_med_dose_mat_w|| ds_aux_diluir_w||' ' || wheb_mensagem_pck.get_texto(306840) || ' '|| -- Diluir	-- em
				ds_dose_diluicao_w||' ' || wheb_mensagem_pck.get_texto(306844) || ' '||ds_material_w || chr(13) || chr(10); -- de
		elsif	(ds_diluir_w is null) then
			ds_diluir_w := 	ds_diluir_w || wheb_mensagem_pck.get_texto(306860) || ' '||ds_conv_ml_w ||ds_aux_diluir_w||' ' || wheb_mensagem_pck.get_texto(306840) || ' '|| -- Diluir	-- em
				ds_dose_diluicao_w||' ' || wheb_mensagem_pck.get_texto(306844) || ' '||ds_material_w || chr(13) || chr(10); -- de
		else
			ds_diluir_w := 	ds_diluir_w || wheb_mensagem_pck.get_texto(306860) || ' '||  wheb_mensagem_pck.get_texto(306869) || ' ' || -- Diluir a solu��o em 
				ds_dose_diluicao_w||' ' || wheb_mensagem_pck.get_texto(306844) || ' '||ds_material_w || chr(13) || chr(10); -- de
		end if;
		end;
	end if;

	if 	(cd_material_w is not null) and
		(qt_dose_w is not null) and
		(cd_unid_med_dose_w is not null) and
		(ie_agrupador_w = 7) then
		begin
		select 	decode(ie_descr_redu_dil_w,'S',ds_reduzida_w,ds_material_ww)
		into	ds_material_w
		from 	dual;
		
		cd_volume_final_w := qt_dose_ml_w + qt_solucao_w;
	
		if	(qt_solucao_w < 1) or
			(mod(qt_solucao_w, trunc(qt_solucao_w)) <> 0) then
			mascara_w	:= 0;
		else
			mascara_w	:= 1;
		end if;

		ds_dose_diluicao_w	:= replace(qt_dose_ml_w,'.',',') ||' '||obter_unid_med_usua('ml');
		if	(qt_dose_ml_w = 0) then
			begin
			ds_dose_diluicao_w	:= replace(qt_dose_w,'.',',') ||' '||cd_unid_med_dose_w;
			end;
		end if;
		
		Select 	decode(mascara_w, 0, campo_mascara(qt_solucao_w, 2), campo_mascara(qt_solucao_w, 0))
		into	qt_solucao_str_w
		from 	dual;

		ds_rediluir_w := ds_rediluir_w||wheb_mensagem_pck.get_texto(306848) || ' '||replace(qt_solucao_str_w,'.',',')||' '||obter_unid_med_usua('ml')||' ' || wheb_mensagem_pck.get_texto(309596) || ' '|| -- Rediluir	-- da solu��o em
				 ds_dose_diluicao_w ||' ' || wheb_mensagem_pck.get_texto(306844) || ' '||ds_material_w || chr(13) || chr(10); -- de
		end;
	end if;
	end;
end loop;
close c01;

if	(cd_volume_final_w < 1) or
	(mod(cd_volume_final_w, trunc(cd_volume_final_w)) <> 0) then
	mascara_w	:= 0;
else
	mascara_w	:= 1;
end if;

Select 	decode(mascara_w, 0, campo_mascara(cd_volume_final_w, 2), campo_mascara(cd_volume_final_w, 0))
into	cd_volume_final_str_w
from 	dual;

if	(ds_rediluir_w is not null) then
	ds_volume_w	:= ' '||cd_volume_final_str_w ||' '||obter_unid_med_usua('ml');
elsif	(ds_diluir_w is not null) then
	ds_volume_w	:= ' '||to_char(qt_conv_ml_w + qt_ml_diluente_w) ||' '||obter_unid_med_usua('ml');
elsif	(ds_conv_ml_w is not null) then
	ds_volume_w	:= ' '||ds_conv_ml_w;
end if;

ds_volume_w	:= replace(ds_volume_w,'.',',');
ds_retorno_w	:= replace(ds_volume_w,' ,',' 0,');

RETURN	substr(ds_retorno_w,1,2000);

end Obter_Volume_Diluicao;
/
