CREATE OR REPLACE 
function qt_obter_dados_pendencia(
				nr_seq_pend_agenda_p	number,
				ie_opcao_p		varchar2,
				nm_usuario_p		varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(255 char);
nr_seq_paciente_w	number(10);
cd_pessoa_fisica_w	varchar2(15);
cd_protocolo_w		number(10);
qt_tempo_medic_w	number(10);
ie_protocolo_livre_w	varchar2(1);
cd_setor_atendimento_w	number(5,0);
cd_medico_resp_w	varchar2(10);
qt_dias_intervalo_w	number(3,0);
ds_protocolo_w		Varchar2(255);
ds_medic_w		Varchar2(255);
nr_prontuario_w		number(10,0);
nr_seq_medic_w		number(10,0);
nr_seq_classif_w	number(10,0);
dt_cancelamento_w	date;

begin
if 	(ie_opcao_p	<> 'DPM') then
	select	max(a.nr_seq_paciente),
		max(a.cd_pessoa_fisica),
		max(a.cd_protocolo),
		max(a.qt_tempo_medic),
		max(a.ie_protocolo_livre),
		max(a.cd_setor_atendimento),
		max(a.cd_medico_resp),
		max(a.qt_dias_intervalo),		
		max(a.nr_seq_medicacao),
		max(b.dt_cancelamento)
	into	nr_seq_paciente_w,
		cd_pessoa_fisica_w,
		cd_protocolo_w,
		qt_tempo_medic_w,
		ie_protocolo_livre_w,
		cd_setor_atendimento_w,
		cd_medico_resp_w,
		qt_dias_intervalo_w,		
		nr_seq_medic_w,
		dt_cancelamento_w
	from	paciente_setor a,
		paciente_atendimento b
	where	a.nr_seq_paciente	= b.nr_seq_paciente
	and	b.nr_seq_pend_agenda	= nr_seq_pend_agenda_p;

	select	max(nr_prontuario)
	into	nr_prontuario_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
else
	select	nvl(max(obter_desc_protocolo(cd_protocolo)),' '),
		nvl(max(obter_desc_protocolo_medic(nr_seq_medicacao, cd_protocolo)),' ')
	into	ds_protocolo_w,
		ds_medic_w		
	from	paciente_setor a,
		paciente_atendimento b
	where	a.nr_seq_paciente	= b.nr_seq_paciente
	and	b.nr_seq_pend_agenda	= nr_seq_pend_agenda_p;
end if;	

if	(ie_opcao_p	= 'P') then
	ds_Retorno_w	:= cd_pessoa_fisica_w;
elsif	(ie_opcao_p	= 'SP') then
	ds_Retorno_w	:= nr_seq_paciente_w;
elsif	(ie_opcao_p	= 'PT') then
	ds_retorno_w	:= cd_protocolo_w;
elsif	(ie_opcao_p	= 'T') then
	ds_retorno_w	:= qt_tempo_medic_w;
elsif	(ie_opcao_p	= 'L') then
	ds_retorno_w	:= ie_protocolo_livre_w;
elsif	(ie_opcao_p	= 'SR') then
	ds_retorno_w	:= cd_setor_atendimento_w;
elsif	(ie_opcao_p	= 'NR') then
	ds_retorno_w	:= substr(obter_nome_medico(cd_medico_resp_w,'N'),1,255);
elsif	(ie_opcao_p	= 'CR') then
	ds_retorno_w	:=cd_medico_resp_w;
elsif	(ie_opcao_p	= 'DI') then
	ds_retorno_w	:=qt_dias_intervalo_w;
elsif   (ie_opcao_p	= 'MEDIC') then
	ds_retorno_w	:= nr_seq_medic_w;
elsif	(ie_opcao_p	= 'DPM') then
	if	(ds_protocolo_w <> ' ') or (ds_medic_w <> ' ') then
		ds_retorno_w	:= substr(ds_protocolo_w ||' / '|| ds_medic_w, 1, 255);
	end if;
elsif	(ie_opcao_p	= 'NP') then
	ds_retorno_w	:= nr_prontuario_w;
elsif	(ie_opcao_p	= 'DSR') then
	select	max(ds_setor_atendimento)
	into	ds_retorno_w
	from	setor_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_w;
elsif	(ie_opcao_p	= 'CLP') then	
	select	nvl(max(nr_seq_classif), 0)
	into	nr_seq_classif_w
	from	protocolo_medicacao
	where	cd_protocolo = cd_protocolo_w
	and	nr_sequencia = nr_seq_medic_w;
	
	if	(nr_seq_classif_w <> 0)	then
		select	ds_classificacao
		into	ds_retorno_w
		from	classif_subtipo_protocolo
		where	nr_sequencia = nr_seq_classif_w;
	
	end if;
elsif	(ie_opcao_p	= 'COR') then
	select	nvl(max(nr_seq_classif), 0)
	into	nr_seq_classif_w
	from	protocolo_medicacao
	where	cd_protocolo = cd_protocolo_w
	and	nr_sequencia = nr_seq_medic_w;
	
	if	(nr_seq_classif_w <> 0)	then
		select	ds_cor
		into	ds_retorno_w
		from	classif_subtipo_protocolo
		where	nr_sequencia = nr_seq_classif_w;	
		
	end if;	
elsif	(ie_opcao_p	= 'DC') then
	ds_retorno_w	:= dt_cancelamento_w;
end if;

return	ds_retorno_w;

end Qt_Obter_Dados_Pendencia;
/
