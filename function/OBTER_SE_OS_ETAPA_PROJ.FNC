create or replace
function Obter_se_OS_etapa_proj (nr_seq_os_p		number)
 		    	return varchar2 is

nr_seq_Proj_cron_etapa_w	number(10);
ie_vinculada_w				varchar2(1) := 'N';
begin

select	max(nr_seq_Proj_cron_etapa)
into	nr_seq_Proj_cron_etapa_w
from	man_ordem_servico
where	nr_sequencia	= nr_seq_os_p;

if	(nr_seq_Proj_cron_etapa_w is not null) then
	ie_vinculada_w	:= 'S';
end if;

return	ie_vinculada_w;

end Obter_se_OS_etapa_proj;
/