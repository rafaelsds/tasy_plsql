create or replace
function lab_obter_se_result_dig	(nr_prescricao_p	number,
				nr_seq_prescr_p	number)
				return varchar2 is
									
									
ds_retorno_w			varchar2(1);									

begin

if 	(nr_prescricao_p is not null) and
	(nr_seq_prescr_p is not null) then
	
	select  decode(count(*),0,'N','S')
	into	ds_retorno_w
	from 	exame_lab_resultado a,
			exame_lab_result_item b
	where 	a.nr_seq_resultado = b.nr_Seq_resultado
	and		(b.dt_digitacao is not null
	or		b.ie_status = 1)
	and		a.nr_prescricao = 	nr_prescricao_p
	and		b.nr_seq_prescr = 	nr_seq_prescr_p;

end if;

return	ds_retorno_w;

end lab_obter_se_result_dig;
/	