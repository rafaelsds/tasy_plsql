create or replace
function verifica_saldo_tit(NR_TITULO_P number) return varchar2 is

vl_saldo_titulo_w     titulo_receber.vl_saldo_titulo%type;

begin

    select	a.vl_saldo_titulo
    into    vl_saldo_titulo_w
    from	titulo_receber a
    where	a.nr_titulo = NR_TITULO_P;
    
    if (nvl(vl_saldo_titulo_w,0) > 0) then
        return 'TRUE';
    else
        return 'FALSE';
    end if;

end verifica_saldo_tit;
/
