create or replace
function obter_vl_rec_consultoria(	dt_mes_referencia_p	date)
				return varchar2 is

qt_registros_w		number(10);
vl_rec_consultoria_w	number(15,2);

begin

select	count(*)
into	qt_registros_w
from 	ctb_movimento 
where 	cd_conta_credito = '2070' 
and 	trunc(dt_movimento,'month') = trunc(dt_mes_referencia_p,'month');

if (qt_registros_w > 0) then

	select	nvl(vl_movimento,0) 
	into	vl_rec_consultoria_w
	from 	ctb_movimento 
	where 	cd_conta_credito = '2070' 
	and 	trunc(dt_movimento,'month') = trunc(dt_mes_referencia_p,'month');

else
	vl_rec_consultoria_w := 0;
end if;

return	replace(campo_mascara(vl_rec_consultoria_w,2),'.',',');

end obter_vl_rec_consultoria;
/