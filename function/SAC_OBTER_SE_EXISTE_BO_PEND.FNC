create or replace
function sac_obter_se_existe_bo_pend(
			cd_level_arvore_p	number,
			nr_seq_level_0_p	number,
			nr_seq_level_1_p	number,
			nr_seq_level_2_p	number,
			nr_seq_level_3_p	number,
			nm_usuario_p	varchar2)
 		    	return varchar2 is

ie_retorno_w		varchar2(1) := 'S';
ie_arvore_grupo_resp_w	varchar2(255);
ds_lista_status_desc_w	varchar2(255);
nr_seq_level_0_w		number(10,0) := nr_seq_level_0_p;
nr_seq_level_1_w		number(10,0) := nr_seq_level_1_p;

begin	
select	nvl(obter_valor_param_usuario(2000,161,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento),'N'),
	obter_valor_param_usuario(2000,104,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento)
into	ie_arvore_grupo_resp_w,
	ds_lista_status_desc_w
from	dual;

if	(ie_arvore_grupo_resp_w = 'N') then
	begin
	if	(cd_level_arvore_p = 0) then
		begin
		select	decode(nr_seq_level_0_p,-1,null,nr_seq_level_0_p)
		into	nr_seq_level_0_w
		from	dual;

		begin
		select	'S'
		into	ie_retorno_w
		from	sac_resp_bol_ocor a
		where	a.nr_seq_responsavel = nvl(nr_seq_level_0_w,a.nr_seq_responsavel)
		and	obter_se_contido_char(a.ie_status,ds_lista_status_desc_w) = 'N'
		and	obter_dados_sac_resp(a.nr_seq_responsavel,nm_usuario_p,'R') = 'S'
		and	rownum < 2;
		exception
		when others then
			ie_retorno_w := 'N';
		end;
		end;
	elsif	(cd_level_arvore_p = 1) then
		begin
		select	'S'
		into	ie_retorno_w
		from	sac_resp_bol_ocor a
		where	a.nr_seq_responsavel = nr_seq_level_0_w
		and	a.nr_seq_classif = nr_seq_level_1_w
		and	obter_se_contido_char(a.ie_status,ds_lista_status_desc_w) = 'N'
		and	rownum < 2;
		exception
		when others then
			ie_retorno_w := 'N';
		end;
	elsif	(cd_level_arvore_p = 2) then
		begin
		select	'S'
		into	ie_retorno_w
		from	sac_resp_bol_ocor a
		where	a.nr_seq_responsavel = nr_seq_level_0_w
		and	a.nr_seq_classif = nr_seq_level_1_w
		and	a.nr_seq_gravidade = nr_seq_level_2_p
		and	obter_se_contido_char(a.ie_status,ds_lista_status_desc_w) = 'N'
		and	rownum < 2;
		exception
		when others then
			ie_retorno_w := 'N';
		end;
	end if;
	end;
elsif	(ie_arvore_grupo_resp_w = 'S') then
	begin
	if	(cd_level_arvore_p = 0) then
		begin
		select	decode(nr_seq_level_0_p,-1,null,nr_seq_level_0_p)
		into	nr_seq_level_0_w
		from	dual;

		begin
		select	'S'
		into	ie_retorno_w
		from	sac_grupo_responsavel c,
			sac_responsavel b,
			sac_resp_bol_ocor a
		where	b.nr_sequencia = a.nr_seq_responsavel 
		and	c.nr_sequencia = b.nr_seq_grupo
		and	nvl(b.ie_situacao,'A') = 'A' 
		and	c.nr_sequencia = nvl(nr_seq_level_0_w,c.nr_sequencia)
		and	obter_se_contido_char(a.ie_status,ds_lista_status_desc_w) = 'N'
		and	obter_dados_sac_resp(b.nr_sequencia,nm_usuario_p,'R') = 'S'
		and	rownum < 2;
		exception
		when others then
			ie_retorno_w := 'N';
		end;
		end;
	elsif	(cd_level_arvore_p = 1) then
		begin
		select	decode(nr_seq_level_1_p,-1,null,nr_seq_level_1_p)
		into	nr_seq_level_1_w
		from	dual;

		begin
		select	'S'
		into	ie_retorno_w
		from	sac_responsavel b,
			sac_resp_bol_ocor a
		where	b.nr_sequencia = a.nr_seq_responsavel 
		and	b.nr_seq_grupo = nr_seq_level_0_w
		and	b.nr_sequencia = nvl(nr_seq_level_1_w,b.nr_sequencia)
		and	obter_se_contido_char(a.ie_status,ds_lista_status_desc_w) = 'N'
		and	obter_dados_sac_resp(b.nr_sequencia,nm_usuario_p,'R') = 'S'
		and	rownum < 2;
		exception
		when others then
			ie_retorno_w := 'N';
		end;
		end;
	elsif	(cd_level_arvore_p = 2) then
		begin
		select	'S'
		into	ie_retorno_w
		from	sac_resp_bol_ocor a
		where	a.nr_seq_responsavel = nr_seq_level_1_w
		and	a.nr_seq_classif = nr_seq_level_2_p
		and	obter_se_contido_char(a.ie_status,ds_lista_status_desc_w) = 'N'
		and	rownum < 2;
		exception
		when others then
			ie_retorno_w := 'N';
		end;
	elsif	(cd_level_arvore_p = 3) then
		begin
		select	'S'
		into	ie_retorno_w
		from	sac_resp_bol_ocor a
		where	a.nr_seq_responsavel = nr_seq_level_1_w
		and	a.nr_seq_classif = nr_seq_level_2_p
		and	a.nr_seq_gravidade = nr_seq_level_3_p
		and	obter_se_contido_char(a.ie_status,ds_lista_status_desc_w) = 'N'
		and	rownum < 2;
		exception
		when others then
			ie_retorno_w := 'N';
		end;
	end if;
	end;
end if;

return	ie_retorno_w;

end sac_obter_se_existe_bo_pend;
/