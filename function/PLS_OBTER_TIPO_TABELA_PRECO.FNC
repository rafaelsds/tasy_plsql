create or replace
function pls_obter_tipo_tabela_preco
			(	nr_seq_regra_p		Number,
				ie_opcao_p		Varchar2,
				cd_estabelecimento_p	Number)
 		    		return Varchar2 is
				
/*	IE_OP��O_P
	
	P Procedimento
	M Material
	S Servico
	
	DS_RETORNO_W
	
	C               Contrato
	CG              Cong�nere
	CO              Coparticipa�
	I               Interc�mbio
	O               Operadora
	P               Prestador
	R               Reembolso		
*/

ds_retorno_w			Varchar2(255)	:= '';

begin

If	(ie_opcao_p = 'P') then

	select	ie_tipo_tabela
	into	ds_retorno_w
	from	pls_regra_preco_proc
	where	nr_sequencia = nr_seq_regra_p
	and	cd_estabelecimento = cd_estabelecimento_p;
	
elsif	(ie_opcao_p = 'M') then
	
	select	ie_tipo_tabela
	into	ds_retorno_w
	from	pls_regra_preco_mat
	where	nr_sequencia = nr_seq_regra_p
	and	cd_estabelecimento = cd_estabelecimento_p;

elsif	(ie_opcao_p = 'M') then
	
	select	ie_tipo_tabela
	into	ds_retorno_w
	from	pls_regra_preco_servico
	where	nr_sequencia = nr_seq_regra_p
	and	cd_estabelecimento = cd_estabelecimento_p;

end if;	

return	ds_retorno_w;

end pls_obter_tipo_tabela_preco;
/