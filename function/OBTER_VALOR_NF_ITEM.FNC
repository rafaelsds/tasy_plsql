create or replace
function obter_valor_nf_item	(nr_sequencia_p		number,
				nr_item_nf_p		number,
				ie_tributo_p		varchar2,
				ie_opcao_p		varchar2)
				return number is

/*
VI - Valor total do item da nota_fiscal_item
TX - Taxa do tributo do item (aliquota)
BC - Valor da base de c�lculo do tributo do item
VT - Valor do tributo do item
*/
				
vl_retorno_w		number(15,2);

begin
if	(ie_opcao_p = 'VI')	then
	select	vl_total_item_nf
	into	vl_retorno_w
	from 	nota_fiscal_item
	where	nr_sequencia	= nr_sequencia_p
	and	nr_item_nf	= nr_item_nf_p;

elsif	(ie_opcao_p = 'TX')	then
	select	t.tx_tributo
	into	vl_retorno_w
	from	nota_fiscal a,
		nota_fiscal_item_trib t,
		tributo b
	where	a.nr_sequencia	= t.nr_sequencia
	and	t.cd_tributo 	= b.cd_tributo
	and	a.nr_sequencia	= nr_sequencia_p
	and	t.nr_item_nf	= nr_item_nf_p
	and	b.ie_tipo_tributo	= ie_tributo_p;

elsif 	(ie_opcao_p = 'BC')	then
	select	nvl(a.vl_base_calculo,0)
	into	vl_retorno_w
	from	nota_fiscal_item_trib a,
		tributo b
	where	a.cd_tributo	= b.cd_tributo
	and	a.nr_sequencia	= nr_sequencia_p
	and	a.nr_item_nf	= nr_item_nf_p
	and	b.ie_tipo_tributo	= ie_tributo_p;
	
elsif 	(ie_opcao_p = 'VT')	then
	select	nvl(a.vl_tributo,0)
	into	vl_retorno_w
	from	nota_fiscal_item_trib a,
		tributo b
	where	a.cd_tributo	= b.cd_tributo
	and	a.nr_sequencia	= nr_sequencia_p
	and	a.nr_item_nf	= nr_item_nf_p
	and	b.ie_tipo_tributo	= ie_tributo_p;

end if;

return	vl_retorno_w;

end obter_valor_nf_item;
/
