create or replace
function pls_obter_vl_calc_analise 
		(	nr_seq_analise_p	number		)
			return Number is
		
vl_retorno_w		pls_conta.vl_total%type;
vl_cobrado_proc_w	pls_conta_proc.vl_procedimento%type;
vl_cobrado_mat_w	pls_conta_mat.vl_material%type;

/*Obter as contas da analise*/
Cursor C01 is
	select	nr_sequencia
	from	pls_conta
	where 	nr_seq_analise = nr_seq_analise_p;

begin
for r_c01_w in C01() loop
	begin
	
	select	sum(vl_procedimento)
	into	vl_cobrado_proc_w
	from	pls_conta_proc
	where	nr_seq_conta = r_c01_w.nr_sequencia;
	
	select	sum(vl_material)
	into	vl_cobrado_mat_w
	from	pls_conta_mat
	where	nr_seq_conta = r_c01_w.nr_sequencia;
	
	vl_retorno_w := nvl(vl_retorno_w,0) + nvl(vl_cobrado_proc_w,0) + nvl(vl_cobrado_mat_w,0);
	end;
end loop;

if	(vl_retorno_w	is null) then
	vl_retorno_w := 0;
end if;
return vl_retorno_w;

end pls_obter_vl_calc_analise;
/
