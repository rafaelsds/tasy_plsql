create or replace
function obter_result_lab_ds(
				nr_seq_prescr_p	number,
				nr_prescricao_p	number)
				return varchar2 is

ds_retorno_w		varchar2(4000);

begin

	begin
	select	ds_resultado
	into	ds_retorno_w
	from	result_laboratorio
	where	nr_seq_prescricao = nr_seq_prescr_p    
	and	nr_prescricao = nr_prescricao_p;  
	exception
		when no_data_found then
		ds_retorno_w := null;
	end;


return ds_retorno_w;
end obter_result_lab_ds;
/