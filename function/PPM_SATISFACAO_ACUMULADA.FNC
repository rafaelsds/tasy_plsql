create or replace 
function PPM_Satisfacao_Acumulada (dt_referencia_p 	date)
	return number is

dt_inicial_w		date;
dt_final_w			date;
pr_satisfacao_w		number(15,4);

begin

dt_inicial_w	:= PKG_DATE_UTILS.start_of(dt_referencia_p, 'YEAR', 0);
dt_final_w		:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_referencia_p, 'YEAR', 0)-1,12, 0);


select	(dividir((
select	count(*)
	from	man_ordem_servico a
	where	PKG_DATE_UTILS.start_of(a.dt_fim_real, 'dd', 0) between dt_inicial_w and dt_final_w
	and	a.ie_grau_satisfacao in('B','O','E')
	and	a.ie_status_ordem = 3
	and	a.nr_seq_meta_pe is null
	and	a.nr_seq_obj_bsc is null
	and	exists	(select	1
		 from	man_ordem_serv_estagio y,
			man_estagio_processo x
		 where	x.nr_sequencia	= y.nr_seq_estagio
		 and	y.nr_seq_ordem	= a.nr_sequencia
		 and	((x.ie_desenv	= 'S') or (x.ie_tecnologia = 'S'))) ), 
		 (
     select count(*)
		from	man_ordem_servico a,
			man_grau_satisf_justif b
		where	a.nr_seq_justif_grau_satisf = b.nr_sequencia(+)
		and	nvl(b.ie_indicador,'S') = 'S'
		and	PKG_DATE_UTILS.start_of(a.dt_fim_real, 'dd', 0) between dt_inicial_w and dt_final_w
		and	nvl(a.ie_grau_satisfacao, 'N') <> 'N'
		and	a.ie_status_ordem = 3
		and	a.nr_seq_meta_pe is null
		and	a.nr_seq_obj_bsc is NULL
    and	exists	(select	1
				from	man_ordem_serv_estagio y,
					man_estagio_processo x
				where	x.nr_sequencia	= y.nr_seq_estagio
				and	y.nr_seq_ordem	= a.nr_sequencia
			and	((x.ie_desenv	= 'S') or (x.ie_tecnologia = 'S'))))) * 100) 
into	pr_satisfacao_w
from 	dual;

return	pr_satisfacao_w;

end	PPM_Satisfacao_Acumulada;
/