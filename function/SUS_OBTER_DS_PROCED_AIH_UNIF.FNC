CREATE OR REPLACE
FUNCTION Sus_Obter_Ds_Proced_Aih_Unif
		(	nr_atendimento_p	Number,
			ie_tipo_proc_aih_p	Number,
			ie_retorno_p		varchar2)
			return Varchar2 is

/* ie_tipo_proc_aih_p
	1 - Solicitado
	2 - Realizado
*/

/* ie_retorno_p
	C - C�digo
	D - Descri��o
*/

nr_aih_w		Number(13);
nr_seq_aih_w		Number(10);
cd_procedimento_solic_w	Number(15);
cd_procedimento_real_w	Number(15);
cd_procedimento_w	Number(15);
ds_retorno_w		Varchar2(255);

BEGIN

begin
select	nr_aih,
	nr_sequencia
into	nr_aih_w,
	nr_seq_aih_w
from	sus_aih_unif
where	nr_atendimento	= nr_atendimento_p;
exception
	when others then
	nr_aih_w	:= 0;
	nr_seq_aih_w	:= 0;
end;

if	(nr_aih_w	> 0) then
	select	cd_procedimento_solic,
		cd_procedimento_real
	into	cd_procedimento_solic_w,
		cd_procedimento_real_w
	from	sus_aih_unif
	where	nr_aih		= nr_aih_w
	and	nr_sequencia	= nr_seq_aih_w;
end if;

if	(ie_tipo_proc_aih_p	= 1) then
	cd_procedimento_w	:= cd_procedimento_solic_w;
elsif	(ie_tipo_proc_aih_p	= 2) then
	cd_procedimento_w	:= cd_procedimento_real_w;
end if;

if	(ie_retorno_p	= 'C') then
	ds_retorno_w	:= cd_procedimento_w;
elsif	(ie_retorno_p	= 'D') then
	ds_retorno_w	:= substr(obter_descricao_procedimento(cd_procedimento_w, 7),1,255);
end if;

return	ds_retorno_w;

END Sus_Obter_Ds_Proced_Aih_Unif;
/