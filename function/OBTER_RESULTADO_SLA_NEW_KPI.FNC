create or replace function obter_resultado_sla_new_kpi(	dt_referencia_p date,
			     										nr_seq_gerencia_p	number default null,
			     										nr_seq_grupo_p	varchar default null)
 		    	return number is

qt_sla_w			number(10, 1);
qt_response_breached_w		number(10);
qt_resolution_breached_w	number(10);
qt_total_w			number(10, 1);
qt_sla_breached_w		number(10);
dt_ref_mes_w		date;
dt_fim_mes_w		date;

begin
  qt_sla_w := 100;
	dt_ref_mes_w	:= trunc(dt_referencia_p,'month');
	dt_fim_mes_w	:= last_day(dt_ref_mes_w) + 86399/86400;  
  
	select  count(status_sla_resolution) qtde
	into	qt_resolution_breached_w
	from    sla_informations
	where   extract_date between dt_ref_mes_w and dt_fim_mes_w
	and     upper(status_sla_resolution) = upper('breached')
	and     upper(philips_classification) = upper('defeito')
	and		develop_management_id <> 68
	and		(nr_seq_gerencia_p is null or develop_management_id = nr_seq_gerencia_p)
	and 	(nr_seq_grupo_p is null or develop_group = nr_seq_grupo_p);
  
  	select  count(status_sla_response)
	into	qt_response_breached_w
	from    sla_informations
	where   extract_date between dt_ref_mes_w and dt_fim_mes_w
	and     upper(status_sla_response) = upper('breached')
	and     upper(philips_classification) = upper('defeito')
	and		develop_management_id <> 68
	and		(nr_seq_gerencia_p is null or develop_management_id = nr_seq_gerencia_p)
	and 	(nr_seq_grupo_p is null or develop_group = nr_seq_grupo_p);
  
  select  count(status_sla_response)
	into	qt_total_w
	from    sla_informations
	where   extract_date between dt_ref_mes_w and dt_fim_mes_w
	and     upper(philips_classification) = upper('defeito')
	and     develop_management_id <> 68 -- GERENCIA ICAP
	and		(nr_seq_gerencia_p is null or develop_management_id = nr_seq_gerencia_p)
	and 	(nr_seq_grupo_p is null or develop_group = nr_seq_grupo_p);
	qt_total_w := qt_total_w * 2; -- uma vez que, contamos 2 slas por ordem de servi�o.
	qt_sla_breached_w := qt_response_breached_w + qt_resolution_breached_w;

	if	(((qt_response_breached_w <> 0) or (qt_resolution_breached_w <> 0)) and (qt_total_w <> 0)) then
		qt_sla_w := 100 - ((qt_sla_breached_w * 100) / qt_total_w);
	end if;

return	qt_sla_w;

end obter_resultado_sla_new_kpi;
/