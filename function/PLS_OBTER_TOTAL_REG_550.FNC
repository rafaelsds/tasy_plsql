create or replace
function pls_obter_total_reg_550
		( 	nr_seq_contestacao_p    	number,
			ds_versao_p			varchar2)
		return number is

qt_retorno_w		number(10);		
		
begin

if	(ds_versao_p = '3.4') then

	select	count(*)
	into	qt_retorno_w
	from	ptu_questionamento a,
		ptu_questionamento_codigo b
	where	a.nr_sequencia = b.nr_seq_registro
	and	a.nr_seq_contestacao = nr_seq_contestacao_p;

end if;

return qt_retorno_w;

end pls_obter_total_reg_550;
/