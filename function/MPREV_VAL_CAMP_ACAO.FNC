create or replace
function mprev_val_camp_acao(nr_seq_campanha_p number,
				nr_seq_acao_p number,
				nr_seq_atual_p number) 
 		    	return varchar2 is			
ds_retorno_w	varchar2(10);
qt_acao_w	number(10);
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar a possibilidade de a a��o a ser inserida ser igual a alguma a��o ja cadastrada para a campanha selecionada.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
	if (nr_seq_campanha_p is not null) and
	   (nr_seq_acao_p is not null) then 
		select	count(1)
		into	qt_acao_w 
		from	mprev_camp_acao
		where	nr_seq_campanha = nr_seq_campanha_p
		and	nr_seq_acao = nr_seq_acao_p
		and	nr_seq_atual_p <> nr_sequencia;
		
		if (qt_acao_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(247940);
		else 
			ds_retorno_w := 'S';
		end if;
	end if;
return	ds_retorno_w;

end mprev_val_camp_acao;
/
