create or replace
function obter_dados_pf_dt
			(	cd_pessoa_fisica_p		varchar2,
				ie_opcao_p			varchar2)
				return date is

/* IE_OPCAO_P
DN	  - Data de nascimento
DO	  - Data de �bito
DRG	  - Data de emiss�o da carteira de identidade
VRG	  - Data de validade da carteira de identidade
DCB	  - Data chegada Brasil
ECTPS - Data de emiss�o do CTPS
DEC	  - Data de emiss�o da certid�o de casamento
DNAT  - Data da naturaliza��o
*/
ds_retorno_w date;

begin
if	(nvl(cd_pessoa_fisica_p,'0') <> '0') then
	begin

	begin
	if	(ie_opcao_p	= 'DN') then
		select	max(dt_nascimento)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	elsif	(ie_opcao_p	= 'DO') then
		select	max(dt_obito)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	elsif	(ie_opcao_p	= 'DRG') then
		select	max(dt_emissao_ci)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	elsif	(ie_opcao_p	= 'VRG') then
		select	max(dt_validade_rg)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	elsif	(ie_opcao_p	=  'DCB') then
		select	max(dt_chegada_brasil)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	elsif	(ie_opcao_p	=  'ECTPS') then
		select	max(dt_emissao_ctps)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	elsif	(ie_opcao_p	=  'DEC') then
		select	max(dt_emissao_cert_casamento)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;	
	elsif	(ie_opcao_p	=  'DNAT') then
		select	max(dt_naturalizacao_pf)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;		
	end if;	
	end;
	
	end;
end if;

return ds_retorno_w;

end obter_dados_pf_dt;
/
