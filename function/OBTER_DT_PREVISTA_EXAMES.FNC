create or replace
function Obter_dt_prevista_exames	(nr_prescricao_p	number,
					nr_seq_exame_p		number,
					cd_material_exame_p	varchar2,
					dt_prev_execucao_p	date)
					return varchar2 is
dt_prevista_w	varchar2(50);
qt_itens_w	number(10);

begin

select 	count(*)
into	qt_itens_w
from	prescr_procedimento
where	nr_prescricao 	= nr_prescricao_p
and	nr_seq_exame	= nr_seq_exame_p;

dt_prevista_w	:= null;

if	(qt_itens_w > 1) then
	dt_prevista_w	:= to_char(dt_prev_execucao_p,'dd/mm/yyyy hh24:mi:ss');
end if;

return	dt_prevista_w;

end Obter_dt_prevista_exames;
/