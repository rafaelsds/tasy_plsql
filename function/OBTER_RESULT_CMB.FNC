create or replace function obter_result_cmb (qt_cmb_p		number)
 		    	return varchar2 is

ds_retorno_w	 varchar2(50);
sql_w          varchar2(300);
begin

  begin
      sql_w := 'CALL definir_valor_ds_md(:1) INTO :ds_retorno_w';
      EXECUTE IMMEDIATE sql_w USING IN qt_cmb_p,                                    
                                    OUT ds_retorno_w;
       
  exception
    when others then
      ds_retorno_w := null;
  end;


return ds_retorno_w;

end obter_result_cmb;
/
