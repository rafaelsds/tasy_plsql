create or replace
function pls_obter_prest_solic_conta(	nr_seq_conta_p		pls_conta.nr_sequencia%type,
					nr_seq_guia_p		pls_conta.nr_seq_guia%type,
					nr_seq_prestador_p	pls_conta.nr_seq_prestador%type)
 		    	return Number is
--Rotina respons�vel por trazer o prestador solicitante da conta  baseado na OS 643699, ir� verificar primeiro na requisicao
--caso o n�o exista requisi��o ou este seja nulo, ir� verificar na guia, e se ainda assim for nulo ir� utilizar o prestador solicitante da conta
nr_seq_prest_solic_w		pls_requisicao.nr_seq_prestador%type;

begin

select	max(c.nr_seq_prestador)
into	nr_seq_prest_solic_w
from	pls_requisicao c,
	pls_execucao_req_item a
where	a.nr_seq_requisicao	= c.nr_sequencia
and	a.nr_seq_guia		= nr_seq_guia_p;

if	(nr_seq_prest_solic_w	is null) then

	select	max(nr_seq_prestador)
	into	nr_seq_prest_solic_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_p;
	
	if	(nr_seq_prest_solic_w 	is null) then
		nr_seq_prest_solic_w	:= nr_seq_prestador_p;
	end if;
end if;

return	nr_seq_prest_solic_w;

end pls_obter_prest_solic_conta;
/
