CREATE OR REPLACE FUNCTION SITE_M_DESCRICAO_SNOMED(nr_sequencia_p	NUMBER) RETURN VARCHAR2 IS
ds_retorno_w		VARCHAR2(2000);

BEGIN

    SELECT c.ds_term
      INTO ds_retorno_w
      FROM sm_location e, 
           res_cadastro_ontologia_cli b,
           snomed_descricao c
     WHERE e.nr_sequencia= b.cd_valor_tasy
       AND c.DS_ID  = b.CD_VALOR_ONTOLOGIA
       AND b.nm_tabela='SM_LOCATION'
       AND c.ds_effectivetime = (SELECT MAX(d.ds_effectivetime) 
                                   FROM snomed_descricao d 
                                  WHERE d.ds_id = c.ds_id)
       AND b.cd_valor_tasy = nr_sequencia_p;

    RETURN ds_retorno_w;
End SITE_M_DESCRICAO_SNOMED;
/
