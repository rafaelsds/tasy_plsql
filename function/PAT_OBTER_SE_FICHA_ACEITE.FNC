create or replace
function pat_obter_se_ficha_aceite(	cd_estabelecimento_p	number,
					nr_seq_bem_p		number,
					nr_seq_local_p		number default null,
					nr_seq_tipo_p		number default null)
					return varchar2 is

cd_empresa_w				number(10);
ie_gerar_w				varchar2(01)	:= 'N';
ie_retorno_w				varchar2(01)	:= 'N';
nr_seq_grupo_w				number(10);
nr_seq_local_w				number(10);
nr_seq_tipo_w				number(10);
qt_registro_w				number(10);

cursor c01 is
select	a.ie_gerar
from	pat_regra_ficha_aceite a
where	a.cd_empresa		= cd_empresa_w
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	nvl(a.nr_seq_grupo, nr_seq_grupo_w) = nr_seq_grupo_w
and	nvl(a.nr_seq_tipo, nr_seq_tipo_w) = nr_seq_tipo_w
and	nvl(a.nr_seq_local, nr_seq_local_w) = nr_seq_local_w
order	by	nvl(a.nr_seq_local,0),
		nvl(a.nr_seq_tipo,0),
		nvl(a.nr_seq_grupo,0);

begin

cd_empresa_w	:= obter_empresa_estab(cd_estabelecimento_p);

select	count(*)
into	qt_registro_w
from	pat_regra_ficha_aceite
where	cd_empresa	= cd_empresa_w;

if	(qt_registro_w > 0) then
	begin
	
	nr_seq_local_w	:= nr_seq_local_p;
	nr_seq_tipo_w	:= nr_seq_tipo_p;
	
	if	(nr_seq_local_w is null) then
		begin
		select	nvl(max(nr_seq_tipo),0),
			nvl(max(nr_seq_local),0)
		into	nr_seq_tipo_w,
			nr_seq_local_w
		from	pat_bem
		where	nr_sequencia	= nr_seq_bem_p;
		end;
	end if;
	
	select	nvl(max(nr_seq_grupo),0)
	into	nr_seq_grupo_w
	from	pat_tipo_bem
	where	nr_sequencia	= nr_seq_tipo_w;
	
	open C01;
	loop
	fetch C01 into	
		ie_gerar_w;
	exit when C01%notfound;
		begin
		
		ie_retorno_w	:= ie_gerar_w;
		
		end;
	end loop;
	close C01;

	end;
end if;

return	ie_retorno_w;

end pat_obter_se_ficha_aceite;
/