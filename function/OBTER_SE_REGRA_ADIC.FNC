create or replace
function obter_se_regra_adic 	(nr_seq_criterio_p	number,
				nr_seq_proc_p		number,
				nr_seq_partic_p		number)	return varchar2 is

ds_retorno_w		varchar2(1);
nr_Seq_adic_w		number(10);
vl_retorno_w		number(5);
ds_function_w		varchar2(255);
ds_comando_aux_w	varchar2(2000);
ds_comando_w		varchar2(2000);
begin
/*	FUNCTIONS UTILIZADAS 
		obter_se_gera_repasse_30_dias (n�o vai na vers�o)
		HRP_OBTER_SE_PROC_REPASSE (n�o vai na vers�o)
*/
ds_retorno_w	:= 'S';

select 	nvl(Max(nr_Sequencia),0)
into	nr_Seq_adic_w
from 	proc_crit_repasse_adic
where	nr_seq_criterio = nr_seq_criterio_p;

if (nr_Seq_adic_w > 0) then

	select 	Upper(ds_function)
	into	ds_function_w
	from 	proc_crit_repasse_adic	
	where	nr_sequencia = nr_Seq_adic_w;
	
	ds_comando_aux_w	:= replace(ds_function_w,':NR_SEQ_PROC',nr_seq_proc_p);
	ds_comando_w		:= 'select ' ||  ds_comando_aux_w || ' from dual';
	
	Obter_Valor_Dinamico(ds_comando_w, vl_retorno_w);
	
	if (nvl(vl_retorno_w,0) > 0) then
		ds_retorno_w := 'N';
	end if;
end if;

return ds_retorno_w;

end;
/