create or replace 
function Qt_Obter_Status_Pendencia_2(
				nr_seq_pend_agenda_p	number)
 		    	return varchar2 is

ds_retorno_w		number(1)	:= 3;
qt_dias_pendencia_w	number(10);
qt_dias_w		number(10,0);
qt_dias_canc_w		number(10,0);
qt_dias_susp_medico_w	number(10);
			
begin
/*
select	count(*)
into	qt_dias_pendencia_w
from	paciente_atendimento a
where	nr_seq_pend_agenda	= nr_seq_pend_agenda_p
and		dt_suspensao is null
and		dt_cancelamento is null
and		not exists ( select	1
				from	agenda_quimio b
				where	a.nr_seq_Atendimento		= b.nr_seq_atendimento
				and	nvl(b.ie_Status_agenda, 'X')	<> 'C'
				and	trunc(b.dt_Agenda)		= trunc(nvl(dt_prevista_agenda, nvl(dt_real, dt_prevista))));
	*/			
select	count(*)
	into	qt_dias_pendencia_w
	from	paciente_atendimento a,
			agenda_quimio b
	where	a.nr_seq_pend_agenda	= nr_seq_pend_agenda_p
	and		a.nr_Seq_Atendimento	= b.nr_Seq_Atendimento
	and		nvl(b.ie_Status_agenda, 'X')	<> 'C'
	and		a.dt_suspensao is null
	and		a.dt_cancelamento is null
	and		trunc(b.dt_Agenda)		= trunc(nvl(a.dt_prevista_agenda, nvl(a.dt_real, a.dt_prevista)));

if	(qt_dias_pendencia_w	= 0) then
	ds_retorno_w	:= 2;
else
				
	/*select	count(*)
	into	qt_dias_pendencia_w
	from	paciente_atendimento a,
			agenda_quimio b
	where	a.nr_seq_pend_agenda	= nr_seq_pend_agenda_p
	and		a.nr_Seq_Atendimento	= b.nr_Seq_Atendimento
	and		nvl(b.ie_Status_agenda, 'X')	<> 'C'
	and		trunc(b.dt_Agenda)		= trunc(nvl(a.dt_prevista_agenda, nvl(a.dt_real, a.dt_prevista)));
				
	if	(qt_dias_pendencia_w	= 0) then*/
	
		select	sum(decode(dt_suspensao, null, 1, 0)),
				sum(decode(dt_suspensao, null, 0, 1))
		into	qt_dias_w,
				qt_dias_susp_medico_W
		from	paciente_atendimento a
		where	a.nr_seq_pend_agenda	= nr_seq_pend_agenda_p;
			
		/*select	count(*)
		into	qt_dias_susp_medico_w
		from	paciente_atendimento
		where	nr_seq_pend_agenda	= nr_seq_pend_agenda_p
		and		dt_suspensao_medico is not null;*/
		if	(qt_dias_susp_medico_w	= qt_dias_w) or
			(qt_dias_pendencia_w	= qt_dias_w) then
			ds_retorno_w	:= 1;
		elsif	(qt_dias_w	> qt_dias_pendencia_w) and
			(qt_dias_pendencia_w	> 0) then
			ds_retorno_w	:= 3;
		elsif 	(qt_dias_pendencia_w = 0) then
			ds_retorno_w	:= 2;
		else
			ds_retorno_w	:= 3;
		end if;
	--end if;
end if;


return	ds_retorno_w;

end Qt_Obter_Status_Pendencia_2;
/
