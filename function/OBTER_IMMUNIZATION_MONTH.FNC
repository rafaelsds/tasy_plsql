create or replace function obter_immunization_month(nr_seq_vacina_p  	number ,
                                                    ie_dose_p varchar2)
 		    	return number is

ie_retorno_w		number;
ie_qt_idade_minima_mes number(15);    
ie_qt_idade_minima_ano number(15);

begin

select nvl(qt_idade_minima_mes,0),nvl(qt_idade_minima_ano,0) 
into ie_qt_idade_minima_mes,ie_qt_idade_minima_ano
from Vacina_Calendario where nr_seq_vacina = nr_seq_vacina_p and ie_dose = ie_dose_p and rownum=1 ;

ie_retorno_w := ie_qt_idade_minima_mes + ie_qt_idade_minima_ano * 12;

return	ie_retorno_w;

end obter_immunization_month;
/