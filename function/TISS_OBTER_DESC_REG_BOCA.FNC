create or replace 
function tiss_obter_desc_reg_boca(cd_regiao_p	varchar2) 
								return	varchar2 is							
								
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter  e descria��o da regi�o da boca
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
ds_retorno_w
Retorna descri��o da regi�o da boca, da tabela tiss_regiao_boca
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w	tiss_regiao_boca.ds_regiao%type;

begin

if	(cd_regiao_p is not null)	then
	select	b.ds_regiao 
	into	ds_retorno_w
	from	tiss_regiao_boca b
	where	b.cd_regiao = upper(cd_regiao_p);
end if;

return ds_retorno_w;

end tiss_obter_desc_reg_boca;
/