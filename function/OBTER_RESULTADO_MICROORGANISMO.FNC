create or replace
function obter_resultado_microorganismo(nr_prescricao_p 	number,
					nr_seq_exame_p 		number,
					nr_seq_material_p	number default null,
					nr_seq_resultado_lab_p	number default 0)
					return varchar2 is
ds_medicamento_w	varchar2(20);
ie_tipo_w		varchar2(30);
cih_microorganismo_w	varchar2(30);
ds_resultado_w		varchar2(2000) := '';
cd_microorganismo_w	number(10);
ds_microorganismo_w	varchar2(255);
ie_ordem_w		number(1);	
QT_MICROORGANISMO_w	varchar2(150);	
ds_resultado_antib_w exame_lab_result_antib.ds_resultado_antib%TYPE;

Cursor C01 is
	select 	distinct a.cd_microorganismo,
		substr(obter_cih_microorganismo(a.cd_microorganismo),1,255) ds,
		a.QT_MICROORGANISMO 
	from	exame_lab_result_antib a, 
		exame_lab_result_item b,
		exame_laboratorio c,
		exame_lab_resultado d
	where 	 a.nr_seq_resultado = b.nr_seq_resultado
	and    	d.nr_seq_resultado = b.nr_seq_resultado 
	and    	a.nr_seq_result_item = b.nr_sequencia
	and    	c.nr_seq_exame = b.nr_seq_exame	
	and	a.ie_resultado <> 'N'
	and    	c.nr_seq_exame = nr_seq_exame_p
	and	d.nr_prescricao = nr_prescricao_p
	and	((nr_seq_material_p is null) or (b.nr_seq_material = nr_seq_material_p))
	order by ds;

cursor c02 is
	select 	substr(obter_desc_medic_cih(a.cd_medicamento),1,20) ds_medicamento, 
		decode(a.ie_resultado, 'S', obter_desc_expressao(306274), 'I', obter_desc_expressao(304784), 'R', obter_desc_expressao(306164), 'D', obter_desc_expressao(344985)) ie_tipo,
        decode(a.ie_resultado, 'S',1 , 'I', 3, 'R', 2, 'D', 4)	ie_ordem,
        a.ds_resultado_antib
	from    	exame_lab_result_antib a, 
		exame_lab_result_item b,
		exame_laboratorio c,
		exame_lab_resultado d
	where  	a.nr_seq_resultado = b.nr_seq_resultado
	and    	d.nr_seq_resultado = b.nr_seq_resultado 
	and    	a.nr_seq_result_item = b.nr_sequencia
	and    	c.nr_seq_exame = b.nr_seq_exame	
	and	a.ie_resultado <> 'N'
	and     	c.nr_seq_exame = nr_seq_exame_p
	and	d.nr_prescricao = nr_prescricao_p
	and	a.cd_microorganismo =  cd_microorganismo_w
	and	((nr_seq_material_p is null) or (b.nr_seq_material = nr_seq_material_p))
	and	((nvl(nr_seq_resultado_lab_p,0) = 0) or (b.nr_sequencia = nr_seq_resultado_lab_p))
	order by ie_ordem, ds_medicamento;
	
	
	
Cursor C03 is
	select 	distinct a.cd_microorganismo,
		substr(obter_cih_microorganismo(a.cd_microorganismo),1,30)  ds,
		a.QT_MICROORGANISMO 
	from	exame_lab_result_antib a, 
		exame_lab_result_item b,
		exame_laboratorio c,
		exame_lab_resultado d
	where 	 a.nr_seq_resultado = b.nr_seq_resultado
	and    	d.nr_seq_resultado = b.nr_seq_resultado 
	and    	a.nr_seq_result_item = b.nr_sequencia
	and    	c.nr_seq_exame = b.nr_seq_exame	
	and	nvl(a.ie_resultado,'N') ='N'
	and    	c.nr_seq_exame = nr_seq_exame_p
	and	d.nr_prescricao = nr_prescricao_p
	and	((nr_seq_material_p is null) or (b.nr_seq_material = nr_seq_material_p))
	order by ds;	
begin		
open c01;
loop
fetch c01 into	
	cd_microorganismo_w,
	ds_microorganismo_w,
	QT_MICROORGANISMO_w;
exit when c01%notfound;					
	
	ds_resultado_w := ds_resultado_w||ds_microorganismo_w||'   -   '||QT_MICROORGANISMO_w|| chr(13) || chr(10);
	open C02;
	loop
	fetch C02 into	
		ds_medicamento_w,
		ie_tipo_w,
		ie_ordem_w,
        ds_resultado_antib_w;
	exit when C02%notfound;
		begin
        if (ds_resultado_antib_w is null) then
            ds_resultado_w := ds_resultado_w||'       '||ds_medicamento_w||' - '||ie_tipo_w|| chr(13) || chr(10);
        else 
            ds_resultado_w := ds_resultado_w||'       '||ds_medicamento_w||' - '||ie_tipo_w||' ('||ds_resultado_antib_w||')'|| chr(13) || chr(10);
        end if;
		end;
	end loop;
	close C02;
	
end loop;
close c01;
	
	
if	(ds_resultado_w		is null) then
	open C03;
	loop
	fetch C03 into	
		cd_microorganismo_w,
		ds_microorganismo_w,
		QT_MICROORGANISMO_w;
	exit when C03%notfound;
		begin
		ds_resultado_w := ds_resultado_w||ds_microorganismo_w||'   -   '||QT_MICROORGANISMO_w|| chr(13) || chr(10);
		end;
	end loop;
	close C03;
end if;
	
	
return ds_resultado_w;

end obter_resultado_microorganismo;
/