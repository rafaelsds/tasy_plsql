create or replace
function obter_orient_proc_tipo_classif(	nr_seq_procedimento_p		number,
				nr_seq_agenda_p			number)			
				return varchar2 is
			
ds_retorno_w			varchar2(4000);
ds_retorno_concat_w		varchar2(8000);

Cursor C01 is
	select 	nvl(d.ds_orientacao_pac, '')	
	from   	agenda_paciente a,
		proc_interno b,
		agenda c,
		proc_interno_pac_orient d
	where  	a.nr_seq_proc_interno = b.nr_sequencia
	and	a.cd_agenda = c.cd_agenda
	and	b.nr_sequencia = d.nr_seq_proc_interno
	and	a.nr_seq_tipo_classif_pac = d.nr_seq_tipo_classif_pac
	and	b.nr_sequencia = nr_seq_procedimento_p
	and	a.nr_sequencia = nr_seq_agenda_p
	and	c.cd_tipo_agenda = '2';

begin
ds_retorno_w	:= '';

if 	(nr_seq_procedimento_p is not null) and
	(nr_seq_agenda_p is not null) then
	begin

	open C01;
	loop
	fetch C01 into	
		ds_retorno_w;
	exit when C01%notfound;
		begin
		
		if	(ds_retorno_w is not null) or
			(ds_retorno_w <> '')then
			ds_retorno_concat_w := ds_retorno_concat_w||' '||ds_retorno_w; 
		
		end if;
		
		end;
	end loop;
	close C01;
	
	if 	(ds_retorno_w is null) or
		(ds_retorno_w = '') then  
		begin
		select 	max(b.ds_orientacao_usuario)
		into	ds_retorno_concat_w
		from   	agenda_paciente a,
			proc_interno b,
			agenda c
		where  	a.nr_seq_proc_interno = b.nr_sequencia
		and	a.cd_agenda = c.cd_agenda
		and	a.nr_seq_proc_interno = nr_seq_procedimento_p
		and	a.nr_sequencia = nr_seq_agenda_p
		and	c.cd_tipo_agenda = '2';
		end;
	
	end if;   
	
	exception
	when others then
		ds_retorno_concat_w := '';
	
	end;
end if;

return	ds_retorno_concat_w;

end obter_orient_proc_tipo_classif;
/