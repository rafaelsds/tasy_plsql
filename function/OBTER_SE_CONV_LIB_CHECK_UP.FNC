create or replace
function obter_se_conv_lib_check_up(	
		cd_estabelecimento_p	number,
		cd_convenio_p		number,		
		cd_funcao_p		number,
		cd_perfil_p 		number,
		dt_referencia_p		date)
		return varchar2 is

ie_permite_w		varchar2(1);
ds_retorno_w		varchar2(01)	:= 'S';
qt_regra_w		number(10,0);
dt_referencia_w		date;		
qt_convenio_w		number(10,0);
			
cursor c01 is
	select	nvl(a.ie_permite_convenio,'S')
	from	conveio_estab_func_lib a
	where	a.cd_estabelecimento		= cd_estabelecimento_p
	and	a.cd_funcao			= cd_funcao_p
	and	a.cd_convenio			= cd_convenio_p
	and	((nvl(a.dt_inicio_regra,dt_referencia_w)		<= dt_referencia_w))
	and	nvl(a.cd_perfil, cd_perfil_p)	= cd_perfil_p
	order by nvl(a.cd_perfil,999);
			
begin

dt_referencia_w	:= nvl(dt_referencia_p,sysdate);

select	count(*)
into	qt_regra_w
from	conveio_estab_func_lib a
where	a.cd_estabelecimento		= cd_estabelecimento_p
and	a.cd_funcao			= cd_funcao_p
and	a.cd_convenio			= cd_convenio_p
and	((nvl(a.dt_inicio_regra,dt_referencia_w)		<= dt_referencia_w))
and 	rownum < 2;

if	(qt_regra_w	> 0) then
	open c01;
	loop
	fetch c01 into	
		ie_permite_w;
	exit when c01%notfound;
		begin
		ds_retorno_w	:= ie_permite_w;
		end;
	end loop;
	close c01;
end if;

return	ds_retorno_w;

end obter_se_conv_lib_check_up;
/
