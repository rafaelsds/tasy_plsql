create or replace
function obter_submotivo_alta_atend(nr_atendimento_p  number)
        return varchar2 is

nr_submotivo_alta_w  number(10);
ds_submotivo_alta_w  varchar2(180);
begin
if (nr_atendimento_p is not null) then
 select max(nr_submotivo_alta)
 into nr_submotivo_alta_w
 from  atendimento_paciente
 where nr_atendimento = nr_atendimento_p;

 if (nvl(nr_submotivo_alta_w,0) > 0) then
  ds_submotivo_alta_w := substr(obter_desc_submotivo_alta(nr_submotivo_alta_w),1,180);
 end if;

end if;

return ds_submotivo_alta_w;

end obter_submotivo_alta_atend;
/
