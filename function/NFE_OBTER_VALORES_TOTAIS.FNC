create or replace
function nfe_obter_valores_totais(nr_seq_nf_p		number,
				  ie_tipo_tributo_p	varchar2,
				  ie_tipo_soma_p	varchar2)
 		    	return varchar2 is
	
/* ALTERAR A FUNCTION NFE_OBTER_VALORES_TOTAIS_NUM */
			
ds_retorno_w	varchar2(255);			
vl_base_w	varchar2(255);
vl_tributo_w	varchar2(255);
ie_existe_trib_w	number(10);

begin

select	count(*)
into	ie_existe_trib_w
from	nota_fiscal_item_trib a,
	tributo d
where	a.cd_tributo = d.cd_tributo
and	a.nr_sequencia = nr_seq_nf_p
and 	d.ie_tipo_tributo = ie_tipo_tributo_p;

if	(ie_existe_trib_w > 0) then
	
	select  to_char(cast(sum(a.vl_base_calculo) as number(15,2)),'FM9999999990.0099'),
		to_char(cast(sum(a.vl_tributo) as number(15,2)),'FM9999999990.0099')
	into	vl_base_w,
		vl_tributo_w
	from	nota_fiscal_item_trib a,
		tributo d
	where	a.cd_tributo = d.cd_tributo
	and	a.nr_sequencia = nr_seq_nf_p
	and 	d.ie_tipo_tributo = ie_tipo_tributo_p;	
	
else
	
	vl_base_w := to_char(0,'FM9999999990.0099');
	vl_tributo_w := to_char(0,'FM9999999990.0099');	
	
end if;


if (ie_tipo_soma_p = 'BC') then
	ds_retorno_w := vl_base_w;
elsif (ie_tipo_soma_p = 'VL') then
	ds_retorno_w := vl_tributo_w;
end if;


return	ds_retorno_w;

end nfe_obter_valores_totais;
/
