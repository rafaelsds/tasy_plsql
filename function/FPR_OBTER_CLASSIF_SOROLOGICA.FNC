create or replace
function fpr_obter_classif_sorologica(	nr_seq_unid_dialise_p		number,
					cd_pessoa_fisica_p		varchar2,
					nr_linha_p			number)
 		    	return varchar2 is

ds_classificacao_w	varchar2(90);
			
begin
if	(nr_linha_p is not null) and ((nr_seq_unid_dialise_p is not null) or (cd_pessoa_fisica_p is not null)) then
	
	select	ds_classificacao
	into	ds_classificacao_w
		from(	select	ds_classificacao,
			rownum 	linha
			from	(select	y.ds_classificacao,
					x.dt_inicio
				from	hd_paciente_classif_sor x,
					hd_classificacao_sorologia y,
					hd_pac_renal_cronico       a
				where    y.nr_sequencia = x.nr_seq_classificacao
				and      a.cd_pessoa_fisica = x.cd_pessoa_fisica
				and	x.dt_inicio is not null
				and	x.dt_fim is null
				and      ((x.cd_pessoa_fisica = cd_pessoa_fisica_p) or (cd_pessoa_fisica_p = '0'))
				and	((a.nr_seq_unid_dialise = nr_seq_unid_dialise_p) or (nr_seq_unid_dialise_p = '0'))
				order by	x.dt_inicio desc))
	where	linha = nr_linha_p;
end if;
return	ds_classificacao_w;

end fpr_obter_classif_sorologica;
/
