create or replace 
function ctb_obter_valor_balancete( 	dt_referencia_p		date,
					cd_conta_contabil_p	varchar2,
					ie_opcao_p		varchar2,
					cd_empresa_p		number) 
		return Number is

vl_retorno_w		ctb_balancete_v.vl_saldo%type;
nr_seq_mes_ref_w	ctb_mes_ref.nr_sequencia%type;

begin

select	max(a.nr_sequencia)
into	nr_seq_mes_ref_w
from	ctb_mes_ref a
where	trunc(a.dt_referencia,'month')	= dt_referencia_p
and	a.cd_empresa = cd_empresa_p;

if (nr_seq_mes_ref_w is not null) then
	
	if (ie_opcao_p = 'S') then
		select	nvl(sum(b.vl_saldo),0)
		into	vl_retorno_w
		from	ctb_balancete_v b
		where	b.nr_seq_mes_ref 	= nr_seq_mes_ref_w
		and	b.cd_conta_contabil	= cd_conta_contabil_p
		and	ie_normal_encerramento	<> 'E';
	elsif (ie_opcao_p = 'C') then
		select	nvl(sum(b.vl_credito),0)
		into	vl_retorno_w
		from	ctb_balancete_v b
		where	b.nr_seq_mes_ref 	= nr_seq_mes_ref_w
		and	b.cd_conta_contabil	= cd_conta_contabil_p
		and	ie_normal_encerramento	<> 'E';
	elsif (ie_opcao_p = 'D') then
		select	nvl(sum(b.vl_debito),0)
		into	vl_retorno_w
		from	ctb_balancete_v b
		where	b.nr_seq_mes_ref 	= nr_seq_mes_ref_w
		and	b.cd_conta_contabil	= cd_conta_contabil_p
		and	ie_normal_encerramento	<> 'E';
	elsif (ie_opcao_p = 'M') then
		select	nvl(sum(b.vl_movimento),0)
		into	vl_retorno_w
		from	ctb_balancete_v b
		where	b.nr_seq_mes_ref 	= nr_seq_mes_ref_w
		and	b.cd_conta_contabil	= cd_conta_contabil_p
		and	ie_normal_encerramento	<> 'E';	
	end if;
	
else
	vl_retorno_w := 0;
end if;

return nvl(vl_retorno_w,0);
end ctb_obter_valor_balancete;
/