create or replace 
FUNCTION Obter_UM_Habito_Vicio(nr_sequencia_p	Number)
			return varchar2 is

ds_retorno_w		Varchar2(100);

BEGIN

if	(nr_sequencia_p is not null) then
	select	ds_unidade_medida
	into	ds_retorno_w
	from	unid_med_habito_vicio
	where	nr_sequencia	= nr_sequencia_p;
end if;

return ds_retorno_w;

END Obter_UM_Habito_Vicio;
/
