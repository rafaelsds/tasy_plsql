create or replace
function cpoe_obter_ds_item_reval_event	(nr_seq_dialysis_p number,
									nr_seq_diet_p number,
									nr_seq_exam_p number,
									nr_seq_hemotherapy_p number,
									nr_seq_material_p number,
									nr_seq_recomendation_p number,
									nr_seq_gasoterapia_p number)
				return varchar2 is

ds_item_w		varchar2(255);
ie_tipo_dieta_w	cpoe_dieta.ie_tipo_dieta%type;

begin


if	(nr_seq_material_p is not null) then
	select	max(substr(obter_desc_material(cd_material),1,255))
	into	ds_item_w
	from	cpoe_material
	where	nr_sequencia	= nr_seq_material_p;
elsif	(nr_seq_exam_p is not null) then
	select	max(substr(obter_desc_proc_interno(nr_seq_proc_interno),1,255))
	into	ds_item_w
	from	cpoe_procedimento
	where	nr_sequencia	= nr_seq_exam_p;
elsif (nr_seq_diet_p is not null) then
	select 	max(ie_tipo_dieta)
	into 	ie_tipo_dieta_w
	from	cpoe_dieta
	where	nr_sequencia = nr_seq_diet_p;

	if	(ie_tipo_dieta_w in ('D', 'O')) then
		select	max(substr(obter_desc_dieta(cd_dieta),1,255))
		into	ds_item_w
		from	cpoe_dieta
		where	nr_sequencia = nr_seq_diet_p;
	elsif	(ie_tipo_dieta_w in ('SNE','S', 'E')) then
		select	max(substr(obter_desc_material(cd_material),1,255))
		into	ds_item_w
		from	cpoe_dieta
		where	nr_sequencia = nr_seq_diet_p;
	elsif	(ie_tipo_dieta_w = 'J') then
		select	max(substr(obter_desc_tipo_jejum(nr_seq_tipo),1,255))
		into	ds_item_w
		from	cpoe_dieta
		where	nr_sequencia = nr_seq_diet_p;
	elsif	(ie_tipo_dieta_w in ('LD', 'L')) then
		select	max(substr(obter_desc_material(cd_mat_prod1),1,255))
		into	ds_item_w
		from	cpoe_dieta
		where	nr_sequencia = nr_seq_diet_p;
	elsif (ie_tipo_dieta_w = 'P')then 
		ds_item_w := obter_desc_expressao(305331);
	elsif (ie_tipo_dieta_w = 'I') then
		ds_item_w := obter_desc_expressao(722485);
	end if;
elsif	(nr_seq_dialysis_p is not null) then
	select	max(substr(cpoe_obter_dialise_rel(ie_tipo_dialise, ie_hemodialise,ie_tipo_peritoneal),1,255))
	into	ds_item_w
	from	cpoe_dialise
	where	nr_sequencia	= nr_seq_dialysis_p;
elsif	(nr_seq_gasoterapia_p is not null) then
	select	max(substr(obter_desc_gas(nr_seq_gas),1,255))
	into	ds_item_w
	from	cpoe_gasoterapia
	where	nr_sequencia	= nr_seq_gasoterapia_p;
elsif	(nr_seq_recomendation_p is not null) then
	select	max(substr(obter_desc_recomendacao(cd_recomendacao),1,255))
	into	ds_item_w
	from	cpoe_recomendacao
	where	nr_sequencia	= nr_seq_recomendation_p;
elsif	(nr_seq_hemotherapy_p is not null) then
	ds_item_w := substr(cpoe_obter_desc_info_hemote(nr_seq_hemotherapy_p),1,255);
end if;

return trim(ds_item_w);

end cpoe_obter_ds_item_reval_event;
/