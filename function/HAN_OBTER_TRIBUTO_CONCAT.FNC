create or replace 
function HAN_obter_tributo_concat(nr_seq_nota_p	number) return varchar2 is			

ds_tributo_w	tributo.ds_tributo%type;
vl_tributo_w	nota_fiscal_trib.vl_tributo%type;
ds_retorno_w	varchar2(4000);

cursor C01 is		
select	substr(obter_descricao_padrao('TRIBUTO', 'DS_TRIBUTO', CD_TRIBUTO), 1, 200),
	t.vl_tributo
from	nota_fiscal_trib t,
	nota_fiscal n
where	n.nr_sequencia = t.nr_sequencia(+)
and	n.nr_sequencia = nr_seq_nota_p;

begin
open C01;
loop
fetch C01 into
	ds_tributo_w,
	vl_tributo_w;
exit when C01%notfound;
	begin
	ds_retorno_w	:= ds_retorno_w || ds_tributo_w || ' ' || 'R$ ' || '; ' || vl_tributo_w;
	end;
end loop;
close C01;

return ds_retorno_w;

end HAN_obter_tributo_concat;
/	