create or replace
function obter_proximo_evento_fanep(	nr_seq_pepo_p		number)
					return number is

nr_seq_exec_max_registrado_w	number(10,0);
nr_seq_exec_min_disponivel_w	number(10,0);
nr_sequencia_proximo_evento_w	number(10,0);
cd_perfil_ativo_w				number(5,0);
cd_estabelecimento_w			number(10) := wheb_usuario_pck.get_cd_estabelecimento;

					
begin
cd_perfil_ativo_w := wheb_usuario_pck.get_cd_perfil;

select	max(a.nr_seq_execucao)
into	nr_seq_exec_max_registrado_w
from	evento_cirurgia a,
	evento_cirurgia_paciente b
where	b.nr_seq_evento = a.nr_sequencia
and	b.nr_seq_pepo 	= nr_seq_pepo_p
and	nvl(b.ie_situacao,'A') = 'A';

select	min(nr_seq_execucao)
into	nr_seq_exec_min_disponivel_w
from	evento_cirurgia
where	substr(obter_se_mostra_evento(null,nr_seq_pepo_p,nr_sequencia, cd_perfil_ativo_w),1,1) = 'S'
and	ie_situacao = 'A'
and 	NVL(ie_exibicao, 'A') IN ('A', 'F')
and	((cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento is null))
and	(nr_seq_exec_max_registrado_w is null or nr_seq_execucao > nr_seq_exec_max_registrado_w);
	
if	(nr_seq_exec_min_disponivel_w is not null) then
	select	max(a.nr_sequencia)
	into	nr_sequencia_proximo_evento_w
	from	evento_cirurgia a
	where	a.nr_seq_execucao = nr_seq_exec_min_disponivel_w
	and 	NVL(ie_exibicao, 'A') IN ('A', 'F');
end if;

return nvl(nr_sequencia_proximo_evento_w,0);

end obter_proximo_evento_fanep;
/
