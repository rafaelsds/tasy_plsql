create or replace
function pls_oter_cd_coop_benef(
		nr_sequencia_p	number)
 		    	return varchar2 is
			
cd_cooperativa_w	varchar2(10);

begin
if	(nr_sequencia_p is not null) then
	begin
	select	cd_cooperativa
	into	cd_cooperativa_w
	from	pls_congenere	a
	where	nr_sequencia	= nr_sequencia_p;
	end;
end if;
return	cd_cooperativa_w;

end pls_oter_cd_coop_benef;
/