create or replace
function Obter_se_obriga_clearance( nr_prescricao_p		number,
									ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(4000);
ds_material_w		varchar2(250);

--ie_opcao = O
--ie_opcao = Q

Cursor C01 is
	select	distinct substr(b.ds_material,1,240)
	from	prescr_material a,
			material b
	where	a.nr_prescricao = nr_prescricao_p
	and		a.cd_material = b.cd_material
	and		nvl(b.ie_clearance, 'N') = 'S';

begin
if	(ie_opcao_p = 'O') then

	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	prescr_material a,
			material b
	where	rownum = 1
	and		a.cd_material = b.cd_material
	and		a.nr_prescricao = nr_prescricao_p
	and		nvl(b.ie_clearance,'N') = 'S';

	if	(ds_retorno_w = 'S') then

		select	nvl(max('N'),'S')
		into	ds_retorno_w
		from	prescr_medica a,
				pac_clereance_creatinina b
		where	rownum = 1
		and		a.cd_pessoa_fisica = b.cd_pessoa_fisica
		and		a.nr_prescricao = nr_prescricao_p;

	end if;

elsif	(ie_opcao_p = 'Q') then

	open C01;
	loop
	fetch C01 into	
		ds_material_w;
	exit when C01%notfound;
		begin
		ds_retorno_w := substr(ds_retorno_w ||chr(10)||' - '||ds_material_w,1,4000);
		end;
	end loop;
	close C01;

end if;	

return	ds_retorno_w;

end Obter_se_obriga_clearance;
/