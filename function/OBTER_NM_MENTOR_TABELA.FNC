create or replace function obter_nm_mentor_tabela (nm_tabela_origem_p varchar2, nr_seq_origem_p number default null) return varchar2 is 
	nm_campo_mentor_w varchar2(100);
	nm_campo_mentor_escala_w varchar2(100);

begin
	if (nm_tabela_origem_p is null) then
		case nr_seq_origem_p
			when 	1	then	nm_campo_mentor_w := obter_desc_expressao(490529);
			when	2	then	nm_campo_mentor_w := obter_desc_expressao(618778);
			when 	3	then	nm_campo_mentor_w := obter_desc_expressao(948116);
			when	4	then	nm_campo_mentor_w := obter_desc_expressao(316083);
			when 	5	then	nm_campo_mentor_w := obter_desc_expressao(490530);	
			when	6	then	nm_campo_mentor_w := obter_desc_expressao(722313);
			when 	7	then	nm_campo_mentor_w := obter_desc_expressao(317505);
			else	nm_campo_mentor_w := null;
		end case;
	else	
		case nm_tabela_origem_p
			when	'ATENDIMENTO_SINAL_VITAL' 	then nm_campo_mentor_w := obter_desc_expressao(844337);
			when	'DIAGNOSTICO_DOENCA'		then nm_campo_mentor_w := obter_desc_expressao(287694);
			when	'QUA_EVENTO_PACIENTE'		then nm_campo_mentor_w := obter_desc_expressao(849599);
			when	'EXAME_LAB_RESULT_ITEM' 	then nm_campo_mentor_w := obter_desc_expressao(323606);
			when	'MAN_CLASSIF_RISCO'			then nm_campo_mentor_w := obter_desc_expressao(628009);
			when	'ESCALA_EIF'				then nm_campo_mentor_w := obter_desc_expressao(820907);
			when	'ESCALA_EIF_II'				then nm_campo_mentor_w := obter_desc_expressao(820909);
			else	
				select DS_INFORMACAO 
				into nm_campo_mentor_escala_w
				from vice_escala 
				where nm_tabela = nm_tabela_origem_p 
				and ie_tipo_atributo = 'NUMBER';
				nm_campo_mentor_w := nm_campo_mentor_escala_w;
		end case;
	end if;	
	return nm_campo_mentor_w;
end obter_nm_mentor_tabela;
/