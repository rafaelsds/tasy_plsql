create or replace
function obter_lote_validade_item_oc(
				nr_ordem_compra_p	number,
				nr_item_oci_p		number,
				ie_opcao_p		varchar2)
		return varchar2 is


ie_retorno_w		varchar2(20);
ds_lote_fornec_w	varchar2(20);
dt_validade_w		Date;
nr_sequencia_w	number(10);

/*ie_opcao_p
	L = Lote (Retorna o numero do lote fornecedor
	D = Data Validade (Retorna a data de validade do lote*/


begin

select	nvl(max(nr_seq_lote_fornec), 0)
into	nr_sequencia_w
from	movimento_estoque
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p;

if	(nr_sequencia_w > 0) then
	select	ds_lote_fornec,
		dt_validade
	into	ds_lote_fornec_w,
		dt_validade_w
	from	material_lote_fornec
	where	nr_sequencia = nr_sequencia_w;

	if	(ie_opcao_p = 'L') then
		ie_retorno_w	:= ds_lote_fornec_w;
	elsif	(ie_opcao_p = 'D') then
		ie_retorno_w	:= dt_validade_w;
	end if;
end if;

return ie_retorno_w;

end obter_lote_validade_item_oc;
/