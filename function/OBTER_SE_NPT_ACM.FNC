create or replace
function obter_se_npt_acm (nr_prescricao_p		number,
						   nr_seq_npt_p	number)
						   return varchar2 is

ie_acm_sn_w	varchar2(1) := 'N';
ie_acm_w	varchar2(1);
			
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_npt_p is not null) then
	begin
	select	nvl(max(ie_acm),'N')
	into	ie_acm_w
	from	nut_pac
	where	nr_prescricao	= nr_prescricao_p
	and		nr_sequencia	= nr_seq_npt_p;
	
	if	(ie_acm_w = 'S') then
		begin
		ie_acm_sn_w	:= 'S';
		end;
	end if;
	end;
end if;

return ie_acm_sn_w;

end obter_se_npt_acm;
/
