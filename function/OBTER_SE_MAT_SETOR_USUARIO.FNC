create or replace
function obter_se_mat_setor_usuario(cd_estabelecimento_p	number,
				cd_material_p			number,
				nm_usuario_p			varchar2)
 		    	return varchar2 is
			
cd_local_estoque_w	number(4);
ie_local_valido_w varchar2(1);
ie_liberado_ww	varchar2(1);

Cursor C01 is
	select	cd_local_estoque
	from	usuario_local_estoque_v
	where	upper(nm_usuario) like(upper(nm_usuario_p))
	and	cd_estabelecimento = cd_estabelecimento_p
	group by cd_local_estoque;
begin
ie_liberado_ww := 'N';
open C01;
loop
fetch C01 into	
	cd_local_estoque_w;
exit when (C01%notfound or ie_liberado_ww = 'S');
	begin
	OBTER_LOCAL_VALIDO(
		cd_estabelecimento_p,
		cd_local_estoque_w,
		cd_material_p,
		null,
		ie_local_valido_w);
	
	if	(ie_local_valido_w = 'S') then
		ie_liberado_ww := 'S';
	end if;
	end;
end loop;
close C01;

return	ie_liberado_ww;
end obter_se_mat_setor_usuario;
/