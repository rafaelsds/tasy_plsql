create or replace
function obter_desc_hd_locl_armaz	(	nr_sequencia_p	number)
 		    	return varchar2 is
ds_retorno_w	varchar2(255);
begin
ds_retorno_w := '';
if (nvl(nr_sequencia_p,0) > 0) then

	select	 nvl(max(ds_local),'')
	into	 ds_retorno_w
	from  	 hd_local_armazenamento
	where	 nr_sequencia = nr_sequencia_p;
	
end if;

return	ds_retorno_w;

end obter_desc_hd_locl_armaz;
/