create or replace
function ctb_pls_obter_estip_contrato
			(	nr_contrato_p		number)
				return varchar2 is

ds_retorno_w varchar2(255);

begin

ds_retorno_w := substr(pls_obter_dados_nr_contrato(nr_contrato_p, 'S'),1,255);

return	ds_retorno_w;

end ctb_pls_obter_estip_contrato;
/