create or replace function obter_data_prev_futura(	dt_inicial	date,
						qt_dias	number,
						ie_converte_mes varchar)
						Return date is 
						
mes_w number;
begin

mes_w := qt_dias/30;

if (ie_converte_mes = 'S') then
	return pkg_date_utils.ADD_MONTH(dt_inicial ,mes_w);
else
	return dt_inicial + qt_dias;
end if;			

end obter_data_prev_futura;
/