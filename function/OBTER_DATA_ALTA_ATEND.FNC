create or replace
FUNCTION Obter_data_alta_Atend(nr_atendimento_p NUMBER)
 			RETURN DATE IS

ds_retorno_w		DATE;

BEGIN
IF	(nr_atendimento_p IS NOT NULL) THEN
	SELECT	a.dt_alta
	INTO	ds_retorno_w
	FROM	atendimento_paciente a
	WHERE	a.nr_atendimento = nr_atendimento_p
	AND	a.dt_alta IS NOT NULL;	
END IF;

RETURN	ds_retorno_w;

END	Obter_data_alta_Atend;
/