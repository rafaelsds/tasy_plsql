create or replace
function Verifica_Setor_Exclusivo(	cd_procedimento_p		number,
				ie_origem_proced_p	number,
				cd_setor_atendimento_p	number,
				nr_seq_proc_interno_p	number,
				nm_usuario_p		varchar2)
				return number is

qt_retorno_w		number(10) := 1;
qt_proc_interno_w		number(10);
cd_setor_exclusivo_w	number(5);
qt_proc_setor_atend_w	number(10);

begin

Select 	count(*) 
into	qt_proc_interno_w
from	proc_interno_setor
where	nr_seq_proc_interno = nr_seq_proc_interno_p;

select	count(*)
into	qt_proc_setor_atend_w
from 	procedimento_setor_atend a
where 	a.cd_procedimento  = cd_procedimento_p
and 	a.ie_origem_proced = ie_origem_proced_p;

if 	(nr_seq_proc_interno_p is not null) and
	(qt_proc_interno_w > 0) then
	begin
	Select 	count(*)
	into	qt_retorno_w
	from   	proc_interno_setor
	where  	nr_seq_proc_interno 	= nr_seq_proc_interno_p
	and    	cd_setor_atendimento	= cd_setor_atendimento_p
	and	nvl(cd_perfil,obter_perfil_ativo) = obter_perfil_ativo;
	end;
else
	begin
	
	select	cd_setor_exclusivo 
	into	cd_setor_exclusivo_w
	from 	procedimento
	where 	cd_procedimento   =  cd_procedimento_p 
	and 	ie_origem_proced  =  ie_origem_proced_p;
	
	if (nvl(cd_setor_exclusivo_w,0) <> 0) then
		begin
		select	count(*)
		into	qt_retorno_w
		from 	usuario_setor_v b,
			procedimento a
		where 	a.cd_setor_exclusivo	= b.cd_setor_atendimento
		and 	b.nm_usuario         		=  nm_usuario_p
		and 	a.cd_procedimento		=  cd_procedimento_p
		and 	a.ie_origem_proced		=  ie_origem_proced_p
		and 	a.cd_setor_exclusivo	=  cd_setor_atendimento_p;
		end;
	elsif (nvl(qt_proc_setor_atend_w,0) > 0) then 
		begin
		select 	count(*)
		into	qt_retorno_w
		from 	usuario_setor_v b,
			procedimento_setor_atend a 
		where 	a.cd_setor_atendimento	= b.cd_setor_atendimento
		and 	b.nm_usuario		= nm_usuario_p
		and 	a.cd_procedimento    	= cd_procedimento_p
		and 	a.ie_origem_proced   	= ie_origem_proced_p
		and 	a.cd_setor_atendimento 	= cd_setor_atendimento_p;
		end;
	end if;
	end;
end if;

return	qt_retorno_w;

end Verifica_Setor_Exclusivo;
/