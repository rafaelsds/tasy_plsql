create or replace
function ptu_obter_se_benef_existe
			(	cd_usuario_plano_p	Varchar2,
				cd_estabelecimento_p	Number)
			return Varchar2 is

ds_retorno_w		Varchar2(2);
cd_pessoa_fisica_w	Varchar2(20);
nr_seq_segurado_w	Number(10);
			
begin
ds_retorno_w	:= 'N';

select  max(cd_pessoa_fisica),
	max(b.nr_sequencia)
into	cd_pessoa_fisica_w,
	nr_seq_segurado_w
from    pls_segurado            b,
	pls_segurado_carteira   a                 
where   a.nr_seq_segurado    = b.nr_sequencia    
and     a.cd_estabelecimento = cd_estabelecimento_p
and     a.cd_usuario_plano   = cd_usuario_plano_p;

if	(cd_pessoa_fisica_w	is not null) and (nr_seq_segurado_w	is not null) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end ptu_obter_se_benef_existe;
/