Create or Replace
FUNCTION OBTER_SE_TERCEIRO_REGRA_ESP(	nr_seq_regra_esp_p	number,
					nr_seq_terceiro_p	number)
					RETURN VarChar2 IS

ds_retorno_w		varchar2(1);
nr_seq_terceiro_w	number(10);
qt_terc_regra_w		number(10);

BEGIN

select	a.nr_seq_terceiro
into	nr_seq_terceiro_w
from	regra_esp_repasse a
where	a.nr_sequencia	= nr_seq_regra_esp_p;

ds_retorno_w	:= 'N';

if	(nr_seq_terceiro_w	is not null) and
	(nr_seq_terceiro_w	= nr_seq_terceiro_p) then
	ds_retorno_w	:= 'S';
else

	select	count(*)
	into	qt_terc_regra_w
	from	regra_esp_repasse_terc a
	where	a.nr_seq_regra_esp	= nr_seq_regra_esp_p;

	if	(qt_terc_regra_w	= 0) then

		if	(nr_seq_terceiro_w	is null) then
			ds_retorno_w	:= 'S';
		end if;

	else

		select	nvl(max('S'),'N')
		into	ds_retorno_w
		from	regra_esp_repasse_terc a
		where	a.nr_seq_terceiro	= nr_seq_terceiro_p
		and	a.nr_seq_regra_esp	= nr_seq_regra_esp_p;

	end if;

end if;

RETURN	ds_retorno_w;

END OBTER_SE_TERCEIRO_REGRA_ESP;
/