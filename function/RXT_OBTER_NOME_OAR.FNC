create or replace
function rxt_obter_nome_oar	(nr_seq_oar_p	number)
			return varchar2 is

ds_oar_w	varchar2(80);

begin
if	(nr_seq_oar_p is not null) then

	select	max(ds_orgao)
	into	ds_oar_w
	from	rxt_orgam_at_risk
	where	nr_sequencia = nr_seq_oar_p;

end if;

return ds_oar_w;

end rxt_obter_nome_oar;
/