create or replace
function Obter_Doc_Convenio_Proc
		(nr_interno_conta_p	number)
		return varchar2 is

nr_doc_convenio_w	varchar2(20);

begin

select	nvl(max(cd_autorizacao),'0')
into	nr_doc_convenio_w
from 	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

if	(nr_doc_convenio_w = '0') then
	select	max(nr_doc_convenio)
	into	nr_doc_convenio_w
	from	procedimento_paciente
	where	nr_interno_conta	= nr_interno_conta_p;
end if;

return	nr_doc_convenio_w;

end Obter_Doc_Convenio_Proc;
/