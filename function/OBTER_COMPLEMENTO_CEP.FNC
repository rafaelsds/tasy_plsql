create or replace
function obter_complemento_cep (
		cd_cep_p	varchar2)
		return varchar2 is
	
ds_complemento_w	varchar2(2000);
ie_cep_internet_w	varchar2(15);
nm_logradouro_w		varchar2(125);
nm_localidade_w		varchar2(60);
nm_bairro_w		varchar2(80);
sg_estado_w		valor_dominio.vl_dominio%type;
cd_cep_w		number(9,0);
cd_municipio_ibge_w	varchar2(6);
cd_cep_p_w		number(15,0);

cursor c01 is
select 	a.nm_logradouro, 
	a.nm_localidade, 
	a.nm_bairro, 
	a.cd_unidade_federacao, 
	a.nr_logradouro 
from 	cep_logradouro a  
where 	a.cd_logradouro = cd_cep_p_w
order by
	a.dt_atualizacao;
	
cursor c02 is
select 	a.nm_logradouro, 
	b.nm_localidade,
	c.ds_bairro,
	a.ds_uf,
	a.cd_cep
from 	cep_loc b,
	cep_bairro c,
	cep_log a
where  	b.nr_sequencia      	= c.nr_seq_loc 
and   	a.cd_bairro_inicial	= c.nr_sequencia 
and   	b.nr_sequencia      	= a.nr_seq_loc 
and   	a.cd_cep            	= cd_cep_p_w
order by
	a.dt_atualizacao;
	
cursor c03 is
select 	null, 
	b.nm_localidade, 
	null, 
	b.ds_uf,
	b.cd_cep
from   	cep_loc b
where  	b.cd_cep = cd_cep_p_w
order by
	b.dt_atualizacao;
		
begin
/*if	(cd_cep_p is not null) then
	begin
	cd_cep_p_w := somente_numero(cd_cep_p);
	
	Obter_Param_Usuario(0, 25, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_cep_internet_w);
	
	if	(ie_cep_internet_w = 'N') then
		begin
		open c01;
		loop
		fetch c01 into	nm_logradouro_w,
				nm_localidade_w,
				nm_bairro_w,
				sg_estado_w,
				cd_cep_w;
		exit when c01%notfound;
			begin
			nm_logradouro_w	:= nm_logradouro_w;
			nm_localidade_w	:= nm_localidade_w;
			nm_bairro_w	:= nm_bairro_w;
			sg_estado_w	:= sg_estado_w;
			cd_cep_w	:= cd_cep_w;
			end;
		end loop;
		close c01;
		end;
	else
		begin
		open c02;
		loop
		fetch c02 into	nm_logradouro_w,
				nm_localidade_w,
				nm_bairro_w,
				sg_estado_w,
				cd_cep_w;
		exit when c02%notfound;
			begin
			nm_logradouro_w	:= nm_logradouro_w;
			nm_localidade_w	:= nm_localidade_w;
			nm_bairro_w	:= nm_bairro_w;
			sg_estado_w	:= sg_estado_w;
			cd_cep_w	:= cd_cep_w;
			end;
		end loop;
		close c02;
		end;
	end if;
	
	if	(cd_cep_w is null) then
		begin
		open c03;
		loop
		fetch c03 into	nm_logradouro_w,
				nm_localidade_w,
				nm_bairro_w,
				sg_estado_w,
				cd_cep_w;
		exit when c03%notfound;
			begin
			nm_logradouro_w	:= nm_logradouro_w;
			nm_localidade_w	:= nm_localidade_w;
			nm_bairro_w	:= nm_bairro_w;
			sg_estado_w	:= sg_estado_w;
			cd_cep_w	:= cd_cep_w;
			end;
		end loop;
		close c03;
		end;
	end if;
	
	cd_municipio_ibge_w 	:= substr(obter_municipio_ibge(cd_cep_p_w),1,6);
	
	ds_complemento_w	:= nm_localidade_w || ';' || sg_estado_w || ';' || nm_logradouro_w || ';' || nm_bairro_w || ';' || cd_municipio_ibge_w;
	end;
end if;
return ds_complemento_w;*/
return null;
end obter_complemento_cep;
/