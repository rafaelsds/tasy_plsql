create or replace
function obter_dados_param_int_compras(
			cd_estabelecimento_p	number,
			ie_tipo_p			varchar2)
	return varchar2 is

nm_projeto_w		varchar2(80);
nm_empresa_w		varchar2(80);
nm_org_compra_w		varchar2(80);
nm_centro_w		varchar2(80);
ds_local_entrega_w		varchar2(80);

ds_retorno_w		varchar2(80);


begin

select	nvl(max(nm_projeto_borg), 'HSL'),
	nvl(max(nm_empresa_borg), 'HSL'),
	nvl(max(nm_org_compra_borg), 'HSL'),
	nvl(max(nm_centro_borg), 'HSL'),
	nvl(max(ds_local_entrega), 'HSL01')
into	nm_projeto_w,
	nm_empresa_w,
	nm_org_compra_w,
	nm_centro_w,
	ds_local_entrega_w
from	parametro_int_compras
where	cd_estabelecimento	= cd_estabelecimento_p;



if	(ie_tipo_p = 'P') then
	ds_retorno_w := nm_projeto_w;
elsif	(ie_tipo_p = 'E') then
	ds_retorno_w := nm_empresa_w;
elsif	(ie_tipo_p = 'O') then
	ds_retorno_w := nm_org_compra_w;
elsif	(ie_tipo_p = 'C') then
	ds_retorno_w := nm_centro_w;
elsif	(ie_tipo_p = 'LE') then
	ds_retorno_w := ds_local_entrega_w;
end if;

return ds_retorno_w;

end obter_dados_param_int_compras;
/