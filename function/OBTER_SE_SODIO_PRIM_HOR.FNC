create or replace
function obter_se_sodio_prim_hor(
					nr_seq_perfil_sodio_p		number) return varchar2 is

ie_retorno_w		varchar2(15);					
begin
	select	nvl(max(ie_primeira_hora), 'N')
	into	ie_retorno_w
	from	hd_perfil_sodio
	where	nr_sequencia = nr_seq_perfil_sodio_p;
	
return	ie_retorno_w;

end obter_se_sodio_prim_hor;
/