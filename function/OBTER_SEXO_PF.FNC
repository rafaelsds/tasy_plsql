create or replace
function Obter_Sexo_PF(	cd_pessoa_fisica_p	Varchar2,
			ie_opcao_p		Varchar2)
			return Varchar2 is

ie_sexo_w	Varchar2(1);
ds_retorno_w	Varchar2(100);

begin

-- ie_opcao_p 'C' C�digo  'D' Descri��o

select	nvl(max(ie_sexo),'X')
into	ie_sexo_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(ie_sexo_w <> 'X') then
	begin

	if	(ie_opcao_p = 'C') then
		ds_retorno_w	:= ie_sexo_w;
	else
		select	substr(nvl(ds_valor_dominio_cliente, obter_desc_expressao(cd_exp_valor_dominio, ds_valor_dominio)), 1, 255)
		into	ds_retorno_w
		from	valor_dominio
		where	cd_dominio	= 4
		and	vl_dominio	= ie_sexo_w;
	end if;
	
	end;
end if;

return ds_retorno_w;

end Obter_Sexo_PF;
/
