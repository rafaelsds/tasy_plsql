create or replace
function TX_Obter_Dados_Laboratorio (	nr_seq_laboratorio_p	number,
					ie_opcao_p		varchar2)
					return			varchar2 is

cd_cgc_w		varchar2(14);
ds_laboratorio_w	varchar2(80);
ds_retorno_w		varchar2(255);
	
begin

select	cd_cgc,
	substr(obter_dados_pf_pj('',cd_cgc, 'N'),1,80)
into	cd_cgc_w,
	ds_laboratorio_w
from	tx_laboratorio
where	nr_sequencia	= nr_seq_laboratorio_p;

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= cd_cgc_w;
elsif	(ie_opcao_p = 'R') then
	ds_retorno_w	:= ds_laboratorio_w;
end if;

return	ds_retorno_w;
	
end TX_Obter_Dados_Laboratorio;
/