Create or Replace
Function obter_material_baixa_estoq_pac (	cd_estabelecimento_p		number,
						cd_estabelecimento_base_p	number,
						cd_material_p			number) 
return Varchar is

ds_retorno_w			Varchar2(1);
cd_estabelecimento_base_w	Number(5);
qt_reg_w			Number(3);

BEGIN

select	count(*)
into	qt_reg_w
from	material_estab
where	cd_material = cd_material_p
and	cd_estabelecimento = cd_estabelecimento_p;

if	(qt_reg_w > 0) then
	select	max(ie_baixa_estoq_pac)
	into	ds_retorno_w
	from	material_estab
	where	cd_material = cd_material_p
	and	cd_estabelecimento = cd_estabelecimento_p;
else
	select	max(ie_baixa_estoq_pac)
	into	ds_retorno_w
	from	material
	where	cd_material = cd_material_p;
end if;

RETURN ds_retorno_w;
END obter_material_baixa_estoq_pac;
/