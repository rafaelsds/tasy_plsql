create or replace
function Obter_se_suspen_presc_set(cd_setor_atendimento_p	Number)
 		    	return varchar2 is
--Informar se o setor pode suspender prescricoes ao fazer a transferencias de setor
			
ie_suspende_pres_w	varchar2(1) := null;	
		

begin

if (cd_setor_atendimento_p is not null) then
	select nvl(max(ie_susp_rep_transf),'N')
	into   ie_suspende_pres_w 
	from   setor_atendimento
	where  cd_setor_atendimento = cd_setor_atendimento_p;
end if;


return	ie_suspende_pres_w;

end Obter_se_suspen_presc_set;
/