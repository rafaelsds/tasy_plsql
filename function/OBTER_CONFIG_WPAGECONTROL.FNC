create or replace
function obter_config_wpagecontrol(	nr_seq_objeto_p		number,
					nm_usuario_p		varchar2,
					cd_perfil_p		number)
 		    	return varchar2 is

nm_usuario_ativo_w	varchar2(255);
cd_perfil_ativo_w	number(10);
ds_retorno_w		varchar2(255);

Cursor C01 is
	select	ds_titulo || ';' ||
		ie_visible
	from	config_wpage_control
	where	nr_seq_objeto = nr_seq_objeto_p
	and	nvl(nm_usuario_regra, nm_usuario_ativo_w) = nm_usuario_ativo_w
	and	nvl(cd_perfil, cd_perfil_ativo_w) = cd_perfil_ativo_w
	order by
		nvl(nm_usuario_regra,'AAA'),
		nvl(cd_perfil,0);
begin

nm_usuario_ativo_w	:= nvl(nm_usuario_p, nvl(obter_usuario_ativo,'AAA'));
cd_perfil_ativo_w	:= nvl(cd_perfil_p, nvl(obter_perfil_ativo,0));

open C01;
loop
fetch C01 into	
	ds_retorno_w;
exit when C01%notfound;
	begin
	null;
	end;
end loop;
close C01;

return	ds_retorno_w;

end obter_config_wpagecontrol;
/