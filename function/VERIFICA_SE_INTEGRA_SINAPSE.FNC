create or replace
function verifica_se_integra_sinapse( nr_prescricao_p	varchar2)
				return varchar2 is
ds_retorno_w	varchar2(1);


begin
SELECT	decode(count(*),0,'N','S')
into 	ds_retorno_w 
FROM 	pessoa_fisica a,  
		atendimento_paciente b,
		prescr_medica  c, 
		prescr_procedimento d,
	    proc_interno e,          
	    proc_interno_classif f          	       
WHERE 	a.cd_pessoa_fisica    = b.cd_pessoa_fisica  
AND     b.nr_atendimento      = c.nr_atendimento
AND     c.nr_prescricao       = d.nr_prescricao
AND 	d.nr_seq_proc_interno = e.nr_sequencia
AND     e.nr_seq_classif      = f.nr_sequencia
AND     UPPER(f.ds_sigla) IN ('CR','CT','MG','DX','MR','US','XA','EC','NM','PT','ES','TE','OT','ECG')  
AND     d.nr_prescricao = nr_prescricao_p;

return	ds_retorno_w;

end verifica_se_integra_sinapse;
/