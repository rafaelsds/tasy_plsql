create or replace
function obter_desc_agenda_GS(cd_agenda_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar(255);
		
begin

if	(obter_tipo_agenda(cd_agenda_p) = 3) then

	select	substr(obter_nome_medico_combo_agcons(cd_estabelecimento, cd_agenda, cd_tipo_agenda, nvl(ie_ordenacao,'N')),1,255)
	into	ds_retorno_w
	from	agenda
	where	cd_agenda	= cd_agenda_p;
elsif	(obter_tipo_agenda(cd_agenda_p) = 4) then
	select	nvl(ds_curta, ds_agenda)
	into	ds_retorno_w
	from	agenda
	where	cd_agenda = cd_agenda_p;
else	
	select	ds_agenda
	into	ds_retorno_w
	from	agenda
	where	cd_agenda = cd_agenda_p;
end if;

return	ds_retorno_w;

end obter_desc_agenda_GS;
/
