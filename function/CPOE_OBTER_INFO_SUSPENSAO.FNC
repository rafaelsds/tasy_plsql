create or replace	
function cpoe_obter_info_suspensao (nr_sequencia_p number,
								   ie_tipo_item_p varchar2,
								   ie_informacao_out_p varchar2)
								 return varchar2 is

dt_suspensao_w		cpoe_material.dt_suspensao%type;				
nm_usuario_susp_w	usuario.ds_usuario%type;

/*
ie_informacao_out_p
D  = Data suspens�o
N  = nome usu�rio suspens�o
*/


begin

if	(nvl(nr_sequencia_p,0) > 0) then
	
	if (ie_tipo_item_p = 'N') then
		
		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_dieta
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;
	
	elsif (ie_tipo_item_p = 'M') then
	
		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_material
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;	
	
	elsif (ie_tipo_item_p = 'P') then

		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_procedimento
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;	
		
	elsif (ie_tipo_item_p = 'G') then
	
		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_gasoterapia
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;		
	
	elsif (ie_tipo_item_p = 'R') then

		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_recomendacao
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;		
	
	elsif (ie_tipo_item_p = 'D') then
	
		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_dialise
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;			
	
	elsif (ie_tipo_item_p = 'H') then
	
		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_hemoterapia
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;			
	
	elsif (ie_tipo_item_p = 'I') then
	
		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_intervencao
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;
		
	elsif (ie_tipo_item_p = 'AP') then
	
		select	max(dt_suspensao),
				max(obter_nome_usuario(nm_usuario_susp))
		into	dt_suspensao_w,
				nm_usuario_susp_w
		from	cpoe_anatomia_patologica
		where	dt_lib_suspensao is not null
		and nr_sequencia = nr_sequencia_p;	
		
	end if;
end if;

if	(ie_informacao_out_p = 'D') then
	return	nvl(to_char(dt_suspensao_w,'dd/mm/yyyy hh24:mi:ss'),null);
else
	return	nvl(nm_usuario_susp_w,null);
end if;

end cpoe_obter_info_suspensao;
/
