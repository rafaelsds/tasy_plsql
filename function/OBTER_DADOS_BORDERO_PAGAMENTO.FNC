create or replace
function OBTER_DADOS_BORDERO_PAGAMENTO(	nr_bordero_p	number,
						ie_opcao_p	varchar2)
								return varchar2 is


/*

ie_opcao_p
C -> cheque do bordero + cheque bordero titulo
TP -> TIpo de baixa do bordero
*/

nr_cheque_w	varchar2(20);
ds_resultado_w	varchar2(255);
cd_tipo_baixa_w		bordero_pagamento.cd_tipo_baixa%type;


Cursor c01 is
select	decode(a.nr_cheque,null,'',a.nr_cheque || ', ')
from 	cheque b,
	CHEQUE_BORDERO_TITULO a
WHERE	a.nr_seq_cheque	= b.nr_sequencia
and	b.dt_cancelamento	is null
and	a.nr_bordero	= nr_bordero_p;

begin

select	max(nr_cheque),
		max(cd_tipo_baixa)
into	nr_cheque_w,
		cd_tipo_baixa_w
from	bordero_pagamento
where	nr_bordero	= nr_bordero_p;

if	(ie_opcao_p = 'C') then

	ds_resultado_W	:= nr_cheque_w;

	nr_cheque_w	:= '';

	open c01;
	loop
	fetch c01 into
	        nr_cheque_w;
	exit when c01%notfound;

        	ds_resultado_w	:= substr(ds_resultado_w || nr_cheque_w,1,254);

	end loop;
	close c01;
	
elsif (ie_opcao_p = 'TP') then	
	ds_resultado_w :=	to_char(cd_tipo_baixa_w); 
end if;

return  ds_resultado_w;

end OBTER_DADOS_BORDERO_PAGAMENTO;
/