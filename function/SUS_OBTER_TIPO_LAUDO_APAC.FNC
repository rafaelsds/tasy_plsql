create or replace
function sus_obter_tipo_laudo_apac (	cd_procedimento_solic_p	number,
				ie_origem_proced_p	number)
 		    		return number is
				
ie_tipo_laudo_retorno_w		number(10);				
ie_tipo_laudo_apac_w		varchar2(10);				

begin

begin
select	nvl(max(ie_tipo_laudo_apac),'00')
into	ie_tipo_laudo_apac_w
from	sus_procedimento
where	cd_procedimento	= cd_procedimento_solic_p
and	ie_origem_proced	= ie_origem_proced_p;
exception
when others then
	ie_tipo_laudo_apac_w := '00';
end;

case ie_tipo_laudo_apac_w 
	when '00' then
		if	(cd_procedimento_solic_p in (505010097,505010100,505010119)) then
			ie_tipo_laudo_retorno_w := 13;
		elsif	(cd_procedimento_solic_p = 506010023) then
			ie_tipo_laudo_retorno_w := 14;
		elsif	(cd_procedimento_solic_p = 506010015) then
			ie_tipo_laudo_retorno_w := 15;
		elsif	(cd_procedimento_solic_p in (506010040,506010058)) then
			ie_tipo_laudo_retorno_w := 16;
		elsif	(cd_procedimento_solic_p = 0506010031) then
			ie_tipo_laudo_retorno_w := 17;
		else
			ie_tipo_laudo_retorno_w := null;
		end if;			 
	when '01' then --Medicamentos
		ie_tipo_laudo_retorno_w := 12; --Medicamentos
	when '02' then --Nefrologia
		ie_tipo_laudo_retorno_w := 8; --Hemodiálise
	when '03' then --Oncologia - Quimioterapia
		if	(upper(substr(obter_descricao_procedimento(cd_procedimento_solic_p,ie_origem_proced_p),1,8)) = 'HORMONIO') then
			ie_tipo_laudo_retorno_w := 4;--Hormonioterapia
		else
			ie_tipo_laudo_retorno_w := 1;--Quimioterapia
		end if;			
	when '04' then --Oncologia - Radioterapia
		ie_tipo_laudo_retorno_w := 2; --Radioterapia	
	when '05' then 
		ie_tipo_laudo_retorno_w := null; 
	when '06' then --Medicamentos
		ie_tipo_laudo_retorno_w := 12; --Medicamentos	
	when '07' then 
		ie_tipo_laudo_retorno_w := null; 
end case;

return	ie_tipo_laudo_retorno_w;

end sus_obter_tipo_laudo_apac;
/
