create or replace
function obter_casas_diluicao	(cd_material_p	number,
				cd_setor_p	number)
 		    	return number is

nr_casas_diluicao_w	number(10,0);

begin

select	max(nr_casas_diluicao)
into	nr_casas_diluicao_w
from	rep_arredonda_diluicao
where	nvl(cd_material, cd_material_p) = cd_material_p
and	nvl(cd_setor_atendimento, nvl(cd_setor_p,0)) = nvl(cd_setor_p,0);

return	nr_casas_diluicao_w;

end obter_casas_diluicao;
/