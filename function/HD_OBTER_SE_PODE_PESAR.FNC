create or replace 
function HD_Obter_se_pode_pesar (cd_pessoa_fisica_p	varchar2)
			return		varchar2 is

ie_pode_pesar_w		varchar2(1);

begin

if	(cd_pessoa_fisica_p is not null) then

	select	nvl(max(ie_pode_pesar), 'S')
	into	ie_pode_pesar_w
	from	hd_pac_renal_cronico
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

end if;

return ie_pode_pesar_w;

end HD_Obter_se_pode_pesar;
/
