create or replace
function obter_ult_transferencia ( nr_seq_senha_p		number)
 		    	return varchar2 is

nm_usuario_w			varchar(255); 
nr_seq_mot_trasf_senha_w	number(10);
nr_fila_destino_w		number(10);
ds_motivo_w			varchar2(255);
ds_fila_w			varchar2(255);
retorno				varchar2(255) := '';	
		
begin

select	nm_usuario, 
	nr_seq_mot_trasf_senha,
	nr_fila_destino
into	nm_usuario_w, 
	nr_seq_mot_trasf_senha_w,
	nr_fila_destino_w
from	paciente_senha_transf_hist
where	nr_seq_senha = nr_seq_senha_p
and	nr_sequencia = (select max(nr_sequencia)
 				   from paciente_senha_transf_hist
 				   where nr_seq_senha = nr_seq_senha_p);

select	nvl(ds_curto, ds_fila)
into	ds_fila_w
from	fila_espera_senha
where	nr_sequencia = nr_fila_destino_w;
				   
if(nr_seq_mot_trasf_senha_w is not null) then
	select	ds_motivo
	into	ds_motivo_w
	from	motivo_transf_senha
	where	nr_sequencia = nr_seq_mot_trasf_senha_w;
		
	retorno := WHEB_MENSAGEM_PCK.get_texto(784301,'DS_FILA='|| ds_fila_w || ';' || 'NM_USUARIO='|| nm_usuario_w || ';' || 'DS_MOTIVO='|| ds_motivo_w );
--  Esta senha j� foi transferida para a fila #@DS_FILA#@ pelo usu�rio #@NM_USUARIO#@ com o motivo #@DS_MOTIVO#@

else
	retorno := WHEB_MENSAGEM_PCK.get_texto(784302,'DS_FILA=' || ds_fila_w || ';' ||'NM_USUARIO='|| nm_usuario_w );
-- Esta senha j� foi transferida para a fila #@DS_FILA#@ pelo usu�rio #@NM_USUARIO#@
end if;

return	retorno;

end obter_ult_transferencia;
/