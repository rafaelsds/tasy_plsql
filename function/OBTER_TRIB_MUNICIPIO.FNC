create or replace
function obter_trib_municipio(cd_tributo_p		number)	
					return varchar2 is

trib_municipo_w			varchar2(10);


begin
select	nvl(max(NR_TRIBUTACAO_MUNICIPIO),'')
into	trib_municipo_w
from	tributo
where	cd_tributo = cd_tributo_p;

return trib_municipo_w;

end obter_trib_municipio;
/
