create or replace
function Obter_desc_tipo_hist_saude(nr_seq_tipo_hist_p number,
									nr_seq_problema_p number default null)
 		    	return varchar2 is

ds_retorno_w	varchar2(80);
			
begin

if (nr_seq_tipo_hist_p is not null) then

	select	max(DS_TIPO_HISTORICO)	
	into	ds_retorno_w
	from	tipo_historico_saude b
	where	b.nr_sequencia = nr_seq_tipo_hist_p;
		
elsif (nr_seq_problema_p is not null) then
	select	max(ds) ds
	into	ds_retorno_w
	from (
		select	substr(obter_desc_problema(ie_tipo_registro, ie_tipo_problema, nm_tabela),1,200) ds 
		from	lista_problema_pac_item 
		where	nr_seq_problema = nr_seq_problema_p
		and	ie_tipo_registro = 'HS'
		order	by dt_atualizacao asc	
	) where	rownum <= 1;
end if;

return	ds_retorno_w;

end Obter_desc_tipo_hist_saude;
/
