create or replace
function pls_obter_dados_material(	nr_seq_material_p	pls_material.nr_sequencia%type,
					ie_opcao_p		varchar2,
					cd_material_p		material.cd_material%type default null) return  Varchar2 is

/*
ie_opcao_p
ANVISA	- Codigo anvisa
PA	- Principio ativo
DPA	-  Descricao do Principio Ativo
UM	-  Unidade de Medida
FB	-  Nr Fabricante
CO	- Codigo ops
DS	- Descricao material
NC	- Nome comercial
TISS	- Versao TISS do material
DE	- Data exclusao
SEQ	- Sequencia
COD	- Codigo
EST	- Estrutura do material
LABT	- Laboratorio TUSS
NT	- Nome tecnico
*/

ds_retorno_w		varchar2(255);
nr_seq_material_w	pls_material.nr_sequencia%type;

begin
-- se for passado a sequencia
if	(nr_seq_material_p is not null) then

	nr_seq_material_w := nr_seq_material_p;

-- se for passado o codigo tenta encontrar a sequencia
elsif	(cd_material_p is not null) then
	
	select	max(a.nr_sequencia)
	into	nr_seq_material_w
	from	pls_material a
	where	a.cd_material = cd_material_p
	and	sysdate between a.dt_inclusao and nvl(a.dt_exclusao,sysdate)
	and	a.ie_situacao = 'A';
end if;

-- so verifica o retorno caso tenha uma sequencia de material
if	(nr_seq_material_w is not null) then

	case	(ie_opcao_p)

		when 'ANVISA' then
			select	substr(to_char(max(a.cd_anvisa)), 1, 255)
			into	ds_retorno_w
			from	pls_mat_unimed_fed_sc a,
				pls_material b
			where	a.nr_sequencia = b.nr_seq_mat_uni_fed_sc
			and	b.nr_sequencia = nr_seq_material_w;

		when 'PA' then
			select	substr(to_char(max(a.nr_seq_ficha_tecnica)), 1, 255)
			into	ds_retorno_w
			from	material a,
				pls_material b
			where	b.nr_sequencia 	= nr_seq_material_w
			and	b.cd_material	= a.cd_material;

		when 'DPA' then
			select	substr(to_char(max(c.ds_substancia)), 1, 255)
			into	ds_retorno_w
			from	material a,
				pls_material b,
				medic_ficha_tecnica c
			where	b.nr_sequencia = nr_seq_material_w
			and	b.cd_material = a.cd_material
			and	a.nr_seq_ficha_tecnica = c.nr_sequencia;

		when 'UM' then
			select	substr(to_char(max(b.ds_unidade_medida)), 1, 255)
			into 	ds_retorno_w
			from	unidade_medida b,
				pls_material a
			where 	a.nr_sequencia = nr_seq_material_w
			and 	a.cd_unidade_medida = b.cd_unidade_medida;

		when 'FB' then
			select	substr(to_char(max(a.nr_seq_fabric)), 1, 255)
			into	ds_retorno_w
			from	material a,
				pls_material b
			where	b.nr_sequencia = nr_seq_material_w
			and	b.cd_material = a.cd_material;

		when 'CO' then
			select	substr(to_char(max(b.cd_material_ops)), 1, 255)
			into	ds_retorno_w
			from	pls_material b
			where	b.nr_sequencia = nr_seq_material_w;

		when 'DS' then
			select	substr(to_char(max(b.ds_material)), 1, 255)
			into	ds_retorno_w
			from	pls_material b
			where	b.nr_sequencia = nr_seq_material_w;

		when 'NC' then
			select 	substr(to_char(max(b.ds_nome_comercial)), 1, 255)
			into	ds_retorno_w
			from 	pls_material b
			where 	b.nr_sequencia 	= nr_seq_material_w;
		
		when 'TISS' then
			select	substr(to_char(max(ds_versao_tiss)), 1, 255)
			into	ds_retorno_w
			from 	pls_material
			where 	nr_sequencia = nr_seq_material_w;

		when 'DE' then
			select	substr(to_char(max(dt_exclusao), 'dd/mm/yyyy'), 1, 255)
			into	ds_retorno_w
			from 	pls_material
			where 	nr_sequencia = nr_seq_material_w;
			
		when 'SEQ' then
			ds_retorno_w := to_char(nr_seq_material_w);

		when 'COD' then
			select	substr(to_char(max(cd_material)), 1, 255)
			into	ds_retorno_w
			from 	pls_material
			where 	nr_sequencia = nr_seq_material_w;

		when 'EST' then
			select	substr(to_char(max(nr_seq_estrut_mat)), 1, 255)
			into	ds_retorno_w
			from 	pls_material
			where 	nr_sequencia = nr_seq_material_w;
		when 'LABT' then
		
			select	max(b.ds_laboratorio)
			into	ds_retorno_w
			from	pls_material		a,
				tuss_material_item	b
			where	b.nr_sequencia		= a.nr_seq_tuss_mat_item
			and	a.nr_sequencia		= nr_seq_material_w;
			
		when 'NT' then
		
			select	max(b.nm_tecnico)
			into	ds_retorno_w
			from	pls_material		a,
				tuss_material_item	b
			where	b.nr_sequencia		= a.nr_seq_tuss_mat_item
			and	a.nr_sequencia		= nr_seq_material_w;

		else
			ds_retorno_w := null;
	end case;
end if;

return	ds_retorno_w;

end pls_obter_dados_material;
/