create or replace
function obter_se_prescr_reaprazar(
			nr_sequencia_p		number,
			nr_atendimento_p	number,
			cd_pessoa_fisica_p	number,
			dt_inicio_p 		date)
			return varchar2 is

dt_inicio_prescr_w	prescr_medica.dt_inicio_prescr%type;
nr_prescricao_w		prescr_medica.nr_prescricao%type;
ie_retorno_w		varchar2(1) := 'N';
ie_parametro1_w		varchar2(1 char);

begin

obter_param_usuario(252, 1, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_parametro1_w);

select 	max(a.dt_inicio_prescr)
into	dt_inicio_prescr_w
from 	prescr_medica a,
		prescr_material b
where	a.nr_prescricao = b.nr_prescricao
and		b.nr_seq_mat_cpoe = nr_sequencia_p
and		dt_inicio_p between a.dt_inicio_prescr and a.dt_validade_prescr
and		((ie_parametro1_w = 'E' and a.dt_liberacao is null)
		or (ie_parametro1_w = 'F' and a.dt_liberacao is not null and a.dt_liberacao_farmacia is null));
		
if (dt_inicio_prescr_w is null) then
	nr_prescricao_w := gpt_obter_prescricao_vigente(nr_sequencia_p, 'M');

	select 	max(dt_inicio_prescr)
	into	dt_inicio_prescr_w
	from 	prescr_medica
	where	nr_prescricao = nr_prescricao_w
	and		((ie_parametro1_w = 'E' and dt_liberacao is null)
			or (ie_parametro1_w = 'F' and dt_liberacao is not null and dt_liberacao_farmacia is null));
end if;

if  (dt_inicio_p < dt_inicio_prescr_w) then
	ie_retorno_w := 'S';
end if;
	
return	ie_retorno_w;

end obter_se_prescr_reaprazar;
/
