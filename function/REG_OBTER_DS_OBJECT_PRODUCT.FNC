CREATE OR REPLACE
FUNCTION reg_obter_ds_object_product(
		nr_seq_object_p		reg_product_dic.nr_seq_objeto%TYPE,
		ie_object_type_p	reg_product_dic.ie_tipo_objeto%TYPE) RETURN VARCHAR2
	IS

	ds_object_w		VARCHAR2(4000);
	ds_select_table_w	VARCHAR2(50);
	ds_select_field_w	VARCHAR2(255);
	ie_object_type_w	reg_product_dic.ie_tipo_objeto%TYPE;

BEGIN	
	/**
	 * 'S' -- Schematics
	 * 'O' -- Dic_Objetos
	 * 'E' -- Evento
	 * 'D' -- Dic_dados
	 */
	IF	ie_object_type_p = 'S' THEN
		ds_select_table_w	:= 'OBJETO_SCHEMATIC';
		ds_select_field_w	:= 'DS_OBJETO || '' : '' || NR_SEQUENCIA';
	ELSIF	ie_object_type_p = 'O' THEN
		ds_select_table_w	:= 'DIC_OBJETO';
		ds_select_field_w	:= 'NM_OBJETO || '' : '' || NR_SEQUENCIA';
	ELSIF	ie_object_type_p = 'E' THEN
		ds_select_table_w	:= 'OBJ_SCHEMATIC_EVENTO';
		ds_select_field_w	:= 'Obter_Valor_Dominio(7697,ie_tipo_evento)||'' : ''||NR_SEQUENCIA';
	ELSE
		ds_select_table_w	:= 'OBJETO_SISTEMA';
		ds_select_field_w	:= 'NM_OBJETO || '' : '' || NR_SEQUENCIA';
	END IF;
	
	OBTER_VALOR_DINAMICO_4000_BV('SELECT '||ds_select_field_w||' FROM '||ds_select_table_w||'@WHEBL02_ORCL WHERE NR_SEQUENCIA = :nr_seq_obj ', 'nr_seq_obj='||nr_seq_object_p, ds_object_w);

	RETURN SUBSTR(ds_object_w, 1, 255);
END reg_obter_ds_object_product;
/