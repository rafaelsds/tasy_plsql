create or replace
function obter_se_lib_motivo_bloq(nr_seq_motivo_p	number,
				  cd_perfil_p		number,
				  nm_usuario_p		varchar2)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);			

Cursor C01 is
	select	nvl(ie_liberado,'S')
	from	liberacao_motivo_bloqueio
	where	nr_seq_motivo = nr_seq_motivo_p
	and 	nvl(cd_perfil, nvl(cd_perfil_p,0)) = nvl(cd_perfil_p,0)
	and 	nvl(nm_usuario_regra, nm_usuario_p) = nm_usuario_p
	and 	ie_situacao = 'A'
	order by	nvl(cd_perfil,0),
		nvl(nm_usuario_regra,' ');
			
begin

ie_retorno_w:= 'S';

open C01;
loop
fetch C01 into	
	ie_retorno_w;
exit when C01%notfound;
	begin
	ie_retorno_w:= ie_retorno_w;
	end;
end loop;
close C01;

return	ie_retorno_w;

end obter_se_lib_motivo_bloq;
/
