create or replace function obter_cor_status_unidades(	ds_status_p		varchar2,
							cd_perfil_p		number,
							cd_estabelecimento_p	number,
							ie_html5_p varchar2 default 'N')
 		    	return varchar2 is

ds_cor_status_w		varchar2(15);
nr_seq_cor_w		number(10);
ds_cor_fonte     	varchar2(15);
ds_cor_selecao      	varchar2(15);
				
begin

ds_cor_status_w := ''; 

	if ie_html5_p = 'N' then
		select	max(nr_sequencia)
		into	nr_seq_cor_w
		from	tasy_padrao_cor
		where	nr_seq_legenda = 1023
		and		upper(ds_item) = upper(ds_status_p);
	else
		select	max(nr_sequencia),
			max(ds_cor_html)
		into	nr_seq_cor_w,
			ds_cor_status_w
		from	tasy_padrao_cor
		where	nr_seq_legenda = 1023
		and		upper(obter_desc_expressao(cd_exp_item, '')) = upper(ds_status_p);
		
		
		
		if	(ds_cor_status_w	is not null) then
			return ds_cor_status_w;
		end if;
		
	end if;
	if	(nvl(nr_seq_cor_w,0) > 0) then

		Tasy_Obter_Cor(nr_seq_cor_w,cd_perfil_p,cd_estabelecimento_p,ds_cor_status_w,ds_cor_fonte,ds_cor_selecao);

	end if;

return	ds_cor_status_w;

end obter_cor_status_unidades;
/