create or replace function VERIFICA_EVOLUCAO_FERIDA(NR_ATENDIMENTO_P NUMBER,
                                                    NR_SEQ_FERIDA_P  NUMBER)
  RETURN VARCHAR2 IS

  RETORNO  VARCHAR2(1) := 'N';
  EVOLUCAO NUMBER := 0;

BEGIN

  IF (nvl(NR_ATENDIMENTO_P, 0) > 0) AND (nvl(NR_SEQ_FERIDA_P, 0) > 0) THEN
    select count(0)
      INTO EVOLUCAO
      from CUR_EVOLUCAO ce
     where ce.nr_atendimento = NR_ATENDIMENTO_P 
       and nr_seq_ferida = NR_SEQ_FERIDA_P
       and dt_liberacao is not null;

  END IF;

 IF EVOLUCAO > 0 THEN
 RETORNO := 'S';
 END IF;

 RETURN RETORNO;

END VERIFICA_EVOLUCAO_FERIDA;
/