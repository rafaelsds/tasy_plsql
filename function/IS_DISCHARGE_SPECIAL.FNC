CREATE OR REPLACE 
FUNCTION is_discharge_special
			(	nr_atendimento_p	number)
				return varchar is

ie_special_discharge_w	varchar2(1);


begin

select	nvl(max(b.IE_ALTA_ESPECIAL), 'N') 
into	ie_special_discharge_w
from	motivo_alta		b,
	atendimento_paciente	a
WHERE	a.nr_atendimento	= nr_atendimento_p
AND	a.cd_motivo_alta	= b.cd_motivo_alta;


return	ie_special_discharge_w;

END is_discharge_special;
/
