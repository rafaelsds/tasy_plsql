create or replace
function obter_modelo_carta(ie_preliminar_p		varchar2)
 		    	return number is
				
nr_seq_modelo_w	carta_medica_modelo.nr_sequencia%type;		
cd_perfil_w		carta_medica_modelo.cd_perfil%type;	

begin
	
cd_perfil_w	:= nvl(obter_perfil_ativo,0);

select	nvl(max(nr_sequencia),0)
into	nr_seq_modelo_w
from	carta_medica_modelo
where	nvl(cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
and		nvl(cd_perfil,cd_perfil_w) = cd_perfil_w
and		ie_preliminar = ie_preliminar_p
order by nr_sequencia desc;


return	nr_seq_modelo_w;

end obter_modelo_carta;
/
