create or replace
function Obter_Filme_Abramge
		(nr_interno_conta_p	number)
		return number is

vl_filme_w	number(15,4)	:= 0;

begin

select 	nvl(sum(vl_item),0)
into	vl_filme_w
from 	w_conta_abramge_item a
where 	nr_interno_conta 	= nr_interno_conta_p
and 	ie_tipo_item		= 2
and 	substr(upper(ds_item),1,5) = 'FILME'
and 	cd_grupo_item = 5
and 	vl_item > 0;

return	vl_filme_w;

end Obter_Filme_Abramge;
/