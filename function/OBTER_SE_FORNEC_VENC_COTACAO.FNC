create or replace
function obter_se_fornec_venc_cotacao(	nr_cot_compra_p			number,
					nr_item_cot_compra_p		number,
					nr_seq_cot_forn_p			number)
 		    	return varchar2 is

ie_vencedor_w			varchar2(1) := 'N';
qt_existe_w			number(10);

begin
select	count(*)
into	qt_existe_w
from	cot_compra_resumo
where	nr_cot_compra		= nr_cot_compra_p
and	nr_item_cot_compra	= nr_item_cot_compra_p
and	nr_seq_cot_forn		= nr_seq_cot_forn_p;

if	(qt_existe_w > 0) then
	ie_vencedor_w := 'S';
end if;

return	ie_vencedor_w;

end	obter_se_fornec_venc_cotacao;
/