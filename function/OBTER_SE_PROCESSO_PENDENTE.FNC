CREATE OR REPLACE 
FUNCTION obter_se_processo_pendente	(nr_seq_processo_p	number,
					nm_usuario_p	varchar2)
				RETURN VARCHAR2 IS

ie_retorno_w		varchar2(1) 	:= 'N';
cd_pessoa_fisica_w	varchar2(10);
ie_resultado_w		varchar2(1);
ie_aval_anterior_w	varchar2(1)	:= 'N';	
qt_etapa_w		number(5)	:= 0;
cd_pessoa_fisica_p	varchar2(10);
i			number(5)	:= 0;

CURSOR c01 IS
	SELECT	nvl(cd_pessoa_fisica,0),
		ie_resultado
	FROM	gap_processo_etapa
	WHERE	nr_seq_processo	= nr_seq_processo_p
	ORDER BY	dt_prevista;

begin

select	nvl(cd_pessoa_fisica,0)
into	cd_pessoa_fisica_p
from	usuario
where	nm_usuario = nm_usuario_p;

SELECT	COUNT(*)
INTO	qt_etapa_w
FROM	gap_processo_etapa
WHERE	nr_seq_processo = nr_seq_processo_p;

IF	(qt_etapa_w	= 0) then
	ie_retorno_w	:= 'N';
elsif	(qt_etapa_w	= 1) then
	begin

	select	cd_pessoa_fisica,
		ie_resultado
	into	cd_pessoa_fisica_w,
		ie_resultado_w
	from	gap_processo_etapa
	where	nr_seq_processo		= nr_seq_processo_p;

	if	(cd_pessoa_fisica_w	= cd_pessoa_fisica_p) and
		(ie_resultado_w		= 'N') then
		ie_retorno_w		:= 'S'; 
	end if;
	
	end;
ELSE	BEGIN

	open c01;
	loop	fetch c01 into
		cd_pessoa_fisica_w,
		ie_resultado_w;
	exit	when c01%notfound;
		if	(cd_pessoa_fisica_w	= cd_pessoa_fisica_p)	and
			(ie_resultado_w		= 'N')			and
			(ie_aval_anterior_w	<> 'N')			then
			ie_retorno_w		:= 'S';
		elsif	(cd_pessoa_fisica_w	= cd_pessoa_fisica_p)	and
			(ie_resultado_w		= 'N') and (i = 0)	then
			ie_retorno_w		:= 'S';			
		end if;
		ie_aval_anterior_w	:= ie_resultado_w;
		i			:= i + 1;
	end loop;
	close C01;

	END;
end if;

return	ie_retorno_w;

end	obter_se_processo_pendente;
/
