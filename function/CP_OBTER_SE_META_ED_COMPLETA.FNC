create or replace
function cp_obter_se_meta_ed_completa(
					nr_seq_pat_cp_goal_p number) 
				return varchar2 is

qt_registros_w	number(10);
ds_retorno_w	varchar2(1) := 'N';

begin

if(nr_seq_pat_cp_goal_p is not null) then

	select	count(1)
	into	qt_registros_w
	from	pat_cp_ind_measure_eg
	where	nr_seq_pat_cp_goal = nr_seq_pat_cp_goal_p;
	
	if (qt_registros_w > 0) then
		select 	decode(count(1), 0, 'S', 'N')
		into	ds_retorno_w
		from 	pat_cp_ind_measure_eg a
		where 	a.nr_seq_pat_cp_goal = nr_seq_pat_cp_goal_p
		and 	a.dt_liberacao is null;
	end if;
end if;

return ds_retorno_w;

end cp_obter_se_meta_ed_completa;
/
