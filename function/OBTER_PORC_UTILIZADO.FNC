create or replace
function obter_porc_utilizado(	qt_previsto_p		number,
				qt_realizado_p		number)
 		    	return number is

begin

return ((qt_realizado_p / qt_previsto_p) * 100);

end obter_porc_utilizado;
/