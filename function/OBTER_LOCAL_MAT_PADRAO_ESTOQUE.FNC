create or replace
function Obter_local_mat_padrao_estoque
		(cd_material_p		varchar2,
		cd_local_estoque_p	number,
		cd_estabelecimento_p	number)
		return varchar2 is

ds_local_w	varchar2(60);

begin

ds_local_w := '';

select	max(b.DS_LOCAL)
into	ds_local_w
from	LOCALIZACAO_ESTOQUE_LOCAL a,
	MATERIAL_LOCAL b
where	a.NR_SEQ_LOCAL = b.nr_sequencia
and	a.cd_local_estoque = cd_local_estoque_p
and	a.cd_material	= cd_material_p
and	a.cd_estabelecimento = cd_estabelecimento_p;

return	ds_local_w;

end Obter_local_mat_padrao_estoque;
/