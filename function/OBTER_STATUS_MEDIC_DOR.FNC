create or replace 
function obter_status_medic_dor (	nr_prescricao_p		number,
									nr_seq_material_p	number
								)
								return varchar2 is

dt_horario_w		prescr_mat_hor.dt_horario%type;
dt_suspensao_w		prescr_mat_hor.dt_suspensao%type;
dt_fim_horario_w	prescr_mat_hor.dt_fim_horario%type;
ds_status_w			varchar2(25);
ds_espaco_w			varchar2(10) := '     ';
ds_retorno_w		varchar2(1000) := '';

cursor c01 is
	select 	a.dt_horario,
			a.dt_suspensao,
			a.dt_fim_horario			
	from 	prescr_mat_hor a
	where 	a.nr_prescricao = nr_prescricao_p 
	and 	a.nr_seq_material = nr_seq_material_p
	and		nvl(a.ie_situacao, 'A') = 'A'
	and		nvl(a.ie_adep, 'S') = 'S'
	and		a.ie_agrupador = 1
	and		((nvl(a.ie_horario_especial,'N') = 'N') or (a.dt_fim_horario is not null))
	order by	a.nr_sequencia;
								
begin

	open c01;
		loop
			fetch c01 into				
				dt_horario_w,
				dt_suspensao_w,
				dt_fim_horario_w;
			exit when C01%notfound;
  				begin				
					if (dt_horario_w is not null) then
						ds_status_w := obter_desc_expressao(295406);
						
						if (dt_suspensao_w is not null) then
							ds_status_w := obter_desc_expressao(298991);
						elsif (dt_fim_horario_w is not null) then
							ds_status_w := obter_desc_expressao(321558);
						end if;
						
						ds_retorno_w := substr(ds_retorno_w || to_char(dt_horario_w, 'dd/mm hh24:mi') || ' ' || substr('(' || ds_status_w || ')' || ds_espaco_w, 1, 15), 1, 1000);
												
					end if;		
				end;
		end loop;
	close c01;			

return ds_retorno_w;

end obter_status_medic_dor;
/
