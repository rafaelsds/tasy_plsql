create or replace
function pls_obter_preco_atual_mat	(nr_seq_simpro_p		number,
					nr_seq_brasindice_p		number,
					ie_tipo_tabela_p		varchar2,
					ie_opcao_p			varchar2)
					return number is

/*
ie_opcao_p		'PAB' - Pre�o atual brasindice
			'PCB' - Pre�o convertido brasindice
			'PAS' - Pre�o atual simpro
			'PCS' - Pre�o convertido simpro

ie_tipo_tabela_p	'S' - Simpro
			'B' - Brasindice	
*/

vl_preco_venda_w	Number(15,2);
vl_preco_w		Number(15,4) := 0;
cd_simpro_w		Number(15);
qt_conversao_w		number(13,4);
cd_medicamento_w	varchar2(6);
cd_laboratorio_w	varchar2(6);
cd_apresentacao_w	varchar2(6);

begin

/*
if	(ie_tipo_tabela_p = 'S') then

	select	nvl(max(qt_conversao),1),
		nvl(max(cd_simpro),0)
	into	qt_conversao_w,
		cd_simpro_w
	from	pls_material_simpro
	where	nr_sequencia	= nr_seq_simpro_p;

	if	(cd_simpro_w > 0) then

		select	nvl(max(vl_preco_venda),0)
		into	vl_preco_venda_w
		from	simpro_preco
		where	cd_simpro = cd_simpro_w
		and	dt_vigencia =	(select max(dt_vigencia)
					from simpro_preco
					where cd_simpro = cd_simpro_w);

		if	(ie_opcao_p	= 'PAS') then
			vl_preco_w	:= vl_preco_venda_w;
		elsif	(ie_opcao_p	= 'PCS') then
			vl_preco_w	:= dividir_sem_round(vl_preco_venda_w,qt_conversao_w);
		end if;	
	end if;

elsif	(ie_tipo_tabela_p = 'B') then

	if	(nr_seq_brasindice_p is not null) then

		select	cd_medicamento,
			cd_laboratorio,
			cd_apresentacao,
			qt_conversao
		into	cd_medicamento_w,
			cd_laboratorio_w,
			cd_apresentacao_w,
			qt_conversao_w
		from	pls_material_brasindice
		where	nr_sequencia	= nr_seq_brasindice_p;

		if	(cd_medicamento_w is not null) and
			(cd_laboratorio_w is not null) and
			(cd_apresentacao_w is not null) then

			select	nvl(sum(vl_preco_medicamento),0)
			into	vl_preco_venda_w
			from	brasindice_preco
			where	cd_medicamento	= cd_medicamento_w
			and	cd_apresentacao	= cd_apresentacao_w
			and	cd_laboratorio	= cd_laboratorio_w
			and	dt_inicio_vigencia =
						(select	max(dt_inicio_vigencia)
						from	brasindice_preco
						where	cd_medicamento	= cd_medicamento_w);

			if	(ie_opcao_p	= 'PAB') then
				vl_preco_w	:= vl_preco_venda_w;
			elsif	(ie_opcao_p	= 'PCB') then
				vl_preco_w	:= dividir_sem_round(vl_preco_venda_w,qt_conversao_w);
			end if;
		end if;
	end if;
end if;

*/

return vl_preco_w;

end pls_obter_preco_atual_mat;
/