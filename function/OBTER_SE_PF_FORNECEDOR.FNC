create or replace function obter_se_pf_fornecedor (
  cd_pessoa_fisica_p in varchar2 default null,
  cd_estabelecimento_p in number
) return varchar2 is

ie_fornecedor_pf_w varchar2(1);
ie_fornecedor_w varchar2(1);
ie_retorno_w varchar2(1) := 'S';

begin

begin
  select nvl(ie_fornecedor_pf, 'N')
  into ie_fornecedor_pf_w
  from parametro_compras
  where cd_estabelecimento = cd_estabelecimento_p;
  exception
    when no_data_found then
      ie_fornecedor_pf_w := 'N';
  end;
  
if cd_pessoa_fisica_p is not null and ie_fornecedor_pf_w = 'S' then
  
  begin
  select nvl(ie_fornecedor, 'N')
  into ie_fornecedor_w
  from pessoa_fisica
  where cd_pessoa_fisica = cd_pessoa_fisica_p;
  exception
    when no_data_found then
      ie_fornecedor_w := 'N';
  end;

  ie_retorno_w := ie_fornecedor_w;

end if;

return ie_retorno_w;

end obter_se_pf_fornecedor;
/