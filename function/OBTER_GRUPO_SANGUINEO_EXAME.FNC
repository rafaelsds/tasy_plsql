create or replace
function obter_grupo_sanguineo_exame	(cd_pessoa_fisica_p	varchar2,
					ie_opcao_p		varchar2)
					return varchar2 is

nr_seq_exame_tipo_sang_w	number(10);
nr_seq_exame_fator_rh_w		number(10);


nr_seq_result_tipo_sanguineo_w	number(10);
nr_seq_resultado_fator_rh_w	number(10);

ds_result_tipo_sanguineo_w	varchar2(255);
ds_resultado_fator_rh_w		varchar2(255);

ds_retorno_w			varchar2(255);

begin	
/*

Elton - Comentado todo o processo abaixo na OS 412692 pois o processo � inutil...ver que o ds_retorno_w � sempre null, desconsiderando o resto do processo
					
select	nvl(max(nr_seq_exame_tipo_sang),0),
	nvl(max(nr_seq_exame_fator_rh),0)
into	nr_seq_exame_tipo_sang_w,
	nr_seq_exame_fator_rh_w
from	parametro_medico;

select  nvl(max(nr_seq_resultado),0)
into	nr_seq_result_tipo_sanguineo_w
from	(select	max(nvl(c.nr_seq_resultado,0)) nr_seq_resultado
	from	prescr_medica d,
		exame_lab_resultado c,
		exame_lab_result_item b
	where	b.nr_seq_resultado = c.nr_seq_resultado
	and	c.nr_prescricao =  d.nr_prescricao
	and	b.nr_seq_exame	= nr_seq_exame_tipo_sang_w
	and 	obter_data_result_exame(c.nr_prescricao,b.nr_seq_prescr) is not null
	and 	((b.qt_resultado is not null) or (nvl(ds_resultado,'0') <> '0'))
	and	d.cd_pessoa_fisica = cd_pessoa_fisica_p
	union
	select	max(nvl(c.nr_seq_resultado,0))
	from	exame_lab_resultado c,
		exame_lab_result_item b
	where	b.nr_seq_resultado = c.nr_seq_resultado
	and	b.nr_seq_exame	= nr_seq_exame_tipo_sang_w
	and 	c.dt_liberacao is not null
	and 	((b.qt_resultado is not null) or (nvl(ds_resultado,'0') <> '0'))
	and	c.cd_pessoa_fisica = cd_pessoa_fisica_p);

select 	max(decode(nvl(to_char(b.qt_resultado),'0'),'0',ds_resultado,to_char(b.qt_resultado)))
into	ds_result_tipo_sanguineo_w
from	exame_lab_resultado c,
	exame_lab_result_item b
where	b.nr_seq_resultado = c.nr_seq_resultado
and	b.nr_seq_exame	= nr_seq_exame_tipo_sang_w
and	c.nr_seq_resultado = nr_seq_result_tipo_sanguineo_w;


select  nvl(max(nr_seq_resultado),0)
into	nr_seq_resultado_fator_rh_w
from	(select	c.nr_seq_resultado
	from	prescr_medica d,
		exame_lab_resultado c,
		exame_lab_result_item b
	where	b.nr_seq_resultado = c.nr_seq_resultado
	and	c.nr_prescricao =  d.nr_prescricao
	and	b.nr_seq_exame	= nr_seq_exame_fator_rh_w
	and 	obter_data_result_exame(c.nr_prescricao,b.nr_seq_prescr) is not null
	and 	((b.qt_resultado is not null) or (nvl(ds_resultado,'0') <> '0'))
	and	d.cd_pessoa_fisica = cd_pessoa_fisica_p
	union
	select	c.nr_seq_resultado
	from	exame_lab_resultado c,
		exame_lab_result_item b
	where	b.nr_seq_resultado = c.nr_seq_resultado
	and	b.nr_seq_exame	= nr_seq_exame_fator_rh_w
	and 	c.dt_liberacao is not null
	and 	((b.qt_resultado is not null) or (nvl(ds_resultado,'0') <> '0'))
	and	c.cd_pessoa_fisica = cd_pessoa_fisica_p);
	
select 	max(decode(nvl(to_char(b.qt_resultado),'0'),'0',ds_resultado,to_char(b.qt_resultado)))
into	ds_resultado_fator_rh_w
from	exame_lab_resultado c,
	exame_lab_result_item b
where	b.nr_seq_resultado = c.nr_seq_resultado
and	b.nr_seq_exame	= nr_seq_exame_fator_rh_w
and	c.nr_seq_resultado = nr_seq_resultado_fator_rh_w;
*/ 
if	(ds_retorno_w is null) then

	select	max(IE_FATOR_RH),
		max(IE_TIPO_SANGUE)
	into	ds_resultado_fator_rh_w,
		ds_result_tipo_sanguineo_w
	from 	pessoa_fisica
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
end if;

if	(ie_opcao_p = 'S') then
	
	ds_retorno_w	:= ds_result_tipo_sanguineo_w || ds_resultado_fator_rh_w;

end if;

return ds_retorno_w;

end obter_grupo_sanguineo_exame;
/
