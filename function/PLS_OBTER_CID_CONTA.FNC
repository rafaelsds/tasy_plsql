create or replace
function pls_obter_cid_conta
			(	nr_seq_conta_p		number)
				return Varchar2 is
	
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter o CID da conta
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/
	
cd_cid_w		Varchar2(10);				

begin

select	max(nvl(cd_doenca, cd_doenca_imp))
into	cd_cid_w
from	pls_diagnostico_conta
where 	nr_seq_conta = nr_seq_conta_p
and	nvl(ie_classificacao,'P') = 'P';
				
return	cd_cid_w;

end pls_obter_cid_conta;
/

