create or replace
function Busca_total_orcamento_custo(	ie_orcado_real_p	varchar2,
					cd_empresa_p		number, 
					cd_estabelecimento_p	number,
					cd_centro_controle_p	number,
					dt_referencia_p		date)
					return number is
				
total_w		number(15,2);
				
begin

SELECT SUM(a.vl_orcado) total
into   total_w
FROM	grupo_natureza_gasto e,
	natureza_gasto d,
	centro_controle c,
	tabela_custo b,
	orcamento_custo a
WHERE	a.cd_tabela_custo	= b.cd_tabela_custo
AND	b.ie_orcado_real        = ie_orcado_real_p
AND	a.cd_estabelecimento	= b.cd_estabelecimento
AND	a.cd_centro_controle	= c.cd_centro_controle
AND	a.cd_estabelecimento	= c.cd_estabelecimento
AND	a.nr_seq_ng		= d.nr_sequencia
AND	d.nr_seq_gng		= e.nr_sequencia
AND	a.cd_estabelecimento	= NVL(d.cd_estabelecimento,a.cd_estabelecimento)
AND	a.cd_estabelecimento	= NVL(e.cd_estabelecimento,a.cd_estabelecimento)
AND	e.cd_empresa		= d.cd_empresa
AND	e.cd_empresa		= cd_empresa_p
AND	a.cd_estabelecimento	= cd_estabelecimento_p
AND	(c.cd_centro_controle	= cd_centro_controle_p)
AND	PKG_DATE_UTILS.start_of(b.dt_mes_referencia, 'month', 0) BETWEEN PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p, -2, 0), 'month', 0) AND PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0);

return	total_w;

end Busca_total_orcamento_custo;
/