create or replace
function rep_obter_dados_atend_pac(	nr_atendimento_p	number,
									ie_opcao_p			varchar2)
		return varchar2 is


ds_retorno_w	varchar2(255);
/*
P - possui conta encerrada;
DPA - data previsto alta;
DAM - data alta medico
*/
begin
if	(ie_opcao_p is not null) and
	(nr_atendimento_p is not null) then
	begin
	if	(ie_opcao_p = 'P') then
		begin
		select	nvl(max('S'),'N')
		into	ds_retorno_w
		from	atendimento_paciente
		where	rownum = 1
		and		dt_fim_conta is not null
		and		nr_atendimento = nr_atendimento_p;
		end;
	elsif	(ie_opcao_p = 'DPA') then
		begin
		select	to_char(max(dt_previsto_alta),'dd/mm/yyyy hh24:mi:ss')
		into	ds_retorno_w
		from	atendimento_paciente
		where	nr_atendimento 		= nr_atendimento_p
		and		ie_probabilidade_alta 	= 'C';
		end;
	elsif	(ie_opcao_p = 'DPAA') then
		begin
		ds_retorno_w	:= to_char(obter_data_prev_alta(nr_atendimento_p),'dd/mm/yyyy hh24:mi:ss');
		end;
	elsif	(ie_opcao_p = 'DAM') then
		begin
		select	to_char(max(dt_alta_medico),'dd/mm/yyyy hh24:mi:ss')
		into	ds_retorno_w
		from	atendimento_paciente
		where	nr_atendimento		= nr_atendimento_p;
		end;
	end if;
	end;
end if;

return ds_retorno_w;

end rep_obter_dados_atend_pac;
/