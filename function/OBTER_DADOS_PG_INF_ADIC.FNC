create or replace
function obter_dados_pg_inf_adic(	nr_sequencia_p	number,
					ie_opcao_p	varchar2)
					return varchar2 is

ds_informacao_w		varchar2(255);
ds_retorno_w		varchar2(255);
					
begin

if	(nr_sequencia_p > 0) then
	select	ds_informacao
	into	ds_informacao_w
	from	perda_ganho_inf_adic
	where	nr_sequencia = nr_sequencia_p;
end if;
	
if	(ie_opcao_p = 'D') then
	ds_retorno_w := ds_informacao_w;
end if;

return ds_retorno_w;

end obter_dados_pg_inf_adic;
/