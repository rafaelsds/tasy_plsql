create or replace
function pls_obter_se_prestador_pacote
			(	nr_seq_prestador_p		number,
				nr_seq_conta_proc_p	number)
				return varchar2 is
				
ds_retorno_w 		Varchar2(1);
nr_seq_regra_w		Number(10);
nr_seq_preco_pacote_w	Number(10);

begin

ds_retorno_w := 'N';
select 	max(nr_seq_preco_pacote)
into	nr_seq_preco_pacote_w
from 	pls_conta_proc
where	nr_sequencia = nr_seq_conta_proc_p;

/*askono - alterei para que seja  comparado o prestador do pacote e nao o prestador da regra de pacote , pois este prestador da regra de pacote � cadastrado como regra */
if	(nvl(nr_seq_preco_pacote_w,0) > 0) then
	select	max(a.nr_sequencia)
	into	nr_seq_regra_w
	from	pls_pacote_tipo_acomodacao 	a,
		pls_pacote 			b
	where	b.nr_sequencia 	=  a.nr_Seq_pacote
	and	a.nr_Sequencia 	=  nr_seq_preco_pacote_w 		
	and	((b.nr_seq_prestador is null) or
		(b.nr_seq_prestador = nr_seq_prestador_p));

	if	(nvl(nr_seq_regra_w,0) > 0 ) then
		ds_retorno_w := 'S';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_prestador_pacote;
/