create or replace function verificar_cpoe_ativa(cd_estabelecimento_p number, nr_atendimento_p number)
    return varchar2 is

ie_cpoe_ativo_w  varchar2(1);

begin

select NVL(max('S'), 'N')
into ie_cpoe_ativo_w
from    (select  count(*) itens
         from    parametro_medico
         where   cd_estabelecimento = cd_estabelecimento_p
         and     ie_rep_cpoe = 'S'
         union all
         select  count(*) itens
         from    CPOE_MATERIAL 
         where   nr_atendimento = nr_atendimento_p)
where   itens > 0;

return ie_cpoe_ativo_w;
end verificar_cpoe_ativa;
/
