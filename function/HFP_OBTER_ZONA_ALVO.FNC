create or replace
function hfp_obter_zona_alvo(	nr_seq_paciente_p	Number) 
			return varchar2 is 
vl_zona_min	number(3);
vl_zona_max	number(3);
ds_resultado	varchar2(10);
begin
ds_resultado := '';

if (nvl(nr_seq_paciente_p,0) > 0) then
	begin
	
	select	vl_zona_alvo_min,
		vl_zona_alvo_max
	into	vl_zona_min,
		vl_zona_max
	from	hfp_paciente
	where	nr_sequencia = nr_seq_paciente_p;	
		
	if ((vl_zona_min > 0) and (vl_zona_max > 0)) then
		ds_resultado := (vl_zona_min || ' - ' || vl_zona_max);
	end if;
	end;
end if;

return ds_resultado;

end hfp_obter_zona_alvo;
/
