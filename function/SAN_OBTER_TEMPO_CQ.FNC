create or replace
function san_obter_tempo_cq (nr_seq_producao_p	number)
 		    	return number is

qt_tempo_dias_cq_w	number(10);
			
begin

if (nr_seq_producao_p is not null) then
	
	select	trunc(max(a.dt_liberacao)-max(a.dt_realizacao))
	into	qt_tempo_dias_cq_w
	from	san_controle_qualidade a,
		san_controle_qual_prod b
	where	a.nr_sequencia = b.nr_seq_qualidade
	and	b.nr_seq_producao = nr_seq_producao_p;	

end if;


return	qt_tempo_dias_cq_w;

end san_obter_tempo_cq;
/