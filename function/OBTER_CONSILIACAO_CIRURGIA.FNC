create or replace
function obter_consiliacao_cirurgia(	nr_cirurgia_p		number,
					cd_material_p		number,
					cd_unidade_medida_p	varchar2,
					qt_disp_pepo_p		number,
					ie_opcao_p		varchar2,
					nr_seq_lote_fornec_p    number default 0)
					return number is

/*
P - Prescricao
E - Enfermagem dispensou
*/

nr_prescricao_w			number(14,0);
qt_material_w			number(15,3);
qt_dispensacao_w		number(15,3);
qt_retorno_w			number(15,3);
ie_consolidacao_generico_w	varchar2(1);
cd_material_estoque_w		number(6);	

begin

Obter_Param_Usuario(872, 152, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_consolidacao_generico_w);

--if	(nvl(qt_disp_pepo_p,0) > 0) then
--	qt_retorno_w := qt_disp_pepo_p;
--else	
	if	(ie_opcao_p = 'P') then

		select	nr_prescricao
		into	nr_prescricao_w
		from	cirurgia
		where	nr_cirurgia	=	nr_cirurgia_p;

		select	sum(nvl(qt_material,0))
		into	qt_material_w
		from	prescr_material
		where	nr_prescricao	=	nr_prescricao_w
		and	cd_material	=	cd_material_p
		and	cd_unidade_medida = 	cd_unidade_medida_p;

		qt_retorno_w	:=	qt_material_w;	

	elsif	(ie_opcao_p = 'E') then
		if	(ie_consolidacao_generico_w = 'S') then
			select	sum(nvl(qt_dispensacao,0))
			into	qt_dispensacao_w
			from	cirurgia_agente_disp a,
				material b
			where	a.cd_material	= 	b.cd_material_estoque
			and	a.ie_operacao	=	'D'
			and	a.nr_cirurgia	=	nr_cirurgia_p
			and	b.cd_material	=	cd_material_p
			and 	(nvl(nr_seq_lote_fornec,nvl(nr_seq_lote_fornec_p,0)) = nvl(nr_seq_lote_fornec_p,0))
			and	a.cd_unidade_medida =	cd_unidade_medida_p;
		else
			select	sum(nvl(qt_dispensacao,0))
			into	qt_dispensacao_w
			from	cirurgia_agente_disp
			where	nr_cirurgia	=	nr_cirurgia_p
			and	ie_operacao	=	'D'
			and	cd_material	=	cd_material_p
			and 	(nvl(nr_seq_lote_fornec,nvl(nr_seq_lote_fornec_p,0)) = nvl(nr_seq_lote_fornec_p,0))
			and	cd_unidade_medida =	cd_unidade_medida_p;
		end if;	

		qt_retorno_w	:=	qt_dispensacao_w;	
	end if;
--end if;	

return	nvl(qt_retorno_w,0);

end obter_consiliacao_cirurgia;
/