create or replace
function	pls_obter_qtd_contratos_mes
			(	dt_mes_referencia_p	date,
				ie_tipo_retorno_p	varchar2)
				return  varchar2 is
				

qt_retorno_w			number(10);				
qt_cgc_estipulante_ati_w	number(10);
qt_cgc_subestipulante_ati_w	number(10);
qt_cgc_estipulante_ina_w	number(10);
qt_cgc_subestipulante_ina_w	number(10);
			
begin

select	count(*)
into	qt_cgc_estipulante_ati_w
from	pls_contrato	a
where	((trunc(a.dt_rescisao_contrato,'month') > trunc(dt_mes_referencia_p,'Month')) or 
	(a.dt_rescisao_contrato is null))
and	trunc(a.dt_contrato,'Month')			<=  trunc(dt_mes_referencia_p,'Month')
and	a.cd_cgc_estipulante is not null;

select	count(*)
into	qt_cgc_estipulante_ina_w
from	pls_contrato	a
where	trunc(a.dt_rescisao_contrato,'month')		<= trunc(dt_mes_referencia_p,'Month')
and	a.cd_cgc_estipulante is not null;

select	count(*)
into	qt_cgc_subestipulante_ati_w
from	pls_sub_estipulante	a,
	pls_contrato		b
where	a.nr_seq_contrato	= b.nr_sequencia
and	((trunc(b.dt_rescisao_contrato,'month') > trunc(dt_mes_referencia_p,'Month')) or 
	(b.dt_rescisao_contrato is null))
and	trunc(b.dt_contrato,'Month')		<=  trunc(dt_mes_referencia_p,'Month')
and	a.cd_cgc is not null;

select	count(cd_cgc)
into	qt_cgc_subestipulante_ina_w
from	pls_sub_estipulante	a,
	pls_contrato		b
where	a.nr_seq_contrato	= b.nr_sequencia
and	trunc(b.dt_rescisao_contrato,'month')		<= trunc(dt_mes_referencia_p,'Month');

if	(ie_tipo_retorno_p	= 'A') then
qt_retorno_w	:=	(qt_cgc_estipulante_ati_w) + (qt_cgc_subestipulante_ati_w);

elsif	(ie_tipo_retorno_p	= 'I') then
qt_retorno_W	:=	(qt_cgc_estipulante_ina_w) + (qt_cgc_subestipulante_ina_w);

end if;

return qt_retorno_w;

end pls_obter_qtd_contratos_mes;
/