create or replace function PFCS_GET_TELE_PAT_REVIEW_CNT(
        cd_establishment_p in pfcs_panel.nr_seq_operational_level%type) return pfcs_panel.vl_indicator%type deterministic is

qt_patient_on_review_w	pfcs_panel.vl_indicator%type := 0;

begin

    select	count(*) into qt_patient_on_review_w
    from	pfcs_panel_detail ppd join pfcs_detail_patient pdp on ppd.nr_sequencia = pdp.nr_seq_detail
		join pfcs_detail_bed pdb on ppd.nr_sequencia = pdb.nr_seq_detail
        join pfcs_detail_device pdd on ppd.nr_sequencia = pdd.nr_seq_detail
    where	ppd.nr_seq_indicator =  pfcs_pck_indicators.nr_tele_monitors_in_use
    and	ppd.nr_seq_operational_level = cd_establishment_p
    and	ppd.ie_situation = pfcs_pck_constants.ie_active
    and pfcs_telemetry_config_pck.get_patient_review(pdp.dt_monitor_entrance) =  pfcs_pck_constants.ie_yes_br ;

    return qt_patient_on_review_w;

    exception
        when no_data_found then
            return 0;
            
end PFCS_GET_TELE_PAT_REVIEW_CNT;
/
