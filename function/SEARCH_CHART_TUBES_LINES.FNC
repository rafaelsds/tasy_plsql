create or replace
function search_chart_tubes_lines(	code_group_p			varchar2,
												nr_seq_pepo_p			number,
												nr_cirurgia_p			number,
												nr_atendimento_p		number,
												cd_pessoa_fisica_p	varchar2,
												dt_atendimento_p		date,
												ie_em_andamento_p		varchar2)	
			return get_chart_tubes_lines_table pipelined is type cursor_ref is ref cursor;
			
name_w				varchar2(80);
color_w				varchar2(30);
dt_instalacao_w	date;
end_date_w			date;
code_chart_w		number(10);
nr_seq_apres_w		number(10);
dt_fim_w				date;
ie_sucesso_w		varchar2(15);
nr_atendimento_w	number(10);
nr_cirurgia_w		number(10);
nr_seq_pepo_w		number(10);
ie_permite_disp_alta_w varchar2(15);
ie_exige_disp_grafico_w		varchar2(15);
			
	
get_values_r		get_chart_tubes_lines_row := get_chart_tubes_lines_row(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

cursor	tubes_and_lines is
	select	substr(b.ds_dispositivo,1,80) NAME,
				substr(Obter_Cor_Grafico_Dispositivo(nr_seq_dispositivo,'S'),1,30) COLOR,
				a.dt_instalacao, 
				(sysdate + 0.1) end_date,
				a.nr_sequencia code_chart,
				b.nr_seq_apres,
				a.dt_retirada dt_fim,
				nvl(a.ie_sucesso,'S') ie_sucesso,
				a.nr_atendimento nr_Atendimento,
				a.nr_cirurgia nr_cirurgia,
				a.nr_seq_pepo nr_seq_pepo,
				nvl(b.ie_permite_disp_alta,'N') ie_permite_disp_alta,
				obter_se_exibe_graf_disp(a.nr_sequencia) ie_exige_disp_grafico
	from   	dispositivo b, 
				atend_pac_dispositivo a
	where 	a.nr_seq_dispositivo = b.nr_sequencia
	and		(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
				 ((nvl(nr_cirurgia_p,0) = 0) and (nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)) or
				 ((nvl(nr_cirurgia_p,0) = 0) and (nvl(nr_seq_pepo_p,0) = 0) and (( a.nr_atendimento = nr_atendimento_p)) or (
																										 a.nr_atendimento in (
																															select	a.nr_atendimento
																															from		atendimento_paciente a,
																																		atend_pac_dispositivo b
																															where		b.nr_atendimento 			= a.nr_atendimento
																															and 		a.cd_pessoa_fisica	 	= cd_pessoa_fisica_p
																															and		a.cd_estabelecimento 	= nvl(wheb_usuario_pck.get_cd_estabelecimento, 1)
																															and		a.dt_alta					is not null																										
																															and 		obter_se_dispo_usado_paciente(a.nr_atendimento) = 'S' 
																															and 		nvl(b.dt_retirada,sysdate) >= dt_atendimento_p
																										)
																										and nvl(b.ie_permite_disp_alta,'N') = 'S'
																									)))
	and   	((nvl(ie_em_andamento_p,'N') = 'N') or (nvl(ie_em_andamento_p,'N') = 'S' and (a.dt_retirada  is null)));
				
	
cursor	equipment is	
	select 	substr(d.ds_equipamento,1,80) NAME,
				substr(obter_dados_equipamento(c.cd_equipamento,'COR','S'),1,30) COLOR,
				c.dt_inicio dt_instalacao,
				(sysdate + 0.1) end_date,
				c.nr_sequencia code_chart,
				1 nr_seq_apres,
				c.dt_fim dt_fim,
				'S' ie_sucesso,
				c.nr_atendimento nr_Atendimento,
				c.nr_cirurgia nr_cirurgia,
				c.nr_seq_pepo nr_seq_pepo,
				null ie_permite_disp_alta
	from  	equipamento_cirurgia c,
				equipamento d
	where  	c.cd_equipamento = d.cd_equipamento
	and    	nvl(obter_dados_equipamento(c.cd_equipamento,'GD'),'S') = 'S'
	and   	((nvl(ie_em_andamento_p,'N') = 'N') or (nvl(ie_em_andamento_p,'N') = 'S' and (c.dt_fim  is null)))
	and    	c.dt_inicio is not null
	and		(((nvl(nr_cirurgia_p,0) > 0) and (c.nr_cirurgia = nr_cirurgia_p)) or 
				 ((nvl(nr_cirurgia_p,0) = 0) and (nvl(nr_seq_pepo_p,0) > 0) and (c.nr_seq_pepo = nr_seq_pepo_p)) or
				 ((nvl(nr_cirurgia_p,0) = 0) and (nvl(nr_seq_pepo_p,0) = 0) and (c.nr_atendimento = nr_atendimento_p)));
				

	
begin

if	(code_group_p = 'D') then
	open tubes_and_lines;
	loop
	fetch tubes_and_lines into	
		name_w,
		color_w,
		dt_instalacao_w,
		end_date_w,
		code_chart_w,
		nr_seq_apres_w,
		dt_fim_w,
		ie_sucesso_w,
		nr_atendimento_w,
		nr_cirurgia_w,
		nr_seq_pepo_w,
		ie_permite_disp_alta_w,
		ie_exige_disp_grafico_w;
	exit when tubes_and_lines%notfound;
		begin
		if	(ie_exige_disp_grafico_w = 'S') then
			get_values_r	:=	get_chart_tubes_lines_row(	name_w, 						--name
																		color_w,						--color
																		dt_instalacao_w,			--dt_instalacao
																		end_date_w,					--end_date
																		'D',							--code_group
																		code_chart_w,				--code_chart
																		nr_seq_apres_w,			--nr_seq_apres
																		dt_fim_w,					--dt_fim
																		ie_sucesso_w,				--ie_sucesso
																		nr_atendimento_w,			--nr_atendimento
																		nr_cirurgia_w,				--nr_cirurgia
																		nr_seq_pepo_w,				--nr_seq_pepo
																		ie_permite_disp_alta_w,	--ie_permite_disp_alta
																		'',							--description
																		null,							--max_scale
																		null,							--min_scale
																		'lines',						--style
																		null,							--shape
																		10,							--qt_size
																		'true',						--shaded
																		0,								--fixed_scale
																		null,							--fill_position
																		null);						--point_shape
			pipe row(get_values_r);										 
		end if;	
		end;
	end loop;
	close tubes_and_lines;
elsif (code_group_p = 'E') then
	open equipment;
	loop
	fetch equipment into	
		name_w,
		color_w,
		dt_instalacao_w,
		end_date_w,
		code_chart_w,
		nr_seq_apres_w,
		dt_fim_w,
		ie_sucesso_w,
		nr_atendimento_w,
		nr_cirurgia_w,
		nr_seq_pepo_w,
		ie_permite_disp_alta_w;
	exit when equipment%notfound;
		begin
		get_values_r := 	get_chart_tubes_lines_row	(name_w,						--name
																	color_w,						--color
																	dt_instalacao_w,			--dt_instalacao
																	end_date_w,					--end_date
																	'E',							--code_group
																	code_chart_w,				--code_chart
																	nr_seq_apres_w,			--nr_seq_apres
																	dt_fim_w,					--dt_fim
																	ie_sucesso_w,				--ie_sucesso	
																	nr_atendimento_w,			--nr_atendimento
																	nr_cirurgia_w,				--nr_cirurgia
																	nr_seq_pepo_w,				--nr_seq_pepo
																	ie_permite_disp_alta_w,	--ie_permite_disp_alta
																	'',							--description
																	null,							--max_scale
																	null,							--min_scale
																	'lines',						--style
																	null,							--shape
																	10,							--qt_size
																	'true',						--shaded
																	0,								--fixed_scale
																	null,							--fill_position
																	null);						--point_shape);
		pipe row(get_values_r);	
		
		end;
	end loop;
	close equipment;
end if;	

end search_chart_tubes_lines;
/