create or replace
function obter_resp_proj(nr_seq_projeto_p  number,
			nm_usuario_p  varchar2)
			return varchar2 is

ds_retorno_w		varchar2(2) := 'N';

begin

if	(nvl(nm_usuario_p,'X') <> 'X') and
	(nvl(nr_seq_projeto_p,0) > 0) then
	begin
	select	max(ds_retorno)
	into	ds_retorno_w
	from	(
		select	'S' ds_retorno -- se n�o existe registro
		from 	gpi_resp_projeto
		where	nr_seq_proj_gpi = nr_seq_projeto_p
		having	count(*) = 0
		union all
		select	'S' ds_retorno -- se existe para o usu�rio
		from 	gpi_resp_projeto
		where	nr_seq_proj_gpi = nr_seq_projeto_p
		and	nm_usuario_lib = nm_usuario_p
		and	rownum < 2
		union all
		select	'N' ds_retorno
		from	dual);

	if	(nvl(ds_retorno_w,'N') = 'N') then
		begin
		
		select	max(ds_retorno)
		into	ds_retorno_w
		from(	select	'S' ds_retorno -- se o usu�rio � o gestor funcional
			from	gpi_projeto c,
				usuario a
			where	a.cd_pessoa_fisica = c.cd_gestor_funcional
			and	c.nr_sequencia = nr_seq_projeto_p
			and	a.nm_usuario = nm_usuario_p
			union all
			select 'S' ds_retorno -- se o usu�rio � o gestor do projeto
			from	gpi_projeto c,
				usuario a
			where	a.cd_pessoa_fisica = c.cd_pf_gestor
			and	c.nr_sequencia = nr_seq_projeto_p
			and	a.nm_usuario = nm_usuario_p
			union all
			select 'N'
			from	dual);
		end;
	end if;
	end;
end if;

return ds_retorno_w;

end obter_resp_proj;
/
