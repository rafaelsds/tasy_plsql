create or replace
function cpoe_get_prior_prescriber(
				nr_seq_cpoe_previous_p 	number,
				nm_table_p				varchar2,
				ie_value_option_p 		varchar2)
			return varchar2 is

ds_retorno_w	varchar2(255);

begin

if ((nr_seq_cpoe_previous_p is not null) and (nm_table_p is not null)) then
	if (ie_value_option_p = 'N') then 
		Obter_valor_Dinamico_char_bv('select obter_nome_medico(obter_pf_usuario(nvl(nm_usuario_nrec,nm_usuario),''C''),''PMS'') from ' || nm_table_p|| ' where nr_sequencia = :nr_sequencia',
							'nr_sequencia=' || nr_seq_cpoe_previous_p || ';',
							ds_retorno_w);
	elsif (ie_value_option_p = 'D') then
		Obter_valor_Dinamico_char_bv('select cpoe_get_approval_time(coalesce(dt_atualizacao_nrec, dt_inicio, sysdate)) from ' || nm_table_p|| ' where nr_sequencia = :nr_sequencia',
							'nr_sequencia=' || nr_seq_cpoe_previous_p || ';',
							ds_retorno_w);
	end if;

end if;

return ds_retorno_w;

end cpoe_get_prior_prescriber;
/
