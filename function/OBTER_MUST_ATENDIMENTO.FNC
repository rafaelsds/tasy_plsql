create or replace
function Obter_MUST_Atendimento(nr_atendimento_p		number)
 		    	return number is

			
QT_PONTUACAO_w	number(10);			
			
Cursor C01 is
	select	QT_PONTUACAO	
	from	escala_must
	where	nr_atendimento	= nr_atendimento_p
	order by dt_avaliacao;
			
begin

open C01;
loop
fetch C01 into	
	QT_PONTUACAO_w;
exit when C01%notfound;
end loop;
close C01;

return	QT_PONTUACAO_w;

end Obter_MUST_Atendimento;
/