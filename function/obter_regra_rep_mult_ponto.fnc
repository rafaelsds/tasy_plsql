create or replace function obter_regra_rep_mult_ponto(	
							nr_seq_regra_pontuacao_p 	in mat_crit_rep_regra_mult.nr_seq_regra_rep_ponto%type,
							cd_estabelecimento_p 		in estabelecimento.cd_estabelecimento%type,
							cd_empresa_p 			in empresa.cd_empresa%type,
							cd_medico_p 			in mat_crit_rep_regra_mult.cd_medico%type,
							cd_especialidade_p		in mat_crit_rep_regra_mult.cd_especialidade%type,
							dt_referencia_p 		in date) return number is

vl_retorno_w	mat_crit_rep_regra_mult.vl_multiplo%type;

cursor	regras_multiplos is
select	nvl(vl_multiplo, 1) vl_multiplo
from	mat_crit_rep_regra_mult
where	nr_seq_regra_rep_ponto = nr_seq_regra_pontuacao_p
and	(cd_estabelecimento is null or cd_estabelecimento = cd_estabelecimento_p)
and	(nvl(cd_empresa, cd_empresa_p) = nvl(cd_empresa_p, cd_empresa))
and	(nvl(cd_medico, cd_medico_p)  = nvl(cd_medico_p, cd_medico))
and	(nvl(cd_especialidade, cd_especialidade_p) = nvl(cd_especialidade_p, cd_especialidade))
and	(
	(dt_inicio_vigencia is null and dt_fim_vigencia is null)
	or
	(dt_inicio_vigencia <= dt_referencia_p and dt_fim_vigencia is null)
	or
	(dt_inicio_vigencia is null and dt_referencia_p <= dt_fim_vigencia)
	or
	(dt_referencia_p   between dt_inicio_vigencia and dt_fim_vigencia)
);


begin

vl_retorno_w := 1;

for	regra in regras_multiplos loop
	vl_retorno_w := regra.vl_multiplo;
end loop;

return vl_retorno_w;

end obter_regra_rep_mult_ponto;
/