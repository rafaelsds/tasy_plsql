create or replace
function sac_obter_compl_grav_bo(	nr_seq_gravidade_p		number)
 		    	return varchar2 is

nr_seq_complexidade_w	number(10);
ds_retorno_w			varchar2(255);

begin
select	nr_seq_complexidade
into	nr_seq_complexidade_w
from	sac_gravidade
where	nr_sequencia = nr_seq_gravidade_p;

ds_retorno_w := nvl(substr(sac_obter_complexidade(nr_seq_complexidade_w),1,255),'');

return	ds_retorno_w;

end sac_obter_compl_grav_bo;
/