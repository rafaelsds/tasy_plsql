create or replace 
function converte_numero_hexadecimal( nr_valor_p in number ) 
			return varchar2 is
begin
	return to_base(nr_valor_p, 16 );
end converte_numero_hexadecimal;
/