create or replace
function obter_Posicao_item_nota_ipe (	nr_seq_item_p		number,
				nr_interno_conta_p		number,
				nr_seq_protocolo_p		number,
				cd_item_convenio_p		number,
				ie_opcao_p		varchar2)
				return number as
/*
IE_OPCAO_P:
F-Folha
L-Linha
N-Nota
*/
nr_retorno_w		number(15) := 0;
					
begin

if	(ie_opcao_p = 'F') then
	begin
	
	begin
	select	nvl(max(nr_folha_item),0)
	into	nr_retorno_w
	from	w_posicao_item_nota_ipe
	where	nr_seq_item	= nr_seq_item_p
	and	nr_interno_conta	= nr_interno_conta_p
	and	cd_item_convenio	= cd_item_convenio_p
	and	nr_seq_protocolo	= nr_seq_protocolo_p;
	exception
	when others then
		nr_retorno_w := 0;
	end;
	
	end;
elsif	(ie_opcao_p = 'L') then
	begin
	
	begin
	select	nvl(max(nr_linha_item),0)
	into	nr_retorno_w
	from	w_posicao_item_nota_ipe
	where	nr_seq_item	= nr_seq_item_p
	and	nr_interno_conta	= nr_interno_conta_p
	and	cd_item_convenio	= cd_item_convenio_p
	and	nr_seq_protocolo	= nr_seq_protocolo_p;
	exception
	when others then
		nr_retorno_w := 0;
	end;
	
	end;
elsif	(ie_opcao_p = 'N') then
	begin
	
	begin
	select	nvl(max(nr_nota_item),0)
	into	nr_retorno_w
	from	w_posicao_item_nota_ipe
	where	nr_seq_item	= nr_seq_item_p
	and	nr_interno_conta	= nr_interno_conta_p
	and	cd_item_convenio	= cd_item_convenio_p
	and	nr_seq_protocolo	= nr_seq_protocolo_p;
	exception
	when others then
		nr_retorno_w := 0;
	end;
	
	end;	
end if;

return nr_retorno_w;

end obter_Posicao_item_nota_ipe;
/