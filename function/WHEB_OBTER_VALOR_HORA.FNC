create or replace
function wheb_obter_valor_hora(qt_horas_p		number,
				ie_opcao_p		varchar2)
 		    	return number is

/*
ie_opcao_p

essa function tem por finalidade receber um valor X em horas
Exmeplo:
2,98194444444444 horas

o valor antes da v�rgula deve ser horas
o valor ap�s a v�rgula ser� transformado em minutos

sempre haver� uma diferen�a de minutos ou segundos.

o retorno do exemplo ser�:

2:59:00

H - horas
M - Minutos
*/
vl_horas_w		number(10);
retorno_w		number(10);	
vl_minutos_ww		number(10);
vl_minutos_w		number(10);	
begin



if	(ie_opcao_p	=	'H') then
	begin
	for	i in 1 .. length(qt_horas_p) loop
		begin
		if	substr(qt_horas_p,i,1) = ',' then
			vl_horas_w	:= substr(qt_horas_p,1,i-1);
		end if;
		end;
	end loop;
	
	end;
end if;


if	(ie_opcao_p	=	'M') then
	begin
	for	i in 1 .. length(qt_horas_p) loop
		begin
		if	substr(qt_horas_p,i,1) = ',' then
			begin
			vl_minutos_w	:= substr(qt_horas_p,i+1,2);
			
			select trunc((vl_minutos_w	* 61) / 100)
			into	vl_minutos_ww
			from	dual;
			
			end;
		end if;
		end;
	end loop;
	
	end;
end if;



if	(ie_opcao_p	=	'H') then
	retorno_w	:=	vl_horas_w;
end if;
if	(ie_opcao_p	=	'M') then
	retorno_w	:=	vl_minutos_ww;
end if;

return	retorno_w;

end wheb_obter_valor_hora;
/