create or replace
function obter_desc_opcao_card(nr_sequencia_p		varchar2)
 		    	return varchar2 is

ds_opcao_w	 varchar2(255);

begin

select	max(ds_opcao)
into	ds_opcao_w
from	mapa_dieta_opcao_cardapio 
where	nr_sequencia = nr_sequencia_p;

return	ds_opcao_w;

end obter_desc_opcao_card;
/