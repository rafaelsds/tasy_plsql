create or replace
function obter_se_dieta_acomp(	nr_atendimento_p	number,
				dt_servico_p		date)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);
			
begin

select 	decode(nvl(max(a.qt_dieta_acomp),0),0,'N','S')
into	ds_retorno_w
from	atend_categoria_convenio a
where	a.nr_atendimento = nr_atendimento_p
and	a.nr_seq_interno = (	select	max(b.nr_seq_interno)
				from	atend_categoria_convenio b
				where	b.nr_atendimento = a.nr_atendimento
				and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(b.dt_inicio_vigencia) <= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_servico_p));

return	ds_retorno_w;

end obter_se_dieta_acomp;
/