create or replace
function pls_obter_se_item_prospect
			(	cd_perfil_p	number,
				nr_seq_item_p	number)
 		    	return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter se o item est� liberado para visualiza��o para o perfil.
Cadastro realizado em Administra��o do Sistema Tasy / Par�metros / Par�metros OPS / OPS - Gest�o Comercial / Prospec��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w	varchar2(255);

begin

select	max(b.ie_controle)
into	ds_retorno_w
from	pls_prospeccao_item_perfil	b,
	pls_itens_prospeccao		a
where	b.nr_seq_item		= a.nr_sequencia
and	a.nr_sequencia		= nr_seq_item_p
and	b.cd_perfil		= cd_perfil_p
and	a.ie_situacao		= 'A';
ds_retorno_w	:= nvl(ds_retorno_w,'N');

return	ds_retorno_w;

end pls_obter_se_item_prospect;
/