create or replace
function obter_fornecedor_prod_conc(
			nr_sequencia_p	number)
 		    	return varchar2 is

ds_fornecedor_w	varchar2(80);
begin
if	(nr_sequencia_p is not null) then
	begin
	
	select 	substr(obter_nome_pf_pj(null,cd_cnpj),1,255)
	into	ds_fornecedor_w
	from	com_prod_concorrente
	where	nr_sequencia	= nr_sequencia_p;
	end;
end if;
return	ds_fornecedor_w;
end obter_fornecedor_prod_conc;
/