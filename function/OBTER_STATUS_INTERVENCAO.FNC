
create or replace
function Obter_Status_Intervencao(	nr_prescricao_p		number,
					nr_sequencia_p		number,
					nr_seq_horario_p	number)
 		    	return varchar2 is

ie_status_w			varchar2(1) := 'N';

begin

if	(nr_prescricao_p is not null) and
	(nvl(nr_sequencia_p,nr_seq_horario_p) is not null) then

	select	nvl(max(ie_suspenso),'N')
	into	ie_status_w
	from	pe_prescr_proc
	where	nr_seq_prescr = nr_prescricao_p
	and	nr_sequencia = nr_sequencia_p;

	if	(ie_status_w = 'N') and
		(nr_seq_horario_p is not null) then
		select	decode(nvl(max(a.ie_alteracao),0),55,'Z','N')
		into	ie_status_w
		from	prescr_mat_alteracao a
		where	a.nr_prescricao = nr_prescricao_p
		and	a.nr_seq_horario_sae = nr_seq_horario_p
		and	nvl(a.ie_evento_valido,'S') = 'S'
		and	ie_tipo_item = 'E'
		and	a.ie_alteracao <> 57
		and	((a.ie_alteracao <> 56) or
			 not exists( 	select		1
					 from		prescr_mat_alteracao b
					 where		a.nr_prescricao = b.nr_prescricao
					 and		a.nr_seq_horario_sae = b.nr_seq_horario_sae
					 and		a.cd_item = b.cd_item
					 and		b.ie_alteracao in (4)
					 and		b.nr_sequencia > a.nr_sequencia));
	end if;
end if;

if	(ie_status_w = 'N') then
	ie_status_w	:= null;
end if;

return	ie_status_w;

end Obter_Status_Intervencao;
/