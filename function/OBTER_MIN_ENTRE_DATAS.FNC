CREATE OR REPLACE
FUNCTION Obter_Min_Entre_Datas
			(	dt_inicial_p		date,
				dt_final_p			date,
				qt_min_ref_p		Number)
		            RETURN 			Number deterministic IS

qt_minuto_w			Number(15,0);
qt_min_ref_w		Number(3,0);

BEGIN

if	(dt_inicial_p is null) or
	(dt_final_p	is null) then
	qt_minuto_w		:= 0;
else
	begin
	qt_min_ref_w	:= nvl(qt_min_ref_p,1);
	qt_minuto_w		:= round((dt_final_p - dt_inicial_p) * 1440);
	qt_minuto_w		:= round(qt_minuto_w / qt_min_ref_w) * qt_min_ref_w;
	end;
end if;

return qt_minuto_w;

END Obter_Min_Entre_Datas;
/
