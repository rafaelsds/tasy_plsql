create or replace 
function Obter_valor_Orig_Proc(
		nr_sequencia_p	Number,
		ie_opcao_p		Varchar2)
		return Number is

vl_origem_w		Number(15,2);

BEGIN
select nvl(max(vl_procedimento),0)
into	vl_origem_w
from 	proc_paciente_valor
where	nr_seq_procedimento	= nr_sequencia_p
  and	ie_tipo_valor		= 1;

if	(vl_origem_w = 0) then
	select vl_procedimento
	into	vl_origem_w
	from procedimento_paciente
	where nr_sequencia	= nr_sequencia_p;
end if;
RETURN vl_origem_w;
end Obter_valor_Orig_Proc;
/
