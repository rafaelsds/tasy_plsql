create or replace
function pls_obter_dados_lanc_prog
			(	nr_seq_lancamento_mens_p	number,
				ie_tipo_valor_p			varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
ie_operacao_w		varchar2(1);

begin

if	(ie_tipo_valor_p = 'VL') then
	select	decode(ie_operacao, 'D', vl_lancamento*-1, vl_lancamento)
	into	ds_retorno_w
	from	(select	max(a.vl_lancamento) vl_lancamento,
			max(b.ie_operacao_motivo) ie_operacao
		from	pls_lancamento_mensalidade a,
			pls_tipo_lanc_adic b
		where	b.nr_sequencia = a.nr_seq_motivo
		and	a.nr_sequencia = nr_seq_lancamento_mens_p);
	
	ds_retorno_w	:= campo_mascara_virgula(ds_retorno_w);
end if;

return	ds_retorno_w;

end pls_obter_dados_lanc_prog;
/