create or replace
function Qt_Obter_Seq_Pac_Seq_Atend(
				nr_Seq_atendimento_p	number,
				nm_usuario_p		varchar2)
 		    	return number is

vl_retorno_w		number(10);
nr_seq_paciente_w	number(10);
qt_convenio_w		number(10);
			
begin

select	max(nr_seq_paciente)
into	vl_retorno_w
from	paciente_atendimento
where	nr_seq_atendimento	= nr_seq_atendimento_p;

return	vl_Retorno_w;

end Qt_Obter_Seq_Pac_Seq_Atend;
/