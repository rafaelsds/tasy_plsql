create or replace
function PLS_OBTER_DADOS_AJUSTE_FAT
		(nr_seq_ajuste_fat_p	number,
		 ie_opcao_p		varchar2) return varchar2 is

/*
SP	- Seq do pagador da fatura original
NP	- Nome do pagador original
MC	- M�s de compet�ncia
VF	- Valor fatura original
VC	- Valor das contas a ajustar
VA	- Valor ajustado
VFS	- Valor refaturado sem ajuste
SFS	- Sequencia da fatura sem ajuste
*/

ds_retorno_w		varchar2(255);
nr_seq_pagador_w	number(10,0);

begin

select	max(b.nr_seq_pagador)
into	nr_seq_pagador_w
from	pls_fatura b,
	pls_ajuste_fatura a
where	a.nr_seq_fatura	= b.nr_sequencia
and	a.nr_sequencia	= nr_seq_ajuste_fat_p;

if	(ie_opcao_p	= 'SP') then
	ds_retorno_w	:= nr_seq_pagador_w;
elsif	(ie_opcao_p	= 'NP') then
	ds_retorno_w	:= pls_obter_dados_pagador(nr_seq_pagador_w,'NF');
elsif	(ie_opcao_p	= 'MC') then
	select	max(c.dt_mesano_referencia)
	into	ds_retorno_w
	from	pls_lote_faturamento c,
		pls_fatura b,
		pls_ajuste_fatura a
	where	a.nr_seq_fatura	= b.nr_sequencia
	and	b.nr_seq_lote	= c.nr_sequencia
	and	a.nr_sequencia	= nr_seq_ajuste_fat_p;
elsif	(ie_opcao_p	= 'VF') then
	select	nvl(max(b.vl_fatura),0) + nvl(max(b.vl_total_ndc),0)
	into	ds_retorno_w
	from	pls_fatura b,
		pls_ajuste_fatura a
	where	a.nr_seq_fatura	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_ajuste_fat_p;
elsif	(ie_opcao_p	= 'VC') then
	select	sum(b.vl_faturado + b.vl_faturado_ndc)
	into	ds_retorno_w
	from	pls_fatura_evento d,
		pls_fatura_conta b,
		pls_ajuste_fatura_conta a,
		pls_ajuste_fatura c
	where	c.nr_sequencia	= a.nr_seq_ajuste_fatura
	and	a.nr_seq_conta	= b.nr_seq_conta
	and	d.nr_sequencia	= b.nr_seq_fatura_evento
	and	d.nr_seq_fatura	= c.nr_seq_fatura
	and	c.nr_sequencia	= nr_seq_ajuste_fat_p;
elsif	(ie_opcao_p	= 'VA') then
	select	sum(b.vl_beneficiario)
	into	ds_retorno_w
	from	pls_conta_pos_estabelecido b,
		pls_ajuste_fatura_conta a
	where	a.nr_seq_conta	= b.nr_seq_conta
	and	a.nr_seq_ajuste_fatura	= nr_seq_ajuste_fat_p
	and	((b.ie_situacao		= 'A') or (b.ie_situacao	is null))
	and	b.ie_status_faturamento in ('P','L');
elsif	(ie_opcao_p	= 'VFS') then
	select	max(e.vl_fatura)
	into	ds_retorno_w
	from	pls_fatura e,
		pls_ajuste_fatura a
	where	e.nr_seq_fatura_origem	= a.nr_seq_fatura
	and	a.nr_sequencia		= nr_seq_ajuste_fat_p;
elsif	(ie_opcao_p	= 'SFS') then
	select	max(e.nr_sequencia)
	into	ds_retorno_w
	from	pls_fatura e,
		pls_ajuste_fatura a
	where	e.nr_seq_fatura_origem	= a.nr_seq_fatura
	and	a.nr_sequencia		= nr_seq_ajuste_fat_p;
end if;

return	ds_retorno_w;

end PLS_OBTER_DADOS_AJUSTE_FAT;
/