create or replace
function pls_obter_dados_auditor
				(	cd_ocorrencia_p		varchar2,
					nr_seq_glosa_p		Number,
					nm_usuario_p		varchar2,					
					ie_tipo_p		varchar2,
					nr_seq_grupo_p		Number) 
				return varchar2 is

/*	ie_tipo_p
	L - Nivel de libera��o (Retorna nivel do auditor no grupo)
	A - Se auditor  grupo(Retorna S para Sim e N para N�o)
	C - Se auditor ocorr�ncia
		Retorna T Se n�o possue grupo na ocorrencia
		Retorna S Se o auditor, neste grupo, � parte do grupo de an�lise da ocorr�ncia
		Retorna N Se n�o faz parte
*/

ds_retorno_w		varchar2(255);
nr_seq_nivel_lib_w	number(10);
ie_permissao_w		varchar2(2);
nr_seq_ocorrencia_w	number(10);
qt_grupo_w		Number(10);

begin

if	(ie_tipo_p = 'L') then


	select	max(nvl(a.nr_seq_nivel_lib,b.nr_seq_nivel_lib))
	into	nr_seq_nivel_lib_w
	from	pls_membro_grupo_aud a,
		pls_grupo_auditor b	
	where	a.nr_seq_grupo = nr_seq_grupo_p
	and	a.nm_usuario_exec = nm_usuario_p
	and	a.nr_seq_grupo = b.nr_sequencia;	
	
	if	(nr_seq_nivel_lib_w is not null) then
		
		begin
		select	nr_nivel_liberacao
		into	ds_retorno_w
		from	pls_nivel_liberacao
		where	nr_sequencia = nr_seq_nivel_lib_w;		
		exception
		when others then
			ds_retorno_w := null;
		end;
		
		goto final;
	end if;

elsif	(ie_tipo_p = 'A') then

	select	max(ie_permissao)
	into	ie_permissao_w
	from	pls_grupo_auditor
	where	nr_sequencia = nr_seq_grupo_p;	

	/*Se o usu�rio for master ent�o � retornado que este faz pasrte do grupo de an�lise*/
	if	(ie_permissao_w in('T','R')) then -- tratamento realizado para o tipo de permiss�o Acesso total com restri��es  - Demitrius  07/02/2011
		ds_retorno_w := 'S';
	else			
		begin
		select	nr_sequencia
		into	ds_retorno_w
		from	pls_membro_grupo_aud
		where	nr_seq_grupo = nr_seq_grupo_p
		and	nm_usuario_exec = nm_usuario_p
		and	ie_situacao = 'A';		
		exception
		when others then
			ds_retorno_w := null;
		end;
		
		/*Se encontrou um registro n�o � necess�rio continuar*/
		if	(ds_retorno_w is not null) then		
			ds_retorno_w := 'S';	
			goto final;
		end if;			
		
		ds_retorno_w := 'N';
	end if;
	
elsif	(ie_tipo_p = 'C') then
	
	select	max(ie_permissao)
	into	ie_permissao_w
	from	pls_grupo_auditor
	where	nr_sequencia = nr_seq_grupo_p;	
	
	if	(ie_permissao_w in('T','R')) then
		ds_retorno_w := 'S';
		goto final;
	end if;		  
	
	select	max(nr_sequencia)
	into	nr_seq_ocorrencia_w
	from	pls_ocorrencia
	where	cd_ocorrencia = cd_ocorrencia_p;	
	
	select  count(1)
	into	qt_grupo_w
	from 	pls_ocorrencia_grupo
	where 	nr_seq_ocorrencia = nr_seq_ocorrencia_w
	and	ie_situacao = 'A';
	
	if	(qt_grupo_w = 0) then
		ds_retorno_w := 'T';
	else	
		select	decode(count(a.nr_sequencia),0,'N','S')
		into	ds_retorno_w
		from	pls_membro_grupo_aud a,
			pls_ocorrencia_grupo b
		where	a.nr_seq_grupo			= b.nr_seq_grupo
		and	nvl(a.nm_usuario_exec,'X') 	= nm_usuario_p
		and	a.nr_seq_grupo		 	= nr_seq_grupo_p 
		and 	b.nr_seq_ocorrencia 		= nr_seq_ocorrencia_w
		and	b.ie_situacao = 'A'
		and	a.ie_situacao = 'A';
	end if;

end if;

<<final>>
return ds_retorno_w;

end pls_obter_dados_auditor;
/
