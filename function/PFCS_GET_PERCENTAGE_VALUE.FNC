create or replace
function pfcs_get_percentage_value(	vl_dividend_p		number,
					vl_divisor_p		number)
					return number is

vl_result_w		number(10);

begin

if	(nvl(vl_divisor_p,0) = 0) then
	vl_result_w	:= 0;
else
	vl_result_w	:= (nvl(vl_dividend_p,0) / vl_divisor_p) * 100;
end if;

return	vl_result_w;

end pfcs_get_percentage_value;
/