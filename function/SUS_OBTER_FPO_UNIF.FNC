create or replace 
function sus_obter_fpo_unif( 	cd_procedimento_p		number,
			ie_origem_proced_p	number,
			ie_tipo_atendimento_p	number,
			dt_vigencia_p		date,
			cd_cbo_p		varchar2,
			ie_tipo_p			number,
			cd_estabelecimento_p	number,
			nr_aih_p			number,
			cd_setor_atendimento_p	number,
			cd_procedencia_p	number,
			ie_carater_inter_sus_p	varchar2)
			return number is

/* IE_TIPO_P
	1 - Fisico
	2 - Orcamentario
	3 - Sequencia
*/

nr_seq_grupo_w			sus_fpo_regra.nr_seq_grupo%type;
nr_seq_subgrupo_w		sus_fpo_regra.nr_seq_subgrupo%type;
nr_seq_forma_org_w		sus_fpo_regra.nr_seq_forma_org%type;
qt_fisico_w			sus_fpo_regra.qt_fisico%type;
vl_orcamentario_w		sus_fpo_regra.vl_orcamento%type;
ds_retorno_w			number(12,2);
nr_sequencia_w			sus_fpo_regra.nr_sequencia%type;
ie_tipo_financiamento_w		sus_fpo_regra.ie_tipo_financiamento%type;
ie_tipo_financ_proc_w		sus_fpo_regra.ie_tipo_financiamento%type;
ie_complexidade_w		sus_fpo_regra.ie_complexidade%type;
cd_carater_internacao_w		sus_fpo_regra.cd_carater_internacao%type;


cursor cursor01(cd_procedimento_pp              sus_fpo_regra.cd_procedimento%type,
                ie_origem_proced_pp             sus_fpo_regra.ie_origem_proced%type,
                cd_setor_atendimento_pp         setor_atendimento.cd_setor_atendimento%type,
                nr_seq_grupo_pp                 sus_fpo_regra.nr_seq_grupo%type,
                nr_seq_subgrupo_pp              sus_fpo_regra.nr_seq_subgrupo%type,
                nr_seq_forma_org_pp             sus_fpo_regra.nr_seq_forma_org%type,
                ie_tipo_atendimento_pp          sus_fpo_regra.ie_tipo_atendimento%type,
                cd_procedencia_pp               sus_fpo_regra.cd_procedencia%type,
                ie_tipo_financiamento_pp        sus_fpo_regra.ie_tipo_financiamento%type,
                ie_complexidade_pp              sus_fpo_regra.ie_complexidade%type,
                cd_cbo_pp                       sus_fpo_regra.cd_cbo%type,
                cd_estabelecimento_pp           sus_fpo_regra.cd_estabelecimento%type,
                dt_vigencia_pp                  sus_fpo_regra.dt_competencia%type,
                cd_carater_internacao_pp        sus_fpo_regra.cd_carater_internacao%type) is
	select	nvl(a.qt_fisico,0) qt_fisico,
		nvl(a.vl_orcamento,0) vl_orcamento,
		a.nr_sequencia
	from	sus_fpo_regra a
	where	((nvl(sus_obter_fpo_regra_proc(cd_procedimento_pp,ie_origem_proced_p,a.nr_sequencia),'N') = 'S') or
		((nvl(a.ie_origem_proced,ie_origem_proced_pp)		= ie_origem_proced_pp) and
		(nvl(a.cd_procedimento,cd_procedimento_pp)		= cd_procedimento_pp)))
	and	(nvl(sus_obter_fpo_regra_setor(cd_setor_atendimento_pp,a.nr_sequencia),'N') = 'S')
	and	nvl(a.nr_seq_grupo,nr_seq_grupo_pp)			= nr_seq_grupo_pp
	and	nvl(a.nr_seq_subgrupo,nr_seq_subgrupo_pp)		= nr_seq_subgrupo_pp
	and	nvl(a.nr_seq_forma_org,nr_seq_forma_org_pp)		= nr_seq_forma_org_pp
	and	obter_se_contido(ie_tipo_atendimento_pp,nvl(a.ie_tipo_atendimento,ie_tipo_atendimento_pp)) = 'S'
	and	obter_se_contido(cd_procedencia_pp,nvl(a.cd_procedencia,cd_procedencia_pp)) = 'S'
	and	nvl(a.ie_tipo_financiamento,ie_tipo_financiamento_pp) 	= ie_tipo_financiamento_pp
	and	nvl(a.ie_complexidade,ie_complexidade_pp)		= ie_complexidade_pp
	and	((nvl(sus_obter_fpo_cbo(nvl(cd_cbo_pp,'X'),a.nr_sequencia),'N')	= 'S') or
		(nvl(a.cd_cbo,nvl(cd_cbo_pp,'X'))				= nvl(cd_cbo_pp,'X')))
	and	a.cd_estabelecimento				= cd_estabelecimento_pp
	and	establishment_timezone_utils.startOfMonth(a.dt_competencia)			= establishment_timezone_utils.startOfMonth(dt_vigencia_pp)
	and	nvl(a.cd_carater_internacao,cd_carater_internacao_pp)	= cd_carater_internacao_pp
	and	not exists (	select	1
				from	sus_fpo_regra_desconsid x
				where	x.nr_seq_fpo_regra	= a.nr_sequencia
				and	nvl(a.cd_cbo,'0')	= nvl(x.cd_cbo,nvl(a.cd_cbo,'0'))
				and	a.cd_procedimento	= nvl(x.cd_procedimento,a.cd_procedimento)
				and	a.ie_origem_proced	= nvl(x.ie_origem_proced,a.ie_origem_proced)
				and	a.nr_seq_forma_org	= nvl(x.nr_seq_forma_org,a.nr_seq_forma_org)
				and	a.nr_seq_grupo	= nvl(x.nr_seq_grupo,a.nr_seq_grupo)
				and	a.nr_seq_subgrupo	= nvl(x.nr_seq_subgrupo,a.nr_seq_subgrupo))
	and	nvl(a.ie_situacao,'A') = 'A'
	order by nvl(cd_procedimento,0),
		 nvl(nr_seq_forma_org,0),
		 nvl(nr_seq_subgrupo,0),
		 nvl(nr_seq_grupo,0),
		 dt_competencia;
                 
cursor01_w      cursor01%rowtype;

begin

begin
select	ie_tipo_financiamento,
	ie_complexidade,
        c.nr_seq_grupo,
        b.nr_seq_subgrupo,
        a.nr_seq_forma_org
into	ie_tipo_financ_proc_w,
	ie_complexidade_w,
        nr_seq_grupo_w,
	nr_seq_subgrupo_w,
	nr_seq_forma_org_w
from	sus_subgrupo c,
        sus_forma_organizacao b,
        sus_procedimento a
where	a.nr_seq_forma_org      = b.nr_sequencia
and     b.nr_seq_subgrupo       = c.nr_sequencia
and     cd_procedimento         = cd_procedimento_p
and	ie_origem_proced        = ie_origem_proced_p;
exception
	when others then
        ie_tipo_financ_proc_w   := 'X';
        ie_complexidade_w       := 'X';
	nr_seq_grupo_w		:= null;
	nr_seq_subgrupo_w	:= null;
	nr_seq_forma_org_w	:= null;
end;

if	(nvl(nr_aih_p,0) = 0) then
	begin
	cd_carater_internacao_w := nvl(ie_carater_inter_sus_p,'00');
	ie_tipo_financiamento_w	:= ie_tipo_financ_proc_w;
	end;
else
	begin

	begin
	select	nvl(max(cd_carater_internacao),'00'),
		nvl(max(cd_carater_internacao),ie_tipo_financ_proc_w)
	into	cd_carater_internacao_w,
		ie_tipo_financiamento_w
	from	sus_aih_unif
	where	nr_aih = nr_aih_p;
	exception
	when others then
		cd_carater_internacao_w := '00';
		ie_tipo_financiamento_w	:= ie_tipo_financ_proc_w;
	end;

	end;
end if;

for  	cursor01_w in cursor01( cd_procedimento_p,
                                ie_origem_proced_p,
                                cd_setor_atendimento_p,
                                nr_seq_grupo_w,
                                nr_seq_subgrupo_w,
                                nr_seq_forma_org_w,
                                ie_tipo_atendimento_p,
                                cd_procedencia_p,
                                ie_tipo_financiamento_w,
                                ie_complexidade_w,
                                cd_cbo_p,
                                cd_estabelecimento_p,
                                dt_vigencia_p,
                                cd_carater_internacao_w) loop
        begin
        qt_fisico_w             := cursor01_w.qt_fisico;
	vl_orcamentario_w       := cursor01_w.vl_orcamento;
	nr_sequencia_w          := cursor01_w.nr_sequencia;
        
        if	(ie_tipo_p	= 1) then
		ds_retorno_w	:= qt_fisico_w;
	elsif	(ie_tipo_p	= 2) then
		ds_retorno_w	:= vl_orcamentario_w;
	elsif	(ie_tipo_p	= 3) then
		ds_retorno_w	:= nr_sequencia_w;
	end if;
        
        end;
end loop;

return	ds_retorno_w;

end sus_obter_fpo_unif;
/