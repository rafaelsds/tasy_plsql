/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter a descrição da classificação do pagador, conforme itens pagos.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_classif_item_pag
		(	nr_seq_classif_item_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255)	:= null;
			
begin

if	(nvl(nr_seq_classif_item_p,0) <> 0) then

	select	ds_classificacao
	into	ds_retorno_w
	from	pls_classif_itens_pagador
	where	nr_sequencia = nr_seq_classif_item_p;
	
end if;

return	ds_retorno_w;

end pls_obter_classif_item_pag;
/