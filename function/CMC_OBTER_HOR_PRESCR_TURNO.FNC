create or replace 
function CMC_obter_hor_prescr_turno
			(nr_prescricao_p	Number,
			nr_sequencia_p		Number,
			ie_opcao_p		varchar2) 
			return varchar2 is
/*
	ie_opcao_p	
 T - Tarde
 M - Manh�
 N - Noite
 A - Ambos
*/			
ds_retorno_w		varchar2(255);
cd_setor_atendimento_w	Number(5);
cd_estabelecimento_w	Number(4);
hr_prim_turno_w		varchar2(5);
hr_seg_turno_w		varchar2(5);
hr_ter_turno_w		varchar2(5);
ds_horarios_w		varchar2(2000);
ds_horarios_dif_w	varchar2(2000);
qt_manha_w		varchar2(2000);
qt_tarde_w		varchar2(2000);
qt_noite_w		varchar2(2000);
ds_hora_w		varchar2(20);
qt_unitaria_w		Number(15,3);
ie_agupador_w		Number(2);
nr_sequencia_diluicao_w	Number(6);
k			Integer;
qt_flag_w		Number(5) := 0;

begin

select	nvl(cd_setor_atendimento,obter_setor_atendimento(nr_atendimento)),
	cd_estabelecimento
into	cd_setor_atendimento_w,
	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

begin
select	hr_prim_turno,
	hr_seg_turno,
	hr_ter_turno
into	hr_prim_turno_w,
	hr_seg_turno_w,
	hr_ter_turno_w
from	regra_turno_prescr
where	cd_estabelecimento	= cd_estabelecimento_w
and	nr_sequencia		= (	select	min(b.nr_sequencia)
					from	regra_turno_prescr b
					where	cd_setor_atendimento = cd_setor_atendimento_w);
exception
	when others then
		hr_prim_turno_w := null;
end;

if	(hr_prim_turno_w is null) then
	begin
	select	hr_prim_turno,
		hr_seg_turno,
		hr_ter_turno
	into	hr_prim_turno_w,
		hr_seg_turno_w,
		hr_ter_turno_w
	from	regra_turno_prescr
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	nr_sequencia		= (	select	min(b.nr_sequencia)
						from	regra_turno_prescr b
						where	cd_setor_atendimento is null);
	exception
		when others then
			hr_prim_turno_w := null;
	end;	
end if;

if	(hr_prim_turno_w is not null) then
	select	padroniza_horario_prescr(ds_horarios, '00:00'),
		ds_horarios,
		qt_unitaria,
		ie_agrupador,
		nr_sequencia_diluicao
	into	ds_horarios_w,
		ds_horarios_dif_w,
		qt_unitaria_w,
		ie_agupador_w,
		nr_sequencia_diluicao_w
	from	prescr_material
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_sequencia_p;
	
	if	(ie_agupador_w in (3,7,9)) then
		select	padroniza_horario_prescr(ds_horarios, '00:00'),
			ds_horarios
		into	ds_horarios_w,
			ds_horarios_dif_w
		from	prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_sequencia_diluicao_w;
	end if;
	
	if	(ds_horarios_w is null) then	
		ds_retorno_w	:= ds_horarios_dif_w;
	else
		ds_horarios_w	:= replace(ds_horarios_w, 'A','');
		while	ds_horarios_w is not null LOOP
			begin
			select	instr(ds_horarios_w, ' ') 
			into	k 
			from	dual;
			if	(k > 1) and
				(substr(ds_horarios_w, 1, k -1) is not null) then
				ds_hora_w		:= substr(ds_horarios_w, 1, k-1);
				ds_hora_w		:= replace(ds_hora_w, ' ','');
				ds_horarios_w		:= substr(ds_horarios_w, k + 1, 2000);
			elsif	(ds_horarios_w is not null) then
				ds_hora_w 		:= replace(ds_horarios_w,' ','');
				ds_horarios_w		:= '';
			end if;

			if	(hr_ter_turno_w is not null) and
				((ds_hora_w >= hr_ter_turno_w) or
				 (ds_hora_w < hr_prim_turno_w)) then
				qt_noite_w	:= qt_noite_w || ds_hora_w || ' ';
			elsif	(hr_seg_turno_w is not null) and
				(ds_hora_w >= hr_seg_turno_w) and
				(ds_hora_w < hr_ter_turno_w) then
				qt_tarde_w	:= qt_tarde_w || ds_hora_w || ' ';
			elsif	(hr_prim_turno_w is not null) then
				qt_manha_w	:= qt_manha_w || ds_hora_w || ' ';
			end if;
			
			qt_flag_w	:= qt_flag_w + 1;
			if	(qt_flag_w > 30) then	
				ds_horarios_w	:= '';
			end if;
			end;
		END LOOP;
		
		if	(nvl(ie_opcao_p,'A') = 'A') then
			ds_retorno_w	:= 'T '|| qt_tarde_w ||' - N '|| qt_noite_w ||' - M '|| qt_manha_w;
		elsif	(nvl(ie_opcao_p,'A') = 'T') then
			ds_retorno_w	:= 'T '|| qt_tarde_w;
		elsif	(nvl(ie_opcao_p,'A') = 'N') then
			ds_retorno_w	:= 'N '|| qt_noite_w;
		else	
			ds_retorno_w	:= 'M '|| qt_manha_w;
		end if;
		
	end if;	
end if;

return ds_retorno_w;

end CMC_obter_hor_prescr_turno;
/
