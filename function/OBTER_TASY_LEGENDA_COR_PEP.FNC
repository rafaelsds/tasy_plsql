create or replace
function obter_tasy_legenda_cor_pep(	nr_seq_schematic_p	number,
										nr_seq_legenda_p	number,
										ie_opcao_p			varchar2)
		return varchar2 is


ds_resultado_w			varchar2(15)	:= '';
ds_cor_html_w			varchar2(15)	:= '';
cd_estabelecimento_w	number(4);
cd_perfil_w				number(10);
nr_seq_padrao_w			number(10);


BEGIN

cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w	:= obter_perfil_ativo;

Select 	max(C.DS_COR_HTML),
		max(c.nr_sequencia)
into	ds_cor_html_w,
		nr_seq_padrao_w
from   	OBJETO_SCHEMATIC a,
		OBJETO_SCHEMATIC_LEGENDA b,
		tasy_padrao_cor c,
		regra_condicao d,
		regra_condicao_item e
where  	a.nr_Sequencia = nr_seq_schematic_p
and   	 b.NR_SEQ_OBJETO = a.nr_sequencia
and    	b.NR_SEQ_LEGENDA = nr_seq_legenda_p
and    	c.NR_SEQ_LEGENDA = b.NR_SEQ_LEGENDA
and    	d.NR_SEQ_LEGENDA = c.NR_SEQUENCIA
and    	e.nr_seq_regra = d.nr_Sequencia
and    	e.ie_valor = ie_opcao_p;


select	nvl(max(ds_cor_html),ds_cor_html_w)
into	ds_resultado_w
from	tasy_padrao_cor_perfil
where	cd_estabelecimento	= cd_estabelecimento_w
and		nr_seq_padrao		= nr_seq_padrao_w
and		cd_perfil		= cd_perfil_w;


return ds_resultado_w;

END obter_tasy_legenda_cor_pep;
/
