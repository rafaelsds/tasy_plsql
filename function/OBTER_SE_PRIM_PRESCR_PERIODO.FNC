CREATE OR REPLACE
FUNCTION Obter_se_prim_prescr_periodo(	nr_atendimento_p	NUMBER,
					nr_prescricao_p		NUMBER,
					cd_setor_atendimento_p  NUMBER,
					cd_material_p		NUMBER,
					dt_inicial_p		DATE,
					dt_final_p		DATE)
 		    	RETURN VARCHAR2 IS

ie_primeira_w			VARCHAR2(1) := 'N';
nr_prescricao_w			NUMBER(14,0);

BEGIN

SELECT	MIN(a.nr_prescricao)
INTO	nr_prescricao_w
FROM	material c,
	prescr_medica a,
	prescr_material b
WHERE	a.nr_atendimento = nr_atendimento_p
AND	a.nr_prescricao = b.nr_prescricao
AND	b.cd_material = c.cd_material
AND	a.dt_prescricao BETWEEN dt_inicial_p AND dt_final_p
AND	c.ie_controle_medico <> 0
AND	(('0' = cd_setor_atendimento_p) OR (Obter_Unidade_Atendimento(a.nr_atendimento,'A', 'CS') = cd_setor_atendimento_p))
AND	b.cd_material = cd_material_p;

IF	(nr_prescricao_w = nr_prescricao_p) THEN
	ie_primeira_w := 'S';
END IF;

RETURN	ie_primeira_w;

END Obter_se_prim_prescr_periodo;
/