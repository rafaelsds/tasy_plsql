CREATE OR REPLACE
FUNCTION obter_hora_agenda_paciente	(nr_atendimento_p       number)
 					return date is
dt_agenda_w        date;
BEGIN

if	(nr_atendimento_p is not null)	then
	select  max(a.hr_inicio)
	into    dt_agenda_w
	from    agenda_paciente a
	where   a.nr_atendimento = nr_atendimento_p;

   	if	(dt_agenda_w is null)	or
		(dt_agenda_w = '')	then
		select  max(b.dt_agenda)
		into	dt_agenda_w
		from	agenda_consulta b
		where	nr_atendimento = nr_atendimento_p;
	end if;
end if;

RETURN	dt_agenda_w;

END	obter_hora_agenda_paciente;
/