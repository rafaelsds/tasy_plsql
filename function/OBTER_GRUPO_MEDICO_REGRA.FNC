create or replace
function obter_grupo_medico_regra (nr_seq_medico_regra_p	number)
 		    	return varchar2 is
ds_grupo_w	varchar2(255) :=  '';

cursor c_grupos is
select	substr(a.ds_grupo,1,255) ds_grupo
from	ageint_regra_grupo_medico a,
	ageint_medico_regra b
where	a.nr_sequencia = b.nr_seq_grupo
and	b.nr_sequencia = nr_seq_medico_regra_p;

begin

for r_c_grupos in c_grupos loop
	ds_grupo_w := r_c_grupos.ds_grupo;
end loop;

return	ds_grupo_w;

end obter_grupo_medico_regra ;
/