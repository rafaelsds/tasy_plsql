CREATE OR REPLACE
FUNCTION Obter_Desc_Unid_Med(  	cd_unidade_medida_p	Varchar2)
					RETURN VARCHAR IS

ds_unidade_medida_w			Varchar2(100);

BEGIN

select	max(ds_unidade_medida)
into	ds_unidade_medida_w
from 	Unidade_medida
where 	cd_unidade_medida	= cd_unidade_medida_p;
	
RETURN ds_unidade_medida_w;

END Obter_desc_unid_med;
/