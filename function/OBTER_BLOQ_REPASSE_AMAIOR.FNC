CREATE or replace function  obter_bloq_repasse_amaior(	cd_procedimento_p	number,
			cd_material_p	number,
			nr_seq_terceiro_p	number,
			dt_repasse_p date)
			return number is

regra_bloq_rep_amaior_w	number;
qt_regra_bloqueio_w	number(10,0);

begin

select count(*)
into qt_regra_bloqueio_w
from regra_bloq_rep_amaior;

if	(qt_regra_bloqueio_w > 0) then

	if	(cd_procedimento_p is not null) then

		select max(a.nr_sequencia)
		into regra_bloq_rep_amaior_w
		from regra_bloq_rep_amaior a
		where trunc(dt_repasse_p, 'dd') >= dt_inicio_vigencia
		and trunc(dt_repasse_p, 'dd') <= dt_fim_vigencia
		and (a.cd_procedimento is null OR a.cd_procedimento = cd_procedimento_p)
		and (not exists (select 1 from regra_bloq_terc_amaior b where b.nr_seq_regra_bloc = a.nr_sequencia)
			or exists (select 1 from regra_bloq_terc_amaior b where b.nr_seq_regra_bloc = a.nr_sequencia and b.nr_seq_terceiro = nr_seq_terceiro_p));

	elsif	(cd_material_p is not null) then

		select max(a.nr_sequencia)
		into regra_bloq_rep_amaior_w
		from regra_bloq_rep_amaior a
		where trunc(dt_repasse_p, 'dd') >= dt_inicio_vigencia
		and trunc(dt_repasse_p, 'dd') <= dt_fim_vigencia
		and (a.cd_material is null OR a.cd_material = cd_material_p)
		and (not exists (select 1 from regra_bloq_terc_amaior b where b.nr_seq_regra_bloc = a.nr_sequencia)
			or exists (select 1 from regra_bloq_terc_amaior b where b.nr_seq_regra_bloc = a.nr_sequencia and b.nr_seq_terceiro = nr_seq_terceiro_p));

	end if;

end if;

return regra_bloq_rep_amaior_w;

end obter_bloq_repasse_amaior;
/