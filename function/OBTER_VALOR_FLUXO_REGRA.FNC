create or replace
function obter_valor_fluxo_regra(
		cd_estabelecimento_p	number,
		nr_seq_regra_p		number,
		dt_referencia_p		date,
		dt_final_p			date,
		cd_empresa_p		number) 	
		return number is

vl_regra_w		number(15,2) := 0;
nr_sequencia_w		number(10);
dt_referencia_w		date;
ie_regra_origem_w		varchar2(3);
ie_regra_data_w		varchar2(3);
vl_fixo_w			number(15,2) := 0;
nr_dia_fixo_w		number(2,0);
ie_tratar_fim_semana_w	varchar2(3);
cd_conta_financ_w		number(10,0);
qt_mes_anterior_w		number(3,0);
ie_antecipar_dia_util_w	varchar2(100);
ie_dia_fixo_w		varchar2(100);
ie_dia_diferente_w		varchar2(100);
dt_final_w		date;
dt_ref_original_w		date;

dt_vigencia_inicial_w		date;
dt_vigencia_final_w			date;

begin

dt_final_w		:= nvl(dt_final_p, dt_referencia_p);

select	ie_tratar_fim_semana
into	ie_tratar_fim_semana_w
from	parametro_fluxo_caixa
where	cd_estabelecimento	= cd_estabelecimento_p;

select	max(ie_regra_origem),
	max(ie_regra_data),
	max(vl_fixo),
	max(nr_dia_fixo),
	max(cd_conta_financ),
	max(qt_mes_anterior),
	max(ie_antecipar_dia_util),
	nvl(max(ie_dia_fixo), 'N'),
	max(dt_vigencia_inicial),
	max(dt_vigencia_final)
into	ie_regra_origem_w,
	ie_regra_data_w,
	vl_fixo_w,
	nr_dia_fixo_w,
	cd_conta_financ_w,
	qt_mes_anterior_w,
	ie_antecipar_dia_util_w,
	ie_dia_fixo_w,
	dt_vigencia_inicial_w,
	dt_vigencia_final_w
from	regra_fluxo_caixa
where	nr_sequencia		= nr_seq_regra_p;
--and	dt_referencia_p	between nvl(dt_vigencia_inicial, dt_referencia_p - 1) and nvl(dt_vigencia_final, dt_referencia_p + 1);


if	(ie_regra_origem_w = 'VF') then

	ie_dia_diferente_w		:= 'N';
	if	(nr_dia_fixo_w > PKG_DATE_UTILS.extract_field('DAY', PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0))) then
		dt_referencia_w		:= PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0);
		ie_dia_diferente_w	:= 'S';
	else
		dt_referencia_w		:= PKG_DATE_UTILS.GET_DATE(nr_dia_fixo_w, dt_referencia_p);
	end if;

	dt_ref_original_w		:= dt_referencia_w;

	if	(ie_regra_data_w = 'DU') then
		begin
		dt_referencia_w	:= obter_dia_util_mes(cd_estabelecimento_p, dt_referencia_w, PKG_DATE_UTILS.extract_field('DAY', dt_referencia_w));
		end;
	else
		if	(ie_tratar_fim_semana_w = 'S') and (ie_antecipar_dia_util_w <> 'U') then
			dt_referencia_w	:= obter_proximo_dia_util(cd_estabelecimento_p, dt_referencia_w);
		end if;
	end if;
	
	-- edgar 22/07/2008, os 101222, tratar casos em que o pr�ximo dia �til caia no outro m�s
	if	(ie_antecipar_dia_util_w = 'U') or
		((ie_antecipar_dia_util_w = 'S') and
		 ((PKG_DATE_UTILS.start_of(dt_referencia_w, 'MONTH', 0) > PKG_DATE_UTILS.start_of(dt_ref_original_w, 'MONTH', 0)) or
		  (obter_se_dia_util(dt_referencia_w, cd_estabelecimento_p) = 'N'))) then
		dt_referencia_w			:= obter_dia_anterior_util(cd_estabelecimento_p, dt_ref_original_w);
	end if;
	
	if	((ie_dia_fixo_w = 'N') or ((dt_ref_original_w = dt_referencia_w) and (ie_dia_diferente_w = 'N'))) and
		 (PKG_DATE_UTILS.start_of(dt_referencia_w, 'dd',0) between PKG_DATE_UTILS.start_of(dt_referencia_p, 'dd',0) and dt_final_w) then
		vl_regra_w		:= vl_fixo_w;
	end if;
	
	/*Se entrar nesse IF, significa que a data de referencia n�o est� na vig�ncia da regra. Ai tem que verificar se � referente a feriado, pois a data de vigencia foi tirada do cursor*/
	if ( pkg_date_utils.start_of(dt_referencia_p, 'DDD', 0) < pkg_date_utils.start_of(nvl(dt_vigencia_inicial_w,dt_referencia_p), 'DDD', 0) ) or ( pkg_date_utils.start_of(dt_referencia_p, 'DDD', 0) > pkg_date_utils.start_of(nvl(dt_vigencia_final_w,dt_referencia_p),'DDD',0)) then
	
		/*Se a data de refencia for menor que a vigencia, precisa verificar se a regra da conta � para antecipar o dia util e se o dia util  antecipado com base no dia fixo da regra � igual a data de refenncia*/
		if  (pkg_date_utils.start_of(dt_referencia_p,'DDD',0) < pkg_date_utils.start_of(nvl(dt_vigencia_inicial_w,dt_referencia_p),'DDD',0)) and (ie_antecipar_dia_util_w = 'U') and 
			(obter_dia_anterior_util(cd_estabelecimento_p, dt_referencia_w) = dt_referencia_p) then
				vl_regra_w		:= vl_fixo_w;
		else
				vl_regra_w		:= 0;
		end if;
		
	end if;
			
elsif	(ie_regra_origem_w = 'MA') then

	select	nvl(sum(vl_fluxo),0)
	into	vl_regra_w
	from	fluxo_caixa
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_periodo		= 'D'
	and	ie_classif_fluxo	= 'P'
	and	cd_conta_financ	= cd_conta_financ_w
	and	PKG_DATE_UTILS.start_of(dt_referencia, 'MONTH', 0) between 
					PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p,-1 * qt_mes_anterior_w,0), 'MONTH', 0) and
					PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_final_w,-1 * qt_mes_anterior_w,0), 'MONTH', 0);

elsif	(ie_regra_origem_w = 'RP') then

	if	(ie_regra_data_w = 'F') then

		select	sum(a.vl_repasse)
		into	vl_regra_w
		from	terceiro b,
			repasse_terceiro_item_v a
		where	b.cd_estabelecimento	= cd_estabelecimento_p
		and	a.nr_seq_terceiro	= b.nr_sequencia
		and	PKG_DATE_UTILS.start_of(a.dt_item, 'dd', 0)	between	PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p, qt_mes_anterior_w * -1,0) and 
							PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p, qt_mes_anterior_w * -1,0);

	elsif	(ie_regra_data_w = 'MA') then

		select	sum(a.vl_repasse)
		into	vl_regra_w
		from	terceiro b,
			repasse_terceiro_item_v a
		where	b.cd_estabelecimento	= cd_estabelecimento_p
		and	a.nr_seq_terceiro	= b.nr_sequencia
		and	PKG_DATE_UTILS.start_of(a.dt_item, 'dd', 0) 	= PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p, qt_mes_anterior_w * -1,0);

	end if;

elsif	(ie_regra_origem_w = 'FS') then
	null;
end if;

return	vl_regra_w;

end obter_valor_fluxo_regra;
/