create or replace
function obter_media_nas_atend(	nr_atendimento_p	number,
					dt_inicial_p	date)
					return		number is
qt_retorno_w		number(5,2);
qt_pontuacao_w		number(5,2);
qt_total_w		number(5,2) := 0;
qt_contador_w		number(5,0) := 0;
dt_atual_w		date;
dt_final_w		date;



cursor c01 is
select	nvl(qt_pontuacao,0)
from	escala_nas a
where	a.dt_avaliacao = ( 	select	max(e.dt_avaliacao)
				from	escala_nas e
				where	e.nr_atendimento = nr_atendimento_p
				and	pkg_date_utils.start_of(e.dt_avaliacao, 'DD', 0) = pkg_date_utils.start_of(dt_atual_w,'DD',0));

begin

dt_atual_w := pkg_date_utils.start_of(dt_inicial_p, 'MONTH',0);

dt_final_w := PKG_DATE_UTILS.ADD_MONTH(dt_atual_w, 1, 0);


while	(dt_atual_w < dt_final_w) loop
	begin
	open c01;
		loop
		fetch c01 into	
			qt_pontuacao_w;
		exit when c01%notfound;
			begin
				qt_total_w 	:= qt_total_w + qt_pontuacao_w;
				qt_contador_w	:= qt_contador_w + 1;
			end;
		end loop;
	close C01;

	dt_atual_w	:= pkg_date_utils.start_of(dt_atual_w + 1, 'DD',0);

	end;
end loop;

if(qt_contador_w > 0)then
	qt_retorno_w := qt_total_w / qt_contador_w;
end if;

return	qt_retorno_w;



end obter_media_nas_atend;
/