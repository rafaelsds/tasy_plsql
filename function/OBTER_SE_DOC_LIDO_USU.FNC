create or replace
function obter_se_doc_lido_usu(	nr_seq_doc_p	number,
				nm_usuario_p	varchar2)
				return varchar2 is

nr_seq_log_w		number(10);
dt_ultima_leitura_w		Date;
dt_revisao_w		Date;
ie_retorno_w		varchar2(1);
ie_data_aprov_rev_w	varchar2(1); /*Considera a data da aprovacao (S) ou data da revisao (N)*/

/*
N - Nao Lido
S - Lido
*/

begin
ie_retorno_w	:= 'N';

obter_param_usuario(4000, 165, obter_perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_data_aprov_rev_w);

select	max(a.nr_sequencia)
into	nr_seq_log_w
from	qua_doc_log_acesso a
where	a.nr_seq_doc = nr_seq_doc_p
and	a.nm_usuario = nm_usuario_p
and	((dt_fim_leitura is not null
AND dt_leitura IS NOT NULL) or dt_pendente is not null);

select	max(a.dt_leitura)
into	dt_ultima_leitura_w
from	qua_doc_log_acesso a
where	a.nr_sequencia = nr_seq_log_w;

if	(dt_ultima_leitura_w is not null) then
	begin
	select	max(dt_revisao)
	into	dt_revisao_w
	from	qua_doc_revisao
	where	nr_seq_doc = nr_seq_doc_p
	and	dt_aprovacao is not null;

	if	(nvl(ie_data_aprov_rev_w,'N') = 'S') then
		select	max(dt_aprovacao)
		into	dt_revisao_w
		from	qua_doc_revisao
		where	nr_seq_doc = nr_seq_doc_p
		and	dt_aprovacao is not null;
	end if;
	
	if	(dt_revisao_w is null) or
		(dt_revisao_w <= dt_ultima_leitura_w) then
		ie_retorno_w	:= 'S';
	end if;
	end;
end if;	

return ie_retorno_w;

end obter_se_doc_lido_usu;
/
