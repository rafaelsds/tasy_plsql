create or replace
function ageint_obter_regra_med_exame(
			cd_medico_p		varchar2,
			cd_agenda_p		number,
			nr_seq_proc_interno_p	number,
			nr_seq_ageint_p		number,
			cd_procedimento_p	number,
			ie_origem_proced_p	number)
 		    	return varchar2 is

cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);
cd_estabelecimento_w	number(4);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
qt_regra_w		number(5);
ds_retorno_w		varchar2(1)	:= 'S';
cd_plano_w		varchar2(10);
			
begin

if	(nvl(nr_seq_proc_interno_p,0)	> 0) then
	select	max(cd_convenio),
		max(cd_categoria),
		max(cd_estabelecimento),
		max(cd_plano)
	into	cd_convenio_w,
		cd_categoria_w,
		cd_estabelecimento_w,
		cd_plano_w
	from	agenda_integrada
	where	nr_sequencia	= nr_seq_Ageint_p;

	if	(nvl(cd_procedimento_p,0) 	= 0) or
		(nvl(ie_origem_proced_p,0)	= 0) then
		obter_proc_tab_interno_conv(
							nr_seq_proc_interno_p,
							cd_estabelecimento_w,
							cd_convenio_w,
							cd_categoria_w,
							cd_plano_w,
							null,
							cd_procedimento_w,
							ie_origem_proced_w,
							null,
							sysdate,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null);
	else
		cd_procedimento_w	:= cd_procedimento_p;
		ie_origem_proced_w	:= ie_origem_proced_p;
	end if;

	select	count(*)
	into	qt_regra_w
	from	agenda_medico_proced
	where	nvl(cd_agenda,cd_agenda_p) 	= cd_agenda_p
	and	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
	and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null)))
	and	((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
	AND	((cd_area_procedimento = obter_area_procedimento( cd_procedimento_w, ie_origem_proced_w)) or  (cd_area_procedimento is null) ) and
	        ((cd_especialidade = obter_especialidade_proced( cd_procedimento_W, ie_origem_proced_w)) or  (cd_especialidade is null) ) and
	        ((cd_grupo_proc = obter_grupo_procedimento( cd_procedimento_w, ie_origem_proced_w,'C')) or (cd_grupo_proc is null));
		
	if	(qt_regra_w	> 0) then
		if	(cd_medico_p is not null) then
			select	count(*)
			into	qt_regra_w
			from	agenda_medico_proced
			where	nvl(cd_agenda,cd_agenda_p) 	= cd_agenda_p
			and	cd_medico	= cd_medico_p
			and	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
			and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null)))
			and	((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno is null)) and
				((cd_area_procedimento = obter_area_procedimento( cd_procedimento_w, ie_origem_proced_w)) or  (cd_area_procedimento is null) ) and
				((cd_especialidade = obter_especialidade_proced( cd_procedimento_W, ie_origem_proced_w)) or  (cd_especialidade is null) ) and
				((cd_grupo_proc = obter_grupo_procedimento( cd_procedimento_w, ie_origem_proced_w,'C')) or (cd_grupo_proc is null));
			
			if	(qt_regra_w	= 0) then
				ds_retorno_w	:= 'N';
			end if;
		else
			ds_retorno_w	:= 'N';
		end if;
		
	end if;
end if;
	
return	ds_retorno_w;

end ageint_obter_regra_med_exame;
/
