create or replace
function man_obter_min_prev_usuario
			(	nr_seq_ordem_serv_p		number,
				nm_usuario_prev_p		varchar2,
				dt_prevista_p			date)
 		    		return	number is

qt_retorno_w			number(15,2);

begin

select	nvl(sum(qt_min_prev),0) qt_minuto
into	qt_retorno_w
from	man_ordem_servico_exec a
where	nr_seq_ordem	= nr_seq_ordem_serv_p
and		nm_usuario_exec		= nm_usuario_prev_p;

return	qt_retorno_w;

end man_obter_min_prev_usuario;
/