create or replace
function pls_obter_valor_despesa_mes
			(	dt_mes_p			Date,
				nr_seq_contrato_p		Number,
				ie_tipo_valor_p			varchar2)
				return Number is

vl_retorno_w			Number(15,2);

begin
if	(ie_tipo_valor_p = 'A') then
	if	(nvl(nr_seq_contrato_p,0) = 0) then
		select	nvl(sum(b.vl_total),0) vl_despesa
		into	vl_retorno_w
		from	pls_conta		b,
			pls_protocolo_conta	a
		where	trunc(a.dt_mes_competencia, 'Month') = trunc(dt_mes_p,'Month')
		and	b.nr_seq_protocolo	= a.nr_sequencia;
	elsif	(nvl(nr_seq_contrato_p,0) > 0) then
		select	nvl(sum(b.vl_total),0) vl_despesa
		into	vl_retorno_w
		from	pls_segurado		c,
			pls_conta		b,
			pls_protocolo_conta	a
		where	trunc(a.dt_mes_competencia, 'Month') = trunc(dt_mes_p,'Month')
		and	b.nr_seq_protocolo	= a.nr_sequencia
		and	b.nr_seq_segurado	= c.nr_sequencia
		and	c.nr_seq_contrato	= nr_seq_contrato_p;
	end if;
elsif	(ie_tipo_valor_p	= 'AC') then
	if	(nvl(nr_seq_contrato_p,0) = 0) then
		select	nvl(sum(b.vl_total),0) vl_despesa
		into	vl_retorno_w
		from	pls_conta		b,
			pls_protocolo_conta	a
		where	a.dt_mes_competencia between trunc(dt_mes_p,'yyyy') and last_day(dt_mes_p)
		and	b.nr_seq_protocolo	= a.nr_sequencia;
	elsif	(nvl(nr_seq_contrato_p,0) > 0) then
		select	nvl(sum(b.vl_total),0) vl_despesa
		into	vl_retorno_w
		from	pls_segurado		c,
			pls_conta		b,
			pls_protocolo_conta	a
		where	a.dt_mes_competencia between trunc(dt_mes_p,'yyyy') and last_day(dt_mes_p)
		and	b.nr_seq_protocolo	= a.nr_sequencia
		and	b.nr_seq_segurado	= c.nr_sequencia
		and	c.nr_seq_contrato	= nr_seq_contrato_p;
	end if;
end if;
return	vl_retorno_w;

end pls_obter_valor_despesa_mes;
/