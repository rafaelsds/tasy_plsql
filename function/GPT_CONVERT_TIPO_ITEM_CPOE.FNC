create or replace
function gpt_convert_tipo_item_cpoe(ie_tipo_item_p	varchar2) return varchar2 is


begin
	if (ie_tipo_item_p = 'O') then
		return 'G';
	elsif (ie_tipo_item_p = 'SOL') then
		return 'M';
	elsif (ie_tipo_item_p in ('LD', 'S', 'SNE', 'NPTA', 'NPTI', 'NAN', 'NPN', 'J', 'D')) then
		return 'N';
	elsif (ie_tipo_item_p in ('DI', 'DP')) then
		return 'D';
	else
		return ie_tipo_item_p;
	end if;
end gpt_convert_tipo_item_cpoe;
/