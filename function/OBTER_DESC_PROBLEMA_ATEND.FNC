create or replace
function obter_desc_problema_atend
			(nr_seq_problema_p	number)
			return varchar2 is

	
ds_retorno_w		varchar2(255) := '';   

begin


If (nr_seq_problema_p is not null) then

	Select  substr(ds_problema,1,255)
	into	ds_retorno_w
	from	lista_problema_pac
	where	nr_Sequencia = nr_seq_problema_p;


end if;

return ds_retorno_w;

end obter_desc_problema_atend;
/
