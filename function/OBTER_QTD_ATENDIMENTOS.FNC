create or replace
function obter_qtd_atendimentos(	cd_pessoa_fisica_p	varchar2) 
	return number is
	
qt_atendimento_w	number(14);

PRAGMA AUTONOMOUS_TRANSACTION;
	
begin

select  count(1)
into    qt_atendimento_w
from    atendimento_paciente a
where   a.cd_pessoa_fisica = cd_pessoa_fisica_p;

return qt_atendimento_w;

end obter_qtd_atendimentos;
/