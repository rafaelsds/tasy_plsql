create or replace 
function LAB_Obter_Result_prescr(nr_prescricao_p	number,
				nr_seq_prescr_p		number,
				ie_opcao_p		number,
				ie_mostra_resultado_p	varchar2,
				ie_separador_p		varchar2)
				return varchar2 is
					
cd_pessoa_fisica_w	varchar2(10);
dt_aprovacao_w	date;


ds_unidade_medida_w	varchar2(100);
ds_resultado_w		varchar2(2000);

ds_retorno_w		varchar2(2000);
cd_estabelecimento_w	number(4);
qt_exame_ant_w		number(5);
qt_result_anterior_w	number(5);
ie_recem_nato_w		varchar2(1);

cursor c01 is
	select	substr(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado),1,100)
	from	exame_laboratorio d,
		exame_lab_result_item c,
		exame_lab_resultado b
	where	b.nr_seq_resultado	= c.nr_seq_resultado
	  and	d.nr_seq_exame 		= c.nr_seq_exame
	  and 	b.nr_prescricao 	= nr_prescricao_p
	  and	c.nr_seq_prescr		= nr_seq_prescr_p
	  and	ie_mostra_resultado_p 	= 'S'
	order by nm_exame;
begin

open c01;
loop
	fetch c01 into	ds_resultado_w;
	exit when c01%notfound;
	
	if	(ie_opcao_p = 0) and (nvl(length(ds_retorno_w),0) < 2000) and ( ds_resultado_w is not null) then

		ds_retorno_w := ds_retorno_w || ds_resultado_w || ie_separador_p;
		
	end if;

end loop;
close c01;

ds_retorno_w	:= substr(ds_retorno_w,1,length(ds_retorno_w)-1);

return ds_retorno_w;

end LAB_Obter_Result_prescr;
/