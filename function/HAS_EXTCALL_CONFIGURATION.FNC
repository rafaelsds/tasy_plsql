create or replace
function has_extcall_configuration(
		nr_seq_external_call_p		number)
 		return varchar2 is

qt_records_w	number(10);
ds_return_w	varchar2(1) := 'N'; --No

begin


select	sum(amount) 
into	qt_records_w
from (
        select  count(*) amount
        from    obj_schem_estab a,
        	objeto_schematic_param b
        where   a.nr_seq_obj_schem_param = b.nr_sequencia
        and	b.nr_seq_obj_sch = nr_seq_external_call_p
        union
        select  count(*) amount
        from    obj_schem_perfil a,
        	objeto_schematic_param b
        where   a.nr_seq_obj_schem_param = b.nr_sequencia
        and	b.nr_seq_obj_sch = nr_seq_external_call_p
        union
        select  count(*) amount
        from    obj_schem_usuario a,
        	objeto_schematic_param b
        where   a.nr_seq_obj_schem_param = b.nr_sequencia
        and	b.nr_seq_obj_sch = nr_seq_external_call_p
);

if (qt_records_w > 0) then
	ds_return_w := 'Y'; --Yes
end if;

return ds_return_w;

end has_extcall_configuration;
/
