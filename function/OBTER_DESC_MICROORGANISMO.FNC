create or replace
function Obter_desc_microorganismo(cd_microorganismo_cih_p		Number)
 		    	return Varchar2 is

ds_retorno_w	varchar2(255);			
			
begin

Select	max(ds_microorganismo)
into	ds_retorno_w
from	cih_microorganismo
where	cd_microorganismo = cd_microorganismo_cih_p;

return	ds_retorno_w;

end Obter_desc_microorganismo;
/