create or replace
function obter_prioridade_espec_medico(cd_especialidade_p		number)
 		    	return number is
				
nr_seq_prioridade_w	medico_especialidade.nr_seq_prioridade%type;

begin

select	nvl(min(nr_seq_prioridade),999) nr_seq_prioridade
into	nr_seq_prioridade_w
from	medico_especialidade
where	cd_pessoa_fisica = obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C')
and		cd_especialidade = cd_especialidade_p;


return	nr_seq_prioridade_w;

end obter_prioridade_espec_medico;
/