create or replace
function pls_obter_descricao_motivo_sib	(cd_motivo_p	number)	
					return varchar2 is

ds_retorno_w	varchar2(2000);
begin

if (cd_motivo_p = 1) then
	return 'Rompimento do contrato por iniciativa do benefici�rio';
elsif (cd_motivo_p = 2) then
	return 'T�rmino da rela��o de vinculado a um benefici�rio titular';
elsif (cd_motivo_p = 3) then
	return 'Desligamento da empresa (para planos coletivos)';
elsif (cd_motivo_p = 4) then
	return 'Inadimpl�ncia';
elsif (cd_motivo_p = 5) then
	return '�bito';
elsif (cd_motivo_p = 7) then
	return 'Exclus�o decorrente de mudan�a de c�digo de benefici�rio motivada pela adapta��o de sistema da operadora';
elsif (cd_motivo_p = 8) then
	return 'Tranfer�ncia de carteira';
elsif (cd_motivo_p = 9) then
	return 'Altera��o individual do c�digo do benefici�rio';
elsif (cd_motivo_p = 13) then
	return 'Inclus�o indevida de benefici�rios';
elsif (cd_motivo_p = 14) then
	return 'Fraude (art. 13 da Lei n� 9.656/98)';
elsif (cd_motivo_p = 6) then
	return 'Mudan�a de plano';
elsif (cd_motivo_p = 11) then
	return 'Plano antigo migrado';
elsif (cd_motivo_p = 12) then
	return 'Plano antigo adaptado';
elsif (cd_motivo_p = 15) then
	return 'Inclus�o de novos benefici�rios';
elsif (cd_motivo_p = 16) then
	return 'Inclus�o de benefici�rios motivada por transfer�ncia volunt�ria de carteira';
elsif (cd_motivo_p = 17) then
	return 'Inclus�o de benefici�rios motivada por transfer�ncia compuls�ria de carteira';
elsif (cd_motivo_p = 18) then
	return 'Inclus�o decorrente de mudan�a de c�digo de benefici�rio motivada pela adapta��o de sistema da operadora';
/*aaschlote 25/05/2011 OS - 305656*/
elsif (cd_motivo_p = 41) then
	return 'Rompimento do contrato por iniciativa do benefici�rio';
elsif (cd_motivo_p = 42) then
	return 'Desligamento da empresa (para planos coletivos)';
elsif (cd_motivo_p = 43) then
	return 'Inadimpl�ncia';
elsif (cd_motivo_p = 44) then
	return '�bito';	
elsif (cd_motivo_p = 45) then
	return 'Tranfer�ncia de carteira';	
elsif (cd_motivo_p = 46) then
	return 'Inclus�o indevida de benefici�rios';		
elsif (cd_motivo_p = 47) then
	return 'Fraude (art. 13 da Lei n� 9.656/98)';
elsif (cd_motivo_p = 48) then
	return 'Por portabilidade de car�ncia';
elsif (cd_motivo_p = 51) then
	return 'Corre��o de dados de benefici�rios em registros ativos no cadastro de benefici�rios do SIB/ANS';	
else	
	return null;
end if;

end;
/
