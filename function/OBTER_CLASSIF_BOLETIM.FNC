create or replace
function obter_classif_boletim(nr_sequencia_p		number)
 		    	return varchar2 is

ds_classif_boletim_w	varchar2(255);			
			
begin
if	(nr_sequencia_p	is not  null) then
	select	ds_classificacao
	into	ds_classif_boletim_w
	from	sac_classif_ocorrencia
	where	nr_sequencia	= nr_sequencia_p;
end if;

return	ds_classif_boletim_w;

end obter_classif_boletim;
/