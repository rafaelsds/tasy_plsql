create or replace
function pls_sib_obter_status_mov
			(	nr_seq_sib_movimento_p		pls_sib_movimento.nr_sequencia%type)
 		    	return varchar2	is

ds_retorno_w		varchar2(255)	:= null;
nr_seq_lote_w		pls_sib_lote.nr_sequencia%type;
ie_status_lote_w	pls_sib_lote.ie_status%type;
nr_seq_segurado_w	pls_sib_movimento.nr_seq_segurado%type;
ie_tipo_movimento_w	pls_sib_movimento.ie_tipo_movimento%type;
qt_arquivo_devolucao_w	pls_integer;
qt_devolucao_erro_w	pls_integer;

begin
if	(nr_seq_sib_movimento_p is not null) then
	select	a.nr_seq_lote,
		a.nr_seq_segurado,
		a.ie_tipo_movimento,
		b.ie_status
	into	nr_seq_lote_w,
		nr_seq_segurado_w,
		ie_tipo_movimento_w,
		ie_status_lote_w
	from	pls_sib_movimento a,
		pls_sib_lote b
	where	b.nr_sequencia	= a.nr_seq_lote
	and	a.nr_sequencia	= nr_seq_sib_movimento_p;
	
	if	(ie_status_lote_w in (2,3)) then --Gerado, Arquivo gerado
		ds_retorno_w	:= wheb_mensagem_pck.get_texto(1048453);--Gerado
	else
		select	count(1)
		into	qt_arquivo_devolucao_w
		from	pls_sib_devolucao
		where	nr_seq_lote_sib	= nr_seq_lote_w;
		
		if	(qt_arquivo_devolucao_w = 0) then
			ds_retorno_w	:= wheb_mensagem_pck.get_texto(1048448);--Enviado
		else
			select	count(1)
			into	qt_devolucao_erro_w
			from	pls_sib_devolucao_erro a,
				pls_sib_devolucao b
			where	b.nr_sequencia	= a.nr_seq_devolucao
			and	b.nr_seq_lote_sib = nr_seq_lote_w
			and	a.nr_seq_segurado = nr_seq_segurado_w
			and	a.ie_tipo_movimento = ie_tipo_movimento_w;
			
			if	(qt_devolucao_erro_w > 0) then
				ds_retorno_w	:= wheb_mensagem_pck.get_texto(1048450);--Erro
			else
				ds_retorno_w	:= wheb_mensagem_pck.get_texto(1048449);--Processado
			end if;
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_sib_obter_status_mov;
/