create or replace
function obter_se_nota_bonificacao(nr_sequencia_p	number)
 		    	return varchar2 is

ie_bonificacao_w		varchar2(1);			
			
begin
select	nvl(max(b.ie_bonificacao),'N')
into	ie_bonificacao_w
from	nota_fiscal a,
	operacao_nota b
where	a.cd_operacao_nf = b.cd_operacao_nf
and	a.nr_sequencia = nr_sequencia_p;

return	ie_bonificacao_w;

end obter_se_nota_bonificacao;
/