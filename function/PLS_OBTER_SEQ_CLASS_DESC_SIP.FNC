create or replace
function pls_obter_seq_class_desc_sip
			(	nr_seq_classificacao_p		Number,
				cd_classificacao_p		Varchar2,
				ie_opcao_p			Number)
				return Varchar2 is
				
/*ie_opcao_p	1 = Sequencia,
		2 = C�digo
		3 = Descri��o

*/

ds_retorno_w		Varchar2(255);

begin
if	(ie_opcao_p	= 1) then
	select	nr_sequencia
	into	ds_retorno_w
	from	sip_item_assistencial
	where	cd_classificacao	= cd_classificacao_p;
elsif	(ie_opcao_p	= 2) then
	select	cd_classificacao
	into	ds_retorno_w
	from	sip_item_assistencial
	where	nr_sequencia		= nr_seq_classificacao_p;
elsif	(ie_opcao_p	= 3) then
	select	ds_item
	into	ds_retorno_w
	from	sip_item_assistencial
	where	((nr_sequencia		= nr_seq_classificacao_p)
	or	(cd_classificacao	= cd_classificacao_p));
end if;

return	ds_retorno_w;

end pls_obter_seq_class_desc_sip;
/