create or replace
function obter_estagio_lote_audit_rec 	(nr_seq_lote_audit_rec_p	number,
					nr_seq_ret_hist_p		number,
					ie_opcao_p			varchar2) 
					return varchar2 is

/* ie_opcao_p

'S'	sequ�ncia
'D'	descri��o

*/

nr_seq_estagio_w		number(10);
ds_estagio_lote_w		varchar2(255);
ds_retorno_w			varchar2(255);

begin

select	max(b.nr_sequencia),
	max(b.ds_estagio_lote)
into	nr_seq_estagio_w,
	ds_estagio_lote_w
from	estagio_lote_audit_rec b,
	lote_audit_rec_estagio a
where	a.nr_seq_lote_audit_rec	= nr_seq_lote_audit_rec_p
and	a.nr_seq_ret_hist	= nr_seq_ret_hist_p
and	a.nr_seq_est_lote_rec	= b.nr_sequencia;

if	(ie_opcao_p	= 'S') then
	ds_retorno_w	:= nr_seq_estagio_w;
elsif	(ie_opcao_p	= 'D') then
	ds_retorno_w	:= ds_estagio_lote_w;
end if;

return	ds_retorno_w;

end;
/