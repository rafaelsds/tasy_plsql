create or replace
function pls_obter_data_conta_mat(	nr_seq_prestador_exec_p	pls_conta_v.nr_seq_prestador_exec%type,
					dt_atendimento_mat_p	pls_conta_mat.dt_atendimento%type,
					dt_atendimento_conta_p	pls_conta_v.dt_atendimento%type,
					dt_protocolo_p		pls_protocolo_conta.dt_protocolo%type,
					dt_mes_competencia_p	pls_protocolo_conta.dt_mes_competencia%type)
 		    	return date is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Obter a data do material olhando a regra cadastrada para o prestador executor da conta,
	onde � poss�vel informar de onde deve ser buscada a data do item.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

SEMPRE QUE FOR ALTERAR ESTA FUNCTION VERIFICAR SE � NECESS�RIO ALTERAR TAMB�M A FUNCTION PLS_OBTER_DATA_CONTA_PROC!

 Se o prestador n�o for informado ou se n�o for encontrado dado da regra para o mesmo ent�o ser� olhado por padr�o na data da do material, 
se esta n�o tiver informada olha para a data da conta.

Altera��es:
-------------------------------------------------------------------------------------------------------------------
jjung OS 589337 09/07/2013 - Cria��o da function.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dt_retorno_w		date;	
ie_regra_data_preco_w	pls_prestador.ie_regra_data_preco_mat%type;

begin

ie_regra_data_preco_w := null;

-- Se o prestador n�o for informado ou n�o tiver regra de data para ele entra por padr�o na primeira verificacao do item.
if	(nr_seq_prestador_exec_p is not null) then

	select	max(a.ie_regra_data_preco_mat)
	into	ie_regra_data_preco_w
	from	pls_prestador a
	where	a.nr_sequencia = nr_seq_prestador_exec_p;
end if;

-- Se for informado a data do Item ou se n�o tiver regra ou prestador informado ent�o verifica primeiro a data do item, se n�o
-- tiver informada verifica a data da conta. 
if	(ie_regra_data_preco_w = 'E') or 
	(ie_regra_data_preco_w is null) then
	
	-- Se a data do item n�o for informada ent�o retorna a data da conta.
	if	(dt_atendimento_mat_p is not null) then

		dt_retorno_w := dt_atendimento_mat_p;
	else	
		dt_retorno_w := dt_atendimento_conta_p;
	end if;

-- Se for informado para usar a data da conta ent�o retorna a data da conta. 
elsif	(ie_regra_data_preco_w = 'X') then

	dt_retorno_w := dt_atendimento_conta_p;
	
-- Se for informado para usar a data do protocolo ent�o se d� preferencia a data do protocolo, caso n�o for informada retorna a data de competencia do mesmo.
elsif	(ie_regra_data_preco_w = 'P') then

	-- Se a data do protocolo n�o for informado ent�o retorna a data de competencia.
	if	(dt_protocolo_p is not null) then
		
		dt_retorno_w := dt_protocolo_p;
	else
		dt_retorno_w := dt_mes_competencia_p;
	end if;
end if;

return	dt_retorno_w;

end pls_obter_data_conta_mat;
/
