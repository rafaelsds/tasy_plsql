create or replace
function obter_se_laudo_compl(nr_seq_laudo_p		number)
 		    	return varchar2 is
ds_retorno_w	varchar2(10);
begin

select	decode(nvl(nr_seq_superior,'0'),0,'N','S')
into	ds_retorno_w
from	laudo_paciente
where	nr_sequencia = nr_seq_laudo_p;

return	ds_retorno_w;

end obter_se_laudo_compl;
/