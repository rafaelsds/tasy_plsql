Create or replace
function pls_obter_dados_produto 
			(	nr_seq_produto_p	number,
				ie_opcao_p		varchar2)
				return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter dados do produto do benefici�rio
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	S - Segmenta��o assistencial
	C - Tipo de contrata��o
	A - Abrang�ncia geogr�fica
	AG - Abrang�ncia geogr�fica (valor do dominio)
	P - Forma��o do pre�o
	N - Nome do produto
	ANS - Registro ANS do Outorgante
	NF - Nome fantasia
	PA - Protocolo ANS
	CA - Condigo anterior
	R - Regulamenta��o
	PF - Participa��o financeira
	AP - Acomoda��o padr�o
	APD - Descri��o da acomoda��o padr�o
	CO - Combina��es opcionais
	DC - Descri��o coberturas
	SCPA - C�digo SCPA do produto
	DA - Descri��o acomoda��o
	IA - Informa��o acomoda��o
	TAP - Tipo Acomoda��o PTU
*/

ds_retorno_w			varchar2(255);
nm_fantasia_w			varchar2(255);
ds_combinacao_opcionais_w	varchar2(255);
ds_cobertura_aux_w		varchar2(255);
ds_cobertura_w			varchar2(255);
ds_retorno_aux_w		varchar2(255);
ds_plano_w			varchar2(80);
cd_ans_w			varchar2(20);
nr_protocolo_ans_w		varchar2(20);
cd_codigo_ant_w			varchar2(20);
ie_segmentacao_w		varchar2(3);
ie_abrangencia_w		varchar2(3);
ie_tipo_contratacao_w		varchar2(2);
ie_preco_w			varchar2(2);
ie_regulamentacao_w		varchar2(2);
ie_participacao_w		varchar2(1);
ie_situacao_w			varchar2(1);
ie_acomodacao_w			varchar2(1);
nr_seq_outorgante_w		number(10);
nr_seq_cobertura_aux_w		number(10);
nr_seq_tipo_acomodacao_w	number(10);

Cursor C01 is
	select	substr(pls_obter_dados_tipo_cobertura(nr_seq_tipo_cobertura, 'D'), 1, 255)
	from	pls_cobertura
	where	nr_seq_plano	= nr_seq_produto_p;

Cursor C02 is
	select	nr_sequencia
	from	pls_cobertura
	where	nr_seq_plano	= nr_seq_produto_p;

begin
if	(nr_seq_produto_p is not null) then
	select	ie_segmentacao,
		ie_tipo_contratacao,
		ie_abrangencia,
		ie_preco,
		ds_plano,
		nr_seq_outorgante,
		nr_protocolo_ans,
		nm_fantasia,
		cd_codigo_ant,
		ie_regulamentacao,
		ie_participacao,
		ds_combinacao_opcionais,
		ie_situacao,
		ie_acomodacao
	into	ie_segmentacao_w,
		ie_tipo_contratacao_w,
		ie_abrangencia_w,
		ie_preco_w,
		ds_plano_w,
		nr_seq_outorgante_w,
		nr_protocolo_ans_w,
		nm_fantasia_w,
		cd_codigo_ant_w,
		ie_regulamentacao_w,
		ie_participacao_w,
		ds_combinacao_opcionais_w,
		ie_situacao_w,
		ie_acomodacao_w
	from	pls_plano
	where	nr_sequencia		= nr_seq_produto_p;
	
	if	(ie_opcao_p = 'S') then
		select	substr(ds_valor_dominio, 1, 255)
		into	ds_retorno_w
		from	valor_dominio
		where	cd_dominio	= 1665
		  and	vl_dominio	= ie_segmentacao_w;
	elsif 	(ie_opcao_p = 'SCPA') then
		select 	cd_scpa
		into	ds_retorno_w
		from	pls_plano
		where	nr_sequencia = nr_seq_produto_p;
	elsif	(ie_opcao_p = 'C') then
		select	substr(ds_valor_dominio, 1, 255)
		into	ds_retorno_w
		from	valor_dominio
		where	cd_dominio	= 1666
		  and	vl_dominio	= ie_tipo_contratacao_w;
	elsif	(ie_opcao_p = 'CC') then
		ds_retorno_w := ie_tipo_contratacao_w;
	elsif	(ie_opcao_p = 'A') then
		select	substr(ds_valor_dominio, 1, 255)
		into	ds_retorno_w
		from	valor_dominio
		where	cd_dominio	= 1667
		  and	vl_dominio	= ie_abrangencia_w;
	elsif	(ie_opcao_p = 'P') then
		select	substr(ds_valor_dominio, 1, 255)
		into	ds_retorno_w
		from	valor_dominio
		where	cd_dominio	= 1669
		  and	vl_dominio	= ie_preco_w;
	elsif	(ie_opcao_p = 'N') then
		ds_retorno_w	:= ds_plano_w;
	elsif	(ie_opcao_p = 'ANS') then
		select	cd_ans
		into	cd_ans_w
		from	pls_outorgante
		where	nr_sequencia	= nr_seq_outorgante_w;
		ds_retorno_w	:= cd_ans_w;
	elsif	(ie_opcao_p = 'NF') then
		ds_retorno_w	:= nm_fantasia_w;
	elsif	(ie_opcao_p = 'PA') then
		ds_retorno_w	:= nr_protocolo_ans_w;
	elsif	(ie_opcao_p = 'CA') then
		ds_retorno_w	:= cd_codigo_ant_w;
	elsif 	(ie_opcao_p = 'R') then
		ds_retorno_w	:= ie_regulamentacao_w;
	elsif	(ie_opcao_p = 'PF') then
		ds_retorno_w	:= ie_participacao_w;
	elsif	(ie_opcao_p in ('AP','APD','TAP')) then
		select	max(nr_seq_tipo_acomodacao)
		into	nr_seq_tipo_acomodacao_w
		from	pls_plano_acomodacao
		where	nr_seq_plano		= nr_seq_produto_p
		and	ie_acomod_padrao	= 'S';
		
		if	(nvl(nr_seq_tipo_acomodacao_w,0) = 0) then
			select	max(a.nr_seq_tipo_acomodacao)
			into	nr_seq_tipo_acomodacao_w
			from	pls_plano_acomodacao 	c,
				pls_categoria		b,
				pls_regra_categoria	a
			where	c.nr_Seq_categoria	= b.nr_sequencia
			and	b.nr_sequencia		= a.nr_seq_categoria
			and	c.nr_seq_plano		= nr_seq_produto_p
			and	a.ie_acomod_padrao	= 'S'
			and	c.ie_acomod_padrao	= 'S';
			
			if 	(nvl(nr_seq_tipo_acomodacao_w, 0) = 0) then			
				select	max(a.nr_seq_tipo_acomodacao)
				into	nr_seq_tipo_acomodacao_w
				from	pls_plano_acomodacao 	c,
					pls_categoria		b,
					pls_regra_categoria	a
				where	c.nr_Seq_categoria	= b.nr_sequencia
				and	b.nr_sequencia		= a.nr_seq_categoria
				and	c.nr_seq_plano		= nr_seq_produto_p
				and	a.ie_acomod_padrao	= 'S';
			end if;
		end if;
		
		if	(ie_opcao_p = 'AP') then
			ds_retorno_w	:= nr_seq_tipo_acomodacao_w;
		elsif	(ie_opcao_p = 'APD') then
			select	max(ds_tipo_acomodacao)
			into	ds_retorno_w
			from	pls_tipo_acomodacao
			where	nr_sequencia	= nr_seq_tipo_acomodacao_w;
		elsif	(ie_opcao_p = 'TAP') then
			select	max(ie_tipo_acomodacao_ptu)
			into	ds_retorno_w
			from	pls_tipo_acomodacao
			where	nr_sequencia	= nr_seq_tipo_acomodacao_w;
		end if;
	elsif	(ie_opcao_p = 'CO') then
		ds_retorno_w	:= ds_combinacao_opcionais_w;
	elsif	(ie_opcao_p = 'DC') then
		open C01;
		loop
		fetch C01 into
			ds_cobertura_aux_w;
		exit when C01%notfound;
			begin
			ds_cobertura_w	:= ds_cobertura_w || substr(ds_cobertura_aux_w || ' ', 1, (255 - nvl(length(ds_cobertura_w), 0)));
			end;
		end loop;
		close C01;
		
		ds_retorno_w	:= ds_cobertura_w;
	elsif	(ie_opcao_p = 'NC') then
		open C02;
		loop
		fetch C02 into
			nr_seq_cobertura_aux_w;
		exit when C02%notfound;
			begin
			ds_retorno_aux_w	:= ds_retorno_aux_w || substr(to_char(nr_seq_cobertura_aux_w) || ', ',1,(255 - nvl(length(ds_retorno_aux_w), 0)));
			end;
		end loop;
		close C02;
		
		ds_retorno_w	:= substr(ds_retorno_aux_w, 1, length(ds_retorno_aux_w) -2);
	elsif	(ie_opcao_p = 'IS') then
		ds_retorno_w	:= ie_situacao_w;
		
	elsif	(ie_opcao_p = 'IC') then
		ds_retorno_w	:= ie_tipo_contratacao_w;
	elsif	(ie_opcao_p = 'IA') then
		ds_retorno_w	:= ie_acomodacao_w;
	elsif	(ie_opcao_p = 'DA') then
		select	decode(ie_acomodacao_w, 'I', 'Individual', 'C', 'Coletivo', 'Nenhum')
		into	ds_retorno_w
		from	dual;
	elsif	(ie_opcao_p = 'AG') then
		ds_retorno_w	:= ie_abrangencia_w;
	end if;
end if;

return ds_retorno_w;

end pls_obter_dados_produto;
/