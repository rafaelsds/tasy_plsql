create or replace
function Obter_proced_atend_pa(	nr_atendimento_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(2000);
ds_procedimento_w	varchar2(255);
cd_procedimento_w	number(15);

Cursor C01 is
	select	obter_descricao_procedimento(a.cd_procedimento, a.ie_origem_proced),
		a.cd_procedimento
	from	prescr_procedimento a,
		prescr_medica b
	where	a.nr_prescricao = b.nr_prescricao
	and	b.nr_atendimento = nr_atendimento_p
	and	obter_tipo_atendimento(b.nr_atendimento) = '3'
	order by 1;
	
begin

open C01;
loop
fetch C01 into	
	ds_procedimento_w,
	cd_procedimento_w;
exit when C01%notfound;
	begin
	ds_retorno_w := ds_retorno_w || cd_procedimento_w || '        ' || ds_procedimento_w || chr(13);
	end;
end loop;
close C01;

return	ds_retorno_w;

end Obter_proced_atend_pa;
/