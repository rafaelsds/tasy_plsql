create or replace
function pls_obter_seq_protocolo_ws
		(	nr_seq_protocolo_prest_p	varchar2,
			cd_cnpj_prest_p			varchar2,
			nr_cpf_prest_p			varchar2,
			cd_prestador_p			varchar2,
			cd_estabelecimento_p		number)
 		    	return number is
			

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter a sequencia da PLS_PROTOCOLO_CONTA conforme os parametros recebidos pelo WebSerivice do Prestador
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [ X ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */			
			
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
nr_seq_protocolo_w		pls_protocolo_conta.nr_sequencia%type;

begin

/*Obtem qual e o prestador*/
nr_seq_prestador_w	:= pls_obter_prestador_imp(cd_cnpj_prest_p,nr_cpf_prest_p,cd_prestador_p,null,null,null,'C',cd_estabelecimento_p);

/*Busca o protocolo conforme o numero do protocolo prestador e o prestador importado de referencia*/
select	max(nr_sequencia)
into	nr_seq_protocolo_w
from	pls_protocolo_conta
where	nr_protocolo_prestador		= nr_seq_protocolo_prest_p
and	nr_seq_prestador_imp_ref	= nr_seq_prestador_w;

/*Caso nao achar, busca o protocolo conforme o numero do protocolo prestador e o prestador da tabela*/
if	(nr_seq_protocolo_w is null) then
	select	max(nr_sequencia)
	into	nr_seq_protocolo_w
	from	pls_protocolo_conta
	where	nr_protocolo_prestador	= nr_seq_protocolo_prest_p
	and	nr_seq_prestador	= nr_seq_prestador_w;

	/*Caso nao achar, busca o protocolo conforme o numero de sequencia do protocolo e o prestador importador de referencia da tabela*/
	if	(nr_seq_protocolo_w is null) then
		select	max(nr_sequencia)
		into	nr_seq_protocolo_w
		from	pls_protocolo_conta
		where	to_char(nr_sequencia)		= nr_seq_protocolo_prest_p
		and	nr_seq_prestador_imp_ref	= nr_seq_prestador_w;
	
		/*Caso nao achar, busca o protocolo conforme o numero de sequencia do protocolo e o prestador da tabela*/
		if	(nr_seq_protocolo_w is null) then
			select	max(nr_sequencia)
			into	nr_seq_protocolo_w
			from	pls_protocolo_conta
			where	to_char(nr_sequencia)	= nr_seq_protocolo_prest_p
			and	nr_seq_prestador	= nr_seq_prestador_w;

			/*Caso nao achar, busca o protocolo de recurso de glosa conforme o numero de sequencia do protocolo e o prestador da tabela*/
			if (nr_seq_protocolo_w is null) then
				select	max(nr_sequencia)
				into	nr_seq_protocolo_w
				from	pls_rec_glosa_protocolo
				where	to_char(nr_sequencia) = nr_seq_protocolo_prest_p
				and	nr_seq_prestador = nr_seq_prestador_w;
			end if;      

		end if;
	end if;
end if;

return	nr_seq_protocolo_w;

end pls_obter_seq_protocolo_ws;
/