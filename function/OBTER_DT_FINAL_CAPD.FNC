create or replace
function Obter_Dt_final_CAPD(	dt_horario_p	varchar2,
				qt_minutos_p	number)
 		    	return varchar2 is

dt_fim_w	varchar2(5);

begin

select	to_char(to_date(dt_horario_p,'hh24:mi') + qt_minutos_p/1440,'hh24:mi')
into	dt_fim_w
from	dual;

return	dt_fim_w;

end Obter_Dt_final_CAPD;
/
