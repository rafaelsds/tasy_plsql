create or replace
function pls_obter_regra_recalculo_fixo 
			(	dt_mes_competencia_p		Date,
				cd_medico_executor_p		Varchar2,
				nr_seq_prestador_exec_p		Number)
				return Number is
				
nr_seq_regra_w			Number(10);
 
begin

if	(nvl(cd_medico_executor_p,'X') <> 'X') then	
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	pls_prestador_pgto_fixo
	where	trunc(dt_mes_competencia_p) between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, dt_mes_competencia_p))
	and	cd_pessoa_fisica	= cd_medico_executor_p
	and	nr_seq_prestador_pf	= nr_seq_prestador_exec_p;

	if	(nvl(nr_seq_regra_w,0) = 0) then
		select	max(nr_sequencia)
		into	nr_seq_regra_w
		from	pls_prestador_pgto_fixo
		where	trunc(dt_mes_competencia_p) between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, dt_mes_competencia_p))
		and	cd_pessoa_fisica	= cd_medico_executor_p
		and	nr_seq_prestador	= nr_seq_prestador_exec_p;
		
		if	(nvl(nr_seq_regra_w,0) = 0) then
			select	max(nr_sequencia)
			into	nr_seq_regra_w
			from	pls_prestador_pgto_fixo
			where	trunc(dt_mes_competencia_p) between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, dt_mes_competencia_p))
			and	cd_pessoa_fisica	= cd_medico_executor_p
			and	nr_seq_prestador	is null
			and	nr_seq_prestador_pf	is null;
		
		end if;
	end if;
end if;

if	(nvl(nr_seq_regra_w,0)	= 0) then
	select	max(nr_sequencia) 
	into	nr_seq_regra_w
	from	pls_prestador_pgto_fixo
	where	nr_seq_prestador	= nr_seq_prestador_exec_p
	and 	cd_pessoa_fisica	is null
	and 	nr_seq_prestador_pf	is null
	and	trunc(dt_mes_competencia_p) between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, dt_mes_competencia_p));
	if	(nvl(nr_seq_regra_w,0)	= 0) then
		select	max(nr_sequencia) 
		into	nr_seq_regra_w
		from	pls_prestador_pgto_fixo
		where	nr_seq_prestador	= nr_seq_prestador_exec_p
		and 	cd_pessoa_fisica	is null
		and 	nr_seq_prestador_pf	= nr_seq_prestador_exec_p
		and	trunc(dt_mes_competencia_p) between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, dt_mes_competencia_p));
	end if;
end if;	

return	nr_seq_regra_w;

end pls_obter_regra_recalculo_fixo;
/