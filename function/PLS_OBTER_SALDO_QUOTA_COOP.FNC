create or replace
function pls_obter_saldo_quota_coop(	nr_seq_cooperado_p	varchar2)
 		    	return number is

vl_saldo_acumulado_w		number(15,2);			
			
begin

select	sum(a.vl_escrituracao)
into	vl_saldo_acumulado_w
from	pls_escrituracao_quota a
where	a.nr_seq_cooperado	= nr_seq_cooperado_p
and	(dt_geracao_titulos is not null or exists (	select	1
							from	pls_tipo_escrit_quota x
							where	x.nr_sequencia	= a.nr_seq_tipo_escrit_quota
							and	x.ie_tipo_escrituracao in ('AS','AC','DS')));

return	vl_saldo_acumulado_w;

end pls_obter_saldo_quota_coop;
/