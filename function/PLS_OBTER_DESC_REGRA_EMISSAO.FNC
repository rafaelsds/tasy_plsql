create or replace
function pls_obter_desc_regra_emissao
			(	nr_seq_regra_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin

begin
select	ds_regra
into	ds_retorno_w
from	pls_regra_emissao_carteira
where	nr_sequencia	= nr_seq_regra_p;
exception
when others then
	ds_retorno_w	:= null;
end;

return	ds_retorno_w;

end pls_obter_desc_regra_emissao;
/