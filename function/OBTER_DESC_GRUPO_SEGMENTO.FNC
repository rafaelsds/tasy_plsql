create or replace
function obter_desc_grupo_segmento(	nr_sequencia_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);
	
begin
begin
select	ds_grupo
into	ds_retorno_w
from	grupo_segmento_compras
where	nr_sequencia = nr_sequencia_p;
exception
when others then
	ds_retorno_w := null;
end;

return	ds_retorno_w;
end obter_desc_grupo_segmento;
/