CREATE OR REPLACE
PROCEDURE update_laudo_pac_impressao(nr_seq_laudo VARCHAR2, ie_opcao VARCHAR2, nr_seq_laudos VARCHAR2) IS

BEGIN
		
	UPDATE  laudo_paciente 
	SET     ie_impresso = ie_opcao
	WHERE   nr_sequencia = nr_seq_laudo OR nr_sequencia = nr_seq_laudos;

COMMIT;

end update_laudo_pac_impressao;
/