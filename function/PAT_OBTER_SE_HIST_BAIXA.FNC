create or replace
function pat_obter_se_hist_baixa(nr_seq_tipo_p	number)
				return varchar2	is

ds_retorno_w		varchar2(5);
ds_valor_w		varchar2(5);

begin

ds_retorno_w := 'N';

select	max(ie_valor)
into	ds_valor_W
from	pat_tipo_historico
where	nr_sequencia	= nr_seq_tipo_p
and	ie_transferencia= 'N';

if	(nvl(ds_valor_w,'x') = 'B') then
	ds_retorno_w := 'S';
end if;

return ds_retorno_w;

end pat_obter_se_hist_baixa;
/