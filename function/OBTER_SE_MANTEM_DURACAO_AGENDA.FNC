create or replace
function obter_se_mantem_duracao_agenda	(cd_agenda_p		number,
						nr_seq_agenda_p	number,
						dt_agenda_p		date,
						nr_min_duracao_p	number)
						return varchar2 is

ie_manter_duracao_w	varchar2(1) := 'N';

begin
if	(cd_agenda_p is not null) and
	(nr_seq_agenda_p is not null) and
	(dt_agenda_p is not null) and
	(nr_min_duracao_p is not null) then
	select	decode(count(*),0,'S','N')
	into	ie_manter_duracao_w
	from	agenda_paciente
	where	cd_agenda = cd_agenda_p
	and	ie_status_agenda not in ('C','L','LF')
	--and	hr_inicio between dt_agenda_p and dt_agenda_p + nr_min_duracao_p / 1440;
	and	trunc(dt_agenda,'dd') = trunc(dt_agenda_p,'dd')
	and	hr_inicio > dt_agenda_p
	and	dt_agenda_p + nr_min_duracao_p / 1440 > hr_inicio;
end if;

return ie_manter_duracao_w;

end obter_se_mantem_duracao_agenda;
/