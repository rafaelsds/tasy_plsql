create or replace
function obter_ordenacao_grid_mat(cd_funcao_p varchar2,
				  ds_grid_p 	varchar2,
				  nm_usuario_p varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(200) := '';

begin

	select	max(ds_regra_ordem)
	into    ds_retorno_w
	from	usuario_ordem_grid
	where 	cd_funcao = cd_funcao_p
	and 	ds_grid_tabela = ds_grid_p
	and	nm_usuario = nm_usuario_p;


return	ds_retorno_w;

end obter_ordenacao_grid_mat;
/