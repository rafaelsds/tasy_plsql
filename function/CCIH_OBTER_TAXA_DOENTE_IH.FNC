create or replace
function ccih_obter_taxa_doente_ih	(dt_inicial_p		date,
					dt_final_p		date,
					cd_estabelecimento_p	number)
 		    	return number is

qt_total_fichas_mes_w	number(10);
qt_total_alta_obito_w	number(10);
vl_taxa_doente_w	number(10,2);
begin
if	(dt_inicial_p is not null) and (dt_final_p is not null) then
	--Total de Fichas de Ocorr�ncia Mensal
	select	count(*)
	into	qt_total_fichas_mes_w
	from	cih_ficha_ocorrencia a,
		atendimento_paciente b
	where	a.nr_atendimento	= b.nr_atendimento
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	exists	(select	1
			from	cih_local_infeccao c
			where	c.nr_ficha_ocorrencia = a.nr_ficha_ocorrencia
			and	c.dt_infeccao between trunc(dt_inicial_p) and trunc(dt_final_p)+86399/86400);
	
	--Total de Altas + �bitos
	select	(sum(x.nr_altas) + sum(x.nr_obitos))
	into	qt_total_alta_obito_w
	from	eis_ocupacao_setor_v x
	where	x.ie_periodo = 'M'
	and	dt_referencia between trunc(dt_inicial_p) and trunc(dt_final_p)+86399/86400
	and	((x.cd_estabelecimento = cd_estabelecimento_p) or (cd_estabelecimento_p = '0'));
	
	


	vl_taxa_doente_w	:=	(qt_total_fichas_mes_w / qt_total_alta_obito_w)*100;

end if;

return	vl_taxa_doente_w;

end ccih_obter_taxa_doente_ih;
/