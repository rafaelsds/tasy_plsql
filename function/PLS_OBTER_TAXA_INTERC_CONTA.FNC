create or replace
function pls_obter_taxa_interc_conta
			(	dt_conta_p			date,
				dt_prev_envio_p			date,
				nr_seq_congenere_p		number,
				nr_seq_segurado_p		number)
			return number is 

pr_taxa_w			number(15,2);
nr_seq_intercambio_w		number(10);
cd_estabelecimento_w		number(4);
ie_taxa_w			Varchar2(10);
qt_dia_proced_receb_w		number(10);

begin

begin
select	max(nr_seq_intercambio),
	max(cd_estabelecimento)
into	nr_seq_intercambio_w,
	cd_estabelecimento_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;
exception
when others then
	nr_seq_intercambio_w	:= null;
end;


qt_dia_proced_receb_w	:= (trunc(dt_prev_envio_p) - trunc(dt_conta_p));


pls_obter_taxa_operadora('P', qt_dia_proced_receb_w, nr_seq_congenere_p, cd_estabelecimento_w, nr_seq_segurado_p, dt_conta_p, '', null,null,pr_taxa_w, ie_taxa_w);

return pr_taxa_w;

end pls_obter_taxa_interc_conta;
/
