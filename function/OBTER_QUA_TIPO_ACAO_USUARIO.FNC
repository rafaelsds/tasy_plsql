CREATE OR REPLACE
FUNCTION obter_qua_tipo_acao_usuario(	nm_usuario_p	Varchar2,
					nr_seq_tipo_p	Number,
					ie_tipo_p		Varchar2)
					return varchar2 is

ie_registrar_w		varchar2(1);
ie_causa_acao_w		varchar2(1);
ie_definir_eficacia_w	varchar2(1);
ie_cancelar_w		varchar2(1);
ie_fechar_w		varchar2(1);
ds_retorno_w		varchar2(1);
cd_perfil_w		number(05,0);
cd_perfil_ww		number(05,0);
nm_usuario_lib_ww		varchar2(15);
ie_visualizar_w		varchar2(01) := 'S';

/* ie_tipo
NC - Abrir N�o Conformidade
CA - Abrir Causas e A��es
DE - Definir Efic�cia
PC - Permite Cancelar
PF - Permite Fechar
PV - Permite Visualizar
*/

Cursor C01 is
	select	nvl(ie_registrar,'N'),
		nvl(ie_causa_acao,'N'),
		nvl(ie_definir_eficacia,'N'),
		nvl(ie_cancelar,'N'),
		nvl(ie_fechar,'N'),
		cd_perfil,
		nm_usuario_lib,
		nvl(ie_visualizar,'S')
	from	qua_tipo_nconf_usuario
	where	nm_usuario_lib		= nm_usuario_p
	and	nr_seq_nconform		= nr_seq_tipo_p
	union
	select	nvl(ie_registrar,'N'),
		nvl(ie_causa_acao,'N'),
		nvl(ie_definir_eficacia,'N'),
		nvl(ie_cancelar,'N'),
		nvl(ie_fechar,'N'),
		cd_perfil,
		nm_usuario_lib,
		nvl(ie_visualizar,'S')
	from	qua_tipo_nconf_usuario
	where	nr_seq_nconform		= nr_seq_tipo_p
	and     cd_perfil			= cd_perfil_w
	order by cd_perfil,
		nm_usuario_lib;

BEGIN

cd_perfil_w := obter_perfil_ativo;

open c01;
loop
fetch c01 into
	ie_registrar_w,
	ie_causa_acao_w,
	ie_definir_eficacia_w,
	ie_cancelar_w,
	ie_fechar_w,
	cd_perfil_ww,
	nm_usuario_lib_ww,
	ie_visualizar_w;
exit when c01%notfound;
	begin
	if	(ie_tipo_p = 'NC') then
		ds_retorno_w	:= ie_registrar_w;
	elsif	(ie_tipo_p = 'CA') then
		ds_retorno_w	:= ie_causa_acao_w;
	elsif	(ie_tipo_p = 'DE') then
		ds_retorno_w	:= ie_definir_eficacia_w;
	elsif	(ie_tipo_p = 'PC') then
		ds_retorno_w	:= ie_cancelar_w;
	elsif	(ie_tipo_p = 'PF') then
		ds_retorno_w	:= ie_fechar_w;
	elsif	(ie_tipo_p = 'PV') then
		ds_retorno_w	:= ie_visualizar_w;
	end if;
	end;
end loop;
close c01;

return	ds_retorno_w;

END obter_qua_tipo_acao_usuario;
/