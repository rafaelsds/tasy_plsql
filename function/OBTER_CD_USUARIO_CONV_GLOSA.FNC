CREATE OR REPLACE FUNCTION 
	OBTER_CD_USUARIO_CONV_GLOSA (	CD_CONVENIO_P	NUMBER,
					NR_ATENDIMENTO_P	NUMBER)
						RETURN VARCHAR2 IS	

CD_USUARIO_W		VARCHAR2(60);

BEGIN
SELECT	NVL(MAX(CD_USUARIO_CONV_GLOSA) || max(CD_COMPLEMENTO_GLOSA),0)
INTO	CD_USUARIO_W
FROM	ATEND_CATEGORIA_CONVENIO
WHERE	NR_ATENDIMENTO 	= NR_ATENDIMENTO_P
AND 	CD_CONVENIO		= CD_CONVENIO_P
AND 	DT_INICIO_VIGENCIA	= (	SELECT MAX(X.DT_INICIO_VIGENCIA)
					FROM ATEND_CATEGORIA_CONVENIO X
					WHERE X.NR_ATENDIMENTO	= NR_ATENDIMENTO_P
					AND X.CD_CONVENIO		= CD_CONVENIO_P);

RETURN CD_USUARIO_W;
END OBTER_CD_USUARIO_CONV_GLOSA;
/
