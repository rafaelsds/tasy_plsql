create or replace
function pharm_obter_saldo_disp_estoque(cd_material_p		number,
										cd_local_estoque_p	number,
										cd_estabelecimento_p	number,
										nr_seq_lote_p		number default null)
										return number is
				
qt_estoque_w			number(15,4);
ie_consignado_w			material.ie_consignado%type;
ie_tipo_saldo_w			varchar2(1);
cd_cgc_fornec_w			material_lote_fornec.cd_cgc_fornec%type;
dt_mesano_ref_saldo_w	date;
qt_estoque_consig_w     number(15,4) := 0;

begin

qt_estoque_w := 0;
ie_tipo_saldo_w := 'X';

select	nvl(max(ie_consignado),'0')
into	
	ie_consignado_w
from	material
where	cd_material = cd_material_p;

if	(ie_consignado_w = '2') then	
	begin
		begin
		select	max(f.dt_mesano_referencia)
		into	dt_mesano_ref_saldo_w
		from	fornecedor_mat_consignado f
		where	f.cd_estabelecimento = cd_estabelecimento_p
		and		f.cd_local_estoque = nvl(cd_local_estoque_p,  f.cd_local_estoque)
		and		f.cd_material = cd_material_p;
		exception
			when others then
				dt_mesano_ref_saldo_w := PKG_DATE_UTILS.start_of(sysdate, 'MONTH', 0);
		end;
		begin
		select 	sum(f.qt_estoque)
		into	qt_estoque_consig_w
		from 	fornecedor_mat_consignado f
		where 	f.cd_estabelecimento = cd_estabelecimento_p
		and 	f.cd_material = cd_material_p
		and 	f.cd_local_estoque = nvl(cd_local_estoque_p, f.cd_local_estoque)
		and 	f.dt_mesano_referencia = dt_mesano_ref_saldo_w;
		exception
			when others then
				qt_estoque_consig_w := 0;
		end;
		
		obter_saldo_estoque(cd_estabelecimento_p, cd_material_p, cd_local_estoque_p, PKG_DATE_UTILS.start_of(sysdate, 'month', 0), qt_estoque_w);

		qt_estoque_w := nvl(qt_estoque_w + nvl(qt_estoque_consig_w, 0),0);
				
	end;
elsif (ie_consignado_w = '1') then
	begin
		begin
		select	max(f.dt_mesano_referencia)
		into	dt_mesano_ref_saldo_w
		from	fornecedor_mat_consignado f
		where	f.cd_estabelecimento = cd_estabelecimento_p
		and		f.cd_local_estoque = nvl(cd_local_estoque_p,  f.cd_local_estoque)
		and		f.cd_material = cd_material_p;
		exception
			when others then
				dt_mesano_ref_saldo_w := PKG_DATE_UTILS.start_of(sysdate, 'MONTH', 0);
		end;
		begin
		select 	sum(f.qt_estoque)
		into	qt_estoque_consig_w
		from 	fornecedor_mat_consignado f
		where 	f.cd_estabelecimento = cd_estabelecimento_p
		and 	f.cd_material = cd_material_p
		and 	f.cd_local_estoque = nvl(cd_local_estoque_p, f.cd_local_estoque)
		and 	f.dt_mesano_referencia = dt_mesano_ref_saldo_w;
		exception
			when others then
				qt_estoque_consig_w := 0;
		end;	
		qt_estoque_w := nvl(qt_estoque_consig_w,0);
	end;
elsif (ie_consignado_w = '0') then
	begin
		qt_estoque_w :=	nvl(obter_saldo_disp_estoque(
			cd_estabelecimento_p	=> cd_estabelecimento_p,
			cd_material_p		=> cd_material_p,
			cd_local_estoque_p	=> cd_local_estoque_p,
			dt_mesano_referencia_p	=> PKG_DATE_UTILS.start_of(sysdate, 'month', 0)),0);
	end;
end if;

return qt_estoque_w;

end pharm_obter_saldo_disp_estoque;
/
