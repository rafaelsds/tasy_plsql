create or replace
function obter_se_link_liberado	(nr_seq_link_p		number,
				cd_pessoa_fisica_p	varchar2,
				cd_perfil_p		number)
				return	varchar2 is
				
ie_possui_regra_w	varchar2(1);
ie_liberado_w		varchar2(1):= 'S';

begin

select	decode(count(*),0,'N','S') 
into	ie_possui_regra_w
from	link_base_dados_farm_lib
where	nr_seq_link = nr_seq_link_p;

if	(ie_possui_regra_w = 'S') then
	select	decode(count(*),0,'N','S') 
	into	ie_liberado_w
	from	link_base_dados_farm_lib
	where	nr_seq_link = nr_seq_link_p
	and	nvl(cd_pessoa_fisica,cd_pessoa_fisica_p)= cd_pessoa_fisica_p
	and	nvl(cd_perfil,cd_perfil_p)		= cd_perfil_p;
end if;

return	ie_liberado_w;

end	obter_se_link_liberado;
/