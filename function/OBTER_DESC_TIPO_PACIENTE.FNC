create or replace
function Obter_Desc_Tipo_Paciente(ie_tipo_paciente_p	varchar2)
 		    	return varchar2 is

ds_tipo_paciente_w	    varchar2(10);
			
begin

if	ie_tipo_paciente_p = 'CI' then
		ds_tipo_paciente_w := wheb_mensagem_pck.get_texto(308140); -- Cir�rgico
elsif   ie_tipo_paciente_p = 'CL' then  
		ds_tipo_paciente_w := wheb_mensagem_pck.get_texto(308141); -- Cl�nico
end if;
		
return	ds_tipo_paciente_w;

end Obter_Desc_Tipo_Paciente;
/