create or replace
function Nut_obter_se_nova_prescr(	nr_seq_mapa_p		Number)
				return Varchar2 is

ds_retorno_w		Varchar2(1) := 'N';
nr_atendimento_w	Number(10);
dt_liberacao_dieta_w	Date;
				
begin

if (nr_seq_mapa_p is not null) then
	
	select	max(nr_atendimento),
		max(dt_liberacao)
	into	nr_atendimento_w,
		dt_liberacao_dieta_w
	from	mapa_dieta
	where	nr_sequencia = nr_seq_mapa_p;

	select	decode(count(*),0,'N','S')
	into	ds_retorno_w 
	from	prescr_medica a
	where 	a.nr_atendimento = nr_atendimento_w
	and 	a.dt_prescricao	> dt_liberacao_dieta_w
	and	a.dt_validade_prescr > sysdate
	and 	(exists(select	1
			from	prescr_dieta b
			where	a.nr_prescricao = b.nr_prescricao)
	or	exists(	select	1
			from	prescr_material b
			where	a.nr_prescricao = b.nr_prescricao
			and	b.ie_agrupador in (8,12)));
end if;

return	ds_retorno_w;

end Nut_obter_se_nova_prescr;
/