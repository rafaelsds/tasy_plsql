CREATE OR REPLACE FUNCTION cpoe_si_format_value (
    nr_sequencia_p NUMBER
) RETURN VARCHAR2 IS
    ie_format_value_w VARCHAR2(1);
BEGIN
    SELECT
        MAX(si_format_value)
    INTO ie_format_value_w
    FROM
        cpoe_std_comment a
    WHERE
        a.nr_sequencia = nr_sequencia_p;

    RETURN ie_format_value_w;
END cpoe_si_format_value;
/
