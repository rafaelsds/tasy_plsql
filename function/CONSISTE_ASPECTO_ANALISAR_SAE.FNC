create or replace
function Consiste_aspecto_analisar_SAE(	nr_prescricao_p		number,
										cd_empresa_p		number,
										nr_seq_modelo_p		number,
										cd_setor_pac_p		number,
										ie_opcao_p			varchar2) 
 		    	return Varchar2 is
retorno_w		varchar2(4000) := 'N';
pendentes_w		varchar2(4000);
todos_pendentes_w	varchar2(4000);
vl_parametro_w		varchar2(5);
total_pendentes_w	number(10) := 0;

/*ie_opcao_p
	C = Consistir;
	I = Itens Consistir;*/

Cursor C01 is
	select	distinct a.ds_tipo_item||':'||b.ds_item
	from	pe_item_tipo_item c,
			pe_item_examinar b, 
			pe_tipo_item a,
			pe_sae_mod_item d	
	where	a.cd_empresa	= cd_empresa_p
	and	a.nr_sequencia	= c.nr_seq_tipo_item
	and	b.nr_sequencia	= c.nr_seq_item
	and	b.nr_sequencia	= d.nr_seq_item(+)
	and	((nr_seq_modelo_p = 0) or (d.nr_seq_modelo = nr_seq_modelo_p))
	and	((d.cd_setor_pac = cd_setor_pac_p) or (d.cd_setor_pac is null))
	and	b.nr_sequencia not in (	select   distinct(nr_seq_item)
								from     pe_prescr_item_result
								where    nr_seq_prescr = nr_prescricao_p
								union
								select  distinct(nr_seq_item)
								from    pe_result_item_diag b,
										pe_prescr_item_result a
								where   b.nr_seq_result = a.nr_seq_result
								and		nr_seq_prescr 	= nr_prescricao_p)
	order by 1;

begin

select nvl(obter_valor_param_usuario(-1004, 2, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento), 'N')
into   vl_parametro_w
from   dual;

if (vl_parametro_w = 'N') then
    retorno_w := 'S';
else
    
    open C01;
    loop
    fetch C01 into	
        pendentes_w;
    exit when C01%notfound;
        begin
            if (pendentes_w is not null) then
                todos_pendentes_w := substr(todos_pendentes_w||pendentes_w||';'||chr(10)||chr(13),1,3900);
                total_pendentes_w := total_pendentes_w + 1;
            end if;
        end;
    end loop;
    close C01;
    
    if (ie_opcao_p = 'V') then
        if (total_pendentes_w = 0) then
            retorno_w := 'S';
        end if;
    end if;
    
        if (ie_opcao_p = 'I') then
                retorno_w	:= todos_pendentes_w;
        end if;
        
end if;

return	retorno_w;

end Consiste_aspecto_analisar_SAE;
/
