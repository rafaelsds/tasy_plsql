create or replace
function obter_cor_regra_meta_grupo(
			nr_seq_grupo_des_p	number,
			nm_usuario_p		varchar2,
			qt_atual_p		number,
			ie_opcao_p		varchar2)
		return varchar2 is

/* ie_opcao_p
COR	= Cor referente a regra
PER	= Percentual de diferenša
INI	= Quantidade inicio dia
ATU	= Quantidade Atual*/


nr_sequencia_w		number(10);
nr_seq_grupo_w		number(10);
ds_retorno_w		varchar2(255);
ds_cor_w			varchar2(255);
qt_ordem_inicio_dia_w	number(5);
qt_ordem_servico_w	number(5);
pr_diferenca_w		number(5,2);
pr_diferenca_ww		number(5,2);


begin

qt_ordem_servico_w	:= qt_atual_p;

if	(nr_seq_grupo_des_p > 0) then
	nr_seq_grupo_w	:= nr_seq_grupo_des_p;
else
	nr_seq_grupo_w	:= obter_grupo_usuario_wheb(nm_usuario_p);
end if;

select	nvl(sum(qt_ordem_servico), 0)
into	qt_ordem_inicio_dia_w
from	man_regra_resumo_meta
where	dt_referencia = OBTER_INICIO_SEMANA(trunc(sysdate, 'dd'))
and	nr_seq_grupo_des = nr_seq_grupo_w;

pr_diferenca_w		:= obter_variacao_entre_valor(qt_ordem_inicio_dia_w, qt_ordem_servico_w);
pr_diferenca_ww		:= null;
if	(pr_diferenca_w < 0) then
	pr_diferenca_ww	:= abs(pr_diferenca_w);
end if;


select	nvl(max(nr_sequencia), 0)
into	nr_sequencia_w
from	man_regra_cor_meta
where	pr_diferenca_w between nvl(pr_diferenca_min, 0) and nvl(pr_diferenca_max, 99999);

if	(nr_sequencia_w > 0) then
	select	ds_cor
	into	ds_cor_w
	from	man_regra_cor_meta
	where	nr_sequencia	= nr_sequencia_w;
end if;

if	(ie_opcao_p = 'COR') then
	ds_retorno_w	:= ds_cor_w;
elsif	(ie_opcao_p = 'PER') then
	ds_retorno_w	:= nvl(pr_diferenca_ww, pr_diferenca_w);
elsif	(ie_opcao_p = 'INI') then
	ds_retorno_w	:= qt_ordem_inicio_dia_w;
elsif	(ie_opcao_p = 'ATU') then
	ds_retorno_w	:= qt_ordem_servico_w;
end if;

return ds_retorno_w;

end obter_cor_regra_meta_grupo;
/