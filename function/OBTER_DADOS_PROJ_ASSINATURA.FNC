create or replace function obter_dados_proj_assinatura(	cd_tipo_registro_p	varchar2,
					ie_opcao_p		varchar2,
					ie_escala_p		varchar2,
					ie_retorna_projeto_p	 varchar2 default null)
						return varchar2 is

/*
Realizado essas customizacao devido a aplicacao em weblogic, onde atraves do java nao busca as informacoes no wheb_assist_pck

Descricao 					ie_opcao_p
wheb_assist_pck.get_se_gera_assinatura		A
*/


ds_retorno_w			varchar2(255) := 'N';
nr_seq_projeto_w		number(10);
cd_perfil_w			number(10);
ie_assinatura_w			varchar2(10) := 'N';
cd_estabelecimento_w		number(4);
ie_gerar_pend_sem_assinat_w	varchar2(10);


cursor c01 is
select	a.ie_assinatura
from	tasy_proj_assin_perfil a,
		tasy_projeto_assinatura b
where 	a.nr_seq_proj  = b.nr_sequencia
and 	b.nr_sequencia  = nr_seq_projeto_w
and 	nvl(a.cd_perfil, nvl(cd_perfil_w,0)) = nvl(cd_perfil_w,0)
order by nvl(a.cd_perfil,0) desc;



begin
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

select	nvl(max(IE_GERAR_PEND_SEM_ASSINAT),'N')
into	ie_gerar_pend_sem_assinat_w
from	parametro_medico
where	cd_estabelecimento  = cd_estabelecimento_w;


if	((cd_tipo_registro_p is not null) and
	(ie_opcao_p is not null)  and
	((obter_se_certificado_assinat is not null) or (ie_gerar_pend_sem_assinat_w = 'S')))then


	If	(ie_opcao_p = 'A') then

		if	(cd_tipo_registro_p = 'XE') then -- Evolucoes

			nr_seq_projeto_w := 183;

		elsif	(cd_tipo_registro_p = 'XAP') then -- Anamneses

			nr_seq_projeto_w := 25172;

		elsif	(cd_tipo_registro_p = 'XALA') then -- Alertas do paciente

			nr_seq_projeto_w := 53662;

		elsif	(cd_tipo_registro_p = 'XALP') then -- Alertas do paciente

			nr_seq_projeto_w := 53658;

		elsif	(cd_tipo_registro_p = 'XQLAN') THEN -- Quimioterapia Anotacoes

	          nr_seq_projeto_w := 51590;

		elsif	(cd_tipo_registro_p = 'XAV') then  -- Avaliacoes

			nr_seq_projeto_w := 50718;

		elsif	(cd_tipo_registro_p = 'XSV') then  -- Sinais vitais

			nr_seq_projeto_w := 184;

		elsif	(cd_tipo_registro_p = 'XGP') then  -- Perdas e ganhos
			if (obter_funcao_ativa = 10026) then
				nr_seq_projeto_w := 52449;
			else
				nr_seq_projeto_w := 36671;
			end if;
		elsif	(cd_tipo_registro_p = 'XD') then -- Diagnosticos

			nr_seq_projeto_w := 25173;

		elsif	(cd_tipo_registro_p = 'XESC') then -- Escalas e indices

			if	((ie_escala_p = '23') or (ie_escala_p = '39')) then -- Avaliacao nutricional

				nr_seq_projeto_w := 51172;

			elsif	(ie_escala_p = '3') then -- Escala de coma de Glasgow

				nr_seq_projeto_w := 51346;

            elsif	(ie_escala_p = '105') then -- Escala de Toxidade

				nr_seq_projeto_w := 53214;

			elsif	(ie_escala_p = '13') then -- Escore de Framingham revisado - ATP III - 2001 (Risco de DAC em 10 anos - morte ou IAM nao fatal)

				nr_seq_projeto_w := 51440;

			elsif	(ie_escala_p = '14') then -- Teste de Fargestrom - Avaliacao do grau de dependencia a nicotina

				nr_seq_projeto_w := 51440;

			elsif	(ie_escala_p = '18') then -- SOFA - Sepsis-related organ failure assessment

				nr_seq_projeto_w := 51486;

			elsif	(ie_escala_p = '140') then -- PEP - ADL - Escala PEWS

				nr_seq_projeto_w := 53697;

			elsif	(ie_escala_p = '75') then -- PEP - ADL - Escala Richmond/RASS

				nr_seq_projeto_w := 53699;

			elsif	(ie_escala_p = '168') then -- SOFA - Sepsis-related organ failure assessment

				nr_seq_projeto_w := 51486;

			elsif	(ie_escala_p = '24') then -- NAS - Nursing activities score

				nr_seq_projeto_w := 51486;

			elsif	(ie_escala_p = '40') then -- Karnofsky

				nr_seq_projeto_w := 51412;

			elsif	(ie_escala_p = '28') then -- MUST - Malnutrition Universal Screening Tool

				nr_seq_projeto_w := 51412;

			elsif	(ie_escala_p = '2') then -- Escala de Braden

				nr_seq_projeto_w := 51412;

			elsif	(ie_escala_p = '183') then -- Escala de News

				nr_seq_projeto_w := 53476;

			elsif	(ie_escala_p = '4') then -- Escala de Complexidade Assistencial (Sistema de Classificacao de Pacientes) - Enfermagem

				nr_seq_projeto_w := 51352;

			elsif	(ie_escala_p = '37') then -- Escala de avaliacao da qualidade de vida

				nr_seq_projeto_w := 51436;

			elsif	(ie_escala_p = '41') then -- Escala de ansiedade e depressao em pacientes

				nr_seq_projeto_w := 51418;

			elsif	(ie_escala_p = '43') then -- ECOG

				nr_seq_projeto_w := 51474;

			elsif	(ie_escala_p = '57') then -- Sistema de apoio a decisao para indicacao de profilaxia de tromboembolismo venoso (TEV)

				nr_seq_projeto_w := 51193;

			elsif	(ie_escala_p = '60') then -- Mini Nutritional Assessment -  Long Form

				nr_seq_projeto_w := 51430;

			elsif	(ie_escala_p = '61') then -- MELD

				nr_seq_projeto_w := 51340;

			elsif	(ie_escala_p = '67') then -- Escala de Braden (Criancas)

				nr_seq_projeto_w := 51350;

			elsif	(ie_escala_p = '70') then -- Medida de Independencia Funcional - MIF

				nr_seq_projeto_w := 51414;

			elsif	(ie_escala_p = '71') then -- Escala de Steward - Recuperacao Pos-Anestesica

				nr_seq_projeto_w := 51337;

			elsif	(ie_escala_p = '73') then -- Cincinnati Prehospital Stroke Scale - CPSS

				nr_seq_projeto_w := 51344;

			elsif	(ie_escala_p = '79') then -- Escala de atividades Basicas de Vida Diaria - Katz

				nr_seq_projeto_w := 51424;

			elsif	(ie_escala_p = '80') then -- AKIN - Acute Kidney Injury Network

				nr_seq_projeto_w := 51478;

			elsif	(ie_escala_p = '83') then -- SAPS III - Simplified acute physiology score and expanded

				nr_seq_projeto_w := 51484;

			elsif	(ie_escala_p = '84') then -- Escala de Depressao Geriatrica de Yesavage - versao reduzida (GDS-15)

				nr_seq_projeto_w := 51416;

			elsif	(ie_escala_p = '89') then -- Escala de risco de queda - Morse

				nr_seq_projeto_w := 51434;


			elsif	(ie_escala_p = '91') then -- Escala de equilibrio e mobilidade de Tinetti

				nr_seq_projeto_w := 51422;

			elsif	(ie_escala_p = '94') then -- Escala de Avaliacao de Risco de Queda da NHS(EARQ)

				nr_seq_projeto_w := 51420;

			elsif	(ie_escala_p = '101') then -- Escala mini mental

				nr_seq_projeto_w := 51432;

			elsif	(ie_escala_p = '139') then -- Mini Nutritional Assessment - Short Form

				nr_seq_projeto_w := 51428;

			elsif	(ie_escala_p = '88') then -- Escala Flebit

				nr_seq_projeto_w := 53462;

			elsif	(ie_escala_p = '240') then -- Escala IMPROVE Bleeding

				nr_seq_projeto_w := 53597;

			elsif	(ie_escala_p = '241') then -- Escala Padua

				nr_seq_projeto_w := 53599;

			elsif	(ie_escala_p = '242') then -- Escala WAKE

				nr_seq_projeto_w := 53604;

			elsif	(ie_escala_p = '243') then -- Escala MRC

				nr_seq_projeto_w := 53605;
      
			elsif	(ie_escala_p = '244') then -- Escala Pittsburg Brain Stem Score'

				nr_seq_projeto_w := 53606;
				
			elsif	(ie_escala_p = '246') then -- Escala Rev Geneva

				nr_seq_projeto_w := 53609;		
				
			elsif	(ie_escala_p = '247') then -- Thompson Score

				nr_seq_projeto_w := 53622;

			elsif	(ie_escala_p = '248') then -- Escala NPass

				nr_seq_projeto_w := 53613;
			
			elsif	(ie_escala_p = '249') then -- Escala Kuss

				nr_seq_projeto_w := 53616;

			elsif	(ie_escala_p = '245') then -- Escala Atice

				nr_seq_projeto_w := 53610;
				
			elsif	(ie_escala_p = '250') then -- Escala Minds

				nr_seq_projeto_w := 53615;
			
			elsif	(ie_escala_p = '142') then -- Escala Score Flex II

				nr_seq_projeto_w := 53471;
				
			elsif	(ie_escala_p = '251') then -- Escala Balthazar

				nr_seq_projeto_w := 53619;

			elsif	(ie_escala_p = '253') then -- Escala SAD PERSONS

				nr_seq_projeto_w := 53625;
				
			elsif	(ie_escala_p = '252') then -- Neonatal Abstinence

				nr_seq_projeto_w := 53626;

			end if;

		elsif	(cd_tipo_registro_p = 'XPM') then -- Parecer medico

			nr_seq_projeto_w := 50721;
			
		elsif	(cd_tipo_registro_p = 'XRP') then -- Resposta de Parecer medico

			nr_seq_projeto_w := 50723;	

		elsif	(cd_tipo_registro_p = 'XREC') then -- Receitas

			nr_seq_projeto_w := 50172;

		elsif	(cd_tipo_registro_p = 'XAT') then -- Atestado
			if (obter_funcao_ativa = 872) then
				nr_seq_projeto_w := 50814;
			else
				nr_seq_projeto_w := 51454;
			end if;
		elsif (cd_tipo_registro_p = 'XAVN') then -- Avaliacao Nutricional

			nr_seq_projeto_w := 53452;
		elsif (cd_tipo_registro_p = 'XAVF') then -- Avaliacao Fisica

			nr_seq_projeto_w := 53666;
		elsif	 (cd_tipo_registro_p = 'XHSAC')then --  Historico de saude - Doencas previas e atuais

			nr_seq_projeto_w := 25177;

		elsif	(cd_tipo_registro_p = 'XHSAM') then --  Historico de saude -  Historia familiar

			nr_seq_projeto_w := 25179;

		elsif	(cd_tipo_registro_p = 'XHSAL') then --  Historico de saude - Paciente alergias/adversas

			nr_seq_projeto_w := 25175;

		elsif	(cd_tipo_registro_p = 'XHSHV') then --  Historico de saude - Habitos e vicios

			nr_seq_projeto_w := 25178;

		elsif	(cd_tipo_registro_p = 'XHSMU') then --  Historico de saude - Medicamentos em uso

			nr_seq_projeto_w := 25180;

		elsif	(cd_tipo_registro_p = 'XHSCI') then --  Historico de saude - Cirugias

			nr_seq_projeto_w := 25176;

		elsif	(cd_tipo_registro_p = 'XSVMR') then --  Monitorizacao respiratoria

			nr_seq_projeto_w := 25185;

		elsif	(cd_tipo_registro_p = 'XSVMH') then --  Monitorizacao hemodinamica

			nr_seq_projeto_w := 25184;

		elsif	(cd_tipo_registro_p = 'XSVMA') then  -- Monitorizacao de analgesia

			nr_seq_projeto_w := 25183;

		elsif	(cd_tipo_registro_p = 'XOA') then  -- Orientacao de alta

			nr_seq_projeto_w := 25186;

		elsif	(cd_tipo_registro_p = 'XONC') then -- Atendimento do ciclo - Tratamento oncologico

			nr_seq_projeto_w := 51293;

		elsif	(cd_tipo_registro_p = 'XSAE') then -- SAE - Sistematizacao da assistencia de enfermagem

			nr_seq_projeto_w := 25290;

		elsif	(cd_tipo_registro_p = 'XLR') then -- Loco regional / Diagnostico do tumor

			nr_seq_projeto_w := 50713;

		elsif	(cd_tipo_registro_p = 'XHSO') then --   Historico de saude - Ocorrencias

			nr_seq_projeto_w := 25181;

		elsif	(cd_tipo_registro_p = 'XHSAS') then --   Historico de saude - Acessorio/Ortese/Protese

			nr_seq_projeto_w := 25174;

		elsif	(cd_tipo_registro_p = 'XCIH') then --   Precaucao

			nr_seq_projeto_w := 51335;

		elsif	(cd_tipo_registro_p = 'XEHR') then --   Templates

			nr_seq_projeto_w := 25187;

		elsif	(cd_tipo_registro_p = 'XHSS') then --  Historico de saude - Aspectos socio-economico-culturais e outros habitos

			nr_seq_projeto_w := 51492;

		elsif	(cd_tipo_registro_p = 'XHSD') then --  Historico de saude - Deficiencia do paciente

			nr_seq_projeto_w := 51496;

		elsif	(cd_tipo_registro_p = 'XRECAMB') then --  Historico de saude - Receita Farmacia

			nr_seq_projeto_w := 51550;

		elsif	(cd_tipo_registro_p = 'XOAE') then --  Orientacao de alta enfermagem

			nr_seq_projeto_w := 51373;

		elsif	(cd_tipo_registro_p = 'XHSDS') then --  Historico de saude - Declaracao Complementar

			nr_seq_projeto_w := 51498;

		elsif	(cd_tipo_registro_p = 'XSAPS') then -- SAPS - Sistematizacao da assistencia dos profissionais de saude

			nr_seq_projeto_w := 50715;

		elsif	(cd_tipo_registro_p = 'XAD') then -- Escala de Aldrete modificada

			nr_seq_projeto_w := 51438;

		elsif	(cd_tipo_registro_p = 'XHSP') then -- Historico de saude - Exames do paciente

			nr_seq_projeto_w := 51508;

		elsif	(cd_tipo_registro_p = 'XHSTR') then -- Historico de saude - Transfusoes do paciente

			nr_seq_projeto_w := 49782;

		elsif	(cd_tipo_registro_p = 'XHSV') then -- Historico de saude - Vacinas

			nr_seq_projeto_w := 51494;
         
      elsif	(cd_tipo_registro_p = 'XHRP') then -- Historico de saude - Restricoes

			nr_seq_projeto_w := 25182;   

		elsif	(cd_tipo_registro_p = 'XHSI') then -- Historico de saude - Internacoes

			nr_seq_projeto_w := 51488;

		elsif	(cd_tipo_registro_p = 'XHST') then -- Historico de saude - Tratamentos anteriores

			nr_seq_projeto_w := 51490;

		elsif	(cd_tipo_registro_p = 'XHSM') then -- Historico de saude - Saude da mulher

			nr_seq_projeto_w := 51501;

          elsif	(cd_tipo_registro_p = 'XJS') then -- Justificativas / Solicitacoes

               nr_seq_projeto_w := 52848;

		elsif	(cd_tipo_registro_p = 'XDOB') then -- Declaracao de obito

               nr_seq_projeto_w := 52889;

		elsif	(cd_tipo_registro_p = 'XSUM') then -- Sumario de alta

               nr_seq_projeto_w := 52908;


		elsif	(cd_tipo_registro_p = 'XAPAE') then -- APAE

               nr_seq_projeto_w := 52510;

       elsif	(cd_tipo_registro_p = 'XAON') then -- Anamnese oncologia

               nr_seq_projeto_w := 53208;

       elsif	(cd_tipo_registro_p = 'XEVT') then -- Evento Paciente

               nr_seq_projeto_w := 53169;

       elsif	(cd_tipo_registro_p = 'XPEE') then -- Solicitacoes de exames externo

               nr_seq_projeto_w := 53210;
		elsif	(cd_tipo_registro_p = 'XEFAN') then -- Equipamentos
               nr_seq_projeto_w := 50852;

      elsif	(cd_tipo_registro_p = 'XESCCC') then -- Escala Chung
            nr_seq_projeto_w := 52169;

      elsif	(cd_tipo_registro_p = 'XESP') then -- Escala Possum
            nr_seq_projeto_w := 52129;

      elsif	(cd_tipo_registro_p = 'XAEL') then -- Escala Lee
            nr_seq_projeto_w := 52512;

      elsif (cd_tipo_registro_p = 'XPP') then --Participantes
            if (obter_funcao_ativa = 10026) then
               nr_seq_projeto_w := 52010;
            else
               nr_seq_projeto_w := 50792;
            end if;
		elsif	(cd_tipo_registro_p = 'XTMOV') then -- Tempos e Movimentos
               nr_seq_projeto_w := 50972;
		elsif (cd_tipo_registro_p = 'XCAAT') then --Terapia hidroeletrolitica
               nr_seq_projeto_w := 51097;
      elsif (cd_tipo_registro_p = 'XCAAA') then --Agentes Anestesicos
            if (obter_funcao_ativa = 10026) then
               nr_seq_projeto_w := 52228;
            else
               nr_seq_projeto_w := 51092;
            end if;
      elsif (cd_tipo_registro_p = 'XCAAH') then --Hemocomponente
            if (obter_funcao_ativa = 10026) then
               nr_seq_projeto_w := 52209;
            else
               nr_seq_projeto_w := 51101;
            end if;
      elsif (cd_tipo_registro_p = 'XCAAM') then --Medicamentos
            if (obter_funcao_ativa = 10026) then
               nr_seq_projeto_w := 52288;
            else
               nr_seq_projeto_w := 51095;
            end if;
		elsif (cd_tipo_registro_p = 'XADTH') then --Adm Terapia hidroeletrolitica
               nr_seq_projeto_w := 51116;
		elsif (cd_tipo_registro_p = 'XADHE') then -- Adm Hemocomponete/Hemoderivados ADHE
               nr_seq_projeto_w := 51118;
		elsif (cd_tipo_registro_p = 'XADMD') then -- Adm  Medicamentos ADMD
               nr_seq_projeto_w := 51114;
		elsif (cd_tipo_registro_p = 'XADAA') then -- Adm Agentes Anestesicos ADAA
            if (obter_funcao_ativa = 10026) then
               nr_seq_projeto_w := 52250;
            else
               nr_seq_projeto_w := 51112;
            end if;
       elsif	(cd_tipo_registro_p = 'XPEE') then -- Solicitacoes de exames externo

               nr_seq_projeto_w := 53210;

       elsif	(cd_tipo_registro_p = 'XEVL') then -- Evolucoes Oftalmologia

               nr_seq_projeto_w := 53244;

       elsif	(cd_tipo_registro_p = 'XAGRL') then -- Angioretinofluoresceinografia Oftalmologia
               nr_seq_projeto_w := 53252;
      elsif	(cd_tipo_registro_p = 'XARTL') then -- Angioretinofluoresceinografia Oftalmologia
               nr_seq_projeto_w := 53252;
       elsif	(cd_tipo_registro_p = 'XBIOL') then -- Biometria Oftalmologia

               nr_seq_projeto_w := 53256;

       elsif	(cd_tipo_registro_p = 'XBICL') then -- Biomicroscopia Oftalmologia

               nr_seq_projeto_w := 53258;

       elsif	(cd_tipo_registro_p = 'XCAPL') then -- Campimetria Oftalmologia

               nr_seq_projeto_w := 53260;

	elsif	(cd_tipo_registro_p = 'XCERL') then --  Ceratometria Oftalmologia

               nr_seq_projeto_w := 53263;

       elsif	(cd_tipo_registro_p = 'XAVNL') then -- Acuidade visual nova Oftalmologia

               nr_seq_projeto_w := 53349;

       elsif	(cd_tipo_registro_p = 'XCTL') then -- Curva tensional Oftalmologia

               nr_seq_projeto_w := 53271;

       elsif	(cd_tipo_registro_p = 'XDNPL') then -- Distancia naso pupilar

               nr_seq_projeto_w := 53273;

       elsif	(cd_tipo_registro_p = 'XEOEL') then -- Exame ocular externo

               nr_seq_projeto_w := 53275;

       elsif	(cd_tipo_registro_p = 'XFNCL') then -- Fundoscopia

               nr_seq_projeto_w := 53277;

      elsif	(cd_tipo_registro_p = 'XGNOL') then -- Gonioscopia

               nr_seq_projeto_w := 53280;

      elsif	(cd_tipo_registro_p = 'XMIEL') then -- Microscopia Especular

               nr_seq_projeto_w := 53282;

      elsif	(cd_tipo_registro_p = 'XMOOL') then -- Motilidade ocular

               nr_seq_projeto_w := 53284;

       elsif (cd_tipo_registro_p = 'XPAQL') then -- Paquimetria
			nr_seq_projeto_w := 53288;
       elsif (cd_tipo_registro_p = 'XPQUL') then -- Paquimetria ultra
			nr_seq_projeto_w := 53288;

       elsif (cd_tipo_registro_p = 'XPAVL') then -- Potencial de Acuidade Visual

	nr_seq_projeto_w := 53291;

        elsif (cd_tipo_registro_p = 'XPUPL') then --Pupilometria

	nr_seq_projeto_w := 53293;

       elsif (cd_tipo_registro_p = 'XTOCL') then --Tomografia de coerencia optica

   	nr_seq_projeto_w := 53295;

       elsif (cd_tipo_registro_p = 'XTOGL') then --Tomografia do olho

               nr_seq_projeto_w := 53297;

       elsif (cd_tipo_registro_p = 'XTAPL') then --Tonometria aplanacao/pneumatica

			nr_seq_projeto_w := 53299;
		 elsif (cd_tipo_registro_p = 'XTPNL') then --Tonometria aplanacao/pneumatica

			nr_seq_projeto_w := 53433;

       elsif (cd_tipo_registro_p = 'XULTL') then --Ultrassonografia

	nr_seq_projeto_w := 53301;

       elsif (cd_tipo_registro_p = 'XIRIL') then --Iridectomia

	nr_seq_projeto_w := 53303;

       elsif (cd_tipo_registro_p = 'XAVAL') then --Avaliacao Lacrimal

	nr_seq_projeto_w := 53305;

       elsif (cd_tipo_registro_p = 'XDATL') then --Daltonismo

	nr_seq_projeto_w := 53307;

       elsif (cd_tipo_registro_p = 'XCASL') then --Capsulotomia

	nr_seq_projeto_w := 53309;

       elsif (cd_tipo_registro_p = 'XREFL') then --Refracao

	nr_seq_projeto_w := 53311;

       elsif (cd_tipo_registro_p = 'XAURL') then --Auto Refracao

	nr_seq_projeto_w := 53313;

       elsif (cd_tipo_registro_p = 'XMARL') then --Mapeamento da retina

	nr_seq_projeto_w := 53315;

       elsif (cd_tipo_registro_p = 'XSOHL') then --Sobrecarga hidrica

	nr_seq_projeto_w := 53317;

       elsif (cd_tipo_registro_p = 'XFOTL') then --Fotocoagulacao

	nr_seq_projeto_w := 53319;

       elsif (cd_tipo_registro_p = 'XACUL') then --Acuidade Visual

	nr_seq_projeto_w := 53269;

       elsif (cd_tipo_registro_p = 'XRECL') then --Receita

	nr_seq_projeto_w := 53321;

       elsif (cd_tipo_registro_p = 'XREOL') then --Receita

	nr_seq_projeto_w := 53323;

	elsif (cd_tipo_registro_p = 'XAV') then /* PEPO - Avaliacoes */
		nr_seq_projeto_w := 50672;

	elsif (cd_tipo_registro_p = 'XCDA') then /* PEPO - Descricao anestesica */
		nr_seq_projeto_w := 44469;

	elsif (cd_tipo_registro_p = 'XCR') then /* PEPO - Curativo */
		nr_seq_projeto_w := 50338;

	elsif (cd_tipo_registro_p = 'XCTA') then /* PEPO - Tecnica anestesica  */
		nr_seq_projeto_w := 44466;

	elsif (cd_tipo_registro_p = 'XCDC') then /* PEPO - Descricao cirurgia*/
		nr_seq_projeto_w := 44284;

	elsif (cd_tipo_registro_p = 'XPO') then /*PEPO - Posicao */
		nr_seq_projeto_w := 50332;

	elsif (cd_tipo_registro_p = 'XLAVAL') then /* PEPO - Liberacao SRA - Intena */
		nr_seq_projeto_w := 50613;

	elsif (cd_tipo_registro_p = 'XPL') then /* PEPO - Pele*/
		nr_seq_projeto_w := 50334;

	elsif (cd_tipo_registro_p = 'XCP') then /* PEPO - Participantes */
		nr_seq_projeto_w := 50792;

	elsif (cd_tipo_registro_p = 'XCAAMAT') then /*Materiais*/
		nr_seq_projeto_w := 51099;

	elsif (cd_tipo_registro_p = 'XGRAV') then /*Gravidez*/
		nr_seq_projeto_w := 50772;
	elsif (cd_tipo_registro_p = 'XONL') then /*Anamnese inicial*/
		nr_seq_projeto_w := 53222;
	elsif (cd_tipo_registro_p = 'XOAL') then /*Anexo oftalmo*/
		nr_seq_projeto_w := 53226;
	elsif (cd_tipo_registro_p = 'XCNL') then /*Consentimentos*/
		nr_seq_projeto_w := 53236;
	elsif (cd_tipo_registro_p = 'XODL') then /*Diagnostico oftalmo*/
		nr_seq_projeto_w := 53238;
	elsif (cd_tipo_registro_p = 'XODF') then /*Diagnostico oftalmo*/
		nr_seq_projeto_w := 53242;
	elsif (cd_tipo_registro_p = 'XOGL') then /*Orientacoes gerais*/
		nr_seq_projeto_w := 51032;
	elsif (cd_tipo_registro_p = 'XOCL') then /*Conduta oftalmo*/
		nr_seq_projeto_w := 53234;
	elsif (cd_tipo_registro_p = 'XOIL') then /*Imagem oftalmo*/
		nr_seq_projeto_w := 53440;
	elsif (cd_tipo_registro_p = 'XCJ') then /*Conjunto*/
		nr_seq_projeto_w := 50952;
	elsif (cd_tipo_registro_p = 'XLCC') then /*Controle de cavidade*/
		nr_seq_projeto_w := 50992;
	elsif (cd_tipo_registro_p = 'XIC') then /*Incisao cirurgica*/
		nr_seq_projeto_w := 50336;
	elsif (cd_tipo_registro_p = 'XLFM') then /*Faturamento medico*/
		if (obter_funcao_ativa = 281) then
			nr_seq_projeto_w := 53450;
		else
			nr_seq_projeto_w := 50692;
		end if;
	elsif (cd_tipo_registro_p = 'XABP') then /*Analisador bioquimico*/
		nr_seq_projeto_w := 25170;
	elsif (cd_tipo_registro_p = 'XLBI') then /*Boletins informativos*/
		nr_seq_projeto_w := 50632;
		
	elsif	(cd_tipo_registro_p = 'XHSFR') then --  Historico de saude - fatores de Risco
		nr_seq_projeto_w := 53495;
	elsif  (cd_tipo_registro_p = 'XAPN')then /*APAE alertas anestesia */
		nr_seq_projeto_w := 53632;	
	elsif (cd_tipo_registro_p = 'XABRO') then -- OFT - Aberrometria ocular
		nr_seq_projeto_w := 53584;   
	elsif (cd_tipo_registro_p = 'XCMA') then -- Complicacoes Anestesicas
		if(obter_funcao_ativa = 872) then /*PEPO*/
			nr_seq_projeto_w := 53628;
		elsif(obter_funcao_ativa = 10026) then /*FANEP*/
			nr_seq_projeto_w := 53630;	    		
        end if;
	elsif  (cd_tipo_registro_p = 'XHSAP')then -- Historico de saude - Amputacoes
		nr_seq_projeto_w := 53675;	
	elsif (cd_tipo_registro_p = 'XHSAT') then -- Historico de saude - Antecedentes Sexuais
		nr_seq_projeto_w := 53673;	
	elsif (cd_tipo_registro_p = 'XPPD') then -- Historico de saude - Antecedentes Sexuais
		nr_seq_projeto_w := 53681;
	elsif (cd_tipo_registro_p = 'XPART') then -- Partograma
		nr_seq_projeto_w := 53688;
	elsif (cd_tipo_registro_p = 'XNASC') then -- Nascimento
		nr_seq_projeto_w := 53693;
    end if;
    
		cd_perfil_w := 	obter_perfil_ativo;
		
		if	(ie_retorna_projeto_p is not null) and 
			(nr_seq_projeto_w is not null) then
			return nr_seq_projeto_w;
		end if;

		open c01;
		loop
		fetch c01 into
			ie_assinatura_w;
		exit when c01%notfound;
			ds_retorno_w		:= nvl(ie_assinatura_w,'N');
		end loop;


	end if;


end if;

return	ds_retorno_w;

end obter_dados_proj_assinatura;
/
