create or replace
function obter_dias_vencimento_atend(nr_atendimento_p	number,
					cd_convenio_p	number)
 		    	return number is

dt_alta_w		date;
qt_dias_venc_w		number(10,0) := 0;
qt_dias_restante_w	number(10,0) := 0;
			
begin

select	max(a.dt_alta),
	max(b.nr_dias_venc_atend)
into	dt_alta_w,
	qt_dias_venc_w
from	atendimento_paciente a,
	convenio_estabelecimento b
where	a.cd_estabelecimento = b.cd_estabelecimento
and	a.nr_atendimento = nr_atendimento_p
and	b.cd_convenio = cd_convenio_p;

if	(dt_alta_w is not null)and
	(nvl(qt_dias_venc_w,0) <> 0)and
	((dt_alta_w + qt_dias_venc_w) > sysdate)then
	begin
	qt_dias_restante_w := ((dt_alta_w + qt_dias_venc_w) - sysdate);
	end;
end if;

return	qt_dias_restante_w;

end obter_dias_vencimento_atend;
/