create or replace function obter_tipo_medico(	nm_usuario_p varchar2,
					nr_seq_carta_p number default null)

			return varchar2 is

ds_retorno_w	                varchar2(255);
ds_medico_senior_w	        varchar2(255);
ds_medico_responsavel_w	      	varchar2(255);
ds_chefe_departamento_w	      	varchar2(255);
ie_sexo_usuario_w             	pessoa_fisica.IE_SEXO%type;
nr_cirurgia_w			carta_medica.nr_cirurgia%type;
ie_funcao_w			cirurgia_participante.ie_funcao%type;
ie_cirurgiao_w			funcao_medico.ie_cirurgiao%type;

begin

ie_sexo_usuario_w := OBTER_SEXO_PF(OBTER_PESSOA_FISICA_USUARIO(nm_usuario_p, 'C'), 'C');

if (ie_sexo_usuario_w = 'F') then
  ds_medico_senior_w := obter_desc_expressao(945810); --Medico senior
  ds_medico_responsavel_w := obter_desc_expressao(945822); --Medico responsavel
  ds_chefe_departamento_w := obter_desc_expressao(945804); --Chefe departamento
else
  ds_medico_senior_w := obter_texto_tasy(1026475, null); --Medico senior
  ds_medico_responsavel_w := obter_texto_tasy(1026476, null); --Medico responsavel
  ds_chefe_departamento_w := obter_texto_tasy(1026473, null); --Chefe departamento
end if;

if (Obter_se_pf_Medico(OBTER_PESSOA_FISICA_USUARIO(nm_usuario_p, 'C'), 'S') = 'N') then
  ds_medico_responsavel_w := '';
end if;

select  	decode(nvl(max(b.cd_pessoa_fisica), 0), 0, decode(nvl(max(cd_departamento), 0), 0, ds_medico_responsavel_w, ds_chefe_departamento_w), ds_medico_senior_w)
into 	ds_retorno_w
from	usuario a, medico b, departamento_medico c
where 	lower(a.nm_usuario) like lower(nm_usuario_p)
and   	b.cd_pessoa_fisica (+) = a.cd_pessoa_fisica
and    	b.ie_medico_senior (+) = 'S'
and   	c.cd_responsavel (+) = a.cd_pessoa_fisica;

if	(nr_seq_carta_p is not null) then
	select 	nr_cirurgia
	into	nr_cirurgia_w
	from	carta_medica
	where	nr_sequencia = nr_seq_carta_p;
	
	if	(nr_cirurgia_w is not null) then
		select	max(a.ie_funcao)
		into	ie_funcao_w
		from	cirurgia_participante a
		where	a.nr_cirurgia = nr_cirurgia_w
		and	a.dt_inativacao is null
		and	a.cd_pessoa_fisica = obter_pessoa_fisica_usuario(nm_usuario_p, 'C');
		
		if	(ie_funcao_w is not null) then
			select	max(b.ie_cirurgiao)
			into	ie_cirurgiao_w
			from 	funcao_medico b
			where 	b.cd_funcao = ie_funcao_w;
			
			if 	(nvl(ie_cirurgiao_w, 'N') = 'S') then
				ds_retorno_w := obter_desc_expressao(486331);
			end if;
		end if;
	end if;
end if;

return ds_retorno_w;

end obter_tipo_medico;
/
