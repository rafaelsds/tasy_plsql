create or replace
function intpd_conv(
	nm_tabela_p		varchar2,
	nm_atributo_p		varchar2,
	cd_valor_p		varchar2,
	nr_seq_regra_p		number default null,
	ie_conversao_p		varchar2 default 'I',
	ie_conv_int_ext_p	varchar2 default 'I')
	return varchar2 is

/*
ie_conversao_p: 	Onde fica o De-Para na integracao: [I] = Interno (no Tasy); [E] = Externo (no sistema para qual o Tasy envia/recebe informacao
ie_conv_int_ext_p:	Que tipo de conversao sera realizado: [I] Obter valor interno com base no valor externo; [E] Obter valor externo com base num valor interno
*/

cd_conversao_w			conversao_meio_externo.cd_interno%type;
cd_cgc_w			conversao_meio_externo.cd_cgc%type;

begin
if	(ish_param_pck.get_se_regra_ish(nr_seq_regra_p) = 'S') then
	select	cd_cgc
	into	cd_cgc_w
	from	estabelecimento
	where	cd_estabelecimento = obter_estabelecimento_ativo;
end if;	


if	(nvl(ie_conversao_p,'X') = 'E') then
	cd_conversao_w	:=	cd_valor_p;
else
	begin
	if	(nvl(ie_conv_int_ext_p,'X') = 'I') then
		begin
		if	(nr_seq_regra_p > 0) then
			begin
			select	cd_interno
			into	cd_conversao_w
			from	conversao_meio_externo
			where	nm_tabela	= nm_tabela_p
			and	nm_atributo	= nm_atributo_p
			and	cd_externo	= cd_valor_p
			and	nr_seq_regra 	= nr_seq_regra_p
			and	((cd_cgc_w is null) or (nvl(cd_cgc, cd_cgc_w) = cd_cgc_w))
			and	rownum = 1;			
			exception
			when others then
				cd_conversao_w	:=	null;
			end;
		else
			begin
			select	cd_interno
			into	cd_conversao_w
			from	conversao_meio_externo
			where	nm_tabela	= nm_tabela_p
			and	nm_atributo	= nm_atributo_p
			and	cd_externo	= cd_valor_p
			and	((cd_cgc_w is null) or (nvl(cd_cgc, cd_cgc_w) = cd_cgc_w))
			and	rownum = 1;			
			exception
			when others then
				cd_conversao_w	:=	null;
			end;
		end if;
		end;
	else
		begin
		if	(nr_seq_regra_p > 0) then
			begin
			select	cd_externo
			into	cd_conversao_w
			from	conversao_meio_externo
			where	nm_tabela	= nm_tabela_p
			and	nm_atributo	= nm_atributo_p
			and	cd_interno	= cd_valor_p
			and	nr_seq_regra 	= nr_seq_regra_p
			and	((cd_cgc_w is null) or (nvl(cd_cgc, cd_cgc_w) = cd_cgc_w))
			and	rownum = 1;			
			exception
			when others then
				cd_conversao_w	:=	null;
			end;
		else
			begin
			select	cd_externo
			into	cd_conversao_w
			from	conversao_meio_externo
			where	nm_tabela	= nm_tabela_p
			and	nm_atributo	= nm_atributo_p
			and	cd_interno	= cd_valor_p
			and	((cd_cgc_w is null) or (nvl(cd_cgc, cd_cgc_w) = cd_cgc_w))
			and	rownum = 1;			
			exception
			when others then
				cd_conversao_w	:=	null;
			end;
		end if;
		end;
	end if;
	end;
end if;

return cd_conversao_w;

end intpd_conv;
/
