create or replace
function ageweb_macros_comprovante return varchar2 is

ds_retorno_w	varchar2(4000);	

begin

ds_retorno_w	:=	obter_desc_expressao(349611)||CHR(10)||
			obter_desc_expressao(349617)||CHR(10)||			
			obter_desc_expressao(349622)||CHR(10)||
			obter_desc_expressao(349623)||CHR(10)||
			obter_desc_expressao(349634)||CHR(10)||
			obter_desc_expressao(633009);
			

return	ds_retorno_w;

end ageweb_macros_comprovante;
/