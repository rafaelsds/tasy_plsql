create or replace
function OBTER_DIA_UTIL_MES
		(cd_estabelecimento_p	number,
		dt_mes_ref_p			date,
		qt_dia_p			number) return date is

dt_mes_w	date;
dt_retorno_w	date	:= null;
qt_dia_w	number(3,0);

begin

dt_mes_w		:= trunc(dt_mes_ref_p, 'month');
qt_dia_w		:= 0;

while 	(dt_mes_w <= fim_mes(dt_mes_ref_p)) loop

	-- verificar se � dia util
	if	(dt_mes_w = obter_proximo_dia_util(cd_estabelecimento_p, dt_mes_w)) then
		qt_dia_w		:= qt_dia_w + 1;
	end if;

	if	(qt_dia_w = qt_dia_p) then
		dt_retorno_w		:= dt_mes_w;
		exit;
	end if;

	dt_mes_w			:= dt_mes_w + 1;

end loop;

return	dt_retorno_w;

end OBTER_DIA_UTIL_MES;
/
