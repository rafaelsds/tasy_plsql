create or replace
function pls_obter_mes_reajuste_benef
		(	nr_seq_segurado_p	number,
			ie_formato_p		varchar2)
 		    	return varchar2 is

ds_retorno_w			varchar2(15)	:= null;
dt_retorno_w			date 		:= null;
ie_tipo_contrato_w		varchar2(1);
ie_reajuste_w			varchar2(1);
nr_seq_contrato_w		number(10);
nr_mes_reajuste_w		pls_contrato.nr_mes_reajuste%type;
ie_retorno_w			varchar2(1);

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter o m�s de reajuste do benefici�rio, contrato ou grupo de contrato,
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

begin

ie_retorno_w := 'S';

if	(nr_seq_segurado_p is not null) then
	select	decode(nr_seq_contrato, null, 'I', 'C') 
	into	ie_tipo_contrato_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;

	if	(ie_tipo_contrato_w = 'C') then
		select	nvl(a.ie_reajuste,'C'),
			a.nr_sequencia,
			a.nr_mes_reajuste
		into	ie_reajuste_w,
			nr_seq_contrato_w,
			nr_mes_reajuste_w
		from	pls_segurado	b,
			pls_contrato	a
		where	a.nr_sequencia = b.nr_seq_contrato
		and	b.nr_sequencia = nr_seq_segurado_p;
		
		if	(ie_reajuste_w = 'C') then
			if	(nr_mes_reajuste_w is not null) then
				if	(ie_formato_p = 'E') then
					select	obter_valor_dominio(8483, nr_mes_reajuste_w)
					into	ds_retorno_w
					from	dual;
				elsif	(ie_formato_p = 'MM') then
					ds_retorno_w	:= nr_mes_reajuste_w;
				end if;
				
				ie_retorno_w	:= 'N';
			else
				begin
				select	max(c.dt_reajuste)
				into	dt_retorno_w
				from	pls_contrato		a,
					pls_contrato_grupo	b,
					pls_grupo_contrato	c
				where	a.nr_sequencia 		= b.nr_seq_contrato
				and	b.nr_seq_grupo		= c.nr_sequencia
				and	b.ie_reajuste_grupo	= 'S'
				and	a.nr_sequencia 		= nr_seq_contrato_w;
				exception
				when others then
					dt_retorno_w := null;
				end;
				
				if	(dt_retorno_w is null) then
					begin
					select	max(a.dt_reajuste)
					into	dt_retorno_w
					from	pls_contrato		a
					where	a.nr_sequencia 		= nr_seq_contrato_w;
					exception
					when others then
						dt_retorno_w := null;
					end;
					
					if	(dt_retorno_w is null) then
						begin
						select	add_months(a.dt_contrato,nvl(qt_intervalo,12))
						into	dt_retorno_w
						from	pls_contrato		a
						where	a.nr_sequencia 		= nr_seq_contrato_w;
						exception
						when others then
							dt_retorno_w := null;
						end;
					end if;
				end if;
			end if;
		elsif	(ie_reajuste_w = 'A') then
			select	nvl(dt_reajuste,dt_contratacao)
			into	dt_retorno_w
			from	pls_segurado
			where	nr_sequencia = nr_seq_segurado_p;
		end if;	
	elsif	(ie_tipo_contrato_w = 'I') then
		select	max(c.dt_reajuste)
		into	dt_retorno_w
		from	pls_regra_grupo_inter	c,
			pls_segurado		b,
			pls_intercambio		a
		where	a.nr_sequencia 			= b.nr_seq_intercambio
		and	a.nr_seq_grupo_intercambio	= c.nr_sequencia
		and	b.nr_sequencia 			= nr_seq_segurado_p;
		
		if	(dt_retorno_w is null) then
			select	max(a.dt_inclusao)
			into	dt_retorno_w
			from	pls_segurado		b,
				pls_intercambio		a
			where	a.nr_sequencia = b.nr_seq_intercambio
			and	b.nr_sequencia = nr_seq_segurado_p;
		end if;
	end if;

	if	((dt_retorno_w is not null) and (ie_retorno_w = 'S')) then
		if	(ie_formato_p = 'E') then
			ds_retorno_w := initcap(to_char(dt_retorno_w,'month'));
		elsif	(ie_formato_p = 'MM') then
			ds_retorno_w := to_char(dt_retorno_w,'MM');
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_mes_reajuste_benef;
/
