create or replace
function man_obter_se_os_sla(nr_sequencia_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(01) := 'N';
begin

if	(nr_sequencia_p > 0) then
	begin
	select	'S'
	into	ds_retorno_w
	from	man_ordem_serv_sla
	where	nr_seq_ordem = nr_sequencia_p
	and	rownum < 2;
	exception
	when others then
		ds_retorno_w := 'N';
	end;

end if;

return	ds_retorno_w;

end man_obter_se_os_sla;
/
