create or replace function obter_se_exibe_tipo_card(	cd_pessoa_fisica_p	varchar2,
						ie_tipo_item_p	varchar2,
                  					nr_seq_modelo_princ_p number default null)
				return  varchar2 is



ie_exibe_w	varchar2(1) := 'S';
ie_sexo_w	varchar2(1);

begin

if (nr_seq_modelo_princ_p is not null) then

  Select nvl(max('N'),'S')
  into  ie_exibe_w
  from  suep a,
        item_suep b
  where b.NR_SEQ_SUEP = a.nr_sequencia
  and   a.nr_SEQUENCIA = nr_seq_modelo_princ_p;

end if;

if (ie_exibe_w = 'S') and (ie_tipo_item_p in ('PA','HP','PN','SM')) then

	Select 	obter_sexo_pf(cd_pessoa_fisica_p,'C')
	into	ie_sexo_w
	from 	dual;


	if (ie_sexo_w <> 'F') then
		ie_exibe_w := 'N';
	end if;

end if;


return	ie_exibe_w;

end obter_se_exibe_tipo_card;
/