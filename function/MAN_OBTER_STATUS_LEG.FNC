create or replace
function man_obter_status_leg(nr_seq_ordem_serv		number)
 		    	return number is

nr_seq_leg_w			number(10);
ie_prioridade_w			varchar2(1);
ie_novo_hist_w			varchar2(1);
dt_fim_previsto_w		date;
dt_reabertura_w			date;
dt_fim_real_w			date;
ie_status_ordem_w		varchar2(1);
dt_inicio_desejado_w	date;
		
begin

select	substr(Man_Obter_Se_Novo_Hist(a.nr_sequencia, wheb_usuario_pck.get_nm_usuario),1,3) ie_novo_hist,
		dt_fim_previsto,
		dt_reabertura,
		dt_fim_real,
		ie_status_ordem,
		dt_inicio_desejado	
into	ie_novo_hist_w,
		dt_fim_previsto_w,
		dt_reabertura_w,
		dt_fim_real_w,
		ie_status_ordem_w,
		dt_inicio_desejado_w
from	man_ordem_servico_v2 a
where	nr_sequencia = nr_seq_ordem_serv;

if (ie_status_ordem_w = '1' and dt_inicio_desejado_w < sysdate and dt_fim_previsto_w is null and dt_reabertura_w is null) then
	nr_seq_leg_w := 8678;	-- Aberta
end if;

if (dt_fim_previsto_w <> sysdate and dt_reabertura_w is not null) then
	nr_seq_leg_w := 8680;	--Reaberta
end if;

if (dt_fim_previsto_w is not null and dt_fim_previsto_w = sysdate) then
	nr_seq_leg_w := 8679;	--fim previsto hoje
end if;

if (dt_fim_previsto_w is not null and dt_fim_previsto_w < sysdate and dt_fim_real_w is null and dt_reabertura_w is null) then
	nr_seq_leg_w := 8681;	 --Atrasada
end if;

if (ie_novo_hist_w = 'S') then
	nr_seq_leg_w := 8677;	-- Novo histórico
end if;

return nr_seq_leg_w;

end man_obter_status_leg;
/