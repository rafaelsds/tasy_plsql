create or replace function obter_setor(cd_setor_atendimento_p	varchar2)
								return varchar2 is

ds_conversao_w	conversao_meio_externo.cd_externo%type;
ds_retorno_w conversao_meio_externo.cd_externo%type;

BEGIN

select	max(cd_externo)
into	ds_conversao_w
from	conversao_meio_externo
where	nm_tabela	= 'SETOR_ATENDIMENTO'
and	nm_atributo	= 'CD_SETOR_ATENDIMENTO'
and	cd_interno	= 'SETOR_EPIMED';

if (upper(ds_conversao_w) = 'EPIMED') then

	SELECT Max(cd_setor_atendimento)
	into ds_retorno_w
	FROM SETOR_ATENDIMENTO
    WHERE IE_EPIMED = 'S'
    AND ie_situacao = 'A'
	AND cd_setor_atendimento = cd_setor_atendimento_p;

elsif (ds_conversao_w is not null) then
	ds_retorno_w := ds_conversao_w;
end if;

return ds_retorno_w;

end obter_setor;
/