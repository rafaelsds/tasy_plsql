CREATE OR REPLACE
FUNCTION Obter_Curva_ABC (
					cd_material_p			Number,
					ie_curva_grupo_p		varchar2,
					dt_mesano_referencia_p	Date)
					RETURN Varchar2 IS

ie_curva_w				varchar2(1);
cd_material_w				Number(06,0);

BEGIN

select	nvl(max(cd_material_estoque), cd_material_p)
into	cd_material_w
from 	material
where 	cd_material	= cd_material_p;

select	nvl(max(decode(ie_curva_grupo_p,'S',ie_curva_abc_grupo,ie_curva_abc)),'X')
into		ie_curva_w
from		Material_abc
where		cd_material	= cd_material_w
and		dt_mesano_Referencia	= 
		(select max(dt_mesano_referencia)
		from	Material_abc
		where	cd_material			= cd_material_w
		and	dt_mesano_Referencia	<= dt_mesano_Referencia_p);

Return ie_curva_w;

END Obter_Curva_ABC;
/ 