create or replace
function obter_consistir_tit_classif(nr_titulo_p	number,
			cd_perfil_p		number,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number)
 		return varchar2 is
qt_registros_w 		number(5,0) 	:= -1;
retorno_w			varchar2(1)	:= 'S';
param_usuario_w		varchar2(1);
begin
if 	(nr_titulo_p is not null) and
	(cd_perfil_p is not null) and
	(nm_usuario_p is not null) and 
	(cd_estabelecimento_p is not null) then
	begin
	param_usuario_w := obter_parametro_usuario(801, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p);
	if	(param_usuario_w = 'S') then
		begin
		select  	count(*)
		into	qt_registros_w
		from    	titulo_pagar_classif a
		where   	a.nr_titulo = nr_titulo_p
		and     	(a.cd_centro_custo is not null 
		or 	a.cd_conta_contabil is not null);		
		end;	
	elsif	(param_usuario_w = 'F') then
		begin
		select  	count(*)
		into	qt_registros_w
		from    	titulo_pagar_classif a
                	where   	a.nr_titulo = nr_titulo_p
                	and     	(a.nr_seq_conta_financ is not null);		
		end;	
	end if;	
	if	(qt_registros_w = 0) then
		begin
		retorno_w	:= 'N';
		end;
	end if;	
	end;
end if;
return	retorno_w;
end obter_consistir_tit_classif;
/