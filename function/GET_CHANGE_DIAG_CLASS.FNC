create or replace 
Function GET_CHANGE_DIAG_CLASS	( 	nr_atendimento_p			number,
									nr_seq_interno_p			number,
									IE_DIAG_TRATAMENTO_P		varchar2,
									IE_DIAG_PRINC_EPISODIO_P	varchar2,
									IE_DIAG_REFERENCIA_P		varchar2,
									IE_DIAG_ADMISSAO_P			varchar2,
									IE_DIAG_ALTA_P				varchar2,
									IE_DIAG_CIRURGIA_P			varchar2,
									IE_DIAG_OBITO_P				varchar2,
									IE_DIAG_PRE_CIR_P			varchar2,
									IE_DIAG_PRINC_DEPART_P		varchar2,
									IE_DIAG_TRAT_CERT_P			varchar2,
									IE_DIAG_CRONICO_P			varchar2,
									IE_DIAG_TRAT_ESPECIAL_P		varchar2) 
				return varchar2 is

cursor C01 is
	select 'DPE' cd, 'IE_DIAG_PRINC_EPISODIO' ds from dual
	union
	select 'UBE' cd, 'IE_DIAG_REFERENCIA' ds from dual
	union
	select 'BEH' cd, 'IE_DIAG_TRATAMENTO' ds from dual
	union
	select 'AUF' cd, 'IE_DIAG_ADMISSAO' ds from dual
	union
	select 'ENT' cd, 'IE_DIAG_ALTA' ds from dual
	union
	select 'OPD' cd, 'IE_DIAG_CIRURGIA' ds from dual
	union
	select 'TOD' cd, 'IE_DIAG_OBITO' ds from dual
	union
	select 'POD' cd, 'IE_DIAG_PRE_CIR' ds from dual
	union
	select 'FAC' cd, 'IE_DIAG_PRINC_DEPART' ds from dual
	union
	select 'BHS' cd, 'IE_DIAG_TRAT_CERT' ds from dual
	union
	select 'FAL' cd, 'IE_DIAG_CRONICO' ds from dual
	union
	select 'ASV' cd, 'IE_DIAG_TRAT_ESPECIAL' ds from dual;
				
ds_retorno_w		Varchar2(4000) := 'S';
ds_dominio_w		Varchar2(255) := 'S';
qt_max_diag_w		number(10);
qt_diag_depart_w	number(10);

IE_DIAG_TRATAMENTO_W	 	number(10);
IE_DIAG_PRINC_EPISODIO_W	number(10);
IE_DIAG_REFERENCIA_W		number(10);
IE_DIAG_ADMISSAO_W			number(10);
IE_DIAG_ALTA_W				number(10);
IE_DIAG_CIRURGIA_W			number(10);
IE_DIAG_OBITO_W				number(10);
IE_DIAG_PRE_CIR_W			number(10);
IE_DIAG_WRINC_DEPART_W		number(10);
IE_DIAG_TRAT_CERT_W			number(10);
IE_DIAG_CRONICO_W			number(10);
IE_DIAG_TRAT_ESPECIAL_W		number(10);

begin

for c01_w in C01 loop
	begin
		select	nvl(max(qt_diagnostico), 0), max(nvl(DS_CURTA, Obter_Valor_Dominio(9288, ie_classificacao_diagnostico)))
		into	qt_max_diag_w,
				ds_dominio_w
		from	classificacao_diagnostico
		where	ie_classificacao_diagnostico = c01_w.cd
		and		ie_situacao = 'A';
		
		if (qt_max_diag_w > 0) then
			select	sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_PRINC_EPISODIO = 'S' and IE_DIAG_PRINC_EPISODIO_P = 'S') 	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 	then 1 else 0 end) IE_DIAG_PRINC_EPISODIO, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_REFERENCIA = 'S' and IE_DIAG_REFERENCIA_P = 'S')     	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S'))     	then 1 else 0 end) IE_DIAG_REFERENCIA, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_TRATAMENTO = 'S' and IE_DIAG_TRATAMENTO_P = 'S') 	  	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 	  	then 1 else 0 end) IE_DIAG_TRATAMENTO, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_ADMISSAO = 'S' and IE_DIAG_ADMISSAO_P = 'S') 		or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 			then 1 else 0 end) IE_DIAG_ADMISSAO, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_ALTA = 'S' and IE_DIAG_ALTA_P = 'S') 			or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 				then 1 else 0 end) IE_DIAG_ALTA, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_CIRURGIA = 'S' and IE_DIAG_CIRURGIA_P = 'S') 		or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 			then 1 else 0 end) IE_DIAG_CIRURGIA, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_OBITO = 'S' and IE_DIAG_OBITO_P = 'S') 		 	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 		 		then 1 else 0 end) IE_DIAG_OBITO, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_PRE_CIR = 'S' and IE_DIAG_PRE_CIR_P = 'S') 		 	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 			then 1 else 0 end) IE_DIAG_PRE_CIR, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_PRINC_DEPART = 'S' and IE_DIAG_PRINC_DEPART_P = 'S') 	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 		then 1 else 0 end) IE_DIAG_PRINC_DEPART, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_TRAT_CERT = 'S' and IE_DIAG_TRAT_CERT_P = 'S') 	 	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 	 		then 1 else 0 end) IE_DIAG_TRAT_CERT, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_CRONICO = 'S' and IE_DIAG_CRONICO_P = 'S') 		 	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S')) 		 	then 1 else 0 end) IE_DIAG_CRONICO, 
					sum(case when ((x.nr_sequencia <> nvl(nr_seq_interno_p, 0) and IE_DIAG_TRAT_ESPECIAL = 'S' and IE_DIAG_TRAT_ESPECIAL_P = 'S')  	or (x.nr_sequencia = nr_seq_interno_p and 'S' <> 'S'))  	then 1 else 0 end) IE_DIAG_TRAT_ESPECIAL
			into	IE_DIAG_PRINC_EPISODIO_W,
			        IE_DIAG_REFERENCIA_W,	
					IE_DIAG_TRATAMENTO_W,	
			        IE_DIAG_ADMISSAO_W,		
			        IE_DIAG_ALTA_W,			
			        IE_DIAG_CIRURGIA_W,		
			        IE_DIAG_OBITO_W,			
			        IE_DIAG_PRE_CIR_W,		
			        IE_DIAG_WRINC_DEPART_W,	
			        IE_DIAG_TRAT_CERT_W,		
			        IE_DIAG_CRONICO_W,		
			        IE_DIAG_TRAT_ESPECIAL_W
			from	diagnostico_doenca_depart x
			where	x.nr_atendimento in (	select 	nr_atendimento
											from	atendimento_paciente
											where	nr_seq_episodio = obter_episodio_atendimento(nr_atendimento_p) )
			and		nvl(x.ie_situacao,'A') = 'A';
			
			if (c01_w.cd = 'DPE' and IE_DIAG_PRINC_EPISODIO_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'UBE' and IE_DIAG_REFERENCIA_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'BEH' and IE_DIAG_TRATAMENTO_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'AUF' and IE_DIAG_ADMISSAO_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'ENT' and IE_DIAG_ALTA_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'OPD' and IE_DIAG_CIRURGIA_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'TOD' and IE_DIAG_OBITO_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'POD' and IE_DIAG_PRE_CIR_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'FAC' and IE_DIAG_WRINC_DEPART_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			elsif (c01_w.cd = 'BHS' and IE_DIAG_TRAT_CERT_W >= qt_max_diag_w) then
				if (ds_retorno_w = 'S') then
					ds_retorno_w := null;
				end if;
				ds_retorno_w := ds_retorno_w || chr(10) || chr(13) || ' - ' || ds_dominio_w;
			end if;
		end if;
	end;
end loop;

return ds_retorno_w;

end get_change_diag_class;
/