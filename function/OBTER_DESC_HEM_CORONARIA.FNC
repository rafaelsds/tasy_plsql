CREATE OR REPLACE
FUNCTION Obter_desc_hem_coronaria	(nr_sequencia_p		number)
					RETURN VARCHAR2 IS

ds_retorno_w	VARCHAR2(150);

BEGIN

if	(nr_sequencia_p is not null) then
	select	ds_avaliacao
	into	ds_retorno_w
	from	hem_coronaria
	where	nr_sequencia = nr_sequencia_p;
end if;

RETURN	ds_retorno_w;

END Obter_desc_hem_coronaria;
/
