CREATE OR REPLACE FUNCTION GET_NR_GRUPO(ds_grupo_p VARCHAR2)
  RETURN number
 IS
  nr_seq_w number;
BEGIN
select nr_sequencia INTO nr_seq_w from dar_app_grupos where ds_grupo = ds_grupo_p;
 RETURN nr_seq_w;
END GET_NR_GRUPO;
/
