create or replace
function obter_regra_fatur_direto_mat(cd_material_p	number,
					cd_convenio_p	number,
					dt_referencia_p	date)
 		    	return varchar2 is

cd_grupo_material_w		number(10,0);
cd_subgrupo_material_w		number(10,0);
cd_classe_material_w		number(10,0);
ie_faturamento_direto_w		varchar2(5) := 'S';
ie_faturamento_direto_ww	varchar2(5) := 'S';
nr_seq_familia_w		number(10,0);

Cursor C01 is
	select	ie_faturamento_direto
	from	regra_fatur_direto_mat
	where	nvl(cd_grupo_material,cd_grupo_material_w) 		= cd_grupo_material_w
	and	nvl(cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
	and	nvl(cd_classe_material,cd_classe_material_w)		= cd_classe_material_w
	and	nvl(cd_material,cd_material_p)				= cd_material_p
	and	nvl(nr_seq_familia,nr_seq_familia_w)			= nr_seq_familia_w
	and	nvl(cd_convenio,cd_convenio_p)				= cd_convenio_p
	and	nvl(dt_referencia_p,sysdate) between nvl(dt_inicio_vigencia,sysdate - 9999) and nvl(dt_fim_vigencia,sysdate+999)
	order by nvl(cd_material,0),
		nvl(nr_seq_familia,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_convenio,0),
		nvl(dt_inicio_vigencia,sysdate-9999);
begin

select	nvl(max(cd_grupo_material),0),
	nvl(max(cd_subgrupo_material),0),
	nvl(max(cd_classe_material),0),
	nvl(max(nr_seq_familia),0)
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	nr_seq_familia_w
from	estrutura_material_v
where	cd_material = cd_material_p;

open C01;
loop
fetch C01 into	
	ie_faturamento_direto_w;
exit when C01%notfound;
	begin
	ie_faturamento_direto_ww := ie_faturamento_direto_w;
	end;
end loop;
close C01;

return	ie_faturamento_direto_ww;

end obter_regra_fatur_direto_mat;
/
