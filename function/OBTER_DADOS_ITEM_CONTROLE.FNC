create or replace
function obter_dados_item_controle(	nr_sequencia_p	number,
					ie_opcao_p	varchar2)
					return varchar2 is

ds_item_controle_w	varchar2(255);
cd_unidade_med_cons_w	varchar2(5);
ds_retorno_w		varchar2(255);
ie_freq_utilizacao_w	number(10,0);
					
begin

if	(nr_sequencia_p > 0) then
	select	ds_item_controle,
		cd_unidade_med_cons,
		ie_freq_utilizacao
	into	ds_item_controle_w,
		cd_unidade_med_cons_w,
		ie_freq_utilizacao_w
	from	pepo_item_controle
	where	nr_sequencia	=	nr_sequencia_p;
end if;

if	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_item_controle_w;
elsif	(ie_opcao_p = 'U') then
	ds_retorno_w	:= cd_unidade_med_cons_w;
elsif	(ie_opcao_p = 'F') then
	ds_retorno_w	:= ie_freq_utilizacao_w;
end if;

return ds_retorno_w;

end obter_dados_item_controle;
/