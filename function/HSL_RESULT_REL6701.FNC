create or replace
function HSL_Result_Rel6701(	nr_atendimento_p number,
				ie_opcao_p	varchar2)
 		    	return varchar2 is
			
nr_atendimento_w	number(10);
ds_retorno_w		varchar2(4000);
nm_pf_w			varchar2(255);
ds_idade_w		varchar2(200);
ds_sexo_w		varchar2(5);
dt_nascto_w		varchar2(255);
ds_avaliacao_w		varchar2(4000);
ds_resultado_w		varchar2(255);
checkup_w		varchar2(255):= '';
checkup_ww		varchar2(4000):= '';
espaco_w		varchar2(255) := '';
	
Cursor C02 is
	select 	a.ds_diagnostico
	from   	checkup_tipo_diag a,
		checkup_diagnostico b
	where	b.nr_seq_diag = a.nr_sequencia
	and	nr_atendimento = nr_atendimento_p;
	
begin

	if (ie_opcao_p >= 1 and ie_opcao_p <= 5) then
		/*Campos ie_opcao entre 1 e 5*/
		SELECT	nr_atendimento,
			SUBSTR(obter_nome_pf(cd_pessoa_fisica),1,150) nm_pf,
			SUBSTR(obter_idade(obter_data_nascto_pf(cd_pessoa_fisica),SYSDATE,'S'),1,10) ds_idade,
			SUBSTR(obter_sexo_pf(cd_pessoa_fisica,'C'),1,10) ds_sexo,
			obter_data_nascto_pf(cd_pessoa_fisica) dt_nascto
		into	nr_atendimento_w,
			nm_pf_w,	
			ds_idade_w,	
			ds_sexo_w,	
			dt_nascto_w	
		FROM	atendimento_paciente
		where	nr_atendimento = (nr_atendimento_p)
		order by 2;
	end if;

	if (ie_opcao_p = 1638) then
		/*Avalia��o, Seq = 1638*/
		SELECT	substr(nvl(max(ds_resultado),' '),1,4000) ds_resultado
		into	ds_avaliacao_w
		FROM	med_avaliacao_paciente a,
			med_tipo_avaliacao b,
			med_item_avaliar c,
			med_avaliacao_result d
		WHERE	a.nr_seq_tipo_avaliacao = b.nr_sequencia
		AND	c.nr_seq_tipo	= b.nr_sequencia
		AND	d.nr_seq_avaliacao = a.nr_sequencia
		and	nr_atendimento = (nr_atendimento_p)
		AND	c.nr_sequencia = ie_opcao_p;
	end if;

	if ((ie_opcao_p > 2000) or (ie_opcao_p = 0))then

		/*ie_opcao ser� utilizado como par�metro para o select para trazer somente o exame correto 
		Exame  �c urico, Cod. Exame = 2384
		Exame  Colesterol, Cod. Exame = ?
		Exame  Triglic�rides, Cod. Exame = 2808
		Exame  HDL, Cod. Exame = ?
		Exame  LDL, Cod. Exame = 2640
		Exame  Fosfatase Alcalina, N�o h�
		Exame  Gama GT, Cod. Exame = 2523
		Exame  Glicose, Cod. Exame = 2542
		Exame  Hematocrito, Cod. Exame = 2610
		Exame  Hemoglobina, Cod. Exame = 2616
		Exame  Leuc�citos, Cod. Exame = 2591
		Exame  PSA Livre, Cod. Exame = 2679
		Exame  PSA (TOTAL), Cod. Exame = 2678
		Exame  TGO, Cod. Exame = 2543
		Exame TGP, Cod. Exame = 2544
		Exame TSH, Cod. Exame = 2813
		Exame ProteinaC , Cod. Exame = 2661
		Exame Plaquetas , Cod. Exame = 2608
		Exame % PSA Livre , Cod. Exame = ?*/	
		SELECT	SUBSTR(NVL(NVL(DECODE(a.ds_resultado,'0','',DECODE(c.ie_formato_resultado,'V','',a.ds_resultado)), NVL(TO_CHAR(a.qt_resultado),TO_CHAR(DECODE(a.pr_resultado,0,'',a.pr_resultado)))),a.ds_resultado),1,100) ds_resultado
		INTO	ds_resultado_w
		FROM	exame_lab_result_item a,
				exame_lab_resultado b,
				exame_laboratorio c,
				prescr_procedimento d,
				prescr_medica e
		WHERE	a.nr_seq_resultado = b.nr_seq_resultado
		AND	a.nr_seq_exame	= c.nr_seq_exame
		AND	d.nr_sequencia	= a.nr_seq_prescr
		AND	d.nr_prescricao	= b.nr_prescricao
		AND	d.nr_prescricao = e.nr_prescricao
		AND	b.nr_prescricao = (	SELECT	MAX(b.nr_prescricao)
								FROM	exame_lab_result_item a,
									exame_lab_resultado b,
									exame_laboratorio c,
									prescr_procedimento d,
									prescr_medica e
								WHERE	a.nr_seq_resultado = b.nr_seq_resultado
								AND	a.nr_seq_exame	= c.nr_seq_exame
								AND	d.nr_sequencia	= a.nr_seq_prescr
								AND	d.nr_prescricao	= b.nr_prescricao
								AND	d.nr_prescricao = e.nr_prescricao
								AND	e.nr_atendimento = nr_atendimento_p)
		AND	c.nr_seq_exame = ie_opcao_p;
	end if;
	
/*Respostas relevantes da anamnese*/
	if (ie_opcao_p = 999) then
		open C02;
		loop
		fetch C02 into	
			checkup_w;
		exit when C02%notfound;
			begin
				if (checkup_w is not null) then
					checkup_ww := substr(checkup_ww||espaco_w||checkup_w,1,4000);
					espaco_w := chr(10)||chr(13);
				end if;
			end;
		end loop;
		close C02;	
	end if;

	if (ie_opcao_p = 1) then
		ds_retorno_w := nr_atendimento_p; 
	end if;
	if (ie_opcao_p = 2) then
		ds_retorno_w := nm_pf_w; 
	end if;
	if (ie_opcao_p = 3) then
		ds_retorno_w := ds_idade_w;
	end if;
	if (ie_opcao_p = 4) then
		ds_retorno_w := ds_sexo_w;
	end if;
	if (ie_opcao_p = 5) then
		ds_retorno_w := dt_nascto_w;
	end if;
	if (ie_opcao_p = 1638) then
		ds_retorno_w := ds_avaliacao_w;
	end if;
	if ((ie_opcao_p > 2000) or (ie_opcao_p = 0)) then
		ds_retorno_w := ds_resultado_w;
	end if;
	if (ie_opcao_p = 999) then
		ds_retorno_w := checkup_ww ;
	end if;

return	ds_retorno_w;

end HSL_Result_Rel6701;
/