create or replace
function lab_obter_se_conta_paga (nr_atendimento_p		varchar2)
				return varchar2 is
				
ds_retorno_w		varchar2(1);

begin
ds_retorno_w	:=	'S';

if	(nr_atendimento_p is not null) then

	select 	decode(count(*),'0','N','S')
	into	ds_retorno_w
	from 	conta_paciente
	where	nr_atendimento = nr_atendimento_p
	and 	((cd_convenio_parametro = 3) or (ie_status_acerto = 2 and nvl(nr_seq_protocolo,0) <> 0));
	
end if;
	
return	ds_retorno_w;

end lab_obter_se_conta_paga;
/