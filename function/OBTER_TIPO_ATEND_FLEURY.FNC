create or replace
function Obter_Tipo_Atend_Fleury(	ie_tipo_atendimento_p	Number,
									cd_classif_setor_p		Varchar2,
									cd_Setor_atendimento_p	Number,
									cd_perfil_p				number default null)
 		    	return Number is

ds_retorno_w	Number(3);
			
begin

select	max(cd_tipo_atend_externo)
into	ds_retorno_w
from	regra_tipo_atend_externo a
where	nvl(a.cd_setor_atendimento, cd_setor_atendimento_p)	= cd_setor_atendimento_p
and		nvl(a.ie_tipo_atendimento, ie_tipo_atendimento_p)	= ie_tipo_atendimento_p
and		nvl(a.cd_classif_setor, cd_classif_setor_p)			= cd_classif_setor_p
and		nvl(a.cd_perfil, nvl(cd_perfil_p,0))	= nvl(cd_perfil_p,0);

return	ds_retorno_w;

end Obter_Tipo_Atend_Fleury;
/
