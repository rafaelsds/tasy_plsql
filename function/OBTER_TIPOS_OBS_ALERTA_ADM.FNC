create or replace
function Obter_tipos_obs_alerta_adm 
			return varchar2 is

ds_tipos_obs_alerta_adm_w	varchar2(255);
nr_seq_tipo_obs_adm_w		adep_tipo_observacao.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	adep_tipo_observacao
	where	ie_alerta = 'S';

begin

open C01;
loop
fetch C01 into	
	nr_seq_tipo_obs_adm_w;
exit when C01%notfound;
	ds_tipos_obs_alerta_adm_w	:=	ds_tipos_obs_alerta_adm_w || nr_seq_tipo_obs_adm_w || ',';
end loop;
close C01;

return ds_tipos_obs_alerta_adm_w;

end Obter_tipos_obs_alerta_adm;
/