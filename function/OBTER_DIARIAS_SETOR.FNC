create or replace function obter_diarias_setor
			(	cd_setor_atendimento_p  	number,
				dt_inicial_p		date,
				dt_final_p			date)
return number is

qt_total_dias_w	number(15);

begin
	
	SELECT	SUM(qt_total_c)
	INTO	qt_total_dias_w
	FROM
	(
	SELECT	DISTINCT
		obter_dias_entre_datas(dt_entrada_unidade,dt_saida_unidade) qt_total_c
	FROM	atend_paciente_unidade
	WHERE	((dt_entrada_unidade BETWEEN dt_inicial_p AND dt_final_p + 86399/86400) 
		OR (dt_saida_unidade BETWEEN dt_inicial_p AND dt_final_p + 86399/86400)
		OR (dt_entrada_unidade < dt_inicial_p AND dt_saida_unidade IS NULL))
	AND	cd_setor_atendimento = cd_setor_atendimento_p
	GROUP BY 	nr_atendimento, dt_entrada_unidade, dt_saida_unidade
	);

return nvl(qt_total_dias_w,0);

end obter_diarias_setor;
/
