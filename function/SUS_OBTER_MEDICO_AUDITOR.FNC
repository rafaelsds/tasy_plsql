CREATE OR REPLACE
FUNCTION Sus_Obter_Medico_Auditor
		(	nr_atendimento_p	number,
			ie_opcao_p		number) 
			return varchar2 is

/* IE_OPCAP_P
	1 - C�digo
	2 - CPF
*/

cd_estabelecimento_w	number(4);
cd_medico_auditor_w	varchar2(10);
ie_auditor_laudo_w	varchar2(1);
nr_cpf_w		varchar2(11);
ds_retorno_w		varchar2(11);

BEGIN

select	cd_estabelecimento
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

select	cd_medico_auditor,
	ie_auditor_laudo
into	cd_medico_auditor_w,
	ie_auditor_laudo_w
from	sus_parametros
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_auditor_laudo_w	= 'S') then
	begin
	select	cd_medico_responsavel
	into	cd_medico_auditor_w
	from	sus_laudo_paciente
	where	nr_atendimento	= nr_atendimento_p
	and	ie_tipo_laudo_sus = 0;
	exception
		when others then
		cd_medico_auditor_w	:= cd_medico_auditor_w;
	end;
end if;

ds_retorno_w	:= cd_medico_auditor_w; 

if	(ie_opcao_p	= 2) then
	select	nr_cpf
	into	nr_cpf_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_medico_auditor_w;
	ds_retorno_w	:= nr_cpf_w;
end if;

return	ds_retorno_w;

END Sus_Obter_Medico_Auditor;
/