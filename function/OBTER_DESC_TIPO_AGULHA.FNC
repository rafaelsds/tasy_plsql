create or replace
function obter_desc_tipo_agulha( nr_seq_tipo_agulha_p	number)
				return varchar is
			
ds_retorno_w	varchar2(255);

begin

select	max(ds_tipo_agulha)
into	ds_retorno_w
from	hd_tipo_agulha
where	nr_sequencia = nr_seq_tipo_agulha_p;


return	ds_retorno_w;

end obter_desc_tipo_agulha;
/
