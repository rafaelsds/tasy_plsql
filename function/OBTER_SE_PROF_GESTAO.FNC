create or replace
function Obter_Se_Prof_Gestao(  nr_prescricao_p		number,
				nr_seq_prescr_p		number,
				nr_seq_proc_interv_p	number,
				nr_prescr_sae_p		number,
				nm_usuario_p		varchar2)	
				return varchar2 is
			
ds_retorno_w		varchar2(1) := 'N'; 
nr_seq_equipe_w		number(10);	
	
Cursor C01 is
select	nr_seq_equipe
from	sp_prescr_proc
where	nr_prescricao = nr_prescricao_p
and	nr_seq_prescr = nr_seq_prescr_p
order by nr_seq_equipe;

Cursor C02 is
select	nr_seq_equipe
from	sp_prescr_proc
where	nr_seq_pe_prescr = nr_prescr_sae_p
and	nr_seq_proc_interv = nr_seq_proc_interv_p
order by nr_seq_equipe;
			
begin

if	(nr_seq_prescr_p is not null) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_equipe_w;
	exit when C01%notfound;
	begin
		select nvl(max('S'), 'N')
		into	ds_retorno_w
		from	sp_prof_equipe
		where	nr_seq_equipe = nr_seq_equipe_w
		and	obter_usuario_pf(cd_pessoa_fisica) = nm_usuario_p;

		if (ds_retorno_w = 'S') then
			exit;
		end if;

	end;
	end loop;
	close C01;
	 
elsif	(nr_seq_proc_interv_p is not null) then

	open C02;
	loop
	fetch C02 into	
		nr_seq_equipe_w;
	exit when C02%notfound;
	begin
		select nvl(max('S'), 'N')
		into	ds_retorno_w
		from	sp_prof_equipe
		where	nr_seq_equipe = nr_seq_equipe_w
		and	obter_usuario_pf(cd_pessoa_fisica) = nm_usuario_p;

		if (ds_retorno_w = 'S') then
			exit;
		end if;

	end;
	end loop;
	close C02;

end if;

return	ds_retorno_w;

end Obter_Se_Prof_Gestao;
/
