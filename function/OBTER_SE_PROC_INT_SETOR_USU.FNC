create or replace
function obter_se_proc_int_setor_usu(	nr_seq_proc_interno_p		number,
					nm_usuario_p			varchar2)
				return varchar2 is

ie_retorno_w		varchar2(1) := 'N';
cd_procedimento_w	number(14);
ie_origem_proced_w	number(10);

begin

if	(nr_seq_proc_interno_p is not null) then

	select	cd_procedimento,
		ie_origem_proced
	into	cd_procedimento_w,
		ie_origem_proced_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_p;

	
	select 	decode(count(*),0,'N','S')
	into	ie_retorno_w
	from	procedimento
	where	cd_procedimento = cd_procedimento_w
	and	ie_origem_proced = ie_origem_proced_w
	and	cd_setor_exclusivo in (	select 	x.cd_setor_atendimento      
					from   	usuario_setor_v x           
					where  	x.nm_usuario	= nm_usuario_p);

	if	(ie_retorno_w = 'N') then
	
		select 	decode(count(*),0,'N','S')
		into	ie_retorno_w
		from	proc_interno_setor p,
			usuario_setor_v u
		where	p.nr_seq_proc_interno 	= nr_seq_proc_interno_p
		and	u.nm_usuario		= nm_usuario_p
		and	u.cd_setor_atendimento	= p.cd_setor_atendimento
		and	nvl(cd_perfil,obter_perfil_ativo) = obter_perfil_ativo;

	
	end if;
	
	
end if;

return	ie_retorno_w;

end obter_se_proc_int_setor_usu;
/