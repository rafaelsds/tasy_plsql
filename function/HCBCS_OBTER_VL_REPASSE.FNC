create or replace
function HCBCS_OBTER_VL_REPASSE(nr_seq_proc_p number)
				return number is

vl_retorno_w		number(18,4);
cd_convenio_w		number(15);
qt_filme_w		number(15,4);
vl_filme_w		number(18,4);
vl_contraste_w		number(18,4);
vl_procedimento_w		number(18,4);
nr_atendimento_w		number(15);
cd_procedimento_w	number(15);

begin

select	a.vl_procedimento,
	obter_convenio_atendimento(obter_atendimento_conta(a.nr_interno_conta)),
	a.qt_filme,
	a.nr_atendimento,
	a.cd_procedimento
into	vl_procedimento_w,
	cd_convenio_w,
	qt_filme_w,
	nr_atendimento_w,
	cd_procedimento_w
from	procedimento_paciente a
where	nr_sequencia	= nr_seq_proc_p;

select	max(qt_filme)
into	qt_filme_w
from	preco_amb
where	cd_procedimento	= cd_procedimento_w
and	cd_edicao_amb	= 92;

select	max(vl_filme)
into	vl_filme_w
from	convenio_amb
where	cd_categoria	= obter_dados_categ_conv(nr_atendimento_w, 'CA')
and	cd_edicao_amb	= 92
and	dt_inicio_vigencia	= (	select	max(z.dt_inicio_vigencia)
				from	convenio_amb z
				where	cd_categoria	= obter_dados_categ_conv(nr_atendimento_w, 'CA')
				and	cd_edicao_amb	= 92);

select	sum(nvl(vl_item,0))
into	vl_contraste_w
from	conta_paciente_v2
where	nr_atendimento	= nr_atendimento_w
and	nr_seq_proc_princ	= nr_seq_proc_p
and	nr_interno_conta	is null;

vl_retorno_w		:= nvl(vl_procedimento_w,0) - nvl(vl_contraste_w,0) - (nvl(qt_filme_w,0) * nvl(vl_filme_w,0));

vl_retorno_w		:= (vl_retorno_w * 25) / 100;

return	vl_retorno_w;

end	HCBCS_OBTER_VL_REPASSE;
/