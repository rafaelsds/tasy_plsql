create or replace
function san_obter_de_para_integracao(	ie_tabela_p		varchar2,
					nr_seq_item_integ_p	number)
 		    	return varchar2 is

cd_para_retorno_w	varchar2(255);
begin

if	(ie_tabela_p = 'ME') then

	select	max(a.ie_valor_integracao)
	into	cd_para_retorno_w
	from	san_tabela_item_integ a,
		san_tabela_integracao b,
		san_exame_metodo_util c
	where	a.nr_seq_tabela	= b.nr_sequencia
	and	a.nr_seq_item	= c.nr_seq_metodo
	and	c.nr_sequencia	= nr_seq_item_integ_p
	and	b.ie_tabela	= ie_tabela_p
	and	a.ie_situacao	= 'A';
else
	select	max(a.ie_valor_integracao)
	into	cd_para_retorno_w
	from	san_tabela_item_integ a,
		san_tabela_integracao b
	where	a.nr_seq_tabela	= b.nr_sequencia
	and	a.nr_seq_item	= nr_seq_item_integ_p
	and	b.ie_tabela	= ie_tabela_p
	and	a.ie_situacao	= 'A';
end if;

return	cd_para_retorno_w;

end san_obter_de_para_integracao;
/