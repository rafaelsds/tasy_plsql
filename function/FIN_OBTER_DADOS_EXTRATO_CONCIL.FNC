create or replace
function fin_obter_dados_extrato_concil(nr_seq_conciliacao_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w				varchar2(4000);
nr_seq_extrato_w			number(10);
dt_inicio_w				date;
dt_final_w				date;

begin

if	(nvl(nr_seq_conciliacao_p,0) <> 0) then
	begin

	select	max(a.nr_sequencia),
		max(a.dt_inicio),
		max(a.dt_final)
	into	nr_seq_extrato_w,
		dt_inicio_w,
		dt_final_w
	from	banco_extrato a,
		banco_extrato_lanc b,
		concil_banc_movto c
	where	b.nr_seq_extrato	= a.nr_sequencia
	and	c.nr_sequencia		= b.nr_seq_concil
	and	c.nr_seq_conciliacao	= nr_seq_conciliacao_p;

	if	(nr_seq_extrato_w	is null) then

		select	max(b.nr_sequencia),
			max(b.dt_inicio),
			max(b.dt_final)
		into	nr_seq_extrato_w,
			dt_inicio_w,
			dt_final_w
		from	banco_extrato b,
			banco_extrato_lanc a,
			concil_banc_pend_bco c
		where	a.nr_seq_extrato	= b.nr_sequencia
		and	a.nr_seq_concil		is null
		and	a.ie_conciliacao	= 'N'
		and	c.nr_seq_lanc_extrato	= a.nr_sequencia
		and	c.nr_seq_conciliacao	= nr_seq_conciliacao_p;

	end if;

	if	(ie_opcao_p	= 'NRE') then
		ds_retorno_w	:= nr_seq_extrato_w;
	elsif	(ie_opcao_p	= 'DIE') and
		(dt_inicio_w	is not null) then
		ds_retorno_w	:= to_char(dt_inicio_w,'DD/MM/YYYY');
	elsif	(ie_opcao_p	= 'DFE') and
		(dt_final_w	is not null) then
		ds_retorno_w	:= to_char(dt_final_w,'DD/MM/YYYY');
	end if;

	end;
end if;

return	ds_retorno_w;

end fin_obter_dados_extrato_concil;
/