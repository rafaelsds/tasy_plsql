create or replace 
function cpoe_obter_volume_total(	
						cd_material_p				number,
						qt_dose_p					number,
						cd_unidade_medida_p			varchar2,
						cd_mat_dil_p				number,
						qt_dose_dil_p				number,
						cd_unid_med_dose_dil_p		varchar2,
						qt_volume_red_p				number,
						cd_mat_red_p				number,
						qt_dose_red_p				number,
						cd_unid_med_dose_red_p		varchar2,
						cd_mat_comp1_p				number,
						qt_dose_comp1_p				number,
						cd_unid_med_dose_comp1_p	varchar2,
						cd_mat_comp2_p				number,
						qt_dose_comp2_p				number,
						cd_unid_med_dose_comp2_p	varchar2,
						cd_mat_comp3_p				number,
						qt_dose_comp3_p				number,
						cd_unid_med_dose_comp3_p	varchar2,
						cd_mat_comp4_p				number default null,
						qt_dose_comp4_p				number default null,
						cd_unid_med_dose_comp4_p	varchar2  default null,
						cd_mat_comp5_p				number default null,
						qt_dose_comp5_p				number default null,
						cd_unid_med_dose_comp5_p	varchar2  default null,
						cd_mat_comp6_p				number default null,
						qt_dose_comp6_p				number default null,
						cd_unid_med_dose_comp6_p	varchar2  default null,
						cd_mat_comp7_p				number default null,
						qt_dose_comp7_p				number default null,
						cd_unid_med_dose_comp7_p	varchar2  default null,
						nr_seq_mat_diluicao_p		material_diluicao.nr_seq_interno%type default null,
						cd_pessoa_fisica_p  		number default null,
						nr_atendimento_p  			number default null,
						ie_via_aplicacao_p 			varchar2 default null,
						nr_sequencia_p 				number default null) return number is


	qt_dose_ml_w			cpoe_material.qt_dose%type;
	qt_volume_total_w		number(18,6) := 0;
	qt_volume_total_medic_w	number(18,6);
	nr_casas_diluicao_w		number(10);
	nr_casas_diluicao_cad_w parametro_medico.nr_casas_diluicao%type;
	cd_estabelecimento_w	number(4);
	cd_unid_med_usua_ml_w   varchar2(30);
	cd_unid_med_usua_w		varchar2(30);
	unid_med_usua_ml_w		varchar2(30);
	ie_diluir_inteiro_w		material_diluicao.ie_diluir_inteiro%type;
	qt_volume_cad_w			material_diluicao.qt_volume%type;
	qt_volume_medic_w		material_diluicao.qt_volume_medic%type;
	ie_proporcao_w			material_diluicao.ie_proporcao%type;
	qt_conversao_mat_w	 	material_conversao_unidade.qt_conversao%type;
	qt_dose_dividida_w		number(18,6);
	qt_dose_mat_w			number(18,6);
	qt_conv_ml_w			cpoe_material.qt_dose%type;
	qt_conversao_ww			material_conversao_unidade.qt_conversao%type;
	exec_w                  varchar2(4000);			
	cd_unid_med_cons_w		varchar2(30);
	cd_unid_med_cons_ml_w	varchar2(30);
	qt_conversao_und_ori_w	number(18,6);
	qt_conversao_und_dest_w	number(18,6);
	qt_conversao_w		    number(18,6);
	qt_conversao_ml_w       number(18,6);
	qt_vol_item_comp_w 		number(18,6);

	procedure obter_dados_conversao_ml(
			cd_material_p         in number,
			cd_unidade_medida_p   in varchar2,
			cd_unid_med_usua_p    out varchar2, 
			cd_unid_med_cons_p    out varchar2,
			qt_conversao_p        out number,
			qt_conversao_ml_p     out number) is		
	begin
		cd_unid_med_usua_p := obter_unid_med_usua('ML');

		cd_unid_med_cons_p := upper(substr(obter_dados_material_estab(cd_material_p, wheb_usuario_pck.get_cd_estabelecimento, 'UMS'),1,30));

		select	nvl(max(qt_conversao),0)
		into	qt_conversao_p
		from	material_conversao_unidade
		where	cd_material = cd_material_p
		and	upper(cd_unidade_medida) = upper(cd_unidade_medida_p);


		select	nvl(max(qt_conversao),0)
		into	qt_conversao_ml_p
		from	material_conversao_unidade
		where	cd_material = cd_material_p
		and	upper(cd_unidade_medida) = upper (obter_unid_med_usua('ML'));
	end;

	procedure obter_dados_dose_convertida(
			cd_unid_med_usua_p      in varchar2,
			cd_unidade_medida_p     in varchar2,
			cd_material_p           in number,
			cd_unid_med_cons_p      out varchar2, 
			qt_conversao_und_ori_p  out number,
			qt_conversao_und_dest_p out number) is	
	begin
		begin
			select  nvl(a.cd_unidade_medida_consumo, b.cd_unidade_medida_consumo)
			into	cd_unid_med_cons_p
			from	material_estab a,
					material b
			where	b.cd_material	= a.cd_material
			and	    a.cd_material	= cd_material_p
			and  	a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
		exception
			when others then
				begin
					select	cd_unidade_medida_consumo
					into	cd_unid_med_cons_p
					from	material
					where	cd_material = cd_material_p;
				exception
				when others then
				null;
				end;
		end;

		begin
			select nvl(max(qt_conversao),0)
			into	 qt_conversao_und_ori_p
			from	 material_conversao_unidade
			where	 upper(cd_unidade_medida) = upper(cd_unid_med_usua_p)
			and		 cd_material = cd_material_p;
	    exception
		  when others then
		      qt_conversao_und_ori_p := null;
		end;

		begin
			select nvl(max(qt_conversao),0)
			into   qt_conversao_und_dest_p
			from   material_conversao_unidade
			where  upper(cd_unidade_medida) = upper(cd_unidade_medida_p)
			and	   cd_material = cd_material_p;
	    exception
		  when others then
		      qt_conversao_und_dest_p := null;
		end;
	end;		
begin
if	(cd_material_p is not null) and
	(qt_dose_p is not null) then
	
	cd_estabelecimento_w := nvl(wheb_usuario_pck.get_cd_estabelecimento,0);

	select	nvl(max(nr_casas_diluicao),0)
	into	nr_casas_diluicao_cad_w
	from	parametro_medico
	where	cd_estabelecimento	= cd_estabelecimento_w;		

	nr_casas_diluicao_w := nr_casas_diluicao_cad_w;
	if (nr_casas_diluicao_w = 0) then
		nr_casas_diluicao_w := 3;
	end if;
	
	cd_unid_med_usua_w	:= obter_unid_med_usua('ml');

	obter_dados_dose_convertida(cd_unid_med_usua_w,cd_unidade_medida_p,cd_material_p,cd_unid_med_cons_w,qt_conversao_und_ori_w,qt_conversao_und_dest_w);

	if	(upper(cd_unidade_medida_p) <> upper(cd_unid_med_usua_w)) then

		qt_volume_total_medic_w := obter_dose_convertida(cd_material_p, qt_dose_p, cd_unidade_medida_p, cd_unid_med_usua_w);

		if nvl(qt_volume_total_medic_w, 0) = 0 then
			qt_volume_total_medic_w := nvl(cpoe_obter_conversao_ml(cd_material_p, qt_dose_p, cd_unidade_medida_p,ie_via_aplicacao_p, cd_pessoa_fisica_p, nr_atendimento_p),0);
		end if;
	else
		qt_volume_total_medic_w := qt_dose_p;
	end if;	
	
	qt_vol_item_comp_w := cpoe_obter_volume_item_comp(cd_mat_comp1_p,
													  qt_dose_comp1_p,
													  cd_unid_med_dose_comp1_p,
													  cd_mat_comp2_p,
													  qt_dose_comp2_p,
													  cd_unid_med_dose_comp2_p,
													  cd_mat_comp3_p,
													  qt_dose_comp3_p,
													  cd_unid_med_dose_comp3_p,
													  cd_mat_comp4_p,
													  qt_dose_comp4_p,
													  cd_unid_med_dose_comp4_p,
													  cd_mat_comp5_p,
													  qt_dose_comp5_p,
													  cd_unid_med_dose_comp5_p,
													  cd_mat_comp6_p,
													  qt_dose_comp6_p,
													  cd_unid_med_dose_comp6_p,
													  cd_mat_comp7_p,
													  qt_dose_comp7_p,
													  cd_unid_med_dose_comp7_p,
													  nr_sequencia_p);

	begin 
		exec_w := 'call cpoe_obter_dados_vol_md_pck.obter_soma_volume_item_md (:1,:2) into :result';

		Execute immediate exec_w using in qt_volume_total_w,
									   in qt_vol_item_comp_w,
									   out qt_volume_total_w;
					
	exception
		when others then
			qt_volume_total_w := null;
	end;
	
	if	(cd_mat_dil_p is not null) and (qt_dose_dil_p > 0) then
	
		select	max(b.ie_diluir_inteiro),
				max(b.qt_volume),
				max(b.qt_volume_medic),
				max(b.ie_proporcao)
		into	ie_diluir_inteiro_w,
				qt_volume_cad_w,
				qt_volume_medic_w,
				ie_proporcao_w
		from	material_diluicao b,
				material a
		where	a.cd_material = b.cd_material
		and 	b.nr_seq_interno = nr_seq_mat_diluicao_p
		and 	a.cd_material = cd_material_p
		and 	cd_mat_dil_p is not null;
		
		if (ie_diluir_inteiro_w = 'S') then
			select	nvl(max(a.qt_conversao), 1)
			into	qt_conversao_mat_w
			from	material_conversao_unidade a
			where	a.cd_material = cd_material_p
			and		a.cd_unidade_medida = cd_unidade_medida_p;
               
			begin
				exec_w := 'call cpoe_obter_dados_vol_md_pck.obter_conversao_mat_md (:1,:2) into :result';

				execute immediate exec_w using in qt_dose_p,
											   in qt_conversao_mat_w,
											   out qt_conversao_mat_w;
							
			exception
				when others then
					qt_conversao_mat_w := null;
			end;
            
			qt_dose_dividida_w := qt_conversao_mat_w;
			qt_dose_mat_w := obter_dose_convertida(cd_material_p, ceil(qt_dose_dividida_w), obter_dados_material(cd_material_p,'UMC'), cd_unidade_medida_p);

			obter_dados_conversao_ml(cd_material_p,cd_unidade_medida_p,cd_unid_med_usua_ml_w,cd_unid_med_cons_w,qt_conversao_w,qt_conversao_ml_w);
			begin
				exec_w := 'call obter_conversao_ml_md(:1,:2,:3,:4,:5,:6) into :result';

				execute immediate exec_w using in qt_dose_mat_w,
											   in cd_unidade_medida_p,
											   in cd_unid_med_usua_ml_w,
											   in cd_unid_med_cons_w,
											   in qt_conversao_w,
											   in qt_conversao_ml_w,
											   out qt_conv_ml_w;
			exception
				when others then
					qt_conv_ml_w := null;
			end;

			obter_dados_conversao_ml(cd_mat_dil_p,cd_unid_med_dose_dil_p,cd_unid_med_usua_ml_w,cd_unid_med_cons_w,qt_conversao_w,qt_conversao_ml_w); 			

			begin
				exec_w := 'call obter_conversao_ml_md(:1,:2,:3,:4,:5,:6) into :result';

				execute immediate exec_w using in qt_dose_dil_p,
											   in cd_unidade_medida_p,
											   in cd_unid_med_usua_ml_w,
											   in cd_unid_med_cons_w,
											   in qt_conversao_w,
											   in qt_conversao_ml_w,
											   out qt_dose_ml_w;
			exception
				when others then
					qt_dose_ml_w := null;
			end;
			
			if (qt_volume_cad_w > 0) then
				qt_conv_ml_w := qt_dose_ml_w;
			end if;
			
			obter_dados_dose_convertida(cd_unid_med_dose_dil_p,cd_unid_med_usua_w,cd_mat_dil_p,cd_unid_med_cons_w,qt_conversao_und_ori_w,qt_conversao_und_dest_w);
			begin
				exec_w := 'call cpoe_obter_dados_vol_md_pck.obter_qt_dose_ml_md (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10) into :result';

				execute immediate exec_w using in ie_diluir_inteiro_w,
											   in nr_casas_diluicao_w, 
											   in qt_dose_ml_w,  
											   in qt_conversao_mat_w, 
											   in qt_dose_dividida_w, 
											   in cd_unid_med_dose_dil_p,
											   in cd_unid_med_usua_w,			
											   in cd_unid_med_cons_w,
											   in qt_conversao_und_ori_w,
											   in qt_conversao_und_dest_w,
											   out qt_dose_ml_w;
							
			exception
				when others then
					qt_dose_ml_w := null;
			end;
		else
			obter_dados_dose_convertida(cd_unid_med_dose_dil_p,cd_unid_med_usua_w,cd_mat_dil_p,cd_unid_med_cons_w,qt_conversao_und_ori_w,qt_conversao_und_dest_w);
			begin
				exec_w := 'call cpoe_obter_dados_vol_md_pck.obter_qt_dose_ml_md (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10) into :result';

				execute immediate exec_w using in ie_diluir_inteiro_w,
											   in nr_casas_diluicao_w, 
											   in qt_dose_dil_p,  
											   in qt_conversao_mat_w, 
											   in qt_dose_dividida_w, 
											   in cd_unid_med_dose_dil_p,
											   in cd_unid_med_usua_w,			
											   in cd_unid_med_cons_w,
											   in qt_conversao_und_ori_w,
											   in qt_conversao_und_dest_w,
											   out qt_dose_ml_w;                        
			exception
				when others then
					qt_dose_ml_w := null;
			end;

			if ((nvl(ie_proporcao_w, 'F') = 'F') and (nvl(qt_volume_medic_w,0) > 0) and (nr_casas_diluicao_cad_w > 0)) then
				qt_volume_total_medic_w := qt_volume_medic_w;
			end if;
		end if;
        
		begin
			exec_w := 'call cpoe_obter_dados_vol_md_pck.obter_vol_total_com_dil_md (:1,:2,:3,:4) into :result';
			execute immediate exec_w using 	in qt_volume_total_medic_w,
											in nr_casas_diluicao_w, 
											in qt_volume_total_w, 
											in qt_dose_ml_w,											
											out qt_volume_total_w;                      
		exception
			when others then
				qt_volume_total_w := null;
		end;
        
		if ((nvl(qt_volume_medic_w,0) > 0) and (nvl(nr_casas_diluicao_cad_w, 0) > 0) and (nvl(ie_diluir_inteiro_w, 'N') = 'N')) then
			qt_conversao_ww := Obter_dose_convertida(cd_material_p, qt_volume_medic_w, obter_unid_med_usua('ml'), cd_unidade_medida_p);

			if (nvl(qt_conversao_ww,0) > 0) then
			begin
				exec_w := 'call cpoe_obter_dados_vol_md_pck.obter_vol_total_dil_prop_md (:1,:2,:3,:4,:5) into :result';

				execute immediate exec_w using 	in qt_volume_medic_w,
												in qt_dose_ml_w,
												in qt_dose_p,
												in qt_conversao_ww,
												in nr_casas_diluicao_w,
												out qt_volume_total_w;
			exception
				when others then
					qt_volume_total_w := null;
			end;
			end if;
		end if;
		
	else
		begin
		exec_w := 'call cpoe_obter_dados_vol_md_pck.obter_soma_volume_item_md (:1,:2) into :result';

		execute immediate exec_w using in qt_volume_total_w,
									   in qt_volume_total_medic_w,
									   out qt_volume_total_w;
					  
		exception
			when others then
				qt_volume_total_w := null;
		end;
	end if;
	
	if	(cd_mat_red_p is not null) and 
		(qt_dose_red_p > 0) then		
		
		obter_dados_dose_convertida(cd_unid_med_dose_red_p,cd_unid_med_usua_w,cd_mat_red_p,cd_unid_med_cons_w,qt_conversao_und_ori_w,qt_conversao_und_dest_w);
		begin
		  exec_w := 'call cpoe_obter_dados_vol_md_pck.obter_vol_total_com_red_md(:1,:2,:3,:4,:5,:6,:7,:8) into :result';

		  execute immediate exec_w using in qt_volume_red_p,
										 in qt_dose_red_p,
										 in cd_unid_med_usua_w, 
										 in cd_unid_med_dose_red_p, 			  
										 in cd_unid_med_cons_w,
										 in qt_conversao_und_ori_w,
										 in qt_conversao_und_dest_w,
										 in qt_volume_total_w,
										 out qt_volume_total_w;
		exception
		  when others then
			qt_volume_total_w := null;
		end;
		
	end if;
end if;

return	qt_volume_total_w;

end cpoe_obter_volume_total;
/