create or replace
function obter_protocolo_descricao(cd_protocolo_p	number,
				nr_seq_protocolo_p	number,
				cd_procedimento_p	number,
				ie_origem_proced_p	number,
				ie_opcao_p			varchar2,
				nr_seq_item_prot_p	number)
 		    	return varchar2 is
			
ds_descricao_w	varchar2(254)	:= '';			

begin

if	(cd_protocolo_p is not null) and (nr_seq_protocolo_p is not null) then

	if	(cd_procedimento_p is not null) and 
		(ie_origem_proced_p is not null) and
		(ie_opcao_p	= 'P') then
	
		select	substr(max(a.ds_prescricao),1,254)
		into	ds_descricao_w
		from	protocolo_medic_proc a
		where	cd_protocolo 	 = cd_protocolo_p
		and	nr_sequencia 	 = nr_seq_protocolo_p
		and	cd_procedimento  = cd_procedimento_p
		and	ie_origem_proced = ie_origem_proced_p;
		
	elsif	(ie_opcao_p = 'HM') then
	
		select	substr(max(ds_prescricao),1,254)
		into	ds_descricao_w		
		from	prot_solic_bco_sangue
		where	cd_protocolo 	 = cd_protocolo_p
		and		nr_seq_protocolo = nr_seq_protocolo_p
		and		nr_sequencia	= nr_seq_item_prot_p;
	
	end if;	
	
end if;

return	ds_descricao_w;

end obter_protocolo_descricao;
/
