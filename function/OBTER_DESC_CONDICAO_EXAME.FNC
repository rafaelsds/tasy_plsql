create or replace
function obter_desc_condicao_exame(	nr_sequencia_p	number)
					return varchar2 is

ds_condicao_exame_w	varchar2(255);
					
begin

if	(nr_sequencia_p > 0) then
	select	max(ds_condicao_exame)
	into	ds_condicao_exame_w
	from	condicao_exame
	where	nr_sequencia	=	nr_sequencia_p;
end if;

return	ds_condicao_exame_w;

end obter_desc_condicao_exame;
/