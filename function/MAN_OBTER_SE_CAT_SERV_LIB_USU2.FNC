create or replace
function man_obter_se_cat_serv_lib_usu2
			(	nr_seq_servico_p		number,
				nm_usuario_p			varchar2,
				cd_setor_usuario_p		number,
				cd_estabelecimento_p		number)
 		    	return varchar2 is

ds_retorno_w			varchar2(01)	:= 'S';
cd_estabelecimento_w		number(04,0);
ie_permissao_cat_serv_w		varchar2(01);
qt_existe_w			number(10,0);

begin
select	count(1)
into	qt_existe_w
from	man_catalogo_setor
where	nr_seq_cs	= nr_seq_servico_p
and	rownum		= 1;

if	(qt_existe_w = 0) then
	ds_retorno_w	:= 'S';
elsif	(qt_existe_w > 0) then
	begin
	select	substr(nvl(obter_valor_param_usuario(299, 158, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1)
	into	ie_permissao_cat_serv_w
	from	dual;
	
	if	(ie_permissao_cat_serv_w = 'T') then
		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	man_catalogo_setor
		where	nr_seq_cs 		= nr_seq_servico_p
		and	(substr(obter_se_setor_usuario(cd_setor_atendimento, nm_usuario_p),1,1) = 'S');
	elsif	(ie_permissao_cat_serv_w = 'U') and 
		(nvl(cd_setor_usuario_p,0) > 0) then
		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	man_catalogo_setor
		where	nr_seq_cs 		= nr_seq_servico_p
		and	cd_setor_atendimento	= cd_setor_usuario_p;
	else
		ds_retorno_w	:= ie_permissao_cat_serv_w;
	end if;
	end;
end if;

return	nvl(ds_retorno_w,'N');

end man_obter_se_cat_serv_lib_usu2;
/