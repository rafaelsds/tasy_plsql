create or replace
function obter_se_depart_atend_usuario (nr_atendimento_p	number)
				return varchar2 as

ds_retorno_w	varchar2(1);
cd_setor_atendimento_w	number(15);

begin
	cd_setor_atendimento_w := obter_setor_atendimento(nr_atendimento_p);
	if (cd_setor_atendimento_w is null) then
		cd_setor_atendimento_w := obter_ultimo_setor_atendimento(nr_atendimento_p);
	end if;
	select 	nvl(max('S'),'N')
	into	ds_retorno_w
	from   	departamento_setor a,
			setor_atendimento b
	where 	b.cd_setor_atendimento = a.cd_setor_atendimento
	and 	a.cd_setor_atendimento = cd_setor_atendimento_w
	and 	obter_se_depart_lib_setor(a.cd_departamento) = 'S';

return	ds_retorno_w;

end obter_se_depart_atend_usuario;
/