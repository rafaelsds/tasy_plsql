create or replace
function Obter_desc_agrupamento_setor(cd_setor_atendimento_p		number)
 		    	return varchar is
			
ds_agrupamento_w 	varchar2(255);

begin

select  nvl(max(ds_agrupamento),wheb_mensagem_pck.get_texto(802724))
into	ds_agrupamento_w
from	setor_atendimento a,
	agrupamento_setor b
where   a.cd_setor_atendimento = cd_setor_atendimento_p
and	b.nr_sequencia = a.nr_seq_agrupamento;

return	ds_agrupamento_w;

end Obter_desc_agrupamento_setor;
/
