Create or Replace
function obter_se_setor_usuario(
			cd_setor_atendimento_p	Number,
  			nm_usuario_p		varchar2)
			return varchar2 is

ie_setor_usuario_w	varchar2(1);

begin

	begin
	select	'S'
	into	ie_setor_usuario_w
	from	usuario
	where	cd_setor_atendimento	= cd_setor_atendimento_p
	and	nm_usuario = nm_usuario_p
	and	rownum  = 1;
	exception
	when no_data_found then
		ie_setor_usuario_w := 'N';
	end;


if (ie_setor_usuario_w = 'N') then
	
	begin
	select	'S'
	into	ie_setor_usuario_w
	from	usuario_setor
	where	cd_setor_atendimento	= cd_setor_atendimento_p
	and	nm_usuario_param = nm_usuario_p
	and	rownum  = 1;
	exception
	when no_data_found then
		ie_setor_usuario_w := 'N';
	end;

end if;
	
return	ie_setor_usuario_w;


End obter_se_setor_usuario;
/
