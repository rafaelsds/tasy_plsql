create or replace
function obter_feriado(	dt_feriado_p		date,
			cd_estabelecimento_p	number )
			return varchar2 is

ds_motivo_feriado_w	varchar2(40);

begin

begin
select	ds_motivo_feriado
into	ds_motivo_feriado_w
from	feriado
where	dt_feriado		= dt_feriado_p
and	cd_estabelecimento	= cd_estabelecimento_p;
exception 
	 when others then
	 ds_motivo_feriado_w	:= null;
end;

return	ds_motivo_feriado_w;

end obter_feriado;
/