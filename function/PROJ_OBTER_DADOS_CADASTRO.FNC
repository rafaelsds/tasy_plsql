create or replace 
FUNCTION proj_obter_dados_cadastro(nr_sequencia_p	number,
				ie_opcao_p	varchar2)
					return varchar2 is

ds_retorno_w		varchar2(255);
ds_cadastro_w		varchar2(255);

/*
D - Descricao
*/

begin
if	(nr_sequencia_p is not null) then
	select	substr(obter_desc_expressao(cd_exp_cadastro, ds_cadastro), 1, 255) ds_cadastro
	into	ds_cadastro_w
	from	proj_cadastro
	where	nr_sequencia	= nr_sequencia_p;

	if	(ie_opcao_p = 'D') then
		ds_retorno_w	:= ds_cadastro_w;
	end if;
end if;

return ds_retorno_w;

end proj_obter_dados_cadastro;
/
