create or replace function obter_se_contrato_titulo(nr_titulo_pagar_p	number,
						nr_titulo_receber_p	number)
				return varchar2 is

/* Function utilizada no fluxo de caixa para identificar se o t�tulo possui contrato vinculado */
qt_reg_w		number(10);
ie_contrato_w		varchar2(1) := 'N';

begin
	if (nr_titulo_pagar_p is not null) then
		select	decode(b.nr_seq_contrato,null,'N','S')
		into	ie_contrato_w
		from 	titulo_pagar b
		where	b.nr_titulo = nr_titulo_pagar_p;
	elsif (nr_titulo_receber_p is not null) then
		select	decode(b.nr_seq_contrato,null,'N','S')
		into	ie_contrato_w
		from 	titulo_receber b
		where	b.nr_titulo = nr_titulo_receber_p;
	else
		ie_contrato_w := 'N';
	end if;
	
	return ie_contrato_w;
end obter_se_contrato_titulo;
/
