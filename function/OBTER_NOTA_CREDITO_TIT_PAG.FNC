create or replace
function obter_nota_credito_tit_pag(	nr_titulo_pagar_p	number)
				return varchar2 is

ds_nota_credito_w			varchar2(4000);
nr_seq_nota_credito_w		number(10);

cursor c01 is
select	a.nr_seq_nota_credito
from	titulo_pagar a
where	a.nr_titulo		= nr_titulo_pagar_p
and	a.nr_seq_nota_credito is not null
union
select	a.nr_seq_nota_credito
from	titulo_pagar c,
	bordero_nc b,
	bordero_nc_nota_credito a
where	a.nr_seq_bordero	= b.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_bordero_nc
and	c.nr_titulo		= nr_titulo_pagar_p;

begin

ds_nota_credito_w		:= '';

open C01;
loop
fetch C01 into	
	nr_seq_nota_credito_w;
exit when C01%notfound;
	begin
	
	ds_nota_credito_w	:= substr(ds_nota_credito_w || nr_seq_nota_credito_w || ', ',1,4000);

	end;
end loop;
close C01;

ds_nota_credito_w	:= substr(ds_nota_credito_w,1,length(ds_nota_credito_w)-2);

return	ds_nota_credito_w;

end obter_nota_credito_tit_pag;
/