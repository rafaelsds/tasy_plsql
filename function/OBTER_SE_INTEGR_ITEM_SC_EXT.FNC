create or replace
function obter_se_integr_item_sc_ext(
			nr_solic_compra_p		number,
			nr_item_solic_compra_p	number,
			ie_sistema_ext_p		varchar2)
	return varchar2 is

ie_tipo_servico_w				varchar2(15);
nr_seq_regra_w				number(10);
cd_material_w				number(10);
cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
nr_seq_familia_w				number(5);
ie_integra_w				varchar2(1) := 'N';
qt_integra_w				number(10) := 0;
qt_itens_w				number(10) := 0;
cd_estabelecimento_w			number(10);
ie_padronizado_w				varchar2(1);
ie_estoque_w				varchar2(1);
cd_perfil_w				number(5);
cd_centro_custo_w				number(8);
ie_sistema_ext_w				varchar2(15);
nr_prescricao_w				number(10);
ie_envia_integracao_w			varchar2(1);

cursor c01 is
select	cd_material
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p
and	nr_item_solic_compra = nr_item_solic_compra_p;

cursor c02 is
select	nvl(ie_integra,'S')
from	regra_integr_sc_estrut
where	nr_seq_regra = nr_seq_regra_w
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(cd_material, cd_material_w) 			= cd_material_w or cd_material_w = 0)
and	((nvl(ie_estoque,'T') = 'T') or
	((nvl(ie_estoque,'T') = 'S') and (ie_estoque_w = 'S')) or
	((nvl(ie_estoque,'T') = 'N') and (ie_estoque_w = 'N')))
and	((nvl(ie_padronizado,'T') = 'T') or
	((nvl(ie_padronizado,'T') = 'S') and (ie_padronizado_w = 'S')) or
	((nvl(ie_padronizado,'T') = 'N') and (ie_padronizado_w = 'N')))
and	((nvl(ie_solic_prescr,'T') = 'T') or
	((nvl(ie_solic_prescr,'T') = 'S') and (nr_prescricao_w > 0)) or
	((nvl(ie_solic_prescr,'T') = 'N') and (nr_prescricao_w = 0)))
order by
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);

cursor c03 is
select	a.nr_sequencia
from	regra_integr_solic_compra a
where	a.ie_tipo_servico = ie_tipo_servico_w
and	a.ie_integracao = ie_sistema_ext_w
and	(nvl(cd_centro_custo, cd_centro_custo_w) = cd_centro_custo_w or cd_centro_custo is null)
and	(cd_perfil = cd_perfil_w or cd_perfil is null)
and exists(
	select	1
	from	regra_integr_sc_estrut x
	where	x.nr_seq_regra = a.nr_sequencia
	and	nvl(x.cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
	and	nvl(x.cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
	and	nvl(x.cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
	and	(nvl(x.cd_material, cd_material_w) 			= cd_material_w or cd_material_w = 0)
	and	((nvl(x.ie_estoque,'T') = 'T') or
		((nvl(x.ie_estoque,'T') = 'S') and (ie_estoque_w = 'S')) or
		((nvl(x.ie_estoque,'T') = 'N') and (ie_estoque_w = 'N')))
	and	((nvl(x.ie_padronizado,'T') = 'T') or
		((nvl(x.ie_padronizado,'T') = 'S') and (ie_padronizado_w = 'S')) or
		((nvl(x.ie_padronizado,'T') = 'N') and (ie_padronizado_w = 'N')))
	and	((nvl(x.ie_solic_prescr,'T') = 'T') or
		((nvl(x.ie_solic_prescr,'T') = 'S') and (nr_prescricao_w > 0)) or
		((nvl(x.ie_solic_prescr,'T') = 'N') and (nr_prescricao_w = 0))))
order by 	nvl(cd_centro_custo,999999) desc,
	nvl(cd_perfil,99999) desc;

	
begin

ie_sistema_ext_w	:= upper(ie_sistema_ext_p);

select	nvl(ie_tipo_servico,'ES'),
	cd_estabelecimento,
	Obter_perfil_Ativo,
	nvl(cd_centro_custo,0),
	nvl(nr_prescricao,0),
	nvl(ie_envia_integracao,'N')
into	ie_tipo_servico_w,
	cd_estabelecimento_w,
	cd_perfil_w,
	cd_centro_custo_w,
	nr_prescricao_w,
	ie_envia_integracao_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

ie_integra_w := 'N';

if	(ie_envia_integracao_w = 'S') then
	ie_integra_w := 'S';
else
	open C01;
	loop
	fetch C01 into	
		cd_material_w;
	exit when C01%notfound;
		begin

		select	a.cd_grupo_material,
			a.cd_subgrupo_material,
			a.cd_classe_material,
			b.ie_padronizado,
			b.ie_material_estoque
		into	cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w,
			ie_padronizado_w,
			ie_estoque_w
		from	estrutura_material_v a,
			material_estab b
		where	a.cd_material = b.cd_material
		and	b.cd_estabelecimento = cd_estabelecimento_w
		and	a.cd_material = cd_material_w;

		open C03;
		loop
		fetch C03 into	
			nr_seq_regra_w;
		exit when C03%notfound;
			begin
			nr_seq_regra_w := nr_seq_regra_w;
			end;
		end loop;
		close C03;

		if	(nr_seq_regra_w > 0) then
		
			open C02;
			loop
			fetch C02 into	
				ie_integra_w;
			exit when C02%notfound;
				begin	
				ie_integra_w	:= ie_integra_w;
				end;
			end loop;
			close C02;		

		end if;
		end;
	end loop;
	close C01;	
end if;

return ie_integra_w;

end obter_se_integr_item_sc_ext;
/
