create or replace
function obter_com_cliente_cnpj_ata (
			nr_seq_ata_p	number) 
			return varchar2 is
cd_cnpj_w	varchar2(14) := '';

begin
if	(nr_seq_ata_p is not null) then
	begin
	select	max(cd_cnpj)
	into	cd_cnpj_w
	from	com_cliente a,
			proj_ata	b
	where	a.nr_sequencia = b.nr_seq_cliente
	and		b.nr_sequencia = nr_seq_ata_p;
	end;
end if;
return cd_cnpj_w;
end obter_com_cliente_cnpj_ata;
/