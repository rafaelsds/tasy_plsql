create or replace
function verifica_regra_turno_usuario(cd_agenda_p	number,
				     dt_agenda_p	date,
             cd_pessoa_fisica_p varchar2,
             nr_seq_horario_p   number)
 		    	return varchar2 is
			
ie_retorno_w	varchar2(1 char);
nr_seq_horario_w number(10);
qt_regras_usuario_w number(10);
begin

  ie_retorno_w := 'S';
  
  if (nr_seq_horario_p is null) then
    nr_seq_horario_w := obter_turno_encaixe_ageexame(cd_agenda_p, dt_agenda_p);
  else
    nr_seq_horario_w := nr_seq_horario_p;
  end if;

  select count(1)
  into qt_regras_usuario_w
  from agenda_horario_permissao
  where nr_seq_horario = nr_seq_horario_w
  and ie_situacao = 'A';

  if (qt_regras_usuario_w > 0) then

    select decode(count(1), 0, 'N', 'S')
    into ie_retorno_w
    from agenda_horario_permissao
    where nr_seq_horario = nr_seq_horario_w
    and cd_pessoa_fisica = cd_pessoa_fisica_p
    and ie_situacao = 'A';

  end if;

  return	ie_retorno_w;

end verifica_regra_turno_usuario;
/
