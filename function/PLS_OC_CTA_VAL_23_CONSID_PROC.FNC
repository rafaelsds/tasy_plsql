create or replace
function pls_oc_cta_val_23_consid_proc(	dados_regra_p			pls_tipos_ocor_pck.dados_regra,
					nr_seq_conta_proc_p		pls_conta_proc_v.nr_sequencia%type,
					nr_seq_proc_ref_p		pls_conta_proc_v.nr_seq_proc_ref%type,
					nr_seq_partic_hi_p		pls_conta_proc_v.nr_seq_participante_hi%type,
					ie_tipo_conta_p			pls_conta_v.ie_tipo_conta%type,
					nr_seq_outro_proc_p		pls_conta_proc_v.nr_sequencia%type,
					nr_seq_outro_proc_ref_p		pls_conta_proc_v.nr_seq_proc_ref%type,
					nr_seq_outro_partic_hi_p	pls_conta_proc_v.nr_seq_participante_hi%type)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Verificar se os procedimentos passados por par�metros devem ser considerados iguais
	para a regra de utiliza��o de itens.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjung 17/09/2013 - Cria��o da function
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ie_considera_w		varchar2(1);

qt_partic_proc_w	pls_integer;
qt_partic_outro_proc_w	pls_integer;

dados_partic_proc_w	pls_tipos_ocor_pck.dados_participante;

ds_sql_partic_w		varchar2(4000);
ds_sql_outro_partic_w	varchar2(4000);
ds_res_outro_partic_w	varchar2(2000);

var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;

var_cur_outro_w		pls_integer;
var_exec_outro_w	pls_integer;
var_retorno_outro_w	pls_integer;

-- Retorna o acesso a tabela de participantes que deve ser feito, ou seja, diz de quem deve ser buscado os participantes.
function obter_acesso_proc_partic(	ie_opcao_p			varchar2,
					nr_cursor_p			pls_integer,
					nr_seq_conta_proc_p		pls_conta_proc_v.nr_sequencia%type,
					nr_seq_proc_ref_p		pls_conta_proc_v.nr_seq_proc_ref%type,
					nr_seq_partic_hi_p		pls_conta_proc_v.nr_seq_participante_hi%type) 
					return varchar2 is
ds_retorno_w	varchar2(255);

begin

-- Participante de refer�ncia, quando este for informado apenas retorna o mesmo para que seja verificado os participantes do outro procedimento.
if	(nr_seq_partic_hi_p is not null) then
				
	-- Verificar se � para montar a restricao ou atualizar o valor das Binds
	if	(ie_opcao_p = 'RESTRICAO') then
		
		ds_retorno_w := 'and	partic.nr_sequencia = :nr_seq_partic ';
	else
		dbms_sql.bind_variable(nr_cursor_p, ':nr_seq_partic', nr_seq_partic_hi_p);
	end if;
-- Quando for informado apenas o procedimento de refer�ncia e n�o for informado o participante de refer�ncia, ent�o deve
-- ser retornodo todos os partipantes do procedimento de refer�ncia.			
elsif	(nr_seq_proc_ref_p is not null) then

	-- Verificar se � para montar a restricao ou atualizar o valor das Binds
	if	(ie_opcao_p = 'RESTRICAO') then
		
		ds_retorno_w := 'and	partic.nr_seq_conta_proc = :nr_seq_proc_ref ';
	else
		dbms_sql.bind_variable(nr_cursor_p, ':nr_seq_proc_ref', nr_seq_proc_ref_p);
	end if;
else
	-- Se n�o vai atr�s dos participantes do procedimento mesmo.
	-- Verificar se � para montar a restricao ou atualizar o valor das Binds
	if	(ie_opcao_p = 'RESTRICAO') then
		
		ds_retorno_w := 'and	partic.nr_seq_conta_proc = :nr_seq_conta_proc ';
	else
		dbms_sql.bind_variable(nr_cursor_p, ':nr_seq_conta_proc', nr_seq_conta_proc_p);
	end if;
end if;

return ds_retorno_w;

end obter_acesso_proc_partic;

-- retorna qual a valida��o deve ser aplicada no participante para que ele seja considerado igual ao participante da tabela de sele��o.
function obter_validacao_proc_partic (	ie_opcao_p	varchar2,
					nr_cursor_p	pls_integer,
					dados_regra_p	pls_tipos_ocor_pck.dados_regra,
					dados_partic_p	pls_tipos_ocor_pck.dados_participante,
					ie_tipo_conta_p	pls_conta_v.ie_tipo_conta%type) 
					return varchar2 is
ds_retorno_w	varchar2(2000);

begin

-- Deve ser verificado o evento de gera��o das ocorr�ncias para que se saiba de onde buscar a informa��o.
if	(dados_regra_p.ie_evento = 'IMP') then
	
	-- Caso o participante tenha informa��o de grau de participa��o informada ent�o busca os dados do grau de participa��o dos participantes
	-- do outro procedimento.
	if	(dados_partic_p.cd_grau_partic_imp is not null) then
		
		-- Quando for conta de interc�mbio deve se olhado o campo CD_PTU da PLS_GRAU_PARTICIPACAO e comparar com o c�digo
		-- de participa��o informado para o participante do procedimento que est� sendo consistido. Caso contr�rio usa o campo CD_TISS.
		if	(ie_tipo_conta_p = 'I') then
			
			if	(ie_opcao_p = 'RESTRICAO') then
			
				ds_retorno_w :=	'and	partic.nr_seq_grau_partic in (	select	grau_partic.nr_sequencia ' || pls_tipos_ocor_pck.enter_w ||
						'					from	pls_grau_participacao grau_partic ' || pls_tipos_ocor_pck.enter_w ||
						'					where	grau_partic.cd_ptu = :cd_ptu )';
			else
				dbms_sql.bind_variable(nr_cursor_p, ':cd_ptu', dados_partic_p.cd_grau_partic_imp);
			end if;
			
		else
		
			if	(ie_opcao_p = 'RESTRICAO') then
			
				ds_retorno_w := 'and	partic.nr_seq_grau_partic in (	select	grau_partic.nr_sequencia '  || pls_tipos_ocor_pck.enter_w ||
						'					from	pls_grau_participacao grau_partic '|| pls_tipos_ocor_pck.enter_w ||
						'					where	grau_partic.cd_tiss = :cd_tiss ) ';
			else
				dbms_sql.bind_variable(nr_cursor_p, ':cd_tiss', dados_partic_p.cd_grau_partic_imp);
			end if;
			
		end if;
	end if;
	
else	-- Se o evento n�o for importa��o deve ser contado os participantes atrav�s do campo nr_seq_grau_partic da PLS_PROC_PARTICIPANTE. Caso exista
	-- algum participante com o mesmo campo informado para o procedimento que est� sendo obtido para contagem ent�o o mesmo � inserido e j� aborta o cursor dos 
	-- participantes.
	if	(dados_partic_p.nr_seq_grau_partic is not null) then
		
		if	(ie_opcao_p = 'RESTRICAO') then
			
			ds_retorno_w :=	'and	partic.nr_seq_grau_partic = :nr_seq_grau_partic ';
		else
			dbms_sql.bind_variable(nr_cursor_p, ':nr_seq_grau_partic', dados_partic_p.nr_seq_grau_partic);
		end if;
	end if;		
end if;

return ds_retorno_w;

end obter_validacao_proc_partic;

begin

-- Por padr�o n�o � considerado o procedimento, para que isto mude � necess�rio que o procedimento se encaixe em determinado cen�rio.
ie_considera_w := 'N';

-- Deve ter informa��o sobre os procedimentos para que seja poss�vel aplicar a valida��o.
if	(nr_seq_conta_proc_p is not null and nr_seq_outro_proc_p is not null) then

	-- Se estiver falando do mesmo procedimento ent�o insere e n�o � necess�rio verificar os participantes.
	if	(nr_seq_conta_proc_p = nr_seq_outro_proc_p) then
		
		ie_considera_w := 'S';
	else
		-- Verificar de onde devem ser buscados os participantes do procedimento. Deve ser separado por que as contas de HI n�o tem os participantes
		-- e  sempre deve ser verificado o participante do procedimento de refer�ncia ou o participante ao qual o procedimento est� verificado.
		
		-- Participante de refer�ncia, quando este for informado apenas verifica se existe este participante na base.
		if	( nr_seq_partic_hi_p is not null ) then

			select	count(1)
			into	qt_partic_proc_w
			from	pls_proc_participante
			where	nr_sequencia = nr_seq_partic_hi_p;
			
		-- Quando for informado apenas o procedimento de refer�ncia e n�o for informado o participante de refer�ncia, ent�o deve
		-- ser verificado se o procedimento de refer�ncia tem participantes informado.
		elsif	(nr_seq_proc_ref_p is not null) then
		
			select count(1)
			into	qt_partic_proc_w
			from	pls_proc_participante
			where	nr_seq_conta_proc = nr_seq_proc_ref_p;
		else
			-- Se n�o verifica se o pr�prio procedimento tem participantes informados.
			select	count(1)
			into	qt_partic_proc_w
			from	pls_proc_participante
			where	nr_seq_conta_proc = nr_seq_conta_proc_p;
		end if;
		
		-- Se o procedimento tiver participante
		if	(qt_partic_proc_w > 0) then
		
			-- Montar o select para obter os participantes do item que est� na tabela de sele��o.
			ds_sql_partic_w	:=	'select	partic.nr_sequencia, ' || pls_tipos_ocor_pck.enter_w || 
						'	partic.nr_seq_grau_partic, ' || pls_tipos_ocor_pck.enter_w || 
						'	partic.cd_grau_partic_imp ' || pls_tipos_ocor_pck.enter_w || 
						'from	pls_proc_participante partic ' || pls_tipos_ocor_pck.enter_w || 
						'where	1 = 1 ' || pls_tipos_ocor_pck.enter_w || 
						-- Obter o acesso as participantes, seja do procedimento, do procedimento de refer�ncia ou um participante em espec�fico.
						obter_acesso_proc_partic(	'RESTRICAO', var_cur_w, 
										nr_seq_conta_proc_p,
										nr_seq_proc_ref_p,
										nr_seq_partic_hi_p);  
			-- Prepara��o para varrer os participantes do procedimento da tabela de sele��o.
			-- Abertura de um novo cursor
			var_cur_w := dbms_sql.open_cursor;
			-- Realizar o parse com o comando montado din�micamente
			dbms_sql.parse(var_cur_w,  ds_sql_partic_w, 1);
			
			-- Atualizar o valor das binds na consulta dos participantes do procedimento.
			ds_sql_partic_w	:=	obter_acesso_proc_partic(	'BINDS', var_cur_w, 
										nr_seq_conta_proc_p,
										nr_seq_proc_ref_p,
										nr_seq_partic_hi_p);  
			
			-- preparar o retorno do comando para que seja poss�vel relizar o fetch dos valores.
			dbms_sql.define_column(var_cur_w, 1, dados_partic_proc_w.nr_sequencia);
			dbms_sql.define_column(var_cur_w, 2, dados_partic_proc_w.nr_seq_grau_partic);
			dbms_sql.define_column(var_cur_w, 3, dados_partic_proc_w.cd_grau_partic_imp, 2);
			
			-- Executar o comando.
			var_exec_w := dbms_sql.execute(var_cur_w);
			
			-- Aqui busca os participantes do item que est� sendo consistido para verificar s algum destes est� tamb�m no item que est� sendo obtido para a contagem.
			loop
			var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
			exit when var_retorno_w = 0;
			
				-- Obter os valores da linha do resultado do select.
				dbms_sql.column_value(var_cur_w, 1, dados_partic_proc_w.nr_sequencia);
				dbms_sql.column_value(var_cur_w, 2, dados_partic_proc_w.nr_seq_grau_partic);
				dbms_sql.column_value(var_cur_w, 3, dados_partic_proc_w.cd_grau_partic_imp);
				
				-- Para cada paticipante zera a var�vel de controle.
				qt_partic_outro_proc_w := 0;
				
				-- Montar o comando que ser� usado para verificar se o outro procedimento tem o mesmo participante que o procedimento informado.
				ds_sql_outro_partic_w :=	'select	count(1) ' || pls_tipos_ocor_pck.enter_w ||
								'from	pls_proc_participante partic ' || pls_tipos_ocor_pck.enter_w ||
								'where	1 = 1 ' || pls_tipos_ocor_pck.enter_w ||
								-- Obt�m o acesso que ser� realizado para os participantes do outro procedimento.
								obter_acesso_proc_partic(	'RESTRICAO', var_cur_outro_w,
												nr_seq_outro_proc_p,
												nr_seq_outro_proc_ref_p,
												nr_seq_outro_partic_hi_p);
				
				-- Obter a restri��o de valida��o do participante para verificar se o participante do outro procedimento
				-- � o mesmo do procedimento da tabela de sele��o.
				ds_res_outro_partic_w := obter_validacao_proc_partic (	'RESTRICAO', var_cur_outro_w,
											dados_regra_p,
											dados_partic_proc_w,
											ie_tipo_conta_p);
				
				-- Caso n�o tenha sido retornado nenhuma restri��o de valida��o do participante significa que o participante do procedimento
				-- da tabela de sele��o n�o tem os dados necess�rio informado e n�o ser� feita nenhuma valida��o sobre o  outro procedimento, 
				-- nenuhm procedimento ser� considerado para um participante que n�o tem dados informados.
				if	(ds_res_outro_partic_w is not null or
					ds_res_outro_partic_w <> '') then
				
				
					ds_sql_outro_partic_w :=	ds_sql_outro_partic_w || pls_tipos_ocor_pck.enter_w || 
									ds_res_outro_partic_w;
												
					var_cur_outro_w := dbms_sql.open_cursor;
					dbms_sql.parse(var_cur_outro_w,  ds_sql_outro_partic_w, 1);
					
					-- Atualizar as binds da consulta dos participantes do outro procedimento.
					ds_sql_outro_partic_w := obter_acesso_proc_partic(	'BINDS', var_cur_outro_w,
												nr_seq_outro_proc_p,
												nr_seq_outro_proc_ref_p,
												nr_seq_outro_partic_hi_p);
					
					
					-- Obter a restri��o de valida��o do participante para verificar se o participante do outro procedimento
					-- � o mesmo do procedimento da tabela de sele��o.
					ds_res_outro_partic_w := obter_validacao_proc_partic (	'BINDS', var_cur_outro_w,
												dados_regra_p,
												dados_partic_proc_w,
												ie_tipo_conta_p);
					
					-- Preparar o retorno do comando de contagem dos participantes do outro procedimento.
					dbms_sql.define_column(var_cur_outro_w, 1, qt_partic_outro_proc_w);
					
					-- Executar a contagem dos participantes do outro procedimento.
					var_exec_outro_w := dbms_sql.execute(var_cur_outro_w);
					
					loop
					var_retorno_outro_w := dbms_sql.fetch_rows(var_cur_outro_w);
					exit when var_retorno_outro_w = 0;
						
						-- Atualizar o valor da contagem dos participantes do outro procedimento
						dbms_sql.column_value(var_cur_outro_w, 1, qt_partic_outro_proc_w);
					end loop;
					dbms_sql.close_cursor(var_cur_outro_w);-- Pariticpantes do outro procedimento
				end if;
				
				-- Se o procedimento que est� sendo obtido para a contagem cont�m algum participante com mesmo grau de participa��o do proceidmento sendo consistido
				-- ent�o o mesmo deve ser inserido portanto j� aborta o cursor dos participantes e insere o procedimento na tabela de sele��o.
				if	(qt_partic_outro_proc_w > 0) then
					
					ie_considera_w := 'S';
					exit;
				end if;
			end loop;
			dbms_sql.close_cursor(var_cur_w); -- Pariticpantes do procedimento da tabela de sele��o
		else		
			-- Se o procedimento que est� na tabela de sele��o n�o tiver participantes ent�o deve ser verificado se o outro procedimento tem algum participante, 
			-- Caso ele tenha et�o n�o ser� considerado, se n�o tiver ser� considerado.
			-- A octen��o se deve do mesmo modo, primeiro verifica se tem participante de refer�ncia informado, 
			-- depois se tem procedimento de refer�ncia informado, e se n�o tiver nenhum dos dois ent�o verifica os participantes do procedimento.
			-- Participante de refer�ncia
			if	(nr_seq_outro_partic_hi_p is not null) then
				
				select	count(1)
				into	qt_partic_outro_proc_w
				from	pls_proc_participante
				where	nr_sequencia = nr_seq_outro_partic_hi_p;
				
			-- Procedimento de refer�ncia	
			elsif	(nr_seq_outro_proc_ref_p is not null) then
			
				select	count(1)
				into	qt_partic_outro_proc_w
				from	pls_proc_participante
				where	nr_seq_conta_proc = nr_seq_outro_proc_ref_p;
			else	
				-- se n�o tiver nenhum dos dois conta os participantes do procedimento.
				select	count(1)
				into	qt_partic_outro_proc_w
				from	pls_proc_participante
				where	nr_seq_conta_proc = nr_seq_outro_proc_p;
			end if;
			
			-- Se o outro procedimento tiver participantes e o procedimento da tabela de sele��o n�o tiver ent�o
			-- n�o deve ser considerado, caso contr�rio deve.
			if	(qt_partic_outro_proc_w > 0) then
				
				ie_considera_w := 'N';
			else
				ie_considera_w := 'S';
			end if;
		end if;
	end if;
else
	ie_considera_w := 'N';
end if;

return	ie_considera_w;

end pls_oc_cta_val_23_consid_proc;
/