create or replace
function gv_obter_se_proc_biobanco(nr_seq_agenda_p		number)
 		    	return varchar2 is

ie_pac_biobanco_w	varchar2(1);
begin

select	 decode(count(*),0,'N','S')
into	 ie_pac_biobanco_w
from	 agenda_pac_servico
where	 nr_seq_agenda = nr_seq_agenda_p
and	 exists		(select 1
			from	gestao_vaga_biobanco a
			where	a.nr_seq_proc_interno = nr_seq_proc_servico);
	
return	ie_pac_biobanco_w;

end gv_obter_se_proc_biobanco;
/