CREATE OR REPLACE
FUNCTION Obter_Classif_CTB(	cd_classificacao_p 	varchar2,
				ie_opcao_p	varchar2)
			return	varchar2  is
/* ie_opcao_p
NIVEL =	N�vel
SUPERIOR = Superior*/

ds_resultado_w		Varchar2(40);
i			Integer;
ie_nivel_w		Number(5,0);
qt_posicao_w		Number(5,0);

BEGIN
ds_resultado_w		:= null;
qt_posicao_w		:= 0;
if	(upper(ie_opcao_p) =  'NIVEL') and
	(cd_classificacao_p is not null)  then
	begin
	ie_nivel_w				:= 1;
	for	i in 1..length(cd_classificacao_p) loop
		if	(substr(cd_classificacao_p,i, 1) = '.')  then
			ie_nivel_w		:= ie_nivel_w + 1;
		end if;
	end loop;
	ds_resultado_w			:= ie_nivel_w;
	end;
elsif	(upper(ie_opcao_p) =  'SUPERIOR') and
	(cd_classificacao_p is not null)  then
	begin
	for	i in 1..length(cd_classificacao_p) loop
		if	(substr(cd_classificacao_p,i, 1) = '.')  then
			qt_posicao_w		:= i;
		end if;
	end loop;
	if	(qt_posicao_w	> 0) then
		ds_resultado_w			:= substr(cd_classificacao_p,1,qt_posicao_w - 1);
	end if;
	end;
end if;
return ds_resultado_w;
END Obter_Classif_CTB;
/