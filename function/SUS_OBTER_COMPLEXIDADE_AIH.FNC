create or replace
function sus_obter_complexidade_aih(	nr_interno_conta_p	number)
 		    		return varchar2 is

cd_procedimento_real_w		number(15);
ie_alta_complexidade_w		varchar2(2);
ie_ordem_w			number(10);	
nr_detalhe_proc_w		number(1);
nr_detalhe_proc_cornea_w	number(1);
ie_vincular_laudos_aih_w	varchar2(1)	:= 'N';
nr_atendimento_w		conta_paciente.nr_atendimento%type;
nr_seq_interno_w		sus_laudo_paciente.nr_seq_interno%type;
dt_emissao_w		sus_laudo_paciente.dt_emissao%type;

Cursor C01 is
select	a.ie_complexidade,
	sus_obter_seq_ordem(b.nr_sequencia)
from	sus_procedimento a,
	procedimento_paciente b
where	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	b.nr_interno_conta	= nr_interno_conta_p
and	(((nr_detalhe_proc_w = 1) and (sus_obter_tiporeg_proc(b.cd_procedimento,b.ie_origem_proced,'C',2) in (3,4))) or
	 ((nr_detalhe_proc_w = 0) and (sus_obter_tiporeg_proc(b.cd_procedimento,b.ie_origem_proced,'C',2) = 3)) or
	 (nr_detalhe_proc_cornea_w = 1))
order by 2;

begin

select 	nvl(max(nr_atendimento),0)
into	nr_atendimento_w
from 	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

begin
select	max(cd_procedimento_real)
into	cd_procedimento_real_w
from	sus_aih_unif
where	nr_interno_conta	= nr_interno_conta_p;
exception
when others then
	cd_procedimento_real_w := null;
end;

ie_vincular_laudos_aih_w 	:= nvl(obter_valor_param_usuario(1123,180,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento),'N');

if ((ie_vincular_laudos_aih_w = 'S') and 
   (cd_procedimento_real_w is null)) then
	begin
	select	nvl(max(x.nr_seq_interno),0)
	into	nr_seq_interno_w
	from	sus_laudo_paciente x
	where	x.nr_interno_conta	= nr_interno_conta_p
	and	x.nr_atendimento	= nr_atendimento_w
	and	x.ie_classificacao 	= 1
	and	x.ie_tipo_laudo_sus 	= 1;
	
	if	(nr_seq_interno_w = 0) then
		begin
		select	nvl(max(x.nr_seq_interno),0)
		into	nr_seq_interno_w
		from	sus_laudo_paciente x
		where	x.nr_interno_conta	= nr_interno_conta_p
		and	x.nr_atendimento	= nr_atendimento_w
		and	x.ie_classificacao 	= 1
		and	x.ie_tipo_laudo_sus 	= 0;
		end;
	end if;
	
	if	(nr_seq_interno_w = 0) then
		begin
		select	nvl(max(x.nr_seq_interno),0)
		into	nr_seq_interno_w
		from	sus_laudo_paciente x
		where	x.nr_atendimento	= nr_atendimento_w
		and	x.ie_classificacao 	= 1
		and	x.ie_tipo_laudo_sus 	= 0;
		end;
	end if;	
	
	if	(nvl(nr_seq_interno_w,0) > 0) then
		begin
		
		begin
		select  cd_procedimento_solic,
				dt_emissao
		into	cd_procedimento_real_w,
				dt_emissao_w
		from	sus_laudo_paciente
		where	nr_seq_interno = nr_seq_interno_w;		
		exception
		when others then
			cd_procedimento_real_w := null;
		end;
		end;
	end if;		
	
	end;	
end if;	

if	(cd_procedimento_real_w is not null) then
	begin
	if	(sus_validar_regra(11,cd_procedimento_real_w,7,dt_emissao_w) = 0) then
		begin
		select	ie_complexidade
		into	ie_alta_complexidade_w
		from	sus_procedimento
		where	cd_procedimento	= cd_procedimento_real_w
		and	ie_origem_proced	= 7
		and	rownum = 1;		
		exception
		when others then
			ie_alta_complexidade_w := '';
		end;
	else
		begin
		
		nr_detalhe_proc_w:= nvl(sus_obter_se_detalhe_proc(cd_procedimento_real_w,7, '10056', sysdate),0);
		nr_detalhe_proc_cornea_w:= nvl(sus_obter_se_detalhe_proc(cd_procedimento_real_w,7, '10068', sysdate),0);
		
		open C01;
		loop
		fetch C01 into	
			ie_alta_complexidade_w,
			ie_ordem_w;
		exit when C01%notfound;
			begin
			exit;
			end;
		end loop;
		close C01;	
		end;	
	end if;
	end;
end if;

return	ie_alta_complexidade_w;

end sus_obter_complexidade_aih;
/
