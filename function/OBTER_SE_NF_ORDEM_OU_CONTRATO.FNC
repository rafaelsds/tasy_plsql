create or replace
function obter_se_nf_ordem_ou_contrato(nr_sequencia_p			number)
return varchar2 is

/*Returno
N	Nota Normal
C	Nota de Contrato
O	Nota de Ordem de compra
*/			
			
qt_existe_w		number(10);	
ie_tipo_w			varchar2(1) := 'N';			
			
begin

select	count(*)
into	qt_existe_w
from	nota_fiscal_item
where	nr_ordem_compra is not null
and	nr_sequencia = nr_sequencia_p;

if	(qt_existe_w > 0) then
	ie_tipo_w := 'O';
else
	select	count(*)
	into	qt_existe_w
	from	nota_fiscal_item
	where	nr_contrato is not null
	and	nr_sequencia = nr_sequencia_p;
	
	if	(qt_existe_w > 0) then
		ie_tipo_w := 'C';
	end if;
end if;

return	ie_tipo_w;

end obter_se_nf_ordem_ou_contrato;
/