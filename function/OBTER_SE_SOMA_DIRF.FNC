create or replace
function obter_se_soma_dirf	(nr_titulo_p		number)
 		    	return varchar2 is

ie_tipo_titulo_w	varchar2(2);
retorno_w		varchar2(1);
qt_registros_w		number(10);

begin

select	ie_tipo_titulo
into	ie_tipo_titulo_w
from	titulo_pagar
where	nr_titulo = nr_titulo_p;

select	count(*)
into	qt_registros_w 
from	dirf_regra_tipo_tit r
where	r.ie_tipo_titulo = ie_tipo_titulo_w
and	ie_somar_imposto = 'S';

if (qt_registros_w > 0) then
	retorno_w := 'S';
else
	retorno_w := 'N';
end if;

return retorno_w;

end obter_se_soma_dirf;
/