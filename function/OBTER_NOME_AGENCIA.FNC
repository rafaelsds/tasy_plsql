create or replace
function Obter_Nome_Agencia(
			cd_banco_p		number,
			cd_agencia_p		Varchar2)
			return varchar2 is

ds_agencia_w		varchar2(254);

begin

if	(cd_agencia_p is not null) and
	(cd_banco_p is not null) then
	select	ds_agencia_bancaria
	into	ds_agencia_w
	from	agencia_bancaria
	where	cd_agencia_bancaria	= cd_agencia_p
	and	cd_banco		= cd_banco_p;
end if;

return	ds_agencia_w;

end Obter_Nome_Agencia;
/