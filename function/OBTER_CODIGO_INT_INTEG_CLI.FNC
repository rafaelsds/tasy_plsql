create or replace
function obter_codigo_int_integ_cli
			(	nm_atributo_p		varchar2,
				ie_tipo_pessoa_p	varchar2,
				cd_externo_p		varchar2)
				return	varchar2 is

cd_retorno_w		varchar2(30);
qt_existe_w		number(10);

begin
select	count(1)
into	qt_existe_w
from	w_importa_cliente_depara
where	((ie_tipo_pessoa = ie_tipo_pessoa_p) or (ie_tipo_pessoa = 'T'))
and	nm_atributo = nm_atributo_p
and	cd_externo = cd_externo_p;

if	(qt_existe_w > 0) then
	select	max(cd_interno)
	into	cd_retorno_w
	from	w_importa_cliente_depara
	where	((ie_tipo_pessoa = ie_tipo_pessoa_p) or (ie_tipo_pessoa = 'T'))
	and	nm_atributo = nm_atributo_p
	and	cd_externo = cd_externo_p;

else
	cd_retorno_w	:= cd_externo_p;
end if;

return cd_retorno_w;

end obter_codigo_int_integ_cli;
/