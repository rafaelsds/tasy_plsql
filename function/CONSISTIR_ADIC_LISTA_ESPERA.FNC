create or replace
function consistir_adic_lista_espera(
		cd_pessoa_fisica_p	varchar2,
		dt_desejada_p	date)
 	return varchar2 is

ie_registro_lista_w	varchar2(1);
ds_msg_w		varchar2(255);

begin

select	decode(count(*), 0, 'N', 'S')
into	ie_registro_lista_w
from	agenda_lista_espera
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	dt_desejada	= dt_desejada_p
and	ie_status_espera	= 'A';

if	(ie_registro_lista_w = 'S') then
	ds_msg_w	:= substr(obter_texto_tasy(49713, wheb_usuario_pck.get_nr_seq_idioma),1,255);
end if;

return	ds_msg_w;
end consistir_adic_lista_espera;
/