create or replace
function 	gpt_get_stages_balance	( nr_seq_mat_cpoe_p	number)
			return number is

qt_etapas_saldo_w		number;
nr_etapas_w				number;
nr_prescricao_w			number;
nr_seq_solucao_w		number;
nr_etapas_suspensas_w	number;

begin
	
	select	max(a.nr_prescricao),
			max(a.nr_sequencia_solucao),
			max(b.nr_etapas),
			max(b.nr_etapas_suspensa)
	into	nr_prescricao_w,
			nr_seq_solucao_w,
			nr_etapas_w,
			nr_etapas_suspensas_w
	from 	prescr_material a,
			prescr_solucao b
	where	a.nr_seq_mat_cpoe		= nr_seq_mat_cpoe_p
	and		a.nr_prescricao			= b.nr_prescricao
	and		a.nr_sequencia_solucao	= b.nr_seq_solucao
	and		a.nr_prescricao 		= obter_prescr_item_cpoe(nr_seq_mat_cpoe, 'SOL');
						
	return nvl(nr_etapas_w,0) - (obter_etapas_adep_sol(1,nr_prescricao_w, nr_seq_solucao_w) + nvl(nr_etapas_suspensas_w,0));
	
end gpt_get_stages_balance;
/