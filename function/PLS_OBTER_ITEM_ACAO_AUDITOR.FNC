create or replace
function pls_obter_item_acao_auditor	
			(	nr_seq_item_analise_p	number,
				nm_usuario_p		varchar2,
				nr_seq_grupo_atual_p	number,
				nr_seq_analise_p	number		)
				return varchar2 is
				
nr_seq_conta_proc_mat_w			Number(10);
ds_retorno_w				varchar2(1)	:= 'N';
ds_retorno_ww				varchar2(1);
ds_tipo_despesa_w			varchar2(3);
nr_seq_conta_proc_w			Number(10);
nr_seq_conta_mat_w			Number(10);
cd_ocorrencia_w				varchar2(15);
nr_seq_conta_w				Number(10);

/*Obter ocorrencias do item*/
Cursor C01 is		
	select	cd_codigo	
	from	pls_analise_conta_item
	where	((nr_seq_conta = nr_seq_conta_w and ((nvl(nr_seq_conta_proc,0) = 0) and (nvl(nr_seq_conta_mat,0) = 0)))
	or	 (nr_seq_conta = nr_seq_conta_w and ((nr_seq_conta_proc = nr_seq_conta_proc_w) or (nr_seq_conta_mat = nr_seq_conta_mat_w))))
	and	ie_tipo = 'O'
	and	ie_status in ('P', 'E')
	and	nr_seq_analise = nr_seq_analise_p;

begin
/*
select	nr_seq_item,
	nr_seq_conta,
	ds_tipo_despesa
into	nr_seq_conta_proc_mat_w,
	nr_seq_conta_w,
	ds_tipo_despesa_w
from	w_pls_resumo_conta
where	nr_sequencia = nr_seq_item_analise_p;

if	(ds_tipo_despesa_w in ('P1', 'P2', 'P3', 'P4')) then --Procedimento
	nr_seq_conta_proc_w	:=	nr_seq_conta_proc_mat_w;
	nr_seq_conta_mat_w 	:= 0;
	
	open C01;
	loop
	fetch C01 into	
		cd_ocorrencia_w;
	exit when C01%notfound;
		begin	
		
		ds_retorno_w := pls_obter_dados_auditor(cd_ocorrencia_w, null, nm_usuario_p, 
							'C', nr_seq_grupo_atual_p) ;
							
		if	(ds_retorno_w = 'S') then
			ds_retorno_ww := 'S';
			goto final;
		end if;
							
		end;
	end loop;
	close C01;

elsif	(ds_tipo_despesa_w in ('M1', 'M2', 'M3', 'M4')) then --Material
	nr_seq_conta_mat_w	:=	nr_seq_conta_proc_mat_w;
	nr_seq_conta_proc_w	:=	0;
	
	open C01;
	loop
	fetch C01 into	
		cd_ocorrencia_w;
	exit when C01%notfound;
		begin
		
		ds_retorno_w := pls_obter_dados_auditor(cd_ocorrencia_w, null, nm_usuario_p, 
							'C', nr_seq_grupo_atual_p) ;
							
		if	(ds_retorno_w = 'S') then
			ds_retorno_ww := 'S';
			goto final;
		end if;
		
		end;
	end loop;
	close C01;
	
end if; 
*/
<<final>>
return ds_retorno_ww;

end pls_obter_item_acao_auditor;
/
