create or replace
function pls_obter_se_mat_prestador
			(	nr_seq_prestador_p		number,
				nr_seq_prest_fornec_p		number,
				nr_seq_material_p		number,
				dt_atendimento_p		date,
				nr_seq_prestador_exec_p		number,
				ie_internacao_p			varchar2,
				cd_estabelecimento_p		number,
				nr_seq_fornec_mat_p		number)
				return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se o material esta liberado para o prestador
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
	QUALQUER ATELRACAO FEITA NESTA ROTINA DEVERA SER FEITA NA
	PROCEDURE 'PLS_VALIDAR_LIB_MAT_PRESTADOR'
-------------------------------------------------------------------------------------------------------------------
Referencias:
	PLS_CONSISTIR_MAT_PRESTADOR
	PLS_GERAR_OCORRENCIA
	PLS_GERAR_OCORRENCIA_CONTA_WEB
	PLS_GERAR_W_ITENS_EXECUCAO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_atendimento_w		varchar2(3)	:= null;
ie_liberar_w			varchar2(2);
ie_retorno_w			varchar2(2)	:= 'N';
ie_grupo_material_w		varchar2(2)	:= 'S';
ie_estrut_mat_w			varchar2(1);
nr_seq_regra_w			number(10);
nr_seq_grupo_material_w		number(10);
nr_seq_estrut_mat_w		number(10);
nr_seq_regra_prest_ww		number(10);
nr_seq_estrut_regra_w		number(10);
nr_seq_tipo_prestador_w		number(10);
dt_atendimento_w		date;
nr_seq_material_w		pls_prestador_mat.nr_seq_material%type;

cursor C01 is
	select	a.nr_sequencia,
		a.ie_liberar,
		a.nr_seq_grupo_material,
		a.nr_seq_estrutura_mat,
		a.nr_seq_material
	from	pls_prestador_mat	a
	where	nvl(a.nr_seq_material, nr_seq_material_p)		= nr_seq_material_p
	and	a.nr_seq_prestador					= nr_seq_prestador_p
	and	nvl(a.nr_seq_prest_fornec, nvl(nr_seq_prest_fornec_p,0))	= nvl(nr_seq_prest_fornec_p, nvl(a.nr_seq_prest_fornec, 0))
	and	dt_atendimento_w between trunc(nvl(a.dt_inicio_vigencia, dt_atendimento_w),'dd') and fim_dia(nvl(a.dt_fim_vigencia, dt_atendimento_w))
	and	(a.nr_seq_tipo_prestador = nvl(nr_seq_tipo_prestador_w, a.nr_seq_tipo_prestador) 	or a.nr_seq_tipo_prestador is null)
	and	(a.ie_internado = nvl(ie_tipo_atendimento_w, a.ie_internado) or (a.ie_internado is null) or (a.ie_internado = 'N'))
	and	((exists	(select	1
				from	pls_material_fornec	x
				where	x.nr_seq_material	= nr_seq_material_p
				and	x.nr_seq_fornecedor	= a.nr_seq_fornec_mat)) or
		(a.nr_seq_fornec_mat is null))
	and	((a.nr_seq_fornec_mat = nvl(nr_seq_fornec_mat_p,a.nr_seq_fornec_mat)) or (a.nr_seq_fornec_mat is null))
	order by
		nvl(a.nr_seq_material, -1) desc,
		nvl(a.nr_seq_estrutura_mat, -1),
		nvl(a.nr_seq_grupo_material, 0),
		nvl(a.nr_seq_fornec_mat, 0),
		a.nr_sequencia;		

begin
if	(ie_internacao_p = 'S') then
	ie_tipo_atendimento_w	:= 'S'; /* Internado */
else
	ie_tipo_atendimento_w	:= 'A'; /* Ambulatorial */
end if;

dt_atendimento_w	:= nvl(dt_atendimento_p, sysdate);

begin
select	nr_seq_tipo_prestador
into	nr_seq_tipo_prestador_w
from	pls_prestador
where	nr_sequencia	= nr_seq_prestador_exec_p;
exception
when others then
	nr_seq_tipo_prestador_w	:= null;
end;

begin
select	a.nr_seq_estrut_mat
into	nr_seq_estrut_mat_w
from	pls_material	a
where	a.nr_sequencia	= nr_seq_material_p;
exception
when others then
	nr_seq_estrut_mat_w	:= 0;
end;

open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	ie_liberar_w,
	nr_seq_grupo_material_w,
	nr_seq_estrut_regra_w,
	nr_seq_material_w;
exit when C01%notfound;
	begin
	ie_grupo_material_w	:= 'S';
	ie_estrut_mat_w		:= 'S';
	
	if	(nr_seq_estrut_regra_w is not null) then
		if	(pls_obter_se_mat_estrutura(nr_seq_material_p, nr_seq_estrut_regra_w) = 'N') then
			ie_estrut_mat_w	:= 'N';
		end if;
	end if;
	
	if	(ie_estrut_mat_w = 'S') then
		
		/* Grupo de material */
		if	(nvl(nr_seq_grupo_material_w,0) > 0) then
			ie_grupo_material_w	:= pls_se_grupo_preco_material(nr_seq_grupo_material_w, nr_seq_material_p);
		end if;	
		
		if	(ie_grupo_material_w = 'N') then
			ie_retorno_w	:= 'N';
		elsif	(nr_seq_material_w is not null) and (ie_liberar_w = 'N') then
			ie_retorno_w	:= 'N';
			exit;
		elsif	(ie_liberar_w = 'S') then
			ie_retorno_w	:= 'S';
			exit;
		end if;
	end if;
	end;
end loop;
close C01;

return ie_retorno_w;

end pls_obter_se_mat_prestador;
/
