create or replace
function obter_obj_plano_educ(nr_seq_prescr_proc_p	varchar2)
			      return varchar2 is
qt_plano_educacional_w	varchar2(10);	
ie_objetivo_atingido_w	varchar2(10);

begin

select	count(*)
into	qt_plano_educacional_w 
from 	pe_prescr_proc_educ
where	nr_seq_prescr_proc = nr_seq_prescr_proc_p;	

if	(nvl(qt_plano_educacional_w,0) = 0) then
	ie_objetivo_atingido_w := 'X';
else 
	select 	max(ie_objetivo_atingido)
	into 	ie_objetivo_atingido_w
	from 	pe_prescr_proc_educ
	where 	nr_sequencia = (select 	max(nr_sequencia) 
				from 	pe_prescr_proc_educ  	
				where 	nr_seq_prescr_proc = nr_seq_prescr_proc_p);
end if;

return ie_objetivo_atingido_w;

end obter_obj_plano_educ;
/