create or replace
FUNCTION 	obter_oc_material(
		cd_material_p	Number,
		cd_estabelecimento_p number,
		dt_inicial_p	date,
		dt_final_p	date)

	RETURN number IS



ds_retorno_w		number(10);



BEGIN



if	( cd_material_p is not null ) then

	select  max(a.nr_ordem_compra)
	into 	ds_retorno_w
	from    ordem_compra a,
		ordem_compra_item b
	where   a.nr_ordem_compra = b.nr_ordem_compra
	and     b.cd_material = cd_material_p
	and	a.nr_seq_motivo_cancel is null
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	dt_ordem_compra between dt_inicial_p and fim_dia(dt_final_p);



end if;



RETURN ds_retorno_w;



END obter_oc_material;
/
