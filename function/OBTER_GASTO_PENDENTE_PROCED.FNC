create or replace
function obter_gasto_pendente_proced(	nr_atendimento_p	Number,
					ie_filtra_dt_exec_p	varchar,
					dt_alta_p		date)
						return 		Varchar IS 

qt_prescr_w		Number(15,4);
ie_pendente_w		Varchar2(1)	:= 'N';

begin

select	count(*)
into	qt_prescr_w
from	prescr_procedimento_pend_v
where	nr_atendimento = nr_atendimento_p
and	nvl(dt_liberacao, dt_liberacao_medico) is not null
and	((ie_filtra_dt_exec_p = 'N') or ((ie_filtra_dt_exec_p = 'S') and (dt_prev_execucao < dt_alta_p)));

if	(qt_prescr_w <> 0) then
	ie_pendente_w	:= 'S';
end if;

return ie_pendente_w;

end obter_gasto_pendente_proced;
/