create or replace
function obter_proc_princ_autor(nr_atendimento_p	number)
 		    	return number is

cd_proc_principal_w	procedimento.cd_procedimento%type;			
			
begin
select	max(cd_procedimento_principal)
into	cd_proc_principal_w
from	autorizacao_convenio
where	nr_atendimento		= nr_atendimento_p
and	nr_sequencia		=	(select min(b.nr_sequencia)
					from  	autorizacao_convenio b
					where	b.nr_atendimento	= nr_atendimento_p
					and	b.ie_tipo_autorizacao	= '1'
					and	b.cd_procedimento_principal is not null);

return	cd_proc_principal_w;

end obter_proc_princ_autor;
/


