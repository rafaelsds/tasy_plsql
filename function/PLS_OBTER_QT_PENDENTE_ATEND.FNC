create or replace
function pls_obter_qt_pendente_atend
			(	nr_seq_atendimento_p		number)
				return number is

nr_retorno_w			number(5);
				
begin

select	count(*)
into	nr_retorno_w
from	pls_atendimento_evento
where	nr_seq_atendimento	= nr_seq_atendimento_p
and	ie_status	= 'P';

return	nr_retorno_w;

end pls_obter_qt_pendente_atend;
/
