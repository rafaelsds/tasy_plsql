create or replace
function pls_obter_vl_item_ptu
		(	nr_seq_item_ref_p		number,
			nr_seq_analise_p		number,	
			ie_opcao_p			varchar2	)
 		    	return number is

vl_retorno_w	number(15,2);			
			
begin

if	(ie_opcao_p = 'UA') then
	/*Unit�rio apresentado*/	
	select	sum(vl_unitario_apres)
	into	vl_retorno_w
	from	w_pls_resumo_conta
	where	nr_seq_item_ref = nr_seq_item_ref_p
	and	nr_seq_analise	= nr_seq_analise_p;

elsif	(ie_opcao_p = 'TA') then
	/*Total apresentado*/		
	select	sum(vl_total_apres)
	into	vl_retorno_w
	from	w_pls_resumo_conta
	where	nr_seq_item_ref = nr_seq_item_ref_p
	and	nr_seq_analise	= nr_seq_analise_p;

elsif	(ie_opcao_p = 'UC') then
	/*Unit�rio Calculado*/
	select	sum(dividir_sem_round(vl_calculado, qt_apresentado))
	into	vl_retorno_w
	from	w_pls_resumo_conta
	where	nr_seq_item_ref = nr_seq_item_ref_p
	and	nr_seq_analise	= nr_seq_analise_p;

elsif	(ie_opcao_p = 'TC') then
	/*Total calculado*/
	select	sum(vl_calculado)
	into	vl_retorno_w
	from	w_pls_resumo_conta
	where	nr_seq_item_ref = nr_seq_item_ref_p
	and	nr_seq_analise	= nr_seq_analise_p;

elsif	(ie_opcao_p = 'XI') then
	/*Taxa interc�mbio importada*/
	select	sum(vl_taxa_intercambio_imp)
	into	vl_retorno_w
	from	w_pls_resumo_conta
	where	nr_seq_item_ref = nr_seq_item_ref_p
	and	nr_seq_analise	= nr_seq_analise_p;

elsif	(ie_opcao_p = 'UL') then
	/*Unit�rio liberado*/
	select	sum(vl_unitario)
	into	vl_retorno_w
	from	w_pls_resumo_conta
	where	nr_seq_item_ref = nr_seq_item_ref_p
	and	nr_seq_analise	= nr_seq_analise_p;

elsif	(ie_opcao_p = 'TL') then
	/*Total liberado*/
	select	sum(vl_total)
	into	vl_retorno_w
	from	w_pls_resumo_conta
	where	nr_seq_item_ref = nr_seq_item_ref_p
	and	nr_seq_analise	= nr_seq_analise_p;

elsif	(ie_opcao_p = 'VX') then
	/*Valor taxa interc�mbio calculado*/
	select	sum(vl_taxa_intercambio)
	into	vl_retorno_w
	from	w_pls_resumo_conta
	where	nr_seq_item_ref = nr_seq_item_ref_p
	and	nr_seq_analise	= nr_seq_analise_p;
	
end if;

return	vl_retorno_w;

end pls_obter_vl_item_ptu;
/