create or replace
function obter_cor_item_executado(cd_perfil_p number)
 		    	return varchar2 is

ds_cor_fundo_w	varchar2(15);			
			
begin

select 	max(ds_cor_fundo)
into	ds_cor_fundo_w
from 	regra_cor_item_executado
where 	cd_perfil = cd_perfil_p;

return	ds_cor_fundo_w;

end obter_cor_item_executado;
/