create or replace 
function	OBTER_CONTA_OUTRO_RETORNOS(	nr_seq_retorno_p	number,
						nr_interno_conta_p	number,
						cd_autorizacao_p	varchar2) return number is

ie_retorno_w	number(10);

begin

select	count(*) 
into	ie_retorno_w
from(	select   1 
	from	convenio_retorno b, 
		convenio_retorno_item a 
	where	a.nr_interno_conta   = nr_interno_conta_p 
	--and	a.cd_autorizacao     = cd_autorizacao_p 
	and	a.nr_seq_retorno     = b.nr_sequencia 
	and	b.nr_sequencia       <> nr_seq_retorno_p 
	union 
	select   1 
	from    lote_audit_hist_guia a, 
		lote_audit_hist b 
	where   a.nr_seq_lote_hist  = b.nr_sequencia 
	and     b.dt_fechamento      is null 
	and     a.nr_interno_conta   = nr_interno_conta_p 
	and     a.cd_autorizacao     = cd_autorizacao_p);

return 	ie_retorno_w;

end	OBTER_CONTA_OUTRO_RETORNOS;	
/
