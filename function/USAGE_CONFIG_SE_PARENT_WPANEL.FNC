create or replace function USAGE_CONFIG_SE_PARENT_WPANEL(
                        nr_sequencia_p number)
                        return char
is

ds_retorno_w            char;

begin

        select  decode(max(nr_sequencia),null,'N','S')
        into    ds_retorno_w
        from    objeto_schematic
        where   ie_tipo_componente = 'WPANEL'
        CONNECT BY nocycle prior nr_seq_obj_sup = nr_sequencia
        START WITH nr_sequencia = nr_sequencia_p;

        return  ds_retorno_w;

end USAGE_CONFIG_SE_PARENT_WPANEL;
/