create or replace 
function bl_obter_vol_atendido(nr_seq_nut_prod_p	Number)
		return Number is
			
qt_atendida_w		Number(15);
ie_leite_materno_w	Varchar2(1);
nr_prescricao_w		Number(14);
nr_seq_prod_lac_w	Number(10);
dt_horario_w		Date;

begin
if	(nr_seq_nut_prod_p is not null) then
	
	begin
	select	'S'
	into	ie_leite_materno_w
	from	nut_prod_lac_item a,
		nutricao_leite_deriv b,
		classif_leite_deriv c
	where	b.cd_material = a.cd_material
	and	b.nr_seq_classif = c.nr_sequencia
	and 	(b.ie_situacao = 'A' or b.ie_situacao is null)
	and	a.nr_seq_prod_princ = nr_seq_nut_prod_p
	and	c.ie_leite_materno = 'S'
	and	rownum = 1;
	exception
	WHEN value_error THEN
		ie_leite_materno_w := 'N';
	end;
	
	if	(ie_leite_materno_w = 'S') then
		select	nvl(sum(qt_volume),0)
		into	qt_atendida_w
		from	nut_prod_lac_item a,
			bl_frasco b
		where	a.nr_sequencia = b.nr_seq_prod_item
		and  	a.nr_seq_prod_princ = nr_seq_nut_prod_p;
	else
		select	nr_prescricao,
			nr_sequencia,
			dt_horario
		into	nr_prescricao_w,
			nr_seq_prod_lac_w,
			dt_horario_w
		from	nut_prod_lac_item
		where	nr_seq_prod_princ = nr_seq_nut_prod_p;
		
		select	nvl(sum(b.qt_volume),0)
		into	qt_atendida_w
		from	nut_prod_lac_item a,
			bl_frasco b
		where	a.nr_sequencia 	= b.nr_seq_prod_item
		and	a.nr_prescricao	= nr_prescricao_w
		and	a.nr_sequencia = nr_seq_prod_lac_w
		and	a.dt_horario	= dt_horario_w;
	end if;
end if;

return	qt_atendida_w;

end bl_obter_vol_atendido;
/