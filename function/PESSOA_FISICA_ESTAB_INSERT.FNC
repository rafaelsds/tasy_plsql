create or replace trigger pessoa_fisica_estab_insert
before insert on pessoa_fisica_estab
for each row
declare

qt_count_w	number(10);

begin

select	count(*)
into	qt_count_w
from	pessoa_fisica_estab
where	cd_estabelecimento = :NEW.cd_estabelecimento
and	cd_pessoa_fisica = :NEW.cd_pessoa_fisica;

if (qt_count_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(950038);
end if;
	
end;
/