create or replace
function Obter_Desc_ordem_medica(nr_seq_tipo_ordem_p	number)
 		    	return varchar2 is

ds_ordem_w	varchar2(255);

begin

select	max(ds_ordem)
into	ds_ordem_w
from	ordem_medica
where	nr_sequencia	= nr_seq_tipo_ordem_p;

return	ds_ordem_w;

end Obter_Desc_ordem_medica;
/
