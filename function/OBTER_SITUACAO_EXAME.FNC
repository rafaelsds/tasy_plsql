create or replace function obter_situacao_exame (	CD_ESTABELECIMENTO_P NUMBER,    
								IE_STATUS_EXECUCAO_P NUMBER,
								NR_PRESCRICAO_P  NUMBER,
								NR_SEQ_PRESCRICAO_P NUMBER,
								NR_SEQ_SITUACAO_PROC_P NUMBER,
								IE_EXAME_TRAZIDO_P VARCHAR2,
								IE_EXAME_ANTERIOR_P VARCHAR2,
								IE_EXAME_IMPORTANTE_P VARCHAR2,
								IE_EXAME_IMPORTADO_P VARCHAR2,
								IE_EXAME_COMPLEMENTAR_P VARCHAR2,
								CD_ESPECIALIDADE_P NUMBER)
 		    	return NUMBER is
				
nr_sequencia_w		number(10) := 0;

begin

select	se.nr_sequencia
into	nr_sequencia_w
from	situacao_exame se
inner join regra_situacao_exame rse on (se.NR_SEQUENCIA = rse.NR_SEQ_SITUACAO_EXAME)
where	rse.CD_ESTABELECIMENTO = CD_ESTABELECIMENTO_P
and     ((rse.IE_STATUS_EXECUCAO = IE_STATUS_EXECUCAO_P) or (rse.IE_STATUS_EXECUCAO is null and IE_STATUS_EXECUCAO_P is null))
and     ((rse.IE_LISTA_CENTRAL_MEDICA = 'S' and (SELECT count(1) FROM lista_central_exame lce INNER JOIN lista_central_medico lcm ON lce.nr_seq_lista_medico = lcm.nr_sequencia WHERE lce.nr_prescricao = NR_PRESCRICAO_P AND lce.nr_sequencia_prescricao = NR_SEQ_PRESCRICAO_P) > 0) 
		or (rse.IE_LISTA_CENTRAL_MEDICA = 'N' and (SELECT count(1) FROM lista_central_exame lce INNER JOIN lista_central_medico lcm ON lce.nr_seq_lista_medico = lcm.nr_sequencia WHERE lce.nr_prescricao = NR_PRESCRICAO_P AND lce.nr_sequencia_prescricao = NR_SEQ_PRESCRICAO_P) = 0))
and     ((rse.NR_SEQ_SITUACAO_PROC = NR_SEQ_SITUACAO_PROC_P) or (rse.NR_SEQ_SITUACAO_PROC is null and NR_SEQ_SITUACAO_PROC_P is null))
and     ((rse.IE_EXAME_TRAZIDO = IE_EXAME_TRAZIDO_P) or (rse.IE_EXAME_TRAZIDO is null))
and     ((rse.IE_EXAME_ANTERIOR = IE_EXAME_ANTERIOR_P) or (rse.IE_EXAME_ANTERIOR is null))
and     ((rse.IE_EXAME_IMPORTANTE = IE_EXAME_IMPORTANTE_P) or (rse.IE_EXAME_IMPORTANTE is null))
and     ((rse.IE_EXAME_IMPORTADO = IE_EXAME_IMPORTADO_P) or (rse.IE_EXAME_IMPORTADO is null))
and     ((rse.IE_EXAME_COMPLEMENTAR = IE_EXAME_COMPLEMENTAR_P) or (rse.IE_EXAME_COMPLEMENTAR is null))
and     ((rse.CD_MEDICO_PREFERENCIAL is null) or (rse.CD_MEDICO_PREFERENCIAL in (select CD_PESSOA_FISICA from prescr_proc_laudo_pref where nr_prescricao = NR_PRESCRICAO_P and nr_seq_prescricao = NR_SEQ_PRESCRICAO_P)))
and     ((rse.CD_ESPECIALIDADE = CD_ESPECIALIDADE_P) or (rse.CD_ESPECIALIDADE is null and CD_ESPECIALIDADE_P is null));


return	nr_sequencia_w;

end obter_situacao_exame;
/
