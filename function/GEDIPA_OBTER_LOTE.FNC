create or replace 
function  gedipa_obter_lote(nr_seq_processo_p		number,
							cd_material_p			number)
 		    	return varchar2 is

nr_seq_lote_fornec_w	varchar2(255);
begin

select max(nr_seq_lote_fornec)
into   nr_seq_lote_fornec_w
from   adep_processo_item
where  NR_SEQ_processo 	= nr_seq_processo_p
and    cd_material 		= cd_material_p;


return	nr_seq_lote_fornec_w;

end gedipa_obter_lote;
/