create or replace
function Obter_ult_horario_sol (nr_atendimento_p	number)
 		    	return varchar2 is

ds_horario_w		varchar2(50);
nr_prescricao_w		number(15);
nr_seq_solucao_w	number(15);

begin

select	max(a.nr_prescricao)
into	nr_prescricao_w
from	prescr_solucao a,
	prescr_medica b
where	a.nr_prescricao		= b.nr_prescricao
and	b.nr_atendimento	= nr_atendimento_p
and	b.dt_inicio_prescr 	between sysdate - 15 and sysdate + 1
and	a.ds_horarios 		is not null;

if	(nr_prescricao_w is not null) then
	select	max(a.nr_seq_solucao)
	into	nr_seq_solucao_w
	from	prescr_solucao a,
		prescr_medica b
	where	a.nr_prescricao		= b.nr_prescricao
	and	a.nr_prescricao		= nr_prescricao_w
	and	a.ds_horarios 		is not null;

	select	trim(max(obter_ultimo_horario_solucao(nr_prescricao_w,nr_seq_solucao_w)))
	into	ds_horario_w
	from	dual;
	
end if;

if	(ds_horario_w is not null) then
	if	(length(ds_horario_w) = 2) then
		ds_horario_w	:= ds_horario_w || ':00';
	end if;
end if;

return	ds_horario_w;

end Obter_ult_horario_sol;
/