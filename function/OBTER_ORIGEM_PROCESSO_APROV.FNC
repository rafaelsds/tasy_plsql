create or replace
function obter_origem_processo_aprov(nr_seq_aprovacao_p	number)
 		    	return varchar2 is
				
/* Retorno
O - Ordem compra
S - Solicitacao compra
R - Requisicao materiais
C - Cotacao de compra 
N - Nota fiscal
L - Registro licitacao
G - Contrato*/

ie_ordem_w		varchar2(1);
ie_solicitacao_w	varchar2(1);
ie_requisicao_w		varchar2(1);
vl_retorno_w		varchar2(1);
ie_cotacao_w		varchar2(1);
ie_nota_fiscal_w	varchar2(1);
ie_reg_licitacao_w	varchar2(1);
ie_contrato_w  varchar(1);

begin
select	nvl(max('S'),'N')
into	ie_ordem_w
from	ordem_compra_item a,
	processo_aprov_compra b
where	a.nr_seq_aprovacao = b.nr_sequencia
and	b.nr_sequencia = nr_seq_aprovacao_p;

if	(ie_ordem_w = 'S') then
	vl_retorno_w := 'O';
else
	
	begin
	select	nvl(max('S'),'N')
	into	ie_solicitacao_w
	from	solic_compra_item a,
		processo_aprov_compra b
	where	a.nr_seq_aprovacao = b.nr_sequencia
	and	b.nr_sequencia = nr_seq_aprovacao_p;
	if	(ie_solicitacao_w = 'S') then
		vl_retorno_w := 'S';
	else
		begin
		select	nvl(max('S'),'N')
		into	ie_requisicao_w
		from	item_requisicao_material a,
			processo_aprov_compra b
		where	a.nr_seq_aprovacao = b.nr_sequencia
		and	b.nr_sequencia = nr_seq_aprovacao_p;
		if	(ie_requisicao_w = 'S') then
			vl_retorno_w := 'R';
		else
			begin
			select	nvl(max('S'),'N')
			into	ie_cotacao_w
			from	cot_compra_item a,
				processo_aprov_compra b
			where	a.nr_seq_aprovacao = b.nr_sequencia
			and	b.nr_sequencia = nr_seq_aprovacao_p;
			if	(ie_cotacao_w = 'S') then
				vl_retorno_w := 'C';
			else
				begin
				select	nvl(max('S'),'N')
				into	ie_nota_fiscal_w
				from	nota_fiscal_item a,
					processo_aprov_compra b
				where	a.nr_seq_aprovacao = b.nr_sequencia
				and	b.nr_sequencia = nr_seq_aprovacao_p;
				if	(ie_nota_fiscal_w = 'S') then
					vl_retorno_w := 'N';
				else
					begin
					select	nvl(max('S'),'N')
					into	ie_reg_licitacao_w
					from	reg_lic_item a,
						processo_aprov_compra b
					where	a.nr_seq_aprovacao = b.nr_sequencia
					and	b.nr_sequencia = nr_seq_aprovacao_p;
					if	(ie_reg_licitacao_w = 'S') then
						vl_retorno_w := 'L';
					else
						begin
						select	nvl(max('S'),'N')
						into	ie_contrato_w
						from	contrato a,
						  processo_aprov_compra b
						where	a.nr_seq_aprovacao = b.nr_sequencia
						and	b.nr_sequencia = nr_seq_aprovacao_p;
						if	(ie_contrato_w = 'S') then
						  vl_retorno_w := 'G';
						end if;
						end;
					end if;	
					end;
				end if;					
				end;
			end if;	
			end;
		end if;
		end;
	end if;
	end;
end if;

return	vl_retorno_w;

end obter_origem_processo_aprov;
/
