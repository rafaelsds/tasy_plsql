create or replace
function dados_parametro_integ_pacs (		nm_campo_p		varchar2,
						cd_estabelecimento_p	number)
 		    	return varchar2 is
ds_retorno_w		varchar2(255);

begin

select obter_select_concatenado_bv(
	' select max(' || nm_campo_p  || ') 
	  from    parametro_integracao_pacs
	  where   cd_estabelecimento = :cd_estabelecimento ',
	  'cd_estabelecimento=' || cd_estabelecimento_p, 
	  '')
into	ds_retorno_w
from	dual;

	 
return	ds_retorno_w;

end dados_parametro_integ_pacs;
/