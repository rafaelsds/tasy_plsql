CREATE OR REPLACE
FUNCTION Obter_Quantidade_Executada(dt_inicial_p		DATE,
					 dt_final_p			DATE,
					 cd_material_p		NUMBER)

 		    	RETURN NUMBER IS

qt_executada_w	NUMBER(9,3) := 0;

BEGIN
	IF	(cd_material_p IS NOT NULL) THEN
	SELECT SUM(nvl(qt_executada,0))
	INTO   qt_executada_w
	FROM   material_atend_paciente
	WHERE  cd_material	= cd_material_p
	AND	   dt_atendimento BETWEEN NVL(dt_inicial_p,SYSDATE) AND fim_dia(NVL(dt_final_p,SYSDATE))
	AND	   nr_seq_ordem_prod IS NOT NULL;
	END IF;


RETURN	qt_executada_w;

END Obter_Quantidade_Executada;
/