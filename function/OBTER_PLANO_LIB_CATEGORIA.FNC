create or replace
function Obter_Plano_Lib_Categoria
		(cd_convenio_p		number,
		cd_categoria_p		varchar2,
		cd_plano_p		varchar2,
		cd_estabelecimento_p	number,
		dt_atendimento_p	date)
		return varchar2 is


ie_liberado_w	varchar2(01)	:= 'S';
qt_regra_w	number(10,0);
dt_atendimento_w	date;

begin

select	count(*)
into	qt_regra_w
from	categoria_plano
where	cd_convenio	= cd_convenio_p
and	cd_categoria	= cd_categoria_p
and	ie_situacao	= 'A';

dt_atendimento_w	:= trunc(dt_atendimento_p);
if	(qt_regra_w > 0) then
	
	select	nvl(max('S'), 'N')
	into	ie_liberado_w
	from	categoria_plano
	where	cd_convenio	= cd_convenio_p
	and	cd_categoria	= cd_categoria_p
	and	cd_plano	= cd_plano_p
	and 	nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
	and	ie_situacao	= 'A'
	and	dt_atendimento_w between nvl(dt_inicio_vigencia,dt_atendimento_w) -1 and fim_dia(nvl(DT_FINAL_VIGENCIA,dt_atendimento_w));
end if;



return	ie_liberado_w;

end Obter_Plano_Lib_Categoria;
/
