create or replace
function obter_tamanho_grupo_rotina (nr_seq_grupo_p    number)
                return number is

qt_tamanho_w      number(10,0);

begin
if      (nr_seq_grupo_p is not null) then
        begin
        select  max(qt_tamanho_grupo)
        into    qt_tamanho_w
        from    rep_rotina_grupo
        where   nr_sequencia = nr_seq_grupo_p;
        end;
end if;
return qt_tamanho_w;
end obter_tamanho_grupo_rotina;
/
