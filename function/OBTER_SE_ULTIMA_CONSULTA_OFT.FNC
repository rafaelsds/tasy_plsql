create or replace
function obter_se_ultima_consulta_oft			(nr_seq_consulta_p	number,
							 nr_atendimento_p	number,
							 cd_pessoa_fisica_p     varchar2)
							return varchar2 is

ds_retorno_w varchar2 (1) := 'N';

begin

if (nr_seq_consulta_p is not null) and
   ((nr_atendimento_p is not null) and (nr_atendimento_p <> 0)) then
   
	select nvl(max('S'),'N')
	into   ds_retorno_w
	from   oft_consulta
	where  nr_sequencia = nr_seq_consulta_p
	and    dt_consulta = (select max(dt_consulta)
	   		      from	  oft_consulta
			      where    nr_atendimento = nr_atendimento_p
					and		dt_cancelamento is null);
			      
elsif  (nr_seq_consulta_p is not null) and
	(cd_pessoa_fisica_p is not null) then
	
	select nvl(max('S'),'N')
	into   ds_retorno_w
	from   oft_consulta
	where  nr_sequencia = nr_seq_consulta_p
	and    dt_consulta = (select max(dt_consulta)
	   		      from	  oft_consulta
			      where    cd_pessoa_fisica = cd_pessoa_fisica_p
					and		dt_cancelamento is null);
end if;

return	ds_retorno_w;

end obter_se_ultima_consulta_oft;
/