create or replace
function obter_motivo_atraso_cir(	nr_cirurgia_p		number,
					ie_opcao_p		varchar2)
					return varchar2 is
/*
ie_opcao_p
d - descri��o
c - c�digo
t - tempo
*/

ds_retorno_w	varchar2(100) := null;
nr_seq_motivo_w	number(10,0);
qt_tempo_w	number(5);

begin

select	nvl(max(nr_seq_motivo),0),
	nvl(max(qt_tempo),0)
into	nr_seq_motivo_w,
	qt_tempo_w
from	cir_paciente_atraso
where	nr_cirurgia		=	nr_cirurgia_p
and	dt_atualizacao_nrec	= 	(select	max(dt_atualizacao_nrec)
					 from	cir_paciente_atraso
					 where	nr_cirurgia = nr_cirurgia_p);

if	(ie_opcao_p = 'D') and (nr_seq_motivo_w > 0) then
	select	ds_motivo
	into	ds_retorno_w
	from	cir_motivo_atraso
	where	nr_sequencia	=	nr_seq_motivo_w;
elsif 	(ie_opcao_p = 'C') and (nr_seq_motivo_w > 0) then
	ds_retorno_w	:= nr_seq_motivo_w;
elsif	(ie_opcao_p = 'T') then
	ds_retorno_w	:= qt_tempo_w;
end if;

return ds_retorno_w;

end obter_motivo_atraso_cir;
/