Create or Replace
Function  Obter_qt_dia_pendente_compras(
			cd_estabelecimento_p	number)
		return number is
	
qt_dia_compra_pend_w	Number(5);

begin

select	nvl(max(qt_dia_compra_pend), 180)
into	qt_dia_compra_pend_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

return qt_dia_compra_pend_w;

end Obter_qt_dia_pendente_compras;
/