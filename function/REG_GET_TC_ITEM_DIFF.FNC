CREATE OR REPLACE FUNCTION reg_get_tc_item_diff (nr_seq_test_case_p	reg_caso_teste.nr_sequencia%TYPE)
RETURN VARCHAR2 IS

	CURSOR c_tc_item IS
	SELECT	h.nr_sequencia nr_history,
		i.nr_sequencia nr_item,
		MAX(NR_REVISION)
	FROM	reg_tc_item_history h
	FULL OUTER JOIN reg_acao_teste i ON i.nr_sequencia = h.nr_seq_tc_his
	WHERE	NVL(h.cd_versao, (SELECT MAX(ds_version) FROM reg_verion_scheduler WHERE ie_major = 'S')) >= (
					SELECT	MAX(ds_version)
					FROM	reg_verion_scheduler
					WHERE	ie_major = 'S')
	GROUP BY h.nr_sequencia, i.nr_sequencia;
	
	ie_different_w NUMBER(1);
BEGIN

	FOR	tc_item IN c_tc_item
	LOOP
		IF	(tc_item.nr_history IS NULL) OR (tc_item.nr_item IS NULL) THEN
			RETURN 'U';
		ELSE
			SELECT	MAX(1)
			INTO	ie_different_w
			FROM	reg_tc_item_history h,
				reg_acao_teste i
			WHERE	h.nr_sequencia = tc_item.nr_history
			AND	i.nr_sequencia = tc_item.nr_item
			AND	i.dt_atualizacao <> h.dt_atualizacao;
			
			IF	ie_different_w = 1 THEN
				RETURN 'U';
			END IF;
		END IF;
	END LOOP;
	
	RETURN 'N';
END reg_get_tc_item_diff;
/