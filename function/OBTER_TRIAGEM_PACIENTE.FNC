create or replace
function obter_triagem_paciente(
			nr_seq_fila_senha_p		number)
 		    	return varchar2 is

nr_seq_classif_w				triagem_pronto_atend.nr_seq_classif%type;
nr_seq_triagem_prioridade_w		triagem_pronto_atend.nr_seq_triagem_prioridade%type;
nr_seq_triagem_w		     	varchar2(15)	:=	'';
prioridade_tolife_w  			varchar2(10);
tem_regra_tolife_w   			varchar(2):= 'N';

begin

select	max(NR_SEQ_CLASSIF),
		max(NR_SEQ_TRIAGEM_PRIORIDADE)
into	nr_seq_classif_w,
		nr_seq_triagem_prioridade_w
from	TRIAGEM_PRONTO_ATEND
where	nr_seq_fila_senha	=	nr_seq_fila_senha_p;

if	(nr_seq_classif_w is null) then

	select	max(nr_seq_classif),
			max(nr_seq_triagem_prioridade)
	into	nr_seq_classif_w,
			nr_seq_triagem_prioridade_w
	from	triagem_pronto_atend
	where	nr_sequencia		=
		(select	max(nr_sequencia)
		from	triagem_pronto_atend
		where	nr_atendimento		=
			(select	max(nr_atendimento)
			from	atendimento_paciente
			where	nr_seq_pac_senha_fila	=	nr_seq_fila_senha_p));

end if;

-- Processo de classificação de risco executado no sistema da ToLife - OS 982686
if	(nr_seq_classif_w is null) then

	select	nvl(max('S'), 'N')
	into	tem_regra_tolife_w
	from	TO_LIFE_TRIAGEM_RISCO
	where   rownum = 1;
    
	if (tem_regra_tolife_w = 'S') then
	
		select	max(nr_seq_triagem)
		into	nr_seq_classif_w
		from	atendimento_paciente
		where	nr_seq_pac_senha_fila = nr_seq_fila_senha_p;
		
	end if;
	
end if;
-- Fim processo de classificação de risco executado no sistema da ToLife - OS 982686

if (nr_seq_triagem_prioridade_w > 0) then
	nr_seq_triagem_w := 'P' || nr_seq_triagem_prioridade_w;
elsif (nr_seq_classif_w > 0) then
	nr_seq_triagem_w := 'R' || nr_seq_classif_w;
end if;

return	nr_seq_triagem_w;

end obter_triagem_paciente;
/
