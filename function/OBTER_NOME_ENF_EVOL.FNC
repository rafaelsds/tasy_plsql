create or replace
function obter_nome_enf_evol(nr_atendimento_p	number,
								ie_evolucao_clinica_p	varchar2)
 		    	return varchar2 is

nm_enf_w	varchar2(255);

begin
select	min(substr(obter_nome_usuario(nm_usuario),1,255))
into	nm_enf_w
from	evolucao_paciente
where	nr_atendimento = nr_atendimento_p
and		ie_evolucao_clinica = ie_evolucao_clinica_p
and		dt_liberacao is not null
and		dt_inativacao is null;

return	nm_enf_w;

end obter_nome_enf_evol;
/