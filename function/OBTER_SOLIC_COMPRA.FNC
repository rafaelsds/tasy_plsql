create or replace
function obter_solic_compra(nr_lote_producao_p		number)
				return varchar2 is
nr_solic_compra_w	number(10);
ds_retorno_w		varchar2(4000);
Cursor C01 is
	select	nr_solic_compra
	from	solic_compra
	where 	nr_lote_producao = nr_lote_producao_p
	order by 1;				
begin
open C01;
loop
fetch C01 into	
	nr_solic_compra_w;
exit when C01%notfound;
	begin
	if	(ds_retorno_w is null) then 
		ds_retorno_w := nr_solic_compra_w;
	else 
		ds_retorno_w := ds_retorno_w||','||nr_solic_compra_w;
	end if;
	end;
end loop;
close C01;

return	ds_retorno_w;

end obter_solic_compra;
/