create or replace
function TISS_OBTER_REGRA_MAT_OPM (	cd_convenio_p		number,
					cd_estabelecimento_p	number,
					cd_material_p		number,
					cd_classe_material_p	number,
					cd_subgrupo_material_p	number,
					cd_grupo_material_p	number,
					ie_tipo_atendimento_p	number,
					vl_material_unitario_p	number) return varchar2 is

ie_opm_w	varchar2(10);

cursor c01 is
select	ie_opm
from	tiss_regra_mat_opm a
where	cd_convenio		= cd_convenio_p
and	cd_estabelecimento	= cd_estabelecimento_p
and	nvl(cd_material, nvl(cd_material_p,0))				= nvl(cd_material_p,0)
and	nvl(cd_classe_material, nvl(cd_classe_material_p, 0))		= nvl(cd_classe_material_p, 0)
and	nvl(cd_subgrupo_material, nvl(cd_subgrupo_material_p, 0))	= nvl(cd_subgrupo_material_p, 0)
and	nvl(cd_grupo_material, nvl(cd_grupo_material_p, 0))		= nvl(cd_grupo_material_p, 0)
and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_p, 0))		= nvl(ie_tipo_atendimento_p,0)
and	nvl(vl_material_unitario_p,0) between nvl(a.vl_material_min,0)	and	nvl(a.vl_material_max,999999999)
order by nvl(cd_material,0),
	nvl(cd_classe_material,0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0),
	nvl(ie_tipo_atendimento,0);

begin

open c01;
loop
fetch c01 into
	ie_opm_w;
exit when c01%notfound;
	ie_opm_w := ie_opm_w;
end loop;
close c01;

return	nvl(ie_opm_w, 'N');

end TISS_OBTER_REGRA_MAT_OPM;
/
