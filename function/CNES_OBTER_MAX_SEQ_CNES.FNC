create or replace
function cnes_obter_max_seq_cnes(nr_seq_ident_cnes_p	number)
 		    	return number is

nr_sequencia_w	number(10);

begin
select 	max(nr_sequencia)
into	nr_sequencia_w
from	compl_pf_tel_adic
where	nr_seq_ident_cnes = nr_seq_ident_cnes_p;

return	nr_sequencia_w;

end cnes_obter_max_seq_cnes;
/