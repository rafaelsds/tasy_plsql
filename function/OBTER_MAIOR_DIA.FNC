create or replace
function obter_maior_dia(	nr_sequencia_p	number)
							return varchar2 is

ds_retorno_w	varchar2(32000);
type campos is record (ds_ciclo	varchar2(6));
type vetor is table of campos index by binary_integer;
ciclos_w			vetor;
posicao_w			number(3);
cont_w				number(10);
z 			    	integer;
Maior_w         	Integer;
Maior_ds        	varchar2(100);
retorno_ds      	varchar2(4000);
nr_dias_setor_w	number(10);

cursor c01 is
	SELECT  DECODE(PM.DS_DIAS_APLICACAO,'','',PM.DS_DIAS_APLICACAO||',') ||
	DECODE(SOL.DS_DIAS_APLICACAO,'','',SOL.DS_DIAS_APLICACAO||',') || 
	DECODE(PP.DS_DIAS_APLICACAO,'','',PP.DS_DIAS_APLICACAO||',') ||
	DECODE(PR.DS_DIAS_APLICACAO,'','',PR.DS_DIAS_APLICACAO||',') 
	DS_DIAS_APLICACAO FROM PACIENTE_SETOR PS,
	PACIENTE_PROTOCOLO_MEDIC PM, 
	PACIENTE_PROTOCOLO_SOLUC SOL,
	PACIENTE_PROTOCOLO_PROC PP,
	PACIENTE_PROTOCOLO_REC PR
	WHERE PS.NR_SEQ_PACIENTE = nr_sequencia_p
	AND PS.NR_SEQ_PACIENTE = PM.NR_SEQ_PACIENTE(+)
	AND PS.NR_SEQ_PACIENTE = SOL.NR_SEQ_PACIENTE(+)
	AND PS.NR_SEQ_PACIENTE = PP.NR_SEQ_PACIENTE(+)
	AND PS.NR_SEQ_PACIENTE = PR.NR_SEQ_PACIENTE(+);
	
begin
	open c01;
	loop
	fetch c01 into
		retorno_ds;
	exit when c01%notfound;
	begin
		ds_retorno_w := ds_retorno_w || retorno_ds;
	end;
	end loop;
	close C01;	
	
	Maior_w := -999;
    ciclos_w.delete;
	z := 0;
	cont_w := 0;
	posicao_w	:= instr(ds_retorno_w,',');
	
	while	posicao_w > 0 loop
		begin
			posicao_w	:= instr(ds_retorno_w,',');
			z := z + 1;
			ciclos_w(z).ds_ciclo	:= substr(ds_retorno_w,1,posicao_w - 1);
			ds_retorno_w		    := substr(ds_retorno_w,posicao_w + 1,length(ds_retorno_w));
			
			if Maior_w  <  substr(ciclos_w(z).ds_ciclo,2,3) then 
			   Maior_w  :=   substr(ciclos_w(z).ds_ciclo,2,3) ; 	
			   Maior_ds :=   substr(ciclos_w(z).ds_ciclo,2,3);
			 end if;
			
			cont_w	:= cont_w + 1;
			if	(cont_w > 100) then
				exit;
			end if;
		end;
		end loop;
	if	(Maior_ds is null) or
		(Maior_ds = '') then
		
		select 	nvl(somente_numero(nr_ciclos),0)
		into	nr_dias_setor_w
		from	paciente_setor
		where	nr_seq_paciente = nr_sequencia_p;

		if	(nr_dias_setor_w > 0) then
			
			Maior_ds := to_char(nr_dias_setor_w);
		else
			Maior_ds := '99999';
		end if;
	end if;
	return 	Maior_ds;
end obter_maior_dia;
/
