create or replace
function obter_peso_conjunto(nr_sequencia_p	number)
 		    	return number is

qt_peso_total_w	        cm_item.qt_peso%type := 0;
qt_peso_w	        cm_item.qt_peso%type := 0;

Cursor C01 is
	select 	nr_seq_item
	from 	cm_item_cont
	where 	nr_seq_conjunto = nr_sequencia_p;	
			
begin


for co1_w in C01 loop
		
	select qt_peso
	into qt_peso_w
	from cm_item
	where nr_sequencia = co1_w.nr_seq_item;
	
	qt_peso_total_w := qt_peso_total_w + nvl(qt_peso_w,0);
	
end loop;

return	qt_peso_total_w;

end obter_peso_conjunto;
/