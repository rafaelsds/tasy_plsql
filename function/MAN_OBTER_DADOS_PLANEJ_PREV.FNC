create or replace
function man_obter_dados_planej_prev(
			nr_sequencia_p	number,
			ie_opcao_p	varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
ds_planejamento_w		varchar2(80);
nr_seq_frequencia_w	number(10,0);

/*	ie_opcao_p
	D - Descri��o
	F - Frequencia
	DF - Descri��o frequencia
	UP - �ltima preventiva gerada	*/

begin

if	(nvl(nr_sequencia_p,0) > 0) then
	begin
	select	ds_planejamento,
		nr_seq_frequencia
	into	ds_planejamento_w,
		nr_seq_frequencia_w
	from	man_planej_prev
	where	nr_sequencia = nr_sequencia_p;

	if	(ie_opcao_p = 'D') then
		ds_retorno_w	:= ds_planejamento_w;
	elsif	(ie_opcao_p = 'F') then
		ds_retorno_w	:= nr_seq_frequencia_w;
	elsif	(ie_opcao_p = 'DF') then
		select	ds_frequencia
		into	ds_retorno_w
		from	man_freq_planej
		where	nr_sequencia = nr_seq_frequencia_w;
	elsif	(ie_opcao_p = 'UP') then
		begin
		select	max(nr_seq_ordem_servico)
		into	ds_retorno_w
		from	man_planej_prev_log
		where	ie_status = 'G'
		and	nr_seq_planej = nr_sequencia_p;
		
		if	(nvl(ds_retorno_w,'0') <> '0') then
			select	to_char(dt_ordem_servico,'dd/mm/yyyy')
			into	ds_retorno_w
			from	man_ordem_servico
			where	nr_sequencia = ds_retorno_w;
		end if;
		end;
	end if;
	end;
end if;

return	ds_retorno_w;

end man_obter_dados_planej_prev;
/