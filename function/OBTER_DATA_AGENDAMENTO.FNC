create or replace
function obter_data_agendamento(	nr_seq_agenda_p	number)
					return date is

dt_agendamento_w	date;

begin

if	(nr_seq_agenda_p is not null) then

	select	hr_inicio
	into	dt_agendamento_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p
	and	obter_tipo_agenda(cd_agenda) = 1;

end if;

return dt_agendamento_w;

end obter_data_agendamento;
/