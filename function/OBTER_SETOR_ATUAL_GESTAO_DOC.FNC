create or replace
function obter_setor_atual_gestao_doc(	cd_setor_p	number)
					return varchar2 is

ds_retorno_w		varchar2(1) := 'N';
qt_reg_w		number(10);
					
begin

select	count(1)
into	qt_reg_w
from	gestao_documento a,
	gestao_doc_item d,
	solic_documento b,
	solic_item_documento c
where	b.nr_sequencia =		c.nr_seq_solic_doc
and	a.nr_sequencia =		d.nr_seq_gestao_doc
and	c.nr_seq_item_gestao_doc =	d.nr_sequencia
and	a.cd_setor_atendimento	=	cd_setor_p
and	b.dt_liberacao	is not null
and	b.dt_baixa	is null;

if	(nvl(qt_reg_w,0) > 0) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end obter_setor_atual_gestao_doc;
/	
