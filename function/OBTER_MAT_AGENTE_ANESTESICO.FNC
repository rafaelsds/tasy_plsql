create or replace
function obter_mat_agente_anestesico (
		nr_seq_agente_p		number,
		cd_material_p		varchar2,
		ie_considera_mat_p	varchar2)
 		return number is

ie_via_aplicacao_w	varchar2(5);
qtde_agente_anest_w	number(5);
cd_material_w		number(6) := null;

begin
if	(ie_considera_mat_p = 'S') then
	begin
	select	nvl(max(ie_via_aplicacao),'0')
	into 	ie_via_aplicacao_w 
	from	agente_anest_material
	where	nr_seq_agente 	= nr_seq_agente_p
	and	cd_material in (cd_material_p);
	end;
else
	begin
	select	nvl(max(ie_via_aplicacao),'0')
	into 	ie_via_aplicacao_w 
	from	agente_anest_material
	where	nr_seq_agente 	= nr_seq_agente_p;
	end;
end if;
	
select	count(*) 
into 	qtde_agente_anest_w
from 	agente_anestesico 
where 	nr_sequencia = nr_seq_agente_p
and 	ie_tipo in (1,3);

if 	(ie_via_aplicacao_w = '0') and
	(qtde_agente_anest_w > 0) then
	begin
	if	(ie_considera_mat_p = 'S') then
      		select  nvl(max(cd_material),0)
      		into	cd_material_w
      		from	agente_anest_material
      		where	nr_seq_agente = nr_seq_agente_p
      		and	cd_material in (cd_material_p);
   	else
      		select  nvl(max(cd_material),0)
      		into	cd_material_w
      		from	agente_anest_material
      		where	nr_seq_agente = nr_seq_agente_p;
   	end if;
	end;
end if;

return	cd_material_w;

end obter_mat_agente_anestesico;
/
