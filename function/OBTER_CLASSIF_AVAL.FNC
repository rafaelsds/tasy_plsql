create or replace
function obter_classif_aval (ie_prioridade_p		number)
 		    	return varchar2 is

ds_classif_w	varchar2(255);

begin

SELECT	ds_classificacao
INTO	ds_classif_w
FROM	nut_atend_serv_classif
WHERE	ie_prioridade = ie_prioridade_p;	

return	ds_classif_w;

end obter_classif_aval;
/