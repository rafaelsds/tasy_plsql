create or replace
function obter_lista_tipo_acomp(	nr_atendimento_p	number )
					return varchar2 is

ds_tipo_acompanhante_w	varchar2(255);
ds_lista_tipo_acompanhante_w	varchar2(2000);

cursor c01 is
	
	select	substr(b.ds_tipo_acompanhante,1,80)
	from   	atend_tipo_acompanhante a,
		tipo_acompanhante b
	where  	a.nr_seq_tipo_acompanhante = b.nr_sequencia 
	and	a.nr_atendimento = nr_atendimento_p
	order by 1;

begin

OPEN C01;
LOOP
FETCH C01 into
	ds_tipo_acompanhante_w;
exit when c01%notfound;
	begin

	if	(ds_lista_tipo_acompanhante_w is not null) then	
		ds_lista_tipo_acompanhante_w := substr(ds_lista_tipo_acompanhante_w || ', ',1,2000);
	end if;
		
	ds_lista_tipo_acompanhante_w	:= substr(ds_lista_tipo_acompanhante_w || ds_tipo_acompanhante_w,1,2000);	

	end;
END LOOP;
CLOSE C01;

return ds_lista_tipo_acompanhante_w;

end obter_lista_tipo_acomp;
/
