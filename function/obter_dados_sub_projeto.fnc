create or replace function obter_dados_sub_projeto(
			nr_seq_projeto_p	number,
			ie_tipo_dado_p		varchar2)
			return varchar2 is
					
ds_retorno_w		varchar2(255);
qt_subprojeto_w		number(10);
nr_seq_proj_sup_w	proj_projeto.nr_seq_proj_sup%type;

/*
>> ie_tipo_dado_p
> TSP - Retorna se o Projeto e um Subprojeto ou Projeto Superior
> QTS - Retorna a quantidade de Subprojetos vinculados ao Projeto 
> SOP - Retorna a quantidade de Subprojetos nao encerrados vinculados ao Projeto 
*/

begin

if	(nr_seq_projeto_p is not null) then
	
    select  nr_seq_proj_sup
    into    nr_seq_proj_sup_w
    from    proj_projeto
    where   nr_sequencia = nr_seq_projeto_p;

    if (ie_tipo_dado_p = 'TSP') then
        if (nr_seq_proj_sup_w is null) then
            ds_retorno_w := obter_desc_expressao(296535);
        else
            ds_retorno_w := obter_desc_expressao(597801);
        end if;
    elsif (ie_tipo_dado_p = 'QTS') then
      select  count(*)
      into    qt_subprojeto_w
      from    proj_projeto
      where   nr_seq_proj_sup = nr_seq_projeto_p;
      ds_retorno_w := qt_subprojeto_w;
    elsif (ie_tipo_dado_p = 'SOP') then
      select  count(*)
      into    qt_subprojeto_w
      from    proj_projeto
      where   nr_seq_proj_sup = nr_seq_projeto_p
      and     ie_status <> 'F';
      ds_retorno_w := qt_subprojeto_w;
    end if;	

end if;

return	ds_retorno_w;

end obter_dados_sub_projeto;
/
