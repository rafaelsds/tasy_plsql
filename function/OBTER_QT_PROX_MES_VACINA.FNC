create or replace
function obter_qt_prox_mes_vacina(
		nr_seq_vacina_p	number,
		ie_dose_p	varchar)
		return number is
		
qt_prox_mes_w	vacina_calendario.qt_prox_mes%type;
begin
if	(nr_seq_vacina_p is not null) then
	begin
	select 	max(a.qt_prox_mes)
	into 	qt_prox_mes_w
	from 	vacina_calendario a,
		valor_dominio b
	where 	a.nr_seq_vacina = nr_seq_vacina_p
	and 	a.ie_dose       = ie_dose_p
	and 	a.ie_dose       = b.vl_dominio
	and 	b.cd_dominio    = 1018;	
	end;
end if;
return qt_prox_mes_w;
end obter_qt_prox_mes_vacina;
/