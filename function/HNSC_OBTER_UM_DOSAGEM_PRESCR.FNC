Create or Replace
FUNCTION HNSC_Obter_UM_Dosagem_Prescr(
					nr_prescricao_p		Number,
					nr_sequencia_p		Number)
					RETURN VARCHAR IS

qt_dose_w			Varchar2(15);
qt_solucao_w			Number(15,4);
cd_unidade_medida_dose_w	Varchar2(30);
ds_prescr_w			Varchar2(15);
ds_prescricao_w			Varchar2(60);
ds_dose_diferenciada_w		Varchar2(50);
ds_retorno_w			Varchar2(80);
nr_agrupamento_w		Number(07,1);
qt_reg_w			Number(06,0);
nr_seq_w			Number(06,0);
ie_via_w			Varchar2(05);
ie_agrupador_w			Number(2);
qt_dose_especial_w		number(15,4);
hr_dose_especial_w		varchar2(5);

BEGIN

select	converte_fracao_dose(a.cd_unidade_medida_dose, a.qt_dose)  qt_dose,
	a.cd_unidade_medida_dose,
	c.ds_prescricao,
	a.ds_dose_diferenciada,
	qt_solucao,
	a.nr_agrupamento,
	a.ie_via_aplicacao,
	a.ie_agrupador,
	a.qt_dose_especial,
	a.hr_dose_especial
into	qt_dose_w,
	cd_unidade_medida_dose_w,
	ds_prescr_w,
	ds_dose_diferenciada_w,
	qt_solucao_w,
	nr_agrupamento_w,
	ie_via_w,
	ie_agrupador_w,
	qt_dose_especial_w,
	hr_dose_especial_w
from  	intervalo_prescricao c,
	material b,
	prescr_material a
where 	a.cd_material    	= b.cd_material
and 	a.cd_intervalo 		= c.cd_intervalo
and 	a.nr_prescricao  	= nr_prescricao_p
and	a.nr_sequencia		= nr_sequencia_p;

if	(substr(qt_dose_w,1,1) = ',') then
	qt_dose_w	:= '0' || qt_dose_w;
end if;
qt_dose_w	:= replace(qt_dose_w,'.',',');

select	count(*),
	min(nr_sequencia)
into	qt_reg_w,
	nr_seq_w
from	prescr_material
where	nr_prescricao		= nr_prescricao_p
  and	ie_agrupador		= 1
  and	nr_agrupamento		= nr_agrupamento_w;

if	(qt_reg_w > 1) and
	(nr_sequencia_p <> nr_seq_w) then
	ds_prescricao_w	:= ds_prescr_w;
	--ie_via_w		:= '';
elsif	(ds_dose_diferenciada_w is not null) then
	ds_prescricao_w	:= ds_dose_diferenciada_w || ' ' || cd_unidade_medida_dose_w;
	cd_unidade_medida_dose_w	:= '';
	qt_dose_w			:= '';
else
	ds_prescricao_w	:= ds_prescr_w;
end if;

/* Ivan em 08/02/2008 OS82257 - Incluída consistência para comparar o qt_dosagem_w somente no agrupador 1 */
if 	((ie_agrupador_w <> 1) or 
	(ie_agrupador_w = 1) and (qt_solucao_w is null)) then
	ds_retorno_w	:= qt_dose_w || ' ' || cd_unidade_medida_dose_w || 
			'   ' ||ds_prescricao_w || '   ' || ie_via_w;
else
	ds_retorno_w	:= ds_prescricao_w || '   ' || ie_via_w;
end if;

if	(qt_dose_especial_w is not null) then
	ds_retorno_w	:= ds_retorno_w || '  DE ' || to_char(qt_dose_especial_w) || ' ' || cd_unidade_medida_dose_w || '(' || hr_dose_especial_w || ')';
end if;	

RETURN ds_retorno_w;
END HNSC_Obter_UM_Dosagem_Prescr;
/
