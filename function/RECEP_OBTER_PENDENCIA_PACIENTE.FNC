create or replace
function recep_obter_pendencia_paciente(nr_interno_conta_p		number)
 		 return Varchar2 is
		 
ds_retorno_w			Varchar2(1) := 'N';
ie_pendencia_w			Varchar2(1);
nr_seq_regra_fluxo_w		number(10);
nr_seq_status_fat_w		number(10);
ie_pendencia_regra_w		varchar2(3);
ie_pendencia_cadastro_w		varchar2(3);


begin
if	(nr_interno_conta_p is not null) then
	
	select  max(nr_seq_regra_fluxo),
		max(nr_seq_status_fat)
	into    nr_seq_regra_fluxo_w,
		nr_seq_status_fat_w
	from    conta_paciente
	where   nr_interno_conta = nr_interno_conta_p;

	if	(nr_seq_regra_fluxo_w is not null) and
		(nr_seq_status_fat_w is not null) then
	
		select	max(ie_pendencia)
		into	ie_pendencia_regra_w
		from	cf_regra_estagio
		where	nr_seq_regra = nr_seq_regra_fluxo_w
		and	nr_seq_status_fat = nr_seq_status_fat_w;
		
		select 	max(ie_pendencia)
		into	ie_pendencia_cadastro_w
		from 	cf_status_faturamento
		where 	nr_sequencia = nr_seq_status_fat_w;
		
		ie_pendencia_w:= null;
		
		if		(ie_pendencia_regra_w is not null) then
				ie_pendencia_w:= ie_pendencia_regra_w;
		elsif 		(ie_pendencia_cadastro_w is not null) then
				ie_pendencia_w:= ie_pendencia_cadastro_w;
		end if;

		if	(ie_pendencia_w = 'R') then
			ds_retorno_w := 'S';
		end if;
	
	end if;
end if;

return	ds_retorno_w;

end recep_obter_pendencia_paciente;
/
