create or replace
function obter_tempo_total_gas (nr_seq_gas_p		number,
				ie_disp_resp_esp_p	varchar2,
				dt_inicial_p		date,
				dt_final_p		date,
				nr_atendimento_p	number)
 		    	return varchar2 is

ds_w			varchar2(50);
ds_tempo_w		varchar2(50);
qt_horas_w		number(15);
qt_min_w		number(15);
qt_total_horas_w	number(15);
qt_total_min_w		number(15);
			
cursor c01 is
SELECT 	obter_tempo_entre_datas(Obter_Datas_Gasoterapia(a.nr_sequencia,'I'),NVL(Obter_Datas_Gasoterapia(a.nr_sequencia,'T'),SYSDATE),'D') * 24+
	obter_tempo_entre_datas(Obter_Datas_Gasoterapia(a.nr_sequencia,'I'),NVL(Obter_Datas_Gasoterapia(a.nr_sequencia,'T'),SYSDATE),'H') 
        ||':'|| 
	obter_tempo_entre_datas(Obter_Datas_Gasoterapia(a.nr_sequencia,'I'),NVL(Obter_Datas_Gasoterapia(a.nr_sequencia,'T'),SYSDATE),'M')
FROM	prescr_gasoterapia a,
	prescr_medica b
WHERE	a.dt_atualizacao	BETWEEN dt_inicial_p AND dt_final_p
AND 	a.nr_prescricao		= b.nr_prescricao
and	a.nr_seq_gas		= nr_seq_gas_p
and	nvl(a.ie_disp_resp_esp,'XPTO')	= nvl(ie_disp_resp_esp_p,'XPTO')
AND	b.nr_atendimento	= nr_atendimento_p
and	Obter_Datas_Gasoterapia(a.nr_sequencia,'I') is not null;

begin

open C01;
loop
fetch C01 into	
	ds_w;
exit when C01%notfound;
	
	qt_horas_w		:= to_number(substr(ds_w,1,instr(ds_w,':')-1));
	
	qt_min_w		:= to_number(substr(ds_w,instr(ds_w,':')+1,length(ds_w)));
	
	qt_total_horas_w	:= nvl(qt_total_horas_w,0) + qt_horas_w;
	
	qt_total_min_w		:= nvl(qt_total_min_w,0) + qt_min_w;
	
	if	(qt_total_min_w	> 60) then
		qt_total_horas_w	:= nvl(qt_total_horas_w,0) + to_number(trim(replace(to_char(floor(dividir(qt_total_min_w,60)),'999,999,900'),',','.')));
		qt_total_min_w		:= to_number(substr(to_char(mod(qt_total_min_w,60),'00'),2,2));
	end if;
	
end loop;
close C01;

ds_tempo_w	:= to_char(qt_total_horas_w);
if	(length(qt_total_horas_w) = 1) then
	ds_tempo_w	:= '0' || qt_total_horas_w;
end if;

if	(length(qt_total_min_w) = 1) then
	ds_tempo_w	:= ds_tempo_w || ':' || '0' || to_char(qt_total_min_w);
else
	ds_tempo_w	:= ds_tempo_w || ':' || to_char(qt_total_min_w);
end if;
--ds_tempo_w	:= to_char(qt_total_horas_w || ':' || qt_total_min_w);

return	ds_tempo_w;

end obter_tempo_total_gas;
/

