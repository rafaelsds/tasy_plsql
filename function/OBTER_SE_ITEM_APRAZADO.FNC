create or replace
function obter_se_item_aprazado (
				nr_prescricao_p	number,
				nr_seq_item_p	number)
				return varchar2 is
				
ie_aprazado_w	varchar2(1);
				
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_item_p is not null) then
	
	select	decode(count(*),0,'N','S')
	into	ie_aprazado_w
	from	prescr_mat_hor
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_material	= nr_seq_item_p
	and	ie_aprazado	= 'S'
	and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
	
end if;

return ie_aprazado_w;

end obter_se_item_aprazado;
/