create or replace
function reinf_4020_dados_imp_tit_pagar( nr_seq_titulo_p 	number,
			ie_tipo_valor_p 		varchar2,
			ie_tipo_tributo_p 		varchar2,
			ie_exigibilidade_p		varchar2,
			cd_estabelecimento_p 	number,
			dt_baixa_p		date default null,
			ie_exig_susp_forma_p 	varchar2 default null)
 		    	return number is

vl_tributo_w		titulo_pagar_imposto.vl_imposto%type;
vl_base_calculo_w		titulo_pagar_imposto.vl_base_calculo%type;
ds_retorno_w		number(15,4);
				
begin

if (ie_exigibilidade_p = 'S') then
	begin
		select 	sum(vl_imposto),
			sum(vl_base_calculo)
		into	vl_tributo_w,
			vl_base_calculo_w
		from(		
			select	nvl(sum(i.vl_imposto),0) vl_imposto,
				nvl(sum(i.vl_base_calculo),0) vl_base_calculo
			from	titulo_pagar_baixa b,
				titulo_pagar_imposto i,
				tributo_conta_pagar tc,
				tributo t
			where	i.nr_seq_trib_cp = tc.nr_sequencia
			and	t.cd_tributo = i.cd_tributo
			and	tc.cd_tributo = t.cd_tributo		
			and	b.nr_sequencia(+) = i.nr_seq_baixa
			and	i.nr_titulo = nr_seq_titulo_p
			and   	((i.nr_seq_baixa is null) or (b.dt_baixa =  dt_baixa_p))		
			and	((nvl(ie_tipo_tributo_p,'X') = 'X') or (t.ie_tipo_tributo = ie_tipo_tributo_p))
			and	tc.ie_exigibilidade_suspensa = 'S'
			and	i.cd_tributo in (select cd_tributo from fis_reinf_r4020_regra where cd_estabelecimento = cd_estabelecimento_p and ie_tipo_data = 2)
			and	((tc.ie_exig_susp_forma = ie_exig_susp_forma_p) or (ie_exig_susp_forma_p is null))			
		);
						
	end;
else
	begin
		select 	sum(vl_imposto),
			sum(vl_base_calculo)
		into	vl_tributo_w,
			vl_base_calculo_w
		from(
			select	nvl(sum(i.vl_imposto),0) vl_imposto,
				nvl(sum(i.vl_base_calculo),0) vl_base_calculo
			from	titulo_pagar_baixa b,
				titulo_pagar_imposto i,
				tributo_conta_pagar tc,
				tributo t
			where	i.nr_seq_trib_cp = tc.nr_sequencia
			and	t.cd_tributo = i.cd_tributo
			and	tc.cd_tributo = t.cd_tributo		
			and	i.nr_titulo = nr_seq_titulo_p
			and	b.nr_sequencia(+) = i.nr_seq_baixa
			and   	((i.nr_seq_baixa is null) or (b.dt_baixa =  dt_baixa_p))		
			and	((nvl(ie_tipo_tributo_p,'X') = 'X') or (t.ie_tipo_tributo = ie_tipo_tributo_p))
			and	i.cd_tributo in (select cd_tributo from fis_reinf_r4020_regra where cd_estabelecimento = cd_estabelecimento_p and ie_tipo_data = 2)
			and	(tc.ie_exigibilidade_suspensa is null or tc.ie_exigibilidade_suspensa = 'N')
			and	tc.ie_exig_susp_forma is null
			union all
			select	nvl(sum(i.vl_imposto),0) vl_imposto,
				nvl(sum(i.vl_base_calculo),0) vl_base_calculo	
			from	titulo_pagar_baixa b,
				titulo_pagar_imposto i,
				tributo t
			where	t.cd_tributo = i.cd_tributo
			and	i.nr_titulo = nr_seq_titulo_p
			and	b.nr_sequencia(+) = i.nr_seq_baixa
			and	i.nr_seq_trib_cp is null
			and   	((i.nr_seq_baixa is null) or (b.dt_baixa =  dt_baixa_p))		
			and	((nvl(ie_tipo_tributo_p,'X') = 'X') or (t.ie_tipo_tributo = ie_tipo_tributo_p))		
			and	i.cd_tributo in (select cd_tributo from fis_reinf_r4020_regra where cd_estabelecimento = cd_estabelecimento_p and ie_tipo_data = 2)
		);
		
		
	end;
end if;

if	(ie_tipo_valor_p = 'B' )then
	ds_retorno_w := vl_base_calculo_w;
elsif (ie_tipo_valor_p = 'V') then
	ds_retorno_w := vl_tributo_w;
end if;


return	ds_retorno_w;

end reinf_4020_dados_imp_tit_pagar;
/