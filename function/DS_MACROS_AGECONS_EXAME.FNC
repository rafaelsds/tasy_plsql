create or replace 
function ds_macros_agecons_exame
 		    	return varchar2 is
			
ds_retorno_w		varchar2(4000);			

begin

ds_retorno_w	:= 	replace(wheb_mensagem_pck.get_texto(801941), '@Medico', obter_traducao_macro_pront('@Medico',820)) ||CHR(10)||
			replace(wheb_mensagem_pck.get_texto(801945), '@Procedimento', obter_traducao_macro_pront('@Procedimento',820)) ||CHR(10)||
			replace(wheb_mensagem_pck.get_texto(801947), '@Orientacao', obter_traducao_macro_pront('@Orientacao',820)) ||CHR(10)||
			replace(wheb_mensagem_pck.get_texto(801948), '@Paciente', obter_traducao_macro_pront('@Paciente',820)) ||CHR(10)||
			replace(wheb_mensagem_pck.get_texto(801949), '@Dia', obter_traducao_macro_pront('@Dia',820)) ||CHR(10)||
			replace(wheb_mensagem_pck.get_texto(801950), '@Hora', obter_traducao_macro_pront('@Hora',820));

return	ds_retorno_w;

end ds_macros_agecons_exame;
/
