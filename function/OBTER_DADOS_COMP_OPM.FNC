create or replace
function obter_dados_comp_opm(	nr_sequencia_p		number,
											nr_seq_origem_p	number,
											ie_opcao_p			varchar) 
											return 				varchar2 is

ds_retorno_w	varchar2(255);
/*
ie_opcao_p 
SC - Solicitação de compra
RM - Requisição de material
ST - Solicitação de transferência
*/
begin

if	(ie_opcao_p	= 'SC') then
	select 	substr(max(nr_solic_compra),1,255)
	into		ds_retorno_w
	from   	solic_compra_item 
	where  	((nr_seq_op_comp_opm = nr_sequencia_p) or (nr_seq_op_comp_opm = nr_seq_origem_p));
elsif	(ie_opcao_p	= 'RM') then
	select 	substr(max(nr_requisicao),1,255)
	into		ds_retorno_w
	from   	item_requisicao_material
	where  	((nr_seq_op_comp_opm = nr_sequencia_p) or (nr_seq_op_comp_opm = nr_seq_origem_p));
elsif	(ie_opcao_p	= 'ST') then
	select 	substr(max(nr_ordem_compra),1,255)
	into		ds_retorno_w
	from   	ordem_compra_item
	where  	((nr_seq_op_comp_opm = nr_sequencia_p) or (nr_seq_op_comp_opm = nr_seq_origem_p))
	and		nvl(ie_situacao,'A') = 'A';
end if;	

return	ds_retorno_w;

end obter_dados_comp_opm;
/