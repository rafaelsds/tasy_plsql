create or replace
function cus_obter_rec_liquida(	cd_estabelecimento_p	number,
					cd_tabela_custo_p	number,
					cd_centro_controle_p	number,
					ie_centro_resultado_p	varchar2,
					ie_tipo_centro_controle_p	varchar2,
					ie_unidade_negocio_p number := null) 
					return number is

cd_empresa_w				number(4);
vl_receita_liquida_w			number(15,2);

BEGIN

select	max(obter_empresa_estab(cd_estabelecimento_p))
into	cd_empresa_w
from	dual;

if	(cd_centro_controle_p = 99999999) then
	begin
	if	(ie_tipo_centro_controle_p = 2) then
		select	sum(vl_mes)
		into	vl_receita_liquida_w
		from	centro_controle c,
			classif_result b,
			resultado_centro_controle a
		where	a.cd_estabelecimento		= c.cd_estabelecimento
		and	a.cd_centro_controle		= c.cd_centro_controle
		and	a.ie_classif_conta		= b.cd_classificacao
		and	c.ie_situacao			= 'A'
		and	b.ie_receita_liquida		= 'S'
		and	c.ie_tipo_centro_controle	= ie_tipo_centro_controle_p
		and	c.ie_centro_resultado	= nvl(ie_centro_resultado_p,'N')
		and	a.cd_estabelecimento		= cd_estabelecimento_p
		and	a.cd_tabela_custo		= cd_tabela_custo_p
		and	((nvl(ie_unidade_negocio_p, 0) = 0) or (substr(cus_obter_se_centro_unid_neg(c.cd_estabelecimento, c.cd_centro_controle, ie_unidade_negocio_p),1,1) = 'S'))
		and	b.cd_empresa			= cd_empresa_w;
	else
		select	sum(vl_mes)
		into	vl_receita_liquida_w
		from	centro_controle c,
			classif_result b,
			resultado_centro_controle a
		where	a.cd_estabelecimento		= c.cd_estabelecimento
		and	a.cd_centro_controle		= c.cd_centro_controle
		and	a.ie_classif_conta		= b.cd_classificacao
		and	c.ie_situacao			= 'A'
		and	b.ie_receita_liquida		= 'S'
		and	c.ie_tipo_centro_controle <> ie_tipo_centro_controle_p
		and	c.ie_centro_resultado	= nvl(ie_centro_resultado_p,'N')
		and	a.cd_estabelecimento		= cd_estabelecimento_p
		and	a.cd_tabela_custo		= cd_tabela_custo_p
		and	((nvl(ie_unidade_negocio_p, 0) = 0) or (substr(cus_obter_se_centro_unid_neg(c.cd_estabelecimento, c.cd_centro_controle, ie_unidade_negocio_p),1,1) = 'S'))
		and	b.cd_empresa			= cd_empresa_w;
	end if;
	end;	
else
	select	sum(vl_mes)
	into	vl_receita_liquida_w
	from	centro_controle c,
		classif_result b,
		resultado_centro_controle a
	where	a.cd_estabelecimento			= c.cd_estabelecimento
	and	a.cd_centro_controle			= c.cd_centro_controle
	and	a.ie_classif_conta			= b.cd_classificacao
	and	c.ie_situacao				= 'A'
	and	b.ie_receita_liquida			= 'S'
	and	((c.ie_tipo_centro_controle		= ie_tipo_centro_controle_p) or (ie_tipo_centro_controle_p is null))
	and	c.ie_centro_resultado		= nvl(ie_centro_resultado_p,'N')
	and	a.cd_estabelecimento			= cd_estabelecimento_p
	and	a.cd_tabela_custo			= cd_tabela_custo_p
	and	c.cd_centro_controle			= cd_centro_controle_p
	and	((nvl(ie_unidade_negocio_p, 0) = 0) or (substr(cus_obter_se_centro_unid_neg(c.cd_estabelecimento, c.cd_centro_controle, ie_unidade_negocio_p),1,1) = 'S'))
	and	b.cd_empresa				= cd_empresa_w;
end if;

return vl_receita_liquida_w;

END cus_obter_rec_liquida;
/