create or replace
function obter_dados_processo_aprov(	nr_seq_aprovacao_p			number,
				ie_opcao_p				number)
	return number is

ie_tipo_w				number(1);
vl_total_w			number(15,2);
vl_tot_qt_ult_compra_w		number(15,2);

begin

/*
ie_tipo_w
0 - Ordens
1 - Solicitacao
2 - Requisicao
*/

select	distinct nvl(max(ie_tipo),0)
into	ie_tipo_w
from	(
	select	0 ie_tipo
	from	ordem_compra_item
	where	nr_seq_aprovacao = nr_seq_aprovacao_p
	union all				 		
	select	1 ie_tipo
	from	solic_compra_item
	where	nr_seq_aprovacao = nr_seq_aprovacao_p
	union all				 		
	select	2 ie_tipo
	from	item_requisicao_material
	where	nr_seq_aprovacao = nr_seq_aprovacao_p);

if	(ie_tipo_w = 0) then
	select	nvl(sum((b.qt_material * b.vl_unitario_material)),0) vl_total,
		nvl(sum((to_number(obter_dados_ult_compra_data(a.cd_estabelecimento,b.cd_material,null,sysdate,0,'VU')) * b.qt_material)),0) vl_tot_qt_ult_compra
	into	vl_total_w,
		vl_tot_qt_ult_compra_w
	from	ordem_compra a,
		ordem_compra_item b
	where	a.nr_ordem_compra = b.nr_ordem_compra
	and	b.dt_aprovacao is null
	and	b.dt_reprovacao is null
	and	nr_seq_aprovacao = nr_seq_aprovacao_p;
elsif	(ie_tipo_w = 1) then
	select	nvl(sum((b.qt_material * NVL(b.vl_unit_previsto,0))),0)vl_total,
		nvl(sum((to_number(obter_dados_ult_compra_data(a.cd_estabelecimento,b.cd_material,null,sysdate,0,'VU')) * b.qt_material)),0) vl_tot_qt_ult_compra
	into	vl_total_w,
		vl_tot_qt_ult_compra_w
	from	solic_compra a,
		solic_compra_item b
	where	a.nr_solic_compra = b.nr_solic_compra
	and	b.dt_reprovacao is null
	and	b.dt_autorizacao is null
	and	nr_seq_aprovacao = nr_seq_aprovacao_p;
elsif	(ie_tipo_w = 2) then
	select	nvl(sum((b.qt_material_requisitada * NVL(b.vl_unit_previsto,0))),0)vl_total,
		nvl(sum((to_number(obter_dados_ult_compra_data(a.cd_estabelecimento,b.cd_material,null,sysdate,0,'VU')) * b.qt_material_requisitada)),0) vl_tot_qt_ult_compra
	into	vl_total_w,
		vl_tot_qt_ult_compra_w
	from	requisicao_material a,
		item_requisicao_material b
	where	a.nr_requisicao = b.nr_requisicao
	and	b.dt_reprovacao is null
	and	b.dt_aprovacao is null
	and	nr_seq_aprovacao = nr_seq_aprovacao_p;	
end if;	
		
if	(ie_opcao_p = 0) then
	return vl_total_w;
elsif	(ie_opcao_p = 1) then
	return vl_tot_qt_ult_compra_w;
end if;
	
end obter_dados_processo_aprov;
/
