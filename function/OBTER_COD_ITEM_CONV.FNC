create or replace
function obter_cod_item_conv (nr_seq_proc_p	number,
				nr_prescricao_p	number)
 		    	return varchar2 is

nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
cd_convenio_w		convenio.cd_convenio%type;			
nr_seq_atecaco_w	prescr_medica.nr_seq_atecaco%type;			
cd_especialidade_w	medico_especialidade.cd_especialidade%type;
cd_medico_exec_w	prescr_procedimento.cd_medico_exec%type;
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type;
cd_procedimento_w	prescr_procedimento.cd_procedimento%type;
ie_origem_proced_w	prescr_procedimento.ie_origem_proced%type;

cd_retorno_w		conversao_proc_convenio.cd_proc_convenio%type;
cd_categoria_w		atend_categoria_convenio.cd_categoria%type;

nr_seq_proc_interno_w	prescr_procedimento.NR_SEQ_PROC_INTERNO%type;
NR_SEQ_EXAME_w		prescr_procedimento.nr_seq_exame%type;

begin

if (nr_prescricao_p > 0) then

	select 	max(nvl(nr_seq_atecaco, 0)),
		max(nr_atendimento)
	into	nr_seq_atecaco_w,
		nr_atendimento_w
	from 	prescr_medica
	where 	nr_prescricao = nr_prescricao_p;

	if (nr_seq_atecaco_w = 0) then
		nr_seq_atecaco_w := substr(obter_atecaco_atendimento(nr_atendimento_w), 1, 50);
	end if;

	select	substr(obter_especialidade_medico(cd_medico_exec, 'CD'), 1, 255),
		cd_medico_exec,
		cd_procedimento,
		ie_origem_proced,
		NR_SEQ_EXAME,
		NR_SEQ_PROC_INTERNO
	into 	cd_especialidade_w,
		cd_medico_exec_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		NR_SEQ_EXAME_w,
		nr_seq_proc_interno_w
	from 	prescr_procedimento
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_seq_proc_p;

	select 	max(ie_tipo_atendimento)
	into 	ie_tipo_atendimento_w
	from 	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_w;

	select 	max(cd_convenio),
		max(cd_categoria)
	into 	cd_convenio_w,
		cd_categoria_w
	from 	atend_categoria_convenio
	where 	nr_seq_interno = nr_seq_atecaco_w;	
		
	cd_retorno_w := obter_dados_item_conv(cd_convenio_w, cd_categoria_w, 1, cd_procedimento_w, ie_origem_proced_w, 'S', cd_especialidade_w, cd_medico_exec_w, ie_tipo_atendimento_w, nr_atendimento_w, nr_seq_proc_interno_w, NR_SEQ_EXAME_w, 'C');
end if;

return	cd_retorno_w;

end obter_cod_item_conv ;
/