create or replace
function obter_se_duplic_prescr_just	(nr_atendimento_p	number,
					nr_prescricao_p		number,
					nr_seq_proced_p		number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					cd_material_exame_p	varchar2,
					dt_prev_execucao_p	date,
					qt_hora_proced_duplic_p	number,
					nr_seq_proc_interno_p	number,
					nr_seq_exame_p		number)
					return varchar2 is
					
ie_proced_duplic_w	varchar2(1) := null;
dt_prev_execucao_w	date;
nr_seq_derivado_w	number(10);
qt_registros_w		number;
					
begin

if	(nr_atendimento_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_proced_p is not null) and
	(cd_procedimento_p is not null) and
	(ie_origem_proced_p is not null) and
	(dt_prev_execucao_p is not null) then
	
	if	(nvl(qt_hora_proced_duplic_p,0) = 0) then
	
		select	nvl(max('N'),'S')
		into	ie_proced_duplic_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia = nr_seq_proced_p
		and	ds_observacao is not null;
		
		select	max(nr_seq_derivado)
		into	nr_seq_derivado_w
		from	prescr_procedimento
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_proced_p;
		
		if	(ie_proced_duplic_w = 'S') then
			begin
	
			select	nvl(max('S'),'N')
			into	ie_proced_duplic_w
			from	prescr_medica a,
				prescr_procedimento b 
			where	b.nr_prescricao			= a.nr_prescricao
			and	a.nr_atendimento		= nr_atendimento_p
			and	b.cd_procedimento		= cd_procedimento_p
			and	b.ie_origem_proced		= ie_origem_proced_p
			and	nvl(b.nr_seq_proc_interno,0)	= nvl(nr_seq_proc_interno_p,0)
			and	nvl(b.nr_seq_exame,0)		= nvl(nr_seq_exame_p,0)
			and	nvl(b.cd_material_exame,'0')	= nvl(cd_material_exame_p,'0')
			and	trunc(b.dt_prev_execucao,'dd') 	= trunc(dt_prev_execucao_p,'dd')
			and	nvl(b.nr_seq_derivado,0)	= nvl(nr_seq_derivado_w,0)
			and	((a.nr_prescricao <> nr_prescricao_p) or (b.nr_sequencia <> nr_seq_proced_p))			
			and	b.nr_seq_origem is null		
			and	b.dt_suspensao is null;	
			end;
		end if;				
		
	else
	
		select	nvl(max('N'),'S')
		into	ie_proced_duplic_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia = nr_seq_proced_p
		and	ds_observacao is not null;
		
		if	(ie_proced_duplic_w = 'S') then
			begin
			
			dt_prev_execucao_w := dt_prev_execucao_p - (qt_hora_proced_duplic_p / 24);
			
			select	max(nr_seq_derivado)
			into	nr_seq_derivado_w
			from	prescr_procedimento
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	= nr_seq_proced_p;
		
			select	nvl(max('S'),'N')
			into	ie_proced_duplic_w
			from	prescr_medica a,
				prescr_procedimento b
			where	b.nr_prescricao			= a.nr_prescricao
			and	a.nr_atendimento		= nr_atendimento_p
			and	b.cd_procedimento		= cd_procedimento_p
			and	b.ie_origem_proced		= ie_origem_proced_p
			and	nvl(b.nr_seq_proc_interno,0)	= nvl(nr_seq_proc_interno_p,0)
			and	nvl(b.cd_material_exame,'0')	= nvl(cd_material_exame_p,'0')
			and	nvl(b.nr_seq_exame,0)		= nvl(nr_seq_exame_p,0)
			and	nvl(b.nr_seq_derivado,0)	= nvl(nr_seq_derivado_w,0)
			and	b.dt_prev_execucao between dt_prev_execucao_w and dt_prev_execucao_p
			and	((a.nr_prescricao <> nr_prescricao_p) or (b.nr_sequencia <> nr_seq_proced_p))			
			and	b.nr_seq_origem is null		
			and	b.dt_suspensao is null;
		
			end;
		end if;		
		
	end if;	
	
end if;

return ie_proced_duplic_w;

end obter_se_duplic_prescr_just;
/