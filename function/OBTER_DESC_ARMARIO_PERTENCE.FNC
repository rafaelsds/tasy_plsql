create or replace
function Obter_desc_armario_pertence(nr_seq_armario_p	Number)
          	return varchar2 is

ds_retorno_w	varchar2(80);

begin

select	ds_armario
into	ds_retorno_w
from	armario_pertence
where	nr_sequencia	= nr_seq_armario_p;

return	ds_retorno_w;

end Obter_desc_armario_pertence;
/