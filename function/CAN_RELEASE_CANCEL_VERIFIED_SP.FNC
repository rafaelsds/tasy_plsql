CREATE OR REPLACE FUNCTION CAN_RELEASE_CANCEL_VERIFIED_SP RETURN VARCHAR2 AS 
can_release_verified_sp_w varchar2(1) := 'N';
count_w number(10);

BEGIN
  select count(1) into count_w 
  from MAN_SERVICE_PACK_VERSAO
  where ie_status_build = 'G';

  if (count_w > 0) then
    can_release_verified_sp_w := 'S';
  end if;
 
  RETURN can_release_verified_sp_w;
END CAN_RELEASE_CANCEL_VERIFIED_SP;
/