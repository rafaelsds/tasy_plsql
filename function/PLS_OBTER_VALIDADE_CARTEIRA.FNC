create or replace
function pls_obter_validade_carteira
			(	cd_usuario_plano_p		varchar2,
				cd_estabelecimento_p		number)
				return date is

nr_seq_segurado_w		number(10);
dt_validade_cart_w		date	:= '';

begin

select	nvl(max(a.nr_sequencia),0)
into	nr_seq_segurado_w
from	pls_segurado a,
	pls_segurado_carteira b
where	a.nr_sequencia		= b.nr_seq_segurado
and	b.cd_usuario_plano	= cd_usuario_plano_p
and	b.cd_estabelecimento	= cd_estabelecimento_p;

if (nr_seq_segurado_w <> 0) then
	select	max(dt_validade_carteira)
	into	dt_validade_cart_w
	from	pls_segurado_carteira
	where	nr_seq_segurado   = nr_seq_segurado_w
	and	cd_usuario_plano  = cd_usuario_plano_p;
end if;

return	dt_validade_cart_w;

end pls_obter_validade_carteira;
/