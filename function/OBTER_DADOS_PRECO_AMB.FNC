create or replace
FUNCTION Obter_dados_preco_amb(	nr_sequencia_p	Number,
				ie_campo_p	Varchar2)
				return		Number is

/* ie_campo_p
'O' = vl_custo_operacional_w
'F' = qt_filme_w */

qt_retorno_w			Number(15,4);
vl_custo_operacional_w		Number(15,2);
qt_filme_w			Number(15,4);

BEGIN

begin
select	a.vl_custo_operacional,
	a.qt_filme
into	vl_custo_operacional_w,
	qt_filme_w
from	procedimento_paciente b,
	preco_amb a
where	b.nr_sequencia = nr_sequencia_p
and	b.cd_edicao_amb = a.cd_edicao_amb
and	b.cd_procedimento = a.cd_procedimento
and	b.ie_origem_proced = a.ie_origem_proced
and	a.dt_inicio_vigencia = (
	select	max(x.dt_inicio_vigencia)
	from	preco_amb x
	where	x.dt_inicio_vigencia = a.dt_inicio_vigencia);
exception
	when others then
	begin
	vl_custo_operacional_w	:= 0;
	qt_filme_w			:= 0;
	end;
end;

if	(ie_campo_p = 'O') then
	qt_retorno_w	:= vl_custo_operacional_w;
elsif	(ie_campo_p = 'F') then
	qt_retorno_w	:= qt_filme_w;
end if;

return qt_retorno_w;

END Obter_dados_preco_amb;
/