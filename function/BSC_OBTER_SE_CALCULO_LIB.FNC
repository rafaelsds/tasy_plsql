create or replace
function bsc_obter_se_calculo_lib(	cd_ano_p	number,
				cd_periodo_p	number,
				nr_seq_edicao_p	number default null)
				return varchar2 is

ie_retorno_w				varchar2(1);
nr_seq_edicao_w			number(10);

begin

ie_retorno_w		:= 'S';
nr_seq_edicao_w		:= nr_seq_edicao_p;


select	nvl(max('S'),'N')
into	ie_retorno_w
from	bsc_calculo
where	cd_ano		= cd_ano_p
and	cd_periodo	= cd_periodo_p
and	dt_liberacao is not null
and	nvl(nr_seq_edicao,nr_seq_edicao_w)	= nvl(nr_seq_edicao_w,nr_seq_edicao);

return	ie_retorno_w;

end bsc_obter_se_calculo_lib;
/
