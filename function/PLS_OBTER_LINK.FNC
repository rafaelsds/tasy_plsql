create or replace
function pls_obter_link(	ie_opcao_p		varchar2,
				nr_seq_contrato_p	varchar2,
				cd_operadora_empresa_p	varchar2,
				cd_matricula_familia_p	varchar2,
				cd_usuario_plano_p	pls_segurado_carteira.cd_usuario_plano%type)
			return	varchar2 is	

/*
ie_opcao_p
B = Beneficiário
C = Contrato
*/
			
ds_retorno_w	varchar2(255);

begin

if	(ie_opcao_p = 'C') then
	select	max(ds_link)
	into	ds_retorno_w
	from	pls_contrato_ged
	where	nr_seq_contrato		= nr_seq_contrato_p
	and	ie_tipo_registro	= 'C'
	and	((cd_operadora_empresa	= cd_operadora_empresa_p or cd_operadora_empresa_p is null) or cd_operadora_empresa is null);
elsif 	(ie_opcao_p = 'B') then
	select	max(ds_link)
	into	ds_retorno_w
	from	pls_contrato_ged
	where	nr_seq_contrato 	= nr_seq_contrato_p
	and	ie_tipo_registro	= 'B'
	and	((cd_matricula_familia	= cd_matricula_familia_p or cd_matricula_familia_p is null) or cd_matricula_familia is null)
	and	((cd_usuario_plano = cd_usuario_plano_p or cd_usuario_plano_p is null) or (cd_usuario_plano is null));
end if;

return ds_retorno_w;

end pls_obter_link;
/
