create or replace
function Obter_Se_Responsavel_etapa (nr_interno_conta_p number, 
				     nr_seq_etapa_p number, 
				     nm_usuario_p varchar2) 
				     return varchar2 is 

ds_retorno_w		     Varchar2(1);
ie_tipo_atendimento_w 	     number(3,0); 
ie_clinica_w		     number(5,0); 	
cd_convenio_parametro_w      number(5,0); 	  
cd_categoria_parametro_w     varchar2(10);   
nr_seq_classificacao_w       number(10,0); 
nr_atendimento_w 	     number(10,0); 
qt_existe_w		     number(10,0);
cd_setor_atendimento_w       setor_atendimento.cd_setor_atendimento%type;	
				    
begin

ds_retorno_w:= 'N';

select 	nvl(max(a.ie_tipo_atendimento),1) ie_tipo_atendimento,
	nvl(max(a.nr_seq_classificacao),0) nr_seq_classificacao,
	nvl(max(a.ie_clinica),0) ie_clinica,		
	nvl(max(b.cd_convenio_parametro),0) cd_convenio_parametro,
	nvl(max(b.cd_categoria_parametro),'0') cd_categoria_parametro,
	nvl(max(a.nr_atendimento),0) nr_atendimento
into    ie_tipo_atendimento_w,
	nr_seq_classificacao_w,
	ie_clinica_w,
	cd_convenio_parametro_w,
	cd_categoria_parametro_w,
	nr_atendimento_w
from 	atendimento_paciente a,
	conta_paciente b		
where 	a.nr_atendimento = b.nr_atendimento
and 	b.nr_interno_conta = nr_interno_conta_p;
        
if 	(nr_atendimento_w > 0) then

	cd_setor_atendimento_w:= nvl(Obter_Setor_Atendimento(nr_atendimento_w),0);	

	--verificar o responsável
	select 	count(*)
	into	qt_existe_w
	from    fatur_etapa_responsavel
	where   nr_seq_etapa = nr_seq_etapa_p
	and 	nm_usuario_resp = nm_usuario_p
	and 	ie_situacao = 'A'
	and 	trunc(sysdate,'dd') between dt_inicio_vigencia and nvl(dt_final_vigencia, sysdate)
	and 	nvl(cd_convenio, cd_convenio_parametro_w) = cd_convenio_parametro_w
	and 	nvl(cd_categoria, cd_categoria_parametro_w) = cd_categoria_parametro_w
	and 	nvl(ie_tipo_atendimento, ie_tipo_atendimento_w) = ie_tipo_atendimento_w
        and     nvl(nr_seq_classificacao, nr_seq_classificacao_w) = nr_seq_classificacao_w
	and 	nvl(ie_clinica, ie_clinica_w) = ie_clinica_w
	and 	nvl(cd_setor_atendimento, cd_setor_atendimento_w) = cd_setor_atendimento_w;

end if;
		
if 	(qt_existe_w > 0)then
	ds_retorno_w:= 'S';
else
	ds_retorno_w:= 'N';	
end if;
	
return	ds_retorno_w;

end Obter_Se_Responsavel_etapa;
/