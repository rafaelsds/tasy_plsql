create or replace
function rel_obter_setor_2604(nr_atendimento_p	number)
 		    	return varchar2 is
			
cd_setor_atendimento_w	number(10);

begin

	select  	max(a.cd_setor_atendimento)
	into	cd_setor_atendimento_w 
	from 	atend_paciente_unidade a 
	where	a.dt_entrada_unidade = (select max(b.dt_entrada_unidade) 
			      from atend_paciente_unidade b 
			      where b.nr_atendimento = a.nr_atendimento)
and	a.nr_atendimento = nr_atendimento_p;

return	cd_setor_atendimento_w;

end rel_obter_setor_2604;
/