create or replace
function obter_tx_desc_antec_pj(cd_estabelecimento_p	number,
				cd_cgc_p		varchar)
 		    	return number is

			
tx_desc_antecipacao_w	number(7,4);

begin

select	max(nvl(tx_desc_antecipacao,0))
into	tx_desc_antecipacao_w
from	pessoa_juridica_estab 
where	cd_cgc 			= cd_cgc_p
and	cd_estabelecimento 	= cd_estabelecimento_p;

return	tx_desc_antecipacao_w;

end obter_tx_desc_antec_pj;
/