create or replace 
Function Obter_Desc_Morfologia(	cd_morfologia_p		Varchar2,
                                nr_seq_morf_desc_adic_p number  default null) 
				return varchar2 is
ds_retorno_w	Varchar2(255);
begin
	if (cd_morfologia_p is not null) then
		select nvl((select a.ds_morfologia from CIDO_MORFOLOGIA_DESC_ADIC a
		where a.nr_sequencia = nr_seq_morf_desc_adic_p and a.cd_morfologia = cd_morfologia_p),
		b.ds_morfologia)
		into
		ds_retorno_w
		from 
		CIDO_MORFOLOGIA b
		where cd_morfologia = cd_morfologia_p;
	end if;
	return ds_retorno_w;
end Obter_Desc_Morfologia;
/