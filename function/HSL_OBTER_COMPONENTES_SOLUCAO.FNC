create or replace
function hsl_obter_componentes_solucao(	nr_prescricao_p number, 
					nr_seq_solucao_p number) 
					return varchar2 is
ds_resultado				varchar2(1000);
ds_material_w  				varchar2(60);
qt_dose_w					varchar2(10);
cd_unidade_medida_dose_w	varchar2(30);

cursor C01 is
	select	substr(ds_reduzida,1,50), 
		qt_dose,
		cd_unidade_medida_dose 
	from	material c,
		prescr_material p
	where	p.nr_prescricao		= nr_prescricao_p
	and	p.nr_sequencia_solucao	= nr_seq_solucao_p
	and	p.cd_material		= c.cd_material;

begin
	open C01;
	loop
	fetch C01 into  
		ds_material_w, qt_dose_w, cd_unidade_medida_dose_w;
	exit when C01%notfound;
		begin
		ds_resultado := ds_resultado || rpad(trim(rpad(ds_material_w, 39)) || ' - ' || qt_dose_w || ' ' || cd_unidade_medida_dose_w,50); 
		end;
	end loop;
	close C01;
return(ds_resultado);
end hsl_obter_componentes_solucao;
/