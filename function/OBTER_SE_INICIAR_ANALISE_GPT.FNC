create or replace
function obter_se_iniciar_analise_gpt(	nr_atendimento_p	number,
										cd_pessoa_fisica_p	varchar2) 
					return varchar2 is

qt_prescricao_w			pls_integer;
ie_retorno_w			varchar2(1 char);
dt_inicio_prescricao_w	date;
					
begin

qt_prescricao_w	 		:= 0;
ie_retorno_w	 		:= 'N';
dt_inicio_prescricao_w 	:= (sysdate - 7);

select	count(*)
into	qt_prescricao_w
from	prescr_medica
where	nr_atendimento 		= nr_atendimento_p
and		cd_pessoa_fisica 	= cd_pessoa_fisica_p
and		dt_liberacao_medico is not null 
and		dt_liberacao is null
and		dt_inicio_analise_farm is null
and		nm_usuario_analise_farm is null
and 	dt_prescricao > dt_inicio_prescricao_w;

if 	(qt_prescricao_w = 0) then
	
	select	count(*)
	into 	qt_prescricao_w
	from	prescr_medica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and		nr_atendimento is null
	and		dt_liberacao_medico is not null 
	and		dt_liberacao is null
	and		dt_inicio_analise_farm is null
	and		nm_usuario_analise_farm is null
	and 	dt_prescricao > dt_inicio_prescricao_w;
	
end if;

if 	(qt_prescricao_w > 0) then
	ie_retorno_w	:= 'S';
end if;

return ie_retorno_w;

end obter_se_iniciar_analise_gpt;
/