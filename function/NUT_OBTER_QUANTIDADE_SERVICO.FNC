create or replace
function nut_obter_quantidade_servico(	cd_setor_atendimento_p		number,
					dt_inicio_p			date,
					dt_final_p			date)
 		    	return Number is

quant_servico_w		number(10) := 0;
				
begin
if	((nvl(cd_setor_atendimento_p,0) > 0)) then
	select	 count(*) 
	into	 quant_servico_w	
	from	 nut_atend_serv_dia a 	
	where	 cd_setor_atendimento = cd_setor_atendimento_p
	and	 dt_servico between trunc(dt_inicio_p) and fim_dia(dt_final_p)
	and	 dt_liberacao is null
	and      substr(nut_obter_se_tem_jejum(a.nr_sequencia), 1, 1) = 'N';	
end if;

return	quant_servico_w;

end nut_obter_quantidade_servico;
/