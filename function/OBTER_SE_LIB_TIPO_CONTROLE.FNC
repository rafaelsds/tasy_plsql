create or replace
function obter_se_lib_tipo_controle(	cd_tipo_controle_p	number,
				cd_perfil_p		number)
				return varchar2 is

ie_libera_w	varchar2(1);
begin

select	decode(count(*),0,'S','N')
into	ie_libera_w
from	controle_pessoa_perfil
where	cd_tipo_controle = cd_tipo_controle_p;

if	(ie_libera_w = 'N') then
	select	decode(count(*),0,'N','S')
	into	ie_libera_w
	from	controle_pessoa_perfil
	where	nvl(cd_tipo_controle,cd_tipo_controle_p) 	= cd_tipo_controle_p
	and	nvl(cd_perfil,cd_perfil_p) 		= cd_perfil_p;
end if;

return	ie_libera_w;

end	obter_se_lib_tipo_controle;
/