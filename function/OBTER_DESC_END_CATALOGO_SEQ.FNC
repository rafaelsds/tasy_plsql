create or replace 
function obter_desc_end_catalogo_seq(
							cd_endereco_catalogo_p	varchar2,
							nr_seq_catalogo_p 		number,
							ie_informacao_p			varchar2) return varchar2 is

retorno_w			end_endereco.ds_endereco%type;										  
										  
begin
										  
select	max(a.ds_endereco)
into 	retorno_w
from	end_endereco a
where	a.cd_endereco_catalogo = cd_endereco_catalogo_p
and		a.ie_informacao = ie_informacao_p
and 	nr_seq_catalogo = nr_seq_catalogo_p;

return retorno_w;

end obter_desc_end_catalogo_seq;
/