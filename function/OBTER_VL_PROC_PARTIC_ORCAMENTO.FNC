create or replace
function obter_vl_proc_partic_orcamento	(cd_procedimento_p	number,
					ie_origem_proced_p	number,
					ie_tipo_atendimento_p	number,
					cd_medico_p		varchar2,
					ie_clinica_p		number,
					nr_seq_proc_interno_p	number,
					nr_seq_exame_p		number,
					nr_seq_orcamento_p	number,
					nm_usuario_P		varchar2,
					nr_seq_proc_orc_p   	number )
					return Number is

cd_convenio_w		orcamento_paciente.cd_convenio%type;
cd_categoria_w		orcamento_paciente.cd_categoria%type;
vl_procedimento_w	Number(15,2);
vl_Retorno_w		Number(15,2);
vl_lanc_automatico_w	Number(15,2);
cd_estabelecimento_w	number(4,0);
ie_classsif_proced_w	varchar2(10);



ie_calculo_valor_w	orcamento_paciente.ie_calculo_valor%type := '';
ie_regra_plano_w	orcamento_paciente_proc.ie_regra_plano%type := '';
ie_regra_w		orcamento_paciente_proc.ie_regra_plano%type := '';

cd_convenio_particular_w	parametro_faturamento.cd_convenio_partic%type;
cd_categoria_particular_w	parametro_faturamento.cd_categoria_partic%type;
nr_seq_proc_interno_w		orcamento_paciente_proc.nr_seq_proc_interno%type;
ds_erro_w			varchar2(2000 char)	:= null;
nr_seq_regra_retorno_w		regra_convenio_plano.nr_sequencia%type;
ie_glosa_w			varchar2(1 char);
nr_seq_regra_preco_w		number(10,0);
cd_tipo_acomodacao_w		orcamento_paciente.cd_tipo_acomodacao%type;
cd_procedimento_w		orcamento_paciente_proc.cd_procedimento%type;
ie_origem_proced_w              orcamento_paciente_proc.ie_origem_proced%type;
ie_tipo_atendimento_w		orcamento_paciente.ie_tipo_atendimento%type;
nr_atendimento_w		orcamento_paciente.nr_atendimento%type;
dt_orcamento_w			orcamento_paciente.dt_orcamento%type;
cd_plano_w			orcamento_paciente.cd_plano%type;
cd_pessoa_fisica_w		orcamento_paciente.cd_pessoa_fisica%type;
cd_proced_convertido_w		orcamento_paciente_proc.cd_procedimento%type;
ie_origem_proced_convert_w	orcamento_paciente_proc.ie_origem_proced%type;
qt_procedimento_w		orcamento_paciente_proc.qt_procedimento%type;
cd_setor_atendimento_w		orcamento_paciente_proc.cd_setor_atendimento%type;
nr_seq_exame_w			orcamento_paciente_proc.nr_seq_exame%type;
cd_medico_w			orcamento_paciente_proc.cd_medico%type;
cd_exame_w 			exame_laboratorio.cd_exame%type;

begin

begin
select  ie_calculo_valor,
	nr_atendimento,
	cd_convenio,
	cd_categoria,
	dt_orcamento,
	cd_plano,
	cd_pessoa_fisica,
	cd_tipo_acomodacao,
	ie_tipo_atendimento
into	ie_calculo_valor_w,
	nr_atendimento_w,
	cd_convenio_w,
	cd_categoria_w,
	dt_orcamento_w,
	cd_plano_w,
	cd_pessoa_fisica_w,
	cd_tipo_acomodacao_w,
	ie_tipo_atendimento_w
from	orcamento_paciente
where	nr_sequencia_orcamento = nr_seq_orcamento_p;
exception when others then
	ie_calculo_valor_w := '';
end;

if (ie_calculo_valor_w <> 'B') then
	vl_retorno_w := obter_valor_proc_partic_orc2(
			cd_procedimento_p,
			ie_origem_proced_p,
			ie_tipo_atendimento_p,
			cd_medico_p,
			ie_clinica_p,
			nr_seq_proc_interno_p,
			nr_seq_exame_p,
			nr_seq_orcamento_p,
			nm_usuario_p );
	return vl_retorno_w;
end if;


select ie_regra_plano,
       nr_seq_proc_interno,
       qt_procedimento,
       cd_setor_atendimento,
       nr_seq_exame,
       cd_medico,
       cd_procedimento,
       ie_origem_proced
into   ie_regra_plano_w,
       nr_seq_proc_interno_w,
       qt_procedimento_w,
       cd_setor_atendimento_w,
       nr_seq_exame_w,
       cd_medico_w,
       cd_procedimento_w,
       ie_origem_proced_w
from   orcamento_paciente_proc
where  nr_sequencia = nr_seq_proc_orc_p;

select	wheb_usuario_pck.get_cd_estabelecimento
into	cd_estabelecimento_w
from 	dual;

if	(nvl(ie_regra_plano_w, 0) = 0) then
	/*Cadastro do convenio\Regras\Regra plano proced material\Procedimento */
	consiste_plano_convenio(nr_atendimento_w,
				cd_convenio_w,
				cd_procedimento_p,
				ie_origem_proced_p,
				dt_orcamento_w,
				qt_procedimento_w,
				null,
				cd_plano_w,
				null,
				ds_erro_w, -- out
				cd_setor_atendimento_w,
				nvl(nr_seq_exame_w,0),
				ie_regra_plano_w, -- out
				null,
				nr_seq_regra_retorno_w, -- out
				nr_seq_proc_interno_w,
				cd_categoria_w,
				cd_estabelecimento_w,
				null,
				cd_medico_w,
				cd_pessoa_fisica_w,
				ie_glosa_w, --out
				nr_seq_regra_preco_w ); --out
end if;


/* pegar o convenio particular do estabelecimento */
obter_convenio_particular(cd_estabelecimento_w,
			cd_convenio_particular_w,
			cd_categoria_particular_w );

/*  	ie_regra_plano_w:
	1 - Bloqueio atendimento,
	2 - Bloqueio execucao,
	5 - Bloqueia prescricao,
	8 - Sem cobertura/glosa particular
*/
if	(( ie_regra_plano_w in (1,2,5,8)) or ( ie_regra_plano_w = 0)) then
	
	if	( nr_seq_proc_interno_w is not null) then
	
		obter_proc_tab_interno_conv(	nr_seq_proc_interno_w,
						cd_estabelecimento_w,
						nvl(cd_convenio_particular_w, cd_convenio_w), 
						nvl(cd_categoria_particular_w, cd_categoria_w), 
						cd_plano_w, 
						cd_setor_atendimento_w,
						cd_proced_convertido_w, -- out
						ie_origem_proced_convert_w, --out
						cd_setor_atendimento_w,
						dt_orcamento_w,
						cd_tipo_acomodacao_w, null, null, null, null, null, null, null);

		cd_procedimento_w	:= cd_proced_convertido_w;
		ie_origem_proced_w	:= ie_origem_proced_convert_w;
		
		
	elsif	( nr_seq_exame_w is not null) then
	
		select	max(cd_exame)
		into	cd_exame_w
		from 	exame_laboratorio
		where 	nr_seq_exame = nr_seq_exame_w;

		Obter_Proc_Exame_Laborat(cd_exame_w,
					 nvl(cd_convenio_particular_w, cd_convenio_w),
					 cd_estabelecimento_w,
					 nvl(cd_categoria_particular_w, cd_categoria_w),
					 cd_procedimento_w, --out
					 ie_origem_proced_w, --out
					 nr_seq_exame_w, --out
					 0);
	end if;

end if;

ie_classsif_proced_w := obter_classificacao_proced(cd_procedimento_w,ie_origem_proced_w,'C');

if (ie_classsif_proced_w <> 1) then
	nr_seq_proc_interno_w := null;
end if;

select	obter_preco_proced(	cd_estabelecimento_w,
				nvl(cd_convenio_particular_w,cd_convenio_w),
				nvl(cd_categoria_particular_w,cd_categoria_w),
				sysdate,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				ie_tipo_atendimento_w,
				null,
				cd_medico_w,
				null,
				null,
				null,
				null, --ie_clinica_p,
				null,
				'P',
				nr_seq_exame_w,
				nr_seq_proc_interno_w)
into	vl_procedimento_w
from	dual; /*pega o preco do procedimento cadastrado para particular*/

		
vl_lanc_automatico_w:= 0;
begin
select  obter_valor_regra_lanc_aut2(	cd_estabelecimento_w,
					nvl(cd_convenio_particular_w,cd_convenio_w),
					nvl(cd_categoria_particular_w,cd_categoria_w),
					cd_procedimento_w,
					ie_origem_proced_w, 
					nr_seq_proc_interno_w,
					34, 
					nr_seq_exame_w,
					'T', 
					nr_seq_orcamento_p,
					nm_usuario_p)
into    vl_lanc_automatico_w
from    dual;
exception
when others then
        vl_lanc_automatico_w    := 0;
end;

vl_retorno_w	:= vl_procedimento_w + vl_lanc_automatico_w;

return	vl_Retorno_w;

end obter_vl_proc_partic_orcamento;
/
