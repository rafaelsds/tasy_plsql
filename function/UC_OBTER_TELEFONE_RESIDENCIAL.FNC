create or replace
function uc_obter_telefone_residencial(	cd_pessoa_fisica_p		number)
					return varchar2 is

ds_retorno_w	varchar2(25);
ds_telefone_w	varchar2(25);

begin
select	max(p.nr_telefone)
into	ds_telefone_w
from	compl_pessoa_fisica p
where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
and	p.ie_tipo_complemento = 1;

ds_retorno_w := substr(ds_telefone_w,-8,4) || '-' || substr(ds_telefone_w,-4,4);

select	max(p.nr_ddd_telefone)
into	ds_telefone_w
from	compl_pessoa_fisica p
where	p.cd_pessoa_fisica = cd_pessoa_fisica_p
and	p.ie_tipo_complemento = 1;

ds_retorno_w := '(' || ds_telefone_w ||') ' || ds_retorno_w;

return	ds_retorno_w;

end 	uc_obter_telefone_residencial;
/