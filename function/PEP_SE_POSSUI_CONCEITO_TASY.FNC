create or replace
function pep_se_possui_conceito_tasy(cd_node_p		varchar2)
			              return varchar2 is

ds_retorno_w		varchar2(1) := 'X';
ie_possui_elemento_w	varchar2(1) := 'N';
nr_seq_conceito_w		ehr_elemento_conceito.nr_seq_conceito%type;

cursor c01 is
select  a.nr_seq_conceito
from 	ehr_elemento_conceito a,
	 conceito_elemento_tasy b
where a.nr_seq_conceito = b.nr_sequencia
and	  a.cd_node = cd_node_p;

begin

if	(cd_node_p is not null) then

	open c01;
	loop
		fetch c01 into	nr_seq_conceito_w;
		exit when c01%notfound;
		
		ds_retorno_w := 'S';
		
		select nvl(max('S'),'N')
		into   ie_possui_elemento_w
		from   conceito_elemento_attrib
		where  nr_seq_conceito = nr_seq_conceito_w;
	
		if (nvl(ie_possui_elemento_w,'N') = 'S') then
			return 'E';
		end if;

	end loop;
	close c01;
	

end if;
return	ds_retorno_w;

end pep_se_possui_conceito_tasy;
/