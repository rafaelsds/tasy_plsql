create or replace
function TISS_OBTER_REGIME_INTERNACAO
	(cd_procedencia_p	in	number) return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	nvl(max(ie_regime_internacao),'1')
into	ds_retorno_w
from	TISS_REGIME_INTERNACAO
where	cd_procedencia	= cd_procedencia_p;

return ds_retorno_w;

end TISS_OBTER_REGIME_INTERNACAO;
/
