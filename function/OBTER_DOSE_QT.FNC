create or replace 
FUNCTION Obter_dose_qt(
			cd_material_p		Number,
			qt_dose_p		Number,
			cd_unidade_medida_p	Varchar2,
			nr_seq_ordem_p		Number,
			nr_seq_lote_fornec_p	number)
			return number is

nr_seq_cabine_w		Number(10);
qt_dose_w		Number(18,6);
qt_estoque_w		Number(18,6);
qt_dose_real_w		Number(18,6);
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;


BEGIN

select	b.nr_seq_cabine
into	nr_seq_cabine_w
from	far_etapa_producao b,
	can_ordem_prod a
where	a.nr_sequencia = nr_seq_ordem_p
and	b.nr_sequencia = a.nr_seq_etapa_prod;

select	decode(upper(cd_unidade_medida_p),
		upper(substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30)), 
			qt_dose_p, 
				qt_dose_p * obter_conversao_unid_med(cd_material_p,upper(substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30))))
into	qt_dose_w
from	material
where	cd_material = cd_material_p;

if	( nr_seq_lote_fornec_p is not null) then 
	select	nvl(sum(qt_estoque),0)
	into	qt_estoque_w
	from	far_estoque_cabine
	where	nr_seq_cabine	   = nr_seq_cabine_w
	and 	nr_seq_lote_fornec = nr_seq_lote_fornec_p;
else 
	select	nvl(sum(qt_estoque),0)
	into	qt_estoque_w
	from	far_estoque_cabine
	where	nr_seq_cabine	   = nr_seq_cabine_w
	and	cd_material	= cd_material_p;
end if;

select	nvl(sum(qt_dose_real),0)
into	qt_dose_real_w
from	can_ordem_prod_mat
where	nr_seq_ordem	= nr_seq_ordem_p
and	cd_material	= cd_material_p;

qt_dose_w	:= qt_dose_w - qt_dose_real_w;

if	(qt_dose_w > qt_estoque_w) then
	qt_dose_w	:= qt_estoque_w; 
end if;

if	(qt_dose_w < 0) then
	qt_dose_w	:= 0;
end if;

return	qt_dose_w;

END Obter_dose_qt;
/
