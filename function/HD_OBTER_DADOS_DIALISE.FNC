create or replace
function hd_obter_dados_dialise	(	nr_seq_dialise_p	number,
					ie_opcao_p		varchar2)
					return			varchar2 is
					
nr_seq_paciente_w				number(10,0);					
nr_seq_unid_dialise_w				number(10,0);
nr_seq_dialisador_ativo_w			number(10,0);
nr_seq_dialise_dialis_ativo_w			number(10,0);
nr_seq_ponto_acesso_w				number(10,0);
nr_seq_maquina_w				number(10,0);
qt_pa_diast_pre_deitado_w			number(3,0);
qt_pa_diast_pre_pe_w				number(3,0);
qt_pa_sist_pre_deitado_w			number(3,0);
qt_pa_sist_pre_pe_w				number(3,0);
qt_peso_pre_w					number(6,3);
qt_peso_pos_w					number(6,3);
dt_dialise_w					date;
nm_pessoa_fisica_w				varchar2(90);
cd_pessoa_fisica_w				varchar2(10);
nr_atendimento_w				number(10);
ie_tipo_dialise_w				varchar2(15);

ds_retorno_w					varchar2(255);

begin

/* obtem dados da di�lise */
begin
select	nvl(nr_seq_paciente,0),
	nvl(nr_seq_unid_dialise,0),
	qt_pa_diast_pre_deitado,
	qt_pa_diast_pre_pe,
	qt_pa_sist_pre_deitado,
	qt_pa_sist_pre_pe,
	qt_peso_pre,
	qt_peso_pos,
	dt_dialise,
	substr(obter_nome_pf(cd_pessoa_fisica),1,90),
	cd_pessoa_fisica,
	nr_atendimento,
	ie_tipo_dialise
into	nr_seq_paciente_w,
	nr_seq_unid_dialise_w,
	qt_pa_diast_pre_deitado_w,
	qt_pa_diast_pre_pe_w,
	qt_pa_sist_pre_deitado_w,
	qt_pa_sist_pre_pe_w,
	qt_peso_pre_w,
	qt_peso_pos_w,
	dt_dialise_w,
	nm_pessoa_fisica_w,
	cd_pessoa_fisica_w,
	nr_atendimento_w,
	ie_tipo_dialise_w
from	hd_dialise
where	nr_sequencia		= nr_seq_dialise_p;
exception
	when others then
	nr_seq_paciente_w	:= 0;
end;

/* obtem dados do dialisador da di�lise */
begin
select	max(nr_seq_dialisador),
	max(nr_sequencia),
	max(nr_seq_ponto_acesso),
	max(nr_seq_maquina)
into	nr_seq_dialisador_ativo_w,
	nr_seq_dialise_dialis_ativo_w,
	nr_seq_ponto_acesso_w,
	nr_seq_maquina_w	
from	hd_dialise_dialisador
where	nr_seq_dialise		= nr_seq_dialise_p
and	dt_retirada is null;	
exception
	when others then
	nr_seq_dialise_dialis_ativo_w	:= 0;		
end;
	
/* retornos*/
if	(ie_opcao_p = 'SP') then
	ds_retorno_w	:= nvl(nr_seq_paciente_w,0);
elsif	(ie_opcao_p = 'SU') then
	ds_retorno_w	:= nvl(nr_seq_unid_dialise_w,0);
elsif	(ie_opcao_p = 'DA') then
	ds_retorno_w	:= nvl(nr_seq_dialisador_ativo_w,0);
elsif	(ie_opcao_p = 'DS') then
	ds_retorno_w	:= nvl(nr_seq_dialise_dialis_ativo_w,0);
elsif	(ie_opcao_p = 'DP') then
	ds_retorno_w	:= nvl(nr_seq_ponto_acesso_w,0);
elsif	(ie_opcao_p = 'DM') then
	ds_retorno_w	:= nvl(nr_seq_maquina_w,0);
elsif	(ie_opcao_p = 'PDD') then
	ds_retorno_w	:= qt_pa_diast_pre_deitado_w;
elsif	(ie_opcao_p = 'PSD') then
	ds_retorno_w	:= qt_pa_sist_pre_deitado_w;
elsif	(ie_opcao_p = 'PDP') then
	ds_retorno_w	:= qt_pa_diast_pre_pe_w;
elsif	(ie_opcao_p = 'PSP') then
	ds_retorno_w	:= qt_pa_sist_pre_pe_w;
elsif	(ie_opcao_p = 'PP') then
	ds_retorno_w	:= qt_peso_pre_w;
elsif	(ie_opcao_p = 'PPS') then
	ds_retorno_w	:= qt_peso_pos_w;	
elsif	(ie_opcao_p = 'NP') then
	ds_retorno_w	:= nm_pessoa_fisica_w;
elsif	(ie_opcao_p = 'PF') then
	ds_retorno_w	:= cd_pessoa_fisica_w;
elsif	(ie_opcao_p = 'AT') then
	ds_retorno_w	:= nr_atendimento_w;	
elsif	(ie_opcao_p = 'TD') then
	ds_retorno_w	:= ie_tipo_dialise_w;	
end if;


return ds_retorno_w;

end hd_obter_dados_dialise;
/
