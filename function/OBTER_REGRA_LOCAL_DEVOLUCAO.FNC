create or replace
function obter_regra_local_devolucao(
				cd_material_p		number,
				cd_estabelecimento_p	number,
				cd_setor_atendimento_p	number)
				return number is

cd_grupo_material_w		number(3);
cd_subgrupo_material_w		number(3);
cd_classe_material_w		number(5);
qt_regra_w			number(5);
cd_local_estoque_w		number(4);
nr_seq_familia_w		number(10);

cursor c01 is
select	cd_local_estoque
from	regra_local_devolucao
where	cd_estabelecimento					= cd_estabelecimento_p
and	((nvl(cd_setor_atendimento, cd_setor_atendimento_p)	= cd_setor_atendimento_p) or (cd_setor_atendimento_p is null))
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(cd_material, cd_material_p) = cd_material_p or cd_material_p = 0)
and	((nvl(nr_seq_familia, nr_seq_familia_w)			= nr_seq_familia_w) or	 (nr_seq_familia is null))
and	((nvl(ie_considerar_padrao_est,'N') = 'N' or obter_se_mat_existe_padrao_loc(cd_material_p,cd_local_estoque) = 'S') or
	(nvl(ie_considerar_controlador,'N') = 'S' and obter_se_mat_existe_padrao_loc((	select	x.cd_material_estoque 
											from	material x
											where	x.cd_material = cd_material_p),cd_local_estoque) = 'S'))
order by nvl(nr_prioridade,999) desc,
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);

begin

select	count(*)
into	qt_regra_w
from	regra_local_devolucao;

if	(qt_regra_w > 0) then
	begin
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		nr_seq_familia
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		nr_seq_familia_w
	from	estrutura_material_v
	where	cd_material = cd_material_p;
	
	open c01;
	loop
	fetch c01 into
		cd_local_estoque_w;
	exit when c01%notfound;
	end loop;
	close c01;
       	end;
end if;

return	cd_local_estoque_w;

end obter_regra_local_devolucao;
/