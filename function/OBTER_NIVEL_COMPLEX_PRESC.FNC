create or replace function obter_nivel_complex_presc(
	nr_seq_prescricao_p number)
return varchar2 is 

ds_nivel_complexidade_w varchar2(10);
ie_dominio_w number;

begin
	ie_dominio_w := 8694;
	select	obter_descricao_dominio(ie_dominio_w, nvl(max(ie_nivel_compl), 0)) 
	into 	ds_nivel_complexidade_w 
	from 	paciente_compl_assist
	where 	nr_sequencia = nr_seq_prescricao_p;
	
	return	ds_nivel_complexidade_w;
	
end obter_nivel_complex_presc;
/
