create or replace
function ptu_obter_proced_conversao
			(	cd_procedimento_p	in	Number,
				ie_origem_proced_p	in	Number,
				cd_prestador_p		in	Number,
				cd_unimed_p		in	varchar2)
				return varchar2 is

cd_retorno_w			varchar2(15);
ie_origem_conversao_w		Number(10);
nr_seq_regra_w			Number(10);
nr_seq_prestador_w		number(10);
cd_estabelecimento_w		number(10);
nr_seq_congenere_w		number(10);
sg_estado_w			pessoa_juridica.sg_estado%type;
sg_estado_int_w			pessoa_juridica.sg_estado%type;
ie_tipo_intercambio_w		pls_conversao_proc.ie_tipo_intercambio%type;
ie_somente_codigo_w		pls_conversao_proc.ie_somente_codigo%type;

begin
begin
select	nr_sequencia,
	cd_estabelecimento
into	nr_seq_prestador_w,
	cd_estabelecimento_w
from	pls_prestador
where	cd_prestador	= cd_prestador_p;
exception
when others then
	nr_seq_prestador_w	:= null;
end;

begin
	select	max(nr_sequencia)
	into	nr_seq_congenere_w
	from	pls_congenere
	where	somente_numero(cd_cooperativa)	= somente_numero(cd_unimed_p);
exception
when others then
	nr_seq_congenere_w	:= null;
end;

ie_tipo_intercambio_w	:= 'A';
	
if	(nr_seq_congenere_w is not null) then
	-- Obter a UF da operadora  - Tasy
	select	nvl(max(sg_estado),'X')
	into	sg_estado_w
	from	pessoa_juridica
	where	cd_cgc	=	(select	max(cd_cgc_outorgante)
				from	pls_outorgante
				where	cd_estabelecimento	= cd_estabelecimento_w);
	
	-- Obter a UF da operadora do beneficiário eventual ou que enviou o protocolo
	select	nvl(max(a.sg_estado),'X')
	into	sg_estado_int_w
	from	pessoa_juridica	a,
		pls_congenere	b
	where	a.cd_cgc	= b.cd_cgc
	and	b.nr_sequencia	= nr_seq_congenere_w;
	
	if	(sg_estado_w <> 'X') and
		(sg_estado_int_w <> 'X') then
		if	(sg_estado_w	= sg_estado_int_w) then
			ie_tipo_intercambio_w	:= 'E';
		else	
			ie_tipo_intercambio_w	:= 'N';
		end if;
	else
		ie_tipo_intercambio_w	:= 'A';
	end if;
end if;	

if (nr_seq_prestador_w is not null) then
	pls_obter_proced_conversao(	cd_procedimento_p, ie_origem_proced_p, nr_seq_prestador_w,
					cd_estabelecimento_w, 1, nr_seq_congenere_w, 
					null, null, null, 
					null, null, null,
					ie_tipo_intercambio_w, cd_retorno_w, ie_origem_conversao_w, 
					nr_seq_regra_w, ie_somente_codigo_w, sysdate,
					null, null, null);
end if;
				
if (cd_retorno_w is null) then
	cd_retorno_w := cd_procedimento_p;
end if;

return	cd_retorno_w;

end ptu_obter_proced_conversao;
/