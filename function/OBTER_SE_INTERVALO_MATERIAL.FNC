create or replace
function Obter_se_intervalo_material(	cd_material_p	number,
					cd_intervalo_p	varchar2)
 		    	return varchar2 is

ie_exibir_w	varchar2(1) := 'S';
cont_w		number(10,0);

begin

select	count(*)
into	cont_w
from	lib_material_intervalo
where	cd_material	= cd_material_p
and	nvl(ie_tipo,'I') = 'I';

if	(cont_w	> 0) then
	select	count(*)
	into	cont_w
	from	lib_material_intervalo
	where	cd_material	= cd_material_p
	and	cd_intervalo	= cd_intervalo_p
	and	nvl(ie_tipo,'I') = 'I';
	
	if	(cont_w = 0) then
		ie_exibir_w	:= 'N';
	end if;
end if;

return	ie_exibir_w;

end Obter_se_intervalo_material;
/
