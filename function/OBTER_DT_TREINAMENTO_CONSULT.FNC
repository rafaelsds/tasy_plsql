create or replace
function obter_dt_treinamento_consult(	cd_funcao_p		number,
					cd_consultor_p		varchar2)
 		    	return date is

			
ds_retorno_w		date;
			
begin

select	to_char(max(c.dt_inicio), 'dd/mm/yyyy')dt_inicio
into	ds_retorno_w
from	tre_inscrito a,
	tre_inscrito_presenca b,
	tre_evento_modulo c,
	tre_curso_modulo d
where	a.nr_sequencia 		= b.nr_seq_inscrito
and	a.nr_seq_mod_evento 	= c.nr_sequencia
and	c.nr_seq_modulo 	= d.nr_sequencia
and	a.cd_pessoa_fisica 	= cd_consultor_p
AND	d.cd_funcao  		= cd_funcao_p
group by a.cd_pessoa_fisica, 
	 c.nr_seq_modulo;


return	ds_retorno_w;

end obter_dt_treinamento_consult;
/