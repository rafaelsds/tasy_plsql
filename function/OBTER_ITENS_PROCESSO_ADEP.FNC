create or replace
function obter_itens_processo_adep	(nr_atendimento_p	number,
					nr_seq_processo_p	number)
					return varchar2 is

vl_retorno_w	varchar2(4000);
dt_leitura_w	date;
dt_preparo_w	date;
ds_material_w	varchar2(100);

cursor c01 is
select	substr(obter_dados_horario(nr_seq_horario,'DI'),1,100) ds_material
from	adep_processo_item
where	nr_seq_processo = nr_seq_processo_p
order by
	ds_material;

begin
if	(nr_atendimento_p is not null) and
	(nr_seq_processo_p is not null) then

	select	decode(count(*), 0, 'N', 'S')
	into	vl_retorno_w
	from	adep_processo
	where	nr_sequencia = nr_seq_processo_p;

	if	(vl_retorno_w = 'S') then

		select	validar_processo_atendimento(nr_atendimento_p, nr_seq_processo_p)
		into	vl_retorno_w
		from	dual;

		if	(vl_retorno_w = 'S') then
	
			vl_retorno_w := null;

			select	dt_leitura,
				dt_preparo
			into	dt_leitura_w,
				dt_preparo_w
			from	adep_processo
			where	nr_sequencia = nr_seq_processo_p;

			if	(dt_leitura_w is null) then

				vl_retorno_w := '#@Este processo ainda n�o foi lido por barras.';

			elsif	(dt_preparo_w is null) then

				vl_retorno_w := '#@Este processo ainda n�o foi preparado.';

			end if;

			if	(vl_retorno_w is null) then

				open c01;
				loop
				fetch c01 into ds_material_w;
				exit when c01%notfound;
					begin
					vl_retorno_w := vl_retorno_w || ds_material_w || chr(10);
					end;
				end loop;
				close c01;
			
			end if;

		else
	
			vl_retorno_w := '#@Este processo n�o pretence a este atendimento.';
			
		end if;

	else
	
		vl_retorno_w := '#@Processo inv�lido.';

	end if;

end if;

return vl_retorno_w;

end obter_itens_processo_adep;
/