create or replace 
function TISS_OBTER_CODIGO_MEDICO
				(cd_estabelecimento_p	in number,
				cd_convenio_p		in number,
				cd_pessoa_fisica_p		in varchar2,
				ie_opcao_p		in varchar2)
				return varchar2 is

/*
ie_opcao_p
'CIM' - C�digo Interno TasyMed
'CPF'
*/

cd_interno_w	varchar2(255) := null;
nr_cpf_w	varchar2(255) := null;
ds_retorno_w	varchar2(255) := null;

begin

select	max(cd_medico_tasymed_tiss)
into	cd_interno_w
from	medico_convenio
where	cd_convenio					= cd_convenio_p
and	cd_pessoa_fisica					= cd_pessoa_fisica_p
and	nvl(cd_estabelecimento,nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0)
and	sysdate between nvl(dt_inicio_vigencia,to_date('01/01/2000','dd/mm/yyyy')) and nvl(dt_final_vigencia,sysdate + 1);

select	max(nr_cpf)
into	nr_cpf_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(ie_opcao_p = 'CIM') then
	ds_retorno_w	:= cd_interno_w;
elsif	(cd_interno_w is not null) then
	ds_retorno_w	:= null;
elsif	(ie_opcao_p = 'CPF') then
	ds_retorno_w	:= nr_cpf_w;
end if;

return	ds_retorno_w;

end TISS_OBTER_CODIGO_MEDICO;
/
