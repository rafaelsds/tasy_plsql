create or replace
function HMCC_OBTER_VALOR_DESCONTO_PROC(
                                        nr_repasse_terceiro_p   number,
                                        cd_procedimento_p       number,
                                        ie_origem_proced_p      number,
                                        cd_medico_p             varchar2,
                                        cd_centro_custo_p       number,
					dt_entrada_p		date default sysdate)
                                        return number is
vl_desconto_w   number(15,2);
cont_w          number(10);

begin

if 	(dt_entrada_p < to_date('01/09/2011')) then

	vl_desconto_w := 0;
else

	select  count(*)
	into    cont_w
	from    procedimento d,
	        setor_atendimento c,
	        procedimento_paciente b,
	        procedimento_repasse a,
		atendimento_paciente e
	where   (d.cd_grupo_proc in (10107,50010) or b.cd_procedimento = 10014)
	and     b.ie_origem_proced              = d.ie_origem_proced
	and	b.nr_atendimento		= e.nr_atendimento
	and     b.cd_procedimento               = d.cd_procedimento
	and     nvl(c.cd_centro_custo,0)        = nvl(cd_centro_custo_p,0)
	and     b.cd_setor_atendimento          = c.cd_setor_atendimento
	and     nvl(b.cd_medico_executor,'X')   = nvl(cd_medico_p,'X')
	and     b.cd_procedimento               <> 10057
	and     a.nr_seq_procedimento           = b.nr_sequencia
	and     a.nr_repasse_terceiro           = nr_repasse_terceiro_p
	and     d.ie_origem_proced              = ie_origem_proced_p
	and     d.cd_procedimento               = cd_procedimento_p
	and     obter_tipo_convenio(b.cd_convenio) in ('1', '2')
	and     c.cd_setor_atendimento          in (76, 174,181,125)
	and     b.cd_especialidade              not in (54,66)
	and	b.cd_motivo_exc_conta		is null
	and	e.dt_entrada			> to_date('01/09/2011','dd/mm/yyyy');
	
	if      (cont_w = 0) then
	        vl_desconto_w   := 0;
	else
	
	        select  count(*)
	        into    cont_w
	        from    procedimento d,
	                setor_atendimento c,
	                procedimento_paciente b,
	                procedimento_repasse a,
			atendimento_paciente e
	        where   (d.cd_grupo_proc in (10107,50010) or b.cd_procedimento = 10014)
	        and     b.ie_origem_proced              = d.ie_origem_proced
		and	b.nr_atendimento		= e.nr_atendimento
	        and     b.cd_procedimento               = d.cd_procedimento
	        and     nvl(c.cd_centro_custo,0)        = nvl(cd_centro_custo_p,0)
	        and     b.cd_setor_atendimento          = c.cd_setor_atendimento
	        and     nvl(b.cd_medico_executor,'X')   = nvl(cd_medico_p,'X')
	        and     b.cd_procedimento               <> 10057
	        and     a.nr_seq_procedimento           = b.nr_sequencia
		and	b.cd_motivo_exc_conta		is null
	        and     a.nr_repasse_terceiro           = nr_repasse_terceiro_p
		and	e.dt_entrada		> to_date('01/09/2011','dd/mm/yyyy');
	
	        if      (cont_w > 0 and cont_w < 121) then
	                vl_desconto_w   := 10;
	        elsif   (cont_w > 120) then
        	        vl_desconto_w   := 8;
	        end if;

	end if;

end if;

return vl_desconto_w;

end HMCC_OBTER_VALOR_DESCONTO_PROC;
/