create or replace
function Obter_Cod_Tiss_MatPaci( Nr_Seq_MatPaci_p	Material_Atend_Paciente.Nr_Sequencia%Type)
 		    	return Varchar2 is

Cd_Retorno_w	Material_Atend_Paciente.Cd_Material_Tiss%Type;
				
Begin
				
	Begin

		Select	Cd_Material_Tiss
		Into	Cd_Retorno_w
		From	Material_Atend_Paciente
		Where	Nr_Sequencia = Nr_Seq_MatPaci_p;

	Exception
		When Others Then
		Cd_Retorno_w:=Null;
	End;

	return	Cd_Retorno_w;

end Obter_Cod_Tiss_MatPaci;
/