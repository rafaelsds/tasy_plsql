create or replace
FUNCTION Obter_Dias_Internacao_aih(
				nr_atendimento_p 	number,
				nr_interno_conta_p	number)
				return number is

qt_dias_internacao_w	number(10) := 0;
dt_entrada_w		date;
dt_alta_w		date;


begin

if	(nr_atendimento_p is not null) then
	select	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_entrada),
		ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(dt_alta, sysdate))
	into	dt_entrada_w,
		dt_alta_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	select	nvl(max(DT_INICIAL),dt_entrada_w),
		nvl(max(DT_FINAL),dt_alta_w)
	into	dt_entrada_w,
		dt_alta_w
	from	sus_aih_unif	
	where	nr_interno_conta	= nr_interno_conta_p;
		
	
	qt_dias_internacao_w := (dt_alta_w - dt_entrada_w) + 1;
end if;

return qt_dias_internacao_w;

end Obter_Dias_Internacao_aih;
/
