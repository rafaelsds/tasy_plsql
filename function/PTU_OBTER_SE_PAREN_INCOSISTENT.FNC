create or replace
function ptu_obter_se_paren_incosistent
			(	nr_seq_empresa_p	number,
				cd_familia_p		number,
				nr_seq_parentesco_p	number)
				return varchar2 is
				
ds_retorno_w			varchar2(10);
nr_seq_regra_cons_paren_w	number(10);
cd_dependencia_w		number(10);
nr_seq_parentesco_w		number(10);
qt_registros_w			number(10);
ie_sexo_w			varchar2(10);
qt_benef_existente_w		number(10) := 0;
qt_beneficiario_w		number(10);
ds_w			varchar2(255);
				
Cursor C01 is
	select	cd_dependencia,
		ie_sexo
	from	ptu_intercambio_benef
	where	nr_seq_empresa	= nr_seq_empresa_p
	and	cd_familia	= cd_familia_p
	and	cd_dependencia	<> 0;

begin
	
ds_retorno_w	:= 'N';

select	max(b.nr_sequencia)
into	nr_seq_regra_cons_paren_w
from	ptu_consist_parentesco	b
where	exists	(	select	1
			from	PTU_CONSIST_GRAU_PAREN	a
			where	a.nr_seq_regra		= b.nr_sequencia
			and	a.nr_seq_parentesco	= nr_seq_parentesco_p);

if	(nr_seq_regra_cons_paren_w is not null) then
	open C01;
	loop
	fetch C01 into	
		cd_dependencia_w,
		ie_sexo_w;
	exit when C01%notfound;
		begin
		
		select	count(*)
		into	qt_registros_w
		from	grau_parentesco
		where	cd_ptu	= cd_dependencia_w;
		
		/*Caso houver mais q 2 cadastro de parentesco do c�digo do PTU, ent�o busca da regra de convers�o*/
		if	(qt_registros_w > 1) then
			nr_seq_parentesco_w	:= ptu_conversao_obter_parentesco(cd_dependencia_w,ie_sexo_w);
		end if;
		
		if	(nr_seq_parentesco_w is null) then
			begin		
			select	max(nr_sequencia)
			into	nr_seq_parentesco_w
			from	grau_parentesco
			where	cd_ptu	= cd_dependencia_w;
			exception
			when others then
				nr_seq_parentesco_w	:= null;
			end;
		end if;
		
		if	(nr_seq_parentesco_w is not null) and
			(nr_seq_parentesco_p = nr_seq_parentesco_w) then
			qt_benef_existente_w	:= qt_benef_existente_w + 1;
		end if;

		nr_seq_parentesco_w	:= null;
		end;
	end loop;
	close C01;
	
	select	qt_beneficiario
	into	qt_beneficiario_w
	from	ptu_consist_parentesco
	where	nr_sequencia	= nr_seq_regra_cons_paren_w;
	
	if	(qt_benef_existente_w > qt_beneficiario_w) then
		ds_retorno_w	:= 'S';
	end if;
end if;

return	ds_retorno_w;

end ptu_obter_se_paren_incosistent;
/
