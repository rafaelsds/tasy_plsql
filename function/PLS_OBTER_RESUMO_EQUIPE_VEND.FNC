create or replace
function pls_obter_resumo_equipe_vend
			(	nr_seq_equipe_p		number,
				dt_mes_competencia_p	date,
				ie_opcao_p		varchar2)
	
				return number is
/*
G - Global
PF - Pessoa Fisica
PJ - Pessoa Juridica
GP - Global pago
*/			
			
qt_vendedores_w		number(10) := 0;	
qt_retorno_w		number(10) := 0;
nr_seq_vendedor_w	number(10);

Cursor C01 is
	select	nr_seq_vendedor
	from	pls_equipe_vend_vinculo
	where	nr_seq_equipe	= nr_seq_equipe_p
	and	dt_mes_competencia_p between nvl(dt_inicio_vigencia,dt_mes_competencia_p) and nvl(dt_fim_vigencia,dt_mes_competencia_p);
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_vendedor_w;
exit when C01%notfound;
	begin
	
	if	(ie_opcao_p = 'G') then
		select	count(*)
		into	qt_vendedores_w
		from	pls_segurado
		where	nr_seq_vendedor_canal	= nr_seq_vendedor_w
		and	trunc(dt_contratacao,'Month')		= dt_mes_competencia_p
		and	dt_liberacao is not null;
	elsif	(ie_opcao_p = 'PF') then
		select	count(*)
		into	qt_vendedores_w
		from	pls_segurado	b,
			pls_contrato	a
		where	b.nr_seq_contrato			= a.nr_sequencia	
		and	b.nr_seq_vendedor_canal			= nr_seq_vendedor_w
		and	a.cd_cgc_estipulante is null
		and	trunc(b.dt_contratacao,'Month')		= dt_mes_competencia_p
		and	b.dt_liberacao is not null;
	elsif	(ie_opcao_p = 'PJ') then
		select	count(*)
		into	qt_vendedores_w
		from	pls_segurado	b,
			pls_contrato	a
		where	b.nr_seq_contrato			= a.nr_sequencia	
		and	b.nr_seq_vendedor_canal			= nr_seq_vendedor_w
		and	a.cd_cgc_estipulante is not null
		and	trunc(b.dt_contratacao,'Month')		= dt_mes_competencia_p
		and	b.dt_liberacao is not null;
	elsif	(ie_opcao_p = 'GP') then
		select	count(*)
		into	qt_vendedores_w
		from	titulo_receber			e,
			pls_mensalidade			d,
			pls_mensalidade_segurado	c,
			pls_segurado			b,
			pls_contrato			a
		where	b.nr_seq_contrato			= a.nr_sequencia
		and	c.nr_seq_segurado			= b.nr_sequencia	
		and	c.nr_seq_mensalidade			= d.nr_sequencia
		and	e.nr_seq_mensalidade			= d.nr_sequencia
		and	b.nr_seq_vendedor_canal			= nr_seq_vendedor_w
		and	c.nr_parcela				= 1
		and	e.ie_situacao				= '2'
		and	trunc(b.dt_contratacao,'Month')		= dt_mes_competencia_p
		and	b.dt_liberacao is not null;
	end if;
		
	qt_retorno_w := qt_retorno_w + qt_vendedores_w;
	
	end;
end loop;
close C01;

return	qt_retorno_w;

end pls_obter_resumo_equipe_vend;
/