Create or Replace function 
		obter_nome_agenda_home_care(nr_sequencia_p	number)
				return varchar2 is

ds_retorno_w	Varchar2(100);

begin

select	nm_agenda
into	ds_retorno_w
from	agenda_home_care
where	nr_sequencia = nr_sequencia_p;

return ds_retorno_w;

end;
/