create or replace
function obter_se_nf_parcial_bord_rec(nr_bordero_rec_p		number)
				      return varchar2 is

ie_finalizar_sem_receb_w	varchar2(1);
count_w				number(10);	
ds_retorno_w			varchar2(1) := 'N';		
			
begin

Obter_Param_Usuario(813, 199,  obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, obter_estabelecimento_ativo, ie_finalizar_sem_receb_w);

if ( nvl(ie_finalizar_sem_receb_w,'N') = 'S' and philips_param_pck.get_cd_pais = 2) then

	select	count(b.nr_titulo) 
	into	count_w
	from	bordero_recebimento a,
		bordero_tit_rec b,
		titulo_receber c,
		conta_paciente d,
		convenio e
	where	a.nr_bordero 	   	 = b.nr_bordero
	and	b.nr_titulo	       	 = c.nr_titulo
	and	c.nr_interno_conta    	 = d.nr_interno_conta
	and	d.cd_convenio_parametro  = e.cd_convenio
	and	e.ie_tipo_convenio 	 = 1
	and	a.nr_bordero 		 = nr_bordero_rec_p;

	if (count_w > 0) then
		ds_retorno_w := 'S';
	end if;
end if;

return	ds_retorno_w;

end obter_se_nf_parcial_bord_rec;
/