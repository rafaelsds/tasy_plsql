create or replace function hrc_obter_conta_estoque(
			cd_material_p		number,
			ie_tipo_conta_p		number,
			dt_vigencia_p		date)
			return varchar2 is

/* -------------- tipo de conta -------
	1 - receita
	2 - estoque
	3 - passagem direta
*/

cd_classe_material_w      	number(5,0)  := 0;
cd_subgrupo_material_w    	number(3,0)  := 0;
cd_grupo_material_w		number(3,0)  := 0;
cd_conta_contabil_w		varchar2(20);
cd_conta_receita_w        	varchar2(40);
cd_conta_estoque_w        	varchar2(40);
cd_conta_passag_direta_w  	varchar2(40);
ie_considera_dt_vig_w		varchar2(1);

cursor c001 is
/* obter contas do material  */
select	cd_conta_receita,
	cd_conta_estoque,
	cd_conta_passag_direta
from 	parametros_conta_contabil
where	nvl(cd_material,cd_material_p) 			= cd_material_p
and	nvl(cd_grupo_material,cd_grupo_material_w)	= cd_grupo_material_w
and	nvl(cd_subgrupo_material,cd_subgrupo_material_w)= cd_subgrupo_material_w
and	nvl(cd_classe_material,cd_classe_material_w)	= cd_classe_material_w
and	(((ie_tipo_conta_p = 1) and (cd_conta_receita is not null)) or
	((ie_tipo_conta_p = 2) and (cd_conta_estoque is not null)) or
	((ie_tipo_conta_p = 3) and (cd_conta_passag_direta is not null)))
and	cd_procedimento					is null
and	cd_grupo_proced					is null
and	cd_especial_proced					is null
and	cd_area_proced					is null
and	nr_seq_forma_org is null
and	nr_seq_grupo is null
and	nr_seq_subgrupo is null
and	(nvl(dt_inicio_vigencia, dt_vigencia_p) <= dt_vigencia_p and nvl(dt_fim_vigencia, dt_vigencia_p) >= dt_vigencia_p)
order by	decode(ie_considera_dt_vig_w,'S',dt_inicio_vigencia,null),
		nvl(cd_convenio,0),
		nvl(ie_classif_convenio,0),
		nvl(cd_setor_atendimento,0),
		nvl(ie_tipo_atendimento,0),
		nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_local_estoque,0);

begin
ie_considera_dt_vig_w := nvl(obter_valor_param_usuario(7050, 20, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo), 'S');
/*      obter estrutura material */
begin
select	cd_classe_material,
		cd_subgrupo_material,
		cd_grupo_material
into		cd_classe_material_w,
		cd_subgrupo_material_w,
		cd_grupo_material_w
from		estrutura_material_v
where		cd_material	 	= cd_material_p;
exception
    		when others then
			cd_classe_material_w 	:= 0;
			cd_subgrupo_material_w 	:= 0;
			cd_grupo_material_w 	:= 0;
end;

open c001;
loop
fetch	c001	into
		cd_conta_receita_w,
		cd_conta_estoque_w,
		cd_conta_passag_direta_w;
exit when c001%notfound;
		begin
			if	(ie_tipo_conta_p = 1) then
				cd_conta_contabil_w	:= cd_conta_receita_w;
			elsif	(ie_tipo_conta_p = 2) then
				cd_conta_contabil_w	:= cd_conta_estoque_w;
			elsif	(ie_tipo_conta_p = 3) then
				cd_conta_contabil_w 	:= cd_conta_passag_direta_w;
			end if;
		end;
end loop;
close c001;
return cd_conta_contabil_w;
end hrc_obter_conta_estoque;
/
