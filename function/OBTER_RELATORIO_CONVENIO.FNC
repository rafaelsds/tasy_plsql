create or replace
function obter_relatorio_convenio (	cd_pessoa_fisica_p	number,
				cd_convenio_p		number) 
				return number is

ie_possui_registro_w	varchar2(1);
cd_relatorio_w		number(10):=0;

begin

select	decode(count(*),0,'N','S')
into	ie_possui_registro_w
from	med_relatorio_convenio
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

if	(ie_possui_registro_w = 'S') then
	select	cd_relatorio
	into	cd_relatorio_w
	from	med_relatorio_convenio
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_p
	and	cd_convenio	= cd_convenio_p;
end if;


return cd_relatorio_w;

end obter_relatorio_convenio;
/