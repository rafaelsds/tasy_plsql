create or replace
function TISS_OBTER_REGRA_CAMPO
		(ie_tipo_guia_p		varchar2,
		nm_atributo_p		varchar2,
		cd_convenio_p		varchar2,
		vl_campo_p		varchar2,
		ie_tipo_atendimento_p	number,
		cd_categoria_conv_p	varchar2,
		ie_tipo_despesa_p	varchar2,
		vl_max_campo_p		number,
		cd_estabelecimento_p	number,
		ie_clinica_p		number,
		nr_seq_classificacao_p	number,
		cd_setor_atendimento_p	number,
		ds_parametros_p		varchar2,
		cd_setor_execucao_p	number default null)
	return varchar2 is

/*
ESTA FUNCTION E UTILIZADA TAMBEM NOS PROJETOS XML (SOLICITACAO PROCEDIMENTO -> DIAGNOSTICO)
*/

ie_envio_w			varchar2(4000);
ds_padrao_w			varchar2(4000);
vl_permitido_w			number(10,2);
ds_retorno_w			varchar2(4000);
ds_temp_w			varchar2(4000);
dt_retorno_aux_w		date;
ds_valor_gerado_w		varchar2(4000);
ds_conteudo_w			varchar2(4000);
vl_atributo_w			varchar2(255);
ie_tipo_atend_tiss_w		varchar2(255);
ie_tipo_atend_tiss_proc_w	varchar2(255);
ds_mascara_w			varchar2(255);
qt_digitos_mascara_w		number(10);
qt_digitos_valor_w		number(10);
qt_registros_vetor_w		number(15);
k				Integer;
cd_perfil_w			number(5,0) := wheb_usuario_pck.get_cd_perfil;

ds_versao_tiss_w		varchar2(20);
nm_atributo_w			varchar2(255);
qt_regra_w			number(10);
qt_existentes_w			number(5) := 0;
qt_geradas_w			number(5) := 0;
cd_convenio_w			convenio.cd_convenio%type := to_number(cd_convenio_p);
cursor c01 is
select	nvl(a.ie_envio, 'E'),
	a.ds_padrao,
	a.vl_max_campo,
	a.ds_valor_gerado,
	a.ds_mascara
from	tiss_lista_campo b,
	tiss_regra_campo_conv a
where	a.nr_seq_campo		= b.nr_sequencia
and	a.cd_convenio		= cd_convenio_p
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	b.ie_tiss_tipo_guia	= ie_tipo_guia_p
and	b.nm_atributo		= nm_atributo_w
and	((a.ie_taxas = 'N') or
	(a.ie_taxas = 'S' and ie_tipo_despesa_p in ('4','5'))) 		-- Edgar 29/09/2007, Tratar se a regra acima deve ser aplicada para as taxas
and	((a.ie_gases = 'S') or
	((a.ie_gases = 'N') and (ie_tipo_despesa_p <> '1')))
and	((a.ie_medicamento = 'S') or
	((a.ie_medicamento = 'N') and (ie_tipo_despesa_p <> '2')))
and	((a.ie_material = 'S') or
	((a.ie_material = 'N') and (ie_tipo_despesa_p <> '3')))
and	((a.ie_taxas_diversas = 'S') or
	((a.ie_taxas_diversas = 'N') and (ie_tipo_despesa_p <> '4')))
and	((a.ie_diarias = 'S') or
	((a.ie_diarias = 'N') and (ie_tipo_despesa_p <> '5')))
and	((a.ie_alugueis = 'S') or
	((a.ie_alugueis = 'N') and (ie_tipo_despesa_p <> '6')))
and	nvl(a.ie_tipo_atendimento, nvl(ie_tipo_atendimento_p, 0))		= nvl(ie_tipo_atendimento_p, 0)
and	nvl(a.ie_clinica, nvl(ie_clinica_p, 0))					= nvl(ie_clinica_p, 0)
and	nvl(a.cd_categoria, nvl(cd_categoria_conv_p, '0'))			= nvl(cd_categoria_conv_p, '0')
and	nvl(a.nr_seq_classificacao, nvl(nr_seq_classificacao_p, '0'))		= nvl(nr_seq_classificacao_p, '0')
and	nvl(a.cd_setor_entrada, nvl(cd_setor_atendimento_p, '0'))		= nvl(cd_setor_atendimento_p, '0')
and	nvl(a.cd_setor_exec, nvl(cd_setor_execucao_p, '0'))			= nvl(cd_setor_execucao_p, '0')
and	nvl(a.ie_tipo_atend_tiss, nvl(ie_tipo_atend_tiss_w,'0'))		= nvl(ie_tipo_atend_tiss_w,'0')
and	nvl(a.ie_tipo_atend_proc_tiss, nvl(ie_tipo_atend_tiss_proc_w,'0'))	= nvl(ie_tipo_atend_tiss_proc_w,'0')
and	nvl(a.cd_perfil,nvl(cd_perfil_w,0))					= nvl(cd_perfil_w,0)
order by	a.ie_taxas,
	nvl(cd_setor_exec, 0),
	nvl(ie_tipo_atendimento, 0),
	nvl(ie_clinica_p, 0),
	nvl(cd_categoria, '0'),
	nvl(cd_setor_entrada, '0'),
	nvl(a.nr_seq_classificacao,0),
	nvl(a.ie_tipo_atend_tiss,'0'),
	nvl(a.ie_tipo_atend_proc_tiss,'0'),
	nvl(a.cd_perfil,0);

--Este cursor e  para os casos do TISS 3.2 onde o codigo das despesas e diferente	
cursor c02 is
select	nvl(a.ie_envio, 'E'),
	a.ds_padrao,
	a.vl_max_campo,
	a.ds_valor_gerado,
	a.ds_mascara
from	tiss_lista_campo b,
	tiss_regra_campo_conv a
where	a.nr_seq_campo		= b.nr_sequencia
and	a.cd_convenio		= cd_convenio_p
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	b.ie_tiss_tipo_guia	= ie_tipo_guia_p
and	b.nm_atributo		= nm_atributo_w
and	((a.ie_taxas = 'N') or
	(a.ie_taxas = 'S' and ie_tipo_despesa_p in ('5','7'))) 		-- Edgar 29/09/2007, Tratar se a regra acima deve ser aplicada para as taxas
and	((a.ie_gases = 'S') or
	((a.ie_gases = 'N') and (ie_tipo_despesa_p <> '1')))
and	((a.ie_medicamento = 'S') or
	((a.ie_medicamento = 'N') and (ie_tipo_despesa_p <> '2')))
and	((a.ie_material = 'S') or
	((a.ie_material = 'N') and (ie_tipo_despesa_p <> '3')))
and	((a.ie_taxas_diversas = 'S') or
	((a.ie_taxas_diversas = 'N') and (ie_tipo_despesa_p <> '7')))
and	((a.ie_diarias = 'S') or
	((a.ie_diarias = 'N') and (ie_tipo_despesa_p <> '5')))
and	((a.ie_alugueis = 'S') or
	((a.ie_alugueis = 'N') and (ie_tipo_despesa_p <> '7')))
and	nvl(a.ie_tipo_atendimento, nvl(ie_tipo_atendimento_p, 0))		= nvl(ie_tipo_atendimento_p, 0)
and	nvl(a.ie_clinica, nvl(ie_clinica_p, 0))					= nvl(ie_clinica_p, 0)
and	nvl(a.cd_categoria, nvl(cd_categoria_conv_p, '0'))			= nvl(cd_categoria_conv_p, '0')
and	nvl(a.nr_seq_classificacao, nvl(nr_seq_classificacao_p, '0'))		= nvl(nr_seq_classificacao_p, '0')
and	nvl(a.cd_setor_entrada, nvl(cd_setor_atendimento_p, '0'))		= nvl(cd_setor_atendimento_p, '0')
and	nvl(a.cd_setor_exec, nvl(cd_setor_execucao_p, '0'))			= nvl(cd_setor_execucao_p, '0')
and	nvl(a.ie_tipo_atend_tiss, nvl(ie_tipo_atend_tiss_w,'0'))		= nvl(ie_tipo_atend_tiss_w,'0')
and	nvl(a.ie_tipo_atend_proc_tiss, nvl(ie_tipo_atend_tiss_proc_w,'0'))	= nvl(ie_tipo_atend_tiss_proc_w,'0')
and	nvl(a.cd_perfil,nvl(cd_perfil_w,0))					= nvl(cd_perfil_w,0)
order by	a.ie_taxas,
	nvl(cd_setor_exec, 0),
	nvl(ie_tipo_atendimento, 0),
	nvl(ie_clinica_p, 0),
	nvl(cd_categoria, '0'),
	nvl(cd_setor_entrada, '0'),
	nvl(a.nr_seq_classificacao,0),
	nvl(a.ie_tipo_atend_tiss,'0'),
	nvl(a.ie_tipo_atend_proc_tiss,'0'),
	nvl(a.cd_perfil,0);

begin

nm_atributo_w			:= upper(nm_atributo_p);

ds_conteudo_w			:= ds_parametros_p;
k 				:= instr(ds_conteudo_w,'#@');
ie_tipo_atend_tiss_w		:= ltrim(substr(substr(ds_conteudo_w,1, k - 1),1,255),'0');
ds_conteudo_w			:= substr(ds_conteudo_w, k + 2, 4000);
ie_tipo_atend_tiss_proc_w	:= ltrim(substr(substr(ds_conteudo_w,1, k - 1),1,255),'0');
ds_conteudo_w			:= substr(ds_conteudo_w, k + 2, 4000);



tiss_regra_campos_pck.gerar_regra_campos(cd_convenio_w, cd_estabelecimento_p, qt_existentes_w, qt_geradas_w);


ds_retorno_w := tiss_regra_campos_pck.obter_regra_campos(ie_tipo_guia_p,
							nm_atributo_p,
							cd_convenio_w,
							vl_campo_p,
							ie_tipo_atendimento_p,
							cd_categoria_conv_p,
							ie_tipo_despesa_p,
							vl_max_campo_p,
							cd_estabelecimento_p,
							ie_clinica_p,
							nr_seq_classificacao_p,
							cd_setor_atendimento_p,
							ds_parametros_p,
							cd_setor_execucao_p);




/*
begin
ds_versao_tiss_w		:= tiss_obter_versao(cd_convenio_p,cd_estabelecimento_p);
exception
when others then
	ds_versao_tiss_w	:= null;
end;
*/
/*
begin
select	1
into	qt_regra_w
from	tiss_regra_campo_conv a
where	a.cd_convenio		= cd_convenio_p
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	rownum 			= 1;
exception
when others then
	qt_regra_w	:= 0;
end;

if	(qt_regra_w > 0) then

	if (obter_se_projeto_versao(0,12,ds_versao_tiss_w,0) = 'N') then
		open c01;
		loop
		fetch c01 into
			ie_envio_w,
			ds_padrao_w,
			vl_permitido_w,
			ds_valor_gerado_w,
			ds_mascara_w;
		exit when c01%notfound;	
			if	(nvl(ie_envio_w, 'E') = 'PG') and (nvl(ds_valor_gerado_w,'X') = nvl(vl_campo_p,'Z')) then
				ds_retorno_w	:= ds_padrao_w;
			end if;
		end loop;
		close c01;	
	else
		open c02;
		loop
		fetch c02 into
			ie_envio_w,
			ds_padrao_w,
			vl_permitido_w,
			ds_valor_gerado_w,
			ds_mascara_w;
		exit when c02%notfound;	
			if	(nvl(ie_envio_w, 'E') = 'PG') and (nvl(ds_valor_gerado_w,'X') = nvl(vl_campo_p,'Z')) then
				ds_retorno_w	:= ds_padrao_w;
			end if;
		end loop;
		close c02;	
	end if;
end if;
*/


return ds_retorno_w;

end TISS_OBTER_REGRA_CAMPO;
/
