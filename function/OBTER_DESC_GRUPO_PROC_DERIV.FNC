create or replace
function Obter_desc_grupo_proc_deriv(nr_sequencia_p number)
 		    	return varchar2 is
ds_grupo_w	varchar2(200);

begin

select max(ds_grupo)
into 	ds_grupo_w
from 	grupo_proc_derivado
where 	nr_sequencia = nr_sequencia_p;

return	ds_grupo_w;

end Obter_desc_grupo_proc_deriv;
/