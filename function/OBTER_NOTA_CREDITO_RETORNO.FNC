create or replace
function obter_nota_credito_retorno
			(nr_seq_retorno_p in number)
			return number is
			
nr_sequencia_w	number(10);

begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	nota_credito
where	nr_seq_retorno	= nr_seq_retorno_p;

return 	nr_sequencia_w;

end obter_nota_credito_retorno;
/