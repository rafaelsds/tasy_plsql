create or replace
function Obter_desc_interv_grupo( nr_seq_proc_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
begin
select	max(b.ds_grupo) ds_grupo
into	ds_retorno_w
from	PE_PROCEDIMENTO a,
	grupo_interv_enf b
where	a.nr_sequencia = nr_seq_proc_p
and	a.nr_seq_grupo = b.nr_sequencia;


return	ds_retorno_w;

end Obter_desc_interv_grupo;
/