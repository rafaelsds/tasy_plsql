create or replace
FUNCTION cih_obter_se_tecnica(  nr_seq_dados_hig_p		NUMBER)
 		    	RETURN VARCHAR2 IS

is_11_tec_w             VARCHAR2(1) := 'N';
BEGIN
IF      (NVL(nr_seq_dados_hig_p,0) > 0) THEN

        SELECT  DECODE(COUNT(1), 11,'S','N')
        INTO    is_11_tec_w
        FROM    cih_tecnica_hig_maos
        WHERE   nr_seq_dados_hig  = nr_seq_dados_hig_p;

END IF;

RETURN	is_11_tec_w;

END cih_obter_se_tecnica;
/
