create or replace
function obter_se_cons_impl_modulos (
	nr_seq_consultor_p	number,
	ds_lista_modulos_p	varchar2) 
	return varchar2 is
	
ie_impl_modulos_w	varchar2(1) := 'N';
ds_lista_modulos_w	varchar2(1000);
nr_pos_virgula_w	number(3,0);
nr_seq_modulo_w		number(10,0);
ie_impl_modulos_ww	varchar2(1) := 'N';

begin
if	(nr_seq_consultor_p is not null) and
	(ds_lista_modulos_p is not null) then
	begin
	ds_lista_modulos_w := ds_lista_modulos_p;	
	ie_impl_modulos_ww := 'S';
	while 	(ie_impl_modulos_ww = 'S') and
		(ds_lista_modulos_w is not null) loop
		begin
		nr_pos_virgula_w := instr(ds_lista_modulos_w,',');		
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_seq_modulo_w		:= to_number(substr(ds_lista_modulos_w,1,nr_pos_virgula_w-1));
			ds_lista_modulos_w	:= substr(ds_lista_modulos_w,nr_pos_virgula_w+1,length(ds_lista_modulos_w));
			end;
		else
			begin
			nr_seq_modulo_w		:= to_number(ds_lista_modulos_w);
			ds_lista_modulos_w	:= null;			
			end;
		end if;
		
		if	(nr_seq_modulo_w > 0) then
			begin
			select	decode(count(*),0,'N','S')
			into	ie_impl_modulos_ww
			from	com_canal_cons_modulo
			where	nr_seq_consultor = nr_seq_consultor_p
			and	nr_seq_modulo_implant = nr_seq_modulo_w;
			end;
		else
			begin
			ie_impl_modulos_ww := 'N';
			end;
		end if;
		end;
	end loop;
	end;
end if;
ie_impl_modulos_w := ie_impl_modulos_ww;
return ie_impl_modulos_w;
end obter_se_cons_impl_modulos;
/