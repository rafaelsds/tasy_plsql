CREATE OR REPLACE FUNCTION OBTER_SE_USUARIO_GRUPO(	nr_grupo_usuario_p	varchar2,
						  	nm_usuario_p		varchar2)
Return Varchar2 IS

nr_seq_grupo_w		number;
ds_result_w		Varchar2(1);

Cursor C01 IS
Select	nr_seq_grupo
from	usuario_grupo
where	nm_usuario_grupo = nm_usuario_p;

BEGIN
ds_result_w	:= 'N';

if	(nr_grupo_usuario_p is not null) and
	(nm_usuario_p	is not null)then
	
	select	decode(count(*),0,'N','S')
	into	ds_result_w
	from	usuario_grupo
	where	nm_usuario_grupo = nm_usuario_p
	and	nr_seq_grupo	= nr_grupo_usuario_p;
end if;

Return ds_result_w;

END OBTER_SE_USUARIO_GRUPO;
/
