create or replace function pfcs_get_tele_device_intg_v2
(
  ds_device_type_p in varchar2,
  ds_filter_type_p in varchar2,
  cd_estabelecimento_p	varchar2
) return number as
nr_retorno_w						number(10);
ds_active_status_w				varchar2(15) := 'ACTIVE';
ds_dev_inactive_status_w			varchar2(15) := 'INACTIVE';
ds_dev_unknown_status				varchar2(15) := 'UNKNOWN';
ds_device_type_w					varchar2(15) := 'Monitor';

/*
   Function "pfcs_get_tele_device_intg_v2" is used to calculate housekeeping/closed/contaminated/available/blocked/occupied/unoccupied telemetry beds.
*/

begin
	nr_retorno_w := 0;
	-- ds_filter_type A is for all available tele beds
	if (ds_filter_type_p = 'A') then
		select	count(1) into nr_retorno_w
		from	pfcs_device dev,
				pfcs_organization org,
				pfcs_location loc
		where	(dev.si_status in (ds_dev_inactive_status_w) or dev.si_status is null)
		and		dev.ds_device_type = ds_device_type_w
		and		dev.nr_seq_organization = org.nr_sequencia
		and		org.cd_estabelecimento = to_number(cd_estabelecimento_p)
		and 	dev.nr_seq_location = loc.nr_sequencia
		and 	loc.si_status in ('ACTIVE')	and	(loc.cd_operational_status not in ('K', 'C','O') or loc.cd_operational_status is null);
	-- ds_filter_type Unoccupied is for all Unoccupied tele beds
	elsif (ds_filter_type_p = 'Unoccupied') then
		select	count(1) into nr_retorno_w
		from	pfcs_device dev,
				pfcs_organization org,
				pfcs_location loc
		where	(dev.si_status in (ds_dev_inactive_status_w) or dev.si_status is null)
		and		dev.ds_device_type = ds_device_type_w
		and		dev.nr_seq_organization = org.nr_sequencia
		and		org.cd_estabelecimento = to_number(cd_estabelecimento_p)
		and 	dev.nr_seq_location = loc.nr_sequencia
		and 	loc.si_status in ('ACTIVE')	and	(loc.cd_operational_status in ('U') or loc.cd_operational_status is null);
	-- ds_filter_type H is for Housekeeping tele beds
	elsif (ds_filter_type_p = 'H') then
		select	count(1) into nr_retorno_w
		from	pfcs_device dev,
				pfcs_organization org,
				pfcs_location loc
		where	(dev.si_status in (ds_dev_inactive_status_w) or dev.si_status is null)
		and		dev.ds_device_type = ds_device_type_w
		and		dev.nr_seq_organization = org.nr_sequencia
		and		org.cd_estabelecimento = to_number(cd_estabelecimento_p)
		and 	dev.nr_seq_location = loc.nr_sequencia
		and 	loc.si_status in ('ACTIVE')	and	loc.cd_operational_status in ('H');
	-- ds_filter_type I is for Isolated tele beds
	elsif (ds_filter_type_p = 'I') then
		select	count(1) into nr_retorno_w
		from	pfcs_device dev,
				pfcs_organization org,
				pfcs_location loc
		where	(dev.si_status in (ds_dev_inactive_status_w) or dev.si_status is null)
		and		dev.ds_device_type = ds_device_type_w
		and		dev.nr_seq_organization = org.nr_sequencia
		and		org.cd_estabelecimento = to_number(cd_estabelecimento_p)
		and 	dev.nr_seq_location = loc.nr_sequencia
		and 	loc.si_status in ('ACTIVE')	and	loc.cd_operational_status in ('I');
	-- ds_filter_type U is for devices in use
	elsif (ds_filter_type_p = 'U') then
		select count(1) into nr_retorno_w
		from pfcs_device dev,
		pfcs_location loc,
        pfcs_organization org
		where
			dev.si_status = ds_active_status_w
			and dev.ds_device_type = ds_device_type_w
			and dev.nr_seq_organization = org.nr_sequencia
			and dev.nr_seq_location = loc.nr_sequencia
            and org.cd_estabelecimento = to_number(cd_estabelecimento_p)
            and loc.si_status in ('ACTIVE');
	-- ds_filter_type B is for all Blocked tele beds
	elsif (ds_filter_type_p = 'B') then
		select	count(1) into nr_retorno_w
		from	pfcs_device dev,
				pfcs_location loc,
				pfcs_organization org
		where   dev.nr_seq_location = loc.nr_sequencia
		and dev.ds_device_type = ds_device_type_w
		and (loc.si_status in ('SUSPENDED') or (loc.si_status in ('ACTIVE') and loc.cd_operational_status in ('K', 'C')))
		and dev.nr_seq_organization = org.nr_sequencia
		and org.cd_estabelecimento  = to_number(cd_estabelecimento_p);
	-- ds_filter_type K is for Contaminated tele beds
	elsif (ds_filter_type_p = 'K') then
		select	count(1) into nr_retorno_w
		from	pfcs_device dev,
				pfcs_location loc,
				pfcs_organization org
		where   dev.nr_seq_location = loc.nr_sequencia
		and dev.ds_device_type = ds_device_type_w
		and dev.nr_seq_organization = org.nr_sequencia
		and org.cd_estabelecimento  = to_number(cd_estabelecimento_p)
		and (loc.si_status in ('ACTIVE') and  loc.cd_operational_status in ('K'));
	-- ds_filter_type C is for Closed tele beds
	elsif (ds_filter_type_p = 'C') then
		select	count(1) into nr_retorno_w
		from	pfcs_device dev,
				pfcs_location loc,
				pfcs_organization org
		where   dev.nr_seq_location = loc.nr_sequencia
		and dev.ds_device_type = ds_device_type_w
		and dev.nr_seq_organization = org.nr_sequencia
		and org.cd_estabelecimento  = to_number(cd_estabelecimento_p)
		and (loc.si_status in ('SUSPENDED') or  (loc.si_status in ('ACTIVE') and loc.cd_operational_status in ('C')));
	-- ds_filter_type BorkenLostDevice is for Borken/Lost devices
	elsif (ds_filter_type_p = 'BorkenLostDevice') then
		select	count(*) into nr_retorno_w
    	from 	pfcs_device dev,
    			pfcs_organization org
    	where	dev.si_status in ('UNKNOWN')
    	and		dev.ds_device_type = 'Monitor'
    	and		dev.nr_seq_organization = org.nr_sequencia
    	and		org.cd_estabelecimento = to_number(cd_estabelecimento_p);
	else
	-- when ds_filter_type is not passed then all devices are considered
	  select count(1) into nr_retorno_w
		from pfcs_device dev,
        pfcs_organization org
		where
			dev.ds_device_type = ds_device_type_w
			and dev.nr_seq_organization = org.nr_sequencia
            and org.cd_estabelecimento = to_number(cd_estabelecimento_p)
			and		(dev.nr_seq_location is null or
			         dev.si_status in ('UNKNOWN') or
					 not exists (	select	loc.nr_sequencia
									from	pfcs_location loc
									where	dev.nr_seq_location = loc.nr_sequencia
									and 	(loc.si_status not in ('ACTIVE')
									or		(loc.cd_operational_status in ('K', 'C')))));
	end if;

	return nr_retorno_w;

	exception
		-- If there are no records found
		when no_data_found then
		  return nr_retorno_w;
end pfcs_get_tele_device_intg_v2;
/
