create or replace
function pepo_obter_pasta_ini_item_js(
					nr_seq_item_pepo_p	number,
					cd_perfil_p		number
					)
					return number is
					
					
ds_retorno_w	number(10) := 0;
					
begin


if	(nr_seq_item_pepo_p is not null) then
	begin
	
	select	a.nr_seq_dic_obj
	into	ds_retorno_w
	from	perfil_item_pepo b,
		prontuario_pasta a
	where	a.nr_sequencia = b.nr_seq_pasta_inicial
	and	b.cd_perfil = cd_perfil_p
	and	nr_seq_pasta_superior is null
	and	b.nr_seq_item_pepo = nr_seq_item_pepo_p
	and	a.ie_situacao = 'A';	
	
	end;
end if;

return	ds_retorno_w;

end pepo_obter_pasta_ini_item_js;
/