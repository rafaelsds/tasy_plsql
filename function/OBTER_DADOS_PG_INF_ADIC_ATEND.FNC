create or replace
function obter_dados_pg_inf_adic_atend(	nr_sequencia_p	number,
					ie_opcao_p	varchar2)
					return varchar2 is

ds_informacao1_w		varchar2(255);
ds_informacao2_w		varchar2(255);
ds_retorno_w			varchar2(255);

cursor c01 is
select 	substr(obter_dados_pg_inf_adic(nr_seq_inf_adic, 'D'),1,255),
		substr(obter_dados_pg_inf_adic_item(nr_seq_inf_adic_item, 'D'),1,255)
from	atend_perda_ganho_inf_adic a
where	nr_seq_atend_pg = nr_sequencia_p
order by 1,2;

begin

if	(nr_sequencia_p > 0) then
	if	(ie_opcao_p = 'D') then
		open C01;
		loop
			fetch C01 into	ds_informacao1_w,
							ds_informacao2_w;
			exit when C01%notfound;
			if (ds_retorno_w is not null) then
				ds_retorno_w	:=	ds_retorno_w || ', ';
			end if;
			ds_retorno_w	:=	ds_retorno_w || ds_informacao1_w || ': ' || ds_informacao2_w;
		end loop;
		close C01;	
	end if;
end if;

return ds_retorno_w;

end obter_dados_pg_inf_adic_atend;
/
