create or replace
function obter_valor_reserva_viagem (	nr_seq_viagem_p	number)
				return number is

vl_total_w		number(15,2):= 0;

begin

select	nvl(sum(VL_RESERVA),0) vl_total
into	vl_total_w
from	via_reserva a
where	a.nr_seq_viagem	= nr_seq_viagem_p
and		a.ie_responsavel_custo <> 'N'
and		ie_cancelamento is null
and		a.ie_tipo_reserva	= 'T'
having	nvl(sum(VL_RESERVA),0) > 0;

return vl_total_w;

end obter_valor_reserva_viagem;
/
