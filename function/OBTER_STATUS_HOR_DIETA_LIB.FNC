create or replace
function obter_status_hor_dieta_lib (
				dt_administracao_p	date,
				dt_suspensao_p	date,
				dt_lib_horario_p	date)
				return varchar2 is
				
ie_status_w	varchar2(1);
				
begin
if	(dt_administracao_p is null) and
	(dt_suspensao_p is null) and
	(dt_lib_horario_p is not null)then
	begin
	ie_status_w	:= 'N';
	end;
elsif	(dt_administracao_p is not null) and
	(dt_suspensao_p is null) and
	(dt_lib_horario_p is not null)	then
	begin
	ie_status_w	:= 'A';
	end;
elsif	(dt_administracao_p is null) and
	(dt_suspensao_p is not null) and
	(dt_lib_horario_p is not null)then
	begin
	ie_status_w	:= 'S';
	end;
elsif	(dt_administracao_p is null) and
	(dt_suspensao_p is null) and
	(dt_lib_horario_p is null)then
	begin
	ie_status_w	:= 'W';
	end;
end if;

return ie_status_w;
end obter_status_hor_dieta_lib;
/