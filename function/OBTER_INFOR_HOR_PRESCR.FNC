create or replace
function obter_infor_hor_prescr(nr_atendimento_p	number,
				nr_prescricao_p		number,
				nr_sequencia_p		number)
 	return varchar2 is

	
dt_horario_w		date;
dt_horario_ant_w	date := to_date('01/01/2000','dd/mm/yyyy');
ie_situacao_w 		varchar2(1);
dt_horarios_w		varchar2(2000) := ' ';	

	
Cursor C01 is
SELECT	DISTINCT h.dt_horario,
		 DECODE(obter_status_hor_prescr(h.nr_sequencia,'M'),'S','#','A','-','N','')
FROM		 prescr_material a,
		 prescr_mat_hor h,
		 prescr_medica m
	WHERE	h.nr_prescricao = m.nr_prescricao
	AND	a.nr_prescricao = m.nr_prescricao
	AND	a.cd_material 	= h.cd_material
	AND	m.nr_atendimento = nr_atendimento_p
	AND	m.nr_prescricao = nr_prescricao_p
	AND	NVL(a.ie_se_necessario,'N') = 'N'
	AND     a.nr_sequencia  = nr_sequencia_p
	AND	NVL(a.ie_acm,'N') = 'N'
	AND	Obter_se_horario_liberado(h.dt_lib_horario, h.dt_horario) = 'S';

begin
open c01;
loop
fetch c01 into	
	dt_horario_w,
	ie_situacao_w;
exit when c01%notfound;
	begin
	
	if	(trunc(dt_horario_ant_w) <> trunc(dt_horario_w)) then
		
		dt_horarios_w := dt_horarios_w || chr(13) || chr(10) || '  ' || replace(to_char(dt_horario_w,'dd/mm hh24:mi'),':00','') || ' ' ||ie_situacao_w ;
	else
		dt_horarios_w := dt_horarios_w ||'  ' ||  replace(to_char(dt_horario_w,'hh24:mi'),':00','') || ' ' || ie_situacao_w  ;		
	end if;
	
	dt_horario_ant_w := dt_horario_w;
	
	end;
end loop;
close c01;

return	substr(dt_horarios_w,4,length(dt_horarios_w) - 3);	



end obter_infor_hor_prescr;
/