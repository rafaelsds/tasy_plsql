CREATE OR REPLACE FUNCTION obter_dados_cid_japan(nr_seq_cid_interno_japan_p cid_interno_japan.nr_sequencia%type,
                                                 campo_p varchar2)
  RETURN varchar2 IS
  
  ds_result_v varchar2(200);
  
BEGIN

  CASE UPPER(campo_p)
    WHEN 'DS_CID_DOENCA' THEN
      SELECT cd.ds_doenca_cid
        INTO ds_result_v
        FROM cid_interno_japan cj         
       INNER JOIN cid_doenca cd ON cd.cd_doenca_cid = cj.cd_doenca
       WHERE nr_sequencia = nr_seq_cid_interno_japan_p; 

    WHEN 'CD_DOENCA' THEN
      SELECT cd_doenca
        INTO ds_result_v
        FROM cid_interno_japan
       WHERE nr_sequencia = nr_seq_cid_interno_japan_p; 

    WHEN 'CD_DOENCA_MDFY' THEN
      SELECT cd_doenca_mdfy
        INTO ds_result_v
        FROM cid_interno_japan
       WHERE nr_sequencia = nr_seq_cid_interno_japan_p; 
         
    WHEN 'DS_DOENCA_MDFY' THEN
      SELECT ds_doenca_mdfy
        INTO ds_result_v
        FROM cid_interno_japan
       WHERE nr_sequencia = nr_seq_cid_interno_japan_p;     
          
    WHEN 'DS_DOENCA_KATANA' THEN
       SELECT ds_doenca_katana
         INTO ds_result_v
         FROM cid_interno_japan
        WHERE nr_sequencia = nr_seq_cid_interno_japan_p; 
                 
    ELSE 
      ds_result_v := '';     
         
  END CASE;  

  RETURN ds_result_v;
  
  EXCEPTION
     WHEN NO_DATA_FOUND THEN
       RETURN '';
END obter_dados_cid_japan;
/
