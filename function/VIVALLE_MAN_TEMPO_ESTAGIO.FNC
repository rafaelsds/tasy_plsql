create or replace 
function vivalle_man_tempo_estagio(	nr_sequencia_p		number,
				nr_seq_ordem_p		number,
				ie_tipo_p			varchar2)
				return number is

qt_retorno_w				number(10) := 0;
dt_atual_w				date;
dt_atual_ww				date;
dt_atualizacao_w				Date;
nr_seq_localizacao_w			number(10);
cd_estabelecimento_w			number(04);

/*Legenda
	D - Dias corridos
	M - Minutos corridos
	DC - Dias uteis
	MC - MInutos uteis
*/
pragma autonomous_transaction;
begin

select	dt_atualizacao
into	dt_atualizacao_w
from	man_ordem_serv_estagio
where	nr_seq_ordem = nr_seq_ordem_p
and	nr_sequencia = nr_sequencia_p;


/* Obtem dados da Ordem de Servi�o */

select	nr_seq_localizacao,
	nvl(dt_fim_real,sysdate)
into	nr_seq_localizacao_w,
	dt_atual_w
from	man_ordem_servico
where	nr_sequencia = nr_seq_ordem_p;

/* Estabelecimento da OS*/
select	cd_estabelecimento
into	cd_estabelecimento_w
from	man_localizacao
where	nr_sequencia = nr_seq_localizacao_w;

/* Data do pr�ximo est�gio para calcular a diferen�a*/
select	min(dt_atualizacao)
into	dt_atual_ww
from	man_ordem_serv_estagio
where	nr_seq_ordem	= nr_seq_ordem_p
and	dt_atualizacao > dt_atualizacao_w;

/* Se n�o tiver pr�ximo est�gio calcula pela data atual ou pela data de fim da ordem*/
if	(dt_atual_ww is null) then
	dt_atual_ww	:= dt_atual_w;
end if;

if	(ie_tipo_p = 'M') then
	qt_retorno_w			:= vivalle_obter_min_entre_datas(dt_atualizacao_w,dt_atual_ww);
elsif	(ie_tipo_p = 'D') then
	qt_retorno_w			:= obter_dias_entre_datas(dt_atualizacao_w,dt_atual_ww);
elsif	(ie_tipo_p = 'MC') then
	qt_retorno_w			:= man_obter_tempo_com(cd_estabelecimento_w,dt_atualizacao_w,dt_atual_ww,'MC');
elsif	(ie_tipo_p = 'DC') then
	qt_retorno_w			:= man_obter_tempo_com(cd_estabelecimento_w,dt_atualizacao_w,dt_atual_ww,'DC');
end if;

return	qt_retorno_w;

end vivalle_man_tempo_estagio;
/