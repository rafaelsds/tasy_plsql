create or replace
function man_obter_hr_fim
			(	nm_usuario_p			varchar2)
				return date is

dt_retorno_w		date := null;
hr_inicio_w			varchar2(4);
hr_fim_w			varchar2(4);
dt_base_w			date;

begin

select	hr_inicio,
		hr_fim
into	hr_inicio_w,
		hr_fim_w
from	man_horario_trabalho
where	to_char(sysdate, 'd') = ie_dia_semana
and		man_obter_se_hor_trab_lib(nr_sequencia, null, null, nm_usuario_p) = 'S'
and		rownum = 1;

if	(to_number(hr_inicio_w) > to_number(hr_fim_w)) then
	dt_base_w := sysdate + 1;
else
	dt_base_w := sysdate;
end if;

select	to_date(max(to_char(dt_base_w, 'dd/mm/yyyy') || ' ' || lpad(hr_fim_w, 2, 0) || ':' || substr(hr_fim_w, 3, 4) || ':00'), 'dd/mm/yyyy hh24:mi:ss')
into 	dt_retorno_w
from	dual;

return	dt_retorno_w;

end man_obter_hr_fim;
/
