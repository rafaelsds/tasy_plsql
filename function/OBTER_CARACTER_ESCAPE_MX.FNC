create or replace function obter_caracter_escape_mx(ds_texto_p varchar2)
        return varchar2 is

ds_retorno_w  varchar2(255);

begin

ds_retorno_w := ds_texto_p;

if (instr(ds_texto_p,'&') > 0)then
   ds_retorno_w := replace(ds_retorno_w,'&','&'||'amp;');
end if;

if (instr(ds_texto_p,'<') > 0)then
  ds_retorno_w := replace(ds_retorno_w,'<','&'||'alt;');
end if;

if (instr(ds_texto_p,'>') > 0)then
  ds_retorno_w := replace(ds_retorno_w,'>','&'||'gt;');
end if;

if (instr(ds_texto_p,'"') > 0)then
  ds_retorno_w := replace(ds_retorno_w,'"','&'||'quot;');
end if;

if (instr(ds_texto_p,'''') > 0)then
  ds_retorno_w := replace(ds_retorno_w,'''','&'||'#39;');
end if;

return ds_retorno_w;


end obter_caracter_escape_mx;
/