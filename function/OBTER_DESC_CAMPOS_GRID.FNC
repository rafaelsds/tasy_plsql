create or replace
function obter_desc_campos_grid(cd_grade_p	number)
				return varchar2 is
					
ds_retorno_w		varchar2(4000) := null;
ds_grid_w 			varchar2(20) := '';

begin

select DS_DESCRICAO_CLIENTE DS_GRID
into ds_grid_w
from EVENTOS_ADVERSOS_GRADE
where CD_GRADE = cd_grade_p
and DS_DESCRICAO_CLIENTE is not null;

if	(ds_grid_w	is not null) then
	ds_retorno_w	:=  ds_grid_w;

end if;
 

return	ds_retorno_w;

end;
/