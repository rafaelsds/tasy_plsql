create or replace function get_expression(cd_expression_p		number,
						ds_language_p			varchar2)
				return varchar2 is
		 			 
ds_retorno_w			varchar2(4000)	:= null;
nr_seq_idioma_w			number(10);				


begin
--Still using legacy approach, despite using DS_LANGUAGE instead of NR_SEQ_IDIOMA.

if 	(ds_language_p = 'es-MX') then -- Espanhol  - Mexico(MX)
	nr_seq_idioma_w	:= 2;
elsif 	(ds_language_p = 'es-AR') then -- Espanhol - Argentina (AR)
	nr_seq_idioma_w	:= 4;
elsif 	(ds_language_p = 'en-US') then -- Ingles (US)
	nr_seq_idioma_w	:= 5;
elsif 	(ds_language_p = 'de-DE') then -- Alemao - DE
	nr_seq_idioma_w	:= 7;
elsif 	(ds_language_p = 'ar-SA') then -- Arabia Saudita - SA
	nr_seq_idioma_w	:= 8;
elsif 	(ds_language_p = 'en-AU') then -- Ingles (AU)
	nr_seq_idioma_w	:= 9;
elsif 	(ds_language_p = 'pl-PL') then -- Polones (PL)
	nr_seq_idioma_w	:= 11;
elsif 	(ds_language_p = 'ja-JP') then -- Japones (JP)
	nr_seq_idioma_w	:= 12;
elsif 	(ds_language_p = 'es-DO') then -- Republica Dominicana (DO)
	nr_seq_idioma_w	:= 13;
elsif 	(ds_language_p = 'de-AT') then -- German Austria(AT)
	nr_seq_idioma_w	:= 14;
elsif 	(ds_language_p = 'es-BO') then -- Spanish Bolivia(BO)
	nr_seq_idioma_w	:= 15;
elsif 	(substr(ds_language_p,1,2) = 'en') then --Default english
	nr_seq_idioma_w	:= 5;
elsif 	(substr(ds_language_p,1,2) = 'es') then --Default espanhol
	nr_seq_idioma_w	:= 2;
elsif 	(substr(ds_language_p,1,2) = 'de') then --Default German
	nr_seq_idioma_w	:= 7;
else
	nr_seq_idioma_w	:= 1;  -- pt-BR
end if;

ds_retorno_w	:= OBTER_DESC_EXPRESSAO_IDIOMA(cd_expression_p,null,nr_seq_idioma_w);


return	ds_retorno_w;

end get_expression;
/
