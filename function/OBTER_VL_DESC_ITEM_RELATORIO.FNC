create or replace
function obter_vl_desc_item_relatorio (   	nr_seq_propaci_p	number,
						nr_seq_matpaci_p        number,
						nr_interno_conta_p	number,
						ie_imposto_p		varchar2)
						return number is
/* FUNCTION CRIADA PARA A VIEW  HSJ_RESUMO_CONTA_V */
						
/*ie_com_imposto_p
	'S'   considera imposto
	'N'  nao considera imposto
*/

vl_desc_item_w		number(15,4) := 0;
qt_imposto_w		number(15,0) := 0;
vl_original_w		number(18,5) := 0;
qt_material_w		material_atend_paciente.qt_material%type   := 0;
qt_procedimento_w	procedimento_paciente.qt_procedimento%type := 0;
vl_material_w		material_atend_paciente.vl_material%type   := 0;
ie_conta_deducao_w	varchar2(2);
vl_procedimento_w	procedimento_paciente.vl_procedimento%type := 0;

begin

if (nvl(nr_seq_propaci_p,0) > 0) then
	select 	decode(count(1),0,'N','S') 
	into	ie_conta_deducao_w
	from 	conta_pac_ded_conv_item z 
	where 	z.nr_seq_propaci_dest = nr_seq_propaci_p;
	
	if (ie_conta_deducao_w = 'S') then		
		return vl_desc_item_w;
	end if;
	
	select 	count(1) 
	into    qt_imposto_w
	from 	propaci_imposto 
	where 	nr_seq_propaci = nr_seq_propaci_p 
	and 	pr_imposto > 0;

	if (ie_imposto_p  = 'N' and qt_imposto_w > 0) or
	   (ie_imposto_p  = 'S' and qt_imposto_w = 0) then		
		return vl_desc_item_w;
	end if;	
		
	vl_original_w := nvl(obter_vl_item_conta_original( nr_seq_propaci_p,null),0);
	
	if ( vl_original_w <> 0) then
		select  qt_procedimento,
			nvl(vl_procedimento,0)
		into	qt_procedimento_w,
			vl_procedimento_w --trocar
		from  	procedimento_paciente
		where 	nr_sequencia = nr_seq_propaci_p;
		
		vl_desc_item_w	:= 	(vl_original_w * qt_procedimento_w)
					- vl_procedimento_w					
					- obter_valor_deducao_conv(nr_interno_conta_p,nr_seq_propaci_p,null,null,null);
	end if;
	
elsif (nvl(nr_seq_matpaci_p,0) > 0) then

	select 	decode(count(1),0,'N','S') 
	into	ie_conta_deducao_w
	from 	conta_pac_ded_conv_item z 
	where 	z.nr_seq_matpaci_dest = nr_seq_matpaci_p;

	if (ie_conta_deducao_w = 'S') then		
		return vl_desc_item_w;
	end if;
	
	select 	count(1) 
	into    qt_imposto_w
	from 	matpaci_imposto 
	where 	nr_seq_matpaci = nr_seq_matpaci_p 
	and 	pr_imposto > 0;

	if (ie_imposto_p  = 'N' and qt_imposto_w > 0) or
	   (ie_imposto_p  = 'S' and qt_imposto_w = 0) then		
		return vl_desc_item_w;
	end if;	
		
	vl_original_w := nvl(obter_vl_item_conta_original(null,nr_seq_matpaci_p),0);
	
	if ( vl_original_w <> 0) then
		select  qt_material,
			nvl(vl_material,0)
		into	qt_material_w,
			vl_material_w
		from  	material_atend_paciente
		where 	nr_sequencia = nr_seq_matpaci_p;
		
		vl_desc_item_w	:= 	(vl_original_w * qt_material_w)
					- vl_material_w					
					- obter_valor_deducao_conv(nr_interno_conta_p,null,nr_seq_matpaci_p,null,null);
	end if;	

end if;
	
return	vl_desc_item_w;

end obter_vl_desc_item_relatorio;
/