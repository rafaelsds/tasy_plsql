create or replace
function PS_obter_forecasting_quarter (dt_forecasting_p	date)
	return number is
	
forecasting_quarter_w	number(1,0);
	
begin
if	(dt_forecasting_p is not null) then
	begin
	if	(to_number(to_char(dt_forecasting_p,'mm')) <= 3) then
		begin
		forecasting_quarter_w := 1;
		end;
	elsif	(to_number(to_char(dt_forecasting_p,'mm')) <= 6) then
		begin
		forecasting_quarter_w := 2;
		end;
	elsif	(to_number(to_char(dt_forecasting_p,'mm')) <= 9) then
		begin
		forecasting_quarter_w := 3;
		end;			
	else
		begin
		forecasting_quarter_w := 4;
		end;
	end if;
	end;
end if;
return forecasting_quarter_w;
end PS_obter_forecasting_quarter;
/