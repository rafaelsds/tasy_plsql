create or replace
function pls_obter_grupo_analise_atual
			(	nr_seq_auditoria_p	number)
				return number is

nr_retorno_w			number(10);
nr_seq_ordem_w			number(10);

begin

begin
	select	nvl(min(nr_seq_ordem),0)
	into	nr_seq_ordem_w
	from	pls_auditoria_grupo
	where	dt_liberacao is null
	and	nr_seq_auditoria = nr_seq_auditoria_p;

	select	nvl(nr_sequencia,0)
	into	nr_retorno_w
	from	pls_auditoria_grupo	 
	where	nr_seq_ordem		= nr_seq_ordem_w
	and	nr_seq_auditoria	= nr_seq_auditoria_p;
exception
when others then
	select	nvl(max(nr_sequencia),0)
	into	nr_retorno_w
	from	pls_auditoria_grupo	 
	where	nr_seq_ordem		= nr_seq_ordem_w
	and	nr_seq_auditoria	= nr_seq_auditoria_p
	and	dt_liberacao 		is null;
end;

return	nr_retorno_w;

end pls_obter_grupo_analise_atual;
/