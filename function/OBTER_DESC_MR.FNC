create or replace
function Obter_desc_MR(nr_atendimento_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);			
			
begin

select  substr(max(Obter_desc_microorganismo(a.cd_microorganismo)),1,255)
into	ds_retorno_w
from    cih_cultura a
where   CIH_Obter_dados_Ficha(a.nr_ficha_ocorrencia,1) = nr_atendimento_p
and	not exists (	select	1
			from	atendimento_paciente x
			where	nr_atendimento_p = x.nr_atendimento
			and	dt_descolonizar_paciente is not null
			and	nm_descolonizar_paciente is not null)
and     a.ie_multi_resistente = 'S';

return	ds_retorno_w;

end Obter_desc_MR;
/
