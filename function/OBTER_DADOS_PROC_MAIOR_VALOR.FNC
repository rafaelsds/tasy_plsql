create or replace
function obter_dados_proc_maior_valor(	nr_interno_conta_p	number,
					ie_opcao_p		varchar2)
	return varchar2 is

ds_retorno_w		varchar2(80);
nr_sequencia_w		number(10);
nr_seq_proc_interno_w	number(10);
dt_procedimento_w		date;

cursor c01 is
	select	a.nr_sequencia,
		a.nr_seq_proc_interno,
		a.dt_procedimento
	from	procedimento_paciente a,
		procedimento b
	where	a.cd_procedimento = b.cd_procedimento
	and	a.ie_origem_proced = b.ie_origem_proced
	and	a.nr_interno_conta = nr_interno_conta_p
	and	a.cd_motivo_exc_conta is null
	and	b.ie_classificacao = 1
	order by	vl_procedimento asc;

begin

ds_retorno_w		:= null;
nr_sequencia_w		:= null;
nr_seq_proc_interno_w	:= null;
dt_procedimento_w		:= null;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	nr_seq_proc_interno_w,
	dt_procedimento_w;
exit when c01%notfound;
	begin
	nr_sequencia_w		:= nr_sequencia_w;
	nr_seq_proc_interno_w	:= nr_seq_proc_interno_w;
	dt_procedimento_w		:= dt_procedimento_w;
	end;
end loop;
close c01;

if	(ie_opcao_p = 'NR') then
	ds_retorno_w := nr_sequencia_w;
elsif	(ie_opcao_p = 'PI') then
	ds_retorno_w := nr_seq_proc_interno_w;
elsif	(ie_opcao_p = 'DT') then
	ds_retorno_w := to_char(dt_procedimento_w, 'dd/mm/yyyy hh24:mi:ss');
end if;

return	ds_retorno_w;

end obter_dados_proc_maior_valor;
/