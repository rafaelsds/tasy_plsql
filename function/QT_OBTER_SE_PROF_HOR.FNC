create or replace
function Qt_Obter_Se_Prof_Hor(
				nr_seq_prof_p	number,
				dt_agenda_p	date,
				cd_estabelecimento_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1)	:= 'N';
ie_feriado_w	varchar2(1);
qt_prof_turno_w	number(10);
			
begin

select	decode(count(*),0,'N','S')
into	ie_feriado_w
from 	feriado a
where 	a.cd_estabelecimento 	= cd_estabelecimento_p
and	a.dt_feriado between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400;

select	count(*)
into	qt_prof_turno_w
from	qt_prof_turno
where	nr_seq_prof	= nr_seq_prof_p
and	dt_agenda_p between to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') 
				and to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
and	(ie_feriado_w	<> 'S' or (nvl(ie_feriado, 'S') = 'S' and ie_feriado_w = 'S'));

if	(qt_prof_turno_w	> 0) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end Qt_Obter_Se_Prof_Hor;
/