create or replace
function pls_obter_faixa_salarial
		(	nr_seq_faixa_salarial_p		number,
			ie_opcao_p			varchar2)
 		    	return varchar2 is

/*
ie_opcao:
	D = retorna a faixa et�ria em si - campo DS_FAIXA_SALARIAL
	N = retorna o n�vel da faixa salarial
*/

ds_retorno_w	pls_faixa_salarial.ds_faixa_salarial%type;
			
begin

ds_retorno_w := null;

if	(nr_seq_faixa_salarial_p is not null) then
	if	(ie_opcao_p = 'D') then
		select	ds_faixa_salarial
		into	ds_retorno_w
		from	pls_faixa_salarial
		where	nr_sequencia = nr_seq_faixa_salarial_p;
	elsif	(ie_opcao_p = 'N') then
		select	nr_nivel
		into	ds_retorno_w
		from	pls_faixa_salarial
		where	nr_sequencia = nr_seq_faixa_salarial_p;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_faixa_salarial;
/