create or replace
function pmdm_ie_intervalos_conflitam(	vl_i_faixa_a_p in number,
										vl_j_faixa_a_p in number,
										vl_i_faixa_b_p in number,
										vl_j_faixa_b_p in number)
							return varchar2 is
vl_min_a_w			number(15,4);
vl_max_a_w			number(15,4);
vl_min_b_w			number(15,4);
vl_max_b_w			number(15,4);
ie_faixa_conflita	varchar2(1 char)  := 'N';
begin

if (vl_i_faixa_a_p > vl_j_faixa_a_p) then
	vl_min_a_w := vl_j_faixa_a_p;
	vl_max_a_w := vl_i_faixa_a_p;
else
	vl_min_a_w := vl_i_faixa_a_p;
	vl_max_a_w := vl_j_faixa_a_p;
end if;

if (vl_i_faixa_b_p > vl_j_faixa_b_p) then
	vl_min_b_w := vl_j_faixa_b_p;
	vl_max_b_w := vl_i_faixa_b_p;
else
	vl_min_b_w := vl_i_faixa_b_p;
	vl_max_b_w := vl_j_faixa_b_p;
end if;

if (vl_min_a_w < vl_min_b_w and vl_min_b_w < vl_max_a_w) then
	ie_faixa_conflita := 'S';
elsif (vl_min_a_w < vl_max_b_w and vl_max_b_w < vl_max_a_w) then
	ie_faixa_conflita := 'S';
elsif (vl_min_b_w < vl_min_a_w and vl_min_a_w < vl_max_b_w) then
	ie_faixa_conflita := 'S';
elsif (vl_min_b_w < vl_max_a_w and vl_max_a_w < vl_max_b_w) then
	ie_faixa_conflita := 'S';
end if;

return ie_faixa_conflita;

end pmdm_ie_intervalos_conflitam;
/
