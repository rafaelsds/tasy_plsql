create or replace
function Obter_Solic_Ped_Exame(nr_sequencia_p	number)
 		    	return varchar2 is

ds_longo_w		varchar2(32000);
			
begin

begin
select 	ds_solicitacao
into 	ds_longo_w
from 	pedido_exame_externo
where	nr_sequencia = nr_sequencia_p;
exception
when others then
	ds_longo_w	:= '';
end;

return	substr(ds_longo_w,1,4000);

end Obter_Solic_Ped_Exame;
/