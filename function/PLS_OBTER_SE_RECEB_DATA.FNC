create or replace
function pls_obter_se_receb_data
		(	nr_seq_segurado_p	number,
			dt_recebimento_de_p	date,
			dt_recebimento_ate_p	date,
			ie_tipo_lote_p		varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(1);
qt_receb_data_w		number(3)	:= 0;

begin

ds_retorno_w := 'N';

select	count(*)
into	qt_receb_data_w
from	pls_carteira_emissao a,
	pls_lote_carteira b
where	a.nr_seq_lote = b.nr_sequencia
and	trunc(a.dt_recebimento,'dd') between trunc(dt_recebimento_de_p,'dd') and trunc(dt_recebimento_ate_p,'dd')
and	to_number(substr(pls_obter_dados_carteira(a.nr_seq_seg_carteira,null,'S'),1,10)) = nr_seq_segurado_p
and	b.ie_tipo_lote = ie_tipo_lote_p;

if 	(qt_receb_data_w > 0) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end pls_obter_se_receb_data;
/
