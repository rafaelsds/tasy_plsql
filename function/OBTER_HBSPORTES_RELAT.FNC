CREATE OR REPLACE
FUNCTION Obter_Hbsportes_Relat
			(	nr_cirurgia_p	number)
				return varchar2 is

cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
dt_cirurgia_w		date;
cd_convenio_w		number(10,0);
cd_edicao_amb_w		number(6);
nr_atendimento_w	number(10,0);
cd_retorno_w		number(5);

begin

select	cd_procedimento_princ,
	ie_origem_proced,
	nvl(dt_inicio_real, dt_inicio_prevista),
	nvl(nr_atendimento,0)
into	cd_procedimento_w,
	ie_origem_proced_w,
	dt_cirurgia_w,
	nr_atendimento_w
from	cirurgia
where	nr_cirurgia	= nr_cirurgia_p;

select	obter_convenio_atendimento(nr_atendimento_w)
into	cd_convenio_w
from	dual;

if	(ie_origem_proced_w	= 5) then
	select	max(a.nr_porte_anest)
	into	cd_retorno_w
	from	cbhpm_preco a
	where	a.cd_procedimento	= cd_procedimento_w
	and	a.ie_origem_proced	= ie_origem_proced_w
	and	a.dt_vigencia		=
		(select max(nvl(x.dt_vigencia,sysdate - 3650))
		from 	cbhpm_preco x
		where	x.cd_procedimento	= cd_procedimento_w
		and	x.ie_origem_proced	= ie_origem_proced_w
		and	x.dt_vigencia		<= dt_cirurgia_w);
else
	select	max(cd_edicao_amb)
	into	cd_edicao_amb_w
	from 	convenio_amb
	where 	(cd_convenio            = cd_convenio_w)
  	and 	(nvl(ie_situacao,'A')	= 'A')
  	and 	(dt_inicio_vigencia     =
	      	(select max(dt_inicio_vigencia)
	       	from 	convenio_amb a
       		where 	(a.cd_convenio         = cd_convenio_w)
	   	and 	(nvl(a.ie_situacao,'A')= 'A')
         	and 	(a.dt_inicio_vigencia <=  dt_cirurgia_w)));

	select	nvl(max(qt_porte_anestesico),0)
	into	cd_retorno_w
	from 	preco_amb a
	where	(a.cd_edicao_amb    	= cd_edicao_amb_w)
	and 	(a.cd_procedimento  	= cd_procedimento_w)
  	and 	nvl(a.dt_inicio_vigencia,sysdate - 3650)	=
			(select max(nvl(b.dt_inicio_vigencia,sysdate - 3650))
			from  	preco_amb b
			where 	b.cd_edicao_amb		= cd_edicao_amb_w
			and	b.cd_procedimento		= cd_procedimento_w
			and	nvl(b.dt_inicio_vigencia,sysdate - 3650) <= dt_cirurgia_w);
end if;

return	cd_retorno_w;

end obter_hbsportes_relat;
/