create or replace
function		obter_tipo_doc_origem_proc(
			nr_seq_aprovacao_p	number)
			return varchar2 is

ie_retorno_w		varchar2(2);
ie_ordem_w		varchar2(1);
ie_solic_w		varchar2(1);
nr_solic_compra_w		number(10);
ie_requisicao_w		varchar2(1);
ie_cotacao_w		varchar2(1);
ie_nota_w		varchar2(1);
ie_licitacao_w		varchar2(1);

begin

select	nvl(max('S'),'N')
into	ie_ordem_w
from	ordem_compra_item
where	nr_seq_aprovacao = nr_seq_aprovacao_p;

if	(ie_ordem_w = 'S') then
	ie_retorno_w	:= 'OC';
else
	begin

	select	nvl(max('S'),'N')
	into	ie_solic_w
	from	solic_compra_item
	where	nr_seq_aprovacao = nr_seq_aprovacao_p;

	if	(ie_solic_w = 'S') then
		begin

		select	max(nr_solic_compra)
		into	nr_solic_compra_w
		from	solic_compra_item
		where	nr_seq_aprovacao = nr_seq_aprovacao_p;

		select	nvl(ie_tipo_servico,'SC')
		into	ie_retorno_w
		from	solic_compra
		where	nr_solic_compra = nr_solic_compra_w;

		end;
	else
		begin

		select	nvl(max('S'),'N')
		into	ie_requisicao_w
		from	item_requisicao_material
		where	nr_seq_aprovacao = nr_seq_aprovacao_p;

		if	(ie_requisicao_w = 'S') then
			ie_retorno_w	:= 'RM';
		else
			begin

			select	nvl(max('S'),'N')
			into	ie_cotacao_w
			from	cot_compra_item
			where	nr_seq_aprovacao = nr_seq_aprovacao_p;

			if	(ie_cotacao_w = 'S') then
				ie_retorno_w	:= 'CC';
			else
				begin
				
				select	nvl(max('S'),'N')
				into	ie_nota_w
				from	nota_fiscal_item
				where	nr_seq_aprovacao = nr_seq_aprovacao_p;

				if	(ie_nota_w = 'S') then
					ie_retorno_w	:= 'NF';
				else					
					begin				
					select	nvl(max('S'),'N')
					into	ie_licitacao_w
					from	reg_lic_item
					where	nr_seq_aprovacao = nr_seq_aprovacao_p;

					if	(ie_licitacao_w = 'S') then
						ie_retorno_w	:= 'L';
					
					end if; /*Fim IR Licita��o*/
					end;
					
				end if; /*Fim IF Nota*/	
			
				end;
			end if; /*Fim IF Cota��o*/

			end;
		end if; /*Fim IF Requisi��o*/

		end;
	end if; /*Fim IF Solicita��o*/

	end;
end if; /*Fim IF Ordem Compra*/

return	ie_retorno_w;

end obter_tipo_doc_origem_proc;
/
