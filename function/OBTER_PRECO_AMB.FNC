create or replace
function OBTER_PRECO_AMB
		(cd_procedimento_p	number,
		cd_convenio_p		number,
		cd_categoria_p		varchar2,
		cd_estabelecimento_p	number,
		ie_opcao_p		varchar2) return number is

/*
ie_opcao_p:

'P' - vl_procedimento
'CO' - vl_custo_operacional
'M' - vl_medico
'A' - vl_anestesista
'F' - vl_filme
'MC' - vl_medico + vl_custo_operacional

*/

CD_EDICAO_AMB_w			number(6,0);
VL_CUSTO_OPERACIONAL_W		number(15,2);
VL_MEDICO_W			number(15,2);
VL_FILME_W			number(15,2);
NR_PORTE_ANESTESICO_W		number(2,0);
NR_AUXILIARES_W			number(2,0);
VL_AUXILIARES_INFORM_W		number(15,2);
VL_PTO_CUSTO_OPERAC_W		number(15,2);
VL_PTO_MEDICO_W			number(15,2);
vl_procedimento_W		number(15,2);
CD_MOEDA_W			number(2,0);
VL_ANEST_INFORM_W		number(15,2);

begin

SELECT	max(nvl(CD_EDICAO_AMB, 0))
INTO	CD_EDICAO_AMB_W
FROM	CONVENIO_AMB
WHERE	CD_ESTABELECIMENTO	= CD_ESTABELECIMENTO_P
AND	CD_CONVENIO		= CD_CONVENIO_P
AND	CD_CATEGORIA		= CD_CATEGORIA_P
AND	nvl(IE_SITUACAO,'A')	= 'A'
AND	DT_INICIO_VIGENCIA	=
	(SELECT	MAX(DT_INICIO_VIGENCIA)
	FROM	CONVENIO_AMB A
	WHERE	A.CD_ESTABELECIMENTO  = CD_ESTABELECIMENTO_P
	AND	A.CD_CONVENIO         = CD_CONVENIO_P
	AND	A.CD_CATEGORIA        = CD_CATEGORIA_P
	AND	nvl(A.IE_SITUACAO,'A')= 'A');

begin
SELECT	nvl(a.VL_CUSTO_OPERACIONAL,0),
	nvl(a.VL_MEDICO,0),
	nvl(a.QT_FILME,0),
	nvl(a.QT_PORTE_ANESTESICO,0),
	nvl(a.NR_AUXILIARES,0),
	NVL(a.VL_AUXILIARES,0),
	nvl(a.VL_CUSTO_OPERACIONAL,0),
	nvl(a.VL_MEDICO,0),
	a.CD_MOEDA,
	NVL(a.VL_ANESTESISTA,0),
	nvl(a.vl_procedimento,0)
INTO	VL_CUSTO_OPERACIONAL_W,
	VL_MEDICO_W,
	VL_FILME_W,
	NR_PORTE_ANESTESICO_W,
	NR_AUXILIARES_W,
	VL_AUXILIARES_INFORM_W,
	VL_PTO_CUSTO_OPERAC_W,
	VL_PTO_MEDICO_W,
	CD_MOEDA_W,
	VL_ANEST_INFORM_W,
	vl_procedimento_w
FROM	PRECO_AMB A
WHERE	A.CD_PROCEDIMENTO  	= CD_PROCEDIMENTO_P
and	a.CD_EDICAO_AMB		= CD_EDICAO_AMB_w
and	nvl(a.dt_inicio_vigencia,sysdate - 3650) =
				(select	max(nvl(b.dt_inicio_vigencia,sysdate - 3650))
				from	preco_amb b
				where	b.cd_procedimento	= cd_procedimento_p
				and	b.CD_EDICAO_AMB		= CD_EDICAO_AMB_w);
exception
	when others then
		vl_procedimento_w		:= 0;
		VL_CUSTO_OPERACIONAL_W	:= 0;
		VL_PTO_MEDICO_W		:= 0;
		VL_ANEST_INFORM_W		:= 0;
end;

if	(ie_opcao_p	= 'P') then
	return vl_procedimento_w;
elsif	(ie_opcao_p	= 'CO') then
	return VL_CUSTO_OPERACIONAL_W;
elsif	(ie_opcao_p	= 'M') then
	return VL_PTO_MEDICO_W;
elsif	(ie_opcao_p	= 'A') then
	return VL_ANEST_INFORM_W;
elsif	(ie_opcao_p	= 'MC') then
	return VL_PTO_MEDICO_W + VL_CUSTO_OPERACIONAL_W;
elsif	(ie_opcao_p	= 'F') then
	return	vl_filme_w;
elsif	(ie_opcao_p	= 'NR') then
	return	nr_auxiliares_w;
else
	return	0;
end if;

end;
/
