create or replace
function pls_obter_se_tit_rec_camara
			(	ie_origem_titulo_p	number)
				return varchar2 is
				
ie_retorno_w		varchar2(1)	:= 'S';

Cursor C01 is
	select	ie_permite_inclusao
	from	pls_regra_tit_rec_camara
	where	nvl(ie_origem_titulo, ie_origem_titulo_p)	= ie_origem_titulo_p;

begin

open C01;
loop
fetch C01 into	
	ie_retorno_w;
exit when C01%notfound;
end loop;
close C01;

return	ie_retorno_w;

end pls_obter_se_tit_rec_camara;
/