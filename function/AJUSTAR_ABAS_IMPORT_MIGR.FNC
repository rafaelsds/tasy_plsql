create or replace
function ajustar_abas_import_migr (nm_obj_aba_p	varchar2) 
	return varchar2 is

nm_obj_aba_w		varchar2(50);
nr_pos_underscore_w	number(2,0);
nm_obj_aba_ww		varchar2(50);

begin
if	(nm_obj_aba_p is not null) then
	begin
	nm_obj_aba_w		:= lower(nm_obj_aba_p);
	nm_obj_aba_w 		:= replace(nm_obj_aba_w,'_ts','');
	nr_pos_underscore_w	:= instr(nm_obj_aba_w,'_');
	
	while (nr_pos_underscore_w > 0) loop
		begin
		nm_obj_aba_ww		:= substr(nm_obj_aba_w,1,nr_pos_underscore_w - 1);
		nm_obj_aba_ww		:= nm_obj_aba_ww || initcap(substr(nm_obj_aba_w,nr_pos_underscore_w + 1,length(nm_obj_aba_w)));
		nm_obj_aba_w		:= nm_obj_aba_ww;
		nr_pos_underscore_w	:= instr(nm_obj_aba_w,'_');
		end;
	end loop;
	
	nm_obj_aba_w := obter_initcap_classe_java(nm_obj_aba_w);
	nm_obj_aba_w := nm_obj_aba_w || 'WJP';
	end;
end if;
return nm_obj_aba_w;
end ajustar_abas_import_migr;
/