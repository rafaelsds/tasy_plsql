create or replace
function pls_obter_dados_medico_prest	(	cd_medico_p			medico.cd_pessoa_fisica%type,
						cd_especialidade_medica_p	medico_especialidade.cd_especialidade%type,
						ie_opcao_p			varchar2)	return varchar2 is

/*
_*_*_*_*_*_*_*_*_*_IE_OPCAO_P_*_*_*_*_*_*_*_*_*_
	* PG	= P�s - Gradua��o
*/

ds_retorno_w	varchar2(255);
ie_opcao_w	varchar2(255);

begin

select	upper(ie_opcao_p)
into	ie_opcao_w
from	dual;

if	(ie_opcao_w = 'PG') then
	select	max(e.ie_pos_grad_360)
	into	ds_retorno_w
	from	medico_especialidade	e
	where	e.cd_pessoa_fisica	= cd_medico_p
	and	((cd_especialidade_medica_p is null) or
		e.cd_especialidade	= cd_especialidade_medica_p);
end if;

return	ds_retorno_w ;

end pls_obter_dados_medico_prest ;
/