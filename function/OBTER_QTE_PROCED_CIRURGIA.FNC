create or replace
FUNCTION obter_qte_proced_cirurgia (	cd_procedimento_p		NUMBER,
					ie_origem_proced_p		NUMBER,
					dt_procedimento_p		DATE,
					dt_inicio_procedimento_p	DATE)
					RETURN NUMBER IS

nr_dias_w	NUMBER(9);
nr_horas_w	NUMBER(14,7);
nr_minutos_w	NUMBER(9);
ie_forma_apresentacao_w	NUMBER(2); 

nr_horas_ww	NUMBER(14,7);


BEGIN

nr_dias_w 	:= TRUNC(dt_procedimento_p - dt_inicio_procedimento_p);
nr_horas_w	:= TRUNC((dt_procedimento_p - dt_inicio_procedimento_p) * 24);
nr_minutos_w	:= (((dt_procedimento_p - dt_inicio_procedimento_p) * 24)) * 60;

nr_horas_ww	:= (dt_procedimento_p - dt_inicio_procedimento_p) * 24;

if	(mod(nr_horas_ww,2) > 0) then
	nr_horas_ww:= trunc(nr_horas_ww) + 1;
end if;

SELECT	max(ie_forma_apresentacao)
INTO	ie_forma_apresentacao_w
FROM	procedimento
WHERE	cd_procedimento		= cd_procedimento_p
AND	ie_origem_proced	= ie_origem_proced_p;

IF	(ie_forma_apresentacao_W = 2) THEN
	RETURN nr_minutos_w;
ELSIF	(ie_forma_apresentacao_W = 3) THEN
	RETURN ((nr_dias_w * 24) + nr_horas_w);
ELSIF	(ie_forma_apresentacao_w = 10) THEN
	if	((nr_minutos_w / 30) > round(nr_minutos_w / 30)) then
		RETURN nvl(round((nr_minutos_w / 30)) + 1,0);
	else
		RETURN nvl(round((nr_minutos_w / 30)),0);
	end if;
ELSIF	(ie_forma_apresentacao_W = 11) THEN
	RETURN nr_horas_ww;
ELSIF	(ie_forma_apresentacao_W = 12) THEN
	RETURN mod(trunc(nr_minutos_w),60);
ELSIF	(ie_forma_apresentacao_W = 13) THEN
	if	(mod(trunc(nr_minutos_w),60) > 0) then
		if	(mod(trunc(nr_minutos_w),60) > 30) then
			RETURN 2;
		else
			RETURN 1;
		end if;
	else		
		RETURN 0;
	end if;	
ELSIF	(ie_forma_apresentacao_W = 14) THEN
	if	(nr_minutos_w <= 60) then
		return 1;	
	else
		nr_minutos_w := nr_minutos_w - 60;
		if	((nr_minutos_w / 30) > round(nr_minutos_w / 30)) then
			RETURN nvl(round((nr_minutos_w / 30)) + 1,0);
		else
			RETURN nvl(round((nr_minutos_w / 30)),0);
		end if;
	end if;
ELSIF	(ie_forma_apresentacao_W = 15) THEN
	if	(nr_minutos_w <= 75) then
		return 1;	
	else
		nr_minutos_w := nr_minutos_w - 75;
		
		if	((nr_minutos_w / 60) > round(nr_minutos_w / 60)) then
			RETURN nvl(round((nr_minutos_w / 60)) + 1,0) + 1;
		else
			RETURN nvl(round((nr_minutos_w / 60)),0) + 1;
		end if;
	end if;	
ELSIF	(ie_forma_apresentacao_W = 17) THEN
	if	(nr_minutos_w <= 60) then
		return 0;
	else
		return nr_minutos_w - 60;
	end if;
END 	IF;

END	obter_qte_proced_cirurgia;
/
