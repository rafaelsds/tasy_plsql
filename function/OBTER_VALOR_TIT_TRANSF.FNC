create or replace
function obter_valor_tit_transf (nr_titulo_p	number,
				dt_baixa_p	date,
				ie_tipo_tit	varchar)
				return varchar is

ds_retorno_w		varchar2(255);
vl_titulo_w		number(15,2);
vl_alteracao_w		number(15,2);
nr_seq_alteracao_w	varchar2(5);
dt_baixa_w		date;

begin

if	(ie_tipo_tit = 'CR') then

	select	a.vl_titulo
	into	vl_titulo_w
	from	titulo_receber a
	where	a.nr_titulo	= nr_titulo_p;

	select	max(nr_sequencia)
	into	nr_seq_alteracao_w
	from	alteracao_valor
	where	nr_titulo	= nr_titulo_p;

	dt_baixa_w	:= fim_dia(dt_baixa_p);
	
	select	nvl(sum(decode(a.ie_aumenta_diminui, 'A', a.vl_alteracao, a.vl_alteracao * -1)),0)
	into	vl_alteracao_w
	from	alteracao_valor a
	where	a.nr_titulo	= nr_titulo_p
	and	a.nr_sequencia 	<> nr_seq_alteracao_w
	and	a.dt_alteracao	<= dt_baixa_w;

elsif	(ie_tipo_tit = 'CP') then

	select	a.vl_titulo
	into	vl_titulo_w
	from	titulo_pagar a
	where	a.nr_titulo	= nr_titulo_p;

	dt_baixa_w	:= fim_dia(dt_baixa_p);

	SELECT	NVL(SUM(vl_anterior - vl_alteracao),0)
	INTO	vl_alteracao_w
	FROM	titulo_pagar_alt_valor
	WHERE	dt_alteracao	<= dt_baixa_w
	and	nr_titulo	= nr_titulo_p;

end if;

ds_retorno_w	:= vl_titulo_w + vl_alteracao_w;

return	ds_retorno_w;

end obter_valor_tit_transf;
/