CREATE OR REPLACE 
function obter_ref_prescr( nr_prescricao_p number,
			  nr_seq_exame_p number,
			  nr_seq_material_p number)
 		    	return varchar2 is

qt_dias_w	NUMBER(10,2)	:= 0;
qt_horas_w	NUMBER(10,0)	:= 0;
qt_minima_w	number(10,4);
qt_maxima_w	number(10,4);
pr_minimo_w	number(10,4);
pr_maximo_w	number(10,4);
ds_observacao_w	varchar2(2000);
ds_refer_w	varchar2(2000);
ie_sexo_w	varchar2(1);
cd_estabelecimento_W	prescr_medica.cd_estabelecimento%type;
nr_seq_metodo_w	metodo_exame_lab.nr_sequencia%type;
qt_casas_decimais_dias_w	number(10,0) := 2;

begin

select	obter_dias_entre_datas_lab(obter_nascimento_prescricao(nr_prescricao_p),sysdate),
	substr(obter_sexo_prescricao(nr_prescricao_p),1,1),
	Obter_Hora_Entre_datas(obter_nascimento_prescricao(nr_prescricao_p),sysdate)
into	qt_dias_w,
	ie_sexo_w,
	qt_horas_w
from	dual;

begin

select 	obter_metodo_regra_material(nr_prescricao_p,nr_seq_exame_p, nr_seq_material_p)
into	nr_seq_metodo_w
from	dual;

exception
	when others then
		nr_seq_metodo_w := null;
end;		


select	nvl(max(cd_estabelecimento),0)
into	cd_estabelecimento_w
from 	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select	decode(nvl(max(ie_idade_int_val_ref), 'N'), 'N', 2, 0)
into	qt_casas_decimais_dias_w
from	lab_parametro
where	cd_estabelecimento = cd_estabelecimento_w;

select 	qt_minima,
	qt_maxima,
	qt_percent_min,
	qt_percent_max,
	ds_observacao
into	qt_minima_w,
	qt_maxima_w,
	pr_minimo_w,
	pr_maximo_w,
	ds_observacao_w
from (
	select 	qt_minima,
		qt_maxima,
		qt_percent_min,
		qt_percent_max,
		ds_observacao
	from 	exame_lab_padrao
	where 	((ie_sexo = ie_sexo_w) or (ie_sexo = '0'))
	and 	nr_seq_exame = nr_seq_exame_p
	and 	nvl(nr_seq_material,nr_seq_material_p) = nr_seq_material_p
	and nvl(nr_seq_metodo, nvl(nr_seq_metodo_w,0)) = nvl(nr_seq_metodo_w,0)
	and 	(((trunc((qt_dias_w / 365.25),qt_casas_decimais_dias_w) between qt_idade_min and qt_idade_max) and (ie_periodo = 'A')) or
		((trunc(((qt_dias_w / 365.25) * 12),qt_casas_decimais_dias_w) between qt_idade_min and qt_idade_max) and (ie_periodo = 'M')) or
		((qt_dias_w between qt_idade_min and qt_idade_max) and (ie_periodo = 'D')) or
		((qt_horas_w between qt_idade_min and qt_idade_max) and (ie_periodo = 'H')))
	and 	ie_tipo_valor in (0,3)
	and nvl(ie_situacao,'A') = 'A'
	order by nvl(nr_seq_material, 9999999999), nvl(nr_seq_metodo, 9999999999), ie_sexo, decode(ie_periodo,'D',1,'M',2,3))
	where rownum = 1;


if	(qt_minima_w >= 0) and
	(qt_maxima_w > 0) then
	ds_refer_w := TO_number(qt_minima_w) || ' a '|| TO_number(qt_maxima_w);
elsif	(pr_minimo_w >= 0) and
	(pr_maximo_w > 0) then
	ds_refer_w := TO_number(pr_minimo_w) || ' a '|| TO_number(pr_maximo_w);
else
	ds_refer_w := ds_observacao_w;
end if;

return	ds_refer_w;

end obter_ref_prescr;
/
