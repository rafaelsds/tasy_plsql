create or replace
function obter_data_internacao_dt	(nr_atendimento_p		number)
		return date is

dt_entrada_w	date;

begin

select	min(a.dt_entrada_unidade)
into	dt_entrada_w
from	setor_atendimento b,
	atend_paciente_unidade a
where	a.nr_atendimento		= nr_atendimento_p
and	a.cd_setor_atendimento	= b.cd_setor_atendimento
and	b.cd_classif_setor		in ('3','4');

return	dt_entrada_w;

end obter_data_internacao_dt;
/
