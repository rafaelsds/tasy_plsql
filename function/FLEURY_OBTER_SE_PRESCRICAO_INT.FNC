create or replace
function fleury_obter_se_prescricao_int (nr_prescricao_p	number)
 		    	return varchar2 is

ds_retorno_w			varchar2(1);
qt_registros_w			number(10);

ie_agrupa_ficha_fleury_w	lab_parametro.ie_agrupa_ficha_fleury%type;
nr_controle_w			prescr_medica.nr_controle%type;
			
begin
ds_retorno_w	:= 'N';

if	(obter_se_existe_evento(106) = 'S') then

	select	nvl(max(ie_agrupa_ficha_fleury),'N') ds 
	into	ie_agrupa_ficha_fleury_w
	from	lab_parametro a 
	where	a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	select	max(a.nr_controle)
	into	nr_controle_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p;

	if	(nr_controle_w is null) or (ie_agrupa_ficha_fleury_w <> 'N') then

		select	count(*)
		into	qt_registros_w
		from	prescr_procedimento a
		where	a.nr_seq_exame is not null 
		and	a.nr_prescricao = nr_prescricao_p;

		if	(qt_registros_w <> 0) and
			(obter_se_int_prescr_fleury(nr_prescricao_p) <> '0') then
			ds_retorno_w	:= 'S';
		else
			select	count(*)
			into	qt_registros_w
			from	prescr_procedimento a
			where	a.nr_seq_exame is null 
			and	a.nr_prescricao = nr_prescricao_p;
			
			if	(qt_registros_w <> 0) and
				(obter_se_int_pr_fleury_diag(nr_prescricao_p) <> '0') then
				ds_retorno_w	:= 'S';
			end if;
		end if;
	end if;
end if;

return	ds_retorno_w;

end fleury_obter_se_prescricao_int;
/