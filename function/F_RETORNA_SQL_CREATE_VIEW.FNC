create or replace function f_retorna_sql_create_view return varchar2 is
  
ds_comando_sql_w	varchar2(32000);
ds_campos_select	varchar2(16000);
ds_campo_temp		varchar2(200);
enter_w 		varchar2(10) := chr(13) || chr(10);
    
cursor C01 is
select	nr_sequencia_criacao, 
	nm_atributo,
	vl_default,
	ie_tipo_atributo
from    tabela_atributo
where   nm_tabela = 'PLS_PARAMETROS'
order by nr_sequencia_criacao;
    
begin
-- Essa fun��o existe apenas para automatizar a cria��o da view pls_parametros_v.
-- Esta view � requisito para toda essa package.
-- Se for alterado alguma coisa na tabela pls_parametros essa function deve ser chamada
-- e o retorno dela sera o comando para a cria��o da nova view.
  
ds_campos_select := ' ';
for r_C01_w in C01 loop
      
	-- todos os par�metros que trabalham com @ n�o s�o recuperados aqui, entram pelo caminho normal
	-- motivo: existe a substitui��o desses valores em tempo de execu��o
	if  (r_C01_w.vl_default is not null and r_C01_w.vl_default not like '@%') then
      
		ds_campo_temp := 'nvl(' || lower(r_C01_w.nm_atributo) || ',';
          
		-- tratamento para convers�o de campos/tipos de dados
		if      (r_C01_w.ie_tipo_atributo = 'DATE') then
		
			if  (r_C01_w.vl_default = 'SYSDATE') then
				ds_campo_temp := ds_campo_temp || r_C01_w.vl_default;
			else
				ds_campo_temp := ds_campo_temp || 'to_date(''' || r_C01_w.vl_default || ''')';
			end if;
		
		elsif   (r_C01_w.ie_tipo_atributo = 'NUMBER') then
		
			ds_campo_temp := ds_campo_temp || 'to_number(' || r_C01_w.vl_default || ')';
		else
			ds_campo_temp := ds_campo_temp || '''' || r_C01_w.vl_default || '''';
		end if;
          
		ds_campo_temp := ds_campo_temp || ') ' || lower(r_C01_w.nm_atributo);
      
	else
		ds_campo_temp := lower(r_C01_w.nm_atributo);
	end if;
      
	ds_campos_select := ds_campos_select || ds_campo_temp || ', ' || enter_w;
end loop;
    
-- tratamento para tirar a v�rgula e espa�o
ds_campos_select := substr(ds_campos_select, 0, (length(ds_campos_select) - 4));
ds_comando_sql_w := 'Create or replace view pls_parametros_v as ' || enter_w;
ds_comando_sql_w := ds_comando_sql_w || 'select ' || ds_campos_select || enter_w;
ds_comando_sql_w := ds_comando_sql_w || ' from pls_parametros' || enter_w;
    
return ds_comando_sql_w;
    
end f_retorna_sql_create_view;
/