create or replace function obter_vl_juros_pago_contrato(nr_seq_parcela_p	number)
	return number is

vl_juros_w	number(15,2) := 0;

begin

select	nvl(max(a.vl_juros),0)
into	vl_juros_w
from	emprest_financ_parc a
where	a.nr_sequencia = nr_seq_parcela_p
and	a.dt_cancelamento is null
and	exists (select 1
		from titulo_pagar x
		where x.nr_titulo = a.nr_titulo
		and x.dt_liquidacao is not null);

return	vl_juros_w;

end obter_vl_juros_pago_contrato;
/
