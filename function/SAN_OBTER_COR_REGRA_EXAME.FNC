create or replace
function san_obter_cor_regra_exame ( 	ds_resultado_p	varchar2,
					nr_seq_exame_p	number)

 		    	return varchar2 is

ds_cor_w	varchar2(15);
vl_resultado_w	number(15,4);

begin

if  ((ds_resultado_p is not null) and (nr_seq_exame_p is not null)) then
	
	begin
	vl_resultado_w := to_number(ds_resultado_p);
	exception
	when others then
		vl_resultado_w := 0;
	end;	
	
	select	max(ds_cor)    
	into	ds_cor_w
	from    san_regra_cor_resultado a  
	where   (upper(a.ds_resultado)  = upper(ds_resultado_p)
	or	(vl_resultado_w > 0
	and	(vl_resultado_min is not null or vl_resultado_max is not null)
	and	vl_resultado_w between nvl(vl_resultado_min,vl_resultado_w) and nvl(vl_resultado_max,vl_resultado_w)))
	and	a.nr_seq_exame	= nr_seq_exame_p; 

end if;
return	ds_cor_w;


end san_obter_cor_regra_exame;
/