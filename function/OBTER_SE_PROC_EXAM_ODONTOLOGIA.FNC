create or replace
function obter_se_proc_exam_odontologia(nr_seq_exame_p	number,
										cd_procedimento_p	number,
										ie_origem_proced_p	number,
										nr_proc_interno_p	number)
										return varchar2 is

cd_procedimento_w		med_exame_padrao.cd_procedimento%type := 0;
ie_origem_proced_w		med_exame_padrao.ie_origem_proced%type := 0;
nr_proc_interno_w		med_exame_padrao.nr_proc_interno%type := 0;
ds_retorno_w			varchar2(1) := 'N';
ds_retorno1_w			varchar2(1) := 'N';
ds_retorno2_w			varchar2(1) := 'N';		

begin

if (nvl(nr_seq_exame_p, 0) > 0) then

	select 	nvl(max(cd_procedimento), 0),
			nvl(max(ie_origem_proced), 0),
			nvl(max(nr_proc_interno), 0)	
	into 	cd_procedimento_w,
			ie_origem_proced_w,
			nr_proc_interno_w
	from 	med_exame_padrao
	where 	nr_sequencia = nr_seq_exame_p;
	
	ds_retorno1_w := obter_se_proc_odontologia(cd_procedimento_w, ie_origem_proced_w, nr_proc_interno_w);

end if;

ds_retorno2_w := obter_se_proc_odontologia(cd_procedimento_p, ie_origem_proced_p, nr_proc_interno_p);


if (ds_retorno1_w = 'S') or
	(ds_retorno2_w = 'S') then

	ds_retorno_w := 'S';

end if;

return	ds_retorno_w;

end obter_se_proc_exam_odontologia;
/