create or replace
function obter_dados_operacao_estoque(	cd_operacao_estoque_p	number,
					ie_tipo_p			varchar2)
return varchar2 is

ds_retorno_w				varchar2(40)	:= '';
ie_entrada_saida_w				varchar2(01)	:= '';
ie_consignado_w				varchar2(01)	:= '';
ie_tipo_requisicao_w			varchar2(3)	:= '';	

/* E - Entrada/Sa�da 
   C - ie_consignado
   O - Tipo de opera��o.

*/

begin

if	(cd_operacao_estoque_p is not null) then

	select	ie_entrada_saida,
		ie_consignado,
		ie_tipo_requisicao
	into	ie_entrada_saida_w,
		ie_consignado_w,
		ie_tipo_requisicao_w
	from	operacao_estoque
	where	cd_operacao_estoque = cd_operacao_estoque_p;

	if	(ie_tipo_p = 'E') then
		ds_retorno_w			:= ie_entrada_saida_w;
	elsif	(ie_tipo_p = 'C') then
		ds_retorno_w			:= ie_consignado_w;
	elsif	(ie_tipo_p = 'O') then
		ds_retorno_w			:= ie_tipo_requisicao_w;
	end if;
	
end if;
return	ds_retorno_w;
end obter_dados_operacao_estoque;
/