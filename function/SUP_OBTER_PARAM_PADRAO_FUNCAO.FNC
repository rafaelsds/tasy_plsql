create or replace
function sup_obter_param_padrao_funcao
			(	nr_parametro_p	number,
				cd_funcao_p 	number
			)
				return 		varchar2 is			
				
ds_retorno_w	varchar2(255);

begin

select	nvl(a.vl_parametro,vl_parametro_padrao) vl_parametro
into	ds_retorno_w
from 	funcao_parametro a,
	funcao b	
where 	a.cd_funcao = b.cd_funcao
and	a.nr_sequencia = nr_parametro_p
and	b.cd_funcao = cd_funcao_p;

return	ds_retorno_w;

end sup_obter_param_padrao_funcao;
/
