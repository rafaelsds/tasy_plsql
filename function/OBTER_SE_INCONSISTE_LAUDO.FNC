create or replace
function obter_se_inconsiste_laudo (	nr_seq_laudo_p		number,
				ie_tipo_consiste_p		number) 		
				return varchar2 is

qt_inconsistencia_laudo_w		number(10);
ie_inconsiste_w			varchar2(1) := 'N';

/*
ie_tipo_consiste_p

1 - Todas inconsist�ncia ativas do laudo
2 - Somente as ativas e que n�o permitem liberar o laudo
*/

begin

if	(nr_seq_laudo_p is not null) and
	(ie_tipo_consiste_p is not null) then
	begin

	if	(ie_tipo_consiste_p = 1) then
		begin
		select	count(*)
		into	qt_inconsistencia_laudo_w
		from	sus_inconsistencia_laudo b,
			sus_inco_reg_laudo	 a
		where	a.nr_seq_inconsistencia = b.nr_sequencia
		and	a.nr_seq_laudo = nr_seq_laudo_p
		and	nvl(sus_obter_se_cons_laudo_estab(b.nr_sequencia,b.cd_estabelecimento,'C'),b.ie_consiste) =  'S';
		end;
	elsif	(ie_tipo_consiste_p = 2) then
		begin
		select	count(*)
		into	qt_inconsistencia_laudo_w
		from    sus_inconsistencia_laudo   b,
			sus_inco_reg_laudo a 
		where	a.nr_seq_inconsistencia = b.nr_sequencia 
		and	a.nr_seq_laudo = nr_seq_laudo_p
		and	nvl(sus_obter_se_cons_laudo_estab(b.nr_sequencia,b.cd_estabelecimento,'C'),b.ie_consiste) = 'S'
		and	nvl(sus_obter_se_cons_laudo_estab(b.nr_sequencia,b.cd_estabelecimento,'L'),b.ie_permite_liberar) = 'N';
		end;
	end if;		

	if	(qt_inconsistencia_laudo_w > 0) then
		begin
		
		ie_inconsiste_w	:= 'S';
		
		end;
	end if;

	end;
end if;

return	ie_inconsiste_w;

end obter_se_inconsiste_laudo;
/
