create or replace
function obter_descricao_doc_loc(nr_seq_loc_p		number)
 		    	return varchar2 is

retorno_w	 	varchar2(200);			
begin

select 	max(DS_LOCALIZACAO)
into  	retorno_w
from 	QUA_LOC_DOC 
where 	nr_sequencia = nr_seq_loc_p;

return	retorno_w;

end obter_descricao_doc_loc;
/
