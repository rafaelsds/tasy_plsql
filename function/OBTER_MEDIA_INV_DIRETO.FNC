create or replace
function obter_media_inv_direto(dt_mesano_referencia_p		date,
				    cd_material_p	number,
				    cd_estabelecimento_p	number) 
 		    	return number is

qt_estoque_w	number(13,4);
qt_movimento_w	number(13,4);
qt_media_w	number(15,4);
			
begin

select	nvl(sum(qt_estoque),0)
into	qt_estoque_w
from	movimento_estoque_v2 a,
	operacao_estoque b
where	a.cd_operacao_estoque	= b.cd_operacao_estoque
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.dt_mesano_referencia	= dt_mesano_referencia_p
and	b.ie_tipo_requisicao		= 5
and	a.cd_material_estoque	= cd_material_p;

select	count(nr_movimento_estoque)
into	qt_movimento_w
from	movimento_estoque_v2 a,
	operacao_estoque b
where	a.cd_operacao_estoque	= b.cd_operacao_estoque
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.dt_mesano_referencia	= dt_mesano_referencia_p
and	b.ie_tipo_requisicao	= 5
and	a.cd_material_estoque	= cd_material_p;

select	dividir(qt_estoque_w,qt_movimento_w)
into	qt_media_w 
from	dual;

return	qt_media_w;

end obter_media_inv_direto;
/