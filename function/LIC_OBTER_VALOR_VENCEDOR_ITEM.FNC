create or replace
function lic_obter_valor_vencedor_item(	nr_seq_licitacao_p			number,
				nr_seq_lic_item_p			number)
 		    	return number is

vl_vencedor_w			number(15,4);
nr_lote_compra_w		varchar2(80);
			
begin

select	nvl(max(vl_item),0)
into	vl_vencedor_w
from	reg_lic_vencedor
where	nr_seq_licitacao	= nr_seq_licitacao_p
and	nr_seq_lic_item	= nr_seq_lic_item_p;

if	(vl_vencedor_w = 0) then
	
	select	max(nr_lote_compra)
	into	nr_lote_compra_w
	from	reg_lic_item
	where	nr_seq_licitacao	= nr_seq_licitacao_p
	and	nr_seq_lic_item	= nr_seq_lic_item_p;
	
	if	(nr_lote_compra_w is not null) then

		select	max(lic_obter_vl_fim_item_lote(nr_seq_licitacao, nr_seq_lic_item,  lic_obter_forn_vencedor_item(nr_seq_licitacao, nr_seq_lic_item)))
		into	vl_vencedor_w
		from	reg_lic_item
		where	nr_seq_licitacao	= nr_seq_licitacao_p
		and	nr_lote_compra		= nr_lote_compra_w
		and	nr_seq_lic_item		= nr_seq_lic_item_p
		and      nvl(ie_lote,'N')	= 'N';
	end if;
end if;

return	vl_vencedor_w;

end lic_obter_valor_vencedor_item;
/
