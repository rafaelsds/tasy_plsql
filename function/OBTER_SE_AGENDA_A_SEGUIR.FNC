create or replace
function obter_se_agenda_a_seguir( 	nr_seq_agenda_p	number )
					return varchar2 is

ie_as_w		varchar2(1);
hr_inicio_w	date;
hr_inicio_ww	date;
cd_medico_w	varchar2(10);
			
begin

select	a.hr_inicio,
	a.cd_medico
into	hr_inicio_w,
	cd_medico_w
from	agenda_paciente a
where	a.nr_sequencia	=	nr_seq_agenda_p;	

hr_inicio_ww	:= hr_inicio_w - (10/1440);

select	nvl(max('S'),'N')
into	ie_as_w
from	agenda_paciente x
where	x.hr_inicio between hr_inicio_ww  and hr_inicio_w 
and	x.cd_medico	= cd_medico_w
and	x.nr_sequencia	<> nr_seq_agenda_p;

return	ie_as_w;	

end obter_se_agenda_a_seguir;
/
