create or replace
function obter_local_aplic_ccg(	nr_sequencia_p		number)
								return varchar2 is

ie_local_aplicacao_w		varchar2(15);
ie_primeiro_local_w			varchar2(15);
ie_local_anterior_w			varchar2(15);
nr_seq_protocolo_w			number(10);
nr_seq_glicemia_w			number(10);
nr_seq_anterior_w			number(10);
ds_local_w					varchar2(255);
						
cursor c01 is
Select	a.ie_local_aplicacao
from	prot_glic_local_insulina a
where	a.nr_seq_protocolo_glic = nr_seq_protocolo_w
and		a.ie_local_aplicacao is not null
order by	a.nr_seq_prioridade;
				
begin

select	max(nr_seq_protocolo),
		max(nr_seq_glicemia)
into	nr_seq_protocolo_w,
		nr_seq_glicemia_w
from	atendimento_glicemia
where	nr_sequencia = nr_sequencia_p;

open C01;
loop
fetch C01 into	
	ie_primeiro_local_w;
exit when C01%notfound;
	begin
	exit;
	end;
end loop;
close C01;

Select	nvl(max(nr_sequencia),0)
into	nr_seq_anterior_w
from	atendimento_glicemia
where	nr_seq_glicemia = nr_seq_glicemia_w
and		nr_sequencia 	< nr_sequencia_p;

Select	max(ie_local_aplicacao)
into	ie_local_anterior_w
from	atend_glicemia_evento
where	nr_seq_glicemia = nr_seq_anterior_w;

if	(ie_local_anterior_w	is not null) then
	Select	max(a.ie_local_aplicacao)
	into	ie_local_aplicacao_w
	from	prot_glic_local_insulina a
	where	a.nr_seq_protocolo_glic = nr_seq_protocolo_w
	and		nr_seq_prioridade > (Select max(b.nr_seq_prioridade)
								 from 	prot_glic_local_insulina b
								 where	b.nr_seq_protocolo_glic = nr_seq_protocolo_w
								 and	b.ie_local_aplicacao = ie_local_anterior_w)
	and 	rownum = 1;
end if;

if	(ie_local_aplicacao_w	is null) then
	ie_local_aplicacao_w	:= ie_primeiro_local_w;
end if;

if	(ie_local_aplicacao_w	is not null) then
	ds_local_w	:= substr(obter_valor_dominio(6166,ie_local_aplicacao_w),1,50);
end if;

return	ds_local_w;

end obter_local_aplic_ccg;
/