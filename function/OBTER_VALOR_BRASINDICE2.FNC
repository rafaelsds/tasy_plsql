create or replace function
obter_valor_brasindice2 (	cd_laboratorio_p 	varchar2,
				cd_medicamento_p	varchar2,
				cd_apresentacao_p	varchar2,
				qt_conversao_p		number,
				cd_material_p		number,
				ie_opcao_p		varchar2)
			return number is

/* ie_opcao_p:	'C' - Convertido */

vl_preco_medicamento_w		number(15,2);	
cd_estabelecimento_w		number(10);

begin

begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

select	max(nvl(vl_preco_medicamento * (1 + NVL(pr_ipi,0)/100),0))
into	vl_preco_medicamento_w
from	brasindice_preco a 
where	a.cd_laboratorio 						= cd_laboratorio_p 
and 	a.cd_medicamento 						= cd_medicamento_p
and 	a.cd_apresentacao 						= cd_apresentacao_p
and 	nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_w, 0)) 	= nvl(cd_estabelecimento_w, 0)
and 	a.dt_inicio_vigencia 						= (	select 	max(b.dt_inicio_vigencia) 
										from	brasindice_preco b 
										where	b.cd_laboratorio 	= cd_laboratorio_p 
										and	b.cd_medicamento 	= cd_medicamento_p
										and 	b.cd_apresentacao 	= cd_apresentacao_p
										and 	nvl(b.cd_estabelecimento, nvl(cd_estabelecimento_w, 0)) 	= nvl(cd_estabelecimento_w, 0)
										);
exception
	when no_data_found then
		vl_preco_medicamento_w := 0;
end;

if	((qt_conversao_p > 0 ) and (ie_opcao_p = 'C')) then
	vl_preco_medicamento_w := vl_preco_medicamento_w / qt_conversao_p;
end if;
/*
if	(vl_preco_medicamento_w = 0) then
	select	nvl(max(vl_preco_venda),0)
	into	vl_preco_medicamento_w	
	from	preco_material
	where	dt_inicio_vigencia	=	(select	max(dt_inicio_vigencia)
					from	preco_material
					where	cd_material	=	cd_material_p)
	and	cd_material	=	cd_material_p;
end if;	
*/

return	vl_preco_medicamento_w;	

end obter_valor_brasindice2;
/
