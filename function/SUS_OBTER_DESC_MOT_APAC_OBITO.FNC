create or replace
function sus_obter_desc_mot_apac_obito (nr_seq_mot_apac_obt_p	number)
 		    		return varchar2 is
					
ds_retorno_w		varchar2(255);

begin

begin
select	nvl(substr(ds_motivo,1,255),'')
into	ds_retorno_w
from	sus_motiv_apac_ate_obito
where	nr_sequencia = nr_seq_mot_apac_obt_p;
exception
when others then
	ds_retorno_w := '';
end;

return	ds_retorno_w;

end sus_obter_desc_mot_apac_obito;
/