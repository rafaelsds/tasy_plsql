create or replace 
function HOSP_OBTER_DADOS_CONV_REC
				(cd_convenio_p		number,
				dt_inicial_p		date,
				dt_final_p		date,
				ie_nao_vinculado_p	varchar2,
				ie_parcial_p		varchar2,
				ie_total_p		varchar2,
	 			ie_opcao_p		varchar2,
				cd_estabelecimento_p	number)
				return			varchar2 is

ds_retorno_w		varchar2(4000);
nr_interno_conta_w	number(10);
cd_autorizacao_w	varchar2(20);
vl_atendimento_w	number(15,2);
vl_tot_atendimento_w	number(15,2);
vl_amenor_w		number(15,2);
vl_tot_amenor_w		number(15,2);
nr_seq_retorno_max_w	number(10);
nr_seq_ret_item_min_w	number(10);
vl_pago_anterior_w	number(15,2);
vl_tot_pago_ant_w	number(15,2);
vl_vinculacao_w		number(15,2);
vl_outros_receb_w	number(15,2);
vl_tot_outros_receb_w	number(15,2);
vl_tot_glosa_w		number(15,2);
vl_glosa_w		number(15,2);
vl_tot_adicional_w	number(15,2);
vl_adicional_w		number(15,2);
nr_seq_ret_item_max_w	number(10);

/*	ie_opcao_p

'VAT'	valor atendimento
'VA'	valor a menor
'VP'	valor pago anterior
'VO'	valor outros recebimentos
'VG'	valor glosado
'VAD'	valor adicional

*/

cursor	c01 is
select	distinct
	d.nr_interno_conta,
	d.cd_autorizacao
from	convenio_retorno_item d,
	convenio_retorno c,
	convenio_ret_receb b,
	hosp_faturamento_convenio_v a
where	c.nr_sequencia		= d.nr_seq_retorno
and	b.nr_seq_retorno	= c.nr_sequencia
and	a.nr_sequencia		= b.nr_seq_receb
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	('S' = ie_nao_vinculado_p or a.ie_vinculado = 'S')
and	('S' = ie_parcial_p or a.ie_parcial = 'N')
and	('S' = ie_total_p or a.ie_total = 'N')
and	a.dt_recebimento	between dt_inicial_p and fim_dia(dt_final_p)
and	a.cd_convenio		= cd_convenio_p;

begin

open	c01;
loop
fetch	c01 into
	nr_interno_conta_w,
	cd_autorizacao_w;
exit	when c01%notfound;

	if	(ie_opcao_p	= 'VAT') then

		select	sum(a.vl_guia)
		into	vl_atendimento_w
		from	conta_paciente_guia a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w;

		vl_tot_atendimento_w	:= nvl(vl_tot_atendimento_w,0) + nvl(vl_atendimento_w,0);

	elsif	(ie_opcao_p	= 'VA') then

		select	max(a.nr_seq_retorno)
		into	nr_seq_retorno_max_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w;

		select	sum(a.vl_amenor)
		into	vl_amenor_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w
		and	a.nr_seq_retorno	= nr_seq_retorno_max_w;

		vl_tot_amenor_w		:= nvl(vl_tot_amenor_w,0) + nvl(vl_amenor_w,0);

	elsif	(ie_opcao_p	= 'VP') then /* valor pago anterior */

		select	min(a.nr_sequencia)
		into	nr_seq_ret_item_min_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w;

		select	nvl(sum(a.vl_glosado),0) + nvl(sum(a.vl_pago),0)
		into	vl_pago_anterior_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w
		and	a.nr_sequencia		< nr_seq_ret_item_min_w;

		vl_tot_pago_ant_w	:= nvl(vl_tot_pago_ant_w,0) + nvl(vl_pago_anterior_w,0);

	elsif	(ie_opcao_p	= 'VO') then

		select	max(a.nr_sequencia)
		into	nr_seq_ret_item_max_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w;

		select	nvl(sum(a.vl_glosado),0) + nvl(sum(a.vl_pago),0)
		into	vl_outros_receb_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w
		and	a.nr_sequencia		> nr_seq_ret_item_max_w;

		select	max(a.nr_seq_retorno)
		into	nr_seq_retorno_max_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w
		and	a.nr_sequencia		> nr_seq_ret_item_max_w;

		select	sum(a.vl_amenor)
		into	vl_amenor_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w
		and	a.nr_seq_retorno	= nr_seq_retorno_max_w;

		vl_tot_outros_receb_w	:= nvl(vl_tot_outros_receb_w,0) + nvl(vl_outros_receb_w,0) + nvl(vl_amenor_w,0);

	elsif	(ie_opcao_p	= 'VG') then

		select	sum(a.vl_glosado)
		into	vl_glosa_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada') = nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w;

		vl_tot_glosa_w	:= nvl(vl_tot_glosa_w,0) + nvl(vl_glosa_w,0);

	elsif	(ie_opcao_p	= 'VAD') then

		select	sum(a.vl_adicional)
		into	vl_adicional_w
		from	convenio_retorno_item a
		where	nvl(a.cd_autorizacao,'N�o Informada') = nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w;

		vl_tot_adicional_w	:= nvl(vl_tot_adicional_w,0) + nvl(vl_adicional_w,0);

	end if;

end	loop;
close	c01;

if	(ie_opcao_p	= 'VAT') then
	ds_retorno_w	:= vl_tot_atendimento_w;
elsif	(ie_opcao_p	= 'VA') then
	ds_retorno_w	:= vl_tot_amenor_w;
elsif	(ie_opcao_p	= 'VP') then
	ds_retorno_w	:= vl_tot_pago_ant_w;
elsif	(ie_opcao_p	= 'VO') then
	ds_retorno_w	:= vl_tot_outros_receb_w;
elsif	(ie_opcao_p	= 'VG') then
	ds_retorno_w	:= vl_tot_glosa_w;
elsif	(ie_opcao_p	= 'VAD') then
	ds_retorno_w	:= vl_tot_adicional_w;
end if;

return	ds_retorno_w;

end HOSP_OBTER_DADOS_CONV_REC;
/