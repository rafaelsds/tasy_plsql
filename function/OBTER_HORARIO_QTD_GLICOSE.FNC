create or replace
function obter_horario_qtd_glicose(
					nr_prescricao_p		number,
					dt_inicial_p		date,
					dt_final_p		date)
 		    	return varchar2 is
			
ds_resultado_w	varchar2(4000);
ds_horarios_w	varchar2(100);
			
Cursor C01 is
	select	b.ds_horario||' - '||nvl(qt_glicemia,0)||' mg%'||' /'
	from	prescr_proc_hor b,
		atendimento_glicemia c
	where	b.nr_sequencia		 = c.nr_seq_horario(+)
	and	b.nr_prescricao		 = nr_prescricao_p
	and	b.dt_horario between dt_inicial_p and dt_final_p
	order by b.nr_sequencia;

begin

open C01;
loop
fetch C01 into	
	ds_horarios_w;
exit when C01%notfound;
	begin
	
	if 	(ds_horarios_w is not null) then
		begin
		ds_resultado_w	:=	ds_resultado_w||' '||ds_horarios_w;
		end;
	end if;
	end;
end loop;
close C01;

return	ds_resultado_w;

end obter_horario_qtd_glicose;
/