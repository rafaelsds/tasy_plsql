CREATE OR REPLACE
FUNCTION Obter_Material_Origem_Item	(nr_prescricao_p	Number,
					nr_seq_item_p		Number)
					RETURN NUMBER IS

cd_material_origem_w	NUMBER(6,0);

BEGIN

if	(nr_prescricao_p is not null) and
	(nr_seq_item_p is not null) then
	select	cd_material
	into	cd_material_origem_w
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_seq_item_p;
end if;

RETURN	cd_material_origem_w;

END	Obter_Material_Origem_Item;
/