create or replace
function Obter_dados_nut_reserva(	nr_seq_nut_reserva_p	number,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(80);

dt_servico_w		date;
ds_nut_servico_w	varchar2(60);
nm_restaurante_w	varchar2(80);
hr_inicio_w		date;
hr_final_w		date;

			
begin

select	dt_servico,
	substr(obter_desc_nut_servico(nr_seq_servico),1,60),
	substr(obter_nome_restaurante(nr_seq_restaurante),1,80),
	hr_inicio,
	hr_final
into	dt_servico_w,
	ds_nut_servico_w,
	nm_restaurante_w,
	hr_inicio_w,
	hr_final_w
from	nut_reserva
where	nr_sequencia	= nr_seq_nut_reserva_p;
	
if	(ie_opcao_p = 'D') then
	ds_retorno_w := to_char(dt_servico_w,'dd/mm/yyyy');
elsif	(ie_opcao_p = 'S') then
	ds_retorno_w := ds_nut_servico_w;
elsif	(ie_opcao_p = 'R') then
	ds_retorno_w := nm_restaurante_w;
elsif	(ie_opcao_p = 'I') then
	ds_retorno_w := to_char(hr_inicio_w,'hh24:mi:ss');
else	
	ds_retorno_w := to_char(hr_final_w,'hh24:mi:ss');
end if;

return	ds_retorno_w;

end	Obter_dados_nut_reserva;
/