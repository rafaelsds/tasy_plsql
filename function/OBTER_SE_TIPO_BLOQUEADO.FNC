create or replace
function obter_se_tipo_bloqueado(nr_atendimento_p	Number,
			cd_convenio_p		Number,
			ie_tipo_bloqueio_p	varchar)
 		    	return varchar2 is

ie_retorno_w		varchar2(1):= 'N';	
nr_seq_motivo_w		number(10,0);
ie_bloqueio_w		varchar2(1);	
qt_tipo_bloqueio_w	number(10,0);
ie_tipo_convenio_w	number(2,0);

Cursor C01 is
	select	nr_seq_motivo
	from	bloqueio_titulo_paciente
	where	nr_atendimento = nr_atendimento_p
	order by nr_sequencia;
			
begin

select	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio = cd_convenio_p;

open C01;
loop
fetch C01 into	
	nr_seq_motivo_w;
exit when C01%notfound;
	begin	
	
	nr_seq_motivo_w:= nr_seq_motivo_w;
	
	end;
end loop;
close C01;

select 	count(*)
into	qt_tipo_bloqueio_w
from 	tipo_bloqueio_atend
where 	ie_tipo_bloqueio = ie_tipo_bloqueio_p
and 	nr_seq_motivo = nr_seq_motivo_w
and	nvl(ie_tipo_convenio, nvl(ie_tipo_convenio_w,0)) = nvl(ie_tipo_convenio_w,0)
and 	ie_situacao = 'A';

if	(qt_tipo_bloqueio_w = 0) then
	ie_retorno_w:= 'N';
else
	ie_retorno_w:= 'S';
end if;

return	ie_retorno_w;

end obter_se_tipo_bloqueado;
/
