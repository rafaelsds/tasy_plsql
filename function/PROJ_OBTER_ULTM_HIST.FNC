create or replace
function proj_obter_ultm_hist(	nr_seq_projeto_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(100);

begin
select	nvl(max(a.ds_titulo),'')
into	ds_retorno_w
from	proj_cron_etapa_hist a
where	a.nr_seq_etapa_cron = nr_seq_projeto_p;

return	ds_retorno_w;

end proj_obter_ultm_hist;
/