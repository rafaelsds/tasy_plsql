create or replace 
function obter_consist_venc_cheque_js(
		ie_msg_p		number,
		ie_retroativa_p		varchar2)
		return varchar2 is

ds_compl_msg_w		varchar2(255);
ds_msg_w		varchar2(2000);
		
begin
	if	(ie_msg_p = 1) then
		ds_msg_w := obter_texto_tasy(96147, wheb_usuario_pck.get_nr_seq_idioma);
	elsif	(ie_msg_p = 2) then
		ds_msg_w := obter_texto_tasy(96148, wheb_usuario_pck.get_nr_seq_idioma);
	elsif	(ie_msg_p = 3) then
		ds_msg_w := obter_texto_tasy(96149, wheb_usuario_pck.get_nr_seq_idioma);
	end if;
	
	if	(ie_retroativa_p = 'S') then
		ds_compl_msg_w := obter_texto_tasy(96150, wheb_usuario_pck.get_nr_seq_idioma);
	else
		ds_compl_msg_w := obter_texto_tasy(96151, wheb_usuario_pck.get_nr_seq_idioma);
	end if;
	
return ds_msg_w || ds_compl_msg_w;
end obter_consist_venc_cheque_js;
/