create or replace
function obter_valor_total_pagamentos(
				nm_usuario_p				varchar2,
				dt_inicial_p				date,
				dt_final_p					date,
				ie_tipo_data_p				varchar2,
				ie_status_p					varchar2,
				cd_estab_p					number,
				ie_controla_todas_aprov_p	varchar2,
				ie_tipo_documento_p			number,
				ie_status_docto_p			number,
				ds_lista_titulos_p			varchar2)
 		    	return varchar2 is
				
vl_total_bordero_w		number(15,2);
vl_total_escritural_w	number(15,2);
vl_total_titulo_w		number(15,2);
vl_total_recurso_w		number(15,2);
vl_total_w				varchar2(255);

begin

	/*Borderos a Pagar*/

	select  nvl(sum((select  sum(vl_total_bordero) from titulo_pagar_bordero_v b where b.nr_bordero = a.nr_bordero)),0) vl_total_bordero
	into	vl_total_bordero_w
	from    bordero_pagamento a 
	where   exists ( 
				select	1 
				from	conta_pagar_lib d 
				where	d.nr_bordero		= a.nr_bordero 
				and		(((ie_controla_todas_aprov_p = 'N') and (((d.nm_usuario_lib is not null)
				and 	(d.nm_usuario_lib = nm_usuario_p))
				or      (d.cd_perfil = obter_perfil_ativo))) or (ie_controla_todas_aprov_p = 'S'))
				and		(('N' = ie_tipo_data_p) or
						('DA' = ie_tipo_data_p and d.dt_liberacao between dt_inicial_p and fim_dia(dt_final_p)) or 
						('DR' = ie_tipo_data_p and d.dt_reprovacao between dt_inicial_p and fim_dia(dt_final_p)))
				and 	(('T' = ie_status_p) or
						('A' = ie_status_p and d.dt_liberacao is not null) or
						('R' = ie_status_p and d.dt_reprovacao is not null) or
						('P' = ie_status_p and d.dt_reprovacao is null and d.dt_liberacao is null))
				and 	ie_tipo_documento_p in (0,1)
				and		((ie_status_docto_p = 0) or 	
						 (ie_status_docto_p = 1 and obter_se_pagamento_liberado(a.nr_bordero, 'B') = 'S') or 	
						 (ie_status_docto_p = 2 and obter_se_pagamento_liberado(a.nr_bordero, 'B') = 'N') ) )
	and		(nvl(cd_estab_p,0) = 0 or cd_estab_p = a.cd_estabelecimento); 
	
	/*Pagamento escritural*/
	
	select  sum(obter_valor_banco_escrit(a.nr_sequencia)) vl_total_escritural
	into	vl_total_escritural_w
	from    banco_escritural a 
	where   exists ( 
				select  1 
				from	conta_pagar_lib d 
				where	d.nr_seq_banco_escrit	= a.nr_sequencia 
				and		(((ie_controla_todas_aprov_p = 'N') and (((d.nm_usuario_lib is not null)
				and 	(d.nm_usuario_lib = nm_usuario_p))
				or      (d.cd_perfil = obter_perfil_ativo))) or (ie_controla_todas_aprov_p = 'S'))
				and		(('N' = ie_tipo_data_p) or
						('DA' = ie_tipo_data_p and dt_liberacao between dt_inicial_p and fim_dia(dt_final_p)) or 
						('DR' = ie_tipo_data_p and dt_reprovacao between dt_inicial_p and fim_dia(dt_final_p)))
				and 	(('T' = ie_status_p) or
						('A' = ie_status_p and dt_liberacao is not null) or
						('R' = ie_status_p and dt_reprovacao is not null) or
						('P' = ie_status_p and dt_reprovacao is null and dt_liberacao is null))
				and 	ie_tipo_documento_p in (0,2)
				and		((ie_status_docto_p = 0) or 	
						 (ie_status_docto_p = 1 and obter_se_pagamento_liberado(a.nr_sequencia, 'XX') = 'S') or 	
						 (ie_status_docto_p = 2 and obter_se_pagamento_liberado(a.nr_sequencia, 'XX') = 'N') ) )
	and		(nvl(cd_estab_p,0) = 0 or cd_estab_p = a.cd_estabelecimento);

	/*Titulo Pagar*/ 
	
	select 	sum(vl_titulo) vl_total_titulo
	into	vl_total_titulo_w
	from 	(
	select  a.nr_titulo,
			a.ds_beneficiario, 
			substr(obter_status_tit_pagamento(a.nr_titulo, a.nr_bordero, 'T', nm_usuario_p),1,255) ds_status,
			a.dt_vencimento_atual, 
			a.vl_titulo, 
			b.ie_nivel, 
			b.dt_liberacao, 
			b.dt_reprovacao, 
			c.nr_nota_fiscal, 
			substr(obter_dados_tit_pagar(a.nr_titulo,'OB'),1,255) ds_observacao
	from    titulo_pagar_v a,
			conta_pagar_lib b,
			nota_fiscal c
	where   a.nr_titulo	= b.nr_titulo
	and     a.nr_seq_nota_fiscal = c.nr_sequencia (+)
	and     b.nr_bordero is null
	and     b.nr_seq_banco_escrit is null
	and     a.ie_situacao not in ('C','D')
	and     (ie_controla_todas_aprov_p = 'N'
	and 	b.nm_usuario_lib = nm_usuario_p)
	and (substr(obter_se_regra_lib_tit(a.nr_titulo, nm_usuario_p, obter_perfil_ativo, a.cd_estabelecimento, decode(a.cd_pessoa_fisica,null,'J','F'), a.vl_titulo,decode(a.cd_pessoa_fisica,null,null,(select max(x.cd_tipo_pessoa) from pessoa_juridica x 
		where x.cd_cgc = a.cd_cgc)), a.nr_seq_nota_fiscal, nvl(a.cd_cgc,0),nvl(a.dt_emissao, sysdate), a.nr_seq_proj_rec, a.ie_tipo_titulo, a.nr_seq_classe, a.ie_origem_titulo, a.cd_operacao_nf, a.vl_total_nota, a.nr_ordem_compra_nf ),1,1) = 'S')
	and		(('N' = ie_tipo_data_p) or
			 ('DA' = ie_tipo_data_p and b.dt_liberacao between dt_inicial_p and fim_dia(dt_final_p)) or 
			 ('DR' = ie_tipo_data_p and b.dt_reprovacao between dt_inicial_p and fim_dia(dt_final_p)))
	and 	(('T' = ie_status_p) or
			 ('A' = ie_status_p and b.dt_liberacao is not null) or
			 ('R' = ie_status_p and b.dt_reprovacao is not null) or
			 ('P' = ie_status_p and b.dt_reprovacao is null and b.dt_liberacao is null))
	and 	ie_tipo_documento_p in (0,3)
	and		((obter_se_contido(a.nr_titulo,ds_lista_titulos_p) = 'S') or
			 nvl(ds_lista_titulos_p,'X') = 'X')
	and		((nvl(cd_estab_p,0) = 0 or cd_estab_p = a.cd_estabelecimento) or 
			 cd_estab_p = a.cd_estab_financeiro)
	and		((ie_status_docto_p = 0) or 	
			 (ie_status_docto_p = 1 and obter_se_pagamento_liberado(a.nr_titulo, 'T') = 'S') or 	
			 (ie_status_docto_p = 2 and obter_se_pagamento_liberado(a.nr_titulo, 'T') = 'N'))
	union all
	select  distinct a.nr_titulo, 
			a.ds_beneficiario, 
			substr(obter_status_tit_pagamento(a.nr_titulo, a.nr_bordero, 'T', nm_usuario_p),1,255) ds_status,
			a.dt_vencimento_atual, 
			a.vl_titulo, 
			b.ie_nivel, 
			b.dt_liberacao, 
			b.dt_reprovacao, 
			c.nr_nota_fiscal, 
			substr(obter_dados_tit_pagar(a.nr_titulo,'OB'),1,255) ds_observacao
	from    titulo_pagar_v a,
			conta_pagar_lib b,
			nota_fiscal c
	where   a.nr_titulo	= b.nr_titulo
	and     a.nr_seq_nota_fiscal = c.nr_sequencia (+)
	and     b.nr_bordero is null
	and     b.nr_seq_banco_escrit is null
	and		a.ie_situacao not in ('C','D')
	and    	(ie_controla_todas_aprov_p = 'S')
	and (substr(obter_se_regra_lib_tit(a.nr_titulo, nm_usuario_p, obter_perfil_ativo ,  a.cd_estabelecimento, decode(a.cd_pessoa_fisica,null,'J','F'), a.vl_titulo,decode(a.cd_pessoa_fisica,null,null,(select max(x.cd_tipo_pessoa) from pessoa_juridica x 
		where x.cd_cgc = a.cd_cgc)),a.nr_seq_nota_fiscal, nvl(a.cd_cgc,0), nvl(a.dt_emissao, sysdate), a.nr_seq_proj_rec, a.ie_tipo_titulo, a.nr_seq_classe, a.ie_origem_titulo, a.cd_operacao_nf, a.vl_total_nota, a.nr_ordem_compra_nf ),1,1) = 'S')
	and		(('N' = ie_tipo_data_p) or
			 ('DA' = ie_tipo_data_p and b.dt_liberacao between dt_inicial_p and fim_dia(dt_final_p)) or 
			 ('DR' = ie_tipo_data_p and b.dt_reprovacao between dt_inicial_p and fim_dia(dt_final_p)))
	and 	(('T' = ie_status_p) or
			 ('A' = ie_status_p and b.dt_liberacao is not null) or
			 ('R' = ie_status_p and b.dt_reprovacao is not null) or
			 ('P' = ie_status_p and b.dt_reprovacao is null and b.dt_liberacao is null))
	and 	ie_tipo_documento_p in (0,3)
	and		((obter_se_contido(a.nr_titulo,ds_lista_titulos_p) = 'S') or
			 nvl(ds_lista_titulos_p,'X') = 'X')
	and		((nvl(cd_estab_p,0) = 0 or cd_estab_p = a.cd_estabelecimento) or 
			cd_estab_p = a.cd_estab_financeiro)
	and		((ie_status_docto_p = 0) or 	
			 (ie_status_docto_p = 1 and obter_se_pagamento_liberado(a.nr_titulo, 'T') = 'S') or 	
			 (ie_status_docto_p = 2 and obter_se_pagamento_liberado(a.nr_titulo, 'T') = 'N')));

	/* Gest�o Recurso Glosa */

	select 	sum(decode((select c.ie_tipo_valor from regra_lib_grg c where c.nr_sequencia = a.nr_seq_regra_lib),'A', b.vl_glosa, 'R', b.vl_recurso, 0)) vl_total_recurso
	into	vl_total_recurso_w
	from lote_audit_hist_lib a,
	lote_audit_hist b,
	lote_auditoria c
	where a.nr_seq_lote_audit_hist = b.nr_sequencia
	and	c.nr_sequencia = b.nr_seq_lote_audit
	and	ie_tipo_documento_p in (0,4)
	and (((ie_controla_todas_aprov_p = 'N') and	a.nm_usuario_lib = nm_usuario_p ) or 
		 (ie_controla_todas_aprov_p = 'S'))
	and	(('N' = ie_tipo_data_p) or
		 ('DA' = ie_tipo_data_p and a.dt_aprovacao between dt_inicial_p and fim_dia(dt_final_p)) or 
		 ('DR' = ie_tipo_data_p and a.dt_reprovacao between dt_inicial_p and fim_dia(dt_final_p)))
	and (('T' = ie_status_p) or
		 ('A' = ie_status_p and a.dt_aprovacao is not null) or
	     ('R' = ie_status_p and a.dt_reprovacao is not null) or
		 ('P' = ie_status_p and a.dt_reprovacao is null and a.dt_aprovacao is null));
	
	vl_total_w := nvl(vl_total_bordero_w,0) + nvl(vl_total_escritural_w,0) + nvl(vl_total_titulo_w,0) + nvl(vl_total_recurso_w,0);

return	vl_total_w;

end obter_valor_total_pagamentos;
/