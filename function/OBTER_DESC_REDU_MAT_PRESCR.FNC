CREATE OR REPLACE
FUNCTION Obter_Desc_Redu_Mat_Prescr(
			nr_prescricao_p	Number,
			nr_sequencia_p	Number,
			ie_localizacao_p	Varchar2,
			cd_local_estoque_p	Number)
			RETURN VARCHAR IS

ds_retorno_w		Varchar2(128);
ds_material_w		Varchar2(100);
ie_tipo_fonte_w	Varchar2(003);
ds_local_w		Varchar2(80);
cd_material_w		Number(06,0);
dt_baixa_w		Date;
ie_urgencia_w		Varchar2(01);
cd_local_w		Number(05,0);

BEGIN

if	(ie_localizacao_p = 'S') and
	(nvl(cd_local_estoque_p,0) = 0) then
	SELECT	max(b.cd_local_estoque)
	into	cd_local_w
	FROM	setor_atendimento b,
		prescr_medica a
	where	nr_prescricao	= nr_prescricao_p
	and	a.cd_setor_atendimento	= b.cd_setor_atendimento;
end if;

cd_local_w := nvl(cd_local_w, cd_local_estoque_p);

select	cd_material,
	dt_baixa,
	nvl(ie_urgencia,'N')
into	cd_material_w,
	dt_baixa_w,
	ie_urgencia_w
from	Prescr_material
where	nr_prescricao		= nr_prescricao_p
  and	nr_sequencia		= nr_sequencia_p;

select	substr(max(ds_reduzida),1,100),
	max(nvl(ie_tipo_fonte_prescr,' '))
into	ds_material_w,
	ie_tipo_fonte_w
from 	material
where cd_material = cd_material_w;

ds_local_w			:= '';
if	(ie_localizacao_p = 'S') and
	(nvl(cd_local_w,0) > 0) then
	Select substr(obter_localizacao_material(cd_material_w, cd_local_w),1,80)
	into	ds_local_w
	from dual;
	if	(ds_local_w is not null) then
		ds_local_w	:= ' [' || ds_local_w || ']';
	end if;
end if;

ds_retorno_w		:= ds_material_w || ds_local_w;

if	(ie_urgencia_w = 'S') then
	ds_retorno_w	:= '#' || ds_retorno_w || '#' || 'NI' ||'#P';
elsif	(ie_tipo_fonte_w = 'MS') then
	begin
	ds_retorno_w 	:= upper(ds_retorno_w);
	ie_tipo_fonte_w	:= 'S';
	ds_retorno_w	:= '#' || ds_retorno_w || '#' || ie_tipo_fonte_w ||'#P';
	end;
elsif	(ie_tipo_fonte_w <> ' ') and (ie_tipo_fonte_w <> 'M')  then
	ds_retorno_w	:= '#' || ds_retorno_w || '#' || ie_tipo_fonte_w ||'#P';
elsif	(ie_tipo_fonte_w = 'M') then
	ds_retorno_w	:= upper(ds_retorno_w);
elsif	(ie_tipo_fonte_w = 'MN') then
	ds_retorno_w	:= upper(ds_retorno_w);
	ds_retorno_w	:= '#' || ds_retorno_w || '#' || 'N' ||'#P';
end if;

RETURN ds_retorno_w;

END Obter_Desc_Redu_Mat_Prescr;
/
