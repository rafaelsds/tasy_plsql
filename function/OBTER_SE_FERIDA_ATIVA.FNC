create or replace
function obter_se_ferida_ativa	( 	nr_seq_prescr_p number,
									cd_pessoa_fisica_p number,
									ie_opcao_p varchar2 default 'S')
					return varchar2 is

ie_ativo_w			varchar2(1) := 'N';
qt_ferida_w			number(10);
qt_resultados_w		number(10);
qt_imagem_ferida_w	number(10);


begin
if	(nr_seq_prescr_p is not null) and
	(cd_pessoa_fisica_p is not null) then

	If	(ie_opcao_p = 'S') then
	
		Select  count(*)
		into	qt_ferida_w
		from 	cur_ferida 
		where   cd_pessoa_fisica = cd_pessoa_fisica_p
		and		dt_inativacao is null
		and		dt_alta_curativo is null
		and		nr_seq_localizacao is not null;

		Select 	count(*)
		into	qt_resultados_w
		from    pe_prescr_item_result a,
				pe_item_resultado b
		where  	a.nr_seq_prescr = nr_seq_prescr_p
		and		a.nr_seq_result = b.nr_sequencia
		and		nvl(ie_nova_ferida,'N') = 'S';
		
		if	(qt_ferida_w > 0) or
			(qt_resultados_w > 0) then
			
			ie_ativo_w := 'S';
			
		end if;
	
	elsif	(ie_opcao_p = 'I') then
	
		Select 	count(*)
		into	qt_imagem_ferida_w
		from	pe_prescr_imagem
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
		
		if	(qt_imagem_ferida_w = 0 ) then
		
			ie_ativo_w := 'S';
		
		end if;
	
	
	end if;

end if;

return ie_ativo_w;

end obter_se_ferida_ativa;
/