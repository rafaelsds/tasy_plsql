create or replace
function add_espaco_apos_caracter(ds_texto_p	varchar2,
			      ds_caracter_p     varchar2)
			      return varchar2 is

ds_texto_w	varchar2(2000);
ds_caracter_w   varchar2(1) := '';
ds_retorno_w	varchar2(2000) := '';
i		number(10);

begin

i		:= 1;
ds_texto_w 	:= ds_texto_p;
ds_caracter_w   := ds_caracter_p;

while (i <= length(ds_texto_w)) loop
	if	(substr(ds_texto_w,i,1) = ds_caracter_p) then
		ds_retorno_w := ds_retorno_w || substr(ds_texto_w,i,1) || ' ';
	else
		ds_retorno_w := ds_retorno_w || substr(ds_texto_w,i,1);
	end if;
	i := i + 1;
end loop;

return	ds_retorno_w;

end add_espaco_apos_caracter;
/