create or replace
PROCEDURE oft_liberar_formulario (	nr_seq_consulta_p			NUMBER,
												nr_seq_consulta_form_p	NUMBER,
												vLista						strRecTypeFormOft,
												ie_gerar_receita_p		VARCHAR2) IS

												

nr_seq_consulta_form_w	oft_consulta_formulario.nr_sequencia%TYPE;
nr_seq_interno_w			diagnostico_doenca.nr_seq_interno%TYPE;
nm_usuario_w				usuario.nm_usuario%TYPE := wheb_usuario_pck.get_nm_usuario;
x 			 					INTEGER;
nr_seq_refracao_w			NUMBER(10);
nr_seq_oft_oculos_w		NUMBER(10) := 0;
nm_tabela_w             tabela_sistema.nm_tabela%type;
nm_atributo_w           tabela_atributo.nm_atributo%type;

TYPE tabela IS RECORD (nm_tabela 	VARCHAR2(100));
TYPE Vetor IS TABLE OF tabela INDEX 	BY BINARY_INTEGER;

CURSOR	c01 IS
	SELECT	nr_sequencia
	FROM		oft_consulta_formulario	
	WHERE		nr_seq_consulta 	= nr_seq_consulta_p
	AND		nm_usuario			= nm_usuario_w
	AND		dt_liberacao 		IS NULL;
   
cursor   c_pendencias is
   select   a.nr_seq_diag_doenca,
            a.cd_evolucao,
            a.nr_seq_hist_saude_transfusao,
            a.nr_seq_hist_saude_vicio,
            a.nr_seq_hist_saude_medic_uso,
            a.nr_seq_atestado,
            a.nr_seq_avaliacao,
            a.nr_seq_hist_saude_cirurgia,
            a.nr_seq_hist_saude_internacao,
            a.nr_seq_hist_saude_alergia,
            a.nr_seq_hist_saude_clinico,
            a.nr_seq_anamnese,
            a.nr_seq_hist_saude_acessorio,
            a.nr_seq_hist_saude_deficiencia,
            a.nr_seq_receita,
            a.ie_tipo_registro, 
            a.nr_seq_registro,
            a.nr_seq_hist_saude_atend_medic
   from	   pep_item_pendente a,
            atendimento_paciente b,
            oft_consulta c
   where    a.nr_atendimento        = b.nr_atendimento 
   and      b.nr_atendimento        = c.nr_atendimento      
   and	   a.nm_usuario            = nm_usuario_w
   and		c.nr_sequencia				= nr_seq_consulta_p
   and	   b.dt_cancelamento       is null 
   and	   a.ie_gerado_funcao_pepo = 'O'
   and      a.ie_tipo_pendencia     = 'L'
   union
   select   a.nr_seq_diag_doenca,
            a.cd_evolucao,
            a.nr_seq_hist_saude_transfusao,
            a.nr_seq_hist_saude_vicio,
            a.nr_seq_hist_saude_medic_uso,
            a.nr_seq_atestado,
            a.nr_seq_avaliacao,
            a.nr_seq_hist_saude_cirurgia,
            a.nr_seq_hist_saude_internacao,
            a.nr_seq_hist_saude_alergia,
            a.nr_seq_hist_saude_clinico,
            a.nr_seq_anamnese,
            a.nr_seq_hist_saude_acessorio,
            a.nr_seq_hist_saude_deficiencia,
            a.nr_seq_receita,
            a.ie_tipo_registro, 
            a.nr_seq_registro,
            a.nr_seq_hist_saude_atend_medic
   from	   pep_item_pendente a,
            oft_consulta c
   where    a.cd_pessoa_fisica      = c.cd_pessoa_fisica
   and	   a.nm_usuario            = nm_usuario_w
   and		c.nr_sequencia				= nr_seq_consulta_p
   and      a.nr_atendimento        is null
   and	   a.ie_gerado_funcao_pepo = 'O'
   and      a.ie_tipo_pendencia     = 'L'
   and      a.dt_registro           between c.dt_consulta and nvl(c.dt_fim_consulta,sysdate);
BEGIN

SELECT	MAX(nr_sequencia)
INTO		nr_seq_refracao_w
FROM   	oft_refracao
WHERE  	nr_seq_consulta_form = nr_seq_consulta_form_p
AND   	(dt_liberacao IS NOT NULL OR nm_usuario = nm_usuario_w) 
AND    	dt_inativacao IS NULL
AND 		((ie_receita_dinamica = 'S') OR (ie_receita_estatica = 'S'));


IF	(ie_gerar_receita_p = 'S') AND (nr_seq_refracao_w IS NOT NULL) THEN
	oft_gerar_receita_oculos(nr_seq_consulta_p,nm_usuario_w,nr_seq_refracao_w,'S',nr_seq_consulta_form_p,nr_seq_oft_oculos_w);
END IF;	

OPEN C01;
LOOP
FETCH C01 INTO	
	nr_seq_consulta_form_w;
EXIT WHEN C01%NOTFOUND;
	BEGIN
	liberar_informacao('OFT_CONSULTA_FORMULARIO','NR_SEQUENCIA',nr_seq_consulta_form_w,nm_usuario_w);
	FOR j IN 1..vLista.COUNT LOOP
		BEGIN
		IF (vLista(j).nm_tabela = 'DIAGNOSTICO_DOENCA') OR (vLista(j).nm_tabela = 'OFT_DIAGNOSTICO') THEN
			SELECT	MAX(nr_seq_interno)
			INTO 		nr_seq_interno_w
			FROM		diagnostico_doenca
			WHERE		NR_SEQ_CONSULTA_FORM = nr_seq_consulta_form_w
			AND		dt_liberacao IS NULL;

			IF	(nr_seq_interno_w IS NOT NULL) THEN	
				liberar_informacao('DIAGNOSTICO_DOENCA', 'NR_SEQ_INTERNO', nr_seq_interno_w, nm_usuario_w);
			END IF;	
			liberar_informacao(vLista(j).nm_tabela, 'NR_SEQ_CONSULTA_FORM', nr_seq_consulta_form_w, nm_usuario_w);
		ELSE
			liberar_informacao(vLista(j).nm_tabela, 'NR_SEQ_CONSULTA_FORM', nr_seq_consulta_form_w, nm_usuario_w);
		END IF;
	
		IF	(ie_gerar_receita_p = 'S') AND (nr_seq_refracao_w IS NOT NULL) THEN
			liberar_informacao('OFT_OCULOS', 'NR_SEQ_CONSULTA_FORM', nr_seq_consulta_form_w, nm_usuario_w);
		END IF;	
	END;
	END LOOP;	
	END;
END LOOP;
CLOSE C01;

for r_pendencias in c_pendencias loop
   begin
   begin
   if (r_pendencias.nr_seq_hist_saude_atend_medic is not null) then
      liberar_informacao('PACIENTE_ANTEC_CLINICO', 'NR_SEQUENCIA', r_pendencias.nr_seq_hist_saude_atend_medic, nm_usuario_w);
   end if;   
   
   if (r_pendencias.nr_seq_diag_doenca is not null) then
      liberar_informacao('DIAGNOSTICO_DOENCA', 'NR_SEQ_INTERNO', r_pendencias.nr_seq_diag_doenca, nm_usuario_w);
   end if;   
   
   if (r_pendencias.NR_SEQ_HIST_SAUDE_CLINICO is not null) then
      liberar_informacao('PACIENTE_ANTEC_CLINICO', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_CLINICO, nm_usuario_w);
   end if;
   
   if (r_pendencias.CD_EVOLUCAO is not null) then
      liberar_informacao('EVOLUCAO_PACIENTE', 'CD_EVOLUCAO', r_pendencias.CD_EVOLUCAO, nm_usuario_w);
   end if;
   
   if (r_pendencias.NR_SEQ_HIST_SAUDE_INTERNACAO is not null) then
      liberar_informacao('HISTORICO_SAUDE_INTERNACAO', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_INTERNACAO, nm_usuario_w);
   end if;
   
   if (r_pendencias.NR_SEQ_HIST_SAUDE_ALERGIA is not null) then
      liberar_informacao('PACIENTE_ALERGIA', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_ALERGIA, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_ATESTADO is not null) then
      liberar_informacao('ATESTADO_PACIENTE', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_ATESTADO, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_HIST_SAUDE_ACESSORIO is not null) then
      liberar_informacao('PACIENTE_ACESSORIO', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_ACESSORIO, nm_usuario_w);
   end if;
   
   if (r_pendencias.NR_SEQ_HIST_SAUDE_MEDIC_USO is not null) then
      liberar_informacao('PACIENTE_MEDIC_USO', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_MEDIC_USO, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_HIST_SAUDE_CIRURGIA is not null) then
      liberar_informacao('HISTORICO_SAUDE_CIRURGIA', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_CIRURGIA, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_HIST_SAUDE_TRANSFUSAO is not null) then
      liberar_informacao('PACIENTE_TRANSFUSAO', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_TRANSFUSAO, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_ANAMNESE is not null) then
      liberar_informacao('ANAMNESE_PACIENTE', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_ANAMNESE, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_AVALIACAO is not null) then
      liberar_informacao('MED_AVALIACAO_PACIENTE', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_AVALIACAO, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_HIST_SAUDE_CLINICO is not null) then
      liberar_informacao('PACIENTE_ANTEC_CLINICO', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_CLINICO, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_HIST_SAUDE_VICIO is not null) then
      liberar_informacao('PACIENTE_HABITO_VICIO', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_VICIO, nm_usuario_w);
   end if;

   if (r_pendencias.NR_SEQ_HIST_SAUDE_DEFICIENCIA is not null) then
      liberar_informacao('PF_TIPO_DEFICIENCIA', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_HIST_SAUDE_DEFICIENCIA, nm_usuario_w);
   end if;
   
   if (r_pendencias.NR_SEQ_RECEITA is not null) then
      liberar_informacao('MED_RECEITA', 'NR_SEQUENCIA', r_pendencias.NR_SEQ_RECEITA, nm_usuario_w);
   end if;
   
   case r_pendencias.ie_tipo_registro
      when  'ONL' then liberar_informacao('OFT_ANAMNESE', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'OAL' then liberar_informacao('OFT_ANEXO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'CNL' then liberar_informacao('PEP_PAC_CI', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'ODL' then liberar_informacao('OFT_DIAGNOSTICO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'ODF' then liberar_informacao('OFT_DIAGNOSTICO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'OGL' then liberar_informacao('PEP_ORIENTACAO_GERAL', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'PEE' then liberar_informacao('PEDIDO_EXAME_EXTERNO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'OCL' then liberar_informacao('OFT_CONDUTA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'BIOL' then liberar_informacao('OFT_BIOMETRIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'CAPL' then liberar_informacao('OFT_CAMPIMETRIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'OIL' then liberar_informacao('OFT_IMAGEM_EXAME', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'CTL' then liberar_informacao('OFT_CURVA_TENCIONAL', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'DNPL' then liberar_informacao('OFT_DNP', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'REFL' then liberar_informacao('OFT_REFRACAO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'EOEL' then liberar_informacao('OFT_EXAME_EXTERNO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'FNCL' then liberar_informacao('OFT_FUNDOSCOPIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'GNOL' then liberar_informacao('OFT_GONIOSCOPIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'MIEL' then liberar_informacao('OFT_MICROSCOPIA_ESPECULAR', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'MOOL' then liberar_informacao('OFT_MOTILIDADE_OCULAR', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'PAQL' then liberar_informacao('OFT_PAQUIMETRIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'PQUL' then liberar_informacao('OFT_PAQUIMETRIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'PAVL' then liberar_informacao('OFT_POTENCIAL_ACUIDADE', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'PUPL' then liberar_informacao('OFT_PUPILOMETRIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'TOCL' then liberar_informacao('OFT_OCT', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'TOGL' then liberar_informacao('OFT_TOMOGRAFIA_OLHO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'TAPL' then liberar_informacao('OFT_TONOMETRIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'TPNL' then liberar_informacao('OFT_TONOMETRIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'ULTL' then liberar_informacao('OFT_ULTRASSONOGRAFIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'ARTL' then liberar_informacao('OFT_ANGIO_RETINO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'AGRL' then liberar_informacao('OFT_ANGIO_RETINO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'BICL' then liberar_informacao('OFT_BIOMICROSCOPIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'FOTL' then liberar_informacao('OFT_FOTOCOAGULACAO_LASER', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'CASL' then liberar_informacao('OFT_CAPSULOTOMIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'DATL' then liberar_informacao('OFT_DALTONISMO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'AVAL' then liberar_informacao('OFT_OLHO_SECO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'IRIL' then liberar_informacao('OFT_IRIDECTOMIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'AURL' then liberar_informacao('OFT_AUTO_REFRACAO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'AVNL' then liberar_informacao('OFT_CORRECAO_ATUAL', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'SOHL' then liberar_informacao('OFT_SOBRECARGA_HIDRICA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'MARL' then liberar_informacao('OFT_MAPEAMENTO_RETINA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'CERL' then liberar_informacao('OFT_CERASTOCOPIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'REOL' then liberar_informacao('OFT_OCULOS', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      when  'HRP' then liberar_informacao('PACIENTE_REP_PRESCRICAO', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
	   when  'ABRO' then liberar_informacao('OFT_ABERROMETRIA', 'NR_SEQUENCIA', r_pendencias.nr_seq_registro, nm_usuario_w);
      else  
      null;
   end case;   
   exception
      when others then
         null;
      end;                              
   end;
end loop;


END oft_liberar_formulario;
/