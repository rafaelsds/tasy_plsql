create or replace
function Obter_se_atend_tratamento_onc	(nr_atendimento_p		number)
					return varchar2 is
					
ie_tratamento_w		varchar2(1);

begin

select	decode(count(*),0,'N','S')
into	ie_tratamento_w
from	prescr_medica
where	nr_atendimento	= nr_atendimento_p
and	nr_seq_atend	is not null;

return	ie_tratamento_w;

end Obter_se_atend_tratamento_onc;
/
