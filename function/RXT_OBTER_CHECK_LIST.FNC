create or replace
function rxt_obter_check_list(cd_pessoa_fisica_p varchar2)
 		    	return number is

nr_seq_check_list_w	rxt_tumor.nr_seq_checklist%type;
				
begin

if	(cd_pessoa_fisica_p is not null) then
	begin

	select 	max(nr_seq_checklist)
	into	nr_seq_check_list_w
	from 	RXT_TUMOR
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	end;
end if;

return	nr_seq_check_list_w;

end rxt_obter_check_list;
/