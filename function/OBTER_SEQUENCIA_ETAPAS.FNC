create or replace
function Obter_Sequencia_Etapas(nr_etapas_p	 number,
				nr_prescricao_p	 number,
				nr_seq_solucao_p number)
 		    	return varchar2 is
			
nr_qtde_etapas_w	varchar2(1000);			
Hr1_w			varchar2(2);
Hr2_w			varchar2(2);
Hr3_w			varchar2(2);
Hr4_w			varchar2(2);
Hr5_w			varchar2(2);
Hr6_w			varchar2(2);
Hr7_w			varchar2(2);

begin

select  (nr_etapas_p - 6) Hr1,
	(nr_etapas_p - 5) Hr2,
	(nr_etapas_p - 4) Hr3,
	(nr_etapas_p - 3) Hr4,
	(nr_etapas_p - 2) Hr5,
	(nr_etapas_p - 1) Hr6,
	nr_etapas_p Hr7
into	Hr1_w,
	Hr2_w,
	Hr3_w,
	Hr4_w,
	Hr5_w,
	Hr6_w,
	Hr7_w
from	prescr_solucao
where   nr_prescricao = nr_prescricao_p
and	nr_seq_solucao = nr_seq_solucao_p;

if	(Hr1_w > 0) then
	nr_qtde_etapas_w	:= 	nr_qtde_etapas_w||'                            '||Hr1_w;
end if;
	
if 	(Hr2_w > 0) then
	nr_qtde_etapas_w	:= 	nr_qtde_etapas_w||'                            '|| Hr2_w;
end if;	

if 	(Hr3_w > 0) then
	nr_qtde_etapas_w	:= 	nr_qtde_etapas_w||'                            '|| Hr3_w;
end if;
	
if 	(Hr4_w > 0) then
	nr_qtde_etapas_w	:= 	nr_qtde_etapas_w||'                            '|| Hr4_w;
end if;

if 	(Hr5_w > 0) then
	nr_qtde_etapas_w	:= 	nr_qtde_etapas_w||'                            '|| Hr5_w;
end if;	

if 	(Hr6_w > 0) then
	nr_qtde_etapas_w	:= 	nr_qtde_etapas_w||'                            '|| Hr6_w;
end if;
	
if 	(Hr7_w > 0) then
	nr_qtde_etapas_w	:= 	nr_qtde_etapas_w||'                            '|| Hr7_w;
end if;

return	nr_qtde_etapas_w;

end Obter_Sequencia_Etapas;
/
