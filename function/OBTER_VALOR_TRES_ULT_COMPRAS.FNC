create or replace
function obter_valor_tres_ult_compras(
			cd_estabelecimento_p	Number,
			qt_dia_p		Number,
			cd_material_p		number,
			cd_local_estoque_p	Number,
			ie_tipo_p		Varchar2,
			ie_compra_p		number)
		return number is
		
vl_retorno_w			number(15,4);
nr_sequencia_w			number(10,0);
ie_tipo_w			varchar2(10);
ie_consignado_w			varchar2(1);
cd_material_estoque_w		number(6);
i				number(1);

cursor c01 is
select	a.nr_sequencia
from	natureza_operacao o,
	operacao_nota p,
	nota_fiscal b,
	nota_fiscal_item a
where	a.nr_sequencia		= b.nr_sequencia
and	b.cd_natureza_operacao	= o.cd_natureza_operacao
and	o.ie_entrada_saida	= 'E'
and	cd_material  		= cd_material_p
and	b.cd_operacao_nf 	= p.cd_operacao_nf
and	nvl(p.ie_ultima_compra, 'S') = 'S'
and	a.dt_atualizacao >= sysdate - nvl(qt_dia_p,90)
and	((cd_local_estoque_p is null) or (cd_local_estoque = cd_local_estoque_p))
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	b.ie_situacao	= '1'
order by a.nr_sequencia desc;

begin
vl_retorno_w		:= 0;
i			:= 0;
open C01;
loop
fetch C01 into
	nr_sequencia_w;
exit when C01%notfound;
	begin
	i := i + 1;
	if ((ie_compra_p <= 1) and (i = 1)) then
		exit;
	end if;
	if ((ie_compra_p = 2) and (i = 2)) then
		exit;
	end if;
	if ((ie_compra_p >= 3) and (i = 3)) then
		exit;
	end if;
	end;
end loop;
close C01;

if	(nr_sequencia_w > 0) and
	(ie_compra_p = i) then
	select	nvl(max(dividir((a.vl_total_item_nf - a.vl_desconto -
			vl_desconto_rateio + vl_frete + a.vl_despesa_acessoria + a.vl_seguro),
		a.qt_item_estoque)),0)
	into	vl_retorno_w
	from 	nota_fiscal_item a
	where	nr_sequencia 		= nr_sequencia_w
	and	(((ie_tipo_p = 'N') and (cd_material = cd_material_p)) or
		((ie_tipo_p <> 'N') and (cd_material_estoque = cd_material_p)));
end if;

return vl_retorno_w;

end obter_valor_tres_ult_compras;
/