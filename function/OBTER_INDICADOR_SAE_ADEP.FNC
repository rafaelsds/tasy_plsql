create or replace
function obter_indicador_sae_adep	(nr_seq_item_p	number)
					return varchar2 is

ie_adep_w	varchar2(1);

begin
if	(nr_seq_item_p is not null) then
	select	nvl(max(ie_adep),'N')
	into	ie_adep_w
	from	pe_procedimento
	where	nr_sequencia = nr_seq_item_p;
end if;

return ie_adep_w;

end obter_indicador_sae_adep;
/