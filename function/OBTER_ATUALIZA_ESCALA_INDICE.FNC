create	or replace
function	obter_atualiza_escala_indice(	nr_sequencia_p	number,
				cd_perfil_p	number,
				ie_escala_p	number) return varchar2 is 
											
ie_atualizar_w	varchar2(1) := 'S';
											
begin	

select	nvl(max(ie_atualizar),'S')
into	ie_atualizar_w
from	perfil_escala_indice b,
	grupo_escala_indice_item a
where	a.nr_seq_grupo	= b.nr_seq_grupo
and	b.nr_sequencia	= nr_sequencia_p
and	a.ie_escala	= ie_escala_p
and	b.cd_perfil	= cd_perfil_p;

return	ie_atualizar_w;

end obter_atualiza_escala_indice;
/