create or replace
function obter_desc_regra_score_setor ( 	nr_seq_flex_p	Number,
											ie_complexidade_p	varchar2)
 		    			return varchar2 is

ds_retorno_w		varchar2(2000)	:= '';
ds_escala_flex_w		Varchar2(255);
ds_escala_w		Varchar2(255);


begin


if 	(nr_seq_flex_p is not null) then
	
	if ( ie_complexidade_p = 'SFII') then 
	
		select	max(ds_escala)
		into	ds_escala_flex_w
		from	eif_escala_ii
		where	nr_sequencia = nr_seq_flex_p;
		
	else
	
		select	max(ds_escala)
		into	ds_escala_flex_w
		from	eif_escala
		where	nr_sequencia = nr_seq_flex_p;
		
	end if;

	ds_retorno_w := ds_escala_flex_w;
	
end if;


return	ds_retorno_w;

end obter_desc_regra_score_setor;
/
