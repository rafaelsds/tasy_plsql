create or replace
function obter_classif_agenda(nr_seq_agenda_p 	number)
 		    	return number is
		
nr_seq_classif_w	number(10,0);

begin

if	(nr_seq_agenda_p is not null) then
	select	max(nr_seq_classif_agenda)
	into	nr_seq_classif_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
end if;	

return	nr_seq_classif_w;

end obter_classif_agenda;
/