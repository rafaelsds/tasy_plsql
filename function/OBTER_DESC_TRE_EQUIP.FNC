create or replace
function obter_desc_tre_equip(nr_seq_equip_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);			
			
begin

select	ds_equipamento
into	ds_retorno_w
from	tre_equip
where	nr_sequencia = nr_seq_equip_p;

return	ds_retorno_w;

end obter_desc_tre_equip;
/