create or replace
function bsc_obter_iniciativa_dt_max(	nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(10) := '';
qt_hist_w		number(10);

begin
select	count(*)
into	qt_hist_w
from	man_ordem_serv_tecnico
where	nr_seq_ordem_serv = nr_sequencia_p;

if	(qt_hist_w > 0) then
	begin
	select	to_char(max(dt_atualizacao),'dd/mm/yyyy')
	into	ds_retorno_w
	from	man_ordem_serv_tecnico
	where	nr_seq_ordem_serv = nr_sequencia_p;
	end;
end if;

return	ds_retorno_w;

end bsc_obter_iniciativa_dt_max;
/