create or replace
function obter_se_prescr_CPOE(nr_prescricao_p		number)
 		    	return varchar2 is

ie_cpoe_w	varchar2(1);
			
begin

select	nvl(max(ie_cpoe),'N')
into	ie_cpoe_w
from	(
	select	'S' ie_cpoe
	from	prescr_dieta
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_dieta_cpoe is not null
	union
	select	'S' ie_cpoe
	from	prescr_material
	where	nr_prescricao	= nr_prescricao_p
	and	((nr_seq_dieta_cpoe is not null) or
		 (nr_Seq_mat_cpoe is not null))
	union
	select	'S' ie_cpoe
	from	rep_jejum
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_dieta_cpoe is not null
	union
	select	'S' ie_cpoe
	from	prescr_leite_deriv
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_dieta_cpoe is not null
	union
	select	'S' ie_cpoe
	from	nut_pac
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_npt_cpoe is not null
	union
	select	'S' ie_cpoe
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_proc_cpoe is not null
	union
	select	'S' ie_cpoe
	from	prescr_gasoterapia
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_gas_cpoe is not null
	union
	select	'S' ie_cpoe
	from	prescr_recomendacao
	where	nr_prescricao	= nr_prescricao_p
	and	nr_Seq_rec_cpoe	is not null
	union
	select	'S' ie_cpoe
	from	prescr_solic_bco_Sangue
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_hemo_cpoe is not null
	union
	select	'S' ie_cpoe
	from	prescr_solucao
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_dialise_cpoe is not null);

return	ie_cpoe_w;

end obter_se_prescr_CPOE;
/