CREATE OR REPLACE function obter_aval_procedimento(	
                           nr_seq_proc_interno_p NUMBER,
                           cd_procedimento_p	number)
                           return number IS

nr_seq_tipo_aval_w	number(10);

begin
if	( cd_procedimento_p is not NULL OR nr_seq_proc_interno_p IS NOT null) then
	select	max(nr_seq_tipo_aval)
	into	nr_seq_tipo_aval_w
	from	med_tipo_aval_proc
	where (nr_seq_proc_interno is not null and nr_seq_proc_interno = nr_seq_proc_interno_p) or
   (cd_procedimento is not null and cd_procedimento = cd_procedimento_p);
end if;

return nr_seq_tipo_aval_w;

end obter_aval_procedimento;
/