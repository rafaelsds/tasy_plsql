create or replace
function pls_obter_qt_pessoa_inc_lote(nr_seq_lote_p	number)
					return number is

qt_pessoa_w		number(10);
qt_pf_w			number(10);
qt_pj_w			number(10);

begin

if	(nr_seq_lote_p is not null) then
	select	count(distinct cd_pessoa_fisica)
	into	qt_pf_w
	from	pls_pessoa_inconsistente
	where	nr_seq_lote	= nr_seq_lote_p
	and	dt_consistente is null
	and	cd_pessoa_fisica is not null;
			
	select	count(distinct cd_cgc)
	into	qt_pj_w
	from	pls_pessoa_inconsistente
	where	nr_seq_lote	= nr_seq_lote_p
	and	dt_consistente is null
	and	cd_cgc is not null;

	qt_pessoa_w	:= qt_pf_w + qt_pj_w;
end if;

return qt_pessoa_w;

end;
/