create or replace 
FUNCTION Obter_formato_ferida(
			nr_sequencia_p	Number)
			return varchar2 is

ds_formato_w	Varchar2(60);

BEGIN

if	(nr_sequencia_p is not null) then
	select	ds_formato
	into	ds_formato_w
	from	formato_ferida
	where	nr_sequencia	= nr_sequencia_p;
end if;

return	ds_formato_w;

END Obter_formato_ferida;
/