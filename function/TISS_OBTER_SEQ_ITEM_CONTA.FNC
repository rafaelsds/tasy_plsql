create or replace
function TISS_OBTER_SEQ_ITEM_CONTA
			(nr_interno_conta_p	in number,
			cd_autorizacao_p	in varchar2,
			cd_item_p		in number,
			ie_opcao_p		in varchar2)
			return	number is
			
/*
ie_opcao_p
M	Materiais
P	Procedimentos
*/

nr_sequencia_w	number(10);

begin

if	(nr_interno_conta_p is not null) and
	(cd_item_p is not null) then

	if	(ie_opcao_p = 'M') then
		
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	material_atend_paciente a
		where	a.nr_interno_conta	= nr_interno_conta_p
		and	a.nr_doc_convenio	= cd_autorizacao_p
		and	a.cd_material		= cd_item_p
		and	a.qt_material		> 0
		and	a.cd_motivo_exc_conta	is null;
		
	elsif	(ie_opcao_p = 'P') then
	
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	procedimento_paciente a
		where	a.nr_interno_conta	= nr_interno_conta_p
		and	a.nr_doc_convenio	= cd_autorizacao_p
		and	a.cd_procedimento	= cd_item_p
		and	a.qt_procedimento	> 0
		and	a.cd_motivo_exc_conta	is null;
	
	end if;
	
end if;

return	nr_sequencia_w;

end TISS_OBTER_SEQ_ITEM_CONTA;
/