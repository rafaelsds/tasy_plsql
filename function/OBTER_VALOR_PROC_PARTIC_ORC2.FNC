create or replace
function obter_valor_proc_partic_orc2(	cd_procedimento_p		number,
										ie_origem_proced_p		number,
										ie_tipo_atendimento_p	number,
										cd_medico_p				varchar2,
										ie_clinica_p			number,
										nr_seq_proc_interno_p	number,
										nr_seq_exame_p			number,
										nr_seq_orcamento_p		number,
										nm_usuario_P			varchar2)
							return Number is

cd_convenio_w		Number(5);			
cd_categoria_w		Varchar2(10);
vl_procedimento_w	Number(15,2);
vl_Retorno_w		Number(15,2);
vl_lanc_automatico_w	Number(15,2);
cd_estabelecimento_w	number(4,0);
ie_classsif_proced_w		varchar2(10);
nr_seq_proc_interno_w number(10);

begin

begin
ie_classsif_proced_w := obter_classificacao_proced(cd_procedimento_p,ie_origem_proced_p,'C');

if (ie_classsif_proced_w = 1) then
	nr_seq_proc_interno_w := nr_seq_proc_interno_p;
else
	nr_seq_proc_interno_w:= null;
end if;
exception
when others then
       nr_seq_proc_interno_w := nr_seq_proc_interno_p;
end;


select	wheb_usuario_pck.get_cd_estabelecimento
into	cd_estabelecimento_w
from 	dual;


obter_convenio_particular(cd_estabelecimento_w, cd_convenio_w, cd_categoria_w);

select	obter_preco_proced(
				cd_estabelecimento_w,
				cd_convenio_w,
				cd_categoria_w,
				sysdate,
				cd_procedimento_p,
				ie_origem_proced_p,
				null,
				ie_tipo_atendimento_p,
				null,
				cd_medico_p,
				null,
				null,
				null,
				ie_clinica_p,
				null,
				'P',
				nr_seq_exame_p,
				nr_seq_proc_interno_w)
into	vl_procedimento_w
from	dual;

vl_lanc_automatico_w:= 0;
begin
select  obter_valor_regra_lanc_aut2(cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, cd_procedimento_p,
                                   ie_origem_proced_p, nr_seq_proc_interno_p, 34, nr_seq_exame_p, 'T', 
								   nr_seq_orcamento_p, nm_usuario_p)
into    vl_lanc_automatico_w
from    dual;
exception
when others then
        vl_lanc_automatico_w    := 0;
end;

vl_retorno_w	:= vl_procedimento_w + vl_lanc_automatico_w;

return	vl_Retorno_w;

end obter_valor_proc_partic_orc2;
/