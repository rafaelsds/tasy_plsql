create or replace
function cpoe_obter_se_solic_lib_cih(nr_seq_cpoe_anterior_p	number,
				nr_atendimento_p	number,
				cd_material_p	number,
				qt_dose_p	number,
				ie_via_aplicacao_p	varchar2,
				cd_intervalo_p	varchar2,
				cd_mat_dil_p	number,
				cd_mat_red_p	number,
				ds_justificativa_p	varchar2,
				ds_observacao_p	varchar2) return varchar2 is

ie_dose_w	atb_change_rule.ie_dose%type;
ie_via_aplicacao_w	atb_change_rule.ie_via_aplicacao%type;
ie_intervalo_w	atb_change_rule.ie_intervalo%type;
ie_diluicao_w	atb_change_rule.ie_diluicao%type;
ie_rediluicao_w	atb_change_rule.ie_rediluicao%type;
ie_justificativa_w	atb_change_rule.ie_justificativa%type;
ie_observacao_w	atb_change_rule.ie_observacao%type;

ds_retorno_w varchar2(1) := 'S';
begin

if (nr_seq_cpoe_anterior_p is not null) then

	cpoe_atb_change_rule(wheb_usuario_pck.get_cd_estabelecimento, obter_setor_atendimento(nr_atendimento_p), obter_perfil_ativo,
							cd_material_p,ie_dose_w, ie_via_aplicacao_w, ie_intervalo_w, ie_diluicao_w, ie_rediluicao_w, ie_justificativa_w,ie_observacao_w);

	select	nvl(max('N'), 'S')
	into	ds_retorno_w
	from	cpoe_material a
	where a.nr_sequencia  = nr_seq_cpoe_anterior_p
	and (((nvl(ie_dose_w, 'N') = 'S') and (nvl(a.qt_dose,0) = nvl(qt_dose_p,0))) or (nvl(ie_dose_w, 'N') = 'N'))
	and (((nvl(ie_via_aplicacao_w, 'N') = 'S') and (nvl(a.ie_via_aplicacao, 'XPT') = nvl(ie_via_aplicacao_p, 'XPT'))) or (nvl(ie_via_aplicacao_w, 'N') = 'N'))
	and (((nvl(ie_intervalo_w, 'N') = 'S') and (nvl(a.cd_intervalo, 'XPT') = nvl(cd_intervalo_p,'XPT'))) or (nvl(ie_intervalo_w, 'N') = 'N'))
	and (((nvl(ie_diluicao_w, 'N') = 'S') and (nvl(a.cd_mat_dil, 0) = nvl(cd_mat_dil_p,0))) or (nvl(ie_diluicao_w, 'N') = 'N'))
	and (((nvl(ie_rediluicao_w, 'N') = 'S') and (nvl(a.cd_mat_red,0) = nvl(cd_mat_red_p,0))) or (nvl(ie_rediluicao_w, 'N') = 'N'))
	and (((nvl(ie_justificativa_w, 'N') = 'S') and (nvl(a.ds_justificativa,'XPT') = nvl(ds_justificativa_p,'XPT'))) or (nvl(ie_justificativa_w, 'N') = 'N'))
	and (((nvl(ie_observacao_w, 'N') = 'S') and (nvl(a.ds_observacao, 'XPT') = nvl(ds_observacao_p, 'XPT'))) or (nvl(ie_observacao_w, 'N') = 'N'));
end if;

return ds_retorno_w;

end cpoe_obter_se_solic_lib_cih;
/	