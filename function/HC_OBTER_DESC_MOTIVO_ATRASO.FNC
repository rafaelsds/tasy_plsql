create or replace
function hc_obter_desc_motivo_atraso( nr_seq_motivo_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(80);

begin
if	(nr_seq_motivo_p is not null) then
	select	max(ds_motivo)
	into	ds_retorno_w
	from	hc_motivo_atraso
	where	nr_sequencia = nr_seq_motivo_p;
end if;	

return	ds_retorno_w;

end hc_obter_desc_motivo_atraso;
/