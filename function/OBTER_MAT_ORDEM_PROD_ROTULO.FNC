create or replace 
FUNCTION Obter_mat_ordem_prod_rotulo(
			nr_seq_ordem_p	Number)
			return varchar2 is

ds_retorno_w	Varchar2(255);
ds_material_w	Varchar2(100);	
cd_unid_med_w	Varchar2(30);
qt_dose_w	Number(15,3);

Cursor C01 is
	select	substr(obter_desc_material_dcb(b.cd_material),1,100),
		qt_dose,
		cd_unidade_medida
	from	can_ordem_item_prescr b,
		material a
	where	b.nr_seq_ordem		= nr_seq_ordem_p
	and	a.cd_material		= b.cd_material
	and	a.ie_mistura		= 'S'	
	and	b.nr_sequencia_diluente is null
	order by nr_sequencia;

BEGIN


open C01;
loop
	fetch C01 into 
		ds_material_w,
		qt_dose_w,
		cd_unid_med_w;
	exit when C01%notfound;
	begin
	if	((nvl(length(ds_retorno_w),0) + nvl(length(ds_material_w),0) + nvl(length(cd_unid_med_w),0)) < 255) then
		ds_retorno_w	:= ds_retorno_w || ds_material_w ||' '||qt_dose_w ||' '||cd_unid_med_w|| '/';
	end if;	
	end;
end loop;
close C01;

ds_retorno_w	:= substr(ds_retorno_w,1,length(ds_retorno_w)-1);

return	ds_retorno_w;

END Obter_mat_ordem_prod_rotulo;
/