create or replace
function obter_altura_escala(nr_atendimento_p	number)
 		    	return varchar2 is

ds_retorno_altura varchar2(12);
			
begin
begin
	select 	NVL(b.qt_altura_cm ,0)
	into 	ds_retorno_altura
	from 	escala_must b
	where 	b.nr_sequencia = (
		select  max(nr_sequencia)
		from 	escala_must
		where 	ie_situacao   = 'A'							
		and		nr_atendimento in (select 	x.nr_atendimento
									from 	atendimento_paciente x
									where x.cd_pessoa_fisica = obter_dados_atendimento(nr_atendimento_p,'CP'))
									);
								
								
exception
  when others then
    ds_retorno_altura:= '0';
end;

return	ds_retorno_altura;

end obter_altura_escala;
/
