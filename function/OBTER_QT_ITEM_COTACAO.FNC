CREATE OR REPLACE
FUNCTION Obter_qt_item_cotacao(	
				nr_cot_compra_p		number,
				nr_item_cot_compra_p		number)	
			RETURN Number IS


qt_material_w		Number(13,4);

/*Retorna a quantidade original do item na cota��o.
Utilizo para verificar se algum item foi alterado a quantidade em determinado fornecedor*/

BEGIN

select	nvl(max(qt_material),0)
into	qt_material_w
from	cot_compra_item
where	nr_cot_compra		= nr_cot_compra_p
and	nr_item_cot_compra	= nr_item_cot_compra_p;

RETURN	qt_material_w;

END Obter_qt_item_cotacao;
/