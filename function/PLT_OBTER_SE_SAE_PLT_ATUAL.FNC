create or replace
function PLT_obter_se_sae_plt_atual(	nr_sequencia_p		number)
					return varchar2 is

ie_atual_w		varchar2(1);					
					
begin

select	nvl(max('S'),'N')
into	ie_atual_w
from	pe_prescricao
where	nr_sequencia	= nr_sequencia_p
and	sysdate		between dt_inicio_prescr and dt_validade_prescr;

return	ie_atual_w;

end PLT_obter_se_sae_plt_atual;
/
