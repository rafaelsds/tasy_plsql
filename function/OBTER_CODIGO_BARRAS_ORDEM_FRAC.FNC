create or replace
function obter_codigo_barras_ordem_frac(	nr_seq_ordem_p	number,
											nr_seq_digito_p	number default null)
				return varchar2 is
		
cd_barras_w		varchar2(14)	:= null;
nr_seq_digito_w	adep_processo_frac.nr_seq_digito%type;
					
begin
if	(nr_seq_ordem_p is not null) then

	if	(nr_seq_digito_p is null) then
		select	max(nr_seq_digito)
		into	nr_seq_digito_w
		from	adep_processo_frac
		where	nr_sequencia = nr_seq_ordem_p;
	else
		nr_seq_digito_w	:= nr_seq_digito_p;
	end if;

	if	(nr_seq_digito_w is not null) then
		cd_barras_w	:= '9' || lpad(nr_seq_ordem_p,10,0) || nr_seq_digito_w;
	end if;

end if;

return cd_barras_w;

end obter_codigo_barras_ordem_frac;
/