create or replace
function  obter_se_considera_sae(cd_setor_atendimento_p     number,
				cd_pessoa_fisica_p	    varchar2,
				nr_atendimento_p	    number)
 		    	return varchar2 is

ie_considerar_sae_w 		varchar2(1);
qt_idade_w			varchar2(10);
ie_paciente_isolamento_w	varchar2(1);
cd_tipo_acomodacao_w		number(10);
cd_convenio_w			convenio.cd_convenio%type;
nr_seq_classif_w		atendimento_paciente.nr_seq_classificacao%type;
ie_dependente_w			pessoa_fisica.ie_dependente%type;
nr_Seq_interno_w		atend_categoria_convenio.nr_Seq_interno%type;
cd_categoria_w			atend_categoria_convenio.cd_categoria%type;
cd_plano_convenio_w		atend_categoria_convenio.cd_plano_convenio%type;
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type;

begin

select	obter_idade_pf(cd_pessoa_fisica_p,sysdate,'A'),
	obter_dados_pf(cd_pessoa_fisica_p, 'DP')
into	qt_idade_w,
	ie_dependente_w
from 	dual;

select  obter_se_pac_isolamento(nr_atendimento_p),
	obter_dados_categ_conv(nr_atendimento_p,'A'),
	Obter_Atecaco_atendimento(nr_atendimento_p),
	obter_classif_atendimento(nr_atendimento_p),
	Obter_Tipo_Atendimento(nr_atendimento_p)
into	ie_paciente_isolamento_w,
	cd_tipo_acomodacao_w,
	nr_Seq_interno_w,
	nr_seq_classif_w,
	ie_tipo_atendimento_w
from 	dual;

select	max(cd_convenio),
		max(cd_categoria),
		max(cd_plano_convenio)
into	cd_convenio_w,
		cd_categoria_w,
		cd_plano_convenio_w
from	atend_categoria_convenio
where	nr_seq_interno = nr_Seq_interno_w
and		nr_atendimento = nr_atendimento_p;


select	nvl(max(ie_considerar_sae),'N')
into	ie_considerar_sae_w
from	regra_acompanhante_dieta
where 	((cd_setor_atendimento = cd_setor_atendimento_p) or (cd_setor_atendimento is null))
and	((cd_tipo_acomodacao  = cd_tipo_acomodacao_w) or (cd_tipo_acomodacao is null))
and	((qt_idade_w between nvl(qt_idade_de,0) and nvl(qt_idade_ate,999))  or ((nvl(ie_pac_deficiente, 'N')) = 'S' and (ie_dependente_w = 'S')))
and	(ie_paciente_isolamento_w = ie_isolado)
and	((cd_convenio = nvl(cd_convenio_w, 0)) or (cd_convenio is null))
and		((cd_categoria = nvl(cd_categoria_w, 'XPTO')) or (cd_categoria is null))
and		((cd_plano_convenio = nvl(cd_plano_convenio_w, 'XPTO')) or (cd_plano_convenio is null))
and		((ie_tipo_atendimento = nvl(ie_tipo_atendimento_w, 0)) or (ie_tipo_atendimento is null))
and ((nr_seq_classificacao = nvl(nr_seq_classif_w, 0)) or (nr_seq_classificacao is null));

return	ie_considerar_sae_w;

end	obter_se_considera_sae;
/