Create or Replace 
FUNCTION	Obter_preco_proced(
			cd_estabelecimento_p		Number,
			cd_convenio_p			Number,
			cd_categoria_p			Varchar2,
			dt_conta_p			Date,
			cd_procedimento_p			Number,
			ie_origem_proced_p		Number,
			cd_tipo_acomodacao_p		Number,
			ie_tipo_atendimento_p		Number, 
			cd_setor_atendimento_p		Number,
			cd_medico_p			Varchar2,
			cd_funcao_medico_p		Varchar2,
			cd_usuario_convenio_p		Varchar2,
			cd_plano_p			Varchar2,
			ie_clinica_p			Number,
			cd_empresa_ref_p			Number,
			ie_opcao_p			Varchar2,
			nr_seq_exame_lab_p		Number,
			nr_Seq_proc_interno_p		Number)
			Return Number IS

/* P - Procedimento	
     M - M�dico	
     C - Custo Op.	
     F - Filme   
     PM - Ponto M�dico
     E - Edi��o AMB */
/*  H - Honorario*/

qt_pontos_w			preco_amb.qt_pontuacao%type;
vl_procedimento_w			Number(15,2);
vl_custo_operacional_w		Number(15,2);
vl_anestesista_w			Number(15,2);
vl_medico_w				Number(15,2);
vl_auxiliares_w			Number(15,2);
vl_materiais_w			Number(15,2);
vl_pto_procedimento_w		Number(15,4);
vl_pto_custo_operac_w		Number(15,4);
vl_pto_anestesista_w			Number(15,4);
vl_pto_medico_w			Number(15,4);
vl_pto_auxiliares_w			Number(15,4);
vl_pto_materiais_w			Number(15,4);
qt_porte_anestesico_w		Number(15,4);
cd_edicao_amb_w			Number(06,0);
vl_resultado_w			Number(15,2);
cd_area_procedimento_w		Number(15,2);
ie_preco_informado_w		varchar2(01);
ie_classificacao_w		varchar2(01);
nr_seq_ajuste_proc_w		number(10,0);

BEGIN

select	max(ie_classificacao)
into	ie_classificacao_w
from	procedimento
where	cd_procedimento	= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

if	(ie_classificacao_w = '1') or
	(nr_Seq_proc_interno_p is not null) or
	(nr_Seq_exame_lab_p is not null) then
	begin	
	Define_Preco_Procedimento(
		CD_ESTABELECIMENTO_P,
		CD_CONVENIO_P,
		CD_CATEGORIA_P,
		DT_CONTA_P,
		CD_PROCEDIMENTO_P,
		CD_TIPO_ACOMODACAO_P,
		IE_TIPO_ATENDIMENTO_P,
		CD_SETOR_ATENDIMENTO_P,
		CD_MEDICO_P,
		CD_FUNCAO_MEDICO_P,
		Null,
		nr_seq_exame_lab_p,
		nr_Seq_proc_interno_p,
		cd_usuario_convenio_p, cd_plano_p, ie_clinica_p, cd_empresa_ref_p,null,
		VL_PROCEDIMENTO_W,
		VL_CUSTO_OPERACIONAL_W,
		VL_ANESTESISTA_W,
		VL_MEDICO_W,
		VL_AUXILIARES_W,
		VL_MATERIAIS_W,
		VL_PTO_PROCEDIMENTO_W,
		VL_PTO_CUSTO_OPERAC_W,
		VL_PTO_ANESTESISTA_W,
		VL_PTO_MEDICO_W,
		VL_PTO_AUXILIARES_W,
		VL_PTO_MATERIAIS_W,
		QT_PORTE_ANESTESICO_W,
		qt_pontos_w,
		CD_EDICAO_AMB_W, ie_preco_informado_w, nr_seq_ajuste_proc_w, 0, null, 0, null, null,
		NULL, NULL, null, null, null, null, null, null, null, null, null, ie_origem_proced_p,null, null, null);
	end;
else	begin
	Define_Preco_servico(
		CD_ESTABELECIMENTO_P,
              CD_CONVENIO_P,
              CD_CATEGORIA_P,
              dt_conta_p,
              cd_procedimento_p,
		CD_SETOR_ATENDIMENTO_P,
		IE_TIPO_ATENDIMENTO_P,
		CD_TIPO_ACOMODACAO_P,
		cd_usuario_convenio_p,
		cd_plano_p,
		ie_clinica_p,
		cd_empresa_ref_p,null,
              vl_procedimento_w, nr_seq_ajuste_proc_w, null, 0, null,
	      nr_Seq_proc_interno_p, null,null,null,null, null, null, null,null, null, CD_MEDICO_P);
	end;

end if;

if	(ie_opcao_p	is not null) then
	begin
	if	(ie_opcao_p	= 'P') then
		vl_resultado_w	:= vl_procedimento_w;
	elsif	(ie_opcao_p	= 'C') then
		vl_resultado_w	:= vl_custo_operacional_w;
	elsif	(ie_opcao_p	= 'M') then
		vl_resultado_w	:= vl_medico_w;
	elsif	(ie_opcao_p	= 'F') then
		vl_resultado_w	:= vl_materiais_w;
	elsif	(ie_opcao_p	= 'PP') then
		vl_resultado_w	:= vl_pto_procedimento_w;
	elsif	(ie_opcao_p	= 'PM') then
		vl_resultado_w	:= VL_PTO_MEDICO_W;
	elsif	(ie_opcao_p	= 'H') then
		vl_resultado_w	:= VL_AUXILIARES_W + vl_medico_w + VL_ANESTESISTA_W;
	elsif	(ie_opcao_p	= 'E') then
		vl_resultado_w	:= CD_EDICAO_AMB_W;
	end if;
	
	end;
end if;

RETURN vl_resultado_w;
END Obter_preco_proced;
/
