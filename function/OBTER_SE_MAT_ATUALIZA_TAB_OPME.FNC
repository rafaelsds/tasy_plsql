CREATE OR REPLACE
function obter_se_mat_atualiza_tab_opme(		cd_estabelecimento_p	number,
						cd_material_p		number)
return varchar2 is

cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
nr_seq_familia_w				number(5);
ie_consignado_w				varchar2(15);
ie_atualiza_w				varchar2(1) := 'N';
qt_registro_w				number(10);

begin

select	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material,
	a.ie_consignado,
	a.nr_seq_familia
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	ie_consignado_w,
	nr_seq_familia_w
from	estrutura_material_v a
where	a.cd_material = cd_material_p;

select	count(*)
into	qt_registro_w
from	regra_atualiza_tab_opme
where	cd_estabelecimento = cd_estabelecimento_p
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	nvl(ie_consignado, ie_consignado_w)			= ie_consignado_w
and	(nvl(cd_material, cd_material_p) 			= cd_material_p or cd_material_p = 0)
and	((nvl(nr_seq_familia, nr_seq_familia_w)			= nr_seq_familia_w) or (nr_seq_familia is null));

if	(qt_registro_w > 0) then
	ie_atualiza_w := 'S';
end if;

return ie_atualiza_w;

end obter_se_mat_atualiza_tab_opme;
/
