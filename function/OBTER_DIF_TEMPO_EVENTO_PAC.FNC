create or replace 
function obter_dif_tempo_evento_pac	(	nr_cirurgia_p		number,
											nr_seq_evento_ini_p	varchar2, 
											nr_seq_evento_fim_p	varchar2) 
											return number	is


dt_evento_ini_w			date;
dt_evento_fim_w 		date;
qt_min_retorno_w		number(15,4);

begin

select	max(dt_registro)
into	dt_evento_ini_w
from	evento_cirurgia_paciente
where	nr_cirurgia = nr_cirurgia_p
and	nr_seq_evento = nr_seq_evento_ini_p
and	nvl(ie_situacao,'A') = 'A'
order by 1;

select	max(dt_registro)
into	dt_evento_fim_w
from	evento_cirurgia_paciente
where	nr_cirurgia = nr_cirurgia_p
and	nr_seq_evento = nr_seq_evento_fim_p
and	nvl(ie_situacao,'A') = 'A'
order by 1;

if	(dt_evento_ini_w is not null) and (dt_evento_fim_w is not null) then
	select	(dt_evento_fim_w - dt_evento_ini_w) * 1440
	into	qt_min_retorno_w
	from	dual;
end if;

return nvl(qt_min_retorno_w,0);

end	obter_dif_tempo_evento_pac;
/
