create or replace
function bsc_obter_desc_periodo (nr_seq_indicador_p	number)
 		    		             return varchar2 is

ds_periodo_w			varchar2(20);
begin

select	substr(obter_valor_dominio(2303, nvl(ie_periodo,'M')),1,20)
into	ds_periodo_w
from	bsc_indicador
where	nr_sequencia = nr_seq_indicador_p;

return ds_periodo_w;

end bsc_obter_desc_periodo;
/