create or replace
function obter_unid_atend_adep	(nr_atendimento_p	number,
					dt_unidade_p		date,
					ie_informacao_p	varchar2)
					return varchar2 is

ds_unid_adep_w		varchar2(255);
nr_seq_interno_w		number(10,0);
cd_setor_atendimento_w	number(5,0);
ds_setor_atendimento_w	varchar2(100);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);

begin
if	(nr_atendimento_p is not null) and
	(dt_unidade_p is not null) and
	(ie_informacao_p is not null) then

	/* obter atend_paciente_unidade x data */
	select	nvl(max(a.nr_seq_interno),0)
	into	nr_seq_interno_w
	from	atend_paciente_unidade a
	where	a.nr_atendimento	= 	nr_atendimento_p
	and	a.nr_seq_interno	=	(
						select	nvl(max(x.nr_seq_interno),0)
						from	setor_atendimento y,
							atend_paciente_unidade x
						where	y.cd_classif_setor			in (1,3,4,8)
						and	y.cd_setor_atendimento		= x.cd_setor_atendimento
						and	nvl(x.ie_passagem_setor,'N')	<> 'S'
						and	x.dt_entrada_unidade			<= dt_unidade_p
						and	x.nr_atendimento			= nr_atendimento_p
						);

	/* obter informacao */
	if	(nr_seq_interno_w > 0) then

		select	cd_setor_atendimento,
			substr(obter_nome_setor(cd_setor_atendimento),1,100) ds_setor_atendimento,
			cd_unidade_basica,
			cd_unidade_compl
		into	cd_setor_atendimento_w,
			ds_setor_atendimento_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w
		from	atend_paciente_unidade
		where	nr_atendimento = nr_atendimento_p
		and	nr_seq_interno = nr_seq_interno_w;

		if	(ie_informacao_p = 'SU') then
			ds_unid_adep_w := ds_setor_atendimento_w || ' ' || cd_unidade_basica_w || ' ' || cd_unidade_compl_w;
		elsif	(ie_informacao_p = 'S') then
			ds_unid_adep_w := ds_setor_atendimento_w;
		elsif	(ie_informacao_p = 'U') then
			ds_unid_adep_w := cd_unidade_basica_w || ' ' || cd_unidade_compl_w;
		end if;

	end if;

end if;

return ds_unid_adep_w;

end obter_unid_atend_adep;
/