create or replace
function Obter_Atrib_Unid_Med(	nm_tabela_p	varchar2,
				nm_atributo_p	varchar2,
				dt_referencia_p	date) 
				return number is

nr_sequencia_w	number(10);

begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	tab_atrib_regra_unid_med
where nm_tabela = nm_tabela_p
   and nm_atributo = nm_atributo_p
   and dt_referencia_p between dt_inicio_vigencia and fim_dia(nvl(dt_final_vigencia,dt_referencia_p));

return nr_Sequencia_w;

end;
/