create or replace
function obter_label_mecanismos_calculo(  nm_tabela_p    in varchar2,
                                          nm_atributo_p	in varchar2)
                                          return varchar2 is


ds_retorno_w   calculos_engine_dest_cli.ds_label%type;
begin
ds_retorno_w := null;
if (nm_tabela_p is not null) and (nm_atributo_p is not null) then
	select 	max(ds_label)
	into	   ds_retorno_w
	from	   calculos_engine_dest_cli
	where	   ds_inf_destino    = nm_tabela_p
   and      ds_campo_destino  = nm_atributo_p;
end if;

return	ds_retorno_w;

end obter_label_mecanismos_calculo;
/