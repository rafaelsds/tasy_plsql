create or replace
function obter_pessoa_laudo(nr_seq_grupo_laudo_p	number)
 		    	return varchar2 is
			
ds_retorno_w varchar2(255);
begin

select	(max(substr(obter_pessoa_atendimento(l.nr_atendimento,'n'),1,255))) 
into	ds_retorno_w
from   	laudo_questao_item i,
	laudo_grupo_questao g,
	laudo_paciente l
where  	i.nr_seq_laudo_grupo = g.nr_sequencia
and    	l.nr_sequencia = g.nr_seq_laudo
and	i.nr_seq_laudo_grupo = nr_seq_grupo_laudo_p;

return	ds_retorno_w;

end obter_pessoa_laudo;
/