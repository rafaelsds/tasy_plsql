create or replace 
function Obter_Dia_Semana	(dt_referencia_p	date)
		return	varchar2 is

ds_dia_semana_w			varchar2(30);

begin
	select decode(pkg_date_utils.get_weekday(dt_referencia_p),
			1, obter_desc_expressao(288200),
			2, obter_desc_expressao(298106),
			3, obter_desc_expressao(299302),
			4, obter_desc_expressao(297139),
			5, obter_desc_expressao(297214),
			6, obter_desc_expressao(298488),
			7, obter_desc_expressao(297960))
	into ds_dia_semana_w
	from dual;

return ds_dia_semana_w;

end Obter_Dia_Semana;
/