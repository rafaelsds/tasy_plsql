CREATE OR REPLACE
FUNCTION TISS_Obter_Espec_medico(cd_medico_p 	Varchar2,
                                  ie_opcao_p        VarChar2)	
				   	RETURN Varchar2 IS

cd_especialidade_w		Number(5)	:= null;
ds_especialidade_w		Varchar2(255)	:= '';

Cursor C01 is
select	a.cd_especialidade
from	medico_especialidade a
where	a.cd_pessoa_fisica = cd_medico_p
and	exists (select	1 
		from	tiss_cbo_saude b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.cd_especialidade	= b.cd_especialidade)
order by a.nr_seq_prioridade desc;

BEGIN

OPEN C01;
LOOP
FETCH C01 into
	cd_especialidade_w;
exit when c01%notfound;
	cd_especialidade_w	:= cd_especialidade_w;
END LOOP;
CLOSE C01;

if 	(substr(ie_opcao_p,1,1) = 'D') and
	(cd_especialidade_w is not null) then
	select	ds_especialidade
	into	ds_especialidade_w
	from	especialidade_medica
	where	cd_especialidade = cd_especialidade_w;
end if;

if	(substr(ie_opcao_p,1,1) <> 'D') then
	ds_especialidade_w	:= cd_especialidade_w;
end if;

RETURN ds_especialidade_w;

END TISS_Obter_Espec_medico;
/
