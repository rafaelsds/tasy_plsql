create or replace
function Obter_mensagem_material(	cd_material_p	number)
 		    	return varchar2 is

ds_mensagem_w	varchar2(2000);

begin

if	(cd_material_p is not null) then
	select	max(ds_mensagem)
	into	ds_mensagem_w
	from	material
	where	cd_material = cd_material_p;
end if;

return	ds_mensagem_w;

end Obter_mensagem_material;
/