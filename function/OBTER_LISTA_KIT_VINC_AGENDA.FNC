create or replace
function	obter_lista_kit_vinc_agenda(nr_seq_agenda_p	number)
					return varchar2 is	
					
					
ie_ordem_w		number(1);
cd_kit_material_w	number(10);
ds_kit_material_w	varchar2(100);
ds_lista_kit_w		varchar2(4000) := null;
nr_seq_proc_interno_w	proc_interno.nr_sequencia%type;
cd_medico_kit_w 			pessoa_fisica.cd_pessoa_fisica%type;
cd_medico_agenda_w		pessoa_fisica.cd_pessoa_fisica%type;

cursor	c01 is
	select   1 ie_ordem,
				b.cd_kit_material,
				substr(k.ds_kit_material,1,100) ds_kit_material,
				null cd_medico_kit,
				null cd_medico_agenda
	from     kit_material k,
				proc_interno b,
				agenda_paciente a
	where    a.nr_seq_proc_interno= b.nr_sequencia
	and      a.nr_sequencia       = nr_seq_agenda_p
	and      b.cd_kit_material    = k.cd_kit_material
	and      k.ie_situacao        = 'A'
	and		a.nr_seq_proc_interno is not null
	and		b.cd_kit_material is not null	
	union
	select   1 ie_ordem,
				c.cd_kit_material,
				substr(k.ds_kit_material,1,100) ds_kit_material,
				c.cd_medico cd_medico_kit,
				a.cd_medico cd_medico_agenda
	from     kit_material k,
				proc_interno b,
				proc_interno_kit c,
				agenda_paciente a
	where    a.nr_seq_proc_interno= b.nr_sequencia
	and      c.nr_seq_proc_interno= b.nr_sequencia
	and      a.nr_sequencia       = nr_seq_agenda_p
	--and      ((c.cd_medico         = a.cd_medico) or (c.cd_medico is null) or (a.cd_medico is null))
	and      c.cd_kit_material    = k.cd_kit_material
	and      k.ie_situacao        = 'A'
	and		a.nr_seq_proc_interno is not null;

cursor	c02 is	
	select   2 ie_ordem,
				b.cd_kit_material,
				substr(k.ds_kit_material,1,100) ds_kit_material,
				null cd_medico_kit,
				null cd_medico_agenda
	from     kit_material k,
				proc_interno b,
				agenda_paciente_proc a
	where    a.nr_seq_proc_interno= b.nr_sequencia
	and      a.nr_sequencia       = nr_seq_agenda_p
	and      b.cd_kit_material    = k.cd_kit_material
	and      k.ie_situacao        = 'A'
	and		a.nr_seq_proc_interno is not null
	and		b.cd_kit_material is not null	
	union
	select   2 ie_ordem,
				c.cd_kit_material,
				substr(k.ds_kit_material,1,100) ds_kit_material,
				c.cd_medico cd_medico_kit,
				a.cd_medico cd_medico_agenda
	from     kit_material k,
				proc_interno b,
				proc_interno_kit c,
				agenda_paciente_proc a
	where    a.nr_seq_proc_interno= b.nr_sequencia
	and      c.nr_seq_proc_interno= b.nr_sequencia
	--and      ((c.cd_medico         = a.cd_medico) or (c.cd_medico is null) or (a.cd_medico is null))
	and      a.nr_sequencia       = nr_seq_agenda_p
	and      c.cd_kit_material    = k.cd_kit_material
	and      k.ie_situacao        = 'A'
	and		a.nr_seq_proc_interno is not null;

begin

if	(nvl(nr_seq_agenda_p,0) > 0) then
	select	max(nr_seq_proc_interno)
	into		nr_seq_proc_interno_w
	from		agenda_paciente
	where		nr_sequencia       = nr_seq_agenda_p;

	if	(nr_seq_proc_interno_w is not null) then
		open c01;
		loop
		fetch c01 into	
			ie_ordem_w,
			cd_kit_material_w,
			ds_kit_material_w,
			cd_medico_kit_w,
			cd_medico_agenda_w;
		exit when c01%notfound;
			begin
			if	(cd_medico_kit_w = cd_medico_agenda_w) or	(cd_medico_kit_w is null) or 	(cd_medico_agenda_w is null) then
				if	(ds_lista_kit_w is null) then
					ds_lista_kit_w :=  substr('[' ||cd_kit_material_w || ' - ' ||ds_kit_material_w||']',1,4000);
				else	
					ds_lista_kit_w :=  substr(ds_lista_kit_w || '[' ||cd_kit_material_w || ' - ' ||ds_kit_material_w||']',1,4000);
				end if;	
			end if;	
			end;
		end loop;
		close c01;
	end if;

	select	max(nr_seq_proc_interno)
	into		nr_seq_proc_interno_w
	from		agenda_paciente_proc
	where		nr_sequencia       = nr_seq_agenda_p;

	if	(nr_seq_proc_interno_w is not null) then
		open c02;
		loop
		fetch c02 into	
			ie_ordem_w,
			cd_kit_material_w,
			ds_kit_material_w,
			cd_medico_kit_w,
			cd_medico_agenda_w;
		exit when c02%notfound;
			begin
			if	(cd_medico_kit_w = cd_medico_agenda_w) or	(cd_medico_kit_w is null) or 	(cd_medico_agenda_w is null) then
				if	(ds_lista_kit_w is null) then
					ds_lista_kit_w :=  substr('[' ||cd_kit_material_w || ' - ' ||ds_kit_material_w||']',1,4000);
				else	
					ds_lista_kit_w :=  substr(ds_lista_kit_w || '[' ||cd_kit_material_w || ' - ' ||ds_kit_material_w||']',1,4000);
				end if;	
			end if;	
			end;
		end loop;
		close c02;
	end if;
end if;	

return ds_lista_kit_w;

end obter_lista_kit_vinc_agenda;
/