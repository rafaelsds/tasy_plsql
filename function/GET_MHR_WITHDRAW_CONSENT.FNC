create or replace
function GET_MHR_WITHDRAW_CONSENT	(	nr_atendimento_p	atendimento_paciente.nr_atendimento%type)
 		    	return varchar2 is

qt_reg_w		number(10);	
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;

	
begin

--Check the consent under the encounter


select	count(1)
into	qt_reg_w
from	pep_pac_ci
where	nr_atendimento		= nr_atendimento_p
and	ie_tipo_consentimento	= 'MHR'
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(qt_reg_w	> 0) then
	return 'S';
end if;

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

--Check the consent under the patient

select	count(1)
into	qt_reg_w
from	pep_pac_ci
where	cd_pessoa_fisica	= cd_pessoa_fisica_w
and	ie_tipo_consentimento	= 'MHR'
and	nr_atendimento is null
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(qt_reg_w	> 0) then
	return 'S';
end if;


return	'N';

end get_mhr_withdraw_consent;
/