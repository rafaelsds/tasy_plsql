create or replace
function Obter_Hora_Apap_GP	(dt_referencia_p		date)
 		    	return number is

nr_hora_w	number(10);
ie_regra_apap_ganho_perda_w	varchar2(10);
begin

select	nvl(max(ie_regra_apap_ganho_perda),'R')
into	ie_regra_apap_ganho_perda_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;


if	(ie_regra_apap_ganho_perda_w	= 'R') then

	nr_hora_w	:= to_number(to_char(round(dt_referencia_p,'hh24'),'hh24'));

else
	nr_hora_w	:= to_number(to_char(trunc(dt_referencia_p,'hh24'),'hh24'));

end if;

return	nr_hora_w;

end Obter_Hora_Apap_GP;
/