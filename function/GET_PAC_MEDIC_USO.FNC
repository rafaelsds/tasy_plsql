create or replace
function get_pac_medic_uso	( nr_atendimento_p	number default null) return varchar2 is

ds_medicamento_w varchar2(100);
ds_medicamentos_w varchar2(1000);

cursor c01 is
select 	substr(obter_desc_material(cd_material),1,100)
from	paciente_medic_uso
where	nr_atendimento = nr_atendimento_p;

begin

	open c01;
		loop
			fetch c01 into
				ds_medicamento_w;
			exit when c01%notfound;
			
			if (ds_medicamentos_w is not null) then
				ds_medicamentos_w := ds_medicamentos_w || ', ';
			end if;
			ds_medicamentos_w := substr(ds_medicamentos_w || ds_medicamento_w, 1, 1000);
		end loop;
	close c01;
	
	return ds_medicamentos_w;

end get_pac_medic_uso;
/