create or replace
function Obter_san_proc_tran_parcial(nr_seq_reserva_p	NUMBER,
					 nr_seq_transfusao_p	NUMBER,
					 nr_seq_exame_p		NUMBER,
					 ie_busca_sentido_p	VARCHAR) -- 'A' Ambas dire��es; 'O' na Origem; 'D' no Destino
 		    	return NUMBER is

nr_seq_retorno_w	NUMBER(10);
nr_seq_reserva_w	NUMBER(10);
nr_seq_res_origem_w	NUMBER(10);
nr_seq_res_gerada_w	NUMBER(10);
nr_seq_transf_origem_w	NUMBER(10);
nr_seq_transf_destino_w	NUMBER(10);
begin

IF (nr_seq_exame_p IS NOT NULL) AND (ie_busca_sentido_p IS NOT NULL) THEN 

	--1a Parte: verifica se a transfus�o � remanescente de reserva anterior j� transfundido (quebra de reserva com transfus�o parcial)
	IF (ie_busca_sentido_p = 'A') OR (ie_busca_sentido_p = 'O') then
		IF (nr_seq_transfusao_p IS NOT NULL) THEN
			SELECT MAX(nr_seq_reserva)
			INTO	nr_seq_reserva_w
			FROM	san_transfusao
			WHERE	nr_sequencia = nr_seq_transfusao_p;		
		END IF;
		
		SELECT MAX(nr_seq_reserva_origem)
		INTO	nr_seq_res_origem_w
		FROM	san_reserva
		WHERE	nr_sequencia = nvl(nr_seq_reserva_w, nr_seq_reserva_p);
		
		IF (nr_seq_res_origem_w IS NOT NULL) THEN
			SELECT MAX(nr_sequencia)
			INTO	nr_seq_transf_origem_w
			FROM	san_transfusao
			WHERE	nr_seq_reserva = nr_seq_res_origem_w;
		END IF;				

		IF (nr_seq_transf_origem_w IS NOT NULL) THEN
			SELECT 	MAX(a.nr_sequencia)
			INTO	nr_seq_retorno_w
			FROM 	procedimento_paciente a,
				san_exame_realizado b,
				san_exame_lote c
			WHERE 	b.nr_seq_exame_lote = c.nr_sequencia
			and	c.nr_seq_transfusao = nr_seq_transf_origem_w
			and	a.nr_seq_transfusao = nr_seq_transf_origem_w
			and	b.nr_seq_propaci = a.nr_sequencia
			AND 	b.nr_seq_exame = nr_seq_exame_p;
		END IF;
		
		IF (nvl(nr_seq_retorno_w,0) <= 0) and (nr_seq_res_origem_w is not null) THEN
			SELECT 	MAX(a.nr_sequencia)
			INTO	nr_seq_retorno_w
			FROM 	procedimento_paciente a,
				san_exame_realizado b,
				san_exame_lote c
			WHERE 	b.nr_seq_exame_lote = c.nr_sequencia
			and	c.nr_seq_reserva = nr_seq_res_origem_w
			and	a.nr_seq_reserva = nr_seq_res_origem_w
			and	b.nr_seq_propaci = a.nr_sequencia
			AND 	b.nr_seq_exame = nr_seq_exame_p;
		END IF;
		
		--1.1a Parte: recursividade, verificando se sequ�ncia de doa��o ou de transfus�o de origem, � desdobramento de outro registro.
		IF (nvl(nr_seq_retorno_w,0) <= 0) THEN
			
			IF (nr_seq_res_origem_w IS NOT NULL) OR (nr_seq_transf_origem_w IS NOT NULL) THEN
				nr_seq_retorno_w := Obter_san_proc_tran_parcial(nr_seq_res_origem_w, nr_seq_transf_origem_w, nr_seq_exame_p, 'O');
			END IF;
		
		END IF;
	END IF;
	
	--2a Parte: verifica se reserva originou quebras (novas reservas) por transfus�o parcial.
	IF (ie_busca_sentido_p = 'A') OR (ie_busca_sentido_p = 'D') then
		IF (nvl(nr_seq_retorno_w,0) <= 0) THEN
			SELECT MAX(nr_sequencia)
			INTO	nr_seq_res_gerada_w
			FROM	san_reserva
			WHERE	nr_seq_reserva_origem = nr_seq_reserva_p;
		
			IF (nr_seq_res_gerada_w IS NOT NULL) THEN
				SELECT MAX(nr_sequencia)
				INTO	nr_seq_transf_destino_w
				FROM	san_transfusao
				WHERE	nr_seq_reserva = nr_seq_res_gerada_w;
			END IF;	
			
			IF (nr_seq_transf_destino_w IS NOT NULL) THEN
				SELECT 	MAX(a.nr_sequencia)
				INTO	nr_seq_retorno_w
				FROM 	procedimento_paciente a,
					san_exame_realizado b,
					san_exame_lote c
				WHERE 	b.nr_seq_exame_lote = c.nr_sequencia
				and	c.nr_seq_transfusao = nr_seq_transf_destino_w
				and	a.nr_seq_transfusao = nr_seq_transf_destino_w
				and	b.nr_seq_propaci = a.nr_sequencia
				AND 	b.nr_seq_exame = nr_seq_exame_p;
			END IF;
			
			IF (nvl(nr_seq_retorno_w,0) <= 0) and (nr_seq_res_gerada_w is not null) THEN
				SELECT 	MAX(a.nr_sequencia)
				INTO	nr_seq_retorno_w
				FROM 	procedimento_paciente a,
					san_exame_realizado b,
					san_exame_lote c
				WHERE 	b.nr_seq_exame_lote = c.nr_sequencia
				and	c.nr_seq_reserva = nr_seq_res_gerada_w
				and	a.nr_seq_reserva = nr_seq_res_gerada_w
				and	b.nr_seq_propaci = a.nr_sequencia
				AND 	b.nr_seq_exame = nr_seq_exame_p;
			END IF;
			
			--2.1a Parte: recursividade, verificando se sequ�ncia de doa��o ou de transfus�o, � desdobrada para outro registro.
			IF (nvl(nr_seq_retorno_w,0) <= 0) THEN
				
				IF (nr_seq_res_gerada_w IS NOT NULL) OR (nr_seq_transf_destino_w IS NOT NULL) THEN
					nr_seq_retorno_w := Obter_san_proc_tran_parcial(nr_seq_res_gerada_w, nr_seq_transf_destino_w, nr_seq_exame_p, 'D');
				END IF;
				
			END IF;	
				
		END IF;
	END IF;

	
END IF;

return	nvl(nr_seq_retorno_w,0);

end Obter_san_proc_tran_parcial;
/