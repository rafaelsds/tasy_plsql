create or replace
function Qua_Obter_Se_Novo_Hist(	nr_seq_nao_conform_p	number,
									nm_usuario_p		varchar2)
				return varchar2 is

ie_novo_historico_w			char(1) := 'N';
dt_acesso_w					date;
dt_historico_w				date;
qt_existe_hist_w			Number(5);
qt_existe_usuario_w			Number(5);
ie_consid_hist_sistema_w	varchar2(01);

begin
/*Nao existe novo historico*/

obter_param_usuario(4000, 266, obter_perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_consid_hist_sistema_w);

select	nvl(max('S'),'N')
into	ie_novo_historico_w
from	qua_nao_conform_hist
where	rownum = 1
and		nr_seq_nao_conform	= nr_seq_nao_conform_p
and		((ie_origem = 'U' and ie_consid_hist_sistema_w = 'N') or (ie_consid_hist_sistema_w = 'S'));

if	(ie_novo_historico_w = 'S') then
	begin
	select	nvl(max('N'),'S')
	into	ie_novo_historico_w
	from	qua_nao_conform_leitura
	where	rownum = 1
	and		nr_seq_nao_conform	= nr_seq_nao_conform_p
	and		nm_usuario_acesso	= nm_usuario_p;
	
	if	(ie_novo_historico_w = 'N') then
		begin		
		select	max(dt_acesso)
		into	dt_acesso_w
		from	qua_nao_conform_leitura
		where	nr_seq_nao_conform	= nr_seq_nao_conform_p
		and		nm_usuario_acesso	= nm_usuario_p;

		select	max(dt_historico)
		into	dt_historico_w
		from	qua_nao_conform_hist
		where	nr_seq_nao_conform	= nr_seq_nao_conform_p
		and		((ie_origem = 'U' and ie_consid_hist_sistema_w = 'N') or (ie_consid_hist_sistema_w = 'S'))
		and		nm_usuario <> nm_usuario_p;

		if	(dt_acesso_w is null) or
			(dt_acesso_w < dt_historico_w) then
			ie_novo_historico_w	:= 'S';
		end if;
		end;
	end if;
	end;
end if;

return ie_novo_historico_w;

end Qua_Obter_Se_Novo_Hist;
/