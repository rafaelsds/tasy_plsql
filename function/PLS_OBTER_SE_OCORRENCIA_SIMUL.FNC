create or replace
function pls_obter_se_ocorrencia_simul
			(	nr_seq_simultaneo_p		Number,
				cd_procedimento_p		Number,
				ie_origem_proced_p		Number,
				ie_tipo_item_p			Number,
				nr_seq_item_p			number)
				return Varchar2 is
				
ie_simultaneo_w			varchar(1) := 'N';
cd_procedimento_w		number(15) := 0;
ie_origem_proced_w		Number(10);
nr_seq_guia_w			number(10);
nr_seq_conta_w			number(10);
qt_contador_simul_w		number(5) := 0;
qt_proc_w			number(5);
qt_proc_simul_w			number(5) := 0;
qt_item_proc_simul_w		number(5) := 0;

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia = nr_seq_guia_w
	order by 1;
	
Cursor C02 is
	select	cd_procedimento,
		ie_origem_proced
	from	pls_conta_proc
	where	nr_seq_conta = nr_seq_conta_w
	order by 1;
	
begin

select	count(1)
into	qt_proc_simul_w
from	pls_ocorrencia_simul_item
where	nr_seq_regra		= nr_seq_simultaneo_p
and	cd_procedimento 	= cd_procedimento_p
and	ie_origem_proced 	= ie_origem_proced_p
and	ie_ocorrencia		= 'S'
and	ie_situacao 		= 'A';

if	(qt_proc_simul_w > 0) then	
	if	(ie_tipo_item_p = 1) then
		select	nr_seq_guia
		into	nr_seq_guia_w 
		from	pls_guia_plano_proc
		where	nr_sequencia = nr_seq_item_p;
		
		for r_c01_w in C01() loop
			begin
			
			select	count(1)
			into	qt_proc_w
			from	pls_ocorrencia_simul_item
			where	nr_seq_regra		= nr_seq_simultaneo_p
			and	cd_procedimento 	= r_c01_w.cd_procedimento
			and	ie_origem_proced	= r_c01_w.ie_origem_proced
			and	ie_situacao 		= 'A';
			
			qt_contador_simul_w := qt_contador_simul_w + qt_proc_w;	
			end;
		end loop;
		
		select	count(1)
		into	qt_item_proc_simul_w
		from	pls_guia_plano_proc
		where	nr_seq_guia = nr_seq_guia_w;		
	elsif	(ie_tipo_item_p = 3) then
		select	nr_seq_conta
		into	nr_seq_conta_w
		from	pls_conta_proc
		where	nr_sequencia = nr_seq_item_p;
		
		for r_c02_w in C02() loop
			begin
			select	count(1)
			into	qt_proc_w
			from	pls_ocorrencia_simul_item
			where	nr_seq_regra		= nr_seq_simultaneo_p
			and	cd_procedimento 	= r_c02_w.cd_procedimento
			and	ie_origem_proced 	= r_c02_w.ie_origem_proced
			and	ie_situacao = 'A';
			
			qt_contador_simul_w := qt_contador_simul_w + qt_proc_w;
			
			end;
		end loop;
		
		select	count(1)
		into	qt_item_proc_simul_w
		from	pls_conta_proc
		where	nr_seq_conta = nr_seq_conta_w;
	end if;	
	
	select	count(1)
	into	qt_proc_simul_w
	from	pls_ocorrencia_simul_item
	where	nr_seq_regra	= nr_seq_simultaneo_p;

	if	( qt_item_proc_simul_w = qt_contador_simul_w ) and
		( qt_proc_simul_w = qt_contador_simul_w ) then
		ie_simultaneo_w := 'S';
	end if;
end if;

return	ie_simultaneo_w;

end pls_obter_se_ocorrencia_simul;
/