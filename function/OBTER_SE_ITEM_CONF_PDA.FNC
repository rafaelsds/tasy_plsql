create or replace
function obter_se_item_conf_pda(nr_seq_horario_p	Number,
								nr_prescricao_p		Number,
								nr_seq_item_p		Number,
								dt_horario_p		date,
								ie_tipo_item_p		Varchar2)
 		    					return Varchar2 is

ds_retorno_w	varchar2(1)	:= 'N';		
qt_reg_w		Number(5);						
								
begin

if	(nr_seq_horario_p	is not null) then
	begin
	
	select	count(1)
	into	qt_reg_w
	from	administracao_lote_item
	where	nr_seq_horario = nr_seq_horario_p
	and		ie_situacao = 'S';
	
	if	(qt_reg_w = 0) then
		begin
		
		if	(ie_tipo_item_p = 'M') then			
			
			select	count(1)
			into	qt_reg_w
			from	prescr_mat_alteracao
			where	nr_seq_horario = nr_seq_horario_p
			and		ie_alteracao = 58
			and		ie_evento_valido = 'S';
			
			if	(qt_reg_w = 0) then
						
			    select	count(1)
				into	qt_reg_w
				from	prescr_solucao_evento
				where	nr_prescricao = nr_prescricao_p
				and		nr_seq_material = nr_seq_item_p	
				and		ie_alteracao = 54
				and		ie_evento_valido = 'S'
				and		dt_horario = dt_horario_p;
				
			end if;
			
		elsif  (ie_tipo_item_p in ('IA', 'C', 'G', 'CCG', 'CIG')) then				
		
			select	count(1)
			into	qt_reg_w
			from	prescr_mat_alteracao
			where	dt_horario = dt_horario_p			
			and		nr_prescricao = nr_prescricao_p
			and		nr_seq_procedimento = nr_seq_item_p
			and		ie_alteracao = 58
			and		ie_evento_valido = 'S';
			
		elsif  (ie_tipo_item_p in ('GAS')) then				
		
			select	count(1)
			into	qt_reg_w
			from	prescr_gasoterapia_evento
			where	nr_seq_horario = nr_seq_horario_p
			and		ie_evento = 'CR'
			and		ie_evento_valido = 'S'
			and		nr_seq_gasoterapia = nr_seq_item_p;
			
		elsif  (ie_tipo_item_p in ('SOL')) then		

			select	count(1)
			into	qt_reg_w
			from	prescr_solucao_evento
			where	nr_prescricao = nr_prescricao_p
			and		nr_seq_solucao = nr_seq_item_p
			and		ie_alteracao = 54
			and		dt_horario = dt_horario_p
			and		ie_evento_valido = 'S';
			
		end if;
		
		end;
	end if;
	
	end;
end if;

if	(qt_reg_w > 0) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end obter_se_item_conf_pda;
/