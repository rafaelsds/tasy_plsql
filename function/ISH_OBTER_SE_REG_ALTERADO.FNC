create or replace
function ish_obter_se_reg_alterado(
		nr_seq_fila_p		number,
		creation_user_p		varchar2,
		update_user_p		varchar2,
		cancel_user_p		varchar2 default null)
 		return varchar2 deterministic is

ds_login_w		intpd_sistemas.ds_login%type;
ds_retorno_w		varchar2(15);
ie_geracao_w		intpd_fila_transmissao.ie_geracao%type;
nm_usuario_w		varchar2(255);

pragma autonomous_transaction;
begin
begin
select	replace(upper(c.ds_login),'_',null),
	ie_geracao
into	ds_login_w,
	ie_geracao_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b,
	intpd_sistemas c
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	b.nr_seq_sistema = c.nr_sequencia
and	a.nr_sequencia = nr_seq_fila_p;
exception
when others then
	ds_login_w	:=	null;
end;

if	(ie_geracao_w = 'U') then
	ds_retorno_w	:=	'S';
else
	nm_usuario_w	:= upper(replace(nvl(nvl(cancel_user_p, update_user_p), creation_user_p),'_',null));
	
	if	(nm_usuario_w = ds_login_w) or
		(nm_usuario_w like 'TASY%') then
		intpd_gravar_log_recebimento(nr_seq_fila_p, 'No changes have been received.', 'TASY', null, 'W');
		
		ds_retorno_w	:=	'N';
		--ds_retorno_w	:=	'S';
	else	
		ds_retorno_w	:=	'S';
	end if;
end if;

return	ds_retorno_w;

commit;
end ish_obter_se_reg_alterado;
/