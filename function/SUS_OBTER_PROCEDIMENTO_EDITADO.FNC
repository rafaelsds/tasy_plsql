CREATE OR REPLACE
FUNCTION Sus_Obter_Procedimento_Editado(cd_procedimento_p	number)
					RETURN VarChar2 IS

ds_campo_w    		Varchar2(20);
cd_procedimento_w	Varchar2(15);
BEGIN
if	(cd_procedimento_p is not null) then
	cd_procedimento_w	:=	lpad(cd_procedimento_p,10,0);
	
	ds_campo_w		:= 	substr(cd_procedimento_w,1,2) || '.' ||
				   	substr(cd_procedimento_w,3,2) || '.' ||
					substr(cd_procedimento_w,5,2) || '.' ||
					substr(cd_procedimento_w,7,3) || '-' ||
					substr(cd_procedimento_w,10,1);

end if;
return ds_campo_w;

END Sus_Obter_Procedimento_Editado;
/