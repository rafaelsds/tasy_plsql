create or replace
function Lab_obter_amostra_prescr_int	(ds_valor_p 	varchar2,
					nr_prescricao_p number,
					nr_seq_exame_p	number,
					nr_seq_prescr_p	number)
 		    	return varchar2 is
			
ie_existe_tasylab_w	varchar2(1) := 'N';
cd_barras_w		varchar2(255);

begin

begin
select	'S'
into	ie_existe_tasylab_w
from	lab_tasylab_cli_prescr a
where	a.nr_prescricao = nr_prescricao_p;
exception
	when others then
	ie_existe_tasylab_w	:= 'N';
	end;
	
if (ie_existe_tasylab_w = 'S') then
	select	max(b.cd_barras)
	into	cd_barras_w
	from  	prescr_proc_mat_item a, 
		prescr_proc_material b
	where	a.nr_prescricao = b.nr_prescricao
	and	a.nr_seq_prescr_proc_mat = b.nr_sequencia
	and	a.nr_prescricao = nr_prescricao_p
	and	a.nr_seq_prescr = nr_seq_prescr_p;	
end if;

if (cd_barras_w is null) then
	cd_barras_w	:= Lab_obter_amostra_lablink_v2(ds_valor_p, nr_prescricao_p, nr_seq_exame_p, nr_seq_prescr_p);
end if;

return	cd_barras_w;

end Lab_obter_amostra_prescr_int;
/