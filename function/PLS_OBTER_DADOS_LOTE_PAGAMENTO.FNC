create or replace
function pls_obter_dados_lote_pagamento(nr_seq_lote_pagamento_p		pls_lote_pagamento.nr_sequencia%type,
					ie_opcao_p			varchar2) return varchar2 is
/* 
ie_opcao_p
	DN	- Data de emiss�o da nota fiscal
	ON	- Opera��o da nota fiscal
	NN	- Natureza da nota fiscal
	VL	- Valor lote l�quido
	VB	- Valor bruto tr�s o valor bruto antes de qualquer desconto
	VLP	- Para retornar os valores do PLS_PAGAMENTO_PRESTADOR.VL_PAGAMENTO com valor positivos.
	VLR	- Para retornar os valores do PLS_PAGAMENTO_PRESTADOR.VL_PAGAMENTO com valor negativos.
	VLB	- Valor bruto lote, neste campo consideram apenas os eventos de provento menos os descontos
*/

ds_retorno_w		varchar2(255);
nr_seq_nota_w		number(10);
dt_emissao_w		date;
cd_operacao_nf_w	number(4);
cd_natureza_operacao_w	number(4);

begin

select	max(nr_sequencia)
into	nr_seq_nota_w
from	nota_fiscal
where	nr_seq_lote_res_pls = nr_seq_lote_pagamento_p;

if	(nr_seq_nota_w is not null) then

	select	dt_emissao,
		cd_operacao_nf,
		cd_natureza_operacao
	into	dt_emissao_w,
		cd_operacao_nf_w,
		cd_natureza_operacao_w
	from	nota_fiscal
	where	nr_sequencia = nr_seq_nota_w;

	if	(ie_opcao_p = 'DN') then

		ds_retorno_w	:= dt_emissao_w;

	elsif	(ie_opcao_p = 'ON') and

		(nvl(cd_operacao_nf_w,0) <> 0) then
		select	ds_operacao_nf
		into	ds_retorno_w
		from	operacao_nota
		where	cd_operacao_nf	= cd_operacao_nf_w;

	elsif	(ie_opcao_p = 'NN') and

		(nvl(cd_natureza_operacao_w,0) <> 0) then
		select	ds_natureza_operacao
		into	ds_retorno_w
		from	natureza_operacao
		where	cd_natureza_operacao = cd_natureza_operacao_w;
	end if;

elsif	(ie_opcao_p = 'VL') then

	select	nvl(sum(a.vl_pagamento), 0)
	into	ds_retorno_w
	from	pls_pagamento_prestador a
	where	a.nr_seq_lote = nr_seq_lote_pagamento_p
	and	a.ie_cancelamento is null;

elsif	(ie_opcao_p = 'VLP') then

	select	nvl(sum(a.vl_pagamento), 0)
	into	ds_retorno_w
	from	pls_pagamento_prestador a
	where	nvl(a.vl_pagamento,0) > 0
	and	a.nr_seq_lote = nr_seq_lote_pagamento_p
	and	a.ie_cancelamento is null;

elsif	(ie_opcao_p = 'VLR') then

	select	nvl(sum(a.vl_pagamento), 0)
	into	ds_retorno_w
	from	pls_pagamento_prestador a
	where	nvl(a.vl_pagamento,0) < 0
	and	a.nr_seq_lote = nr_seq_lote_pagamento_p
	and	a.ie_cancelamento is null;
	
elsif	(ie_opcao_p = 'VB') then

	select	nvl(sum(b.vl_item), 0)
	into	ds_retorno_w
	from	pls_evento c,
		pls_pagamento_item b,
		pls_pagamento_prestador a
	where	a.nr_sequencia = b.nr_seq_pagamento
	and	c.nr_sequencia = b.nr_seq_evento
	and	c.ie_natureza = 'P'
	and	nvl(b.vl_item,0) > 0
	and	a.nr_seq_lote = nr_seq_lote_pagamento_p
	and 	a.ie_cancelamento is null;

elsif 	(ie_opcao_p = 'VLB') then

	select	nvl(sum(b.vl_item), 0)
	into	ds_retorno_w
	from	pls_pagamento_prestador a,
		pls_pagamento_item b
	where	a.nr_seq_lote = nr_seq_lote_pagamento_p
	and  	b.nr_seq_pagamento = a.nr_sequencia
	and 	a.ie_cancelamento is null;
end if;

return	ds_retorno_w;

end pls_obter_dados_lote_pagamento;
/