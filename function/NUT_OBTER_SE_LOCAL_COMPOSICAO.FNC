create or replace
function nut_obter_se_local_composicao(	nr_seq_local_p		number)
 		    	return varchar2 is

qt_valida_w	number(10);			
ds_retorno_w	varchar2(1);	
	
begin
ds_retorno_w := 'N';
if	(nr_seq_local_p is not null) then
	select	count(*)
	into	qt_valida_w
	from	nut_local_refeicao
	where	nr_sequencia = nr_seq_local_p
	and	nvl(ie_local_paciente,'N') = 'C';	
	
	if	(qt_valida_w > 0 ) then
		ds_retorno_w := 'S';	
	end if;
	
	
end if;	


return	 ds_retorno_w;

end nut_obter_se_local_composicao;
/