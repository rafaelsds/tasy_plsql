create or replace 
function Obter_data_hist_unidade(	nr_seq_unidade_p 		Number,
					ie_status_unidade_p 		Varchar2)
					return varchar2 is
					
					
dt_historico_w		Date;
ie_status_unidade_w	varchar2(1);
ds_retorno_w		varchar2(1000);
nr_Seq_interd_w		number(10);
	
begin

select	max(ie_status_unidade)
into	ie_status_unidade_w
from	unidade_atendimento
where	nr_seq_interno	= nr_seq_unidade_p;


if	(nr_seq_unidade_p is not null) and
	(ie_status_unidade_w = 'I') then

	select 	max(NR_SEQUENCIA)
	into	nr_Seq_interd_w
	from	unidade_atend_hist
	where	nr_seq_unidade		= nr_seq_unidade_p 
	and	ie_status_unidade	= ie_status_unidade_p;	
	
	
	select	max(dt_historico)
	into	dt_historico_w
	from	unidade_atend_hist
	where	nr_seq_unidade		= nr_seq_unidade_p 
	and	ie_status_unidade	= ie_status_unidade_p
	and	nr_sequencia		= nr_Seq_interd_w;
	
	ds_retorno_w := to_char(dt_historico_w,'dd/mm/yyyy hh24:mi:ss');
end if;

return ds_retorno_w;

end Obter_data_hist_unidade;
/
