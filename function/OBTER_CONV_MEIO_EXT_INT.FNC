create or replace
function obter_conv_meio_ext_int(	
				nm_tabela_p			varchar2,
				nm_atributo_p		varchar2,
				cd_origem_p			varchar2,
				IE_SISTEMA_EXTERNO_p	varchar2)
				return varchar2 is


cd_conversao_w			varchar2(80);
nm_atributo_w			varchar2(50);
nm_tabela_w				varchar2(50);

BEGIN

	cd_conversao_w	:= '';
	nm_atributo_w	:= upper(nm_atributo_p);
	nm_tabela_w		:= upper(nm_tabela_p);


	begin
		if (nvl(ie_sistema_externo_p, 'XPTO') = 'LXC') then
			select	max(cd_externo)
			into	cd_conversao_w
			from	conversao_meio_externo
			where	upper(nm_tabela)	= upper(nm_tabela_w)
			and		upper(nm_atributo)	= upper(nm_atributo_w)
			and		upper(cd_interno)	= upper(cd_origem_p)
			and		ie_sistema_externo	= ie_sistema_externo_p;
		else
			select	max(cd_externo)
			into	cd_conversao_w
			from	conversao_meio_externo
			where	nm_tabela			= nm_tabela_w
			and		nm_atributo			= nm_atributo_w
			and		cd_interno			= cd_origem_p
			and		ie_sistema_externo = ie_sistema_externo_p;
		end if;
	exception when others then
		cd_conversao_w := null;
	end;


	return cd_conversao_w;

end obter_conv_meio_ext_int;
/

