create or replace
function rxt_obter_autorizacao_proc(nr_seq_tipo_tratamento_p	number,
					nr_atendimento_p	number)
 		    	return varchar2 is

nr_seq_proc_interno_w	number(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
nr_seq_trat_proced_w	number(10);	
ie_autorizado_w		varchar2(1);	
nr_seq_estagio_w	number(10);	
ie_autor_interno_w	varchar2(2);
			
begin

if	((nvl(nr_seq_tipo_tratamento_p,0) > 0) and
	 (nvl(nr_atendimento_p,0) > 0)) then

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_trat_proced_w
	from	rxt_tipo_trat_proced
	where	nr_seq_tipo = nr_seq_tipo_tratamento_p;
	
	if	(nr_seq_trat_proced_w > 0) then

		select	nvl(max(nr_seq_proc_interno),0)
		into	nr_seq_proc_interno_w
		from	rxt_tipo_trat_proced
		where	nr_sequencia = nr_seq_trat_proced_w;
		
		if	(nr_seq_proc_interno_w > 0) then

			select	nvl(max(cd_procedimento),0)
			into	cd_procedimento_w
			from 	proc_interno
			where 	nr_sequencia = nr_seq_proc_interno_w; 
			
			if	(cd_procedimento_w > 0) then
			
				select	nvl(max(nr_seq_estagio),0)
				into	nr_seq_estagio_w
				from	autorizacao_convenio
				where	nr_atendimento = nr_atendimento_p
				and	cd_procedimento_principal = cd_procedimento_w;
				
				if	(nr_seq_estagio_w > 0) then
				
					select	ie_interno
					into	ie_autor_interno_w
					from	estagio_autorizacao
					where	nr_sequencia = nr_seq_estagio_w
					and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
					
					if	(ie_autor_interno_w is not null) then
					
						if	(ie_autor_interno_w = '10') then
							ie_autorizado_w := 'S';
						else	
							ie_autorizado_w := 'N';
						end if;
					end if;
				end if;
				
			end if;
		end if;
	end if;
end if;


return	ie_autorizado_w;

end rxt_obter_autorizacao_proc;
/