create or replace
function Obter_Dados_Classif_Espec	(nr_sequencia_p	number,
									ie_opcao_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);	
/*
  ie_opcao_p
  C = Color
  D = Description
*/			
begin

if	(nr_sequencia_p	is not null) then
	if	(ie_opcao_p	= 'C') then
		select	ds_cor
		into	ds_retorno_w
		from	CLASSIF_ESPECIAL_PACIENTE
		where	nr_sequencia	= nr_sequencia_p;
	elsif  (ie_opcao_p	= 'D') then
		select	ds_classificacao
		into	ds_retorno_w
		from	CLASSIF_ESPECIAL_PACIENTE
		where	nr_sequencia	= nr_sequencia_p;
	end if;
end if;
return	ds_retorno_w;

end Obter_Dados_Classif_Espec;
/