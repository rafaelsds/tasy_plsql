create or replace
function Agcons_obter_senha_aval_web(cd_pessoa_fisica_p	varchar2)
										return varchar2 is
ds_retorno_w	varchar2(15);
										
begin

select	max(ds_senha)
into	ds_retorno_w
from 	pessoa_fisica
where 	cd_pessoa_fisica = cd_pessoa_fisica_p
order by 1;

return	ds_retorno_w;

end Agcons_obter_senha_aval_web;
/