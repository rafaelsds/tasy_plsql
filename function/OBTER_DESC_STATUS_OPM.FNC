create or replace
function obter_desc_status_opm		(nr_seq_status_prescr_opm_p  number)
					return varchar2 is

ds_retorno_w	varchar2(100);

begin

if	(nvl(nr_seq_status_prescr_opm_p,0) > 0) then
	select	ds_status_opm
	into	ds_retorno_w
	from	status_prescr_opm
	where	nr_sequencia	=	nr_seq_status_prescr_opm_p
	and	nvl(ie_situacao,'A') = 'A';	
end if;

return	ds_retorno_w;


return	ds_retorno_w;

end obter_desc_status_opm;
/