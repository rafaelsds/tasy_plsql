create or replace
function OBTER_SE_CI_CLASSIF_USUARIO(	nm_usuario_p	varchar2,
					 nr_seq_classif_p	number) 
				return varchar2 is

ds_retorno_w	varchar2(255);
cont_w		number(10,0);

begin

select	count(*)
into	cont_w
from	comunic_int_classif_regra
where	nr_seq_classif	= nr_seq_classif_p;

ds_retorno_w	:= 'S';
if	(cont_w > 0) then
	select	count(*)
	into	cont_w
	from	comunic_int_classif_regra a
	where	a.nr_seq_classif	= nr_seq_classif_p
	and	exists	(select	1
			 from	usuario x
			 where	x.nm_usuario		= nm_usuario_p
			 and	x.cd_setor_atendimento	= a.cd_setor_atendimento
			 union
			 select	1
			 from	usuario_setor x
			 where	x.nm_usuario_param	= nm_usuario_p
			 and	x.cd_setor_atendimento	= a.cd_setor_atendimento
			 union
			 select	1
			 from	usuario_perfil x
			 where	x.nm_usuario		= nm_usuario_p
			 and	x.cd_perfil		= a.cd_perfil
			 union
			 select	1
			 from	dual
			 where	a.nm_usuario_lib		= nm_usuario_p);
	if	(cont_w = 0) then
		ds_retorno_w	:= 'N';
	end if;
end if;

return	ds_retorno_w;

end OBTER_SE_CI_CLASSIF_USUARIO;
/