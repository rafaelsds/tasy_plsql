create or replace
function pls_valida_atri_compl_web( nr_seq_conta_p    number,
                                    ds_atributo_p     varchar default 'TD' )
                 return Varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Verificar se no complemento existe algum regra de atributo, no momento que o usu�rio vai finalizar o complemento.
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 

PR - Procedimento
MT - Material
CD - Cid
TD - Todos
 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

*/            

qt_registros_w        Number(10);
ie_tipo_guia_w        Varchar2(1);    
        
begin

select  max(ie_tipo_guia)
into    ie_tipo_guia_w
from    pls_conta
where   nr_sequencia = nr_seq_conta_p;

-- procedimento
if  (ds_atributo_p = 'PR' or ds_atributo_p = 'TD') then

    select  count(1)
    into    qt_registros_w
    from    pls_restricao_atrib_web a ,
            pls_restricao_funcao_web b
    where   a.nr_seq_rest_funcao_web = b.nr_sequencia
    and     b.ie_funcionalidade = 3
    and     a.nr_seq_atributo_web =  17
    and     b.ie_tipo_guia = ie_tipo_guia_w
    and     a.ie_obrigatorio = 'S';

    if    (qt_registros_w > 0) then

        select  count(1)
        into    qt_registros_w
        from    pls_conta_proc
        where   nr_seq_conta = nr_seq_conta_p
        and     ie_status <> 'D'
        and     (vl_procedimento_imp is null
        or      vl_procedimento_imp = 0);
        
        if    (qt_registros_w > 0) then
            return 'Procedimentos sem valor apresentado!';
        end if;
        
    end if;

end if;

-- material
if  (ds_atributo_p = 'MT' or ds_atributo_p = 'TD') then

    select  count(1)
    into    qt_registros_w
    from    pls_restricao_atrib_web a ,
            pls_restricao_funcao_web b
    where   a.nr_seq_rest_funcao_web = b.nr_sequencia
    and     b.ie_funcionalidade = 3
    and     a.nr_seq_atributo_web =  18
    and     b.ie_tipo_guia = ie_tipo_guia_w
    and     a.ie_obrigatorio = 'S';

    if    (qt_registros_w > 0) then

        select  count(1)
        into    qt_registros_w
        from    pls_conta_mat
        where   nr_seq_conta = nr_seq_conta_p
        and     ie_status <> 'D'
        and     (vl_material_imp is null
        or      vl_material_imp = 0);
        
        if    (qt_registros_w > 0) then
            return 'Materiais sem valor apresentado!';
        end if;
        
    end if;

end if;

-- CID
if  (ds_atributo_p = 'CD' or ds_atributo_p = 'TD') then

    select  count(1)
    into    qt_registros_w
    from    pls_restricao_atrib_web a ,
            pls_restricao_funcao_web b
    where   a.nr_seq_rest_funcao_web = b.nr_sequencia
    and     b.ie_funcionalidade = 3
    and     a.nr_seq_atributo_web =  9
    and     b.ie_tipo_guia = ie_tipo_guia_w
    and     a.ie_obrigatorio = 'S';

    if    (qt_registros_w > 0) then

        select  count(1)
        into    qt_registros_w
        from    pls_diagnostico_conta
        where   nr_seq_conta = nr_seq_conta_p
        and     ie_classificacao = 'P';
        
        if    (qt_registros_w = 0) then
            return '� obrigat�rio informar CID de atendimento!';
        end if;
    end if;

end if;

return    'OK';

end pls_valida_atri_compl_web;
/