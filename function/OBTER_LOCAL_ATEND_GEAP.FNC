create or replace
function Obter_Local_Atend_Geap
			(nr_atendimento_p	number,
			cd_setor_item_p		number,
			nr_prescricao_p		number,
			nr_interno_conta_p	number,
			cd_cgc_convenio_p	varchar2)
			return number is

cd_local_atend_w		number(10,0);
cd_setor_prescr_w		number(10,0);
cd_classif_setor_w		varchar2(02);
nr_seq_atepacu_w		number(10,0);
cd_unidade_basica_w		varchar2(010);
cd_unidade_compl_w		varchar2(010);
cd_setor_atendimento_w		number(10,0);
cd_tipo_acomodacao_w		number(04,0);
ie_tipo_atendimento_w		number(3);
dt_prescricao_w			date;
ie_classificacao_w		varchar2(2);
ie_tipo_guia_w			varchar2(2);

begin

cd_local_atend_w	:= 0;

select	nvl(max(ie_tipo_atendimento),0)
into	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select	nvl(max(ie_tipo_guia),'X')
into	ie_tipo_guia_w
from	atend_categoria_convenio
where	nr_atendimento = nr_atendimento_p;

/* DAY CLINIC */

if	(ie_tipo_guia_w	= 'H') then
	cd_local_atend_w	:= 13;
end if;

/* Ambulatorio */

if	(ie_tipo_guia_w	= 'A') then
	cd_local_atend_w	:= 4;
end if;


/* Local de atendimento diferente de internados */

if	(ie_tipo_atendimento_w	<> 1) then
	cd_local_atend_w	:= 4;
end if;



/* Local de atendimento de internados */
if	(ie_tipo_atendimento_w	= 1) and
	(cd_local_atend_w = 0) then
	BEGIN

/*	if	(nvl(nr_prescricao_p,0) > 0) then
		select	nvl(max(cd_setor_atendimento),cd_setor_item_p),
			nvl(max(dt_prescricao),sysdate)
		into	cd_setor_prescr_w,
			dt_prescricao_w
		from	prescr_medica
		where	nr_prescricao		= nr_prescricao_p;

		select	cd_classif_setor
		into	cd_classif_setor_w
		from	setor_atendimento
		where	cd_setor_atendimento	= cd_setor_prescr_w;
	else*/
		select	cd_classif_setor
		into	cd_classif_setor_w
		from	setor_atendimento
		where	cd_setor_atendimento	= cd_setor_item_p;
/*	end if;*/


	/* LOcais Fixos */
	if	(cd_setor_item_p	= 90) or
		(cd_setor_item_p	= 111) then	
		cd_local_atend_w	:= 99;
	end if;
	if	(cd_classif_setor_w	= '4') then
		cd_local_atend_w	:= 9;	
	end if;

	if	(cd_classif_setor_w	= '3') then
		cd_local_atend_w	:= 8;
	end if;

	if	(cd_local_atend_w	= 0) then
		BEGIN
		nr_seq_atepacu_w := 0;
		select	nvl(max(a.nr_seq_interno),0)
		into	nr_seq_atepacu_w
		from 	setor_atendimento y,
			atend_paciente_unidade a
		where	a.nr_atendimento 		= nr_atendimento_p
		and	a.dt_entrada_unidade		<= nvl(dt_prescricao_w,a.dt_entrada_unidade)
		and	nvl(a.dt_saida_unidade,sysdate)	>= nvl(dt_prescricao_w,sysdate)
		and	a.cd_setor_atendimento		= y.cd_setor_atendimento
		and	y.cd_classif_setor		in(3);

		if	(nr_seq_atepacu_w	= 0) then
			begin
			select	nvl(max(a.nr_seq_interno),0)
			into	nr_seq_atepacu_w
			from 	setor_atendimento y,
				atend_paciente_unidade a
			where	a.nr_atendimento 		= nr_atendimento_p
			and	a.cd_setor_atendimento		= y.cd_setor_atendimento
			and	y.cd_classif_setor		in(3)
			and	exists
				(select	1
				from	material_atend_paciente x
				where	x.nr_seq_atepacu	= a.nr_seq_interno
				and	x.nr_interno_conta	= nr_interno_conta_p
				union
				select	1
				from	procedimento_paciente x
				where	x.nr_seq_atepacu	= a.nr_seq_interno
				and	x.nr_interno_conta	= nr_interno_conta_p);
			end;
		end if;


		if	(nr_seq_atepacu_w > 0) then
			begin
			select 	a.cd_unidade_basica,
				a.cd_unidade_compl,
				a.cd_setor_atendimento,
				d.cd_classif_setor,
				c.ie_classificacao
			into	cd_unidade_basica_w,	
				cd_unidade_compl_w,
				cd_setor_atendimento_w,
				cd_classif_setor_w,
				ie_classificacao_w
			from	setor_atendimento d,
				tipo_acomodacao c,
				unidade_atendimento b,
				atend_paciente_unidade a
			where	a.nr_seq_interno = nr_seq_atepacu_w
			and	a.cd_unidade_basica	= b.cd_unidade_basica
			and	a.cd_unidade_compl	= b.cd_unidade_compl
			and     a.cd_setor_atendimento  = b.cd_setor_atendimento
			and	b.cd_tipo_acomodacao	= c.cd_tipo_acomodacao
			and	a.cd_setor_atendimento	= d.cd_setor_atendimento;
			end;
		end if;

		if	(ie_classificacao_w	= '1') then
			cd_local_atend_w	:= 5;
		elsif	(ie_classificacao_w	= '3') then
			cd_local_atend_w	:= 8;
		elsif	(ie_classificacao_w	= '5') then
			cd_local_atend_w	:= 8;
		else
			begin

			select	nvl(max(8),0)
			into	cd_local_atend_w
			from	atend_paciente_unidade
			where	nr_atendimento	= nr_atendimento_p
			and	cd_setor_atendimento	= 117;

			end;
		end if;
 
		if	(cd_local_atend_w	= 0) 	and
			(cd_classif_setor_w	= '3') 	then
			cd_local_atend_w	:= 8;
		end if;
		END;
	end if;
	END;
end if;

return	cd_local_atend_w;

end Obter_Local_Atend_Geap; 
/
