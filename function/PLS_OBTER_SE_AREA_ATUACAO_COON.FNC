create or replace
function pls_obter_se_area_atuacao_coon(	nr_seq_congenere_p		number,
						nr_seq_cooperativa_area_p	number,
						ie_area_atuacao_p		varchar2,
						ds_congenere_p			varchar2)
 		    	return varchar2 is

ie_retorno_w	varchar2(1)	:= 'N';
qt_registro_w	pls_integer;
begin

if (ie_area_atuacao_p = 'C') then--Cong�nere
	select	count(1)
	into	qt_registro_w
	from	pls_congenere 	a,
		pessoa_juridica b
	where	a.cd_cgc	= b.cd_cgc
	and	nr_sequencia 	= nr_seq_congenere_p
	and	b.ds_razao_social like '%'||upper(ds_congenere_p)||'%';
	if	(qt_registro_w > 0) then
		ie_retorno_w :=	'C';
	end if;
elsif (ie_area_atuacao_p = 'A') then--�rea de Atua��o
	select	count(1)
	into	qt_registro_w
	from	pls_cooperativa_area 	a,
		pls_regiao 		b,
		pls_regiao_local 	c
	where	a.nr_seq_regiao 	= b.nr_sequencia
	and	b.nr_sequencia 		= c.nr_seq_regiao
	and	a.nr_sequencia 		= nr_seq_cooperativa_area_p
	and	obter_desc_municipio_ibge(c.cd_municipio_ibge) like '%'||upper(ds_congenere_p)||'%';
	
	if (qt_registro_w > 0) then
		ie_retorno_w := 'A';
	else
		ie_retorno_w := 'N';
	end if;
end if;

return ie_retorno_w;

end pls_obter_se_area_atuacao_coon;
/