create or replace
function pls_obter_dados_recalculo (	nr_seq_conta_proc_p		pls_conta_proc.nr_sequencia%type,
					nr_seq_conta_mat_p		pls_conta_mat.nr_sequencia%type,
					ie_opcao_p			varchar2)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ie_opcao_p
VO - Valor Original
L  - N�mero do lote de rec�lculo
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ds_retorno_w			varchar2(15);
nr_seq_item_recalculo_w		pls_item_recalculo.nr_sequencia%type;


begin

if	(nr_seq_conta_proc_p is not null) then
	select	max(nr_sequencia)
	into	nr_seq_item_recalculo_w
	from	pls_item_recalculo
	where	nr_seq_procedimento = nr_seq_conta_proc_p;
else
	select	max(nr_sequencia)
	into	nr_seq_item_recalculo_w
	from	pls_item_recalculo
	where	nr_seq_material = nr_seq_conta_mat_p;
end if;

if	(nr_seq_item_recalculo_w is not null) then
	
	if	(ie_opcao_p = 'L') then
	
		select	max(nr_seq_lote)
		into	ds_retorno_w
		from	pls_conta_recalculo a,
			pls_item_recalculo b
		where	a.nr_sequencia = b.nr_seq_conta
		and	b.nr_sequencia = nr_seq_item_recalculo_w;
		
	elsif	(ie_opcao_p = 'VO') then
		
		select	max(vl_item_atual)
		into	ds_retorno_w
		from	pls_item_recalculo
		where	nr_sequencia = nr_seq_item_recalculo_w;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_dados_recalculo;
/
