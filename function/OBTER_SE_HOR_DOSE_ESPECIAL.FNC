create or replace
function obter_se_hor_dose_especial(	nr_seq_horario_p	number)
			return varchar2 is

ie_dose_especial_w	varchar2(1) := 'N';

begin

if	(nr_seq_horario_p is not null) then

	select	nvl(max('S'),'N')
	into	ie_dose_especial_w
	from	prescr_mat_hor w,
			prescr_material q
	where	rownum = 1
	and		q.nr_prescricao = w.nr_prescricao
	and		q.nr_sequencia = w.nr_seq_material
	and		(((q.qt_dose_especial = w.qt_dose) and
			  (q.hr_dose_especial = to_char(w.dt_horario,'hh24:mi'))) or
			 (nvl(w.ie_dose_especial,'N') = 'S'))
	and		w.nr_sequencia = nr_seq_horario_p;

end if;

return ie_dose_especial_w;

end obter_se_hor_dose_especial;
/