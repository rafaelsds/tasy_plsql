create or replace
FUNCTION Obter_Telefone_PF	(cd_pessoa_fisica_p	VARCHAR2,

				ie_tipo_telefone_p	NUMBER)

				RETURN VARCHAR2 IS



ds_telefone_w		VARCHAR2(255);



/*

Tipos

	1 - Residencial

	2 - Comercial

	7 - Bip/Pager

	8 - Comercial

	9 - Contato
	
	10 -Contato com DDD
	
	0 - Celular (cadastro pessoa f�sica) - Rafael em 30/10/2006 OS43519

	11- Fax
	
	12- DDD / Celular 
	
	13- DDD / Residencial

*/



BEGIN



IF	(ie_tipo_telefone_p = 1) THEN

	BEGIN

	SELECT	MAX(p.nr_telefone)

	INTO	ds_telefone_w

	FROM	compl_pessoa_fisica p

	WHERE	p.cd_pessoa_fisica = cd_pessoa_fisica_p

	AND	p.ie_tipo_complemento = 1;

	END;



ELSIF	(ie_tipo_telefone_p = 2) THEN

	BEGIN

	SELECT	MAX(p.nr_telefone)

	INTO	ds_telefone_w

	FROM	compl_pessoa_fisica p

	WHERE	p.cd_pessoa_fisica = cd_pessoa_fisica_p

	AND	p.ie_tipo_complemento = 2;

	END;

ELSIF	(ie_tipo_telefone_p = 4) THEN

	BEGIN

	SELECT	MAX(DECODE(p.NR_DDD_TELEFONE,NULL,'','('||p.NR_DDD_TELEFONE||') ')) || MAX(p.nr_telefone)

	INTO	ds_telefone_w

	FROM	compl_pessoa_fisica p

	WHERE	p.cd_pessoa_fisica = cd_pessoa_fisica_p

	AND	p.ie_tipo_complemento = 1;

	END;



ELSIF	(ie_tipo_telefone_p = 5) THEN

	BEGIN

	SELECT	MAX(DECODE(p.NR_DDD_TELEFONE,NULL,'','('||p.NR_DDD_TELEFONE||') ')) || MAX(p.nr_telefone)

	INTO	ds_telefone_w

	FROM	compl_pessoa_fisica p

	WHERE	p.cd_pessoa_fisica = cd_pessoa_fisica_p

	AND	p.ie_tipo_complemento = 2;

	END;

ELSIF	(ie_tipo_telefone_p = 8) THEN

	BEGIN

	SELECT	MAX(p.nr_ramal)

	INTO	ds_telefone_w

	FROM	compl_pessoa_fisica p

	WHERE	p.cd_pessoa_fisica = cd_pessoa_fisica_p

	AND	p.ie_tipo_complemento = 2;

	END;





ELSIF	(ie_tipo_telefone_p = 9) THEN

	BEGIN

	SELECT	MAX(nr_telefone_celular)

	INTO	ds_telefone_w

	FROM	pessoa_fisica

	WHERE	cd_pessoa_fisica = cd_pessoa_fisica_p;

	END;

ELSIF	(ie_tipo_telefone_p = 10) THEN

	BEGIN

	SELECT	MAX(DECODE(NR_DDD_CELULAR,NULL,'','('||NR_DDD_CELULAR||') ')) || MAX(NR_TELEFONE_CELULAR)

	INTO	ds_telefone_w

	FROM	pessoa_fisica

	WHERE	cd_pessoa_fisica = cd_pessoa_fisica_p;

	END;

ELSIF	(ie_tipo_telefone_p = 7) THEN

	BEGIN

	SELECT	MAX(nr_pager_bip)

	INTO	ds_telefone_w

	FROM	pessoa_fisica

	WHERE	cd_pessoa_fisica = cd_pessoa_fisica_p;

	END;



ELSIF	(ie_tipo_telefone_p = 0) THEN

	BEGIN

	SELECT	MAX(nr_telefone_celular)

	INTO	ds_telefone_w

	FROM	pessoa_fisica

	WHERE	cd_pessoa_fisica = cd_pessoa_fisica_p;

	END;



ELSIF	(ie_tipo_telefone_p = 11) THEN

	BEGIN

	SELECT	MAX(DECODE(p.NR_DDD_FAX,NULL,'','('||p.NR_DDD_FAX||')')) || MAX(p.ds_fax)

	INTO	ds_telefone_w

	FROM	compl_pessoa_fisica p

	WHERE	p.cd_pessoa_fisica = cd_pessoa_fisica_p

	AND	p.ie_tipo_complemento = 1;

	END;	


ELSIF	(ie_tipo_telefone_p = 12) THEN

	BEGIN

	SELECT	decode(MAX(NR_DDD_CELULAR), null, MAX(nr_telefone_celular), '(' || MAX(NR_DDD_CELULAR) || ') ' || MAX(nr_telefone_celular))

	INTO	ds_telefone_w

	FROM	pessoa_fisica

	WHERE	cd_pessoa_fisica = cd_pessoa_fisica_p;

	END;
	
	
ELSIF	(ie_tipo_telefone_p = 13) THEN

	BEGIN

	SELECT	decode(MAX(NR_DDD_TELEFONE), null, MAX(nr_telefone), '(' || MAX(NR_DDD_TELEFONE) || ')' || MAX(nr_telefone))

	INTO	ds_telefone_w

	FROM	compl_pessoa_fisica p

	WHERE	p.cd_pessoa_fisica = cd_pessoa_fisica_p

	AND	p.ie_tipo_complemento = 1;

	END;	



END IF;



RETURN ds_telefone_w;



END Obter_Telefone_PF;
/
