create or replace
function obter_se_cliente_usa_ios(	qt_dias_p		number) return varchar2 is

/*
qt_dias_p = Quantidade de dias retroativos que se deseja verificar a existencia de acessos.  Padrao 30 dias.
*/

ds_retorno_w	varchar2(1);

begin

select  nvl(max('S'),'N')
into	ds_retorno_w
from    dual
where   exists (        select  1
                        from    tasy_log_acesso
                        where   dt_acesso > sysdate - qt_dias_p
                        and     cd_aplicacao_tasy = 'iOS');
						
return	ds_retorno_w;						

end obter_se_cliente_usa_ios;
/