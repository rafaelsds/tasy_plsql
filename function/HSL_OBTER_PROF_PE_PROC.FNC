create or replace
function Hsl_Obter_Prof_pe_proc( nr_atendimento_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
nm_usuario_w		varchar2(255);
nm_usuario_adm_w	varchar2(255);
nm_usuario_susp_w	varchar2(255);
	
Cursor C01 is
	select	distinct substr(obter_nome_usuario(c.nm_usuario) || ' Coren: ' || obter_crm_medico(obter_pessoa_fisica_usuario(c.nm_usuario,'C')),1,100)
	from	pe_prescr_proc x,
		pe_prescr_proc_hor c,
		pe_prescricao a
	where	x.nr_seq_prescr = c.nr_seq_pe_prescr
	and	x.nr_sequencia = c.nr_seq_pe_proc
	and	x.nr_seq_prescr = a.nr_sequencia
	and	c.nr_seq_pe_prescr = a.nr_sequencia
	and	a.nr_atendimento = nr_atendimento_p;

Cursor C02 is
	select	distinct substr(obter_nome_usuario(c.nm_usuario_adm) || ' Coren: ' || obter_crm_medico(obter_pessoa_fisica_usuario(c.nm_usuario_adm,'C')),1,100)
	from	pe_prescr_proc x,
		pe_prescr_proc_hor c,
		pe_prescricao a
	where	x.nr_seq_prescr = c.nr_seq_pe_prescr
	and	x.nr_sequencia = c.nr_seq_pe_proc
	and	x.nr_seq_prescr = a.nr_sequencia
	and	c.nr_seq_pe_prescr = a.nr_sequencia
	and	a.nr_atendimento = nr_atendimento_p;
	
Cursor C03 is
	select	distinct substr(obter_nome_usuario(c.nm_usuario_susp) || ' Coren: ' || obter_crm_medico(obter_pessoa_fisica_usuario(c.nm_usuario_susp,'C')),1,100)
	from	pe_prescr_proc x,
		pe_prescr_proc_hor c,
		pe_prescricao a
	where	x.nr_seq_prescr = c.nr_seq_pe_prescr
	and	x.nr_sequencia = c.nr_seq_pe_proc
	and	x.nr_seq_prescr = a.nr_sequencia
	and	c.nr_seq_pe_prescr = a.nr_sequencia
	and	a.nr_atendimento = nr_atendimento_p;
			
begin

open C01;
loop
fetch C01 into	
	nm_usuario_w;
exit when C01%notfound;
	
	if 	(nm_usuario_w is not null) then
		ds_retorno_w := ds_retorno_w || nm_usuario_w || '; ';
	end if;
	
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nm_usuario_adm_w;
exit when C02%notfound;
	
	if 	(nm_usuario_adm_w is not null) and
		(nm_usuario_adm_w <> nm_usuario_w) and
		(nm_usuario_adm_w <> nm_usuario_susp_w) and
		(nm_usuario_adm_w <> ' Coren: ') then
		ds_retorno_w := ds_retorno_w || nm_usuario_adm_w || '; ';
	end if;
	
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nm_usuario_susp_w;
exit when C03%notfound;
	
	if 	(nm_usuario_susp_w is not null) and
		(nm_usuario_susp_w <> nm_usuario_adm_w) and
		(nm_usuario_susp_w <> nm_usuario_w) and
		(nm_usuario_susp_w <> ' Coren: ') then
		ds_retorno_w := ds_retorno_w || nm_usuario_susp_w || '; ';
	end if;
	
end loop;
close C03;

return	ds_retorno_w;

end Hsl_Obter_Prof_pe_proc;
/