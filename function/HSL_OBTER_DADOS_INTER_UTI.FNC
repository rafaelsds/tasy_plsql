create or replace
function hsl_obter_dados_inter_uti(nr_atendimento_p		number,
				   cd_setor_atendimento_p 	number,	
				   ie_opcao_p			varchar2)
 		    	return number is

qt_passagens_uti_w	number(10);
qt_internacao_w		number(10);	
qt_mov_mesmo_setor_w	number(10);
qt_mov_setor_w		number(10);
cd_classif_setor_w	varchar2(2);
qt_reinternacao_w	number(10);
qt_retorno		number(10);
nr_seq_interno_w	number(10);
qt_horas_reint_w	number(10);
nr_seq_interno_ww	number(10);
ie_tipo_internacao_w	number(10);
cd_classif_setor_atual_w	number(10);
ie_saida_setor_uti_w	number(10);
qt_saida_uti_w		number(10);
qt_pas_uti_w		number(10);	
qt_reint_w		number(10);	
cd_motivo_alta_w	number(3);
	
/* ie_opcao_p
	I interna��o
	M transf para mesmo setor
	D transf para setor diferente
	R reinterna��o na UTI
	TP tipo de reinternacao
	SU sa�da UTI
	P passagens UTI
	RI reinterna��es UTI
*/
pragma autonomous_transaction;
begin

ie_tipo_internacao_w := 0;

nr_seq_interno_w := obter_atepacu_paciente(nr_atendimento_p, 'A');

select	count(*)
into	qt_passagens_uti_w
from	atend_paciente_unidade a,
	setor_atendimento b
where	a.nr_atendimento = nr_atendimento_p
and	b.cd_setor_atendimento = a.cd_setor_atendimento
and	(b.cd_classif_setor = 4 or nvl(ie_epimed,'N') = 'S');

-- Verifica se � a primeira entrada do paciente no setor de UTI

select	count(*)
into	qt_internacao_w
from	atend_paciente_unidade a,
	setor_atendimento b
where	nr_atendimento = nr_atendimento_p
and	a.cd_setor_atendimento = b.cd_setor_atendimento
and	(b.cd_classif_setor = 4  or nvl(ie_epimed,'N') = 'S');

-- Verifica se a entrada atual foi para o mesmo setor 
select 	count(*)
into	qt_mov_mesmo_setor_w
from	atend_paciente_unidade
where	nr_seq_interno       = nr_seq_interno_w
and	nr_atendimento       = nr_atendimento_p
and	cd_setor_atendimento = cd_setor_atendimento_p;

-- Verifica se a entrada atual foi para outro setor de UTI 

select  max(cd_classif_setor)
into	cd_classif_setor_w
from    atend_paciente_unidade a,
        setor_atendimento b
where   a.cd_setor_atendimento = b.cd_setor_atendimento
and     a.nr_seq_interno =  nr_seq_interno_w;


select 	count(*)
into	qt_mov_setor_w
from	atend_paciente_unidade a,
	setor_atendimento b
where	a.nr_seq_interno = nr_seq_interno_w
and	a.cd_setor_atendimento = b.cd_setor_atendimento
and  	nr_atendimento = nr_atendimento_p
and	a.cd_setor_atendimento <> cd_setor_atendimento_p
and	(b.cd_classif_setor = 4 or nvl(ie_epimed,'N') = 'S');

-- Reinterna��o na UTI
qt_reinternacao_w := 0;

if	(cd_classif_setor_w <> 4) then

	select	count(*)
	into	qt_reinternacao_w
	from	atend_paciente_unidade a,
		setor_atendimento b
	where	a.cd_setor_atendimento = b.cd_setor_atendimento
	and   	a.nr_atendimento = nr_atendimento_p
	and	(b.cd_classif_setor = 4 or nvl(ie_epimed,'N') = 'S');
	
	if	(qt_reinternacao_w > 0) then
		
		select	max(nr_seq_interno)
		into	nr_seq_interno_ww
		from	atend_paciente_unidade a,
			setor_atendimento b
		where 	a.nr_atendimento = nr_atendimento_p
		and	b.cd_setor_atendimento = a.cd_setor_atendimento
		and	(b.cd_classif_setor = 4 or nvl(ie_epimed,'N') = 'S');
		
		select	(sysdate - dt_entrada_unidade)
		into	qt_horas_reint_w
		from	atend_paciente_unidade
		where	nr_atendimento = nr_atendimento_p
		and	nr_seq_interno = nr_seq_interno_ww;
		
		if	(qt_horas_reint_w > 24) then
			ie_tipo_internacao_w := 1;
		else
			ie_tipo_internacao_w := 2;
		end if;
	end if;
end if;

-- sa�da do setor de UTI
select	cd_classif_setor
into	cd_classif_setor_atual_w
from 	setor_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_p;


if	(cd_classif_setor_atual_w <> 4) then
	
	select  count(*)
	into	qt_saida_uti_w
	from    atend_paciente_unidade a,
		setor_atendimento b
	where   a.cd_setor_atendimento = b.cd_setor_atendimento
	and     a.nr_seq_interno =  nr_seq_interno_w
	and	(b.cd_classif_setor = 4 or nvl(ie_epimed,'N') = 'S');
	
end if; 

-- qt passagens UTI

select	count(nr_internacao) + 1
into	qt_pas_uti_w
from 	hsl_epimed_internacao_uti
where	nr_atendimento = nr_atendimento_p;

-- qt reinterna��es UTI

select	count(qt_passagem) + 1
into	qt_reint_w
from	hsl_epimed_internacao_uti
where	nr_atendimento = nr_atendimento_p;

if	(ie_opcao_p = 'I') then
	qt_retorno := qt_internacao_w;
elsif	(ie_opcao_p = 'M') then
	qt_retorno := qt_mov_mesmo_setor_w;
elsif	(ie_opcao_p = 'D') then
	qt_retorno := qt_mov_setor_w;
elsif	(ie_opcao_p = 'R') then
	qt_retorno := qt_reinternacao_w;
elsif	(ie_opcao_p = 'TR') then
	qt_retorno := ie_tipo_internacao_w;	
elsif	(ie_opcao_p = 'SU') then
	qt_retorno := qt_saida_uti_w;
elsif	(ie_opcao_p = 'P') then
	qt_retorno := qt_pas_uti_w;
elsif	(ie_opcao_p = 'RI') then
	qt_retorno := qt_reint_w;
end if;

return	qt_retorno;

end hsl_obter_dados_inter_uti;
/