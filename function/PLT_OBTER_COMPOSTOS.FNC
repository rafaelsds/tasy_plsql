create or replace
function PLT_obter_compostos(	nr_prescricao_p		number,
				nr_seq_item_p		number,
				nr_agrupamento_p	number,
				ie_tipo_item_p		varchar2)
				return varchar2 is
					
ie_medic_composto_w	varchar2(1);
ds_material_w		varchar2(255);
ds_retorno_w		varchar2(2000);
qt_dose_w		number(18,6);
cd_unidade_medida_dose_w varchar2(255);
qt_dose_ww		 varchar2(255);

cursor c01 is
select	substr(obter_desc_material(cd_material),1,80),
		qt_dose,
		cd_unidade_medida_dose
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and		nr_agrupamento	= nr_agrupamento_p
and		nr_sequencia	<> nr_seq_item_p
and		ie_agrupador	= 1
and		ie_tipo_item_p	= 'M'
and		cd_kit_material is null
and		nr_seq_substituto is null
union all
select	substr(obter_desc_material(cd_material),1,80),
		qt_dose,
		cd_unidade_medida_dose
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and		nr_sequencia_diluicao = nr_seq_item_p
and		nr_agrupamento	= nr_agrupamento_p
and		ie_agrupador	= 17
and		ie_tipo_item_p	= 'LD';

begin
if	(nr_agrupamento_p is not null) then
	
	if	(ie_tipo_item_p = 'LD') then
		ie_medic_composto_w := obter_se_leite_composto(nr_prescricao_p, nr_seq_item_p, nr_agrupamento_p);
	elsif	(ie_tipo_item_p = 'M') then
		ie_medic_composto_w := obter_se_medic_composto(nr_prescricao_p, nr_seq_item_p, nr_agrupamento_p);	
	end if;
	
	if	(ie_medic_composto_w = 'S') then
		open C01;
		loop
		fetch C01 into	
			ds_material_w,
			qt_dose_w,
			cd_unidade_medida_dose_w;
		exit when C01%notfound;
			qt_dose_ww	:= qt_dose_w;
			if	(qt_dose_w > 0) and
				(qt_dose_w < 1) then
				qt_dose_ww := '0'||qt_dose_w;
			end if;
			ds_retorno_w	:=  substr(ds_material_w || ' - ' || qt_dose_ww || ' ' || cd_unidade_medida_dose_w || chr(13) || chr(10) || ds_retorno_w,1,2000);
		end loop;
		close C01;
	end if;
	
end if;

return ds_retorno_w;

end plt_obter_compostos;
/