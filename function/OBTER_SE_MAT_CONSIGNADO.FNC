create or replace
function Obter_se_mat_Consignado
		(cd_material_p		number)	
		return varchar2 is

ie_consignado_w		varchar2(01);

begin

select	max(ie_consignado)
into	ie_consignado_w
from	material
where	cd_material	= cd_material_p;


return	ie_consignado_w;

end Obter_se_mat_Consignado;
/