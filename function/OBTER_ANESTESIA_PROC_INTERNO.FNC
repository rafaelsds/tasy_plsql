create or replace
function obter_anestesia_proc_interno(	nr_seq_proc_interno_p	number )
					return varchar2 is

cd_tipo_anestesia_w	varchar2(2);
					
begin	

if	(nr_seq_proc_interno_p > 0) then
	select	max(cd_tipo_anestesia)
	into	cd_tipo_anestesia_w
	from	proc_interno
	where	nr_sequencia	=	nr_seq_proc_interno_p;
end if;

return	cd_tipo_anestesia_w;

end obter_anestesia_proc_interno;
/