create or replace
function obter_hiperhidratacao ( cd_pessoa_fisica_p	varchar2 )
 		    	return number is
				
qt_hiperhidratacao_w		number(15,1);				

begin

select	nvl(max(qt_hiperhidratacao),0)
into	qt_hiperhidratacao_w
from	atend_bioimpedancia
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	ie_situacao = 'A'
and     dt_liberacao is not null
and     dt_atualizacao_nrec = (select max(dt_atualizacao_nrec) 
				from	atend_bioimpedancia  
				where	cd_pessoa_fisica = cd_pessoa_fisica_p
				and	ie_situacao = 'A'
				and     dt_liberacao is not null);

return	qt_hiperhidratacao_w;

end obter_hiperhidratacao;
/
