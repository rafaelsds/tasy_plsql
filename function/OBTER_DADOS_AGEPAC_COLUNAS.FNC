create or replace
function Obter_Dados_AgePac_Colunas(
			cd_agenda_p	Number,
			nm_usuario_p	Varchar2,
			ie_opcao_p	Varchar2)
 		    	return Number is

vl_Retorno_w	Number(10);
nr_height_w	Number(10);
nr_width_w	Number(10);
	
begin

select	nvl(max(nr_height),0),
	nvl(max(nr_width),0)
into	nr_height_w,
	nr_width_w
from	agepac_colunas_temp
where	nm_usuario_coluna	= nm_usuario_p
and	cd_agenda		= cd_agenda_p;

if	(ie_opcao_p	= 'W') then
	vl_Retorno_w	:= nr_width_w;
else
	vl_Retorno_w	:= nr_height_w;
end if;

return	vl_Retorno_w;

end Obter_Dados_AgePac_Colunas;
/