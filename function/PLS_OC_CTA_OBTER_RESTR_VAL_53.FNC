create or replace
function pls_oc_cta_obter_restr_val_53
		return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Montar a restri��o e atualizar os binds utilizados para montar o select din�mico
	da valida��o 53 - Validar duplicidade de itens.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:

-------------------------------------------------------------------------------------------------------------------
jjung OS 654073  - 13/02/2014 - Descontinua��o da function
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

-- ESTA FUNCTION FOI DESCONTINUADA POR N�O SER MAIS NECESS�RIA
return	null;

end pls_oc_cta_obter_restr_val_53;
/
