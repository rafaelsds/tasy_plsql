create or replace
function pls_obter_desc_regra_honorario
			(	nr_seq_regra_honorario_p	Number)
 		    		return Varchar2 is
				
ds_retorno_w			Varchar2(80);

begin

select	nvl(max(ds_regra),'')
into	ds_retorno_w
from	pls_regra_honorario
where	nr_sequencia	= nr_seq_regra_honorario_p;

return	ds_retorno_w;

end pls_obter_desc_regra_honorario;
/