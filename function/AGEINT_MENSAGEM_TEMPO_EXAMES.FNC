create or replace
function Ageint_Mensagem_Tempo_Exames(
					cd_agenda_p		Number,
					hr_Agenda_p		Date,
					nr_minuto_duracao_p	Number,
					nr_Seq_ageint_p		Number,
					nr_seq_ageint_item_p	Number,
					nm_usuario_p		Varchar2)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(2000);
nr_Seq_marcacao_w	Number(10);
hr_Agenda_w		Date;
nr_minuto_duracao_w	Number(10);
cd_Agenda_w		Number(10);
qt_tempo_sala_w		Number(10);
qt_sobra_w		Number(10);
hr_max_agenda_w		Date;
hr_min_agenda_w		Date;
ds_retorno_max_w	Varchar2(1)	:= 'S';
ds_Retorno_min_w	Varchar2(1)	:= 'S';
nr_Seq_ageint_item_w	Number(10);
nr_Seq_proc_interno_w	Number(10);
nr_Seq_proc_marc_w	Number(10);
qt_Tempo_Exame_w	Number(10);
nr_Seq_regra_w		Number(10);
cd_exame_w		Number(10);
qt_Tempo_w		Number(10);
ie_regra_exclusiva_w	ageint_tempo_entre_exames.ie_regra_exclusiva%type;
ie_tempo_exclusivo_w	ageint_tempo_entre_exames.ie_tempo_exclusivo%type;
cd_exame_pri_w		ageint_tempo_entre_exames.cd_exame_pri%type;
cd_exame_seg_w		ageint_tempo_entre_exames.cd_exame_seg%type;
ds_hora_minuto_w	varchar2(60);

Cursor C01 is
	select	nr_Sequencia,
		nvl(ie_regra_exclusiva,'N'),
		nvl(ie_tempo_exclusivo,'N'),
		cd_exame_pri,
		cd_exame_seg
	from	ageint_tempo_entre_exames
	where	((cd_exame_pri	= nr_Seq_proc_interno_w)
	or	(cd_exame_seg	= nr_seq_proc_interno_w))
	and	ie_situacao	= 'A';
	
Cursor C02 is
	select	a.hr_Agenda,
		a.nr_minuto_duracao
	from	agenda_integrada_item b,
		ageint_marcacao_usuario a
	where	a.nr_seq_Ageint		= b.nr_seq_agenda_int
	and	a.nr_Seq_Ageint_item	= b.nr_sequencia
	and	a.nm_usuario		= nm_usuario_p
	and	a.nr_Seq_ageint		= nr_seq_ageint_p
	and	b.nr_seq_proc_interno	= cd_exame_w;
	
begin

select	nr_Seq_proc_interno
into	nr_seq_proc_interno_w
from	agenda_integrada_item
where	nr_sequencia	= nr_Seq_Ageint_item_p;

open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	ie_regra_exclusiva_w,
	ie_tempo_exclusivo_w,
	cd_exame_pri_w,
	cd_exame_seg_w;
exit when C01%notfound;
	begin
	select	decode(cd_Exame_pri, nr_Seq_proc_interno_w, cd_exame_Seg, cd_exame_pri),
		qt_tempo
	into	cd_exame_w,
		qt_tempo_w
	from	ageint_tempo_entre_exames
	where	nr_sequencia	= nr_seq_regra_w;
	
	open C02;
	loop
	fetch C02 into	
		hr_Agenda_w,
		nr_minuto_duracao_w;
	exit when C02%notfound;
		begin
		if (ie_regra_exclusiva_w = 'N') or 
			(cd_exame_pri_w = nr_seq_proc_interno_w and hr_agenda_w >= hr_agenda_p) or
			(cd_exame_seg_w = nr_seq_proc_interno_w and hr_agenda_w <= hr_agenda_p) then
			if	(ds_retorno_w	is null) then
				if	(hr_Agenda_w	< hr_Agenda_p) then
					select	Obter_Min_Entre_Datas((hr_Agenda_w + nr_minuto_duracao_w / 1440), hr_Agenda_p, 1)
					into	qt_sobra_w
					from	dual;		
				else
					select	Obter_Min_Entre_Datas((hr_Agenda_p + nr_minuto_duracao_p / 1440), hr_Agenda_w, 1)
					into	qt_sobra_w
					from	dual;		
				end if;
				
				if	(qt_sobra_w	< qt_tempo_w) or (((qt_sobra_w - qt_tempo_w) <> 0) and (ie_tempo_exclusivo_w = 'S')) then
					ds_hora_minuto_w	:= OBTER_HORAS_MINUTOS(qt_tempo_w);
					ds_retorno_w	:= wheb_mensagem_pck.get_texto(306944 ,	'QT_HORAS=' || substr(ds_hora_minuto_w,1,instr(ds_hora_minuto_w,':')-1) || ';' ||
																			'QT_MINUTOS=' || substr(ds_hora_minuto_w,instr(ds_hora_minuto_w,':')+1,2) || ';' || 
																			'NM_PROC=' || substr(OBTER_DESC_PROC_INTERNO(nr_Seq_proc_interno_w),1,90) || ';' ||
																			'NM_PROC_2=' || substr(OBTER_DESC_PROC_INTERNO(cd_exame_w),1,90)); 
									/*
										Intervalo entre procedimentos: #@QT_HORAS#@ horas #@QT_MINUTOS#@ minutos
										Procedimentos: #@NM_PROC#@ - #@NM_PROC_2#@
									*/		
				end if;
			end if;
		end if;
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

return	ds_retorno_w;

end Ageint_Mensagem_Tempo_Exames;
/
