create or replace
function obter_dados_grupo_sup_des(	nr_sequencia_p	number,
				ie_opcao_p	varchar2,
				ie_sup_des_p	varchar2)
 		    	return varchar2 is

/*S� tratei para grupo desenv, mas j� est� preparada para buscar as inf do suporte*/

ds_retorno_w			varchar2(255);
ds_grupo_w			varchar2(80);
nm_usuario_lider_grupo_w		varchar2(15);
ds_gerencia_w			varchar2(80);
cd_resp_gerencia_w		varchar2(10);

nm_gerente_w			varchar2(255);
nm_analista_grupo_w		varchar2(255);

/*ie_opcao_p
NA - nome analista grupo
NG - nome gerente
RG - Ramal do gerente
*/

begin

if	(nr_sequencia_p > 0) then
	begin
	if	(ie_sup_des_p = 'D') then
		select	a.ds_grupo,
			c.nm_usuario_grupo,
			b.ds_gerencia,
			b.cd_responsavel
		into	ds_grupo_w,
			nm_usuario_lider_grupo_w,
			ds_gerencia_w,
			cd_resp_gerencia_w
		from	gerencia_wheb b,
			grupo_desenvolvimento a,
			usuario_grupo_des c
		where	a.nr_seq_gerencia(+) = b.nr_sequencia
		and	a.nr_sequencia = c.nr_seq_grupo
		and	c.ie_funcao_usuario = 'S'
		and	a.nr_sequencia = nr_sequencia_p
		and	rownum <2;
		
		if	(ie_opcao_p = 'NA') then
			select	ds_usuario
			into	ds_retorno_w
			from	usuario
			where	nm_usuario = nm_usuario_lider_grupo_w;
		elsif	(ie_opcao_p = 'NG') then
			select	substr(obter_nome_pf(cd_pessoa_fisica), 1, 255)
			into	ds_retorno_w
			from	pessoa_fisica
			where	cd_pessoa_fisica = cd_resp_gerencia_w;		
		elsif	(ie_opcao_p = 'RG') then
			select	max(nr_ramal)
			into	ds_retorno_w
			from	usuario
			where	cd_pessoa_fisica = cd_resp_gerencia_w;
		end if;
	end if;
	end;
end if;

return	ds_retorno_w;

end obter_dados_grupo_sup_des;
/
