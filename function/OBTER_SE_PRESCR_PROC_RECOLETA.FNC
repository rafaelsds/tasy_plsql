create or replace
function obter_se_prescr_proc_recoleta(nr_prescricao_p		number)
 		    	return varchar2 is


ds_retorno_w	varchar2(1);
qt_recoleta_w	NUMBER(10);

begin

SELECT 	COUNT(*)
INTO	qt_recoleta_w
FROM	prescr_procedimento
WHERE	nr_prescricao = nr_prescricao_p
AND	nr_seq_recoleta is not null;

IF	(qt_recoleta_w > 0) THEN
	ds_retorno_w := 'S';
ELSE
	ds_retorno_w := 'N';
END IF;

return	ds_retorno_w;

end obter_se_prescr_proc_recoleta;
/