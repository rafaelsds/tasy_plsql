create or replace
function obter_express_adl(opcao_p  varchar)
        return varchar2 is

    ds_retorno_w            number;

begin
    if (opcao_p = 'I') then
        ds_retorno_w := 1009734;
    end if;

    if (opcao_p = 'O') then
        ds_retorno_w := 1013718;
    end if;
    
    if (opcao_p = 'P') then
        ds_retorno_w := 1048011;
    end if;
    
    if (opcao_p = 'T') then
        ds_retorno_w := 1048017;
    end if;

    return obter_desc_expressao(ds_retorno_w);
end;
/