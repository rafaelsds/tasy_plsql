create or replace
FUNCTION man_obter_dados_ordem_serv(	nr_seq_ordem_serv_p	NUMBER,
				ie_opcao_p		VARCHAR2)
				RETURN VARCHAR2 IS

/* ie_opcao_p
	DB - Dano breve
	S - Solicitante
	PA - Pessoa acordo
	DO - Data da ordem de servi�o
	DF - Data do fim do acordo
	DE - Data externa do acordo
	DI - Data interna do acordo
	DFP - Data de fim prevista
	DIP - Data de inicio prevista
	DFR - Data de fim real
	DIR - Data de inicio real
	DCD - Data de conclus�o desejada
	ST - Situa��o/Status Ordem
	DST - Descri��o da Situa��o/Status Ordem
	FU - Fun��o
	RNC
	DEO - Data de Entrega da Ordem (tabela: DESENV_ACORDO_OS)
	CC - Classifica��o da OS pelo cliente
	LOC - Localiza��o
	SLOC - Nr Seq Localiza��o da O.S.
	DS - Descri��o da Severidade
	DP - Descri��o da Prioridade
	CABC - Curva ABC
*/

ds_dano_breve_w			VARCHAR2(255);
ds_retorno_w			VARCHAR2(255);
nm_solicitante_w			VARCHAR2(200);
nm_pessoa_acordo_w		VARCHAR2(255);
dt_ordem_servico_w		DATE;
dt_fim_acordo_w			DATE;
dt_interna_acordo_w		DATE;
dt_externa_acordo_w		DATE;
dt_inicio_previsto_w		DATE;
dt_fim_previsto_w		DATE;
dt_inicio_real_w		DATE;
dt_fim_real_w			DATE;
dt_conclusao_desejada_w		DATE;
ie_status_ordem_w		VARCHAR2(01);
nr_seq_nao_conform_w		NUMBER(10,0);
cd_funcao_w			NUMBER(10,0);
ds_status_ordem_w		VARCHAR2(255);
dt_entrega_prev_w		DATE;
ie_classificacao_cliente_w	VARCHAR2(255);
ie_classificacao_w		VARCHAR2(1);
ds_localizacao_w		man_ordem_servico_v.ds_localizacao%type;
nr_seq_cliente_w		man_ordem_servico.nr_seq_cliente%type;
NR_SEQ_SEVERIDADE_w		man_ordem_servico.NR_SEQ_SEVERIDADE%type;
ie_prioridade_w			man_ordem_servico.ie_prioridade%type;
nr_seq_loc_w			man_ordem_servico.nr_seq_localizacao%TYPE;

BEGIN

SELECT	MAX(ds_dano_breve),
	MAX(nm_solicitante),
	MAX(nm_pessoa_acordo),
	MAX(dt_ordem_servico),
	MAX(dt_fim_acordo),
	MAX(dt_interna_acordo),
	MAX(dt_externa_acordo),
	MAX(dt_inicio_previsto),
	MAX(dt_fim_previsto),
	MAX(dt_inicio_real),
	MAX(dt_fim_real),
	MAX(dt_conclusao_desejada),
	MAX(ie_status_ordem),
	MAX(nr_seq_nao_conform),
	MAX(cd_funcao),
	MAX(ie_classificacao_cliente),
	max(ds_localizacao),
	max(nr_seq_cliente),
	max(nvl(NR_SEQ_SEVERIDADE_WHEB,NR_SEQ_SEVERIDADE)),
	max(ie_prioridade),
	max(nr_seq_localizacao),
	max(ie_classificacao)	
INTO	ds_dano_breve_w,
	nm_solicitante_w,
	nm_pessoa_acordo_w,
	dt_ordem_servico_w,
	dt_fim_acordo_w,
	dt_interna_acordo_w,
	dt_externa_acordo_w,
	dt_inicio_previsto_w,
	dt_fim_previsto_w,
	dt_inicio_real_w,
	dt_fim_real_w,
	dt_conclusao_desejada_w,
	ie_status_ordem_w,
	nr_seq_nao_conform_w,
	cd_funcao_w,
	ie_classificacao_cliente_w,
	ds_localizacao_w,
	nr_seq_cliente_w,
	NR_SEQ_SEVERIDADE_w,
	ie_prioridade_w,
	nr_seq_loc_w,
	ie_classificacao_w
FROM	man_ordem_servico_v
WHERE	nr_sequencia	= nr_seq_ordem_serv_p;


IF 	(ie_opcao_p	= 'DB') THEN
	ds_retorno_w	:= ds_dano_breve_w;
ELSIF 	(ie_opcao_p	= 'S') THEN
	ds_retorno_w	:= nm_solicitante_w;
ELSIF 	(ie_opcao_p	= 'PA') THEN
	ds_retorno_w	:= nm_pessoa_acordo_w;
ELSIF 	(ie_opcao_p	= 'DO') THEN
	ds_retorno_w	:= TO_CHAR(dt_ordem_servico_w,'dd/mm/yyyy hh24:mi:ss');
ELSIF 	(ie_opcao_p	= 'DF') THEN
	ds_retorno_w	:= dt_fim_acordo_w;
ELSIF 	(ie_opcao_p	= 'DE') THEN
	ds_retorno_w	:= dt_externa_acordo_w;
ELSIF 	(ie_opcao_p	= 'DI') THEN
	ds_retorno_w	:= dt_interna_acordo_w;
ELSIF 	(ie_opcao_p	= 'DFP') THEN
	ds_retorno_w	:= TO_CHAR(dt_fim_previsto_w,'dd/mm/yyyy hh24:mi:ss');
ELSIF 	(ie_opcao_p	= 'DIP') THEN
	ds_retorno_w	:= TO_CHAR(dt_inicio_previsto_w,'dd/mm/yyyy hh24:mi:ss');
ELSIF 	(ie_opcao_p	= 'DFR') THEN
	ds_retorno_w	:= TO_CHAR(dt_fim_real_w,'dd/mm/yyyy hh24:mi:ss');
ELSIF 	(ie_opcao_p	= 'DIR') THEN
	ds_retorno_w	:= TO_CHAR(dt_inicio_real_w,'dd/mm/yyyy hh24:mi:ss');
ELSIF 	(ie_opcao_p	= 'DCD') THEN
	ds_retorno_w	:= TO_CHAR(dt_conclusao_desejada_w,'dd/mm/yyyy hh24:mi:ss');
ELSIF 	(ie_opcao_p	= 'ST') THEN
	ds_retorno_w	:= ie_status_ordem_w;
ELSIF 	(ie_opcao_p	= 'CL') THEN
	ds_retorno_w	:= ie_classificacao_w;	
ELSIF 	(ie_opcao_p	= 'RNC') THEN
	ds_retorno_w	:= nr_seq_nao_conform_w;
ELSIF   (ie_opcao_p	= 'FU') THEN
	ds_retorno_w	:= OBTER_DESC_FUNCAO(NVL(cd_funcao_w,0));
ELSIF	(ie_opcao_p	= 'DST') THEN
	ds_retorno_w	:= SUBSTR(obter_valor_dominio(1279,ie_status_ordem_w),1,255);
ELSIF	(ie_opcao_p	= 'DEO') THEN
	SELECT	MAX(a.dt_entrega_prev)
	INTO	dt_entrega_prev_w
	FROM	desenv_acordo_os a
	WHERE	nr_seq_ordem_servico = nr_seq_ordem_serv_p;

	ds_retorno_w	:= TO_CHAR(dt_entrega_prev_w,'dd/mm/yyyy');
ELSIF	(ie_opcao_p	= 'CC') THEN
	ds_retorno_w	:= ie_classificacao_cliente_w;
elsif	(ie_opcao_p	= 'LOC') then
	ds_retorno_w	:= ds_localizacao_w;
ELSIF   (ie_opcao_p     = 'SLOC') THEN
	ds_retorno_w	:= nr_seq_loc_w;
elsif	(ie_opcao_p	= 'DS') and (NR_SEQ_SEVERIDADE_w is not null) then
	select	max(ds_severidade)
	into	ds_retorno_w
	from	man_severidade
	where	nr_sequencia = nr_seq_severidade_w;
elsif	(ie_opcao_p	= 'DP') and (ie_prioridade_w is not null) then
	select	max(ds_valor_dominio)
	into	ds_retorno_w
	from	valor_dominio
	where	cd_dominio = 1046
	  and	vl_dominio = ie_prioridade_w; 
elsif	(ie_opcao_p	= 'CABC') and (nr_seq_cliente_w is not null) then
	select	substr(nvl(man_obter_curva_abc(nr_seq_cliente_w),'C'),1,1)
	into	ds_retorno_w
	from dual;
END IF;

RETURN	ds_retorno_w;

END man_obter_dados_ordem_serv;
/