create or replace
function Obter_se_regra_evento_sae(	nr_seq_resultado_p	Number,
									nr_seq_item_examinar_p Number,
									nr_seq_evento_p		Number) 
						return varchar2 is

ds_retorno_w			number(2) := 'N';
qt_reg_w				number;
cd_estabelecimento_w	number(4,0);
cd_perfil_w				number(10);

begin

cd_estabelecimento_w		:= nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
cd_perfil_w					:= nvl(obter_perfil_ativo,0);

select 	count(*)
into	qt_reg_w
from	regra_eventos_result_sae
where	nvl(cd_estabelecimento,cd_estabelecimento_w)	= cd_estabelecimento_w
and		nvl(cd_perfil,cd_perfil_w)	= cd_perfil_w
and		nr_seq_resultado = nr_seq_resultado_p
and		nr_seq_item_examinar = nr_seq_item_examinar_p
and		nr_seq_evento = nr_seq_evento_p ;

if	(qt_reg_w > 0 ) then
	ds_retorno_w := 'S';
end if;		

return ds_retorno_w;

end Obter_se_regra_evento_sae;
/