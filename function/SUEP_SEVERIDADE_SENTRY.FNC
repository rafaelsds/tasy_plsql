create or replace
function suep_severidade_sentry (	ie_severidade_p		in		number,
									ie_tipo_info_p	in		varchar2)
									return varchar2 is

ds_return_w 	varchar2(50 char);

begin

if (ie_tipo_info_p is not null) then

	if (ie_tipo_info_p = 'C') then

		CASE
			WHEN ie_severidade_p < 4	THEN ds_return_w := 'thirdCell:#00bd00';
			WHEN ie_severidade_p = 4	THEN ds_return_w := 'thirdCell:#a1e600';
			WHEN ie_severidade_p = 5	THEN ds_return_w := 'thirdCell:#cdf200';
			WHEN ie_severidade_p = 6	THEN ds_return_w := 'thirdCell:#ffff00';
			WHEN ie_severidade_p = 7	THEN ds_return_w := 'thirdCell:#ffed00';
			WHEN ie_severidade_p = 8	THEN ds_return_w := 'thirdCell:#ffc700';
			WHEN ie_severidade_p = 9	THEN ds_return_w := 'thirdCell:#ffba00';
			WHEN ie_severidade_p = 10	THEN ds_return_w := 'thirdCell:#ffae00';
			WHEN ie_severidade_p = 11	THEN ds_return_w := 'thirdCell:#ffa100';
			WHEN ie_severidade_p = 12	THEN ds_return_w := 'thirdCell:#ff9500';
			WHEN ie_severidade_p = 13	THEN ds_return_w := 'thirdCell:#ff8800';
			WHEN ie_severidade_p = 14	THEN ds_return_w := 'thirdCell:#ff7c00';
			WHEN ie_severidade_p = 15	THEN ds_return_w := 'thirdCell:#ff6f00';
			WHEN ie_severidade_p = 16	THEN ds_return_w := 'thirdCell:#ff6300';
			WHEN ie_severidade_p = 17	THEN ds_return_w := 'thirdCell:#ff5700';
			WHEN ie_severidade_p = 18	THEN ds_return_w := 'thirdCell:#ff4400';
			WHEN ie_severidade_p = 19	THEN ds_return_w := 'thirdCell:#ff3700';
			WHEN ie_severidade_p = 20	THEN ds_return_w := 'thirdCell:#ff2b00';
			WHEN ie_severidade_p = 21	THEN ds_return_w := 'thirdCell:#ff1f00';
			WHEN ie_severidade_p = 22	THEN ds_return_w := 'thirdCell:#ff0c00';
			WHEN ie_severidade_p >= 23	THEN ds_return_w := 'thirdCell:#ff0000';
			ELSE ds_return_w := null;
		END CASE;

	elsif (ie_tipo_info_p = 'T') then
	
		CASE
			WHEN  ie_severidade_p < 4 THEN ds_return_w := obter_desc_expressao_idioma(860062,null,wheb_usuario_pck.get_nr_seq_idioma);
			WHEN  ie_severidade_p >= 4 and ie_severidade_p < 23 THEN ds_return_w := obter_desc_expressao_idioma(293044,null,wheb_usuario_pck.get_nr_seq_idioma);
			WHEN  ie_severidade_p >= 23 THEN ds_return_w := obter_desc_expressao_idioma(724031,null,wheb_usuario_pck.get_nr_seq_idioma);
			ELSE  ds_return_w := null;
		END CASE;
	end if;

end if;

return ds_return_w;

end suep_severidade_sentry;
/
