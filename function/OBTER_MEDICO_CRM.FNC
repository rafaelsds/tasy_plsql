create or replace
function Obter_Medico_CRM(nr_crm_p		varchar2)
 		    	return varchar2 is
cd_pessoa_fisica_w 	varchar2(10);
begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	medico
where	nr_crm			= nr_crm_p;

return	cd_pessoa_fisica_w;

end Obter_Medico_CRM;
/
