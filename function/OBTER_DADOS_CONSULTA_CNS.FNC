create or replace
function obter_dados_consulta_cns(	nr_seq_solicitacao_p	number,
					cd_estabelecimento_p	number,
					ie_tipo_p		varchar2)
 		    	return varchar2 is
/*
     C - CPF
     N - Nome Completo
     NM - Nome Mae
		 DN - Data nascimento
*/

cd_pessoa_fisica_w		pls_solicitacao_cns.cd_pessoa_fisica%type;
ds_retorno_w			varchar2(255) := '';

begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	pls_solicitacao_cns
where	nr_sequencia		= nr_seq_solicitacao_p
and	cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_tipo_p = 'C') then
	begin
	select	nr_cpf
	into	ds_retorno_w
	from	pessoa_fisica
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_w;
	exception
	when others then
		ds_retorno_w 	:= '';
	end;	
elsif	(ie_tipo_p = 'N') then
	begin
	select	nm_pessoa_fisica
	into	ds_retorno_w
	from	pessoa_fisica
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_w;
	exception
	when others then
		ds_retorno_w 	:= '';
	end;
elsif	(ie_tipo_p = 'NM') then
	begin
	select    max(nm_contato)
	into	  ds_retorno_w
	from      compl_pessoa_fisica
	where     cd_pessoa_fisica 	= cd_pessoa_fisica_w
	and       ie_tipo_complemento 	= 5;
	exception
	when others then
		ds_retorno_w 	:= '';
	end;
elsif (ie_tipo_p = 'DN') then
	begin
	select	to_char(dt_nascimento, 'yyyy-MM-dd')
	into	ds_retorno_w
	from	pessoa_fisica
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_w;
	exception
	when others then
		ds_retorno_w := '';
	end;
end if;

return	ds_retorno_w;

end obter_dados_consulta_cns;
/
