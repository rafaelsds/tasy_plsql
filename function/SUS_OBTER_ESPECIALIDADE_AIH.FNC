create or replace
function sus_obter_especialidade_aih(	cd_procedimento_p	Number,
					ie_origem_proced_p	Number,
					cd_pessoa_fisica_p	Varchar2,
					cd_estabelecimento_p	number)
 		    	return Number is

nr_seq_grupo_w		Number(10);
nr_seq_subgrupo_w	Number(10);
nr_seq_forma_org_w	Number(10);
qt_idade_pac_w		Number(5);
cd_especialidade_aih_w	Number(2);
ds_retorno_w		Number(2);
			
Cursor C01 is
	select	cd_especialidade_aih
	from	sus_regra_espec_aih_proc
	where	nvl(nr_seq_grupo, nr_seq_grupo_w)		= nr_seq_grupo_w
	and	nvl(nr_seq_subgrupo, nr_seq_subgrupo_w)		= nr_seq_subgrupo_w
	and	nvl(nr_seq_forma_org, nr_seq_forma_org_w)	= nr_seq_forma_org_w
	and	nvl(cd_procedimento, cd_procedimento_p)		= cd_procedimento_p
	and	nvl(ie_origem_proced, ie_origem_proced_p)	= ie_origem_proced_p
	and	nvl(cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
	and	qt_idade_pac_w	>= nvl(qt_idade_minima,0)
	and 	qt_idade_pac_w <= nvl(qt_idade_maxima,999)
        and     ie_situacao = 'A'
	order by nvl(cd_procedimento, 0),
		nvl(nr_seq_grupo, 0),
		nvl(nr_seq_subgrupo, 0),
        nvl(nr_seq_forma_org, 0);	
               

		
begin

qt_idade_pac_w	:= obter_idade_pf(cd_pessoa_fisica_p, sysdate, 'A');

begin
select	nr_seq_grupo,
	nr_seq_subgrupo,
	nr_seq_forma_org
into	nr_seq_grupo_w,
	nr_seq_subgrupo_w,
	nr_seq_forma_org_w
from	sus_estrutura_procedimento_v
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
exception
	when others then
	nr_seq_grupo_w		:= 0;
	nr_seq_subgrupo_w	:= 0;
	nr_seq_forma_org_w	:= 0;
end;

open C01;
loop
fetch C01 into	
	cd_especialidade_aih_w;
exit when C01%notfound;
	begin
	ds_retorno_w	:= cd_especialidade_aih_w;
	end;
end loop;
close C01;

return	nvl(ds_retorno_w,0);

end sus_obter_especialidade_aih;
/
