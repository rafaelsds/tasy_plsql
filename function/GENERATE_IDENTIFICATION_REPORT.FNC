create or replace function generate_identification_report
	return varchar2
is

	identification_code_w varchar2(250);
	random_count          number(2);
	identification_code_temp varchar2(250);

begin
	select IDENTIFICATION_REPORT_SEQ.nextval into identification_code_temp from dual;
    select concat(concat(to_char(sysdate,'YYYYMMDD' ),'-'),identification_code_temp)  into identification_code_w from dual;

	while 1 = 1

	loop

		select  COUNT(*)
			into random_count
			from PACIENTE_MEDIC_USO
			where CD_IDENTIFICATION_REPORT = identification_code_w;

		exit when random_count = 0;

		if random_count                 > 0 then

			select IDENTIFICATION_REPORT_SEQ.nextval into identification_code_temp from dual;
			select concat(concat(to_char(sysdate,'YYYYMMDD' ),'-'),identification_code_temp)  into identification_code_w from dual;

		end if;

	end loop;

return identification_code_w;

end generate_identification_report;
/
