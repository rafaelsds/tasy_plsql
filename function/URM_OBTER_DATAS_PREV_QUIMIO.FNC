create or replace
function URM_obter_datas_prev_quimio(cd_pessoa_fisica_p number,
									 dt_inicial_p		date,
									 nr_ciclo_p 		number)
 		    	return varchar is
				
ds_retorno_w		varchar2(4000);
ds_retorno_aux_w	varchar2(40);


cursor c01 is
	select	nvl(a.dt_real, a.dt_prevista)
	from	paciente_atendimento a
	where	a.nr_ciclo > nr_ciclo_p
	and		obter_dados_paciente_setor(a.nr_seq_paciente, 'C') = cd_pessoa_fisica_p
	and		((nvl(a.dt_real, a.dt_prevista) < 
									(select	min(nvl(b.dt_real, b.dt_prevista))
									from	paciente_atendimento b
									where	b.nr_ciclo = 1
									and		nvl(b.dt_real, b.dt_prevista) > dt_inicial_p
									and		obter_dados_paciente_setor(b.nr_seq_paciente, 'C') = cd_pessoa_fisica_p))or 
			 ((select	min(nvl(b.dt_real, b.dt_prevista))
			   from		paciente_atendimento b
			   where	b.nr_ciclo = 1
			   and		nvl(b.dt_real, b.dt_prevista) > dt_inicial_p
			   and		obter_dados_paciente_setor(b.nr_seq_paciente, 'C') = cd_pessoa_fisica_p)) is null)
	order by 1;


begin
		
	open c01;
	loop
	fetch c01 into
		ds_retorno_aux_w;
	exit when c01%notfound;
	
	ds_retorno_w := ds_retorno_w ||ds_retorno_aux_w||'; ';
	
	end loop;

return trim(' ' from ds_retorno_w);
end	URM_obter_datas_prev_quimio;
/