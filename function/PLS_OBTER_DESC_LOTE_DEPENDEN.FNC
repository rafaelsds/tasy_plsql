create or replace
function pls_obter_desc_lote_dependen
		(	nr_seq_lote_dependencia_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);			

begin

if	(nr_seq_lote_dependencia_p is not null) then
	select	ds_regra
	into	ds_retorno_w
	from	pls_lote_parentesco
	where	nr_sequencia	= nr_seq_lote_dependencia_p;
end if;	

return	ds_retorno_w;

end pls_obter_desc_lote_dependen;
/
