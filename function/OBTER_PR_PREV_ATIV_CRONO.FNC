create or replace function Obter_Pr_Prev_Ativ_Crono (
dt_inicio_crono_p date, --Data prevista de inicio do cronograma
dt_fim_crono_p    date, -- Data prevista de fim do cronograma
dt_inicio_prev_ativ_p date --Data prevista de inicio de uma atividade do cronograma

)

/*
	O objetivo desta function � retornar a porcentagem prevista de um cronograma em um determinado per�odo entre  dt_inicio_prev_ativ_p e dt_fim_prev_ativ_p.	
*/
return number is

pr_retorno_w      number(15,2);
pr_por_dia_w      number(15,2);
qt_dias_uteis_w   number(10);
qt_dias_trab_w    number(10);

begin
	
	select	OBTER_DIAS_UTEIS_PERIODO(dt_inicio_crono_p, dt_fim_crono_p, 1)
	into	qt_dias_uteis_w
	from	dual;

	select	dividir(100, qt_dias_uteis_w)
	into	pr_por_dia_w
	from 	dual;
	
	select	OBTER_DIAS_UTEIS_PERIODO(dt_inicio_crono_p, dt_inicio_prev_ativ_p, 1)
	into	qt_dias_trab_w
	from 	dual;
		
	pr_retorno_w	:= round((pr_por_dia_w * qt_dias_trab_w), 1);	
		
	if	(pr_retorno_w > 100) then
		pr_retorno_w	:= 100;	
	end if;	
	
return	pr_retorno_w;

end Obter_Pr_Prev_Ativ_Crono;
/