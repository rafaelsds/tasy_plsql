CREATE OR REPLACE
FUNCTION Obter_Desc_Carater_Int_Atend
	(	cd_carater_int_p	varchar2)
		return varchar2 is


ds_retorno_w	Varchar2(100);


BEGIN

select	nvl(max(ds_carater_internacao),'')
into	ds_retorno_w
from	sus_carater_internacao
where	cd_carater_internacao	= cd_carater_int_p;

return	ds_retorno_w;

END Obter_Desc_Carater_Int_Atend;
/