CREATE OR REPLACE FUNCTION obter_nr_copias_laudo_pac(nr_atendimento_p       NUMBER,	
													 nm_usuario_p		    VARCHAR,
													 cd_estabelecimento_p	NUMBER,
													 cd_setor_atendimento_p	NUMBER) 
				                                     RETURN NUMBER IS
			
vl_retorno_w		     NUMBER(5);
nr_copias_w		         NUMBER(5) := 0;
nr_seq_unid_int_w		 NUMBER(6);
cd_convenio_w		     NUMBER(5);
ie_tipo_atendimento_w	 NUMBER(3);
copias_laudo_w	 	     NUMBER(5);
copias_laudo_internado_w NUMBER(5);
		
BEGIN				
		
  IF (nr_atendimento_p IS NOT NULL) THEN
  BEGIN
    SELECT nr_seq_unid_int,
		   cd_convenio,
		   ie_tipo_atendimento		
	INTO   nr_seq_unid_int_w,
		   cd_convenio_w,
		   ie_tipo_atendimento_w		
	FROM   atendimento_paciente_v
	WHERE  nr_atendimento = nr_atendimento_p;
	
	obter_param_usuario(28, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, copias_laudo_w);
	obter_param_usuario(28, 17, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, copias_laudo_internado_w);
	
	IF (nr_seq_unid_int_w IS NULL) THEN
	BEGIN
	
	  nr_copias_w := obter_relat_regra_copia(cd_convenio_w, cd_setor_atendimento_p, ie_tipo_atendimento_w, cd_estabelecimento_p);
		
	  IF (nr_copias_w IS NULL) THEN 
	  BEGIN
	    nr_copias_w := copias_laudo_w;
	  END;
	  END IF;
	END;
	ELSE
	BEGIN
	  nr_copias_w := copias_laudo_internado_w;
	END;
	END IF;
  END;
  END IF;

  vl_retorno_w := nr_copias_w;

  RETURN vl_retorno_w;

END obter_nr_copias_laudo_pac;
/