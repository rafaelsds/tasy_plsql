create or replace
function obter_contribuinte_receita(cd_cgc_p		varchar2)
 		    	return varchar2 is

ds_retorno_w varchar2(1);
			
begin

select nvl(max(ie_contribuinte_receita),0)
into   ds_retorno_w
FROM   pessoa_juridica_compl pjc, 
       pessoa_juridica pj 
WHERE  pjc.cd_cgc = pj.cd_cgc
and    pj.cd_cgc = cd_cgc_p;  

return	ds_retorno_w;

end obter_contribuinte_receita;
/