CREATE OR REPLACE
FUNCTION HDJF_Obter_Se_Medico_Conven(
					cd_estabelecimento_p		Number,
					cd_pessoa_fisica_p		varchar2,
					cd_convenio_p			number,
					cd_prestador_p			varchar2,
					cd_especialidade_p		number,
					cd_categoria_p			varchar2,
					cd_setor_atendimento_p		number)
					RETURN VARCHAR IS

ie_conveniado_w			Varchar2(1)	:= 'N';

BEGIN

select	HDJF_Obter_Se_Medico_Credenc(cd_estabelecimento_p, cd_pessoa_fisica_p, cd_convenio_p, 
	cd_prestador_p, cd_especialidade_p, cd_categoria_p,cd_setor_atendimento_p)
into	ie_conveniado_w
from	dual;

RETURN ie_conveniado_w;

END HDJF_Obter_Se_Medico_Conven;
/
