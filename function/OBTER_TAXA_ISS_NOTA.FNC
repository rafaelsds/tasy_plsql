create or replace
function obter_taxa_iss_nota(	nr_seq_nota_p		number)
			return number is

tx_tributo_w		nota_fiscal_trib.tx_tributo%type;

begin

tx_tributo_w := 0;

if nr_seq_nota_p > 0 then

	select	nvl(sum(tx_tributo),0)
	into	tx_tributo_w
	from 	nota_fiscal_trib n,
		tributo t
	where	n.cd_tributo 	= t.cd_tributo
	and	t.ie_tipo_tributo	= 'ISS'
	and 	n.nr_sequencia	= nr_seq_nota_p;
	
end if;

return	tx_tributo_w;

end obter_taxa_iss_nota;
/
