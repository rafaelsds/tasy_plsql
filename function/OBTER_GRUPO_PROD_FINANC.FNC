create or replace
function OBTER_GRUPO_PROD_FINANC
		(nr_seq_produto_p	number,
		 ie_opcao_p	varchar2) return varchar2 is

/*
ie_opcao_p	
	1 - NR_SEQUENCIA
	2 - DS_GRUPO
*/

nr_seq_grupo_w		number(15,0);
ds_grupo_w		varchar2(255);
ds_retorno_w		varchar2(255);
nr_seq_grupo_sup_w	number(15);

begin

select	max(nr_seq_grupo) --Subgrupo
into	nr_seq_grupo_w
from	produto_financeiro
where	nr_sequencia		= nr_seq_produto_p;

select 	max(nr_seq_grupo_sup) --Grupo
into	nr_seq_grupo_sup_w
from 	grupo_prod_financ 
where 	nr_sequencia	= nr_seq_grupo_w;

if	(nr_seq_grupo_w is not null) then
	if	(ie_opcao_p = 1) then
		ds_retorno_w	:= nr_seq_grupo_sup_w;
	elsif	(ie_opcao_p = 2) then	
		select	ds_grupo
		into	ds_grupo_w
		from	grupo_prod_financ
		where	nr_sequencia	= nvl(nr_seq_grupo_sup_w,nr_seq_grupo_w);
		ds_retorno_w	:= ds_grupo_w;
	end if;
end if;

return	ds_retorno_w;

end OBTER_GRUPO_PROD_FINANC;
/