Create or replace
function tiss_obter_motivo_glosa	(nr_seq_motivo_glosa_p		number,
					ie_opcao_p			varchar2)
					return varchar2 is

/*	C - C�digo glosa
	D - Descri��o glosa
	G - Grupo glosa
	DG - Descri��o do grupo glosa	*/

ie_grupo_glosa_w		varchar2(2);
cd_motivo_tiss_w		varchar2(10);
ds_motivo_tiss_w		varchar2(255);
ds_grupo_glosa_w		varchar2(255);
ds_retorno_w			varchar2(255);

begin

select	ie_grupo_glosa,
	cd_motivo_tiss,
	ds_motivo_tiss,
	substr(obter_valor_dominio(1771, ie_grupo_glosa),1,255)
into	ie_grupo_glosa_w,
	cd_motivo_tiss_w,
	ds_motivo_tiss_w,
	ds_grupo_glosa_w
from	tiss_motivo_glosa
where	nr_sequencia	= nr_seq_motivo_glosa_p;

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= cd_motivo_tiss_w;
elsif	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_motivo_tiss_w;
elsif	(ie_opcao_p = 'G') then
	ds_retorno_w	:= ie_grupo_glosa_w;
elsif	(ie_opcao_p = 'DG') then
	ds_retorno_w	:= ds_grupo_glosa_w;
end if;

return ds_retorno_w;

end tiss_obter_motivo_glosa;
/