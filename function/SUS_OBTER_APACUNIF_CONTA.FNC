create or replace
function sus_obter_apacunif_conta(	nr_interno_conta_p	Number)
				return varchar2 is

ds_retorno_w	Varchar2(255);
nr_apac_w	Number(13);
nr_sequencia_w	Number(10);

begin

begin
select	max(nr_apac),
	max(nr_sequencia)
into	nr_apac_w,
	nr_sequencia_w
from	sus_apac_unif
where	nr_interno_conta	= nr_interno_conta_p;
exception
when others then
	ds_retorno_w		:= '';
end;

if	(nr_apac_w is not null) and
	(nr_sequencia_w is not null) then
	begin
	ds_retorno_w	:= nr_apac_w || ' - ' || nr_sequencia_w;
	end;
end if;
return ds_retorno_w;

end sus_obter_apacunif_conta;
/