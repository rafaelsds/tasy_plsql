create or replace
Function pls_obter_dados_vendedor_vinc
			(	nr_seq_vendedor_vinc_p		number,
				ie_opcao_p			varchar2)
				return varchar2 is

/*ie_opcao_p: 
	"N" - Nome vendedor vinculado
	"C" - C�digo de pessoa f�sica
	"M" - E-mail
*/

ds_retorno_w			varchar2(255);
nm_vendedor_w			varchar2(255);
cd_pessoa_fisica_w		varchar2(10);
ds_email_w			varchar2(255);

begin

select	substr(obter_nome_pf(cd_pessoa_fisica),1,255) nm_vendedor,
	cd_pessoa_fisica,
	obter_dados_pf_pj(cd_pessoa_fisica,null,'M') ds_email
into	nm_vendedor_w,
	cd_pessoa_fisica_w,
	ds_email_w
from	pls_vendedor_vinculado
where	nr_sequencia	= nr_seq_vendedor_vinc_p;

if	(ie_opcao_p = 'N') then
	ds_retorno_w	:= nm_vendedor_w;
elsif	(ie_opcao_p	= 'C') then
	ds_retorno_w	:= cd_pessoa_fisica_w;
elsif	(ie_opcao_p	= 'M') then
	ds_retorno_w	:= ds_email_w;
end if;

return ds_retorno_w;

end pls_obter_dados_vendedor_vinc;
/