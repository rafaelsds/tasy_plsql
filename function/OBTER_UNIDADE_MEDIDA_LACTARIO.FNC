create or replace
function Obter_unidade_medida_lactario( cd_material_p number)
			       return varchar2 is
			
ds_retorno_w	varchar2(5);			


begin

	SELECT 	max(cd_unidade_medida)
	INTO	ds_retorno_w
	FROM	nutricao_leite_deriv
	WHERE	cd_material = cd_material_p
	and 	nvl(ie_situacao,'A') = 'A';
	

return	ds_retorno_w;

end Obter_unidade_medida_lactario;
/