create or replace 
function MCSR_OBTER_SE_GERA_REPASSE(	nr_seq_propaci_p		number,
					nr_seq_proc_crit_repasse_p	number) 
					return number is

/*
0 - Gera repasse
1 - N�o gera
*/

ds_retorno_w			number(1);
nr_interno_conta_w		conta_paciente.nr_interno_conta%type;
cd_procedimento_w		procedimento_paciente.cd_procedimento%type;
cd_proc_criterio_w		proc_criterio_repasse.cd_procedimento%type;
ie_origem_proced_crit_w		proc_criterio_repasse.ie_origem_proced%type;
cd_area_proced_crit_w		proc_criterio_repasse.cd_area_proced%type;
cd_espec_proced_crit_w		proc_criterio_repasse.cd_especial_proced%type;
cd_grupo_proced_crit_w		proc_criterio_repasse.cd_grupo_proced%type;
cont_w				number(10);
ie_origem_proced_w		procedimento_paciente.ie_origem_proced%type;

begin

select	max(a.nr_interno_conta),
	max(a.cd_procedimento),
	max(a.ie_origem_proced)
into	nr_interno_conta_w,
	cd_procedimento_w,
	ie_origem_proced_w
from	procedimento_paciente a
where	a.nr_sequencia	= nr_seq_propaci_p;

select	max(a.cd_procedimento),
	max(a.ie_origem_proced),
	max(a.cd_area_proced),
	max(a.cd_especial_proced),
	max(a.cd_grupo_proced)
into	cd_proc_criterio_w,
	ie_origem_proced_crit_w,
	cd_area_proced_crit_w,
	cd_espec_proced_crit_w,
	cd_grupo_proced_crit_w
from	proc_criterio_repasse a
where	a.nr_sequencia	= nr_seq_proc_crit_repasse_p;

ds_retorno_w	:= 1;

if	(nr_interno_conta_w is not null) then
	
	select	count(*)
	into	cont_w
	from	estrutura_procedimento_v b,
		procedimento_paciente a
	where	a.nr_interno_conta	= nr_interno_conta_w
	and	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced	= b.ie_origem_proced
	and	(nvl(cd_proc_criterio_w,0) = 0 or a.cd_procedimento = cd_proc_criterio_w)
	and	(nvl(ie_origem_proced_crit_w,0) = 0 or a.ie_origem_proced = ie_origem_proced_crit_w)
	and	(nvl(cd_area_proced_crit_w,0) = 0 or b.cd_area_procedimento = cd_area_proced_crit_w)
	and	(nvl(cd_espec_proced_crit_w,0) = 0 or b.cd_especialidade = cd_espec_proced_crit_w)
	and	(nvl(cd_grupo_proced_crit_w,0) = 0 or b.cd_grupo_proc = cd_grupo_proced_crit_w)
	and	exists(	select	1
			from	procedimento_repasse x
			where	x.nr_seq_procedimento	= a.nr_sequencia);
	
	if	(cont_w	<> 0) then
		ds_retorno_w	:= 1;
	else	
		ds_retorno_w	:= 0;
	end if;

end if;	

return	ds_retorno_w;

end MCSR_OBTER_SE_GERA_REPASSE;
/