create or replace function obter_medic_marg_terap_estreit(	cd_material_p		NUMBER)
					return VARCHAR2 is
ds_retorno_w 	VARCHAR2(1);


begin

select nvl(max(mft.IE_MARGEM_TERAP_ESTREITA),'N')
 into	ds_retorno_w
from	material m, medic_ficha_tecnica mft
where	m.cd_material	= cd_material_p
and mft.NR_SEQUENCIA = m.NR_SEQ_FICHA_TECNICA;


return	ds_retorno_w;

end obter_medic_marg_terap_estreit;
/

