create or replace
function obter_se_clinica_curativo (	ie_clinica_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(2);
qt_reg_w	number(10);

begin

select	count(*)
into	qt_reg_w
from	regra_clinica_curativo;

if	(qt_reg_w = 0) or (ie_clinica_p is null) then
	ds_retorno_w	:= 'S';
else
	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	regra_clinica_curativo
	where	ie_clinica = ie_clinica_p;
end if;

return	ds_retorno_w;

end obter_se_clinica_curativo;
/