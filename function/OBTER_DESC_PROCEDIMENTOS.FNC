create or replace
function obter_desc_procedimentos( nr_atendimento_p	number)
 		    	return varchar2 is
ds_proc_w varchar(60);
ds_procedimentos_w varchar(2000);

Cursor C01 is
	select	substr(obter_desc_prescr_proc(a.cd_procedimento,a.ie_origem_proced,a.nr_seq_proc_interno),1,60)
	into	ds_proc_w
	from	prescr_procedimento a,
		prescr_medica b
	where	b.nr_prescricao = a.nr_prescricao
	and	b.nr_atendimento = nr_atendimento_p;

begin	
	
open C01;
loop
fetch C01 into	
	ds_proc_w;
exit when C01%notfound;
	begin
	if	( ds_procedimentos_w is null ) then 
		ds_procedimentos_w := ds_proc_w;
	else
		ds_procedimentos_w := substr(ds_procedimentos_w ||' / '|| ds_proc_w,1,2000);
	end if;
	end;
end loop;
close C01;
 

return	ds_procedimentos_w;

end obter_desc_procedimentos;
/ 
