create or replace
function hd_obter_profissional_paciente(	cd_pessoa_fisica_p	number,
						ie_tipo_p		varchar2,
						ie_opcao_p		varchar2) 
						return 			varchar2	is

/* 
ie_tipo_p
	'M' => M�dico
	'E' => Enfermeiro

ie_opcao_p
	'C' => C�digo
	'N' => Nome
*/


ds_retorno_w		varchar2(100);	
cd_profissional_w	varchar2(20);
nm_profissional_w	varchar2(100);


cursor C01 IS
select	cd_profissional,
	substr(obter_nome_pf(cd_profissional),1,50)
from	hd_profissional_resp
where	ie_tipo_profissional	= ie_tipo_p
and	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	dt_fim is null
order by
	dt_inicio;

begin

ds_retorno_w := '';

open C01;
	loop	
		fetch C01 into
			cd_profissional_w,
			nm_profissional_w;
		exit when C01%notfound;
	end loop;
	close C01;	


if (ie_opcao_p = 'C') then
	ds_retorno_w := cd_profissional_w;
else
	ds_retorno_w := nm_profissional_w;
end if;	



return	ds_retorno_w;

end	hd_obter_profissional_paciente;
/
