create or replace
function pls_obter_se_solic_comercial(	nr_sequencia_p	number,
				nr_telefone_p	varchar2,
				nr_celular_p	varchar2)
	 		    	return varchar2 is

qt_registro_w		number(10)	:= 0;
ie_retorno_w		varchar2(1)	:= 'N';
			
begin

select	count(*) 
into	qt_registro_w
from	pls_solicitacao_comercial
where	nr_sequencia <> nr_sequencia_p
and	(nr_telefone = nr_telefone_p
	or nr_telefone = nr_celular_p
	or nr_celular = nr_telefone_p
	or nr_celular = nr_celular_p)
order by	1;

if	(qt_registro_w > 0) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end pls_obter_se_solic_comercial;
/