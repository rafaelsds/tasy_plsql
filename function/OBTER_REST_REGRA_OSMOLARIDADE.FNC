create or replace 
function obter_rest_regra_osmolaridade( 
					qt_osmolaridade_p		nut_pac.qt_osmolaridade_total%type,
					ie_via_administracao_p	nut_pac.ie_via_administracao%type,
					ie_opcao_p				varchar2) return varchar2 is
/*  
C = Cor da regra
M = Mensagem da regra
*/
ds_cor_w			rep_consiste_osmolaridade.ds_cor%type;
ds_mensagem_w		rep_consiste_osmolaridade.ds_mensagem%type;
ds_retorno_w		varchar2(255);

begin

if	(qt_osmolaridade_p	is not null) then

	select	max(ds_mensagem),
			max(ds_cor)
	into	ds_mensagem_w,
			ds_cor_w
	from	rep_consiste_osmolaridade
	where	qt_osmolaridade_p between qt_inicial and qt_final
	and		((ie_via_administracao is null) or
			 (ie_via_administracao = ie_via_administracao_p));

	if	(ie_opcao_p = 'C') then
		ds_retorno_w := ds_cor_w;
	elsif	(ie_opcao_p = 'M') then
		ds_retorno_w := ds_mensagem_w;
	end if;

end if;

return ds_retorno_w;

end obter_rest_regra_osmolaridade;
/
