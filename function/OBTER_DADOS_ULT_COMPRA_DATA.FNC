create or replace
function obter_dados_ult_compra_data(	cd_estabelecimento_p	Number,
				cd_material_p		Number,
				cd_local_estoque_p	Number,
				dt_final_p			Date,
				qt_dias_p			Number,
				ie_opcao_p		Varchar2)
	 		    	return Varchar2 is

/*
DT	= Data Emissao
CM	= Custo medio NF
UC	= unidade medida compra
QT	= Quantidade Item NF
VTI	= valor total item nf
VU	= valor unitario item nf
VE	= valor unitario do item, convertido para quantidade de estoque
VI	= valor do item (considera descontos, frete, etc)
SN	= Sequencia NF
DTES	= Data Entrada Saida
C	= Comprador
OC	= Ordem Compra
PJ	= CGC fornecedor
NFS	= Numero sequencia da Nota 	Matheus 06/11/208 OS 115473
VEC	= valor unitario do item, convertido para quantidade de estoque, mesma coisa do VE
S	= Serie da nota
SI	= Serie do item
VLI	= Valor liquido item (mesmo que a function obter_valor_ultima_compra)
VEDT	= ultimo valor unitario do item em nota fiscal ate dt_final_p
*/

nr_sequencia_w			nota_fiscal.nr_sequencia%type;
cd_unidade_medida_compra_w	unidade_medida.cd_unidade_medida%type;
vl_total_item_nf_w			nota_fiscal_item.vl_total_item_nf%type;
vl_unitario_item_nf_w		nota_fiscal_item.vl_unitario_item_nf%type;
vl_item_nf_w			nota_fiscal_item.vl_total_item_nf%type;
qt_item_nf_w			nota_fiscal_item.qt_item_nf%type;
nr_nota_fiscal_w			nota_fiscal.nr_nota_fiscal%type;
nr_ordem_compra_w		ordem_compra.nr_ordem_compra%type;
cd_cgc_emitente_w		pessoa_juridica.cd_cgc%type;
dt_entrada_saida_w		nota_fiscal.dt_entrada_saida%type;
qt_item_estoque_w			nota_fiscal_item.qt_item_estoque%type;
vl_liquido_w			nota_fiscal_item.vl_liquido%type;
cd_serie_nf_w			nota_fiscal.cd_serie_nf%type;
cd_imob_serie_w			nota_fiscal_item.cd_imob_serie%type;

vl_retorno_w			Varchar2(255);
dt_emissao_w			nota_fiscal.dt_emissao%type;
vl_custo_medio_nf_w		nota_fiscal_item.vl_total_item_nf%type;
dt_final_w			date;
qt_dias_w			number(10);
nr_item_nf_w			nota_fiscal_item.nr_item_nf%type;
cd_material_w			material.cd_material%type;
qt_registros_w			number(10);
vl_liquido_item_w		nota_fiscal_item.vl_liquido%type;


begin

dt_final_w	:= nvl(dt_final_p,sysdate);
qt_dias_w	:= nvl(qt_dias_p,0);
qt_registros_w	:= 0;

select	nvl(max(nr_seq_nota),0)
into	nr_sequencia_w
from	sup_dados_ultima_compra
where	cd_estabelecimento = cd_estabelecimento_p
and	cd_material = cd_material_p;

if	(nr_sequencia_w = 0) then
	
	begin
	select	1
	into	qt_registros_w
	from	nota_fiscal a,
		nota_fiscal_item b,
		natureza_operacao n,
		operacao_nota o		
	where	a.dt_atualizacao_estoque <= sysdate
	and	a.nr_sequencia = b.nr_sequencia
	and	n.cd_natureza_operacao = a.cd_natureza_operacao
	and	a.cd_operacao_nf = o.cd_operacao_nf
	and	n.ie_entrada_saida = 'E'
	and	a.ie_situacao = '1'
	and	nvl(o.ie_devolucao,'N') = 'N'	
	and	a.ie_acao_nf = 1
	and	nvl(o.ie_ultima_compra, 'S') = 'S'
	and	b.cd_material	= cd_material_p
	and	b.cd_estabelecimento = cd_estabelecimento_p
	and	rownum <= 1;
	exception
	when others then
		qt_registros_w := 0;
	end;
	
	
end if;

if	(qt_registros_w > 0) and
	(qt_dias_w = 0) and
	(nr_sequencia_w = 0) then

	begin	
	select	nr_sequencia,
		nr_item_nf,
		cd_material
	into	nr_sequencia_w,
		nr_item_nf_w,
		cd_material_w
	from (
		SELECT /*+ FIRST_ROWS */  
			a.nr_sequencia,
			b.nr_item_nf,
			b.cd_material
		from	nota_fiscal a,
			nota_fiscal_item b,
			natureza_operacao n,
			operacao_nota o		
		where	a.dt_atualizacao_estoque <= dt_final_w
		and	a.nr_sequencia = b.nr_sequencia
		and	n.cd_natureza_operacao = a.cd_natureza_operacao
		and	a.cd_operacao_nf = o.cd_operacao_nf
		and	n.ie_entrada_saida = 'E'
		and	a.ie_situacao = '1'
		and	nvl(o.ie_devolucao,'N') = 'N'	
		and	a.ie_acao_nf = 1
		and	nvl(o.ie_ultima_compra, 'S') = 'S'
		and	b.cd_material	= cd_material_p
		and	b.cd_estabelecimento = cd_estabelecimento_p
		and not exists(
			select	1
			from	nota_fiscal x,
				nota_fiscal_item y
			where	x.nr_sequencia = y.nr_sequencia
			and	x.nr_sequencia_ref = a.nr_sequencia
			and	y.nr_item_nf = b.nr_item_nf
			union
			select	1
			from	nota_fiscal_item y
			where	y.nr_seq_nf_orig = a.nr_sequencia
			and	y.nr_seq_item_nf_orig = b.nr_item_nf)		
		order by a.nr_sequencia desc)
	where rownum <= 1;
	exception
	when others then
		nr_sequencia_w := 0;
	end;

elsif	(qt_registros_w > 0) and
	(qt_dias_w > 0) and
	(nr_sequencia_w = 0) then
	
	begin	
	select	nr_sequencia,
		nr_item_nf,
		cd_material
	into	nr_sequencia_w,
		nr_item_nf_w,
		cd_material_w
	from (
		SELECT /*+ FIRST_ROWS */  
			a.nr_sequencia,
			b.nr_item_nf,
			b.cd_material
		from	nota_fiscal a,
			nota_fiscal_item b,
			natureza_operacao n,
			operacao_nota o		
		where	a.dt_atualizacao_estoque between (dt_final_p - nvl(qt_dias_w,0)) and dt_final_p
		and	a.nr_sequencia = b.nr_sequencia
		and	n.cd_natureza_operacao = a.cd_natureza_operacao
		and	a.cd_operacao_nf = o.cd_operacao_nf
		and	n.ie_entrada_saida = 'E'
		and	a.ie_situacao = '1'
		and	nvl(o.ie_devolucao,'N') = 'N'	
		and	a.ie_acao_nf = 1
		and	nvl(o.ie_ultima_compra, 'S') = 'S'
		and	b.cd_material	= cd_material_p
		and	b.cd_estabelecimento = cd_estabelecimento_p	
		and not exists(
			select	1
			from	nota_fiscal x,
				nota_fiscal_item y
			where	x.nr_sequencia = y.nr_sequencia
			and	x.nr_sequencia_ref = a.nr_sequencia
			and	y.nr_item_nf = b.nr_item_nf
			union
			select	1
			from	nota_fiscal_item y
			where	y.nr_seq_nf_orig = a.nr_sequencia
			and	y.nr_seq_item_nf_orig = b.nr_item_nf)
		order by a.nr_sequencia desc)
	where rownum <= 1;
	exception
	when others then
		nr_sequencia_w := 0;
	end;
end if;
	
if	(nr_sequencia_w > 0) then

	select	nvl(max(cd_cgc_emitente),''),
		nvl(max(dt_entrada_saida),''),
		nvl(max(dt_emissao),''),
		nvl(max(somente_numero(nr_nota_fiscal)),0),
		nvl(max(cd_serie_nf),'')
	into	cd_cgc_emitente_w,
		dt_entrada_saida_w,
		dt_emissao_w,
		nr_nota_fiscal_w,
		cd_serie_nf_w
	from	nota_fiscal
	where	nr_sequencia = nr_sequencia_w;

	if	(ie_opcao_p in ('VU','VUQT')) then
		select	nvl(max(vl_unitario_item_nf),0),
			nvl(max(qt_item_nf),0)			
		into	vl_unitario_item_nf_w,
			qt_item_nf_w
		from	nota_fiscal_item
		where	nr_sequencia = nr_sequencia_w
		and	cd_material = cd_material_p;
		
	elsif	(ie_opcao_p = 'CM') then
		select	nvl(max(decode(vl_liquido,0,0,dividir(vl_liquido, qt_item_estoque))),0) vl_custo_medio_nf
		into	vl_custo_medio_nf_w
		from	nota_fiscal_item
		where	nr_sequencia = nr_sequencia_w
		and	cd_material = cd_material_p;
	else
	
		select 	max(cd_unidade_medida_compra),
			nvl(max(vl_total_item_nf),0),
			nvl(max(nr_ordem_compra),0),
			nvl(max(qt_item_nf),0),
			nvl(max(dividir((nvl(vl_total_item_nf,0) - nvl(vl_desconto,0) - nvl(vl_desconto_rateio,0) + nvl(vl_frete,0) + nvl(vl_despesa_acessoria,0) + nvl(vl_seguro,0)),qt_item_nf)),0),
			nvl(max(qt_item_estoque),0),
			nvl(max(vl_liquido),0),
			nvl(max(cd_imob_serie),''),
			nvl(max(dividir((nvl(vl_total_item_nf,0) - nvl(vl_desconto,0) - nvl(vl_desconto_rateio,0) + nvl(vl_frete,0) + nvl(vl_despesa_acessoria,0) + nvl(vl_seguro,0)),qt_item_estoque)),0)
		into	cd_unidade_medida_compra_w,
			vl_total_item_nf_w,
			nr_ordem_compra_w,
			qt_item_nf_w,
			vl_item_nf_w,
			qt_item_estoque_w,
			vl_liquido_w,
			cd_imob_serie_w,
			vl_liquido_item_w
		from	nota_fiscal_item
		where	nr_sequencia = nr_sequencia_w
		and	cd_material = cd_material_p;
	end if;
end if;

if	(ie_opcao_p = 'UC') then
	vl_retorno_w	:= cd_unidade_medida_compra_w;
elsif	(ie_opcao_p = 'VTI') then
	vl_retorno_w	:= vl_total_item_nf_w;
elsif	(ie_opcao_p = 'VU') then
	vl_retorno_w 	:= vl_unitario_item_nf_w;
elsif	(ie_opcao_p = 'NF') then
	vl_retorno_w 	:= substr(nr_nota_fiscal_w,1,100);
elsif	(ie_opcao_p = 'NFS') then
	vl_retorno_w 	:= nr_sequencia_w;
elsif	(ie_opcao_p = 'PJ') then
	vl_retorno_w	:= cd_cgc_emitente_w;
elsif	(ie_opcao_p = 'DTES') then
	vl_retorno_w	:= to_char(dt_entrada_saida_w,'dd/mm/yyyy');
elsif	(ie_opcao_p = 'S') then
	vl_retorno_w	:= cd_serie_nf_w;
elsif	(ie_opcao_p = 'SI') then
	vl_retorno_w	:= cd_imob_serie_w;
elsif	(ie_opcao_p = 'OC') then
	vl_retorno_w	:= to_char(nr_ordem_compra_w);
elsif	(ie_opcao_p = 'C') and
	(nr_ordem_compra_w > 0) then
	begin
	select	substr(sup_obter_nome_comprador(cd_estabelecimento_p,cd_comprador),1,100)
	into	vl_retorno_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_w;
	end;
elsif	(ie_opcao_p = 'QT') then
	vl_retorno_w	:= to_char(qt_item_nf_w);
elsif	(ie_opcao_p = 'VI') then
	vl_retorno_w	:= to_char(vl_item_nf_w);
elsif	(ie_opcao_p = 'VE') then
	vl_retorno_w 	:= to_char(dividir(vl_total_item_nf_w, qt_item_estoque_w));
elsif	(ie_opcao_p = 'CM') then
	vl_retorno_w 	:= to_char(vl_custo_medio_nf_w);
elsif	(ie_opcao_p = 'VEC') then
	vl_retorno_w 	:= to_char(dividir(vl_total_item_nf_w, qt_item_estoque_w));  /*vl_retorno_w := to_char(dividir(vl_unitario_item_nf_w, OBTER_DADOS_MATERIAL(cd_material_p,'QCE')));*/
elsif	(ie_opcao_p = 'DT') then
	vl_retorno_w	:= PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_emissao_w,'shortDate', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone);
elsif	(ie_opcao_p = 'VUQT') then
	vl_retorno_w := nvl(vl_unitario_item_nf_w * qt_item_nf_w,0);
elsif	(ie_opcao_p = 'SN') then
	vl_retorno_w := nr_sequencia_w;
elsif	(ie_opcao_p = 'VLI') then
	vl_retorno_w := vl_liquido_item_w;
elsif	(ie_opcao_p = 'VEDT') then
	select  nvl(max(vl_unitario_item_nf),0)
	into	vl_retorno_w
	from nota_fiscal_item
	where cd_material = cd_material_p
	and	nr_sequencia = nr_sequencia
	and nr_sequencia_nf = nr_sequencia_nf
	and nr_item_nf = nr_item_nf
	and nr_sequencia = (
		select	nvl(max(nf.nr_sequencia),0)
			from	nota_fiscal nf,
				nota_fiscal_item nfi,
				natureza_operacao n
			where	nf.nr_sequencia = nfi.nr_sequencia
			and	nfi.cd_material = cd_material_p
			and	nf.dt_atualizacao_estoque <= dt_final_w
			and	nf.ie_situacao = '1'
			and	n.cd_natureza_operacao = nf.cd_natureza_operacao
			and	n.ie_entrada_saida = 'E'
			and 	nf.cd_estabelecimento = cd_estabelecimento_p);
end if;
	

return	vl_retorno_w;

end obter_dados_ult_compra_data;
/
