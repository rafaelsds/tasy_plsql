create or replace
function com_obter_se_mostrar_mes_relat (
		dt_mes_p		date,
		dt_ref_prev_term_p	date)
		return varchar2 is
		
ie_mostrar_w	varchar2(1) := 'S';		
		
begin
if	(dt_mes_p is not null) and
	(dt_ref_prev_term_p is not null) then
	begin
	if	(trunc(dt_mes_p,'month') <= trunc(dt_ref_prev_term_p,'month')) then
		begin
		ie_mostrar_w := 'S';
		end;
	else
		begin
		ie_mostrar_w := 'N';
		end;
	end if;
	end;
end if;
return ie_mostrar_w;
end com_obter_se_mostrar_mes_relat;
/