create or replace
function pls_obter_dados_prest_titulo(	nr_titulo_p		number,
					ie_opcao_p		varchar2,
					ie_titulo_p		varchar2)
					return varchar2 is

nr_seq_prestador_w		number(10) := null;
ds_retorno_w			varchar2(255);
		
-- ie_titulo_p				
--'P' - T�tulo pagar

-- ie_opcao_p
-- 'CD' - C�digo do prestador

begin

if	(upper(ie_titulo_p) = 'P') then
	select	max(b.nr_seq_prestador)
	into	nr_seq_prestador_w
	from	pls_pagamento_prestador		b,
		titulo_pagar	a
	where	b.nr_sequencia	= a.nr_seq_pls_pag_prest
	and	a.nr_titulo = nr_titulo_p;
	
	if	(nr_seq_prestador_w is null) then
		select	max(b.nr_seq_prestador)
		into	nr_seq_prestador_w
		from	pls_pp_prestador b
		where	b.nr_titulo_pagar = nr_titulo_p;
	end if;
	
	if	(nr_seq_prestador_w is null) then
		select	max(a.nr_seq_prestador)
		into	nr_seq_prestador_w
		from	pls_evento_movimento	a
		where	a.nr_titulo_pagar	= nr_titulo_p;
	end if;

	if	(nr_seq_prestador_w is not null) and
		(upper(ie_opcao_p) = 'CD') then
		ds_retorno_w := substr(pls_obter_cod_prestador(nr_seq_prestador_w,null),1,30);
	end if;	
end if;

return	ds_retorno_w;

end pls_obter_dados_prest_titulo;
/