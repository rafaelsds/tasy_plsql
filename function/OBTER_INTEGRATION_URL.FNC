create or replace
function obter_integration_url(	nr_sequencia_p		number) 
						return varchar2 is

--ds_return_w			varchar2();
ie_evento_w			intpd_eventos.ie_evento%type;
NR_SEQ_SISTEMA_w	INTPD_EVENTOS_SISTEMA.NR_SEQ_SISTEMA%type;
						
begin

select	max(b.ie_evento),
		max(a.nr_seq_sistema)
into	ie_evento_w,
        nr_seq_sistema_w
from	intpd_eventos_sistema a,
		intpd_eventos b,
		INTPD_CONFIG_EVENTOS c		
where	a.nr_seq_evento = b.nr_sequencia
and		c.ie_evento = b.ie_evento
and		a.nr_sequencia = nr_sequencia_p
and		c.IE_ENVIA_RECEBE = 'R';

if (ie_evento_w is not null) then
	return '/WhebServidorIntegracao/philips_rest/endpoint?event='||ie_evento_w||chr(38)||'system='||nr_seq_sistema_w;
end if;

return null;

end;
/