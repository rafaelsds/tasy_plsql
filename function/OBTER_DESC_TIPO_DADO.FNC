CREATE OR REPLACE
FUNCTION obter_desc_tipo_dado	(nr_seq_tipo_dado_p	number)
				RETURN VARCHAR2 IS

ds_retorno_w	varchar2(80);

BEGIN

if	(nr_seq_tipo_dado_p is not null) then

	select	a.ds_tipo_dado
	into	ds_retorno_w
	from	proj_tipo_dado a
	where	a.nr_sequencia = nr_seq_tipo_dado_p;
end if;

RETURN	ds_retorno_w;

END	obter_desc_tipo_dado;
/