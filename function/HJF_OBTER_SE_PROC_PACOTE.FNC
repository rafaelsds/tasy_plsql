create or replace
function hjf_obter_se_proc_pacote (	nr_atendimento_p	number,
					nr_seq_item_p		number,
					nr_seq_proc_pacote_p	number)
					return varchar2 is

ds_retorno_w	varchar2(1);					

begin

if	(nr_seq_proc_pacote_p is not null) then
	ds_retorno_w := 'S';
else

	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	dual
	where	exists (select	*
			from	atendimento_pacote
			where	nr_atendimento 		= nr_atendimento_p
			and	nr_seq_proc_origem	= nr_seq_item_p);
end if;

			
return	ds_retorno_w;

end hjf_obter_se_proc_pacote;
/