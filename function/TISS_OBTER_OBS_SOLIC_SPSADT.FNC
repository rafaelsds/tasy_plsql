create or replace
function	tiss_obter_obs_solic_spsadt	(cd_convenio_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2)
		return varchar2 is

ds_observacao_w	varchar2(255) := null;
ie_observacao_w	varchar2(1);

begin

select	nvl(max(a.ie_observacao),'N')
into	ie_observacao_w
from	tiss_parametros_convenio a
where	a.cd_convenio		= cd_convenio_p
and	a.cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_observacao_w = 'U') then
	ds_observacao_w	:= nm_usuario_p;
end if;

return	ds_observacao_w;

end	tiss_obter_obs_solic_spsadt;
/