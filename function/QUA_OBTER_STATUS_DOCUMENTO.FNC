CREATE OR REPLACE
FUNCTION qua_obter_status_documento (nr_sequencia_p        Number)
                            	RETURN Varchar2 IS
ie_status_w	Varchar2(1);

BEGIN

if	(nr_sequencia_p is not null) then
    	select	ie_status
	into	ie_status_w
	from	qua_documento
	where	nr_sequencia = nr_sequencia_p;
end if;

RETURN	ie_status_w;

END	qua_obter_status_documento;
/