create or replace
function pls_obter_cnpj_congenere
			(	nr_sequencia_p		number)
				return varchar2 is
				
cd_cgc_w			varchar2(14);
	
begin

if (nvl(nr_sequencia_p,0) <> 0) then

	select	cd_cgc
	into	cd_cgc_w
	from	pls_congenere
	where	nr_sequencia	= nr_sequencia_p;

end if;

return	cd_cgc_w;

end pls_obter_cnpj_congenere;
/