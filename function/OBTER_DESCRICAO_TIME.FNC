create or replace
function obter_descricao_time	(cd_time_p	protocolo_integrado_time.nr_sequencia%TYPE)
				return varchar2 is
					
ds_equipe_w	protocolo_integrado_time.DS_EQUIPE%TYPE;			

begin
  if	(cd_time_p is not null) then
  
    select	ds_equipe
    into	ds_equipe_w
    from	protocolo_integrado_time
    where	nr_sequencia = cd_time_p;
  
  end if;

return ds_equipe_w;

end obter_descricao_time;
/