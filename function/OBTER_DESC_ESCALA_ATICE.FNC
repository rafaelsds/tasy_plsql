create or replace
function OBTER_DESC_ESCALA_ATICE(QT_SCORE_P	number)
 		return varchar2 is

DS_SCORE_W	varchar2(255) := '';

begin

if (QT_SCORE_P is not null) then
	begin
	if (QT_SCORE_P = 0) then
		begin
			DS_SCORE_W := obter_desc_expressao(971723);
		end;
	elsif (QT_SCORE_P = 20) then
		begin
			DS_SCORE_W := obter_desc_expressao(971719);
		end;
	end if;
	end;
end if;

return DS_SCORE_W;
end OBTER_DESC_ESCALA_ATICE;
/