CREATE OR REPLACE
FUNCTION Lab_Obter_Regra_Alerta	(nr_prescricao_p	number,
				 nr_sequencia_p	number,
				 ie_opcao_p	varchar2)
						return varchar is

qt_min_espera_w		number(10,0) := 0;
dt_inicial_w		date;
dt_final_w		date;
ie_status_final_w		number(2);
ie_status_atend_w		number(2);
qt_tempo_w		number(15);
ie_alerta_w		varchar2(1) := 'N';
ds_cor_w			varchar2(15) := '0';
	
BEGIN

/*ie_opcao_p
A - Verificar se deve alertar 
C - Retorna c�digo da cor da regra*/

if (ie_opcao_p = 'A') then

	select 	max(nvl(ie_status_atend,0))
	into	ie_status_atend_w
	from 	prescr_procedimento
	where 	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_sequencia_p;

	if (ie_status_atend_w > 0) then

		select 	max(nvl(dt_atualizacao,null))
		into 	dt_inicial_w
		from 	prescr_proc_etapa
		where 	nr_prescricao = nr_prescricao_p
		and 	nr_seq_prescricao = nr_sequencia_p
		and 	ie_etapa = ie_status_atend_w;

		select	max(nvl(ie_status_final,0))
		into	ie_status_final_w
		from 	regra_alerta_tempo_etapa
		where 	ie_status_inicial = ie_status_atend_w;

		if (ie_status_final_w > 0) then

			select 	max(dt_atualizacao)
			into 	dt_final_w
			from 	prescr_proc_etapa
			where 	nr_prescricao = nr_prescricao_p
			and 	nr_seq_prescricao = nr_sequencia_p
			and 	ie_etapa = ie_status_final_w;
			
			select	max(nvl(qt_tempo,0))
			into	qt_tempo_w
			from 	regra_alerta_tempo_etapa
			where 	ie_status_inicial = ie_status_atend_w
			and	ie_status_final = ie_status_final_w;

			begin
			select	round((nvl(dt_final_w, sysdate) -  dt_inicial_w) * 1440)
			into	qt_min_espera_w
			from 	dual;
			exception
			when 	others then
				qt_min_espera_w	:= 0;
			end;
			
			if (qt_min_espera_w > 0) and
			(qt_tempo_w > 0) and
			(qt_min_espera_w > qt_tempo_w) then
				ie_alerta_w := 'S';
			else
				ie_alerta_w := 'N';
			end if;

		end if;
	end if;

	RETURN	ie_alerta_w;
	
elsif (ie_opcao_p = 'C') then

	select 	max(nvl(ie_status_atend,0))
	into	ie_status_atend_w
	from 	prescr_procedimento
	where 	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_sequencia_p;
	
	if (ie_status_atend_w > 0) then
		
		select	max(nvl(ds_cor,0))
		into	ds_cor_w
		from 	regra_alerta_tempo_etapa
		where 	ie_status_inicial = ie_status_atend_w;
		
	end if;
	
	RETURN	ds_cor_w;
	
end if;

END	Lab_Obter_Regra_Alerta;
/