create or replace
function hd_obter_ds_germe(nr_seq_germe_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);			

begin
if (nr_seq_germe_p is not null) then

	select	ds_germe
	into	ds_retorno_w
	from	hd_germes
	where	nr_sequencia = nr_seq_germe_p;
	
end if;

return	ds_retorno_w;

end hd_obter_ds_germe;
/