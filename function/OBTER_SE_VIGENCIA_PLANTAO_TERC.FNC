create or replace
function obter_se_vigencia_plantao_terc(	nr_seq_terceiro_p	number,
						cd_medico_p		varchar2,
						dt_plantao_p		date)
				return varchar2 is

ds_retorno_w		varchar2(1);
qt_vig_w		number(10);

begin

select 	count(1)
into	qt_vig_w
from 	terceiro_pessoa_fisica
where	nr_seq_terceiro		= nr_seq_terceiro_p
and	dt_plantao_p	 	between 	nvl(dt_inicio_vigencia,dt_plantao_p) and nvl(dt_fim_vigencia,sysdate+1)
and	cd_pessoa_fisica = cd_medico_p;

if	(qt_vig_w = 0) then
	select 	count(1)
	into	qt_vig_w
	from 	terceiro
	where	nr_sequencia		= nr_seq_terceiro_p
	and	dt_plantao_p	 	between 	nvl(dt_inicio_vigencia,dt_plantao_p) and nvl(dt_fim_vigencia,sysdate+1)
	and	cd_pessoa_fisica = cd_medico_p;
end if;

if	(qt_vig_w = 0) then
	ds_retorno_w	:= 'N';
else
	ds_retorno_w	:= 'S';
end if;	

return	ds_retorno_w;

end obter_se_vigencia_plantao_terc;
/