create or replace 
function obter_preco_proc_ipasgo(
			cd_estabelecimento_p		number,
			cd_convenio_p			number,
			cd_categoria_p		varchar2,
			dt_conta_p			date,
			cd_procedimento_p		number,
			ie_origem_proced_p		number,
			nr_seq_proc_interno_p		number,
			cd_tipo_acomodacao_p		number,
			ie_tipo_atendimento_p	number, 
			cd_setor_atendimento_p	number,
			cd_medico_p			varchar2,
			cd_funcao_medico_p		varchar2,
			cd_usuario_convenio_p	varchar2,
			cd_plano_p			varchar2,
			ie_clinica_p			number,
			cd_empresa_ref_p		number,
			ie_resp_credito_p		varchar2,
			ie_opcao_p			varchar2,
			nr_sequencia_p		number default null)
			return number is

/* ie_opcao_p:
P	- Procedimento	
M	- Medico	
C	- Custo Op.	
F	- Filme   
PM	- Ponto Medico
H	- Honorario
IPDT	- IPASGO Dados taratmento: Resp. credito = 'M' vl_medico, <> 'M' vl_procedimento
IPDE	- IPASGO Dados exames: Resp. credito = 'M' (vl_medico + vl_material), <> 'M' vl_procedimento
IPAC	- IPASGO Alto custo: Resp. credito = 'M' (vl_medico + vl_material), <> 'M' entao se vl_material = 0 vl_procedimento, <> 0 vl_material
*/

qt_pontos_w			preco_amb.qt_pontuacao%type;
vl_procedimento_w			number(15,4);
vl_custo_operacional_w		number(15,4);
vl_anestesista_w			number(15,4);
vl_medico_w				number(15,4);
vl_auxiliares_w			number(15,4);
vl_materiais_w			number(15,4);
vl_pto_procedimento_w		number(15,4);
vl_pto_custo_operac_w		number(15,4);
vl_pto_anestesista_w			number(15,4);
vl_pto_medico_w			number(15,4);
vl_pto_auxiliares_w			number(15,4);
vl_pto_materiais_w			number(15,4);
qt_porte_anestesico_w		number(15,4);
cd_edicao_amb_w			number(06,0);
vl_resultado_w			number(15,4);
cd_area_procedimento_w		number(15,4);
ie_preco_informado_w		varchar2(01);
ie_classificacao_w		varchar2(01);
nr_seq_ajuste_proc_w		number(10,0);
qt_participante_w			number(10,0) := 0;
vl_auxiliares_ww			number(15,4) := 0;

begin

select	max(ie_classificacao)
into	ie_classificacao_w
from	procedimento
where	cd_procedimento	= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

if	(ie_classificacao_w = '1') then
	begin	
	define_preco_procedimento(
		cd_estabelecimento_p,
		cd_convenio_p,
		cd_categoria_p,
		dt_conta_p,
		cd_procedimento_p,
		cd_tipo_acomodacao_p,
		ie_tipo_atendimento_p,
		cd_setor_atendimento_p,
		cd_medico_p,
		cd_funcao_medico_p,
		null,
		null,
		nr_seq_proc_interno_p,
		cd_usuario_convenio_p, 
		cd_plano_p, 
		ie_clinica_p, 
		cd_empresa_ref_p,
		null,
		vl_procedimento_w,
		vl_custo_operacional_w,
		vl_anestesista_w,
		vl_medico_w,
		vl_auxiliares_w,
		vl_materiais_w,
		vl_pto_procedimento_w,
		vl_pto_custo_operac_w,
		vl_pto_anestesista_w,
		vl_pto_medico_w,
		vl_pto_auxiliares_w,
		vl_pto_materiais_w,
		qt_porte_anestesico_w,
		qt_pontos_w,
		cd_edicao_amb_w, 
		ie_preco_informado_w,
		nr_seq_ajuste_proc_w,
		0, 
		null, 
		0, 
		null, 
		null,
		null,
		null, 
		null, 
		null, 
		null, 
		null, 
		null, 
		null, 
		null, 
		null, 
		null,
		null,
		null,
		null,
		null);

	if	(vl_auxiliares_w > 0) and 
		(nr_sequencia_p is not null) then
		begin
		
		select sum(nvl(vl_conta,0))
		into vl_auxiliares_ww
		from procedimento_participante
		where nr_sequencia = nr_sequencia_p
		and	nvl(vl_conta,0) > 0;
		
		if (vl_auxiliares_ww > 0) then
			vl_procedimento_w := (vl_custo_operacional_w +  vl_anestesista_w +  vl_medico_w + vl_auxiliares_ww + vl_materiais_w);
		else
			vl_procedimento_w := vl_procedimento_w - vl_auxiliares_w;
		end if;
		
		end;
	end if;
	end;
else	begin
	define_preco_servico(
		cd_estabelecimento_p,
		cd_convenio_p,
		cd_categoria_p,
		dt_conta_p,
		cd_procedimento_p,
		cd_setor_atendimento_p,
		ie_tipo_atendimento_p,
		cd_tipo_acomodacao_p,
		cd_usuario_convenio_p,
		cd_plano_p,
		ie_clinica_p,
		cd_empresa_ref_p,
		null,
		vl_procedimento_w,
		nr_seq_ajuste_proc_w,
		null, 
		0, 
		null,
		nr_seq_proc_interno_p, 
		null, 
		null, 
		null, 
		null, 
		null, null,
		null,
		null,
		null,
    cd_medico_p);
	end;

end if;

if	(ie_opcao_p	is not null) then
	begin
	if	(ie_opcao_p	= 'P') then
		vl_resultado_w	:= vl_procedimento_w;
	elsif	(ie_opcao_p	= 'C') then
		vl_resultado_w	:= vl_custo_operacional_w;
	elsif	(ie_opcao_p	= 'M') then
		vl_resultado_w	:= vl_medico_w;
	elsif	(ie_opcao_p	= 'F') then
		vl_resultado_w	:= vl_materiais_w;
	elsif	(ie_opcao_p	= 'PP') then
		vl_resultado_w	:= vl_pto_procedimento_w;
	elsif	(ie_opcao_p	= 'PM') then
		vl_resultado_w	:= vl_pto_medico_w;
	elsif	(ie_opcao_p	= 'H') then
		vl_resultado_w	:= vl_auxiliares_w + vl_medico_w + vl_anestesista_w;
	elsif	(ie_opcao_p	= 'PA') then
		vl_resultado_w	:= qt_porte_anestesico_w;
	elsif	(ie_opcao_p	= 'A') then
		vl_resultado_w	:= nvl(vl_anestesista_w,0);
	elsif	(ie_opcao_p	= 'IPDT') then
		if	(nvl(ipasgo_obter_se_respcred_soma(nvl(ie_resp_credito_p,'0')),'N') = 'S') then
			if	(ie_classificacao_w = 1) then
				vl_resultado_w	:= nvl(vl_medico_w,0) + nvl(vl_custo_operacional_w,0);	
			else
				vl_resultado_w	:= vl_procedimento_w;
			end if;
		else
			vl_resultado_w	:= vl_procedimento_w;
		end if;
	elsif	(ie_opcao_p	= 'IPDE') then
		if	(nvl(ipasgo_obter_se_respcred_soma(nvl(ie_resp_credito_p,'0')),'N') = 'S') then
			vl_resultado_w	:= nvl(vl_medico_w,0) + nvl(vl_materiais_w,0);	
		else
			vl_resultado_w	:= vl_procedimento_w;
		end if;
	elsif	(ie_opcao_p	= 'IPAC') then
		if	(nvl(ipasgo_obter_se_respcred_soma(nvl(ie_resp_credito_p,'0')),'N') = 'S') then
			vl_resultado_w	:= nvl(vl_medico_w,0) + nvl(vl_materiais_w,0);	
		elsif	(nvl(vl_materiais_w,0) = 0) then
			vl_resultado_w	:= vl_procedimento_w;
		else	
			vl_resultado_w	:= nvl(vl_materiais_w,0);
		end if;
	end if;	
	end;
end if;

return vl_resultado_w;

end obter_preco_proc_ipasgo;
/
