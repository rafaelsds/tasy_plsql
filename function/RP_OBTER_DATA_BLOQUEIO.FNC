create or replace
function rp_obter_data_bloqueio(nr_seq_reabilitacao_p		number,
			cd_estabelecimento_p		number)
 		    	return date is
dt_retorno_w	date;
begin

if 	(nr_seq_reabilitacao_p is not null) then

	select 	nvl(max(a.dt_alteracao),to_date('01/01/1900','dd/mm/yyyy'))
	into	dt_retorno_w
	from	rp_status_pac a,
		rp_paciente_reabilitacao b
	where   a.nr_seq_pac_reab = b.nr_sequencia
	and	b.nr_sequencia = nr_seq_reabilitacao_p 
	and	a.nr_seq_status_ant = (	select (NR_SEQ_PAC_REAB_BLOQUEIO)
					from    RP_PARAMETROS
					where   cd_estabelecimento = cd_estabelecimento_p);						
end if;
return	dt_retorno_w;

end rp_obter_data_bloqueio;
/