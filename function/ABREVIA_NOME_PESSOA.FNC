create or replace function abrevia_nome_pessoa(	cd_pessoa_fisica_p	varchar2)
				return varchar2 is

/*
ie_opcao_p - Para futuras customizações
*/

nm_pf_abrev_w		varchar2(60);
nm_pessoa_fisica_w	varchar2(80);
nm_sobrenome_w		varchar2(80);

k			number(2);
i			number(2);

begin

select	SUBSTR(obter_initcap(obter_nome_pf(cd_pessoa_fisica_p)), 0, 80)
into	nm_pessoa_fisica_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;	

k	:= instr(nm_pessoa_fisica_w,' ');

nm_pf_abrev_w := substr(nm_pessoa_fisica_w,1,k-1);

for i in k..length(nm_pessoa_fisica_w) loop
	begin
	if	(substr(nm_pessoa_fisica_w,i,1) = upper(substr(nm_pessoa_fisica_w,i,1))) then
		if	(nm_sobrenome_w is null) then
			nm_sobrenome_w := substr(nm_pessoa_fisica_w,i,1);
		else
			nm_sobrenome_w := nm_sobrenome_w || substr(nm_pessoa_fisica_w,i,1);
		end if;
	end if;
	end;
end loop;

return	substr(nm_pf_abrev_w,1,1) || replace(nm_sobrenome_w,'  ',' ');

end abrevia_nome_pessoa;
/
