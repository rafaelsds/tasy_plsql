create or replace
function Sus_Obter_Competencia(	cd_estabelecimento_p	Number,
				ie_opcao_p		Varchar2)
 		    	return date is

ds_retorno_w		Date;
dt_competencia_aih_w	Date;
dt_comp_apac_bpa_w	Date;
			
begin
select	max(dt_competencia_sus),
	max(dt_comp_apac_bpa)
into	dt_competencia_aih_w,
	dt_comp_apac_bpa_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_opcao_p	= 'AIH') then
	ds_retorno_w	:= dt_competencia_aih_w;
else
	ds_retorno_w	:= dt_comp_apac_bpa_w;
end if;

return	ds_retorno_w;

end Sus_Obter_Competencia;
/