create or replace
function pls_obter_rata_antecip_men_seg
			(nr_seq_mens_segurado_p	number,
			ie_opcao_p		varchar2)
 		    	return number is
/*
PR - Valor Pr� Rata dia
AT - Valor Antecipacao
*/

vl_retorno_w		number(10,2);
begin

if	(ie_opcao_p	= 'PR') then
	select	sum(nvl(a.vl_pro_rata_dia,0))
	into	vl_retorno_w
	from	pls_mensalidade_seg_item	a,
		pls_mensalidade_segurado	b
	where	b.nr_sequencia			= nr_seq_mens_segurado_p
	and	a.nr_seq_mensalidade_seg	= b.nr_sequencia;
elsif	(ie_opcao_p	= 'AT') then
	select	sum(nvl(a.vl_antecipacao,0))
	into	vl_retorno_w
	from	pls_mensalidade_seg_item	a,
		pls_mensalidade_segurado	b
	where	b.nr_sequencia			= nr_seq_mens_segurado_p
	and	a.nr_seq_mensalidade_seg	= b.nr_sequencia;
end if;	

return	vl_retorno_w;

end pls_obter_rata_antecip_men_seg;
/
