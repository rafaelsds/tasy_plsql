create or replace
function obter_se_amostra_item_sc(	nr_solic_compra_p		number,
				nr_item_solic_compra_p		number)
 		    	return varchar2 is

ie_amostra_w		varchar2(1) := 'N';

begin

select	nvl(max(ie_amostra),'N')
into	ie_amostra_w
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p
and	nr_item_solic_compra = nr_item_solic_compra_p;

return	ie_amostra_w;

end obter_se_amostra_item_sc;
/