create or replace
function HMLB_OBTER_SE_EXAME_PENDENTE(nr_seq_doacao_p	number)
 		    	return varchar2 is
			
ie_pendente_w	varchar2(1);			

begin
select 	   decode(count(*),0,'N','S')
into   	   ie_pendente_w
from   	   san_exame_realizado a,
	   san_exame_lote b,
	   san_exame c
where  	   b.nr_sequencia = a.nr_seq_exame_lote
and	   a.nr_seq_exame = c.nr_sequencia
and	   c.ie_sorologia = 'S'
and	   b.nr_seq_doacao = nr_seq_doacao_p
and	   upper(a.ds_resultado) in ('ANDAMENTO','REAGENTE','INDETERMINADO');

return	ie_pendente_w;

end HMLB_OBTER_SE_EXAME_PENDENTE;
/