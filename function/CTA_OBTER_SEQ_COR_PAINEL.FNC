create or replace
function cta_obter_seq_cor_painel( vl_referencia_p		number)  
				return varchar2 is 

nr_seq_cor_w	cta_regra_cor_painel.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	cta_regra_cor_painel
	where	vl_referencia_p between nvl(vl_minimo,vl_referencia_p) and nvl(vl_maximo,vl_referencia_p)
	order by nvl(vl_maximo,vl_referencia_p);

begin

open C01;
loop
fetch C01 into	
	nr_seq_cor_w;
exit when C01%notfound;
	begin
	nr_seq_cor_w	:= nr_seq_cor_w;
	end;
end loop;
close C01;

return	nr_seq_cor_w;

end cta_obter_seq_cor_painel;
/
