CREATE OR REPLACE
FUNCTION Obter_Desc_Cep_Loc(cd_cep_p	varchar2)	
					RETURN VarChar2 IS

ds_resultado_w		VarChar2(240)	:= '';
ie_cep_novo_w		Varchar2(01)	:= '';
ie_cep_logradouro_w	varchar2(01)	:= 'N';

cursor	c01 is
	select	a.nm_localidade
	from	cep_loc a
	where	a.cd_cep	= cd_cep_p
	  and	nvl(a.nr_seq_superior, 0) = 0
	  and	ie_cep_novo_w	= 'S'
	union
	select	b.nm_localidade
	from	cep_localidade b
	where	b.cd_localidade	= cd_cep_p
	and	ie_cep_novo_w	= 'N';

BEGIN

begin

begin
if	(cd_cep_p is not null) then
	begin
	
	select	nvl(vl_parametro, vl_parametro_padrao)
	into	ie_cep_novo_w
	from	funcao_parametro
	where	cd_funcao	= 0
	and	nr_sequencia	= 25;
	
	open c01;
	loop
	exit when c01%notfound;
		fetch c01 into ds_resultado_w;
	end loop;
	close c01;
	end;
end if;
exception
	when others then
	ds_resultado_w	:= '';
end;
	
if	((cd_cep_p is not null) and ((ds_resultado_w	is null) or
	(ds_resultado_w = ''))) then
	begin

	if	(ie_cep_novo_w		= 'S') then
		select	max(b.nm_localidade)
		into	ds_resultado_w
		from	cep_loc b,
			cep_log a
		where	to_number(a.nr_seq_loc)	= b.nr_sequencia
		and	a.cd_cep	= cd_cep_p;
		if	(ds_resultado_w is null) then
			select	max(b.nm_localidade)
			into	ds_resultado_w
			from	cep_loc b
			where	b.cd_cep	= cd_cep_p;
		end if;
	else
		select	max(nm_localidade)
		into	ds_resultado_w
		from	cep_logradouro
		where	cd_logradouro	= cd_cep_p;
	end if;
	

	exception
		when others then
		ds_resultado_w	:= '';
	end;
end if;

end;

RETURN ds_resultado_w;

END Obter_Desc_Cep_Loc;
/