create or replace
function Obter_titulo_etapa(nr_seq_etapa_p	number)
			return VARCHAR2 is

ds_retorno_w		VARCHAR2(80);

begin

select	ds_titulo_etapa
into	ds_retorno_w
from	gap_ritual
where	nr_sequencia = nr_seq_etapa_p;

return	ds_retorno_w;
end Obter_titulo_etapa;
/
