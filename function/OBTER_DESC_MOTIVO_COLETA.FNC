create or replace
function obter_desc_motivo_coleta(nr_sequencia_p		number)
 		    	return varchar2 is

ds_motivo_coleta_w	varchar2(255);
ds_retorno_w	varchar2(255);

begin

if (nr_sequencia_p is not null) then
	select	a.ds_motivo_coleta
	into	ds_motivo_coleta_w
	from	biob_motivo_coleta a
	where	a.nr_sequencia = nr_sequencia_p;
end if;

if (ds_motivo_coleta_w is not null) then
	ds_retorno_w := ds_motivo_coleta_w;
else
	ds_retorno_w := '';
end if;

return	ds_retorno_w;

end obter_desc_motivo_coleta;
/