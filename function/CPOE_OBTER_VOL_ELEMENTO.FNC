create or replace 
function cpoe_obter_vol_elemento(
			nr_seq_npt_cpoe_p		cpoe_dieta.nr_sequencia%type,
			nr_seq_elem_cpoe_p		cpoe_npt_elemento.nr_sequencia%type) return number is


qt_volume_w		Number(15,3) := 0;

begin

select	nvl(sum(nvl(qt_volume,0)),0)
into	qt_volume_w
from	cpoe_npt_produto
where	nr_seq_npt_cpoe = nr_seq_npt_cpoe_p
and	nr_seq_elemento = nr_seq_elem_cpoe_p;

return qt_volume_w;

end cpoe_obter_vol_elemento;
/