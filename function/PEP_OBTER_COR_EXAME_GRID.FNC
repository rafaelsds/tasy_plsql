create or replace
function pep_obter_cor_exame_grid(	qt_resultado_p	number,
					qt_minima_p	number,
					qt_maxima_p	number,
					pr_resultado_p	number,
					pr_minimo_p	number,
					pr_maximo_p	number)
 		    	return varchar2 is

			
		
begin


if	(qt_resultado_p	is not null) then

	if	(qt_minima_p is not null) and
		(qt_resultado_p	< qt_minima_p) then
		return '1088';
	elsif	(qt_maxima_p	is not null) and
		(qt_resultado_p	> qt_maxima_p) then
		return '1089';
	end if;	

end if;

if	(pr_resultado_p	is not null) then

	if	(pr_minimo_p is not null) and
		(pr_resultado_p	< pr_minimo_p) then
		return '1088';
	elsif	(pr_maximo_p	is not null) and
		(pr_resultado_p	> pr_maximo_p) then
		return '1089';
	end if;	

end if;


return	null;

end pep_obter_cor_exame_grid;
/