create or replace
function rxt_obter_desc_tecnica(
				nr_seq_tecnica_p	number)
				return varchar2 is

ds_retorno_w	varchar2(255);
			
begin

SELECT	MAX(ds_tecnica)
INTO	ds_retorno_w
FROM	rxt_tecnica_modalidade
WHERE	nr_sequencia	= nr_seq_tecnica_p;


return	ds_retorno_w;

end rxt_obter_desc_tecnica;
/