create or replace function obter_se_medico_plantao (cd_medico_p 	varchar2,
						    	dt_parametro_p	date)
							return number is


/*
1 - M�dico est� plantao
0 - M�dico n�o est� plantao
*/

qt_plantao_w	number(5);

begin

select 	nvl(count(*),0)
into   	qt_plantao_w
from	escala_diaria
where	cd_pessoa_fisica = cd_medico_p
and	dt_parametro_p between dt_inicio and dt_fim;

if (qt_plantao_w > 0) then
	return 1;
else
	return 0;
end if;

end;
/