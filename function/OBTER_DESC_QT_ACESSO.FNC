create or replace
function obter_desc_qt_acesso (	nr_sequencia_p	number) 
					return varchar2 is

ds_retorno_w	varchar2(255):= '';

begin

if	(nr_sequencia_p is not null) then

	select	ds_acesso
	into	ds_retorno_w
	from	qt_acesso
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end obter_desc_qt_acesso;
/
