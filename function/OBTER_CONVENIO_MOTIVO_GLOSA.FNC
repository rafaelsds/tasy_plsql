Create or Replace
FUNCTION Obter_Convenio_Motivo_Glosa(	cd_convenio_p		number,
					cd_glosa_convenio_p	Varchar2)
					RETURN VarChar2 IS

cd_motivo_w		varchar2(255)	:= null;

BEGIN

if	(cd_glosa_convenio_p is not null) then

	select	nvl(max(to_char(cd_motivo_glosa)), cd_glosa_convenio_p)
	into	cd_motivo_w
	from	convenio_motivo_glosa
	where	cd_convenio			= cd_convenio_p
	and	to_char(cd_glosa_convenio)		= nvl(cd_glosa_convenio_p,'X');

end if;

RETURN cd_motivo_w;

END Obter_Convenio_Motivo_Glosa;
/