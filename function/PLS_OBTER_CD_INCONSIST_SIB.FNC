create or replace
function  pls_obter_cd_inconsist_sib
		(	nr_seq_inconsistencia_p		number)
			return number is
			
nr_sequencia_w		number(10);

begin

select	cd_inconsistencia
into	nr_sequencia_w
from	sib_inconsistencia
where	nr_sequencia	= nr_seq_inconsistencia_p;

return	nr_sequencia_w;

end  pls_obter_cd_inconsist_sib;
/