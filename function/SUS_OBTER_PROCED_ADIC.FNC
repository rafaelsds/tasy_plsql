CREATE OR REPLACE
FUNCTION sus_Obter_proced_adic(	nr_seq_interno_p	number,
				ie_laudo_p		number,
				ie_tipo_p		number)
				return number is

cd_procedimento_w	number(15);
qt_laudo_w		number(5)	:= 1;
qt_procedimento_w	number(5)	:= 0;
cd_retorno_ww		number(15);
cd_retorno_w		number(15);

/* ie_tipo_p
   1 - C�d. Procedimento
   2 - Qtde Procedimento   */

CURSOR C01 IS
   select	cd_procedimento,
           	qt_procedimento 
   from		sus_laudo_proced_adic
   where	nr_seq_laudo      = nr_seq_interno_p
   order by	nr_seq_laudo;

BEGIN
OPEN C01;
LOOP

FETCH C01 into
   cd_procedimento_w,
   qt_procedimento_w;
exit when c01%notfound;
   begin
   if	   (ie_tipo_p      = 1) then
           cd_retorno_ww   := cd_procedimento_w;
   elsif   (ie_tipo_p      = 2) then
           cd_retorno_ww   := qt_procedimento_w;
   end if;
   if      (ie_laudo_p     = 1) and
           (qt_laudo_w     = 1) then
           cd_retorno_w    := cd_retorno_ww;
   elsif   (ie_laudo_p     = 2) and
           (qt_laudo_w     = 2) then
           cd_retorno_w    := cd_retorno_ww;
   elsif   (ie_laudo_p     = 3) and
           (qt_laudo_w     = 3) then
           cd_retorno_w    := cd_retorno_ww;
   end if;
   qt_laudo_w      := qt_laudo_w + 1;
   end;
END LOOP;
CLOSE C01;

return     cd_retorno_w;

END sus_Obter_proced_adic;
/
