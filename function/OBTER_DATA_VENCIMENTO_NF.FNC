Create or Replace
Function	obter_data_vencimento_nf(nr_sequencia_p	number)
		return Date is

dt_retorno_w	Date;

BEGIN

select	max(dt_vencimento)
into	dt_retorno_w
from	nota_fiscal_venc
where	nr_sequencia = nr_sequencia_p;

RETURN dt_retorno_w;

END obter_data_vencimento_nf;
/