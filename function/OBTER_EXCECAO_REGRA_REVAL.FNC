create or replace
function obter_excecao_regra_reval (nr_seq_cpoe_p 		number,
								   ie_tipo_item_p		varchar2,
								   dt_liberacao_p		date)
						return date is
		
ds_retorno_w 		date;
ie_exists_rule_w	varchar2(1 char);
ie_exists_events_w	varchar2(1 char);

begin

	ie_exists_events_w := 'S';

	begin
		select	nvl(max('S'),'N')
		into	ie_exists_rule_w
		from	cpoe_revalidation_rule
		where	nvl(ie_situacao,'I') = 'A';

		if (ie_exists_rule_w = 'S') then

			if (ie_tipo_item_p = 'N') then

				select	nvl(max('S'),'N')
				into	ie_exists_events_w
				from	cpoe_revalidation_events
				where	nr_seq_diet = nr_seq_cpoe_p;

			elsif (ie_tipo_item_p = 'H') then
				
				select	nvl(max('S'),'N')
				into	ie_exists_events_w
				from	cpoe_revalidation_events
				where	nr_seq_hemotherapy = nr_seq_cpoe_p;

			elsif (ie_tipo_item_p in ('D','DI','DP')) then
				
				select	nvl(max('S'),'N')
				into	ie_exists_events_w
				from	cpoe_revalidation_events
				where	nr_seq_dialysis = nr_seq_cpoe_p;

			elsif (ie_tipo_item_p = 'G') then

				select	nvl(max('S'),'N')
				into	ie_exists_events_w
				from	cpoe_revalidation_events
				where	nr_seq_gasotherapy = nr_seq_cpoe_p;

			elsif (ie_tipo_item_p in ('M','MA')) then
			
				select	nvl(max('S'),'N')
				into	ie_exists_events_w
				from	cpoe_revalidation_events
				where	nr_seq_material = nr_seq_cpoe_p;

			elsif (ie_tipo_item_p = 'R') then
				
				select	nvl(max('S'),'N')
				into	ie_exists_events_w
				from	cpoe_revalidation_events
				where	nr_seq_recomendation = nr_seq_cpoe_p;
				
			elsif (ie_tipo_item_p = 'P') then
				
				select	nvl(max('S'),'N')
				into	ie_exists_events_w
				from	cpoe_revalidation_events
				where	nr_seq_exam = nr_seq_cpoe_p;

			end if;

			if (ie_exists_events_w = 'N') then
				ds_retorno_w := dt_liberacao_p + 1;
			end if;

		end if;

	exception when others then
		ds_retorno_w := null;
	end;

	return ds_retorno_w;
	
end obter_excecao_regra_reval;
/
