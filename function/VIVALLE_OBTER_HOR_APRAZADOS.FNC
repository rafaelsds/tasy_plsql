create or replace
function Vivalle_Obter_Hor_aprazados(	nr_prescricao_p       number,
					 nr_seq_material_p     number,
					 nr_seq_lote_p		number)
                        return varchar2 is

ds_horarios_w           varchar2(255);
dt_horario_w            varchar2(255);
ie_acm_w		varchar2(255);
ie_se_necessario_w	varchar2(255);
ds_horarios_ww		varchar2(4000);

cursor c01 is
select  to_char(min(a.dt_horario),'hh24:mi')
from    prescr_mat_hor a,
	prescr_material x
where   a.nr_prescricao   = nr_prescricao_p
and	x.nr_prescricao	= a.nr_prescricao
and	x.nr_sequencia	= a.nr_seq_material
and     a.nr_seq_material = nr_seq_material_p
and	a.nr_seq_lote	= nr_seq_lote_p
order by a.dt_horario;

begin

open C01;
loop
fetch C01 into
        dt_horario_w;
exit when C01%notfound;
        ds_horarios_w   := ds_horarios_w || ' ' || dt_horario_w;
end loop;
close C01;

return  nvl(ds_horarios_ww, ds_horarios_w);

end Vivalle_Obter_Hor_aprazados;
/
