create or replace
function obter_se_estag_alt(nr_seq_proj_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(2);				
nr_seq_estagio_w	number(10);				
qt_regra_w			number(10);				
begin

ds_retorno_w	:=	'S';

select  max(nr_seq_estagio)
into	nr_seq_estagio_w
from	gpi_projeto
where	nr_sequencia = nr_seq_proj_p;


select  count(*)
into	qt_regra_w
from	gpi_estagio
where	ie_altera_percent_etapa = 'S'
and	nr_sequencia = nr_seq_estagio_w;

if  (qt_regra_w > 0) then

	select  nvl(max(ie_altera_percent_etapa),'N')
	into   	ds_retorno_w
	from	gpi_estagio
	where	nr_sequencia = nr_seq_estagio_w;
	
end if;

return	ds_retorno_w;

end obter_se_estag_alt;
/
