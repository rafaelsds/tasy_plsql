create or replace 
Function Obter_Desc_classif_tnm(	nr_sequencia_p		Number) 
					return varchar2 is

ds_retorno_w	Varchar2(100);

begin

if	(nr_sequencia_p is not null) then
	select	ds_classificacao
	into	ds_retorno_w
	from	can_tnm_classif
	where	nr_sequencia = nr_sequencia_p;
end if;

return ds_retorno_w;

end Obter_Desc_classif_tnm;
/