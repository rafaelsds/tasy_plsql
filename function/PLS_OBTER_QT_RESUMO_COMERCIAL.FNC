create or replace
function pls_obter_qt_resumo_comercial
		(	ie_dimensao_p		Varchar2,
			dt_mes_referencia_p	Date,
			ie_opcao_p		varchar2)
 		    	return number is
			
qt_retorno_w		number(10);

begin

if	(ie_opcao_p = 'D') then
	select	qt_registros
	into	qt_retorno_w
	from	w_pls_resumo_comercial
	where	dt_referencia	= dt_mes_referencia_p
	and	ie_dimensao	= ie_dimensao_p;
elsif	(ie_opcao_p = 'T') then
	select	sum(qt_registros)
	into	qt_retorno_w
	from	w_pls_resumo_comercial
	where	dt_referencia	= dt_mes_referencia_p;
end if;	

return	qt_retorno_w;

end pls_obter_qt_resumo_comercial;
/
