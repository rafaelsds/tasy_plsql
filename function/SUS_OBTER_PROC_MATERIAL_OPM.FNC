create or replace
function sus_obter_proc_material_opm (	cd_material_p	material.cd_material%type)
					return			number is
					
cd_procedimento_w	procedimento.cd_procedimento%type;

begin

select	max(cd_procedimento)
into	cd_procedimento_w
from	sus_material_opm
where	cd_material	= cd_material_p;

return cd_procedimento_w;

end sus_obter_proc_material_opm;
/
