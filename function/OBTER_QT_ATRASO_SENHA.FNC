create or replace
function obter_qt_atraso_senha (	nr_seq_fila_espera_p	number,
				ie_opcao_p		varchar2,
				nr_sequencia_p		number default 0,
				dt_data_atraso_p	date default null)
 		    	return number is

/*ie_opcao_p
MAX - tempo limite maximo
MIN - tempo limite minimo
*/
qt_diferenca_w			number(20,10);
qt_tempo_limite_max_w	number(20);
qt_tempo_limite_min_w	number(20);
ie_regra_tempo_espera_w	varchar2(1);

				
begin

select  nvl(max(Obter_Valor_Param_Usuario(10021, 77, Obter_Perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento)), 'S')
into	ie_regra_tempo_espera_w
from 	dual;

select	nvl(max(qt_tempo_maximo),0),
	nvl(max(qt_tempo_medio),0)
into	qt_tempo_limite_max_w,
	qt_tempo_limite_min_w
from 	fila_espera_senha
where	nr_sequencia = nr_seq_fila_espera_p;

if (ie_opcao_p = 'MAX') then
	begin
	SELECT /*+INDEX (a PACSEFI_FILESPS_FK_I) */ max(((nvl(dt_data_atraso_p,sysdate) - DECODE(ie_regra_tempo_espera_w, 'F', NVL(dt_entrada_fila, dt_geracao_senha), dt_geracao_senha)) * 1440) - qt_tempo_limite_max_w)
	into	qt_diferenca_w
	FROM 	paciente_senha_fila a
	WHERE	(NVL(ie_rechamada,'N') = 'N')
	AND	dt_utilizacao	IS NULL
	AND	dt_inutilizacao IS NULL
	AND	dt_inicio_atendimento IS NULL
	AND	dt_fim_atendimento IS NULL
	AND	NVL(dt_entrada_fila, dt_geracao_senha) > SYSDATE - 2
	and 	nr_seq_fila_senha = nr_seq_fila_espera_p
	and 	((nr_sequencia_p = 0) or (nr_sequencia = nr_sequencia_p))
	ORDER BY dt_geracao_senha DESC;
	end;
	
elsif (ie_opcao_p = 'MIN') then
	begin
	SELECT /*+INDEX (a PACSEFI_FILESPS_FK_I) */ max(((nvl(dt_data_atraso_p,sysdate) - DECODE(ie_regra_tempo_espera_w, 'F', NVL(dt_entrada_fila, dt_geracao_senha), dt_geracao_senha)) * 1440) - qt_tempo_limite_min_w)
	into	qt_diferenca_w
	FROM 	paciente_senha_fila a
	WHERE	(NVL(ie_rechamada,'N') = 'N')
	AND	dt_utilizacao	IS NULL
	AND	dt_inutilizacao IS NULL
	AND	dt_inicio_atendimento IS NULL
	AND	dt_fim_atendimento IS NULL
	AND	NVL(dt_entrada_fila, dt_geracao_senha) > SYSDATE - 2
	and 	nr_seq_fila_senha = nr_seq_fila_espera_p
	and 	((nr_sequencia_p = 0) or (nr_sequencia = nr_sequencia_p))
	ORDER BY dt_geracao_senha DESC;
	end;
end if;
 
if (qt_diferenca_w < 0) then
	qt_diferenca_w	:= 0;
end if;

return	qt_diferenca_w;

end obter_qt_atraso_senha;
/