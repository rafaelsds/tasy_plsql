create or replace function san_obter_fenotipagem_pf	(cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ds_fenotipagem_w		varchar2(255) := null;
begin
if	(cd_pessoa_fisica_p is not null) then

	select	max(san_obter_result_fenotipagem(c.nr_sequencia))
	into	ds_fenotipagem_w
	from	san_result_fenotipagem c,
		san_transfusao b,
		atendimento_paciente a
	where	1 = 1
	and	c.nr_seq_transfusao    = b.nr_sequencia
	and	b.nr_atendimento       = a.nr_atendimento
	and	a.cd_pessoa_fisica     = cd_pessoa_fisica_p 
    and c.dt_liberacao is not null
    and c.dt_inativacao is null;

	if	(ds_fenotipagem_w is null) then
		select	max(san_obter_result_fenotipagem(b.nr_sequencia))
		into	ds_fenotipagem_w
		from	san_result_fenotipagem b,
			san_reserva a
		where	1 = 1
		and	b.nr_seq_reserva       = a.nr_sequencia
		and	a.cd_pessoa_fisica     = cd_pessoa_fisica_p
        and b.dt_liberacao is not null
        and b.dt_inativacao is null;
	end if;

	if	(ds_fenotipagem_w is null) then
		select	max(san_obter_result_fenotipagem(b.nr_sequencia))
		into	ds_fenotipagem_w
		from	san_result_fenotipagem b,
			san_doacao a
		where	1 = 1
		and	b.nr_seq_doacao	       = a.nr_sequencia
		and	a.cd_pessoa_fisica     = cd_pessoa_fisica_p
        and b.dt_liberacao is not null
        and b.dt_inativacao is null;
	end if;

end if;

return	ds_fenotipagem_w;

end san_obter_fenotipagem_pf;
/