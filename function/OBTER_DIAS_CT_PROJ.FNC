CREATE OR REPLACE FUNCTION OBTER_DIAS_CT_PROJ(
 IE_TAMANHO_P VARCHAR2)
RETURN NUMBER IS
  PR_RETORNO_W	NUMBER  := 0;
BEGIN
  IF (IE_TAMANHO_P IS NULL) THEN
    PR_RETORNO_W := 0;
  ELSIF (IE_TAMANHO_P = 'XS') THEN
    PR_RETORNO_W := 1;
  ELSIF (IE_TAMANHO_P = 'S') THEN
    PR_RETORNO_W := 2;
  ELSIF (IE_TAMANHO_P = 'M') THEN
    PR_RETORNO_W := 5;
  ELSIF (IE_TAMANHO_P = 'L') THEN
    PR_RETORNO_W := 9;
  ELSIF (IE_TAMANHO_P = 'XL') THEN
    PR_RETORNO_W := 15;
  END IF;    

  RETURN	PR_RETORNO_W;

END OBTER_DIAS_CT_PROJ;
/