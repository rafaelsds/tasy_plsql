create or replace
function obter_ocorrencias_horarios_rep (	ds_horarios_p varchar2)
						return number is

ds_das_w 		varchar2(255 char) := obter_texto_tasy(439834, wheb_usuario_pck.get_nr_seq_idioma);
ds_as_w			varchar2(50 char) := obter_texto_tasy(439836, wheb_usuario_pck.get_nr_seq_idioma);
ds_horarios_w 	varchar2(2000 char);
hr_horario_w 	varchar2(7 char);
nr_ocorrencia_w number(15,4) := 0;
i  				pls_integer;

begin

if 	(ds_horarios_p is not null) then
	begin
		if	(instr(ds_horarios_p, ds_das_w) > 0) and
			(instr(ds_horarios_p, ds_as_w) > 0) then
			-- Este replace foi adicionado, para normalizar 1 espa�o entre os hor�rios (O m�ximo de espa�amento pode ser 5) {Par�metro [1033] da REP}
			ds_horarios_w	:= replace(replace(replace(ds_horarios_p, '   ', ' '), '  ', ' '), '  ', ' ');
			ds_horarios_w	:= replace(replace(ds_horarios_w, ';', ' e') || ' e',' e e',' e');
					   
			
			while	substr(ds_horarios_w,1,1) = ' ' loop 
				begin
				ds_horarios_w	:= substr(ds_horarios_w,2,length(ds_horarios_w));
				end;
			end loop;
			
			while (ds_horarios_w is not null) loop
				begin
				select instr(ds_horarios_w, ' e')
				into i
				from  dual;

				if	(i > 1) and
					(substr(ds_horarios_w, 1, i-1) is not null) then
					ds_horarios_w 	:= substr(ds_horarios_w, i+3, 2000);

					nr_ocorrencia_w := nr_ocorrencia_w + 1;

				elsif (ds_horarios_w is not null) then
					ds_horarios_w := '';
				end if;
				end;
			end loop;		
		else
			-- Este replace foi adicionado, para normalizar 1 espa�o entre os hor�rios (O m�ximo de espa�amento pode ser 5) {Par�metro [1033] da REP}
			ds_horarios_w	:= replace(replace(replace(ds_horarios_p, '   ', ' '), '  ', ' '), '  ', ' ');
			ds_horarios_w	:= ds_horarios_w || ' ';	

			while	substr(ds_horarios_w,1,1) = ' ' loop 
				begin
				ds_horarios_w	:= substr(ds_horarios_w,2,length(ds_horarios_w));
				end;
			end loop;
			
			
			while (ds_horarios_w is not null) loop
				begin
				select instr(ds_horarios_w, ' ')
				into i
				from  dual;

				if	(i > 1) and
					(substr(ds_horarios_w, 1, i-1) is not null) then
					hr_horario_w	:= substr(ds_horarios_w, 1, i-1);
					hr_horario_w 	:= replace(hr_horario_w, ' ', '');
					ds_horarios_w 	:= substr(ds_horarios_w, i+1, 2000);

					nr_ocorrencia_w := nr_ocorrencia_w + 1;

				elsif (ds_horarios_w is not null) then
					ds_horarios_w := '';
				end if;
				end;
			
			end loop;
		end if;
	exception
	when others then
		nr_ocorrencia_w := 0;	
	end;
end if;

return nr_ocorrencia_w;

end obter_ocorrencias_horarios_rep;
/
