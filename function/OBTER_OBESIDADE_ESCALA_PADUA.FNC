create or replace function obter_obesidade_escala_padua(nr_atendimento_p		number)
				return varchar2 is
			
ds_retorno_w		varchar2(1);
qt_imc_w	number(4);
sql_w     varchar2(300);
begin
	ds_retorno_w := 'N';

	begin
		select nvl(qt_imc,0) into qt_imc_w
		from (
			select qt_imc from atendimento_sinal_vital 
			where nr_atendimento = nr_atendimento_p
			and qt_imc is not null
			order by dt_sinal_vital desc
		) where rownum = 1;
	exception
		when NO_DATA_FOUND then 
    begin
			qt_imc_w := 0;
		end;
	end;
	
      begin
      sql_w := 'CALL OBTER_OBESIDADE_MD(:1) INTO :ds_retorno_w';
      EXECUTE IMMEDIATE sql_w USING IN qt_imc_w,
                                    OUT ds_retorno_w;       
        exception
          when others then
            ds_retorno_w := null;
        end;
  
	return ds_retorno_w;

end obter_obesidade_escala_padua;
/
