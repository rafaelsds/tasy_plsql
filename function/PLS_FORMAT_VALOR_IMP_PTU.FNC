create or replace
function pls_format_valor_imp_ptu(	ds_valor_p		varchar2 )
					return number is

vl_retorno_w		number(15,2);

begin

begin	
vl_retorno_w := to_number(substr(ds_valor_p,1, (length(ds_valor_p) - 2))||','||substr(ds_valor_p,(length(ds_valor_p) - 1), 15));
exception
when others then
	vl_retorno_w := null;
end;

return vl_retorno_w;

end pls_format_valor_imp_ptu;
/