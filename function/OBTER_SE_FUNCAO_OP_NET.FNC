create or replace
function obter_se_funcao_op_net(	cd_funcao_p		number)
 		    	return varchar2 is
				
ds_retorno_w		varchar2(1);

begin
	ds_retorno_w := 'N';
	if (cd_funcao_p is not null) then
		select	nvl(max('S'),'N') 
		into	ds_retorno_w
		from	funcao
		where	cd_funcao = cd_funcao_p
		and	cd_funcao in (	1361,
					1374,
					1204,
					1333,
					1240,
					1330,
					1362,
					1385,
					1291,
					1270,
					1336,
					1359,
					1219,
					1266,
					1271,
					1398,
					1367,
					1368,
					1313,
					1353,
					1280,
					1236,
					1339,
					1234,
					1237,
					1278,
					1206,
					347,
					1232,
					1347,
					1233,
					1317,
					1365,
					1348,
					1349,
					1211,
					244,
					1371,
					1210,
					1283,
					1341,
					1388,
					1238,
					1285,
					1229,
					1326,
					1387,
					1284,
					1208,
					1279,
					1321,
					1306,
					1221,
					1363,
					1315,
					107,
					1282,
					1203,
					1314,
					1308,
					1310,
					1309,
					1383,
					1378,
					337,
					1322,
					1298,
					1213,
					1212,
					1296,
					1265,
					1201,
					1209,
					1220,
					1243,
					1226,
					1235,
					1267,
					1307,
					1215,
					1357,
					1202,
					1225,
					209,
					268,
					1273,
					1260,
					1216,
					1345,
					1346,
					1344,
					1360,
					1269,
					1222,
					1287,
					1320,
					1288,
					1354,
					1242,
					110,
					1337,
					210,
					1244,
					1268,
					1264,
					1227,
					1370,
					1328,
					343,
					1230,
					1386,
					1259,
					1380,
					1375,
					1338,
					1214,
					280,
					1231,
					1239,
					1369,
					1389,
					1382,
					353,
					1324,
					1241,
					1372,
					1356,
					1384,
					261,
					1228,
					1205,
					336,
					1207,
					1262,
					1245,
					1343,
					349,
					1275,
					1318,
					1319,
					1293,
					1277,
					1334,
					1281,
					1261,
					78,
					1312,
					1297,
					1366,
					1364,
					262,
					1286,
					1323,
					1292,
					1311,
					1305,
					1352,
					1393,
					1390,
					1340,
					1327,
					1316,
					1395,
					135,
					40,
					851,
					801,
					1120,
					911,
					5,
					6
					);
	end if;

	return	ds_retorno_w;

end obter_se_funcao_op_net;
/
