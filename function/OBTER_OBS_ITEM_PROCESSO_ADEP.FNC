create or replace
function obter_obs_item_processo_adep	(nr_prescricao_p	number,
					nr_seq_material_p	number,
					nr_seq_horario_p	number)
					return varchar2 is

ds_observacao_w	varchar2(255);

begin

if	(nr_seq_horario_p is not null) then

	ds_observacao_w := null;

elsif	(nr_prescricao_p is not null) and
	(nr_seq_material_p is not null) then

	select	obter_dados_material_prescr(nr_prescricao_p,nr_seq_material_p,'DS_OBSERVACAO')
	into	ds_observacao_w
	from	dual;

end if;

return ds_observacao_w;

end obter_obs_item_processo_adep;
/