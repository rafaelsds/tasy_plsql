create or replace
function EPIMED_obter_data_entrada(	nr_atendimento_p		number)
				return varchar2 is

data_entrada_w 		varchar2(50);

begin

select 	to_char(max(b.dt_entrada_unidade), 'YYYY-MM-DD"T"HH24:MI:SS."000"')
into	data_entrada_w
from	atend_paciente_unidade b 
where 	nr_atendimento = nr_atendimento_p;


return	data_entrada_w;

end EPIMED_obter_data_entrada;
/