create or replace
function HSM_consiste_mat_plano_conv
			(cd_convenio_p				number,
			cd_plano_p				varchar2,
			cd_material_p				number,
			nr_atendimento_p			number,
			cd_setor_atendimento_p		number,
			ie_regra_p				varchar2,
			qt_material_p				number,
			dt_atendimento_p		 	date,
			nr_seq_agenda_p				number,
			cd_estabelecimento_p		 	number,
			cd_categoria_p			 	varchar2,
			ie_tipo_atendimento_p 		 	varchar2) return varchar2 is


ie_regra_w			number(02,0) 	:= 0;
ds_retorno_w			varchar2(255)	:= '';
ie_bloqueia_agenda_w		varchar2(01)	:= 'N';
cd_grupo_material_w		number(08,0)	:= 0;
cd_subgrupo_material_w		number(08,0)	:= 0;
cd_classe_material_w		number(08,0)	:= 0;
ie_tipo_atendimento_w		number(03,0);
cd_funcao_ativa_w			number(10,0)	:= 0;
ie_valor_w			varchar2(5);
vl_material_min_w			number(15,4);
vl_material_max_w			number(15,4);
cd_categoria_w			varchar2(10);
cd_estabelecimento_w		number(5,0);
cont_w				number(5,0);
cd_tipo_acomodacao_w		number(4);
ds_observacao_w			Varchar2(255):= '';
nr_seq_regra_w			number(10);
cd_classif_setor_w			varchar2(10);
ds_erro_autor_w			varchar2(255) := '';
ds_aviso_w			varchar2(255) := '';
vl_referencia_w			number(15,2)	:= 0;
dt_ult_vigencia_w			date;
cd_tab_preco_mat_w		number(10,0);
ie_origem_preco_w			number(10,0);
ie_carater_inter_sus_w		varchar2(2);
ie_clinica_w			number(5);
cd_usuario_convenio_w		varchar2(30);
cd_doenca_atend_w		varchar2(10);
qt_utilizada_w			number(15,2);
qt_minima_w			number(15,3);
qt_maxima_w			number(15,3);
ie_autor_generico_w		varchar2(1);
cd_material_w			number(6);
cd_material_preco_w		number(15);
cd_empresa_conv_w		number(15);
ie_mat_conta_w			varchar2(255);
ie_consignado_w			varchar2(01);
ie_cobertura_w			varchar2(1):= 'N';
nr_seq_cobertura_w		number(10,0);
nr_seq_bras_preco_w		number(10,0);
nr_seq_mat_bras_w		number(10,0);
nr_seq_conv_bras_w		number(10,0);
nr_seq_conv_simpro_w		number(10,0);
nr_seq_mat_simpro_w		number(10,0);
nr_seq_simpro_preco_w		number(10,0);
nr_seq_ajuste_mat_w		number(10,0);
ie_tipo_material_w		varchar2(3);
nr_seq_classif_atend_w		atendimento_paciente.nr_seq_classificacao%type;


CURSOR C01 IS
select	ie_regra,
	ie_valor,
	nvl(vl_material_min, 0),
	nvl(vl_material_max, 0),
	nr_sequencia,
	nvl(qt_minima,0),
	nvl(qt_maxima,999),
	ie_mat_conta
from 	regra_Convenio_Plano_mat
where	cd_convenio							= cd_convenio_p
and 	((nvl(cd_plano, cd_plano_p) 					= cd_plano_p) or
	 (nvl(cd_plano_p, 'X') = 'X'))
and	nvl(cd_material,cd_material_w)					= cd_material_w
and	nvl(cd_grupo_material,cd_grupo_material_w)			= cd_grupo_material_w	
and	nvl(cd_subgrupo_material,cd_subgrupo_material_w)		= cd_subgrupo_material_w
and	nvl(cd_classe_material,cd_classe_material_w)			= cd_classe_material_w
and	nvl(ie_tipo_atendimento, nvl(nvl(ie_tipo_atendimento_p,ie_tipo_atendimento_w),0)) 		= nvl(nvl(ie_tipo_atendimento_p, ie_tipo_atendimento_w),0)
and	nvl(cd_classif_setor, nvl(cd_classif_setor_w,'X')) 		= nvl(cd_classif_setor_w,'X')
and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_p,0))	= nvl(cd_setor_atendimento_p,0)
and	nvl(cd_tipo_acomodacao, nvl(cd_tipo_acomodacao_w,0)) 		= nvl(cd_tipo_acomodacao_w,0)
and	nvl(ie_carater_inter_sus,nvl(ie_carater_inter_sus_w,0))		= nvl(ie_carater_inter_sus_w,0)
and	nvl(ie_clinica,nvl(ie_clinica_w,0))				= nvl(ie_clinica_w,0)
and	nvl(obter_se_carteira_mascara(ds_mascara_carteira,cd_usuario_convenio_w),'S') = 'S'
and	(nvl(dt_atendimento_p,sysdate) between 
	 nvl(dt_inicio_vigencia,to_date('01/01/1900','dd/mm/yyyy')) and 
	 nvl(dt_fim_vigencia,to_date('31/12/2099','dd/mm/yyyy')))
and	(cd_estabelecimento = cd_estabelecimento_p or cd_estabelecimento is null)
and	nvl(cd_doenca,nvl(cd_doenca_atend_w,'X'))			= nvl(cd_doenca_atend_w,'X')
and	nvl(cd_empresa_conv, nvl(cd_empresa_conv_w,0))			= nvl(cd_empresa_conv_w,0)
and	nvl(ie_consignado, nvl(ie_consignado_w,'X'))			= nvl(ie_consignado_w,'X')
and	nvl(cd_categoria, nvl(cd_categoria_p, nvl(cd_categoria_w,'0')))	= nvl(cd_categoria_p, nvl(cd_categoria_w,'0'))
--and 	((ie_cobertura_w = 'N') or ((ie_cobertura_w = 'S') and (nvl(nr_seq_cobertura,nvl(nr_seq_cobertura_w,0)) = nvl(nr_seq_cobertura_w,0))))
and 	nvl(ie_tipo_material, nvl(ie_tipo_material_w,'0')) = nvl(ie_tipo_material_w,'0')
order by	nvl(ds_mascara_carteira,'X'), 
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0),
	nvl(ie_tipo_material, '0'),
	nvl(cd_tipo_acomodacao,0),
	nvl(cd_setor_atendimento,0),
	nvl(cd_classif_setor, 'X'),
	nvl(cd_categoria_w,'0'),
	nvl(ie_tipo_atendimento,0),
	nvl(ie_carater_inter_sus,'X'),
	nvl(ie_clinica,0),
	nvl(cd_empresa_conv,0);
	
/*Cursor	c02 is
	select 	b.nr_sequencia
	from 	convenio_cobertura_regra a,
		convenio_cobertura b
	where 	b.nr_sequencia = a.nr_seq_cobertura
	and 	b.cd_convenio = cd_convenio_p
	and 	ie_cobertura_w = 'S'
	and 	nvl(ie_tipo_atendimento, nvl(nvl(ie_tipo_atendimento_p,ie_tipo_atendimento_w),0)) = nvl(nvl(ie_tipo_atendimento_p,ie_tipo_atendimento_w),0)
	and 	nvl(cd_plano, nvl(cd_plano_p,'0')) = nvl(cd_plano_p,'0')
	and 	nvl(a.ie_situacao,'A') = 'A'
	order by nvl(ie_tipo_atendimento,0),
		nvl(cd_plano,'0'),
		a.nr_seq_cobertura;*/

begin

cd_material_w	:= cd_material_p;

/*lhalves OS 219697 em 01/06/2010*/
ie_consignado_w	:= Obter_se_mat_Consignado(cd_material_w);

ie_cobertura_w		:= 'N';

/*select 	nvl(max(ie_cobertura),'N')
into	ie_cobertura_w
from 	convenio
where 	cd_convenio = cd_convenio_p;*/

select	count(*)
into	cont_w
from	regra_Convenio_Plano_mat
where	cd_convenio	= cd_convenio_p;

if	(cont_w > 0) then
	
	select	nvl(max(ie_autor_generico),'N')
	into	ie_autor_generico_w
	from	convenio_estabelecimento
	where	cd_convenio		= cd_convenio_p
	and	cd_estabelecimento	= cd_estabelecimento_p;

	if	(ie_autor_generico_w = 'S') then
		cd_material_w	:= obter_material_generico(cd_material_p);
	end if;

	select nvl(max(ie_tipo_material),'0')
	into	ie_tipo_material_w
	from 	material
	where 	cd_material = cd_material_p;

	select	nvl(max(cd_classif_setor),'X')
	into	cd_classif_setor_w
	from	setor_atendimento
	where	cd_setor_atendimento		= cd_setor_atendimento_p;

	if	(nr_atendimento_p is not null) then

		select	nvl(max(ie_tipo_atendimento),0),
			nvl(max(cd_estabelecimento),0),
			nvl(max(ie_carater_inter_sus),0),
			nvl(max(ie_clinica),0),
			nvl(max(obter_cid_atendimento(nr_atendimento,'P')),'X'),
			max(nr_seq_classificacao)
		into	ie_tipo_atendimento_w,
			cd_estabelecimento_w,
			ie_carater_inter_sus_w,
			ie_clinica_w,
			cd_doenca_atend_w,
			nr_seq_classif_atend_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_p;

		select	max(cd_categoria),
			nvl(max(cd_tipo_acomodacao),0),
			max(cd_usuario_convenio),
			max(cd_empresa)
		into	cd_categoria_w,
			cd_tipo_acomodacao_w,
			cd_usuario_convenio_w,
			cd_empresa_conv_w
		from	atend_categoria_convenio
		where	nr_atendimento	= nr_atendimento_p
		and	cd_convenio	= cd_convenio_p
		and	obter_atecaco_atendimento(nr_atendimento)	= nr_seq_interno;

	elsif	(nr_seq_agenda_p is not null) then

		select	nvl(max(a.ie_tipo_atendimento),0),
			nvl(max(b.cd_estabelecimento),0),
			nvl(max(a.cd_categoria),0),
			nvl(max(a.cd_tipo_acomodacao),0),
			max(a.cd_usuario_convenio)
		into	ie_tipo_atendimento_w,
			cd_estabelecimento_w,
			cd_categoria_w,
			cd_tipo_acomodacao_w,
			cd_usuario_convenio_w
		from	agenda b,
			agenda_paciente a
		where	a.cd_agenda	= b.cd_agenda
		and	a.nr_sequencia	= nr_seq_agenda_p;
	end if;


	/* Obter Estrutura do material */
	begin
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from	Estrutura_material_v
	where	cd_material	 	= nvl(cd_material_w,0)
	and	cd_material_w		is not null;
	exception when others then
		begin
		cd_grupo_material_w	:= 0;
		cd_subgrupo_material_w	:= 0;
		cd_classe_material_w	:= 0;
	end;
	end;
	
	-- Cobertura
	/*nr_seq_cobertura_w:= 0;
	open C02;
	loop
	fetch C02 into	
		nr_seq_cobertura_w;
	exit when C02%notfound;
		begin
		nr_seq_cobertura_w:=  nr_seq_cobertura_w;
		end;
	end loop;
	close C02;*/	

	open c01;
	loop
	fetch c01 into 
		ie_regra_w,
		ie_valor_w,
		vl_material_min_w,
		vl_material_max_w,
		nr_seq_regra_w,
		qt_minima_w,
		qt_maxima_w,
		ie_mat_conta_w;
	exit when c01%notfound;

		ie_regra_w	:=  ie_regra_w;

		if	(ie_valor_w in ('S','F')) then
			/* Francisco - OS 74123 - 12/11/07 - Com a function de antes nao buscava o preco correto conforme a consuolta preco
			troquei "obter_preco_mat_conv(cd_estabelecimento_w, cd_convenio_p,cd_categoria_w,cd_material_w,sysdate) pela proc abaixo */

			select	max(decode(nvl(ie_mat_conta_w,'N'), 'N', cd_material_w, obter_material_conta(cd_estabelecimento_p, cd_convenio_p, nvl(cd_categoria_p, cd_categoria_w), cd_material_w, cd_material_w, cd_material_w, cd_plano_p, cd_setor_atendimento_p, dt_atendimento_p, 0, 0)))
			into	cd_material_preco_w
			from	dual;

			-- dsantos em 25/03/2010, OS 203809. acompanhado por Ricardo, autorizado por Marcus.
			if	(nvl(cd_categoria_p,0) = 0) and (nvl(cd_categoria_w,0) = 0) then
				select	min(cd_categoria)
				into	cd_categoria_w
				from	categoria_convenio
				where	cd_convenio	= cd_convenio_p
				and	ie_situacao	= 'A';
			end if;

			define_preco_material(	nvl(cd_estabelecimento_p, cd_estabelecimento_w),
						cd_convenio_p,
						nvl(cd_categoria_p, cd_categoria_w),
						sysdate,
						cd_material_preco_w,
						cd_tipo_acomodacao_w,
						nvl(ie_tipo_atendimento_p, ie_tipo_atendimento_w),
						0,
						'',
						0,
						0,
						cd_plano_p,
						null,
						null,
						null,
						null,
						nr_seq_classif_atend_w,
						null,
						null,
						vl_referencia_w,
						dt_ult_vigencia_w,
						cd_tab_preco_mat_w,
						ie_origem_preco_w,
						nr_seq_bras_preco_w,
						nr_seq_mat_bras_w,
						nr_seq_conv_bras_w,
						nr_seq_conv_simpro_w,
						nr_seq_mat_simpro_w,
						nr_seq_simpro_preco_w,
						nr_seq_ajuste_mat_w);


		end if;

		if	(ie_valor_w = 'S') then		-- se consiste valor
			
			if	(vl_referencia_w between vl_material_min_w and vl_material_max_w) then
				ie_regra_w	:= null;
				nr_seq_regra_w	:= null;
			end if;
		elsif	(ie_valor_w = 'F') then -- Francisco - 10/04/07 - Aplicar regra quando est� na faixa de valores

			if	not(vl_referencia_w between vl_material_min_w and vl_material_max_w) then
				ie_regra_w	:= null;
				nr_seq_regra_w	:= null;
			end if;	
		end if;
								

	end loop;
	close c01;

	/* Bloqueio Atendimento (Quando a Chamada � no Atendimento: Proc = null) */
	if	(ie_regra_w in (1,2)) then
		begin
		ds_retorno_w 		:= 'Este material n�o � autorizado para este conv�nio';
		ie_bloqueia_agenda_w	:= 'S';
		end;
	/* Libera com Autoriza��o */
	elsif	(ie_regra_w = 3) then
		ds_retorno_w 		:= 'Este material deve ter autoriza��o especial';

		
		/* Francisco - OS 103531 - 06/08/2008 - Tratar qt autorizada/utilizada */
		consiste_autorizacao_mat(nr_atendimento_p,
					cd_convenio_p,
					dt_atendimento_p,
					cd_material_w,
					qt_material_p,
					null,
					ds_aviso_w,
					ds_erro_autor_w);
	
		if	(ds_erro_autor_w is null) then
			ie_regra_w	:= null;
			ds_retorno_w	:= null;
		end if;

		/* Tratar qtd min e max - OS 118922 */

		if	(qt_minima_w > 0) or
			(qt_maxima_w < 999) then
			select	nvl(sum(qt_material),0)
			into	qt_utilizada_w
			from	material_atend_paciente
			where	nr_atendimento 		= nr_atendimento_p
			and	cd_convenio		= cd_convenio_p
			and	cd_material		= cd_material_w
			and	cd_motivo_exc_conta	is null;

			if	(qt_utilizada_w + nvl(qt_material_p,1) < qt_minima_w) or
				(qt_utilizada_w + nvl(qt_material_p,1) > qt_maxima_w) then
				ie_regra_w	:= null;
				ds_retorno_w	:= null;
			end if;									
		end if;
		
		/* Fim 26/01/2009 */

	/* Bloqueio previstos na cirurgia */
	elsif	(ie_regra_w = 5) then
		ds_retorno_w 		:= 'Este material n�o � autorizado para este conv�nio';

	/* Bloqueia sem autoriza��o */
	elsif	(ie_regra_w = 8) then
		consiste_autorizacao_mat(nr_atendimento_p,
					cd_convenio_p,
					dt_atendimento_p,
					cd_material_w,
					qt_material_p,
					null,
					ds_aviso_w,
					ds_erro_autor_w);
	
		if	(ds_erro_autor_w is null) then
			ie_regra_w	:= null;
		else
			ie_bloqueia_agenda_w	:= 'S';	
		end if;

		ds_retorno_w	:= ds_retorno_w || ds_erro_autor_w || ds_aviso_w;

		/* Tratar qtd min e max - OS 118922 */

		if	(qt_minima_w > 0) or
			(qt_maxima_w < 999) then
			select	nvl(sum(qt_material),0)
			into	qt_utilizada_w
			from	material_atend_paciente
			where	nr_atendimento 		= nr_atendimento_p
			and	cd_convenio		= cd_convenio_p
			and	cd_material		= cd_material_w
			and	cd_motivo_exc_conta	is null;

			if	(qt_utilizada_w + nvl(qt_material_p,1) < qt_minima_w) or
				(qt_utilizada_w + nvl(qt_material_p,1) > qt_maxima_w) then
				ie_regra_w		:= null;
				ds_retorno_w		:= null;
				ie_bloqueia_agenda_w	:= 'N';	
			end if;									
		end if;
		
		/* Fim 26/01/2009 */
	end if;

end if; 

return ie_regra_w;

end HSM_consiste_mat_plano_conv;
/
