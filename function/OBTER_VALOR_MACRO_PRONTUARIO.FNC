create or replace function obter_valor_macro_prontuario (
			nm_macro_p	varchar2, 
      cd_funcao_p number default 6001)
			return varchar2 is

nm_atributo_w		varchar2(50);
ds_macro_cliente_w		varchar2(255);

begin
if	(nm_macro_p is not null) then
	begin	
	select	max(m.nm_macro)
	into	ds_macro_cliente_w
	from	macro_prontuario m,
		funcao_macro_cliente c,
		funcao_macro f
	where	f.cd_funcao = cd_funcao_p
	and	f.nr_sequencia = c.nr_seq_macro
	and	c.ds_macro = nm_macro_p
	and	m.nm_macro = f.ds_macro;

	select	max(nm_atributo)
	into	nm_atributo_w
	from	macro_prontuario
	where	upper(nm_macro) = upper(nvl(ds_macro_cliente_w,nm_macro_p));
	
	end;
end if;
return nm_atributo_w;
end obter_valor_macro_prontuario;
/