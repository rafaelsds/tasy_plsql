create or replace
function san_obter_doador(	nr_Seq_Doacao_p		number,
				ie_opcao_p		varchar2) 
				return varchar2 is

/*
Opcao
C - C�digo
N - Nome
*/				
				
cd_pessoa_Fisica_w	Varchar2(10);				
ds_retorno_w		varchar2(255);
				
begin

select	cd_pessoa_fisica
into	cd_pessoa_Fisica_w
from	san_doacao a
where	a.nr_Sequencia = nr_seq_doacao_p;

if (ie_opcao_p = 'C') then
	ds_Retorno_w := cd_pessoa_Fisica_w;
elsif (ie_opcao_p = 'N') then
	ds_Retorno_w := obter_nome_pf(cd_pessoa_Fisica_w);
end if;

return ds_retorno_w;

end san_obter_doador;
/