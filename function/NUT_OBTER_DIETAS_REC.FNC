create or replace
function nut_obter_dietas_rec(	nr_seq_receita_p	Number)
 		    	return varchar is

ds_dietas_w	Varchar(250);
qt_registros_w	number(5);
			
begin

select  count(*) qt_registros
into 	qt_registros_w
from   	nut_dieta_rec  
where  	nr_seq_receita = nr_seq_receita_p  
and  	cd_dieta is not null;

if	(nr_seq_receita_p is not null) then
	if (qt_registros_w > 1) then 

		SELECT SUBSTR(LTRIM(sys_connect_by_path(ds_dieta, ','),','),1,250) nr_seq_evento
		into ds_dietas_w
		FROM (
			 SELECT SUBSTR(obter_nome_dieta(cd_dieta),1,200) ds_dieta,
				row_number() over (ORDER BY nr_sequencia) AS fila 
			  FROM   NUT_DIETA_REC  
		      WHERE  nr_seq_receita = nr_seq_receita_p   AND  cd_dieta IS NOT NULL)
		WHERE connect_by_isleaf = 1
		START WITH fila = 1 
		CONNECT BY PRIOR fila = (fila-1);	
	else 
		select 	substr(obter_nome_dieta(cd_dieta),1,200) ds_dieta 
		into	ds_dietas_w
		from   	nut_dieta_rec  
		where  	nr_seq_receita = nr_seq_receita_p
		and    	cd_dieta is not null;
	end if;
end if;

return	ds_dietas_w;

end nut_obter_dietas_rec;
/