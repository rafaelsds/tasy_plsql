create or replace
function hrf_obter_status_solic(nr_solic_compra_p		number)
				return varchar2 is

dt_liberacao_w		date;
dt_autorizacao_w	date;
dt_baixa_w		date;
nr_seq_motivo_cancel_w	number(10);
ds_retorno_w		varchar2(20);
qt_notas_w		number(10);

begin

select	count(*)
into	qt_notas_w
from	(select	a.nr_nota_fiscal nr_nota
	from	nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_sequencia = b.nr_sequencia
	and	a.nr_solic_compra = nr_solic_compra_p
	and	a.nr_solic_compra is not null);

select	nvl(dt_liberacao,null),
	nvl(dt_autorizacao,null),
	nvl(dt_baixa,null),
	nvl(nr_seq_motivo_cancel,null)
into	dt_liberacao_w,
	dt_autorizacao_w,
	dt_baixa_w,
	nr_seq_motivo_cancel_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

if	(qt_notas_w > 0) then
	ds_retorno_w := 'Comprada';
elsif	(dt_baixa_w is not null) and (nr_seq_motivo_cancel_w is null) then
	ds_retorno_w := 'Baixada';
elsif	(dt_baixa_w is not null) and (nr_seq_motivo_cancel_w is not null) then
	ds_retorno_w := 'Cancelada';
elsif	(dt_autorizacao_w is not null) then
	ds_retorno_w := 'Aprovada';
elsif	(dt_liberacao_w is not null) then
	ds_retorno_w := 'Liberada';
elsif	(dt_liberacao_w is null) then
	ds_retorno_w := 'N�o Liberada';
end if;

return ds_retorno_w;

end hrf_obter_status_solic;
/