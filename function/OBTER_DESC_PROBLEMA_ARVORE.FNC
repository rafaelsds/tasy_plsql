create or replace
function  obter_desc_problema_Arvore (	nr_seq_problema_p	number,
										ie_tipo_p			varchar2)
			return varchar2 is

	
ds_retorno_w		varchar2(4000) := '';
nr_sequencia_w		number(10);
nr_seq_cadastro_w	number(10);

ie_informacao_w		cadastro_problema_apres.ie_informacao%type;

ds_problema_w		lista_problema_pac.ds_problema%type;
dt_inicio_w		lista_problema_pac.dt_inicio%type;
dt_fim_w		lista_problema_pac.dt_fim%type;
ds_complemento_w	lista_problema_pac.ds_complemento%type;
ds_duracao_w		varchar2(255);
ds_intensidade_w	varchar2(255);
ds_status_w			varchar2(255);
ds_nivel_cuidado_w	varchar2(255);


Cursor C01 is
	select	nr_sequencia
	from	cadastro_problema_arvore
	where	ie_tipo = ie_tipo_p
	order by nr_sequencia;
	
Cursor C02 is
	select	ie_informacao
	from	cadastro_problema_apres
	where	nr_seq_cad_problema = nr_seq_cadastro_w
	order by nr_seq_apresentacao;	

begin


If ( nr_seq_problema_p is not null) then

	Select 	max(ds_problema),
			max(obter_tempo_problema_ativo(nr_sequencia)),
			max(dt_inicio),
                        max(dt_fim),
			max(obter_desc_radio_group((SELECT MAX(b.CD_EXP_VALORES) FROM tabela_visao_atributo b WHERE nr_sequencia = 72876 AND NM_ATRIBUTO = 'IE_INTENSIDADE'), ie_intensidade)),
			max(obter_valor_dominio(8221,IE_STATUS)),
			max(ds_complemento),
			max(obter_desc_nivel_atencao(IE_NIVEL_ATENCAO))
	into	ds_problema_w,
			ds_duracao_w,
			dt_inicio_w,
                        dt_fim_w, 
			ds_intensidade_w,
			ds_status_w,
			ds_complemento_w,
			ds_nivel_cuidado_w
	from	lista_problema_pac
	where	nr_sequencia = nr_seq_problema_p;
	
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w;
	exit when C01%notfound;
		begin			
		nr_seq_cadastro_w :=  nr_sequencia_w;
		end;
	end loop;
	close C01;
	
	if ( nr_seq_cadastro_w > 0) then		
		
		open C02;
		loop
		fetch C02 into	
			ie_informacao_w;
		exit when C02%notfound;
			begin
			
			if (ds_retorno_w is not null) then
			
				ds_retorno_w := ds_retorno_w||'; ';
			
			end if;
			
			if (ie_informacao_w = 'PROP') then
			
				ds_retorno_w := substr(ds_retorno_w ||ds_problema_w,1,4000);
				
			elsif (ie_informacao_w = 'DURA') then
			
				ds_retorno_w := substr(ds_retorno_w ||ds_duracao_w,1,4000);
				
			elsif (ie_informacao_w = 'DATI') then
			
				ds_retorno_w := substr(ds_retorno_w ||to_date(dt_inicio_w,'dd/mm/yyyy'),1,4000);
			
                        elsif (ie_informacao_w = 'DAFI') then
			
				ds_retorno_w := substr(ds_retorno_w ||to_date(dt_fim_w,'dd/mm/yyyy'),1,4000);
								
			elsif (ie_informacao_w = 'INTE') then
			
				ds_retorno_w := substr(ds_retorno_w ||ds_intensidade_w,1,4000);
				
			elsif (ie_informacao_w = 'STAT') then
			
				ds_retorno_w := substr(ds_retorno_w ||ds_status_w,1,4000);
				
			elsif (ie_informacao_w = 'COMP') then
			
				ds_retorno_w := substr(ds_retorno_w ||ds_complemento_w,1,4000);
				
			elsif (ie_informacao_w = 'CUID') then
			
				ds_retorno_w := substr(ds_retorno_w ||ds_nivel_cuidado_w,1,4000);
			
			end if;
			
			end;
		end loop;
		close C02;
	
	end if;	
	
	if ( ds_retorno_w is null ) then
	
		ds_retorno_w := substr(ds_problema_w||'; '||ds_duracao_w,1,4000);
	
	end if;


end if;

return ds_retorno_w;

end obter_desc_problema_Arvore;
/
