create or replace 
function HSL_Obter_Glicemia_Exam(nr_atendimento_p	number,
				nr_seq_exame_p		number,
				ie_opcao_p			varchar2)
				return varchar2 is
					
cd_pessoa_fisica_w	varchar2(10);
dt_aprovacao_w	date;


ds_unidade_medida_w	varchar2(100);
ds_resultado_w		varchar2(2000);

ds_retorno_w		varchar2(2000);
cd_estabelecimento_w	number(4);
qt_exame_ant_w		number(5);
qt_result_anterior_w	number(5);
ie_recem_nato_w		varchar2(1);

cursor c01 is
	select	substr(nvl(nvl(decode(c.ds_resultado,'0','',decode(d.ie_formato_resultado,'V','',c.ds_resultado)),	nvl(to_char(c.qt_resultado),to_char(decode(c.pr_resultado,0,'',c.pr_resultado)))),c.ds_resultado),1,100)
	from	exame_laboratorio d,
		exame_lab_result_item c,
		exame_lab_resultado b
	where	b.nr_seq_resultado	= c.nr_seq_resultado
	  and	d.nr_seq_exame 		= c.nr_seq_exame
	  and 	b.nr_atendimento	= nr_atendimento_p
	  and	d.nr_seq_exame		= nr_seq_exame_p
	--  and	ie_mostra_resultado_p 	= 'S'
	order by dt_resultado;
	
cursor c02 is
select c.dt_emissao_resultado
from   result_laboratorio a,
       prescr_procedimento c,
       prescr_medica b
WHERE  a.nr_prescricao     = c.nr_prescricao
AND    a.nr_seq_prescricao = c.nr_sequencia
AND    c.nr_prescricao     = b.nr_prescricao
AND    b.nr_atendimento    = nr_atendimento_p
and	   c.nr_seq_exame = nr_Seq_exame_p;

cursor c03 is
select Obter_data_aprov_lab(b.nr_prescricao,c.nr_sequencia)
from   result_laboratorio a,
       prescr_procedimento c,
       prescr_medica b
WHERE  a.nr_prescricao     = c.nr_prescricao
AND    a.nr_seq_prescricao = c.nr_sequencia
AND    c.nr_prescricao     = b.nr_prescricao
AND    b.nr_atendimento    = nr_atendimento_p
and	   c.nr_seq_exame = nr_Seq_exame_p;
  
begin

if (ie_opcao_p = 'E') then
	open c01;
	loop
	fetch c01 into	ds_resultado_w;
	exit when c01%notfound;
	end loop;
	close c01;
elsif	(ie_opcao_p = 'A') then	
	open c02;
	loop
	fetch c02 into	ds_resultado_w;
	exit when c02%notfound;
	end loop;
	close c02;
elsif (ie_opcao_p = 'D') then
	open c03;
	loop
	fetch c03 into	ds_resultado_w;
	exit when c03%notfound;
	end loop;
	close c03;	
end if;



return ds_resultado_w;

end HSL_Obter_Glicemia_Exam;
/
