create or replace
function	obter_se_rge_passaporte_info(
			cd_pessoa_fisica_p		varchar2)
		return	varchar2 is

cd_nacionalidade_w			pessoa_fisica.cd_nacionalidade%type;
nr_cpf_w				pessoa_fisica.nr_cpf%type;
nr_reg_geral_estrang_w			pessoa_fisica.nr_reg_geral_estrang%type;
nr_passaporte_w				pessoa_fisica.nr_passaporte%type;
ie_brasileiro_w				nacionalidade.ie_brasileiro%type;
ie_retorno_w				varchar2(1)	:= 'S';

begin

select	nvl(max(cd_nacionalidade),0),
	nvl(max(nr_cpf),'0'),
	nvl(max(nr_reg_geral_estrang),'0'),
	nvl(max(nr_passaporte),'0')
into	cd_nacionalidade_w,
	nr_cpf_w,
	nr_reg_geral_estrang_w,
	nr_passaporte_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

select	nvl(max(ie_brasileiro),'S')
into	ie_brasileiro_w
from	nacionalidade
where	cd_nacionalidade = cd_nacionalidade_w;

if	(ie_brasileiro_w = 'N') and
	(nr_cpf_w = '0') and
	(nr_reg_geral_estrang_w = '0') and
	(nr_passaporte_w = '0') then
	ie_retorno_w	:= 'N';
end if;

return	ie_retorno_w;

end obter_se_rge_passaporte_info;
/
