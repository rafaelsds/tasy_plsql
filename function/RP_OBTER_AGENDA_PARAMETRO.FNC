create or replace
function rp_obter_agenda_parametro(ie_tipo_agenda_p	varchar2)
 		    	return varchar2 is

nr_sequencia_w	number(10);
ds_retorno_w	varchar2(255);
cd_estabelecimento_w number(10);
cd_perfil_w	number(10);
			
begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w         	     := Obter_perfil_Ativo;

select	max(nr_sequencia)
into	nr_sequencia_w
from	rp_parametros
where	cd_estabelecimento = cd_estabelecimento_w
and	cd_perfil = nvl(cd_perfil_w,cd_perfil);

if (nr_sequencia_w > 0) and (nr_sequencia_w is not null) then
	select	decode(ie_tipo_agenda_p,'GA',cd_agenda_ga,'GO',cd_agenda_go)
	into	ds_retorno_w
	from	rp_parametros
	where	nr_sequencia = nr_sequencia_w;
end if;

return	ds_retorno_w;

end rp_obter_agenda_parametro;
/

