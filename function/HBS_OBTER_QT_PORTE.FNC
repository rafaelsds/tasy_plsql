create or replace
function hbs_obter_qt_porte(	
				cd_procedimento_p	Number,
				nr_interno_conta_p Number)
	    		return number is

ds_retorno_w	Number(10,0);			
			
begin

select		max(p.qt_porte_anestesico)
into		ds_retorno_w
from		w_conta_pmmg_item a,
		w_conta_pmmg b,
		procedimento_paciente c,
		PRECO_AMB p
where   		 b.nr_interno_conta	= nr_interno_conta_p
and		a.NR_SEQ_ITEM			=  2
and		a.nr_seq_conta_pmmg	= b.nr_sequencia
and		b.nr_interno_conta	= c.nr_interno_conta
and		P.CD_PROCEDIMENTO		=  c.CD_PROCEDIMENTO
and		p.QT_PORTE_ANESTESICO	= a.cd_atuacao
and		p.CD_PROCEDIMENTO	= cd_procedimento_p
and     		p.cd_edicao_amb  = c.cd_edicao_amb;

return ds_retorno_w;

end hbs_obter_qt_porte;
/
