create or replace
function obter_se_nota_item_me(
		nr_seq_nota_p		number)
 		return varchar2 is

nr_ordem_compra_w		number(10);
qt_existe_w			number(5);
ie_integracao_me_w		varchar2(1) := 'N';

cursor c01 is
	select	nr_ordem_compra
	from	nota_fiscal_item
	where	nr_sequencia = nr_seq_nota_p;

begin

open c01;
loop
fetch c01 into	
	nr_ordem_compra_w;
exit when c01%notfound;
	begin
	
	select	count(*)
	into	qt_existe_w
	from	ordem_compra
	where	ie_origem_imp = 'MercadoEletronico'
	and	nr_ordem_compra = nr_ordem_compra_w;
	
	if	(qt_existe_w > 0) then
		ie_integracao_me_w := 'S';
		goto final;
	end if;
	
	end;
end loop;
close c01;

<<final>>

return	ie_integracao_me_w;

end obter_se_nota_item_me;
/