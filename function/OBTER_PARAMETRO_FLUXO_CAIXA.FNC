create or replace function obter_parametro_fluxo_caixa(
				cd_estabelecimento_p number,
				ie_tipo_dado_p varchar) return varchar2 is
				
/* ie_tipo_dado:
	CRE = C�digo conta financeira recebidos
	CPA = C�digo conta financeira pagos
	TFS = Tratar fim de semana
*/
		
cd_conta_financ_cpa_w	number;		
cd_conta_financ_cre_w	number;
ie_tratar_fim_semana_w	varchar2(1);
ds_retorno_w		varchar2(100);

begin

select	cd_conta_financ_cpa,
	cd_conta_financ_cre,
	ie_tratar_fim_semana
into	cd_conta_financ_cpa_w,
	cd_conta_financ_cre_w,
	ie_tratar_fim_semana_w
from	parametro_fluxo_caixa
where	cd_estabelecimento = cd_estabelecimento_p;

if (ie_tipo_dado_p = 'CPA') then
	ds_retorno_w := cd_conta_financ_cpa_w;
elsif (ie_tipo_dado_p = 'CRE') then
	ds_retorno_w := cd_conta_financ_cre_w;
elsif (ie_tipo_dado_p = 'TFS') then
	ds_retorno_w := ie_tratar_fim_semana_w;
end if;

return ds_retorno_w;

end obter_parametro_fluxo_caixa;
/