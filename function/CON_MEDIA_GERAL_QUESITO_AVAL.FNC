create or replace
function con_media_geral_quesito_aval(	cd_consultor_p		varchar2)
 		    	return varchar2 is

qt_retorno_w		varchar2(15);
qt_resultado_w		varchar2(15);			

begin
select	round(nvl((sum(to_number(vl_media_proj))/count(nr_seq_proj)),0),2)
into	qt_resultado_w 
from	(select	distinct 
		nr_seq_proj,
		substr(obter_ds_proj_projeto(NR_SEQ_PROJ),1,255) ds_titulo,
		substr(con_media_quesito_aval(cd_executor,nr_seq_proj,'V'),1,40) vl_media_proj
	from	proj_rat_v
	where	substr(con_media_quesito_aval(cd_executor,nr_seq_proj,'V'),1,40) <> 0
	and	cd_executor = cd_consultor_p);

if (qt_resultado_w <= 0) then
qt_retorno_w := '0';
end if;
if (qt_resultado_w > 0.00) and (qt_resultado_w <= 1.40) then
qt_retorno_w := qt_resultado_w||' - P�ssimo';
end if;
if (qt_resultado_w > 1.40) and (qt_resultado_w <= 2.40) then
qt_retorno_w := qt_resultado_w||' - Ruim';
end if;
if (qt_resultado_w > 2.40) and (qt_resultado_w <= 3.40) then
qt_retorno_w := qt_resultado_w||' - Regular';
end if;
if (qt_resultado_w > 3.40) and (qt_resultado_w <= 4.40) then
qt_retorno_w := qt_resultado_w||' - Bom';
end if;
if (qt_resultado_w > 4.40) then
qt_retorno_w := qt_resultado_w||' - �timo';
end if;

return	qt_retorno_w;

end con_media_geral_quesito_aval;
/
