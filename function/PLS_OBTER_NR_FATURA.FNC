create or replace
function pls_obter_nr_fatura(	nr_seq_fatura_p		ptu_fatura.nr_sequencia%type)
					return varchar2 is
nr_fatura_w	ptu_fatura.nr_fatura%type;
begin

if	(nr_seq_fatura_p	is not null) then
	select	max(z.nr_fatura)
	into	nr_fatura_w
	from	ptu_fatura z
	where	z.nr_sequencia = nr_seq_fatura_p;
end if;

return	nr_fatura_w;

end pls_obter_nr_fatura;
/
