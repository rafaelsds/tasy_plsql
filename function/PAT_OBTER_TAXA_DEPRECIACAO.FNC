CREATE OR REPLACE
FUNCTION Pat_obter_taxa_depreciacao( nr_sequencia_p	Number)
			RETURN NUMBER IS
				
vl_retorno_w		Number(15,2);

BEGIN
select	nvl(max(pr_depreciacao),0)
into	vl_retorno_w
from	pat_bem_taxa
where	nr_seq_bem = nr_sequencia_p
and	dt_vigencia = 
	(select	max(dt_vigencia)
	from	pat_bem_taxa
	where	nr_seq_bem = nr_sequencia_p
	and	dt_vigencia <= sysdate);

if	(vl_retorno_w = 0) then
	begin
	select	tx_deprec
	into	vl_retorno_w
	from	pat_bem
	where	nr_sequencia = nr_sequencia_p;
	end;
end if;

RETURN	vl_retorno_w;

END Pat_obter_taxa_depreciacao;
/