create or replace
function obter_cd_protocolo_pac_atend (nr_seq_atendimento_p		number)
 		    	return varchar2 is

ds_protocolo_w	varchar2(255);

begin
if 	(nvl(nr_seq_atendimento_p,0) > 0) then 
	SELECT	MAX(a.cd_protocolo)
	INTO    ds_protocolo_w
	FROM	paciente_setor a,
		paciente_atendimento b
	WHERE	a.nr_seq_paciente	= b.nr_seq_paciente
	AND 	b.nr_seq_atendimento 	= nr_seq_atendimento_p;
end if;

return	ds_protocolo_w;

end obter_cd_protocolo_pac_atend;
/