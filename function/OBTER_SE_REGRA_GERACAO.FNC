create or replace function obter_se_regra_geracao(
	nr_seq_regra_p 				number,
	nr_seq_episodio_p			number,
	nr_seq_tipo_admissao_fat_p 	number,
	cd_setor_atendimento_p		number default null)
return varchar2 is 

ie_regra_geracao_w 			varchar2(1);
nr_seq_tipo_episodio_w 		episodio_paciente.nr_seq_tipo_episodio%type;

begin
	select	decode(count(1), 0,'S','N') 
	into	ie_regra_geracao_w
	from	wl_regra_geracao rg,
			wl_regra_item ri,
			wl_regra_worklist rw
	where	rg.nr_seq_regra_item = ri.nr_sequencia
	and		rg.nr_seq_item = rw.nr_sequencia
	and		ri.nr_seq_regra = rw.nr_sequencia
	and		ri.ie_situacao = 'A'
	and		rg.nr_seq_regra_item = nr_seq_regra_p;
	
	if (ie_regra_geracao_w = 'N') then 
	
		select 	max(x.nr_seq_tipo_episodio)
		into	nr_seq_tipo_episodio_w
		from	episodio_paciente x
		where	x.nr_sequencia = nr_seq_episodio_p;
		
		select	nvl(max('S'),'N') 
		into	ie_regra_geracao_w
		from	wl_regra_geracao rg
		where	rg.nr_seq_regra_item = nr_seq_regra_p
		and 	nvl(rg.nr_seq_tipo_episodio,nr_seq_tipo_episodio_w) = nr_seq_tipo_episodio_w
		and 	(nr_seq_tipo_admissao_fat_p is null or (nvl(rg.nr_seq_tipo_admissao_fat,nr_seq_tipo_admissao_fat_p) = nr_seq_tipo_admissao_fat_p))
		and 	(cd_setor_atendimento_p is null or (nvl(rg.cd_setor_atendimento,cd_setor_atendimento_p) = cd_setor_atendimento_p));
		
	end if;
	
	return ie_regra_geracao_w;
	
end obter_se_regra_geracao;
/