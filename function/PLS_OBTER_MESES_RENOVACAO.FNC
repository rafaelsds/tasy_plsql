create or replace
function pls_obter_meses_renovacao
		(	nr_seq_segurado_p	number)
 		    	return number is

qt_dias_renovacao_w	number(10);
nr_seq_contrato_w	number(10);
ie_contrato_w		varchar2(2);
ie_tipo_segurado_w	varchar2(10);
ie_tipo_contrato_w	varchar2(10);
nr_seq_intercambio_w	number(10);
ie_titularidade_w	varchar2(10);

Cursor C01 is
	select	qt_dias_renovacao
	from	pls_param_regra_carteira
	where	((ie_tipo_renovacao = 'R') or (ie_tipo_renovacao is null))
	and	((ie_tipo_contrato = ie_tipo_contrato_w) or (ie_tipo_contrato = 'A'))
	and	((ie_contrato = ie_contrato_w) or (ie_contrato is null))
	and	((nvl(ie_titularidade,'A') = ie_titularidade_w) or (nvl(ie_titularidade,'A') = 'A'))
	and	((ie_tipo_segurado = ie_tipo_segurado_w) or (ie_tipo_segurado is null))
	order by nvl(ie_tipo_renovacao, ' '),
		ie_tipo_contrato,
		decode(ie_tipo_segurado,'',-1,1);

begin

select	nr_seq_contrato,
	ie_tipo_segurado,
	nr_seq_intercambio,
	decode(nr_seq_titular,null,'T','D')
into	nr_seq_contrato_w,
	ie_tipo_segurado_w,
	nr_seq_intercambio_w,
	ie_titularidade_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

qt_dias_renovacao_w	:= 0;

if	(nr_seq_contrato_w is not null) then
	select	nvl(max(qt_meses_renovacao),0) 
	into	qt_dias_renovacao_w
	from    pls_carteira_renovacao
	where   nr_seq_contrato 	= nr_seq_contrato_w
	and	((ie_tipo_renovacao	= 'R')
	or	(ie_tipo_renovacao is null));
	
	select	decode(cd_pf_estipulante,null,'PJ','PF')
	into	ie_tipo_contrato_w
	from	pls_contrato
	where	nr_sequencia = nr_seq_contrato_w;
	
	ie_contrato_w	:= 'O';
elsif	(nr_seq_intercambio_w is not null) then
	select	nvl(max(qt_meses_renovacao),0) 
	into	qt_dias_renovacao_w
	from    pls_carteira_renovacao
	where   nr_seq_intercambio 	= nr_seq_intercambio_w
	and	((ie_tipo_renovacao	= 'R')
	or	(ie_tipo_renovacao is null));
	
	select	decode(cd_pessoa_fisica,null,'PJ','PF')
	into	ie_tipo_contrato_w
	from	pls_intercambio
	where	nr_sequencia = nr_seq_intercambio_w;
	
	ie_contrato_w	:= 'I';
end if;

if	(qt_dias_renovacao_w = 0) then
	open C01;
	loop
	fetch C01 into
		qt_dias_renovacao_w;
	exit when C01%notfound;
	end loop;
	close C01;
end if;

return	nvl(qt_dias_renovacao_w,0);

end pls_obter_meses_renovacao;
/