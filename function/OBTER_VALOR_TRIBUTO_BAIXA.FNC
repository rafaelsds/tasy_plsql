create or replace
function obter_valor_tributo_baixa(nr_titulo_p	number,
				nr_bordero_p	number,
				nr_seq_baixa_p	number) 
				return number is

vl_imposto_w	number(15,2);

begin

select	nvl(sum(b.vl_imposto),0)
into	vl_imposto_w
from	titulo_pagar_imposto b,
	titulo_pagar_baixa a
where	a.nr_titulo	= b.nr_titulo
and	a.nr_sequencia	= b.nr_seq_baixa
and	b.nr_titulo	= nr_titulo_p
and	a.nr_bordero	= nr_bordero_p
and	b.ie_pago_prev	= 'V';

return vl_imposto_w;

end;
/