create or replace
function obter_regra_ordenacao_pa(nm_usuario_p 		varchar2) 
								  return varchar2 is

ds_order_by_w			varchar2(4000);
cd_estabelecimento_w	number(4);
cd_perfil_w				number(5);
cd_setor_atendimento_w  number(5);

ds_sql_w				varchar2(4000);
ie_order_by				varchar(1) := 'N';

cursor c01 is
	select	' '||ds_sql
	from	regra_ordenacao_pa
	where	nvl(ie_situacao,'I') = 'A'
	and  	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and		nvl(cd_perfil,cd_perfil_w) = cd_perfil_w
	and		nvl(cd_setor_atendimento,cd_setor_atendimento_w) = cd_setor_atendimento_w
	order by cd_estabelecimento, cd_perfil, cd_setor_atendimento;
		   
begin

ds_order_by_w := ' order by dt_entrada';

if (nm_usuario_p is not null) then

	cd_estabelecimento_w := Obter_estab_usuario(nm_usuario_p);
	
	cd_perfil_w := obter_perfil_ativo;
	
	cd_setor_atendimento_w := obter_setor_usuario(nm_usuario_p);

	open c01;
	loop
	fetch c01 into
		ds_sql_w;
	exit when c01%notfound;
		begin	
			if (ie_order_by = 'N') then 
				ds_order_by_w := ds_sql_w;
				ie_order_by := 'S';
			end if;
		end;
	end loop;
	close c01;
end if;

return ds_order_by_w;

end obter_regra_ordenacao_pa;
/