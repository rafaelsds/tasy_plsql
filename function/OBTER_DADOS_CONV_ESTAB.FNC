create or replace
function obter_dados_conv_estab(cd_estabelecimento_p number,
				cd_convenio_p number,
				ie_opcao_p number) return varchar2 is
/*
ie_opcao_p:
1 -  Permite utilizar op��o Gerar NF (desdobrar por contas)
2 -  Tomador NF Desdob
3 - Buscar cd conta contab�il convenio estab.
4 - Buscar a quantidade de horas do in�cio do ciclo
*/
ds_retorno_w	varchar2(4000);

begin

if	(ie_opcao_p = 1) then				
	select	nvl(ie_gerar_nf_desdob,'N')
	into	ds_retorno_w
	from	convenio_estabelecimento
	where	cd_estabelecimento 	= cd_estabelecimento_p
	and	cd_convenio 		= cd_convenio_p;
elsif	(ie_opcao_p = 2) then
	select	nvl(ie_tomador_nf_desdob,'P')
	into	ds_retorno_w
	from	convenio_estabelecimento
	where	cd_estabelecimento 	= cd_estabelecimento_p
	and	cd_convenio 		= cd_convenio_p;
elsif	(ie_opcao_p = 3) then
	select	max(cd_conta_contabil)
	into	ds_retorno_w
	from	convenio_estabelecimento
	where	cd_estabelecimento 	= cd_estabelecimento_p
	and	cd_convenio 		= cd_convenio_p;
elsif	(ie_opcao_p = 4) then
	select	nvl(qt_horas_inicio_ciclo,0)
	into	ds_retorno_w
	from	convenio_estabelecimento
	where	cd_estabelecimento 	= cd_estabelecimento_p
	and	cd_convenio 		= cd_convenio_p;
end if;

return ds_retorno_w;

end obter_dados_conv_estab;
/