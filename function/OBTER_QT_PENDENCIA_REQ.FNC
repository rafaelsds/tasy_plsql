create or replace
function obter_qt_pendencia_req(cd_local_estoque_p		number,
				cd_material_p			number)
 		    	return number is
			
qt_retorno_w		number(18,4) := 0;
qt_pend_req_consumo_w	number(18,4);

qt_pend_req_transf_saida_w	number(18,4);
qt_pend_req_transf_entrada_w	number(18,4);


begin

select	nvl(sum(c.qt_estoque),0)
into	qt_pend_req_consumo_w
from	material a ,
	operacao_estoque o ,
	requisicao_material b,
	item_requisicao_material c,
	sup_motivo_baixa_req e
where	b.nr_requisicao		= c.nr_requisicao
and	a.cd_material		= c.cd_material
and	b.cd_operacao_estoque	= o.cd_operacao_estoque
and	c.cd_motivo_baixa	= e.nr_sequencia
and	e.cd_motivo_baixa	= 0
and	o.ie_tipo_requisicao	= '1'
and	a.cd_material		= cd_material_p
and	b.dt_liberacao		is not null;

if	(cd_local_estoque_p > 0) then

	select	nvl(sum(c.qt_estoque),0)
	into	qt_pend_req_transf_saida_w
	from	material a ,
		operacao_estoque o ,
		requisicao_material b,
		item_requisicao_material c,
		sup_motivo_baixa_req e
	where	b.nr_requisicao		= c.nr_requisicao
	and	a.cd_material		= c.cd_material
	and	b.cd_operacao_estoque	= o.cd_operacao_estoque
	and	c.cd_motivo_baixa	= e.nr_sequencia
	and	e.cd_motivo_baixa	= 0
	and	o.ie_tipo_requisicao	= '2'
	and	a.cd_material		= cd_material_p
	and	b.dt_liberacao		is not null
	and	b.cd_local_estoque	= cd_local_estoque_p
	and	o.ie_entrada_saida	= 'S';
	
	if	(qt_pend_req_transf_saida_w > 0) then
		qt_pend_req_consumo_w := qt_pend_req_consumo_w + qt_pend_req_transf_saida_w;
	end if;
	
	
	select	nvl(sum(c.qt_estoque),0)
	into	qt_pend_req_transf_entrada_w
	from	material a ,
		operacao_estoque o ,
		requisicao_material b,
		item_requisicao_material c,
		sup_motivo_baixa_req e
	where	b.nr_requisicao		= c.nr_requisicao
	and	a.cd_material		= c.cd_material
	and	b.cd_operacao_estoque	= o.cd_operacao_estoque
	and	c.cd_motivo_baixa	= e.nr_sequencia
	and	e.cd_motivo_baixa	= 0
	and	o.ie_tipo_requisicao	= '2'
	and	a.cd_material		= cd_material_p
	and	b.dt_liberacao		is not null
	and	b.cd_local_estoque	= cd_local_estoque_p
	and	o.ie_entrada_saida	= 'E';
	
	if	(qt_pend_req_transf_entrada_w > 0) then
		qt_pend_req_consumo_w := qt_pend_req_consumo_w - qt_pend_req_transf_entrada_w;
	end if;		

end if;	
	
qt_retorno_w := qt_pend_req_consumo_w;

return	qt_retorno_w;

end obter_qt_pendencia_req;
/