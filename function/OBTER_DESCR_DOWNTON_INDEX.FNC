create or replace function OBTER_DESCR_DOWNTON_INDEX(qt_pontos_p		number)
									return varchar2 is

ds_retorno_w	varchar2(20);
begin
if 	(qt_pontos_p = 0) then
	ds_retorno_w := wheb_mensagem_pck.get_texto(308202); -- Nenhum risco
elsif (qt_pontos_p = 1) then
	ds_retorno_w := wheb_mensagem_pck.get_texto(308203); -- Baixo risco
elsif (qt_pontos_p = 2) then
	ds_retorno_w := wheb_mensagem_pck.get_texto(308204); -- Medio risco
else
	ds_retorno_w := wheb_mensagem_pck.get_texto(308205); -- Alto risco
end if;

return	ds_retorno_w;

end OBTER_DESCR_DOWNTON_INDEX;
/
