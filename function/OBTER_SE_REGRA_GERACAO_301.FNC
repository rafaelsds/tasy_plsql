create or replace
function obter_se_regra_geracao_301(	nr_seq_episodio_p	number,
					dt_referencia_p		date,
					ie_dataset_p		varchar2,
					cd_convenio_p		number default null)
					return varchar2 is
										
ie_gerar_w	varchar2(1)	:= 'N';
										
/* Regras de gera��o de datasets */
Cursor c_regra_geracao is
select	a.*
from	c301_regra_geracao a
where	a.dt_inicio_vigencia <= dt_referencia_p
and	(a.dt_fim_vigencia >= nvl(dt_referencia_p,to_date('31/12/9999','dd/mm/yyyy')) or a.dt_fim_vigencia is null)
and	a.ie_dataset_geracao = ie_dataset_p;

begin
for	r_c_regra_geracao in c_regra_geracao loop
	select	decode(count(1),0,'N','S') ie_gerar
	into	ie_gerar_w
	from	atendimento_paciente a,
		episodio_paciente b,
		atend_categoria_convenio c,
		convenio d
	where	a.nr_seq_episodio		= b.nr_sequencia
	and	a.nr_atendimento		= c.nr_atendimento
	and	c.cd_convenio			= d.cd_convenio
	and	c.cd_convenio			= cd_convenio_p
	and	b.nr_sequencia			= nr_seq_episodio_p
	and	(b.nr_seq_tipo_episodio 	= r_c_regra_geracao.nr_seq_tipo_episodio or r_c_regra_geracao.nr_seq_tipo_episodio is null)
	and	(b.nr_seq_subtipo_episodio 	= r_c_regra_geracao.nr_seq_subtipo_episodio or r_c_regra_geracao.nr_seq_subtipo_episodio is null)
	and	(d.ie_tipo_convenio		= r_c_regra_geracao.ie_tipo_convenio or r_c_regra_geracao.ie_tipo_convenio is null)
	and	(c.cd_convenio			= r_c_regra_geracao.cd_convenio or r_c_regra_geracao.cd_convenio is null)
	and	(exists	(select	1
			from	convenio_classif x
			where	x.cd_convenio		= c.cd_convenio
			and	x.nr_seq_classificacao	= r_c_regra_geracao.nr_seq_classif_conv) or r_c_regra_geracao.nr_seq_classif_conv is null)			
	and	(a.ie_tipo_atendimento = r_c_regra_geracao.ie_tipo_atendimento or r_c_regra_geracao.ie_tipo_atendimento is null)
	and	(a.nr_seq_tipo_admissao_fat = r_c_regra_geracao.nr_seq_tipo_adm_fat or r_c_regra_geracao.nr_seq_tipo_adm_fat is null)
	and	(a.nr_seq_classificacao = r_c_regra_geracao.nr_seq_classif_atend or r_c_regra_geracao.nr_seq_classif_atend is null)
	and	(a.nr_seq_queixa = r_c_regra_geracao.nr_seq_queixa_atend or r_c_regra_geracao.nr_seq_queixa_atend is null);
end loop;

return	ie_gerar_w;

end obter_se_regra_geracao_301;
/