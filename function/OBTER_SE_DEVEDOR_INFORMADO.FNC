create or replace
function obter_se_devedor_informado(nr_titulo_p	number)
					return varchar2 is

cd_convenio_w		number(15);
cd_tipo_portador_w	number(15);
cd_portador_w		number(15);
nr_seq_terc_conta_w	number(15);
nr_seq_contrato_w		number(15);
ie_origem_titulo_w		varchar2(100);
ie_tipo_devedor_w		varchar2(100);
ds_convenio_w		varchar2(255);

begin

select	nvl(cd_convenio, 0),
	cd_tipo_portador,
	cd_portador,
	ie_origem_titulo,
	nr_seq_terc_conta,
	nr_seq_contrato
into	cd_convenio_w,
	cd_tipo_portador_w,
	cd_portador_w,
	ie_origem_titulo_w,
	nr_seq_terc_conta_w,
	nr_seq_contrato_w
from 	titulo_receber_v
where	nr_titulo	= nr_titulo_p;

if 	(cd_convenio_w <> 0) then
	ds_convenio_w	:= substr(obter_nome_convenio(cd_convenio_w),1,40);
elsif	(nr_seq_terc_conta_w is not null) then
	ds_convenio_w := wheb_mensagem_pck.get_texto(309457); -- Contas de terceiros
elsif	(nr_seq_contrato_w is not null) then
	ds_convenio_w := wheb_mensagem_pck.get_texto(309458); -- Contratos
else
	obter_regra_eis_titulo_venc(cd_tipo_portador_w,cd_portador_w,ie_origem_titulo_w,ie_tipo_devedor_w);
	ds_convenio_w := nvl(substr(obter_valor_dominio(2919,ie_tipo_devedor_w),1,255),null);
end if;

if	(ds_convenio_w is null) then
	return	'N';
else
	return	'S';
end if;	

end	obter_se_devedor_informado;
/