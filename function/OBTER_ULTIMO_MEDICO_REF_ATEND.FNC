create or replace
function obter_ultimo_medico_ref_atend(cd_pessoa_fisica_p		varchar2)
 		    	return varchar2 is
			
cd_medico_referido_w	atendimento_paciente.cd_medico_referido%type;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;

begin

if	(cd_pessoa_fisica_p is not null) then

	select	nvl(max(nr_atendimento),0)
	into	nr_atendimento_w
	from	atendimento_paciente 
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	cd_medico_referido is not null; 
	
	if	(nr_atendimento_w > 0) then 
	
		select	max(cd_medico_referido)
		into	cd_medico_referido_w
		from	atendimento_paciente 
	        where	nr_atendimento = nr_atendimento_w;
	
	end if;

end if;

return	cd_medico_referido_w;

end obter_ultimo_medico_ref_atend;
/