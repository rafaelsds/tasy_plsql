create or replace
function Man_Obter_data_prev_OS (nr_seq_ordem_p	number)
 		    	return varchar2 is
dt_prevista_w	varchar2(30);
begin

select 	to_char(min(DT_PREVISTA),'dd/mm/yyyy hh24:mi:ss')
into	dt_prevista_w
from	MAN_ORDEM_ATIV_PREV a
where	nr_seq_ordem_p = a.nr_seq_ordem_serv
and	DT_REAL is null
and	trunc(dt_prevista) >= trunc(sysdate);

return	dt_prevista_w;

end Man_Obter_data_prev_OS;
/