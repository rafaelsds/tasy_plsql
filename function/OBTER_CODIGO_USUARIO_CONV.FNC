create or replace
function OBTER_CODIGO_USUARIO_CONV	(nr_atendimento_p		NUMBER)
				return VARCHAR2 is

cd_usuario_convenio_w	VARCHAR2(30);

begin

select	MAX(cd_usuario_convenio)
into	cd_usuario_convenio_w
from	atend_categoria_convenio
where	nr_atendimento		= nr_atendimento_p
and	dt_inicio_vigencia	= (	select	MAX(dt_inicio_vigencia)
			  		from	atend_categoria_convenio
			  		where	nr_atendimento	= nr_atendimento_p);

return	cd_usuario_convenio_w;

end	OBTER_CODIGO_USUARIO_CONV;
/
