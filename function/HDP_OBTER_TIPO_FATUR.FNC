create or replace
function HDP_OBTER_TIPO_FATUR(nr_interno_conta_p	Number)
 		    	return Varchar2 is
			
ie_tipo_fatur_w		varchar2(1);

begin

select 	nvl(max(IE_TIPO_FATUR_TISS),'T')
into	ie_tipo_fatur_w
from 	conta_paciente
where 	nr_interno_conta = nr_interno_conta_p;

return	ie_tipo_fatur_w;

end HDP_OBTER_TIPO_FATUR;
/