create or replace function obter_pr_real_programa( nr_seq_programa_p number ) return number is
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter percentual real de execu��o de um programa
---------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [ ]  Relat�rios [ ] Outros: 
 --------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

pr_retorno_w number(15,2) := 0;

begin

  select ( select dividir(t.per_exec * 100,t.qt_hora_prev) pr_realizado
           from   ( select sum(pe.qt_hora_prev) qt_hora_prev,
                           sum(dividir(pe.qt_hora_prev * pe.pr_etapa,100)) per_exec
                    from   proj_cronograma pc,
                           proj_cron_etapa pe,
                           proj_projeto    pp
                    where  pc.nr_sequencia = pe.nr_seq_cronograma 
                    and    pp.nr_sequencia = pc.nr_seq_proj 
                    and    pp.nr_seq_programa = nr_seq_programa_p
                    and    pc.ie_situacao = 'A'
                    and    pe.ie_fase     = 'N'
	    and  not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  pe.nr_sequencia)
                  ) t
         ) pr_real
  into   pr_retorno_w
  from   dual;

  return  pr_retorno_w;

end obter_pr_real_programa;
/

