create or replace
function OBTER_SETOR_MAT_AUTOR
				(nr_seq_mat_autor_p	number,
				 ie_opcao_p		varchar2) return varchar2 is
/*
ie_opcao_p
C - C�dito do setor
N - Nome do setor
*/

ds_retorno_w			varchar2(255);
nr_atendimento_w		number(15,0);
cd_material_w			number(15,0);
cd_setor_atendimento_w		number(15,0);

begin

select	max(b.nr_atendimento),
	max(a.cd_material)
into	nr_atendimento_w,
	cd_material_w
from	autorizacao_cirurgia b,
	material_autor_cirurgia a
where	a.nr_sequencia		= nr_seq_mat_autor_p
and	a.nr_seq_autorizacao	= b.nr_sequencia;

ds_retorno_w	:= null;
if	(nr_atendimento_w is not null) and (cd_material_w is not null) then
	select	max(cd_setor_atendimento)
	into	cd_setor_atendimento_w
	from	material_atend_paciente 
	where	nr_atendimento		= nr_atendimento_w
	and	cd_material		= cd_material_w
	and	cd_motivo_exc_conta	is null;

	if	(ie_opcao_p = 'C') then
		ds_retorno_w	:= cd_setor_atendimento_w;
	elsif	(ie_opcao_p = 'D') then
		ds_retorno_w	:= obter_nome_setor(cd_setor_atendimento_w);
	end if;
end if;

return	ds_retorno_w;

end OBTER_SETOR_MAT_AUTOR;
/
