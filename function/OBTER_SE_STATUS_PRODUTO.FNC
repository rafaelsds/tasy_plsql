CREATE OR REPLACE
FUNCTION obter_se_status_produto	(	nr_seq_status_p		number,
						nr_lote_producao_p	Number)
						return Varchar2 is

ie_retorno_w		VARCHAR2(1) := 'N';

BEGIN

if (nr_seq_status_p is not null) and (nr_lote_producao_p is not null)  then
	select	nvl(MAX('S'),'N')
	into	ie_retorno_w
	from    lote_producao_comp a
	where   a.nr_lote_producao = nr_lote_producao_p
	and	nr_seq_estagio 	   = nr_seq_status_p;

end if;


return ie_retorno_w;

end obter_se_status_produto;
/