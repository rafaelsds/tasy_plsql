create or replace
function obter_status_calibracao(	nr_sequencia_p			number)
 		    	return varchar2 is

nr_seq_equip_calib_escala_w				number(10);			
qt_erro_max_permitido_w				    number(15,4);
qt_erro_total_w					        number(15,4);
ds_retorno_w					        varchar2(1) := 'A';

begin

select	nvl(max(nr_seq_equip_calib_escala),0)
into	nr_seq_equip_calib_escala_w
from	man_calibracao
where	nr_sequencia = nr_sequencia_p;

if	(nr_seq_equip_calib_escala_w > 0) then
    
	qt_erro_max_permitido_w	:= nvl(obter_qt_erro_max_permitido(nr_seq_equip_calib_escala_w),0);
	qt_erro_total_w		:= nvl(obter_qt_erro_total_calibracao(nr_sequencia_p),0);

	if	(qt_erro_total_w > qt_erro_max_permitido_w) then
		ds_retorno_w := 'R';
	end if;
end if;

return	ds_retorno_w;

end obter_status_calibracao;
/
