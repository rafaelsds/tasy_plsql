create or replace
function pls_obter_dados_benef_rescisao (	nr_seq_notific_pagador_p	pls_notificacao_pagador.nr_sequencia%type,
						ie_opcao_p			varchar2)
						return number is
-- V - Valor
-- Q - Quantidade

vl_retorno_w			number(15,2) := 0;
nr_seq_preco_w			pls_segurado_preco.nr_sequencia%type;
vl_preco_atual_w		pls_segurado_preco.vl_preco_atual%type;

cursor c01 (	nr_seq_notific_pagador_pc	pls_notificacao_pagador.nr_sequencia%type)	is
	select	nr_sequencia
	from	pls_segurado	a
	where	a.dt_rescisao is not null
	and	a.nr_seq_notific_pagador	= nr_seq_notific_pagador_pc;

begin
if	(ie_opcao_p	= 'V') then

	for r_C01_w in C01(nr_seq_notific_pagador_p) loop
		select	max(x.nr_sequencia)
		into	nr_seq_preco_w
		from	pls_segurado_preco x
		where	x.nr_seq_segurado = r_C01_w.nr_sequencia;
	
		vl_preco_atual_w := 0;
		if	(nr_seq_preco_w is not null) then
			select	nvl(x.vl_preco_atual,0)
			into	vl_preco_atual_w
			from	pls_segurado_preco x
			where	x.nr_sequencia	= nr_seq_preco_w;
		end if;
		
		vl_retorno_w	:= vl_retorno_w + vl_preco_atual_w;
	end loop;
	
elsif	(ie_opcao_p	= 'Q') then
	for r_C01_w in C01(nr_seq_notific_pagador_p) loop
		vl_retorno_w := vl_retorno_w + 1;
	end loop;
end if;

return	nvl(vl_retorno_w,0);

end pls_obter_dados_benef_rescisao;
/