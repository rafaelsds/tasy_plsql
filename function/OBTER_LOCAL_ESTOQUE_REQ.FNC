create or replace
function obter_local_estoque_req(nr_requisicao_p	number)
				return number is

cd_local_estoque_w		number(6);

begin

if	(nr_requisicao_p is not null) then
	select	cd_local_estoque
	into	cd_local_estoque_w
	from	requisicao_material
	where	nr_requisicao = nr_requisicao_p;
end if;

return cd_local_estoque_w;

end obter_local_estoque_req;
/