create or replace 
function obter_se_atend_aprovado(	nr_atendimento_p	number)
					return varchar2 is

ie_aprov_w		varchar2(1);				
qt_exame_pendente_w	number(10);
qt_exame_aprov_w	number(10);

begin

select 	count(*)
into	qt_exame_pendente_w
from	prescr_medica b,
	prescr_procedimento a
where	a.nr_prescricao = b.nr_prescricao
and	a.nr_seq_exame is not null
and	a.ie_status_atend < 35
and	a.cd_motivo_baixa = 0
and	b.nr_atendimento = nr_atendimento_p;

ie_aprov_w	:= 'N';

if	(qt_exame_pendente_w = 0) then

	select 	count(*)
	into	qt_exame_aprov_w
	from	prescr_medica b,
		prescr_procedimento a
	where	a.nr_prescricao = b.nr_prescricao
	and	nr_seq_exame is not null
	and	ie_status_atend >= 35
	and	nr_atendimento = nr_atendimento_p;

	if 	(qt_exame_aprov_w > 0)	then
		ie_aprov_w	:= 'S';	
	end if;

end if;

return ie_aprov_w;

end;
/
