create or replace
function Obter_Tipo_Pessoa_Relat	(cd_cgc_p		varchar2,
					cd_estabelecimento_p	number,
					ie_notin_p		varchar2,
					ds_sql_p		varchar2)
 		    	return varchar is

ds_retorno_w		varchar2(1);
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

Cursor C01 is
select	a.cd_tipo_pessoa
from	pessoa_jur_tipo_adic a
where	a.cd_cgc		= cd_cgc_p
and	a.cd_estabelecimento	= cd_estabelecimento_p
union
select	a.cd_tipo_pessoa
from	pessoa_juridica a
where	a.cd_cgc		= cd_cgc_p;

begin

open C01;
loop
fetch C01 into	
	cd_tipo_pessoa_w;
exit when C01%notfound or (ds_retorno_w = 'N' and ie_notin_p = 'S') or (ds_retorno_w = 'S' and ie_notin_p = 'N');
	begin
	
	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	dual
	where	((ie_notin_p = 'S') and (', ' || trim(ds_sql_p) || ',' not like '%, ' || cd_tipo_pessoa_w || ',%')) or
		((ie_notin_p = 'N') and (', ' || trim(ds_sql_p) || ',' like '%, ' || cd_tipo_pessoa_w || ',%'));
	
	end;
end loop;
close C01;

return	ds_retorno_w;

end Obter_Tipo_Pessoa_Relat;
/