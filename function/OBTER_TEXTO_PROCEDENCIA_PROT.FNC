create or replace
function obter_texto_procedencia_prot(	nr_seq_protocolo_p	Number)
 		    	return Varchar2 is

ds_retorno_w		varchar2(4000);
ds_procedencia_w	varchar2(40);
qt_contas_w		number(10,0);

Cursor C01 is
	select	substr(obter_desc_procedencia(b.cd_procedencia),1,40) ds_procedencia,
		count(*) qt_contas
	from	conta_paciente a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	a.nr_seq_protocolo = nr_seq_protocolo_p
	group by	b.cd_procedencia
	order by 	ds_procedencia;
	
begin

open C01;
loop
fetch C01 into	
	ds_procedencia_w,
	qt_contas_w;
exit when C01%notfound;
	begin	
	ds_retorno_w	:= substr(ds_retorno_w || ds_procedencia_w || ' = ' || qt_contas_w || chr(10) ,1,4000);	
	end;
end loop;
close C01;

return	ds_retorno_w;

end obter_texto_procedencia_prot;
/