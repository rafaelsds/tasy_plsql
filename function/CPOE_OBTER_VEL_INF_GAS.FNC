create or replace
function cpoe_obter_vel_inf_gas(nr_seq_gas_cpoe_p		cpoe_gasoterapia.nr_sequencia%type)
 		    	return number is

qt_gasoterapia_w	prescr_gasoterapia_evento.qt_gasoterapia%type;

begin

select	max(a.qt_gasoterapia)
into	qt_gasoterapia_w
from	prescr_gasoterapia_evento	a
where	a.nr_seq_gas_cpoe 	= nr_seq_gas_cpoe_p
and		a.ie_evento in ('V','I','R')
and		nvl(a.ie_evento_valido,'S') = 'S'
and		a.nr_sequencia	= (	select	max(x.nr_sequencia)
							from	prescr_gasoterapia_evento	x
							where	x.nr_seq_gas_cpoe 	= nr_seq_gas_cpoe_p
							and		x.ie_evento in ('V','I','R')
							and		nvl(x.ie_evento_valido,'S') = 'S');

return	qt_gasoterapia_w;

end cpoe_obter_vel_inf_gas;
/
