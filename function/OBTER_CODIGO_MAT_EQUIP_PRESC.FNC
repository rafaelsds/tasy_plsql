create or replace
function obter_codigo_mat_equip_presc(cd_sigla_equip_p	varchar2,
					nr_prescricao_p		number,
					nr_seq_prescr_p		number)
					return 	varchar2 is

resultado_w	 		varchar2(100);
cd_exame_integr_w		varchar2(100);

nr_seq_exame_w			number(10);
ie_tipo_atendimento_w		number(10);
ie_urgente_w			varchar2(2);
nr_seq_material_w		number(10);

cd_estabelecimento_w		number(10);
cd_setor_atendimento_w		number(5);

cursor c01 is
	select	nvl((b.cd_material_integr),null)
	from 	equipamento_lab a,
		lab_exame_equip b,
		exame_lab_equip_regra c
	where 	a.cd_equipamento	= b.cd_equipamento 
	and 	upper(a.ds_sigla)	= upper(cd_sigla_equip_p)
	and 	b.nr_seq_exame 		= nr_seq_exame_w
	and	b.nr_sequencia		= c.nr_seq_exame_equip(+)
	and 	((b.nr_seq_material 	= nvl(nr_seq_material_w,b.nr_seq_material)) or (nr_seq_material is null))
	and 	((b.ie_tipo_atendimento	= nvl(ie_tipo_atendimento_w,b.ie_tipo_atendimento)) or (b.ie_tipo_atendimento is null))
	and 	(b.ie_urgente		= nvl(ie_urgente_w,b.ie_urgente) or b.ie_urgente is null)
	and 	(c.cd_estabelecimento	= nvl(cd_estabelecimento_w,c.cd_estabelecimento) or c.cd_estabelecimento is null)
	and 	(c.cd_setor_atendimento	= nvl(cd_setor_atendimento_w,c.cd_setor_atendimento) or c.cd_setor_atendimento is null)
	order 	by nvl(b.ie_tipo_atendimento,0), nvl(b.nr_seq_material,0);

begin

resultado_w :=  null;

select 	max(b.nr_seq_exame), 
	max(e.ie_tipo_atendimento), 
	max(b.ie_urgencia), 
	max(d.nr_sequencia),
	max(a.cd_estabelecimento),
	max(a.cd_setor_atendimento)
into	nr_seq_exame_w,
	ie_tipo_atendimento_w,
	ie_urgente_w,
	nr_seq_material_w,
	cd_estabelecimento_w,
	cd_setor_atendimento_w
from 	atendimento_paciente e,
	material_exame_lab d,
	prescr_procedimento b,
	prescr_medica a
where 	a.nr_atendimento = e.nr_atendimento
and 	a.nr_prescricao = b.nr_prescricao
and	b.cd_material_exame = d.cd_material_exame
and 	a.nr_prescricao = nr_prescricao_p
and	b.nr_sequencia	= nr_seq_prescr_p;


open c01;
loop
fetch c01 into cd_exame_integr_w;
exit when c01%notfound;
	resultado_w := cd_exame_integr_w;
end loop;
close c01;

return resultado_w;

end;
/