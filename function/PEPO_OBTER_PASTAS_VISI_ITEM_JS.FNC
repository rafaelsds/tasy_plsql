create or replace
function pepo_obter_pastas_visi_item_js(
					nr_seq_item_pepo_p	number,
					cd_perfil_p		number
					)
					return varchar2 is
					
					
ds_retorno_w	varchar2(2000) := '';

					
begin


if	(nr_seq_item_pepo_p is not null) then
	begin

	select obter_select_concatenado_bv('select	a.nr_seq_dic_obj
						from	perfil_item_pepo d,
							perfil_item_pront_pasta c,
							prontuario_item_pasta b,
							prontuario_pasta a
						where	a.nr_sequencia = b.nr_seq_pasta_pepo
						and	b.nr_sequencia = c.nr_seq_item_pasta
						and     d.nr_sequencia = c.nr_seq_item_perfil_pepo
						and	d.cd_perfil = :cd_perfil_p						
						and	d.nr_seq_item_pepo = :nr_seq_item_pepo_p
						and	a.ie_situacao = '|| chr(39) || 'A' || chr(39),				
					'cd_perfil_p=' || cd_perfil_p || ';nr_seq_item_pepo_p=' || nr_seq_item_pepo_p, ',' || chr(13))
	into	ds_retorno_w	
	from dual;
	
	end;
end if;

return	ds_retorno_w;

end pepo_obter_pastas_visi_item_js;
/