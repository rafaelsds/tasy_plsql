create or replace function obter_dose_presc_medic(cd_pessoa_fisica_p varchar2,
												  nr_seq_paciente_p number,
												  nr_seq_atendimento_p number,
												  nr_seq_material_p number,
												  cd_material_p number,
												  qt_peso_p number,
												  qt_altura_p number,
												  qt_peso_ideal_p number,
												  qt_fator_correcao_p number,
												  ie_peso_considerar_p varchar2,
												  qt_dose_p number,
												  qt_hora_aplicacao_p number,
												  qt_redutor_sc_p number,
												  pr_reducao_p number,
												  pr_reducao_item_p number,
												  nm_usuario_p varchar2,
												  cd_unidade_med_p varchar2,
												  cd_unid_med_prescr_p varchar2,
												  ie_formula_sup_corporea_p varchar2,
												  ie_via_aplicacao_p varchar2,
												  ie_aplica_reducao_p varchar2,
												  cd_intervalo_p varchar2) return float is

/* function created using the procedure atualizar_dose_onc_protocolo as base */
dose_presc_w 				number(18,6);
temp_dose_presc_w			number(18,6);
qt_superficie_corporea_w	number(15,4);
ie_peso_considerar_w    	varchar2(1)  := 'A';
cd_pessoa_fisica_w 			varchar2(20);
mgcar_desc_w				varchar2(10);
casas_dec_sc_w				number(4, 0);
cd_unidade_med_sec_w		varchar2(15) := '';
qt_mg_carboplatina_w		number(15,4); 
ie_mult_h_aplic_w			varchar2(3);
ie_calcula_preenchido_w		varchar2(3);
cd_unidade_medida_conv_w	varchar2(30);
qt_peso_w					number(10,3);

begin
	Obter_Param_Usuario(281,476,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo,ie_calcula_preenchido_w);

	dose_presc_w := qt_dose_p;
	
	if (nr_seq_atendimento_p is not null) then
		qt_mg_carboplatina_w := obter_qt_mg_carbo_ciclo(nr_seq_atendimento_p);
	else
		qt_mg_carboplatina_w := obter_qt_mg_carboplatina(nr_seq_paciente_p);
	end if;
	
	mgcar_desc_w := obter_desc_expressao(782176);
	casas_dec_sc_w := nvl(obter_numero_casas_sc, 2);

	qt_superficie_corporea_w := round(obter_superficie_corp_red_ped(qt_peso_p, 
												   qt_altura_p, 
												   qt_redutor_sc_p, 
												   cd_pessoa_fisica_p, 
												   nm_usuario_p,
												   ie_formula_sup_corporea_p),casas_dec_sc_w);
												   
	 select max(lower(cd_unidade_med_sec))
     into cd_unidade_med_sec_w
     from unidade_medida
     where cd_unidade_medida = cd_unidade_med_p
     and (cd_unidade_med_princ is not null and cd_unidade_medida <> cd_unidade_med_princ);

	select nvl(upper(ie_mult_h_aplic), 'N')
	into ie_mult_h_aplic_w
	from unidade_medida  
	where cd_unidade_medida = cd_unidade_med_p;
	
	if (cd_unidade_med_sec_w = 'm2' and qt_superficie_corporea_w is not null) then
		dose_presc_w := qt_dose_p * qt_superficie_corporea_w;
	end if;

	if (cd_unidade_med_sec_w = 'mgc' and qt_mg_carboplatina_w is not null and qt_mg_carboplatina_w > 0) then
		dose_presc_w := qt_dose_p * qt_mg_carboplatina_w;
	end if;

	if (cd_unidade_med_sec_w = mgcar_desc_w and qt_mg_carboplatina_w is not null and qt_mg_carboplatina_w > 0) then
		dose_presc_w := qt_mg_carboplatina_w;
	end if;

	if (cd_unidade_med_sec_w in ('m2','mgc','kg', mgcar_desc_w) and ie_calcula_preenchido_w = 'C') then
		dose_presc_w := nvl(Obter_dose_convertida(cd_material_p,qt_dose_p,cd_unidade_med_p,cd_unid_med_prescr_p),dose_presc_w);
	end if;

	qt_peso_w := qt_peso_p;
	
	if (ie_peso_considerar_p is not null) then
		if ('J' = ie_peso_considerar_p) then
			qt_peso_w := obter_peso_ideal_ajust_pac(cd_pessoa_fisica_p, qt_peso_p, qt_fator_correcao_p, qt_altura_p);

		elsif ('I' = ie_peso_considerar_p) then
			qt_peso_w := qt_peso_ideal_p;
		end if;
	end if;

	if (cd_unidade_med_sec_w = 'kg' and qt_peso_w is not null and qt_peso_w > 0) then
		dose_presc_w := qt_dose_p * qt_peso_w;
	end if;

	if (ie_aplica_reducao_p = 'S' and pr_reducao_p is not null and pr_reducao_p > 0) then
		dose_presc_w := dose_presc_w - ((pr_reducao_p * nvl(dose_presc_w, 0)) / 100);
	end if;

	if (ie_mult_h_aplic_w = 'S' and qt_hora_aplicacao_p is not null and qt_hora_aplicacao_p <> 0) then
		dose_presc_w := dose_presc_w * qt_hora_aplicacao_p;
	end if;
	
	if (ie_aplica_reducao_p = 'S' and pr_reducao_item_p is not null and pr_reducao_item_p not in (100, 0)) then
		dose_presc_w := (nvl(pr_reducao_item_p,100) * nvl(dose_presc_w,0)) / 100;
	end if;
	
	temp_dose_presc_w := dose_presc_w;
	Obter_qt_dose_onco_conv(cd_material_p,temp_dose_presc_w,ie_via_aplicacao_p,cd_unidade_med_p,cd_unidade_medida_conv_w,cd_intervalo_p);
	
	if (temp_dose_presc_w is not null and temp_dose_presc_w <> dose_presc_w) then
		dose_presc_w := temp_dose_presc_w;
	end if;
	
return dose_presc_w;

end obter_dose_presc_medic;
/
