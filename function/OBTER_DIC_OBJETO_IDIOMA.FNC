create or replace
function obter_dic_objeto_idioma(
				nr_seq_objeto_p	number,
				nr_seq_idioma_p	number)
 		    	return varchar2 is

ds_objeto_w		varchar2(2000);
ie_tipo_objeto_w	varchar2(15);
begin

if	(nr_seq_objeto_p is null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(279673);
end if;

if	(nr_seq_idioma_p is not null) and (nr_seq_idioma_p > 0) then
	select	max(ds_traducao)
	into	ds_objeto_w
	from	dic_objeto_idioma
	where	nr_seq_objeto = nr_seq_objeto_p
	and	nr_seq_idioma = nr_seq_idioma_p;
	
	if	(ds_objeto_w is not null) and (ds_objeto_w <> ' ') then
		return	ds_objeto_w;
	end if;
end if;

select	ie_tipo_objeto
into	ie_tipo_objeto_w
from	dic_objeto
where	nr_sequencia = nr_seq_objeto_p;

if	(ie_tipo_objeto_w = 'T') then
	select	ds_informacao
	into	ds_objeto_w
	from	dic_objeto
	where	nr_sequencia = nr_seq_objeto_p;
elsif	(ie_tipo_objeto_w = 'AC') then
	select	nm_campo_tela
	into	ds_objeto_w
	from	dic_objeto
	where	nr_sequencia = nr_seq_objeto_p;
else
	select	ds_texto
	into	ds_objeto_w
	from	dic_objeto
	where	nr_sequencia = nr_seq_objeto_p;	
end if;

return	ds_objeto_w;

end obter_dic_objeto_idioma;
/