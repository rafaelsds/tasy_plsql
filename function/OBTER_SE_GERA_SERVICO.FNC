create or replace
function obter_se_gera_servico(	nr_seq_servico_p	number,
				nr_prescricao_p		number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);			
cd_dieta_w	number(10);
			
Cursor C01 is
	select	cd_dieta
	from	prescr_dieta
	where	nr_prescricao = nr_prescricao_p
	AND	NVL(ie_suspenso,'N') <> 'S';
			
begin
ie_retorno_w := 'N';

open C01;
loop
fetch C01 into	
	cd_dieta_w;
exit when C01%notfound;
	begin
	
	if (ie_retorno_w = 'N') then
		
		select 	decode(count(*),0,'S','N')
		into	ie_retorno_w
		from	nut_regra_dieta_servico a
		where	nr_seq_servico = nr_seq_servico_p
		AND	cd_dieta = cd_dieta_w
		AND	NVL(ie_gera_servico,'N') = 'N';
	
	end if;
	end;
end loop;
close C01;

return	ie_retorno_w;

end obter_se_gera_servico;
/