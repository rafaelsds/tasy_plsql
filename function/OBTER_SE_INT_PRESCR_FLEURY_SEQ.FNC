create or replace
function obter_se_int_prescr_fleury_seq(
					nr_prescricao_p         number,
					nr_seq_prescr_p 		number
				)
 		    	return varchar2 is

cd_sigla_w			varchar2(20) := null;
nr_seq_proc_interno_w	number(10);
nr_seq_exame_w		number(10);
qt_regra_w			number(10);
ie_exame_fleury_w	varchar2(2);
nr_seq_prescr_w		number(15);

Cursor C01 is
	select	p.nr_sequencia	
	from	prescr_procedimento p,
			proc_interno i,
			prescr_medica a
	where	a.nr_prescricao = p.nr_prescricao
	and		p.nr_seq_proc_interno = i.nr_sequencia
	and		p.nr_prescricao	= nr_prescricao_p
	and		p.nr_sequencia = nr_seq_prescr_p
	and		a.dt_liberacao_medico is null
	and		p.cd_cgc_laboratorio = '60840055000131'
	and		i.ie_tipo in ('AP','APH','APC');

Cursor C02 is
		select	p.nr_sequencia
		from	exame_laboratorio e,
				prescr_procedimento p
		where	p.nr_prescricao = nr_prescricao_p
		and		e.nr_seq_exame 	= p.nr_seq_exame
		and		p.nr_sequencia = nr_seq_prescr_p
		and		e.cd_cgc_externo = '60840055000131';
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_prescr_w;
exit when C01%notfound;
	begin
	
	select 	max(nr_seq_proc_interno)
	into	nr_seq_proc_interno_w
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia = nr_seq_prescr_w;
	
	if	(nr_seq_proc_interno_w is not null) then

		select	max(cd_integracao)
		into	cd_sigla_w
		from	regra_proc_interno_integra
		where	nr_seq_proc_interno 	= nr_seq_proc_interno_w
		and		ie_tipo_integracao 	= 3;
		
		if	(cd_sigla_w is null)	then
			
			select	count(*)
			into	qt_regra_w
			from	regra_proc_interno_integra
			where	nr_seq_proc_interno = nr_seq_proc_interno_w
			and	ie_tipo_integracao 	= 3;
			
			if	(qt_regra_w > 0) then
			
				select	nvl(max(cd_integracao), to_char(nr_seq_proc_interno_w))	
				into	cd_sigla_w
				from	proc_interno
				where	nr_sequencia = nr_seq_proc_interno_w;
				
			end if;
			
		end if;

	end if;

	if (cd_sigla_w is not null) then
		exit;
	end if;
	
	end;
end loop;
close C01;

if (cd_sigla_w is null) then

	open C02;
	loop
	fetch C02 into	
		nr_seq_prescr_w;
	exit when C02%notfound;
		begin
		
		select	decode(count(*),0,'N','S')
		into	ie_exame_fleury_w
		from	exame_laboratorio e,
				prescr_procedimento p
		where	p.nr_prescricao = nr_prescricao_p
		and		p.nr_sequencia = nr_seq_prescr_w
		and		e.nr_seq_exame 	= p.nr_seq_exame
		and		e.cd_cgc_externo = '60840055000131';
		
		if	(ie_exame_fleury_w = 'S') then
			cd_sigla_w := obter_codigo_exame_equip_presc('FLEURY', nr_prescricao_p, nr_seq_prescr_w);    
		end if;
		
		if (cd_sigla_w is not null) then
			exit;
		end if;
		
		end;
	end loop;
	close C02;

end if;

if (cd_sigla_w is null) then
	cd_sigla_w := '0';
end if;

return	cd_sigla_w;

end obter_se_int_prescr_fleury_seq;
/