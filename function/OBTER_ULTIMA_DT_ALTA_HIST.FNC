create or replace 
function obter_ultima_dt_alta_hist (nr_atendimento_p	number)
 		    	return date is
			
dt_retorno_w		Date;			
nr_sequencia_w		number(15);

begin

if (nr_atendimento_p is not null) then

	select 	max(nr_sequencia)
	into	nr_sequencia_w
	from 	ATEND_ALTA_HIST
	where 	nr_atendimento = nr_atendimento_p
	and	ie_alta_estorno = 'A';
	
	select 	max(dt_alta)
	into	dt_retorno_w
	from	ATEND_ALTA_HIST
	where 	nr_atendimento = nr_atendimento_p
	and	nr_sequencia = nr_sequencia_w;
	
end if;

return	dt_retorno_w;

end obter_ultima_dt_alta_hist;
/
