create or replace
function ctb_obter_se_estrut_orc_lib(	nr_seq_estrut_orc_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_lib_p		varchar2)
				return			varchar2 is

ie_permite_w			varchar2(1);

cd_perfil_ativo_w			number(5);
cd_perfil_w			number(5);
qt_registro_w			number(10);
nm_usuario_lib_w			varchar2(15);

cursor	c01 is
select	nvl(ie_permite, 'N'),
	cd_perfil,
	nm_usuario_lib
from	ctb_tipo_estrut_orc_lib
where	nr_seq_estrut_orc			= nr_seq_estrut_orc_p
and	nvl(cd_perfil, cd_perfil_ativo_w)	= cd_perfil_ativo_w
and	nvl(nm_usuario_lib, nm_usuario_lib_p)	= nm_usuario_lib_p
order by	nvl(nm_usuario_lib, 'A'),
		nvl(cd_perfil, 0);


begin

cd_perfil_ativo_w			:= nvl(obter_perfil_ativo, 0);
if	(nvl(cd_perfil_ativo_w, 0) = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(266454);
end if;

ie_permite_w			:= 'S';

select	count(*)
into	qt_registro_w
from	ctb_tipo_estrut_orc_lib
where	nr_seq_estrut_orc	= nr_seq_estrut_orc_p;

if	(qt_registro_w > 0) then

	ie_permite_w	:= 'N';

	open C01;
	loop
	fetch C01 into	
		ie_permite_w,
		cd_perfil_w,
		nm_usuario_lib_w;
	exit when C01%notfound;
		begin
		
		if	(nm_usuario_lib_w is null) and
			(cd_perfil_w is not null) then
			
			select	nvl(max('S'), 'N')
			into	ie_permite_w
			from	usuario_perfil b
			where	b.cd_perfil	= cd_perfil_w
			and	b.nm_usuario	= nm_usuario_lib_p
			and	ie_permite_w	= 'S';
			
		end if;
		ie_permite_w	:= ie_permite_w;
		end;
	end loop;
	close C01;

end if;

return	ie_permite_w;

end ctb_obter_se_estrut_orc_lib;
/