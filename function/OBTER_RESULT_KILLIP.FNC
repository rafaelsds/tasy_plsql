create or replace
function obter_result_killip(ie_killip_p	varchar2)
			return varchar2 is

begin
if	(ie_killip_p = 'I') then
	return '6';
elsif	(ie_killip_p = 'II') then
	return '17';
elsif	(ie_killip_p = 'III') then
	return '38';
elsif	(ie_killip_p = 'IV') then
	return '81';
else
	return '';
end if;

end obter_result_killip;
/
