create or replace
function qr_obter_status_param(nr_sequencia_p		number,
								cd_funcao_p		number,
								ie_opcao_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
qt_reg_w		number(10);
ie_retorno_w	varchar2(255);

/*
P = Pendente
E = Erro
S = OK
*/

begin

ds_retorno_w	:= obter_desc_expressao(295406);
ie_retorno_w	:= 'P';


select	count(*)
into	qt_reg_w
from	qr_parametro_result
where	ie_finalizado = 'N'
and		nr_seq_param = nr_sequencia_p
and		cd_funcao = cd_funcao_p;

if	(qt_reg_w > 0) then
	ds_retorno_w := obter_desc_expressao(289370);	
	ie_retorno_w := 'E';	
else
	select	count(*)
	into	qt_reg_w
	from	qr_parametro_result
	where	ie_finalizado = 'S'
	and		ie_tipo_resultado = 'S'
	and		cd_funcao = cd_funcao_p
	and		nr_seq_param = nr_sequencia_p;
	
	if	(qt_reg_w > 0) then		
		ds_retorno_w	:= obter_desc_expressao(311702);		
		ie_retorno_w	:= 'S';		
	end if;	
end if;	

if	(ie_opcao_p <> 'D') then
	ds_retorno_w := ie_retorno_w;
end if;

return	ds_retorno_w;


end qr_obter_status_param;
/