CREATE OR REPLACE FUNCTION obter_situacao_medico(cd_pessoa_fisica_p	VARCHAR)                      
	RETURN VARCHAR2 IS                                                             
ie_retorno_w	VARCHAR2(1);                                                       
BEGIN                                                                           
SELECT	MAX(ie_situacao)                                                         
INTO	ie_retorno_w                                                               
FROM	medico                                                                     
WHERE	cd_pessoa_fisica = cd_pessoa_fisica_p;                                    
RETURN	ie_retorno_w;                                                            
END	obter_situacao_medico;
/