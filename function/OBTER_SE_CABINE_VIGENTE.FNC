create or replace 
function obter_se_cabine_vigente (	nr_seq_horario_p 	 in prescr_mat_hor.nr_sequencia%type,
									nr_prescricao_p		 in prescr_mat_hor.nr_prescricao%type,
									dt_horario_p		 in prescr_mat_hor.dt_horario%type,
									nr_seq_area_prep_p 	 in adep_area_horario_atend.nr_seq_area%type,
									cd_estabelecimento_p in estabelecimento.cd_estabelecimento%type,
									nm_usuario_p 		 in usuario.nm_usuario%type,
									qt_min_inicio_p		 in adep_regra_area_prep.qt_min_inicio%type default null )
				return varchar2 is

dt_horario_w        prescr_mat_hor.dt_horario%type;
dt_inicial_w        adep_area_horario_atend.hr_inicial%type;
dt_final_w          adep_area_horario_atend.hr_final%type;
ie_dia_semana_w     adep_area_horario_atend.ie_dia_semana%type;
ie_dia_semana_ww    number;
cd_formato_w        locale_formats.id_mask%type;
ie_regra_cabine_w   varchar2(1 char);
ie_prescr_copia_w   varchar2(1 char);
nr_seq_mat_cpoe_w   prescr_material.nr_seq_mat_cpoe%type;
dt_lib_medico_w     prescr_medica.dt_liberacao_medico%type;

cursor c01 is
select  hr_inicial,
        hr_final,
        ie_dia_semana
from    adep_area_horario_atend
where   nr_seq_area = nr_seq_area_prep_p
and     ((ie_dia_semana_ww = ie_dia_semana) or
        (ie_dia_semana = 9 and ie_dia_semana_ww not in (1, 7)))
order by ie_dia_semana desc;

begin

select  nvl(max('S'),'N') ie_regra_cabine
into    ie_regra_cabine_w
from    adep_area_horario_atend a
where   a.nr_seq_area = nr_seq_area_prep_p;

if ( ie_regra_cabine_w = 'S' ) then
    
    select  max(a.dt_liberacao_medico)
    into    dt_lib_medico_w
    from    prescr_medica a
    where   a.nr_prescricao = nr_prescricao_p;

    if ( nr_prescricao_p is not null ) then
        
        select  max(a.nr_seq_mat_cpoe)
        into    nr_seq_mat_cpoe_w
        from    prescr_material a
        where   a.nr_prescricao = nr_prescricao_p;

        if ( nr_seq_mat_cpoe_w is not null ) then
            
            select decode(count(a.nr_prescricao), 1, 'N', 'S') ie_prescr_copia
            into   ie_prescr_copia_w
            from ( select z.nr_prescricao
                   from   prescr_material z
                   where  z.nr_seq_mat_cpoe   = nr_seq_mat_cpoe_w
                   group by z.nr_prescricao ) a;
        else
            ie_prescr_copia_w := 'N';
        end if;
    end if;

    if ( nr_seq_horario_p is not null
         and dt_horario_p is not null
         and nr_seq_area_prep_p is not null
         and nm_usuario_p is not null
         and cd_estabelecimento_p is not null
         and dt_lib_medico_w is not null) then

        ie_dia_semana_ww := pkg_date_utils.get_weekday(dt_lib_medico_w);

        for c01_w in c01
        loop
            dt_inicial_w    := c01_w.hr_inicial;
            dt_final_w      := c01_w.hr_final;
            ie_dia_semana_w := c01_w.ie_dia_semana;
        end loop;

        if ( ie_dia_semana_w is not null ) then
            if ( dt_inicial_w is null ) then
                dt_inicial_w := pkg_date_utils.start_of(sysdate, 'DAY');
            end if;

            cd_formato_w    := 'shortTime';
            dt_inicial_w    := obter_data_formato(dt_inicial_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w);
            dt_final_w      := obter_data_formato(dt_final_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w);
            cd_formato_w    := 'timestamp';
            dt_inicial_w    := obter_data_formato(dt_lib_medico_w, cd_estabelecimento_p, nm_usuario_p, 'shortDate') +
                extract(hour from cast(obter_data_formato(dt_inicial_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w) as timestamp)) / 24 +
                extract(minute from cast(obter_data_formato(dt_inicial_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w) as timestamp)) / 1440;
            dt_lib_medico_w := obter_data_formato(dt_lib_medico_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w);
            dt_horario_w    := obter_data_formato(dt_horario_p, cd_estabelecimento_p, nm_usuario_p, cd_formato_w);
            
            if ( dt_final_w is null ) then
                dt_final_w := obter_data_formato(dt_inicial_w, cd_estabelecimento_p, nm_usuario_p, 'shortDate') + '1' +
                    extract(hour from cast(obter_data_formato(dt_inicial_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w) as timestamp)) / 24 +
                    extract(minute from cast(obter_data_formato(dt_inicial_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w) as timestamp)) / 1440;
            else
                dt_final_w := obter_data_formato(dt_lib_medico_w, cd_estabelecimento_p, nm_usuario_p, 'shortDate') +
                    extract(hour from cast(obter_data_formato(dt_final_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w) as timestamp)) / 24 +
                    extract(minute from cast(obter_data_formato(dt_final_w, cd_estabelecimento_p, nm_usuario_p, cd_formato_w) as timestamp)) / 1440;
            end if;

            if ( ((dt_horario_w not between dt_inicial_w and dt_final_w)
                 and (dt_horario_w < dt_inicial_w))
                 or (dt_lib_medico_w >= dt_inicial_w
                    and ie_prescr_copia_w = 'N') ) then
                return 'N';
            end if;

            if ( ((dt_horario_w between dt_inicial_w and dt_final_w)
                 and (dt_horario_w >= dt_inicial_w))
                 or (dt_lib_medico_w < dt_inicial_w
                    and ie_prescr_copia_w = 'S') ) then
                return 'S';
            end if;
        end if;
    end if;
else
    return 'S';
end if;

return 'N';

end obter_se_cabine_vigente;
/
