create or replace
function Obter_desc_nut_perfil(nr_seq_pefil_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(80);
			
begin

select	ds_perfil
into	ds_retorno_w
from	nut_perfil
where	nr_sequencia	= nr_seq_pefil_p;

return	ds_retorno_w;

end	Obter_desc_nut_perfil;
/