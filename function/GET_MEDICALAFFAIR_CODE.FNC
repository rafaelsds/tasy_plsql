create or replace function get_medicalaffair_code (
    ie_type_message_p   nais_conversion_master.ie_type_message%type,
    nm_tabela_p         nais_conversion_master.nm_tasy_table%type,
    nm_attribute_p      nais_conversion_master.nm_tasy_column%type,
    int_tasy_code_p     nais_conversion_master.vl_tasy_code%type,
    cd_body_part_p     nais_conversion_master.cd_body_part%type default null,
    cd_direction_p     nais_conversion_master.cd_direction%type default null
) return nais_conversion_master%rowtype as
    l_med_rec nais_conversion_master%rowtype;
begin
    begin
        select  *
        into  l_med_rec
        from  nais_conversion_master
        where sysdate between dt_valid_start and dt_valid_end
        and   ie_send_flaf = 'S'
        and   ie_type_message = ie_type_message_p
        and   nm_tasy_table = nm_tabela_p
        and   nm_tasy_column = nm_attribute_p
        and   vl_tasy_code = int_tasy_code_p
        and   nvl(cd_body_part,0) = nvl(cd_body_part_p,0)
        and   nvl(cd_direction,0) = nvl(cd_direction_p,0);
            

    exception
        when no_data_found then
            l_med_rec := null;
    end;

    return l_med_rec;
end;
/