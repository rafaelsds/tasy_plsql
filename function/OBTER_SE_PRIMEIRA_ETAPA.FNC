create or replace
function obter_se_primeira_etapa(	nr_prescricao_p		number,
									nr_seq_superior_p	number,
									dt_horario_p		date) return varchar2 is

ds_retorno_w	varchar2(1);
nr_etapa_sol_w  number;
									
begin
	select 	nr_etapa_sol
	into	nr_etapa_sol_w
	from 	prescr_mat_hor 
	where 	nr_prescricao = nr_prescricao_p
	and   	nr_seq_material = nr_seq_superior_p 
	and   	to_char(dt_horario, 'dd/mm/yyyy hh24:mi:ss') = to_char(dt_horario_p, 'dd/mm/yyyy hh24:mi:ss');
	if (nvl(nr_etapa_sol_w,0) = 1) then
		ds_retorno_w := 'S';
	else
		ds_retorno_w := 'N';
	end if;
	
	return ds_retorno_w; 
end obter_se_primeira_etapa;
/