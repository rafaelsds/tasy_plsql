create or replace
function obter_se_apresenta_checklist (	nr_seq_grupo_p	number,
										nr_seq_check_list_p	number)
 		    	return varchar2 is

nr_seq_sl_unid_w sl_check_list_unid.nr_seq_sl_unid%type;
nr_seq_servico_w sl_unid_atend.nr_seq_servico%type;

ds_retorno_w	varchar2(1) := 'S';
qt_regra_grupo_w number(1);

begin

select 	count(1)
into	qt_regra_grupo_w
from 	SL_GRUP_CHECK_LIST_LIB
where	nr_seq_regra = nr_seq_grupo_p
and		rownum = 1
and		ie_situacao = 'A';
		
if  (nvl(qt_regra_grupo_w,0) > 0) and	
	(nvl(nr_seq_check_list_p,0) > 0) then	

	begin
	
		select	nr_seq_sl_unid
		into	nr_seq_sl_unid_w
		from	SL_CHECK_LIST_UNID
		where	nr_sequencia = nr_seq_check_list_p;	
		
		select	nr_seq_servico
		into	nr_seq_servico_w
		from	sl_unid_atend
		where	nr_sequencia = nr_seq_sl_unid_w;
		
		begin
			select 	'S'
			into	ds_retorno_w
			from 	SL_GRUP_CHECK_LIST_LIB
			where	nr_seq_regra = nr_seq_grupo_p
			and		nr_seq_servico = nr_seq_servico_w
			and		ie_situacao = 'A';
		exception
		when others then
			ds_retorno_w := 'N';
		end;
		
	exception
	when others then
		nr_seq_sl_unid_w := 0;
	end;

end if;

return	ds_retorno_w;

end obter_se_apresenta_checklist;
/