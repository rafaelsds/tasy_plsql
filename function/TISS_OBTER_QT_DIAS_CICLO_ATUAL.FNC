create or replace
function Tiss_Obter_qt_dias_ciclo_atual(nr_seq_paciente_setor_p number,
					nr_ciclo_p		number)
 		    	return number is
qt_dia_ciclo_w	number(10);

/*
NR_SEQ_PACIENTE_SETOR_P = AUTORIZACAO_CONVENIO.NR_SEQ_PACIENTE_SETOR
NR_CICLO		              = AUTORIZACAO_CONVENIO.NR_CICLO
*/


begin

select	count(*)
into	qt_dia_ciclo_w
from	paciente_atendimento
where	nr_seq_paciente = nr_seq_paciente_setor_p
and	nr_ciclo	= nr_ciclo_p;

return	qt_dia_ciclo_w;

end Tiss_Obter_qt_dias_ciclo_atual;
/