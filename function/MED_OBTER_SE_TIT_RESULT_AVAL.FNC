create or replace
function Med_Obter_Se_Tit_Result_Aval
		(nr_seq_avaliacao_p	number,
		nr_seq_tipo_aval_p	number,
		nr_seq_titulo_p		number)
		return varchar2 is

ds_retorno_w		varchar2(01);
nr_seq_item_niv1_w	number(10,0);
nr_seq_item_niv2_w	number(10,0);
nr_seq_item_niv3_w	number(10,0);
nr_seq_item_niv4_w	number(10,0);
nr_seq_item_niv5_w	number(10,0);

cursor	c01 is
	select	nr_sequencia
	from	med_item_avaliar
	where	nr_seq_tipo	= nr_seq_tipo_aval_p
	and	nr_seq_superior	= nr_seq_titulo_p;

cursor	c02 is
	select	nr_sequencia
	from	med_item_avaliar
	where	nr_seq_tipo	= nr_seq_tipo_aval_p
	and	nr_seq_superior	= nr_seq_item_niv1_w;

cursor	c03 is
	select	nr_sequencia
	from	med_item_avaliar
	where	nr_seq_tipo	= nr_seq_tipo_aval_p
	and	nr_seq_superior	= nr_seq_item_niv2_w;

cursor	c04 is
	select	nr_sequencia
	from	med_item_avaliar
	where	nr_seq_tipo	= nr_seq_tipo_aval_p
	and	nr_seq_superior	= nr_seq_item_niv3_w;

cursor	c05 is
	select	nr_sequencia
	from	med_item_avaliar
	where	nr_seq_tipo	= nr_seq_tipo_aval_p
	and	nr_seq_superior	= nr_seq_item_niv4_w;

begin

ds_retorno_w	:= 'N';

open	c01;
loop
fetch	c01 into nr_seq_item_niv1_w;
exit	when c01%notfound;
	begin

	select	nvl(max('S'), 'N')
	into	ds_retorno_w
	from	med_avaliacao_result
	where	nr_seq_avaliacao	= nr_seq_avaliacao_p
	and	nr_seq_item		= nr_seq_item_niv1_w;

	if	(ds_retorno_w	= 'S') then
		exit;
	elsif	(ds_retorno_w	= 'N') then
		begin

		open	c02;
		loop
		
		fetch	c02 into nr_seq_item_niv2_w;
		exit	when c02%notfound;
			begin

			select	nvl(max('S'), 'N')
			into	ds_retorno_w
			from	med_avaliacao_result
			where	nr_seq_avaliacao	= nr_seq_avaliacao_p
			and	nr_seq_item		= nr_seq_item_niv2_w;

			if	(ds_retorno_w	= 'S') then
				exit;
			elsif	(ds_retorno_w	= 'N') then
				begin
		
				open	c03;
				loop
		
				fetch	c03 into nr_seq_item_niv3_w;
				exit	when c03%notfound;
					begin
		
					select	nvl(max('S'), 'N')
					into	ds_retorno_w
					from	med_avaliacao_result
					where	nr_seq_avaliacao	= nr_seq_avaliacao_p
					and	nr_seq_item		= nr_seq_item_niv3_w;
			
					if	(ds_retorno_w	= 'S') then
						exit;
					elsif	(ds_retorno_w	= 'N') then
						begin
		
						open	c04;
						loop
		
						fetch	c04 into nr_seq_item_niv4_w;
						exit	when c04%notfound;
							begin
		
							select	nvl(max('S'), 'N')
							into	ds_retorno_w
							from	med_avaliacao_result
							where	nr_seq_avaliacao	= nr_seq_avaliacao_p
							and	nr_seq_item		= nr_seq_item_niv4_w;

		
							if	(ds_retorno_w	= 'S') then
								exit;
							elsif	(ds_retorno_w	= 'N') then
								begin
			
								open	c05;
								loop
		
								fetch	c05 into nr_seq_item_niv5_w;
								exit	when c05%notfound;
									begin
		
									select	nvl(max('S'), 'N')
									into	ds_retorno_w
									from	med_avaliacao_result
									where	nr_seq_avaliacao	= nr_seq_avaliacao_p
									and	nr_seq_item		= nr_seq_item_niv5_w;
	
			
								end;
								end loop;
								close c05;
								end;
							end if;

							end;
						end loop;

						close c04;
			
						end;
					end if;

					end;
				end loop;
				close c03;

				end;
			end if;
			end;
		end loop;
		close c02;
		end;
	end if;

	end;
end loop;
close c01;

return	ds_retorno_w;

end Med_Obter_Se_Tit_Result_Aval;
/