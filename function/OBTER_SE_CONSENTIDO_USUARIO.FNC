create or replace FUNCTION OBTER_SE_CONSENTIDO_USUARIO (NM_USUARIO_P IN VARCHAR2,
                                                        NR_SEQUENCIA_P IN NUMBER)
                                        RETURN VARCHAR2 IS 
qt_registros number;
nr_seq_doc_w number;
ret varchar2(1);
BEGIN
    select count(*) into qt_registros
    from 
        documento_aprov_prof 
    where
        nr_sequencia = NR_SEQUENCIA_P
    and
        UPPER(ie_status) = 'A'
    and
        obter_codigo_usuario(NM_USUARIO_P) = cd_pessoa_fisica;

    if(qt_registros > 0) then
        ret := 'S';
    else 
        select nr_seq_doc into nr_seq_doc_w
        from  
            documento_aprov_prof
        where
            nr_sequencia = NR_SEQUENCIA_P;
            
        select count(*) into qt_registros
        from
            documento_aprov_prof
        where
            nr_seq_doc = nr_seq_doc_w 
        and
            UPPER(ie_status) = 'A'
        and
            obter_codigo_usuario(NM_USUARIO_P) = cd_pessoa_fisica;
            
        if(qt_registros > 0) then
            ret := 'A';
        else
            ret := 'N';
        end if;
    end if;
    RETURN ret;
END OBTER_SE_CONSENTIDO_USUARIO;
/