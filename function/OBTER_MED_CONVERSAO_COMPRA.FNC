create or replace
function	obter_med_conversao_compra(	cd_material_p		number,
					cd_cgc_p		varchar2,
					ie_opcao_p		varchar2,
					cd_estabelecimento_p	number)
return varchar2 is

ds_retorno_w			varchar(25);
qt_conv_fornec_w			number(10);
qt_conv_compra_w			number(14,3);
cd_medida_compra_w		varchar2(25);


begin

select	count(*)
into	qt_conv_fornec_w
from	material_fornec
where	cd_cgc			= cd_cgc_p
and	cd_material		= cd_material_p
and	nvl(ie_ressuprimento,'N') 	= 'S';

if	(cd_cgc_p is not null) and
	(qt_conv_fornec_w > 0) then
	begin
	
	select	cd_unid_medida,
		qt_conv_compra_est
	into	cd_medida_compra_w,
		qt_conv_compra_w
	from	material_fornec
	where	cd_cgc = cd_cgc_p
	and	cd_material = cd_material_p
	and	nvl(ie_ressuprimento,'N') = 'S'
	and	cd_estabelecimento = cd_estabelecimento_p
	and	nr_sequencia = (
		select	max(x.nr_sequencia)
		from	material_fornec x
		where	x.cd_material = cd_material_p
		and	x.cd_cgc = cd_cgc_p
		and	x.cd_estabelecimento = cd_estabelecimento_p
		and	nvl(x.ie_ressuprimento,'N') = 'S');
	end;		
end if;

if ((qt_conv_fornec_w = 0) or (cd_cgc_p is null)) then
	begin
	
	select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,255) cd_unidade_medida_compra,
		qt_conv_compra_estoque
	into	cd_medida_compra_w,
		qt_conv_compra_w
	from	material
	where	cd_material = cd_material_p;
	
	end;

end if;

if	(ie_opcao_p = 'Q') then
	ds_retorno_w := qt_conv_compra_w;
elsif	(ie_opcao_p = 'U') then
	ds_retorno_w := cd_medida_compra_w;
end if;

return ds_retorno_w;

end obter_med_conversao_compra;
/