create or replace 
function cpoe_get_comment_proc(	nr_seq_comment_p		in	cpoe_std_comment.nr_sequencia%type,
								nr_seq_proc_interno_p	in	cpoe_procedimento.nr_seq_proc_interno%type)
								
						return varchar2 is
						
vl_retorno			varchar2(20) := 'S';
is_show_comment_w	pls_integer;
has_proc_w			pls_integer;
nr_seq_order_type_w	cpoe_std_comment.nr_seq_order_type%type;
has_type_w			pls_integer;
nr_seq_estrutura_w	pls_integer;
has_structure_w		pls_integer;

cursor c01 is

select				nr_sequencia
from				pi_estrutura
connect by prior	nr_sequencia = nr_seq_estrut_sup
start with			nr_sequencia in (select		nr_seq_estrutura
									from		cpoe_std_comment_proc
									where		nr_seq_std_comment = nr_seq_comment_p)
and		ie_situacao = 'A';

begin

if (nr_seq_comment_p is not null and nr_seq_proc_interno_p is not null) then

	select	nr_seq_order_type
	into	nr_seq_order_type_w
	from	cpoe_std_comment a
	where	a.nr_sequencia	=	nr_seq_comment_p;

	if (nr_seq_order_type_w is not null) then
	
		select count(1)
		into	has_type_w
		from	proc_order_type
		where	nr_Seq_proc_interno =  nr_seq_proc_interno_p
		and 	nr_seq_order_type = nr_seq_order_type_w
		and		nvl(cd_estabelecimento,1) = obter_estabelecimento_ativo;
		
			
		if (has_type_w > 0 ) then
			
			select	count(1)
			into	is_show_comment_w
			from	cpoe_std_comment_proc
			where	nr_seq_std_comment = nr_seq_comment_p;
			
			if (is_show_comment_w >= 1) then

				select	count(1)
				into	has_proc_w
				from	cpoe_std_comment_proc
				where	nr_seq_std_comment = nr_seq_comment_p
				and		nr_seq_proc_interno in (nr_seq_proc_interno_p);
	
				if (has_proc_w = 0) then
					
					select	count(1)
					into	nr_seq_estrutura_w
					from	cpoe_std_comment_proc
					where	nr_seq_std_comment = nr_seq_comment_p;
					
					if (nr_seq_estrutura_w >= 1) then
					
						for r_c01 in c01 loop
	
							begin
							
							select	count(1)
							into	has_structure_w
							from	pi_estrutura_cad
							where	nr_seq_estrutura = r_c01.nr_Sequencia
							and		nr_Seq_proc_int in (nr_Seq_proc_interno_p);
							
							
							exit when has_structure_w > 0;
									
							end;
							
							
						end loop;
						
						if (has_structure_w = 0) then
						
							vl_retorno := 'N';
							
						else
						
							vl_retorno := 'S';
							
						end if;
						
					else
					
						vl_retorno := 'N';
						
					end if;	
	
				else
				
					vl_retorno := 'S';
					
				end if;
				
			end if;
		
		else
		
			vl_retorno := 'N';
			
		end if;

	end if;
	
end if;

return vl_retorno;

end cpoe_get_comment_proc;
/
