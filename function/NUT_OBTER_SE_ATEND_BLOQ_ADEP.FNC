create or replace
function nut_obter_se_atend_bloq_Adep(	dt_inicial_p		date,
					dt_final_p		date,
					cd_setor_atendimento_p	number,
					nr_atendimento_p	number,
					ie_info_p		varchar2)
					return varchar2 is

/*
D - Data de Bloqueio
M - Motivo de bloqueio
U - Usu�rio de bloqueio
*/

ds_retorno_w			varchar2(4000);
ie_utiliza_servico_adep_w	varchar2(1);
			
begin

obter_param_usuario(1113, 309, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_utiliza_servico_adep_w);

if (ie_info_p = 'D') then
	if (ie_utiliza_servico_adep_w = 'N') then
		select	max(to_char(c.dt_bloqueio,'dd/mm/yyyy hh24:mi:ss'))
		into	ds_retorno_w
		from	nut_Atend_Serv_dia a,
			nut_Atend_serv_dia_rep b,
			prescr_dieta_hor c
		where	a.nr_sequencia = b.nr_seq_serv_dia
		and	a.dt_servico between trunc(dt_inicial_p,'dd') and trunc(dt_final_p,'dd') + 86399/86400
		and	a.cd_setor_Atendimento = cd_setor_Atendimento_p
		and	a.nr_atendimento = nr_Atendimento_p
		and	b.nr_prescr_oral = c.nr_prescricao
		and	to_char(a.dt_servico,'dd/mm/yyyy hh24:mi') = to_char(c.dt_horario,'dd/mm/yyyy hh24:mi')
		and	c.dt_bloqueio is not null
		and	(c.dt_desbloqueio_dieta is null or c.dt_desbloqueio_dieta >= sysdate)
		and	c.dt_suspensao is null;
	elsif (ie_utiliza_servico_adep_w = 'S') then
		select	max(to_char(a.dt_bloqueio,'dd/mm/yyyy hh24:mi:ss'))
		into	ds_retorno_w
		from	nut_Atend_Serv_dia a
		where	a.dt_servico between trunc(dt_inicial_p,'dd') and trunc(dt_final_p,'dd') + 86399/86400
		and		a.cd_setor_Atendimento = cd_setor_Atendimento_p
		and		a.nr_atendimento = nr_Atendimento_p
		and		a.dt_bloqueio is not null
		and		(a.dt_desbloqueio is null or a.dt_desbloqueio >= sysdate)
		and		a.dt_suspensao is null;
	end if;
	
	if (ds_retorno_w is null) then		
		SELECT	MAX(TO_CHAR(dt_inicio,'dd/mm/yyyy hh24:mi:ss'))
		INTO	ds_retorno_w
		FROM	nut_atend_serv_dia a,
			prescr_medica b,
			rep_jejum c
		WHERE	a.nr_atendimento = b.nr_atendimento
		AND	b.nr_prescricao = c.nr_prescricao
		AND	b.dt_liberacao IS NOT NULL
		AND	b.dt_suspensao IS NULL
		AND	NVL(c.ie_suspenso,'N') = 'N' 
		AND	a.dt_servico BETWEEN b.dt_inicio_prescr AND b.dt_validade_prescr
		AND	a.nr_atendimento = nr_atendimento_p
		AND	a.cd_setor_atendimento = cd_setor_atendimento_p
		and	nvl(c.dt_fim,sysdate) > sysdate
		AND	a.dt_servico BETWEEN TRUNC(dt_inicial_p,'dd') AND TRUNC(dt_final_p,'dd') + 86399/86400;	
	end if;
	
	
elsif (ie_info_p = 'M') then
	if (ie_utiliza_servico_adep_w = 'N') then
		select	max(d.ds_justificativa)
		into	ds_retorno_w
		from	nut_Atend_Serv_dia a,
			nut_Atend_serv_dia_rep b,
			prescr_dieta_hor c,
			dieta_justif_bloqueio d
		where	a.nr_sequencia = b.nr_seq_serv_dia
		and	a.dt_servico between trunc(dt_inicial_p,'dd') and trunc(dt_final_p,'dd') + 86399/86400
		and	a.cd_setor_Atendimento = cd_setor_Atendimento_p
		and	a.nr_atendimento = nr_Atendimento_p
		and	b.nr_prescr_oral = c.nr_prescricao
		and	c.nr_seq_justif_bloqueio = d.nr_sequencia
		and	c.dt_suspensao is null
		and	to_char(a.dt_servico,'dd/mm/yyyy hh24:mi') = to_char(c.dt_horario,'dd/mm/yyyy hh24:mi')
		and	c.dt_bloqueio = 	(select max(e.dt_bloqueio) 
						from	prescr_dieta_hor e 
						where	e.nr_prescricao = c.nr_prescricao
						and	to_char(a.dt_servico,'dd/mm/yyyy hh24:mi') = to_char(e.dt_horario,'dd/mm/yyyy hh24:mi'))
		and	c.dt_bloqueio is not null
		and	(c.dt_desbloqueio_dieta is null or c.dt_desbloqueio_dieta >= sysdate);
	elsif (ie_utiliza_servico_adep_w = 'S') then
		select	max(d.ds_justificativa)
		into	ds_retorno_w
		from	nut_Atend_Serv_dia a,
			dieta_justif_bloqueio d
		where	a.dt_servico between trunc(dt_inicial_p,'dd') and trunc(dt_final_p,'dd') + 86399/86400
		and	a.cd_setor_Atendimento = cd_setor_Atendimento_p
		and	a.nr_atendimento = nr_Atendimento_p
		and	a.nr_seq_justif_bloqueio = d.nr_sequencia
		and	a.dt_suspensao is null
		and	a.dt_bloqueio = 	(select max(e.dt_bloqueio) 
						from	nut_Atend_Serv_dia e 
						where	e.dt_servico between trunc(dt_inicial_p,'dd') and trunc(dt_final_p,'dd') + 86399/86400
						and	e.cd_setor_Atendimento = cd_setor_Atendimento_p
						and	e.nr_atendimento = nr_Atendimento_p
						and	e.dt_suspensao is null
						and	to_char(a.dt_servico,'dd/mm/yyyy hh24:mi') = to_char(e.dt_servico,'dd/mm/yyyy hh24:mi')
						and	e.dt_bloqueio is not null
						and	(a.dt_desbloqueio is null or a.dt_desbloqueio >= sysdate))
		and	a.dt_bloqueio is not null
		and	(a.dt_desbloqueio is null or a.dt_desbloqueio >= sysdate);
	end if;
elsif (ie_info_p = 'U') then
	if (ie_utiliza_servico_adep_w = 'N') then
		select	max(c.nm_usuario_bloqueio)
		into	ds_retorno_w
		from	nut_Atend_Serv_dia a,
			nut_Atend_serv_dia_rep b,
			prescr_dieta_hor c
		where	a.nr_sequencia = b.nr_seq_serv_dia
		and	a.dt_servico between trunc(dt_inicial_p,'dd') and trunc(dt_final_p,'dd') + 86399/86400
		and	a.cd_setor_Atendimento = cd_setor_Atendimento_p
		and	a.nr_atendimento = nr_Atendimento_p
		and	b.nr_prescr_oral = c.nr_prescricao
		and	c.dt_suspensao is null
		and	to_char(a.dt_servico,'dd/mm/yyyy hh24:mi') = to_char(c.dt_horario,'dd/mm/yyyy hh24:mi')
		and	c.dt_bloqueio = 	(select max(e.dt_bloqueio) 
						from	prescr_dieta_hor e 
						where	e.nr_prescricao = c.nr_prescricao
						and	to_char(a.dt_servico,'dd/mm/yyyy hh24:mi') = to_char(e.dt_horario,'dd/mm/yyyy hh24:mi'))
		and	c.dt_bloqueio is not null
		and	(c.dt_desbloqueio_dieta is null or c.dt_desbloqueio_dieta >= sysdate);
	elsif (ie_utiliza_servico_adep_w = 'S') then
		select	max(a.nm_usuario_bloqueio)
		into	ds_retorno_w
		from	nut_Atend_Serv_dia a
		where	a.dt_servico between trunc(dt_inicial_p,'dd') and trunc(dt_final_p,'dd') + 86399/86400
		and	a.cd_setor_Atendimento = cd_setor_Atendimento_p
		and	a.nr_atendimento = nr_Atendimento_p
		and	a.dt_suspensao is null
		and	a.dt_bloqueio = 	(select max(e.dt_bloqueio) 
						from	nut_Atend_Serv_dia e 
						where	e.dt_servico between trunc(dt_inicial_p,'dd') and trunc(dt_final_p,'dd') + 86399/86400
						and	e.cd_setor_Atendimento = cd_setor_Atendimento_p
						and	e.nr_atendimento = nr_Atendimento_p
						and	e.dt_suspensao is null
						and	to_char(a.dt_servico,'dd/mm/yyyy hh24:mi') = to_char(e.dt_servico,'dd/mm/yyyy hh24:mi')
						and	e.dt_bloqueio is not null
						and	(a.dt_desbloqueio is null or a.dt_desbloqueio >= sysdate))
		and	a.dt_bloqueio is not null
		and	(a.dt_desbloqueio is null or a.dt_desbloqueio >= sysdate);		
	end if;
end if;

return	ds_retorno_w;

end nut_obter_se_atend_bloq_Adep;
/