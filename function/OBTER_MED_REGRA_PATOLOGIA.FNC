create or replace
function Obter_med_regra_patologia(nr_prescricao_p	number,
				   nr_seq_proc_p	number,
				   nr_atendimento_p	number)
 		    	return varchar2 is

cd_medico_retorno_w	varchar2(10);
cd_setor_atendimento_w	number(5);
ie_tipo_regra_w		varchar2(15);
			
begin

select	nvl(cd_setor_atendimento,cd_setor_entrega)
into	cd_setor_atendimento_w
from	prescr_medica pm
where	pm.nr_prescricao = nr_prescricao_p;

select	ie_tipo_regra
into	ie_tipo_regra_w
from	REGRA_LAUDO_PAT_MEDICO
where	cd_setor_atendimento = cd_setor_atendimento_w;

if	(ie_tipo_regra_w = 'N') then
	 cd_medico_retorno_w := '';
elsif	(ie_tipo_regra_w = 'RA') then
	 select cd_medico_resp
	 into	cd_medico_retorno_w
	 from	atendimento_paciente
	 where	nr_atendimento = nr_atendimento_p;
elsif	(ie_tipo_regra_w = 'S') then
	 select cd_medico_executor
	 into	cd_medico_retorno_w
	 from	procedimento_paciente
	 where	nr_sequencia = nr_seq_proc_p;
elsif	(ie_tipo_regra_w = 'SO') then
	 select cd_medico
	 into	cd_medico_retorno_w
	 from	prescr_medica
	 where	nr_prescricao = nr_prescricao_p;
end if;

return	cd_medico_retorno_w;

end Obter_med_regra_patologia;
/
