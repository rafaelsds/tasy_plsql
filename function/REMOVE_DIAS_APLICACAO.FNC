create or replace
function remove_dias_aplicacao(ds_dias_p 	varchar2,
							   qt_dias_p 	number)
 		    	return varchar2 is

ds_dias_w		varchar2(4000) := '';
cont_w			number(10);
posicao_w		number(3);

begin

cont_w := 0;
ds_dias_w := ds_dias_p;
while ds_dias_w is not null loop
	begin
	if	(cont_w = qt_dias_p) then
		exit;
	end if;
	posicao_w	:= instr(ds_dias_w,',');
	ds_dias_w	:= substr(ds_dias_w, posicao_w + 1, length(ds_dias_w));
	cont_w		:= cont_w + 1;
	end;
end loop;

return ds_dias_w;

end remove_dias_aplicacao;
/