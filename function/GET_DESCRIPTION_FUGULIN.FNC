create or replace 
function get_description_fugulin(cd_chave_p	varchar2)
			return varchar2 is

vl_retorno_w	varchar2(255);

begin
	if	(cd_chave_p	is not null) then
		select ds_gradacao
		into vl_retorno_w
		from gca_gradacao
		where nr_sequencia = cd_chave_p;
	
	end if;

return vl_retorno_w;
end get_description_fugulin;
/