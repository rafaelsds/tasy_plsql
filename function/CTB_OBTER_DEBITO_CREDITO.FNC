create or replace
function ctb_obter_debito_credito(	cd_conta_contabil_p	varchar2,
				ie_saldo_p	number,
				vl_debito_p	number,
				vl_credito_p	number,
				vl_saldo_ant_p	number,
				vl_saldo_periodo_p	number)
 		    	return varchar2 is
			
ie_debito_credito_w	varchar2(1);
ie_debito_credito_ww	varchar2(1);

/*
1 - Coluna Saldo anterior
2 - Saldo per�odo
3 - Saldo atual
*/

begin

select	max(b.ie_debito_credito)
into	ie_debito_credito_ww
from	ctb_grupo_conta b,
	conta_contabil a
where	a.cd_grupo = b.cd_grupo
and	a.cd_conta_contabil = cd_conta_contabil_p;

ie_debito_credito_w	:= ie_debito_credito_ww;

if	(ie_debito_credito_ww = 'D') then
	begin
	
	if	(ie_saldo_p = 1) then
		begin
		if	(vl_saldo_ant_p < 0) then
			ie_debito_credito_w	:= 'C';
		end if;
		end;
	elsif	(ie_saldo_p = 2) then
		begin
		if	(vl_debito_p > vl_credito_p) then
			ie_debito_credito_w	:= 'D';
		else
			ie_debito_credito_w	:= 'C';
		end if;
		end;
	elsif	(ie_saldo_p = 3) then
		begin
		if	(vl_saldo_periodo_p < 0) then
			ie_debito_credito_w	:= 'C';
		end if;
		end;
	end if;

	end;
elsif	(ie_debito_credito_ww = 'C') then
	begin
	if	(ie_saldo_p = 1) then
		begin
		if	(vl_saldo_ant_p < 0) then
			ie_debito_credito_w	:= 'D';
		end if;
		end;
	elsif	(ie_saldo_p = 2) then
		begin
		if	(vl_debito_p > vl_credito_p) then
			ie_debito_credito_w	:= 'D';
		else
			ie_debito_credito_w	:= 'C';
		end if;
		end;
	elsif	(ie_saldo_p = 3) then
		begin
		if	(vl_saldo_periodo_p < 0) then
			ie_debito_credito_w	:= 'D';
		end if;
		end;
	end if;

	end;
end if;

return	substr(ie_debito_credito_w,1,1);

end ctb_obter_debito_credito;
/