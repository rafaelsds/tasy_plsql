create or replace
function obter_descricao_processo(cd_processo_p	number)
 		    	return varchar2 is

ds_processo_w	varchar2(50);			
begin
ds_processo_w := '';

if	(nvl(cd_processo_p,0) > 0) then

	select	max(nm_processo)
	into	ds_processo_w
	from	processo
	where	cd_processo = cd_processo_p;

end if;

return	ds_processo_w;

end obter_descricao_processo;
/