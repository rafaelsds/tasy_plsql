create or replace
function pls_obter_se_grupo_serv_sip( nr_seq_conta_p		number,
				      cd_procedimento_p		number,
				      ie_origem_proced_p	number, 
				      nr_seq_grupo_servico_p	number)
 		    	return varchar2 is
/*dados do grupo de servi�o*/
ie_item_servico_w		varchar2(1)	:= 'N';

/*dados do procedimento vindo como par�metro*/
ie_origem_proced_w		number(10);
cd_area_procedimento_w		number(15);
cd_especialidade_w		number(15);
cd_grupo_proc_w			number(15);
qt_proced_regra_w		number(10);
cd_area_w			number(15);
cd_grupo_w			number(15);


/*dados da conta*/
cd_guia_referencia_w		varchar2(20);
cd_proced_conta_w		number(15);
ie_origem_proced_conta_w	number(10);
cd_area_conta_w			number(15);
cd_especialidade_con_w		number(15);
cd_grupo_conta_w		number(15);
ie_origem_proced_conta_ww	number(10);
nr_seq_conta_w			number(10);
qt_existe_proc_w		number(10);

qt_proc_valido_w		number(10);

Cursor C01 is
	select	nr_sequencia 
	from 	pls_conta 
	where nvl(cd_guia_referencia,cd_guia) = cd_guia_referencia_w 
	order by 1;

		
cursor c02 is
	select	distinct
		(cd_procedimento),
		ie_origem_proced
	from 	pls_conta_proc	
	where	nr_seq_conta	= nr_seq_conta_w;
	
begin

qt_proc_valido_w	:= 0;

select	nvl(cd_guia_referencia,cd_guia)
into	cd_guia_referencia_w
from 	pls_conta
where 	nr_sequencia = nr_seq_conta_p;

pls_obter_estrut_proc(	cd_procedimento_p, ie_origem_proced_p,cd_area_w, 
			cd_especialidade_w, cd_grupo_w, ie_origem_proced_w);
			
select	count(*)
into	qt_proced_regra_w
from 	pls_preco_servico
where	nr_seq_grupo	 = nr_seq_grupo_servico_p;

if	(qt_proced_regra_w > 0)	then
	open C01;
	loop
	fetch C01 into	
		nr_seq_conta_w;
	exit when C01%notfound;
		begin
		open C02;
		loop
		fetch C02 into	
			cd_proced_conta_w,
			ie_origem_proced_conta_w;
		exit when C02%notfound;
		begin
		pls_obter_estrut_proc(	cd_proced_conta_w, ie_origem_proced_conta_w,cd_area_conta_w, 
					cd_especialidade_con_w, cd_grupo_conta_w, ie_origem_proced_conta_ww);
			
		/*Conta se os outro procedimento daquele atendimento existe na regra do grupo de servi�o*/
		
		select	count(1)
		into	qt_existe_proc_w
		from 	pls_preco_servico
		where	nr_seq_grupo	 	= nr_seq_grupo_servico_p
		and	(((cd_procedimento 	= cd_proced_conta_w) and (ie_origem_proced = ie_origem_proced_conta_w))
		or	cd_area_procedimento 	= cd_area_conta_w
		or	cd_especialidade  	= cd_especialidade_con_w
		or	cd_grupo_proc 	  	= cd_grupo_conta_w);
		
		if	(nvl(qt_existe_proc_w,0) > 0)	then
			qt_proc_valido_w	:= qt_proc_valido_w + 1;
		end if;
		
		end;
		end loop;
		close C02;	
					
		end;
	end loop;
	close C01;
end if;

if	(qt_proc_valido_w = qt_proced_regra_w)	then
	ie_item_servico_w := 'S';
end if;

return	ie_item_servico_w;	

end pls_obter_se_grupo_serv_sip;
/
