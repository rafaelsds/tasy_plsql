create or replace
function DCD_OBTER_SE_MAIOR_VALOR (nr_seq_proc_p number) return number is

nr_interno_conta_w 	number(10);
cd_medico_executor_w	number(10);
count_w			number(10);
nr_sequencia_w		number(10);

cursor c01 is

select  nr_sequencia
from    (   select  nr_sequencia
            from    procedimento_paciente 
            where   nr_interno_conta = nr_interno_conta_w
            and     cd_medico_executor = cd_medico_executor_w
            order by vl_procedimento desc)
where rownum <= 3;

begin

count_w := 0;

select nr_interno_conta, cd_medico_executor
into nr_interno_conta_w,
	cd_medico_executor_w
from procedimento_paciente 
where nr_sequencia = nr_seq_proc_p;

select count(*)
into count_w
from procedimento_paciente 
where nr_interno_conta = nr_interno_conta_w
and   cd_medico_executor = cd_medico_executor_w;


if (count_w > 3) then
	
	open c01;
	loop
		fetch c01 into nr_sequencia_w;
		exit when c01%notfound;
		begin
			if (nr_seq_proc_p = nr_sequencia_w) then
				return 0;
			end if;
		end;
	end loop;
	close c01;

end if;
return 1;
end;
/