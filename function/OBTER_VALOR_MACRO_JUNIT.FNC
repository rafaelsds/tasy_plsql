create or replace function obter_valor_macro_junit(texto_p varchar2)
 		    	return varchar2 is

ds_retorno_w			varchar2(4000);				
	
function get_tipo_operacao(spplited_texto_p varchar2)
return varchar2 as          
begin        
return nvl(REGEXP_SUBSTR (spplited_texto_p, '[+-/*//]',1,1),'N');
end;

begin
/*
case get_tipo_operacao(texto_p)
	when '+' then		
	when '-' then	
	when '*' then	
	when '/' then
	else
end case;
*/
	
if (INSTR(texto_p, '@') >  0) then

	case
		when (INSTR(texto_p, '@DATA_HORA') > 0) then
			case get_tipo_operacao(texto_p)
				when '+' then
					return to_char(SYSDATE + SUBSTR( texto_p ,INSTR( texto_p ,'+')+1,LENGTH(texto_p)),'DD/MM/YYYY HH24:MI:SS');
				when '-' then
					return to_char(SYSDATE - SUBSTR( texto_p ,INSTR( texto_p ,'-')+1,LENGTH(texto_p)),'DD/MM/YYYY HH24:MI:SS');
				else
					return to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SS');
			end case;
			
		when (INSTR(texto_p, '@DATA') > 0) then
			case get_tipo_operacao(texto_p)
				when '+' then
					return to_char(SYSDATE + SUBSTR( texto_p ,INSTR( texto_p ,'+')+1,LENGTH(texto_p)),'DD/MM/YYYY');
				when '-' then
					return to_char(SYSDATE - SUBSTR( texto_p ,INSTR( texto_p ,'-')+1,LENGTH(texto_p)),'DD/MM/YYYY');
				else
					return to_char(SYSDATE,'DD/MM/YYYY');
			end case;
		
		else
			return texto_p;
		end case;
		
end if;

return texto_p;

end obter_valor_macro_junit;
/