create or replace
function Obter_erro_produto_Gerencia
		(dt_parametro_p				date,
		nr_seq_gerencia_p			number,
		ie_tipo_valor_p			varchar2,
		nr_seq_localizacao_p	number)
 		return number is

qt_total_w		number(15,4);	
qt_total_tec_w		number(15,4);	
qt_erro_w		number(15,4);	
qt_retorno_w	number(15,4);	

dt_ref_mes_w	date;
dt_fim_mes_w	date;
	
begin

dt_ref_mes_w			:= pkg_date_utils.start_of(dt_parametro_p,'MONTH',0);
dt_fim_mes_w			:= pkg_date_utils.end_of(dt_ref_mes_w, 'MONTH', 0);

if	(ie_tipo_valor_p	= 'A') then
	dt_ref_mes_w			:= pkg_date_utils.add_month(pkg_date_utils.start_of(dt_parametro_p,'MONTH',0), -11,0);
	dt_fim_mes_w			:= pkg_date_utils.start_of(dt_parametro_p,'MONTH',0);
end if;


		select	count(*) qt_total_os, 
			sum(decode(COM_Obter_Tipo_Classif_OS(a.nr_sequencia),'W',1,0)) qt_erro_os
		into	qt_total_w,
				qt_erro_w
		from	grupo_desenvolvimento b,
				man_ordem_servico a
		where	a.dt_ordem_servico between dt_ref_mes_w and dt_fim_mes_w
		and		a.nr_seq_grupo_des	= b.nr_sequencia(+)
		and		((nr_seq_gerencia_p = 0) or (b.nr_seq_gerencia	= nr_seq_gerencia_p))
		and	((nvl(nr_seq_localizacao_p,0) = 0) or (a.nr_seq_localizacao = nr_seq_localizacao_p));
		
		/*
		Tecnologia
		Verifica as OS que 
			- Passou pela Tecnologia
			- Tem atividade realizada 
			- Tem hist�rico da tecnologia
			- Por�m foi encerrada em um grupo de desenvolvimento
		Esta situa��o � para tratar as OS onde: o Desenvolvimento necessita de altera��o em componente, � feita a altera��o e devolvido para finalizarem a customiza��o da fun��o.
		*/
		if	( nr_seq_gerencia_p = 2) then
			select	count(*) qt_total_os
			into	qt_total_tec_w
			from	grupo_desenvolvimento b,
				man_ordem_servico a
			where	a.dt_ordem_servico between dt_ref_mes_w and dt_fim_mes_w
			and	a.nr_seq_grupo_des	= b.nr_sequencia(+)
			and	b.nr_seq_gerencia	!= 2
			and	((nvl(nr_seq_localizacao_p,0) = 0) or (a.nr_seq_localizacao = nr_seq_localizacao_p))
			and	exists (
				select 	1 
				from	man_ordem_serv_ativ c,
					grupo_desenvolvimento d,
					usuario_grupo_des e
				where	a.nr_sequencia = c.nr_seq_ordem_serv
				and	d.nr_seq_gerencia = 2
				and	e.nr_seq_grupo = d.nr_sequencia
				and	c.nm_usuario_exec = e.nm_usuario_grupo)
			and 	exists (
				select 	1 
				from	man_ordem_serv_tecnico	c,
					grupo_desenvolvimento d,
					usuario_grupo_des e
				where	a.nr_sequencia = c.nr_seq_ordem_serv
				and	d.nr_seq_gerencia = 2
				and	e.nr_seq_grupo = d.nr_sequencia
				and	c.nm_usuario = e.nm_usuario_grupo)
			and	exists (
				select	1
				from	man_ordem_serv_estagio f,
					man_estagio_processo g
				where	f.nr_seq_ordem = a.nr_sequencia
				and	g.nr_sequencia = f.nr_seq_estagio
				and	g.ie_tecnologia = 'S');
				
			qt_total_w := qt_total_w + qt_total_tec_w;
				
		end if;
		
		qt_retorno_w	:= dividir(qt_erro_w, qt_total_w)*100;

if	(ie_tipo_valor_p	in ('A', 'T')) then
	return	qt_erro_w;
else	
	return	qt_retorno_w;
end if;

end Obter_erro_produto_Gerencia;
/
