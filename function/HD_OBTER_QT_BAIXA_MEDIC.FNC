create or replace
function hd_obter_qt_baixa_medic(cd_pessoa_fisica_p	varchar2,
				nr_seq_medic_p		number,
				dt_inicial_p		date,
				dt_final_p		date,
				ie_ignorar_datas_p	varchar2,
				cd_estabelecimento_p	number)
				return number is
			
qt_baixa_w	number(10);

begin

select	sum(qt_medicamento)
into	qt_baixa_w
from	hd_dialise_med_ext
where	ie_local = 'P'
and	cd_pessoa_fisica = cd_pessoa_fisica_p
and	nr_seq_medic = nr_seq_medic_p
and	((ie_ignorar_datas_p = 'S') 
	or ((dt_atualizacao_nrec between dt_inicial_p and fim_dia(dt_final_p)) 
		and (ie_ignorar_datas_p = 'N')))
and 	((cd_estabelecimento = cd_estabelecimento_p) or (cd_estabelecimento_p = '0'));

return	qt_baixa_w;

end hd_obter_qt_baixa_medic;
/