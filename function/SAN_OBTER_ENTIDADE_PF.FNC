create or replace
function san_obter_entidade_pf(	cd_pessoa_fisica_p	varchar2)
				return number is

nr_seq_entidade_w	number(10);

begin

	if (cd_pessoa_fisica_p is not null) then

		select	max(nr_seq_entidade)
		into	nr_seq_entidade_w
		from	san_reserva
		where	cd_pessoa_fisica	=	cd_pessoa_fisica_p;
		
		if (nr_seq_entidade_w is null) then
		
			select	max(nr_seq_entidade)
			into	nr_seq_entidade_w
			from	san_transfusao
			where	cd_pessoa_fisica	=	cd_pessoa_fisica_p;
		end if;
		
	end if;
	
return	nr_seq_entidade_w;

end san_obter_entidade_pf;
/