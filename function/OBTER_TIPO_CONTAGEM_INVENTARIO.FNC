create or replace
function obter_tipo_contagem_inventario (nr_seq_inventario_p	number)
 		    	return varchar2 is

qt_existe_w	number(10);
ds_retorno_w	varchar2(15) := 'I';

/*
Retorna o tipo de contagem do invent�rio, isto �, se a altera��o da coluna de contagem ser� pelo pr�prio invent�rio ou pelo item do mesmo.
Retorno:
I - Invent�rio
T - Item
*/

begin

select	count(*)
into	qt_existe_w
from	inventario
where	nr_sequencia = nr_seq_inventario_p;

if	(qt_existe_w > 0) then
	begin

	select	nvl(ie_tipo_contagem,'I')
	into	ds_retorno_w
	from	inventario
	where	nr_sequencia = nr_seq_inventario_p;

	end;
end if;

return	ds_retorno_w;

end obter_tipo_contagem_inventario;
/