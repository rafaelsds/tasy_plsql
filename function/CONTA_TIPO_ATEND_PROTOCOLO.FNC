create or replace
function Conta_tipo_atend_protocolo
		(nr_seq_protocolo_p		number,
		cd_convenio_p			number,
		ie_tipo_atendimento_p	number)
		return number is

qt_atend_w	number(10,0);

begin

if	(ie_tipo_atendimento_p is not null) then
	select	sum(decode(b.ie_tipo_atendimento, ie_tipo_atendimento_p, 1, 0))
	into	qt_atend_w
	from	atendimento_paciente b,
		conta_paciente a
	where	a.nr_atendimento	= b.nr_atendimento
	and	((cd_convenio_p is null) or (obter_convenio_atendimento(b.nr_atendimento)= cd_convenio_p))
	and	a.nr_seq_protocolo	= nr_seq_protocolo_p;
else
	select	count(*)
	into	qt_atend_w
	from	atendimento_paciente b,
		conta_paciente a
	where	a.nr_atendimento	= b.nr_atendimento
	and	((cd_convenio_p is null) or (obter_convenio_atendimento(b.nr_atendimento)= cd_convenio_p))
	and	a.nr_seq_protocolo	= nr_seq_protocolo_p;
end if;

return qt_atend_w;

end Conta_tipo_atend_protocolo;
/