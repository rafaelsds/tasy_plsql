create or replace
function Obter_Classif_Convenio
	(cd_convenio_p		number)
	return varchar2 is

ds_classificacao_w	varchar2(100);

begin

select	max(a.ds_classificacao)
into	ds_classificacao_w
from	convenio_classif b,
	classificacao_convenio a
where	a.nr_sequencia	= b.nr_seq_classificacao
and	b.cd_convenio	= cd_convenio_p;

return	ds_classificacao_w;

end Obter_Classif_Convenio;
/