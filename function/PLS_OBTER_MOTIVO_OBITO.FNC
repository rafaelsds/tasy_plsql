create or replace
function pls_obter_motivo_obito
		(	nr_seq_motivo_cancel_p	number)
 		    	return varchar2 is
			
ie_retorno_w		varchar2(1);

begin

begin
select	nvl(ie_obito,'N')
into	ie_retorno_w
from	pls_motivo_cancelamento
where	nr_sequencia	= nr_seq_motivo_cancel_p;
exception
when others then
	ie_retorno_w := 'N';
end;

return	ie_retorno_w;

end pls_obter_motivo_obito;
/
