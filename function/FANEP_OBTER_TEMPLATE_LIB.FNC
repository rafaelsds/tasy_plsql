create or replace
function fanep_obter_template_lib(	nr_seq_item_fanep_p	number )
					return number is

nr_sequencia_w		number(10,0);
nr_seq_template_w	number(10,0);
					
begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	perfil_item_fanep
where	nr_seq_item	=	nr_seq_item_fanep_p
and	cd_perfil	=	obter_perfil_ativo;

if	(nr_sequencia_w > 0) then
	select	max(nr_seq_template)
	into	nr_seq_template_w
	from	perfil_item_fanep_template
	where	nr_seq_item_perfil	=	nr_sequencia_w;
end if;

return	nr_seq_template_w;

end fanep_obter_template_lib;
/