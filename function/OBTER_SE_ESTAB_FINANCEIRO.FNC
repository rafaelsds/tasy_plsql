create or replace
function obter_se_estab_financeiro(cd_estab_corrente_p	number,
				   cd_estab_conta_p	number)
 		    	return varchar2 is

ie_estab_financeiro_w	varchar2(1):= 'N';
cd_estabelecimento_w	number(4);

Cursor C01 is
	select	cd_estabelecimento
	from	estabelecimento
	where	nvl(cd_estab_financeiro,cd_estabelecimento) = cd_estab_corrente_p;
			
begin

ie_estab_financeiro_w:= 'N';

open C01;
loop
fetch C01 into	
	cd_estabelecimento_w;
exit when C01%notfound;
	begin
	
	if	(cd_estab_conta_p = cd_estabelecimento_w) then
		ie_estab_financeiro_w:= 'S';
	end if;
	
	end;
end loop;
close C01;

return	ie_estab_financeiro_w;

end obter_se_estab_financeiro;
/