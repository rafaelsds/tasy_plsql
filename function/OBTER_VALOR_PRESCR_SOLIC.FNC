create or replace
function obter_valor_prescr_solic (nr_prescricao_p		number)
 		    	return number is

			
ie_tipo_w 	number(10);
ds_retorno_w	number(10);	
		
begin

if	(nr_prescricao_p is not null) then
	SELECT 	max(ie_tipo)
	INTO	ie_tipo_w
	FROM 	prescr_solic_bco_sangue
	WHERE 	nr_prescricao = nr_prescricao_p;

	SELECT 	vl_dominio
	into	ds_retorno_w 
	FROM 	valor_dominio
	WHERE 	cd_dominio = 1223
	AND	vl_dominio = ie_tipo_w;
end if;

return	ds_retorno_w;

end obter_valor_prescr_solic;
/
