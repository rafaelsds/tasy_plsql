create or replace function cpoe_obter_volume_corrigido
         (ie_bomba_infusao_p   varchar2,
          ie_fator_correcao_p  varchar2,
          qt_volume_p          number,
          cd_material_p        number,
          qt_dose_p            number,
          cd_unidade_medida_p  varchar2)
          return number is

  qt_dose_correcao_w  number(18,6)	:= 0;
  qt_equipo_w         number(15,4)	:= Obter_vol_Dispositivo_solucao(ie_bomba_infusao_p);
  qt_total_w          number(18,6);
  qt_dose_ml_w        number(15,4);	
  qt_conversao_w      number(18,6);
  qt_conversao_ml_w   number(18,6);

  unid_med_usua_w     varchar2(30);
  cd_unid_med_cons_w  varchar2(30);		
  EXEC_W              varchar2(200); 
begin
  if	(nvl(ie_fator_correcao_p, 'N') = 'S') and (nvl(ie_bomba_infusao_p, 'N') <> 'N') and (nvl(qt_equipo_w, 0) > 0) then
    unid_med_usua_w := obter_unid_med_usua('ML');

    cd_unid_med_cons_w := upper(substr(obter_dados_material_estab(cd_material_p, wheb_usuario_pck.get_cd_estabelecimento, 'UMS'),1,30));

    select nvl(max(qt_conversao),0)
    into	 qt_conversao_w
    from   material_conversao_unidade
    where	cd_material = cd_material_p
    and	upper(cd_unidade_medida) = cd_unidade_medida_p;


    select nvl(max(qt_conversao),0)
    into   qt_conversao_ml_w
    from   material_conversao_unidade
    where cd_material = cd_material_p
    and	upper(cd_unidade_medida) = upper (obter_unid_med_usua('ML'));

    begin
      EXEC_W := 'CALL OBTER_CONVERSAO_ML_MD(:1,:2,:3,:4,:5,:6) INTO :result';
      
      EXECUTE IMMEDIATE EXEC_W USING IN qt_dose_p,
                                     IN cd_unidade_medida_p,
                                     IN unid_med_usua_w,
                                     IN cd_unid_med_cons_w,
                                     IN qt_conversao_w,
                                     IN qt_conversao_ml_w,
                                     OUT qt_dose_ml_w;
    exception
      when others then
        qt_dose_ml_w := null;
    end;
      
    begin
      EXEC_W := 'CALL OBTER_TOT_E_VOL_VOL_COR_MD(:1,:2) INTO :result';
      
      EXECUTE IMMEDIATE EXEC_W USING IN qt_equipo_w,
                                     IN qt_volume_p,
                                     OUT qt_total_w;
    exception
      when others then
        qt_total_w := null;
    end;

    begin
      EXEC_W := 'CALL OBTER_DOSE_COR_V_CORRIG_MD(:1,:2,:3) INTO :result';
      
      EXECUTE IMMEDIATE EXEC_W USING IN qt_total_w,
                                     IN qt_volume_p,
                                     IN qt_dose_ml_w,
                                     OUT qt_dose_correcao_w;
    exception
      when others then
        qt_dose_correcao_w := null;
    end;
  end if;

  return	qt_dose_correcao_w;

end cpoe_obter_volume_corrigido;
/