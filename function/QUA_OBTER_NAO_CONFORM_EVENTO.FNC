create or replace
function qua_obter_nao_conform_evento(nr_seq_evento_p	number)
 		    	return number is


nr_seq_nao_conformidade_w	number(10,0);

begin

if	(nr_seq_evento_p > 0) then
	select	max(nr_sequencia)
	into	nr_seq_nao_conformidade_w
	from	qua_nao_conformidade
	where	nr_seq_evento = nr_seq_evento_p;
end if;

return	nr_seq_nao_conformidade_w;

end qua_obter_nao_conform_evento;
/