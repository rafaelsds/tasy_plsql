create or replace
function obter_tipo_prof_regulacao	(ie_tipo_profissional_p	varchar)
					return varchar is

ds_tipo_profissional_w		varchar2(255);

begin
if	( ie_tipo_profissional_p is not null) then
	
	if (ie_tipo_profissional_p = 'R') then
	
		ds_tipo_profissional_w := obter_desc_expressao(886890);
	
	else
	
		ds_tipo_profissional_w := obter_desc_expressao(298656);
	
	end if;
	
end if;

return ds_tipo_profissional_w;

end obter_tipo_prof_regulacao;
/