create or replace 
FUNCTION Obter_Ramal_Unidade(
				cd_unidade_basica_p	Varchar2,
				cd_unidade_compl_p	Varchar2)
				return Number is


nr_ramal_w			Number(08);

BEGIN

select max(nr_ramal)
into	nr_ramal_w
from	Unidade_atendimento
where	cd_unidade_basica	= cd_unidade_basica_p
and	cd_unidade_compl	= cd_unidade_compl_p;

return nr_ramal_w;

END Obter_Ramal_Unidade;
/





