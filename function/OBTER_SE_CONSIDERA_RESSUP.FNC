create or replace
function obter_se_considera_ressup(
		cd_estabelecimento_p	number,
		cd_local_estoque_p	number,
		cd_material_p		number)
	return	varchar2 as


cd_grupo_material_w		number(3);
cd_subgrupo_w			number(3);
cd_classe_material_w		number(5);
ie_controlado_w			varchar2(1);
nr_regras_w			number(5);
ie_considera_w			varchar2(01);
ie_consignado_w			material.ie_consignado%type;


cursor c01 is
	select	ie_considera
	from	regra_estoque_ressup
	where	cd_estabelecimento = cd_estabelecimento_p	
	and	nvl(cd_local_estoque, cd_local_estoque_p)		= cd_local_estoque_p
	and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
	and	nvl(cd_subgrupo_material, cd_subgrupo_w)		= cd_subgrupo_w
	and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
	and	nvl(cd_material, cd_material_p)			= cd_material_p
	and	((nvl(ie_controlado,'A') = 'A') or (nvl(ie_controlado, 'A') 	= ie_controlado_w))
	and	((ie_consignado is null) or (ie_consignado = ie_consignado_w))
	order by
		nvl(cd_material, 0),
		nvl(cd_classe_material, 0),
		nvl(cd_subgrupo_material, 0),
		nvl(cd_grupo_material, 0),
		nvl(cd_local_estoque, 0);
begin
ie_considera_w	:= 'S';

select	count(*)
into	nr_regras_w
from	regra_estoque_ressup
where	rownum = 1;

if	(nr_regras_w > 0) then
	begin
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		ie_consignado
	into	cd_grupo_material_w,
		cd_subgrupo_w,
		cd_classe_material_w,		
		ie_consignado_w
	from	estrutura_material_v
	where 	cd_material	= cd_material_p
	and	rownum = 1;

	select	substr(obter_se_medic_controlado(cd_material_p),1,1)
	into	ie_controlado_w
	from	dual;
	
	open c01;
	loop
	fetch c01 into
		ie_considera_w;
	exit when c01%notfound;
	end loop;
	close c01;
	end;
end if;

return ie_considera_w;

end obter_se_considera_ressup;
/
