create or replace
function obter_se_responsavel_tarefa(	nm_usuario_p		varchar2,
					nr_seq_worklist_p	number)
 		    	return varchar2 is

ds_retorno_w			varchar2(1) := 'S';
nr_atendimento_w		wl_worklist.nr_atendimento%type;
cd_categoria_w			wl_item.cd_categoria%type;
nr_seq_carta_mae_w		wl_worklist.nr_seq_carta_mae%type;
nr_parecer_w			wl_worklist.nr_parecer%type;
cd_pessoa_fisica_usu_w	usuario.nm_usuario%type;
cd_setor_usuario_w		usuario.cd_setor_atendimento%type;
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
nr_prescricao_w			wl_worklist.nr_prescricao%type;
ie_tipo_pend_carta_w	wl_regra_item.ie_tipo_pend_carta%type;
cd_profissional_w		wl_worklist.cd_profissional%type;

begin

if	(nm_usuario_p is not null) and (nr_seq_worklist_p is not null) then

	select	max(cd_pessoa_fisica),
		max(cd_setor_atendimento)
	into	cd_pessoa_fisica_usu_w,
		cd_setor_usuario_w
	from	usuario
	where	nm_usuario = nm_usuario_p;

	select	max(a.nr_atendimento),
		max(Obter_Setor_Atendimento(a.nr_atendimento)),
		max(b.cd_categoria),
		max(a.nr_seq_carta_mae),
		max(a.nr_parecer),
		max(a.nr_prescricao),
		max((select	max(x.ie_tipo_pend_carta)
			from	wl_regra_item x
			where	x.nr_sequencia = a.nr_seq_regra)),
		max(obter_pf_usuario(nm_usuario_p,'C'))
	into	nr_atendimento_w,
		cd_setor_atendimento_w,
		cd_categoria_w,
		nr_seq_carta_mae_w,
		nr_parecer_w,
		nr_prescricao_w,
		ie_tipo_pend_carta_w,
		cd_profissional_w
	from	wl_worklist a,
		wl_item b
	where	a.nr_seq_item = b.nr_sequencia
	and	a.nr_sequencia = nr_seq_worklist_p;

	if	(cd_categoria_w in ('AD','CN','TL','EN','EX','S','WO','MR')) then

		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	departamento_setor a
		where	a.cd_setor_atendimento = cd_setor_usuario_w
		and	exists(select	1
				from	departamento_setor b
				where	b.cd_setor_atendimento = cd_setor_atendimento_w
				and	a.cd_departamento = b.cd_departamento);
	
	elsif	(cd_categoria_w = 'DL') then
	
		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	parecer_medico_req
		where	nr_parecer = nr_parecer_w
		and	(obter_se_medico_equipe(nr_seq_equipe_dest,cd_pessoa_fisica_usu_w) = 'S'
		or	obter_se_especialidade_medico(cd_pessoa_fisica_usu_w,cd_especialidade_dest) = 'S'
		or	cd_pessoa_parecer = cd_pessoa_fisica_usu_w);
		
	elsif	(cd_categoria_w = 'ML') then
	
		if (ie_tipo_pend_carta_w = 'A' or ie_tipo_pend_carta_w = 'E') then
			select	decode(count(*),0,'N','S')
			into	ds_retorno_w
			from	wl_worklist
			where	nr_seq_carta_mae = nr_seq_carta_mae_w
			and		(cd_profissional = cd_profissional_w
			or		cd_profissional is null)
			and		nr_sequencia = nr_seq_worklist_p;
			
		end if;
		
	elsif	(cd_categoria_w = 'RA') then
	
		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	prescr_medica
		where	nr_prescricao = nr_prescricao_w
		and		cd_medico = cd_pessoa_fisica_usu_w;
		
	elsif (cd_categoria_w = 'ED') then
	
		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_w
		and		cd_medico_resp = cd_pessoa_fisica_usu_w;
		
	end if;

end if;

return	ds_retorno_w;

end obter_se_responsavel_tarefa;
/
