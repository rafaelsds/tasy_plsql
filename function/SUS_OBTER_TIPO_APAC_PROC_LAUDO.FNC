create or replace
function Sus_Obter_Tipo_Apac_Proc_Laudo(cd_procedimento_p	sus_procedimento.cd_procedimento%type,
					ie_origem_proced_p	sus_procedimento.ie_origem_proced%type)
					return Varchar2 as

ie_tipo_laudo_apac_w		sus_procedimento.ie_tipo_laudo_apac%type;
ds_retorno_w			number(10) := 0;
	
begin

select	nvl(max(ie_tipo_laudo_apac),'0')
into	ie_tipo_laudo_apac_w
from	sus_procedimento
where	ie_origem_proced = ie_origem_proced_p
and	cd_procedimento = cd_procedimento_p;
	
if	(nvl(ie_tipo_laudo_apac_w,'0') > '0') then
	begin
	if	(ie_tipo_laudo_apac_w = '01') then
		ds_retorno_w := 12;
	elsif	(ie_tipo_laudo_apac_w = '02') then
		ds_retorno_w := 8;
	elsif	(ie_tipo_laudo_apac_w = '03') then
		ds_retorno_w := 1;
	elsif	(ie_tipo_laudo_apac_w = '04') then
		ds_retorno_w := 2;
	elsif	(ie_tipo_laudo_apac_w = '06') then
		ds_retorno_w := 12;
	elsif	(ie_tipo_laudo_apac_w = '07') then
		ds_retorno_w := 22;
	elsif	(ie_tipo_laudo_apac_w = '10') then
		ds_retorno_w := 8;
	elsif	(ie_tipo_laudo_apac_w = '12') then
		ds_retorno_w := 19;
	end if;		 
	end;
end if;

return	ds_retorno_w;

end Sus_Obter_Tipo_Apac_Proc_Laudo;
/