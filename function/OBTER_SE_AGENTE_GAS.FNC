create or replace
function obter_se_agente_gas(	nr_seq_agente_p	number )
				return varchar2 is

ds_retorno_w	varchar2(5);

begin

/* NI - N�o Informado */

select	nvl(ie_forma_farmaceutica,'NI')
into	ds_retorno_w
from	agente_anestesico
where	nr_sequencia 	= 	nr_seq_agente_p;

return	ds_retorno_w;

end obter_se_agente_gas;
/