create or replace
function hsp_obter_hor_imp_prescr(	nr_prescricao_p	Number,
					nr_sequencia_p	Number,
					ie_proc_mat_p	varchar2)
		return varchar2 is

ds_retorno_w		varchar2(2000)	:= null;
ds_horario_w		Varchar2(2000);
ds_hora_w			Varchar2(010);
ds_hor_res_w		Varchar2(255);
ds_pos_res_w		Varchar2(255);
ds_cor_res_w		Varchar2(255);
ds_estilo_res_w		Varchar2(255);
type campos is record (dt_hora		date);
type Vetor is table of campos index by binary_integer;
horarios_w			Vetor;
horario_w			Vetor;
i					Integer	:= 0;
k					Integer := 0;
z					Integer;
dt_hora_w			Date;
dt_inic_w			Date;
dt_fim_w			Date;
ie_separador_w		Varchar2(1)	:= '';
ds_noturno_w		Varchar2(5)	:= '19/06';
ie_cor_fonte_w		Varchar2(2)	:= 'V'; 
ie_Estilo_w			Varchar2(1)	:= '';
ie_Cor_w			Varchar2(5)	:= '';
qt_reg_w			Number(05,0);
nr_seq_w			Number(06,0);
ie_horarios_w		Varchar2(10);
ie_se_necessario_w	Varchar2(1);
ie_estilo_padrao_w	Varchar2(1);
ie_operacao_w		varchar2(1);
nr_atendimento_w	Number(10);
cd_material_w		Number(06);
ie_susp_w			Varchar2(1);
ie_susp_proc_w			Varchar2(1);
ie_adm_proc_w			Varchar2(1);

Cursor C01 is
	select	padroniza_horario_prescr(decode(nvl(c.ie_operacao,''),'F',Reordenar_Horarios(nvl(to_date('01/01/2000 '||decode(b.hr_prim_horario,'  :  ',to_char(a.dt_primeiro_horario,'hh24:mi'), b.hr_prim_horario)||':00','dd/mm/yyyy hh24:mi:ss'), a.dt_primeiro_horario),b.ds_horarios),b.ds_horarios), decode(nvl(b.qt_dia_prim_hor,0),0,decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,decode(b.hr_prim_horario,'  :  ',to_char(a.dt_primeiro_horario,'hh24:mi'), b.hr_prim_horario)),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss')),'01/01/2000 23:59:59')),
			a.nr_atendimento
	from	intervalo_prescricao c,
			prescr_material b,
			prescr_medica a
	where	b.nr_prescricao	= nr_prescricao_p
	and		b.cd_intervalo	= c.cd_intervalo(+)
	and		a.nr_prescricao = b.nr_prescricao
	and		ie_proc_mat_p	= 'M'
	and		b.ie_agrupador	= 1	
	and		padroniza_horario_prescr(decode(nvl(c.ie_operacao,''),'F',Reordenar_Horarios(nvl(to_date('01/01/2000'||decode(b.hr_prim_horario,'  :  ',to_char(a.dt_primeiro_horario,'hh24:mi'), b.hr_prim_horario)||':00','dd/mm/yyyy hh24:mi:ss'), a.dt_primeiro_horario),b.ds_horarios),b.ds_horarios), decode(nvl(b.qt_dia_prim_hor,0),0,decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,decode(b.hr_prim_horario,'  :  ',to_char(a.dt_primeiro_horario,'hh24:mi'), b.hr_prim_horario)),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss')),'01/01/2000 23:59:59'))  is not null
	Union
	select	padroniza_horario_prescr(decode(nvl(c.ie_operacao,''),'F',Reordenar_Horarios(nvl(a.dt_primeiro_horario,a.dt_prescricao),b.ds_horarios),b.ds_horarios), decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,to_char(b.dt_prev_execucao,'hh24:mi')),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss'))),
			a.nr_atendimento
	from	intervalo_prescricao c,
			prescr_Procedimento b,
			prescr_medica a
	where	a.nr_prescricao	= nr_prescricao_p
	and		b.cd_intervalo	= c.cd_intervalo(+)
	and		a.nr_prescricao = b.nr_prescricao
	and		b.nr_sequencia	= nr_sequencia_p
	and		b.nr_seq_origem is null
	and		padroniza_horario_prescr(decode(nvl(c.ie_operacao,''),'F',Reordenar_Horarios(nvl(a.dt_primeiro_horario,a.dt_prescricao),b.ds_horarios),b.ds_horarios), decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,to_char(b.dt_prev_execucao,'hh24:mi')),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss'))) is not null
	and		ie_proc_mat_p	= 'P'
	union
	select	padroniza_horario_prescr(b.ds_horarios,null),
			a.nr_atendimento
	from		prescr_medica a,
			prescr_dieta b
	where	a.nr_prescricao	= nr_prescricao_p
	and		a.nr_prescricao	= b.nr_prescricao
	and		b.nr_sequencia	= nr_sequencia_p
	and		padroniza_horario_prescr(b.ds_horarios,null) is not null
	and		ie_proc_mat_p	= 'D';

BEGIN

select	max(a.ie_operacao),
	max(b.cd_material)
into	ie_operacao_w,
		cd_material_w
from	intervalo_prescricao a,
		prescr_material b
where	b.cd_intervalo	= a.cd_intervalo(+)
and		b.nr_prescricao	= nr_prescricao_p
and		b.nr_sequencia	= nr_sequencia_p;

begin
select	nvl(vl_parametro, vl_parametro_padrao)
into	ds_noturno_w
from	funcao_parametro
where	cd_funcao	= 924
and		nr_sequencia	= 71;
dt_inic_w	:= To_date('01/01/2000 ' || substr(ds_noturno_w,1,2), 'dd/mm/yyyy hh24');
dt_fim_w	:= To_date('02/01/2000 ' || substr(ds_noturno_w,4,2), 'dd/mm/yyyy hh24');
exception
	when others then
	Wheb_mensagem_pck.exibir_mensagem_abort(182566);
end;

select	nvl(vl_parametro, vl_parametro_padrao)
into	ie_horarios_w
from	funcao_parametro
where	cd_funcao = 924
and	nr_sequencia = 70;

select	nvl(nvl(vl_parametro, vl_parametro_padrao),'V')
into	ie_cor_fonte_w
from	funcao_parametro
where	cd_funcao = 924
and		nr_sequencia = 86;

select	nvl(nvl(vl_parametro, vl_parametro_padrao),'N')
into	ie_estilo_padrao_w
from	funcao_parametro
where	cd_funcao = 924
and		nr_sequencia = 132;

select	count(*),
		min(nr_sequencia)
into	qt_reg_w,
		nr_seq_w
from	prescr_material
where	nr_prescricao		= nr_prescricao_p
and		ie_agrupador		= 1
and		nr_agrupamento	= 
		(select	nr_agrupamento
		from	prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_sequencia_p);

if	(qt_reg_w > 1) and
	(nr_sequencia_p <> nr_seq_w) then
	ds_retorno_w		:= '';
	ds_horario_w		:= '';
elsif	(ie_proc_mat_p = 'M') then
	select	decode(ie_operacao_w,'F',Reordenar_Horarios(nvl(to_date('01/01/2000'||decode(b.hr_prim_horario,'  :  ',to_char(a.dt_primeiro_horario,'hh24:mi'), b.hr_prim_horario)||':00','dd/mm/yyyy hh24:mi:ss'),a.dt_primeiro_horario),b.ds_horarios),b.ds_horarios),
			padroniza_horario_prescr(decode(ie_operacao_w,'F',Reordenar_Horarios(nvl(to_date('01/01/2000'||decode(b.hr_prim_horario,'  :  ',to_char(a.dt_primeiro_horario,'hh24:mi'), b.hr_prim_horario)||':00','dd/mm/yyyy hh24:mi:ss'),a.dt_primeiro_horario),b.ds_horarios),b.ds_horarios), decode(nvl(b.qt_dia_prim_hor,0),0,decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,decode(b.hr_prim_horario,'  :  ',to_char(a.dt_primeiro_horario,'hh24:mi'), b.hr_prim_horario)),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss')),'01/01/2000 23:59:59')),
			b.ie_se_necessario
	into	ds_retorno_w,
			ds_horario_w,
			ie_se_necessario_w
	from	Prescr_material b,
			prescr_medica a
	where	b.nr_prescricao	= nr_prescricao_p
	and		b.nr_sequencia	= nr_sequencia_p
	and		b.ie_agrupador	= 1
	and		b.nr_prescricao = a.nr_prescricao;
elsif	(ie_proc_mat_p = 'P') then
	select	distinct
		b.ds_horarios,
		padroniza_horario_prescr(b.ds_horarios,to_char(b.dt_prev_execucao,'dd/mm/yyyy hh24:mi:ss')),
		b.ie_se_necessario
	into	ds_retorno_w,
		ds_horario_w,
		ie_se_necessario_w
	from	prescr_procedimento b
	where	b.nr_prescricao	= nr_prescricao_p
	and	b.nr_sequencia	= nr_sequencia_p;
elsif	(ie_proc_mat_p = 'D') then
	select	distinct a.ds_horarios,
			padroniza_horario_prescr(a.ds_horarios,null),
			'N' ie_se_necessario
	into	ds_retorno_w,
		ds_horario_w,
		ie_se_necessario_w
	from	prescr_dieta a
	where	a.nr_prescricao	= nr_prescricao_p
	and	a.nr_sequencia	= nr_sequencia_p;

end if;

if	(ds_horario_w is null) or
	(ie_se_necessario_w = 'S') then
	ds_retorno_w		:= '1#1#'  || ds_retorno_w || '#1#;#;';
else
	begin
	i	:= 0;
	while	ds_horario_w is not null LOOP
		begin
		select instr(ds_horario_w, ' ') into k from dual;
		if	(k > 1) then
			ds_hora_w	:= substr(ds_horario_w, 1, k-1);
			ds_horario_w	:= substr(ds_horario_w, k + 1, 2000);
		elsif	(ds_horario_w is not null) then
			ds_hora_w 	:= replace(ds_horario_w, ' ','');
			ds_horario_w	:= '';
		end if;
		i	:= i + 1;
		if	(substr(upper(ds_hora_w),1,1) = 'A') then
			horario_w(i).dt_hora	:= To_date('01/01/2000 ' || replace(ds_hora_w,'A',''), 'dd/mm/yyyy hh24:mi') + 1;
		else
			horario_w(i).dt_hora	:= To_date('01/01/2000 ' || ds_hora_w, 'dd/mm/yyyy hh24:mi');
		end if; 
		end;
	END LOOP;
/* Selecionar os hor�rios de toda prescri��o */
	i	:= 0;
	OPEN C01;
	LOOP
	FETCH C01 into 
		ds_horario_w,
		nr_atendimento_w;
	EXIT when C01%notfound;
		while	ds_horario_w is not null LOOP
			begin
			select	instr(ds_horario_w, ' ')
			into	k
			from	dual;



			if	(k > 1) then
				ds_hora_w	:= substr(ds_horario_w, 1, k-1);
				ds_horario_w	:= substr(ds_horario_w, k + 1, 2000);
			elsif	(ds_horario_w is not null) then
				if 	(instr(ds_horario_w, 'A') > 0) then
					ds_hora_w 	:= replace(substr(ds_horario_w, 1, 6), ' ','');
				else
					ds_hora_w 	:= replace(substr(ds_horario_w, 1, 5), ' ','');
				end if;

				ds_horario_w	:= '';
			end if;

			select	length(substr(ds_hora_w, instr(ds_hora_w, ':')+1, length(ds_hora_w)))
			into	k
			from	dual;
			if (k = 1) then
				ds_hora_w := ds_hora_w ||'0';
			end if;
			/*  Victor 21/10/2009  OS 173774  fim*/
			z			:= 0;
			k			:= 1;
			if	(substr(upper(ds_hora_w),1,1) = 'A') then
				dt_hora_w	:= To_date('02/01/2000 ' || replace(ds_hora_w,'A',''), 'dd/mm/yyyy hh24:mi');
			else
				dt_hora_w	:= To_date('01/01/2000 ' || ds_hora_w, 'dd/mm/yyyy hh24:mi');
			end if;

			FOR k in 1..Horarios_w.count LOOP
				if	(horarios_w(k).dt_hora = dt_hora_w) then
					z	:= k;
				end if;
			END LOOP;
			if	(z = 0) then
				i	:= i + 1;
				horarios_w(i).dt_hora	:= dt_hora_w;
			end if;
			end;
		END LOOP;
	END LOOP;
	CLOSE C01;	
/* Classificar os hor�rios */
	k		:= 1;
	z		:= 0;
	while	z = 0 LOOP
		z	:= 1;
		FOR k in 1..Horarios_w.count LOOP
			if	(k < Horarios_w.count) and
				(horarios_w(k).dt_hora > horarios_w(k+ 1).dt_hora) then
				dt_hora_w		:= horarios_w(k).dt_hora;
				horarios_w(k).dt_hora	:= horarios_w(K + 1).dt_hora;
				horarios_w(k+1).dt_hora	:= dt_hora_w;
				z			:= 0;
			end if;
		END LOOP;
	END LOOP;

/* Retorno dos hor�rios na sequencia */
	k	:= 1;
	FOR k in 1..Horario_w.count LOOP
		begin
		i	:= 1;
		for i in 1..horarios_w.count LOOP
			if	(to_char(horario_w(k).dt_hora, 'dd/mm/yyyy hh24:mi') = to_char(horarios_w(i).dt_hora, 'dd/mm/yyyy hh24:mi')) then
				z	:= i;
			end if;

		END LOOP;
		ds_hora_w		:= to_char(horario_w(k).dt_hora,'hh24:mi');

		ie_estilo_w := '';

		if	(obter_se_horario_adm(nr_atendimento_w, nr_prescricao_p, cd_material_w, ds_hora_w) = 'S') then
			ie_estilo_w	:= 'K';
		end if;

		if	(ie_proc_mat_p = 'M') then

			select	nvl(max('S'),'N')
			into	ie_susp_w
			from	prescr_mat_hor
			where	nr_prescricao	= nr_prescricao_p
			and	dt_suspensao	is not null
			and	to_char(dt_horario, 'hh24:mi') = ds_hora_w
			and	nvl(ie_situacao,'A') = 'A'
			and	nr_seq_material = nr_sequencia_p
			and	cd_material	= cd_material_w;

			if	(ie_susp_w = 'S') then
				ie_estilo_w	:= 'S';
			end if;

		elsif	(ie_proc_mat_p = 'P') then

			select	nvl(max('S'),'N')
			into	ie_adm_proc_w
			from	prescr_proc_hor
			where	nr_prescricao	= nr_prescricao_p
			and	dt_fim_horario is not null
			and	nvl(ie_situacao,'S') = 'S'
			and	nr_seq_procedimento = nr_sequencia_p
			and	to_char(dt_horario, 'hh24:mi') = ds_hora_w;		

			if	(ie_adm_proc_w = 'S') then
				ie_estilo_w	:= 'K';
			else
				select	nvl(max('S'),'N')
				into	ie_susp_proc_w
				from	prescr_proc_hor
				where	nr_prescricao	= nr_prescricao_p
				and	dt_suspensao is not null
				and	nvl(ie_situacao,'S') = 'S'
				and	nr_seq_procedimento = nr_sequencia_p
				and	to_char(dt_horario, 'hh24:mi') = ds_hora_w;

				if	(ie_susp_proc_w = 'S') then
					ie_estilo_w	:= 'S';
				end if;
			end if;

		elsif	(ie_proc_mat_p = 'D') then

			select	nvl(max('S'),'N')
			into	ie_adm_proc_w
			from	prescr_dieta_hor
			where	nr_prescricao	= nr_prescricao_p
			and	dt_fim_horario is not null
			and	nvl(ie_situacao,'S') = 'S'
			and	nr_seq_dieta = nr_sequencia_p
			and	to_char(dt_horario, 'hh24:mi') = ds_hora_w;		

			if	(ie_adm_proc_w = 'S') then
				ie_estilo_w	:= 'K';
			else
				select	nvl(max('S'),'N')
				into	ie_susp_proc_w
				from	prescr_dieta_hor
				where	nr_prescricao	= nr_prescricao_p
				and	dt_suspensao is not null
				and	nvl(ie_situacao,'S') = 'S'
				and	nr_seq_dieta = nr_sequencia_p
				and	to_char(dt_horario, 'hh24:mi') = ds_hora_w;

				if	(ie_susp_proc_w = 'S') then
					ie_estilo_w	:= 'S';
				end if;
			end if;

		end if;
		
		if	(substr(ds_hora_w,1,2) = '00') then
			ds_hora_w	:= '24:' || substr(ds_hora_w,4,2);
		end if;
		if	(substr(ds_hora_w,4,2) = '00') then
			ds_hora_w	:= substr(ds_hora_w,1,2);
		end if;

		if	(horario_w(k).dt_hora >= dt_inic_w) and
			(horario_w(k).dt_hora <= dt_fim_w) and 
			(nvl(ie_estilo_w,'0')	<> 'O') then
			ie_Cor_w	:= ie_cor_fonte_w;
			if	((ie_estilo_padrao_w <> 'T')   and
			(nvl(ie_estilo_w,'0') = '0')) then
				ie_estilo_w	:= ie_estilo_padrao_w;
			elsif	(ie_estilo_w not in ('K','S')) then
				ie_estilo_w	:= '';
			end if;
		elsif	(ie_estilo_w not in ('K','S')) then
			ie_Cor_w	:= '';
			ie_estilo_w	:= '';
		end if;
		
		ds_hor_res_w		:= substr(ds_hor_res_w || ie_separador_w || ds_hora_w,1,255);
		ds_pos_res_w		:= substr(ds_pos_res_w || ie_separador_w || z,1,255);
		ds_cor_res_w		:= substr(ds_cor_res_w  || ie_separador_w || ie_cor_w,1,255);
		ds_estilo_res_w		:= substr(ds_estilo_res_w || ie_separador_w || ie_estilo_w,1,255);
		ie_separador_w		:= ';';
		end;
	END LOOP;
	ds_retorno_w	:=	horarios_w.count || '#' || horario_w.count || '#' ||  
				ds_hor_res_w || '#' || ds_pos_res_w ||
				'#' || ds_estilo_res_w || '#' || ds_cor_res_w;
	end;
end if;

return substr(ds_retorno_w,1,2000);
end;
/