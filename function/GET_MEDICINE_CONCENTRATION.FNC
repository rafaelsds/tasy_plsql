create or replace FUNCTION get_medicine_concentration(	cd_material_p 	VARCHAR)

RETURN NUMBER IS

total_conentration_w		NUMBER (15,4) := 0.0000;
cd_unid_med_concetracao_w VARCHAR2(10);
qt_concentracao_total_w NUMBER(15,4);
cd_unid_med_conc_total_w VARCHAR2(10);
qt_conversao_mg_w NUMBER(15,4);
qt_dose_diaria_w NUMBER(15,4);
cd_unidade_medida_w VARCHAR2(10);

BEGIN
if (cd_material_p is not null) then
SELECT m.cd_unid_med_concetracao,
m.qt_concentracao_total ,
m.cd_unid_med_conc_total,
m.qt_conversao_mg,
c.qt_dose_diaria,
c.cd_unidade_medida into cd_unid_med_concetracao_w,qt_concentracao_total_w,cd_unid_med_conc_total_w,qt_conversao_mg_w,qt_dose_diaria_w,cd_unidade_medida_w
FROM 
material m inner join
cih_medicamento c on c.cd_medicamento = m.cd_medicamento and m.cd_material = cd_material_p;
    
if ((UPPER(cd_unid_med_conc_total_w) = UPPER(cd_unidade_medida_w)) and qt_concentracao_total_w is not null) then

    select decode(UPPER(cd_unid_med_conc_total_w),'MG',qt_concentracao_total_w/1000, 'MCG',qt_concentracao_total_w/1000000,qt_concentracao_total_w) into total_conentration_w from dual ;
    
elsif ((UPPER(cd_unid_med_conc_total_w) != UPPER(cd_unidade_medida_w)) and qt_concentracao_total_w is not null) then

    select decode(UPPER(cd_unid_med_conc_total_w),'MG',qt_concentracao_total_w/1000, 'MCG',qt_concentracao_total_w/1000000,qt_concentracao_total_w) into total_conentration_w from dual ;
    
elsif ((UPPER(cd_unid_med_concetracao_w) = UPPER(cd_unidade_medida_w )) and qt_conversao_mg_w is not null)then

    select  decode(UPPER(cd_unid_med_concetracao_w),'MG',qt_conversao_mg_w/1000, 'MCG',qt_conversao_mg_w/1000000,qt_conversao_mg_w) into total_conentration_w from dual ;
    
elsif ((UPPER(cd_unid_med_concetracao_w) != UPPER(cd_unidade_medida_w )) and qt_conversao_mg_w is not null)then

    select  decode(UPPER(cd_unid_med_concetracao_w),'MG',qt_conversao_mg_w/1000, 'MCG',qt_conversao_mg_w/1000000,qt_conversao_mg_w) into total_conentration_w from dual ;
    
elsif (cd_unidade_medida_w is null and cd_unid_med_conc_total_w is not null and qt_concentracao_total_w is not null) then

    select decode(UPPER(cd_unid_med_conc_total_w),'MG',qt_concentracao_total_w/1000, 'MCG',qt_concentracao_total_w/1000000,qt_concentracao_total_w) into total_conentration_w from dual ;
    
elsif (cd_unidade_medida_w is null and qt_concentracao_total_w is null and qt_conversao_mg_w is not null)then

    select decode(UPPER(cd_unid_med_concetracao_w),'MG',qt_conversao_mg_w/1000, 'MCG',qt_conversao_mg_w/1000000,qt_conversao_mg_w) into total_conentration_w from dual ;
    
end if;
    
END IF;

RETURN total_conentration_w;

END get_medicine_concentration;

/