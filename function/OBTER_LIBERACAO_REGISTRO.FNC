create or replace
function obter_liberacao_registro (
		ie_liberacao_p		varchar2,
		nm_usuario_p		varchar2,
		dt_liberacao_registro_p	date,
		nm_usuario_registro_p	varchar2)
		return varchar2 is
		
ie_liberacao_w	varchar2(1) := 'S';
		
begin
if	(ie_liberacao_p = 'S') then
	begin
	if	(dt_liberacao_registro_p is not null) or
		(nm_usuario_p = nm_usuario_registro_p) then
		begin
		ie_liberacao_w := 'S';
		end;
	else
		begin
		ie_liberacao_w := 'N';
		end;
	end if;
	end;
end if;
return ie_liberacao_w;
end obter_liberacao_registro;
/