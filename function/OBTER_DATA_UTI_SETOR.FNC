create or replace
function Obter_Data_UTI_Setor(	nr_atendimento_p	number,
				dt_periodo_inicio_p	date,
				dt_periodo_final_p	date,
				qt_dias_p		number,
				ie_opcao_p		varchar2)
 		    	return date is

dt_retorno_w		Date;	
dt_entrada_unidade_w	Date;
dt_saida_unidade_w	Date;
qt_dias_acumulado_w	Number;
qt_dias_passagem_w	Number;
qt_dif_restante_w	Number;
dt_entrada_w		Date;
dt_saida_w		Date;
			
Cursor C01 is
	select	pkg_date_utils.start_of(a.dt_entrada_unidade, 'dd'),
		pkg_date_utils.start_of(NVL(a.dt_saida_unidade,SYSDATE), 'dd'),
		dt_entrada_unidade,
		nvl(dt_saida_unidade,sysdate)
	from	atend_paciente_unidade  a,
		setor_atendimento 	b
	where	a.nr_atendimento = nr_atendimento_p	
	and 	a.cd_setor_atendimento = b.cd_setor_atendimento	
	and 	(((ie_opcao_p = 'UTI') and (b.cd_classif_setor = 4)) or
		 ((ie_opcao_p = 'SETOR') and (b.cd_classif_setor in (1,3,8))))	
	and 	nvl(a.dt_saida_unidade, sysdate) > dt_periodo_inicio_p
	and 	a.dt_entrada_unidade <= dt_periodo_final_p
	order by a.dt_entrada_unidade;
			
begin

/*
OS 569475

Pacote de 01/07/2013 10:00:00 � 15/07/2013 15:00:00
Sendo 6 Dias de UTI e 2 dias de Setores.

01/07/2013 10:00:00    05/07/2013 17:00:00   UTI
05/07/2013 17:00:00	08/07/2013 17:10:00   Unidade A
08/07/2013 17:10:00	  		UTI


A function retorna a data limite para que os gastos entrem no pacote.
No caso do exemplo acima deve retornar 10/07/2013 17:10:00 que completa 6 dias de UTI
Todos os lanl�amentos at� 10/07/2013 

*/

qt_dias_acumulado_w	:= 0;
qt_dif_restante_w	:= qt_dias_p;
qt_dias_passagem_w	:= 0;

open C01;
loop
fetch C01 into	
	dt_entrada_unidade_w,
	dt_saida_unidade_w,
	dt_entrada_w,
	dt_saida_w;
exit when C01%notfound;
	begin
	
	if	(dt_saida_unidade_w > dt_periodo_final_p) then
		dt_saida_unidade_w:= dt_periodo_final_p;
	elsif	(dt_entrada_unidade_w < dt_periodo_inicio_p) then
		dt_entrada_unidade_w:= pkg_date_utils.start_of(dt_periodo_inicio_p,'dd');
	end if;
	
	qt_dias_passagem_w	:= (dt_saida_unidade_w - dt_entrada_unidade_w);	
		
	
	if	((qt_dias_passagem_w + qt_dias_acumulado_w) > qt_dias_p) then
		dt_retorno_w		:= dt_entrada_w + qt_dif_restante_w;
	else
		dt_retorno_w		:= dt_saida_w;
		qt_dif_restante_w	:= qt_dif_restante_w - qt_dias_passagem_w;		
	end if;
	
	qt_dias_acumulado_w	:= qt_dias_acumulado_w + qt_dias_passagem_w;
	
	end;
end loop;
close C01;

return	dt_retorno_w;

end Obter_Data_UTI_Setor;
/
