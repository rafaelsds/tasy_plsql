create or replace
function obter_frete_rateio_item_nf(
			nr_sequencia_p			number,
			nr_item_nf_p			number)
return number is

vl_retorno_w			number(13,4);
qt_item_total_nf_w			number(13,4);
qt_item_nf_w			number(13,4);
vl_frete_w			number(13,4);
vl_porcento_w			number(13,4);

begin

select	nvl(vl_frete,0)
into	vl_frete_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

select	sum(qt_item_nf)
into	qt_item_total_nf_w
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_p;

select	sum(qt_item_nf)
into	qt_item_nf_w
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_p
and	nr_item_nf = nr_item_nf_p;

vl_porcento_w	:= dividir((qt_item_nf_w * 100),qt_item_total_nf_w);  /* decobrir quantos % o item representa na nota*/
vl_retorno_w	:= nvl(dividir((vl_frete_w * vl_porcento_w),100),0);


return	vl_retorno_w;

end obter_frete_rateio_item_nf;
/