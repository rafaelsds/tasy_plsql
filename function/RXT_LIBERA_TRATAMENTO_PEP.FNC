create or replace
procedure rxt_libera_tratamento_pep(nr_sequencia_p NUMBER,
									nm_usuario_p		Varchar2,
									cd_estabelecimento_p varchar2) is 

param_60_w 			varchar2(1);			
param_61_w 			varchar2(1);
qt_check_film_w 	rxt_tratamento.qt_check_film%type;
cd_pessoa_w			rxt_tumor.cd_pessoa_fisica%type;

begin
Obter_Param_Usuario(3030,60,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,param_60_w);
Obter_Param_Usuario(3030,61,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,param_61_w);

RXT_LIBERAR_TRATAMENTO(nr_sequencia_p, nm_usuario_p);

select 	a.qt_check_film,
		b.cd_pessoa_fisica
into 	qt_check_film_w,
		cd_pessoa_w
from rxt_tratamento a,
	rxt_tumor b
where a.nr_sequencia = nr_sequencia_p
and a.nr_seq_tumor = b.nr_sequencia;

if (param_60_w = 'S') and
	(qt_check_film_w > 0) then
	RXT_LANCAR_CHECKFILM_CONTA(nr_sequencia_p, qt_check_film_w, cd_pessoa_w, obter_setor_ativo, nm_usuario_p);
	
end if;

if (param_61_w = 'S') then
	RXT_LANCAR_ACESSORIO_CONTA(nr_sequencia_p,cd_pessoa_w,obter_setor_ativo,nm_usuario_p);
end if;


				
end rxt_libera_tratamento_pep;
/