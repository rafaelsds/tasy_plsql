create or replace
function obter_prescr_cir_especial(	nr_cirurgia_p	number)
				return number is

nr_prescricao_w		number;

begin

select 	max(nr_prescricao)
into 	nr_prescricao_w
from   	prescr_medica
where  	nr_cirurgia = nr_cirurgia_p
and 	nvl(ie_tipo_prescr_cirur,0) <> '2';

return nr_prescricao_w;

end obter_prescr_cir_especial;
/