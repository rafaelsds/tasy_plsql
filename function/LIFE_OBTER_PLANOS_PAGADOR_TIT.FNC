create or replace
function life_obter_planos_pagador_tit(nr_seq_pagador_p		number,
										ie_opcao_p			number)
									return varchar2 is

/* IE_OPCAO_P
1 = Nr seq Plano + Ds Plano.
2 = Plano: Ds plano + C�digo ANS: Nr protocolo ANS
*/
									
									
									
ds_retorno_w		varchar2(4000);
nr_seq_plano_w		pls_contrato_plano.nr_seq_plano%type;
ds_plano_w			pls_plano.ds_plano%type;
nr_protocolo_ans_w	pls_plano.nr_protocolo_ans%type;
				
Cursor C01 is
	select c.nr_seq_plano,
		   d.ds_plano,
		   d.nr_protocolo_ans
	from   pls_contrato  a,
		   pls_contrato_pagador b,
		   pls_contrato_plano c,
		   pls_plano  d
	where a.nr_sequencia = b.nr_seq_contrato
	and   a.nr_sequencia = c.nr_seq_contrato
	and   d.nr_sequencia = c.nr_seq_plano
	and   b.nr_sequencia = nr_seq_pagador_p/*686*/;			
				
begin

if (nr_seq_pagador_p is not null) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_plano_w,
		ds_plano_w,
		nr_protocolo_ans_w;
	exit when C01%notfound;
		begin
		
		if (ie_opcao_p = 1) then
		
			if (ds_retorno_w is null) then
				ds_retorno_w := nr_seq_plano_w || ' - ' || ds_plano_w;
			else 
				ds_retorno_w := ds_retorno_w  || chr(13) || chr (10) || nr_seq_plano_w || ' - ' || ds_plano_w;
			end if;
			
		elsif (ie_opcao_p = 2) then
		
			if (ds_retorno_w is null) then
				ds_retorno_w := 'Plano: ' || ds_plano_w || ' C�digo ANS: '|| nr_protocolo_ans_w;
			else 
				ds_retorno_w := ds_retorno_w  || chr(13) || chr (10) || 'Plano: ' || ds_plano_w || ' C�digo ANS: '|| nr_protocolo_ans_w;
			end if;
		
		end if;
		
		end;
	end loop;
	close C01;

end if;

return	ds_retorno_w;

end life_obter_planos_pagador_tit;
/