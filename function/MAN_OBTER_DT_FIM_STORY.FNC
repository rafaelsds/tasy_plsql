create or replace
function man_obter_dt_fim_story(nr_story_p		number,
				dt_referencia_p		date default sysdate)
 		    	return date is

dt_atualizacao_nrec_w	desenv_atividade.dt_atualizacao_nrec%type;
			
begin

select	max(dt_atualizacao_nrec)
into	dt_atualizacao_nrec_w
from	(select	dt_atualizacao_nrec
	from	desenv_atividade
	where	nr_story = nr_story_p
	and	cd_status = 4
	and	dt_atualizacao_nrec < dt_referencia_p
	order by dt_atualizacao_nrec desc)
where	rownum = 1;

return	dt_atualizacao_nrec_w;

end man_obter_dt_fim_story;
/