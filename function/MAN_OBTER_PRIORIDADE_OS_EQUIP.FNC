create or replace
function man_obter_prioridade_os_equip(
			ie_parado_p		varchar2,
			nr_seq_equipamento_p	number)
 		    	return varchar2 is
			
ie_retorno_w		varchar2(1);
cd_impacto_w		varchar2(1);

Cursor C01 is
	select	ie_prioridade
	from	man_regra_prior_equip_os
	where	nvl(ie_parado,ie_parado_p) = ie_parado_p
	and	nvl(cd_impacto,cd_impacto_w) = cd_impacto_w
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate)
	order by ie_parado desc,
		cd_impacto desc,
		nr_sequencia;

begin

if	(nvl(ie_parado_p,'0') <> '0') and
	(nvl(nr_seq_equipamento_p,0) > 0) then
	begin
	select	nvl(max(cd_impacto),'0')
	into	cd_impacto_w
	from	man_equipamento
	where	nr_sequencia = nr_seq_equipamento_p;
	
	open C01;
	loop
	fetch C01 into	
		ie_retorno_w;
	exit when C01%notfound;
	end loop;
	close C01;
	end;
end if;

return	ie_retorno_w;

end man_obter_prioridade_os_equip;
/