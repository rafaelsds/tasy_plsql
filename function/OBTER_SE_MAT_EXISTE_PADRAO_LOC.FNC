create or replace
function obter_se_mat_existe_padrao_loc(
		cd_material_p		number,
		cd_local_estoque_p	number)
	return varchar2 is

qt_existe_w	number(10);
ie_existe_w	varchar2(1);

begin

ie_existe_w	:= 'N';

select	count(*)
into	qt_existe_w
from	padrao_estoque_local
where	cd_material		= cd_material_p
and	cd_local_estoque	= cd_local_estoque_p;

if	(qt_existe_w > 0) then
	ie_existe_w := 'S';
end if;

return	ie_existe_w;

end obter_se_mat_existe_padrao_loc;
/