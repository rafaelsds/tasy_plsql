CREATE OR REPLACE FUNCTION VERIFY_HAS_ITEM_PEPA_RELEASED (nr_seq_atend_cons_pepa_p number) RETURN varchar2
IS
    array_tabelas_w STRARRAY := STRARRAY();
    count_register_w number := 0;
    ds_tipo_item_w varchar2(20);
    nr_sequencia_item_w ehr_registro.nr_sequencia%type;
    nr_node_schematic_pepa_w prontuario_item.nr_node_schematic_pepa%type;

cursor c01 is 
    (SELECT
        a.nr_seq_template AS NR_SEQUENCIA, 
        'TEMPLATE' AS DS_TIPO
    FROM 
        EHR_TIPO_REG_ITEM a,
        ATEND_CONSULTA_PEPA b
    WHERE 1=1
        AND a.nr_seq_tipo_reg = b.NR_SEQ_TIPO_CONSULTA
        AND b.nr_sequencia = nr_seq_atend_cons_pepa_p
        AND a.nr_seq_template IS NOT NULL
    UNION
    SELECT
        a.NR_SEQ_ITEM_PEP AS NR_SEQUENCIA, 
        'ITEM_PEPA' AS DS_TIPO
    FROM 
        EHR_TIPO_REG_ITEM a,
        ATEND_CONSULTA_PEPA b
    WHERE 1=1
        AND a.nr_seq_tipo_reg = b.NR_SEQ_TIPO_CONSULTA
        AND b.nr_sequencia = nr_seq_atend_cons_pepa_p
        AND a.NR_SEQ_ITEM_PEP IS NOT NULL);


cursor c02(nr_sequencia_item_p NUMBER) is
    (SELECT 
        nr_sequencia
    FROM 
        ehr_registro
    WHERE 1=1 
        AND nr_seq_atend_cons_pepa = nr_seq_atend_cons_pepa_p
        AND nr_seq_templ = nr_sequencia_item_p
        AND dt_liberacao IS NOT NULL);

cursor c03(nr_sequencia_item_p NUMBER) is
    (SELECT 
        nr_node_schematic_pepa
    FROM 
        prontuario_item 
    WHERE 1=1
        AND nr_sequencia = nr_sequencia_item_p);


FUNCTION TO_STRING(str varchar2) RETURN varchar2 IS
BEGIN
    return chr(39) || str || chr(39);
END TO_STRING;

FUNCTION VERIFY_HAS_ATTR(nm_tabela_p varchar2, nm_atributo_p varchar2) RETURN varchar2 IS
  sql_stmt varchar2 ( 1000 ) := null;
  count_table number := 0;
BEGIN
  sql_stmt := 'SELECT COUNT(1) FROM tabela_atributo attr1 INNER JOIN  tabela_atributo attr2 ON attr1.nm_tabela = attr2.nm_tabela AND attr2.nm_atributo = '|| TO_STRING(nm_atributo_p) ||' WHERE 1=1 AND attr1.nm_tabela = '|| TO_STRING(nm_tabela_p);
  EXECUTE IMMEDIATE sql_stmt into count_table;
  if (count_table > 0) then
      return 'S';
  end if;
  
  return 'N';  
END VERIFY_HAS_ATTR;


FUNCTION COUNT_REGISTER(nm_tabela_p varchar2) RETURN number IS
    sql_stmt varchar2 ( 1000 ) := null;
    count_table number := 0;
BEGIN
    if (VERIFY_HAS_ATTR(nm_tabela_p, 'DT_LIBERACAO') = 'S') then
        sql_stmt := 'SELECT COUNT(1) FROM '|| nm_tabela_p ||' WHERE 1=1 AND DT_LIBERACAO IS NOT NULL AND NR_SEQ_ATEND_CONS_PEPA = '|| nr_seq_atend_cons_pepa_p;
        EXECUTE IMMEDIATE sql_stmt into count_table;
    end if;
    return count_table;
END COUNT_REGISTER;

PROCEDURE EXECUTE_ACTION_PER_ITEM(array_tabelas_p STRARRAY) IS
count_reg_table number := 0;
BEGIN
  for i in 1..array_tabelas_p.count loop
    begin
      if (VERIFY_HAS_ATTR(array_tabelas_p(i), 'NR_SEQ_ATEND_CONS_PEPA') = 'S') then
        count_reg_table := COUNT_REGISTER(array_tabelas_p(i));
        if (count_reg_table > 0) then
          count_register_w := count_register_w + count_reg_table;
        end if;
      end if;
    end;
  end loop;
END EXECUTE_ACTION_PER_ITEM;

begin
open c01;
	loop
	fetch c01 into nr_sequencia_item_w, ds_tipo_item_w;
	exit when c01%notfound;
		begin
      if (ds_tipo_item_w = 'ITEM_PEPA') then
          open c03(nr_sequencia_item_w);
              loop 
                  fetch c03 into nr_node_schematic_pepa_w;
                  exit when c03%notfound;
                      begin
                        EXECUTE_ACTION_PER_ITEM(GET_TABLE_NAMES_BY_NODE_PEPA(nr_node_schematic_pepa_w));
                      end;
              end loop;
          close c03;

      elsif ( ds_tipo_item_w = 'TEMPLATE') then
          open c02(nr_sequencia_item_w);
              loop
              fetch c02 into nr_sequencia_item_w;
              exit when c02%notfound;
                  begin
                      count_register_w := count_register_w + 1;
                  end;
              end loop;
          close c02;

      end if;
    end;
	end loop;
	close c01;
    
    if (count_register_w > 0) then
        return 'S';
    end if;
    
    return 'N';

end VERIFY_HAS_ITEM_PEPA_RELEASED;
/