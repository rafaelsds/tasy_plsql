create or replace
function hem_obter_ds_lesao_padrao(nr_seq_proc_p	hem_proc.nr_sequencia%type)
				return varchar2 is

Cursor C01 is
	select	ds_lesao
	from	hem_lesao
	where	nr_seq_proc = nr_seq_proc_p 
	order by 1;

qt_registros_w 		number(10);
ds_nova_lesao_w		hem_lesao.ds_lesao%type;
nr_contador_w		number(10);
ds_texto_w	varchar2(10);

begin

select	count(1)
into	qt_registros_w
from	hem_lesao
where	nr_seq_proc = nr_seq_proc_p;

ds_texto_w := wheb_mensagem_pck.get_Texto(1008092); --Les�o
nr_contador_w := 1;

if	(qt_registros_w > 0) then
	
	ds_nova_lesao_w :=  ds_texto_w ||' '|| (qt_registros_w + 1);

	-- Verificar se existe alguma descri��o igual para gerar a pr�xima.
	
	/*for r_C01 in C01 loop
		begin
		if	(r_C01.ds_lesao = ds_nova_lesao_w) then
			nr_contador_w := nr_contador_w + 1;
			ds_nova_lesao_w := ds_texto_w ||' '|| nr_contador_w;
		end if;
		end;
	end loop;*/
	
else
	ds_nova_lesao_w :=  ds_texto_w ||' 1';
end if;

return	ds_nova_lesao_w;

end hem_obter_ds_lesao_padrao;
/
