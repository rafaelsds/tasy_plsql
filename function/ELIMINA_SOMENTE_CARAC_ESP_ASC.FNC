create or replace
function elimina_somente_carac_esp_asc(
		ds_campo_p	varchar2)
		return varchar2 is

ds_retorno_w	varchar2(2000);

begin

ds_retorno_w := elimina_caractere_esp_asc(ds_campo_p, 'S', 'N');
	
return	ds_retorno_w;

end elimina_somente_carac_esp_asc;
/