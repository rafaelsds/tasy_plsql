create or replace
function age_cons_obter_motivo_agend (
		cd_motivo_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
begin

select	max(ds_motivo)
into	ds_retorno_w
from	motivo_agendamento
where	nr_sequencia = cd_motivo_p;

return	ds_retorno_w;

end age_cons_obter_motivo_agend;
/