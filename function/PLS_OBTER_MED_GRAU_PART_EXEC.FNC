create or replace
function pls_obter_med_grau_part_exec(	nr_seq_item_p		pls_conta_proc.nr_sequencia%type,
					ie_mat_proc_p		varchar2,
					ie_tipo_p		varchar2)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter o c�digo do m�dico executor da conta ou o grau de participa��o do mesmo.

ie_tipo_p 
'M' = M�dico
'G' = 'Grau de participa��o'
 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
		
ds_retorno_w		pls_conta.cd_medico_executor%type;
cd_medico_executor_w	pls_conta.cd_medico_executor%type;
nr_seq_conta_w		pls_conta.nr_sequencia%type;
nr_seq_grau_partic_w	pls_conta.nr_seq_grau_partic%type;

begin
--Busca o sequencial da conta do item
select	max(nr_seq_conta)
into	nr_seq_conta_w
from	(
	select	nr_seq_conta
	from	pls_conta_proc
	where	nr_sequencia = nr_seq_item_p
	and	ie_mat_proc_p = 'P'
	union all
	select	nr_seq_conta
	from	pls_conta_mat
	where	nr_sequencia = nr_seq_item_p
	and	ie_mat_proc_p = 'M');

--Verifica se na conta possui m�dico executor
select	max(cd_medico_executor),
	max(nr_seq_grau_partic)
into	cd_medico_executor_w,
	nr_seq_grau_partic_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_w;

--Caso sim, retorna o mesmo.
if	(cd_medico_executor_w is not null) then
	
	if (ie_tipo_p = 'M') then
		ds_retorno_w := cd_medico_executor_w;
	else
		ds_retorno_w := nr_seq_grau_partic_w;
	end if;
--Caso n�o, ir� procurar pelos participantes	
elsif	(cd_medico_executor_w is null) then
	--Procedimento
	if	(ie_mat_proc_p = 'P') then
		--Ir� verificar primeiramente por participantes do tipo cirurgi�o
		select	max(cd_medico),
			max(nr_seq_grau_partic)
		into	cd_medico_executor_w,
			nr_seq_grau_partic_w
		from	pls_proc_participante a
		where	nr_seq_conta_proc = nr_seq_item_p
		and	exists(	select	1
				from	pls_grau_participacao b
				where	b.nr_sequencia = a.nr_seq_grau_partic
				and	cd_tiss = '00');
		--Caso n�o possua cirurgi�o, ir� procurar por anestesista
		if	(cd_medico_executor_w is null) then
			select	max(cd_medico),
				max(nr_seq_grau_partic)
			into	cd_medico_executor_w,
				nr_seq_grau_partic_w
			from	pls_proc_participante a
			where	nr_seq_conta_proc = nr_seq_item_p
			and	exists(	select	1
					from	pls_grau_participacao b
					where	b.nr_sequencia = a.nr_seq_grau_partic
					and	cd_tiss = '06');
		end if;
		--Caso n�o encontre anestesista, ir� procurar por primeiro auxiliar
		if	(cd_medico_executor_w is null) then
			select	max(cd_medico),
				max(nr_seq_grau_partic)
			into	cd_medico_executor_w,
				nr_seq_grau_partic_w
			from	pls_proc_participante a
			where	nr_seq_conta_proc = nr_seq_item_p
			and	exists(	select	1
					from	pls_grau_participacao b
					where	b.nr_sequencia = a.nr_seq_grau_partic
					and	cd_tiss = '01');
		end if;
		--Caso n�o encontre primeiro auxiliar, ir� pegar o maior c�digo (tratativa atual)
		if	(cd_medico_executor_w is null) then
			select	max(cd_medico),
				max(nr_seq_grau_partic)
			into	cd_medico_executor_w,
				nr_seq_grau_partic_w
			from	pls_proc_participante a
			where	nr_seq_conta_proc = nr_seq_item_p;
		end if;
		
		if (ie_tipo_p = 'M') then
			ds_retorno_w := cd_medico_executor_w;
		else
			ds_retorno_w := nr_seq_grau_partic_w;
		end if;
	else
		--Caso n�o seja procedimento, ir� retornar nulo, pois no caso de materiais
		--n�o temos como identificar qual o m�dico executor que utilizou o material,
		--desta forma, para materiais, deve-se cadastrar regra de ato cooperado
		--restringindo pelo prestador pagador do item, assim podendo gerar as informa��es
		--do ato cooperado com mais integridade.
		ds_retorno_w := null;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_med_grau_part_exec;
/
