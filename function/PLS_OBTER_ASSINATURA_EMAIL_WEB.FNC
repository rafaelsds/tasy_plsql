create or replace
function pls_obter_assinatura_email_web(ie_funcao_regra_p	Number,
					ie_quebrar_linha_p	varchar2,
					cd_estabelecimento_p	number)
					return Varchar2 is


ds_retorno_w		Varchar2(4000);						
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
						
begin
if (cd_estabelecimento_p is not null and cd_estabelecimento_p > 0) then
	cd_estabelecimento_w	:= cd_estabelecimento_p;
end if;

select	substr(max(ds_assinatura),1,4000)
into	ds_retorno_w
from	pls_assinatura_email_web
where	(ie_funcao_regra = ie_funcao_regra_p or ie_funcao_regra = 1)
and	ie_situacao = 'A'
and	(cd_estabelecimento_w is null 
or	cd_estabelecimento	= cd_estabelecimento_w);

if (ds_retorno_w is not null and ie_quebrar_linha_p = 'S') then
	ds_retorno_w	:= replace(ds_retorno_w, '\n', '<br/>');
elsif (ds_retorno_w is not null and ie_quebrar_linha_p = 'N') then
	ds_retorno_w	:= replace(ds_retorno_w, '\n', '');
end if;	

return	ds_retorno_w;

end pls_obter_assinatura_email_web;
/
