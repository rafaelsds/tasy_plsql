create or replace
function	obter_data_saida_unidade(nr_prescricao_p	number,nr_atendimento_p number)
		return date is

dt_saida_unidade_w	date;

begin

if	(nr_atendimento_p is not null) then

	select	max(x.dt_saida_unidade)
	into	dt_saida_unidade_w
	from	atend_paciente_unidade x,
			atendimento_paciente z,
			prescr_medica y
	where	x.nr_atendimento = z.nr_atendimento
	and		x.nr_atendimento = y.nr_atendimento
	and		x.nr_atendimento = nr_atendimento_p
	and		y.nr_prescricao  = nr_prescricao_p;
end if;

return	dt_saida_unidade_w;

end obter_data_saida_unidade;
/
