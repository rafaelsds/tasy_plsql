create or replace
function hjf_obter_se_divide_honorario(	cd_procedimento_p	number)
					return varchar2 is

ie_divide_w		varchar2(1):= 'N';

begin

if	((substr(cd_procedimento_p,1,2) in ('31','32','33','34','36')) and
	(substr(cd_procedimento_p,1,4) not in ('3112','3213'))) then

	ie_divide_w:= 'S';

elsif	(substr(cd_procedimento_p,1,4) = '8201') then

	ie_divide_w:= 'S';

else

	ie_divide_w:= 'N';

end if;

return ie_divide_w;

end hjf_obter_se_divide_honorario;
/