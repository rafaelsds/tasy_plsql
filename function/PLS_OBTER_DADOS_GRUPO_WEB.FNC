create or replace
function pls_obter_dados_grupo_web
			(	nr_seq_grupo_web_p	number,
				ie_opcao_p		varchar2)
				return varchar2 is

/*	
N - Nome do usuario acesso grupo contrato
*/

ds_retorno_w			varchar2(255);


begin
if	(ie_opcao_p = 'N') then
	select	nvl(max(nm_usuario_web),'')
	into 	ds_retorno_w
	from	pls_grupo_contrato_web
	where	nr_sequencia	=  nr_seq_grupo_web_p;
end if;

return	ds_retorno_w;

end pls_obter_dados_grupo_web;
/