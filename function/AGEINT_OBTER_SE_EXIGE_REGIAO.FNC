create or replace
function Ageint_obter_se_exige_regiao(nr_seq_proc_interno_p	number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1) := 'N';
			
begin

if	(nr_seq_proc_interno_p is not null) then

	select	nvl(max(ie_exige_regiao_ageint),'N')
	into	ie_retorno_w
	from	proc_interno
	where	nr_sequencia	= nr_seq_proc_interno_p;
	
end if;

return	ie_retorno_w;

end Ageint_obter_se_exige_regiao;
/