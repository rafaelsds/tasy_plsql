create or replace
function obter_cbo_saude(nr_seq_cbo_p	number)
					return varchar2 is
/** Retorna a descri��o do CBO 
 *  Juliane Menin - 25/10/2007
*/
ds_retorno_w	varchar2(80);

begin
if	(nr_seq_cbo_p is not null) then
	select	substr(ds_cbo,1,80)
	into	ds_retorno_w
	from	cbo_saude
	where	nr_sequencia = nr_seq_cbo_p;
end if;

return ds_retorno_w;

end obter_cbo_saude;
/