create or replace
function obter_qtd_doacoes_aptas( cd_pessoa_fisica_p	varchar2, 
				  nr_seq_doacao_p	number)
 		    	return number is

qtd_w	number(5);
ie_considera_doacao_w	varchar2(1);
ie_doacao_w		varchar2(1);
begin

select ie_contar_doacao
into	ie_considera_doacao_w
from 	san_parametro 
where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;


if (ie_considera_doacao_w = 'S') then
	select	count(*)+1
	into	qtd_w
	from   	san_doacao
	where  	ie_avaliacao_final 	= 'A'
	and	nr_sequencia	 <> nr_seq_doacao_p
	and	cd_pessoa_fisica 	= cd_pessoa_fisica_p;
else
	begin
	select 	decode(count(*),0,'S','N')
	into	ie_doacao_w
	from	san_doacao
	where	nr_sequencia = nr_seq_doacao_p
	and 	ie_status in (1,5);
	
	if (ie_doacao_w = 'S') then
		select	count(*)
		into	qtd_w
		from   	san_doacao
		where  	ie_avaliacao_final 	= 'A'
		and	nr_sequencia	 <> nr_seq_doacao_p
		and	cd_pessoa_fisica 	= cd_pessoa_fisica_p;
	else
		select	count(*)+1
		into	qtd_w
		from   	san_doacao
		where  	ie_avaliacao_final 	= 'A'
		and	nr_sequencia	 <> nr_seq_doacao_p
		and	cd_pessoa_fisica 	= cd_pessoa_fisica_p;
	end if;
	end;
end if;

return	qtd_w;

end obter_qtd_doacoes_aptas;
/
