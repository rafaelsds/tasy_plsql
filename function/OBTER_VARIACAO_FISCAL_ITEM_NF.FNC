create or replace
function obter_variacao_fiscal_item_nf(	nr_seq_nota_p		varchar2,
					ie_tributo_p		varchar2,
					ie_tipo_valor_fiscal_p	number)
					return varchar2 is

ds_retorno_w		varchar2(20);

begin

select  nvl(a.vl_tributo,0)
into	ds_retorno_w
from    nota_fiscal_item_trib a,
        nota_fiscal_item i,
        material m,
        fis_variacao_fiscal f,
        tributo b
where   i.nr_item_nf     		= a.nr_item_nf
and     i.nr_sequencia   		= a.nr_sequencia
and     m.cd_material    		= i.cd_material
and     m.cd_material    		= f.cd_material
and     a.cd_tributo 			= b.cd_tributo
and	f.ie_situacao			= 'A'
and     i.nr_sequencia   		= nr_seq_nota_p
and     b.ie_tipo_tributo    		= ie_tributo_p
and     f.cd_codigo_valores_fiscais	= ie_tipo_valor_fiscal_p;


return	ds_retorno_w;

end obter_variacao_fiscal_item_nf;
/