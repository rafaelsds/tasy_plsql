create or replace
function Obter_desc_proc_rotina(nr_sequencia_p		number)
 		    	return varchar2 is

ds_informacao_w		varchar2(2000);
	
begin

select	max(ds_prescricao)
into	ds_informacao_w
from	procedimento_rotina
where	nr_sequencia = nr_sequencia_p;

return	ds_informacao_w;

end Obter_desc_proc_rotina;
/
