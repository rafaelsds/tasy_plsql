Create Or Replace
Function Obter_Descricao_Equipe(
			nr_seq_area_p 	varchar2)
			return varchar2 is

ds_retorno_w		varchar2(200);

Begin

Select Max(E.Ds_Equipe)
into ds_retorno_w
from pf_equipe e
inner join psf_area a
On E.Nr_Sequencia = A.Nr_Seq_Equipe
And A.Nr_Sequencia = Nr_Seq_Area_P
Group By Ds_Equipe;

return ds_retorno_w;

end Obter_Descricao_Equipe;
/