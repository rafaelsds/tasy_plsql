create or replace
function obter_regra_alta_atend_rn(cd_setor_atendimento_p		number,
				cd_estabelecimento_p	number)
 		    	return varchar2 is
ie_possui_regra_w	varchar2(1);
begin
if	(nvl(cd_setor_atendimento_p,0) > 0) then
	select	decode(count(*),0,'N','S')
	into	ie_possui_regra_w
	from	regra_gerar_alta_atend_rn
	where	cd_setor_atendimento = cd_setor_atendimento_p
	and	cd_estabelecimento = cd_estabelecimento_p;
	
end if;	


return	ie_possui_regra_w;

end obter_regra_alta_atend_rn;
/