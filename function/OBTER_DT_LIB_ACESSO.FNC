create or replace
function Obter_Dt_Lib_Acesso(
				nr_Atendimento_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(3)	:= 'S';
qt_visita_w	number(10);
			
begin

select	count(*)
into	qt_visita_w
from	atendimento_visita
where	nr_atendimento		= nr_atendimento_p
and	dt_liberacao_acesso is not null
and	ie_paciente		= 'S';

if	(qt_visita_w	> 0) then
	ds_retorno_w	:= 'N';
end if;

return	ds_retorno_w;

end Obter_Dt_Lib_Acesso;
/