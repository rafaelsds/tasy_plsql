create or replace
function obtem_status_documento(nr_documento_p		ctb_documento.nr_documento%type,
				nr_seq_doc_compl_p	ctb_documento.nr_seq_doc_compl%type,
				nr_doc_analitico_p	ctb_documento.nr_doc_analitico%type,
				dt_inicial_p		date,
				dt_final_p		date,
				cd_tipo_lote_contabil_p	ctb_documento.cd_tipo_lote_contabil%type)
				return varchar is

retorno 			varchar(1);
qt_nao_conciliado_w	integer(15);
qt_conciliado_w		integer(15);
qt_par_conciliado_w	integer(15);

begin

if	(nvl(nr_seq_doc_compl_p, 0) = 0) then
	begin
	
	select (select count(1)
		from	ctb_documento
		where	nr_documento = nr_documento_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'N'
		and 	rownum 			= 1)  qt_nao_conciliado,
		(select	count(1)
		from	ctb_documento
		where	nr_documento		= nr_documento_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'C'
		and 	rownum 			= 1) qt_conciliado,
		(select count(1)
		from	ctb_documento
		where	nr_documento 		= nr_documento_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'P'
		and 	rownum 			= 1) qt_par_conciliado
	into	qt_nao_conciliado_w,
		qt_conciliado_w,
		qt_par_conciliado_w
	from 	dual;
	
	end;
elsif	(nvl(nr_seq_doc_compl_p, 0) <> 0) and (nvl(nr_doc_analitico_p, 0) = 0) then
	begin
	
	select (select count(1)
		from	ctb_documento
		where	nr_documento 		= nr_documento_p
		and	nr_seq_doc_compl 	= nr_seq_doc_compl_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'N'
		and 	rownum 			= 1)  qt_nao_conciliado,
		(select	count(1)
		from	ctb_documento
		where	nr_documento 		= nr_documento_p
		and	nr_seq_doc_compl 	= nr_seq_doc_compl_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'C'
		and 	rownum 			= 1) qt_conciliado,
		(select count(1)
		from	ctb_documento
		where	nr_documento 		= nr_documento_p
		and	nr_seq_doc_compl 	= nr_seq_doc_compl_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'P'
		and 	rownum 			= 1) qt_par_conciliado
	into	qt_nao_conciliado_w,
		qt_conciliado_w,
		qt_par_conciliado_w
	from 	dual;
	
	end;
else
	begin
	
	select (select count(1)
		from	ctb_documento
		where	nr_documento 		= nr_documento_p
		and	nr_seq_doc_compl 	= nr_seq_doc_compl_p
		and	nr_doc_analitico 	= nr_doc_analitico_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'N'
		and 	rownum 			= 1)  qt_nao_conciliado,
		(select	count(1)
		from	ctb_documento
		where	nr_documento 		= nr_documento_p
		and	nr_seq_doc_compl 	= nr_seq_doc_compl_p
		and	nr_doc_analitico 	= nr_doc_analitico_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'C'
		and 	rownum 			= 1) qt_conciliado,
		(select count(1)
		from	ctb_documento
		where	nr_documento 		= nr_documento_p
		and	nr_seq_doc_compl 	= nr_seq_doc_compl_p
		and	nr_doc_analitico 	= nr_doc_analitico_p
		and 	dt_competencia between	trunc(dt_inicial_p, 'dd') and fim_dia(dt_final_p)
		and 	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
		and	ie_status 		= 'P'
		and 	rownum 			= 1) qt_par_conciliado
	into	qt_nao_conciliado_w,
		qt_conciliado_w,
		qt_par_conciliado_w
	from 	dual;
	
	end;
end if;

if	(qt_nao_conciliado_w > 0) and (qt_conciliado_w = 0) and (qt_par_conciliado_w = 0) then
	retorno:= 'N';
elsif	(qt_nao_conciliado_w = 0) and (qt_conciliado_w > 0) and (qt_par_conciliado_w = 0) then
	retorno:= 'C';
else
	retorno:= 'P';
end if;

return	retorno;

end obtem_status_documento;
/