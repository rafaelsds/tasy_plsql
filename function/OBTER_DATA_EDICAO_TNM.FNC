create or replace
function obter_data_edicao_tnm(cd_topografia_p		varchar2)
 		    	return date is

dt_edicao_w	date;			
			
begin

if	(cd_topografia_p is not null) then


	select 	max(DT_EDICAO)
	into	dt_edicao_w
	from   	cido_topografia a,
		can_tnm_localizacao b
	where  	a.nr_seq_loc_tnm = b.nr_sequencia
	and    	a.cd_topografia = cd_topografia_p;
	
end if;	

return	dt_edicao_w;

end obter_data_edicao_tnm;
/
