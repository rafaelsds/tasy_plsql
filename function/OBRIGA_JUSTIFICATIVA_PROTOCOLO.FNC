create or replace
function obriga_justificativa_protocolo( nr_prescricao_p 	number,
									cd_protocolo_p 		number,
								nr_seq_protocolo_p	number)
							return varchar2 is

ds_retorno_w	varchar2(1) := 'N';
nr_prescricao_anterior_w	number(14);

begin

select 	max(nr_prescricao_anterior)
into	nr_prescricao_anterior_w
from	prescr_medica
where 	nr_prescricao = nr_prescricao_p;

if (nvl(nr_prescricao_p,0) > 0) and
   (nr_prescricao_anterior_w is null) and
   (cd_protocolo_p is not null) and
   (nr_seq_protocolo_p is not null) then

		select  nvl(max(ie_obriga_justificativa), 'N')
		into	ds_retorno_w
		from    protocolo_medicacao
		where   cd_protocolo = cd_protocolo_p
		and     nr_sequencia = nr_seq_protocolo_p;

end if;

return	ds_retorno_w;

end obriga_justificativa_protocolo;
/