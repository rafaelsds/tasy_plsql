create or replace
function pls_obter_seq_pagador(nr_seq_segurado_p	number)
				return 			varchar2 is
nr_seq_pagador_w 		number(10);

begin

select	nr_seq_pagador
into	nr_seq_pagador_w
from	pls_segurado
where	nr_sequencia = nr_seq_segurado_p;

return	nr_seq_pagador_w;

end pls_obter_seq_pagador;
/