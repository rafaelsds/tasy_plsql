create or replace FUNCTION PFCS_GET_PRED_CAPACITY (
    nr_sim_census_p varchar2,
    nr_total_beds_p number
)
RETURN VARCHAR2 IS

nr_census_w         number(10);
nr_census_std_w     number(3);
nr_pred_capacity    varchar(20);

BEGIN
    nr_census_w     := to_number(REGEXP_SUBSTR(nr_sim_census_p, '[^;]+', 1, 1));
    nr_census_std_w := to_number(REGEXP_SUBSTR(nr_sim_census_p, '[^;]+', 1, 2));
    
    nr_pred_capacity    := pfcs_get_percentage_value(nr_census_w, nr_total_beds_p) || ';' || pfcs_get_percentage_value(nr_census_std_w, nr_total_beds_p);
    
    RETURN nr_pred_capacity;
END PFCS_GET_PRED_CAPACITY;
/