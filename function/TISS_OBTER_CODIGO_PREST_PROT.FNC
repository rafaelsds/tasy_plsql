create or replace
function TISS_OBTER_CODIGO_PREST_PROT
		(nr_seq_retorno_p			number) return varchar2 is

ds_retorno_w		varchar2(255);
nr_seq_protocolo_w	varchar2(255);

begin

select	max(nr_seq_protocolo)
into	nr_seq_protocolo_w
from	convenio_retorno a
where	a.nr_sequencia	= nr_seq_retorno_p;

select	max(a.cd_prestador_convenio)
into	ds_retorno_w
from	protocolo_convenio a
where	a.nr_seq_protocolo	= nr_seq_protocolo_w;

return	ds_retorno_w;

end TISS_OBTER_CODIGO_PREST_PROT;
/
