create or replace
function ptu_obter_id_nota_principal(nr_seq_conta_p		pls_conta.nr_sequencia%type)
 		    	return varchar2 is

ie_retorno_w			Varchar2(2);
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type;
nr_seq_conta_referencia_w	pls_conta.nr_seq_conta_referencia%type;
/*Identifica se a conta pode ser considera de referencia ou n�o*/
begin

select	max(ie_tipo_guia),
	max(nr_seq_conta_referencia)
into	ie_tipo_guia_w,
	nr_seq_conta_referencia_w
from	pls_conta
where	nr_sequencia	= nr_seq_conta_p;

if	(ie_tipo_guia_w	= '3') then
	ie_retorno_w	:= 'S';
elsif	(ie_tipo_guia_w	= '6') then
	ie_retorno_w	:= 'N';
else
	if	(nr_seq_conta_referencia_w is not null) then
		ie_retorno_w	:= 'N';
	else
		ie_retorno_w	:= 'S';
	end if;
end if;

return	ie_retorno_w;

end ptu_obter_id_nota_principal;
/