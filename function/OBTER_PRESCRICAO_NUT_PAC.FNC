create or replace
function obter_prescricao_nut_pac(nr_sequencia_p		number)
 		    	return number is
nr_retorno_w	number(15);
begin

if (nr_sequencia_p is not null) then
		select 	max(nr_prescricao)
		into	nr_retorno_w
		from	nut_pac
		where	nr_sequencia = nr_sequencia_p;
end if;

return	nr_retorno_w;

end obter_prescricao_nut_pac;
/