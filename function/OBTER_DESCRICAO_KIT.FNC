create or replace 
function obter_descricao_kit(
			cd_kit_p	Number)
			return varchar2 is
			
ds_retorno_w	varchar2(80);

BEGIN

if	(cd_kit_p is not null) then
	select	ds_kit_material
	into	ds_retorno_w
	from	kit_material
	where	nvl(ie_situacao,'A') = 'A'
	and	cd_kit_material = cd_kit_p;
end if;

return ds_retorno_w;

end obter_descricao_kit;
/
