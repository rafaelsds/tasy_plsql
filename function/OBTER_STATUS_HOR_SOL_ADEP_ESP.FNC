create or replace
function obter_status_hor_sol_adep_esp (
		dt_instalacao_p		date,
		dt_administracao_p	date,
		dt_suspensao_p		date,
		dt_interrupcao_p	date,
		ie_dose_especial_p	varchar2,
		nr_seq_processo_p	number,
		nr_seq_area_prep_p	number,
		nr_prescricao_p		number,
		nr_seq_solucao_p	number,
		ie_solucao_especial_p	varchar2,
		ie_param673_p		varchar2,
		nr_seq_mat_hor_p	number default null)
		return varchar2 is

ie_status_processo_w	varchar2(15)	:= 'X';
ie_status_w		varchar2(15);
ie_status_sol_w		varchar2(5)	:= '';
nr_seq_mat_cpoe_w	number(10);
dt_horario_w		date;
qt_reg_w		number(10);

begin
	
if	((ie_param673_p = 'S') and (ie_solucao_especial_p = 'S')) then
	ie_status_sol_w := substr(obter_status_solucao_prescr(1,nr_prescricao_p,nr_seq_solucao_p),1,3);
end if;

if	(dt_instalacao_p is null) and
	(dt_administracao_p is null) and
	(dt_suspensao_p is null) then
	begin
		
	if	(nr_seq_mat_hor_p is not null) then
	
		select	max(b.nr_seq_mat_cpoe),
			max(a.dt_horario)
		into	nr_seq_mat_cpoe_w,
			dt_horario_w
		from	prescr_mat_hor a,
			prescr_material b
		where	a.nr_prescricao 	= b.nr_prescricao
		and	a.nr_seq_material 	= b.nr_sequencia
		and	a.nr_sequencia 		= nr_seq_mat_hor_p;
		
		select	count(1)
		into	qt_reg_w
		from	cpoe_susp_medication
		where	nr_seq_mat_cpoe = nr_seq_mat_cpoe_w
		and	dt_horario_w between dt_start and dt_end
		and	dt_inactive is null;
		
		if	(qt_reg_w > 0) then
			return 'S'; --Suspenso
		end if;
		
	end if;
		
	
	if	(nr_seq_processo_p is not null) then
		begin
		--ie_status_processo_w	:= obter_status_processo_area(nr_seq_processo_p,nr_seq_area_prep_p);
		ie_status_processo_w	:= obter_status_processo(nr_seq_processo_p);
		end;
	end if;
	if	(ie_status_processo_w in ('D','H','P','L','R','A')) then
		ie_status_w	:= ie_status_processo_w;
	elsif	(ie_dose_especial_p = 'S') then
		ie_status_w	:= 'E';
	else
		ie_status_w	:= 'N';
	end if;	
	end;
elsif	(dt_administracao_p is null) and
	(dt_suspensao_p is null) and
	(dt_interrupcao_p is null) and
	(dt_instalacao_p is not null) then
	begin
	ie_status_w	:= 'I';
	end;
elsif	(dt_administracao_p is not null) and
	(dt_suspensao_p is null) then
	begin
	ie_status_w	:= 'A';
	end;
elsif	(dt_administracao_p is null) and
	(dt_suspensao_p is not null) then
	begin
	ie_status_w	:= 'S';
	end;
elsif	(dt_interrupcao_p is not null) then
	ie_status_w	:= 'T';	
end if;

if	((ie_param673_p = 'S') and (ie_solucao_especial_p = 'S') and (ie_status_w = 'A')) then

	if	(ie_status_sol_w in ('INT', 'II')) then
		ie_status_w	:= 'T';	
	elsif	(ie_status_sol_w <> 'T') then
		ie_status_w	:= 'I';	
	end if;
	
end if;

return ie_status_w;

end obter_status_hor_sol_adep_esp;
/