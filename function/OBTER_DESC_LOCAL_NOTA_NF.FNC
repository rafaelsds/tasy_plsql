CREATE OR REPLACE
FUNCTION Obter_Desc_local_nota_nf(
		NR_SEQ_NOTA_p	Number)
	RETURN VARCHAR IS

ds_retorno_w		Varchar2(255);

BEGIN

if	( NR_SEQ_NOTA_p is not null ) then
	select	max(OBTER_DESC_LOCALIZACAO_NF(NR_SEQ_LOCALIZACAO))
	into	ds_retorno_w
	from	NOTA_FISCAL_localizacao
	where	NR_SEQ_NOTA = NR_SEQ_NOTA_p;
end if;

RETURN ds_retorno_w;

END Obter_Desc_local_nota_nf;
/
