create or replace
function Obter_Espec_Medico_Agenda (	cd_pessoa_fisica_p	varchar2,
					cd_agenda_p		number)
					return			varchar2 is

ds_retorno_w			varchar2(4000);
cd_especialidade_w		number(10,0);
					
begin

begin
select	cd_especialidade
into	cd_especialidade_w
from	agenda
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	cd_agenda		= cd_agenda_p
and	cd_especialidade is not null;
exception
	when others then
	cd_especialidade_w	:= null;
end;	

if	(cd_especialidade_w is null) then
	ds_retorno_w	:= obter_especialidades_medico(cd_pessoa_fisica_p);
else	
	ds_retorno_w	:= obter_desc_espec_medica(cd_especialidade_w);
end if;

return ds_retorno_w;

end Obter_Espec_Medico_Agenda;
/
