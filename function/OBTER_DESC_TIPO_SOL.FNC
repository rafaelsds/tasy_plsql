create or replace
function obter_Desc_tipo_Sol(ie_tipo_sol_p	varchar2) return varchar2 is

ds_retorno_w	varchar2(255);

begin

if	(ie_tipo_sol_p = '1') then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308142); -- Solu��o intermitente
elsif	(ie_tipo_sol_p = '2') then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308143); -- Solu��o em esquema alternado
elsif	(ie_tipo_sol_p = '3') then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308144); -- Solu��o cont�nua
elsif	(ie_tipo_sol_p = '4') then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308146); -- Solu��o especial
elsif	(ie_tipo_sol_p = '5') then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(308147); -- Solu��o para analgesia controlada pelo paciente (PCA)
end if;
	
return ds_retorno_w;

end obter_Desc_tipo_Sol;
/
