create or replace
function Lab_obter_se_exame_massas (nr_seq_exame_p	number)
 		    	return varchar2 is

ie_exame_massas_w varchar2(1);

begin

ie_exame_massas_w:= 'N';

if (nr_seq_exame_p is not null) then

select  nvl(max('S'),'N')
into	ie_exame_massas_w
from  	lote_ent_exame_massa
where	nr_seq_exame = nr_seq_exame_p
and		nvl(ie_situacao,'A') = 'A';	

end if;

return	ie_exame_massas_w;

end Lab_obter_se_exame_massas;
/