create or replace 
FUNCTION CTB_Obter_Se_Conta_Contida(
			cd_conta_contabil_p	Varchar2,
			cd_conta_parametro_p	Varchar2)
			return varchar2 is

cd_conta_contabil_ww		Varchar2(2000);
ie_gerar_w			Varchar2(0001);

BEGIN

cd_conta_contabil_ww		:= ' ' || cd_conta_parametro_p || ' ';
cd_conta_contabil_ww		:= replace(cd_conta_contabil_ww, ',', ' , ');
cd_conta_contabil_ww		:= replace(cd_conta_contabil_ww, '(', ' ');
cd_conta_contabil_ww		:= replace(cd_conta_contabil_ww, ')', ' ');
cd_conta_contabil_ww		:= replace(cd_conta_contabil_ww, '  ', ' ');

select	nvl(max('S') , 'N')
into	ie_gerar_w
from	dual
where	cd_conta_contabil_ww like '% ' || cd_conta_contabil_p || ' %';

return	ie_gerar_w;

END CTB_Obter_Se_Conta_Contida;
/

