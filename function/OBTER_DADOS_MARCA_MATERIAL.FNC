create or replace function obter_dados_marca_material(		ds_marca_p		varchar2,
					cd_material_p		number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is

/*
S	Status da marca
SD	Status da marca e Data atualizacao
D	Data atualizacao
C	C�digo do status da marca
P	Padronizado
*/			
			
			
nr_seq_marca_w		number(10);
ds_status_w		varchar2(255) := '';
ds_status_data_w		varchar2(255) := '';
ds_retorno_w		varchar2(255) := '';
dt_atualizacao_w		date;
nr_seq_status_aval_w	number(10);
ie_padronizado_w	material_marca.ie_padronizado%type;
			
begin

select	nvl(max(a.nr_sequencia),0)
into	nr_seq_marca_w
from	marca b,
	material_marca a
where	a.cd_material		= cd_material_p
and	a.nr_sequencia		= b.nr_sequencia
and	((substr(upper(substr(decode(a.cd_referencia,'',b.ds_marca, b.ds_marca || '(' || a.cd_referencia || ')'),1,255)),1,30) = substr(upper(ds_marca_p),1,30)) or
	(substr(upper(b.ds_marca),1,30) = substr(upper(ds_marca_p),1,30)));
	
	
if	(nr_seq_marca_w > 0) then

	select	max(nr_seq_status_aval),
		substr(max(obter_desc_status_aval_marca(nr_seq_status_aval)),1,255),
		max(dt_atualizacao),
		max(nvl(ie_padronizado,'N'))
	into	nr_seq_status_aval_w,
		ds_status_w,
		dt_atualizacao_w,
		ie_padronizado_w
	from	material_marca
	where	nr_sequencia	= nr_seq_marca_w
	and	cd_material	= cd_material_p;
	
	if	(ds_status_w is not null) then
		ds_status_data_w	:= substr(ds_status_w || ' - ' || PKG_DATE_FORMATERS.TO_VARCHAR(dt_atualizacao_w, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.GET_NM_USUARIO),1,255);
	end if;	

end if;

if	(ie_opcao_p = 'S') then
	ds_retorno_w := ds_status_w;
elsif	(ie_opcao_p = 'D') then
	ds_retorno_w := PKG_DATE_FORMATERS.TO_VARCHAR(dt_atualizacao_w, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.GET_NM_USUARIO);
elsif	(ie_opcao_p = 'SD') then
	ds_retorno_w := ds_status_data_w;
elsif	(ie_opcao_p = 'C') then
	ds_retorno_w := nr_seq_status_aval_w;
elsif	(ie_opcao_p = 'P') then
	ds_retorno_w := ie_padronizado_w;		
end if;


return	ds_retorno_w;

end obter_dados_marca_material;
/
