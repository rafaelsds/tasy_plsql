create or replace function get_current_cp_progress	(
														nr_seq_pe_prescr_p	number
                                                    )	return varchar2 is

si_progress_w	pe_prescr_cp_progress.si_progress%type;

cursor c_progress is

		select		a.si_progress
		from		pe_prescr_cp_progress a
		where		a.nr_seq_prescr = nr_seq_pe_prescr_p
		and			a.dt_liberacao is not null
		and			a.ie_situacao = 'A'
		and			a.dt_suspensao is null
		order by    a.dt_liberacao;

begin

    for r_c_progress in c_progress loop
        si_progress_w := r_c_progress.si_progress;
    end loop;

    return	si_progress_w;

end get_current_cp_progress;
/