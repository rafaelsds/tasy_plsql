create or replace
function pls_obter_tp_grau_parent_tws(	nr_seq_contrato_p	pls_contrato.nr_sequencia%type,
					nr_seq_intercambio_p	pls_intercambio.nr_sequencia%type)
					return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Identificar a origem do cadastro de grau de parentesco que sera utilizada na inclusao de beneficiarios
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
TWS
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:

ds_retorno_w
- 'C' - cadastros no contrato
- 'P' - cadastros no produto
- 'R' - cadastros da OPS - Cadastro de Regras
- 'G' - cadastros dos Cadastros Gerais
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 


ds_retorno_w	varchar2(1)	:= 'G';
qt_registro_w	pls_integer;

begin

if (nr_seq_contrato_p is not null) then
	select 	count(1)
	into	qt_registro_w
	from 	pls_plano_parentesco
	where 	ie_situacao	= 'A'
	and 	ie_restringe_portal	= 'S'
	and	nr_seq_contrato	= nr_seq_contrato_p;

	if (qt_registro_w > 0) then
		ds_retorno_w	:= 'C';	
	else
		select 	count(1)
		into	qt_registro_w
		from 	pls_plano_parentesco 
		where 	ie_situacao	= 'A'
		and 	ie_restringe_portal	= 'S'
		and	nr_seq_plano in(	select 	a.nr_seq_plano 
						from	pls_contrato_plano a
						where 	a.nr_seq_contrato = nr_seq_contrato_p );
						
		if (qt_registro_w > 0) then
			ds_retorno_w	:= 'P';	
		else
			select 	count(1)
			into	qt_registro_w
			from 	pls_regra_parentesco
			where 	ie_situacao	= 'A'
			and 	ie_portal	= 'S';

			if (qt_registro_w > 0) then
				ds_retorno_w	:= 'R';
			end if;

		end if;
	end if;
else
	ds_retorno_w	:= 'G';
end if;
	
return	ds_retorno_w;

end pls_obter_tp_grau_parent_tws;
/