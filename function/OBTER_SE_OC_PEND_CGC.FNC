create or replace
function obter_se_oc_pend_cgc(	cd_cgc_fornecedor_p	varchar2,
				cd_estabelecimento_p	number,
				ie_tipo_parametro_p 	varchar2 default 'OP')
 		    	return number is

qt_reg_w	number(10) := 0;

begin
if(ie_tipo_parametro_p = 'OP') then
      select	count(*)
      into	qt_reg_w
      from	ordem_compra
      where	cd_cgc_fornecedor = cd_cgc_fornecedor_p
      and	cd_estabelecimento = cd_estabelecimento_p
      and	(ie_tipo_ordem <> 'G')
      and	(dt_baixa is null)
      and	(dt_aprovacao is null);
elsif (ie_tipo_parametro_p = 'OL') then
      select	count(*)
      into	qt_reg_w
      from	ordem_compra
      where	cd_cgc_fornecedor = cd_cgc_fornecedor_p
      and	cd_estabelecimento = cd_estabelecimento_p
      and	(dt_liberacao is not null);      
elsif (ie_tipo_parametro_p = 'OA') then
      select	count(*)
      into	qt_reg_w
      from	ordem_compra
      where	cd_cgc_fornecedor = cd_cgc_fornecedor_p
      and	cd_estabelecimento = cd_estabelecimento_p
      and	(dt_aprovacao is not null);     
elsif(ie_tipo_parametro_p = 'OE') then
      select	count(*)
      into	qt_reg_w
      from	ordem_compra
      where	cd_cgc_fornecedor = cd_cgc_fornecedor_p
      and	cd_estabelecimento = cd_estabelecimento_p
      and	(dt_baixa is null)
      and	(dt_entrega is null);
end if;

return	qt_reg_w;

end obter_se_oc_pend_cgc;
/
