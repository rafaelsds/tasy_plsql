create or replace
function obter_qt_estoque_consig_locais(cd_material_p		number,
					cd_fornecedor_p		varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2)
 		    	return number is

qt_estoque_w	fornecedor_mat_consignado.qt_estoque%type;			
qt_existe_locais_ignorados_w	number(5);
begin

select	count(*)
into	qt_existe_locais_ignorados_w
from	w_loc_est_gestao_consig
where 	nm_usuario = nm_usuario_p;

if (qt_existe_locais_ignorados_w > 0) then
	
	select	sum(a.qt_estoque)
	into	qt_estoque_w
	from 	fornecedor_mat_consignado a
	where	a.cd_material = cd_material_p
	and	a.cd_fornecedor = cd_fornecedor_p
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.dt_mesano_referencia = trunc(sysdate,'MM')	
	and 	not exists ( select 1
			     from w_loc_est_gestao_consig b
			     where a.cd_local_estoque = b.cd_local_estoque
			     and   b.nm_usuario = nm_usuario_p);	
			     
elsif (qt_existe_locais_ignorados_w = 0) then
	select	sum(a.qt_estoque)
	into	qt_estoque_w
	from 	fornecedor_mat_consignado a
	where	a.cd_material = cd_material_p
	and	a.cd_fornecedor = cd_fornecedor_p
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.dt_mesano_referencia = trunc(sysdate,'MM');
end if;	

qt_estoque_w := nvl(qt_estoque_w,0);

return	qt_estoque_w;

end obter_qt_estoque_consig_locais;
/