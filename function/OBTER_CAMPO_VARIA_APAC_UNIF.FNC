create or replace
function obter_campo_varia_apac_unif(	nr_sequencia_p		number,
				ie_exc_dt_sem_cid_p	varchar2 default 'N')
				return varchar2 is

ie_tipo_laudo_apac_w		varchar2(3);
ds_varia_w			varchar2(141);
cd_forma_organizacao_w		number(6,0);
dt_inicio_validade_w		sus_apac_unif.dt_inicio_validade%type;
dt_fim_validade_w		sus_apac_unif.dt_fim_validade%type;
dt_emissao_w			sus_apac_unif.dt_emissao%type;
ie_tipo_apac_w			sus_apac_unif.ie_tipo_apac%type;
ie_ajusta_radio_w		varchar2(1) := 'N';

begin

begin
select	a.ie_tipo_laudo_apac,
	c.cd_forma_organizacao,
	b.dt_inicio_validade,
	b.dt_fim_validade,
	b.dt_emissao,
	b.ie_tipo_apac
into	ie_tipo_laudo_apac_w,
	cd_forma_organizacao_w,
	dt_inicio_validade_w,
	dt_fim_validade_w,
	dt_emissao_w,
	ie_tipo_apac_w
from	sus_forma_organizacao c,
	sus_procedimento a,
	sus_apac_unif b
where	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	a.nr_seq_forma_org	= c.nr_sequencia
and	b.nr_sequencia		= nr_sequencia_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(174179);
	/*'Problemas ao obter as inforama�oes para o campo varia.'*/
end;

if	(trunc(dt_inicio_validade_w,'month') >= to_date('01/05/2019','dd/mm/yyyy')) then
	ie_ajusta_radio_w := 'S';
end if;

/*if	(ie_tipo_laudo_apac_w = '06') and
	(cd_forma_organizacao_w <> 60405) then*/
if	(ie_tipo_laudo_apac_w = '06') then
	/*MEDIC*/
	begin
	select	'3' ||
		nvl(rpad(qt_peso,3,' '),'   ') ||
		nvl(lpad(qt_altura_cm,3,0),'   ') ||
		nvl(ie_transplantado,'N') ||
		nvl(lpad(decode(nvl(ie_transplantado,'N'),'S',qt_transplante,0),2,0),'00') ||
		'  '||
		nvl(ie_gestante,'N')
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
		when others then
		ds_varia_w := null;
		end;
elsif	(ie_tipo_laudo_apac_w = '04') then
	/* RADIO */
	begin
	select	'R' ||
		nvl(rpad(cd_cid_topografia,4,' '),'    ') ||
		nvl(decode(ie_linfonodos_reg_inv,'O','3',ie_linfonodos_reg_inv),' ') ||
		nvl(decode(nvl(cd_estadio,99),99,' ',to_char(decode(obter_valor_dominio(2502,cd_estadio),'0',0,'I',1,'II',2,'III',3,'IV',4,4))),' ') ||
		nvl(lpad(substr(cd_grau_histopat,1,2),2,' '),'  ') ||
		nvl(to_char(dt_diag_cito_hist,'YYYYMMDD'),'        ') ||
		nvl(ie_tratamento_ant,' ') ||
		nvl(rpad(cd_cid_primeiro_trat,4,' '),'    ') ||
		decode(ie_exc_dt_sem_cid_p,'S',
			decode(nvl(cd_cid_primeiro_trat,'X'),'X','        ',nvl(to_char(dt_pri_tratamento,'YYYYMMDD'),'        ')),
				nvl(to_char(dt_pri_tratamento,'YYYYMMDD'),'        ')) ||
		nvl(rpad(cd_cid_segundo_trat,4,' '),'    ') ||
		decode(ie_exc_dt_sem_cid_p,'S',
			decode(nvl(cd_cid_segundo_trat,'X'),'X','        ',nvl(to_char(dt_seg_tratamento,'YYYYMMDD'),'        ')),
				nvl(to_char(dt_seg_tratamento,'YYYYMMDD'),'        ')) ||
		nvl(rpad(cd_cid_terceiro_trat,4,' '),'    ') ||
		decode(ie_exc_dt_sem_cid_p,'S',
			decode(nvl(cd_cid_terceiro_trat,'X'),'X','        ',nvl(to_char(dt_ter_tratamento,'YYYYMMDD'),'        ')),
				nvl(to_char(dt_ter_tratamento,'YYYYMMDD'),'        ')) ||
		nvl(ie_tratamento_ant,' ') ||
		nvl(to_char(dt_inicio_tratamento,'YYYYMMDD'),'        ') ||
		nvl(ie_finalidade,' ') ||
		nvl(rpad(cd_cid_pri_radiacao,4,' '),'    ') ||
		decode(ie_ajusta_radio_w,'S','    ',nvl(rpad(cd_cid_seg_radiacao,4,' '),'    ')) ||
		decode(ie_ajusta_radio_w,'S','    ',nvl(rpad(cd_cid_ter_radiacao,4,' '),'    ')) ||
		decode(ie_ajusta_radio_w,'S','   ',nvl(lpad(nr_campos_pri_radi,3,0),'   ')) ||
		nvl(to_char(dt_inicio_pri_radi,'YYYYMMDD'),'        ') ||
		decode(ie_ajusta_radio_w,'S','        ',nvl(to_char(dt_inicio_seg_radi,'YYYYMMDD'),'        ')) ||
		decode(ie_ajusta_radio_w,'S','        ',nvl(to_char(dt_inicio_ter_radi,'YYYYMMDD'),'        ')) ||
		nvl(to_char(dt_fim_pri_radi,'YYYYMMDD'),'        ') ||
		decode(ie_ajusta_radio_w,'S','        ',nvl(to_char(dt_fim_seg_radi,'YYYYMMDD'),'        ')) ||
		decode(ie_ajusta_radio_w,'S','        ',nvl(to_char(dt_fim_ter_radi,'YYYYMMDD'),'        ')) ||
		nvl(rpad(cd_cid_principal,4,' '),'    ') ||
		nvl(rpad(cd_cid_secundario,4,' '),'    ') ||
		decode(ie_ajusta_radio_w,'S','',nvl(lpad(nr_campos_seg_radi,3,0),'   ')) ||
		decode(ie_ajusta_radio_w,'S','',nvl(lpad(nr_campos_ter_radi,3,0),'   '))		
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
		when others then
			ds_varia_w := null;
	end;
elsif	(ie_tipo_laudo_apac_w = '03') then
	/* QUIMIO */
	begin
	select	'Q' ||
		nvl(rpad(cd_cid_topografia,4,' '),'    ') ||
		nvl(decode(ie_linfonodos_reg_inv,'O','3',ie_linfonodos_reg_inv),' ') ||
		nvl(decode(nvl(cd_estadio,99),99,' ',to_char(decode(obter_valor_dominio(2502,cd_estadio),'0',0,'I',1,'II',2,'III',3,'IV',4,4))),' ') ||
		nvl(lpad(substr(cd_grau_histopat,1,2),2,' '),'  ') ||
		nvl(to_char(dt_diag_cito_hist,'YYYYMMDD'),'        ') ||
		nvl(ie_tratamento_ant,' ') ||
		nvl(rpad(cd_cid_primeiro_trat,4,' '),'    ') ||
		decode(ie_exc_dt_sem_cid_p,'S',
			decode(nvl(cd_cid_primeiro_trat,'X'),'X','        ',nvl(to_char(dt_pri_tratamento,'YYYYMMDD'),'        ')),
				nvl(to_char(dt_pri_tratamento,'YYYYMMDD'),'        ')) ||
		nvl(rpad(cd_cid_segundo_trat,4,' '),'    ') ||
		decode(ie_exc_dt_sem_cid_p,'S',
			decode(nvl(cd_cid_segundo_trat,'X'),'X','        ',nvl(to_char(dt_seg_tratamento,'YYYYMMDD'),'        ')),
				nvl(to_char(dt_seg_tratamento,'YYYYMMDD'),'        ')) ||
		nvl(rpad(cd_cid_terceiro_trat,4,' '),'    ') ||
		decode(ie_exc_dt_sem_cid_p,'S',
			decode(nvl(cd_cid_terceiro_trat,'X'),'X','        ',nvl(to_char(dt_ter_tratamento,'YYYYMMDD'),'        ')),
				nvl(to_char(dt_ter_tratamento,'YYYYMMDD'),'        ')) ||
		nvl(ie_tratamento_ant,' ') ||
		nvl(to_char(dt_inicio_tratamento,'YYYYMMDD'),'        ') ||
		nvl(rpad(substr(ds_sigla_esquema,1,5),5,' '),'     ') ||
		nvl(lpad(qt_meses_planejados,3,0),'   ') ||
		nvl(lpad(qt_meses_autorizados,3,0),'   ') ||
		nvl(rpad(cd_cid_principal,4,' '),'    ') ||
		nvl(rpad(cd_cid_secundario,4,' '),'    ') ||
		nvl(rpad(substr(elimina_caracteres_especiais(replace(ds_sigla_esquema,'/','')),6,15),10,' '),'          ') --Alterado por lhalves na OS 860718 - troquei para rpad, pois podem existir esquemas de at� 15 d�gitos, entao tem que completar com o ds_sigla_esquema que � considerado na linha acima tamb�m.
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
		when others then
			ds_varia_w := null;
	end;
elsif	(ie_tipo_laudo_apac_w = '02') or
	((ie_tipo_laudo_apac_w = '10') and
	(trunc(dt_inicio_validade_w) < to_date('01/07/2014','dd/mm/yyyy')) and
	(trunc(dt_emissao_w) < to_date('01/07/2014','dd/mm/yyyy')) and
	(trunc(dt_fim_validade_w) < to_date('01/08/2014','dd/mm/yyyy')) and
	(ie_tipo_apac_w = 2)) then
	/* NEFROLOGIA */
	begin
	select	'N' ||
		nvl(to_char(dt_pri_dialise,'YYYYMMDD'),'        ') ||
		nvl(lpad(substr(qt_altura_cm,1,3),3,0),'   ') ||
		nvl(lpad(substr(qt_peso,1,3),3,0),'   ') ||
		nvl(lpad(substr(qt_diurese,1,4),4,0),'    ') ||
		nvl(lpad(substr(qt_glicose,1,4),4,0),'    ') ||
		nvl(ie_acesso_vascular,' ') ||
		nvl(ie_ultra_abdomen,' ') ||
		nvl(lpad(substr(nr_tru,1,4),4,0),'    ') ||
		nvl(lpad(substr(qt_interv_fistola,1,2),2,0),'  ') ||
		nvl(ie_inscrito_cncdo,'N') ||
		nvl(lpad(substr(pr_albumina,1,2),2,0),'  ') ||
		nvl(decode(ie_hcv,'S','P','N'),'N') ||
		nvl(decode(ie_hb_sangue,'S','P','N'),'N') ||
		nvl(decode(ie_hiv,'S','P','N'),'N') ||
		nvl(lpad(substr(pr_hb,1,2),2,0),'00') ||
		nvl(rpad(substr(cd_cid_principal,1,4),4,' '),'    ') ||
		nvl(rpad(substr(cd_cid_secundario,1,4),4,' '),'    ')
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
		when others then
			ds_varia_w := null;
	end; 
/*elsif	(ie_tipo_laudo_apac_w = '00') or
	(cd_forma_organizacao_w = 60405) then*/
elsif	((ie_tipo_laudo_apac_w = '00') or
	(ie_tipo_laudo_apac_w = '07')) then	
	begin
	select	'O' ||
		nvl(rpad(cd_cid_principal,4,' '),'    ') ||
		nvl(rpad(cd_cid_secundario,4,' '),'    ')
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
		when others then
			ds_varia_w := null;
	end;
elsif	(ie_tipo_laudo_apac_w = '10') and
	((trunc(dt_inicio_validade_w) >= to_date('01/07/2014','dd/mm/yyyy')) or
	(trunc(dt_emissao_w) >= to_date('01/07/2014','dd/mm/yyyy')) or
	(trunc(dt_fim_validade_w) >= to_date('01/08/2014','dd/mm/yyyy')) or
	(ie_tipo_apac_w = 1))then
	begin
	
	begin
	select	'T' ||
		nvl(rpad(substr(cd_cid_principal,1,4),4,' '),'    ') ||
		nvl(rpad(substr(cd_cid_secundario,1,4),4,' '),'    ') ||
		nvl(rpad(ie_caract_tratamento,1,' '),' ') ||
		nvl(to_char(dt_pri_dialise,'YYYYMMDD'),'        ') ||
		nvl(to_char(dt_inicio_dialise_cli,'YYYYMMDD'),'        ') ||
		nvl(rpad(ie_acesso_vasc_dial,1,' '),' ') ||
		nvl(rpad(ie_acomp_nefrol,1,' '),' ') ||
		nvl(rpad(ie_situacao_usu_ini,1,' '),' ') ||
		nvl(rpad(ie_situacao_trasp,1,' '),' ') ||
		nvl(rpad(ie_dados_apto,1,' '),' ') ||
		nvl(lpad(substr(pr_hb,1,4),4,0),'    ') ||
		nvl(lpad(substr(qt_fosforo,1,4),4,0),'    ') ||
		nvl(lpad(substr(qt_ktv_semanal,1,4),4,0),'    ') ||
		nvl(lpad(substr(nr_tru,1,4),4,0),'    ') ||
		nvl(lpad(substr(pr_albumina,1,4),4,0),'    ') ||
		nvl(lpad(substr(qt_pth,1,4),4,0),'    ') ||
		nvl(decode(ie_hiv,'S','P','N'),'N') ||
		nvl(decode(ie_hcv,'S','P','N'),'N') ||
		nvl(decode(ie_hb_sangue,'S','P','N'),'N') ||
		nvl(ie_inter_clinica,'I') ||
		nvl(ie_peritonite_diag,'I')
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
	when others then
		ds_varia_w := null;
	end;
		
	end;
elsif	(ie_tipo_laudo_apac_w = '11') then
	begin
	
	begin
	select	'M' ||
		nvl(rpad(cd_cid_principal,4,' '),'    ') ||
		nvl(rpad(cd_cid_secundario,4,' '),'    ') ||
		nvl(rpad(ie_caract_tratamento,1,' '),' ') ||
		nvl(to_char(dt_inicio_tratamento,'YYYYMMDD'),'        ') ||
		nvl(ie_encaminhado_fav,'N') ||
		nvl(ie_encam_imp_cateter,'N') ||
		nvl(lpad(qt_altura_cm,3,0),'   ') ||
		nvl(lpad(qt_peso,3,0),'   ') ||
		nvl(lpad(ie_situacao_vacina,1,' '),' ') ||
		nvl(lpad(ie_anti_hbs,1,' '),' ') ||
		nvl(ie_influenza,'I') ||
		nvl(ie_difteria_tetano,'I') ||
		nvl(ie_pneumococica,'I') ||
		nvl(lpad(pr_hb,4,0),'    ') ||
		nvl(lpad(qt_fosforo,4,0),'    ') ||
		nvl(lpad(pr_albumina,4,0),'    ') ||
		nvl(lpad(qt_pth,4,0),'    ') ||
		nvl(decode(ie_hiv,'S','P','N'),'N') ||
		nvl(decode(ie_hcv,'S','P','N'),'N') ||
		nvl(decode(ie_hb_sangue,'S','P','N'),'N') ||
		nvl(ie_ieca,'I') ||
		nvl(ie_bra,'I')
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
	when others then
		ds_varia_w := null;
	end;
		
	end;
elsif	(ie_tipo_laudo_apac_w = '12') then
	begin
	
	begin
	select	'F' ||
		nvl(rpad(cd_cid_principal,4,' '),'    ') ||
		nvl(rpad(cd_cid_secundario,4,' '),'    ') ||
		nvl(ie_duplex_previo,'N') ||
		nvl(ie_cateter_outros,'N') ||
		nvl(ie_fav_previas,'N') ||
		nvl(ie_flebites,'N') ||
		nvl(ie_hematomas,'N') ||
		nvl(ie_veia_visivel,'N') ||
		nvl(ie_presenca_pulso,'N') ||
		nvl(lpad(qt_diametro_veia,4,0),'    ') ||
		nvl(lpad(qt_diametro_arteria,4,0),'    ') ||
		nvl(lpad(ie_fremito_traj_fav,1,' '),' ') ||
		nvl(ie_pulso_fremito,'N')
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
	when others then
		ds_varia_w := null;
	end;
		
	end;
elsif	(ie_tipo_laudo_apac_w = '05') then
	begin
	
	begin
	select	'B' ||
		nvl(rpad(qt_icm_atual_pac,3,' '),'   ') ||
		nvl(rpad(pr_exces_peso_perd,3,' '),'   ') ||
		nvl(rpad(qt_kilogr_perdido,3,' '),'   ') ||
		nvl(ie_gastrec_desv_duode,'N') ||
		nvl(ie_gastrec_vert_manga,'N') ||
		nvl(ie_gastrec_deri_intes,'N') ||
		nvl(ie_gastrec_vert_banda,'N') ||
		nvl(to_char(dt_cirur_bariatrica,'YYYYMMDD'),'        ') ||
		nvl(lpad(nr_aih_bariatrica,13,'0'),'0000000000000') ||
		' '||
		nvl(ie_comorbidades,'N') ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_i10,'N')) ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_o243,'N')) ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_e780,'N')) ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_m190,'N')) ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_g473,'N')) ||
		nvl(rpad(cd_cid_outro_comor,4,' '),'    ') ||
		nvl(ie_uso_medicamento,'N') ||
		nvl(ie_pratica_ativ_fisica,'N') ||
		nvl(ie_uso_polivitaminico,'N') ||
		nvl(ie_reganho_peso,'N') ||
		nvl(ie_ades_alim_saud,'N') ||
		nvl(ie_dermo_abdominal,'N') ||
		nvl(rpad(qt_dermo_abdominal,3,' '),'   ') ||
		nvl(ie_mamoplastia,'N') ||
		nvl(rpad(qt_mamoplastia,3,' '),'   ') ||
		nvl(ie_dermo_braquial,'N') ||
		nvl(rpad(qt_dermo_braquial,3,' '),'   ') ||
		nvl(ie_dermo_crural,'N') ||
		nvl(rpad(qt_dermo_crural,3,' '),'   ') ||
		nvl(ie_reconst_microcir,'N') ||
		nvl(rpad(qt_reconst_microcir,3,' '),'   ') ||
		'     '||
		nvl(rpad(qt_meses_acompanha,2,' '),'  ') ||
		nvl(rpad(qt_anos_acompanha,4,' '),'    ') ||
		nvl(rpad(cd_cid_principal,4,' '),'    ') ||
		nvl(rpad(cd_cid_secundario,4,' '),'    ')
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
	when others then
		ds_varia_w := null;
	end;
	
	end;
elsif	(ie_tipo_laudo_apac_w = '09') then
	begin
	
	begin
	select	'P' ||
		nvl(rpad(qt_icm_atual_pac,3,' '),'   ') ||
		nvl(to_char(dt_avaliacao_atual,'YYYYMMDD'),'        ') ||
		nvl(rpad(qt_peso,3,' '),'   ') ||
		nvl(rpad(qt_imc_pri_avaliacao,3,' '),'   ') ||
		nvl(to_char(dt_pri_avaliacao,'YYYYMMDD'),'        ') ||
		nvl(ie_aval_nutricionista,'N') ||
		nvl(ie_aval_med_psiquiatr,'N') ||
		nvl(ie_aval_cirur_geral,'N') ||
		nvl(ie_aval_psicologo,'N') ||
		nvl(ie_aval_endocrino,'N') ||
		nvl(ie_aval_med_clinico,'N') ||
		nvl(ie_aval_cirur_digesti,'N') ||
		nvl(ie_grupo_multiprofis,'N') ||
		nvl(ie_aval_risco_cirurg,'N') ||
		nvl(ie_real_exam_laborat,'N') ||
		nvl(ie_comorbidades,'N') ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_i10,'N')) ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_o243,'N')) ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_e780,'N')) ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_m190,'N')) ||
		Decode(nvl(ie_comorbidades,'N'),'N',' ',nvl(ie_cid_g473,'N')) ||
		nvl(rpad(cd_cid_outro_comor,4,' '),'    ') ||
		nvl(ie_uso_medicamento,'N') ||
		nvl(ie_uso_polivitaminico,'N') ||
		nvl(ie_perda_peso_pre_op,'N') ||
		nvl(ie_esofagogastroduo,'N') ||
		nvl(ie_ultr_abdomen_tot,'N') ||
		nvl(ie_ecocardio_transt,'N') ||
		nvl(ie_ultras_doppl_col,'N') ||
		nvl(ie_prova_pulmon_bro,'N') ||
		Decode(nvl(ie_apto_proc_cirurg,'N'),'P','A',nvl(ie_apto_proc_cirurg,'N')) ||
		nvl(rpad(cd_cid_principal,4,' '),'    ') ||
		nvl(rpad(cd_cid_secundario,4,' '),'    ')
	into	ds_varia_w
	from	sus_apac_unif
	where	nr_sequencia	= nr_sequencia_p;
	exception
	when others then
		ds_varia_w := null;
	end;
	
	end;
end if;

return ds_varia_w;

end obter_campo_varia_apac_unif;
/
