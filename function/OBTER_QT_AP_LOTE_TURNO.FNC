create or replace
function obter_qt_ap_lote_turno(	nr_seq_turno_p			number,
				cd_setor_atendimento_p		varchar2,
				cd_local_estoque_p 		number,
				nr_seq_classif_p			varchar2,
				ie_tipo_data_p			varchar2,
				dt_geracao_inicio_lote_p		date,
				dt_geracao_final_lote_p		date)
 		    	return number is

qt_lotes_w	number(10);
qt_lotes_agrup_w number(10);
ie_lote_alta_w	varchar2(1);
ie_somente_desdobrado_w	varchar2(1);
ie_agrupar_w 	varchar2(1);

begin

select	nvl(max(ie_agrupar_lote_copia),'N')
into	ie_agrupar_w
from	parametros_farmacia
where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

ie_lote_alta_w := substr(obter_valor_param_usuario(7029,17,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento),1,1);
ie_somente_desdobrado_w := substr(obter_valor_param_usuario(88,94,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento),1,1);

if	(nr_seq_classif_p is not null) and
	(nvl(nr_seq_classif_p, 'X') <> 'X') then
	select	count(distinct a.nr_sequencia) qt_lotes
	into	qt_lotes_w
	from   	ap_lote a,
		ap_lote_item b,
		atendimento_paciente c,
		regra_turno_disp d
	where  	a.nr_sequencia = b.nr_seq_lote
	and	d.nr_sequencia = a.nr_seq_turno
	and	d.ie_situacao = 'A'
	and	a.nr_sequencia = b.nr_seq_lote
	and	obter_atendimento_prescr(a.nr_prescricao) = c.nr_atendimento(+)
	and	a.nr_seq_turno = nr_seq_turno_p
	and	nvl(a.cd_local_estoque, cd_local_estoque_p) = cd_local_estoque_p
	and	b.qt_dispensar > 0
	and	a.ie_status_lote = 'G'
	and	(ie_lote_alta_w = 'S' or c.dt_alta is null)
	and 	decode(ie_tipo_data_p,'G',a.dt_geracao_lote,'I',a.dt_inicio_turno,'A',a.dt_atend_lote) between dt_geracao_inicio_lote_p and dt_geracao_final_lote_p
	and	((ie_somente_desdobrado_w = 'T') or ((ie_somente_desdobrado_w = 'S') and ( (a.nr_seq_lote_sup is not null or nvl(a.ie_reaprazado,'N') = 'S'))) 
	or 	((ie_somente_desdobrado_w = 'N') and ((a.nr_seq_lote_sup is null or nvl(a.ie_reaprazado,'N') = 'S'))))
	and	substr(obter_se_contido(a.cd_setor_atendimento, cd_setor_atendimento_p),1,1) = 'S'
	and	substr(obter_se_contido_char(a.nr_seq_classif, nr_seq_classif_p),1,1) = 'S'
	and 	(ie_agrupar_w = 'N' or (ie_agrupar_w = 'S' and a.nr_lote_agrupamento is null));
	
	select	count(distinct a.nr_sequencia) qt_lotes
	into	qt_lotes_agrup_w
	from   	ap_lote a,
		atendimento_paciente c,
		regra_turno_disp d
	where  	a.nr_atendimento = c.nr_atendimento(+)
	and	a.nr_seq_turno = d.nr_sequencia
	and	d.ie_situacao = 'A'
	and	a.nr_seq_turno = nr_seq_turno_p
	and	nvl(a.cd_local_estoque, cd_local_estoque_p) = cd_local_estoque_p
	and	a.ie_status_lote = 'G'
	and	(ie_lote_alta_w = 'S' or c.dt_alta is null)
	and 	decode(ie_tipo_data_p,'G',a.dt_geracao_lote,'I',a.dt_inicio_turno,'A',a.dt_atend_lote) between dt_geracao_inicio_lote_p and dt_geracao_final_lote_p
	and	((ie_somente_desdobrado_w = 'T') or ((ie_somente_desdobrado_w = 'S') and ( (a.nr_seq_lote_sup is not null or nvl(a.ie_reaprazado,'N') = 'S'))) 
	or 	((ie_somente_desdobrado_w = 'N') and ((a.nr_seq_lote_sup is null or nvl(a.ie_reaprazado,'N') = 'S'))))
	and	substr(obter_se_contido(a.cd_setor_atendimento, cd_setor_atendimento_p),1,1) = 'S'
	and	substr(obter_se_contido_char(a.nr_seq_classif, nr_seq_classif_p),1,1) = 'S'
	and 	a.ie_agrupamento = 'S';
else
	select	count(distinct a.nr_sequencia) qt_lotes
	into	qt_lotes_w
	from   	ap_lote a,
		ap_lote_item b,
		atendimento_paciente c,
		regra_turno_disp d
	where  	a.nr_sequencia = b.nr_seq_lote
	and	d.nr_sequencia = a.nr_seq_turno
	and	d.ie_situacao = 'A'
	and	a.nr_sequencia = b.nr_seq_lote
	and	obter_atendimento_prescr(a.nr_prescricao) = c.nr_atendimento(+)
	and	a.nr_seq_turno = nr_seq_turno_p
	and	nvl(a.cd_local_estoque, cd_local_estoque_p) = cd_local_estoque_p
	and	b.qt_dispensar > 0
	and	a.ie_status_lote = 'G'
	and	(ie_lote_alta_w = 'S' or c.dt_alta is null)
	and 	decode(ie_tipo_data_p,'G',a.dt_geracao_lote,'I',a.dt_inicio_turno,'A',a.dt_atend_lote) between dt_geracao_inicio_lote_p and dt_geracao_final_lote_p
	and	((ie_somente_desdobrado_w = 'T') or ((ie_somente_desdobrado_w = 'S') and ( (a.nr_seq_lote_sup is not null or nvl(a.ie_reaprazado,'N') = 'S'))) 
	or ((ie_somente_desdobrado_w = 'N') and ((a.nr_seq_lote_sup is null or nvl(a.ie_reaprazado,'N') = 'S'))))
	and	substr(obter_se_contido(a.cd_setor_atendimento, cd_setor_atendimento_p),1,1) = 'S'
	and 	(ie_agrupar_w = 'N' or (ie_agrupar_w = 'S' and a.nr_lote_agrupamento is null));
	
	select	count(distinct a.nr_sequencia) qt_lotes
	into	qt_lotes_agrup_w
	from   	ap_lote a,
		atendimento_paciente c,
		regra_turno_disp d
	where  	a.nr_atendimento = c.nr_atendimento(+)
	and 	d.nr_sequencia = a.nr_seq_turno
	and	d.ie_situacao = 'A'	
	and	a.nr_seq_turno = nr_seq_turno_p
	and	nvl(a.cd_local_estoque, cd_local_estoque_p) = cd_local_estoque_p
	and	a.ie_status_lote = 'G'
	and	(ie_lote_alta_w = 'S' or c.dt_alta is null)
	and 	decode(ie_tipo_data_p,'G',a.dt_geracao_lote,'I',a.dt_inicio_turno,'A',a.dt_atend_lote) between dt_geracao_inicio_lote_p and dt_geracao_final_lote_p
	and	((ie_somente_desdobrado_w = 'T') or ((ie_somente_desdobrado_w = 'S') and ( (a.nr_seq_lote_sup is not null or nvl(a.ie_reaprazado,'N') = 'S'))) 
	or 	((ie_somente_desdobrado_w = 'N') and ((a.nr_seq_lote_sup is null or nvl(a.ie_reaprazado,'N') = 'S'))))
	and	substr(obter_se_contido(a.cd_setor_atendimento, cd_setor_atendimento_p),1,1) = 'S'
	and 	a.ie_agrupamento = 'S';
	
	
end if;

return	(qt_lotes_w + qt_lotes_agrup_w);

end obter_qt_ap_lote_turno;
/
