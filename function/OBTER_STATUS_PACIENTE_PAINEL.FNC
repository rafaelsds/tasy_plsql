CREATE OR REPLACE
FUNCTION obter_status_paciente_painel (	DT_ANALISE_P		DATE,
										DT_CHEGADA_P		DATE,
										DT_PAC_AGUARDO_P	DATE,
										DT_ALTA_P			DATE,
										DT_PRE_INDUC_P		DATE,
										DT_SALA_REC_P		DATE,
										DT_CHEGADA_FIM_P	DATE,
										DT_SAIDA_CC_P		DATE,
										IE_STATUS_AGENDA	VARCHAR2)
				RETURN VARCHAR2 IS

ds_retorno_w	varchar2(2);

/*
SC	- Sa�da do paciente do centro cir�rgico
CF	- Paciente na sala
SR	- Paciente em recupera��o anest�sica
PI	- Paciente em pr� indu��o
AL	- Paciente em alta do centro cir�rgico
PA	- Paciente em aguardo
CH	- Paciente chamado
AN	- Em an�lise
CA  - Cancelada 
*/

BEGIN

if(IE_STATUS_AGENDA = 'C') then
	ds_retorno_w := 'CA';
elsif(DT_SAIDA_CC_P is not null) then
	ds_retorno_w := 'SC';
elsif(DT_ALTA_P is not null) then
	ds_retorno_w := 'AL';
elsif(DT_SALA_REC_P is not null) then
	ds_retorno_w := 'SR';
elsif(DT_CHEGADA_FIM_P is not null) then
	ds_retorno_w := 'CF';
elsif(DT_PRE_INDUC_P is not null) then
	ds_retorno_w := 'PI';
elsif(DT_PAC_AGUARDO_P is not null) then
	ds_retorno_w := 'PA';
elsif(DT_CHEGADA_P is not null) then
	ds_retorno_w := 'CH';
elsif(DT_ANALISE_P is not null) then
	ds_retorno_w := 'AN';
end if;

RETURN	ds_retorno_w;

END obter_status_paciente_painel;
/
