create or replace
function retorna_cron_etapa( nr_seq_ordem_p		number,
			     nm_usuario_p		varchar2)
 		    	return number is

nr_seq_atividade_w	number(10);
nr_seq_cron_etapa_w	number(10);

begin

	select	max(nr_sequencia)
	into	nr_seq_atividade_w
	from	man_ordem_serv_ativ
	where	nr_seq_ordem_serv = nr_seq_ordem_p
	and	nm_usuario_exec	= nm_usuario_p;

	if	(nr_seq_atividade_w is not null) then
		select	max(nr_seq_proj_cron_etapa)
		into	nr_seq_cron_etapa_w
		from	man_ordem_serv_ativ a
		where	nr_sequencia	= nr_seq_atividade_w;
	end if;

return	nr_seq_cron_etapa_w;

end retorna_cron_etapa;
/