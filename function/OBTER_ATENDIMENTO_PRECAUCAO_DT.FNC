create or replace
function obter_atendimento_precaucao_dt(nr_atendimento_p	number, 
					ie_opcao_p 		varchar2)
 		    			return date is

nr_seq_precaucao_w		number;
ds_precaucao_w			varchar2(100);
nr_seq_atend_precaucao_w	number;
dt_inicio_w			date;
dt_termino_w			date;
ds_motivo_isolamento_w		Varchar2(255);

begin

select	nvl(max(nr_Sequencia),0)
into	nr_seq_atend_precaucao_w
from	atendimento_precaucao
where	nr_atendimento = nr_atendimento_p
and	dt_liberacao is not null
and	dt_inativacao is null
and	nvl(dt_final_precaucao, sysdate + 1) > sysdate;

if 	(nr_seq_atend_precaucao_w > 0) then
	select  dt_inicio,
		dt_termino
	into	dt_inicio_w,
		dt_termino_w
	from	atendimento_precaucao
	where	nr_sequencia = nr_seq_atend_precaucao_w;
end if;

if	(upper(ie_opcao_p) = 'I') then
	return	dt_inicio_w;
elsif	(upper(ie_opcao_p) = 'T') then
	return 	dt_termino_w;
end if;

end obter_atendimento_precaucao_dt;
/