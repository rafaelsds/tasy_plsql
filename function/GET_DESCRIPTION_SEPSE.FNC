create or replace 
function get_description_sepse(ie_status_sepsis_p varchar2)
			return varchar2 is

vl_retorno_w	varchar2(255);

begin
	if	(ie_status_sepsis_p	is not null) then
		select  substr(obter_valor_dominio(7056,ie_status_sepsis_p),1,255)
		into vl_retorno_w
		from dual;
		
	end if;

return vl_retorno_w;
end get_description_sepse;
/