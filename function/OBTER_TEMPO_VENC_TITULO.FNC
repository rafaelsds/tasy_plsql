create or replace
function obter_tempo_venc_titulo(	nr_titulo_p	number)
					return VarChar2 is

ds_vencimento_w		varchar2(255);
dt_vencimento_atual_w	titulo_pagar.dt_vencimento_atual%type;
qt_dias_w		number(10);

begin

select	max(a.dt_vencimento_atual)
into	dt_vencimento_atual_w
from	titulo_pagar a
where	a.nr_titulo	= nr_titulo_p;

qt_dias_w	:= sysdate - dt_vencimento_atual_w;

if	(nvl(qt_dias_w,0)	<= 0) then

	ds_vencimento_w	:= WHEB_MENSAGEM_PCK.get_texto(817860) /*'A vencer'*/;

elsif	(qt_dias_w		<= 30) then

	ds_vencimento_w	:= WHEB_MENSAGEM_PCK.get_texto(819236); /*'Vencido de 1 a 30 dias'*/

elsif	(qt_dias_w		<= 60) then

	ds_vencimento_w	:= WHEB_MENSAGEM_PCK.get_texto(817861) /*'Vencido de 31 a 60 dias'*/;

elsif	(qt_dias_w		<= 90) then

	ds_vencimento_w	:= WHEB_MENSAGEM_PCK.get_texto(817862) /*'Vencido de 61 a 90 dias'*/;

elsif	(qt_dias_w		<= 120) then

	ds_vencimento_w	:= WHEB_MENSAGEM_PCK.get_texto(819237); /*'Vencido de 91 a 120 dias'*/

elsif	(qt_dias_w		> 120) then

	ds_vencimento_w	:= WHEB_MENSAGEM_PCK.get_texto(819238); /*'Vencido a mais de 120 dias'*/

end if;

return ds_vencimento_w;

end obter_tempo_venc_titulo;
/