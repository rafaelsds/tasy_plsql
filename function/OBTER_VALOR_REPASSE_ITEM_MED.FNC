Create or Replace
FUNCTION Obter_Valor_Repasse_Item_Med(
		nr_sequencia_p	Number,
		cd_medico_p		Number,
		ie_proc_mat_p		Varchar2)
		RETURN Number IS

vl_lib_proc_w		Number(15,2);
vl_lib_mat_w		Number(15,2);
vl_rep_proc_w		Number(15,2);
vl_rep_mat_w		Number(15,2);

vl_repasse_w		Number(15,2);

BEGIN

vl_repasse_w		:= 0;

if	(ie_proc_mat_p = 'P') then
	select	sum(vl_repasse)
	into	vl_rep_proc_w
	from 	procedimento_repasse
	where 	nr_seq_procedimento = nr_sequencia_p
	and	cd_medico = nvl(cd_medico_p,cd_medico);

	vl_repasse_w := vl_rep_proc_w;

elsif	(ie_proc_mat_p = 'PL') then
	select	sum(decode(ie_status,'L',vl_liberado,decode(ie_status,'S',vl_liberado,decode(ie_status,'R',vl_liberado,decode														(ie_status,'E',vl_repasse,0)))))
	into	vl_lib_proc_w
	from 	procedimento_repasse
	where 	nr_seq_procedimento = nr_sequencia_p
	and	nr_repasse_terceiro is not null
	and	cd_medico = nvl(cd_medico_p,cd_medico);

	vl_repasse_w := vl_lib_proc_w;

elsif	(ie_proc_mat_p = 'M') then
	select	sum(vl_repasse)
	into	vl_rep_mat_w
	from 	material_repasse
	where 	nr_seq_material	= nr_sequencia_p
	and	cd_medico = nvl(cd_medico_p,cd_medico);

	vl_repasse_w := vl_rep_mat_w;

elsif	(ie_proc_mat_p = 'ML') then
	select	sum(decode(ie_status,'L',vl_liberado,decode(ie_status,'S',vl_liberado,decode(ie_status,'R',vl_liberado,decode														(ie_status,'E',vl_repasse,0)))))
	into	vl_lib_mat_w
	from 	material_repasse
	where 	nr_seq_material	= nr_sequencia_p
	and	nr_repasse_terceiro is not null
	and	cd_medico = nvl(cd_medico_p,cd_medico);

	vl_repasse_w := vl_lib_mat_w;
end if;

RETURN nvl(vl_repasse_w,0);

END Obter_Valor_Repasse_Item_Med;
/
