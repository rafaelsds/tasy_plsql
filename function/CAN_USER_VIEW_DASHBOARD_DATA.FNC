CREATE OR REPLACE FUNCTION CAN_USER_VIEW_DASHBOARD_DATA(
    cd_estabelecimento_p NUMBER,
    nm_usuario_p         VARCHAR2,
    nr_sequencia_p       NUMBER)
  RETURN VARCHAR2
AS
  /*Return value*/
  ie_allowed_w VARCHAR2(1):= 'S';
  /*Checks if the user can view sensitive data*/
  ie_user_sensitive_w VARCHAR2(1):= 'N';
  /*Checks if the user can view personal data*/
  ie_user_patient_w VARCHAR2(1):= 'N';
  /*Checks if the user group can view sensitive data*/
  ie_user_grp_sensitive_w VARCHAR2(1):= 'N';
  /*Checks if the user group can view personal data*/
  ie_user_grp_patient_w VARCHAR2(1):= 'N';
  /* Checks if the dashboard containing sensitive data can be viewed by the user */
  ie_dashboard_is_sensitive_w VARCHAR2(1):= 'N';
  /* Checks if the dashboard containing personal data can be viewed by the user */
  ie_dashboard_is_patient_w VARCHAR2(1):= 'N';
BEGIN

	SELECT	NVL(IE_CAN_VIEW_SENSITIVE_INFO, 'N'),
			NVL(IE_CAN_VIEW_PATIENT_INFO, 'N')
	INTO	ie_user_sensitive_w,
			ie_user_patient_w
	FROM	USUARIO
	WHERE	NM_USUARIO = nm_usuario_p;

	SELECT	NVL(DECODE(CAN_USER_SEE_SENSITIVE_INFO('', 'S', cd_estabelecimento_p, nm_usuario_p), 'Y', 'Y', 'N', 'N'), 'N'),
			NVL(DECODE(CAN_USER_SEE_SENSITIVE_INFO('', 'P', cd_estabelecimento_p, nm_usuario_p), 'Y', 'Y', 'N', 'N'), 'N')
	INTO	ie_user_grp_sensitive_w,
			ie_user_grp_patient_w
	FROM	dual;

	SELECT	NVL(ie_info_sensivel, 'N'),
			NVL(ie_info_pessoal, 'N')
	INTO	ie_dashboard_is_sensitive_w,
			ie_dashboard_is_patient_w
	FROM	ind_dimensao
	WHERE	nr_sequencia = nr_sequencia_p;

	
	if (ie_dashboard_is_sensitive_w  = 'S') then
		if (ie_user_sensitive_w = 'Y' or ie_user_grp_sensitive_w = 'Y') then
			ie_allowed_w    := 'S';
		else
			ie_allowed_w    := 'N';
		end if;
	elsif (ie_dashboard_is_patient_w  = 'S') then
		if (ie_user_patient_w = 'Y' or ie_user_grp_patient_w = 'Y') then
			ie_allowed_w    := 'S';
		else
			ie_allowed_w    := 'N';
		end if;
	end if;
	
	RETURN ie_allowed_w;

END CAN_USER_VIEW_DASHBOARD_DATA;
/
