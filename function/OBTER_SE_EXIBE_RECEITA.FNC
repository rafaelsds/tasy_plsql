create or replace
function obter_se_exibe_receita(nr_seq_grupo_p		number,
				nr_seq_composicao_p	number,
				nr_sequencia_p		number)
 		    	return varchar2 is

ie_exibe_w 	varchar2(1);
ie_parametro_w	varchar2(1);
			
begin

obter_param_usuario(912, 18, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_parametro_w);

if	(ie_parametro_w = 'S') and (nr_sequencia_p is not null) then
	select  nvl(max('S'),'N')
	into	ie_exibe_w
	from    nut_receita 
	where   (nr_seq_grupo 		= nr_seq_grupo_p or nr_seq_grupo_p is null) 
	and	(nr_seq_composicao 	= nr_seq_composicao_p)
	and	nr_sequencia		= nr_sequencia_p;
else
	ie_exibe_w := 'S';
end if;
	
return	ie_exibe_w;

end obter_se_exibe_receita;
/