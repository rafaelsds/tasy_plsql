create or replace
function OBTER_DATA_ASSINA_DIG_LAUDO (	nr_seq_assinatura_p	number,
					dt_liberacao_p		date,
					dt_seg_aprovacao_p	date,
					dt_aprovacao_p		date) return date is

dt_assinatura_w		date;

begin

if	(nvl(nr_seq_assinatura_p,0) > 0) and
	((dt_liberacao_p is not null) or (dt_aprovacao_p is not null) or (dt_seg_aprovacao_p is not null)) then
	select	max(dt_assinatura)
	into	dt_assinatura_w
	from	TASY_ASSINATURA_DIGITAL a
	where	a.nr_sequencia	= nr_seq_assinatura_p;
end if;

return	dt_assinatura_w;

end OBTER_DATA_ASSINA_DIG_LAUDO;
/
