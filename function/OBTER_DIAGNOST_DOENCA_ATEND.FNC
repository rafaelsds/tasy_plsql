CREATE OR REPLACE
FUNCTION Obter_Diagnost_doenca_atend(		
					nr_atendimento_p		Number)
					RETURN Varchar2 IS

dt_diagnostico_w		date;
ds_diagnostico_w		Varchar2(240);

BEGIN

begin
select max(dt_diagnostico)
into dt_diagnostico_w
from diagnostico_medico
where	nr_atendimento	= nr_atendimento_p;
exception
	when no_data_found then
		dt_diagnostico_w := null;
end;

if	(dt_diagnostico_w is not null) then
	select nvl(max(ds_doenca_cid),null)
	into	ds_diagnostico_w
	from 	cid_doenca
	where	cd_doenca_cid = (	select max(cd_doenca)
					from	diagnostico_doenca
					where	nr_atendimento	= nr_atendimento_p
					  and	dt_diagnostico =	dt_diagnostico_w);
					  
		if	(ds_diagnostico_w is null) then
			select nvl(max(ds_doenca_cid),null)
			into	ds_diagnostico_w
			from 	cid_doenca
			where	cd_doenca_cid = (	select max(cd_doenca)
							from	diagnostico_doenca
							where	nr_atendimento	= nr_atendimento_p
							  and	dt_diagnostico = (	select	max(dt_diagnostico)
											from	diagnostico_doenca
											where	nr_atendimento	= nr_atendimento_p));			  
		end if;
end if;

RETURN ds_diagnostico_w;

END Obter_Diagnost_doenca_atend;
/
