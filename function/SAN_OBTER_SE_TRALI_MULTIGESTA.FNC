create or replace
function san_obter_se_trali_multigesta( nr_seq_derivado_p	number, 
					nr_seq_doacao_p		number)
 		    	return varchar2 is
			

ie_opcao_w		varchar2(1);
ie_lavado_w		varchar2(1);
ie_industrializado_w	varchar2(1);
ie_tipo_w		varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
ds_texto_w		varchar2(80);
ds_retorno_w		varchar2(255);
			
begin

if ( nr_seq_doacao_p is not null) then

	Select 	max(b.ie_opcao),
		max(b.ie_lavado),
		max(b.ie_industrializado),
		max(b.ds_texto)
	into	ie_opcao_w,
		ie_lavado_w,
		ie_industrializado_w,
		ds_texto_w
	from	san_regra_trali b
	where 	b.nr_seq_derivado 	= nr_seq_derivado_p;
		
	select 	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	san_doacao
	where	nr_sequencia = nr_seq_doacao_p;
	
	if (ie_opcao_w = 'M') then
		ie_tipo_w:= obter_se_doadora_multigesta(nr_seq_doacao_p);
	elsif (ie_opcao_w = 'T') then
		ie_tipo_w := san_obter_se_doador_trali(cd_pessoa_fisica_w);
	else
		ie_tipo_w:= obter_se_doadora_multigesta(nr_seq_doacao_p);
		if (ie_tipo_w <> 'S') then
			ie_tipo_w := san_obter_se_doador_trali(cd_pessoa_fisica_w);
		end if;	
	end if;
		
	
	if (ie_tipo_w = 'S') then -- Ent�o se for doa��o Trali ou Multigesta
		ds_retorno_w := ds_texto_w;
	end if;
end if;


return	ds_retorno_w;

end san_obter_se_trali_multigesta;
/

