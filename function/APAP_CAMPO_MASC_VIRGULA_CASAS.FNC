CREATE OR REPLACE
FUNCTION APAP_Campo_Masc_virgula_casas(
			vl_campo_p		number,
			qt_decimais_p		number)
	    	return varchar2 deterministic IS


ds_retorno_w		varchar2(30) := '';
vl_campo_w		varchar2(30);
vl_novo_campo_w		varchar2(30);
i			number(10)  := 0;
ie_decimais_w		varchar2(10);

begin

if	(qt_decimais_p = '1') then
	ie_decimais_w	:= '.9';
elsif	(qt_decimais_p = '2') then
	ie_decimais_w	:= '.99';
elsif	(qt_decimais_p = '3') then
	ie_decimais_w	:= '.999';
elsif	(qt_decimais_p = '4') then
	ie_decimais_w	:= '.9999';
elsif	(qt_decimais_p = '0') then
	ie_decimais_w	:= '';
end if;

vl_novo_campo_w	:= vl_campo_p;
if	(vl_campo_p is not null) and
	((trunc(vl_campo_p,qt_decimais_p)	<> vl_campo_p) or
	((vl_campo_p	< 1) and (vl_campo_p	> -1))) then
	begin
	vl_campo_w	:= to_char(vl_campo_p);

	select	replace(replace(replace(replace(to_char(vl_campo_p, '999,999,999,990' || ie_decimais_w), ',', 'X'),'.', ','), 'X', '.'),' ','')
	into	vl_campo_w
	from	dual;
	vl_novo_campo_w	:= null;
	i := 0;
	for	i in 1..length(vl_campo_w) loop
		if	(substr(vl_campo_w,i,1) <> ' ') then
			vl_novo_campo_w	:= vl_novo_campo_w || substr(vl_campo_w,i,1);
		end if;
	end loop;
	end;
end if;

ds_retorno_w	:= vl_novo_campo_w;

RETURN ds_retorno_w;

END APAP_Campo_Masc_virgula_casas;
/
