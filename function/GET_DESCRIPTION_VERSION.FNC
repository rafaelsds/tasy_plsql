create or replace
function get_description_version (nr_sequencia_p	number )
			return varchar2 is

ds_retorno_w		varchar2(255) := '';

begin

	if	(nr_sequencia_p is not null) then
		
		select  ds_version	
		into 	ds_retorno_w
		from    privacy_version
		where 	nr_sequencia = nr_sequencia_p;
		
	end if;

	return ds_retorno_w;

end get_description_version;
/