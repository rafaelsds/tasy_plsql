create or replace
function obter_dt_analise_gpt(	nr_atendimento_p	number,
				dt_inicial_p		date,
				dt_final_p		date,
				ie_tipo_usuario_p	varchar2,
				ie_registro_p		varchar2,
				ie_lib_farm_p           varchar2,
				cd_pessoa_fisica_p      pessoa_fisica.cd_pessoa_fisica%type,
				nr_seq_analise_gpt_p 			number default null,
				qt_horas_quimio_p				number default 0)
				return date is

/*
DTI : Data In�cio da an�lise
DTF: Data Fim an�lise
*/

dt_inicio_analise_w 	date;
dt_fim_analise_w		date;
dt_retorno_w			date;

begin

if(nr_seq_analise_gpt_p is not null) then 

	select 	max(dt_inicio_analise),
			max(dt_fim_analise)
	into	dt_inicio_analise_w,
			dt_fim_analise_w
	from 	gpt_hist_analise_plano
	where 	nr_sequencia = nr_seq_analise_gpt_p;
	
else

	select 	max(dt_inicio_analise),
			max(dt_fim_analise)
	into	dt_inicio_analise_w,
			dt_fim_analise_w
	from 	gpt_hist_analise_plano
	where 	nr_sequencia = obter_seq_analise_gpt(nr_atendimento_p, ie_tipo_usuario_p, ie_lib_farm_p, cd_pessoa_fisica_p, nvl(qt_horas_quimio_p,0));
	
end if;

if	(ie_registro_p = 'DTI') then
	dt_retorno_w := dt_inicio_analise_w;
elsif	(ie_registro_p = 'DTF') then
	dt_retorno_w := dt_fim_analise_w;
end if;

return dt_retorno_w;

end obter_dt_analise_gpt;
/
