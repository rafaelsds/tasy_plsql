create or replace
function obter_se_item_req_barras(nr_requisicao_p		number,
				  nr_item_p			number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);
nr_seq_cor_w	number(10);
			
begin

select	nvl(a.nr_seq_cor_exec,0)
into	nr_seq_cor_w
from	item_requisicao_material a
where	a.nr_requisicao = nr_requisicao_p
and	a.nr_sequencia = nr_item_p;

if	(nr_seq_cor_w = 103) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end obter_se_item_req_barras;
/