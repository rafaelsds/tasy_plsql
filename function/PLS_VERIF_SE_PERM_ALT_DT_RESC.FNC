create or replace
function pls_verif_se_perm_alt_dt_resc(	nr_seq_solicitacao_p	number)
 		    	return varchar2 is

ie_permite_w			varchar2(1);
qt_benef_solicitacao_w		number(10);
qt_benef_permite_alt_venc_w	number(10);
	
begin

select	count(1)
into	qt_benef_solicitacao_w
from	pls_solic_rescisao_benef
where	nr_seq_solicitacao = nr_seq_solicitacao_p;

select	count(1)
into	qt_benef_permite_alt_venc_w
from	pls_solic_rescisao_benef a,
	pls_motivo_cancelamento b
where	b.nr_sequencia = a.nr_seq_motivo_rescisao
and	a.nr_seq_solicitacao = nr_seq_solicitacao_p
and	nvl(b.ie_permite_alt_data_solic, 'N') = 'S';

if	(qt_benef_solicitacao_w = qt_benef_permite_alt_venc_w) then
	ie_permite_w	:= 'S';
else
	ie_permite_w	:= 'N';
end if;

return	ie_permite_w;

end pls_verif_se_perm_alt_dt_resc;
/