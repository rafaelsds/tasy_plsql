create or replace 
function validate_storage_log(
          cd_establishment_p		number,
          cd_profile_p			    number,
          cd_department_p			number,
          cd_function_p			    number,
          nr_seq_fun_schematic_p	number,
          nr_seq_obj_schematic_p	number,
          nm_user_p				    varchar2)
          return log_settings_table pipelined is type cursor_ref is ref cursor;

settings_r	log_settings_row := log_settings_row(null,null,null,null);
found_rule_w varchar2(1) := 'N';
nr_seq_obj_schematic_w number(10,0);
nr_seq_fun_schematic_w number(10,0);

cursor c01 is
	select 	ie_storage_access_log,
			ie_storage_repeated_access_log,
			ie_storage_filter_log,
			nr_sequencia
	from	access_item
	where	cd_estabelecimento = cd_establishment_p
	and		cd_funcao = cd_function_p
	and		nr_seq_obj_schematic = nr_seq_obj_schematic_w
	and   	ie_situacao = 'A'
	and   	dt_effective < sysdate
	and		nr_seq_funcao_schematic = nr_seq_fun_schematic_w
	and		nvl(nm_usuario_regra,nvl(nm_user_p,'')) = nvl(nm_user_p,'')
	and		nvl(cd_perfil,nvl(cd_profile_p,0)) = nvl(cd_profile_p,0)
	and		nvl(cd_setor,nvl(cd_department_p,0)) = nvl(cd_department_p,0)  
	order by cd_perfil, cd_setor, nm_usuario_regra;
  
cursor c02 is
	select 	ie_storage_access_log,
			ie_storage_repeated_access_log,
			ie_storage_filter_log,
			nr_sequencia
	from	access_item
	where	cd_estabelecimento = cd_establishment_p
	and		cd_funcao = cd_function_p
	and		nr_seq_funcao_schematic = nr_seq_fun_schematic_w
	and		nr_seq_obj_schematic is null
	and   	ie_situacao = 'A'
	and   	dt_effective < sysdate
	and		nvl(nm_usuario_regra,nvl(nm_user_p,'')) = nvl(nm_user_p,'')
	and		nvl(cd_perfil,nvl(cd_profile_p,0)) = nvl(cd_profile_p,0)
	and		nvl(cd_setor,nvl(cd_department_p,0)) = nvl(cd_department_p,0)
	order by cd_perfil, cd_setor, nm_usuario_regra;

begin
if	(cd_establishment_p is not null) then
	begin	
	 if	(nr_seq_obj_schematic_p is not null) then
		begin
		nr_seq_obj_schematic_w := nr_seq_obj_schematic_p;
		nr_seq_fun_schematic_w := nr_seq_fun_schematic_p;
		
		if 	(nr_seq_fun_schematic_w is null) and
			(cd_function_p is not null) then
			begin
			select 	max(nr_sequencia)
			into	nr_seq_fun_schematic_w
			from	funcao_schematic
			where	cd_funcao = cd_function_p
			and		ie_situacao_funcao = 'A';
			end;
		end if;
		
		while
			(found_rule_w = 'N') and
			(nr_seq_obj_schematic_w is not null) loop
			open c01;
			loop
			fetch c01
			into	settings_r.ie_storage_access,
					settings_r.ie_storage_repeated_access,
					settings_r.ie_storage_filter,
					settings_r.nr_sequence_record;
			exit when c01%notfound;
				begin
				found_rule_w := 'S';
				pipe row(settings_r);
				end;
			end loop;
			close c01;
			if (found_rule_w = 'N') then
				begin
				begin
				nr_seq_obj_schematic_w := schematic_obter_parent_nav(nr_seq_obj_schematic_w);
				exception
				when no_data_found then
					nr_seq_obj_schematic_w := null;
				end;

				if (nr_seq_obj_schematic_w is null) then
					begin
					open c02;
					loop
					fetch c02
					into	settings_r.ie_storage_access,
							settings_r.ie_storage_repeated_access,
							settings_r.ie_storage_filter,
							settings_r.nr_sequence_record;
					exit when c02%notfound;
						begin			
						pipe row(settings_r);
						end;
					end loop;
					close c02;
					end;
				end if;
				end;
			end if;
		end loop;
		end;
	else
		begin
		if (nr_seq_fun_schematic_p is not null) then
			begin
			nr_seq_fun_schematic_w := nr_seq_fun_schematic_p;
			end;
		elsif (cd_function_p is not null) then
			begin
			select 	max(nr_sequencia)
			into	nr_seq_fun_schematic_w
			from	funcao_schematic
			where	cd_funcao = cd_function_p
			and		ie_situacao_funcao = 'A';
			end;
		end if;
		
		open c02;
		loop
		fetch c02
		into	settings_r.ie_storage_access,
				settings_r.ie_storage_repeated_access,
				settings_r.ie_storage_filter,
				settings_r.nr_sequence_record;
		exit when c02%notfound;
			begin
			pipe row(settings_r);
			end;
		end loop;
		close c02;
		end;
	end if;	
	end;
end if;
end validate_storage_log;
/