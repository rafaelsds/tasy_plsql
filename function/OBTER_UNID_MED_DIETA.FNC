CREATE OR REPLACE
FUNCTION Obter_Unid_Med_Dieta(
					cd_dieta_p	Number)
					RETURN VARCHAR IS

ds_retorno_w			Varchar2(100);

BEGIN

if	(cd_dieta_p	is not null) then

	select 	ds_unidade_medida
	into	ds_retorno_w
	from 	dieta
	where 	cd_dieta = cd_dieta_p;

end if;

RETURN ds_retorno_w;
END Obter_Unid_Med_Dieta;
/
