create or replace
function obter_proximo_evento_mult_pepo(
    nr_cirurgia_p       evento_cirurgia_paciente.nr_cirurgia%type,
    nr_seq_pepo_p       evento_cirurgia_paciente.nr_seq_pepo%type,
    nm_usuario_p        evento_cirurgia_paciente.nm_usuario%type,
    cd_perfil_ativo_p   number,
    ie_estrutura_pepo   varchar2 default 'N',
    ie_tipo_item_p      varchar2 default 'P'
) return number is
nr_seq_exec_max_registrado_w evento_cirurgia.nr_seq_execucao%type;
nr_seq_exec_min_disponivel_w evento_cirurgia.nr_seq_execucao%type;
nr_sequencia_proximo_evento_w evento_cirurgia.nr_sequencia%type;
nr_seq_pepo_w evento_cirurgia_paciente.nr_seq_pepo%type;
cd_estabelecimento_w number(10) := wheb_usuario_pck.get_cd_estabelecimento;
begin
    nr_sequencia_proximo_evento_w := obter_proximo_evento_pepo(nr_cirurgia_p, cd_perfil_ativo_p, ie_estrutura_pepo, ie_tipo_item_p, nr_seq_pepo_p);

    nr_seq_pepo_w := nr_seq_pepo_p;
    
    if(nr_cirurgia_p is not null) then
        nr_seq_pepo_w := 0;
    end if;
    
    begin
        select  nr_seq_execucao
        into    nr_seq_exec_min_disponivel_w
        from    evento_cirurgia
        where   nr_sequencia = nr_sequencia_proximo_evento_w;
    exception when others then
        nr_seq_exec_min_disponivel_w := 0;
    end;
    
    begin
        select  max(nr_seq_execucao)
        into    nr_seq_exec_max_registrado_w
        from    w_evento_cirurgia_paciente a,
                evento_cirurgia b
        where   a.nr_seq_evento = b.nr_sequencia
        and     ((nvl(a.nr_cirurgia, 0) = nvl(nr_cirurgia_p, 0) and ie_estrutura_pepo = 'N') or
                ((((nvl(a.nr_cirurgia, 0) = nvl(nr_cirurgia_p, 0) and a.nr_seq_pepo is null) or (a.nr_cirurgia is null and nvl(a.nr_seq_pepo, 0) = nvl(nr_seq_pepo_p, 0)))) and ie_estrutura_pepo = 'S'))
        and     a.nm_usuario = nm_usuario_p;
    exception when others then
        nr_seq_exec_max_registrado_w := 0;
    end;
    
    if(nr_seq_exec_max_registrado_w is not null and nr_seq_exec_max_registrado_w >= nvl(nr_seq_exec_min_disponivel_w, 0)) then
        select  min(nr_seq_execucao)
        into    nr_seq_exec_min_disponivel_w
        from    evento_cirurgia
        where   (substr(obter_se_mostra_evento(nr_cirurgia_p, nr_seq_pepo_w, nr_sequencia, cd_perfil_ativo_p),1,1) = 'S')
        and     ie_situacao = 'A'
        and     ((cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento is null))
        and     nr_seq_execucao > nvl(nr_seq_exec_max_registrado_w, 0)
        and     ((nvl(ie_tipo_item, ie_tipo_item_p) = ie_tipo_item_p) or (ie_tipo_item = 'A'));
    end if;
    
    if (nr_seq_exec_min_disponivel_w is not null) then
        select  max(a.nr_sequencia)
        into    nr_sequencia_proximo_evento_w
        from    evento_cirurgia a
        where   (substr(obter_se_mostra_evento(nr_cirurgia_p, nr_seq_pepo_w, nr_sequencia, cd_perfil_ativo_p),1,1) = 'S')
        and     a.nr_seq_execucao = nr_seq_exec_min_disponivel_w
        and     ((a.cd_estabelecimento = cd_estabelecimento_w) or (a.cd_estabelecimento is null))
        and     a.ie_situacao = 'A'
        and     ((nvl(ie_tipo_item, ie_tipo_item_p) = ie_tipo_item_p) or (ie_tipo_item = 'A'));
    end if;
    
    return nvl(nr_sequencia_proximo_evento_w,0);
end obter_proximo_evento_mult_pepo;
/