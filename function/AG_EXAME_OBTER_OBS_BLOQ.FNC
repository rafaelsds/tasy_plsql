create or replace
function Ag_exame_obter_Obs_Bloq(	cd_agenda_p		number,
					dt_agenda_p		date)
					return varchar2 is 

					
ds_observacao_w	varchar2(255) := '';
ds_retorno_w	varchar2(255);

Cursor C01 is
	SELECT 	a.ds_observacao	
	FROM 	(	SELECT	b.nr_sequencia,
				b.ds_observacao
			FROM   	agenda_bloqueio b,
				agenda_paciente a
			WHERE  	b.cd_agenda    = cd_agenda_p
			AND	a.cd_agenda    = b.cd_agenda
			AND   	dt_agenda_p BETWEEN b.dt_inicial AND b.dt_final
			and    ((nvl(ie_dia_semana, pkg_date_utils.get_WeekDay(dt_agenda_p)) = pkg_date_utils.get_WeekDay(dt_agenda_p)) or ((ie_dia_semana = 9) and ie_dia_semana not in (7,1)) or (ie_dia_semana is null))
			ORDER BY	b.nr_sequencia DESC) a
	WHERE
	ROWNUM <= 1;

begin

open C01;
loop
fetch C01 into	
	ds_observacao_w;
exit when C01%notfound;
	begin
	ds_retorno_w := ds_observacao_w;
	end;
end loop;
close C01;


return ds_retorno_w;

end Ag_exame_obter_Obs_Bloq;
/