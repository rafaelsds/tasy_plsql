create or replace
function obter_saldo_lote_fornec_local(
	nr_seq_lote_fornec_p	number,
	cd_local_estoque_p	number,
	ie_conversao_consumo_p	varchar2 default 'N')

return number is

qt_lote_w			number(13,4);
qt_movimento_w		number(13,4);
qt_saldo_w		number(13,4) := 0;
qt_saldo_ww		number(13,4) := 0;
qt_movimento_nf_w		number(13,4);
nr_sequencia_nf_w		number(10);
cd_estabelecimento_w	number(4);
dt_mesano_referencia_w	date;
cd_material_w		number(6);
cd_material_estoque_w	number(6);
ie_estoque_lote_w		varchar2(1);
cd_local_estoque_w	number(4);

begin
if	(nvl(nr_seq_lote_fornec_p,0) > 0) then
	begin
	if	(nvl(cd_local_estoque_p,0) > 0) then
		cd_local_estoque_w := cd_local_estoque_p;
	end if;
	
	select	qt_material,
		nr_sequencia_nf,
		cd_material,
		cd_estabelecimento
	into	qt_lote_w,
		nr_sequencia_nf_w,
		cd_material_w,
		cd_estabelecimento_w
	from	material_lote_fornec
	where	nr_sequencia = nr_seq_lote_fornec_p;
	
	select	nvl(max(ie_estoque_lote),'N')
	into	ie_estoque_lote_w
	from	material_estab
	where	cd_material = cd_material_w
	and	cd_estabelecimento = cd_estabelecimento_w;
	
	select	cd_material_estoque
	into	cd_material_estoque_w
	from	material
	where	cd_material = cd_material_w;
	
	if	(ie_estoque_lote_w = 'N') then
		begin
		select	nvl(sum(decode( a.cd_acao,
				1,	decode(	b.ie_entrada_saida, 'E', a.qt_estoque, a.qt_estoque * -1),
					decode(	b.ie_entrada_saida, 'E', a.qt_estoque * -1, a.qt_estoque))),0)
		into	qt_movimento_w
		from	movimento_estoque a,
			operacao_estoque b
		where	a.cd_operacao_estoque = b.cd_operacao_estoque
		and	a.cd_estabelecimento = cd_estabelecimento_w
		and	a.cd_material_estoque = cd_material_estoque_w
		and	a.dt_processo is not null
		and	a.nr_seq_lote_fornec = nr_seq_lote_fornec_p;

		if	(nr_sequencia_nf_w is null) then
			qt_saldo_w := nvl(qt_lote_w,0) + nvl(qt_movimento_w,0);
		else
			begin
			select	nvl(sum(decode( a.cd_acao,
				1,	decode(	b.ie_entrada_saida, 'E', a.qt_estoque, a.qt_estoque * -1),
					decode(	b.ie_entrada_saida, 'E', a.qt_estoque * -1, a.qt_estoque))),0)
			into	qt_movimento_nf_w
			from	movimento_estoque a,
				operacao_estoque b
			where	a.cd_operacao_estoque = b.cd_operacao_estoque
			and	a.cd_estabelecimento = cd_estabelecimento_w
			and	a.cd_material_estoque = cd_material_estoque_w
			and	a.dt_processo is not null
			and	a.ie_origem_documento = 1
			and	a.nr_seq_tab_orig = nr_sequencia_nf_w
			and	a.nr_seq_lote_fornec = nr_seq_lote_fornec_p;
			
			qt_movimento_w	:= qt_movimento_w - qt_movimento_nf_w;
			qt_saldo_w := nvl(qt_lote_w,0) + nvl(qt_movimento_w,0);
			end;
		end if;
		end;
	else
		begin		
		dt_mesano_referencia_w	:= PKG_DATE_UTILS.start_of(sysdate, 'mm', 0);
		
		select	nvl(sum(qt_estoque),0)
		into	qt_saldo_w
		from	saldo_estoque_lote
		where	nr_seq_lote = nr_seq_lote_fornec_p
		and	cd_estabelecimento = cd_estabelecimento_w
		and	dt_mesano_referencia = dt_mesano_referencia_w
		and	cd_local_estoque = nvl(cd_local_estoque_w,cd_local_estoque);
		
		select	nvl(sum(qt_estoque),0)
		into	qt_saldo_ww
		from	fornecedor_mat_consig_lote
		where	nr_seq_lote = nr_seq_lote_fornec_p
		and	cd_estabelecimento = cd_estabelecimento_w
		and	dt_mesano_referencia = dt_mesano_referencia_w
		and	cd_local_estoque = nvl(cd_local_estoque_w,cd_local_estoque);
		
		qt_saldo_w := qt_saldo_w + qt_saldo_ww;
		end;
	end if;
	end;
end if;

if	(ie_conversao_consumo_p = 'S') then
	select	max(cd_material)
	into	cd_material_w
	from	material_lote_fornec
	where	nr_sequencia = nr_seq_lote_fornec_p;
	
	select	round(qt_saldo_w * qt_conv_estoque_consumo,4)
	into	qt_saldo_w
	from	material
	where	cd_material = cd_material_w;
end if;

return	qt_saldo_w;
end obter_saldo_lote_fornec_local; 
/
