create or replace
function pls_regra_matricula_valida_web
		(	nr_seq_congenere_p	Number,
			ie_origem_p		varchar2,
			ds_matricula_p		varchar2)
 		    	return Varchar2 is

ie_valido_w			Varchar2(1) := 'S';
ie_numero_igual_w		Varchar2(1);
ie_somente_numero_w		Varchar2(1);
ds_codigo_inicial_w		Varchar2(5);
qt_caracter_matricula_w		Number;
qt_caracter_igual_w		Number;
ie_regra_especifica_w		Number;
ds_regra_especifica_w		Varchar2(255);
i				Number;
ds_primeiro_caracter_w		Varchar2(1);
/*Vari�veis criadas para controles de loop, verifica��es, etc...*/
ie_auxiliar_w			Varchar2(1);
nr_auxiliar_w			Number := 0;

/*
T - Tasy
P - Portal
*/

cursor	C01 is
	select	ie_numero_igual,
		qt_caracter_matricula,
		ie_somente_numero,
		qt_caracter_igual,
		ds_codigo_inicial,
		ie_regra_especifica
	from	pls_regra_bloq_matricula
	where	nr_seq_congenere = nr_seq_congenere_p
	and	ie_situacao = 'A'
	and 	(nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento and pls_obter_se_controle_estab('RE') = 'S')
	union all
	select	ie_numero_igual,
		qt_caracter_matricula,
		ie_somente_numero,
		qt_caracter_igual,
		ds_codigo_inicial,
		ie_regra_especifica
	from	pls_regra_bloq_matricula
	where	nr_seq_congenere = nr_seq_congenere_p
	and	ie_situacao = 'A'
	and 	pls_obter_se_controle_estab('RE') = 'N';
	
begin

open C01;
loop
fetch C01 into	
	ie_numero_igual_w,
	qt_caracter_matricula_w,
	ie_somente_numero_w,
	qt_caracter_igual_w,
	ds_codigo_inicial_w,
	ie_regra_especifica_w;
exit when C01%notfound;
	begin
	
		-- CESP -  possui uma regra de vaida��o espec�fica
		if((ie_regra_especifica_w is not null) and (ie_regra_especifica_w = 1)) then
			ds_regra_especifica_w := pls_valida_matricula_cesp(ds_matricula_p);
			if(ds_regra_especifica_w <> 'MV') then
				ie_valido_w := 'N';
				exit;
			end if;
		end if;

		--Se diferente de 0, � a quantidade exata obrigat�ria de caracteres
		if	(nvl(qt_caracter_matricula_w, 0) > 0) then
			if	(qt_caracter_matricula_w <> length(ds_matricula_p)) then
				ie_valido_w :='N';
				exit;
			end if;
		end if;
		
		--Se sim, s� podem haver n�meros
		if	(nvl(ie_somente_numero_w,'N') = 'S') then
			select	nvl(max('S'),'N')
			into	ie_auxiliar_w
			from	dual
			where 	REGEXP_LIKE(ds_matricula_p, '^[[:digit:]]+$');
			if	(ie_auxiliar_w = 'N') then
				ie_valido_w := ie_auxiliar_w;
				exit;
			end if;
		end if;
		
		--C�digo inicial
		if	(ds_codigo_inicial_w is not null and ds_codigo_inicial_w <> substr(ds_matricula_p,1,length(ds_codigo_inicial_w))) then
			ie_valido_w := 'N';
			exit;
		end if;
		
		--Se sim, n�o podem ser todos os caracteres iguais
		if	(nvl(ie_numero_igual_w, 'N') = 'S') then
			i := 2;
			ds_primeiro_caracter_w := substr(ds_matricula_p,1,1);
			
			while	(i <= length(ds_matricula_p)) loop
				if(ds_primeiro_caracter_w <> substr(ds_matricula_p,i,1)) then
					exit;
				end if;
				if(i = length(ds_matricula_p)) then
					ie_valido_w := 'N';
					exit;
				end if;
				i := i + 1;
			end loop;
			
			if(ie_valido_w = 'N') then
				exit;
			end if;
		end if;
		
		--Se maior que zero, � a quantidade de caracteres iguais permitidos
		if	(nvl(qt_caracter_igual_w, 0) > 0) then
			i :=2;
			ds_primeiro_caracter_w := substr(ds_matricula_p,1,1);
			while(i <= length(ds_matricula_p)) loop	
				if	(ds_primeiro_caracter_w = substr(ds_matricula_p,i,1)) then
					nr_auxiliar_w := nr_auxiliar_w + 1;
					if(nr_auxiliar_w = qt_caracter_igual_w) then
						ie_valido_w := 'N';
						exit;
					end if;
				else
					ds_primeiro_caracter_w := substr(ds_matricula_p,i,1);
					nr_auxiliar_w := 0;
				end if;
				i := i + 1;
			end loop;
		end if;
		
	end;
end loop;
close C01;

return	ie_valido_w;

end pls_regra_matricula_valida_web;
/
