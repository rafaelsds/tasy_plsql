CREATE OR REPLACE
FUNCTION Obter_desc_dado_clinico	(nr_sequencia_p		number,
					ie_dado_clinico_p	varchar2)
					RETURN VARCHAR2 IS

ds_dado_retorno_w	VARCHAR2(80);

BEGIN

if	(nr_sequencia_p is not null) then
	if	(nvl(ie_dado_clinico_p, 'D') = 'D') then
		select	ds_dado_clinico
		into	ds_dado_retorno_w
		from	hem_dado_clinico
		where	nr_sequencia = nr_sequencia_p;
	elsif	(nvl(ie_dado_clinico_p, 'D') = 'M') then
		select	ds_medic
		into	ds_dado_retorno_w
		from	hem_medic
		where	nr_sequencia = nr_sequencia_p;
	elsif	(nvl(ie_dado_clinico_p, 'D') = 'E') then
		select	ds_exame
		into	ds_dado_retorno_w
		from	hem_exame
		where	nr_sequencia = nr_sequencia_p;
	end if;		
end if;

RETURN	ds_dado_retorno_w;

END Obter_desc_dado_clinico;
/