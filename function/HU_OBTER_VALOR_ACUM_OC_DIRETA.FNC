create or replace
function hu_obter_valor_acum_oc_direta(	cd_conta_contabil_p		varchar2,
					cd_estabelecimento_p		number,
					nr_seq_tipo_compra_p		number,
					nr_seq_mod_compra_p		number,
					nr_seq_proj_rec_p			number,
					dt_inicio_p			date,
					dt_final_p				date)
 		    	return number is

vl_unitario_material_w		number(17,4);
vl_acumulado_w			number(17,4) := 0;
vl_total_w			number(17,4) := 0;
qt_prevista_entrega_w		number(13,4);
nr_ordem_compra_w		number(10);
nr_item_oci_w			number(5);

cursor c01 is
select	distinct
	a.nr_ordem_compra,
	b.nr_item_oci
from	ordem_compra a,
	ordem_compra_item b
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.cd_estabelecimento = cd_estabelecimento_p
and	a.dt_aprovacao between dt_inicio_p and fim_dia(dt_final_p)
and	a.nr_seq_motivo_cancel is null
--and	a.nr_seq_licitacao is null 	Retirado essa restricao porque no relat�rio � para retirar por modalidade, entao busca todas as ordens daquela modalidade
and	b.dt_aprovacao is not null
and	b.dt_reprovacao is null
and	a.nr_seq_tipo_compra		= nr_seq_tipo_compra_p
and	a.nr_seq_mod_compra		= nr_seq_mod_compra_p
and	((cd_conta_contabil_p is null) or (b.cd_conta_contabil		= cd_conta_contabil_p))
and	((nr_seq_proj_rec_p is null) or (b.nr_seq_proj_rec		= nr_seq_proj_rec_p));

begin

open C01;
loop
fetch C01 into
	nr_ordem_compra_w,
	nr_item_oci_w;
exit when C01%notfound;
	begin

	select	nvl(vl_unitario_material,0)
	into	vl_unitario_material_w
	from	ordem_compra_item
	where	nr_ordem_compra = nr_ordem_compra_w
	and	nr_item_oci = nr_item_oci_w;

	select	nvl(sum(qt_prevista_entrega),0)
	into	qt_prevista_entrega_w
	from	ordem_compra_item_entrega
	where	nr_ordem_compra = nr_ordem_compra_w
	and	nr_item_oci = nr_item_oci_w
	and	dt_cancelamento is null;

	vl_total_w	:= (vl_unitario_material_w * qt_prevista_entrega_w);
	vl_acumulado_w	:= (vl_acumulado_w + vl_total_w);

	end;
end loop;
close C01;

return	vl_acumulado_w;

end hu_obter_valor_acum_oc_direta;
/