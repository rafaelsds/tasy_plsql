create or replace
function obter_tipo_cirur_proc(	nr_seq_proc_interno_p	number )
				return number is

cd_tipo_cirurgia_w	number(10,0);

begin

if	(nr_seq_proc_interno_p > 0) then
	select	cd_tipo_cirurgia
	into	cd_tipo_cirurgia_w
	from	proc_interno
	where	nr_sequencia	=	nr_seq_proc_interno_p;	
end if;

return	cd_tipo_cirurgia_w;

end obter_tipo_cirur_proc;
/