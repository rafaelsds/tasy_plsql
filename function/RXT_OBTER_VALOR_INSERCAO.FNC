create or replace
function RXT_Obter_Valor_Insercao (	nr_sequencia_p		Number,
					ie_opcao_p		Varchar2)
					return varchar2 is

/*
P - Protocolo
T - Tratamento
*/
					
ds_retorno_w	varchar2(255);

begin

if (ie_opcao_p = 'P') then

	select	max(decode(b.ds_tipo_resultado,'N',to_char(a.qt_resultado),a.ds_resultado))
	into	ds_retorno_w
	from	rxt_braq_campo_aplic_prot a,
		rxt_braq_campo b
	where	a.nr_seq_campo = b.nr_sequencia
	and	a.nr_sequencia = nr_sequencia_p;
	
elsif (ie_opcao_p = 'T') then

	select	max(decode(b.ds_tipo_resultado,'N',to_char(a.qt_resultado),a.ds_resultado))
	into	ds_retorno_w
	from	rxt_braq_campo_aplic_trat a,
		rxt_braq_campo b
	where	a.nr_seq_campo = b.nr_sequencia
	and	a.nr_sequencia = nr_sequencia_p;

end if;

return	ds_retorno_w;

end RXT_Obter_Valor_Insercao;
/