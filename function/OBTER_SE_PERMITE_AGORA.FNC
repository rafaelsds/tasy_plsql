create or replace
function obter_se_permite_agora( cd_material_p		number)
 		    	return varchar2 is

cd_grupo_material_w		number(5);
cd_subgrupo_material_w	number(5);
cd_classe_material_w	number(5);
ie_permite_agora_w		varchar2(10);

cursor c01 is
select	ie_permite_agora
from	rep_restricao_medicamento
where	nvl(cd_grupo_material,nvl(cd_grupo_material_w,0))		= nvl(cd_grupo_material_w,0)
and		nvl(cd_subgrupo_material,nvl(cd_subgrupo_material_w,0))	= nvl(cd_subgrupo_material_w,0)
and 	nvl(cd_classe_material,nvl(cd_classe_material_w,0))		= nvl(cd_classe_material_w,0)
and		nvl(cd_material,nvl(cd_material_p,0))					= nvl(cd_material_p,0)
order by
	nvl(cd_grupo_material,0), 
	nvl(cd_subgrupo_material,0), 
	nvl(cd_classe_material,0), 
	nvl(cd_material,0);

begin

select 	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
from 	estrutura_material_v
where	cd_material	= cd_material_p;

open C01;
loop
fetch C01 into	
	ie_permite_agora_w;
exit when C01%notfound;
	ie_permite_agora_w	:= ie_permite_agora_w;
end loop;
close C01;


return	ie_permite_agora_w;

end obter_se_permite_agora;
/