create or replace
function obter_qt_pend_ordem_compra(
			cd_estabelecimento_p	number,
			cd_material_p		number)
		return number is

qt_ordem_compra_w		number(17,4) := 0;
qt_dia_w				number(05,0) := 180;
cd_unid_med_compra_w		varchar2(30);
cd_unid_med_movto_w		varchar2(30);
qt_conversao_w			number(17,4) := 0;
ie_data_base_compra_pend_w	varchar2(80);

begin

select	nvl(max(qt_dia_compra_pend), 180),
	nvl(max(ie_data_base_compra_pend), 'DT_ATUALIZACAO')
into	qt_dia_w,
	ie_data_base_compra_pend_W
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,255) cd_unidade_medida_compra,
	qt_conv_compra_estoque
into	cd_unid_med_compra_w,
	qt_conversao_w
from	material
where	cd_material = cd_material_p;

/* Obter Ordens de compra pendentes  */
select	/*+ index(a Orcoite_materia_fk_i) */
	nvl(sum(a.qt_material - 
	nvl(a.qt_material_entregue,0) -
	nvl(obter_qt_entr_cancel_ordem(a.nr_ordem_compra, a.nr_item_oci),0)),0),
	max(cd_unidade_medida_compra)
into 	qt_ordem_compra_w,
	cd_unid_med_movto_w
from 	ordem_compra b,
	ordem_compra_item a
where 	a.nr_ordem_compra = b.nr_ordem_compra
and	a.qt_material > nvl(a.qt_material_entregue,0)
and	DECODE(ie_data_base_compra_pend_W,
		'DT_ATUALIZACAO', a.dt_atualizacao,
		'DT_ORDEM_COMPRA', b.dt_ordem_compra,
		'DT_ENTREGA', b.dt_entrega) > sysdate - qt_dia_w
and	a.cd_material 	in (	select cd_material
				from material
				where cd_material_estoque = cd_material_p)
and	exists(select	1
		from	ordem_compra b
		where	a.nr_ordem_compra = b.nr_ordem_compra
	  	and	b.dt_baixa is null
		and	b.cd_estabelecimento = cd_estabelecimento_p);
		
if	(cd_unid_med_movto_w = cd_unid_med_compra_w) then
	qt_ordem_compra_w := qt_ordem_compra_w * qt_conversao_w;
end if;

return qt_ordem_compra_w;

end obter_qt_pend_ordem_compra;
/