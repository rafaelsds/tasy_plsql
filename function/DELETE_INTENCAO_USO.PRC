create or replace procedure delete_intencao_uso(nr_seq_intencao_uso_p number) is

begin

  --REG_ANEXO_TESTE
  delete from REG_ANEXO_TESTE
   where nr_seq_caso_teste in
         (select nr_sequencia
            from reg_caso_teste
           where nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  --REG_ACAO_TESTE
  delete from REG_ACAO_TESTE
   where nr_seq_caso_teste in
         (select nr_sequencia
            from reg_caso_teste
           where nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  --reg_caso_teste
  delete from reg_caso_teste
   where nr_seq_intencao_uso = nr_seq_intencao_uso_p;

  delete from reg_permissao
   where nr_seq_intencao_uso = nr_seq_intencao_uso_p;

  delete from reg_customer_req_loc
   where nr_sequencia in
         (select loc.nr_sequencia
            from reg_customer_req_loc loc, reg_customer_requirement urs
           where loc.nr_seq_customer_req = urs.nr_sequencia
             and urs.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  delete from reg_escopo_customer_req
   where nr_sequencia in
         (select esc.nr_sequencia
            from reg_escopo_customer_req esc, reg_customer_requirement urs
           where esc.nr_customer_req = urs.nr_sequencia
             and urs.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  delete from reg_product_req_loc
   where nr_sequencia in
         (select loc.nr_sequencia
            from reg_product_req_loc loc, reg_product_requirement prs
           where loc.nr_seq_product_req = prs.nr_sequencia
             and prs.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  delete from reg_escopo_product_req
   where nr_sequencia in
         (select esc.nr_sequencia
            from reg_escopo_product_req esc, reg_product_requirement prs
           where esc.nr_seq_product_req = prs.nr_sequencia
             and prs.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  delete from reg_funcao_pr
   where nr_sequencia in
         (select fnc.nr_sequencia
            from reg_funcao_pr fnc, reg_product_requirement prs
           where fnc.nr_seq_product_req = prs.nr_sequencia
             and prs.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  delete from reg_product_requirement
   where nr_seq_intencao_uso = nr_seq_intencao_uso_p;

  delete from reg_product_requirement
   where nr_sequencia in
         (select prs.nr_sequencia
            from reg_product_requirement  prs,
                 reg_customer_requirement urs,
                 reg_features_customer    fea,
                 reg_area_customer        area
           where prs.nr_customer_requirement = urs.nr_sequencia
             and urs.nr_seq_features = fea.nr_sequencia
             and fea.nr_seq_area_customer = area.nr_sequencia
             and area.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  delete from reg_customer_requirement
   where nr_seq_intencao_uso = nr_seq_intencao_uso_p;

  delete from reg_customer_requirement
   where nr_sequencia in
         (select urs.nr_sequencia
            from reg_customer_requirement urs,
                 reg_features_customer    fea,
                 reg_area_customer        area
           where urs.nr_seq_features = fea.nr_sequencia
             and fea.nr_seq_area_customer = area.nr_sequencia
             and area.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  delete from reg_features_customer
   where nr_sequencia in
         (select fea.nr_sequencia
            from reg_features_customer fea, reg_area_customer area
           where fea.nr_seq_area_customer = area.nr_sequencia
             and area.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  delete from reg_area_customer
   where nr_seq_intencao_uso = nr_seq_intencao_uso_p;

  delete from reg_intencao_uso_hist
   where nr_seq_intencao_uso = nr_seq_intencao_uso_p;

  delete from reg_intencao_uso where nr_sequencia = nr_seq_intencao_uso_p;

  commit;

end delete_intencao_uso;
/