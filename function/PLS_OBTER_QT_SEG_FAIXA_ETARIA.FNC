create or replace
function pls_obter_qt_seg_faixa_etaria
			(	qt_idade_inicial_p	number,
				qt_idade_final_p	number,
				ie_tipo_data_p		varchar2,
				dt_inicial_p		date,
				dt_final_p		date,
				ie_situacao_p		varchar2,
				cd_estabelecimento_p	number)
				return number is

qt_beneficiarios_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
qt_idade_benef_w		number(10);
nr_seq_segurado_w		number(10);

Cursor C01 is
	select	a.nr_sequencia
	from	pls_segurado		a
	where	ie_tipo_segurado	in ('A','B','R')
	and	(pls_obter_dados_segurado(a.nr_sequencia,'CS')	= ie_situacao_p)
	and	((ie_tipo_data_p = 'C' and a.dt_contratacao between dt_inicial_p and fim_dia(dt_final_p)) or
		(ie_tipo_data_p	 = 'A' and a.dt_liberacao between dt_inicial_p and fim_dia(dt_final_p)) or
		(ie_tipo_data_p	 = 'N'))
	and	a.cd_estabelecimento	= cd_estabelecimento_p;
	
begin
qt_beneficiarios_w	:= 0;

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	
	qt_idade_benef_w	:= obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A');
	
	if	(qt_idade_benef_w between qt_idade_inicial_p and qt_idade_final_p) then
		qt_beneficiarios_w	:= qt_beneficiarios_w + 1;
	end if;	
	
	end;
end loop;
close C01;

return	qt_beneficiarios_w;

end pls_obter_qt_seg_faixa_etaria;
/