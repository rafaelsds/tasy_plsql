create or replace
function 	tx_obter_desc_reconstrucao (nr_seq_reconstrucao_p number)
	return varchar2 is

ds_reconstrucao_w varchar2(90);

begin

select  max(ds_reconstrucao)
into ds_reconstrucao_w
from  tx_reconstrucao
where nr_sequencia = nr_seq_reconstrucao_p;


return ds_reconstrucao_w;

end tx_obter_desc_reconstrucao;
/