create or replace
function proj_obter_se_etapa_os(	nr_seq_etapa_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(01);

begin

select	decode(count(*),0,'N','S')
into	ds_retorno_w
from	man_ordem_servico
where	nr_seq_proj_cron_etapa = nr_seq_etapa_p;

return	ds_retorno_w;

end proj_obter_se_etapa_os;
/