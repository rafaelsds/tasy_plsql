create or replace
function obter_dados_viagem(	nr_sequencia_p	number,
				ie_opcao_p		varchar2)
			     	return varchar2 is

/*
ie_opcao_p

'NP' - Nome da pessoa
'SQ' - Sequencia da viagem	
*/


ds_retorno_w	varchar2(255);
nm_pessoa_w	varchar2(60);
nr_sequencia_w	number(10,0);

begin

if	(nr_sequencia_p > 0) then
	select 	substr(obter_nome_pf(cd_pessoa_fisica),1,60),
		nr_sequencia
	into	nm_pessoa_w,
		nr_sequencia_w
	from 	via_viagem
	where 	nr_sequencia = nr_sequencia_p;
end if;

if	(ie_opcao_p	= 'NP') then
	ds_retorno_w	:= nm_pessoa_w;
elsif	(ie_opcao_p	= 'SQ') then
	ds_retorno_w	:= to_char(nr_sequencia_w);
end if;

return ds_retorno_w;

end obter_dados_viagem;
/