create or replace
function OBTER_SEQ_VALOR_301
			(nm_tabela_301_p	varchar2,
			nm_campo_chave_p	varchar2,
			ds_campo_p		varchar2)
			return varchar2 is

ds_sql_w	varchar2(255);		
vl_retorno_w	varchar2(255);
vl_resultado_w	varchar2(255);	
c001		integer;
			
begin

if	(nm_tabela_301_p is not null) and
	(nm_campo_chave_p is not null) then
	
	ds_sql_w := 'SELECT NR_SEQUENCIA FROM '||nm_tabela_301_p||' WHERE '||nm_campo_chave_p||' = '||chr(39)||ds_campo_p||chr(39);
	
	begin
		c001    := dbms_sql.open_cursor;
		dbms_sql.parse(c001,ds_sql_w,dbms_sql.native);
		dbms_sql.define_column(c001, 1, vl_retorno_w, 10);
		vl_resultado_w  := dbms_sql.execute(c001);
		vl_resultado_w  := dbms_sql.fetch_rows(c001);
		dbms_sql.column_value(c001,1,vl_retorno_w);
		dbms_sql.close_cursor(c001);
	exception
	when others then
		ds_sql_w := null;
	end;
	
end if;
	
return	vl_retorno_w;

end OBTER_SEQ_VALOR_301;
/