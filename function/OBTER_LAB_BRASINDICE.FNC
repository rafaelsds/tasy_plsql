CREATE OR REPLACE
FUNCTION Obter_Lab_Brasindice(
				CD_ESTABELECIMENTO_P		NUMBER,
				CD_MATERIAL_P			NUMBER,
				DT_VIGENCIA_P			DATE,
				IE_OPCAO_P			VARCHAR2)
				Return Varchar2 IS


cd_laboratorio_w		Varchar2(06);
dt_vigencia_w			Date;

ds_laboratorio_w		varchar2(40) := '';

Cursor C01 is 
	SELECT	CD_LABORATORIO
	FROM	MATERIAL_BRASINDICE
	WHERE	CD_MATERIAL						= CD_MATERIAL_P
	and	nvl(IE_SITUACAO, 'A')					= 'A'
	and	nvl(dt_vigencia,dt_vigencia_w)				<= dt_vigencia_w
	and	nvl(cd_estabelecimento, nvl(cd_estabelecimento_p, 0))	= nvl(cd_estabelecimento_p, 0)
	order by nvl(dt_vigencia,sysdate - 1000);

BEGIN

DT_VIGENCIA_W 	:= nvl(DT_VIGENCIA_P,sysdate);

OPEN C01;
LOOP
	FETCH C01 into	cd_laboratorio_w;
	EXIT when C01%notfound;
	cd_laboratorio_w	:= cd_laboratorio_w;
END LOOP;
CLOSE C01;


if	(ie_opcao_p = 'C') then
	Return cd_laboratorio_w;
else
	if	(cd_laboratorio_w is not null) then
		select nvl(max(ds_laboratorio), wheb_mensagem_pck.get_texto(799515,'CD_LABORATORIO=' || cd_laboratorio_w))
		into ds_laboratorio_w
		from brasindice_laboratorio
		where cd_laboratorio = cd_laboratorio_w;
	end if;

	Return ds_laboratorio_w;
end if;

END Obter_Lab_Brasindice;
/