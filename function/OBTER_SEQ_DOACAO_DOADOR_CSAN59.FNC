create or replace
function obter_seq_doacao_doador_csan59(	nr_seq_doacao_p		Varchar2,
						cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

qtd_doacao_w		varchar2(10);			
begin

select 	count(*)
into	qtd_doacao_w
from 	san_doacao 
where 	nr_sequencia <= nr_seq_doacao_p
and	((dt_coleta is not null) or (dt_coleta is null and ie_avaliacao_final = 'I'))
and	cd_pessoa_fisica = cd_pessoa_fisica_p;


return	qtd_doacao_w;

end obter_seq_doacao_doador_csan59;
/