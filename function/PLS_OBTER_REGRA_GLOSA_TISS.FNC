create or replace
function pls_obter_regra_glosa_tiss
			(	cd_motivo_glosa_p	varchar2,
				ie_tipo_retorno_p	varchar2,
				dt_conta_p		date,
				dt_item_p		date)
				return varchar2 is

/*	IE_TIPO_RETORNO_P
	D - Tipo data	*/

ds_retorno_w			varchar2(255);
ie_tipo_data_w			varchar2(10);
nr_seq_motivo_glosa_w		number(10);
qt_regra_w			number(10);
ie_ocorrencia_w			pls_controle_estab.ie_ocorrencia%type := 'N';
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

begin
begin
select	nvl(nr_sequencia,0)
into	nr_seq_motivo_glosa_w
from	tiss_motivo_glosa
where	cd_motivo_tiss	= cd_motivo_glosa_p
and	cd_convenio is null;
exception
when others then
	nr_seq_motivo_glosa_w	:= 0;
end;

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
ie_ocorrencia_w := pls_obter_se_controle_estab('GO');

if	(nr_seq_motivo_glosa_w > 0) then
	if	(ie_ocorrencia_w = 'N') then
		select	count(1)
		into	qt_regra_w
		from	pls_regra_glosa_tiss
		where	nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w
		and	rownum			= 1;
		
		if	(qt_regra_w > 0) then
			select	max(ie_tipo_data)
			into	ie_tipo_data_w
			from	pls_regra_glosa_tiss
			where	nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w;
			
			if	(ie_tipo_retorno_p = 'D') then
				if	(ie_tipo_data_w	= 'D') then
					ds_retorno_w	:= dt_conta_p;
				elsif	(ie_tipo_data_w	= 'I') then
					ds_retorno_w	:= dt_item_p;
				end if;
			end if;	
		end if;
	else
		select	count(1)
		into	qt_regra_w
		from	pls_regra_glosa_tiss
		where	nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w
		and	cd_estabelecimento	= cd_estabelecimento_w
		and	rownum			= 1;
		
		if	(qt_regra_w > 0) then
			select	max(ie_tipo_data)
			into	ie_tipo_data_w
			from	pls_regra_glosa_tiss
			where	nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w
			and	cd_estabelecimento	= cd_estabelecimento_w;
			
			if	(ie_tipo_retorno_p = 'D') then
				if	(ie_tipo_data_w	= 'D') then
					ds_retorno_w	:= dt_conta_p;
				elsif	(ie_tipo_data_w	= 'I') then
					ds_retorno_w	:= dt_item_p;
				end if;
			end if;	
		end if;
	end if;
end if;

return ds_retorno_w;

end pls_obter_regra_glosa_tiss;
/