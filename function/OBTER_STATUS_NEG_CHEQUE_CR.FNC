create or replace
function obter_status_neg_cheque_cr(nr_seq_cheque_p	number)
			return number is

/*	0 - Fora de negocia��o
	1 - A negociar
	2 - negociado
*/

ie_status_w		number(3);
vl_saldo_negociado_w	number(15,2);
ie_retorno_w		number(3)	:= null;
vl_cheque_w		number(15,2);

begin

if	(nr_seq_cheque_p is not null) then

	select	obter_status_cheque(nr_seq_cheque),
		vl_saldo_negociado,
		vl_cheque
	into	ie_status_w,
		vl_saldo_negociado_w,
		vl_cheque_w
	from	cheque_cr
	where	nr_seq_cheque	= nr_seq_cheque_p;

	/* Se est� em nesses status (Posse do hospital), o cheque pode ser negociado */
	if	(ie_status_w in (1,3,5,10)) then

		/* Se o saldo de negocia��o � maior que zero � porque parte do cheque j� foi negociado */
		if	(vl_saldo_negociado_w = 0) and
			(vl_cheque_w <> 0) then
			ie_retorno_w	:= 2;
		else
			ie_retorno_w	:= 1;
		end if;
	else
		ie_retorno_w	:= 0;	
	end if;

end if;

return	ie_retorno_w;

end;
/