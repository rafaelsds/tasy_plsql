create or replace
function obter_inf_saude_mulher(nr_atendimento_p number,
								ie_tipo_info_p	varchar2,
								cd_pessoa_fisica_p	varchar2 default null)
						return	varchar2 is

ds_retorno_w			varchar2(1) := 'N';
nr_retorno_w      		number(5,3) := null;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;

-- G = Gestante
-- A = Amamentando
-- P = Pregnant
-- PW = Pregnant Weeks

begin

if (cd_pessoa_fisica_p is not null) then
	cd_pessoa_fisica_w := cd_pessoa_fisica_p;
elsif (nr_atendimento_p is not null) then
	select	obter_pessoa_atendimento(nr_atendimento_p,'C')
	into	cd_pessoa_fisica_w
	from	dual;
end if;

if	(cd_pessoa_fisica_w is not null) then
	
	if(ie_tipo_info_p = 'G') then
		
		select nvl(max('S'),'N')
		into   ds_retorno_w
		from historico_saude_mulher
		where dt_liberacao is not null
		and ie_situacao = 'A'
		and	cd_pessoa_fisica = cd_pessoa_fisica_w		
		and dt_inicio_gestacao is not null
		and dt_inicio_gestacao <= sysdate
		and dt_fim_gestacao is null;
		
	elsif(ie_tipo_info_p = 'A') then
		
		select nvl(max('S'),'N')
		into   ds_retorno_w
		from historico_saude_mulher
		where dt_liberacao is not null
		and ie_situacao = 'A'
		and	cd_pessoa_fisica = cd_pessoa_fisica_w
		and dt_inicio_amamentacao is not null
		and dt_inicio_amamentacao <= sysdate
		and dt_fim_amamentacao is null;

	elsif(ie_tipo_info_p = 'P') then

		select nvl(max('S'),'N')
		into   ds_retorno_w
		from historico_saude_mulher
		where dt_liberacao is not null
		and ie_pac_gravida = 'S'
		and ie_situacao = 'A'
		and	cd_pessoa_fisica = cd_pessoa_fisica_w;

	elsif(ie_tipo_info_p = 'PW') then

		select max(qt_sem_ig_cronologica)
		into   nr_retorno_w
		from historico_saude_mulher
		where dt_liberacao is not null
		and ie_situacao = 'A'
		and ie_pac_gravida = 'S'
		and	cd_pessoa_fisica = cd_pessoa_fisica_w;

	end if;

end if;

if nr_retorno_w is not null then
    return	nr_retorno_w;
else
    return  ds_retorno_w;
end if;

return	ds_retorno_w;	

end obter_inf_saude_mulher;
/
