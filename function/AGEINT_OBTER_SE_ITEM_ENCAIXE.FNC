create or replace
function ageint_obter_se_item_encaixe (	nr_seq_agenda_exame_p	Number,
					nr_seq_agenda_cons_p	Number)
					return varchar2 is
					
ds_retorno_w		Varchar2(10);
ie_sim_w			Varchar2(10) := wheb_mensagem_pck.get_texto(306949, null);
ie_nao_w			Varchar2(10) := wheb_mensagem_pck.get_texto(306950, null);
begin
ds_retorno_w	:= ie_nao_w;

if (nvl(nr_seq_agenda_exame_p,0) > 0) then

	select	decode(nvl(max(ie_encaixe),'N'),'S',ie_sim_w,ie_nao_w)
	into	ds_retorno_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_exame_p;

elsif (nvl(nr_seq_agenda_cons_p,0)  > 0) then

	select	decode(nvl(max(ie_encaixe),'N'),'S',ie_sim_w,ie_nao_w)
	into	ds_retorno_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_cons_p;	
	
end if;

return	ds_retorno_w;

end ageint_obter_se_item_encaixe;
/
