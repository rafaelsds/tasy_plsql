create or replace
function visualizar_convenio_regra(	cd_estabelecimento_p	number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2,
					cd_convenio_p		number ) return varchar2 is
						
ds_retorno_w	varchar2(1) := 'N';
qt_conv_w	number(10);

begin
				
select	count(*)
into	qt_conv_w
from	regra_liberacao_convenio;

if	(nvl(qt_conv_w,0) = 0) then
	ds_retorno_w := 'S';
else
	select	count(*)
	into	qt_conv_w
	from	regra_liberacao_convenio
	where	cd_convenio						= cd_convenio_p
	and 	nvl(cd_estabelecimento, nvl(cd_estabelecimento_p,0)) 	= nvl(cd_estabelecimento_p,0)
	and 	nvl(cd_perfil, nvl(cd_perfil_p,0)) 			= nvl(cd_perfil_p,0)
	and 	nvl(nm_usuario_lib, nvl(nm_usuario_p,'X'))		= nvl(nm_usuario_p,'X');
	
	if	(qt_conv_w > 0) then
		ds_retorno_w 	:= 'S';
	else
		ds_retorno_w	:= 'N';
	end if;
		
end if;

return ds_retorno_w;

end visualizar_convenio_regra;
/