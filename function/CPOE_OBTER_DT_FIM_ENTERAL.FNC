create or replace function CPOE_Obter_dt_fim_enteral(cd_estabelecimento_p estabelecimento.cd_estabelecimento%type,
                                                     cd_perfil_p          perfil.cd_perfil%type,
                                                     nm_usuario_p         usuario.nm_usuario%type,
                                                     dt_inicio_p          date,
                                                     ie_continuo_p        varchar2,
                                                     nr_ocorrencia_p      number,
                                                     cd_intervalo_p       varchar2,
                                                     qt_tempo_aplicacao_p varchar2,
                                                     qt_tempo_etapa_p     varchar2,
                                                     hr_inicio_pausa_p    varchar2,
                                                     dt_fim_p             date default null
) return date

is
  nr_ocorrencia_w      cpoe_dieta.nr_ocorrencia%type;
  qt_tempo_aplicacao_w number(18, 6);
  dt_fim_ret_w         date;
  param_CPOE_24_w      varchar2(1);
    
  /*Medical Device*/
  sql_tempo_w          varchar2(250);
  sql_w                varchar2(700);
  
begin
/***
  ** Esta procedure e similar a CPOE_Calcula_horarios_enteral,o que muda e seu retorno,
     quando houver alteracao neste objeto, deve-se replicar tambem na outra
***/

  nr_ocorrencia_w := nr_ocorrencia_p;
  Obter_Param_Usuario(2314, 24, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, param_CPOE_24_w);

  begin
    sql_tempo_w := 'call obter_tempo_duracao_md(:1, :2) into :qt_tempo_aplicacao_w';
    execute immediate sql_tempo_w using in qt_tempo_aplicacao_p,
                        in 24,
                        out qt_tempo_aplicacao_w;
  exception
    when others then
      qt_tempo_aplicacao_w := null;
  end;
      
  /***
    ** O comando IF abaixo foi retirado do objeto por nao pode colocar o objeto Obter_ocorrencia_intervalo
       na rotina Medical Device.
    ** Alterado pelo desenvolvedor Anderson Almeida (META) 03 de Marco de 2021  
  ***/
  
  /* Caso nao possua NR_ETAPA, sera verificado o numero de etapas do intervalo (Gasoterapia) */
  if (nvl(cd_intervalo_p, 'null') <> 'null') then
    nr_ocorrencia_w := nvl(Obter_ocorrencia_intervalo(cd_intervalo_p, 24, 'O'), 0);
  end if;
  
  begin
    sql_w := 'call obter_dt_fim_enteral_md(:1, :2, :3, :4, :5, :6, :7, :8) into :dt_fim_ret_w';
    execute immediate sql_w using in dt_inicio_p,
                                  in nr_ocorrencia_w,
                                  in ie_continuo_p,
                                  in qt_tempo_aplicacao_p,
                                  in qt_tempo_aplicacao_w,
                                  in hr_inicio_pausa_p,
                                  in dt_fim_p,
                                  in param_CPOE_24_w,
                                  out dt_fim_ret_w;
  exception
    when others then
      dt_fim_ret_w := null;
  end;
  
  return dt_fim_ret_w;

end;
/
