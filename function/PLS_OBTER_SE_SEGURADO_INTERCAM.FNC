create or replace
function pls_obter_se_segurado_intercam
			(	nr_seq_guia_p		number,
				nr_seq_requisicao_p	number,
				nr_seq_segurado_p	number)
				return Varchar2 is

ie_retorno_w			Varchar2(2)	:= 'N';
ie_tipo_segurado_w		Varchar2(4);
nr_seq_congenere_w		Number(10);
nr_seq_uni_exec_w		Number(10);
qt_cooperativa_w		Number(10);
nr_seq_intercambio_w		Number(10);
ie_tipo_contrato_w		Varchar2(4);
nr_seq_segurado_w		Number(10);
nr_seq_congenere_sup_w		pls_congenere.nr_seq_congenere%type;
qt_registros_ww			pls_integer := 0;
cd_carteirinha_w		pls_segurado_carteira.cd_usuario_plano%type;

ie_valida_token_w		pls_param_atend_geral.ie_valida_token%type;
cd_cgc_w			varchar2(20);
begin

begin
	select	nvl(max(ie_valida_token), 'N')
	into	ie_valida_token_w
	from	pls_param_atend_geral;
exception
when others then
	ie_valida_token_w := 'N';
end;


if	(nr_seq_segurado_p is not null) then
	begin
		select	ie_tipo_segurado,
			nr_seq_congenere,
			nr_seq_intercambio
		into	ie_tipo_segurado_w,
			nr_seq_congenere_w,
			nr_seq_intercambio_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_p;
	exception
	when others then
		ie_tipo_segurado_w	:= 'X';
		nr_seq_congenere_w	:= 0;
		nr_seq_intercambio_w	:= 0;
	end;

	begin
		select	ie_tipo_contrato
		into	ie_tipo_contrato_w
		from	pls_intercambio
		where	nr_sequencia	= nr_seq_intercambio_w;
	exception
	when others then
		ie_tipo_contrato_w	:= 'X';
	end;

	if	(nvl(ie_tipo_segurado_w,'X') in ('C','I','P','T','R','H')) and (nvl(ie_tipo_contrato_w,'X')	<> 'S') then
		ie_retorno_w	:= 'S';
	end if;

	if	(ie_valida_token_w = 'S') then
		ie_retorno_w		:= 'N';

		begin
			select 	max(cd_cgc_outorgante)
			into 	cd_cgc_w
			from 	pls_outorgante
			where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
		exception
		when others then
			select 	max(cd_cgc_outorgante)
			into 	cd_cgc_w
			from 	pls_outorgante;
		end;
		

		select	max(nr_sequencia)
		into	nr_seq_uni_exec_w
		from 	pls_congenere
		where 	cd_cgc = cd_cgc_w;

		if	(nvl(ie_tipo_segurado_w,'X')	in ('I','H')) then
			select	count(1)
			into	qt_cooperativa_w
			from	pls_congenere
			where	nr_sequencia		= nr_seq_congenere_w
			and	ie_tipo_congenere	= 'CO'
			and	rownum			= 1;

			if	(qt_cooperativa_w	> 0) then
				ie_retorno_w	:= 'S';
			end if;
		elsif	(nvl(ie_tipo_segurado_w,'X')	in ('T', 'R')) then
			if ((ie_tipo_segurado_w = 'T')  and (ie_tipo_contrato_w = 'F')) then
				begin
					select	nvl(nr_cartao_intercambio,cd_usuario_plano)
					into	cd_carteirinha_w
					from	pls_segurado_carteira
					where	nr_seq_segurado	= nr_seq_segurado_p;
				exception
				when others then
					cd_carteirinha_w	:= 'X';
				end;
				
				if	(nvl(cd_carteirinha_w,'X')	= 'X') then
					wheb_mensagem_pck.exibir_mensagem_abort(191728);
				end if;

				begin
					select	nr_seq_congenere
					into	nr_seq_congenere_sup_w
					from	pls_congenere
					where	nr_sequencia	= nr_seq_congenere_w;
				exception
				when others then
					nr_seq_congenere_sup_w	:= null;
				end;
				
				select	count(1)
				into	qt_registros_ww
				from	pls_regra_envio_fundac_scs
				where	(nvl(ie_superior,'N') = 'S' and nr_seq_congenere	= nr_seq_congenere_sup_w);
			end if;
		
			select	count(1)
			into	qt_cooperativa_w
			from	pls_congenere
			where	nr_sequencia		= nr_seq_congenere_w
			and	ie_tipo_congenere	= 'CO'
			and	rownum			= 1;

			if	(qt_registros_ww	> 0) then
				ie_retorno_w	:= 'S';
			elsif	((nvl(nr_seq_congenere_w,0) <> 0) and (nvl(nr_seq_uni_exec_w,0) <> 0) and (qt_cooperativa_w > 0) and (nvl(nr_seq_congenere_w,0) <> nvl(nr_seq_uni_exec_w,0)) and
				(nvl(ie_tipo_contrato_w,'X') <> 'S')) or (ie_tipo_contrato_w	= 'F') then
				if	(nvl(nr_seq_requisicao_p,0)	<> 0) then
					if	(pls_obter_se_env_scs_benef_rep(nr_seq_segurado_w)	= 'S') then
						ie_retorno_w	:= 'S';
					end if;
				else
					ie_retorno_w	:= 'S';
				end if;
			end if;
		end if;
	end if;	
elsif	(nr_seq_guia_p is not null or nr_seq_requisicao_p is not null) then
	if	(nr_seq_guia_p is not null) then
		begin
			select	a.ie_tipo_segurado,
				a.nr_seq_congenere,
				b.nr_seq_uni_exec,
				a.nr_seq_intercambio,
				b.nr_seq_segurado
			into	ie_tipo_segurado_w,
				nr_seq_congenere_w,
				nr_seq_uni_exec_w,
				nr_seq_intercambio_w,
				nr_seq_segurado_w
			from	pls_guia_plano	b,
				pls_segurado	a
			where	a.nr_sequencia	= b.nr_seq_segurado
			and	b.nr_sequencia	= nr_seq_guia_p;
		exception
		when others then
			ie_tipo_segurado_w	:= 'X';
			nr_seq_congenere_w	:= 0;
			nr_seq_uni_exec_w	:= 0;
			nr_seq_intercambio_w	:= 0;
		end;
	else
		begin
			select	a.ie_tipo_segurado,
				a.nr_seq_congenere,
				b.nr_seq_uni_exec,
				a.nr_seq_intercambio,
				b.nr_seq_segurado
			into	ie_tipo_segurado_w,
				nr_seq_congenere_w,
				nr_seq_uni_exec_w,
				nr_seq_intercambio_w,
				nr_seq_segurado_w
			from	pls_requisicao	b,
				pls_segurado	a
			where	a.nr_sequencia	= b.nr_seq_segurado
			and	b.nr_sequencia	= nr_seq_requisicao_p;
		exception
		when others then
			ie_tipo_segurado_w	:= 'X';
			nr_seq_congenere_w	:= 0;
			nr_seq_uni_exec_w	:= 0;
			nr_seq_intercambio_w	:= 0;
		end;
	end if;

	if	(nvl(ie_tipo_segurado_w,'X')	in ('I','H')) then
		select	count(1)
		into	qt_cooperativa_w
		from	pls_congenere
		where	nr_sequencia		= nr_seq_congenere_w
		and	ie_tipo_congenere	= 'CO'
		and	rownum			= 1;

		if	(qt_cooperativa_w	> 0) then
			ie_retorno_w	:= 'S';
		end if;
	elsif	(nvl(ie_tipo_segurado_w,'X')	in ('T', 'R')) then
		
		begin
			select	ie_tipo_contrato
			into	ie_tipo_contrato_w
			from	pls_intercambio
			where	nr_sequencia	= nr_seq_intercambio_w;
		exception
		when others then
			ie_tipo_contrato_w	:= 'X';
		end;
		
		if ((ie_tipo_segurado_w = 'T')  and (ie_tipo_contrato_w = 'F')) then
			begin
				select	nvl(nr_cartao_intercambio,cd_usuario_plano)
				into	cd_carteirinha_w
				from	pls_segurado_carteira
				where	nr_seq_segurado	= nr_seq_segurado_w;
			exception
			when others then
				cd_carteirinha_w	:= 'X';
			end;
			
			if	(nvl(cd_carteirinha_w,'X')	= 'X') then
				wheb_mensagem_pck.exibir_mensagem_abort(191728);
			end if;

			begin
				select	nr_seq_congenere
				into	nr_seq_congenere_sup_w
				from	pls_congenere
				where	nr_sequencia	= nr_seq_congenere_w;
			exception
			when others then
				nr_seq_congenere_sup_w	:= null;
			end;
			
			select	count(1)
			into	qt_registros_ww
			from	pls_regra_envio_fundac_scs
			where	(nvl(ie_superior,'N') = 'S' and nr_seq_congenere	= nr_seq_congenere_sup_w);
		end if;
	
		select	count(1)
		into	qt_cooperativa_w
		from	pls_congenere
		where	nr_sequencia		= nr_seq_congenere_w
		and	ie_tipo_congenere	= 'CO'
		and	rownum			= 1;

		if	(qt_registros_ww	> 0) then
			ie_retorno_w	:= 'S';
		elsif	((nvl(nr_seq_congenere_w,0) <> 0) and (nvl(nr_seq_uni_exec_w,0) <> 0) and (qt_cooperativa_w > 0) and (nvl(nr_seq_congenere_w,0) <> nvl(nr_seq_uni_exec_w,0)) and
			(nvl(ie_tipo_contrato_w,'X') <> 'S')) or (ie_tipo_contrato_w	= 'F') then
			if	(nvl(nr_seq_requisicao_p,0)	<> 0) then
				if	(pls_obter_se_env_scs_benef_rep(nr_seq_segurado_w)	= 'S') then
					ie_retorno_w	:= 'S';
				end if;
			else
				ie_retorno_w	:= 'S';
			end if;
		end if;
	end if;
end if;

return	ie_retorno_w;

end pls_obter_se_segurado_intercam;
/
