create or replace
function obter_nome_membro_time(nr_sequencia_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
			
begin

select	obter_nome_pf_pj(cd_pessoa_fisica,null)
into	ds_retorno_w
from	desenv_member
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end obter_nome_membro_time;
/