create or replace
function corp_obter_valor_integr(	nr_sequencia_p		number,
					nr_item_nf_p		number,
					ie_opcao_p		number)
					return number is

cd_procedimento_w			number(14);
vl_total_item_nf_w			number(13,2);
vl_retorno_w				number(13,2);
/*
6900700	Serviços de Manutenção do Software Tasy
69000100	Atualização de direito de uso do sistema Tasy
69000200	Implantaçao do sistema Tasy conforme relat. anexo
69000300	Reembolso de despesa conf. relatório anexo 
69000400	CDU -  Cessão de direito de uso do sistema Tasy

69000500	LUT - Atual. Licenc. de uso do sistema Tasy 
69000600	Consultoria e Treinamento em Informática
69000700	Customizações do sistema Tasy
69000750	Suporte Adicional
69000800	Comissionamento sobre vendas (licenças Oracle)
69000902	Atualização de direito de uso de operadora do Sistema Tasy
85000900	Revenda do sistema de gerenciamento de banco de dados*/

begin
vl_retorno_w	:= 0;
select	nvl(vl_total_item_nf,0),
	cd_procedimento
into	vl_total_item_nf_w,
	cd_procedimento_w
from	nota_fiscal_item
where	nr_sequencia	= nr_sequencia_p
and	nr_item_nf	= nr_item_nf_p;

if	(ie_opcao_p = 1) then
	begin
	if	(cd_procedimento_w in(6900700, 69000500, 69000902)) then
		vl_retorno_w	:= vl_total_item_nf_w;
	end if;
	end;
elsif	(ie_opcao_p = 2) then
	begin
	if	(cd_procedimento_w in(69000100, 69000600, 69000900)) then
		vl_retorno_w	:= vl_total_item_nf_w;
	end if;
	end;
elsif	(ie_opcao_p = 3) then
	begin
	if	(cd_procedimento_w in(69000200, 69000700)) then
		vl_retorno_w	:= vl_total_item_nf_w;
	end if;
	end;
elsif	(ie_opcao_p = 4) then
	begin
	if	(cd_procedimento_w in(69000300, 69000750)) then
		vl_retorno_w	:= vl_total_item_nf_w;
	end if;
	end;
elsif	(ie_opcao_p = 5) then
	begin
	if	(cd_procedimento_w in(69000400, 69000800)) then
		vl_retorno_w	:= vl_total_item_nf_w;
	end if;
	end;
end if;

return	vl_retorno_w;

end corp_obter_valor_integr;
/