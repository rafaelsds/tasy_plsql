create or replace
function Obter_Se_MotAlta_usu	(	cd_motivo_alta_p	Number,
					cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(1)	:= 'S';
qt_existe_w	number(10);
begin

if (nvl(cd_motivo_alta_p,0) > 0) and (cd_pessoa_fisica_p is not null) then

select 	count(*)
into	qt_existe_w
from	regra_motivo_alta_usuario
where	ie_situacao = 'A'
and	cd_motivo_alta = cd_motivo_alta_p;

	if (qt_existe_w > 0) then
	
		select 	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	regra_motivo_alta_usuario
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	cd_motivo_alta = cd_motivo_alta_p
		and	ie_situacao 	 = 'A';
		
	end if;

end if;

return	ds_retorno_w;

end Obter_Se_MotAlta_usu;
/