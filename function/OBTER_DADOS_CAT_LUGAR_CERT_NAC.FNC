create or replace
function obter_dados_cat_lugar_cert_nac(	nr_seq_cat_lugar_nac_p		number,
						ie_tipo_inf_p			varchar2)
						return varchar2 is

ds_retorno_w			varchar2(255);
cd_lug_cert_nasc_w		cat_lugar_cert_nasc.cd_lug_cert_nasc%type;
ds_lug_cert_nasc_w		cat_lugar_cert_nasc.ds_lug_cert_nasc%type;

begin
if (nr_seq_cat_lugar_nac_p is not null) then

	select	cd_lug_cert_nasc,
		ds_lug_cert_nasc
	into	cd_lug_cert_nasc_w,
		ds_lug_cert_nasc_w
	from	cat_lugar_cert_nasc
	where	nr_sequencia	= nr_seq_cat_lugar_nac_p;
	
	if 	(ie_tipo_inf_p = 'CD_LUG_CERT_NASC') then
		ds_retorno_w 	:= cd_lug_cert_nasc_w;
		
	elsif	(ie_tipo_inf_p = 'DS_LUG_CERT_NASC') then
		ds_retorno_w 	:= ds_lug_cert_nasc_w;
	end if;	
end if;

return	ds_retorno_w;

end obter_dados_cat_lugar_cert_nac;
/