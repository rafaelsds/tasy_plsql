create or replace
function Obter_Desc_Sexo_Unid (	ie_sexo_paciente_p	varchar2)
				return			varchar2 is
				
ds_sexo_w		varchar2(80);

begin

if	(ie_sexo_paciente_p = 'F') then
	ds_sexo_w	:= Wheb_mensagem_pck.get_texto(309187); --'Feminino';
elsif	(ie_sexo_paciente_p = 'M') then
	ds_sexo_w	:= Wheb_mensagem_pck.get_texto(309188); --'Masculino';
elsif	(ie_sexo_paciente_p = 'L') then
	ds_sexo_w	:= Wheb_mensagem_pck.get_texto(309190); --'Livre';
elsif	(ie_sexo_paciente_p = 'A') then
	ds_sexo_w	:= Wheb_mensagem_pck.get_texto(309191); --'Livre ambos';
elsif	(ie_sexo_paciente_p = 'f') then
	ds_sexo_w	:= Wheb_mensagem_pck.get_texto(309192); --'Livre feminino';
elsif	(ie_sexo_paciente_p = 'm') then
	ds_sexo_w	:= Wheb_mensagem_pck.get_texto(309193); --'Livre masculino';
else	
	ds_sexo_w	:= Wheb_mensagem_pck.get_texto(309194); --'N�o informado';
end if;	
	
return ds_sexo_w;

end Obter_Desc_Sexo_Unid;
/