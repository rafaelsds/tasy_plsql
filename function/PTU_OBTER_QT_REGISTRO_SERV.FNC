create or replace
function ptu_obter_qt_registro_serv (	nr_sequencia_p          number,
					ie_opcao_p		varchar2)
						return number is

/*ie_opcao_p =
        702 -> qt_total_r702
        703 -> qt_total_r703
        704 -> qt_total_r704
        705 -> qt_total_r705
        qt_nota -> qt_nota_exec
        qt_serv -> qt_total_inclusao
        vl_serv -> qt_exclusos_benef
        vl_co   -> val tot custo op
        vl_fi   -> val tot filme
        */

qt_total_w                      number(15,2) := 0;
qt_total_r702_w                 number(10);
qt_total_r703_w                 number(10);
qt_total_r704_w                 number(10);
qt_total_r705_w                 number(10);
qt_serv_w                       number(10);
vl_serv_w                       number(15,2);
qt_nota_w                       number(10);
vl_tot_co_w                     number(15,2);
vl_tot_filme_w                  number(15,2);
ie_opcao_w			varchar2(255);

begin
ie_opcao_w := upper(ie_opcao_p);

if      (ie_opcao_w     = '702') then
	select  count(1)
	into    qt_total_r702_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     a.nr_sequencia          = nr_sequencia_p;

        qt_total_w      := qt_total_r702_w;
	
elsif   (ie_opcao_w     = '703') then
	select  count(1)
	into    qt_total_r703_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b,
		ptu_nota_hospitalar     c
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     b.nr_sequencia          = c.nr_seq_nota_cobr
	and     a.nr_Sequencia          = nr_sequencia_p;

        qt_total_w      := qt_total_r703_w;
	
elsif   (ie_opcao_w     = '704') then
	select  count(1)
	into    qt_total_r704_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b,
		ptu_nota_servico        c
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     b.nr_sequencia          = c.nr_seq_nota_cobr
	and     a.nr_sequencia          = nr_sequencia_p;

        qt_total_w      := qt_total_r704_w;
	
elsif   (ie_opcao_w     = '705') then
	select  count(1)
	into    qt_total_r705_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b,
		ptu_nota_complemento    c
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     b.nr_sequencia          = c.nr_seq_nota_cobr
	and     a.nr_sequencia          = nr_sequencia_p;

        qt_total_w      := qt_total_r705_w;
	
elsif   (ie_opcao_w     = 'QT_NOTA') then
	select  count(1)
	into    qt_nota_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b,
		ptu_nota_fiscal         c
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     b.nr_sequencia          = c.nr_seq_nota_cobr
	and     a.nr_sequencia          = nr_sequencia_p;

        qt_total_w      := qt_nota_w;
	
elsif   (ie_opcao_w     = 'QT_SERV') then
	select  count(1)
	into    qt_serv_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b,
		ptu_nota_servico        c
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     b.nr_sequencia          = c.nr_seq_nota_cobr
	and     c.vl_procedimento       is not null
	and     a.nr_sequencia          = nr_sequencia_p;

        qt_total_w      := qt_serv_w;
	
elsif   (ie_opcao_w     = 'VL_SERV') then
	select  sum(nvl(c.vl_procedimento,0))
	into    vl_serv_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b,
		ptu_nota_servico        c
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     b.nr_sequencia          = c.nr_seq_nota_cobr
	and     a.nr_sequencia          = nr_sequencia_p;

        qt_total_w      := vl_serv_w;
	
elsif   (ie_opcao_w     = 'VL_CO')      then
	select  sum(nvl(c.vl_custo_operacional,0))
	into    vl_tot_co_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b,
		ptu_nota_servico        c
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     b.nr_sequencia          = c.nr_seq_nota_cobr
	and     a.nr_sequencia          = nr_sequencia_p;

        qt_total_w      := vl_tot_co_w;
	
elsif   (ie_opcao_w     = 'VL_FI')      then
	select  sum(nvl(c.vl_filme,0))
	into    vl_tot_filme_w
	from    ptu_servico_pre_pagto   a,
		ptu_nota_cobranca       b,
		ptu_nota_servico        c
	where   a.nr_sequencia          = b.nr_seq_serv_pre_pagto
	and     b.nr_sequencia          = c.nr_seq_nota_cobr
	and     a.nr_sequencia          = nr_sequencia_p;

        qt_total_w      := vl_tot_filme_w;
end if;

return  qt_total_w;

end ptu_obter_qt_registro_serv;
/