CREATE OR REPLACE FUNCTION obter_cod_MEDICO_ASSISTENTE(
          cd_crm_uf_p    varchar2)
          RETURN Varchar2 IS

cd_medico_laudante_w  varchar2(10);
nr_crm_w            varchar2(20);
uf_crm_w            varchar2(2);
pos_inicio_uf_w        number(10);

begin

if  (cd_crm_uf_p is not null) then

  pos_inicio_uf_w := length(cd_crm_uf_p) - 2;
  nr_crm_w := substr(cd_crm_uf_p,1,pos_inicio_uf_w);
  uf_crm_w := substr(cd_crm_uf_p,pos_inicio_uf_w + 1,2);

   select m.cd_pessoa_fisica
    into cd_medico_laudante_w
    from medico m
   inner join pessoa_fisica pf on pf.cd_pessoa_fisica = m.cd_pessoa_fisica
   inner join conselho_profissional cp on cp.nr_sequencia = pf.nr_seq_conselho and
                                       cp.sg_conselho = 'CRM'
   where m.nr_crm = nr_crm_w
     and m.uf_crm = uf_crm_w;
end if;
  return cd_medico_laudante_w;


end obter_cod_MEDICO_ASSISTENTE;       
/