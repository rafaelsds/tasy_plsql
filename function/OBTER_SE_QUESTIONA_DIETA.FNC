create or replace
function obter_se_questiona_dieta(	nr_prescricao_p		number,
					nr_atendimento_p	number)
					return varchar2 is


qtd_w			number(10,0) 	:= 0; 
ie_retorno_w		varchar2(1)	:= 'N';

begin

Select 	count(*)
into	qtd_w
from   	prescr_dieta 
Where  	nr_prescricao = nr_prescricao_p;

if	(qtd_w > 0) then
	ie_retorno_w	:=	obter_se_atend_nutricao(nr_atendimento_p, nr_prescricao_p);
end if;

return	ie_retorno_w;

end obter_se_questiona_dieta;
/