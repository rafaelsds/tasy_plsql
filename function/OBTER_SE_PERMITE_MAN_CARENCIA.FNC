create or replace
function obter_se_permite_man_carencia(	nr_seq_proposta_p	number)
					return varchar2 is

ie_parametro_w	varchar2(2);
ds_parametro_w	varchar2(255);

begin

if	(nvl(nr_seq_proposta_p,0) > 0) then

	obter_param_usuario(1232, 53, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ds_parametro_w);
	
	select	max(pls_obter_permite_man_carencia(nr_seq_proposta_p, ds_parametro_w))
	into	ie_parametro_w
	from	dual;
	
end if;

return	ie_parametro_w;

end obter_se_permite_man_carencia;
/