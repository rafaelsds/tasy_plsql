create or replace
function obter_se_atendimento_alta	(nr_atendimento_p	number)
					return varchar2 is

ie_alta_w	varchar2(1) := 'N';

begin
if	(nr_atendimento_p is not null) then
	begin
	select  'S'
	into	ie_alta_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p
	and	dt_alta is not null;							
	exception
		when others then
		ie_alta_w := 'N';
	end;
end if;

return ie_alta_w;

end obter_se_atendimento_alta;
/