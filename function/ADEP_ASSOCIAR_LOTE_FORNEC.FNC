create or replace
procedure adep_associar_lote_fornec(nr_lote_fornec_p number,
			nr_seq_horario_p number,
			cd_material_p number,
			ie_existe_lote_p out varchar2) is

ie_existe_lote_w	varchar2(1) := 'N';
nr_seq_material_w prescr_mat_hor.nr_seq_material%TYPE;
nr_prescricao_w prescr_mat_hor.nr_prescricao%TYPE;

begin

begin
	select	'S'
	into	ie_existe_lote_w
	from	material_lote_fornec a
	where	a.nr_sequencia = nr_lote_fornec_p
	and 	a.cd_material = cd_material_p;
exception
	when NO_DATA_FOUND then
		ie_existe_lote_w := 'N';
end;

select	nr_seq_material, nr_prescricao 
into	nr_seq_material_w, nr_prescricao_w
from	prescr_mat_hor
where	nr_sequencia = nr_seq_horario_p;

if (ie_existe_lote_w = 'S') then

	update	prescr_mat_hor
	set		nr_seq_lote_fornec = nr_lote_fornec_p
	where	nr_sequencia = nr_seq_horario_p;

  if (nr_seq_material_w is not null) and
    (nr_prescricao_w is not null) then
	  update	prescr_material
	  set		nr_seq_lote_fornec = nr_lote_fornec_p
	  where	nr_prescricao = nr_prescricao_w
	  and		nr_sequencia = nr_seq_material_w;
  end if;

	commit;
end if;

ie_existe_lote_p := ie_existe_lote_w;

end adep_associar_lote_fornec;
/
