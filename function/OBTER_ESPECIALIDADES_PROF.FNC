CREATE OR REPLACE
FUNCTION Obter_Especialidades_prof
		(cd_pessoa_fisica_p 	Varchar2,
		ie_opcao_p	varchar2 default 'CD')	
		return Varchar2 IS

cd_especialidade_prof_w		profissional_especialidade.cd_especialidade_prof%type;
ds_especialidade_prof_w		especialidade_profissional.ds_especialidade_prof%type;
ds_retorno_w			varchar2(4000);

cursor	c01 is
	select 	a.cd_especialidade_prof,
		b.ds_especialidade_prof
	from 	profissional_especialidade a,
		especialidade_profissional b
	where 	a.cd_especialidade_prof = b.cd_especialidade_prof
	and 	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
BEGIN

if	(cd_pessoa_fisica_p is not null) then
	open	c01;
	loop
	fetch	c01 into 	cd_especialidade_prof_w,
				ds_especialidade_prof_w;
		exit when c01%notfound;
		begin
		
			if (ie_opcao_p = 'DS') then
				ds_retorno_w	:= ds_retorno_w || ds_especialidade_prof_w || '\';
			else 
				ds_retorno_w	:= ds_retorno_w || cd_especialidade_prof_w || ',';
			end if;
		end;
	end loop;
	close c01;
end if;	

ds_retorno_w	:= substr(ds_retorno_w, 1, length(ds_retorno_w) - 1);

RETURN ds_retorno_w;
END Obter_Especialidades_prof;
/
