/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter a sequencia da tabela PLS_SEGURADO_PRECO_ORIGEM que gera o valor de mensalidade de SCA
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[x]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_seq_valor_sca_benef
			(	nr_seq_segurado_p	number,
				nr_seq_plano_p		number,
				dt_referencia_p		date)
 		    	return number is
				
nr_seq_segurado_preco_w		number(10);

begin

select	max(nr_sequencia)
into	nr_seq_segurado_preco_w
from	pls_segurado_preco_origem
where	nr_seq_segurado = nr_seq_segurado_p
and	nr_seq_plano	= nr_seq_plano_p
and	dt_liberacao	is not null
and	(((dt_referencia_p >= trunc(dt_reajuste, 'month')) and (cd_motivo_reajuste	<> 'E'))
	or	((cd_motivo_reajuste	= 'E') and (dt_referencia_p >= trunc(add_months(dt_reajuste,1), 'month'))));

return	nr_seq_segurado_preco_w;

end pls_obter_seq_valor_sca_benef;
/
