create or replace
function cpoe_obter_orientacao_prep_amb(
						cd_material_p		number,
						qt_dose_p			number,
						cd_unidade_medida_p varchar2,
						ie_via_aplicacao_p	varchar2,
						cd_intervalo_p		varchar2) return varchar2 is
ds_retorno_w		varchar2(255);

	function obter_texto(cd_mensagem number) return varchar2 is
	begin
		return wheb_mensagem_pck.get_texto(cd_mensagem);
	end;
	
	function obter_desc_intervalo return varchar2 is 
	begin
		if (cd_intervalo_p is not null) then
			return Obter_desc_intervalo_prescr(cd_intervalo_p);
		end if;
		
		return '';
	end;
	
	function obter_desc_via_aplic return varchar2 is 
	begin
		if (ie_via_aplicacao_p is not null) then
			return ' ' || obter_texto(1005806) || ' ' || obter_desc_via(ie_via_aplicacao_p);
		end if;
		
		return '';
	end;
begin
ds_retorno_w := null;
if	(cd_material_p is not null) and
	(qt_dose_p is not null) and
	(cd_unidade_medida_p is not null) then
	ds_retorno_w := obter_texto(1005805) ||' '|| qt_dose_p ||' '|| cd_unidade_medida_p || ' ' || obter_desc_material(cd_material_p) || ' ' || obter_desc_intervalo || obter_desc_via_aplic;
end if;

return ds_retorno_w;

end cpoe_obter_orientacao_prep_amb;
/ 