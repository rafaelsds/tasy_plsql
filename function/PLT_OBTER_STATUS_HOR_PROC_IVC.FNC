create or replace
function plt_obter_status_hor_proc_ivc (
				dt_administracao_p	date,
				dt_suspensao_p		date,
				ie_dose_especial_p	varchar2,
				dt_lib_horario_p	date)
				return varchar2 is

ie_status_w	varchar2(15);

begin
if	(dt_administracao_p is null) and
	(dt_suspensao_p is null) and
	(dt_lib_horario_p is not null) then
	begin
	if	(ie_dose_especial_p = 'S') then
		begin
		ie_status_w	:= 'E';
		end;
	else
		begin
		ie_status_w	:= 'N';
		end;
	end if;	
	end;
elsif	(dt_administracao_p is not null) and
	(dt_suspensao_p is null) and
	(dt_lib_horario_p is not null) then
	begin
	ie_status_w	:= 'A';
	end;
elsif	(dt_administracao_p is null) and
	(dt_suspensao_p is not null) then
	begin
	ie_status_w	:= 'S';
	end;
elsif	(dt_administracao_p is null) and
	(dt_suspensao_p is null) and
	(dt_lib_horario_p is null) then
	begin
	ie_status_w	:= 'W';
	end;
end if;
return ie_status_w;
end plt_obter_status_hor_proc_ivc;
/
