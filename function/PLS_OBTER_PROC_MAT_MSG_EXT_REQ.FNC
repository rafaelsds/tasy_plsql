create or replace
function pls_obter_proc_mat_msg_ext_req
			(	nr_seq_requisicao_p	Number,
				nr_seq_guia_proc_p	Number,
				nr_seq_guia_mat_p	Number)
				return Varchar2 is
		
ds_retorno_w			Varchar2(4000) := '';
ds_menssagem_w			Varchar2(255) := '';
nr_seq_requisicao_w		Number(10);
nr_seq_req_proc_w		Number(10);
nr_seq_req_mat_w		Number(10);
cd_guia_w			Varchar2(255) := '';
ie_tipo_guia_w			Varchar2(255) := '';

Cursor C01 is
	select	substr(a.ds_msg_externa,1,255)
	from	pls_requisicao_glosa_ocorr_v a,
		pls_requisicao b
	where	a.nr_seq_requisicao		= nvl(nr_seq_requisicao_w,nr_seq_requisicao_p)
	and	a.nr_seq_requisicao		= b.nr_sequencia
	and	a.ds_msg_externa		is not null;

Cursor C02 is
	select	substr(b.cd_procedimento||' - '||a.ds_msg_externa,1,255)
	from	pls_requisicao_glosa_ocorr_v a,
		pls_requisicao_proc b
	where	a.nr_seq_proc		= nvl(nr_seq_req_proc_w,nr_seq_guia_proc_p)
	and	a.nr_seq_proc		= b.nr_sequencia
	and	a.ds_msg_externa	is not null
	and	b.ie_status		in ('G','N');
	
Cursor C03 is
	select	substr(nvl(substr(pls_obter_seq_codigo_material(b.nr_seq_material,''),1,255),b.nr_seq_material)||' - '||a.ds_msg_externa,1,255)
	from	pls_requisicao_glosa_ocorr_v a,
		pls_requisicao_mat b
	where	a.nr_seq_mat		= nvl(nr_seq_req_mat_w,nr_seq_guia_mat_p)
	and	a.nr_seq_mat		= b.nr_sequencia
	and	a.ds_msg_externa	is not null
	and	b.ie_status		in ('G','N');
	
Cursor C04 is
	select	nr_sequencia
	from	pls_requisicao
	where	nr_seq_guia_principal	= nr_seq_requisicao_p
	and	ie_tipo_guia		= '8';
	
Cursor C05 is
	select	nr_sequencia
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_w;
	
Cursor C06 is
	select	nr_sequencia
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_w;

begin

if	(nr_seq_requisicao_p	is not null) then

	select	ds_observacao
	into	ds_retorno_w
	from	pls_Requisicao
	where	nr_sequencia 	= nr_seq_requisicao_p;
	
	open C04;
	loop
	fetch C04 into	
		nr_seq_requisicao_w;
	exit when C04%notfound;
		begin
		open C01;
		loop
		fetch C01 into	
			ds_menssagem_w;
		exit when C01%notfound;
			begin
			if	(ds_retorno_w	is null) then
				ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
			else
				ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
			end if;
			end;
		end loop;
		close C01;
		
		open C05;
		loop
		fetch C05 into	
			nr_seq_req_proc_w;
		exit when C05%notfound;
			begin
			open C02;
			loop
			fetch C02 into	
				ds_menssagem_w;
			exit when C02%notfound;
				begin
				if	(ds_retorno_w	is null) then
					ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
				else
					ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
				end if;
				end;
			end loop;
			close C02;
			end;
		end loop;
		close C05;
		
		open C06;
		loop
		fetch C06 into	
			nr_seq_req_mat_w;
		exit when C06%notfound;
			begin
			open C03;
			loop
			fetch C03 into	
				ds_menssagem_w;
			exit when C03%notfound;
				begin
				if	(ds_retorno_w	is null) then
					ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
				else
					ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
				end if;
				end;
			end loop;
			close C03;
			end;
		end loop;
		close C06;
		end;
	end loop;
	close C04;
	
	open C01;
	loop
	fetch C01 into	
		ds_menssagem_w;
	exit when C01%notfound;
		begin

		if	(ds_retorno_w	is null) then
			ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
		else
			ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
		end if;
		end;
	end loop;
	close C01;
end if;

if	(nr_seq_guia_proc_p	is not null) then
	open C02;
	loop
	fetch C02 into	
		ds_menssagem_w;
	exit when C02%notfound;
		begin
		if	(ds_retorno_w	is null) then
			ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
		else
			ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
		end if;
		end;
	end loop;
	close C02;
end if;

if	(nr_seq_guia_mat_p	is not null) then
	open C03;
	loop
	fetch C03 into	
		ds_menssagem_w;
	exit when C03%notfound;
		begin
		if	(ds_retorno_w	is null) then
			ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
		else
			ds_retorno_w	:= ds_retorno_w||chr(13)||ds_menssagem_w;
		end if;
		end;
	end loop;
	close C03;
end if;

return	ds_retorno_w;

end pls_obter_proc_mat_msg_ext_req;
/
