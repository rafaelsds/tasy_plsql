create or replace
function	obter_se_exibe_opme_grupo	(	nr_seq_agenda_p			number,
							nr_seq_agenda_proc_adic_p	number,	
							nr_sequencia_p			number
						) 
						return varchar2 is

cd_especialidade_w	number(10,0):=null;
cd_medico_w		varchar2(10):=null;
cd_convenio_w		number(10):=null;
cd_especialidade_ww	number(10,0):=null;
cd_medico_ww		varchar2(10):=null;
cd_convenio_ww		number(10):=null;
qt_idade_paciente_w	number(3,0);
ie_sexo_w		varchar2(1);
ie_exibe_w		varchar2(1);
qt_registro_w		number(10);
ie_tipo_convenio_w	number(2);
		
begin

select	obter_especialidade_medico(cd_medico,'C'),
	cd_medico,
	cd_convenio,
	nvl(qt_idade_paciente,obter_idade_pf(cd_pessoa_fisica,sysdate,'A')),
	obter_sexo_pf(cd_pessoa_fisica,'C')
into	cd_especialidade_w,
	cd_medico_w,
	cd_convenio_w,
	qt_idade_paciente_w,
	ie_sexo_w	
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_p;

if	(nvl(nr_seq_agenda_proc_adic_p,0) > 0) then
	select	obter_especialidade_medico(b.cd_medico,'C'),
		b.cd_medico,
		b.cd_convenio,
		nvl(a.qt_idade_paciente,obter_idade_pf(a.cd_pessoa_fisica,sysdate,'A')),
		obter_sexo_pf(a.cd_pessoa_fisica,'C')
	into	cd_especialidade_ww,
		cd_medico_ww,
		cd_convenio_ww,
		qt_idade_paciente_w,
		ie_sexo_w	
	from	agenda_paciente a,
		agenda_paciente_proc b
	where	a.nr_sequencia 	= b.nr_sequencia
	and	a.nr_sequencia 	= nr_seq_agenda_p
	and	b.nr_seq_agenda = nr_seq_agenda_proc_adic_p;
end if;	

ie_tipo_convenio_w	:= 	nvl(obter_tipo_convenio(nvl(cd_convenio_ww,cd_convenio_w)),0);


select	count(*)
into	qt_registro_w
from	proc_interno_opme a
where	a.nr_sequencia = nr_sequencia_p
and	((a.cd_medico is null) or (a.cd_medico	= nvl(cd_medico_ww,cd_medico_w)))
and	nvl(a.cd_convenio,nvl(nvl(cd_convenio_ww,cd_convenio_w),0))	= nvl(nvl(cd_convenio_ww,cd_convenio_w),0)
and	nvl(a.ie_tipo_convenio,ie_tipo_convenio_w)	= ie_tipo_convenio_w
and	((a.cd_especialidade is null) or (a.cd_especialidade = nvl(cd_especialidade_ww,cd_especialidade_w)))
and	(nvl(qt_idade_paciente_w,0) between nvl(a.qt_idade_minima,0) and nvl(a.qt_idade_maxima,999))
and	((a.ie_sexo is null) or (a.ie_sexo = ie_sexo_w))
and 	(((a.ie_somente_exclusivo = 'S') and not exists (	select  1 
								from	proc_interno_opme x
								where	a.nr_seq_proc_interno     	= x.nr_seq_proc_interno
								and	x.cd_medico			= nvl(cd_medico_ww,cd_medico_w))) or (nvl(a.ie_somente_exclusivo,'N') = 'N'));

if	(qt_registro_w > 0) then
	ie_exibe_w	:= 'S';
else	
	ie_exibe_w	:= 'N';
end if;	

return	ie_exibe_w;

end obter_se_exibe_opme_grupo;
/