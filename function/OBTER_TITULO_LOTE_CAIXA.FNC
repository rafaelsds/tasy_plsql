CREATE OR REPLACE
FUNCTION Obter_Titulo_Lote_Caixa( 		nr_seq_movto_p		number,
					ie_opcao_p		varchar2)
					RETURN Varchar2 IS

/*
TP	= T�tulo a Pagar
TR	= T�tulo a Receber
*/

nr_titulo_result_w		Number(10);
nr_titulo_receber_w	Number(10);
nr_titulo_pagar_w		Number(10);
nr_seq_lote_w		Number(10);
nr_seq_caixa_w		Number(10);

begin

if	(nr_seq_movto_p is not null) and (ie_opcao_p is not null) then

	select	a.nr_seq_lote,
		a.nr_seq_caixa
	into	nr_seq_lote_w,
		nr_seq_caixa_w
	from	movto_trans_financ a
	where	a.nr_sequencia	= nr_seq_movto_p;


	select	max(a.nr_seq_titulo_pagar),
		max(a.nr_seq_titulo_receber)
	into	nr_titulo_pagar_w,
		nr_titulo_receber_w
	from	movto_trans_financ a
	where	a.nr_seq_caixa	= nr_seq_caixa_w
	and	a.nr_seq_lote	= nr_seq_lote_w;

end if;

if	(ie_opcao_p = 'TP') then
	nr_titulo_result_w := nr_titulo_pagar_w;
elsif	(ie_opcao_p = 'TR') then
	nr_titulo_result_w := nr_titulo_receber_w;
end if;

return	nr_titulo_result_w;

END Obter_Titulo_Lote_Caixa;
/
