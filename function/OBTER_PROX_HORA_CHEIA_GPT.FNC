create or replace
function Obter_prox_hora_cheia_gpt(	dt_referencia_p	Date)
 		    	return date is
ds_retorno_w		date;
begin
	ds_retorno_w := to_date(to_char(dt_referencia_p,'dd/mm/yyyy') || ' ' || to_char(to_date(Obter_prox_hora_cheia(dt_referencia_p),'hh24:mi:ss'),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'); 
	return	ds_retorno_w;
end Obter_prox_hora_cheia_gpt;
/