create or replace
function obter_local_ferida_ident(	nr_seq_identificacao_p		number)
 		    	return varchar2 is

ds_resultado_w	varchar2(255);

begin
SELECT	max(ds_identificacao)
into	ds_resultado_w
FROM	localizacao_ferida_ident b
WHERE	b.nr_sequencia = nr_seq_identificacao_p;

return	ds_resultado_w;

end obter_local_ferida_ident;
/