create or replace
function ADEP_obter_se_consiste_item(	ie_tipo_item_p			varchar2,
										cd_perfil_p				number,
										cd_procedimento_p		varchar2,
										ie_origem_proced_p		varchar2,
										nr_seq_proc_interno_p	number default null) 
						return varchar2 is

type tcursor is ref cursor;
cCursor tcursor;

nr_sequencia_w			adep_regra_inconsistencia.nr_sequencia%type;

cd_procedimento_w		procedimento.cd_procedimento%type;
ie_origem_proced_w		procedimento.ie_origem_proced%type;
cd_area_procedimento_w	estrutura_procedimento_v.cd_area_procedimento%type;
cd_especialidade_w		estrutura_procedimento_v.cd_especialidade%type;
cd_grupo_proc_w			estrutura_procedimento_v.cd_grupo_proc%type;

ie_consistir_w			varchar2(3) := 'S';
ie_existe_w				char(1);
ds_sql_cursor_w			varchar2(500);
ie_tem_proced_w			varchar2(1) := 'N';

Cursor C01 is
	select	ie_consistir
	from	adep_regra_incons_item
	where	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
	and		((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))	
	and		((cd_area_procedimento = cd_area_procedimento_w)	or (cd_area_procedimento is null))
	and		((cd_especialidade = cd_especialidade_w) or			(cd_especialidade is null))
	and		((cd_grupo_proc = cd_grupo_proc_w)  or (cd_grupo_proc is null))
	and		nr_seq_regra 								= nr_sequencia_w
	order by nvl(cd_procedimento,0),
			 nvl(nr_seq_proc_interno,0),
			 nvl(cd_especialidade,0),			 
			 nvl(cd_area_procedimento,0);
			 
			 
begin

if	(ie_tipo_item_p is not null) then
	ds_sql_cursor_w		:= ' select		nr_sequencia, ';
	
	if	(ie_tipo_item_p	= 'D') then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_dieta,''S'') ';
	elsif	(ie_tipo_item_p	= 'MAT') then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_material,''S'') ';
	elsif	(ie_tipo_item_p	= 'R') then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_recomendacao,''S'') ';
	elsif	(ie_tipo_item_p	= 'E') then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_sae,''S'') ';
	elsif	(ie_tipo_item_p	= 'SOL') then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_solucao,''S'') ';
	elsif	(ie_tipo_item_p	= 'CCG') then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_ccg,''S'') ';
	elsif	(ie_tipo_item_p	= 'O') then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_gasoterapia,''S'') ';
	elsif	(ie_tipo_item_p	in ('S','NE')) then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_suplemento,''S'') ';
	elsif	(ie_tipo_item_p	in ('M', 'ME', 'MAP')) then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_medicamento,''S'') ';
	elsif	(ie_tipo_item_p	in ('P', 'L')) then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' 			coalesce(ie_procedimento,''S'') ';
	else
		return ie_consistir_w;
	end if;
	
	ds_sql_cursor_w		:=	ds_sql_cursor_w || ' from	adep_regra_inconsistencia ';
	
	if	(coalesce(cd_perfil_p,0) = 0) then
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' where	cd_perfil is null ';
	else
		ds_sql_cursor_w		:=	ds_sql_cursor_w || ' where	coalesce(cd_perfil,'||cd_perfil_p||') = '||cd_perfil_p||' ';
	end if;
	
	ds_sql_cursor_w		:=	ds_sql_cursor_w || ' order by coalesce(cd_perfil,0) ';

	if	(ie_tipo_item_p	in ('P', 'L')) and
		((coalesce(cd_procedimento_p,'0') <> '0') or
		(nvl(nr_seq_proc_interno_p,0) > 0))	then
		
		if	((ie_tipo_item_p = 'P') and (coalesce(cd_procedimento_p,'0') <> '0')) then
			cd_procedimento_w	:= to_number(cd_procedimento_p);		
			ie_origem_proced_w	:= to_number(ie_origem_proced_p);				
			ie_tem_proced_w 	:= 'S';
		else
			cd_area_procedimento_w	:= 0;
			cd_grupo_proc_w			:= 0;
			cd_especialidade_w		:= 0;			
			cd_procedimento_w		:= 0;
		end if;
		
		open cCursor for ds_sql_cursor_w;
		loop
		fetch cCursor into	
			nr_sequencia_w,
			ie_consistir_w;
		exit when cCursor%notfound;
		
			select	coalesce(max('S'),'N')
			into	ie_existe_w
			from	adep_regra_incons_item
			where	rownum = 1
			and		nr_seq_regra	= nr_sequencia_w;
			
			if	(ie_existe_w = 'S') then			
								
				if	(ie_tem_proced_w = 'S') then
					begin
					select 	cd_area_procedimento,
							cd_especialidade,
							cd_grupo_proc
					into	cd_area_procedimento_w,
							cd_especialidade_w,
							cd_grupo_proc_w
					from 	estrutura_procedimento_v
					where 	ie_origem_proced 	= ie_origem_proced_w
					and		cd_procedimento 	= cd_procedimento_w;
					exception
						when others then 
							cd_area_procedimento_w	:= 0;
							cd_grupo_proc_w			:= 0;
							cd_especialidade_w		:= 0;						
					end;
				end if;
								
				open C01;
				loop
				fetch C01 into	
					ie_consistir_w;
				exit when C01%notfound;
					begin			
					ie_consistir_w	:= ie_consistir_w;						
					end;
				end loop;
				close C01;
				
								
			end if;
		end loop;
		close cCursor;
	else
		open cCursor for ds_sql_cursor_w;
		loop
		fetch cCursor into	
			nr_sequencia_w,
			ie_consistir_w;
		exit when cCursor%notfound;
		end loop;
		close cCursor;
	end if;
end if;

return	ie_consistir_w;

end ADEP_obter_se_consiste_item;
/