create or replace 
function Obter_Desc_Proc_Princ_interno(
				nr_atendimento_p	number)
				return varchar2 is

ds_retorno_w			varchar2(255) := null;
ie_tipo_atendimento_w	Number(15,0);
cd_procedimento_w		Number(15,0);
ie_origem_proced_w		Number(10,0);

BEGIN

select	ie_tipo_atendimento
into	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

obter_proc_princ_interno(nr_atendimento_p, ie_tipo_atendimento_w, cd_procedimento_w, ie_origem_proced_w, 0);
select	max(ds_procedimento)
into	ds_retorno_w
from	procedimento
where	cd_procedimento	= cd_procedimento_w
and	ie_origem_proced	= ie_origem_proced_w;

return ds_retorno_w;

end Obter_desc_Proc_Princ_Interno;
/