CREATE OR REPLACE
FUNCTION Obter_Instrucao_Titulo_Receber(nr_titulo_p		NUMBER)
					RETURN VARCHAR2 IS

cd_estabelecimento_w		NUMBER(04,0);
ds_instrucao_w			VARCHAR2(4000);
vl_titulo_w			NUMBER(15,2);
vl_juro_w			NUMBER(15,2);
pr_juro_w			NUMBER(15,2) 	:= 3;
cd_moeda_w			titulo_receber.cd_moeda%TYPE;

BEGIN

SELECT NVL(vl_titulo,0),
	NVL(tx_juros,0),
	cd_estabelecimento,
	cd_moeda
INTO	vl_titulo_w,
	pr_juro_w,
	cd_estabelecimento_w,
	cd_moeda_w
FROM	titulo_receber
WHERE	nr_titulo	= nr_titulo_p;

BEGIN

SELECT ds_instrucao_bloqueto
INTO ds_instrucao_w
FROM parametro_contas_receber
WHERE cd_estabelecimento	= cd_estabelecimento_w;

EXCEPTION
WHEN OTHERS THEN
	ds_instrucao_w	:= '';
END;

vl_juro_w	:= vl_titulo_w * pr_juro_w / 100 / 30;
IF	(pr_juro_w > 0) THEN
	ds_instrucao_w	:=	ds_instrucao_w || CHR(13) || CHR(10) || 
				wheb_mensagem_pck.get_texto(301941) || ' ' || obter_desc_sigla_moeda(cd_moeda_w) || ' ' || Campo_Mascara_casas(vl_juro_w,2);
END IF;

RETURN ds_instrucao_w;

END Obter_Instrucao_Titulo_Receber;
/
