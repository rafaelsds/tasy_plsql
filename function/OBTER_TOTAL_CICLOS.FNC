create or replace
function obter_total_ciclos (nr_prescricao_p	number) 
							return number is

nr_total_ciclos_w	number(15) := 0;

begin

if (nr_prescricao_p is not null) then
	
	select	max(trunc(decode((nvl(qt_hora_duracao,0) * 60) + nvl(qt_min_duracao,0),0,0,((nvl(qt_hora_sessao,0) * 60) + nvl(qt_min_sessao,0)) / ((nvl(qt_hora_duracao,0) * 60) + nvl(qt_min_duracao,0)))))
	into 	nr_total_ciclos_w
	from 	hd_prescricao 
	where 	nr_prescricao = nr_prescricao_p;
	
	if (nvl(nr_total_ciclos_w,0) = 0) then
		nr_total_ciclos_w	:= 0;
	end if;
	
end if;

return nr_total_ciclos_w;

end obter_total_ciclos;
/