create or replace
function pls_obter_classif_canal_venda
			(	nr_seq_classif_p	number)
 		    	return varchar2 is

ds_classificacao_w	pls_classif_canal_venda.ds_classificacao%type;
ds_retorno_w		varchar2(255);

begin

select	max(ds_classificacao)
into	ds_classificacao_w
from	pls_classif_canal_venda
where	nr_sequencia	= nr_seq_classif_p;

ds_retorno_w	:= substr(ds_classificacao_w,1,255);

return	ds_retorno_w;

end pls_obter_classif_canal_venda;
/