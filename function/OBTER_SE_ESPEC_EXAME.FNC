create or replace
function obter_se_espec_exame(	nr_seq_exame_p	number,
				cd_medico_p	varchar2 )
				return varchar2 is

cd_especialidade_w	number(5,0);
ie_utiliza_w		varchar2(1) := 'N';

cursor c01 is
	select	cd_especialidade
	from	medico_especialidade
	where	cd_pessoa_fisica	=	cd_medico_p;

begin

OPEN C01;
LOOP
FETCH C01 into
	cd_especialidade_w;
exit when c01%notfound;
	begin

	select	nvl(max('S'),'N')
	into	ie_utiliza_w
	from	med_exame_padrao_espec
	where	nr_seq_exame_padrao	=	nr_seq_exame_p
	and	cd_especialidade	=	cd_especialidade_w;

	if	(ie_utiliza_w = 'S') then
		exit;
	end if;

	end;
END LOOP;
CLOSE C01;

if	(ie_utiliza_w = 'N') then
	select	nvl(max('N'),'S')
	into	ie_utiliza_w
	from	med_exame_padrao_espec
	where	nr_seq_exame_padrao	=	nr_seq_exame_p;
end if;

return	ie_utiliza_w;

end obter_se_espec_exame;
/