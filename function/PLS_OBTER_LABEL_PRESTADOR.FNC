create or replace
function pls_obter_label_prestador
			(	nm_tabela_p		varchar2)
 		    		return Varchar2 is
				
nm_retorno_w			varchar2(80);

begin

if	(nm_tabela_p = 'PLS_PRESTADOR') then
	nm_retorno_w	:= 'Prestador';
elsif	(nm_tabela_p = 'PLS_PRESTADOR_AREA') then
	nm_retorno_w	:= '�rea Atua��o';
elsif	(nm_tabela_p = 'PLS_PRESTADOR_MAT') then
	nm_retorno_w	:= 'Materiais';
elsif	(nm_tabela_p = 'PLS_PRESTADOR_PLANO') then
	nm_retorno_w	:= 'Produtos';
elsif	(nm_tabela_p = 'PLS_PRESTADOR_MED_ESPEC') then
	nm_retorno_w	:= 'Especialidades m�dicas do prestador';
elsif	(nm_tabela_p = 'PLS_PRESTADOR_TIPO') then
	nm_retorno_w	:= 'Tipos do prestador';
elsif	(nm_tabela_p = 'PLS_PRESTADOR_USUARIO_WEB') then
	nm_retorno_w	:= 'Prestadores';
elsif	(nm_tabela_p = 'PLS_PRESTADOR_PROC') then
	nm_retorno_w	:= 'Servi�os';
elsif	(nm_tabela_p = 'PLS_PRESTADOR_PAGTO') then
	nm_retorno_w	:= 'Dados pagamento';
elsif	(nm_tabela_p = 'PLS_GERAR_DADOS_PROC') then
	nm_retorno_w	:= 'Regra de gera��o de dados do procedimento';
elsif	(nm_tabela_p = 'PLS_CONVERSAO_PROC') then
	nm_retorno_w	:= 'Convers�o procedimento';
elsif	(nm_tabela_p = 'PLS_USUARIO_WEB') then
	nm_retorno_w	:= 'Acesso web';
elsif	(nm_tabela_p = 'PLS_ENDERECO_GUIA_MEDICO') then
	nm_retorno_w	:= 'Endere�o opcional guia m�dico';
else
	nm_retorno_w	:= 'N';
end if;

return	nm_retorno_w;

end pls_obter_label_prestador;
/
