create or replace function obter_mentor_campo (	nm_tabela_origem_p varchar2,
												ds_campo_p varchar2) return varchar2 is 
	ds_campo_mentor_w varchar2(100);
begin
	if (nm_tabela_origem_p = 'ATENDIMENTO_SINAL_VITAL') THEN
		select ds_sinal_vital
		into ds_campo_mentor_w
		from sinal_vital
		where nm_atributo = ds_campo_p;
	elsif (nm_tabela_origem_p = 'EXAME_LAB_RESULT_ITEM') THEN
		ds_campo_mentor_w := ds_campo_p;
	elsif (nm_tabela_origem_p = 'DIAGNOSTICO_DOENCA') THEN
		ds_campo_mentor_w := ds_campo_p;
	elsif (nm_tabela_origem_p = 'QUA_EVENTO_PACIENTE') THEN
		ds_campo_mentor_w := ds_campo_p;
	elsif (nm_tabela_origem_p = 'MAN_CLASSIF_RISCO') THEN
		ds_campo_mentor_w := ds_campo_p;
	else
		if (ds_campo_p is not null) THEN
			ds_campo_mentor_w := obter_desc_expressao(296046);
		else
			ds_campo_mentor_w := null;
		end if;
	end if;
	return ds_campo_mentor_w;
end obter_mentor_campo;
/