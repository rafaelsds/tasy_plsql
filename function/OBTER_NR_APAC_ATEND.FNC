create or replace
function obter_nr_apac_atend(nr_atendimento_p	number)
 		    	return varchar2 is

nr_apac_w	number(13);
nr_todos_apac_w	varchar2(255);

			
Cursor C01 is
	select	nr_apac
	from	sus_laudo_paciente
	where	nr_atendimento = nr_atendimento_p;
	
begin

open C01;
loop
fetch C01 into	
	nr_apac_w;
exit when C01%notfound;
	begin
	if 	(nr_apac_w is not null) then 
		if	(nr_todos_apac_w is null) then
			nr_todos_apac_w := nr_apac_w; 
		else
			nr_todos_apac_w := nr_todos_apac_w ||'/'|| nr_apac_w;
		end if;
	end if;
	end;
end loop;
close C01;


return	nr_todos_apac_w;

end obter_nr_apac_atend;
/