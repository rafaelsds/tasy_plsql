create or replace
function obter_status_impr_lote_fornec(nr_seq_lote_fornec_p Number)
        return Varchar2 is

/*ds_retorno_w:
N - N�o impresso
I - Impresso
P - Parcialmente impresso
S - Impress�o superou quantidade do lote*/

ds_retorno_w  Varchar2(1);
qt_material_w  Number(10);
qt_impressa_w  Number(10);
dt_impressao_w  Date;

begin

select qt_material,
 dt_impressao
into qt_material_w,
 dt_impressao_w
from material_lote_fornec
where nr_sequencia = nr_seq_lote_fornec_p;

select sum(qt_impressao)
into qt_impressa_w
from material_lote_fornec_imp
where nr_seq_lote_fornec = nr_seq_lote_fornec_p;

if (nvl(qt_impressa_w,0) = 0 and dt_impressao_w is null) then
 ds_retorno_w := 'N';
elsif  ((nvl(qt_impressa_w,0) = qt_material_w) or (nvl(qt_impressa_w,0) = 0 and dt_impressao_w is not null)) then
 ds_retorno_w := 'I';
elsif  (nvl(qt_impressa_w,0) > 0 and nvl(qt_impressa_w,0) < qt_material_w) then
 ds_retorno_w := 'P';
elsif  (nvl(qt_impressa_w,0) > qt_material_w) then
 ds_retorno_w := 'S';
end if;

return ds_retorno_w;

end obter_status_impr_lote_fornec;
/
