create or replace
function pls_obter_seq_titulo_receber(  nr_seq_guia_p		Number)
					return 			number is

nr_seq_guia_w	number(15);
begin
	select	nvl(max(nr_titulo),0)
	into	nr_seq_guia_w
	from	titulo_receber
	where	nr_seq_guia	= nr_seq_guia_p;

return	nr_seq_guia_w;

end pls_obter_seq_titulo_receber;
/