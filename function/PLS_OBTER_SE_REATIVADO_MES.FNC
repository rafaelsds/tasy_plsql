create or replace
function pls_obter_se_reativado_mes	(nr_seq_segurado_p	varchar2,
					dt_referencia_p		date)	return varchar2 is

ie_retorno_w		varchar2(1)	:= 'N';
dt_hist_rescisao_w	date;
dt_hist_reativacao_w	date;

begin

select	max(dt_historico)
into	dt_hist_rescisao_w
from	pls_segurado_historico
where	nr_seq_segurado = nr_seq_segurado_p
and	ie_tipo_historico = '1';

select	max(dt_historico)
into	dt_hist_reativacao_w
from	pls_segurado_historico
where	nr_seq_segurado = nr_seq_segurado_p
and	ie_tipo_historico = '2';

if (trunc(dt_hist_reativacao_w,'month') = trunc(dt_hist_rescisao_w,'month')) then
	ie_retorno_w := 'S';
end if;

return ie_retorno_w;

end pls_obter_se_reativado_mes;
/