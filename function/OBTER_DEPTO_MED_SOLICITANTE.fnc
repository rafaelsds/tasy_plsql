create or replace function obter_depto_med_solicitante( 
            cd_medico_p in medico.cd_pessoa_fisica%type
) return varchar2 is 

ds_retorno_w		varchar2(200);

begin

    if (cd_medico_p is not null) then
    
        select max(dm.ds_depto)
        into ds_retorno_w
        from depto_medico dm,
            medico_especialidade mc,
            medico m,
            depto_medico_espec dme
        where mc.cd_pessoa_fisica = m.cd_pessoa_fisica
            and mc.cd_especialidade = dme.cd_especialidade
            and dme.nr_seq_depto = dm.nr_sequencia
            and mc.cd_pessoa_fisica = cd_medico_p
            and mc.nr_seq_prioridade = ( select min(mde.nr_seq_prioridade)
                        from depto_medico dpm,
                            medico_especialidade mde,
                            medico medc,
                            depto_medico_espec dpme
                        where mde.cd_pessoa_fisica = medc.cd_pessoa_fisica
                            and mde.cd_especialidade = dpme.cd_especialidade
                            and dpme.nr_seq_depto = dpm.nr_sequencia
                            and medc.cd_pessoa_fisica = m.cd_pessoa_fisica);

    end if;
    
return ds_retorno_w;

end obter_depto_med_solicitante;
/
