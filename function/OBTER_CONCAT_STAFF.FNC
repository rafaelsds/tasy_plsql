create or replace FUNCTION  OBTER_CONCAT_STAFF
(
  NR_SEQUENCIA_P IN NUMBER ,
  PROF_P IN NUMBER 
) RETURN VARCHAR2 AS 

data_w varchar2(1000); 
Assist_Physician varchar2(1000); 
Sec_nurse varchar2(1000); 

BEGIN

IF(PROF_P=3) then --Assist_Physician 
for i in (select (CD_PHYSICIAN) as Assist_Physician from MED_TREATMENT_PLAN_TEAM where SI_PERSON_TYPE=2 and NR_SEQ_MED_TREAT_PLAN=NR_SEQUENCIA_P)
loop
data_w := data_w ||',' ||i.Assist_Physician;
end loop;

end if ;

IF(PROF_P=4) then --Nurse Secondary 
for i in (select (CD_PERSON) as Sec_nurse   from MED_TREATMENT_PLAN_TEAM  where SI_PERSON_TYPE=5 and NR_SEQ_MED_TREAT_PLAN=NR_SEQUENCIA_P and cd_position in (717,707,441))
loop
data_w := data_w||','||i.Sec_nurse;
end loop;
end if ;


  RETURN ltrim(data_w,',');
END Obter_concat_staff; 
/
