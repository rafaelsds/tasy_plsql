CREATE OR REPLACE
FUNCTION OBTER_INTERVALO_GLICEMIA(nr_sequencia_p number)

					RETURN VARCHAR2 IS

ds_retorno_w		Varchar2(80);
cd_intervalo_w		varchar2(10);
nr_prescricao_w		number;
nr_sequencia_w          number;


BEGIN

select nr_prescricao,
       nr_seq_procedimento
into   nr_prescricao_w,
       nr_sequencia_w
from   atend_glicemia 
where  nr_sequencia = nr_sequencia_p;       

select nvl(max(cd_intervalo),'')
into   cd_intervalo_w
from   prescr_procedimento
where  nr_sequencia  = nr_sequencia_w
and    nr_prescricao = nr_prescricao_w;

ds_retorno_w := substr(OBTER_DESC_INTERVALO(cd_intervalo_w),1,80);

return ds_retorno_w;

end OBTER_INTERVALO_GLICEMIA;
/