create or replace
function obter_esclarecimento_regulacao(nr_regulacao_atendimento_p	number)
 		    	return number is

nr_esclarecimento_w	regulacao_info_adicional.nr_esclarecimento_regulacao%type;

begin


SELECT	MAX(a.nr_esclarecimento_regulacao)
into	nr_esclarecimento_w
FROM	regulacao_info_adicional a
WHERE	a.nr_seq_regulacao_atendimento = nr_regulacao_atendimento_p
AND	a.nr_sequencia = 	(SELECT MAX(c.nr_sequencia) 
				FROM   	regulacao_info_adicional c
				WHERE  	c.nr_esclarecimento_regulacao IS NOT NULL
                                AND     c.nr_esclarecimento_regulacao > 0 
				AND	c.nr_seq_regulacao_atendimento = nr_regulacao_atendimento_p);

return	nr_esclarecimento_w;

end obter_esclarecimento_regulacao;
/