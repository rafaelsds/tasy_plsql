create or replace
function paf_se_pv_possui_item(nr_prevenda_p		number)
 		    	return varchar2 is


qt_cupom_w		number(13);
qt_prevenda_ativas_w	number(13);
nr_sequencia_w		number(10);
ie_situacao_w		varchar2(1);

ds_retorno_w		varchar2(255):= 'N';

/* ds_retorno_w
N - Cancelamento n�o permitido
U - Cancela �ltimo item
E -  Apresentar panel para cancelamento
S -  Apresentar mensagem de cancelar �ltimo item, caso contr�rio, abrir panel para selecionar item a ser cancelado
*/
	
begin

-- total geral
select	count(*)
into	qt_cupom_w
from	paf_pre_venda_item a
where	a.nr_seq_pre_venda = nr_prevenda_p;

-- somente ativos
select	count(*)
into	qt_prevenda_ativas_w
from	paf_pre_venda_item a
where	a.nr_seq_pre_venda = nr_prevenda_p
and	a.ie_situacao <> 'C';

select	nvl(max(nr_sequencia), 0)
into	nr_sequencia_w
from	paf_pre_venda_item a
where	a.nr_seq_pre_venda = nr_prevenda_p;

if	(nr_sequencia_w <> 0) then
	select	nvl(ie_situacao, 'N')
	into	ie_situacao_w
	from	paf_pre_venda_item a
	where	a.nr_sequencia = nr_sequencia_w;	
end if;

if	(qt_cupom_w = 0) then 
	
	/*  caso n�o existir item no cupom, retornar  cancelamento n�o permitido */	
	ds_retorno_w:= 'N';
		
else
	
	if	((qt_cupom_w = 1) and (ie_situacao_w = 'C')) then

		ds_retorno_w:= 'N';
		
	elsif ((qt_cupom_w = 1) and (ie_situacao_w <> 'C')) then
		
		ds_retorno_w:= 'U';
		
	elsif ((qt_cupom_w > 1) and (ie_situacao_w = 'C') and (qt_prevenda_ativas_w > 0)) then

		ds_retorno_w:= 'E';
		
	elsif ((qt_cupom_w > 1) and (ie_situacao_w = 'C') and (qt_prevenda_ativas_w = 0)) then

		ds_retorno_w:= 'N';	
		
	elsif ((qt_cupom_w > 1) and (ie_situacao_w <> 'C') and (qt_prevenda_ativas_w <> 1)) then	

		ds_retorno_w:= 'S';	
		
	elsif ((qt_cupom_w > 1) and (ie_situacao_w <> 'C') and (qt_prevenda_ativas_w = 1)) then	

		ds_retorno_w:= 'U';
			
	end if;
		
end if;
			
return	ds_retorno_w;

end paf_se_pv_possui_item;
/
