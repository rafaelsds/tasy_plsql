create or replace
function lab_verifica_tipo_coleta (	nr_seq_secretaria_p	number,
                                    nm_usuario_p	varchar2) 
										return varchar2 is 

	
nr_seq_pacote_w		    lote_ent_secretaria.nr_seq_pacote%type;
nr_seq_ficha_w		    lote_ent_sec_ficha.nr_sequencia%type;
nr_seq_ficha_ww		    lote_ent_sec_ficha.nr_sequencia%type;
nr_seq_exame_lab_w	  exame_laboratorio.nr_seq_exame%type;
cd_material_exame_w	  material_exame_lab.cd_material_exame%type;
ie_tipo_data_w		    lote_ent_tipo_coleta.ie_tipo_data%type;
qt_unidade_w		      lote_ent_tipo_coleta.qt_unidade%type;
qt_dif_coleta_w		    number(14,4);
qt_valor_abs_w		    number(14,4);
ie_precoce_w		      varchar2(1);
ds_mensagem_w		      varchar2(4000);
ds_fichas_w			      varchar2(4000);
nr_seq_instituicao_w	number(10);


Cursor C01 is
	select	nr_sequencia,		
			nvl(dt_coleta_ficha_f,sysdate) - nvl(dt_nascimento_f,sysdate)
	from	lote_ent_sec_ficha
	where	nr_seq_lote_sec = nr_seq_secretaria_p;		

Cursor C02 is
	SELECT	SUBSTR(obter_cd_material_exame_lab(b.nr_seq_exame_lab),1,20) ds_material_lab,		
			b.nr_seq_exame_lab nr_seq_exame_lab
	FROM  	pacote_procedimento a,
			proc_interno b
	WHERE	a.nr_seq_pacote = nr_seq_pacote_w
	AND		a.nr_seq_proc_interno = b.nr_sequencia
	AND		b.nr_seq_exame_lab IS NOT NULL
	UNION
	SELECT	SUBSTR(obter_cd_material_exame_lab(a.nr_seq_exame),1,20) ds_material_lab,		
			a.nr_seq_exame nr_seq_exame_lab
	FROM	lote_ent_sec_ficha_exam a,
			lote_ent_sec_ficha b
	WHERE	a.nr_seq_ficha = b.nr_sequencia
	AND		b.nr_seq_lote_sec = nr_seq_secretaria_p
	AND		a.nr_seq_exame IS NOT NULL
	and		a.ie_tipo_coleta is not null;

begin

ie_precoce_w := 'N';
ds_mensagem_w := '';
ds_fichas_w := '';

select	max(nr_seq_pacote),
		max(nr_seq_instituicao)
into	nr_seq_pacote_w,
		nr_seq_instituicao_w
from	lote_ent_secretaria
where	nr_sequencia = nr_seq_secretaria_p;

open c01;
loop
fetch c01 into 	nr_seq_ficha_w, 
				qt_dif_coleta_w;
	exit when c01%notfound;
	begin

	open c02;
	loop
	fetch c02 into 	cd_material_exame_w, 
					nr_seq_exame_lab_w;
	exit when c02%notfound;
		begin		

		select	max(a.qt_unidade),
				max(a.ie_tipo_data)
		into	qt_unidade_w,
				ie_tipo_data_w		
		from	lote_ent_tipo_coleta a
		where	a.nr_seq_exame = nr_seq_exame_lab_w
		and		ie_tipo_coleta = 'P'
		AND		((a.nr_seq_instituicao = nr_seq_instituicao_w) OR (NVL(nr_seq_instituicao_w,0) = 0))
		and		nvl(a.ie_situacao,'A') = 'A';
				
		if	(ie_tipo_data_w is not null) then

			if	(ie_tipo_data_w = 'H') then
				qt_valor_abs_w	:= qt_dif_coleta_w * 24;
			else
				qt_valor_abs_w	:= ROUND(qt_dif_coleta_w);
			end if;
			
			if	(qt_valor_abs_w <= qt_unidade_w) then
				ie_precoce_w	:= 'P';
				
				select	a.nr_sequencia
				into	nr_seq_ficha_ww
				from	lote_ent_sec_ficha a
				where	a.nr_seq_lote_sec = nr_seq_secretaria_p
				and exists (select 1 from lote_ent_sec_ficha_exam x 
							where x.nr_seq_exame = nr_seq_exame_lab_w 
							and x.nr_seq_ficha = a.nr_sequencia
							and x.ie_tipo_coleta = 'P');
				
				ds_fichas_w := ds_fichas_w||','||nr_seq_ficha_ww;
				
			end if;
		end if;
		
		select	max(a.qt_unidade),
				max(a.ie_tipo_data)
		into	qt_unidade_w,
				ie_tipo_data_w		
		from	lote_ent_tipo_coleta a
		where	a.nr_seq_exame = nr_seq_exame_lab_w
		AND		((a.nr_seq_instituicao = nr_seq_instituicao_w) OR (NVL(nr_seq_instituicao_w,0) = 0))
		and		ie_tipo_coleta = 'T'
		and		nvl(a.ie_situacao,'A') = 'A';
		
		if	(ie_tipo_data_w is not null) then

			if	(ie_tipo_data_w = 'H') then
				qt_valor_abs_w	:= qt_dif_coleta_w * 24;
			else
				qt_valor_abs_w	:= ROUND(qt_dif_coleta_w);
			end if;
			
			if	(qt_valor_abs_w >= qt_unidade_w) then
				ie_precoce_w	:= 'T';
				
				select	a.nr_sequencia
				into	nr_seq_ficha_ww
				from	lote_ent_sec_ficha a
				where	a.nr_seq_lote_sec = nr_seq_secretaria_p
				and exists (select 1 from lote_ent_sec_ficha_exam x 
							where x.nr_seq_exame = nr_seq_exame_lab_w 
							and x.nr_seq_ficha = a.nr_sequencia
							and	x.ie_tipo_coleta = 'T');
				
				ds_fichas_w := ds_fichas_w||','||nr_seq_ficha_ww;

			end if;
		end if;

		if (ie_precoce_w <> 'N') then
			ds_mensagem_w := substr(wheb_mensagem_pck.get_texto(805151,'DS_FICHA='||substr(ds_fichas_w,2,length(ds_fichas_w))||';'),1,2000);
		end if;
		
		end;
	end loop;
	close c02;
	
	end;

end loop;
close c01;

return 	ds_mensagem_w;

end lab_verifica_tipo_coleta;
/