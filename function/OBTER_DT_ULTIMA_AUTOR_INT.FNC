create or replace
function OBTER_DT_ULTIMA_AUTOR_INT(nr_atendimento_p	number)
				return date is
	
dt_retorno_w	date;
	
begin

--Primeiro verifica se tem alguma prorroga��o que foi autorizado por �ltimo.
select	max(a.dt_fim_vigencia)
into	dt_retorno_w
from	autorizacao_convenio a,
	estagio_autorizacao b
where	a.nr_seq_estagio	= b.nr_sequencia
and	a.nr_atendimento	= nr_atendimento_p
and	b.ie_interno		= 10 --Somente se autorizado
and	a.ie_tipo_autorizacao	= '2';

--Se n�o encontrou, verifica a interna��o.
if	(dt_retorno_w is null) then
	select	max(a.dt_fim_vigencia)
	into	dt_retorno_w
	from	autorizacao_convenio a,
		estagio_autorizacao b
	where	a.nr_seq_estagio	= b.nr_sequencia
	and	a.nr_atendimento	= nr_atendimento_p
	and	b.ie_interno		= 10 --Somente se autorizado
	and	a.ie_tipo_autorizacao	= '1';
end if;

return dt_retorno_w;

end OBTER_DT_ULTIMA_AUTOR_INT;
/