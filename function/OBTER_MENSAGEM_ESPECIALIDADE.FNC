create or replace
function obter_mensagem_especialidade( cd_especialidade_p	varchar2,
									ie_finalidade_p varchar2 default 'B')

return varchar2 is

ds_mensagem_w	especialidade_mensagem.ds_mensagem%type;
ds_retorno_w	varchar2(2000);

begin

select	max(ds_mensagem)
into	ds_mensagem_w
from	especialidade_mensagem
where	cd_especialidade = cd_especialidade_p
and		ie_finalidade = ie_finalidade_p
and		ie_situacao = 'A';

if (ds_mensagem_w is not null) then
	ds_retorno_w	:= 	replace(ds_mensagem_w, '@DS_ESPECIALIDADE', obter_ds_especialidade(cd_especialidade_p));
end if;	

return	ds_retorno_w;

end obter_mensagem_especialidade;
/
