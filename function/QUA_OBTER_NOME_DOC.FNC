create or replace 
FUNCTION qua_obter_nome_doc(	nr_sequencia_p	Number)
				return varchar2 is

ds_retorno_w		qua_documento.nm_documento%type;

begin

if	(nr_sequencia_p is not null) then
	begin
	select	nm_documento
	into	ds_retorno_w
	from	qua_documento
	where	nr_sequencia	= nr_sequencia_p;
	exception when others then
		ds_retorno_w	:= '';
	end;
end if;

return ds_retorno_w;

end qua_obter_nome_doc;
/