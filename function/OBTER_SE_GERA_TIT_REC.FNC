create or replace
function OBTER_SE_GERA_TIT_REC (cd_convenio_p		number,
				cd_estabelecimento_p	number,
				ie_tipo_atendimento_p	number) return varchar2 is

ie_gerar_w	varchar2(10);

cursor c01 is
select	'S'
from	regra_gerar_tit_rec a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	nvl(a.cd_convenio, nvl(cd_convenio_p,0))			= nvl(cd_convenio_p,0)
and	nvl(a.ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0))	= nvl(ie_tipo_atendimento_p,0);

begin

open c01;
loop
fetch c01 into
	ie_gerar_w;
exit when c01%notfound;
	ie_gerar_w	:= ie_gerar_w;
end loop;
close c01;

return nvl(ie_gerar_w, 'N');

end OBTER_SE_GERA_TIT_REC;
/
