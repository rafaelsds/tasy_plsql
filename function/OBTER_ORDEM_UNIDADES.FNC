create or replace
function obter_ordem_unidades	(	cd_setor_atendimento_p	Number,
					cd_unidade_basica_p	varchar2,
					cd_unidade_compl_p	varchar2)
 		    	return Number is

nr_seq_apres_unid_w	number(15);
			
begin

select	nvl(max(nr_seq_apresent),999)
into	nr_seq_apres_unid_w
from	unidade_atendimento	
where	cd_setor_atendimento = cd_setor_atendimento_p
and	cd_unidade_basica    = cd_unidade_basica_p
and	cd_unidade_compl     = cd_unidade_compl_p;

return	nr_seq_apres_unid_w;

end obter_ordem_unidades;
/