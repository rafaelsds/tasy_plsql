create or replace
function obter_qtde_kit_mat_prescr(	cd_material_p		number,
					cd_estabelecimento_p	number,
					ie_tipo_atendimento_p	number,
					cd_setor_atendimento_p	number)
 		    	return number is

qt_kit_w	number(10):=0;

begin

select	count(*)
into	qt_kit_w
from	kit_mat_prescricao
where	cd_material		= cd_material_p
and	cd_estabelecimento	= cd_estabelecimento_p
and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_p)	= ie_tipo_atendimento_p
and	nvl(cd_setor_atendimento,cd_setor_atendimento_p)= cd_setor_atendimento_p
order by 	nvl(ie_tipo_atendimento,0),
		nvl(cd_setor_atendimento,0);

return	qt_kit_w;

end obter_qtde_kit_mat_prescr;
/