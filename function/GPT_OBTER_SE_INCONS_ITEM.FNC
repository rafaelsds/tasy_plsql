create or replace
function gpt_obter_se_incons_item(nr_seq_cpoe_p	number,
								nm_tabela_p		varchar2)
								return varchar2 is
								
ie_inconsistencia_w		varchar2(1)	:= 'N';

begin



if	(nm_tabela_p = 'CPOE_MATERIAL') then
	
SELECT   decode(count(1),0,'N','S')
into	ie_inconsistencia_w
FROM	 prescr_material_incon_farm a
WHERE    a.nr_prescricao  = (SELECT  max(a.nr_prescricao)
								 FROM  prescr_medica a, prescr_material b
								 WHERE  b.nr_prescricao = a.nr_prescricao
								 AND 		  b.nr_seq_mat_cpoe = nr_seq_cpoe_p
								 AND 		  a.dt_suspensao IS  NULL
								 AND 		  a.dt_liberacao IS NOT NULL								 
								 AND          a.cd_funcao_origem NOT IN (924,950)
								 AND		  b.ie_agrupador in (1,5))
AND  nr_seq_material IN (SELECT x.nr_sequencia
FROM	prescr_material x
WHERE x.nr_prescricao = a.nr_prescricao 
AND	x.ie_agrupador in (1,5) 
AND	x.nr_seq_mat_cpoe = nr_seq_cpoe_p)
AND a.ie_situacao = 'A'; 				
					
	
elsif	(nm_tabela_p = 'SOLUCAO') then
	
SELECT  decode(count(1),0,'N','S')
into	ie_inconsistencia_w
FROM	 prescr_material_incon_farm a
WHERE  a.nr_prescricao  = (SELECT  max(a.nr_prescricao)
								 FROM  prescr_medica a, prescr_material b
								 WHERE  b.nr_prescricao = a.nr_prescricao
								 AND 		  b.nr_seq_mat_cpoe = nr_seq_cpoe_p
								 AND 		  a.dt_suspensao IS  NULL
								 AND 		  a.dt_liberacao IS NOT NULL								 
								 AND          a.cd_funcao_origem NOT IN (924,950)
								 AND		  b.ie_agrupador = 4)
AND  nr_seq_solucao IN (SELECT x.nr_sequencia_solucao
FROM	prescr_material x
WHERE x.nr_prescricao = a.nr_prescricao 
AND	x.ie_agrupador = 4 
AND	x.nr_seq_mat_cpoe = nr_seq_cpoe_p)
AND a.ie_situacao = 'A'; 			
	
end if;

return	ie_inconsistencia_w;

end gpt_obter_se_incons_item;
/
