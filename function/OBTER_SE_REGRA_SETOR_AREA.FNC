create or replace
function obter_se_regra_setor_area(cd_setor_atendimento_p number,
									nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1);
qt_registros_w		number(10);
			
begin
ds_retorno_w := 'S';

if 	(nr_sequencia_p is not null) and
	(cd_setor_atendimento_p is not null) then

	select 		 count(*) 
	into		 qt_registros_w
	from 		 adep_area_prep_setor 
	where 		 nr_seq_area = nr_sequencia_p;
	
	if (qt_registros_w > 0) then
	
		select 	nvl(max('S'),'N')
		into	ds_retorno_w
		from	adep_area_prep_setor
		where	nr_seq_area	=	nr_sequencia_p
		and		cd_setor_atendimento = 	cd_setor_atendimento_p;
	
	else
		ds_retorno_w := 'S';
	end if;
end if;

return	ds_retorno_w;

end obter_se_regra_setor_area;
/