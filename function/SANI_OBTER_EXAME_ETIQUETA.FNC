create or replace
function sani_obter_exame_etiqueta (nr_prescricao_p		number,
									nr_seq_material_p   number,
									nr_seq_grupo_p		number,
									nr_seq_frasco_p 	number,
									nr_seq_prescr_proc_mac_p number,
									nr_amostra_p 		number,
									qt_exames_p			number,
									ie_tipo_p			varchar2)
return varchar2 is				
ds_exames_w 				varchar2(32000);
cd_exame_w					varchar2(20);
nr_seq_exame_w				number;
nr_menor_prioridade_w		number;
qt_exames_w					number;
nr_seq_exame_sup_w			number;
qt_exames_imp_w				number;
nr_result_pag_w				number;

cursor C01 is
	select 	distinct 
			a.nr_seq_exame		
	from   	exame_laboratorio a,
			prescr_procedimento b,       
			exame_lab_frasco c,
			material_exame_lab d,
			prescr_proc_mat_item f,
			exame_lab_material 	e
	where  	a.nr_seq_exame = b.nr_seq_exame
	and    	b.nr_prescricao  = nr_prescricao_p
	and    	d.nr_sequencia = nr_seq_material_p
	and    	b.cd_material_exame  = d.cd_material_exame
	and		e.nr_seq_exame =  a.nr_seq_exame
	and		e.nr_seq_material = d.nr_sequencia
	and    	c.nr_seq_material(+) = e.nr_seq_material
	and		f.nr_prescricao	= b.nr_prescricao
	and		f.nr_seq_prescr 	= b.nr_sequencia
	and		nvl(c.ie_situacao,'A') = 'A'
	and		f.NR_SEQ_PRESCR_PROC_MAT = nr_seq_prescr_proc_mac_p
	and    	c.nr_seq_exame(+)	= e.nr_seq_exame
	and		a.nr_seq_grupo	= nr_seq_grupo_p
	and    	nvl(c.nr_seq_frasco,0) 	= nvl(nr_seq_frasco_p,0);

begin	
	ds_exames_w := '';
	qt_exames_imp_w := 0;
	open C01;
		loop
		fetch C01 into
			nr_seq_exame_w;
		exit when c01%notfound;
		begin
			
			
			select count(*) 
			into   qt_exames_w
			from   exame_lab_frasco
			where  nr_seq_exame    = nr_seq_exame_w
			and	   nr_seq_material = nr_seq_material_p
			and	   nvl(ie_situacao,'A') = 'A';
			
			if (qt_exames_w > 0) then			
			
				select nvl(min(ie_prioridade),0)
				into   nr_menor_prioridade_w
				from   exame_lab_frasco
				where  nr_seq_exame    = nr_seq_exame_w
				and	   nr_seq_material = nr_seq_material_p
				and	   nvl(ie_situacao,'A') = 'A';
				
				if (nr_menor_prioridade_w > 0) then
				
					select count(*)
					into   qt_exames_w
					from   exame_lab_frasco
					where  nr_seq_exame    = nr_seq_exame_w
					and	   nr_seq_material = nr_seq_material_p
					and	   nr_seq_frasco   = nr_seq_frasco_p
					and	   ie_prioridade   = nr_menor_prioridade_w
					and	   nvl(ie_situacao,'A') = 'A';
					
				else
					qt_exames_w := 1;
				end if;
				
			else
				qt_exames_w := 1;
			end if;
					
			if (qt_exames_w > 0) then
				
				
				select nvl(max(cd_exame),'')
				into   cd_exame_w
				from   exame_laboratorio
				where  nr_seq_exame = nr_seq_exame_w
				and	   nvl(nr_ordem_amostra,1) = 1;
				
				if (cd_exame_w is null)  then
					
					qt_exames_w := 0;
					
					-- verificar se � analito.
					select nvl(max(nr_seq_superior),0)
					into nr_seq_exame_sup_w
					from exame_laboratorio
					where nr_seq_exame = nr_seq_exame_w;
				
					
					-- verificar se exame pai n�o � solicitado
					if (nr_seq_exame_sup_w > 0) then					
					
						select count(*)
						into   qt_exames_w  
						from   exame_laboratorio
						where  nr_seq_exame = nr_seq_exame_sup_w 
						and    ie_solicitacao = 'N';
					end if;
					
					select nvl(max(cd_exame),'')
					into   cd_exame_w
					from   exame_laboratorio
					where  ((nr_seq_superior = nr_seq_exame_w)  or ((nr_seq_exame_w = nr_seq_exame) and(qt_exames_w > 0)))
					and	   nr_ordem_amostra = nr_amostra_p;
					
				end if;
				
				if (cd_exame_w is not null) then					
					nr_result_pag_w := trunc(qt_exames_imp_w/qt_exames_p);
					
					if (((ie_tipo_p = '1') and (qt_exames_imp_w < qt_exames_p))  or
					    ((ie_tipo_p = '2') and (qt_exames_imp_w >= qt_exames_p) and (qt_exames_imp_w < (qt_exames_p*2))) or 
						((ie_tipo_p = '3') and (qt_exames_imp_w >= (qt_exames_p*2)) and (qt_exames_imp_w < (qt_exames_p*3))) or
						((ie_tipo_p = '4') and (qt_exames_imp_w >= (qt_exames_p*3)) and (qt_exames_imp_w < (qt_exames_p*4)))) then
						ds_exames_w := ds_exames_w || cd_exame_w || ', ';
					end if;
					qt_exames_imp_w := qt_exames_imp_w +1;					
				end if;
			end if;
		end;	
		end loop;
	close C01;
	
	ds_exames_w := substr(ds_exames_w,1,length(ds_exames_w)-2);

return	ds_exames_w;
end sani_obter_exame_etiqueta;
/