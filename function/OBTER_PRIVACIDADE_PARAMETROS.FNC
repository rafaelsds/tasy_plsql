create or replace 
function obter_privacidade_parametros(ds_campo_p in varchar2) 
			return varchar2 is
			
ds_retorno_w		varchar2(200 char) := 'N';

begin

    if	(upper(ds_campo_p) = 'PPF') then
        begin    
            select	nvl(ie_privacidade_pf,'N')
            into	ds_retorno_w
            from	privacidade_parametros;
        exception
            when others then
             ds_retorno_w := 'N';
        end;
    elsif	(upper(ds_campo_p) = 'UC') then
        begin    
            select	nvl(ie_utiliza_captcha,'N')
            into	ds_retorno_w
            from	privacidade_parametros;
        exception
            when others then
             ds_retorno_w := 'N';
        end;
    elsif	(upper(ds_campo_p) = 'SP') then
        begin    
            select	nvl(ie_show_privacy,'N')
            into	ds_retorno_w
            from	privacidade_parametros;
        exception
            when others then
             ds_retorno_w := 'N';
        end;
    end if;

    return	ds_retorno_w;
	
end obter_privacidade_parametros;
/