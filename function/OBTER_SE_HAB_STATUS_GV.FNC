create or replace
function obter_Se_hab_status_gv	(	ie_status_p	varchar2)
 		    	return varchar2 is
ds_retorno_w		varchar2(1) := 'S';
ie_valor_param_w	varchar2(255);
begin
Obter_param_Usuario(1002, 136, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_valor_param_w);

if 	(ie_status_p is not null) and 
	(ie_valor_param_w is not null) then
	
	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	dual
	where	obter_se_contido_char(ie_status_p, ie_valor_param_w) = 'S';

end if;

return	ds_retorno_w;

end obter_Se_hab_status_gv;
/