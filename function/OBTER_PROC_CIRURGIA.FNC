CREATE OR REPLACE 
FUNCTION obter_proc_cirurgia	(nr_atendimento_p	number,
				ie_opcao_p		varchar)
				RETURN VARCHAR2 IS

/* IE_OPCAO_P

C	- C�digo
D	- Descri��o
*/

cd_procedimento_princ_w	NUMBER(15,0);
ds_procedimento_w	PROCEDIMENTO.DS_PROCEDIMENTO%type;
ds_retorno_w		VARCHAR2(300);

BEGIN

SELECT	max(a.cd_procedimento_princ),
	max(b.ds_procedimento)
INTO	cd_procedimento_princ_w,
	ds_procedimento_w
FROM	procedimento b,
	cirurgia a
WHERE	a.nr_atendimento	= nr_atendimento_p
AND	a.cd_procedimento_princ	= b.cd_procedimento
AND	a.ie_origem_proced	= b.ie_origem_proced;

IF	(ie_opcao_p		= 'C') THEN
	ds_retorno_w		:= cd_procedimento_princ_w;
ELSIF	(ie_opcao_p		= 'D') THEN
	ds_retorno_w		:= ds_procedimento_w;
END IF;

RETURN	ds_retorno_w;

END	obter_proc_cirurgia;
/