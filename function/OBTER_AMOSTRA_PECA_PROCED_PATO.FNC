create or replace
function obter_amostra_peca_proced_pato  ( cd_procedimento_p	number, 
					   ie_origem_proced_p	number)
 		    	return number is
			
nr_seq_amostra_peca_w	number(10);

begin


select	max(nr_seq_tipo_amostra)
into	nr_seq_amostra_peca_w
from	tipo_amostra_vinculo_proc
where	cd_procedimento = cd_procedimento_p
and	ie_origem_proced = ie_origem_proced_p;

return	nr_seq_amostra_peca_w;

end obter_amostra_peca_proced_pato;
/