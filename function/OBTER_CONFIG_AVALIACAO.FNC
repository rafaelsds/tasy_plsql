create or replace 
FUNCTION obter_config_avaliacao(
		 nr_seq_avaliacao_p	NUMBER)
		 return varchar2 is

cd_item_w	VARCHAR2(50);
vl_item_w	VARCHAR2(50);
vl_config_w	VARCHAR2(4000);


CURSOR C01 IS
	SELECT	CD_ITEM,                
		VL_ITEM                	
	FROM	Med_aval_config
	WHERE	NR_SEQ_TIPO_AVAL  = nr_seq_avaliacao_p;		
BEGIN
	OPEN C01;
	LOOP
	FETCH C01 INTO
		cd_item_w,
		vl_item_w;
	EXIT WHEN C01%NOTFOUND;		
		BEGIN				
		vl_config_w := vl_config_w || cd_item_w ||'='|| vl_item_w|| ';';
		END;
	END LOOP;
	CLOSE C01;

return vl_config_w;
END obter_config_avaliacao;
/