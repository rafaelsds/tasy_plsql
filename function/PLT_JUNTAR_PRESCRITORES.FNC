create or replace
function plt_juntar_prescritores	(	nm_prescritores_p	varchar2,
						nm_prescritor_p		varchar2)
					return varchar2 is

nm_prescritores_w	varchar2(2000);

begin
nm_prescritores_w := nm_prescritores_p;

if	(nm_prescritores_w is null) then
	nm_prescritores_w := nm_prescritor_p;
elsif	(obter_se_contido_char(' '||nm_prescritor_p, '( ' || nm_prescritores_w || ')') = 'N') then
	nm_prescritores_w := nm_prescritores_w || ', ' || nm_prescritor_p;
end if;

return nm_prescritores_w;

end plt_juntar_prescritores;
/
