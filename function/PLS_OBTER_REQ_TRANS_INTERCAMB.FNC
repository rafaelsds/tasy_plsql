create or replace
function pls_obter_req_trans_intercamb
			(	nr_seq_transacao_p	Number,
				nr_seq_requisicao_p	Number,
				ie_tipo_transacao_p	Varchar2)
				return Number is

nr_seq_retorno_w		Number(10);

Cursor C01 is
	select	nr_seq_requisicao
	from	ptu_pedido_autorizacao
	where	nr_seq_execucao	= nr_seq_transacao_p
	union
	select	nr_seq_requisicao
	from	ptu_pedido_compl_aut
	where	nr_seq_execucao	= nr_seq_transacao_p
	union
	select	nr_seq_requisicao
	from	ptu_requisicao_ordem_serv
	where	nr_seq_origem	= nr_seq_transacao_p;
	
Cursor C02 is
	select	nr_seq_execucao
	from	ptu_pedido_autorizacao
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	union
	select	nr_seq_execucao
	from	ptu_pedido_compl_aut
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	union
	select	nr_seq_origem
	from	ptu_requisicao_ordem_serv
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	
Cursor C03 is
	select	nr_seq_origem
	from	ptu_resposta_autorizacao
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	union
	select	nr_seq_origem
	from	ptu_resposta_auditoria
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	union
	select	nr_seq_origem
	from	ptu_confirmacao
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
begin

if	(nr_seq_transacao_p	is not null) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_retorno_w;
	exit when C01%notfound;
	end loop;
	close C01;
elsif	(nr_seq_requisicao_p	is not null) then
	if	(ie_tipo_transacao_p	= 'P') then
		open C02;
		loop
		fetch C02 into	
			nr_seq_retorno_w;
		exit when C02%notfound;
		end loop;
		close C02;
	elsif	(ie_tipo_transacao_p	= 'O') then
		open C03;
		loop
		fetch C03 into	
			nr_seq_retorno_w;
		exit when C03%notfound;
		end loop;
		close C03;
	end if;
end if;

return	nvl(nr_seq_retorno_w,0);

end pls_obter_req_trans_intercamb;
/