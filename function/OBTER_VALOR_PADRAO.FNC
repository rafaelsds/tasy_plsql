create or replace
function Obter_Valor_Padrao
		(nm_tabela_p		varchar2,
		nm_atributo_p		varchar2)
		return	varchar2 is

vl_padrao_w	varchar2(50);

begin

select	max(vl_default)
into	vl_padrao_w
from	tabela_atributo
where	nm_tabela	= UPPER(nm_tabela_p)
and	nm_atributo	= UPPER(nm_atributo_p);

return	vl_padrao_w;

end Obter_Valor_Padrao;
/