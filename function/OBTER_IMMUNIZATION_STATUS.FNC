create or replace function obter_immunization_status(nr_sequencia_p  	number , nr_seq_vac_modelo_p number default null)
 		    	return number is

ie_retorno_w		number;
ie_executed_w    varchar2(1);
dt_vacina_w  date ;
dt_prevista_execucao_w date;
ie_dose_w varchar2(2);
dt_liberacao_w date;
ie_localidade_inf_w varchar2(1);
dt_cancelamento_w date;
DT_INATIVACAO_w date;
IE_SITUACAO_w varchar2(1);

ano_1_w number(10);
ano_2_w number(10);

qt_ano_w number(10);

begin

ie_retorno_w := 2;

if (nr_sequencia_p is not null) then

select ie_executado ,dt_vacina,dt_prevista_execucao,ie_dose,dt_liberacao,ie_localidade_inf ,dt_cancelamento,DT_INATIVACAO, ie_situacao
into ie_executed_w,dt_vacina_w,dt_prevista_execucao_w,ie_dose_w ,dt_liberacao_w,ie_localidade_inf_w ,dt_cancelamento_w,DT_INATIVACAO_w, IE_SITUACAO_w
from paciente_vacina where nr_sequencia = nr_sequencia_p;
 
 ano_1_w := PKG_DATE_UTILS.extract_field('YEAR',dt_vacina_w);
 ano_2_w := PKG_DATE_UTILS.extract_field('YEAR',dt_prevista_execucao_w);
 
 if(nr_seq_vac_modelo_p is not null and nr_seq_vac_modelo_p <> 0) then
     select max(qt_anos_prim_dose)
     into qt_ano_w
     from vacinas_modelo where nr_sequencia = nr_seq_vac_modelo_p
     and nvl(ie_dose_inicio, '1D') = ie_dose_w;
 end if;

if (ie_executed_w = 'S' and (qt_ano_w is not null and ano_1_w <= ano_2_w or dt_vacina_w is not null and dt_prevista_execucao_w is not null and PKG_DATE_UTILS.get_Date(1,dt_vacina_w) <= PKG_DATE_UTILS.get_Date(1,dt_prevista_execucao_w))) then
    ie_retorno_w := 5;
    
elsif (ie_executed_w = 'S' and dt_vacina_w > dt_prevista_execucao_w ) then 

    ie_retorno_w := 6;
    
elsif (dt_vacina_w is null and  dt_prevista_execucao_w is not null and dt_prevista_execucao_w < sysdate) then
  
    ie_retorno_w := 3 ;
  
elsif ((dt_vacina_w is null and  dt_prevista_execucao_w is not null and dt_prevista_execucao_w >= sysdate) or (dt_vacina_w is null and  dt_prevista_execucao_w is null)  ) then

    ie_retorno_w := 2;
    
elsif (dt_liberacao_w is not null) then
    ie_retorno_w := 7 ;

end if;

  if (IE_SITUACAO_w = 'I' and  DT_INATIVACAO_w is not NULL) then
       ie_retorno_w := 8 ;
  elsif (dt_cancelamento_w is not null) then
      ie_retorno_w := 4 ;
  end if;
end if;
return	ie_retorno_w;

end obter_immunization_status;
/
