create or replace
function obter_acao_procedimento(	nr_sequencia_p	number,
					nr_seq_dispositivo_p	number,
					ie_opcao_p		varchar2 default 'P')
					return varchar2 is

ie_acao_w	varchar2(15);

begin

if	((nr_sequencia_p > 0) and (nr_seq_dispositivo_p > 0)) and
	(ie_opcao_p	= 'P') then
	begin
	select	max(ie_acao)
	into	ie_acao_w
	from	dispositivo_proc_interno
	where	nr_seq_proc_interno	=	nr_sequencia_p
	and	nr_seq_dispositivo	=	nr_seq_dispositivo_p;
	exception
	when others then
		ie_acao_w := null;
	end;
	
elsif	((nr_sequencia_p > 0) and (nr_seq_dispositivo_p > 0)) and
	(ie_opcao_p	= 'SAE') then
	begin
	select	max(ie_acao)
	into	ie_acao_w
	from	PE_PROC_DISPOSITIVO
	where	nr_seq_proc		=	nr_sequencia_p
	and	nr_seq_dispositivo	=	nr_seq_dispositivo_p;
	exception
	when others then
		ie_acao_w := null;
	end;
end if;

return	ie_acao_w;

end obter_acao_procedimento;
/ 
