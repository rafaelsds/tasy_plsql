create or replace 
function Obter_Estab_Conta_Paciente(nr_interno_conta_p	number) return number is

cd_estabelecimento_w	number(5);

begin
begin

select 	cd_estabelecimento
into	cd_estabelecimento_w
from 	conta_paciente
where 	nr_interno_conta = nr_interno_conta_p;

exception
when others then
	cd_estabelecimento_w := null;
end;

return cd_estabelecimento_w;

end;
/
