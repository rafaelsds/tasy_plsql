create or replace
function Obter_desc_motivo_devol(nr_seq_motivo_devol_p	number)
 		    	return varchar2 is

ds_motivo_devolucao_w		varchar2(80):= '';
			
begin

select 	max(ds_motivo_devolucao)
into	ds_motivo_devolucao_w
from 	fatur_motivo_devol
where 	nr_sequencia = nr_seq_motivo_devol_p;

return	ds_motivo_devolucao_w;

end Obter_desc_motivo_devol;
/