create or replace
function obter_se_proced_repasse_valido	(nr_interno_conta_p		number,
					nr_seq_criterio_p			number,
					nr_seq_proc_princ_p		number,
					nr_atendimento_p			number)
					return varchar2 is
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
qt_proced_w		number(5);
ie_criterio_valido_w	varchar2(1);

cd_area_proc_regra_w           mat_criterio_rep_proc.cd_area_procedimento%type;
cd_especialidade_regra_w     mat_criterio_rep_proc.cd_especialidade_proc%type;
cd_grupo_regra_w                  mat_criterio_rep_proc.cd_grupo_proc%type;
ie_proc_princ_w		varchar2(255);
cd_medico_executor_w	varchar2(255);
nr_atendimento_w	number(10);

/*
'S' - Crit�rio V�lido
'N' - Crit�rio inv�lido
*/
cursor c01 is
	select 	cd_procedimento,
		ie_origem_proced,
		cd_area_procedimento,
		cd_especialidade_proc,
		cd_grupo_proc,
		nvl(ie_proc_princ, 'N'),
		cd_medico_executor
	from	mat_criterio_rep_proc
	where	nr_seq_criterio = nr_seq_criterio_p;
begin

ie_criterio_valido_w	:= 'S';

OPEN C01;
LOOP
FETCH C01 into
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_area_proc_regra_w,
	cd_especialidade_regra_w,
	cd_grupo_regra_w,
	ie_proc_princ_w,
	cd_medico_executor_w;
exit when c01%notfound;
	begin
	ie_criterio_valido_w	:= 'S';
	if (ie_criterio_valido_w = 'S') then

		if	(cd_procedimento_w is not null) and
			(ie_origem_proced_w is not null) then
			
			select 	count(*)
			into	qt_proced_w
			from 	procedimento_paciente
			where	cd_procedimento	 	= cd_procedimento_w
			and	ie_origem_proced		= ie_origem_proced_w
			and	nr_atendimento		= nr_atendimento_p
			and	cd_motivo_exc_conta	is null
			and	nr_sequencia		= decode(ie_proc_princ_w, 'S', nr_seq_proc_princ_p, nr_sequencia)
			and	nvl(cd_medico_executor, 'X')	= nvl(cd_medico_executor_w, nvl(cd_medico_executor, 'X'));
			
			if 	(qt_proced_w = 0) then
				ie_criterio_valido_w := 'N';
			end if;
		else
			select 	count(*)
			into	qt_proced_w
			from 	estrutura_procedimento_v b,
				procedimento_paciente a
			where	a.cd_procedimento		= b.cd_procedimento
			and	a.ie_origem_proced	= b.ie_origem_proced
			and	a.nr_atendimento		= nr_atendimento_p
			and	nvl(cd_area_proc_regra_w,b.cd_area_procedimento)	= b.cd_area_procedimento
			and	nvl(cd_especialidade_regra_w,b.cd_especialidade)	= b.cd_especialidade
			and	nvl(cd_grupo_regra_w,b.cd_grupo_proc)		= b.cd_grupo_proc
			and	a.cd_motivo_exc_conta	is null
			and	a.nr_sequencia					= decode(ie_proc_princ_w, 'S', nr_seq_proc_princ_p, a.nr_sequencia)
			and	nvl(cd_medico_executor, 'X')	= nvl(cd_medico_executor_w, nvl(cd_medico_executor, 'X'));

			if	(qt_proced_w = 0) then
				ie_criterio_valido_w := 'N';
			end if;
		end if;
		
		if(ie_criterio_valido_w = 'S') then
			exit;
		end if;
	end if;

	end;
END LOOP;
CLOSE C01;

return ie_criterio_valido_w;

end;
/