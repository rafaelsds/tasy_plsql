create or replace 
function sup_obter_estab_planej_compras(
			nr_seq_planejamento_p	number)
		return number is

cd_estabelecimento_w		number(5);

begin

select	nvl(max(cd_estabelecimento),null)
into	cd_estabelecimento_w
from	sup_planejamento_compras
where	nr_sequencia	= nr_seq_planejamento_p;

return cd_estabelecimento_w;

end sup_obter_estab_planej_compras;
/