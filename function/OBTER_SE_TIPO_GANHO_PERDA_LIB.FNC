create or replace
function Obter_se_tipo_ganho_perda_lib(	nr_seq_tipo_pg_p	number,
					nr_atendimento_p	number default null)
				return		varchar2 is

qt_itens_w	number(10);
ds_retorno_w	varchar2(10);
ie_permite_w	varchar2(1);
cd_setor_atendimento_w	number(10);
cd_estabelecimento_w	number(10);
cd_perfil_w				number(10)   := Obter_perfil_ativo;


Cursor C01 is
	select 	l.ie_permite
	from 	TIPO_PERDA_GANHO t,
		grupo_perda_ganho g,
		grupo_perda_ganho_lib l    
	where 	t.nr_sequencia = nr_seq_tipo_pg_p
	and   	t.nr_seq_grupo = g.nr_sequencia
	and   	g.nr_sequencia = l.nr_seq_grupo
	and   	nvl(l.cd_setor_atendimento,cd_setor_atendimento_w) = cd_setor_atendimento_w
	and		nvl(l.cd_estabelecimento,cd_estabelecimento_w)	= cd_estabelecimento_w
	and		nvl(l.cd_perfil,cd_perfil_w) = cd_perfil_w
	order	by nvl(l.cd_setor_atendimento,0),
				nvl(l.cd_estabelecimento,0);
				
				
				
Cursor C02 is
	select	nvl(ie_permite,'S')
	from	tipo_perda_ganho_regra
	where	nr_seq_tipo_pg	= nr_seq_tipo_pg_p
	and	nvl(cd_estabelecimento,cd_estabelecimento_w)	= cd_estabelecimento_w
	 order by nvl(cd_estabelecimento,0);


begin

select	count(*)
into	qt_itens_w
from	tipo_perda_ganho_regra
where	nr_seq_tipo_pg	= nr_seq_tipo_pg_p;

cd_estabelecimento_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

if	(qt_itens_w = 0) then
	ds_retorno_w  :='S';
else
	ds_retorno_w	:= 'N';
	open C02;
	loop
	fetch C02 into	
		ds_retorno_w;
	exit when C02%notfound;
	end loop;
	close C02;
	
end if;

if	(nr_atendimento_p	is not null) and
	(ds_retorno_w	= 'S') then
	select	count(*)
	into	qt_itens_w
	from	atend_perda_ganho_controle
	where	nr_atendimento	= nr_atendimento_p
	and	IE_REGISTRA_PG	= 'S';
	
	if	(qt_itens_w	> 0) then
		select	count(*)
		into	qt_itens_w
		from	atend_perda_ganho_controle
		where	nr_atendimento	= nr_atendimento_p
		and	nr_seq_tipo	= nr_seq_tipo_pg_p
		and	IE_REGISTRA_PG	= 'S';
		
		if	(qt_itens_w	= 0) then
			ds_retorno_w	:= 'N';
		end if;
		
	end if;
	
	cd_setor_atendimento_w := Obter_Setor_Atendimento(nr_atendimento_p);
	
	select 	count(*)
	into	qt_itens_w
	from 	tipo_perda_ganho t,
		grupo_perda_ganho g,
		grupo_perda_ganho_lib l    
	where 	t.nr_sequencia = nr_seq_tipo_pg_p
	and   	t.nr_seq_grupo = g.nr_sequencia
	and   	g.nr_sequencia = l.nr_seq_grupo;

	if	(qt_itens_w	> 0) and
		(ds_retorno_w	= 'S')then
		begin
		
		open C01;
		loop
		fetch C01 into	
			ie_permite_w;
		exit when C01%notfound;
			begin
			ds_retorno_w :=  ie_permite_w;
			end;
		end loop;
		close C01;
		
		
		end;
	end if;
	
end if;
	
return ds_retorno_w;	
	
end Obter_se_tipo_ganho_perda_lib;
/
