CREATE OR REPLACE FUNCTION TRE_CONSISTIR_HORA_TREINAMENTO(nr_seq_evento_p		number,
							     nr_seq_modulo_p		number,
							     dt_treinamento_p		date,
							     cd_estabelecimento_p	number,
							     ie_opcao_p			varchar2)
RETURN VARCHAR2 IS

/* ie_opcao_p:
		HI - Hora inicial;
		HF - Hora final;
		CH - Carga hor�ria di�ria;
*/

type tre is record (	dt_inicial	date,
			dt_final	date,
			qt_carga_dia	number);
type vetor is table of tre index by binary_integer;
treinamento_w			Vetor;
i				Integer := 0;

dt_inicio_treinamento_w		Date;
dt_termino_treinamento_w	Date;
qt_carga_horaria_treinamento_w	Number(13,2);
nr_seq_curso_w			Number;
qt_data_treinamento_w		Number := 0;
dt_treinamento_w		Date;
ie_data_treinamento_w		Varchar2(30);
hr_inicial_w			date;
hr_final_w			date;
qt_carga_total_w		Number := 0;
qt_carga_diaria_w		Number := 0;
qt_carga_diaria_ww		Number := 0;
x				Number := 0;
y				Number := 0;
z				Number := -1;

Cursor C01 IS
select	dt_dia,
	   pkg_date_utils.get_WeekDay(dt_dia)
from   dia_v
where  trunc(dt_dia) between trunc(dt_inicio_treinamento_w) and trunc(((dt_inicio_treinamento_w-1) + Round((qt_carga_horaria_treinamento_w/qt_carga_diaria_ww))))
and	   obter_se_feriado(cd_estabelecimento_p,dt_dia) = 0
and	   pkg_date_utils.get_WeekDay(dt_dia) <> 1;

BEGIN

/* Busca a data inicial e final do treinamento */
if	((nr_seq_modulo_p is null) or
	(nr_seq_modulo_p = 0)) then
	Select	nvl(a.dt_inicio_real,a.dt_inicio),
		nvl(a.dt_fim_real,a.dt_fim) dt_termino,
		a.nr_seq_curso
	into	dt_inicio_treinamento_w,
		dt_termino_treinamento_w,
		nr_seq_curso_w
	from	tre_evento a
	where	a.nr_sequencia = nr_seq_evento_p;
else
	Select	b.dt_inicio,
		b.dt_termino,
		a.nr_seq_curso
	into	dt_inicio_treinamento_w,
		dt_termino_treinamento_w,
		nr_seq_curso_w
	from	tre_evento a,
		tre_evento_modulo b
	where	a.nr_sequencia = b.nr_seq_evento
	and	b.nr_seq_evento = nr_seq_evento_p
	and	b.nr_sequencia = nr_seq_modulo_p;
end if;

/*Busca a carga hor�ria total do treinamento*/
if	((nr_seq_modulo_p is null) or
	(nr_seq_modulo_p = 0)) then
	select	sum(nvl(qt_carga_horaria,0))
	into	qt_carga_horaria_treinamento_w
	from	tre_curso_modulo
	where	nr_seq_curso = nr_seq_curso_w;
else
	select	nvl(qt_carga_horaria,0)
	into	qt_carga_horaria_treinamento_w
	from	tre_evento_modulo
	where	nr_sequencia = nr_seq_modulo_p;
end if;

/*Busca a carga hor�ria di�ria*/
Select	((to_date(to_char(dt_termino_treinamento_w,'hh24:mi'),'hh24:mi')-to_date(to_char(dt_inicio_treinamento_w,'hh24:mi'),'hh24:mi'))*24)
into	qt_carga_diaria_w
from	dual;

if	(qt_carga_diaria_w >= 8) then
	qt_carga_diaria_ww	:= qt_carga_diaria_w-2;
else
	qt_carga_diaria_ww	:= qt_carga_diaria_w;
end if;

Open C01;
Loop
	fetch C01 into	dt_treinamento_w,
			ie_data_treinamento_w;
	Exit when C01%notfound;

	if	(to_char(dt_inicio_treinamento_w,'mi') = '00') then
		dt_treinamento_w	:= trunc(dt_treinamento_w)+to_number(to_char(dt_inicio_treinamento_w,'hh24'))/24;
	elsif	(to_char(dt_inicio_treinamento_w,'mi') = '30') then
		dt_treinamento_w	:= trunc(dt_treinamento_w)+(to_number(to_char(dt_inicio_treinamento_w,'hh24'))/24)+0.5/24;
	end if;

	treinamento_w(i).dt_inicial	:= dt_treinamento_w;
	if	(ie_data_treinamento_w <> '7') then
		if	((qt_carga_horaria_treinamento_w - qt_carga_total_w) >= qt_carga_diaria_ww) then
			qt_carga_total_w		:= qt_carga_total_w + qt_carga_diaria_ww;
			treinamento_w(i).dt_final	:= dt_treinamento_w+(qt_carga_diaria_w)/24;
			treinamento_w(i).qt_carga_dia	:= qt_carga_diaria_ww;
		else
			if	((qt_carga_horaria_treinamento_w - qt_carga_total_w) > (qt_carga_diaria_ww/2)) then
				qt_carga_total_w		:= qt_carga_total_w + (qt_carga_horaria_treinamento_w - qt_carga_total_w);
				treinamento_w(i).dt_final	:= dt_treinamento_w+((qt_carga_horaria_treinamento_w - qt_carga_total_w)+2)/24;
				treinamento_w(i).qt_carga_dia	:= (qt_carga_horaria_treinamento_w - qt_carga_total_w)+2;
			else
				qt_carga_total_w		:= qt_carga_total_w + (qt_carga_horaria_treinamento_w - qt_carga_total_w);
				treinamento_w(i).dt_final	:= dt_treinamento_w+(qt_carga_horaria_treinamento_w - qt_carga_total_w)/24;
				treinamento_w(i).qt_carga_dia	:= (qt_carga_horaria_treinamento_w - qt_carga_total_w);
			end if;
		end if;
	else
		if	((qt_carga_horaria_treinamento_w - qt_carga_total_w) <= Round(qt_carga_diaria_ww/2)) then
			qt_carga_total_w		:= qt_carga_total_w + (qt_carga_horaria_treinamento_w - qt_carga_total_w);
			treinamento_w(i).dt_final	:= dt_treinamento_w+((qt_carga_horaria_treinamento_w - qt_carga_total_w)/24);
			treinamento_w(i).qt_carga_dia	:= (qt_carga_horaria_treinamento_w - qt_carga_total_w);
		else
			qt_carga_total_w		:= qt_carga_total_w + Round(qt_carga_diaria_ww/2);
			treinamento_w(i).dt_final	:= dt_treinamento_w+Round(qt_carga_diaria_ww/2)/24;
			treinamento_w(i).qt_carga_dia	:= Round(qt_carga_diaria_ww/2);
		end if;
	end if;
	if	(trunc(dt_treinamento_w) = trunc(dt_treinamento_p)) then
		z	:= i;
	end if;
	i	:= i+1;
end loop;
Close C01;

/*Verifica se faltou horas na carga hor�ria e adiciona o(s) dia(s) no registro*/
if	(qt_carga_total_w < qt_carga_horaria_treinamento_w) then
	for	x in y..(Round((qt_carga_horaria_treinamento_w - qt_carga_total_w)/qt_carga_diaria_w)) loop
		begin
		dt_treinamento_w		:= dt_treinamento_w + 1;
		if	((obter_se_feriado(cd_estabelecimento_p,dt_treinamento_w) > 0) or
			(pkg_date_utils.get_WeekDay(dt_treinamento_w) = 1)) then
			y := y-1;
			exit;
		end if;
		i	:= i+1;
		if	(to_char(dt_inicio_treinamento_w,'mi') = '00') then
			dt_treinamento_w	:= trunc(dt_treinamento_w)+to_number(to_char(dt_inicio_treinamento_w,'hh24'))/24;
		elsif	(to_char(dt_inicio_treinamento_w,'mi') = '30') then
			dt_treinamento_w	:= trunc(dt_treinamento_w)+(to_number(to_char(dt_inicio_treinamento_w,'hh24'))/24)+0.5/24;
		end if;

		/* Data final */
		if	(ie_data_treinamento_w <> '7') then
		if	((qt_carga_horaria_treinamento_w - qt_carga_total_w) >= qt_carga_diaria_ww) then
			qt_carga_total_w		:= qt_carga_total_w + qt_carga_diaria_ww;
			treinamento_w(i).dt_final	:= dt_treinamento_w+(qt_carga_diaria_w)/24;
			treinamento_w(i).qt_carga_dia	:= qt_carga_diaria_ww;
		else
			if	((qt_carga_horaria_treinamento_w - qt_carga_total_w) > (qt_carga_diaria_ww/2)) then
				qt_carga_total_w		:= qt_carga_total_w + (qt_carga_horaria_treinamento_w - qt_carga_total_w);
				treinamento_w(i).dt_final	:= dt_treinamento_w+((qt_carga_horaria_treinamento_w - qt_carga_total_w)+2)/24;
				treinamento_w(i).qt_carga_dia	:= (qt_carga_horaria_treinamento_w - qt_carga_total_w)+2;
			else
				qt_carga_total_w		:= qt_carga_total_w + (qt_carga_horaria_treinamento_w - qt_carga_total_w);
				treinamento_w(i).dt_final	:= dt_treinamento_w+(qt_carga_horaria_treinamento_w - qt_carga_total_w)/24;
				treinamento_w(i).qt_carga_dia	:= (qt_carga_horaria_treinamento_w - qt_carga_total_w);
			end if;
		end if;
	else
		if	((qt_carga_horaria_treinamento_w - qt_carga_total_w) <= Round(qt_carga_diaria_ww/2)) then
			qt_carga_total_w		:= qt_carga_total_w + (qt_carga_horaria_treinamento_w - qt_carga_total_w);
			treinamento_w(i).dt_final	:= dt_treinamento_w+((qt_carga_horaria_treinamento_w - qt_carga_total_w)/24);
			treinamento_w(i).qt_carga_dia	:= (qt_carga_horaria_treinamento_w - qt_carga_total_w);
		else
			qt_carga_total_w		:= qt_carga_total_w + Round(qt_carga_diaria_ww/2);
			treinamento_w(i).dt_final	:= dt_treinamento_w+Round(qt_carga_diaria_ww/2)/24;
			treinamento_w(i).qt_carga_dia	:= Round(qt_carga_diaria_ww/2);
		end if;
	end if;

		if	(trunc(dt_treinamento_w) = trunc(dt_treinamento_p)) then
			z	:= i;
		end if;
		end;
	end loop;
end if;

if	(z = -1) then
	Wheb_mensagem_pck.exibir_mensagem_abort(279330);
end if;

if	(ie_opcao_p = 'HI') then
	Return to_char(treinamento_w(z).dt_inicial,'dd/mm/yyyy hh24:mi:ss');
elsif	(ie_opcao_p = 'HF') then
	Return to_char(treinamento_w(z).dt_final,'dd/mm/yyyy hh24:mi:ss');
elsif	(ie_opcao_p = 'CH') then
	Return to_char(treinamento_w(z).qt_carga_dia);
end if;

END TRE_CONSISTIR_HORA_TREINAMENTO;
/
