create or replace
function obter_status_cme_agenda(	nr_seq_agenda_p		number,
					nr_seq_conjunto_p	number,
					ie_codigo_p		varchar)
					return	varchar2 is

qt_agenda_per_w			number(10,0);
qt_tempo_esterilizacao_w	number(10,0)	:= 0;
dt_inicial_w			date;
dt_final_w			date;
qt_conj_agenda_w		number(10,0);
qt_conj_agenda_exec_w		number(10,0);
ie_codigo_w			varchar2(15)	:='D';
qt_tempo_w 			number(10)	:= null;
nr_seq_conjunto_w		number(10);
qt_consiste_agenda_w		number(10,0);
cd_estab_agenda_w		number(4);
cd_estabelecimento_w		number(4);
cd_estabelecimento_cme_w	number(4);
nr_seq_agenda_w			number(10);
qt_tempo_transporte_w		number(10);
nr_seq_agenda_C_E_w			agenda_paciente.nr_sequencia%type;
nr_seq_agenda_I_w			agenda_paciente.nr_sequencia%type;

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(303785, null, wheb_usuario_pck.get_nr_seq_idioma);--Dispon�vel
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(495852, null, wheb_usuario_pck.get_nr_seq_idioma);--Emprestado
expressao3_w	varchar2(255) := obter_desc_expressao_idioma(495853, null, wheb_usuario_pck.get_nr_seq_idioma);--Indispon�vel

begin

Obter_Param_Usuario(871, 484, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,qt_tempo_transporte_w);

nr_seq_conjunto_w := nr_seq_conjunto_p;

if	(nvl(nr_seq_agenda_p,0) > 0) then
	select	nvl(max(b.cd_estabelecimento),0)
	into	cd_estab_agenda_w
	from	agenda_paciente a,
		agenda b
	where	a.cd_agenda 	= b.cd_agenda
	and	a.nr_sequencia 	= nr_seq_agenda_p;

	select	nvl(max(qt_consiste_agenda), 0),
		max(cd_estabelecimento),
		nvl(max(qt_tempo_esterelizacao),0)
	into	qt_consiste_agenda_w,
		cd_estabelecimento_cme_w,
		qt_tempo_esterilizacao_w
	from	cm_conjunto
	where	nr_sequencia		= nr_seq_conjunto_w;

	if	(cd_estabelecimento_cme_w = cd_estab_agenda_w) then
		qt_tempo_transporte_w := 0;
	end if;	

	select	a.hr_inicio - (qt_tempo_transporte_w/24),
		decode(a.ie_status_agenda, 'E', (a.hr_inicio + (qt_tempo_esterilizacao_w / 1440) - (1/86400)), (a.hr_inicio + ((nvl(qt_tempo_w,a.nr_minuto_duracao) + nvl(qt_tempo_esterilizacao_w,0)) / 1440) - (1/86400))) + (qt_tempo_transporte_w/24)
	into	dt_inicial_w,
		dt_final_w
	from	agenda_paciente a,
		agenda b
	where	a.cd_agenda 	= b.cd_agenda
	and	a.nr_sequencia	= nr_seq_agenda_p;

	--Obter qtde de agendamentos que foram executadas no periodo - neste s� � contado o tempo de esteriliza��o e n�o considera mais o tempo de dura��o pois o mesmo j� foi realizado 
	select 	nvl(max(b.nr_sequencia),0) nr_seq_agenda, 
			count(*) 
	into	nr_seq_agenda_I_w, 
			qt_conj_agenda_exec_w
	from	agenda_pac_cme a,
		agenda_paciente b 
	where	b.nr_sequencia = a.nr_seq_agenda 
	and	((hr_inicio between dt_inicial_w and dt_final_w) or  
		(hr_inicio + (qt_tempo_esterilizacao_w / 1440) - (1/86400) between dt_inicial_w and dt_final_w) or
		((hr_inicio < dt_inicial_w) and (hr_inicio + (qt_tempo_esterilizacao_w / 1440) - (1/86400) > dt_final_w)))
	and	a.nr_seq_conjunto = nr_seq_conjunto_w
	and	b.ie_status_agenda = 'E'
	and	a.ie_origem_inf = 'I';
		
	--Obter qtde de agendamentos que n�o foram executados e/ou cancelados no periodo 	
	select 	nvl(max(b.nr_sequencia),0) nr_seq_agenda, 
			count(*) 
	into	nr_seq_agenda_C_E_w, 
			qt_conj_agenda_w
	from	agenda_pac_cme a,
		agenda_paciente b 
	where	b.nr_sequencia = a.nr_seq_agenda 
	and	((hr_inicio between dt_inicial_w and dt_final_w) or  
		(hr_inicio + ((nr_minuto_duracao + nvl(qt_tempo_esterilizacao_w,0)) / 1440) - (1/86400) between dt_inicial_w and dt_final_w) or
		((hr_inicio < dt_inicial_w) and (hr_inicio + ((nr_minuto_duracao + nvl(qt_tempo_esterilizacao_w,0)) / 1440) - (1/86400) > dt_final_w)))
	and	a.nr_seq_conjunto = nr_seq_conjunto_w
	and	b.ie_status_agenda not in ('C','E')
	and	a.ie_origem_inf = 'I';

	--Somado todos os agendamentos 
	qt_conj_agenda_w	:= qt_conj_agenda_exec_w + qt_conj_agenda_w;

	if	(qt_conj_agenda_w > 0) then
		-- Obter o �ltimo agendamento executado - Obter o �ltimo agendamento desconsiderando os executados e os cancelados 			
		qt_agenda_per_w := GREATEST(nvl(qt_conj_agenda_exec_w,0), nvl(qt_conj_agenda_w,0));
		nr_seq_agenda_w := GREATEST(nvl(nr_seq_agenda_C_E_w,0), nvl(nr_seq_agenda_I_w,0));

		if	(qt_agenda_per_w >= qt_consiste_agenda_w) then
			select	nvl(max(b.cd_estabelecimento),0)
			into	cd_estabelecimento_w
			from	agenda_paciente a,
				agenda b
			where	a.cd_agenda 	= b.cd_agenda
			and	a.nr_sequencia 	= nr_seq_agenda_w;
			if	(cd_estabelecimento_w > 0) and
				(cd_estabelecimento_w <> cd_estab_agenda_w) then
				ie_codigo_w	:= 'E';	
			else
				ie_codigo_w	:= 'I';
			end if;	
		end if;	
	end if;
end if;

if	(ie_codigo_p = 'S') then
	return	ie_codigo_w;
elsif	(ie_codigo_w = 'D') then
	return 	expressao1_w;
elsif	(ie_codigo_w = 'E') then	
	return 	expressao2_w;
elsif	(ie_codigo_w = 'I') then	
	return 	expressao3_w;
end if;	

end obter_status_cme_agenda;
/
