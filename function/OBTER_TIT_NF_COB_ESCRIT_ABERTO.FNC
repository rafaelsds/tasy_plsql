create or replace
function obter_tit_nf_cob_escrit_aberto(nr_seq_nf_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(01) := 'N';
cd_estabelecimento_w	number(04,0);

begin

select	nvl(max(cd_estabelecimento),0)
into	cd_estabelecimento_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nf_p;

if	(cd_estabelecimento_w > 0) then
	select	decode(max(qt_existe),0,'N','S')
	into	ds_retorno_w
	from	(
		select  count(*) qt_existe
		from	titulo_pagar_escrit t,
			titulo_pagar a
		where	a.nr_seq_nota_fiscal 	= nr_seq_nf_p
		and	a.cd_estabelecimento 	= cd_estabelecimento_w
		and	a.ie_situacao		= 'A'
		and	a.nr_titulo		= t.nr_titulo
		union
		select	count(*)
		from	titulo_receber_cobr t,
			titulo_receber a
		where	a.nr_seq_nf_saida    	= nr_seq_nf_p
		and	a.ie_situacao		= '1'
		and	a.cd_estabelecimento	= cd_estabelecimento_w
		and	t.nr_titulo			= a.nr_titulo);
end if;

return	nvl(ds_retorno_w,'N');

end obter_tit_nf_cob_escrit_aberto;
/
