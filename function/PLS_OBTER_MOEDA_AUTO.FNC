create or replace
function pls_obter_moeda_auto (	nr_seq_preco_auto_p	pls_preco_autogerado.nr_sequencia%type,
				nr_seq_prestador_p	pls_prestador.nr_sequencia%type,
				cd_procedimento_p	procedimento.cd_procedimento%type,
				ie_origem_proced_p	procedimento.ie_origem_proced%type)
 		    	return moeda.cd_moeda%type is
			
cd_especialidade_w		varchar2(4000);
ie_valido_w			varchar2(1);
cd_moeda_w			moeda.cd_moeda%type;
			
Cursor C01 (	nr_seq_preco_auto_pc		pls_preco_autogerado.nr_sequencia%type) is
	select	a.cd_moeda,
		a.nr_seq_grupo_servico,
		a.nr_seq_grupo_prestador,
		a.cd_especialidade_prest
	from	pls_preco_regra_autogerado a
	where	a.nr_seq_preco_auto = nr_seq_preco_auto_pc
	order by 
		nvl(a.nr_seq_grupo_servico,0),
		nvl(a.nr_seq_grupo_prestador,0),
		nvl(a.cd_especialidade_prest,0);

begin

cd_especialidade_w := substr(pls_obter_cod_espec_prest(nr_seq_prestador_p, null)||',',1,4000);

for r_C01_w in C01 (nr_seq_preco_auto_p) loop
	
	ie_valido_w := 'S';
	
	if	(r_C01_w.nr_seq_grupo_servico is not null) then
		ie_valido_w := pls_se_grupo_preco_servico(r_C01_w.nr_seq_grupo_servico, cd_procedimento_p, ie_origem_proced_p);
	end if;
	
	if	(r_C01_w.nr_seq_grupo_prestador is not null) and
		(ie_valido_w = 'S') then
		ie_valido_w := pls_obter_se_grupo_prestador(nr_seq_prestador_p, r_C01_w.nr_seq_grupo_prestador);
	end if;
	
	if	(r_C01_w.cd_especialidade_prest is not null) and
		not(cd_especialidade_w like('%'||r_c01_w.cd_especialidade_prest||',%')) then
		ie_valido_w := 'N';
	end if;
	
	if	(ie_valido_w = 'S') then
		cd_moeda_w := r_C01_w.cd_moeda;
	end if;
end loop;

return	cd_moeda_w;

end pls_obter_moeda_auto;
/
