create or replace function obter_result_pct(qt_pct_p number)
  return varchar2 is

  ds_retorno_w varchar2(50);
  sql_w        VARCHAR2(200);
begin

  begin
    sql_w := 'CALL OBTER_DSC_EXP_RESULT_PCT_MD(:1) INTO :ds_retorno_w';
  
    execute immediate sql_w
      using in qt_pct_p, out ds_retorno_w;
  
  exception
    when others then
      ds_retorno_w := null;
  end;

  return ds_retorno_w;

end obter_result_pct;
/