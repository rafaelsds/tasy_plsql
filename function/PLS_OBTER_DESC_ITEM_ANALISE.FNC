/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter a descri��o do item da an�lise de ades�o do benefici�rio
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_desc_item_analise
		(	nr_seq_item_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);

begin

select	max(ds_item)
into	ds_retorno_w
from	pls_itens_analise_adesao
where	nr_sequencia	= nr_seq_item_p;

return	ds_retorno_w;

end pls_obter_desc_item_analise;
/
