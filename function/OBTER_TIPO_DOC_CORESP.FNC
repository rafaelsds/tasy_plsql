create or replace
function obter_tipo_doc_coresp(nr_sequencia_p	number)
 		    	return varchar2 is

ds_tipo_doc_w	varchar2(255);
begin
if (nr_sequencia_p is not null) then

	SELECT	max(ds_tipo_doc)
	into	ds_tipo_doc_w
	FROM 	tipo_documento_corresp 
	where	nr_sequencia	= nr_sequencia_p;
	
end if;
	
return	ds_tipo_doc_w;

end obter_tipo_doc_coresp;
/
