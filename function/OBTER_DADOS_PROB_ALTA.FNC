create or replace
function obter_dados_prob_alta	(	nr_atendimento_p	number,
					ie_tipo_p		number,
					ie_cd_ds_p		varchar2 default 'D')
 		    	return varchar2 is
ds_retorno_w	varchar2(255);
begin
if (nvl(nr_atendimento_p,0) > 0) and (nvl(ie_tipo_p,0) > 0)  then
	if (nvl(ie_cd_ds_p,'D') = 'D') then
	
		if (nvl(ie_tipo_p,0) = 1) then
			select	max(substr(obter_valor_dominio(5708,ie_tipo_status_alta),1,255))
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 2) then
			select	max(substr(obter_valor_dominio(5683,ie_tipo_alta),1,255))
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 3) then
			select	max(substr(obter_valor_dominio(1410,ie_tipo_vaga),1,255))
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 4) then
			select	max(substr(obter_nome_setor(cd_setor_desejado),1,255))
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 5) then
			select	max(substr(obter_descricao_padrao('TIPO_ACOMODACAO','DS_TIPO_ACOMODACAO', CD_TIPO_ACOMOD_DESEJ),1,255))
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 6) then
			select	max(substr(ds_obs_prev_alta,1,255))
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;		
		end if;
	else
	
		if (nvl(ie_tipo_p,0) = 1) then
			select	max(ie_tipo_status_alta)
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 2) then
			select	max(ie_tipo_alta)
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 3) then
			select	max(ie_tipo_vaga)
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 4) then
			select	max(cd_setor_desejado)
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 5) then
			select	max(cd_tipo_acomod_desej)
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		elsif (nvl(ie_tipo_p,0) = 6) then
			select	max(substr(ds_obs_prev_alta,1,255))
			into	ds_retorno_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;		
		end if;	
	end if;
end if;
		
return	ds_retorno_w;

end obter_dados_prob_alta;
/