create or replace
function obter_se_espec_parecer(	cd_especialidade_p	number)
 		    	return varchar2 is

qt_espec_parecer_w	number(10);
cd_estabelecimento_w	number(10);
cd_estab_logado_w	number(10);
ds_retorno_w		varchar2(1);

begin
cd_estab_logado_w := Obter_Estabelecimento_Ativo;

select	count(*)
into	qt_espec_parecer_w
from	espec_medica_parecer
where	cd_especialidade = cd_especialidade_p;

if	(qt_espec_parecer_w = 0) then
	ds_retorno_w := 'S';
elsif	(qt_espec_parecer_w > 0) then
	select	count(*)
	into	qt_espec_parecer_w
	from	espec_medica_parecer
	where	cd_especialidade = cd_especialidade_p
	and		cd_estabelecimento = cd_estab_logado_w;
	
	if	(qt_espec_parecer_w = 0) then
		ds_retorno_w := 'N';
	else
		ds_retorno_w := 'S';
	end if;
end if;

return	ds_retorno_w;

end obter_se_espec_parecer;
/