create or replace
function obter_se_gera_req_protocolo(
	nr_seq_protocolo_req_p	number)
	return varchar2 is



ie_gerar_w	varchar2(1);
ie_retorno_w	varchar2(1);
ie_ordem_w	number(5);

ds_horarios_w	prot_requisicao_regra.ds_horarios%type;
ds_horarios_aux_w	prot_requisicao_regra.ds_horarios%type;
pos		number(5);
ds_horario_w	varchar2(20);
dt_horario_w	date;

dt_inicio_w	date := PKG_DATE_UTILS.start_of(sysdate,'dd', 0);
dt_fim_w		date := PKG_DATE_UTILS.END_OF(sysdate, 'DAY', 0);
dt_ultima_exec_w	date;

cursor c01 is
select	1 ie_ordem,
	ie_gerar,
	ds_horarios
from	prot_requisicao_regra
where	nr_seq_prot_requisicao	= nr_seq_protocolo_req_p
and	ie_dia_semana is null
union all
select	9 ie_ordem,
	ie_gerar,
	ds_horarios
from	prot_requisicao_regra
where	nr_seq_prot_requisicao	= nr_seq_protocolo_req_p
and	ie_dia_semana		= obter_cod_dia_semana(sysdate)
order by
	1;

begin

ie_retorno_w	:= 'S';

select	max(dt_ultima_exec)
into	dt_ultima_exec_w
from	prot_requisicao
where	nr_sequencia = nr_seq_protocolo_req_p;

open	c01;
loop
fetch	c01 into
	ie_ordem_w,
	ie_gerar_w,
	ds_horarios_w;
exit when c01%notfound;
	ie_retorno_w	:= ie_gerar_w;
end loop;
close 	c01;

if	(ie_retorno_w = 'S') then
	begin
	if	(nvl(ds_horarios_w,'X') = 'X') then
		ie_retorno_w	:= 'S';
	else
		begin
		ie_retorno_w		:= 'N';		
		ds_horarios_aux_w	:= ds_horarios_w;		
		dt_horario_w		:= sysdate;
		
		while	(dt_horario_w is not null) and (ie_retorno_w = 'N') loop 
			begin
			dt_horario_w	:= null;			
			pos		:= instr(ds_horarios_aux_w,',');
			
			if	(pos > 0) then
				begin
				ds_horario_w	:= substr(ds_horarios_aux_w,1,pos-1);				
				ds_horarios_aux_w	:= substr(ds_horarios_aux_w,pos+1,length(ds_horarios_aux_w));				
				begin
				ds_horario_w	:= to_char(dt_inicio_w,'dd/mm/yyyy') || ' ' || trim(ds_horario_w);
				dt_horario_w	:= to_date(ds_horario_w,'dd/mm/yyyy hh24:mi:ss');
				exception
				when others then
					dt_horario_w	:= null;
				end;
				end;
			end if;
			
			if	(dt_horario_w is null) and (nvl(ds_horarios_aux_w,'X') <> 'X') then
				begin
				ds_horario_w	:= to_char(dt_inicio_w,'dd/mm/yyyy') || ' ' || trim(ds_horarios_aux_w);
				dt_horario_w	:= to_date(ds_horario_w,'dd/mm/yyyy hh24:mi:ss');
				ds_horarios_aux_w	:= null;
				exception
				when others then
					dt_horario_w	:= null;
				end;
			end if;
			
			if	(dt_horario_w is not null) and 
				(dt_horario_w < sysdate) and
				((dt_ultima_exec_w is null) or (dt_horario_w > dt_ultima_exec_w)) then
				begin
				select	'N'
				into	ie_retorno_w
				from	requisicao_material
				where	nr_seq_protocolo = nr_seq_protocolo_req_p
				and	dt_solicitacao_requisicao between dt_horario_w and dt_fim_w
				and	rownum = 1;
				exception
				when others then
					ie_retorno_w	:= 'S';
				end;
			end if;	
			end;
		end loop;		
		end; 
	end if;
	end;
end if;

return ie_retorno_w;

end obter_se_gera_req_protocolo;
/