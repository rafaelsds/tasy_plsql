create or replace
function obter_se_proc_suspenso	(nr_prescricao_p	number,
					nr_seq_proced_p	number)
					return varchar2 is

ie_suspenso_w	varchar2(1);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_proced_p is not null) then
	select	nvl(max(ie_suspenso),'N')
	into	ie_suspenso_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_seq_proced_p;
end if;

return ie_suspenso_w;

end obter_se_proc_suspenso;
/