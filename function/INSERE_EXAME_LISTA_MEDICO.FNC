create or replace
PROCEDURE INSERE_EXAME_LISTA_MEDICO(
    nm_usuario_p              VARCHAR2,
    nr_seq_lista_p            NUMBER,
    nr_prescricao_p           NUMBER,
    nr_sequencia_prescricao_p NUMBER)
IS
  nm_usuario_w              lista_central_exame.nm_usuario%type;
  nr_sequencia_w            lista_central_exame.nr_sequencia%type;
  nr_prescricao_w           lista_central_exame.nr_prescricao%type;
  nr_sequencia_prescricao_w lista_central_exame.nr_sequencia_prescricao%type;
  nr_seq_lista_w            lista_central_exame.nr_seq_lista_medico%type;
  nr_ordem_apresentacao_w   lista_central_exame.nr_ordem_apresentacao%type;
  
BEGIN
  nm_usuario_w              := nm_usuario_p;
  nr_seq_lista_w            := nr_seq_lista_p;
  nr_prescricao_w           := nr_prescricao_p;
  nr_sequencia_prescricao_w := nr_sequencia_prescricao_p;

  SELECT lista_central_exame_seq.nextval 
  INTO nr_sequencia_w 
  FROM dual;
  
  SELECT NVL(MAX(a.nr_ordem_apresentacao),0)+5
  INTO nr_ordem_apresentacao_w
  FROM lista_central_exame a
  WHERE a.nr_seq_lista_medico = nr_seq_lista_w
  AND a.nr_prescricao IN (SELECT pp.nr_prescricao
                          FROM prescr_procedimento pp
                          WHERE a.nr_prescricao         = pp.nr_prescricao
                          AND a.nr_sequencia_prescricao = pp.nr_sequencia
                          AND pp.ie_status_execucao     = '20');
    
  INSERT INTO lista_central_exame(nr_sequencia,
                           dt_atualizacao,
                           nm_usuario,
                           dt_atualizacao_nrec,
                           nm_usuario_nrec,
                           nr_seq_lista_medico,
                           nr_prescricao,
                           nr_sequencia_prescricao,
                           nr_ordem_apresentacao)
  VALUES(nr_sequencia_w,
         sysdate,
         nm_usuario_w,
         sysdate,
         nm_usuario_w,
         nr_seq_lista_w,
         nr_prescricao_w,
         nr_sequencia_prescricao_w,
         nr_ordem_apresentacao_w);
    
  COMMIT;
  
END INSERE_EXAME_LISTA_MEDICO;
/
