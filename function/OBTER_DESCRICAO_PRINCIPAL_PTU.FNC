create or replace
function Obter_descricao_principal_ptu(nr_sequencia_p	number)
 		    	return varchar is
			
ds_retorno_w	varchar2(255);	
ie_principal_w	number(1);	

begin
select 	max(ie_principal)
into	ie_principal_w
from 	ptu_pacote_servico
where 	nr_sequencia	= nr_sequencia_p;
if	(ie_principal_w = 1) 	then
	ds_retorno_w := obter_desc_expressao(305803);
end if;
	
if	(ie_principal_w = 2) 	then
	ds_retorno_w	:= obter_desc_expressao(314452);
end if;
return	ds_retorno_w;

end Obter_descricao_principal_ptu;
/
