create or replace
function obter_nr_seq_mat_hor (NR_SEQ_ORDEM_P number)
return number is

nr_seq_mat_hor_w number(10);

begin

if	(NR_SEQ_ORDEM_P is not null) then
	
	select nr_seq_mat_hor 
	into nr_seq_mat_hor_w 
	from can_ordem_item_prescr 
	where NR_SEQ_ORDEM = NR_SEQ_ORDEM_P;
	
end if;

return	nr_seq_mat_hor_w;

end obter_nr_seq_mat_hor;
/