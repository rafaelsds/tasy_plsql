create or replace
function obter_tipo_carater_inter_sus	(ie_carater_inter_sus_p	varchar2) 
					return	varchar2 is

ie_carater_inter_sus_w	varchar2(2):= '';

begin

if	(ie_carater_inter_sus_p = '1') then
	ie_carater_inter_sus_w := 'E';
elsif	(ie_carater_inter_sus_p in ('5','20','26','27','28','29')) then
	ie_carater_inter_sus_w := 'U';
end if;

return	ie_carater_inter_sus_w;

end obter_tipo_carater_inter_sus;
/