create or replace
function PEP_Obter_Desc_gp	(	ds_tipo_p	varchar2,
								nr_seq_topografia_p	number,
								ie_lado_p			varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(4000);				

begin

ds_retorno_w	:= ds_tipo_p;

if	(nr_seq_topografia_p	is not null) or
	(ie_lado_p	is not null) then
	ds_retorno_w	:= ds_retorno_w||' ( ';
	
	if	(nr_seq_topografia_p	is not null) then
		ds_retorno_w	:= ds_retorno_w||Obter_desc_topografia_dor(nr_seq_topografia_p);
	end if;
	
	
	if	(ie_lado_p	is not null) then
		ds_retorno_w	:= ds_retorno_w||' '||obter_valor_dominio( 1372,ie_lado_p );
	end if;
	
	ds_retorno_w	:= ds_retorno_w||' )';
	
end if;

return	ds_retorno_w;

end PEP_Obter_Desc_gp;
/