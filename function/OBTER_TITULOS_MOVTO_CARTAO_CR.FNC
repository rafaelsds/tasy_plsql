create or replace
function obter_titulos_movto_cartao_cr( nr_seq_movto_cartao_p		number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(2000);			
nr_seq_trans_financ_w	number(15);
nr_seq_lote_w		number(15);
nr_titulo_w		number(15);
nr_seq_caixa_w		number(10);
		
Cursor C01 is
	select	nr_seq_titulo_receber
	from	movto_trans_financ b
	where	b.nr_seq_caixa		= nr_seq_caixa_w
	and	b.nr_seq_lote		= nr_seq_lote_w
	and	nr_seq_titulo_receber is not null;

begin

ds_retorno_w	:= null;

if	(nr_seq_movto_cartao_p is not null) then

	select	max(nr_seq_caixa)
	into	nr_seq_caixa_w
	from	movto_trans_financ a
	where	nr_seq_movto_cartao	= nr_seq_movto_cartao_p
	and	nr_seq_caixa is not null;

	select	max(a.nr_seq_lote)
	into	nr_seq_lote_w
	from	movto_trans_financ a
	where	a.nr_seq_caixa		= nr_seq_caixa_w
	and	a.nr_seq_movto_cartao	= nr_seq_movto_cartao_p;

	open C01;
	loop
	fetch C01 into	
		nr_titulo_w;
	exit when C01%notfound;
		ds_retorno_w	:= ds_retorno_w	|| nr_titulo_w || ', ';
	end loop;
	close C01;
	
	ds_retorno_w	:= substr(ds_retorno_w,1,(length(ds_retorno_w)-2));
	
end if;

return	ds_retorno_w;

end obter_titulos_movto_cartao_cr;
/