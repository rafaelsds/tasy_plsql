create or replace
function obter_executor_agenda_exame	(nr_seq_medico_p	number)
						return varchar2 is

cd_medico_w	varchar2(10);

begin
if	(nr_seq_medico_p is not null) then
	select	max(cd_medico)
	into	cd_medico_w
	from	agenda_medico
	where	nr_sequencia = nr_seq_medico_p;
end if;

return cd_medico_w;

end obter_executor_agenda_exame;
/