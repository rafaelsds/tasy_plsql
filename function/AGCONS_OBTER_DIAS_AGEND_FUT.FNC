create or replace
function Agcons_obter_dias_agend_fut(	cd_agenda_p	number)
					return integer is

qt_dias_agend_futuros_w	number(10);

begin

select	nvl(max(nr_dias_fut_agendamento),0)
into	qt_dias_agend_futuros_w
from	agenda
where	cd_agenda	= cd_agenda_p;

return	qt_dias_agend_futuros_w;

end Agcons_obter_dias_agend_fut;
/