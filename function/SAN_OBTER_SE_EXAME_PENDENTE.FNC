create or replace
function san_obter_se_exame_pendente(	nr_seq_doacao_p		Number,
					cd_estabelecimento_p	Number)
 		    	return Varchar2 is

ie_existe_exame_pend_w	Varchar2(1) := 'N';
			
begin

if	(nvl(nr_seq_doacao_p,0) > 0) then

	select	nvl(max('S'),'N')
	into	ie_existe_exame_pend_w
	from	san_doacao a,
		san_exame_realizado b,
		san_exame_lote c
	where	a.nr_sequencia		= nr_seq_doacao_p
	and	c.nr_sequencia 		= b.nr_seq_exame_lote
	and	c.nr_seq_doacao		= a.nr_sequencia
	and	a.ie_auto_exclusao	= 'S'
	and	(san_obter_se_exame_auxiliar(b.nr_seq_exame_lote, b.nr_seq_exame, cd_estabelecimento_p, 'O') = 'S'
		or	san_obter_se_exame_auxiliar(b.nr_seq_exame_lote, b.nr_seq_exame, cd_estabelecimento_p, 'S') = 'S')
	and	b.dt_liberacao is null;
	
end if;

return	ie_existe_exame_pend_w;

end san_obter_se_exame_pendente;
/
