create or replace 
function TISS_OBTER_COD_TUSS_DESP
			(nr_seq_procedimento_p	number)
			return varchar2 is
	
cd_convenio_w		conta_paciente.cd_convenio_parametro%type;
cd_estabelecimento_w	conta_paciente.cd_estabelecimento%type;
	
cd_procedimento_w	procedimento_paciente.cd_procedimento%type;
ie_origem_proced_w	procedimento_paciente.ie_origem_proced%type;
cd_procedimento_tuss_w	procedimento_paciente.cd_procedimento_tuss%type;
nr_seq_proc_pacote_w	procedimento_paciente.nr_seq_proc_pacote%type;
cd_proc_conv_w		procedimento_paciente.cd_procedimento_convenio%type;
dt_procedimento_w	date;

ie_classif_proced_w	procedimento.ie_classificacao%type;

ie_proc_tuss_w		tiss_parametros_convenio.ie_proc_tuss%type;
ie_codigo_tuss_w	tiss_regra_tuss.ie_codigo_tuss%type;

ds_retorno_w		varchar2(80);

cursor	c01 is
select 	a.ie_codigo_tuss
from 	tiss_regra_tuss a
where	a.cd_estabelecimento	= cd_estabelecimento_w
and 	a.cd_convenio		= cd_convenio_w
and 	a.dt_inicio_vigencia	<= dt_procedimento_w
and	(exists		(select	1
			from	tiss_regra_tuss_filtro x
			where	x.nr_seq_regra						= a.nr_sequencia
			and	nvl(x.ie_classif_proced,nvl(ie_classif_proced_w,'X'))	= nvl(ie_classif_proced_w,'X')) or
	not exists 	(select	1
			from	tiss_regra_tuss_filtro x
			where	x.nr_seq_regra				= a.nr_sequencia))		
order by a.dt_inicio_vigencia;

begin

begin
	select	a.cd_procedimento,
		a.ie_origem_proced,
		a.cd_procedimento_tuss,
		a.nr_seq_proc_pacote,
		b.cd_convenio_parametro,
		b.cd_estabelecimento,
		c.ie_classificacao,
		a.dt_procedimento,
		a.cd_procedimento_convenio
	into	cd_procedimento_w,
		ie_origem_proced_w,
		cd_procedimento_tuss_w,
		nr_seq_proc_pacote_w,
		cd_convenio_w,
		cd_estabelecimento_w,
		ie_classif_proced_w,
		dt_procedimento_w,
		cd_proc_conv_w
	from	procedimento_paciente a,
		conta_paciente b,
		procedimento c
	where	a.nr_interno_conta	= b.nr_interno_conta
	and	a.cd_procedimento	= c.cd_procedimento
	and	a.ie_origem_proced	= c.ie_origem_proced
	and	a.nr_sequencia		= nr_seq_procedimento_p
	and	rownum			= 1;
exception
when others then
	cd_convenio_w		:= null;
	cd_estabelecimento_w	:= null;	
end;

if	(cd_convenio_w is not null) and
	(cd_estabelecimento_w is not null) then
	
	select	nvl(max(ie_proc_tuss), 'N')
	into	ie_proc_tuss_w
	from	tiss_parametros_convenio
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_convenio		= cd_convenio_w;
	
	if	(nvl(cd_procedimento_tuss_w,0) <> 0) then	
		open C01;
		loop
		fetch C01 into	
			ie_codigo_tuss_w;
		exit when C01%notfound;
			begin
			ie_proc_tuss_w	:= ie_codigo_tuss_w;
			end;
		end loop;
		close C01;	
	end if;

	if	(ie_proc_tuss_w = 'S') and --Gerar para todos
		(nvl(cd_procedimento_tuss_w,0) <> 0) then
		
		ds_retorno_w	:= to_char(cd_procedimento_tuss_w);
		
	elsif	(ie_proc_tuss_w = 'P') and --Gerar para todos exceto pacote
		(nvl(cd_procedimento_tuss_w,0) <> 0) and
		(nr_seq_proc_pacote_w is null) then
		
		ds_retorno_w	:= to_char(cd_procedimento_tuss_w);
		
	elsif	(ie_proc_tuss_w = 'C') then --Convers�o ou TUSS
	
		if	(nvl(cd_proc_conv_w,'0') <> '0') and (cd_proc_conv_w <> to_char(cd_procedimento_w)) then --Convers�o
			ds_retorno_w	:= cd_proc_conv_w;
		elsif	(((nvl(cd_proc_conv_w,'0') = '0') or (cd_proc_conv_w = to_char(cd_procedimento_w))) and --Proc TUSS
			 (nvl(cd_procedimento_tuss_w,'0') <> '0'))  then 
			ds_retorno_w	:= to_char(cd_procedimento_tuss_w);		
		end if;	
	
	end if;	
end if;

return	ds_retorno_w;

end TISS_OBTER_COD_TUSS_DESP;
/