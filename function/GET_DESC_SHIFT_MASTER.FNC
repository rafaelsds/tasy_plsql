create or replace
function get_desc_shift_master(	nr_sequencia_p		number)
 		    	return varchar2 is
			

nm_shift_w	shift_master.nm_shift%type;

begin

select	nm_shift
into	nm_shift_w
from	shift_master
where	nr_sequencia = nr_sequencia_p;


return	nm_shift_w;

end get_desc_shift_master;
/

