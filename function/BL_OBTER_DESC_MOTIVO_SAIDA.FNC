create or replace
function Bl_Obter_Desc_Motivo_Saida( nr_seq_motivo_p	Number)
 		    	return Varchar2 is

ds_retorno_w	varchar2(255);
			
begin

if (Nr_Seq_Motivo_p is not null) then
	begin
	
	select	ds_motivo
	into	ds_retorno_w
	from	bl_motivo_saida
	where	nr_sequencia = nr_seq_motivo_p;
	
	end;
end if;

return	ds_retorno_w;

end Bl_Obter_Desc_Motivo_Saida;
/