create or replace
function TISS_OBTER_CGC_PREST_PROTOCOLO (cd_estabelecimento_p			number,
					  cd_setor_protocolo_p			number,
					  ie_tipo_protocolo_p			number,
					  cd_convenio_p				number,
					  cd_categoria_p			varchar2,
					  cd_procedencia_p			number) 
					  return varchar2 is

cd_cgc_cabecalho_w	varchar2(14);

cursor c01 is
select	cd_cgc_cabecalho
from	tiss_regra_cod_prestador
where	cd_estabelecimento					= cd_estabelecimento_p
and	cd_convenio						= cd_convenio_p
and	cd_prestador_convenio					is null
and	nvl(cd_procedencia, nvl(cd_procedencia_p, 0))		= nvl(cd_procedencia_p, 0)
and	nvl(cd_setor_protocolo, nvl(cd_setor_protocolo_p, 0))	= nvl(cd_setor_protocolo_p, 0)
and	nvl(ie_tipo_protocolo, nvl(ie_tipo_protocolo_p, 0))	= nvl(ie_tipo_protocolo_p, 0)
and	nvl(cd_categoria, nvl(cd_categoria_p,'X'))		= nvl(cd_categoria_p,'X')
and	sysdate between nvl(dt_inicio_vigencia,sysdate-9999) and nvl(dt_fim_vigencia,sysdate + 1)
order by nvl(cd_setor_protocolo,0),
	nvl(ie_tipo_protocolo,0),
	nvl(cd_categoria,'X'),
	nvl(cd_procedencia,0),
	dt_inicio_vigencia;

begin

open c01;
loop
fetch c01 into
	cd_cgc_cabecalho_w;
exit when c01%notfound;
end loop;
close c01;

return cd_cgc_cabecalho_w;

end TISS_OBTER_CGC_PREST_PROTOCOLO;
/