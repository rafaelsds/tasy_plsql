create or replace
function OBTER_SE_USUARIO_TERC	
		(nr_seq_terceiro_p	number,
		nm_usuario_p	varchar2)
		return varchar2 is

ie_terceiro_w		varchar2(1);

begin

ie_terceiro_w 	:= 'N';

if	(nm_usuario_p is not null and nr_seq_terceiro_p is not null)	then
	select	decode(count(*),0,'N','S')
	into	ie_terceiro_w
	from	usuario a,
		terceiro b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	a.nm_usuario = nm_usuario_p
	and	b.nr_sequencia = nr_seq_terceiro_p;
	
	if	(ie_terceiro_w = 'N')	Then
		select	decode(count(*),0,'N','S')
		into	ie_terceiro_w
		from	terceiro_pessoa_fisica a,
			usuario b
		where	nr_seq_terceiro = nr_seq_terceiro_p
		and	b.nm_usuario = nm_usuario_p
		and	a.cd_pessoa_fisica = b.cd_pessoa_fisica;	

	end if;
	
end if;


return ie_terceiro_w;

end OBTER_SE_USUARIO_TERC;
/