create or replace
function obter_se_existe_rat_cont(cd_conta_contabil_p		varchar2)
 		    	return varchar2 is

vl_regra_w			number(10);
ds_retorno_w		varchar2(255);

begin

select  sum(pr_total_rateio)
into    vl_regra_w	
from    ctb_regra_rat_contabil
where   cd_conta_origem 	 = 	cd_conta_contabil_p
and	nvl(ie_regra_rat_saldo,'N') = 'N';

if (vl_regra_w	 >= 100) then
	ds_retorno_w := 'S';
	else
	ds_retorno_w := 'N';
end if;


return	ds_retorno_w;

end obter_se_existe_rat_cont;
/
