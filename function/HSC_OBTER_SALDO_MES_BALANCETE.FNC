CREATE OR REPLACE FUNCTION hsc_obter_saldo_mes_balancete(
	cd_grupo_p 			NUMBER,
	ano_ref_p			NUMBER,
	cd_classificacao_p		VARCHAR,
	cd_emp				NUMBER,
	cd_estab_p			NUMBER,
	ie_normal_encerramento_p 	VARCHAR,
	nr_nivel_p			NUMBER,
	ie_balancete_p			VARCHAR,
	mes_p				VARCHAR,
	ie_zero_p			VARCHAR,
	cd_conta_contabil_p		VARCHAR2)
	RETURN VARCHAR2 IS

vl_saldo_mes_w	 	number(15,2);
nr_seq_mes_ref_w	number(15);

BEGIN

select 	min(nr_seq_mes_ref)
into	nr_seq_mes_ref_w
from	ctb_balancete_v2
where	TO_CHAR(dt_referencia, 'mm') = mes_p
and	nr_seq_mes_ref >= ano_ref_p
and	cd_estabelecimento = cd_estab_p;


SELECT	nvl(SUM(a.vl_movimento),0) vl_saldo
INTO	vl_saldo_mes_w
FROM 	ctb_balancete_v2 a
WHERE 	a.cd_grupo	= cd_grupo_p
AND	a.dt_referencia BETWEEN ctb_obter_mes_ref(ano_ref_p) AND fim_ano(ctb_obter_mes_ref(ano_ref_p))
AND	(CTB_Obter_Se_Conta_Classif_Sup(a.cd_conta_contabil, cd_classificacao_p)	= 'S' OR cd_classificacao_p = '0')
AND	a.cd_empresa	= cd_emp
AND	((cd_estab_p = '0') OR (a.cd_estabelecimento = cd_estab_p))
AND	(a.ie_normal_encerramento	= ie_normal_encerramento_p)
AND	(SUBSTR(obter_se_conta_vigente(a.cd_conta_contabil, ctb_obter_mes_ref(a.nr_seq_mes_ref)),1,1) = 'S')
AND	ctb_obter_nivel_classif_conta(cd_classificacao) <= nr_nivel_p
AND	ie_balancete_p = ie_balancete_p
AND	a.nr_seq_mes_ref = nr_seq_mes_ref_w
AND	a.cd_conta_contabil = cd_conta_contabil_p
GROUP BY 	a.ie_tipo,
	a.cd_conta_contabil,
	a.cd_classificacao,
	a.ds_conta_apres,
	a.ie_centro_custo;



RETURN vl_saldo_mes_w;
END hsc_obter_saldo_mes_balancete;
/