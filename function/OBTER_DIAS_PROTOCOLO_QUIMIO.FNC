create or replace
function obter_dias_protocolo_quimio(	nr_seq_paciente_p	number,
					cd_material_p		number,
					cd_intervalo_p		varchar2)
					return varchar2 is

ds_dias_aplicacao_w	varchar2(4000);
					
begin

select	max(ds_dias_aplicacao)
into	ds_dias_aplicacao_w
from	paciente_protocolo_medic
where	nr_seq_paciente		= nr_seq_paciente_p
and	cd_material 		= cd_material_p
and	nvl(cd_intervalo,'X')	= nvl(cd_intervalo_p,'X');

return	ds_dias_aplicacao_w;

end obter_dias_protocolo_quimio;
/
