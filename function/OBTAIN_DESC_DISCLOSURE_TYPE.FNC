create or replace function obtain_desc_disclosure_type(nr_disc_type number)
return varchar2 is
	ds_retorno_w 	emr_disclousure_claim_type.ds_emr_disc_claim_type%type;
begin

	if (nr_disc_type is not null) then
		select ds_emr_disc_claim_type
		into ds_retorno_w
		from emr_disclousure_claim_type
		where nr_sequencia = nr_disc_type;
	end if;

	return ds_retorno_w;
end obtain_desc_disclosure_type;
/

