create or replace
function man_obter_dados_news_os(
			nr_sequencia_p		number,
			ie_opcao_p		varchar2)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);
dt_alteracao_w		date;
ds_tipo_alteracao_w	varchar2(255);
ds_alteracao_w		varchar2(255);
ds_aplicacao_w		varchar2(50);
ds_funcao_w		varchar2(80);
ie_destaque_new_w	varchar2(1);
cd_versao_atual_w	varchar2(15);

/*	ie_opcao_p
	DT - Data altera��o
	T - Tipo altera��o
	A - Altera��o
	AP - Aplica��o
	F - Fun��o
	D - Destaque News
	V - Vers�o		*/

begin

if	(nvl(nr_sequencia_p,0) <> 0) and
	(nvl(ie_opcao_p,'X') <> 'X') then
	begin
	select	a.dt_alteracao,
		substr(obter_valor_dominio(1929, a.ie_tipo_alteracao),1,255),
		substr(obter_valor_dominio(1025, a.ie_alteracao),1,255),
		substr(obter_desc_aplicacao(a.ds_aplicacao),1,50),
		substr(obter_desc_funcao(cd_funcao),1,80),
		a.ie_destaque_new,
		a.cd_versao_atual
	into	dt_alteracao_w,
		ds_tipo_alteracao_w,
		ds_alteracao_w,
		ds_aplicacao_w,
		ds_funcao_w,
		ie_destaque_new_w,
		cd_versao_atual_w
	from	alteracao_tasy a
	where	a.nr_sequencia = (	select	nvl(max(x.nr_sequencia),0)
					from	alteracao_tasy x
					where	x.nr_seq_ordem_serv = nr_sequencia_p);
	
	if	(ie_opcao_p = 'DT') then
		ds_retorno_w := to_char(dt_alteracao_w,'dd/mm/yyyy hh24:mi:ss');
	elsif	(ie_opcao_p = 'T') then
		ds_retorno_w := ds_tipo_alteracao_w;
	elsif	(ie_opcao_p = 'A') then
		ds_retorno_w := ds_alteracao_w;
	elsif	(ie_opcao_p = 'AP') then
		ds_retorno_w := ds_aplicacao_w;
	elsif	(ie_opcao_p = 'F') then
		ds_retorno_w := ds_funcao_w;
	elsif	(ie_opcao_p = 'D') then
		ds_retorno_w := ie_destaque_new_w;
	elsif	(ie_opcao_p = 'V') then
		ds_retorno_w := cd_versao_atual_w;
	end if;
	end;
end if;

return	ds_retorno_w;

end man_obter_dados_news_os;
/