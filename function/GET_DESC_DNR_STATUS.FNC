create or replace
function GET_DESC_DNR_STATUS(cd_pessoa_fisica_p varchar2,
							nr_atendimento_p number) 
return varchar2 is
	
ds_retorno_w varchar2(4000);
ds_auxiliar_w diretriz_atendimento.DS_GUIDELINE_DESCRIPTION%type;

Cursor C01 is
	select NVL(DS_GUIDELINE_DESCRIPTION, substr(obter_valor_dominio(8478 , a.ie_diretriz) , 1, 100))
	from diretriz_atendimento a 
	WHERE dt_liberacao IS NOT NULL     
	AND nvl(ie_situacao, 'A') = 'A' 
	AND (NR_ATENDIMENTO_P is null or NR_ATENDIMENTO_P = NR_ATENDIMENTO)
	AND cd_pessoa_fisica = CD_PESSOA_FISICA_P
	AND IE_DISPLAY_PATIENTBAR = 'S'
    AND dt_final is null
	Order by NR_SEQ_PRESENTATION;

begin
	
	open C01;
	loop
	fetch C01 into
		ds_auxiliar_w;
	exit when C01%notfound;

		if (ds_retorno_w is null) then
			ds_retorno_w := ds_auxiliar_w;
		else
			ds_retorno_w := ds_retorno_w||'; '|| ds_auxiliar_w;
		end if;

	end loop;
	close C01;
    
	return ds_retorno_w;

end GET_DESC_DNR_STATUS;
/