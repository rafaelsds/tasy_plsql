create or replace
function obter_ds_item_servico(	nr_seq_item_serv_p	number)
 		    		return varchar2 is

ds_item_w	varchar2(255);

begin
ds_item_w := '';

if	(nvl(nr_seq_item_serv_p,0) > 0) then

	select	max(ds_item_servico)
	into	ds_item_w
	from	grupo_servico_item
	where	nr_sequencia = nr_seq_item_serv_p;
	
end if;

return	ds_item_w;

end obter_ds_item_servico;
/