create or replace
function pls_obter_total_geral_eventos
			(	nr_seq_tipo_ocorrencia_p	Varchar2,
				nr_seq_operador_p		Varchar2,
				dt_inicio_p			Date,
				dt_fim_p			Date,
				ie_tipo_atendimento_p		Varchar2,
				nr_seq_evento_p			Varchar2,
				ie_tipo_pessoa_p		Varchar2,
				cd_setor_atendimento_p		Varchar2,
				ie_status_p			Varchar2,
				nr_seq_agente_motivador_p	Varchar2,
				nr_seq_grupo_atend_p		Varchar2)
				return Number is

nr_seq_operador_w		Number(10);
nr_seq_tipo_ocorrencia_w	Number(10);
qt_eventos_w			Number(10)	:= 0;
qt_tot_geral_w			Number(10)	:= 0;
nr_seq_evento_w			Number(10);
ie_tipo_atendimento_w		Varchar2(2);
ie_tipo_atendimento_ww		Varchar2(2);

Cursor C01 is
	select	distinct(a.nr_seq_operador),
		b.nr_seq_tipo_ocorrencia,
		a.ie_tipo_atendimento
	from	pessoa_fisica  				d,
		pls_operador   				c,
		pls_atendimento_evento			b,
		pls_atendimento				a
	where	b.nr_seq_atendimento			= a.nr_sequencia
	and	a.nr_seq_operador			= c.nr_sequencia
	and	c.cd_pessoa_fisica			= d.cd_pessoa_fisica
	and	((a.ie_tipo_atendimento			= ie_tipo_atendimento_p) 	  or (ie_tipo_atendimento_p		= '0'))
	and	((b.nr_seq_tipo_ocorrencia		= to_number(nr_seq_tipo_ocorrencia_p))	or (to_number(nr_seq_tipo_ocorrencia_p)		= 0))
	and	a.nr_seq_operador			= decode(to_number(nr_seq_operador_p),0,a.nr_seq_operador,to_number(nr_seq_operador_p))
	and	trunc(a.dt_fim_atendimento)		between trunc(dt_inicio_p)		and fim_dia(dt_fim_p)
	and	((a.ie_tipo_pessoa			= ie_tipo_pessoa_p)		  	or (ie_tipo_pessoa_p				= '0'))
	and	((a.cd_setor_atendimento		= to_number(cd_setor_atendimento_p))	or (to_number(cd_setor_atendimento_p)		= 0))
	and	((a.ie_status				= ie_status_p)		  		or (ie_status_p					= '0'))
	and	((b.nr_seq_evento			= to_number(nr_seq_evento_p))		or (to_number(nr_seq_evento_p)			= 0))
	and	((a.nr_seq_agente_motivador		= to_number(nr_seq_agente_motivador_p)) or (to_number(nr_seq_agente_motivador_p)	= 0))
	and	((c.nr_seq_grupo_atend			= to_number(nr_seq_grupo_atend_p))	or (to_number(nr_seq_grupo_atend_p)		= 0))
	group by	a.ie_tipo_atendimento,
			b.nr_seq_tipo_ocorrencia,
			a.nr_seq_operador;
			
Cursor C02 is
	select	distinct(d.nr_seq_evento),
		a.ie_tipo_atendimento
	from	pls_atendimento_evento			d,
		pls_evento_ocorrencia 			c,
		pls_tipo_ocorrencia	  		b,
		pls_atendimento				a
	where	d.nr_seq_evento				= c.nr_sequencia
	and	d.nr_seq_tipo_ocorrencia		= b.nr_sequencia
	and	d.nr_seq_atendimento			= a.nr_sequencia
	and	((a.ie_tipo_atendimento			= ie_tipo_atendimento_w) 	  or (ie_tipo_atendimento_w		= '0'))
	and	d.nr_seq_tipo_ocorrencia		= nr_seq_tipo_ocorrencia_w
	and	a.nr_seq_operador			= nr_seq_operador_w
	and	trunc(a.dt_fim_atendimento)		between trunc(dt_inicio_p)		and fim_dia(dt_fim_p)
	and	((d.nr_seq_evento			= to_number(nr_seq_evento_p))	or (to_number(nr_seq_evento_p)	= 0))
	group by	a.ie_tipo_atendimento,
			d.nr_seq_evento;

begin

open C01;
loop
fetch C01 into	
	nr_seq_operador_w,
	nr_seq_tipo_ocorrencia_w,
	ie_tipo_atendimento_w;
exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into	
		nr_seq_evento_w,
		ie_tipo_atendimento_ww;
	exit when C02%notfound;
		begin
		
		select	nvl(sum(count(*)),0)
		into	qt_eventos_w
		from	pls_atendimento_evento		b,
			pls_atendimento			a
		where	b.nr_seq_atendimento		= a.nr_sequencia
		and	((a.ie_tipo_atendimento		= ie_tipo_atendimento_w) 	  or (ie_tipo_atendimento_w		= '0'))
		and	b.nr_seq_tipo_ocorrencia	= nr_seq_tipo_ocorrencia_w
		and	a.nr_seq_operador		= nr_seq_operador_w
		and	trunc(a.dt_fim_atendimento)	between trunc(dt_inicio_p)	and fim_dia(dt_fim_p)
		and	b.nr_seq_evento			= nr_seq_evento_w
		and	((a.ie_tipo_pessoa		= ie_tipo_pessoa_p)			or (ie_tipo_pessoa_p = '0'))
		and	((a.nr_seq_agente_motivador	= to_number(nr_seq_agente_motivador_p))	or (to_number(nr_seq_agente_motivador_p)	= 0))	
		and	((a.cd_setor_atendimento	= to_number(cd_setor_atendimento_p))	or (to_number(cd_setor_atendimento_p)	= 0))
		and	((a.ie_status			= ie_status_p)		 		or (ie_status_p		= '0'))
		group by	a.ie_tipo_atendimento,
			b.nr_seq_tipo_ocorrencia,
			a.nr_seq_operador;	
		
		qt_tot_geral_w	:= qt_tot_geral_w + qt_eventos_w;
		
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

return	qt_tot_geral_w;

end pls_obter_total_geral_eventos;
/