create or replace
function obter_proced_princ_adic_ag_pac	(nr_seq_agenda_p	number,
					cd_estabelecimento_p	number)
						return varchar2 is

ds_procedimentos_w	varchar2(4000);
ds_proced_w		varchar2(260);
nr_ordem_w		number(10,0);

cursor	c01 is
select	1 ordem,
	b.ds_procedimento|| ' ** ' || wheb_mensagem_pck.get_texto(299663) || ' =  ' || obter_se_pacote_convenio(a.cd_procedimento,a.ie_origem_proced, a.cd_convenio, cd_estabelecimento_p)
from	procedimento b,
	agenda_paciente_proc a
where	a.cd_procedimento = b.cd_procedimento
and	a.ie_origem_proced = b.ie_origem_proced
and	a.nr_sequencia = nr_seq_agenda_p
and	a.nr_seq_proc_interno is null
union all
select	2 ordem,
	b.ds_proc_exame|| ' ** ' || wheb_mensagem_pck.get_texto(299663) || ' =  ' || obter_se_pacote_convenio(a.cd_procedimento,a.ie_origem_proced, a.cd_convenio, cd_estabelecimento_p)
from	proc_interno b,
	agenda_paciente_proc a
where	a.nr_seq_proc_interno = b.nr_sequencia
and	a.nr_sequencia = nr_seq_agenda_p
union all
select	3 ordem,
	b.ds_procedimento || ' ** ' || wheb_mensagem_pck.get_texto(299663) || ' =  ' || obter_se_pacote_convenio(a.cd_procedimento,a.ie_origem_proced, a.cd_convenio, c.cd_estabelecimento)
from	procedimento b,
	agenda_paciente a,
	agenda c
where 	c.cd_agenda = a.cd_agenda 
and	a.cd_procedimento = b.cd_procedimento
and	a.ie_origem_proced = b.ie_origem_proced
and	a.nr_sequencia = nr_seq_agenda_p	
order by
	1;

begin
if	(nr_seq_agenda_p is not null) then
	open c01;
	loop
	fetch c01	into
			nr_ordem_w,
			ds_proced_w;
	exit when c01%notfound;
		begin
		if	(nvl(length(ds_procedimentos_w),0) < 3930) then
			ds_procedimentos_w := ds_procedimentos_w || ds_proced_w || '; ' || chr(13);
		end if;
		end;
	end loop;
	close c01;
end if;

return ds_procedimentos_w;

end obter_proced_princ_adic_ag_pac;
/