create or replace
function obter_se_setor_regra_entrada
		(cd_setor_atendimento_p		number,
		ie_clinica_p			number,
		ie_tipo_atendimento_p		number,
		ie_regra_p			varchar2,
		nr_seq_classificacao_p		number,
		cd_convenio_p			Number,
		cd_categoria_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number default null)
		return varchar2 is

ie_liberado_w			varchar2(01) := 'N';
cd_classif_setor_w		varchar2(02); 
qt_setor_usuario_w		number(10,0); 
qt_regra_convenio_w		Number(5);
ie_tipo_convenio_w		Number(3);
qt_regra_tipo_convenio_w	Number(5);
qt_regra_categoria_w		number(5);
ie_verifica_se_gv_w		varchar2(2);
cd_medico_resp_w		varchar2(10);
cd_estabelecimento_w		number(4);
cd_especialidade_medico_w	varchar(4000);
cd_especialidade_medico_ww	especialidade_medica.cd_especialidade%type;
nr_seq_regra_perfil_w		number(10);
cd_estabelecimento_ww		number(4);
qt_regra_clinica_setor_w   	number(4);
ie_categoria_convenio_w    	varchar2(2); 
qt_regra_clinica_w		number(10);
nr_seq_classif_w		number(10);
ie_nivel_atencao_usuario_w 	varchar2(1) := 'T';
cd_estabelecimento_base_w	setor_atendimento.cd_estabelecimento_base%type;

Cursor C01 is
	SELECT a.cd_especialidade
	FROM	especialidade_medica b,
		medico_especialidade a
	WHERE 	a.cd_especialidade	= b.cd_especialidade
	AND	a.cd_pessoa_fisica	= cd_medico_resp_w;

begin
ie_liberado_w		:= 'N';

if	(cd_estabelecimento_ww is null) then
	cd_estabelecimento_ww := wheb_usuario_pck.get_cd_estabelecimento;
end if;

select	max(cd_classif_setor),
		nvl(max(ie_verifica_se_gv),'N'),
		max(cd_estabelecimento_base)
into	cd_classif_setor_w,
		ie_verifica_se_gv_w,
		cd_estabelecimento_base_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_p;

if	(ie_tipo_atendimento_p	in (7,8)) and
	(cd_classif_setor_w	= '5') and (ie_regra_p	not in ('SCCSS','SSU','CTA','SCCTA','SSC', 'SEM','SPU', 'SCLU','SCTP')) then
	ie_liberado_w		:= 'S';
end if;

if	(ie_tipo_atendimento_p	= 8) then

	ie_nivel_atencao_usuario_w := wheb_assist_pck.get_nivel_atencao_perfil;

	if ( ie_nivel_atencao_usuario_w  = 'P') then

	    ie_liberado_w := 'S';

	end if;

end if;

if	(ie_liberado_w = 'N') then
	begin

	if	((ie_regra_p	= 'S') or (ie_regra_p	= 'T')) and
		(cd_classif_setor_w in ('1','2','3','4','5','7','8','9','10','12','13')) then
		ie_liberado_w		:= 'S';
		
	elsif	(ie_regra_p	= 'N') and
		(cd_classif_setor_w in ('1','2','3','4','8','9','12','13')) then
		ie_liberado_w		:= 'S';
		
	elsif	(ie_regra_p	= 'E') and
		(cd_classif_setor_w in ('1','2','3','4','5','8','12','13')) then
		ie_liberado_w		:= 'S';
		
	elsif	(ie_regra_p	= 'PS') and
		(cd_classif_setor_w = '1') then
		ie_liberado_w		:= 'S';
		
	elsif	(ie_regra_p	= 'RN') and
		(cd_classif_setor_w = '9') then
		ie_liberado_w		:= 'S';
		
	elsif	(ie_regra_p	= 'SU') and
		(cd_classif_setor_w in ('1','2','3','4','5','8','10','12','13')) then

		select	count(*)
		into	qt_setor_usuario_w
		from	usuario_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	nm_usuario_param	= nm_usuario_p;

		if	(qt_setor_usuario_w > 0) then
			ie_liberado_w		:= 'S';
		end if;
	elsif	(ie_regra_p	= 'SSU') then
		select	count(*)
		into	qt_setor_usuario_w
		from	usuario_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	nm_usuario_param	= nm_usuario_p;

		if	(qt_setor_usuario_w > 0) then
			ie_liberado_w		:= 'S';
		end if;
	elsif	(ie_regra_p	= 'TA') then
		if	(cd_classif_setor_w in ('2','3','4')) and
			(ie_tipo_atendimento_p = 1) then
			ie_liberado_w		:= 'S';
		elsif	(cd_classif_setor_w = '1') and
			(ie_tipo_atendimento_p = 3) then
			ie_liberado_w		:= 'S';
		elsif	(cd_classif_setor_w	= '5') and
			(ie_tipo_atendimento_p	in (7,8)) then
			ie_liberado_w		:= 'S';
		end if;
	elsif (ie_regra_p = 'TACG') then
        if (cd_classif_setor_w in ('2','3','4')) and (ie_tipo_atendimento_p = 1) then
            ie_liberado_w := 'S';
        elsif (cd_classif_setor_w = '2') and (ie_tipo_atendimento_p = 8) then
            ie_liberado_w := 'S';
		elsif (cd_classif_setor_w = '1') and (ie_tipo_atendimento_p = 8) then
            ie_liberado_w := 'S';
		elsif (cd_classif_setor_w	= '5') and (ie_tipo_atendimento_p in (7,8)) then
            ie_liberado_w := 'S';
		end if;
	elsif	(ie_regra_p	= 'TSP') and
		(cd_classif_setor_w in ('1','2','3','4','5','6','7','8','9','10','12','13')) then
		ie_liberado_w		:= 'S';		
	elsif	((ie_regra_p	= 'TACC') or (ie_regra_p	= 'TACCE'))then
		if	(cd_classif_setor_w in ('3','4')) and
			(ie_tipo_atendimento_p = 1) then
			ie_liberado_w		:= 'S';
		elsif	(cd_classif_setor_w = '1') and
			(ie_tipo_atendimento_p = 3) then
			ie_liberado_w		:= 'S';
		elsif	(cd_classif_setor_w	in ('5','2')) and
			(ie_tipo_atendimento_p	in (7,8)) then
			ie_liberado_w		:= 'S';
		end if;
		
		if (ie_regra_p	= 'TACCE' and ie_liberado_w = 'S') then			
			begin
				select	'S'
				into	ie_liberado_w
				from	dual
				where	obter_se_usuario_estab(nm_usuario_p,cd_estabelecimento_base_w) = 'S';
			exception
			when others then
				ie_liberado_w := 'N';
			end;
		end if;
		
	elsif	(ie_regra_p	= 'SC') then
		begin

		select	nvl(max('S'), 'N')
		into	ie_liberado_w
		from 	regra_clinica_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	ie_clinica		= ie_clinica_p
		and	nvl(cd_estabelecimento,cd_estabelecimento_ww) = cd_estabelecimento_ww;
	
		end;
	elsif	(ie_regra_p	= 'SSC') then
		begin
		select	nvl(max('S'), 'N')
		into	ie_liberado_w
		from 	regra_clinica_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	ie_clinica		= ie_clinica_p
		and	nvl(cd_estabelecimento,cd_estabelecimento_ww) = cd_estabelecimento_ww;
		end;
	elsif	(ie_regra_p= 'SCCSS') then
		begin
		
		select	nvl(max('S'), 'N')
		into	ie_liberado_w
		from 	regra_classificacao_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	nr_seq_classificacao	= nr_seq_classificacao_p;	
		
		end;	
	elsif	(ie_regra_p	= 'SCC') then
		begin

		select	nvl(max('S'), 'N')
		into	ie_liberado_w
		from 	regra_classificacao_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	nr_seq_classificacao	= nr_seq_classificacao_p;
	
		end;
	elsif	(ie_regra_p	= 'SCCTA') then
		begin

		select	nvl(max('S'), 'N')
		into	ie_liberado_w
		from 	regra_classificacao_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	nr_seq_classificacao	= nr_seq_classificacao_p;
		
		if	(ie_liberado_w	= 'S') then
			ie_liberado_w	:= 'N';
			
			if	(cd_classif_setor_w in ('2','3','4','12')) and
				(ie_tipo_atendimento_p = 1) then
				ie_liberado_w		:= 'S';
			elsif	(cd_classif_setor_w = '1') and
				(ie_tipo_atendimento_p = 3) then
				ie_liberado_w		:= 'S';
			elsif	(cd_classif_setor_w	= '5') and
				(ie_tipo_atendimento_p	in (7,8)) then
				ie_liberado_w		:= 'S';
			end if;
			
		end if;
	
		end;
	elsif	(ie_regra_p	= 'CLATIA') then
		begin

		select	nvl(max('S'), 'N')
		into	ie_liberado_w
		from 	regra_classificacao_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	nr_seq_classificacao	= nr_seq_classificacao_p;
		
			
		if	(((cd_classif_setor_w in ('2','3','4','12')) and
			(ie_tipo_atendimento_p = 1)) or 
			(ie_liberado_w = 'S')) then
			ie_liberado_w		:= 'S';
		elsif	(((cd_classif_setor_w = '1') and
			(ie_tipo_atendimento_p = 3)) or
			(ie_liberado_w = 'S')) then
			ie_liberado_w		:= 'S';
		elsif	((cd_classif_setor_w	= '5') and
			(ie_tipo_atendimento_p	in (7,8)) or
			(ie_liberado_w = 'S')) then
			ie_liberado_w		:= 'S';
		end if;
	
		end;
	elsif	(ie_regra_p	= 'SE') then	
		
		if	(ie_verifica_se_gv_w = 'S') then
			ie_liberado_w		:= 'S';
		end if;
			
	elsif	(ie_regra_p	= 'C') then
		
		select	max(ie_tipo_convenio)
		into	ie_tipo_convenio_w
		from	convenio
		where	cd_convenio	= cd_convenio_p;
	
		select	count(*)
		into	qt_regra_tipo_convenio_w
		from	regra_convenio_setor
		where	ie_tipo_convenio	= ie_tipo_convenio_w;
	
		select	count(*)
		into	qt_regra_convenio_w
		from	regra_convenio_setor
		where	cd_convenio	= cd_convenio_p;
		
		select	count(*)
		into	qt_regra_categoria_w
		from	regra_convenio_setor
		where	cd_convenio	= cd_convenio_p
		and		cd_categoria = cd_categoria_p;
		
		if	(qt_regra_convenio_w	> 0) or (qt_regra_tipo_convenio_w > 0) then
			select	nvl(max('S'),'N')
			into	ie_liberado_w
			from	regra_convenio_setor
			where	cd_setor_atendimento	= cd_setor_atendimento_p
			and	((cd_convenio		= cd_convenio_p)	
			and cd_categoria = nvl(cd_categoria_p,cd_categoria)
			or (qt_regra_categoria_w = 0) and ie_tipo_convenio	= ie_tipo_convenio_w);
		else
			ie_liberado_w	:= 'S';
		end if;
	elsif	(ie_regra_p	= 'CTA') then
		
		select	max(ie_tipo_convenio)
		into	ie_tipo_convenio_w
		from	convenio
		where	cd_convenio	= cd_convenio_p;
	
		select	count(*)
		into	qt_regra_tipo_convenio_w
		from	regra_convenio_setor
		where	ie_tipo_convenio	= ie_tipo_convenio_w;
	
		select	count(*)
		into	qt_regra_convenio_w
		from	regra_convenio_setor
		where	cd_convenio	= cd_convenio_p;
		
		select	count(*)
		into	qt_regra_categoria_w
		from	regra_convenio_setor
		where	cd_convenio	= cd_convenio_p
		and		cd_categoria = cd_categoria_p;
		
		if	(qt_regra_convenio_w	> 0) or (qt_regra_tipo_convenio_w > 0) then
			select	nvl(max('S'),'N')
			into	ie_liberado_w
			from	regra_convenio_setor
			where	cd_setor_atendimento	= cd_setor_atendimento_p
			and	((cd_convenio		= cd_convenio_p)	
			and cd_categoria = nvl(cd_categoria_p,cd_categoria)
			or (qt_regra_categoria_w = 0) and ie_tipo_convenio	= ie_tipo_convenio_w);
		else
			ie_liberado_w	:= 'S';
		end if;
		
		if (ie_liberado_w = 'S') then
			if	(cd_classif_setor_w in ('2','3','4','12')) and
			(ie_tipo_atendimento_p = 1) then
				ie_liberado_w		:= 'S';
			elsif	(cd_classif_setor_w = '1') and
			(ie_tipo_atendimento_p = 3) then
				ie_liberado_w		:= 'S';
			elsif	(cd_classif_setor_w	= '5') and
			(ie_tipo_atendimento_p	in (7,8)) then
				ie_liberado_w		:= 'S';
			else
				ie_liberado_w		:= 'N';
			end if;
		end if;
		
	elsif	(ie_regra_p	= 'CTV') then
		
		select	(nvl(obter_se_vagas_clinica(ie_clinica_p,cd_setor_atendimento_p,cd_convenio_p),'N')) 
		into	ie_liberado_w
		from	dual;
		
	elsif	(ie_regra_p	= 'SEM') then
	
		if	(nvl(nr_atendimento_p,0) > 0) then

			select	cd_medico_resp,
				cd_estabelecimento
			into	cd_medico_resp_w,
				cd_estabelecimento_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
			
			if (cd_medico_resp_w is not null) then
				open C01;
				loop
				fetch C01 into	
					cd_especialidade_medico_ww;
				exit when C01%notfound;
					begin
					if (cd_especialidade_medico_w is null) then
						cd_especialidade_medico_w := cd_especialidade_medico_ww|| ',';
					else
						cd_especialidade_medico_w := cd_especialidade_medico_w ||','|| cd_especialidade_medico_ww;
					end if;
					
					end;
				end loop;
				close C01;			
			
				if	(cd_especialidade_medico_w is not null) then
			
					select	nvl(max('S'), 'N')
					into	ie_liberado_w
					from 	regra_especialidade_setor
					where	cd_setor_atendimento	= cd_setor_atendimento_p
					and	obter_se_contido_entre_virgula(cd_especialidade_medico_w, cd_especialidade) = 'S'
					and	cd_estabelecimento	= cd_estabelecimento_w;
				
				end if;
			end if;
		
		end if;
	elsif	(ie_regra_p = 'SPU') then -- Regra de libera��es por perfil (Shift + F11)
		
		select	max(cd_estabelecimento),
			nvl(max(nr_seq_classificacao),0)
		into	cd_estabelecimento_w,
			nr_seq_classif_w
		from	atendimento_paciente
		where	nr_atendimento = nvl(nr_atendimento_p,0);

		if	(cd_estabelecimento_w is null) then
			cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
		end if;
		
		select	max(nr_sequencia)
		into	nr_seq_regra_perfil_w
		from	regra_perfil_recepcao
		where	cd_perfil = nvl(obter_perfil_ativo,0)
		and	cd_estabelecimento = cd_estabelecimento_w
		and 	(NVL(ie_tipo_atendimento,ie_tipo_atendimento_p) = ie_tipo_atendimento_p)
		and	ie_situacao = 'A';
		
		if	(nr_seq_regra_perfil_w > 0) then			
			select	nvl(max('S'), 'N')
			into	ie_liberado_w
			from	regra_perfil_set_recepcao
			where	nr_seq_regra_perfil = nr_seq_regra_perfil_w
			and	cd_setor_Atendimento = cd_setor_atendimento_p
			and	(NVL(nr_seq_classificacao,nr_seq_classif_w) = nr_seq_classif_w)
			and  	ie_situacao = 'A';
		else	
			ie_liberado_w := 'S';
		end if;
	elsif	(ie_regra_p = 'SCLU') then -- Setores por cl�nica, somente setores liberados para o usu�rio
		
		select	nvl(max('S'), 'N')
		into	ie_liberado_w
		from 	regra_clinica_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	ie_clinica		= ie_clinica_p
		and	nvl(cd_estabelecimento,cd_estabelecimento_ww) = cd_estabelecimento_ww;
		
		select	count(*)
		into	qt_setor_usuario_w
		from	usuario_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	nm_usuario_param	= nm_usuario_p;

		if	(qt_setor_usuario_w = 0) then
			ie_liberado_w	:= 'N';
		end if;
	elsif	(ie_regra_p	= 'CSSC') then
		select	count(*)
		into	qt_regra_clinica_w
		from 	regra_clinica_setor
		where	ie_clinica		= ie_clinica_p
		and	nvl(cd_estabelecimento,cd_estabelecimento_ww) = cd_estabelecimento_ww
		and	nvl(cd_convenio,cd_convenio_p) = cd_convenio_p;
		
		if	(qt_regra_clinica_w > 0) then
			select	count(*)
			into	qt_regra_clinica_setor_w
			from 	regra_clinica_setor
			where	cd_setor_atendimento	= cd_setor_atendimento_p
			and	ie_clinica		= ie_clinica_p
			and	nvl(cd_estabelecimento,cd_estabelecimento_ww) = cd_estabelecimento_ww
			and	nvl(cd_convenio,cd_convenio_p) = cd_convenio_p;
			
			if (qt_regra_clinica_setor_w > 0) then
				ie_liberado_w := 'S';	
			end if;	

		elsif (qt_regra_clinica_w = 0 ) then
			select	max(ie_tipo_convenio)
			into	ie_tipo_convenio_w
			from	convenio
			where	cd_convenio	= cd_convenio_p;
		
			select	count(*)
			into	qt_regra_tipo_convenio_w
			from	regra_convenio_setor
			where	nvl(ie_tipo_convenio,ie_tipo_convenio_w)	= ie_tipo_convenio_w;
		
			select	count(*)
			into	qt_regra_convenio_w
			from	regra_convenio_setor
			where	nvl(cd_convenio,cd_convenio_p)	= cd_convenio_p;
			
			select	count(*)
			into	qt_regra_categoria_w
			from	regra_convenio_setor
			where	cd_convenio	= cd_convenio_p
			and	nvl(cd_categoria,cd_categoria_p) = cd_categoria_p;
			
			if	(qt_regra_categoria_w > 0) and ((qt_regra_convenio_w	> 0) or (qt_regra_tipo_convenio_w > 0)) then
				select	nvl(max('S'),'N')
				into	ie_liberado_w
				from	regra_convenio_setor
				where	cd_setor_atendimento	= cd_setor_atendimento_p
				and	((cd_convenio		= cd_convenio_p)	
				and nvl(cd_categoria,cd_categoria_p) = nvl(cd_categoria_p,cd_categoria)
				or (qt_regra_categoria_w = 0) and ie_tipo_convenio	= ie_tipo_convenio_w);
				
			elsif 	(qt_regra_categoria_w = 0) and ((qt_regra_convenio_w = 0) or (qt_regra_tipo_convenio_w = 0)) and
				(qt_regra_clinica_w = 0) then 
				ie_liberado_w := 'S';
			end if;
		end if;
	elsif	(ie_regra_p = 'SCTP') then
		begin
		select	nvl(max('S'), 'N')
		into	ie_liberado_w
		from 	regra_clinica_setor
		where	cd_setor_atendimento	= cd_setor_atendimento_p 
		and	ie_clinica		= ie_clinica_p
		and	nvl(cd_estabelecimento,cd_estabelecimento_ww) = cd_estabelecimento_ww
		and	nvl(ie_tipo_atendimento, ie_tipo_atendimento_p) = ie_tipo_atendimento_p;
		end;
	elsif	(obter_se_contido_char(cd_setor_atendimento_p,ie_regra_p)	= 'S') then
		ie_liberado_w		:= 'S';
	end if;
	end;
end if;
return	ie_liberado_w;

end Obter_Se_Setor_Regra_Entrada;
/
