create or replace
function lic_melhor_vl_lance_fornec(	nr_seq_fornec_p		number,
				nr_seq_lic_item_p		number)
 		    	return number is

vl_item_w	number(15,4);
			
begin

select	nvl(min(vl_item), 0)
into	vl_item_w
from	reg_lic_fornec_lance
where	nr_seq_fornec = nr_seq_fornec_p
and	nr_seq_lic_item = nr_seq_lic_item_p
and	vl_item > 0;

return	vl_item_w;

end lic_melhor_vl_lance_fornec;
/