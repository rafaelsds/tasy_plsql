create or replace
function obter_atualizacao_preco_custo(	nr_interno_conta_p	number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					vl_custo_p		number)
					return varchar2 is

cd_estabelecimento_w			number(4);
cd_tabela_custo_w			number(5);
dt_mesano_referencia_w			date;	
ie_atualizado_w				varchar2(1) := 'S';
vl_custo_w				number(15,4);		
begin

if	(nr_interno_conta_p is not null) then
	
	select 	max(cd_estabelecimento),
		max(trunc(dt_mesano_referencia,'mm'))
	into	cd_estabelecimento_w,
		dt_mesano_referencia_w
	from 	conta_paciente
	where 	nr_interno_conta = nr_interno_conta_p;

	select	max(decode(ie_mes_calculo,'C',cd_tabela_venda,0))
	into	cd_tabela_custo_w
	from	parametro_custo
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(nvl(cd_tabela_custo_w,0) = 0)	 then
		select	nvl(max(cd_tabela_custo),0)
		into	cd_tabela_custo_w
		from	tabela_custo
		where	cd_tipo_tabela_custo	= 9
		and	cd_estabelecimento	= cd_estabelecimento_w
		and	dt_mes_referencia	= dt_mesano_referencia_w;
	end if;

	select	nvl(max(vl_preco_calculado),0)
	into	vl_custo_w
	from	preco_padrao_proc_v
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_tabela_custo		= cd_tabela_custo_w
	and	ie_origem_proced	= ie_origem_proced_p
	and	cd_procedimento		= cd_procedimento_p
	and	nvl(ie_calcula_conta,'S')	= 'S';

	ie_atualizado_w		:= 'S';
	if	(round(nvl(vl_custo_p,0),2) <> round(vl_custo_w,2)) then
		ie_atualizado_w	:= 'N';
	end if;
end if;
return	ie_atualizado_w;

end obter_atualizacao_preco_custo;
/