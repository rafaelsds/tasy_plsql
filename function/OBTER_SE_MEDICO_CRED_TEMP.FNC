create or replace
function obter_se_medico_cred_temp(	cd_medico_p		VARCHAR2,
					cd_pessoa_fisica_p	varchar2,
					nr_atendimento_p	number)
					return VARCHAR2 is

ie_corpo_clinico_w		VARCHAR2(1) := 'N';
ie_pf_credenciada_w		VARCHAR2(2);
ie_retorno_w			VARCHAR2(1);
qt_credenciamento_w             NUMBER(5);

begin


consistir_credenciamento_pf(nvl(wheb_usuario_pck.get_cd_estabelecimento,0), nvl(obter_especialidade_medico(cd_medico_p,'C'),0), cd_pessoa_fisica_p, sysdate, cd_medico_p, nr_atendimento_p,null, ie_pf_credenciada_w);
if	(ie_pf_credenciada_w = 'N')  then
	ie_retorno_w	:= 'N';
else
	ie_retorno_w	:= 'S';
	select  count(*)
	into    qt_credenciamento_w
	from    pf_credenciamento
	where   cd_pf_credenciamento    = cd_medico_p
	and     ((cd_estabelecimento    = wheb_usuario_pck.get_cd_estabelecimento) or (cd_estabelecimento is null));
	if      (qt_credenciamento_w = 0) then
		ie_retorno_w	:= 'N';
	end if;
end if;


return	ie_retorno_w;

end obter_se_medico_cred_temp;
/
