create or replace
function rxt_obter_prox_campo_aplic(	nr_seq_fase_p	number,
				nr_seq_agenda_p	number)
				return number is

nr_seq_campo_w	number(10,0);

begin
if	(nr_seq_fase_p is not null) and
	(nr_seq_agenda_p is not null) then

	select	min(nr_sequencia)
	into	nr_seq_campo_w
	from	rxt_campo
	where	nr_seq_fase = nr_seq_fase_p
	and	rxt_obter_se_campo_aplic(nr_seq_agenda_p,nr_sequencia,'F') = 'N';

end if;

return nr_seq_campo_w;

end rxt_obter_prox_campo_aplic;
/
