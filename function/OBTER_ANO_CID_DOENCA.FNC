create or replace function obter_ano_cid_doenca(nr_sequencia_p number) return varchar2 is
result_w    varchar2(4);

begin 

select to_char(dt_versao, 'yyyy') dt_versao_year INTO result_w
from cid_doenca_versao c
where c.nr_sequencia = nr_sequencia_p;

return result_w;

end obter_ano_cid_doenca;
/