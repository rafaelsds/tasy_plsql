create or replace
function obter_usuario_pf_ci(cd_pessoa_fisica_p 	varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(15) := '';
			
begin

if 	(cd_pessoa_fisica_p is not null) then
	select 	substr(nvl(nm_usuario_princ_ci,'0'),1,15)
	into	ds_retorno_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

	if 	(ds_retorno_w = '0') then
		select	substr(obter_usuario_pf(cd_pessoa_fisica_p),1,15)
		into	ds_retorno_w
		from	dual;
	end if;
end if;

return	ds_retorno_w;

end obter_usuario_pf_ci;
/	