create or replace
function san_obter_se_reserva_parcial(nr_seq_reserva_p	number)
 		    	return varchar2 is

qt_solicitada_w		san_reserva_item.qt_solicitada%type;
qt_hemocomponte_w	number(10);
ds_retorno_w		varchar2(1);

begin

select 	sum(QT_SOLICITADA)
into	qt_solicitada_w
from	SAN_RESERVA_ITEM
where	nr_seq_reserva = nr_seq_reserva_p;

select 	count(*)
into	qt_hemocomponte_w
from	SAN_RESERVA_prod
where	nr_seq_reserva = nr_seq_reserva_p;

ds_retorno_w	:= 'N';

if	(qt_hemocomponte_w < qt_solicitada_w) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end san_obter_se_reserva_parcial;
/
