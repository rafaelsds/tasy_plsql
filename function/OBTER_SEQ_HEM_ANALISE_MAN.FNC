CREATE OR REPLACE
FUNCTION Obter_seq_hem_analise_man	(nr_seq_analise_p	number)
					RETURN varchar2 IS

nr_seq_apresent_w	varchar2(20);

BEGIN

if	(nr_seq_analise_p is not null) then
	select	cd_manometria
	into	nr_seq_apresent_w
	from	hem_analise_manometrica
	where	nr_sequencia = nr_seq_analise_p;
end if;

RETURN	nr_seq_apresent_w;

END Obter_seq_hem_analise_man;
/
