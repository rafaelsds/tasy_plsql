create or replace
function cpoe_obter_se_perm_prescr_item(nr_atendimento_p	in atendimento_paciente.nr_atendimento%type,
										nm_usuario_p		in usuario.nm_usuario%type,
										ie_motivo_prescr_p	in regra_prescr_medic.ie_motivo_prescr%type,
										ie_tipo_item_p		in varchar2,
										cd_protocolo_p		in protocolo_medic_material.cd_protocolo%type,
										nr_sequencia_p		in protocolo_medic_material.nr_sequencia%type,
										nr_seq_item_p		in protocolo_medic_material.nr_seq_material%type,
										nr_prescricao_p		in prescr_medica.nr_prescricao%type default null,
										cd_material_p		in cpoe_material.cd_material%type default null,
										cd_mat_comp1_p		in cpoe_material.cd_mat_comp1%type default null,
										cd_mat_comp2_p		in cpoe_material.cd_mat_comp2%type default null,
										cd_mat_comp3_p		in cpoe_material.cd_mat_comp3%type default null,
										cd_mat_comp4_p		in cpoe_material.cd_mat_comp4%type default null,
										cd_mat_comp5_p		in cpoe_material.cd_mat_comp5%type default null,
										cd_mat_comp6_p		in cpoe_material.cd_mat_comp6%type default null,
										cd_mat_comp7_p		in cpoe_material.cd_mat_comp7%type default null)
								return varchar2 is

ie_perm_prescr_w		varchar2(1 char);
nr_agrupamento_w		protocolo_medic_material.nr_agrupamento%type;

/*
ie_tipo_item_p = [M]edicamento e [S]olucao.

Passar os parametros de acordo com o tipo da prescricao.

Protocolo ou Rotina: nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p, ie_tipo_item_p, cd_protocolo_p, nr_sequencia_p e nr_seq_item_p.

Favoritos: nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p, cd_material_p, cd_mat_comp1_p, cd_mat_comp2_p, cd_mat_comp3_p, cd_mat_comp4_p, cd_mat_comp5_p, cd_mat_comp6_p e cd_mat_comp7_p.

Prescricoes anteriores: nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p, ie_tipo_item_p, nr_seq_item_p e nr_prescricao_p.
*/

begin

	if (cd_material_p is null and nr_prescricao_p is null) then

		if (ie_tipo_item_p = 'M') then

			select	nvl(max(a.nr_agrupamento),0)
			into	nr_agrupamento_w
			from	protocolo_medic_material a
			where	a.cd_protocolo = cd_protocolo_p
			and		a.nr_sequencia = nr_sequencia_p
			and		a.nr_seq_material = nr_seq_item_p;

			select	nvl(max('N'),'S')
			into	ie_perm_prescr_w
			from	protocolo_medic_material a
			where	a.cd_protocolo = cd_protocolo_p
			and		a.nr_sequencia = nr_sequencia_p
			and		a.nr_agrupamento = nr_agrupamento_w
			and		a.nr_seq_solucao is null
			and		a.nr_seq_diluicao is null
			and		cpoe_regra_se_permite_presc(a.cd_material, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'N';

		elsif (ie_tipo_item_p = 'S') then

			select	nvl(max('N'),'S')
			into	ie_perm_prescr_w
			from	protocolo_medic_material a
			where	a.cd_protocolo = cd_protocolo_p
			and		a.nr_sequencia = nr_sequencia_p
			and		a.nr_seq_solucao = nr_seq_item_p
			and		nvl(a.ie_diluente,'N') = 'N'
			and		cpoe_regra_se_permite_presc(a.cd_material, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'N';

		else

			ie_perm_prescr_w := 'S';

		end if;

	elsif (cd_material_p is null and nr_prescricao_p is not null) then

		if (ie_tipo_item_p = 'M') then

			select	nvl(max(a.nr_agrupamento),0)
			into	nr_agrupamento_w
			from	prescr_material a
			where	a.nr_prescricao = nr_prescricao_p
			and		a.nr_sequencia = nr_seq_item_p;

			select	nvl(max('N'),'S')
			into	ie_perm_prescr_w
			from	prescr_material a
			where	a.nr_prescricao = nr_prescricao_p
			and		a.nr_agrupamento = nr_agrupamento_w
			and 	a.nr_sequencia_solucao is null
			and		a.nr_sequencia_diluicao is null
			and		a.ie_agrupador not in (3,7,9)
			and		cpoe_regra_se_permite_presc(a.cd_material, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'N';

		elsif (ie_tipo_item_p = 'S') then

			select	nvl(max('N'),'S')
			into	ie_perm_prescr_w
			from	prescr_material a
			where	a.nr_prescricao = nr_prescricao_p
			and		a.nr_sequencia_solucao = nr_seq_item_p
			and		a.nr_sequencia_diluicao is null
			and		a.ie_agrupador not in (3,7,9)
			and		cpoe_regra_se_permite_presc(a.cd_material, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'N';

		end if;

	else

		if ((cd_material_p is null or cpoe_regra_se_permite_presc(cd_material_p, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'S')
		and (cd_mat_comp1_p is null or cpoe_regra_se_permite_presc(cd_mat_comp1_p, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'S')
		and (cd_mat_comp2_p is null or cpoe_regra_se_permite_presc(cd_mat_comp2_p, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'S')
		and (cd_mat_comp3_p is null or cpoe_regra_se_permite_presc(cd_mat_comp3_p, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'S')
		and (cd_mat_comp4_p is null or cpoe_regra_se_permite_presc(cd_mat_comp4_p, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'S')
		and (cd_mat_comp5_p is null or cpoe_regra_se_permite_presc(cd_mat_comp5_p, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'S')
		and (cd_mat_comp6_p is null or cpoe_regra_se_permite_presc(cd_mat_comp6_p, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'S')
		and (cd_mat_comp7_p is null or cpoe_regra_se_permite_presc(cd_mat_comp7_p, nr_atendimento_p, nm_usuario_p, ie_motivo_prescr_p) = 'S')) then

			ie_perm_prescr_w := 'S';

		else

			ie_perm_prescr_w := 'N';

		end if;

	end if;

	return nvl(ie_perm_prescr_w, 'S');

end cpoe_obter_se_perm_prescr_item;
/
