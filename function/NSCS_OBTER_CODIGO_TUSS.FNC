create or replace
function NSCS_Obter_codigo_Tuss(nr_sequencia_p	number)
 		    	return number is

cd_procedimento_tuss_w	number(15,0);			
			
begin

select 	cd_procedimento_tuss
into	cd_procedimento_tuss_w
from 	procedimento_paciente
where 	nr_sequencia = nr_sequencia_p;

return	cd_procedimento_tuss_w;

end NSCS_Obter_codigo_Tuss;
/