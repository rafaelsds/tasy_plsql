create or replace
function get_score_escala_helios(	nr_seq_escala_p		number)
				return varchar2 is


ds_retorno_w				varchar2(255);
qt_pontuacao_w				escala_helios.qt_pontuacao%type;

begin

select	max(a.qt_pontuacao)
into	qt_pontuacao_w
from	escala_helios	a
where	a.nr_sequencia	= nr_seq_escala_p;

if	(qt_pontuacao_w <= 5) then
	ds_retorno_w	:= substr(obter_desc_expressao(802447),1,255);
elsif	(qt_pontuacao_w = 6) then
	ds_retorno_w	:= substr(obter_desc_expressao(802449),1,255);
elsif	(qt_pontuacao_w > 6) then
	ds_retorno_w	:= substr(obter_desc_expressao(802451),1,255);
end if;

return	ds_retorno_w;

end get_score_escala_helios;
/
