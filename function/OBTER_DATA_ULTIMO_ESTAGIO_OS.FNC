create or replace
function obter_data_ultimo_estagio_os (nr_seq_os_p	number)
	return date is
	
dt_ultimo_estagio_w	date;	
	
begin
if	(nr_seq_os_p is not null) then
	begin
	select	max(dt_atualizacao)
	into	dt_ultimo_estagio_w
	from	man_ordem_serv_estagio
	where	nr_seq_ordem = nr_seq_os_p;
	end;
end if;
return dt_ultimo_estagio_w;
end obter_data_ultimo_estagio_os;
/