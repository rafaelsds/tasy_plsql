create or replace
function diops_obter_regra_sinistro(nr_seq_periodo_p		number)
 		    	return varchar2 is

dt_periodo_inicial_w	date;
ie_evento_30_60_w	diops_fin_regra_sinistro.ie_evento_30_60%type;
	
begin

begin
select	trunc(a.dt_periodo_inicial, 'dd')
into	dt_periodo_inicial_w
from	diops_periodo	a
where	a.nr_sequencia	= nr_seq_periodo_p;

select	nvl(max(a.ie_evento_30_60), '60')
into	ie_evento_30_60_w
from	diops_fin_regra_sinistro	a
where	dt_periodo_inicial_w between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia, dt_periodo_inicial_w);

exception
when others then
	ie_evento_30_60_w := '60';
end;

return	ie_evento_30_60_w;

end diops_obter_regra_sinistro;
/