create or replace
function cnes_obter_apresent_equip
			(	nr_seq_equipamento_p	Number)
				return Number is
			
nr_retorno_w			Number(10);

begin

select	max(nr_seq_apresent)
into	nr_retorno_w
from	cnes_equipamento
where	nr_sequencia	= nr_seq_equipamento_p;

return	nr_retorno_w;

end cnes_obter_apresent_equip;
/