create or replace
function obter_carteira_convenio_atend(nr_atendimento_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
				
begin

select	max(a.ds_arquivo)
into	ds_retorno_w
from	pessoa_documentacao a,
		tipo_documentacao b,
		atendimento_paciente c
where	c.nr_atendimento = nr_atendimento_p
and		a.cd_pessoa_fisica = c.cd_pessoa_fisica
and		a.nr_seq_documento = b.nr_sequencia
and		b.ie_cart_convenio = 'S';

return	ds_retorno_w;

end obter_carteira_convenio_atend;
/