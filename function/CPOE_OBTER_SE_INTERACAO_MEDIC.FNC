create or replace 
function cpoe_obter_se_interacao_medic(	cd_material_p	number,
						nr_atendimento_p	number,
						cd_pessoa_fisica_p	varchar2,
						nr_sequencia_p	number)  return varchar2 is

qt_count_w	number(10);
ds_retorno_w	varchar2(1) := 'N';

begin

if (cd_material_p is not null and nr_atendimento_p is not null and cd_pessoa_fisica_p is not null and nr_sequencia_p is not null) then

	select	count(1)
	into	qt_count_w
	from(	select	b.ie_severidade ie_severidade, a.nr_atendimento nr_atendimento
			from	cpoe_material_vig_v a, 
				material_interacao_medic b 
			where	(((b.cd_material = cd_material_p) and (a.cd_material = b.cd_material_interacao)) or ((b.cd_material_interacao = cd_material_p) and (a.cd_material = b.cd_material))) 
			and	((a.nr_atendimento = nr_atendimento_p) or (a.cd_pessoa_fisica = cd_pessoa_fisica_p and a.nr_atendimento is null)) 
			and	((cpoe_reg_valido_ativacao(decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)),a.dt_inicio,sysdate) = 'S') or (a.ie_retrogrado = 'S' and a.dt_liberacao is null)) 
			and	((a.dt_liberacao is not null) or (a.nm_usuario = wheb_usuario_pck.get_nm_usuario)) and a.nr_sequencia <> nr_sequencia_p 
			and	a.ie_tipo_item in('MAT','MCOMP1','MCOMP2','MCOMP3', 'PMAT1', 'PMAT2', 'PMAT3', 'PMAT4', 'PMAT5', 'GMAT1', 'GMAT2', 'GMAT3','E','S','L') 
			and	nvl(dt_fim, dt_inicio + 1) > (sysdate - (select nvl(nr_dias_interacao,0) from parametro_medico where cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento))
			union 
			select	b.ie_severidade ie_severidade, a.nr_atendimento nr_atendimento
			from	cpoe_material_vig_v a,material m,material mi,material_interacao_medic b 
			where	m.cd_material = a.cd_material 
			and		((a.nr_atendimento = nr_atendimento_p) or (a.cd_pessoa_fisica = cd_pessoa_fisica_p and a.nr_atendimento is null)) 
			and		((a.dt_liberacao is not null) or (a.nm_usuario = wheb_usuario_pck.get_nm_usuario)) and a.nr_sequencia <> nr_sequencia_p 
			and 	a.ie_tipo_item in('MAT','MCOMP1','MCOMP2','MCOMP3', 'PMAT1', 'PMAT2', 'PMAT3', 'PMAT4', 'PMAT5', 'GMAT1', 'GMAT2', 'GMAT3','E','S','L') 
			and		mi.cd_material = cd_material_p 
			and		(((b.nr_seq_ficha = mi.nr_seq_ficha_tecnica) and (m.nr_seq_ficha_tecnica = b.nr_seq_ficha_interacao)) or ((b.nr_seq_ficha = m.nr_seq_ficha_tecnica) and (mi.nr_seq_ficha_tecnica = b.nr_seq_ficha_interacao))))
	where	cpoe_consiste_severidade_apres(wheb_usuario_pck.get_cd_perfil, nr_atendimento, ie_severidade) = 'S';
	
	if (qt_count_w > 0) then 
		ds_retorno_w := 'S';
	end if;

end if;

return ds_retorno_w;

end cpoe_obter_se_interacao_medic;
/