create or replace
function lab_obter_se_somente_numero( 	ds_campo_p    	varchar2)
				return 		varchar2 is

ds_campo_w    	varchar2(255);
ds_hor_aux_w	varchar2(255);
x     		integer;
ds_retorno_w	varchar2(1) := 'S';

begin

ds_campo_w		:= '';

if	(ds_campo_p is null) or (ds_campo_p = '') then
	ds_retorno_w	:= 'N';
else
	ds_campo_w := replace(replace(ds_campo_p,' ',''),':','');
		x	:= 1;
		while	(x	< length(ds_campo_p)) loop
			if	(substr(ds_campo_w,x,1) not in('0','1','2','3','4','5','6','7','8','9',',','.')) then
				ds_retorno_w := 'N';
				exit;
			end if;
			x	:= x + 1;
		end loop;

end if;

return ds_retorno_w;

end lab_obter_se_somente_numero;
/
