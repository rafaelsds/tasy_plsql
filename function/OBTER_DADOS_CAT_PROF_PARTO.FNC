create or replace
function obter_dados_cat_prof_parto(	nr_seq_cat_prof_p		number,
					ie_tipo_inf_p			varchar2)
					return varchar2 is

ds_retorno_w			varchar2(255);
cd_atend_parto_w		cat_profissional_parto.cd_atend_parto%type;
ds_atend_parto_w		cat_profissional_parto.ds_atend_parto%type;
cd_atend_parto_mf_w		cat_profissional_parto.cd_atend_parto_mf%type;
			
begin
if (nr_seq_cat_prof_p is not null) then
	select	cd_atend_parto,
		ds_atend_parto,
		cd_atend_parto_mf
	into	cd_atend_parto_w,
		ds_atend_parto_w,
		cd_atend_parto_mf_w
	from	cat_profissional_parto
	where	nr_sequencia	= nr_seq_cat_prof_p;

	if 	(ie_tipo_inf_p = 'CD_ATEND_PARTO') then
		ds_retorno_w 	:= cd_atend_parto_w;
		
	elsif	(ie_tipo_inf_p = 'DS_ATEND_PARTO') then
		ds_retorno_w 	:= ds_atend_parto_w;
		
	elsif	(ie_tipo_inf_p = 'CD_ATEND_PARTO_MF') then
		ds_retorno_w 	:= cd_atend_parto_mf_w;
	end if;
end if;

return	ds_retorno_w;

end obter_dados_cat_prof_parto;
/