create or replace
function obter_pf_forma_trat_EGK(ds_form_trat_p  Varchar2)
 		    	return number is


nr_seq_w 	number(15);

begin

select	max(nr_sequencia) 
into	nr_seq_w
from	pf_forma_tratamento 
where	upper(ds_forma_tratamento) = upper(ds_form_trat_p);


return	nvl(nr_seq_w, 0);

end obter_pf_forma_trat_EGK;
/