create or replace
function obter_dados_PCR( 	nr_seq_pcr_p	Number,
				ie_opcao_p	Varchar2	)
 		    	return Number is

/*
Quantidade de profissionais	'P'
*/			
			
			
qt_profissionais_w	Number(10) := 0;			
			
begin

if	(ie_opcao_p = 'P') then

	Select	count(*)
	into	qt_profissionais_w	
	from	qua_evento_pcr_prof
	where	nr_seq_pcr = nr_seq_pcr_p;	
	
end if;

if	(qt_profissionais_w > 0) then

	return	qt_profissionais_w;
	
else

	return	1;	

end if;


end obter_dados_PCR;
/
