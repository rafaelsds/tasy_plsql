create or replace
function obter_obser_regra_onco( nr_seq_Atendimento_p        number)
 		    	return varchar2 is

ds_retorno_w         varchar(255) := '';
contador_w	number(10);
begin

if (nr_seq_Atendimento_p is not null)then

	select 	count(*)
	into 	contador_w
	from 	regra_observacao_oncologia;

	if (contador_w > 0) then
	   select	   max(a.ds_observacao)
	   into          ds_retorno_w
	   from  	   paciente_Setor b,
	           	   paciente_atendimento c,
		   regra_observacao_oncologia a
	   where	   b.cd_protocolo = a.cd_protocolo
	   and	   a.nr_seq_medicacao = b.nr_seq_medicacao
	   and	   c.nr_Seq_paciente = b.nr_seq_paciente
	   and	   nr_seq_atendimento = nr_seq_Atendimento_p
	   and	   a.nr_ciclo = c.nr_ciclo
	   and	   a.ds_dia_ciclo = c.ds_dia_ciclo;
	end if;
end if;

return	ds_retorno_w;

end obter_obser_regra_onco;
/