create or replace
function nut_opcao_lib_servico(	nr_seq_opcao_p		number,
				nr_seq_servico_p	number)
				return varchar2 is

qt_servico_w	number(10);
ie_retorno_w	varchar2(1) := 'N';

begin

select	count(*)
into	qt_servico_w
from	nut_tipo_reserva_serv
where	nr_seq_tp_reserva = nr_seq_opcao_p;

if ((qt_servico_w > 0) and (nr_seq_servico_p is not null)) then

	select	count(*)
	into	qt_servico_w
	from	nut_tipo_reserva_serv
	where	nr_seq_tp_reserva = nr_seq_opcao_p
	and	nr_seq_servico = nr_seq_servico_p;
	
	if (qt_servico_w > 0) then
		ie_retorno_w := 'S';
	end if;
	
else
	ie_retorno_w := 'S';
end if;

return ie_retorno_w;

end nut_opcao_lib_servico;
/