create or replace
function obter_nr_aidf_nota_fiscal(	nr_sequencia_p	number)
 		    	return varchar2 is

nr_nota_fiscal_w		varchar2(255);
cd_serie_nf_w			nota_fiscal.cd_serie_nf%type;
cd_estabelecimento_w	number(04,0);
nr_aidf_w				varchar2(20);

begin

/*Utilizada para buscar o numero da autorizacao de impress�o de documento fiscal - AIDF da nota fiscal*/

select	nr_nota_fiscal,
	cd_estabelecimento,
	cd_serie_nf
into	nr_nota_fiscal_w,
	cd_estabelecimento_w,
	cd_serie_nf_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

select	max(nr_id_dief)
into	nr_aidf_w
from	nota_fiscal_aidf
where	cd_serie_nf = cd_serie_nf_w
and	to_number(nr_nota_fiscal_w) between nr_nota_ini and nr_nota_fim;

return	nr_aidf_w;

end obter_nr_aidf_nota_fiscal;
/
