create or replace
function obter_se_integracao_ativa	(	cd_evento_p	number,	
					nr_seq_sistema_destino_p		number)
					return	varchar2 is


ds_result_w	varchar2(1) :=	'N';

begin

if	(cd_evento_p is not null) and
	(nr_seq_sistema_destino_p is not null) then

	select 	nvl(max('S'), 'N')
	into 	ds_result_w
	from 	cliente_integracao c,
		informacao_integracao i
	where 	c.nr_seq_inf_integracao 	= i.nr_sequencia
	and 	i.nr_seq_sistema_destino 	= nr_seq_sistema_destino_p
	and 	i.nr_seq_evento 	= cd_evento_p
	and  	c.ie_situacao 	in ('A','P');
				
end if;	

return ds_result_w;

end obter_se_integracao_ativa;
/
