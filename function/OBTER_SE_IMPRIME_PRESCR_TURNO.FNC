create or replace 
FUNCTION Obter_se_imprime_prescr_turno(
			nr_prescricao_p		Number,
			nr_seq_turno_p		Number,
			cd_local_estoque_p	Number,
			qt_hora_antes_p		Number)
			return number is

qt_imprime_w			Number(10) 	:= 0;

BEGIN

select	count(*)
into	qt_imprime_w
from	prescr_mat_hor b,
	prescr_material x,
	prescr_medica a
where	a.nr_prescricao	= nr_prescricao_p
and	a.nr_prescricao = x.nr_prescricao
and	x.nr_prescricao = b.nr_prescricao
and	x.nr_sequencia	= b.nr_seq_material
and	a.nr_prescricao	= b.nr_prescricao
and	b.nr_seq_turno	= nr_seq_turno_p
and	a.dt_emissao_farmacia is null
and	b.dt_emissao_farmacia is null
and	nvl(b.ie_dispensar_farm,'S') = 'S'
and	b.QT_DISPENSAR_HOR > 0
and	dt_horario	<= Obter_data_fim_turno(b.nr_prescricao,nr_seq_turno_p,qt_hora_antes_p)
and substr(obter_se_imprimi_turno(nr_prescricao_p,nr_seq_turno_p,qt_hora_antes_p,dt_horario),1,1) = 'S'
and	((x.cd_local_estoque = cd_local_estoque_p) or (x.cd_local_estoque is null))
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';

return qt_imprime_w ;

END Obter_se_imprime_prescr_turno;
/
