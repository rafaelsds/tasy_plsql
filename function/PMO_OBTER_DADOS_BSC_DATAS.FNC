create or replace
function pmo_obter_dados_bsc_datas	(	dt_inicial_p	date,
						dt_final_p	date,
						ie_opcao_p		varchar2)
 		    	return number is

qt_retorno_w		number(15,2);			
			
begin


if	(ie_opcao_p	= 'HPI') then

	SELECT (sum (s.qt_earned_value) / sum (s.hrs_real)) hpi   
	into	qt_retorno_w
	FROM (
	SELECT  t.hrs_real,
		(t.pr_etapa * t.hrs_previstas / 100) qt_earned_value
	FROM(
	SELECT  e.qt_hora_prev hrs_previstas, --
			e.PR_ETAPA pr_etapa, --
		e.QT_HORA_REAL hrs_real --
	FROM    proj_projeto p,
			proj_cronograma c, 
			proj_cron_etapa e
	WHERE	p.nr_sequencia = c.nr_seq_proj
	AND     c.nr_sequencia = e.nr_seq_cronograma
	AND     c.ie_situacao  = 'A'  -- only active schedules
	AND     c.DT_APROVACAO is not null  -- only approved schedules
	AND     p.nr_seq_programa is not null
	AND     e.NR_SEQ_SUB_PROJ is null -- only activities that do not represent subprojects
	AND     p.nr_seq_programa <> 80 -- retira programa do pmo servicos
	AND     p.nr_seq_programa <> 83 -- retira programa do pmo servicos
	AND     p.NR_SEQ_ESTAGIO <> 44 -- remove canceled projects
	AND     c.IE_CLASSIFICACAO <> 'N' -- remove PRP/NPI schedules
	AND     e.Dt_Inicio_prev <= trunc(dt_inicial_p)
	AND 	e.Dt_Fim_prev >= fim_dia(dt_final_p)
	AND     e.NR_SEQ_SUB_PROJ is null -- somente atividades que nao possuem subprojeto
	AND  	NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior =  e.nr_sequencia and xx.nr_seq_cronograma = e.nr_seq_cronograma) -- somente subatividades
	) t 
	) s;
elsif (ie_opcao_p	= 'SPI') then
	
	SELECT (sum (s.qt_earned_value) / sum (s.qt_planned_value)) spi    
	into	qt_retorno_w
	FROM (
	SELECT  CASE 
			WHEN (t.dias_uteis_data_atual >= t.dias_uteis)
			  THEN t.hrs_previstas
			ELSE ((t.dias_uteis_data_atual / t.dias_uteis)) * t.hrs_previstas
			END qt_planned_value,
		(t.pr_etapa * t.hrs_previstas / 100) qt_earned_value
	FROM(
	SELECT  e.qt_hora_prev hrs_previstas, --
			OBTER_DIAS_UTEIS_PERIODO(e.dt_inicio_prev, e.dt_fim_prev, 1) dias_uteis, --
			OBTER_DIAS_UTEIS_PERIODO(e.dt_inicio_prev, TRUNC(SYSDATE), 1) dias_uteis_data_atual, --
			e.PR_ETAPA pr_etapa --
	FROM    proj_projeto p,
			proj_cronograma c, 
			proj_cron_etapa e
	WHERE	p.nr_sequencia = c.nr_seq_proj
	AND     c.nr_sequencia = e.nr_seq_cronograma
	AND     c.ie_situacao  = 'A' --only active schedules
	AND     c.DT_APROVACAO is not null -- only approved schedules
	AND     e.NR_SEQ_SUB_PROJ is null -- only activities that do not represent subproject
	AND     p.NR_SEQ_ESTAGIO <> 44 -- remove canceled projects
	AND     c.IE_CLASSIFICACAO <> 'N' -- remove PRP/NPI schedules
	AND     p.nr_seq_programa is not null
	AND     p.nr_seq_programa <> 80 -- retira programa do pmo servicos
	AND     p.nr_seq_programa <> 83 -- retira programa do pmo servicos
	AND     e.Dt_Inicio_prev <= trunc(dt_inicial_p)
	AND 	e.Dt_Fim_prev >= fim_dia(dt_final_p)
	AND     e.NR_SEQ_SUB_PROJ is null -- somente atividades que nao possuem subprojeto
	AND  	NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior =  e.nr_sequencia and xx.nr_seq_cronograma = e.nr_seq_cronograma) -- somente subatividades
	) t 
	) s;
	
end if;


return	qt_retorno_w;

end pmo_obter_dados_bsc_datas;
/
