create or replace
function obter_itens_assoc_interv(nr_seq_intervencao_p		number)
 		    	return varchar2 is
			
ds_associados_w		varchar2(254);
ds_retorno_w		varchar2(4000);

cursor c01 is
	select	substr(obter_desc_material(cd_material),1,255)
	into	ds_associados_w
	from   	pe_material_proced
	where  	nr_seq_proc = nr_seq_intervencao_p --2212
	order by 1;

begin

open c01;
loop
fetch c01 into
	ds_associados_w;
exit when c01%NOTFOUND;
	begin

	if	(ds_retorno_w is not null) then
		ds_retorno_w	:= ds_retorno_w||' - '||ds_associados_w;
	else
		ds_retorno_w	:= ds_associados_w;
	end if;
	end; 
end loop;
close c01;


return ds_retorno_w;

end obter_itens_assoc_interv;
/