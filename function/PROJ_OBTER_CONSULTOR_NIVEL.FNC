create or replace
function proj_obter_consultor_nivel(nr_seq_nivel_p	number)
 		    	return number is
qt_consultores_w	number(10);			

begin

select	count(*)
into	qt_consultores_w
from
	(select	z.cd_consultor,
	 max(z.dt_atualizacao) dt_atualizacao
	 from 	proj_consultor_nivel z
	 group by z.cd_consultor) a,
	proj_consultor_nivel b,
	com_canal_consultor c
where	a.dt_atualizacao = b.dt_atualizacao
and	b.cd_consultor = c.cd_pessoa_fisica
and	b.nr_seq_nivel = nr_seq_nivel_p
and	c.ie_situacao = 'A'
and	c.ie_regime_contr = 'CLT'
and	c.ie_consultor_wheb = 'S';

return	qt_consultores_w;

end proj_obter_consultor_nivel;
/