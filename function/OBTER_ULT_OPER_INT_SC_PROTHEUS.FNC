create or replace
function obter_ult_oper_int_sc_protheus(nr_solic_compra_p	number)
 		    	return varchar2 is

ie_operacao_w		varchar2(1);			
			
cursor c01 is
select	nvl(ie_operacao_integ,'I')
from	w_envia_solic_compra_integ
where	nr_solic_compra = nr_solic_compra_p
order by dt_atualizacao;

begin

open C01;
loop
fetch C01 into	
	ie_operacao_w;
exit when C01%notfound;
	begin
	ie_operacao_w := ie_operacao_w;
	end;
end loop;
close C01;

return	ie_operacao_w;

end obter_ult_oper_int_sc_protheus;
/