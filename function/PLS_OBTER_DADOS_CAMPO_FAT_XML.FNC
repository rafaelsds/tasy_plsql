create or replace
function pls_obter_dados_campo_fat_xml(	nr_seq_campo_p		pls_fat_regra_campo.nr_seq_campo%type,
					nr_seq_fatura_p		pls_fatura.nr_sequencia%type,
					nr_seq_conta_proc_p	pls_conta_proc.nr_sequencia%type,
					nr_seq_conta_mat_p	pls_conta_mat.nr_sequencia%type,
					ds_valor_original_p	varchar2 )
						return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ds_retorno_w			varchar2(255);
nr_seq_regra_w			pls_fat_regra_campo_mat.nr_sequencia%type;
nr_seq_pagador_w		pls_contrato_pagador.nr_sequencia%type;
cd_procedimento_w		pls_fat_regra_campo.cd_procedimento%type;
ie_origem_proced_w		pls_fat_regra_campo.ie_origem_proced%type;
cd_area_procedimento_w		pls_fat_regra_campo.cd_area_procedimento%type;
cd_especialidade_w		pls_fat_regra_campo.cd_especialidade%type;
cd_grupo_proc_w			pls_fat_regra_campo.cd_grupo_proc%type;
tx_reducao_acrescimo_w		number(15,4) := null;
ie_via_acesso_w			pls_fat_regra_campo.ie_via_acesso%type;
ie_origem_w			pls_fat_regra_campo.ie_origem_proced%type;
nr_seq_material_w		pls_material.nr_sequencia%type;
nr_seq_estrut_mat_w		pls_material.nr_seq_estrut_mat%type;
nr_seq_estrut_mat_reg_w		pls_material.nr_seq_estrut_mat%type;
ie_tipo_despesa_mat_w		pls_conta_mat.ie_tipo_despesa%type;
nr_seq_grupo_material_w		pls_preco_grupo_material.nr_sequencia%type;
ie_sem_tag_informada_w		pls_fat_regra_campo.ie_somente_sem_tag%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_fat_regra_campo
	where	nr_seq_campo		= nr_seq_campo_p
	and	(nr_seq_pagador		= nr_seq_pagador_w or nr_seq_pagador is null or nr_seq_pagador_w is null)
	and	(cd_procedimento	= cd_procedimento_w or cd_procedimento is null or cd_procedimento_w is null)
	and	((ie_origem_proced	= ie_origem_proced_w) or (ie_origem_proced is null) or 
		(ie_origem_proced_w is null) or (cd_procedimento is null and ie_origem_proced is not null))
	and	(cd_area_procedimento	= cd_area_procedimento_w or cd_area_procedimento is null or cd_area_procedimento_w is null)
	and	(cd_especialidade	= cd_especialidade_w or cd_especialidade is null or cd_especialidade_w is null)
	and	(cd_grupo_proc		= cd_grupo_proc_w or cd_grupo_proc is null or cd_grupo_proc_w is null)
	and	(tx_reducao_acrescimo	= tx_reducao_acrescimo_w or tx_reducao_acrescimo is null or tx_reducao_acrescimo_w is null)
	and	(ie_via_acesso		= ie_via_acesso_w or ie_via_acesso is null or ie_via_acesso_w is null)
	and	(ie_somente_sem_tag	= ie_sem_tag_informada_w or ie_somente_sem_tag = 'N')
	order by
		nr_seq_pagador,
		cd_procedimento,
		cd_area_procedimento,
		cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc,
		tx_reducao_acrescimo,
		ie_via_acesso;
		
Cursor C02 is
	select	nr_sequencia,
		nr_seq_estrut_mat,
		nr_seq_grupo_material
	from	pls_fat_regra_campo_mat
	where	nr_seq_campo		= nr_seq_campo_p
	and	(nr_seq_pagador		= nr_seq_pagador_w or nr_seq_pagador is null or nr_seq_pagador_w is null)
	and	(nr_seq_material	= nr_seq_material_w or nr_seq_material is null or nr_seq_material_w is null)
	and	(ie_tipo_despesa_mat	= ie_tipo_despesa_mat_w or ie_tipo_despesa_mat is null or ie_tipo_despesa_mat_w is null)
	and	(tx_reducao_acrescimo	= tx_reducao_acrescimo_w or tx_reducao_acrescimo is null or tx_reducao_acrescimo_w is null)
	order by
		nr_seq_pagador,
		nr_seq_material,
		nr_seq_estrut_mat,
		ie_tipo_despesa_mat,
		nr_seq_grupo_material,
		tx_reducao_acrescimo;

begin
-- FATURA
if	(nr_seq_fatura_p is not null) then
	select	max(nr_seq_pagador)
	into	nr_seq_pagador_w
	from	pls_fatura
	where	nr_sequencia	= nr_seq_fatura_p;
end if;

select	decode(ds_valor_original_p,null,'S','N')
into	ie_sem_tag_informada_w
from	dual;

-- PROCEDIMENTO
if	(nr_seq_conta_proc_p is not null) then
	select	max(cd_procedimento),
		max(ie_origem_proced),
		max(ie_via_acesso),
		max(tx_reducao_acrescimo_imp)
	into	cd_procedimento_w,
		ie_origem_proced_w,
		ie_via_acesso_w,
		tx_reducao_acrescimo_w
	from	pls_conta_proc
	where	nr_sequencia	= nr_seq_conta_proc_p;	
	
	pls_obter_estrut_proc( cd_procedimento_w, ie_origem_proced_w, cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w, ie_origem_w);
	
	if	(ie_origem_proced_w is null) then
		ie_origem_proced_w := ie_origem_w;
	end if;
end if;

-- PROCEDIMENTO
if	(nr_seq_conta_proc_p is not null) or
	(nr_seq_conta_mat_p is null and nr_seq_campo_p in (1,2,6,8,9,10)) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_regra_w;
	exit when C01%notfound;
		begin
		
		select	max(ds_valor)
		into	ds_retorno_w
		from	pls_fat_regra_valor
		where	nr_seq_regra = nr_seq_regra_w;
		
		end;
	end loop;
	close C01;
end if;

-- MATERIAL
if	(nr_seq_conta_mat_p is not null) then
	select	max(y.nr_sequencia),
		max(y.nr_seq_estrut_mat),
		max(x.ie_tipo_despesa),
		max(x.tx_reducao_acrescimo)
	into	nr_seq_material_w,
		nr_seq_estrut_mat_w,
		ie_tipo_despesa_mat_w,
		tx_reducao_acrescimo_w
	from	pls_conta_mat	x,
		pls_material	y
	where	y.nr_sequencia	= x.nr_seq_material
	and	x.nr_sequencia	= nr_seq_conta_mat_p;
end if;
	
-- MATERIAL
if	(nr_seq_conta_mat_p is not null) or
	(ds_retorno_w is null and nr_seq_conta_proc_p is null and nr_seq_campo_p in (1,2,6,8,9,10)) then
	open C02;
	loop
	fetch C02 into	
		nr_seq_regra_w,
		nr_seq_estrut_mat_reg_w,
		nr_seq_grupo_material_w;
	exit when C02%notfound;
		begin
		
		-- Verificar se o material esta dentro da estrutura de material da regra
		if	((nr_seq_estrut_mat_reg_w is null) or
			(pls_obter_se_estruturas_mat( nr_seq_estrut_mat_reg_w, nr_seq_material_w) = 'S')) and
			((nr_seq_grupo_material_w is null) or
			(pls_se_grupo_preco_material( nr_seq_grupo_material_w, nr_seq_material_w) = 'S'))then			
			select	max(ds_valor)
			into	ds_retorno_w
			from	pls_fat_regra_valor_mat
			where	nr_seq_regra = nr_seq_regra_w;
		end if;
		
		end;
	end loop;
	close C02;
end if;

return	ds_retorno_w;

end pls_obter_dados_campo_fat_xml;
/