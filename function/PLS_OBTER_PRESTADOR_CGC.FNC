create or replace
function pls_obter_prestador_cgc
			(	cd_cgc_prestador_p		Varchar2,
				nr_seq_guia_p			pls_conta.nr_seq_guia%type,
                                cd_estabelecimento_p            estabelecimento.cd_estabelecimento%type default null)
	 		    	return Number is
				
qt_prestador_w		pls_integer;
nr_seq_prestador_w	pls_conta.nr_seq_prestador%type;
nr_seq_prestador_guia_w	pls_guia_plano.nr_seq_prestador%type;
cd_cgc_prestador_guia_w	pls_prestador.cd_cgc%type;
begin

if	(cd_cgc_prestador_p	is not null) then
	select 	sum(t.contador)
	into	qt_prestador_w
        from    (       select  count(1) contador
                        from	pls_prestador
                        where	cd_cgc	= cd_cgc_prestador_p
                        and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                        and     cd_estabelecimento_p    is null
                        union all
                        select  count(1) contador
                        from	pls_prestador
                        where	cd_cgc	= cd_cgc_prestador_p
                        and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                        and     cd_estabelecimento_p    is not null
                        and     cd_estabelecimento      = cd_estabelecimento_p) t;
	
	if	(qt_prestador_w	= 1) then
                select  max(t.nr_sequencia)
                into	nr_seq_prestador_w
                from    (       select 	nr_sequencia
                                from	pls_prestador
                                where	cd_cgc	= cd_cgc_prestador_p
                                and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                                and     cd_estabelecimento_p    is null
                                union all
                                select 	nr_sequencia
                                from	pls_prestador
                                where	cd_cgc	= cd_cgc_prestador_p
                                and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                                and     cd_estabelecimento_p    is not null
                                and     cd_estabelecimento      = cd_estabelecimento_p) t;
	elsif	(qt_prestador_w	> 1) then
		select 	sum(t.contador)
		into	qt_prestador_w	
                from    (       select  count(1) contador
                                from	pls_prestador
                                where	cd_cgc		= cd_cgc_prestador_p
                                and	ie_situacao 	= 'A'
                                and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                                and     cd_estabelecimento_p    is null
                                union all
                                select  count(1) contador
                                from	pls_prestador
                                where	cd_cgc		= cd_cgc_prestador_p
                                and	ie_situacao 	= 'A'
                                and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                                and     cd_estabelecimento_p    is not null
                                and     cd_estabelecimento      = cd_estabelecimento_p) t;
		
		if	(qt_prestador_w	= 1) then
			select 	max(t.nr_sequencia)
			into	nr_seq_prestador_w
                        from    (       select  nr_sequencia
                                        from	pls_prestador
                                        where	cd_cgc		= cd_cgc_prestador_p
                                        and	ie_situacao 	= 'A'
                                        and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                                        and     cd_estabelecimento_p    is null
                                        union all
                                        select  nr_sequencia
                                        from	pls_prestador
                                        where	cd_cgc		= cd_cgc_prestador_p
                                        and	ie_situacao 	= 'A'
                                        and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                                        and     cd_estabelecimento_p    is not null
                                        and     cd_estabelecimento      = cd_estabelecimento_p) t;


		elsif	(qt_prestador_w	> 1) then
			select	max(nr_seq_prestador)
			into	nr_seq_prestador_guia_w
			from	pls_guia_plano
			where	nr_sequencia	= nr_seq_guia_p;

			if	(nr_seq_prestador_guia_w	is not null) then
				select	max(cd_cgc)
				into	cd_cgc_prestador_guia_w
				from	pls_prestador
				where	nr_sequencia	= nr_seq_prestador_guia_w;
				
				if	(cd_cgc_prestador_guia_w = cd_cgc_prestador_p) then
					nr_seq_prestador_w	:= nr_seq_prestador_guia_w;
				end if;
			end if;
			
			if	(nr_seq_prestador_w	is null) then
				select	min(t.nr_sequencia)
				into	nr_seq_prestador_w
                                from    (       select  nr_sequencia
                                                from	pls_prestador
                                                where	cd_cgc		= cd_cgc_prestador_p
                                                and	ie_situacao	= 'A'
                                                and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                                                and     cd_estabelecimento_p    is null
                                                union all
                                                select  nr_sequencia
                                                from	pls_prestador
                                                where	cd_cgc		= cd_cgc_prestador_p
                                                and	ie_situacao	= 'A'
                                                and	(ie_prestador_matriz	= 'S' or ie_prestador_matriz is null)
                                                and     cd_estabelecimento_p    is not null
                                                and     cd_estabelecimento      = cd_estabelecimento_p) t;
			end if;
			
		end if;

		if	(nr_seq_prestador_w	is null) then
			select	min(t.nr_sequencia)
			into	nr_seq_prestador_w
                        from    (       select  nr_sequencia
                                        from	pls_prestador
                                        where	cd_cgc		= cd_cgc_prestador_p
                                        and	ie_situacao	= 'A'
                                        and     cd_estabelecimento_p    is null
                                        union all
                                        select  nr_sequencia
                                        from	pls_prestador
                                        where	cd_cgc		= cd_cgc_prestador_p
                                        and	ie_situacao	= 'A'
                                        and     cd_estabelecimento_p    is not null
                                        and     cd_estabelecimento      = cd_estabelecimento_p) t;
		end if;
	end if;
	
	if	(nr_seq_prestador_w	is null) then
		select	min(t.nr_sequencia)
		into	nr_seq_prestador_w
                from    (       select  nr_sequencia
                                from	pls_prestador
                                where	cd_cgc		= cd_cgc_prestador_p
                                and	ie_situacao 	= 'A'
                                and     cd_estabelecimento_p    is null
                                union all
                                select  nr_sequencia
                                from	pls_prestador
                                where	cd_cgc		= cd_cgc_prestador_p
                                and	ie_situacao 	= 'A'
                                and     cd_estabelecimento_p    is not null
                                and     cd_estabelecimento      = cd_estabelecimento_p) t;
		
		if	(nr_seq_prestador_w	is null) then
			select	min(t.nr_sequencia)
			into	nr_seq_prestador_w
                        from    (       select nr_sequencia
                                        from	pls_prestador
                                        where	cd_cgc		= cd_cgc_prestador_p
                                        and     cd_estabelecimento_p    is null
                                        union all
                                        select nr_sequencia
                                        from	pls_prestador
                                        where	cd_cgc		= cd_cgc_prestador_p
                                        and     cd_estabelecimento_p    is not null
                                        and     cd_estabelecimento      = cd_estabelecimento_p) t;
		end if;
	end if;
end if;

return	nr_seq_prestador_w;

end pls_obter_prestador_cgc;
/
