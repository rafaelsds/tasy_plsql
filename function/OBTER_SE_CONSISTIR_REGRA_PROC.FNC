create or replace
function obter_se_consistir_regra_proc(	cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nr_prescricao_p		number,
					ie_lado_p		varchar2)
 		    	return varchar2 is
			
ie_permite_prescricao_w	varchar2(1) := 'S';
cont_w			number(10,0);
cd_area_procedimento_w	number(15,0);
cd_espec_proced_w	number(15,0);
cd_grupo_proc_w		number(15,0);
cd_convenio_w		number(5,0);
qt_idade_w		number(3,0);
cd_pessoa_fisica_w	varchar2(15);

cursor c01 is
select	ie_permite_prescricao
from	rep_regra_restricao_proc
where	nvl(cd_procedimento, cd_procedimento_p)		= cd_procedimento_p
and	((cd_procedimento is null) or
	 (ie_origem_proced = ie_origem_proced_p))
and	nvl(cd_area_procedimento, cd_area_procedimento_w)	= cd_area_procedimento_w
and	nvl(cd_especialidade, cd_espec_proced_w)		= cd_espec_proced_w
and	nvl(cd_grupo_proc, cd_grupo_proc_w)			= cd_grupo_proc_w
and	((cd_convenio is null) or (cd_convenio = cd_convenio_w))
and	((ie_lado is null) or (ie_lado = ie_lado_p))
and 	nvl(qt_idade_w, 0) between nvl(qt_idade_min, 0) and nvl(qt_idade_max, 999)
order by nvl(cd_convenio,0),
	nvl(cd_area_procedimento,0), 
	nvl(cd_especialidade,0), 
	nvl(cd_grupo_proc,0),
	nvl(cd_procedimento,0);

begin

select	count(*)
into	cont_w
from	rep_regra_restricao_proc;

if	(cont_w > 0) then
	select	nvl(max(cd_area_procedimento),0),
		nvl(max(cd_especialidade),0),
		nvl(max(cd_grupo_proc),0)
	into	cd_area_procedimento_w,
		cd_espec_proced_w,
		cd_grupo_proc_w
	from	estrutura_procedimento_v
	where	cd_procedimento = cd_procedimento_p
	and	ie_origem_proced = ie_origem_proced_p;
	
	select	max(obter_convenio_atendimento(nr_atendimento)),
		max(cd_pessoa_fisica)
	into	cd_convenio_w,
		cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	
	select	max(obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A'))
	into	qt_idade_w
	from	dual;
	
	open C01;
	loop
	fetch C01 into
		ie_permite_prescricao_w;
	exit when C01%notfound;
		ie_permite_prescricao_w := ie_permite_prescricao_w;
	end loop;
	close C01;
	
end if;

return	ie_permite_prescricao_w;

end obter_se_consistir_regra_proc;
/