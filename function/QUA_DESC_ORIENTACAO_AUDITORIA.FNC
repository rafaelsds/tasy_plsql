create or replace 
FUNCTION qua_desc_orientacao_auditoria(
			nr_sequencia_p	Number)
			return varchar2 is

ds_retorno_w	varchar2(2000);
begin

if	(nr_sequencia_p is not null) then
	select	ds_orientacao
	into	ds_retorno_w
	from	qua_auditoria_item
	where	nr_sequencia	= nr_sequencia_p;
end if;
return ds_retorno_w;

end qua_desc_orientacao_auditoria;
/
