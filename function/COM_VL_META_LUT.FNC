create or replace
function com_vl_meta_lut(dt_mes_ref_p	date,
			nr_seq_canal_p	number)
 		    	return number is

vl_retorno_w	number(15,2);
			
begin

select	sum(a.vl_licenca)
into	vl_retorno_w
from	com_modalidade_lic b,
	com_meta_venda a
where	a.nr_seq_mod_licenc = b.nr_sequencia
and	b.ie_modalidade = 'LUT'
and	(a.nr_seq_canal = nr_seq_canal_p or nr_seq_canal_p = 0)
and	trunc(a.dt_mes_ref,'month') between trunc(dt_mes_ref_p,'yyyy') and fim_dia(last_day(dt_mes_ref_p));

return	vl_retorno_w;

end com_vl_meta_lut;
/
