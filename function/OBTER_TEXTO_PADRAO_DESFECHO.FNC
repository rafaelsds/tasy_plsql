create or replace
function obter_texto_padrao_desfecho(cd_estabelecimento_p	in number,
				ie_desfecho_p		in varchar2,
				nm_usuario_p		in varchar2) 
				return number is

nr_sequencia_w	number(10,0);
cd_perfil_ativo_w	number;
				
begin

cd_perfil_ativo_w	:= obter_perfil_ativo;

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from 	texto_padrao
where nr_seq_item_pront = 1103 
and	nvl(ie_opcao_padrao,'S')	= 'S'
and ie_desfecho = ie_desfecho_p  	
and	((nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p) or cd_estabelecimento_p is null)
and	((cd_setor_atendimento is null) or (obter_se_setor_usuario(cd_setor_atendimento, nm_usuario_p) = 'S'))
and	(Obter_Se_Texto_Padrao_lib(nr_sequencia) = 'S')
and	(nvl(cd_perfil,cd_perfil_ativo_w) = cd_perfil_ativo_w);

return nr_sequencia_w;
	
end obter_texto_padrao_desfecho;
/