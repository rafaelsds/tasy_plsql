create or replace
function obter_se_prescr_regra_exame(nr_prescricao_p		number,
				     nr_seq_proc_p		number,	
				     nr_seq_regra_exame_p	number	
				     )
 		    	return varchar2 is

ds_retorno_w	varchar2(1) := 'N';

nr_seq_exame_w		number(10);
nr_seq_proc_interno_w	number(10);

begin


if	((nr_prescricao_p > 0) 	and	(nr_seq_proc_p > 0 ))then
	begin
	
		
	select	nvl(max(nr_seq_exame),0),
		nvl(max(nr_seq_proc_interno),0)
	into	nr_seq_exame_w,
		nr_seq_proc_interno_w
	from 	prescr_procedimento
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia	= nr_seq_proc_p;
	
	
	if (nr_seq_exame_w > 0) then
				
		select  nvl(max('S'),'N')
		into 	ds_retorno_w
		from	cih_exame_regra
		where	(nr_seq_exame = nr_seq_exame_w)
		and	((nr_seq_regra_exame_p = 0) or (nr_seq_regra = nr_seq_regra_exame_p));
	
	else
	
		select  nvl(max('S'),'N')
		into 	ds_retorno_w
		from	cih_exame_regra
		where	nr_seq_proc_interno = nr_seq_proc_interno_w
		and	((nr_seq_regra_exame_p = 0) or (nr_seq_regra = nr_seq_regra_exame_p));
	
	end if;
	
	end;

end if;	

return	ds_retorno_w;

end obter_se_prescr_regra_exame;
/