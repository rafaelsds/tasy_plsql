create or replace
function obter_tempo_higienizacao(	nr_seq_proc_interno_p	number)
					return number is
	
qt_min_higienizacao_w	number(10,0):=0;
	
begin

if	(nvl(nr_seq_proc_interno_p,0) > 0) then
	select	nvl(max(qt_min_higienizacao),0)
	into	qt_min_higienizacao_w
	from	proc_interno
	where	nr_sequencia	= nr_seq_proc_interno_p;
end if;	

return	qt_min_higienizacao_w;

end	obter_tempo_higienizacao;
/