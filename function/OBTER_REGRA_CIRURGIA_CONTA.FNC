create or replace
function obter_regra_cirurgia_conta(nr_interno_conta_p 	varchar2)
 		    	return varchar2 is

ie_retorno_w  		varchar2(2);
qt_regra_w 		number(10);
cd_convenio_w		number(5);

cd_procedimento_w	Number(15,0);
ie_origem_proced_w	Number(10,0);
qt_regra_cirurgia_w		Number(10,0);

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced
	from	procedimento_paciente
	where	nr_interno_conta = nr_interno_conta_p
	and	nr_cirurgia is null
	order by 1;

begin

ie_retorno_w := 'S';

SELECT 	nvl(max(cd_convenio_parametro),0)
into    cd_convenio_w      
FROM   	conta_paciente                           
WHERE  	nr_interno_conta = nr_interno_conta_p;

select 	count(*) 
into 	qt_regra_w
from 	conv_regra_exige_cir
where 	cd_convenio = cd_convenio_w;


if 	(qt_regra_w > 0) then

	-- Verificacao dos procedimentos da conta
	open C01;
	loop
	fetch C01 into
		cd_procedimento_w,
		ie_origem_proced_w;
	exit when C01%notfound;
		begin
		select	count(*)
		into	qt_regra_cirurgia_w
		from	conv_regra_exige_cir
		where	cd_procedimento = cd_procedimento_w
		and 	ie_origem_proced = ie_origem_proced_w
		and	cd_convenio = cd_convenio_w;
		
		if  	(qt_regra_cirurgia_w > 0) then
			ie_retorno_w := 'N';
			exit;
		end if;
		
		end;
	end loop;
	close C01;
	
end if;

return	ie_retorno_w;

end obter_regra_cirurgia_conta;
/
