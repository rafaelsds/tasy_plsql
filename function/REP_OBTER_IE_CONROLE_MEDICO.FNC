create or replace
function rep_obter_ie_conrole_medico(cd_material_p	number)
 		    	return number is

ie_controle_medico_w	number(1);
			
begin
if	(cd_material_p is not null) then
	begin
	select	max(obter_controle_medic(cd_material,wheb_usuario_pck.get_cd_estabelecimento,ie_controle_medico,null,null,null))
	into		ie_controle_medico_w
	from		material
	where	cd_material = cd_material_p;
	end;
end if;

return	ie_controle_medico_w;

end rep_obter_ie_conrole_medico;
/
