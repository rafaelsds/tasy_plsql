create or replace function	get_file_type(version_number_p number)

	return number AS file_count number;
        version_status_w NUMBER;

        BEGIN
        
         select a.IE_STATUS
                         into version_status_w
                         from mims_version a
                         where a.NR_SEQUENCIA = version_number_p;

                SELECT

                        COUNT(*)

                INTO

                        file_count

                FROM

                        MIMS_FILE

                WHERE

                        NR_MIMS_VERSION=version_number_p;

                

                IF (file_count=0) THEN

                        RETURN 8;

                ELSIF(file_count=1) THEN

                        RETURN 9;

                ELSIF(file_count=2) THEN

                        RETURN 5;

                ELSIF(file_count=3) THEN

                        RETURN 7;

                ELSIF(file_count=4) THEN

                        RETURN 1;

                ELSIF(file_count=5) THEN

                        RETURN 2;

                ELSIF(file_count=6) THEN

                        RETURN 3;

                ELSIF(file_count=7) THEN

                        RETURN 4;

                ELSIF(file_count=8) THEN

                        RETURN 18;

                ELSIF(file_count=9) THEN

                        RETURN 15;

                ELSIF(file_count=10) THEN

                        RETURN 11;

                ELSIF(file_count=11) THEN

                        RETURN 16;

                ELSIF(file_count=12) THEN

                        RETURN 14;

                ELSIF(file_count=13) THEN

                        RETURN 13;

                ELSIF(file_count=14) THEN

                        RETURN 12;

                ELSIF(file_count=15) THEN

                        RETURN 10;

                ELSIF(file_count=16) THEN

                        RETURN 17;

                ELSIF(file_count=17) THEN

                        RETURN 19;
						
                 ELSIF(file_count=18) THEN

                        RETURN 20;
						
				ELSIF(file_count=19) THEN

                        RETURN 21;
						
				ELSIF(file_count=20) THEN

                        RETURN 22;
						
				ELSIF(file_count=21) THEN

                        RETURN 23;
				
				ELSIF(file_count=22) THEN
					RETURN 24;
					
				ELSIF(file_count=23) THEN
					RETURN 25;
						
				ELSIF(file_count=24) THEN
                 
                         if(version_status_w is null) then
                            RETURN 0;
                         else 
                            RETURN 99;
                         end if;
 
                ELSE

                        RETURN 0;

                END IF;

        END get_file_type;
/
