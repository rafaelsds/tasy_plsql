create or replace
function pls_consistir_gerar_tit_rec(nr_seq_ptu_fatura_p	number)
					return varchar2 is	
					
ds_retorno_w		varchar2(1);
nr_titulo_pagar_w	number(10);
ie_situacao_titulo_w	varchar2(2);
qt_baixa_glosa_w	number(10);

begin
/* Tratamento para a gera��o de t�tulo a receber a partir da contesta��o */
if	(nr_seq_ptu_fatura_p is not null) then
	select	max(b.nr_titulo),
		max(b.ie_situacao)
	into	nr_titulo_pagar_w,
		ie_situacao_titulo_w
	from	titulo_pagar b,
		ptu_fatura a
	where	a.nr_titulo	= b.nr_titulo
	and	a.nr_sequencia	= nr_seq_ptu_fatura_p;
	
	select	count(*)
	into	qt_baixa_glosa_w
	from	titulo_pagar_baixa a
	where	a.nr_titulo	= nr_titulo_pagar_w
	and	a.vl_glosa	> 0;
	
	if	(qt_baixa_glosa_w = 0) and
		(ie_situacao_titulo_w = 'L') then
		ds_retorno_w := 'S';
	else
		ds_retorno_w := 'N';
	end if;
end if;

return	ds_retorno_w;

end pls_consistir_gerar_tit_rec;
/