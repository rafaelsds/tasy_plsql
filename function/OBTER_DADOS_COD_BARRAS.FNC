create or replace
function OBTER_DADOS_COD_BARRAS	(nr_bloqueto_p	varchar2,
					ie_opcao_p	varchar2)
					return	varchar2 is

/*	ie_opcao_p

B	banco
A	ag�ncia
DA	d�gito ag�ncia
C	conta
DC	d�gito conta
V	valor bloqueto
DT	vencimento

*/

cd_banco_w		varchar2(3);
cd_agencia_bancaria_w	varchar2(8);
ie_digito_agencia_w	varchar2(5);
nr_conta_w		varchar2(20);
ie_digito_conta_w	varchar2(5);
vl_bloqueto_w		number(15,2);
dt_vencimento_w		date;

begin

select	substr(nr_bloqueto_p,1,3)
into	cd_banco_w
from	dual;

if	(ie_opcao_p	<> 'B') then

	/* HSBC */
	if	(cd_banco_w = '399') then

		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,31,4);
			
		elsif	(ie_opcao_p	= 'DA') then
		
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,31,4));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,35,7);
			
		elsif	(ie_opcao_p 	= 'DC') then
			
			ie_digito_conta_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,35,7));
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
			
		end if;
		
		
	/* Ita� */
	elsif	(cd_banco_w = '341') then
	
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,32,4);
			
		elsif	(ie_opcao_p	= 'DA') then
			
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,32,4));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,36,5);
		
		elsif	(ie_opcao_p	= 'DC') then
		
			ie_digito_conta_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,36,5));
			
		elsif	(ie_opcao_p	= 'V') then
			
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;

	/* Banco do Brasil */
	elsif	(cd_banco_w = '001') then
	
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,32,5);
		
		elsif	(ie_opcao_p	= 'DA') then
		
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,32,5));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,37,6);
			
		elsif	(ie_opcao_p	= 'DC') then
		
			ie_digito_conta_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,37,6));
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;

	/* Bradesco */
	elsif	(cd_banco_w in ('237', '291')) then
	
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,20,4);
		
		elsif	(ie_opcao_p	= 'DA') then
		
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,20,4));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,37,7);
			
		elsif	(ie_opcao_p	= 'DC') then
		
			ie_digito_conta_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,37,7));
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;

	/* Caixa Economica Federal */
	elsif	(cd_banco_w = '104') then
	
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,20,4);
		
		elsif	(ie_opcao_p	= 'DA') then
		
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,20,4));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,37,8);
			
		elsif	(ie_opcao_p	= 'DC') then
		
			ie_digito_conta_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,37,8));
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;
		
		
	/* Unibanco */
	elsif	(cd_banco_w = '409') then
	
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,28,4);
		
		elsif	(ie_opcao_p	= 'DA') then
		
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,28,4));
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;
		
	/* ABN Amro Real SA */
	elsif	(cd_banco_w = '356') then
	
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,20,4);
		
		elsif	(ie_opcao_p	= 'DA') then
		
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,20,4));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,24,7);
			
		elsif	(ie_opcao_p	= 'DC') then
		
			ie_digito_conta_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,24,7));
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;
		
	/* Citibank */
	elsif	(cd_banco_w = '745') then
			
		if	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;

	/* Banco Santander Banespa */
	elsif	(cd_banco_w = '033') then
			
		if	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,21,7);
		end if;
		
	/*Banco Safra*/	
	elsif	(cd_banco_w = '422') then
	
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,20,4);
		
		elsif	(ie_opcao_p	= 'DA') then
		
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,20,4));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,37,7);
			
		elsif	(ie_opcao_p	= 'DC') then
		
			ie_digito_conta_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,37,7));
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;
		
	/*Banco Industrial e Comercial*/	
	elsif	(cd_banco_w = '320') then
	
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,20,4);
		
		elsif	(ie_opcao_p	= 'DA') then
		
			ie_digito_agencia_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,20,4));
			
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,37,7);
			
		elsif	(ie_opcao_p	= 'DC') then
		
			ie_digito_conta_w	:= Calcula_Digito('Modulo11',substr(nr_bloqueto_p,37,7));
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;	
	
	/* Unicred */
	elsif	(cd_banco_w = '136') then
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,20,4);
		
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,24,10);
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;	
		
	/* Sicredi */
	elsif	(cd_banco_w = '748') then
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,31,4);
		
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,37,5);
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;	
		
	/* Banrisul */
	elsif	(cd_banco_w = '041') then
		if	(ie_opcao_p	= 'A') then
		
			cd_agencia_bancaria_w	:= substr(nr_bloqueto_p,22,4);
		
		elsif	(ie_opcao_p	= 'C') then
		
			nr_conta_w		:= substr(nr_bloqueto_p,26,7);
			
		elsif	(ie_opcao_p	= 'V') then
		
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;	
		
	else
		/* Caso n�o seja dos bancos acima, retira o valor do bloqueto e data de vencimento da posi��o padr�o da Febraban */
		if (ie_opcao_p = 'V') then
			
			vl_bloqueto_w		:= to_number(substr(nr_bloqueto_p,10,10))/100;
			
		elsif	(ie_opcao_p	= 'DT') then
		
			dt_vencimento_w		:= to_date('07/10/1997','dd/mm/yyyy') + somente_numero(substr(nr_bloqueto_p,6,4));
		end if;
	end if;
	
end if;

if	(ie_opcao_p	= 'B') then
	return	cd_banco_w;
elsif	(ie_opcao_p	= 'A') then
	return	cd_agencia_bancaria_w;
elsif	(ie_opcao_p	= 'DA') then
	return	ie_digito_agencia_w;
elsif	(ie_opcao_p	= 'C') then
	return	nr_conta_w;
elsif	(ie_opcao_p	= 'DC') then
	return	ie_digito_conta_w;
elsif	(ie_opcao_p	= 'V') and (nvl(vl_bloqueto_w,-1) > -1) then
	return	to_char(vl_bloqueto_w);
elsif	(ie_opcao_p	= 'DT') then
	return	to_char(dt_vencimento_w,'dd/mm/yyyy');
else
	return	null;
end if;

end	OBTER_DADOS_COD_BARRAS;
/