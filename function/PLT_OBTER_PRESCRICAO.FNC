create or replace
function plt_obter_prescricao (	nr_prescricao_p			number,
				nr_prescricoes_p		varchar2,
				nr_prescr_titulo_p		number,
				nm_usuario_p			varchar2,
				cd_pessoa_fisica_p		varchar2,
				ie_opcao_p				varchar2)
				return number is
				
/*
	ie_opcao_p
	P = Prescri��o do usu�rio que est� estendendo
	U - �ltima(maior) prescri��o do paciente
*/
			
nr_prescricao_w		number(14,0);

begin

if	(nr_prescricao_p is not null) then

	nr_prescricao_w	:= nr_prescricao_p;
	
elsif	(nr_prescricoes_p is not null) then

	if (ie_opcao_p = 'U') then
		nr_prescricao_w := plt_obter_max_nr_prescricao(nr_prescricoes_p);
	else
		nr_prescricao_w := plt_obter_max_nr_prescr_user(nr_prescricoes_p, cd_pessoa_fisica_p, nm_usuario_p);
	end if;
	
elsif	(nr_prescr_titulo_p is not null) and
	(nr_prescr_titulo_p <> 1) then

	nr_prescricao_w := to_number(nr_prescr_titulo_p);
	
end if;

return nr_prescricao_w;

end plt_obter_prescricao;
/