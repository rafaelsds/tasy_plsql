create or replace
function Obter_base_calc_tit_bordero ( nr_bordero_p	number)
 		    	return number is

vl_base_calculo_w	number(15,2);
vl_base_calculo_tot_w	number(15,2);

Cursor C01 is
select	max(vl_base_calculo)
from	titulo_pagar_imposto b,
	titulo_pagar_bordero_v a
where	a.nr_titulo = b.nr_titulo
and	a.nr_bordero = nr_bordero_p
group by a.nr_titulo;

begin

if	(nr_bordero_p is not null) then
	open C01;
	loop
	fetch C01 into
		vl_base_calculo_w;
	exit when C01%notfound;
		begin
		vl_base_calculo_tot_w := nvl(vl_base_calculo_tot_w,0) + nvl(vl_base_calculo_w,0);
		end;
	end loop;
	close C01;
end if;

return	vl_base_calculo_tot_w;

end Obter_base_calc_tit_bordero;
/