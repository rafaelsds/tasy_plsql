CREATE OR REPLACE
FUNCTION OBTER_PROCED_PRINC_SUS_conta
                         	(nr_interno_conta_p		number)
				RETURN Number IS

ie_tipo_laudo_sus_w		number(5);
cd_procedimento_solic_w		Number(15);
cd_procedimento_realiz_w	number(15);
ie_politraumatizado_w		varchar2(1);
ie_cirurgia_multipla_w		varchar2(1);
dt_emissao_w			date;



CURSOR	C01 IS
select	cd_procedimento_solic,
	ie_tipo_laudo_sus,
	dt_emissao
from 	sus_laudo_paciente 
where 	ie_tipo_laudo_sus	in (0,1,9)
and	nr_interno_conta 	= nr_interno_conta_p
order by 2, 3;

BEGIN

cd_procedimento_realiz_w	:= 0;
ie_politraumatizado_w	:= 'N';
ie_cirurgia_multipla_w	:= 'N';

OPEN C01;
LOOP
FETCH C01 	into
		cd_procedimento_solic_w,
		ie_tipo_laudo_sus_w,
		dt_emissao_w;
	exit 	when c01%notfound;
		begin
		if	(ie_tipo_laudo_sus_w in(0,1)) then
			cd_procedimento_realiz_w	:= cd_procedimento_solic_w;
		end if;
		if	(cd_procedimento_solic_w in
			(39000001,70000000,40290000,33000000)) then
			ie_politraumatizado_w	 := 'S';
		end if;	
		if	(cd_procedimento_solic_w in	(31000002)) then
			ie_cirurgia_multipla_w	 := 'S';
		end if;	
		if	(ie_tipo_laudo_sus_w = 9) and
			(ie_politraumatizado_w	= 'S' or
			 ie_cirurgia_multipla_w	= 'S')	then
			cd_procedimento_realiz_w	:= cd_procedimento_solic_w;
		end if;
		end;
END LOOP;
CLOSE C01;

RETURN	cd_procedimento_realiz_w;
	
END OBTER_PROCED_PRINC_SUS_conta;
/
