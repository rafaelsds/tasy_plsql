create or replace
function obter_se_prescr_erro_item(
		nr_prescricao_p	number)
 		return number is

ie_erro_w	varchar2(1);
nr_seq_item_w	number(14);
begin
if	(nr_prescricao_p is not null) then
	begin
	
	select	max(nr_sequencia)
	into	nr_seq_item_w
	from	prescr_material
	where	nr_prescricao	= nr_prescricao_p;
	
	select	decode(count(*),0,'N','S')
	into	ie_erro_w
	from	prescr_medica_erro
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_medic	> nr_seq_item_w;
	
	if	(ie_erro_w = 'N') then
		begin
		nr_seq_item_w	:= null;
		end;
	end if;
	end;
end if;
return	nr_seq_item_w;
end obter_se_prescr_erro_item;
/