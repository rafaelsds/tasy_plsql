create or replace
function obter_se_mostra_conv_bo(cd_convenio_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(01);
cd_estabelecimento_w	number(04,0) := wheb_usuario_pck.get_cd_estabelecimento;

begin
if	(nvl(cd_convenio_p,0) > 0) then
	begin
	select	ie_utiliza_conv_bo
	into	ds_retorno_w
	from	convenio_estabelecimento
	where	cd_convenio 		= cd_convenio_p
	and	cd_estabelecimento 	= cd_estabelecimento_w;
	exception
	when others then
		ds_retorno_w := 'S';
	end;
end if;

return	nvl(ds_retorno_w,'S');

end obter_se_mostra_conv_bo;
/