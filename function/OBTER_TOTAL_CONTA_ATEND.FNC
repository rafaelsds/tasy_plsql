CREATE OR REPLACE FUNCTION OBTER_TOTAL_CONTA_ATEND(	nr_atendimento_p number)
				RETURN NUMBER IS

vl_conta_w		number(15,2) := 0;
vl_total_w		number(15,2) := 0;
nr_interno_conta_w	number;

cursor C01 is
Select	nr_interno_conta
from	conta_paciente
where	nr_atendimento = nr_atendimento_p;

BEGIN

open C01;
loop
	fetch C01 into
		nr_interno_conta_w;
	exit when C01%notfound;

	Select	sum(nvl(vl_procedimento,0) + nvl(vl_material,0))
	into	vl_conta_w
	from	conta_paciente_resumo
	where	nr_interno_conta = nr_interno_conta_w;

	vl_total_w	:= vl_total_w + vl_conta_w;
end loop;
close C01;

RETURN vl_total_w;

END OBTER_TOTAL_CONTA_ATEND;
/