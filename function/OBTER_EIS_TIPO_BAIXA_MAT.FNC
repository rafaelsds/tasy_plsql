create or replace
function obter_eis_tipo_baixa_mat(		
		cd_tipo_baixa_p	number,
		ie_ocorrencia_p	varchar2)
		return varchar is

/* ie ocorrencia

R  	Requisicao
P	Prescri��o
D	Devoluc�o
*/

ds_tipo_baixa_w	varchar2(50);

begin

if	(ie_ocorrencia_p = 'R') then
	begin
	select	ds_motivo_baixa
	into	ds_tipo_baixa_w
	from	sup_motivo_baixa_req
	where	ie_situacao = 'A'
	and	nr_sequencia = cd_tipo_baixa_p;
	exception
	when others then
		ds_tipo_baixa_w	:=	substr(obter_valor_dominio(40, to_char(cd_tipo_baixa_p)),1,40);
	end;
else
	select	nvl(max(ds_tipo_baixa), wheb_mensagem_pck.get_texto(301935)) -- 301935 - N�o informado
	into	ds_tipo_baixa_w
	from	tipo_baixa_prescricao
	where	ie_prescricao_devolucao = ie_ocorrencia_p
	and	cd_tipo_baixa = cd_tipo_baixa_p;
end if;

return ds_tipo_baixa_w;

end obter_eis_tipo_baixa_mat;
/