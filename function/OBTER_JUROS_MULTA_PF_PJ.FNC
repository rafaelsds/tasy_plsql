create or replace
function OBTER_JUROS_MULTA_PF_PJ
		(cd_estabelecimento_p	number,
		cd_pessoa_fisica_p	varchar2,
		cd_cgc_p		varchar2,
		ie_juro_multa_p	varchar2) return number is

/*

'J' - juros
'M' - multa

*/

pr_juro_padrao_w		number(7,4);
pr_multa_padrao_w		number(7,4);
pr_juro_pj_w			number(7,4);
pr_multa_pj_w			number(7,4);
pr_juro_w			number(7,4);
pr_multa_w			number(7,4);
pr_resultado_w			number(7,4);
ie_juros_multa_pj_w		varchar2(5);

cursor c01 is
select	a.tx_juros,
	a.tx_multa
from	regra_taxa_pessoa a
where	a.cd_pessoa_fisica	is null
and	a.cd_cgc		= cd_cgc_p
union
select	a.tx_juros,
	a.tx_multa
from	regra_taxa_pessoa a
where	a.cd_cgc		is null
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
union
select	a.tx_juros,
	a.tx_multa
from	regra_taxa_pessoa a
where	not exists
	(select	1
	from	regra_taxa_pessoa x
	where	(x.cd_cgc = cd_cgc_p or x.cd_pessoa_fisica = cd_pessoa_fisica_p))
and	cd_pessoa_fisica	is null
and	cd_cgc			is null;

begin

select	pr_juro_padrao,
	pr_multa_padrao,
	pr_juro_pj,
	pr_multa_pj,
	ie_juros_multa_pj
into	pr_juro_padrao_w,
	pr_multa_padrao_w,
	pr_juro_pj_w,
	pr_multa_pj_w,
	ie_juros_multa_pj_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_p;

pr_juro_w	:= pr_juro_padrao_w;
pr_multa_w	:= pr_multa_padrao_w;

if	(cd_cgc_p is not null) then

	if	(nvl(pr_juro_pj_w,0) > 0) or (ie_juros_multa_pj_w = 'S') then
		pr_juro_w	:= pr_juro_pj_w;
	end if;

	if	(nvl(pr_multa_pj_w,0) > 0) or (ie_juros_multa_pj_w = 'S') then
		pr_multa_w	:= pr_multa_pj_w;
	end if;

end if;

open c01;
loop
fetch c01 into
	pr_juro_w,
	pr_multa_w;
exit when c01%notfound;
end loop;
close c01;

pr_resultado_w			:= 0;
if	(upper(ie_juro_multa_p) = 'M') then
	pr_resultado_w		:= pr_multa_w;
elsif	(upper(ie_juro_multa_p) = 'J') then
	pr_resultado_w		:= pr_juro_w;
end if;

return	pr_resultado_w;

end OBTER_JUROS_MULTA_PF_PJ;
/