create or replace
function ICESP_obter_prox_setor(nr_atendimento_p	number,
				nr_sequencia_p		number)
 		    	return number is
			
cd_setor_atendimento_w	number(10);
nr_seq_setor_w		number(10);

begin

select 	min(a.nr_sequencia) 
into	nr_seq_setor_w
from 	atend_paciente_unidade a 
where 	a.nr_atendimento = nr_atendimento_p 
and 	a.nr_sequencia > nr_sequencia_p 
and 	a.ie_passagem_setor = 'N';

select MIN(a.cd_setor_atendimento)
into	cd_setor_atendimento_w
from 	atend_paciente_unidade a
where 	a.nr_atendimento = nr_atendimento_p 
and 	a.nr_sequencia = nr_seq_setor_w;

return	cd_setor_atendimento_w;

end ICESP_obter_prox_setor;

/
