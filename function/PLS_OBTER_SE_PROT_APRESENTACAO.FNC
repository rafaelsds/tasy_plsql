create or replace
function pls_obter_se_prot_apresentacao(nr_seq_protocolo_p		Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(2);
ie_apresentacao_w	Varchar2(3);

begin
select	nvl(ie_apresentacao,'A')
into	ie_apresentacao_w
from	pls_protocolo_conta
where	nr_sequencia = nr_seq_protocolo_p;

if	(ie_apresentacao_w = 'R') then
	ds_retorno_w := 'N';
else
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end pls_obter_se_prot_apresentacao;
/
