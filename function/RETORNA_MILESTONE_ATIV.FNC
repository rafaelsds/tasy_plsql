create or replace
function retorna_milestone_ativ(nr_sequencia_p		number)
 		    	return number is

ie_milestone_w			varchar2(1);
nr_seq_superior_w		number(10);
nr_seq_milestone_w		number(10);
nr_nivel_w			number(10);
nr_nivel_sup_w			number(10);

begin

	select	a.nr_seq_superior,
		a.ie_milestone,
		proj_obter_nivel_etapa(a.nr_sequencia)
	into	nr_seq_superior_w,
		ie_milestone_w,
		nr_nivel_w
	from	proj_cron_etapa a
	where	a.nr_sequencia = nr_sequencia_p;

	if ie_milestone_w = 'S' then
		nr_seq_milestone_w := nr_sequencia_p;
	else
		select	proj_obter_nivel_etapa(b.nr_sequencia)
		into	nr_nivel_sup_w
		from	proj_cron_etapa b
		where	b.nr_sequencia = nr_seq_superior_w;

		if (nr_seq_superior_w is not null) and (nr_nivel_sup_w < nr_nivel_w) then
			nr_seq_milestone_w := retorna_milestone_ativ(nr_seq_superior_w);
		end if;
	end if;

return	nr_seq_milestone_w;

end retorna_milestone_ativ;
/