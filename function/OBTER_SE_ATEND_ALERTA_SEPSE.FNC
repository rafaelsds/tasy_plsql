create or replace
function obter_se_atend_alerta_sepse(	nr_atendimento_p		atendimento_paciente.nr_atendimento%type,
					cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type)
 		    	return varchar2 is

qt_alerta_w	number(5);
vl_retorno_w	varchar2(1) := 'N';
			
begin

select 	count(*)
into	qt_alerta_w
from 	sepsis_alert
where 	nr_atendimento = nr_atendimento_p
and	cd_pessoa_fisica = cd_pessoa_fisica_p;

if (qt_alerta_w > 0) then
	vl_retorno_w := 'S';
end if;

return	vl_retorno_w;

end obter_se_atend_alerta_sepse;
/
