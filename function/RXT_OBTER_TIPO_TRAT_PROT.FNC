create or replace
function Rxt_Obter_Tipo_Trat_Prot(
			nr_seq_protocolo_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(5);
			
begin

select	substr(max(rxt_obter_tipo_tratamento(nr_seq_tipo)),1,5)
into	ds_retorno_w
from	rxt_protocolo
where	nr_sequencia	= nr_seq_protocolo_p;

return	ds_retorno_w;

end Rxt_Obter_Tipo_Trat_Prot;
/