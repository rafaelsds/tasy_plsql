create or replace
function obter_exporta_carta_medica(nr_seq_carta_p		number)
 		    	return varchar2 is
				
ds_retorno_w	varchar2(1);

begin

select	nvl(max('S'),'N')
into	ds_retorno_w
from	carta_medica
where	nr_sequencia = nr_seq_carta_p
and	dt_inativacao is null;


return	ds_retorno_w;

end obter_exporta_carta_medica;
/