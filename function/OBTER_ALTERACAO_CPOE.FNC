create or replace
function obter_alteracao_cpoe (nr_atendimento_p	number,
								ie_lista_tipos_p varchar2 default null,
								ie_lista_outros_p varchar2 default null,
								ie_restricao_prescritor_p varchar2 default 'A')
				return date is

max_atualizacao_w	date;
max_atualizacao_2w 	date;
ie_lista_tipos_w	varchar2(200);
nr_registro_w 		number(10);

cursor C01 is
	select	nr_registro
	from	table(lista_pck.obter_lista(ie_lista_outros_p))
	order by nr_registro;

cursor C02 is
	select	nr_registro
	from	table(lista_pck.obter_lista(ie_lista_tipos_p))
	order by nr_registro;

function buscar_todos(nr_registro_p number) return date is
dt_atualizacao_w	date;
begin
	
	select	max(dt_atualizacao)
	into	dt_atualizacao_w
	from	(select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_material a
			where	nr_atendimento = nr_atendimento_p
			and		(((ie_controle_tempo = 'S') and (nr_registro_p = 16)) or
					((ie_controle_tempo = 'N') and (nr_registro_p = 7)) or
					((ie_material = 'S') and (nr_registro_p = 6)))
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_dieta a
			where	nr_atendimento = nr_atendimento_p
			and		(((ie_tipo_dieta = 'O') and (nr_registro_p = 2)) or
					((ie_tipo_dieta = 'E') and (nr_registro_p = 14)) or
					((ie_tipo_dieta = 'S') and (nr_registro_p = 15)) or
					((ie_tipo_dieta = 'J') and (nr_registro_p = 5)) or
					((ie_tipo_dieta = 'L') and (nr_registro_p = 21)) or
					(ie_tipo_dieta in ('P','I') and (nr_registro_p = 8)))
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_recomendacao a
			where	nr_atendimento = nr_atendimento_p
			and 	nr_registro_p = 13
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_gasoterapia a
			where	nr_atendimento = nr_atendimento_p
			and		nr_registro_p = 3
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_procedimento a
			where	nr_atendimento = nr_atendimento_p
			and		nr_registro_p = 12
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_dialise a
			where	nr_atendimento = nr_atendimento_p
			and		nr_registro_p = 1
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null))));
	
	return	dt_atualizacao_w;

end;

function buscar_outros(nr_registro_p number) return date is
dt_atualizacao_w	date;
begin

	select	max(dt_atualizacao)
	into	dt_atualizacao_w
	from	(select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_material a
			where	nr_atendimento = nr_atendimento_p
			and		(((ie_controle_tempo = 'S') and (nr_registro_p <> 16)) or
					((ie_controle_tempo = 'N') and (nr_registro_p <> 7)) or
					((ie_material = 'S') and (nr_registro_p <> 6)))
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_dieta a
			where	nr_atendimento = nr_atendimento_p
			and		(((ie_tipo_dieta = 'O') and (nr_registro_p <> 2)) or
					((ie_tipo_dieta = 'E') and (nr_registro_p <> 14)) or
					((ie_tipo_dieta = 'S') and (nr_registro_p <> 15)) or
					((ie_tipo_dieta = 'J') and (nr_registro_p <> 5)) or
					((ie_tipo_dieta = 'L') and (nr_registro_p <> 21)) or
					(ie_tipo_dieta in ('P','I') and (nr_registro_p <> 8)))
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_recomendacao a
			where	nr_atendimento = nr_atendimento_p
			and 	nr_registro_p <> 13
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_gasoterapia a
			where	nr_atendimento = nr_atendimento_p
			and		nr_registro_p <> 3
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_procedimento a
			where	nr_atendimento = nr_atendimento_p
			and		nr_registro_p <> 12
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
			union all
			select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
			from	cpoe_dialise a
			where	nr_atendimento = nr_atendimento_p
			and		nr_registro_p <> 1
			and		((ie_restricao_prescritor_p = 'A') or
					((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
					((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
					((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null))));
	
	return	dt_atualizacao_w;

end;

begin
if	(nr_atendimento_p is not null) and (obter_se_atendimento_alta(nr_atendimento_p) = 'N') then
		
	if (ie_lista_outros_p is not null) then
	
		open C01;
		loop
		fetch C01 into	
			nr_registro_w;
		exit when C01%notfound;
			begin
			
			max_atualizacao_2w := buscar_outros(nr_registro_w);
			
			if (max_atualizacao_w is null) or
			   (max_atualizacao_2w > max_atualizacao_w) then
				max_atualizacao_w := max_atualizacao_2w;
			end if;
			
			end;
		end loop;
		close C01;
		
	elsif (ie_lista_tipos_p is not null) then
		
		open C02;
		loop
		fetch C02 into	
			nr_registro_w;
		exit when C02%notfound;
			begin
			
			max_atualizacao_2w := buscar_todos(nr_registro_w);
			
			if (max_atualizacao_w is null) or
			   (max_atualizacao_2w > max_atualizacao_w) then
				max_atualizacao_w := max_atualizacao_2w;
			end if;
			
			end;
		end loop;
		close C02;

	elsif (nvl(ie_restricao_prescritor_p,'A') <> 'A') then
	
		select	max(dt_atualizacao)
		into	max_atualizacao_w
		from	(select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_material a
				where	nr_atendimento = nr_atendimento_p
				and		(((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
						((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
						((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_dieta a
				where	nr_atendimento = nr_atendimento_p
				and		(((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
						((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
						((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_recomendacao a
				where	nr_atendimento = nr_atendimento_p
				and		(((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
						((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
						((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_gasoterapia a
				where	nr_atendimento = nr_atendimento_p
				and		(((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
						((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
						((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_procedimento a
				where	nr_atendimento = nr_atendimento_p
				and		(((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
						((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
						((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null)))
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_dialise a
				where	nr_atendimento = nr_atendimento_p
				and		(((ie_restricao_prescritor_p = 'M') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') = 'S')) or
						((ie_restricao_prescritor_p = 'F') and (obter_se_medico(obter_pf_usuario(nvl(a.nm_usuario_nrec,a.nm_usuario),'C'),'M') <> 'S')) or
						((ie_restricao_prescritor_p = 'N') and (nr_sequencia is null))));
	
	else
	
		select	max(dt_atualizacao)
		into	max_atualizacao_w
		from	(select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_material a
				where	nr_atendimento = nr_atendimento_p
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_dieta a
				where	nr_atendimento = nr_atendimento_p
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_recomendacao a
				where	nr_atendimento = nr_atendimento_p
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_gasoterapia a
				where	nr_atendimento = nr_atendimento_p
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_procedimento a
				where	nr_atendimento = nr_atendimento_p
				union all
				select	decode(ie_restricao_prescritor_p,'A',nvl(dt_liberacao_farm,nvl(dt_liberacao_enf,dt_liberacao)),'F',nvl(dt_liberacao_farm,dt_liberacao_enf),'M',dt_liberacao) dt_atualizacao
				from	cpoe_dialise a
				where	nr_atendimento = nr_atendimento_p);
	
	end if;

end if;

return	nvl(max_atualizacao_w,sysdate);

end obter_alteracao_cpoe;
/