create or replace
function mprev_obter_data_fim_ciclo(	ie_tipo_periodo_ciclo_p 	varchar2,
					dt_inicio_ciclo_p 		date,
					qt_total_periodo_ciclo_p 	number)
					return date is

dt_fim_ciclo_w	date;
dt_retorno_w 	date;

begin

if (ie_tipo_periodo_ciclo_p is not null) then
		
	if	( ie_tipo_periodo_ciclo_p = 'A') then
		dt_fim_ciclo_w	:=	pkg_date_utils.add_month(nvl(dt_inicio_ciclo_p, sysdate), 12 * nvl(qt_total_periodo_ciclo_p,0), 0);
	elsif	(ie_tipo_periodo_ciclo_p = 'M') then
		dt_fim_ciclo_w	:=	pkg_date_utils.add_month(nvl(dt_inicio_ciclo_p, sysdate), 1 * nvl(qt_total_periodo_ciclo_p,0), 0);
	elsif	(ie_tipo_periodo_ciclo_p = 'S') then
		dt_fim_ciclo_w	:=	nvl(dt_inicio_ciclo_p, sysdate) + (7 * nvl(qt_total_periodo_ciclo_p,0));
	elsif	(ie_tipo_periodo_ciclo_p = 'D') then
		dt_fim_ciclo_w	:=	nvl(dt_inicio_ciclo_p, sysdate) + (1 * nvl(qt_total_periodo_ciclo_p,0));
	end if;
	
end if;

dt_retorno_w	:= obter_proximo_dia_util(wheb_usuario_pck.get_cd_estabelecimento,dt_fim_ciclo_w);

return	dt_retorno_w;

end mprev_obter_data_fim_ciclo;
/
