create or replace
function OBTER_LOTE_CONTAB_PROTOCOLO
		(nr_seq_protocolo_p	number) return varchar2 is

ds_retorno_w		varchar2(254);
nr_lote_contabil_w	number(10,0);

/*cursor c01 is
select	distinct nr_lote_contabil
from	protocolo_convenio_item_v
where	nr_seq_protocolo	= nr_seq_protocolo_P
and	NVL(nr_lote_contabil,0)	<> 0
order	by nr_lote_contabil;*/

cursor c01 is
	select	distinct nr_lote_contabil
	from 	( 	
		select	a.nr_lote_contabil
		from   	procedimento_paciente a,
			conta_paciente b,
			protocolo_convenio d
		where	d.nr_seq_protocolo	= b.nr_seq_protocolo
		and	a.cd_motivo_exc_conta is null
		and	a.nr_interno_conta   	= b.nr_interno_conta
		and	d.nr_seq_protocolo	= nr_seq_protocolo_p
		and	nvl(a.nr_lote_contabil,0) <> 0
		union all
		select	x.nr_lote_contabil
		from   	material_atend_paciente x,
			conta_paciente y,
			protocolo_convenio z
		where 	z.nr_seq_protocolo = y.nr_seq_protocolo
		and	x.cd_motivo_exc_conta is null
		and 	x.nr_interno_conta = y.nr_interno_conta
		and	z.nr_seq_protocolo	= nr_seq_protocolo_p
		and	nvl(x.nr_lote_contabil,0) <> 0)
	order by nr_lote_contabil;


begin

ds_retorno_w		:= '';
open c01;
loop
fetch c01 into
	nr_lote_contabil_w;
exit when c01%notfound;

	ds_retorno_w	:= nr_lote_contabil_w || ', ' || ds_retorno_w;

end loop;
close c01;

return substr(nvl(ds_retorno_w, '  '), 1, length(nvl(ds_retorno_w, '  ')) - 2);

end OBTER_LOTE_CONTAB_PROTOCOLO;
/