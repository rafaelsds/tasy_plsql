create or replace
function obter_se_minhas_tarefas
	(	nr_seq_worklist_p 	number,
		nr_seq_item_p		number,
		nm_usuario_p		varchar2)
    return varchar2 is

ds_retorno_w  		varchar2(1) default 'N';

begin

select	decode(max(it.nr_sequencia),null,'S','N')
into	ds_retorno_w
from	wl_item it
where	it.nr_sequencia = nr_seq_item_p
and		it.cd_categoria = 'MT'
and		it.ie_situacao = 'A';

if (ds_retorno_w = 'N') then
	select	decode(max(wo.nr_sequencia),null,'N','S') 
	into	ds_retorno_w
	from	wl_worklist wo
	where	wo.nr_sequencia = nr_seq_worklist_p
	and		wo.cd_profissional = obter_pf_usuario(nm_usuario_p, 'C');
end if;

return ds_retorno_w;

end obter_se_minhas_tarefas;
/
