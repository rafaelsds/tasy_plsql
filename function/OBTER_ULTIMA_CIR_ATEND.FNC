CREATE OR REPLACE
FUNCTION obter_ultima_cir_atend ( 	nr_atendimento_p NUMBER)
     								RETURN VARCHAR2 IS

nr_cirurgia_w  NUMBER(10);

BEGIN

IF (NVL(nr_atendimento_p,0) > 0) THEN

	SELECT 	MAX(nr_cirurgia)
	INTO 	nr_cirurgia_w
	FROM  	cirurgia
	WHERE 	nr_atendimento = nr_atendimento_p
	AND   	NVL(dt_inicio_real,dt_inicio_prevista) IS NOT NULL;
		
END IF;

RETURN nr_cirurgia_w;

END obter_ultima_cir_atend;
/