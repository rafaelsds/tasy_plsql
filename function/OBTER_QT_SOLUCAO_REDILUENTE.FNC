create or replace
function OBTER_QT_SOLUCAO_REDILUENTE(	ie_proporcao_dil_p				in varchar2,
										qt_diluente_p					in number,
										qt_ref_diluente_p				in number,
										qt_dose_medic_p					in number,
										cd_unidade_medida_dose_p		in varchar2,
										cd_material_p					in number,
										ie_subtrair_volume_medic_p		in varchar2,
										qt_volume_medic_p				in number
									)
 		    	return number deterministic is

qt_dose_diluicao_w		number(18,6);
qt_conversao_w			material_conversao_unidade.qt_conversao%type;
qt_unitaria_medic_w		number(18,6);
qt_conc_medic_w			number(15,4);
qt_medic_diluido_w		number(15,4);
qt_diluicao_w			material_diluicao.qt_diluicao%type;
qt_solucao_w			material_diluicao.qt_volume%type;

begin

if (ie_subtrair_volume_medic_p = 'S') and
   (qt_volume_medic_p > 0) then
	qt_medic_diluido_w		:= qt_diluente_p - qt_volume_medic_p;
end if;

qt_dose_diluicao_w	:= qt_diluente_p ;

if	(nvl(qt_ref_diluente_p,0) > 0) then

	if	(ie_proporcao_dil_p = 'SC') then

		select	nvl(max(qt_conversao),1)
		into	qt_conversao_w
		from	material_conversao_unidade
		where	cd_unidade_medida = cd_unidade_medida_dose_p
		and		cd_material = cd_material_p;

		qt_unitaria_medic_w := dividir_sem_round(qt_dose_medic_p, qt_conversao_w);

		qt_diluicao_w	:= qt_unitaria_medic_w * qt_ref_diluente_p;						

		if	(qt_medic_diluido_w > 0) then
			
			qt_conc_medic_w 	 := obter_dose_convertida(cd_material_p, qt_volume_medic_p, obter_unid_med_usua('ml'), cd_unidade_medida_dose_p);
			
			qt_solucao_w 		:= dividir_sem_round(qt_dose_medic_p, dividir_sem_round(qt_conc_medic_w, qt_medic_diluido_w));
			qt_dose_diluicao_w	:= qt_diluicao_w - qt_solucao_w;
		end if;
	end if;						
end if;

return	qt_dose_diluicao_w;

end OBTER_QT_SOLUCAO_REDILUENTE;
/