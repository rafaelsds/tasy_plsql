create or replace
function Obter_Idade_Framingham(	cd_pessoa_fisica_p varchar2 ,
									qt_ponto_p		number)
 		    	return varchar2 is
ie_sexo_w	varchar2(10);
begin
select max(ie_sexo)
into	ie_sexo_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(ie_sexo_w	= 'F') then

	if	(qt_ponto_p	< 1) then
		return '<30';
	elsif	(qt_ponto_p	 =1) then
		return '31';
	elsif	(qt_ponto_p	 =2) then
		return '34';
	elsif	(qt_ponto_p	 =3) then
		return '36';
	elsif	(qt_ponto_p	 =4) then
		return '39';
	elsif	(qt_ponto_p	 =5) then
		return '42';
	elsif	(qt_ponto_p	 =6) then
		return '45';
	elsif	(qt_ponto_p	 =7) then
		return '48';
	elsif	(qt_ponto_p	 =8) then
		return '51';
	elsif	(qt_ponto_p	 =9) then
		return '55';
	elsif	(qt_ponto_p	 =10) then
		return '59';
	elsif	(qt_ponto_p	 =11) then
		return '64';
	elsif	(qt_ponto_p	 =12) then
		return '68';
	elsif	(qt_ponto_p	 =13) then
		return '73';
	elsif	(qt_ponto_p	 =14) then
		return '79';
	elsif	(qt_ponto_p	 >= 15) then
		return '80';
	end if;

else
	if	(qt_ponto_p	< 0) then
		return '<30';
	elsif	(qt_ponto_p	 =0) then
		return '30';
	elsif	(qt_ponto_p	 =1) then
		return '32';
	elsif	(qt_ponto_p	 =2) then
		return '34';
	elsif	(qt_ponto_p	 =3) then
		return '36';
	elsif	(qt_ponto_p	 =4) then
		return '38';
	elsif	(qt_ponto_p	 =5) then
		return '40';
	elsif	(qt_ponto_p	 =6) then
		return '42';
	elsif	(qt_ponto_p	 =7) then
		return '45';
	elsif	(qt_ponto_p	 =8) then
		return '48';
	elsif	(qt_ponto_p	 =9) then
		return '51';
	elsif	(qt_ponto_p	 =10) then
		return '54';
	elsif	(qt_ponto_p	 =11) then
		return '57';
	elsif	(qt_ponto_p	 =12) then
		return '60';
	elsif	(qt_ponto_p	 =13) then
		return '64';
	elsif	(qt_ponto_p	 =14) then
		return '68';
	elsif	(qt_ponto_p	 = 15) then
		return '72';
	elsif	(qt_ponto_p	 = 16) then
		return '76';
	elsif	(qt_ponto_p	 >= 17) then
		return '>80';
	end if;
end if;

return	null;

end Obter_Idade_Framingham;
/