create or replace 
function cpoe_obter_hor_assoc_item(
							nr_seq_item_cpoe_p			number,
							cd_item_p					number,
							dt_inicial_p				date,
							dt_final_p					date,
							ie_tipo_item_p				varchar2) return varchar2 is
	
cursor c01 is
select	a.ds_horario,
		a.dt_horario
from	prescr_mat_hor a,
		prescr_material b
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_material = b.nr_sequencia
and		b.nr_seq_mat_cpoe = nr_seq_item_cpoe_p
and		b.cd_material = cd_item_p
and		a.dt_horario between dt_inicial_p and dt_final_p
and		ie_tipo_item_p = 'L'
union
select	a.ds_horario,
		a.dt_horario
from	prescr_mat_hor a,
		prescr_material b,
		prescr_procedimento c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_prescricao = c.nr_prescricao
and		a.nr_seq_material = b.nr_sequencia
and		b.nr_sequencia_proc = c.nr_sequencia
and		c.nr_seq_proc_cpoe = nr_seq_item_cpoe_p
and		b.cd_material = cd_item_p
and		a.dt_horario between dt_inicial_p and dt_final_p
and		ie_tipo_item_p = 'P'
order by dt_horario;


ds_horario_w		varchar2(15);
ds_horarios_w		varchar2(2000);
dt_hor_aux			date;
							
begin

ds_horarios_w := null;
open c01;
loop
fetch c01 into	ds_horario_w,
				dt_hor_aux;
exit when c01%notfound;
	begin
	if (ds_horarios_w is not null) then
		ds_horarios_w := ds_horarios_w || ' ' || ds_horario_w;
	else 
		ds_horarios_w := ds_horario_w;
	end if;
	end;
end loop;
close c01;

return ds_horarios_w;

end cpoe_obter_hor_assoc_item;
/