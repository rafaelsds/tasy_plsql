create or replace
function pls_obter_grupo_regra_mens
			(	nr_seq_regra_p		number,
				ie_tipo_p		varchar2)
 		    	return varchar2 is

/*
	N = Nome do grupo de regras
*/

ds_retorno_w		varchar2(255);
ds_regra_w		varchar2(255);

begin

select	ds_regra
into	ds_regra_w
from	pls_mensalidade_grupo
where	nr_sequencia	= nr_seq_regra_p;

if	(ie_tipo_p	= 'N') then
	ds_retorno_w	:= ds_regra_w;
end if;

return	ds_retorno_w;

end pls_obter_grupo_regra_mens;
/