create or replace
function obter_se_laudos_conta_lib(nr_interno_conta_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1):= 'S';			
qt_laudos_w	number(10):= 0;
begin

if (nr_interno_conta_p is not null)  then

	select	count(*)
	into	qt_laudos_w
	from    	laudo_paciente a,
		atendimento_paciente b,
		conta_paciente c
	where   	a.nr_atendimento = b.nr_atendimento
	and     	b.nr_atendimento = c.nr_atendimento
	and 	c.nr_interno_conta = nr_interno_conta_p
	and  	   a.dt_liberacao is null;

	if (qt_laudos_w > 0) then
		ds_retorno_w := 'N';
	end if;

end if;

return ds_retorno_w;

end obter_se_laudos_conta_lib;
/ 