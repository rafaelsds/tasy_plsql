create or replace
function pls_status_valor_disc (	nr_seq_discussao_p		pls_contestacao_discussao.nr_sequencia%type,
					nr_seq_disc_proc_p		pls_discussao_proc.nr_sequencia%type,
					nr_seq_disc_mat_p		pls_discussao_mat.nr_sequencia%type,
					nm_usuario_p			usuario.nm_usuario%type)
				return varchar2 is

qt_registro_w			pls_integer := 0;
ds_retorno_w			varchar2(255) := null;
vl_auxiliar_w			number(15,2) := 0;
vl_aux_proc_w			number(15,2) := 0;
vl_aux_mat_w			number(15,2) := 0;
vl_ndc_w			number(15,2) := 0;
vl_ndc_proc_w			number(15,2) := 0;
vl_ndc_mat_w			number(15,2) := 0;
vl_reconhecido_w		number(15,2) := 0;
vl_reconhec_proc_w		number(15,2) := 0;
vl_reconhec_mat_w		number(15,2) := 0;
ie_tipo_arquivo_w		pls_lote_discussao.ie_tipo_arquivo%type;
nr_seq_disc_w			pls_contestacao_discussao.nr_sequencia%type;
nr_seq_motivo_glosa_neg_w	pls_discussao_proc.nr_seq_motivo_glosa_neg%type;
nr_seq_lote_contest_w		pls_lote_contestacao.nr_sequencia%type;
ie_pagamento_w			varchar2(2);
nr_seq_contestacao_proc_w	pls_discussao_proc.nr_seq_contestacao_proc%type;
nr_seq_contestacao_mat_w	pls_discussao_mat.nr_seq_contestacao_mat%type;

begin
-- Painel "Discuss�o"
if	(nr_seq_discussao_p is not null) then
	select	nvl(max(ie_tipo_arquivo),0),
		max(nr_seq_lote_contest)
	into	ie_tipo_arquivo_w,
		nr_seq_lote_contest_w
	from	pls_lote_discussao
	where	nr_sequencia = (	select	max(nr_seq_lote)
					from	pls_contestacao_discussao
					where	nr_sequencia = nr_seq_discussao_p);
	
	-- Tipo de pagamento
	ie_pagamento_w := pls_obter_dados_lote_contest( nr_seq_lote_contest_w, 'PC');
		
	-- Somente lote de fechamento
	if	(ie_tipo_arquivo_w not in (1,2,3,4)) then
		select	sum(nvl(vl_prestador,0)) + sum(nvl(vl_assumido,0))
		into	vl_aux_proc_w
		from	pls_discussao_proc
		where	nr_seq_contestacao_proc in	(select	nr_seq_contestacao_proc
							from	pls_discussao_proc
							where	nr_seq_discussao	= nr_seq_discussao_p
							and	nr_seq_motivo_glosa_neg is null);
		
		if	(nvl(vl_aux_proc_w,0) = 0) then
			select	sum(nvl(vl_prestador,0)) + sum(nvl(vl_assumido,0))
			into	vl_aux_mat_w
			from	pls_discussao_mat
			where	nr_seq_contestacao_mat in	(select	nr_seq_contestacao_mat
								from	pls_discussao_mat
								where	nr_seq_discussao	= nr_seq_discussao_p
								and	nr_seq_motivo_glosa_neg is null);
		end if;
		
		if	(nvl(vl_aux_proc_w,0) + nvl(vl_aux_mat_w,0) > 0) then
			qt_registro_w := 1;
		end if;		
		
		if	(ie_tipo_arquivo_w = 1) then
			ds_retorno_w := 'N�o necessita a��o';
			
		elsif	(qt_registro_w > 0) then
			ds_retorno_w := 'A��o pendente';
		else
			ds_retorno_w := 'A��o finalizada';
		end if;
		
		-- Verificar pagamento Integral
		if	(ie_pagamento_w = 'I') then
			qt_registro_w := 0;
				
			select	sum(nvl(vl_ndc,0))
			into	vl_ndc_proc_w
			from	pls_discussao_proc
			where	nr_seq_contestacao_proc in	(select	nr_seq_contestacao_proc
								from	pls_discussao_proc
								where	nr_seq_discussao	= nr_seq_discussao_p
								and	nr_seq_motivo_glosa_neg is null);
			
			if	(nvl(vl_ndc_proc_w,0) = 0) then
				select	sum(nvl(vl_ndc,0))
				into	vl_ndc_mat_w
				from	pls_discussao_mat
				where	nr_seq_contestacao_mat in	(select	nr_seq_contestacao_mat
									from	pls_discussao_mat
									where	nr_seq_discussao	= nr_seq_discussao_p
									and	nr_seq_motivo_glosa_neg is null);
			end if;
			
			if	(nvl(vl_ndc_proc_w,0) + nvl(vl_ndc_mat_w,0) > 0) then
				qt_registro_w := 1;
			end if;
				
			if	(qt_registro_w = 0) then
				ds_retorno_w := 'N�o necessita a��o';
			else
				ds_retorno_w := 'A��o pendente';
			end if;
		
		-- Verificar pagamento Parcial
		elsif	(ie_pagamento_w = 'P') then
			qt_registro_w := 0;
				
			select	sum(nvl(vl_aceito,0))
			into	vl_reconhec_proc_w
			from	pls_discussao_proc
			where	nr_seq_contestacao_proc in	(select	nr_seq_contestacao_proc
								from	pls_discussao_proc
								where	nr_seq_discussao	= nr_seq_discussao_p
								and	nr_seq_motivo_glosa_neg is null);
			
			if	(nvl(vl_reconhec_proc_w,0) = 0) then
				select	sum(nvl(vl_aceito,0))
				into	vl_reconhec_mat_w
				from	pls_discussao_mat
				where	nr_seq_contestacao_mat in	(select	nr_seq_contestacao_mat
									from	pls_discussao_mat
									where	nr_seq_discussao	= nr_seq_discussao_p
									and	nr_seq_motivo_glosa_neg is null);
			end if;
			
			if	(nvl(vl_reconhec_proc_w,0) + nvl(vl_reconhec_mat_w,0) > 0) then
				qt_registro_w := 1;
			end if;
				
			if	(qt_registro_w = 0) then
				ds_retorno_w := 'N�o necessita a��o';
			else
				ds_retorno_w := 'A��o pendente';
			end if;
		end if;
	
	-- Arquivo de inclus�o de questionamento ou de fechameno parcial "N�o necessita"
	elsif	(ie_tipo_arquivo_w in (1,2,3,4)) then
		ds_retorno_w := 'N�o necessita a��o';
	end if;

-- Painel "Discuss�o Procedimento"
elsif	(nr_seq_disc_proc_p is not null) then
	select	max(i.nr_seq_contestacao_proc),
		max(i.nr_seq_discussao)
	into	nr_seq_contestacao_proc_w,
		nr_seq_disc_w
	from	pls_discussao_proc	i
	where	i.nr_sequencia	= nr_seq_disc_proc_p;

	select	sum(nvl(i.vl_prestador,0)) + sum(nvl(i.vl_assumido,0)),
		max(i.nr_seq_motivo_glosa_neg),
		sum(nvl(i.vl_aceito,0)),
		sum(nvl(i.vl_ndc,0))
	into	vl_auxiliar_w,
		nr_seq_motivo_glosa_neg_w,
		vl_reconhecido_w,
		vl_ndc_w
	from	pls_discussao_proc		i,
		pls_contestacao_discussao	c,
		pls_lote_discussao		l
	where	l.nr_sequencia			= c.nr_seq_lote
	and	c.nr_sequencia			= i.nr_seq_discussao
	and	l.ie_status			!= 'C'
	and	i.nr_seq_contestacao_proc	= nr_seq_contestacao_proc_w;
	
-- Painel "Discuss�o Material"
elsif	(nr_seq_disc_mat_p is not null) then
	select	max(i.nr_seq_contestacao_mat),
		max(i.nr_seq_discussao)
	into	nr_seq_contestacao_mat_w,
		nr_seq_disc_w
	from	pls_discussao_mat	i
	where	i.nr_sequencia	= nr_seq_disc_mat_p;

	select	sum(nvl(i.vl_prestador,0)) + sum(nvl(i.vl_assumido,0)),
		max(i.nr_seq_motivo_glosa_neg),
		sum(nvl(i.vl_aceito,0)),
		sum(nvl(i.vl_ndc,0))
	into	vl_auxiliar_w,
		nr_seq_motivo_glosa_neg_w,
		vl_reconhecido_w,
		vl_ndc_w
	from	pls_discussao_mat		i,
		pls_contestacao_discussao	c,
		pls_lote_discussao		l
	where	l.nr_sequencia			= c.nr_seq_lote
	and	c.nr_sequencia			= i.nr_seq_discussao
	and	l.ie_status			!= 'C'
	and	i.nr_seq_contestacao_mat	= nr_seq_contestacao_mat_w;
	
end if;

-- Painel "Discuss�o Procedimento" / "Discuss�o Material"
if	(nr_seq_disc_w is not null) then
	select	nvl(max(ie_tipo_arquivo),0),
		max(nr_seq_lote_contest)
	into	ie_tipo_arquivo_w,
		nr_seq_lote_contest_w
	from	pls_lote_discussao
	where	nr_sequencia = (	select	max(nr_seq_lote)
					from	pls_contestacao_discussao
					where	nr_sequencia = nr_seq_disc_w );
					
	ie_pagamento_w := pls_obter_dados_lote_contest( nr_seq_lote_contest_w, 'PC');
	
	if	(nr_seq_motivo_glosa_neg_w is null) and
		(vl_auxiliar_w = 0) and
		(ie_tipo_arquivo_w != 1) then
		ds_retorno_w	:= 'Necessita a��o';
		
	elsif	(nr_seq_motivo_glosa_neg_w is not null) or
		(vl_auxiliar_w > 0) or
		(ie_tipo_arquivo_w = 1) then
		ds_retorno_w	:= 'N�o necessita a��o';
		
		if	(nr_seq_motivo_glosa_neg_w is not null) or
			(vl_auxiliar_w > 0) then
			ds_retorno_w := 'A��o realizada';
		end if;
	end if;
	
	-- Pagamento Integral
	if	(ie_pagamento_w = 'I') then
		if	(vl_ndc_w = 0) then
			ds_retorno_w := 'N�o necessita a��o';
		
		elsif	(nr_seq_motivo_glosa_neg_w is null) then
			ds_retorno_w := 'A��o pendente';
		end if;
		
	-- Pagamento Parcial
	elsif	(ie_pagamento_w = 'P') then
		if	(vl_reconhecido_w = 0) then
			ds_retorno_w := 'N�o necessita a��o';
		
		elsif	(nr_seq_motivo_glosa_neg_w is null) then
			ds_retorno_w := 'A��o pendente';
		end if;
	end if;
	
	-- Arquivo de inclus�o de questionamento ou de fechameno parcial "N�o necessita"
	if	(ie_tipo_arquivo_w in (1,2,3,4)) then
		ds_retorno_w := 'N�o necessita a��o';
	end if;
end if;

return	ds_retorno_w ;

end pls_status_valor_disc;
/