create or replace
function obter_dados_proj_tasy(	nr_sequencia_p	number,
				ie_opcao_p	varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255) := '';
nm_projeto_w	varchar2(50);
ds_projeto_w	varchar2(255);
ds_grupo_w	varchar2(255);
nr_seq_grupo_w	number(10,0);

begin
if	(nr_sequencia_p > 0) then
	select	nm_projeto,
		ds_projeto,
		nr_seq_grupo
	into	nm_projeto_w,
		ds_projeto_w,
		nr_seq_grupo_w
	from	projeto_tasy
	where	nr_sequencia = nr_sequencia_p;
	
	
	if	(ie_opcao_p = 'N') then
		ds_retorno_w := nm_projeto_w;
	elsif	(ie_opcao_p = 'D') then
		ds_retorno_w := ds_projeto_w;
	elsif	(ie_opcao_p = 'CG') then
		ds_retorno_w := nr_seq_grupo_w;
	elsif	(ie_opcao_p = 'DG') then
		select	ds_grupo_projeto
		into	ds_retorno_w
		from	grupo_projeto
		where	nr_sequencia = nr_seq_grupo_w;
	end if;	
end if;

return	ds_retorno_w;

end obter_dados_proj_tasy;
/