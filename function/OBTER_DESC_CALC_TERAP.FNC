create or replace
FUNCTION Obter_desc_calc_terap(nr_sequencia_p	Number)
			return varchar2 is

ds_dose_terapeutica_w	varchar2(100);
	
begin

if	(nr_sequencia_p is not null) then
	begin
		select	substr(obter_desc_expressao(cd_exp_dose,ds_dose_terapeutica),1,100)
		into	ds_dose_terapeutica_w
		from	regra_dose_terap
		where	nr_sequencia	= nr_sequencia_p;
	
		if (instr(ds_dose_terapeutica_w,'/') > 0) then
			ds_dose_terapeutica_w := substr(ds_dose_terapeutica_w,0,instr(ds_dose_terapeutica_w,'/')-1);
		end if;
		
	end;	
end if;

return ds_dose_terapeutica_w;

END Obter_desc_calc_terap;
/