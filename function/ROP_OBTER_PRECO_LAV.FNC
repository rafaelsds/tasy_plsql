create or replace
function rop_obter_preco_lav(	cd_estabelecimento_p		number,
			cd_cnpj_p			varchar2)
return number is

vl_preco_w		number(15,4);

begin

select	nvl(min(vl_preco),0)
into	vl_preco_w
from	rop_tabela_preco_lavacao
where	cd_estabelecimento = cd_estabelecimento_p
and	nvl(cd_cnpj, cd_cnpj_p) = cd_cnpj_p
and	trunc(sysdate,'dd') between dt_inicio_vigencia and dt_fim_vigencia;

return	vl_preco_w;

end rop_obter_preco_lav;
/