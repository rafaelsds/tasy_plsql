create or replace
function pls_obter_desc_periodo
			(	nr_seq_periodo_p		number)
				return varchar2 is

ds_retorno_w			varchar2(255);
begin

select	nvl(ds_periodo,'')
into	ds_retorno_w
from	pls_periodo_pagamento
where	nr_sequencia	= nr_seq_periodo_p;

return	ds_retorno_w;

end pls_obter_desc_periodo;
/