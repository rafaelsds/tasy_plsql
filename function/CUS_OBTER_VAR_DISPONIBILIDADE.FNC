create or replace
function cus_obter_var_disponibilidade(	cd_centro_controle_p		number,
						cd_nivel_capacidade_p	number,
						dt_referencia_p		date)
				return number is

cd_tabela_custo_w			number(5)	:= 0;
cd_tabela_custo_ant_w		number(5)	:= 0;
qt_disponibilidade_w			number(15,4)	:= 0;
qt_disponibilidade_ant_w		number(15,4)	:= 0;
pr_var_disponibilidade_w		number(15,4)	:= 0;
dt_referencia_ant_w			date;
qt_diferenca_w			number(15,4)	:= 0;

begin

select	pkg_date_utils.add_month(dt_referencia_p, -1,0)
into	dt_referencia_ant_w
from	dual;

/* Busca tabela do m�s */
select	nvl(max(a.cd_tabela_custo),0)
into	cd_tabela_custo_w
from	tabela_custo a
where	a.cd_tipo_tabela_custo = 2
and	a.dt_mes_referencia = dt_referencia_p;

/* Busca tabela do m�s anterior */
select	nvl(max(a.cd_tabela_custo),0)
into	cd_tabela_custo_ant_w
from	tabela_custo a
where	a.cd_tipo_tabela_custo = 2
and	a.dt_mes_referencia = dt_referencia_ant_w;

/* Busca disponibilidade do m�s */
select	nvl(sum(x.qt_disponibilidade),0)
into	qt_disponibilidade_w
from	capac_centro_controle x,
	tabela_custo y
where	y.cd_tabela_custo    = x.cd_tabela_custo
and	x.cd_centro_controle = cd_centro_controle_p
and	x.cd_nivel_capacidade = nvl(cd_nivel_capacidade_p,x.cd_nivel_capacidade)
and	x.cd_tabela_custo    = cd_tabela_custo_w;

/* Busca disponibilidade do m�s anterior */
select	nvl(sum(x.qt_disponibilidade),0)
into	qt_disponibilidade_ant_w
from	capac_centro_controle x,
	tabela_custo y
where	y.cd_tabela_custo    = x.cd_tabela_custo
and	x.cd_centro_controle = cd_centro_controle_p
and	x.cd_nivel_capacidade = nvl(cd_nivel_capacidade_p,x.cd_nivel_capacidade)
and	x.cd_tabela_custo    = cd_tabela_custo_ant_w;

/* Calcula % de varia��o da disponibilidade entre os meses */
pr_var_disponibilidade_w		:= ((dividir(qt_disponibilidade_w,qt_disponibilidade_ant_w) - 1)*100);

return	pr_var_disponibilidade_w;

end cus_obter_var_disponibilidade;
/