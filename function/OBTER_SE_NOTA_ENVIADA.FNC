create or replace
function obter_se_nota_enviada(nr_sequencia_p		number)
 		    	return varchar2 is
			
ds_retorno_w 		varchar2(1);
ie_status_envio_w 	nota_fiscal.ie_status_envio%type;
nr_nfe_imp_w 		nota_fiscal.nr_nfe_imp%type;

begin

ds_retorno_w := 'S';

if 	(nr_sequencia_p > 0) then

	select  nvl(max(ie_status_envio),'N'),
		nvl(max(nr_nfe_imp),0) 
	into    ie_status_envio_w,
	        nr_nfe_imp_w
	from    nota_fiscal
	where   nr_sequencia = nr_sequencia_p; 

	if 	((ie_status_envio_w = 'E') and (nr_nfe_imp_w > 0)) then
		ds_retorno_w := 'N';

	elsif	((ie_status_envio_w = 'C') or (ie_status_envio_w = 'T'))  then    
		ds_retorno_w := 'N';
	end if; 

end if;     

return	ds_retorno_w;

end obter_se_nota_enviada;
/