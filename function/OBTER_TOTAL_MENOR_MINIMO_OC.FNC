create or replace
function obter_total_menor_minimo_oc(nr_ordem_p			varchar2,
				ie_consiste_vl_minimo_p		varchar2,
				nm_usuario_p			varchar2)
 		    	return varchar2 is

ie_tot_menor_minimo_w		varchar2(01) := 'N';
qt_historico_w			number(3);
vl_minimo_nf_w			number(15,2);
vl_total_ordem_w		number(15,2);
ie_juros_negociado_oc_w		varchar2(1);
pr_juros_negociado_w		number(13,4);
cd_estabelecimento_w		number(4);
			
begin

select	nvl(sum(obter_valor_liquido_ordem(nr_ordem_compra)),0),
	nvl(max(pr_juros_negociado),0),
	max(cd_estabelecimento)
into	vl_total_ordem_w,
	pr_juros_negociado_w,
	cd_estabelecimento_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_p;

select	nvl(ie_juros_negociado_oc,'N')
into	ie_juros_negociado_oc_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;

if	(ie_juros_negociado_oc_w = 'S') and
	(pr_juros_negociado_w > 0) then
	vl_total_ordem_w	:= 	vl_total_ordem_w  + dividir((vl_total_ordem_w * pr_juros_negociado_w),100);
end if;	

select	count(*)
into	qt_historico_w
from 	ordem_compra_hist	
where 	ds_titulo like '%' || WHEB_MENSAGEM_PCK.get_texto(305962) || '%'
and	nr_ordem_compra = nr_ordem_p;

if 	(ie_consiste_vl_minimo_p = 'A') and (qt_historico_w = 0) then
	begin
	select	to_number(substr(obter_dados_pf_pj_estab(cd_estabelecimento, null, cd_cgc_fornecedor, 'EVM'),1,20))
	into	vl_minimo_nf_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_p;

	if	(vl_total_ordem_w < vl_minimo_nf_w) then
		ie_tot_menor_minimo_w := 'S';		
	end if;
	end;
end if;

return	ie_tot_menor_minimo_w;

end obter_total_menor_minimo_oc;
/
