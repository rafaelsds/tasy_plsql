create or replace
function pls_obter_item_selec_contabil
		(	nr_seq_item_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(10);

begin

if	(nr_seq_item_p	= 1) then
	ds_retorno_w	:= 'AM';
elsif	(nr_seq_item_p	= 2) then
	ds_retorno_w	:= 'CM';
elsif	(nr_seq_item_p	= 3) then
	ds_retorno_w	:= 'DC';
elsif	(nr_seq_item_p	= 4) then
	ds_retorno_w	:= 'EF';
elsif	(nr_seq_item_p	= 5) then
	ds_retorno_w	:= 'F';
elsif	(nr_seq_item_p	= 6) then
	ds_retorno_w	:= 'I';
elsif	(nr_seq_item_p	= 7) then
	ds_retorno_w	:= 'IC';
elsif	(nr_seq_item_p	= 8) then
	ds_retorno_w	:= 'IM';
elsif	(nr_seq_item_p	= 9) then
	ds_retorno_w	:= 'M';
elsif	(nr_seq_item_p	= 10) then
	ds_retorno_w	:= 'PT';
elsif	(nr_seq_item_p	= 11) then
	ds_retorno_w	:= 'R';
elsif	(nr_seq_item_p	= 12) then
	ds_retorno_w	:= 'RM';
elsif	(nr_seq_item_p	= 13) then
	ds_retorno_w	:= 'TS';
elsif	(nr_seq_item_p	= 14) then
	ds_retorno_w	:= 'V';
end if;

return	ds_retorno_w;

end pls_obter_item_selec_contabil;
/
