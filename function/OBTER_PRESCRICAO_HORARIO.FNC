create or replace
function obter_prescricao_horario	(nr_seq_horario_p	number,
				ie_tipo_item_p	varchar2,
				ie_opcao_p	varchar2)
				return varchar2 is

/*
P - prescricao
I - sequencia item prescricao
*/

ds_adep_w	varchar2(240);
nr_prescricao_w	number(14,0);
nr_seq_item_w	number(10,0);

begin
if	(nr_seq_horario_p is not null) and
	(ie_tipo_item_p is not null) and
	(ie_opcao_p is not null) then
	/* obter dados prescricao */
	if	(ie_tipo_item_p in ('M','S','MAT','SNE', 'IA')) then
		select	nr_prescricao,
			nr_seq_material
		into	nr_prescricao_w,
			nr_seq_item_w		
		from	prescr_mat_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
	elsif	(ie_tipo_item_p in ('P','G','C','I')) then
		select	nr_prescricao,
			nr_seq_procedimento
		into	nr_prescricao_w,
			nr_seq_item_w		
		from	prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
	elsif	(ie_tipo_item_p = 'R') then
		select	nr_prescricao,
			nr_seq_recomendacao
		into	nr_prescricao_w,
			nr_seq_item_w		
		from	prescr_rec_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
	elsif	(ie_tipo_item_p = 'E') then
		select	nr_seq_pe_prescr,
			nr_seq_pe_proc
		into	nr_prescricao_w,
			nr_seq_item_w		
		from	pe_prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p;
	elsif	(ie_tipo_item_p = 'D') then
		select	nr_prescricao,
			null
		into	nr_prescricao_w,
			nr_seq_item_w
		from	prescr_dieta_hor
		where	nr_sequencia = nr_seq_horario_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
	end if;

	/* obter valores */
	if	(ie_opcao_p = 'P') then
		ds_adep_w := nr_prescricao_w;
	elsif	(ie_opcao_p = 'I') then
		ds_adep_w := nr_seq_item_w;
	end if;
end if;

return ds_adep_w;

end obter_prescricao_horario;
/