CREATE OR REPLACE 
Function	OBTER_DADOS_LIC_ITEM	(IE_OPCAO_P		VARCHAR2,
				NR_SEQUENCIA_P	NUMBER)	
		return Varchar2 is


/*
CD - retorna o c�digo do material
NRL - retorna a sequencia da licita��o
NR - retorna a sequencia cronologica do item
QT - retorna a qtde do item solicitado
UN - retorna o c�digo da unidade de medida
DSUN - retorna a descri��o da unidade de medida
DSL - retorna a desc. do material na licita��o
COMPL - retorna o complemento do item
DSA - retorna a desc. adicional do item
CDP - retorna o c�gio do processo da licita��o em que o item est�
DSP - retorna o parecer do item do fornecedor vencedor
*/

CD_MATERIAL_W		number(10);
NR_SEQ_ITEM_W		number(7);
NR_SEQ_LICITACAO_W	number(7);
QT_ITEM_W		number(15,4);
DS_COMPLEMENTO_W	varchar2(254);
DS_MATERIAL_W		varchar2(100);
CD_UNID_MEDIDA_W	varchar2(30);
DS_MAT_LICITACAO_W	varchar2(500);
DS_UNID_MEDIDA_W	varchar2(100);
cd_processo_w		varchar2(100);
DS_ADICIONAL_W		varchar2(255);
ds_parecer_w		varchar2(255);

begin
SELECT	A.CD_MATERIAL,
	A.NR_SEQ_ITEM,
	A.QT_ITEM,
	substr(B.DS_MATERIAL,1,100),
	A.CD_UNID_MEDIDA,
	A.NR_SEQ_LICITACAO,
	substr(A.DS_ADICIONAL,1,255),
	c.cd_processo
INTO	CD_MATERIAL_W,
	NR_SEQ_ITEM_W,
	QT_ITEM_W,
	DS_MATERIAL_W,
	CD_UNID_MEDIDA_W,
	NR_SEQ_LICITACAO_W,
	DS_ADICIONAL_W,
	cd_processo_w
FROM	MATERIAL B,
	LIC_ITEM A,
	lic_licitacao c
WHERE	A.NR_SEQUENCIA = NR_SEQUENCIA_P
AND	A.CD_MATERIAL = B.CD_MATERIAL
and	c.nr_sequencia = a.nr_seq_licitacao;

if	(IE_OPCAO_P = 'CD')	then
	return CD_MATERIAL_W;
elsif	(IE_OPCAO_P = 'DS')	then
	return DS_MATERIAL_W;
elsif  (IE_OPCAO_P = 'NRL') then
	return NR_SEQ_LICITACAO_W;
elsif	(IE_OPCAO_P = 'NR')	then
	return NR_SEQ_ITEM_W;
elsif	(IE_OPCAO_P = 'QT')	then
	return QT_ITEM_W;
elsif	(IE_OPCAO_P = 'UN')	then
	return CD_UNID_MEDIDA_W;
elsif	(IE_OPCAO_P = 'DSUN')	then
	SELECT	DS_UNIDADE_MEDIDA
	INTO	DS_UNID_MEDIDA_W
	FROM	UNIDADE_MEDIDA
	WHERE	CD_UNIDADE_MEDIDA = CD_UNID_MEDIDA_W;
	return DS_UNID_MEDIDA_W;
elsif	(IE_OPCAO_P = 'DSL')	then
	SELECT	DS_MAT_LICITACAO
	INTO	DS_MAT_LICITACAO_W
	FROM	MATERIAL_LIC
	WHERE	CD_MATERIAL = CD_MATERIAL_W;
	RETURN	DS_MAT_LICITACAO_W;
elsif	(IE_OPCAO_P = 'COMPL')  then
	SELECT DS_COMPLEMENTO
	INTO   DS_COMPLEMENTO_W 
	FROM   LIC_COMPLEMENTO 
	WHERE  NR_SEQUENCIA = (SELECT NR_SEQ_COMPL 
		  			   	   FROM	  MATERIAL_LIC 
						   WHERE  CD_MATERIAL = CD_MATERIAL_W);
	return DS_COMPLEMENTO_W;
elsif	(IE_OPCAO_P = 'DSA')  then
	return DS_ADICIONAL_W;
elsif	(IE_OPCAO_P = 'CDP') then
	return cd_processo_w;
elsif	(IE_OPCAO_P = 'DSP') then
	begin
	select	lic_obter_parecer_item(a.nr_seq_parecer)
	into	ds_parecer_w
	from	lic_item_fornec a,
		lic_item b
	where	a.ie_vencedor 	= 'S'
	and	b.nr_sequencia 	= a.nr_seq_item
	and	b.nr_sequencia	= nr_sequencia_P;
	exception
		when others then
				ds_parecer_w := null;
	end;
	return	ds_parecer_w;
end if;
end;
/