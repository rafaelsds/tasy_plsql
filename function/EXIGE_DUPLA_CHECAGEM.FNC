create or replace
function exige_dupla_checagem (cd_material_p	number)
return char is

ie_dupla_checagem_w		char(1) := 'N';

begin

if	(cd_material_p is not null) then
	
	select 	nvl(max(IE_DUPLA_CHECAGEM),'N')			
	into	ie_dupla_checagem_w			
	from 	MATERIAL
	where 	cd_material = cd_material_p;
	
end if;

return	ie_dupla_checagem_w;

end exige_dupla_checagem;
/