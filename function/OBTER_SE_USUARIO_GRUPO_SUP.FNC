create or replace
function obter_se_usuario_grupo_sup(
			nr_seq_grupo_p		number,
			nm_usuario_grupo_p	varchar2)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(1) := 'N';

begin

if	(nvl(nr_seq_grupo_p,0) > 0) and
	(nvl(nm_usuario_grupo_p,'X') <> 'X') then
	begin
	select	'S'
	into	ds_retorno_w
	from	usuario_grupo_sup a
	where	a.nr_seq_grupo = nr_seq_grupo_p
	and	a.nm_usuario_grupo = nm_usuario_grupo_p
	and	rownum < 2;
	exception
	when others then
		ds_retorno_w := 'N';
	end;
end if;

return	ds_retorno_w;

end obter_se_usuario_grupo_sup;
/