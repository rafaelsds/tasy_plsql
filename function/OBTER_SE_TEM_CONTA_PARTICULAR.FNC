create or replace
function obter_se_tem_conta_particular(nr_atendimento_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);			
			
begin

select	nvl(max('S'),'N')
into	ds_retorno_w
from	conta_paciente a,
	convenio b
where	a.cd_convenio_parametro = b.cd_convenio
and	a.nr_atendimento = nr_atendimento_p
and	b.ie_tipo_convenio = 1;

return	ds_retorno_w;

end obter_se_tem_conta_particular;
/