create or replace
function Obter_se_regra_inf_PEP(	nr_seq_regra_p	number)
								return char is
				
ie_existe_w		char(1);
cd_perfil_w		perfil.cd_perfil%type;
nm_usuario_w	usuario.nm_usuario%type;
ie_visualizar_w	char(1);
ds_sql_cursor_w	varchar2(300);
bind_w			sql_pck.t_dado_bind;
cursor_w		sql_pck.t_cursor;

begin

if	(nr_seq_regra_p is not null) then
	select	coalesce(max('S'),'N')
	into	ie_existe_w
	from	pep_regra_informacao_lib
	where	rownum = 1
	and		nr_seq_regra	= nr_seq_regra_p;

	if	(ie_existe_w = 'S') then
		bind_w.delete;

		ds_sql_cursor_w	:=	' select * from ( select	coalesce(ie_visualizar,''S'') ' ||
							' from		pep_regra_informacao_lib ' ||
							' where		nr_seq_regra = :nr_seq_regra ';
		sql_pck.bind_variable(':nr_seq_regra', nr_seq_regra_p, bind_w);

		cd_perfil_w	:= coalesce(obter_perfil_ativo,0);
		if	(cd_perfil_w = 0) then
			ds_sql_cursor_w	:=	ds_sql_cursor_w || ' and		cd_perfil is null ';
		else
			ds_sql_cursor_w	:=	ds_sql_cursor_w || ' and		cd_perfil = :cd_perfil ';
			sql_pck.bind_variable(':cd_perfil', cd_perfil_w, bind_w);
		end if;

		nm_usuario_w := coalesce(obter_usuario_ativo,'XPTO');
		if	(nm_usuario_w = 'XPTO') then
			ds_sql_cursor_w	:=	ds_sql_cursor_w || ' and		nm_usuario_param is null ';
		else
			ds_sql_cursor_w	:=	ds_sql_cursor_w || ' and		nvl(nm_usuario_param, :nm_usuario)  = :nm_usuario ';
			sql_pck.bind_variable(':nm_usuario', nm_usuario_w, bind_w);
		end if;

		ds_sql_cursor_w	:=	ds_sql_cursor_w || ' order by  nm_usuario_param, coalesce(cd_perfil,0)) where rownum = 1 ';
		cursor_w :=	sql_pck.executa_sql_cursor(ds_sql_cursor_w, bind_w);

		loop
		fetch cursor_w into
			ie_visualizar_w;
		exit when cursor_w%notfound;
		end loop;
		close cursor_w;

		return coalesce(ie_visualizar_w,'N');
	end if;
end if;

return	'S';

end Obter_se_regra_inf_PEP;
/