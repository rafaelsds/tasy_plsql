create or replace
function pls_obter_valor_parametro(	nr_seq_parametro_p	number,
					ds_parametros_p		varchar2)
					return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:

-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [   ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

vl_parametro_w			varchar2(4000);
contador_w			number(10) := 0;
nr_posicao_w			number(10) := 0;
ds_lista_parametros_w		varchar2(4000);

begin
if (nr_seq_parametro_p is not null and ds_parametros_p is not null) then	
	ds_lista_parametros_w	:= ds_parametros_p;	
	while (contador_w < nr_seq_parametro_p) loop 
		begin
		nr_posicao_w	:= instr(ds_lista_parametros_w,'#@');		
			if	(nr_posicao_w > 0) then
				vl_parametro_w		:= substr(ds_lista_parametros_w, 0, nr_posicao_w - 1);
				ds_lista_parametros_w	:= substr(ds_lista_parametros_w, nr_posicao_w + 2, length(ds_lista_parametros_w));	
				
				contador_w	:= contador_w + 1;		
			else
				begin
				vl_parametro_w		:= ds_lista_parametros_w;
				ds_lista_parametros_w	:= null;
				
				contador_w	:= contador_w + 1;
				end;
			end if;			
		end;
	end loop;
	
end if;

return	vl_parametro_w;

end pls_obter_valor_parametro;
/