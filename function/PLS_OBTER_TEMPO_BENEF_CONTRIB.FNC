create or replace
function pls_obter_tempo_benef_contrib
			(	nr_seq_segurado_p		number,
				nr_seq_alteracao_motivo_p	number,
				dt_inicio_p			date)
 		    	return number is

dt_adesao_w		date;
qt_meses_incluido_w	number(5);
dt_admissao_w		date;
ie_tipo_data_w		varchar2(10);
dt_contratacao_w	date;
dt_contribuicao_w	date;
dt_calculo_base_w	date;
qt_meses_contrib_w	number(5);
nr_seq_solicitacao_w	pls_solicitacao_rescisao.nr_sequencia%type;

begin

select	max(a.nr_sequencia)
into	nr_seq_solicitacao_w
from	pls_solicitacao_rescisao a,
	pls_solic_rescisao_benef b
where	a.nr_sequencia = b.nr_seq_solicitacao
and	b.nr_seq_segurado = nr_seq_segurado_p
and	a.ie_status = 3;

if	(nr_seq_solicitacao_w is not null) then
	select  max(qt_meses_contribuicao)
	into 	qt_meses_contrib_w
	from	pls_solic_rescisao_benef
	where 	nr_seq_segurado	= nr_seq_segurado_p
	and	nr_seq_solicitacao = nr_seq_solicitacao_w;
end if;

if	(qt_meses_contrib_w is null) then
	select	dt_inclusao_operadora,
		dt_contratacao
	into	dt_adesao_w,
		dt_contratacao_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;

	select	max(dt_admissao),
		max(dt_contribuicao)
	into	dt_admissao_w,
		dt_contribuicao_w
	from	pls_segurado_compl
	where	nr_seq_segurado	= nr_seq_segurado_p;

	begin
	select	nvl(ie_tipo_data,'I')
	into	ie_tipo_data_w
	from	pls_motivo_alt_pagador
	where	nr_sequencia	= nr_seq_alteracao_motivo_p
	and	ie_situacao	= 'A';
	exception
	when others then
		ie_tipo_data_w		:= 'I';
	end;

	if	(ie_tipo_data_w	= 'I') then
		dt_calculo_base_w	:= dt_adesao_w;
	elsif	(ie_tipo_data_w	= 'A') then
		dt_calculo_base_w	:= dt_admissao_w;
	elsif	(ie_tipo_data_w	= 'C') then
		dt_calculo_base_w	:= dt_contratacao_w;
	elsif	(ie_tipo_data_w	= 'T') then
		dt_calculo_base_w	:= nvl(dt_contribuicao_w,dt_adesao_w);
	end if;

	select	round(months_between(nvl(dt_inicio_p,sysdate), dt_calculo_base_w)) + 1
	into	qt_meses_incluido_w
	from	dual;
else
	qt_meses_incluido_w	:= qt_meses_contrib_w;
end if;

return	qt_meses_incluido_w;

end pls_obter_tempo_benef_contrib;
/
