create or replace 
function eis_obter_ocup_setor(	cd_setor_atendimento_p	number,
					dt_inicial_p		date,
					dt_final_p		date,
					cd_estabelecimento_p	number,
					ie_tipo_dado_p			varchar2 default null)
					return number is

qt_leitos_w		number(15,0);
qt_leitos_ocupados_w	number(15,0);
ie_calculo_padronizado_w	varchar2(1);

begin

ie_calculo_padronizado_w := obter_dados_param_atend(cd_estabelecimento_p,'TO');

if	(ie_calculo_padronizado_w = 'S')
and 	(cd_setor_atendimento_p is null) then
	return	nvl(eis_obter_ocup_censo(dt_inicial_p, dt_final_p, cd_estabelecimento_p,ie_tipo_dado_p),0);
else
	select	sum(nr_leitos_ocupados + nr_leitos_livres - nr_unidades_interditadas)
	into	qt_leitos_w
	from	eis_ocupacao_setor_v a
	where	dt_referencia between dt_inicial_p and dt_final_p
	and	ie_periodo = 'D'
	and	ie_ocup_hospitalar = 'S'
	and	((cd_setor_atendimento = cd_setor_atendimento_p) or (cd_setor_atendimento_p is null))
	and	cd_estabelecimento = cd_estabelecimento_p;

	select	sum(nr_leitos_ocupados)
	into	qt_leitos_ocupados_w
	from	eis_ocupacao_setor_v a
	where	dt_referencia between dt_inicial_p and dt_final_p
	and	ie_periodo = 'D'
	and	ie_ocup_hospitalar = 'S'
	and	cd_estabelecimento = cd_estabelecimento_p
	and	((cd_setor_atendimento = cd_setor_atendimento_p) or (cd_setor_atendimento_p is null));

	return	dividir((qt_leitos_ocupados_w * 100),qt_leitos_w);
end if;

end  eis_obter_ocup_setor;
/