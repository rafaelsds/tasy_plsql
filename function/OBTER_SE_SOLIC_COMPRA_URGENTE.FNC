create or replace
function obter_se_solic_compra_urgente(	nr_solic_compra_p		number)
 		    	return varchar2 is

ie_urgente_w		varchar2(1);
			
begin

select	nvl(max(ie_urgente),'N')
into	ie_urgente_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

return	ie_urgente_w;

end obter_se_solic_compra_urgente;
/