create or replace
function obter_nosso_numero_interf(	cd_banco_p	number,
					nr_titulo_p	number)
 		    	return number is

nr_retorno_w		number(20);
nr_nosso_numero_w	number(20);
			
begin
if (cd_banco_p is not null) and
   (nr_titulo_p is not null) then

	begin
		
	select 	elimina_caracteres_especiais(obter_nosso_numero_banco(cd_banco_p, nr_titulo_p))
	into	nr_nosso_numero_w
	from	dual;

	exception
	when others then
		/*Erro no cadastros da regra nosso n�mero em: 'Cadastros financeiros / Banco / Contas a receber / Regra nosso n�mero'*/
		wheb_mensagem_pck.exibir_mensagem_abort(264901);
	end;
	
	nr_retorno_w := nr_nosso_numero_w;
end if;

return	nr_retorno_w;

end obter_nosso_numero_interf;
/
