create or replace
function obter_qep_respostas(	nr_seq_que_atend_questao_p	number,
				ie_opcao_p			varchar2,
				ie_tipo_resposta_p		varchar2,
				ie_retornar_dor_p		varchar2 default 'N')
 		    	return varchar2 is
/* ie_opcao_p:
C = nr_sequencia da que_questao_opcao (da fun��o Question�rio Eletr�nico Paciente)
D = descri��o da op��o
O = observa��o da resposta
*/


ds_retorno_w			varchar2(4000);
cd_opcao_w			que_questao_opcao.nr_sequencia%type;
ds_opcao_w			que_questao_opcao.ds_opcao%type;
ds_resposta_w			que_atendimento_questao_op.ds_resposta%type;
nr_seq_tasy_padrao_cor_w	que_atendimento_questao_op.nr_seq_tasy_padrao_cor%type;
cd_perfil_w			perfil.cd_perfil%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ds_item_legenda_w		tasy_padrao_cor.ds_item%type;

Cursor C01 is
	select	a.nr_sequencia,
		a.ds_opcao,
		b.nr_seq_tasy_padrao_cor
	from	que_questao_opcao a, 
		que_atendimento_questao_op b
	where	a.nr_sequencia = b.nr_seq_que_questao_opcao
	and	b.nr_seq_que_atend_questao = nr_seq_que_atend_questao_p
	order by nvl(a.nr_seq_apres,9999), a.ds_opcao;
begin

if	(ie_opcao_p = 'O') then
	select	max(ds_observacao)
	into	ds_retorno_w
	from	que_atendimento_questao_op
	where	nr_seq_que_atend_questao = nr_seq_que_atend_questao_p
	and	ds_observacao is not null; -- caso houver registros de resposta multisele��o com observa��o nula
else
	if	(ie_tipo_resposta_p = 'D') then

		select	null,
			max(ds_resposta)
		into	cd_opcao_w,
			ds_resposta_w
		from	que_atendimento_questao_op
		where	nr_seq_que_atend_questao = nr_seq_que_atend_questao_p;
		
		if	(ie_opcao_p = 'C') then
			ds_retorno_w	:= cd_opcao_w;
		elsif	(ie_opcao_p = 'D') then
			ds_retorno_w	:= ds_resposta_w;
		end if;
		
	elsif	(ie_tipo_resposta_p = 'S') then
		select	to_char(max(a.nr_sequencia)),
			max(a.ds_opcao)
		into	cd_opcao_w,
			ds_opcao_w
		from	que_questao_opcao a, 
			que_atendimento_questao_op b
		where	a.nr_sequencia = b.nr_seq_que_questao_opcao
		and	b.nr_seq_que_atend_questao = nr_seq_que_atend_questao_p;
		
		if	(ie_opcao_p = 'C') then
			ds_retorno_w	:= cd_opcao_w;
		elsif	(ie_opcao_p = 'D') then
			ds_retorno_w	:= ds_opcao_w;
		end if;
	else
		cd_perfil_w		:= wheb_usuario_pck.get_cd_perfil;
		cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
		open C01;
		loop
		fetch C01 into	
			cd_opcao_w,
			ds_opcao_w,
			nr_seq_tasy_padrao_cor_w;
		exit when C01%notfound;
			begin

			if (ds_retorno_w is not null) then
				ds_retorno_w	:= ds_retorno_w || ',';
			end if;
			
			if	(ie_opcao_p = 'C') then
				ds_retorno_w	:= ds_retorno_w || cd_opcao_w;
				if	(ie_retornar_dor_p = 'S') and
					(nr_seq_tasy_padrao_cor_w is not null) then
					ds_retorno_w	:= ds_retorno_w || '=' || ds_item_legenda_w;
				end if;
				
			elsif	(ie_opcao_p = 'D') then
				ds_retorno_w	:= ds_retorno_w || ds_opcao_w;

				if	(ie_retornar_dor_p = 'S') and
					(nr_seq_tasy_padrao_cor_w is not null) and
					(cd_perfil_w is not null) and
					(cd_estabelecimento_w is not null) then

					select	max(nvl(a.ds_item, obter_desc_expressao(b.cd_exp_item,b.ds_item)))
					into	ds_item_legenda_w
					from	tasy_padrao_cor b,
						tasy_padrao_cor_cliente a
					where	b.nr_seq_legenda = 1133 -- legenda intensidade da dor
					and	b.nr_sequencia = nr_seq_tasy_padrao_cor_w
					and	a.nr_seq_cor(+) = b.nr_sequencia
					and	nvl (a.cd_perfil(+), cd_perfil_w) = cd_perfil_w
					and	nvl (a.cd_estabelecimento(+), cd_estabelecimento_w) = cd_estabelecimento_w;
				
					if	(ds_item_legenda_w is not null) then
						ds_retorno_w	:= ds_retorno_w || '(' || ds_item_legenda_w || ')';
					end if;
				end if;
			end if;
			
			end;
		end loop;
		close C01;
	end if;
end if;

return	substr(ds_retorno_w,1,2000);

end obter_qep_respostas;
/
