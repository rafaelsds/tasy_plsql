create or replace
function ctb_obter_nome_demonstrativo(	nr_seq_demonstrativo_p	ctb_demonstrativo.nr_sequencia%type)
					return varchar2 is

ds_retorno_w				varchar2(255);

begin

if	(nvl(nr_seq_demonstrativo_p,0) <> 0) then
	begin
	select	nvl(a.ds_titulo, b.ds_modelo)
	into	ds_retorno_w
	from	ctb_modelo_relat b,
		ctb_demonstrativo a
	where	a.nr_seq_tipo	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_demonstrativo_p;
	exception when others then
		ds_retorno_w	:= '';
	end;
end if;

return	ds_retorno_w;

end ctb_obter_nome_demonstrativo;
/