create or replace
function obter_desc_status_anexo(	nr_seq_status_p	number )
					return varchar2 is

ds_retorno_w	varchar2(100);

begin

if	(nr_seq_status_p is not null) then
	select	ds_status
	into	ds_retorno_w
	from	status_anexo_agenda
	where	nr_sequencia	=	nr_seq_status_p;	
end if;

return	ds_retorno_w;

end obter_desc_status_anexo;
/