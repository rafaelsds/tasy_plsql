create or replace
procedure DUPLICA_REGRA_REPASSE_PONT(	nr_seq_regra_origem_p	in	number,
					ds_pontuacao_p		in	varchar2,
					cd_pontuacao_p		in	number,
					dt_inicio_vigencia_p	in	date,
         				dt_fim_vigencia_p		in	date,					
         				nm_usuario_p		in	varchar2,
					nr_seq_regra_gerada_p	out	number) is


nr_seq_regra_gerada_w	number(10);
nr_seq_pont_tem_w		number(10);
nr_seq_adicional_w	number(10);
nr_seq_rep_ponto_w 	number(10);
nr_seq_mat_criterio_w	number(10);

cursor c01 is
select	a.nr_sequencia
from	regra_repasse_pont_item a
where	a.nr_seq_regra_rep_ponto	= nr_seq_rep_ponto_w;

begin

	select 	nr_seq_regra_rep_ponto,
		nr_seq_mat_criterio
	into 	nr_seq_rep_ponto_w,
		nr_seq_mat_criterio_w
	from 	mat_crit_rep_regra_ponto
	where 	nr_sequencia = nr_seq_regra_origem_p;

	select	regra_repasse_pontuacao_seq.nextval
	into	nr_seq_regra_gerada_w
	from	dual;

	insert	into regra_repasse_pontuacao(
		nr_sequencia, 
		cd_estabelecimento,   
		dt_atualizacao,     
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_regra_pontuacao,
		ie_tipo_pontuacao,
		vl_unitario_ponto,
		nr_seq_trans_financ,  
		pr_desconto_repasse,
		ie_situacao,  
		dt_inicio_vigencia,   
		dt_fim_vigencia)
	select	nr_seq_regra_gerada_w, 
		a.cd_estabelecimento,   
		sysdate,     
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nvl(ds_pontuacao_p,a.ds_regra_pontuacao),
		a.ie_tipo_pontuacao,
		nvl(cd_pontuacao_p,a.vl_unitario_ponto),
		a.nr_seq_trans_financ,  
		a.pr_desconto_repasse,
		'A',  
		dt_inicio_vigencia_p,   
		dt_fim_vigencia_p
	from	regra_repasse_pontuacao a
	where	a.nr_sequencia	= nr_seq_rep_ponto_w;
	
	insert	into mat_crit_rep_regra_ponto(
		nr_sequencia,  
		dt_atualizacao,      
		nm_usuario, 
		dt_atualizacao_nrec,     
		nm_usuario_nrec,
		nr_seq_mat_criterio,
		nr_seq_regra_rep_ponto)
	select	mat_crit_rep_regra_ponto_seq.nextval, 
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_mat_criterio_w,   
		nr_seq_regra_gerada_w
	from	mat_crit_rep_regra_ponto a
	where	a.nr_sequencia	= nr_seq_regra_origem_p;
	

	open c01;
	loop
	fetch c01 into
		nr_seq_pont_tem_w;
	exit when c01%notfound;

		insert	into regra_repasse_pont_item(
			nr_sequencia,
			dt_atualizacao,     
			nm_usuario,
			dt_atualizacao_nrec,  
			nm_usuario_nrec,
			nr_seq_regra_rep_ponto, 
			dt_inicio_vigencia,     
			dt_fim_vigencia,     
			qt_item_periodo,
			ie_tipo_periodo_repasse,
			ie_forma_cons_qtd_item,
			ie_tipo_data_item,
			vl_inicial_faixa,
			vl_final_faixa, 
			qt_ponto_faixa,
			pr_repassar,
			qt_ponto_item)
		select	regra_repasse_pont_item_seq.nextval,
			sysdate,     
			nm_usuario_p,
			sysdate,  
			nm_usuario_p,
			nr_seq_regra_gerada_w, 
			dt_inicio_vigencia_p,     
			dt_fim_vigencia_p,     
			b.qt_item_periodo,
			b.ie_tipo_periodo_repasse,
			b.ie_forma_cons_qtd_item,
			b.ie_tipo_data_item,
			b.vl_inicial_faixa,
			b.vl_final_faixa, 
			b.qt_ponto_faixa,
			b.pr_repassar,
			b.qt_ponto_item
		from	regra_repasse_pont_item b
		where 	b.nr_sequencia	= nr_seq_pont_tem_w;

	end loop;
	close c01;


nr_seq_regra_gerada_p	:= nr_seq_regra_gerada_w;

commit;

end DUPLICA_REGRA_REPASSE_PONT;
/