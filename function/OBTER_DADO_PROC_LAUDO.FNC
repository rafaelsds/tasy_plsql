CREATE OR REPLACE
FUNCTION OBTER_DADO_PROC_LAUDO (	nr_sequencia_p number,
					ie_opcao_p	number)
					return 	varchar2  is

nr_seq_proc_w			number(10,0);
nr_prescricao_w			number(14,0);
nr_seq_prescricao_w		number(06,0);
cd_tipo_proc_W			number(03,0);
dt_procedimento_w		date;
nr_seq_interno_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
ds_retorno_w			varchar2(20);
nr_seq_proc_interno_w		number(10,0);
nr_seq_exame_w			number(10,0);

/*
ie_opcao_p
  1 - Tipo de procedimento
  2 - Data do procedimento
  3 - Sequencia interna prescr_procedimento
  4 - CD_PROCEDIMENTO
  5 - IE_ORIGEM_PROCED
  6 - Procedimento interno
  7 - Exame
*/

BEGIN

select	nvl(max(nr_seq_proc),0) nr_seq_proc,
	nvl(max(nr_prescricao),0) nr_prescricao,
	nvl(max(nr_seq_prescricao),0) nr_seq_prescricao
into	nr_seq_proc_w,
	nr_prescricao_w,
	nr_seq_prescricao_w
from	laudo_paciente
where	nr_sequencia = nr_sequencia_p;

if	(nr_seq_proc_w <> 0) then
	select	nvl(max(b.cd_tipo_procedimento),0) cd_tipo_procedimento,
		nvl(max(a.dt_procedimento),sysdate),
		nvl(max(nr_prescricao),0),
		nvl(max(nr_sequencia_prescricao),0),
		nvl(max(a.cd_procedimento),0),
		nvl(max(a.ie_origem_proced),0),
		nvl(max(a.nr_seq_proc_interno),0),
		nvl(max(a.nr_seq_exame),0)		
	into	cd_tipo_proc_W,
		dt_procedimento_w,
		nr_prescricao_w,
		nr_seq_prescricao_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		nr_seq_exame_w
	from	procedimento b,
		procedimento_paciente a
	where	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced	= b.ie_origem_proced
	and	a.nr_sequencia	= nr_seq_proc_w;
elsif	(nr_prescricao_w <> 0) and (nr_seq_prescricao_w <> 0) then
	select	nvl(max(b.cd_tipo_procedimento),0) cd_tipo_procedimento,
		nvl(max(a.dt_procedimento),sysdate),
		nvl(max(a.cd_procedimento),0),
		nvl(max(a.ie_origem_proced),0),
		nvl(max(a.nr_seq_proc_interno),0),
		nvl(max(a.nr_seq_exame),0)
	into	cd_tipo_proc_W,
		dt_procedimento_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		nr_seq_exame_w
	from	procedimento b,
		procedimento_paciente a
	where	a.cd_procedimento  = b.cd_procedimento
	and	a.ie_origem_proced = b.ie_origem_proced
	and	a.nr_sequencia_prescricao = nr_seq_prescricao_w
	and	a.nr_prescricao    = nr_prescricao_w;
end if;

if	(ie_opcao_p = 1) then
	ds_retorno_w := cd_tipo_proc_w;
elsif	(ie_opcao_p = 2) then
	ds_retorno_w := to_char(dt_procedimento_w, 'dd/mm/yyyy hh24:mi:ss');
elsif	(ie_opcao_p = 3) then
	select	nvl(max(nr_seq_interno),0)
	into	nr_seq_interno_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_w
	  and	nr_sequencia	= nr_seq_prescricao_w;
	ds_retorno_w := nr_seq_interno_w;
elsif	(ie_opcao_p = 4) then
	ds_retorno_w := cd_procedimento_w;
elsif	(ie_opcao_p = 5) then
	ds_retorno_w := ie_origem_proced_w;
elsif	(ie_opcao_p = 6) then
	ds_retorno_w := nr_seq_proc_interno_w;
elsif	(ie_opcao_p = 7) then
	ds_retorno_w := nr_seq_exame_w;
end if;

return	ds_retorno_w;

END OBTER_DADO_PROC_LAUDO;
/