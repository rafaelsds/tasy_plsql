create or replace 
function Verifica_saldo_baixar(nr_prescricao_p		number,
			       nr_sequencia_p		number,
			       cd_estabelecimento_p	number,
			       cd_motivo_baixa_query_p	number,
			       nm_usuario_p		varchar2)
 		    	return varchar2 is

ie_retorno_w		varchar2(1):= 'S';
varIe_Baixa_Compl_w	varchar2(1);    /*Par�metro 26 */
qt_total_dispensar_w	number(18,6);
qt_material_w		number(15,0);
ie_conta_paciente_w	varchar2(1);
			
begin

obter_param_usuario(7025, 26, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, varIe_Baixa_Compl_w);

ie_retorno_w := 'S';
if (varIe_Baixa_Compl_w = 'S') then

	select 	a.qt_total_dispensar, 
		sum(b.qt_material)
	into	qt_total_dispensar_w,
		qt_material_w
	from 	material_atend_paciente b, 
		prescr_material a
        where 	a.nr_prescricao = nr_prescricao_p
	and 	a.nr_sequencia = nr_sequencia_p
	and 	a.nr_prescricao = b.nr_prescricao
	and 	a.nr_sequencia = b.nr_sequencia_prescricao
	group by a.qt_total_dispensar;

	select 	nvl(max(ie_conta_paciente),'N')
	into	ie_conta_paciente_w
	from 	tipo_baixa_prescricao
	where 	cd_tipo_baixa = cd_motivo_baixa_query_p;			

	if 	(qt_total_dispensar_w > qt_material_w) and
		(ie_conta_paciente_w = 'S') then
		ie_retorno_w := 'N';  
	end if;
end if;

return	ie_retorno_w;

end Verifica_saldo_baixar;
/