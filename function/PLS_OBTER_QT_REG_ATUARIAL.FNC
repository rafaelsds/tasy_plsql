/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter a quantidade de registros gerados no arquivo atuarial conforme o tipo de registro
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_qt_reg_atuarial
			(	dt_referencia_p		date,
				ie_tipo_registro_p	number)
 		    	return number is
			
qt_registros_w			number(10);

begin

if	(ie_tipo_registro_p = 1) then
	select	count(1)
	into	qt_registros_w
	from	PLS_ATUARIAL_v
	where	ie_tipo_registro	= 1;
else
	select	count(1)
	into	qt_registros_w
	from	PLS_ATUARIAL
	where	trunc(dt_referencia,'Month') = trunc(dt_referencia_p,'Month')
	and	ie_tipo_registro	= ie_tipo_registro_p;
end if;

return	qt_registros_w;

end pls_obter_qt_reg_atuarial;
/
