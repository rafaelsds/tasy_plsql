create or replace
function Obter_porcent_plano_estend	(nr_atendimento_p		number,
					cd_perfil_p			number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2)
					return number is

pr_estendido_w		number(10);
qt_total_w		number(10);
qt_estendidos_w		number(10);
					
begin

if	(nr_atendimento_p	is not null) then

	qt_total_w	:= Obter_quant_itens_plano(null,nr_atendimento_p,'T',nm_usuario_p,cd_perfil_p,cd_estabelecimento_p);
	qt_estendidos_w	:= Obter_quant_itens_plano(null,nr_atendimento_p,'E',nm_usuario_p,cd_perfil_p,cd_estabelecimento_p);

	pr_estendido_w	:= trunc(dividir((qt_estendidos_w * 100),qt_total_w));
	
end if;

return	pr_estendido_w;

end Obter_porcent_plano_estend;
/