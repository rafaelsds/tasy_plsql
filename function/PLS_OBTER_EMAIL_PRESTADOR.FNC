create or replace
function pls_obter_email_prestador(	nr_seq_prestador_p		number)
				return varchar2 is

ds_email_w	varchar2(4000);

begin

select	pls_obter_email_prest(p.cd_estabelecimento, p.cd_pessoa_fisica, p.cd_cgc)
into	ds_email_w
from	pls_prestador p
where	p.nr_sequencia = nr_seq_prestador_p;

return	ds_email_w;

end pls_obter_email_prestador;
/