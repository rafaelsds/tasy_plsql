create or replace
function HD_Obter_Priming_Fabrica ( 	nr_seq_modelo_p		number)
					return			number is

qt_priming_fabrica_w		number(10,2);

begin

/* Pega os dados do modelo do dialisador */
begin
select	qt_priming_fabrica
into	qt_priming_fabrica_w
from	hd_modelo_dialisador
where	nr_sequencia	= nr_seq_modelo_p;
exception
	when others then
	qt_priming_fabrica_w	:= 0;	
end;

return qt_priming_fabrica_w;

end HD_Obter_Priming_Fabrica;
/