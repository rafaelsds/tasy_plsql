create or replace
function pixeon_obter_na_requirenit(nr_atendimento_p	number,
				    cd_estabelecimento_p number,
				    ds_setor_p		varchar2)
				    return varchar2 is
			
ds_retorno_w			varchar2(255);
ie_procedencia_pixeon_w		varchar2(1);
begin

select  max(ie_procedencia_pixeon)
into	ie_procedencia_pixeon_w
from	parametro_medico
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_procedencia_pixeon_w = 'S') then
	select	substr(Obter_Procedencia_atend(nr_atendimento_p,'D'),1,255)
	into	ds_retorno_w
	from dual;
else
	ds_retorno_w	:= ds_setor_p;
end if;



return	ds_retorno_w;

end pixeon_obter_na_requirenit;
/