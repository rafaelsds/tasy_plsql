create or replace
function Obter_Se_Medico_Turno_Agenda
		(nr_seq_agenda_p	number,
		cd_medico_p		varchar2)
 		return varchar2 is

nr_seq_turno_w			number(10,0);
cd_agenda_w			number(10,0);
hr_inicio_w			date;	
ie_dia_semana_w			number(10,0);	
ie_liberado_w			varchar2(01) := 'S';
qt_reg_w			number(10,0);
		
begin

select	max(cd_agenda),
		max(hr_inicio)
into	cd_agenda_w,
		hr_inicio_w
from	agenda_paciente
where	nr_sequencia	= nr_seq_agenda_p;


select	count(*)
into	qt_reg_w
from 	agenda_medico
where	cd_medico	= cd_medico_p
and		nr_seq_turno	is not null
and		cd_agenda = cd_agenda_w;

if	(qt_reg_w > 0) then
	begin
	
	select	cd_agenda,
		hr_inicio
	into	cd_agenda_w,
		hr_inicio_w
	from	agenda_paciente
	where	nr_sequencia	= nr_seq_agenda_p;

	select 	pkg_date_utils.get_WeekDay(hr_inicio_w)
	into	ie_dia_semana_w
	from 	dual;

	select	max(nr_sequencia)
	into	nr_seq_turno_w
	from 	agenda_horario
	where	cd_agenda	= cd_agenda_w
	and	dt_dia_semana	= ie_dia_semana_w
	and	to_date(trunc(sysdate, 'dd') || ' ' || to_char(hr_inicio_w, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') between
			to_date(trunc(sysdate, 'dd') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') and
			to_date(trunc(sysdate, 'dd') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');
	 
	 
	select	nvl(max('S'),'N')
	into	ie_liberado_w
	from	agenda_medico
	where	cd_agenda	= cd_agenda_w
	and	cd_medico	= cd_medico_p
	and 	nvl(nr_seq_turno, nr_seq_turno_w) = nr_seq_turno_w;
	end;
end if;
	
return	ie_liberado_w;

end Obter_Se_Medico_Turno_Agenda;
/
