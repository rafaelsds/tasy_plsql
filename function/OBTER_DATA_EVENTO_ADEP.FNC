create or replace
function Obter_data_evento_adep( nr_seq_evento_p	number,
				ie_tipo_item_p		varchar)
 		    	return date is

dt_evento_w	date;

begin

if	(ie_tipo_item_p = 'SOL') then
	select	max(dt_alteracao)
	into	dt_evento_w
	from	prescr_solucao_evento
	where	nr_sequencia = nr_seq_evento_p;
	
else
	select	max(dt_alteracao)
	into	dt_evento_w
	from	prescr_mat_alteracao
	where	nr_sequencia = nr_seq_evento_p;

end if;

return	dt_evento_w;

end Obter_data_evento_adep;
/