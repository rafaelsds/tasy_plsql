create or replace 
function obter_regra_deducao_conv(
			cd_estabelecimento_p	number,
			cd_convenio_p		number,
			ie_tipo_calculo_p	varchar2,
			dt_referencia_p		date)
			return Number is

nr_seq_regra_w		conv_regra_deducao.nr_sequencia%type;
dt_referencia_w		date;

cursor c01 is 
	select	nr_sequencia
	from	conv_regra_deducao
	where	nvl(cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0)
	and	nvl(cd_convenio, nvl(cd_convenio_p,0)) = nvl(cd_convenio_p,0)
	and	((ie_tipo_calculo = ie_tipo_calculo_p) or (ie_tipo_calculo_p is null))
	and	dt_referencia_w between PKG_DATE_UTILS.get_Time(nvl(dt_inicio_vigencia,dt_referencia_w),0) and
				PKG_DATE_UTILS.get_Time(nvl(dt_fim_vigencia,dt_referencia_w),0) + 86399/86400
	order by	nvl(cd_convenio,0),
		nvl(cd_estabelecimento,0);
			
begin

dt_referencia_w	:= nvl(dt_referencia_p, sysdate);

open c01;
loop
fetch c01 into	
	nr_seq_regra_w;
exit when c01%notfound;
	nr_seq_regra_w	:= nr_seq_regra_w;
end loop;
close c01;

return nr_seq_regra_w;

end obter_regra_deducao_conv;
/
