create or replace function qua_obter_data_prox_rev(nr_seq_documento_p number) 
return varchar2 is

ds_retorno_w	varchar2(20);

begin

select	nvl(max(b.dt_revalidacao), nvl(max(a.dt_aprovacao), nvl(max(b.dt_aprovacao), nvl(max(b.dt_validacao), max(b.dt_elaboracao))))) + nvl(max(b.qt_dias_revisao),0)
into 	ds_retorno_w
from	qua_doc_revisao a,
	qua_documento b
where	b.nr_sequencia = nr_seq_documento_p
and	b.nr_sequencia = a.nr_seq_doc(+);

	return	ds_retorno_w;
end qua_obter_data_prox_rev;
/