create or replace
function obter_se_validade_anvisa(	cd_material_p		number,
					dt_referencia_p		date,
					cd_estabelecimento_p	number)
					return varchar2 is

dt_validade_reg_anvisa_w	date;
dt_validade_prot_anvisa_w	date;					
ie_valido_w 			varchar2(1):= 'S';
dt_referencia_w			date;
ie_vigente_anvisa_w		material_estab.ie_vigente_anvisa%type;
					
begin

dt_referencia_w := nvl(dt_referencia_p,sysdate);

dt_validade_reg_anvisa_w 	:= obter_dados_material_estab(cd_material_p,cd_estabelecimento_p,'VA');
dt_validade_prot_anvisa_w	:= obter_dados_material_estab(cd_material_p,cd_estabelecimento_p,'VPA');
ie_vigente_anvisa_w		:= obter_dados_material_estab(cd_material_p,cd_estabelecimento_p,'VI');



if	(ie_vigente_anvisa_w = 'N') and 
	(dt_validade_reg_anvisa_w is not null) and 
	(dt_validade_reg_anvisa_w < trunc(dt_referencia_w)) then
	
	if	(dt_validade_prot_anvisa_w is null) or (dt_validade_prot_anvisa_w < trunc(dt_referencia_w)) then
		ie_valido_w := 'N';
	end if;
end if;	

return	ie_valido_w;

end obter_se_validade_anvisa;
/
