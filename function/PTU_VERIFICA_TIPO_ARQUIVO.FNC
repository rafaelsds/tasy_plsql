create or replace
function ptu_verifica_tipo_arquivo
			(	nm_usuario_p		Varchar2)
				return Varchar2 is

ds_retorno_w		Varchar2(5);
nr_seq_importacao_w	Number(10);
ds_conteudo_w		Varchar2(4000);
			

cursor c01 is
	select	nr_seq_importacao,
		ds_valores
	from	w_scs_importacao
	where	nm_usuario	= nm_usuario_p
	order by nr_seq_importacao;

begin

open c01;
loop
fetch c01 into
	nr_seq_importacao_w,
	ds_conteudo_w;
exit when c01%notfound;
	begin
	
	if	(substr(ds_conteudo_w,1,4)	<> 'FIM$') then
		if	(nr_seq_importacao_w	= 1) then
			if	(substr(ds_conteudo_w,1,5) = '00500') then
				ds_retorno_w	:= '00500';
			elsif	(substr(ds_conteudo_w,1,5) = '00505') then
				ds_retorno_w	:= '00505';
			elsif	(substr(ds_conteudo_w,1,5) = '00401') then
				ds_retorno_w	:= '00401';
			elsif	(substr(ds_conteudo_w,1,5) = '00210') then
				ds_retorno_w	:= '00210';
			elsif	(substr(ds_conteudo_w,1,5) = '00312') then
				ds_retorno_w	:= '00312';
			elsif	(substr(ds_conteudo_w,1,5) = '00313') then
				ds_retorno_w	:= '00313';
			elsif	(substr(ds_conteudo_w,1,5) = '00318') then
				ds_retorno_w	:= '00318';
			elsif	(substr(ds_conteudo_w,1,5) = '00319') then
				ds_retorno_w	:= '00319';
			elsif	(substr(ds_conteudo_w,1,5) = '00506') then
				ds_retorno_w	:= '00506';
			elsif	(substr(ds_conteudo_w,1,5) = '00304') then
				ds_retorno_w	:= '00304';
			elsif	(substr(ds_conteudo_w,1,5) = '00211') then
				ds_retorno_w	:= '00211';
			elsif	(substr(ds_conteudo_w,1,5) = '00209') then
				ds_retorno_w	:= '00209';
			elsif	(substr(ds_conteudo_w,1,5) = '00202') then
				ds_retorno_w	:= '00202';
			elsif	(substr(ds_conteudo_w,1,5) = '00507') then
				ds_retorno_w	:= '00507';
			elsif	(substr(ds_conteudo_w,1,5) = '00600') then
				ds_retorno_w	:= '00600';
			elsif	(substr(ds_conteudo_w,1,5) = '00605') then
				ds_retorno_w	:= '00605';
			elsif	(substr(ds_conteudo_w,1,5) = '00501') then
				ds_retorno_w	:= '00501';
			elsif	(substr(ds_conteudo_w,1,5) = '00310') then
				ds_retorno_w	:= '00310';
			elsif	(substr(ds_conteudo_w,1,5) = '00412') then
				ds_retorno_w	:= '00412';
			elsif	(substr(ds_conteudo_w,1,5) = '00413') then
				ds_retorno_w	:= '00413';
			elsif	(substr(ds_conteudo_w,1,5) = '00418') then
				ds_retorno_w	:= '00418';
			elsif	(substr(ds_conteudo_w,1,5) = '00419') then
				ds_retorno_w	:= '00419';
			elsif	(substr(ds_conteudo_w,1,5) = '00606') then
				ds_retorno_w	:= '00606';
			elsif	(substr(ds_conteudo_w,1,5) = '00404') then
				ds_retorno_w	:= '00404';
			elsif	(substr(ds_conteudo_w,1,5) = '00311') then
				ds_retorno_w	:= '00311';
			elsif	(substr(ds_conteudo_w,1,5) = '00309') then
				ds_retorno_w	:= '00309';
			elsif	(substr(ds_conteudo_w,1,5) = '00302') then
				ds_retorno_w	:= '00302';
			elsif	(substr(ds_conteudo_w,1,5) = '00607') then
				ds_retorno_w	:= '00607';
			elsif	(substr(ds_conteudo_w,1,5) = '00430') then
				ds_retorno_w	:= '00430';
			elsif	(substr(ds_conteudo_w,1,5) = '00360') then
				ds_retorno_w	:= '00360';
			elsif	(substr(ds_conteudo_w,1,5) = '00361') then
				ds_retorno_w	:= '00361';
			elsif	(substr(ds_conteudo_w,1,5) = '00700') then
				ds_retorno_w	:= '00700';
			elsif	(substr(ds_conteudo_w,1,5) = '00804')  then
				ds_retorno_w	:= '00804';
			elsif	(substr(ds_conteudo_w,1,5) = '00806') then
				ds_retorno_w	:= '00806';
			elsif	(substr(ds_conteudo_w,1,5) = '00807') then
				ds_retorno_w	:= '00807';
			end if;
		end if;
	end if;
	
	end;
end loop;
close c01;
	
return	ds_retorno_w;

end ptu_verifica_tipo_arquivo;
/
