create or replace
function lote_ent_obter_result_ant (	nr_prescricao_p		number,
									nr_seq_prescr_p		number,
									nr_seq_resultado_p	number,
									nr_seq_exame_p		number,
									ie_opcao_p			number,
									ie_trigger_p		varchar2 default null,
									qt_resultado_p		number default null,
									pr_resultado_p		number default null)
									return varchar2 is

resultado_w				varchar2(255);
qt_repeticoes_w			number(10);
ds_resultado_p_w		varchar2(255);
ds_resultado_s_w		varchar2(255);
ds_resultado_t_w		varchar2(255);
ds_resultado_q_w		varchar2(255);
ds_resultado_qq_w		varchar2(255);
ds_resultado_se_w		varchar2(255);
qt_resultado_media_w	number(10,4);
qt_result_item_w		number(15,4);

rep_resultado_1_w		varchar2(255);
rep_resultado_2_w		varchar2(255);
rep_resultado_3_w		varchar2(255);

resultado_result1_w		varchar2(255);
resultado_result2_w		varchar2(255);
resultado_result3_w		varchar2(255);

qt_rep_1_w				number(10);
qt_rep_2_w				number(10);
qt_rep_3_w				number(10);


ds_repeticao_1_w		varchar2(255);
ds_repeticao_2_w 		varchar2(255);
ds_repeticao_3_w		varchar2(255);
ds_repeticao_4_w		varchar2(255);
ds_repeticao_5_w		varchar2(255);
ds_repeticao_6_w		varchar2(255);

ds_repeticao_aux_w		varchar2(255);
vl_media_calc_w			lote_ent_res_ant_repet.vl_media_calc%type;

/*ie_opcao_p � utilizado para definir o resultado de qual repeti��o que dever� retornar
Primeiro resultado = 1
Segundo resultado = 2
Terceiro resultado = 3
Quarto resultado = 4
Quinto resultado = 5
Sexto resultado = 6

M�todo do primeiro resultado = 11
M�todo do segundo resultado = 12
M�todo do terceiro resultado = 13
M�todo do quarto resultado = 14
M�todo do quinto resultado = 15
M�todo do sexto resultado = 16

M�dia calc = 98
M�dia = 99
*/

begin

if 	(nr_prescricao_p is not null) and
	(nr_seq_prescr_p is not null) and
	(nr_seq_resultado_p is not null) and
	(nr_seq_exame_p is not null) then
	
	select 	nvl(max(nr_repeticao),0),
			max(vl_media_calc)
	into	qt_repeticoes_w,
			vl_media_calc_w
	from	lote_ent_res_ant_repet
	where	nr_prescricao = nr_prescricao_p
	and		nr_seq_prescr = nr_seq_prescr_p
	and		nr_seq_resultado = nr_seq_resultado_p
	and		nr_seq_exame = nr_seq_exame_p;

	ds_repeticao_aux_w := '';
	
	if 	(ie_opcao_p = 1) then
		
		select	DECODE(SUBSTR(DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant)),1,1),
						',',
						('0'||DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))),
						(DECODE(nvl(NVL(qt_result_ant,pr_result_ant),0),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))))
		into	ds_repeticao_1_w
		from	lote_ent_res_ant_repet
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_prescr = nr_seq_prescr_p
		and		nr_seq_resultado = nr_seq_resultado_p
		and		nr_seq_exame = nr_seq_exame_p
		and		nr_repeticao = 1;

		if (SUBSTR(ds_repeticao_1_w,1,1) = '.') then
			select ('0'||ds_repeticao_1_w)
			into	ds_repeticao_aux_w
			from 	dual;
			
			ds_repeticao_1_w	:= ds_repeticao_aux_w;
		
		end if;
		
		resultado_w := ds_repeticao_1_w;

	elsif 	(ie_opcao_p = 2) then


		select	DECODE(SUBSTR(DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant)),1,1),
						',',
						('0'||DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))),
						(DECODE(nvl(NVL(qt_result_ant,pr_result_ant),0),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))))
		into	ds_repeticao_2_w
		from	lote_ent_res_ant_repet
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_prescr = nr_seq_prescr_p
		and		nr_seq_resultado = nr_seq_resultado_p
		and		nr_seq_exame = nr_seq_exame_p
		and		nr_repeticao = 2;
		
		if (SUBSTR(ds_repeticao_2_w,1,1) = '.') then
			select ('0'||ds_repeticao_2_w)
			into	ds_repeticao_aux_w
			from 	dual;
			
			ds_repeticao_2_w	:= ds_repeticao_aux_w;
		
		end if;
		

		resultado_w := ds_repeticao_2_w;

	elsif 	(ie_opcao_p = 3) then

		select	DECODE(SUBSTR(DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant)),1,1),
						',',
						('0'||DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))),
						(DECODE(nvl(NVL(qt_result_ant,pr_result_ant),0),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))))
		into	ds_repeticao_3_w
		from	lote_ent_res_ant_repet
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_prescr = nr_seq_prescr_p
		and		nr_seq_resultado = nr_seq_resultado_p
		and		nr_seq_exame = nr_seq_exame_p
		and		nr_repeticao = 3;

		
		if (SUBSTR(ds_repeticao_3_w,1,1) = '.') then
			select ('0'||ds_repeticao_3_w)
			into	ds_repeticao_aux_w
			from 	dual;
			
			ds_repeticao_3_w	:= ds_repeticao_aux_w;
		
		end if;
		
		
		resultado_w := ds_repeticao_3_w;

	elsif 	(ie_opcao_p = 4) then

		select	DECODE(SUBSTR(DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant)),1,1),
						',',
						('0'||DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))),
						(DECODE(nvl(NVL(qt_result_ant,pr_result_ant),0),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))))
		into	ds_repeticao_4_w
		from	lote_ent_res_ant_repet
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_prescr = nr_seq_prescr_p
		and		nr_seq_resultado = nr_seq_resultado_p
		and		nr_seq_exame = nr_seq_exame_p
		and		nr_repeticao = 4;
		
		if (SUBSTR(ds_repeticao_4_w,1,1) = '.') then
			select ('0'||ds_repeticao_4_w)
			into	ds_repeticao_aux_w
			from 	dual;
			
			ds_repeticao_4_w	:= ds_repeticao_aux_w;
		
		end if;
		

		resultado_w := ds_repeticao_4_w;

	elsif 	(ie_opcao_p = 5) then

		select	DECODE(SUBSTR(DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant)),1,1),
						',',
						('0'||DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))),
						(DECODE(nvl(NVL(qt_result_ant,pr_result_ant),0),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))))
		into	ds_repeticao_5_w
		from	lote_ent_res_ant_repet
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_prescr = nr_seq_prescr_p
		and		nr_seq_resultado = nr_seq_resultado_p
		and		nr_seq_exame = nr_seq_exame_p
		and		nr_repeticao = 5;
		
		if (SUBSTR(ds_repeticao_5_w,1,1) = '.') then
			select ('0'||ds_repeticao_5_w)
			into	ds_repeticao_aux_w
			from 	dual;
			
			ds_repeticao_5_w	:= ds_repeticao_aux_w;
		
		end if;
		

		resultado_w := ds_repeticao_5_w;

	elsif 	(ie_opcao_p = 6) then

		select	DECODE(SUBSTR(DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant)),1,1),
						',',
						('0'||DECODE(NVL(qt_result_ant,pr_result_ant),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))),
						(DECODE(nvl(NVL(qt_result_ant,pr_result_ant),0),0,NVL(ds_result_ant,'0'),NVL(qt_result_ant,pr_result_ant))))
		into	ds_repeticao_6_w
		from	lote_ent_res_ant_repet
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_prescr = nr_seq_prescr_p
		and		nr_seq_resultado = nr_seq_resultado_p
		and		nr_seq_exame = nr_seq_exame_p
		and		nr_repeticao = 6;
		
		if (SUBSTR(ds_repeticao_6_w,1,1) = '.') then
			select ('0'||ds_repeticao_6_w)
			into	ds_repeticao_aux_w
			from 	dual;
			
			ds_repeticao_6_w	:= ds_repeticao_aux_w;
		
		end if;
		

		resultado_w := ds_repeticao_6_w;

	--VERIFICAR OS METODOS ANTERIORES

	elsif 	(ie_opcao_p = 11) then

			/*select	SUBSTR(obter_descricao_padrao('METODO_EXAME_LAB','DS_METODO', NR_SEQ_METODO),1,200) ds_metodo
			into	resultado_w
			from	exame_lab_result_item
			where	nr_seq_resultado = nr_seq_resultado_p
			and		nr_seq_prescr = nr_seq_prescr_p
			and		nr_seq_exame = nr_seq_exame_p
			AND 	nr_seq_material IS NOT NULL;*/
			
			SELECT 	SUBSTR(obter_descricao_padrao('METODO_EXAME_LAB','DS_METODO', NR_SEQ_METODO),1,200) ds_metodo
			INTO   	resultado_w
			FROM   	lote_ent_res_ant_repet
			WHERE  	nr_prescricao = nr_prescricao_p
			AND	   	nr_seq_prescr = nr_seq_prescr_p
			AND	   	nr_seq_resultado = nr_seq_resultado_p
			and		nr_seq_exame = nr_seq_exame_p
			AND	   	nr_repeticao = 1;
			
	elsif 	(ie_opcao_p = 12) then

			SELECT 	SUBSTR(obter_descricao_padrao('METODO_EXAME_LAB','DS_METODO', NR_SEQ_METODO),1,200) ds_metodo
			INTO   	resultado_w
			FROM   	lote_ent_res_ant_repet
			WHERE  	nr_prescricao = nr_prescricao_p
			AND	   	nr_seq_prescr = nr_seq_prescr_p
			AND	   	nr_seq_resultado = nr_seq_resultado_p
			and		nr_seq_exame = nr_seq_exame_p
			AND	   	nr_repeticao = 2;

	elsif 	(ie_opcao_p = 13) then

			SELECT 	SUBSTR(obter_descricao_padrao('METODO_EXAME_LAB','DS_METODO', NR_SEQ_METODO),1,200) ds_metodo
			INTO   	resultado_w
			FROM   	lote_ent_res_ant_repet
			WHERE  	nr_prescricao = nr_prescricao_p
			AND	   	nr_seq_prescr = nr_seq_prescr_p
			AND	   	nr_seq_resultado = nr_seq_resultado_p
			and		nr_seq_exame = nr_seq_exame_p
			AND	   	nr_repeticao = 3;

	elsif 	(ie_opcao_p = 14) then

			SELECT 	SUBSTR(obter_descricao_padrao('METODO_EXAME_LAB','DS_METODO', NR_SEQ_METODO),1,200) ds_metodo
			INTO   	resultado_w
			FROM   	lote_ent_res_ant_repet
			WHERE  	nr_prescricao = nr_prescricao_p
			AND	   	nr_seq_prescr = nr_seq_prescr_p
			AND	   	nr_seq_resultado = nr_seq_resultado_p
			and		nr_seq_exame = nr_seq_exame_p
			AND	   	nr_repeticao = 4;
			
	elsif 	(ie_opcao_p = 15) then

			SELECT 	SUBSTR(obter_descricao_padrao('METODO_EXAME_LAB','DS_METODO', NR_SEQ_METODO),1,200) ds_metodo
			INTO   	resultado_w
			FROM   	lote_ent_res_ant_repet
			WHERE  	nr_prescricao = nr_prescricao_p
			AND	   	nr_seq_prescr = nr_seq_prescr_p
			AND	   	nr_seq_resultado = nr_seq_resultado_p
			and		nr_seq_exame = nr_seq_exame_p
			AND	   	nr_repeticao = 5;
			
	elsif 	(ie_opcao_p = 16) then

			SELECT 	SUBSTR(obter_descricao_padrao('METODO_EXAME_LAB','DS_METODO', NR_SEQ_METODO),1,200) ds_metodo
			INTO   	resultado_w
			FROM   	lote_ent_res_ant_repet
			WHERE  	nr_prescricao = nr_prescricao_p
			AND	   	nr_seq_prescr = nr_seq_prescr_p
			AND	   	nr_seq_resultado = nr_seq_resultado_p
			and		nr_seq_exame = nr_seq_exame_p
			AND	   	nr_repeticao = 6;
			
	end if;
	
	
	if (ie_opcao_p = 98) then
		resultado_w := replace(to_char( ''||vl_media_calc_w||'','fm9,990.00'),'.',',');
	end if;

	if (ie_opcao_p = 99) then

		if (ie_trigger_p = 'S') then
		
			select 	coalesce(qt_resultado_p, pr_resultado_p,0)
			into 	qt_result_item_w	
			from 	dual;
			
		else
			select 	nvl(nvl(qt_resultado,pr_resultado),0)
			into	qt_result_item_w
			from	exame_lab_result_item
			where	nr_seq_resultado = nr_seq_resultado_p
			and		nr_seq_prescr = nr_seq_prescr_p
			and		nr_seq_exame = nr_seq_exame_p
			and		nr_seq_material is not null;
		end if;

		if (qt_result_item_w > 0) then

			SELECT NVL((SELECT NVL(NVL(qt_result_ant,pr_result_ant),0)
						FROM   lote_ent_res_ant_repet
						WHERE  nr_prescricao = nr_prescricao_p
						AND	   nr_seq_prescr = nr_seq_prescr_p
						AND	   nr_seq_resultado = nr_seq_resultado_p
						and		nr_seq_exame = nr_seq_exame_p
						AND	   nr_repeticao = qt_repeticoes_w),0)
			INTO   /*ds_resultado_s_w */ ds_resultado_p_w
			FROM dual;

			SELECT NVL((SELECT NVL(NVL(qt_result_ant,pr_result_ant),0)
						FROM   lote_ent_res_ant_repet
						WHERE  nr_prescricao = nr_prescricao_p
						AND	   nr_seq_prescr = nr_seq_prescr_p
						AND	   nr_seq_resultado = nr_seq_resultado_p
						and		nr_seq_exame = nr_seq_exame_p
						AND	   nr_repeticao = (qt_repeticoes_w - 1)),0)
			INTO   /*ds_resultado_t_w*/ ds_resultado_s_w
			FROM dual;

			SELECT NVL((SELECT NVL(NVL(qt_result_ant,pr_result_ant),0)
						FROM   lote_ent_res_ant_repet
						WHERE  nr_prescricao = nr_prescricao_p
						AND	   nr_seq_prescr = nr_seq_prescr_p
						AND	   nr_seq_resultado = nr_seq_resultado_p
						and		nr_seq_exame = nr_seq_exame_p
						AND	   nr_repeticao = (qt_repeticoes_w - 2)),0)
			INTO   /*ds_resultado_q_w*/ ds_resultado_t_w
			FROM dual;

			SELECT NVL((SELECT NVL(NVL(qt_result_ant,pr_result_ant),0)
						FROM   lote_ent_res_ant_repet
						WHERE  nr_prescricao = nr_prescricao_p
						AND	   nr_seq_prescr = nr_seq_prescr_p
						AND	   nr_seq_resultado = nr_seq_resultado_p
						and		nr_seq_exame = nr_seq_exame_p
						AND	   nr_repeticao = (qt_repeticoes_w - 3)),0)
			INTO   /*ds_resultado_qq_w*/ ds_resultado_q_w
			FROM dual;

			SELECT NVL((SELECT NVL(NVL(qt_result_ant,pr_result_ant),0)
						FROM   lote_ent_res_ant_repet
						WHERE  nr_prescricao = nr_prescricao_p
						AND	   nr_seq_prescr = nr_seq_prescr_p
						AND	   nr_seq_resultado = nr_seq_resultado_p
						and		nr_seq_exame = nr_seq_exame_p
						AND	   nr_repeticao = (qt_repeticoes_w - 4)),0)
			INTO  /*ds_resultado_se_w*/ ds_resultado_qq_w
			FROM dual;

			SELECT NVL((SELECT NVL(NVL(qt_result_ant,pr_result_ant),0)
						FROM   lote_ent_res_ant_repet
						WHERE  nr_prescricao = nr_prescricao_p
						AND	   nr_seq_prescr = nr_seq_prescr_p
						AND	   nr_seq_resultado = nr_seq_resultado_p
						and		nr_seq_exame = nr_seq_exame_p
						AND	   nr_repeticao = (qt_repeticoes_w - 5)),0)
			INTO  ds_resultado_se_w
			FROM dual;


			if (qt_repeticoes_w = 1) then

				select	((nvl(to_number(nvl(ds_resultado_p_w,0)),0) + nvl(to_number(nvl(ds_resultado_s_w,'0')),0) + nvl(to_number(nvl(ds_resultado_t_w,'0')),0) + nvl(to_number(nvl(ds_resultado_q_w,'0')),0) + nvl(to_number(nvl(ds_resultado_qq_w,'0')),0) + nvl(to_number(nvl(ds_resultado_se_w,'0')),0)) / 1)
				into	qt_resultado_media_w
				from 	dual;

			elsif (qt_repeticoes_w = 2) then

				select	((nvl(to_number(nvl(ds_resultado_p_w,0)),0) + nvl(to_number(nvl(ds_resultado_s_w,'0')),0) + nvl(to_number(nvl(ds_resultado_t_w,'0')),0) + nvl(to_number(nvl(ds_resultado_q_w,'0')),0) + nvl(to_number(nvl(ds_resultado_qq_w,'0')),0) + nvl(to_number(nvl(ds_resultado_se_w,'0')),0)) / 2)
				into	qt_resultado_media_w
				from 	dual;

			elsif (qt_repeticoes_w = 3) then

				select	((nvl(to_number(nvl(ds_resultado_p_w,0)),0) + nvl(to_number(nvl(ds_resultado_s_w,'0')),0) + nvl(to_number(nvl(ds_resultado_t_w,'0')),0) + nvl(to_number(nvl(ds_resultado_q_w,'0')),0) + nvl(to_number(nvl(ds_resultado_qq_w,'0')),0) + nvl(to_number(nvl(ds_resultado_se_w,'0')),0)) / 3)
				into	qt_resultado_media_w
				from 	dual;

			elsif (qt_repeticoes_w = 4) then

				select	((nvl(to_number(nvl(ds_resultado_p_w,0)),0) + nvl(to_number(nvl(ds_resultado_s_w,'0')),0) + nvl(to_number(nvl(ds_resultado_t_w,'0')),0) + nvl(to_number(nvl(ds_resultado_q_w,'0')),0) + nvl(to_number(nvl(ds_resultado_qq_w,'0')),0) + nvl(to_number(nvl(ds_resultado_se_w,'0')),0)) / 4)
				into	qt_resultado_media_w
				from 	dual;

			elsif (qt_repeticoes_w = 5) then

				select	((nvl(to_number(nvl(ds_resultado_p_w,0)),0) + nvl(to_number(nvl(ds_resultado_s_w,'0')),0) + nvl(to_number(nvl(ds_resultado_t_w,'0')),0) + nvl(to_number(nvl(ds_resultado_q_w,'0')),0) + nvl(to_number(nvl(ds_resultado_qq_w,'0')),0) + nvl(to_number(nvl(ds_resultado_se_w,'0')),0)) / 5)
				into	qt_resultado_media_w
				from 	dual;

			elsif (qt_repeticoes_w >= 6) then

				select	((nvl(to_number(nvl(ds_resultado_p_w,0)),0) + nvl(to_number(nvl(ds_resultado_s_w,'0')),0) + nvl(to_number(nvl(ds_resultado_t_w,'0')),0) + nvl(to_number(nvl(ds_resultado_q_w,'0')),0) + nvl(to_number(nvl(ds_resultado_qq_w,'0')),0) + nvl(to_number(nvl(ds_resultado_se_w,'0')),0)) / 6)
				into	qt_resultado_media_w
				from 	dual;

			end if;

			if (qt_resultado_media_w > 0) then

				resultado_w := replace(to_char( ''||qt_resultado_media_w||'','fm9,990.00'),'.',',');

			end if;

		end if;

	end if;

end if;

return	resultado_w;

end lote_ent_obter_result_ant;
/
