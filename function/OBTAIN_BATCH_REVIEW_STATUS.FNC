create or replace function obtain_batch_review_status(nr_seq_lote_p number, cd_resp_type_p varchar2)
return varchar2
is
  ie_status_review_w ap_lote_review.ie_status_review%type;
  cd_status_review_w varchar2(256);
begin
    ie_status_review_w := 'X';
    cd_status_review_w := 'NA';
 
    if ( nr_seq_lote_p is not null ) then
      select nvl(ie_status_review,'X')
      into   ie_status_review_w
      from   ap_lote_review
      where  nr_seq_lote = nr_seq_lote_p;
    end if;
    
    if ( nvl(cd_resp_type_p, 'NA') = 'CD' ) then
        cd_status_review_w := ie_status_review_w;
    elsif ( nvl(cd_resp_type_p, 'NA') = 'DS' ) then
        if (ie_status_review_w = 'X') then
            cd_status_review_w  := 'NA';
        else
            select SUBSTR(expressao_pck.obter_desc_expressao(CD_EXP_VALOR_DOMINIO, PKG_I18N.get_user_locale()), 1, 255)
                into cd_status_review_w
            from valor_dominio 
            where cd_dominio = 10425 and vl_dominio = ie_status_review_w;
        end if;
    else
        cd_status_review_w := 'NA';
    end if;
 


    return cd_status_review_w;
end obtain_batch_review_status;
/