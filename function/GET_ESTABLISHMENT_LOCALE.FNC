create or replace 
function get_establishment_locale(cd_estabelecimento_p	number)
	return varchar2 as

ds_locale_w	establishment_locale.ds_locale%type;

begin

select	max(ds_locale)
into	ds_locale_w
from	establishment_locale
where	cd_estabelecimento = cd_estabelecimento_p;

return ds_locale_w;

end get_establishment_locale;
/