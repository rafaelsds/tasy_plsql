create or replace
function Obter_conversao_ml(	cd_material_p	number,
			qt_dose_p	number,
			cd_unid_med_p	varchar2)
			return number is


qt_retorno_w			number(18,6);
qt_conversao_w			number(18,6);
cd_unidade_medida_w		varchar2(30);
cd_unid_med_cons_w		varchar2(30);

begin
cd_unidade_medida_w	:= upper(cd_unid_med_p);

if	(cd_unidade_medida_w = upper (obter_unid_med_usua('ML'))) then
	qt_retorno_w	:= qt_dose_p;
else
	cd_unid_med_cons_w	:= upper(substr(obter_dados_material_estab(cd_material_p, wheb_usuario_pck.get_cd_estabelecimento, 'UMS'),1,30));

	if	(cd_unidade_medida_w = cd_unid_med_cons_w) then
		qt_retorno_w	:= qt_dose_p;
	else	
		select	nvl(max(qt_conversao),0)
		into	qt_conversao_w
		from	material_conversao_unidade
		where	cd_material = cd_material_p
		and	upper(cd_unidade_medida) = cd_unidade_medida_w;

		qt_retorno_w	:= dividir(qt_dose_p,qt_conversao_w);
	end if;
	
	select	nvl(max(qt_conversao),0)
	into	qt_conversao_w
	from	material_conversao_unidade
	where	cd_material = cd_material_p
	and	upper(cd_unidade_medida) = upper (obter_unid_med_usua('ML'));

	qt_retorno_w := qt_retorno_w * qt_conversao_w;
end if;

return qt_retorno_w;

end Obter_conversao_ml;
/

