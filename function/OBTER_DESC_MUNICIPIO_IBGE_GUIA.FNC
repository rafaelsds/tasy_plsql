create or replace 
function obter_desc_municipio_ibge_guia(cd_municipio_ibge_p	varchar2) 
		return varchar2 is

ds_municipio_w	varchar2(100);

begin

select	substr(nvl(ds_municipio_guia_medico,ds_municipio),1,100)
into	ds_municipio_w
from	sus_municipio
where	cd_municipio_ibge = cd_municipio_ibge_p;

return	ds_municipio_w;

end	obter_desc_municipio_ibge_guia;
/