create or replace
function	Obter_prim_DsHorarios(	ds_horarios_p	varchar2)
					return varchar2 is
			
ds_retorno_w	varchar2(10) := '';
cont_espaco_w 	number(5);
ds_horarios_w	varchar2(2000);
cont_ponto_w	number(5);


begin
ds_horarios_w := cpoe_substr_horario(ds_horarios_p,2000);

if	(obter_se_somente_numero(ds_horarios_w) = 'N') then 
	ds_horarios_w := cpoe_retirar_das_as(ds_horarios_w);
end if;

while	substr(ds_horarios_w,1,1) = ' ' loop 
begin
ds_horarios_w	:= substr(ds_horarios_w,2,length(ds_horarios_w));
end;
end loop;
ds_horarios_w	:= substr(ds_horarios_w,1,5);

if	(obter_se_somente_numero(ds_horarios_w) = 'S') then 
	cont_espaco_w 	:= instr(ds_horarios_w,' ');
	cont_ponto_w	:= instr(ds_horarios_w,':');	
	
	if	(cont_espaco_w = 2)	then
		ds_retorno_w	:=	'0'	||	substr(ds_horarios_w,1,1)	||':00';
	elsif	(cont_espaco_w = 3) and
		(substr(ds_horarios_w,1,2) < 24) then
		ds_retorno_w 	:=	substr(ds_horarios_w,1,2)	||':00';
	elsif	(cont_espaco_w = 0) then
		if	(cont_ponto_w = 3) then
			if	(length(ds_horarios_w) = 5)	then
				ds_retorno_w	:=	ds_horarios_w; 
			elsif	(length(ds_horarios_w) = 3) then
				ds_retorno_w	:=	ds_horarios_w	||'00';
			elsif	(length(ds_horarios_w) = 4) then
				ds_retorno_w 	:=	ds_horarios_w	||'0';
			end if;
		elsif	((length(ds_horarios_w) = 2) and
			 (substr(ds_horarios_w,1,2) < 24))	then
			ds_retorno_w	:=	substr(ds_horarios_w,1,2)	||':00';
		elsif	((length(ds_horarios_w) = 1) and
			 (substr(ds_horarios_w,1,1) <> ':'))  then
			ds_retorno_w	:=	'0'	||	substr(ds_horarios_w,1,1)	||':00';	
		end if;
	end if;
	
end if;

return	ds_retorno_w;

end Obter_prim_DsHorarios;
/
