create or replace
function Permitir_Fechar_Conta_Adiant
		(nr_interno_conta_p		varchar2)
 		return varchar2 is

ie_fecha_conta_w	varchar2(01)	:= 'S';
ie_consiste_w		varchar2(01)	:= 'S';
nr_atendimento_w	number(15,0);
cd_convenio_w		number(15,0);
cd_estabelecimento_w	number(15,0);
qt_reg_w		number(10,0);
		
begin

ie_fecha_conta_w	:= 'S';

select	max(nr_atendimento),
	max(cd_convenio_parametro),
	max(cd_estabelecimento)
into	nr_atendimento_w,
	cd_convenio_w,
	cd_estabelecimento_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

if	(nr_atendimento_w	is not null) then
	
	select	nvl(max(ie_fechar_atend_adiant),'N')
	into	ie_consiste_w
	from	convenio_estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_convenio		= cd_convenio_w;
	
	if	(ie_consiste_w	= 'S') then
		
		select	count(*)
		into	qt_reg_w
		from	adiantamento
		where	nr_atendimento	= nr_atendimento_w
		and	dt_baixa	is null;
		
		if	(qt_reg_w	> 0) then
			ie_fecha_conta_w	:= 'N';
		end if;
	end if;
	
end if;

return	ie_fecha_conta_w;

end Permitir_Fechar_Conta_Adiant;
/
