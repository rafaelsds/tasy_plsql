CREATE OR REPLACE
Function	Obter_Dados_Ultimo_Atend_dt(	cd_pessoa_fisica_p		VarChar2,
					nr_atendimento_p		Number,
					ie_tipo_atend_p		Number,
					ie_opcao_p		VarChar2)
					Return date is

/* ie_opcao_p :

  - DA - data de alta
  - DE - data de entrada
  - DAF - data ultimo atendimento (ficha financeira).  
		  
*/

nr_atendimento_w	number(10);
dt_retorno_w		date := null;

BEGIN

if	(nr_atendimento_p is not null) then
	begin
	nr_atendimento_w	:= null;
	if (cd_pessoa_fisica_p is not null) then
		select 	max(nr_atendimento)
		into	nr_atendimento_w
		from 	atendimento_paciente
		where 	nr_atendimento		< nr_atendimento_p
		and	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and 	ie_tipo_atendimento	= nvl(ie_tipo_atend_p, ie_tipo_atendimento);
	end if;
	end;
	
elsif (cd_pessoa_fisica_p is not null) then
	begin
	if	(ie_tipo_atend_p is null) then
		select nvl(max(nr_atendimento),null)
		into	nr_atendimento_w
		from atendimento_paciente
		where cd_pessoa_fisica		= cd_pessoa_fisica_p;
	else
		select nvl(max(nr_atendimento),null)
		into	nr_atendimento_w
		from atendimento_paciente
		where cd_pessoa_fisica		= cd_pessoa_fisica_p
		and ie_tipo_atendimento	= nvl(ie_tipo_atend_p, ie_tipo_atendimento);
	end if;
	end;
end if;

if	(nr_atendimento_w is not null) then

	if	(ie_opcao_p = 'DA') then
	
		select max(dt_alta)
		into dt_retorno_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_w;
		
	elsif	(ie_opcao_p = 'DE') then
	
		select max(dt_entrada)
		into dt_retorno_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_w;
		
	elsif	(ie_opcao_p = 'DAF') then
		
		select max(dt_alta)
		into dt_retorno_w
		from atendimento_paciente
		where nr_atendimento = nr_atendimento_w;
	
	end if;
end if;

RETURN dt_retorno_w;

END Obter_Dados_Ultimo_Atend_dt;
/
