create or replace
function obter_desc_motivo_int_gas(nr_sequencia_p	number)
 		    	return varchar2 is

ds_motivo_w	varchar2(2000);
			
begin

select	max(ds_motivo)
into	ds_motivo_w
from	motivo_evento_gas
where	nr_sequencia	= nr_sequencia_p;


return	ds_motivo_w;

end obter_desc_motivo_int_gas;
/