CREATE OR REPLACE
FUNCTION Obter_Dados_Loinc_Exame (nr_prescricao_p	NUMBER,
                                  nr_seq_prescr_p NUMBER,
                                  IE_OPCAO_P		VARCHAR2)
                                  RETURN VARCHAR2 IS

cd_loinc_w	    Varchar2(10);
ds_componente_w	Varchar2(255);
ds_retorno_w		varchar2(255);

/*
CL - C�digo Loinc
DC - Componente
*/

BEGIN
  if ((nr_prescricao_p is not null) and
      (nr_seq_prescr_p is not null)) then
    
  	select lld.cd_loinc, lldr.ds_componente
      into cd_loinc_w, ds_componente_w
      from PRESCR_PROCEDIMENTO_LOINC ppl
     inner join LAB_LOINC_DADOS lld on lld.nr_sequencia = ppl.nr_seq_loinc_dados  
     inner join LAB_LOINC_DADOS_REG lldr on lldr.nr_seq_loinc_dados = lld.nr_sequencia
     where ppl.nr_prescricao = nr_prescricao_p and
           ppl.nr_seq_prescr = nr_seq_prescr_p;
       
    if (nvl(ie_opcao_p, 'CL') = 'CL') then
      ds_retorno_w := cd_loinc_w;    
    elsif	(nvl(ie_opcao_p, 'CL') = 'DC') then		  
      ds_retorno_w := ds_componente_w;
    end if;
  end if;

  RETURN	ds_retorno_w;
END Obter_Dados_Loinc_Exame;
/