create or replace 
function OBTER_SE_REGRA_MED_CRIT_REP
	(cd_medico_prescr_p	varchar2,
	 ie_regra_medico_p	varchar2) return varchar2 is


ds_retorno_w		varchar2(100);
ie_corpo_clinico_w	varchar2(100);

begin

ds_retorno_w		:= 'S';

if	(nvl(ie_regra_medico_p, 'X') = '1') then -- Edgar 16/03/2009, tratar regra para somente considerar se o m�dico da prescri��o N�O for do corpo cl�nico

	select	ie_corpo_clinico
	into	ie_corpo_clinico_w
	from	medico
	where	cd_pessoa_fisica	= cd_medico_prescr_p;

	ds_retorno_w			:= 'S';
	if	(nvl(ie_corpo_clinico_w, 'N') = 'S') then
		ds_retorno_w		:= 'N';
	end if;
end if;

return	ds_retorno_w;

end OBTER_SE_REGRA_MED_CRIT_REP;
/
