create or replace
function proj_obter_media_hora_cobrar	(	nr_seq_proj_p			number,
											dt_inicio_vigencia_p	date	)
 		    	return number is

vl_retorno_w			number(15,2);

begin

	select	nvl(avg(nvl(vl_hora, 0)), 0)
	into	vl_retorno_w
	from	proj_hora_cobrar
	where	nr_seq_proj = nr_seq_proj_p
	and		dt_inicio >= dt_inicio_vigencia_p;

return	vl_retorno_w;

end proj_obter_media_hora_cobrar;
/