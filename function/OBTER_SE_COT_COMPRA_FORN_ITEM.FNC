create or replace
function obter_se_cot_compra_forn_item(	nr_cot_compra_p			number,
					nr_item_cot_compra_p		number)
 		    	return varchar2 is

qt_existe_w		number(10);
ie_existe_w		varchar2(1) := 'N';
			
begin

select	count(*)
into	qt_existe_w
from	cot_compra_forn_item
where	nr_cot_compra		= nr_cot_compra_p
and	nr_item_cot_compra	= nr_item_cot_compra_p;

if	(qt_existe_w > 0) then
	ie_existe_w := 'S';
end if;

return	ie_existe_w;

end obter_se_cot_compra_forn_item;
/