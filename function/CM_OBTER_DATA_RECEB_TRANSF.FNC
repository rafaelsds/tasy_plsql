create or replace
function	cm_obter_data_receb_transf(
		nr_seq_conjunto_p		number) return date is

nr_seq_lote_transf_w		number(10);
dt_retorno_w			date;

begin

select	max(a.nr_sequencia)
into	nr_seq_lote_transf_w
from	cm_lote_transferencia a,
	cm_lote_transf_conj b
where	a.nr_sequencia = b.nr_seq_lote_transf
and	a.dt_liberacao is not null
and	b.nr_seq_conjunto = nr_seq_conjunto_p;

if	(nvl(nr_seq_lote_transf_w,0) > 0) then
	begin

	select	dt_baixa
	into	dt_retorno_w
	from	cm_lote_transferencia
	where	nr_sequencia = nr_seq_lote_transf_w;

	end;
end if;

return	dt_retorno_w;

end cm_obter_data_receb_transf;
/