create or replace
function pls_obter_status_imp_a550(	nr_seq_camara_contest_p		ptu_camara_contestacao.nr_sequencia%type)
					return varchar2 is

ds_retorno_w		varchar2(255) := 'Arquivo de contesta��o importado com sucesso.';
nr_seq_log_w		ptu_processo_camara_cont.nr_sequencia%type;
ie_status_processo_w	ptu_processo_camara_cont.ie_status_processo%type;

begin
select	max(nr_sequencia)
into	nr_seq_log_w
from	ptu_processo_camara_cont
where	nr_seq_camara_contest	 = nr_seq_camara_contest_p
and	ie_tipo_processo = 'IA';

if	(nr_seq_log_w is not null) then
	select	nvl(max(ie_status_processo),'FI')
	into	ie_status_processo_w
	from	ptu_processo_camara_cont
	where	nr_sequencia	= nr_seq_log_w;
end if;

if	(ie_status_processo_w = 'FI') then
	select	max(nr_sequencia)
	into	nr_seq_log_w
	from	ptu_processo_camara_cont
	where	nr_seq_camara_contest	 = nr_seq_camara_contest_p
	and	ie_tipo_processo = 'GD';
	
	if	(nr_seq_log_w is not null) then
		select	nvl(max(ie_status_processo),'FI')
		into	ie_status_processo_w
		from	ptu_processo_camara_cont
		where	nr_sequencia	= nr_seq_log_w;
		
		if	(ie_status_processo_w = 'FA') then
			ds_retorno_w := 'Falha na gera��o do lote de discuss�o.';
		end if;
	end if;

elsif	(ie_status_processo_w = 'FA') then
	ds_retorno_w := 'Falha na importa��o do arquivo A550.';
end if;

return	ds_retorno_w;

end pls_obter_status_imp_a550;
/