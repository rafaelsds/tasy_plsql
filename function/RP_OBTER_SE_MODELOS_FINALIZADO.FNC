create or replace
function rp_obter_se_modelos_finalizado(nr_sequencia_p		number)
 		    	return varchar2 is
ds_retorno_w	varchar(10);
ie_existe_w	number(10);
begin

--verifica se existe o paciente na tabela de modelos
select 	max(nr_sequencia)
into	ie_existe_w
from	rp_pac_modelo_agendamento
where	nr_seq_pac_reab = nr_sequencia_p;

if (ie_existe_w is not null) then
	select 	count(*) 
	into	ds_retorno_w
	from	rp_pac_modelo_agendamento
	where	dt_fim_tratamento is null
	and	nr_seq_pac_reab = nr_sequencia_p;
else
	ds_retorno_w := '1';
end if;

return	ds_retorno_w;

end rp_obter_se_modelos_finalizado;
/