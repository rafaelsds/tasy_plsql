create or replace
function obter_vl_comprometido_proj_rec(	nr_seq_proj_rec_p		number,
					dt_referencia_p		date,
					ie_dia_mes_p		varchar2)
 		    	return number is

/*ie_dia_mes_p
M - Mes
D - Dia
*/			
			
vl_pendente_oc_w	number(13,4);
vl_acum_oc_w		number(13,4);
vl_titulo_w		number(13,4);
vl_comprometido_w	number(13,4);
ie_param_25_w		varchar2(1) := 'N';
vl_total_solic_acum_w	number(13,4);
ie_considera_solic_w	varchar2(1);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nm_usuario_w		usuario.nm_usuario%type;
cd_perfil_w		perfil.cd_perfil%type;
			
cursor c01 is
select	((c.qt_prevista_entrega - nvl(qt_real_entrega,0)) * b.vl_unitario_material) 
from	ordem_compra a,
	ordem_compra_item b,
	ordem_compra_item_entrega c
where	a.nr_ordem_compra = b.nr_ordem_compra
and	b.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_item_oci = c.nr_item_oci
and	a.dt_aprovacao is not null
and	a.dt_baixa is null
and	a.nr_seq_motivo_cancel is null
and	c.dt_cancelamento is null
and	(((ie_param_25_w = 'N') and (b.nr_seq_proj_rec = nr_seq_proj_rec_p)) or	
	((ie_param_25_w = 'S') and ((b.nr_seq_proj_rec = nr_seq_proj_rec_p) or (b.nr_seq_proj_rec in (select nr_sequencia from projeto_recurso where nr_seq_superior = nr_seq_proj_rec_p)))))
and	(((ie_dia_mes_p = 'M') and (PKG_DATE_UTILS.start_of(a.dt_aprovacao,'month',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'month',0))) or
	 ((ie_dia_mes_p = 'D') and (PKG_DATE_UTILS.start_of(a.dt_aprovacao,'dd',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'dd',0))));
			
begin
vl_titulo_w		:= 0;
vl_acum_oc_w		:= 0;
vl_comprometido_w	:= 0;
vl_total_solic_acum_w	:= 0;

select	wheb_usuario_pck.get_cd_estabelecimento,
	wheb_usuario_pck.get_nm_usuario,
	obter_perfil_ativo
into	cd_estabelecimento_w,
	nm_usuario_w,
	cd_perfil_w
from	dual;

select	nvl(obter_valor_param_usuario(928, 25, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w),'N')
into	ie_param_25_w
from	dual;

select	nvl(ie_cons_solic_acum_proj_rec,'N')
into	ie_considera_solic_w
from	parametro_compras
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

select	nvl(sum(vl_titulo),0)
into	vl_titulo_w
from	titulo_pagar
where	(((ie_param_25_w = 'N') and (nr_seq_proj_rec = nr_seq_proj_rec_p)) or	
	((ie_param_25_w = 'S') and ((nr_seq_proj_rec = nr_seq_proj_rec_p) or (nr_seq_proj_rec in (select nr_sequencia from projeto_recurso where nr_seq_superior = nr_seq_proj_rec_p)))))
and	dt_liquidacao is null
and	ie_situacao = 'A'
and	(((ie_dia_mes_p = 'M') and (PKG_DATE_UTILS.start_of(dt_emissao,'month',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'month',0))) or
	 ((ie_dia_mes_p = 'D') and (PKG_DATE_UTILS.start_of(dt_emissao,'dd',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'dd',0))));

open C01;
loop
fetch C01 into	
	vl_pendente_oc_w;
exit when C01%notfound;
	begin
	vl_acum_oc_w := vl_acum_oc_w + vl_pendente_oc_w;
	end;
end loop;
close C01;

if	(ie_considera_solic_w = 'S') then

	select	nvl(sum(b.qt_material * b.vl_unit_previsto),0)
	into	vl_total_solic_acum_w
	from	solic_compra a,
		solic_compra_item b
	where	a.nr_solic_compra = b.nr_solic_compra
	and	(((ie_param_25_w = 'N') and (b.nr_seq_proj_rec = nr_seq_proj_rec_p)) or	
		((ie_param_25_w = 'S') and ((b.nr_seq_proj_rec = nr_seq_proj_rec_p) or (b.nr_seq_proj_rec in (select nr_sequencia from projeto_recurso where nr_seq_superior = nr_seq_proj_rec_p)))))
	and	a.dt_liberacao is not null
	and	a.nr_seq_motivo_cancel is null
	and	b.vl_unit_previsto > 0
	and	(((ie_dia_mes_p = 'M') and (PKG_DATE_UTILS.start_of(a.dt_liberacao,'month',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'month',0))) or
		((ie_dia_mes_p = 'D') and (PKG_DATE_UTILS.start_of(a.dt_liberacao,'dd',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'dd',0))))
	and	not exists (	select	 1
				from	ordem_compra x,
					ordem_compra_item y
				where	x.nr_ordem_compra = y.nr_ordem_compra
				and	y.nr_solic_compra = a.nr_solic_compra
				and	y.nr_item_solic_compra	= b.nr_item_solic_compra
				and	substr(obter_se_item_oc_cancelado(x.nr_ordem_compra,y.nr_item_solic_compra),1,1) = 'N'
				and	x.dt_liberacao is not null);
end if;

vl_comprometido_w := nvl(vl_titulo_w,0) + nvl(vl_acum_oc_w,0) + nvl(vl_total_solic_acum_w,0);


return	vl_comprometido_w;

end obter_vl_comprometido_proj_rec;
/