create or replace
function obter_qt_total_usuario( cd_tipo_agenda_p		varchar2,
			cd_agenda_p		number,
			dt_inicial_p		date,
			dt_final_p		date,
			nr_seq_exame_p		number,
			cd_medico_p		varchar2,
			cd_setor_atendimento_p	number,
			nm_usuario_p		varchar2,
			ie_opcao_p		varchar2)
			
 		    	return number is
			
ds_retorno_w			number(6);

qt_total_agendam_cons_w		number(6);
qt_total_agendam_exam_w		number(6);
qt_total_cp_transf_cons_w	number(6);
qt_total_cp_transf_exam_w	number(6);
qt_total_cancel_cons_w		number(6);
qt_total_cancel_exam_w		number(6);
ds_retorno_cons_w		number(6);
ds_retorno_exam_w		number(6);

/*  ie_opcao 
1  - Total de Agendamentos
2  - Total de C�pias/Transferencias
3  - Total de Cancelamentos
4  - Total geral  */

begin

if (dt_inicial_p is not null) and 
   (dt_final_p is not null) then
	if ((cd_tipo_agenda_p = '3') or (cd_tipo_agenda_p = '0'))  then
		if (ie_opcao_p = '1') then	   
			select 	count(a.nm_usuario_origem)
			into	qt_total_agendam_cons_w
			from   	agenda_consulta a, 
				agenda b
			where  	a.cd_agenda = b.cd_agenda
			and	a.cd_agenda = cd_agenda_p
			and	b.ie_situacao = 'A'
			and	a.cd_pessoa_fisica is not null
			and	b.cd_tipo_agenda = 3
			and	trunc(a.dt_agendamento) between dt_inicial_p and fim_dia( dt_final_p)
			and	((a.nr_seq_exame = nr_seq_exame_p) or ( nr_seq_exame_p = '0'))
			and	((b.cd_pessoa_fisica = cd_medico_p) or ( cd_medico_p = '0'))
			and	((a.cd_setor_atendimento = cd_setor_atendimento_p) or ( cd_setor_atendimento_p = '0'))
			and	upper(a.nm_usuario_origem) = upper(nm_usuario_p);
			
			ds_retorno_w := qt_total_agendam_cons_w;
			ds_retorno_cons_w := qt_total_agendam_cons_w;
		
		elsif (ie_opcao_p = '2') then		
			select	count(a.nm_usuario_copia_trans)
			into	qt_total_cp_transf_cons_w
			from   	agenda_consulta a, 
				agenda b
			where  	a.cd_agenda = b.cd_agenda
			and	a.cd_agenda = cd_agenda_p
			and	b.ie_situacao = 'A'
			and	a.cd_pessoa_fisica is not null
			and	b.cd_tipo_agenda = 3
			and	trunc(a.dt_agendamento) between dt_inicial_p and fim_dia( dt_final_p)
			and	((a.nr_seq_exame = nr_seq_exame_p) or ( nr_seq_exame_p = '0'))
			and	((b.cd_pessoa_fisica = cd_medico_p) or ( cd_medico_p = '0'))
			and	((a.cd_setor_atendimento = cd_setor_atendimento_p) or ( cd_setor_atendimento_p = '0'))
			and	upper(a.nm_usuario_copia_trans) = upper(nm_usuario_p);
			
			ds_retorno_w := qt_total_cp_transf_cons_w;
			ds_retorno_cons_w := qt_total_cp_transf_cons_w;
		
		elsif (ie_opcao_p = '3') then		
			select	count(a.nm_usuario_cancelamento)
			into	qt_total_cancel_cons_w
			from   	agenda_consulta a, 
				agenda b
			where  	a.cd_agenda = b.cd_agenda
			and	a.cd_agenda = cd_agenda_p
			and	b.ie_situacao = 'A'
			and	a.cd_pessoa_fisica is not null
			and	b.cd_tipo_agenda = 3
			and	trunc(a.dt_agendamento) between dt_inicial_p and fim_dia( dt_final_p)
			and	((a.nr_seq_exame = nr_seq_exame_p) or ( nr_seq_exame_p = '0'))
			and	((b.cd_pessoa_fisica = cd_medico_p) or ( cd_medico_p = '0'))
			and	((a.cd_setor_atendimento = cd_setor_atendimento_p) or ( cd_setor_atendimento_p = '0'))
			and	upper(a.nm_usuario_cancelamento) = upper(nm_usuario_p);
			
			ds_retorno_w := qt_total_cancel_cons_w;
			ds_retorno_cons_w := qt_total_cancel_cons_w;
			
		end if;
		
	end if;	
		
	if ((cd_tipo_agenda_p = '2') or (cd_tipo_agenda_p = '0')) then
		if (ie_opcao_p = '1') then	   
			select 	count(a.nm_usuario_orig)
			into	qt_total_agendam_exam_w
			from   	agenda_paciente a, 
				agenda b
			where  	a.cd_agenda = b.cd_agenda
			and	a.cd_agenda = cd_agenda_p
			and	b.ie_situacao = 'A'
			and	a.cd_pessoa_fisica is not null
			and	b.cd_tipo_agenda = 2
			and	trunc(a.dt_agendamento) between dt_inicial_p and fim_dia( dt_final_p)
			and	((b.cd_pessoa_fisica = cd_medico_p) or ( cd_medico_p = '0'))
			and	((a.cd_setor_atendimento = cd_setor_atendimento_p) or ( cd_setor_atendimento_p = '0'))
			and	upper(a.nm_usuario_orig) = upper(nm_usuario_p);

			ds_retorno_w := qt_total_agendam_exam_w;
			ds_retorno_exam_w := qt_total_agendam_exam_w;
		
		elsif (ie_opcao_p = '2') then		
			select	count(a.nm_usuario_copia_trans)
			into	qt_total_cp_transf_exam_w
			from   	agenda_paciente a, 
				agenda b
			where  	a.cd_agenda = b.cd_agenda
			and	a.cd_agenda = cd_agenda_p
			and	b.ie_situacao = 'A'
			and	a.cd_pessoa_fisica is not null
			and	b.cd_tipo_agenda = 2
			and	trunc(a.dt_agendamento) between dt_inicial_p and fim_dia( dt_final_p)
			and	((b.cd_pessoa_fisica = cd_medico_p) or ( cd_medico_p = '0'))
			and	((a.cd_setor_atendimento = cd_setor_atendimento_p) or ( cd_setor_atendimento_p = '0'))
			and	upper(a.nm_usuario_copia_trans) = upper(nm_usuario_p);
			
			ds_retorno_w := qt_total_cp_transf_exam_w;
			ds_retorno_exam_w := qt_total_cp_transf_exam_w;
		
		elsif (ie_opcao_p = '3') then		
			select	count(a.nm_usuario_cancel)
			into	qt_total_cancel_exam_w
			from   	agenda_paciente a, 
				agenda b
			where  	a.cd_agenda = b.cd_agenda
			and	a.cd_agenda = cd_agenda_p
			and	b.ie_situacao = 'A'
			and	a.cd_pessoa_fisica is not null
			and	b.cd_tipo_agenda = 2
			and	trunc(a.dt_agendamento) between dt_inicial_p and fim_dia( dt_final_p)
			and	((b.cd_pessoa_fisica = cd_medico_p) or ( cd_medico_p = '0'))
			and	((a.cd_setor_atendimento = cd_setor_atendimento_p) or ( cd_setor_atendimento_p = '0'))
			and	upper(a.nm_usuario_cancel) = upper(nm_usuario_p);
			
			ds_retorno_w := qt_total_cancel_exam_w;
			ds_retorno_exam_w := qt_total_cancel_exam_w;
			
		end if;		
	end if;	
end if;

if (cd_tipo_agenda_p = '0') then
	ds_retorno_w := ds_retorno_cons_w + ds_retorno_exam_w;	
end if;

return	ds_retorno_w;

end obter_qt_total_usuario;
/