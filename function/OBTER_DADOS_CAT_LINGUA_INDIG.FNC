create or replace
function obter_dados_cat_lingua_indig(	nr_seq_cat_lingua_indig_p		number,
					ie_tipo_inf_p				varchar2)
					return varchar2 is

ds_retorno_w			varchar2(255);
cd_lingua_indigena_w		cat_lingua_indigena.cd_lingua_indigena%type;
ds_lingua_indigena_w		cat_lingua_indigena.ds_lingua_indigena%type;
cd_lingua_indigena_mf_w		cat_lingua_indigena.cd_lingua_indigena_mf%type;

			
begin
if (nr_seq_cat_lingua_indig_p is not null) then
	select	cd_lingua_indigena,
		ds_lingua_indigena,
		cd_lingua_indigena_mf
	into	cd_lingua_indigena_w,
		ds_lingua_indigena_w,
		cd_lingua_indigena_mf_w
	from	cat_lingua_indigena
	where	nr_sequencia	= nr_seq_cat_lingua_indig_p;

	if 	(ie_tipo_inf_p = 'CD_LINGUA_INDIGENA') then
		ds_retorno_w 	:= cd_lingua_indigena_w;
		
	elsif	(ie_tipo_inf_p = 'DS_LINGUA_INDIGENA') then
		ds_retorno_w 	:= ds_lingua_indigena_w;
		
	elsif	(ie_tipo_inf_p = 'CD_LINGUA_INDIGENA_MF') then
		ds_retorno_w 	:= cd_lingua_indigena_mf_w;
	end if;
end if;

return	ds_retorno_w;

end obter_dados_cat_lingua_indig;
/