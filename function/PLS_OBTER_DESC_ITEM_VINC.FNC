create or replace
function pls_obter_desc_item_vinc(	nr_seq_conta_item_regra_p	number,
					ie_tipo_item_p			varchar2,
					nr_seq_item_tiss_p		number)
 		    	return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter a descri��o do item a partir do nr_seq_item_tiss

ie_tipo_item_p : origem do item que est� reazlizando o v�nculo
P - procediemnto
M - material
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ds_retorno_w		varchar2(255);
cd_guia_w		pls_conta.cd_guia%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;

begin

if (ie_tipo_item_p = 'P') then
	begin	
		select	a.cd_guia,
			a.nr_seq_segurado
		into	cd_guia_w,
			nr_seq_segurado_w
		from	pls_conta a,
			pls_conta_proc b
		where	b.nr_seq_conta	= a.nr_sequencia
		and	b.nr_sequencia	= nr_seq_conta_item_regra_p;				
	exception
	when others then
		cd_guia_w	:= null;
	end;
else
	begin	
		select	a.cd_guia,
			a.nr_seq_segurado
		into	cd_guia_w,
			nr_seq_segurado_w
		from	pls_conta a,
			pls_conta_mat b
		where	b.nr_seq_conta	= a.nr_sequencia
		and	b.nr_sequencia	= nr_seq_conta_item_regra_p;				
	exception
	when others then
		cd_guia_w	:= null;
	end;
end if;

if (cd_guia_w is not null) then
	select ds_item
	into	ds_retorno_w
	from	(
			select 	substr(obter_descricao_procedimento(a.cd_procedimento, a.ie_origem_proced),1,255) ds_item
			from 	pls_conta_proc a,
				pls_conta b,
				pls_conta_proc_regra c
			where   a.nr_seq_conta		= b.nr_sequencia
			and	a.nr_sequencia		= c.nr_sequencia
			and	c.nr_seq_item_tiss	= nr_seq_item_tiss_p
			and	b.cd_guia		= cd_guia_w
			and	b.nr_seq_segurado	= nr_seq_segurado_w
			union	all
			select  substr(obter_descricao_padrao('PLS_MATERIAL','DS_MATERIAL', a.nr_seq_material),1,255) ds_item
			from 	pls_conta_mat a,
				pls_conta b,
				pls_conta_mat_regra c
			where   a.nr_seq_conta		= b.nr_sequencia
			and	a.nr_sequencia		= c.nr_sequencia
			and	c.nr_seq_item_tiss	= nr_seq_item_tiss_p
			and	b.cd_guia		= cd_guia_w
			and	b.nr_seq_segurado	= nr_seq_segurado_w );
end if;

return	ds_retorno_w;

end pls_obter_desc_item_vinc;
/
