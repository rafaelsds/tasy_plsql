CREATE OR REPLACE PROCEDURE oft_release_pendencies (
    nr_seq_consulta_p    NUMBER,
    ie_gerar_receita_p   VARCHAR2
) IS

    nm_usuario_w             usuario.nm_usuario%TYPE;
    nr_seq_consulta_form_w   oft_consulta_formulario.nr_sequencia%TYPE;
    nr_seq_refracao_w        oft_refracao.nr_sequencia%TYPE;
	nr_seq_oft_oculos_w		 oft_oculos.nr_sequencia%TYPE;
	
    CURSOR c_pendencies IS
    SELECT
        a.nr_seq_diag_doenca,
        a.cd_evolucao,
        a.nr_seq_hist_saude_transfusao,
        a.nr_seq_hist_saude_vicio,
        a.nr_seq_hist_saude_medic_uso,
        a.nr_seq_atestado,
        a.nr_seq_avaliacao,
        a.nr_seq_hist_saude_cirurgia,
        a.nr_seq_hist_saude_internacao,
        a.nr_seq_hist_saude_alergia,
        a.nr_seq_hist_saude_clinico,
        a.nr_seq_anamnese,
        a.nr_seq_hist_saude_acessorio,
        a.nr_seq_hist_saude_deficiencia,
        a.nr_seq_receita,
        a.ie_tipo_registro,
        a.nr_seq_registro,
        a.nr_seq_hist_saude_atend_medic
    FROM
        pep_item_pendente      a,
        atendimento_paciente   b,
        oft_consulta           c
    WHERE
        a.nr_atendimento = b.nr_atendimento
        AND b.nr_atendimento = c.nr_atendimento
        AND a.nm_usuario = nm_usuario_w
        AND c.nr_sequencia = nr_seq_consulta_p
        AND b.dt_cancelamento IS NULL
        AND a.ie_gerado_funcao_pepo = 'O'
        AND a.ie_tipo_pendencia = 'L'
        AND a.dt_registro BETWEEN c.dt_consulta AND nvl(c.dt_fim_consulta, sysdate)
    UNION
    SELECT
        a.nr_seq_diag_doenca,
        a.cd_evolucao,
        a.nr_seq_hist_saude_transfusao,
        a.nr_seq_hist_saude_vicio,
        a.nr_seq_hist_saude_medic_uso,
        a.nr_seq_atestado,
        a.nr_seq_avaliacao,
        a.nr_seq_hist_saude_cirurgia,
        a.nr_seq_hist_saude_internacao,
        a.nr_seq_hist_saude_alergia,
        a.nr_seq_hist_saude_clinico,
        a.nr_seq_anamnese,
        a.nr_seq_hist_saude_acessorio,
        a.nr_seq_hist_saude_deficiencia,
        a.nr_seq_receita,
        a.ie_tipo_registro,
        a.nr_seq_registro,
        a.nr_seq_hist_saude_atend_medic
    FROM
        pep_item_pendente   a,
        oft_consulta        c
    WHERE
        a.cd_pessoa_fisica = c.cd_pessoa_fisica
        AND a.nm_usuario = nm_usuario_w
        AND c.nr_sequencia = nr_seq_consulta_p
        AND a.nr_atendimento IS NULL
        AND a.ie_gerado_funcao_pepo = 'O'
        AND a.ie_tipo_pendencia = 'L'
        AND a.dt_registro BETWEEN c.dt_consulta AND nvl(c.dt_fim_consulta, sysdate);

BEGIN 

	nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
	nr_seq_consulta_form_w := nvl(oft_obter_form_consulta(nr_seq_consulta_p), 0);
	
    IF ( ie_gerar_receita_p = 'S' ) THEN
		
        SELECT 	MAX(nr_sequencia)
        INTO 	nr_seq_refracao_w
        FROM	oft_refracao
        WHERE 	nr_seq_consulta_form = nr_seq_consulta_form_w
		AND 	( dt_liberacao IS NOT NULL
					OR nm_usuario = nm_usuario_w )
		AND 	dt_inativacao IS NULL
		AND 	( ( ie_receita_dinamica = 'S' )
					OR ( ie_receita_estatica = 'S' ) );

        IF ( nr_seq_refracao_w IS NOT NULL ) THEN
            oft_gerar_receita_oculos(nr_seq_consulta_p, nm_usuario_w, nr_seq_refracao_w, 'S', nr_seq_consulta_form_w, nr_seq_oft_oculos_w);
        END IF;

    END IF;
	
	IF ( nr_seq_consulta_form_w > 0 ) THEN
		liberar_informacao('OFT_CONSULTA_FORMULARIO','NR_SEQUENCIA',nr_seq_consulta_form_w,nm_usuario_w);
	END IF;
	
    FOR r_pendencie IN c_pendencies LOOP BEGIN
        BEGIN
            IF ( r_pendencie.nr_seq_hist_saude_atend_medic IS NOT NULL ) THEN
                liberar_informacao('PACIENTE_ANTEC_CLINICO', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_atend_medic, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_diag_doenca IS NOT NULL ) THEN
                liberar_informacao('DIAGNOSTICO_DOENCA', 'NR_SEQ_INTERNO', r_pendencie.nr_seq_diag_doenca, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_clinico IS NOT NULL ) THEN
                liberar_informacao('PACIENTE_ANTEC_CLINICO', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_clinico, nm_usuario_w);
            END IF;

            IF ( r_pendencie.cd_evolucao IS NOT NULL ) THEN
                liberar_informacao('EVOLUCAO_PACIENTE', 'CD_EVOLUCAO', r_pendencie.cd_evolucao, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_internacao IS NOT NULL ) THEN
                liberar_informacao('HISTORICO_SAUDE_INTERNACAO', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_internacao, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_alergia IS NOT NULL ) THEN
                liberar_informacao('PACIENTE_ALERGIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_alergia, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_atestado IS NOT NULL ) THEN
                liberar_informacao('ATESTADO_PACIENTE', 'NR_SEQUENCIA', r_pendencie.nr_seq_atestado, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_acessorio IS NOT NULL ) THEN
                liberar_informacao('PACIENTE_ACESSORIO', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_acessorio, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_medic_uso IS NOT NULL ) THEN
                liberar_informacao('PACIENTE_MEDIC_USO', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_medic_uso, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_cirurgia IS NOT NULL ) THEN
                liberar_informacao('HISTORICO_SAUDE_CIRURGIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_cirurgia, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_transfusao IS NOT NULL ) THEN
                liberar_informacao('PACIENTE_TRANSFUSAO', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_transfusao, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_anamnese IS NOT NULL ) THEN
                liberar_informacao('ANAMNESE_PACIENTE', 'NR_SEQUENCIA', r_pendencie.nr_seq_anamnese, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_avaliacao IS NOT NULL ) THEN
                liberar_informacao('MED_AVALIACAO_PACIENTE', 'NR_SEQUENCIA', r_pendencie.nr_seq_avaliacao, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_clinico IS NOT NULL ) THEN
                liberar_informacao('PACIENTE_ANTEC_CLINICO', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_clinico, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_vicio IS NOT NULL ) THEN
                liberar_informacao('PACIENTE_HABITO_VICIO', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_vicio, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_hist_saude_deficiencia IS NOT NULL ) THEN
                liberar_informacao('PF_TIPO_DEFICIENCIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_hist_saude_deficiencia, nm_usuario_w);
            END IF;

            IF ( r_pendencie.nr_seq_receita IS NOT NULL ) THEN
                liberar_informacao('MED_RECEITA', 'NR_SEQUENCIA', r_pendencie.nr_seq_receita, nm_usuario_w);
            END IF;

            CASE r_pendencie.ie_tipo_registro
                WHEN 'ONL' THEN
                    liberar_informacao('OFT_ANAMNESE', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'OAL' THEN
                    liberar_informacao('OFT_ANEXO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'CNL' THEN
                    liberar_informacao('PEP_PAC_CI', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'ODL' THEN
                    liberar_informacao('OFT_DIAGNOSTICO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'ODF' THEN
                    liberar_informacao('OFT_DIAGNOSTICO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'OGL' THEN
                    liberar_informacao('PEP_ORIENTACAO_GERAL', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'PEE' THEN
                    liberar_informacao('PEDIDO_EXAME_EXTERNO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'OCL' THEN
                    liberar_informacao('OFT_CONDUTA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'BIOL' THEN
                    liberar_informacao('OFT_BIOMETRIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'CAPL' THEN
                    liberar_informacao('OFT_CAMPIMETRIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'OIL' THEN
                    liberar_informacao('OFT_IMAGEM_EXAME', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'CTL' THEN
                    liberar_informacao('OFT_CURVA_TENCIONAL', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'DNPL' THEN
                    liberar_informacao('OFT_DNP', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'REFL' THEN
                    liberar_informacao('OFT_REFRACAO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'EOEL' THEN
                    liberar_informacao('OFT_EXAME_EXTERNO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'FNCL' THEN
                    liberar_informacao('OFT_FUNDOSCOPIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'GNOL' THEN
                    liberar_informacao('OFT_GONIOSCOPIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'MIEL' THEN
                    liberar_informacao('OFT_MICROSCOPIA_ESPECULAR', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'MOOL' THEN
                    liberar_informacao('OFT_MOTILIDADE_OCULAR', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'PAQL' THEN
                    liberar_informacao('OFT_PAQUIMETRIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'PQUL' THEN
                    liberar_informacao('OFT_PAQUIMETRIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'PAVL' THEN
                    liberar_informacao('OFT_POTENCIAL_ACUIDADE', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'PUPL' THEN
                    liberar_informacao('OFT_PUPILOMETRIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'TOCL' THEN
                    liberar_informacao('OFT_OCT', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'TOGL' THEN
                    liberar_informacao('OFT_TOMOGRAFIA_OLHO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'TAPL' THEN
                    liberar_informacao('OFT_TONOMETRIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'TPNL' THEN
                    liberar_informacao('OFT_TONOMETRIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'ULTL' THEN
                    liberar_informacao('OFT_ULTRASSONOGRAFIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'ARTL' THEN
                    liberar_informacao('OFT_ANGIO_RETINO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'AGRL' THEN
                    liberar_informacao('OFT_ANGIO_RETINO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'BICL' THEN
                    liberar_informacao('OFT_BIOMICROSCOPIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'FOTL' THEN
                    liberar_informacao('OFT_FOTOCOAGULACAO_LASER', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'CASL' THEN
                    liberar_informacao('OFT_CAPSULOTOMIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'DATL' THEN
                    liberar_informacao('OFT_DALTONISMO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'AVAL' THEN
                    liberar_informacao('OFT_OLHO_SECO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'IRIL' THEN
                    liberar_informacao('OFT_IRIDECTOMIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'AURL' THEN
                    liberar_informacao('OFT_AUTO_REFRACAO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'AVNL' THEN
                    liberar_informacao('OFT_CORRECAO_ATUAL', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'SOHL' THEN
                    liberar_informacao('OFT_SOBRECARGA_HIDRICA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'MARL' THEN
                    liberar_informacao('OFT_MAPEAMENTO_RETINA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'CERL' THEN
                    liberar_informacao('OFT_CERASTOCOPIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'REOL' THEN
                    liberar_informacao('OFT_OCULOS', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'HRP' THEN
                    liberar_informacao('PACIENTE_REP_PRESCRICAO', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                WHEN 'ABRO' THEN
                    liberar_informacao('OFT_ABERROMETRIA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
				WHEN 'JS' THEN
                    liberar_informacao('PACIENTE_JUSTIFICATIVA', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
				WHEN 'CLEN' THEN
                    liberar_informacao('OFT_CONSULTA_LENTE', 'NR_SEQUENCIA', r_pendencie.nr_seq_registro, nm_usuario_w);
                ELSE
                    NULL;
            END CASE;
			
        EXCEPTION
            WHEN OTHERS THEN
                NULL;
        END;
    END;
    END LOOP;
END oft_release_pendencies;
/
