create or replace
function esus_obter_exames_paciente(	cd_profissional_p	varchar2,
				cd_cbo_p		varchar2,
				nr_atendimento_p	number,
				dt_atendimento_p	date)
				return varchar2 is

ds_retorno_w			varchar2(2000) := ',';
ie_campo_exame_w		esus_regra_exame_pac_cam.ie_campo_exame%type;
nr_sequencia_w			esus_regra_exame_pac_cam.nr_sequencia%type;
cd_procedimento_w		esus_regra_exame_pac_pro.cd_procedimento%type;
ie_origem_proced_w		esus_regra_exame_pac_pro.ie_origem_proced%type;
qt_proc_exame_w			number(10) := 0;

cursor c01 is
	select	ie_campo_exame,
		nr_sequencia
	from	esus_regra_exame_pac_cam
	where	ie_situacao = 'A'
	order by ie_campo_exame;
	
cursor c02 is
	select	cd_procedimento,
		ie_origem_proced
	from	esus_regra_exame_pac_pro
	where	nr_seq_regra_exame = nr_sequencia_w
	order by cd_procedimento;
					
begin

open C01;
loop
fetch C01 into	
	ie_campo_exame_w,
	nr_sequencia_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		cd_procedimento_w,
		ie_origem_proced_w;
	exit when C02%notfound;
		begin
		
		select	count(1)
		into	qt_proc_exame_w
		from	procedimento_paciente
		where	nr_atendimento = nr_atendimento_p
		and	trunc(dt_procedimento) = dt_atendimento_p
		and	cd_medico_executor = cd_profissional_p
		and	cd_cbo = cd_cbo_p
		and	cd_procedimento = cd_procedimento_w
		and	ie_origem_proced = ie_origem_proced_w
		and	cd_motivo_exc_conta is null
		and	rownum = 1;
		
		if	(qt_proc_exame_w > 0) then
			ds_retorno_w := ds_retorno_w || ie_campo_exame_w || ',';
		end if;
		
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

return	ds_retorno_w;

end esus_obter_exames_paciente;
/
