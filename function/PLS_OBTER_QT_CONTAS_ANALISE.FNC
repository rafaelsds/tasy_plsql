create or replace
function pls_obter_qt_contas_analise
			(	nr_seq_analise_p		number,
				ie_tipo_guia_p			varchar2)
				return number is

nr_retorno_w			number(10);
qt_discussao_w			number(10);
qt_pos_w			number(10);
				
begin

select	count(*)
into	qt_discussao_w
from	pls_contestacao_discussao a
where	a.nr_seq_analise	= nr_seq_analise_p;

select	count(1)
into	qt_pos_w
from	pls_conta_pos_estabelecido a
where	a.nr_seq_analise	= nr_seq_analise_p
and	((a.ie_situacao	= 'A') or (a.ie_situacao	is null));

if	(qt_discussao_w > 0) then
	select	count(count(nr_seq_conta))
	into	nr_retorno_w
	from	w_pls_discussao_item
	where	nr_seq_analise = nr_seq_analise_p
	and	ie_tipo_guia = ie_tipo_guia_p
	group by nr_seq_conta;
elsif	(qt_pos_w > 0) then
	select	count(distinct b.nr_sequencia)
	into	nr_retorno_w
	from	pls_conta b,
		pls_conta_pos_estabelecido a
	where	a.nr_seq_conta	= b.nr_sequencia
	and	a.nr_seq_analise = nr_seq_analise_p
	and	b.ie_tipo_guia = ie_tipo_guia_p
	and	((a.ie_situacao	= 'A') or (a.ie_situacao	is null));
else
	select	count(count(nr_sequencia))
	into	nr_retorno_w
	from	pls_conta 
	where	nr_seq_analise = nr_seq_analise_p
	and	ie_tipo_guia = ie_tipo_guia_p
	group by nr_sequencia;
end if;

return	nr_retorno_w;

end pls_obter_qt_contas_analise;
/