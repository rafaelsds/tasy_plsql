create or replace function OBTER_AJUSTE_COMPLEXIDADE(nr_seq_complexidade_p	number,
							dt_parametro_p		date)
				return number is

tx_ajuste_w		proced_proc_complex.tx_ajuste%type;

begin

	tx_ajuste_w := 0;
	
	if (nr_seq_complexidade_p is not null) then
		
		select	max(tx_ajuste)
		into	tx_ajuste_w
		from	proced_proc_complex
		where	nr_sequencia = nr_seq_complexidade_p
		and	dt_parametro_p between dt_inicio_vigencia and nvl(dt_fim_vigencia,dt_parametro_p)
		and	cd_estabelecimento = obter_estabelecimento_ativo;
		
	end if;
	
	return tx_ajuste_w;
end OBTER_AJUSTE_COMPLEXIDADE;
/
