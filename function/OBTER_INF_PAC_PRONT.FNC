create or replace
function obter_inf_pac_pront	(CD_PESSOA_FISICA_P	number,

			IE_OPCAO_P	varchar2)
								
			return date is
								
ds_retorno_w	date;
qt_retorno_w 	varchar(255);

/*

DE - Data de Entrada 1� atendimento do paciente

DAU - Data de Alta do �ltimo atendimento, ou sysdate se o atendimento estiver aberto

*/							

begin

if (cd_pessoa_fisica_p is not null) then
		
	if (ie_opcao_p = 'DE') then
	
		SELECT	min(a.dt_entrada)
		into 	ds_retorno_w
		FROM 	atendimento_paciente a
		where 	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
		
	elsif (ie_opcao_p = 'DAU') then
	
		select 	count(*) QT_REG
		into	qt_retorno_w
		from 	atendimento_paciente a 
		where 	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	a.dt_alta is null;
		
		if (qt_retorno_w > 0) then
		
			ds_retorno_w := sysdate;
			
		else
		
		select 	max(a.dt_alta)
		into	ds_retorno_w
		from 	atendimento_paciente a
		where 	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
		
		end if;
		
	end if;	
	
end if;

return	ds_retorno_w;

end obter_inf_pac_pront;
/