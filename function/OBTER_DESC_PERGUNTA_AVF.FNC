create or replace
function obter_desc_pergunta_avf(	nr_sequencia_p		number)
 		    	return varchar2 is

ds_pergunta_w		varchar2(255);			

begin

select	ds_pergunta
into	ds_pergunta_w
from	avf_pergunta
where	nr_sequencia = nr_sequencia_p;

return	ds_pergunta_w;

end obter_desc_pergunta_avf;
/