CREATE OR REPLACE FUNCTION get_outpatient_consultation (
    nr_atendimento_p atendimento_paciente.nr_atendimento%TYPE
) RETURN VARCHAR2 IS
    nr_seq_agenda_w   atendimento_paciente_aux.nr_seq_agenda%TYPE;
    ie_response_w     VARCHAR2(300) := NULL;
BEGIN
    IF ( nr_atendimento_p IS NOT NULL ) THEN
        SELECT
            MAX(nr_seq_agenda)
        INTO nr_seq_agenda_w
        FROM
            atendimento_paciente_aux
        WHERE
            nr_atendimento = nr_atendimento_p;

        IF ( nvl(nr_seq_agenda_w, 0) > 0 ) THEN
            SELECT
                obter_descricao_agenda(cd_agenda, 3, NULL, wheb_usuario_pck.get_cd_estabelecimento)
                || ' '
                || to_char(dt_agenda, 'dd/mm/yyyy hh24:mi')
            INTO ie_response_w
            FROM
                agenda_consulta
            WHERE
                nr_sequencia = nr_seq_agenda_w;

        END IF;

    END IF;

    RETURN ie_response_w;
END;
/