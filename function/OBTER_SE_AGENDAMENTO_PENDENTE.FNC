create or replace
function obter_se_agendamento_pendente(cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ie_agendamento_pendente	varchar2(1);	
nr_agendamento_pendente	number(10);		
begin

select	count(*)
into	nr_agendamento_pendente
from	agenda_consulta c,
	agenda a
where	c.cd_pessoa_fisica = cd_pessoa_fisica_p
and	a.cd_tipo_agenda = 3
and	a.cd_agenda        = c.cd_agenda;
	 
if	(nr_agendamento_pendente > 0) then
	ie_agendamento_pendente := 'S';
else
	ie_agendamento_pendente := 'N';
end if;

return	ie_agendamento_pendente;

end obter_se_agendamento_pendente;
/