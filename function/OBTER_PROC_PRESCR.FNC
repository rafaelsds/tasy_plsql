create or replace
function obter_proc_prescr	(nr_prescricao_p	number,
				nr_seq_proced_p	number)
				return number is

cd_procedimento_w	number(15,0);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_proced_p is not null) then
	select	max(cd_procedimento)
	into	cd_procedimento_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_seq_proced_p;
end if;

return cd_procedimento_w;

end obter_proc_prescr;
/