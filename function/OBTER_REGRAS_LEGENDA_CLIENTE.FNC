create or replace function obter_regras_legenda_cliente( nr_seq_cor_p tasy_regra_legenda_cliente.nr_seq_cor%type,
														nr_seq_imagem_p	tasy_regra_legenda_cliente.nr_seq_imagem%type,
													   cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
													   cd_perfil_p	perfil.cd_perfil%type)
 		    	return number is

nr_seq_retorno_w	tasy_regra_legenda_cliente.nr_sequencia%type;
count_w	number(5)	:= 0;

cursor c01 is
	select	max(nr_sequencia) nr_sequencia
	from		tasy_regra_legenda_cliente
	where	(nr_seq_cor = nr_seq_cor_p or nr_seq_imagem = nr_seq_imagem_p)
	and		cd_perfil = cd_perfil_p
	and		cd_estabelecimento	= cd_estabelecimento_p
	and		nm_usuario_regra is null;

c01_w		c01%rowtype;

cursor c02 is
	select	max(nr_sequencia) nr_sequencia
	from		tasy_regra_legenda_cliente
	where	(nr_seq_cor = nr_seq_cor_p or nr_seq_imagem = nr_seq_imagem_p)
	and		cd_perfil = cd_perfil_p
	and		cd_estabelecimento	is null
	and		nm_usuario_regra is null;

c02_w		c02%rowtype;

cursor c03 is
	select	max(nr_sequencia) nr_sequencia
	from		tasy_regra_legenda_cliente
	where	(nr_seq_cor = nr_seq_cor_p or nr_seq_imagem = nr_seq_imagem_p)
	and		cd_estabelecimento	= cd_estabelecimento_p
	and		cd_perfil is null
	and		nm_usuario_regra is null;

c03_w		c03%rowtype;

cursor c04 is
	select	max(nr_sequencia) nr_sequencia
	from		tasy_regra_legenda_cliente
	where	(nr_seq_cor = nr_seq_cor_p or nr_seq_imagem = nr_seq_imagem_p)
	and		cd_estabelecimento	is null
	and		cd_perfil is null
	and		nm_usuario_regra is null;

c04_w		c04%rowtype;

begin

select	count(*)
into		count_w
from		tasy_regra_legenda_cliente
where	(nr_seq_cor = nr_seq_cor_p or nr_seq_imagem = nr_seq_imagem_p);

if	(count_w > 0) then
	open c01;
	loop
	fetch c01 into
		c01_w;
	exit when c01%notfound;
		begin
			nr_seq_retorno_w	:= nvl(c01_w.nr_sequencia, 0);
		end;
	end loop;
	close c01;
	if	(nr_seq_retorno_w = 0) then
		open c02;
		loop
		fetch c02 into
			c02_w;
		exit when c02%notfound;
		begin
			nr_seq_retorno_w	:= nvl(c02_w.nr_sequencia, 0);
		end;
		end loop;
		close c02;
		if	(nr_seq_retorno_w = 0) then
			open c03;
			loop
			fetch c03 into
				c03_w;
			exit when c03%notfound;
				begin
					nr_seq_retorno_w	:= nvl(c03_w.nr_sequencia, 0);
				end;
			end loop;
			close c03;
			if	(nr_seq_retorno_w = 0) then
				open c04;
				loop
				fetch c04 into
					c04_w;
				exit when c04%notfound;
					begin
						nr_seq_retorno_w	:= nvl(c04_w.nr_sequencia, 0);
					end;
				end loop;
				close c04;
			end if;
		end if;
	end if;
end if;

return	nr_seq_retorno_w;

end obter_regras_legenda_cliente;
/