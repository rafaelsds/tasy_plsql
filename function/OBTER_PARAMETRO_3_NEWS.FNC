create or replace
function obter_parametro_3_news (nr_sequencia_p number,
								 ie_tipo_p      varchar2 default 'A',
								 vl_param_p		number default 0) return varchar2 is

ie_parametro3_w  varchar2(1) := 'N';
qt_reg_w		 number(10);
nr_atendimento_w number(15);
								  
begin

if (ie_tipo_p = 'FR') and (vl_param_p <= 8 or vl_param_p >= 25) then
	ie_parametro3_w := 'S';
elsif (ie_tipo_p = 'SO2') and (vl_param_p <= 91) then
	ie_parametro3_w := 'S';
elsif (ie_tipo_p = 'T') and (vl_param_p <= 35) then
	ie_parametro3_w := 'S';
elsif (ie_tipo_p = 'PA') and (vl_param_p <= 90 or vl_param_p >= 220 ) then
	ie_parametro3_w := 'S';
elsif (ie_tipo_p = 'FC') and (vl_param_p <= 40 or vl_param_p >= 131) then
	ie_parametro3_w := 'S';
elsif (ie_tipo_p = 'N') and (vl_param_p in ('1','2','3')) then
	ie_parametro3_w := 'S';
elsif (nr_sequencia_p > 0) then
	select	nvl(max('S'),'N')
	into	ie_parametro3_w
	from 	escala_news
	where	nr_sequencia = nr_sequencia_p
	and     (qt_freq_resp <= 8 or qt_freq_resp >= 25
	or 		qt_saturacao_02 <= 91
	or 		qt_temp <= 35
	or 		qt_pa_sistolica <= 90 or qt_pa_sistolica >= 220
	or 		qt_freq_cardiaca <= 40 or qt_freq_cardiaca >= 131
	or		ie_nivel_consciencia in ('1','2','3'));
end if;

return ie_parametro3_w;

end;
/
