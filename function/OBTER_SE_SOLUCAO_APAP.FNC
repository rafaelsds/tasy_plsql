create or replace
function obter_se_solucao_apap(	nr_prescricao_p		number,
				nr_seq_solucao_p	number,
				nr_ficha_tecnica_p	number)
				return varchar2 is

ds_retorno_w		varchar2(1)	:= 'N';
qt_existe_w		number(10);

begin

select	count(*)
into	qt_existe_w
from	material c,
	prescr_material b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	a.nr_prescricao		= nr_prescricao_p
and	b.nr_sequencia_solucao	= nr_seq_solucao_p
and	b.cd_material		= c.cd_material
and	c.nr_seq_ficha_tecnica	= nr_ficha_tecnica_p;

if	(qt_existe_w > 0) then
	begin
	ds_retorno_w	:= 'S';
	end;
end if;

return	ds_retorno_w;

end obter_se_solucao_apap;
/