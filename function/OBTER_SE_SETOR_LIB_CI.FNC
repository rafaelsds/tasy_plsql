create or replace
function obter_se_setor_lib_ci(	nr_sequencia_p	number,
								cd_setor_p		number)
 		    	return varchar2 is

ie_liberado_w	char(1) := 'S';
			
begin

select	nvl(max('N'),'S')
into	ie_liberado_w
from	rep_regra_envio_ci_setor
where	nr_seq_regra	= nr_sequencia_p;

if	(ie_liberado_w = 'N') then
	select	nvl(max('S'), 'N')
	into	ie_liberado_w
	from	rep_regra_envio_ci_setor
	where	rownum = 1
	and		nr_seq_regra		= nr_sequencia_p
	and		cd_setor_atendimento	= cd_setor_p;
end if;

return ie_liberado_w;

end obter_se_setor_lib_ci;
/
