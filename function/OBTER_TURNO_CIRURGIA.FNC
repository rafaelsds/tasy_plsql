create or replace
function Obter_Turno_Cirurgia ( dt_cirurgia_p		date,
				cd_estabelecimento_p	number,
				ie_opcao_p		varchar2) 
				return			varchar2 is

dt_cirurgia_w		date;
nr_seq_turno_w		number(10,0);
ds_turno_w		varchar2(15);
dt_inicio_w		date;
dt_final_w		date;
ds_retorno_w		varchar2(15);

cursor c01 is
	select	nr_sequencia,
		ds_turno,
		to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(dt_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
		to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(dt_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
	from	turno_cirurgia
	where	cd_estabelecimento	= cd_estabelecimento_p;

begin

dt_cirurgia_w	:= to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(dt_cirurgia_p, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

open c01;
	loop
	fetch c01 into
		nr_seq_turno_w,
		ds_turno_w,
		dt_inicio_w,
		dt_final_w;	
	exit when c01%notfound;

		if	((dt_cirurgia_w >= dt_inicio_w) and (dt_cirurgia_w <= dt_final_w)) or
			((dt_final_w < dt_inicio_w) and ((dt_cirurgia_w >= dt_inicio_w) or (dt_cirurgia_w <= dt_final_w))) then

			if	(ie_opcao_p = 'C') then
				ds_retorno_w	:= nr_seq_turno_w;
			elsif	(ie_opcao_p = 'D') then
				ds_retorno_w	:= ds_turno_w;
			end if;
		end if;
	
	end loop;
close c01;

return	ds_retorno_w;

end Obter_Turno_Cirurgia;
/
					