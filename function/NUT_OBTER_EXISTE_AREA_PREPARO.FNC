create or replace
function nut_obter_existe_area_preparo(	nr_seq_area_p		number,
					dt_opcao_p		date,
					nr_seq_local_p		number,
					nr_seq_preparo_p	number,
					nr_seq_servico_p	number,
					cd_setor_atendimento_p	number
					)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);			
qtd_w		number(5);
nr_seq_local_w	number(10);
begin

/*SELECT  decode(COUNT(*),0,'N','S')
into	ie_retorno_w
FROM    Nut_Resumo_Dia_v a
WHERE   TRUNC(dt_opcao)    	= dt_opcao_p
AND     nr_seq_area 		= nr_seq_area_p
AND     ((nr_seq_local_p = 0)     	OR (a.nr_seq_local = nr_seq_local_p))
AND     ((nr_seq_servico_p = 0)   	OR (a.nr_seq_servico = nr_seq_servico_p))
AND     ((cd_setor_atendimento_p = 0)   OR (a.cd_setor_atendimento = cd_setor_atendimento_p))
AND     ((nr_seq_preparo_p = 0) 	OR (nut_existe_receita_preparo(a.nr_seq_receita, nr_seq_preparo_p) = 'S'));*/

nr_seq_local_w :=  nut_obter_local_paciente;

--Paciente
SELECT 	COUNT(*)
into	qtd_w
FROM   	nut_area_prod_rec d,
	nut_receita c,
	nut_pac_opcao_rec b,
	nut_atend_serv_dia a
WHERE	b.nr_seq_receita	   = c.nr_sequencia
AND	c.nr_sequencia		   = d.nr_seq_receita
AND	a.nr_sequencia		   = b.nr_seq_servico_dia
AND	TRUNC(a.dt_servico)    	   = trunc(dt_opcao_p)
AND     nr_seq_area 		   = nr_seq_area_p
AND     ((nr_seq_local_p = 0)     	OR (nr_seq_local_w = nr_seq_local_p))
AND     ((nr_seq_servico_p = 0)   	OR (a.nr_seq_servico = nr_seq_servico_p))
AND     ((cd_setor_atendimento_p = 0)   OR (a.cd_setor_atendimento = cd_setor_atendimento_p))
AND     ((nr_seq_preparo_p = 0) 	OR (nut_existe_receita_preparo(b.nr_seq_receita, nr_seq_preparo_p) = 'S'))
AND	b.dt_liberacao IS NOT NULL;

--Restaurante
if (qtd_w = 0) then

	SELECT 	COUNT(*)
	into	qtd_w
	FROM	nut_area_prod_rec d,
		nut_receita c,
		nut_pac_opcao_rec b,
		nut_cardapio_dia a
	WHERE 	b.nr_seq_receita	   = c.nr_sequencia
	AND	c.nr_sequencia	   	   = d.nr_seq_receita
	AND	a.nr_sequencia		   = b.nr_seq_cardapio_dia
	AND	TRUNC(a.dt_cardapio)   	   = trunc(dt_opcao_p)
	AND     nr_seq_area 		   = nr_seq_area_p
	AND     ((nr_seq_local_p = 0)     	OR (a.nr_seq_local = nr_seq_local_p))
	AND     ((nr_seq_servico_p = 0)   	OR (a.nr_seq_servico = nr_seq_servico_p))
	AND     ((nr_seq_preparo_p = 0) 	OR (nut_existe_receita_preparo(b.nr_seq_receita, nr_seq_preparo_p) = 'S'))
	AND	b.dt_liberacao IS NOT NULL;

end if;


if (qtd_w > 0) then
	ie_retorno_w := 'S';
else	
	ie_retorno_w := 'N';
end if;

return	ie_retorno_w;

end nut_obter_existe_area_preparo;
/