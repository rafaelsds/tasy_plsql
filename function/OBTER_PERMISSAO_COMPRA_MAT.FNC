create or replace
function obter_permissao_compra_mat(		cd_estabelecimento_p		number,
					cd_material_p			number,
					ie_opcao_p			varchar2)
 		    	return varchar2 is

/*ie_opcao_p
S - Solicita��o de compras
C - Cota��o de compras
O - Ordem de compras*/			
	

ds_retorno_w			varchar2(15);
cd_grupo_material_w		grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w		subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w		classe_material.cd_classe_material%type;
ie_permite_solic_w			permissao_compras_material.ie_permite_solic%type;
ie_permite_cotacao_w		permissao_compras_material.ie_permite_cotacao%type;
ie_permite_ordem_w		permissao_compras_material.ie_permite_ordem%type;

cursor c01 is
select	nvl(ie_permite_solic,'S'),
	nvl(ie_permite_cotacao,'S'),
	nvl(ie_permite_ordem,'S')
from	permissao_compras_material
where	cd_estabelecimento = cd_estabelecimento_p
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	(nvl(cd_material, cd_material_p) 			= cd_material_p or cd_material_p = 0)
order by
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0);

	
begin

select	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v a
where	a.cd_material = cd_material_p;

open C01;
loop
fetch C01 into	
	ie_permite_solic_w,
	ie_permite_cotacao_w,
	ie_permite_ordem_w;
exit when C01%notfound;
	begin
	ie_permite_solic_w			:= ie_permite_solic_w;
	ie_permite_cotacao_w		:= ie_permite_cotacao_w;
	ie_permite_ordem_w		:= ie_permite_ordem_w;
	end;
end loop;
close C01;

if	(ie_opcao_p = 'S') then
	ds_retorno_w	:= ie_permite_solic_w;
elsif	(ie_opcao_p = 'C') then
	ds_retorno_w	:= ie_permite_cotacao_w;
elsif	(ie_opcao_p = 'O') then
	ds_retorno_w	:= ie_permite_ordem_w;
end if;

return	ds_retorno_w;

end obter_permissao_compra_mat;
/