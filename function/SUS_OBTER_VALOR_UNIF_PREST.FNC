create or replace
function sus_obter_valor_unif_prest(	nr_interno_conta_p in number ,
					cd_cgc_prestador_p in varchar2 )
					return number as

vl_retorno_w		number(15,2);
ie_arred_sp_sisaih_w	varchar2(1);

begin

select	nvl(max(ie_arred_sp_sisaih),'N')
into	ie_arred_sp_sisaih_w
from	conta_paciente a,
	sus_parametros_aih b
where	a.nr_interno_conta	= nr_interno_conta_p
and	b.cd_estabelecimento	= a.cd_estabelecimento;

if	(ie_arred_sp_sisaih_w		= 'N') then
	select	/*+ INDEX(A PROPACI_I11) INDEX(B SUSVAPR_PK) */
		sum(nvl(vl_sadt,0)) + 
		sum(nvl(vl_ato_anestesista,0)) + 
		sum(nvl(b.vl_medico,0)) + 
		sum(nvl(vl_matmed,0))
	into	vl_retorno_w
	from	sus_valor_proc_paciente b,
		procedimento_paciente a
	where	a.nr_sequencia		= b.nr_sequencia
	and	a.cd_motivo_exc_conta	is null
	and	a.nr_interno_conta	= nr_interno_conta_p
	and	a.cd_cgc_prestador	= cd_cgc_prestador_p
	and	a.ie_origem_proced	= 7;

else
	select	/*+ INDEX(A PROPACI_I11) INDEX(B SUSVAPR_PK) */
		sum(nvl(vl_sadt,0)) +
		sum(nvl(a.vl_medico,0)) +
		sum(nvl(vl_matmed,0)) +
		sum(nvl(Obter_Valor_Participante(a.nr_sequencia),0))
	into	vl_retorno_w
	from	sus_valor_proc_paciente b,
		procedimento_paciente a
	where	a.nr_sequencia		= b.nr_sequencia
	and	a.cd_motivo_exc_conta	is null
	and	a.nr_interno_conta	= nr_interno_conta_p
	and	a.cd_cgc_prestador	= cd_cgc_prestador_p
	and	a.ie_origem_proced	= 7;
end if;

return nvl(vl_retorno_w,0);

end sus_obter_valor_unif_prest;
/
