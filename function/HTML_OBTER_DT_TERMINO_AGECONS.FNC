create or replace
function html_obter_dt_termino_agecons(	nr_seq_agenda_p		number,
					ie_diario_p		varchar2,
					ie_final_semana_p	varchar2,
					qt_dias_p		number,
					ds_dias_p		varchar2)
					return date is

qt_dia_w		number(2);
dt_agenda_w	date;
dt_atual_w	date;
cd_dia_semana_w	varchar2(1);
ds_retorno_w	date;
qt_dias_w	number(10,0) := 0;
ds_dias_w	varchar2(255);
ie_gerar_dia_w	varchar2(1)  := 'S';

-- criada function para o html5 como c�pia da funciton obter_dt_termino_per_agecons com o retorno de date

begin

if	(nr_seq_agenda_p is not null) then
	begin
	/* obter dados agenda */
	select	max(dt_agenda)
	into	dt_agenda_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_p;
	
	/* Gerar hor�rios por semana ou diariamente */
	if	(ie_diario_p = 'S') then
		qt_dia_w	:= 1;
	elsif	(ie_diario_p = 'N') then
		qt_dia_w	:= 7;
	end if;

	dt_atual_w	:= dt_agenda_w + 1;
	
	if	(ds_dias_p is not null) then
		ds_dias_w := replace(ds_dias_p, ' ', '');
	end if;
	
	
	if	(qt_dias_p > 0) then
		while	(qt_dias_w < qt_dias_p) loop
			begin

			select	substr(Obter_Cod_Dia_Semana(dt_atual_w),1,1)
			into	cd_dia_semana_w
			from	dual;	

			if	(ie_final_semana_p = 'N') and
				(((cd_dia_semana_w = 1)) or ((cd_dia_semana_w = 7)))then
					
				if	(cd_dia_semana_w = 1) then
					dt_atual_w	:= dt_atual_w + 1;
				elsif	(cd_dia_semana_w = 7) then
					dt_atual_w	:= dt_atual_w + 2;
				end if;
				
				
				select	substr(Obter_Cod_Dia_Semana(dt_atual_w),1,1)
				into	cd_dia_semana_w
				from	dual;	
			end if;
					
			if	(ds_dias_w is not null) then
				ie_gerar_dia_w	:=  obter_se_contido(cd_dia_semana_w,ds_dias_w);
				
				if	(ie_gerar_dia_w	= 'S') then
					qt_dias_w	:= qt_dias_w + 1;
				end if;
			else				
				qt_dias_w	:= qt_dias_w + 1;				
			end if;
			 
			dt_atual_w	:= dt_atual_w + 1;		
			end;
		end loop;
	end if;

	ds_retorno_w	:= dt_atual_w - 1;
	end;
end if;
	
return ds_retorno_w;

end html_obter_dt_termino_agecons;
/