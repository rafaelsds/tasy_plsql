create or replace function OBTER_DESC_PONT_ODI (IE_RADIO_GROUP_P VARCHAR2,
			      QT_PONTUACAO_P NUMBER)
			      return VARCHAR2 is

DS_RETORNO_W VARCHAR2 (255);

begin

/*
DR = DOR;
CP = CUIDADOS PESSOAIS;
LO = LEVANTAMENTO;
MA = MARCHA;
SR = SENTAR;
FP = FICAR EM P�;
DM = DORMIR;
VSX = VIDA SEXUAL;
VSC = VIDA SOCIAL;
VM = VIAGEM;
*/

--DOR
if (IE_RADIO_GROUP_P = 'DR') then

	if (QT_PONTUACAO_P = 0) then -- N�o tenho dor no momento;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740207),1,255);

	elsif (QT_PONTUACAO_P = 1) then -- A dor � muito leve no momento;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740209),1,255);

	elsif (QT_PONTUACAO_P = 2) then -- A dor � moderada no momento;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740213),1,255);

	elsif (QT_PONTUACAO_P = 3) then -- A dor � regulamente intensa no momento;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740215),1,255);

	elsif (QT_PONTUACAO_P = 4) then -- A dor � muito intensa no momento;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740217),1,255);

	elsif (QT_PONTUACAO_P = 5) then -- A dor � a pior imagin�vel no momento;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740219),1,255);

	end if;

--CUIDADOS PESSOAIS
elsif (IE_RADIO_GROUP_P = 'CP') then

	if (QT_PONTUACAO_P = 0) then -- Posso cuidar de mim normalmente sem causar dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740221),1,255);

	elsif (QT_PONTUACAO_P = 1) then -- Posso cuidar de mim normalmente;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740223),1,255);

	elsif (QT_PONTUACAO_P = 2) then -- � doloroso cuidar de mim e eu sou lento e cuidadoso;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740225),1,255);

	elsif (QT_PONTUACAO_P = 3) then -- Necessito de alguma ajuda; mas consigo realizar a maior parte dos meus cuidados pessoais;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740227),1,255);

	elsif (QT_PONTUACAO_P = 4) then -- Necessito de ajuda todos os dias na maioria dos aspectos dos meus cuidados pessoais;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740229),1,255);

	elsif (QT_PONTUACAO_P = 5) then -- N�o me visto; lavo-me com dificuldade e permane�o na cama;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740231),1,255);

	end if;

--LEVANTAMENTO
elsif (IE_RADIO_GROUP_P = 'LO') then

	if (QT_PONTUACAO_P = 0) then -- Posso levantar pesos pesados sem causar dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740233),1,255);

	elsif (QT_PONTUACAO_P = 1) then -- Posso levantar pesos pesados; mas causa dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740237),1,255);

	elsif (QT_PONTUACAO_P = 2) then -- A dor me impede de levanta pesos pesados do ch�o; mas consigo se estiverem bem posicionados;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740239),1,255);

	elsif (QT_PONTUACAO_P = 3) then -- A dor me impede de levanta pesos pesados do ch�o; mas consigo pesos leves a m�dios quando bem posicionados;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740241),1,255);

	elsif (QT_PONTUACAO_P = 4) then -- Posso levanta apenas pesos muito leves;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740243),1,255);

	elsif (QT_PONTUACAO_P = 5) then -- N�o posso levantar ou carregar absolutamente nada;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740245),1,255);

	end if;

--MARCHA
elsif (IE_RADIO_GROUP_P = 'MA') then

	if (QT_PONTUACAO_P = 0) then --Dor n�o me impede de andar qualquer distancia;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740247),1,255);

	elsif (QT_PONTUACAO_P = 1) then --Dor impede-me de andar mais de 1600 m;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740251),1,255);

	elsif (QT_PONTUACAO_P = 2) then --Dor impede-me de andar mais de 400 m
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740255),1,255);

	elsif (QT_PONTUACAO_P = 3) then --Dor impede-me de andar mais de 100 m;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(337958),1,255);

	elsif (QT_PONTUACAO_P = 4) then --Posso andar apenas usando uma bengala ou muletas;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740257),1,255);

	elsif (QT_PONTUACAO_P = 5) then --Estou na cama a maior parte do tempo e tenho que me arrastar at� o banheiro;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740259),1,255);

	end if;

--SENTAR
elsif (IE_RADIO_GROUP_P = 'SR') then

	if (QT_PONTUACAO_P = 0) then --Posso sentar em qualquer cadeira o tempo eu quiser;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740391),1,255);

	elsif (QT_PONTUACAO_P = 1) then --Posso sentar na minha cadeira favorita quanto tempo eu quiser;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740393),1,255);

	elsif (QT_PONTUACAO_P = 2) then --A dor me impede de sentar durante mais de 1 uma hora;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740395),1,255);

	elsif (QT_PONTUACAO_P = 3) then --A dor me impede de sentar durante mais de meia hora;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740397),1,255);

	elsif(QT_PONTUACAO_P = 4) then --A dor evita que eu permane�a sentado por mais de 10 minutos;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740399),1,255);

	elsif(QT_PONTUACAO_P = 5) then --A dor me impede inteiramente de sentar;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740401),1,255);

	end if;

--FICAR EM P�
elsif (IE_RADIO_GROUP_P = 'FP') then

	if (QT_PONTUACAO_P = 0) then --Posso ficar em p� quanto tempo eu quiser sem causar dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740403),1,255);

	elsif (QT_PONTUACAO_P = 1) then --Posso ficar em p� quanto tempo eu quiser; mas causa-me dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740407),1,255);

	elsif (QT_PONTUACAO_P = 2) then --A dor me impede de ficar de p� durante mais de 1 hora;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740409),1,255);

	elsif (QT_PONTUACAO_P = 3) then --A dor me impede de ficar de p� durante mais de meia hora;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740411),1,255);

	elsif(QT_PONTUACAO_P = 4) then --A dor me impede de ficar de p� durante mais de 10 minutos;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740413),1,255);

	elsif(QT_PONTUACAO_P = 5) then --A dor me impede inteiramente de ficar de p�;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740415),1,255);

	end if;

--DORMIR
elsif (IE_RADIO_GROUP_P = 'DM') then

	if (QT_PONTUACAO_P = 0) then --Meu sono nunca � perturbado pela dor;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740417),1,255);

	elsif (QT_PONTUACAO_P = 1) then --Meu sono � ocasionalmente perturbado pela dor;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740419),1,255);

	elsif (QT_PONTUACAO_P = 2) then --Por causa da dor tenho menos de 6 horas de sono;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740421),1,255);

	elsif (QT_PONTUACAO_P = 3) then --Por causa da dor tenho menos de 4 horas de sono;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740423),1,255);

	elsif(QT_PONTUACAO_P = 4) then --Por causa da dor tenho menos de 2 horas de sono;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740425),1,255);

	elsif(QT_PONTUACAO_P = 5) then --A dor me impede completamente de dormir;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740427),1,255);

	end if;

--VIDA SEXUAL
elsif (IE_RADIO_GROUP_P = 'VSX') then

	if (QT_PONTUACAO_P = 0) then --Minha vida sexual � normal e n�o causa dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740429),1,255);

	elsif (QT_PONTUACAO_P = 1) then --Minha vida sexual � normal; mas causa alguma dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740431),1,255);

	elsif (QT_PONTUACAO_P = 2) then --Minha vida sexual � quase normal; mas � muito dolorosa;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740433),1,255);

	elsif (QT_PONTUACAO_P = 3) then --Minha vida sexual � gravemente restringida por dor;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740435),1,255);

	elsif(QT_PONTUACAO_P = 4) then --Minha vida sexual � quase ausente por causa de dor;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740437),1,255);

	elsif(QT_PONTUACAO_P = 5) then --A dor me impede completamente qualquer vida sexual;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740439),1,255);

	end if;

--VIDA SOCIAL
elsif (IE_RADIO_GROUP_P = 'VSC') then

	if (QT_PONTUACAO_P = 0) then --Minha vida social � normal e n�o me causa dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740441),1,255);

	elsif (QT_PONTUACAO_P = 1) then --Minha vida social � normal; mas aumenta o grau de dor;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740443),1,255);

	elsif (QT_PONTUACAO_P = 2) then --A dor n�o tem efeito importante sobre a minha social; exceto limitar interesses que despende mais energia;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740445),1,255);

	elsif (QT_PONTUACAO_P = 3) then --A dor restringiu minha vida social e n�o saio t�o freq�entemente quanto antes;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740447),1,255);

	elsif(QT_PONTUACAO_P = 4) then --A dor restringiu minha vida social a minha casa;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740449),1,255);

	elsif(QT_PONTUACAO_P = 5) then --N�o tenho vida social por causa da dor;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740451),1,255);

	end if;

--VIAGEM
elsif (IE_RADIO_GROUP_P = 'VM') then

	if (QT_PONTUACAO_P = 0) then --Posso viajar a qualquer lugar sem dor;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740453),1,255);

	elsif (QT_PONTUACAO_P = 1) then --Posso viajar para qualquer lugar; mas causa dor extra;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740455),1,255);

	elsif (QT_PONTUACAO_P = 2) then --A dor � forte; mas consigo fazer viagens de mais de 2 horas;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740457),1,255);

	elsif (QT_PONTUACAO_P = 3) then --A dor me restringe a viagens de menos de 1 hora;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740459),1,255);

	elsif(QT_PONTUACAO_P = 4) then --A dor me restringe a viagens necess�rias curtas de menos 30 minutos;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740461),1,255);

	elsif(QT_PONTUACAO_P = 5) then --A dor me impede de viajar; exceto para receber tratamento;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(740463),1,255);

	end if;

end if;

return DS_RETORNO_W;

end OBTER_DESC_PONT_ODI;
 /