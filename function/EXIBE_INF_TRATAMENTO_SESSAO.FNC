create or replace
function exibe_inf_tratamento_sessao(nr_seq_agenda_p		number,
				     ie_informacao_p		varchar2)
 		    	return varchar2 is
ds_retorno_final_w	varchar2(1000);			
						
ds_retorno_w 		varchar2(2000);			
ds_msgm_inf_i_w		varchar2(1000);
ds_msgm_inf_t_w		varchar2(1000);
ds_msgm_inf_s_w		varchar2(1000);
ds_sessao_w		varchar2(50);
qt_total_secao_w		agenda_consulta.qt_total_secao%type;
nr_secao_w		agenda_consulta.nr_secao%type;
nr_secao_ww		agenda_consulta.nr_secao%type;
menor_nr_secao_w		agenda_consulta.nr_secao%type;
nr_controle_secao_w	agenda_consulta.nr_controle_secao%type;
dt_agenda_w		agenda_consulta.dt_agenda%type;


Cursor C01 is
	select 	nr_secao,
		dt_agenda
	from	agenda_consulta
	where	nr_controle_secao = nr_controle_secao_w
	and	nr_secao > nr_secao_ww
	and	ie_status_agenda not in ('C','F','I','S')
	order by   dt_agenda;

begin

if (nr_seq_agenda_p is not null) and
   (ie_informacao_p is not null) then

	-- RETORNA O CONTROLE E NUMERO DA SESSAO ATUAL 
	select	nr_controle_secao,
		nr_secao
	into	nr_controle_secao_w,
		nr_secao_ww
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_p;
	
	-- VERIFICA QUAL A PRIMEIRA SESS�O DO PACIENTE
	select 	min(nr_secao)
	into	menor_nr_secao_w
	from	agenda_consulta
	where	nr_controle_secao = nr_controle_secao_w;
	
	if (nr_controle_secao_w is not null) then
		
		-- RETORNA ULTIMA SECAO DO PACIENTE
		select 	max(qt_total_secao)
		into	qt_total_secao_w
		from	agenda_consulta
		where	nr_controle_secao = nr_controle_secao_w;
	
	end if;

	-- IE_INFORMACAO_P = 'I'
	if (nr_secao_ww = menor_nr_secao_w) then
		/*
		ds_msgm_inf_i_w := 'Esta � a primeira sess�o do tratamento do paciente de' || chr(13) ||
				   'um total de ' || qt_total_secao_w || ' sess�es. Segue a rela��o dos pr�ximos' || chr(13) ||
				   'agendamentos vinculados a esta sess�o do paciente' || chr(13) || chr(13);
		*/
		ds_msgm_inf_i_w	:= obter_texto_tasy(404070, wheb_usuario_pck.get_nr_seq_idioma) || chr(13) || 
				   obter_texto_tasy(404549, wheb_usuario_pck.get_nr_seq_idioma) || chr(13) ||
				   obter_texto_tasy(404550, wheb_usuario_pck.get_nr_seq_idioma) || chr(13) || chr(13);
		
		ds_msgm_inf_i_w	:= replace(ds_msgm_inf_i_w, '#@QT_TOTAL_SECAO#@', qt_total_secao_w);
		   
	end if;
	
	-- IE_INFORMACAO_P = 'T'
	if (nr_secao_ww = qt_total_secao_w) then
		/*
		ds_msgm_inf_t_w := 'Esta � a �ltima sess�o do tratamento do paciente de um total de ' || qt_total_secao_w || ' sess�es.';
		*/
		ds_msgm_inf_t_w := obter_texto_tasy(404551, wheb_usuario_pck.get_nr_seq_idioma);
		ds_msgm_inf_t_w	:= replace(ds_msgm_inf_t_w, '#@QT_TOTAL_SECAO#@', qt_total_secao_w);

	end if;
	
	-- IE_INFORMACAO_P = 'S'
	if (nr_secao_ww > menor_nr_secao_w) and
	   (nr_secao_ww < qt_total_secao_w) then
	        /*
		ds_msgm_inf_s_w := 'Esta � a ' || nr_secao_ww || '� sess�o do tratamento do paciente de um '|| chr(13) ||
				   'total de ' || qt_total_secao_w || ' sess�es. Segue a rela��o dos pr�ximos' || chr(13) ||
				   'agendamentos vinculados a esta sess�o do paciente' || chr(13) || chr(13);
		*/
		ds_msgm_inf_s_w := obter_texto_tasy(406107, wheb_usuario_pck.get_nr_seq_idioma);
		
		ds_msgm_inf_s_w := replace(ds_msgm_inf_s_w, '#@NR_SESSAO#@', nr_secao_ww);
		
		ds_msgm_inf_s_w := ds_msgm_inf_s_w || chr(13) ||
				   obter_texto_tasy(404549, wheb_usuario_pck.get_nr_seq_idioma) || chr(13) ||
				   obter_texto_tasy(404550, wheb_usuario_pck.get_nr_seq_idioma) || chr(13) || chr(13);
		
		ds_msgm_inf_s_w := replace(ds_msgm_inf_s_w, '#@QT_TOTAL_SECAO#@', qt_total_secao_w);		   
		   
	end if;
	
	-- Sess�o
	ds_sessao_w := obter_texto_tasy(407055, wheb_usuario_pck.get_nr_seq_idioma);
	
	open C01;
	loop
	fetch C01 into	
		nr_secao_w,
		dt_agenda_w;
	exit when C01%notfound;
		begin
		
		if ((ie_informacao_p = 'I') or (ie_informacao_p = 'S') or (ie_informacao_p = 'IT')) and 
		   (nr_secao_w > menor_nr_secao_w) then
		
			if (ds_retorno_w is null) then 
				ds_retorno_w := ds_sessao_w||' '||nr_secao_w||'/'||qt_total_secao_w||': '||to_char(dt_agenda_w, 'dd/mm/yyyy hh24:mi');
			else
				ds_retorno_w := ds_retorno_w || chr(13) || ds_sessao_w||' '||nr_secao_w||'/'||qt_total_secao_w||': '||to_char(dt_agenda_w, 'dd/mm/yyyy hh24:mi');
			end if;
			
		end if;
			
		end;
	end loop;
	close C01;

end if;

if ((ie_informacao_p = 'I') or (ie_informacao_p = 'S') or (ie_informacao_p = 'IT')) and
   (ds_msgm_inf_i_w is not null) and
   (ds_retorno_w is not null) then
	ds_retorno_final_w := ds_msgm_inf_i_w || ds_retorno_w;
	
elsif ((ie_informacao_p = 'T') or (ie_informacao_p = 'S') or (ie_informacao_p = 'IT')) and
      (ds_msgm_inf_t_w is not null) then	
	ds_retorno_final_w := ds_msgm_inf_t_w;
	
elsif (ie_informacao_p = 'S') and
      (ds_msgm_inf_s_w is not null) and
      (ds_retorno_w is not null) then
	ds_retorno_final_w := ds_msgm_inf_s_w || ds_retorno_w;
end if;

return	ds_retorno_final_w;

end exibe_inf_tratamento_sessao;
/
