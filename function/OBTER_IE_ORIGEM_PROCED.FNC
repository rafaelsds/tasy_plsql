create or replace
function obter_ie_origem_proced(
		nr_atendimento_p	number)
		return number is

ie_origem_proced_w	number(10,0);
cd_convenio_w		number(5,0);
cd_categoria_w		varchar2(10);
ie_tipo_atendimento_w	number(3,0);
ie_tipo_convenio_w	number(2,0);
begin
if	(nr_atendimento_p is not null) then	
	begin
	cd_convenio_w	:= obter_convenio_atendimento(nr_atendimento_p);
	cd_categoria_w	:= obter_categoria_atendimento(nr_atendimento_p);
	
	select	ie_tipo_atendimento,
		ie_tipo_convenio
	into	ie_tipo_atendimento_w,
		ie_tipo_convenio_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_p;
	
	ie_origem_proced_w	:= nvl(obter_origem_proced_cat(
					wheb_usuario_pck.get_cd_estabelecimento,
					ie_tipo_atendimento_w,
					ie_tipo_convenio_w,
					cd_convenio_w,
					cd_categoria_w),1);
	end;
end if;
return ie_origem_proced_w;
end obter_ie_origem_proced;
/