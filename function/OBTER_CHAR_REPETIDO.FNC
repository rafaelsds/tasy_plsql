create or replace
function obter_char_repetido(	nr_char_ascii_p	number,
				qt_repeticoes_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(4000) := null;

begin

for i in 1..qt_repeticoes_p loop
	ds_retorno_w := ds_retorno_w || chr(nr_char_ascii_p);
end loop;

return	ds_retorno_w;

end obter_char_repetido;
/