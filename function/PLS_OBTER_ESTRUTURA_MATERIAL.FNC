create or replace
function pls_obter_estrutura_material(	nr_seq_estrut_mat_p		number,
					nr_sequencia_p			number,
					ie_nivel_p			number)
					return varchar2 is
					
ds_retorno_w		varchar2(255);
ds_estrutura_w		varchar2(100);
qt_loop_w		number(10) := 2;
nr_seq_superior_w	number(10);
nr_seq_superior_ww	number(10);
nr_seq_estrutura_w	number(15);

begin
if	(ie_nivel_p is null) then 
	-- Obter a estrutura mais superior
	select 	min(CONNECT_BY_ROOT nr_sequencia)
	into	nr_seq_estrutura_w
	from  	pls_estrutura_material
	where 	nr_sequencia = nr_seq_estrut_mat_p
	connect by prior nr_sequencia = nr_seq_superior;	
else	
	-- Obter estrutura conforme n�vel especificado
	select 	max(CONNECT_BY_ROOT nr_sequencia)
	into	nr_seq_estrutura_w
	from  	pls_estrutura_material
	where 	level = ie_nivel_p and nr_sequencia = nr_seq_estrut_mat_p
	connect by prior nr_sequencia = nr_seq_superior;	
end if;

select	substr(max(ds_estrutura),1,50)
into	ds_estrutura_w
from	pls_estrutura_material
where	nr_sequencia = nr_seq_estrutura_w;

ds_retorno_w := ds_estrutura_w; 

return	ds_retorno_w;

end pls_obter_estrutura_material;
/