create or replace
function gel_obter_destino_malote(nr_seq_malote_p	number)
 		    	return varchar2 is

nr_seq_local_entrega_w	number(10,0);
ds_retorno_w		varchar2(80);
			
			
begin

select	max(nr_seq_local_entrega)
into	nr_seq_local_entrega_w
from	malote_envelope_laudo
where	nr_sequencia =	nr_seq_malote_p;

select	max(substr(ds_local_entrega,1,80))
into	ds_retorno_w
from	local_entrega_malote
where	nr_sequencia	= nr_seq_local_entrega_w;


return	ds_retorno_w;

end gel_obter_destino_malote;
/