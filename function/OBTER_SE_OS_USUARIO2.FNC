create or replace
function obter_se_os_usuario2
			(	nr_ordem_servico_p		number,
				nm_usuario_p			varchar2,
				ie_opcao_p			varchar2,
				nm_usuario_exec_p		varchar2)
				return varchar2 is

/*		ie_op��o_p
		MU	Somente as minhas
		MG	Meu grupo
		MD	Meu departamento
		T	Todas
		M	Minha gerencia
		MGT	Meu grupo de trabalho
		MGP	Meu grupo de planejamento
*/

ie_retorno_w			varchar2(15);
nr_seq_grupo_des_w		number(15,0);
nr_seq_grupo_sup_w		number(15,0);
cd_pessoa_fisica_w		varchar2(10);
cd_setor_atendimento_w		number(10,0);
cd_setor_gerencia_w		number(10,0);
nm_user_w			varchar2(50);
nr_grupo_trabalho_w		number(10,0);
nr_grupo_planejamento_w		number(10,0);

BEGIN

/*select	username
into	nm_user_w
from	user_users;*/

ie_retorno_w			:= 'N';

if	(ie_opcao_p = 'MG') or
	(ie_opcao_p = 'MD') or
	(ie_opcao_p = 'M') then
	select	nr_seq_grupo_des,
		nr_seq_grupo_sup
	into	nr_seq_grupo_des_w,
		nr_seq_grupo_sup_w
	from	man_ordem_servico
	where	nr_sequencia		= nr_ordem_servico_p;
end if;

/*select	max(cd_setor_atendimento)
into	cd_setor_gerencia_w
from 	gerencia_wheb b, 
	grupo_desenvolvimento a
where 	a.nr_seq_gerencia	= b.nr_sequencia
and 	a.nr_seq_gerencia	= a.nr_seq_gerencia
and	a.nr_sequencia	= nr_seq_grupo_des_w;*/

if	(ie_opcao_p = 'MU') then
	select	obter_se_usuario_exec2(nr_ordem_servico_p, nm_usuario_p, 0, nm_usuario_exec_p)
	into	ie_retorno_w
	from	dual;
elsif	(ie_opcao_p = 'MG') then
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	usuario_grupo_des a
	where	a.nr_seq_grupo		= nr_seq_grupo_des_w
	and	a.nm_usuario_grupo	= nm_usuario_p;
	
	if	(ie_retorno_w = 'N') then
		select	nvl(max('S'),'N')
		into	ie_retorno_w
		from	usuario_grupo_sup
		where	nr_seq_grupo		= nr_seq_grupo_sup_w
		and	nm_usuario_grupo	= nm_usuario_p;
	end if;
elsif	(ie_opcao_p = 'MD') then
	select	max(cd_setor_atendimento)
	into	cd_setor_gerencia_w
	from 	gerencia_wheb b, 
		grupo_desenvolvimento a
	where 	a.nr_seq_gerencia	= b.nr_sequencia
	and 	a.nr_seq_gerencia	= a.nr_seq_gerencia
	and	a.nr_sequencia		= nr_seq_grupo_des_w;
	
	select	nvl(max(cd_setor_atendimento), 0)
	into	cd_setor_atendimento_w
	from	usuario
	where	nm_usuario		= nm_usuario_p;
	
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	usuario_grupo_des a
	where	cd_setor_atendimento_w 	= cd_setor_gerencia_w
	and	a.nm_usuario_grupo 	= nm_usuario_p;
		
	if	(ie_retorno_w = 'N') then
		select	nvl(max('S'),'N')
		into	ie_retorno_w
		from	usuario_grupo_sup
		where	nm_usuario_grupo = nm_usuario_p;
	end if;
	
elsif	(ie_opcao_p	= 'M') then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	usuario
	where	nm_usuario		= nm_usuario_p;
	
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	gerencia_wheb a,
		grupo_desenvolvimento b
	where	a.nr_sequencia		= b.nr_seq_gerencia
	and	b.nr_sequencia		= nr_seq_grupo_des_w
	and 	a.cd_responsavel		= cd_pessoa_fisica_w;
	
	select	nvl(max(cd_setor_atendimento), 0)
	into	cd_setor_atendimento_w
	from	usuario
	where	nm_usuario		= nm_usuario_p;
	
	select	username
	into	nm_user_w
	from	user_users;
	
	if	(cd_setor_atendimento_w = 4) and
		(nm_user_w = 'CORP') then
		select	nvl(max('S'),'N')
		into	ie_retorno_w
		from	gerencia_wheb a,
			grupo_desenvolvimento b
		where	a.nr_sequencia		= b.nr_seq_gerencia_sup
		and	b.nr_sequencia		= nr_seq_grupo_des_w
		and 	a.cd_responsavel		= cd_pessoa_fisica_w;
	end if;
elsif	(ie_opcao_p = 'MGT') then
	begin
	select	nr_grupo_trabalho
	into	nr_grupo_trabalho_w
	from	man_ordem_servico
	where	nr_sequencia	= nr_ordem_servico_p;
	
	select	substr(obter_se_usuario_grupo_trab(nr_grupo_trabalho_w, nm_usuario_p),1,1)
	into	ie_retorno_w
	from	dual;	
	end;
elsif	(ie_opcao_p = 'MGP') then
	begin
	select	nr_grupo_planej
	into	nr_grupo_planejamento_w
	from	man_ordem_servico
	where	nr_sequencia = nr_ordem_servico_p;
	
	select	substr(obter_se_usuario_grupo_planej(nr_grupo_planejamento_w, nm_usuario_p),1,1)
	into	ie_retorno_w
	from	dual;	
	end;
elsif	(ie_opcao_p = 'T') then
	ie_retorno_w		:= 'S';
end if;

return ie_retorno_w;

end obter_se_os_usuario2;
/