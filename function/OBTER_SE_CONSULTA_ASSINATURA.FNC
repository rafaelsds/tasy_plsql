create or replace
function obter_se_consulta_assinatura(	nr_seq_projeto_p	number,
										cd_perfil_p			number)
									return char is

ie_consulta_automatica_w	char(1);
cd_perfil_w					perfil.cd_perfil%type;

cursor c01 is
select	nvl(a.ie_consulta_automatica,'S')
from	tasy_proj_assin_perfil a
where	a.nr_seq_proj		= nr_seq_projeto_p
and		nvl(a.cd_perfil, cd_perfil_w) = cd_perfil_w
order by
		nvl(a.cd_perfil,0);

begin

cd_perfil_w	:= nvl(cd_perfil_p,0);

open c01;
loop
fetch c01 into
	ie_consulta_automatica_w;
exit when c01%notfound;
	ie_consulta_automatica_w := ie_consulta_automatica_w;
end loop;
close c01;

return ie_consulta_automatica_w;

end obter_se_consulta_assinatura;
/