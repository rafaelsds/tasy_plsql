create or replace
function Obter_Apres_Serv_Nut
		(nr_seq_servico_p	number,
		nr_seq_comp_p		number)
 		return number DETERMINISTIC is

nr_seq_apresent_w	number(10,0);
		
begin

select	nvl(max(nr_seq_apresent), 0)
into	nr_seq_apresent_w
from 	nut_comp_servico
where	nr_seq_servico		= nr_seq_servico_p
and	nr_seq_composicao	= nr_seq_comp_p;

return	nr_seq_apresent_w;

end Obter_Apres_Serv_Nut;
/