create or replace
function obter_se_espec_medico_plano(	cd_espec_plano_p	number,
					cd_medico_exec_p	varchar2)	
						return varchar2 is

ie_existe_w			varchar2(5) := 'S';
begin

if	(nvl(cd_espec_plano_p,0) <> 0) then

	select	decode(count(*),0,'N','S')
	into	ie_existe_w
	from	especialidade_medica b,
		medico_especialidade a
	where	a.cd_especialidade	= b.cd_especialidade
	and	a.cd_pessoa_fisica	= cd_medico_exec_p
	and	b.cd_especialidade	= cd_espec_plano_p;
end if;


return ie_existe_w;

end obter_se_espec_medico_plano;
/
