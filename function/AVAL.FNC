create or replace
function Aval
	(nr_seq_avaliacao_p	number,
	nr_seq_item_p		number)
	return varchar2 is

ds_resultado_w	varchar2(4000);

begin


select	obter_result_avaliacao(nr_seq_avaliacao_p, nr_seq_item_p)
into	ds_resultado_w
from	dual;


return	ds_resultado_w;

end Aval;
/