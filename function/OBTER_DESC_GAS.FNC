create or replace
function Obter_Desc_Gas(nr_sequencia_p		number)
 		    	return varchar2 is
ds_gas_w		varchar2(80);
begin

if	(nr_sequencia_p	is not null) then
	select	ds_gas
	into	ds_gas_w
	from	gas
	where	nr_sequencia	= nr_sequencia_p;
	
end if;

return	ds_gas_w;

end Obter_Desc_Gas;
/