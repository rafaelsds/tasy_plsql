create or replace
function Obter_qtd_Exame_laudo_lib(	nr_prescricao_p		number,
					nr_sequencia_p		number,
					nr_seq_exame_p		number)
 		    	return number is

qt_retorno_w	number(10);	
		
			
begin
if	(nr_seq_exame_p	is null)then
	select	count(*)
	into	qt_retorno_w
	from	laudo_paciente
	where	nr_prescricao		= nr_prescricao_p
	and	nr_seq_prescricao	= nr_sequencia_p
	and	dt_liberacao is not null;

else	
	SELECT 	nvl(max(1),0)
	into	qt_retorno_w
	from 	prescr_procedimento b, 
        	prescr_medica c,
       		exame_lab_resultado e, 
        	exame_lab_result_item g,
		exame_laboratorio f                               
	where 	b.nr_prescricao         = c.nr_prescricao
       	and   	b.nr_prescricao         = e.nr_prescricao
	and   	e.nr_seq_resultado   = g.nr_seq_resultado
--	and   	g.nr_seq_exame       = f.nr_seq_exame
	and   	b.nr_seq_exame       = f.nr_seq_exame
	and     g.nr_seq_prescr        = b.nr_sequencia
       	and   	b.nr_prescricao         = nr_prescricao_p
       	and   	b.nr_sequencia         = nr_sequencia_p;

	
end if;

return	qt_retorno_w;

end Obter_qtd_Exame_laudo_lib;
/
