create or replace
function pls_obter_status_lote_analise(	nr_seq_lote_p	Number)
			return Varchar2 is

ds_status_w		Varchar2(255);
begin

if	(nvl(nr_seq_lote_p,0) > 0) then
	begin
		select	substr(obter_valor_dominio(4892,IE_STATUS),1,255)
		into	ds_status_w
		from	pls_lote_protocolo_conta
		where	nr_sequencia 	= nr_seq_lote_p;
	exception
	when others then
		ds_status_w	:= null;
	end;
end if;

return ds_status_w;

end pls_obter_status_lote_analise;
/
