create or replace
function man_obter_data_por_hora
			(	dt_inicial_p		date,
				qt_horas_p		number,
				nr_seq_grupo_planej_p	number default null,
				nr_seq_grupo_trab_p	number default null)
				return date is

dt_aux_w			date;
dt_hr_inicio_w			date;
dt_hr_fim_w			date;
dt_final_w			date;
dt_min_dia_w			date;
qt_horas_w			number(7,2);
qt_min_dia_w			number(10) := 0;
qt_min_intervalo_w		number(5) := 0;
ie_feriado_w			number(10);
ie_dia_semana_w			man_horario_trabalho.ie_dia_semana%type;

begin
dt_aux_w	:= dt_inicial_p;
dt_final_w	:= dt_inicial_p;
qt_horas_w	:= qt_horas_p;

if	(dt_inicial_p is not null) and (nvl(qt_horas_w,0) > 0) then
	begin
	dt_final_w	:= null;
	
	while	(qt_horas_w > 0) loop
		begin
		ie_dia_semana_w	:= pkg_date_utils.get_WeekDay(dt_aux_w);
		ie_feriado_w	:= obter_se_feriado(wheb_usuario_pck.get_cd_estabelecimento,dt_aux_w);
		
		select	max(to_date(to_char(dt_aux_w,'dd/mm/yyyy') || ' ' || substr(hr_inicio,1,2) || ':' ||substr(hr_inicio,3,4) || ':00','dd/mm/yy hh24:mi:ss')),
			max(to_date(to_char(dt_aux_w,'dd/mm/yyyy') || ' ' || substr(hr_fim,1,2) || ':' ||substr(hr_fim,3,4) || ':00','dd/mm/yy hh24:mi:ss')),
			max(nvl(qt_min_intervalo,0))
		into	dt_hr_inicio_w,
			dt_hr_fim_w,
			qt_min_intervalo_w	
		from	(select	man_obter_se_hor_trab_lib(nr_sequencia,nr_seq_grupo_planej_p,nr_seq_grupo_trab_p,wheb_usuario_pck.get_nm_usuario) ie_lib,
				hr_inicio,
				hr_fim,
				qt_min_intervalo
			from	man_horario_trabalho
			where	ie_dia_semana	= ie_dia_semana_w
			and	0		= ie_feriado_w)
		where	ie_lib	= 'S';
		
		if	(dt_hr_inicio_w is null) and (dt_hr_fim_w is null) then
			select	max(to_date(to_char(dt_aux_w,'dd/mm/yyyy') || ' ' || substr(hr_inicio,1,2) || ':' ||substr(hr_inicio,3,4) || ':00','dd/mm/yy hh24:mi:ss')),
				max(to_date(to_char(dt_aux_w,'dd/mm/yyyy') || ' ' || substr(hr_fim,1,2) || ':' ||substr(hr_fim,3,4) || ':00','dd/mm/yy hh24:mi:ss')),
				max(nvl(qt_min_intervalo,0))
			into	dt_hr_inicio_w,
				dt_hr_fim_w,
				qt_min_intervalo_w	
			from	(select	hr_inicio,
					hr_fim,
					qt_min_intervalo
				from	man_horario_trabalho
				where	ie_dia_semana	= ie_dia_semana_w
				and	0		= ie_feriado_w);
		end if;
		
		if	(dt_hr_inicio_w is not null) and (dt_hr_fim_w is not null) then
			begin
			if	(to_date(dt_aux_w,'dd/mm/yyyy') = to_date(dt_inicial_p,'dd/mm/yyyy')) then
				begin				
				if	(dt_inicial_p < dt_hr_inicio_w) and (dt_inicial_p <= dt_hr_fim_w) then
					dt_final_w	:= dt_hr_inicio_w + (qt_min_intervalo_w/1440);
				elsif	(dt_inicial_p >= dt_hr_inicio_w) and (dt_inicial_p <= dt_hr_fim_w) then
					dt_final_w	:= dt_inicial_p + (qt_min_intervalo_w/1440);
				end if;
				
				if 	(dt_final_w is not null) then
					begin
					if	((dt_final_w + ((qt_horas_w / 1440) * 60)) <= dt_hr_fim_w) then	
						begin
						dt_final_w := dt_final_w + ((qt_horas_w / 1440) * 60);
						qt_horas_w := 0;
						end;
					elsif	(dt_final_w < dt_hr_fim_w) then
						begin
						qt_horas_w := qt_horas_w - trunc((dt_hr_fim_w - dt_final_w) * 1440/60);
						
						dt_min_dia_w := dt_final_w;
						while	(dt_min_dia_w < dt_hr_fim_w) loop
							begin
							dt_min_dia_w := dt_min_dia_w + ((1 / 1440) * 60);
							end;
						end loop;
						dt_min_dia_w := dt_min_dia_w - ((1 / 1440) * 60);
						if	(obter_min_entre_datas(dt_min_dia_w,dt_hr_fim_w,1) < 60) then
							qt_min_dia_w := obter_min_entre_datas(dt_min_dia_w,dt_hr_fim_w,1);
						end if;						
						end;
					end if;
					end;
				end if;
				end;
			end if;
		
			if	(to_date(dt_aux_w,'dd/mm/yyyy') > to_date(dt_inicial_p,'dd/mm/yyyy')) then
				begin
				dt_final_w 	:= dt_hr_inicio_w + (nvl(qt_min_dia_w,0)/1440) + (qt_min_intervalo_w/1440);
				qt_min_dia_w	:= 0;
				
				if	((dt_final_w + ((qt_horas_w / 1440) * 60)) <= dt_hr_fim_w) then
					begin
					dt_final_w	:= dt_final_w + ((qt_horas_w / 1440) * 60);
					qt_horas_w	:= 0;
					end;
				else
					begin
					qt_horas_w	:= qt_horas_w - trunc((dt_hr_fim_w - dt_final_w) * 1440/60);
					dt_min_dia_w	:= dt_final_w;
					
					while	(dt_min_dia_w < dt_hr_fim_w) loop
						begin
						dt_min_dia_w := dt_min_dia_w + ((1 / 1440) * 60);
						end;
					end loop;
					
					dt_min_dia_w	:= dt_min_dia_w - ((1 / 1440) * 60);
					
					if	(obter_min_entre_datas(dt_min_dia_w,dt_hr_fim_w,1) < 60) then
						qt_min_dia_w	:= obter_min_entre_datas(dt_min_dia_w,dt_hr_fim_w,1);
					end if;
					end;
				end if;
				end;	
			end if;
			end;
		end if;
		
		dt_aux_w := dt_aux_w +1;
		end;
	end loop;
	end;
end if;

return dt_final_w;

end man_obter_data_por_hora;
/