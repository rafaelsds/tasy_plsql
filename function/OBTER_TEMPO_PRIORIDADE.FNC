create or replace
function obter_tempo_prioridade (nr_prescricao_p number,
                            nr_seq_proc_interno_p number,
                            nr_sequencia_p number)
							return varchar2 is

nr_seq_prioridade_w REG_PRIOR_PROCED_EXAME.nr_sequencia%type;
nr_prioridade_w PROTOCOLO_MEDIC_PROC.NR_SEQ_REG_PRIOR_PROCED_EXAME%type;
nr_tmp_liberacao_w	number(4, 0);
dt_prazo_execucao_w      date;
dt_execucao_w      date;
ds_tmp_para_execucao_w	varchar2(255);

begin

  nr_seq_prioridade_w := obter_prioridade(nr_prescricao_p, nr_seq_proc_interno_p);
  
  select  max(DT_BAIXA)
  into    dt_execucao_w
  from    prescr_procedimento
  where   nr_prescricao = nr_prescricao_p
  and     nr_seq_proc_interno = nr_seq_proc_interno_p
  and     nr_sequencia = nr_sequencia_p;
   
	if (nr_seq_prioridade_w is not null) then
		select max(rppe.nr_prioridade),
			max(rppe.nr_tempo_execucao)
		into nr_prioridade_w,
			nr_tmp_liberacao_w
		from reg_prior_proced_exame rppe
		where rppe.nr_sequencia = nr_seq_prioridade_w;		
			
		if (nr_tmp_liberacao_w is not null) and (nr_tmp_liberacao_w > 0) then

			select ADICIONAR_MINUTOS_DATA(obter_estabelecimento_ativo, dt_execucao_w, nr_tmp_liberacao_w, 'COR')
			into dt_prazo_execucao_w
			from dual;
			
			select obter_horas_min(sysdate, dt_prazo_execucao_w)
			into ds_tmp_para_execucao_w
			from dual;
			
			if (((SYSDATE - dt_prazo_execucao_w)*24*60) > nr_tmp_liberacao_w)
				or (((SYSDATE - dt_prazo_execucao_w)*24*60) > 0) then
				ds_tmp_para_execucao_w := 0;
			end if;

		end if;
	end if;

return ds_tmp_para_execucao_w;
end;
/