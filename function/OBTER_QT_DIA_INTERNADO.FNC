create or replace
function obter_qt_dia_internado ( 	nr_atendimento_p	number,
					cd_estabelecimento_p	number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2)
 		    	return number is

qt_dia_internado_w	number(10) := 0;
dt_entrada_w		date;
ie_dias_alta_w		varchar2(1);
nr_atend_alta_w		number(10);
nr_atendimento_w	number(10);
qt_dias_alta_w		number(3);

/*
P - primeiro atendimento do paciente com altas administrativas;
U - �ltimo atendimento do paciente.
*/
begin

if 	(nr_atendimento_p is not null) then
	begin
	Obter_Param_Usuario(44, 155, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_dias_alta_w);

	if	(ie_dias_alta_w = 'N') then
		begin
		nr_atend_alta_w	:= 	nr_atendimento_p;
		nr_atendimento_w := 	nr_atendimento_p;
		select	count(*)
		into	qt_dias_alta_w
		from 	atendimento_paciente a
		where	a.nr_atendimento =	nr_atendimento_p
		and	a.nr_atend_alta is not null;
		
		if	(qt_dias_alta_w = 0) then
			ie_dias_alta_w := 'S';
			nr_atend_alta_w	:= 0;
		end if;
		
		while (nr_atend_alta_w <> 0)
			loop
			select	max(a.nr_atend_alta),
				max(a.dt_entrada)
			into	nr_atend_alta_w,
				dt_entrada_w
			from 	atendimento_paciente a
			where	a.nr_atendimento = 	(select b.nr_atend_alta
							from atendimento_paciente b
							where b.nr_atendimento = nr_atend_alta_w);
			
			end loop;
			
		if 	(dt_entrada_w is null) then
			begin
			nr_atend_alta_w	:= 	nr_atendimento_p ;
			nr_atendimento_w := 	nr_atendimento_p;
			while (nr_atend_alta_w <> 0)
				loop

				select  max(a.nr_atend_alta),
					max(a.dt_entrada)
				into    nr_atend_alta_w,
					dt_entrada_w              
				from    atendimento_paciente a
				where   a.nr_atendimento =  nr_atendimento_w;

				select  max(b.nr_atend_alta)
				into    nr_atendimento_w
				from    atendimento_paciente b
				where   b.nr_atendimento = nr_atend_alta_w;
				end loop;
			end;
		end if;
		end;
	end if;
	if	(ie_dias_alta_w = 'S') then
		begin
		select 	max(b.dt_entrada)
		into	dt_entrada_w
		from 	atendimento_paciente b
		where 	b.nr_atendimento = nr_atendimento_p;
		end;
	end if;
	
	if 	(dt_entrada_w is not null) then
		qt_dia_internado_w 	:= (trunc(sysdate) - trunc(dt_entrada_w));
	end if;
	
	end;
end if;

return	qt_dia_internado_w;

end obter_qt_dia_internado;
/