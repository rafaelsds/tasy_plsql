create or replace
function Obter_Med_Regulaco_Receita(nr_sequencia_p		number)
 		    	return varchar2 is


nr_seq_receita_amb_w	number(10);
nr_seq_reg_w			number(10);
cd_material_w			number(10);
ds_retorno_w			varchar2(4000);
ds_material_w			varchar2(255);
virgula_w				varchar2(2);


Cursor C01 is
	select	substr(obter_desc_material(b.cd_material),1,255) ds_material
	from	fa_receita_farmacia a,
			fa_receita_farmacia_item b,
			regulacao_atend c
	where	 c.nr_seq_receita_amb =  a.nr_sequencia
	and   	a.nr_sequencia = b.nr_seq_receita
	and   	b.nr_sequencia = c.nr_seq_receita_item
	and   	a.nr_sequencia = nr_seq_receita_amb_w
	and		obter_status_regulacao(c.nr_seq_receita_amb, 'RF') in ('AE','EN','CA','AN','RP','PS','LE','AG','NG');

begin

select	max(nr_seq_receita_amb)
into	nr_seq_receita_amb_w
from 	fa_entrega_medicacao
where	nr_sequencia = nr_sequencia_p;

if( nr_seq_receita_amb_w is not null)then
	
	 for material_farmacia in C01 loop
        ds_material_w := material_farmacia.ds_material;
			
		ds_retorno_w := ds_retorno_w || virgula_w || ds_material_w;
		
		if( virgula_w is null)then
			virgula_w := ',';
		end if;
        
    end loop;

end if;


return	nvl( ds_retorno_w, '');

end Obter_Med_Regulaco_Receita;
/