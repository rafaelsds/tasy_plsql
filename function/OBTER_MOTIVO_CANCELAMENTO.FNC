create or replace
function Obter_motivo_cancelamento(	nr_seq_motivo_cancel_p		number)
					return varchar2 is

ds_retorno_w		varchar2(100) := '';					

begin

if (nr_seq_motivo_cancel_p is not null) then
	begin
		select	ds_motivo
		into	ds_retorno_w
		from	via_motivo_cancelamento
		where	nr_sequencia = nr_seq_motivo_cancel_p;
	end;
end if;

return	ds_retorno_w;

end Obter_motivo_cancelamento;
/ 