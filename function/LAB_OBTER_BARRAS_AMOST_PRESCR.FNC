create or replace
function lab_obter_barras_amost_prescr(	
					nr_prescricao_p		number,
					nr_seq_prescr_p		number		
					)
 		    	return varchar2 is

cd_barras_w		varchar2(255);
ie_padrao_amostra_w	varchar2(5) := null;
			
begin

select	max(l.ie_padrao_amostra)
into	ie_padrao_amostra_w
from	lab_parametro l,
	prescr_medica p
where	p.nr_prescricao		= nr_prescricao_p
and	p.cd_estabelecimento 	= l.cd_estabelecimento;


return	lab_obter_barras_amostra(nr_prescricao_p, nr_seq_prescr_p, null, ie_padrao_amostra_w);

end lab_obter_barras_amost_prescr;
/