Create or replace
Function obter_se_inspecao_liberada(	nr_ordem_compra_p	number)
return	varchar2 is

/* Essa function verifica se todas as inspe��es da ordem de compra (passada por par�metro) est�o liberadas  */

qt_inspecao_w		number(5);
qt_ordem_w		number(5);
ds_resultado_w		varchar2(1);

begin
ds_resultado_w := 'N';

select	count(*)
into	qt_inspecao_w
from	inspecao_recebimento
where	nr_ordem_compra = nr_ordem_compra_p;

select	count(*)
into	qt_ordem_w
from	inspecao_recebimento
where	nr_ordem_compra = nr_ordem_compra_p
and	dt_liberacao is not null;

if	(qt_inspecao_w = qt_ordem_w) then
	ds_resultado_w := 'S';
end if;

return	ds_resultado_w;

end	obter_se_inspecao_liberada;
/