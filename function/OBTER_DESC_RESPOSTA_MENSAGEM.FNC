create or replace
function obter_desc_resposta_mensagem(ie_valor_retorno	number,
				  ie_tipo_mensagem number)
				return varchar2 is
					
ds_retorno_w		varchar2(250) := null;

begin
	select case
		when ie_valor_retorno = 0 then
			obter_desc_expressao(decode(ie_tipo_mensagem, 0, 311702, 327113))
		when ie_valor_retorno = 1 then
			obter_desc_expressao(327114)
		when ie_valor_retorno = 2 then
			obter_desc_expressao(302745)
	end tipoMsg
		into ds_retorno_w
	from dual;

	return	ds_retorno_w;

end;
/