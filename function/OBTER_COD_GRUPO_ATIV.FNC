create or replace
function obter_cod_grupo_ativ(	cd_estabelecimento_p	number,
				nr_seq_nota_p		varchar2,
				ie_opcao_p		varchar2)
 		    	return Varchar2 is
				
qt_item_w		Number(15);
qt_item_serv_w		Number(15);
qt_pessoa_juridica_w	Number(15);
qt_possui_w		Number(15);
cd_cgc_emitente_w	Varchar(14);
nr_codigo_serv_prest_w	Varchar(10):= null;
retorno_w			Varchar(255):= null;
cod_serv_prest_proc_w	varchar(255) := null;
nr_ativ_w			Varchar(10):= '0';
cd_item_servico_w		Varchar(20);
ds_item_servico_w		Varchar(255);
cd_procedimento_w	procedimento.cd_procedimento%type;
cd_material_w		material.cd_material%type;
cd_grupo_w		grupo_servico.cd_servico%type;
cd_item_serv_mun_w	Varchar(20);

/*
I - Codigo do servico prestado Item da LC 116/2003
C - Codigo do servico prestado proprio do municipio 
D - Descricao
G- Codigo do Grupo
*/

begin

select	cd_cgc_emitente
into	cd_cgc_emitente_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nota_p;

select	count(*)
into	qt_pessoa_juridica_w
from	pessoa_juridica_estab
where	cd_estabelecimento = cd_estabelecimento_p
and	cd_cgc = cd_cgc_emitente_w;

if (qt_pessoa_juridica_w > 0) then

	select	max(nr_codigo_serv_prest)
	into	nr_ativ_w
	from	pessoa_juridica_estab
	where	cd_estabelecimento = cd_estabelecimento_p
	and	cd_cgc = cd_cgc_emitente_w;	
	
end if;

select	count(*)
into	qt_item_w
from	nota_fiscal_item
where	nr_sequencia = nr_seq_nota_p;

if (qt_item_w >= 1) then	

	select	sum(qt)
	into	qt_item_serv_w 
	from	(
		select	count(*) qt	
		from	nota_fiscal_item i,
			procedimento p
		where	p.cd_procedimento = i.cd_procedimento
		and	p.ie_origem_proced = i.ie_origem_proced
		and	i.cd_estabelecimento = cd_estabelecimento_p
		and	i.nr_sequencia = nr_seq_nota_p
		and	p.nr_seq_item_serv is not null
		union all
		select	count(*) qt
		from	nota_fiscal_item i,
			material_fiscal p
		where	p.cd_material = i.cd_material
		and	i.cd_estabelecimento = cd_estabelecimento_p
		and	i.nr_sequencia = nr_seq_nota_p
		and	p.nr_seq_item_serv is not null
		);
	
	if (qt_item_serv_w > 0) then
	
		select 	max(nr_seq_item_serv), 
			max(material),
			max(procedimento)
		into	nr_codigo_serv_prest_w,
			cd_material_w,
			cd_procedimento_w
		from(	 
			select	nr_seq_item_serv,
				null material,
				i.cd_procedimento procedimento
			from	nota_fiscal_item i,
				procedimento p
			where	p.cd_procedimento = i.cd_procedimento
			and	p.ie_origem_proced = i.ie_origem_proced
			and	i.cd_estabelecimento = cd_estabelecimento_p
			and	i.nr_sequencia = nr_seq_nota_p
			and	p.nr_seq_item_serv is not null
			union all
			select	nr_seq_item_serv,
				p.cd_material material,
				null procedimento
			from	nota_fiscal_item i,
				material_fiscal p
			where	p.cd_material = i.cd_material
			and	i.cd_estabelecimento = cd_estabelecimento_p
			and	i.nr_sequencia = nr_seq_nota_p
			and	p.nr_seq_item_serv is not null);
	
	end if;

end if;

if cd_procedimento_w is not null then

	select	max(nr_seq_servico_item)
	into	cod_serv_prest_proc_w
	from   	procedimento_item_servico
	where  	cd_procedimento = cd_procedimento_w
	and	cd_estabelecimento = cd_estabelecimento_p;
	
	if cod_serv_prest_proc_w is not null then
		select	count(*)
		into	qt_possui_w
		from	grupo_servico_item
		where	nr_sequencia = cod_serv_prest_proc_w;

		if (qt_possui_w > 0) then

			select	max(nvl(b.cd_prefeitura, b.cd_item_servico)),
				max(nvl(b.ds_item_servico, 0)),
				max(nvl(a.cd_servico,0)),
				max(b.cd_item_servico)
			into	cd_item_servico_w,
				ds_item_servico_w,
				cd_grupo_w,
				cd_item_serv_mun_w
			from	grupo_servico a,
				grupo_servico_item b
			where	b.nr_sequencia =   cod_serv_prest_proc_w
			and    	a.nr_sequencia =    b.nr_seq_grupo_serv ;

			if (ie_opcao_p = 'C') then
				retorno_w:= cd_item_servico_w;
			elsif (ie_opcao_p = 'I') then	
				retorno_w:= cd_item_serv_mun_w;
			elsif (ie_opcao_p = 'D') then	
				retorno_w:= ds_item_servico_w;
			elsif (ie_opcao_p = 'G') then
				retorno_w:= cd_grupo_w;
			end if;
		end if;	
	end if;
end if;

if cd_material_w is not null then

	select	max(nr_seq_servico_item)
	into	cod_serv_prest_proc_w
	from   	fis_material_item_servico
	where  	cd_material = cd_material_w
	and	cd_estabelecimento = cd_estabelecimento_p;
	
	if cod_serv_prest_proc_w is not null then
		select	count(*)
		into	qt_possui_w
		from	grupo_servico_item
		where	nr_sequencia = cod_serv_prest_proc_w;

		if (qt_possui_w > 0) then

			select	max(nvl(b.cd_prefeitura, b.cd_item_servico)),
				max(nvl(substr(b.ds_item_servico, 1, 255), 0)),
				max(nvl(a.cd_servico,0)),
				max(b.cd_item_servico)
			into	cd_item_servico_w,
				ds_item_servico_w,
				cd_grupo_w,
				cd_item_serv_mun_w
			from	grupo_servico a,
				grupo_servico_item b
			where	b.nr_sequencia =   cod_serv_prest_proc_w
			and    	a.nr_sequencia =    b.nr_seq_grupo_serv ;

			if (ie_opcao_p = 'C') then
				retorno_w:= cd_item_servico_w;
			elsif (ie_opcao_p = 'I') then	
				retorno_w:= cd_item_serv_mun_w;
			elsif (ie_opcao_p = 'D') then	
				retorno_w:= ds_item_servico_w;
			elsif (ie_opcao_p = 'G') then
				retorno_w:= cd_grupo_w;
			end if;
		end if;	
	end if;
end if;

if retorno_w is null then

	if (nr_codigo_serv_prest_w is null) then

		if (ie_opcao_p = 'C') then
			retorno_w:= nr_ativ_w;
		else
			select	count(*)
			into	qt_possui_w
			from	grupo_servico_item
			where	cd_item_servico = nr_ativ_w;

			if (qt_possui_w > 0) then
			
				select	ds_item_servico
				into	retorno_w
				from	grupo_servico_item
				where	cd_item_servico = nr_ativ_w;			
			end if;			
		end if;		
	else

		select	count(*)
		into	qt_possui_w
		from	grupo_servico_item
		where	nr_sequencia = nr_codigo_serv_prest_w;

		if (qt_possui_w > 0) then
				
			select	max(nvl(b.cd_prefeitura, b.cd_item_servico)),
				max(nvl(substr(b.ds_item_servico, 1, 255), 0)),
				max(nvl(a.cd_servico,0)),
				max(b.cd_item_servico)
			into	cd_item_servico_w,
				ds_item_servico_w,
				cd_grupo_w,
				cd_item_serv_mun_w
			from	grupo_servico a,
				grupo_servico_item b
			where	b.nr_sequencia =   nr_codigo_serv_prest_w
			and    	a.nr_sequencia =    b.nr_seq_grupo_serv;

			if (ie_opcao_p = 'C') then
				retorno_w:= cd_item_servico_w;
			elsif (ie_opcao_p = 'I') then	
				retorno_w:= cd_item_serv_mun_w;
			elsif (ie_opcao_p = 'D') then	
				retorno_w:= ds_item_servico_w;
			elsif (ie_opcao_p = 'G') then
				retorno_w:= cd_grupo_w;
			end if;
		end if;			
	end if;
end if;	

return	retorno_w;

end obter_cod_grupo_ativ;
/