create or replace
function qt_obter_dose_total_ordem(cd_material_p	number,
				   nr_seq_ordem_p	number)
 		    	return number is
			
qt_retorno_w	number(15,3);

begin

if 	(cd_material_p is not null) and
	(nr_seq_ordem_p is not null) then
	
	select 	sum(a.qt_dose)
	into	qt_retorno_w
	from	material	b,
			can_ordem_item_prescr a
	where	a.cd_material	=	b.cd_material
	and		a.nr_seq_ordem 	=	nr_seq_ordem_p
	and		b.nr_seq_ficha_tecnica	in	substr((select	x.nr_seq_ficha_tecnica
						 from	material x
						 where	x.cd_material = cd_material_p),1,10);						 
end if;

return	qt_retorno_w;

end qt_obter_dose_total_ordem;
/
