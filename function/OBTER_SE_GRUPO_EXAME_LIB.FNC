create or replace
function obter_se_Grupo_exame_lib	(	nr_seq_grupo_p		number,
										nr_atendimento_p	number default null)
										return varchar2 is


qt_registro_lib_w	number(10);
ie_tipo_convenio_w	number(10);
ie_tipo_atendimento_w	number(10) := 0;
cd_especialidade_w      number(5) := 0;
cd_pessoa_fisica_w	varchar2(10);

begin

if	(nvl(nr_atendimento_p,0) > 0) then
	select	max(ie_tipo_convenio),
			max(ie_tipo_atendimento)
	into	ie_tipo_convenio_w,
			ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
end if;

select 	count(*)
into	qt_registro_lib_w
from	med_grupo_exame_lib
where	nr_seq_grupo	= 	nr_seq_grupo_p;

if	(qt_registro_lib_w	> 0) then
	
	cd_pessoa_fisica_w := nvl(Obter_Pf_Usuario(Obter_Usuario_Ativo,'C'),0);
	
	select 	count(*)
	into	qt_registro_lib_w
	from	med_grupo_exame_lib
	where	nr_seq_grupo	= 	nr_seq_grupo_p
	and		nvl(cd_perfil, obter_perfil_ativo)		=	obter_perfil_ativo
	and		((nvl(nr_atendimento_p,0) = 0) or (nvl(ie_tipo_convenio, nvl(ie_tipo_convenio_w, 0))	=	nvl(ie_tipo_convenio_w, 0)))
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)	= ie_tipo_atendimento_w
	and     ( (nvl(cd_especialidade,0) > 0 
	and     cd_especialidade in (select cd_especialidade 
				     from   medico_especialidade
				     where  cd_pessoa_fisica = cd_pessoa_fisica_w))
	or      nvl(cd_especialidade,0) = 0 );

	if	(qt_registro_lib_w	= 0) then
		return 'N';
	end if;
end if;

return	'S';

end obter_se_Grupo_exame_lib;
/