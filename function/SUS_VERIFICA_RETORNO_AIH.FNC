CREATE OR REPLACE
FUNCTION Sus_Verifica_Retorno_Aih
			(	nr_atendimento_p	Number)
				return Varchar2 is

nr_atend_original_w	Number(10);
qt_aih_retorno_w	Number(5);
ds_retorno_w		Varchar2(1)	:= 'N';

BEGIN

select	nvl(max(nr_atend_original),0)
into	nr_atend_original_w
from	atendimento_paciente
where	nr_atendimento		= nr_atendimento_p
and	ie_tipo_atendimento	= 1
and	ie_tipo_convenio	= 3;

select	count(*)
into	qt_aih_retorno_w
from	sus_aih
where	nr_atendimento	= nr_atend_original_w;

if	(qt_aih_retorno_w	> 0) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

END Sus_Verifica_Retorno_Aih;
/