create or replace
function obter_dados_cat_municipio(	cd_municipio_ibge_p		sus_municipio.cd_municipio_ibge%type,
					ie_tipo_inf_p			varchar2)
					return varchar2 is

ds_retorno_w			varchar2(255);
nr_seq_municipio_mx_w		sus_municipio.nr_seq_municipio_mx%type;
cd_cat_entidades_w		cat_municipio.cd_cat_entidades%type;
cd_cat_municipio_w		cat_municipio.cd_cat_municipio%type;
ds_cat_municipio_w		cat_municipio.ds_cat_municipio%type;

			
begin
if (cd_municipio_ibge_p is not null) then
	
	select 	max(nr_seq_municipio_mx)
	into	nr_seq_municipio_mx_w
	from 	sus_municipio
	where 	cd_municipio_ibge = cd_municipio_ibge_p;

	if 	(nr_seq_municipio_mx_w is not null) then
		select	cd_cat_entidades,
			cd_cat_municipio,
			ds_cat_municipio
		into	cd_cat_entidades_w,
			cd_cat_municipio_w,
			ds_cat_municipio_w
		from	cat_municipio
		where	nr_sequencia	= nr_seq_municipio_mx_w;

		if 	(ie_tipo_inf_p = 'CD_CAT_ENTIDADES') then
			ds_retorno_w 	:= cd_cat_entidades_w;
			
		elsif	(ie_tipo_inf_p = 'CD_CAT_MUNICIPIO') then
			ds_retorno_w 	:= cd_cat_municipio_w;
			
		elsif	(ie_tipo_inf_p = 'DS_CAT_MUNICIPIO') then
			ds_retorno_w 	:= ds_cat_municipio_w;
		end if;
	end if;
end if;

return	ds_retorno_w;

end obter_dados_cat_municipio;
/