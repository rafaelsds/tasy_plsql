CREATE OR REPLACE FUNCTION psa_is_config_parameter (
  function_code    IN NUMBER,
  parameter_code   IN NUMBER
) RETURN VARCHAR2 AS
  philips_security_parameter   CONSTANT NUMBER := 229;
BEGIN
  IF
    function_code = 0 AND
      parameter_code IN (
        philips_security_parameter
      )
  THEN
    RETURN 'TRUE';
  ELSE
    RETURN 'FALSE';
  END IF;
END psa_is_config_parameter;
/