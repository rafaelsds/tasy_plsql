CREATE OR REPLACE FUNCTION hd_obter_ult_maquina_prc (	cd_pessoa_fisica_p	varchar2)
								return varchar2 is

dt_viagem_w			date;
nr_seq_dialise_w		number(10,0);
nr_seq_dialise_dialisador_w	number(10,0);
nr_seq_maquina_w		number(10,0);

ds_retorno_w		varchar2(80);


Begin

select	dt_inicio
into	dt_viagem_w
from	hd_escala_dialise
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	dt_fim	is null;


select	max(nr_sequencia)
into	nr_seq_dialise_w
from	hd_dialise
where	cd_pessoa_fisica 	= cd_pessoa_fisica_p
and	dt_inicio_dialise	<= dt_viagem_w;


select	max(nr_sequencia)
into	nr_seq_dialise_dialisador_w
from	hd_dialise_dialisador
where	nr_seq_dialise		= nr_seq_dialise_w;

select	nr_seq_maquina
into	nr_seq_maquina_w
from	hd_dialise_dialisador
where	nr_sequencia		= nr_seq_dialise_dialisador_w;


select	cd_maquina || ' - ' || ds_maquina_dialise
into	ds_retorno_w
from    hd_maquina_dialise
where   nr_sequencia		= nr_seq_maquina_w;


return	ds_retorno_w;


End hd_obter_ult_maquina_prc;
/