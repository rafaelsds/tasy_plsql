create or replace
function obter_conta_contabil_material(	cd_estabelecimento_p	number,
				cd_material_p		number)
 		    	return varchar2 is

cd_conta_contabil_w	varchar2(20);
cd_centro_custo_w		number(10);
begin

define_conta_material(	cd_estabelecimento_p,
			cd_material_p,
			2,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			Null,
			sysdate,
			cd_conta_contabil_w,
			cd_centro_custo_w,
			null);



return	cd_conta_contabil_w;

end obter_conta_contabil_material;
/