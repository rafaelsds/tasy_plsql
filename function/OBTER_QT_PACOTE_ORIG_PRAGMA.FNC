create or replace
function obter_qt_pacote_orig_pragma(nr_sequencia_p	number,
			nr_atendimento_p		number,
			ie_pacote_p			Varchar2)
 		    	return number is

qt_pacote_orig_w	number(10,0)	:= 0;
qt_pacote_w		number(10,0)	:= 0;
qt_pacote_total_w	number(10,0)	:= 0;

pragma autonomous_transaction;
				
begin

if	(nvl(nr_sequencia_p,0) > 0) then

	if	(nvl(ie_pacote_p,'O') in ('O','T')) then
		select	count(*)
		into	qt_pacote_orig_w
		from	atendimento_pacote
		where	nr_seq_proc_origem = nr_sequencia_p
		and	nr_atendimento = nr_atendimento_p;
	end if;
	
	if	(nvl(ie_pacote_p,'O') in ('P','T')) then
		select	count(*)
		into	qt_pacote_w
		from	atendimento_pacote
		where	nr_seq_procedimento = nr_sequencia_p
		and	nr_atendimento = nr_atendimento_p;
	end if;
	
	qt_pacote_total_w	:= qt_pacote_orig_w + qt_pacote_w;

end if;


return	qt_pacote_total_w;

end obter_qt_pacote_orig_pragma;
/
