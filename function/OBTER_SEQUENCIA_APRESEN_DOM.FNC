create or replace
function obter_sequencia_apresen_dom	(cd_dominio_p		number,
					vl_dominio_p		varchar2)
 		    	return number is
			
nr_seq_apres_w	number(10);
begin
if	(cd_dominio_p is not null) and (vl_dominio_p is not null) then
	select	x.nr_seq_apresent
	into	nr_seq_apres_w
	from	valor_dominio x
	where	x.cd_dominio	= cd_dominio_p
	and	x.vl_dominio	= vl_dominio_p;
end if;

return	nr_seq_apres_w;

end obter_sequencia_apresen_dom;
/