create or replace
function ptu_obter_cid_anexo_trans
			(	nr_seq_pedido_anexo_p	Number,
				nr_registro_p		Number)
 		    	return Varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
nr_registros_w	pls_integer;
ds_retorno_w	varchar2(4);

Cursor C01 is
	select	cd_doenca
	from	ptu_pedido_aut_diag_anexo
	where	nr_seq_pedido_anexo	= nr_seq_pedido_anexo_p
	order by nr_sequencia;

begin
nr_registros_w	:= 1;

for r_c01_w in C01 loop
	if	(nr_registros_w	= nr_registro_p) then
		ds_retorno_w	:= r_c01_w.cd_doenca;
	end if;

	nr_registros_w	:= nr_registros_w + 1;
end loop;

return	ds_retorno_w;

end ptu_obter_cid_anexo_trans;
/