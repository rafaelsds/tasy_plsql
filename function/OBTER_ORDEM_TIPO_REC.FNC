create or replace
function Obter_ordem_tipo_rec(cd_tipo_recomendacao_p		number)
 		    	return number is

nr_seq_apres_relat_w	number(10,0);

begin

select	nvl(nr_seq_apres_relat,999)
into	nr_seq_apres_relat_w
from	tipo_recomendacao b
where	b.cd_tipo_recomendacao	= cd_tipo_recomendacao_p;

return	nr_seq_apres_relat_w;

end Obter_ordem_tipo_rec;
/
