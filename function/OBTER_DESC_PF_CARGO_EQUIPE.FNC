create or replace
function obter_desc_pf_cargo_equipe(nr_sequencia_p		number)
 		    	return varchar2 is

ds_cargo_equipe_w	pf_cargo_equipe.ds_cargo%type;	
			
begin

select	max(ds_cargo)
into	ds_cargo_equipe_w
from	pf_cargo_equipe
where	nr_sequencia = nr_sequencia_p;


return	ds_cargo_equipe_w;

end obter_desc_pf_cargo_equipe;
/