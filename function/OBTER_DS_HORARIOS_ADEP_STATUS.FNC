create or replace
function obter_ds_horarios_adep_status(	ie_tipo_item_p		varchar2,
										nr_seq_cpoe_p number,
										dt_inicio_vig_p		date,
										dt_fim_vig_p		date,
										cd_material_p		number default null)
	return varchar2 is
	
dt_horario_w	date;
dt_suspensao_w	date;
ds_retorno_w	varchar2(255);
qt_horarios_w	number(10) := 0;
ds_horarios_w	varchar2(255);
ds_status_w		varchar2(255);
ds_contagem_w	varchar2(255);

type cc01 is Ref Cursor;
c01 cc01;

begin

if (ie_tipo_item_p in ('M', 'MA', 'SOL')) then
	open c01 for
	select	b.dt_horario,
			b.dt_suspensao
	from   	prescr_material a,
			prescr_mat_hor b
	where  	a.nr_seq_mat_cpoe = nr_seq_cpoe_p
	and		a.cd_material = cd_material_p
	and		a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia = b.nr_seq_material
	and		b.dt_horario between dt_inicio_vig_p and dt_fim_vig_p
	order by b.dt_horario;
elsif (ie_tipo_item_p in ('E', 'S')) then
	open c01 for
	select	b.dt_horario,
			b.dt_suspensao
	from   	prescr_material a,
			prescr_mat_hor b
	where  	a.nr_seq_dieta_cpoe = nr_seq_cpoe_p
	and		a.cd_material = cd_material_p
	and		a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia = b.nr_seq_material
	and		b.dt_horario between dt_inicio_vig_p and dt_fim_vig_p
	order by b.dt_horario;
elsif (ie_tipo_item_p in ('O')) then
	open c01 for
	select	b.dt_horario,
			b.dt_suspensao
	from   	prescr_dieta a,
			prescr_dieta_hor b
	where  	a.nr_seq_dieta_cpoe = nr_seq_cpoe_p
	and		a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia = b.nr_seq_dieta
	and		b.dt_horario between dt_inicio_vig_p and dt_fim_vig_p
	order by b.dt_horario;	

elsif (ie_tipo_item_p = 'G') then	

	open c01 for
	select	b.dt_horario,
			b.dt_suspensao
	from   	prescr_gasoterapia a,
			prescr_gasoterapia_hor b
	where  	a.nr_seq_gas_cpoe = nr_seq_cpoe_p
	and		a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia = b.nr_seq_gasoterapia
	and		b.dt_horario between dt_inicio_vig_p and dt_fim_vig_p
	order by b.dt_horario;	
	
elsif (ie_tipo_item_p = 'R') then
	open c01 for
	select 	b.dt_horario,
			b.dt_suspensao
	from	prescr_recomendacao a,
			prescr_rec_hor b			
	where	a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia = b.nr_seq_recomendacao
	and		a.nr_seq_rec_cpoe = nr_seq_cpoe_p
	and		b.dt_horario between dt_inicio_vig_p and dt_fim_vig_p
	order by b.dt_horario;
	
elsif (ie_tipo_item_p = 'P') then
	open c01 for
	select 	b.dt_horario,
			b.dt_suspensao
	from	prescr_procedimento a,
			prescr_proc_hor b			
	where	a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia = b.nr_seq_procedimento
	and		a.nr_seq_proc_cpoe = nr_seq_cpoe_p
	and		b.dt_horario between dt_inicio_vig_p and dt_fim_vig_p
	order by b.dt_horario;
elsif (ie_tipo_item_p = 'H') then
	open c01 for
	select 	b.dt_horario,
			b.dt_suspensao
	from	prescr_procedimento a,
			prescr_proc_hor b,
			prescr_solic_bco_sangue c
	where	a.nr_prescricao = b.nr_prescricao
	and		a.nr_prescricao	= c.nr_prescricao
	and		a.nr_sequencia = b.nr_seq_procedimento
	and		c.nr_seq_hemo_cpoe = nr_seq_cpoe_p
	and		c.nr_sequencia = a.nr_seq_solic_sangue
	and		b.dt_horario between dt_inicio_vig_p and dt_fim_vig_p
	order by b.dt_horario;
end if;

loop
fetch c01 into
	dt_horario_w,
	dt_suspensao_w;
exit when c01%notfound;		
	qt_horarios_w := qt_horarios_w +1;
	
	ds_horarios_w := substr(ds_horarios_w ||to_char(dt_horario_w, 'hh24:mi')||';',1,255);
	ds_contagem_w := substr(ds_contagem_w||qt_horarios_w||';',1,255);
	
	if (dt_suspensao_w is not null) then
		ds_status_w := substr(ds_status_w||'K;',1,255);
	else 
		ds_status_w := substr(ds_status_w||'V;',1,255);
	end if;
end loop;
close c01;

if (qt_horarios_w > 0) then
	ds_retorno_w := substr(qt_horarios_w||'#'||qt_horarios_w||'#'||ds_retorno_w||ds_horarios_w||'#'||ds_contagem_w||'#'||ds_status_w,1,255);
end if;

return ds_retorno_w; 

end  obter_ds_horarios_adep_status;
/