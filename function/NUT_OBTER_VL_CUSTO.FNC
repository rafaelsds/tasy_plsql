create or replace
function Nut_obter_vl_custo(	nr_seq_prod_centro_p	Number)
				return Number is

vl_custo_w		Number(17,4) := 0;
nr_seq_prod_card_w	Number(10);
qt_refeicao_custo_w	Number(15);
qt_refeicao_total_w	Number(10);
vl_custo_total_w	Number(17,4);
				
begin

if (nr_seq_prod_centro_p is not null) then

	select	nr_seq_prod_card,
		nvl(qt_refeicao,0)
	into	nr_seq_prod_card_w,
		qt_refeicao_custo_w
	from	nut_ordem_prod_centro
	where	nr_sequencia = nr_seq_prod_centro_p;

	select	nvl(max(qt_refeicao),0),
		nvl(max(vl_custo),0)
	into	qt_refeicao_total_w,
		vl_custo_total_w
	from	nut_ordem_prod_card
	where	nr_sequencia = nr_seq_prod_card_w;
	
	if (qt_refeicao_total_w > 0) then
		vl_custo_w := (vl_custo_total_w / qt_refeicao_total_w) * qt_refeicao_custo_w;
	end if;

end if;

return	vl_custo_w;

end Nut_obter_vl_custo;
/