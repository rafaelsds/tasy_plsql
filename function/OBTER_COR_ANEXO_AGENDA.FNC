create or replace
function 	obter_cor_anexo_agenda(	nr_seq_status_p  number)
				return varchar2 is

ds_cor_w	varchar2(15);
begin

select	ds_cor
into
	ds_cor_w
from 	status_anexo_agenda
where	nr_sequencia = nr_seq_status_p;

return	ds_cor_w;

end	obter_cor_anexo_agenda;
/