create or replace function gqa_obter_tag_protocolos_json(cd_pessoa_fisica_p in varchar2, nr_atendimento_p in number, ie_tag_tipo_p varchar2)
  return varchar2 is

ds_retorno_w          varchar2(4000);
qtd number(10)        := 0;

cursor c01 is
  select 
          a.nr_sequencia nr_seq_protocolo,
          to_number(gqa_obter_protocolo_tag_atual(a.nr_sequencia, 'SEQUENCIA')) nr_seq_etapa,
          gqa_obter_protocolo_tag_atual(a.nr_sequencia, 'TAG') ds_tag,
          gqa_obter_protocolo_tag_atual(a.nr_sequencia, 'COR') ds_cor,
          gqa_obter_protocolo_tag_atual(a.nr_sequencia, 'NOME') ds_etapa,
          gqa_obter_protocolo_tag_atual(a.nr_sequencia, 'ICONE') ds_icon,
          gqa_obter_protocolo_tag_atual(a.nr_sequencia, 'PATH') ds_icon_path,
          nvl(gqa_obter_se_etapa_tag_lib(ie_tag_tipo_p, a.nr_sequencia), 'S') ie_mostrar_tag
  from gqa_protocolo_pac a
  where (a.nr_atendimento = nr_atendimento_p 
          or (nr_atendimento_p is null and a.cd_pessoa_fisica = cd_pessoa_fisica_p))
  and a.ie_situacao = 'A'
  and a.dt_liberacao is not null
  and a.dt_termino is null
  and a.dt_inativacao is null;

begin
 
  ds_retorno_w := '[';
  for item in c01 loop
    ds_retorno_w := ds_retorno_w || '{';
    ds_retorno_w := ds_retorno_w || '"NR_SEQ_PROTOCOLO":"' || item.nr_seq_protocolo || '"';
    ds_retorno_w := ds_retorno_w || ',"NR_SEQ_ETAPA":"' || item.nr_seq_etapa || '"';
    ds_retorno_w := ds_retorno_w || ',"DS_TAG":"' || item.ds_tag || '"';
    ds_retorno_w := ds_retorno_w || ',"DS_COR":"' || item.ds_cor || '"';
    ds_retorno_w := ds_retorno_w || ',"DS_ETAPA":"' || item.ds_etapa || '"';
    ds_retorno_w := ds_retorno_w || ',"DS_ICON":"' || item.ds_icon || '"';
    ds_retorno_w := ds_retorno_w || ',"DS_ICON_PATH":"' || item.ds_icon_path || '"';
    ds_retorno_w := ds_retorno_w || ',"IE_MOSTRAR_TAG":"' || item.ie_mostrar_tag || '"';
    ds_retorno_w := ds_retorno_w || '},';
    
    qtd := qtd + 1;
  end loop;
  
  if(qtd > 0) then --remover ultima virgula
    ds_retorno_w := SUBSTR(ds_retorno_w, 0, LENGTH(ds_retorno_w) - 1);
  end if;
  
  ds_retorno_w := ds_retorno_w || ']';

  return ds_retorno_w;
end gqa_obter_tag_protocolos_json;
/
