create or replace
function Obter_Desc_Hint_Funcao
		(cd_funcao_p		number)
		return varchar2 is

ds_hint_w	varchar2(60);


begin


select	max(ds_hint)
into	ds_hint_w
from	funcao
where	cd_funcao = cd_funcao_p;

return 	ds_hint_w;

end Obter_Desc_Hint_Funcao;
/