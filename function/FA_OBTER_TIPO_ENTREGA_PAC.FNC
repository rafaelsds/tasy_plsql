create or replace
function fa_obter_tipo_entrega_pac(nr_seq_paciente_pmc_p		number)
 		    	return varchar2 is
ie_tipo_entrega_w	varchar2(3);
begin
if	(nr_seq_paciente_pmc_p is not null) then

	select	decode(count(*),0,'N','PNC')
	into	ie_tipo_entrega_w
	from	fa_paciente_pmc a,
		fa_paciente_pmc_medic b,
		fa_medic_farmacia_amb c
	where	a.nr_sequencia = nr_seq_paciente_pmc_p
	and	a.nr_sequencia = b.nr_seq_fa_pmc
	and	c.nr_sequencia = b.nr_seq_medic_fa	
	and	nvl(c.ie_nutricao,'N') = 'S';

	if	(ie_tipo_entrega_w = 'N') then
		select	decode(count(*),0,'N','PMC')
		into	ie_tipo_entrega_w
		from	fa_paciente_pmc a,
			fa_paciente_pmc_medic b,
			fa_medic_farmacia_amb c
		where	a.nr_sequencia = nr_seq_paciente_pmc_p
		and	a.nr_sequencia = b.nr_seq_fa_pmc
		and	c.nr_sequencia = b.nr_seq_medic_fa		
		and	nvl(c.ie_medicamento_pmc,'N') = 'S';
	end if;

	if	(ie_tipo_entrega_w = 'N') then
		ie_tipo_entrega_w := 'PMC';
	end if;
else
	ie_tipo_entrega_w := 'N';	
end if;	
return	ie_tipo_entrega_w;

end fa_obter_tipo_entrega_pac;
/