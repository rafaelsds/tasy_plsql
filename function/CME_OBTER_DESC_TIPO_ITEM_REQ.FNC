create or replace 
function cme_obter_desc_tipo_item_req(nr_seq_conjunto_p	number,
				nr_seq_classif_p	number,
				nr_seq_classif_conj_p	number) 
			return varchar2 is

begin

if	(nr_seq_conjunto_p is not null) then
	begin
	return wheb_mensagem_pck.get_texto(445235);
	end;
elsif (nr_seq_classif_p is not null) then
	begin
	return wheb_mensagem_pck.get_texto(1040369);
	end;
elsif (nr_seq_classif_conj_p is not null) then
	begin
	return wheb_mensagem_pck.get_texto(1040366);
	end;
end if;

return '';

end cme_obter_desc_tipo_item_req;
/