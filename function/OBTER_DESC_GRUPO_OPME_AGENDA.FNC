create or replace
function obter_desc_grupo_opme_agenda(	nr_seq_opme_p in number)
					return varchar2 as

ds_retorno_w		varchar2(255) := '';
nr_seq_opme_w		number(10);

begin

begin
select	max(substr(b.ds_protocolo,1,255))
into	ds_retorno_w
from	agenda_pac_opme a,
	agenda_pac_opme_agrup b
where	b.nr_sequencia = a.nr_seq_agrup
and	a.nr_sequencia = nr_seq_opme_p;
exception
when others then
	ds_retorno_w := '';
end;	

return ds_retorno_w;

end obter_desc_grupo_opme_agenda;
/