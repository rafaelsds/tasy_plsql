create or replace
function obter_status_gas_anterior (nr_seq_gasoterapia_p	number,
									ie_opcao_p				varchar2)	return varchar2 is

/* **************
 C - C�digo
 D - Descri��o 
****************/
					
ie_status_w		varchar2(5);

begin

if (ie_opcao_p = 'C') then
	select	max(ie_evento)
	into	ie_status_w
	from	prescr_gasoterapia_evento a
	where	a.nr_seq_gasoterapia	= nr_seq_gasoterapia_p
	and		nvl(a.ie_evento, 'X')		<> 'V'
	and		nvl(a.ie_evento_valido,'S') = 'S'	
	and		a.nr_sequencia		= (	select	max(b.nr_sequencia)
									from	prescr_gasoterapia_evento b
									where	b.nr_seq_gasoterapia	= nr_seq_gasoterapia_p
									and		nvl(b.ie_evento, 'X')		<> 'V'
									and		nvl(b.ie_evento_valido,'S') = 'S');
elsif (ie_opcao_p  = 'CC') then
	select	max(ie_evento)
	into	ie_status_w
	from	prescr_gasoterapia_evento a
	where	a.nr_seq_gasoterapia	= nr_seq_gasoterapia_p
	and		nvl(a.ie_evento_valido,'S') = 'S'	
	and		a.nr_sequencia		= (	select	max(b.nr_sequencia)
									from	prescr_gasoterapia_evento b
									where	b.nr_seq_gasoterapia	= nr_seq_gasoterapia_p
									and		nvl(b.ie_evento_valido,'S') = 'S');
end if;

if	(nvl(ie_status_w,'0') <> '0') then
	if	(ie_opcao_p	= 'D') then
		ie_status_w	:= substr(obter_valor_dominio(2702, ie_status_w),1,255);
	end if;
end if;

return	ie_status_w;

end obter_status_gas_anterior;
/