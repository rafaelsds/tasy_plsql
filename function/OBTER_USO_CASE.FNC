create or replace
function obter_uso_case( nm_usuario_p varchar2 default null)
        return varchar2 is

ds_uso_case_w varchar2(1);

cd_estabelecimento_w estabelecimento.cd_estabelecimento%type;
cd_perfil_w perfil.cd_perfil%type;

begin

ds_uso_case_w := 'N';
cd_estabelecimento_w := obter_estabelecimento_ativo;
cd_perfil_w := obter_perfil_ativo;


select 	nvl(max(case when (ordem is not null) then ie_utiliza_episodio else 'N' end), 'N') ie_utiliza_episodio
into	ds_uso_case_w
from
    (select 
            case 
              when (cd_estabelecimento_w = cd_estabelecimento and cd_perfil_w = cd_perfil) then 1
              when (cd_estabelecimento is null and cd_perfil_w = cd_perfil) then 2
              when (cd_estabelecimento = cd_estabelecimento_w and cd_perfil is null) then 3
              when (cd_estabelecimento is null and cd_perfil is null) then 4
            end ordem, ie_utiliza_episodio, cd_estabelecimento, cd_perfil 
    from    parametros_episodio 
    order by ordem) 
where rownum < 2;


return	ds_uso_case_w;

end obter_uso_case;
/
