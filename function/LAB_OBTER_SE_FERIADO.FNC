create or replace
FUNCTION lab_obter_Se_Feriado(cd_estabelecimento_p    number,
				dt_parametro_p		date,
				ie_tipo_feriado_p	number)
				RETURN number IS

ie_feriado_w	integer;

BEGIN

if (nvl(ie_tipo_feriado_p,0) <> 0) then

	select	count(*)
	into	ie_feriado_w
	from	feriado
	where	cd_estabelecimento = cd_estabelecimento_p
	and	dt_feriado = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_parametro_p)
	and	nvl(ie_tipo_feriado,ie_tipo_feriado_p) = ie_tipo_feriado_p;
else
	select	count(*)
	into	ie_feriado_w
	from	feriado
	where	cd_estabelecimento = cd_estabelecimento_p
	and	dt_feriado = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_parametro_p);

end if;

RETURN ie_feriado_w;
END lab_obter_Se_Feriado;
/