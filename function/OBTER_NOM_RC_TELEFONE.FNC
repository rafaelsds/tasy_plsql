create or replace
function obter_nom_rc_telefone(cd_pessoa_fisica_p	varchar2,
								cd_cgc_p		varchar2,
								ie_opcao_p		varchar2) return varchar2 is
								
ds_telefone_w	varchar2(50);
nr_ddi_telefone_w	varchar2(30);
nr_ddd_telefone_w	varchar2(30);
nr_telefone_w		varchar2(30);
								
begin

if	(cd_pessoa_fisica_p is not null) then
	/* Find first by celphone */
	select	a.nr_ddi_celular,
			a.nr_ddd_celular,
			a.nr_telefone_celular
	into	nr_ddi_telefone_w,
			nr_ddd_telefone_w,
			nr_telefone_w
	from	pessoa_fisica a
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
	if	(nr_telefone_w is null) then
		select	max(a.nr_ddi_telefone),
				max(a.nr_ddd_telefone),
				max(a.nr_telefone)
		into	nr_ddi_telefone_w,
				nr_ddd_telefone_w,
				nr_telefone_w
		from	compl_pessoa_fisica a
		where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and		ie_tipo_complemento	= 1;
	end if;

elsif	(cd_cgc_p is not null) then
	select	a.nr_ddi_telefone,
			a.nr_ddd_telefone,
			a.nr_telefone
	into	nr_ddi_telefone_w,
			nr_ddd_telefone_w,
			nr_telefone_w
	from	pessoa_juridica a
	where	a.cd_cgc = cd_cgc_p;
end if;

if	(nr_ddi_telefone_w is not null) and
	(nr_telefone_w is not null) then
	ds_telefone_w	:= '+' || nr_ddi_telefone_w || nr_ddd_telefone_w || nr_telefone_w;
end if;

return ds_telefone_w;	

end obter_nom_rc_telefone;
/