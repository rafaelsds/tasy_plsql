create or replace 
function validar_medico_assistente (
	ds_lista_p varchar2,
	nr_atendimento_p number
) return number is

	ds_curr	varchar2(1000);
    
    cursor c1 is
    select  distinct b.cd_pessoa_fisica cd_medico
    from    pessoa_fisica c, medico b, prescr_medica a
    where   b.cd_pessoa_fisica = c.cd_pessoa_fisica
    and     a.cd_prescritor = b.cd_pessoa_fisica
    and     a.nr_atendimento = nr_atendimento_p
    and     b.ie_situacao = 'A'
    union
    select  distinct b.cd_pessoa_fisica cd_medico
    from    pessoa_fisica c, medico b, evolucao_paciente a
    where   b.cd_pessoa_fisica = c.cd_pessoa_fisica
    and     a.cd_medico = b.cd_pessoa_fisica
    and     a.nr_atendimento = nr_atendimento_p
    and     b.ie_situacao = 'A'
    union
    select  distinct b.cd_pessoa_fisica cd_medico
    from    pessoa_fisica c, medico b, atendimento_paciente a
    where   b.cd_pessoa_fisica = c.cd_pessoa_fisica
    and     a.cd_medico_referido = b.cd_pessoa_fisica
    and     a.nr_atendimento = nr_atendimento_p
    and     b.ie_situacao = 'A';
    
begin
    open c1;
    loop
        fetch c1 into ds_curr;
        exit when c1%notfound;
            begin
                if instr(ds_lista_p, ds_curr) > 0 then
                    return 1;
                end if;
            end;
    end loop;
    
    return 0;
	
end validar_medico_assistente;
/