create or replace
function Obter_cor_nivel_alta( dt_entada_p  date,
								dt_prev_alta_p  date)
						return varchar2 is
						
ds_cor_w  varchar2(255) := '';
qt_porcentagem_w number(15,2);
begin
begin
/*Converte para porcentagem*/
if	(dt_prev_alta_p < sysdate) then
	qt_porcentagem_w := 100;
end if;
if (dt_prev_alta_p > sysdate) then
 qt_porcentagem_w := (100 - trunc(dividir((round(dt_prev_alta_p - sysdate)*100),round(dt_prev_alta_p - dt_entada_p)),2));
end if;

if 	(qt_porcentagem_w = 0) then
	qt_porcentagem_w := 1;
end if;

select 	max(DS_COR)
into 	ds_cor_w
from 	REGRA_COR_PREV_ALTA
where 	qt_porcentagem_w between PR_INICIAL and PR_FINAL;
exception
	when others then
	null;
end;

return ds_cor_w;

end Obter_cor_nivel_alta;
/
