create or replace
function pls_obter_auditor_atual_fluxo
			(	nr_seq_analise_p		number,
				nr_Seq_grupo_atual_p		number)			
						return varchar2 is

nm_auditor_atual_w		varchar2(255):= ''; 
nr_seq_ordem_auditor_w		number(10);	
			
begin

if	( nvl(nr_seq_analise_p,0) > 0) and (nvl(nr_Seq_grupo_atual_p,0)>0) then	
	
	select	max(nm_auditor_atual)
	into	nm_auditor_atual_w
	from 	pls_auditoria_conta_grupo
	where	nr_seq_analise 	= nr_seq_analise_p
	and	nr_Seq_grupo 	= nr_Seq_grupo_atual_p
	and	nr_seq_ordem	= (	select  min(nr_seq_ordem)
					from 	pls_auditoria_conta_grupo
					where	nr_Seq_analise  = nr_seq_analise_p
					and	nr_Seq_grupo 	= nr_Seq_grupo_atual_p	
					and	nvl(ie_pre_analise,'N') = 'N'
					and	dt_liberacao is null);	
elsif	(nvl(nr_seq_analise_p,0) > 0) and (nvl(nr_Seq_grupo_atual_p,0) = 0) then
	select	max(nr_seq_ordem)
	into	nr_seq_ordem_auditor_w
	from 	pls_auditoria_conta_grupo
	where	nr_seq_analise 	= nr_seq_analise_p
	and	nr_seq_ordem	= (	select  max(nr_seq_ordem)
					from 	pls_auditoria_conta_grupo
					where	nr_Seq_analise  = nr_seq_analise_p
					and	nvl(ie_pre_analise,'N') = 'N'
					and	nm_auditor_atual is not null);

	if	(nvl(nr_seq_ordem_auditor_w,0) > 0) then
		select	max(nm_auditor_atual)
		into	nm_auditor_atual_w
		from	pls_auditoria_conta_grupo
		where	nr_seq_ordem = nr_seq_ordem_auditor_w
		and	nr_seq_analise	= nr_seq_analise_p
		and	nm_auditor_atual is not null;
	end if;
	
end if;

return	nm_auditor_atual_w;

end pls_obter_auditor_atual_fluxo;
/			
