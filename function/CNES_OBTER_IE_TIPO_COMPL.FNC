create or replace
function cnes_obter_ie_tipo_compl(nr_seq_ident_p	number)
 		    	return number is

ie_tipo_complemento_w	number(2);
			
begin

select	nvl(max(ie_tipo_complemento),1)
into	ie_tipo_complemento_w
from	compl_pessoa_fisica
where	nr_seq_ident_cnes = nr_seq_ident_p;

return	ie_tipo_complemento_w;

end cnes_obter_ie_tipo_compl;
/
