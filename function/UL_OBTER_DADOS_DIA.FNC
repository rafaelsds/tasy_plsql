create or replace
function UL_Obter_dados_dia	(dt_parametro_p		date,
				cd_pessoa_fisica_p	varchar2,
				ie_opcao_p		varchar2)
 		    	return number is

/*	ie_opcao_p = 
	A - Primeiro atendimento do dia
	P - Primeira prescrição do dia
*/			
		
nr_retorno_w	number(15,0);
		
begin

if	(ie_opcao_p	= 'A') then
	select	min(nr_atendimento)
	into	nr_retorno_w
	from	atendimento_paciente
	where	trunc(dt_entrada,'dd')	= trunc(dt_parametro_p,'dd')
	and	cd_pessoa_fisica	= nvl(cd_pessoa_fisica_p, cd_pessoa_fisica);
else	
	select	min(nr_prescricao)
	into	nr_retorno_w
	from	prescr_medica
	where	trunc(dt_prescricao,'dd') = trunc(dt_parametro_p,'dd')
	and	cd_pessoa_fisica	= nvl(cd_pessoa_fisica_p, cd_pessoa_fisica);
end if;

return	nr_retorno_w;

end UL_Obter_dados_dia;
/