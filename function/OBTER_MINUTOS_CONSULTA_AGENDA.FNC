create or replace
function Obter_Minutos_Consulta_Agenda
			(nr_seq_agenda_p	number)
			return number is

dt_inicio_consulta_w	date;
dt_saida_w		date;
retorno_w			Number(10);

begin

select	max(dt_inicio_consulta),
	nvl(max(dt_saida), sysdate)
into	dt_inicio_consulta_w,
	dt_saida_w
from	med_atendimento
where	nr_seq_agenda	= nr_seq_agenda_p;

if	(dt_inicio_consulta_w is not null) then
	begin
	select 	to_number(decode(dt_inicio_consulta_w,null, null, 
		round((nvl(dt_saida_w,sysdate) - dt_inicio_consulta_w) * 1440))) 
	into	retorno_w
	from 	dual;
	exception
		when others then
		retorno_w	:= 0;
	end;
end if;

return	retorno_w;

end Obter_Minutos_Consulta_Agenda;
/