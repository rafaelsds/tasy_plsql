create or replace
function pls_obter_mascara_relat_fat(	nr_seq_guia_arquivo_p		pls_fat_guia_arquivo.nr_sequencia%type,
					nr_seq_fatura_p			pls_fatura.nr_sequencia%type,
					nr_seq_regra_relat_p		pls_regra_arquivo_fat_item.nr_sequencia%type,
					ie_opcao_p			varchar2,
					qt_arquivo_p			number,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type) return varchar is
					
nm_mascara_relatorio_w		pls_regra_arquivo_fat_item.nm_mascara_relatorio%type;
nm_mascara_relat_inter_w	pls_regra_arquivo_fat_item.nm_mascara_relat_inter%type;
ds_mascara_w			pls_regra_arquivo_fat_item.nm_mascara_relatorio%type;
ds_retorno_w			varchar2(255);
qt_arquivo_w			pls_integer;
cd_unimed_origem_w		pls_congenere.cd_cooperativa%type;
nr_seq_pagador_w		pls_contrato_pagador.nr_sequencia%type;
vl_fat_total_w			pls_fatura.vl_fatura%type;
dt_ano_referencia_w		varchar2(4);
dt_mes_referencia_w		varchar2(2);
nr_fatura_w			ptu_fatura.nr_fatura%type;
nr_nota_credito_debito_w	ptu_fatura.nr_nota_credito_debito%type;
nr_seq_contrato_w		pls_contrato.nr_sequencia%type;
nr_seq_intercambio_w		pls_intercambio.nr_sequencia%type;
nr_contrato_w			pls_contrato.nr_contrato%type;
ie_tipo_guia_w			pls_fat_guia_arquivo.ie_tipo_guia%type;
ie_tipo_guia_fund_w		pls_fat_guia_arquivo.ie_tipo_guia%type;
qt_caracter_w			pls_integer;
ds_caracter_w			varchar2(255);
ds_valor_w			varchar2(255);
nr_titulo_w			pls_fatura.nr_titulo%type;

begin

if	(nr_seq_regra_relat_p is not null) and (ie_opcao_p in ('B', 'I')) then
	select	upper(trim(nm_mascara_relatorio)),
		upper(trim(nm_mascara_relat_inter))
	into	nm_mascara_relatorio_w,
		nm_mascara_relat_inter_w
	from	pls_regra_arquivo_fat_item
	where	nr_sequencia = nr_seq_regra_relat_p;
	
	if	(ie_opcao_p = 'B') and (nm_mascara_relatorio_w is not null) then
		ds_mascara_w	:= nm_mascara_relatorio_w;
	elsif	(ie_opcao_p = 'I') and (nm_mascara_relat_inter_w is not null) then
		ds_mascara_w	:= nm_mascara_relat_inter_w;
	end if;
	
	if	(nr_seq_fatura_p is not null) and (ds_mascara_w is not null) then
		ds_retorno_w 		:= ds_mascara_w;
		qt_arquivo_w 		:= qt_arquivo_p;
		cd_unimed_origem_w 	:= pls_obter_unimed_estab(cd_estabelecimento_p);
		
		select	max(b.nr_seq_pagador),
			nvl(max(b.vl_fatura), 0) + nvl(max(b.vl_total_ndc), 0),
			to_char(max(a.dt_mesano_referencia), 'yyyy'),
			to_char(max(a.dt_mesano_referencia), 'mm'),
			max(b.nr_titulo)
		into	nr_seq_pagador_w,
			vl_fat_total_w,
			dt_ano_referencia_w,
			dt_mes_referencia_w,
			nr_titulo_w
		from	pls_fatura b,
			pls_lote_faturamento	a
		where	a.nr_sequencia		= b.nr_seq_lote
		and	b.nr_sequencia		= nr_seq_fatura_p;
		
		select	max(nr_fatura),
			max(nr_nota_credito_debito)
		into	nr_fatura_w,
			nr_nota_credito_debito_w
		from	ptu_fatura
		where	nr_seq_pls_fatura = nr_seq_fatura_p;
		
		nr_fatura_w := nvl(nvl(nr_fatura_w, to_char(nr_titulo_w)), to_char(nr_seq_fatura_p));
		
		/*select	max(nr_seq_contrato),
			max(nr_seq_intercambio)
		into	nr_seq_contrato_w,
			nr_seq_intercambio_w
		from	pls_segurado
		where	nr_seq_pagador = nr_seq_pagador_w;
		
		if	(nr_seq_contrato_w is not null) then
			select	max(nr_contrato)
			into	nr_contrato_w
			from	pls_contrato
			where	nr_sequencia = nr_seq_contrato_w;
		end if;
		
		nr_contrato_w	:= nvl(nvl(nr_contrato_w, nr_seq_intercambio_w), nr_seq_contrato_w);
		
		select	max(a.ie_tipo_guia),						-- Regra Tasy | 3 - Consulta | 4 -SADT | 5 - Interna��o | 6 - Honor�rio Individual
			decode(max(a.ie_tipo_guia), '3', '1', '4', '2', '5', '3', '6', '4')	-- Regra estipulada pela Federa��o SC | 1 - Consulta | 2 -SADT | 3 - Interna��o | 4 - Honor�rio Individual
		into	ie_tipo_guia_w,
			ie_tipo_guia_fund_w
		from	pls_fat_guia_arquivo	a
		where	a.nr_sequencia		= nr_seq_guia_arquivo_p
		and	a.nr_seq_lote 		= (	select	max( b.nr_sequencia )
							from	pls_lote_fat_guia_envio b
							where	b.nr_seq_fatura 	= nr_seq_fatura_p);
							
		if	(ds_mascara_w like '%C%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'C'));
			ds_caracter_w	:= lpad('C', qt_caracter_w, 'C');
			
			ds_valor_w	:= substr(nvl(nr_contrato_w, 0), 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;
		
		if	(ds_mascara_w like '%V%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'V'));
			ds_caracter_w	:= lpad('V', qt_caracter_w, 'V');
			
			ds_valor_w	:= substr(nvl(elimina_caractere_especial(vl_fat_total_w), 0), 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;
		
		if	(ds_mascara_w like '%A%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'A'));
			ds_caracter_w	:= lpad('A', qt_caracter_w, 'A');
			
			ds_valor_w	:= substr(dt_ano_referencia_w, 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;
		
		if	(ds_mascara_w like '%M%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'M'));
			ds_caracter_w	:= lpad('M', qt_caracter_w, 'M');
			
			ds_valor_w	:= substr(dt_mes_referencia_w, 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;*/
		
		if	(ds_mascara_w like '%F%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'F'));
			ds_caracter_w	:= lpad('F', qt_caracter_w, 'F');
			
			ds_valor_w	:= substr(nvl(nr_fatura_w, 0), 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;
		
		/*if	(ds_mascara_w like '%S%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'S'));
			ds_caracter_w	:= lpad('S', qt_caracter_w, 'S');
			
			ds_valor_w	:= substr(nvl(qt_arquivo_w, 1), 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;
		
		if	(ds_mascara_w like '%U%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'U'));
			ds_caracter_w	:= lpad('U', qt_caracter_w, 'U');
			
			ds_valor_w	:= substr(somente_numero(cd_unimed_origem_w), 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;
		
		if	(ds_mascara_w like '%N%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'N'));
			ds_caracter_w	:= lpad('N', qt_caracter_w, 'N');
			
			ds_valor_w	:= substr(somente_numero(pls_obter_versao_tiss), 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;
		
		if	(ds_mascara_w like '%T%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'T'));
			ds_caracter_w	:= lpad('T', qt_caracter_w, 'T');
			
			ds_valor_w	:= substr(somente_numero(ie_tipo_guia_w), 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;
		
		if	(ds_mascara_w like '%G%') then
			qt_caracter_w	:= length(ds_mascara_w) - length(replace(ds_mascara_w, 'G'));
			ds_caracter_w	:= lpad('G', qt_caracter_w, 'G');
			
			ds_valor_w	:= substr(somente_numero(ie_tipo_guia_fund_w), 1, qt_caracter_w);
			ds_valor_w	:= lpad(ds_valor_w, qt_caracter_w, '0');
			
			ds_retorno_w	:= replace(ds_retorno_w, ds_caracter_w, ds_valor_w);
		end if;*/
	end if;
end if;

if	(ds_retorno_w is not null) then
	ds_retorno_w	:= ds_retorno_w || '.pdf';
end if;

return ds_retorno_w;

end pls_obter_mascara_relat_fat;
/