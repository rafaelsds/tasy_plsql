create or replace function obter_qtd_sla_status(
		nr_seq_gerencia_p	number,
		nr_seq_grupo_p		number,
		ie_tipo_sla_p		varchar2,
		ie_abrangencia_p	varchar2,
		ie_tipo_grupo_p		varchar2)
	return number
is

qt_sla_w	number := - 1;

/* ie_tipo_sla_p
'G' =	Good
'W' =	Warning
'C' =	Critical
'B' =	Breached

ie_abrangencia
'GER' = Gerencia
'GRU' = Grupo

ie_tipo_grupo_p
'DEV' Desenvolvimento
'SUP' Suporte

Function para pegar a quantidade de slas (de um tipo especifico) de um grupo ou 
gerencia, do desenvolvimento ou suporte
*/

begin
	if	(ie_tipo_grupo_p = 'SUP') then
		if	(	ie_abrangencia_p = 'GER'
			and	nr_seq_gerencia_p is not null
			and	ie_tipo_sla_p in ('G','W','C','B')) then
			
			select	count(*)
			into	qt_sla_w
			from	man_estagio_processo c,
				man_ordem_servico_v a,
				man_ordem_serv_sla s,
				grupo_suporte g
			where	a.nr_sequencia = s.nr_seq_ordem
			and	c.nr_sequencia = a.nr_seq_estagio
			and	a.nr_seq_grupo_sup = g.nr_sequencia
			and	g.nr_seq_gerencia_sup =	nr_seq_gerencia_p
			and	nvl(c.ie_suporte, 'N') = 'S'
			and	s.nr_seq_status <> 412
			and	a.ie_status_ordem <> '3'
			and	obter_periodo_os(a.nr_sequencia) = ie_tipo_sla_p;	
		end if;
	
		if	(	ie_abrangencia_p = 'GRU'
			and	nr_seq_grupo_p is not null
			and	ie_tipo_sla_p in ('G','W','C','B')) then
			
			select	count(*)
			into	qt_sla_w
			from	man_estagio_processo c,
				man_ordem_servico_v a,
				man_ordem_serv_sla s,
				grupo_suporte g
			where	a.nr_sequencia = s.nr_seq_ordem
			and	c.nr_sequencia = a.nr_seq_estagio
			and	a.nr_seq_grupo_sup = g.nr_sequencia
			and	a.nr_seq_grupo_sup = nr_seq_grupo_p
			and	nvl(c.ie_suporte, 'N') = 'S'
			and	s.nr_seq_status <> 412
			and	a.ie_status_ordem <> '3'
			and	obter_periodo_os(a.nr_sequencia) = ie_tipo_sla_p;
		end if;
	end if;

	if	(ie_tipo_grupo_p = 'DEV') then
		if	(	ie_abrangencia_p = 'GER'
			and	nr_seq_gerencia_p is not null
			and	ie_tipo_sla_p in ('G','W','C','B')) then		
			
			select	count(*)
			into	qt_sla_w
			from
				(
					select	a.nr_sequencia,
						d.nr_seq_gerencia,
						a.nr_seq_estagio,
						obter_periodo_os(a.nr_sequencia) ds_periodo_os,
						b.nr_seq_status
					from	man_ordem_servico_v a,
						man_ordem_serv_sla b,
						grupo_desenvolvimento d
					where	a.nr_sequencia = b.nr_seq_ordem
					and	(b.ie_tipo_sla <> 3 -- quando a OS e tipo sla 3 e tipo sla termino 3, deve ser contabilizada como defeito
						or	b.ie_tipo_sla_termino <> 3)
					and	a.nr_seq_grupo_des = d.nr_sequencia
					and	b.nr_seq_status <> 412
					and	substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) is not null
					and	obter_se_os_pend_cliente(b.nr_seq_ordem) = 'N'
					and	obter_se_os_desenv(b.nr_seq_ordem) = 'S'
					and	ie_status_ordem <> 3
					and	not exists
						(
							select	1
							from	man_estagio_processo x
							where	x.nr_sequencia = a.nr_seq_estagio
							and	nvl(x.ie_testes, 'N') = 'S'
						)
					union all
					select	a.nr_sequencia,
						d.nr_seq_gerencia,
						a.nr_seq_estagio,
						substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) ds_periodo_os,
						null
					from	man_ordem_servico_v a,
						grupo_desenvolvimento d
					where	a.nr_seq_grupo_des = d.nr_sequencia
					and	a.ie_classificacao = 'E'
					and	not exists
						(
							select	1
							from	man_ordem_serv_sla b
							where	a.nr_sequencia = b.nr_seq_ordem
						)
					and	obter_se_os_pend_cliente(a.nr_sequencia) = 'N'
					and	obter_se_os_desenv(a.nr_sequencia) = 'S'
					and	substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) is not null
					and	ie_status_ordem <> 3
					and	not exists
						(
							select	1
							from	man_estagio_processo x
							where	x.nr_sequencia = a.nr_seq_estagio
							and	nvl(x.ie_testes, 'N') = 'S'
						)
					union all
					select	a.nr_sequencia,
						d.nr_seq_gerencia,
						a.nr_seq_estagio,
						substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) ds_periodo_os,
						null
					from	man_ordem_servico_v a,
						grupo_desenvolvimento d,
						desenv_acordo_os b,
						desenv_acordo c
					where	b.nr_seq_ordem_servico = a.nr_sequencia
					and	a.nr_seq_grupo_des = d.nr_sequencia
					and	c.nr_sequencia = b.nr_seq_acordo
					and	a.ie_status_ordem <> 3
					and	c.dt_fim_acordo is null
					and	b.ie_status_acordo = 'A'
					and	obter_se_os_desenv(a.nr_sequencia) = 'S'
					and	substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) is not null
				) os
			where	os.nr_seq_status > 0
			and	nr_seq_gerencia = nr_seq_gerencia_p
			and	ds_periodo_os = ie_tipo_sla_p;
		end if;
	
		if	(	ie_abrangencia_p = 'GRU'
			and	nr_seq_grupo_p is not null
			and	ie_tipo_sla_p in ('G','W','C','B')) then
	
			select	count(*)
			into	qt_sla_w
			from
				(
					select	a.nr_sequencia,
						d.nr_sequencia nr_seq_grupo,
						d.nr_seq_gerencia,
						a.nr_seq_estagio,
						substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) ds_periodo_os,
						b.nr_seq_status
					from	man_ordem_servico_v a,
						man_ordem_serv_sla b,
						grupo_desenvolvimento d
					where	a.nr_sequencia = b.nr_seq_ordem
					and	(b.ie_tipo_sla <> 3 -- quando a OS e tipo sla 3 e tipo sla termino 3, deve ser contabilizada como defeito
						or	b.ie_tipo_sla_termino <> 3)
					and	a.nr_seq_grupo_des = d.nr_sequencia
					and	d.ie_situacao = 'A'
					and	d.ie_gerencia = 'N'
					and	b.nr_seq_status <> 412
					and	substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) is not null
					and	obter_se_os_pend_cliente(b.nr_seq_ordem) = 'N'
					and	obter_se_os_desenv(b.nr_seq_ordem) = 'S'
					and	ie_status_ordem <> 3
					and	not exists
						(
							select	1
							from	man_estagio_processo x
							where	x.nr_sequencia = a.nr_seq_estagio
							and	nvl(x.ie_testes, 'N') = 'S'
						)
					union all
					select	a.nr_sequencia,
						d.nr_sequencia nr_seq_grupo,
						d.nr_seq_gerencia,
						a.nr_seq_estagio,
						substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) ds_periodo_os,
						null
					from	man_ordem_servico_v a,
						grupo_desenvolvimento d
					where	a.nr_seq_grupo_des = d.nr_sequencia
					and	d.ie_situacao = 'A'
					and	d.ie_gerencia = 'N'
					and	a.ie_classificacao = 'E'
					and	not exists
						(
							select	1
							from	man_ordem_serv_sla b
							where	a.nr_sequencia = b.nr_seq_ordem
						)
					and	obter_se_os_pend_cliente(a.nr_sequencia) = 'N'
					and	obter_se_os_desenv(a.nr_sequencia) = 'S'
					and	substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) is not null
					and	ie_status_ordem <> 3
					and	not exists
						(
							select	1
							from	man_estagio_processo x
							where	x.nr_sequencia = a.nr_seq_estagio
							and	nvl(x.ie_testes, 'N') = 'S'
						)
					union all
					select	a.nr_sequencia,
						d.nr_sequencia nr_seq_grupo,
						d.nr_seq_gerencia,
						a.nr_seq_estagio,
						substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) ds_periodo_os,
						null
					from	man_ordem_servico_v a,
						grupo_desenvolvimento d,
						desenv_acordo_os b,
						desenv_acordo c
					where	b.nr_seq_ordem_servico = a.nr_sequencia
					and	a.nr_seq_grupo_des = d.nr_sequencia
					and	d.ie_situacao = 'A'
					and	d.ie_gerencia = 'N'
					and	c.nr_sequencia = b.nr_seq_acordo
					and	a.ie_status_ordem <> 3
					and	c.dt_fim_acordo is null
					and	b.ie_status_acordo = 'A'
					and	obter_se_os_desenv(a.nr_sequencia) = 'S'
					and	substr(sla_dashboard_pck.obter_status_resp(a.nr_sequencia), 1, 1) is not null
				) os
			where	os.nr_seq_status > 0
			and	nr_seq_grupo = nr_seq_grupo_p
			and	ds_periodo_os = ie_tipo_sla_p;
		end if;
	end if;

	return qt_sla_w;

end obter_qtd_sla_status;
/
