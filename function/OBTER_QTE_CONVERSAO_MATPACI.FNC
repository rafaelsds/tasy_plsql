create or replace
function obter_qte_conversao_matpaci (nr_seq_matpaci_p		number)
							return varchar2 is
				
ds_conversao_unidade_w	varchar2(100);
tx_conversao_qtde_w		number(9,4);
qt_material_w			number(17,4);
cd_unidade_medida_w     varchar2(30);

begin
begin
select	nvl(max(tx_conversao_qtde),1),
	nvl(max(cd_unidade_medida),' ')
into	tx_conversao_qtde_w,
	cd_unidade_medida_w
from	mat_atend_pac_convenio
where	nr_seq_material = nr_seq_matpaci_p;

select	nvl(max(qt_material),0)
into	qt_material_w
from	material_atend_paciente
where	nr_sequencia = nr_seq_matpaci_p;

qt_material_w := qt_material_w * tx_conversao_qtde_w;

--ds_conversao_unidade_w := campo_mascara_virgula(qt_material_w) || ' ' || cd_unidade_medida_w;  --Retirado 331716
ds_conversao_unidade_w := campo_mascara_virgula(qt_material_w);
exception
when others then
	ds_conversao_unidade_w:= '';
end;
return	ds_conversao_unidade_w;

end obter_qte_conversao_matpaci;
/