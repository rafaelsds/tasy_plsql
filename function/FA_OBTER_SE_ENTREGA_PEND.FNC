create or replace
function fa_obter_se_entrega_pend(cd_pessoa_fisica_p		varchar2,
								  nr_sequencia_p			number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);
				
begin

if (cd_pessoa_fisica_p is not null) then

	select  decode(count(*),0,'N','S')
	into	ie_retorno_w
	from    FA_entrega_medicacao a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and		a.nr_sequencia <> nr_sequencia_p
	and		a.ie_status_medicacao <> 'EM'
	and		a.dt_cancelamento is null;
end if;

return	ie_retorno_w;

end fa_obter_se_entrega_pend;
/