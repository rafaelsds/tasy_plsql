create or replace 
function obter_nome_paciente_agenda(	nr_seq_agenda_p	number)
					return varchar2 is

nm_paciente_w	varchar2(60);

begin

begin
select	nm_paciente
into	nm_paciente_w
from	agenda_paciente
where	nr_sequencia	=	nr_seq_agenda_p;
exception
	when others then
		nm_paciente_w	:= null;
end;

if	(nm_paciente_w is null) then
	select	max(obter_nome_pf(cd_pessoa_fisica))
	into	nm_paciente_w
	from	agenda_paciente
	where	nr_sequencia	=	nr_seq_agenda_p;
end if;

return	nm_paciente_w;

end obter_nome_paciente_agenda;
/
