create or replace
function obter_prescr_oncologia_atend(	nr_seq_atendimento_p	number )
					return number is

nr_prescricao_w	number(14);

begin

select	max(nr_prescricao)
into	nr_prescricao_w
from	paciente_atendimento
where	nr_seq_atendimento = nr_seq_atendimento_p;

return	nr_prescricao_w;

end obter_prescr_oncologia_atend;
/