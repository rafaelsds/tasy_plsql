create or replace
function Obter_Se_Modalidade_lib	(	ie_modalidade_trat_p	varchar2,
						nr_seq_loco_regional_p	number)
 		    	return varchar2 is
cd_topografia_w		varchar2(10);
cd_estadiamento_w	varchar2(10);
ie_retorno_w		varchar2(10)	:= 'S';
qt_reg_w		number(10);
CD_ESTADIO_OUTRO_W	varchar2(10);
begin

select	count(*)
into	qt_reg_w
from	REGRA_MODALIDADE_ONC;


if	(nr_seq_loco_regional_p	is not null) and
	(qt_reg_w	> 0)then
	select	max(cd_topografia),
		max(cd_estadiamento),
		max(CD_ESTADIO_OUTRO)
	into	cd_topografia_w,
		cd_estadiamento_w,
		CD_ESTADIO_OUTRO_W
	from	can_loco_regional
	where	nr_sequencia	= nr_seq_loco_regional_p;
	
	if	(cd_topografia_w	is not null) and
		(cd_estadiamento_w	is not null) and
		(CD_ESTADIO_OUTRO_W	is null)then
		select	count(*)
		into	qt_reg_w
		from	REGRA_MODALIDADE_ONC
		where	cd_topografia	= cd_topografia_w
		and	cd_estadiamento	= cd_estadiamento_w;
		
		if	(qt_reg_w		> 0) then
			select	count(*)
			into	qt_reg_w
			from	REGRA_MODALIDADE_ONC
			where	cd_topografia	= cd_topografia_w
			and	cd_estadiamento	= cd_estadiamento_w
			and	ie_modalidade_trat	= ie_modalidade_trat_p;
			
			if	(qt_reg_w	= 0) then
				ie_retorno_w	:= 'N';
			end if;
		end if;
	end if;
	
	
	if	(CD_ESTADIO_OUTRO_W	is not null) and
		(cd_topografia_w	is not null) then
		select	count(*)
		into	qt_reg_w
		from	REGRA_MODALIDADE_ONC
		where	cd_topografia	= cd_topografia_w
		and	cd_estadiamento	= CD_ESTADIO_OUTRO_W;
		
		if	(qt_reg_w		> 0) then
			select	count(*)
			into	qt_reg_w
			from	REGRA_MODALIDADE_ONC
			where	cd_topografia	= cd_topografia_w
			and	cd_estadiamento	= CD_ESTADIO_OUTRO_W
			and	ie_modalidade_trat	= ie_modalidade_trat_p;
			
			if	(qt_reg_w	= 0) then
				ie_retorno_w	:= 'N';
			end if;
		end if;
	end if;

end if;



return	ie_retorno_w;

end Obter_Se_Modalidade_lib;
/