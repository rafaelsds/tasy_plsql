create or replace
function Verifica_exame_duplic_dia(	nr_seq_exame_p			number,
									cd_procedimento_p		number,
									dt_prev_execucao_p		date,
									nr_atendimento_p		number,
									nr_prescricao_p			number,
									nr_seq_procedimento_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255) := '';
begin

if	(nr_atendimento_p is not null) and
	(nr_seq_exame_p is not null) and
	(cd_procedimento_p is not null) and
	(dt_prev_execucao_p is not null) then
	
	select	substr(Obter_Desc_Exame(max(b.nr_seq_exame)),1,200)
	into	ds_retorno_w
	from	prescr_procedimento b,
			prescr_medica a
	where	b.cd_procedimento = cd_procedimento_p
	and		b.nr_seq_exame = nr_seq_exame_p
	and		trunc(b.dt_prev_execucao) = trunc(dt_prev_execucao_p)
	and		((b.nr_sequencia <> nvl(nr_seq_procedimento_p,0)) or
			 (b.nr_prescricao <> nvl(nr_prescricao_p,0)))
	and		b.dt_suspensao is null
	and		a.nr_prescricao = b.nr_prescricao
	and		a.dt_suspensao is null
	and		dt_prev_execucao_p between a.dt_inicio_prescr and a.dt_validade_prescr
	and		a.nr_atendimento = nr_atendimento_p;
	
	if	(ds_retorno_w is not null) then
		--O exame #@DS_EXAME#@ j� foi prescrito neste dia para o paciente!
		ds_retorno_w	:= substr(obter_texto_dic_objeto(219312, 1, 'DS_EXAME='||ds_retorno_w||';'),1,255);
	end if;

end if;

return	ds_retorno_w;

end Verifica_exame_duplic_dia;
/