create or replace
function obter_dias_entrega_item_contr(	cd_material_p		number,
					cd_estabelecimento_p	number)
return varchar2 is

qt_dias_entrega_w		number(5);
nr_seq_contrato_w		number(10);
ie_estoque_w			varchar2(1);

begin

if	(cd_material_p is not null) then

	select 	(max(obter_valor_param_usuario(913, 185, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento)))
	into	ie_estoque_w
	from 	dual;

	select	nvl(max(nr_seq_contrato),0)
	into	nr_seq_contrato_w
	from   (select	a.nr_Seq_contrato
		from	contrato_regra_nf a,
			contrato b
		where	a.nr_seq_contrato = b.nr_sequencia
		and	(((ie_estoque_w = 'N') and (nvl(a.cd_material, cd_material_p) = cd_material_p)) or
			((ie_estoque_w = 'S') and (obter_dados_material(a.cd_material,'EST') = obter_dados_material(cd_material_p,'EST'))))
		and	substr(obter_dados_pf_pj(null,b.cd_cgc_contratado,'S'),1,1) = 'A'
		and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
		and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
		and	cd_pessoa_contratada is null
		and	nvl(b.ie_situacao,'A') = 'A'
		union all
		select	a.nr_seq_contrato
		from	contrato_regra_nf a,
			contrato b
		where	a.nr_seq_contrato = b.nr_sequencia
		and	(((ie_estoque_w = 'N') and (nvl(a.cd_material, cd_material_p) = cd_material_p)) or
			((ie_estoque_w = 'S') and (obter_dados_material(a.cd_material,'EST') = obter_dados_material(cd_material_p,'EST'))))
		and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
		and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
		and	cd_pessoa_contratada is not null
		and	nvl(b.ie_situacao,'A') = 'A');

		if	(nr_seq_contrato_w > 0) then
		
			select	nvl(max(qt_dias_entrega),0)
			into	qt_dias_entrega_w
			from	contrato_estab_adic
			where	nr_seq_contrato = nr_seq_contrato_w
			and	cd_estab_adic = cd_estabelecimento_p;
			
			if	(qt_dias_entrega_w = 0) then
				
				select	max(qt_dias_entrega)
				into	qt_dias_entrega_w
				from   (select	a.qt_dias_entrega
					from	contrato_regra_nf a,
						contrato b
					where	a.nr_seq_contrato = b.nr_sequencia
					and	(((ie_estoque_w = 'N') and (nvl(a.cd_material, cd_material_p) = cd_material_p)) or
						((ie_estoque_w = 'S') and (obter_dados_material(a.cd_material,'EST') = obter_dados_material(cd_material_p,'EST'))))
					and	substr(obter_dados_pf_pj(null,b.cd_cgc_contratado,'S'),1,1) = 'A'
					and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
					and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
					and	cd_pessoa_contratada is null
					and	nvl(b.ie_situacao,'A') = 'A'
					union all
					select	a.qt_dias_entrega
					from	contrato_regra_nf a,
						contrato b
					where	a.nr_seq_contrato = b.nr_sequencia
					and	(((ie_estoque_w = 'N') and (nvl(a.cd_material, cd_material_p) = cd_material_p)) or
						((ie_estoque_w = 'S') and (obter_dados_material(a.cd_material,'EST') = obter_dados_material(cd_material_p,'EST'))))
					and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
					and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
					and	cd_pessoa_contratada is not null
					and	nvl(b.ie_situacao,'A') = 'A');
			
			end if;		
		end if;	
end if;

return	qt_dias_entrega_w;

end	obter_dias_entrega_item_contr;
/
