create or replace
function obter_qtde_agenda_tipo_dia( 	dt_agenda_p date,
										ie_tipo_p	VARCHAR2)
 		    	RETURN Number IS

nr_retorno_w	number(1);

BEGIN
if (ie_tipo_p = 'P') then	
	begin
		select	1	
		into	nr_retorno_w
		from 	dual
		where	pkg_date_utils.get_DiffDate(SYSDATE, dt_agenda_p, 'DAY') < 0;
	exception
	when others then
		nr_retorno_w := 0;
	end;
elsif(ie_tipo_p = 'F') then
	begin
		select	1	
		into	nr_retorno_w
		from 	dual
		where	pkg_date_utils.get_DiffDate(SYSDATE, dt_agenda_p, 'DAY') > 0;
	exception
	when others then
		nr_retorno_w := 0;
	end;
elsif(ie_tipo_p = 'D') then
	begin
		select	1	
		into	nr_retorno_w
		from 	dual
		where	pkg_date_utils.get_DiffDate(SYSDATE, dt_agenda_p, 'DAY') = 0;
	exception
	when others then
		nr_retorno_w := 0;
	end;
end if;

RETURN	nr_retorno_w;

END obter_qtde_agenda_tipo_dia;
 /