create or replace
function pls_convert_masc_cart_usuario
		(	cd_usuario_plano_p 	varchar2,
			cd_estabelecimento_p	number)
			return varchar2 is

ds_retorno_w		varchar2(255);
ds_mascara_w		varchar2(255);
nr_cont_cart_w		Number(10) := 1;
i			Number(10) := 1;
				
begin
ds_mascara_w := pls_obter_mascara_cartao_benef(null,'O',cd_estabelecimento_p);

if	(ds_mascara_w is not null) then
	while (nr_cont_cart_w <> length(cd_usuario_plano_p) + 1) loop 
		begin	
		
		if	(substr(ds_mascara_w, i, 1) in ('/','-','.',' ','_')) then
			ds_retorno_w := ds_retorno_w || substr(ds_mascara_w, i, 1);
		else
			ds_retorno_w := ds_retorno_w || substr(cd_usuario_plano_p, nr_cont_cart_w,1);
			nr_cont_cart_w := nr_cont_cart_w + 1;
		end if;	
		
		i := i + 1;
		
		end;
	end loop;
else
	ds_retorno_w	:= cd_usuario_plano_p;
end if;

return	ds_retorno_w;

end pls_convert_masc_cart_usuario;
/
