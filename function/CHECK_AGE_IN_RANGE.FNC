create or replace function check_age_in_range(
                dt_birth_p      in  pessoa_fisica.dt_nascimento%type,
                qt_min_years_p  in  documento_idade.nr_idade_inicial%type,
                qt_min_days_p   in  documento_idade.nr_idade_inicial_dias%type,
                qt_max_years_p  in  documento_idade.nr_idade_final%type,
                qt_max_days_p   in  documento_idade.nr_idade_final_dias%type
                ) return varchar2 is

dt_current_trunc_w pessoa_fisica.dt_nascimento%type := null;
dt_birth_trunc_w pessoa_fisica.dt_nascimento%type := null;
dt_last_birthday_w pessoa_fisica.dt_nascimento%type := null;
qt_years_w documento_idade.nr_idade_inicial%type	:= 0;
qt_days_w documento_idade.nr_idade_inicial_dias%type	:= 0;
dt_min_age pessoa_fisica.dt_nascimento%type := null;
dt_max_age pessoa_fisica.dt_nascimento%type := null;
dt_age pessoa_fisica.dt_nascimento%type := null;
ie_return varchar2(1) := 'N';

BEGIN

    dt_current_trunc_w := trunc(sysdate);
    dt_birth_trunc_w := trunc(dt_birth_p);

    select obter_idade(dt_birth_trunc_w, dt_current_trunc_w, 'A')
    into qt_years_w
    from dual;

    if (dt_birth_trunc_w > dt_current_trunc_w) then
        dt_last_birthday_w := to_date(to_char(dt_birth_trunc_w, 'dd/mm')||to_char(add_months(dt_current_trunc_w, -12), 'yyyy'), 'dd/mm/yyyy');
    elsif (dt_birth_trunc_w < dt_current_trunc_w) then
        dt_last_birthday_w := to_date(to_char(dt_birth_trunc_w, 'dd/mm')||to_char(dt_current_trunc_w,'yyyy'),'dd/mm/yyyy');
    end if;

    select obter_idade(dt_last_birthday_w, dt_current_trunc_w, 'DIA')
    into qt_days_w
    from dual;

    dt_min_age := add_months(dt_current_trunc_w, qt_min_years_p*12) + nvl(qt_min_days_p, 0);
    if (qt_max_days_p is not null) then
        dt_max_age := add_months(dt_current_trunc_w, qt_max_years_p*12) + qt_max_days_p;
    else
        dt_max_age := add_months(dt_current_trunc_w, (qt_max_years_p + 1)*12) - 1;
    end if;
    dt_age := add_months(dt_current_trunc_w, qt_years_w*12) + qt_days_w;

    if (dt_age between dt_min_age and dt_max_age) then 
        ie_return := 'S';
    else
        ie_return := 'N';
    end if;

    return ie_return;

end check_age_in_range;
/

