create or replace
function pls_obter_desc_glosa
			(	nr_seq_glosa_ref_p	number)
				return varchar2 is
			
ds_retorno_w	varchar2(255);			

begin
begin
select	tiss_obter_motivo_glosa(nr_seq_motivo_glosa, 'C')||' - '||tiss_obter_motivo_glosa(nr_seq_motivo_glosa, 'D')
into	ds_retorno_w
from	pls_conta_glosa a
where	nr_sequencia = nr_seq_glosa_ref_p;
exception
when others then
	ds_retorno_w	:= null;
end;

return	ds_retorno_w;

end pls_obter_desc_glosa;
/