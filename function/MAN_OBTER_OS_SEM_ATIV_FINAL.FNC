create or replace
function	man_obter_os_sem_ativ_final(
		nr_seq_ordem_p		number,
		nm_usuario_p		varchar2) return varchar2 is

ds_retorno_w		varchar2(2000);
nr_ordem_servico_w	number(10);

cursor	c01 is
select	a.nr_sequencia
from	man_ordem_servico a
where	substr(obter_se_usuario_exec(a.nr_sequencia, nm_usuario_p, 0),1,1) = 'S'
and	a.ie_status_ordem <> '3'
and	(select   count(*)
	from	man_ordem_serv_ativ b
	where	b.dt_fim_atividade is null
	and	b.nm_usuario_exec = nm_usuario_p
	and	a.nr_sequencia = b.nr_seq_ordem_serv) > 0
and	a.nr_sequencia <> nr_seq_ordem_p;

begin

open c01;
loop
fetch c01 into
	nr_ordem_servico_w;
exit when c01%notfound;
	begin

	if	(ds_retorno_w = '') then
		ds_retorno_w	:= to_char(nr_ordem_servico_w);
	else
		ds_retorno_w	:= ds_retorno_w || ', ' || to_char(nr_ordem_servico_w);
	end if;

	end;
end loop;
close c01;

return ds_retorno_w;

end man_obter_os_sem_ativ_final;
/