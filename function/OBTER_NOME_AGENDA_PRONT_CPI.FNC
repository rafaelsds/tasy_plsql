create or replace
function obter_nome_agenda_pront_cpi	(cd_agenda_p	number)
					return varchar2 is
cd_tipo_agenda_w	number;
ds_agenda_w		varchar2(100);

begin

cd_tipo_agenda_w	:= obter_tipo_agenda(cd_agenda_p);

if	(cd_tipo_agenda_w in (3,4)) then
	ds_agenda_w	:=	substr(obter_nome_agenda_cons(cd_agenda_p),1,100);
else
	ds_agenda_w	:=	substr(obter_nome_agenda(cd_agenda_p),1,100);

end if;

return ds_agenda_w;
end;
/