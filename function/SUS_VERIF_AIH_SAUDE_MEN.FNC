create or replace
function sus_verif_aih_saude_men(nr_interno_conta_p	conta_paciente.nr_interno_conta%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				dt_procedimento_p	procedimento_paciente.dt_procedimento%type,
				cd_procedimento_p	procedimento_paciente.cd_procedimento%type)
				return varchar2 is

ds_retorno_w		varchar2(15) := 'N';
qt_reg_habil_w		number(10) := 0;
qt_reg_aih_w		number(10) := 0;

begin

if	(cd_procedimento_p in (303170166,303170131,303170158,303170174,303170182,303170140)) then
	begin
	
	select	count(1)
	into	qt_reg_habil_w
	from	sus_habilitacao_hospital
	where	cd_estabelecimento = cd_estabelecimento_p
	and	dt_procedimento_p between nvl(dt_inicio_vigencia,dt_procedimento_p) and nvl(dt_final_vigencia,dt_procedimento_p)
	and	cd_habilitacao = 636
	and	rownum = 1;

	if	(qt_reg_habil_w > 0) then
		begin
		
		select	count(1)
		into	qt_reg_aih_w
		from	sus_aih_unif
		where	nr_interno_conta = nr_interno_conta_p
		and	cd_especialidade_aih = 87
		and	cd_procedimento_real in (303170166,303170131,303170158,303170174,303170182,303170140)
		and	rownum = 1;
		
		if	(qt_reg_aih_w > 0) then
			ds_retorno_w := 'S';		
		else
			ds_retorno_w := 'N';		
		end if;
		
		end;
	else
		ds_retorno_w := 'N';
	end if;       
	
	end;
end if;

	
return	ds_retorno_w;

end sus_verif_aih_saude_men;
/