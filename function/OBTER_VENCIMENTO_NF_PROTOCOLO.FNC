create or replace
function obter_vencimento_nf_protocolo(nr_seq_protocolo_p	number) 
                              			return date is
dt_vencimento_w		date;
                              
begin

if	(nr_seq_protocolo_p is not null) then
	begin
	
	select	min(b.dt_vencimento)
	into	dt_vencimento_w
	from	nota_fiscal_venc b,
		nota_fiscal a
	where	a.nr_seq_protocolo	= nr_seq_protocolo_p
	and	a.nr_sequencia	= b.nr_sequencia;
	
	end;
end if;

return dt_vencimento_w;

end obter_vencimento_nf_protocolo;
/