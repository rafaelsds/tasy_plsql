create or replace
function verifica_medic_dupl_prescr(	cd_material_p		number,
					nr_prescricao_p		number,
					nr_seq_mat_p		number,
					nm_usuario_p		varchar2)
 		    	return varchar2 is

ie_duplicado_w		varchar2(1);
			
begin

select	decode(count(nr_sequencia),0,'N','S')
into	ie_duplicado_w
from	prescr_material
where	nr_prescricao			= nr_prescricao_p
and	cd_material			= cd_material_p
and	nr_sequencia			< nr_seq_mat_p
and	ie_agrupador			= 1;

return	ie_duplicado_w;

end verifica_medic_dupl_prescr;
/