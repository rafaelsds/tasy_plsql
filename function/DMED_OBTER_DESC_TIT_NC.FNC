create or replace
function dmed_obter_desc_tit_nc
			(		nr_titulo_receber_p number,
					nr_seq_tit_receber_liq_p number,
					dt_vigencia_p date)
					return number is
					
vl_desconto_nc_w	number(15,2);

begin

select  nvl(sum(vl_total),0)
into	vl_desconto_nc_w
from 	(
			select 	nvl(b.vl_baixa, 0) vl_total
			from  	nota_credito c,
					nota_credito_baixa b
			where  	c.nr_sequencia = b.nr_seq_nota_credito
			and 	c.nr_titulo_receber = nr_titulo_receber_p
			and    	c.nr_seq_baixa_origem = nr_seq_tit_receber_liq_p
			and 	c.ie_situacao = 'L'
			and 	PKG_DATE_UTILS.start_of(b.dt_baixa, 'YEAR', 0) = PKG_DATE_UTILS.start_of(dt_vigencia_p, 'YEAR', 0)
			union all
			select	nvl(vl_devolver,0) vl_total
			from	pls_solic_resc_fin_item a,
					pls_solic_rescisao_fin b,
					pls_solic_resc_fin_venc c,
					pls_segurado d
			where  	b.nr_sequencia = a.nr_seq_solic_resc_fin
			and    	b.nr_sequencia = c.nr_seq_solic_resc_fin
			and    	d.nr_sequencia = a.nr_seq_segurado
			and    	b.dt_devolucao is not null
			and		a.nr_titulo = nr_titulo_receber_p
			and    exists (	select 1
                      from  nota_credito x
                      where x.nr_sequencia = c.nr_seq_nota_credito
                      and   x.ie_situacao = 'L'));

return vl_desconto_nc_w;

end dmed_obter_desc_tit_nc;
/