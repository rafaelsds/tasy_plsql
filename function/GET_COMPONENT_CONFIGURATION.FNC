create or replace
function get_component_configuration(
		ie_tipo_component_p	varchar2,
		nm_tabela_p     	varchar2,
		nr_seq_visao_p		number,
		nr_seq_dic_objeto_p     number,
		nr_seq_obj_schem_p	number,
		cd_funcao_p		number)
 		return number is

if_config_w	varchar2(1);
vl_return_w	number(10);

begin

if_config_w := has_component_configuration(ie_tipo_component_p, nm_tabela_p, nr_seq_visao_p, nr_seq_dic_objeto_p, nr_seq_obj_schem_p, cd_funcao_p);

if (if_config_w = 'Y') then
	vl_return_w := 2469;	
end if;

return vl_return_w;

end get_component_configuration;
/