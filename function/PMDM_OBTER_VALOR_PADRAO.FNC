create or replace
function pmdm_obter_valor_padrao
		(nm_tabela_p	in	tabela_atributo.nm_tabela%type,
		nm_atributo_p	in	varchar2,
        nr_seq_visao_p   in   tabela_visao_atributo.nr_sequencia%type,
        ds_valor_retorno_p  in  varchar2)
return varchar2 is
ds_valor_w  varchar2(255 char);
begin
    ds_valor_w := ds_valor_retorno_p;
    begin
        if nvl(nr_seq_visao_p,0) > 0 then 
            select	max(vl_padrao)
            into	ds_valor_w
            from	tabela_visao_atributo
            where	nr_sequencia = nr_seq_visao_p
            and	nm_atributo = nm_atributo_p
            and	vl_padrao not like '@%';
        else
            select	max(vl_default)
            into	ds_valor_w
            from	tabela_atributo
            where	nm_tabela = nm_tabela_p
            and	nm_atributo = nm_atributo_p
            and	vl_default not like '@%';
        end if;
    exception
    when others then
        ds_valor_w := null;
    end;	
    
    if	(lower(ds_valor_w) = 'sysdate') then
        ds_valor_w	:=	to_char(sysdate);
    end if;
    return ds_valor_w;
end pmdm_obter_valor_padrao;
/
