create or replace
function obter_qt_erro_max_perm_escala(
			nr_sequencia_p		number)
return number is

qt_erro_max_permitido_w				number(15,4);
			
begin

select	qt_erro_max_permitido
into	qt_erro_max_permitido_w
from	man_equip_calib_escala
where	nr_sequencia = nr_sequencia_p;

return	qt_erro_max_permitido_w;

end obter_qt_erro_max_perm_escala;
/