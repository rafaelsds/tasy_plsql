create or replace
function obter_saldo_lote_audit_item	(	nr_seq_lote_item_p	number,
					ie_opcao_p		varchar2)
					return number is

/*	ie_opcao_p
'SI'	saldo inicial (saldo do item antes dele entrar na reapresenta��o de contas)
'SA'	saldo atual do procedimento/material dentro do lote (considerando todas as an�lises do lote)
'SAR'	saldo atual real (vl_amenor)
'SIA'	saldo inicial do item dentro de uma mesma an�lise (antes de alterar os valores pago e glosado)
'SAN'	saldo anterior � an�lise atual
'SNN'	saldo atual do item dentro da an�lise
'SIP'	saldo do item na an�lise anterior
*/

vl_amenor_w		number(15,2);
vl_glosa_w		number(15,2);
vl_pago_w		number(15,2);
vl_retorno_w		number(15,2);
nr_seq_ret_glosa_w	number(10);
nr_seq_propaci_w	number(10);
nr_seq_matpaci_w	number(10);
nr_seq_lote_w		number(10);
nr_seq_analise_w	number(10);
nr_analise_w		number(10);
vl_saldo_titulo_w	number(15,2);
nr_titulo_w		number(10);
vl_saldo_w		number(15,2);

begin

select	max(a.nr_seq_ret_glosa),
	max(a.nr_seq_propaci),
	max(a.nr_seq_matpaci),
	max(c.nr_seq_lote_audit),
	max(c.nr_sequencia),
	max(c.nr_analise),
	max(to_number(obter_titulo_conta_guia(b.nr_interno_conta,b.cd_autorizacao,null,null))),
	max(vl_saldo)
into	nr_seq_ret_glosa_w,
	nr_seq_propaci_w,
	nr_seq_matpaci_w,
	nr_seq_lote_w,
	nr_seq_analise_w,
	nr_analise_w,
	nr_titulo_w,
	vl_saldo_w
from	lote_audit_hist c,
	lote_audit_hist_guia b,
	lote_audit_hist_item a
where	a.nr_sequencia		= nr_seq_lote_item_p
and	a.nr_seq_guia		= b.nr_sequencia
and	b.nr_seq_lote_hist		= c.nr_sequencia;

if	(nvl(nr_seq_ret_glosa_w,0) > 0) and (ie_opcao_p <> 'SAN') and (ie_opcao_p <> 'SNN') then /* se for de retorno com saldo, busca do item glosado */

	select	a.vl_glosa
	into	vl_amenor_w
	from	convenio_retorno_glosa a
	where	a.nr_sequencia		= nr_seq_ret_glosa_w;

else	/* sen�o busca do pr�prio procedimento/material */

	select	nvl(vl_item,0)
	into	vl_amenor_w
	from	(select	a.vl_procedimento vl_item
		from	procedimento_paciente a
		where	a.nr_sequencia	= nr_seq_propaci_w
		union
		select	a.vl_material vl_item
		from	material_atend_paciente a
		where	a.nr_sequencia	= nr_seq_matpaci_w);

end if;

if	(ie_opcao_p = 'SI') then
	vl_retorno_w	:= vl_amenor_w;

elsif	(ie_opcao_p = 'SA') then

	select	nvl(sum(a.vl_glosa),0),
		nvl(sum(a.vl_pago),0)
	into	vl_glosa_w,
		vl_pago_w
	from	lote_audit_hist c,
		lote_audit_hist_guia b,
		lote_audit_hist_item a
	where	(a.nr_seq_propaci = nr_seq_propaci_w or a.nr_seq_matpaci = nr_seq_matpaci_w)
	and	a.nr_seq_guia		= b.nr_sequencia
	and	b.nr_seq_lote_hist		= c.nr_sequencia
	and	c.nr_seq_lote_audit	= nr_seq_lote_w;

	vl_retorno_w	:= (vl_amenor_w - vl_glosa_w - vl_pago_w);

elsif	(ie_opcao_p = 'SAR') then

	select	a.vl_amenor
	into	vl_retorno_w
	from	lote_audit_hist_item a
	where	a.nr_sequencia	= nr_seq_lote_item_p;

elsif	(ie_opcao_p = 'SIA') then

	if	(nvl(nr_analise_w,1) = 1) then	/* se for a primeira an�lise, busca o saldo do retorno conv�nio, antes de entra na reapresenta��o de contas */
	
		select	max(a.vl_saldo_titulo)
		into	vl_saldo_titulo_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_w;

		if	(nvl(vl_saldo_titulo_w,0) = 0) and (nr_titulo_w is not null) then
			vl_retorno_w	:= 0;
		else
			vl_retorno_w	:= nvl(vl_saldo_w, vl_amenor_w);
		end if;
	else					/* sen�o, busca o saldo da an�lise anterior */
		select	nvl(max(a.vl_amenor),0)
		into	vl_retorno_w
		from	lote_audit_hist c,
			lote_audit_hist_guia b,
			lote_audit_hist_item a
		where	(a.nr_seq_propaci = nr_seq_propaci_w or a.nr_seq_matpaci = nr_seq_matpaci_w)
		and	a.nr_seq_guia		= b.nr_sequencia
		and	b.nr_seq_lote_hist		= c.nr_sequencia
		and	c.nr_seq_lote_audit	= nr_seq_lote_w
		and	c.nr_analise		= nr_analise_w - 1;
	end if;

elsif	(ie_opcao_p = 'SAN') then

	select	sum(vl_glosa),
		sum(vl_pago)
	into	vl_glosa_w,
		vl_pago_w
	from	(select	decode(nvl(a.ie_acao_glosa,b.ie_acao_glosa),'A',a.vl_glosa) vl_glosa,
			nvl(to_number(obter_dados_ret_movto_glosa(a.nr_sequencia, 3)),0) vl_pago
		from	convenio_retorno d,
			convenio_retorno_item c,
			motivo_glosa b,
			convenio_retorno_glosa a
		where	a.nr_seq_propaci	= nr_seq_propaci_w
		and	a.cd_motivo_glosa	= b.cd_motivo_glosa
		and	a.nr_seq_ret_item	= c.nr_sequencia
		and	c.nr_seq_retorno	= d.nr_sequencia
		and	d.ie_status_retorno	= 'F'
		union
		select	decode(nvl(a.ie_acao_glosa,b.ie_acao_glosa),'A',a.vl_glosa) vl_glosa,
			nvl(to_number(obter_dados_ret_movto_glosa(a.nr_sequencia, 3)),0) vl_pago
		from	convenio_retorno d,
			convenio_retorno_item c,
			motivo_glosa b,
			convenio_retorno_glosa a
		where	a.nr_seq_matpaci	= nr_seq_matpaci_w
		and	a.cd_motivo_glosa	= b.cd_motivo_glosa
		and	a.nr_seq_ret_item	= c.nr_sequencia
		and	c.nr_seq_retorno	= d.nr_sequencia
		and	d.ie_status_retorno	= 'F'
		union
		select	a.vl_glosa,
			a.vl_pago
		from	lote_audit_hist c,
			lote_audit_hist_guia b,
			lote_audit_hist_item a
		where	a.nr_seq_propaci	= nr_seq_propaci_w
		and	a.nr_seq_guia		= b.nr_sequencia
		and	b.nr_seq_lote_hist	= c.nr_sequencia
		and	c.nr_analise		< nr_analise_w
		and	c.dt_envio		is not null
		union
		select	a.vl_glosa,
			a.vl_pago
		from	lote_audit_hist c,
			lote_audit_hist_guia b,
			lote_audit_hist_item a
		where	a.nr_seq_matpaci	= nr_seq_matpaci_w
		and	a.nr_seq_guia		= b.nr_sequencia
		and	b.nr_seq_lote_hist	= c.nr_sequencia
		and	c.nr_analise		< nr_analise_w
		and	c.dt_envio		is not null);

	vl_retorno_w	:= nvl(vl_amenor_w,0) - nvl(vl_glosa_w,0) - nvl(vl_pago_w,0);

elsif	(ie_opcao_p = 'SNN') then

	select	sum(vl_glosa),
		sum(vl_pago)
	into	vl_glosa_w,
		vl_pago_w
	from	(select	decode(nvl(a.ie_acao_glosa,b.ie_acao_glosa),'A',a.vl_glosa) vl_glosa,
			nvl(to_number(obter_dados_ret_movto_glosa(a.nr_sequencia, 3)),0) vl_pago
		from	convenio_retorno d,
			convenio_retorno_item c,
			motivo_glosa b,
			convenio_retorno_glosa a
		where	a.nr_seq_propaci	= nr_seq_propaci_w
		and	a.cd_motivo_glosa	= b.cd_motivo_glosa
		and	a.nr_seq_ret_item	= c.nr_sequencia
		and	c.nr_seq_retorno	= d.nr_sequencia
		and	d.ie_status_retorno	= 'F'
		union
		select	decode(nvl(a.ie_acao_glosa,b.ie_acao_glosa),'A',a.vl_glosa) vl_glosa,
			nvl(to_number(obter_dados_ret_movto_glosa(a.nr_sequencia, 3)),0) vl_pago
		from	convenio_retorno d,
			convenio_retorno_item c,
			motivo_glosa b,
			convenio_retorno_glosa a
		where	a.nr_seq_matpaci	= nr_seq_matpaci_w
		and	a.cd_motivo_glosa	= b.cd_motivo_glosa
		and	a.nr_seq_ret_item	= c.nr_sequencia
		and	c.nr_seq_retorno	= d.nr_sequencia
		and	d.ie_status_retorno	= 'F'
		union
		select	a.vl_glosa,
			a.vl_pago
		from	lote_audit_hist c,
			lote_audit_hist_guia b,
			lote_audit_hist_item a
		where	a.nr_seq_propaci	= nr_seq_propaci_w
		and	a.nr_seq_guia		= b.nr_sequencia
		and	b.nr_seq_lote_hist	= c.nr_sequencia
		and	c.nr_analise		<= nr_analise_w
		union
		select	a.vl_glosa,
			a.vl_pago
		from	lote_audit_hist c,
			lote_audit_hist_guia b,
			lote_audit_hist_item a
		where	a.nr_seq_matpaci	= nr_seq_matpaci_w
		and	a.nr_seq_guia		= b.nr_sequencia
		and	b.nr_seq_lote_hist	= c.nr_sequencia
		and	c.nr_analise		<= nr_analise_w);

	vl_retorno_w	:= nvl(vl_amenor_w,0) - nvl(vl_glosa_w,0) - nvl(vl_pago_w,0);

elsif	(ie_opcao_p = 'SIP') then

	if	(nr_analise_w = 1) then

		select	nvl(max(a.vl_glosa_informada),0)
		into	vl_retorno_w
		from	lote_audit_hist_item a
		where	a.nr_sequencia	= nr_seq_lote_item_p;

	else

		select	nvl(sum(vl_amenor),0)
		into	vl_retorno_w
		from	(select	c.vl_amenor
			from	lote_audit_hist_item c,
				lote_audit_hist_guia b,
				lote_audit_hist a
			where	a.nr_seq_lote_audit	= nr_seq_lote_w
			and	a.nr_analise		= nvl(nr_analise_w,2) - 1
			and	a.nr_sequencia		= b.nr_seq_lote_hist
			and	b.nr_sequencia		= c.nr_seq_guia
			and	c.nr_seq_propaci	= nr_seq_propaci_w
			union all
			select	c.vl_amenor
			from	lote_audit_hist_item c,
				lote_audit_hist_guia b,
				lote_audit_hist a
			where	a.nr_seq_lote_audit	= nr_seq_lote_w
			and	a.nr_analise		= nvl(nr_analise_w,2) - 1
			and	a.nr_sequencia		= b.nr_seq_lote_hist
			and	b.nr_sequencia		= c.nr_seq_guia
			and	c.nr_seq_matpaci	= nr_seq_matpaci_w);

	end if;

end if;

return vl_retorno_w;

end obter_saldo_lote_audit_item;
/
