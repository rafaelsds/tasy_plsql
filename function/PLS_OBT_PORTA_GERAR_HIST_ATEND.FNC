create or replace
function pls_obt_porta_gerar_hist_atend(
		nr_seq_atendimento_p	number,
		ie_tipo_historico_p		varchar2,
		nm_usuario_receb_p		varchar2,
		nm_usuario_envio_p		varchar2,
		ds_menssagem_p		varchar2,
		cd_funcao_p		number,
		nm_usuario_p		varchar2)
	return varchar2 is 

qt_tempo_w	number(10);
nr_porta_w	varchar2(20);

begin

select	round((sysdate - dt_atualizacao) * 86400,2)
into	qt_tempo_w
from	pls_atendimento_historico
where	nr_seq_tipo_historico	= 2
and	nr_seq_atendimento		= nr_seq_atendimento_p;

pls_gerar_historico_atend(
	qt_tempo_w,
	nr_seq_atendimento_p,
	ie_tipo_historico_p,
	nm_usuario_receb_p,
	nm_usuario_envio_p,
	ds_menssagem_p);

select	nr_porta
into	nr_porta_w
from	usuario_conectado
where	cd_funcao		= cd_funcao_p
and	nm_usuario	= nm_usuario_p
and	dt_fim_conexao	is null;

return	nr_porta_w;
end pls_obt_porta_gerar_hist_atend;
/