create or replace function PFCS_GET_ENCOUNTER_DEVICE_NAME
(
  nr_encounter in number,
  ds_filter_type in varchar2,
  cd_estabelecimento_p	varchar2
) return varchar2 as

ds_device varchar(100);

ie_telemetria varchar2(5) :='S';
ie_rec_status varchar2(5) :='S';
ie_not_rec_status varchar2(5) :='F';

begin

select  
        eq.ds_equipamento into ds_device
			from
        unidade_atend_equip uae,
        equipamento eq,
        unidade_atendimento ua,
        atend_paciente_unidade apu,
        atendimento_paciente ap,
        cpoe_recomendacao cr,
        tipo_recomendacao tr,
        setor_atendimento sa
			where eq.cd_estabelecimento = cd_estabelecimento_p
      and eq.ie_tipo_equipamento = ds_filter_type
      and uae.cd_equipamento        = eq.cd_equipamento
			and uae.cd_unidade_basica       = ua.cd_unidade_basica
			and uae.cd_setor_atendimento    = ua.cd_setor_atendimento
			and ap.nr_atendimento         = apu.nr_atendimento
			and apu.cd_unidade_basica    = uae.cd_unidade_basica
			and apu.cd_setor_atendimento = uae.cd_setor_atendimento
			and apu.cd_unidade_compl     = uae.cd_unidade_compl
      and ap.nr_atendimento         = cr.nr_atendimento
      and cr.cd_recomendacao = tr.cd_tipo_recomendacao
      and uae.cd_setor_atendimento = sa.cd_setor_atendimento
      and tr.ie_telemetria = ie_telemetria
      and apu.nr_atendimento = nr_encounter
      and rownum = 1;

return ds_device;

end PFCS_GET_ENCOUNTER_DEVICE_NAME;
/