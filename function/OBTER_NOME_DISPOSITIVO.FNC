create or replace
function obter_nome_dispositivo	(nr_seq_dispositivo_p	number)
					return varchar2 is

ds_dispositivo_w	varchar2(80);

begin
if	(nr_seq_dispositivo_p is not null) then

	select	max(ds_dispositivo)
	into	ds_dispositivo_w
	from	dispositivo
	where	nr_sequencia = nr_seq_dispositivo_p;

end if;

return ds_dispositivo_w;

end obter_nome_dispositivo;
/