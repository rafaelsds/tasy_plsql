create or replace
function obter_atend_max_dor(dt_sinal_vital_p 	date)
 		    	return number is

			
ds_retorno_w		number(10);
nr_seq_result_dor_w	number(10);
qt_intensidade_w	varchar(255);			
			
begin

select 	max(obter_desc_result_dor(nr_seq_result_dor,'I'))
	into	qt_intensidade_w
	from 	w_eis_escala_dor 		
	where 	trunc(dt_sinal_vital) = trunc(dt_sinal_vital_p);
	
select	nr_sequencia
into	nr_seq_result_dor_w
from   	escala_dor_regra
where  	ie_situacao = 'A'
and    	qt_intensidade = to_number(qt_intensidade_w);

select	count(*)
into	ds_retorno_w
from	w_eis_escala_dor
where	trunc(dt_sinal_vital) = trunc(dt_sinal_vital_p)
and	nr_seq_result_dor  = nr_seq_result_dor_w;

return	ds_retorno_w;

end obter_atend_max_dor;
/
