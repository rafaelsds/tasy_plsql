create or replace
function pls_obter_se_proc_grau_partic
			(       nr_seq_proc_grau_partic_p       number,
				cd_procedimento_p               number,
				ie_origem_proced_p              number,
				nr_seq_proc_p                   number,
				nr_seq_conta_p			number)
				return number is
/*
	RETORNOS DA FUN��O:
	- Se retornar  NULL entao nao gera ocorrencia.
	- Se retornar nr_Seq_proc_partic entao gera ocorrencia para o participante do procedimento.
	- Se retornar 0'zero',  entao gera ocorrencia por  grau de participacao da conta, mas nao insere seq participante na ocorrencia.
 */				
				
--ds_retorno_w                    varchar2(1) := 'S';
ds_retorno_w                    number(10) 	:= null;
nr_seq_grau_partic_w            number(10) ;
qt_reg_w                        number(5);
qt_grau_partic_w                number(5);
ie_tipo_guia_w			varchar(2);
ie_partic_obrigatorio_w		varchar2(1);
nr_seq_proc_partic_w		number(10)	:= null;	


Cursor C01 is
        select  a.nr_seq_grau_partic,
		a.nr_sequencia
        from	pls_conta_proc          b,
                pls_proc_participante   a
        where   a.nr_seq_conta_proc     = b.nr_sequencia
        and     b.nr_sequencia          = nr_seq_proc_p;

begin
select  count(*)
into    qt_reg_w
from    pls_oc_regra_proc_partic
where   cd_procedimento         = cd_procedimento_p
and     ie_origem_proced        = ie_origem_proced_p
and	nr_sequencia		= nr_seq_proc_grau_partic_p;

select 	max(ie_partic_obrigatorio)
into 	ie_partic_obrigatorio_w
from 	pls_oc_regra_proc_partic
where   cd_procedimento         = cd_procedimento_p
and     ie_origem_proced        = ie_origem_proced_p
and	nr_sequencia		= nr_seq_proc_grau_partic_p;

/*se tem regra para o procedimento*/
if      (qt_reg_w > 0) then
	select  count(*)
	into	qt_reg_w
        from    pls_conta_proc          b,
                pls_proc_participante   a
        where   a.nr_seq_conta_proc     = b.nr_sequencia
        and     b.nr_sequencia          = nr_seq_proc_p;
	
	/*se existe participante no procedimento*/
	if	(qt_reg_w > 0) then
		open C01;
		loop
		fetch C01 into
			nr_seq_grau_partic_w,
			nr_seq_proc_partic_w;
		exit when C01%notfound;
			begin
			if	(nr_seq_grau_partic_w is null)  and
				(ie_partic_obrigatorio_w = 'S')	then
				--ds_retorno_w := 'N';
				ds_retorno_w := nr_seq_proc_partic_w; --retorna o participante
				goto final; -- NAO CONTINUA A VERIFICAO DO PARTICIPANTE, POIS AT� AQUI J� FOI IDENTIFICADO AO MENOS UM.
			end if;
			
			select  count(*)
			into    qt_grau_partic_w
			from    pls_proc_grau_partic
			where   nr_seq_regra            = nr_seq_proc_grau_partic_p
			and     nr_seq_grau_partic      = nr_seq_grau_partic_w
			and	ie_situacao = 'A';
			
			if      (qt_grau_partic_w = 0) then
				--ds_retorno_w := 'N';
				ds_retorno_w := nr_seq_proc_partic_w; --retorna o participante
				goto final; -- NAO CONTINUA A VERIFICAO DO PARTICIPANTE, POIS AT� AQUI J� FOI IDENTIFICADO AO MENOS UM.
			end if;
			
			end;
		end loop;
		close C01;
	else 	/*se entrar aqui n�o tem participante no procedimento*/		
		select	nr_seq_grau_partic,
			ie_tipo_guia
		into	nr_seq_grau_partic_w,	
			ie_tipo_guia_w
		from	pls_conta
		where	nr_sequencia = nr_seq_conta_p;
			
		/*Se for grau de participa��o nulo e uma conta de SP/SADT ou Honorario individual gera a ocorr�ncia  */
		if	(nr_seq_grau_partic_w is null)    then
			if (ie_tipo_guia_w in ('4','6'))	  and
				(ie_partic_obrigatorio_w = 'S')	then
				ds_retorno_w := 0;
				goto final;
			end if;
		else
			/*se  for tipo de guia HI ou SP/SADT  entra na regra*/
			if 	(ie_tipo_guia_w in ('4','6'))	then
				--(ie_partic_obrigatorio_w = 'S') then
				select  count(*)
				into    qt_grau_partic_w
				from    pls_proc_grau_partic
				where   nr_seq_regra            = nr_seq_proc_grau_partic_p
				and     nr_seq_grau_partic      = nr_seq_grau_partic_w
				and	ie_situacao = 'A';	
			
				if (qt_grau_partic_w = 0 ) then
					ds_retorno_w := 0;
				end if;
			end if;	/*Retorna 'zero' para identificar  a gera��o da ocorrencia por grau de participacao da CONTA */ 
		end if;
	end if;
end if;
<<final>>
return  ds_retorno_w;

end pls_obter_se_proc_grau_partic;
/
