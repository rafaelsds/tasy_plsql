create or replace
function ul_sintomas_aval(valor_p	number)
 		    	return number is

ds_retorno_w	number;
					
begin

if	(valor_p <= 5) then
	ds_retorno_w := 1;
elsif	(valor_p <= 10) then
	ds_retorno_w := 2;
elsif	(valor_p <= 19) then
	ds_retorno_w := 3;
elsif	(valor_p >= 20) then
	ds_retorno_w := 4;
end if;

return	ds_retorno_w;

end ul_sintomas_aval;
/