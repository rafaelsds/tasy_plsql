create or replace
function obter_ult_classif_man(nr_atendimento_p		number,
				dt_entrada		date)
				return varchar2 is
ie_retorno_w	varchar2(255);

begin

select 	max(ie_classif_manchester)
into	ie_retorno_w
from	triagem_classif_risco b,
	triagem_pronto_atend a
where	a.nr_seq_classif = b.nr_sequencia 
and	a.nr_atendimento = nr_atendimento_p
and a.nr_sequencia = (select max(nr_sequencia) from triagem_pronto_atend c where c.nr_atendimento = a.nr_atendimento);


return	ie_retorno_w;

end obter_ult_classif_man;
/
