create or replace
function Obter_Desc_Tipo_Convenio
		(cd_convenio_p		number)
		return varchar2 is

ds_tipo_convenio_w		varchar2(100);

begin

select	Obter_Valor_Dominio(11, ie_tipo_convenio)
into	ds_tipo_convenio_w
from	convenio
where 	cd_convenio	= cd_convenio_p;


return ds_tipo_convenio_w;

end Obter_Desc_Tipo_Convenio;
/