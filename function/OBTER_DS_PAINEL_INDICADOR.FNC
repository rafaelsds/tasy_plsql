create or replace
function obter_ds_painel_indicador	(	nr_seq_painel_p		number)
					return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	ds_painel
into	ds_retorno_w
from	painel_indicadores
where	nr_sequencia = nr_seq_painel_p;

return	ds_retorno_w;

end obter_ds_painel_indicador;
/