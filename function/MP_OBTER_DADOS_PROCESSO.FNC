create or replace
function mp_obter_dados_processo(nr_seq_processo_p	number,
				ie_opcao_p		varchar2)
						return varchar2 is

ds_retorno_w	varchar2(255)	:= null;
nm_processo_w	varchar(80);

/*
N - Nome do processo
*/

begin

if	(nr_seq_processo_p is not null) then
	select	nm_processo
	into	nm_processo_w
	from	mp_processo
	where	nr_sequencia	= nr_seq_processo_p;

	if	(ie_opcao_p = 'N') then
		ds_retorno_w	:= nm_processo_w;
	end if;
end if;


return ds_retorno_w;

end;
/