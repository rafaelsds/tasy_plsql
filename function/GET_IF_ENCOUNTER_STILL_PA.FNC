create or replace function get_if_encounter_still_pa(	nr_atendimento_p	number)
					return varchar2 is

ds_return_w		varchar2(255) := 'N';
ie_outcome_w		varchar2(1);
ie_hospitalized_w	varchar2(1);

begin

begin

	ie_outcome_w := obter_dados_desfecho_pa(nr_atendimento_p, 'DES');
	
	select	decode(nvl(count(*),0),0,'N','S')
	into	ie_hospitalized_w
	from	atendimento_alta a, parametro_medico p
	where	a.nr_atendimento = nr_atendimento_p
	and	a.ie_desfecho = 'I'
	and     p.cd_estabelecimento = (select cd_estabelecimento from atendimento_paciente where nr_atendimento = nr_atendimento_p)
	and 	((a.ie_tipo_orientacao <> 'P')
	or  	(nvl(p.ie_liberar_desfecho,'N')  = 'N') 
	or  	((a.dt_liberacao is not null) and (a.dt_inativacao is null)))
	and	obter_atepacu_paciente(nr_atendimento, 'PI') is not null;
			
	if (ie_outcome_w = 'N' or ie_hospitalized_w = 'N') then
		ds_return_w := 'S';
	end if;

exception
when others then
	ds_return_w := 'N';
end;

return ds_return_w;

end get_if_encounter_still_pa;
/
