create or replace
function phi_obter_valor_usu_grupo(
			dt_referencia_p		date,
			nm_usuario_grupo_p	varchar2,
			nr_seq_grupo_des_p	number,
			nr_seq_grupo_sup_p	number,
			ie_opcao_p		number default 1)
			return number is

dt_inicio_mes_w		date;
qt_reg_w		number(10);
vl_alocacao_w		number(30,16) := 0;
vl_ausencia_w		number(30,16) := 0;

/*	ie_opcao_p
	1 - Alocacao
	2 - Ausencia
	3 - Alocacao real (alocacao - ausencia)
*/

Cursor c01 (dt_inicio_p date, dt_fim_p date) is
	select	a.dt_base_vigencia,
		(select	nvl(max(x.pr_alocacao / 100),0)
		from	alocacao_usuario_grupo x
		where	x.nm_usuario_grupo = nm_usuario_grupo_p
		and	nvl(x.nr_seq_grupo_des,nvl(nr_seq_grupo_des_p,0)) = nvl(nr_seq_grupo_des_p,0)
		and	nvl(x.nr_seq_grupo_sup,nvl(nr_seq_grupo_sup_p,0)) = nvl(nr_seq_grupo_sup_p,0)
		and	a.dt_base_vigencia between x.dt_inicio_vigencia and nvl(x.dt_fim_vigencia,a.dt_base_vigencia)) vl_alocacao,
		count(1) over() cnt
	from	(
		select	dt_inicio_p + (level-1) dt_base_vigencia
		from	dual
		connect by dt_inicio_p + (level-1) <= dt_fim_p
		) a
	where	pkg_date_utils.is_business_day(a.dt_base_vigencia) = 1;

begin

dt_inicio_mes_w := pkg_date_utils.start_of(dt_referencia_p,'month',0);

for c01_w in c01 (dt_inicio_mes_w, trunc(dt_referencia_p)) loop
	begin
	vl_alocacao_w := vl_alocacao_w + c01_w.vl_alocacao;

	if	(ie_opcao_p in (2,3)) and
		(c01_w.vl_alocacao > 0) then
		begin
		select	count(1)
		into	qt_reg_w
		from	ausencia_tasy a
		where	a.nm_usuario_ausente = nm_usuario_grupo_p
		and	nvl(a.nr_seq_grupo_des,nvl(nr_seq_grupo_des_p,0)) = nvl(nr_seq_grupo_des_p,0)
		and	nvl(a.nr_seq_grupo_sup,nvl(nr_seq_grupo_sup_p,0)) = nvl(nr_seq_grupo_sup_p,0)
		and	(a.nr_seq_grupo_des is not null or a.nr_seq_grupo_sup is not null)
		and	nvl(a.cd_motivo_ausencia,'0') <> '16' --Loan
		and	c01_w.dt_base_vigencia between a.dt_inicio and a.dt_fim
		and	rownum = 1;

		if	(qt_reg_w = 1) then
			vl_ausencia_w := vl_ausencia_w + c01_w.vl_alocacao;
		end if;
		end;
	end if;

	if	(c01_w.cnt = c01%rowcount) then
		begin
		vl_alocacao_w := vl_alocacao_w / c01_w.cnt;

		if	(vl_ausencia_w > 0) then
			vl_ausencia_w := vl_ausencia_w / c01_w.cnt;
		end if;
		
		if	(nvl(ie_opcao_p,1) = 1) then
			return vl_alocacao_w;
		elsif	(ie_opcao_p = 2) then
			return vl_ausencia_w;
		elsif	(ie_opcao_p = 3) and
			(vl_alocacao_w > vl_ausencia_w) then
			return vl_alocacao_w - vl_ausencia_w;
		end if;
		end;
	end if;
	end;
end loop;

return 0;

end phi_obter_valor_usu_grupo;
/