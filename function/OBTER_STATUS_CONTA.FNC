CREATE OR REPLACE FUNCTION OBTER_STATUS_CONTA (	nr_interno_conta_p	number,
						ie_opcao_p		varchar2)
RETURN VARCHAR2 IS

ie_status_conta_w	varchar2(1);
ds_status_conta_w	varchar2(255);

BEGIN

if	( nr_interno_conta_p is not null ) then
	/*Coelho - OS110404*/
	Select	to_char(a.ie_status_acerto),
			b.ds_valor_dominio
	into	ie_status_conta_w,
			ds_status_conta_w
	from	valor_dominio b,
			conta_paciente a
	where	a.nr_interno_conta 	= nr_interno_conta_p
	and		to_char(a.ie_status_acerto)	= b.vl_dominio
	and		b.cd_dominio = 49;

	if	(ie_opcao_p = 'C') then
		RETURN ie_status_conta_w;
	else
		return ds_status_conta_w;
	end if;
else
	RETURN null;
end if;
END OBTER_STATUS_CONTA;
/