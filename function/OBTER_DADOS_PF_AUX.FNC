create or replace function obter_dados_pf_aux(	cd_pessoa_fisica_p pessoa_fisica.cd_pessoa_fisica%type, 
						campo_p varchar2) 
						return varchar2 as
wreturn varchar2(255) := null;

begin
	
    if (campo_p = 'C') then
	select 	max(a.nr_digito_verif) 
	into 	wreturn 
	from 	pessoa_fisica_aux a 
	where 	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
	
    elsif ( campo_p = 'SD' ) then
	select 	max(a.sg_departamento) 
	into 	wreturn 
	from 	pessoa_fisica_aux a 
	where 	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
	
    elsif ( campo_p = 'DS' ) then
	select 	max(a.ds_digito_verif) 
	into 	wreturn 
	from 	pessoa_fisica_aux a 
	where 	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
    end if;
    
return wreturn;

end obter_dados_pf_aux;
/