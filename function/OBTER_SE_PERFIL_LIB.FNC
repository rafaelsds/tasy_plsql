Create or Replace
function obter_se_perfil_lib(	cd_perfil_p		Number,
			cd_funcao_p		number)	
			return varchar2	is

ie_perfil_liberado_w	varchar2(1);

begin

select 	decode(count(*),0,'N','S')
into	ie_perfil_liberado_w
from 	funcao_perfil
where 	cd_perfil = cd_perfil_p
and	  	cd_funcao = cd_funcao_p;

return	ie_perfil_liberado_w;

end obter_se_perfil_lib;
/
