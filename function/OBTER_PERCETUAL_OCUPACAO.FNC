create or replace
function Obter_percetual_ocupacao(	nr_unidades_setor_p		number,
					nr_unidades_temporarias_p	number,
					nr_unidades_ocupadas_p		number,
					qt_unidade_acomp_p		number,
					nr_unidades_interditadas_p	number,
					nr_unidades_livres_p		number,
					nr_unidades_higienizacao_p	number,
					nr_unidades_reservadas_p	number,
					qt_unidades_isolamento_p	number,
					qt_unidades_alta_p		number,
					nr_unid_temp_ocup_p		number,
					nr_unid_temp_ocupadas_p		number,
					qt_unid_temp_acomp_p		number,
					nr_unid_temp_interditadas_p	number,
					nr_unid_temp_livres_p		number,
					nr_unid_temp_higienizacao_p	number,
					nr_unid_temp_reservadas_p	number,
					qt_unid_temp_isolamento_p	number,
					qt_unid_temp_alta_p		number,
					qt_unidade_manut_p		number,
					Ie_forma_calculo_p		varchar2
					)
			return number is
pr_ocupacao_w	number(18,6);

ie_forma_calculo_w	varchar2(2);

begin
ie_forma_calculo_w := Ie_forma_calculo_p;
If	(Ie_forma_calculo_p = 'X') then
	Obter_param_Usuario(44, 130, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_forma_calculo_w);
end if;

if	(ie_forma_calculo_w = '1') then
	pr_ocupacao_w	:= dividir((nr_unidades_ocupadas_p*100),(nr_unidades_setor_p-nr_unidades_temporarias_p+nr_unid_temp_ocup_p));
	
elsif	(ie_forma_calculo_w = '2') then
	pr_ocupacao_w	:= dividir((qt_unidades_alta_p+nr_unidades_ocupadas_p+qt_unidades_isolamento_p)*100,
				nr_unidades_setor_p-nr_unidades_temporarias_p-
				qt_unidade_acomp_p-nr_unidades_interditadas_p-
				nr_unidades_reservadas_p+nr_unid_temp_ocupadas_p);
				
elsif	(ie_forma_calculo_w = '3') then
	pr_ocupacao_w	:= dividir((nr_unidades_ocupadas_p*100),(nr_unidades_setor_p - nr_unidades_interditadas_p -
								 nr_unidades_temporarias_p + nr_unid_temp_ocup_p));
elsif	(ie_forma_calculo_w = '4') then
	pr_ocupacao_w	:= dividir((nr_unidades_ocupadas_p*100),(nr_unidades_setor_p - nr_unidades_interditadas_p -
								 nr_unidades_temporarias_p));
elsif	(ie_forma_calculo_w = '5') then
	pr_ocupacao_w	:= dividir((nr_unidades_ocupadas_p*100),(nr_unidades_setor_p - nr_unidades_interditadas_p -
								 qt_unidade_manut_p));								 								 
elsif	(ie_forma_calculo_w = '6') then
	pr_ocupacao_w	:= dividir(((nr_unidades_ocupadas_p + qt_unidades_isolamento_p) * 100),
				(nr_unidades_setor_p - nr_unidades_interditadas_p - nr_unidades_temporarias_p + nr_unid_temp_ocup_p));
elsif	(ie_forma_calculo_w = '7') then
	pr_ocupacao_w	:= dividir(((nr_unid_temp_ocupadas_p + nr_unidades_ocupadas_p)*100),(nr_unidades_setor_p - nr_unidades_interditadas_p -
								 nr_unidades_temporarias_p));
elsif	(ie_forma_calculo_w = '8') then
	pr_ocupacao_w	:= dividir((nr_unidades_ocupadas_p*100),(nr_unidades_setor_p - nr_unidades_temporarias_p));								 
end if;

if	(pr_ocupacao_w > 100)
and	(ie_forma_calculo_w <> '4') then
	pr_ocupacao_w := 100;
end if;

return	pr_ocupacao_w;

end Obter_percetual_ocupacao;
/
