create or replace
function imes_obter_dados_aval(           cd_pessoa_fisica_p     number,
				          dt_inicio_p		 date,
					  dt_fim_p		 date,
					  nr_seq_aval_p		 number,
					  ie_opcao_p		 varchar2)
 		    	return 		  varchar2   is
	 		
ds_retorno_dados_w      varchar2(2000);
ds_retorno_datas_w      varchar2(2000);

ds_dados_w      	varchar2(100);
ds_datas_w      	varchar2(100);

CURSOR C01 IS
	SELECT 	RPAD(substr(obter_desc_result_avaliacao(c.nr_sequencia,nr_seq_aval_p),1,20),18,' '),
		RPAD(SUBSTR(c.dt_avaliacao,1,10),18,' ')
	FROM 	med_avaliacao_paciente c,
		med_tipo_avaliacao b,
		med_item_avaliar a
	WHERE   c.nr_seq_tipo_avaliacao = b.nr_sequencia 
	AND	a.nr_seq_tipo = b.nr_sequencia
	AND 	a.nr_seq_tipo = 643
	AND     a.nr_sequencia = nr_seq_aval_p
	AND	c.cd_pessoa_fisica = cd_pessoa_fisica_p
	AND	c.dt_avaliacao BETWEEN dt_inicio_p AND fim_dia(dt_fim_p); 


begin
ds_retorno_dados_w      := null;
ds_retorno_datas_w      := null;

ds_dados_w      	:= null;
ds_datas_w      	:= null;

open C01;
loop
fetch C01 into	
	ds_dados_w,
	ds_datas_w;
exit when C01%notfound;
	begin
	
	ds_retorno_dados_w := ds_retorno_dados_w || ds_dados_w;
	ds_retorno_datas_w := ds_retorno_datas_w || ds_datas_w;
	
	end;
end loop;
close C01;

if (ie_opcao_p = 'DT') then
	return	ds_retorno_datas_w;
end if;
if  (ie_opcao_p = 'DD') then
	return	ds_retorno_dados_w;
end if;

end imes_obter_dados_aval;
/