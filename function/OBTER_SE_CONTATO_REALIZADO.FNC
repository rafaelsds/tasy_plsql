create or replace
function obter_se_contato_realizado(nr_seq_lista_p		varchar2)
 		    	return varchar2 is
			
ds_retorno_w		Varchar2(1);
ie_contato_realizado_w	ag_lista_espera_contato.ie_contato_realizado%type;

Cursor C01 is
	select	ie_contato_realizado
	from 	ag_lista_espera_contato 
	where 	nr_seq_lista_espera = nr_seq_lista_p;

begin
	ds_retorno_w := 'N';
	
	if (nr_seq_lista_p is not null) then
		open C01;
		loop
		fetch C01 into	
			ie_contato_realizado_w;
		exit when C01%notfound;
			begin
				if(ie_contato_realizado_w = 'S') then
					ds_retorno_w := ie_contato_realizado_w;
				end if;
			end;
		end loop;
		close C01;
	end if;

	return	ds_retorno_w;
end obter_se_contato_realizado;
/
