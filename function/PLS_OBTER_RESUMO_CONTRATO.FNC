create or replace
function pls_obter_resumo_contrato(	nr_seq_contrato_p	number,
												ie_opcao_p			varchar2)
 		    	return number is

/* ie_opcao_p
	QAA - Quantidade de ativos ades�o
	VAA - Valor de ativos ades�o
	QIA - Quantidade de inativos ades�o
	VIA - Valor de inativos ades�o
	QBA - Quantidade abertos ades�o
	
	QAM - Quantidade de ativos migra��o
	VAM - Valor de ativos migra��o
	QIM - Quantidade de inativos migra��o
	VIM - Valor de inativos migra��o
	QBM - Quantidade abertos migra��o
	
	QAR - Quantidade de ativos reativa��o
	VAR - Valor de ativos reativa��o
	QIR - Quantidade de inativos reativa��o
	VIR - Valor de inativos reativa��o
	QBR - Quantidade abertos reativa��o
*/
vl_retorno_w				number(15,2);
qt_retorno_w				number(10);

begin

if (ie_opcao_p = 'QAA') or (ie_opcao_p = 'VAA') then
	select	count(*),
				nvl(sum(pls_obter_valor_segurado(b.nr_sequencia, 'VCD')),0)
	into		qt_retorno_w,
				vl_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is not null
	and		b.dt_migracao is null
	and		b.dt_rescisao is null
	and		b.dt_reativacao is null;
elsif (ie_opcao_p = 'QIA') or (ie_opcao_p = 'VIA') then
	select	count(*),
				nvl(sum(pls_obter_valor_segurado(b.nr_sequencia, 'VCD')),0)
	into		qt_retorno_w,
				vl_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is not null
	and		b.dt_rescisao is not null
	and		b.dt_migracao is null;
elsif (ie_opcao_p = 'QBA') then
	select	count(*)
	into		qt_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is null
	and		b.dt_rescisao is null
	and		b.dt_migracao is null;
elsif (ie_opcao_p = 'QAM') or (ie_opcao_p = 'VAM') then
	select	count(*),
				nvl(sum(pls_obter_valor_segurado(b.nr_sequencia, 'VCD')),0)
	into		qt_retorno_w,
				vl_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is not null
	and		b.dt_migracao is not null
	and		b.dt_rescisao is null;
elsif (ie_opcao_p = 'QIM') or (ie_opcao_p = 'VIM') then
	select	count(*),
				nvl(sum(pls_obter_valor_segurado(b.nr_sequencia, 'VCD')),0)
	into		qt_retorno_w,
				vl_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is not null
	and		b.dt_migracao is not null
	and		b.dt_rescisao is not null;
elsif (ie_opcao_p = 'QBM') then
	select	count(*)
	into		qt_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is null
	and		b.dt_migracao is not null
	and		b.dt_rescisao is null;
elsif (ie_opcao_p = 'QAR') or (ie_opcao_p = 'VAR') then
	select	count(*),
				nvl(sum(pls_obter_valor_segurado(b.nr_sequencia, 'VCD')),0)
	into		qt_retorno_w,
				vl_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is not null
	and		b.dt_migracao is null
	and		b.dt_reativacao is not null;
elsif (ie_opcao_p = 'QIR') or (ie_opcao_p = 'VIR') then
	select	count(*),
				nvl(sum(pls_obter_valor_segurado(b.nr_sequencia, 'VCD')),0)
	into		qt_retorno_w,
				vl_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is not null
	and		b.dt_migracao is null
	and		b.dt_reativacao is not null
	and		b.dt_rescisao is not null;
elsif (ie_opcao_p = 'QBR') then
	select	count(*)
	into		qt_retorno_w
	from		pls_segurado b
	where		b.nr_seq_contrato = nr_seq_contrato_p
	and		b.dt_liberacao is null
	and		b.dt_migracao is null
	and		b.dt_reativacao is not null;
end if;

if (substr(ie_opcao_p,1,1) = 'Q') then
	return qt_retorno_w;
else
	return vl_retorno_w;
end if;

end pls_obter_resumo_contrato;
/