CREATE OR REPLACE FUNCTION SUS_OBTER_VALORES_PROC
					(	dt_procedimento_p	date,
						cd_procedimento_p 	number,
						ie_origem_proced_p 	number,
						cd_estabelecimento_p	number,
						ie_valor_p		varchar2)
						return number is
vl_retorno_w		number(12,2) := 0;
vl_aux_num_w		number(12,2);
dt_aux_date_w		date;
ds_aux_varch_w		varchar2(255);
vl_procedimento_w	number(12,2) := 0;
vl_medico_w		number(12,2) := 0;
vl_custo_operacional_w	number(12,2) := 0;
vl_filme_w		number(12,2) := 0;
vl_diaria_w		number(12,2) := 0;
vl_matmed_w		number(12,2) := 0;
vl_taxas_W		number(12,2) := 0;
vl_sadt_w		number(12,2) := 0;
vl_contraste_w		number(12,2) := 0;
vl_gesso_w		number(12,2) := 0;
vl_quimioterapia_w	number(12,2) := 0;
vl_dialise_w		number(12,2) := 0;
vl_thp_w		number(12,2) := 0;
vl_filme_rx_w		number(12,2) := 0;
vl_filme_ressonancia_w	number(12,2) := 0;
vl_anestesista_w	number(12,2) := 0;
vl_sadt_rx_w		number(12,2) := 0;
vl_sadt_pc_w		number(12,2) := 0;
vl_outros_w		number(12,2) := 0;
vl_ato_medico_w		number(12,2) := 0;
vl_ato_anestesista_w	number(12,2) := 0;
ie_valor_w		varchar2(10);


/* ie_opcao_p    CO - Custo Operacional
		 MA - Material/Medicamento
		 ME - M�dico
*/


begin

DEFINE_PRECO_PROC_SUS	(dt_procedimento_p,
		 	 cd_procedimento_p,
		 	 ie_origem_proced_p,
			 cd_estabelecimento_p,
			 vl_aux_num_w,
			 vl_aux_num_w,
		 	 vl_matmed_w,
		 	 vl_diaria_w,
		 	 vl_taxas_W,
		 	 vl_medico_w,
		 	 vl_sadt_w,
		 	 vl_contraste_w,
		 	 vl_gesso_w,
		 	 vl_quimioterapia_w,
		 	 vl_dialise_w,
		 	 vl_thp_w,
		 	 vl_filme_rx_w,
		 	 vl_filme_ressonancia_w,
		 	 vl_anestesista_w,
		 	 vl_sadt_rx_w,
		 	 vl_sadt_pc_w,
		 	 vl_outros_w,
		 	 vl_ato_medico_w,
		 	 vl_ato_anestesista_w,
		 	 vl_procedimento_w,
		 	 vl_aux_num_w,
		 	 dt_aux_date_w,
		 	 ds_aux_varch_w,
	 	 vl_aux_num_w);


ie_valor_w	:= upper(ie_valor_p);


if	(ie_valor_w	= 'CO' ) then
	vl_retorno_w	:= vl_diaria_w + vl_taxas_w + vl_sadt_w + vl_thp_w + vl_dialise_w + vl_gesso_w +
			   vl_sadt_rx_w + vl_sadt_pc_w + vl_matmed_w + 	vl_contraste_w +   vl_filme_rx_w +
			   vl_quimioterapia_w + vl_filme_ressonancia_w + vl_outros_w;
elsif	(ie_valor_w	= 'ME' ) then
	vl_retorno_w	:= vl_medico_w;
elsif	(ie_valor_w	= 'MA') then
	vl_retorno_w	:= vl_matmed_w;
end if;


return vl_retorno_w;

end SUS_OBTER_VALORES_PROC;
/
