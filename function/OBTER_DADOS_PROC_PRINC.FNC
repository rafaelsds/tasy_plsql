create or replace
function Obter_dados_proc_princ(nr_sequencia_p  Number)
                        return Varchar2 is

ds_retorno_w    varchar2(255);

begin


select  substr(to_char(dt_procedimento, 'dd/mm/yyyy hh24:mi:ss') || ' - ' || b.ds_procedimento,1,250) ds
into    ds_retorno_w
from    procedimento b,
        procedimento_paciente a
where   a.cd_procedimento       = b.cd_procedimento
and     a.ie_origem_proced      = b.ie_origem_proced
and     a.nr_sequencia          = nr_sequencia_p;

return  ds_retorno_w;

end Obter_dados_proc_princ;
/