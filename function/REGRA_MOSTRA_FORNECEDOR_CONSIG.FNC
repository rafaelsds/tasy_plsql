create or replace
function regra_mostra_fornecedor_consig(
				cd_estabelecimento_p	number,
				cd_cgc_p		varchar2,
				cd_material_p		number)
			return	varchar2 is

/*
Regra do dominio = 1384
1 = Mostrar todos os fornecedores
2 = Somente fornecedores com saldo de estoque
3 = Ordenar fornecedores pelo saldo de estoque
*/


ie_mostra_w			varchar2(1);
ie_regra_fornec_consig_w	varchar2(1);
qt_existe_w			number(5);

begin
ie_mostra_w		:= 'S';

select	nvl(max(ie_regra_fornec_consig),1)
into	ie_regra_fornec_consig_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_p;
if	(ie_regra_fornec_consig_w = 2) then

	select	count(*)
	into	qt_existe_w
	from	(
		select distinct a.cd_fornecedor
	from	pessoa_juridica b,
		fornecedor_mat_consignado a,
		material m
	where	a.cd_fornecedor = b.cd_cgc
	and a.cd_estabelecimento = cd_estabelecimento_p
	and	a.cd_material   = m.cd_material_estoque
	and	m.cd_material   = cd_material_p
	and	b.ie_situacao = 'A'
	and	a.qt_estoque > 0) x
	where	x.cd_fornecedor = cd_cgc_p;

	ie_mostra_w	:= 'N';	
	if	(qt_existe_w > 0) then
		ie_mostra_w	:= 'S';
	end if;

end if;

return ie_mostra_w;

end regra_mostra_fornecedor_consig;
/