create or replace function obter_pr_real_port_fase( nr_seq_portifolio_p number, 
                                                    nr_seq_area_fase_p  number ) return number is
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Obter percentual real de execu��o da fase de um portifolio
---------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [ ]  Relat�rios [ ] Outros: 
 --------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

pr_retorno_w number(15,2) := 0;

begin

  select ( select dividir(t.per_exec * 100,t.qt_hora_prev) pr_realizado
           from   ( select sum(pe.qt_hora_prev) qt_hora_prev,
                           sum(dividir(pe.qt_hora_prev * pe.pr_etapa,100)) per_exec
                    from   proj_cronograma pc,
                           proj_cron_etapa pe,
                           proj_projeto    pp,
                           proj_programa   pr
                    where  pc.nr_sequencia = pe.nr_seq_cronograma 
                    and    pp.nr_sequencia = pc.nr_seq_proj 
                    and    pr.nr_sequencia = pp.nr_seq_programa
                    and    pr.nr_seq_portifolio = nr_seq_portifolio_p
                    and    pe.nr_seq_area_fase  = nr_seq_area_fase_p
                    and    pc.ie_situacao = 'A'
                  ) t
         ) pr_real
  into   pr_retorno_w
  from   dual;
  
  return  pr_retorno_w;

end obter_pr_real_port_fase;
/