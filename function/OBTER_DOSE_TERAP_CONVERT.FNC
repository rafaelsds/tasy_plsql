create or replace 
function obter_dose_terap_convert(cd_material_p			number,
								qt_dose_p		number,
								nr_seq_dose_terap_origem_p	number,
								nr_seq_dose_terap_dest_p	number) return number is

cd_unid_med_terap_origem_w	unidade_medida.cd_unidade_medida%type;
cd_unid_med_terap_dest_w	unidade_medida.cd_unidade_medida%type;
qt_dose_convet_w	cpoe_material.qt_dose%type;

function obter_unid_med_terap(nr_seq_dose_terap_p number) return unidade_medida.cd_unidade_medida%type is
cd_unid_med_terap_w	unidade_medida.cd_unidade_medida%type;
begin
	select	min(a.cd_unidade_medida)
	into	cd_unid_med_terap_w
	from	material_conversao_unidade a,
		unidade_medida b,
		regra_dose_terap c
	where	a.cd_material = cd_material_p
	and	a.cd_unidade_medida = b.cd_unidade_medida
	and	b.ie_unidade_med_inter in ('mg', 'MCG', 'UI')
	and	c.nr_sequencia = nr_seq_dose_terap_p
	and upper(b.ie_unidade_med_inter) = upper(c.ie_unidade)
	order by a.ie_prioridade;
	
	return cd_unid_med_terap_w;
end;

begin
if (nr_seq_dose_terap_origem_p is not null and nr_seq_dose_terap_dest_p is not null and qt_dose_p is not null and cd_material_p is not null) then
	cd_unid_med_terap_origem_w := obter_unid_med_terap(nr_seq_dose_terap_origem_p);
	cd_unid_med_terap_dest_w := obter_unid_med_terap(nr_seq_dose_terap_dest_p);

	if (cd_unid_med_terap_origem_w is not null and cd_unid_med_terap_dest_w is not null) then
		qt_dose_convet_w := Obter_dose_convertida(cd_material_p, qt_dose_p, cd_unid_med_terap_origem_w, cd_unid_med_terap_dest_w);
	end if;
end if;

return nvl(qt_dose_convet_w, qt_dose_p);

end obter_dose_terap_convert;
/