create or replace function CPOE_obter_diluicao(	ie_tipo_solucao_p		in varchar2,
								qt_volume_p				in number,
								qt_tempo_aplicacao_p	in number,
								cd_intervalo_p			in varchar2 default null,
								nr_etapas_p				in number default null,
								qt_dosagem_p			in varchar2 default null,
								qt_solucao_total_p		in number default null,
								qt_hora_fase_hor_p		in varchar2 default null,
								qt_hora_fase_p			in number default null,
								ie_tipo_dosagem_p		in varchar2 default null,
								ie_ref_calculo_p		in varchar2 default null,
								si_rapid_infusion_p		in varchar2 default null)
							return varchar2 is

-- QT_VOLUME  = Volume etapa (ml)
-- QT_TEMPO_APLICACAO = Tempo total (h)
-- IE_REF_CALCULO = Referencia para calculo
-- CD_INTERVALO = Intervalo
-- NR_ETAPAS = N etapas
-- QT_DOSAGEM = Velocidade de infusao
-- QT_SOLUCAO_TOTAL = Volume total
-- QT_HORA_FASE_P = Tempo etapa (h) para caululo do intervalo ex: 9
-- QT_HORA_FASE_HOR_P = Tempo etapa (h) em formato hora ex: 09:00
-- CD_TIPO_DOSAGEM_P = Tipos de dosagem solucao - Codigo da descricao no dicionario de objetos

ADMINISTRAR_319869  CONSTANT number(10) := 319869;  /*Administrar*/
DE_319872           CONSTANT number(10) := 319872;  /*De*/
ETAPA_327591        CONSTANT number(10) := 327591;  /*Etapa*/
ETAPAS_319879       CONSTANT number(10) := 319879;  /*Etapas*/
EM_1167364          CONSTANT number(10) := 1167364; /*Em*/
HORA_319882         CONSTANT number(10) := 319882;  /*Hora*/
HORAS_326727        CONSTANT number(10) := 326727;  /*Horas*/
E_309413            CONSTANT number(10) := 309413;  /*e*/
MINUTO_327445       CONSTANT number(10) := 327445;  /*Minuto*/
MINUTOS_327446      CONSTANT number(10) := 327446;  /*Minutos*/
DOSAGEM_ETAPA_326695    CONSTANT number(10) := 326695;  /*DOSAGEM ETAPA*/
TEMPO_APLICACAO_347273  CONSTANT number(10) := 347273;  /*Tempo de aplicacao*/
VOLUME_TOTAL_319870     CONSTANT number(10) := 319870;  /*Volume total*/
ABERTA_1142668     CONSTANT number(10) := 1142668;  /*ABERTA*/

ds_retorno_w			varchar2(4000 char);
ds_tipo_dosagem			varchar2(255 char);
cd_traducao_dosagem_w	number(10);
qt_hora					number(2);
qt_min					number(2);
sql_w                   VARCHAR2(300);

function getDosageFormated( qt_valor_p in number ) return varchar2 is
    ds_return_w varchar2(255 char);
begin
    ds_return_w := to_char(qt_valor_p);

    if (substr(ds_return_w, 1, 1) in (',', '.')) then
        ds_return_w := '0' || ds_return_w;
    end if;

    return ds_return_w;
end;

begin

if	(nvl(qt_volume_p,0) > 0) then

	if (ie_tipo_solucao_p <> 'V') then
	   if (ie_ref_calculo_p in ('VI', 'VT')) then

		ds_retorno_w	:=	substr(Wheb_mensagem_pck.get_texto(ADMINISTRAR_319869,'QT_VOLUME='||getDosageFormated(qt_solucao_total_p)),1,4000);
	else

		ds_retorno_w := substr(Wheb_mensagem_pck.get_texto(ADMINISTRAR_319869,'QT_VOLUME='||getDosageFormated(qt_volume_p)),1,4000);
	end if;
	end if;

	if	(nvl(qt_hora_fase_p,0) > 0) and
		(cd_intervalo_p is not null)then

		ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(DE_319872,'QT_HORA_FASE='||to_char(qt_hora_fase_p)),1,4000);
	end if;

	if	(ie_ref_calculo_p not in ('VI', 'VT') and nvl(nr_etapas_p,0) > 0) then
		if	(nvl(nr_etapas_p,0) = 1) then

			ds_retorno_w	:=	substr(ds_retorno_w || ' ' || '(' || Wheb_mensagem_pck.get_texto(ETAPA_327591,'NR_ETAPAS='||to_char(nr_etapas_p)) || ')',1,4000);
		else

			ds_retorno_w	:=	substr(ds_retorno_w || ' ' || '(' || Wheb_mensagem_pck.get_texto(ETAPAS_319879,'NR_ETAPAS='||to_char(nr_etapas_p)) || ')',1,4000);
		end if;
	end if;
  
  if (si_rapid_infusion_p = 'O') then 
		ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(ABERTA_1142668),1,4000);
	else
	
		if	(ie_tipo_solucao_p = 'I') and
		(qt_hora_fase_hor_p is not null) and
		(qt_hora_fase_hor_p <> ':') and
		(qt_hora_fase_hor_p <> '  :  ') and
		(qt_hora_fase_hor_p <> '__:__') then

		qt_hora := to_number(substr(qt_hora_fase_hor_p,1,2));
		qt_min := to_number(substr(qt_hora_fase_hor_p,4,5));

		-- em
		ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(EM_1167364),1,4000);

			if (qt_hora > 0) then
				if (qt_hora = 1) then
				
					ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(HORA_319882,'QT_HORA_FASE_HOR='||qt_hora),1,4000);
				else
			
					ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(HORAS_326727,'QT_HORA_FASE_HOR='||qt_hora),1,4000);
				end if;
			end if;

			if (qt_min > 0) then
				if (qt_hora > 0) then
					-- e
					ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(E_309413),1,4000);
				end if;

				if (qt_min = 1) then
	
					ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(MINUTO_327445,'QT_HORA_FASE_MIN='||qt_min),1,4000);
				else
	
					ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(MINUTOS_327446,'QT_HORA_FASE_MIN='||qt_min),1,4000);
				end if;
			end if;

		end if;

		if	(qt_dosagem_p is not null) then

			case ie_tipo_dosagem_p
				when 'GTM' then
					cd_traducao_dosagem_w := 327073;
				when 'MGM' then
					cd_traducao_dosagem_w := 327074;
				when 'MLH' then
					cd_traducao_dosagem_w := 327075;
				else
					cd_traducao_dosagem_w	:= 0;
			end case;

			if	(cd_traducao_dosagem_w	> 0) then

				ds_tipo_dosagem := Wheb_mensagem_pck.get_texto(cd_traducao_dosagem_w);
			end if;

			/*ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(DOSAGEM_ETAPA_326695,'QT_DOSAGEM='|| cpoe_dose_formatada(qt_dosagem_p) ||' ;IE_TIPO_DOSAGEM='||lower(ds_tipo_dosagem)),1,4000);*/
			begin
				ds_retorno_w	:=	substr(ds_retorno_w || ' ' || Wheb_mensagem_pck.get_texto(DOSAGEM_ETAPA_326695,'QT_DOSAGEM='|| cpoe_dose_formatada(qt_dosagem_p) ||' ;IE_TIPO_DOSAGEM='||lower(ds_tipo_dosagem)),1,4000);
			exception
			when others then
				ds_retorno_w := ds_retorno_w;
			end;
		end if;
        /*Inicio MD1*/
        
    BEGIN
        sql_w := 'CALL obter_desc_cpoe_diluicao_md(:1, :2, :3, :4, :5) INTO :ds_retorno_w';
        EXECUTE IMMEDIATE sql_w
            USING IN ie_ref_calculo_p  ,IN qt_solucao_total_p , IN getdosageformated(qt_solucao_total_p), IN qt_tempo_aplicacao_p, IN ds_retorno_w,
                  OUT ds_retorno_w;
    EXCEPTION
        WHEN OTHERS THEN
            ds_retorno_w := null;
    END;
        
        /*Fim MD1*/
	
	end if;

	if ( ds_retorno_w is not null ) then
		ds_retorno_w	:=	substr(ds_retorno_w || '.' || chr(13),1,4000);
		else
		ds_retorno_w	:=	'';
	end if;

end if;



return ds_retorno_w;

end CPOE_obter_diluicao;
/