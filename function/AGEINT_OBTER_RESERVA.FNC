create or replace
function Ageint_Obter_Reserva(
			nr_seq_agenda_exame_p	number,
			nr_seq_agenda_cons_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(20);		
			
begin

if	(nvl(nr_seq_agenda_cons_p,0)	> 0) then
	select	max(nr_reserva)
	into	ds_retorno_w
	from	agenda_consulta
	where	nr_sequencia	= nr_seq_agenda_cons_p;
elsif	(nvl(nr_seq_Agenda_exame_p,0)	> 0) then
	select	max(nr_reserva)
	into	ds_retorno_w
	from	agenda_paciente
	where	nr_sequencia	= nr_seq_agenda_exame_p;
end if;

return	ds_retorno_w;

end Ageint_Obter_Reserva;
/