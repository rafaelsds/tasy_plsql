create or replace
FUNCTION 	obter_ult_oc_material(
		cd_material_p	Number,
		cd_estabelecimento_p number,
		cd_local_estoque_p  number)
	RETURN number IS

ds_retorno_w		number(10);

BEGIN

if	( cd_material_p is not null ) then
	select  a.nr_ordem_compra
	into 	ds_retorno_w
	from    ordem_compra a,
		ordem_compra_item b
	where   a.nr_ordem_compra = b.nr_ordem_compra
	and     b.cd_material = cd_material_p
	and	a.nr_seq_motivo_cancel is null
	and	((cd_local_estoque_p = 0) or (b.cd_local_estoque = cd_local_estoque_p))
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.dt_liberacao = (select max(dt_liberacao) from ordem_compra);

end if;

RETURN ds_retorno_w;

END obter_ult_oc_material;
/