create or replace
function CVSP_obter_valor_ultima_compra(
			cd_estabelecimento_p	Number,
			qt_dia_p			Number,
			cd_material_p		number,
			cd_local_estoque_p	Number,
			ie_tipo_p			Varchar2)
		return number is



vl_retorno_w			number(15,4);
nr_sequencia_w			number(10,0);
ie_tipo_w				varchar2(10);
ie_consignado_w			varchar2(1);
cd_material_estoque_w		number(6);
ie_forma_preco_ult_compra_w	varchar2(1);


/*ie_tipo_p
Fabio em 24/07/2008 - Tratamento especial para os tipos:
N	= Material da nota, ou seja: Busca a nota com pr�prio material passado no parametro
Sen�o busca sempre do Material estoque, ou seja: Busca a nota que tenha o material de estoque do material passado no parametro
*/

BEGIN

select	nvl(max(ie_forma_preco_ult_compra), 'N')
into	ie_forma_preco_ult_compra_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_forma_preco_ult_compra_w = 'S') then
	begin

	select	cd_material_estoque
	into	cd_material_estoque_w
	from	material
	where	cd_material = cd_material_p;

	select	nvl(max(vl_preco_ult_compra), 0)
	into	vl_retorno_w
	from	saldo_estoque
	where	cd_estabelecimento = cd_estabelecimento_p
	and	cd_material = cd_material_estoque_w
	and	dt_mesano_referencia = (
		select	max(dt_mesano_referencia)
		from	saldo_estoque
		where	cd_estabelecimento = cd_estabelecimento_p
		and	cd_material = cd_material_estoque_w);
	end;

else
	begin

	vl_retorno_w		:= 0;


	/*Fabio 20/03 - Separei em 2 select devido a performance pois fazia com nvl no local*/
	if	(nvl(cd_local_estoque_p, 0) = 0) then
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	natureza_operacao o,
			operacao_nota p,
			nota_fiscal b,
			nota_fiscal_item a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_natureza_operacao	= o.cd_natureza_operacao
		and	b.cd_operacao_nf 	= p.cd_operacao_nf
		and	a.cd_estabelecimento	= cd_estabelecimento_p
		and	a.dt_atualizacao	>= sysdate - nvl(qt_dia_p,90)
		and	a.cd_material  		= cd_material_p
		and	b.ie_situacao		= '1'
		and	o.ie_entrada_saida	= 'E'
		and	p.ie_ultima_compra	= 'S'
		and	obter_se_nota_bonificacao(b.nr_sequencia) = 'N';

	elsif	(nvl(cd_local_estoque_p, 0) > 0) then
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	natureza_operacao o,
			operacao_nota p,
			nota_fiscal b,
			nota_fiscal_item a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_natureza_operacao	= o.cd_natureza_operacao
		and	b.cd_operacao_nf 	= p.cd_operacao_nf
		and	a.cd_estabelecimento	= cd_estabelecimento_p
		and	a.cd_local_estoque	= cd_local_estoque_p
		and	a.dt_atualizacao	>= sysdate - nvl(qt_dia_p,90)
		and	a.cd_material  		= cd_material_p
		and	b.ie_situacao		= '1'
		and	o.ie_entrada_saida	= 'E'
		and	p.ie_ultima_compra	= 'S'
		and	obter_se_nota_bonificacao(b.nr_sequencia) = 'N';
	end if;


	if	(nr_sequencia_w > 0) then
		begin

		/*Fabio 20/03 - Separei em 2 select devido a performance pois fazia com nvl com ie_tipo para o material*/
		if	(nvl(ie_tipo_p, 'N') = 'N') then
			select	nvl(max(dividir((a.vl_total_item_nf - a.vl_desconto -
					vl_desconto_rateio + vl_frete + a.vl_despesa_acessoria + a.vl_seguro),
				a.qt_item_estoque)),0)
			into	vl_retorno_w
			from 	nota_fiscal_item a
			where	nr_sequencia 	= nr_sequencia_w
			and	cd_material	= cd_material_p;

		elsif	(nvl(ie_tipo_p, 'N') <> 'N') then
			select	nvl(max(dividir((a.vl_total_item_nf - a.vl_desconto -
					vl_desconto_rateio + vl_frete + a.vl_despesa_acessoria + a.vl_seguro),
				a.qt_item_estoque)),0)
			into	vl_retorno_w
			from 	nota_fiscal_item a
			where	nr_sequencia 		= nr_sequencia_w
			and	cd_material_estoque	= cd_material_p;
		end if;
		end;
	end if;
	end;
end if;

return vl_retorno_w;

END CVSP_obter_valor_ultima_compra;
/
