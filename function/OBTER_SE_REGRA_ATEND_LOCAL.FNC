create or replace
function obter_se_regra_atend_local(	nr_requisicao_p		number)
 		    	return varchar2 is

cd_local_estoque_w		local_estoque.cd_local_estoque%type;
cd_perfil_w			perfil.cd_perfil%type;
qt_regra_w			number(5) := 0;

begin

select	cd_local_estoque,
	obter_perfil_ativo
into	cd_local_estoque_w,
	cd_perfil_w
from	requisicao_material
where	nr_requisicao = nr_requisicao_p;

select	count(nr_sequencia)
into	qt_regra_w
from	regra_atend_req_local
where	cd_local_estoque = cd_local_estoque_w
and		(cd_perfil = cd_perfil_w or cd_perfil is null);

return	qt_regra_w;

end obter_se_regra_atend_local;
/
