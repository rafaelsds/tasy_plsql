create or replace
function obter_se_atendimento_tem_setor(	nr_atendimento_p	number)
					return varchar2 is

ie_passou_setor_w	varchar2(1);
					
begin

select 	decode(count(*), 0, 'S', 'N')
into 	ie_passou_setor_w
from 	atendimento_paciente
where  	nr_atendimento = nr_atendimento_p
and 	dt_cancelamento is null;

if (nvl(ie_passou_setor_w, 'N') = 'N') then
	select	nvl(max('S'),'N')
	into	ie_passou_setor_w
	from	atend_paciente_unidade
	where	nr_atendimento = nr_atendimento_p;
end if;
	
return ie_passou_setor_w;

end obter_se_atendimento_tem_setor;
/
