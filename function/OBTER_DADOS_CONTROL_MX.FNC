create or replace
function obter_dados_control_mx(nr_titulo_p		number,
				nr_seq_baixa_p		number,
				ie_opcao_p		varchar2)
 		    	return varchar2 is

ds_retorno_w   		varchar2(255);
cd_pessoa_fisica_w      pessoa_fisica.cd_pessoa_fisica%type;
cd_cgc_w                pessoa_juridica.cd_cgc%type;
nr_seq_pais_w           pais.nr_sequencia%type;
ds_banco_w              banco.ds_banco%type;
cd_cnpj_agencia_w 	pessoa_juridica.cd_cgc%type;
sg_pais_w               pais.sg_pais%type;
			
begin

select  max(a.cd_cgc),
	max(a.cd_pessoa_fisica)
into	cd_cgc_w,
	cd_pessoa_fisica_w
from    titulo_receber a,
	titulo_receber_liq b
where   a.nr_titulo = b.nr_titulo
and     a.nr_titulo = nr_titulo_p
and     exists (select 1 
		from  movto_banco_pend_baixa
		where nr_titulo = nr_titulo_p
		and   nr_seq_baixa = nr_seq_baixa_p);
				

	if 	(ie_opcao_p = 'RFC') then   -- rfc da banco vinculado a pessoa do titulo
				
		if (cd_cgc_w is not null) then 	
				
			select	max(a.cd_cnpj_agencia)
			into	cd_cnpj_agencia_w
			from	pessoa_juridica_conta a 
			where	a.cd_cgc = cd_cgc_w;
			
			if 	(nvl(cd_cnpj_agencia_w,'X') <> 'X')then
			
				select	max(cd_rfc)
				into	ds_retorno_w
				from 	pessoa_juridica
				where	cd_cgc = cd_cnpj_agencia_w;
		
			end if;	
		
		elsif (cd_pessoa_fisica_w is not null) then
		
			select	max(a.cd_cnpj_agencia)
			into	cd_cnpj_agencia_w
			from	pessoa_fisica_conta a 
			where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
			
			
			if 	(nvl(cd_cnpj_agencia_w,'X') <> 'X')then
			
				select	max(cd_rfc)
				into	ds_retorno_w
				from 	pessoa_juridica
				where	cd_cgc = cd_cnpj_agencia_w;
		
			end if;	
		
		end if;

	elsif   (ie_opcao_p = 'B') then -- nome do banco
		
		if (cd_cgc_w is not null) then
		
			select	max(a.cd_cnpj_agencia),
				max(obter_nome_banco(a.cd_banco))			
			into	cd_cnpj_agencia_w,
				ds_banco_w
			from	pessoa_juridica_conta a 
			where	a.cd_cgc = cd_cgc_w;	

				if 	(nvl(cd_cnpj_agencia_w,'X') <> 'X') then
				
					select  max(nr_seq_pais)
					into    nr_seq_pais_w
					from 	pessoa_juridica
					where   cd_cgc = cd_cnpj_agencia_w;
					
					select 	nvl(max(obter_dados_pais(nr_seq_pais_w,'SG')),'MEX')
					into   	sg_pais_w
					from 	dual;
				
					if (sg_pais_w <> 'MEX') then
					
						ds_retorno_w := ds_banco_w;
					
					end if;					
				end if;
		
		elsif (cd_pessoa_fisica_w is not null) then
			
			select	max(a.cd_cnpj_agencia),
				max(obter_nome_banco(a.cd_banco))			
			into	cd_cnpj_agencia_w,
				ds_banco_w
			from	pessoa_fisica_conta a 
			where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;	

				if 	(nvl(cd_cnpj_agencia_w,'X') <> 'X') then
				
					select  max(nr_seq_pais)
					into    nr_seq_pais_w
					from 	pessoa_juridica
					where   cd_cgc = cd_cnpj_agencia_w;
					
					select 	nvl(max(obter_dados_pais(nr_seq_pais_w,'SG')),'MEX')
					into   	sg_pais_w
					from 	dual;
				
					if (sg_pais_w <> 'MEX') then
					
						ds_retorno_w := ds_banco_w;
					
					end if;
					
				end if;			
		end if;
			
		
	elsif   (ie_opcao_p = 'C') then	 -- numero da conta do banco da pessoa do t�tulo
	
		if (cd_cgc_w is not null) then
			
			select	max(b.nr_conta)
			into	ds_retorno_w
			from	pessoa_juridica a,
				pessoa_juridica_conta b,
				titulo_receber c
			where   a.cd_cgc = b.cd_cgc
			and     a.cd_cgc = c.cd_cgc
			and     c.nr_titulo = nr_titulo_p
			and     exists (select 1 
					from  movto_banco_pend_baixa
					where nr_titulo = nr_titulo_p
					and   nr_seq_baixa = nr_seq_baixa_p);
					
		elsif (cd_pessoa_fisica_w is not null)	then		

			select	max(b.nr_conta)
			into	ds_retorno_w
			from	pessoa_fisica a,
				pessoa_fisica_conta b,
				titulo_receber c
			where   a.cd_pessoa_fisica = b.cd_pessoa_fisica
			and     a.cd_pessoa_fisica = c.cd_pessoa_fisica
			and     c.nr_titulo = nr_titulo_p
			and     exists (select 1 
					from  movto_banco_pend_baixa
					where nr_titulo = nr_titulo_p
					and   nr_seq_baixa = nr_seq_baixa_p);
		end if;
		
		
	elsif 	(ie_opcao_p = 'CONTAB') then -- conta do banco do controle de credito n�o identificado

		select  max(b.cd_conta)
		into	ds_retorno_w
		from    movto_banco_pend a,
			banco_estabelecimento_v b,
			agencia_bancaria c,
			movto_banco_pend_baixa d
		where   a.nr_sequencia = d.nr_seq_movto_pend
		and     b.nr_sequencia = a.nr_seq_conta_banco
		and     b.cd_agencia_bancaria = c.cd_agencia_bancaria
		and     d.nr_titulo = nr_titulo_p
		and     d.nr_seq_baixa = nr_seq_baixa_p;
		
		
	elsif 	(ie_opcao_p = 'RFCB') then  -- rfc da agencia bancaria do conta do controle de credito n�o identificado
	
		select	max(obter_dados_pf_pj(null,c.cd_cgc_agencia,'RFC'))
		into	ds_retorno_w
		from    movto_banco_pend a,
			banco_estabelecimento_v b,
			agencia_bancaria c,
			movto_banco_pend_baixa d
		where 	a.nr_sequencia = d.nr_seq_movto_pend
		and     b.nr_sequencia = a.nr_seq_conta_banco 
		and   	b.cd_agencia_bancaria = c.cd_agencia_bancaria
		and   	d.nr_titulo = nr_titulo_p
		and    	d.nr_seq_baixa = nr_seq_baixa_p;
	
	end if;	

return	ds_retorno_w;

end obter_dados_control_mx;
/
