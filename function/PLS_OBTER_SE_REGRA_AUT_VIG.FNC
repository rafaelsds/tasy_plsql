create or replace
function pls_obter_se_regra_aut_vig(	cd_material_p	material.cd_material%type) return varchar2 is
--verifica se existe alguma regra de autoriza��o para o material vigente.

ie_regra_vigente_w	varchar2(1);

begin
-- inicia n�o tendo regra vigente
ie_regra_vigente_w := 'N';

-- se foi passado o material faz a consulta
if	(cd_material_p is not null) then

	select	nvl(max(a.ie_liberado), 'N')
	into	ie_regra_vigente_w
	from	pls_regra_autorizacao a,
		pls_material b
	where	b.cd_material = cd_material_p
	and	a.nr_seq_material = b.nr_sequencia
	and	sysdate between a.dt_inicio_vigencia_ref and a.dt_fim_vigencia_ref
	and	a.ie_situacao = 'A';
end if;

return ie_regra_vigente_w;

end pls_obter_se_regra_aut_vig;
/