create or replace
function Gerint_desc_de_para(ds_param_p	varchar2)
 		    	return varchar2 is
				
	--TYPE dpExp IS TABLE OF VARCHAR2(140) INDEX BY VARCHAR2(35);
	type hash_map is table of varchar2(1000) index by varchar2(45);
  dpExp hash_map ;
  ds_retorno_w Varchar2(500);
	
begin	
	if	(ds_param_p is null) then	
		return null;
	end if;
		
/* 14.1 Tabela de c�digos dos tipos de leitos */

/* CIR�RGICO */
	dpExp('1') := '350984';
	dpExp('2') := '350940';	
	dpExp('3') := '798009';
	dpExp('4') := '798011';
	dpExp('5') := '488413';
	dpExp('6') := '315116';
	dpExp('8') := '798035';
	dpExp('9') := '294090';
	dpExp('11') := '350990';
	dpExp('12') := '632419';
	dpExp('13') := '798037';
	dpExp('14') := '498072';
	dpExp('15') := '498759';
	dpExp('16') := '798041';
	dpExp('67') := '300421';
	dpExp('90') := '798043';
	dpExp('91') := '798045';
/* CL�NICO */
	dpExp('31') := '283312';
	dpExp('32') := '350940';
	dpExp('33') := '487614';
	dpExp('35') := '500043';
	dpExp('36') := '488587';
	dpExp('37') := '799036';
	dpExp('38') := '487971';
	dpExp('40') := '799044';
	dpExp('41') := '487970';
	dpExp('42') := '350989';
	dpExp('44') := '632419';
	dpExp('46') := '350941';
	dpExp('87') := '298057';
	dpExp('88') := '798043';
	dpExp('89') := '798045';
/* COMPLEMENTAR */
	dpExp('65') := '8';
	dpExp('66') := '632429';
	dpExp('74') := '799046';
	dpExp('75') := '799048';
	dpExp('76') := '799050';
	dpExp('77') := '799064';
	dpExp('78') := '799066';
	dpExp('79') := '799068';
	dpExp('80') := '799070';
	dpExp('81') := '799072';
	dpExp('82') := '799074';
	dpExp('83') := '632424';
	dpExp('85') := '799076';
	dpExp('92') := '799078';
	dpExp('93') := '799080';
	dpExp('94') := '799082';
	dpExp('95') := '799084';
/* OBST�TRICO */
	dpExp('10') := '799086';	
	dpExp('43') := '799090';
/* PEDIATRICO */
	dpExp('45') := '799098';
	dpExp('68') := '799124';
/* OUTRAS ESPECIALIDADES */		
	dpExp('34') := '799126';
	dpExp('47') := '331484';
	dpExp('48') := '632418';
	dpExp('49') := '799128';
	dpExp('84') := '799130';
/* HOSPITAL DIA */		
	dpExp('7') := '799132';
	dpExp('69') := '283312';
	dpExp('71') := '800837';
	dpExp('72') := '488587';
	dpExp('73') := '298057';
	
/* 14.2 Tabela de valores dos tipos de leitos de interna��o	*/
	dpExp('ENFADULTO') := '800839';
	dpExp('ENFPEDIATRICA') := '800841';
	dpExp('UTIADULTO') := '632422';
	dpExp('UTIPEDIATRICA') := '632423';
	dpExp('UTINEONATAL') := '632427';
	dpExp('PSIQUIATRICO') := '487598';
	dpExp('OBSTETRICO') := '632421';
	dpExp('HOSPDIA') := '487244';
/* 14.3 Tabela de valores dos tipos de acesso */
	dpExp('espontaneo') := '799134';
	dpExp('samuPoa') := '799136';
	dpExp('samuEstado') := '799138';
	dpExp('ambulatorioProprio') := '799140';
	dpExp('outraInstituicao') := '799142';
/* 14.4 Tabela de valores de sens�rio */
	dpExp('ALERTA') := '326438';
	dpExp('RESPONDE_A_VOZ') := '800843';
	dpExp('RESPONDE_A_DOR') := '800845';
	dpExp('IRRESPONSIVO') := '650907';
	dpExp('NAO_AVALIADO') := '491628';
/* 14.5 Tabela de valores do d�bito urin�rio */
	dpExp('NORMAL') := '294264';
	dpExp('OLIGURIA') := '799144';
	dpExp('ANURIA') := '799146';
	dpExp('NAO_AVALIADO') := '491628';
/* 14.6 Tabela de valores dos tipos de suporte O2 */
	dpExp('AR_AMBIENTE') := '762287';
	dpExp('OCULOS_OU_CATETER_NASAL') := '799148';
	dpExp('CAMPANULA') := '799150';
	dpExp('MASCARA_VENTURINI_OU_HUDSON') := '799152';
	dpExp('VENTILACAO_MECANICA') := '301626';
/* 14.7 Tabela de valores dos motivos de bloqueio de leito */
	dpExp('reservado') := '331547';
	dpExp('desinfeccao_leito') := '799156';
	dpExp('interditado') := '307834';
	dpExp('manutencao') := '740836';
	dpExp('isolamento') := '292257';
	dpExp('acompanhante') := '283172';	
/* 14.8 Tabela de valores de cor */	
	dpExp('VERMELHO') := '799632';
	dpExp('LARANJA') := '799634';
	dpExp('AMARELO') := '799636';
	dpExp('VERDE') := '799638';
	dpExp('AZUL') := '799640';
	dpExp('NENHUM') := '799642';
/* 14.9 Tabela de valores da condi��o de paciente sem cart�o SUS/CPF */
	dpExp('RECEM_NASCIDO') := '701340';
	dpExp('SITUACAO_RUA') := '701702';
	dpExp('INTRA_UTERO') := '799962';
	dpExp('DOADOR_ORGAOS') := '799964';
	dpExp('PACIENTE_IGNORADO') := '799966';
			
	dpExp('C') := 'CMCE';
	dpExp('A') := 'AGHOS';
	dpExp('N') := 'CNRAC';
	dpExp('G') := 'GERCON';
	dpExp('S') := 'SMC';
	
	dpExp('QUERY') := 'begin Gerar_evento_gerint(:nm_usuario_p, :cd_estabelecimento_p, :id_evento_p, :nr_atendimento_p, :ie_leito_extra_p, :dt_alta_p, :ds_motivo_alta_p, :ds_justif_transferencia_p, :nr_seq_solic_internacao_p, :nr_seq_leito_p, :ds_ident_leito_p, :nr_seq_classif_p, :ie_status_unidade_p, :nr_cpf_paciente_p, :nr_cartao_sus_p, :cd_cid_p, :cd_evolucao_p); end;';
	
	if	(REGEXP_INSTR (dpExp(ds_param_p), '[^[:digit:]]') = 0) then 
		ds_retorno_w := obter_desc_expressao(dpExp(ds_param_p));		
	else		
		ds_retorno_w := dpExp(ds_param_p);
	end if;
	


return	ds_retorno_w;

end Gerint_desc_de_para;
/
