CREATE OR REPLACE
FUNCTION sigh_obter_espec_medico
		(cd_medico_p 	Varchar2)	
		return Varchar2 IS

cd_especialidade_w		Varchar2(255)	:= '';
ds_retorno_w			varchar2(4000);

cursor	c01 is
	select 	b.cd_especialidade
	from 	especialidade_medica b,
		medico_especialidade a
	where 	a.cd_pessoa_fisica	= cd_medico_p
	and	a.cd_especialidade	= b.cd_especialidade;
	
BEGIN

open	c01;
loop
fetch	c01 into cd_especialidade_w;
	exit when c01%notfound;
	begin
	
	ds_retorno_w	:= ds_retorno_w || cd_especialidade_w ||',' ;

	end;
end loop;
close c01;

ds_retorno_w	:= substr(ds_retorno_w, 1, length(ds_retorno_w) - 1);

RETURN ds_retorno_w;
END sigh_obter_espec_medico;
/
