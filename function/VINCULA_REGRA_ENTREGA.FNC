create or replace procedure vincula_regra_entrega(nr_prescricao_p number,
												  nr_seq_prescricao_p number,
												  nm_usuario_p varchar2,
												  ie_opcao_p varchar2) is 

nr_seq_proc_compl_w PRESCR_PROCEDIMENTO_COMPL.NR_SEQUENCIA%TYPE;
nr_seq_estagio_w prescr_procedimento_compl.nr_seq_estagio_entrega%type;
ie_status_execucao_w prescr_procedimento.ie_status_execucao%type;
nr_regra_w number;
nr_sequencia_w		number(10);
													  
begin

select nr_seq_proc_compl, ie_status_execucao
into nr_seq_proc_compl_w, ie_status_execucao_w
from prescr_procedimento
where nr_prescricao = nr_prescricao_p
and   nr_sequencia = nr_seq_prescricao_p;

if(ie_opcao_p = 'V') then
	nr_regra_w := obter_regra_entrega_apropriada(nr_prescricao_p,nr_seq_prescricao_p);
	
	if(nr_seq_proc_compl_w is null) then
		select	prescr_procedimento_compl_seq.nextval
		into	nr_sequencia_w
		from	dual;
	
		insert into prescr_procedimento_compl (	nr_sequencia,
												dt_atualizacao,
												nm_usuario,
												dt_atualizacao_nrec,
												nm_usuario_nrec) 
									values    (	nr_sequencia_w,
												sysdate,
												nm_usuario_p,
												sysdate,
												nm_usuario_p);
												
        													
		nr_seq_proc_compl_w := nr_sequencia_w;
		
		update prescr_procedimento
		set nr_seq_proc_compl = nr_seq_proc_compl_w,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where nr_prescricao = nr_prescricao_p
		and   nr_sequencia = nr_seq_prescricao_p;
	end if;

	nr_seq_estagio_w := obter_estagio_inicial(nr_regra_w, ie_status_execucao_w);	

elsif (ie_opcao_p = 'D') then
	update prescr_procedimento_compl
	set nr_seq_estagio_entrega = null,
	    nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where nr_sequencia = nr_seq_proc_compl_w;

end if;


update prescr_procedimento_compl
set nr_seq_estagio_entrega = nr_seq_estagio_w,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where nr_sequencia = nr_seq_proc_compl_w;


gravar_auditoria_entrega(nr_seq_estagio_w, 
						 nr_prescricao_p, 
						 nr_seq_prescricao_p,
						 nm_usuario_p);

commit;
end vincula_regra_entrega;
/