create or replace
function pls_substituir_macros(	ds_mensagem_p		varchar2,
				nr_seq_requisicao_p 	number,
				nr_seq_auditoria_p 	number,
				nr_seq_segurado_p 	number,
				cd_lista_proc_p		varchar2,
				ie_lista_orig_proc_p	varchar2,
				cd_lista_mat_p		varchar2 default null,
				ie_lista_status_p	varchar2 default null) return varchar2 is

ds_mensagem_regra_w		pls_alerta_evento_mensagem.ds_mensagem%type;
dt_validade_senha_w		varchar2(20);
ie_tipo_intercambio_w		pls_requisicao.ie_tipo_intercambio%type;
nm_beneficiario_w		pessoa_fisica.nm_pessoa_fisica%type;
cd_usuario_plano_w		pls_segurado_carteira.cd_usuario_plano%type;
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;
cd_guia_w			pls_auditoria.cd_guia%type;
ds_procedimento_w		varchar2(4000) := '';
ds_proc_interno_w 		varchar2(4000) := '';
cd_carteirinha_antiga_w		varchar2(20);
nr_seq_plano_ant_w		pls_segurado.nr_seq_plano%type;
cd_plano_novo_w			pls_segurado.nr_seq_plano%type;
ds_plano_ant_w			pls_plano.ds_plano%type;
ds_plano_novo_w			pls_plano.ds_plano%type;
dt_referencia_w			pls_mensalidade.dt_referencia%type;
cd_operadora_empresa_w		pls_contrato.cd_operadora_empresa%type;
nm_pagador_w			Varchar2(80);
nr_contrato_w			pls_contrato.nr_contrato%type;
cd_procedimento_w		number(10);
ds_procedimento_ww		varchar2(100);
cd_lista_proc_w			varchar2(4000);
ie_lista_ori_proc_w		varchar2(4000);
ie_lista_status_proc_w		varchar2(4000);
ie_origem_proc_w		number(10);
ie_status_proc_w		varchar2(255);
ie_status_mat_w			varchar2(255);
nm_prestador_w			varchar2(255);

cd_lista_mat_w			varchar2(4000);
ds_material_w			varchar2(4000) := '';
ds_material_ptu_w		varchar2(4000) := '';
ds_mat_ptu_ou_orig_w		varchar2(4000) := '';
ds_marca_w			varchar2(4000) := '';
nr_seq_item_w			pls_auditoria_item.nr_sequencia%type;
ds_material_ww			varchar2(4000) := '';
ds_material_ptu_ww		varchar2(4000) := '';
nr_seq_material_w		pls_material.nr_sequencia%type;

ds_procedimento_quebra_w	varchar2(4000) := '';
ds_proc_interno_quebra_w	varchar2(4000) := '';
ds_material_quebra_w		varchar2(4000) := '';
ds_material_ptu_quebra_w	varchar2(4000) := '';
ds_mat_ptu_ou_orig_quebra_w	varchar2(4000) := '';

nm_prestador_exec_w		pls_prestador.nm_interno%type;
dt_entrada_hospital_w		pls_requisicao.dt_entrada_hospital%type;

begin
if(cd_lista_proc_p is not null) then
	ds_procedimento_w := '';
	ds_proc_interno_w := '';
	ds_procedimento_quebra_w := '';
	ds_proc_interno_quebra_w := '';
	cd_lista_proc_w := cd_lista_proc_p;
	ie_lista_ori_proc_w := ie_lista_orig_proc_p;
	ie_lista_status_proc_w := ie_lista_status_p;
	while length(cd_lista_proc_w) > 0 loop
		begin
		cd_procedimento_w		:= substr(cd_lista_proc_w, 1, instr(cd_lista_proc_w, ',')-1); --isolar cd_procedimento
		ie_origem_proc_w		:= substr(ie_lista_ori_proc_w, 1, instr(ie_lista_ori_proc_w, ',')-1);
		ie_status_proc_w		:= substr(ie_lista_status_proc_w, 1, instr(ie_lista_status_proc_w, ',')-1);
		ds_procedimento_w		:= ds_procedimento_w || obter_descricao_procedimento(cd_procedimento_w, ie_origem_proc_w) || ', ';
		ds_proc_interno_w		:= ds_proc_interno_w || obter_desc_int_proc(cd_procedimento_w, ie_origem_proc_w) || ', ';
		ds_procedimento_quebra_w	:= substr(ds_procedimento_quebra_w || obter_descricao_procedimento(cd_procedimento_w, ie_origem_proc_w) || ' - Status: '|| ie_status_proc_w || ',' || chr(13),1,4000);
		ds_proc_interno_quebra_w	:= substr(ds_proc_interno_quebra_w || obter_desc_int_proc(cd_procedimento_w, ie_origem_proc_w) || ' - Status: '|| ie_status_proc_w || ',' || chr(13),1,4000);
		cd_lista_proc_w			:= replace(cd_lista_proc_w, cd_procedimento_w||','); --apagar procedimento usado
		ie_lista_ori_proc_w		:= substr(ie_lista_ori_proc_w, instr(ie_lista_ori_proc_w, ',')+1, length(ie_lista_ori_proc_w));--apagar ie_origem do procedimento usado
		ie_lista_status_proc_w		:= substr(ie_lista_status_proc_w, instr(ie_lista_status_proc_w, ',')+1, length(ie_lista_status_proc_w));
		end;
	end loop;
end if;

if(cd_lista_mat_p is not null) then
	ds_material_w			:= '';
	ds_material_ptu_w		:= '';
	ds_mat_ptu_ou_orig_w		:= '';
	ds_material_quebra_w		:= '';
	ds_material_ptu_quebra_w	:= '';
	ds_mat_ptu_ou_orig_quebra_w	:= '';
	
	
	cd_lista_mat_w		:= substr(cd_lista_mat_p, 1, length(cd_lista_mat_p)-1);
	while length(cd_lista_mat_w) > 0 loop
		begin
		if (instr(cd_lista_mat_w, ',') > 0) then
			nr_seq_item_w	:= substr(cd_lista_mat_w, 1, instr(cd_lista_mat_w, ',')-1);
		else
			nr_seq_item_w := cd_lista_mat_w;
		end if;

		if	(nr_seq_item_w is not null) then
			select	pls_obter_desc_material_regra(a.nr_seq_material),
				pls_obter_item_desc_conv_scs(a.nr_sequencia,'M','D'),
				a.nr_seq_material,
				decode(a.ie_status,'A','Aprovado','N','Negado','Pendente')
			into	ds_material_ww,
				ds_material_ptu_ww,
				nr_seq_material_w,
				ie_status_mat_w
			from	pls_auditoria_item a
			where	a.nr_sequencia = nr_seq_item_w;

			begin
				select	b.ds_marca
				into	ds_marca_w
				from	marca b,
					pls_material c
				where	b.nr_sequencia = c.nr_seq_marca
				and	c.nr_sequencia = nr_seq_material_w;
			exception
			when others then
				ds_marca_w := null;
			end;

			if	(ds_material_ww is not null) then
				if	(ds_marca_w is not null) then
					ds_material_ww := ds_material_ww || ' - Marca: ' || ds_marca_w;
				end if;

				ds_material_w			:= substr(ds_material_w || ds_material_ww || ', ',1,4000);
				ds_material_quebra_w		:= substr(ds_material_quebra_w || ds_material_ww || ' - Status: '|| ie_status_mat_w || ',' || chr(13),1,4000);
			end if;

			if	(ds_material_ptu_ww is not null) then
				if	(ds_marca_w is not null) then
					ds_material_ptu_ww := ds_material_ptu_ww || ' - Marca: ' || ds_marca_w;
				end if;

				ds_material_ptu_w		:= substr(ds_material_ptu_w || ds_material_ptu_ww || ', ',1,4000);
				ds_material_ptu_quebra_w	:= substr(ds_material_ptu_quebra_w || ds_material_ptu_ww || ' - Status: '|| ie_status_mat_w || ',' || chr(13),1,4000);
			end if;

			if	(ds_material_ww is not null or ds_material_ptu_ww is not null) then
				ds_mat_ptu_ou_orig_w		:= substr(ds_mat_ptu_ou_orig_w || nvl(ds_material_ptu_ww, ds_material_ww) || ', ',1,4000);
				ds_mat_ptu_ou_orig_quebra_w	:= substr(ds_mat_ptu_ou_orig_quebra_w || nvl(ds_material_ptu_ww, ds_material_ww) || ' - Status: '|| ie_status_mat_w || ',' || chr(13),1,4000);
			end if;
		end if;

		if	(instr(cd_lista_mat_w, ',') = 0)then
			cd_lista_mat_w	:= null;
		else
			cd_lista_mat_w	:= substr(cd_lista_mat_w, length(nr_seq_item_w) +2, length(cd_lista_mat_w));
		end if;
		end;
	end loop;
end if;

-- in�cio nr_seq_requisicao
if(nr_seq_requisicao_p is not null)then
	select	to_char(r.dt_validade_senha, 'DD/MM/YYYY'),
		substr(pls_obter_dados_segurado(r.nr_seq_segurado, 'N'),1,70) nm_beneficiario,
		substr(pls_obter_dados_segurado(r.nr_seq_segurado,'CR'),1,20) cd_usuario_plano,
		substr(pls_obter_dados_prestador(nvl(r.nr_seq_prestador_exec, r.nr_seq_prestador), 'N'),1,255) nm_prestador,
		substr(pls_obter_dados_prestador(r.nr_seq_prestador_exec, 'N'),1,255) nm_prestador_exec,
		to_char(r.dt_entrada_hospital, 'DD/MM/YYYY') dt_entrada_hospital
	into	dt_validade_senha_w,
		nm_beneficiario_w,
		cd_usuario_plano_w,
		nm_prestador_w,
		nm_prestador_exec_w,
		dt_entrada_hospital_w
	from	pls_requisicao r
	where	r.nr_sequencia	= nr_seq_requisicao_p;
end if;
-- fim nr_seq_requisicao

--inicio nr_seq_auditoria_p
if(nr_seq_auditoria_p is not null)then
	select	nr_seq_requisicao,
		cd_guia,
		substr(pls_obter_dados_segurado(nr_seq_segurado, 'N'),1,70) nm_beneficiario,
		substr(pls_obter_dados_segurado(nr_seq_segurado,'CR'),1,20) cd_usuario_plano
	into	nr_seq_requisicao_w,
		cd_guia_w,
		nm_beneficiario_w,
		cd_usuario_plano_w
	from	pls_auditoria
	where	nr_sequencia = nr_seq_auditoria_p;
end if;

if	(nr_seq_requisicao_w is not null) then
	begin
		select	to_char(dt_validade_senha, 'DD/MM/YYYY') dt_validade_senha,
			substr(pls_obter_dados_prestador(nr_seq_prestador_exec, 'N'),1,255) nm_prestador_exec,
			to_char(dt_entrada_hospital, 'DD/MM/YYYY') dt_entrada_hospital
		into	dt_validade_senha_w,
			nm_prestador_exec_w,
			dt_entrada_hospital_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
	exception
	when others then
		dt_validade_senha_w	:= null;
		nm_prestador_exec_w	:= null;
		dt_entrada_hospital_w	:= null;
	end;
end if;
--fim nr_seq_auditoria_p

ds_mensagem_regra_w := ds_mensagem_p;

--Estas macros tem o mesmo tratamento de retorno das abaixo, apenas pulando a linha ao inv�s de concatenar em uma �nica linha toda as descri��es	OS 1779845
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_PROC_QUEBRA', ds_procedimento_quebra_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_PROC_INT_QUEBRA', ds_proc_interno_quebra_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_MAT_MARCA_QUEBRA', ds_material_quebra_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_MAT_PTU_MARCA_QUEBRA', ds_material_ptu_quebra_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_MAT_PTU_ORIG_MARCA_QUEBRA', ds_mat_ptu_ou_orig_quebra_w), 1, 4000);

ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@ANALISE', nr_seq_auditoria_p), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@BENEFICIARIO', nm_beneficiario_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@CARTEIRA_BENEFICIARIO', cd_usuario_plano_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@CARTEIRA_BENEF_TITULAR', nm_beneficiario_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@CATEIRINHA_ANTIGA', cd_carteirinha_antiga_w), 1, 4000); -- vem do nr_seq_segurado_p da procedure: pls_alerta_alteracao_plano
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@CD_GUIA', cd_guia_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@CD_OPERADORA_EMPRESA', cd_operadora_empresa_w), 1, 4000); --vem do pls_obter_dados_contrato(nr_seq_contrato_w,'CD') da procedure pls_atualizar_msg_mens_atraso
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@CD_PLANO_ANTERIOR', nr_seq_plano_ant_w), 1, 4000); -- vem do nr_seq_segurado_p da procedure: pls_alerta_alteracao_plano
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@CD_PLANO_NOVO', cd_plano_novo_w), 1, 4000);  -- vem do nr_seq_segurado_p da procedure: pls_alerta_alteracao_plano
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_PLANO_ANTERIOR', ds_plano_ant_w), 1, 4000); -- vem do nr_seq_segurado_p da procedure: pls_alerta_alteracao_plano
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_PLANO_NOVO', ds_plano_novo_w), 1, 4000); -- vem do nr_seq_segurado_p da procedure: pls_alerta_alteracao_plano
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_PROCEDIMENTO', ds_procedimento_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_PROC_INTERNO', ds_proc_interno_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DT_REFERENCIA_MENSALIDADE', to_char(dt_referencia_w, 'dd/mm/yyyy')), 1, 4000); -- vem do pls_atualizar_msg_mens_atraso
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DT_VALIDADE_SENHA', dt_validade_senha_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NOME_PAGADOR', substr(nm_pagador_w,1,instr(nm_pagador_w,' '))), 1, 4000); --vem do pls_obter_dados_pagador(r_C03_w.nr_seq_pagador,'N'); procedure pls_atualizar_msg_mens_atraso
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NR_CONTRATO', nr_contrato_w), 1, 4000); --vem do pls_obter_dados_contrato(nr_seq_contrato_w,'N') da procedure pls_atualizar_msg_mens_atraso
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@PAGADOR', nm_pagador_w), 1, 4000);  --vem do pls_obter_dados_pagador(r_C03_w.nr_seq_pagador,'N'); procedure pls_atualizar_msg_mens_atraso
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@PRESTADOR', nm_prestador_w), 1, 4000);
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@REQUISICAO', nvl(nr_seq_requisicao_p, nr_seq_requisicao_w)), 1, 4000); --Usa nr_seq_requisicao_w quando o par�metro for da auditoria

ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_MATERIAL_MARCA', ds_material_w), 1, 4000);		--Apresentar a descri��o original do material da tabela pls_material concatenado com a marca da tabela pls_material
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_MATERIAL_PTU_MARCA', ds_material_ptu_w), 1, 4000);	--Apresentar a descri��o gen�rica do material (campo descri��o OPME da tabela pls_requisicao_mat) concatenado com a marca da tabela pls_material
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_MAT_PTU_OU_ORIG_MARCA', ds_mat_ptu_ou_orig_w), 1, 4000);	--Apresentar primeiramente caso existir a informa��o no campo descri��o OPME da tabela pls_requisicao_mat, se n�o existir essa informa��o apresentar descri��o original do material da tabela pls_material concatenado com a marca da tabela pls_material

ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NM_PRESTADOR_EXECUTOR', nm_prestador_exec_w), 1, 4000);	--Retorna o prestador executor da requisi��o (NM_PRESTADOR_EXEC)
ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DT_ENTRADA_HOSPITAL', to_char(dt_entrada_hospital_w, 'dd/mm/yyyy')), 1, 4000);		--Retorna a data de entrada no hospital da requisi��o (DT_ENTRADA_HOSPITAL)

return	ds_mensagem_regra_w;

end pls_substituir_macros;
/
