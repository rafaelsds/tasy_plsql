create or replace
function obter_inf_dieta_adep	(nr_prescricao_p	number,
				nr_seq_dieta_p		number,
				ie_opcao_p		varchar2)
				return varchar2 is

ds_dieta_w		varchar2(80);
ds_obs_dieta_w		varchar2(4000);
ds_inf_w		varchar2(2000);
ie_suspenso_w		varchar2(2);
dt_suspensao_w		date;
qt_horas_jejum_w	number(2);
qt_parametro_w		number(15,4);
ds_um_w			varchar2(255);

cursor c01 is
select	substr(obter_nome_dieta(cd_dieta),1,80),
	ds_observacao,
	nvl(ie_suspenso,'N'),
	dt_suspensao ,
	qt_horas_jejum, 
	qt_parametro,
	substr(obter_unid_med_dieta(cd_dieta),1,150)
from	prescr_dieta
where	nr_prescricao = nr_prescricao_p
and	nr_sequencia	= nvl(nr_seq_dieta_p,nr_sequencia)
order by
	nr_sequencia;	

begin
if	(nr_prescricao_p is not null) and
	(ie_opcao_p is not null) then
	open c01;
	loop
	fetch c01 into	
		ds_dieta_w,
		ds_obs_dieta_w,
		ie_suspenso_w,
		dt_suspensao_w,
		qt_horas_jejum_w,
		qt_parametro_w,
		ds_um_w;
	exit when c01%notfound;
		begin
		if	(ie_opcao_p = 'D') and
			(ds_dieta_w is not null) and
			(ie_suspenso_w = 'N')	then
			ds_inf_w := substr(ds_inf_w || ' - ' || ds_dieta_w || chr(10),1,2000);
		elsif (ie_opcao_p = 'O') and
			  (ds_obs_dieta_w is not null) then
			  ds_inf_w := substr(ds_inf_w || ' - ' || obter_desc_expressao(692668) || ds_obs_dieta_w || chr(10),1,2000);
		elsif (ie_opcao_p = 'D') and
			  (ds_dieta_w is not null) and
			  (ie_suspenso_w = 'S')	then
			  ds_inf_w := substr(ds_inf_w || ' - ' || ds_dieta_w || ' - '||obter_desc_expressao(298988)|| to_char(dt_suspensao_w, 'dd/mm/yyyy hh24:mi:ss') || chr(10),1,2000);		
		elsif	(ie_opcao_p = 'DI') and
			(ds_dieta_w is not null) and
			(ie_suspenso_w = 'N')	then
			if (qt_horas_jejum_w is not null) then
				ds_inf_w := substr(ds_inf_w || ' - ' || ds_dieta_w || obter_desc_expressao(346208, 'dt_validade_w='||qt_horas_jejum_w)||' hr'||chr(10),1,2000);		
			else
				ds_inf_w := substr(ds_inf_w || ' - ' || ds_dieta_w ||chr(10),1,2000);
			end if;
		elsif	(ie_opcao_p = 'DIO') and
			(ds_dieta_w is not null) and
			(ie_suspenso_w = 'N')	then
			if 	(qt_horas_jejum_w is not null) then
				ds_inf_w := substr(ds_inf_w || ' - ' || ds_dieta_w ||obter_desc_expressao(346208, 'dt_validade_w='||qt_horas_jejum_w)||' hr'||chr(10),1,2000);		
			else
				ds_inf_w := substr(ds_inf_w || ' - ' || ds_dieta_w ||chr(10),1,2000);
			end if;
			if	(ds_obs_dieta_w is not null) then
				ds_inf_w := substr(ds_inf_w || chr(10)||obter_desc_expressao(692668) ||ds_obs_dieta_w||chr(10),1,2000);
			end if;
		end if;
		end;
		
		if	(qt_parametro_w > 0) and (qt_parametro_w < 1) then
			ds_inf_w := substr('0' || qt_parametro_w || ' ' ||ds_um_w || ' - ' ||  ds_inf_w,1,2000); 
		elsif	(qt_parametro_w > 0) then
			ds_inf_w := substr(qt_parametro_w || ' ' ||ds_um_w || ' - ' ||  ds_inf_w,1,2000); 
		end if;
	end loop;
	close c01;
end if;

return ds_inf_w;

end obter_inf_dieta_adep;
/