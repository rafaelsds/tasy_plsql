create or replace
function sugest_dias_futuros_uteis(dt_inicial_p		date,
											qt_dias_p		number,
                      cd_estabelecimento_p number)
											return date is

dt_aux_w			date;
ie_dia_semana_w		number(1);
ie_feriado_w		number(1);
qt_dias_atend_w		number(2);
qt_limitador_w		number(10) := 1;
ie_dia_util			varchar2(1);

begin

if	(dt_inicial_p is not null) then

	dt_aux_w		:= trunc(dt_inicial_p, 'dd');	
	ie_dia_util		:= obter_se_dia_util(dt_aux_w, cd_estabelecimento_p);	
	qt_dias_atend_w := qt_dias_p;

	if qt_dias_atend_w > 0 then
		qt_dias_atend_w := qt_dias_atend_w - 1;
	end if;
	
	
	while	((qt_dias_atend_w > 0) or (ie_dia_util = 'N')) and (qt_limitador_w < 1000) loop
		begin

		ie_dia_semana_w := obter_cod_dia_semana(dt_aux_w);
		ie_feriado_w := obter_se_feriado(cd_estabelecimento_p, dt_aux_w);

		if	(ie_feriado_w = 0) and
			(ie_dia_semana_w in (2, 3, 4, 5,6)) then
						
			qt_dias_atend_w	:= qt_dias_atend_w - 1;
			dt_aux_w		:= dt_aux_w + 1;
		
		elsif	((ie_feriado_w > 0) or (ie_dia_semana_w in (1, 7))) then
			
			if	(ie_dia_semana_w = 7) then
				dt_aux_w		:= dt_aux_w + 2;
			else
				dt_aux_w		:= dt_aux_w + 1;
			end if;
			
		end if;
		
		ie_dia_util := obter_se_dia_util(dt_aux_w, cd_estabelecimento_p);
		qt_limitador_w := qt_limitador_w + 1;
		end;		
	end loop;	
else
	dt_aux_w	:= nvl(dt_inicial_p,sysdate);
end if;

return fim_dia(dt_aux_w);

end sugest_dias_futuros_uteis;
/
