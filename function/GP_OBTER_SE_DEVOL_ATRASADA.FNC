create or replace
function gp_obter_se_devol_atrasada (nr_seq_solic_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1) := 'N';
			
begin

if (nr_seq_solic_p is not null) then

	begin
	select	'S'
	into	ds_retorno_w
	from	same_solic_pront a
	where	a.nr_sequencia = nr_seq_solic_p
	and	a.dt_devolucao is null
	and	sysdate > nvl(a.dt_devolucao_prevista,sysdate);
	exception
	when others then
		ds_retorno_w := 'N';
	end;

end if;

return	ds_retorno_w;

end gp_obter_se_devol_atrasada;
/