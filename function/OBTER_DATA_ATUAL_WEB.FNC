Create or replace
Function obter_data_atual_web(ds_mascara_p	Varchar2)
				return Varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter data atual
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/				
				
ds_data_atual_w 	Varchar2(30);

begin

select	to_char(sysdate,ds_mascara_p)
into	ds_data_atual_w
from    dual;

return ds_data_atual_w;

end obter_data_atual_web;
/
