create or replace FUNCTION OBTER_SE_DIA_UTIL_ONCOLOGIA
   (dt_referencia_p       date,
    cd_estabelecimento_p  number,
    nm_usuario_p          varchar,
    ie_tipo_atendimento_p number default null,
    cd_protocolo_p        number default null,
    nr_seq_medicacao_p    number default null
   ) return varchar2 is

    ie_dia_semana_w	 varchar2(01);
    ie_feriado_w     varchar2(01);
    ie_dia_util_w    varchar2(01)	:= 'N';
    vl_parametro_w	 varchar2(255);
    vl_parametro_ww  varchar2(255) := null;
    EXEC_w           varchar2(200); 
BEGIN
    select nvl(obter_valor_param_usuario(281, 92, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'TD')
    into	 vl_parametro_w
    from	 dual;

    select obter_regra_dia_gera_ciclo(ie_tipo_atendimento_p,Obter_Perfil_Ativo,'',nm_usuario_p, cd_protocolo_p, nr_seq_medicacao_p)
    into   vl_parametro_ww
    from   dual;

    ie_dia_semana_w := to_char(pkg_date_utils.get_WeekDay(dt_referencia_p));

    select nvl(max('S'), 'N')
    into   ie_feriado_w
    from   feriado
    where  dt_feriado		between trunc(dt_referencia_p) and trunc(dt_referencia_P) + 86399/86400
    and	cd_estabelecimento	= cd_estabelecimento_p;

    begin
      EXEC_w := 'CALL OBTER_SE_DIA_UTIL_ONCOLOG_MD(:1,:2,:3,:4) INTO :result';
			
      EXECUTE IMMEDIATE EXEC_w USING IN vl_parametro_w,
                                     IN vl_parametro_ww,
                                     IN ie_dia_semana_w,
                                     IN ie_feriado_w,
                                     OUT ie_dia_util_w;
    exception
       when others then
          ie_dia_util_w := null;
    end;

    return ie_dia_util_w;

END OBTER_SE_DIA_UTIL_ONCOLOGIA;
/