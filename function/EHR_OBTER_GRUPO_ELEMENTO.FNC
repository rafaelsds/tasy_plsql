create or replace
function EHR_Obter_Grupo_Elemento(	nr_seq_grupo_p	Number)
				return varchar2 is

ds_retorno_w	varchar2(80);
	
begin

if	(nr_seq_grupo_p > 0) then
	begin
	select	ds_grupo
	into	ds_retorno_w
	from	ehr_grupo_elemento
	where	nr_sequencia	= nr_seq_grupo_p;
	end;
end if;

return	ds_retorno_w;

end EHR_Obter_Grupo_Elemento;
/