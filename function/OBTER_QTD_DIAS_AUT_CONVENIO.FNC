create or replace
function obter_qtd_dias_aut_convenio	(cd_convenio_p	number,
					 ie_tipo_p	varchar,
					 cd_estabelecimento_p	number)
					 return number is
					 
qt_tempo_autorizacao_w	number(10,0):=0;					 

begin


if	(nvl(cd_convenio_p,0) > 0) then
	if	(ie_tipo_p = 'M') then
	
		select	nvl(max(qt_dias_autorizacao),0)
		into	qt_tempo_autorizacao_w
		from	convenio_estabelecimento
		where	cd_convenio = cd_convenio_p
		and	 cd_estabelecimento = cd_estabelecimento_p;

		if	(nvl(qt_tempo_autorizacao_w,0) = 0) then
			select	nvl(max(qt_dias_autorizacao),0)
			into	qt_tempo_autorizacao_w
			from	convenio
			where	cd_convenio = cd_convenio_p;
		end if;
	elsif (ie_tipo_p = 'P') then
		select	nvl(max(qt_dias_autor_proc),0)
		into	qt_tempo_autorizacao_w
		from	convenio_estabelecimento
		where	cd_convenio = cd_convenio_p
		and	 cd_estabelecimento = cd_estabelecimento_p;
		
		if	(nvl(qt_tempo_autorizacao_w,0) = 0) then
			select	nvl(max(qt_dias_autor_proc),0)
			into	qt_tempo_autorizacao_w
			from	convenio
			where	cd_convenio = cd_convenio_p;
		end if;
	elsif (ie_tipo_p = 'C') then
		select	nvl(max(qt_dias_codificacao),0)
		into	qt_tempo_autorizacao_w
		from	convenio
		where	cd_convenio = cd_convenio_p;
	elsif (ie_tipo_p = 'T') then
		select	nvl(max(qt_dias_term_agenda),0)
		into	qt_tempo_autorizacao_w
		from	convenio
		where	cd_convenio = cd_convenio_p;	
	end if;	
end if;	

return 	qt_tempo_autorizacao_w;

end obter_qtd_dias_aut_convenio;
/
