CREATE OR REPLACE
FUNCTION Sus_Obter_desc_Classif
                        (       cd_parametro_p       Number,
				nr_seq_servico_p	number,
				ie_opcao_p		varchar2)
                                return Number is


nr_retorno_w         number(10);


BEGIN
if	(ie_opcao_p	= 'S') then
	select	max(nr_sequencia)
	into	nr_retorno_w
	from	sus_servico_classif
	where	cd_servico_classif	= cd_parametro_p
	and	nr_seq_servico		= nr_seq_servico_p;
elsif	(ie_opcao_p	= 'C') then
	select	max(CD_SERVICO_CLASSIF)
	into	nr_retorno_w
	from	sus_servico_classif
	where	nr_sequencia	= cd_parametro_p;

end if;


return nr_retorno_w;

END Sus_Obter_desc_Classif;
/