create or replace
function Obter_seq_ordenacao_regra(nr_seq_regra_p number, cd_pessoa_fisica_p number, cd_perfil_p number, cd_setor_atendimento_p number)
 		    	return number is
				
nr_seq_ordenacao_w number(10);
nr_sequencia_w number(10);
Cursor C01 is
	select decode(nvl(cd_pessoa_fisica,0),0,decode(nvl(cd_setor_atendimento,0),0,decode(nvl(cd_perfil,0),0,0,1),2),3)
	from	regra_prescr_proc_perfil 
	where	nr_seq_regra	= nr_seq_regra_p
	and		nvl(cd_perfil, cd_perfil_p)	= cd_perfil_p
	and		nvl(cd_pessoa_fisica, cd_pessoa_fisica_p) = cd_pessoa_fisica_p
	and		nvl(cd_setor_atendimento, cd_setor_atendimento_p) = cd_setor_atendimento_p;

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	nr_seq_ordenacao_w := nr_sequencia_w;
	end;
end loop;
close C01;

return	nr_seq_ordenacao_w;

end Obter_seq_ordenacao_regra;
/