create or replace function GET_BLOOD_ALLOCATION_RULE_5_OK(nr_seq_mensagem_p number, 
                                                    nr_seq_reserva_p number) 
                                                  return number is
                                                  
ie_retorno_w    number := 0;
qt_exame_w number := 0;

Cursor C01 is
    select nr_sequencia from san_exame where CD_TIPO_RESULTADO = 10053;

c01_w           c01%rowtype;

begin

open C01;
loop
fetch C01 into  
    c01_w;
exit when C01%notfound;
    begin

    if(nr_seq_mensagem_p = 1) then
        select count(c.NR_SEQ_EXAME)
            into qt_exame_w
            from san_reserva a, san_exame_lote b, san_exame_realizado c 
            where a.nr_sequencia = nr_seq_reserva_p
            and a.nr_sequencia = b.nr_seq_reserva
            and c.nr_seq_exame_lote = b.nr_sequencia
            and c.nr_seq_exame = c01_w.nr_sequencia
            and c.dt_liberacao is not null
            and (c.cd_exp_resultado = 291833 or c.cd_exp_resultado is null);
        
        if(qt_exame_w > 0) then
            ie_retorno_w:= ie_retorno_w+1;
        end if;
    elsif(nr_seq_mensagem_p = 2) then 
        select count(c.NR_SEQ_EXAME)
            into qt_exame_w
            from san_reserva a, san_exame_lote b, san_exame_realizado c 
            where a.nr_sequencia = nr_seq_reserva_p
            and a.nr_sequencia = b.nr_seq_reserva
            and c.nr_seq_exame_lote = b.nr_sequencia
            and c.nr_seq_exame = c01_w.nr_sequencia
            and c.dt_liberacao is not null
			and c.cd_exp_resultado = 291820;

        if(qt_exame_w > 0) then
            ie_retorno_w:= ie_retorno_w+1;
        end if;
    end if;

    end;
end loop;
close C01;

return ie_retorno_w;

end GET_BLOOD_ALLOCATION_RULE_5_OK;
/