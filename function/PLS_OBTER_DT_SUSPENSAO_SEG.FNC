create or replace
function pls_obter_dt_suspensao_seg(nr_seq_segurado_p	number)
						return date is
						
dt_retorno_w	date	:= null;

begin

if	(nr_seq_segurado_p is not null) then
	select	max(trunc(a.dt_inicio_suspensao))
	into	dt_retorno_w
	from	pls_segurado_suspensao a
	where	a.nr_seq_segurado	= nr_seq_segurado_p
	and	a.dt_fim_suspensao	is null;
	
	if	(dt_retorno_w is null) then
		select	max(trunc(a.dt_inicio_suspensao))
		into	dt_retorno_w
		from	pls_segurado_suspensao 	a,
			pls_segurado		b
		where	a.nr_seq_contrato 	= b.nr_seq_contrato
		and	b.nr_sequencia		= nr_seq_segurado_p
		and	a.dt_fim_suspensao	is null;		
	end if;
	
	if	(dt_retorno_w is null) then
		select	max(trunc(a.dt_inicio_suspensao))
		into	dt_retorno_w
		from	pls_segurado_suspensao 	a,
			pls_segurado		b
		where	a.nr_seq_pagador 	= b.nr_seq_pagador
		and	b.nr_sequencia		= nr_seq_segurado_p
		and	a.dt_fim_suspensao	is null;		
	end if;
end if;

return	dt_retorno_w;

end pls_obter_dt_suspensao_seg;
/
