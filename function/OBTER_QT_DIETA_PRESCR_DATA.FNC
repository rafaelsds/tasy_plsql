create or replace
function Obter_qt_dieta_prescr_data(nr_prescricao_p		in Number)
 		    	return Number is

qt_retorno_w			Number(15);
nr_atendimento_w		Number(15);
dt_inicio_prescr_w		date;
dt_validade_prescr_w	date;
ie_forma_consiste_w		varchar2(5 char);
nm_usuario_w			varchar2(50 char);

begin

select	max(dt_inicio_prescr),
	max(dt_validade_prescr),
	max(nr_atendimento),
	max(nm_usuario)
into	dt_inicio_prescr_w,
	dt_validade_prescr_w,
	nr_atendimento_w,
	nm_usuario_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

obter_param_usuario(924,1012, obter_perfil_ativo, nm_usuario_w, wheb_usuario_pck.get_cd_estabelecimento, ie_forma_consiste_w); 

select	sum(dieta)
into	qt_retorno_w
from(
	select	nvl(count(a.nr_sequencia),0) dieta
	from	Prescr_dieta a,
			prescr_medica b
	where	a.nr_prescricao = nr_prescricao_p
	and a.nr_prescricao		= b.nr_prescricao
	and	b.nr_atendimento	= nr_atendimento_w
	and	obter_se_prescr_vig_adep(b.dt_inicio_prescr,b.dt_validade_prescr,dt_inicio_prescr_w,dt_validade_prescr_w) = 'S'
	and	obter_datas_prescricao(b.nr_prescricao,'L') is null
	and	nvl(ie_forma_consiste_w,'N') = 'N'
	and	a.dt_suspensao is null	
	union all
	select	nvl(count(a.nr_sequencia),0) dieta
	from	Prescr_dieta a,
			prescr_medica b
	where	a.nr_prescricao	= b.nr_prescricao
	and	b.nr_atendimento	= nr_atendimento_w
	and	obter_se_prescr_vig_adep(b.dt_inicio_prescr,b.dt_validade_prescr,dt_inicio_prescr_w,dt_validade_prescr_w) = 'S'
	and	obter_datas_prescricao(b.nr_prescricao,'L') is not null
	and	nvl(ie_forma_consiste_w,'N') = 'N'
	and	a.dt_suspensao is null
	and exists (	select	1
					from	prescr_dieta c
					where	c.nr_prescricao = nr_prescricao_p
					and		c.dt_suspensao is null
					and		rownum = 1)
	union all
	select	nvl(count(a.nr_sequencia),0) dieta
	from	Prescr_dieta a,
			prescr_medica b
	where	a.nr_prescricao = nr_prescricao_p
	and	a.nr_prescricao		= b.nr_prescricao
	and	b.nr_atendimento	= nr_atendimento_w
	and	obter_se_prescr_vig_adep(b.dt_inicio_prescr,b.dt_validade_prescr,dt_inicio_prescr_w,dt_validade_prescr_w) = 'S'
	and	obter_datas_prescricao(b.nr_prescricao,'L') is not null
	and	nvl(ie_forma_consiste_w,'N') = 'S'
	and	a.dt_suspensao is null) a;

return	qt_retorno_w;

end Obter_qt_dieta_prescr_data;
/
