create or replace FUNCTION HFP_OBTER_COMPOS_CORP_HTML5
(
    nr_sequencia_p  NUMBER,
    ie_informacao_p VARCHAR2)
  RETURN NUMBER
IS
  qt_idade_w           NUMBER(10,5);
  aux_w                pls_integer; --para arredondar o valor antes de retornar como texto
  qt_peso_w            NUMBER(10,5);
  dc_triceps_w         NUMBER(10,5);
  dc_subescapular_w    NUMBER(10,5);
  dc_suprailiaca_w     NUMBER(10,5);
  dc_abdominal_w       NUMBER(10,5);
  qt_altura_cm_w       NUMBER(10,5);
  perc_gordura_w       NUMBER(10,5);
  perc_ideal_gordura_w NUMBER(10,5);
  ds_retorno_w         pls_integer;
  qt_imc_w             pls_integer;
BEGIN
  ds_retorno_w := 0;
  --Busca dados para permitir c lculos posteriores
  SELECT obter_idade_pf(c.cd_pessoa_fisica, sysdate, 'A'),
    a.qt_peso,
    a.dc_tricipes,
    a.dc_subescapular,
    a.dc_suprailiaca,
    a.dc_abdominal,
    a.qt_altura_cm,
    obter_imc(a.qt_peso, a.qt_altura_cm, c.cd_pessoa_fisica)
  INTO qt_idade_w,
    qt_peso_w,
    dc_triceps_w,
    dc_subescapular_w,
    dc_suprailiaca_w,
    dc_abdominal_w,
    qt_altura_cm_w,
    qt_imc_w
  FROM hfp_composicao_corporal a,
    hfp_cinesio_funcional b,
    hfp_paciente c
  WHERE a.nr_seq_aval    = b.nr_sequencia
  AND b.nr_seq_paciente  = c.nr_sequencia
  AND a.nr_sequencia     = nr_sequencia_p;
  perc_gordura_w        := ((dc_triceps_w + dc_subescapular_w + dc_suprailiaca_w + dc_abdominal_w) * 0.153 + 5.783);
  perc_ideal_gordura_w  := 12             + (qt_idade_w - 30) * 0.2;
  IF (ie_informacao_p    = 'PI') THEN -- PESO IDEAL
    aux_w               := (qt_peso_w * (100 - perc_gordura_w) * 0.01)/(1-(perc_ideal_gordura_w) * 0.01);
    ds_retorno_w        := aux_w;
  elsif (ie_informacao_p = 'PSG') THEN -- PESO GORDURA
    aux_w               := (qt_peso_w * perc_gordura_w * 0.01);
    ds_retorno_w        := aux_w;
  elsif (ie_informacao_p = 'IMC') THEN -- IMC
    aux_w               := qt_imc_w;
    ds_retorno_w        := aux_w;
  elsif (ie_informacao_p = 'PS') THEN -- PESO
    ds_retorno_w        := qt_peso_w;
  elsif (ie_informacao_p = 'PM') THEN -- PESO MAGRO
    aux_w               := (qt_peso_w * (100 - perc_gordura_w) * 0.01);
    ds_retorno_w        := aux_w;
  elsif (ie_informacao_p = 'PG') THEN -- PERCENTUAL GORDURA
    aux_w               := perc_gordura_w;
    ds_retorno_w        := aux_w;
  elsif (ie_informacao_p = 'PLG') THEN -- PERCENTUAL LIVRE GORDURA
    aux_w               := (100 - perc_gordura_w);
    ds_retorno_w        := aux_w;
  elsif (ie_informacao_p = 'PIG') THEN -- PERCENTUAL IDEAL GORDURA
    aux_w               := perc_ideal_gordura_w;
    ds_retorno_w        := aux_w;
  END IF;
  RETURN ds_retorno_w;
END hfp_obter_compos_corp_html5;
/
