create or replace
function rp_qtd_agenda_carona_amiga_dia(dt_agenda_p		date,
					nr_seq_regiao_p		number,
					ie_opcao_p		varchar2)
 		    	return number is

cd_pessoa_fisica_w	varchar2(10);			
qt_agendamento_w	number(10);
qt_retorno_w		number(10);
			
Cursor C01 is
	select	distinct cd_pessoa_fisica
	from	rp_paciente_reabilitacao a
	where	ie_carona_amiga = 'S'
	and	nr_seq_regiao_p = (select	max(CD_MUNICIPIO_IBGE)
				from		compl_pessoa_fisica b
				where		a.cd_pessoa_fisica = b.cd_pessoa_fisica
				and		b.ie_tipo_complemento = 1);
	
begin
if	(dt_agenda_p is not null) and
	(nr_seq_regiao_p is not null) then
	if	(ie_opcao_p = 'T') then
		qt_retorno_w := 0;
		
		open C01;
		loop
		fetch C01 into	
			cd_pessoa_fisica_w;
		exit when C01%notfound;
			begin
			qt_agendamento_w := 0;
			
			select	count(*)
			into	qt_agendamento_w
			from	agenda_paciente		
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	trunc(hr_inicio)	= trunc(dt_agenda_p)
			and	ie_status_agenda not in ('C','B');
			
			if	(qt_agendamento_w > 0) then
				qt_retorno_w := qt_retorno_w + 1;
			else
				select	count(*)
				into	qt_agendamento_w
				from	agenda_consulta			
				where	cd_pessoa_fisica 	= cd_pessoa_fisica_w
				and	trunc(dt_agenda)	= trunc(dt_agenda_p)
				and	ie_status_agenda not in ('C','B');
				
				if	(qt_agendamento_w > 0) then
					qt_retorno_w := qt_retorno_w + 1;
				else
					select	count(*)
					into	qt_agendamento_w
					from	agenda_integrada
					where	cd_pessoa_fisica = cd_pessoa_fisica_w
					and	trunc(dt_inicio_agendamento) = trunc(dt_agenda_p);
					
					if	(qt_agendamento_w > 0) then
						qt_retorno_w := qt_retorno_w + 1;
					end if;				
				end if;
				
			end if;
			
			end;
		end loop;
		close C01;
	elsif	(ie_opcao_p = 'C') then
		qt_retorno_w := 0;
		open C01;
		loop
		fetch C01 into	
			cd_pessoa_fisica_w;
		exit when C01%notfound;
			begin
			select	count(*)
			into	qt_agendamento_w
			from	agenda_consulta			
			where	cd_pessoa_fisica 	= cd_pessoa_fisica_w
			and	trunc(dt_agenda)	= trunc(dt_agenda_p)
			and	ie_status_agenda not in ('C','B');
			
			if	(qt_agendamento_w > 0) then
				qt_retorno_w := qt_retorno_w + 1;
			end if;	
			end;
		end loop;
		close C01;
	elsif	(ie_opcao_p = 'P') then
		
		qt_retorno_w := 0;
		open C01;
		loop
		fetch C01 into	
			cd_pessoa_fisica_w;
		exit when C01%notfound;
			begin
			select	count(*)
			into	qt_agendamento_w
			from	agenda_paciente		
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	trunc(hr_inicio)	= trunc(dt_agenda_p)
			and	ie_status_agenda not in ('C','B');
			
			if	(qt_agendamento_w > 0) then
				qt_retorno_w := qt_retorno_w + 1;
			end if;	
			end;
		end loop;
		close C01;
		
	elsif	(ie_opcao_p = 'I') then
		qt_retorno_w := 0;
		open C01;
		loop
		fetch C01 into	
			cd_pessoa_fisica_w;
		exit when C01%notfound;
			begin
			select	count(*)
			into	qt_agendamento_w
			from	agenda_integrada
			where	cd_pessoa_fisica = cd_pessoa_fisica_w
			and	trunc(dt_inicio_agendamento) = trunc(dt_agenda_p);
			
			if	(qt_agendamento_w > 0) then
				qt_retorno_w := qt_retorno_w + 1;
			end if;				
			end;
		end loop;
		close C01;
	end if;
end if;

return	qt_retorno_w;

end rp_qtd_agenda_carona_amiga_dia;
/
