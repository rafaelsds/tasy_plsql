create or replace
function pls_obter_qt_mens_faixa_etaria
			(	qt_idade_inicial_p	number,
				qt_idade_final_p	number,
				ie_tipo_data_p		varchar2,
				dt_inicial_p		date,
				dt_final_p		date,
				ie_trazer_dependente_p	number)
				return varchar2 is

vl_total_mens_segurado_w	number(15,2) := 0;

begin

select	nvl(sum(e.vl_item),0)
into	vl_total_mens_segurado_w
from	pls_mensalidade_seg_item	e,
	pls_segurado			d,
	pls_mensalidade_segurado	c,
	pls_mensalidade			b,
	pls_lote_mensalidade		a
where	e.nr_seq_mensalidade_seg	= c.nr_sequencia
and	c.nr_seq_mensalidade		= b.nr_sequencia
and	b.nr_seq_lote			= a.nr_sequencia
and	c.nr_seq_segurado		= d.nr_sequencia
and	a.ie_status			= '2'
and	b.ie_cancelamento is null
and	nvl(c.qt_idade,0) between qt_idade_inicial_p and qt_idade_final_p
and	((ie_tipo_data_p = 'G' and a.dt_geracao between dt_inicial_p and fim_dia(dt_final_p)) or
	(ie_tipo_data_p = 'M' and b.dt_referencia between dt_inicial_p and fim_dia(dt_final_p)) or
	(ie_tipo_data_p = 'N'))
and	((ie_trazer_dependente_p = 1) or
	(ie_trazer_dependente_p = 0 and d.nr_seq_titular is null));

return	campo_mascara_virgula(vl_total_mens_segurado_w);

end pls_obter_qt_mens_faixa_etaria;
/