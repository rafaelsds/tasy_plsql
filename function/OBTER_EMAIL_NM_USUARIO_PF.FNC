create or replace function obter_email_nm_usuario_pf (
		nm_usuario_email_p varchar2)
	return varchar2
is
	ds_retorno_w varchar2 (4000 char) ;
begin

	select	max(u.ds_email)
	into	ds_retorno_w
	from	usuario u
	where	u.nm_usuario = nm_usuario_email_p;
	
	return ds_retorno_w;
end obter_email_nm_usuario_pf;
/