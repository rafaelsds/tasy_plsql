create or replace function    get_req_column_value(cd_perfil_p number, 
                nm_usuario_proxy_p varchar2, 
                dt_atualizacao_nrec_p date)
    return varchar2 is

cd_razao_w          med_razao_delegacao.nr_sequencia%type;
ds_razao_w          med_razao_delegacao.ds_razao%type;
nm_usuario_w        perfil_delegacao.nm_usuario%type;
cd_pessoa_fisia_w   pessoa_fisica.cd_pessoa_cross%type;
begin

    select  max(cd_razao),
            max(nm_usuario)
    into    cd_razao_w,
            nm_usuario_w
    from    perfil_delegacao
    where   nm_proxy = nm_usuario_proxy_p
    and     cd_perfil = cd_perfil_p
    and     dt_atualizacao_nrec_p between dt_inicio_delegacao and dt_fim_delegacao;

    if (nm_usuario_w is not null) then 
        goto finish;
    end if;

    cd_pessoa_fisia_w := Obter_Pf_Usuario(nm_usuario_proxy_p, 'C');
    select  max(a.cd_razao),
            max(a.nm_usuario)
    into    cd_razao_w,
            nm_usuario_w
    from    perfil_delegacao a,
            user_medical_department b,
            pessoa_fisica c
    where   a.cd_departamento = b.cd_departamento
    and     b.nm_usuario = nm_usuario_proxy_p
    and     a.cd_perfil = cd_perfil_p
    and     c.cd_pessoa_fisica = cd_pessoa_fisia_w
    and     a.cd_cargo = c.cd_cargo
    and     dt_atualizacao_nrec_p between a.dt_inicio_delegacao and a.dt_fim_delegacao;

    if (nm_usuario_w is not null) then 
        goto finish;
    end if;


    select  max(a.cd_razao),
            max(a.nm_usuario)
    into    cd_razao_w,
            nm_usuario_w
    from    perfil_delegacao a,
            user_medical_department b
    where   a.cd_departamento = b.cd_departamento
    and     a.cd_perfil = cd_perfil_p
    and     dt_atualizacao_nrec_p between a.dt_inicio_delegacao and a.dt_fim_delegacao;

    if (nm_usuario_w is not null) then 
        goto finish;
    end if;

    select  max(a.cd_razao),
            max(a.nm_usuario)
    into    cd_razao_w,
            nm_usuario_w
    from    perfil_delegacao a,
            pessoa_fisica b
    where   b.cd_pessoa_fisica = cd_pessoa_fisia_w
    and     a.cd_perfil = cd_perfil_p
    and     dt_atualizacao_nrec_p between a.dt_inicio_delegacao and a.dt_fim_delegacao
    and     a.cd_cargo = b.cd_cargo;

    <<finish>>
    if (nm_usuario_w is null) then
        return '***';
    else
        if (cd_razao_w is not null) then
            select  max(ds_razao)
            into    ds_razao_w
            from    med_razao_delegacao
            where   nr_sequencia = cd_razao_w;
        end if;
        return obter_desc_usuario(nm_usuario_w) || '***' || ds_razao_w;
    end if;

end get_req_column_value;
/
