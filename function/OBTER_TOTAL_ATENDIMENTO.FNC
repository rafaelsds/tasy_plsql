create or replace
function obter_total_atendimento(	nr_atendimento_p		number,
					ie_opcao_p			integer)
 		    	return number is

Cursor C01 is
	select	nr_interno_conta
	from	conta_paciente
	where	nr_atendimento		= nr_atendimento_p;
	
nr_interno_conta_w		number(15);
vl_retorno_w			number(15,4) :=0;
begin


open C01;
loop
fetch C01 into	
	nr_interno_conta_w;
exit when C01%notfound;
	begin
	vl_retorno_w	:= vl_retorno_w + obter_valor_conta(nr_interno_conta_w,ie_opcao_p);
	end;
end loop;
close C01;

return	vl_retorno_w;

end obter_total_atendimento;
/