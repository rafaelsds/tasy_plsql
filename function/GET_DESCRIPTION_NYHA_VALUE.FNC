create or replace 
function get_description_nyha_value(ie_classe	number)
		return varchar2 is

ds_retorno_w varchar2(4000);

begin

if((ie_classe is not null)) then
    if(ie_classe = 1) then
        ds_retorno_w := obter_desc_expressao(874456);
    
    elsif(ie_classe = 2) then
        ds_retorno_w := obter_desc_expressao(874458);
    
    elsif(ie_classe = 3) then
        ds_retorno_w := obter_desc_expressao(874460);
        
    elsif(ie_classe = 4) then
        ds_retorno_w := obter_desc_expressao(874462);

    end	if;
end	if;

return	ds_retorno_w;

end	get_description_nyha_value;
/