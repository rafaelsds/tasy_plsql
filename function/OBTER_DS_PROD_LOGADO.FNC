create or replace
function obter_ds_prod_logado ( vl_dominio_p		varchar2)
 		    	return varchar2 is
ds_retorno_w	varchar2(20);
begin

select	ds_valor_dominio
into	ds_retorno_w 
from	VALOR_DOMINIO
where	cd_dominio = 7081
and		vl_dominio = vl_dominio_p;

return	ds_retorno_w ;

end obter_ds_prod_logado ;
/