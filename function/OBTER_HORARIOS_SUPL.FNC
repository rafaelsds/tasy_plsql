create or replace
function Obter_Horarios_Supl(nr_sequencia_p	number,
			nr_seq_serv_p           varchar2,
			tipo_serv	        varchar2)
 		    	return Varchar2 is
				
				
ds_retorno_w	varchar2(510);				
ds_virgula_w	Varchar2(02);
ds_grupo_w	Varchar2(255);	
nr_seq_serv_w	number(10);
ie_tipo_prescricao_servico_w	varchar2(1);

/*
Tipo_Serv:
S = suplemento
L =  lactario
*/
cursor	c01 is

SELECT	TO_CHAR(dt_servico,'hh24:mi')
FROM	nut_atend_serv_dia x
WHERE	((ie_tipo_prescricao_servico_w = 3 and tipo_serv = 'S')
or	(ie_tipo_prescricao_servico_w = 8 and tipo_serv = 'L'))
AND	x.nr_sequencia  = nr_sequencia_p
AND 	EXISTS( SELECT  1  
                FROM    nut_servico ns,  
                        nut_servico_horario nsh  
                WHERE   ns.nr_sequencia = nsh.nr_seq_servico  
                AND     ns.nr_sequencia = x.nr_seq_servico  
                AND     ((obter_se_contido(nsh.CD_SETOR_ATENDIMENTO, x.cd_setor_atendimento) = 'S')
                        OR (nsh.CD_SETOR_ATENDIMENTO IS NULL)  AND NOT EXISTS (SELECT 1 FROM nut_servico_horario WHERE CD_SETOR_ATENDIMENTO = x.cd_setor_atendimento AND nr_seq_servico = x.nr_seq_servico))
                AND x.dt_servico BETWEEN pkg_date_utils.get_DateTime(extract(year FROM x.dt_servico),  
                                                                           extract(month FROM x.dt_servico),  
                                                                           extract(day FROM x.dt_servico),  
                                                                           extract(hour FROM cast(nsh.DS_HORARIOS_HTML AS TIMESTAMP)),  
                                                                           extract(minute FROM cast(nsh.DS_HORARIOS_HTML AS TIMESTAMP)),  
                                                                           0)  
                AND pkg_date_utils.get_DateTime(extract(year FROM x.dt_servico),  
                                                                           extract(month FROM x.dt_servico),  
                                                                           extract(day FROM x.dt_servico),  
                                                                           extract(hour FROM cast(nsh.DS_HORARIOS_FIM_HTML AS TIMESTAMP)),  
                                                                           extract(minute FROM cast(nsh.DS_HORARIOS_FIM_HTML AS TIMESTAMP)),   
                                                                           0));


begin
ds_virgula_w := '';

select	max(ie_tipo_prescricao_servico)
into	ie_tipo_prescricao_servico_w
from	nut_servico a,
	nut_atend_serv_dia b
where	b.nr_seq_servico = a.nr_sequencia
and	b.nr_sequencia = nr_sequencia_p;

open	c01;
loop
fetch	c01 into
	ds_grupo_w;
exit	when c01%notfound;
begin
	ds_retorno_w := ds_retorno_w || ds_virgula_w || ds_grupo_w;
	ds_virgula_w		:= ', ';
end;
end loop;
close C01;

return	ds_retorno_w;

end Obter_Horarios_Supl;
/