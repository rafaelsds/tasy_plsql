create or replace function
Obter_Se_Guia_Reapresentada	(	nr_seq_ret_item_p		number) 
					return varchar2 is

ie_retorno_w	varchar2(1) := 'N';

vl_pago_w		number(15,2);
vl_glosado_w	number(15,2);
vl_adicional_w	number(15,2);
vl_amenor_w		number(15,2);
vl_adequado_w	number(15,2);
vl_guia_w		number(15,2);

begin

select	a.vl_pago,
		a.vl_glosado,
		a.vl_adicional,
		a.vl_amenor,
		a.vl_adequado,
		nvl(b.vl_guia,0)
into	vl_pago_w,
	vl_glosado_w,
	vl_adicional_w,
	vl_amenor_w,
	vl_adequado_w,
	vl_guia_w
from conta_paciente_guia b,
	convenio_retorno_item a
where a.nr_interno_conta	= b.nr_interno_conta
  and a.cd_autorizacao		= b.cd_autorizacao
  and a.nr_sequencia		= nr_seq_ret_item_p;

if	(vl_guia_w <> vl_pago_w) then
	if	(vl_pago_w + vl_amenor_w + vl_glosado_w <> vl_guia_w) then
 			ie_retorno_w := 'S';
	end if;
end if;

return	ie_retorno_w;

end Obter_Se_Guia_Reapresentada;
/