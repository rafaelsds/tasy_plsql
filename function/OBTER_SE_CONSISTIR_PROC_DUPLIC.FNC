create or replace
function obter_se_consistir_proc_duplic(nr_seq_proc_interno_p		number)
 		    	return varchar2 is

ie_consistir_duplic_w	varchar2(1);

begin

select 	nvl(max(ie_consistir_duplic),'S')
into	ie_consistir_duplic_w
from	proc_interno
where	nr_sequencia	=	nr_seq_proc_interno_p;


return	ie_consistir_duplic_w;

end obter_se_consistir_proc_duplic;
/