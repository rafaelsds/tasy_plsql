create or replace
function Obter_Totais_Cobranca(nr_seq_cobranca_p	number,
				ie_tipo_dado_p		varchar2)
 		    	return number is
/*
VNB = vl_nao_baixado	
VBP = vl_baixa_parcial
QNB = qt_nao_baixado	
QBP = qt_baixa_parcial	
VL =   vl_liquidado
QNC = qt_nota_credito 
VTL = vl_titulo_log
QTL = qt_titulo_liq  
QNTL = qt_nao_tit_liq

*/			
ds_retorno_w  		number(15,2);			
vl_nao_baixado_w  	number(15,2);
vl_baixa_parcial_w  	number(15,2);
qt_nao_baixado_w	number(15,2);
qt_baixa_parcial_w	number(15,2);
vl_liquidado_w		number(15,2);
qt_nota_credito_w	number(15,2);
vl_titulo_log_w		number(15,2);
qt_titulo_liq_w		number(15,2);
qt_nao_tit_liq_w	number(15,2);

begin


IF	(ie_tipo_dado_p = 'VNB') then

	select	nvl(sum(nvl(vl_nao_baixado,0)),0)
	into 	vl_nao_baixado_w
	from	(select	(nvl(x.vl_saldo_inclusao,0) - nvl(x.vl_liquidacao,0) - nvl(x.vl_desconto,0)) vl_nao_baixado
		from	banco_ocorr_escrit_ret y,
			titulo_receber_cobr x
		where	x.nr_seq_cobranca	= nr_seq_cobranca_p
		and	x.nr_seq_ocorrencia_ret = y.nr_sequencia(+)
		and	nvl(y.ie_rejeitado, 'X') <> 'L'
		and	nvl(y.ie_situacao, 'A') = 'A'
		and	nvl(x.vl_saldo_inclusao,0) > 0);
		
		ds_retorno_w:= vl_nao_baixado_w;

ELSIF	(ie_tipo_dado_p = 'VBP') THEN

	select	nvl(sum(nvl(vl_baixa_parcial,0)),0)
	into vl_baixa_parcial_w 
from	(select	nvl(x.vl_liquidacao,0) vl_baixa_parcial
		from	titulo_receber_cobr x
		where	x.nr_seq_cobranca	= nr_seq_cobranca_p
		and	decode(nvl(x.vl_saldo_inclusao,0),0,0,(nvl(x.vl_saldo_inclusao,0) - nvl(x.vl_liquidacao,0) - nvl(x.vl_desconto,0))) > 0);

		ds_retorno_w:= vl_baixa_parcial_w; 
		
ELSIF	(ie_tipo_dado_p = 'QNB') THEN


	select	nvl(sum(nvl(qt_nao_baixado,0)),0)
	into qt_nao_baixado_w
from	(select	1 qt_nao_baixado
		from	banco_ocorr_escrit_ret y,
			titulo_receber_cobr x
		where	x.nr_seq_cobranca	= nr_seq_cobranca_p
		and	x.nr_seq_ocorrencia_ret = y.nr_sequencia(+)
		and	nvl(y.ie_rejeitado, 'X') <> 'L'
		and	nvl(y.ie_situacao, 'A') = 'A'
		and	nvl(x.vl_saldo_inclusao,0) > 0);

		ds_retorno_w:=	qt_nao_baixado_w;

ELSIF	(ie_tipo_dado_p = 'QBP') THEN

	select	nvl(sum(nvl(qt_baixa_parcial,0)),0)
	into qt_baixa_parcial_w
from	(select	1 qt_baixa_parcial
		from	titulo_receber_cobr x
		where	x.nr_seq_cobranca	= nr_seq_cobranca_p
		and	decode(nvl(x.vl_saldo_inclusao,0),0,0,(nvl(x.vl_saldo_inclusao,0) - nvl(x.vl_liquidacao,0) - nvl(x.vl_desconto,0))) > 0);
		
		ds_retorno_w:= qt_baixa_parcial_w;
		
ELSIF	(ie_tipo_dado_p = 'VL') THEN
		
	select	nvl(sum(nvl(vl_liquidado,0)),0)
	into vl_liquidado_w
from	(select	nvl(x.vl_cobranca,0) vl_liquidado
		from	banco_ocorr_escrit_ret y,
			titulo_receber_cobr x
		where	x.nr_seq_cobranca	= nr_seq_cobranca_p
		and	x.nr_seq_ocorrencia_ret	= y.nr_sequencia
		and	y.ie_rejeitado = 'L');
		
		ds_retorno_w:= vl_liquidado_w;
		
ELSIF	(ie_tipo_dado_p = 'QNC') THEN		
	select	count(1)
	into qt_nota_credito_w
		from	titulo_receber_cobr	b	
		where	b.nr_seq_cobranca	= nr_seq_cobranca_p
		and	substr(obter_dados_titulo_rec_cobr(b.nr_sequencia,'NC'),1,255) is not null;

		ds_retorno_w:= qt_nota_credito_w;

ELSIF	(ie_tipo_dado_p = 'VTL') THEN

	select   nvl(sum(nvl(vl_saldo_titulo,0)),0)
	into vl_titulo_log_w
		from     cobranca_escrit_log
		where    nr_seq_cobranca = nr_seq_cobranca_p
		order by 1;
		
		ds_retorno_w:= vl_titulo_log_w;
		
ELSIF	(ie_tipo_dado_p = 'QTL') THEN	


	select 	count(1)
	into qt_titulo_liq_w
		from	titulo_receber		b,
			titulo_receber_cobr 	a
		where 	a.nr_titulo		= b.nr_titulo
		and	a.nr_seq_cobranca	= nr_seq_cobranca_p
		and	b.ie_situacao		in ('2');

		ds_retorno_w:= qt_titulo_liq_w;
		
ELSIF	(ie_tipo_dado_p = 'QNTL') THEN			
	
	select 	count(1)
	into	qt_nao_tit_liq_w
		from	titulo_receber		b,
			titulo_receber_cobr 	a
		where 	a.nr_titulo		= b.nr_titulo
		and	a.nr_seq_cobranca	= nr_seq_cobranca_p
		and	b.ie_situacao		not in ('2');

	ds_retorno_w:= qt_nao_tit_liq_w;
end if;	
return	ds_retorno_w;

end Obter_Totais_Cobranca;
/