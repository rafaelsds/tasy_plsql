create or replace
function Obter_Regra_Atributo3(	nm_tabela_p		varchar2,
				nr_seq_visao_p		number,
				cd_estabelecimento_p	number,
				cd_perfil_p		number,
				cd_funcao_p		number,
				nm_usuario_p		varchar2)
return	varchar2 is

/* Fernando Piccini - 08/03/2013 */

nm_atributo_p	varchar2(50);
qt_tam_max_p	number(5);
qt_tam_min_p	number(5);
ds_tam_min_p	varchar2(2000) := ''; 
ds_tam_max_p	varchar2(2000) := '';

cursor C01 is
	select	nm_atributo,
		qt_tam_max,				
		qt_tam_min
	from	tabela_atrib_regra
	where	nm_tabela 		= nm_tabela_p	
	and	(qt_tam_max is not null or qt_tam_min is not null)
	and	nvl(cd_estabelecimento,	cd_estabelecimento_p)	= cd_estabelecimento_p
	and	nvl(cd_perfil, 		cd_perfil_p)		= cd_perfil_p
	and	nvl(cd_funcao, 		cd_funcao_p)		= cd_funcao_p
	and	nvl(nm_usuario_param, 	nm_usuario_p) 		= nm_usuario_p
	and	nvl(nr_seq_visao, 	nr_seq_visao_p)		= nr_seq_visao_p
	order by 
		NVL(nm_usuario_param,'AAA'), 
		NVL(cd_perfil,0),
		NVL(cd_estabelecimento,0), 
		NVL(cd_funcao,0), 
		NVL(nr_seq_visao,0), 
		NVL(nr_seq_regra_funcao,0);

begin

open C01;
loop
fetch C01 into
	nm_atributo_p,
	qt_tam_max_p,
	qt_tam_min_p;
exit when C01%notfound;				
	if	(qt_tam_min_p is not null) then
		ds_tam_min_p	:= ds_tam_min_p || nm_atributo_p || '=' || qt_tam_min_p || ';';
	end	if;	
	if	(qt_tam_max_p is not null) then
		ds_tam_max_p	:= ds_tam_max_p || nm_atributo_p || '=' || qt_tam_max_p || ';';
	end	if;				
end	loop;
close C01;

return	(ds_tam_min_p || '<FIM1>' || ds_tam_max_p || '<FIM2>');

end	Obter_Regra_Atributo3;
/
