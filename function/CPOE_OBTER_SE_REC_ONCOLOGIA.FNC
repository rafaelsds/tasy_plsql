create or replace
function cpoe_obter_se_rec_oncologia(	nr_atendimento_p		cpoe_material.nr_atendimento%type,
									nr_sequencia_cpoe_p		cpoe_material.nr_sequencia%type )
									return char IS 
				
result_w	char(1);

begin

select NVL(max('S'),'N') 
into	result_w
from 	prescr_medica a, prescr_recomendacao b
where  	a.nr_prescricao = b.nr_prescricao
and  	b.nr_seq_rec_cpoe = nr_sequencia_cpoe_p
and  	a.nr_atendimento = nr_atendimento_p
and 	a.nr_seq_atend is not null
and 	rownum = 1;

return result_w;

end cpoe_obter_se_rec_oncologia;
/
