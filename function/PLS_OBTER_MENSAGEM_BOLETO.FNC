create or replace
function pls_obter_mensagem_boleto(	nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
					ie_tipo_mensagem_p	varchar2,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type)
 		    	return varchar2 is

ds_mensagem_w			clob;
dt_referencia_w			date;
dt_nascimento_w			date;
ie_aniversariante_w		varchar2(1)	:= 'N';
ds_observacao_w			varchar2(2000);
nr_seq_pagador_w		number(10);
nr_seq_plano_w			number(10);
ie_regulamentacao_w		varchar2(1);
pos_fim_macro_w			number(10);
ds_macro_w			varchar2(30);
nm_atributo_w			varchar2(50);
pos_macro_w			number(10);
ds_resultado_macro_w		varchar2(4000);
ds_texto_alterado_w		varchar2(4000);
i				number(10) := 0;
nr_seq_pagador_fin_w		number(10);
nr_seq_conta_banco_w		number(10);
nr_seq_contrato_w		number(10);
ds_segurados_w			varchar2(2000);
ds_valores_pre_w		varchar2(2000);
qt_registro_w			number(10) := 0;
ie_tipo_estipulante_w		varchar2(255);
dt_mesano_referencia_w		date;
nr_seq_classif_itens_w		number(10);
nr_ano_w			pls_pagador_quitacao_anual.nr_ano%type;

ds_retorno_w			varchar2(4000);

Cursor C01 is 
	select	a.ds_mensagem,
		a.qt_meses_portabilidade,
		a.nr_seq_tipo_portabilidade,
		a.ie_mensalidade_em_dia
	from	pls_mensagem_mensalidade a
	where	(dt_mesano_referencia_w between trunc(nvl(a.dt_inicial,dt_mesano_referencia_w),'dd') and fim_dia(nvl(a.dt_final,dt_mesano_referencia_w)))
	and	((a.ie_aniversario		= 'S' and ie_aniversariante_w = 'S') or	(a.ie_aniversario = 'N'))
	and	a.cd_estabelecimento		= cd_estabelecimento_p
	and	((nr_seq_conta_banco_w		= a.nr_seq_conta_banco and nr_seq_conta_banco_w is not null and a.nr_seq_conta_banco is not null) or (a.nr_seq_conta_banco is null))
	and	((nr_seq_classif_itens_w	= a.nr_seq_classif_itens and nr_seq_classif_itens_w is not null and a.nr_seq_classif_itens is not null) or (nr_seq_classif_itens is null))
	and	nvl(a.ie_regulamentacao,ie_regulamentacao_w)	= ie_regulamentacao_w
	and	((nvl(a.ie_tipo_estipulante,'A')	= NVL(ie_tipo_estipulante_w,nvl(a.ie_tipo_estipulante,'A'))) or
		nvl(a.ie_tipo_estipulante,'A')	= 'A')
	and 	(exists (select x.nr_ano 
			from 	pls_pagador_quitacao_anual x
			where 	x.nr_ano		= to_number(to_char(a.ie_mensalidade_em_dia, 'yyyy'))
			and 	x.nr_seq_pagador	= nr_seq_pagador_w
			and	x.nr_seq_mensalidade is null) or (a.ie_mensalidade_em_dia is null));

Cursor C02 is
	select	c.dt_nascimento,
		a.nr_seq_plano
	from	pessoa_fisica	c,
		pls_segurado	b,
		pls_mensalidade_segurado a
	where	b.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and	a.nr_seq_segurado	= b.nr_sequencia
	and	a.nr_seq_mensalidade	= nr_seq_mensalidade_p
	and	ie_aniversariante_w	= 'N';

begin


ie_aniversariante_w	:= 'N';

select	trunc(a.dt_referencia,'month'),
	a.nr_seq_pagador,
	a.nr_seq_pagador_fin,
	b.nr_seq_contrato,
	c.dt_mesano_referencia,
	b.nr_seq_classif_itens
into	dt_referencia_w,
	nr_seq_pagador_w,
	nr_seq_pagador_fin_w,
	nr_seq_contrato_w,
	dt_mesano_referencia_w,
	nr_seq_classif_itens_w
from	pls_mensalidade a,
	pls_contrato_pagador b,
	pls_lote_mensalidade c
where	a.nr_seq_pagador	= b.nr_sequencia
and	a.nr_seq_lote		= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_mensalidade_p;

if	(nr_seq_pagador_fin_w is not null) then
	select	nr_seq_conta_banco
	into	nr_seq_conta_banco_w
	from	pls_contrato_pagador_fin
	where	nr_sequencia	= nr_seq_pagador_fin_w;
end if;

if	(nr_seq_contrato_w is not null) then
	select	decode(cd_cgc_estipulante,null,'PF','PJ')
	into	ie_tipo_estipulante_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
end if;

for r_c02_w in C02 loop
	begin
	if	(nvl(r_c02_w.dt_nascimento,sysdate) <> sysdate) then
		if	to_char(r_c02_w.dt_nascimento,'mm') = to_char(dt_referencia_w,'mm') then
			ie_aniversariante_w	:= 'S';
		elsif	(ie_aniversariante_w	<> 'S') then
			ie_aniversariante_w	:= 'N';
		end if;
	end if;
	
	select	max(ie_regulamentacao)
	into	ie_regulamentacao_w
	from	pls_plano
	where	nr_sequencia	= r_c02_w.nr_seq_plano;
	end;
end loop;

/* Verifica se ha regra de portabilidade */
select	count(1)
into	qt_registro_w
from	pls_portab_regra_direito
where	cd_estabelecimento = cd_estabelecimento_p;

for r_c01_w in C01 loop
	begin	
	ds_segurados_w 		:= '';
	ds_valores_pre_w	:= '';
	ds_mensagem_w		:= r_c01_w.ds_mensagem || '  ';
	ds_texto_alterado_w	:= ds_mensagem_w;
	
	if	(r_c01_w.qt_meses_portabilidade is not null) then
		if	(qt_registro_w > 0) then
			/* Verifica se na mensagem quais segurados estarao com portabilidade */
			pls_obter_se_seg_portabilidade(nr_seq_mensalidade_p,
							dt_referencia_w,
							cd_estabelecimento_p,
							r_c01_w.qt_meses_portabilidade,
							r_c01_w.nr_seq_tipo_portabilidade,
							ds_segurados_w,
							ds_valores_pre_w);
			
			/* Se n�o retornar benefici�rios, n�o emite mensagem nenhuma */
			if	(ds_segurados_w is null) then
				ds_mensagem_w	:= null;
			end if;
		else
			ds_mensagem_w	:= null;
		end if;	
	end if;
	
	--aaschlote 08/02/2011 OS - 284461
	if	(ds_mensagem_w is not null) then
		i := 0;
		WHILE	(ds_mensagem_w is not null) and
			(i < 5) LOOP
			begin
			i := i + 1;
			
			select	replace(replace(ds_mensagem_w,'',' '),chr(13) || chr(10),' ')
			into	ds_mensagem_w
			from	dual;
			
			select	instr(ds_mensagem_w,'@')
			into	pos_macro_w
			from	dual;
			
			if	(pos_macro_w > 0) then
				select	substr(ds_mensagem_w,pos_macro_w,length(ds_mensagem_w))
				into	ds_mensagem_w
				from	dual;
				
				select	instr(ds_mensagem_w,' ')
				into	pos_fim_macro_w
				from	dual;
				
				select	Elimina_Caracteres_Especiais(substr(ds_mensagem_w,1,pos_fim_macro_w -1))
				into	ds_macro_w
				from	dual;
				
				if (nvl(ds_macro_w,null) is not null) then
					begin
					select  nm_atributo
					into    nm_atributo_w
					from    pls_macro_mensalidade
					where   upper(ds_macro) = upper(ds_macro_w);
					exception
					when others then
						nm_atributo_w := null;
					end;
					
					if	(nvl(nm_atributo_w,null) is not null) and
						(upper(ds_macro_w) <> upper('@BENEF_PORTAB')) and
						(upper(ds_macro_w) <> upper('@BENEF_VAL_PORTAB')) then
						select	pls_obter_macro_mensalidade(nr_seq_mensalidade_p,nm_atributo_w,ds_macro_w)
						into	ds_resultado_macro_w
						from	dual;
						
						select	replace(ds_texto_alterado_w, ds_macro_w, ds_resultado_macro_w)
						into	ds_texto_alterado_w
						from	dual;
						
					--Tratamento para a portabilidade
					elsif	(upper(nm_atributo_w) = upper('NR_SEQ_MENSALIDADE')) and
						(upper(ds_macro_w) = upper('@BENEF_PORTAB')) then
						ds_resultado_macro_w := ds_segurados_w;
						
						select	replace(ds_texto_alterado_w, ds_macro_w, ds_resultado_macro_w)
						into	ds_texto_alterado_w
						from	dual;
						
						if	(ds_resultado_macro_w is null) then
							ds_texto_alterado_w := null;
						end if;
					elsif	(upper(nm_atributo_w) = upper('NR_SEQ_MENSALIDADE')) and
						(upper(ds_macro_w) = upper('@BENEF_VAL_PORTAB')) then
						ds_resultado_macro_w := ds_valores_pre_w;
						
						select	replace(ds_texto_alterado_w, ds_macro_w, ds_resultado_macro_w)
						into	ds_texto_alterado_w
						from	dual;
						
						if	(ds_resultado_macro_w is null) then
							ds_texto_alterado_w := null;
						end if;
					end if;
				end if;
				
				select	substr(ds_mensagem_w,pos_fim_macro_w, length(ds_mensagem_w))
				into	ds_mensagem_w
				from	dual;
			else
				ds_mensagem_w := '';
			end if;
			
			end;
		end loop;
		
		if	(r_c01_w.ie_mensalidade_em_dia is not null) then
			update	pls_pagador_quitacao_anual 
			set	nr_seq_mensalidade	= nr_seq_mensalidade_p
			where	nr_seq_pagador		= nr_seq_pagador_w 
			and	nr_ano			= to_number(to_char(r_c01_w.ie_mensalidade_em_dia,'yyyy'));
		end if;
		
		if	(ie_tipo_mensagem_p = 'P') and
			(ds_segurados_w is not null) then
			ds_retorno_w := ds_texto_alterado_w;
			exit;
		elsif	(ie_tipo_mensagem_p = 'Q') and
			(r_c01_w.ie_mensalidade_em_dia is not null) then
			ds_retorno_w := ds_texto_alterado_w;
			exit;
		elsif	(ie_tipo_mensagem_p = 'A') and
			(ds_segurados_w is null) and
			(r_c01_w.ie_mensalidade_em_dia is null) then
			
			ds_retorno_w := ds_texto_alterado_w;
			exit;
		end if;
	end if;
	end;
end loop;


return	ds_retorno_w;

end pls_obter_mensagem_boleto;
/