create or replace
function pls_obter_tipo_pagamento(	nr_seq_prestador_p	in pls_prestador.nr_sequencia%type,
					dt_competencia_p  	in pls_rel_pag_prestador.dt_competencia%type) return varchar2 is

-- ds_retorno_w
-- 1 = OPS - Pagamentos de Producao Medica
-- 2 = OPS - Pagamentos de Prestadores

ds_retorno_w		varchar2(1);

begin

select	decode(count(1), 0, ds_retorno_w, '1')
into	ds_retorno_w
from	pls_pagamento_prestador	b,
	pls_lote_pagamento	a
where	a.nr_sequencia		= b.nr_seq_lote
and	b.nr_seq_prestador	= nr_seq_prestador_p
and	a.dt_mes_competencia	= trunc(dt_competencia_p, 'mm')
and	a.ie_status		= 'D'
and	b.ie_cancelamento is null;

if	(ds_retorno_w is null) then
	select	decode(count(1), 0, ds_retorno_w, '2')
	into	ds_retorno_w
	from	pls_pp_prestador	b,
		pls_pp_lote		a
	where	a.nr_sequencia		= b.nr_seq_lote
	and	b.nr_seq_prestador	= nr_seq_prestador_p
	and	a.dt_mes_competencia	= trunc(dt_competencia_p, 'mm')
	and	a.ie_status		= 'D'
	and	b.ie_cancelado		= 'N';
end if;

return	nvl(ds_retorno_w, '1');

end pls_obter_tipo_pagamento;
/