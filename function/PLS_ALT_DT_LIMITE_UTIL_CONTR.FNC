create or replace
procedure pls_alt_dt_limite_util_contr(	nr_seq_contrato_p	number,
					dt_data_alteracao_p	date,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

dt_limite_util_ant_w		date;

begin
select	dt_limite_utilizacao
into	dt_limite_util_ant_w
from	pls_contrato
where	nr_sequencia		= nr_seq_contrato_p;

update	pls_contrato
set	dt_limite_utilizacao	= dt_data_alteracao_p,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_contrato_p;

insert	into	pls_contrato_historico
	(	nr_sequencia, cd_estabelecimento, nr_seq_contrato, dt_historico, ie_tipo_historico,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ds_historico)
values	( 	pls_contrato_historico_seq.nextval, cd_estabelecimento_p, nr_seq_contrato_p, sysdate, '15',
		sysdate, nm_usuario_p, sysdate, nm_usuario_p, 'De: ' || dt_limite_util_ant_w || '. Para: ' || dt_data_alteracao_p);

commit;

end pls_alt_dt_limite_util_contr;
/