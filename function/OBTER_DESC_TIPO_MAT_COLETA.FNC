CREATE OR REPLACE
FUNCTION Obter_Desc_Tipo_Mat_Coleta(
		nr_seq_tipo_mat_p	Number)
	RETURN VARCHAR IS

ds_retorno_w		Varchar2(255);

BEGIN

if	( nr_seq_tipo_mat_p is not null ) then
	select 	substr(a.ds_tipo_material,1,255)
	into	ds_retorno_w
	from 	biob_tipo_mat_coleta a
	where 	a.nr_sequencia = nr_seq_tipo_mat_p;
end if;

RETURN ds_retorno_w;

END Obter_Desc_Tipo_Mat_Coleta;
/