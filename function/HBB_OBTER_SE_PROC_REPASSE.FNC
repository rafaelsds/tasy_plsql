create or replace
function HBB_OBTER_SE_PROC_REPASSE
		(nr_seq_proc_p		number,
		 qt_prioridade_p	number) return number is

/*

Function criada para a OS 191429

Verifica o porte do procedimento conforme as defini��es do cliente

retorno:
	0 - Gera repasse
	1 - N�o gera

*/

ie_retorno_w		number(15,0);
cd_procedimento_w 	number(15,0);
ie_origem_proced_w	number(15,0);
ie_prioridade_w		number(15,0);
vl_procedimento_w	number(15,4);
nr_interno_conta_w	number(15,0);
qt_prioridade_w		number(15,0);
qt_prioridade_result_w	number(15,0);
cd_procedimento_orig_w	number(15,0);
ie_origem_proced_orig_w	number(15,0);
ie_prioridade_orig_w	number(15,0);
vl_procedimento_orig_w	number(15,4);


cursor	c01 is
select	distinct a.cd_procedimento,
	a.ie_origem_proced,
	decode(b.ie_complexidade, 'AC', 3, 'MC', 2, 'AB', 1, 1) ie_prioridade,
	a.vl_procedimento
from	sus_procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta	= nr_interno_conta_w
and	a.cd_procedimento	= b.cd_procedimento(+)
and	a.ie_origem_proced	= b.ie_origem_proced(+)
and	a.cd_motivo_exc_conta	is null
order 	by ie_prioridade desc,
	a.vl_procedimento desc;

begin

select	max(a.nr_interno_conta),
	max(a.cd_procedimento),
	max(a.ie_origem_proced),
	max(decode(b.ie_complexidade, 'AC', 3, 'MC', 2, 'AB', 1, 1)),
	max(a.vl_procedimento)
into	nr_interno_conta_w,
	cd_procedimento_orig_w,
	ie_origem_proced_orig_w,
	ie_prioridade_orig_w,
	vl_procedimento_orig_w
from	sus_procedimento b,
	procedimento_paciente a
where	a.nr_sequencia		= nr_seq_proc_p
and	a.cd_procedimento	= b.cd_procedimento(+)
and	a.ie_origem_proced	= b.ie_origem_proced(+);

qt_prioridade_w		:= 0;
qt_prioridade_result_w	:= 3;

open c01;
loop
fetch c01 into
	cd_procedimento_w,
	ie_origem_proced_w,
	ie_prioridade_w,
	vl_procedimento_w;
exit when c01%notfound;
	qt_prioridade_w		:= qt_prioridade_w + 1;
	if	(qt_prioridade_w > 3) then
		qt_prioridade_w	:= 3;
	end if;
	if	(cd_procedimento_w 	= cd_procedimento_orig_w) and
		(ie_origem_proced_w	= ie_origem_proced_orig_w) and
		(ie_prioridade_w	= ie_prioridade_orig_w) and
		(vl_procedimento_w	= vl_procedimento_orig_w) then
		qt_prioridade_result_w	:= qt_prioridade_w;
		exit;
	end if;
end loop;
close c01;

if	(qt_prioridade_result_w = qt_prioridade_p) then
	return	0;
else
	return	1;
end if;

end HBB_OBTER_SE_PROC_REPASSE;
/
