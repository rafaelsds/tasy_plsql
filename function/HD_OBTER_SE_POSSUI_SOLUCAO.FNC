create or replace
function hd_obter_se_possui_solucao(nr_seq_dialise_p		number)
 		    	return varchar2 is
ie_retorno_w	varchar2(1);
begin
ie_retorno_w := 'S';
if	(nr_seq_dialise_p is not null) then
	select	decode(count(*),0,'N','S') 
	into	ie_retorno_w 
	from	hd_dialise_concentrado a,	
		material b
	where	a.cd_material = b.cd_material
	and	a.nr_seq_dialise = nr_seq_dialise_p
	and	b.ie_tipo_solucao = 'A';
	
	if	(ie_retorno_w = 'S') then
		select	decode(count(*),0,'N','S') 
		into	ie_retorno_w 
		from	hd_dialise_concentrado a,	
			material b
		where	a.cd_material = b.cd_material
		and	a.nr_seq_dialise = nr_seq_dialise_p
		and	b.ie_tipo_solucao = 'B';
	end if;
	
end if;

return	ie_retorno_w;

end hd_obter_se_possui_solucao;
/