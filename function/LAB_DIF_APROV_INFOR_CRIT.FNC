create or replace
function lab_dif_aprov_infor_crit (nr_prescricao_p 	varchar2,
				   dt_aprovacao_p 	date)
				   return varchar2 is
			
ds_retorno_w		varchar2(255);	
dt_contato_medico_w	prescr_contato_result_crit.dt_contato%type;		

begin

select 	Max(dt_contato)
into	dt_contato_medico_w
from	prescr_contato_result_crit
where	nr_prescricao = nr_prescricao_p ;

select  dt_contato_medico_w - dt_aprovacao_p
into	ds_retorno_w
from	dual;

if (ds_retorno_w < 0) then
	ds_retorno_w := 0;
end if;

return	ds_retorno_w;

end lab_dif_aprov_infor_crit;
/