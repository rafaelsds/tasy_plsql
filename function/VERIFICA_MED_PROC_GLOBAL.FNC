create or replace
function verifica_med_proc_global(	nr_seq_proc_interno_p	number,
					cd_medico_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(1) := 'S';
qt_reg_w	varchar2(10);
			
begin


select	count(*)
into	qt_reg_w
from 	agenda_global
where	nr_seq_proc_interno 	= nr_seq_proc_interno_p
and	cd_medico_agenda	is not null;

if (qt_reg_w > 0) then

	ds_retorno_w := 'N';
	
	
	select	count(*)
	into	qt_reg_w
	from 	agenda_global
	where	nr_seq_proc_interno 	= nr_seq_proc_interno_p
	and	cd_medico_agenda	= cd_medico_p;
	
	if (qt_reg_w > 0) then
		ds_retorno_w	:= 'S';
	end if;
	
end if;


return	ds_retorno_w;

end verifica_med_proc_global;
/