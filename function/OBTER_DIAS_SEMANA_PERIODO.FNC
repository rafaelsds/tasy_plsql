create or replace 
FUNCTION obter_dias_semana_periodo
		(dt_inicial_p		date,
		dt_final_p		date)
		return number deterministic is

dt_aux_w		date;
qt_diarias_w		number(10,0);
ie_feriado_w		varchar2(01);

BEGIN

dt_aux_w	:= pkg_date_utils.start_of(dt_inicial_p, 'DD', 0);
qt_diarias_w	:= 0;

while	(dt_aux_w <= dt_final_p) loop
	begin

	If	(to_char(dt_aux_w,'d') in (2,3,4,5,6)) then

		qt_diarias_w	:= qt_diarias_w + 1;
		
	end if;
		
	dt_aux_w	:= dt_aux_w + 1;
	
	end;
end loop;

RETURN qt_diarias_w;

END obter_dias_semana_periodo;
/
