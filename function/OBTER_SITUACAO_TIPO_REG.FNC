create or replace
function obter_situacao_tipo_reg
			(nr_seq_tipo_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1):= 'I';
ie_situacao_w		varchar2(1);
begin

if	(nr_seq_tipo_p is not null) then
	begin
	
	select 	ie_situacao
	into	ie_situacao_w
	from 	qua_auditoria_tipo
	where  	nr_sequencia = nr_seq_tipo_p;

	if	(ie_situacao_w = 'A') then
		 ds_retorno_w := 'A';
	end if;
	end;
end if;
	
return	ds_retorno_w;

end obter_situacao_tipo_reg;
/
