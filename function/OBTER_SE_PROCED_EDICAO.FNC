create or replace
function obter_se_proced_edicao
		(cd_procedimento_p	number,
		ie_origem_proced_p	number,
		cd_edicao_amb_p		number,
		ie_prior_edicao_ajuste_p	varchar2)
		return varchar2 is

ie_liberado_w			varchar2(01)	:= 'N';
ie_origem_proced_edicao_w	number(10,0);
qt_preco_w			number(10,0);

begin

ie_liberado_w	:= 'N';
qt_preco_w	:= 0;

if	(ie_prior_edicao_ajuste_p = 'N') then

	select	max(ie_origem_proced)
	into	ie_origem_proced_edicao_w
	from	edicao_amb
	where	cd_edicao_amb	= cd_edicao_amb_p;


	if	(cd_edicao_amb_p in (2004,2005,2008)) or (ie_origem_proced_edicao_w = 5) then

		if	(ie_origem_proced_p = 5) then
		
			select	count(*)
			into	qt_preco_w
			from	cbhpm_preco
			where	cd_procedimento		= cd_procedimento_p
			and	ie_origem_proced	= ie_origem_proced_p;
	
		else
	
			select	count(*)
			into	qt_preco_w
			from	preco_amb
			where	cd_edicao_amb		= cd_edicao_amb_p
			and	cd_procedimento		= cd_procedimento_p;
	
		end if;
	
	else

		select	count(*)
		into	qt_preco_w
		from	preco_amb
		where	cd_edicao_amb		= cd_edicao_amb_p
		and	cd_procedimento		= cd_procedimento_p;
	
	end if;
	
end if;

if	(qt_preco_w > 0) then
	ie_liberado_w	:= 'S';
end if;

return	ie_liberado_w;

end obter_se_proced_edicao;
/