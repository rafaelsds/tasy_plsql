create or replace
function lic_obter_marca_material(	nr_seq_licitacao_p			number,
				nr_seq_lic_item_p			number,
				cd_cgc_fornec_p			varchar2)
return varchar2 is


ds_marca_w				varchar2(255);
nr_seq_fornec_w				number(10);

begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_fornec_w
from	reg_lic_fornec
where	nr_seq_licitacao	= nr_seq_licitacao_p
and	cd_cgc_fornec	= cd_cgc_fornec_p;

if	(nr_seq_fornec_w > 0) then
	
	select	ds_marca
	into	ds_marca_w
	from	reg_lic_item_fornec
	where	nr_seq_fornec	= nr_seq_fornec_w
	and	nr_seq_lic_item	= nr_seq_lic_item_p;	
	
end if;

return	ds_marca_w;

end lic_obter_marca_material;
/