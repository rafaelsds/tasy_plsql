create or replace
function hem_obter_status_agenda(	nr_seq_proc_p		Number,
					ie_opcao_p		Varchar2)
 		    	return Varchar2 is

/*
IE_OPCAO_P
C = C�digo
D = Descri��o
*/
			
ds_retorno_w	Varchar2(255);
			
begin
if	(nvl(nr_seq_proc_p, 0) > 0) then

	if	(ie_opcao_p = 'C') then
	
		select	decode(dt_inativacao,null,decode(dt_liberacao,null,decode(dt_digitacao,null,'E','C'),'L'),'I')
		into	ds_retorno_w
		from	hem_proc
		where	nr_sequencia = nr_seq_proc_p;
		
	elsif	(ie_opcao_p = 'D') then
	
		select	substr(obter_desc_expressao(decode(dt_inativacao,null,decode(dt_liberacao,null,decode(dt_digitacao,null,491195,330733),292549),319239)),1,255)
		into	ds_retorno_w
		from	hem_proc
		where	nr_sequencia = nr_seq_proc_p;
	end if;

end if;
return	ds_retorno_w;

end hem_obter_status_agenda;
/