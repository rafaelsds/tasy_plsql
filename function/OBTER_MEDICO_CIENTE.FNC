create or replace
function obter_medico_ciente(nr_atend_alta_p	number)
 		    	return varchar2 is	

ie_retorno_w char(1);
				
begin

	select max(b.ie_medico_ciente)
	into   ie_retorno_w
	from   atendimento_paciente a, 
		   atendimento_alta b, 
		   parametro_medico p
	where  a.nr_atend_alta = b.nr_atendimento
	and    a.nr_atendimento = nr_atend_alta_p
	and    p.cd_estabelecimento = obter_estabelecimento_ativo
	and    ((b.ie_tipo_orientacao <> 'P')
	or     (nvl(p.ie_liberar_desfecho,'N')  = 'N') 
	or     ((b.dt_liberacao is not null) and (b.dt_inativacao is null)));

return	ie_retorno_w;

end obter_medico_ciente;
/
