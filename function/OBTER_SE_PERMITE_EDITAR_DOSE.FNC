create or replace
function obter_se_permite_editar_dose(	cd_material_p	number,
										cd_intervalo_p	varchar2)
		return varchar2 is

ie_permite_editar_w		char(1) := 'S';
ie_dose_diferenciada_w	intervalo_prescricao.ie_dose_diferenciada%type;

begin
if	(cd_material_p is not null) and
	(cd_intervalo_p is not null) then
	begin
	
	select	nvl(max(ie_editar_dose),'S')
	into	ie_permite_editar_w
	from	material
	where	cd_material = cd_material_p;
	
	if	(ie_permite_editar_w = 'S') then
		ie_permite_editar_w	:= 'N';
		
		select	nvl(max(ie_dose_diferenciada),'N')
		into	ie_dose_diferenciada_w
		from	intervalo_prescricao
		where	cd_intervalo = cd_intervalo_p;
	
		if	(nvl(ie_dose_diferenciada_w,'XPTO') in ('N','P','XPTO')) then
			ie_permite_editar_w	:= 'S';
		end if;
	else
		ie_permite_editar_w	:= 'N';
	end if;
	end;
end if;

return	ie_permite_editar_w;

end obter_se_permite_editar_dose;
/