create or replace function        CALC_EXPIRY_NO_OF_DAYS (cd_medico_p varchar2,cd_pessoa_fisica_p varchar2)
  return Number is
  noofdays_w Number;
begin
  if (cd_medico_p is not null and cd_pessoa_fisica_p is not null) then
    SELECT PKG_DATE_UTILS.get_DiffDate(sysdate,
                                       p.dt_ausencia_trabalho,
                                       'DAY')
      into noofdays_w
      from atestado_paciente p
     where p.nm_usuario =wheb_usuario_pck.get_nm_usuario
       and p.cd_medico = cd_medico_p
       and p.cd_pessoa_fisica = cd_pessoa_fisica_p
       and p.ie_situacao='A' and p.dt_liberacao is not null and rownum=1 order by dt_atualizacao desc ;
    return noofdays_w;
  end if;
  return 0;
end calc_expiry_no_of_days;
/
