create or replace
function pls_obter_dados_grupo_contrato
			(	nr_seq_grupo_p		number,
				ie_opcao_p		varchar2)
				return varchar2 is

/*	ie_opcao_p
QV - Quantidade de vidas
QVN - Quantidade de vidas n�o liberadas
N - Nome do grupo
RE - Respons�vel pela equipe
TR - Tipo de relacionamento
DTR - Data de reajuste
*/

ds_retorno_w			varchar2(255)	:= null;
cd_pessoa_responsavel_w		pls_grupo_contrato_equipe.cd_pessoa_responsavel%type;
ie_tipo_relacionamento_w	pls_grupo_contrato.ie_tipo_relacionamento%type;
dt_reajuste_w			pls_grupo_contrato.dt_reajuste%type;

begin
if	(ie_opcao_p = 'QV') then
	select	sum(qt_beneficiarios)
	into	ds_retorno_w
	from	(	select	sum(1) qt_beneficiarios
			from	pls_segurado		a,
				pls_contrato		b,
				pls_contrato_grupo	d,
				pls_grupo_contrato	c
			where	a.nr_seq_contrato	= b.nr_sequencia
			and	d.nr_seq_contrato	= b.nr_sequencia
			and	d.nr_seq_grupo		= c.nr_sequencia
			and	c.nr_sequencia		= nr_seq_grupo_p
			and	a.dt_liberacao is not null
			and	((a.dt_rescisao > sysdate and a.dt_rescisao is not null) 
			or 	(a.dt_rescisao is null))
			union all
			select	sum(1) qt_beneficiarios
			from	pls_segurado		a,
				pls_intercambio		b,
				pls_contrato_grupo	d,
				pls_grupo_contrato	c
			where	a.nr_seq_intercambio	= b.nr_sequencia
			and	d.nr_seq_intercambio	= b.nr_sequencia
			and	d.nr_seq_grupo		= c.nr_sequencia
			and	c.nr_sequencia		= nr_seq_grupo_p
			and	a.dt_liberacao is not null
			and	((a.dt_rescisao > sysdate and a.dt_rescisao is not null) 
			or 	(a.dt_rescisao is null)));
elsif	(ie_opcao_p = 'QVN') then
	select	sum(qt_beneficiarios)
	into	ds_retorno_w
	from	(	select	sum(1) qt_beneficiarios
			from	pls_segurado		a,
				pls_contrato		b,
				pls_contrato_grupo	d,
				pls_grupo_contrato	c
			where	a.nr_seq_contrato	= b.nr_sequencia
			and	d.nr_seq_contrato	= b.nr_sequencia
			and	d.nr_seq_grupo		= c.nr_sequencia
			and	c.nr_sequencia		= nr_seq_grupo_p
			and	((a.dt_rescisao > sysdate and a.dt_rescisao is not null) 
			or 	(a.dt_rescisao is null))
			union all
			select	sum(1) qt_beneficiarios
			from	pls_segurado		a,
				pls_intercambio		b,
				pls_contrato_grupo	d,
				pls_grupo_contrato	c
			where	a.nr_seq_intercambio	= b.nr_sequencia
			and	d.nr_seq_intercambio	= b.nr_sequencia
			and	d.nr_seq_grupo		= c.nr_sequencia
			and	c.nr_sequencia		= nr_seq_grupo_p
			and	((a.dt_rescisao > sysdate and a.dt_rescisao is not null) 
			or 	(a.dt_rescisao is null)));
elsif	(ie_opcao_p = 'N') then
	select	ds_grupo
	into	ds_retorno_w
	from	pls_grupo_contrato
	where	nr_sequencia	= nr_seq_grupo_p;
elsif	(ie_opcao_p = 'RE') then
	select	max(cd_pessoa_responsavel)
	into	cd_pessoa_responsavel_w
	from	pls_grupo_contrato_equipe
	where	nr_seq_grupo_contrato	= nr_seq_grupo_p
	and	dt_fim_vigencia is null;
	
	ds_retorno_w	:= obter_nome_pf(cd_pessoa_responsavel_w);
elsif	(ie_opcao_p = 'TR') then
	select	max(ie_tipo_relacionamento)
	into	ie_tipo_relacionamento_w
	from	pls_grupo_contrato
	where	nr_sequencia = nr_seq_grupo_p;
	
	if	(ie_tipo_relacionamento_w is not null) then
		select 	substr(max(ds_valor_dominio),1,60)
		into	ds_retorno_w
		from   	valor_dominio 
		where  	cd_dominio = 7548
		and	vl_dominio = ie_tipo_relacionamento_w;
	else
		ds_retorno_w := null;
	end if;
elsif	(ie_opcao_p = 'DTR') then
	select	max(dt_reajuste)
	into	dt_reajuste_w
	from	pls_grupo_contrato
	where	nr_sequencia = nr_seq_grupo_p;
	
	if	(dt_reajuste_w is not null) then
		ds_retorno_w	:= to_char(dt_reajuste_w,'dd/mm/yyyy');
	end if;
end if;

return	ds_retorno_w;

end pls_obter_dados_grupo_contrato;
/