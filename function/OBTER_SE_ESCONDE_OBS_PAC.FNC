create or replace
function obter_se_esconde_obs_pac(nr_seq_plano_p	number)
					return varchar2 is


ds_retorno_w			varchar2(2000)	:= 'N';
ds_alergias_w			varchar2(50);
qt_peso_w				number(10,4);
qt_altura_w				number(10,4);
qt_creatinina_w			number(10,4);
ds_sexo_w				varchar2(10);
ds_outras_condicoes_w	varchar2(50);
nr_linhas_w				number(5) := 0;

begin

select 	substr(replace(ds_alergias,'|',', '),1,50),
	  	qt_peso,
		qt_altura,
		qt_creatinina,
		decode(obter_sexo_pf(cd_pessoa_fisica,'C'),'F','w','M','m','unbestimmt'),
		substr(ds_outras_condicoes ,1,50)
into 	ds_alergias_w,
        qt_peso_w,
        qt_altura_w,
        qt_creatinina_w,
        ds_sexo_w,
        ds_outras_condicoes_w
from    plano_versao
where   nr_sequencia = nr_seq_plano_p;

if(ds_alergias_w is not null)then
	ds_retorno_w := ds_retorno_w || 'Allerg./Unv.: ' || ds_alergias_w ;
	nr_linhas_w := nr_linhas_w + 1;
end if;

if(qt_peso_w is not null)then
	ds_retorno_w := ds_retorno_w || chr(13)||chr(10) ||'Gew.: ' || qt_peso_w || ' Kg ';
end if;

if(qt_altura_w is not null)then
	if(nr_linhas_w < 3)then
		ds_retorno_w := ds_retorno_w || 'Gr��e: ' || qt_altura_w || ' cm ';
		nr_linhas_w := nr_linhas_w + 1;
	else
		nr_linhas_w := nr_linhas_w + 1;
	end if;
end if;

if(qt_creatinina_w is not null)then
	if(nr_linhas_w < 3)then
		ds_retorno_w := ds_retorno_w || chr(13)||chr(10) || 'Krea.: ' ||qt_creatinina_w || ' mg/dl';
		nr_linhas_w := nr_linhas_w + 1;
	else
		nr_linhas_w := nr_linhas_w + 1;
	end if;
end if;

if(ds_sexo_w is not null)then
	if(nr_linhas_w < 3)then
		ds_retorno_w := ds_retorno_w || chr(13)||chr(10) ||'Geschl.: ' || ds_sexo_w ;
		nr_linhas_w := nr_linhas_w + 1;
	else
		nr_linhas_w := nr_linhas_w + 1;
	end if;
end if;

if(ds_outras_condicoes_w is not null)then
	if(nr_linhas_w < 3)then
		ds_retorno_w := ds_retorno_w || chr(13)||chr(10) ||ds_outras_condicoes_w;
		nr_linhas_w := nr_linhas_w + 1;
	else
		nr_linhas_w := nr_linhas_w + 1;
	end if;
end if;

if(nr_linhas_w > 3 or length(ds_retorno_w) > 75)then
	ds_retorno_w := 'S';
end if;

return ds_retorno_w;

end obter_se_esconde_obs_pac;
/