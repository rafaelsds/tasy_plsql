create or replace
function obter_protocolo_trans(nr_sequencia_p varchar2)
 		    	return varchar2 is

			
ds_retorno_w 	nota_fiscal_lote_nfe.nr_protocolo%type;

begin

select 	max(nr_protocolo)
into	ds_retorno_w
from	nota_fiscal_lote_nfe
where	nr_seq_transmissao = nr_sequencia_p;	

return	ds_retorno_w;

end obter_protocolo_trans;
/