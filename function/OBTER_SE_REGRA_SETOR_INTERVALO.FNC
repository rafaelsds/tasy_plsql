create or replace
function Obter_se_regra_setor_intervalo(	cd_setor_atendimento_p		number,
						cd_intervalo_p			varchar2)
 		    	return varchar2 is

ie_retorno_w		varchar2(1);			
			
begin

select	nvl(max('N'),'S')
into	ie_retorno_w
from	rep_regra_setor_intervalo
where	cd_setor_atendimento = cd_setor_atendimento_p;

if	(ie_retorno_w = 'N') then

	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	rep_regra_setor_intervalo
	where	cd_intervalo = cd_intervalo_p
	and	cd_setor_atendimento = cd_setor_atendimento_p;

end if;

return	ie_retorno_w;

end Obter_se_regra_setor_intervalo;
/