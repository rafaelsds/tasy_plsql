create or replace
function pls_obter_inf_pagto(	nr_seq_prestador_p	number,
				nr_seq_lote_p		pls_lote_pagamento.nr_sequencia%type,
				cd_tributo_p		tributo.cd_tributo%type,
				ie_opcao_p		varchar2)
				return varchar2 is 

ds_retorno_w			varchar2(30);
cpf_cgc_w			varchar2(255);
vl_total_base_calculo_w		pls_pag_prest_venc_trib.vl_base_calculo%type;
dt_vencimento_w			pls_pag_prest_vencimento.dt_vencimento%type;

begin

cpf_cgc_w	:= '';

select	decode(cd_cgc, null, cd_pessoa_fisica, cd_cgc)
into	cpf_cgc_w
from	pls_prestador
where	nr_sequencia = nr_seq_prestador_p;

select	max(trunc(b.dt_vencimento,'mm'))
into	dt_vencimento_w
from	pls_pag_prest_vencimento	b,
	pls_pagamento_prestador		a
where	a.nr_sequencia			= b.nr_seq_pag_prestador
and	a.nr_seq_prestador		= nr_seq_prestador_p
and	a.nr_seq_lote			= nr_seq_lote_p;

select	nvl(sum(nvl(f.vl_base_calculo,0)),0)
into	vl_total_base_calculo_w
from	pls_lote_pagamento 		a,
	pls_pagamento_prestador		b,
	pls_prestador 			c,
	pls_pag_prest_vencimento	d,
	titulo_pagar 			e,
	pls_pag_prest_venc_trib		f
where	a.nr_sequencia 			= b.nr_seq_lote
and	b.nr_seq_prestador 		= c.nr_sequencia
and	d.nr_seq_pag_prestador 		= b.nr_sequencia
and	d.nr_sequencia			= f.nr_seq_vencimento
and	d.nr_titulo			= e.nr_titulo
and	((c.cd_pessoa_fisica		= cpf_cgc_w) or
	 (c.cd_cgc			= cpf_cgc_w))
and	f.cd_tributo			= cd_tributo_p
and	trunc(d.dt_vencimento,'mm')	= dt_vencimento_w
and	a.nr_sequencia <= nr_seq_lote_p;

if	(ie_opcao_p = 'TB') then
	ds_retorno_w := substr(vl_total_base_calculo_w,1,30);
end if;

return ds_retorno_w;

end pls_obter_inf_pagto;
/