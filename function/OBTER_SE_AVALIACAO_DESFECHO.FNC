create or replace
function obter_se_avaliacao_desfecho(nr_atendimento_p 		number)
			return varchar is

cd_setor_atendimento_w		number(5);
nr_seq_tipo_avaliacao_w		number(10);
nr_total_w			number(10)	:= 1;
ds_avaliacao_w			varchar2(200);
ds_retorno_w			varchar2(200);
cd_doenca_cid_w			varchar2(10);

cursor c02 is
select	nr_seq_avaliacao,
	cd_doenca_cid
from	setor_desfecho_aval
where	cd_setor_atendimento = cd_setor_atendimento_w;

begin

select	obter_setor_atendimento(nr_atendimento_p)
into	cd_setor_atendimento_w
from	dual;

OPEN C02;
LOOP
FETCH C02 into 	nr_seq_tipo_avaliacao_w,
		cd_doenca_cid_w;
	exit when c02%notfound;
	if	(obter_se_cid_atendimento(nr_atendimento_p,cd_doenca_cid_w) = 'S') then
		begin
		select	count(*)
		into	nr_total_w
		from	med_avaliacao_paciente
		where	nr_seq_tipo_avaliacao 	= nr_seq_tipo_avaliacao_w
		and	nr_atendimento 		= nr_atendimento_p;
		end;
	end if;
	
	if	(nr_total_w = 0) then
		exit;
	end if;

END LOOP;
CLOSE C02;

select	substr(obter_descricao_padrao('MED_TIPO_AVALIACAO','DS_TIPO', nr_seq_tipo_avaliacao_w),1,100)
into	ds_avaliacao_w
from	dual;

if	(nr_total_w = 0)	then
	ds_retorno_w := Wheb_mensagem_pck.get_texto(309800, 'DS_AVALIACAO_W='||ds_avaliacao_w); --'Para fazer o desfecho � obrigat�rio o preenchimento da avalia��o: '||ds_avaliacao_w||'.';
else	
	ds_retorno_w := '' ;
end if;

return	 ds_retorno_w;

end obter_se_avaliacao_desfecho;
/
