create or replace
function gpt_obter_info_revisao(nr_atendimento_p	number,
								cd_pessoa_fisica_p	varchar2,
								ie_opcao_p 			varchar2 default 'P')
 		    	return varchar2 is

nr_prescricao_w				prescr_medica.nr_prescricao%type;
nm_usuario_analise_enf_w	prescr_medica_compl.nm_usuario_analise_enf%type;
dt_inicio_analise_enf_w		varchar2(40);
ds_retorno_w		varchar2(254);
				
begin

select 	max(nm_usuario_revisao),
        max(to_char(dt_revisao,'dd/mm/yyyy hh24:mi:ss'))
into	nm_usuario_analise_enf_w,
        dt_inicio_analise_enf_w
from   ( select  nm_usuario_revisao,
                 dt_revisao
          from   table(gpt_utils_pck.get_rev_prescr_plan( nr_atendimento_p, 
                                                          cd_pessoa_fisica_p,
                                                          'N'))
      order by dt_revisao desc)
where rownum = 1;

if  (ie_opcao_p = 'U') and
    (nm_usuario_analise_enf_w is not null) then
    
    ds_retorno_w	:= substr(obter_nome_usuario(nm_usuario_analise_enf_w),1,254);

elsif	(ie_opcao_p = 'D') and
        (dt_inicio_analise_enf_w is not null) then
    
    ds_retorno_w	:= dt_inicio_analise_enf_w; --PKG_DATE_UTILS.get_DateTime(dt_inicio_analise_enf_w,dt_inicio_analise_enf_w);

elsif (ie_opcao_p = 'P') then
    
    ds_retorno_w	:= nr_prescricao_w;
    
end if;

return	ds_retorno_w;

end gpt_obter_info_revisao;
/