create or replace
function obter_unid_med_terap(
			nr_sequencia_p	regra_dose_terap.nr_sequencia%type) return varchar2 is

ie_unidade_w	regra_dose_terap.ie_unidade%type;
	
begin

if	(nr_sequencia_p is not null) then
	begin
	select	ie_unidade
	into 	ie_unidade_w	
	from 	regra_dose_terap
	where	nr_sequencia = nr_sequencia_p;		
	end;
end if;

return ie_unidade_w;

end obter_unid_med_terap;
/