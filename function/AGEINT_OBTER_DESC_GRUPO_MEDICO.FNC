create or replace
function ageint_obter_desc_grupo_medico(nr_seq_grupo_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
			
			
begin

select	max(ds_grupo)
into	ds_retorno_w
from	ageint_regra_grupo_medico
where	nr_sequencia = nr_seq_grupo_p;

return	ds_retorno_w;

end ageint_obter_desc_grupo_medico;
/