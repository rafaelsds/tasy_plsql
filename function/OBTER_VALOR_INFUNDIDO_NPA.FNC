create or replace
function Obter_valor_infundido_npa(	nr_prescricao_p		number, 
									ie_tipo_solucao_p 	number)
 		    	return number is
				
nr_sequencia_w		prescr_solucao_evento.nr_sequencia%type;
qt_vol_infundido_w	prescr_solucao_evento.qt_vol_infundido%type;
nr_seq_nut_neo_w   	prescr_solucao_evento.nr_seq_nut_neo%type; 
begin

select	nvl(max(nr_sequencia),0),
		max(nr_seq_nut_neo)
into	nr_sequencia_w,
		nr_seq_nut_neo_w
from	prescr_solucao_evento
where	nr_prescricao		= nr_prescricao_p
and		ie_tipo_solucao		= ie_tipo_solucao_p		
and		ie_alteracao 		= 2
and  	nr_seq_motivo		= 1
and		ie_evento_valido	= 'S';

if (nvl(nr_sequencia_w, 0) > 0) and
   (nvl(nr_seq_nut_neo_w, 0) > 0) then

	select	nvl(sum(qt_vol_infundido), 0)
	into	qt_vol_infundido_w
	from	prescr_solucao_evento
	where	nr_prescricao	= nr_prescricao_p
	and		ie_tipo_solucao	= ie_tipo_solucao_p	
	and 	nr_sequencia > nr_sequencia_w 
	and		nr_seq_nut_neo 	= nr_seq_nut_neo_w
	and		ie_alteracao 	= 2
	and		nr_seq_motivo <> 1
	and		ie_evento_valido	= 'S';

end if;	

return	nvl(qt_vol_infundido_w,0);

end Obter_valor_infundido_npa;
/		