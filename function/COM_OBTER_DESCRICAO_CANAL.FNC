create or replace
function com_obter_descricao_canal(nr_seq_canal_p	com_canal.nr_sequencia%type,
				   ds_opcao_p varchar2)
 		    	return varchar2 is
			
ds_valor_canal_w	com_canal.nm_guerra%type;
nr_sequencia_w		com_canal.nr_sequencia%type;
nm_guerra_w		com_canal.nm_guerra%type;
			
begin

select	max(b.nr_sequencia),
	max(b.nm_guerra)			
into	nr_sequencia_w,
	nm_guerra_w			
from	com_canal b
where 	nr_sequencia = nr_seq_canal_p;

if (ds_opcao_p = 'C') then
	begin
	ds_valor_canal_w := nr_sequencia_w;
	end;
else
	begin
	ds_valor_canal_w := nm_guerra_w;
	end;
end if;

return	ds_valor_canal_w;

end com_obter_descricao_canal;
/