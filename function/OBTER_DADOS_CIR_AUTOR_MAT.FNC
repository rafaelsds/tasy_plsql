create or replace
function obter_dados_cir_autor_mat(	nr_atendimento_p	in number,
					nr_seq_agenda_p		in number,
					ie_opcao_p		in varchar2)
					return varchar2 is

nr_cirurgia_w			number(10);
ds_retorno_w			varchar2(255);

ie_mostra_realizadas_w		varchar2(5) := 'N';
dt_inicio_previsto_w		date;
/*
ie_opcao_p
NR - N�mero da cirurgia
DT - Data prevista
*/

begin

obter_param_usuario(3006,31,obter_perfil_ativo,Wheb_Usuario_Pck.Get_nm_usuario,Wheb_Usuario_Pck.Get_cd_estabelecimento,ie_mostra_realizadas_w);

if	(nvl(nr_seq_agenda_p,0) > 0) then

	select	max(nr_cirurgia),
		max(dt_inicio_prevista)
	into	nr_cirurgia_w,
		dt_inicio_previsto_w
	from	cirurgia
	where	nr_seq_agenda		= nr_seq_agenda_p;

elsif	(nvl(nr_atendimento_p,0) > 0) then

	select	max(a.nr_cirurgia),
		max(a.dt_inicio_prevista) dt
	into	nr_cirurgia_w,
		dt_inicio_previsto_w
	from	cirurgia a
	where	a.nr_atendimento	= nr_atendimento_p;

end if;

if	(ie_opcao_p = 'NR') then
	ds_retorno_w	:= nr_cirurgia_w;
ELSIF	(ie_opcao_p = 'DT') THEN

	if	(nvl(ie_mostra_realizadas_w,'N') = 'N') and
		(sysdate < dt_inicio_previsto_w) then
		
		ds_retorno_w := to_char(dt_inicio_previsto_w,'dd/mm/yyyy hh24:mi:ss');
		
	elsif	(nvl(ie_mostra_realizadas_w,'N') = 'S') then
	
		ds_retorno_w := to_char(dt_inicio_previsto_w,'dd/mm/yyyy hh24:mi:ss');
	end if;

end if;

return ds_retorno_w;

end;
/
