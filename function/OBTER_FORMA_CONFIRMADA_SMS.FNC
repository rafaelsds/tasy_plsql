create or replace
function obter_forma_confirmada_sms (	nr_sequencia_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);
ds_retorno_cons_w	varchar2(255);
ds_retorno_exam_w	varchar2(255);
nr_seq_agenda_exame_w	number(10);
nr_seq_agenda_cons_w	number(10);

Cursor C01 is
	select	nr_seq_agenda_exame, 
		nr_seq_agenda_cons
	from	agenda_integrada_item
	where	nr_seq_agenda_int =  nr_sequencia_p;
	
	
begin
if (nvl(nr_sequencia_p,0) > 0) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_agenda_exame_w,
		nr_seq_agenda_cons_w;
	exit when C01%notfound;
		begin
		
		if (nvl(nr_seq_agenda_cons_w,0) > 0) then
		
			select  max(obter_desc_forma_conf(nr_seq_forma_confirmacao))
			into	ds_retorno_cons_w
			from	agenda_consulta
			where	nr_sequencia 	= nr_seq_agenda_cons_w;
		
		elsif (nvl(nr_seq_agenda_exame_w,0) > 0) then
			select  max(obter_desc_forma_conf(nr_seq_forma_confirmacao))
			into	ds_retorno_exam_w
			from	agenda_paciente
			where	nr_sequencia 	= nr_seq_agenda_exame_w;
		end if;
		
		end;
	end loop;
	close C01;
end if;

if (ds_retorno_cons_w is not null) then
	ds_retorno_w	:= ds_retorno_cons_w;
else
	ds_retorno_w	:= ds_retorno_exam_w;
end if;

return	ds_retorno_w;

end obter_forma_confirmada_sms;
/	