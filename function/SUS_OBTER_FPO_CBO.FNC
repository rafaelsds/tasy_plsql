create or replace
function sus_obter_fpo_cbo (	cd_cbo_p		varchar2,
			nr_seq_regra_p		number)
			return varchar2 is

ds_retorno_w		varchar2(15) := 'N';
qt_cbo_w			number(10) := 0;				
				
begin

begin
select	nvl(count(*),0)
into	qt_cbo_w
from	sus_fpo_regra_cbo
where	cd_cbo		= cd_cbo_p
and	nr_seq_fpo_regra	= nr_seq_regra_p;
exception
when others then
	qt_cbo_w := 0;
end;

if	(nvl(qt_cbo_w,0) > 0) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;
	
return	ds_retorno_w;

end sus_obter_fpo_cbo;
/