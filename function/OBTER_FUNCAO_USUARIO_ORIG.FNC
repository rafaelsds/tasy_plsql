create or replace
function Obter_funcao_usuario_orig( nm_usuario_orig_p varchar2)
     return varchar2 is

ds_retorno_w varchar2(255);

begin

select  substr(nvl(b.ds_valor_dominio_cliente,obter_valor_dominio(72,b.vl_dominio)),1,255)
into ds_retorno_w
from valor_dominio_v b,
 usuario a
where a.nm_usuario  = nm_usuario_orig_p
and b.cd_dominio  = 72
and a.ie_tipo_evolucao = b.vl_dominio;

return ds_retorno_w;

end Obter_funcao_usuario_orig;
/