create or replace
function Obter_desc_san_indicacao
		(nr_sequencia_p		Number)
		return varchar2 is

ds_retorno_w	Varchar2(256);

begin

if 	(nr_sequencia_p is not null) then
	select	ds_indicacao
	into	ds_retorno_w
	from	san_indicacao
	where	nr_sequencia	= nr_sequencia_p;
end if;
	
return	ds_retorno_w;

end Obter_desc_san_indicacao;
/