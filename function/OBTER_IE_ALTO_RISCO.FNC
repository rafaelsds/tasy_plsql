create or replace
function obter_ie_alto_risco (	nr_prescricao_p		number,
								nr_seq_solucao_p 	number) 
								return varchar2 is
								
ie_alto_risco_w varchar2(1);


begin
ie_alto_risco_w := 'N';

if	(nvl(nr_prescricao_p,0) > 0) and
	(nvl(nr_seq_solucao_p,0) > 0) then

	select	nvl(max(a.ie_alto_risco),'N')
	into	ie_alto_risco_w
	from	prescr_material a,
		prescr_solucao b
	where	a.nr_prescricao	= nr_prescricao_p
	and	a.nr_sequencia_solucao	= nr_seq_solucao_p
	and	a.nr_sequencia_solucao	= b.nr_seq_solucao
	and	a.nr_prescricao	= b.nr_prescricao
	and	a.ie_alto_risco	= 'S';
	
end if;

return ie_alto_risco_w;
	
end ;
/

