create or replace
function pls_obter_se_excluir_partic
			(	nr_seq_participante_p	Number,
				nm_usuario_p		Varchar2)
				return Varchar2 is

ie_retorno_w			Varchar2(2);
ie_insercao_manual_w		Varchar2(2);
nm_usuario_nrec_w		Varchar2(255);

begin

begin
	select	nvl(ie_insercao_manual,'N'),
		nm_usuario_nrec
	into	ie_insercao_manual_w,
		nm_usuario_nrec_w
	from	pls_proc_participante
	where	nr_sequencia	= nr_seq_participante_p;
exception
when others then
	ie_insercao_manual_w	:= 'N';
end;

if	(ie_insercao_manual_w	= 'N') or ((ie_insercao_manual_w	= 'S') and (nm_usuario_nrec_w	<> nm_usuario_p)) then
	ie_retorno_w	:= 'N';
elsif	(ie_insercao_manual_w	= 'S') and (nm_usuario_nrec_w	= nm_usuario_p) then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end pls_obter_se_excluir_partic;
/