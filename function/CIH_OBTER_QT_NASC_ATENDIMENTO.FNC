create or replace
function CIH_OBTER_QT_NASC_ATENDIMENTO (nr_atendimento_p	number) return number is

qt_nascimentos_w	number(15);

begin

select	count(*)
into	qt_nascimentos_w
from	nascimento
where	nr_atendimento		= nr_atendimento_p
and	ie_unico_nasc_vivo	= 'S';

return	qt_nascimentos_w;

end CIH_OBTER_QT_NASC_ATENDIMENTO;
/
