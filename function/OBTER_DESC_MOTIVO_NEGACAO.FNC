create or replace 
function obter_desc_motivo_negacao(
			nr_sequencia_p	number)
			return varchar2 is

ds_retorno_w	varchar2(60);

begin

if	(nr_sequencia_p is not null) then
	begin
       select ds_motivo
	   into	  ds_retorno_w
	   from	  solic_vaga_motivo_negacao
	   where  nr_sequencia	= nr_sequencia_p;
	   
	exception
    when others then
		ds_retorno_w	:= '';
	end;
end if;


return	ds_retorno_w;

end obter_desc_motivo_negacao;
/