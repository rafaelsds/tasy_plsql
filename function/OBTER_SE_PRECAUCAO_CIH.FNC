create or replace
function obter_Se_precaucao_cih(nr_atendimento_p	number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(1);

begin

select	decode(count(*),0,'N','S')
into	ds_retorno_w 
from	atendimento_precaucao a 
where	a.nr_atendimento = nr_atendimento_p;

return	ds_retorno_w;

end obter_Se_precaucao_cih;
/