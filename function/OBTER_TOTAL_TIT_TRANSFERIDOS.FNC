create or replace function obter_total_tit_transferidos(cd_pessoa_p number, 
						       tp_pessoa_p number, 
						       nm_usuario_p varchar2) 
return number is vl_transferido number(11,2);
begin
	select nvl(sum(a.vl_titulo),0) 
	into vl_transferido
	from	titulo_pagar a	
	where	a.ie_situacao = 'T'
		and ((tp_pessoa_p = 0 
		     and decode(tp_pessoa_p, 0, null, cd_pessoa_p) is not null
		     and exists	(select	1 
				 from	w_ficha_financ_consulta x	
				 where	x.nr_titulo_cp = a.nr_titulo
				 and	x.cd_cgc = decode(tp_pessoa_p, 0, null, cd_pessoa_p)
				 and x.nm_usuario = nm_usuario_p))
		or (tp_pessoa_p = 0 
		    and decode(tp_pessoa_p, 0, cd_pessoa_p, null) is not null
		    and exists	(select	1
				 from	w_ficha_financ_consulta x
				 where	x.nr_titulo_cp = a.nr_titulo
				 and	x.cd_pessoa_fisica = decode(tp_pessoa_p, 0, cd_pessoa_p, null)
				 and	x.nm_usuario = nm_usuario_p))
		or (tp_pessoa_p <> 0 
		    and exists	(select	1 
				 from	w_ficha_financ_consulta x 
				 where	x.nr_titulo_cp = a.nr_titulo
				 and	x.cd_cgc = cd_pessoa_p
				 and	x.nm_usuario = nm_usuario_p)));
	return vl_transferido;

end obter_total_tit_transferidos;
/