create or replace
function obter_seq_grupo_diag(	nr_seq_grupo_p	number)
				return number is
				
nr_seq_apres_w		number(10);
				
begin
if	(nr_seq_grupo_p	is not null) and
	(nr_seq_grupo_p	> 0) then
	
	select	NR_SEQ_APRES
	into	nr_seq_apres_w
	from	diagnostico_grupo
	where	nr_sequencia	= nr_seq_grupo_p;
end if;
return nr_seq_apres_w;

end obter_seq_grupo_diag;
/
