create or replace
function obter_se_med_uso_cont_padr_lib(nr_sequencia_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(10)	:= 'S';
qt_registro_w	number(10);
cd_pessoa_fisica_w	number(10);
cd_perfil_w		number(10);

begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	usuario
where	nm_usuario	= wheb_usuario_pck.get_nm_usuario;

cd_perfil_w	:= obter_perfil_ativo;

select	count(*)
into	qt_registro_w
from	medic_uso_cont_pad_regra
where	nr_seq_medic	= nr_sequencia_p;

if	(qt_registro_w	> 0) then
	select	count(*)
	into	qt_registro_w
	from	medic_uso_cont_pad_regra
	where	nr_seq_medic	= nr_sequencia_p
	and	nvl(cd_perfil,cd_perfil_w)	= cd_perfil_w
	and	((cd_especialidade is null) or (obter_se_especialidade_medico(cd_pessoa_fisica_w,cd_especialidade)	= 'S') );
	
	if	(qt_registro_w	= 0) then
		ds_retorno_w	:= 'N';
	end if;
end if;

return	ds_retorno_w;

end obter_se_med_uso_cont_padr_lib;
/
