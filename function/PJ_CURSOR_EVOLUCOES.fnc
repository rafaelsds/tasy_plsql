CREATE OR REPLACE FUNCTION pj_cursor_evolucoes(dt_inicial_p       DATE,
                                               dt_final_p         DATE,
                                               cd_pessoa_fisica_p VARCHAR2,
                                               nr_seq_modelo_p    NUMBER,
                                               ie_problema_paciente_p VARCHAR) RETURN SYS_REFCURSOR IS
    cursor_evolucoes_w SYS_REFCURSOR;
    count_regras_w     NUMBER;

BEGIN

    SELECT COUNT(*)
      INTO count_regras_w
      FROM informacao_suep ifsp,
           item_suep       itsp
     WHERE ifsp.nr_seq_item = itsp.nr_sequencia
       AND itsp.nr_seq_suep = nr_seq_modelo_p
       AND itsp.ie_tipo_item = 'EV';

    IF count_regras_w = 0 THEN
        OPEN cursor_evolucoes_w FOR
            SELECT ev.dt_evolucao dt_registro,
                   ev.cd_evolucao nr_sequencia,
                   substr((SELECT obter_nome_pf(ev.cd_medico) FROM dual), 1, 60) ds_profissional,
                   substr(obter_valor_dominio(72, ev.ie_tipo_evolucao), 1, 200) ds_funcao_usuario,
                   substr((SELECT obter_desc_tipo_evolucao(ev.ie_evolucao_clinica) FROM dual), 1, 200) ds_tipo_evolucao,
                   ev.nr_atendimento nr_atendimento_reg,
                   281 cd_funcao,
                   5 nr_seq_item_pront,
                   ev.ie_nivel_atencao,
                   'EVOLUCAO_PACIENTE' nm_tabela
              FROM evolucao_paciente ev
             WHERE ev.cd_pessoa_fisica = cd_pessoa_fisica_p
               AND ev.dt_liberacao IS NOT NULL
               AND ev.dt_inativacao IS NULL
               AND (ie_problema_paciente_p is null or obter_se_problema_paciente(ie_problema_paciente_p, ev.cd_doenca, ev.nr_atendimento) = 'S')
               AND nvl(ev.dt_liberacao, ev.dt_atualizacao_nrec) BETWEEN dt_inicial_p AND dt_final_p
             ORDER BY 3;
    ELSE
        OPEN cursor_evolucoes_w FOR
            SELECT ev.dt_evolucao dt_registro,
                   ev.cd_evolucao nr_sequencia,
                   substr((SELECT obter_nome_pf(ev.cd_medico) FROM dual), 1, 60) ds_profissional,
                   substr(obter_valor_dominio(72, ev.ie_tipo_evolucao), 1, 200) ds_funcao_usuario,
                   substr((SELECT obter_desc_tipo_evolucao(ev.ie_evolucao_clinica) FROM dual), 1, 200) ds_tipo_evolucao,
                   ev.nr_atendimento nr_atendimento_reg,
                   281 cd_funcao,
                   5 nr_seq_item_pront,
                   ev.ie_nivel_atencao,
                   'EVOLUCAO_PACIENTE' nm_tabela
              FROM evolucao_paciente ev,
                   item_suep         itsp,
                   informacao_suep   ifsp
             WHERE ev.cd_pessoa_fisica = cd_pessoa_fisica_p
               AND ev.dt_liberacao IS NOT NULL
               AND ev.dt_inativacao IS NULL
               AND (ie_problema_paciente_p is null or obter_se_problema_paciente(ie_problema_paciente_p, ev.cd_doenca, ev.nr_atendimento) = 'S')
               AND nvl(ev.dt_liberacao, ev.dt_atualizacao_nrec) BETWEEN dt_inicial_p AND dt_final_p
               AND itsp.nr_seq_suep = nr_seq_modelo_p
               AND itsp.ie_tipo_item = 'EV'
               AND itsp.nr_sequencia = ifsp.nr_seq_item
               AND ev.ie_evolucao_clinica = ifsp.ie_evolucao_clinica
             GROUP BY ev.cd_evolucao,
                      ev.dt_evolucao,
                      ev.cd_medico,
                      ev.ie_tipo_evolucao,
                      ev.ie_evolucao_clinica,
                      ev.nr_atendimento,
                      ev.ie_nivel_atencao
             ORDER BY 3;
    
    END IF;

    RETURN cursor_evolucoes_w;
END pj_cursor_evolucoes;
/
