create or replace
function obter_item_contrato_mercado(	cd_material_p		number,
					cd_estabelecimento_p	number)
 		    	return varchar2 is

			
ie_contrato_w		varchar2(15);
ie_mercado_w		varchar2(15);
ds_retorno_w		varchar2(15) := '';
			
begin

select	nvl(obter_se_item_contrato(cd_material_p),'N')
into	ie_contrato_w
from	dual;

if	(ie_contrato_w = 'S') then
	ds_retorno_w	:= 'C';
else
	select	nvl(obter_se_item_regra_solic_psc(cd_material_p, cd_estabelecimento_p),'N')
	into	ie_mercado_w
	from	dual;
	
	if	(ie_mercado_w = 'S') then
		ds_retorno_w := 'M';
	end if;	
end if;

return	ds_retorno_w;

end obter_item_contrato_mercado;
/