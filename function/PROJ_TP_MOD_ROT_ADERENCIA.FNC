create or replace
function proj_tp_mod_rot_aderencia(	nr_seq_cliente_p	number,
						nr_seq_modulo_p	number)
 			    			return number is

qt_retorno_w			number(17,2);
pr_grau_importancia_w	number(17,2);

begin

select	sum(a.pr_grau_importancia)
into	pr_grau_importancia_w
from	proj_tp_cli_rot b,
	proj_tp_roteiro a
where	a.nr_sequencia = b.nr_seq_roteiro
and	b.nr_seq_cliente = nr_seq_cliente_p
and	a.nr_seq_modulo = nr_seq_modulo_p;

select	dividir(sum(proj_tp_rot_aderencia(nr_seq_cliente_p,nr_sequencia) * pr_grau_importancia),pr_grau_importancia_w)
into	qt_retorno_w
from	proj_tp_roteiro
where	nr_seq_modulo = nr_seq_modulo_p;

return	qt_retorno_w;

end proj_tp_mod_rot_aderencia;
/