create or replace
function pls_obter_segurado_data
			(	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
				dt_referencia_p		pls_conta.dt_atendimento_referencia%type)
				return pls_segurado.ie_tipo_segurado%type is

ie_tipo_segurado_w		pls_segurado.ie_tipo_segurado%type;
qt_repasse_w			pls_integer;
ie_tipo_segurado_ww		pls_segurado.ie_tipo_segurado%type;
dt_alteracao_tipo_segurado_w	pls_segurado.dt_alteracao_tipo_segurado%type;

begin

select	max(dt_alteracao_tipo_segurado),
	max(ie_tipo_segurado)
into	dt_alteracao_tipo_segurado_w,
	ie_tipo_segurado_ww
from	pls_segurado
where	nr_sequencia = nr_seq_segurado_p;

if	(dt_referencia_p >= dt_alteracao_tipo_segurado_w) then
	ie_tipo_segurado_w	:= ie_tipo_segurado_ww;
else
	select	count(1)
	into	qt_repasse_w
	from	pls_segurado_repasse
	where	nr_seq_segurado	= nr_seq_segurado_p
	and	ie_tipo_compartilhamento = 1 --Repasse
	and	dt_liberacao is not null
	and	fim_dia(dt_referencia_p) >= dt_repasse
	and	trunc(dt_referencia_p,'dd') <= nvl(dt_fim_repasse,dt_referencia_p);

	if	(qt_repasse_w > 0) then --Precisa verificar se tem repasse, pois pode haver cadastro com data retroativa
		ie_tipo_segurado_w	:= 'R'; --Repasse para outra operadora  (Resp. Transferida)
	else
		select	max(w.ie_tipo_segurado)
		into	ie_tipo_segurado_w
		from	pls_segurado_historico w
		where	w.nr_sequencia = (	select	max(a.nr_sequencia)
						from	pls_segurado_historico a
						where	a.nr_seq_segurado = nr_seq_segurado_p
						and	a.ie_tipo_historico = '102'
						and	nvl(a.ie_situacao_compartilhamento, 'A') = 'A'
						and	a.dt_ocorrencia_sib = (	select	max(x.dt_ocorrencia_sib)
										from	pls_segurado_historico x
										where	x.nr_seq_segurado = nr_seq_segurado_p
										and	x.ie_tipo_historico = '102'
										and	x.dt_ocorrencia_sib <= trunc(dt_referencia_p,'dd')
										and	nvl(x.ie_situacao_compartilhamento, 'A') = 'A'));
		
		if	(ie_tipo_segurado_w is null) then
			select	max(w.ie_tipo_segurado_ant) --Se n�o encontrar nenhuma altera��o no passado, busca por altera��es com data futura e pega o ie_tipo_segurado_ant
			into	ie_tipo_segurado_w
			from	pls_segurado_historico w
			where	w.nr_sequencia = (	select	max(a.nr_sequencia)
							from	pls_segurado_historico a
							where	a.nr_seq_segurado = nr_seq_segurado_p
							and	a.ie_tipo_historico = '102'
							and	nvl(a.ie_situacao_compartilhamento, 'A') = 'A'
							and	a.dt_ocorrencia_sib = (	select	min(x.dt_ocorrencia_sib)
											from	pls_segurado_historico x
											where	x.nr_seq_segurado = nr_seq_segurado_p
											and	x.ie_tipo_historico = '102'
											and	x.dt_ocorrencia_sib > trunc(dt_referencia_p,'dd')
											and	nvl(x.ie_situacao_compartilhamento, 'A') = 'A'));
			
			if	(ie_tipo_segurado_w is null) then
				ie_tipo_segurado_w := ie_tipo_segurado_ww;
			end if;
		end if;
	end if;	
end if;

return	ie_tipo_segurado_w;

end pls_obter_segurado_data;
/
