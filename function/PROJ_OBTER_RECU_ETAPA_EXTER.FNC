create or replace
function proj_obter_recu_etapa_exter(nr_seq_etapa_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
ds_equipe_w	varchar2(255);
ds_equipe_ww	varchar2(255);

cursor c01 is
	select	obter_nome_pessoa_fisica(cd_recurso_externo, null)
	from	proj_cron_etapa_equipe
	where	nr_seq_etapa_cron = nr_seq_etapa_p
	and	ie_recurso_externo = 'S';

begin

ds_equipe_ww := '';

open c01;
loop
fetch c01 into	
	ds_equipe_w;
exit when c01%notfound;
	begin
	
	ds_equipe_ww := ds_equipe_ww || ', ' || ds_equipe_w;
	
	end;
end loop;
close c01;

ds_retorno_w := substr(ds_equipe_ww,2,255);

return	ds_retorno_w;

end proj_obter_recu_etapa_exter;
/