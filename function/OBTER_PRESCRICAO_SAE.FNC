create or replace
function obter_prescricao_sae	(nr_prescr_sae_p	number)
					return varchar2 is

nr_prescricao_w	number(14,0) := 0;

begin
if	(nr_prescr_sae_p is not null) then

	select	nvl(max(nr_prescricao),0)
	into	nr_prescricao_w
	from	pe_prescricao
	where	nr_sequencia = nr_prescr_sae_p;

end if;

return nr_prescricao_w;

end obter_prescricao_sae;
/