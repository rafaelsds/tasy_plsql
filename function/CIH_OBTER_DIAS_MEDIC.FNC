CREATE OR REPLACE
FUNCTION CIH_Obter_Dias_Medic(	nr_ficha_ocorrencia_p	number,
				nr_seq_medicamento_p	number,
				dt_inicial_p 		date,
				dt_final_p		date)
				return number is


dt_inicial_utilizacao_w		date;
dt_final_utilizacao_w		date;
dt_inicial_w			date;
dt_final_w			date;
qt_dias_w			number(10);

BEGIN

select	dt_inicial_utilizacao,
	dt_final_utilizacao
into	dt_inicial_utilizacao_w,
	dt_final_utilizacao_w
from	cih_medic_utilizado_v
where	nr_seq_medicamento	= nr_seq_medicamento_p
and	nr_ficha_ocorrencia	= nr_ficha_ocorrencia_p;

dt_inicial_w		:= dt_inicial_utilizacao_w;
if	(dt_inicial_utilizacao_w < dt_inicial_p) then
	dt_inicial_w	:= dt_inicial_p;
end if;

dt_final_w			:= dt_final_utilizacao_w;
if	(dt_final_utilizacao_w > dt_final_p) then
	dt_final_w		:= dt_final_p;
end if;

qt_dias_w	:= (dt_final_w - dt_inicial_w) + 1;

return qt_dias_w;
END CIH_Obter_Dias_Medic;
/