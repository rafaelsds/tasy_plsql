create or replace
function TISS_OBTER_REGRA_QTDE_PROC(nr_seq_procedimento_p	in number)
				return varchar2 is


cd_estabelecimento_w	number(10);
cd_convenio_w		number(10);
ie_regra_w		varchar2(10);
ie_regra_convenio_w	varchar2(10);
ds_retorno_w		varchar2(10);
cd_procedimento_w		tiss_regra_quantidade_mat.cd_procedimento%type;
ie_origem_proced_w	tiss_regra_quantidade_mat.ie_origem_proced%type;
cd_grupo_proc_w		tiss_regra_quantidade_mat.cd_grupo_proc%type;
cd_area_procedimento_w	tiss_regra_quantidade_mat.cd_area_procedimento%type;
cd_especialidade_w	tiss_regra_quantidade_mat.cd_especialidade%type;



cursor 	c01 is
select	a.ie_conversao_qtde_mat
from	tiss_regra_quantidade_mat a
where	a.cd_estabelecimento					= cd_estabelecimento_w
and	nvl(a.cd_convenio,nvl(cd_convenio_w,0))			= nvl(cd_convenio_w,0)
and	nvl(a.cd_procedimento,nvl(cd_procedimento_w,0))		= nvl(cd_procedimento_w,0)
and	nvl(a.ie_origem_proced,nvl(ie_origem_proced_w,0))		= nvl(ie_origem_proced_w,0)
and	nvl(a.cd_area_procedimento,nvl(cd_area_procedimento_w,0))	= nvl(cd_area_procedimento_w,0)
and 	nvl(a.cd_especialidade,nvl(cd_especialidade_w,0))		= nvl(cd_especialidade_w,0)
and	nvl(a.cd_grupo_proc,nvl(cd_grupo_proc_w,0))			= nvl(cd_grupo_proc_w,0)
and 	nvl(a.ie_situacao,'A') = 'A'
order by
	nvl(a.cd_procedimento,0),
	nvl(a.cd_grupo_proc,0),
	nvl(a.cd_especialidade,0),
	nvl(a.cd_area_procedimento,0),	
	nvl(a.cd_convenio,0);

begin

select	max(cd_procedimento),
	max(ie_origem_proced),
	max(cd_convenio),	
	max(cd_estabelecimento)
into	cd_procedimento_w,
	ie_origem_proced_w,
	cd_convenio_w,
	cd_estabelecimento_w	
from	conta_paciente b,
	procedimento_paciente a
where	a.nr_interno_conta	= b.nr_interno_conta
and	a.nr_sequencia		= nr_seq_procedimento_p;

begin
select	cd_area_procedimento,
	cd_especialidade,
	cd_grupo_proc	
into	cd_area_procedimento_w,
	cd_especialidade_w,
	cd_grupo_proc_w	
from	estrutura_procedimento_v
where	cd_procedimento = cd_procedimento_w
and	rownum = 1;
exception
when others then
	cd_especialidade_w	:= null;
	cd_grupo_proc_w		:= null;
	cd_area_procedimento_w	:= null;
end;

open C01;
loop
fetch C01 into	
	ie_regra_convenio_w;
exit when C01%notfound;	
	ds_retorno_w	:= ie_regra_convenio_w;
end loop;
close C01;

if	(ie_regra_convenio_w is null) then
	ds_retorno_w	:= 'M';
end if;


return	ds_retorno_w;

end TISS_OBTER_REGRA_QTDE_PROC;
/
