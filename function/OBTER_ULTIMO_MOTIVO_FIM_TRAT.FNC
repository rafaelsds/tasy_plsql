create or replace
function obter_ultimo_motivo_fim_trat(cd_pessoa_fisica_p varchar2,
										ie_retorno_p varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);	
dt_retorno_w	date;
			
begin
ds_retorno_w	:= '';

if (cd_pessoa_fisica_p is not null) then

	select max(substr(obter_dados_motivo_fim(nr_seq_motivo_fim,'D'),1,255)),
			max(DT_FINAL_TRATAMENTO)
	into  ds_retorno_w,
		  dt_retorno_w
	from  (SELECT nr_seq_motivo_fim,
					dt_final_tratamento
			from  	paciente_tratamento p
			where 	p.cd_pessoa_fisica = cd_pessoa_fisica_p
			and		not exists(select 1 from paciente_tratamento x where x.cd_pessoa_fisica = p.cd_pessoa_Fisica and x.dt_final_tratamento is null)			
			order by dt_final_tratamento desc)
	where	rownum = 1;	

end if;

if (ie_retorno_p = 'M') then
	return	ds_retorno_w;
else
	return	dt_retorno_w;
end if;

end obter_ultimo_motivo_fim_trat;
/