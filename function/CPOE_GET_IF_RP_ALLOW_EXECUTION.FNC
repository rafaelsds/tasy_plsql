create or replace function cpoe_get_if_rp_allow_execution(
                              nr_seq_cpoe_rp_p in number)
 		    	            return varchar2 is
									
ds_retorno_w		cpoe_rp.ie_allow_execution%type	:= 'N';

begin

	select	max(ie_allow_execution)
	into 	ds_retorno_w
	from	cpoe_rp
	where	nr_sequencia = nr_seq_cpoe_rp_p;
	
	return ds_retorno_w;
	
end cpoe_get_if_rp_allow_execution;
/
