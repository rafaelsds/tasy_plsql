create or replace
function Obter_Nr_Transplante_PF(cd_pessoa_fisica_p		Varchar2)
				return number is 
				
nr_transplante_w	number(10,0);
begin

SELECT		max(a.nr_transplante) nr_transplante
INTO		nr_transplante_w
FROM		pessoa_fisica a
WHERE		a.cd_pessoa_fisica = cd_pessoa_fisica_p;

return nr_transplante_w;

end Obter_Nr_Transplante_PF;
/