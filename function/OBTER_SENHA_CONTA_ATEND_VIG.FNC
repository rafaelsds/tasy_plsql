create or replace
function OBTER_SENHA_CONTA_ATEND_VIG
			(nr_atendimento_p		NUMBER,
			cd_convenio_p			number,
			nr_interno_conta_p		number)
				return VARCHAR2 is

cd_senha_w		VARCHAR2(30);
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;

begin

select	dt_periodo_inicial,
	dt_periodo_final
into	dt_periodo_inicial_w,
	dt_periodo_final_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

select	MAX(cd_senha)
into	cd_senha_w
from	atend_categoria_convenio
where	nr_atendimento		= nr_atendimento_p
and	cd_convenio		= cd_convenio_p
and	dt_inicio_vigencia	= (	select	MAX(dt_inicio_vigencia)
			  		from	atend_categoria_convenio
			  		where	nr_atendimento	= nr_atendimento_p
					and	cd_convenio	= cd_convenio_p
					and	nvl(dt_periodo_inicial_w, sysdate) between dt_inicio_vigencia and nvl(dt_final_vigencia, sysdate));

return	cd_senha_w;

end OBTER_SENHA_CONTA_ATEND_VIG;
/
