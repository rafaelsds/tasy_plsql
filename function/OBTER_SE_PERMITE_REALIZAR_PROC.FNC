create or replace
function obter_se_permite_realizar_proc(nr_atendimento_p	number,
					dt_inicio_real_p	date,
					dt_termino_p		date)
 		    		return varchar2 is

ie_exec_proc_conta_fechada_w	varchar2(1); -- 132
ie_perm_env_proc_fim_conta_w	varchar2(1); -- 196
ie_perm_real_proc_sem_fim_w	varchar2(1); -- 420

ie_fim_conta_w			varchar2(1);
dt_fim_conta_w			date;
ie_conta_fechada_w		varchar2(1);

begin

Obter_param_Usuario(900, 132, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_exec_proc_conta_fechada_w);
Obter_param_Usuario(900, 196, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_perm_env_proc_fim_conta_w);
Obter_param_Usuario(900, 420, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_perm_real_proc_sem_fim_w);

select	max(ie_fim_conta),
	max(dt_fim_conta)
into	ie_fim_conta_w,
	dt_fim_conta_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

if	(nvl(ie_fim_conta_w, 'X') = 'F') and
	(dt_fim_conta_w is not null) then
	ie_conta_fechada_w := 'S';
else
	ie_conta_fechada_w := 'N';
end if;

if	(dt_inicio_real_p is not null) and
	((ie_exec_proc_conta_fechada_w = 'S') or (ie_conta_fechada_w = 'N')) and
	((ie_perm_real_proc_sem_fim_w = 'S')  or (dt_termino_p is null)) and
	((ie_perm_env_proc_fim_conta_w = 'S') or (ie_fim_conta_w = 'N')) then
	return 'S';
else
	return 'N';
end if;

end obter_se_permite_realizar_proc;
/