create or replace
function obter_tipo_logradouro_VGS( nr_tipo_logradouro_p		number)
 		    	return Varchar is

/*
LOGRADOURO	VGS	   TASY
Acesso		AC	   001
Aeroporto	AER	   501
Alameda		AL	   004
Apartamento	AP		   
Avenida		AV	   008
Beco		BC	   011
Bloco		BL	   012
Caminho		CAM	   017
Escadinha	ESCAD	   030
Esta��o		EST	   032
Estrada		ETR	   031
Fazenda		FAZ	   037
Fortaleza	FORT		   
Galeria		GL	   045
Quilometro	KM		   
Ladeira		LD	   053
Largo		LGO	   054
Pra�a		P�A	   065
Praia		PR	   070
Parque		PRQ		   
Quadra		QD	   077
Quinta		QTA	   079
Rua		R	   081
Rodovia		ROD	   090
Super Quadra	SQD	   
Travessa	TRV	   100
Viaduto		VD	   103
Vila		VL	   104
*/
ds_logradouro_vgs_w	varchar(10);	
			
begin

if (nr_tipo_logradouro_p is not null) then
	begin
	if (nr_tipo_logradouro_p = 1) then
		ds_logradouro_vgs_w := 'AC';
	elsif (nr_tipo_logradouro_p = 501) then
		ds_logradouro_vgs_w := 'AER';
	elsif (nr_tipo_logradouro_p = 4) then
		ds_logradouro_vgs_w := 'AL';
	elsif (nr_tipo_logradouro_p = 8) then
		ds_logradouro_vgs_w := 'AV';
	elsif (nr_tipo_logradouro_p = 11) then
		ds_logradouro_vgs_w := 'BC';
	elsif (nr_tipo_logradouro_p = 12) then
		ds_logradouro_vgs_w := 'BL';
	elsif (nr_tipo_logradouro_p = 17) then
		ds_logradouro_vgs_w := 'CAM';
	elsif (nr_tipo_logradouro_p = 30) then
		ds_logradouro_vgs_w := 'ESCAD';
	elsif (nr_tipo_logradouro_p = 32) then
		ds_logradouro_vgs_w := 'EST';
	elsif (nr_tipo_logradouro_p = 31) then
		ds_logradouro_vgs_w := 'ETR';
	elsif (nr_tipo_logradouro_p = 37) then
		ds_logradouro_vgs_w := 'FAZ';
	elsif (nr_tipo_logradouro_p = 45) then
		ds_logradouro_vgs_w := 'GL';
	elsif (nr_tipo_logradouro_p = 53) then
		ds_logradouro_vgs_w := 'LD';
	elsif (nr_tipo_logradouro_p = 54) then
		ds_logradouro_vgs_w := 'LGO';
	elsif (nr_tipo_logradouro_p = 65) then
		ds_logradouro_vgs_w := 'P�A';
	elsif (nr_tipo_logradouro_p = 70) then
		ds_logradouro_vgs_w := 'PR';
	elsif (nr_tipo_logradouro_p = 77) then
		ds_logradouro_vgs_w := 'QD';
	elsif (nr_tipo_logradouro_p = 79) then
		ds_logradouro_vgs_w := 'QTA';
	elsif (nr_tipo_logradouro_p = 81) then
		ds_logradouro_vgs_w := 'R';
	elsif (nr_tipo_logradouro_p = 90) then
		ds_logradouro_vgs_w := 'ROD';
	elsif (nr_tipo_logradouro_p = 100) then
		ds_logradouro_vgs_w := 'TRV';
	elsif (nr_tipo_logradouro_p = 103) then
		ds_logradouro_vgs_w := 'VD';
	elsif (nr_tipo_logradouro_p = 104) then
		ds_logradouro_vgs_w := 'VL';
	else	ds_logradouro_vgs_w := '';
	end if;

end;
end if;
	
return	ds_logradouro_vgs_w;

end obter_tipo_logradouro_VGS;
/