create or replace
function verifica_estab_pac_age(nr_seq_ageint_p number) return varchar2 is

  ie_bloq_estab_diferente_w     varchar2(1);
  retorno_w                     varchar2(2);
  cd_estabelecimento_agenda_w	  number(10);
  cd_estabelecimento_paciente_w	number(10);
  cd_agenda_w				            number(10,0);  
begin
  retorno_w := 'OK';
  
  select max(cd_agenda)
    into cd_agenda_w
    from ageint_marcacao_usuario
   where nr_seq_ageint = nr_seq_ageint_p;
  
  select max(cd_estabelecimento), max(nvl(ie_bloq_estab_diferente,'N'))
    into cd_estabelecimento_agenda_w, ie_bloq_estab_diferente_w
    from agenda
   where cd_agenda = cd_agenda_w; 
   
  select max(pessoa_fisica.cd_estabelecimento) 
    into cd_estabelecimento_paciente_w 
    from pessoa_fisica
   inner join agenda_integrada on agenda_integrada.cd_pessoa_fisica = pessoa_fisica.cd_pessoa_fisica and agenda_integrada.nr_sequencia = nr_seq_ageint_p;
   

  if ((cd_estabelecimento_agenda_w is not null) and (cd_estabelecimento_paciente_w is not null) and
      (cd_estabelecimento_agenda_w <> cd_estabelecimento_paciente_w)) then
      retorno_w := ie_bloq_estab_diferente_w;
  end if;    

  return retorno_w;
end verifica_estab_pac_age;
/