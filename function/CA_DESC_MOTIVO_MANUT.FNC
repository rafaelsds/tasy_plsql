create or replace
function ca_desc_motivo_manut(nr_seq_motivo_p		number)
 		    	return varchar2 is
ds_retorno_w	varchar2(80);
begin

if	(nr_seq_motivo_p is not null) then
	select	ds_motivo
	into	ds_retorno_w
	from	ca_motivo_manut_bomba
	where	nr_sequencia = nr_seq_motivo_p;
end if;
return	ds_retorno_w;

end ca_desc_motivo_manut;
/