create or replace
function obter_num_parcialidad(nr_titulo_p		number)
 		    	return number is
qt_num_parc_w     number(5);

begin

select  count(*)
into    qt_num_parc_w
from    titulo_receber_liq a,
        titulo_receber b
where   a.vl_recebido > 0
and     a.nr_titulo = b.nr_titulo
and     b.nr_titulo = nr_titulo_p
and     not exists  (select *
		    from   titulo_receber_liq c,
			   titulo_receber d
                    where  c.nr_titulo = d.nr_titulo
		    and    a.nr_sequencia = c.nr_seq_liq_origem
		    and    d.nr_titulo =  nr_titulo_p);

	if (qt_num_parc_w = 0) then
	    qt_num_parc_w := 1;
	end if;

return	qt_num_parc_w;

end obter_num_parcialidad;
/



