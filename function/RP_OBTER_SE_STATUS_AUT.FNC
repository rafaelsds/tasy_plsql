create or replace
function Rp_Obter_Se_Status_Aut(nr_seq_status_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1)	:= 'S';
qt_regra_w	number(10,0);			
			
begin

select	count(*)
into	qt_regra_w
from	rp_status_geracao_agenda;

if	(qt_regra_w > 0) then

	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	rp_status_geracao_agenda
	where	nr_seq_status	= nr_seq_status_p;

end if;

return	ds_retorno_w;

end Rp_Obter_Se_Status_Aut;
/