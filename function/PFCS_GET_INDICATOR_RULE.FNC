create or replace function pfcs_get_indicator_rule(
        nr_seq_indicator_p      number,
        vl_indicator_p          number,
        cd_establishment_p      number,
        ie_type_p               varchar2,
        ds_column_p		        varchar2 default null
) return varchar2 is

/*
    nr_seq_indicator_p: indicator to be analyzed by any rule type (ie_type_p)
    vl_indicator_p: value to be analyzed to apply the color rule
    ie_type_p:
        -- COLOR: will return the color based on the color rule for the specified indicator
        -- ARROW: will return the arrow direction based on the rule defined by the user
        -- VISIBLE: will return the indicator visibility for the left card (WChart) items (except on module/function main screen left card)
        -- VISIBLE_MOD: will return the indicator visibility for the module/function left card items
        -- ORDER: will return the order of the items being showed on left card (WChart)
        -- SPACE: will return the spacing rule for some specific screens (if there is a space or not between two indicators)
        -- SHOW_POPOVER: will return if the TRS popover must be displayed or not
    ds_column_p: used to inform which columm the color will be applied to (used on WTableChart. Example: thirdcell)
*/

nr_seq_rule_w       pfcs_indicator_rule.nr_sequencia%type;
nr_seq_apresent_w   pfcs_indicator_rule.nr_seq_apresent%type;
ie_visible_w        pfcs_indicator_rule.ie_visible%type;
ie_arrow_w          pfcs_indicator_rule.ie_arrow%type;
ie_blank_space_w    pfcs_indicator_rule.ie_blank_space%type;
qt_initial_target_w pfcs_indicator_target.qt_initial_target%type;
qt_final_target_w   pfcs_indicator_target.qt_final_target%type;
ie_time_w           pfcs_indicator.ie_time%type;
ds_cor_html_w       varchar2(50);
ds_return_w       varchar2(255);
qt_rule_w           number(5);
vl_indicator_w      number(10);
hr_indicator_w      date;

begin
    select MAX(nr_sequencia),
    nvl(MAX(nr_seq_apresent),0),
    nvl(MAX(ie_visible),'S'),
    nvl(MAX(ie_arrow),2),
    nvl(MAX(ie_blank_space),'N'),
    nvl(MAX(ie_time),'N')
    into nr_seq_rule_w,
        nr_seq_apresent_w,
        ie_visible_w,
        ie_arrow_w,
        ie_blank_space_w,
        ie_time_w
    from (select
        a.nr_sequencia,
        a.nr_seq_apresent nr_seq_apresent,
        a.ie_visible ie_visible,
        a.ie_arrow ie_arrow,
        a.ie_blank_space ie_blank_space,
        b.ie_time
    from pfcs_indicator_rule a,
        pfcs_indicator b
    where a.nr_seq_indicator = nr_seq_indicator_p
        and a.nr_seq_indicator = b.nr_sequencia
        and  nvl(a.cd_estabelecimento, cd_establishment_p) = nvl(cd_establishment_p, a.cd_estabelecimento)
        and (ie_type_p <> 'COLOR' or nvl(b.ie_target, 'S') <> 'N')
        order by a.cd_estabelecimento asc) tab
    where ROWNUM = 1;

    if (ie_type_p = 'COLOR') then
        if  (ie_time_w = 'N') then
            select  max(b.ds_cor_html)
            into    ds_cor_html_w
            from    pfcs_indicator_target b
            where   b.nr_seq_rule = nr_seq_rule_w
            and     vl_indicator_p between nvl(b.qt_initial_target,vl_indicator_p) and nvl(b.qt_final_target,vl_indicator_p);
        else
            select  max(b.ds_cor_html)
            into    ds_cor_html_w
            from    pfcs_indicator_target b
            where   b.nr_seq_rule = nr_seq_rule_w
            and     vl_indicator_p between nvl(to_char(b.hr_initial_target, 'hh24Mi'),vl_indicator_p||'') and nvl(to_char(b.hr_final_target, 'hh24Mi'),vl_indicator_p||'');
        end if;

        if (ds_cor_html_w is null) then
            select  count(1)
            into    qt_rule_w
            from    pfcs_indicator_target b
            where   b.nr_seq_rule = nr_seq_rule_w;

            if  (qt_rule_w = 0) then
                select  max(qt_initial_target),
                        max(qt_final_target)
                into    qt_initial_target_w,
                        qt_final_target_w
                from    pfcs_indicator_target_def
                where   nr_seq_indicator    = nr_seq_indicator_p;

                if (qt_initial_target_w < qt_final_target_w) then
                    if (vl_indicator_p between qt_initial_target_w and qt_final_target_w) then
                        ds_cor_html_w := '#FFCD05';-- YELLOW
                    elsif (vl_indicator_p > qt_final_target_w) then
                        ds_cor_html_w := '#FA6557'; -- RED
                    end if;
                else -- REVERSE
                    if (vl_indicator_p < qt_final_target_w) then
                        ds_cor_html_w := '#FA6557'; -- RED
                    elsif (vl_indicator_p between qt_final_target_w and qt_initial_target_w) then
                        ds_cor_html_w := '#FFCD05';-- YELLOW
                    end if;
                end if;
            end if;
        end if;

        if ( (ds_cor_html_w is not null) and (ds_column_p is not null) ) then
            ds_cor_html_w := trim((ds_column_p || ':' || ds_cor_html_w));
        end if;
		
		ds_return_w:= ds_cor_html_w;

    elsif (ie_type_p = 'ARROW') then
		ds_return_w:= ie_arrow_w;

    elsif (ie_type_p = 'VISIBLE') then
		ds_return_w:= ie_visible_w;

    elsif (ie_type_p = 'VISIBLE_MOD') then
        select nvl(max(ie_left_card_main), 'N')
        into ie_visible_w
        from pfcs_indicator
            where nr_sequencia = nr_seq_indicator_p;

		ds_return_w:= ie_visible_w;

    elsif (ie_type_p = 'ORDER') then
		ds_return_w:= nvl(nr_seq_apresent_w,0);

    elsif (ie_type_p = 'SPACE') then
		ds_return_w:= ie_blank_space_w;

    elsif (ie_type_p = 'SHOW_POPOVER') then
		select  max(QT_INITIAL_TARGET)
		into    qt_initial_target_w
		from    pfcs_indicator_target
		where   nr_seq_rule = nr_seq_rule_w;

		if (qt_initial_target_w is null) then
			select  max(QT_INITIAL_TARGET)
			into    qt_initial_target_w
			from    pfcs_indicator_target_def
			where   nr_seq_indicator = nr_seq_indicator_p;
		end if;

		ds_return_w:= 'N';

        if (vl_indicator_p >= nvl(qt_initial_target_w, vl_indicator_p)) then
		ds_return_w:= 'S';
        end if;

    elsif (ie_type_p = 'INITIAL') then
    
        if (cd_establishment_p is not null) then
			select  max(QT_INITIAL_TARGET)
			into    qt_initial_target_w
			from    pfcs_indicator_target
			where   nr_seq_rule = nr_seq_rule_w;
		end if;
		
        if (qt_initial_target_w is null) then
            select  max(QT_INITIAL_TARGET)
            into    qt_initial_target_w
            from    pfcs_indicator_target_def
            where   nr_seq_indicator = nr_seq_indicator_p;
        end if;
 
        ds_return_w:= qt_initial_target_w;
 
    elsif (ie_type_p = 'FINAL') then
 
		if (cd_establishment_p is not null) then
			select  max(qt_final_target)
			into    qt_final_target_w
			from    pfcs_indicator_target
			where   nr_seq_rule = nr_seq_rule_w;
		end if;
		
        if (qt_final_target_w is null) then
            select  max(qt_final_target)
            into    qt_final_target_w
            from    pfcs_indicator_target_def
            where   nr_seq_indicator = nr_seq_indicator_p;
        end if;
        ds_return_w:= qt_final_target_w;

    end if;

    return ds_return_w;
end pfcs_get_indicator_rule;
/
