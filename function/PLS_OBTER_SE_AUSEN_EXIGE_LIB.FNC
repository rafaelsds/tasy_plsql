create or replace
function pls_obter_se_ausen_exige_lib
			(	nr_seq_motivo_p		Number)
				return Varchar2 is
			
ds_retorno_w			Varchar2(1)	:= 'N';
nr_seq_classif_ausencia_w	Number(10);

begin

select	nvl(nr_seq_classif_ausencia,0)
into	nr_seq_classif_ausencia_w
from	pls_atend_motivo_ausencia
where	nr_sequencia	= nr_seq_motivo_p;	

if	(nr_seq_classif_ausencia_w > 0) then
	select	ie_exige_liberacao
	into	ds_retorno_w
	from	pls_classif_ausencia_atend
	where	nr_sequencia	= nr_seq_classif_ausencia_w
	and	ie_situacao	= 'A';
end if;

return	ds_retorno_w;

end pls_obter_se_ausen_exige_lib;
/