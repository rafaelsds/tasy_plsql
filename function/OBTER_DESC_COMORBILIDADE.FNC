create or replace 
function obter_desc_comorbilidade (nr_atendimento_p	number)
			         return varchar2 is

ds_doenca_w	varchar2(4000) 	:= '';

Cursor C01 is
	select 	cd_doenca,
			substr(obter_desc_cid(cd_doenca),1,240) ds_doenca_cid
	from 	diagnostico_doenca
	where 	nr_atendimento 	= nr_atendimento_p
	order by nr_seq_interno;

begin
for r_C01_w in C01 loop
	begin
		if (r_C01_w.cd_doenca in ('O00','O01','O02','O03','O04','O05','O06','O07','O08','O80','O81','O82','O83','O84')) then
			if (ds_doenca_w is null) then
				ds_doenca_w := trim(TRAILING ' ' FROM r_C01_w.ds_doenca_cid);
			else
				ds_doenca_w := ds_doenca_w || '&' || trim(TRAILING ' ' FROM r_C01_w.ds_doenca_cid);
			end if;
		end if;
end;
end loop;

return	ds_doenca_w;

end obter_desc_comorbilidade;
/