CREATE OR REPLACE
FUNCTION Obter_desc_etapa_checkup(
			nr_sequencia_p	Number,
			ie_opcao_p	varchar2 default 'A')
			RETURN VARCHAR2 IS

ds_retorno_w		Varchar2(100);

BEGIN

if	(nr_sequencia_p is not null) then
	
	if	(ie_opcao_p	= 'A') then
	
		select	nvl(ds_etapa_abrev,ds_etapa)
		into	ds_retorno_w
		from	etapa_checkup
		where	nr_sequencia	= nr_sequencia_p;
		
	elsif	(ie_opcao_p	= 'C') then
		select	ds_etapa
		into	ds_retorno_w
		from	etapa_checkup
		where	nr_sequencia	= nr_sequencia_p;
	end if;
end if;

RETURN ds_retorno_w;

END Obter_desc_etapa_checkup;
/
