create or replace
function obter_texto_evolucao(cd_evolucao_p number)
 		    	return varchar2 is

ds_evolucao_w	varchar2(2000);
			
begin
	select	ds_evolucao
	into	ds_evolucao_w
	from	evolucao_paciente
	where	cd_evolucao	= cd_evolucao_p;
	
return ds_evolucao_w;

end obter_texto_evolucao;
/