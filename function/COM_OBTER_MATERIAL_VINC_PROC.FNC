create or replace
function com_obter_material_vinc_proc(cd_procedimento_p		procedimento.cd_procedimento%type,
				      ie_origem_proced_p	procedimento.ie_origem_proced%type)
 		    	return number is
			
cd_material_w		material.cd_material%type;			

begin

if (nvl(cd_procedimento_p,0) <> 0) and
   (nvl(ie_origem_proced_p,0) <> 0) then
	begin
	select	max(cd_material)
	into	cd_material_w
	from 	com_vinc_proced_material
	where	cd_procedimento = cd_procedimento_p
	and	ie_origem_proced = ie_origem_proced_p;
	end;
end if;

return	cd_material_w;

end com_obter_material_vinc_proc;
/