create or replace
function obter_dados_ficha_financ_res(	nr_seq_valor_p	number,
					ie_restricao_p	varchar2,
					ie_opcao_p	varchar2)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);

begin

if	(ie_opcao_p is null) then
	select	max(a.ds_restricao)
	into	ds_retorno_w
	from	capa_ficha_financ_campos_res_v a,
		ficha_financ_valor b
	where	a.nm_tabela	= b.nm_tabela
	and	b.nr_sequencia	= nr_seq_valor_p
	and	ie_restricao	= ie_restricao_p;
else
	select	max(a.ds_opcao)
	into	ds_retorno_w
	from	capa_ficha_financ_campos_res_v a,
		ficha_financ_valor b
	where	a.nm_tabela	= b.nm_tabela
	and	b.nr_sequencia	= nr_seq_valor_p
	and	ie_restricao	= ie_restricao_p
	and	ie_opcao	= ie_opcao_p;
end if;

return	ds_retorno_w;

end obter_dados_ficha_financ_res;
/
