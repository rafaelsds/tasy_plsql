create or replace
function obter_tempo_atend_senhas(nr_seq_pac_senha_fila_p  	number)
 		    	return varchar2 is
			
qt_segundo_w    	number(10,0);
qt_min_w   		number(10,0);
qt_hora_w		number(10,0);
qt_dia_w		number(10);
dt_diferenca_w		varchar2(100);		
			
begin
qt_segundo_w    := 0;
qt_min_w   	:= 0;
qt_hora_w	:= 0;
	
		begin
			select 	sum(nvl(dt_fim_atendimento,sysdate) - dt_inicio_atendimento) * 86400
			into 	qt_segundo_w
			from	atendimentos_senha
			where	nr_seq_pac_senha_fila = nr_seq_pac_senha_fila_p;
		exception
		when others then
			qt_segundo_w := 9999999999;
		end;
	
	while	(qt_segundo_w > 60) loop
		qt_min_w := qt_min_w + 1;
		qt_segundo_w := qt_segundo_w - 60;
	end loop;
	
	while	(qt_min_w > 60) loop
		qt_hora_w := qt_hora_w + 1;
		qt_min_w := qt_min_w - 60;
	end loop;
	
	if (qt_hora_w < 100) then
		dt_diferenca_w := lpad(qt_hora_w, 2, '0') || ':' || lpad(qt_min_w, 2, '0')|| ':' || lpad(nvl(qt_segundo_w,'00'), 2, '0');
		
	elsif (qt_hora_w < 1000) then
		dt_diferenca_w := lpad(qt_hora_w, 3, '0') || ':' || lpad(qt_min_w, 2, '0')|| ':' || lpad(nvl(qt_segundo_w,'00'), 2, '0');
		
	elsif (qt_hora_w < 10000) then
		dt_diferenca_w := lpad(qt_hora_w, 4, '0') || ':' || lpad(qt_min_w, 2, '0')|| ':' || lpad(nvl(qt_segundo_w,'00'), 2, '0');
		
	else
		dt_diferenca_w := lpad(qt_hora_w, 5, '0') || ':' || lpad(qt_min_w, 2, '0')|| ':' || lpad(nvl(qt_segundo_w,'00'), 2, '0');
	end if;
	

	return dt_diferenca_w;
	
end obter_tempo_atend_senhas;
/