create or replace
function Obter_desc_rotina_proced(	nr_prescricao_p	number,
					nr_sequencia_p	number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);

begin

select	ds_rotina
into	ds_retorno_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end Obter_desc_rotina_proced;
/