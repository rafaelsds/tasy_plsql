create or replace 
function ctb_obter_se_elemento_contido(	ds_elemento_p		varchar2,
					ds_conjunto_p		varchar2)
					return varchar2 is

ds_conjunto_ww			varchar2(2000);
ie_contido_w			varchar2(0001);

begin

ds_conjunto_ww			:= ' ' || ds_conjunto_p || ' ';
ds_conjunto_ww			:= replace(ds_conjunto_ww, ',', ' , ');
ds_conjunto_ww			:= replace(ds_conjunto_ww, '(', ' ');
ds_conjunto_ww			:= replace(ds_conjunto_ww, ')', ' ');
ds_conjunto_ww			:= replace(ds_conjunto_ww, '  ', ' ');

select	nvl(max('S') , 'N')
into	ie_contido_w
from	dual
where	ds_conjunto_ww like '% ' || ds_elemento_p || ' %';

return	ie_contido_w;

end ctb_obter_se_elemento_contido;
/