create or replace
function TISS_OBTER_DATA_PROC_GUIA
			(nr_seq_guia_p	in number)
 		    	return date is
dt_retorno_w	date;

begin

select	nvl(min(dt_procedimento),sysdate)
into	dt_retorno_w
from	tiss_conta_proc
where	nr_seq_guia	= nr_seq_guia_p;

return	dt_retorno_w;

end TISS_OBTER_DATA_PROC_GUIA;
/
