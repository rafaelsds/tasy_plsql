create or replace
function qua_obter_se_doc_obrigatorio(
			nr_sequencia_p	number,
			nm_usuario_p	varchar2)
			return varchar2 is

ds_resultado_w		varchar2(1);
qt_reg_w			number(15,0);
cd_cargo_w		number(10);
cd_estabelecimento_w	number(04,0);
cd_setor_atendimento_w	number(05,0);
cd_perfil_w		number(05,0) := obter_perfil_ativo;
ie_libera_estab_w		Varchar2(1);
cd_classif_setor_w		varchar2(02);
nr_seq_superior_w		varchar2(10);
ie_doc_pai_w		varchar2(1);
nr_seq_doc_w		number(10);
ds_program_w		varchar2(60);

begin

begin
    $IF DBMS_DB_VERSION.VERSION <= 11 $THEN
      select max(program)
      into ds_program_w
      from gv$session where audsid = userenv('sessionid');
    $ELSE
      SELECT SYS_CONTEXT ('USERENV', 'CLIENT_PROGRAM_NAME')
      into ds_program_w
      FROM DUAL;
    $END
end;

if	(instr(ds_program_w,'.EXE') > 0) then
	return 'S';
end if;

	ds_resultado_w	:= 'N';

	select	decode(count(*), 0, 'N', 'S')
	into	ie_doc_pai_w
	from	qua_documento a
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_seq_superior is null;
	
	if	(ie_doc_pai_w = 'S') then
		nr_seq_doc_w := nr_sequencia_p;
	else
		select	a.nr_seq_superior
		into	nr_seq_doc_w
		from	qua_documento a
		where	a.nr_sequencia = nr_sequencia_p;
	end if;
	
	select	max(a.cd_setor_atendimento),
		max(b.cd_classif_setor)
	into	cd_setor_atendimento_w,
		cd_classif_setor_w
	from	setor_atendimento b,
		usuario a
	where	a.nm_usuario 		= nm_usuario_p
	and	a.cd_setor_atendimento 	= b.cd_setor_atendimento;

	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	qua_documento
	where	nr_sequencia = nr_seq_doc_w;

	select	count(*)
	into	qt_reg_w
	from	qua_doc_lib
	where	nr_seq_doc	= nr_seq_doc_w;

	select	b.cd_cargo
	into	cd_cargo_w
	from	usuario a,
		pessoa_fisica b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nm_usuario 	= nm_usuario_p;

	if	cd_cargo_w is null then
		cd_cargo_w	:= 0;
	end if;

	/*Se tiver regra para usuario ou cargo do usuario, esta liberado*/
	select	count(*)
	into	qt_reg_w
	from	qua_doc_lib
	where	nr_seq_doc	= nr_seq_doc_w
	and	(nm_usuario_lib	= nm_usuario_p or cd_cargo = cd_cargo_w)
	and     (ie_obrigatorio = 'S')
	and     ( ((dt_inicio_vigencia is null) and (dt_fim_vigencia is null)) or
		((sysdate >= dt_inicio_vigencia) and (dt_fim_vigencia is null))or
		((sysdate <= dt_fim_vigencia) and (dt_inicio_vigencia is null))or
		((sysdate >= dt_inicio_vigencia) and (sysdate <= dt_fim_vigencia)));
	if	(qt_reg_w > 0) then
		ds_resultado_w	:= 'S';
	else
		/*Se tiver regra para algum perfil liberado ao usuario, esta liberado*/
		select count(*)
		into	qt_reg_w
		from	usuario_perfil b,
			qua_doc_lib a
		where	a.nr_seq_doc	= nr_seq_doc_w
		and	a.cd_perfil	= b.cd_perfil
		and	b.nm_usuario	= nm_usuario_p
		and 	a.ie_obrigatorio = 'S'
		and     ( ((a.dt_inicio_vigencia is null) and (a.dt_fim_vigencia is null)) or
			((sysdate >= a.dt_inicio_vigencia) and (a.dt_fim_vigencia is null))or
			((sysdate <= a.dt_fim_vigencia) and (a.dt_inicio_vigencia is null))or
			((sysdate >= a.dt_inicio_vigencia) and (sysdate <= a.dt_fim_vigencia)));
		if	(qt_reg_w > 0) then
			ds_resultado_w	:= 'S';
		end if;
	end if;

	/*Se tiver regra para algum setor liberado ao usuario, esta liberado*/
	if	(ds_resultado_w = 'N') then
		/* Compara o setor da regra com o setor do usuario logado (buscando pela package) */
		select	decode(count(*),0,'N','S')
		into	ds_resultado_w
		from	qua_doc_lib a
		where	a.nr_seq_doc	       = nr_seq_doc_w
		and	a.cd_setor_atendimento = cd_setor_atendimento_w
		and 	a.ie_obrigatorio = 'S'
		and     ( ((a.dt_inicio_vigencia is null) and (a.dt_fim_vigencia is null)) or
			((sysdate >= a.dt_inicio_vigencia) and (a.dt_fim_vigencia is null))or
			((sysdate <= a.dt_fim_vigencia) and (a.dt_inicio_vigencia is null))or
			((sysdate >= a.dt_inicio_vigencia) and (sysdate <= a.dt_fim_vigencia)));
	end if;

	/* Se tiver regra para algum agrupamento liberado ao usuario, esta liberado*/
	if	(ds_resultado_w = 'N') then
		select	count(*)
		into	qt_reg_w
		from	qua_doc_lib a,
			qua_grupo_cargo b,
			qua_cargo_agrup c
		where	a.nr_seq_grupo_cargo	= b.nr_sequencia
		and	b.nr_sequencia 		= c.nr_seq_agrup
		and	a.nr_seq_doc   		= nr_seq_doc_w
		and	c.cd_cargo     		= cd_cargo_w
		and 	a.ie_obrigatorio	= 'S'
		and     ( ((a.dt_inicio_vigencia is null) and (a.dt_fim_vigencia is null)) or
			((sysdate >= a.dt_inicio_vigencia) and (a.dt_fim_vigencia is null))or
			((sysdate <= a.dt_fim_vigencia) and (a.dt_inicio_vigencia is null))or
			((sysdate >= a.dt_inicio_vigencia) and (sysdate <= a.dt_fim_vigencia)));

		if	(qt_reg_w > 0) then
			ds_resultado_w := 'S';
		end if;
	end if;

	/* Se tiver regra para algum 'grupo de usuario' liberado ao usuario, esta liberado*/
	if	(ds_resultado_w = 'N') then
		select	count(*)
		into	qt_reg_w
		from	qua_doc_lib a,
			grupo_usuario b,
			usuario_grupo c
		where	a.nr_seq_grupo_usuario	= b.nr_sequencia
		and	b.nr_sequencia 		= c.nr_seq_grupo
		and	a.nr_seq_doc   		= nr_seq_doc_w
		and	((c.nm_usuario_grupo 	= nm_usuario_p) or (nm_usuario_exclusivo = nm_usuario_p))
		and 	a.ie_obrigatorio	= 'S'
		and     ( ((a.dt_inicio_vigencia is null) and (a.dt_fim_vigencia is null)) or
			((sysdate >= a.dt_inicio_vigencia) and (a.dt_fim_vigencia is null))or
			((sysdate <= a.dt_fim_vigencia) and (a.dt_inicio_vigencia is null))or
			((sysdate >= a.dt_inicio_vigencia) and (sysdate <= a.dt_fim_vigencia)));

		if	(qt_reg_w > 0) then
			ds_resultado_w := 'S';
		end if;
	end if;

	/*Se tiver regra para algum 'grupo perfil' liberado ao usuario, esta liberado*/
	if	(ds_resultado_w = 'N') then
		select	count(*)
		into	qt_reg_w
		from	qua_doc_lib a,
			grupo_perfil b,
			grupo_perfil_item c
		where	a.nr_seq_grupo_perfil	= b.nr_sequencia
		and	b.nr_sequencia 		= c.nr_seq_grupo_perfil
		and	a.nr_seq_doc   		= nr_seq_doc_w
		and	c.cd_perfil		= cd_perfil_w
		and 	a.ie_obrigatorio	= 'S'
		and     ( ((a.dt_inicio_vigencia is null) and (a.dt_fim_vigencia is null)) or
			((sysdate >= a.dt_inicio_vigencia) and (a.dt_fim_vigencia is null))or
			((sysdate <= a.dt_fim_vigencia) and (a.dt_inicio_vigencia is null))or
			((sysdate >= a.dt_inicio_vigencia) and (sysdate <= a.dt_fim_vigencia)));

		if	(qt_reg_w > 0) then
			ds_resultado_w := 'S';
		end if;
	end if;


	/*Se tiver regra para algum classif setor liberado ao usuario, esta liberado*/
	if	(ds_resultado_w = 'N') then
		/* Compara o setor da regra com o setor do usuario logado (buscando pela package) */
		select	decode(count(*),0,'N','S')
		into	ds_resultado_w
		from	setor_atendimento b,
			qua_doc_lib a
		where	a.nr_seq_doc	       = nr_seq_doc_w
		and	b.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_classif_setor = b.cd_classif_setor
		and 	ie_obrigatorio	= 'S'
		and     ( ((a.dt_inicio_vigencia is null) and (a.dt_fim_vigencia is null)) or
			((sysdate >= a.dt_inicio_vigencia) and (a.dt_fim_vigencia is null))or
			((sysdate <= a.dt_fim_vigencia) and (a.dt_inicio_vigencia is null))or
			((sysdate >= a.dt_inicio_vigencia) and (sysdate <= a.dt_fim_vigencia)));
	end if;

	if (ds_resultado_w = 'N') then
		select	count(*)
		into	qt_reg_w
		from	qua_doc_lib a,
				usuario b,
				pessoa_fisica c
		where	a.nr_seq_doc = nr_seq_doc_w
		and		a.nr_seq_funcao is not null
		and		b.nm_usuario = nm_usuario_p
		and		b.cd_pessoa_fisica = c.cd_pessoa_fisica
		and		a.nr_seq_funcao = c.nr_seq_funcao_pf
		and		a.ie_obrigatorio = 'S';

		if	(qt_reg_w > 0) then
			ds_resultado_w := 'S';
		end if;

	end if;

	if	(ds_resultado_w = 'N') then
		begin
		select	count(*)
		into	qt_reg_w
		from 	qua_doc_lib a
		where	a.nr_seq_doc = nr_seq_doc_w
		and	exists (	select 	1
					from	gerencia_wheb e,
						gerencia_wheb_grupo b,
						gerencia_wheb_grupo_usu c,
						usuario d
					where	e.nr_sequencia = b.nr_seq_gerencia
					and	b.nr_sequencia = c.nr_seq_grupo
					and	c.nm_usuario_grupo = d.nm_usuario
					and	b.nr_sequencia = a.nr_seq_grupo_gerencia
					and	e.ie_situacao = 'A'
					and	b.ie_situacao = 'A'
					and	nvl(c.ie_emprestimo, 'N') = 'N'
					and	trunc(sysdate) between trunc(c.dt_inicio) and trunc(nvl(c.dt_fim, sysdate))
					and	d.nm_usuario = nm_usuario_p)
		and	a.ie_obrigatorio = 'S'
		and     ( ((a.dt_inicio_vigencia is null) and (a.dt_fim_vigencia is null)) or
			((sysdate >= a.dt_inicio_vigencia) and (a.dt_fim_vigencia is null))or
			((sysdate <= a.dt_fim_vigencia) and (a.dt_inicio_vigencia is null))or
			((sysdate >= a.dt_inicio_vigencia) and (sysdate <= a.dt_fim_vigencia)));

		if	(qt_reg_w > 0)	then
			ds_resultado_w := 'S';
		end if;
		end;

	end if;

	RETURN ds_resultado_w;
end qua_obter_se_doc_obrigatorio;
/
