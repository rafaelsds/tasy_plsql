create or replace
function obter_total_resp_viagem	(	ie_responsavel_p		varchar2,
					nr_seq_viagem_p		number,
					dt_referencia_p		date) 
						
					return number is

vl_total_w 	number(15,2);

begin

select	sum(d.vl_despesa)
into	vl_total_w
from	via_viagem v,
	via_relat_desp r,
	via_despesa d
where	d.nr_seq_relat = r.nr_sequencia
and	r.nr_seq_viagem = v.nr_sequencia
and	v.nr_sequencia = decode(nr_seq_viagem_p,'0',v.nr_sequencia,nr_seq_viagem_p)
and	d.ie_responsavel_custo = ie_responsavel_p
and	trunc(d.dt_despesa,'mm') = trunc(dt_referencia_p,'mm');

return	nvl(vl_total_w,0);

end obter_total_resp_viagem;
/