create or replace
Function Obter_Se_DLP_Obrigatorio(  nr_seq_proc_p number,
      cd_tipo_procedimento_p number,
      cd_procedimentos_p varchar2)
return varchar is

ds_retorno_w       Varchar2(1);


Begin

select decode(count(1),1,'S','N')
into ds_retorno_w
from procedimento_paciente proc_pac,
procedimento proc
where proc_pac.nr_sequencia = nr_seq_proc_p
and proc_pac.cd_procedimento = proc.cd_procedimento
and proc_pac.ie_origem_proced = proc.ie_origem_proced
and ((proc.cd_tipo_procedimento = cd_tipo_procedimento_p) or 'S' = Obter_Se_Contido(proc_pac.cd_procedimento,cd_procedimentos_p));

return ds_retorno_w;

end Obter_Se_DLP_Obrigatorio;
/ 