CREATE OR REPLACE
FUNCTION Obter_Pessoa_Adiantamento(	
					nr_adiantamento_p		number)
					RETURN Varchar2 IS

ds_retorno_w			Varchar2(200) := Null;

BEGIN

begin
select	nm_pessoa_adiant || ' - ' ||
		to_char(dt_adiantamento, 'dd/mm/yyyy') || ' - ' ||
		campo_mascara(vl_adiantamento, 2)
into		ds_retorno_w
from		adiantamento_v
where		nr_adiantamento	= nr_adiantamento_p;
exception
    		when others then
		ds_retorno_w	:= Null;
end;

RETURN ds_retorno_w;

END Obter_Pessoa_Adiantamento;
/