create or replace
function nut_obter_desc_receita( nr_seq_receita_p	number)
 		    	return varchar2 is

ds_receita_w	varchar2(255);
begin

if (nr_seq_receita_p is not null) then
	select 	ds_receita
	into	ds_receita_w
	from	nut_receita
	where	nr_sequencia	= nr_seq_receita_p;
end if;


return	ds_receita_w;

end nut_obter_desc_receita;
/