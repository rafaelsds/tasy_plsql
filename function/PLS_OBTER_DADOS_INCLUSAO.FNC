create or replace
function pls_obter_dados_inclusao
			(	nr_seq_inclusao_p		number,
				ie_opcao_p			varchar2)
 		    	return varchar2 is
			
nr_seq_segurado_w	number(10);
nr_seq_proposta_w	number(10);
nm_pessoa_fisica_w	varchar2(255);
ds_tipo_operacao_w	varchar2(255);

ds_retorno_w		varchar2(255);

begin

if	(ie_opcao_p = 'S') then
	select	c.nr_sequencia
	into	nr_seq_segurado_w
	from	pls_segurado			c,
		pls_proposta_beneficiario	b,
		pls_inclusao_beneficiario	a
	where	a.nr_seq_proposta_benef		= b.nr_sequencia
	and	c.nr_seq_pessoa_proposta	= b.nr_sequencia
	and	a.nr_sequencia			= nr_seq_inclusao_p;
	
	ds_retorno_w	:= to_char(nr_seq_segurado_w);
elsif	(ie_opcao_p = 'N') then
	select	nm_pessoa_fisica
	into	nm_pessoa_fisica_w
	from	pls_inclusao_beneficiario
	where	nr_sequencia			= nr_seq_inclusao_p;
	
	ds_retorno_w	:= nm_pessoa_fisica_w;
elsif	(ie_opcao_p = 'O') then
	select	substr(obter_valor_dominio(4525,nvl(ie_tipo_operacao,'I')),1,255)
	into	ds_tipo_operacao_w
	from	pls_inclusao_beneficiario
	where	nr_sequencia			= nr_seq_inclusao_p;
	
	ds_retorno_w	:= ds_tipo_operacao_w;
elsif	(ie_opcao_p = 'P') then
	select	NR_SEQ_PROPOSTA
	into	nr_seq_proposta_w
	from	pls_inclusao_beneficiario
	where	nr_sequencia		= nr_seq_inclusao_p;
	
	ds_retorno_w	:= to_char(nr_seq_proposta_w);
end if;

return	ds_retorno_w;

end pls_obter_dados_inclusao;
/
