create or replace
function qt_obter_box_agendado(nr_seq_ageint_item_p		number,
			       nr_seq_pend_quimio_p		number,
			       dt_agenda_p			date)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);			
			
begin

if	(nr_seq_ageint_item_p > 0) then
	select	max(b.ds_local)
	into	ds_retorno_w
	from	agenda_quimio_marcacao a,
		qt_local b
	where	a.nr_seq_local = b.nr_sequencia
	and	a.nr_seq_ageint_item = nr_seq_ageint_item_p;
elsif	(nr_seq_pend_quimio_p > 0) then
	select	max(b.ds_local)
	into	ds_retorno_w
	from	agenda_quimio_marcacao a,
		qt_local b
	where	a.nr_seq_local = b.nr_sequencia
	and	a.nr_seq_pend_agenda	= nr_seq_pend_quimio_p
	and	trunc(a.dt_agenda)	= trunc(dt_agenda_p);
end if;
	
return	ds_retorno_w;

end qt_obter_box_agendado;
/