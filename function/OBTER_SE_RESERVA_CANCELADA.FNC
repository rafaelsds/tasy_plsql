create or replace
function obter_se_reserva_cancelada (nr_seq_transporte_p	Number)
					return varchar2 is

ie_retorno_w		varchar2(1):= 'N';
qt_reg_dependentes_w	number(10,0);
nr_seq_reserva_w	number(10,0);

begin

qt_reg_dependentes_w:= 0;

select	count(*)
into	qt_reg_dependentes_w
from 	via_reserva_transporte	a,
	via_transporte		b,
	via_reserva		c
where 	a.nr_seq_transporte = b.nr_sequencia
and 	a.nr_seq_reserva    = c.nr_sequencia
and 	a.nr_seq_transporte = nr_seq_transporte_p
and 	c.ie_cancelamento is not null;


if	(qt_reg_dependentes_w > 0) then
	ie_retorno_w:= 'S';
else
	ie_retorno_w:= 'N';
end if;

return ie_retorno_w;

end obter_se_reserva_cancelada;
/