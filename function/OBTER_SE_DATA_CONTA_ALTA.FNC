create or replace
FUNCTION obter_se_data_conta_alta(	nr_atendimento_p		NUMBER,
				ie_opcao_p		VARCHAR2 DEFAULT NULL)
 		    		RETURN DATE IS

dt_retorno_w	DATE;
dt_intermed_w	DATE;
qt_contas_w	NUMBER(5);
dt_alta_w		DATE;
dt_tesouraria_w	DATE;

/* Function criada para atender altera��es realizadas na pasta Tempo de Status da Ocupa��o Hospitalar para o HMSL */

BEGIN

	SELECT MAX(dt_conta_definitiva)
	INTO dt_intermed_w
	FROM conta_paciente
	WHERE nr_atendimento = nr_atendimento_p;

	IF 	(dt_intermed_w IS NOT NULL) THEN
		dt_retorno_w := dt_intermed_w; /*houve conta particular fechada*/
	ELSE
		SELECT MAX(dt_alta)
		INTO dt_alta_w
		FROM atendimento_paciente
		WHERE nr_atendimento = nr_atendimento_p;

		dt_retorno_w := dt_alta_w;
	END IF;

	/* Op��o R criada para atender o relat�rio status de leito do S�o Luiz*/

	IF 	(ie_opcao_p = 'R') THEN
		SELECT COUNT(*)
		INTO qt_contas_w
		FROM conta_paciente
		WHERE nr_atendimento = nr_atendimento_p;

		IF (qt_contas_w = 0) THEN
			dt_retorno_w := dt_alta_w;
		END IF;
		IF (dt_intermed_w IS NULL) THEN
			SELECT dt_alta_tesouraria
			INTO dt_tesouraria_w
			FROM atendimento_paciente
			WHERE nr_atendimento = nr_atendimento_p;

			IF (dt_tesouraria_w IS NOT NULL) THEN
				dt_retorno_w := dt_tesouraria_w;
			END IF;
		END IF;
	elsif	(ie_opcao_p = 'AT') THEN
		SELECT dt_alta_tesouraria
		INTO DT_TESOURARIA_W
		FROM ATENDIMENTO_PACIENTE
		WHERE NR_ATENDIMENTO = NR_ATENDIMENTO_P;

		IF	(DT_TESOURARIA_W IS NOT NULL) THEN
			DT_RETORNO_W := DT_TESOURARIA_W;
		END IF;
	END IF;

RETURN	dt_retorno_w;

END obter_se_data_conta_alta;
/

