create or replace
function ish_get_event_hcm(
		nr_seq_patient_p		number,
		nr_seq_fall_p		number)
		return varchar2 is

nr_seq_segmento_w	hcm_patienten.nr_seq_segmento%type;
nr_seq_registro_w		hcm_segmento.nr_seq_registro%type;
sapevent_w		hcm_kopf.sapevent%type;

begin
if	(nr_seq_patient_p is not null) or (nr_seq_fall_p is not null) then
	begin
	if	(nr_seq_patient_p is not null) then
		select	nr_seq_segmento
		into	nr_seq_segmento_w
		from	hcm_patienten
		where	nr_sequencia = nr_seq_patient_p;
	elsif	(nr_seq_fall_p is not null) then
		select	nr_seq_segmento
		into	nr_seq_segmento_w
		from	hcm_fall
		where	nr_sequencia = nr_seq_fall_p;
	end if;

	select	nr_seq_registro
	into	nr_seq_registro_w
	from	hcm_segmento
	where	nr_sequencia = nr_seq_segmento_w;
		
	select	nr_sequencia
	into	nr_seq_segmento_w
	from	hcm_segmento
	where	nr_seq_registro = nr_seq_registro_w
	and	ie_tipo = 'HEA' 
	and	rownum = 1;

	select	sapevent
	into	sapevent_w
	from	hcm_kopf
	where	nr_seq_segmento = nr_seq_segmento_w;
	exception
	when others then
		sapevent_w	:=	null;
	end;
end if;

return	sapevent_w;

end ish_get_event_hcm;
/