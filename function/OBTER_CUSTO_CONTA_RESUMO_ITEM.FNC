create or replace
function Obter_custo_conta_resumo_item (nr_interno_conta_p	Number,
					cd_item_p		number,
					ie_proc_mat_p		varchar2,
					nm_usuario_p	varchar2)
					return Number is

vl_custo_w	Number(15,2);
qt_resumo_w	Number(15,4);
ds_retorno_w	Number(15,2);

begin

if	(ie_proc_mat_p = '1') then
	select	nvl(sum(vl_custo),0), 
		nvl(sum(qt_resumo),1)
	into	vl_custo_w,
		qt_resumo_w
	from	conta_paciente_resumo
	where	nr_interno_conta	= nr_interno_conta_p
	and 	cd_procedimento		= cd_item_p;
elsif	(ie_proc_mat_p = '2') then
	select	nvl(sum(vl_custo),0),
		nvl(sum(qt_resumo),1)
	into	vl_custo_w,
		qt_resumo_w
	from	conta_paciente_resumo
	where	nr_interno_conta	= nr_interno_conta_p
	and 	cd_material		= cd_item_p;
end if;

ds_retorno_w := vl_custo_w;

return ds_retorno_w;

end Obter_custo_conta_resumo_item;
/