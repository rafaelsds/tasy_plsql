/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter e-mail vendedor envio an�lise encerrada
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_email_vendedor_analise
			(	nr_seq_analise_p	number)
 		    	return varchar2 is
			
ds_retorno_w			varchar2(255);

begin

if	(nvl(obter_valor_param_usuario(1236,22, Obter_Perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'P') = 'P') then
	select	substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'M'),1,255)
	into	ds_retorno_w
	from	pls_analise_adesao a,
		pls_proposta_adesao b,
		pls_vendedor c
	where	a.nr_seq_proposta	= b.nr_sequencia
	and	b.nr_seq_vendedor_canal	= c.nr_sequencia
	and	a.nr_sequencia		= nr_seq_analise_p;
else
	select	max(d.ds_email)
	into	ds_retorno_w
	from	pls_analise_adesao	a,
		pls_proposta_adesao	b,
		pls_vendedor 		c,
		usuario			d
	where	a.nr_seq_proposta	= b.nr_sequencia
	and	b.nr_seq_vendedor_canal	= c.nr_sequencia
	and	d.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and	a.nr_sequencia		= nr_seq_analise_p;
end if;

return	ds_retorno_w;

end pls_email_vendedor_analise;
/
