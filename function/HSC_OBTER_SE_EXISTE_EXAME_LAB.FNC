create or replace
function  hsc_obter_se_existe_exame_lab(nr_prescricao_p number,	
									cd_setor_atendimento_p number)
					return number is
					
nr_exames_w number := 0;					

begin
	select	count(*)
	into	nr_exames_w
	from	prescr_medica e,
			exame_lab_material d,
			material_exame_lab c,
			exame_laboratorio b,
			prescr_procedimento a
	where 	a.nr_seq_exame		= b.nr_seq_exame
	and 	a.nr_seq_exame		= d.nr_seq_exame
	and 	d.nr_seq_material		= c.nr_sequencia
	and 	a.nr_prescricao		= nr_prescricao_p
	and 	nvl(d.qt_coleta,1) >= to_number('1')
	and		e.nr_prescricao = a.nr_prescricao
	and		hsc_obter_se_exame_result(nr_prescricao_p, a.nr_sequencia) = 'N'
	and		nvl(e.cd_setor_atendimento, a.cd_setor_entrega) = cd_setor_atendimento_p;
	
return nr_exames_w;
end hsc_obter_se_existe_exame_lab;
/