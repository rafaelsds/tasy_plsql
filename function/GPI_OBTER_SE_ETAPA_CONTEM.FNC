create or replace
function gpi_obter_se_etapa_contem(	nr_sequencia_p	number,
				cd_pessoa_equipe_p	varchar2,
				nr_seq_equipe_p		number)
				return varchar2 is

cd_classif_etapa_w		varchar2(80);
ie_classif_w		number(10);
nr_seq_etapa_w		number(10);
nr_seq_cronograma_w	number(10);
nr_sequencia_w		number(10);
nr_seq_superior_w		number(10);
qt_registro_w		number(10);
ie_retorno_w		varchar2(1)	:= 'N';	



begin

nr_seq_etapa_w	:= nvl(nr_sequencia_p,0);

if	(nvl(cd_pessoa_equipe_p,'0') <> '0') then
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	gpi_cron_etapa_equipe
	where	nr_seq_etapa		= nr_sequencia_p
	and	cd_pessoa_fisica	= cd_pessoa_equipe_p;
	
end if;

if	(nvl(nr_seq_equipe_p,0) <> 0) then
	begin
	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	gpi_cron_etapa_equipe
	where	nr_seq_etapa		= nr_sequencia_p
	and	nr_seq_equipe		= nr_seq_equipe_p;
	end;
end if;

if	(ie_retorno_w = 'N') and
	(nr_seq_etapa_w <> 0) then
	begin

	select	nr_seq_cronograma
	into	nr_seq_cronograma_w
	from	gpi_cron_etapa
	where	nr_sequencia	= nr_seq_etapa_w;
	
	select	count(*)
	into	qt_registro_w
	from	gpi_cron_etapa
	where	nr_seq_superior = nr_seq_etapa_w;
	
	if	(qt_registro_w > 0) then
		
		cd_classif_etapa_w	:= substr(gpi_obter_classif_etapa(nr_seq_etapa_w),1,80);
		ie_classif_w		:= nvl(length(cd_classif_etapa_w),0);
		
		if	(nvl(cd_pessoa_equipe_p,'0') <> '0') then
			select	nvl(max('S'),'N')
			into	ie_retorno_w
			from	gpi_cron_etapa a,
				gpi_cron_etapa_equipe b
			where	a.nr_sequencia		= b.nr_seq_etapa
			and	substr(gpi_obter_classif_etapa(a.nr_sequencia),1,ie_classif_w)		= cd_classif_etapa_w
			and	a.nr_seq_cronograma	= nr_seq_cronograma_w
			and	b.cd_pessoa_fisica	= cd_pessoa_equipe_p;
		end if;

		if	(nvl(nr_seq_equipe_p,0) <> 0) then
			begin
			select	nvl(max('S'),'N')
			into	ie_retorno_w
			from	gpi_cron_etapa a,
				gpi_cron_etapa_equipe b
			where	a.nr_sequencia		= b.nr_seq_etapa
			and	substr(gpi_obter_classif_etapa(a.nr_sequencia),1,ie_classif_w)		= cd_classif_etapa_w
			and	a.nr_seq_cronograma	= nr_seq_cronograma_w
			and	nr_seq_equipe		= nr_seq_equipe_p;
			end;
		end if;
		
		if	(nvl(nr_seq_equipe_p,0) = 0)and
			(nvl(cd_pessoa_equipe_p,'0') = '0') then
			begin
			select	nvl(max('S'),'N')
			into	ie_retorno_w
			from	gpi_cron_etapa a
			where	substr(gpi_obter_classif_etapa(a.nr_sequencia),1,ie_classif_w)		= cd_classif_etapa_w
			and	a.nr_seq_cronograma	= nr_seq_cronograma_w;
			end;
		end if;
	end if;
	
	end;
end if;

return	ie_retorno_w;

end gpi_obter_se_etapa_contem;
/