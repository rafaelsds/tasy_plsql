create or replace 
function san_qt_doador_pendente ( nr_seq_reposicao_p  number )
        return number is

retorno_w		number;
qt_total_doador_w  	number;
qt_repo_w    		number;

begin

SELECT	nvl(SUM(NVL(qt_doacao, 1)),0)
INTO 	qt_total_doador_w
FROM 	san_reposicao_doador
WHERE 	nr_seq_reposicao = nr_seq_reposicao_p;

select 	qt_reposicao
into 	qt_repo_w
from  	san_reposicao
where 	nr_sequencia = nr_seq_reposicao_p ;

if	(qt_repo_w >= qt_total_doador_w ) then
	retorno_w := qt_repo_w - qt_total_doador_w;
else
	retorno_w := 0;
end if;

return retorno_w ;

end san_qt_doador_pendente ;
/
