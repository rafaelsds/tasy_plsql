create or replace
function HMSL_Obter_Tipo_Proced(nr_atendimento_p		varchar2)
 		    	return varchar2 is
ds_retorno_w		varchar2(255);
ie_tipo_atendimento_w 	number(3);
ie_clinica_w		number(5);

begin
if	(nr_atendimento_p is not null) then
	begin
	
	select	ie_tipo_atendimento
	into	ie_tipo_atendimento_w
	from atendimento_paciente
	where nr_atendimento = nr_atendimento_p;
	
	select	ie_clinica
	into	ie_clinica_w
	from atendimento_paciente
	where nr_atendimento = nr_atendimento_p;
	
	if	(ie_tipo_atendimento_w = 7) then
		ds_retorno_w := 'EXAMES';
		
	else if	(ie_tipo_atendimento_w = 3) then
		ds_retorno_w := 'CONSULTA PS';

	else if (ie_tipo_atendimento_w = 1 and ie_clinica_w = 1) then
		ds_retorno_w := 'INTERNACAO CL�NICA';
	
	else
		ds_retorno_w := substr(OBTER_CIRURGIA_PACIENTE(nr_atendimento_p,'AA'),1,255);
	end if;
	end if;
	end if;	
	end;
end if;
return	ds_retorno_w;
end HMSL_Obter_Tipo_Proced;
/