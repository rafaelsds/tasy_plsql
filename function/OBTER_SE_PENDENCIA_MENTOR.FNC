create or replace function Obter_se_pendencia_mentor( nr_atendimento_p         number,
                                    nr_seq_sinal_vital_p     number   default null,
                                    nm_usuario_integracao_p  varchar2 default null,
                                    cd_estab_integracao_p    number   default null,
									nr_seq_regra_p           number   default null,
                                    ie_exige_justificativa_p varchar2 default 'S|N') return varchar2 is

ie_retorno_w			varchar2(1) := 'N';
ie_medico_w             varchar2(1) := 'N';
qt_horas_retroativa_w	number(15,3);
cd_estabelecimento_w	number(4);
nm_usuario_w		    varchar2(15);
qt_reg_w		        number(10);
qt_pendencia_w		    number(10);
cd_pessoa_usuario_w		number(10);

begin

cd_estabelecimento_w := nvl(cd_estab_integracao_p,wheb_usuario_pck.get_cd_estabelecimento);
nm_usuario_w := nvl(nm_usuario_integracao_p,wheb_usuario_pck.get_nm_usuario);

obter_param_usuario(355,2,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,qt_horas_retroativa_w);

select obter_pf_usuario(nm_usuario_w, 'C')
into cd_pessoa_usuario_w
from dual;

select OBTER_SE_PF_MEDICO(cd_pessoa_usuario_w) 
into ie_medico_w
from dual;

qt_horas_retroativa_w := nvl( ((1/24)* qt_horas_retroativa_w),0);

Select  count(*)
into	qt_reg_w
from	gqa_pendencia_pac
where	nr_atendimento = nr_atendimento_p
  and   ((qt_horas_retroativa_w = 0) or (dt_atualizacao_nrec >= sysdate - qt_horas_retroativa_w));

if (qt_reg_w > 0) then

	select  count(*)
	into	qt_pendencia_w
	from    gqa_pendencia_pac      a,
		    gqa_pend_pac_acao      b,
		    gqa_pend_pac_acao_dest c,
		    gqa_pendencia_regra    d
	where	a.nr_sequencia  = b.nr_seq_pend_pac
	and	b.nr_sequencia      = c.nr_seq_pend_pac_acao
	and	d.nr_sequencia      = a.nr_seq_pend_regra
	and	a.nr_atendimento    = nr_atendimento_p
    and c.dt_encerramento   is null
    and c.dt_justificativa  is null
	and	b.dt_encerramento   is null
and	(   (c.cd_pessoa_fisica = cd_pessoa_usuario_w and c.IE_PROFISSIONAL_MEDICO <> 'S') 
    or    ( ie_medico_w = 'S' and c.IE_PROFISSIONAL_MEDICO = 'S' and c.nr_seq_pend_pac_acao not in (select p.nr_seq_pend_pac_acao 
                                                                                          from GQA_PEND_PAC_ACAO_DEST p,
                                                                                          gqa_pendencia_pac m,
                                                                                          GQA_PEND_PAC_ACAO v
                                                                                          where   m.nr_sequencia = v.nr_seq_pend_pac
                                                                                          and     p.dt_ciencia is not null
                                                                                          and     v.nr_sequencia = p.nr_seq_pend_pac_acao
                                                                                          and     m.nr_atendimento = nr_atendimento_p
                                                                                          and     p.cd_pessoa_fisica = cd_pessoa_usuario_w))
    or (nr_seq_sinal_vital_p > 0))
	and	((qt_horas_retroativa_w = 0) or (a.dt_atualizacao_nrec >= sysdate - qt_horas_retroativa_w))
	and	nvl(d.nr_seq_sinal_vital,0) = nvl(nr_seq_sinal_vital_p, nvl(d.nr_seq_sinal_vital,0))
	and	nvl(d.nr_sequencia,0) = nvl(nr_seq_regra_p, nvl(d.nr_sequencia,0))
    and regexp_like(pep_obter_info_pend_mentor(c.nr_seq_pend_pac_acao, 'J'), ie_exige_justificativa_p)
	and	( ((b.nr_seq_proc is not null or b.nr_seq_proc_saps is not null)
		and not exists (select	1
				from	pe_prescricao e
				where	e.nr_seq_pend_pac_acao in (select nr_sequencia
								   from   gqa_pend_pac_acao
								   where  nr_seq_pend_pac = a.nr_sequencia)))
		or ((b.nr_seq_protocolo is not null)
		and not exists (select	1
				from	prescr_medica f
				where	f.nr_seq_pend_pac_acao = b.nr_sequencia))
		or ((b.nr_seq_mprev_programa is not null)
		and not exists (select	1
				from	atend_programa_saude g
				where	g.nr_seq_pend_pac_acao = b.nr_sequencia))
		or ((b.nr_seq_escala is not null)
		and not exists (select	1
				from	escala_pend_pac_acao e
				where	e.nr_seq_pend_pac_acao = b.nr_sequencia)));

	if  (qt_pendencia_w > 0) then

		ie_retorno_w := 'S';

	end if;

end if;


return ie_retorno_w;

end Obter_se_pendencia_mentor;
/
