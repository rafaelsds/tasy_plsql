create or replace
function adep_obter_se_assin_perfil(nr_seq_projeto_p		tasy_projeto_assinatura.nr_sequencia%type,
									cd_perfil_p				perfil.cd_perfil%type)
 		    	return varchar2 is

ie_liberado_w		varchar2(1 char);
				
begin

ie_liberado_w	:= 'N';

if	(nr_seq_projeto_p is not null) then
	select	nvl(max('S'),'N')
	into	ie_liberado_w
	from	tasy_proj_assin_perfil a,
			tasy_projeto_assinatura b
	where 	a.nr_seq_proj  	= b.nr_sequencia
	and 	b.nr_sequencia  = nr_seq_projeto_p
	and		nvl(a.cd_perfil, cd_perfil_p) = cd_perfil_p;
end if;

return	ie_liberado_w;

end adep_obter_se_assin_perfil;
/
