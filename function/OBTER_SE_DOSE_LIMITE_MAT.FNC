create or replace 
function obter_se_dose_limite_mat(	nr_seq_elem_mat_p	number,
									cd_material_p		number,
									qt_dose_p			number) return varchar2 is

qt_max_w	number(18,6);
qt_min_w	number(18,6);
ds_retorno_w	varchar2(255);
begin
if (nr_seq_elem_mat_p is not null) then
	select 	nvl(max(qt_max),99999999999),
			nvl(max(qt_min),0)
	into		qt_max_w,
			qt_min_w
	from		nut_elem_material
	where 	nr_sequencia = nr_seq_elem_mat_p;
	if	((nvl(qt_dose_p,0) > qt_max_w) or 
		 (nvl(qt_dose_p,0) < qt_min_w)) then
		ds_retorno_w := obter_desc_expressao(347742)||chr(13)|| --O valor informado est� fora da faixa padr�o deste produto.
						obter_desc_expressao(781805, 'QT_MIN_W' || qt_min_w || ';QT_MAX_W=' || qt_max_w);--'Faixa padr�o: '||qt_min_w||' - '||qt_max_w||'.';  
	end if;
elsif	(cd_material_p is not null) then
	select 	nvl(max(qt_max),99999999999),
			nvl(max(qt_min),0)
	into		qt_max_w,
			qt_min_w
	from		nut_elem_material
	where 	cd_material = cd_material_p;
	if	((nvl(qt_dose_p,0) > qt_max_w) or 
		 (nvl(qt_dose_p,0) < qt_min_w)) then
		ds_retorno_w := obter_desc_expressao(347742)||chr(13)|| --O valor informado est� fora da faixa padr�o deste produto.
						obter_desc_expressao(781805, 'QT_MIN_W' || qt_min_w || ';QT_MAX_W=' || qt_max_w);--'Faixa padr�o: '||qt_min_w||' - '||qt_max_w||'.'; 
	end if;
end if;

return ds_retorno_w;

end obter_se_dose_limite_mat;
/
