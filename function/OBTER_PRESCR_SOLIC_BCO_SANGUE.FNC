create or replace
function obter_prescr_solic_bco_sangue	(nr_prescricao_p	number,
					ie_opcao_p		varchar2,
					nr_sequencia_p		number)
			return varchar2 is
/*
Atualmente apenas nr_prescricao_p � utilizado, posteriormente, poder� ser util o uso da sequencia da PRESCR_SOLIC_BCO_SANGUE em outras ocasi��es

IE_OPCAO_P

HOA	-> QT_HEMOGLOBINA
HTC	-> QT_HEMATOCRITO
PLQ	-> QT_PLAQUETA
DIAG	-> DS_DIAGNOSTICO
r	- RESERVADO
*/

ds_retorno_w	varchar2(300) := null;

begin
if	(nr_prescricao_p is not null) then

	if	(ie_opcao_p = 'HOA') then		
		select	max(qt_hemoglobina)
		into	ds_retorno_w
		from	prescr_solic_bco_sangue
		where	nr_prescricao	= nr_prescricao_p;
		
	elsif	(ie_opcao_p = 'HTC') then
		select	max(qt_hematocrito)
		into	ds_retorno_w
		from	prescr_solic_bco_sangue
		where	nr_prescricao = nr_prescricao_p;
		
	elsif	(ie_opcao_p = 'R') then
		select	max(ie_reserva)
		into	ds_retorno_w
		from	prescr_solic_bco_sangue
		where	nr_prescricao	= nr_prescricao_p;
		
	elsif	(ie_opcao_p = 'PLQ') then
		select	max(qt_plaqueta)
		into	ds_retorno_w
		from	prescr_solic_bco_sangue
		where	nr_prescricao	= nr_prescricao_p;
		
	elsif	(ie_opcao_p = 'DIAG') then
		select 	max(ds_diagnostico)
		into	ds_retorno_w
		from 	prescr_solic_bco_sangue
		where 	nr_prescricao = nr_prescricao_p;
		
	elsif	(ie_opcao_p = 'TIPOA') then
		select	max(substr(obter_valor_dominio(1223,ie_tipo),1,255))
		into	ds_retorno_w
		from	prescr_solic_bco_sangue
		where	nr_prescricao	= nr_prescricao_p;
	end if;
	
end if;

return	ds_retorno_w;

end obter_prescr_solic_bco_sangue;
/