create or replace function obter_profissional_funcao(	nr_seq_agenda_p	number,
														ie_type_p       varchar2)
														return 			varchar2 is

/*
  M  - M�dico                               - IE_MEDICO
  A  - Anestesista                          - IE_ANESTESISTA
  MC - M�dico e Cirurgi�o                   - IE_CIRURGIAO + IE_MEDICO
  AP - Anestesista e Anestesista Principal  - IE_ANESTESISTA_PRINCIPAL + IE_ANESTESISTA
  E  - Enfermeiro                           - IE_ENFERMEIRA
  O  - Outros                               - IE_AUXILIAR + IE_INSTRUMENTADOR + IE_OBSTETRA + IE_PEDIATRA              
*/

ds_return_w			varchar2(255) := '';
ds_names_w			varchar2(255);
ds_cirurgiao_w    	varchar2(255) := null;
ds_anestesista_w	varchar2(255) := null;

cursor c01 is
	select  obter_nome_pessoa_fisica(pa.cd_profissional, null) ds_names
	from    profissional_agenda pa,
		funcao_medico fm
	where   pa.nr_seq_agenda = nr_seq_agenda_p
	and     pa.cd_funcao = fm.cd_funcao
	and     nvl(fm.ie_medico, 'N')= 'S';	
  
cursor c02 is
	select  obter_nome_pessoa_fisica(pa.cd_profissional, null) ds_names
	from    profissional_agenda pa,
		funcao_medico fm
	where   pa.nr_seq_agenda = nr_seq_agenda_p
	and     pa.cd_funcao = fm.cd_funcao
	and     nvl(fm.ie_anestesista,'N') = 'S';
  
cursor c03 is
	select  obter_nome_pessoa_fisica(pa.cd_profissional, null) ds_names
	from    profissional_agenda pa,
		funcao_medico fm
	where   pa.nr_seq_agenda = nr_seq_agenda_p
	and     pa.cd_funcao = fm.cd_funcao
  and     (nvl(fm.ie_medico, 'N') = 'S' or nvl(fm.ie_cirurgiao, 'N') = 'S');
  
cursor c04 is
	select  obter_nome_pessoa_fisica(pa.cd_profissional, null) ds_names
	from    profissional_agenda pa,
		funcao_medico fm
	where   pa.nr_seq_agenda = nr_seq_agenda_p
	and     pa.cd_funcao = fm.cd_funcao
  and     (nvl(fm.ie_anestesista, 'N') = 'S' or nvl(fm.ie_anestesista_principal, 'N') = 'S');
  
cursor c05 is
	select  obter_nome_pessoa_fisica(pa.cd_profissional, null) ds_names
	from    profissional_agenda pa,
		funcao_medico fm
	where   pa.nr_seq_agenda = nr_seq_agenda_p
	and     pa.cd_funcao = fm.cd_funcao
	and     nvl(ie_enfermeira, 'N') = 'S';
  
cursor c06 is
	select  obter_nome_pessoa_fisica(pa.cd_profissional, null) ds_names
	from    profissional_agenda pa,
			funcao_medico fm
	where   pa.nr_seq_agenda = nr_seq_agenda_p
	and     pa.cd_funcao = fm.cd_funcao
	and     (nvl(fm.IE_CIRURGIAO, 'N')                 = 'N' 
            and nvl(fm.IE_MEDICO, 'N')                = 'N' 
            and nvl(fm.IE_ANESTESISTA_PRINCIPAL, 'N') = 'N' 
            and nvl(fm.IE_ANESTESISTA, 'N')           = 'N'   
            and nvl(fm.IE_ENFERMEIRA, 'N')            = 'N');
begin

if(ie_type_p = 'M')then
	open c01;
	loop
	fetch c01 into
		ds_names_w;
		exit when c01%notfound;
		begin
			ds_return_w := ds_return_w||ds_names_w||', ';	
		end;
	end loop;
	close c01;
elsif(ie_type_p = 'A')then
	open c02;
	loop
	fetch c02 into
		ds_names_w;
		exit when c02%notfound;
		begin
			ds_return_w := ds_return_w||ds_names_w||', ';	
		end;
	end loop;
	close c02;
  
elsif(ie_type_p = 'MC')then
	open c03;
	loop
	fetch c03 into
		ds_names_w;
		exit when c03%notfound;
		begin
			ds_return_w := ds_return_w||ds_names_w||', ';	
		end;
	end loop;
	close c03;
	
	select 	obter_nome_pessoa_fisica(cd_medico, null)
	into 	ds_cirurgiao_w
	from 	agenda_paciente
	where 	nr_sequencia = nr_seq_agenda_p;

	if((ds_cirurgiao_w is not null 
		and    instr(upper(ds_return_w), upper(ds_cirurgiao_w)) = 0)
		or ds_return_w is null)  then
		ds_return_w := ds_cirurgiao_w || ', ' || ds_return_w;
	end if;
  
elsif(ie_type_p = 'AP')then
	open c04;
	loop
	fetch c04 into
		ds_names_w;
		exit when c04%notfound;
		begin
			ds_return_w := ds_return_w||ds_names_w||', ';	
		end;
	end loop;
	close c04;
	
	select 	obter_nome_pessoa_fisica(cd_anestesista, null)
	into 	ds_anestesista_w
	from 	agenda_paciente
	where 	nr_sequencia = nr_seq_agenda_p;

	if(ds_anestesista_w is not null) then
		ds_return_w := ds_anestesista_w || ', ' || ds_return_w;
	end if;
  
elsif(ie_type_p = 'E')then
	open c05;
	loop
	fetch c05 into
		ds_names_w;
		exit when c05%notfound;
		begin
			ds_return_w := ds_return_w||ds_names_w||', ';	
		end;
	end loop;
	close c05;
  
elsif(ie_type_p = 'O')then
	open c06;
	loop
	fetch c06 into
		ds_names_w;
		exit when c06%notfound;
		begin
			ds_return_w := ds_return_w||ds_names_w||', ';	
		end;
	end loop;
	close c06;
end if;

if (length(ds_return_w) > 0) then
	ds_return_w := substr(ds_return_w,1,length(ds_return_w) - 2);
end if;

return	ds_return_w;

end obter_profissional_funcao;
/
