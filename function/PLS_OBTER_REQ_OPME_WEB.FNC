create or replace
function pls_obter_req_opme_web
			(	nr_seq_item_p		Number,
				nr_seq_requisicao_p	Number,	
				ie_origem_proced_p	Number,
				ie_tipo_p			Varchar2)
				return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter regra para exigir o prestador informar informa��es de um determinado material/medicamento
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				
	IE_TIPO_P:
	P - Procedimento
	M - Material
*/

ds_retorno_w			Varchar2(1) := 'N';
ie_origem_w			Number(10);
cd_area_w			Number(10);
cd_especialidade_w		Number(10);
cd_grupo_w			Number(10);
nr_seq_contrato_w			Number(10);
ie_tipo_guia_w			Varchar2(2);

Cursor C01 is
	select  ds_retorno
	from	(	select  'S' ds_retorno,
				cd_procedimento,
				cd_grupo_proc,
				cd_especialidade,
				cd_area_procedimento,
				nr_seq_material
			from    pls_regra_inform_mat
			where   nvl(nr_seq_material,nvl(nr_seq_item_p,0))		= nvl(nr_seq_item_p,0)
			and     nvl(nr_seq_contrato, nvl(nr_seq_contrato_w,0))       	= nvl(nr_seq_contrato_w,0)
			and	nvl(ie_tipo_guia, nvl(ie_tipo_guia_w,0))		= nvl(ie_tipo_guia_w,0)
			and	nvl(cd_procedimento,nvl(nr_seq_item_p,0))        	= nvl(nr_seq_item_p,0)
			and     nvl(cd_grupo_proc,nvl(cd_grupo_w,0))                 	= nvl(cd_grupo_w,0)
			and     nvl(cd_especialidade, nvl(cd_especialidade_w,0))     	= nvl(cd_especialidade_w,0)
			and	nvl(ie_origem_proced, nvl(ie_origem_w,0))     		= nvl(ie_origem_w,0)
			and     nvl(cd_area_procedimento, nvl(cd_area_w,0))          	= nvl(cd_area_w,0)
			and     nvl(nr_seq_contrato, nvl(nr_seq_contrato_w,0))       	= nvl(nr_seq_contrato_w,0)
			and	nvl(ie_tipo_guia, nvl(ie_tipo_guia_w,0))		= nvl(ie_tipo_guia_w,0)
			and	sysdate between dt_inicio_vigencia and nvl(dt_fim_vigencia, sysdate)
			and	(cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento and (pls_obter_se_controle_estab('RE') = 'S'))
			union all
			select  'S' ds_retorno,
				cd_procedimento,
				cd_grupo_proc,
				cd_especialidade,
				cd_area_procedimento,
				nr_seq_material
			from    pls_regra_inform_mat
			where   nvl(nr_seq_material,nvl(nr_seq_item_p,0))		= nvl(nr_seq_item_p,0)
			and     nvl(nr_seq_contrato, nvl(nr_seq_contrato_w,0))       	= nvl(nr_seq_contrato_w,0)
			and	nvl(ie_tipo_guia, nvl(ie_tipo_guia_w,0))		= nvl(ie_tipo_guia_w,0)
			and	nvl(cd_procedimento,nvl(nr_seq_item_p,0))        	= nvl(nr_seq_item_p,0)
			and     nvl(cd_grupo_proc,nvl(cd_grupo_w,0))                 	= nvl(cd_grupo_w,0)
			and     nvl(cd_especialidade, nvl(cd_especialidade_w,0))     	= nvl(cd_especialidade_w,0)
			and	nvl(ie_origem_proced, nvl(ie_origem_w,0))     		= nvl(ie_origem_w,0)
			and     nvl(cd_area_procedimento, nvl(cd_area_w,0))          	= nvl(cd_area_w,0)
			and     nvl(nr_seq_contrato, nvl(nr_seq_contrato_w,0))       	= nvl(nr_seq_contrato_w,0)
			and	nvl(ie_tipo_guia, nvl(ie_tipo_guia_w,0))		= nvl(ie_tipo_guia_w,0)
			and	sysdate between dt_inicio_vigencia and nvl(dt_fim_vigencia, sysdate)
			and 	(pls_obter_se_controle_estab('RE') = 'N'))
	order by		
		nvl(cd_procedimento,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0),
		nvl(nr_seq_material,0);

	
begin

select	pls_obter_dados_segurado(nr_seq_segurado, 'NC'),
	ie_tipo_guia
into	nr_seq_contrato_w,
	ie_tipo_guia_w
from 	pls_requisicao
where 	nr_sequencia = nr_seq_requisicao_p;

if	(ie_tipo_p = 'P') then	
	pls_obter_estrut_proc(nr_seq_item_p, ie_origem_proced_p, cd_area_w,
			      cd_especialidade_w, cd_grupo_w, ie_origem_w);
end if;


open C01;
loop
fetch C01 into	
	ds_retorno_w;
exit when C01%notfound;
	begin
		ds_retorno_w := 'S';
	end;
end loop;
close C01;


return	ds_retorno_w;

end pls_obter_req_opme_web;
/
