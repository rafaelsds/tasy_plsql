create or replace
function obter_agrupamentos_agenda(	cd_agenda_p				number,
									nr_seq_agrupamento_p	number)
									return varchar2 is

qt_agrupamento_w	number(10);
ie_perm_agenda_w	varchar2(1);
									
begin

if	(cd_agenda_p is not null) and
	(nr_seq_agrupamento_p is not null)then
	select	count(*)
	into	qt_agrupamento_w
	from	agenda_agrupamento a
	where	a.cd_agenda 			= cd_agenda_p
	and		a.nr_seq_agrupamento 	= nr_seq_agrupamento_p;

	if	(qt_agrupamento_w = 0) then
		ie_perm_agenda_w := 'N';
	else
		ie_perm_agenda_w := 'S';
	end if;
	

end if;


return	ie_perm_agenda_w;

end obter_agrupamentos_agenda;
/