create or replace
function obter_se_req_bloqueio(	nr_requisicao_p	number,	
				nm_usuario_p	varchar2)
 		    	return varchar2 is

ds_retorno_w		varchar2(1) := 'N';
		
begin

select  nvl(max('S'),'N')    
into	ds_retorno_w       
from    requisicao_bloqueio        
where   nm_usuario <> nm_usuario_p 
and     nr_requisicao = nr_requisicao_p
and    ((sysdate - dt_atualizacao) * 1440) < 1; 


return	ds_retorno_w;

end obter_se_req_bloqueio;
/

