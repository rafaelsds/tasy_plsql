create or replace
function obter_se_atrib_constraint(nm_tabela_p varchar2,nm_atributo_p varchar2,ie_tipo_p varchar2) return varchar2 as
qt_resultado_w     number(5);
ds_comando_w       varchar2(1000);
ds_parametros_w    varchar2(255);
begin
	ds_parametros_w :=      'NM_TABELA='|| nm_tabela_p ||';' ||
				'NM_ATRIBUTO='|| nm_atributo_p || ';';
	if      ( ie_tipo_p = 'FK' ) then
		ds_comando_w := 'select count(*) ' ||
                		'from   integridade_atributo ' ||
				'where  nm_tabela = :NM_TABELA ' ||
				'and    nm_atributo = :NM_ATRIBUTO ';
	else
        	ds_comando_w := 'select count(*) ' ||
                		'from   indice_atributo b, ' ||
				'        indice a ' ||
				'where  b.nm_tabela    = :NM_TABELA ' ||
				'and    b.nm_atributo  = :NM_ATRIBUTO ' ||
				'and    a.nm_tabela    = b.nm_tabela ' ||
				'and    a.nm_indice    = b.nm_indice ' ||
				'and    a.ie_tipo      = :IE_TIPO ';
		ds_parametros_w := ds_parametros_w || 'IE_TIPO='||ie_tipo_p || ';';
	end if;

	obter_valor_dinamico_bv(ds_comando_w,ds_parametros_w,qt_resultado_w);

	if      (qt_resultado_w > 0 ) then
		return 'S';
	else
		return 'N';
	end if;
end obter_se_atrib_constraint;
/