create or replace 
function obter_setor_reserva_cpoe ( nr_prescricao_p		number,
                                                        nr_atendimento_p	number)

return number is

cd_setor_reserva_w		number(5);

begin
select nvl(cd_setor_recebimento,cd_setor_tranfusao)
into cd_setor_reserva_w 
from cpoe_hemoterapia h,
    prescr_solic_bco_sangue b
where b.nr_prescricao = nr_prescricao_p
and h.nr_sequencia = b.nr_seq_hemo_cpoe;

return nvl(cd_setor_reserva_w, substr(obter_unidade_atendimento(nr_atendimento_p, 'IAA', 'CS'),1,50));
end;
/
