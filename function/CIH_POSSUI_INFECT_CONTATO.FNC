create or replace
function cih_possui_infect_contato( nr_ficha_ocorrencia_p		number,
					nr_atendimento_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(1);			
begin
ds_retorno_w := 'N';
if	(nr_ficha_ocorrencia_p is not null) then

	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	cih_contato
	where	((nr_ficha_ocorrencia = nr_ficha_ocorrencia_p)
		or	((nr_atendimento = nr_atendimento_p) and (nr_ficha_ocorrencia is null)))
	and	ie_complicacao_infect = 'S';
end if;
return	ds_retorno_w;

end cih_possui_infect_contato;
/