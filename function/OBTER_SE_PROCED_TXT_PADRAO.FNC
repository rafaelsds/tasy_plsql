create or replace
function obter_se_proced_txt_padrao	(nr_seq_texto_padrao_p number,
									cd_procedimento_p	number,
									ie_origem_proced_p number)
					return varchar2 is

ds_retorno_w	varchar2(1);

begin
if	(cd_procedimento_p is not null) then
	select	nvl(max('S'),'N')
	into	ds_retorno_w
	from	texto_padrao a,
		procedimento_texto_padrao b,
		procedimento c
	where	a.nr_sequencia = b.nr_seq_texto_padrao
	and	b.nr_seq_texto_padrao = nr_seq_texto_padrao_p
	and	(((b.cd_procedimento = c.cd_procedimento) and (b.ie_origem_proced = c.ie_origem_proced))
	or	 (b.cd_tipo_procedimento = c.cd_tipo_procedimento))	
	and	c.cd_procedimento = cd_procedimento_p
	and	c.ie_origem_proced = ie_origem_proced_p;
	
end if;

return ds_retorno_w;

end obter_se_proced_txt_padrao;
/
