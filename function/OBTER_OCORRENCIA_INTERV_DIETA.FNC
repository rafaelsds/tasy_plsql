create or replace
function Obter_ocorrencia_interv_dieta(	cd_intervalo_p	varchar2,
					qt_hora_p	Number,
					ie_opcao_p	Varchar2)
					return number is

/*
ie_opcao_p	
	O	Ocorr�ncias
	H	Horas de dura��o de cada intervalo
*/

nr_ocorrencia_w		Number(15,4);
qt_hora_w		Number(15,4);
qt_operacao_w		intervalo_prescricao.qt_operacao%type;
ie_operacao_w		Varchar2(1);

begin

if	(cd_intervalo_p is not null) then

	select	nvl(max(qt_operacao),1),
		nvl(max(ie_operacao),'F')
	into	qt_operacao_w,
		ie_operacao_w
	from	intervalo_prescricao
	where	cd_intervalo	= cd_intervalo_p;

	if	(ie_operacao_w = 'X') then
		nr_ocorrencia_w	:= dividir(24,qt_operacao_w); --Ceil(qt_hora_p / (24 / qt_operacao_w));
		qt_hora_w	:= dividir(24,qt_operacao_w);

	elsif	(ie_operacao_w = 'H') then
		qt_hora_w	:= qt_operacao_w;
		nr_ocorrencia_w	:= qt_operacao_w;

	elsif	(ie_operacao_w in ('F','V')) then
		nr_ocorrencia_w	:= qt_operacao_w;
		qt_hora_w	:= dividir(24,qt_operacao_w);
	else
		nr_ocorrencia_w	:= qt_operacao_w;
		qt_hora_w	:= dividir(24,qt_operacao_w);
	end if;

end if;

if	(ie_opcao_p = 'O') then
	return nr_ocorrencia_w;
else
	return qt_hora_w;
end if;

end Obter_ocorrencia_interv_dieta;
/
