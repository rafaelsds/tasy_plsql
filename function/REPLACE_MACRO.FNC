create or replace
function replace_macro(
			ds_text_p		varchar2,
			ds_macro_p		varchar2,
			ds_value_p		varchar2)
 		    	return varchar2 is

ie_macro_feature_enabled	varchar2(1) := 'S';
ds_text_w varchar2(32000);
macro_row_w search_macros_row := search_macros_row(null,null,null,null,null);

cursor cursor_macros is
select 	*
from	table(search_macros(null, ds_macro_p, null, 'R'));

begin
/*
	Ao alterar esta function favor verificar tamb�m as functions replace_macro_long e replace_macro_clob
*/

ds_text_w := ds_text_p;

if (ie_macro_feature_enabled = 'S') then
	begin
	open cursor_macros;
		loop
		fetch cursor_macros into
			macro_row_w.ds_macro_philips,
			macro_row_w.nr_seq_macro_philips,
			macro_row_w.ds_macro_client,
			macro_row_w.nr_seq_macro_client,
			macro_row_w.ds_locale;
		exit when cursor_macros%notfound;
			begin
			ds_text_w := substr(replace(ds_text_w, macro_row_w.ds_macro_client, ds_value_p),1,32000);
			end;
		end loop;
	close cursor_macros;	
	end;
else
	begin
	ds_text_w := substr(replace(ds_text_w, ds_macro_p, ds_value_p),1,32000);
	end;
end if;

return	ds_text_w;

end replace_macro;
/
