create or replace
function pls_tipo_hist_grupo_contrato
			(	nr_seq_tipo_hist_p	number)
				return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	max(ds_tipo_historico)
into	ds_retorno_w
from	pls_tipo_hist_grup_contrat
where	nr_sequencia	= nr_seq_tipo_hist_p;

return	ds_retorno_w;

end pls_tipo_hist_grupo_contrato;
/