create or replace
function	far_obter_veiculo_entregador	(nr_seq_entregador_p	number,
				cd_estabelecimento_p	number) return number is

ds_retorno_w		number(10);

begin

select	max(nvl(a.nr_sequencia,null))
into	ds_retorno_w
from	eme_veiculo a
where	a.cd_estabelecimento = cd_estabelecimento_p
and	exists (select 	1
	       from     	man_veiculo_entregador x
	       where    	x.nr_seq_veiculo = a.nr_sequencia
	       and      	x.nr_seq_entregador = nr_seq_entregador_p
	       and      	x.dt_inicio <= trunc(sysdate)
	       and      	nvl(x.dt_final,sysdate) >= trunc(sysdate));

return ds_retorno_w;

end far_obter_veiculo_entregador;
/