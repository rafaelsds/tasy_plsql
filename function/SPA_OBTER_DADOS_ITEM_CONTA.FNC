create or replace
function spa_obter_dados_item_conta(	nr_seq_item_p	Number,
				ie_tipo_item_p	Varchar2,
				ie_opcao_p	Varchar2)
 		    	return Varchar2 is
			
ds_retorno_w		varchar2(255);
cd_item_w		number(10,0);
ds_item_w		varchar2(255);
dt_conta_item_w		date;
qt_item_w		number(9,3);
vl_item_w		number(15,2);
vl_unit_item_w		number(15,4);

/*
IE_TIPO_ITEM_P:
P - Procedimentos
M - Materiais
*/

/*
IE_OPCAO_P:
C - C�digo
D - Descri��o
DC - Data conta
Q - Quantidade
VU - Valor unit�rio
V - Valor
QM - Quantidade sem m�scara
VM - Valor sem m�scara
*/

begin

if	(ie_tipo_item_p = 'P') then
	select	cd_procedimento,
		substr(obter_desc_procedimento(cd_procedimento, ie_origem_proced),1,255),
		dt_conta,
		qt_procedimento,
		dividir(vl_procedimento, qt_procedimento),
		vl_procedimento
	into	cd_item_w,
		ds_item_w,
		dt_conta_item_w,
		qt_item_w,
		vl_unit_item_w,
		vl_item_w
	from	procedimento_paciente
	where	nr_sequencia = nr_seq_item_p;
else
	select	cd_material,
		substr(obter_desc_material(cd_material),1,255),
		dt_conta,
		qt_material,
		vl_unitario,
		vl_material
	into	cd_item_w,
		ds_item_w,
		dt_conta_item_w,
		qt_item_w,
		vl_unit_item_w,
		vl_item_w
	from	material_atend_paciente
	where	nr_sequencia = nr_seq_item_p;
end if;

if	(ie_opcao_p = 'C') then
	ds_retorno_w	:= cd_item_w;
elsif	(ie_opcao_p = 'D') then
	ds_retorno_w	:= ds_item_w;
elsif	(ie_opcao_p = 'DC') then
	ds_retorno_w	:= to_char(dt_conta_item_w, 'dd/mm/yyyy hh24:mi:ss');
elsif	(ie_opcao_p = 'Q') then
	ds_retorno_w	:= campo_mascara_virgula(qt_item_w);
elsif	(ie_opcao_p = 'VU') then
	ds_retorno_w	:= campo_mascara_virgula(vl_unit_item_w);
elsif	(ie_opcao_p = 'V') then
	ds_retorno_w	:= campo_mascara_virgula(vl_item_w);
elsif	(ie_opcao_p = 'VM') then
	ds_retorno_w	:= vl_item_w;
elsif	(ie_opcao_p = 'QM') then
	ds_retorno_w	:= qt_item_w;
end if;

return	ds_retorno_w;

end spa_obter_dados_item_conta;
/
