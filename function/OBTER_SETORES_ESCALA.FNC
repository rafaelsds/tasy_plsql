create or replace
function obter_setores_escala(nr_seq_escala_p		number)
 		    	return varchar2 is

ds_setor_w	varchar2(20)	:= '';
ds_retorno_w	varchar2(4000);

cursor	c01 is
	select 	substr(obter_dados_setor(cd_setor_atendimento,'DO'),1,20)
	from 	escala_setor
	where	nr_seq_escala	= nr_seq_escala_p;
			
begin

open	c01;
loop
fetch	c01 into ds_setor_w;
	exit when c01%notfound;
	begin

	ds_retorno_w	:= ds_retorno_w || ds_setor_w || '/';

	end;
end loop;
close c01;

ds_retorno_w	:= substr(ds_retorno_w, 1, length(ds_retorno_w) - 1);

return	ds_retorno_w;

end obter_setores_escala;
/