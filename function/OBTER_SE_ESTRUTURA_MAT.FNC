create or replace
function	OBTER_SE_ESTRUTURA_MAT(	cd_grupo_material_p	number,
					cd_subgrupo_material_p	number,
					cd_classe_material_p	number,
					cd_material_estrut_p	number,
					cd_material_p		number,
					ie_acao_p		varchar2)
							return	varchar2 is

cont_w		number(15);

begin


if	((cd_grupo_material_p	is not null) or
	(cd_subgrupo_material_p	is not null) or
	(cd_classe_material_p	is not null) or
	(cd_material_estrut_p	is not null)) and 
	(ie_acao_p = 'S') then

	select	count(*)
	into	cont_w
	from	estrutura_material_v
	where	cd_grupo_material		= nvl(cd_grupo_material_p, cd_grupo_material)
	and	cd_subgrupo_material	= nvl(cd_subgrupo_material_p, cd_subgrupo_material)
	and	cd_classe_material		= nvl(cd_classe_material_p, cd_classe_material)
	and	cd_material		= nvl(cd_material_estrut_p, cd_material)
	and	cd_material		= cd_material_p;

	if	(cont_w > 0) then
		return	'S';
	else	
		return	'N';
	end if;

else
	
	return	'S';
end if;

end	OBTER_SE_ESTRUTURA_MAT;
/