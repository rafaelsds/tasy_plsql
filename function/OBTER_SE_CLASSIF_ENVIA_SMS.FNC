create or replace
function Obter_Se_Classif_Envia_SMS(cd_estabelecimento_p    Number,
        ie_classificacao_p  Varchar2)
           return Varchar2 is

qt_regra_envio_w  Number(5);
ie_tipo_regra_w	regra_classif_envio_sms.ie_tipo_regra%type;
ds_retorno_w    Varchar2(1) := 'S';

begin

select  count(*)
into  qt_regra_envio_w
from  regra_classif_envio_sms
where ie_situacao = 'A'
and cd_estabelecimento = cd_estabelecimento_p;

if  (qt_regra_envio_w > 0) then

	select max(ie_tipo_regra)
	into ie_tipo_regra_w
	from (
		select  ie_tipo_regra
		from  regra_classif_envio_sms
		where ie_situacao = 'A'
		and cd_estabelecimento = cd_estabelecimento_p
		and (ie_classif_agenda = ie_classificacao_p
		or ie_classif_agenda is null)
		order by ie_classif_agenda)
	where rownum = 1;

	if (ie_tipo_regra_w = 'B') then  
		ds_retorno_w := 'N';
	end if;

end if;

return	ds_retorno_w;

end Obter_Se_Classif_Envia_SMS;
/
