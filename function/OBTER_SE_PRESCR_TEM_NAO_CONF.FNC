create or replace
function obter_se_prescr_tem_nao_conf(nr_prescricao_p		number)
				return varchar2 is

ds_retorno_w	varchar2(1) := '';

begin

select 	nvl(max('S'),'N')
into	ds_retorno_w
from	prescr_material a,
		prescr_medica_erro b
where	a.nr_prescricao = b.nr_prescricao
and		b.nr_seq_medic  = a.nr_sequencia
and		a.nr_prescricao = nr_prescricao_p
and		a.ie_agrupador  = 1;

return ds_retorno_w;

end obter_se_prescr_tem_nao_conf;
/