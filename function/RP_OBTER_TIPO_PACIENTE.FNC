create or replace
function rp_obter_tipo_paciente( nr_sequencia_p		number)
 		    	return varchar2 is
				
ds_retorno_w	varchar2(255);
ds_retorno_ww	varchar2(255);

cursor c01 is
	select	d.ds_tipo_paciente
	from	rp_lista_espera_modelo a,
			rp_paciente_reabilitacao b,
			rp_tipo_pac_reab c,
			rp_tipo_paciente d						   
	where  	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	   	b.nr_sequencia = c.nr_seq_pac_reab
	and	   	c.nr_tipo_pac_reab = d.nr_sequencia
	and	   	nvl(c.dt_fim_tipo,sysdate) <= sysdate 
	and	   	a.nr_sequencia = nr_sequencia_p;


begin
if ( nr_sequencia_p is not null) then
	
	open c01;
	loop
	fetch c01 into	
		ds_retorno_ww;
	exit when c01%notfound;
		begin
		if (ds_retorno_w is not null) then
			ds_retorno_w := substr(ds_retorno_w || ', ' || ds_retorno_ww,1,255);
			exit when length (ds_retorno_w) >= 254;
		elsif	(ds_retorno_w is null) then
				ds_retorno_w := ds_retorno_ww;
		end if;
		end;
	end loop;
	close c01;
	
end if;
	
return	ds_retorno_w ;

end rp_obter_tipo_paciente;
/
