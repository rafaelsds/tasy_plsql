create or replace 
function HMC_obter_relatorio_concat( nr_atendimento_p	Number,
										dt_inicio_p		date,
										dt_fim_p 		date
									  )
									return varchar2 is
			
ds_temp_w		varchar(4000);
ds_retorno_w 	varchar(4000);
posicao_w 		varchar(4000);

cursor C01 is
select 	w.nr_prescricao
from	prescr_medica w
WHERE w.nr_atendimento = nr_atendimento_p
AND	((w.dt_inicio_prescr BETWEEN dt_inicio_p AND dt_fim_p) OR
	 (w.dt_validade_prescr BETWEEN dt_inicio_p AND dt_fim_p))
order by w.nr_prescricao desc;
begin

open 	c01;
	loop
	fetch 	c01 into
	ds_temp_w;
	
	if	((ds_retorno_w is null) or
		(instr(ds_retorno_w,ds_temp_w) = 0)) then	
		ds_retorno_w := ds_retorno_w ||','|| ds_temp_w;
	end if;
	exit when c01%notfound;
end loop;
close c01;

select length(ds_retorno_w)
into posicao_w
from dual;
	
return	substr(ds_retorno_w,2,posicao_w);

end HMC_obter_relatorio_concat;
/
