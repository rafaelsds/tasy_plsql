create or replace function GET_BLOOD_ALLOCATION_RULE_8_OK(ie_tipo_derivado_p varchar2,
                                                  ie_tipo_sangue_p varchar2,
                                                  ie_fator_rh_p varchar2,
                                                  ie_tipo_sangue_paciente_p varchar2,
                                                  ie_fator_rh_paciente_p varchar2) 
                                                  return varchar2 is
                                                  
ds_retorno_w varchar(200) := null;
ds_derivado_w varchar(200) := null;
qt_exams_compat_w number :=0;
qt_exams_incompat_w number :=0;
qt_exams_compat_w2 number :=0;
qt_exams_incompat_w2 number :=0;

begin

select count(*) 
into qt_exams_compat_w
from san_compat_atipica
where ie_tp_derivado = ie_tipo_derivado_p
and ie_tp_sangue_derivado = ie_tipo_sangue_p
and ie_tp_sangue_paciente = ie_tipo_sangue_paciente_p
and ie_tp_rh_derivado = ie_fator_rh_p
and ie_tp_rh_paciente = ie_fator_rh_paciente_p
and ie_resultado = 'COM';

select count(*) 
into qt_exams_incompat_w
from san_compat_atipica
where ie_tp_derivado = ie_tipo_derivado_p
and ie_tp_sangue_derivado = ie_tipo_sangue_p
and ie_tp_sangue_paciente = ie_tipo_sangue_paciente_p
and ie_tp_rh_derivado = ie_fator_rh_p
and ie_tp_rh_paciente = ie_fator_rh_paciente_p
and ie_resultado = 'INC';

select count(*) 
into qt_exams_compat_w2
from san_compat_atipica
where ie_tp_derivado = ie_tipo_derivado_p
and ie_tp_sangue_derivado = ie_tipo_sangue_p
and ie_tp_sangue_paciente = ie_tipo_sangue_paciente_p
and (ie_tp_rh_derivado = ie_fator_rh_p or ie_tp_rh_derivado is null)
and (ie_tp_rh_paciente = ie_fator_rh_paciente_p or ie_tp_rh_paciente is null)
and ie_resultado = 'COM';

select count(*) 
into qt_exams_incompat_w2
from san_compat_atipica
where ie_tp_derivado = ie_tipo_derivado_p
and ie_tp_sangue_derivado = ie_tipo_sangue_p
and ie_tp_sangue_paciente = ie_tipo_sangue_paciente_p
and (ie_tp_rh_derivado = ie_fator_rh_p or ie_tp_rh_derivado is null)
and (ie_tp_rh_paciente = ie_fator_rh_paciente_p or ie_tp_rh_paciente is null)
and ie_resultado = 'INC';

if (qt_exams_compat_w > 0) then
    ds_retorno_w := replace(replace(obter_desc_expressao(1041429), '%1', obter_valor_dominio(1341, ie_tipo_derivado_p)),'%2', ie_tipo_sangue_p||ie_fator_rh_p);
elsif(qt_exams_incompat_w > 0) then
    ds_retorno_w := replace(replace(obter_desc_expressao(1041433), '%1', obter_valor_dominio(1341, ie_tipo_derivado_p)),'%2', ie_tipo_sangue_p||ie_fator_rh_p);
elsif (qt_exams_compat_w2 > 0) then
    ds_retorno_w := replace(replace(obter_desc_expressao(1041429), '%1', obter_valor_dominio(1341, ie_tipo_derivado_p)),'%2', ie_tipo_sangue_p||ie_fator_rh_p);
elsif(qt_exams_incompat_w2 > 0) then
    ds_retorno_w := replace(replace(obter_desc_expressao(1041433), '%1', obter_valor_dominio(1341, ie_tipo_derivado_p)),'%2', ie_tipo_sangue_p||ie_fator_rh_p);
elsif (qt_exams_compat_w = 0 and qt_exams_incompat_w = 0 and qt_exams_compat_w2 = 0 and qt_exams_incompat_w2 = 0) then
    ds_retorno_w := obter_desc_expressao(1041511);
end if;

return ds_retorno_w;

end GET_BLOOD_ALLOCATION_RULE_8_OK;
/
