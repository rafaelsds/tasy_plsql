CREATE OR REPLACE
FUNCTION Obter_Cid10_Envio_Laudo
		(	nr_seq_interno_p	Number,
			nr_cid10_p		Number)
			return	Varchar2 is


cd_doenca_cid_w		Varchar2(10);
ds_retorno_w		Varchar2(10);
qt_cid10_w		Number	:= 0;


Cursor C01 is
	select	cd_doenca_cid
	from	sus_laudo_area_irradiada
	where	nr_seq_laudo	= nr_seq_interno_p
	order by nr_sequencia;

BEGIN

OPEN C01;
LOOP
FETCH C01 into
		cd_doenca_cid_w;
exit when c01%notfound;
	qt_cid10_w	:= qt_cid10_w + 1;
	if	(nr_cid10_p	= 1) and
		(qt_cid10_w	= 1) then
		ds_retorno_w	:= cd_doenca_cid_w;
	elsif	(nr_cid10_p	= 2) and
		(qt_cid10_w	= 2) then
		ds_retorno_w	:= cd_doenca_cid_w;
	elsif	(nr_cid10_p	= 3) and
		(qt_cid10_w	= 3) then
		ds_retorno_w	:= cd_doenca_cid_w;
	end if;
END LOOP;
Close C01;

return ds_retorno_w;

END Obter_Cid10_Envio_Laudo;
/