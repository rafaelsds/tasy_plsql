create or replace
function bsc_obter_media_indicador(nr_seq_indicador_p		number,
				cd_ano_p			number,
				nm_usuario_p		varchar2)
 		    	return number is

qt_media_retorno_w	bsc_ind_inf.qt_real%type;

begin

select	avg(qt_real) qt_media
into	qt_media_retorno_w
from	bsc_ind_inf
where	nr_seq_indicador = nr_seq_indicador_p
and	cd_ano = cd_ano_p;

return	qt_media_retorno_w;

end bsc_obter_media_indicador;
/