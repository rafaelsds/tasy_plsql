Create or replace
FUNCTION CARE_PLAN_ACTIVITY_QUANTITY (nr_prescricao_p varchar2, pe_procs varchar2)
return number is

nr_retorno_w                        number (10) := 0;
nr_seq_pe_procedimento_w            number (10);
qtd_w                               number (10) := 0;

begin
    begin
        for item in (
            with temp as  (
                   select nr_prescricao_p nr_prescricao  from dual
            ) SELECT trim(regexp_substr(nr_prescricao, '[^,]+', 1, level)) nr_prescricao
                FROM (SELECT  nr_prescricao FROM temp) t
                CONNECT BY instr(nr_prescricao, ',', 1, level - 1) > 0 
        ) loop

            select count(*)
            into qtd_w from 
                    CP_INTERVENTION_ITEM_ASSOC iia,
                    CP_INTERVENTION_ITEM ii,
                    CP_INTERVENTION i,
                    PE_PROCEDIMENTO p,
                    PE_PRESCR_PROC ppp

                    where iia.NR_SEQ_INTERV_ITEM = ii.NR_SEQUENCIA
                    and iia.NR_SEQ_INTERVENTION = i.NR_SEQUENCIA
                    and i.NR_SEQ_PE_PROCEDIMENTO = p.NR_SEQUENCIA
                    and i.NR_SEQ_PE_PROCEDIMENTO = nr_seq_pe_procedimento_w
                    and i.NR_SEQ_PE_PROCEDIMENTO = ppp.NR_SEQ_PROC
                    and ppp.NR_SEQ_PRESCR = item.nr_prescricao;

            nr_retorno_w := nr_retorno_w + qtd_w;

        end loop;

    exception when others then
        nr_retorno_w := 0;
    end;
return nr_retorno_w;
END CARE_PLAN_ACTIVITY_QUANTITY;
/
