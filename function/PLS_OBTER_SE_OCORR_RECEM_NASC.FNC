create or replace
function pls_obter_se_ocorr_recem_nasc
		(	nr_seq_guia_p		Number,
			nr_seq_requisicao_p	Number,
                        nr_seq_execucao_p       Number,
			ie_recem_nascido_p	Varchar2,                        
			qt_dias_limite_util_p	Number)
 		    	return Varchar2 is

ie_retorno_w		Varchar2(1)	:= 'N';
ie_recem_nascido_w	Varchar2(1);
dt_nasc_recem_nascido_w	Date;
dt_solicitacao_w	Date;
qt_dias_recem_nasc_w	Number(10);
nr_seq_requisicao_w     Number(10);

begin
if	(nvl(nr_seq_guia_p,0)	<> 0) then
	begin
		select	ie_recem_nascido,
			dt_nasc_recem_nascido,
			dt_solicitacao
		into	ie_recem_nascido_w,
			dt_nasc_recem_nascido_w,
			dt_solicitacao_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;
	exception
	when others then
		ie_recem_nascido_w		:= 'N';
		dt_nasc_recem_nascido_w		:= null;
		dt_solicitacao_w		:= null;
	end;
elsif	(nvl(nr_seq_requisicao_p,0)	<> 0) then
	begin
		select	ie_recem_nascido,
			dt_nasc_recem_nascido,
			dt_requisicao
		into	ie_recem_nascido_w,
			dt_nasc_recem_nascido_w,
			dt_solicitacao_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;
	exception
	when others then
		ie_recem_nascido_w		:= 'N';
		dt_nasc_recem_nascido_w		:= null;
		dt_solicitacao_w		:= null;
	end;

elsif	(nr_seq_execucao_p	is not null) then
	begin
		select	nr_seq_requisicao
		into	nr_seq_requisicao_w
		from	pls_execucao_requisicao
		where	nr_sequencia	= nr_seq_execucao_p;
	exception
	when others then
		nr_seq_requisicao_w	:= null;
	end;
        
        if (nr_seq_requisicao_w is not null)    then
        begin
		select	ie_recem_nascido,
                        dt_nasc_recem_nascido,
                        dt_requisicao
		into	ie_recem_nascido_w,
                        dt_nasc_recem_nascido_w,
                        dt_solicitacao_w                        
                from	pls_requisicao
                where	nr_sequencia	= nr_seq_requisicao_w;
        exception
	when others then
		ie_recem_nascido_w		:= 'N';
		dt_nasc_recem_nascido_w		:= null;
		dt_solicitacao_w		:= null;                        
		end;
	end if;
end if;

begin
	select	trunc(sysdate) - trunc(dt_nasc_recem_nascido_w)
	into	qt_dias_recem_nasc_w
	from	dual;
exception
when others then
	qt_dias_recem_nasc_w	:= 0;
end;

-- Se for uma regra com os campos "Rec�m nascido" e "Qt dias limite utiliza��o rec�m nasc" informados
if	((nvl(ie_recem_nascido_p,'N')	= 'S')	and (nvl(qt_dias_limite_util_p,0)	> 0)) then
	if	(nvl(ie_recem_nascido_p,'N')	= 'S') and (nvl(ie_recem_nascido_w,'N')	= 'S') then
		ie_retorno_w	:= 'S';	
		--Se o valor informado na regra for maior que "0" e a quantidade de dias limite tamb�m for maior que "0",  mas a quantidade de dias limite form menor ou igual ao valor informado na regra, n�o deve gerar a ocorr�ncia.
		if	(nvl(qt_dias_recem_nasc_w,0)	>= 0) and (nvl(qt_dias_limite_util_p,0)	> 0) then
			if	(qt_dias_recem_nasc_w	<= qt_dias_limite_util_p) then
				ie_retorno_w	:= 'N';
			end if;
		end if;
	end if;
-- Se for uma regra somente com o campo "Rec�m nascido" informado
elsif	((nvl(ie_recem_nascido_p,'N')	= 'S')	and (nvl(qt_dias_limite_util_p,0)	= 0)) then
	if	(nvl(ie_recem_nascido_p,'N')	= 'S') and (nvl(ie_recem_nascido_w,'N')	= 'S') then
		ie_retorno_w	:= 'S';
	end if;
-- Se for uma regra somente com o campo "Qt dias limite utiliza��o rec�m nasc" informado
elsif	((nvl(ie_recem_nascido_p,'N')	= 'N')	and (nvl(qt_dias_limite_util_p,0)	> 0)) then
	if	(nvl(qt_dias_recem_nasc_w,0)	> qt_dias_limite_util_p) then
		ie_retorno_w	:= 'S';
	end if;
end if;

return	ie_retorno_w;

end pls_obter_se_ocorr_recem_nasc;
/
