create or replace
function obter_maior_valor_compra_geral(
				cd_material_p		number,
				cd_local_estoque_p	Number,
				ie_tipo_p			Varchar2,
				cd_unidade_medida_p	varchar2)
return number is

/*Function correta para buscar o maior valor de compra de um material.*/

vl_maior_valor_w			number(15,4);
vl_select_w			number(15,4);
nr_sequencia_w			number(10,0);
cd_material_estoque_w		number(6);
cd_local_estoque_w		number(10);
nr_item_nf_w			number(10);

cursor c01 is
select	a.nr_sequencia,
	a.nr_item_nf
from	natureza_operacao o,
	operacao_nota p,
	nota_fiscal b,
	nota_fiscal_item a
where	a.nr_sequencia			= b.nr_sequencia
and	b.cd_natureza_operacao		= o.cd_natureza_operacao
and	b.cd_operacao_nf 		= p.cd_operacao_nf
and	a.dt_atualizacao		>= sysdate - 365
and	(((ie_tipo_p = 'N') and (a.cd_material = cd_material_p)) or
	 ((ie_tipo_p = 'S') and (a.cd_material_estoque = cd_material_p)))
and	b.ie_situacao			= '1'
and	o.ie_entrada_saida		= 'E'
and	p.ie_ultima_compra		= 'S'
and	obter_tipo_operacao_nota(b.cd_operacao_nf) <> 'T'
and	a.cd_unidade_medida_compra	= cd_unidade_medida_p
and	((cd_local_estoque_w = 0) or
	((cd_local_estoque_w > 0) and (a.cd_local_estoque = cd_local_estoque_w)));

begin

cd_local_estoque_w	:= nvl(cd_local_estoque_p,0);
vl_maior_valor_w	:= 0;
vl_select_w		:= 0;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	nr_item_nf_w;
exit when C01%notfound;
	begin
	
	if	(nvl(ie_tipo_p, 'N') = 'N') then
		
		select	nvl(max(vl_unitario_item_nf),0)
		into	vl_select_w
		from 	nota_fiscal_item a
		where	nr_sequencia 			= nr_sequencia_w
		and	nr_item_nf			= nr_item_nf_w
		and	cd_material			= cd_material_p
		and	a.cd_unidade_medida_compra	= cd_unidade_medida_p;
		
		if	(vl_select_w > vl_maior_valor_w) then
			vl_maior_valor_w	:= vl_select_w;
		end if;

	elsif	(nvl(ie_tipo_p, 'N') <> 'N') then
		
		select	nvl(max(vl_unitario_item_nf),0)
		into	vl_select_w
		from 	nota_fiscal_item a
		where	nr_sequencia 			= nr_sequencia_w
		and	nr_item_nf			= nr_item_nf_w
		and	cd_material_estoque		= cd_material_p
		and	a.cd_unidade_medida_compra	= cd_unidade_medida_p;
		
		if	(vl_select_w > vl_maior_valor_w) then
			vl_maior_valor_w	:= vl_select_w;
		end if;		
	end if;		
	end;
end loop;
close C01;


return vl_maior_valor_w;

END obter_maior_valor_compra_geral;
/
