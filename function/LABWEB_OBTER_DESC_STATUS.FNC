create or replace
function LabWeb_obter_desc_status ( nr_prescricao_p		number, 
									 nr_seq_prescricao_p	number,
									 ie_param_status_p		number,
									 ds_desc_status_p		varchar2,
									 ds_mensagem_proc_p		varchar2,
									 ds_mensagem_bloq_p		varchar2,
									 nm_usuario_p			Varchar2,
									 cd_estabelecimento_p	Number)
					return varchar2 is

ds_retorno_w		varchar2(255);
ie_status_atend_w	number(2);

begin

ds_retorno_w := '';

if (ie_param_status_p > 0 ) then
	if  (nr_prescricao_p is not null) and
		(nr_seq_prescricao_p is not null) then
		select 	MAX(ie_status_atend) 
		into	ie_status_atend_w
		from	prescr_procedimento
		where 	nr_prescricao = nr_prescricao_p
		and		nr_sequencia  = nr_seq_prescricao_p;

		if  (ie_status_atend_w <= ie_param_status_p) and
			(obter_status_result_exame_site(nr_prescricao_p,nr_seq_prescricao_p,nm_usuario_p,cd_estabelecimento_p) <> 'L')then
			ds_retorno_w := ds_desc_status_p;
		elsif (obter_status_result_exame_site(nr_prescricao_p,nr_seq_prescricao_p,nm_usuario_p,cd_estabelecimento_p) = 'L') or
			  (ie_status_atend_w > ie_param_status_p)then
			   select Decode(obter_status_result_exame_site(nr_prescricao_p,nr_seq_prescricao_p,nm_usuario_p,cd_estabelecimento_p),
                       'P', ds_mensagem_proc_p,
                       'B', ds_mensagem_bloq_p,
                       decode(Obter_Regra_Status_Parcial(nr_prescricao_p, nr_seq_prescricao_p), ' ', obter_desc_expressao(292549), Obter_Regra_Status_Parcial(nr_prescricao_p, nr_seq_prescricao_p)))
			   into	ds_retorno_w 
			   from dual;
		end if;
	end if;
else
	select Decode(obter_status_result_exame_site(nr_prescricao_p,nr_seq_prescricao_p,nm_usuario_p,cd_estabelecimento_p),
                'P', ds_mensagem_proc_p,
                'B', ds_mensagem_bloq_p, 
                decode(Obter_Regra_Status_Parcial(nr_prescricao_p, nr_seq_prescricao_p), ' ', obter_desc_expressao(292549), Obter_Regra_Status_Parcial(nr_prescricao_p, nr_seq_prescricao_p)))
    into	ds_retorno_w 
    from dual;
end if;


return	ds_retorno_w;

end LabWeb_obter_desc_status;
/