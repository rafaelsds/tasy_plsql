create or replace
function cpoe_obter_quant_horas_ant(
					cd_perfil_p		perfil.cd_perfil%type) return number is

qt_horas_ant_w	cpoe_presentation_rule.qt_horas_anteriores%type;
qt_horas_default_w	cpoe_presentation_rule.qt_horas_anteriores%type := 6;
					
begin

if (obter_funcao_ativa <> 7010) then
	wheb_usuario_pck.set_cd_funcao(2314);
end if;

wheb_usuario_pck.set_ds_form('AtePac_OE');

select	nvl(max(qt_horas_anteriores),qt_horas_default_w)
into	qt_horas_ant_w
from	(select qt_horas_anteriores
	from	cpoe_presentation_rule
	where 	nvl(cd_perfil, nvl(cd_perfil_p,0)) = nvl(cd_perfil_p,0)
	and	nvl(cd_estabelecimento, nvl(wheb_usuario_pck.get_cd_estabelecimento,0)) = nvl(wheb_usuario_pck.get_cd_estabelecimento,0)
	order by cd_perfil, cd_estabelecimento)
where 	rownum = 1;

if (qt_horas_ant_w < 2) then
	qt_horas_ant_w := 2;
elsif (qt_horas_ant_w > qt_horas_default_w) then
	qt_horas_ant_w := qt_horas_default_w;
end if;

return qt_horas_ant_w;

end cpoe_obter_quant_horas_ant;
/
