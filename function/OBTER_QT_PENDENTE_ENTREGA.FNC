create or replace
function obter_qt_pendente_entrega( nr_solic_compra_p		number,
									nr_item_solic_compra_p	number)
									return number is

retorno		number(13,4) := 0;

begin

if (nvl(nr_solic_compra_p,0) > 0) and (nvl(nr_item_solic_compra_p,0) > 0) then
	select	nvl(sum (a.qt_prevista_entrega),0) - nvl(max(obter_qt_entregue_solic_nf(b.nr_solic_compra, b.nr_item_solic_compra)),0)
	into	retorno
	from	ordem_compra_item_entrega a,
			ordem_compra_item b
	where   a.nr_ordem_compra = b.nr_ordem_compra
	and		a.nr_item_oci = b.nr_item_oci
	and		b.nr_solic_compra = nr_solic_compra_p 
	and		b.nr_item_solic_compra = nr_item_solic_compra_p
	and		a.dt_cancelamento is null;
end if;

return retorno;

end obter_qt_pendente_entrega;
/