create or replace
function TISS_OBTER_REGRA_CID_INVALIDO
			(cd_estabelecimento_p	in number,
			cd_convenio_p		in number,
			cd_doenca_cid_p		in varchar2,
			ie_tipo_atendimento_p	in number) 
			return varchar2 is

ie_invalido_w	varchar2(1);	
ds_retorno_w	varchar2(1);		
			
cursor c01 is
select	nvl(ie_invalido,'N')
from	tiss_regra_cid_invalido
where	cd_estabelecimento					= cd_estabelecimento_p
and	cd_doenca						= cd_doenca_cid_p
and	nvl(cd_convenio,nvl(cd_convenio_p,0))			= nvl(cd_convenio_p,0)
and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0))	= nvl(ie_tipo_atendimento_p,0)
order by nvl(cd_convenio,0),
	nvl(ie_tipo_atendimento,0);
			
begin

ds_retorno_w	:= 'N';

open C01;
loop
fetch C01 into	
	ie_invalido_w;
exit when C01%notfound;
	begin
	ds_retorno_w	:= ie_invalido_w;
	end;
end loop;
close C01;

return	ds_retorno_w;

end TISS_OBTER_REGRA_CID_INVALIDO;
/