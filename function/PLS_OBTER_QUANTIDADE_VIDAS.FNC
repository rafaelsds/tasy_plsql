create or replace
function pls_obter_quantidade_vidas
			(	nr_sequencia_p		number,
				ie_opcao_p		varchar2,
				ie_tipo_p		varchar2)
				return number is
/* ie_opcao_p
	T - Todos
	I - Titular
	L - Parentes legais
	E - Titular + Parentes legais
*/

/* ie_tipo_p
	C - Contrato
	P - Proposta de ades�o
	S - Simulador de pre�o
	PO - Proposta online
*/

ds_retorno_w			number(10);
qt_vidas_w			number(10);
qt_vidas_subcontrato_w		number(10);
nr_seq_contrato_w		number(10);
ie_tipo_proposta_w		number(2);
qt_vidas_contrato_w		number(10);
nr_seq_simulacao_w		number(10);
nr_seq_titular_contrato_w	number(10);
nr_seq_titular_w		number(10);
nr_seq_proposta_w		Number(10);
nr_contrato_principal_w		Number(10);

begin

if 	(ie_tipo_p	 = 'C') then /* Contrato */
	select	nr_seq_contrato,
		nr_seq_titular
	into	nr_seq_contrato_w,
		nr_seq_titular_w
	from	pls_segurado
	where	nr_sequencia	= nr_sequencia_p;

	/* Obter o contrato principal */
	select	nvl(nr_contrato_principal, nr_seq_contrato_w)
	into	nr_contrato_principal_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;

	if 	(ie_opcao_p	= 'T') then /* Todos */
		select	count(*)
		into	qt_vidas_w
		from	pls_contrato	b,
			pls_segurado 	a
		where	a.nr_seq_contrato	= b.nr_sequencia
		and	((b.nr_sequencia in (nr_seq_contrato_w, nr_contrato_principal_w)) or (b.nr_contrato_principal in (nr_seq_contrato_w, nr_contrato_principal_w)))
		and	a.dt_liberacao is not null
		and	a.dt_rescisao is null;
	elsif 	(ie_opcao_p	= 'I') then /* Titular */
		select	count(*)
		into	qt_vidas_w
		from	pls_contrato	b,
			pls_segurado 	a
		where	a.nr_seq_contrato	= b.nr_sequencia
		and	((b.nr_sequencia in (nr_seq_contrato_w, nr_contrato_principal_w)) or (b.nr_contrato_principal in (nr_seq_contrato_w, nr_contrato_principal_w)))
		and	a.nr_sequencia		= nr_sequencia_p
		and	a.dt_liberacao is not null
		and	a.dt_rescisao is null
		and	a.nr_seq_titular is null;
	elsif 	(ie_opcao_p	= 'L') then /* Parentes legais */
		select	count(*)
		into	qt_vidas_w
		from	pls_contrato	c,
			grau_parentesco b,
			pls_segurado 	a
		where	a.nr_seq_parentesco	= b.nr_sequencia
		and	a.nr_seq_contrato	= c.nr_sequencia
		and	a.nr_seq_titular	= nr_sequencia_p
		and	((c.nr_sequencia in (nr_seq_contrato_w, nr_contrato_principal_w)) or (c.nr_contrato_principal in (nr_seq_contrato_w, nr_contrato_principal_w)))
		and	a.dt_liberacao is not null
		and	a.dt_rescisao is null
		and	b.ie_tipo_parentesco	= '1';
	elsif 	(ie_opcao_p	= 'E') then /* Titular + Parentes legais */
		if 	(nvl(nr_seq_titular_w,0) = 0) then
			select	count(*)
			into	qt_vidas_w
			from	pls_contrato	c,
				grau_parentesco	b,
				pls_segurado	a
			where	a.nr_seq_parentesco	= b.nr_sequencia(+)
			and	a.nr_seq_contrato	= c.nr_sequencia
			and	(((a.nr_seq_titular	= nr_sequencia_p) and (b.ie_tipo_parentesco	= '1'))
			or	((a.nr_sequencia	= nr_sequencia_p) and (a.nr_seq_titular is null)))
			and	((c.nr_sequencia in (nr_seq_contrato_w, nr_contrato_principal_w)) or (c.nr_contrato_principal in (nr_seq_contrato_w, nr_contrato_principal_w)))
			and	a.dt_liberacao is not null
			and	a.dt_rescisao is null;
		else
			select	count(*)
			into	qt_vidas_w
			from	pls_contrato	c,
				grau_parentesco	b,
				pls_segurado	a
			where	a.nr_seq_parentesco	= b.nr_sequencia(+)
			and	a.nr_seq_contrato	= c.nr_sequencia
			and	(((a.nr_seq_titular	= nr_seq_titular_w) and (b.ie_tipo_parentesco	= '1'))
			or	((a.nr_sequencia	= nr_seq_titular_w) and (a.nr_seq_titular is null)))
			and	((c.nr_sequencia in (nr_seq_contrato_w, nr_contrato_principal_w)) or (c.nr_contrato_principal in (nr_seq_contrato_w, nr_contrato_principal_w)))
			and	a.dt_liberacao is not null
			and	a.dt_rescisao is null;
		end if;
	end if;	
	ds_retorno_w	:= (nvl(qt_vidas_subcontrato_w,0) + nvl(qt_vidas_w,0));
	
elsif 	(ie_tipo_p	= 'P') then /* Proposta de ades�o */
	qt_vidas_contrato_w	:= 0;
	
	select	nvl((	select	max(x.nr_sequencia)
			from	pls_contrato x
			where	x.nr_contrato = a.nr_seq_contrato),0),
		a.ie_tipo_proposta,
		b.nr_seq_titular_contrato,
		a.nr_sequencia,
		b.nr_seq_titular
	into	nr_seq_contrato_w,
		ie_tipo_proposta_w,
		nr_seq_titular_contrato_w,
		nr_seq_proposta_w,
		nr_seq_titular_w
	from	pls_proposta_adesao 		a,
		pls_proposta_beneficiario 	b
	where	a.nr_sequencia	= b.nr_seq_proposta
	and	b.nr_sequencia	= nr_sequencia_p;
	
	if 	(ie_opcao_p	= 'T') then /* Todos */
		select	count(*)
		into	qt_vidas_w
		from	pls_proposta_beneficiario
		where	nr_seq_proposta	= nr_seq_proposta_w
		and	dt_cancelamento is null;
		
		if 	(nr_seq_contrato_w	<> 0) and
			(ie_tipo_proposta_w in (2,4)) then
			select	count(*)
			into	qt_vidas_contrato_w
			from	pls_contrato a,
				pls_segurado b
			where	a.nr_sequencia	= b.nr_seq_contrato
			and	a.nr_sequencia	= nr_seq_contrato_w
			and	b.nr_sequencia	= nr_seq_titular_contrato_w
			and	b.dt_rescisao is null
			and	b.dt_liberacao is not null;
		end if;
	elsif 	(ie_opcao_p	= 'I') then /* Titular */
		select	count(*)
		into	qt_vidas_w
		from	pls_proposta_beneficiario
		where	nr_seq_proposta	= nr_seq_proposta_w
		and	nr_sequencia	= nr_sequencia_p
		and	nr_seq_titular is null
		and	dt_cancelamento is null;
		
		if 	(nr_seq_contrato_w	<> 0) and
			(ie_tipo_proposta_w in (2,4)) then
			select	count(*)
			into	qt_vidas_contrato_w
			from	pls_contrato a,
				pls_segurado b
			where	a.nr_sequencia	= b.nr_seq_contrato
			and	a.nr_sequencia	= nr_seq_contrato_w
			and	b.dt_rescisao is null
			and	b.dt_liberacao is not null
			and	b.nr_seq_titular is null;
		end if;
	elsif 	(ie_opcao_p	= 'L') then /* Parentes legais */
		select	count(*)
		into	qt_vidas_w
		from	pls_proposta_beneficiario	a,
			grau_parentesco 		b
		where	a.nr_seq_proposta	= nr_seq_proposta_w
		and	a.nr_seq_parentesco	= b.nr_sequencia
		and	a.nr_sequencia		= nr_sequencia_p
		and	b.ie_tipo_parentesco	= '1'
		and	a.dt_cancelamento is null;
		
		if 	(nr_seq_contrato_w	<> 0) and
			(ie_tipo_proposta_w in (2,4)) then
			select	count(*)
			into	qt_vidas_contrato_w
			from	pls_contrato a,
				pls_segurado b,
				grau_parentesco c
			where	a.nr_sequencia		= b.nr_seq_contrato
			and	b.nr_seq_parentesco	= c.nr_sequencia
			and	a.nr_sequencia		= nr_seq_contrato_w
			and	b.dt_rescisao is null
			and	b.dt_liberacao is not null
			and	c.ie_tipo_parentesco	= '1';
		end if;
	elsif 	(ie_opcao_p	= 'E') then /* Titular + Parentes legais */
		/*Inseri select abaixo pois n�o estava calculando certo para os casos onde na proposta tem somente os dependentes e o titular j� est� no contrato */
		if	(nvl(nr_seq_titular_contrato_w,0) > 0) then
			select	count(*)
			into	qt_vidas_w
			from	pls_proposta_beneficiario	a,
				grau_parentesco			b
			where	a.nr_seq_parentesco	= b.nr_sequencia(+)
			and	a.nr_seq_titular_contrato	= nr_seq_titular_contrato_w 
			and 	b.ie_tipo_parentesco	= '1'
			and	a.nr_seq_proposta	= nr_seq_proposta_w
			and	a.dt_cancelamento is null;
		elsif	(nvl(nr_seq_titular_w,0) = 0) then
			select	count(*)
			into	qt_vidas_w
			from	pls_proposta_beneficiario	a,
				grau_parentesco 		b
			where	a.nr_seq_parentesco	= b.nr_sequencia(+)
			and	(((a.nr_seq_titular	= nr_sequencia_p) and (b.ie_tipo_parentesco	= '1')) 
			or	((a.nr_sequencia	= nr_sequencia_p) and (a.nr_seq_titular is null))) 
			and	a.nr_seq_proposta	= nr_seq_proposta_w
			and	a.dt_cancelamento is null;		
		else
			select	count(*)
			into	qt_vidas_w
			from	pls_proposta_beneficiario	a,
				grau_parentesco			b
			where	a.nr_seq_parentesco	= b.nr_sequencia(+)
			and	(((a.nr_seq_titular	= nr_seq_titular_w) and (b.ie_tipo_parentesco	= '1')) 
			or	((a.nr_sequencia	= nr_seq_titular_w) and (a.nr_seq_titular is null))) 
			and	a.nr_seq_proposta	= nr_seq_proposta_w
			and	a.dt_cancelamento is null;
		end if; 
		
		if 	(nr_seq_contrato_w	<> 0) and
			(ie_tipo_proposta_w in (2,4)) then
			select	count(*)
			into	qt_vidas_contrato_w
			from	pls_contrato a,
				pls_segurado b,
				grau_parentesco c
			where	a.nr_sequencia		= b.nr_seq_contrato
			and	b.nr_seq_parentesco	= c.nr_sequencia(+)
			and	a.nr_sequencia		= nr_seq_contrato_w
			and	b.dt_rescisao is null
			and	b.dt_liberacao is not null
			and	((c.ie_tipo_parentesco	= '1') or (b.nr_seq_titular is null));
		end if;
	end if;
	
	ds_retorno_w	:= (qt_vidas_w + qt_vidas_contrato_w);
elsif 	(ie_tipo_p	= 'S') then /* Simulador de pre�o */
	select	nr_seq_simulacao
	into	nr_seq_simulacao_w
	from	pls_simulpreco_individual
	where	nr_sequencia	= nr_sequencia_p;
	
	if (ie_opcao_p	= 'T') then /* Todos */
		select	count(*)
		into	qt_vidas_w
		from	pls_simulpreco_individual
		where	nr_seq_simulacao	= nr_seq_simulacao_w;
		
	elsif (ie_opcao_p	= 'I') then /* Titular */
		select	count(*)
		into	qt_vidas_w
		from	pls_simulpreco_individual
		where	nr_seq_simulacao	= nr_seq_simulacao_w
		and	ie_tipo_benef		= 'T';
		
	elsif (ie_opcao_p	= 'L') then /* Parentes legais */
		select	count(*)
		into	qt_vidas_w
		from	pls_simulpreco_individual a,
			grau_parentesco b
		where	a.nr_seq_parentesco	= b.nr_sequencia
		and	a.nr_seq_simulacao	= nr_seq_simulacao_w
		and	b.ie_tipo_parentesco	= '1';
		
	elsif (ie_opcao_p	= 'E') then /* Titular + Parentes legais */
		select	count(*)
		into	qt_vidas_w
		from	pls_simulpreco_individual a,
			grau_parentesco b
		where	a.nr_seq_parentesco	= b.nr_sequencia(+)
		and	a.nr_seq_simulacao	= nr_seq_simulacao_w
		and	((b.ie_tipo_parentesco	= '1')
		or	(a.ie_tipo_benef		= 'T'));
	end if;
	
	ds_retorno_w	:= qt_vidas_w;
elsif 	(ie_tipo_p	= 'PO') then /* Solicita��o de proposta online*/
	select	nr_seq_prop_online
	into	nr_seq_proposta_w
	from	pls_proposta_benef_online
	where	nr_sequencia	= nr_sequencia_p;
	
	if (ie_opcao_p	= 'T') then /* Todos */
		select	count(*)
		into	qt_vidas_w
		from	pls_proposta_benef_online
		where	nr_seq_prop_online	= nr_seq_proposta_w;
		
	elsif (ie_opcao_p	= 'I') then /* Titular */
		select	count(*)
		into	qt_vidas_w
		from	pls_proposta_benef_online
		where	nr_seq_prop_online	= nr_seq_proposta_w
		and	ie_tipo_benef		= 'T';
		
	elsif (ie_opcao_p	= 'L') then /* Parentes legais */
		select	count(*)
		into	qt_vidas_w
		from	pls_proposta_benef_online a,
			grau_parentesco b
		where	a.nr_seq_parentesco	= b.nr_sequencia
		and	a.nr_seq_prop_online	= nr_seq_proposta_w
		and	b.ie_tipo_parentesco	= '1';
		
	elsif (ie_opcao_p	= 'E') then /* Titular + Parentes legais */
		select	count(*)
		into	qt_vidas_w
		from	pls_proposta_benef_online a,
			grau_parentesco b
		where	a.nr_seq_parentesco	= b.nr_sequencia(+)
		and	a.nr_seq_prop_online	= nr_seq_proposta_w
		and	((b.ie_tipo_parentesco	= '1')
		or	(a.ie_tipo_benef		= 'T'));
	end if;
	
	ds_retorno_w	:= qt_vidas_w;	
end if;

return	ds_retorno_w;

end pls_obter_quantidade_vidas;
/
