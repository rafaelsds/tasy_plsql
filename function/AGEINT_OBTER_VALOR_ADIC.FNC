create or replace
function Ageint_Obter_Valor_Adic(
				nr_seq_ageint_p		number)
 		    	return number is

vl_retorno_w		number(15,2);
vl_adic_pacote_w	number(15,2);
vl_adic_item_w		number(15,2);
ie_suprimir_val_exames_w	varchar2(1);
		
begin

Obter_param_usuario(869, 360, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, obter_estabelecimento_ativo, ie_suprimir_val_exames_w);

if	(ie_suprimir_val_exames_w = 'N')then

	select 	sum(a.vl_item)
	into	vl_adic_item_w
	from 	ageint_itens_adicionais a, 
			agenda_integrada_item b 
	where 	b.nr_seq_agenda_int	= nr_seq_ageint_p
	and 	a.nr_seq_ageint_item 	= b.nr_sequencia;

	select 	sum(a.vl_item)
	into	vl_adic_pacote_w
	from 	ageint_itens_adicionais a, 
			ageint_proc_pacote b 
	where 	b.nr_seq_Ageint		= nr_seq_ageint_p
	and 	a.nr_seq_ageint_pacote 	= b.nr_sequencia;
else
	select 	sum(a.vl_item)
	into	vl_adic_item_w
	from 	ageint_itens_adicionais a, 
			agenda_integrada_item b 
	where 	b.nr_seq_agenda_int	= nr_seq_ageint_p
	and 	a.nr_seq_ageint_item 	= b.nr_sequencia
	and		b.ie_tipo_agendamento	<> 'E';

	select 	sum(a.vl_item)
	into	vl_adic_pacote_w
	from 	ageint_itens_adicionais a, 
			ageint_proc_pacote b,
			agenda_integrada_item c
	where 	b.nr_seq_Ageint			= nr_seq_ageint_p
	and 	a.nr_seq_ageint_pacote 	= b.nr_sequencia
	and		c.nr_sequencia			= a.nr_seq_ageint_item;
	
end if;

vl_retorno_w	:= (nvl(vl_adic_pacote_w,0) + nvl(vl_adic_item_w,0));

return	vl_retorno_w;

end Ageint_Obter_Valor_Adic;
/