create or replace
function obter_dt_imp_proc_internet(	nr_prescricao_p	number,
				nr_seq_prescr_p	number
									)
return date is

dt_data_impressao_w date;

begin

select	max(dt_impressao)
into
	dt_data_impressao_w
from   	lab_prescr_proc_impressao
where  	nr_prescricao = nr_prescricao_p
and  	nr_seq_prescr = nr_seq_prescr_p
and	ie_mapa_laudo = 'I'
and	nvl(ie_tipo_imp,'I')   = 'V';	

return dt_data_impressao_w;

end obter_dt_imp_proc_internet;
/
