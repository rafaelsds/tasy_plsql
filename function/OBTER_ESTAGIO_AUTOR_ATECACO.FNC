create or replace function OBTER_ESTAGIO_AUTOR_ATECACO	(	
							nr_atendimento_p	Number,
							cd_convenio_p		number,
							cd_procedimento_p	number default null,					
							ie_origem_proced_p	number default null,
							nr_seq_proc_interno_p	number default null,
							ie_lado_p		varchar default null,
							dt_inicio_vigencia_p	date default null,
							dt_final_vigencia_p	date default null
						)
					return Varchar2 is

qt_autor_pend_w		Number(5);
ds_retorno_w		Varchar2(255) :='';
qt_autorizacao_w	Number(5);
nr_sequencia_w		autorizacao_convenio.nr_sequencia%type;
cd_convenio_w		number(10);

ds_sql_w		varchar2(4000) := '';
ds_sql_param_w		varchar2(4000);

ie_tipo_atendimento_w atendimento_paciente.ie_tipo_atendimento%type;

begin
nr_sequencia_w := 0;
If	(cd_convenio_p is null) then
	cd_convenio_w := obter_convenio_atendimento(nr_atendimento_p);
else
	cd_convenio_w := cd_convenio_p;
end if;

ie_tipo_atendimento_w := obter_tipo_atendimento(nr_atendimento_p);

-- If the procedure code is not specified, we query based on the encounter number and insurance code alone
if	cd_procedimento_p is null and nr_atendimento_p is not null then
	    ds_sql_w := ds_sql_w || 'select nr_sequencia  ';
        ds_sql_w := ds_sql_w || 'from (select a.nr_sequencia	';
        ds_sql_w := ds_sql_w || 'from	autorizacao_convenio a 		';
        ds_sql_w := ds_sql_w || 'where	a.nr_atendimento = :nr_atendimento_p ';
        ds_sql_w := ds_sql_w || 'and	a.cd_convenio    = :cd_convenio_p ';

	    ds_sql_param_w := 	''
                            ||'NR_ATENDIMENTO_P='	||	nr_atendimento_p	||';'
                            ||'CD_CONVENIO_P='	||	cd_convenio_w		;

        if 	(dt_inicio_vigencia_p is not null) then
            ds_sql_w := ds_sql_w                || ' and 	a.dt_inicio_vigencia  >= :dt_inicio_vigencia_p    	';
            ds_sql_param_w := ds_sql_param_w    ||';DT_INICIO_VIGENCIA_P='	||	dt_inicio_vigencia_p  ;
        end if;

        if	(dt_final_vigencia_p is not null) then            
            ds_sql_w 		:= ds_sql_w 		|| ' and     Nvl(a.dt_fim_vigencia,sysdate) <= fim_dia(:dt_final_vigencia_p)    	';
            ds_sql_param_w 	:= ds_sql_param_w  	||';DT_FINAL_VIGENCIA_P='	||	dt_final_vigencia_p  ;            
        end if;

        if (ie_tipo_atendimento_w = 1) then
            ds_sql_w := ds_sql_w || ' and a.ie_tipo_autorizacao in (1, 2, 5) ';
            ds_sql_w := ds_sql_w || ' and (a.ie_tipo_autorizacao <> 5  or (a.ie_tipo_autorizacao = 5 and a.qt_dia_solicitado > 0 and a.qt_dia_autorizado > 0)) ';  
        end if;

        ds_sql_w := ds_sql_w  || 'order by a.dt_inicio_vigencia desc)    	';
        ds_sql_w := ds_sql_w  || 'where rownum = 1     	';

        obter_valor_dinamico_bv(ds_sql_w, ds_sql_param_w, nr_sequencia_w);                        

-- Otherwise, if the procedure code is specified, we build the query dynamically
elsif	(cd_procedimento_p is not null) and (ie_origem_proced_p is not null) then
	ds_sql_w := ds_sql_w || 'select max(a.nr_sequencia) nr_sequencia            	';
	ds_sql_w := ds_sql_w || 'from 	autorizacao_convenio a,		            	';
	ds_sql_w := ds_sql_w || '	procedimento_autorizado b	            	';
	ds_sql_w := ds_sql_w || 'where 	a.nr_sequencia 	   = b.nr_sequencia_autor (+) 	';
	ds_sql_w := ds_sql_w || 'and	a.nr_atendimento   = :nr_atendimento_p        	';
	ds_sql_w := ds_sql_w || 'and 	a.cd_convenio 	   = :cd_convenio_p	    	';
	ds_sql_w := ds_sql_w || 'and	b.cd_procedimento  = :cd_procedimento_p	    	';
	ds_sql_w := ds_sql_w || 'and	b.ie_origem_proced = :ie_origem_proced_p    	';

	ds_sql_param_w := ''
	||'NR_ATENDIMENTO_P='	||	nr_atendimento_p	||';'
	||'CD_CONVENIO_P='	||	cd_convenio_w		||';'
	||'CD_PROCEDIMENTO_P='	||	cd_procedimento_p	||';'
	||'IE_ORIGEM_PROCED_P='	||	ie_origem_proced_p	;

	if	nr_seq_proc_interno_p is not null then
		ds_sql_w 	:= ds_sql_w 		|| ' and b.nr_seq_proc_interno = :nr_seq_proc_interno_p ';
		ds_sql_param_w 	:= ds_sql_param_w 	|| ';NR_SEQ_PROC_INTERNO_P='||nr_seq_proc_interno_p;
	end if;

	if	ie_lado_p is not null then
		ds_sql_w 	:= ds_sql_w 		|| ' and b.ie_lado = :ie_lado_p ';
		ds_sql_param_w 	:= ds_sql_param_w 	|| ';IE_LADO_P='||ie_lado_p;
	end if;	

    	if 	(dt_inicio_vigencia_p is not null) then
            ds_sql_w := ds_sql_w                || ' and 	a.dt_inicio_vigencia  >= :dt_inicio_vigencia_p    	';
            ds_sql_param_w := ds_sql_param_w    ||';DT_INICIO_VIGENCIA_P='	||	dt_inicio_vigencia_p  ;
        end if;

        if	(dt_final_vigencia_p is not null) then            
            ds_sql_w 		:= ds_sql_w 		|| ' and     Nvl(a.dt_fim_vigencia,sysdate) <= fim_dia(:dt_final_vigencia_p)    	';
            ds_sql_param_w 	:= ds_sql_param_w  	||';DT_FINAL_VIGENCIA_P='	||	dt_final_vigencia_p  ;            
        end if;	
        
        if (ie_tipo_atendimento_w = 1) then
            ds_sql_w := ds_sql_w || ' and a.ie_tipo_autorizacao in (1, 2) ';
        end if;

        ds_sql_w := ds_sql_w                || 'order by a.dt_fim_vigencia desc)    	';

	obter_valor_dinamico_bv(ds_sql_w, ds_sql_param_w, nr_sequencia_w);

end if;

if	(nr_sequencia_w > 0) then
	select	max(substr(obter_estagio_autor(nr_seq_estagio, 'D'), 1, 255))
	into	ds_retorno_w
	from	autorizacao_convenio
	where	nr_sequencia 	= nr_sequencia_w;
else
	ds_retorno_w := null;
end if;

return	ds_retorno_w;

end Obter_Estagio_Autor_atecaco;
/
