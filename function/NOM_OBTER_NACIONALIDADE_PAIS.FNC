create or replace
function nom_obter_nacionalidade_pais(cd_pais_p	varchar2) return varchar2 is

cd_nacionalidade_w	nacionalidade.cd_nacionalidade%type;

begin

if	(cd_pais_p is not null) then
	/* It is another catalog in clinical summary */
	select	max(b.cd_nacionalidade)
	into	cd_nacionalidade_w
	from	nacionalidade b,
			pais a
	where	a.sg_pais = b.cd_externo
	and		a.cd_codigo_pais = cd_pais_p;
	
end if;

return lpad(cd_nacionalidade_w,3,'0');

end nom_obter_nacionalidade_pais;
/