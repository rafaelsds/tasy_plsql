CREATE OR REPLACE
FUNCTION paf_OBTER_NOME_PF_PJ(
				CD_PESSOA_FISICA_P 	VARCHAR2,
				CD_CGC_P			Varchar2)
				RETURN VARCHAR IS

ds_retorno_w		pessoa_juridica.ds_razao_social%type;

BEGIN
ds_retorno_w		:= '';
if (CD_CGC_P	is not null) then
	begin
	select DS_RAZAO_SOCIAL
	into	 ds_retorno_w
	from	 PESSOA_JURIDICA
	where	 CD_CGC	= CD_CGC_P;
	end;
elsif	(CD_PESSOA_FISICA_P is not null) then
	begin
	select NM_PESSOA_FISICA
	into	 ds_retorno_w
	from	 PESSOA_FISICA
	where	 CD_PESSOA_FISICA	= CD_PESSOA_FISICA_P;
	end;
end if;

RETURN ds_retorno_w;
END paf_OBTER_NOME_PF_PJ;
/
