create or replace 
function FIS_GET_ORIGEM_NF_CANCELADA_MX(seq_nf_atual_p	fis_origem_nf_cancel.seq_nf_atual%type) 
return	fis_origem_nf_cancel.seq_nf_cancelada%type
is

seq_nf_origem_w			fis_origem_nf_cancel.seq_nf_cancelada%type;
seq_nf_atual_w			fis_origem_nf_cancel.seq_nf_atual%type;
cd_estabelecimento_w	fis_origem_nf_cancel.cd_estabelecimento%type;
ie_loop_w				boolean := true;
begin
	seq_nf_origem_w := null;
	seq_nf_atual_w := seq_nf_atual_p;
	
	select	nvl(cd_estabelecimento,0)
	into	cd_estabelecimento_w
	from 	nota_fiscal
	where	nr_sequencia = seq_nf_atual_p;
	
	while ie_loop_w
	loop
	   	begin
			select	seq_nf_cancelada
			into	seq_nf_origem_w
			from	fis_origem_nf_cancel
			where	seq_nf_atual = seq_nf_atual_w
			and		cd_estabelecimento = cd_estabelecimento_w
			and 	rownum = 1;
		exception
			WHEN OTHERS THEN
                ie_loop_w := false;
		end;
		
		seq_nf_atual_w := seq_nf_origem_w;

	end loop;
	
	return seq_nf_origem_w;

end FIS_GET_ORIGEM_NF_CANCELADA_MX;
/
