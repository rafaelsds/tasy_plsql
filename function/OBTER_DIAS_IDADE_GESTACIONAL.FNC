create or replace 
function obter_dias_idade_gestacional	(nr_seq_laudo_p	number,
										 ie_opcao_p		varchar2)
				return varchar2 is

qt_idade_w			number(10,0);
nr_prescricao_w		number(14,0);
dt_exame_w			date;
dt_menstruacao_w	date;


begin

/* 
	Op��o
		0 - Dias
		1 - Semana
*/

select	nvl(max(nr_prescricao),0),
		trunc(nvl(max(dt_exame),sysdate))
into 	nr_prescricao_w,
		dt_exame_w
from	laudo_paciente
where	nr_sequencia = nr_seq_laudo_p;


select	nvl(max(dt_mestruacao),sysdate)
into	dt_menstruacao_w
from	prescr_medica
where	nr_prescricao =  nr_prescricao_w;

if	(ie_opcao_p = '0') then

	select	mod(obter_dias_entre_datas(dt_menstruacao_w,dt_exame_w),7)
	into	qt_idade_w
	from	dual;

elsif (ie_opcao_p = '1') then
	
	select	trunc(obter_dias_entre_datas(dt_menstruacao_w,dt_exame_w)/7)
	into	qt_idade_w
	from	dual;

end if;
	
return	qt_idade_w;

end	obter_dias_idade_gestacional;
/
