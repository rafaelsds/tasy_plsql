create or replace
function obter_nome_leg_status_leito(ie_status_unidade_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin
	ds_retorno_w := '';
	
	if ie_status_unidade_p = 'C' then
		ds_retorno_w := 'C';
	elsif ie_status_unidade_p = 'E' then
		ds_retorno_w := 'M';
	elsif ie_status_unidade_p = 'H' then
		ds_retorno_w := 'H';
	end if;

return	ds_retorno_w;

end obter_nome_leg_status_leito;
/