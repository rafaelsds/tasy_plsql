create or replace
function cpoe_se_apresenta_dt_suspensao(dt_suspensao_p			date,
										dt_lib_suspensao_p		date)
										return date deterministic is

begin

if	(dt_lib_suspensao_p is not null) then
	return dt_suspensao_p;
end if;

return	null;

end cpoe_se_apresenta_dt_suspensao;
/
