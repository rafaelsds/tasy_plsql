create or replace 
function pls_valida_matricula_cesp (cd_matricula_p in varchar2) 
		return varchar2 is
		
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: verificar se a matricula informada da CESP � v�lida  conforme valida��o da function.

-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [X] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
Valores de retorno:
MV - Matrirula v�lida
MI - Matricula inv�lida

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */	
		
		
vl_tamanho_parametro_w  Number(3);
vl_multiplicador_w      Number(2);
vl_soma_w               Number(4);
vl_digito_w             Number(2);

vl_ultimo_digito_w	Number(1);
vl_ret_w		Number(10) := null;
cd_matricula_w		Varchar(255);

begin

if	(cd_matricula_p is not null) then

	begin
	
	vl_ultimo_digito_w := substr(cd_matricula_p,length(cd_matricula_p),1);
	cd_matricula_w :=  substr(cd_matricula_p,0,length(cd_matricula_p)-1);

	vl_soma_w := 0;
	vl_tamanho_parametro_w := length(cd_matricula_w);
	vl_multiplicador_w := 2;
	
	for x in 1..vl_tamanho_parametro_w loop
		vl_soma_w := vl_soma_w + (to_number(substr(cd_matricula_w,vl_tamanho_parametro_w-x+1,1) * vl_multiplicador_w));
		vl_multiplicador_w := vl_multiplicador_w + 1;
		if (vl_multiplicador_w = 12) then 
			vl_multiplicador_w := 2;
		end if;
	end loop;

	if  (substr(cd_matricula_w,12,2) = '00') then
		vl_ret_w := 0;
	else 
		vl_digito_w := mod(vl_soma_w, 11);
		if  ( (vl_digito_w = 0) or (vl_digito_w = 1) ) then
			vl_ret_w := 0;
		else
			vl_ret_w := 11 - vl_digito_w;
		end if ;
	end if;
	
	exception
	when others then
		return 'MI';
	end;
	
	if	(vl_ret_w is not null and vl_ret_w = vl_ultimo_digito_w) then
		return 'MV';
	end if;
end if;

return 'MI';
       
end pls_valida_matricula_cesp;
/
