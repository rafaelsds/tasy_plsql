create or replace
function HD_Obter_Se_Paciente_Ativo(	cd_pessoa_fisica_p	varchar2)
					return			varchar2 is
				
qt_registro_w		number(10,0);
ie_retorno_w		varchar2(1)	:= 'N';
qt_trat_w		number(10,0);

begin

select 	count(*)
into	qt_trat_w
from	paciente_tratamento
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

if (qt_trat_w > 0) then

	select	count(*)
	into	qt_registro_w
	from	paciente_tratamento
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_tratamento		in ('HD','CH','CHD','CHDF','CHDI','PAF','SCUF','SLED','DPA','CPI','CO','CAPD','PL','DPAV','DDC','IHF','IHDF')
	and	dt_inicio_tratamento	is not null
	and	dt_final_tratamento	is null;
end if;

if	(qt_trat_w = 0) then
		ie_retorno_w		:= 'S';
elsif	(qt_registro_w > 0) then
	ie_retorno_w		:= 'S';
end if;

return	ie_retorno_w;

end HD_Obter_Se_Paciente_Ativo;
/
