create or replace
function obter_desc_resposta_glosa(cd_resposta_p	varchar2)
	return varchar2 is

ds_resposta_w	varchar2(2000);

begin

select	nvl(max(ds_resposta),'')
into	ds_resposta_w
from	motivo_resp_glosa
where	to_char(cd_resposta) = nvl(cd_resposta_p,'-1');


return ds_resposta_w;

end obter_desc_resposta_glosa;
/
