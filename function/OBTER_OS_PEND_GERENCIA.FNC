create or replace
function Obter_OS_Pend_Gerencia
		(nr_seq_gerencia_p	number,
		dt_mes_referencia_p	date,
		ie_tipo_informacao_p	varchar2) 
		return number is

/* 'T' - Total pendentes		'P' - Por pessoas pessoas */
	

qt_total_pendente_w		number(10,0);
qt_pessoas_w			number(10,0);
dt_fim_mes_w			date;

begin

if	(trunc(sysdate, 'month') = dt_mes_referencia_p) then
	dt_fim_mes_w		 := trunc(sysdate, 'dd');
else
	dt_fim_mes_w		 := dt_mes_referencia_p;
end if;

select	sum(a.qt_total)
into	qt_total_pendente_w
from 	man_posicao_diaria b, 
	man_posicao_diaria_resumo a
where   a.nr_seq_gerencia       = nr_seq_gerencia_p
and     a.nr_seq_posicao        = b.nr_sequencia
and     b.dt_posicao            = dt_fim_mes_w		 
and     b.ie_tipo_registro 	= 1
and	nvl(a.ie_tipo,'P') 	= 'P'
and	nvl(b.ie_tipo,'P') 	= 'P';

select	Obter_Qt_Colab_Real_mes_des(nr_seq_gerencia_p, trunc(dt_fim_mes_w, 'month'))
into	qt_pessoas_w
from	dual;

if	(ie_tipo_informacao_p	= 'T') then
	return	qt_total_pendente_w;
else
	return	qt_total_pendente_w/qt_pessoas_w;
end if;

end Obter_OS_Pend_Gerencia;
/
