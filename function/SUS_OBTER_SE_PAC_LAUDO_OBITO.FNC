create or replace
function sus_obter_se_pac_laudo_obito(	nr_seq_interno_p		number ,
					nr_atendimento_p     	number ,
					nr_prontuario_p      		number ,
					cd_estabelecimento_p	number )
					return varchar2 as
					
ds_retorno_w		varchar2(15)	:= 'N';
qt_obito_motivo_w		number(3)	:= 0;
qt_obito_cadastro_w	number(3)	:= 0;

begin

if	(nvl(nr_seq_interno_p,0) > 0) then
	begin
	
	select	count(*)
	into	qt_obito_motivo_w
	from	motivo_alta b,
		atendimento_paciente a,
		sus_laudo_paciente c
	where	a.dt_alta is not null
	and	b.ie_obito = 'S'
	and	a.cd_motivo_alta 		= b.cd_motivo_alta
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.nr_atendimento		= c.nr_atendimento
	and	c.nr_seq_interno		= nr_seq_interno_p;
	
	select	count(*)
	into	qt_obito_cadastro_w
	from	pessoa_fisica a,
		atendimento_paciente b,
		sus_laudo_paciente c
	where	a.dt_obito is not null
	and	a.cd_pessoa_fisica 		= b.cd_pessoa_fisica
	and	b.nr_atendimento		= c.nr_atendimento
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	c.nr_seq_interno		= nr_seq_interno_p;
	
	if	(qt_obito_motivo_w > 0) or
		(qt_obito_cadastro_w > 0) then
		ds_retorno_w := 'S';
	end if;
		
	end;
elsif	(nvl(nr_atendimento_p,0) > 0) then
	begin
	
	select	count(*)
	into	qt_obito_motivo_w
	from	motivo_alta b,
		atendimento_paciente a
	where	a.dt_alta is not null
	and	b.ie_obito = 'S'
	and	a.cd_motivo_alta 		= b.cd_motivo_alta
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.nr_atendimento		= nr_atendimento_p;
	
	select	count(*)
	into	qt_obito_cadastro_w
	from	pessoa_fisica a,
		atendimento_paciente b
	where	a.dt_obito is not null
	and	a.cd_pessoa_fisica 		= b.cd_pessoa_fisica
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	b.nr_atendimento		= nr_atendimento_p;
	
	if	(qt_obito_motivo_w > 0) or
		(qt_obito_cadastro_w > 0) then
		ds_retorno_w := 'S';
	end if;
	
	end;
elsif	(nvl(nr_prontuario_p,0) > 0) then
	begin
	select	count(*)
	into	qt_obito_motivo_w
	from	motivo_alta b,
		pessoa_fisica c,
		atendimento_paciente a
	where	a.dt_alta is not null
	and	b.ie_obito = 'S'
	and	a.cd_motivo_alta 	= b.cd_motivo_alta
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_pessoa_fisica 	= c.cd_pessoa_fisica
	and	c.nr_prontuario 	= nr_prontuario_p;
	
	select	count(*)
	into	qt_obito_cadastro_w
	from	pessoa_fisica a,
		atendimento_paciente b
	where	a.dt_obito is not null
	and	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	a.nr_prontuario		= nr_prontuario_p;
	
	if	(qt_obito_motivo_w > 0) or
		(qt_obito_cadastro_w > 0) then
		ds_retorno_w := 'S';
	end if;
	end;	
end if;

return ds_retorno_w;

end sus_obter_se_pac_laudo_obito;
/