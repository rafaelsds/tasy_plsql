create or replace
function Verifica_existe_agrup_uni(cd_setor_atendimento_p	number,
				   cd_unidade_basica_p		varchar2,
				   cd_unidade_compl_p		varchar2)
 		    	return varchar2 is

nr_agrupamento_w	number(5);
ie_unidade_disp_w	varchar2(1);
qt_unid_agrup_hig_w	number(10);
cd_tipo_acomodacao_w	number(4);
			
begin

ie_unidade_disp_w := 'S';

if	(nvl(cd_setor_atendimento_p,0) > 0) then
	
	select	nvl(max(nr_agrupamento),0),
		max(cd_tipo_acomodacao)
	into	nr_agrupamento_w,
		cd_tipo_acomodacao_w
	from	unidade_atendimento
	where 	cd_setor_atendimento = cd_setor_atendimento_p 
	and   	cd_unidade_basica = cd_unidade_basica_p 
	and   	cd_unidade_compl =  cd_unidade_compl_p;
	
	if	(nr_agrupamento_w > 0) then
	
		select	count(*)
		into	qt_unid_agrup_hig_w
		from	unidade_atendimento
		where 	cd_setor_atendimento = cd_setor_atendimento_p 
		and	nr_agrupamento = nr_agrupamento_w
		and     cd_tipo_acomodacao <> cd_tipo_acomodacao_w
		and	ie_status_unidade = 'H';
		
		if	(qt_unid_agrup_hig_w > 0) then
		
			ie_unidade_disp_w := 'N';
		
		end if;
	end if;
	
end if;

return	ie_unidade_disp_w;

end Verifica_existe_agrup_uni;
/