create or replace
Function Obter_Se_Contido(	qt_valor_p		Number,
			ds_possib_p		Varchar2)
return varchar deterministic is

ds_comando_w			Varchar2(2000);
ds_result_w			varchar2(0001);

/*******OBSERVAR QUE O DS_POSSIB_P deve ter o formato (9,4,3) que e gerado pelo sistema de relatorios */


ds_possib_w			varchar2(6000);
qt_controle_w			number(10);
qt_pos_separador_w		number(10);
ds_possib_aux_w			varchar2(6000);

	--Criado o metodo para evitar erros ao passar valores invalidos para a function
	function obterSeNumeroIgual(	ds_param_p	varchar2) return boolean is
	begin
	
	begin
		return ( to_number(trim(ds_param_p)) = qt_valor_p );
	exception
	when others then
		null;
	end;
	
	return false;
	
	end obterSeNumeroIgual;

BEGIN

ds_possib_w := trim(ds_possib_p);

if	(instr(ds_possib_w,'(') > 0 ) and
	(instr(ds_possib_w,')') > 0 ) then
	ds_possib_w		:= substr(ds_possib_p,(instr(ds_possib_p,'(')+1),(instr(ds_possib_p,')')-2));
end if;

qt_controle_w 		:= 0;
ds_result_w		:= 'N';

qt_pos_separador_w 	:= instr(ds_possib_w,',');

if	( qt_pos_separador_w = 0 ) and
	(obterSeNumeroIgual(ds_possib_w) ) then
	ds_result_w	:= 'S';
else	
	while	( qt_pos_separador_w > 0 )  and
		( qt_controle_w < 1000 ) loop
		if	( obterSeNumeroIgual(substr(ds_possib_w,1,qt_pos_separador_w-1)) ) then
			ds_result_w	:= 'S';
			qt_controle_w	:= 1000;
		else
			ds_possib_w		:= substr(ds_possib_w,qt_pos_separador_w+1,length(ds_possib_w));
			qt_pos_separador_w 	:= instr(ds_possib_w,',');
			qt_controle_w		:= qt_controle_w + 1;
		end if;
	end loop;

	if	(ds_result_w	= 'N')  and
		( obterSeNumeroIgual(ds_possib_w)) then
		ds_result_w	:= 'S';
	end if;

end if;

return ds_result_w;

end Obter_Se_Contido;
/
