create or replace 
function obter_just_atraso_aprovacao(	nr_sequencia_p	number)
					return		varchar2 is

ie_resultado			varchar2(1);
qt_just_atraso			number(10);

begin

qt_just_atraso := 0;

select 	count(*)
into	qt_just_atraso 
from	proc_aprov_compra_just
where 	nr_seq_processo = nr_sequencia_p;

if	(qt_just_atraso > 0) then
	ie_resultado :=	'S';
elsif 	(qt_just_atraso < 1) then
	ie_resultado :=	'N';
end if;


return	ie_resultado;
end obter_just_atraso_aprovacao;
/
