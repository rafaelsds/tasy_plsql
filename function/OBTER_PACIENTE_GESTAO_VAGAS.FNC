create or replace 
function obter_paciente_gestao_vagas(	nr_seq_gestao_p		number,
					ie_codigo_nome_p 	varchar2)
					return varchar2 is

ds_retorno_w		varchar2(60)	:= null;

begin

if	(ie_codigo_nome_p = 'C') then
	begin
	select	cd_pessoa_fisica
	into	ds_retorno_w
	from	gestao_vaga
	where	nr_sequencia	=	nr_seq_gestao_p;
	exception
		when others then
			ds_retorno_w	:= null;
	end;
elsif	(ie_codigo_nome_p = 'N') then
	begin
	select	substr(obter_nome_pf(cd_pessoa_fisica),1,60)
	into	ds_retorno_w
	from	gestao_vaga
	where	nr_sequencia	=	nr_seq_gestao_p;	
	exception
		when others then		
		ds_retorno_w	:= null;			
	end;
	
	if	(ds_retorno_w is null) then
	
		select 	substr(nm_paciente,1,60)
		into	ds_retorno_w
		from	gestao_vaga
		where	nr_sequencia	=	nr_seq_gestao_p;
	
	end if;	

end if;
return	ds_retorno_w;

end obter_paciente_gestao_vagas;
/
