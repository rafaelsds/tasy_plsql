/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Verificar se deve aplicar o reajuste aos beneficiários
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_se_aplica_reaj_benef
		(	nr_seq_reajuste_p	number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(10);
qt_registros_w		number(10);

begin	

ds_retorno_w	:= 'N';

select	count(1)
into	qt_registros_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_p
and	nr_seq_contrato is null
and	nr_seq_intercambio is null;

if	(qt_registros_w > 0) then
	select  count(*)
	into	qt_registros_w
	from    pls_lote_reaj_segurado
	where   nr_seq_lote_referencia = nr_seq_reajuste_p;
	
	if	(qt_registros_w = 0) then
		ds_retorno_w	:= 'S';
	end if;
else
	select  count(*)
	into	qt_registros_w
	from    pls_lote_reaj_segurado
	where   nr_seq_reajuste = nr_seq_reajuste_p;
	
	if	(qt_registros_w = 0) then
		ds_retorno_w	:= 'S';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_aplica_reaj_benef;
/