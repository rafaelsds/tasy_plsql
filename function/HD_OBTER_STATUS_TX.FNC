create or replace
function hd_obter_status_tx(cd_pessoa_fisica_p		varchar2,
				ie_opcao_p		varchar2)
 		    	return varchar2 is

ds_status_w	varchar2(80);			
ie_status_w	varchar2(3);
ds_retorno_w	varchar2(80);
begin

if	(cd_pessoa_fisica_p is not null) then
	select	max(substr(obter_valor_dominio(5125,ie_status),1,255)),
		max(ie_status)
	into	ds_status_w,
		ie_Status_w
	from	tx_encaminhamento a
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_inativacao is null
	and	dt_encaminhamento = (	select	max(b.dt_encaminhamento)
					from	tx_encaminhamento b
					where	cd_pessoa_fisica = cd_pessoa_fisica_p
					and	dt_inativacao is null);

	if	(ie_opcao_p = 'D') then
		ds_retorno_w := ds_status_w;
	elsif	(ie_opcao_p = 'C') then
		ds_retorno_w := ie_status_w;
	end if;	
					
end if;

					
return	ds_retorno_w;

end hd_obter_status_tx;
/
