create or replace
function pls_obter_se_benef_sca
			(	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
				nr_seq_operadora_p	pls_congenere.nr_sequencia%type)
				return varchar2 is

ie_abrangencia_w	pls_plano.ie_abrangencia%type;
ie_area_coberta_w	Varchar2(1)	:= 'N';

-- Verificar se o benefici�rio possui SCA
Cursor C_sca is
	select	nr_seq_plano
	from	pls_sca_vinculo
	where	nr_seq_segurado	= nr_seq_segurado_p
	and	dt_liberacao	is not null
	and	sysdate		between	dt_inicio_vigencia and fim_dia(dt_fim_vigencia);

-- �rea de atua��o do SCA
Cursor C_area_sca(nr_seq_plano_pc	pls_plano.nr_sequencia%type) is
	select	cd_municipio_ibge,
		sg_estado,
		nr_seq_regiao
	from	pls_plano_area
	where	nr_seq_plano	= nr_seq_plano_pc;

-- �rea de atua��o da Operadora
Cursor C_area_oper is
	select	cd_municipio_ibge,
		sg_estado,
		nr_seq_regiao
	from	pls_cooperativa_area
	where	nr_seq_cooperativa	= nr_seq_operadora_p;
begin

for r_c_sca_w in C_sca loop
	begin
		select	nvl(ie_abrangencia,'X')
		into	ie_abrangencia_w
		from	pls_plano
		where	nr_sequencia	= r_c_sca_w.nr_seq_plano
		and	nvl(ie_abrangencia_sca_intercambio,'S') = 'S';
	exception
	when others then
		ie_abrangencia_w := 'X';
	end;

	if	(ie_abrangencia_w	= 'N') then
		ie_area_coberta_w	:= 'S';
	elsif	(ie_abrangencia_w	<> 'N') and (ie_abrangencia_w	<> 'X') then
		for r_c_area_sca_w in C_area_sca(r_c_sca_w.nr_seq_plano) loop
			for r_c_area_oper_w in C_area_oper loop
				if	(ie_abrangencia_w	= 'E') then
					if	(r_c_area_sca_w.sg_estado = r_c_area_oper_w.sg_estado) then
						ie_area_coberta_w	:= 'S';
						exit;
					end if;
				elsif	(ie_abrangencia_w	= 'M') then
					if	(r_c_area_sca_w.cd_municipio_ibge = r_c_area_oper_w.cd_municipio_ibge) then
						ie_area_coberta_w	:= 'S';
						exit;
					end if;
				elsif	(ie_abrangencia_w	= 'GM') then
					if	(r_c_area_sca_w.nr_seq_regiao = r_c_area_oper_w.nr_seq_regiao) then
						ie_area_coberta_w	:= 'S';
						exit;
					elsif	(pls_obter_se_mun_uf_regiao(r_c_area_sca_w.cd_municipio_ibge, null, r_c_area_oper_w.nr_seq_regiao) = 'S') then
						ie_area_coberta_w	:= 'S';
						exit;
					elsif	(pls_obter_se_mun_uf_regiao(r_c_area_oper_w.cd_municipio_ibge, null, r_c_area_sca_w.nr_seq_regiao) = 'S') then
						ie_area_coberta_w	:= 'S';
						exit;
					elsif	(pls_obter_se_municipio_em_uf(r_c_area_sca_w.cd_municipio_ibge, r_c_area_oper_w.sg_estado)	= 'S') then
						ie_area_coberta_w	:= 'S';
						exit;
					elsif	(pls_obter_se_municipio_em_uf(r_c_area_oper_w.cd_municipio_ibge, r_c_area_sca_w.sg_estado)	= 'S') then
						ie_area_coberta_w	:= 'S';
						exit;
					end if;
				end if;
			end loop;
		end loop;
	end if;
end loop;

return	ie_area_coberta_w;

end pls_obter_se_benef_sca;
/