create or replace
function Insere_Barra_Entre_Campos
			(ds_valor_campo_p	varchar2,
			 ds_caracter_p		varchar2,
			 nr_campos_p		number)
			 return varchar2 is

ds_retorno_w		varchar2(255);
ds_caracter_w		varchar2(255);
i			integer	:= 1;
j			integer	:= 1;
nr_max_campos_w		number(10) := 0;

begin

ds_retorno_w := ds_caracter_p;
ds_caracter_w	:= ds_caracter_p; 

if	(length(ds_valor_campo_p) is not null) then
	nr_max_campos_w := length(ds_valor_campo_p);
end if;

for	i in 1 .. nr_max_campos_w loop
	ds_retorno_w	:= ds_retorno_w ||' '|| substr(ds_valor_campo_p,i,1) || ' ' || ds_caracter_w;
end loop;
if	(nr_max_campos_w < nr_campos_p) then
	for	j in 1 .. (nr_campos_p - nr_max_campos_w) loop
		ds_retorno_w	:= ds_retorno_w || '    ' || ds_caracter_w;
	end loop;
end if;

return ds_retorno_w;

end Insere_Barra_Entre_Campos;
/