CREATE OR replace FUNCTION Obter_se_os_passou_desenv_data(
						nr_ordem_servico_p NUMBER,
						dt_referencia_p    DATE)

RETURN VARCHAR2
IS
  ie_retorno_w     VARCHAR2(15);
  nr_seq_estagio_w NUMBER(10);
BEGIN
    ie_retorno_w := 'N';

    SELECT Count(1)
    INTO   nr_seq_estagio_w
    FROM   man_ordem_serv_estagio a,
           man_estagio_processo b
    WHERE  a.nr_seq_estagio = b.nr_sequencia
           AND a.nr_seq_ordem = nr_ordem_servico_p
           AND Trunc(a.dt_atualizacao) = Trunc(dt_referencia_p)
           AND (( Nvl(b.ie_desenv, 'N') = 'S'
                   OR Nvl(b.ie_tecnologia, 'N') = 'S'
                   OR Nvl(b.ie_design, 'N') = 'S' ));

    IF nr_seq_estagio_w > 0 THEN
      ie_retorno_w := 'S';
    END IF;

    RETURN ie_retorno_w;

END obter_se_os_passou_desenv_data;
/