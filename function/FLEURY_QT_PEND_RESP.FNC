create or replace
function fleury_qt_pend_resp(	nr_prescricao_p		number)
 		    	return number is

qt_itens_w	number(10) := 0;

begin

if	(nr_prescricao_p is not null) then
	
	select	count(*)
	into	qt_itens_w
	from	prescr_procedimento r
	where	r.nr_prescricao = nr_prescricao_p
	and		((obter_se_int_prescr_fleury(r.nr_prescricao) is not null) or
			(obter_se_int_pr_fleury_diag(r.nr_prescricao) is not null))
	and     not exists (select 	1
						from 	fleury_prescr_perg_tec v, fleury_prescr_perg_resp u
						where 	v.nr_prescricao = r.nr_prescricao
						and 	v.nr_seq_prescr = r.nr_sequencia
						and     v.nr_sequencia = u.nr_seq_perg_tec);
end if;

return	qt_itens_w;

end fleury_qt_pend_resp;
/