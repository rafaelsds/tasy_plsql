create or replace
function pls_obter_setor_atendimento
			(	cd_setor_atend_p	Number)
				return varchar2 is
				
ds_retorno_w			varchar2(255);

begin

select  ds_setor_atendimento
into	ds_retorno_w
from    setor_atendimento
where	cd_setor_atendimento	= cd_setor_atend_p;

return	ds_retorno_w;

end pls_obter_setor_atendimento;
/