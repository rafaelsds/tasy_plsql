create or replace
function cpoe_obter_data_final_etapa(
					dt_horario_p	date,
					ds_duracao_p	varchar2) return date is 
					
dt_retorno_w	date;
qt_horas_w		number(18,6);
nr_horas_w		varchar2(3);
nr_minutos_w	varchar2(3);
begin

if	(ds_duracao_p is not null) and
	(instr(ds_duracao_p,':') > 0) and
	(ds_duracao_p <> '  :  ') and 
	(ds_duracao_p <> '__:__') then
	nr_horas_w		:= substr(ds_duracao_p, 1, instr(ds_duracao_p,':')-1);
	nr_minutos_w	:= substr(ds_duracao_p,instr(ds_duracao_p,':') + 1, length(ds_duracao_p)); 
	qt_horas_w := to_number(nr_horas_w) + dividir_sem_round(nvl(to_number(nr_minutos_w),0), 60);
else 
	qt_horas_w  := 0;
end if;


if (qt_horas_w > 0) then
	dt_retorno_w := dt_horario_p + dividir_sem_round(qt_horas_w, 24);
end if;

return dt_retorno_w;

end cpoe_obter_data_final_etapa;
/