CREATE OR REPLACE
FUNCTION Obter_Exames_Presc_Pixeon(	
					nr_prescricao_p		Number,
					nr_acesso_dicom_p		varchar2)
					RETURN Varchar2 IS

ds_procedimento_w 			PROCEDIMENTO.DS_PROCEDIMENTO%type;
Resultado_w	 			Varchar2(2000);
ds_quebra_w				Varchar2(10) 	:= '';

cursor  c01 is
	select substr(obter_desc_prescr_proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,240)
	from	prescr_procedimento
	where	nr_prescricao		= nr_prescricao_p
	and	nr_acesso_dicom	= nr_acesso_dicom_p
	order by nr_sequencia;
BEGIN

OPEN C01;
LOOP
FETCH C01 into ds_procedimento_w;
EXIT WHEN C01%NOTFOUND;
	Resultado_w	:= substr(Resultado_w || ds_quebra_w || ds_procedimento_w,1,2000);
	ds_quebra_w	:= ' + ';
END LOOP;

RETURN resultado_w;

END Obter_Exames_Presc_Pixeon;
/
