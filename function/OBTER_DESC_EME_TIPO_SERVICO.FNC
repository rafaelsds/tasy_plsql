CREATE OR REPLACE
FUNCTION Obter_desc_eme_tipo_servico(nr_sequencia_p	Number)
			RETURN VARCHAR2 IS
				
ds_retorno_w		Varchar2(100);

BEGIN

select	ds_tipo_servico
into	ds_retorno_w
from	eme_tipo_servico 
where	nr_sequencia     = nr_sequencia_p;
	
RETURN	ds_retorno_w;

END Obter_desc_eme_tipo_servico;
/
