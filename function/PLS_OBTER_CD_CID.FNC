create or replace
function pls_obter_cd_cid( id_cid_p		varchar2) return varchar2 is

ds_retorno_w	varchar2(50):= null;

begin

	select 	max(cd_cid)
	into	ds_retorno_w
	from	pls_cad_cid_monitoramento
	where	nr_id_cid_10 = id_cid_p;

return	ds_retorno_w;

end pls_obter_cd_cid;
/
