create or replace FUNCTION Obter_Se_Tem_Observacao(nr_seq_paciente_p number)

return varchar2 is


retorno_w varchar2(1) := 'N';

ie_obrigatorio_w VARCHAR2(30);

begin

    ie_obrigatorio_w := Obter_Regra_Atributo(cd_estabelecimento_p => wheb_usuario_pck.get_cd_estabelecimento,
					cd_perfil_p => wheb_usuario_pck.get_cd_perfil,
					nm_usuario_p => wheb_usuario_pck.get_nm_usuario,
					nm_tabela_p => 'PACIENTE_PROTOCOLO_MEDIC',
					nm_atributo_p => 'DS_OBSERVACAO',
					ie_regra_p => 'O',
					ds_regra_p => null,
					nr_seq_regra_p => 0,
					nr_seq_visao_p => 0,
					cd_funcao_p => 281);

    if 	(nr_seq_paciente_p is not null) and
        (ie_obrigatorio_w = 'S') then

        select  nvl(max('S'), 'N')
        into    retorno_w
        from    paciente_protocolo_medic
        where   nr_seq_paciente = nr_seq_paciente_p
        and     ds_observacao is null;
    end if;

    return retorno_w;

end Obter_Se_Tem_Observacao;
/