create or replace
function hem_obter_segmentos_concat(nr_seq_coron_p		number)
 		    	return Varchar2 is

ds_retorno_w		varchar2(2000) := '';
cd_segmento_w		hem_segmento_princ.cd_segmento%type;

Cursor C01 is
	SELECT	b.cd_segmento
	FROM	hem_coron_localizacao a,
		hem_segmento_princ b
	WHERE	a.nr_seq_coron =  nr_seq_coron_p
	AND	b.nr_sequencia = a.nr_seq_segmento;
			
begin

open C01;
loop
fetch C01 into	
	cd_segmento_w;
exit when C01%notfound;
	begin
	
	ds_retorno_w := ds_retorno_w||cd_segmento_w||'-';
	
	end;
end loop;
close C01;

if	(ds_retorno_w is not null) then
	ds_retorno_w := substr(ds_retorno_w,1,length(ds_retorno_w)-1);
end if;

return	ds_retorno_w;

end hem_obter_segmentos_concat;
/