create or replace
function obter_dados_agenda_autor
		(nr_seq_agenda_p	number,
		ie_opcao_p		varchar2)
		return			varchar2 is

/*ie_opcao_p
S - sala
U - codigo do usuario no convenio
O - Observacao
C - Carater Cirurgia
*/

ds_retorno_w		varchar2(4000);

begin

if	(ie_opcao_p is not null) then
	if 	(ie_opcao_p = 'S') then
		select	a.ds_agenda
		into	ds_retorno_w
		from	agenda a,
			agenda_paciente b
		where	a.cd_agenda	= b.cd_agenda
		and 	b.nr_sequencia 	= nr_seq_agenda_p;
	elsif 	(ie_opcao_p = 'U') then
		select 	max(b.cd_usuario_convenio)
		into	ds_retorno_w
		from 	agenda_paciente b
		where 	b.nr_sequencia = nr_seq_agenda_p;
	elsif	(ie_opcao_p = 'O') then
		select 	max(b.ds_observacao)
		into	ds_retorno_w
		from 	agenda_paciente b
		where 	b.nr_sequencia = nr_seq_agenda_p;
	elsif	(ie_opcao_p = 'C') then
		select	ie_carater_cirurgia
		into	ds_retorno_w
		from	agenda_paciente
		where 	nr_sequencia = nr_seq_agenda_p;
	end if;
end if;

return	ds_retorno_w;

end obter_dados_agenda_autor;
/
