create or replace
function pls_obter_dados_rede_atend
				(	nr_seq_rede_atend_p		number,
					ie_opcao_p			varchar2)
				return varchar2 is

/*	ie_opcao_p:
	DS = Descri��o
	OB = Observa��o
	CR = C�digo rede
*/
				
ds_retorno_w		varchar2(4000);
ds_observacao_w		varchar2(4000);

begin


if	(ie_opcao_p = 'DS') then
	select	max(ds_rede)
	into	ds_retorno_w
	from	pls_rede_atendimento
	where	nr_sequencia	= nr_seq_rede_atend_p;
elsif	(ie_opcao_p = 'OB') then
	select	max(ds_observacao)
	into	ds_retorno_w
	from	pls_rede_atendimento
	where	nr_sequencia	= nr_seq_rede_atend_p;
elsif	(ie_opcao_p = 'CR') then
	select	max(cd_rede)
	into	ds_retorno_w
	from	pls_rede_atendimento
	where	nr_sequencia	= nr_seq_rede_atend_p;
else
	ds_retorno_w	:= 'Op��o incorreta!';
end if;

return	ds_retorno_w;

end pls_obter_dados_rede_atend;
/
