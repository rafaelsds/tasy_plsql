CREATE OR REPLACE FUNCTION haoc_obter_dados_pagto_escrit(	nr_seq_escrit_p	number,
								ie_opcao_p	varchar2)
								RETURN		VarChar2 IS

/*	ie_opcao_p

'QTRI'		Quantidade de registros tipo 0, 1, 3, 5 e 9 - Ita�
'QTRC'		Quantidade de registros tipo 1, 3 e 5 - Caixa
'QTDOCC'	Quantidade de registros tipo 1, 3 e 5 (segmentos A e B) - Caixa
'QTBLQC'	Quantidade de registros tipo 1, 3 e 5 (bloqueto) - Caixa
'QTLS'		Quantidade de lotes Santander
'QTLI'		Quantidade de lotes Ita�

*/

qt_registro_doc_w	number(10)	:= 0;
qt_doc_nao_itau_w	number(10)	:= 0;
qt_registro_blq_w	number(10)	:= 0;
qt_blq_nao_itau_w	number(10)	:= 0;
qt_ted_mesmo_ti_w	number(10)	:= 0;
qt_ted_outro_ti_w	number(10)	:= 0;
qt_cc_mesmo_tit_w	number(10)	:= 0;
qt_cc_outro_tit_w	number(10)	:= 0;
qt_pag_conc_w		number(10)	:= 0;
ds_retorno_w		varchar2(255);

begin

if	(ie_opcao_p = 'QTRI') or (ie_opcao_p = 'QTRI2') then

	/* registro documento ita� */
	select	count(*)
	into	qt_registro_doc_w
	from	titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'DOC'
	and	c.cd_banco		= 341
	and	c.nr_titulo		= d.nr_titulo;

	if	(qt_registro_doc_w > 0) then	/* somar o header e o trailer de lote */
		qt_registro_doc_w	:= qt_registro_doc_w + 2;
	end if;

	/* registro documento n�o ita� */
	select	count(*)
	into	qt_doc_nao_itau_w
	from	titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'DOC'
	and	c.cd_banco		<> 341
	and	c.nr_titulo		= d.nr_titulo;

	if	(qt_doc_nao_itau_w > 0) then	/* somar o header e o trailer de lote */
		qt_doc_nao_itau_w	:= qt_doc_nao_itau_w + 2;
	end if;

	/* registro bloqueto ita� */
	select	count(*)
	into	qt_registro_blq_w
	from	titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'BLQ'
	and	c.nr_titulo		= d.nr_titulo
	and	'341'			= substr(d.nr_bloqueto, 1,3);

	if	(qt_registro_blq_w > 0) then	/* somar o header e o trailer de lote */
		qt_registro_blq_w	:= qt_registro_blq_w + 2;
	end if;

	/* registro bloqueto n�o ita� */
	select	count(*)
	into	qt_blq_nao_itau_w
	from	titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'BLQ'
	and	c.nr_titulo		= d.nr_titulo
	and	(d.nr_bloqueto is null or '341' <> substr(d.nr_bloqueto, 1,3));

	if	(qt_blq_nao_itau_w > 0) then	/* somar o header e o trailer de lote */
		qt_blq_nao_itau_w	:= qt_blq_nao_itau_w + 2;
	end if;

	/* registro ted mesmo titular */
	select	count(*)
	into	qt_ted_mesmo_ti_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'TED'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento
	and	f.cd_cgc		= d.cd_favorecido;

	if	(qt_ted_mesmo_ti_w > 0) then	/* somar o header e o trailer de lote */
		qt_ted_mesmo_ti_w	:= qt_ted_mesmo_ti_w + 2;
	end if;

	/* registro ted outro titular */
	select	count(*)
	into	qt_ted_outro_ti_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'TED'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento
	and	f.cd_cgc		<> d.cd_favorecido;

	if	(qt_ted_outro_ti_w > 0) then	/* somar o header e o trailer de lote */
		qt_ted_outro_ti_w	:= qt_ted_outro_ti_w + 2;
	end if;

	/* registro conta corrente mesmo titular */
	select	count(*)
	into	qt_cc_mesmo_tit_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'CC'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento
	and	f.cd_cgc		= d.cd_favorecido;

	if	(qt_cc_mesmo_tit_w > 0) then	/* somar o header e o trailer de lote */
		qt_cc_mesmo_tit_w	:= qt_cc_mesmo_tit_w + 2;
	end if;

	/* registro conta corrente outro titular */
	select	count(*)
	into	qt_cc_outro_tit_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'CC'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento
	and	f.cd_cgc		<> d.cd_favorecido;

	if	(qt_cc_outro_tit_w > 0) then	/* somar o header e o trailer de lote */
		qt_cc_outro_tit_w	:= qt_cc_outro_tit_w + 2;
	end if;

	/* registro pagamento de concession�ria */
	select	count(*)
	into	qt_pag_conc_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'PC'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento;

	if	(qt_pag_conc_w > 0) then	/* somar o header e o trailer de lote */
		qt_pag_conc_w	:= qt_pag_conc_w + 2;
	end if;

	/* somar o header e o trailer de arquivo*/
	ds_retorno_w	:= to_char(	nvl(qt_registro_doc_w,0) + nvl(qt_doc_nao_itau_w,0) + nvl(qt_registro_blq_w,0) +
					nvl(qt_blq_nao_itau_w,0) + nvl(qt_ted_mesmo_ti_w,0) + nvl(qt_ted_outro_ti_w,0) +
					nvl(qt_cc_mesmo_tit_w,0) + nvl(qt_cc_outro_tit_w,0) + nvl(qt_pag_conc_w,0) + 2);

elsif	(ie_opcao_p = 'QTRC') then

	/* registro documento - segmento A */
	select	count(*)
	into	qt_registro_doc_w
	from	banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_escrit c
	where	a.nr_sequencia		= c.nr_seq_escrit
	and 	b.ie_tipo_relacao	in ('EP','ECC')
	and	b.nr_sequencia		= a.nr_seq_conta_banco
	and 	c.ie_tipo_pagamento	in ('DOC','TED','CC','CCP','OP','PC')
	and	a.nr_sequencia		= nr_seq_escrit_p;

	if	(qt_registro_doc_w > 0) then	/* somar os registros do segmento B, e o header e o trailer de lote */
		qt_registro_doc_w	:= (qt_registro_doc_w * 2) + 2;
	end if;

	select	count(*)
	into	qt_registro_blq_w
	from	banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_escrit c
	where	a.nr_sequencia          	= c.nr_seq_escrit
	and	b.ie_tipo_relacao       	in ('EP','ECC')
	and	a.nr_seq_conta_banco		= b.nr_sequencia
	and 	c.ie_tipo_pagamento		= 'BLQ'
	and	a.nr_sequencia			= nr_seq_escrit_p;

	if	(qt_registro_blq_w > 0) then	/* somar o header e o trailer de lote */
		qt_registro_blq_w	:= qt_registro_blq_w + 2;
	end if;

	ds_retorno_w	:= to_char(nvl(qt_registro_doc_w,0) + nvl(qt_registro_blq_w,0));

elsif	(ie_opcao_p = 'QTDOCC') then

	/* registro documento - segmento A */
	select	count(*)
	into	qt_registro_doc_w
	from	banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_escrit c
	where	a.nr_sequencia		= c.nr_seq_escrit
	and 	b.ie_tipo_relacao	in ('EP','ECC')
	and	b.nr_sequencia		= a.nr_seq_conta_banco
	and 	c.ie_tipo_pagamento	in ('DOC','TED','CC','CCP','OP','PC')
	and	a.nr_sequencia		= nr_seq_escrit_p;

	if	(qt_registro_doc_w > 0) then	/* somar os registros do segmento B, e o header e o trailer de lote */
		qt_registro_doc_w	:= (qt_registro_doc_w * 2) + 2;
	end if;

	ds_retorno_w	:= to_char(qt_registro_doc_w);

elsif	(ie_opcao_p = 'QTBLQC') then

	select	count(*)
	into	qt_registro_blq_w
	from	banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_escrit c
	where	a.nr_sequencia          	= c.nr_seq_escrit
	and	b.ie_tipo_relacao       	in ('EP','ECC')
	and	a.nr_seq_conta_banco		= b.nr_sequencia
	and 	c.ie_tipo_pagamento		= 'BLQ'
	and	a.nr_sequencia			= nr_seq_escrit_p;

	if	(qt_registro_blq_w > 0) then	/* somar o header e o trailer de lote */
		qt_registro_blq_w	:= qt_registro_blq_w + 2;
	end if;

	ds_retorno_w	:= to_char(qt_registro_blq_w);

elsif	(ie_opcao_p = 'QTLS') then

	select	nvl(max(1),0)
	into	qt_registro_doc_w
	from	banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_escrit c
	where	a.nr_sequencia		= c.nr_seq_escrit
	and 	b.ie_tipo_relacao	in ('EP','ECC')
	and	b.nr_sequencia		= a.nr_seq_conta_banco
	and 	c.ie_tipo_pagamento	in ('DOC','TED','CC','CCP','OP','PC')
	and	a.nr_sequencia		= nr_seq_escrit_p;

	select	nvl(max(1),0)
	into	qt_registro_blq_w
	from	banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_escrit c
	where	a.nr_sequencia          	= c.nr_seq_escrit
	and	b.ie_tipo_relacao       	in ('EP','ECC')
	and	a.nr_seq_conta_banco		= b.nr_sequencia
	and 	c.ie_tipo_pagamento		= 'BLQ'
	and	a.nr_sequencia			= nr_seq_escrit_p;

	ds_retorno_w	:= to_char((nvl(qt_registro_doc_w,0) + nvl(qt_registro_blq_w,0)));

elsif	(ie_opcao_p = 'QTLI') then

	/* registro documento ita� */
	select	nvl(max(1),0)
	into	qt_registro_doc_w
	from	titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'DOC'
	and	c.cd_banco		= 341
	and	c.nr_titulo		= d.nr_titulo;

	/* registro documento n�o ita� */
	select	nvl(max(1),0)
	into	qt_doc_nao_itau_w
	from	titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'DOC'
	and	c.cd_banco		<> 341
	and	c.nr_titulo		= d.nr_titulo;

	/* registro bloqueto ita� */
	select	nvl(max(1),0)
	into	qt_registro_blq_w
	from	titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'BLQ'
	and	c.nr_titulo		= d.nr_titulo
	and	'341'			= substr(d.nr_bloqueto, 1,3);

	/* registro bloqueto n�o ita� */
	select	nvl(max(1),0)
	into	qt_blq_nao_itau_w
	from	titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'BLQ'
	and	c.nr_titulo		= d.nr_titulo
	and	(d.nr_bloqueto is null or '341' <> substr(d.nr_bloqueto, 1,3));

	/* registro ted mesmo titular */
	select	nvl(max(1),0)
	into	qt_ted_mesmo_ti_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'TED'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento
	and	f.cd_cgc		= d.cd_favorecido;

	/* registro ted outro titular */
	select	nvl(max(1),0)
	into	qt_ted_outro_ti_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'TED'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento
	and	f.cd_cgc		<> d.cd_favorecido;

	/* registro conta corrente mesmo titular */
	select	nvl(max(1),0)
	into	qt_cc_mesmo_tit_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'CC'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento
	and	f.cd_cgc		= d.cd_favorecido;

	/* registro conta corrente outro titular */
	select	nvl(max(1),0)
	into	qt_cc_outro_tit_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'CC'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento
	and	f.cd_cgc		<> d.cd_favorecido;

	/* registro conta corrente outro titular */
	select	nvl(max(1),0)
	into	qt_pag_conc_w
	from	estabelecimento f,
		titulo_pagar e,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c,
		banco_estabelecimento b,
		banco_escritural a
	where	a.nr_sequencia		= nr_seq_escrit_p
	and	a.nr_seq_conta_banco	= b.nr_sequencia
	and	b.ie_tipo_relacao	in ('EP','ECC')
	and	a.nr_sequencia		= c.nr_seq_escrit
	and 	c.ie_tipo_pagamento	= 'PC'
	and	c.nr_titulo		= d.nr_titulo
	and	d.nr_titulo		= e.nr_titulo
	and	e.cd_estabelecimento	= f.cd_estabelecimento;

	ds_retorno_w	:= to_char(	nvl(qt_cc_outro_tit_w,0) + nvl(qt_cc_mesmo_tit_w,0) + nvl(qt_ted_outro_ti_w,0) +
					nvl(qt_ted_mesmo_ti_w,0) + nvl(qt_blq_nao_itau_w,0) + nvl(qt_registro_blq_w,0) +
					nvl(qt_doc_nao_itau_w,0) + nvl(qt_registro_doc_w,0) + nvl(qt_pag_conc_w,0));

end if;

return	ds_retorno_w;

END haoc_obter_dados_pagto_escrit;
/