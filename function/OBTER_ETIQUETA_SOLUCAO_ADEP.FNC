create or replace
function obter_etiqueta_solucao_adep(	ie_etiqueta_p		varchar2,
					nr_seq_processo_p	number,
					nr_seq_solucao_p	number,
					nr_etapa_p		number)
 		    	return varchar2 is
			
nr_etiqueta_w		varchar2(50);			

begin

if 	('P' = ie_etiqueta_p) then 
	SELECT  nr_prescricao || nr_seq_solucao || nr_etapa nr_etiqueta
	into	nr_etiqueta_w
	FROM    adep_processo
	WHERE   nr_sequencia   		= nr_seq_processo_p
	and	nr_seq_solucao		= nr_seq_solucao_p
	and	nr_etapa		= nr_etapa_p;
elsif	('E' = ie_etiqueta_p) then 
	SELECT  SUBSTR(obter_codigo_barras_ordem_frac(b.nr_sequencia),1,14) nr_etiqueta
	into	nr_etiqueta_w
	FROM    adep_processo_frac b,
		adep_processo a
	WHERE   b.nr_seq_processo = a.nr_sequencia
	AND     a.nr_sequencia    = nr_seq_processo_p
	and	nr_seq_solucao	  = nr_seq_solucao_p
	and	nr_etapa	  = nr_etapa_p;
elsif	('A' = ie_etiqueta_p) then
	SELECT  max(SUBSTR(obter_codigo_barras_ordem_frac(b.nr_sequencia),1,14)) nr_etiqueta
		into	nr_etiqueta_w
		FROM    adep_processo_frac b,
			adep_processo a
		WHERE   b.nr_seq_processo = a.nr_sequencia
		AND     a.nr_sequencia    = nr_seq_processo_p
		and	nr_seq_solucao	  = nr_seq_solucao_p
		and	nr_etapa	  = nr_etapa_p;
	
	if	(nr_etiqueta_w is null) then
		SELECT  max(nr_prescricao || nr_seq_solucao || nr_etapa) nr_etiqueta
		into	nr_etiqueta_w
		FROM    adep_processo
		WHERE   nr_sequencia   		= nr_seq_processo_p
		and	nr_seq_solucao		= nr_seq_solucao_p
		and	nr_etapa		= nr_etapa_p;
	end if;
	
else
	nr_etiqueta_w := 0;
end if;


return	nr_etiqueta_w;

end obter_etiqueta_solucao_adep;
/