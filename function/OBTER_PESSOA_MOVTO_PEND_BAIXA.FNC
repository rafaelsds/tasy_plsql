create or replace
function obter_pessoa_movto_pend_baixa(	nr_seq_movto_baixa_p	number,
					ie_tipo_dado_p		varchar2,
					ie_opcao_p		varchar2)
 		    			return			varchar2 is

/*	ie_tipo_dado_p

'C'	- C�digo
'N'	- Nome
'CF'	- C�digo pessoa f�sica
'CJ'	- C�digo pessoa jur�dica
'NF'	- Nome pessoa f�sica
'NJ'	- Nome pessoa jur�dica

	ie_opcao_p

'GE'	- Geral

'AD'	- Adiantamento
'BR'	- Border� de Recebimento
'CH'	- Cheque
'Rc'	- Recebimento de Conv�nio
'TR'	- T�tulo a Receber
*/

cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
nr_titulo_w			number(10);
nr_adiantamento_w		number(10);
nr_seq_cheque_w			number(10);
nr_bordero_rec_w		number(10);
nr_seq_conv_receb_w		number(10);

ds_retorno_w			varchar2(255);

begin

select	a.nr_adiantamento,
	a.nr_bordero_rec,
	a.nr_seq_cheque,
	a.nr_seq_conv_receb,
	a.nr_titulo
into	nr_adiantamento_w,
	nr_bordero_rec_w,
	nr_seq_cheque_w,
	nr_seq_conv_receb_w,
	nr_titulo_w
from	movto_banco_pend_baixa a
where	a.nr_sequencia	= nr_seq_movto_baixa_p;

if	(ie_opcao_p = 'AD') or ((nvl(ie_opcao_p,'GE') = 'GE') and (nvl(cd_pessoa_fisica_w,0) = 0) and (nvl(cd_cgc_w,0) = 0)) then
	select	max(a.cd_pessoa_fisica),
		max(a.cd_cgc)
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	adiantamento a
	where	a.nr_adiantamento	= nr_adiantamento_w;
end if;

if	(ie_opcao_p = 'BR') or ((nvl(ie_opcao_p,'GE') = 'GE') and (nvl(cd_pessoa_fisica_w,0) = 0) and (nvl(cd_cgc_w,0) = 0)) then
	select	max(a.cd_pessoa_pagamento),
		max(a.cd_cgc_pagamento)
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	bordero_recebimento a
	where	a.nr_bordero		= nr_bordero_rec_w;
end if;

if	(ie_opcao_p = 'CH') or ((nvl(ie_opcao_p,'GE') = 'GE') and (nvl(cd_pessoa_fisica_w,0) = 0) and (nvl(cd_cgc_w,0) = 0)) then
	select	max(a.cd_pessoa_fisica),
		max(a.cd_cgc)
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	cheque_cr a
	where	a.nr_seq_cheque		= nr_seq_cheque_w;
end if;

if	(ie_opcao_p = 'RC') or ((nvl(ie_opcao_p,'GE') = 'GE') and (nvl(cd_pessoa_fisica_w,0) = 0) and (nvl(cd_cgc_w,0) = 0)) then
	select	null,
		max(b.cd_cgc)
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	convenio b,
		convenio_receb a
	where	a.nr_sequencia		= nr_seq_conv_receb_w
	and	a.cd_convenio		= b.cd_convenio;
end if;

if	(ie_opcao_p = 'TR') or ((nvl(ie_opcao_p,'GE') = 'GE') and (nvl(cd_pessoa_fisica_w,0) = 0) and (nvl(cd_cgc_w,0) = 0)) then
	select	max(a.cd_pessoa_fisica),
		max(a.cd_cgc)
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	titulo_receber a
	where	a.nr_titulo		= nr_titulo_w;
end if;

if	(ie_tipo_dado_p = 'C') then
	ds_retorno_w	:= nvl(cd_pessoa_fisica_w, cd_cgc_w);
elsif	(ie_tipo_dado_p = 'CF') then
	ds_retorno_w	:= cd_pessoa_fisica_w;
elsif	(ie_tipo_dado_p = 'CJ') then
	ds_retorno_w	:= cd_cgc_w;
elsif	(ie_tipo_dado_p = 'N') then
	ds_retorno_w	:= substr(obter_nome_pf_pj(cd_pessoa_fisica_w, cd_cgc_w),1,60);
elsif	(ie_tipo_dado_p = 'NF') then
	ds_retorno_w	:= substr(obter_nome_pf_pj(cd_pessoa_fisica_w, null),1,60);
elsif	(ie_tipo_dado_p = 'NJ') then
	ds_retorno_w	:= substr(obter_nome_pf_pj(null, cd_cgc_w),1,60);
end if;

return	ds_retorno_w;

end obter_pessoa_movto_pend_baixa;
/