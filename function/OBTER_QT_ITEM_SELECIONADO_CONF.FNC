create or replace
function obter_qt_item_selecionado_conf(nr_ordem_compra_w	number)
 		    	return number is

qt_selecionados_w	number(10);
			
begin

select	count(*)
into	qt_selecionados_w
from	ordem_compra_item_conf
where	nr_ordem_compra = nr_ordem_compra_w
and	dt_liberacao is null
and	dt_estorno is null
and	dt_selecao is not null;

return	qt_selecionados_w;

end obter_qt_item_selecionado_conf;
/