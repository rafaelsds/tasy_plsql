create or replace
function PLS_OBTER_DADOS_PAG_ITEM
		(nr_seq_pagamento_item_p	number,
		 ie_opcao_p			varchar2) return varchar2 is

/*
ie_opcao_p	
	'LP' - N�mero do lote de pagamento
*/

nr_seq_pagamento_w		number(10,0);
ds_retorno_w			varchar2(255) := '';

begin

select	max(nr_seq_pagamento)
into	nr_seq_pagamento_w
from	pls_pagamento_item
where	nr_sequencia	= nr_seq_pagamento_item_p;

if	(ie_opcao_p	= 'LP') then
	select	max(nr_seq_lote)
	into	ds_retorno_w
	from	pls_pagamento_prestador
	where	nr_sequencia	= nr_seq_pagamento_w;
end if;

return	ds_retorno_w;

end PLS_OBTER_DADOS_PAG_ITEM;
/
