create or replace
function pls_obter_valor_idade_saldo
			(	nr_seq_periodo_p		number,
				ie_idade_saldo_ativo_p		number,
				ie_idade_saldo_passivo_p	number,
				cd_conta_contabil_p		Varchar2,
				ie_opcao_p			varchar2)
				return number is

vl_retorno_w			number(15,2);
dt_periodo_inicial_w		date;
dt_periodo_final_w		date;

begin

if	(ie_opcao_p	= 'C') then
	select	dt_periodo_inicial,
		dt_periodo_final
	into	dt_periodo_inicial_w,
		dt_periodo_final_w
	from	diops_periodo
	where	nr_sequencia	= nr_seq_periodo_p;
	
	if	(nvl(ie_idade_saldo_ativo_p,0) <> 0) then
		select	sum(decode(trunc(d.dt_referencia,'month'), trunc(dt_periodo_final_w,'month'), c.vl_saldo, 0))
		into	vl_retorno_w
		from	diops_transacao			e,
			ctb_mes_ref			d,
			ctb_balancete_v			c,
			diops_conta_idade_saldo		b,
			diops_idade_saldo		a
		where	a.nr_sequencia			= b.nr_seq_idade_saldo
		and	b.cd_conta_contabil		= c.cd_conta_contabil
		and	c.nr_seq_mes_ref		= d.nr_sequencia
		and	c.ie_normal_encerramento	= 'N'
		and	a.ie_idade_saldo_ativo		= ie_idade_saldo_ativo_p
		and	c.cd_conta_contabil		in (	select	z.cd_conta_contabil	
								from  	w_diops_fin_idadesaldo_ati z
								where  	nr_seq_periodo 		= nr_seq_periodo_p
								and	ie_idade_saldo_ativo	= ie_idade_saldo_ativo_p)	
		and	a.nr_seq_transacao		= e.nr_sequencia
		and	e.dt_vigencia			= (	select	max(x.dt_vigencia)
								from	diops_transacao x
								where	x.ie_tipo_transacao	= e.ie_tipo_transacao
								and	x.cd_estabelecimento	= e.cd_estabelecimento
								and	x.dt_vigencia <= dt_periodo_inicial_w)
		and	d.dt_referencia between dt_periodo_inicial_w and dt_periodo_final_w;
	elsif	(nvl(ie_idade_saldo_passivo_p,0) <> 0) then
		select	sum(decode(trunc(d.dt_referencia,'month'), trunc(dt_periodo_final_w,'month'), c.vl_saldo, 0))
		into	vl_retorno_w
		from	diops_transacao			e,
			ctb_mes_ref			d,
			ctb_balancete_v			c,
			diops_conta_idade_saldo		b,
			diops_idade_saldo		a
		where	a.nr_sequencia			= b.nr_seq_idade_saldo
		and	b.cd_conta_contabil		= c.cd_conta_contabil
		and	c.nr_seq_mes_ref		= d.nr_sequencia
		and	c.ie_normal_encerramento	= 'N'
		and	a.ie_idade_saldo_passivo	= ie_idade_saldo_passivo_p
		and	a.nr_seq_transacao		= e.nr_sequencia
		and	c.cd_conta_contabil		in (	select	z.cd_conta_contabil	
								from  	w_diops_fin_idadesaldo_pas z
								where  	nr_seq_periodo 		= nr_seq_periodo_p
								and	ie_idade_saldo_passivo	= ie_idade_saldo_passivo_p)		
		and	e.dt_vigencia			= (	select	max(x.dt_vigencia)
								from	diops_transacao x
								where	x.ie_tipo_transacao	= e.ie_tipo_transacao
								and	x.cd_estabelecimento	= e.cd_estabelecimento
								and	x.dt_vigencia <= dt_periodo_inicial_w)
		and	d.dt_referencia between dt_periodo_inicial_w and dt_periodo_final_w;
	elsif	(cd_conta_contabil_p is not null) then
		select	sum(decode(trunc(d.dt_referencia,'month'), trunc(dt_periodo_final_w,'month'), c.vl_saldo, 0))
		into	vl_retorno_w
		from	ctb_mes_ref			d,
			ctb_balancete_v			c
		where	c.nr_seq_mes_ref	= d.nr_sequencia
		and	c.ie_normal_encerramento	= 'N'
		and	c.cd_conta_contabil		= cd_conta_contabil_p
		and	d.dt_referencia between dt_periodo_inicial_w and dt_periodo_final_w;
	end if;
elsif	(ie_opcao_p	= 'D') then
	if	(nvl(ie_idade_saldo_ativo_p,0) <> 0) then
		if	(ie_idade_saldo_ativo_p	= 1) then	-- Planos Individuais
			select	sum(nvl(vl_individual,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_ati
			where	nr_seq_periodo	= nr_seq_periodo_p;
		elsif	(ie_idade_saldo_ativo_p	= 2) then	-- Planos coletivos a pre�o  pr�-estabelecido
			select	sum(nvl(vl_coletivo_pre,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_ati
			where	nr_seq_periodo	= nr_seq_periodo_p;
		elsif	(ie_idade_saldo_ativo_p	= 3) then	-- Planos coletivos a pre�o p�s-estabelecidos
			select	sum(nvl(vl_coletivo_pos,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_ati
			where	nr_seq_periodo	= nr_seq_periodo_p;
		elsif	(ie_idade_saldo_ativo_p	= 4) then	-- Taxa de adm. / partic. dos benef.
			select	sum(nvl(vl_taxa_adm,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_ati
			where	nr_seq_periodo	= nr_seq_periodo_p;
		elsif	(ie_idade_saldo_ativo_p	= 5) then	-- Cr�ditos de operadoras
			select	sum(nvl(vl_credito_operadoras,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_ati
			where	nr_seq_periodo	= nr_seq_periodo_p;
		elsif	(ie_idade_saldo_ativo_p	= 6) then	-- Outros cr�ditos
			select	sum(nvl(vl_outros_creditos,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_ati
			where	nr_seq_periodo	= nr_seq_periodo_p;
		end if;
	elsif	(nvl(ie_idade_saldo_passivo_p,0) <> 0) then
		if	(ie_idade_saldo_passivo_p	= 1) then	-- Eventos a liquidar
			select	sum(nvl(vl_evento_liquidar,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_pas
			where	nr_seq_periodo	= nr_seq_periodo_p;
		elsif	(ie_idade_saldo_passivo_p	= 2) then	-- D�bitos com operadoras
			select	sum(nvl(vl_debito_operadora,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_pas
			where	nr_seq_periodo	= nr_seq_periodo_p;
		elsif	(ie_idade_saldo_passivo_p	= 3) then	-- Comercializa��o sobre opera��es
			select	sum(nvl(vl_comercializacao,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_pas
			where	nr_seq_periodo	= nr_seq_periodo_p;
		elsif	(ie_idade_saldo_passivo_p	= 4) then	-- Outros d�bitos operacionais
			select	sum(nvl(vl_debito_oper,0))
			into	vl_retorno_w
			from	diops_fin_idadesaldo_pas
			where	nr_seq_periodo	= nr_seq_periodo_p;
		end if;
	end if;
end if;

return	vl_retorno_w;

end pls_obter_valor_idade_saldo;
/
