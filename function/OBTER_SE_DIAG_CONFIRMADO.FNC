CREATE OR REPLACE 
FUNCTION Obter_se_diag_confirmado (	nr_prescricao_p		Number,
					nr_seq_diag_p		Number) 
				Return varchar2 IS

ds_retorno_w	varchar2(1) := 'S';
qt_existe_w	Number(10);

/* S Sugerido - C Confirmado */

BEGIN

select	count(*)
into	qt_existe_w
from	pe_prescr_diag
where	nr_seq_prescr	= nr_prescricao_p
and	nr_seq_diag	= nr_seq_diag_p;

if	(qt_existe_w > 0) then
	ds_retorno_w	:= 'C';
end if;	

Return ds_retorno_w;

END Obter_se_diag_confirmado;
/