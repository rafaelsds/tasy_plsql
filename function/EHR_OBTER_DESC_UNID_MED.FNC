create or replace
function EHR_Obter_Desc_Unid_Med(
				nr_sequencia_p	number,
				ie_opcao_p		varchar2)
				return Varchar2 is

/*
C - C�digo
D - Descri��o
*/				
				
cd_unidade_medida_w Varchar2(100);
ds_unidade_medida_w Varchar2(100);
ds_retorno_w		Varchar2(100);

begin

if	(nr_sequencia_p > 0) then
	begin
	select	cd_unidade_medida,
			ds_unidade_medida
	into	cd_unidade_medida_w,
			ds_unidade_medida_w
	from	ehr_unidade_medida
	where	nr_sequencia	= nr_sequencia_p;
	
	if	(ie_opcao_p = 'C') then
		ds_retorno_w := cd_unidade_medida_w;
	else	
		ds_retorno_w := ds_unidade_medida_w;
	end if;	
	end;
	
end if;

return	ds_retorno_w;

end EHR_Obter_Desc_Unid_Med;
/
