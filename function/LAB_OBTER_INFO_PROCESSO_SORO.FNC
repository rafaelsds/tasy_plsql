create or replace
function lab_obter_info_processo_soro (nr_seq_armazena_p lab_soro_armazena_rack.nr_sequencia%type,
									   nr_seq_amostra_p  lab_soro_amostra_rack.nr_sequencia%type,
									   ie_acao_p	varchar2,
									   ie_opcao_p   varchar2)
									   return varchar2 is

ds_retorno_w		varchar2(255);
nm_usuario_w		lab_soro_processo_info.nm_usuario%type; 
dt_acao_w			lab_soro_processo_info.nm_usuario%type;

/*
D - Data da a��o
U - Usu�rio da a��o
*/

begin

if  (nr_seq_armazena_p is not null) and
	(nr_seq_amostra_p  is not null) then
	
	SELECT 	max(t.nm_usuario),
			max(t.dt_acao)
	into	nm_usuario_w,
			dt_acao_w
	FROM 	lab_soro_processo_info t 
	WHERE 	t.ie_acao = ie_acao_p
	AND	  	t.nr_seq_armazena_rack = nr_seq_armazena_p
	AND	  	t.nr_seq_amostra_rack  = nr_seq_amostra_p;
	
	if (ie_opcao_p = 'D') then
		ds_retorno_w := dt_acao_w;
	elsif (ie_opcao_p = 'U') then
		ds_retorno_w := nm_usuario_w;
	end if;

end if;


return	ds_retorno_w;

end lab_obter_info_processo_soro;
/
