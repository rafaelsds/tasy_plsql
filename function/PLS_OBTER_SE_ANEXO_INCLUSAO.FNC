/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Verificar se a inclus�o de benefici�rios possui algum anexo vinculado. 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_se_anexo_inclusao
		(	nr_seq_inclusao_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(4);
qt_anexos_w		number(2);
			
begin

if	(nr_seq_inclusao_p > 0) then
	
	select	count(1)
	into	qt_anexos_w
	from	pls_inclusao_anexo		b,
		pls_inclusao_beneficiario	a
	where	a.nr_sequencia = b.nr_seq_inclusao
	and	a.nr_sequencia = nr_seq_inclusao_p;
	
	if	(qt_anexos_w > 0) then
		ds_retorno_w := 'Sim';
	else
		ds_retorno_w := 'N�o';
	end if;

end if;

return	ds_retorno_w;

end pls_obter_se_anexo_inclusao;
/