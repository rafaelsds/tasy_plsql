create or replace
function hcc_obter_vl_antecipacao_tit(nr_titulo_p	number)
						return number is
						
vl_movimento_w			number(15,2)	:= 0;
vl_retorno_w			number(15,2)	:= 0;
nr_lote_contab_antecip_w	number(10);
nr_lote_contab_pro_rata_w	number(10);
nr_seq_baixa_w			number(10);

Cursor c01 is
select	a.nr_sequencia
from	titulo_receber_liq a
where	a.nr_titulo	= nr_titulo_p
and	a.nr_lote_contab_antecip > 0;

begin

if	(nr_titulo_p is not null) then
	open c01;
	loop
	fetch c01 into	
		nr_seq_baixa_w;
	exit when c01%notfound;
		begin
		vl_movimento_w	:= 0;
		
		select	max(nr_lote_contab_antecip),
			max(nr_lote_contab_pro_rata)
		into	nr_lote_contab_antecip_w,
			nr_lote_contab_pro_rata_w
		from	titulo_rec_liq_cc c,
			titulo_receber_liq b
		where	b.nr_titulo	= c.nr_titulo
		and	b.nr_titulo	= nr_titulo_p
		and	b.nr_sequencia	= nr_seq_baixa_w
		and	b.nr_lote_contab_antecip > 0;
		
		if	(nr_lote_contab_pro_rata_w > 0) then
			select	nvl(sum(d.vl_movimento),0)
			into	vl_movimento_w
			from	movimento_contabil d
			where	d.nr_lote_contabil  = nr_lote_contab_pro_rata_w 
			and	d.cd_conta_contabil in ('1236','738')
			and	d.ie_debito_credito = 'C'
			and	substr(d.ds_compl_historico,1,instr(ds_compl_historico,',') - 1) = to_char(nr_titulo_p);
		end if;
		if	(nr_lote_contab_antecip_w > 0) and
			(vl_movimento_w = 0) then
			select	nvl(sum(d.vl_movimento),0)
			into	vl_movimento_w
			from	movimento_contabil d
			where	d.nr_lote_contabil  = nr_lote_contab_antecip_w 
			and	d.cd_conta_contabil = '1003446'
			and	d.ie_debito_credito = 'D'
			and	substr(d.ds_compl_historico,1,instr(ds_compl_historico,',') - 1) = to_char(nr_titulo_p);
		end if;
		
		vl_retorno_w	:= vl_retorno_w + vl_movimento_w;
		end;
	end loop;
	close c01;
end if;

return	vl_retorno_w;

end hcc_obter_vl_antecipacao_tit;
/