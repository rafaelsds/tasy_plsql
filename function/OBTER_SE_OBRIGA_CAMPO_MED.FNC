create or replace
function  Obter_se_obriga_campo_med (	nr_prescricao_p			number,
										cd_material_p			number,
										cd_perfil_p				number,
										ie_opcao_p				varchar2)
										return varchar2 is
					
/*
ie_opcao_p
D=Dias previstos para utilização
*/					
					
ie_obrigar_w			varchar2(1) := 'N';
cd_convenio_w			number(5);

begin

select	max(obter_convenio_atendimento(nr_atendimento))
into	cd_convenio_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	decode(count(nr_sequencia),0,'N','S')
into	ie_obrigar_w
from	regra_obriga_campo_med
where	nvl(cd_perfil,cd_perfil_p)		= cd_perfil_p
and		nvl(cd_convenio,cd_convenio_w)	= cd_convenio_w
and		nvl(cd_material,cd_material_p)	= cd_material_p
and		ie_campo						= ie_opcao_p;

return	ie_obrigar_w;

end Obter_se_obriga_campo_med;
/
