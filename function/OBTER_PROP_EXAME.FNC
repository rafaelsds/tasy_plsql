create or replace
function obter_prop_exame(nr_seq_exame_p number)
                           return number is                           

nr_seq_prop_w	number(10);

begin
	select nr_seq_propriedade
  	into nr_seq_prop_w
		from (select nr_seq_propriedade, ie_prioridade
            from LAB_LOINC_PROP_EXAME
           where nr_seq_exame = nr_seq_exame_p and
                 nvl(ie_situacao,'A') = 'A'
        order by ie_prioridade)
		where rownum = 1;    

  return nr_seq_prop_w;
end obter_prop_exame;
/