create or replace
function obter_desc_grupo_rep_rotina (nr_seq_grupo_p    number)
                return varchar2 is

ds_grupo_w      varchar2(80);

begin
if      (nr_seq_grupo_p is not null) then
        begin
        select  max(ds_grupo)
        into    ds_grupo_w
        from    rep_rotina_grupo
        where   nr_sequencia = nr_seq_grupo_p;
        end;
end if;
return ds_grupo_w;
end obter_desc_grupo_rep_rotina;
/
