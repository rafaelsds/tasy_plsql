create or replace
function Ageint_Mensagem_Tempo_Grupo(
					cd_agenda_p		Number,
					hr_Agenda_p		Date,
					nr_minuto_duracao_p	Number,
					nr_Seq_ageint_p		Number,
					nr_seq_ageint_item_p	Number,
					nm_usuario_p		Varchar2)
 		    	return Varchar2 is

ds_retorno_w			Varchar2(2000);
nr_Seq_marcacao_w		Number(10);
hr_Agenda_w				Date;
nr_minuto_duracao_w		Number(10);
cd_Agenda_w				Number(10);
qt_tempo_sala_w			Number(10);
qt_sobra_w				Number(10);
hr_max_agenda_w			Date;
hr_min_agenda_w			Date;
ds_retorno_max_w		Varchar2(1)	:= 'S';
ds_Retorno_min_w		Varchar2(1)	:= 'S';
nr_Seq_ageint_item_w	Number(10);
nr_seq_grupo_selec_w	Number(10);
nr_Seq_proc_marc_w		Number(10);
qt_Tempo_Exame_w		Number(10);
nr_Seq_regra_w			Number(10);
cd_exame_w				Number(10);
qt_Tempo_w				Number(10);
	
Cursor C01 is
	select	nr_Sequencia
	from	ageint_tempo_entre_grupos
	where	(cd_grupo_pri	= nr_seq_grupo_selec_w)
	or		(cd_grupo_seg	= nr_seq_grupo_selec_w)
	and		ie_situacao	= 'A';
	
Cursor C02 is
	select	a.hr_Agenda,
			a.nr_minuto_duracao
	from	agenda_integrada_item b,
			ageint_marcacao_usuario a
	where	a.nr_seq_Ageint			= b.nr_seq_agenda_int
	and		a.nr_Seq_Ageint_item	= b.nr_sequencia
	and		a.nm_usuario			= nm_usuario_p
	and		a.nr_Seq_ageint			= nr_seq_ageint_p
	and		b.nr_seq_grupo_selec	= cd_exame_w;
	
begin

select	max(nr_seq_grupo_selec)
into	nr_seq_grupo_selec_w
from	agenda_integrada_item
where	nr_sequencia	= nr_Seq_Ageint_item_p;

open C01;
loop
fetch C01 into	
	nr_seq_regra_w;
exit when C01%notfound;
	begin
	select	decode(cd_grupo_pri, nr_seq_grupo_selec_w, cd_grupo_seg, cd_grupo_pri),
			qt_tempo
	into	cd_exame_w,
			qt_tempo_w
	from	ageint_tempo_entre_grupos
	where	nr_sequencia	= nr_seq_regra_w;
	
	open C02;
	loop
	fetch C02 into	
		hr_Agenda_w,
		nr_minuto_duracao_w;
	exit when C02%notfound;
		begin
		if	(ds_retorno_w	is null) then
			if	(hr_Agenda_w	< hr_Agenda_p) then
				select	Obter_Min_Entre_Datas((hr_Agenda_w + nr_minuto_duracao_w / 1440), hr_Agenda_p, 1)
				into	qt_sobra_w
				from	dual;		
			else
				select	Obter_Min_Entre_Datas((hr_Agenda_p + nr_minuto_duracao_p / 1440), hr_Agenda_w, 1)
				into	qt_sobra_w
				from	dual;		
			end if;
			
			if	(qt_sobra_w	< qt_tempo_w) then
				ds_retorno_w	:= wheb_mensagem_pck.get_texto(306939 ,	'QT_HORAS=' || substr(OBTER_HORAS_MINUTOS(qt_tempo_w),1,3) || ';' ||
																		'QT_MINUTOS=' || substr(OBTER_HORAS_MINUTOS(qt_tempo_w),4,2) || ';' || 
																		'NM_GRUPO=' || substr(ageint_obter_nome_grupo(nr_seq_grupo_selec_w),1,90) || ';' ||
																		'NM_GRUPO_2=' || substr(ageint_obter_nome_grupo(cd_exame_w),1,90)); 
								/*
									Intervalo entre grupos: #@QT_HORAS#@ horas #@QT_MINUTOS#@ minutos
									Grupos: #@NM_GRUPO#@ - #@NM_GRUPO_2#@
								*/
			
			
			end if;
		end if;
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

return	ds_retorno_w;

end Ageint_Mensagem_Tempo_Grupo;
/