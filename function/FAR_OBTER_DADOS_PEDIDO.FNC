create or replace
function far_obter_dados_pedido(nr_seq_pedido_p	number,
								ie_retorno_p	varchar2)
 		    	return varchar2 is
/*DE - Data entrega real*/				
				
dt_entrega_real_w	date;

ds_retorno_w	varchar2(255);
begin
if	(ie_retorno_p = 'DE') then
	select	max(dt_entrega_real)
	into	dt_entrega_real_w
	from	far_rom_entrega_pedido
	where	nr_seq_pedido = nr_seq_pedido_p;
	ds_retorno_w := to_char(dt_entrega_real_w,'dd/mm/yyyy hh24:mi:ss');
end if;

return	ds_retorno_w;

end far_obter_dados_pedido;
/