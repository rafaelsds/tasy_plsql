create or replace
function Ageint_Consiste_Tempo_Grupos(
					cd_agenda_p		Number,
					hr_Agenda_p		Date,
					nr_minuto_duracao_p	Number,
					nr_Seq_ageint_p		Number,
					nr_seq_ageint_item_p	Number,
					nm_usuario_p		Varchar2)
 		    	return Varchar2 is

ds_retorno_w			Varchar2(1)	:= 'S';
nr_Seq_marcacao_w		Number(10);
hr_Agenda_w				Date;
nr_minuto_duracao_w		Number(10);
cd_Agenda_w				Number(10);
qt_tempo_sala_w			Number(10);
qt_sobra_w				Number(10);
hr_max_agenda_w			Date;
hr_min_agenda_w			Date; 
ds_retorno_max_w		Varchar2(1)	:= 'S';
ds_Retorno_min_w		Varchar2(1)	:= 'S';
nr_Seq_ageint_item_w	Number(10);
nr_seq_grupo_selec_w	Number(10);
nr_Seq_proc_marc_w		Number(10);
qt_Tempo_Exame_w		Number(10);
nr_Seq_regra_w			Number(10);
cd_exame_w				Number(10);
qt_Tempo_w				Number(10);
nr_seq_proc_adic_w		number(10);
nr_seq_grupo_adic_w		Number(10);
qt_proc_adic_w			number(10);
	
Cursor C01 is
	select	nr_Sequencia
	from	ageint_tempo_entre_grupos
	where	((cd_grupo_pri	= nr_seq_grupo_selec_w)
	or		(cd_grupo_seg	= nr_seq_grupo_selec_w))
	and		ie_situacao	= 'A';
	
Cursor C02 is
	select	a.hr_Agenda,
			a.nr_minuto_duracao,
			b.nr_sequencia
	from	agenda_integrada_item b,
			ageint_marcacao_usuario a
	where	a.nr_seq_Ageint			= b.nr_seq_agenda_int
	and		a.nr_Seq_Ageint_item	= b.nr_sequencia 
	and		a.nm_usuario			= nm_usuario_p
	and		a.nr_Seq_ageint			= nr_seq_ageint_p
	and		b.nr_seq_grupo_selec	= cd_exame_w
	and		b.nr_sequencia			<> nr_seq_ageint_item_p;
	
Cursor C05 is
	select	a.hr_Agenda,
			a.nr_minuto_duracao,
			c.nr_sequencia
	from	ageint_exame_adic_item c,
			agenda_integrada_item b,
			ageint_marcacao_usuario a
	where	a.nr_seq_Ageint			= b.nr_seq_agenda_int
	and		a.nr_Seq_Ageint_item	= b.nr_sequencia 
	and		a.nm_usuario			= nm_usuario_p
	and		c.nr_seq_item			= b.nr_sequencia
	and		c.nr_seq_grupo_selec	= cd_exame_w
	and		a.nr_Seq_ageint			= nr_seq_ageint_p
	and		b.nr_sequencia			<> nr_seq_ageint_item_p;
	
Cursor C03 is
	select	nr_seq_grupo_selec
	from	ageint_exame_adic_item
	where	nr_seq_item	= nr_seq_ageint_item_p
	order by nr_seq_proc_interno;
	
Cursor C04 is
	select	nr_seq_grupo_selec
	from	ageint_exame_adic_item
	where	nr_seq_item			<> nr_seq_ageint_item_p
	and		nr_seq_grupo_selec	= cd_exame_w
	order by nr_seq_proc_interno;
	
begin

select	max(nr_seq_grupo_selec)
into	nr_seq_grupo_selec_w
from	agenda_integrada_item
where	nr_sequencia	= nr_Seq_Ageint_item_p;

/*
cursor das regras a qual o grupo do item se aplica
*/
open C01;
loop
fetch C01 into	
	nr_seq_regra_w;
exit when C01%notfound;
	begin
	select	decode(cd_grupo_pri, nr_seq_grupo_selec_w, cd_grupo_seg, cd_grupo_pri),
			qt_tempo
	into	cd_exame_w,
			qt_tempo_w
	from	ageint_tempo_entre_grupos
	where	nr_sequencia	= nr_seq_regra_w;
	/*
	cursor das marcacoes com os grupos ao qual a regra se aplica
	*/	
	
	open C02;
	loop
	fetch C02 into	
		hr_Agenda_w,
		nr_minuto_duracao_w,
		nr_seq_ageint_item_w;
	exit when C02%notfound;
		begin
		if	(ds_retorno_w	<> 'N') then
			if	(hr_Agenda_w	< hr_Agenda_p) then
				select	Obter_Min_Entre_Datas((hr_Agenda_w + nr_minuto_duracao_w / 1440), hr_Agenda_p, 1)
				into	qt_sobra_w
				from	dual;		
			else
				select	Obter_Min_Entre_Datas((hr_Agenda_p + nr_minuto_duracao_p / 1440), hr_Agenda_w, 1)
				into	qt_sobra_w
				from	dual;		
			end if;
			
			if	(qt_sobra_w	< qt_tempo_w) then
				ds_retorno_w	:= 'N';
			else
				ds_Retorno_w	:= 'S';
			end if;	
			
			if	(ds_retorno_w	<> 'N') then
				select	count(*)
				into	qt_proc_adic_w
				from	ageint_exame_adic_item
				where	nr_seq_item	= nr_seq_ageint_item_w;
				
				if	(qt_proc_adic_w	> 0) then
					/*
					cursor dos exames adicionais dos itens que a regra se aplica
					*/
					open C04;
					loop
					fetch C04 into	
						nr_seq_grupo_adic_w;
					exit when C04%notfound;
						begin
						if	(ds_retorno_w	<> 'N') then
							if	(hr_Agenda_w	< hr_Agenda_p) then
								select	Obter_Min_Entre_Datas((hr_Agenda_w + nr_minuto_duracao_w / 1440), hr_Agenda_p - 1/1440, 1)
								into	qt_sobra_w
								from	dual;		
							else
								select	Obter_Min_Entre_Datas((hr_Agenda_p + nr_minuto_duracao_p / 1440), hr_Agenda_w - 1/1440, 1)
								into	qt_sobra_w
								from	dual;		
							end if;
							
							if	(qt_sobra_w	< qt_tempo_w) then
								ds_retorno_w	:= 'N';
							else
								ds_Retorno_w	:= 'S';
							end if;
						end if;
						end;
					end loop;
					close C04;
				end if;
			end if;
			
		end if;
		end;
	end loop;
	close C02;
	
	open C05;
	loop
	fetch C05 into	
		hr_Agenda_w,
		nr_minuto_duracao_w,
		nr_seq_ageint_item_w;
	exit when C05%notfound;
		begin
		if	(ds_retorno_w	<> 'N') then
			if	(hr_Agenda_w	< hr_Agenda_p) then
				select	Obter_Min_Entre_Datas((hr_Agenda_w + nr_minuto_duracao_w / 1440), hr_Agenda_p, 1)
				into	qt_sobra_w
				from	dual;		
			else
				select	Obter_Min_Entre_Datas((hr_Agenda_p + nr_minuto_duracao_p / 1440), hr_Agenda_w, 1)
				into	qt_sobra_w
				from	dual;		
			end if;
			
			if	(qt_sobra_w	< qt_tempo_w) then
				ds_retorno_w	:= 'N';
			else
				ds_Retorno_w	:= 'S';
			end if;
						
		end if;
		end;
	end loop;
	close C05;
		
	end;
end loop;
close C01;

if	(ds_retorno_w	<> 'N') then
	select	count(*)
	into	qt_proc_adic_w
	from	ageint_exame_adic_item
	where	nr_seq_item	= nr_seq_ageint_item_p;

	if	(qt_proc_adic_w	> 0) then
		/*
		cursor dos exames adicionais do item
		*/
		open C03;
		loop
		fetch C03 into	
			nr_seq_grupo_adic_w;
		exit when C03%notfound;
			begin
			if	(ds_retorno_w	<> 'N') then
				/*
				cursor das regras ao qual o exame adicional se aplica
				*/
				open C01;
				loop
				fetch C01 into	
					nr_seq_regra_w;
				exit when C01%notfound;
					begin
					select	decode(cd_grupo_pri, nr_seq_grupo_adic_w, cd_grupo_seg, cd_grupo_pri),
							qt_tempo
					into	cd_exame_w,
							qt_tempo_w
					from	ageint_tempo_entre_grupos
					where	nr_sequencia	= nr_seq_regra_w;
					
					/*
					cursor das marcacoes com os exames ao qual a regra se aplica
					*/
					open C02;
					loop
					fetch C02 into	
						hr_Agenda_w,
						nr_minuto_duracao_w,
						nr_seq_ageint_item_w;
					exit when C02%notfound;
						begin
						if	(ds_retorno_w	<> 'N') then
							if	(hr_Agenda_w	< hr_Agenda_p) then
								select	Obter_Min_Entre_Datas((hr_Agenda_w + nr_minuto_duracao_w / 1440), hr_Agenda_p, 1)
								into	qt_sobra_w
								from	dual;		
							else
								select	Obter_Min_Entre_Datas((hr_Agenda_p + nr_minuto_duracao_p / 1440), hr_Agenda_w, 1)
								into	qt_sobra_w
								from	dual;		
							end if;
							
							if	(qt_sobra_w	< qt_tempo_w) then
								ds_retorno_w	:= 'N';
							else
								ds_Retorno_w	:= 'S';
							end if;
							if	(ds_retorno_w	<> 'N') then
								select	count(*)
								into	qt_proc_adic_w
								from	ageint_exame_adic_item
								where	nr_seq_item	= nr_seq_ageint_item_w;
								
								if	(qt_proc_adic_w	> 0) then
									/*
									cursor dos exames adicionais dos itens que a regra se aplica
									*/
									open C04;
									loop
									fetch C04 into	
										nr_seq_grupo_adic_w;
									exit when C04%notfound;
										begin
										if	(ds_retorno_w	<> 'N') then
											if	(hr_Agenda_w	< hr_Agenda_p) then
												select	Obter_Min_Entre_Datas((hr_Agenda_w + nr_minuto_duracao_w / 1440), hr_Agenda_p, 1)
												into	qt_sobra_w
												from	dual;		
											else
												select	Obter_Min_Entre_Datas((hr_Agenda_p + nr_minuto_duracao_p / 1440), hr_Agenda_w, 1)
												into	qt_sobra_w
												from	dual;		
											end if;
											
											if	(qt_sobra_w	< qt_tempo_w) then
												ds_retorno_w	:= 'N';
											else
												ds_Retorno_w	:= 'S';
											end if;
										end if;
										end;
									end loop;
									close C04;
								end if;
							end if;
						end if;
						end;
					end loop;
					close C02;
					end;
				end loop;
				close C01;
			end if;
			end;
		end loop;
		close C03;	
	end if;
end if;

return	ds_retorno_w;

end Ageint_Consiste_Tempo_Grupos;
/