create or replace
function obter_se_medico_equipe(	nr_seq_equipe_P		number,
					cd_pessoa_fisica_p	number)
					return varchar2 is
ie_existe_w		varchar(1) := 'N';

begin

if 	(nr_seq_equipe_p is not null) and 
	(cd_pessoa_fisica_p is not null) then
	select 	nvl(max('S'),'N')
	into 	ie_existe_w
	from 	pf_equipe_partic
	where 	nr_seq_equipe 	 = nr_seq_equipe_P
	and 	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	if	(ie_existe_w	= 'N') then
			select 	nvl(max('S'),'N')
		into 	ie_existe_w
		from 	pf_equipe
		where 	nr_sequencia 	 = nr_seq_equipe_P
		and 	cd_pessoa_fisica = cd_pessoa_fisica_p;
	end if;
end if;

return	ie_existe_w;

end obter_se_medico_equipe;
/