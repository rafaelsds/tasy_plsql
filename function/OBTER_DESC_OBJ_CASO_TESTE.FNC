create or replace
function obter_desc_obj_caso_teste(	nr_seq_acao_p		number,
									nr_seq_objeto_p		number) 
	return varchar2 is

ds_retorno_w	varchar(500);

ie_acao_w		caso_teste_acao.ie_acao%type;

begin

select	nvl(max(ie_acao),0)
into	ie_acao_w
from	caso_teste_acao 
where	nr_sequencia = nr_seq_acao_p;

if	(ie_acao_w is not null and ie_acao_w > 0)	then
	
	case ie_acao_w
	when '6' then		--Pastas
	
		select	substr(OBTER_DESC_EXPRESSAO(cd_exp_texto),1,500)
		into 	ds_retorno_w
		from	dic_objeto 
		where	nr_sequencia = nr_seq_objeto_p;
		
	else
		select	substr(nm_objeto,1,500)
		into 	ds_retorno_w
		from	dic_objeto a
		where 	nr_sequencia = nr_seq_objeto_p;
			
	end case;

end if;
	

return	ds_retorno_w;

end obter_desc_obj_caso_teste;
/
