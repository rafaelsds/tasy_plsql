create or replace
function OBTER_DADOS_ADIANT_PAGO
	(nr_adiantamento_p		number,
	 ie_opcao_p		varchar2) return varchar2 is

/*
ie_opcao_p
	'O'	- T�tulo que originou o adiantamento - NR_TITULO_ORIGINAL
	'TG'	- T�tulo gerado a partir do adiantamento
	'DTL'            -  Data de liquida��o do t�tulo gerado  
*/

ds_retorno_w	varchar2(255);

begin

ds_retorno_w		:= null;
if	(ie_opcao_p = 'O') then
	select	max(nr_titulo_original)
	into	ds_retorno_w
	from	adiantamento_pago	
	where	nr_adiantamento	= nr_adiantamento_p;
end if;

if	(ie_opcao_p = 'TG') then
	select	max(a.nr_titulo)
	into	ds_retorno_w
	from	titulo_pagar a
	where	a.nr_adiant_pago	= nr_adiantamento_p;
end if;

if	(ie_opcao_p = 'DTL') then
	select	max(a.dt_liquidacao)
	into	ds_retorno_w
	from	titulo_pagar a
	where	a.nr_adiant_pago	= nr_adiantamento_p;
end if;

return	ds_retorno_w;

end OBTER_DADOS_ADIANT_PAGO;
/
