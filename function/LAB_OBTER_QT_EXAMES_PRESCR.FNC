create or replace
function lab_obter_qt_exames_prescr(nr_prescricao_p		number)
 		    	return number is
qt_exames_w		number(10) := 0;

begin
	if	(nr_prescricao_p is not null) then
	
		select count(*)
		into	qt_exames_w
		from   prescr_procedimento
		where  nr_prescricao = nr_prescricao_p
		and    nr_seq_exame is not null
		and    nr_seq_superior is null;

	end if;

return	qt_exames_w;

end lab_obter_qt_exames_prescr;
/
