create or replace
function obter_se_aval_pac_lib(cd_pessoa_fisica_p	varchar2)
				return varchar2 is
ie_possui_w	varchar2(1);
ie_avaliacao_w	varchar2(255);

begin
Obter_Param_Usuario(3130,354,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_avaliacao_w);

ie_possui_w := 'S';

if	(cd_pessoa_fisica_p is not null) and
	(ie_avaliacao_w is not null) then 
	select nvl(max('S'),'N')
	into	ie_possui_w
	from 	med_avaliacao_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and 	dt_avaliacao >= sysdate - 1
	and	dt_liberacao is not null
	and 	obter_se_contido(nr_seq_tipo_avaliacao, ie_avaliacao_w) = 'S';
end if;

return	ie_possui_w;

end obter_se_aval_pac_lib;
/