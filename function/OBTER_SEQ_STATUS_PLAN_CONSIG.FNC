create or replace
function obter_seq_status_plan_consig	(	nr_seq_agenda_p		number,
							nr_seq_age_consig_p	number,
							ie_linha_coluna_p	varchar2,
							ie_coluna_p		varchar2)
							return number is

					
ds_retorno_w	number(10);
nr_sequencia_w	number(10);
					
cursor	c00 is
	select	nr_sequencia
	from	(select	a.nr_sequencia,
			b.dt_atualizacao
		from	regra_cor_planej_consig a,
			agenda_reg_cor_plan_consig b
		where	a.nr_sequencia		= b.nr_seq_status
		and	(((b.nr_seq_age_consig	= nr_seq_age_consig_p) and (nr_seq_age_consig_p is not null)) or
			 ((b.nr_seq_agenda	= nr_seq_agenda_p) and (nr_seq_agenda_p is not null)))
		and	b.ie_linha_coluna	= ie_linha_coluna_p
		and	nvl(b.ie_coluna,'XPTO')	= nvl(ie_coluna_p,'XPTO')
		and	nvl(a.ie_situacao,'A')	= 'A')
	order by dt_atualizacao;

begin

open c00;
loop
fetch c00 into
	nr_sequencia_w;
exit when c00%notfound;
	ds_retorno_w	:= nr_sequencia_w;
end loop;
close c00;

return	ds_retorno_w;

end obter_seq_status_plan_consig;
/