create or replace
function qr_obter_info_check_item(nr_seq_item_p		number,
								ie_tipo_p			varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(4000) := '';	
ds_checklist_w	varchar2(255);			
				
begin
if	(nr_seq_item_p is not null) then

	select	max(ds_checklist)
	into	ds_checklist_w
	from	qr_checklist a
	where	nr_sequencia = (select	max(nr_seq_checklist)	
							from	qr_checklist_item b
							where	b.nr_sequencia = nr_seq_item_p);

	select	decode(ie_tipo_p,'DC',ds_checklist_item,'DD',ds_detalhamento,'DO',ds_observacao,'DK',ds_checklist_w)
	into	ds_retorno_w
	from	qr_checklist_item
	where	nr_sequencia = nr_seq_item_p;

end if;

return	ds_retorno_w;

end qr_obter_info_check_item;
/