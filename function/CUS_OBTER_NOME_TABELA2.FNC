create or replace
function cus_obter_nome_tabela2(	nr_seq_tabela_p	Number)
					return varchar2 is

ds_tabela_custo_w			varchar2(50);

BEGIN

if	(nr_seq_tabela_p is not null) then
	select	max(ds_tabela_custo)
	into	ds_tabela_custo_w
	from	tabela_custo
	where	nr_sequencia	= nr_seq_tabela_p;
	
end if;

return ds_tabela_custo_w;

END cus_obter_nome_tabela2;
/