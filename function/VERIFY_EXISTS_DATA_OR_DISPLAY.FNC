create or replace FUNCTION VERIFY_EXISTS_DATA_OR_DISPLAY(nr_seq_filter_p number)
  RETURN VARCHAR2
 IS
  related_data_w NUMBER;
  related_display_w NUMBER;
  exists_w VARCHAR2(1);
BEGIN
	SELECT
		COUNT(1)
	into
		related_data_w
	FROM
		query_builder_dados
	WHERE
		nr_seq_filter = nr_seq_filter_p;

	SELECT
		COUNT(1)
	into
		related_display_w
	FROM
		query_builder_display
	WHERE
		nr_seq_filter = nr_seq_filter_p;

	IF (related_data_w > 0 OR related_display_w > 0) THEN
		exists_w := 'S';
	ELSE 
		exists_w := 'N';
	END IF;	

 RETURN exists_w;
END VERIFY_EXISTS_DATA_OR_DISPLAY;
/