create or replace function get_medispan_drug_names( 	nr_prescricao_p			in prescr_material.nr_prescricao%type,
														nr_seq_id_medispan_p	in alerta_api.nr_seq_id_medispan%type,
														nr_agrupamento_p		in alerta_api.nr_agrupamento%type,
														cd_api_p				in alerta_api.cd_api%type
													) return varchar2 is
													
	ds_nome_w	varchar2(2000 char);

	cursor cItemsCode is
		select distinct obter_desc_material(a.cd_material) ds_material
		  from alerta_api b,
			   prescr_material a
		 where a.nr_prescricao = b.nr_prescricao
		   and a.nr_sequencia = b.nr_seq_material
		   and b.nr_prescricao = nr_prescricao_p
		   and b.nr_seq_id_medispan = nr_seq_id_medispan_p
		   and b.cd_api = cd_api_p
		   and b.nr_agrupamento = nr_agrupamento_p
		   and b.ie_status_requisicao is null;

	cursor cItemsDesc is
		select distinct obter_desc_material(a.cd_material) ds_material
		  from alerta_api b,
			   prescr_material a
		 where a.nr_prescricao = b.nr_prescricao
		   and a.nr_sequencia = b.nr_seq_material
		   and b.nr_prescricao = nr_prescricao_p
		   and b.nr_agrupamento = nr_agrupamento_p
		   and b.cd_api = cd_api_p
		   and b.ie_status_requisicao is null;

begin
	ds_nome_w := '';

	if (nvl(nr_seq_id_medispan_p, 0) > 0) then
		for cItems_w in cItemsCode
		loop
			if (nvl(ds_nome_w, 'XPTO') = 'XPTO') then
				ds_nome_w := cItems_w.ds_material;
			else
				ds_nome_w := ds_nome_w || ' ' || obter_desc_expressao(312342) || ' ' || cItems_w.ds_material;
			end if;
		end loop;	
	else
		for cItems_w in cItemsDesc
		loop
			if (nvl(ds_nome_w, 'XPTO') = 'XPTO') then
				ds_nome_w := cItems_w.ds_material;
			else
				ds_nome_w := ds_nome_w || ' ' || obter_desc_expressao(312342) || ' ' || cItems_w.ds_material;
			end if;
		end loop;
	end if;

	return ds_nome_w;

end get_medispan_drug_names;
/
