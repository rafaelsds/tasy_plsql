create or replace
function obter_se_setor_sv_esp_ocup ( cd_setor_atendimento_p   varchar2,
              cd_unidade_basica_p  varchar2,
              cd_unidade_compl_p  varchar2) 
             return varchar2 is 

ie_ocup    varchar2(1) := 'N';
nr_count_temp  number(5);
nr_pac_temp  number(5);

begin

SELECT   count(*)
into     nr_count_temp
FROM     ocupacao_serv_esp_v
where  cd_setor_atendimento = cd_setor_atendimento_p
and  cd_unidade_basica = cd_unidade_basica_p
and  cd_unidade_compl = cd_unidade_compl_p;

select max(nvl(QT_PAC_UNIDADE,-1))
into nr_pac_temp
from unidade_atendimento
where cd_setor_atendimento = cd_setor_atendimento_p
and cd_unidade_basica = cd_unidade_basica_p
and cd_unidade_compl = cd_unidade_compl_p;

if (nr_pac_temp > -1) and 
   (nr_count_temp >= nr_pac_temp) then
	ie_ocup:= 'S';
else
	ie_ocup := 'N';
end if;

return ie_ocup;

end obter_se_setor_sv_esp_ocup; 
/