CREATE OR REPLACE FUNCTION gqa_validar_pontos_etapa(nr_sequencia_p  NUMBER)
  RETURN VARCHAR IS
  
  ie_permite_proxima_etapa_w gqa_pendencia_regra.ie_permite_proxima_etapa%TYPE;
  dt_fim_w                   gqa_protocolo_etapa_pac.dt_fim%TYPE;
  qt_resultado_w             gqa_protocolo_etapa_pac.qt_resultado%TYPE;
  qt_ponto_min_etapa_w       gqa_protocolo_etapa_pac.qt_ponto_min_etapa%TYPE;
  qt_ponto_max_etapa_w       gqa_protocolo_etapa_pac.qt_ponto_max_etapa%TYPE;
  nr_seq_etapa_sup_w         gqa_protocolo_etapa_pac.nr_seq_etapa_sup%TYPE;
  nr_seq_etapa_prot_sup_w    gqa_protocolo_etapa_pac.nr_seq_etapa_prot_sup%TYPE;
  nr_seq_etapa_w             gqa_protocolo_etapa_pac.nr_seq_etapa%TYPE;
  return_w                   VARCHAR2(1) := 'N';
  
BEGIN
  BEGIN
    SELECT t.qt_ponto_min_etapa
          ,t.qt_ponto_max_etapa
          ,t.nr_seq_etapa_sup
          ,t.nr_seq_etapa_prot_sup
          ,t.nr_seq_etapa
      INTO qt_ponto_min_etapa_w
          ,qt_ponto_max_etapa_w
          ,nr_seq_etapa_sup_w
          ,nr_seq_etapa_prot_sup_w
          ,nr_seq_etapa_w
      FROM gqa_protocolo_etapa_pac t
     WHERE t.nr_sequencia = nr_sequencia_p;

    SELECT t.ie_permite_proxima_etapa
      INTO ie_permite_proxima_etapa_w
      FROM gqa_pendencia_regra t
     WHERE t.nr_sequencia = nr_seq_etapa_w;
  EXCEPTION
    WHEN no_data_found THEN
      NULL;
  END;

  IF ie_permite_proxima_etapa_w = 'S' THEN
    BEGIN
      SELECT t.dt_fim
            ,t.qt_resultado
        INTO dt_fim_w
            ,qt_resultado_w
        FROM gqa_protocolo_etapa_pac t
       WHERE t.nr_sequencia = nr_seq_etapa_prot_sup_w;
    EXCEPTION
      WHEN no_data_found THEN
        NULL;
    END;

    IF dt_fim_w IS NOT NULL THEN
      IF nvl(qt_resultado_w,0) BETWEEN qt_ponto_min_etapa_w AND qt_ponto_max_etapa_w THEN
        return_w := 'S';
      ELSE
        return_w := 'N';
      END IF;
    ELSE
      return_w := 'N';
    END IF;    
  ELSIF nr_seq_etapa_sup_w IS NULL THEN
    return_w := 'S';
  ELSE
    return_w := 'N';      
  END IF;
  
  RETURN return_w;
END gqa_validar_pontos_etapa;
/
