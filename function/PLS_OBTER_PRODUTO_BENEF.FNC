create or replace
function pls_obter_produto_benef
			(	nr_seq_segurado_p	number,
				dt_referencia_p		date)
 		    	return number is

nr_seq_plano_w		pls_plano.nr_sequencia%type;
nr_seq_plano_ant_w	pls_plano.nr_sequencia%type;
nr_seq_alt_plano_w	pls_segurado_alt_plano.nr_sequencia%type;
nr_seq_alt_plano_ww	pls_segurado_alt_plano.nr_sequencia%type;
dt_alteracao_w		pls_segurado_alt_plano.dt_alteracao%type;

begin

select	max(nr_seq_plano)
into	nr_seq_plano_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

select	max(nr_sequencia)
into	nr_seq_alt_plano_w
from	pls_segurado_alt_plano
where	nr_seq_segurado	= nr_seq_segurado_p
and	nr_seq_plano_atual = nr_seq_plano_w
and	ie_situacao = 'A'
and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_alteracao) > dt_referencia_p;

if	(nr_seq_alt_plano_w is not null) then
	select	max(nr_seq_plano_ant),
		max(dt_alteracao)
	into	nr_seq_plano_ant_w,
		dt_alteracao_w
	from	pls_segurado_alt_plano
	where	nr_sequencia	= nr_seq_alt_plano_w;
	
	select	max(nr_sequencia)
	into	nr_seq_alt_plano_ww
	from	pls_segurado_alt_plano
	where	nr_seq_segurado	= nr_seq_segurado_p
	and	nr_seq_plano_atual = nr_seq_plano_ant_w
	and	ie_situacao = 'A'
	and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_alteracao) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_alteracao_w);
	
	if	(nr_seq_alt_plano_ww is null) then
		nr_seq_plano_w	:= nr_seq_plano_ant_w;
	end if;
end if;

return	nr_seq_plano_w;

end pls_obter_produto_benef;
/
