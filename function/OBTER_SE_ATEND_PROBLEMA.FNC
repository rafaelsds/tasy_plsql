create or replace
function obter_se_atend_problema(nr_atendimento_p	number,
								 ie_opcao_p			varchar2,
				 nr_seq_problema_p	number default null	)
								 return varchar2 is

								 
								 
-- IE_OPCAO_P 
--  I  -  In�cio  de problema.
--  F -  Fim de problema.
--  R -  Reincid�ncia de prolema.	
						 
ie_problema_w	varchar2(1) := 'N';

begin

if	(nr_atendimento_p is not null) then

	if	(ie_opcao_p = 'I') then
		select	nvl(max('S'),'N')
		into  ie_problema_w
		from  lista_problema_pac a
		where  a.nr_atendimento = nr_atendimento_p
		and	  dt_inicio is not null;
	elsif	(ie_opcao_p = 'F') then	
		select	nvl(max('S'),'N')
		into  ie_problema_w
		from  lista_problema_pac a
		where  a.nr_atendimento = nr_atendimento_p
		and	  dt_inicio is not null;
	elsif	(ie_opcao_p = 'R') then
		select	nvl(max('S'),'N')
		into  ie_problema_w
		from  lista_problema_pac_incid a
		where  a.nr_atendimento = nr_atendimento_p;
	elsif	(ie_opcao_p = 'P') then
		select	nvl(max('S'),'N')
		into  ie_problema_w
		from  lista_problema_pac_incid a
		where  a.nr_atendimento = nr_atendimento_p
		AND	A.NR_SEQ_PROBLEMA = nr_seq_problema_p;	
	end if;
end if;
return	ie_problema_w;

end obter_se_atend_problema;
/
