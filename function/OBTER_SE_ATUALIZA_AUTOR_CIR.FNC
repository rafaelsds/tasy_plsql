CREATE OR REPLACE 
function obter_se_atualiza_autor_cir(nr_seq_autor_conv_p	number,
					nr_seq_estagio_p	number,
					nr_seq_autor_cirurgia_p	number,
					cd_convenio_p		number,
					ie_tipo_autorizacao_p	varchar2,
					cd_estabelecimento_p	number) 
					return varchar2 is

ie_estagio_ame_w		autorizacao_cirurgia.ie_estagio_autor%type;
nr_seq_autor_cirurgia_w		autorizacao_cirurgia.nr_sequencia%type;
ie_estagio_novo_ame_w		regra_estagio_aut_mat_esp.ie_estagio_novo_ame%type;
qt_regra_w			number(10);
ie_existe_regra_w		varchar2(1) := 'N';

Cursor C01 is
	select	a.ie_estagio_novo_ame
	from	regra_estagio_aut_mat_esp a
	where	a.nr_seq_estagio_ac = nr_seq_estagio_p
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	nvl(a.ie_situacao,'A') = 'A'
	and	nvl(a.cd_convenio,cd_convenio_p) 	= cd_convenio_p
	AND	((nvl(a.ie_tipo_autorizacao,ie_tipo_autorizacao_p) = ie_tipo_autorizacao_p) OR (nvl(ie_tipo_autorizacao_p,'X') = 'X'))
	and	((nvl(a.ie_estagio_ame,ie_estagio_ame_w)	= ie_estagio_ame_w) OR (NVL(ie_estagio_ame_w,0) = 0))
	order by nvl(a.cd_convenio,0),
		 nvl(a.ie_tipo_autorizacao,0);


begin

select	count(*)
into	qt_regra_w
from	regra_estagio_aut_mat_esp
where	cd_estabelecimento = cd_estabelecimento_p
and	nvl(ie_situacao,'A') = 	'A';

if	(qt_regra_w > 0) then

	if	(nr_seq_autor_cirurgia_p is not null) then

		select	max(a.ie_estagio_autor),
			max(nr_seq_autor_cirurgia_p)
		into	ie_estagio_ame_w,
			nr_seq_autor_cirurgia_w
		from	autorizacao_cirurgia a
		where	a.nr_sequencia = nr_seq_autor_cirurgia_p;
	else

		select	max(a.ie_estagio_autor),
			max(a.nr_sequencia)
		into	ie_estagio_ame_w,
			nr_seq_autor_cirurgia_w
		from	autorizacao_cirurgia a
		where	nr_seq_autor_conv = nr_seq_autor_conv_p;

	end if;

	open C01;
	loop
	fetch C01 into
		ie_estagio_novo_ame_w;
	exit when C01%notfound;
		begin

		ie_estagio_novo_ame_w := ie_estagio_novo_ame_w;

		end;
	end loop;
	close C01;

	if	(ie_estagio_novo_ame_w is not null) and
		(nr_seq_autor_cirurgia_w is not null) then
		ie_existe_regra_w := 'S';
	end if;


end if;

return ie_existe_regra_w;

end obter_se_atualiza_autor_cir;
/