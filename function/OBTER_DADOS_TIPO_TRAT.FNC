create or replace
function obter_dados_tipo_trat(nr_sequencia_p	number)			
 		    	return varchar2 is

ds_retorno_w	varchar2(255);		

begin

ds_retorno_w := '';

select 	substr(ds_tipo_tratamento,1,255)
into	ds_retorno_w	
from	tipo_tratamento_quimio
where	nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end obter_dados_tipo_trat;
/