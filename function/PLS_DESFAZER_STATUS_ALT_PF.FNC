create or replace
procedure pls_desfazer_status_alt_pf
			(	nr_seq_solicitacao_p	number,
				nm_usuario_p		varchar2) is

begin

update	tasy_solic_alt_campo
set	ie_status	= 'P',
	nr_seq_motivo_rejeicao	= null,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_solicitacao_p;

commit;	

end pls_desfazer_status_alt_pf;
/