CREATE OR REPLACE
FUNCTION Obter_Se_Medico_Lib(	cd_estabelecimento_p	number,
				cd_medico_p		varchar2,
				cd_convenio_p		number)
 			return varchar2 is

qt_registro_w		number(15);
ds_retorno_w		varchar2(10);

BEGIN

select	count(*)
into	qt_registro_w
from	conv_medico_liberacao
where	cd_medico		= cd_medico_p
and	ie_situacao		= 'A'
and	cd_estabelecimento	= cd_estabelecimento_p;

ds_retorno_w	:= 'S';

if	(qt_registro_w	>0) then
	ds_retorno_w	:= 'N';
	
	select	count(*)
	into	qt_registro_w
	from	conv_medico_liberacao
	where	cd_medico		= cd_medico_p
	and	ie_situacao		= 'A'
	and	cd_convenio		= cd_convenio_p
	and	cd_estabelecimento	= cd_estabelecimento_p;
	if	(qt_registro_w	> 0) then
		ds_retorno_w	:= 'S';
	end if;
end if;


return	ds_retorno_w;

END Obter_Se_Medico_Lib;
/