CREATE OR REPLACE FUNCTION BUILD_EXTERNAL_LINK_GRID (nr_sequencia_in IN gqa_pend_pac_acao.nr_sequencia%type ) return varchar2 deterministic IS
    
    nr_sequencia_p     gqa_pend_pac_acao.nr_seq_pend_pac%type not null := nr_sequencia_in;
    external_link_w       varchar2(1000 char);
    ds_link_w             varchar2(500 char);

    CURSOR c_links IS (
        SELECT substr(ds_link, 1) FROM gqa_pend_pac_acao WHERE 1=1
        AND nr_sequencia = nr_sequencia_p
        AND ds_link is not null
    );

BEGIN 
    OPEN c_links;
    LOOP
        FETCH c_links INTO ds_link_w;
        EXIT WHEN c_links%notfound;
        BEGIN
          if (substr(ds_link_w, 1, 4) = 'http') then
            external_link_w := '<a href="'||ds_link_w||'" target="_blank">'|| ds_link_w ||'</a>';
          else
            external_link_w :=  '<a href="http://'||ds_link_w||'" target="_blank">'|| ds_link_w ||'</a>';
          end if;
        END;

    END LOOP;
    CLOSE c_links;

return substr(external_link_w, 1, 1000);
END BUILD_EXTERNAL_LINK_GRID;
/