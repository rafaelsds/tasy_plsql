create or replace
function EME_VERIFICA_HIST_PROCED(nr_atendimento_p		number)
 		    	return varchar2 is

ie_classe_profissional_w 	varchar2(3);	
ds_retorno_w				varchar2(3) := 'S';
				
				
--verifica se o paciente tem procedimento cadastrado nas regras de procedimento				
Cursor C01 is
	select 	a.ie_classe_profissional	
	from 	EME_REGRA_PROCED a,
			PROCEDIMENTO_PACIENTE b 
	where  	1 = 1
	and	   	a.CD_PROCEDIMENTO = b.cd_procedimento
	and	   	a.IE_ORIGEM_PROCED = b.ie_origem_proced	   
	and	   	a.cd_estabelecimento = obter_estabelecimento_ativo
	and	   	b.nr_atendimento = nr_atendimento_p;								
								
begin

open C01;
loop
fetch C01 into	
	ie_classe_profissional_w;
exit when (C01%notfound) or (ds_retorno_w = 'N');
	begin
--verifica se existe histórico liberado para akele atendimento, por um profissional = ao da regra dacastrada	
		select 	decode(count(*),0,'N','S')
		into	ds_retorno_w	
		from   	evolucao_paciente c,
				usuario g
		where  	c.cd_medico = g.cd_pessoa_fisica  
		and	   	dt_liberacao is not null
		and    	c.nr_atendimento = nr_atendimento_p
		and	   	g.ie_profissional = ie_classe_profissional_w;
	end;
end loop;
close C01;

return	ds_retorno_w;

end EME_VERIFICA_HIST_PROCED;
/