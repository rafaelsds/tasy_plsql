create or replace
function get_total_score_dpc(nr_seq_patient_dpc_p number) return number is

begin

return dpc_pkg.get_total_score_dpc(nr_seq_patient_dpc_p);

end get_total_score_dpc;
/