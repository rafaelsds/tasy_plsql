create or replace
function mprev_val_forma_ing_camp(nr_seq_campanha_p number,
				ie_forma_ingresso_p varchar2,
				nr_seq_atual_p varchar2) 
 		    	return varchar2 is			
ds_retorno_w	varchar2(10);
qt_forma_w	number(10);
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar a poss�vel exist�ncia de uma forma de ingresso igual, para uma mesma campanha, a que est� tentando inserir.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
	if (nr_seq_campanha_p is not null) and 
	   (ie_forma_ingresso_p is not null) then 
		select	count(1)
		into	qt_forma_w 
		from	mprev_camp_forma_ingresso
		where	nr_seq_campanha = nr_seq_campanha_p
		and	ie_forma_ingresso = ie_forma_ingresso_p
		and	nr_seq_atual_p <> nr_sequencia;
		
		if (qt_forma_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(247879);
		else 
			ds_retorno_w := 'S';
		end if;
	end if;
return	ds_retorno_w;

end mprev_val_forma_ing_camp;
/
