create or replace
function obter_desc_qua_grupo_cargo(nr_seq_grupo_cargo_p		qua_grupo_cargo.nr_sequencia%type)
 		    	return varchar2 is
			
expressao_w	dic_expressao.ds_expressao_br%type;

begin

select  SUBSTR(obter_desc_expressao(cd_exp_agrupamento, ds_agrupamento),1,255) ds
into	expressao_w
from 	qua_grupo_cargo
where	nr_sequencia = nr_seq_grupo_cargo_p;


return	expressao_w;

end obter_desc_qua_grupo_cargo;
/