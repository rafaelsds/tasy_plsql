create or replace
function obter_vel_infusao_sne(	nr_prescricao_p		number,
				nr_seq_material_p	number)
				return number is

qt_vel_infusao_w	number(15,4);
				
begin

select	max(a.qt_dosagem)
into	qt_vel_infusao_w
from	prescr_solucao_evento a
where	a.nr_prescricao = nr_prescricao_p
and	a.nr_seq_material = nr_seq_material_p
and	a.dt_atualizacao = (	select	max(b.dt_atualizacao)
				from	prescr_solucao_evento b
				where	b.nr_prescricao = a.nr_prescricao
				and	a.nr_seq_material = b.nr_seq_material
				and	ie_alteracao in (1,3,5));

return	qt_vel_infusao_w;

end obter_vel_infusao_sne;
/