create or replace
function pls_obter_req_exec_imp (	nr_seq_guia_plano_imp_p		pls_guia_plano_imp.nr_sequencia%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type)
					return Number is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Verifica se existe uma requisi��o pendente para execu��o, validando os seguintes campos:
Benefici�rio
Prestador solicitante
Medico Solicitante
Guia

Tamb�m valida se todos os procedimentos e materiais existem na requisi��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_guia_w		pls_guia_plano_imp.ie_tipo_guia%type;
nr_seq_segurado_w	pls_segurado.nr_sequencia%type;
nr_seq_prest_solic_w	pls_prestador.nr_sequencia%type;
cd_medico_solic_w	pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_requisicao_w	pls_requisicao.nr_sequencia%type;
ie_origem_proced_w	procedimento.ie_origem_proced%type;
cd_procedimento_w	procedimento.cd_procedimento%type;
qt_registro_w		Number(10);
cd_prestador_exec_w	pls_guia_plano_imp.cd_prestador_exec%type;
cd_guia_prestador_w	pls_guia_plano_imp.cd_guia_prestador%type;

Cursor C01 ( 	nr_seq_guia_plano_imp_pc	pls_guia_plano_imp.nr_sequencia%type ) is
	select	cd_tabela,
		cd_procedimento
	from	pls_guia_plano_proc_imp
	where	nr_seq_guia_plano_imp = nr_seq_guia_plano_imp_pc;

Cursor C02 ( nr_seq_guia_plano_imp_pc	pls_guia_plano_imp.nr_sequencia%type ) is
	select	b.cd_tabela,
		b.cd_material,
		(select	max(a.nr_sequencia)
		from	pls_material a
		where	a.cd_material_ops = b.cd_material) nr_seq_material
	from	pls_guia_plano_mat_imp b
	where	b.nr_seq_guia_plano_imp = nr_seq_guia_plano_imp_pc;

begin

select	ie_tipo_guia,
	pls_obter_prestador_imp(b.cd_cgc_prest_solic, b.cd_cpf_prest_solic, b.cd_prestador_solic, '', '', '', 'G') nr_seq_prest_solic,
	pls_obter_dados_medico_imp(b.nr_conselho_prof_solic, pls_obter_sg_uf_tiss(b.cd_uf_conselho_prof_solic), b.cd_conselho_prof_solic, 'CP', 'G') cd_medico_solic,
	pls_obter_dados_segurado_imp(b.cd_usuario_plano, 'SS', 'G') nr_seq_segurado,
	cd_prestador_exec,
	cd_guia_prestador
into	ie_tipo_guia_w,
	nr_seq_prest_solic_w,
	cd_medico_solic_w,
	nr_seq_segurado_w,
	cd_prestador_exec_w,
	cd_guia_prestador_w
from	pls_guia_plano_imp b
where	b.nr_sequencia = nr_seq_guia_plano_imp_p;

--Benefici�rio
--Prestador solicitante
--Medico Solicitante
-- Guia
select	max(a.nr_sequencia)
into	nr_seq_requisicao_w
from	pls_requisicao	a
where	nr_seq_segurado		= nr_seq_segurado_w
and	nr_seq_prestador 	= nr_seq_prest_solic_w
and	cd_medico_solicitante 	= cd_medico_solic_w
and	ie_tipo_guia		= ie_tipo_guia_w
and	ie_estagio 		in (2, 9)
and	a.cd_guia_prestador	= cd_guia_prestador_w
and	(trunc(a.dt_valid_senha_ext)	>= trunc(sysdate)
or	(a.dt_valid_senha_ext is null
and	trunc(a.dt_validade_senha)	>= trunc(sysdate)))
and	not exists (select 1
		    from	pls_execucao_req_item b
		    where	b.nr_seq_requisicao = a.nr_sequencia)
and	(nvl(cd_prestador_exec_w,'999999') <> '999999');

if	( nr_seq_requisicao_w is not null ) then

	--Valida se os procedimentos importados existem na requisi��o
	for C01_w in C01( nr_seq_guia_plano_imp_p ) loop
		begin

			qt_registro_w := 0;

			--Retorna o c�digo e a origem do procedimento j� convertidos
			pls_gerar_dados_guia_proc_imp(  C01_w.cd_procedimento, C01_w.cd_tabela, nr_seq_prest_solic_w, cd_estabelecimento_p, cd_procedimento_w, ie_origem_proced_w);

			select 	count(1)
			into	qt_registro_w
			from	pls_requisicao_proc
			where	nr_seq_requisicao 	= nr_seq_requisicao_w
			and	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w;

			if	( qt_registro_w = 0 ) then
				return null;
			end if;
		end;
	end loop;

	--Valida se os materiais importados existem na requisi��o
	for C02_w in C02( nr_seq_guia_plano_imp_p ) loop
		begin
			qt_registro_w := 0;

			select 	count(1)
			into	qt_registro_w
			from	pls_requisicao_mat
			where	nr_seq_requisicao 	= nr_seq_requisicao_w
			and	nr_seq_material		= C02_w.nr_seq_material;

			if	( qt_registro_w = 0 ) then
				return null;
			end if;
		end;
	end loop;

end if;

return	nr_seq_requisicao_w;

end pls_obter_req_exec_imp;
/