create or replace
function Lab_obter_decimais (	ds_regra_p	varchar2,
				qt_decimal_p	number,
				qt_resultado_p	number,
				pr_resultado_p	number,
				ds_resultado_p	varchar2) 
				return number is

qt_decimal_w	number(1);
ds_regra_w	varchar2(255);
vl_resultado_w	number(15,4);

begin

qt_decimal_w	:= qt_decimal_p;

if	(ds_regra_p is not null) then
	if	(instr(ds_regra_p, '(') > 0) then
		ds_regra_w := substr(ds_regra_p, instr(ds_regra_p, '(') + 1, 255);
		ds_regra_w := substr(ds_regra_w, 0, instr(ds_regra_p, ')') - 1);
	end if;
end if;

if	(ds_regra_w is not null) then
	ds_regra_w	:= replace(ds_regra_w,'P',pr_resultado_p);
	ds_regra_w	:= replace(ds_regra_w,'V',qt_resultado_p);
	ds_regra_w	:= replace(ds_regra_w,'D',ds_resultado_p);
	if	(instr(ds_regra_w, 'P') = 0) and
		(instr(ds_regra_w, 'V') = 0) and
		(instr(ds_regra_w, 'D') = 0) then
		if	(nvl(pr_resultado_p,0) <> 0) then
			ds_regra_w := pr_resultado_p || ds_regra_w;
		elsif	(nvl(qt_resultado_p,0) <> 0) then
			ds_regra_w := qt_resultado_p || ds_regra_w;
		elsif	(nvl(ds_resultado_p,0) <> '') then
			ds_regra_w := ds_resultado_p || ds_regra_w;
		end if;
	end if;
	qt_decimal_w	:= substr(ds_regra_w, instr(ds_regra_w,';') + 1, 1);
	ds_regra_w	:= substr(ds_regra_w, 0, instr(ds_regra_w,';') - 1);
	ds_regra_w	:= replace(ds_regra_w,',','.');

	obter_valor_dinamico('select 1 from dual where ' || ds_regra_w, vl_resultado_w);

	if	(nvl(vl_resultado_w,0) <> 1) then
		qt_decimal_w :=	qt_decimal_p;
	end if;
end if;

return qt_decimal_w;

end;
/