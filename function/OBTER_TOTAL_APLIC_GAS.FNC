create or replace
function Obter_total_aplic_gas(nr_seq_gasoterapia_p	number,
			       ie_dado_p		varchar2)
 		    	return varchar2 is

/*
Tipo de dado:
	T = Tempo Total
	Q = Quantidade Total
*/

ds_retorno_w		varchar2(80);
ie_evento_w		varchar2(15);
ie_somar_w		varchar2(1);
qt_minutos_w		number(10,0) := 0;
dt_evento_w		Date;
dt_inicio_w		Date;

cursor c01 is
select	dt_evento,
	ie_evento
from	prescr_gasoterapia_evento
where	nr_seq_gasoterapia	= nr_seq_gasoterapia_p
and	nvl(ie_evento_valido,'S') = 'S'
--and	b.ie_evento 		<> 'I'
order by dt_evento;

begin

if	(ie_dado_p = 'Q') then

	select	to_char(sum(nvl(qt_gasoterapia,0)))
	into	ds_retorno_w
	from	prescr_gasoterapia_evento
	where	nr_seq_gasoterapia = nr_seq_gasoterapia_p
	and	nvl(ie_evento_valido,'S') = 'S';

elsif	(ie_dado_p = 'T') then
	
	open C01;
	loop
	fetch C01 into	
		dt_evento_w,
		ie_evento_w;
	exit when C01%notfound;

		if	(ie_evento_w	in ('R','I')) then
			dt_inicio_w	:= dt_evento_w;
			ie_somar_w	:= 'S';
		elsif	(ie_evento_w	in ('IN','T','TE')) and
			(ie_somar_w	= 'S') then
			qt_minutos_w	:= qt_minutos_w + obter_min_entre_datas(dt_inicio_w, dt_evento_w, 1);
			ie_somar_w	:= 'N';
		end if;
		
	end loop;
	close C01;	
	
	select	obter_horas_minutos_segundos(qt_minutos_w*60)
	into	ds_retorno_w
	from	dual;
	
end if;

return	ds_retorno_w;

end Obter_total_aplic_gas;
/