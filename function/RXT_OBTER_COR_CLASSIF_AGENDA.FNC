create or replace
function rxt_obter_cor_classif_agenda	(nr_seq_classif_p	number)
				return varchar2 is

ds_cor_w	varchar2(15);

begin
if	(nr_seq_classif_p is not null) then

	select	max(ds_cor)
	into	ds_cor_w
	from	rxt_classif_agenda
	where	nr_sequencia = nr_seq_classif_p;

end if;

return ds_cor_w;

end rxt_obter_cor_classif_agenda;
/