create or replace function
com_get_lista_modulo (
	nr_seq_ofic_uso_p	com_cli_ofic_uso.nr_sequencia%type,
	ds_pessoa_mod_p		com_cli_ofic_uso_mod_pes.ds_pessoa_fisica%type,
	nr_seq_idioma_p		idioma.nr_sequencia%type
) return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicionário [  ] Tasy (Delphi/Java) [  ] Portal [ X ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
-------------------------------------------------------------------------------------------------------------------
Referências: Relatórios: 4098, 4099 e 4100.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(4000 char);

cursor c01 is
	select	distinct
		(
			select	nvl(obter_desc_exp_idioma(mi.cd_exp_modulo, nr_seq_idioma_p), nm_modulo)
			from	modulo_implantacao mi
			where	mi.nr_sequencia = ccoum.nr_seq_mod_impl
		) ds_modulo
	from	com_cli_ofic_uso_mod ccoum,
		com_cli_ofic_uso_mod_pes ccoump
	where	ccoump.ds_pessoa_fisica = ds_pessoa_mod_p
	and	ccoum.nr_sequencia = ccoump.nr_seq_mod_ofic_uso
	and	ccoum.nr_seq_ofic = nr_seq_ofic_uso_p
	order by 1;

begin

	for	reg01 in c01
	loop
		ds_retorno_w := substr(ds_retorno_w || reg01.ds_modulo || ', ' || chr(13) || chr(10) , 1, 4000);
	end loop;

ds_retorno_w := substr(ds_retorno_w, 1, instr(ds_retorno_w, ',', -1)-1);

return ds_retorno_w;

end com_get_lista_modulo;
/