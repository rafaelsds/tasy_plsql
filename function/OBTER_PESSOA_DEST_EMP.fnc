create or replace function OBTER_PESSOA_DEST_EMP (cd_pf_realizou_p san_producao.cd_pf_realizou%type)
return varchar2 is

cd_pf_destino_w san_emprestimo.cd_pf_destino%type;

begin

if (cd_pf_realizou_p is not null) then

   select   max(a.cd_pf_destino)
   into     cd_pf_destino_w
   from     san_emprestimo a,
            san_producao b
   where    a.nr_sequencia      = b.nr_seq_emp_saida
   and      b.cd_pf_realizou    = cd_pf_realizou_p;

end if;

return cd_pf_destino_w;

end OBTER_PESSOA_DEST_EMP;
/
