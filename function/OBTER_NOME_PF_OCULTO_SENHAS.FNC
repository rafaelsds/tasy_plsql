create or replace
function obter_nome_pf_oculto_senhas( cd_pessoa_fisica_p	Varchar2,
					ie_opcao_p	varchar2)
 		    	return varchar2 is
ds_retorno_w		varchar2(255);
ie_nome_oculto_w	Varchar2(1) := 'N';			

begin

obter_param_Usuario(10021, 130, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_nome_oculto_w );

--ie_nome_oculto_w := 'S';
if	(cd_pessoa_fisica_p is not null) then	
	
	if  ( ie_opcao_p = 'O') and
	    ( ie_nome_oculto_w = 'S') then	    
		ds_retorno_w := substr(obter_nome_pf_oculta(cd_pessoa_fisica_p, obter_perfil_ativo, obter_usuario_ativo, null),1,200);
	elsif	(ie_opcao_p = 'O') and
			(ie_nome_oculto_w = 'N') then	    
		ds_retorno_w := SUBSTR(OBTER_NOME_PF(cd_pessoa_fisica_p), 0, 255);
	elsif ( ie_opcao_p = 'P') then
		ds_retorno_w := SUBSTR(OBTER_NOME_PF(cd_pessoa_fisica_p), 0, 255);
	end if;
end if;

return	ds_retorno_w ;

end obter_nome_pf_oculto_senhas;
/