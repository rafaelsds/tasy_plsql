create or replace
FUNCTION ageint_obter_setor_proc_int(
			nr_seq_proc_interno_p	Number,
			cd_setor_origem_p		number,
			ie_tipo_atendimento_p	Number,
			cd_convenio_p			Number,
			cd_categoria_p			Varchar2,
			ie_proced_principal_p 	varchar2 default 'S',
			cd_estabelecimento_p	number)
			return Number is

cd_setor_atendimento_w	Number(5);
cd_estabelecimento_w	Number(4);
ie_tipo_convenio_w	Number(2);
nr_seq_agrupamento_w	Number(10,0);
cd_medico_w				varchar2(10);

/* Rafael em 5/3/8 OS85232. Inclu� o par�metro cd_setor_origem_p e substitu� este cursos pelo novo cursor abaixo;
Cursor C01 is
select	cd_setor_atendimento
from	proc_interno_setor
where	nr_seq_proc_interno	= nr_seq_proc_interno_p
order by nr_prioridade desc;
*/

Cursor C01 is
select	cd_setor_atendimento
from	proc_interno_setor
where	nr_seq_proc_interno	= nr_seq_proc_interno_p
and	((cd_setor_origem	= cd_setor_origem_p) or (cd_setor_origem_p is null) or (cd_setor_origem is null))
and 	(nvl(cd_estabelecimento,nvl(cd_estabelecimento_p,1))	= nvl(cd_estabelecimento_p,1))
and 	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0)) = nvl(ie_tipo_atendimento_p,0)
and	nvl(cd_perfil,obter_perfil_ativo) = obter_perfil_ativo
AND ((NVL(ie_somente_principal,'N') = 'N') OR
	((NVL(ie_somente_principal,'N') = 'S') AND (ie_proced_principal_p = 'S')))
and 	nvl(ie_tipo_convenio, nvl(ie_tipo_convenio_w,0)) = nvl(ie_tipo_convenio_w,0)
and	nvl(nr_seq_agrupamento, nvl(nr_seq_agrupamento_w,0)) = nvl(nr_seq_agrupamento_w,0)
and		((cd_especialidade is null) or
		 (obter_se_especialidade_medico(cd_medico_w,cd_especialidade) = 'S'))
order by
	nr_prioridade desc,
	nvl(cd_setor_origem,0),
	nvl(ie_tipo_atendimento,0),
	nvl(ie_tipo_convenio_w,0),
	nvl(nr_seq_agrupamento,0);

BEGIN

select	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio 	= cd_convenio_p;

select	nvl(max(nr_seq_agrupamento),0)
into	nr_seq_agrupamento_w
from	setor_atendimento
where	cd_setor_atendimento = cd_setor_origem_p;

select	max(a.cd_pessoa_fisica)
into	cd_medico_w
from	medico a,
		usuario b
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and		b.nm_usuario		= wheb_usuario_pck.get_nm_usuario;

open C01;
loop
	fetch C01 into
		cd_setor_atendimento_w;
	exit when C01%notfound;
	cd_setor_atendimento_w	:= cd_setor_atendimento_w;
end loop;
close C01;

return	cd_setor_atendimento_w;

END ageint_obter_setor_proc_int;
/