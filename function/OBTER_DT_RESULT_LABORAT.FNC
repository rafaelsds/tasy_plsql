create or replace
function obter_dt_result_laborat(
			nr_prescricao_p		NUMBER,
			nr_seq_prescr_p		NUMBER)
 		    RETURN DATE IS

dt_atualizacao_w	DATE;

BEGIN

SELECT	MAX(dt_atualizacao)
INTO	dt_atualizacao_w
FROM	result_laboratorio
WHERE	nr_prescricao		= nr_prescricao_p
AND		nr_seq_prescricao	= nr_seq_prescr_p;

RETURN	dt_atualizacao_w;

END obter_dt_result_laborat;
/