CREATE OR REPLACE
FUNCTION Obter_Ultimo_Cid_Atend(nr_atendimento_p	number,
				ie_classificacao_p	varchar2,
				ie_por_usuario_logado number default 0)	
				RETURN VARCHAR2 IS

/* classificação do cid	 P-Principal S-Secundário
ie_por_usuario_logado  1 true or 0 false  */

cd_doenca_w			Varchar2(10);
dt_diagnostico_w		Date;


BEGIN
cd_doenca_w	:= '';
/* busca diagnostico medico */
begin

select	max(dt_diagnostico)
into	dt_diagnostico_w
from	diagnostico_medico a
where	a.nr_atendimento	= nr_atendimento_p
and	exists	(	select	1
			from	diagnostico_doenca b
			where	b.nr_atendimento	= a.nr_atendimento
			and	b.dt_diagnostico	= a.dt_diagnostico
			and	b.ie_classificacao_doenca	= ie_classificacao_p
			and     (	(ie_por_usuario_logado = 0) or
					(ie_por_usuario_logado = 1 and nm_usuario = Obter_Usuario_Ativo)
				)
		);
	
	
select	cd_doenca
into	cd_doenca_w
from	diagnostico_doenca
where	nr_atendimento		= nr_atendimento_p
and	dt_diagnostico		= dt_diagnostico_w
and	ie_classificacao_doenca	= ie_classificacao_p
and     ((ie_por_usuario_logado = 0) or 
	 (ie_por_usuario_logado = 1 and nm_usuario = Obter_Usuario_Ativo)
	);
exception	
	when others then
	cd_doenca_w	:= '';
end;

RETURN cd_doenca_w;
END Obter_Ultimo_Cid_Atend;
/