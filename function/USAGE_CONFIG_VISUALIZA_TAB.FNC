create or replace FUNCTION usage_config_visualiza_tab(
                nr_sequencia_p IN NUMBER )
        RETURN VARCHAR2
IS
        pais_w   CHAR;
        global_w CHAR;
BEGIN
        SELECT  MAX(p.IE_VISUALIZA)
        INTO    pais_w
        FROM    tab_pais p
        WHERE   p.cd_pais        = obter_nr_seq_locale(wheb_usuario_pck.get_nm_usuario)
        AND     p.nr_seq_tab_ref = nr_sequencia_p;
        
        SELECT  MAX(p.IE_VISUALIZA)
        INTO    global_w
        FROM    tab_pais p
        WHERE   p.cd_pais        = 10
        AND     p.nr_seq_tab_ref = nr_sequencia_p;
        
        RETURN NVL(NVL(pais_w,global_w),'S');
END;
/
