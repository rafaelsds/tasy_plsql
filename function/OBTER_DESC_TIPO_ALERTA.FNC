create or replace
function obter_desc_tipo_alerta(nr_sequencia_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(200);

begin

select 	ds_tipo_alerta
into 	ds_retorno_w
from 	tipo_alerta_atend
where 	nr_sequencia = nr_sequencia_p;

return ds_retorno_w;

end obter_desc_tipo_alerta;
/