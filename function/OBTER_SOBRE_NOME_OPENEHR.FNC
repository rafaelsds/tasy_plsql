create or replace function  obter_sobre_nome_openEHR(
   cd_pessoa_fisica_p varchar2) return varchar2 is

 ie_person_name_feature_w varchar2(1);
 nm_pessoa_fisica_w   pessoa_fisica.nm_usuario%type;

begin
 ie_person_name_feature_w := person_name_enabled();

 if (ie_person_name_feature_w = 'S') then
  select nm_pessoa_fisica
  into nm_pessoa_fisica_w
  from table(search_names_dev(null,cd_pessoa_fisica_p,null,'familyName',null, null));
 else
  select OBTER_NOME_SOBRENOME_PESSOA(cd_pessoa_fisica_p,'S')
  into nm_pessoa_fisica_w
  from dual;

 end if;

 return nm_pessoa_fisica_w;

end  obter_sobre_nome_openEHR;
/