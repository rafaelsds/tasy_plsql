create or replace
function obter_se_armario_setor(nr_seq_armario_p		ARMARIO_PERTENCE.NR_SEQUENCIA%type,
				cd_setor_atendimento_p		ARMARIO_PERTENCE.CD_SETOR_ATENDIMENTO%type)
 		    	return varchar2 is

ie_pertence_w			varchar2(1 char);
ie_param_estab_w		varchar2(3 char);
				
begin

ie_pertence_w	:= 'S';
Obter_Param_Usuario(991,21, obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_param_estab_w);

if	(cd_setor_atendimento_p is not null) then
	select	nvl(max('S'),'N')
	into	ie_pertence_w
	from	ARMARIO_PERTENCE
	where 	nvl(cd_setor_atendimento,cd_setor_atendimento_p) = cd_setor_atendimento_p
	and 	nr_sequencia = nr_seq_armario_p
	and 	ie_situacao = 'A'
	and 	ie_livre = 'S'
    and 	((ie_param_estab_w = 'S' AND cd_estabelecimento = obter_estabelecimento_ativo) OR (ie_param_estab_w = 'N'));
else 
	select	nvl(max('S'),'N')
	into	ie_pertence_w
	from	ARMARIO_PERTENCE
	where 	nr_sequencia = nr_seq_armario_p
	and 	ie_situacao = 'A'
	and 	ie_livre = 'S'
    and 	((ie_param_estab_w = 'S' AND cd_estabelecimento = obter_estabelecimento_ativo) OR (ie_param_estab_w = 'N'));
end if;

return	ie_pertence_w;

end obter_se_armario_setor;
/