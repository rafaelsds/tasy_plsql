create or replace function obter_usuario_gestor_conta (
		nr_seq_ordem_serv_p	man_ordem_servico.nr_sequencia%type,
		ie_apresenta_erro_p	varchar2 default 'N')
	return varchar2
is
	nr_seq_cliente_w	com_cliente.nr_sequencia%type;
	nm_usuario_gestor_w	varchar2(50);

begin

	select	max(mos.nr_seq_cliente)
	into	nr_seq_cliente_w
	from	man_ordem_servico mos
	where	mos.nr_sequencia = nr_seq_ordem_serv_p;
	
	select	max(substr(nvl(obter_usuario_pf(ccc.cd_pessoa_fisica), 'pcfelix') ,1, 40))
	into	nm_usuario_gestor_w
	from	com_cliente_conta ccc
	where	ccc.nr_seq_cliente = nr_seq_cliente_w
	and	sysdate between ccc.dt_inicial and nvl(dt_final, sysdate);
	
	if	(nm_usuario_gestor_w is null
		and ie_apresenta_erro_p = 'S') then
		begin
			wheb_mensagem_pck.exibir_mensagem_abort(1088612);
		end;
	end if;

return nm_usuario_gestor_w;
end obter_usuario_gestor_conta;
/
