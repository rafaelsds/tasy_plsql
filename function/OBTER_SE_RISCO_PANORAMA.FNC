create or replace
function obter_se_risco_panorama (
				nr_atendimento_p	number)
				return varchar2 is

ds_retorno_w			varchar2(4000) := null;
cd_classif_setor_w		varchar2(2);
qt_registros_w			number(10);
ie_profilaxia_tev_w		varchar2(1);
ie_profilaxia_lamg_w	varchar2(1);
ie_decubito_elev_w		varchar2(1);
ie_mudanca_decubito_w	varchar2(1);
ie_sedacao_w			varchar2(1);
ie_desmame_vini_w		varchar2(1);

begin
if 	(nr_atendimento_p is not null) and
	(nr_atendimento_p > 0) then
	begin
	select  obter_classif_setor_atend(nr_atendimento_p)
	into	cd_classif_setor_w
	from	dual;

	if (cd_classif_setor_w = '4') then
		begin
		
		select	count(*)
		into	qt_registros_w			
		from	w_gerenciamento_risco
		where	nr_atendimento = nr_atendimento_p
		and		trunc(dt_atualizacao) = trunc(sysdate);
		
		if	(qt_registros_w > 0) then
			begin
			select  nvl(max(ie_profilaxia_tev),'N'),
					nvl(max(ie_profilaxia_lamg),'N'),
					nvl(max(ie_decubito_elev),'N'),
					nvl(max(ie_mudanca_decubito),'N'),
					nvl(max(ie_sedacao),'N'),
					nvl(max(ie_desmame_vini),'N')
			into	ie_profilaxia_tev_w,
					ie_profilaxia_lamg_w,
					ie_decubito_elev_w,
					ie_mudanca_decubito_w,
					ie_sedacao_w,
					ie_desmame_vini_w
			from	w_gerenciamento_risco
			where	nr_atendimento = nr_atendimento_p
			and		trunc(dt_atualizacao) = trunc(sysdate);
			
			if (ie_profilaxia_tev_w <> 'S') then
				begin
				ds_retorno_w := substr(ds_retorno_w||'Prescrever profilaxia para TEV'|| '<br>',1,4000);
				end;
			end if;
			
			if (ie_profilaxia_lamg_w <> 'S') then
				begin
				ds_retorno_w := substr(ds_retorno_w||'Prescrever profilaxia para LAMG'|| '<br>',1,4000);
				end;
			end if;
			
			if (ie_decubito_elev_w <> 'S') then
				begin
				ds_retorno_w := substr(ds_retorno_w||'Prescrever dec�bito com cabeceira elevada'|| '<br>',1,4000);
				end;
			end if;
			
			if (ie_mudanca_decubito_w <> 'S') then
				begin
				ds_retorno_w := substr(ds_retorno_w||'Prescrever mudan�a de dec�bito conforme protocolo'|| '<br>',1,4000);
				end;
			end if;
			
			if (ie_sedacao_w <> 'S') then
				begin
				ds_retorno_w := substr(ds_retorno_w||'Prescrever Interrup��o de Seda��o Di�ria'|| '<br>',1,4000);
				end;
			end if;
			
			if (ie_desmame_vini_w <> 'S') then
				begin
				ds_retorno_w   :=  substr(ds_retorno_w||'Prescrever Avalia��o para Desmame da VMI'|| '<br>',1,4000);
				end;
			end if;
			
			if (ds_retorno_w = '') then
				begin
				ds_retorno_w := null;
				end;
			end if;
			end;
		else
			begin
			ds_retorno_w := substr(ds_retorno_w||'Prescrever profilaxia para TEV'|| '<br>',1,4000);
			ds_retorno_w := substr(ds_retorno_w||'Prescrever profilaxia para LAMG'|| '<br>',1,4000);
			ds_retorno_w := substr(ds_retorno_w||'Prescrever dec�bito com cabeceira elevada'|| '<br>',1,4000);
			ds_retorno_w := substr(ds_retorno_w||'Prescrever mudan�a de dec�bito conforme protocolo'|| '<br>',1,4000);
			end;
		end if;
		end;
	end if;
	end;
end if;
return ds_retorno_w;
end obter_se_risco_panorama;
/