create or replace
function pls_obter_se_grupo_prest_int (	nr_seq_grupo_prest_int_p	pls_preco_grupo_prest_int.nr_sequencia%type,
					nr_seq_prest_inter_p		pls_prestador_intercambio.nr_sequencia%type)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se o prestador de interc�mbio pertence ao grupo informado
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ds_retorno_w	varchar2(1) := 'N';
qt_prestador_w	pls_integer;

begin
if	(nr_seq_prest_inter_p is not null) and
	(nr_seq_grupo_prest_int_p is not null) then 
	select	count(1)
	into	qt_prestador_w
	from	pls_preco_prestador_int
	where	nr_seq_prest_inter = nr_seq_prest_inter_p
	and	nr_seq_grupo = nr_seq_grupo_prest_int_p;
	
	if	(qt_prestador_w > 0) then
		ds_retorno_w := 'S';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_grupo_prest_int;
/
