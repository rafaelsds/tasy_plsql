create or replace function lab_obter_vol_amostra (nr_prescricao_p	number,
					nr_seq_material_p	number,
					nr_seq_grupo_p	number)
					return number is

qt_volume_w	number(5);

begin


select	nvl(max(a.qt_volume),0)
into	qt_volume_w
from 	prescr_proc_material a
where 	a.nr_prescricao 	= nr_prescricao_p
and	a.nr_seq_material	= nr_seq_material_p
and	((decode(a.nr_seq_grupo,0,nr_seq_grupo_p,a.nr_seq_grupo)  = nr_seq_grupo_p) or (a.nr_seq_grupo is null));

return qt_volume_w;

end;
/