create or replace
function Verifica_Se_proc_Tab_Serv
		(cd_estabelecimento_p		number,
		cd_convenio_p			number,
		cd_categoria_p			varchar2,
		dt_conta_p			date,
		cd_procedimento_p		number) 
		return varchar2 is

dt_vigencia_w			date;
dt_final_vigencia_w		date;
cd_tabela_servico_w		number(06,0);
ie_origem_proced_w		number(10,0);
ie_origem_proced_edicao_w	number(10,0);
qt_preco_w			number(10,0)	:= 0;
TX_AJUSTE_GERAL_w		number(15,4);
ie_liberado_edicao_w		varchar2(01)	:= 'N';
ie_tipo_convenio_w		number(02,0);
nr_aux_w			number(15,4);
ds_aux_w			varchar2(200);
ie_glosa_w			varchar2(2);
vl_servico_w			number(15,4);

cursor	c01 is
	select	b.cd_tabela_servico
	from	convenio_servico b
	where	b.cd_convenio		= cd_convenio_p
	and	b.cd_estabelecimento 	= cd_estabelecimento_p
	and	(b.cd_categoria		= cd_categoria_p or cd_categoria_p is null)
	--and	b.dt_liberacao_tabela	<= dt_conta_p
	and	establishment_timezone_utils.startofday(dt_conta_p)	between b.dt_liberacao_tabela and nvl(b.dt_termino, dt_conta_p)
	and 	nvl(b.ie_situacao,'A')	= 'A'
	order by nvl(b.nr_prioridade,1) desc;

begin

select	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio	= cd_convenio_p;

if	(ie_tipo_convenio_w	= 3) then
	ie_liberado_edicao_w	:= 'S';
else
	open	c01;
	loop
	fetch	c01 into
		cd_tabela_servico_w;
	exit	when c01%notfound;
		begin	

		if	(qt_preco_w = 0) then
			begin

			select	count(*)
			into	qt_preco_w
			from	preco_servico
			where	CD_TABELA_SERVICO	= cd_tabela_servico_w
			and	cd_estabelecimento	= cd_estabelecimento_p
			and	cd_procedimento		= cd_procedimento_p
                        and     (establishment_timezone_utils.startofday(dt_conta_p) between dt_inicio_vigencia and nvl(dt_vigencia_final,establishment_timezone_utils.startofday(dt_conta_p)))
                        and     (dt_inativacao is null);
                        
			if	(qt_preco_w > 0) then --sevico dentro da vigencia
				ie_liberado_edicao_w	:= 'S';
			end if;

			end;
		end if;

		end;
	end loop;
	close c01;
end if;

return	ie_liberado_edicao_w;
--dentro da dt liberacao e dt 
end Verifica_Se_proc_Tab_Serv;
/
