create or replace
function obter_qt_convert_item_inspecao (nr_sequencia_p		number)	return number is

qt_prevista_entrega_w		number(13,4);

begin

select	nvl(decode(obter_dados_material(a.cd_material,'UME'),
		obter_dados_item_ordem(a.nr_ordem_compra, a.nr_item_oci,'UMC'),
		1,
		obter_conversao_material(a.cd_material, 'CE')) * nvl(a.qt_inspecao,0),0)
into	qt_prevista_entrega_w
from	inspecao_recebimento a
where	nr_sequencia = nr_sequencia_p;

return	qt_prevista_entrega_w;

end obter_qt_convert_item_inspecao;
/