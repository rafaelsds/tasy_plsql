create or replace function
obter_acesso_dicom_concat(nr_atendimento_p	number)
 		    return varchar2 is

ds_temp_w	varchar(4000);
ds_retorno_w 	varchar(4000);

cursor C01 is

select 	nr_acesso_dicom
from	gestao_exame_v
where  	nr_atendimento = nr_atendimento_p
order by nr_acesso_dicom desc;

begin
open 	c01;
	loop
	fetch 	c01 into
		ds_temp_w;
	ds_retorno_w := ds_retorno_w || ds_temp_w||', ';
	exit when c01%notfound;
end loop;

close c01;

return	ds_retorno_w;

end obter_acesso_dicom_concat;
/