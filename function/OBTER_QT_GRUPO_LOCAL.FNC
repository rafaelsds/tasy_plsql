create or replace
function obter_qt_grupo_local(nr_seq_local_p		number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(30);

begin

if 	(nr_seq_local_p is not null) then
	select 	max(ds_nome_curto)
	into	ds_retorno_w
	from 	qt_local_grupo a,
		qt_local b
	where	b.nr_sequencia = nr_seq_local_p
	and 	b.nr_seq_grupo_quimio = a.nr_sequencia;
end if;

return	ds_retorno_w;

end obter_qt_grupo_local;
/
