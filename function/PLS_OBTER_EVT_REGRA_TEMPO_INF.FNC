create or replace
function 	pls_obter_evt_regra_tempo_inf(
		qt_tempo_p			number,			
		nr_seq_tipo_ocorrencia_p	number,
		ie_informacao_p			varchar2)
 		    	return varchar2 is

ds_regra_w		varchar2(100);
ds_cor_w		varchar2(15);
qt_tempo_maximo_w	number(10,0);
nr_seq_regra_w		number(10);
ds_informacao_w		varchar2(100);

begin
if	(qt_tempo_p is not null) and
	(nr_seq_tipo_ocorrencia_p is not null) then
	begin
	pls_obter_evento_regra_tempo (
			qt_tempo_p,
			nr_seq_tipo_ocorrencia_p,
			null,
			ds_regra_w,
			ds_cor_w,
			qt_tempo_maximo_w,
			nr_seq_regra_w);
			
	if	(ie_informacao_p = 'R') then
		begin
		ds_informacao_w	:= ds_regra_w;
		end;
	elsif	(ie_informacao_p = 'C') then
		begin
		ds_informacao_w	:= ds_cor_w;
		end;
	elsif	(ie_informacao_p = 'T') then
		begin
		ds_informacao_w	:= qt_tempo_maximo_w;
		end;
	elsif	(ie_informacao_p = 'NR') then
		begin
		ds_informacao_w	:= nr_seq_regra_w;
		end;
	end if;
	end;
end if;
return	ds_informacao_w;
end pls_obter_evt_regra_tempo_inf;
/