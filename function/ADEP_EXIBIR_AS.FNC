create or replace
function adep_exibir_as(nr_atendimento_p	number,
			ie_restr_estab_p	varchar2,
			cd_pessoa_fisica_p	varchar2,
			cd_funcao_p		varchar2,
			cd_funcao_chamada_p	varchar2,
			cd_perfil_p		varchar2,
			cd_estabelecimento_p	number)
 		    	return varchar2 is

ie_w		varchar2(1) := '';
ds_retorno_w	varchar2(1) := 'N';
			
cursor c01 is			
select   'S'
from	 atendimento_alerta a
where	 nr_atendimento       = nr_atendimento
and	 a.ie_situacao        = 'A'
and	 ((a.dt_fim_alerta is null) or (a.dt_fim_alerta >= trunc(sysdate,'dd'))) and (((a.cd_estabelecimento = cd_estabelecimento_p) and (ie_restr_estab_p = 'S')) or (ie_restr_estab_p = 'N')) AND (((a.cd_estabelecimento = cd_estabelecimento_p) AND (ie_restr_estab_p = 'S')) OR (ie_restr_estab_p = 'N') OR (a.cd_estabelecimento IS NULL))
union all
select   'S' 
from	 alerta_paciente a
where	 a.cd_pessoa_fisica   = cd_pessoa_fisica_p
and	 a.ie_situacao        = 'A'
and	 ((a.dt_fim_alerta is null) or (a.dt_fim_alerta >= trunc(sysdate,'dd')))
and      ((cd_funcao_p = 0) or (a.cd_funcao = cd_funcao_p))
and      (((a.cd_estabelecimento = cd_estabelecimento_p) and (ie_restr_estab_p = 'S')) or
         (ie_restr_estab_p = 'N'))
union all
select   'S'
from	 alerta_paciente_geral a, 
         pessoa_fisica b
where    a.ie_situacao        = 'A'
and	 ((a.dt_fim_alerta is null) or (a.dt_fim_alerta >= trunc(sysdate,'dd')))
and      ((a.cd_funcao is null) or (a.cd_funcao = cd_funcao_chamada_p))
and      ((a.cd_perfil is null) or (a.cd_perfil = cd_perfil_p))      
and      obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'A') between 
         nvl(qt_idade_min,obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'A')) and  nvl(qt_idade_max,obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'A')) 
and      b.cd_pessoa_fisica = cd_pessoa_fisica_p;						
			
begin

open C01;
loop
fetch C01 into	
	ie_w;
exit when C01%notfound;
	begin
	
	if (nvl(ie_w,'N') = 'S') then
		ds_retorno_w := 'S';
		exit;		
	end if;
	
	end;
end loop;
close C01;

return	ds_retorno_w;

end adep_exibir_as;
/