create or replace
function Fa_obter_se_saldo_pendente(	nr_sequencia_p		Number,
					ie_opcao_p		Varchar2)
					return Varchar2 is

/*
E - Entrega
M - Medicamento
*/

ds_retorno_w	Varchar2(1);
					
begin

if	(ie_opcao_p = 'E') then

	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	fa_entrega_medicacao_item
	where	qt_dispensar < qt_gerado
	and	nr_seq_fa_entrega = nr_sequencia_p;

elsif	(ie_opcao_p = 'M') then
	
	select	decode(count(*),0,'N','S')
	into	ds_retorno_w
	from	fa_entrega_medicacao_item
	where	qt_dispensar < qt_gerado
	and	nr_sequencia = nr_sequencia_p;

end if;

return	ds_retorno_w;

end Fa_obter_se_saldo_pendente;
/