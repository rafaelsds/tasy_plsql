create or replace
function dmed_obter_diferenca_rateio
			(	vl_mensalidade_p	number,
				nr_titulo_p		number,
				dt_referencia_p		date,
				nr_seq_mensalidade_p	number)
				return number is
				
vl_diferenca_rateio_w		number(15,2);
vl_total_rec_w			number(15,2);
vl_total_itens_rec_w		number(15,2);
vl_recebido_w			number(15,2);
dt_ref_inicial_w		date;
dt_ref_final_w			date;

begin
dt_ref_inicial_w	:= trunc(dt_referencia_p, 'mm');
dt_ref_final_w		:= fim_dia(fim_mes(dt_referencia_p));

select	sum(l.vl_recebido)
into	vl_total_rec_w	
from	titulo_receber		r,
	titulo_receber_liq	l
where	r.nr_titulo	= nr_titulo_p 
and	r.nr_titulo 	= l.nr_titulo(+)
and	l.dt_recebimento between dt_ref_inicial_w and dt_ref_final_w
and	exists	(select	1
		from	dmed_regra_tipo_tit	w
		where	w.ie_tipo_receber		= l.cd_tipo_recebimento
		and	nvl(w.ie_prestadora_ops,'P')	= 'O')
and	exists	(select	1
		from	dmed_regra_origem_tit	w
		where	w.ie_origem_titulo	= r.ie_origem_titulo);
		
vl_recebido_w	:= (vl_total_rec_w / vl_mensalidade_p);

select	sum(round(vl_recebido_w * d.vl_item, 2))
into	vl_total_itens_rec_w	
from	pls_mensalidade_seg_item	d,
	pls_mensalidade_segurado	e
where	e.nr_sequencia		= d.nr_seq_mensalidade_seg
and	e.nr_seq_mensalidade	= nr_seq_mensalidade_p
and	((exists	(select d.nr_seq_tipo_lanc
			from	dmed_regra_tipo_lanc	z,
				dmed_regra_tipo_item	t
			where	t.nr_sequencia		= z.nr_seq_dmed_item
			and 	d.ie_tipo_item		= t.ie_tipo_item
			and	z.nr_tipo_lanc_adic	= d.nr_seq_tipo_lanc))
or	((select	count(1)
	from	dmed_regra_tipo_lanc	z,
		dmed_regra_tipo_item	t
	where 	t.nr_sequencia		= z.nr_seq_dmed_item
	and 	d.ie_tipo_item		= t.ie_tipo_item
	and	z.nr_tipo_lanc_adic	= d.nr_seq_tipo_lanc
	and	rownum			= 1) = 0));


vl_diferenca_rateio_w	:= vl_total_rec_w - vl_total_itens_rec_w;

return	vl_diferenca_rateio_w;

end dmed_obter_diferenca_rateio;
/