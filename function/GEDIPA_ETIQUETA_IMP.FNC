create or replace
function gedipa_etiqueta_imp(	nr_seq_processo_p	Number)
								return Varchar2 is

ds_retorno_w	varchar2(1)	:= 'S';
								
begin

select	coalesce(max(ie_impresso), 'N')
into	ds_retorno_w
from	adep_processo_frac
where	nr_seq_processo = nr_seq_processo_p;

return	ds_retorno_w;

end gedipa_etiqueta_imp;
/
