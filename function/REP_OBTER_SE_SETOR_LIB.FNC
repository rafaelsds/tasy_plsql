create or replace function rep_obter_se_setor_lib (
	nr_atendimento_p	IN atendimento_paciente.nr_atendimento%type,
	nm_usuario_p		IN usuario.nm_usuario%type,
	cd_perfil_p			IN perfil.cd_perfil%type
) return varchar2 is

	ie_setor_atendimento_w	varchar2(1 CHAR);
	ie_unidade_atend_w		varchar2(1 CHAR);
	ie_retorno_w			varchar2(1 CHAR) := 'S';

begin

	select	nvl(max('S'),'N')
	into	ie_setor_atendimento_w
	from	usuario_setor_v b,
			atend_paciente_unidade a
	where	a.nr_atendimento = nr_atendimento_p
	and		a.cd_setor_atendimento = b.cd_setor_atendimento
	and		b.nm_usuario = nm_usuario_p;

	select	nvl(max('S'),'N')
	into	ie_unidade_atend_w
	from	atend_paciente_unidade
	where	nr_atendimento = nr_atendimento_p;

	if	(ie_setor_atendimento_w = 'N') and 
		(ie_unidade_atend_w = 'S') then

		ie_retorno_w := 'N';

	end if;

	return	ie_retorno_w;

end rep_obter_se_setor_lib;
/