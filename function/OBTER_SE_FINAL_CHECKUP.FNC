create or replace 
FUNCTION Obter_Se_Final_Checkup(
			nr_atendimento_p	Number,
			ie_opcao_p		Varchar2)
			return varchar2 is

qt_existe_w	Number(10);

BEGIN

if	(ie_opcao_p = 'N') then
	begin
	select	count(*)
	into	qt_existe_w
	from	checkup
	where	nr_atendimento	= nr_atendimento_p
	and	dt_fim_real is not null;
	end;
else
	begin
	select	count(*)
	into	qt_existe_w
	from	checkup
	where	nr_atendimento	= nr_atendimento_p
	and	dt_fim_interno is not null;
	end;
end if;

if	(qt_existe_w > 0) then
	return 'S';
else	
	return 'N';
end if;

END Obter_Se_Final_Checkup;
/
