create or replace
function obter_codigo_responsavel(nr_atendimento_p	number)
				return number is

ie_responsavel_w	number(5);

begin
select	nvl(max(campo_numerico(ie_responsavel)),2)
into	ie_responsavel_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

return 	ie_responsavel_w;

end obter_codigo_responsavel;
/