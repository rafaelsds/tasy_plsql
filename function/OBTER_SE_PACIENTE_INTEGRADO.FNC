create or replace
function obter_se_paciente_integrado ( nr_sequencia_p	number,
										ie_opcao_p		varchar2 default 'I')
									 return			varchar2 is				

ie_retorno_w					varchar2(1)	:= 'N';
ds_retorno_w					patient_query_pf.cd_pessoa_fisica%type;

ds_patient_cpf_w				patient_query.ds_patient_cpf%type;
ds_patient_cns_w				patient_query.ds_patient_cns%type;
ds_user_id_w					patient_query.ds_user_id%type;
ds_issuer_w						patient_query.ds_issuer%type;
ds_patient_oid_name_w			patient_query_pf.ds_patient_oid_name%type;
ds_patient_oid_code_w			patient_query_pf.ds_patient_oid_code%type;

begin

if ( nr_sequencia_p is not null ) then

	select  max(ds_patient_cpf),
			max(ds_patient_cns),
			max(ds_user_id),
			max(ds_issuer)
	into	ds_patient_cpf_w,
			ds_patient_cns_w,
			ds_user_id_w,
			ds_issuer_w
	from   	patient_query
	where   nr_sequencia = nr_sequencia_p;
	
	
	if ( ds_patient_cpf_w is not null ) then
	
		ds_patient_oid_name_w := '2.16.840.1.113883.13.237';
		ds_patient_oid_code_w := ds_patient_cpf_w;
	
		Select  nvl(max('S'),'N'),
				max(cd_pessoa_fisica)
		into	ie_retorno_w,
				ds_retorno_w
		from	patient_query_pf
		where	ds_patient_oid_name = ds_patient_oid_name_w
		and		upper(DS_PATIENT_OID_CODE) = upper(ds_patient_cpf_w);			
	
	end if;
	
	if ( ie_retorno_w = 'N' and ds_patient_cns_w is not null) then
	
		ds_patient_oid_name_w := '2.16.840.1.113883.13.236';
		ds_patient_oid_code_w := ds_patient_cns_w;

		Select  nvl(max('S'),'N'),
				max(cd_pessoa_fisica)
		into	ie_retorno_w,
				ds_retorno_w
		from	patient_query_pf
		where	ds_patient_oid_name = ds_patient_oid_name_w
		and		upper(DS_PATIENT_OID_CODE) = upper(ds_patient_cns_w);	


	end if;
	
	if ( ie_retorno_w = 'N'  and ds_user_id_w is not null) then
	
		ds_patient_oid_name_w := ds_issuer_w;
		ds_patient_oid_code_w := ds_user_id_w;

		Select  nvl(max('S'),'N'),
				max(cd_pessoa_fisica)
		into	ie_retorno_w,
				ds_retorno_w
		from	patient_query_pf
		where	ds_patient_oid_name = ds_issuer_w
		and		upper(DS_PATIENT_OID_CODE) = upper(ds_user_id_w);
	
	end if;
	
	if ( ie_retorno_w = 'N'  and ds_patient_cpf_w is not null) then
	
		select  nvl(max('S'),'N'),
				max(cd_pessoa_fisica)
		into	ie_retorno_w,
				ds_retorno_w
		from    pessoa_fisica
		where   upper(nr_cpf) = upper(ds_patient_cpf_w);
	
	
	end if;
	
	if ( ie_retorno_w = 'N'  and ds_patient_cns_w is not null) then
	
		select  nvl(max('S'),'N'),
				max(cd_pessoa_fisica)
		into	ie_retorno_w,
				ds_retorno_w
		from    pessoa_fisica
		where   upper(nr_cartao_nac_sus) = upper(ds_patient_cns_w);
	
		
	end if;
	

end if;


if ( ie_opcao_p = 'C') then

	return	nvl(ds_retorno_w,'0');

else

	return	ie_retorno_w;

end if;

end obter_se_paciente_integrado;
/