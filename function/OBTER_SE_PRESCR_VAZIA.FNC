create or replace
function Obter_se_prescr_vazia(	nr_prescricao_p		number)
 		    	return varchar2 is

ds_valor_w	number(10);
ds_retorno_w	varchar2(1);			
			
begin
begin
Select 	nvl(sum(ie_reg),0)
into	ds_valor_w
from(
	select	1 ie_reg
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_dieta
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	rep_jejum
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	nut_paciente
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	nut_pac
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_solucao
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_material_cid
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_medica_albumina
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_gasoterapia
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_solic_bco_sangue
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_recomendacao
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	hd_prescricao
	where	nr_prescricao = nr_prescricao_p
	union
	select	1
	from	prescr_leite_deriv
	where	nr_prescricao = nr_prescricao_p);
exception
when others then
		ds_valor_w := 0;
end;

if  (nvl(ds_valor_w,0) > 0) then
      ds_retorno_w := 'N';
else 
      ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end Obter_se_prescr_vazia;
/
