create or replace
function obter_se_material_exame_rot(	nr_seq_rotina_p		number,
					nr_seq_material_p	number)
					return varchar is
					
ie_possui_w	varchar2(1);
nr_seq_exame_w	number(15);

Cursor C01 is
	select	nr_seq_exame
	from	exame_lab_rotina
	where	nr_seq_rotina = nr_seq_rotina_p;

begin

open C01;
loop
fetch C01 into	
	nr_seq_exame_w;
exit when C01%notfound;
	begin
	if	(nvl(ie_possui_w,'S') = 'S') and
		(nr_seq_exame_w is not null) then
	
		select	DECODE(count(*),0,'N','S')
		into	ie_possui_w
		from	exame_lab_material a,
				exame_lab_rotina b
		where	b.nr_seq_exame = a.nr_seq_exame
		and		b.nr_seq_rotina = nr_seq_rotina_p
		and		a.nr_seq_material = nr_seq_material_p
		and		b.nr_seq_exame = nr_seq_exame_w;
	end if;
	
	end;
end loop;
close C01;

return	ie_possui_w;

end obter_se_material_exame_rot;
/
