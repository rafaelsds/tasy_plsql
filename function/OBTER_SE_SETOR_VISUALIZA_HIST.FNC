create or replace
function obter_se_setor_visualiza_hist(
				nr_seq_historico_p	number,
				cd_setor_p		number,
				ie_visualiza_inclui_p	varchar2 default 'V',
				nm_usuario_p		varchar2 default null)
				return varchar2 is
				
/*ie_visualiza_inclui_p
I - Incluindo
V - Visualizando
*/

ds_retorno_w			varchar2(1)	:= 'S';
nr_seq_tipo_w			number(10);
cd_cargo_w			number(10)	:= 0;
nm_usuario_w			varchar2(15);

cursor c01 is
	select	ie_visualiza
	from	man_tipo_hist_setor
	where	nr_seq_tipo_hist = nr_seq_tipo_w
	and	nvl(cd_setor_atendimento,nvl(cd_setor_p,0)) = nvl(cd_setor_p,0)
	and	nvl(cd_cargo,nvl(cd_cargo_w,0))	= nvl(cd_cargo_w,0)
	and	nvl(nm_usuario_regra,nvl(nm_usuario_w,'X')) = nvl(nm_usuario_w,'X')
	order by nm_usuario_regra desc,
		cd_cargo desc,
		cd_setor_atendimento desc,
		nr_sequencia;

begin

begin
select	nr_seq_tipo
into	nr_seq_tipo_w
from	man_ordem_serv_tecnico
where	nr_sequencia	= nr_seq_historico_p;
exception when others then
	nr_seq_tipo_w	:= 0;
end;

if	(nr_seq_tipo_w > 0) then
	begin
	nm_usuario_w	:= nvl(nm_usuario_p,wheb_usuario_pck.get_nm_usuario);

	begin
	cd_cargo_w := to_number(obter_dados_usuario_opcao(nm_usuario_w,'R'));
	exception when others then
		cd_cargo_w	:= 0;
	end;

	open C01;
	loop
	fetch C01 into	
		ds_retorno_w;
	exit when C01%notfound;
	end loop;
	close C01;
	end;
end if;

if	(ds_retorno_w = 'V') and
	(ie_visualiza_inclui_p = 'I') then
	ds_retorno_w := 'N';
end if;

return ds_retorno_w;

end obter_se_setor_visualiza_hist;
/