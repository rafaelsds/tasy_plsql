create or replace
function Obter_desc_tipo_mensagem_hl7 (nr_seq_tipo_msg_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(100);

begin
select	max(ds_tipo)
into	ds_retorno_w
from	hl7_tipo_mensagem
where	nr_sequencia = 	nr_seq_tipo_msg_p;

return	ds_retorno_w;

end Obter_desc_tipo_mensagem_hl7;
/