create or replace
function man_obter_ordens_dia(	dt_referencia_p		date,
				ie_opcao_p		varchar2,
				nr_seq_gerencia_p		number,
				nr_seq_grupo_des_p	number,
				ie_tipo_prev_p		varchar2,
				nm_usuario_p		varchar2)
 		    		return varchar2 is

ds_retorno_w			varchar2(255);
dt_referencia_w			date;

begin

dt_referencia_w		:= trunc(dt_referencia_p,'dd');

if	(ie_opcao_p	= 'E') then /* Executadas */
	select	count(*)
	into	ds_retorno_w
	from	(
		select	a.nr_sequencia
		from	pessoa_fisica p,
			man_ordem_ativ_prev	y,
			setor_atendimento w,
			man_localizacao z,
			man_estagio_processo	b,
			grupo_desenvolvimento c,
			man_ordem_servico	a,
			w_resumo_prev_os_item r
		where	a.nr_seq_estagio		= b.nr_sequencia
		and	a.nr_sequencia		= y.nr_seq_ordem_serv
		and   	r.nr_seq_ordem       = a.nr_sequencia
		and   	r.ie_tipo_prev = ie_tipo_prev_p
		and   	r.nm_usuario   = nm_usuario_p
		and   	r.dt_previsao  = dt_referencia_w
		and   	a.nr_seq_localizacao = z.nr_sequencia(+)
		and   	z.cd_setor = w.cd_setor_atendimento(+)
		and   	a.nr_seq_grupo_des   = c.nr_sequencia
		and   	a.cd_pessoa_solicitante = p.cd_pessoa_fisica
		and	y.dt_prevista between trunc(dt_referencia_w,'dd') and fim_dia(dt_referencia_w)
		and   	(nr_seq_gerencia_p is null or c.nr_seq_gerencia = nr_seq_gerencia_p)
		and   	(nr_seq_grupo_des_p is null or a.nr_seq_grupo_des = nr_seq_grupo_des_p)
		and	(nvl(b.ie_situacao_os,'0') <> '3' or exists	(	select	1
								from	man_ordem_serv_ativ x
								where	x.nr_seq_ordem_serv = a.nr_sequencia
								and	x.nm_usuario_exec = y.nm_usuario_prev
								and	x.dt_atividade between trunc(y.dt_prevista,'dd') and fim_dia(y.dt_prevista)))
		and	y.dt_real is not null
		and	y.pr_atividade = 100
		and exists(	select	1
				from	usuario_grupo_des p
				where	p.nr_seq_grupo = a.nr_seq_grupo_des
				and   p.nm_usuario_grupo = y.nm_usuario_prev)
		group by	a.nr_sequencia,
			a.ie_classificacao,
			a.dt_ordem_servico,
			a.ie_prioridade,
			w.ds_setor_atendimento,
			p.nm_pessoa_fisica,
			a.ds_dano_breve,
			a.ie_classificacao,
			b.ds_estagio,
			z.ds_localizacao,
			a.ie_prioridade_desen,
			obter_tipo_os(a.dt_ordem_servico),
			a.ie_obriga_news,
			a.ds_just_ret_news,
			a.nr_seq_classif);
elsif	(ie_opcao_p	= 'EE') then /* Em execu��o */ 
	select	count(*)
	into	ds_retorno_w
	from	(
		select	a.nr_sequencia
		from	pessoa_fisica p,
			man_ordem_ativ_prev	y,
			setor_atendimento w,
			man_localizacao z,
			man_estagio_processo	b,
			grupo_desenvolvimento c,
			man_ordem_servico	a,
			w_resumo_prev_os_item r
		where	a.nr_seq_estagio		= b.nr_sequencia
		and	a.nr_sequencia		= y.nr_seq_ordem_serv
		and	a.nr_seq_localizacao = z.nr_sequencia(+)
		and	z.cd_setor = w.cd_setor_atendimento(+)
		and	r.nr_seq_ordem       = a.nr_sequencia
		and	r.IE_TIPO_PREV = ie_tipo_prev_p
		and	r.nm_usuario   = nm_usuario_p
		and	r.dt_previsao  = dt_referencia_w
		and	a.nr_seq_grupo_des   = c.nr_sequencia
		and	a.cd_pessoa_solicitante = p.cd_pessoa_fisica
		and	y.dt_prevista between trunc(dt_referencia_w,'dd') and fim_dia(dt_referencia_w)
		and	(nr_seq_gerencia_p is null or c.nr_seq_gerencia = nr_seq_gerencia_p)
		and	(nr_seq_grupo_des_p is null or a.nr_seq_grupo_des = nr_seq_grupo_des_p)
		and	a.ie_status_ordem <> '3'
		and	(b.ie_desenv = 'S' or b.ie_tecnologia = 'S')
		and	(nvl(b.ie_situacao_os,'0') <> '3' or exists(	select	1
							from	man_ordem_serv_ativ x
							where	x.nr_seq_ordem_serv = a.nr_sequencia
							and	x.nm_usuario_exec = y.nm_usuario_prev
							and	x.dt_atividade between trunc(y.dt_prevista,'dd') and fim_dia(y.dt_prevista)
											    )
			)
		and exists(	select	1
				from	usuario_grupo_des p
				where	p.nr_seq_grupo = a.nr_seq_grupo_des
				and	p.nm_usuario_grupo = y.nm_usuario_prev)
		and exists(	select 	1
				from	man_ordem_serv_ativ
			       	where	nr_seq_ordem_serv	= a.nr_sequencia
				and	dt_fim_atividade is null
				and	nm_usuario_exec		= y.nm_usuario_prev
				and	dt_atividade between trunc(sysdate) and fim_dia(sysdate))
		group by	a.nr_sequencia,
			a.ie_classificacao,
			a.dt_ordem_servico,
			a.ie_prioridade,
			w.ds_setor_atendimento,
			p.nm_pessoa_fisica,
			a.ds_dano_breve,
			a.ie_classificacao,
			b.ds_estagio,
			z.ds_localizacao,
			a.ie_prioridade_desen,
			obter_tipo_os(a.dt_ordem_servico),
			a.ie_obriga_news,
			a.ds_just_ret_news,
			a.nr_seq_classif);
elsif	(ie_opcao_p	= 'NE') then /* N�o executadas */
	select	count(*)
	into	ds_retorno_w
	from	(
		select	a.nr_sequencia
		from	pessoa_fisica p,
			man_ordem_ativ_prev	y,
			setor_atendimento w,
			man_localizacao z,
			man_estagio_processo	b,
			grupo_desenvolvimento c,
			man_ordem_servico	a,
			w_resumo_prev_os_item r
		where	a.nr_seq_estagio		= b.nr_sequencia
		and	a.nr_sequencia		= y.nr_seq_ordem_serv
		and	a.nr_seq_localizacao = z.nr_sequencia(+)
		and	r.nr_seq_ordem       = a.nr_sequencia
		and	r.IE_TIPO_PREV = ie_tipo_prev_p
		and	r.nm_usuario   = nm_usuario_p
		and	r.dt_previsao  = dt_referencia_w
		and	z.cd_setor = w.cd_setor_atendimento(+)
		and	a.nr_seq_grupo_des   = c.nr_sequencia
		and	a.cd_pessoa_solicitante = p.cd_pessoa_fisica
		and	y.dt_prevista between trunc(dt_referencia_w,'dd') and fim_dia(dt_referencia_w)
		and	(nr_seq_gerencia_p is null or c.nr_seq_gerencia = nr_seq_gerencia_p)
		and	(nr_seq_grupo_des_p is null or a.nr_seq_grupo_des = nr_seq_grupo_des_p)
		and	a.ie_status_ordem <> '3'
		and	(b.ie_desenv = 'S' or b.ie_tecnologia = 'S')
		and	(nvl(b.ie_situacao_os,'0') <> '3' or exists(	select	1
							from	man_ordem_serv_ativ x
							where	x.nr_seq_ordem_serv = a.nr_sequencia
							and	x.nm_usuario_exec = y.nm_usuario_prev
							and	x.dt_atividade between trunc(y.dt_prevista,'dd') and fim_dia(y.dt_prevista)
											    )
			)
		and	(y.dt_real is null or y.pr_atividade <> 100)
		and exists(	select	1
				from	usuario_grupo_des p
		      		where	p.nr_seq_grupo = a.nr_seq_grupo_des
				and	p.nm_usuario_grupo = y.nm_usuario_prev)
		and not exists(	select	1
			       	from	man_ordem_serv_ativ
			       	where	nr_seq_ordem_serv	= a.nr_sequencia
			       	and	dt_fim_atividade is null
			       	and	nm_usuario_exec	= y.nm_usuario_prev
			       	and	dt_atividade between trunc(sysdate) and fim_dia(sysdate)
			       )
	group by	a.nr_sequencia,
		a.ie_classificacao,
		a.dt_ordem_servico,
		a.ie_prioridade,
		w.ds_setor_atendimento,
		p.nm_pessoa_fisica,
		a.ds_dano_breve,
		a.ie_classificacao,
		b.ds_estagio,
		z.ds_localizacao,
		a.ie_prioridade_desen,
		obter_tipo_os(a.dt_ordem_servico),
		a.ie_obriga_news,
		a.ds_just_ret_news,
		a.nr_seq_classif);
elsif	(ie_opcao_p	= 'EF') then /* Executadas  fora da previs�o*/
	select	count(*)
	into	ds_retorno_w
	from	(
		select	a.nr_sequencia
		from	pessoa_fisica p,
			man_ordem_ativ_prev	y,
			setor_atendimento w,
			man_localizacao z,
			man_estagio_processo	b,
			grupo_desenvolvimento c,
			man_ordem_servico	a,
			w_resumo_prev_os_item r
		where	a.nr_seq_estagio		= b.nr_sequencia
		and	a.nr_sequencia		= y.nr_seq_ordem_serv
		and	r.nr_seq_ordem       = a.nr_sequencia
		and	r.ie_tipo_prev = ie_tipo_prev_p
		and	r.nm_usuario   = nm_usuario_p
		and	r.dt_previsao  = dt_referencia_w
		and	a.nr_seq_localizacao = z.nr_sequencia(+)
		and	z.cd_setor = w.cd_setor_atendimento(+)
		and	a.nr_seq_grupo_des   = c.nr_sequencia
		and	a.cd_pessoa_solicitante = p.cd_pessoa_fisica
		and	y.dt_prevista between trunc(dt_referencia_w,'dd') and fim_dia(dt_referencia_w)
		and	(nr_seq_gerencia_p is null or c.nr_seq_gerencia = nr_seq_gerencia_p)
		and	(nr_seq_grupo_des_p is null or a.nr_seq_grupo_des = nr_seq_grupo_des_p)
		and	(nvl(b.ie_situacao_os,'0') <> '3' or exists(	select	1
							from	man_ordem_serv_ativ x
							where	x.nr_seq_ordem_serv = a.nr_sequencia
							and	x.nm_usuario_exec = y.nm_usuario_prev
							and	x.dt_atividade between trunc(y.dt_prevista,'dd') and fim_dia(y.dt_prevista)))
		and	y.dt_real is not null
		and	y.pr_atividade = 100
		and	y.ie_fora_planej_diario = 'S'
		and exists(	select	1
				from	usuario_grupo_des p
				where	p.nr_seq_grupo = a.nr_seq_grupo_des
		      		and	p.nm_usuario_grupo = y.nm_usuario_prev)
		group by	a.nr_sequencia,
			a.ie_classificacao,
			a.dt_ordem_servico,
			a.ie_prioridade,
			w.ds_setor_atendimento,
			p.nm_pessoa_fisica,
			a.ds_dano_breve,
			a.ie_classificacao,
			b.ds_estagio,
			z.ds_localizacao,
			a.ie_prioridade_desen,
			obter_tipo_os(a.dt_ordem_servico),
			a.ie_obriga_news,
			a.ds_just_ret_news,
			a.nr_seq_classif);
elsif	(ie_opcao_p	= 'T') then /* Todas */
	select	count(*)
	into	ds_retorno_w
	from	(
		select	a.nr_sequencia
		from	man_ordem_ativ_prev	y,
			setor_atendimento w,
			man_localizacao z,
			man_estagio_processo	b,
			grupo_desenvolvimento c,
			man_ordem_servico	a
		where	a.nr_seq_estagio		= b.nr_sequencia
		and	a.nr_sequencia		= y.nr_seq_ordem_serv
		and   	a.nr_seq_localizacao = z.nr_sequencia(+)
		and   	z.cd_setor = w.cd_setor_atendimento(+)
		and   	a.nr_seq_grupo_des   = c.nr_sequencia
		and   	y.dt_prevista between trunc(dt_referencia_w,'dd') and fim_dia(dt_referencia_w)
		and   	(nr_seq_gerencia_p is null or c.nr_seq_gerencia = nr_seq_gerencia_p)
		and   	(nr_seq_grupo_des_p is null or a.nr_seq_grupo_des = nr_seq_grupo_des_p)
		and exists(	select	1
				from	usuario_grupo_des t
				where	t.nr_seq_grupo = a.nr_seq_grupo_des
				and   t.nm_usuario_grupo = y.nm_usuario_prev)
		group by	a.nr_sequencia,
			a.ie_classificacao,
			a.dt_ordem_servico,
			a.ie_prioridade,
			w.ds_setor_atendimento,
			a.ds_dano_breve,
			a.ie_classificacao,
			b.ds_estagio,
			z.ds_localizacao,
			a.ie_prioridade_desen,
			obter_tipo_os(a.dt_ordem_servico),
			a.ie_obriga_news,
			a.ds_just_ret_news,
			a.nr_seq_classif);
elsif	(ie_opcao_p	= 'ET') then /* Executadas*/
	select	count(*)
	into	ds_retorno_w
	from	(
		select	a.nr_sequencia
		from	man_ordem_ativ_prev	y,
			setor_atendimento w,
			man_localizacao z,
			man_estagio_processo	b,
			grupo_desenvolvimento c,
			man_ordem_servico	a
		where	a.nr_seq_estagio		= b.nr_sequencia
		and	a.nr_sequencia		= y.nr_seq_ordem_serv
		and   	a.nr_seq_localizacao = z.nr_sequencia(+)
		and   	z.cd_setor = w.cd_setor_atendimento(+)
		and   	a.nr_seq_grupo_des   = c.nr_sequencia
		and   	y.dt_prevista between trunc(dt_referencia_w,'dd') and fim_dia(dt_referencia_w)
		and   	(nr_seq_gerencia_p is null or c.nr_seq_gerencia = nr_seq_gerencia_p)
		and   	(nr_seq_grupo_des_p is null or a.nr_seq_grupo_des = nr_seq_grupo_des_p)
		and	y.dt_real is not null
		and	y.pr_atividade = 100
		and exists(	select	1
				from	usuario_grupo_des t
				where	t.nr_seq_grupo = a.nr_seq_grupo_des
				and   t.nm_usuario_grupo = y.nm_usuario_prev)
		group by	a.nr_sequencia,
			a.ie_classificacao,
			a.dt_ordem_servico,
			a.ie_prioridade,
			w.ds_setor_atendimento,
			a.ds_dano_breve,
			a.ie_classificacao,
			b.ds_estagio,
			z.ds_localizacao,
			a.ie_prioridade_desen,
			obter_tipo_os(a.dt_ordem_servico),
			a.ie_obriga_news,
			a.ds_just_ret_news,
			a.nr_seq_classif);
elsif	(ie_opcao_p	= 'DP') then /* Defeitos pendentes*/
	select	count(1)
	into	ds_retorno_w
	from	man_estagio_processo b,
		man_ordem_servico a
	where	1=1
	and	a.ie_classificacao = 'E'
	and	a.nr_seq_estagio = b.nr_sequencia
	and	b.ie_desenv = 'S'
	and	b.ie_aguarda_cliente = 'N'
	and	b.nr_sequencia <> 261
	and	a.ie_status_ordem in ('1','2')
	and	(a.nr_seq_grupo_des = nr_seq_grupo_des_p or nr_seq_grupo_des_p is null)
	and	(substr(obter_gerencia_grupo_desen(a.nr_seq_grupo_des,'C'),1,20)  = nr_seq_gerencia_p or nr_seq_gerencia_p is null);
end if;

return	ds_retorno_w;

end man_obter_ordens_dia;
/
