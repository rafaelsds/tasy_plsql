create or replace
function pls_obter_tipo_ped_impug	(	nr_seq_conta_p	pls_processo_conta.nr_sequencia%type,
						ie_opcao_p	varchar2)
					return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 
ie_tipo_pedido_w	pls_proc_conta_pedido.ie_tipo_pedido%type;
vl_pedido_w		pls_proc_conta_pedido.vl_pedido%type;
ds_memoria_calc_w	pls_proc_conta_pedido.ds_memoria_calc%type;
ds_retorno_w		varchar2(4000);

begin

select	max(nvl(ie_tipo_pedido, '0')) ie_tipo_pedido, 
	max(nvl(vl_pedido, 0)) vl_pedido,
	max(nvl(ds_memoria_calc, '0')) ds_memoria_calc
into	ie_tipo_pedido_w,
	vl_pedido_w,
	ds_memoria_calc_w
from	pls_proc_conta_pedido	
where	nr_seq_proc_conta = nr_seq_conta_p;

if (ie_opcao_p = 'A') then
	if (ie_tipo_pedido_w = 'A') then
		ds_retorno_w := '(x)Anula��o';
	elsif((ie_tipo_pedido_w = '0') or (ie_tipo_pedido_w <> 'A')) then
		ds_retorno_w := '( )Anula��o';
	end if;
elsif(ie_opcao_p = 'R') then
	if (ie_tipo_pedido_w = 'R') then
		select	'(x)Redu��o do valor original'||chr(13)|| chr(10)||
			'(Valor ou porcentagem: '||campo_mascara_virgula(vl_pedido_w)||' )'||chr(13)||chr(10)||
			'Mem�ria de calculo: '||campo_mascara_virgula(ds_memoria_calc_w)
		into	ds_retorno_w	
		from	dual;
	elsif ((ie_tipo_pedido_w = '0') or (ie_tipo_pedido_w <> 'R')) then
		select	'( )Redu��o do valor original'||chr(13)|| chr(10)||
			'(Valor ou porcentagem:_______)'||chr(13)||chr(10)||
			'Mem�ria de calculo:__________ '
		into	ds_retorno_w	
		from 	dual;	
	end if;

elsif(ie_opcao_p = 'RA') then
	if (ie_tipo_pedido_w = 'RA') then
		select	'(x)Anula��o / Redu��o do valor original'||chr(13)||chr(10)||
			'(Valor ou porcentagem: '||campo_mascara_virgula(vl_pedido_w)||' )'||chr(13)||chr(10)||
			'Mem�ria de calculo: '||campo_mascara_virgula(ds_memoria_calc_w)
		into	ds_retorno_w	
		from 	dual;	
	elsif ((ie_tipo_pedido_w = '0') or (ie_tipo_pedido_w <> 'RA')) then
		select	'( )Anula��o / Redu��o do valor original'||chr(13)||chr(10)||
			'(Valor ou porcentagem:_________ )'||chr(13)||chr(10)||
			'Mem�ria de calculo:____________ '
		into	ds_retorno_w	
		from 	dual;	
	end if;
end if;	

return	ds_retorno_w;

end pls_obter_tipo_ped_impug;
/