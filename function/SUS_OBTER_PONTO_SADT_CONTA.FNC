create or replace
function sus_obter_ponto_sadt_conta(	nr_interno_conta_p		number,
					ie_opcao_p			number)
					return number is
					
vl_retorno_w			number(15,4)	:=0;					
vl_ponto_sadt_w			number(15,2)	:= 0;
qt_ato_sadt_w			number(10)	:= 0;
vl_sadt_w			Number(15,2)	:= 0;

/*
ie_opcao_p	
	1 : Vl Ponto Sadt.
	2 : Qt Ponto
	3 : Vl SADT
*/

begin

begin
select	nvl(sum(Sus_Obter_Valor_Sadt(b.nr_sequencia)),0) 
into	vl_sadt_w
from	sus_estrutura_procedimento_v	x,
	procedimento_paciente		b,
	conta_paciente			c
where	c.nr_interno_conta 		= nr_interno_conta_p
and	c.nr_interno_conta		= b.nr_interno_conta
and	b.cd_procedimento		= x.cd_procedimento
and	b.ie_origem_proced		= x.ie_origem_proced
and	nvl(Sus_Obter_Valor_Sadt(b.nr_sequencia),0) <= nvl(Sus_Obter_Valor_Item_Proc(b.nr_sequencia,1),0)
and	c.ie_cancelamento		is null
and	b.cd_motivo_exc_conta		is null;
exception
	when no_data_found then
	begin
	qt_ato_sadt_w	:= 0;
	end;
end;		

begin
select	nvl(sum(Sus_Obter_Ponto_Sadt(b.nr_sequencia)),0)
into	qt_ato_sadt_w
from	sus_estrutura_procedimento_v x,
	procedimento_paciente b,
	conta_paciente c
where	c.nr_interno_conta 		= nr_interno_conta_p
and	c.nr_interno_conta		= b.nr_interno_conta
and	b.cd_procedimento		= x.cd_procedimento
and	b.ie_origem_proced		= x.ie_origem_proced
and	x.cd_grupo			= 2
and	sus_obter_preco_proced(c.cd_estabelecimento,b.dt_procedimento,1,b.cd_Procedimento,b.ie_origem_proced,1)=0
and	Sus_Obter_Ponto_Sadt(b.nr_sequencia) > 0
and	c.ie_cancelamento		is null
and	b.cd_motivo_exc_conta		is null;
exception
	when no_data_found then
	begin
	qt_ato_sadt_w	:= 0;
	end;
end;		
	
if	qt_ato_sadt_w = 0 then
	vl_ponto_sadt_w 	:= 0;
else
	vl_ponto_sadt_w 	:= (vl_sadt_w / qt_ato_sadt_w);
end if; 

if	(ie_opcao_p	= 1 ) then
	vl_retorno_w	:= vl_ponto_sadt_w;
elsif	(ie_opcao_p	= 2 ) then
	vl_retorno_w	:= qt_ato_sadt_w;
elsif	(ie_opcao_p	= 3 ) then
	vl_retorno_w	:=  vl_sadt_w;
end if;

return	vl_retorno_w;

end sus_obter_ponto_sadt_conta;
/
