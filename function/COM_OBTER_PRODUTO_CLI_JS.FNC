create or replace
function com_obter_produto_cli_js(nr_seq_cliente_p	number)
 		    	return varchar2 is

ie_produto_w	varchar2(15);			
			
begin

if	(nvl(nr_seq_cliente_p,0) > 0) then
	
	select	substr(max(ie_produto),1,15)
	into	ie_produto_w
	from	com_cliente
	where	nr_sequencia = nr_seq_cliente_p;
end if;
	
return	ie_produto_w;

end com_obter_produto_cli_js;
/