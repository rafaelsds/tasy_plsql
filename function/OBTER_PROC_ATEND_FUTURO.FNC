create or replace
function Obter_proc_atend_futuro(	cd_pessoa_fisica_p		Varchar2,
				ie_opcao_p		Varchar2)
						return Varchar2 is

/* IE_OPCAO_P
D - Descri��o
*/

ds_retorno_w		Varchar2(2000);
ds_procedimento_w		Varchar2(255);
nr_seq_proc_interno_w	number(15);
cd_procedimento_w	number(15);

Cursor C01 is
	select	substr(obter_exame_agenda(b.cd_procedimento, b.ie_origem_proced, b.nr_seq_proc_interno),1,240),
		b.cd_procedimento,
		b.nr_seq_proc_interno
	from   	atend_pac_futuro a,
		atend_pac_proced_previsto b
	where  	a.nr_sequencia = b.nr_seq_atend_futuro
	and	a.nr_atendimento is null
	and    	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	order by ie_proc_principal desc, 1 asc;

begin

open C01;
loop
fetch C01 into
	ds_procedimento_w,
	cd_procedimento_w,
	nr_seq_proc_interno_w;
exit when C01%notfound;
	begin
	if	(ie_opcao_p = 'D') then
		ds_retorno_w := substr(ds_retorno_w||ds_procedimento_w||', ',1,2000);
		
	end if;
	end;
end loop;
close C01;

ds_retorno_w := substr(ds_retorno_w,1,length(ds_retorno_w)-2);

return	ds_retorno_w; 

end Obter_proc_atend_futuro;
/
