create or replace
function TISS_OBTER_DT_DESPESA
		(cd_estabelecimento_p	number,
		cd_convenio_p		number,
		dt_conta_p		date,
		dt_periodo_inicial_p	date,
		dt_periodo_final_p	date,
		ie_mat_med_p		varchar2,
		ie_tipo_atend_conta_p	number,
		nr_interno_conta_p	number,
		ie_tipo_data_p		varchar2,
		ie_tiss_tipo_despesa_p	varchar2,
		cd_material_p 		number,
		dt_entrada_p		date default null,
		dt_alta_p		date default null,
		dt_mesano_referencia_p	date default null) return date is

/*
ie_mat_med_p	
	'S' - Materias e Medicamentos
	'T' - Taxas e Diarias
	
ie_tipo_data_p	
	'D' - Data da despesa
	'DI' - Data inicial da despesa
	'DF' - Data final da despesa
*/

ie_data_despesa_w	varchar2(40);
ie_data_inicial_w	varchar2(40);
ie_data_final_w		varchar2(40);
ds_versao_w		varchar2(20);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
nr_seq_familia_w	number(10);
dt_retorno_w		date;
dt_entrada_w		date;
dt_alta_w		date;
dt_mesano_Referencia_w	date;

cursor c01 is
select	nvl(IE_DATA_DESPESA, 'C'),
	nvl(IE_DATA_INICIAL,'C'),
	nvl(IE_DATA_FINAL,'C')
from	tiss_regra_data_mat
where	cd_estabelecimento						= cd_estabelecimento_p
and	nvl(cd_convenio,nvl(cd_convenio_p,0))				= nvl(cd_convenio_p,0)
and	nvl(cd_material,nvl(cd_material_p,0))				= nvl(cd_material_p,0)
and	nvl(cd_grupo_material,nvl(cd_grupo_material_w,0))		= nvl(cd_grupo_material_w,0)
and	nvl(cd_subgrupo_material,nvl(cd_subgrupo_material_w,0))		= nvl(cd_subgrupo_material_w,0)
and	nvl(cd_classe_material,nvl(cd_classe_material_w,0))		= nvl(cd_classe_material_w,0)
and	nvl(nr_seq_familia,nvl(nr_seq_familia_w,0))			= nvl(nr_seq_familia_w,0)
and	nvl(ie_tipo_atend_conta,nvl(ie_tipo_atend_conta_p,0))		= nvl(ie_tipo_atend_conta_p,0)
and	(((obter_se_projeto_versao(0,12,ds_versao_w,0) = 'N') and
	  (nvl(ie_tiss_tipo_despesa,nvl(ie_tiss_tipo_despesa_p,'0'))	= nvl(ie_tiss_tipo_despesa_p,'0'))) or
	 ((obter_se_projeto_versao(0,12,ds_versao_w,0) = 'S') and
	  (nvl(ie_tiss_tipo_desp_novo,nvl(ie_tiss_tipo_despesa_p,'0'))	= nvl(ie_tiss_tipo_despesa_p,'0')))
	)
order by	nvl(cd_material,0),
	nvl(cd_grupo_material,0),
	nvl(cd_subgrupo_material,0),
	nvl(cd_classe_material,0),
	nvl(nr_seq_familia,0),
	nvl(cd_convenio,0),
	nvl(ie_tipo_atend_conta,0),
	nvl(ie_tiss_tipo_despesa,'0'),
	nvl(ie_tiss_tipo_desp_novo,'0');

begin

begin
select 	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material,
	nr_seq_familia 
into  	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	nr_seq_familia_w
from	estrutura_material_v
where	cd_material 		= cd_material_p;
exception
when others then
	null;
end;


ie_data_despesa_w	:= tiss_convenio_pck.get_tiss_param_convenio(cd_convenio_p,cd_estabelecimento_p,'DD');

if	(dt_entrada_p	is not null) and
	(dt_mesano_referencia_p	is not null) then
	dt_entrada_w		:= nvl(dt_entrada_p,sysdate);
	dt_alta_w		:= nvl(dt_alta_p,sysdate);
	dt_mesano_Referencia_w	:= dt_mesano_Referencia_p;
else

	select	nvl(max(a.dt_entrada),sysdate),
		nvl(max(a.dt_alta),sysdate),
		max(b.dt_mesano_referencia)
	into	dt_entrada_w,
		dt_alta_w,
		dt_mesano_Referencia_w
	from	atendimento_paciente a,
		conta_paciente b
	where	a.nr_atendimento	= b.nr_atendimento
	and	b.nr_interno_conta	= nr_interno_conta_p;

end if;


select	nvl(max(tiss_obter_versao(cd_convenio_p,cd_estabelecimento_p,dt_mesano_Referencia_w)),'2.02.03')
into	ds_versao_w
from	dual;


if	(ie_data_despesa_w = 'R') then

	open c01;
	loop
	fetch c01 into
		ie_data_despesa_w,
		ie_data_inicial_w,
		ie_data_final_w;
	exit when c01%notfound;
		ie_data_despesa_w	:= ie_data_despesa_w;
		ie_data_inicial_w	:= ie_data_inicial_w;
		ie_data_final_w		:= ie_data_final_w;
	end loop;
	close c01;

end if;

dt_retorno_w			:= dt_conta_p;
if	(nvl(ie_tipo_data_p,'D') = 'D') then
	if	(ie_data_despesa_w = 'C') then
		dt_retorno_w		:= dt_conta_p;
	elsif	(ie_data_despesa_w = 'F') then
		dt_retorno_w		:= dt_periodo_final_p;
	elsif	(ie_data_despesa_w = 'I') then
		dt_retorno_w		:= dt_periodo_inicial_p;
	elsif	(ie_data_despesa_w = 'FM') then
		if	(ie_mat_med_p	   = 'S') then
			dt_retorno_w		:= dt_periodo_final_p;
		else	
			dt_retorno_w		:= dt_conta_p;
		end if;
	elsif	(ie_data_despesa_w = 'FT') then
		if	(ie_mat_med_p	   = 'T') then
			dt_retorno_w		:= dt_periodo_final_p;
		else	
			dt_retorno_w		:= dt_conta_p;
		end if;
	elsif	(ie_data_despesa_w = 'UM') then --Ultimo dia do mes da data do item
		begin
			dt_retorno_w	:= last_day(dt_conta_p);		
		exception
		when others then
			dt_retorno_w	:= dt_conta_p;
		end;		
		if	(dt_retorno_w > dt_periodo_final_p) then --Se o ultimo dia do mes for maior que o periodo final da conta, retorno o periodo final da conta
			dt_retorno_w	:= dt_periodo_final_p;
		end if;
	end if;
elsif	(nvl(ie_tipo_data_p,'D') = 'DI') then
	if	(ie_data_inicial_w = 'C') then
		dt_retorno_w		:= dt_conta_p;
	elsif	(ie_data_inicial_w = 'DE') then
		dt_retorno_w		:= dt_entrada_w;
	elsif	(ie_data_inicial_w = 'DA') then
		dt_retorno_w		:= dt_alta_w;
	elsif	(ie_data_inicial_w = 'DI') then
		dt_retorno_w 		:= dt_periodo_inicial_p;
	elsif	(ie_data_inicial_w = 'DF') then
		dt_retorno_w		:= dt_periodo_final_p;
	elsif	(ie_data_despesa_w = 'UM') then --Ultimo dia do mes da data do item
		begin
    dt_retorno_w := ESTABLISHMENT_TIMEZONE_UTILS.endOfMonth(dt_conta_p);
		exception
		when others then
			dt_retorno_w	:= dt_conta_p;			
		end;		
		if	(dt_retorno_w > dt_periodo_final_p) then --Se o Ultimo dia do mes for maior que o periodo final da conta, retorno o periodo final da conta
			dt_retorno_w	:= dt_periodo_final_p;
		end if;
	end if;
elsif	(nvl(ie_tipo_data_p,'D') = 'DF') then
	if	(ie_data_final_w = 'C') then
		dt_retorno_w		:= dt_conta_p;
	elsif	(ie_data_final_w = 'DE') then
		dt_retorno_w		:= dt_entrada_w;
	elsif	(ie_data_final_w = 'DA') then
		dt_retorno_w		:= dt_alta_w;
	elsif	(ie_data_final_w = 'DI') then
		dt_retorno_w 		:= dt_periodo_inicial_p;
	elsif	(ie_data_final_w = 'DF') then
		dt_retorno_w		:= dt_periodo_final_p;	
	elsif	(ie_data_despesa_w = 'UM') then --Ultimo dia do mes da data do item
		begin
      dt_retorno_w := ESTABLISHMENT_TIMEZONE_UTILS.endOfMonth(dt_conta_p);
		exception
		when others then
			dt_retorno_w	:= dt_conta_p;			
		end;		
		if	(dt_retorno_w > dt_periodo_final_p) then --Se o Ultimo dia do mes for maior que o periodo final da conta, retorno o periodo final da conta
			dt_retorno_w	:= dt_periodo_final_p;
		end if;
	end if;
end if;

return	dt_retorno_w;

end TISS_OBTER_DT_DESPESA;
/