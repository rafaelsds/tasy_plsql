create or replace
function Obter_Vl_cheque_Banco_Saldo(nr_seq_banco_saldo_p	number)
 		    	return number is
vl_cheque_w 	number(15,2);
vl_cheque_cp_w 	number(15,2);
vl_cheque_cr_w 	number(15,2);

begin

select 	sum(decode(c.ie_banco,'C',a.vl_cheque * -1,a.vl_cheque)) vl_cheque
into 	vl_cheque_cp_w
from 	cheque a,
	movto_trans_financ b,
	transacao_financeira c
where	a.nr_sequencia		= b.nr_seq_cheque_cp
and	b.nr_seq_trans_financ	= c.nr_sequencia
and	b.nr_seq_saldo_banco	= nr_seq_banco_saldo_p
and	dt_compensacao 	is null
and	dt_cancelamento is null;

select 	sum(decode(c.ie_banco,'C',a.vl_cheque * -1,a.vl_cheque)) vl_cheque_cr
into	vl_cheque_cr_w
from 	cheque_cr a,
	movto_trans_financ b,
	transacao_financeira c
where	a.nr_seq_cheque		= b.nr_seq_cheque
and	b.nr_seq_trans_financ	= c.nr_sequencia
and	b.nr_seq_saldo_banco	= nr_seq_banco_saldo_p
and	dt_compensacao 	is null;

vl_cheque_w := nvl(vl_cheque_cp_w,0) 	+ nvl(vl_cheque_cr_w,0);

return vl_cheque_w;

end Obter_Vl_cheque_Banco_Saldo;
/