create or replace
function pls_obter_seq_conselho_prof
			(	cd_medico_p		Varchar2)
 		    		return number is

ds_retorno_w			pessoa_fisica.nr_seq_conselho%type;

begin

select	nr_seq_conselho
into	ds_retorno_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_medico_p;

return	ds_retorno_w;

end pls_obter_seq_conselho_prof;
/
