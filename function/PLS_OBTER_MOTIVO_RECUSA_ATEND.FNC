create or replace
function pls_obter_motivo_recusa_atend
			(nr_seq_atendimento_p		number)
 		    	return varchar2 is

ds_mensagem_w		Varchar2(4000)	:= null;
nr_seq_atend_hist_w	number(10);

begin

select	max(nr_sequencia)
into	nr_seq_atend_hist_w
from	pls_atendimento_historico
where	nr_seq_atendimento = nr_seq_atendimento_p;

/* Francisco - OS798738, comentei pois n�o � poss�vel fazer esse tratamento com LONG e esse processo n�o � mais usado
select	substr(ds_historico,instr(ds_historico ,'.')+1,length(ds_historico)) 
into	ds_mensagem_w
from	pls_atendimento_historico
where	nr_sequencia =  nr_seq_atend_hist_w;
*/

return	ds_mensagem_w;

end pls_obter_motivo_recusa_atend;
/