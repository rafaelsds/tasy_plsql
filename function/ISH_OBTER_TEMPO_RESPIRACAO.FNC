create or replace function ish_obter_tempo_respiracao ( nr_seq_episodio_p   number,
                                                        nr_atendimento_p    number default null) return number is
    
    nr_tempo_w  number;
    
begin

    nr_tempo_w:= mdc_obter_total_horas_vm(nr_seq_episodio_p);

    return nr_tempo_w;
    
end ish_obter_tempo_respiracao;
/
