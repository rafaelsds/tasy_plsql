CREATE OR REPLACE
FUNCTION Obter_hr_conta_fim_dia
			(	cd_convenio_p		Number,
				cd_estabelecimento_p	Number)
				return date is

hr_conta_fim_dia_w date;				

BEGIN

select	max(hr_conta_fim_dia)
into 	hr_conta_fim_dia_w
from	convenio_estabelecimento
where	cd_convenio		= cd_convenio_p
and		cd_estabelecimento	= cd_estabelecimento_p;

return	hr_conta_fim_dia_w;

END Obter_hr_conta_fim_dia;
/