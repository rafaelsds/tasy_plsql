create or replace
function	obter_sequencia_nota_fiscal(nr_nota_fiscal_p number)
				return number is

nr_sequencia_w	number(15);

begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	nota_fiscal
where	nr_nota_fiscal	= nr_nota_fiscal_p;

return	nr_sequencia_w;

end	obter_sequencia_nota_fiscal;
/