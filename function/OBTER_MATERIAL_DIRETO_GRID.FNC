create or replace
function obter_material_direto_grid(	ds_material_direto_p		varchar2)
return varchar2 is

ds_material_direto_w		varchar2(255);			
			
begin

select	substr(ds_material_direto_p,1,255)
into	ds_material_direto_w
from	dual;

return	ds_material_direto_w;

end obter_material_direto_grid;
/