CREATE OR REPLACE
FUNCTION OBTER_NOME_EMPRESA(
					CD_EMPRESA_P number)
					RETURN VARCHAR2 IS

ds_retorno_w		Varchar2(80);

BEGIN
if	(cd_empresa_p	is not null) then
	begin
	select	nm_razao_social
	into	ds_retorno_w
	from	empresa
	where	cd_empresa	= cd_empresa_p;
	end;
end if;

RETURN ds_retorno_w;
END OBTER_NOME_EMPRESA;
/



