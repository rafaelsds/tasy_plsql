CREATE OR REPLACE FUNCTION FN_COUNT_DAYS_VENTILATED(PR_NR_ATENDIMENTO IN NUMBER) 
  RETURN NUMBER IS vr_days_ventilated NUMBER;
  
    CURSOR cr_patient IS
    SELECT amr.IE_RESPIRACAO
          ,amr.DT_MONITORIZACAO
      FROM atendimento_monit_resp amr
     WHERE amr.ie_situacao = 'A'
       AND amr.dt_inativacao IS NULL
       AND amr.nr_atendimento = PR_NR_ATENDIMENTO
  ORDER BY amr.DT_MONITORIZACAO DESC;
  
    vr_last_register VARCHAR(20) := NULL;
    vr_last_date DATE := NULL;

  BEGIN 

    vr_days_ventilated := 0;
    
    FOR cr IN cr_patient LOOP
      IF vr_last_register = cr.IE_RESPIRACAO OR (cr.IE_RESPIRACAO NOT IN ('ESPONT','VMNI') AND cr.IE_RESPIRACAO IS NOT NULL) THEN
        IF vr_last_date = cr.DT_MONITORIZACAO THEN
          GOTO end_loop;
        END IF;
        vr_days_ventilated := vr_days_ventilated + 1;
      ELSE
        EXIT;
      END IF;
      vr_last_date := cr.DT_MONITORIZACAO;
      vr_last_register := cr.IE_RESPIRACAO;
      <<end_loop>>
      NULL;
    END LOOP;

    RETURN(vr_days_ventilated);

  END FN_COUNT_DAYS_VENTILATED;
/