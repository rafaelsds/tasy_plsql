create or replace
function obter_se_contrato_do_cnpj(	nr_seq_contrato_p			number,	
				cd_cnpj_p			varchar2,
				ie_origem_p			varchar2)
 		    	return varchar2 is

/*
Origem
NF = Nota Fiscal
OC = Ordem Compra
IR = Inspe��o Recebimento
*/
			
cd_cgc_contratado_w		varchar2(14);
ie_cnpj_contrato_w			varchar2(1) := 'N';
qt_registro_w			number(10);

begin

select	cd_cgc_contratado
into	cd_cgc_contratado_w
from	contrato
where	nr_sequencia = nr_seq_contrato_p;

if	(cd_cgc_contratado_w is not null) and
	(cd_cgc_contratado_w = cd_cnpj_p) then
	ie_cnpj_contrato_w := 'S';
end if;

if	(ie_cnpj_contrato_w = 'N') then
		
	select	count(*)
	into	qt_registro_w
	from	contrato a,
		contrato_fornec_lib b
	where	a.nr_sequencia = b.nr_seq_contrato
	and	b.nr_seq_contrato = nr_seq_contrato_p	
	and	b.cd_cnpj = cd_cnpj_p
	and	((ie_origem_p = 'NF' and nvl(b.ie_nota_fiscal,'S') = 'S') or
		(ie_origem_p = 'OC' and nvl(b.ie_ordem_compra,'S') = 'S') or
		(ie_origem_p = 'IR' and nvl(b.ie_inspecao,'S') = 'S'));

	if	(qt_registro_w > 0) then
		ie_cnpj_contrato_w := 'S';	
	end if;
end if;


return	ie_cnpj_contrato_w;

end obter_se_contrato_do_cnpj;
/
