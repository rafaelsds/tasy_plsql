create or replace
function hmlb_verifica_doador_doacao 	(cd_pessoa_fisica_p	varchar2,
					nr_seq_doacao_p		varchar2)
 		    	return varchar2 is


ds_retorno_w		varchar(10);

begin
if	(cd_pessoa_fisica_p is not null) then
	
	if	(to_number(nr_seq_doacao_p) = 0) then
		select	max(nr_sequencia)
		into	ds_retorno_w
		from	san_doacao
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;	
	
	elsif	(to_number(nr_seq_doacao_p) <> 0) then
		select	decode(count(*), 0,'N','S')
		into	ds_retorno_w
		from	san_doacao
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	nr_sequencia		= nr_seq_doacao_p;
	
	end if;
end if;

return	ds_retorno_w;

end hmlb_verifica_doador_doacao;
/