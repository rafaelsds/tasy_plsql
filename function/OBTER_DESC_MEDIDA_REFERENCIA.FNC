create or replace
function Obter_desc_medida_referencia(nr_seq_med_laudo_p	Number,
									  ie_tipo_medida_p		Varchar2,
									  qt_idade_p			Number,
									  qt_peso_p				Number)
 		    	return Varchar2 is

ds_medida_referencia_w		Varchar2(80);
ds_medida_referencia_aux_w	Varchar2(255);

CURSOR C01 is
select	max(a.ds_medida_referencia)
into	ds_medida_referencia_w
from	medida_exame_refer a,
	medida_exame_laudo b
where	a.nr_seq_medida = b.nr_sequencia
and	b.nr_sequencia = nr_seq_med_laudo_p
and	a.ie_tipo_medida = ie_tipo_medida_p
and (((a.ie_tipo_medida = 'P') and
	  (qt_peso_p between a.QT_PESO_MINIMO and a.QT_PESO_MAXIMO)) or
	((a.ie_tipo_medida = 'I') and
	  (qt_idade_p between a.QT_IDADE_MINIMA and a.QT_IDADE_MAXIMA))) ;
			
begin

open C01;
loop
fetch C01 into	
	ds_medida_referencia_w;
exit when C01%notfound;
end loop;
close C01;


return	ds_medida_referencia_w;

end Obter_desc_medida_referencia;
/