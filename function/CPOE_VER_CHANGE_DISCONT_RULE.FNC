create or replace function cpoe_ver_change_discont_rule(
                cd_estabelecimento_p    cpoe_regra_exame_alteracao.cd_estabelecimento%type,
                nr_sequencia_p  	cpoe_procedimento.nr_sequencia%type,
                nm_usuario_p            varchar2
                ) return varchar2  is 

ie_permite_w                    cpoe_reg_exam_alter_restr.ie_permite%type;
ie_usuario_geral_w              cpoe_regra_exame_alteracao.ie_usuario_geral%type;
ie_usuario_privilegio_w         cpoe_regra_exame_alteracao.ie_usuario_privilegio%type;
ds_status_atend_w               varchar2(80);
ds_result_w                     varchar2(3);
ie_has_access_w                 varchar2(1);

-- The patient could have many status for one single item. 
-- because some items are prescipted in many doses (with schedule)
-- each dose, for instance, have their own status..
-- we consider here, any status that become to match any rule.
-- and we stay with the more restrictive rule. (the worst case scenario). MAX(ie_usuario_geral) e MAX(ie_usuario_privilegio)

cursor c001 IS
        select 
        cast(nvl(cpoe_reg_exam_alter_restr.ie_permite,'S') as char) as ie_permite,
        ( select  substr(max(obter_desc_expressao(CD_EXP_VALOR_DOMINIO)),1,80)
                        from  valor_dominio_v
                        where  cd_dominio  = 1030 
                        and  vl_dominio  = regra_exame_status.ie_status_regra ) as ds_status_atend ,
        COALESCE(cpoe_regra_exame_alteracao.ie_usuario_geral,'X') ie_usuario_geral,
        COALESCE(cpoe_regra_exame_alteracao.ie_usuario_privilegio,'X') ie_usuario_privilegio,
        codes.cod_allowed
        from cpoe_regra_exame_alteracao, regra_exame_status, cpoe_reg_exam_alter_restr, cpoe_tipo_pedido,
                (  select regexp_substr(obter_parametro_funcao_padrao(2314, 42, nm_usuario_p),'[^,]+', 1, level) as cod_allowed
                        from dual
                        connect by regexp_substr(obter_parametro_funcao_padrao(2314, 42, nm_usuario_p), '[^,]+', 1, level) is not null ) codes
        where cpoe_regra_exame_alteracao.nr_seq_status_regra = regra_exame_status.nr_sequencia
        and regra_exame_status.ie_situacao = 'A'
        and cpoe_regra_exame_alteracao.ie_situacao  = 'A'
        and cpoe_reg_exam_alter_restr.nr_seq_reg_exame_alter = cpoe_regra_exame_alteracao.nr_sequencia(+)
        and cpoe_regra_exame_alteracao.cd_estabelecimento = cd_estabelecimento_p
        and cpoe_regra_exame_alteracao.nr_seq_tipo_ordem  = codes.cod_allowed(+)
        and ( cpoe_reg_exam_alter_restr.nr_seq_tipo_atendimento  , regra_exame_status.ie_status_regra )  
        in ( 
                select 
                distinct
                atendimento.ie_tipo_atendimento as ie_tipo_atendimento,
                prescricao.ie_status_atend
                from   cpoe_procedimento procedimento_cpoe, prescr_procedimento prescricao, atendimento_paciente atendimento, proc_interno
                where  procedimento_cpoe.nr_sequencia = prescricao.nr_seq_proc_cpoe
                and procedimento_cpoe.nr_seq_proc_interno = prescricao.nr_seq_proc_interno
                and procedimento_cpoe.nr_atendimento = atendimento.nr_atendimento
                and atendimento.cd_pessoa_fisica = procedimento_cpoe.cd_pessoa_fisica
                and proc_interno.nr_sequencia = procedimento_cpoe.nr_seq_proc_interno
                and proc_interno.ie_situacao = 'A'  
                and prescricao.ie_suspenso = 'N'
                and procedimento_cpoe.nr_sequencia = nr_sequencia_p
        )
        and cpoe_regra_exame_alteracao.nr_seq_tipo_ordem = cpoe_tipo_pedido.nr_sequencia(+)
        and ( COALESCE(cpoe_tipo_pedido.nr_seq_cpoe_group,'') = 'P' OR  cpoe_tipo_pedido.nr_seq_cpoe_group is null )
        order by cast(nvl(cpoe_reg_exam_alter_restr.ie_permite,'S') as char) DESC , COALESCE(codes.cod_allowed,'0') DESC ;

begin


        FOR record_lines IN c001
        LOOP

                if record_lines.ie_permite = 'N' then
                        return 'B';
                elsif (record_lines.ie_permite = 'S' AND record_lines.ie_usuario_privilegio = 'N' AND  record_lines.ie_usuario_geral = 'N')
                then
                        return 'B';
                elsif record_lines.ie_permite = 'S' AND record_lines.ie_usuario_privilegio = 'S' AND record_lines.ie_usuario_geral = 'N' 
                then
                        if record_lines.cod_allowed <> '0'
                        then 
                                return 'J';
                        else
                                return 'B';
                        end if;
                elsif record_lines.ie_permite = 'S' AND record_lines.ie_usuario_privilegio = 'N' AND  record_lines.ie_usuario_geral = 'S' then
                        return 'J';
                elsif record_lines.ie_permite = 'S' AND record_lines.ie_usuario_privilegio = 'S' AND  record_lines.ie_usuario_geral = 'S' then
                        return 'J';
                elsif record_lines.ie_permite = 'S' AND record_lines.ie_usuario_privilegio = 'N' AND  record_lines.ie_usuario_geral = 'S' then
                        return 'J';
                else
                        return 'A';
                end if;
        END LOOP;

        -- IN CASE OF ZERO RECORDS
        return 'A';

end cpoe_ver_change_discont_rule;

/
