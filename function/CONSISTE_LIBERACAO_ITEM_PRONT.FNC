create or replace
function Consiste_Liberacao_Item_Pront( nr_seq_item_p	number) 
                                                 return varchar2 is

qt_reg_w			number(10);
cd_cgc_w			varchar2(14);
ie_apresentar_w     varchar2(1) := 'S';

begin

if	(  nr_seq_item_p = 1493) then

	cd_cgc_w	:= obter_cgc_estabelecimento(wheb_usuario_pck.get_cd_estabelecimento);
	
	select	count(*)
	into	qt_reg_w
	from	ITEM_PRONTUARIO_LIBERACAO
	where	nr_seq_item	= nr_seq_item_p
	and		CD_CGC_CLIENTE = cd_cgc_w
	and		sysdate between nvl(DT_INICIO_VIGENCIA,sysdate - 1)  and  nvl(DT_FIM_VIGENCIA,sysdate + 1);
	
	if	(qt_reg_w	= 0) then
	
		ie_apresentar_w := 'N';
		
	end if;
	
end if;

return	ie_apresentar_w;

end Consiste_Liberacao_Item_Pront;
/