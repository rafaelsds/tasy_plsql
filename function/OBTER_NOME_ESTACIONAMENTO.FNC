create or replace
function obter_nome_estacionamento (	nr_sequencia_p	number)
					return	varchar2 is

ds_retorno_w	varchar2(100);

begin

if	(nr_sequencia_p is not null) then
	select	ds_estacionamento
	into	ds_retorno_w
	from	estacionamento
	where	nr_sequencia = nr_sequencia_p;
end if;

return	ds_retorno_w;

end obter_nome_estacionamento;
/