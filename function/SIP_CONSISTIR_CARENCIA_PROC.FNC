create or replace
function sip_consistir_carencia_proc
			(	nr_seq_segurado_p		number,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				cd_grupo_p			number,
				cd_especialidade_p		number,
				cd_area_p			number,
				qt_carencia_segurado_p		number,
				qt_carencia_contrato_p		number,
				dt_inclusao_operadora_p		date,
				nr_seq_contrato_p		number,
				nr_seq_plano_p			number)
				return date is

ie_cpt_w			varchar2(255);
ie_liberado_w			varchar2(10);
ie_mes_posterior_w		varchar2(1);
cd_area_w			number(15);
cd_especialidade_w		number(15);
cd_grupo_w			number(15);
nr_seq_plano_w			number(10);
nr_seq_contrato_w		number(10);
nr_seq_carencia_w		number(10);
ie_origem_proced_w		number(10);
ie_cursor_w			number(10);
nr_seq_tipo_carencia_w		number(10);
qt_dias_w			number(5)	:= 0;
dt_inclusao_operadora_w		date;
dt_inicio_vigencia_w		date;
dt_exposto_w			date;

Cursor C01 is
	select	x.nr_sequencia,
		nvl(x.ie_mes_posterior,'N') ie_mes_anterior
	from	pls_carencia	x
	where	x.nr_seq_segurado	= nr_seq_segurado_p;

Cursor C03 is
	select	x.nr_sequencia,
		nvl(x.ie_mes_posterior,'N')
	from	pls_carencia	x
	where	x.nr_seq_contrato	= nr_seq_contrato_w;

Cursor C04 is
	select	x.nr_sequencia,
		nvl(x.ie_mes_posterior,'N') ie_mes_posterior
	from	pls_carencia	x
	where	nr_seq_plano	= nr_seq_plano_w;

Cursor C02 is
	select	/* +CHOOSE */
		c.qt_dias,
		c.dt_inicio_vigencia
	from	pls_carencia_proc a,
		pls_carencia c
	where	c.nr_sequencia		= nr_seq_carencia_w
	and	c.nr_seq_tipo_carencia	= a.nr_seq_tipo_carencia
	and	c.ie_cpt		= 'N'
	and	a.ie_liberado		= 'S'
	and	a.nr_seq_tipo_acomodacao is null
	and	a.ie_tipo_guia is null
	and	((a.cd_procedimento is null)		or (a.cd_procedimento	= cd_procedimento_p))
	and	((a.ie_origem_proced is null)		or (a.ie_origem_proced	= ie_origem_proced_p))
	and	((a.cd_grupo_proc is null)		or (a.cd_grupo_proc	= cd_grupo_p))
	and	((a.cd_especialidade is null)		or (a.cd_especialidade	= cd_especialidade_p))
	and	((a.cd_area_procedimento is null)	or (a.cd_area_procedimento	= cd_area_p))
	order by a.cd_area_procedimento,
		a.cd_especialidade,
		a.cd_grupo_proc,
		a.cd_procedimento;

TYPE 		fetch_array1 IS TABLE OF C01%ROWTYPE;
s_array1	fetch_array1;
i1		Integer := 1;
type Vetor1 is table of fetch_array1 index by binary_integer;
Vetor_C01_w			Vetor1;

TYPE 		fetch_array4 IS TABLE OF C04%ROWTYPE;
s_array4 	fetch_array4;
i4		Integer := 1;
type Vetor4 is table of fetch_array4 index by binary_integer;
Vetor_C04_w			Vetor4;

TYPE 		fetch_array2 IS TABLE OF C02%ROWTYPE;
s_array2 	fetch_array2;
i2		Integer := 1;
type Vetor2 is table of fetch_array2 index by binary_integer;
Vetor_C02_w			Vetor2;

begin
dt_inclusao_operadora_w	:= dt_inclusao_operadora_p;
nr_seq_contrato_w	:= nr_seq_contrato_p;
nr_seq_plano_w		:= nr_seq_plano_p;

if	(qt_carencia_segurado_p	= 0) and
	(qt_carencia_contrato_p	= 0) then
	ie_cursor_w	:= 4;
elsif	(qt_carencia_segurado_p	= 0) then
	ie_cursor_w	:= 3;
else
	ie_cursor_w	:= 1;
end if;

if	(ie_cursor_w = 3) then
	open C03;
	loop
	fetch C03 into
		nr_seq_carencia_w,
		ie_mes_posterior_w;
	exit when C03%notfound;
		begin
		if	(ie_mes_posterior_w	= 'S') then
			dt_inclusao_operadora_w	:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_inclusao_operadora_w,'month',0),1,0);
		end if;
		
		open C02;
		loop
		FETCH C02 BULK COLLECT INTO s_array2 LIMIT 1000;
			Vetor_C02_w(i2) := s_array2;
			i2 := i2 + 1;
		EXIT WHEN C02%NOTFOUND;
		END LOOP;
		CLOSE C02;

		for j in 1..Vetor_C02_w.COUNT loop
			s_array2 := Vetor_C02_w(j);
			for k in 1..s_array2.COUNT loop
				begin
				qt_dias_w		:= s_array2(k).qt_dias;
				dt_inicio_vigencia_w	:= s_array2(k).dt_inicio_vigencia;
				end;
			end loop;
		end loop;

		dt_exposto_w	:= (nvl(dt_inicio_vigencia_w,dt_inclusao_operadora_w) + qt_dias_w);
		end;
	end loop;
	close C03;
elsif	(ie_cursor_w	= 4) then
	open C04;
	loop
	FETCH C04 BULK COLLECT INTO s_array4 LIMIT 1000;
		Vetor_C04_w(i4) := s_array4;
		i4 := i4 + 1;
	EXIT WHEN C04%NOTFOUND;
	END LOOP;
	CLOSE C04;

	for i in 1..Vetor_C04_w.COUNT loop
		s_array4 := Vetor_C04_w(i);
		for z in 1..s_array4.COUNT loop
			begin
			nr_seq_carencia_w		:= s_array4(z).nr_sequencia;
			ie_mes_posterior_w		:= s_array4(z).ie_mes_posterior;

			if	(ie_mes_posterior_w = 'S') then
				dt_inclusao_operadora_w	:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_inclusao_operadora_w,'month',0),1,0);
			end if;

			open C02;
			loop
			FETCH C02 BULK COLLECT INTO s_array2 LIMIT 1000;
				Vetor_C02_w(i2) := s_array2;
				i2 := i2 + 1;
			EXIT WHEN C02%NOTFOUND;
			END LOOP;
			CLOSE C02;

			for j in 1..Vetor_C02_w.COUNT loop
				s_array2 := Vetor_C02_w(j);
				for k in 1..s_array2.COUNT loop
					begin
					qt_dias_w		:= s_array2(k).qt_dias;
					dt_inicio_vigencia_w	:= s_array2(k).dt_inicio_vigencia;
					end;
				end loop;
			end loop;
			
			dt_exposto_w	:= (nvl(dt_inicio_vigencia_w,dt_inclusao_operadora_w) + qt_dias_w);
			end;
		end loop;
	end loop;
else
	open C01;
	loop
	FETCH C01 BULK COLLECT INTO s_array1 LIMIT 1000;
		Vetor_C01_w(i1) := s_array1;
		i1 := i1 + 1;
	EXIT WHEN C01%NOTFOUND;
	END LOOP;
	CLOSE C01;
	
	for i in 1..Vetor_C01_w.COUNT loop
		s_array1 := Vetor_C01_w(i);
		for z in 1..s_array1.COUNT loop
			begin
			nr_seq_carencia_w	:= s_array1(z).nr_sequencia;
			ie_mes_posterior_w	:= s_array1(z).ie_mes_anterior;
			
			if	(ie_mes_posterior_w	= 'S') then
				dt_inclusao_operadora_w	:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_inclusao_operadora_w,'month',0),1,0);
			end if;

			open C02;
			loop
			FETCH C02 BULK COLLECT INTO s_array2 LIMIT 1000;
				Vetor_C02_w(i2) := s_array2;
				i2 := i2 + 1;
			EXIT WHEN C02%NOTFOUND;
			END LOOP;
			CLOSE C02;

			for j in 1..Vetor_C02_w.COUNT loop
				s_array2 := Vetor_C02_w(j);
				for k in 1..s_array2.COUNT loop
					begin
					qt_dias_w		:= s_array2(k).qt_dias;
					dt_inicio_vigencia_w	:= s_array2(k).dt_inicio_vigencia;
					end;
				end loop;
			end loop;

			dt_exposto_w	:= (nvl(dt_inicio_vigencia_w,dt_inclusao_operadora_w) + qt_dias_w);
			end;
		end loop;
	end loop;
end if;

return	dt_exposto_w;

end sip_consistir_carencia_proc;
/