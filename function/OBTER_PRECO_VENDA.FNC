create or replace
function obter_preco_venda (	cd_material_p	number,
			ie_opcao_p	varchar2)
			return	number is

vl_resultado_w	number(15,4) := 0;

begin

select	obter_valor_brasindice(cd_material_p, ie_opcao_p)
into	vl_resultado_w
from	dual;

if	(vl_resultado_w is null or vl_resultado_w = 0) then
	begin
	select	max(nvl(a.vl_preco_venda,0))
	into	vl_resultado_w
	from	preco_material a
	where	cd_material = cd_material_p
	and	a.dt_inicio_vigencia		=	(select	max(x.dt_inicio_vigencia)
						from 	preco_material x
						where	x.cd_material	=	a.cd_material
						and	x.vl_preco_venda	> 0);
	end;
end if;

/*
if	(vl_resultado_w is null or vl_resultado_w = 0) then
	begin
	select	nvl(obter_preco_simpro(b.cd_simpro),0)
	into	vl_resultado_w
	from     	material a,
	         	simpro_cadastro c,
	         	material_simpro b
	where    	a.cd_material	= b.cd_material
	and	b.cd_simpro	= c.cd_simpro
	and      	a.cd_material	= cd_material_p;
	end;
end if;
*/

return	vl_resultado_w;

end obter_preco_venda;
/