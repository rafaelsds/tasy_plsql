create or replace
function consistir_se_agrup_quimio(	nr_seq_paciente_p	number,
					nr_seq_atendimento_p	number,
					cd_material_p		number,
					cd_intervalo_p		varchar2)
					return varchar2 is

cd_material_w		number(6,0);
ie_via_aplicacao_w	varchar2(5);
nr_seq_paciente_w	number(10,0);
cd_intervalo_w		varchar2(7);
ie_bomba_infusao_w	varchar2(1);
ds_dias_aplicacao_w	varchar2(4000);
ds_dia_agrupar_w	varchar2(2);
ds_dia_ciclo_w		varchar2(5);
ie_protocolo_livre_w	varchar2(1);
cd_intervalo_ww		varchar2(7);

Cursor C01 is
	select	a.cd_material,
		a.cd_intervalo
	from	paciente_atend_medic a,
		paciente_atendimento b
	where	a.nr_seq_atendimento 	= b.nr_seq_atendimento
	and	b.nr_seq_atendimento 	= nr_seq_atendimento_p
	and	((a.cd_material		= cd_material_p) or (cd_material_p is null))
	and	((a.cd_intervalo	= cd_intervalo_p) or (cd_intervalo_p is null));
	
Cursor C02 is
	select	b.ds_dia_ciclo,
		a.cd_intervalo,
		a.ie_bomba_infusao
	from	paciente_atend_medic a,
		paciente_atendimento b
	where	a.nr_seq_atendimento 	= b.nr_seq_atendimento
	and	b.nr_seq_paciente 	= nr_seq_paciente_p
	and	a.cd_material		= cd_material_w;
	
begin

select	nvl(max(ie_protocolo_livre),'N')
into	ie_protocolo_livre_w
from	paciente_setor
where	nr_seq_paciente	=	nr_seq_paciente_p;


if	(ie_protocolo_livre_w = 'N') and (nr_seq_paciente_p > 0) and (nr_seq_atendimento_p > 0) then

	open C01;
	loop
	fetch C01 into	
		cd_material_w,
		cd_intervalo_ww;
	exit when C01%notfound;
		begin
		
		select	max(cd_intervalo),
			max(ie_bomba_infusao),
			max(ds_dias_aplicacao)
		into	cd_intervalo_w,
			ie_bomba_infusao_w,
			ds_dias_aplicacao_w
		from	paciente_protocolo_medic
		where	nr_seq_paciente		= nr_seq_paciente_p
		and	cd_material		= cd_material_w
		and	((cd_intervalo		= cd_intervalo_p) or (cd_intervalo_p is null));
		
		select	max(ds_dia_agrupar)
		into	ds_dia_agrupar_w
		from	regra_agrupamento_prep
		where	nvl(cd_intervalo,nvl(cd_intervalo_ww,'X'))		= nvl(cd_intervalo_ww,'X')
		and	nvl(ie_bomba_infusao,nvl(ie_bomba_infusao_w,'X'))	= nvl(ie_bomba_infusao_w,'X')
		and	nvl(ds_dias_aplic,nvl(ds_dias_aplicacao_w,'X'))		= nvl(ds_dias_aplicacao_w,'X')
		and	nvl(cd_material,nvl(cd_material_w,0))			= nvl(cd_material_w,0);	
		
		if	(ds_dia_agrupar_w is not null) then
			exit;
		end if;
		
		end;
	end loop;
	close C01;

elsif	(ie_protocolo_livre_w = 'S') and (nr_seq_paciente_p > 0) and (nr_seq_atendimento_p > 0) then

	open C01;
	loop
	fetch C01 into	
		cd_material_w,
		cd_intervalo_ww;
	exit when C01%notfound;
		begin
		ds_dias_aplicacao_w := null;
		
		open C02;
		loop
		fetch C02 into	
			ds_dia_ciclo_w,
			cd_intervalo_w,
			ie_bomba_infusao_w;
		exit when C02%notfound;
			begin
			if	(ds_dias_aplicacao_w is null) then
				ds_dias_aplicacao_w := ds_dia_ciclo_w;
			else			
				ds_dias_aplicacao_w := ds_dias_aplicacao_w || ',' || ds_dia_ciclo_w;
			end if;
			end;
		end loop;
		close C02;
	
		select	max(ds_dia_agrupar)
		into	ds_dia_agrupar_w
		from	regra_agrupamento_prep
		where	nvl(cd_intervalo,nvl(cd_intervalo_w,'X'))		= nvl(cd_intervalo_w,'X')
		and	nvl(ie_bomba_infusao,nvl(ie_bomba_infusao_w,'X'))	= nvl(ie_bomba_infusao_w,'X')
		and	nvl(ds_dias_aplic,nvl(ds_dias_aplicacao_w,'X'))		= nvl(ds_dias_aplicacao_w,'X')
		and	nvl(cd_material,nvl(cd_material_w,0))			= nvl(cd_material_w,0);	
		
		if	(ds_dia_agrupar_w is not null) then
			exit;
		end if;
		
		end;
	end loop;
	close C01;

end if;

return ds_dia_agrupar_w;

end consistir_se_agrup_quimio;
/
