create or replace 
function Obter_Sexo_Prescricao(	nr_prescricao_p		number) 
				return char is

cd_recem_nato_w		pessoa_fisica.cd_pessoa_fisica%type;
ie_recem_nato_w		char(1);
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
ie_retorno_w		char(1);

begin

if	(nr_prescricao_p is not null) then
	select	max(cd_recem_nato),
			nvl(max(ie_recem_nato),'N'),
			max(cd_pessoa_fisica)
	into	cd_recem_nato_w,
			ie_recem_nato_w,
			cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;

	ie_retorno_w := '';

	if	(cd_recem_nato_w is not null) then
		select	max(ie_sexo)
		into	ie_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_recem_nato_w;
	elsif (ie_recem_nato_w = 'N') then
		select	max(ie_sexo)
		into	ie_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	end if;
end if;

return ie_retorno_w;

end Obter_Sexo_Prescricao;
/