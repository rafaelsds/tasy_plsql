create or replace	
function tx_obter_dados_intercorrencia(nr_seq_intercorrencia_p	number,
				ie_opcao_p			varchar2)
				return varchar2 is 

ds_intercorrencia_w		varchar2(90);
dt_tipo_intercorrencia_w	varchar2(90);
ds_retorno_w			varchar2(90);

begin

select	ds_intercorrencia,
	substr(obter_valor_dominio(2374,IE_TIPO_INTERCORRENCIA),1,90)
into	ds_intercorrencia_w,
	dt_tipo_intercorrencia_w
from	tx_intercorrencia
where	nr_sequencia = nr_seq_intercorrencia_p;

if	(ie_opcao_p = 'DI') then
	ds_retorno_w	:= ds_intercorrencia_w;
elsif	(ie_opcao_p = 'DT') then
	ds_retorno_w	:= dt_tipo_intercorrencia_w;
end if;

return ds_retorno_w;
end tx_obter_dados_intercorrencia;
/