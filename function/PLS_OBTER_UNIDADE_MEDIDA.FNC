CREATE OR REPLACE
FUNCTION pls_obter_unidade_medida	(cd_unidade_medida_p	varchar2)
					RETURN VARCHAR2 IS

ds_unidade_medida_w	varchar2(40);

BEGIN
if	(cd_unidade_medida_p is not null) then
	select	max(ds_unidade_medida)
	into	ds_unidade_medida_w
	from	unidade_medida
	where	cd_unidade_medida = cd_unidade_medida_p;
end if;

RETURN	ds_unidade_medida_w;

END	pls_obter_unidade_medida;
/

