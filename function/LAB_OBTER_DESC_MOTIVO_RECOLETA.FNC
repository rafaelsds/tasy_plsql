create or replace
function lab_obter_desc_motivo_recoleta(nr_seq_recoleta_p	number)
 		    	return varchar2 is
			
ds_retorno_w	varchar2(255);

begin


select max(ds_motivo_recoleta)
into	ds_retorno_w
from	lab_motivo_recoleta
where 	nr_sequencia = nr_seq_recoleta_p;


return	ds_retorno_w;

end lab_obter_desc_motivo_recoleta;
/