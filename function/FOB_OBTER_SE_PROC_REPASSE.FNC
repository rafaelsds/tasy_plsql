create or replace
function FOB_OBTER_SE_PROC_REPASSE
		(nr_seq_proc_p		number,
		qt_proc_inicial_p		number,
		qt_proc_final_p		number ) return number is

/*

Function criada para a OS 415300

Esta function retorna se deve gerar repasse
O repasse s� ser� gerado caso a quantidade de procedimentos executados no m�s fique entre a quantidade inicial e final

retorno:
	0 - Gera repasse
	1 - N�o gera

*/

ie_retorno_w		number(15,0);
nr_seq_proc_interno_w	number(15,0);
dt_procedimento_w	date;
cont_w			number(10,0);

begin

select	a.nr_seq_proc_interno,
	a.dt_procedimento
into	nr_seq_proc_interno_w,
	dt_procedimento_w
from	procedimento_paciente a
where	a.nr_sequencia		= nr_seq_proc_p;

select	count(*)
into	cont_w
from	procedimento_paciente a
where	a.nr_seq_proc_interno		= nr_seq_proc_interno_w
and	a.cd_motivo_exc_conta 		is null
and	trunc(a.dt_procedimento, 'month') 	= trunc(dt_procedimento_w, 'month');

ie_retorno_w			:= 0;
if	(cont_w between qt_proc_inicial_p and qt_proc_final_p) then
	ie_retorno_w		:= 1;
end if;

return	ie_retorno_w;

end FOB_OBTER_SE_PROC_REPASSE;
/

