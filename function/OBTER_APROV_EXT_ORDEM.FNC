Create or Replace
Function Obter_Aprov_Ext_Ordem(
			nr_ordem_compra_p	Number)
			Return Varchar2 is
/*
Esta fun��o foi criada para atender uma necessidade da Unimed Joinville
Para utiliza-la � necess�rio incluir a codifica��o necess�ria dentro da mesma
e retornar o erro no campo ie_aprova��o_w. Se estiver correta retornar S
� importante que a cada atualiza��o esta function seja atualizada com a vers�o 
do hospital, pois a vers�o Wheb ser� esta que sempre aprova a ordem do ponto de 
vista externo ao sistema.
*/
ie_aprovacao_w	Varchar2(255);
BEGIN

ie_aprovacao_w	:= 'S';
Return ie_aprovacao_w;

END;
/