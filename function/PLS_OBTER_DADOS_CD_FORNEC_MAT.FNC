create or replace
function pls_obter_dados_cd_fornec_mat(	cd_fornecedor_p		varchar2,
					ie_opcao_p		varchar2)
					return varchar2 is

ds_retorno_w		varchar2(4000);

/* 
ie_opcao_p
'S' -> Situa��o fornecedor
*/

begin
if	(cd_fornecedor_p is not null) and
	(ie_opcao_p is not null) then
	if 	(ie_opcao_p = 'S') then
		select	max(ie_situacao)
		into	ds_retorno_w
		from	pls_fornec_mat_fed_sc
		where	cd_fornecedor = cd_fornecedor_p;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_dados_cd_fornec_mat;
/