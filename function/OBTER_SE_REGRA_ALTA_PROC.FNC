create or replace
function Obter_Se_Regra_Alta_Proc(	
			nr_atendimento_p		Number,
			nr_sequencia_p			number)
 		    	return varchar2 is

			
qt_regra_w		number(10) 	:= 0;
ie_retorno_w		varchar2(1)	:= 'S';
ie_regra_disp_w		varchar2(1);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
ie_forma_regra_proced_w	varchar2(1);
qt_proc_w		number(10,0);
qt_mat_w		number(10,0);
			
cursor c01 is
	select	cd_procedimento,
		ie_origem_proced
	from	regra_fecha_alta_proc
	where	nr_seq_regra = nr_sequencia_p
	order by cd_procedimento;
			
begin

select	nvl(max(ie_forma_regra_proced),'T')
into	ie_forma_regra_proced_w
from	regra_fechar_conta_alta
where	nr_sequencia = nr_sequencia_p;

select	count(*)
into	qt_regra_w
from	regra_fecha_alta_proc
where	nr_seq_regra = nr_sequencia_p;

if	(qt_regra_w = 0) then
	ie_retorno_w	:= 'S';
	
else


	if (nvl(ie_forma_regra_proced_w,'T') = 'T') then

		open C01;
		loop
		fetch C01 into	
			cd_procedimento_w,
			ie_origem_proced_w;
		exit when C01%notfound;
			begin		
			
			select	count(*)
			into	qt_regra_w
			from	procedimento_paciente
			where	nr_atendimento 	= nr_atendimento_p
			and	cd_procedimento	= cd_procedimento_w	
			and 	ie_origem_proced = ie_origem_proced_w;
				
			if	(qt_regra_w = 0) then
				ie_retorno_w	:= 'N';
			end if;		
					
			end;
		end loop;
		close C01;
		
	elsif	(nvl(ie_forma_regra_proced_w,'T') = 'S') then
		
		ie_retorno_w	:= 'N';
		
		select	count(*)
		into	qt_mat_w
		from	material_atend_paciente
		where	nr_atendimento = nr_atendimento_p
		and 	nr_interno_conta is not null
		and	cd_motivo_exc_conta is null;
		
		if	(qt_mat_w = 0) then
		
			select	count(*) 
			into 	qt_proc_w
			from	procedimento_paciente a
			where	a.nr_atendimento = nr_atendimento_p
			and 	a.nr_interno_conta is not null
			and	a.cd_motivo_exc_conta is null
			and	not exists	(select	1	
						from	regra_fecha_alta_proc x
						where	x.cd_procedimento	=	a.cd_procedimento
						and	x.ie_origem_proced	=	a.ie_origem_proced
						and	x.nr_seq_regra	= nr_sequencia_p);

			if 	(qt_proc_w = 0) then
				ie_retorno_w := 'S';
			end if;
		end if;
		
	else
	
		ie_retorno_w	:= 'N';
		
		select	count(*) 
		into 	qt_proc_w
		from	procedimento_paciente a
		where	a.nr_atendimento = nr_atendimento_p
		and 	a.nr_interno_conta is not null
		and	a.cd_motivo_exc_conta is null
		and	exists	(select	1	
				from	regra_fecha_alta_proc x
				where	x.cd_procedimento	=	a.cd_procedimento
				and	x.ie_origem_proced	=	a.ie_origem_proced
				and	x.nr_seq_regra	= nr_sequencia_p);

		if 	(qt_proc_w = 0) then
			ie_retorno_w := 'S';
		end if;
		
	end if;

end if;

return	ie_retorno_w;

end Obter_Se_Regra_Alta_Proc;
/
