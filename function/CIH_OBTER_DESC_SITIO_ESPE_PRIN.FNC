create or replace
function cih_obter_desc_sitio_espe_prin	(nr_seq_sitio_p		number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is

nr_seq_sitio_princ_w	number(10);
ds_retorno_w		varchar2(4000);
ds_sitio_principal_w	varchar2(255);
ds_sitio_especifico_w	varchar2(255);

Cursor C01 is
	select	b.ds_sitio_especifico
	from 	niss_sitio_princ_esp a,
		niss_sitio_especifico b
	where 	a.nr_seq_espec 		= b.nr_sequencia
	and 	nvl(ie_situacao,'A') 	= 'A'
	and	a.nr_seq_princ		= nr_seq_sitio_princ_w
	order by 1;


begin
if	(nr_seq_sitio_p is not null) then

	select	x.nr_sequencia,
		x.ds_sitio_principal
	into	nr_seq_sitio_princ_w,
		ds_sitio_principal_w
	from	niss_sitio_principal x
	where	nvl(x.ie_situacao,'A') 	= 'A'
	and	x.nr_sequencia 		= nr_seq_sitio_p;
	
	
	if	(ie_opcao_p = 'P') then
		ds_retorno_w	:= ds_sitio_principal_w;
	elsif	(ie_opcao_p = 'E') then
		open c01;
		loop
		fetch c01 into
			ds_sitio_especifico_w;
		exit when c01%notfound;	
		begin
		ds_retorno_w	:= ds_retorno_w||'   -   '||ds_sitio_especifico_w;	
		end;
	end loop;
	close c01;
	
	if (length(ds_retorno_w) > 0) then
		ds_Retorno_w	:= substr(ds_retorno_w,7,length(ds_retorno_w));
	end if;
	
	end if;

end if;

return	ds_retorno_w;

end cih_obter_desc_sitio_espe_prin;
/


