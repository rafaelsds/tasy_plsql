create or replace 
function verificar_se_horario_comercial
			(ie_opcao_p		varchar2)
				return varchar2 as

qt_w			number(10);
ds_mensagem_w		varchar2(512);
ds_dia_semana_w		varchar2(60);
nr_dia_w		number(10);
dt_prox_dia_util_w	date;

ie_aviso_w		boolean	:= false;



/*
1, 'Domingo',
2, 'Segunda-Feira',
3, 'Ter�a-Feira',
4, 'Quarta-Feira',
5, 'Quinta-Feira',
6, 'Sexta-Feira',
*/

begin
dt_prox_dia_util_w := sysdate;

if	( dt_prox_dia_util_w > to_date(to_char(dt_prox_dia_util_w,'dd/mm/yyyy') || ' 18:00:00','dd/mm/yyyy hh24:mi:ss')) then
	dt_prox_dia_util_w := dt_prox_dia_util_w + 1;
	ie_aviso_w := true;
end if;
	
select	pkg_date_utils.get_WeekDay(dt_prox_dia_util_w)
into	nr_dia_w
from 	dual;

if	(nr_dia_w = 7) then
	ie_aviso_w := true;
	dt_prox_dia_util_w := dt_prox_dia_util_w + 2;
elsif	(nr_dia_w = 1) then
	ie_aviso_w := true;
	dt_prox_dia_util_w := dt_prox_dia_util_w + 1;
end if;


while Obter_Se_Feriado (1,dt_prox_dia_util_w) > 0 loop 
	begin
	ie_aviso_w := true;
	dt_prox_dia_util_w := dt_prox_dia_util_w + 1;
	end;
end loop;
	
if	(ie_aviso_w) then
	if	(ie_opcao_p in ('T', 'M')) then
		ds_dia_semana_w	:= substr(Obter_Dia_Semana(dt_prox_dia_util_w),1,60);
		
		if	(ie_opcao_p = 'T') then
			/*'Prezado cliente, o atendimento de suporte da Philips Clinical Informatics � feito em dias �teis e durante o hor�rio comercial.*/
			ds_mensagem_w	:= substr(wheb_mensagem_pck.get_texto(1040351),1,512);
		else
			ds_mensagem_w	:= substr(ds_dia_semana_w,1,512);
		end if;
	elsif	(ie_opcao_p = 'C') then
		ds_mensagem_w	:= '1040351';
	end if;
end if;

return ds_mensagem_w;

end;
/