create or replace
function w_ctb_obter_contrapartida_m(	nr_seq_movimento_p	number,
					nr_lote_contabil_p		number,
					ie_contrapartida_p		varchar2,
					ie_debito_credito_p		varchar2)
					return varchar2 is

cd_classif_contrapartida_w			varchar2(40);
cd_conta_credito_w			varchar2(20);
cd_conta_debito_w				varchar2(20);
cd_contrapartida_w				varchar2(20);
ds_contrapartida_w				varchar2(4000);
ie_apres_contrapartida_w			varchar2(15)	:= 'CL';
nr_seq_agrupamento_w			number(10);
nr_seq_movimento_w			number(10);
ie_existe_w				number(10);

cursor c01 is
select	a.nr_sequencia,
	a.cd_conta_credito,
	nvl(a.cd_classif_credito, b.cd_classificacao)
from	conta_contabil b,
	ctb_movimento a
where	a.cd_conta_credito		= b.cd_conta_contabil
and	a.nr_lote_contabil		= nr_lote_contabil_p
and	nvl(a.nr_seq_agrupamento,0)	= nr_seq_agrupamento_w
and	a.nr_sequencia		<> nr_seq_movimento_p
and	cd_conta_debito_w		<> '0'
and	cd_conta_credito_w	= '0'
union all
select	a.nr_sequencia,
	a.cd_conta_debito,
	nvl(a.cd_classif_debito, b.cd_classificacao)
from	conta_contabil b,
	ctb_movimento a
where	a.cd_conta_debito		= b.cd_conta_contabil
and	a.nr_lote_contabil		= nr_lote_contabil_p
and	nvl(a.nr_seq_agrupamento,0)	= nr_seq_agrupamento_w
and	a.nr_sequencia		<> nr_seq_movimento_p
and	cd_conta_credito_w	<> '0'
and	cd_conta_debito_w		= '0'
order by 1;

-- Cursor feito para aqueles lotes de digita��o cujos movimentos possuam cr�dito e d�bito.
--Situa��o identificada na OS 584762.
cursor c02 is
select	a.nr_sequencia,
	decode(ie_debito_credito_p,'C',a.cd_conta_debito,a.cd_conta_credito),
	nvl(decode(ie_debito_credito_p,'C',a.cd_classif_debito,a.cd_classif_credito),b.cd_classificacao)
from	conta_contabil b,
	ctb_movimento a
where	decode(ie_debito_credito_p,'C',a.cd_conta_debito,a.cd_conta_credito)		= b.cd_conta_contabil
and	a.nr_lote_contabil		= nr_lote_contabil_p
and	nvl(a.nr_seq_agrupamento,0)	= nr_seq_agrupamento_w
and	a.nr_sequencia		= nr_seq_movimento_p
and	cd_conta_credito_w	<> '0'
and	cd_conta_debito_w	<> '0'
order by 1;

begin

if	(ie_contrapartida_p = 'TCA') then
	ie_apres_contrapartida_w	:= 'CL';
elsif	(ie_contrapartida_p = 'TCR') then
	ie_apres_contrapartida_w	:= 'CT';
end if;

select	nvl(max(nr_seq_agrupamento),0),
	nvl(max(cd_conta_credito),'0'),
	nvl(max(cd_conta_debito),'0')
into	nr_seq_agrupamento_w,
	cd_conta_credito_w,
	cd_conta_debito_w
from	ctb_movimento
where	nr_sequencia		= nr_seq_movimento_p;

open C01;
loop
fetch C01 into
	nr_seq_movimento_w,
	cd_contrapartida_w,
	cd_classif_contrapartida_w;
exit when C01%notfound;
	begin

	if	(ie_apres_contrapartida_w = 'CL') then
		ie_existe_w := instr(ds_contrapartida_w,cd_classif_contrapartida_w);
		if	(nvl(ds_contrapartida_w,'X') = 'X') then
			ds_contrapartida_w	:= cd_classif_contrapartida_w;
		else
			if	(ie_existe_w = 0) then
					ds_contrapartida_w	:= substr(ds_contrapartida_w || chr(13) || chr(10) || cd_classif_contrapartida_w,1,4000);
			end if;
		end if;

	elsif	(ie_apres_contrapartida_w = 'CT') then
		ie_existe_w := instr(ds_contrapartida_w,cd_contrapartida_w);
		if	(nvl(ds_contrapartida_w,'X') = 'X') then
			ds_contrapartida_w	:= cd_contrapartida_w;
		else
			if 	(ie_existe_w = 0) then
					ds_contrapartida_w	:= substr(ds_contrapartida_w || chr(13) || chr(10) || cd_contrapartida_w,1,4000);
			end if;
		end if;
	end if;
	end;
end loop;
close C01;

if (nvl(ds_contrapartida_w,'X') = 'X') then
	open c02;
	loop
	fetch c02 into
		nr_seq_movimento_w,
		cd_contrapartida_w,
		cd_classif_contrapartida_w;
	exit when C02%notfound;
		begin

		if	(ie_apres_contrapartida_w = 'CL') then
			ie_existe_w := instr(ds_contrapartida_w,cd_classif_contrapartida_w);
			if	(nvl(ds_contrapartida_w,'X') = 'X') then
				ds_contrapartida_w	:= cd_classif_contrapartida_w;
			else
				if	(ie_existe_w = 0) then
						ds_contrapartida_w	:= substr(ds_contrapartida_w || chr(13) || chr(10) || cd_classif_contrapartida_w,1,4000);
				end if;
			end if;

		elsif	(ie_apres_contrapartida_w = 'CT') then
			ie_existe_w := instr(ds_contrapartida_w,cd_contrapartida_w);
			if	(nvl(ds_contrapartida_w,'X') = 'X') then
				ds_contrapartida_w	:= cd_contrapartida_w;
			else
				if 	(ie_existe_w = 0) then
						ds_contrapartida_w	:= substr(ds_contrapartida_w || chr(13) || chr(10) || cd_contrapartida_w,1,4000);
				end if;
			end if;
		end if;
		end;
	end loop;
	close C02;
end if;

return	ds_contrapartida_w;

end w_ctb_obter_contrapartida_m;
/
