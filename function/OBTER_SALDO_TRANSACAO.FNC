create or replace
function OBTER_SALDO_TRANSACAO	(nr_seq_movto_p	number,
				 ie_caixa_banco_p varchar2) return number is

nr_seq_saldo_banco_w	number(10)   := null;
nr_seq_saldo_caixa_w	number(10)   := null;
vl_saldo_w		number(15,2) := 0;
dt_transacao_w		date;
vl_transacao_w		number(15,2) := 0;
nr_seq_movto_w		number(10)   := 0;
ie_banco_w		varchar2(1)  := '';
ie_saldo_caixa_w	varchar2(1)  := '';
dt_trans_movto_W	DATE;

cursor	C01 is
select	 trunc(a.dt_transacao,'dd'),
	 a.vl_transacao,
	 a.nr_sequencia,
	 b.ie_banco
from	 transacao_financeira b,
	 movto_trans_financ a
where	 a.nr_seq_trans_financ  = b.nr_sequencia
and	 a.nr_seq_saldo_banco 	= nr_seq_saldo_banco_w
and	 b.ie_banco <> 'N'
order by trunc(a.dt_transacao,'dd'),
	 a.nr_sequencia;

cursor	C02 is
select	 trunc(a.dt_transacao,'dd'),
	 a.vl_transacao,
	 a.nr_sequencia,
	 b.ie_saldo_caixa
from	 transacao_financeira b,
	 movto_trans_financ a
where	 a.nr_seq_trans_financ  = b.nr_sequencia
and	 a.nr_seq_saldo_caixa 	= nr_seq_saldo_caixa_w
and	 b.ie_caixa in ('D','A','T')
order by trunc(a.dt_transacao,'dd'),
	 a.nr_sequencia;

begin

select	nr_seq_saldo_banco,
	nr_seq_saldo_caixa,
	trunc(dt_transacao,'dd')
into	nr_seq_saldo_banco_w,
	nr_seq_saldo_caixa_w,
	dt_trans_movto_w
from	movto_trans_financ
where	nr_sequencia	= nr_seq_movto_p;

if	(ie_caixa_banco_p = 'B') then
	select 	obter_saldo_banco_anterior(b.nr_sequencia)
	into	vl_saldo_w
	from 	banco_saldo b
	where 	nr_sequencia	= nr_seq_saldo_banco_w;

	open C01;
	loop
	fetch C01 into
		dt_transacao_w,
		vl_transacao_w,
		nr_seq_movto_w,
		ie_banco_w;
	exit	when c01%notfound;

		if 	(ie_banco_w = 'C') then
			vl_saldo_w 	:= vl_saldo_w - vl_transacao_w;
		elsif 	(ie_banco_w = 'D') then
			vl_saldo_w 	:= vl_saldo_w + vl_transacao_w;
		elsif 	(ie_banco_w = 'T') then
			vl_saldo_w 	:= vl_saldo_w;
		end if;
	
		if	(nr_seq_movto_w = nr_seq_movto_p) then
			exit;
		end if;
	end loop;
	close C01;
elsif	(ie_caixa_banco_p = 'C') then
	select	vl_saldo_inicial
	into	vl_saldo_w
	from	caixa_saldo_diario
	where	nr_sequencia 	= nr_seq_saldo_caixa_w;

	open C02;
	loop
	fetch C02 into
		dt_transacao_w,
		vl_transacao_w,
		nr_seq_movto_w,
		ie_saldo_caixa_w;
	exit	when c02%notfound;

		if 	(ie_saldo_caixa_w = 'S') then
			vl_saldo_w 	:= vl_saldo_w - vl_transacao_w;
		elsif 	(ie_saldo_caixa_w = 'E') then
			vl_saldo_w 	:= vl_saldo_w + vl_transacao_w;
		end if;
	
		if	(nr_seq_movto_w = nr_seq_movto_p) then
			exit;
		end if;
	end loop;
	close C02;
end if;

return	nvl(vl_saldo_w,0);

end;
/