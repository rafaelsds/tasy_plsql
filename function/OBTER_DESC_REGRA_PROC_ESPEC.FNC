CREATE OR REPLACE
FUNCTION Obter_desc_regra_proc_espec(nr_seq_p		Number)
		             	RETURN Varchar IS

ds_regra_w		Varchar2(254) := null;
BEGIN

	begin
	select		substr(ds_regra,1,254)
	into		ds_regra_w
	from 		pls_oc_proc_especialidade
	where 		nr_sequencia	= nr_seq_p;
	exception
		when others then
			ds_regra_w	:= null;
	end;

return ds_regra_w;
END Obter_desc_regra_proc_espec;
/