create or replace
function obter_se_perfil_equip(cd_equipamento_p		number,
				cd_perfil_p		number,
				cd_convenio_p	varchar2 default null)
 		    	return varchar2 is

qt_registro_w	number(5,0);
ie_retorno_w	varchar2(1);

begin

if 	(nvl(cd_equipamento_p,0) > 0) and (nvl(cd_perfil_p,0) > 0) then
	select 	count(*)
	into	qt_registro_w
	from	liberacao_equipamento
	where	cd_equipamento	=	cd_equipamento_p;
	
	if (qt_registro_w > 0 ) then
		
		select 	nvl(max('S'),'N')
		into	ie_retorno_w
		from	liberacao_equipamento
		where	cd_equipamento	=	cd_equipamento_p
		and		nvl(cd_perfil,nvl(cd_perfil_p,0))		= nvl(cd_perfil_p,0)
		and		nvl(cd_convenio,nvl(cd_convenio_p,0))	= nvl(cd_convenio_p,0)
		and 	not exists (select 1 
							from	liberacao_equipamento
							where	cd_equipamento	=	cd_equipamento_p
							and		nvl(cd_perfil,nvl(cd_perfil_p,0))		= nvl(cd_perfil_p,0)
							and		nvl(cd_convenio,nvl(cd_convenio_p,0))	= nvl(cd_convenio_p,0)
							and 	ie_exclusao = 'S')
		and 	ie_exclusao <> 'S';

	else
		ie_retorno_w := 'S';
	end if;
	
end if;

return	ie_retorno_w;
 
end obter_se_perfil_equip;
/