create or replace
function ptu_obter_tp_nota(	nr_seq_conta_p		pls_conta.nr_sequencia%type)
				return Varchar2 is

ie_retorno_w	varchar2(1);
ie_tipo_guia_w	pls_conta.ie_tipo_guia%type;
/*Realiza o de para no tipo de guia para que o mesmo se encaixe nos padr�es do PTU*/
begin

if	(nr_seq_conta_p is not null) then
	select	ie_tipo_guia
	into	ie_tipo_guia_w
	from	pls_conta
	where	nr_sequencia	= nr_seq_conta_p;
	
	if	(ie_tipo_guia_w	= '3') then
		ie_retorno_w	:= '1';
	elsif	(ie_tipo_guia_w = '4') then
		ie_retorno_w	:= '2';
	elsif	(ie_tipo_guia_w	= '5') then
		ie_retorno_w	:= '3';
	elsif	(ie_tipo_guia_w	= '6') then
		ie_retorno_w	:= '4';
	end if;
	
end if;

return	ie_retorno_w;

end ptu_obter_tp_nota;
/