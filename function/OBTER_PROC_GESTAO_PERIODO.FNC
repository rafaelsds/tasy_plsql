create or replace
function obter_proc_gestao_periodo(	cd_setor_atendimento_p		number,
					nr_atendimento_p		number,
					ie_somente_exec_p		varchar2,
					nm_usuario_p			varchar2,
					dt_inicial_p			date,
					dt_final_p			date)
					return varchar2 is
				
ie_existe_proc_w	varchar2(1);
				
begin
ie_existe_proc_w	:= 'N';
if	(nr_atendimento_p is not null) then	
	
		select	nvl(max('S'),'N')
		into	ie_existe_proc_w
		from	gestao_servicos_v
		where	dt_validade_prescr	> sysdate
		and	nr_atendimento		= nr_atendimento_p
		and	ie_tipo_item		= 'P'
		and	(((ie_somente_exec_p	= 'S') and (dt_fim_horario is not null)) or
			((ie_somente_exec_p	= 'N') and (dt_fim_horario is null)) or
			(ie_somente_exec_p	= 'A'))
		and	obter_se_proc_classe(nr_seq_proc_interno, nm_usuario_p) = 'S'
		and	dt_prescricao between dt_inicial_p and dt_final_p;
		
		if	(ie_existe_proc_w = 'N') then
			select	nvl(max('S'),'N')
			into	ie_existe_proc_w
			from	gestao_servicos_v
			where	dt_validade_prescr	> sysdate
			and	nr_atendimento		= nr_atendimento_p
			and	ie_tipo_item		= 'E'
			and	(((ie_somente_exec_p	= 'S') and (dt_fim_horario is not null)) or
				((ie_somente_exec_p	= 'N') and (dt_fim_horario is null)) or
				(ie_somente_exec_p	= 'A'))
			and	dt_prescricao between dt_inicial_p and dt_final_p;				
		--	and	obter_se_proc_classe(nr_seq_proc_interno, nm_usuario_p) = 'S';
		end if;
		
end if;

return	ie_existe_proc_w;

end obter_proc_gestao_periodo;
/