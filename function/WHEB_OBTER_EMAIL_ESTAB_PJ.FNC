create or replace
function wheb_obter_email_estab_pj(cd_cnpj_p		varchar2)
 		    	return varchar2 is

ds_email_w	varchar2(255);
begin

select 	max(ds_email)
into	ds_email_w
from	pessoa_juridica_estab
where	cd_cgc = cd_cnpj_p
and	cd_estabelecimento = 1;

return	ds_email_w;

end wheb_obter_email_estab_pj;
/