create or replace
function cpoe_obter_se_obriga_just(	ds_severidade_p		in	varchar2,
									nr_atendimento_p	in	number,
									cd_perfil_p			in	number) 
						return varchar2 is

	ie_consistir_w			varchar2(1 char);
	ie_severidade_just_w 	cpoe_regra_interacao.ie_severidade_just%type;

begin

	ie_consistir_w := 'N';


	if	(ds_severidade_p is not null) then

		<<case_just>>
		CASE
			WHEN ds_severidade_p = 'CONTRAINDICATED' THEN
				ie_severidade_just_w := 'I';
			WHEN ds_severidade_p = 'MINOR' THEN
				ie_severidade_just_w := 'L';
			WHEN ds_severidade_p = 'MODERATE' THEN
				ie_severidade_just_w := 'M';
			WHEN ds_severidade_p = 'MAJOR' THEN
				ie_severidade_just_w := 'S';
			WHEN ds_severidade_p = 'SUBTHERAPEUTIC' THEN
				ie_severidade_just_w := 'M';
			WHEN ds_severidade_p = 'OVERDOSE' THEN
				ie_severidade_just_w := 'S';
		END CASE case_just;

		
		if (ie_severidade_just_w is not null) then
			ie_consistir_w := cpoe_consiste_severidade_just(cd_perfil_p, nr_atendimento_p, ie_severidade_just_w);
		end if;
		
	end if;

	return	ie_consistir_w;

end cpoe_obter_se_obriga_just;
/
