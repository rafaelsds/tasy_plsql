create or replace
function obter_se_permite_gerar_nfse
 		    	return varchar2 is

ie_retorno_w		varchar2(1) := 'S';
qt_servico_w		number(10);
ie_sair_w		varchar2(1) := 'N';
nr_seq_nota_fiscal_w	number(10);
	
Cursor C01 is
	select	nr_seq_nota_fiscal
	from	w_nota_fiscal
	order by
		nr_seq_nota_fiscal;
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_nota_fiscal_w;
exit when C01%notfound or ie_sair_w = 'S';
	begin
	
	select	count(*) 
	into	qt_servico_w
	from	(
		select	obter_dados_grupo_servico_item(obter_item_servico_proced(b.cd_procedimento, b.ie_origem_proced), 'CD') 
		from	nota_fiscal a,
			nota_fiscal_item b
		where	a.nr_sequencia = b.nr_sequencia
		and   	b.cd_material is null
		and   	a.nr_sequencia = nr_seq_nota_fiscal_w
		group by 
			obter_dados_grupo_servico_item(obter_item_servico_proced(b.cd_procedimento, b.ie_origem_proced), 'CD')
		);
				
	if	(qt_servico_w > 1) then			
		ie_retorno_w	:= 'N';
		ie_sair_w	:= 'S';		
	else	
		ie_retorno_w	:= 'S';		
	end if;			
		
	end;
end loop;
close C01;

return	ie_retorno_w;

end obter_se_permite_gerar_nfse;
/
