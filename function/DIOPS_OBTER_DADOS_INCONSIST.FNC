create or replace
function diops_obter_dados_inconsist
			(	nr_seq_inconsistencia_p		Number,
				ie_tipo_retorno_p		Varchar2)
 		    		return Varchar2 is

/* ie_tipo_retorno_p
	C - C�digo
	D - Descri��o
*/

ds_retorno_w			Varchar2(255);
cd_inconsistencia_w		Number(4);
ds_inconsistencia_w		Varchar2(255);

begin

select	cd_inconsistencia,
	ds_inconsistencia
into	cd_inconsistencia_w,
	ds_inconsistencia_w
from	diops_inconsistencia
where	nr_sequencia	= nr_seq_inconsistencia_p;

if	(ie_tipo_retorno_p	= 'C') then
	ds_retorno_w		:= cd_inconsistencia_w;
elsif	(ie_tipo_retorno_p	= 'D') then
	ds_retorno_w		:= ds_inconsistencia_w;	
end if;	

return	ds_retorno_w;

end diops_obter_dados_inconsist;
/