create or replace
function pls_obter_seq_preco_benef
			(	nr_seq_segurado_p	number,
				dt_preco_p		date,
				cd_estabelecimento_p	number)
 		    	return number is

nr_seq_preco_w		number(10);

begin

select	max(a.nr_sequencia)
into	nr_seq_preco_w
from	table(pls_segurado_preco_pck.obter_preco_segurado(nr_seq_segurado_p,null,cd_estabelecimento_p)) a
where	ie_situacao = 'A'
and	dt_preco <= dt_preco_p;

return	nr_seq_preco_w;

end pls_obter_seq_preco_benef;
/