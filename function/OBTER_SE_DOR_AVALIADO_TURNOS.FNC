create or replace
function obter_se_dor_avaliado_turnos(		nr_atendimento_p 	Number,
					dt_sinal_vital_p	Date
					)
					return varchar2 is

ds_retorno_w		Varchar2(255);			
qt_turnos_atendimento_w	Number(10);
qt_turno_avaliados_w	Number(10);

begin

select  	count(distinct nr_sequencia)
into	qt_turnos_atendimento_w
from	turno_Atendimento;

select 	count(distinct nr_seq_turno_atend)
into	qt_turno_avaliados_w
from	w_eis_escala_dor
where	nr_atendimento = nr_atendimento_p
and	dt_sinal_vital = dt_sinal_vital_p;

if	(qt_turnos_atendimento_w = qt_turno_avaliados_w) then
	ds_retorno_w:= 'S';
else
	ds_retorno_w:= 'N';
end if;


return	ds_retorno_w;

end obter_se_dor_avaliado_turnos;
/
