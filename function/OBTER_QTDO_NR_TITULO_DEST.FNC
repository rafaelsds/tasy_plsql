create or replace
function obter_qtdo_nr_titulo_dest(nr_titulo_p	number)
 		    	return number is
qt_registros_w number(10,0);
begin
if	(nr_titulo_p is not null) then
	begin
	select  count(*) 
	into	qt_registros_w
	from    titulo_pagar a
	where   a.nr_titulo_dest  = nr_titulo_p;
	end;
end if;

return	qt_registros_w;

end obter_qtdo_nr_titulo_dest;
/