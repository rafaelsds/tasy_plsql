create or replace 
function obter_rep_calc_pacote(nr_seq_proc_pacote_p	number,
				vl_item_p		number,
				ie_cancelamento_p varchar2 default null, 
				nr_seq_proc_origem_p number default null,
				nr_seq_mat_origem_p number default null)
				return number is

vl_pacote_w		number(15,4);
vl_repasse_calc_w	number(15,4);
vl_retorno_w		number(15,4) := 0;
nr_seq_proc_pacote_w	procedimento_paciente.nr_seq_proc_pacote%type;

begin

if (ie_cancelamento_p is null) or (ie_cancelamento_p <> 'E') then
	nr_seq_proc_pacote_w	:= nr_seq_proc_pacote_p;
elsif	(ie_cancelamento_p = 'E') then
	if	(nvl(nr_seq_proc_origem_p,0) <> 0) then
		begin
		select	nr_seq_proc_pacote
		into	nr_seq_proc_pacote_w
		from	procedimento_paciente
		where	nr_sequencia = nr_seq_proc_origem_p
		and 	nr_seq_proc_pacote 	is not null
		and		cd_motivo_exc_conta	is null;
		exception
		when others then
			nr_seq_proc_pacote_w	:= null;
		end;	
	elsif	(nvl(nr_seq_mat_origem_p,0) <> 0) then
		begin
		select	nr_seq_proc_pacote
		into	nr_seq_proc_pacote_w
		from	material_atend_paciente
		where	nr_sequencia = nr_seq_mat_origem_p
		and 	nr_seq_proc_pacote 	is not null
		and		cd_motivo_exc_conta	is null;
		exception
		when others then
			nr_seq_proc_pacote_w	:= null;
		end;
	end if;
end if;

if	(nr_seq_proc_pacote_w is not null) then

	select 	sum(nvl(vl_procedimento,0)),
		sum(nvl(vl_repasse_calc,0))
	into	vl_pacote_w,
		vl_repasse_calc_w
	from 	procedimento_paciente
	where	nr_sequencia		= nr_seq_proc_pacote_w
	and 	nr_seq_proc_pacote 	is not null
	and	cd_motivo_exc_conta	is null;

	vl_retorno_w	:= dividir_sem_round(vl_item_p, vl_pacote_w) * vl_repasse_calc_w;
else
	vl_retorno_w	:= 0;
end if;

return vl_retorno_w;

end obter_rep_calc_pacote;
/