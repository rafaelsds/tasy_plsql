create or replace
function obter_dados_coleta_entrega(nr_prescricao_p	number,
			   	    ie_opcao_p	varchar2)
				return varchar2 as
/*
ie_opcao_p :
	CC = C�dico setor coleta
	DC = Descri��o setor coleta
	CE = C�digo setor entrega
	DE = Descri��o setor entrega
*/

ds_retorno_w	varchar(100);

begin

if 	(ie_opcao_p = 'CE') then
	select	max(cd_setor_entrega)
	into	ds_retorno_w
	from	prescr_procedimento
	where	nr_prescricao	=	nr_prescricao_p;
elsif 	(ie_opcao_p = 'DE') then
	select	substr(obter_nome_setor(max(cd_setor_entrega)),1,100)
	into	ds_retorno_w
	from	prescr_procedimento
	where	nr_prescricao	=	nr_prescricao_p;
elsif 	(ie_opcao_p = 'CC') then
	select	max(cd_setor_coleta)
	into	ds_retorno_w
	from	prescr_procedimento
	where	nr_prescricao	=	nr_prescricao_p;
elsif 	(ie_opcao_p = 'DC') then
	select	substr(obter_nome_setor(max(cd_setor_coleta)),1,100)
	into	ds_retorno_w
	from	prescr_procedimento
	where	nr_prescricao	=	nr_prescricao_p;
end if;

return	nvl(ds_retorno_w,'0');

end obter_dados_coleta_entrega;
/
