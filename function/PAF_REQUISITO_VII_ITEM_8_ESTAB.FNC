create or replace
function paf_requisito_vii_item_8_estab(cd_estabelecimento_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(2000);			

begin

select 	'E1' ||
	substr(lpad(elimina_caractere_especial(e.cd_cgc),14,'0'),1,14) ||
	substr(lpad(elimina_caractere_especial(nvl(p.nr_inscricao_estadual,' ')),14,'0'),1,14) ||
	substr(lpad(elimina_caractere_especial(nvl(p.nr_inscricao_municipal,' ')),14,'0'),1,14) ||
	substr(rpad(p.ds_razao_social,50,' '),1,50)
into	ds_retorno_w
from 	estabelecimento e,
	pessoa_juridica p
where	e.cd_cgc = p.cd_cgc
and	e.cd_estabelecimento = cd_estabelecimento_p;

return	ds_retorno_w;

end paf_requisito_vii_item_8_estab;
/