create or replace function pc_obter_dt_entrada(nr_atendimento_p varchar2)
return date is 
dt_entrada_w date;

begin

if (nr_atendimento_p is not null) then
  select dt_entrada
  into dt_entrada_w
  from atendimento_paciente
  where nr_atendimento = nr_atendimento_p;
end if;

return dt_entrada_w;

end pc_obter_dt_entrada;
/