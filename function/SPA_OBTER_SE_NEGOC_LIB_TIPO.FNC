create or replace
function spa_obter_se_negoc_lib_tipo(	cd_forma_negociacao_p	Number,
				nr_seq_tipo_p	Number)
 		    	return Varchar2 is
			
ds_retorno_w	varchar2(1)	:= 'S';
qt_regra_w	number(10,0)	:= 0;

begin

if	(cd_forma_negociacao_p is not null) and
	(nr_seq_tipo_p is not null) then
	
	select	count(*)
	into	qt_regra_w
	from	spa_forma_negociacao
	where	nr_seq_tipo = nr_seq_tipo_p;
	
	if	(qt_regra_w > 0) then
		select	nvl(max('S'),'N')
		into	ds_retorno_w
		from	spa_forma_negociacao
		where	nr_seq_tipo = nr_seq_tipo_p
		and	cd_forma_negociacao = cd_forma_negociacao_p;
	end if;
	
end if;

return	ds_retorno_w;

end spa_obter_se_negoc_lib_tipo;
/