create or replace
function obter_percentual_ordem_etapa( nr_seq_cron_etapa_p		number )
 		    	return number is

pr_etapa_w		number(10);

begin
	pr_etapa_w := 0;

	select	pr_etapa
	into	pr_etapa_w
	from	proj_cron_etapa
	where	nr_sequencia = nr_seq_cron_etapa_p;

	/*Se retornar zero, n�o consiste a informa��o do % etapa ao finalizar atividade na OS*/
	if	(pr_etapa_w = 0) then
		pr_etapa_w	:= null;
	end if;
	
	return	pr_etapa_w;

end obter_percentual_ordem_etapa;
/
