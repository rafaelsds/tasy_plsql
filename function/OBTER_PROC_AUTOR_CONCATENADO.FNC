create or replace
function obter_proc_autor_concatenado(nr_sequencia_autor_p 	number)
 		    	return varchar2 is
ds_retorno_w		varchar2(4000) := '';
cd_procedimento_w	number(15);
ds_procedimento_w	varchar2(255);

Cursor C01 is
	select	p.cd_procedimento cd_proced,
		substr(obter_descricao_procedimento(p.cd_procedimento,p.ie_origem_proced),1,100) ds_proced
	from	procedimento_autorizado p
	where	p.nr_sequencia_autor = nr_sequencia_autor_p;

begin

open C01;
loop
fetch C01 into
	cd_procedimento_w,
	ds_procedimento_w;
exit when C01%notfound;
	begin
	if (ds_retorno_w is null) then
		ds_retorno_w := substr(ds_retorno_w || cd_procedimento_w || ' - ' || ds_procedimento_w,1,4000);
	else
		ds_retorno_w := substr(ds_retorno_w || chr(13)|| chr(10) || cd_procedimento_w || ' - ' || ds_procedimento_w,1,4000);
	end if;
	end;
end loop;
close C01;

return	ds_retorno_w;

end obter_proc_autor_concatenado;
/