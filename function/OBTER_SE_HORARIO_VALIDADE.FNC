create or replace
function Obter_Se_horario_validade(	nr_prescricao_p		number,
					nr_seq_item_p		number)
 		    	return varchar2 is

ds_horarios_w		varchar2(255);
ds_hora_w		varchar2(255);
ds_horarios_retorno_w	varchar2(255) := '';
dt_inicio_prescr_w	date;
dt_validade_prescr_w	date;
dt_prescricao_w		date;
dt_horario_w		date;
qt_dia_adic_w		number(10,0);
k			number(10,0);

begin

select	padroniza_horario_prescr(b.ds_horarios, decode(nvl(b.qt_dia_prim_hor,0),0,decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,b.hr_prim_horario),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss')),'01/01/2000 23:59:59')),
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	trunc(a.dt_inicio_prescr,'mi')
into	ds_horarios_w,
	dt_inicio_prescr_w,
	dt_validade_prescr_w,
	dt_prescricao_w
from	prescr_material b,
	prescr_medica a
where	a.nr_prescricao	= nr_prescricao_p
and	a.nr_prescricao	= b.nr_prescricao
and	b.nr_sequencia	= nr_seq_item_p;
qt_dia_adic_w:= 0;
if	(ds_horarios_w is not null) then
	while	(ds_horarios_w is not null) LOOP
		select	instr(ds_horarios_w, ' ') 
		into	k
		from	dual;

		if	(k > 1) and
			(substr(ds_horarios_w, 1, k -1) is not null) then
			ds_hora_w		:= substr(ds_horarios_w, 1, k-1);
			ds_hora_w		:= replace(ds_hora_w, ' ','');
			ds_horarios_w		:= substr(ds_horarios_w, k + 1, 2000);
		elsif	(ds_horarios_w is not null) then
			ds_hora_w 		:= replace(ds_horarios_w,' ','');
			ds_horarios_w	:= '';
		end if;
				
		if	(instr(ds_hora_w,'A') > 0) then
			qt_dia_adic_w	:= 1;
		end if;
		if	(instr(ds_hora_w,'AA') > 0) then
			qt_dia_adic_w	:= qt_dia_adic_w + 1;
		end if;
		
		ds_hora_w	:= replace(ds_hora_w,'A','');
		ds_hora_w	:= replace(ds_hora_w,'A','');
		
		dt_horario_w	:= to_date(to_char(dt_prescricao_w + nvl(qt_dia_adic_w,0),'dd/mm/yyyy')||' '||replace(ds_hora_w,'A','')||':00','dd/mm/yyyy hh24:mi:ss');
		
		if	(dt_horario_w < dt_prescricao_w) then
			dt_horario_w	:= dt_horario_w + 1;
		end if;
		
		if	(dt_horario_w		< dt_inicio_prescr_w) or
			(dt_horario_w		> dt_validade_prescr_w) then
			ds_horarios_retorno_w	:= ds_horarios_retorno_w || ' - ' ||ds_hora_w;
		end if;
		
	end loop;
end if;

return	ds_horarios_retorno_w;

end Obter_Se_horario_validade;
/