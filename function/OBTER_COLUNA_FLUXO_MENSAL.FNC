Create or Replace
FUNCTION OBTER_COLUNA_FLUXO_MENSAL(	qt_dia_p		number,
					dt_mes_ref_p		date,
					dt_atual_p		date,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2)
						RETURN VarChar2 IS

qt_dia_util_w		number(10);
qt_dia_util_anterior_w	number(10);
qt_dia_atual_w		number(10);
ie_dia_util_w		varchar2(1);
ds_coluna_w		varchar2(10);
ie_passado_real_w	varchar2(1);

BEGIN

select	to_number(to_char(obter_dia_util_mes(cd_estabelecimento_p,dt_mes_ref_p,qt_dia_p),'dd'))
into	qt_dia_util_w
from	dual;

select	max(a.ie_passado_real)
into	ie_passado_real_w
from	parametro_fluxo_caixa a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_passado_real_w = 'S') then

	select	substr(obter_se_dia_util(dt_atual_p,cd_estabelecimento_p),1,255)
	into	ie_dia_util_w
	from	dual;

	qt_dia_atual_w	:= to_number(to_char(dt_atual_p,'dd'));

	if	(to_char(qt_dia_util_w)	= to_char(dt_atual_p,'dd')) then
		ds_coluna_w	:= qt_dia_util_w || ' P';
	else
		select	to_number(to_char(obter_dia_util_mes(cd_estabelecimento_p,dt_mes_ref_p,qt_dia_p - 1),'dd'))
		into	qt_dia_util_anterior_w
		from	dual;

		if	(qt_dia_util_anterior_w = qt_dia_atual_w) then
			ds_coluna_w	:= qt_dia_util_anterior_w || ' R';
		elsif	(qt_dia_util_anterior_w > qt_dia_atual_w) then
			ds_coluna_w	:= qt_dia_util_anterior_w;
		else
			ds_coluna_w	:= qt_dia_util_w;
		end if;
	end if;

else

	ds_coluna_w	:= qt_dia_util_w;

end if;

RETURN ds_coluna_w;

END OBTER_COLUNA_FLUXO_MENSAL;
/