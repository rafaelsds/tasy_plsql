create or replace
function Verifica_se_AtendPA( NR_ATEND_ORIGEM_PA_p   number,
			      cd_pessoa_fisica_p     number)
 		    	return varchar2 is

cd_pessoa_fisica_cop_w   varchar2(10);
ds_retorno_w		 varchar2(10);			
begin

if	((NR_ATEND_ORIGEM_PA_p is not null) and
	(cd_pessoa_fisica_p is not null)) then
   select  max(cd_pessoa_fisica)
   into	   cd_pessoa_fisica_cop_w
   from    atendimento_paciente
   where   nr_atendimento = NR_ATEND_ORIGEM_PA_p;
end if; 
 
if ((cd_pessoa_fisica_cop_w is not null) and
      (cd_pessoa_fisica_cop_w = cd_pessoa_fisica_p)) then
   ds_retorno_w := 'S';
else
   ds_retorno_w := 'N';   
end if;

return	ds_retorno_w;

end Verifica_se_AtendPA;
/