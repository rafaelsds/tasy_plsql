create or replace
function man_obter_desc_tipo_equip(	nr_seq_tipo_equip_p	number)
						return varchar2 is

ds_tipo_equip_w				varchar2(50);

begin

if	(nr_seq_tipo_equip_p is not null) then
	
	select	nvl(max(ds_tipo_equip),'')
	into	ds_tipo_equip_w
	from	man_tipo_equipamento
	where	nr_sequencia = nr_seq_tipo_equip_p;

end if;

return ds_tipo_equip_w;

end man_obter_desc_tipo_equip;
/