create or replace
function obter_saldo_dev_resp_consig(
			nr_seq_item_p	Number)
    	return Number is

qt_material_w		Number(13,4);
qt_saldo_w		Number(13,4);
			
begin

begin
select	nvl(sum(a.qt_material),0)
into	qt_material_w
from	sup_resp_consig_item_dev a
where	a.nr_seq_item = nr_seq_item_p;
exception
	when no_data_found then
		qt_saldo_w := 0;
end;		

select	sum(a.qt_material) - qt_material_w
into	qt_saldo_w
from	sup_resp_consig_item a
where	a.nr_sequencia = nr_seq_item_p;

return	qt_saldo_w;

end obter_saldo_dev_resp_consig;
/