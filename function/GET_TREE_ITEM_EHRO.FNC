create or replace
function get_tree_item_ehro(nr_seq_pepo_item_p		number,
							others_p				varchar2 default null)
							return number is

/* Return the schematic panel for relative item of EHRO */

nr_seq_schematic_w	number(10);
							
begin

if	(others_p is not null) then
	/* Agenda */
	if	(others_p = 'AG') then
		nr_seq_schematic_w := 122266;
	/* Meus Pacientes */
	elsif	(others_p = 'MP') then
		nr_seq_schematic_w := 122289;
	/* Cirurgias do dia */
	elsif	(others_p = 'CD') then
		nr_seq_schematic_w := 122281;
	/* Crirugia/Procedimento */
	elsif	(others_p = 'CIR') then
		nr_seq_schematic_w	:= 122272;
	/* Cirurgia PEPO */
	elsif	(others_p = 'CIRP') then
		nr_seq_schematic_w	:= 122280;
	/* Grupo cirurgia */
	elsif	(others_p = 'CIRG') then
		nr_seq_schematic_w	:= 122272;
	/* Pendencias */
	elsif 	(others_p = 'PEND') then
		nr_seq_schematic_w 	:= 1075250;
	/* Criar nova cirurgia */
	elsif 	(others_p = 'NCIR') then
		nr_seq_schematic_w 	:= 1080343;
	/* painel de documentacao cirurgica */
  elsif ( others_p = 'PDC' ) THEN
      nr_seq_schematic_w := 1192904;
  end if;
/* Normal items */
else
	if	(nr_seq_pepo_item_p = 7) then
		/* Avaliacao pre-anestesica */
		nr_seq_schematic_w	:= 122276;
	elsif	(nr_seq_pepo_item_p = 11) then
		/* Sinais vitais e monitorizacao*/
		nr_seq_schematic_w	:= 122303;
	elsif	(nr_seq_pepo_item_p = 14) then
		/* Ganhos e perdas*/
		nr_seq_schematic_w	:= 122298;
	elsif	(nr_seq_pepo_item_p = 15) then
		/* Posicao/Pele/Incisao/Curativo/Demarcacao */
		nr_seq_schematic_w	:= 122311;
	elsif	(nr_seq_pepo_item_p = 49) then
		/* Dispositivos */
		nr_seq_schematic_w := null;
		--nr_seq_schematic_w	:= 122268; Alterado para chamar a funcao Dispositivos utilizados no paciente
	elsif	(nr_seq_pepo_item_p = 9) then
		/* Agentes anestesicos */
		nr_seq_schematic_w	:= 122267;
	elsif	(nr_seq_pepo_item_p = 37) then
		/* Materiais */
		nr_seq_schematic_w	:= 122271;
	elsif	(nr_seq_pepo_item_p = 46) then
		/* Equipamentos */
		nr_seq_schematic_w	:= 122269;
	elsif	(nr_seq_pepo_item_p = 10) then
		/* Terapia hidroeletrolotica */
		nr_seq_schematic_w	:= 122305;
	elsif	(nr_seq_pepo_item_p = 78) then
		/* Boletins informativos */
		nr_seq_schematic_w	:= 122277;
	elsif	(nr_seq_pepo_item_p = 13) then
		/* Medicamentos */
		nr_seq_schematic_w	:= 122288;
	elsif	(nr_seq_pepo_item_p = 43) then
		/* Hemoderivados*/
		nr_seq_schematic_w	:= 122270;
	elsif	(nr_seq_pepo_item_p = 16) then
		/* Anestesia - tecnica e descricao */
		nr_seq_schematic_w	:= 301183;
	elsif	(nr_seq_pepo_item_p = 18) then	
		/*Cirurgia - resumo e descricao */
		nr_seq_schematic_w	:= 122286;
	elsif	(nr_seq_pepo_item_p = 100) then
		/* Hemodinamica (Em desenvolvimento) */
		nr_seq_schematic_w	:= null;
	elsif	(nr_seq_pepo_item_p = 17) then
		/* Evolucoes */
		nr_seq_schematic_w	:= 122294;
	elsif	(nr_seq_pepo_item_p = 20) then
		/* Prescricoes */
		--nr_seq_schematic_w	:= 122299;
		/* Abrir a CPOE */
		nr_seq_schematic_w	:= 689187;
	elsif	(nr_seq_pepo_item_p = 39) then
		/* Resumo dos gastos */
		nr_seq_schematic_w	:= 122301;
	elsif	(nr_seq_pepo_item_p = 21) then
		/* Liberacao SRA */
		nr_seq_schematic_w	:= 122309;
	elsif	(nr_seq_pepo_item_p = 52) then
		/* Controle de cavidade */
		nr_seq_schematic_w	:= 122287;
	elsif	(nr_seq_pepo_item_p = 51) then
		/* Faturamento medico */
		nr_seq_schematic_w	:= 122297;
	elsif	(nr_seq_pepo_item_p = 57) then
		/* SAE */
		nr_seq_schematic_w	:= 283217;
	elsif	(nr_seq_pepo_item_p = 58) then
		/* Avaliacoes*/
		nr_seq_schematic_w	:= 122285;
	elsif	(nr_seq_pepo_item_p = 59) then
		/* Conjuntos */
		nr_seq_schematic_w	:= 122282;
	elsif	(nr_seq_pepo_item_p = 60) then
		/* Orientacoes de alta */
		nr_seq_schematic_w	:= 122310;
	elsif	(nr_seq_pepo_item_p = 61) then
		/* Eventos */
		nr_seq_schematic_w	:= 122291;
	elsif	(nr_seq_pepo_item_p = 62) then
		/* Tempos e movimentos */
		nr_seq_schematic_w	:= 280668;
	elsif	(nr_seq_pepo_item_p = 64) then
		/* Obstetricia/Nascimentos */
		nr_seq_schematic_w	:= 122290;
	elsif	(nr_seq_pepo_item_p = 66) then
		/* Atestado */
		nr_seq_schematic_w	:= 122274;
	elsif	(nr_seq_pepo_item_p = 67) then
		/* Receita */
		nr_seq_schematic_w	:= 122300;
	elsif	(nr_seq_pepo_item_p = 69) then
		/* Escalas e indices */
		nr_seq_schematic_w	:= null;
	elsif	(nr_seq_pepo_item_p = 75) then
		/* Autorizacao convenio */
		nr_seq_schematic_w	:= 122275;
	elsif	(nr_seq_pepo_item_p = 80) then
		/* Check List de prontuario */
		nr_seq_schematic_w	:= 122278;
	elsif	(nr_seq_pepo_item_p = 86) then
		/* Exames laboratoriais */
		nr_seq_schematic_w	:= 122295;
	elsif	(nr_seq_pepo_item_p = 90) then
		/* CIH */
		nr_seq_schematic_w	:= 122279;
	elsif	(nr_seq_pepo_item_p = 93) then
		/* Diagnosticos */
		nr_seq_schematic_w	:= 122283;
	elsif	(nr_seq_pepo_item_p = 96) then
		/* Exames nao laboratoriais*/
		nr_seq_schematic_w	:= 122296;
	elsif	(nr_seq_pepo_item_p = 93) then
		/* CPOE */
		nr_seq_schematic_w	:= null;
	elsif	(nr_seq_pepo_item_p = 104) then
		/* CPOE */
		nr_seq_schematic_w	:= 689187;
	elsif	(nr_seq_pepo_item_p = 121) then
		/* Eventos adicionais */
		nr_seq_schematic_w	:= 1124922;
	elsif	(nr_seq_pepo_item_p = 130) then
		/* Surgery Summary - Japan */
		nr_seq_schematic_w	:= 1203637;
	end if;
end if;

return	nr_seq_schematic_w;

end get_tree_item_ehro;
/
