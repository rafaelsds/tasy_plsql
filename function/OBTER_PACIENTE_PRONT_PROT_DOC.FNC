create or replace
function obter_paciente_pront_prot_doc(
		nr_prontuario_p		number,
		cd_pessoa_fisica_p	varchar2)
		return varchar2 is

ds_retorno_w	varchar2(255);

begin

if	(cd_pessoa_fisica_p is not null) then
	ds_retorno_w := substr(obter_nome_pf(cd_pessoa_fisica_p),1,255);
else
	ds_retorno_w := substr(obter_paciente_prontuario(nr_prontuario_p,'D'),1,255);
end if;

return	ds_retorno_w;

end obter_paciente_pront_prot_doc;
/