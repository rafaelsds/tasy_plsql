create or replace
function fa_obter_pessoa_receita_pmc( nr_seq_receita_p number)
 		    	return varchar2 is

cd_pessoa_fisica_w	varchar2(14);			

begin

if (nr_seq_receita_p is not null) then
	SELECT 	max(b.cd_pessoa_fisica) 
	into	cd_pessoa_fisica_w
	FROM   	fa_paciente_entrega a,
		fa_entrega_medicacao b,
		fa_paciente_pmc c
	WHERE  	a.nr_sequencia  	  = b.nr_seq_paciente_entrega	
	AND	a.cd_pessoa_fisica 	  = c.cd_pessoa_fisica  
	AND    	b.nr_sequencia		  = nr_seq_receita_p;
end if;


return	cd_pessoa_fisica_w;

end fa_obter_pessoa_receita_pmc;
/