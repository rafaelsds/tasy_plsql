create or replace 
function Obter_Valor_tipo_Tributo_Exp
	(nr_seq_nota_p		number,
	ie_tipo_valor_p		varchar2,
	ie_tipo_tributo_p	varchar2,
	ie_nota_item_p		varchar2)
	return number is

/*
Nota ou Item:
	N - tabela nota_fiscal_trib
	I  - tabela nota_fiscal_item_trib  

Tipos de Valores:
	V - Valor do Tributo
	B - Data base calculo
	X - Taxa de tributo
	R - C�digo de Reten��o
	D - Dedu��es 
*/

vl_retorno_w		number(15,4) := 0;
vl_tributo_w		number(13,2) := 0;
vl_base_calculo_w	number(13,2) := 0;
tx_tributo_w		number(15,4) := 0;
vl_reducao_base_w	number(15,4) := 0;
ds_darf_w		varchar2(255);
qt_servico_w		number(13);
ie_gerar_trib_nfse_w	varchar2(1);
begin

select	count(*)
into	qt_servico_w
from	operacao_nota o,
	nota_fiscal n
where	o.cd_operacao_nf = n.cd_operacao_nf
and	n.nr_sequencia = nr_seq_nota_p
and	o.ie_nf_eletronica = 'S'
and	o.ie_servico = 'S';

if	(ie_nota_item_p = 'N') then
	
	select	nvl(sum(x.vl_tributo),0),
		nvl(sum(x.vl_base_calculo),0),
		nvl(sum(x.tx_tributo),0),
		nvl(sum(x.vl_reducao_base),0),
		max(ie_gerar_trib_nfse)
	into	vl_tributo_w,
		vl_base_calculo_w,
		tx_tributo_w,
		vl_reducao_base_w,
		ie_gerar_trib_nfse_w
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	(t.ie_tipo_tributo = ie_tipo_tributo_p or nvl(ie_tipo_tributo_p,'X') = 'X')
	and	x.nr_sequencia    = nr_seq_nota_p
	and	nvl(x.ie_exporta_xml,'S') = 'S';
	
elsif (ie_nota_item_p = 'I') then
	
	select	nvl(sum(x.vl_tributo),0),
		nvl(sum(x.vl_base_calculo),0),
		nvl(sum(x.tx_tributo),0),
		nvl(sum(x.vl_reducao_base),0),
		max(ie_gerar_trib_nfse)
	into	vl_tributo_w,
		vl_base_calculo_w,
		tx_tributo_w,
		vl_reducao_base_w,
		ie_gerar_trib_nfse_w
	from	tributo t,
		nota_fiscal_item_trib x
	where	x.cd_tributo = t.cd_tributo
	and	(t.ie_tipo_tributo = ie_tipo_tributo_p or nvl(ie_tipo_tributo_p,'X') = 'X')
	and	x.nr_sequencia    = nr_seq_nota_p
	and	nvl(x.ie_exporta_xml,'S') = 'S';
	
end if;

if (ie_tipo_valor_p = 'R') then
	begin
	select 	nvl(cd_darf,SUBSTR(obter_codigo_darf(x.cd_tributo, TO_NUMBER(obter_descricao_padrao('NOTA_FISCAL','CD_ESTABELECIMENTO',nr_seq_nota_p)),
		obter_descricao_padrao('NOTA_FISCAL','CD_CGC',nr_seq_nota_p), obter_descricao_padrao('NOTA_FISCAL','CD_PESSOA_FISICA',nr_seq_nota_p)),1,10))
	into	ds_darf_w
	from	nota_fiscal_trib x,
		tributo t
	where	nr_sequencia = nr_seq_nota_p
	and	x.cd_tributo = t.cd_tributo
	and	(t.ie_tipo_tributo = ie_tipo_tributo_p or nvl(ie_tipo_tributo_p,'X') = 'X')
	and	nvl(x.ie_exporta_xml,'S') = 'S';	
	end;
end if;

if 	(ie_gerar_trib_nfse_w = 'N') and (qt_servico_w > 0) then
	vl_retorno_w    := 0;
elsif	(ie_tipo_valor_p = 'V') then
	vl_retorno_w    := vl_tributo_w;
elsif	(ie_tipo_valor_p = 'B') then
	vl_retorno_w    := vl_base_calculo_w;
elsif	(ie_tipo_valor_p = 'X') then
	vl_retorno_w    := tx_tributo_w;
elsif	(ie_tipo_valor_p = 'R') then
	vl_retorno_w    := ds_darf_w;
elsif	(ie_tipo_valor_p = 'D') then
	vl_retorno_w	:= vl_reducao_base_w;
end if;

return  vl_retorno_w;

end Obter_Valor_tipo_Tributo_Exp;
/
