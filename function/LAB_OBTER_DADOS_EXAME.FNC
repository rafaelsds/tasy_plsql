CREATE OR REPLACE
FUNCTION Lab_Obter_Dados_Exame	(nr_seq_exame_p	number,
					ie_opcao_p	varchar2)
					RETURN VARCHAR2 IS

/*
N - Nome Exame;
C - C�digo Exame;
P - C�digo do procedimento
*/

nm_exame_w		Varchar2(80);
cd_exame_w		Varchar2(20);
cd_procedimento_w	number(15);
ds_retorno_w		Varchar2(255);
nr_seq_apresent_w	number(15);

BEGIN
if	(nr_seq_exame_p is not null) then
	select	nm_exame,
		cd_exame,
		cd_procedimento,
		nvl(nr_seq_apresent,0)
	into	nm_exame_w,
		cd_exame_w,
		cd_procedimento_w,
		nr_seq_apresent_w
	from	exame_laboratorio
	where	nr_seq_exame	= nr_seq_exame_p;

	if	(nvl(ie_opcao_p, 'N') = 'N') then
		ds_retorno_w	:= nm_exame_w;
	elsif	(nvl(ie_opcao_p, 'N') = 'C') then
		ds_retorno_w	:= cd_exame_w;
	elsif	(nvl(ie_opcao_p, 'N') = 'P') then
		ds_retorno_w	:= cd_procedimento_w;
	elsif	(ie_opcao_p = 'SA') then
		ds_retorno_w	:= nr_seq_apresent_w;
	end if;
end if;

RETURN	ds_retorno_w;

END Lab_Obter_Dados_Exame;
/
