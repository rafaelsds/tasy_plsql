create or replace
function obter_reg_tit_pagar_baixa(nr_titulo_p	number,
			nr_seq_baixa_p 	number)
 		    	return number is
qt_registros_w	number(10,0);
begin
select 	count(*)         
into	qt_registros_w
from    	titulo_pagar_baixa a
where   	a.nr_titulo = nr_titulo_p
and     	a.nr_seq_baixa_origem = nr_seq_baixa_p;

return	qt_registros_w;

end obter_reg_tit_pagar_baixa;
/