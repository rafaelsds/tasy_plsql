create or replace
function OBTER_CATEGORIA_CONV_TIT_REC
			(nr_titulo_p	number)
 		    	return number is

ds_retorno_w		number(15);
nr_interno_conta_w	number(15);	
		
begin

select	max(nr_interno_conta)
into	nr_interno_conta_w
from	titulo_receber
where	nr_titulo	= nr_titulo_p;

ds_retorno_w	:= null;
if	(nr_interno_conta_w is not null) then

	select	cd_categoria_parametro
	into	ds_retorno_w
	from	conta_paciente
	where	nr_interno_conta	= nr_interno_conta_w;

end if;

return	ds_retorno_w;

end OBTER_CATEGORIA_CONV_TIT_REC;
/