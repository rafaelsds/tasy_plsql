create or replace
function lab_obter_desc_descarte (nr_seq_descarte_p lab_soro_forma_descarte.nr_sequencia%type)
 		    	return varchar2 is
				
ds_retorno_w	varchar2(255);

begin

if (nr_seq_descarte_p is not null) then
	select  ds_forma_descarte
	into	ds_retorno_w
	from    lab_soro_forma_descarte
	where   nr_sequencia = nr_seq_descarte_p;
end if;

return	ds_retorno_w;

end lab_obter_desc_descarte;
/