create or replace
function dis_obter_regra_moviment(	ie_tipo_p		varchar2,
				cd_setor_origem_p	number,
				cd_setor_destino_p	number)
 		    	return varchar2 is

qt_existe_w	number(10);
result_w	varchar(1);
			
begin

if	(ie_tipo_p = 'A') then
	select count(*)
	into	qt_existe_w
	from	dis_regra_movimentacao
	where 	ie_tipo_movimentacao = 'A'
	and 	cd_setor_origem = cd_setor_origem_p;
	
	if	(nvl(qt_existe_w,0) > 0) then
		result_w := 'S';
	else
		result_w := 'N';
	end if;		
end if;	
	
if	(ie_tipo_p = 'T') then	
	select count(*)
	into	qt_existe_w
	from	dis_regra_movimentacao
	where 	ie_tipo_movimentacao = 'T'
	and 	(nvl(cd_setor_origem,0) = nvl(cd_setor_origem_p,0) or nvl(cd_setor_origem,0) = 0)
	and 	(nvl(cd_setor_destino,0) = nvl(cd_setor_destino_p,0) or nvl(cd_setor_destino,0) = 0);
	
	if	(nvl(qt_existe_w,0) > 0) then
		result_w := 'S';
	else
		result_w := 'N';
	end if;	
end if;	

return	result_w;

end dis_obter_regra_moviment;
/