create or replace
function format_array_json (   nm_campo_p	   varchar2,
                               ds_valor_p      varchar2,
                               ie_tipo_p       number)
 		    	return Clob is
                
/* IE_TIPO_P
1 = Atributo x Valor
2 = Array
*/
          
ds_retorno_w    Clob;
          
begin

if  (ie_tipo_p = 1) then
    ds_retorno_w := '"' || nm_campo_p || '":' || '"' || ds_valor_p || '", ';
else
    ds_retorno_w := '"' || nm_campo_p || '":'  || ds_valor_p || ', ' ;
end if;

return ds_retorno_w;

end format_array_json;
/
