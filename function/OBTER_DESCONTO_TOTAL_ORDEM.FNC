create or replace
function obter_desconto_total_ordem(	nr_ordem_compra_p		number)
 		    	return number is

vl_desconto_oc_w		number(13,4) := 0;
pr_descontos_item_w	number(13,4) := 0;
vl_desconto_item_w	number(13,4) := 0;
vl_item_w			number(13,4) := 0;
vl_desconto_w		number(13,4) := 0;
vl_desconto_acum_w	number(13,4) := 0;

cursor c01 is
select	pr_descontos,
	vl_desconto,
	(qt_material * vl_unitario_material)
from	ordem_compra_item
where	nr_ordem_compra	= nr_ordem_compra_p;

begin

select	nvl(obter_valor_desconto_ordem(nr_ordem_compra_p),0)
into	vl_desconto_oc_w
from	dual;

open C01;
loop
fetch C01 into	
	pr_descontos_item_w,
	vl_desconto_item_w,
	vl_item_w;
exit when C01%notfound;
	begin
	
	if	(pr_descontos_item_w > 0) then
		begin
			vl_desconto_w	:= dividir((vl_item_w * pr_descontos_item_w), 100);
		end;
	elsif (vl_desconto_item_w > 0) then
		begin
			vl_desconto_w	:= vl_desconto_item_w;
		end;
	end if;
	
	vl_desconto_acum_w	:= vl_desconto_acum_w + vl_desconto_w;
	
	end;
end loop;
close C01;

vl_desconto_acum_w := vl_desconto_acum_w + vl_desconto_oc_w;


return	vl_desconto_acum_w;

end obter_desconto_total_ordem;
/