create or replace
function pls_obter_tipo_hist_atend
		(	nr_seq_tipo_hist_p	number)
			return varchar2 is

ds_retorno_w		varchar2(150);

begin

select	ds_tipo_historico
into	ds_retorno_w
from	pls_tipo_historico_atend
where	nr_sequencia = nr_seq_tipo_hist_p;

return	ds_retorno_w;

end pls_obter_tipo_hist_atend;
/