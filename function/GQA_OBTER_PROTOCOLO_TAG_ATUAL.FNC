create or replace function gqa_obter_protocolo_tag_atual(nr_sequencia_p in number, tipo varchar2)
  return varchar2 is

nr_seq_w              gqa_protocolo_etapa_pac.nr_sequencia%type;
ds_cor_html_w         gqa_protocolo_etapa_pac.ds_cor_html%type;
ds_tag_w              gqa_protocolo_etapa_pac.ds_tag%type;
ds_nome_w             gqa_protocolo_etapa_pac.ds_nome_etapa%type;
ds_icone_w            gqa_pendencia_regra.ie_icone%type;
ds_retorno_w          varchar2(500) := null;
nm_executor_w         gqa_protocolo_etapa_pac.nm_usuario_executor%type;

begin

  begin
    select nr_sequencia into nr_seq_w from (
        select dt_inicio, nr_sequencia from gqa_protocolo_etapa_pac 
        where nr_seq_prot_pac = nr_sequencia_p and dt_inicio is not null
        order by dt_inicio desc
    ) where rownum = 1;
  exception when no_data_found then
    nr_seq_w := null;
  end;
  
  if(nr_seq_w is not null) then
      select 
            a.ds_cor_html, 
            a.ds_tag, 
            a.ds_nome_etapa, 
            (select b.ie_icone from gqa_pendencia_regra b where b.nr_sequencia = a.nr_seq_etapa) ds_icone,
            a.nm_usuario_executor
      into 
            ds_cor_html_w, 
            ds_tag_w, 
            ds_nome_w, 
            ds_icone_w, 
            nm_executor_w 
      from gqa_protocolo_etapa_pac a
      where nr_sequencia = nr_seq_w;
      
      if(tipo = 'TAG') then
          ds_retorno_w := ds_tag_w;
      elsif(tipo = 'COR') then
          ds_retorno_w := ds_cor_html_w;
      elsif(tipo = 'SEQUENCIA') then
          ds_retorno_w := '' || nr_seq_w;
      elsif(tipo = 'NOME') then
          ds_retorno_w := ds_nome_w;
      elsif(tipo = 'ICONE') then
          ds_retorno_w := obter_valor_dominio(9642, ds_icone_w);
      elsif(tipo = 'PATH') then
          ds_retorno_w := obter_dados_img_html(gqa_obter_de_para_icones(ds_icone_w), 'PATH');
      elsif(tipo = 'EXECUTOR') then
          ds_retorno_w := nm_executor_w;
      end if;
      
  end if;

  return ds_retorno_w;
end gqa_obter_protocolo_tag_atual;
/
