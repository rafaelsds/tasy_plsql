create or replace
function obter_cor_leito_mapa (	cd_perfil_p		regra_perm_panorama.cd_perfil%type,
				cd_classif_setor_p	regra_panorama_cor_leito.cd_classif_setor%type,
				nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
				ie_html_p			varchar2 default null)
 		    	return varchar2 is

nr_seq_regra_w		regra_perm_panorama.nr_sequencia%type;
cd_convenio_w		atend_categoria_convenio.cd_convenio%type;
qt_horas_com_acomod_w	number(15,0);
qt_horas_total_w	number(15,0);
ds_retorno_w		varchar2(255);

Cursor C01 is
	select	decode(ie_html_p,'S',ds_cor_html,ds_cor_fundo) ds_cor
	into	ds_retorno_w
	from	regra_panorama_cor_leito
	where	nr_seq_regra = nr_seq_regra_w
	and	nvl(cd_classif_setor, cd_classif_setor_p) = cd_classif_setor_p
	and	nvl(cd_convenio, cd_convenio_w) = cd_convenio_w
	and	((nvl(qt_horas,0) = 0) or
		 (ie_desconsiderar_sem_acomod = 'S' and qt_horas_com_acomod_w > qt_horas) or
		 (ie_desconsiderar_sem_acomod = 'N' and qt_horas_total_w > qt_horas))
	order by nvl(cd_classif_setor,0), nvl(cd_convenio,0), nvl(qt_horas,0);

begin

select	max(a.nr_sequencia)
into 	nr_seq_regra_w
from	regra_perm_panorama a
where	a.cd_perfil = cd_perfil_p
and exists (	select 1
		from	regra_panorama_cor_leito x
		where	x.nr_seq_regra = a.nr_sequencia);

if	(nr_seq_regra_w is null) then -- se n�o encontrou a regra para o perfil, busca uma regra sem perfil definido.
	select	max(a.nr_sequencia)
	into 	nr_seq_regra_w
	from	regra_perm_panorama a
	where	a.cd_perfil is null
	and exists (	select 1
			from	regra_panorama_cor_leito x
			where	x.nr_seq_regra = a.nr_sequencia);
end if;

if	(nr_seq_regra_w is not null) then
	cd_convenio_w	:= obter_convenio_atendimento(nr_atendimento_p);

	select	trunc(sum((nvl(a.dt_saida_unidade, sysdate) - a.dt_entrada_unidade)*24)) qt_horas
	into	qt_horas_total_w
	from 	atend_paciente_unidade a,
		setor_atendimento s,
		tipo_acomodacao t
	where	s.cd_setor_atendimento = a.cd_setor_atendimento
	and	t.cd_tipo_acomodacao = a.cd_tipo_acomodacao
	and	a.nr_atendimento = nr_atendimento_p
	and	s.cd_classif_setor = cd_classif_setor_p;

	select	trunc(sum((nvl(a.dt_saida_unidade, sysdate) - a.dt_entrada_unidade)*24)) qt_horas
	into	qt_horas_com_acomod_w
	from 	atend_paciente_unidade a,
		setor_atendimento s,
		tipo_acomodacao t
	where	s.cd_setor_atendimento = a.cd_setor_atendimento
	and	t.cd_tipo_acomodacao = a.cd_tipo_acomodacao
	and	a.nr_atendimento = nr_atendimento_p
	and	nvl(t.ie_sem_acomodacao,'N') = 'N'
	and	s.cd_classif_setor = cd_classif_setor_p;
	
	open C01;
	loop
	fetch C01 into	
		ds_retorno_w;
	exit when C01%notfound;
		begin
		null;
		end;
	end loop;
	close C01;
end if;


return	ds_retorno_w;

end obter_cor_leito_mapa;
/