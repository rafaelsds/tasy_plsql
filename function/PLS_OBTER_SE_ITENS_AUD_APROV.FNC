create or replace
function pls_obter_se_itens_aud_aprov
			(	nr_seq_guia_p	number)
				return varchar2 is
				
ds_retorno_w			varchar2(1) := 'N';
nr_seq_auditoria_w		number(10);
qt_itens_aud_w			number(5);
qt_itens_aprovados_w		number(5);

begin

begin
	select	nr_sequencia
	into	nr_seq_auditoria_w
	from	pls_auditoria
	where	nr_seq_guia = nr_seq_guia_p;
exception
when others then
	nr_seq_auditoria_w := 0;
end;

if	(nr_seq_auditoria_w > 0) then
	
	select	count(*)
	into	qt_itens_aud_w
	from	pls_auditoria_item
	where	nr_seq_auditoria = nr_seq_auditoria_w;
	
	select	count(*)
	into	qt_itens_aprovados_w
	from	pls_auditoria_item
	where	nr_seq_auditoria = nr_seq_auditoria_w
	and	ie_status = 'A';
	
	if	(qt_itens_aud_w = qt_itens_aprovados_w) then
		ds_retorno_w := 'S';
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_itens_aud_aprov;
/