create or replace
function lic_obter_valor_total(		nr_seq_licitacao_p		number)
 		    	return number is

vl_item_w		number(15,4);			
			
begin

select	nvl(sum(a.qt_item * b.vl_item),0)
into	vl_item_w
from	reg_lic_item a,
	reg_lic_vencedor b
where	a.nr_Seq_licitacao	= b.nr_seq_licitacao
and	a.nr_seq_lic_item	= b.nr_seq_lic_item
and	a.nr_seq_licitacao	= nr_seq_licitacao_p;


return	vl_item_w;

end lic_obter_valor_total;
/