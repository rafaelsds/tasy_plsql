CREATE OR REPLACE
FUNCTION Obter_Proced_Conversao_Conv	(cd_convenio_p		Number,
					cd_procedimento_p	Number,
					ie_origem_proced_p	Number,
					ie_opcao_p		Varchar2)
					RETURN VARCHAR2 IS
/*
C - C�digo;
D - Descri��o;
*/

cd_proc_convenio_w	Varchar2(20);
ds_proc_convenio_w	Varchar2(100);
ds_retorno_w		Varchar2(100);
ie_escape_w		Varchar2(1)	:= 'N';

BEGIN

if	(cd_convenio_p is not null) and
	(cd_procedimento_p is not null) and
	(ie_origem_proced_p is not null) then
	begin
	select	max(cd_proc_convenio),
		max(ds_proc_convenio)
	into	cd_proc_convenio_w,
		ds_proc_convenio_w
	from	conversao_proc_convenio
	where	cd_convenio		= cd_convenio_p
	and	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;
	exception
		when others then
		ie_escape_w	:= 'S';
	end;

	if	(ie_escape_w <> 'S') then

		if	(ie_opcao_p = 'C') then
			ds_retorno_w	:= cd_proc_convenio_w;
		else
			ds_retorno_w	:= ds_proc_convenio_w;
		end if;
	end if;
	
end if;

RETURN	ds_retorno_w;

END	Obter_Proced_Conversao_Conv;
/