create or replace 
function obter_tipo_acomod_setor(	nr_atendimento_p		number,
				cd_setor_atendimento_p	number,
				nr_seq_interno_p		number,
				dt_entrada_unidade_p	date)
				return number is


cd_tipo_acomodacao_w	number(10,0) := 0;

begin

if	(nvl(nr_atendimento_p,0) > 0) then

	select	nvl(max(cd_tipo_acomodacao),0)
	into	cd_tipo_acomodacao_w
	from 	atend_paciente_unidade a
	where	a.nr_atendimento = nr_atendimento_p
	and	dt_entrada_unidade = dt_entrada_unidade_p
	and	cd_setor_atendimento = cd_setor_atendimento_p
	and	nr_seq_interno = nr_seq_interno_p;
end if;
	
return cd_tipo_acomodacao_w;

end obter_tipo_acomod_setor;
/