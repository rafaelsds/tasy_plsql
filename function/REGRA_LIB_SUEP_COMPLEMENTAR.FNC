create or replace
function regra_lib_suep_complementar(	nr_atendimento_p    number,
                                 		nr_seq_suep_p     	number)
                                 return varchar2 is
  
ds_sql_not_in_regra_w     varchar2(80) := '';
ds_sql_w                  varchar2(4000);
customSQL_w               varchar2(4000);
ie_resultado_w            varchar2(02);
nr_sequencia_w            number;

Cursor C01 is
	select	a.ds_consulta,
            a.nr_sequencia
	from    regra_liberacao_suep a
	where   a.nr_seq_suep = nr_seq_suep_p
	and	    a.ds_consulta is not null;
  
begin
    ds_sql_not_in_regra_w := 'X';

    open C01;
    loop
    fetch C01 into	
        ds_sql_w,
        nr_sequencia_w;
    exit when C01%notfound;
    begin
        
      customSQL_w :=  ' select	nvl(max(''S''),''N'') ' ||
                      ' from  	atendimento_paciente a ' ||
                      ' where  	a.nr_atendimento = :nr_atendimento ' ||
					  ds_sql_w;

      execute immediate customSQL_w
      into   ie_resultado_w
      using  nr_atendimento_p;

      if ie_resultado_w = 'N' then 
        
        if (length(ds_sql_not_in_regra_w) > 0) then
          ds_sql_not_in_regra_w := ds_sql_not_in_regra_w || ',';
        end if;

        ds_sql_not_in_regra_w := ds_sql_not_in_regra_w || nr_sequencia_w;

      end if;
      
    end;
    end loop;
    close C01;

    return	ds_sql_not_in_regra_w;
    
end regra_lib_suep_complementar;
/