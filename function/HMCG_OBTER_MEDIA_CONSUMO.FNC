Create or replace 
function HMCG_obter_media_consumo(  IE_TIPO_P	VARCHAR2,
				CD_ESTABELECIMENTO_P		NUMBER,
				CD_MATERIAL_P			NUMBER,
				DT_MESANO_REFERENCIA_P		DATE,
				QT_MESES_P			NUMBER) 
				RETURN NUMBER IS
				
ds_retorno_w			NUMBER(18,4) := 0;
qt_consumo_w			NUMBER(18,4) := 0;
vl_consumo_w			NUMBER(18,2) := 0;
qt_consumo_ww			NUMBER(18,4) := 0;
vl_consumo_ww			NUMBER(18,2) := 0;	
CONT				NUMBER(8) := 0;
QT_MESES_W			NUMBER(8);

begin

QT_MESES_W := QT_MESES_P;

IF QT_MESES_W <= 0 THEN
	QT_MESES_W := 1;
END IF;

WHILE (CONT < QT_MESES_W) LOOP
	BEGIN
	select 	nvl(OBTER_CONS_MENSAL_MAT_COM_CC('Q', cd_estabelecimento_p, cd_material_p, add_months(trunc(DT_MESANO_REFERENCIA_P,'mm'),-cont)),0),
		nvl(OBTER_CONS_MENSAL_MAT_COM_CC('V', cd_estabelecimento_p, cd_material_p, add_months(trunc(DT_MESANO_REFERENCIA_P,'mm'),-cont)),0)
	into 	qt_consumo_ww,
		vl_consumo_ww
	from dual;
	
	qt_consumo_w := qt_consumo_w + qt_consumo_ww;
	vl_consumo_w := vl_consumo_w + vl_consumo_ww;
	
	cont := cont + 1;
	END;
END LOOP;

qt_consumo_w := qt_consumo_w/QT_MESES_W;
vl_consumo_w := vl_consumo_w/QT_MESES_W;

if	(ie_tipo_p = 'Q') then
	ds_retorno_w := qt_consumo_w;
elsif	(ie_tipo_p = 'V') then
	ds_retorno_w := vl_consumo_w;
end if;

return  ds_retorno_w;

end HMCG_obter_media_consumo;
/
