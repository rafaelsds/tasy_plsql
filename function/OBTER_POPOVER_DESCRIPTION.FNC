create or replace FUNCTION OBTER_POPOVER_DESCRIPTION (
    cd_profile_p      NUMBER,
    nr_sequencia_p    NUMBER,
    ie_option_p       VARCHAR2,
    ie_popover_view_p VARCHAR2)
  RETURN VARCHAR2
AS
  /*
  The popover information constructin dynamically from the table agenda_paciente
  Domain values are being mapped and used to fetch information from the table agenda_paciente
  Configuration are coming from core table settings
  Domain : 9439
  1 - Age - QT_IDADE_PACIENTE
  2 - Patient Name - NM_PACIENTE
  3 - Gender - DS_SEXO
  4 - Physician - CD_MEDICO
  5 - Procedure - NR_SEQ_PROC_INTERNO
  6 - Additional Procedures - AGENDA_PACIENTE_PROC table (CD_PROCEDIMENTO)
  7 - Anesthetist - CD_ANESTESISTA
  8 - Anesthetia type - CD_TIPO_ANESTESIA
  9 - Notes - DS_OBSERVACAO
  10 - Department - DS_SETOR_ATENDIMENTO
  11 - Attending Doctor - ATTENDING_DOCTOR
  12 - Current department - DS_UNIDADE
  13 - Start and end time - HR_INICIO
  14 - Duration - NR_MINUTO_DURACAO
  15 - Surgery description - DS_CIRURGIA
  16 - Requesting Physician - NM_MEDICO_REQ
  17 - Anesthetist Required - DS_ANESTESIA
  18 - Personal title + Attending Physician - DS_ATTENDING_PHYSICIAN
  19 - Case - NR_EPISODIO
  20 - Personal title + Physician - DS_PHYSICIAN
  21 - Record time + Last surgical event - DS_EVENTO
  22 - Medical Department - DS_MEDICAL_DEPARTAMENTO
  23 - Surgery - NR_CIRURGIA
  24 - Order - NR_PRESCRICAO
  */
  v_description VARCHAR2(4000);
  v_bold_view   VARCHAR(20) := '<br>*TITLE*: ';
  v_config_exist VARCHAR(1) := 'S';
type additional_info
IS
  record
  (
        v_age             NUMBER,
        v_pacient_name    VARCHAR2(255),
        v_sex             VARCHAR2(20),
        v_physician       VARCHAR2(100),
        v_procedure       VARCHAR2(100),
        v_addl_proc       VARCHAR2(4000),
        v_anesthetist     VARCHAR2(4000),
        v_anesthetia_type VARCHAR2(4000),
        v_notes           VARCHAR2(4000),
        v_department      VARCHAR2(600),
		v_medical_department VARCHAR2(600),
        v_attending_doctor VARCHAR2(100),
        v_current_department VARCHAR2(100),
        v_start_and_end_time VARCHAR2(100),
        v_duration VARCHAR2(100),
        v_surgery_description VARCHAR2(100),
        v_requesting_physician VARCHAR2(100),
        v_anesthetist_required VARCHAR2(100),
        v_title_attending_physician VARCHAR2(100),
        v_case VARCHAR2(100),
        v_personal_title_physician VARCHAR2(100),
        v_record_time_last_surgical VARCHAR2(200),
        v_surgery_number  NUMBER,
        v_order_number    NUMBER
  );
    v_is_data_loaded  BOOLEAN := false;
    v_add_info        additional_info;
  CURSOR c01 (cd_profile_p NUMERIC)
  IS
    select distinct * from (SELECT f.ie_fields_config,
      NVL(f.ds_title,
      (SELECT SUBSTR(OBTER_DESC_EXPRESSAO(cd_exp_valor_dominio),1,100) ds
      FROM valor_dominio_v
      WHERE cd_dominio = 9439
      AND vl_dominio   = f.ie_fields_config
      )) ds_title,
      (CASE WHEN ie_option_p = 'C' THEN f.nr_seq_presentation ELSE f.nr_seq_apres_popover END) NR_SEQ_PRESENTATION
    FROM surgical_info_config c,
      surgical_field_config f
    WHERE c.nr_sequencia = f.nr_seq_configuration
    AND ((cd_profile_p  IS NULL
    AND c.cd_perfil     IS NULL)
    OR (cd_profile_p    IS NOT NULL
    AND c.cd_perfil      = cd_profile_p))
    AND (f.ie_configure_item      = ie_option_p OR f.ie_configure_item = 'B')
    ) ORDER BY NR_SEQ_PRESENTATION;
    CURSOR c02
    IS
      SELECT vl_dominio ie_fields_config,
        SUBSTR(OBTER_DESC_EXPRESSAO(cd_exp_valor_dominio),1,100) ds_title
      FROM valor_dominio_v
      WHERE cd_dominio = 9439
	  AND (vl_dominio = '13'
	  or vl_dominio = '14')
      ORDER BY ie_fields_config;
    FUNCTION build_template(
        ie_fields_config_p NUMBER,
        ds_title_p         VARCHAR2,
        ai_p additional_info)
      RETURN VARCHAR2
    IS
      ds_template_w VARCHAR2(600) := '';
    BEGIN

     IF ie_option_p = 'C' THEN
        v_bold_view       := '<br>*TITLE*: ';
     ELSIF ie_option_p = 'P' THEN
        v_bold_view       := '<br>*TITLE*:#? ';
     END IF;


      IF ie_option_p = 'C' AND ie_fields_config_p in (1,2,3,4) then
        v_is_data_loaded := true;
      ELSIF ie_fields_config_p = 1 AND ai_p.v_age IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w || REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_age,1,600);
      ELSIF ie_fields_config_p = 2 AND ai_p.v_pacient_name IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_pacient_name,1,600);
      ELSIF ie_fields_config_p = 3 AND ai_p.v_sex IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_sex,1,600);
      ELSIF ie_fields_config_p = 4 AND ai_p.v_physician IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_physician,1,600);
      ELSIF ie_fields_config_p = 5 AND ai_p.v_procedure IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_procedure,1,600);
      ELSIF ie_fields_config_p = 6 AND ai_p.v_addl_proc IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_addl_proc,1,600);
      ELSIF ie_fields_config_p = 7 AND ai_p.v_anesthetist IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_anesthetist,1,600);
      ELSIF ie_fields_config_p = 8 AND ai_p.v_anesthetia_type IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_anesthetia_type,1,600);
      ELSIF ie_fields_config_p = 9 AND ai_p.v_notes IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_notes,1,600);
      ELSIF ie_fields_config_p = 10 AND ai_p.v_department IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_department,1,600);
      ELSIF ie_fields_config_p = 11 AND ai_p.v_attending_doctor IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_attending_doctor,1,600);
      ELSIF ie_fields_config_p = 12 AND ai_p.v_current_department IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_current_department,1,600);
      ELSIF ie_fields_config_p = 13 AND ai_p.v_start_and_end_time IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_start_and_end_time,1,600);
      ELSIF ie_fields_config_p = 14 AND ai_p.v_duration IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_duration,1,600);
      ELSIF ie_fields_config_p = 15 AND ai_p.v_surgery_description IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_surgery_description,1,600);
      ELSIF ie_fields_config_p = 16 AND ai_p.v_requesting_physician IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_requesting_physician,1,600);
      ELSIF ie_fields_config_p = 17 AND ai_p.v_anesthetist_required IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_anesthetist_required,1,600);
      ELSIF ie_fields_config_p = 18 AND ai_p.v_title_attending_physician IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_title_attending_physician,1,600);
      ELSIF ie_fields_config_p = 19 AND ai_p.v_case IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_case,1,600);
      ELSIF ie_fields_config_p = 20 AND ai_p.v_personal_title_physician IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_personal_title_physician,1,600);
      ELSIF ie_fields_config_p = 21 AND ai_p.v_record_time_last_surgical IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_record_time_last_surgical,1,600);
	  ELSIF ie_fields_config_p = 22 AND ai_p.v_medical_department IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_medical_department,1,600);
	  ELSIF ie_fields_config_p = 23 AND ai_p.v_surgery_number IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_surgery_number,1,600);
      ELSIF ie_fields_config_p = 24 AND ai_p.v_order_number IS NOT NULL THEN
        ds_template_w         := substr(ds_template_w|| REPLACE(v_bold_view, '*TITLE*', ds_title_p) || ai_p.v_order_number,1,600);
      ELSE
        NULL;
      END IF;
      RETURN ds_template_w;
    END;
  BEGIN
    IF ie_popover_view_p = 'N' THEN
      v_bold_view       := '<br>*TITLE*: ';
    END IF;
    SELECT MAX(QT_IDADE_PACIENTE),
      MAX(NM_PACIENTE),
      MAX(SUBSTR(obter_Sexo_pf(cd_pessoa_fisica,'D'),1,60)) ds_sexo,
      MAX(obter_nome_medico(cd_medico,'PNNO')) cd_medico,
      MAX(Obter_Desc_Proc_Interno(a.NR_SEQ_PROC_INTERNO)) NR_SEQ_PROC_INTERNO,
      MAX(
      (SELECT trim(SUBSTR (SYS_CONNECT_BY_PATH (cd_procedimento , ','), 2)) csv
      FROM
       (SELECT replace(substr(obter_desc_prescr_proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,240) || decode (nvl(DS_OBSERVACAO,''),'','', ' (' ||  OBTER_DESC_EXPRESSAO(294639) || ' - ' || DS_OBSERVACAO || ' )'),',',';')  cd_procedimento ,
          ROW_NUMBER () OVER (ORDER BY cd_procedimento ) rn,
          COUNT (*) OVER () cnt
        FROM AGENDA_PACIENTE_PROC
        WHERE nr_sequencia = nr_sequencia_p
        )
      WHERE rn        = cnt
        START WITH rn = 1
        CONNECT BY rn = PRIOR rn + 1
      )) ADDL_PROC,
      obter_profissional_funcao(nr_sequencia, 'AP'),
      substr(obter_valor_dominio(36, MAX(CD_TIPO_ANESTESIA)),1,25) CD_TIPO_ANESTESIA,
      ( MAX(DS_OBSERVACAO)),
      MAX(obter_desc_setor_atend(a.cd_setor_atendimento)) ds_setor_atendimento,
	  obter_nome_departamento_medico(obter_depto_agenda_pac(a.nr_sequencia)) ds_medical_departamento,
      MAX(substr(obter_nome_medico((select MAX(b.cd_medico_resp) FROM atendimento_paciente b WHERE b.nr_atendimento = a.nr_atendimento),'N'),1,60)) ATTENDING_DOCTOR,
      MAX(substr(obter_unidade_atendimento((select at.nr_atendimento from atendimento_paciente at WHERE at.nr_atendimento = a.nr_atendimento), 'A', 'S'),1,200)) DS_UNIDADE,
	  MAX(date_as_varchar2(a.hr_inicio,'shortTime') || ' - ' || date_as_varchar2(a.hr_inicio + (a.nr_minuto_duracao/1440),'shortTime')),
	  decode(nvl(pkg_i18n.get_user_locale, 'pt_BR'),'ja_JP',MAX(to_char(trunc(sysdate)+a.nr_minuto_duracao/(1440),'HH24:MI')),MAX(to_char(a.nr_minuto_duracao) || ' ' || obter_desc_expressao(961495))) NR_MINUTO_DURACAO,
	  MAX(to_char(a.ds_cirurgia)) DS_CIRURGIA,
	  MAX(substr(obter_nome_medico(a.cd_medico_req,'NCD'),1,100)) nm_medico_req,
	  MAX(substr(obter_desc_campo_grid(337508, IE_ANESTESIA), 1,255)) ds_anestesia,
          MAX(substr(obter_nome_medico((select MAX(b.cd_medico_resp) FROM atendimento_paciente b WHERE b.nr_atendimento = a.nr_atendimento),'PNGPF'),1,60)) DS_ATTENDING_PHYSICIAN,
	  MAX(decode(obter_episodio_atend(a.nr_atendimento),0,null, obter_episodio_atend(a.nr_atendimento)) || ( case when (a.nr_atendimento) > 0 and (obter_se_atendimento_futuro(a.nr_atendimento) = 'S') then ' (P)' else null end)) NR_EPISODIO,
          MAX(SUBSTR(OBTER_NOME_MEDICO(a.CD_MEDICO,'PNGPF'),1,100)) DS_PHYSICIAN,
	  MAX((select max(date_as_varchar2(cp.dt_atualizacao, 'shortTime') || ' - ' ||obter_desc_evento_cirurgia(cp.nr_seq_evento)) ds_evento from evento_cirurgia_paciente cp where cp.nr_cirurgia = a.nr_cirurgia and cp.dt_inativacao is null
      and dt_liberacao is not null)) DS_EVENTO,
      MAX(NR_CIRURGIA),
      MAX(obter_prescr_cirurgia(NR_CIRURGIA))
    INTO v_add_info.v_age,
        v_add_info.v_pacient_name,
        v_add_info.v_sex,
        v_add_info.v_physician,
        v_add_info.v_procedure,
        v_add_info.v_addl_proc,
        v_add_info.v_anesthetist,
        v_add_info.v_anesthetia_type,
        v_add_info.v_notes,
        v_add_info.v_department,
		v_add_info.v_medical_department,
        v_add_info.v_attending_doctor,
        v_add_info.v_current_department,
        v_add_info.v_start_and_end_time,
        v_add_info.v_duration,
        v_add_info.v_surgery_description,
        v_add_info.v_requesting_physician,
        v_add_info.v_anesthetist_required,
        v_add_info.v_title_attending_physician,
        v_add_info.v_case,
        v_add_info.v_personal_title_physician,
        v_add_info.v_record_time_last_surgical,
        v_add_info.v_surgery_number,
        v_add_info.v_order_number
    FROM agenda_paciente a
    WHERE a.nr_sequencia = nr_sequencia_p
	group by a.nr_sequencia;

    FOR rec IN c01(cd_profile_p)
    LOOP
      v_description := v_description || build_template(to_number(rec.ie_fields_config), rec.ds_title, v_add_info);
    END LOOP;
    IF v_description IS NULL AND v_is_data_loaded = FALSE THEN
      FOR rec IN c01(NULL)
      LOOP
        v_description := v_description || build_template(to_number(rec.ie_fields_config), rec.ds_title, v_add_info);
      END LOOP;
    END IF;
    IF v_description IS NULL AND v_is_data_loaded = FALSE AND ie_popover_view_p = 'S' THEN
	FOR rec IN c02
	LOOP
		v_description := v_description || build_template(to_number(rec.ie_fields_config), rec.ds_title, v_add_info);
	END LOOP;

    END IF;
    RETURN v_description;
  END OBTER_POPOVER_DESCRIPTION;
  /
