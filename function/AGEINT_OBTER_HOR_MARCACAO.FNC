create or replace
function Ageint_Obter_Hor_Marcacao(
				nr_seq_item_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);
			
begin

select	to_char(max(hr_agenda),'dd/mm/yyyy hh24:mi:ss')
into	ds_retorno_w
from	ageint_marcacao_usuario
where	nr_seq_ageint_item	= nr_seq_item_p
and	nvl(ie_gerado, 'N')	= 'N';

return	ds_retorno_w;

end Ageint_Obter_Hor_Marcacao;
/