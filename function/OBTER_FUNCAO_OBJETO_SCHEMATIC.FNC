create or replace
function obter_funcao_objeto_schematic (	ie_tipo_objeto_p		varchar2,
						nr_seq_objeto_p			number )
 		    	return number is

/*
	Function utilizada em todas as triggers do REGULAT�RIO que gravam os logs de objetos do dic objetos para
	descobrir a fun��o de objetos que est�o sendo exclu�dos atrav�s de CASCADE para evitar trigger mutante

	IE_TIPO_OBJETO_P
	S:	OBJETO_SCHEMATIC
	O:	DIC_OBJETO
*/

cd_funcao_w	funcao.cd_funcao%type;

pragma autonomous_transaction;

begin

	if (ie_tipo_objeto_p = 'S') then

		select	max(cd_funcao)
		into	cd_funcao_w
		from	objeto_schematic
		where	nr_sequencia = nr_seq_objeto_p;

	elsif (ie_tipo_objeto_p = 'O') then

		select	max(cd_funcao)
		into	cd_funcao_w
		from	dic_objeto
		where	nr_sequencia = nr_seq_objeto_p;

	end if;

return cd_funcao_w;

end obter_funcao_objeto_schematic;
/