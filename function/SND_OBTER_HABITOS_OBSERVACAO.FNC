create or replace
function SND_Obter_Habitos_Observacao (cd_pessoa_fisica_p varchar2)
           return varchar2 is

/* ie_opcao_p -> eventuais futuras customizações */

ds_observacao_w  varchar2(255);
ds_retorno_w varchar2(2000) := '';
ds_string_retorno_w varchar2(2000) := '';

cursor C01 is
	select 	ds_observacao
	into	ds_retorno_w
	from   	paciente_habito_vicio a
	where  	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.dt_fim is null
	and    	upper(a.ie_classificacao) = upper('C') ;

begin
	open c01;
		loop
			fetch c01 into
			ds_observacao_w;
		    exit when c01%notfound;
				if length(ds_observacao_w) > 0 then
				ds_retorno_w := ds_retorno_w || ds_observacao_w || ', ';
				end if;
		end loop;
	close C01;

	if length(trim(substr((ds_retorno_w),1,length(ds_retorno_w)-2) || '.' )) > 1 then
		ds_string_retorno_w := trim(substr((ds_retorno_w),1,length(ds_retorno_w)-2) || '.' );
	else
		ds_string_retorno_w := null;
	end if;
return ds_string_retorno_w;

--tratamento em pontuaçao final da string e usar separador ' , ' apenas se houver informaçao.
end SND_Obter_Habitos_Observacao;
/
