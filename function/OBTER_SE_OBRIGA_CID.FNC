create or replace
function obter_se_obriga_cid(cd_material_p			number,
								ie_objetivo_uso_p		varchar2,
								cd_estabelecimento_p	number) return varchar2 is


ie_controle_medico_w	number(1,0);
ie_exige_cid_w			varchar2(1)	:= 'N';
cd_perfil_w				number(5);

begin

if	(cd_material_p is not null) then

	cd_perfil_w	:= obter_perfil_ativo;
	
	select	max(obter_controle_medic(cd_material,cd_estabelecimento_p,ie_controle_medico,null,null,null))
	into		ie_controle_medico_w
	from		material
	where	cd_material		=	cd_material_p;

	select	nvl(max('S'),'N')
	into		ie_exige_cid_w
	from		regra_antimicrobiano
	where	cd_regra			= ie_controle_medico_w
	and		nvl(cd_perfil,cd_perfil_w)	= cd_perfil_w
	and		cd_estabelecimento		= cd_estabelecimento_p
	and		((ie_exige_cid = 'S') or
			 ((ie_exige_cid = 'P') and (ie_objetivo_uso_p = 'F')) or
			 ((ie_exige_cid = 'C') and (ie_objetivo_uso_p = 'C')) or
			 ((ie_exige_cid = 'T') and (ie_objetivo_uso_p = 'T')) or
			 ((ie_exige_cid = 'D') and (ie_objetivo_uso_p = 'D')) or
			 ((ie_exige_cid = 'E') and (ie_objetivo_uso_p = 'E')))
	order 	by nvl(cd_perfil,0),
			nr_sequencia desc;

	return	ie_exige_cid_w;
end if;

end obter_se_obriga_cid;
/
