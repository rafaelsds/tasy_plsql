CREATE OR REPLACE
FUNCTION obter_titulo_pendente_atend	(nr_atendimento_p	number,
					nr_interno_conta_p	number)
					RETURN varchar2 IS

nr_titulo_retorno_w	varchar2(255) := null;
ie_tipo_convenio_w	number(02,0);
nr_titulo_w		number(10,0);
dt_vencimento_w		date;
ie_situacao_w		varchar2(01);

cursor	c01 is
	select	b.nr_titulo nr_titulo
	from	titulo_receber b
	where	b.nr_interno_conta = nr_interno_conta_p
	and	ie_tipo_convenio_w = 1
	and	b.ie_situacao 	= '1'
	and	b.dt_vencimento < sysdate;

BEGIN

select	obter_tipo_convenio(obter_convenio_atendimento(a.nr_atendimento)) ie_tipo_convenio
into	ie_tipo_convenio_w
from	atendimento_paciente a
where	a.nr_atendimento = nr_atendimento_p;

open	c01;
loop
fetch	c01 into nr_titulo_w;
exit when c01%notfound;
	begin
	
	nr_titulo_retorno_w	:= nr_titulo_retorno_w || nr_titulo_w || ',';

	end;	
end loop;
close c01;

RETURN	nr_titulo_retorno_w;

END	obter_titulo_pendente_atend;
/