create or replace
function Obter_Cor_Convenio(	cd_convenio_p	number)
 		    	return varchar2 is

ds_cor_w	varchar2(15);			
begin

if	(cd_convenio_p	is not null) then
	begin
	select	ds_cor
	into	ds_cor_w
	from	convenio
	where	cd_convenio	= cd_convenio_p;
	exception 
		when others then
		ds_cor_w:= null;
	end;
end if;


return	ds_cor_w;

end Obter_Cor_Convenio;
/