create or replace
function Ageweb_ie_atualiza_dados(cd_pessoa_fisica_p		varchar2)
 		    	return varchar2 is

dt_atual_inf_pac_w	date;	
qt_dias_atual_inf_pac_w	number(10,0);
ie_retorno_w		varchar2(1);		
			
begin

ie_retorno_w	:= 'N';

select	max(qt_dias_atual_inf_pac)
into	qt_dias_atual_inf_pac_w
from	parametro_agenda_web;

select	max(dt_atual_inf_pac)
into	dt_atual_inf_pac_w
from	ageweb_login
where	ds_login = cd_pessoa_fisica_p;

if	(qt_dias_atual_inf_pac_w is not null) and
	(dt_atual_inf_pac_w is not null) then
	
	if	((trunc(sysdate) - trunc(dt_atual_inf_pac_w)) >= qt_dias_atual_inf_pac_w) then
		ie_retorno_w	:= 'S';
	else
		ie_retorno_w	:= 'N';
	end if;
	
end if;

return	ie_retorno_w;

end Ageweb_ie_atualiza_dados;
/