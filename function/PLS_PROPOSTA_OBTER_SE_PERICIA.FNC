create or replace
function pls_proposta_obter_se_pericia
			(	nr_seq_pessoa_proposta_p		Number)
 		    		return Varchar2 is

ds_retorno_w			Varchar2(1);
nr_seq_proposta_w		pls_proposta_adesao.nr_sequencia%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
qt_registro_w			number(10);

begin

ds_retorno_w := 'N';

if	(nr_seq_pessoa_proposta_p is not null) then
	select	nr_seq_proposta,
		cd_beneficiario
	into	nr_seq_proposta_w,
		cd_pessoa_fisica_w
	from	pls_proposta_beneficiario
	where	nr_sequencia	= nr_seq_pessoa_proposta_p;	

	select	count(1)
	into	qt_registro_w
	from	pls_pericia_medica	b,
		pls_segurado		a
	where	a.nr_sequencia		= b.nr_seq_segurado
	and	a.nr_proposta_adesao 	= nr_seq_proposta_w
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
	
	if	(qt_registro_w > 0) then
		ds_retorno_w := 'S';
	end if;
end if;

return	ds_retorno_w;

end pls_proposta_obter_se_pericia;
/
