create or replace
function Sus_obter_Desc_Forma_Org
			(       cd_forma_organizacao_p       number)
                                return varchar2 is

ds_forma_organizacao_w          varchar2(100);

begin

select	nvl(max(ds_forma_organizacao),'')
into	ds_forma_organizacao_w
from	sus_forma_organizacao
where	cd_forma_organizacao	= cd_forma_organizacao_p;

return ds_forma_organizacao_w;

END Sus_obter_Desc_Forma_Org;
/