create or replace
function obter_valor_item_opme (nr_sequencia_p		NUMBER,
				    ie_opcao_p			VARCHAR)
 		    	return number is
			
cd_material_w		NUMBER(10);
cd_convenio_w		NUMBER(5);
cd_categoria_w		VARCHAR(10);		
vl_retorno_w		NUMBER(10,2);	
qt_material_w		NUMBER(15,3);
vl_unit_w		NUMBER(10,2);
ie_tipo_atendimento_w	number(3,0);
ie_achou_w		varchar2(1) := 'N';

/* U - Valor unitario
     T - Valor unitario * quantidade do material */
cursor	c01 is
	select	cd_categoria
	from	categoria_convenio a
	where	a.cd_convenio	= cd_convenio_w
	and 	Obter_Se_Categoria_Lib_Estab(wheb_usuario_pck.get_cd_estabelecimento, a.cd_convenio, a.cd_categoria) = 'S'
	and	a.ie_situacao	= 'A'
	and	exists
		(select	1
		from	convenio_amb x
		where	x.cd_convenio		= a.cd_convenio
		and	x.cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento
		and	x.cd_categoria		= a.cd_categoria
		union 	all
		select	1
		from	convenio_preco_mat x
		where	x.cd_convenio		= a.cd_convenio
		and	x.cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento
		and	x.cd_categoria		= a.cd_categoria
		union 	all
		select	1
		from	convenio_servico x
		where	x.cd_convenio		= a.cd_convenio
		and	x.cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento
		and	x.cd_categoria		= a.cd_categoria
		union   all
		select	1
		from	categoria_convenio x
		where	x.cd_convenio		= a.cd_convenio
		and	x.cd_categoria		= a.cd_categoria
		and	x.IE_PRECO_CUSTO	= 'S')
	order by cd_categoria desc;     
begin

if	(nr_sequencia_p is not null) then
	begin
	
	select	max(a.cd_material),
		max(b.cd_convenio),
		max(b.cd_categoria),
		max(a.qt_material),
		max(b.ie_tipo_atendimento)
	into	cd_material_w,
		cd_convenio_w,
		cd_categoria_w,
		qt_material_w,
		ie_tipo_atendimento_w
	from    agenda_pac_opme a,
		agenda_paciente b
	where   a.nr_seq_agenda = b.nr_sequencia
	and	a.nr_sequencia = nr_sequencia_p;

	if	(cd_categoria_w is null) then
		/*
		select	max(cd_categoria)
		into	cd_categoria_w
		from	categoria_convenio
		where	cd_convenio = cd_convenio_w
		and	nvl(ie_situacao,'A') = 'A';	
		*/
		ie_achou_w := 'N';
		open c01;
		loop
		fetch c01 into
			cd_categoria_w;
		exit when c01%notfound;
			begin
			if	(ie_achou_w = 'N') then
				if	(ie_opcao_p = 'U') then
					select 	obter_preco_material(wheb_usuario_pck.get_cd_estabelecimento,cd_convenio_w,cd_categoria_w,sysdate,cd_material_w,0,ie_tipo_atendimento_w,0,null,0,0) 
					into	vl_retorno_w
					from 	dual;
				end if;
			
				if	(ie_opcao_p	= 'T') then
					select 	obter_preco_material(wheb_usuario_pck.get_cd_estabelecimento,cd_convenio_w,cd_categoria_w,sysdate,cd_material_w,0,ie_tipo_atendimento_w,0,null,0,0) 
					into	vl_unit_w
					from 	dual;
					vl_retorno_w := (vl_unit_w * qt_material_w);
				end if;
				if	(nvl(vl_retorno_w,0) > 0) then
					ie_achou_w := 'S';
				end if;	
			end if;	
			end;
		end loop;
		close c01;	
	else
		if	(ie_opcao_p = 'U') then
			select 	obter_preco_material(wheb_usuario_pck.get_cd_estabelecimento,cd_convenio_w,cd_categoria_w,sysdate,cd_material_w,0,ie_tipo_atendimento_w,0,null,0,0) 
			into	vl_retorno_w
			from 	dual;
		end if;
	
		if	(ie_opcao_p	= 'T') then
			select 	obter_preco_material(wheb_usuario_pck.get_cd_estabelecimento,cd_convenio_w,cd_categoria_w,sysdate,cd_material_w,0,ie_tipo_atendimento_w,0,null,0,0) 
			into	vl_unit_w
			from 	dual;
			vl_retorno_w := (vl_unit_w * qt_material_w);
		end if;
	end if;	
	end;
end if;

return	vl_retorno_w;

end obter_valor_item_opme;
/