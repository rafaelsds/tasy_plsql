create or replace function obter_peso_ciclo(nr_seq_atendimento_p in number) return number is
  qt_peso_number_w	number(10,3);
  qt_peso_w paciente_atendimento.qt_peso%type;
  value_w nls_session_parameters.value%type;

begin

if (nr_seq_atendimento_p is not null) then
    select 	qt_peso
    into 	qt_peso_w
    from 	paciente_atendimento
    where 	nr_seq_atendimento = nr_seq_atendimento_p;

    select value
    into value_w
    from  nls_session_parameters
    where	parameter = 'NLS_NUMERIC_CHARACTERS';

    if (substr(value_w,1,1) = ',') then
      qt_peso_number_w := to_number(replace(qt_peso_w,'.',','));
    else
      qt_peso_number_w := to_number(replace(qt_peso_w,',','.'));
    end if;
end if;

return qt_peso_number_w;

end obter_peso_ciclo;
/ 
