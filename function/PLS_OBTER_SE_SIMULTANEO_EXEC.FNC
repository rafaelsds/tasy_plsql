create or replace
function pls_obter_se_simultaneo_exec
			(	nr_seq_simultaneo_p		pls_ocorrencia_simultaneo.nr_sequencia%type,
				nr_seq_execucao_p		pls_execucao_requisicao.nr_sequencia%type,
				cd_procedimento_p		procedimento.cd_procedimento%type,
				ie_origem_proced_p		procedimento.ie_origem_proced%type,
				nr_seq_material_p		pls_material.nr_sequencia%type,
				cd_estabelecimento_p		Number,
				nm_usuario_p			varchar2)
				return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 
Verifica se h� itens simult�neos na execu��o da requisi��o.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w			varchar2(1) 	:= 'N';
qt_regra_w			number(5)	:= 0;
ie_regra_w			varchar2(1)	:= 'N';
ie_item_sxclusivo_w		varchar2(1)	:= 'N';
qt_proc_estrutura_w		number(5);
qt_proc_estrutura_simul_w	number(5);

begin

begin
	select	nvl(ie_item_exclusivo,'N'),
		'S'
	into	ie_item_sxclusivo_w,
		ie_regra_w
	from	pls_ocorrencia_simultaneo
	where	ie_situacao	= 'A'
	and	nr_sequencia	= nr_seq_simultaneo_p;
exception
when others then
	ie_item_sxclusivo_w := null;
end;

if	(ie_regra_w = 'S') then
	if	(cd_procedimento_p is not null) then
		-- Essa verifica��o somente � realizada quando est� sendo a consist�ncia do procedimento
		select	count(1)
		into	qt_regra_w
		from	pls_ocorrencia_simul_item
		where	nr_seq_regra		= nr_seq_simultaneo_p
		and	cd_procedimento 	= cd_procedimento_p
		and	ie_origem_proced 	= ie_origem_proced_p
		and	ie_ocorrencia		= 'S'
		and	ie_situacao 		= 'A';
		
	elsif	(nr_seq_material_p is not null) then
		-- Essa verifica��o somente � realizada quando est� sendo a consist�ncia do material
		select	count(1)
		into	qt_regra_w
		from	pls_ocorrencia_simul_item
		where	nr_seq_regra	= nr_seq_simultaneo_p
		and	nr_seq_material	= nr_seq_material_p
		and	ie_ocorrencia	= 'S'
		and	ie_situacao 	= 'A';
	end if;

	if	(nvl(qt_regra_w,0) > 0) then
		select	count(1)
		into	qt_proc_estrutura_w
		from	pls_ocorrencia_simul_item a
		where	a.nr_seq_regra	= nr_seq_simultaneo_p
		and	a.ie_situacao	= 'A'
		and	exists(select	1
				from	pls_execucao_req_item b
				where	b.nr_seq_execucao 	= nr_seq_execucao_p
				and	b.cd_procedimento 	= a.cd_procedimento
				and	b.ie_origem_proced 	= a.ie_origem_proced
				and	b.nr_seq_material 	is null
				group 	by b.cd_procedimento,
					b.ie_origem_proced
				union 	all
				select	1
				from	pls_execucao_req_item c
				where	c.nr_seq_execucao 	= nr_seq_execucao_p
				and	c.nr_seq_material 	= a.nr_seq_material
				and	c.cd_procedimento	is null
				group 	by c.nr_seq_material);
				 
		if	(qt_proc_estrutura_w > 1) then		
			if	(ie_item_sxclusivo_w = 'S') then	
				/*Obter quantos itens da estrutura so simultaneo existem*/
				select	count(1)
				into	qt_proc_estrutura_simul_w
				from	pls_ocorrencia_simul_item a
				where	a.nr_seq_regra = nr_seq_simultaneo_p
				and	((cd_procedimento	> 0) or 
					(nr_seq_material	> 0))
				and	a.ie_situacao	= 'A';

				if	(qt_proc_estrutura_w = qt_proc_estrutura_simul_w) then
					ds_retorno_w := 'S';
				end if;
			else
				ds_retorno_w := 'S';
			end if;
		end if;
		
	end if;
end if;

return	ds_retorno_w;

end pls_obter_se_simultaneo_exec;
/
