Create or Replace
Function Obter_nome_Veiculo(	nr_sequencia_p		number)
				return varchar2 is
ds_retorno_w	varchar2(40);

begin

begin
select	nvl(max(substr(nm_veiculo,1,40)),wheb_mensagem_pck.get_texto(308633)) -- N�o informado
into	ds_retorno_w
from 	eme_veiculo
where 	nr_sequencia	= nr_sequencia_p;
exception
	when others then
	ds_retorno_w:= wheb_mensagem_pck.get_texto(308633); -- N�o informado
end;

return ds_retorno_w;

end;
/