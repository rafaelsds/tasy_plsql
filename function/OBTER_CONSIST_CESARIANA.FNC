CREATE OR REPLACE
FUNCTION Obter_Consist_Cesariana
			(nr_seq_protocolo_p	number,
			 ie_tipo_p		number)
			 return number is

/*
ie_tipo_p		
 1 - Total parto normal
 2 - Total cesariana
 3 - Total parto
 4 - % cesariana 
*/

qt_parto_normal_w	number(9,3)	:= 0;
qt_cesariana_w		number(9,3)	:= 0;
qt_total_parto_w	number(9,3)	:= 0;
pr_cesariana_w		number(7,4)	:= 0;
vl_retorno_w		number(9,4)	:= 0;

BEGIN

begin
Select	nvl(sum(a.qt_procedimento),0)
into 	qt_parto_normal_w
from 	procedimento_paciente	a,
	conta_paciente 		b
where  	a.nr_interno_conta  	= b.nr_interno_conta
and	b.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.cd_procedimento in(35001011,35021012,35025018)
and	a.cd_motivo_exc_conta is null;
exception
	when others then
		qt_parto_normal_w	:= 0;
end;

begin
Select	nvl(sum(a.qt_procedimento),0)
into 	qt_cesariana_w
from 	procedimento_paciente	a,
	conta_paciente 		b
where  	a.nr_interno_conta	= b.nr_interno_conta
and	b.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.cd_procedimento in(35009012,35022019,35026014,35084014)
and	a.cd_motivo_exc_conta is null;
exception
	when others then
		qt_cesariana_w	:= 0;
end;

qt_total_parto_w	:= (qt_parto_normal_w + qt_cesariana_w);

if	(qt_total_parto_w	> 0) then
	pr_cesariana_w	:= ((qt_cesariana_w * 100) / qt_total_parto_w);	
end if;

if	(ie_tipo_p	= 1) then
	vl_retorno_w	:= qt_parto_normal_w;
elsif	(ie_tipo_p	= 2) then
	vl_retorno_w	:= qt_cesariana_w;
elsif	(ie_tipo_p	= 3) then
	vl_retorno_w	:= qt_total_parto_w;
elsif	(ie_tipo_p	= 4) then
	vl_retorno_w	:= pr_cesariana_w;
end if;

Return	vl_retorno_w;

END Obter_Consist_Cesariana;
/
