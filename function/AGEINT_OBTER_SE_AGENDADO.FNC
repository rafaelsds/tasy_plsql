create or replace
function Ageint_Obter_Se_Agendado(
		cd_pessoa_fisica_p		varchar2,
		nr_seq_ageint_p		number,
		nr_seq_item_p		number,
		dt_agenda_p		date)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(1) := 'N';
cd_procedimento_w	number(15);
nr_seq_proc_interno_w	number(10);
ie_origem_proced_w	number(10);
cd_area_procedimento_w	number(10);
cd_especialidade_w	number(10);
cd_grupo_proc_w		number(10);
ie_perm_agendar_w	varchar2(1);
		
begin

select	nvl(max(cd_procedimento),0),
	nvl(max(nr_seq_proc_interno),0),
	nvl(max(ie_origem_proced),0)
into	cd_procedimento_w,
	nr_seq_proc_interno_w,
	ie_origem_proced_w
from	agenda_integrada_item
where	nr_sequencia	= nr_seq_item_p;

if	(nr_seq_proc_interno_w	> 0) then
	ie_perm_agendar_w	:= Ageint_Perm_Marcar_Agendado(cd_procedimento_w, nr_seq_proc_interno_w, ie_origem_proced_w);
	
	if	(ie_perm_agendar_w = 'N') then
		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	agenda_paciente a
		where	a.nr_seq_proc_interno	= nr_seq_proc_interno_w
		and	a.ie_origem_proced	= ie_origem_proced_w
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	a.ie_Status_agenda not in ('L','C')
		and	to_date(to_char(dt_agenda,'dd/mm/yyyy') ||' '|| to_char(hr_inicio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') > sysdate
		and	not exists (	select 1 
					from agenda_integrada_item b 
					where b.nr_seq_agenda_int = nr_seq_ageint_p 
					and b.nr_seq_agenda_exame = a.nr_sequencia);
					--and b.nr_sequencia = nr_seq_item_p);
	end if;
end if;
	
return	ds_retorno_w;

end Ageint_Obter_Se_Agendado;
/