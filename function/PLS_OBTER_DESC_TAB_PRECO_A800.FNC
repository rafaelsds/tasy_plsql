/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Descri��o da tabela de pre�o do A800
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
function pls_obter_desc_tab_preco_a800
		(	nr_seq_tabela_p		number)
 		    	return varchar2 is
			
ds_retorno_w		varchar2(255);

begin

select	max(ds_tabela)
into	ds_retorno_w
from	ptu_tabela_a800
where	nr_sequencia	= nr_seq_tabela_p;

return	ds_retorno_w;

end pls_obter_desc_tab_preco_a800;
/
