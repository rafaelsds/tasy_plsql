create or replace
function obter_desc_estrut_superior(nr_sequencia_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);
nr_seq_estrut_sup_w	number(10);
ds_estrutura_w		varchar2(80);
begin

select	ds_estrutura,
	nvl(nr_seq_estrut_sup,0)
into	ds_estrutura_w,
	nr_seq_estrut_sup_w	
from	mat_estrutura
where	nr_sequencia = nr_sequencia_p;

--ds_retorno_w := ds_estrutura_w;

/*NIVEL 1*/
if	(nr_seq_estrut_sup_w > 0) then
	
	select	a.ds_estrutura,
		nvl(a.nr_seq_estrut_sup,0)
	into	ds_estrutura_w,
		nr_seq_estrut_sup_w
	from	mat_estrutura a
	where	a.nr_sequencia = nr_seq_estrut_sup_w;	
	ds_retorno_w := substr(ds_estrutura_w,1,255);
	
	/*NIVEL 2*/
	if	(nr_seq_estrut_sup_w > 0) then
	
		select	a.ds_estrutura,
			nvl(a.nr_seq_estrut_sup,0)
		into	ds_estrutura_w,
			nr_seq_estrut_sup_w
		from	mat_estrutura a
		where	a.nr_sequencia = nr_seq_estrut_sup_w;
		ds_retorno_w := substr(ds_estrutura_w || ' - ' || ds_retorno_w,1,255);
	
		/*NIVEL 3*/
		if	(nr_seq_estrut_sup_w > 0) then
	
			select	a.ds_estrutura,
				nvl(a.nr_seq_estrut_sup,0)
			into	ds_estrutura_w,
				nr_seq_estrut_sup_w
			from	mat_estrutura a
			where	a.nr_sequencia = nr_seq_estrut_sup_w;			
			ds_retorno_w := substr(ds_estrutura_w || ' - ' || ds_retorno_w,1,255);
			
			/*NIVEL 4*/
			if	(nr_seq_estrut_sup_w > 0) then
	
				select	a.ds_estrutura,
					nvl(a.nr_seq_estrut_sup,0)
				into	ds_estrutura_w,
					nr_seq_estrut_sup_w
				from	mat_estrutura a
				where	a.nr_sequencia = nr_seq_estrut_sup_w;			
				ds_retorno_w := substr(ds_estrutura_w || ' - ' || ds_retorno_w,1,255);
			
				/*NIVEL 5*/
				if	(nr_seq_estrut_sup_w > 0) then
		
					select	a.ds_estrutura,
						nvl(a.nr_seq_estrut_sup,0)
					into	ds_estrutura_w,
						nr_seq_estrut_sup_w
					from	mat_estrutura a
					where	a.nr_sequencia = nr_seq_estrut_sup_w;			
					ds_retorno_w := substr(ds_estrutura_w || ' - ' || ds_retorno_w,1,255);
				
				end if;			
			end if;
		end if;	
	end if;	
end if;

return	ds_retorno_w;

end obter_desc_estrut_superior;
/
