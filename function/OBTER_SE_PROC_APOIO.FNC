create or replace
function obter_se_proc_apoio(nr_seq_paciente_p		number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1) := 'N';


begin

if (nr_seq_paciente_p is not null) then

	select 	nvl(max(ie_procedimento_apoio),'N')
	into 	ie_retorno_w
	from 	paciente_setor
	where	nr_seq_paciente = nr_seq_paciente_p;

end if;


return	ie_retorno_w;

end obter_se_proc_apoio;
/
