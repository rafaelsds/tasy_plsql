create or replace
function pls_obter_define_gera_analise
			(	cd_estabelecimento_p		number,
				ie_origem_p			varchar2)
				return varchar2 is
				
ds_retorno_w			varchar2(1);

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obt�m se � gerada an�lise...
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ x]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

if	(ie_origem_p = 'R') then
	begin
		select	nvl(ie_define_se_gera_analise,'A')
		into	ds_retorno_w
		from	pls_param_requisicao
		where	cd_estabelecimento = cd_estabelecimento_p;		
	exception
	when others then
		ds_retorno_w := 'A';
	end;
elsif	(ie_origem_p = 'A') then
	begin
		select	nvl(ie_define_se_gera_analise,'A')
		into	ds_retorno_w
		from	pls_param_autorizacao
		where	cd_estabelecimento = cd_estabelecimento_p;
	exception
	when others then
		ds_retorno_w	:= null;
	end;
	
	if	(ds_retorno_w	is null) then
		select	max(ie_define_se_gera_analise)
		into	ds_retorno_w
		from	pls_param_autorizacao;
		
		if	(ds_retorno_w	is null) then
			ds_retorno_w	:= 'A';
		end if;
	end if;
end if;

return	ds_retorno_w;

end pls_obter_define_gera_analise;
/