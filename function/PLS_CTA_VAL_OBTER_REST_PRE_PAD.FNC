create or replace
function pls_cta_val_obter_rest_pre_pad	(dados_filtro_prest_p	pls_tipos_cta_val_pck.dados_filtro_prest)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Obter o acesso a tabela pls_prestador que ser� utilizado  para verificar o select
	dos filtros de prestador.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Realizar tratamento para os campos IMP quando hourver necessidade

------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_filtro_prest_w	varchar2(4000);
begin

--Inicializar as vari�veis.
ds_filtro_prest_w	:= null;

if	(dados_filtro_prest_p.ie_tipo_prestador is not null) then
	-- Aqui verifca qual prestador da conta ser� utilizado, se atendimento, executor, solicitante ou de pagamento.
	if	(dados_filtro_prest_p.ie_tipo_prestador = 'A') then 
		ds_filtro_prest_w := ds_filtro_prest_w || pls_util_pck.enter_w || '			and	prest.nr_sequencia = proc.nr_seq_prestador_prot ';	
	elsif	(dados_filtro_prest_p.ie_tipo_prestador = 'E') then 
		ds_filtro_prest_w := ds_filtro_prest_w || pls_util_pck.enter_w || '			and	prest.nr_sequencia = proc.nr_seq_prestador_exec ';	
	elsif	(dados_filtro_prest_p.ie_tipo_prestador = 'S') then 
		ds_filtro_prest_w := ds_filtro_prest_w || pls_util_pck.enter_w || '			and	prest.nr_sequencia = proc.nr_seq_prestador_conta ';	
	end if;
end if;

return	ds_filtro_prest_w;

end pls_cta_val_obter_rest_pre_pad;
/