create or replace
function pls_obter_se_fluxo_adic(	ie_fluxo_adicional_p		Varchar2,
					qt_fluxo_grupo_p		Number,
					qt_fluxo_total_p		Number)
 		    	return Varchar2 is

ds_retorno_w	Varchar2(1)	:= 'N';
begin

if	(ie_fluxo_adicional_p	= 'S') or
	(qt_fluxo_total_p	= 0) or
	(qt_fluxo_grupo_p	> 0) then
	ds_retorno_w	:= 'N';
elsif	(qt_fluxo_grupo_p	= 0) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end pls_obter_se_fluxo_adic;
/