CREATE OR REPLACE
FUNCTION OBTER_DESC_OPERACAO_ESTOQUE	(cd_operacao_estoque_p	number) 
				return varchar2 is 

ds_retorno_w	varchar2(40) := '';

begin

if	(cd_operacao_estoque_p is not null) then
	select	ds_operacao
	into	ds_retorno_w
	from	operacao_estoque
	where	cd_operacao_estoque	= cd_operacao_estoque_p;
end if;

return ds_retorno_w;

end OBTER_DESC_OPERACAO_ESTOQUE;
/