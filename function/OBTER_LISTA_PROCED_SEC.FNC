create or replace
function obter_lista_proced_sec (	nr_seq_agenda_p	number) 
					return	varchar2 is

ds_retorno_w		varchar2(4000):= null;
ds_procedimento_w   varchar2(4000):= null;

cursor c01 is
	select	obter_desc_prescr_proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno) ds_procedimento
	from    pedido_age_cirurgia_proc
	where   nr_seq_pedido = nr_seq_agenda_p;
	
begin

open c01;
loop
	fetch 	c01 
	into 	ds_procedimento_w;
	exit when c01%notfound;
	begin
	   if(ds_retorno_w is null)then
		ds_retorno_w:= ds_procedimento_w;
	   else
		ds_retorno_w := ds_retorno_w||', '||ds_procedimento_w;	   
	   end if;
	   
	end;
end loop;
close c01;

return	ds_retorno_w;

end obter_lista_proced_sec;
/