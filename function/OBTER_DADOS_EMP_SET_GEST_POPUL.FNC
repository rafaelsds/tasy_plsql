create or replace 
function obter_dados_emp_set_gest_popul (	cd_pessoa_fisica_p			varchar2,
											cd_empresa_refer_p			number default null,
											nr_seq_empresa_ref_setor_p	number default null) 
			return number is

qt_retorno_w				number(10) := 0;

begin

	select 	count(1)
	into	qt_retorno_w
	from   	compl_pessoa_fisica a
	left join compl_pessoa_fisica_setor b
	on a.nr_sequencia = b.nr_seq_compl_pessoa_fisica and a.cd_pessoa_fisica = b.cd_pessoa_fisica
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and		nvl(a.cd_empresa_refer,0) = nvl(cd_empresa_refer_p,nvl(a.cd_empresa_refer,0))
	and		nvl(b.nr_seq_empresa_ref_setor,0) = nvl(nr_seq_empresa_ref_setor_p,nvl(b.nr_seq_empresa_ref_setor,0));

return qt_retorno_w;

end obter_dados_emp_set_gest_popul;
/