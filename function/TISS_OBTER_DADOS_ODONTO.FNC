create or replace function TISS_OBTER_DADOS_ODONTO(nr_seq_procedimento_p	number,
				ie_tiss_tipo_guia_p	varchar2,
				ie_opcao_p		varchar2,
                nr_seq_proc_p	number default null)
				return varchar2 is
/*
ie_opcao_p
D	- Numero do dente
F	- Face da boca
R       - Regiao da boca
*/
				
ds_retorno_w		varchar2(255) := null;
nr_dente_w		odont_procedimento.nr_dente%type;
ds_face_w		varchar2(10) := '';
cd_regiao_boca_w  	odont_procedimento.cd_regiao_boca%type;
nr_sequencia_w     odont_procedimento.nr_sequencia%type;
cd_procedimento_w  procedimento_paciente.cd_procedimento%type := null;
nr_atendimento_w   procedimento_paciente.nr_atendimento%type := null;
	
begin

if	(ie_tiss_tipo_guia_p = '11') then
	
    
	if(nr_seq_proc_p is null) then
        begin
            select	nr_sequencia into nr_sequencia_w
            from	odont_procedimento o
            where	(nr_seq_propaci	= nr_seq_procedimento_p
            or exists(select	1
                    from	ODONT_PROCEDIMENTO_PROC p
                    where	o.nr_sequencia = p.nr_seq_plano_proc
                    and p.nr_seq_propaci = nr_seq_procedimento_p))
            and	rownum		= 1;
            
            exception when others then
                begin
                
                
                select pp.cd_procedimento, nr_atendimento into cd_procedimento_w, nr_atendimento_w
                  from procedimento_paciente pp 
                 where pp.nr_sequencia = nr_seq_procedimento_p;
                
                select	o.nr_sequencia into nr_sequencia_w
                from	odont_procedimento o,odont_consulta c 
                where   c.nr_sequencia = o.nr_seq_consulta
                and 	c.nr_atendimento = nr_atendimento_w
                and	exists(select	1
                            from	ODONT_PROCEDIMENTO_PROC p
                            where	o.nr_sequencia = p.nr_seq_plano_proc
                            and p.cd_procedimento = cd_procedimento_w)
                and	rownum		= 1;
                
                
                
                exception when others then
                    nr_sequencia_w := null;
                end;
        end;
    else 
         select p.nr_seq_plano_proc, p.cd_procedimento 
           into nr_sequencia_w, cd_procedimento_w
           from ODONT_PROCEDIMENTO_PROC p
          where p.nr_sequencia = nr_seq_proc_p;
    end if;

	if(nr_sequencia_w is not null) then
		begin
		select	nvl(nr_dente,nr_seq_dente),
			nvl(cd_regiao_boca, 
			    CASE nr_seq_local 
			    WHEN 'I' THEN 'AI'
			    WHEN 'R' THEN 'AS'
			    WHEN 'U' THEN 'ASAI'
			    WHEN 'M' THEN 'HAID'
			    WHEN 'N' THEN 'HAIE'
			    WHEN 'H' THEN 'HASD'
			    WHEN 'J' THEN 'HASE'
			    WHEN 'V' THEN 'RMD'
			    WHEN 'L' THEN 'RME'
			    WHEN 'P' THEN 'RPD'
			    WHEN 'Q' THEN 'RPE'
			    ELSE nr_seq_local
			    END )
			into	nr_dente_w,
			    cd_regiao_boca_w
		from	odont_procedimento o
		where	o.nr_sequencia = nr_sequencia_w;
		exception
		when others then
			ds_retorno_w	:= null;
		end;
		
		if	(ie_opcao_p = 'D') then
			ds_retorno_w	:= nr_dente_w;
		elsif 	(ie_opcao_p = 'R')then
			ds_retorno_w := cd_regiao_boca_w;
		elsif	(ie_opcao_p = 'F') then
			FOR r IN(
			    select
			    IE_FACE_INCISAL,
			    IE_FACE_LINGUAL,
			    IE_FACE_MESIAL,
			    IE_FACE_OCLUSAL,
			    IE_FACE_PALATINA,
			    IE_FACE_VESTIBULAR,
			    IE_FACE_DISTAL
			    from ODONT_PROCEDIMENTO_PROC p 
			    where nr_seq_propaci = nr_seq_procedimento_p
			       or (p.nr_seq_plano_proc = nr_sequencia_w
				   and p.cd_procedimento = cd_procedimento_w)) LOOP
			    
			    IF (NVL(r.IE_FACE_INCISAL,'N') = 'S') THEN
			    ds_face_w := ds_face_w||'I';
			    END IF;
			    IF (NVL(r.IE_FACE_LINGUAL,'N') = 'S') THEN
			    ds_face_w := ds_face_w||'L';
			    END IF;
			    IF (NVL(r.IE_FACE_MESIAL,'N') = 'S') THEN
			    ds_face_w := ds_face_w||'M';
			    END IF;
			    IF (NVL(r.IE_FACE_OCLUSAL,'N') = 'S') THEN
			    ds_face_w := ds_face_w||'O';
			    END IF;
			    IF (NVL(r.IE_FACE_PALATINA,'N') = 'S') THEN
			    ds_face_w := ds_face_w||'P';
			    END IF;
			    IF (NVL(r.IE_FACE_VESTIBULAR,'N') = 'S') THEN
			    ds_face_w := ds_face_w||'V';
			    END IF;
			    IF (NVL(r.IE_FACE_DISTAL,'N') = 'S') THEN
			    ds_face_w := ds_face_w||'D';
			    END IF;
			END LOOP;
			ds_retorno_w	:= substr(ds_face_w,0,5);	
		end if;
	end if;

end if;

return	ds_retorno_w;

end TISS_OBTER_DADOS_ODONTO;
/