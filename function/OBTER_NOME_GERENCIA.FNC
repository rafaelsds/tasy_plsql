create or replace
function obter_nome_gerencia(	nr_sequencia_p		varchar2)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin
select	max(a.ds_gerencia)
into	ds_retorno_w
from	gerencia_wheb a
where	a.nr_sequencia = nr_sequencia_p;

return	ds_retorno_w;

end obter_nome_gerencia;
/