create or replace
function cpoe_obter_desc_hemodialise(nr_seq_protocolo_p		cpoe_dialise.ie_tipo_hemodialise%type,
									dt_lib_suspensao_p		cpoe_dialise.dt_lib_suspensao%type default null,
									dt_suspensao_p			cpoe_dialise.dt_suspensao%type default null)
 		    	return varchar2 is
				
ds_retorno_w		varchar2(255);

		function bold( ds_valor_p varchar2) return varchar2 is
		
		begin
		return '<strong>' ||ds_valor_p||'</strong>';
		
		end;
begin
if (nr_seq_protocolo_p is not null) then
	ds_retorno_w := bold(obter_desc_prot_npt(nr_seq_protocolo_p));
end if;

if (dt_lib_suspensao_p	is not null) and
	(dt_suspensao_p is not null) and
	(dt_suspensao_p <=sysdate) then
	ds_retorno_w	:= '<del> '||ds_retorno_w|| '</del>';
end if;

return ds_retorno_w;

end cpoe_obter_desc_hemodialise;
/