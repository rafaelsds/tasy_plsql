create or replace
function obter_se_atend_agenda_cirur(	nr_atendimento_p	number)
					return	varchar2 is


qt_agenda_w	number(3) := 0;

begin

select	count(*)
into	qt_agenda_w
from	agenda_paciente
where	nr_atendimento	=	nr_atendimento_p
and	ie_status_agenda not in ('C','E')
and 	dt_agenda between (sysdate - 2) and (sysdate + 1);

if	(qt_agenda_w > 0) then
	return 'S';
else
	return 'N';
end if;

end obter_se_atend_agenda_cirur;
/