create or replace
function obter_apelido_escala	(nr_seq_escala_p	number,
			cd_pessoa_fisica_p	varchar2,
			ie_opcao_p	varchar2)
			return varchar2 is

nr_seq_classif_w	number(10);
nr_seq_grupo_w		number(10);
ds_apelido_w		varchar2(80);
ds_retorno_w		varchar2(80);

begin

select 	max(nr_seq_grupo)
into	nr_seq_grupo_w
from	escala
where 	nr_sequencia = nr_seq_escala_p;

select  max(nr_seq_classif)
into	nr_seq_classif_w
from	escala_grupo
where	nr_sequencia = nr_seq_grupo_w;

select	max(ds_apelido)
into	ds_apelido_w
from	escala_profissional
where	cd_profissional = cd_pessoa_fisica_p
and	nr_seq_classif = nr_seq_classif_w;

if	(ie_opcao_p = 'A') then
	ds_retorno_w	:= ds_apelido_w;
end if;
return ds_retorno_w;

end;
/