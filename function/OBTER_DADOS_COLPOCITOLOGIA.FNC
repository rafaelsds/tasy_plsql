create or replace
function obter_dados_colpocitologia( 	nr_prescricao_p 	number)
						return varchar2 is 

ds_retorno_w		varchar2(2000);
ds_medicamento_utilizado_w	varchar(200);
dt_ultima_mestruacao_w	date;
dt_ultimo_parto_w	date;
ie_nuligesta_w		varchar2(1);
ie_gemelar_w		varchar2(1);
qt_gestacao_w		number(10);
qt_lamina_w		number(10);
qt_abortos_paciente_w	number(10);

Cursor C02 is
	select  to_char(d.dt_ultimo_parto,'dd/mm/yyyy'),
		decode(d.ie_gestacao_nuligesta,'','S','N'),
		decode(d.ie_gestacao_gemelar,'','S','N'),
		nvl(d.qt_gestacoes,0),
		nvl(d.qt_laminas,0),
		nvl(d.nr_abortos_paciente,0)
	from    prescr_medica_inf_adic d
	where   nr_prescricao = nr_prescricao_p
	order by 1;

Cursor C03 is
	select  substr(nvl(obter_desc_material(f.cd_material),ds_medicamento),1,100)
	from    paciente_medic_uso f,
		prescr_medica d
	where   d.nr_atendimento   = f.nr_atendimento
	and     d.nr_prescricao    = nr_prescricao_p
	order by 1;
	
begin

if 	(nr_prescricao_p is not null) then
	begin

	select  max(a.Dt_Mestruacao)
	into	dt_ultima_mestruacao_w
	from    prescr_medica a
	where   a.nr_prescricao = nr_prescricao_p;
	
	ds_retorno_w := 	'Data da �ltima mestrua��o: ' || to_char(dt_ultima_mestruacao_w,'dd/mm/yyyy') || ';';
	
	open C02;
	loop
	fetch C02 into	
		dt_ultimo_parto_w,
		ie_nuligesta_w,
		ie_gemelar_w,
		qt_gestacao_w,
		qt_lamina_w,
		qt_abortos_paciente_w;
	exit when C02%notfound;
		begin
		ds_retorno_w := ds_retorno_w || 
				'Data �ltimo parto.............: ' || to_char(dt_ultimo_parto_w,'dd/mm/yyyy')|| ';' ||
				'Nuligesta........................: ' || ie_nuligesta_w 			|| ';' || 
				'Gemelar..........................: ' || ie_gemelar_w 				|| ';' ||
				'Qtd gesta��o..................: ' || qt_gestacao_w 				|| ';' ||
				'Qtd l�minas.....................: ' || qt_lamina_w 				|| ';' ||
				'Qtd aborto......................: ' || qt_abortos_paciente_w			|| ';';
		end;
	end loop;
	close C02;
	
	open C03;
	loop
	fetch C03 into	
		ds_medicamento_utilizado_w;
	exit when C03%notfound;
		begin
		ds_retorno_w := ds_retorno_w ||
				'Medicamentos utilizados....: ' || ds_medicamento_utilizado_w || ';';
		end;
	end loop;
	close C03;
	end;
end if;

return  ds_retorno_w;

end obter_dados_colpocitologia;
/