create or replace
function obter_se_status_leito_saida(nr_atend_p		number)
 		return varchar2 is

ie_status_unidade_w	varchar2(3);
cd_setor_atual_w	varchar2(255);
cd_unidade_basica_w	varchar2(255);
cd_unidade_compl_w	varchar2(255);

begin

select	obter_unidade_atendimento(nr_atend_p, 'A', 'CS'),
	obter_unidade_atendimento(nr_atend_p, 'A', 'UB'),
	obter_unidade_atendimento(nr_atend_p, 'A', 'UC')
into	cd_setor_atual_w,	
	cd_unidade_basica_w,
	cd_unidade_compl_w
from	dual;

select	ie_status_unidade
into	ie_status_unidade_w
from	unidade_atendimento
where	cd_setor_atendimento = cd_setor_atual_w
and	cd_unidade_basica    = cd_unidade_basica_w
and	cd_unidade_compl     = cd_unidade_compl_w;



return	ie_status_unidade_w;

end obter_se_status_leito_saida;
/
