create or replace
function obter_valor_ultima_compra(
			cd_estabelecimento_p	Number,
			qt_dia_p			Number,
			cd_material_p		number,
			cd_local_estoque_p	Number,
			ie_tipo_p			Varchar2)
		return number is


vl_retorno_w			number(15,4);

BEGIN

vl_retorno_w	:=	nvl(obter_dados_ult_compra_data(cd_estabelecimento_p, cd_material_p, cd_local_estoque_p, sysdate, 0, 'VE'),0);

return vl_retorno_w;

END obter_valor_ultima_compra;
/
