create or replace function BUSCAR_QT_MEDIC_AUC(NR_SEQ_PACIENTE_P    number default null,
                                               NR_SEQ_ATENDIMENTO_P number default null)
  RETURN VARCHAR2 is
  
  v_count_atend    number;
  v_count_prot     number;
begin
  IF (NR_SEQ_PACIENTE_P IS NOT NULL) THEN
    BEGIN
      select count(*)
        into v_count_prot
        from paciente_protocolo_medic
       where paciente_protocolo_medic.nr_seq_paciente = nr_seq_paciente_p
         and paciente_protocolo_medic.nr_seq_diluicao is null
         and paciente_protocolo_medic.nr_seq_solucao is null
         and paciente_protocolo_medic.nr_seq_procedimento is null
         and paciente_protocolo_medic.nr_seq_medic_material is null
         and 'mgcar' =
             (select max(lower(cd_unidade_med_sec))
                from unidade_medida um
               where um.cd_unidade_medida =
                     paciente_protocolo_medic.cd_unidade_medida);
    EXCEPTION
      WHEN OTHERS THEN
        v_count_prot := 0;
    END;
    IF (NVL(v_count_prot,0) > 0) THEN
      RETURN 'S';
    END IF;
  END IF;
  
  IF (NR_SEQ_ATENDIMENTO_P IS NOT NULL) THEN
    BEGIN
      select count(*)
        into v_count_atend
        from paciente_atend_medic
       where paciente_atend_medic.dt_cancelamento is null
         and 'mgcar' =
             (select max(lower(cd_unidade_med_sec))
                from unidade_medida um
               where um.cd_unidade_medida = paciente_atend_medic.cd_unid_med_dose)
         and paciente_atend_medic.nr_seq_diluicao is null
         and paciente_atend_medic.nr_seq_solucao is Null
         and paciente_atend_medic.nr_seq_procedimento is null
         and paciente_atend_medic.nr_seq_atendimento = NR_SEQ_ATENDIMENTO_P;
    EXCEPTION
      WHEN OTHERS THEN
        v_count_atend := 0;
    END;
    IF (NVL(v_count_atend,0) > 0) THEN
      RETURN 'S';
    END IF;
  END IF;
  
  RETURN 'N';
end BUSCAR_QT_MEDIC_AUC;
/
