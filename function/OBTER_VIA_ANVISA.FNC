create or replace
function obter_via_anvisa(ie_via_administracao_p	varchar2)
 		    	return varchar2 is

ie_abreviacao_via_w	varchar2(15);

begin

select	max(ie_abreviacao_via)
into	ie_abreviacao_via_w
from 	anvisa_via_administracao a,
	via_aplicacao b
where  	b.nr_seq_via_anvisa = a.nr_sequencia 
and	upper(b.ie_via_aplicacao)  = upper(ie_via_administracao_p);


return	nvl(ie_abreviacao_via_w,ie_via_administracao_p);

end obter_via_anvisa;
/