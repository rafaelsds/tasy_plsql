create or replace
Function obter_valor_ds_resultado_exame 	(nr_seq_exame_p 	number,
					nr_seq_prescr_p	number,
					nr_prescricao_p	number,
					ie_opcao_p	varchar2)
						return varchar2 is
qt_resultado_w	number(15,4);
ds_resultado_w	varchar2(2000);

begin

select 	max(nvl(qt_resultado,pr_resultado)),
	max(nvl(ds_resultado,ds_observacao))
into	qt_resultado_w,
	ds_resultado_w
from	exame_lab_resultado a,
	exame_lab_result_item b
where	a.nr_seq_resultado = b.nr_seq_resultado
and	nr_seq_exame 	= nr_seq_exame_p
and	nr_seq_prescr	= nr_seq_prescr_p
and	nr_prescricao	= nr_prescricao_p;

if (ie_opcao_p = 'V') then

	return 	qt_resultado_w;

elsif (ie_opcao_p = 'D') then

	return 	ds_resultado_w;

end if;


end;
/