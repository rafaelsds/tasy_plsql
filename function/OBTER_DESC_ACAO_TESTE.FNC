create or replace
function obter_desc_acao_teste(	nr_seq_acao_p	number,
								ie_acao_p		varchar2)

return varchar2 is
			
ds_retorno_w		varchar2(255);
ds_campohandle		varchar2(15);
ds_campopaciente_w	varchar2(30);
ds_objeto_w			varchar2(255);
ds_atributo_w		varchar2(255);

nr_seq_objeto_w			caso_teste_execucao.nr_seq_objeto%type;
nm_tabela_w			caso_teste_execucao.nm_tabela%type;
nr_seq_visao_w			caso_teste_execucao.nr_seq_visao%type;
nm_atributo_w			caso_teste_execucao.nm_atributo%type;
ie_acao_handler_w		caso_teste_execucao.ie_acao_handler%type;
ds_valor_w			caso_teste_execucao.ds_valor%type;
nr_sequencia_w			caso_teste_execucao.nr_sequencia%type;
ie_campo_paciente_w		caso_teste_execucao.ie_campo_paciente%type;
nr_seq_obj_filtro_w		caso_teste_execucao.nr_seq_obj_filtro%type;


Cursor C01 is
	select	nr_seq_objeto,
		nm_tabela,
		nr_seq_visao,
		nm_atributo,
		ie_acao_handler,
		ds_valor,
		nr_sequencia,
		ie_campo_paciente,
		nr_seq_obj_filtro
	from	caso_teste_execucao
	where	nr_seq_acao = nr_seq_acao_p
	order by nr_sequencia;
begin

	ds_retorno_w := substr(obter_valor_dominio(7838, ie_acao_p),1,100);

	
	open c01;
	loop
	fetch c01 into
		nr_seq_objeto_w,
		nm_tabela_w,
		nr_seq_visao_w,
		nm_atributo_w,
		ie_acao_handler_w,
		ds_valor_w,
		nr_sequencia_w,
		ie_campo_paciente_w,
		nr_seq_obj_filtro_w;
	exit when c01%notfound;
		begin
			ds_campohandle := substr(obter_valor_dominio(7840, ie_acao_handler_w),1,100);
			ds_campopaciente_w := substr(obter_valor_dominio(7877, ie_campo_paciente_w),1,100);
			
			case (ie_acao_p)
			when '1' then --Preencher campo
				if (nm_atributo_w is not null) then
					ds_retorno_w := substr(ds_retorno_w || ' (' || nm_atributo_w|| ')',1,255);
				end if;
			when '2' then --Clicar bot�o
				if (ds_campohandle is not null) then
					ds_retorno_w := substr(ds_retorno_w || ' (' ||ds_campohandle|| ')',1,255);
				end if;
			when '3' then --Mouse de lado direito ------------erroooooooooowwwwwwwwwwww
				if (nr_seq_objeto_w is not null) then
					select substr(obter_desc_expressao(cd_exp_texto,DS_TEXTO),1,255)
					into ds_objeto_w
					from dic_objeto
					where nr_sequencia = nr_seq_objeto_w;					
					
					if (ds_objeto_w is not null) then
						ds_retorno_w := substr(ds_retorno_w || ' (' ||ds_objeto_w|| ')',1,255);
					end if;
				end if;
			when '4' then --Verificar campo
				if (nm_atributo_w is not null) then
					ds_retorno_w := substr(ds_retorno_w || ' (' || nm_atributo_w|| ')',1,255);
				end if;
			when '6' then --Focar pasta
				if (nr_seq_objeto_w is not null or ds_valor_w is not null) then
					if (ds_valor_w is null) then
						ds_retorno_w := substr(ds_retorno_w || ' (' ||obter_desc_obj_caso_teste(nr_seq_acao_p, nr_seq_objeto_w)|| ')',1,255);
					else
						ds_retorno_w := substr(ds_retorno_w || ' (' ||ds_valor_w|| ')',1,255);
					end if;
				end if;
			when '7' then --Ativar paciente
				if (ds_campopaciente_w is not null or ds_valor_w is not null) then
					ds_retorno_w := substr(ds_retorno_w || ' (' ||ds_campopaciente_w||': '||ds_valor_w|| ')',1,255);
				end if;
			when '8' then --Selecionar item da �rvore
				if (ds_valor_w is not null) then
					ds_retorno_w := substr(ds_retorno_w || ' (' ||ds_valor_w|| ')',1,255);
				end if;
			when '10' then --Delay
				if (ds_valor_w is not null) then
					ds_retorno_w := substr(ds_retorno_w || ' (' ||ds_valor_w|| obter_desc_expressao(614917)/*' segundos)'*/||')',1,255);
				end if;
			when '11' then --Mensagem esperada
				if (ds_campohandle is not null) then		
					ds_retorno_w := substr(ds_retorno_w || ' (' ||ds_campohandle|| ')',1,255);
				end if;
			when '16' then --Preencher campo filtro
				select NVL(MAX(nm_atributo),'')
				into ds_atributo_w
				from dic_objeto_filtro
				where nr_sequencia = nr_seq_obj_filtro_w;
		
				if (ds_atributo_w is not null) then
					ds_retorno_w := substr(ds_retorno_w || ' (' ||ds_atributo_w|| ')',1,255);
				end if;
			else
				null;
			end case;
		
		end;
		end loop;
	close c01;
	

return	ds_retorno_w;

end obter_desc_acao_teste;
/