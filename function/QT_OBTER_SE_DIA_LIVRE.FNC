create or replace
function Qt_Obter_Se_Dia_Livre (
							dt_tratamento_p			date,
							nm_usuario_p			varchar2,
							cd_estabelecimento_p	number)
 		    	return varchar2 is

ds_retorno_w	varchar2(2000);
nr_seq_local_w	number(10);
ie_feriado_w	varchar2(1);
ie_dia_Semana_w	number(5);
qt_bloqueio_w	number(10);
qt_turno_w		number(10);
ie_bloqueio_w	varchar2(1) := 'S';
		
Cursor C01 is
	select	nr_sequencia
	from	qt_local
	where	ie_situacao	= 'A'
	and		cd_estabelecimento	= cd_estabelecimento_p
	order by 1;
		
begin

ie_dia_semana_w	:= obter_cod_dia_semana(dt_tratamento_p);

select	decode(count(*),0,'N','S')
into	ie_feriado_w
from 	feriado a
where 	a.cd_estabelecimento 	= cd_estabelecimento_p
and	a.dt_feriado between trunc(dt_tratamento_p) and trunc(dt_tratamento_p) + 86399/86400;

open C01;
loop
fetch C01 into	
	nr_seq_local_w;
exit when C01%notfound;
	begin
	if	(ie_bloqueio_w = 'S') then
		select	count(*)
		into	qt_turno_w
		from	qt_local_turno a
		where	a.nr_seq_local	= nr_seq_local_w
		and	((a.dt_dia_semana = ie_dia_semana_w) or ((a.dt_dia_semana = 9) and (ie_dia_Semana_w not in (7,1))))	
		and	((ie_feriado_w	<> 'S' and nvl(a.ie_feriado,'N') = 'N') or (nvl(a.ie_feriado, 'N') = 'S' and ie_feriado_w = 'S'))
		and	((a.dt_vigencia_inicial is null) or (a.dt_vigencia_inicial <= sysdate))
		and	((a.dt_vigencia_final is null) or (a.dt_vigencia_final   >= sysdate))
		
		;
		
		if	(qt_turno_w	> 0) then
			select	count(*)
			into	qt_bloqueio_w
			from	qt_local_turno a
			where	a.nr_seq_local	= nr_seq_local_w
			and	((a.dt_dia_semana = ie_dia_semana_w) or ((a.dt_dia_semana = 9) and (ie_dia_Semana_w not in (7,1))))	
			and	((ie_feriado_w	<> 'S' and nvl(a.ie_feriado,'N') = 'N') or (nvl(a.ie_feriado, 'N') = 'S' and ie_feriado_w = 'S'))
			and	((a.dt_vigencia_inicial is null) or (a.dt_vigencia_inicial <= sysdate))
			and	((a.dt_vigencia_final is null) or (a.dt_vigencia_final   >= sysdate))			
			and exists 	(		select	1
							from	qt_bloqueio b
							where	b.nr_seq_local 	= a.nr_seq_local
							and		trunc(dt_tratamento_p) between b.dt_inicial  and b.dt_final
							and		((to_number(b.hr_inicio_bloqueio - trunc(b.hr_inicio_bloqueio)) <= to_number(hr_inicial - trunc(hr_inicial))) or b.hr_inicio_bloqueio is null)
							and		((to_number(b.hr_final_bloqueio  - trunc(b.hr_final_bloqueio))  >= to_number(hr_final - trunc(hr_final))) or b.hr_final_bloqueio is null)
							and		(((b.dt_dia_semana = ie_dia_semana_w) or ((b.dt_dia_semana = 9) and (ie_dia_Semana_w not in (7,1)))) or (dt_dia_semana is null))
					);
		end if;
		if	(qt_bloqueio_w	> 0) or (qt_turno_w = 0) then
			ds_retorno_w	:= to_char(dt_tratamento_p,'dd/mm/yyyy') || ', ';
		else
			ds_retorno_w	:= '';
			ie_bloqueio_w	:= 'N';
		end if;
	end if;
	end;
end loop;
close C01; 

return	substr(ds_retorno_w, 1, length(ds_retorno_w) - 2);

end Qt_Obter_Se_Dia_Livre;
/

