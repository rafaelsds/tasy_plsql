create or replace
function Obter_ult_evento_SNE(	nr_prescricao_p		number,
				nr_seq_material_p	number)
 		    	return number is

ie_alteracao_w		number(5);	
nr_sequencia_w		number(10);		
			
begin

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	prescr_solucao_evento
where	ie_tipo_solucao = 2
and	nr_prescricao = nr_prescricao_p
and	nr_seq_material = nr_seq_material_p
and	ie_alteracao <> 5
and	ie_evento_valido = 'S';

if	(nr_sequencia_w > 0) then

	select	ie_alteracao
	into	ie_alteracao_w
	from	prescr_solucao_evento
	where	nr_sequencia = nr_sequencia_w
	and	ie_alteracao not in (5);

end if;

return	ie_alteracao_w;

end Obter_ult_evento_SNE;
/
