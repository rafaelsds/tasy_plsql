create or replace
function pls_obter_tipo_env_cta_monit (	nr_seq_conta_p			pls_conta.nr_sequencia%type)
 		    	return varchar is

ie_tipo_envio_w		varchar2(1) := '3';
ie_origem_conta_w	pls_conta.ie_origem_conta%type;
ie_tipo_importacao_w	pls_protocolo_conta.ie_tipo_importacao%type;
nr_seq_protocolo_w	pls_conta.nr_seq_protocolo%type;

begin
if	(nr_seq_conta_p is not null) then
	select	nvl(ie_origem_conta,'D'),
		nr_seq_protocolo
	into	ie_origem_conta_w,
		nr_seq_protocolo_w
	from	pls_conta
	where	nr_sequencia = nr_seq_conta_p;

	if	(ie_origem_conta_w in ('C', 'P')) then
		ie_tipo_envio_w := '1';
	elsif	(ie_origem_conta_w in ('A', 'Z')) then
		ie_tipo_envio_w := '2';
	elsif	(ie_origem_conta_w in ('E', 'T')) then
		select	ie_tipo_importacao
		into	ie_tipo_importacao_w
		from	pls_protocolo_conta a
		where	a.nr_sequencia = nr_seq_protocolo_w;
		
		if	(ie_tipo_importacao_w = 'UP') then
			ie_tipo_envio_w := '2';
		else
			ie_tipo_envio_w := '3';
		end if;
	elsif	(ie_origem_conta_w = 'D') then
		ie_tipo_envio_w := '4';
	end if;
end if;

return	ie_tipo_envio_w;

end pls_obter_tipo_env_cta_monit;
/
