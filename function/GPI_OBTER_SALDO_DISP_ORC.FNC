create or replace function gpi_obter_saldo_disp_orc(    nr_seq_proj_gpi_p   gpi_projeto.nr_sequencia%type,
                                                        nr_seq_orcamento_p  gpi_orcamento.nr_sequencia%type,
                                                        nr_seq_orc_item_p   gpi_orc_item.nr_sequencia%type,
                                                        cd_material_p       gpi_orc_item.cd_material%type,
                                                        cd_centro_custo_p   gpi_orc_item.cd_centro_custo%type) 
return number is

qt_saldo_w  gpi_orc_item.qt_real%type;

begin

if  (nvl(nr_seq_orc_item_p,0) != 0) then
    begin
    select  (nvl(a.qt_item,0) - nvl(a.qt_real,0))
    into    qt_saldo_w
    from    gpi_orc_item a
    where   a.nr_sequencia  = nr_seq_orc_item_p;
    exception when others then
        qt_saldo_w  := 0;
    end;
else
    begin
    select  nvl(sum(a.qt_item),0) - nvl(sum(a.qt_real),0)
    into    qt_saldo_w
    from    gpi_orc_item a,
            gpi_orcamento b
    where   b.nr_sequencia      = a.nr_seq_orcamento
    and     b.nr_seq_projeto    = nvl(nr_seq_proj_gpi_p,b.nr_seq_projeto)
    and     b.nr_sequencia      = nvl(nr_seq_orcamento_p,b.nr_sequencia)
    and     nvl(a.cd_material,nvl(cd_material_p,0)) = nvl(cd_material_p,0)
    and     nvl(a.cd_centro_custo, nvl(cd_centro_custo_p,0)) = nvl(cd_centro_custo_p,0)
    and     b.dt_aprovacao is not null
    and     b.ie_situacao   = 'A';

    end;
end if;

return qt_saldo_w;

end GPI_OBTER_SALDO_DISP_ORC;
/
