CREATE OR REPLACE 
FUNCTION Obter_Desc_Sus_Ativ_Prof(	cd_atividade_profissional_p	number)
					return varchar2 is


ds_atividade_profissional_w	varchar2(80);


BEGIN

select	ds_atividade_profissional
into	ds_atividade_profissional_w
from 	sus_atividade_profissional 
where 	cd_atividade_profissional	= cd_atividade_profissional_p;

RETURN ds_atividade_profissional_w;

END Obter_Desc_Sus_Ativ_Prof;
/