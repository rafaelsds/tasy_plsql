create or replace
function Obter_relat_regra_copia(cd_convenio_p		number,
				 cd_setor_atendimento_p	number,
				 ie_tipo_atendimento_p	number,
				 cd_estabelecimento_p	number)
				return number is

cd_convenio_w			number(5);
cd_setor_atendimento_w	number(5);
ie_tipo_atendimento_w	number(3);
qt_copia_w			number(6);

cursor c01 is
	select qt_copia
	from relat_regra_copia
	where cd_estabelecimento = cd_estabelecimento_p
	  and nvl(cd_convenio, cd_convenio_w) = cd_convenio_w
	  and nvl(cd_setor_atendimento, cd_setor_atendimento_w) = cd_setor_atendimento_w
	  and nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)	= ie_tipo_atendimento_w
	order by nvl(ie_tipo_atendimento,0),
		 nvl(cd_setor_atendimento,0),
		 nvl(cd_convenio,0);
begin

cd_convenio_w 		:= nvl(cd_convenio_p,0);
cd_setor_atendimento_w	:= nvl(cd_setor_atendimento_p,0);
ie_tipo_atendimento_w	:= nvl(ie_tipo_atendimento_p,0);

open c01;
loop
fetch c01 into qt_copia_w;
	exit when c01%notfound;
end loop;
close c01;

return qt_copia_w;

end;
/