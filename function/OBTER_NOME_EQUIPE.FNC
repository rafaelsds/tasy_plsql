create or replace
function obter_nome_equipe(nr_sequencia_p		number)
 		    	return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retorna o nome da equipe atrav�z do c�digo passado
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
ds_retorno_w	varchar2(255);
begin
	if (nr_sequencia_p is not null) then 
		select	nm_equipe
		into	ds_retorno_w
		from	MPREV_EQUIPE 
		where	nr_sequencia = nr_sequencia_p;
	end if;
return	ds_retorno_w;

end obter_nome_equipe;
/