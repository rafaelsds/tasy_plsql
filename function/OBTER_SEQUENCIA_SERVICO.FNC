create or replace
function obter_sequencia_servico(	ie_tipo_presc_p		number,
									dt_servico_p		date,
									cd_setor_atend_p	number)
						return number is

nr_sequencia_w	number(10);			
			
begin

select 	nvl(max(a.nr_sequencia),0)
into	nr_sequencia_w
from	nut_servico a
where	not exists(	select	1
					from	regra_geracao_serv_setor b
					where	b.nr_seq_servico = a.nr_sequencia
					and		nvl(b.ie_gera_servico,'N') = 'N'
					and		b.cd_setor_atendimento = cd_setor_atend_p )
and		a.ie_tipo_prescricao_servico = ie_tipo_presc_p
and		nvl(a.ie_situacao,'A') = 'A';

return	nr_sequencia_w;

end obter_sequencia_servico;
/