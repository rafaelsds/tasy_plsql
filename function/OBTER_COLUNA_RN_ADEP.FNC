create or replace 
function obter_coluna_rn_adep (nr_atendimento_p Number)
							   return varchar2 is
						
ie_retorno_w	    char(1) := 'N';
dt_entrada_w        date;
begin

if (nvl(nr_atendimento_p,0) > 0) then

	select max(dt_entrada)
	into   dt_entrada_w
	from   atendimento_paciente
	where  nr_atendimento = nr_atendimento_p;

	select	nvl(max('S'),'N')
	into    ie_retorno_w
	from	atendimento_paciente
	where   rownum = 1
	and		nr_atendimento_mae = nr_atendimento_p
	and     dt_entrada > dt_entrada_w;
  
end if;        

return	ie_retorno_w;
end obter_coluna_rn_adep;
/