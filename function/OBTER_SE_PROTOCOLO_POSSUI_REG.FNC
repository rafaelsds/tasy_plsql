create or replace
function Obter_se_protocolo_possui_reg	(nr_seq_paciente_p	number)
					return varchar2 is

ie_possui_w		varchar2(1);
					
begin

select	nvl(max('S'),'N')
into	ie_possui_w
from	(select	1 qt
	from	paciente_protocolo_medic
	where	nr_seq_paciente	= nr_seq_paciente_p
	and	nr_seq_solucao		is null
	and	nr_seq_diluicao 	is null
	and	nr_seq_procedimento 	is null
	union all
	select	1 qt
	from	paciente_protocolo_soluc
	where	nr_seq_paciente = nr_seq_paciente_p
	union all
	select	1 qt
	from	paciente_protocolo_proc
	where	nr_seq_paciente = nr_seq_paciente_p
	union all
	select	1 qt
	from	paciente_protocolo_rec
	where	nr_seq_paciente = nr_seq_paciente_p);

return	ie_possui_w;

end Obter_se_protocolo_possui_reg;
/