create or replace
FUNCTION OBTER_CONCATENACAO_CRM_SIGLA(
        cd_pessoa_fisica_p NUMBER) 

RETURN  VARCHAR2 IS

nr_crm_w VARCHAR2(20);
uf_crm_w VARCHAR2(15);

BEGIN

SELECT 
  nr_crm,
  uf_crm
INTO 
  nr_crm_w,
  uf_crm_w
FROM 
  medico a
WHERE
  a.cd_pessoa_fisica = cd_pessoa_fisica_p;

RETURN concat(nr_crm_w, concat(' - ', uf_crm_w));

END OBTER_CONCATENACAO_CRM_SIGLA;
/