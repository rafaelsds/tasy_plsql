CREATE OR REPLACE FUNCTION obter_campos_match_attrib(NR_SEQ_DIC_OBJETO_P IN NUMBER, DS_COMANDO_SQL_P IN VARCHAR2) RETURN VARCHAR2 IS wrk_valida VARCHAR2(1);
    CURSOR c_cbase IS
      SELECT NM_CAMPO_BASE FROM dic_objeto
      WHERE nr_seq_obj_sup = nr_seq_dic_objeto_p;

    cursor_w			    INTEGER;
    nr_atributos_sql_w		PLS_INTEGER;
    atributos_sql_w		    Dbms_Sql.desc_tab;
    nm_atributo_sql_w		VARCHAR(80);
    ds_comando_sql_w      	VARCHAR(4000);


BEGIN

  IF ( nr_seq_dic_objeto_p IS NOT NULL AND ds_comando_sql_p IS NOT NULL ) AND ( InStr(Upper(ds_comando_sql_p), 'SELECT') > 0 ) THEN
      ds_comando_sql_w := REPLACE(REPLACE(Upper(ds_comando_sql_p),'@RESTRICAO', ' '),'@SQL_WHERE',' ');
        BEGIN
			cursor_w	:= dbms_sql.open_cursor;
			BEGIN
				dbms_sql.parse(cursor_w, ds_comando_sql_w, dbms_sql.native);
				dbms_sql.describe_columns(cursor_w, nr_atributos_sql_w, atributos_sql_w);
			EXCEPTION
			WHEN Others THEN
				RETURN 'S';
			END;
			
			FOR i IN 1 .. nr_atributos_sql_w LOOP
				wrk_valida := 'N';
				nm_atributo_sql_w	:= atributos_sql_w(i).col_name;
				FOR r01  IN c_cbase LOOP
					IF nm_atributo_sql_w = r01.NM_CAMPO_BASE THEN
						wrk_valida:= 'S';
					END IF;
				END LOOP;
				IF wrk_valida = 'N' THEN
					RETURN wrk_valida;
				END IF;
		    END LOOP;
        END;
  END IF;
  RETURN 'S';
END;
/