create or replace
function consistir_se_mat_estrut_vig(	nr_seq_estrutura_p	number,
					cd_material_p		number,
					dt_referencia_p		date) 
					return varchar2 is 

ie_retorno_w	varchar2(1);
qt_reg_w	number(10,0);
	
begin

select 	count(*)
into	qt_reg_w
from	mat_estrutura a,
	mat_estrutura_cadastro b,
	mat_estrutura_cad_vig c
where	a.nr_sequencia	= b.nr_seq_estrutura
and	a.nr_sequencia	= nr_seq_estrutura_p
and	b.cd_material	= cd_material_p
and 	c.nr_seq_mat_estrutura = b.nr_sequencia;


if	(qt_reg_w > 0) then

	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	mat_estrutura a,
		mat_estrutura_cadastro b,
		mat_estrutura_cad_vig c
	where	a.nr_sequencia	= b.nr_seq_estrutura
	and	a.nr_sequencia	= nr_seq_estrutura_p
	and	b.cd_material	= cd_material_p
	and 	c.nr_seq_mat_estrutura = b.nr_sequencia
	and 	dt_referencia_p between c.dt_inicio_vigencia and nvl(dt_final_vigencia, dt_referencia_p);
			
else

	select	nvl(max('S'),'N')
	into	ie_retorno_w
	from	mat_estrutura a,
		mat_estrutura_cadastro b
	where	a.nr_sequencia	= b.nr_seq_estrutura
	and	a.nr_sequencia	= nr_seq_estrutura_p
	and	b.cd_material	= cd_material_p;
		
end if;

return	ie_retorno_w;

end consistir_se_mat_estrut_vig;
/