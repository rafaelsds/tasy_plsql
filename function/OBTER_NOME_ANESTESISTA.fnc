create or replace function OBTER_NOME_ANESTESISTA(P_NR_CIRURGIA number)
  return varchar2 is
  RET VARCHAR2(255) := NULL;

BEGIN

    IF P_NR_CIRURGIA IS NOT NULL THEN
      SELECT DECODE(INSTR(T.NM, ' ', -1),
                    0,
                    T.NM,
                    SUBSTR(T.NM, INSTR(T.NM, ' ', -1) + 1) || ', ' ||
                    SUBSTR(T.NM, 1, INSTR(T.NM, ' ', -1) - 1))
        INTO RET
        FROM (SELECT OBTER_ANESTESISTA_CIRURGIA(P_NR_CIRURGIA,'N') NM FROM DUAL) T;
    END IF;

    RETURN RET;

END OBTER_NOME_ANESTESISTA;
/
