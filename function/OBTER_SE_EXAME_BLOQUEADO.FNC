CREATE OR REPLACE
FUNCTION Obter_se_exame_bloqueado( nr_sequencia_p  NUMBER, nr_prescricao_p  NUMBER )
     RETURN VARCHAR2 IS
/**
 Retorna se o exame est� bloqueado ou n�o
*/
ie_restringe_result_w 	 Varchar2(1) := 'N';
ie_restricao_w		 Varchar2(1) := 'N';
BEGIN
	Select  nvl(max(g.ie_restringe_resultado),'N'), 
		max(decode(f.ie_restricao, 'S', 'S', 'I', 'S', 'N'))
	into	ie_restringe_result_w,
		ie_restricao_w
	from  	prescr_procedimento b,
        	prescr_medica c,
        	exame_lab_resultado e,
        	exame_lab_result_item g,
        	exame_laboratorio f
  	where  	b.nr_prescricao         = c.nr_prescricao
  	and    	b.nr_prescricao         = e.nr_prescricao
        and    	e.nr_seq_resultado      = g.nr_seq_resultado
        and    	g.nr_seq_exame          = f.nr_seq_exame
        and    	b.nr_seq_exame          = f.nr_seq_exame
  	and     g.nr_seq_prescr         = b.nr_sequencia
        and   	b.nr_prescricao         = nr_prescricao_p
        and    	b.nr_sequencia          = nr_sequencia_p;

if (ie_restricao_w <> 'N') then
	ie_restringe_result_w := 'S';
end if;

RETURN ie_restringe_result_w;
END Obter_se_exame_bloqueado;
/
