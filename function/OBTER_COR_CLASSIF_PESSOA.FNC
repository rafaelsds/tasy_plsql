create or replace
function obter_cor_classif_pessoa(nr_seq_classif_p	number)
 		    	return varchar2 is

ds_retorno_w		varchar2(15);
	
begin

	select	max(ds_cor)
	into	ds_retorno_w
	from    classif_pessoa
	where nr_sequencia = nr_seq_classif_p;

return	ds_retorno_w;

end obter_cor_classif_pessoa;
/
