create or replace
function Lab_Obter_Subformato ( nr_seq_formato_p number,
    nr_seq_exame_p  number,
    qt_resultado_p  number,
    pr_resultado_p  number,
    nr_prescricao_p  number,
    ds_resultado_p	varchar2)
    return number is

nr_seq_formato_w number(10);
ds_regra_w  varchar2(255);
vl_resultado_w number(15,4);
qt_idade_w  number(7,2);
ie_tipo_atend_w	number(10);
nr_seq_cor_pele_w	number(10);
ie_sexo_w	varchar2(1);

cursor c01 is
 select nr_seq_formato,
   replace(replace(ds_regra, '@'||nr_seq_exame_p, 
	decode(qt_resultado_p,null,chr(39)||ds_resultado_p||chr(39),replace(qt_resultado_p,',','.'))),
	'%'||nr_seq_exame_p, replace(pr_resultado_p,',','.'))
 from exame_lab_format
 where nr_seq_superior = nr_seq_formato_p
   and ds_regra like '%@' || nr_seq_exame_p || '%'
 union
 select nr_seq_formato,
   replace(replace(ds_regra, '@'||nr_seq_exame_p, 
	decode(qt_resultado_p,null,chr(39)||ds_resultado_p||chr(39),replace(qt_resultado_p,',','.'))),
	'%'||nr_seq_exame_p, replace(pr_resultado_p,',','.'))
 from exame_lab_format
 where nr_seq_superior = nr_seq_formato_p
   and ds_regra like '%%' || nr_seq_exame_p || '%';
begin

select max(round((a.dt_prescricao - nvl(b.dt_nascimento,a.dt_prescricao)) / 365.25)),
max(nr_seq_cor_pele),
max(ie_sexo)
into qt_idade_w,
nr_seq_cor_pele_w,
ie_sexo_w
from pessoa_fisica b,
 prescr_medica a
where a.cd_pessoa_fisica = b.cd_pessoa_fisica
  and a.nr_prescricao = nr_prescricao_p;

select	OBTER_TIPO_ATEND_PRESCR(nr_prescricao_p)
into	ie_tipo_atend_w
from	dual;

open c01;
loop
fetch c01 into nr_seq_formato_w,
  ds_regra_w;
 exit when c01%notfound;

 ds_regra_w := Substr(replace_macro(upper(ds_regra_w), '@IDADE', qt_idade_w),1,4000);
 ds_regra_w := Substr(replace_macro(upper(ds_regra_w), '@TIPO_ATEND', ie_tipo_atend_w),1,4000);
 ds_regra_w := Substr(replace_macro(upper(ds_regra_w), '@COR_PELE', nr_seq_cor_pele_w),1,4000);
 ds_regra_w := Substr(replace_macro(upper(ds_regra_w), '@SEXO', ie_sexo_w),1,4000);
 
 obter_valor_dinamico('select 1 from dual where ' || ds_regra_w, vl_resultado_w);
 if (vl_resultado_w is null) or
     (vl_resultado_w = 0) then
  obter_valor_dinamico('select 1 from dual where ' || replace(ds_regra_w, ',', '.'), vl_resultado_w);
 end if;

 if (nvl(vl_resultado_w,0) = 0) then
  nr_seq_formato_w := '';
 else
  exit;
 end if;
end loop;
close c01;

return nr_seq_formato_w;

end;
/