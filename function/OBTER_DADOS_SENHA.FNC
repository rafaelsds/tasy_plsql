create or replace
function obter_dados_senha(
			nr_seq_senha_p		number,
			ie_opcao_p		varchar2)
 		    	return varchar2 is

/*
DG - Data de gera��o da senha
CD - C�digo da senha gerada
LC - Letra + C�digo da senha gerada
AG - Nome do paciente na agenda
AH - Hora da agenda
AT - Nome do paciente na agenda / Triagem do Pronto Atendimento
HT - Hora da agenda / Triagem do Pronto Atendimento
NA - nr_sequencia da agenda
MR - M�dico Respons�vel
TE - Tempo espera
SF - Sequ�ncia da fila
QT - Quantidade de chamadas da senha
IN - Se a senha foi inutilizada
DS - Descri��o fila
*/			

ds_retorno_w		varchar2(255);

cd_senha_gerada_w	varchar2(20);
dt_geracao_senha_w	varchar2(100);
cd_pessoa_fisica_w		paciente_senha_fila.cd_pessoa_fisica%type;
cd_pessoa_agenda_triagem_w	paciente_senha_fila.cd_pessoa_fisica%type;
hr_inicio_w		varchar2(100);
nr_seq_agenda_w		number(10);
cd_medico_resp_w	varchar2(10);
ie_regra_tempo_espera_w	varchar2(1);
nr_seq_fila_senha_w	number(10);
qt_chamadas_w		number(10);
ie_senha_finalizada_w	varchar2(10);
ds_fila_senha_w		varchar2(255);
ie_nome_oculto_w		varchar2(1);

begin

obter_param_Usuario(10021, 130, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_nome_oculto_w);

if	(nr_seq_senha_p is not null) then
	
	-- Tratadas separadamente algumas op��es para resolver problema de performance na pasta Filas do Gerenciamento de Senhas
	
	/* AH - Hora da agenda */
	If	(ie_opcao_p = 'AH') then 
		select	max(to_char(hr_inicio,'hh24:mi'))
		into	hr_inicio_w
		from	agenda_paciente
		where	nr_seq_pac_senha_fila = nr_seq_senha_p;
		
		if	(hr_inicio_w is null) then
			select	max(to_char(dt_agenda,'hh24:mi'))
			into	hr_inicio_w
			from	agenda_consulta
			where	nr_seq_pac_senha_fila = nr_seq_senha_p;			
		end if;
	end if;

	/* AG - Nome do paciente na agenda */
	/* AT - Nome do paciente na agenda / Triagem do Pronto Atendimento */
	If	(ie_opcao_p = 'AT') and
		(ie_opcao_p = 'AG') then
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	paciente_senha_fila
		where	nr_sequencia = nr_seq_senha_p;
	end if;
	
	if	(ie_opcao_p <> 'AG') and
		(ie_opcao_p <> 'AT') and
		(ie_opcao_p <> 'MR') and
		(ie_opcao_p <> 'TE') and
		(ie_opcao_p <> 'LC') and
		(ie_opcao_p <> 'SF') and
		(ie_opcao_p <> 'QT') and
		(ie_opcao_p <> 'IN') and
		(ie_opcao_p <> 'AH') then
		
		select	to_char(dt_geracao_senha,'dd/mm/yyyy hh24:mi:ss'),
			cd_senha_gerada
		into	dt_geracao_senha_w,
			cd_senha_gerada_w
		from	paciente_senha_fila
		where	nr_sequencia = nr_seq_senha_p;		
	end if;
	
	if	((ie_opcao_p = 'HT') or 
		 (ie_opcao_p = 'NA') or
		 (ie_opcao_p = 'AT') or
		 (ie_opcao_p = 'AG')) then
	
		select	max(to_char(hr_inicio,'hh24:mi')),
			max(nr_sequencia),
			max(cd_pessoa_fisica)
		into	hr_inicio_w,
			nr_seq_agenda_w,
			cd_pessoa_agenda_triagem_w
		from	agenda_paciente
		where	nr_seq_pac_senha_fila = nr_seq_senha_p;
		
		if	(nr_seq_agenda_w is null) then			
			select	max(to_char(dt_agenda,'hh24:mi')),
				max(nr_sequencia),
				max(cd_pessoa_fisica)
			into	hr_inicio_w,
				nr_seq_agenda_w,
				cd_pessoa_agenda_triagem_w
			from	agenda_consulta
			where	NR_SEQ_PAC_SENHA_FILA = nr_seq_senha_p;			
		end if;
		
		if	((ie_opcao_p = 'HT') or (ie_opcao_p = 'AT')) and
			(nr_seq_agenda_w is null) then
			select	max(to_char(dt_inicio_triagem,'hh24:mi')),
				max(cd_pessoa_fisica)
			into	hr_inicio_w,
				cd_pessoa_agenda_triagem_w
			from	triagem_pronto_atend
			where	nr_seq_fila_senha = nr_seq_senha_p;		
		end if;	
	end if;
	
	if	(ie_opcao_p = 'MR') then
		select	max(cd_medico_resp)
		into	cd_medico_resp_w
		from	atendimento_paciente
		where	NR_SEQ_PAC_SENHA_FILA = nr_seq_senha_p;
	end if;
	
	if	(ie_opcao_p = 'TE') then
		Obter_param_Usuario(10021, 77, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_regra_tempo_espera_w);
		
		if	(ie_regra_tempo_espera_w = 'F') then		
			select	to_char(nvl(dt_entrada_fila, dt_geracao_senha),'dd/mm/yyyy hh24:mi:ss')
			into	dt_geracao_senha_w
			from	paciente_senha_fila
			where	nr_sequencia = nr_seq_senha_p;
		else
			select	to_char(dt_geracao_senha,'dd/mm/yyyy hh24:mi:ss')
			into	dt_geracao_senha_w
			from	paciente_senha_fila
			where	nr_sequencia = nr_seq_senha_p;
		end if;
	end if;
	
	if	(ie_opcao_p = 'LC') then
		select	substr(obter_letra_verifacao_senha(nvl(nr_seq_fila_senha_origem, nr_seq_fila_senha)),1,10) || cd_senha_gerada
		into	cd_senha_gerada_w
		from	paciente_senha_fila
		where	nr_sequencia = nr_seq_senha_p; 
	end if;
	
	if	(ie_opcao_p = 'SF') then
		select	nr_seq_fila_senha
		into	nr_seq_fila_senha_w
		from	paciente_senha_fila
		where	nr_sequencia = nr_seq_senha_p; 
	end if;
	
	if	(ie_opcao_p = 'DS') then
		
		select  max(b.ds_fila)
		into	ds_fila_senha_w
		from 	paciente_senha_fila a,
				fila_espera_senha b
		where	a.nr_sequencia = nr_seq_senha_p
		and		nvl(a.nr_seq_fila_senha,a.nr_seq_fila_senha_origem)  = b.nr_sequencia;		
		
	end if;
	
	if	(ie_opcao_p = 'QT') then
		select	qt_chamadas
		into	qt_chamadas_w
		from	paciente_senha_fila
		where	nr_sequencia = nr_seq_senha_p; 
	end if;
	
	if	(ie_opcao_p = 'IN') then
		select	decode(max(dt_inutilizacao), null, 'N', 'S')
		into	ie_senha_finalizada_w
		from	paciente_senha_fila
		where	nr_sequencia = nr_seq_senha_p; 
	end if;	
	
end if;

if	(ie_opcao_p = 'DG') then
	ds_retorno_w	:= dt_geracao_senha_w;	
elsif	(ie_opcao_p = 'CD') then
	ds_retorno_w	:= cd_senha_gerada_w;
elsif	(ie_opcao_p = 'AG') then
	if  	(ie_nome_oculto_w = 'S')  then
		ds_retorno_w 	:= substr(obter_nome_pf_oculta(nvl(cd_pessoa_fisica_w, cd_pessoa_agenda_triagem_w), obter_perfil_ativo, obter_usuario_ativo, obter_nome_pessoa_fisica(cd_pessoa_fisica_w,null)),1,200);				
	else
		ds_retorno_w 	:= substr(obter_nome_pessoa_fisica(nvl(cd_pessoa_fisica_w, cd_pessoa_agenda_triagem_w),null),1,200);
	end if;
elsif	(ie_opcao_p = 'AH') then
	ds_retorno_w	:= hr_inicio_w;
elsif	(ie_opcao_p = 'AT') then
	if  	(ie_nome_oculto_w = 'S')  then
		ds_retorno_w 	:= substr(obter_nome_pf_oculta(nvl(cd_pessoa_fisica_w, cd_pessoa_agenda_triagem_w), obter_perfil_ativo, obter_usuario_ativo, obter_nome_pessoa_fisica(cd_pessoa_fisica_w,null)),1,200);				
	else
		ds_retorno_w 	:= substr(obter_nome_pessoa_fisica(nvl(cd_pessoa_fisica_w, cd_pessoa_agenda_triagem_w),null),1,200);
	end if;
elsif	(ie_opcao_p = 'HT') then
	ds_retorno_w	:= hr_inicio_w;
elsif	(ie_opcao_p = 'NA') then
	ds_retorno_w	:= nr_seq_agenda_w; 
elsif	(ie_opcao_p = 'MR') then
	if  	(ie_nome_oculto_w = 'S')  then
		ds_retorno_w 	:= substr(obter_nome_pf_oculta(cd_pessoa_fisica_w, obter_perfil_ativo, obter_usuario_ativo, obter_nome_pessoa_fisica(cd_pessoa_fisica_w,null)),1,200);				
	else
		ds_retorno_w 	:= substr(obter_nome_pessoa_fisica(cd_medico_resp_w,null),1,200);
	end if;	
elsif	(ie_opcao_p = 'TE') then	
	ds_retorno_w 	:= dt_geracao_senha_w;
elsif	(ie_opcao_p = 'LC') then
	ds_retorno_w	:= cd_senha_gerada_w;
elsif	(ie_opcao_p = 'SF') then	
	ds_retorno_w 	:= nr_seq_fila_senha_w;
elsif	(ie_opcao_p = 'QT') then
	ds_retorno_w	:= qt_chamadas_w;
elsif	(ie_opcao_p = 'IN') then
	ds_retorno_w	:= ie_senha_finalizada_w;
elsif	(ie_opcao_p = 'DS') then
	ds_retorno_w	:= substr(ds_fila_senha_w,1,200);
end if;

return	ds_retorno_w;

end obter_dados_senha;
/
