create or replace
function OBTER_MACROS_REGRA_AUTOR 
return varchar2 is

ds_retorno_w	varchar2(4000);

begin

ds_retorno_w	:= 	'@atend - '||wheb_mensagem_pck.get_texto(315174)||chr(10)||
			'@seq_autor - '||wheb_mensagem_pck.get_texto(315175)||chr(10)||
			'@paciente - '||wheb_mensagem_pck.get_texto(315176)||chr(10)||
			'@tipo_autor - '||wheb_mensagem_pck.get_texto(315177)||chr(10)||
			'@proc_autor - '||wheb_mensagem_pck.get_texto(315178)||chr(10)||
			'@campos - '||wheb_mensagem_pck.get_texto(315179);
			
return 	ds_retorno_w;

end;
/
