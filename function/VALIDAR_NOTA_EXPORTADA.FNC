Create or Replace
Function validar_nota_exportada(nr_sequencia_p	number) 
return varchar2 is

retorno_w varchar2(2);
qtd_exp_w number(5);

begin

	select count(1) 
	into qtd_exp_w
	from nota_fiscal  n,
		 nota_fiscal_exportada e
	where n.nr_seq_exportada = e.nr_sequencia
	and   n.nr_seq_exportada is not null
	and   n.nr_sequencia = nr_sequencia_p;
	
	if(qtd_exp_w > 0) then
		retorno_w := 'S';
	else
		retorno_w := 'N';
	end if;

return retorno_w;

end validar_nota_exportada;
/