create or replace
function obter_desc_segmento_compras(nr_Sequencia_p		number)
 		    	return varchar2 is

ds_segmento_w		varchar2(255);			
			
begin

select	ds_segmento
into	ds_segmento_w
from	segmento_compras
where	nr_sequencia = nr_sequencia_p;

return	ds_segmento_w;

end obter_desc_segmento_compras;
/