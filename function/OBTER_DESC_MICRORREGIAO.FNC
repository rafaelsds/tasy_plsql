create or replace
function Obter_desc_microrregiao(nr_seq_microrregiao_p  number)
		return varchar2 is
		
ds_micro_reg_w varchar(60);
	
begin
	SELECT max(DS_MICROREGIAO)
	INTO ds_micro_reg_w
	FROM microregiao
	WHERE nr_sequencia = nr_seq_microrregiao_p;
		
return ds_micro_reg_w;

end Obter_desc_microrregiao;
/