create or replace
function obter_classif_proc_interno_2(nr_seq_proc_interno_p in number)
			return varchar2 is
	
ds_retorno_w	varchar2(255);	
begin

begin
select	substr(a.ds_classificacao,1,255)
into	ds_retorno_w
from	proc_interno_classif a,
	proc_interno b
where	a.nr_sequencia 	= b.nr_seq_classif
and	b.nr_sequencia	= nr_seq_proc_interno_p
and	rownum		= 1;
exception
when others then
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(322741);
end;

return ds_retorno_w;

end obter_classif_proc_interno_2;
/