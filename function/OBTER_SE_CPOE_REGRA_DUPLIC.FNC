create or replace
function obter_se_cpoe_regra_duplic(ie_tipo_item_p			varchar2,
									cd_perfil_p				number,
									cd_setor_atendimento_p	number,
									cd_material_p	 		number default null) 
								return varchar2 is
				
ds_retorno_w			varchar2(1);
ie_existe_regra_w		varchar2(1);
nr_seq_regra_w			number(10);
cd_grupo_material_w		grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w	SubGrupo_Material.cd_subgrupo_material%type;
cd_classe_material_w	Classe_material.cd_classe_material%type;
ie_possui_regra_w		varchar2(1) := 'N';
nr_seq_consist_w		cpoe_regra_consistencia.nr_sequencia%type;

begin

select	max(b.nr_sequencia),
		max(c.nr_sequencia)
into	nr_seq_regra_w,
		nr_seq_consist_w
from	cpoe_regra_consistencia c,
		cpoe_regra_duplicidade b
where	c.nr_sequencia = b.nr_seq_regra
and		nvl(c.cd_perfil, cd_perfil_p) = cd_perfil_p
and		nvl(c.cd_setor_atendimento, cd_setor_atendimento_p) = cd_setor_atendimento_p
and		nvl(c.cd_estabelecimento, obter_estabelecimento_ativo) = obter_estabelecimento_ativo;


if (nr_seq_regra_w is not null) then
	select 	nvl(max('N'), 'S')
	into 	ds_retorno_w
	from	cpoe_regra_duplicidade b
	where	b.nr_sequencia = nr_seq_regra_w
	and		((ie_consiste_dupl_sol = 'N' and ie_tipo_item_p in ('MAT','MCOMP1', 'MCOMP2', 'MCOMP3','MCOMP4', 'MCOMP5', 'MCOMP6', 'MCOMPADD',
																'PMAT1', 'PMAT2', 'PMAT3', 'PMAT4', 'PMAT5',
																'GMAT1', 'GMAT2', 'GMAT3',
																'DPROD1', 'DPROD2', 'DPROD3', 'DPROD4', 'DPROD5',
																'MSOL1', 'MSOL2', 'MSOL3', 'MSOL4', 'MSOL5',
																'MCOMP1', 'MCOMP2', 'MCOMP3', 'MCOMP4', 'MCOMP5', 'MCOMP6')) or
			(ie_consiste_dupl_exa = 'N' and ie_tipo_item_p = 'P') or
			(ie_consiste_dupl_nut = 'N' and ie_tipo_item_p in ('E','J','L','O','S')) or
			(ie_consiste_dupl_gas = 'N' and ie_tipo_item_p = 'G') or
			(ie_consiste_dupl_rec = 'N' and ie_tipo_item_p = 'R') or 
			(ie_consiste_dupl_hem = 'N' and ie_tipo_item_p in ('HBC', 'HP')) or
			(ie_consiste_dupl_dia = 'N' and ie_tipo_item_p in ('DI', 'DP')) or
			(ie_consiste_dupl_mat = 'N' and ie_tipo_item_p = 'M')or
			(ie_consiste_dupl_ana = 'N' and ie_tipo_item_p = 'AP'));

	if (ds_retorno_w = 'S') and (cd_material_p is not null) then
		
		select 	max(cd_grupo_material),
				max(cd_subgrupo_material),
				max(cd_classe_material)
		into 	cd_grupo_material_w,
				cd_subgrupo_material_w,
				cd_classe_material_w
		from	Estrutura_Material_V
		where	cd_material = cd_material_p;
		
		select 	nvl(max('S'),'N')
		into 	ie_possui_regra_w
		from 	cpoe_regra_duplic_item
		where	nr_seq_regra = nr_seq_consist_w;
		
		if (ie_possui_regra_w = 'S') then
			
			select	nvl(max('N'),'S')
			into	ds_retorno_w
			from 	cpoe_regra_duplic_item
			where 	nr_seq_regra = nr_seq_consist_w
			and 	nvl(cd_material,cd_material_p) = cd_material_p
			and 	nvl(cd_grupo_material,cd_grupo_material_w) = cd_grupo_material_w
			and 	nvl(cd_subgrupo_material,cd_subgrupo_material_w) = cd_subgrupo_material_w
			and 	nvl(cd_classe_material,cd_classe_material_w) = cd_classe_material_w;
		end if;
	end if;
else
	ds_retorno_w := 'S';
end if;

return ds_retorno_w;

end obter_se_cpoe_regra_duplic;
/
