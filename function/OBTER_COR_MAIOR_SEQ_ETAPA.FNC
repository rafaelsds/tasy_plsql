create or replace
function    obter_cor_maior_seq_etapa(	nr_interno_conta_p		number	)
			return varchar2 is

nr_seq_etapa_w	number(10,0);
ds_cor_etapa_w	varchar2(15) := '0';

Cursor C01 is
	select	nvl(a.nr_seq_etapa,0)		
	from	fatur_etapa		b,
		conta_paciente_etapa	a	  
	where	a.nr_seq_etapa		= b.nr_sequencia
	and	a.nr_interno_conta	= nr_interno_conta_p
	and	nvl(b.ie_situacao,'A')	= 'A'
	order by b.nr_seq_etapa;

begin	

open C01;
loop
fetch C01 into	
	nr_seq_etapa_w;
exit when C01%notfound;
	begin	
	nr_seq_etapa_w:= nr_seq_etapa_w;	
	end;
end loop;
close C01;

if	(nr_seq_etapa_w is not null) and
	(nr_seq_etapa_w	> 0) then

	select	max(nvl(ds_cor_etapa,0))
	into	ds_cor_etapa_w
	from	fatur_etapa
	where	nr_sequencia	= nr_seq_etapa_w
	and	nvl(ie_situacao,'A') = 'A';
end if;

if	(ds_cor_etapa_w is null) then
	ds_cor_etapa_w	:=	0;
end if;

return	ds_cor_etapa_w;

end obter_cor_maior_seq_etapa;
/
