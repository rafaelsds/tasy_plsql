create or replace
function obter_dt_alta_gestao_cirurgia(nr_atendimento_p	number)
 		    	return date is


ie_minutos_tolerancia_w	number(10);

begin
Obter_param_Usuario(900, 528, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_minutos_tolerancia_w);

return	nvl(Obter_data_alta_Atend(nr_atendimento_p), sysdate + 10) - ((1/24/60) * lab_obter_valor_parametro(900, 528));

end obter_dt_alta_gestao_cirurgia;
/
