create or replace
function cpoe_obter_obriga_justif_proc( nr_seq_proc_interno_p	regra_obriga_just_proc.nr_seq_proc_interno%type,
										nr_atendimento_p		atendimento_paciente.nr_atendimento%type)
 		    	return varchar2 is

ie_result_w				char(1) := 'N';
cd_convenio_w			convenio.cd_convenio%type;
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
cd_tipo_procedimento_w	procedimento.cd_tipo_procedimento%type;
ie_origem_proced_w		proc_interno.ie_origem_proced%type;
cd_procedimento_w		proc_interno.cd_procedimento%type;
qt_vezes_util_dia_w		regra_obriga_just_proc.qt_vezes_perm_dia%type;
qt_vezes_perm_dia_w		regra_obriga_just_proc.qt_vezes_perm_dia%type;
nr_seq_proc_interno_w	regra_obriga_just_proc.nr_seq_proc_interno%type;
cd_area_proc_w			regra_obriga_just_proc.cd_area_procedimento%type;
cd_especialidade_w		regra_obriga_just_proc.cd_especialidade%type;
cd_grupo_proc_w			regra_obriga_just_proc.cd_grupo_proc%type;

Cursor proc_data is
	select 	distinct a.cd_procedimento,
		    c.cd_grupo_proc,
		    c.cd_especialidade,
		    e.cd_area_procedimento,
			a.ie_origem_proced,
			b.cd_tipo_procedimento
	from  	proc_interno a,
			procedimento b,
			grupo_proc c,
			especialidade_proc d,
			area_procedimento e
	where   nr_sequencia = nr_seq_proc_interno_p
	and		a.cd_procedimento = b.cd_procedimento
	and		b.ie_origem_proced = a.ie_origem_proced
	and     b.cd_grupo_proc = c.cd_grupo_proc
	and     c.cd_especialidade =  d.cd_especialidade
	and     d.cd_area_procedimento = e.cd_area_procedimento;

begin
select	nvl(max('S'),'N')
into	ie_result_w
from	regra_obriga_just_proc
where	rownum = 1;

if	(ie_result_w = 'S') then
	open proc_data;
	loop
	fetch proc_data into	
		cd_procedimento_w,
		cd_grupo_proc_w,
		cd_especialidade_w,
		cd_area_proc_w,
		ie_origem_proced_w,
		cd_tipo_procedimento_w;
	exit when proc_data%notfound;
		begin	  
		
		cd_convenio_w			:= obter_convenio_atendimento(nr_atendimento_p);
		cd_setor_atendimento_w	:= obter_setor_atendimento(nr_atendimento_p);
		
		select	nvl(min(qt_vezes_perm_dia),-1),
				min(nr_seq_proc_interno)
		into	qt_vezes_perm_dia_w,
				nr_seq_proc_interno_w
		from	regra_obriga_just_proc
		where	rownum = 1
		and		qt_vezes_perm_dia is not null
		and		nvl(cd_procedimento, nvl(cd_procedimento_w,0)) = nvl(cd_procedimento_w,0)
		and		((cd_procedimento is null) or (nvl(ie_origem_proced, nvl(ie_origem_proced_w,0)) = nvl(ie_origem_proced_w,0)))
		and		nvl(nr_seq_proc_interno, nvl(nr_seq_proc_interno_p,0)) = nvl(nr_seq_proc_interno_p,0)
		and		nvl(cd_area_procedimento, nvl(cd_area_proc_w,0)) = nvl(cd_area_proc_w,0)
		and		nvl(cd_especialidade, nvl(cd_especialidade_w,0)) = nvl(cd_especialidade_w,0)
		and		nvl(cd_tipo_procedimento, nvl(cd_tipo_procedimento_w,0)) = nvl(cd_tipo_procedimento_w,0)
		and		nvl(cd_convenio,cd_convenio_w) = cd_convenio_w
		and		nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
		and		nvl(cd_grupo_proc, nvl(cd_grupo_proc_w,0)) = nvl(cd_grupo_proc_w,0);
		
		if	(qt_vezes_perm_dia_w > -1) then
			select	count(*)
			into	qt_vezes_util_dia_w
			from	cpoe_procedimento a
			where	nvl(a.nr_seq_proc_interno,0) = nvl(nr_seq_proc_interno_w,nvl(a.nr_seq_proc_interno,0))
			and		a.dt_suspensao is null
			and		sysdate between trunc(a.dt_inicio) and fim_dia(nvl(a.dt_fim, dt_inicio + 1))
			and		a.nr_atendimento = nr_atendimento_p;
			
			if	(qt_vezes_util_dia_w >= qt_vezes_perm_dia_w) then
				ie_result_w	:= 'S';
			else
				ie_result_w	:= 'N';
			end if;
		else
			ie_result_w	:= 'N';
		end if;
	end;
	end loop;
	close proc_data;
end if;


return	ie_result_w;

end cpoe_obter_obriga_justif_proc;
/