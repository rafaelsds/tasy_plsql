create or replace function OBTER_DESC_ESCALA_FIQ (QT_PONTUACAO_P NUMBER)
			      return VARCHAR2 is

DS_RETORNO_W VARCHAR2 (255);

begin

if (QT_PONTUACAO_P = 0) then -- Sempre;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(306266),1,255);

	elsif (QT_PONTUACAO_P = 1) then -- Quase Sempre;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(494795),1,255);

	elsif (QT_PONTUACAO_P = 2) then -- De vez em quando;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(788127),1,255);

	elsif (QT_PONTUACAO_P = 3) then -- Nunca;
		DS_RETORNO_W := substr(OBTER_DESC_EXPRESSAO(730979),1,255);

end if;

return DS_RETORNO_W;

end OBTER_DESC_ESCALA_FIQ;
/