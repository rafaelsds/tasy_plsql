create or replace
function obter_dados_opme_tipo_cx_item(	nr_sequencia_p number,
					ie_opcao_p	varchar2)
					return varchar2 is
/*ie_opcao
MA - Material*/
retorno_w	varchar2(255);
cd_material_w	number(10);

begin
select  max(cd_material)
into	cd_material_w
from	opme_tipo_caixa_item
where	nr_sequencia = nr_sequencia_p;

if (ie_opcao_p = 'MA') then
	retorno_w := cd_material_w;
end if;	

return	retorno_w;

end obter_dados_opme_tipo_cx_item;
/