create or replace
function obter_se_item_frac(	nr_seq_horario_p	Number)
 		    			return varchar2 is

ds_retorno_w	varchar2(1);						
						
begin

select	coalesce(max('S'), 'N')
into	ds_retorno_w
from	gedi_medic_atend
where	nr_seq_horario = nr_seq_horario_p
and		nvl(qt_saldo,0) > 0;

if	(ds_retorno_w = 'N') then

    select	coalesce(max('S'), 'N')
	into	ds_retorno_w
	from	prescr_material a,
			prescr_mat_hor b
	where	a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia = b.nr_seq_material
	and		b.nr_sequencia = nr_seq_horario_p
	and		b.qt_dispensar_hor = 0
	and		a.ie_regra_disp in ('E', 'S');
	
end if;

return	ds_retorno_w;

end obter_se_item_frac;
/