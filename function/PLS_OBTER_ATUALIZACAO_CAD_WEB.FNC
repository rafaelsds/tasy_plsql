create or replace
function pls_obter_atualizacao_cad_web( nr_seq_segurado_p	Number)
					return Number is

	
nr_seq_solic_alt_w			Number(10)	:= null;
cd_pessoa_fisica_w			Varchar2(10) 	:= '';
ie_valida_tasy_w			Varchar2(1)	:= 'N';
ie_valida_portal_w			Varchar2(1)	:= 'N';
qt_dias_atualizacao_w			Number(5) 	:= 0;
nr_seq_regra_atualizacao_w		Number(10)	:= null;
qt_atualizacao_w			Number(5)	:= null;
ie_tipo_regra_w				varchar2(2);
nr_seq_regra_w				number(10);
qt_reg_w				number(10);

ie_valida_sexo_w			varchar2(1);
ie_valida_dt_nasc_w			varchar2(1);
ie_valida_rg_w				varchar2(1);
ie_valida_cpf_w				varchar2(1);
ie_valida_nm_mae_w			varchar2(1);
ie_valida_endereco_w			varchar2(1);
ie_valida_telefone_w			varchar2(1);
ie_valida_email_w			varchar2(1);
ie_valida_uf_w				varchar2(1);
ie_valida_celular_w			varchar2(1);
ie_valida_pis_w				varchar2(1);
ie_valida_declaracao_nasc_w		varchar2(1);
ie_valida_estado_civil_w		varchar2(1);
ie_valida_cartao_nac_sus_w		varchar2(1);

ie_sexo_w				varchar2(1); 
nr_cpf_w				varchar2(11);
dt_nascimento_w				date;
nr_identidade_w				varchar2(15);
nm_mae_w				varchar2(255);
ie_estado_civil_w			varchar2(2);
nr_cartao_nac_sus_w			varchar2(20);
cd_declaracao_nasc_vivo_w		varchar2(30);
nr_ddd_celular_w			varchar2(3);
nr_ddi_celular_w			varchar2(3);
nr_telefone_celular_w			varchar2(40);
nr_pis_pasep_w				varchar2(11);
ds_endereco_w				varchar2(100);
nr_endereco_w				number(5);
ds_bairro_w				varchar2(80);
ds_municipio_w				varchar2(40);
sg_estado_w				compl_pessoa_fisica.sg_estado%type;
ds_email_w				varchar2(255);
nr_ddd_telefone_w			varchar2(3);
nr_ddi_telefone_w			varchar2(3);
nr_telefone_w				varchar2(15);

ie_falta_informacao_w			varchar2(1) := 'N';
ie_tipo_segurado_w			varchar2(2);

cursor C01 is
	select 	nr_sequencia,
		qt_dias_atualizacao
	from	pls_regra_atualizacao_cad
	where	dt_inicio_vigencia <= sysdate
	and 	((dt_fim_vigencia is not null and dt_fim_vigencia > sysdate) or (dt_fim_vigencia is null))
	and	ie_valida_portal = 'S'
	and	((ie_benef_operadora = 'S' and ie_tipo_segurado_w in ('A','B','R'))
	or	(ie_benef_intercambio = 'S' and ie_tipo_segurado_w in ('I','H')));
	
begin

	begin
		select 	cd_pessoa_fisica,
			ie_tipo_segurado
		into	cd_pessoa_fisica_w,
			ie_tipo_segurado_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_p
		and	dt_liberacao is not null
		and	(dt_limite_utilizacao is null or dt_limite_utilizacao >= sysdate);
	exception
	when others then
		cd_pessoa_fisica_w := null;
		ie_tipo_segurado_w := 'X';
	end;	


	if	(cd_pessoa_fisica_w is not null) then	
		select 	max(nr_sequencia)
		into	nr_seq_regra_w
		from	pls_regra_atualizacao_cad
		where	dt_inicio_vigencia <= sysdate
		and 	((dt_fim_vigencia is not null and dt_fim_vigencia > sysdate) or (dt_fim_vigencia is null))
		and	ie_valida_portal = 'S'
		and	((ie_benef_operadora = 'S' and ie_tipo_segurado_w in ('A','B','R'))
	or		(ie_benef_intercambio = 'S' and ie_tipo_segurado_w in ('I','H')));
	
		if	(nr_seq_regra_w is not null) then
		
			begin
				select	ie_tipo_regra
				into	ie_tipo_regra_w
				from	pls_regra_atualizacao_cad	
				where	nr_sequencia = nr_seq_regra_w;
			exception
			when others then
				ie_tipo_regra_w := 'X';
			end;	
			
			select	nvl(max(a.nr_sequencia),0)	
			into	nr_seq_solic_alt_w
			from	tasy_solic_alteracao a,
					tasy_solic_alt_campo b
			where	a.dt_analise is null
			and	a.nr_sequencia = b.nr_seq_solicitacao
			and	b.nm_tabela in ('PESSOA_FISICA', 'COMPL_PESSOA_FISICA')
			and	(b.ds_chave_simples = cd_pessoa_fisica_w 
			or (substr(b.ds_chave_composta,1,length('CD_PESSOA_FISICA='||cd_pessoa_fisica_w)) = 'CD_PESSOA_FISICA='||cd_pessoa_fisica_w));

		if	( nr_seq_solic_alt_w = 0 ) then	
			if	(ie_tipo_regra_w = 'D') then
					
					open C01;
					loop
					fetch C01 into	
						nr_seq_regra_atualizacao_w,
						qt_dias_atualizacao_w;
					exit when C01%notfound;
						begin			
						if	( qt_dias_atualizacao_w is not null and qt_dias_atualizacao_w > 0 ) then
							select	count(1)
							into	qt_atualizacao_w
							from	pessoa_fisica
							where	cd_pessoa_fisica = cd_pessoa_fisica_w
							and	((dt_revisao is null) 
							or	((dt_revisao + qt_dias_atualizacao_w) <= sysdate));
							
							if	( qt_atualizacao_w > 0 ) then
								exit;
							end if;
						end if;
						nr_seq_regra_atualizacao_w := 0;	
						end;
					end loop;
					close C01;
				
			elsif	(ie_tipo_regra_w = 'C') then
			
				select	count(1)
				into	qt_reg_w
				from	pls_regra_campos_cad;
				
				if	(qt_reg_w > 0) then
					begin				
						select	ie_valida_sexo,
							ie_valida_dt_nasc,
							ie_valida_rg,
							ie_valida_cpf,
							ie_valida_nm_mae,
							ie_valida_telefone,
							ie_valida_celular,
							ie_valida_pis,
							ie_valida_declaracao_nasc_vivo,
							ie_valida_email,
							ie_valida_uf,
							ie_valida_estado_civil,
							ie_valida_cartao_nac_sus,
							ie_valida_endereco
						into	ie_valida_sexo_w,
							ie_valida_dt_nasc_w,
							ie_valida_rg_w,
							ie_valida_cpf_w,
							ie_valida_nm_mae_w,
							ie_valida_telefone_w,
							ie_valida_celular_w,
							ie_valida_pis_w,
							ie_valida_declaracao_nasc_w,
							ie_valida_email_w,
							ie_valida_uf_w,
							ie_estado_civil_w,
							ie_valida_cartao_nac_sus_w,
							ie_valida_endereco_w
						from	pls_regra_campos_cad;
					exception
					when others then
						ie_valida_sexo_w := 'N';
						ie_valida_dt_nasc_w := 'N';
						ie_valida_rg_w := 'N';
						ie_valida_cpf_w := 'N';
						ie_valida_nm_mae_w := 'N';
						ie_valida_telefone_w := 'N';
						ie_valida_celular_w := 'N';
						ie_valida_pis_w := 'N';
						ie_valida_declaracao_nasc_w := 'N';
						ie_valida_email_w := 'N';
						ie_valida_uf_w := 'N';
						ie_estado_civil_w := 'N';
						ie_valida_cartao_nac_sus_w := 'N';
						ie_valida_endereco_w := 'N';
					end;
					
					select	ie_sexo, 
						nr_cpf,
						dt_nascimento,
						nr_identidade,
						obter_nome_mae_pf(cd_pessoa_fisica),
						ie_estado_civil,
						nr_cartao_nac_sus,
						cd_declaracao_nasc_vivo,
						nr_pis_pasep,
						nr_ddd_celular,
						nr_ddi_celular,
						nr_telefone_celular
					into	ie_sexo_w, 
						nr_cpf_w, 
						dt_nascimento_w,
						nr_identidade_w,
						nm_mae_w,
						ie_estado_civil_w,
						nr_cartao_nac_sus_w,
						cd_declaracao_nasc_vivo_w,
						nr_pis_pasep_w,
						nr_ddd_celular_w,
						nr_ddi_celular_w,
						nr_telefone_celular_w
					from	pessoa_fisica
					where	cd_pessoa_fisica = cd_pessoa_fisica_w;
					
					if	(ie_valida_sexo_w = 'S' and nvl(ie_sexo_w,'X') = 'X') then
						ie_falta_informacao_w := 'S';
					elsif	(ie_valida_rg_w = 'S' and nvl(nr_identidade_w,'X') = 'X') then
						ie_falta_informacao_w := 'S';
					elsif	(ie_valida_cpf_w = 'S' and nvl(nr_cpf_w,'X') = 'X') then
						ie_falta_informacao_w := 'S';
					elsif	(ie_valida_nm_mae_w = 'S' and nvl(nm_mae_w,'X') = 'X') then
						ie_falta_informacao_w := 'S';
					elsif	(ie_valida_dt_nasc_w = 'S' and dt_nascimento_w is null) then
						ie_falta_informacao_w := 'S';
					elsif	(ie_valida_pis_w = 'S' and nvl(nr_pis_pasep_w,'X') = 'X') then
						ie_falta_informacao_w := 'S';
					elsif	(ie_valida_declaracao_nasc_w = 'S' and nvl(cd_declaracao_nasc_vivo_w,'X') = 'X') then
						ie_falta_informacao_w := 'S';
					elsif	(ie_valida_cartao_nac_sus_w = 'S' and nvl(nr_cartao_nac_sus_w,'X') = 'X') then
						ie_falta_informacao_w := 'S';
					elsif	(ie_valida_celular_w = 'S') then
						if((nvl(nr_ddd_celular_w,'X') = 'X') or (nvl(nr_ddi_celular_w,'X') = 'X') or (nvl(nr_telefone_celular_w,'X') = 'X')) then
							ie_falta_informacao_w := 'S';
						end if;
					elsif	((ie_valida_endereco_w = 'S') or (ie_valida_uf_w = 'S')
					or (ie_valida_telefone_w = 'S') or (ie_valida_email_w = 'S')) then
						begin
							select	ds_endereco,
								nr_endereco,
								ds_bairro,
								ds_municipio,
								sg_estado,
								ds_email,
								nr_ddd_telefone,
								nr_ddi_telefone,
								nr_telefone
							into	ds_endereco_w,
								nr_endereco_w,
								ds_bairro_w,
								ds_municipio_w,
								sg_estado_w,
								ds_email_w,
								nr_ddd_telefone_w,
								nr_ddi_telefone_w,
								nr_telefone_w
							from	Compl_Pessoa_Fisica
							where	cd_pessoa_fisica = cd_pessoa_fisica_w
						and	ie_tipo_complemento = 1;
						exception
						when others then
							ds_endereco_w		:= null;
							nr_endereco_w		:= null;
							ds_bairro_w 		:= null;
							ds_municipio_w		:= null;
							sg_estado_w			:= null;
							ds_email_w			:= null;
							nr_ddd_telefone_w	:= null;
								nr_ddi_telefone_w	:= null;
								nr_telefone_w		:= null;
						end;

						if	((nvl(ds_endereco_w,'X') = 'X') or 
							(nvl(nr_endereco_w,0) = 0) or
							(nvl(ds_bairro_w,'X') = 'X') or
							(nvl(ds_municipio_w,'X') = 'X')) then
							ie_falta_informacao_w := 'S';
						elsif	(ie_valida_uf_w = 'S' and (nvl(sg_estado_w,'X') = 'X')) then
							ie_falta_informacao_w := 'S';
						elsif	(ie_valida_email_w = 'S' and (nvl(ds_email_w,'X') = 'X')) then  
							ie_falta_informacao_w := 'S';
						elsif	(ie_valida_telefone_w = 'S') then
							if((nvl(nr_ddd_telefone_w,'X') = 'X') or (nvl(nr_ddi_telefone_w,'X') = 'X') or (nvl(nr_telefone_w,'X') = 'X'))then
								ie_falta_informacao_w := 'S';
							end if;
						end if;
					end if;
					
					
					if	(ie_falta_informacao_w = 'S') then
						nr_seq_regra_atualizacao_w := nr_seq_regra_w;
					end if;
				end if;	
			end if;
		end if;
	end if;
end if;

return	nr_seq_regra_atualizacao_w;

end pls_obter_atualizacao_cad_web;
/
