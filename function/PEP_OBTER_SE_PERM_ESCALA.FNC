create or replace
function PEP_obter_se_perm_escala(ie_escala_p	varchar2)
			return varchar2 is

ds_retorno_w	varchar2(1);

begin
select	nvl(max('S'),'N') ie_escala
into	ds_retorno_w
from	grupo_escala_indice_item
where	ie_escala = ie_escala_p
and	nvl(wheb_assist_pck.getEscalaLiberada(ie_escala),'S') = 'S';

return	ds_retorno_w;

end PEP_obter_se_perm_escala;
/