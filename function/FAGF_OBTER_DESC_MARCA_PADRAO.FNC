CREATE OR REPLACE
FUNCTION FAGF_obter_desc_marca_padrao(	ds_marca_p		VARCHAR2,
			ie_padronizado_p	VARCHAR2)
 		    	RETURN VARCHAR2 IS

ds_marca_w	VARCHAR2(255);

BEGIN

IF	(NVL(ie_padronizado_p,'N') = 'S') THEN

	SELECT	ds_marca
	INTO	ds_marca_w
	FROM	marca
	WHERE	ds_marca = ds_marca_p;
	
	ds_marca_w := '#' || ds_marca_w || '#' || 'N' ||'#P';

ELSE
	ds_marca_w := ds_marca_p;
	
END IF;

RETURN	ds_marca_w;

END FAGF_obter_desc_marca_padrao;
/