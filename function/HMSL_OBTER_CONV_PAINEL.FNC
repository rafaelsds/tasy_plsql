create or replace
function  hmsl_obter_conv_painel(dt_parametro_p		date,
				cd_convenio_p		number)
 		    	return varchar2 is

cd_convenio_w	number(10);	
ie_convenio_w   varchar2(1) := 'N';
ie_pertence_w	number(1) := 0;
		
Cursor C01 is
	select  cd_convenio
	from (SELECT 	cd_convenio,
			sum(nr_leitos_ocupados) 
		FROM 	HMSL_EIS_OCUP_convenio_V
		WHERE  	TRUNC(dt_referencia,'month') = TRUNC(dt_parametro_p,'month')
		AND	ie_periodo = 'M'
		and	nr_leitos_ocupados > 0
		GROUP BY cd_convenio
		ORDER BY 2 DESC)
	where	rownum < 6;
		
begin

open C01;
loop
fetch C01 into	
	cd_convenio_w;
exit when C01%notfound;
	begin
	
	if	(cd_convenio_w = cd_convenio_p) then
		ie_pertence_w := 1;
	end if;

	end;
end loop;
close C01;


if (ie_pertence_w = 1) then
	ie_convenio_w := 'S';
end if;

return	ie_convenio_w;

end hmsl_obter_conv_painel;
/