create or replace
function OBTER_SALDO_BANCO_ANTERIOR (nr_seq_saldo_banco_p	in	number) return number is

dt_referencia_w		date;
dt_ref_anterior_w	date;
vl_saldo_w		number(15,2);
nr_seq_conta_w		number(10);

begin

begin
select	dt_referencia,
	nr_seq_conta
into	dt_referencia_w,
	nr_seq_conta_w
from	banco_saldo
where	nr_sequencia = nr_seq_saldo_banco_p;

select	max(dt_referencia)
into	dt_ref_anterior_w
from	banco_saldo
where	dt_referencia < dt_referencia_w
and	nr_seq_conta = nr_seq_conta_w;

select	nvl(max(vl_saldo), 0)
into	vl_saldo_w
from	banco_saldo
where	trunc(dt_referencia, 'month') = trunc(dt_ref_anterior_w, 'month')
and	nr_seq_conta = nr_seq_conta_w;
exception
	when others then
		vl_saldo_w := 0;
end;

return vl_saldo_w;

end;
/