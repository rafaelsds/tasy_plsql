CREATE OR REPLACE
FUNCTION obter_setor_ativo
     			RETURN number IS

cd_setor_w    		Number(5);

BEGIN

cd_setor_w := wheb_usuario_pck.get_cd_setor_atendimento;

RETURN cd_setor_w;

END obter_setor_ativo;
/
