create or replace
function obter_consumo_periodo(ie_tipo_p			varchar2,
			    cd_estabelecimento_p		number,
			    cd_material_p			number,
			   dt_mesano_referencia_p		date,
			    nr_dias_p			number)
			 		return number is

ds_retorno_w			number(18,4);
qt_consumo_w			number(18,4);
vl_consumo_w			number(18,2);
dt_mesano_referencia_w		date;

begin

if	(dt_mesano_referencia_p is null) then
	dt_mesano_referencia_w	:= pkg_date_utils.start_of(sysdate,'MONTH', 0);
else
	dt_mesano_referencia_w	:= pkg_date_utils.start_of(dt_mesano_referencia_p,'MONTH', 0);
end if;

if	(nr_dias_p = 90) then
	select	sum(nvl(a.qt_consumo,0)) qt_estoque,
		sum(nvl(a.vl_consumo,0)) vl_consumo
	into	qt_consumo_w,
		vl_consumo_w
	from	movto_estoque_operacao_v a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_material		= cd_material_p
	and     	a.cd_centro_custo is not null
	and	pkg_date_utils.start_of(a.dt_mesano_referencia,'MONTH', 0) in (pkg_date_utils.add_month(pkg_date_utils.start_of(dt_mesano_referencia_w,'MONTH', 0),-1, 0),pkg_date_utils.add_month(pkg_date_utils.start_of(dt_mesano_referencia_w,'MONTH', 0),-2, 0),pkg_date_utils.add_month(pkg_date_utils.start_of(dt_mesano_referencia_w,'MONTH', 0),-3, 0));										
end if;

if	(nr_dias_p = 60) then
	select	sum(nvl(a.qt_consumo,0)) qt_estoque,
		sum(nvl(a.vl_consumo,0)) vl_consumo
	into	qt_consumo_w,
		vl_consumo_w
	from	movto_estoque_operacao_v a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and     	a.cd_centro_custo is not null
	and	a.cd_material		= cd_material_p
	and	pkg_date_utils.start_of(a.dt_mesano_referencia,'MONTH', 0) in (pkg_date_utils.add_month(pkg_date_utils.start_of(dt_mesano_referencia_w,'MONTH', 0),-1, 0),pkg_date_utils.add_month(pkg_date_utils.start_of(dt_mesano_referencia_w,'MONTH', 0),-2, 0));									
end if;

if	(nr_dias_p = 30) then
	select	sum(nvl(a.qt_consumo,0)) qt_estoque,
		sum(nvl(a.vl_consumo,0)) vl_consumo
	into	qt_consumo_w,
		vl_consumo_w
	from	movto_estoque_operacao_v a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_material		= cd_material_p
	and    	a.cd_centro_custo is not null
	and	pkg_date_utils.start_of(a.dt_mesano_referencia,'MONTH', 0) in (pkg_date_utils.add_month(pkg_date_utils.start_of(dt_mesano_referencia_w,'MONTH', 0),-1, 0));								
end if;

if	(ie_tipo_p = 'Q') then
	ds_retorno_w := qt_consumo_w;
elsif	(ie_tipo_p = 'V') then
	ds_retorno_w := vl_consumo_w;
end if;

return  ds_retorno_w;

end obter_consumo_periodo;
/
