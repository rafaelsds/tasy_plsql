Create or Replace
FUNCTION OBTER_DESC_CLASSIF_TF(	nr_seq_classificacao_p	number)
				RETURN VarChar2 IS

ds_classificacao_w	trans_financ_classif.ds_classificacao%type;

BEGIN

select	max(a.ds_classificacao)
into	ds_classificacao_w
from	trans_financ_classif a
where	a.nr_sequencia	= nr_seq_classificacao_p;

RETURN ds_classificacao_w;

END OBTER_DESC_CLASSIF_TF;
/