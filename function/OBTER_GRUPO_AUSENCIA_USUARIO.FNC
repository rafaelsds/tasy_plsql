create or replace
function obter_grupo_ausencia_usuario
			(	nm_usuario_p		varchar2)
 		    		return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		varchar2(255);

begin
select	max(g.ds_grupo)
into	ds_retorno_w
from	escala_diaria d,
	escala_grupo g,
	escala e,
	usuario u
where	u.cd_pessoa_fisica	= d.cd_pessoa_fisica
and	g.nr_sequencia = e.nr_seq_grupo
and	e.nr_sequencia = d.nr_seq_escala	
and	u.ie_situacao = 'A'
and	u.nm_usuario	= nm_usuario_p;

return ds_retorno_w;

end obter_grupo_ausencia_usuario;
/