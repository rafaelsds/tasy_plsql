create or replace function obter_data_doenca_versao(nr_sequencia_p number default '') return date is
t_versao_w date;

begin 

select dt_versao into t_versao_w
from cid_doenca_versao c
where c.nr_sequencia = nr_sequencia_p;

return t_versao_w;

end obter_data_doenca_versao;
/