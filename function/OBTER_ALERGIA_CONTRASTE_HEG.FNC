CREATE OR REPLACE 
FUNCTION Obter_alergia_contraste_heg(cd_pessoa_fisica_p	varchar2) 
			RETURN Varchar2 IS

qt_alergia_w	Number(10);
ds_retorno_w	varchar2(255);

BEGIN

select	count(*)
into	qt_alergia_w
from	paciente_alergia
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	nr_seq_tipo		= 1;

if	(qt_alergia_w = 0) then
	ds_retorno_w	:= '[ ] Sim     [ ] N�o     [X] N�o sabe';
else
	ds_retorno_w	:= '[X] Sim     [ ] N�o     [ ] N�o sabe';
end if;

RETURN	ds_retorno_w;

END Obter_alergia_contraste_heg;
/