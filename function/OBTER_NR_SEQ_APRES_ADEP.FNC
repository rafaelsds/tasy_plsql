create or replace 
function Obter_nr_seq_apres_ADEP(ie_grupo_p	varchar2,
				nr_regra_p	number,
				ie_status_item_p	varchar2 default null)
				return number is
				
nr_seq_apres_w	number(6,0) := 999;
ie_regra_w	varchar2(1);
ie_grupo_w	varchar2(10);
ie_ordena_suspenso_w		varchar2(1);

begin
Obter_Param_Usuario(950, 135, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_ordena_suspenso_w);

if	(nr_regra_p 	> 0) and
	(ie_grupo_p	is not null) then
	begin
	select 	nvl(max('S'),'N')
	into	ie_regra_w
	from	regra_ordem_rep
	where	nr_sequencia = nr_regra_p;
	
	if	(ie_regra_w = 'S') then
		begin
		
		ie_grupo_w	:= ie_grupo_p;
		if	(ie_grupo_p  = 'DE') then
			ie_grupo_w	:= 'D';
		end if;
		
		select	nvl(max(nr_seq_apres),999)
		into	nr_seq_apres_w
		from	regra_ordem_grupo_rep
		where	nr_seq_regra = nr_regra_p
		and     ((ie_grupo = ie_grupo_w) or
			 ((ie_grupo_p = 'C') and
			  (ie_grupo = 'G')));
			
		if	(ie_ordena_suspenso_w = 'S')	and
			(nvl(ie_status_item_p,'N') = 'S')	then
			
			select	nvl(max(nr_seq_apres),999) + 1
			into	nr_seq_apres_w
			from	regra_ordem_grupo_rep
			where	nr_seq_regra = nr_regra_p;
		
		end if;
		
		end;
		
	end if;
	
	end;
elsif	(ie_ordena_suspenso_w	= 'S') and
	(ie_grupo_p		is null) then
	nr_seq_apres_w	:= 0;
end if;

return nr_seq_apres_w;

end Obter_nr_seq_apres_ADEP;
/
