create or replace
function ageint_obter_se_cons_agendado(cd_agenda_p		number,
				       dt_agenda_p		date,
				       cd_pessoa_fisica_p	varchar2,
				       nr_seq_ageint_item_p	number)
 		    	return varchar2 is

ie_retorno_w		varchar2(1) := 'S';
qt_agenda_pac_dia_w	number(10,0);
nr_seq_agenda_cons_w	number(10,0);		
			
begin

select	nvl(max(nr_seq_agenda_cons),0)
into	nr_seq_agenda_cons_w
from	agenda_integrada_item
where	nr_sequencia = nr_seq_ageint_item_p;

select 	count(*)
into	qt_agenda_pac_dia_w
from	agenda_consulta
where	trunc(dt_agenda) between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400
and	cd_pessoa_fisica = cd_pessoa_fisica_p
and	cd_agenda = cd_agenda_p
and	nr_sequencia	 <> nr_seq_agenda_cons_w
and	ie_status_agenda <> 'C';

if	(qt_agenda_pac_dia_w > 0) then
	ie_retorno_w := 'N';
end if;

return	ie_retorno_w;

end ageint_obter_se_cons_agendado;
/