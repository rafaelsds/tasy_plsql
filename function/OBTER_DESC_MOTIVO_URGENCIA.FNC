create or replace
function obter_desc_motivo_urgencia(nr_seq_motivo_urgente_p  number)
        return varchar2 is

ds_retorno_w		varchar2(255);

begin

select ds_motivo
into ds_retorno_w
from motivo_urgencia_compras
where nr_sequencia = nr_seq_motivo_urgente_p;

return ds_retorno_w;

end obter_desc_motivo_urgencia;
/