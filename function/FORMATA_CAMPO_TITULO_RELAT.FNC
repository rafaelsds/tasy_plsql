create or replace
function FORMATA_CAMPO_TITULO_RELAT (	cd_conta_contabil_p	varchar2,
						cd_centro_custo_p	number,
						cd_estabelecimento_p	number,
						ds_campo_p		varchar2,
						ie_campo_p		varchar2,
						ds_formatacao_p	varchar2) 
			return varchar2 is

/*
Ie_campo_p = 	'V' - Varchar2
		'N' - Number
Criada por Anderson 03/08/2007 para atender uma necessidade específica com base na OS 63821
*/

ds_retorno_w	varchar2(2000);

begin

if	(cd_conta_contabil_p is not null) or
	(cd_centro_custo_p is not null) then

	if	(ie_campo_p = 'V') then

		select	nvl(decode(substr(obter_dados_conta_contabil(cd_conta_contabil_p, cd_estabelecimento_p, 'T'),1,1), 'T',  '#' || 
				ds_campo_p || '#' || ds_formatacao_p || '#P', 
			decode(substr(ctb_obter_dados_centro_custo(cd_centro_custo_p,'T'),1,1), 'T', '#' || ds_campo_p || '#' || 
				ds_formatacao_p || '#P', ds_campo_p)), ds_campo_p)
		into	ds_retorno_w
		from	dual;

	elsif	(ie_campo_p = 'N') then

		select	nvl(decode(substr(obter_dados_conta_contabil(cd_conta_contabil_p, cd_estabelecimento_p, 'T'),1,1), 'T',  '#' || 
				substr(campo_mascara_virgula(ds_campo_p),1,30) || '#' || ds_formatacao_p || '#P', 
			decode(substr(ctb_obter_dados_centro_custo(cd_centro_custo_p,'T'),1,1), 'T', '#' || substr(campo_mascara_virgula(ds_campo_p),1,30) || 
				'#' || ds_formatacao_p || '#P', substr(campo_mascara_virgula(ds_campo_p),1,30))), substr(campo_mascara_virgula(ds_campo_p),1,30))
		into	ds_retorno_w
		from	dual;

	end if;
else
	if	(ie_campo_p = 'V') then

		select	'#' || ds_campo_p || '#' || ds_formatacao_p || '#P'
		into	ds_retorno_w
		from	dual;

	elsif	(ie_campo_p = 'N') then

		select	'#' || substr(campo_mascara_virgula(ds_campo_p),1,30) || '#' || ds_formatacao_p || '#P'
		into	ds_retorno_w
		from	dual;
	end if;
end if;

return ds_retorno_w;

end FORMATA_CAMPO_TITULO_RELAT;
/