create or replace
function contr_temp_item_duplic(nr_seq_setor_temp_p	 number,
				nr_seq_item_p		number)
 		    	return number is

qt_registro_w		number(10);			
cd_setor_w		number(10);
dt_hora_inicio_w	date;
nr_seq_turno_w		number(10);

begin

select	cd_setor_atendimento,
	dt_hora_inicio,
	nr_seq_turno
into	cd_setor_w,
	dt_hora_inicio_w,
	nr_seq_turno_w
from	setor_temperatura
where	nr_sequencia = nr_seq_setor_temp_p;

select  count(*) 
into	qt_registro_w
from    setor_temperatura a, 
        setor_temperatura_item b   
where   b.nr_seq_setor_temp = a.nr_sequencia   
and     a.cd_setor_atendimento = cd_setor_w
and     trunc(a.dt_hora_inicio,'dd') = trunc(dt_hora_inicio_w,'dd') 
and     a.nr_seq_turno =  nr_seq_turno_w
and     b.nr_seq_item  =  nr_seq_item_p;



return	qt_registro_w;

end contr_temp_item_duplic;
/
