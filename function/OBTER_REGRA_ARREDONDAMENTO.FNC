create or replace
function obter_regra_arredondamento(cd_convenio_p		number,
				    cd_categoria_p		varchar2,	
				    cd_item_p			number,
				    ie_origem_proced_p		number,
				    cd_estabelecimento_p	number,
				    dt_vigencia_p		date,
				    ie_tipo_item_p		varchar2,
				    ie_opcao_p			varchar2)
 		    	return varchar2 is

/* ie_opcao_p
1  - Forma de arredondamento
2 - Numero de casas Decimais
*/

/* ie_tipo_item_p
P - Procedimento
M- Material
*/

ds_retorno_w			varchar2(5);
cd_grupo_mat_w			grupo_material.cd_grupo_material%type;
cd_subgrupo_mat_w		subgrupo_material.cd_subgrupo_material%type;
cd_classe_mat_w			classe_material.cd_classe_material%type;
ie_arredondamento_w		varchar2(1);
nr_dec_unitario_w		conv_regra_arred_mat.nr_dec_unitario%type;
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
cd_grupo_proc_w			procedimento.cd_grupo_proc%type;
cd_especialidade_w		grupo_proc.cd_especialidade%type;
cd_area_procedimento_w		especialidade_proc.cd_area_procedimento%type;
dt_vigencia_w			date;

Cursor C01 is
	select	ie_arredondamento,
		nr_dec_unitario
	from	conv_regra_arred_mat
	where	cd_estabelecimento = cd_estabelecimento_p
	and 	ie_situacao = 'A'
	and 	cd_convenio = cd_convenio_p
	and 	(nvl(cd_material,nvl(cd_item_p,0))	= nvl(cd_item_p,0))
	and	(nvl(cd_grupo_material,nvl(cd_grupo_mat_w,0))	= nvl(cd_grupo_mat_w,0))
	and	(nvl(cd_subgrupo_material,nvl(cd_subgrupo_mat_w,0))  = nvl(cd_subgrupo_mat_w,0))
	and	(nvl(cd_classe_material,nvl(cd_classe_mat_w,0))	= nvl(cd_classe_mat_w,0))
	and	nvl(cd_categoria, nvl(cd_categoria_p,'0')) = nvl(cd_categoria_p,'0')
	and	dt_vigencia_w between nvl(dt_inicio_vigencia, dt_vigencia_w) and ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(nvl(dt_final_vigencia, dt_vigencia_w))
	order by
		nvl(cd_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_categoria,'0');
		
Cursor C02 is
	select	ie_arredondamento		
	from	conv_regra_arred_proc
	where	cd_estabelecimento = cd_estabelecimento_p
	and 	ie_situacao = 'A'
	and 	cd_convenio = cd_convenio_p
	and 	(nvl(cd_procedimento,nvl(cd_item_p,0))	= nvl(cd_item_p,0))
	and 	((((cd_procedimento is not null) and (nvl(ie_origem_proced,nvl(ie_origem_proced_p,0))	= nvl(ie_origem_proced_p,0)))) or
		(cd_procedimento is null))
	and	(nvl(cd_area_procedimento,nvl(cd_area_procedimento_w,0))	= nvl(cd_area_procedimento_w,0))
	and	(nvl(cd_especialidade,nvl(cd_especialidade_w,0))  = nvl(cd_especialidade_w,0))
	and	(nvl(cd_grupo_proc,nvl(cd_grupo_proc_w,0))	= nvl(cd_grupo_proc_w,0))
	and	nvl(cd_categoria, nvl(cd_categoria_p,'0')) = nvl(cd_categoria_p,'0')
	and	dt_vigencia_w between nvl(dt_inicio_vigencia, dt_vigencia_w) and ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(nvl(dt_final_vigencia, dt_vigencia_w))
	order by
		nvl(cd_procedimento,0),
		nvl(cd_area_procedimento,0),
		nvl(cd_especialidade,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_categoria,'0');
			
begin

ds_retorno_w	:= '';
dt_vigencia_w	:= nvl(dt_vigencia_p, sysdate);

if	(ie_tipo_item_p = 'M') then

	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_mat_w,
		cd_subgrupo_mat_w,
		cd_classe_mat_w
	from	estrutura_material_v
	where	cd_material = cd_item_p;
	
	open C01;
	loop
	fetch C01 into	
		ie_arredondamento_w,
		nr_dec_unitario_w;
	exit when C01%notfound;
		begin
				
		if	(ie_opcao_p = '1') then
			ds_retorno_w:= ie_arredondamento_w;
		elsif	(ie_opcao_p = '2') then
			ds_retorno_w:= nr_dec_unitario_w;
		end if;
		
		end;
	end loop;
	close C01;

elsif	(ie_tipo_item_p = 'P') then

	select	cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc
	into	cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w
	from	estrutura_procedimento_v
	where	cd_procedimento = cd_item_p
	and 	ie_origem_proced = ie_origem_proced_p;
	
	open C02;
	loop
	fetch C02 into	
		ie_arredondamento_w;		
	exit when C02%notfound;
		begin
				
		if	(ie_opcao_p = '1') then
			ds_retorno_w:= ie_arredondamento_w;
		end if;
		
		end;
	end loop;
	close C02;
	
end if;

return	ds_retorno_w;

end obter_regra_arredondamento;
/
