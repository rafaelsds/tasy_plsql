create or replace 
function fin_obter_cheque_bordero_titpa(nr_bordero_p	number,
					nr_titulo_p 	number) 
					return varchar is

nr_cheque_pago_w varchar2(255);

begin

nr_cheque_pago_w        := '';

for rec in (select      a.nr_cheque
            from	cheque_v a
            where	a.nr_sequencia in (       select	b.nr_seq_cheque
                                                from	cheque_bordero_titulo b
                                                where	b.nr_bordero = nr_bordero_p
                                                and	nvl(b.nr_titulo, nvl(nr_titulo_p, 0)) = nvl(nr_titulo_p, 0))
            and	        dt_cancelamento is null)
loop
        if	(length(rec.nr_cheque || '/' || nr_cheque_pago_w) < 255) then
                nr_cheque_pago_w := rec.nr_cheque || '/' || nr_cheque_pago_w;
        end if;
end loop;

return	nr_cheque_pago_w;

end fin_obter_cheque_bordero_titpa;
/