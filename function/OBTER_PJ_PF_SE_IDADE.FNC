create or replace
function obter_pj_pf_se_idade(
				cd_pessoa_fisica_p 	varchar2,
				cd_cgc_p			varchar2)
				return varchar2 is

ds_retorno_w		pessoa_juridica.ds_razao_social%type;
dt_nascimento_w		varchar2(80);
nr_anos_w		number;

begin
ds_retorno_w		:= '';

select obter_dados_pf(cd_pessoa_fisica_p,'dn')
into dt_nascimento_w
from dual;

select obter_idade(dt_nascimento_w,sysdate,'a') 
into nr_anos_w
from dual;

if (cd_cgc_p	is not null) then
	begin
	select ds_razao_social
	into	 ds_retorno_w
	from	 pessoa_juridica
	where	 cd_cgc	= cd_cgc_p;
	end;
elsif	(cd_pessoa_fisica_p is not null) then
	begin
		if  (nr_anos_w < 18) then
			select obter_compl_pf(cd_pessoa_fisica_p,3,'n')
			into   ds_retorno_w
			from 	dual;
		else
			select 	 substr(obter_nome_pf(cd_pessoa_fisica),1,80)
			into	 ds_retorno_w
			from	 pessoa_fisica
			where	 cd_pessoa_fisica	= cd_pessoa_fisica_p;
		end if;
	end;

end if;

return ds_retorno_w;
end obter_pj_pf_se_idade;
/
