create or replace
function pls_obter_dados_carteira_cob	(nr_seq_carteira_cobr_p		number,
					ie_opcao_p			varchar2)
					return varchar2 is

ds_retorno_w		varchar(255);
	
/* ie_opcao 
D - Descri��o da carteira 
C - C�digo carteira */
	
begin
if	(ie_opcao_p = 'D') then  
	select	substr(ds_carteira,1,255)
	into	ds_retorno_w
	from 	banco_carteira 
	where 	nr_sequencia = nr_seq_carteira_cobr_p;
end if;

if	(ie_opcao_p = 'C') then  
	select	substr(cd_carteira,1,255)
	into	ds_retorno_w
	from 	banco_carteira 
	where 	nr_sequencia = nr_seq_carteira_cobr_p;
end if;


return	ds_retorno_w;

end pls_obter_dados_carteira_cob;
/