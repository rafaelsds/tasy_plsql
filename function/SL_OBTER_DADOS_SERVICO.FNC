create or replace
function sl_obter_dados_servico(nr_seq_servico_p	number,
				ie_opcao_p		number)
				return varchar2 is
				
ds_retorno_w			varchar2(255);
cd_unidade_basica_w		varchar2(10);	
cd_unidade_compl_w		varchar2(10);
cd_setor_atendimento_w		number(5);
nm_executor_inic_serv_w		varchar2(90);
ds_setor_atendimento_w		varchar2(90);
nr_seq_unidade_w		number(10);
cd_executor_inic_serv_w		varchar2(10);
begin

select 	max(nr_seq_unidade),
	max(cd_executor_inic_serv),
	max(substr(obter_nome_pf(cd_executor_inic_serv),1,90))
into	nr_seq_unidade_w,
	cd_executor_inic_serv_w,
	nm_executor_inic_serv_w
from	sl_unid_atend
where	nr_sequencia = nr_seq_servico_p;

if	(nr_seq_unidade_w is not null) then

	select	cd_unidade_basica,
		cd_unidade_compl,
		cd_setor_atendimento,
		substr(obter_nome_setor(cd_setor_atendimento),1,90)
	into	cd_unidade_basica_w,
		cd_unidade_compl_w,
		cd_setor_atendimento_w,
		ds_setor_atendimento_w
	from	unidade_atendimento
	where	nr_seq_interno = nr_seq_unidade_w;

end if;

if	(ie_opcao_p = 0) then
	
	ds_retorno_w	:= wheb_mensagem_pck.get_texto(305720, null) || ' '|| ds_setor_atendimento_w || ' - ' || wheb_mensagem_pck.get_texto(307008,null) || ' ' || cd_unidade_basica_w || cd_unidade_compl_w; -- Setor:		Unidade:

elsif	(ie_opcao_p = 1) then

	ds_retorno_w	:= nm_executor_inic_serv_w;

end if;

return	ds_retorno_w;

end sl_obter_dados_servico;
/