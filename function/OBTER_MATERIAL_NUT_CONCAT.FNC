create or replace
function obter_material_nut_concat(
						nr_sequencia_p		number,
						ie_opcao_p			varchar2) return varchar2 is


/*
MT	=	C�DIGO DO MATERIAL
VR	= 	VAL.SOB.REFRIG.
RF	=	VALIDADE EM TEMPERATURA
VA	=	VALIDADE
*/

ds_retorno_w				varchar2(4000)	:= '';
ds_retorno2_w				varchar2(80) 	:= '';
cd_material_w				number(6);
ds_val_sob_ref_w			varchar2(200);
ds_referencia_w				varchar2(255);
ds_estagio_w				varchar2(80);
ds_forma_w					varchar2(80);
ds_estabilidade_w			varchar2(20);
ie_estab_fornec_w			varchar2(1);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

cursor c01 is
	select 	a.cd_material,
			substr(b.qt_estabilidade || ' ' || obter_valor_dominio(1246,b.ie_tempo_estab) || '; ' || obter_desc_forma_armazenamento(b.nr_seq_forma),1,200),
			b.ds_referencia,
			substr(obter_estagio_armazenamento(b.nr_seq_estagio),1,200),
			substr(obter_desc_forma_armazenamento(b.nr_seq_forma),1,200),
			substr(b.qt_estabilidade || ' ' || obter_valor_dominio(1246,b.ie_tempo_estab),1,200),
			b.ie_estab_fornec
	from	nut_atend_serv_dia_dieta a,
			material_armazenamento b,
			nut_atend_serv_dieta c
	where	c.nr_seq_dieta = a.nr_sequencia
	and		b.cd_material	= a.cd_material
	and		c.nr_seq_servico = nr_sequencia_p
	and	 	nvl(b.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and   	((a.ie_via_aplicacao	= b.ie_via_aplicacao) or		
			 ((b.ie_via_aplicacao is null) and		
			   not exists (select	1
						   from 	material_armazenamento d
						   where 	d.cd_material 		= a.cd_material
						   and   	d.ie_via_aplicacao	= a.ie_via_aplicacao
						   and 	nvl(d.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w)))
	and		a.dt_suspensao 	is null;	
	
begin

cd_estabelecimento_w := nvl(wheb_usuario_pck.get_cd_estabelecimento, 0);

open c01;
loop
fetch c01 into
		cd_material_w,
		ds_val_sob_ref_w,
		ds_referencia_w,
		ds_estagio_w,
		ds_forma_w,
		ds_estabilidade_w,
		ie_estab_fornec_w;
exit when c01%notfound;
	begin

	if	(ie_opcao_p = 'MT') then
		ds_retorno_w := ds_retorno_w|| cd_material_w||'  ';
	elsif	(ie_opcao_p = 'VR') then
		ds_retorno_w := ds_retorno_w|| ds_val_sob_ref_w||'  ';
	ELSIF	(IE_OPCAO_P = 'RF') then
		ds_retorno_w := ds_retorno_w|| ds_referencia_w||'  ';
	elsif	(ie_opcao_p = 'VA') then
		if (ie_estab_fornec_w = 'N') then
			ds_retorno_w := wheb_mensagem_pck.get_texto(309571) || ': ' || ds_estagio_w || '; ' || ds_forma_w || '; ' || ds_estabilidade_w||' '||ds_referencia_w||'  '; -- Validade
		elsif (ie_estab_fornec_w = 'S') then
			ds_retorno_w := wheb_mensagem_pck.get_texto(309571) || ': ' || ds_estagio_w || '; ' || ds_forma_w || '; ' || wheb_mensagem_pck.get_texto(309572) || '  '||ds_referencia_w||'  '; -- Validade	-- Vide Fabricante
		end if;
	end if;

	end;
end loop;
close c01;

 if (ds_retorno_w is not null) then
	ds_retorno_w := substr(ds_retorno_w,1,length(ds_retorno_w)-2);
end if;

return	ds_retorno_w;

end obter_material_nut_concat;
/