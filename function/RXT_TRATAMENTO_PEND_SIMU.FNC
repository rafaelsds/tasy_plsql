CREATE OR REPLACE FUNCTION rxt_tratamento_pend_simu(
    nr_seq_tratamento_p rxt_tratamento.nr_sequencia%TYPE
) RETURN VARCHAR2 IS

nr_count_w NUMBER(3);

BEGIN

    SELECT COUNT(tratamento.nr_sequencia)
    INTO nr_count_w
    FROM 
        rxt_tratamento tratamento,
        rxt_tumor tumor
    WHERE tratamento.nr_sequencia = nr_seq_tratamento_p
    AND tratamento.nr_seq_tumor = tumor.nr_sequencia
    AND rxt_tratamento_ag_autorizacao(tratamento.nr_sequencia) <> 'S'
    AND tratamento.dt_liberacao IS NULL
    AND tratamento.dt_suspensao IS NULL
    AND tratamento.dt_cancelamento IS NULL
    AND rxt_tratamento_finalizado(tratamento.nr_sequencia) <> 'S'
    AND (SELECT COUNT(*)
        FROM rxt_agenda
        WHERE cd_pessoa_fisica = tumor.cd_pessoa_fisica
        AND ie_status_agenda NOT IN ('B','C','E','SE')
        AND ie_tipo_agenda = 'S'
        ) > 0;

    IF (nr_count_w > 0) THEN
        RETURN 'S';
    ELSE 
        RETURN 'N';
    END IF;

END rxt_tratamento_pend_simu;
/