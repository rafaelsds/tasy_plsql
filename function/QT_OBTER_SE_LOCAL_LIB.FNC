create or replace
function Qt_Obter_Se_Local_Lib
			(nr_seq_ageint_item_p	number,
			nr_seq_local_p		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(1)	:= 'S';
ie_tipo_pend_Agenda_w	varchar2(15);
qt_regra_local_w	number(10);
nr_seq_grupo_quimio_w	number(10);
	
begin

if	(nr_seq_ageint_item_p is not null) then
	select	nvl(max(ie_tipo_pend_agenda),0),
		nvl(max(nr_seq_grupo_quimio),0)
	into	ie_tipo_pend_Agenda_w,
		nr_seq_grupo_quimio_w
	from	agenda_integrada_item
	where	nr_sequencia	= nr_seq_Ageint_item_p;

	if	(nr_seq_grupo_quimio_w	= 0) and
		(ie_tipo_pend_agenda_w	= 'Q')  then
		select	max(nr_seq_grupo)
		into	nr_seq_grupo_quimio_w
		from	qt_item_agenda
		where	cd_item	= 'Q';
	end if;

	begin
	select	1
	into	qt_regra_local_w
	from	qt_tipo_quimio_local
	where	nr_seq_local		= nr_seq_local_p
	and	rownum	= 1;
	exception
	when others then
		qt_regra_local_w	:= 0;
	end;

	if	(qt_regra_local_w	> 0) then
		select	decode(count(*),0,'N','S')
		into	ds_retorno_w
		from	qt_tipo_quimio_local
		where	nr_seq_local		= nr_seq_local_p
		and	nvl(ie_tipo_pend_Agenda, ie_tipo_pend_agenda_w)	= ie_tipo_pend_agenda_w
		and	nvl(nr_seq_grupo_quimio, nr_seq_grupo_quimio_w)	=	nr_seq_grupo_quimio_w;
	end if;
end if;

return	ds_retorno_w;

end Qt_Obter_Se_Local_Lib;
/