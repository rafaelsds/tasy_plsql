create or replace
function pls_obter_vl_baixa_amortizacao
			(nr_seq_mensalidade_p		number)
 		    	return number is

nr_titulo_w	number(10);
vl_baixa_w	number(15,2);

begin

select	max(nr_titulo)
into	nr_titulo_w
from	titulo_receber
where	nr_seq_mensalidade	= nr_seq_mensalidade_p;

select	sum(vl_recebido)
into	vl_baixa_w
from	titulo_receber_liq
where	nr_titulo	= nr_titulo_w
and	nr_seq_amortizacao is not null;

return	nvl(vl_baixa_w,0);

end pls_obter_vl_baixa_amortizacao;
/