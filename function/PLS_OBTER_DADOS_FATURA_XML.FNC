create or replace
function pls_obter_dados_fatura_xml(	nr_seq_fatura_p		pls_fatura.nr_sequencia%type,
					nr_seq_conta_proc_p	pls_conta_proc.nr_sequencia%type,
					nr_seq_conta_mat_p	pls_conta_mat.nr_sequencia%type,
					ds_valor_original_p	varchar2,
					ie_opcao_p		varchar2)
						return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [X] Outros: Projeto XML
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ds_retorno_w			varchar2(255);
cd_operadora_w			varchar2(255);
nr_seq_pagador_w		pls_contrato_pagador.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;

begin
-- origem.codigoPrestadorNaOperadora.CNPJ
if	(ie_opcao_p = 'OCNPJ') then
	select	max(cd_cgc_outorgante)
	into	ds_retorno_w
	from	pls_outorgante;
	
	select	max(x.cd_estabelecimento),
		max(a.nr_seq_pagador)
	into	cd_estabelecimento_w,
		nr_seq_pagador_w
	from	pls_lote_faturamento	x,
		pls_fatura		a
	where	x.nr_sequencia	= a.nr_seq_lote
	and	a.nr_sequencia	= nr_seq_fatura_p;
	
	--pega as informa��es da guia - parametro que foi adiconao na pls_obter_campo_fat_guia_xml
	if	(nr_seq_conta_proc_p is not null) then
		select  nr_seq_conta
		into	nr_seq_conta_w
		from	pls_conta_proc 
		where	nr_sequencia = nr_seq_conta_proc_p;
	elsif	(nr_seq_conta_mat_p is not null) then
		select  nr_seq_conta
		into	nr_seq_conta_w
		from	pls_conta_mat 
		where	nr_sequencia = nr_seq_conta_mat_p;
	end if;
	
	if	(nr_seq_conta_w is not null) then
		select	ie_tipo_guia
		into	ie_tipo_guia_w
		from	pls_conta
		where	nr_sequencia = nr_seq_conta_w;
	else
		ie_tipo_guia_w := null;	
	end if;
	
	-- origem.codigoPrestadorNaOperadora.CNPJ
	pls_obter_campo_fat_guia_xml( 1, cd_estabelecimento_w, nr_seq_pagador_w, null, ie_tipo_guia_w, ds_retorno_w, ds_retorno_w);
	
	--origem.codigoPrestadorNaOperadora.codigoPrestadorNaOperadora
	pls_obter_campo_fat_guia_xml( 2, cd_estabelecimento_w, nr_seq_pagador_w, null, ie_tipo_guia_w, cd_operadora_w, cd_operadora_w);
	
	--Caso tiver regra de operadora, ent�o n�o pega o CNPJ
	if	(trim(cd_operadora_w) is not null) then
		ds_retorno_w	:= '';
	end if;
	
--origem.codigoPrestadorNaOperadora.codigoPrestadorNaOperadora
elsif	(ie_opcao_p = 'OCOD') then

	select	max(x.cd_estabelecimento),
		max(a.nr_seq_pagador)
	into	cd_estabelecimento_w,
		nr_seq_pagador_w
	from	pls_lote_faturamento	x,
		pls_fatura		a
	where	x.nr_sequencia	= a.nr_seq_lote
	and	a.nr_sequencia	= nr_seq_fatura_p;
	
	--pega as informa��es da guia - parametro que foi adiconao na pls_obter_campo_fat_guia_xml
	if	(nr_seq_conta_proc_p is not null) then
		select  nr_seq_conta
		into	nr_seq_conta_w
		from	pls_conta_proc 
		where	nr_sequencia = nr_seq_conta_proc_p;
	elsif	(nr_seq_conta_mat_p is not null) then
		select  nr_seq_conta
		into	nr_seq_conta_w
		from	pls_conta_mat 
		where	nr_sequencia = nr_seq_conta_mat_p;
	end if;
	
	if	(nr_seq_conta_w is not null) then
		select	ie_tipo_guia
		into	ie_tipo_guia_w
		from	pls_conta
		where	nr_sequencia = nr_seq_conta_w;
	else
		ie_tipo_guia_w := null;	
	end if;

	pls_obter_campo_fat_guia_xml( 2, cd_estabelecimento_w, nr_seq_pagador_w, null, ie_tipo_guia_w, ds_retorno_w, ds_retorno_w);
-- procedimentosRealizados.procedimentos.reducaoAcrescimo
elsif	(ie_opcao_p = 'TXRA') then
	ds_retorno_w := substr(pls_obter_dados_campo_fat_xml( 5, nr_seq_fatura_p, nr_seq_conta_proc_p, nr_seq_conta_mat_p, ds_valor_original_p),1,255);
	
-- procedimentosRealizados.procedimentos.viaAcesso
elsif	(ie_opcao_p = 'VIAAC') then
	ds_retorno_w := substr(pls_obter_dados_campo_fat_xml( 3, nr_seq_fatura_p, nr_seq_conta_proc_p, nr_seq_conta_mat_p, ds_valor_original_p),1,255);

-- OPS - Envio lote faturamento > ans:cabecalho > ans:destino : ans:registroANS
elsif	(ie_opcao_p = 'RGANS') then
	ds_retorno_w := substr(pls_obter_dados_campo_fat_xml( 6, nr_seq_fatura_p, nr_seq_conta_proc_p, nr_seq_conta_mat_p, ds_valor_original_p),1,255);
	
-- procedimentosRealizados.procedimentos.tecnicaUtilizada
elsif	(ie_opcao_p = 'TECUT') then
	ds_retorno_w := substr(pls_obter_dados_campo_fat_xml( 32, nr_seq_fatura_p, nr_seq_conta_proc_p, nr_seq_conta_mat_p, ds_valor_original_p),1,255);
	
end if;

return	ds_retorno_w;

end pls_obter_dados_fatura_xml;
/