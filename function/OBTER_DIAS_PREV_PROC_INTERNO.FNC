create or replace
function obter_dias_prev_proc_interno(	nr_seq_proc_iterno_p		number,
					ie_opcao_p			varchar2)
 		    	return number is

qt_dias_w		number(10) := 0;
qt_dias_prev_inter_w	number(3);
qt_dias_prev_uti_w	number(3);

begin

if	(nvl(nr_seq_proc_iterno_p,0) > 0) then

	select	nvl(max(qt_dias_prev_inter),0),
		nvl(max(qt_dias_prev_uti),0)
	into	qt_dias_prev_inter_w,
		qt_dias_prev_uti_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_iterno_p;
	
	if	(ie_opcao_p = 'I') then
		qt_dias_w	:= qt_dias_prev_inter_w;
	elsif	(ie_opcao_p = 'U') then
		qt_dias_w	:= qt_dias_prev_uti_w;
	end if;
	
end if;

return	qt_dias_w;

end obter_dias_prev_proc_interno;
/