create or replace
function pls_valida_qtd_conta_web
			(	qt_max_contas_p		Number,
				ie_tipo_guia_p          Varchar2,
				nr_seq_prestador_p	Number,
				nr_seq_protocolo_p      Number)
				return Varchar2 is
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Verificar o limite de quantidade de protocolos por conta
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	ie_qtd_max_w = 	'N' : Protocolo atingiu seu limite de contas
			'S' : Protocolo n�o atingiu seu limite de contas
*/
				
ie_qtd_max_w		Varchar2(1) := 'S';
qt_contas_w		Number(10) := 0;
qt_protocolo_w		Number(10) := 0;
nr_seq_protocolo_w      Number(10);

begin	
	
nr_seq_protocolo_w := nr_seq_protocolo_p;

select	count(*)
into	qt_protocolo_w
from 	pls_protocolo_conta
where	nr_sequencia = nr_seq_protocolo_w
and	ie_tipo_guia = ie_tipo_guia_p;
	
if	(qt_protocolo_w = 0) then
	select	max(nr_sequencia)
	into	nr_seq_protocolo_w
	from 	pls_protocolo_conta
	where	ie_tipo_guia = ie_tipo_guia_p
	and 	ie_situacao = 'I'
	and	ie_status = '1'
	and	ie_apresentacao = 'A'
	and	ie_tipo_protocolo = 'C'
	and 	ie_forma_imp = 'P'
	and	nr_seq_prestador = nr_seq_prestador_p;
end if;
	
if	(nr_seq_protocolo_w is not null) then
	select	count(*)
	into	qt_contas_w
	from 	pls_conta
	where	nr_seq_protocolo = nr_seq_protocolo_p;
		
	if	(qt_contas_w >= qt_max_contas_p) then
		ie_qtd_max_w := 'N';
	end if;		
end if;

return	ie_qtd_max_w;

end pls_valida_qtd_conta_web;
/
