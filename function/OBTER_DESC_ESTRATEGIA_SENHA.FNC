create or replace
function obter_desc_estrategia_senha(nr_seq_estrategia_senha_p	number)
 		    	return varchar2 is
			
ds_estrategia_w		varchar2(255);
begin

if	(nvl(nr_seq_estrategia_senha_p,0) > 0) then
	
	select	ds_estrategia
	into	ds_estrategia_w
	from	estrategia_chamada_senha
	where	nr_sequencia = nr_seq_estrategia_senha_p; 

end if;
return	ds_estrategia_w;

end obter_desc_estrategia_senha;
/