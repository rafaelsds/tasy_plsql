CREATE OR REPLACE FUNCTION GET_SURGERY_DATA(
    dt_agenda      DATE,
    dt_cutoff_time VARCHAR2)
  RETURN VARCHAR2
AS
BEGIN
  IF(TRUNC(dt_agenda - sysdate) < 0) THEN
    RETURN 'S';
  ELSIF(TRUNC(dt_agenda - (sysdate-1)) > -1 AND TRUNC(dt_agenda - (sysdate-1)) <= 1) THEN
    IF (TRUNC(dt_agenda - (sysdate-1)) = 1 AND (to_date(dt_cutoff_time,'HH24:MI') - to_date(TO_CHAR(sysdate,'HH24:MI'),'HH24:MI')) >=0) THEN
      RETURN 'N';
    ELSE
      RETURN 'S';
    END IF;
   ELSIF (TRUNC(dt_agenda - (sysdate-1)) > 1) THEN
      RETURN 'N';    
  END IF;
RETURN 'S';
END GET_SURGERY_DATA;
/