create or replace 
function get_mp_patient_name(
    cd_pessoa_fisica_p varchar2)return varchar2 is

ds_retorno_w 	varchar2(150);    

begin
	select (substr(obter_titulo_academico_pf(OBTER_PESSOA_PERSON_NAME(a.nr_sequencia), 'S'),0,20) 
			|| chr(32) || substr(a.ds_given_name, 1, 45)
			|| chr(32) || substr(a.ds_component_name_3, 0 , 20)
			|| chr(32) || substr(a.ds_component_name_1, 0 , 20)
			|| chr(32) || substr(a.ds_family_name, 1, 45))
	into ds_retorno_w
	from person_name a, pessoa_fisica b 
	where a.nr_sequencia = b.nr_seq_person_name
		and b.cd_pessoa_fisica = cd_pessoa_fisica_p;
   
return	ds_retorno_w;

end	get_mp_patient_name;
/
