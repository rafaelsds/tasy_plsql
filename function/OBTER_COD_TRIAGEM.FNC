create or replace
function Obter_Cod_Triagem(nr_atendimento_p		number)
 		    	return number is

nr_seq_triagem_w	number(10);			
begin

select	max(nr_seq_triagem)
into	nr_seq_triagem_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

return	nr_seq_triagem_w;

end Obter_Cod_Triagem;
/