create or replace 
function OBTER_DESC_RET_TEF
	(cd_estabelecimento_p	varchar2,
	 ie_tipo_evento_p	varchar2,
	 cd_retorno_p		varchar2) return varchar2 is

ds_retorno_w	varchar2(255);

begin

select	max(ds_retorno)
into	ds_retorno_w
from	tipo_evento_tef
where	cd_estabelecimento	= cd_estabelecimento_p
and	ie_tipo_evento		= ie_tipo_evento_p
and	cd_retorno		= cd_retorno_p;

return	ds_retorno_w;

end OBTER_DESC_RET_TEF;
/
