create or replace 
function obter_nome_usuario_exec2
			(	nr_ordem_servico_p 		number,
				nr_seq_estagio_p		number,
				nm_usuario_exec_p		varchar2)
				return varchar2 is

nm_usuario_w			varchar2(80);
ds_resultado_w			varchar2(80);
nm_usuario_exec_w		varchar2(15);
ie_desenv_w			varchar2(1);
ie_suporte_w			varchar2(1);
nr_seq_exec_w			number(10,0);
nr_seq_estagio_w		number(10);
cd_perfil_ativo_w		number(5)	:= obter_perfil_ativo;
ie_tipo_executor_w		man_tipo_executor.ie_tipo_executor%type;
nr_seq_exec_dt_exec_w		number(10,0);
nr_seq_exec_dt_sexec_w		number(10,0);
nr_seq_exec_sdt_exec_w		number(10,0);
nr_seq_exec_sdt_sexec_w		number(10,0);
nm_usuario_dt_exec_w		varchar2(80);
nm_usuario_dt_sexec_w		varchar2(80);
nm_usuario_sdt_exec_w		varchar2(80);
nm_usuario_sdt_sexec_w		varchar2(80);


Cursor C01 is
	select	a.nr_sequencia,
		a.dt_fim_execucao,
		a.nr_seq_tipo_exec,
		nm_usuario_exec
	from	man_ordem_servico_exec	a
	where	a.nr_seq_ordem 	= nr_ordem_servico_p
	and  	exists	(select	1 
			from  	man_estagio_usuario	d
			where   d.nm_usuario_acao 	= a.nm_usuario_exec
			and	decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil)	= decode(nvl(cd_perfil_ativo_w,0),0,decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil),cd_perfil_ativo_w))
	and	(exists	(select	1
			from	usuario_grupo_des	x
			where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_desenv_w = 'N')
	and	(exists	(select	1
			from	usuario_grupo_sup	x
			where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_suporte_w = 'N')
	order by
		a.nr_sequencia;
			
vetC01_w	C01%rowtype;

begin
if 	(nr_ordem_servico_p is not null) then
	begin
	/*select	max(nr_seq_estagio)
	into	nr_seq_estagio_w
	from	man_ordem_servico
	where	nr_sequencia	= nr_ordem_servico_p;*/

	select	nvl(max(ie_desenv),'N'),
		nvl(max(ie_suporte),'N')
	into	ie_desenv_w,
		ie_suporte_w
	from	man_estagio_processo
	where	nr_sequencia	= nr_seq_estagio_p;

	/*begin
	select	nm_usuario_exec
	into	nm_usuario_exec_w
	from	man_ordem_servico
	where	nr_sequencia 	= nr_ordem_servico_p;
	exception
	when others then
		nm_usuario_exec_w := wheb_mensagem_pck.get_texto(308632); -- N�o informado
	end;*/
	nm_usuario_exec_w	:= nm_usuario_exec_p;

	nm_usuario_exec_w := nvl(nm_usuario_exec_w, wheb_mensagem_pck.get_texto(308632)); -- N�o informado

	nr_seq_exec_w	:= 0;
	
	open C01;
	loop
	fetch C01 into	
		vetC01_w;
	exit when C01%notfound;
		begin
		if	(vetC01_w.dt_fim_execucao is null) then
			if	(vetC01_w.nr_seq_tipo_exec is not null) then
				select	max(a.ie_tipo_executor)
				into	ie_tipo_executor_w
				from	man_tipo_executor	a
				where	a.nr_sequencia	= vetC01_w.nr_seq_tipo_exec;
			
				if	(ie_tipo_executor_w = '1') then
					nr_seq_exec_sdt_exec_w	:= vetC01_w.nr_sequencia;
					nm_usuario_sdt_exec_w	:= vetC01_w.nm_usuario_exec;
				end if;
			else
				nr_seq_exec_sdt_sexec_w	:= vetC01_w.nr_sequencia;
				nm_usuario_sdt_sexec_w	:= vetC01_w.nm_usuario_exec;
			end if;
		else
			if	(vetC01_w.nr_seq_tipo_exec is not null) then
				select	max(a.ie_tipo_executor)
				into	ie_tipo_executor_w
				from	man_tipo_executor	a
				where	a.nr_sequencia	= vetC01_w.nr_seq_tipo_exec;
			
				if	(ie_tipo_executor_w = '1') then
					nr_seq_exec_dt_exec_w	:= vetC01_w.nr_sequencia;
					nm_usuario_dt_exec_w	:= vetC01_w.nm_usuario_exec;
				end if;
			else
				nr_seq_exec_dt_sexec_w	:= vetC01_w.nr_sequencia;
				nm_usuario_dt_sexec_w	:= vetC01_w.nm_usuario_exec;
			end if;
		end if;
		end;
	end loop;
	close C01;
	
	if	(nr_seq_exec_dt_exec_w is not null) then
		nr_seq_exec_w	:= nr_seq_exec_dt_exec_w;
		nm_usuario_w	:= nm_usuario_dt_exec_w;
	elsif	(nr_seq_exec_dt_sexec_w is not null) then
		nr_seq_exec_w	:= nr_seq_exec_dt_sexec_w;
		nm_usuario_w	:= nm_usuario_dt_sexec_w;
	elsif	(nr_seq_exec_sdt_exec_w is not null) then
		nr_seq_exec_w	:= nr_seq_exec_sdt_exec_w;
		nm_usuario_w	:= nm_usuario_sdt_exec_w;
	elsif	(nr_seq_exec_sdt_sexec_w is not null) then
		nr_seq_exec_w	:= nr_seq_exec_sdt_sexec_w;
		nm_usuario_w	:= nm_usuario_sdt_sexec_w;
	end if;
	
	/*begin
	select	max(a.nr_sequencia)
	into	nr_seq_exec_w
	from	man_tipo_executor c,
		man_ordem_servico_exec a
	where	a.nr_seq_tipo_exec	= c.nr_sequencia
	and	c.ie_tipo_executor 	= '1'
	and	a.nr_seq_ordem 		= nr_ordem_servico_p
	and	a.dt_fim_execucao is null
	--and	rownum < 2
	and  	exists (select	1 
			from  	man_estagio_usuario d
			where   d.nm_usuario_acao 	= a.nm_usuario_exec
			and	decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil) =
			decode(nvl(cd_perfil_ativo_w,0),0,decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil),cd_perfil_ativo_w))
	and	(exists	(select	1
			from	usuario_grupo_des x
			where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_desenv_w = 'N')
	and	(exists	(select	1
			from	usuario_grupo_sup x
			where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_suporte_w = 'N');
	exception
	when others then
		nr_seq_exec_w := 0;
	end;	
	
	
	if	(nvl(nr_seq_exec_w,0) = 0) then
		begin
		select	max(a.nr_sequencia)
		into	nr_seq_exec_w
		from	man_ordem_servico_exec a
		where	a.nr_seq_ordem 	= nr_ordem_servico_p
		and	a.dt_fim_execucao is null	
		and  	exists (select	1 
			from  	man_estagio_usuario d
			where   d.nm_usuario_acao 	= a.nm_usuario_exec
			and	decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil) =
				decode(nvl(cd_perfil_ativo_w,0),0,decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil),
				cd_perfil_ativo_w))
		and	(exists	(select	1
				from	usuario_grupo_des x
				where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_desenv_w = 'N')
		and	(exists	(select	1
				from	usuario_grupo_sup x
				where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_suporte_w = 'N');
		exception
		when others then
			nr_seq_exec_w := 0;
		end;
	end if;
	
	
	
	if	(nr_seq_exec_w = 0) then
		begin
		select	max(a.nr_sequencia)
		into	nr_seq_exec_w
		from	man_tipo_executor c,
			man_ordem_servico_exec a
		where	a.nr_seq_tipo_exec	= c.nr_sequencia
		and	c.ie_tipo_executor 	= '1'
		and	a.nr_seq_ordem 		= nr_ordem_servico_p
		--and	rownum < 2
		and  	exists (select	1 
				from  	man_estagio_usuario d
				where   d.nm_usuario_acao 	= a.nm_usuario_exec
				and	decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil) =
				decode(nvl(cd_perfil_ativo_w,0),0,decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil),cd_perfil_ativo_w))
		and	(exists	(select	1
				from	usuario_grupo_des x
				where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_desenv_w = 'N')
		and	(exists	(select	1
				from	usuario_grupo_sup x
				where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_suporte_w = 'N');
		exception
		when others then
			nr_seq_exec_w := 0;
		end;
	end if;
	
	if	(nvl(nr_seq_exec_w,0) = 0) then
		begin
		select	max(a.nr_sequencia)
		into	nr_seq_exec_w
		from	man_ordem_servico_exec a
		where	a.nr_seq_ordem 	= nr_ordem_servico_p
		--and	rownum < 2
		and  	exists (select	1 
			from  	man_estagio_usuario d
			where   d.nm_usuario_acao 	= a.nm_usuario_exec
			and	decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil) =
				decode(nvl(cd_perfil_ativo_w,0),0,decode(nvl(d.cd_perfil,0),0,nvl(cd_perfil_ativo_w,0),d.cd_perfil),
				cd_perfil_ativo_w))
		and	(exists	(select	1
				from	usuario_grupo_des x
				where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_desenv_w = 'N')
		and	(exists	(select	1
				from	usuario_grupo_sup x
				where	x.nm_usuario_grupo	= a.nm_usuario_exec) or ie_suporte_w = 'N');
		exception
		when others then
			nr_seq_exec_w := 0;
		end;
	end if;*/

	if 	(nvl(nr_seq_exec_w,0) <> 0) then
		begin
		ds_resultado_w	:= nm_usuario_w;
		end;
	elsif	(nvl(nr_seq_exec_w,0) = 0) then
		begin
		begin
		select	a.nr_sequencia
		into	nr_seq_exec_w
		from	man_ordem_servico_exec a
		where	a.nr_seq_ordem 	= nr_ordem_servico_p
		and	rownum < 2;
		exception
		when others then
			nr_seq_exec_w := 0;
		end;

		if 	(nr_seq_exec_w <> 0) then
			begin
			select	nm_usuario_exec
			into	nm_usuario_w
			from	man_ordem_servico_exec
			where	nr_seq_ordem 	= nr_ordem_servico_p
			and	nr_sequencia	= nr_seq_exec_w;
		
			ds_resultado_w	:= nm_usuario_w;
			end;
		else
			ds_resultado_w	:= nm_usuario_exec_w;	
		end if;
		end;
	end if;
	end;
end if;

return ds_resultado_w;

end obter_nome_usuario_exec2;
/