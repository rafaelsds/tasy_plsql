create or replace
function get_alerta_paciente_alert(
	nr_seq_pac_alerta_p	number,
	cd_estabelecimento_p	number,
	nm_usuario_p		varchar2)
	return varchar2 is
	
ds_alert_w		varchar2(2000);
nr_seq_tipo_alerta_w	alerta_paciente.nr_seq_tipo_alerta%type;
nr_seq_alerta_w 	alerta_paciente.nr_seq_alerta%type;
dt_alerta_w		alerta_paciente.dt_alerta%type;
ie_allow_free_text_w	tipo_alerta_atend.ie_allow_free_text%type;
	
begin

begin
select	substr(ds_alerta, 1, 2000),
	dt_alerta,
	nr_seq_tipo_alerta,
	nr_seq_alerta
into	ds_alert_w,
	dt_alerta_w,
	nr_seq_tipo_alerta_w,
	nr_seq_alerta_w
from	alerta_paciente
where	nr_sequencia = nr_seq_pac_alerta_p;

exception
when others then
	ds_alert_w := '';
	dt_alerta_w := null;
	nr_seq_tipo_alerta_w := null;
end;

if (nr_seq_tipo_alerta_w is not null) then
	begin

	select 	nvl(max(ie_allow_free_text),'Y')
	into	ie_allow_free_text_w
	from	tipo_alerta_atend
	where	nr_sequencia = nr_seq_tipo_alerta_w;

	if 	(ie_allow_free_text_w = 'N') then

		select 	max(substr(ds_alert, 1, 2000))
		into 	ds_alert_w
		from 	tipo_alerta_atend_option
		where 	nr_sequencia = nr_seq_alerta_w;

	end if;

	end;	
end if;

return pkg_date_formaters.to_varchar(dt_alerta_w, 'shortTime', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_p, nm_usuario_p), null) || ' - ' || ds_alert_w;

end get_alerta_paciente_alert;
/
