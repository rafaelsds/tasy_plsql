create or replace
function obter_dt_ref_rel_ordem_compra(nr_ordem_compra_p number, ie_tipo_data_p number, dt_inicio_p date, dt_fim_p date) return number is

retorno_w number;

begin

case ie_tipo_data_p 
	when 0 then
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o
		where o.dt_ordem_compra between dt_inicio_p
			and dt_fim_p
			and nr_ordem_compra_p = o.nr_ordem_compra;
	when 1 then
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o
		where o.dt_entrega between dt_inicio_p
			and dt_fim_p
			and nr_ordem_compra_p = o.nr_ordem_compra;
	when 2 then
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o
		where o.dt_liberacao between dt_inicio_p
			and dt_fim_p
			and nr_ordem_compra_p = o.nr_ordem_compra;
	when 3 then
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o
		where o.dt_aprovacao between dt_inicio_p
			and dt_fim_p
			and nr_ordem_compra_p = o.nr_ordem_compra;
	when 4 then
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o,
			 ordem_compra_item i,  
			 ordem_compra_item_entrega e
		where o.nr_ordem_compra    = i.nr_ordem_compra  
			and  i.nr_ordem_compra        = e.nr_ordem_compra  
			and  i.nr_item_oci            = e.nr_item_oci
			and e.dt_prevista_entrega between dt_inicio_p
			and dt_fim_p
			and nr_ordem_compra_p = o.nr_ordem_compra;
	when 5 then
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o,
			 ordem_compra_item i
		where o.nr_ordem_compra    = i.nr_ordem_compra
			and exists ( select	1 
				from	solic_compra_item_entrega x  
				where  x.nr_solic_compra = i.nr_solic_compra 
				and	   x.dt_entrega_solicitada between dt_inicio_p
				and dt_fim_p)
				and nr_ordem_compra_p = o.nr_ordem_compra;
	when 6 then
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o
		where o.dt_inclusao between  dt_inicio_p
			and dt_fim_p
			and nr_ordem_compra_p = o.nr_ordem_compra;
	when 7 then 
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o
		where exists ( select	1 
			from	   ordem_compra_venc x 
			where	   x.nr_ordem_compra = o.nr_ordem_compra 
			and	   x.dt_vencimento between dt_inicio_p
			and dt_fim_p)
			and nr_ordem_compra_p = o.nr_ordem_compra;
	when 8 then
		select count(o.nr_ordem_compra) 
		into retorno_w
		from ordem_compra o
		where  o.dt_baixa between dt_inicio_p
			and dt_fim_p
			and o.nr_seq_motivo_cancel is not null
			and nr_ordem_compra_p = o.nr_ordem_compra;
end case;

return retorno_w;
end obter_dt_ref_rel_ordem_compra;
/