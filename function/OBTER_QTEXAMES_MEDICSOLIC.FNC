CREATE OR REPLACE
FUNCTION Obter_Qtexames_Medicsolic
				(cd_medico_p		varchar,
				 cd_setor_atendimento_P	number,
				 ie_opcao_p		varchar,
				 dt_inicial_p		date,
				 dt_final_p		date)
				 return number is

/*
ie_opcao_p
 S - SUS
 O - OUTROS
*/

qt_total_w	number(10);

BEGIN

if	(ie_opcao_p = 'S') then
	
	begin
	select	count(b.nr_prescricao)
	into	qt_total_w
	from	atendimento_paciente	c,
		prescr_procedimento 	b,
		prescr_medica 		a
	where	a.nr_prescricao		= b.nr_prescricao
	and	a.nr_atendimento	= c.nr_atendimento
	and	b.cd_setor_atendimento	= cd_setor_atendimento_p
	and	a.cd_medico 		= cd_medico_p
	and	c.ie_tipo_convenio 	= 3
	and	a.dt_prescricao 	between dt_inicial_p and dt_final_p;
	end;

elsif	(ie_opcao_p = 'O') then	
	
	begin
	select	count(b.nr_prescricao)
	into	qt_total_w
	from	atendimento_paciente	c,
		prescr_procedimento	b,
		prescr_medica		a
	where	a.nr_prescricao		= b.nr_prescricao
	and	a.nr_atendimento	= c.nr_atendimento
	and	b.cd_setor_atendimento	= cd_setor_atendimento_p
	and	a.cd_medico		= cd_medico_p
	and	c.ie_tipo_convenio 	<> 3
	and	a.dt_prescricao 	between dt_inicial_p and dt_final_p;
	end;

end if;

return qt_total_w;

END Obter_Qtexames_Medicsolic;
/