create or replace
function obter_se_valor_coluna	(nm_tabela_p	varchar2,
				nm_coluna_p	varchar2,
				nm_usuario_p	varchar2)
				return varchar2 is

ds_comando_w	varchar2(240);
qt_valor_w	number(15,4);
ie_valor_w	varchar2(1) := 'N';

begin
if	(nm_tabela_p is not null) and
	(nm_coluna_p is not null) then
	ds_comando_w := 'select count(*) from '||nm_tabela_p ||' where ' || nm_coluna_p || ' is not null and nm_usuario = ' || chr(39) || nm_usuario_p || chr(39);

	obter_valor_dinamico(ds_comando_w, qt_valor_w);
	if	(qt_valor_w > 0) then
		ie_valor_w := 'S';
	end if;
end if;

return ie_valor_w;

end obter_se_valor_coluna;
/