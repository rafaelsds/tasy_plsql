create or replace
function pls_obter_dados_prestador
			(	nr_seq_prestador_p	Number,
				ie_opcao_p		Varchar2)
				return varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter dados do prestador
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	TR - Tipo de rela��o
	TP - Tipo de prestador
	N  - Nome do prestador
	NF - Nome fantasia
	NGM - Nome Guia M�dico
	NGMP - Nome Guia M�dico portal
	SGCM - Sigla do conselho do m�dico
	CGC - CGC
	CPF - CPF
	CNES - C�digo nacional de estabelecimento de sa�de
	CP - Condi��o de Pagamento
	E - Estabelecimento do prestador
	DP - Quantidade de dias de protocolos para excluir
	B - Banco
	NB - Nome do banco	
	A - Ag�nca banc�ria
	DA - D�gito da Ag�ncia
	ACD - Ag�ncia com d�gito (Separados por �fen)
	C - Conta banc�ria
	DC - D�gito da Conta
	CCD - Conta com d�gito (Separados por �fen)
	FAB - Forma de autentica��o do benefici�rio no OPSW - Autoriza��o ( Solicita��o de autoriza��o)
	CLA - Classifica��o
	NRCLA - Seq da Classifica��o
	M - Email
	CONS - Conselho profissional
	TPR - Tipo prestador PF ou PJ
	PF - C�digo pessoa f�sica
	FP - Forma de pagamento	
	DS - Raz�o social
	MA - Municipio de atua��o
	MT - Matricula do cooperado
	TC - Telefone Comercial/Contato
	TC2 - Telefone 2
	CD - C�digo prestador
	AC - Alto Custo
	NC - N�mero do contrato
	NA - N�vel acredita��o
	IA - Istitui��o acredita��o
	S - Situa��o
	F - Filial
	CEL - Celular
	A400 - C�d A400 (cd_prest_a400)
	IBGE - C�d Munic�pio IBGE 
	IBGEC - C�d Munic�pio IBGE complementar, seguindo a escala de prioridades
		1 - Endere�o comercial da pessoa fisica
		2 - Endere�o residencial da pessoa fisica
		3 - Endere�o do prestador.
	DTE - Data de Exclus�o 
	DSMN - Nome do munic�pio IBGE
	NAP - Nome abreviado prestador
	CCC - CRM ou CPF ou CNPJ
	TE - Tipo endere�o
	UF - Sigla estado prestador
	UFTC - Sigla do estado do prestador conforme tipo de complemento
	DGM - Descri��o do prestador/m�dico para o guia m�dico
	EMC - Email do cooperado
*/					
				
ie_tipo_relacao_w		Varchar2(2);
cd_cgc_w			Varchar2(50);
ds_retorno_w			Varchar2(4000)	:= null;
cd_condicao_pagamento_w		Number(10);
cd_estabelecimento_w		Number(4);
qt_dias_protocolo_w		Number(3);
cd_banco_w			Varchar2(20);
cd_agencia_w			Varchar2(20);
nr_conta_w			Varchar2(50);
cd_pessoa_fisica_w		Varchar2(10);
ds_tipo_prestador_w		Varchar2(255);
ie_forma_pagto_w		varchar2(10);
ds_municipio_ibge_w		varchar2(100);	
cd_prestador_w			varchar2(30);
ie_prest_alto_custo_w		varchar2(2);
ie_situacao_w			varchar2(1);
nr_seq_classif_prestador_w	number(10);
ie_grau_instrucao_w		number(2);
ie_digito_agencia_w		varchar2(2);
nr_digito_conta_w		varchar2(2);
nr_seq_conselho_w		pessoa_fisica.nr_seq_conselho%type;
ie_tipo_complemento_w		compl_pessoa_fisica.ie_tipo_complemento%type;
ie_tipo_endereco_w		pls_prestador.ie_tipo_endereco%type;

Cursor C01 is
	select	ds_tipo_prestador
	from	pls_tipo_prestador	b,
		pls_prestador_tipo	a
	where	a.nr_seq_tipo		= b.nr_sequencia
	and	b.cd_estabelecimento	= cd_estabelecimento_w
	and	sysdate between nvl(a.dt_inicio_vigencia, sysdate - 1) and nvl(a.dt_fim_vigencia, sysdate + 1)
	and	a.nr_seq_prestador	= nr_seq_prestador_p;
	
Cursor c02 is
	select	substr(obter_desc_municipio_ibge(cd_municipio_ibge),1,255)
	from	pls_prestador_area a
	where	a.nr_seq_prestador	= nr_seq_prestador_p
	and	a.cd_municipio_ibge is not null;

begin
/* N�O FAZER SELECT AQUI FORA ! ! ! ! */


/*Diego OS 411152 - Performance - S� ser� realizado o select abaixo nas op��es em que este deve ser utilizado.*/
if	(ie_opcao_p in ('NB', 'B', 'A', 'C', 'DA', 'DC', 'ACD', 'CCD')) then
	select	max(cd_banco),
		max(cd_agencia_bancaria),
		max(nr_conta)
	into	cd_banco_w,
		cd_agencia_w,
		nr_conta_w
	from	pls_prestador_pagto
	where	nr_seq_prestador	= nr_seq_prestador_p
	and	sysdate between dt_inicio_vigencia_ref and dt_fim_vigencia_ref;
	
	if	(cd_banco_w is not null) and
		(cd_agencia_w is not null) and
		(nr_conta_w is not null) then
		
		select	max(a.cd_cgc),
			max(a.cd_pessoa_fisica)
		into	cd_cgc_w,
			cd_pessoa_fisica_w
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_p;
		
		if	(cd_cgc_w is not null) then
		
			begin
			select	ie_digito_agencia,
				nr_digito_conta
			into	ie_digito_agencia_w,
				nr_digito_conta_w
			from	pessoa_juridica_conta
			where	cd_cgc			= cd_cgc_w
			and	cd_banco		= cd_banco_w
			and	cd_agencia_bancaria	= cd_agencia_w
			and	nr_conta_w		= nr_conta_w
			and	rownum = 1;	
			exception
			when no_data_found then
				ie_digito_agencia_w	:= null;
				nr_digito_conta_w	:= null;
			end;
		elsif	(cd_pessoa_fisica_w is not null) then
			begin
			select	ie_digito_agencia,
				nr_digito_conta
			into	ie_digito_agencia_w,
				nr_digito_conta_w
			from	pessoa_fisica_conta
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	cd_banco		= cd_banco_w
			and	cd_agencia_bancaria	= cd_agencia_w
			and	nr_conta_w		= nr_conta_w
			and	rownum = 1;	
			exception
			when no_data_found then
				ie_digito_agencia_w	:= null;
				nr_digito_conta_w	:= null;
			end;
			
		end if;
	end if;
	
	if	(cd_banco_w is null) and
		(cd_agencia_w is null) and
		(nr_conta_w is null) then
		select	max(a.cd_cgc),
			max(a.cd_pessoa_fisica)
		into	cd_cgc_w,
			cd_pessoa_fisica_w
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_p;

		if	(cd_cgc_w is not null) then
			begin
			select	cd_banco,
				cd_agencia_bancaria,
				nr_conta,
				ie_digito_agencia,
				nr_digito_conta
			into	cd_banco_w,
				cd_agencia_w,
				nr_conta_w,
				ie_digito_agencia_w,
				nr_digito_conta_w
			from	pessoa_juridica_conta
			where	ie_prestador_pls	= 'S'
			and	cd_cgc			= cd_cgc_w
			and	rownum = 1;	
			exception
			when no_data_found then
				cd_banco_w	:= null;
				cd_agencia_w	:= null;
				nr_conta_w	:= null;
			end;
		elsif	(cd_pessoa_fisica_w is not null) then
			begin
			select	cd_banco,
				cd_agencia_bancaria,
				nr_conta,
				ie_digito_agencia,
				nr_digito_conta
			into	cd_banco_w,
				cd_agencia_w,
				nr_conta_w,
				ie_digito_agencia_w,
				nr_digito_conta_w
			from	pessoa_fisica_conta
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	rownum = 1;
			exception
			when no_data_found then
				cd_banco_w	:= null;
				cd_agencia_w	:= null;
				nr_conta_w	:= null;
			end;
		end if;
	end if;
	
	if	(ie_digito_agencia_w is null) and
		(nr_digito_conta_w is null) then
		
		select	max(a.cd_cgc),
			max(a.cd_pessoa_fisica)
		into	cd_cgc_w,
			cd_pessoa_fisica_w
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_p;

		if	(cd_cgc_w is not null) then
			begin
			select	ie_digito_agencia,
				nr_digito_conta
			into	ie_digito_agencia_w,
				nr_digito_conta_w
			from	pessoa_juridica_conta
			where	ie_prestador_pls	= 'S'
			and	cd_cgc			= cd_cgc_w
			and	ie_situacao		= 'A'
			and	rownum = 1;
			exception
			when no_data_found then
				ie_digito_agencia_w	:= '';
				nr_digito_conta_w	:= '';
			end;
		elsif	(cd_pessoa_fisica_w is not null) then
			begin
			select	ie_digito_agencia,
				nr_digito_conta
			into	ie_digito_agencia_w,
				nr_digito_conta_w
			from	pessoa_fisica_conta
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	ie_situacao		= 'A'
			and	rownum = 1;
			exception
			when no_data_found then
				ie_digito_agencia_w	:= '';
				nr_digito_conta_w	:= '';
			end;
		end if;
		
	end if;
end if;

if	(ie_opcao_p = 'TR') then
	select	ie_tipo_relacao
	into	ie_tipo_relacao_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;

	select	substr(ds_valor_dominio,1,255)
	into	ds_retorno_w
	from	valor_dominio
	where	cd_dominio	= 1668
	and	vl_dominio	= ie_tipo_relacao_w;
elsif	(ie_opcao_p = 'TP') then
	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	select	max(b.ds_tipo_prestador)
	into	ds_retorno_w
	from	pls_tipo_prestador b,
		pls_prestador a
	where	a.nr_seq_tipo_prestador	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_prestador_p;

	open C01;
	loop
	fetch C01 into
		ds_tipo_prestador_w;
	exit when C01%notfound;
		begin
		if	(ds_retorno_w is not null) then
			ds_retorno_w	:= substr(ds_retorno_w || ', ' || ds_tipo_prestador_w,1,4000);
		else
			ds_retorno_w	:= substr(ds_tipo_prestador_w,1,4000);
		end if;
		end;
	end loop;
	close C01;

	ds_retorno_w	:= substr(ds_retorno_w,1,4000);
elsif	(ie_opcao_p = 'N') then
	select	max(a.nm_interno)
	into	ds_retorno_w
	from	pls_prestador a
	where	nr_sequencia	= nr_seq_prestador_p;

	if	(ds_retorno_w is null) then
		select	max(a.cd_cgc),
			max(a.cd_pessoa_fisica)
		into	cd_cgc_w,
			cd_pessoa_fisica_w
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_p;
		
		if	(cd_cgc_w is not null)	then -- Diether OS 368360 - Alterado para melhoria de desempenho antes usava obter_nome_pf_pj
			select	ds_razao_social
			into	ds_retorno_w
			from	pessoa_juridica
			where	cd_cgc	= cd_cgc_w;
		elsif	(cd_pessoa_fisica_w is not null) then
			select	nm_pessoa_fisica
			into	ds_retorno_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		end if;
	end if;

elsif	(ie_opcao_p = 'CEL') then
	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica)
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;
	
	if	(cd_cgc_w is not null)	then
		select	nr_telefone
		into	ds_retorno_w
		from	pessoa_juridica
		where	cd_cgc	= cd_cgc_w;
	elsif	(cd_pessoa_fisica_w is not null) then
		select	nr_telefone_celular
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
	end if;
elsif	(ie_opcao_p = 'NGM') then
	select	max(nvl(a.nm_adic_guia_medico,a.nm_interno))
	into	ds_retorno_w
	from	pls_prestador a
	where	nr_sequencia	= nr_seq_prestador_p;

	if	(ds_retorno_w is null) then
		select	max(a.cd_cgc),
			max(a.cd_pessoa_fisica)
		into	cd_cgc_w,
			cd_pessoa_fisica_w
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_p;
		
		if	(cd_cgc_w is not null)	then -- Diether OS 368360 - Alterado para melhoria de desempenho antes usava obter_nome_pf_pj
			select	ds_razao_social
			into	ds_retorno_w
			from	pessoa_juridica
			where	cd_cgc	= cd_cgc_w;
		elsif	(cd_pessoa_fisica_w is not null) then
			select	nm_pessoa_fisica
			into	ds_retorno_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		end if;
	end if;
elsif	(ie_opcao_p = 'NGMP') then
	select	max(nvl(a.nm_adic_guia_medico,a.nm_interno))
	into	ds_retorno_w
	from	pls_prestador a
	where	nr_sequencia	= nr_seq_prestador_p;

	if	(ds_retorno_w is null) then
		select	max(a.cd_cgc),
			max(a.cd_pessoa_fisica)
		into	cd_cgc_w,
			cd_pessoa_fisica_w
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_p;
		
		if	(cd_cgc_w is not null)	then
			select	nvl(nm_fantasia,ds_razao_social) 
			into	ds_retorno_w
			from	pessoa_juridica
			where	cd_cgc	= cd_cgc_w;
		elsif	(cd_pessoa_fisica_w is not null) then
			select	nm_pessoa_fisica
			into	ds_retorno_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		end if;
	end if;
elsif	(ie_opcao_p = 'SGCM') then	
	select	max(a.cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;
	
	if	(cd_pessoa_fisica_w is not null) then
		select	max(nr_seq_conselho)
		into	nr_seq_conselho_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

		if	(nr_seq_conselho_w is not null) then
			select	max(sg_conselho)
			into	ds_retorno_w
			from	conselho_profissional
			where	nr_sequencia		= nr_seq_conselho_w;
		end if;
	end if;
elsif	(ie_opcao_p = 'NF') then
	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica)
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	if	(cd_pessoa_fisica_w is not null) then
		select	max(a.nm_pessoa_fisica)
		into	ds_retorno_w
		from	pessoa_fisica	a
		where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
	elsif	(cd_cgc_w is not null) then
		select	max(a.nm_fantasia)
		into	ds_retorno_w
		from	pessoa_juridica	a
		where	a.cd_cgc	= cd_cgc_w;
	end if;
elsif	(ie_opcao_p = 'CNES') then
	select	substr(obter_dados_pf_pj(cd_pessoa_fisica, cd_cgc, 'CNES'),1,20)
	into	ds_retorno_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'CGC') then
	select	max(a.cd_cgc)
	into	cd_cgc_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	ds_retorno_w	:= cd_cgc_w;
elsif	(ie_opcao_p = 'CPF') then
	select	obter_dados_pf(cd_pessoa_fisica, 'CPF')
	into	ds_retorno_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'CP') then
	select	max(a.cd_condicao_pagamento)
	into	cd_condicao_pagamento_w
	from	pls_prestador_pagto a
	where	a.nr_seq_prestador	= nr_seq_prestador_p;

	ds_retorno_w	:= to_char(cd_condicao_pagamento_w);
elsif	(ie_opcao_p = 'FP')then
	select	max(a.ie_forma_pagto)
	into	ie_forma_pagto_w
	from	pls_prestador_pagto a
	where	a.nr_seq_prestador	= nr_seq_prestador_p;

	ds_retorno_w	:= ie_forma_pagto_w;	
elsif	(ie_opcao_p = 'E') then
	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	ds_retorno_w	:= cd_estabelecimento_w;
elsif	(ie_opcao_p = 'DP') then
	select	max(a.qt_dias_protocolo)
	into	qt_dias_protocolo_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	if	(qt_dias_protocolo_w is null) then
		select	nvl(max(qt_dias_protocolo), 30)
		into	qt_dias_protocolo_w
		from	pls_parametros;
	end if;
	
	ds_retorno_w 	:= qt_dias_protocolo_w;
elsif	(ie_opcao_p = 'B') then	
	ds_retorno_w	:= cd_banco_w;
elsif	(ie_opcao_p = 'NB') then	
	select	max(ds_banco)
	into	ds_retorno_w
	from	banco
	where	cd_banco	= cd_banco_w;
elsif	(ie_opcao_p = 'A') then
	ds_retorno_w	:= cd_agencia_w;
elsif	(ie_opcao_p = 'DA') then
	ds_retorno_w	:= ie_digito_agencia_w;
elsif	(ie_opcao_p = 'ACD') then
	if	(ie_digito_agencia_w is null) then
		ds_retorno_w	:= cd_agencia_w;
	else
		ds_retorno_w	:= cd_agencia_w || '-' || ie_digito_agencia_w;
	end if;
elsif	(ie_opcao_p = 'C') then
	ds_retorno_w	:= nr_conta_w;
elsif	(ie_opcao_p = 'DC') then
	ds_retorno_w	:= nr_digito_conta_w;
elsif	(ie_opcao_p = 'CCD') then
	if	(nr_digito_conta_w is null) then
		ds_retorno_w	:= nr_conta_w;
	else
		ds_retorno_w	:= nr_conta_w || '-' || nr_digito_conta_w;
	end if;
elsif	(ie_opcao_p = 'FAB') then
	select	ie_tipo_validacao_aut_solic
	into	ds_retorno_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'CLA') then
	select	max(a.nr_seq_classificacao)
	into	nr_seq_classif_prestador_w
	from	pls_prestador	a
	where	a.nr_sequencia		= nr_seq_prestador_p;
	
	if	(nr_seq_classif_prestador_w is not null) then
		select	b.ds_classificacao
		into	ds_retorno_w
		from	pls_classif_prestador	b
		where	b.nr_sequencia	= nr_seq_classif_prestador_w;
	end if;
elsif	(ie_opcao_p = 'NRCLA') then
	select	max(a.nr_seq_classificacao)
	into	ds_retorno_w
	from	pls_prestador a
	where	a.nr_sequencia		= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'M') then
	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica)
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;
	
	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	ds_retorno_w	:= obter_dados_pf_pj_estab(cd_estabelecimento_w,cd_pessoa_fisica_w, cd_cgc_w, 'M');
elsif	(ie_opcao_p = 'CONS') then
	select	max(a.cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	select	substr(obter_crm_medico(cd_pessoa_fisica_w),1,255)
	into	ds_retorno_w
	from  	dual;
elsif	(ie_opcao_p = 'TPR') then
	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica)
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	if	(cd_pessoa_fisica_w is not null )then
		ds_retorno_w	:= 'PF';
	else
		ds_retorno_w	:= 'PJ';
	end if;
elsif	(ie_opcao_p = 'PF') then
	select	max(a.cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	ds_retorno_w	:= cd_pessoa_fisica_w;
elsif	(ie_opcao_p = 'DS') then
	select	b.ds_razao_social--substr(obter_dados_pf_pj(null,cd_cgc,'N'),1,255)
	into	ds_retorno_w
	from	pessoa_juridica	b,
		pls_prestador	a
	where	b.cd_cgc	= a.cd_cgc
	and	a.nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'MA') then
	open C02;
	loop
	fetch C02 into	
		ds_municipio_ibge_w;
	exit when C02%notfound;
		begin
		if	(ds_retorno_w is null) then
			ds_retorno_w	:= ds_municipio_ibge_w;
		else
			ds_retorno_w	:= ds_retorno_w || ', ' || ds_municipio_ibge_w;
		end if;
		end;
	end loop;
	close C02;
elsif	(ie_opcao_p = 'MT') then
	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica)
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	select	max(a.cd_matricula)
	into	ds_retorno_w
	from	pls_cooperado a
	where	((a.cd_cgc  = cd_cgc_w and cd_cgc_w is not null)
	or	(a.cd_pessoa_fisica  = cd_pessoa_fisica_w and cd_pessoa_fisica_w is not null));
elsif	(ie_opcao_p = 'TC') then
	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica)
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;
	
	if	(cd_cgc_w is not null) then
		begin
		select	'('||nr_ddd_telefone||')'||nr_telefone
		into	ds_retorno_w
		from	pessoa_juridica_compl
		where	cd_cgc			= cd_cgc_w
		and	ie_tipo_complemento	= 3;
		exception
		when others then
			ds_retorno_w	:= '';
		end;
	elsif	(cd_pessoa_fisica_w is not null) then
		begin
		select	'('||nr_ddd_telefone||')'||nr_telefone
		into	ds_retorno_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	ie_tipo_complemento	= 2;
		exception
		when others then
			ds_retorno_w	:= '';
		end;
	end if;
elsif	(ie_opcao_p = 'TC2') then
	select	nr_telefone_dois
	into	ds_retorno_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'CD') then
	select	max(a.cd_prestador)
	into	cd_prestador_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	ds_retorno_w	:= cd_prestador_w;
elsif	(ie_opcao_p = 'AC') then
	begin
	select	nvl(a.ie_prestador_alto_custo,'N')
	into	ds_retorno_w
	from	pls_prestador	a
	where	a.nr_sequencia	= nr_seq_prestador_p;
	exception
	when others then
		ds_retorno_w	:= '';
	end;
elsif	(ie_opcao_p = 'NC') then
	select	max(a.nr_seq_contrato)
	into	ds_retorno_w
	from	pls_prestador	a
	where	a.nr_sequencia = nr_seq_prestador_p;
elsif	(ie_opcao_p = 'NA') then
	select	substr(obter_valor_dominio(4233,a.ie_nivel_acreditacao_ptu),1,255)
	into	ds_retorno_w
	from	pls_prestador	a
	where	a.nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'IA') then
	select	substr(obter_valor_dominio(4232,a.ie_instituicao_acred_ptu),1,255)
	into	ds_retorno_w
	from	pls_prestador	a
	where	a.nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'S') then
	select	max(a.ie_situacao)
	into	ie_situacao_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	ds_retorno_w	:= ie_situacao_w;
	
elsif	(ie_opcao_p = 'GI') then
	select	max(a.cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;
	
	if	(cd_pessoa_fisica_w is not null) then
		select	max(ie_grau_instrucao)
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	end if;
	
elsif	(ie_opcao_p = 'GP') then
	select	max(a.cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;
	
	if	(cd_pessoa_fisica_w is not null) then
		select	max(ie_grau_instrucao)
		into	ie_grau_instrucao_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		if	(ie_grau_instrucao_w in (12,13,5,6)) then
			ds_retorno_w := 'S';
		else
			ds_retorno_w := 'N';
		end if;
	end if;
	
elsif	(ie_opcao_p = 'F') then
	select	nvl(ie_filial,'N')
	into	ds_retorno_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_opcao_p = 'A400') then
	begin
		select	cd_prest_a400
		into	ds_retorno_w
		from	pls_prestador
		where	nr_sequencia = nr_seq_prestador_p;
	exception
	when no_data_found then
		ds_retorno_w := null;
	end;
elsif	(ie_opcao_p = 'IBGE') then
	begin
		select	c.cd_municipio
		into	ds_retorno_w
		from	(select	nvl(	(select	max(b.CD_MUNICIPIO_IBGE)
					from	pessoa_fisica b
					where	b.cd_pessoa_fisica = a.cd_pessoa_fisica),
					(select	max(b.CD_MUNICIPIO_IBGE)
					from	COMPL_PESSOA_FISICA b
					where	b.cd_pessoa_fisica = a.cd_pessoa_fisica)) cd_municipio
			from	pls_prestador a
			where	a.nr_sequencia = nr_seq_prestador_p
			Union     
			select	nvl(	(select	max(b.CD_MUNICIPIO_IBGE)
					from	PESSOA_JURIDICA b
					where	b.cd_cgc = a.cd_cgc),
					(select	max(b.CD_MUNICIPIO_IBGE)
					from	PESSOA_JURIDICA_COMPL b
					where	b.cd_cgc = a.cd_cgc)) cd_municipio
			from	pls_prestador a
			where	a.nr_sequencia = nr_seq_prestador_p) c
		where	c.cd_municipio is not null
		and	rownum <= 1
		group by c.cd_municipio;
	exception
	when others then
		ds_retorno_w := '';
	end;
elsif	(ie_opcao_p = 'IBGEC') then
	begin
		select	c.cd_municipio
		into	ds_retorno_w
		from	(select	nvl(	nvl(	(select	max(b.cd_municipio_ibge)
						from	compl_pessoa_fisica b
						where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
						and	ie_tipo_complemento	= '2'),
						(select	max(b.cd_municipio_ibge)
						from	compl_pessoa_fisica b
						where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
						and	ie_tipo_complemento	= '1')),						
					(select	max(b.cd_municipio_ibge)
					from	pessoa_fisica b
					where	b.cd_pessoa_fisica = a.cd_pessoa_fisica)) cd_municipio
			from	pls_prestador a
			where	a.nr_sequencia = nr_seq_prestador_p
			union     
			select	nvl(	(select	max(b.cd_municipio_ibge)
					from	pessoa_juridica b
					where	b.cd_cgc = a.cd_cgc),
					(select	max(b.cd_municipio_ibge)
					from	pessoa_juridica_compl b
					where	b.cd_cgc = a.cd_cgc)) cd_municipio
			from	pls_prestador a
			where	a.nr_sequencia = nr_seq_prestador_p) c
		where	c.cd_municipio is not null
		and	rownum <= 1
		group by c.cd_municipio;
	exception
	when others then
		ds_retorno_w := '';
	end;
elsif	(ie_opcao_p = 'DSMN') then
	begin
		select	obter_desc_municipio_ibge(a.cd_municipio)
		into	ds_retorno_w
		from	(
				select	nvl(b.cd_municipio_ibge,c.cd_municipio_ibge) cd_municipio
				from	pls_prestador		a,
					pessoa_fisica		b,
					compl_pessoa_fisica	c
				where	a.nr_sequencia = nr_seq_prestador_p
				and	a.cd_pessoa_fisica is not null
				and	b.cd_pessoa_fisica = a.cd_pessoa_fisica
				and	c.cd_pessoa_fisica = b.cd_pessoa_fisica(+)
				union 
				select	nvl(b.cd_municipio_ibge,c.cd_municipio_ibge) cd_municipio
				from	pls_prestador		a,
					pessoa_juridica		b,
					pessoa_juridica_compl	c
				where	a.nr_sequencia = nr_seq_prestador_p
				and	a.cd_cgc is not null
				and	b.cd_cgc = a.cd_cgc
				and	c.cd_cgc = b.cd_cgc(+)
				order by cd_municipio
			) a
		where	a.cd_municipio is not null
		and	rownum <= 1
		group by a.cd_municipio;
	exception
	when others then
		ds_retorno_w := '';
	end;
elsif	(ie_opcao_p = 'DTE') then
	select	max(dt_exclusao)
	into	ds_retorno_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_prestador_p;
	
elsif	(ie_opcao_p = 'NAP') then
	select	cd_pessoa_fisica,
		cd_cgc
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_prestador_p;
	
	if	(cd_cgc_w is not null) then
		select	nm_fantasia
		into	ds_retorno_w
		from	pessoa_juridica
		where	cd_cgc = cd_cgc_w;
	else
		select	nm_abreviado
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	end if;
elsif	(ie_opcao_p = 'CCC') and
	(nr_seq_prestador_p is not null) then
	select	a.cd_pessoa_fisica,
		a.cd_cgc
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	if	(cd_pessoa_fisica_w is not null) then	
		select	substr(obter_crm_medico(cd_pessoa_fisica_w),1,255)
		into	ds_retorno_w
		from  	dual;
		
		if	(ds_retorno_w is null) then
			select	nr_cpf
			into	ds_retorno_w
			from	pessoa_fisica a
			where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
		end if;
	else
		ds_retorno_w := cd_cgc_w;
	end if;
elsif	(ie_opcao_p = 'TE') and
	(nr_seq_prestador_p is not null) then
	begin
		select	ie_tipo_endereco
		into	ds_retorno_w
		from	pls_prestador
		where 	nr_sequencia	= nr_seq_prestador_p;
	exception
	when others then
		ds_retorno_w 	:= null;
	end;	
elsif	(ie_opcao_p = 'UF') and
	(nr_seq_prestador_p is not null) then
	begin
		select	c.sg_uf
		into	ds_retorno_w
		from	(	select	(	select	max(b.sg_estado)
						from	COMPL_PESSOA_FISICA b
						where	b.cd_pessoa_fisica = a.cd_pessoa_fisica) sg_uf
				from	pls_prestador a
				where	a.nr_sequencia = nr_seq_prestador_p
				Union     
				select	nvl(	(select	max(b.sg_estado)
						from	PESSOA_JURIDICA b
						where	b.cd_cgc = a.cd_cgc),
						(select	max(b.sg_estado)
						from	PESSOA_JURIDICA_COMPL b
						where	b.cd_cgc = a.cd_cgc)) sg_uf
				from	pls_prestador a
				where	a.nr_sequencia = nr_seq_prestador_p) c
		where	c.sg_uf is not null
		and	rownum <= 1
		group by c.sg_uf;
	exception
	when others then
		ds_retorno_w := '';
	end;
elsif	(ie_opcao_p = 'UFTC') and
	(nr_seq_prestador_p is not null) then
	
	select	cd_cgc,
		cd_pessoa_fisica,
		ie_tipo_endereco
	into	cd_cgc_w,
		cd_pessoa_fisica_w,
		ie_tipo_endereco_w
	from	pls_prestador
	where	nr_Sequencia = nr_seq_prestador_p;
	
	if	(cd_cgc_w is not null) then
		select	max(b.sg_estado)
		into	ds_retorno_w
		from	pessoa_juridica b
		where	b.cd_cgc = cd_cgc_w;
		
		if	(ds_retorno_w is null) then
			select	max(b.sg_estado)
			into	ds_retorno_w
			from	pessoa_juridica_compl b
			where	b.cd_cgc = cd_cgc_w;
		end if;
	elsif	(cd_pessoa_fisica_w is not null) then
		
		if	(ie_tipo_endereco_w is not null) then
			if	(ie_tipo_endereco_w = 'PFC') then
				ie_tipo_complemento_w := 2;
			elsif	(ie_tipo_endereco_w = 'PFR') then
				ie_tipo_complemento_w := 1;
			elsif	(ie_tipo_endereco_w = 'PFA') then
				ie_tipo_complemento_w := 9;
			end if;
		else
			ie_tipo_complemento_w := 2;
		end if;
		
		select	max(b.sg_estado)
		into	ds_retorno_w
		from	compl_pessoa_fisica b
		where	b.cd_pessoa_fisica = cd_pessoa_fisica_w
		and	b.ie_tipo_complemento = ie_tipo_complemento_w;
		
		if	(ds_retorno_w is null) then
			select	max(b.sg_estado)
			into	ds_retorno_w
			from	compl_pessoa_fisica b
			where	b.cd_pessoa_fisica = cd_pessoa_fisica_w;
		end if;
	end if;
	
elsif	(ie_opcao_p = 'DGM') then
	select	max(nm_busca_guia_medico)
	into	ds_retorno_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_prestador_p;
	
elsif	(ie_opcao_p = 'EMC') then
	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica)
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;
	
	if	(cd_pessoa_fisica_w is not null) then
		select	substr(pls_obter_dados_cooperado(max(nr_sequencia),'EM'),1,255)
		into	ds_retorno_w
		from	pls_cooperado
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
	elsif	(cd_cgc_w is not null) then
		select	substr(pls_obter_dados_cooperado(max(nr_sequencia),'EM'),1,255)
		into	ds_retorno_w
		from	pls_cooperado
		where	cd_cgc	= cd_cgc_w;
	end if;
end if;	

return ds_retorno_w;

end pls_obter_dados_prestador;
/