create or replace
function pls_relat_4370_obter_qt_faixa
		(	nr_seq_plano_preco_p	number,
			nr_titulo_p		number)
			return number is

qt_segurado_w		number(10);
			
begin

select	count(distinct i.nr_sequencia)
into	qt_segurado_w
from	pls_mensalidade_segurado 	b,
	pls_mensalidade 		c,
	pls_lote_mensalidade 		d,
	titulo_receber 			e,
	pls_plano_preco 		f,
	pls_tabela_preco 		g,
	pls_plano 			h,
	pls_segurado 			i
where	c.nr_sequencia	= b.nr_seq_mensalidade
and	d.nr_sequencia	= c.nr_seq_lote
and	c.nr_sequencia	= e.nr_seq_mensalidade
and	g.nr_sequencia	= f.nr_seq_tabela 
and	h.nr_sequencia	= g.nr_seq_plano
and	h.nr_sequencia	= i.nr_seq_plano
and	g.nr_sequencia 	= i.nr_seq_tabela
and	i.nr_sequencia 	= b.nr_seq_segurado
and	f.nr_sequencia	= nr_seq_plano_preco_p
and	e.nr_titulo	= nr_titulo_p
and	obter_idade_pf(i.cd_pessoa_fisica,sysdate,'A') between f.qt_idade_inicial and f.qt_idade_final
and	(i.dt_rescisao is null or trunc(i.dt_rescisao, 'month') > c.dt_referencia);

return	qt_segurado_w;

end pls_relat_4370_obter_qt_faixa;
/