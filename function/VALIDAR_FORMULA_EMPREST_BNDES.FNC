create or replace function	validar_formula_emprest_BNDES(DS_FORMULA_P varchar2)
return CHAR is

query_execucao_p varchar(4000);
recebe_p varchar(4000);
ie_erro_p char;

begin

  ie_erro_p	:= 'N';
  
  if	(ds_formula_p is not null) then
    select REGEXP_REPLACE(ds_formula_p, '(\#:I:#\S*?\!|\#\S*?\@)', '1 ') into query_execucao_p from dual;
    begin
      EXECUTE IMMEDIATE 'select ' || query_execucao_p || ' from dual' into recebe_p;
      exception
      when others then
        ie_erro_p	:= 'S';
    end;
  end if;

return ie_erro_p;

end validar_formula_emprest_BNDES;
/