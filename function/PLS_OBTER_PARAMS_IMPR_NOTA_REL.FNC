create or replace
function pls_obter_params_impr_nota_rel(
		nr_seq_mensalidade_lista_p	varchar2)
 	return varchar2 is

nr_seq_mensalidade_lista_w	varchar2(2000);
nr_seq_mensalidade_w	number(10);
nr_nota_mensalidade_w	varchar2(10);
nr_notas_mensalidades_w	varchar2(2000);

begin

nr_seq_mensalidade_lista_w	:= nr_seq_mensalidade_lista_p;

while	(nr_seq_mensalidade_lista_w is not null) loop
	begin
	nr_seq_mensalidade_w	:= substr(nr_seq_mensalidade_lista_w, 1, instr(nr_seq_mensalidade_lista_w, ',') - 1);
	nr_seq_mensalidade_lista_w	:= substr(nr_seq_mensalidade_lista_w, instr(nr_seq_mensalidade_lista_w, ',') + 1, length(nr_seq_mensalidade_lista_w));

	select	substr(pls_obter_nota_mensalidade(nr_sequencia), 1, 10)
	into	nr_nota_mensalidade_w
	from	pls_mensalidade
	where	nr_sequencia = nr_seq_mensalidade_w;

	nr_notas_mensalidades_w	:= nr_notas_mensalidades_w || nr_nota_mensalidade_w || ',';
	end;
end loop;

nr_notas_mensalidades_w	:= substr(nr_notas_mensalidades_w, 1, length(nr_notas_mensalidades_w) - 1);

return	nr_notas_mensalidades_w;
end pls_obter_params_impr_nota_rel;
/