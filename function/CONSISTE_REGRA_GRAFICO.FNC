create or replace
function Consiste_regra_grafico(ie_tipo_item_p		varchar2)
 		    	return Varchar2 is 

-- Se deve consistir no gr�fico do ADEP
ie_consistir_w	varchar2(1);
ie_tipo_item_w	varchar2(15);

begin
select	nvl(max('N'),'S')
into	ie_consistir_w
from	plt_regra_grafico;

if	(ie_consistir_w	= 'N') then

	select	case	ie_tipo_item_p
		when 'DI'  then '1'
		when 'D'   then '2'
		when 'O'   then '3'
		when 'HM'  then '4'
		when 'J'   then '5'
		when 'MAT' then '6'
		when 'M'   then '7'
		when 'NAN' then '8'
		when 'NPN' then '9'
		when 'P'   then '12'
		when 'R'   then '13'
		when 'SNE' then '14'
		when 'S'   then '15'
		when 'SOL' then '16' 
		when 'G'   then '18' 
		when 'C'   then '18' 
		when 'I'   then '19' 
		when 'L'   then '20' 
		when 'LD'  then '21' else ''
		end	
	into	ie_tipo_item_w
	from	dual;	

	select	nvl(max('S'),'N')
	into	ie_consistir_w
	from	plt_regra_grafico
	where	ie_tipo_item = ie_tipo_item_w;
else
	ie_consistir_w := 'S';
end if;

return	ie_consistir_w;

end Consiste_regra_grafico;
/