create or replace
function obter_classif_esp_pac(nr_atendimento_p		number)
 		    	return varchar2 is

ds_retorno_w	varchar2(255);

begin
	SELECT cep.ds_classificacao
	INTO ds_retorno_w
	FROM CLASSIF_ESPECIAL_PACIENTE cep, atendimento_paciente ap
	WHERE ap.nr_atendimento = nr_atendimento_p 
	AND  cep.nr_sequencia = ap.nr_seq_classif_esp;

return	ds_retorno_w;

end obter_classif_esp_pac;
/
