
create or replace
function cpoe_is_protocol_order_type(
                            cd_protocolo_p 		protocolo_medic_material.cd_protocolo%type,
							nr_seq_protocolo_p 	protocolo_medic_material.nr_sequencia%type,
                            nr_seq_solucao_p    protocolo_medic_material.nr_seq_solucao%type,
                            cd_item_p 			protocolo_medic_material.nr_sequencia%type,
							cd_procedure_p		protocolo_medic_proc.nr_seq_proc_interno%type,
							cd_material_p		protocolo_medic_material.cd_material%type,
							tp_item_p			varchar,
							nr_seq_order_unit_p material_order_type.nr_seq_order_type%type,
                            cd_estabelecimento_p material_order_type.cd_estabelecimento%type
                            ) return  varchar is
						 
is_order_type     varchar(1) := 'N';

cursor cMaterialCode is
        select 	cd_material 
		from	protocolo_medic_material
		where 	cd_protocolo = cd_protocolo_p  
		and 	nr_sequencia = nr_seq_protocolo_p
		and 	nr_seq_solucao = nr_seq_solucao_p;

begin

	if (tp_item_p = 'S') then   
		for cCdMaterial_w in cMaterialCode
			loop
				is_order_type := 'N';
				 select  max('S') 
					into    is_order_type
					from	cpoe_order_unit a,
							material_order_type b
					where   a.nr_sequencia = nr_seq_order_unit_p
					and		a.NR_SEQ_CPOE_TIPO_PEDIDO = b.nr_seq_order_type
					and		b.cd_material = cCdMaterial_w.cd_material
					and     b.cd_estabelecimento = cd_estabelecimento_p
					and		b.IE_SITUACAO = 'A';

					if (is_order_type <> 'S') then
						return is_order_type;
					end if;  
			end loop;

	elsif (tp_item_p = 'P')	then
		is_order_type := cpoe_is_proc_order_type(cd_procedure_p,  nr_seq_order_unit_p, cd_estabelecimento_p);
	elsif (tp_item_p = 'M')	then
		is_order_type := cpoe_is_material_order_type(cd_material_p, nr_seq_order_unit_p, cd_estabelecimento_p);
	end if;
	
    return is_order_type;

end cpoe_is_protocol_order_type;
/
