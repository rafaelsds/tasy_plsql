create or replace
function pls_obter_dados_xml_arquiv_web(nr_seq_protocolo_p	Number,
					ie_tipo_opcao_p		Varchar2,
					ie_nova_imp_p		Varchar2)
 		    	return Varchar2 is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter os dados do arquivo XML
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
	N - Nome 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/			
			
ds_retorno_w		Varchar2(255);
nr_seq_xml_arquivo_w	Number(10);
ie_nova_imp_w		varchar2(1);
			
begin

ie_nova_imp_w := nvl(ie_nova_imp_p, 'N');

if	(ie_nova_imp_w = 'S') then

	select  max(nr_seq_xml_arquivo)
	into	nr_seq_xml_arquivo_w
	from 	pls_protocolo_conta_imp
	where   nr_sequencia	= nr_seq_protocolo_p;
	
else

	select  max(nr_seq_xml_arquivo)
	into	nr_seq_xml_arquivo_w
	from 	pls_protocolo_conta 
	where   nr_sequencia = nr_seq_protocolo_p;
end if;
	

if	(nr_seq_xml_arquivo_w is not null) then

	if	(ie_tipo_opcao_p = 'N') then
		select	substr(max(nm_arquivo),1,255)
		into	ds_retorno_w
		from	pls_xml_arquivo
		where	nr_sequencia = nr_seq_xml_arquivo_w;
	end if;

end if;


return	ds_retorno_w;

end pls_obter_dados_xml_arquiv_web;
/
