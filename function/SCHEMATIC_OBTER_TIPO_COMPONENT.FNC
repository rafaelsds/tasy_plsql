create or replace 
function schematic_obter_tipo_component(
			nr_sequencia_p  number,
			ie_tipo_p  varchar2)
			return varchar2 is

ie_tipo_componente_w  varchar(15);
nr_sequencia_w   number(10);

begin
if (nr_sequencia_p is not null) then
	begin
	nr_sequencia_w := nr_sequencia_p;

	if (ie_tipo_p = 'SUP')  then
		begin
		select  max(nr_seq_obj_sup)
		into nr_sequencia_w
		from objeto_schematic
		where nr_sequencia = nr_sequencia_p;
		end;
	end if;

	select  ie_tipo_componente
	into ie_tipo_componente_w
	from objeto_schematic
	where  nr_sequencia = nr_sequencia_w;
	end;
end if;

return ie_tipo_componente_w;

end schematic_obter_tipo_component;
/