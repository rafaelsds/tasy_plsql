create or replace
function hsl_obter_se_alergia(nr_seq_p		number)
 		    	return varchar2 is

ie_retorno_w	varchar2(1);
ie_alimento_w	varchar2(1);
ie_cadastro_w	varchar2(1);
			
begin

select	max(ie_alimento),
	max(ie_cadastro)
into	ie_alimento_w,
	ie_cadastro_w
from	tipo_alergia
where	nr_sequencia	= nr_seq_p;

if	(ie_alimento_w = 'S') or
	(ie_cadastro_w = 'S') then
	ie_retorno_w	:= 'S';
end if;

return	ie_retorno_w;

end hsl_obter_se_alergia;
/