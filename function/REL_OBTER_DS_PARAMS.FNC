CREATE OR REPLACE
FUNCTION REL_OBTER_DS_PARAMS (	DT_INICIAL_P		DATE,
				DT_FINAL_P		DATE,
				IE_TIPO_CONVENIO_P	NUMBER,
				CD_ESTABELECIMENTO_P	NUMBER,
				CD_CARATER_ATEND_P	VARCHAR2,
				IE_INTERNADO_P		VARCHAR2,
				IE_CDI_P		VARCHAR2,
				IE_PRONTO_SOCORRO_P	VARCHAR2,
				IE_AMBULATORIAL_P	VARCHAR2)
 		    	RETURN VARCHAR2 IS
				DS_RETORNO_W	VARCHAR2(255);
BEGIN
SELECT	SUBSTR(' '||wheb_mensagem_pck.get_texto(796884)||' '||DT_INICIAL_P||' '||wheb_mensagem_pck.get_texto(796885)||' '||DT_FINAL_P||CHR(13)||
	DECODE(IE_TIPO_CONVENIO_P, 0, '', ' '||wheb_mensagem_pck.get_texto(796886)||': '|| SUBSTR(OBTER_VALOR_DOMINIO(11,IE_TIPO_CONVENIO_P),1,255)||CHR(13))||
	DECODE(CD_ESTABELECIMENTO_P, 0, '', ' '||wheb_mensagem_pck.get_texto(796889)||' '|| SUBSTR(OBTER_NOME_ESTABELECIMENTO(CD_ESTABELECIMENTO_P),1,255)||CHR(13))||
   	DECODE(CD_CARATER_ATEND_P, '0', '', ' '||wheb_mensagem_pck.get_texto(796892)||': '|| (SELECT CD_CARATER_INTERNACAO || ' - ' || DS_CARATER_INTERNACAO FROM SUS_CARATER_INTERNACAO WHERE CD_CARATER_INTERNACAO = CD_CARATER_ATEND_P)||CHR(13))||
	DECODE(IE_INTERNADO_P, 'N', DECODE(IE_CDI_P, 'N', DECODE(IE_PRONTO_SOCORRO_P, 'N', DECODE(IE_AMBULATORIAL_P, 'N', '', ' '||wheb_mensagem_pck.get_texto(796896)||': '), ' '||wheb_mensagem_pck.get_texto(796896)||': '), ' '||wheb_mensagem_pck.get_texto(796896)||': '), ' '||wheb_mensagem_pck.get_texto(796896)||': ')||
	DECODE(IE_INTERNADO_P, 'S', wheb_mensagem_pck.get_texto(796903)||', ', '')||
	DECODE(IE_CDI_P, 'S', wheb_mensagem_pck.get_texto(796906)||', ', '')||
	DECODE(IE_PRONTO_SOCORRO_P, 'S', wheb_mensagem_pck.get_texto(796907)||', ', '')||
	DECODE(IE_AMBULATORIAL_P, 'S', wheb_mensagem_pck.get_texto(796908), ''),1,255)
INTO	DS_RETORNO_W
FROM DUAL;
RETURN	DS_RETORNO_W ;
END REL_OBTER_DS_PARAMS ;
/
