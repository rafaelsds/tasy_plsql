create or replace
function PLT_obter_se_plano_estend(	nr_atendimento_p	number,
									dt_quebra_p			date)
 		    	return varchar2 is

ie_estendido_w	char(1);
			
begin

if	(nr_atendimento_p is not null) and
	(dt_quebra_p is not null) then
	select	nvl(max('S'),'N')
	into	ie_estendido_w
	from	prescr_material a,
			prescr_medica b
	where	rownum = 1
	and		a.nr_prescricao		= b.nr_prescricao
	and		b.nr_atendimento	= nr_atendimento_p
	and		b.dt_validade_prescr	> dt_quebra_p;
	
	if	(ie_estendido_w = 'N') then
		select	nvl(max('S'),'N')
		into	ie_estendido_w
		from	prescr_solucao a,
				prescr_medica b
		where	rownum = 1
		and		a.nr_prescricao		= b.nr_prescricao
		and		b.nr_atendimento	= nr_atendimento_p
		and		b.dt_validade_prescr	> dt_quebra_p;
		
		if	(ie_estendido_w = 'N') then
			select	nvl(max('S'),'N')
			into	ie_estendido_w
			from	prescr_dieta a,
					prescr_medica b
			where	rownum = 1
			and		a.nr_prescricao		= b.nr_prescricao
			and		b.nr_atendimento	= nr_atendimento_p
			and		b.dt_validade_prescr	> dt_quebra_p;
			
			if	(ie_estendido_w = 'N') then
				select	nvl(max('S'),'N')
				into	ie_estendido_w
				from	rep_jejum a,
						prescr_medica b
				where	rownum = 1
				and		a.nr_prescricao		= b.nr_prescricao
				and		b.nr_atendimento	= nr_atendimento_p
				and		b.dt_validade_prescr		> dt_quebra_p;

				if	(ie_estendido_w = 'N') then
					select	nvl(max('S'),'N')
					into	ie_estendido_w
					from	nut_pac a,
							prescr_medica b
					where	rownum = 1
					and		a.nr_prescricao		= b.nr_prescricao
					and		b.nr_atendimento	= nr_atendimento_p
					and		b.dt_validade_prescr	> dt_quebra_p;

					if	(ie_estendido_w = 'N') then
						select	nvl(max('S'),'N')
						into	ie_estendido_w
						from	prescr_procedimento a,
								prescr_medica b
						where	rownum = 1
						and		a.nr_prescricao		= b.nr_prescricao
						and		b.nr_atendimento	= nr_atendimento_p
						and		b.dt_validade_prescr	> dt_quebra_p;
						
						if	(ie_estendido_w = 'N') then
							select	nvl(max('S'),'N')
							into	ie_estendido_w
							from	prescr_recomendacao a,
									prescr_medica b
							where	rownum = 1
							and		a.nr_prescricao		= b.nr_prescricao
							and		b.nr_atendimento	= nr_atendimento_p
							and		b.dt_validade_prescr	> dt_quebra_p;
						
							if	(ie_estendido_w = 'N') then
								select	nvl(max('S'),'N')
								into	ie_estendido_w
								from	prescr_gasoterapia a,
										prescr_medica b
								where	rownum = 1
								and		a.nr_prescricao		= b.nr_prescricao
								and		b.nr_atendimento	= nr_atendimento_p
								and		b.dt_validade_prescr	> dt_quebra_p;

							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
end if;

return	ie_estendido_w;

end PLT_obter_se_plano_estend;
/