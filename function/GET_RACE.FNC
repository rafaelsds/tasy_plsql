create or replace function get_race(cd_pessoa_fisica_p IN pessoa_fisica.cd_pessoa_fisica%TYPE)
  return cor_pele.ie_negro%TYPE is
  cd_pessoa_fisica_w pessoa_fisica.cd_pessoa_fisica%TYPE := cd_pessoa_fisica_p;
  black_race_w cor_pele.ie_negro%TYPE;
begin
  select cp.ie_negro
    into black_race_w
    from cor_pele cp
    left join pessoa_fisica pf
      on pf.NR_SEQ_COR_PELE = cp.nr_sequencia
   where pf.cd_pessoa_fisica = cd_pessoa_fisica_w;
  return black_race_w;
end get_race;
/
