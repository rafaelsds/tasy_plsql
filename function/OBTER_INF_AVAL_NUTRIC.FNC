create or replace
function Obter_inf_aval_nutric	(nr_atendimento_p	number,
				ie_informacao_p		varchar2)
				return number is
/*
ie_informacao_p
FA - Fator atividade
FS - Fator stress
*/

nr_seq_fator_ativ_w		number(15);
nr_seq_fator_stress_w		number(15);

begin

if	(ie_informacao_p	= 'FA') then

	select	max(nr_seq_fator_ativ)
	into	nr_seq_fator_ativ_w
	from	aval_nutricao
	where	nr_atendimento	= nr_atendimento_p
	and	dt_liberacao	is not null
	and	nr_seq_fator_ativ is not null;

	return	nr_seq_fator_ativ_w;
	
elsif	(ie_informacao_p	= 'FS') then

	select	max(nr_seq_fator_stress)
	into	nr_seq_fator_stress_w
	from	aval_nutricao
	where	nr_atendimento	= nr_atendimento_p
	and	dt_liberacao	is not null
	and	nr_seq_fator_stress is not null;

	return	nr_seq_fator_stress_w;
	
end if;

end Obter_inf_aval_nutric;
/