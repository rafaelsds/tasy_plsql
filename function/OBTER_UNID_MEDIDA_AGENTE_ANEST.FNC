create or replace
function obter_unid_medida_agente_anest (
		nr_seq_agente_p	number)
		return varchar2 is

cd_unidade_medida_w	varchar2(30) := '';

begin		
select	max(cd_unidade_medida) cd_unidade_medida
into	cd_unidade_medida_w
from	agente_anestesico
where	nr_sequencia = nr_seq_agente_p;

return cd_unidade_medida_w;

end obter_unid_medida_agente_anest;
/