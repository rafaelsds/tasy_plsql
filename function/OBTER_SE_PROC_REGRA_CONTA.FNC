create or replace
function OBTER_SE_PROC_REGRA_Conta
		(nr_interno_conta_p	number,
		nr_atendimento_p	number) 
		return varchar2 is

ds_retorno_w		varchar2(10);
cd_convenio_w		number(10,0);
ie_tipo_atendimento_w	varchar2(10);
cd_estabelecimento_w	number(5,0);
cd_pessoa_fisica_w	varchar2(10);
dt_entrada_w		date;
qt_atend_w		number(5,0);
qt_regra_w		number(15,0);
qt_dias_w		number(10,0);
dt_entrada_passada_w	date;
ie_regra_w		varchar2(10);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
qt_dias_per_conta_w	number(10,0);
qt_proc_w		number(10,0);
cd_plano_convenio_w	varchar2(10);

cursor c02 is
	select	ie_regra,
		qt_regra,
		cd_procedimento,
		ie_origem_proced
	from	convenio_regra_atend
	where	cd_convenio		= cd_convenio_w
	and	ie_tipo_atendimento	= ie_tipo_atendimento_w
	and	ie_regra		= 'P'
	and	cd_procedimento is not null
	and	nvl(cd_plano, nvl(cd_plano_convenio_w, '0')) = nvl(cd_plano_convenio_w,'0')
	order by cd_procedimento  desc;

begin
ds_retorno_w	:= 'S';

select	max(ie_tipo_atendimento)
into	ie_tipo_atendimento_w
from	atendimento_paciente
where 	nr_atendimento	= nr_atendimento_p;

select	max(obter_plano_convenio_atend(nr_atendimento_p,'C'))
into	cd_plano_convenio_w
from	dual;

select	max(cd_convenio_parametro),
	max((ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_periodo_final) - ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_periodo_inicial)))
into	cd_convenio_w,
	qt_dias_per_conta_w
from	conta_paciente
where 	nr_interno_conta	= nr_interno_conta_p;
	
open c02;
loop
fetch c02 into
	ie_regra_w,
	qt_regra_w,
	cd_procedimento_w,
	ie_origem_proced_w;
exit when c02%notfound;
	begin

	select	nvl(sum(qt_procedimento),0)
	into	qt_proc_w
	from	procedimento_paciente
	where	nr_interno_conta	= nr_interno_conta_p
	and	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w
	and	cd_motivo_exc_conta is null;
	
	if	(qt_proc_w < qt_dias_per_conta_w) then
		ds_retorno_w	:= 'N';
	end if;

	end;
end loop;
close c02;		

return ds_retorno_w;

end;
/