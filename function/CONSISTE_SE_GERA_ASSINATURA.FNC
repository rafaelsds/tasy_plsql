create or replace
function consiste_se_gera_assinatura(nr_seq_template_p	number,
									 nr_seq_item_pep_p	number default null)
								return varchar2 is

ds_retorno_w	varchar2(1) := 'S';
cd_perfil_w		number(10);
nr_seq_item_pep_w	number(10);

begin
cd_perfil_w := obter_perfil_ativo;
nr_seq_item_pep_w := wheb_assist_pck.get_seq_item_pep;


if (nr_seq_item_pep_w is null) then
	nr_seq_item_pep_w := nr_seq_item_pep_p;
end if;
	
if	(nr_seq_item_pep_w is not null) then	

	select nvl(max(ie_gerar_assinatura),'S')
	into   ds_retorno_w
	from   perfil_item_pront_template a,
		   perfil_item_pront b
	where  a.nr_seq_item_perfil = b.nr_sequencia
	and    a.nr_seq_template = nr_seq_template_p
	and    b.cd_perfil = cd_perfil_w
	and    b.nr_seq_item_pront = nr_seq_item_pep_w;
	
end if;

return	ds_retorno_w;

end consiste_se_gera_assinatura;
/