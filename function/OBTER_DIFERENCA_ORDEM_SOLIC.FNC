create or replace
function obter_diferenca_ordem_solic(	nr_ordem_compra_p		number,
					nr_item_oci_p			number)
				return varchar2 is

nr_solic_compra_w		number(10);
nr_item_solic_compra_w		number(5);
qt_material_w			number(13,4);
vl_unitario_material_w		number(13,4);
vl_unit_previsto_ww		number(17,4);
qt_material_ww			number(13,4);
ds_divergencia_w		varchar2(255) := '';

begin
select	nvl(max(nr_solic_compra),0),
	nvl(max(nr_item_solic_compra),0),
	nvl(max(qt_material),0),
	nvl(max(vl_unitario_material),0)
into	nr_solic_compra_w,
	nr_item_solic_compra_w,
	qt_material_w,
	vl_unitario_material_w
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p;

if	(nr_solic_compra_w > 0) then
	begin
	select	nvl(max(vl_unit_previsto),0),
		nvl(max(qt_material),0)
	into	vl_unit_previsto_ww,
		qt_material_ww
	from	solic_compra_item
	where	nr_solic_compra		= nr_solic_compra_w
	and	nr_item_solic_compra	= nr_item_solic_compra_w;
	if	(vl_unit_previsto_ww <> 0) and
		(vl_unit_previsto_ww <> vl_unitario_material_w) then
		ds_divergencia_w	:= substr(ds_divergencia_w || WHEB_MENSAGEM_PCK.get_texto(304158,'VL_UNIT_PREVISTO_WW='||vl_unit_previsto_ww||
											';VL_UNITARIO_MATERIAL_W='||vl_unitario_material_w)|| ' ',1,254);
	end if;
	if	(qt_material_ww <> 0) and
		(qt_material_ww <> qt_material_w) then
		ds_divergencia_w	:= substr(ds_divergencia_w || WHEB_MENSAGEM_PCK.get_texto(304162,'QT_MATERIAL_WW='||qt_material_ww||
											';QT_MATERIAL_W='||qt_material_w),1,254);
	end if;
	end;
end if;

return ds_divergencia_w;

end obter_diferenca_ordem_solic;
/
