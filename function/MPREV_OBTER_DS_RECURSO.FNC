create or replace
function mprev_obter_ds_recurso(	nr_sequencia_p	mprev_recurso_agenda.nr_sequencia%type)
					return Varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X] Objetos do dicion�rio [ X] Tasy (Delphi/Java) [] Portal [X]  Relat�rios [X] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */				
					
ds_retorno_w	Varchar2(255);

begin
if	(nr_sequencia_p is not null) then	
	select	substr(a.nm_recurso,1,255)
	into	ds_retorno_w
	from	mprev_recurso_agenda a
	where	nr_sequencia = nr_sequencia_p;
	
end if;

return	ds_retorno_w;

end mprev_obter_ds_recurso;
/
