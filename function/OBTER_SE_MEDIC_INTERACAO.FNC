create or replace
function obter_se_medic_interacao( cd_material_p          number,
                                   cd_pessoa_fisica_p   varchar2)
 		    	return varchar2 is
            
cd_material_cadastro_w      number(10);  
cd_material_atual_w         number(10);          
ds_retorno_w                varchar2(5) := 'N';
           
Cursor C01 is
      select c.cd_material_interacao
      from   medic_uso_continuo a,
             material b,
             material_interacao_medic c
      where  a.cd_material = b.cd_material
      and	 b.cd_material = c.cd_material
      and	 c.cd_material_interacao is not null      
      and	 a.cd_pessoa_fisica = cd_pessoa_fisica_p;
      
Cursor C02 is
      select c.cd_material_interacao  
      from   material b,
             material_interacao_medic c
      where  b.cd_material = c.cd_material       
      and    b.cd_material = cd_material_p;
      
begin

open C01;
loop
fetch C01 into	
	cd_material_cadastro_w;
exit when C01%notfound;
	begin
   
   open C02;
   loop
   fetch C02 into	
   	cd_material_atual_w;
   exit when C02%notfound;
   	begin
   	
      if (cd_material_cadastro_w = cd_material_atual_w) then
         ds_retorno_w := 'S';
      end if;
      
   	end;
   end loop;
   close C02;
   
	
	end;
end loop;
close C01;

return	ds_retorno_w;

end obter_se_medic_interacao;
/