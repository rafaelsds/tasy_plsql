create or replace
function	OBTER_SE_TOTALIZA_CF(	cd_conta_financ_p	number)
					return varchar2 is

ie_totalizar_w	varchar2(10);

begin

select	nvl(max(ie_totalizar),'S')
into	ie_totalizar_w
from	conta_financeira
where	cd_conta_financ	= cd_conta_financ_p;

if	(nvl(ie_totalizar_w, 'S') = 'N') then
	return 'N';
else
	return 'S';
end if;

end	OBTER_SE_TOTALIZA_CF;
/