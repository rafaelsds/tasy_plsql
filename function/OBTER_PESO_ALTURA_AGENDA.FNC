Create or Replace
Function obter_peso_altura_agenda(	nr_sequencia_p	number,
				ie_opcao_p	varchar2)
return Number is

qt_retorno_w	number(15,3);
qt_peso_w	Number(6,3);
qt_altura_cm_w	Number(5,2);

/*
P - Peso
A - Altura
*/

BEGIN

if	(nr_sequencia_p is not null) then
	begin
	select	qt_peso,
		qt_altura_cm
	into	qt_peso_w,
		qt_altura_cm_w
	from	agenda_paciente
	where	nr_sequencia = nr_sequencia_p;

	if	(ie_opcao_p = 'P') then
		qt_retorno_w	:= qt_peso_w;
	elsif	(ie_opcao_p = 'A') then
		qt_retorno_w	:= qt_altura_cm_w;
	end if;
	end;
end if;

RETURN qt_retorno_w;

END obter_peso_altura_agenda;
/
