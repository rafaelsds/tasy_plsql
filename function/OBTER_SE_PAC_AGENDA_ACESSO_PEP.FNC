create or replace
function obter_se_pac_agenda_acesso_pep(
				nr_atendimento_p		number,		
				cd_medico_agenda_p		varchar2,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number
				)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(1);
ie_agendas_executadas_w	Varchar2(1);
qt_agendamentos_w	Number(5);
ie_executado_w		Varchar2(1);
			
begin

select	count(*)
into	qt_agendamentos_w
from	agenda_global_v
where	nr_atendimento		= nr_atendimento_p
and	cd_medico_agenda	= cd_medico_agenda_p
and	dt_agenda		>= trunc(sysdate,'dd')
and	ie_status_agenda   not in ('F','I','L','B','R','LF','PA','C');


if	(qt_agendamentos_w > 0) then
	ds_retorno_w	:= 'S';
else
	ds_retorno_w	:= 'N';
end if;

return	ds_retorno_w;
end obter_se_pac_agenda_acesso_pep;
/