create or replace
function obter_exige_lado_proc_interno (
		nr_sequencia_p	number)
		return varchar2 is
		
ie_exige_lado_w		varchar2(2) := '';
		
begin

if	(nr_sequencia_p > 0) then
	begin
	select	ie_exige_lado
	into	ie_exige_lado_w
	from	proc_interno
	where	nr_sequencia = nr_sequencia_p;
	end;
end if;

return ie_exige_lado_w;

end obter_exige_lado_proc_interno;
/