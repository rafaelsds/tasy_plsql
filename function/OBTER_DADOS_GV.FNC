create or replace 
function Obter_Dados_GV(nr_atendimento_p	number) 
			return varchar2 is

ds_retorno_w		varchar2(2000);
ie_status_w		varchar2(100);
ie_solicitacao_w	varchar2(100);
ie_tipo_vaga_w		varchar2(100);
ds_setor_atendimento_w	varchar2(100);
cd_unidade_basica_w	varchar2(10);
cd_unidade_compl_w	varchar2(10);
cd_setor_atendimento_w	number(5);
ds_status_w		varchar2(255);

cursor C01 is
select	substr(obter_valor_dominio(1408,ie_status),1,100),
	substr(obter_valor_dominio(1407,ie_solicitacao),1,100),
	substr(obter_valor_dominio(1410,ie_tipo_vaga),1,100),
	substr(obter_nome_setor(cd_setor_desejado),1,100),
	cd_unidade_basica,
	cd_unidade_compl,
	cd_setor_desejado
from	gestao_vaga
where	nr_atendimento = nr_atendimento_p
order by dt_solicitacao;

begin


open	c01;
loop
fetch	c01 into
	ie_status_w,
	ie_solicitacao_w,
	ie_tipo_vaga_w,
	ds_setor_atendimento_w,
	cd_unidade_basica_w,
	cd_unidade_compl_w,
	cd_setor_atendimento_w;
exit	when c01%notfound;
	begin

	select	max(obter_valor_dominio(82,ie_status_unidade))
	into	ds_status_w
	from	unidade_atendimento
	where	cd_setor_atendimento	= cd_setor_atendimento_w
	and	cd_unidade_basica	= cd_unidade_basica_w
	and	cd_unidade_compl	= cd_unidade_compl_w;

	ds_retorno_w	:= 	ie_status_w ||' '|| ie_solicitacao_w ||' '|| ie_tipo_vaga_w ||' '||
				ds_setor_atendimento_w ||' '||cd_unidade_basica_w ||' '||cd_unidade_compl_w ||' '||ds_status_w;

	end;
end loop;
close c01;

return ds_retorno_w;

end Obter_Dados_GV;
/
