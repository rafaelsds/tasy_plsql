create or replace
function obter_moeda_item_conv(	cd_edicao_amb_p		number,
				cd_procedimento_p		number,
				dt_consulta_p		date,
				ie_tipo_p			varchar2) return varchar2 is

/*
C = Codigo
D = Descricao
*/	

cd_moeda_w	number(15);
ds_moeda_w	varchar2(30);
ds_retorno_w	varchar2(30);

begin

begin
select	a.cd_moeda,
	substr(obter_desc_moeda(a.cd_moeda),1,30)
into	cd_moeda_w,
	ds_moeda_w
from	preco_amb a
where	(a.cd_edicao_amb    = cd_edicao_amb_p)
and	(a.cd_procedimento  = cd_procedimento_p)
and	nvl(a.dt_inicio_vigencia,sysdate - 3650)	=
	 (	select	max(nvl(b.dt_inicio_vigencia,sysdate - 3650))
		from  	preco_amb b
		where 	b.cd_edicao_amb	= cd_edicao_amb_p
		and	b.cd_procedimento	= cd_procedimento_p
		and	nvl(b.dt_inicio_vigencia,sysdate - 3650)	<= dt_consulta_p);
exception
when others then
	cd_moeda_w := 1;
	ds_moeda_w := '';
end;

if	(ie_tipo_p = 'C') then
	ds_retorno_w := to_char(cd_moeda_w);
else
	ds_retorno_w := ds_moeda_w;
end if;
	
return ds_retorno_w;

end obter_moeda_item_conv;
/