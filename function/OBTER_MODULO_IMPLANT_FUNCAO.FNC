create or replace
function obter_modulo_implant_funcao(cd_funcao_p	number)
 		    	return number is

nr_sequencia_w		number(10,0);

begin

select	max(b.nr_sequencia)
into	nr_sequencia_w
from	funcao a,
	modulo_implantacao b 
where	b.nr_sequencia = a.nr_seq_mod_impl
and	cd_funcao = cd_funcao_p;

return	nr_sequencia_w;

end obter_modulo_implant_funcao;
/