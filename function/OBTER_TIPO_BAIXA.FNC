create or replace
FUNCTION Obter_Tipo_Baixa(cd_tipo_baixa_p varchar2) return varchar2 is

			     tipo_baixa_w varchar2(100);
begin
	select  a.ds_tipo_baixa
	into    tipo_baixa_w
	from    tipo_baixa_cpa A,
	        valor_dominio b
	where   a.ie_tipo_consistencia = b.vl_dominio
	and   	 b.cd_dominio = 503
	and     cd_tipo_baixa_p = a.cd_tipo_baixa;

return (tipo_baixa_w);

END Obter_Tipo_Baixa;
/
