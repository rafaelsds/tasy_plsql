create or replace 
function fis_get_standard_field_desc(	ds_table_p varchar2,
										ds_field_p varchar2 ) 
										return varchar2 is
response_w	varchar2(255);
begin
	select 	substr(nvL(ds_label, nm_atributo), 1, 255)
	into 	response_w
	from 	tabela_atributo 
	where 	nm_tabela = ds_table_p 
	and 	nm_atributo = ds_field_p;
	
	return response_w;
end fis_get_standard_field_desc;
/
