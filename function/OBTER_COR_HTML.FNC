create or replace
function obter_cor_html(nr_seq_legenda_p		number,
			nr_seq_tasy_padrao_cor_p	number,
			cd_perfil_p			number,
			cd_estabelecimento_p		number)
 		    	return varchar2 is

ds_cor_w			varchar2(255);

begin

if (nr_seq_tasy_padrao_cor_p is not null) and
	(nr_seq_legenda_p is not null) then
	select  nvl(max(c.DS_COR_HTML),max(b.DS_COR_HTML))
	into	ds_cor_w
	from    tasy_padrao_cor b,
		tasy_padrao_cor_cliente a,
		TASY_PADRAO_COR_PERFIL c
	where   b.nr_seq_legenda = nr_seq_legenda_p
	and     a.nr_seq_cor(+) = b.nr_sequencia
	and     b.nr_sequencia = c.nr_seq_padrao(+)
	and     nvl (a.cd_perfil(+), cd_perfil_p) = cd_perfil_p
	and     nvl (a.cd_estabelecimento(+), cd_estabelecimento_p) = cd_estabelecimento_p
	and     nvl(nr_seq_tasy_padrao_cor_p,b.nr_sequencia) = b.nr_sequencia;
end if;

return	ds_cor_w;

end obter_cor_html;
/