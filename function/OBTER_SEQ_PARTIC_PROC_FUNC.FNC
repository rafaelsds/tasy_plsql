create or replace
function Obter_Seq_Partic_Proc_Func
		(nr_seq_proced_p	number,
		ie_funcao_p		varchar2)
		return number is

nr_seq_partic_w		number(10,0);

begin

select	nvl(max(nr_seq_partic),0)
into	nr_seq_partic_w
from	procedimento_participante
where	nr_sequencia		= nr_seq_proced_p
and	ie_funcao		= ie_funcao_p;

return	nr_seq_partic_w;

end Obter_Seq_Partic_Proc_Func;
/