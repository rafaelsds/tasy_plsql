create or replace
function Rp_obter_qtd_vaga_restante(nr_seq_modelo_p		number)
 		    	return number is

qt_retorno_w	number(10,0);			
qt_pac_modelo_w	number(10,0);
qt_vagas_w	number(10,0);
	

pragma autonomous_transaction;	
begin

select	count(*)
into	qt_pac_modelo_w
from	rp_paciente_reabilitacao a,
	rp_pac_modelo_agendamento b
where	a.nr_sequencia = b.nr_seq_pac_reab
and	b.nr_seq_modelo_agendamento = nr_seq_modelo_p
and	nvl(b.ie_situacao,'A') = 'A'
and	dt_fim_tratamento is null;

select	nvl(qt_vagas,0)
into	qt_vagas_w
from	rp_modelo_agendamento
where	nr_sequencia = nr_seq_modelo_p;

qt_retorno_w	:= (qt_vagas_w - qt_pac_modelo_w);

return	qt_retorno_w;

end Rp_obter_qtd_vaga_restante;
/
