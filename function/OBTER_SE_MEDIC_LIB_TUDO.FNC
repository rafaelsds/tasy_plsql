create or replace
function obter_se_medic_lib_tudo(nr_prescricao_p		number,
				 nm_usuario_p		varchar2, 
				 cd_perfil_p			number)
 		    	return varchar2 is
				
ie_retorno_w			varchar2(1);
begin

Obter_Param_Usuario(924, 11, cd_perfil_p, nm_usuario_p, 0, ie_retorno_w);
if (ie_retorno_w = 'D') then

	Select	nvl(max(obter_se_medico_lib_tudo(nr_prescricao_p,cd_setor_atendimento)), 'N')
	into	ie_retorno_w
	from 	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	
end if;

return	ie_retorno_w;

end obter_se_medic_lib_tudo;
/
