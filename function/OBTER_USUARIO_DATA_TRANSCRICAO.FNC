create or replace
function obter_usuario_data_transcricao (
				dt_transcricao_p		date,
				nm_usuario_transcricao_p	varchar2)
				return varchar2 is
				
ds_usuario_data_w	varchar2(255);
				
begin
if	(dt_transcricao_p is not null) and
	(nm_usuario_transcricao_p is not null) then
	ds_usuario_data_w	:= wheb_mensagem_pck.get_texto(309593,	'NM_USUARIO_TRANSCRICAO_P=' || nm_usuario_transcricao_p || ';' ||
																'DT_TRANSCRICAO_P=' ||  to_char(dt_transcricao_p,'dd/mm hh24:mi'));
							-- Em transcrição por #@NM_USUARIO_TRANSCRICAO_P#@ em #@DT_TRANSCRICAO_P#@
else
	ds_usuario_data_w	:= null;
end if;

return ds_usuario_data_w;

end obter_usuario_data_transcricao;
/