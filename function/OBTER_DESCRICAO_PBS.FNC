create or replace function obter_descricao_pbs(	cd_pbs_p 	varchar)
					return varchar2 is

ds_retorno_w		varchar2(255);

begin

select max(p.ds_pbs)
into ds_retorno_w
from pbs_record p
inner join material_pbs m
on p.cd_pbs = m.cd_pbs
and p.cd_pbs = cd_pbs_p
group by p.ds_pbs;

return ds_retorno_w;

end obter_descricao_pbs;
/