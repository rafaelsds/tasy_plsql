create or replace
function pls_obter_inf_pagto_linha(	nr_seq_prestador_p	number,
					nr_linha_p		Number,
					ie_informacao_p		varchar2,
					nr_seq_lote_p		pls_lote_pagamento.nr_sequencia%type)
					return varchar2 is 

ds_retorno_w			varchar2(30);
nr_linha_w			Number(10);
dt_liquidacao_w			date;
vl_titulo_w			number(15,2);
dt_mes_competencia_w		date;
cpf_cgc_w			varchar2(255);

begin

cpf_cgc_w	:= '';

select	decode(cd_cgc, null, cd_pessoa_fisica, cd_cgc)
into	cpf_cgc_w
from	pls_prestador
where	nr_sequencia = nr_seq_prestador_p;

select	linha,
	dt_liquidacao,
	vl_titulo,
	dt_mes_competencia
into	nr_linha_w,
	dt_liquidacao_w,
	vl_titulo_w,
	dt_mes_competencia_w
from	(select a.nr_sequencia,
		rownum linha,
		trunc(e.dt_vencimento_atual) dt_liquidacao,
		decode(e.ie_situacao,'L',to_number(obter_dados_tit_pagar(e.nr_titulo,'P')),e.vl_saldo_titulo) vl_titulo, -- e.vl_titulo vl_titulo,
		a.dt_mes_competencia dt_mes_competencia,
		f.vl_base_calculo
	from	pls_lote_pagamento 		a,
		pls_pagamento_prestador		b,
		pls_prestador 			c,
		pls_pag_prest_vencimento	d,
		titulo_pagar 			e,
		pls_pag_prest_venc_trib		f
	where	a.nr_sequencia 		= b.nr_seq_lote
	and	b.nr_seq_prestador 	= c.nr_sequencia
	and	d.nr_seq_pag_prestador 	= b.nr_sequencia
	and	d.nr_sequencia		= f.nr_seq_vencimento
	and	d.nr_titulo		= e.nr_titulo
	and	((c.cd_pessoa_fisica	= cpf_cgc_w) or
		 (c.cd_cgc		= cpf_cgc_w))
	and	a.nr_sequencia 		< nr_seq_lote_p
	and	rownum <= 5)
where	linha = nr_linha_p;

if	(ie_informacao_p = 'L') then
	ds_retorno_w := substr(dt_liquidacao_w,1,10);
elsif	(ie_informacao_p = 'V') then
	ds_retorno_w := campo_mascara_virgula(vl_titulo_w);
elsif	(ie_informacao_p = 'M') then
	ds_retorno_w := substr(dt_mes_competencia_w,1,10);
end if;

return ds_retorno_w;

end pls_obter_inf_pagto_linha;
/