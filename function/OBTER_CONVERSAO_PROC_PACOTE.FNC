create or replace
function obter_conversao_proc_pacote(nr_seq_proc_autor_p	number,
					ie_opcao_p		varchar2)
 		    	return varchar2 is
			
cd_estabelecimento_w	number(5,0) := Wheb_usuario_pck.Get_Cd_estabelecimento;
cd_convenio_autor_w	number(10,0);
cd_proc_conversao_w	varchar2(20);
ds_proc_conversao_w	varchar2(255);
ds_retorno_w		varchar2(255);
cd_proc_pacote_w	number(15);
cd_tipo_acomodacao_w	number(15,0);
begin
if	(nr_seq_proc_autor_p > 0) then
	cd_proc_pacote_w := to_number(OBTER_PROC_PACOTE_AUTOR(nr_seq_proc_autor_p,'C'));
end if;

if	(cd_proc_pacote_w > 0) then

	select	a.cd_convenio,
            a.cd_tipo_acomodacao
	into	cd_convenio_autor_w,
            cd_tipo_acomodacao_w
	from	autorizacao_convenio a,
		procedimento_autorizado b
	where	a.nr_sequencia = b.nr_sequencia_autor
	and	b.nr_sequencia = nr_seq_proc_autor_p;
	

	select	max(cd_proc_convenio),
		max(ds_proc_convenio)
	into	cd_proc_conversao_w,
		ds_proc_conversao_w
	from	conversao_proc_convenio
	where	cd_procedimento = cd_proc_pacote_w
	and	nvl(ie_situacao,'A') = 'A'
	and	cd_convenio	= cd_convenio_autor_w
	and	cd_estabelecimento = cd_estabelecimento_w
    and nvl(cd_tipo_acomod_conv,cd_tipo_acomodacao_w) = cd_tipo_acomodacao_w;
	
	if	(ie_opcao_p = 'C') then
		ds_retorno_w := cd_proc_conversao_w;
	else	
		ds_retorno_w := ds_proc_conversao_w;
	end if;
end if;

return	ds_retorno_w;

end obter_conversao_proc_pacote;
/
