create or replace 
function cus_obter_qt_distribuicao (cd_sequencia_p number)
		return number is

ds_retorno_w	number(15,4);

begin
	if (cd_sequencia_p is not null) then
		begin
		select	sum(a.qt_distribuicao)
		into	ds_retorno_w
		from	criterio_distr_orc_dest a
		where	a.cd_sequencia_criterio	= cd_sequencia_p;
		end;
	end if;
	
return ds_retorno_w;

end cus_obter_qt_distribuicao;
/
