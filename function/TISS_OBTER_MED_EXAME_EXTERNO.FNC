create or replace function TISS_Obter_Med_Exame_Externo
			(nr_sequencia_p	in	number,
      ie_desc_solic_exame_p in varchar2)
			return varchar2 is

ds_exame_w		varchar2(255);
nr_seq_exame_w		number(10);
ds_exame_sem_cad_w	varchar2(255);
nr_seq_exame_lab_w	number(10);
nr_proc_interno_w	number(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
ie_tipo_desc_w varchar2(1);
ie_desc_solic_exame_w varchar(5);
begin

/*
E - Exame Padr�o
S - Exame sem cadastro 
L - Exame de Laborat�rio 
I - Tabela Interna
P - Proc. Faturamento
*/

ie_desc_solic_exame_w := ie_desc_solic_exame_p;
if(ie_desc_solic_exame_w is null) then
  ie_desc_solic_exame_w := 'ESLIP';
end if;

select	nr_seq_exame,
	ds_exame_sem_cad,
	nr_seq_exame_lab,
	nr_proc_interno,
	cd_procedimento,
	ie_origem_proced
into	nr_seq_exame_w,
	ds_exame_sem_cad_w,
	nr_seq_exame_lab_w,
	nr_proc_interno_w,
	cd_procedimento_w,
	ie_origem_proced_w
from	pedido_exame_externo_item
where	nr_sequencia	= nr_sequencia_p;

for pos in 1..length(ie_desc_solic_exame_w)
loop
  ie_tipo_desc_w := substr(ie_desc_solic_exame_w, pos, 1);
  
  if (ds_exame_w is null) then  
    begin
      if ((ie_tipo_desc_w = 'E') and (nr_seq_exame_w > 0)) then
      begin
        select	ds_exame
        into	ds_exame_w
        from	med_exame_padrao
        where	nr_sequencia	= nr_seq_exame_w;      
      end;
      elsif ((ie_tipo_desc_w = 'S') and (ds_exame_sem_cad_w is not null)) then
        ds_exame_w	:= ds_exame_sem_cad_w;
      elsif ((ie_tipo_desc_w = 'L') and (nr_seq_exame_lab_w is not null)) then
        ds_exame_w	:= substr(obter_desc_exame(nr_seq_exame_lab_w),1,255);
      elsif ((ie_tipo_desc_w = 'I') and (nr_proc_interno_w is not null)) then
        ds_exame_w	:= substr( obter_desc_proc_interno(nr_proc_interno_w),1,255);
      elsif ((ie_tipo_desc_w = 'P') and (cd_procedimento_w is not null) and (ie_origem_proced_w is not null)) then
        ds_exame_w	:= substr(obter_descricao_procedimento(cd_procedimento_w,ie_origem_proced_w),1,255);
      end if;    
    end;
  end if;
  
end loop;  

return	ds_exame_w;

end TISS_Obter_Med_Exame_Externo;
/
