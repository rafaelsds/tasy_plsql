CREATE OR REPLACE FUNCTION obter_se_problema_paciente(ie_problema_paciente_p VARCHAR,
                                                      cd_doenca_p            VARCHAR,
                                                      nr_atendimento_p       NUMBER) RETURN VARCHAR IS

    ds_retorno_w VARCHAR(1) := 'N';
    count_w      NUMBER;

BEGIN

    IF (substr(ie_problema_paciente_p, 0, 2) = 'PP') THEN
        /* PP = Problema do Paciente */
    
        SELECT COUNT(*)
          INTO count_w
          FROM lista_problema_pac pp
         WHERE pp.nr_sequencia = substr(ie_problema_paciente_p, 3)
           AND ((cd_doenca_p IS NOT NULL AND cd_doenca_p = pp.cd_doenca) OR
               (nr_atendimento_p IS NOT NULL AND nr_atendimento_p = pp.nr_atendimento));
        IF (count_w > 0) THEN
            ds_retorno_w := 'S';
        END IF;
    
    ELSIF (substr(ie_problema_paciente_p, 0, 2) = 'DP') THEN
        /* DP = Diagnostico do Paciente */
    
        IF (cd_doenca_p IS NOT NULL AND cd_doenca_p = substr(ie_problema_paciente_p, 3)) THEN
            ds_retorno_w := 'S';
        END IF;
    
    END IF;

    RETURN ds_retorno_w;
END obter_se_problema_paciente;
/
