create or replace
function Proj_obter_Media_complex
			(	nr_seq_proj_p			number)
 		    		return Number is


qt_itens_w			Number(10)	:= 0;
qt_soma_w			Number(10)	:= 0;
vl_retorno_w			number(10)	:= 0;


BEGIN

select 	sum(nvl(b.vl_item,0))
into	qt_soma_w
from	proj_complexidade a,
	proj_complex_classif_item b
where	a.nr_seq_complex_classif_item = b.nr_sequencia
and	a.nr_seq_proj = nr_seq_proj_p;

select 	count(*)
into	qt_itens_w
from	proj_complexidade a
where	a.nr_seq_proj = nr_seq_proj_p;


	vl_retorno_w	:= round(dividir(qt_soma_w, qt_itens_w),0);

return	vl_retorno_w;

END Proj_obter_Media_complex;
/
