create or replace
function Obter_Funcao_Medico_Abramge
		(ie_funcao_medico_p	number)
		return number is

ie_funcao_retorno_w	number(05,0);

begin

select	decode(cd_funcao, 1, 0, 2, 0, 3, 1, 4, 2, 9, 3, 6, 4, 5, 5, 7)
into	ie_funcao_retorno_w
from 	funcao_medico
where	cd_funcao	= ie_funcao_medico_p;


return	ie_funcao_retorno_w;

end;
/