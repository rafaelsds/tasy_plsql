create or replace
function obter_min_permanencia_setor(nr_atendimento_p 		number,
				     cd_setor_atendimento_p	number,
				     cd_unidade_basica_p	varchar2,
				     cd_unidade_compl_p		varchar2)
 		    	return number is

qt_min_setor_w	number(20);
dt_entrada_unidade_w	date;
			
begin

select	max(dt_entrada_unidade)
into	dt_entrada_unidade_w
from	atend_paciente_unidade
where	nr_atendimento = nr_atendimento_p
and	cd_setor_atendimento = cd_setor_atendimento_p
and	cd_unidade_basica    = cd_unidade_basica_p
and 	cd_unidade_compl     = cd_unidade_compl_p;


if (dt_entrada_unidade_w is not null) then
   
   qt_min_setor_w := ((sysdate - dt_entrada_unidade_w) * 1440);
   
end if;		

return	qt_min_setor_w;

end obter_min_permanencia_setor;
/