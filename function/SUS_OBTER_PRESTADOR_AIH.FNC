create or replace
FUNCTION Sus_Obter_Prestador_Aih(	nr_sequencia_p	number,
				ie_opcao_p	varchar2)
				return varchar2 is


cd_estabelecimento_w		number(4);
cd_medico_executor_w		varchar2(10);
cd_cgc_prestador_w		VarChar2(14);
ie_doc_executor_w			number(1);
ds_retorno_w			varchar2(20);
cd_cnes_w			varchar2(7);
ie_exporta_cnes_w			varchar2(1)	:= 'N';
ie_exporta_cnes_hosp_w		varchar2(1)	:= 'N';
ie_exporta_cnes_setor_w		varchar2(1)	:= 'N';
cd_setor_atendimento_w		number(5);

BEGIN

select	c.cd_estabelecimento,
	p.cd_cgc_prestador,
	nvl(p.cd_medico_executor,p.cd_pessoa_fisica),
	nvl(p.ie_doc_executor,5),
	cd_setor_atendimento
into	cd_estabelecimento_w,
	cd_cgc_prestador_w,
	cd_medico_executor_w,
	ie_doc_executor_w,
	cd_setor_atendimento_w
from	procedimento_paciente p,
	conta_paciente c
where	p.nr_interno_conta	= c.nr_interno_conta
and	p.nr_sequencia		= nr_sequencia_p;

select	nvl(max(substr(lpad(cd_cnes_hospital,7,'0'),1,7)),''),
	nvl(max(ie_exporta_cnes),'N'),
	nvl(max(ie_exporta_cnes_hosp),'N'),
	nvl(max(ie_exporta_cnes_setor),'N')
into	cd_cnes_w,
	ie_exporta_cnes_w,
	ie_exporta_cnes_hosp_w,
	ie_exporta_cnes_setor_w
from	sus_parametros_aih
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_doc_executor_w	= 1) then
	ds_retorno_w		:= Obter_Cpf_Pessoa_Fisica(cd_medico_executor_w);
elsif	(ie_doc_executor_w	= 3) then
	ds_retorno_w		:= cd_cgc_prestador_w;
elsif	(ie_doc_executor_w	= 5) then
	ds_retorno_w		:= Obter_cnes_estab(cd_estabelecimento_w);
elsif	(ie_doc_executor_w	= 6) then
	select	max(cd_cnes)
	into	ds_retorno_w
	from	pessoa_juridica
	where	cd_cgc		= cd_cgc_prestador_w;
	
	if	(ie_exporta_cnes_hosp_w	= 'S')	and
		(ie_exporta_cnes_setor_w 	= 'N')	then
		ds_retorno_w	:= cd_cnes_w;
	end if;	
	
	if	(ie_exporta_cnes_setor_w = 'S') then
	begin

	begin
	select	substr(nvl(max(cd_cnes),ds_retorno_w),1,7)
	into	ds_retorno_w
	from	setor_atendimento
	where	cd_setor_atendimento	= cd_setor_atendimento_w;
	exception
		when others then
		ds_retorno_w 	:= ds_retorno_w;
		end;

	end;
end if;
	
end if;

return ds_retorno_w;

END Sus_Obter_Prestador_Aih;
/