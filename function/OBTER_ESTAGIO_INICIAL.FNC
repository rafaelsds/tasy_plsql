create or replace function obter_estagio_inicial(nr_regra_p number, ie_status_execucao_p varchar2)
												 return number is

nr_estagio_entrega_w ESTAGIO_STATUS_ENTREGA.NR_SEQUENCIA%TYPE;
nr_seq_status_w STATUS_REGRA_ENTREGA.NR_SEQUENCIA%TYPE;

begin
nr_seq_status_w := 0;

select NR_SEQUENCIA
into nr_seq_status_w
from status_regra_entrega
where nr_seq_regra = nr_regra_p
and   ie_status_execucao = ie_status_execucao_p;

if(nr_seq_status_w is not null) then
  select NR_SEQUENCIA
  into nr_estagio_entrega_w
  from (  select nr_sequencia
      from ESTAGIO_STATUS_ENTREGA
      where nr_seq_status = nr_seq_status_w
      order by nr_seq_ordem, nr_sequencia)
  where rownum = 1;

end if;

return nr_estagio_entrega_w;

end obter_estagio_inicial;
/