create or replace function obter_existe_parcela_contrato
			(nr_seq_contrato_p	number)
	return varchar2 is

ds_retorno_w	varchar2(1);
qt_parcelas_w	number(15);

begin
ds_retorno_w := 'N';
select	count(*)
into	qt_parcelas_w
from	emprest_financ_parc
where	nr_seq_contrato = nr_seq_contrato_p;

if (nvl(qt_parcelas_w,0) > 0) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;

return ds_retorno_w;

end obter_existe_parcela_contrato;
/
