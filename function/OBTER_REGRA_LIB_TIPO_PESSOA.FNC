create or replace
function obter_regra_lib_tipo_pessoa(	cd_tipo_pessoa_p		number,
				cd_perfil_p		number,
				nm_usuario_lib_p		varchar2,
				ie_opcao_p		varchar2)
return varchar2 is

ie_alterar_w		varchar2(1) := 'S';
ie_resultado_w		varchar2(1) := 'S';

cursor c01 is
select	ie_alterar
from	tipo_pessoa_jur_lib
where	cd_tipo_pessoa			= cd_tipo_pessoa_p
and	nvl(cd_perfil, cd_perfil_p)		= cd_perfil_p
and	nvl(nm_usuario_lib, nm_usuario_lib_p)	= nm_usuario_lib_p
order by
	nvl(cd_perfil,99999) desc,
	nvl(nm_usuario_lib, 'XXXX') desc;
	
			
begin

open C01;
loop
fetch C01 into	
	ie_alterar_w;
exit when C01%notfound;
	begin
	if	(ie_opcao_p = 0) then
		ie_resultado_w := ie_alterar_w;
	end if;
	
	end;
end loop;
close C01;


return	ie_resultado_w;

end obter_regra_lib_tipo_pessoa;
/