create or replace function pc_obter_exame_lab (nr_atendimento_p atendimento_paciente.nr_atendimento%type)
			return varchar2 deterministic is

count_yelow_w	pls_integer := 0;
count_blue_w	pls_integer := 0;
count_red_w		pls_integer := 0;
return_w		varchar2(1) := '';

begin

if (nr_atendimento_p is not null) then
	select 	count(*) 
	into	count_yelow_w
	from prescr_procedimento p,
		 prescr_medica m 
	where 	p.nr_prescricao = m.nr_prescricao
	and		ie_status_atend < 35
	and 	nr_seq_exame is not null
	and 	nr_atendimento = nr_atendimento_p;

	select 	count(*) 
	into	count_blue_w 
	from prescr_procedimento p,
		 prescr_medica m 
	where 	p.nr_prescricao = m.nr_prescricao
	and 	ie_status_atend >= 35
	and 	nr_seq_exame is not null
	and 	nr_atendimento = nr_atendimento_p;

	select 	count(*)
	into	count_red_w
	from exame_lab_result_item a,
		 exame_lab_resultado b,
		 prescr_medica m
	where 	b.nr_prescricao = m.nr_prescricao
	and 	a.nr_seq_resultado = b.nr_seq_resultado
	and 	ie_resultado_critico = 'S'
	and 	m.nr_atendimento = nr_atendimento_p;


	if (count_red_w > 0) then
		return_w := 'R';
	elsif (count_yelow_w = 0 and count_blue_w > 0) then
		return_w := 'G';
	elsif (count_blue_w > 0) then
		return_w := 'B';
	elsif (count_yelow_w > 0) then
		return_w := 'Y';
	end if;
end if;

return return_w;

end pc_obter_exame_lab;
/
