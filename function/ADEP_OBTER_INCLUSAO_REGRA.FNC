create or replace
function adep_obter_inclusao_regra (
				ie_tipo_regra_p		varchar2,
				nr_prescricao_p		number,
				cd_estabelecimento_p	number,
				cd_setor_atendimento_p	number,
				cd_perfil_p		number,
				cd_material_p		number,
				cd_procedimento_p	number,
				ie_origem_proced_p	number,
				nr_seq_proc_interno_p   number,
				cd_setor_prescricao_p	number,
				cd_setor_execucao_p	number,
				nr_seq_exame_p		number)
				return varchar2 is
	 			
cd_setor_atendimento_w	number(5,0);
cd_perfil_w		number(5,0);
				
ie_regra_inclusao_w	varchar2(15)	:= 'S';
cd_grupo_material_w	number(3,0);
cd_subgrupo_material_w	number(3,0);
cd_classe_material_w	number(5,0);

nr_seq_proc_interno_w	number(3,0);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10,0);
cd_area_procedimento_w	number(15,0);
cd_especialidade_w	number(15,0);
cd_grupo_proc_w		number(15,0);
cd_setor_prescricao_w	number(5,0);
cd_setor_execucao_w	number(5,0);
cd_setor_atual_w	number(5);
ie_tipo_evolucao_w	varchar2(15);
nr_atendimento_w	number(10);
nr_seq_atecaco_w	number(10);
cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);
cd_plano_w		varchar2(10);
cd_tipo_acomodacao_w	number(4,0);
ie_hemodialise_w	varchar2(10);
nr_seq_exame_w		number(15);
ie_existe_regra_mat_w	varchar2(1 char);
ie_existe_regra_proc_w	varchar2(1 char);

cursor c01 is
select	ie_regra_inclusao
from	adep_material
where	ie_tipo_regra = ie_tipo_regra_p
and		(cd_estabelecimento = cd_estabelecimento_p)
and		((cd_setor_atendimento = cd_setor_atendimento_w) or (cd_setor_atendimento is null))
and		((cd_perfil = cd_perfil_w) or (cd_perfil is null))
and		((cd_material = cd_material_p) or (cd_material is null))
and		((cd_grupo_material = cd_grupo_material_w) or (cd_grupo_material is null))
and		((cd_subgrupo_material = cd_subgrupo_material_w) or (cd_subgrupo_material is null))
and		((cd_classe_material = cd_classe_material_w) or (cd_classe_material is null))
and		((cd_setor_prescr = cd_setor_prescricao_w) or (cd_setor_prescr is null))
and 	((ie_tipo_evolucao = ie_tipo_evolucao_w) or (ie_tipo_evolucao is null))
and		((cd_setor_atual = cd_setor_atual_w) or (cd_setor_atual is null))
and		((nvl(ie_especifica,'N') = 'N') or (ie_hemodialise_w is not null))
and		nvl(ie_situacao,'A') = 'A'
order by
	nvl(cd_setor_atendimento,99999) desc,
	nvl(cd_setor_prescr,99999) desc,
	nvl(cd_perfil,99999) desc,
	nvl(cd_material,999999) desc,
	nvl(cd_grupo_material,999) desc,
	nvl(cd_subgrupo_material,999) desc,
	nvl(cd_classe_material,99999) desc,
	nr_sequencia;	
	
cursor c02 is
select	ie_regra_inclusao
from	adep_regra_inclusao_proc
where		(cd_estabelecimento = cd_estabelecimento_p)
and		((cd_setor_atendimento = cd_setor_atendimento_w) or (cd_setor_atendimento is null))
and		((cd_perfil = cd_perfil_w) or (cd_perfil is null))
and		((nr_seq_proc_interno	= nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
and		((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
and		((ie_origem_proc = ie_origem_proced_w) or (ie_origem_proc is null))
and		((cd_area_procedimento = cd_area_procedimento_w) or (cd_area_procedimento is null))
and		((cd_especialidade = cd_especialidade_w) or (cd_especialidade is null))
and		((cd_grupo_proc = cd_grupo_proc_w) or (cd_grupo_proc is null))
and		((cd_setor_exec = cd_setor_execucao_w) or (cd_setor_exec is null))
and		((nr_seq_exame = nr_seq_exame_p) or (nr_seq_exame is null))
and 	((ie_tipo_evolucao = ie_tipo_evolucao_w) or (ie_tipo_evolucao is null)) 
and		nvl(ie_situacao,'A') = 'A'
order by
	nvl(cd_setor_atendimento,99999) desc,
	nvl(cd_setor_exec,99999) desc,
	nvl(cd_perfil,99999) desc,
	nvl(nr_seq_proc_interno, 9999999999) desc,		
	nvl(cd_procedimento,999999999999999) desc,
	nvl(ie_origem_proced,9) desc,
	nvl(cd_area_procedimento, 99999999) desc,
	nvl(cd_especialidade, 99999999) desc,
	nvl(cd_grupo_proc,999999999999999) desc,	
	nvl(nr_seq_exame, 99999999) desc,
	nr_sequencia;

begin

if	(ie_tipo_regra_p is not null) and
	(cd_estabelecimento_p is not null) then
	
	select	nvl(max('S'),'N') ie_existe_regra
	into	ie_existe_regra_mat_w
	from	adep_material a
	where	a.cd_estabelecimento = cd_estabelecimento_p;
	
	select	nvl(max('S'),'N') ie_existe_regra
	into	ie_existe_regra_proc_w
	from	adep_regra_inclusao_proc a
	where	a.cd_estabelecimento = cd_estabelecimento_p;

	if ((ie_existe_regra_mat_w <> 'N') and (ie_tipo_regra_p in ('SO','MED','MAT'))) or 
		((ie_existe_regra_proc_w <> 'N') and (ie_tipo_regra_p in ('PROC', 'LAB'))) then
		Select	max(b.ie_tipo_evolucao),
			max(a.nr_atendimento),
			max(Obter_Unidade_Atendimento(a.nr_atendimento,'A','CS')),
			max(a.ie_hemodialise)
		into	ie_tipo_evolucao_w,
			nr_atendimento_w,
			cd_setor_atual_w,
			ie_hemodialise_w
		from	prescr_medica a,
			usuario b
		where 	a.cd_prescritor		= b.cd_pessoa_fisica
		and	a.nr_prescricao		= nr_prescricao_p
		and	nvl(b.ie_situacao,'A') = 'A';
	
		nr_seq_atecaco_w := Obter_Atecaco_atendimento(nr_atendimento_w);
		
		select	max(cd_convenio),
				max(cd_categoria),
				max(cd_plano_convenio),
				max(cd_tipo_acomodacao)
		into	cd_convenio_w,
				cd_categoria_w,
				cd_plano_w,
				cd_tipo_acomodacao_w
		from	atend_categoria_convenio
		where	nr_seq_interno = nr_seq_atecaco_w;
	
		cd_setor_atendimento_w	:= nvl(cd_setor_atendimento_p,0);
		cd_perfil_w		:= nvl(cd_perfil_p,0);
		cd_setor_prescricao_w	:= nvl(cd_setor_prescricao_p,0);
		cd_setor_execucao_w	:= nvl(cd_setor_execucao_p,0);
	
		if	(ie_tipo_regra_p in ('SO','MED','MAT')) then
		
			select	max(cd_grupo_material),
				max(cd_subgrupo_material),
				max(cd_classe_material)
			into	cd_grupo_material_w,
				cd_subgrupo_material_w,
				cd_classe_material_w
			from	estrutura_material_v
			where	cd_material = cd_material_p;
			
			open c01;
			loop
			fetch c01 into	ie_regra_inclusao_w;
			exit when c01%notfound;
				begin
				ie_regra_inclusao_w	:= ie_regra_inclusao_w;
				end;
			end loop;
			close c01;
	
		elsif	(ie_tipo_regra_p in ('PROC', 'LAB')) then
	
	
			if	(nr_seq_proc_interno_p is not null) then
				obter_proc_tab_interno_conv(nr_seq_proc_interno_p, cd_estabelecimento_p, cd_convenio_w, cd_categoria_w, cd_plano_w, null,
						cd_procedimento_w, ie_origem_proced_w, null, sysdate, cd_tipo_acomodacao_w, null, null, null, null, null, null, null);
			else
				cd_procedimento_w	:= cd_procedimento_p;
				ie_origem_proced_w	:= ie_origem_proced_p;
			end if;
								
			select 	max(cd_area_procedimento),
				max(cd_especialidade),
				max(cd_grupo_proc)
			into	cd_area_procedimento_w,
				cd_especialidade_w,
				cd_grupo_proc_w
			from	estrutura_procedimento_v
			where	cd_procedimento 	= cd_procedimento_w
			and	ie_origem_proced 	= ie_origem_proced_w;	
			
			open c02;
			loop
			fetch c02 into ie_regra_inclusao_w;
			exit when c02%notfound;
				begin
				ie_regra_inclusao_w := ie_regra_inclusao_w;
				end;
			end loop;
			close c02;		
		end if;
	end if;
end if;

return ie_regra_inclusao_w;

end adep_obter_inclusao_regra;
/
