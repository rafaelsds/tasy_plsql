create or replace
function pls_obter_end_compl_pj(	cd_cgc_p		varchar2,
					nr_seq_compl_pj		number)
 		    	return varchar2 is

ds_retorno_w		varchar2(255);			
ds_endereco_w		varchar2(40);	
nr_endereco_w		varchar2(20);
ds_complemento_w	varchar2(40);
ds_bairro_w		varchar2(40);
sg_estado_w		pessoa_juridica_compl.sg_estado%type;
ds_municipio_w		varchar2(40);			
			
begin
select  substr(ds_endereco,1,38),
	decode(nr_endereco, null, null, ', '||substr(nr_endereco,1,18)),
	decode(ds_complemento, null, null, ', '||substr(ds_complemento,1,38)),
	decode(ds_bairro, null, null, ', '||substr(ds_bairro,1,38)),
	decode(ds_municipio, null, null, ', '||substr(ds_municipio,1,38)),
	decode(sg_estado, null, null, ', '||substr(sg_estado,1,2))
into	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_bairro_w,
	ds_municipio_w,
	sg_estado_w
from	pessoa_juridica_compl
where	cd_cgc 		= cd_cgc_p
and	nr_sequencia = nr_seq_compl_pj;

ds_retorno_w	:= ds_endereco_w || nr_endereco_w || ds_complemento_w || ds_bairro_w || ds_municipio_w || sg_estado_w;

return	ds_retorno_w;

end pls_obter_end_compl_pj;
/