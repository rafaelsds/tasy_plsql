create or replace
function pls_obter_desc_cargo_diops
		(	cd_cargo_p	Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(60);

begin

select	nvl(max(ds_cargo),'')
into	ds_retorno_w
from	pls_cargo_diops
where	cd_cargo	= cd_cargo_p;

return	ds_retorno_w;

end pls_obter_desc_cargo_diops;
/