create or replace
function rxt_obter_fase_trat_agenda	(nr_seq_tratamento_p	number,
				dt_agenda_p		date)
				return number is

dt_inicio_trat_w	date;
nr_seq_fase_w	number(10,0);
qt_dia_fase_w	number(5,0);
qt_dia_fase_ww	number(5,0) := 0;
qt_dia_trat_w	number(5,0);
nr_seq_fase_trat_w	number(10,0);

cursor c01 is
select	nr_sequencia,
	qt_duracao_trat
from	rxt_fase_tratamento
where	nr_seq_tratamento = nr_seq_tratamento_p
order by
	nr_sequencia;

begin
if	(nr_seq_tratamento_p is not null) and
	(dt_agenda_p is not null) then

	select	max(dt_inicio_trat)
	into	dt_inicio_trat_w
	from	rxt_tratamento
	where	nr_sequencia = nr_seq_tratamento_p;

	if	(dt_inicio_trat_w is not null) then

		open c01;
		loop
		fetch c01 into	nr_seq_fase_w,
				qt_dia_fase_w;
		exit when c01%notfound;
			begin

			qt_dia_fase_ww	:= qt_dia_fase_ww + qt_dia_fase_w;

			qt_dia_trat_w	:= obter_dias_entre_datas(dt_inicio_trat_w, dt_agenda_p);

			if	(nr_seq_fase_trat_w is null) and
				(qt_dia_trat_w <= qt_dia_fase_ww) then

				nr_seq_fase_trat_w := nr_seq_fase_w;
			

			end if;

			end;
		end loop;
		close c01;

	end if;

end if;

return nr_seq_fase_trat_w;

end rxt_obter_fase_trat_agenda;
/