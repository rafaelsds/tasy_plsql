create or replace
function sma_soma_val_relat(	nr_cirurgia_p 		number,
				tipo_p			number)
				return varchar2 is

vl_soma_w			number(15,2);

begin

if 	(tipo_p = 1) then
	select 	nvl(sum(vl_glosa),0)
	into	vl_soma_w
	from	sma_repasse_procedimentos b
	where	b.nr_cirurgia = nr_cirurgia_p;
elsif 	(tipo_p = 2) then
	select 	nvl(sum(vl_procedimento),0)
	into	vl_soma_w
	from	sma_repasse_procedimentos b
	where	b.nr_cirurgia = nr_cirurgia_p;
elsif	(tipo_p = 3) then
	select 	nvl(sum(vl_recebido),0)
	into	vl_soma_w
	from	sma_repasse_procedimentos b
	where	b.nr_cirurgia = nr_cirurgia_p;
elsif	(tipo_p = 4) then
	select	(select nvl(sum(vl_procedimento),0)
		 from	sma_repasse_procedimentos b
		 where	b.nr_cirurgia = nr_cirurgia_p) -
		(select nvl(sum(vl_recebido),0)
		 from	sma_repasse_procedimentos b
		 where	b.nr_cirurgia = nr_cirurgia_p) -
		(select nvl(sum(vl_glosa),0)
		 from	sma_repasse_procedimentos b
		 where	b.nr_cirurgia = nr_cirurgia_p)
	into	vl_soma_w
	from 	dual;
end if;

return 	vl_soma_w;
end 	sma_soma_val_relat;
/