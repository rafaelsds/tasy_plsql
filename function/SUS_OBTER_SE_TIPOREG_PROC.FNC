create or replace
function sus_obter_se_tiporeg_proc(	ie_tiporeg_proc_p	Varchar2,
					cd_procedimento_p	Number,
					ie_origem_proced_p	Number)
 		    	return Varchar2 is
			
ds_retorno_w		Varchar2(1)	:= 'N';
qt_tiporeg_proc_w	Number(5);
			
begin
if	(ie_tiporeg_proc_p	= 'BPA') then
	select	count(*)
	into	qt_tiporeg_proc_w
	from	sus_procedimento_registro
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p
	and	cd_registro in (1,2);
elsif	(ie_tiporeg_proc_p	= 'AIH') then
	select	count(*)
	into	qt_tiporeg_proc_w
	from	sus_procedimento_registro
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p
	and	cd_registro in (3,4,5);
elsif	(ie_tiporeg_proc_p	= 'APAC') then
	select	count(*)
	into	qt_tiporeg_proc_w
	from	sus_procedimento_registro
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p
	and	cd_registro in (6,7);
end if;

if	(qt_tiporeg_proc_w	> 0) then
	ds_retorno_w	:= 'S';
end if;

return	ds_retorno_w;

end sus_obter_se_tiporeg_proc;
/