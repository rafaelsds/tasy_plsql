create or replace
function obter_status_worklist
	(	nr_sequencia_p 	number,
		ie_opcao_p 		varchar2)
    return varchar2 is

ds_retorno_w  		varchar2(100);
ie_retorno_w  		varchar2(100);
dt_inicio_w   		date;
dt_final_previsto_w date;
dt_final_real_w     date;
qt_tempo_atraso_w	wl_regra_item.qt_tempo_atraso%type;

/*
 A - Atrasa/Delayed
 T - Em tempo/On time
 C - Concluido/Done

 ie_opcao_p
   D - Description
   C - Code
 */

begin

if (nr_sequencia_p is not null) then

	select	dt_inicial,
			dt_final_previsto,
			dt_final_real
	into	dt_inicio_w,
			dt_final_previsto_w,
			dt_final_real_w
	from 	wl_worklist
	where 	nr_sequencia = nr_sequencia_p;
	 
	select	max(a.qt_tempo_atraso)
	into	qt_tempo_atraso_w
	from	wl_regra_item a,
			wl_worklist b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.nr_sequencia = nr_sequencia_p;

	if (qt_tempo_atraso_w is null) then
		if (sysdate > dt_final_previsto_w and dt_final_real_w is null) then
			begin
			ds_retorno_w := obter_desc_expressao(302651) ; -- delayed
			ie_retorno_w := 'A';
			end;
		elsif (dt_inicio_w <= dt_final_previsto_w and dt_final_real_w is null) then
			begin
			ds_retorno_w := obter_desc_expressao(560323) ; -- in progress
			ie_retorno_w := 'T';
			end;
		elsif (dt_final_real_w is not null) then
			begin
			ds_retorno_w := obter_desc_expressao(654443) ; -- done
			ie_retorno_w := 'C';
			end;
		end if;
		
	else
		if (sysdate > (dt_inicio_w+(qt_tempo_atraso_w/24)) and dt_final_real_w is null) then
			begin
			ds_retorno_w := obter_desc_expressao(302651) ; -- delayed
			ie_retorno_w := 'A';
			end;
		elsif (dt_inicio_w <= dt_final_previsto_w and dt_final_real_w is null) then
			begin
			ds_retorno_w := obter_desc_expressao(560323) ; -- in progress
			ie_retorno_w := 'T';
			end;
		elsif (dt_final_real_w is not null) then
			begin
			ds_retorno_w := obter_desc_expressao(654443) ; -- done
			ie_retorno_w := 'C';
			end;
		end if;
	
	end if;

	if (ie_opcao_p = 'D') then
		return ds_retorno_w;
	else
		return ie_retorno_w;
	end if;
end if;

return null;

end obter_status_worklist;
/
