create or replace
function OBTER_SE_FLUXO_ESP
		(cd_estabelecimento_p	number,
		 cd_conta_financ_p	number,
		 dt_referencia_p	date) return varchar2 is

cont_w			number(10,0);
ie_retorno_w		varchar2(100);
IE_FLUXO_ESPECIAL_w	varchar2(100);

begin

select	IE_FLUXO_ESPECIAL
into	IE_FLUXO_ESPECIAL_w
from	parametro_fluxo_caixa
where	cd_estabelecimento	= cd_estabelecimento_p;

select	count(*)
into	cont_w
from	fluxo_caixa
where	cd_estabelecimento	= cd_estabelecimento_p
and	dt_referencia		= trunc(dt_referencia_p, 'dd')
and	cd_conta_financ	= cd_conta_financ_p
and	ie_periodo		= 'D'
and	ie_classif_fluxo	= 'R'
and	ie_integracao <> 'RE';

ie_retorno_w		:= 'S';
if	(IE_FLUXO_ESPECIAL_w = 'N') and
	(cont_w > 0) then
	ie_retorno_w		:= 'N';
end if;

return	ie_retorno_w;

end OBTER_SE_FLUXO_ESP;
/
