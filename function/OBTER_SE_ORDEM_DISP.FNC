create or replace
function obter_se_ordem_disp(	nr_prescricao_p		number,
				nr_seq_atendimento_p	number default null)
				return varchar2 is


ie_ordem_ger_w	varchar2(1) := 'N';

begin

if	(nr_prescricao_p > 0) then
	select	nvl(max('S'),'N')
	into	ie_ordem_ger_w
	from	can_ordem_prod
	where	nr_prescricao	=	nr_prescricao_p
	and	dt_entrega_setor is not null;
	
	if 	(nvl(ie_ordem_ger_w,'N') = 'N') and 
		(nr_seq_atendimento_p is not null) then 
		select	nvl(max('S'),'N')
		into	ie_ordem_ger_w
		from 	paciente_atendimento 	
		where	dt_retirada_medicacao is not null
		and 	nr_seq_atendimento = nr_seq_atendimento_p;
	end if;
end if;

return	ie_ordem_ger_w;

end obter_se_ordem_disp;
/	