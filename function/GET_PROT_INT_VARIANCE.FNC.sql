create or replace 
function get_prot_int_variance(nr_seq_etapa_p	protocolo_int_pac_resul.nr_seq_etapa%type) return varchar is

qt_variance_w				number(10);
ie_variance_w				varchar(1);

begin
		
	select count(*) 
	into qt_variance_w
	from PROTOCOLO_INT_PAC_VARIANCE 
	where NR_SEQ_EVENTO in (select NR_SEQUENCIA 
						   from PROTOCOLO_INT_PAC_EVENTO 
						   where nr_seq_prt_int_pac_etapa = nr_seq_etapa_p);

	if (qt_variance_w > 0) then
		ie_variance_w := 'S';
	else
		ie_variance_w := 'N';
	end if;
	
return ie_variance_w;
end get_prot_int_variance;
/