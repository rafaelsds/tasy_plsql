create or replace
function proj_obter_risco_conc(nr_sequencia_p	number)
					return varchar2 is

ds_risco_concatenado_w	varchar2(200);

begin

if	(nr_sequencia_p is not null) then

	select	substr(obter_select_concatenado_bv('select proj_obter_desc_risco_impl(c.nr_seq_risco) from proj_risco_impl_ativ c where c.nr_seq_etapa_cron = :nr_sequencia',
						'nr_sequencia='|| nr_sequencia_p,', '),1,200) ds_risco
	into	ds_risco_concatenado_w
	from	dual;
end if;

return	ds_risco_concatenado_w;
end proj_obter_risco_conc;
/
