create or replace
function Obter_obs_proc_prescr
		(nr_seq_proc_interno_p		number,
		cd_procedimento_p		number,
		ie_origem_proced_p		number)
		return varchar2 is

ds_observacao_w		varchar2(2000);

begin

select	max(ds_observacao)
into	ds_observacao_w
from	procedimento_prescricao
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
	

select	nvl(max(ds_observacao), ds_observacao_w)
into	ds_observacao_w
from	proc_interno
where	nr_sequencia = nr_seq_proc_interno_p;


return	ds_observacao_w;

end Obter_obs_proc_prescr;
/