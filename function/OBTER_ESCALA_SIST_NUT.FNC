create or replace
function obter_escala_sist_nut	(nr_atendimento_p 	number)
 		    		return varchar2 is
ds_resultado_w	varchar2(255);
			
begin

select substr(obter_result_escala_sist_nut(e.ie_dietoterapia,e.ie_fator_risco),1,255)
into ds_resultado_w
from escala_sist_nutricional e
where nr_sequencia = (select max(x.nr_sequencia)
      		   from escala_sist_nutricional x
		   where x.nr_atendimento = e.nr_atendimento
		   and	x.dt_liberacao is not null
		   and	x.nr_atendimento = nr_atendimento_p);

return	ds_resultado_w;

end obter_escala_sist_nut;
/