create or replace
function	cm_obter_nota_transferencia(
		nr_seq_lote_transf_p	number,
		ie_transf_receb_p		varchar2,
		ie_opcao_p		varchar2)
		return number is

nr_retorno_w		number(10);
nr_sequencia_w		number(10);
nr_nota_fiscal_w		number(10);

begin

if	(ie_transf_receb_p = 'T') then
	begin

	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	nota_fiscal
	where	nr_seq_transf_cme = nr_seq_lote_transf_p
	and	ie_tipo_nota = 'SE'
	and	ie_situacao = 1;

	if	(nr_sequencia_w > 0) then
		begin

		select	nr_nota_fiscal_w
		into	nr_nota_fiscal_w
		from	nota_fiscal
		where	nr_sequencia = nr_sequencia_w;

		if	(ie_opcao_p = 'N') then
			nr_retorno_w	:= nr_nota_fiscal_w;
		else
			nr_retorno_w	:= nr_sequencia_w;
		end if;

		end;
	end if;

	end;
elsif	(ie_transf_receb_p = 'R') then
	begin

	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	nota_fiscal
	where	nr_seq_transf_cme = nr_seq_lote_transf_p
	and	ie_tipo_nota = 'SE'
	and	ie_situacao = 1;

	if	(nr_sequencia_w > 0) then
		begin

		select	nr_nota_fiscal_w
		into	nr_nota_fiscal_w
		from	nota_fiscal
		where	nr_sequencia = nr_sequencia_w;

		if	(ie_opcao_p = 'N') then
			nr_retorno_w	:= nr_nota_fiscal_w;
		else
			nr_retorno_w	:= nr_sequencia_w;
		end if;

		end;
	end if;

	end;
end if;

return nr_retorno_w;

end cm_obter_nota_transferencia;
/