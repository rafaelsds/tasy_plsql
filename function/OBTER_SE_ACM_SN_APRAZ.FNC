create or replace
function obter_se_acm_sn_apraz	(nr_prescricao_p	number,
				 nr_seq_prescr_p	number)
					return varchar2 is

ie_aprazado_w	varchar2(1) := 'N';
ie_acm_sn_w	number(10);

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_prescr_p is not null) then

	select	nvl(count(*),0)
	into	ie_acm_sn_w
	from 	prescr_procedimento
	where 	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_seq_prescr_p
	and	((ie_acm = 'S') or (ie_se_necessario = 'S'));	
	
	if (ie_acm_sn_w > 0) then
	
		select	decode(count(*),0,'N','S')
		into	ie_aprazado_w
		from	prescr_proc_hor
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_procedimento = nr_seq_prescr_p
		and	ie_aprazado = 'S'
		and	nvl(ie_horario_especial,'N') = 'N'
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
	else
		ie_aprazado_w := 'S';
	end if;

end if;

return ie_aprazado_w;

end obter_se_acm_sn_apraz;
/
