create or replace
function pls_obter_cbo_medico_guia(	cd_pessoa_fisica_p		number,
					cd_especialidade_p		number)
					return number is

nr_seq_cbo_w		tiss_cbo_saude.nr_seq_cbo_saude%type;

begin

select max(nr_seq_cbo_saude)
into	nr_seq_cbo_w
from	tiss_cbo_saude
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	cd_especialidade = cd_especialidade_p
order by ie_versao desc;

if	(nr_seq_cbo_w	is null) then
	select max(nr_seq_cbo_saude)
	into	nr_seq_cbo_w
	from	medico_especialidade
	where	nr_seq_cbo_saude  is not null
	and	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	cd_especialidade = cd_especialidade_p;
end if;

return	nr_seq_cbo_w;

end pls_obter_cbo_medico_guia;
/
