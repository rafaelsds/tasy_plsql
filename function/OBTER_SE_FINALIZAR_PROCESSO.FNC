create or replace
function obter_se_finalizar_processo (nr_seq_area_prep_p	number)
			return varchar2 is

ie_finalizar_w				varchar2(1)	:= 'N';

begin
if	(nr_seq_area_prep_p is not null) then

	select	nvl(ie_finalizar_processo,'N')
	into	ie_finalizar_w
	from	adep_area_prep
	where nr_sequencia	= nr_seq_area_prep_p;

end if;

return ie_finalizar_w;

end obter_se_finalizar_processo;
/