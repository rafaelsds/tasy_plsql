create or replace
function Consistir_Qtd_Classif_Turno(
					nr_Seq_turno_p		Number,
					qt_total_turno_p	Number,
					qt_classif_turno_p	Number,
					nr_seq_classif_turno_p	Number)
 		    	return Varchar2 is

ds_retorno_w		Varchar2(1)	:= 'N';
qt_classif_turno_w	Number(10);
			
begin

if	(nvl(qt_total_turno_p,0)	> 0) then

	select	sum(nvl(qt_classif,0))
	into	qt_classif_turno_w
	from	agenda_turno_classif
	where	nr_seq_turno	= nr_Seq_turno_p
	and	nr_Sequencia	<> nr_seq_classif_turno_p;

	if	((qt_classif_turno_w + nvl(qt_classif_turno_p,0)) > nvl(qt_total_turno_p,0)) then
		ds_retorno_w	:= 'S';
	end if;
end if;
	
return	ds_retorno_w;

end Consistir_Qtd_Classif_Turno;
/