create or replace
function hdjb_obter_regra_prestador (	ie_tipo_atendimento_p	number,
				      	ie_tipo_guia_p		varchar2,
				      	nr_seq_protocolo_p	number,
					cd_medico_resp_p	varchar2)
					return varchar2 is

ds_retorno_w		varchar2(255);
cd_convenio_w		number(5);
cd_estabelecimento_w	number(4);

begin

select 	cd_estabelecimento,
	cd_convenio
into	cd_estabelecimento_w,
	cd_convenio_w
from 	protocolo_convenio
where 	nr_seq_protocolo = nr_seq_protocolo_p;


/*	Substitu�do o Bloco do IF abaixo pela linha abaixo do bloco, pois segundo Victor HDJB, a regra � buscar 1� do cadastro de Conv�nio o m�dico
se n�o encontrar l� trazer o c�digo gen�rico 00050001, OS 88412  10-04-2008 Fabr�cio*/
/*
if	(ie_tipo_atendimento_p = 7) or
	(ie_tipo_guia_p = 'A') then

	ds_retorno_w:=	'00050001';

elsif	(ie_tipo_guia_p in ('I','IC','IO','AI')) then

	ds_retorno_w:= lpad(nvl(substr(obter_medico_convenio(cd_estabelecimento_w,cd_medico_resp_p,cd_convenio_w),1,8),'00050001'),8,0);

end if;
*/

ds_retorno_w:= lpad(nvl(substr(obter_medico_convenio(cd_estabelecimento_w,cd_medico_resp_p,cd_convenio_w, null, null, null, null,null,null,null,null),1,8),'00050001'),8,0);


return ds_retorno_w;

end hdjb_obter_regra_prestador;
/