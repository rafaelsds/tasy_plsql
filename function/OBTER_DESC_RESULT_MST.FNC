create or replace function obter_desc_result_mst(qt_pontuacao_p	number)
			return varchar2 is

begin
if	(qt_pontuacao_p is not null) then
	if	(qt_pontuacao_p <= 1) then	
    --330642 = Baixo risco
		return obter_desc_expressao(330642);
	elsif	(qt_pontuacao_p <= 3) then
    --331179 = M�dio risco
		return obter_desc_expressao(331179);
	else
    --283400 = Alto risco
		return obter_desc_expressao(283400);
	end if;
else
	return '';
end if;

end obter_desc_result_mst;
/
