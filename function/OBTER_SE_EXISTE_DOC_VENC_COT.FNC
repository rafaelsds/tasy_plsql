create or replace
function obter_se_existe_doc_venc_cot(	cd_cnpj_p			varchar2,
					nr_cot_compra_p			number)
return varchar2 is

ie_vencido_w			varchar2(1) := 'N';
qt_existe_w			number(10);
cd_material_w			number(10);
cd_grupo_material_w		number(10);
cd_subgrupo_material_w		number(10);
cd_classe_material_w		number(10);

cursor c01 is
select	cd_material
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_p;

begin

select	count(*)
into	qt_existe_w
from	pessoa_juridica_doc a,
	pessoa_juridica_doc_estrut b
where	a.nr_sequencia = b.nr_seq_documento
and	a.cd_cgc = cd_cnpj_p;

if	(qt_existe_w = 0) then

	select	count(*)
	into	qt_existe_w
	from	pessoa_juridica_doc
	where	cd_cgc = cd_cnpj_p
	and	trunc(dt_revisao,'dd') < trunc(sysdate,'dd')
	and	nvl(ie_consiste_cotacao,'S') = 'S';

	if	(qt_existe_w > 0) then
		ie_vencido_w := 'S';
	end if;
else
	open C01;
	loop
	fetch C01 into	
		cd_material_w;
	exit when C01%notfound;
		begin
		
		select	a.cd_grupo_material,
			a.cd_subgrupo_material,
			a.cd_classe_material
		into	cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w
		from	estrutura_material_v a
		where	a.cd_material = cd_material_w;
		
		select	count(*)
		into	qt_existe_w
		from	pessoa_juridica_doc a,
			pessoa_juridica_doc_estrut b
		where	a.nr_sequencia = b.nr_seq_documento
		and	a.cd_cgc = cd_cnpj_p
		and	trunc(dt_revisao,'dd') < trunc(sysdate,'dd')
		and	nvl(ie_consiste_cotacao,'S') = 'S'		
		and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
		and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
		and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
		and	(nvl(cd_material, cd_material_w) 			= cd_material_w or cd_material_w = 0);
		
		if	(qt_existe_w > 0) then
			ie_vencido_w := 'S';
			exit;
		end if;		
		end;
	end loop;
	close C01;
end if;

return	ie_vencido_w;

end obter_se_existe_doc_venc_cot;
/
