create or replace
function obter_apres_recomendacao_prot(
				nr_sequencia_p		number,
				nr_seq_rec_p		number,
				cd_protocolo_p	 	number)
				return number is

nr_seq_apres_w		number(10);

begin

select	nvl(max(nr_seq_apres),999)
into	nr_seq_apres_w
from	protocolo_medic_rec a
where	a.cd_protocolo	= cd_protocolo_p
and		a.nr_sequencia 	= nr_sequencia_p
and		a.nr_seq_rec	= nr_seq_rec_p;

return nr_seq_apres_w;

end obter_apres_recomendacao_prot;
/
