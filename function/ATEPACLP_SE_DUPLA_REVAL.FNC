create or replace
function atePacLP_se_dupla_reval(nr_seq_reval_rule_p number,
									cd_material_p	number)
				return varchar2 is

ds_retorno_w			varchar2(1) := 'N';

cd_grupo_material_w		estrutura_material_v.cd_grupo_material%type;
cd_subgrupo_material_w	estrutura_material_v.cd_subgrupo_material%type;
cd_classe_material_w	estrutura_material_v.cd_classe_material%type;
ie_possui_registro varchar2(1);

begin

if (cd_material_p is not null) then
  select nvl(max('S'), 'N')
  into ie_possui_registro
  from CPOE_REVALIDATION_DOUBLE
  where NR_SEQ_CPOE_RELAVIDATION = nr_seq_reval_rule_p;
  if(ie_possui_registro = 'S') then
    select 	max(cd_grupo_material),
        max(cd_subgrupo_material),
        max(cd_classe_material)
    into	cd_grupo_material_w,
        cd_subgrupo_material_w,
        cd_classe_material_w
    from 	estrutura_material_v
    where	cd_material = cd_material_p;

    select nvl(max('S'), 'N')
    into	ds_retorno_w
    from	cpoe_revalidation_double
    where	nr_seq_cpoe_relavidation = nr_seq_reval_rule_p
    and		(((cd_material = cd_material_p and cd_material is not null) or cd_material is null)  and
        ((cd_grupo_material = cd_grupo_material_w and cd_grupo_material is not null) or cd_grupo_material is null)  and
        ((cd_subgrupo_material = cd_subgrupo_material_w and cd_subgrupo_material is not null) or cd_subgrupo_material is null)  and
        ((cd_classe_material = cd_classe_material_w and cd_classe_material is not null) or cd_classe_material is null));
  end if;
  if(ie_possui_registro = 'N') then
    ds_retorno_w := 'S';
  end if;

end if;

return ds_retorno_w;

end atePacLP_se_dupla_reval;
/
