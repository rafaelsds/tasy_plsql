create or replace function GET_BLOOD_ALLOCATION_RULE_2_OK(nr_seq_mensagem_p number,
                                                  nr_seq_reserva_p number,
                                                  nr_seq_derivado_p number,
                                                  ie_fator_rh_p varchar2,
                                                  ie_tipo_sangue_p varchar2)
                                                  return number is
                                                  
ie_retorno_w	number := 0;

--nr_seq_mensagem_p = 1 - Request Rh type does not match AND Request blood type does not match
--nr_seq_mensagem_p = 2 - Request blood type does not match
--nr_seq_mensagem_p = 3 - Request Rh type does not match

Cursor C01 is
	select ie_tipo_sangue, ie_fator_rh, nr_seq_derivado from SAN_RESERVA_ITEM where nr_seq_reserva = nr_seq_reserva_p;

c01_w			c01%rowtype;

begin

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	if((nr_seq_mensagem_p = 1 AND c01_w.ie_tipo_sangue <> ie_tipo_sangue_p AND c01_w.ie_fator_rh <> ie_fator_rh_p)
		OR (nr_seq_mensagem_p = 2 AND c01_w.ie_tipo_sangue <> ie_tipo_sangue_p AND c01_w.ie_fator_rh = ie_fator_rh_p)
		OR (nr_seq_mensagem_p = 3 AND c01_w.ie_tipo_sangue = ie_tipo_sangue_p AND c01_w.ie_fator_rh <> ie_fator_rh_p)) then
			ie_retorno_w := 1;
	end if;
    end;
end loop;
close C01;

return ie_retorno_w;

end get_blood_allocation_rule_2_ok;
/
