CREATE OR REPLACE FUNCTION obter_cor_triagem_prioridade (
    nr_seq_fila_senha_p NUMBER
) RETURN VARCHAR2 IS
    nr_seq_triagem_prioridade_w   NUMBER(5);
    ds_cor_w                      VARCHAR2(15) := '';
	
BEGIN

    SELECT
        MAX(nr_seq_triagem_prioridade)
    INTO nr_seq_triagem_prioridade_w
    FROM
        triagem_pronto_atend
    WHERE
        nr_seq_fila_senha = nr_seq_fila_senha_p;

    IF ( nr_seq_triagem_prioridade_w > 0 ) THEN
        BEGIN
            SELECT
                MAX(ds_cor)
            INTO ds_cor_w
            FROM
                triagem_classif_prioridade
            WHERE
                nr_sequencia = nr_seq_triagem_prioridade_w
                AND cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
                AND ie_situacao = 'A';

        END;

    END IF;

    RETURN ds_cor_w;
	
END obter_cor_triagem_prioridade;
/