create or replace
function obter_descricao_etapa(nr_seq_etapa_p Number)
 		    	return varchar is

ds_etapa_w	varchar2(80);			
			
begin

select 	nvl(max(ds_etapa),Wheb_mensagem_pck.get_texto(799238))
into	ds_etapa_w
from 	fatur_etapa
where 	nr_sequencia = nr_seq_etapa_p;

return	ds_etapa_w;

end obter_descricao_etapa;
/
