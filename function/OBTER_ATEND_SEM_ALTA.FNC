create or replace
function obter_atend_sem_alta(cd_pessoa_fisica_p	varchar2)
 		    	return number is

nr_atendimento_w	number(10,0);
		
			
begin

select	nvl(max(nr_atendimento),0)
into	nr_atendimento_w
from	atendimento_paciente
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	dt_alta is null;

return	nr_atendimento_w;

end obter_atend_sem_alta;
/