create or replace function obter_desc_escala_aws(	qt_score_p	number)

					return varchar is

ds_retorno_w	varchar2(100);



begin


if	(qt_score_p >= 0 and qt_score_p <= 4) then

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(1074272); -- Mild

elsif (qt_score_p >= 5 and qt_score_p <= 14) then

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(1074273); -- Moderate

elsif (qt_score_p >=15 ) then

	ds_retorno_w	:= wheb_mensagem_pck.get_texto(1074274); -- Severe
  
end if;

return ds_retorno_w;

end obter_desc_escala_aws;
/