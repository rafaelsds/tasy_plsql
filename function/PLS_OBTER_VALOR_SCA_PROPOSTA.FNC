create or replace
function pls_obter_valor_sca_proposta
		(	nr_seq_proposta_p		number,
			nr_seq_prop_benef_p		number,
			nr_seq_sca_p			number)
	 	   	return number is

vl_retorno_w		number(15,2) := 0;
nr_seq_tabela_sca_w	number(15,2);
cd_beneficiario_w	varchar2(20);
qt_idade_w		number(3);
nr_seq_segurado_w	number(10);
vl_sca_w		number(15,2) := 0;
ie_grau_parentesco_w	varchar2(20);

Cursor C01 is
	select	b.nr_sequencia,
		a.nr_seq_tabela,
		b.cd_beneficiario
	from	pls_sca_vinculo			a,
		pls_proposta_beneficiario	b
	where	a.nr_seq_benef_proposta	= b.nr_sequencia
	and	((b.nr_seq_proposta = nvl(nr_seq_proposta_p,0)) or
		 (b.nr_sequencia = nvl(nr_seq_prop_benef_p,0)))
	and	a.nr_seq_plano	= nr_seq_sca_p
	and	b.dt_cancelamento is null;

Cursor C02 is
	select	nvl(vl_preco_atual,0)
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_sca_w
	and	qt_idade_w between qt_idade_inicial and qt_idade_final
	and	nvl(ie_grau_titularidade,ie_grau_parentesco_w)	= ie_grau_parentesco_w
	order	by	nvl(ie_grau_titularidade,' ');

begin

open C01;
loop
fetch C01 into
	nr_seq_segurado_w,
	nr_seq_tabela_sca_w,
	cd_beneficiario_w;
exit when C01%notfound;
	begin
	ie_grau_parentesco_w	:= nvl(substr(pls_obter_garu_dependencia_seg(nr_seq_segurado_w,'P'),1,2),'X');
	
	select	substr(obter_idade_pf(cd_beneficiario_w,sysdate,'A'),1,3)
	into	qt_idade_w
	from	dual;
	
	open c02;
	loop
	fetch c02 into
		vl_sca_w;
	exit when c02%notfound;
	end loop;
	close c02;
	
	vl_retorno_w	:= vl_retorno_w + vl_sca_w;
	end;
end loop;
close C01;

return	vl_retorno_w;

end pls_obter_valor_sca_proposta;
/