create or replace
function Obter_qt_mat_autor_quimio(nr_seq_atendimento_p number, nr_seq_interno_p number)
 		    	return number is


nr_seq_autor_w			number(10,0) := 0;
qt_retorno_w			number(15,3) := 0;
nr_ciclo_w			number(5,0) := 0;
ds_dia_ciclo_w			varchar2(5);
nr_seq_paciente_w		number(10,0);
cd_material_w			number(10,0);


begin
select	nr_ciclo,
	ds_dia_ciclo,
	nr_seq_paciente
into	nr_ciclo_w,
	ds_dia_ciclo_w,
	nr_seq_paciente_w
from	paciente_atendimento
where	nr_seq_atendimento = nr_seq_atendimento_p;

select	cd_material
into	cd_material_w
from	paciente_atend_medic
where 	nr_seq_interno = nr_seq_interno_p;



begin
select	a.nr_sequencia
into	nr_seq_autor_w
from	autorizacao_convenio a,
	estagio_autorizacao b
where	a.nr_seq_paciente = nr_seq_atendimento_p
and	b.nr_sequencia	 = a.nr_seq_estagio
and	b.ie_interno = '10';
exception
when others then
	nr_seq_autor_w := 0;
end;

if	(nvl(nr_seq_autor_w,0) = 0) then
	begin
	select	max(a.nr_sequencia)
	into	nr_seq_autor_W
	from	autorizacao_convenio a,
		estagio_autorizacao b
	where	a.nr_seq_paciente_setor = nr_seq_paciente_w
	and	a.nr_ciclo	= nr_ciclo_w
	and	a.nr_seq_estagio = b.nr_sequencia
	and	b.ie_interno = '10';
	end;
end if;

if	(nr_seq_autor_w > 0) then
	begin
	select	sum(qt_autorizada)
	into	qt_retorno_w
	from	material_autorizado
	where	nr_sequencia_autor = nr_seq_autor_w
	and	cd_material	= cd_material_w;
	end;
end if;	
	
return	qt_retorno_w;

end Obter_qt_mat_autor_quimio;
/