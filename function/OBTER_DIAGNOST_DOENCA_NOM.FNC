create or replace
function obter_diagnost_doenca_nom(nr_atendimento_p	Number)
 		    	return varchar2 is
			
ds_retorno_w    varchar2(2000);

begin
  ds_retorno_w := '';
  
  if (nr_atendimento_p is not null) then
    select  upper(max(cd.ds_doenca_cid))
    into ds_retorno_w 
    from diagnostico_doenca dd, cid_doenca cd 
    where dd.nr_atendimento = nr_atendimento_p 
    and dd.cd_doenca = cd.cd_doenca_cid 
    and dd.ie_classificacao_doenca = 'P' ;
	
  end if;

return	ds_retorno_w;

end obter_diagnost_doenca_nom ;
/