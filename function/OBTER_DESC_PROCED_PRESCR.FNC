create or replace
function obter_desc_proced_prescr	(nr_prescricao_p	number,
				nr_seq_proced_p	number)
				return varchar2 is
					
ds_proced_w	PROCEDIMENTO.DS_PROCEDIMENTO%type;
					
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_proced_p is not null) then
	
	select	substr(obter_desc_prescr_proc(cd_procedimento,ie_origem_proced,nr_seq_proc_interno),1,240)
	into	ds_proced_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_seq_proced_p;
	
end if;

return ds_proced_w;

end obter_desc_proced_prescr;
/