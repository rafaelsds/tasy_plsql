create or replace
function pls_obter_se_mat_estrut_conv (	nr_seq_material_p	pls_material.nr_sequencia%type,
					nr_seq_estrutura_p	pls_estrutura_material.nr_sequencia%type,
					cd_material_p		pls_material.cd_material_ops%type)
 		    	return varchar2 is

ie_retorno_w		varchar2(1)	:= 'N';
qt_registro_w		pls_integer;
	
begin

if	(nr_seq_material_p is not null) and
	(nr_seq_estrutura_p is not null) then
	
	-- verifica se a estrutura passada de par�metro (nr_seq_estrutura_p) est� na estrutura
	-- ou na �rvore da estrutura do material
	-- usada a tabela tm que cont�m toda a �rvore de estrutura do material
	select	count(1)
	into	qt_registro_w
	from	pls_estrutura_material_tm
	where	nr_seq_estrutura = nr_seq_estrutura_p
	and	nr_seq_material = nr_seq_material_p;
	
	-- Caso n�o encontre pela sequencia do material busca pelo c�digo do materail na operadora
	if	(qt_registro_w = 0) then
		select	count(1)
		into	qt_registro_w
		from	pls_estrutura_material_tm a
		where	a.nr_seq_estrutura = nr_seq_estrutura_p
		and	exists(	select	1
				from	pls_material x
				where	x.nr_sequencia = a.nr_seq_material
				and	cd_material_ops = cd_material_p);
		
		-- Busca pelo c�digo brasindice
		if	(qt_registro_w = 0) then
			select	count(1)
			into	qt_registro_w
			from	pls_estrutura_material_tm a
			where	a.nr_seq_estrutura = nr_seq_estrutura_p
			and	exists(	select	1
					from	pls_material x
					where	x.nr_sequencia = a.nr_seq_material
					and	x.cd_tiss_brasindice = cd_material_p);
			
			-- Busca pelo c�digo Simpro
			if	(qt_registro_w = 0) then
							
				begin
				
					select	count(1)
					into	qt_registro_w
					from	pls_estrutura_material_tm a
					where	a.nr_seq_estrutura = nr_seq_estrutura_p
					and	exists(	select	1
							from	pls_material x
							where	x.nr_sequencia = a.nr_seq_material
							and	x.cd_simpro = cd_material_p );
							
				exception
				when others then
					qt_registro_w := 0;
				end;
					
				-- Busca pelo c�digo material TUSS
				-- Colocado por �ltimo pois � o mais custoso
				if	(qt_registro_w = 0) then
				
					begin
						select	count(1)
						into	qt_registro_w
						from	pls_estrutura_material_tm a
						where	a.nr_seq_estrutura = nr_seq_estrutura_p
						and	exists(	select	1
								from	pls_material x,
									tuss_material_item y
								where	y.nr_sequencia = x.nr_seq_tuss_mat_item
								and	x.nr_sequencia = a.nr_seq_material
								and	y.cd_material_tuss = cd_material_p );
					exception
					when others then
						qt_registro_w := 0;
					end;
				
				end if;				
			end if;
		end if;
	end if;
	
	-- Caso encontre, � v�lido
	if	(qt_registro_w > 0) then
		ie_retorno_w		:= 'S';
	end if;
end if;
	
return	ie_retorno_w;

end pls_obter_se_mat_estrut_conv;
/
