CREATE OR REPLACE
FUNCTION Obter_Cod_Municipio_IBGE
			(cd_cep_p		varchar2)	
			RETURN VarChar2 IS

nr_localidade_w		number(06,0)	:= 0;
cd_cep_w		number(08,0)	:= 0;
cd_municipio_ibge_w	varchar2(6)	:= '';
ie_cep_novo_w		Varchar2(01)	:= '';


begin

select	nvl(vl_parametro, vl_parametro_padrao)
into	ie_cep_novo_w
from	funcao_parametro
where	cd_funcao	= 0
and	nr_sequencia	= 25;

if	(ie_cep_novo_w		= 'S') then
	begin

	select	nvl(max(a.nr_seq_loc),0)
	into	nr_localidade_w
	from	cep_log a
	where	a.cd_cep		= Somente_Numero(cd_cep_p);

	if	(nr_localidade_w > 0) then
		begin

		select	nvl(max(cd_cep),0)
		into	cd_cep_w
		from	cep_loc
		where	nr_sequencia	= nr_localidade_w;

		if	(cd_cep_w > 0) then
			begin
			
			select	a.cd_municipio_ibge
			into	cd_municipio_ibge_w
			from	cep_municipio b, sus_municipio a
			where	a.cd_municipio_sinpas = b.cd_municipio
			and	b.cd_cep	= cd_cep_w;


			end;
		end if;

		end;
	end if;

	end;
end if;

RETURN cd_municipio_ibge_w;

END Obter_Cod_Municipio_IBGE;
/
