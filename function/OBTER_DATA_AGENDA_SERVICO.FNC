create or replace
function obter_data_agenda_servico(nr_seq_agenda_p number)
				return date is

ds_retorno_w		date;

begin

if	(nvl(nr_seq_agenda_p,0) > 0) then

	select  	max(dt_agenda)
	into	ds_retorno_w
	from 	agenda_consulta
	where	nr_seq_agepaci = nr_seq_agenda_p;
end if;	

return	ds_retorno_w;

end obter_data_agenda_servico;
/
