create or replace
function lote_ent_calc_criterio(nr_seq_padrao_p		number,
				nr_seq_exame_padrao_p	number,
				nr_prescricao_p		number,
				ie_opcao_p		number)
 		    	return varchar2 is

nr_prioridade_w					number(10);
ds_mensagem_w					varchar2(4000);
ie_acao_criterio_w				varchar2(1);	

--Variaveis da lote_ent_sec_ficha
lote_qt_idade_gest_w			number(10);
lote_ie_amamentado_w			varchar2(1);
lote_ie_prematuro_w				varchar2(1);
lote_ie_transfundido_w			varchar2(1);
lote_qt_peso_w					number(10,3);
lote_nr_seq_classif_w			number(10);
lote_nr_seq_grau_w				number(10);
lote_ie_gerar_busca_ativa_w		varchar2(1);
lote_ie_gerar_busca_passiva_w	varchar2(1);
lote_ie_mae_veg_w				varchar2(1);
lote_ie_ictericia_w				varchar2(1);
lote_ie_npp_w					varchar2(1);
lote_ie_cor_pf_w				varchar2(10);
lote_ie_gemelar_w				varchar2(1);
lote_qt_media_w					varchar2(10);
lote_qt_gest_w					number(2);
lote_dt_ult_menst_w				date;
lote_ie_tipo_parto_w			varchar2(1);
lote_ie_alim_leite_w			varchar2(1);
ie_classif_lote_ent_w			number(3);
nr_seq_atecaco_w				number(10,0);
lote_cd_convenio_w				number(5);
lote_pessoa_fisica_w			varchar2(10);
lote_medic_uso_w				number(10);
lote_cd_municipio_w				varchar2(6);
lote_ie_tipo_coleta_w			varchar2(1);
lote_ie_data_coleta_w			date;
lote_ie_hora_coleta_w			date;
lote_ie_data_nascimento_w		date;
lote_ie_hora_nascimento_w		date;
	
ds_retorno_w					varchar2(4000);
nr_atendimento_w				number(10);
qt_dias_prev_w					number(10);
ie_tipo_busca_w					varchar2(1);
ds_resultado_sugerido_w			varchar2(255);
nr_seq_resultado_w				number(10);
nr_seq_prescr_w					number(10);
nr_seq_material_w				number(10);
nr_seq_metodo_w					number(10);
ie_corticoide_f_w				lab_valor_padrao_criterio.ie_corticoide%type;
			
begin

/*
ie_opcao_p:
1 - Retorna o DS_MENSAGEM da tabela LAB_VALOR_PADRAO_CRITERIO
2 - Retorna o IE_ACAO_CRITERIO (Descri��o) da tabela LAB_VALOR_PADRAO_CRITERIO
3 - Retornar o QT_DIAS_PREV da tabela LAB_VALOR_PADRAO_CRITERIO
4 - Retorna o tipo de busca que dever� ser realizado ao reconvocar o exame segundo o criterio
5 - Retorna o c�d  IE_ACAO_CRITERIO
6 - Retorna resultado sugerido
7 - Retorna o material crit�rio
8 - Retorna o m�todo crit�rio
*/

--return nr_seq_padrao_p||' - '||nr_seq_exame_padrao_p||' - '||nr_prescricao_p;
--Para buscar o de maior prioridade caso houverem outros cadastrados para o mesmo item
select	nvl(min(nr_seq_prioridade),0)
into	nr_prioridade_w
from	lab_valor_padrao_criterio
where	nr_seq_padrao = nr_seq_padrao_p
and		nr_seq_exame = nr_seq_exame_padrao_p;

select	max(nr_seq_resultado)
into	nr_seq_resultado_w
from	exame_lab_resultado
where 	nr_prescricao = nr_prescricao_p;

select	max(nr_atendimento)
into	nr_atendimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select	max(nr_sequencia)
into	nr_seq_prescr_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and		nr_seq_exame = nr_seq_exame_padrao_p;

select	max(qt_peso_f),
		max(nr_idade_gest_f),
		max(ie_premat_s_f),
		max(IE_AMAMENTADO_F),
		max(ie_transfusao_f),
		max(nr_seq_grau_parentesco),
		max(substr(obter_classif_atendimento(nr_atendimento_w),1,10)),
		max(IE_MAE_VEG_F),
		max(IE_ICTERICIA_F),
		max(IE_NPP_F),
		max(IE_COR_PF_F),
		max(IE_GEMELAR_F),
		max(substr(lote_ent_obter_result_ant(nr_prescricao_p, nr_seq_prescr_w, nr_seq_resultado_w, nr_seq_exame_padrao_p, 99),1,255)),
		max(QT_GEST_F),
		max(DT_ULT_MENST),
		max(IE_TIPO_PARTO),
		max(IE_ALIM_LEITE_F),
		max(cd_pessoa_fisica),
		max(ie_corticoide_f),
		max(DT_COLETA_FICHA_F),
		max(HR_COLETA_F),
		max(DT_NASCIMENTO_F),
		max(HR_NASCIMENTO_F)		
into	lote_qt_peso_w,
		lote_qt_idade_gest_w,
		lote_ie_prematuro_w,
		lote_ie_amamentado_w,
		lote_ie_transfundido_w,
		lote_nr_seq_grau_w,
		lote_nr_seq_classif_w,
		lote_ie_mae_veg_w,
		lote_ie_ictericia_w,
		lote_ie_npp_w,
		lote_ie_cor_pf_w,
		lote_ie_gemelar_w,
		lote_qt_media_w,
		lote_qt_gest_w,
		lote_dt_ult_menst_w,
		lote_ie_tipo_parto_w,
		lote_ie_alim_leite_w,
		lote_pessoa_fisica_w,
		ie_corticoide_f_w,
		lote_ie_data_coleta_w,
		lote_ie_hora_coleta_w,
		lote_ie_data_nascimento_w,
		lote_ie_hora_nascimento_w
from	lote_ent_sec_ficha
where	nr_prescricao = nr_prescricao_p;	

select	max(ie_classif_lote_ent)
into	ie_classif_lote_ent_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;

select	Obter_Atecaco_atendimento(nr_atendimento_w)
into	nr_seq_atecaco_w
from	dual;

if (nvl(nr_seq_atecaco_w,0) <> 0) then

	select	max(cd_convenio)
	into	lote_cd_convenio_w
	from	atend_categoria_convenio
	where	nr_seq_interno = nr_seq_atecaco_w;

end if;

select 	max(cd_material)
into	lote_medic_uso_w
from	paciente_medic_uso
where	cd_pessoa_fisica = lote_pessoa_fisica_w;

select	max(cd_municipio_ibge)
into	lote_cd_municipio_w
from	pessoa_fisica
where	cd_pessoa_fisica = lote_pessoa_fisica_w;

select	max(a.ie_tipo_coleta)
into	lote_ie_tipo_coleta_w
from	lote_ent_sec_ficha_exam a,
		lote_ent_sec_ficha b
where	a.nr_seq_exame = nr_seq_exame_padrao_p
and		a.nr_seq_ficha = b.nr_sequencia
and		b.nr_prescricao = nr_prescricao_p;

select	max(ds_mensagem),
		max(ie_acao_criterio),
		max(qt_dias_prev),
		max(nvl(IE_GERAR_BUSCA_ATIVA,'A')),
		max(ds_result_sugerido),
		max(nr_seq_material),
		max(nr_seq_metodo)
into	ds_mensagem_w,
		ie_acao_criterio_w,
		qt_dias_prev_w,
		ie_tipo_busca_w,
		ds_resultado_sugerido_w,
		nr_seq_material_w,
		nr_seq_metodo_w
from	lab_valor_padrao_criterio
where	nr_seq_padrao = nr_seq_padrao_p
and		nr_seq_prioridade = nr_prioridade_w
and		nr_seq_exame = nr_seq_exame_padrao_p
and		((lote_qt_peso_w between qt_peso_inic and qt_peso_fim) or (nvl(lote_qt_peso_w,'0') = '0') or (qt_peso_inic is null) or (qt_peso_fim is null))
and		((lote_qt_idade_gest_w between QT_IDADE_GESTACIONAL and QT_IDADE_GEST_FIM) or (nvl(lote_qt_idade_gest_w,0) = 0) or (QT_IDADE_GESTACIONAL is null) or (QT_IDADE_GEST_FIM is null))
and		((lote_ie_prematuro_w = nvl(ie_prematuro,lote_ie_prematuro_w)) or (nvl(lote_ie_prematuro_w,'I') = 'I'))
and		((lote_ie_amamentado_w = nvl(ie_amamentado,lote_ie_amamentado_w)) or (nvl(lote_ie_amamentado_w,'I') = 'I'))
and		((lote_ie_transfundido_w = nvl(ie_transfundido,lote_ie_transfundido_w)) or (nvl(lote_ie_transfundido_w,'I') = 'I'))
and		((lote_nr_seq_grau_w = nvl(nr_seq_grau_parentesco,lote_nr_seq_grau_w)) or (nvl(lote_nr_seq_grau_w,0) = 0))
and		((lote_nr_seq_classif_w = nvl(nr_seq_classificacao,lote_nr_seq_classif_w)) or (nvl(lote_nr_seq_classif_w,0) = 0))
and 	((lote_ie_mae_veg_w = nvl(IE_MAE_VEG,lote_ie_mae_veg_w)) or (nvl(lote_ie_mae_veg_w,'I') = 'I'))
and 	((lote_ie_ictericia_w = nvl(IE_ICTERICIA,lote_ie_ictericia_w)) or (nvl(lote_ie_ictericia_w,'I') = 'I'))
and 	((lote_ie_npp_w = nvl(IE_NPP,lote_ie_npp_w)) or (nvl(lote_ie_npp_w,'I') = 'I'))
and 	((lote_ie_cor_pf_w = nvl(IE_COR_PF,lote_ie_cor_pf_w)) or (nvl(lote_ie_cor_pf_w,'N') = 'N'))
and 	((lote_ie_gemelar_w = nvl(IE_GEMELAR,lote_ie_gemelar_w)) or (nvl(lote_ie_gemelar_w,'N') = 'N'))
and		(((QT_MINIMA is not null and QT_MAXIMA is not null and lote_qt_media_w is not null) and (lote_qt_media_w  between QT_MINIMA and QT_MAXIMA)) or	(1=1))
and		((lote_qt_gest_w between QT_GEST_INI and QT_GEST_FIM) or (nvl(lote_qt_gest_w,'0') = '0') or (QT_GEST_INI is null) or (QT_GEST_FIM is null))
and		((lote_dt_ult_menst_w is null) or (lote_dt_ult_menst_w between DT_ULT_MENST_INI and DT_ULT_MENST_FIM))
and		((lote_ie_tipo_parto_w = nvl(IE_TIPO_PARTO,lote_ie_tipo_parto_w)) or (nvl(lote_ie_tipo_parto_w,'N') = 'N'))
and 	((lote_ie_alim_leite_w = nvl(IE_ALIM_LEITE,lote_ie_alim_leite_w)) or (nvl(lote_ie_alim_leite_w,'I') = 'I'))
and	((ie_classif_lote_ent_w = IE_CLASSIF_LOTE_ENT) or (nvl(ie_classif_lote_ent_w,0) = 0))
and		((lote_cd_convenio_w = nvl(CD_CONVENIO,lote_cd_convenio_w)) or (nvl(lote_cd_convenio_w,0) = 0))
and		((lote_medic_uso_w = nvl(CD_MATERIAL_MED,lote_medic_uso_w)) or (nvl(lote_medic_uso_w,0) = 0))
and 	((lote_cd_municipio_w = nvl(CD_MUNICIPIO_IBGE,lote_cd_municipio_w)) or (nvl(lote_cd_municipio_w,'0') = '0'))
and 	((lote_ie_tipo_coleta_w = nvl(IE_TIPO_COLETA,lote_ie_tipo_coleta_w)) or (nvl(lote_ie_tipo_coleta_w,'N') = 'N'))
AND		((ie_corticoide_f_w = nvl(ie_corticoide,ie_corticoide_f_w)) OR (NVL(ie_corticoide_f_w,'0') = '0'))
and 	(((lote_ie_data_coleta_w is null) and (nvl(ie_data_coleta, 'N') = 'I')) or (nvl(ie_data_coleta, 'N') = 'N'))
and 	(((lote_ie_hora_coleta_w is null) and (nvl(ie_hora_coleta, 'N') = 'I')) or (nvl(ie_hora_coleta, 'N') = 'N'))
and 	(((lote_ie_data_nascimento_w is null) and (nvl(ie_data_nascimento, 'N') = 'I')) or (nvl(ie_data_nascimento, 'N') = 'N'))
and 	(((lote_ie_hora_nascimento_w is null) and (nvl(ie_hora_nascimento, 'N') = 'I')) or (nvl(ie_hora_nascimento, 'N') = 'N'))
order by nr_seq_prioridade asc;

if (ie_opcao_p = 1) 	then
	ds_retorno_w 	:=	ds_mensagem_w;
elsif (ie_opcao_p = 2) 	then
	if 	(ie_acao_criterio_w = 'T')	then
		ds_retorno_w	:=	Obter_Desc_Expressao(349095);
	elsif	(ie_acao_criterio_w = 'R')	then
		ds_retorno_w	:=	Obter_Desc_Expressao(349096);
	elsif	(ie_acao_criterio_w = 'N')	then
		ds_retorno_w	:=	Obter_Desc_Expressao(349097);		
	elsif	(ie_acao_criterio_w = 'D')	then
		ds_retorno_w	:=	Obter_Desc_Expressao(349098);		
	end if;
elsif (ie_opcao_p = 3) then
	ds_retorno_w 	:= to_char(qt_dias_prev_w);
elsif (ie_opcao_p = 4) then
	ds_retorno_w := ie_tipo_busca_w;
elsif (ie_opcao_p = 5) then
	ds_retorno_w := ie_acao_criterio_w;
elsif (ie_opcao_p = 6) then
	ds_retorno_w := ds_resultado_sugerido_w;
elsif (ie_opcao_p = 7) then
	ds_retorno_w := substr(Obter_Material_Exame_Lab(nr_seq_material_w,null,3),1,255);
elsif (ie_opcao_p = 8) then
	ds_retorno_w := substr(Obter_Metodo_Exame_Lab(nr_seq_metodo_w,2),1,255);	
end if;

return	ds_retorno_w;

end lote_ent_calc_criterio;
/
