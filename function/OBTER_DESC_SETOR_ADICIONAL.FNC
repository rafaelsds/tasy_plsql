CREATE OR REPLACE
FUNCTION Obter_desc_setor_adicional
			(ds_codigo_p	Varchar2)
			RETURN		Varchar2 IS

lista_setores_w		lista_varchar_pck.tabela_varchar;
			
ds_codigo_w	Varchar2(2000);
ds_retorno_w	Varchar2(4000) := '';
ds_setor_w	Varchar2(100);


BEGIN
ds_codigo_w	:= ds_codigo_p;

if	(ds_codigo_w is not null) then

	lista_setores_w := obter_lista_string2(ds_codigo_w, ',');
	
	for	i in lista_setores_w.first..lista_setores_w.last loop						 						
			begin
			select	obter_nome_setor(to_number(lista_setores_w(i)))
			into	ds_setor_w
			from	dual;

			ds_retorno_w	:= ds_setor_w ||', '||ds_retorno_w;
			exception
			when others then
				ds_retorno_w := ds_retorno_w; 	
			end;
	end loop;
	
end if;

RETURN ds_retorno_w;
END Obter_desc_setor_adicional;
/