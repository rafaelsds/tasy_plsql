create or replace
function gpi_obter_se_etapa_em_atraso(	nr_seq_superior_p	number)
					return varchar2 is

ie_retorno_w		varchar2(1)	:= 'N';
nr_seq_superior_w	number(10);
qt_registro_w		number(10);	
ie_titulo_w		varchar2(1);

begin

ie_titulo_w	:= substr(nvl(gpi_obter_se_etapa_titulo(nr_seq_superior_p),'N'),1,1);
nr_seq_superior_w	:= nr_seq_superior_p;
if	(ie_titulo_w = 'S') then
	begin

	select 	count(*)
	into	qt_registro_w
	from	gpi_cron_etapa
	where	nr_seq_superior = nr_seq_superior_w
	and 	dt_fim_prev < sysdate
	and	pr_etapa    <> 100;
	
	if	(qt_registro_w > 0) then
		ie_retorno_w	:= 'S';
	end if;
	end;
end if;

return	ie_retorno_w;

end gpi_obter_se_etapa_em_atraso;
/
