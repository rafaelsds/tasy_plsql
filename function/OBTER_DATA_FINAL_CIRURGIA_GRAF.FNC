create or replace
function obter_data_final_cirurgia_graf(	nr_cirurgia_p	number)
						return date is

dt_termino_w			date;
dt_termino_prevista_w	date;
dt_retorno_w			date;
dt_retorno_ww			date;
nr_cirurgia_w			number(10,0);
nr_seq_evento_fim_w		varchar2(10);
ie_finaliza_evento_w	varchar2(1);
dt_registro_w			date;
ie_grafico_novo_w		varchar2(1);

begin

obter_param_usuario(872, 272, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, nr_seq_evento_fim_w);
obter_param_usuario(872, 473, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_finaliza_evento_w);
obter_param_usuario(872, 431, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_grafico_novo_w);

if	(nr_seq_evento_fim_w is not null) then
	select	max(dt_registro)
	into	dt_registro_w
	from 	evento_cirurgia_paciente
	where 	nr_cirurgia 		= nr_cirurgia_p
	and	nr_seq_evento 		= campo_numerico(nr_seq_evento_fim_w)
	and	nvl(ie_situacao,'A') 	= 'A';
end if;

select	nvl(max(nr_cirurgia),0) 
into	nr_cirurgia_w
from	cirurgia
where	nr_cirurgia_superior = nr_cirurgia_p;

select	dt_termino,
	dt_termino_prevista
into	dt_termino_w,
	dt_termino_prevista_w
from	cirurgia
where	nr_cirurgia	=	nr_cirurgia_p;

if	(dt_registro_w is not null) then
	dt_retorno_w	:= dt_registro_w;	
elsif	(dt_registro_w is null) and 
	(ie_finaliza_evento_w = 'S') and 
	(dt_termino_prevista_w >= sysdate) then
	/*Altera��es do novo gr�fico, OS 953871*/
	if	(ie_grafico_novo_w = 'S') and (dt_termino_prevista_w > sysdate + 1/24) then
		dt_retorno_w := sysdate + 1/24;
	else	
		dt_retorno_w	:= dt_termino_prevista_w;
	end if;		
elsif	(dt_registro_w is null) and 
	(ie_finaliza_evento_w = 'S') and 
	(dt_termino_prevista_w < sysdate) then
	dt_retorno_w	:= sysdate + (1/24);	
elsif	(dt_termino_w is null) and 
	(dt_termino_prevista_w < sysdate) then	
	dt_retorno_w	:= sysdate + (1/24);	
elsif	(dt_termino_w is null) and 
	(dt_termino_prevista_w >= sysdate) then
	/*Altera��es do novo gr�fico, OS 953871*/
	if	(ie_grafico_novo_w = 'S') and (dt_termino_prevista_w > sysdate + 1/24) then
		dt_retorno_w := sysdate + 1/24;
	else	
		dt_retorno_w	:= dt_termino_prevista_w;
	end if;	
elsif	(dt_termino_w is not null) then
	dt_retorno_w	:= dt_termino_w;	
end if;

if	(nr_cirurgia_w > 0) then
	select	dt_termino,
		dt_termino_prevista
	into	dt_termino_w,
		dt_termino_prevista_w
	from	cirurgia
	where	nr_cirurgia	=	nr_cirurgia_w;

	if	(dt_termino_w is null) and 
		(dt_termino_prevista_w < sysdate) then	
		dt_retorno_ww	:= sysdate + (1/24);		
	elsif	(dt_termino_w is null) and 
		(dt_termino_prevista_w >= sysdate) then
		/*Altera��es do novo gr�fico, OS 953871*/
		if	(ie_grafico_novo_w = 'S') and (dt_termino_prevista_w > sysdate + 1/24) then
			dt_retorno_w := sysdate + 1/24;
		else	
			dt_retorno_w	:= dt_termino_prevista_w;
	end if;		
	elsif	(dt_termino_w is not null) then
		dt_retorno_ww	:= dt_termino_w;		
	end if;
	
	if	(dt_retorno_ww	> dt_retorno_w) then
		dt_retorno_w	:= dt_retorno_ww;		
	end if;
end if;

return	nvl(dt_retorno_w, sysdate);

end obter_data_final_cirurgia_graf;
/
