create or replace
FUNCTION lab_obter_qt_dias_dif(qtde_horas_p	NUMBER)
		RETURN VARCHAR2 IS

hora_formatada_com_dias_w	VARCHAR(100);
ds_horas_resto_w		VARCHAR2(60);
qtde_dias			NUMBER(15,5);
horas_sobrando			NUMBER(15,5);
qt_segundos_p			NUMBER(15,5);

BEGIN

if (qtde_horas_p is not null) then

	qtde_dias := (qtde_horas_p)/24;
	horas_sobrando := qtde_horas_p - ((trunc(qtde_dias,0))*24);
	qt_segundos_p := horas_sobrando*3600;
	ds_horas_resto_w := trim(REPLACE(TO_CHAR(FLOOR(dividir(dividir(qt_segundos_p,60),60)),'999,999,900'),',','.'))
			|| ':' || SUBSTR(TO_CHAR(FLOOR(MOD(dividir(qt_segundos_p,60),60)),'00'),2,2)
			|| ':' || SUBSTR(TO_CHAR(MOD(qt_segundos_p,60),'00'),2,2);
	hora_formatada_com_dias_w := ((trunc(qtde_dias,0))||' dia(s) ') ||' '|| ds_horas_resto_w;

	RETURN	hora_formatada_com_dias_w;

end if;

end lab_obter_qt_dias_dif;
/