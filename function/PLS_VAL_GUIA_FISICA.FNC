create or replace function pls_val_guia_fisica(	nr_seq_lote_p		pls_protocolo_conta.nr_seq_lote_conta%type,
												nr_seq_protocolo_p	pls_protocolo_conta.nr_sequencia%type)
				return varchar2 is

ds_retorno_w			varchar2(1);
qt_regra_w				pls_integer;
nr_seq_regra_w			pls_cta_reg_apres.nr_sequencia%type;
ie_origem_protocolo_w	pls_protocolo_conta.ie_origem_protocolo%type;
ie_tipo_guia_w			pls_protocolo_conta.ie_tipo_guia%type;
ie_status_w				pls_protocolo_conta.ie_status%type;
ie_guia_fisica_w		pls_protocolo_conta.ie_guia_fisica%type;
ie_apresentacao_w		pls_protocolo_conta.ie_apresentacao%type;
ie_apres_ok_w			varchar2(1);

Cursor C01 (nr_seq_regra_pc	pls_cta_reg_apres.nr_sequencia%type) is
	select	ie_origem_protocolo,
			ie_tipo_guia,
			nvl(ie_apresentacao,'AA') ie_apresentacao
	from	pls_cta_reg_apres_prot
	where	nr_seq_regra_vl_apres = nr_seq_regra_pc;
	
Cursor C02 (nr_seq_lote_conta_pc	pls_protocolo_conta.nr_seq_lote_conta%type) is
	select	ie_origem_protocolo,
			ie_tipo_guia,
			nvl(ie_guia_fisica, 'N') ie_guia_fisica,
			nvl(ie_apresentacao, 'A') ie_apresentacao
	from	pls_protocolo_conta
	where	nr_seq_lote_conta = nr_seq_lote_conta_pc
	and		ie_status in ('1', '2', '3', '5', '7');

begin
	ds_retorno_w	:= 'S';

--Procura por uma regra que valide a guia fisica
select	max(nr_sequencia)
into	nr_seq_regra_w
from	pls_cta_reg_apres
where	ie_valida_guia_fisica = 'S';

--Caso n�o tenha ja sai da function retornando 'S'
if	(nr_seq_regra_w is not null) then
	--Abre cursor dos tipos de protocolo
	for r_C01_w in C01 (nr_seq_regra_w) loop
		begin
		ie_apres_ok_w := 'S';
		--Caso o par�metro informado � o lote
		if	(nr_seq_lote_p is not null) then
			--Abre o cursor dos protocolos desse lote.
			for r_C02_w in C02 (nr_seq_lote_p) loop
				begin
							
				ie_apres_ok_w := 'S';
				if (r_C01_w.ie_apresentacao in ('A','R')) then
					
					--Verifica se a forma de apresenta��o do protocolo � a mesma informada na valida��o
					if	(r_C02_w.ie_apresentacao != r_C01_w.ie_apresentacao) then
						--Se for diferente, ent�o vai para pr�xima itera��o do cursor pois a combina��o entre registro atual processado e regra atual no cursor n�o sa� v�lidos.
						ie_apres_ok_w := 'N';
					end if;					
				end if;
				
				if (ie_apres_ok_w = 'S') then
					--Caso na regra tenha sido informado a origem do protocolo e o tipo de guia n�o.				
					if	(r_C01_w.ie_origem_protocolo is not null) and
						(r_C01_w.ie_tipo_guia is null) then
						--Verifica se foi apresentada a guia fisica do protocolo
						if	(r_C02_w.ie_guia_fisica = 'N') then
							--Verifica se a origem do protocolo � o mesmo cadastrado na regra
							if	(r_C02_w.ie_origem_protocolo = r_C01_w.ie_origem_protocolo) then
								--Caso sim, retorna 'N' e sai do cursor.
								ds_retorno_w := 'N';
								exit;
							else
								--Caso n�o o retorno permanece 'S'.
								ds_retorno_w := 'S';
							end if;
						else
							--Caso sim o retorno permanece 'S'.
							ds_retorno_w := 'S';
						end if;
					--Caso na regra n�o tenha sido informado a origem do protocolo e o tipo de guia sim.
					elsif	(r_C01_w.ie_origem_protocolo is null) and
						(r_C01_w.ie_tipo_guia is not null) then
						--Verifica se foi apresentada a guia fisica do protocolo
						if	(r_C02_w.ie_guia_fisica = 'N') then
							--Verifica se o tipo de guia � igual ao da regra
							if	(r_C02_w.ie_tipo_guia = r_C01_w.ie_tipo_guia) then
								--Caso sim, retorna 'N' e sai do cursor.
								ds_retorno_w := 'N';
								exit;
							else
								--Caso n�o o retorno permanece 'S'.
								ds_retorno_w := 'S';
							end if;
						else
							--Caso sim o retorno permanece 'S'.
							ds_retorno_w := 'S';
						end if;
					--Caso na regra n�o tenha sido informado a origem do protocolo, nem o tipo de guia.
					elsif	(r_C01_w.ie_origem_protocolo is null) and
						(r_C01_w.ie_tipo_guia is null) then
						--Verifica se foi apresentada a guia fisica do protocolo
						if	(r_C02_w.ie_guia_fisica = 'N') then
							--Caso n�o retorna 'N' e sai do cursor
							ds_retorno_w := 'N';
							exit;
						else
							--Caso sim o retorno permanece 'S'.
							ds_retorno_w := 'S';
						end if;
					--Caso na regra tenha sido informado a origem do protocolo e o tipo de guia.
					elsif	(r_C01_w.ie_origem_protocolo is not null) and
						(r_C01_w.ie_tipo_guia is not null) then
						--Verifica se foi apresentada a guia fisica do protocolo					
						if	(r_C02_w.ie_guia_fisica = 'N') then
							--Verifica se a origem do protocolo e o tipo de guia � o mesmo cadastrado na regra
							if	(r_C02_w.ie_origem_protocolo = r_C01_w.ie_origem_protocolo)  and
								(r_C02_w.ie_tipo_guia = r_C01_w.ie_tipo_guia) then
								--Caso sim, retorna 'N' e sai do cursor.
								ds_retorno_w := 'N';
								exit;
							else
								--Caso n�o o retorno permanece 'S'.
								ds_retorno_w := 'S';
							end if;
						else
							--Caso sim o retorno permanece 'S'.
							ds_retorno_w := 'S';
						end if;
					end if;
				end if;
				end;
			end loop;--Fecha o cursor de protocolos
			
			--Caso durante o cursor 2 o retorno recebeu 'N' sai do cursor1
			if	(ds_retorno_w = 'N') then
				exit;
			end if;
		--Caso o par�metro passado tenha sido o protocolo	
		elsif	(nr_seq_protocolo_p is not null) then
		
			--Busca a origem, o tipo de guia, o status e se a guia fisica ja foi apresentada
			select	ie_origem_protocolo,
				ie_tipo_guia,
				ie_status,
				nvl(ie_guia_fisica, 'N') ie_guia_fisica,
				nvl(ie_apresentacao,'A') ie_apresentacao
			into	ie_origem_protocolo_w,
			        ie_tipo_guia_w,		
			        ie_status_w,		
			        ie_guia_fisica_w,
					ie_apresentacao_w
			from	pls_protocolo_conta
			where	nr_sequencia = nr_seq_protocolo_p;
			
						
			--Caso n�o esteja nos status v�lidos ja retorna 'S'
			if	(ie_status_w in ('1', '2', '3', '5', '7')) then
			
				if (r_C01_w.ie_apresentacao in ('A','R')) then
						
					--Verifica se a forma de apresenta��o do protocolo � a mesma informada na valida��o
					if	( ie_apresentacao_w != r_C01_w.ie_apresentacao) then
						--Se for diferente, ent�o vai para pr�xima itera��o do cursor pois a combina��o entre registro atual processado e regra atual no cursor n�o sa� v�lidos.
						ie_apres_ok_w := 'N';
					end if;					
				end if;
			
				if (ie_apres_ok_w = 'S') then
					--Caso na regra tenha sido informado a origem do protocolo e o tipo de guia n�o.
					if	(r_C01_w.ie_origem_protocolo is not null) and
						(r_C01_w.ie_tipo_guia is null) then
						--Verifica se foi apresentada a guia fisica do protocolo
						if	(ie_guia_fisica_w = 'N') then
							--Verifica se a origem do protocolo � o mesmo cadastrado na regra
							if	(ie_origem_protocolo_w = r_C01_w.ie_origem_protocolo) then
								--Caso sim, retorna 'N' e sai do cursor.
								ds_retorno_w := 'N';
								exit;
							else
								--Caso n�o o retorno permanece 'S'.
								ds_retorno_w := 'S';
							end if;
						else
							--Caso sim o retorno permanece 'S'.
							ds_retorno_w := 'S';
						end if;
					--Caso na regra n�o tenha sido informado a origem do protocolo e o tipo de guia sim.
					elsif	(r_C01_w.ie_origem_protocolo is null) and
						(r_C01_w.ie_tipo_guia is not null) then
						--Verifica se foi apresentada a guia fisica do protocolo
						if	(ie_guia_fisica_w = 'N') then
							--Verifica se o tipo de guia � igual ao da regra
							if	(ie_tipo_guia_w = r_C01_w.ie_tipo_guia) then
								--Caso sim, retorna 'N' e sai do cursor.
								ds_retorno_w := 'N';
								exit;
							else
								--Caso n�o o retorno permanece 'S'.
								ds_retorno_w := 'S';
							end if;
						else
							--Caso sim o retorno permanece 'S'.
							ds_retorno_w := 'S';
						end if;
					--Caso na regra n�o tenha sido informado a origem do protocolo, nem o tipo de guia.
					elsif	(r_C01_w.ie_origem_protocolo is null) and
						(r_C01_w.ie_tipo_guia is null) then
						--Verifica se foi apresentada a guia fisica do protocolo
						if	(ie_guia_fisica_w = 'N') then
							--Caso n�o retorna 'N' e sai do cursor
							ds_retorno_w := 'N';
							exit;
						else
							--Caso sim o retorno permanece 'S'.
							ds_retorno_w := 'S';
						end if;
					--Caso na regra tenha sido informado a origem do protocolo e o tipo de guia.
					elsif	(r_C01_w.ie_origem_protocolo is not null) and
						(r_C01_w.ie_tipo_guia is not null) then
						--Verifica se foi apresentada a guia fisica do protocolo
						if	(ie_guia_fisica_w = 'N') then
							--Verifica se a origem do protocolo e o tipo de guia � o mesmo cadastrado na regra
							if	(ie_origem_protocolo_w = r_C01_w.ie_origem_protocolo)  and
								(ie_tipo_guia_w = r_C01_w.ie_tipo_guia) then
								--Caso sim, retorna 'N' e sai do cursor.
								ds_retorno_w := 'N';
								exit;
							else
								--Caso n�o o retorno permanece 'S'.
								ds_retorno_w := 'S';
							end if;
						else
							--Caso sim o retorno permanece 'S'.
							ds_retorno_w := 'S';
						end if;
					end if;
				end if;
			else
				--Caso n�o esteja nos status v�lidos ja retorna 'S'
				ds_retorno_w := 'S';
			end if;	
		end if;
		end;
	end loop;
end if;

return	ds_retorno_w;

end pls_val_guia_fisica;
/
