create or replace 
FUNCTION Obter_se_etapa_pronta(
			nr_sequencia_p		Number,
			cd_estabelecimento_p	Number)
			return varchar2 is

qt_existe_w	Number(10);
ds_retorno_w	Varchar2(1) := 'N';

BEGIN

select	count(*)  
into	qt_existe_w
from	far_etapa_producao
where	cd_estabelecimento = cd_estabelecimento_p
and	exists (	select	1 
			from	can_ordem_prod
			where	nr_sequencia_p = nr_seq_etapa_prod
			and	dt_fim_preparo is null);

if	(qt_existe_w = 0) then
	ds_retorno_w	:= 'S';
end if;

return ds_retorno_w;

END Obter_se_etapa_pronta;
/
