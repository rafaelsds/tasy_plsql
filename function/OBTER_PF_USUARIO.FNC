create or replace
function Obter_Pf_Usuario	
		(nm_usuario_p	varchar2,
		ie_opcao_p	varchar2)
		return varchar2 is

ds_retorno_w			varchar2(100) := '';

begin


if	(nm_usuario_p is not null) then
	if	(ie_opcao_p = 'C') then
		begin
		select	a.cd_pessoa_fisica
		into	ds_retorno_w
		from	usuario a
		where	a.nm_usuario_pesquisa = upper(nm_usuario_p);
		exception
			when others then
				ds_retorno_w	:= '';
		end;
	else
		begin
		select	substr(obter_nome_pf(b.cd_pessoa_fisica),1,100)
		into	ds_retorno_w
		from	pessoa_fisica b,
			usuario a
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.nm_usuario_pesquisa = upper(nm_usuario_p);
		exception
			when others then
				ds_retorno_w	:= '';
		end;
	end	if;
end if;

return ds_retorno_w;

end Obter_Pf_Usuario;
/