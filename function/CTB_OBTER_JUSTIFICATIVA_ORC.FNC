create or replace
function ctb_obter_justificativa_orc(
				cd_empresa_p			number,
				cd_centro_custo_p		number,
				cd_conta_contabil_p		varchar2,
				dt_referencia_p		date)
		return 	varchar2 is


dt_referencia_w		date;
nr_seq_mes_ref_w		number(10);
ds_justificativa_w		varchar2(2000);

begin
dt_referencia_w		:= trunc(dt_referencia_p,'month');

select	max(nr_sequencia)
into	nr_seq_mes_ref_w
from	ctb_mes_ref
where	dt_referencia		= dt_referencia_w
and	cd_empresa		= cd_empresa_p;

select	nvl(max(ds_justificativa),'')
into	ds_justificativa_w
from	ctb_orcamento
where	cd_centro_custo	= cd_centro_custo_p
and	cd_conta_contabil	= cd_conta_contabil_p
and	nr_seq_mes_ref	= nr_seq_mes_ref_w;

return	substr(ds_justificativa_w,1,2000);

end ctb_obter_justificativa_orc;
/