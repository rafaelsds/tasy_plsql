create or replace
function obter_cid_sus_aten(nr_atendimento_p		number)
 		    	return varchar2 is

cd_cid_principal_w	varchar2(10);
dt_emissao_w	date;
ds_cid_pricipal_w	varchar2(50);
			
			
begin
ds_cid_pricipal_w := '';

select	max(dt_emissao)
into	dt_emissao_w
from	sus_aih_unif
where	nr_atendimento = nr_atendimento_p;

if	(dt_emissao_w is not null) then

	select	max(nvl(cd_cid_principal,null))
	into	cd_cid_principal_w
	from	sus_aih_unif
	where	nr_atendimento = nr_atendimento_p
	and	dt_emissao = dt_emissao_w;
	
	ds_cid_pricipal_w := substr(sus_obter_desc_cid(cd_cid_principal_w),1,50);
	
end if;

return	ds_cid_pricipal_w;

end obter_cid_sus_aten;
/