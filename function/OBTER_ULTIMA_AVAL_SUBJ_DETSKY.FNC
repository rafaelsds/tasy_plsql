create or replace
function obter_ultima_aval_subj_detsky(	nr_atendimento_p number, ie_opcao_p varchar2)
				return varchar2 is
ie_aval_nut_w		varchar2(255);
dt_avaliacao_w		varchar2(50);

begin

select	max(a.ie_aval_nut),		
		to_char(max(dt_avaliacao),'dd/mm/yyyy hh24:mi:ss')
into	ie_aval_nut_w ,
		dt_avaliacao_w
from	aval_nutric_subjetiva a
where	nr_atendimento = nr_atendimento_p
and		dt_liberacao is not null
and 	dt_inativacao is null
and		a.nr_sequencia = (	select max(x.nr_sequencia)
	from   aval_nutric_subjetiva x
	where  x.nr_atendimento	= nr_atendimento_p
	and	   x.dt_liberacao is not null
	and	   x.dt_inativacao is null);
							
if (ie_opcao_p = 'AV') then
	return	ie_aval_nut_w;
elsif (ie_opcao_p = 'DT') then
	return	dt_avaliacao_w;
end if;

end obter_ultima_aval_subj_detsky;
/