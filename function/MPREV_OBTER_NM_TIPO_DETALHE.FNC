create or replace
function mprev_obter_nm_tipo_detalhe(nr_seq_tipo_detalhe_p	number)
				return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter a descri��o do tipo de detalhamento 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [X] Outros: MedPrev - Programas de Promo��o a Sa�de
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nm_detalhe_w 	varchar2(255);

begin

select 	substr(nm_detalhe,1,255)
into	nm_detalhe_w
from 	mprev_tipo_detalhe_prog 
where 	nr_sequencia = nr_seq_tipo_detalhe_p;

return	nm_detalhe_w;

end mprev_obter_nm_tipo_detalhe;
/