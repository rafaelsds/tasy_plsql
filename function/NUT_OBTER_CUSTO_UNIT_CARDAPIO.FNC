create or replace
function nut_obter_custo_unit_cardapio( nr_seq_servico_p number,
				dt_cardapio_p date)
 		    	return number is

			
qt_pessoa_atend_real_w 	number(15);
vl_custo_w		number(17,4);
ds_retorno_w		number(17,4);


begin

if ( nr_seq_servico_p is not null) then

	SELECT	SUM(x.qt_pessoa_atend_real)
	into	qt_pessoa_atend_real_w
	FROM   	nut_cardapio_dia x
	WHERE   ((x.nr_Seq_servico = nr_seq_servico_p) OR (nr_seq_servico_p = 0))
	and 	TO_CHAR(x.dt_cardapio,'dd/mm/yyyy') = dt_cardapio_p;
	
	
	SELECT	(DECODE('S','S',(SUM(o.vl_custo_real * 1)),SUM(o.vl_custo_real))) --(campo_mascara_virgula(DECODE('S','S',(SUM(o.vl_custo_real * 1)),SUM(o.vl_custo_real))))
	into	vl_custo_w
	FROM   	nut_rec_real_comp o,
        	nut_receita_real r,
        	nut_cardapio c,
        	nut_servico s,
        	nut_cardapio_dia d
	WHERE   o.nr_seq_rec_real = r.nr_sequencia
	AND     r.nr_seq_cardapio    = c.nr_sequencia
	AND     c.nr_seq_card_dia = d.nr_sequencia
	AND     s.nr_sequencia   = d.nr_seq_servico
	AND	TO_CHAR(d.dt_cardapio,'dd/mm/yyyy') = dt_cardapio_p
	AND	((d.nr_Seq_servico = nr_seq_servico_p) OR (nr_seq_servico_p = 0));


	ds_retorno_w := vl_custo_w / qt_pessoa_atend_real_w;



end if;

return	ds_retorno_w;

end nut_obter_custo_unit_cardapio;
/