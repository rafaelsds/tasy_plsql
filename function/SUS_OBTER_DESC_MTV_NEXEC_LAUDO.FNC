create or replace
function sus_obter_desc_mtv_nexec_laudo(nr_seq_motivo_nexec_proc_p		number)
 		    			return varchar2 is
					
ds_retorno_w		varchar2(255) := '';					

begin

begin
select	ds_motivo
into	ds_retorno_w
from	sus_motivo_nexec_laudo
where	nr_sequencia = nr_seq_motivo_nexec_proc_p;
exception
when others then
	ds_retorno_w := '';
end;

return	ds_retorno_w;

end sus_obter_desc_mtv_nexec_laudo;
/