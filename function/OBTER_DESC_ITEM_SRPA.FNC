create or replace 
FUNCTION Obter_desc_item_srpa(	nr_sequencia_p	number)
			return varchar2 is

ds_retorno_w	varchar2(80);

BEGIN

select	ds_item
into	ds_retorno_w
from	srpa_item
where	nr_sequencia = nr_sequencia_p;

return ds_retorno_w;

END Obter_desc_item_srpa;
/
