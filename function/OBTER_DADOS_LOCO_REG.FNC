create or replace
function	obter_dados_loco_reg(	cd_pessoa_fisica_p	varchar2,
					ie_opcao_p		varchar2)
		return varchar2 is

/*

A	- Altura
CT	- C�digo da Topografia
DST	- Descri��o da Topografia

*/


cd_topografia_w	varchar2(10);
qt_altura_w	number(3);
qt_altura_ww	number(3);

begin

begin
select	qt_altura
into	qt_altura_ww
from	paciente_setor
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	nr_seq_paciente		= (	select	max(nr_seq_paciente)
					from	paciente_setor
					where	cd_pessoa_fisica	= cd_pessoa_fisica_p);
exception
when others then
	qt_altura_ww	:= null;
end;					

select	nvl(qt_altura_ww, qt_altura),
	cd_topografia
into	qt_altura_w,
	cd_topografia_w
from	can_loco_regional
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	nr_sequencia		= (	select	max(nr_sequencia)
					from	can_loco_regional
					where	cd_pessoa_fisica	= cd_pessoa_fisica_p);
if	(ie_opcao_p = 'A') then
	return	qt_altura_w;
elsif	(ie_opcao_p = 'DST') then
	return	substr(obter_desc_cido_topografia(cd_topografia_w),1,100);
elsif	(ie_opcao_p = 'CT') then
	return	cd_topografia_w;
end if;

end	obter_dados_loco_reg;
/