create or replace
function Obter_Prox_Dia_Util_Trat_Onc(	nr_seq_paciente_p	number)
 		    	return date is

QT_HORAS_INICIO_CICLO_w	number(10);			
dt_referencia_w		date;
ie_dia_util_w		varchar2(50);
nr_ciclo_w		number(10);
nr_ultimo_dia_ciclo_w	number(10);
dt_prevista_w		date;
qt_dias_intervalo_w	number(10);
IE_INTERV_CICLO_w	varchar2(50);
ie_interv_ciclo_data_w	varchar2(50);
ie_segunda_w		varchar2(1);	
ie_terca_w		varchar2(1);
ie_quarta_w		varchar2(1);
ie_quinta_w		varchar2(1);
ie_sexta_w		varchar2(1);
ie_sabado_w		varchar2(1);
ie_domingo_w		varchar2(1);
ie_dia_valido_w		varchar2(10);
cd_protocolo_w		number(10);
nr_seq_medicacao_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ie_cons_dia_ant_ciclo_w	varchar2(10);

function obter_Se_Dia_Semana_Lib(	dt_referencia_p	Date) 
							return varchar2 is
	ie_dia_w	number(10);
	begin
	ie_dia_w	:= PKG_DATE_UTILS.get_WeekDay(dt_referencia_p);
	
	if	(ie_dia_w	= 1) and
		(ie_domingo_w	= 'N') then
		return 'N';
	elsif	(ie_dia_w	= 2) and
		(ie_segunda_w	= 'N') then
		return 'N';
	elsif	(ie_dia_w	= 3) and
		( ie_terca_w	= 'N') then
		return 'N';
	elsif	(ie_dia_w	= 4) and
		( ie_quarta_w	= 'N') then
		return 'N';
	elsif	(ie_dia_w	= 5) and
		( ie_quinta_w	= 'N') then
		return 'N';
	elsif	(ie_dia_w	= 6) and
		( ie_sexta_w	= 'N') then
		return 'N';
	elsif	(ie_dia_w	= 7) and
		( ie_sabado_w	= 'N') then
		return 'N';
	end if;

	return 'S';
	end;

begin

dt_referencia_w := sysdate-1;
If	(nr_seq_paciente_p > 0) then
	select	nvl(max(IE_INTERV_CICLO),'I'),
		nvl(max(ie_cons_dia_ant_ciclo),'N'),
		nvl(max(ie_interv_ciclo_data),'P')
	into	ie_interv_ciclo_w,
		ie_cons_dia_ant_ciclo_w,
		ie_interv_ciclo_data_w
	from	parametro_medico
	where	cd_estabelecimento = wheb_usuario_pck.get_CD_ESTABELECIMENTO;
	 select nvl(max(nr_ciclo),0)
	 into	nr_ciclo_w
	 from	paciente_atendimento
	 where	nr_seq_paciente = nr_seq_paciente_p
	 and	dt_suspensao is null;
	 
	 
	select	max(cd_pessoa_fisica),
		max(cd_protocolo),
		max(nr_seq_medicacao)
	into	cd_pessoa_fisica_w,
		cd_protocolo_w,
		nr_seq_medicacao_w
	from	paciente_setor
	where	nr_seq_paciente	 = nr_seq_paciente_p;
	 
	 If	(nr_ciclo_w > 0) then
		 
		 if	(ie_interv_ciclo_w	= 'U') then
			 select	max(somente_numero(ds_dia_ciclo))
			 into	nr_ultimo_dia_ciclo_w
			 from	paciente_atendimento
			 where	nr_seq_paciente = nr_seq_paciente_p
			 and	nr_ciclo = nr_ciclo_w
			 and	dt_suspensao is null;
		 elsif	(ie_interv_ciclo_w	= 'I')then
				select	min(somente_numero(ds_dia_ciclo))
			 into	nr_ultimo_dia_ciclo_w
			 from	paciente_atendimento
			 where	nr_seq_paciente = nr_seq_paciente_p
			 and	nr_ciclo = nr_ciclo_w
			 and	dt_suspensao is null;
		 end if;
		 
		 If	(nr_ultimo_dia_ciclo_w > 0) then
			 --select	max(dt_prevista)
			 select	max(decode(ie_interv_ciclo_data_w,'R',nvl(dt_real,dt_prevista),dt_prevista))
			 into	dt_prevista_w
			 from	paciente_atendimento
			 where	nr_seq_paciente = nr_seq_paciente_p
			 and	nr_ciclo = nr_ciclo_w
			 and	somente_numero(ds_dia_ciclo) = nr_ultimo_dia_ciclo_w
			 and	dt_suspensao is null;
			 
			 if	(nr_ultimo_dia_ciclo_w	> 1 ) and
				(ie_interv_ciclo_w	= 'I')  and
				(ie_cons_dia_ant_ciclo_w = 'S')then
				
				for i in 1..nr_ultimo_dia_ciclo_w loop
					begin
					dt_prevista_w	:= dt_prevista_w - 1;
					
				ie_dia_util_w		:= Obter_Se_Dia_Util_Oncologia(dt_referencia_w,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario,null,cd_protocolo_w,nr_seq_medicacao_w);

				while 	(ie_dia_util_w	= 'N') loop 
					 begin
					 dt_referencia_w	:= dt_referencia_w -1;					 
					 ie_dia_util_w		:= Obter_Se_Dia_Util_Oncologia(dt_referencia_w,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario,null,cd_protocolo_w,nr_seq_medicacao_w);
					 end;
				end loop;	
					
					end;
				end loop;
				
				dt_prevista_w	:= dt_prevista_w - (nr_ultimo_dia_ciclo_w-1);
			end if;
			 
			 
			 If	(dt_prevista_w is not null) then
				 dt_referencia_w := dt_prevista_w;
				 
				 select	max(qt_dias_intervalo)
				 into	qt_dias_intervalo_w
				 from	paciente_setor
				 where	nr_seq_paciente = nr_seq_paciente_p;
				 
				 qt_dias_intervalo_w	:= nvl(Obter_Dias_Interv_Protoc(nr_seq_paciente_p,nr_ciclo_w + 1),qt_dias_intervalo_w);
				 
				 
				 If	(qt_dias_intervalo_w > 0) then
					 dt_referencia_w := dt_referencia_w + qt_dias_intervalo_w;
				 end if;
			 end if;
		 end if;		 
	 end if;
end if;	
	
if	(dt_referencia_w < sysdate) then	
	 dt_referencia_w := sysdate;
end if;

select	max(nvl(ie_segunda,'S')),
	max(nvl(ie_terca,'S')),
	max(nvl(ie_quarta,'S')),
	max(nvl(ie_quinta,'S')),
	max(nvl(ie_sexta,'S')),
	max(nvl(ie_sabado,'S')),
	max(nvl(ie_domingo,'S'))
into	ie_segunda_w,
	ie_terca_w,
	ie_quarta_w,
	ie_quinta_w,
	ie_sexta_w,
	ie_sabado_w,
	ie_domingo_w	
from	protocolo_medicacao
where	cd_protocolo 	= cd_protocolo_w
and	nr_sequencia 	= nr_seq_medicacao_w;

ie_dia_util_w		:= Obter_Se_Dia_Util_Oncologia(dt_referencia_w,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario,null,cd_protocolo_w,nr_seq_medicacao_w);
ie_dia_valido_w	:= obter_Se_Dia_Semana_Lib(dt_referencia_w);

while 	(ie_dia_util_w	= 'N') or
	(ie_dia_valido_w = 'N') loop 
	 begin
	 dt_referencia_w	:= dt_referencia_w + 1;
	 ie_dia_util_w		:= Obter_Se_Dia_Util_Oncologia(dt_referencia_w,wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario,null,cd_protocolo_w,nr_seq_medicacao_w);
	 ie_dia_valido_w	:= obter_Se_Dia_Semana_Lib(dt_referencia_w);
	 end;
end loop;	

return	dt_referencia_w;

end Obter_Prox_Dia_Util_Trat_Onc;
/
