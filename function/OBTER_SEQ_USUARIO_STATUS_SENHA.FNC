create or replace
function obter_seq_usuario_status_senha(nm_usuario_alt_p		Varchar2)
 		    	return number is
			
ds_retorno_w		Number(10);			

begin

if (nm_usuario_alt_p is not null) then

	begin
	
	select	max(nr_seq_status)
	into	ds_retorno_w
	from	usuario_status_senha
	where	nr_sequencia = (select 	max(x.nr_sequencia)
				from	usuario_status_senha x
				where	x.nm_usuario = nm_usuario_alt_p);
	
	end;

end if;

return	ds_retorno_w;

end obter_seq_usuario_status_senha;
/