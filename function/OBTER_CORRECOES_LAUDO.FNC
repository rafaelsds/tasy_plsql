CREATE OR REPLACE
FUNCTION Obter_correcoes_laudo	( nr_seq_laudo_p	number)
				RETURN number IS

qt_retorno_w	number(3);



begin

select	count(*)
into	qt_retorno_w
from	laudo_paciente_copia
where	nr_seq_laudo	= nr_seq_laudo_p;


return	qt_retorno_w;

end	Obter_correcoes_laudo;
/