create or replace
function obter_se_tipo_alerta_lib(	nr_seq_tipo_alerta_p	number,
					nm_usuario_p		varchar2,
					cd_perfil_p		number default null)
 		    	return varchar2 is
cd_pessoa_fisica_w	varchar2(10);
qt_reg_w	number(10);
cd_perfil_W	number(10);
cd_setor_atend_w	number(10);
ie_retorno_w	varchar2(10);
cd_funcao_w	number(5);
cd_estabelecimento_w number(4);

Cursor C01 is
select	nvl(IE_PERMITE,'S')
	from	TIPO_ALERTA_ATEND_LIB
	where	NR_SEQ_TIPO_ALERTA	= nr_seq_tipo_alerta_p
	and	nvl(cd_perfil,cd_perfil_W)	= cd_perfil_W
	and	nvl(cd_pessoa_fisica,cd_pessoa_fisica_w)	= cd_pessoa_fisica_w
	and	((cd_especialidade is null) or (obter_se_especialidade_medico(cd_pessoa_fisica_w,cd_especialidade)	= 'S'))
	and	nvl(cd_setor_atendimento,cd_setor_atend_w)	= cd_setor_atend_w
	and	nvl(cd_funcao,cd_funcao_w)	= cd_funcao_w
     and  nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	order by 	nvl(cd_pessoa_fisica,'0'),
			nvl(cd_perfil, 0),
			nvl(cd_especialidade, 0),
			nvl(cd_setor_atendimento, 0);
	
	
begin

cd_setor_atend_w	:= nvl(wheb_usuario_pck.get_cd_setor_atendimento,0);
cd_funcao_w		:= nvl(wheb_usuario_pck.get_cd_funcao,0);
cd_estabelecimento_w 	:= obter_estabelecimento_ativo;

select	nvl(max(cd_pessoa_fisica),'0')
into	cd_pessoa_fisica_w
from	usuario
where	nm_usuario	= nm_usuario_p;

cd_perfil_W		:= nvl(cd_perfil_p, obter_perfil_ativo);

select	count(*)
into	qt_reg_w
from	TIPO_ALERTA_ATEND_LIB
where	NR_SEQ_TIPO_ALERTA	= nr_seq_tipo_alerta_p;

if	(qt_reg_w	> 0) then
/*
	select	count(*)
	into	qt_reg_w
	from	TIPO_ALERTA_ATEND_LIB
	where	NR_SEQ_TIPO_ALERTA	= nr_seq_tipo_alerta_p
	and	nvl(cd_perfil,cd_perfil_W)	= cd_perfil_W
	and	nvl(cd_pessoa_fisica,cd_pessoa_fisica_w)	= cd_pessoa_fisica_w
	and	((cd_especialidade is null) or (obter_se_especialidade_medico(cd_pessoa_fisica_w,cd_especialidade)	= 'S'))
	and	nvl(cd_setor_atendimento,cd_setor_atend_w)	= cd_setor_atend_w;
	
	if	(qt_reg_w	= 0) then
		return 'N';
	end if;
	
	*/
	
	ie_retorno_w	:= 'N';
	
	open C01;
	loop
	fetch C01 into	
		ie_retorno_w;
	exit when C01%notfound;
	end loop;
	close C01;
	
	return nvl(ie_retorno_w,'N');
end if;

return	'S';

end obter_se_tipo_alerta_lib;
/
