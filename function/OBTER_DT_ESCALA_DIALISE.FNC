create or replace
function obter_dt_escala_dialise(	nr_seq_escala_dialise_p		number,
					ie_opcao_p			varchar2)
 		    	return date is

/*
ie_opcao_p 
I = data in�cio
F = data final
*/	

dt_retorno_w		date;
	
begin

if	(ie_opcao_p = 'I') then
	
	select	max(dt_inicio)
	into	dt_retorno_w
	from	hd_escala_dialise
	where	nr_sequencia 	= nr_seq_escala_dialise_p;

elsif	(ie_opcao_p = 'F') then

	select	max(dt_fim)
	into	dt_retorno_w
	from	hd_escala_dialise
	where	nr_sequencia 	= nr_seq_escala_dialise_p;

end if;


return	dt_retorno_w;

end obter_dt_escala_dialise;
/