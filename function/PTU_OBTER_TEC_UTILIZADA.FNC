create or replace
function ptu_obter_tec_utilizada(nr_seq_conta_proc_p	pls_conta_proc.nr_sequencia%type)
 		    	return Varchar2 is

ie_tecnica_utilizada_w	pls_conta_proc.ie_tecnica_utilizada%type;
/*Ir� retornar a tecnica utilizada no procedimento conforme o padr�o PTU devido a diverg�ncia entre os padr�es TISS ()1834 e PTU*/
begin

if	(nr_seq_conta_proc_p is not null) then
	select	max(ie_tecnica_utilizada)
	into	ie_tecnica_utilizada_w
	from	pls_conta_proc
	where	nr_sequencia	= nr_seq_conta_proc_p;
	
	if	(ie_tecnica_utilizada_w = 'C') then
		ie_tecnica_utilizada_w	:= '1';
	elsif	(ie_tecnica_utilizada_w	= 'V') then
		ie_tecnica_utilizada_w	:= '2';
	elsif	(ie_tecnica_utilizada_w	= 'R') then
		ie_tecnica_utilizada_w	:= '3';
	end if;
end if;

return	ie_tecnica_utilizada_w;

end ptu_obter_tec_utilizada;
/