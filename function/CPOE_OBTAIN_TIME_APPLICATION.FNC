create or replace
function CPOE_OBTAIN_TIME_APPLICATION(	qt_old_dose_p		cpoe_dieta.qt_dose%type,
										qt_new_dose_p		cpoe_dieta.qt_dose%type,
										qt_tempo_aplic_p	cpoe_dieta.qt_tempo_aplic%type,
										qt_vel_infusao_p	cpoe_dieta.qt_vel_infusao%type
									  )
							return varchar2 is

	qt_minutos_w	number(18,6);
	ds_return_w		varchar2(50) := null;

begin
	if ((nvl(qt_old_dose_p, 0) > 0) and (nvl(qt_new_dose_p, 0) > 0) and (qt_tempo_aplic_p is not null)) then
		qt_minutos_w := obter_minutos_hora(qt_tempo_aplic_p);

		if (qt_minutos_w is not null) then
			qt_minutos_w := (qt_new_dose_p * qt_minutos_w) / qt_old_dose_p;
		
			ds_return_w := substr(pls_formatar_minutos(qt_minutos_w), 1, 5);
		end if;
	end if;

	return ds_return_w;

end CPOE_OBTAIN_TIME_APPLICATION;
/
