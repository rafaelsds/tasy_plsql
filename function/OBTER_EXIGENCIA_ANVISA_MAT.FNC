create or replace
function obter_exigencia_anvisa_mat(	
		cd_estabelecimento_p	number,
		cd_material_p		number,
		ie_opcao_p		varchar2)
		return varchar2 is

/*
ie_opcao_p
VA - A function vai retornar se exige a data de validade da anvisa
VI - A function vai retornar se o item esta vigente na Anvisa
*/
			
		
nr_seq_familia_w		material.nr_seq_familia%type;
cd_grupo_material_w	grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w	subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w	classe_material.cd_classe_material%type;
nr_seq_classif_risco_w	material.nr_seq_classif_risco%type;
ie_exige_data_validade_w	regra_exigencia_anvisa_mat.ie_exige_data_validade%type := 'N';
ie_exige_vigencia_w	regra_exigencia_anvisa_mat.ie_exige_vigencia%type := 'N';
ie_consiste_validade_w	regra_exigencia_anvisa_mat.ie_consiste_validade%type := 'N';
ds_retorno_w		varchar2(15);


cursor c01 is
select	nvl(ie_exige_data_validade,'N'),
	nvl(ie_exige_vigencia,'N'),
	nvl(ie_consiste_validade, 'N')
from	regra_exigencia_anvisa_mat
where	cd_estabelecimento					= cd_estabelecimento_p
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w) 	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	nvl(cd_material, cd_material_p)			= cd_material_p
and	nvl(nr_seq_familia, nr_seq_familia_w) 			= nr_seq_familia_w
and	nvl(nr_seq_classif_risco, nr_seq_classif_risco_w)		= nr_seq_classif_risco_w

order by	cd_material desc,
	cd_classe_material desc,
	cd_subgrupo_material desc,
	cd_grupo_material desc,
	nr_seq_familia desc,
	nr_seq_classif_risco desc;

begin

begin
select	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material,
	nvl(nr_seq_familia,0)
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	nr_seq_familia_w
from	estrutura_material_v a
where	a.cd_material = cd_material_p;

select	nvl(nr_seq_classif_risco,0)
into	nr_seq_classif_risco_w
from	material
where	cd_material = cd_material_p;


open C01;
loop
fetch C01 into	
	ie_exige_data_validade_w,
	ie_exige_vigencia_w,
	ie_consiste_validade_w;
exit when C01%notfound;
	begin
	ie_exige_data_validade_w := ie_exige_data_validade_w;
	ie_exige_vigencia_w := ie_exige_vigencia_w;
	ie_consiste_validade_w := ie_consiste_validade_w;
	end;
end loop;
close C01;

if	(ie_opcao_p = 'VA') then
	ds_retorno_w := ie_exige_data_validade_w;
elsif	(ie_opcao_p = 'VI') then
	ds_retorno_w := ie_exige_vigencia_w;
elsif	(ie_opcao_p = 'VC') then
	ds_retorno_w := ie_consiste_validade_w;
end if;

exception
when others then
	null;
end;

return	ds_retorno_w;

end obter_exigencia_anvisa_mat;
/
