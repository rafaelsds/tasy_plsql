create or replace
function HNSC_Obter_Medico_Interf(	nr_seq_item_p		number,
					nr_interno_conta_p	number)
 		    	return varchar2 is

cd_medico_executor_w	varchar2(10);
cd_cgc_prestador_w	varchar2(14);
cd_cgc_prestador_ww	varchar2(14);
cd_medico_exec_w	varchar2(10);
cd_procedimento_w	number(15);
			
begin

select	max(cd_medico_executor),
	max(cd_cgc_prestador),
	max(cd_procedimento)
into	cd_medico_exec_w,
	cd_cgc_prestador_w,
	cd_procedimento_w
from	procedimento_paciente
where	nr_sequencia = nr_seq_item_p
and	nr_interno_conta = nr_interno_conta_p;


select	max(cd_cgc_prestador)
into	cd_cgc_prestador_ww
from	material_atend_paciente
where	nr_sequencia = nr_seq_item_p
and	nr_interno_conta = nr_interno_conta_p;

if	(cd_cgc_prestador_ww = '83883306001213') then
	cd_medico_executor_w	:= '22100089';
end if;

if	(cd_cgc_prestador_w = '79004917000114') then
	cd_medico_executor_w	:= '22100072';
end if;

if	(cd_cgc_prestador_w = '83883306001213') and (cd_medico_exec_w in ('7164')) then
	cd_medico_executor_w	:= '22100089';
end if;

if	(cd_cgc_prestador_w = '86897113000157') then
	cd_medico_executor_w	:= '22100210';
end if;

if	(cd_medico_exec_w in ('2031', '586804')) and (cd_procedimento_w = 20010141) then
	cd_medico_executor_w	:= '22100089';
end if;

if	(cd_cgc_prestador_w = '83883306001213') and (cd_medico_exec_w in ('932')) then
	cd_medico_executor_w	:= '22100089';
end if;

if	(cd_medico_exec_w in ('963','820','8621','2931')) then
	cd_medico_executor_w	:= '22100089';
end if;

if	(cd_cgc_prestador_w = '78274123000108') then
	cd_medico_executor_w	:= '22100141';
end if;

if	(cd_medico_exec_w in ('216433')) and (cd_procedimento_w between 33000000 and 34000000) then
	cd_medico_executor_w	:= '22100089';
end if;

if	(cd_cgc_prestador_w = '01082181000105') then
	cd_medico_executor_w	:= '22100214';
end if;

if	(cd_procedimento_w in (32120044,40812049,32100035,40812049,32100051,33010099,40901017,22010050,40103234,22010025,40103218,40902048,32100043)) then
	cd_medico_executor_w	:= '22100089';
end if;

if	(cd_cgc_prestador_w = '78610318000181') then
	cd_medico_executor_w	:= '22100015';
end if;

return	cd_medico_executor_w;

end HNSC_Obter_Medico_Interf;
/
