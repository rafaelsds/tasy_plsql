create or replace
function hsi_obter_marca_oc_cot(nr_ordem_compra_p		number,
      cd_cot_compra_p number, nr_item_cot_p	number)
return varchar2 is

resultado_w	 		varchar2(100);

begin

if	(nr_ordem_compra_p is not null) and	(cd_cot_compra_p is not null) and (nr_item_cot_p is not null) then
  select  ds_marca_fornec
  into    resultado_w
  from    COT_COMPRA_FORN_ITEM
  where   nr_cot_compra = cd_cot_compra_p
  and     nr_item_cot_compra = nr_item_cot_p
  and     nr_ordem_compra = nr_ordem_compra_p;
else
  resultado_w := '';
end if;

return resultado_w;

end hsi_obter_marca_oc_cot;
/