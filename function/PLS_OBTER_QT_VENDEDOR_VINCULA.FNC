create or replace
function pls_obter_qt_vendedor_vincula(
		nr_sequencia_p	number,
		nr_seq_vendedor_p	number)
 	return number is

qt_retorno_w	number(9);

begin

select	count(*)
into	qt_retorno_w
from	pls_vendedor_vinculado
where	nr_sequencia	= nr_sequencia_p
and	nr_seq_vendedor	= nr_seq_vendedor_p;

return	qt_retorno_w;
end pls_obter_qt_vendedor_vincula;
/