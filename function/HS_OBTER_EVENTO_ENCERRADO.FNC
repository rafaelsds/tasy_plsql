create or replace
function HS_Obter_Evento_Encerrado	(dt_fim_p	date)
 		    	return number is

begin

if	(dt_fim_p	is null) or
	((dt_fim_p is not null) and(dt_fim_p	> sysdate)) then
	return 1;
else
	return 2;
	
end if;

return 2;

end HS_Obter_Evento_Encerrado;
/