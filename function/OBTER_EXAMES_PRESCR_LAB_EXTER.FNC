CREATE OR REPLACE
FUNCTION Obter_Exames_Prescr_Lab_Exter  (
					nr_prescricao_p		NUMBER,
					cd_setor_atendimento_p	NUMBER,
					cd_material_p		VARCHAR2,
					nr_seq_grupo_p		NUMBER,
					ie_equipamento_p	VARCHAR2,
					ie_opcao_p		VARCHAR2,
					ie_separador_p		VARCHAR2,
					ie_tam_linha_p		NUMBER)
					RETURN VARCHAR2 IS

ie_opcao_w				VARCHAR2(255);
ie_status_atend_w			VARCHAR2(2);
cd_procedimento_w			NUMBER(15);
nr_seq_exame_w				NUMBER(10);
cd_exame_w				VARCHAR2(20);
cd_exame_integracao_w			VARCHAR2(20);
Resultado_w	 			VARCHAR2(1000);
qt_linha_w				NUMBER(2) := 0;
ds_material_especial_w			VARCHAR2(255);
nr_sequencia_w				NUMBER(10);

ds_aux_w				VARCHAR2(100);
nr_seq_amostra_w			VARCHAR2(2);

nm_exame_w				VARCHAR2(80);

CURSOR  c01 IS
	SELECT		a.cd_procedimento,
			b.nr_seq_exame,
			b.cd_exame,
			NVL(b.cd_exame_integracao, b.cd_exame),
			MAX(a.nr_sequencia),
			b.nm_exame
	FROM	exame_lab_material d,
		material_exame_lab c,
		exame_laboratorio b,
		prescr_procedimento a
	WHERE a.nr_seq_exame		= b.nr_seq_exame
	  AND a.nr_seq_exame		= d.nr_seq_exame
	  AND d.nr_seq_material	= c.nr_sequencia
	  AND a.nr_prescricao		= nr_prescricao_p
	  AND NVL(a.cd_setor_atendimento,0)	= NVL(NVL(cd_setor_atendimento_p, a.cd_setor_atendimento),0)
	  AND (a.cd_material_exame	= NVL(cd_material_p, a.cd_material_exame) OR
		(c.cd_material_exame	= cd_material_p AND d.ie_prioridade = 888))
	  AND b.nr_seq_grupo		= NVL(nr_seq_grupo_p, b.nr_seq_grupo)
	  AND NVL(d.qt_coleta,1) >= TO_NUMBER(NVL(ie_equipamento_p,'1'))
	  AND a.ie_status_atend		= NVL(ie_status_atend_w, a.ie_status_atend)
	  AND b.cd_cgc_externo IS NOT NULL
	  AND (	(ds_material_especial_w = 'X' AND a.ds_material_especial IS NULL) OR
	  	(a.ds_material_especial = ds_material_especial_w))
	GROUP BY 	a.cd_procedimento,
			b.nr_seq_exame,
			b.cd_exame,
			NVL(b.cd_exame_integracao, b.cd_exame),
			b.nm_exame
	ORDER BY 5;

BEGIN

/* Op��es
	CP	- C�digo Procedimento
	EX	- Exame
	CE	- C�digo Exame
	CI	- C�digo Integra��o
	CI3	- C�digo Integra��o com 3 d�gitos
	CI8	- C�digo Integra��o com 8 d�gitos
	NME3	- Nome do exame com 3 digitos
*/

ie_opcao_w	:= ie_opcao_p;
resultado_w	:= '';

IF	(INSTR(ie_opcao_p, ';') > 0) THEN
	ie_opcao_w		:= SUBSTR(ie_opcao_p, 1, INSTR(ie_opcao_p, ';') - 1);
	ie_status_atend_w	:= SUBSTR(ie_opcao_p, INSTR(ie_opcao_p, ';') + 1, 2);
	ds_aux_w		:= REPLACE(ie_opcao_p, ie_opcao_w || ';' || ie_status_atend_w,'');
	ds_aux_w		:= SUBSTR(ds_aux_w, 2, 255);
	ds_material_especial_w  := SUBSTR(ds_aux_w, 1, INSTR(ds_aux_w, ';') - 1);
	IF	(INSTR(ds_aux_w, ';') = 0) THEN
		ds_material_especial_w := ds_aux_w;
	ELSE
		nr_seq_amostra_w	:= SUBSTR(ds_aux_w, INSTR(ds_aux_w, ';') + 1, 255);
	END IF;
END IF;

IF	(ds_material_especial_w IS NULL) THEN
	ds_material_especial_w := 'X';
END IF;

OPEN C01;
LOOP
FETCH C01 INTO	cd_procedimento_w,
			nr_seq_exame_w,
			cd_exame_w,
			cd_exame_integracao_w,
			nr_sequencia_w,
			nm_exame_w;
	EXIT WHEN C01%NOTFOUND;
	BEGIN
	IF	(ie_opcao_w = 'CP') THEN
		Resultado_w	:= Resultado_w || cd_procedimento_w || ie_separador_p;
	ELSIF	(ie_opcao_w = 'EX') THEN
		Resultado_w	:= Resultado_w || nr_seq_exame_w || ie_separador_p;
	ELSIF	(ie_opcao_w = 'CE') THEN
		Resultado_w	:= Resultado_w || cd_exame_w || ie_separador_p;
	ELSIF	(ie_opcao_w = 'CI') THEN
		Resultado_w	:= Resultado_w || cd_exame_integracao_w || ie_separador_p;
	ELSIF	(ie_opcao_w = 'CI3') THEN
		Resultado_w	:= Resultado_w || SUBSTR(cd_exame_integracao_w || '   ',1,3) || ie_separador_p;
	ELSIF	(ie_opcao_w = 'CI8') THEN
		IF (nr_seq_amostra_w IS NOT NULL) THEN
			SELECT	NVL(cd_exame_integracao, cd_exame)
			INTO	cd_exame_integracao_w
			FROM	exame_laboratorio
			WHERE nr_seq_superior = nr_seq_exame_w
			  AND nr_seq_apresent = nr_seq_amostra_w;

			Resultado_w	:= Resultado_w || SUBSTR(cd_exame_integracao_w || '        ',1,8) || ie_separador_p;
		ELSE
			Resultado_w	:= Resultado_w || SUBSTR(cd_exame_integracao_w || '        ',1,8) || ie_separador_p;
		END IF;
	ELSIF	(ie_opcao_w = 'NME3') THEN
		Resultado_w	:= Resultado_w || SUBSTR(nm_exame_w || '   ',1,3) || ie_separador_p;
	ELSIF	(ie_opcao_w = 'NM') THEN
		Resultado_w	:= Resultado_w || nm_exame_w || ie_separador_p;
	END IF;
	IF	(ie_tam_linha_p <> 0) AND
		((LENGTH(resultado_w)/ (qt_linha_w + 1)) >= ie_tam_linha_p) THEN
		Resultado_w := Resultado_w || CHR(13) || CHR(10);
		qt_linha_w := qt_linha_w + 1;
	END IF;
	END;
END LOOP;

RETURN trim(resultado_w || ' ');

END Obter_Exames_Prescr_Lab_Exter;
/