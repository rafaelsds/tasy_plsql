create or replace function obter_vl_ult_resgate(nr_seq_aplicacao_p	number) 
		return number is

vl_ult_resg_w		number(15,2);

begin

if (nr_seq_aplicacao_p is not null) then
	begin
	select	nvl(nvl(vl_resgate_estrang,vl_resgate),0) vl_resgate
	into	vl_ult_resg_w
	from 	banco_resgate
	where 	nr_seq_aplicacao = nr_seq_aplicacao_p
	and 	dt_resgate = 	(select max(x.dt_resgate) 
				from banco_resgate x 
				where x.nr_seq_aplicacao = nr_seq_aplicacao_p);
	exception when others then
	vl_ult_resg_w := null;
	end;
end if;

return vl_ult_resg_w;
end obter_vl_ult_resgate;
/
