create or replace
function pls_obter_se_historico_atend
			(nr_seq_tipo_historico_p	varchar2)
 		    	return varchar2 is

qt_tipo_historico_w	number(5);
ds_retorno_w		varchar2(1);
			
begin


select	count(*)
into	qt_tipo_historico_w
from	pls_tipo_historico_atend
where	ie_gerado_sistema	= 'S'
and	ie_situacao		= 'A'
and	nr_sequencia		= nr_seq_tipo_historico_p;

if	(qt_tipo_historico_w = 0) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;

return	ds_retorno_w;

end pls_obter_se_historico_atend;
/