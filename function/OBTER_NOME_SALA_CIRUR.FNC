create or replace
function obter_nome_sala_cirur(	nr_sequencia_p	number,
				ie_opcao_p	varchar2 )
				return varchar2 is

ds_sala_painel_w	varchar2(15);

begin

if	(nr_sequencia_p > 0) then
	select	ds_sala_painel
	into	ds_sala_painel_w
	from	sala_cirurgia
	where	nr_sequencia	=	nr_sequencia_p;
end if;

return	ds_sala_painel_w;

end obter_nome_sala_cirur;
/