create or replace
function  OBTER_PREV_CONTA_PROT
		(nr_atendimento_p	in	number,
		 nr_interno_conta_p	in	number,
		 ie_tipo_atendimento_p	in 	number,
		 cd_estabelecimento_p	in 	number,
		 cd_convenio_p		in 	number,
		 dt_referencia_p	in	date)
		 return date is


/*Function chamada na cria��o da conta paciente*/		 
		 
dt_prev_protocolo_w		date;
qt_dias_previsao_w		number(10,0);
qt_dias_w			number(10,0);
qt_dia_util_w			number(10,0);
ie_regra_w			varchar2(1);
dt_prev_conta_w			date;

cursor c01 is
select	qt_dias_previsao,
	dt_prev_conta,
	nvl(ie_regra,'P')
from	regra_prev_conta_prot
where	cd_estabelecimento					= cd_estabelecimento_p
and	nvl(ie_tipo_atendimento, ie_tipo_atendimento_p)		= ie_tipo_atendimento_p
and	nvl(cd_convenio,nvl( cd_convenio_p,0))			= nvl( cd_convenio_p,0)
and	(nvl(ie_evento,'A') = 'C') --Cria��o da conta
and	((nvl(ie_regra,'P') = 'P') or 
	 (dt_prev_conta is not null and dt_prev_conta > dt_referencia_p))
order 	by ie_tipo_atendimento,
	nvl(cd_convenio,0),
	dt_prev_conta desc;

begin

	
dt_prev_protocolo_w	:= null;
qt_dias_previsao_w	:= null;
open c01;
loop
fetch c01 into
	qt_dias_previsao_w,
	dt_prev_conta_w,
	ie_regra_w;
exit when c01%notfound;
end loop;
close c01;

if	(qt_dias_previsao_w is not null) and
	(dt_referencia_p is not null) and
	(ie_regra_w = 'P') then

	dt_prev_protocolo_w	:= dt_referencia_p;
	qt_dias_w		:= 0;
	qt_dia_util_w		:= 0;
	while (qt_dia_util_w < qt_dias_previsao_w) loop
		if	(OBTER_SE_DIA_UTIL(dt_prev_protocolo_w + qt_dias_w, cd_estabelecimento_p) = 'S') then
			qt_dia_util_w	:= qt_dia_util_w + 1;
		end if;
		qt_dias_w	:= qt_dias_w + 1;
	end loop;
	dt_prev_protocolo_w	:= dt_prev_protocolo_w + qt_dias_w;
	
elsif	(ie_regra_w = 'D') and
	(dt_prev_conta_w is not null) and
	(dt_referencia_p is not null) then
	
	dt_prev_protocolo_w	:= dt_prev_conta_w;	

end if;

return	dt_prev_protocolo_w;

end OBTER_PREV_CONTA_PROT;
/
