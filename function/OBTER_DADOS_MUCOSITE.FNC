create or replace
function obter_dados_mucosite  (  	nr_sequencia_p		number,
					nr_atendimento_p	number,
					ie_opcao_p		varchar)
 		    		return Varchar is
/*
Protocolo prescrito na SAE com ades�o	'PA'
*/
			
qt_prescrito_adesao_w	Number(10);
ds_retorno_w		Varchar2(255);
	
begin


if	(ie_opcao_p = 'PA') then

	select 	count(*)
	into	qt_prescrito_adesao_w
	from	pe_prescricao a,
		gqa_pendencia_pac b,
		gqa_pendencia_regra c,
		gqa_pend_pac_acao e 
	where	e.nr_seq_superior = nr_sequencia_p
	and	a.nr_seq_pend_pac_acao = e.nr_sequencia
	and	e.nr_seq_pend_pac =  b.nr_sequencia
	and	b.nr_seq_pend_regra = c.nr_sequencia
	and	b.ie_escala = 100;

end if;

ds_retorno_w := qt_prescrito_adesao_w;

return	ds_retorno_w;

end obter_dados_mucosite;
/