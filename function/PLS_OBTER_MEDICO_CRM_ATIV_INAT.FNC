create or replace
function pls_obter_medico_crm_ativ_inat
		(	nr_crm_p		varchar2	)
 		    	return varchar2 is
			
cd_pessoa_fisica_w 	varchar2(10);

Cursor C01 is 
	select	cd_pessoa_fisica
	from	medico
	where	nr_crm		= nr_crm_p
	order by nvl(ie_situacao,'I') desc;

begin
open C01;
loop
fetch C01 into	
	cd_pessoa_fisica_w;
exit when C01%notfound;
end loop;
close C01;

return	cd_pessoa_fisica_w;

end pls_obter_medico_crm_ativ_inat;
/