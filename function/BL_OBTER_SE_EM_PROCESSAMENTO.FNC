create or replace
function BL_obter_se_em_processamento(	nr_seq_frasco_p		Number)
					return Varchar2 is

ds_retorno_w		Varchar2(1);
					
begin
if	(nr_seq_frasco_p is not null) then
	
	begin
	select	'S'
	into	ds_retorno_w
	from	bl_agrup_frasco a,
		bl_agrupamento b,
		bl_frasco c
	where	a.nr_seq_agrupamento = b.nr_sequencia
	and	c.nr_sequencia = a.nr_seq_frasco
	and	c.nr_sequencia = nr_seq_frasco_p
	and	b.dt_liberacao is null
	and	rownum = 1;
	exception
	when others then
		ds_retorno_w := 'N';
	end;
	
	if	(ds_retorno_w = 'N') then
		begin
		select	'S'
		into	ds_retorno_w
		from	bl_lote_item a,
			bl_frasco c
		where	c.nr_sequencia = a.nr_seq_frasco
		and	c.nr_sequencia = nr_seq_frasco_p
		and	rownum = 1;
		exception
		when others then
			ds_retorno_w := 'N';
		end;
	end if;
	
end if;

return	ds_retorno_w;

end BL_obter_se_em_processamento;
/