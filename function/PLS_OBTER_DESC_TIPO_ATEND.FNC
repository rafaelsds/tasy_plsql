create or replace
function pls_obter_desc_tipo_atend
		(	nr_seq_tipo_atend_p	Number)
 		    	return Varchar2 is
			
ds_retorno_w	Varchar2(255);

begin

select	nvl(max(ds_tipo_atendimento),'')
into	ds_retorno_w
from	pls_tipo_atendimento
where	nr_sequencia	= nr_seq_tipo_atend_p;

return	ds_retorno_w;

end pls_obter_desc_tipo_atend;
/