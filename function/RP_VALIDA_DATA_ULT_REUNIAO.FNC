create or replace function rp_valida_data_ult_reuniao(nr_seq_pac_reab_p	number)
 		    	return varchar2 is

dt_ult_reuniao_w	varchar2(255);
ds_retorno_w		varchar2(10);
begin

ds_retorno_w := 'S';
select 	rp_obter_data_ult_reuniao(nr_seq_pac_reab_p)
into	dt_ult_reuniao_w
from 	dual;

if (dt_ult_reuniao_w is not null) then
	if (dt_ult_reuniao_w < (sysdate - 549)) then
		ds_retorno_w := 'N';
	else
		ds_retorno_w := 'S';
	end if;
end if;


return	ds_retorno_w;

end rp_valida_data_ult_reuniao;
/