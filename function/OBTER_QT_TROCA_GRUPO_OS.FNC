create or replace
function OBTER_QT_TROCA_GRUPO_OS
			(nr_seq_ordem_serv_p	number,			
			nr_seq_grupo_p		number,
			nr_seq_gerencia_p	number)
			return number is

qt_trocas_w	number(10);			
begin

if	(nvl(nr_seq_grupo_p,0) > 0) then

	select	count(1)
	into	qt_trocas_w
	from	man_ordem_log_grupo_des
	where	nr_seq_ordem_serv 	= nr_seq_ordem_serv_p 
	and	nr_seq_grupo_des	= nr_seq_grupo_p;
	
elsif	(nvl(nr_seq_gerencia_p,0) > 0) then

	select	count(1)
	into	qt_trocas_w
	from	man_ordem_log_grupo_des a,
		grupo_desenvolvimento b
	where	a.nr_seq_ordem_serv 	= nr_seq_ordem_serv_p 
	and	a.nr_seq_grupo_des	= b.nr_sequencia
	and	b.nr_seq_gerencia	= nr_seq_gerencia_p;

end if;

if	(qt_trocas_w > 0) then
	qt_trocas_w := qt_trocas_w - 1;
end if;

return	qt_trocas_w;

end OBTER_QT_TROCA_GRUPO_OS;
/