create or replace
function obter_end_cnpj_ind(cd_cnpj_indicacao_p	varchar2)
 		    	return varchar2 is

ds_endereco_w	varchar2(255);

begin

select	substr(a.ds_endereco||' '||a.nr_endereco||' '||a.ds_bairro||' '||a.ds_municipio||' '||obter_dados_pf_pj(null, a.cd_cgc, 'DS_UF'),1,255)
into	ds_endereco_w
from	pessoa_juridica a
where	a.cd_cgc = cd_cnpj_indicacao_p;

return	ds_endereco_w;

end obter_end_cnpj_ind;

/
