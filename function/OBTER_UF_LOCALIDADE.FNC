CREATE OR REPLACE
FUNCTION OBTER_UF_LOCALIDADE
		(CD_LOCALIDADE_P		VARCHAR2)
		RETURN VARCHAR2 IS

DS_RESULTADO_W		CEP_LOC.DS_UF%type;
ie_cep_novo_w		varchar2(1);

CURSOR	C01 IS
	SELECT	CD_UNIDADE_FEDERACAO
	FROM	CEP_LOCALIDADE
	WHERE	CD_LOCALIDADE	= CD_LOCALIDADE_P;

CURSOR	C02 IS
	SELECT	DS_UF
	FROM	CEP_LOC
	WHERE	CD_CEP	= CD_LOCALIDADE_P;


BEGIN

if	(CD_LOCALIDADE_P	is not null) then

	select	NVL(vl_parametro, vl_parametro_padrao)
	into	ie_cep_novo_w
	from	funcao_parametro
	where	cd_funcao	= 0
	and	nr_sequencia	= 25;

	IF	(ie_cep_novo_w = 'N') THEN

		BEGIN
		OPEN C01;
		LOOP
		FETCH	C01 INTO DS_RESULTADO_W;
			EXIT WHEN C01%NOTFOUND;
		END LOOP;
		CLOSE C01;
		
		EXCEPTION
			WHEN OTHERS THEN
				DS_RESULTADO_W	:= '';
		END;

	ELSE
		BEGIN
		OPEN C02;
		LOOP
		FETCH	C02 INTO DS_RESULTADO_W;
			EXIT WHEN C02%NOTFOUND;
		END LOOP;
		CLOSE C02;
		
		EXCEPTION
			WHEN OTHERS THEN
				DS_RESULTADO_W	:= '';
		END;	
	END IF;

end if;

RETURN	DS_RESULTADO_W;

END OBTER_UF_LOCALIDADE;
/