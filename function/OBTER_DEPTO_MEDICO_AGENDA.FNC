create or replace
function obter_depto_medico_agenda(	nr_seq_lista_p	number,
					ie_tipo_p	number)	return varchar2 is

cd_depto_medico_w	agenda_lista_espera.cd_departamento_medico%type;
ds_return_w		varchar2(255) := '';

begin

if(nvl(nr_seq_lista_p, 0) > 0)then
	
	select	max(al.cd_departamento_medico)
	into	cd_depto_medico_w
	from	agenda_lista_espera al
	where 	nr_sequencia = nr_seq_lista_p;
	
	if(nvl(cd_depto_medico_w, 0) > 0)then
		if(ie_tipo_p = 1)then
			ds_return_w := obter_nome_departamento_medico(cd_depto_medico_w);
		elsif (ie_tipo_p = 2)then
			ds_return_w := obter_nome_curto_depto_medico(cd_depto_medico_w);	
		end if;
	end if;
end if;

return	ds_return_w;

end obter_depto_medico_agenda;
/