create or replace
function pls_obter_proxima_exec_item
			(	dt_execucao_p		date,
				nr_seq_segurado_p	number,
				nr_seq_proc_p		number,
				ie_tipo_inf_p		varchar2)
				return varchar2 is
				
cd_area_procedimento_w		Number(15)	:= 0;
cd_especialidade_w		Number(15) 	:= 0;
cd_grupo_proc_w			Number(15) 	:= 0;
cd_procedimento_w		Number(15)	:= 0;
ie_origem_proced_w		Number(10)	:= 0;
nr_seq_grupo_w			number(10)	:= 0;
nr_seq_regra_w			Number(10)	:= 0;
ie_tipo_quantidade_w		Varchar2(3)	:= 'X';
qt_permitida_w			Number(9,3);
qt_tipo_quantidade_w		number(10);
dt_liberacao_w			date		:= sysdate;
qt_reg_w			number(10);
ie_tipo_guia_w			varchar2(2);
nr_seq_prestador_w		number(10);
cd_estabelecimento_w		number(10);
ds_retorno_w			varchar2(50);
qt_procedimento_w		number(5);
ie_item_liberado_w		varchar2(1)	:= 'N';

Cursor C01 is
	select	a.nr_sequencia,
		a.ie_tipo_quantidade,
		a.qt_permitida,
		a.qt_tipo_quantidade
	from	pls_regra_qtde_proc	a
	where	a.ie_situacao		= 'A'
	--and	a.cd_estabelecimento	= cd_estabelecimento_w
	and	trunc(dt_execucao_p) between trunc(a.dt_inicio_vigencia) and fim_dia(nvl(a.dt_fim_vigencia, dt_execucao_p))
	and	nvl(a.cd_procedimento, nvl(cd_procedimento_w,0))		= nvl(cd_procedimento_w,0)
	and	nvl(a.ie_origem_proced, nvl(ie_origem_proced_w,0))		= nvl(ie_origem_proced_w,0)
	and	nvl(a.cd_grupo_proc, nvl(cd_grupo_proc_w,0))			= nvl(cd_grupo_proc_w,0)
	and	nvl(a.cd_especialidade, nvl(cd_especialidade_w,0))		= nvl(cd_especialidade_w,0)
	and	nvl(a.cd_area_procedimento, nvl(cd_area_procedimento_w,0)) 	= nvl(cd_area_procedimento_w,0)
	and	nvl(a.nr_seq_grupo_servico, nvl(nr_seq_grupo_w,0)) = nvl(nr_seq_grupo_w,0)
	order by a.cd_area_procedimento,
		a.cd_especialidade,
		a.cd_grupo_proc,
		a.cd_procedimento;
		
Cursor C02 is
	select	a.dt_liberacao
	into	dt_liberacao_w
	from	pls_guia_plano		b,
		pls_guia_plano_proc	a
	where	a.nr_seq_guia		= b.nr_sequencia
	and	b.nr_seq_segurado	= nr_seq_segurado_p
	and	a.cd_procedimento	= cd_procedimento_w
	and	a.ie_origem_proced	= ie_origem_proced_w
	and	a.dt_liberacao >= (dt_execucao_p - qt_tipo_quantidade_w)
	order by 1;

begin

select	cd_procedimento,
	ie_origem_proced,
	ie_tipo_guia,
	nr_seq_prestador,
	cd_estabelecimento
into	cd_procedimento_w,
	ie_origem_proced_w,
	ie_tipo_guia_w,
	nr_seq_prestador_w,
	cd_estabelecimento_w
from	pls_guia_plano_proc 	b,
	pls_guia_plano		a
where	b.nr_sequencia	= nr_seq_proc_p
and	b.nr_seq_guia	= a.nr_sequencia;

nr_seq_grupo_w	:= pls_obter_grupo_servico(cd_procedimento_w, ie_origem_proced_w);

pls_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, cd_area_procedimento_w,
	cd_especialidade_w, cd_grupo_proc_w, ie_origem_proced_w);
	
open C01;
loop
fetch C01 into
	nr_seq_regra_w,
	ie_tipo_quantidade_w,
	qt_permitida_w,
	qt_tipo_quantidade_w;
exit when C01%notfound;
end loop;
close C01;

if	(nr_seq_regra_w	> 0) then	
	if	(ie_tipo_quantidade_w	= 'D') then
		select 	nvl(sum(a.qt_autorizada),0)
		into	qt_procedimento_w
		from	pls_guia_plano		b,
			pls_guia_plano_proc	a
		where	a.nr_seq_guia		= b.nr_sequencia
		and	b.nr_seq_segurado	= nr_seq_segurado_p
		and	a.cd_procedimento	= cd_procedimento_w
		and	a.ie_origem_proced	= ie_origem_proced_w
		and	trunc(nvl(a.dt_liberacao,a.dt_atualizacao)) between
			(trunc(dt_execucao_p) - qt_tipo_quantidade_w) and trunc(dt_execucao_p);
	elsif	(ie_tipo_quantidade_w	= 'M') then
		select 	nvl(sum(a.qt_autorizada),0)
		into	qt_procedimento_w
		from	pls_guia_plano		b,
			pls_guia_plano_proc	a
		where	a.nr_seq_guia		= b.nr_sequencia
		and	b.nr_seq_segurado	= nr_seq_segurado_p
		and	a.cd_procedimento	= cd_procedimento_w
		and	a.ie_origem_proced	= ie_origem_proced_w
		and	trunc(nvl(a.dt_liberacao,a.dt_atualizacao), 'month') between
			(trunc(add_months(dt_execucao_p, - qt_tipo_quantidade_w ), 'month')) and trunc(dt_execucao_p, 'month');
	elsif	(ie_tipo_quantidade_w	= 'A') then
		select 	nvl(sum(a.qt_autorizada),0)
		into	qt_procedimento_w
		from	pls_guia_plano		b,
			pls_guia_plano_proc	a
		where	a.nr_seq_guia		= b.nr_sequencia
		and	b.nr_seq_segurado	= nr_seq_segurado_p
		and	a.cd_procedimento	= cd_procedimento_w
		and	a.ie_origem_proced	= ie_origem_proced_w
		and	trunc(nvl(a.dt_liberacao,a.dt_atualizacao), 'yyyy') between
			(trunc(add_months(dt_execucao_p, - ( 12 * qt_tipo_quantidade_w )), 'yyyy')) and trunc(dt_execucao_p, 'yyyy');			
	end if;
	
	if	(qt_procedimento_w >= qt_permitida_w) then		
		open C02;
		loop
		fetch C02 into	
			dt_liberacao_w;
		exit when C02%notfound;
			begin
			qt_reg_w := qt_reg_w + 1;
			if	(qt_reg_w = qt_permitida_w) then
				goto final;
			end if;
			end;
		end loop;
		close C02;
		<<final>>
		qt_reg_w := 0;
	end if;		
end if;
			

if	(nr_seq_regra_w	> 0) then
	if	(ie_tipo_quantidade_w	= 'D') then
		dt_liberacao_w := dt_liberacao_w + qt_tipo_quantidade_w;
	elsif	(ie_tipo_quantidade_w	= 'M') then
		dt_liberacao_w := add_months(dt_liberacao_w, qt_tipo_quantidade_w );
	elsif	(ie_tipo_quantidade_w	= 'A') then
		dt_liberacao_w := add_months(dt_liberacao_w, ( 12 * qt_tipo_quantidade_w ));
	end if;
end if;

if	(ie_tipo_inf_p = 'D') then	
	ds_retorno_w := to_char(dt_liberacao_w,'DD/MM/YYYY');
elsif	(ie_tipo_inf_p = 'L') then
	if	(dt_liberacao_w <= sysdate) then
		ie_item_liberado_w := 'S';
	end if;
	ds_retorno_w := ie_item_liberado_w;
end if;	

return	ds_retorno_w;

end pls_obter_proxima_exec_item;
/
