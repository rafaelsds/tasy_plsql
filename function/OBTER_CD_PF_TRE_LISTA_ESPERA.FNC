create or replace
function obter_cd_pf_tre_lista_espera(nr_seq_pac_esp_p		number)
 		    	return varchar2 is
			
ds_retorno_w		Varchar2(255);			

begin

if (nr_seq_pac_esp_p is not null) then

	select	max(cd_pessoa_fisica)
	into	ds_retorno_w
	from   	tre_pf_lista_espera
	where  	nr_sequencia = nr_seq_pac_esp_p;

end if;

return	ds_retorno_w;

end obter_cd_pf_tre_lista_espera;
/