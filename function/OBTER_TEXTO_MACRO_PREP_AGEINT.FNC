create or replace
function obter_texto_macro_prep_ageint	(
					nr_seq_regra_prep_p	number,
					nr_seq_ageint_item_p number,
					ds_texto_p	clob,
					ie_dia_p varchar2 default 'S'
					)
					return clob is

/* dominio 10184 */

TYPE lista_macros IS TABLE OF VARCHAR2(255) INDEX BY PLS_INTEGER;

vl_parametro_w	varchar2(10);
ds_comando_w		varchar2(255);
ds_param_w		varchar2(255);
qt_valor_temp_w	AGEINT_ORIENT_PREP_MACRO.vl_macro%type := 0;
qt_valor_w	AGEINT_ORIENT_PREP_MACRO.vl_macro%type := 0;
nr_seq_agenda_exame_w agenda_integrada_item.nr_seq_agenda_exame%type;
nr_seq_agenda_cons_w agenda_integrada_item.nr_seq_agenda_cons%type;
dt_agenda_w		date;
nr_seq_agenda_int_w	agenda_integrada.nr_sequencia%type;
ds_retorno_w	clob := '';
nr_seq_topografia_w	agenda_paciente.cd_topografia_proced%type;
ie_tipo_atendimento_w	agenda_paciente.ie_tipo_atendimento%type;
lista_macros_w	lista_macros;
ind	number(10);
ds_lista_macro_w varchar2(4000) := ageint_obter_macro_prep('S');
ds_macro_w varchar2(255) := '';

cursor c01 is
	select nr_seq_agenda_exame, nr_seq_proc_interno, obter_estab_item_ageint(a.nr_seq_agenda_cons, a.nr_seq_agenda_exame) cd_estabelecimento
	from agenda_integrada_item a
	where nr_seq_agenda_int = nr_seq_agenda_int_w
	and nr_seq_proc_interno is not null
	and ((ie_dia_p = 'N')
	or (((nr_seq_agenda_exame is not null) and (select max(trunc(x.dt_agenda))
												from agenda_paciente x
												where x.nr_sequencia = a.nr_seq_agenda_exame
												and x.ie_status_agenda <> 'C') = dt_agenda_w)
	or ((nr_seq_agenda_cons is not null) and (select max(trunc(x.dt_agenda))
											from agenda_consulta x
											where x.nr_sequencia = a.nr_seq_agenda_cons
											and x.ie_status_agenda <> 'C')= dt_agenda_w)));

begin
ds_retorno_w := ds_texto_p;
if	(nr_seq_ageint_item_p is not null) and
	(ds_texto_p is not null) then
	ind := 1;
	while (length(ds_lista_macro_w) > 0) loop
		ds_macro_w := substr(ds_lista_macro_w, 1, instr(ds_lista_macro_w,';')-1);
		ds_lista_macro_w := substr(ds_lista_macro_w, instr(ds_lista_macro_w,';')+1);
		lista_macros_w(ind) := ds_macro_w;
		ind := ind+1;
	end loop;
	ind := lista_macros_w.last;
	
	select max(nr_seq_agenda_exame), 
			max(nr_seq_agenda_cons),
			max(nr_seq_agenda_int)
	into nr_seq_agenda_exame_w,
		nr_seq_agenda_cons_w,
		nr_seq_agenda_int_w
	from agenda_integrada_item
	where nr_sequencia = nr_seq_ageint_item_p;
	
	if (nr_seq_agenda_exame_w is not null) then
		select 	max(trunc(dt_agenda))
		into 	dt_agenda_w
		from 	agenda_paciente
		where 	nr_sequencia = nr_seq_agenda_exame_w;
	else
		select 	max(trunc(dt_agenda))
		into 	dt_agenda_w
		from 	agenda_consulta
		where 	nr_sequencia = nr_seq_agenda_cons_w;
	end if;
	
	while (ind > 0) loop
		qt_valor_w := 0;
		qt_valor_temp_w := 0;
		for c01_w in c01 loop
			select 	nvl(max(ie_tipo_atendimento),0),
					nvl(max(cd_topografia_proced),0)
			into 	ie_tipo_atendimento_w,
					nr_seq_topografia_w
			from 	agenda_paciente
			where 	nr_sequencia = c01_w.nr_seq_agenda_exame;
			
			select max(vl_macro)
			into qt_valor_temp_w
			from AGEINT_ORIENT_PREP_MACRO a
			where ie_macro = ind
			and a.nr_seq_orient_prep_regra in 
							(select x.nr_sequencia
							from AGEINT_ORIENT_PREP_REGRA x
							where nvl(x.cd_estabelecimento,c01_w.cd_estabelecimento) = c01_w.cd_estabelecimento
							and nvl(x.nr_seq_proc_interno,c01_w.nr_seq_proc_interno) = c01_w.nr_seq_proc_interno
							and	nvl(x.nr_seq_topografia, nvl(nr_seq_topografia_w,0)) = nvl(nr_seq_topografia_w,0)
							and	nvl(x.ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,0)
							and x.nr_seq_orient_preparo = nr_seq_regra_prep_p);
			if (qt_valor_temp_w > qt_valor_w) then
				qt_valor_w := qt_valor_temp_w;
			end if;
		end loop;
		ds_retorno_w := replace_macro_clob(ds_retorno_w, lista_macros_w(ind), qt_valor_w);
		ind := ind-1;
	end loop;
end if;

return ds_retorno_w;

end obter_texto_macro_prep_ageint;
/