create or replace
function obter_qt_sc_comprometido(	nr_solic_compra_p			number,
					nr_item_solic_compra_p		number,
					dt_entrega_p				date,
					cd_estabelecimento_p		number default 1)
 		    	return number is

qt_material_w				solic_compra_item.qt_material%type;			
qt_material_ordem_w			ordem_compra_item_entrega.qt_prevista_entrega%type;
qt_material_cot_w			cot_compra_item.qt_material%type;
ie_cotacao_pendente_w		parametro_compras.ie_regra_gerar_solic_req%type;

/* 
Author: LSSILVA 
OS: 1585561

Criado para substituir a utilizacao da seguinte consulta:

obter_qt_sc_em_ordem_sem_cot(solic_compra.nr_solic_compra, 
					solic_compra_item.nr_item_solic_compra, 
					solic_compra_item_entrega.dt_entrega_solicitada) +
	(SELECT nvl(sum(x.qt_material), 0)  
	FROM	cot_compra_item x  
	WHERE x.nr_solic_compra = solic_compra_item.nr_solic_compra   
	AND	x.nr_item_solic_compra = solic_compra_item.nr_item_solic_compra)

Utilizacoes:
Funcao Solicitacao de Compra
	Aba Pendente - WCPanel 344737
	
Objetos
	baixar_solic_compra_item
	baixar_solic_compra
*/
			
begin

select	nvl(max(IE_CONS_COT_PEND),'S')
into	ie_cotacao_pendente_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

if (ie_cotacao_pendente_w = 'S') then
	begin
	
		if (dt_entrega_p is not null) then
			begin
				select	nvl(sum(b.qt_prevista_entrega),0)
				into	qt_material_ordem_w
				from	ordem_compra_item a,
					ordem_compra_item_entrega b
				where	a.nr_ordem_compra = b.nr_ordem_compra
				and	a.nr_item_oci = b.nr_item_oci
				and	a.nr_cot_compra is null
				and	a.nr_solic_compra = nr_solic_compra_p
				and	a.nr_item_solic_compra = nr_item_solic_compra_p
				and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(b.dt_entrega_original) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_entrega_p) /*Tem que usar essa data entrega original, porque nesse campo fica sempre gravado a data de entrega da solicitacao. Nao pode usar a data prevista entrega porque pode ser alterada e dai nao vai funcionar a comparacao*/
				and	b.dt_cancelamento is null;	
			
			end;
		else
			begin
				select	nvl(sum(b.qt_prevista_entrega),0)
				into qt_material_ordem_w
				from	ordem_compra_item a,
					ordem_compra_item_entrega b,
				solic_compra_item sci,
				solic_compra_item_entrega scie
				where	a.nr_ordem_compra = b.nr_ordem_compra
				and	a.nr_item_oci = b.nr_item_oci
				and	a.nr_cot_compra is null
				and	a.nr_solic_compra = nr_solic_compra_p
				and	a.nr_item_solic_compra = nr_item_solic_compra_p
				AND	sci.nr_solic_compra = a.nr_solic_compra 
				AND	sci.nr_item_solic_compra = a.nr_item_solic_compra 
				AND	sci.nr_solic_compra = scie.nr_solic_compra(+)
				AND	sci.nr_item_solic_compra = scie.nr_item_solic_compra(+) 
				and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(b.dt_entrega_original) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(scie.dt_entrega_solicitada) /*Tem que usar essa data entrega original, porque nesse campo fica sempre gravado a data de entrega da solicitacao. Nao pode usar a data prevista entrega porque pode ser alterada e dai nao vai funcionar a comparacao*/
				and	b.dt_cancelamento is null;
				
			end;
		end if;
		
		select	nvl(sum(x.qt_material), 0)
		into	qt_material_cot_w
		from	cot_compra_item x
		where	x.nr_solic_compra = nr_solic_compra_p
		and	x.nr_item_solic_compra = nr_item_solic_compra_p;
		
		qt_material_w := qt_material_ordem_w + qt_material_cot_w;	
	end;
else
	begin
	
		select nvl(qt_material_cancelado,0)
		into qt_material_w
		from solic_compra_item
		where nr_solic_compra = nr_solic_compra_p
		and nr_item_solic_compra = nr_item_solic_compra_p;
		
		if (dt_entrega_p is not null) then
			begin
				select 	nvl(sum(e.qt_prevista_entrega),0)
				into	qt_material_ordem_w
				from	ordem_compra o,
					ordem_compra_item i,
					ordem_compra_item_entrega e
				where	o.nr_ordem_compra = i.nr_ordem_compra
				and	i.nr_ordem_compra = e.nr_ordem_compra
				and	i.nr_item_oci = e.nr_item_oci
				and	i.nr_solic_compra = nr_solic_compra_p
				and	i.nr_item_solic_compra = nr_item_solic_compra_p
				and	o.dt_liberacao is not null
				and	i.dt_reprovacao is null
				and o.nr_seq_motivo_cancel is null	
				and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(e.dt_entrega_original) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_entrega_p)
				and	e.dt_cancelamento is null;
			end;
		else
			begin
				select 	nvl(sum(e.qt_prevista_entrega),0)
				into	qt_material_ordem_w
				from	ordem_compra o,
					ordem_compra_item i,
					ordem_compra_item_entrega e
				where	o.nr_ordem_compra = i.nr_ordem_compra
				and	i.nr_ordem_compra = e.nr_ordem_compra
				and	i.nr_item_oci = e.nr_item_oci
				and	i.nr_solic_compra = nr_solic_compra_p
				and	i.nr_item_solic_compra = nr_item_solic_compra_p
				and	o.dt_liberacao is not null
				and	i.dt_reprovacao is null
				and o.nr_seq_motivo_cancel is null
				and	e.dt_cancelamento is null;
			end;
		end if;
	
		qt_material_w := qt_material_w + qt_material_ordem_w;	
	end;
end if;

if (qt_material_w < 0) then
	begin
		qt_material_w := 0;
	end;
end if;

return	qt_material_w;

end obter_qt_sc_comprometido;
/
