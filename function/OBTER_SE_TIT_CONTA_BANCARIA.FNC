create or replace
function obter_se_tit_conta_bancaria
			(	cd_cgc_p		varchar2,
				cd_pessoa_fisica_p	varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
        nr_titulo_p number default null)
				return varchar2 is

ie_permite_w		varchar2(1)	:= 'N';
ie_conta_w		varchar2(1);
ie_conta_favorecido_w		varchar2(1);
qt_conta_w		number(10);

begin
Obter_Param_Usuario(851, 107, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_conta_w);
Obter_Param_Usuario(851, 217, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_conta_favorecido_w);

select	sum(qt_conta)
into	qt_conta_w
from	(select	count(*) qt_conta
	from	banco b,
		pessoa_juridica_conta a
	where	a.cd_cgc		= cd_cgc_p
	and	a.cd_banco		= b.cd_banco
	and	nvl(a.ie_situacao,'A')	= 'A'
	and	('N' = ie_conta_w or a.ie_conta_pagamento = 'S')
	and	'S' = verifica_conta_estab(a.cd_banco, a.cd_agencia_bancaria, a.nr_conta, null, a.cd_cgc, cd_estabelecimento_p)
	union
	select	count(*)
	from	banco b,
		pessoa_fisica_conta a
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	a.cd_banco		= b.cd_banco
	and	nvl(a.ie_situacao,'A')	= 'A'
	and	('N' = ie_conta_w or a.ie_conta_pagamento= 'S')
	and	'S' = verifica_conta_estab(a.cd_banco, a.cd_agencia_bancaria, a.nr_conta, a.cd_pessoa_fisica, null, cd_estabelecimento_p)
  union
  select count(*)
  from   banco b,
         titulo_pagar_favorecido a
  where  a.cd_banco          = b.cd_banco
  and   nvl(ie_conta_favorecido_w,'N') = 'S'
  and    'S' = verifica_conta_estab( a.cd_banco, a.cd_agencia_bancaria, a.nr_conta, a.cd_pessoa_fisica, a.cd_cgc, cd_estabelecimento_p)
  and   a.nr_titulo = nr_titulo_p);

if	(qt_conta_w > 0) then
	ie_permite_w	:= 'S';
end if;

return	ie_permite_w;

end obter_se_tit_conta_bancaria;
/
