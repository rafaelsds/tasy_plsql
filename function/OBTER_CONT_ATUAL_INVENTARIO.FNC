create or replace 
function OBTER_CONT_ATUAL_INVENTARIO(	nr_sequencia_p	number)
					 	return	number is

qt_item_cont_w		number(05,0);
qt_item_recont_w		number(05,0);
qt_item_seg_recont_w		number(05,0);
qt_item_terc_recont_w	number(05,0);
qt_retorno_w			number(01,0);


begin

select	count(*) 
into	qt_item_cont_w
from	inventario_material 
where	nr_seq_inventario = nr_sequencia_p
and	qt_contagem is null
and	qt_inventario is null;

select	count(*) 
into	qt_item_recont_w
from	inventario_material 
where	nr_seq_inventario = nr_sequencia_p
and	qt_recontagem is null
and	qt_inventario is null;

select	count(*) 
into	qt_item_seg_recont_w
from	inventario_material 
where	nr_seq_inventario = nr_sequencia_p
and	qt_seg_recontagem is null
and	qt_inventario is null;

select	count(*) 
into	qt_item_terc_recont_w
from	inventario_material 
where	nr_seq_inventario = nr_sequencia_p
and	qt_terc_recontagem is null
and	qt_inventario is null;

if	(qt_item_cont_w > 0) then
	qt_retorno_w := 1;
elsif	(qt_item_recont_w > 0) then
	qt_retorno_w := 2;
elsif	(qt_item_seg_recont_w > 0) then
	qt_retorno_w := 3;
elsif	(qt_item_terc_recont_w > 0) then
	qt_retorno_w := 4;
else
	qt_retorno_w := 0;
end if;

return qt_retorno_w;

end OBTER_CONT_ATUAL_INVENTARIO;
/