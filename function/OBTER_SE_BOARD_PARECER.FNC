create or replace
function Obter_se_board_Parecer	(nr_seq_tipo_parecer_p number)
 		    	return varchar2 is
qt_reg_w	number(10);
begin

select	count(*)
into	qt_reg_w	
from	agenda
where	nr_seq_tipo_parecer	= nr_seq_tipo_parecer_p;


if	(qt_reg_w	> 0) then
	 return 'S';
end if;

return	'N';

end Obter_se_board_Parecer;
/