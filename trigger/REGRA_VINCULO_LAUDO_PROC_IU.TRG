create or replace trigger regra_vinculo_laudo_proc_iu
before insert or update on regra_vinculo_laudo_proc
for each row
begin

if 	(:new.cd_procedimento is null ) and
	(:new.cd_especialidade is null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(232322);
end if;	
end;
/