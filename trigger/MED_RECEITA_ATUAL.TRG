create or replace trigger MED_RECEITA_ATUAL
before insert or update on MED_RECEITA
for each row
declare


begin
if	(nvl(:old.DT_RECEITA,sysdate+10) <> :new.DT_RECEITA) and
	(:new.DT_RECEITA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_RECEITA, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/
