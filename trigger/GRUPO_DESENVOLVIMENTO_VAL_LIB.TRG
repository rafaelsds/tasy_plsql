create or replace trigger grupo_desenvolvimento_val_lib
before delete or update on grupo_desenvolvimento
for each row 
declare

qt_reg_w	number(10);

pragma autonomous_transaction;

begin

if	(deleting) or
	((:old.ie_situacao = 'A') and
	(:new.ie_situacao = 'I')) then
	begin
	select	count(1)
	into	qt_reg_w
	from	funcao c,
		funcao_grupo_des b, 
		grupo_desenvolvimento a
	where	a.nr_sequencia = b.nr_seq_grupo
	and	a.nr_sequencia = :old.nr_sequencia
	and	c.cd_funcao = b.cd_funcao
	and	c.ie_situacao <> 'I'
	and not exists (select	1
			from	funcao_grupo_des y,
				grupo_desenvolvimento x
			where	x.nr_sequencia = y.nr_seq_grupo
			and	x.nr_sequencia <> :old.nr_sequencia
			and	x.ie_situacao = 'A'
			and	y.cd_funcao = b.cd_funcao);

	if	(qt_reg_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1117735);
	end if;
	end;
end if;

end;
/