create or replace trigger disp_local_estoque_insert
after insert or update on local_estoque
for each row

begin
if	(:new.ie_tipo_local = '11') then
	insert into dispensario_local_estoque(
		nr_sequencia,
		cd_estabelecimento,
		cd_local_estoque,
		ds_local_estoque,
		ie_status,
		dt_exclusao,
		dt_registro,
		dt_atualizacao,
		ie_lido)
	values(disp_local_estoque_SEQ.nextval,
		:new.cd_estabelecimento,
		:new.cd_local_estoque,
		:new.ds_local_estoque,
		:new.ie_situacao,
		null,
		sysdate,
		sysdate,
		'');
end if;

end disp_local_estoque_insert;
/
