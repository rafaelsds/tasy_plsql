create or replace trigger agenda_integrada_before_insert
before insert or update on agenda_integrada
for each row 

declare 

  ds_given_name_w           person_name.ds_given_name%type;
  ds_component_name_1_w     person_name.ds_component_name_1%type;
  ds_family_name_w          person_name.ds_family_name%type;
  nm_pessoa_montar_w        agenda_integrada.nm_paciente%type;
  
begin

if	(:new.nr_seq_person_name is not null ) then
  begin
    select 	max(ds_given_name),
			max(ds_component_name_1),
			max(ds_family_name)
    into	ds_given_name_w,
			ds_component_name_1_w,
			ds_family_name_w
    from	person_name
    where	nr_sequencia = :new.nr_seq_person_name
    and	    ds_type	= 'main';
  end;
    nm_pessoa_montar_w := substr(montar_nm_pessoa_fisica(null, ds_given_name_w, ds_family_name_w, ds_component_name_1_w, :new.nm_usuario, :new.nr_seq_person_name),1,60);
  if (trim(nm_pessoa_montar_w) is  not null) then
    :new.nm_paciente	:= nm_pessoa_montar_w;
  end if;
end if;

end;
/
