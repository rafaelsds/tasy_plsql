create or replace 
trigger atend_escala_capurro_atual
before insert or update on escala_capurro
for each row
declare

exec_w               	varchar2(200);
qt_dias_w            	number(10);
qt_semanas_w         	number(10);
qt_reg_w             	number(10);
cd_estabelecimento_w 	number(10);
ie_ig_capurro_w      	varchar2(5);
ds_erro_w				varchar(4000);
ds_parametros_w			varchar(4000);
    
begin
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
		goto Final;
	end if;

	begin
		exec_w := 'begin calcula_escala_capurro_md(:1,:2,:3,:4,:5,:6,:7); end;';

		execute immediate exec_w using in :new.qt_textura_pele, 
									   in :new.qt_pregas_plantares, 
									   in :new.qt_glandula_mamaria, 
									   in :new.qt_formacao_mamilo, 
									   in :new.qt_formato_orelha,  
									   out qt_dias_w, 
									   out qt_semanas_w;
    exception
	    when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := 	substr( ':new.nr_atendimento: '||:new.nr_atendimento||'-:new.nr_atend_rn: '||:new.nr_atend_rn||'-new.qt_textura_pele: '||:new.qt_textura_pele||'-:new.qt_pregas_plantares: '||:new.qt_pregas_plantares||
										'-:new.qt_glandula_mamaria: '||:new.qt_glandula_mamaria||'-:new.qt_formacao_mamilo: '||:new.qt_formacao_mamilo||'- :new.qt_formato_orelha: '||:new.qt_formato_orelha||
										'-qt_dias_w: '||qt_dias_w||'-qt_semanas_w: '||qt_semanas_w, 1, 4000);
            gravar_log_medical_device('ATEND_ESCALA_CAPURRO_ATUAL', 'CALCULA_ESCALA_CAPURRO_MD', ds_parametros_w, ds_erro_w, :new.nm_usuario, 'N');        
			qt_dias_w    := null;
			qt_semanas_w := null;
	end;

	:new.qt_dias    := qt_dias_w;
	:new.qt_semanas	:= qt_semanas_w;

	if	(nvl(:old.dt_avaliacao,sysdate+10) <> :new.dt_avaliacao) and
		(:new.dt_avaliacao is not null) then
		:new.ds_utc		:= obter_data_utc(:new.dt_avaliacao, 'HV');
	end if;

	if	(nvl(:old.dt_liberacao,sysdate+10) <> :new.dt_liberacao) and
		(:new.dt_liberacao is not null) then
		:new.ds_utc_atualizacao	:= obter_data_utc(:new.dt_liberacao,'HV');
	end if;

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	select	count(*)
	into	qt_reg_w
	from	nascimento
	where	nr_atendimento = :new.nr_atendimento;

	select	nvl(max(ie_ig_capurro),'N')
	into	ie_ig_capurro_w
	from	parametro_medico
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(qt_reg_w > 0) and
		(ie_ig_capurro_w = 'S') then
		update	nascimento
		set	qt_sem_ig = :new.qt_semanas,
			qt_dia_ig = :new.qt_dias
		where	nr_atendimento = :new.nr_atendimento;
	end if;

	<<Final>>
	qt_reg_w	:= 0;
end;
/
