CREATE OR REPLACE TRIGGER intpd_fila_transmissao_hist
after insert or update on intpd_fila_transmissao
for each row

declare

begin
if	(inserting) then
	begin
	gerar_reg_alter_fila(:new.nr_sequencia, :new.nr_seq_evento_sistema);
	end;
end if;

if	(nvl(:old.ie_status,'NULL') <> nvl(:new.ie_status,'NULL')) THEN

  INSERT INTO intpd_historico_status (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_seq_fila,
        ie_status,
        ds_log)
  VALUES (
        intpd_historico_status_seq.nextval,
        SYSDATE,
        :new.nm_usuario,
        SYSDATE,
        :new.nm_usuario,
        :new.nr_sequencia,
        :new.ie_status,
        :new.ds_log);

end if;
end;
/
