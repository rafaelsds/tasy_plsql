create or replace trigger paciente_atendimento_CI
after update on paciente_atendimento
for each row

declare
nr_seq_evento_w			number(10);
qt_idade_w				number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w		varchar2(10);
ds_informacao_w			varchar2(255);
nr_atendimento_w		paciente_atendimento.nr_atendimento%type;

cursor C01 is
	select	nr_seq_evento 
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento	= cd_estabelecimento_w))
	and		ie_evento_disp		= 'ATO'
	and		qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and		nvl(ie_situacao,'A') = 'A';
	
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

begin

nr_atendimento_w := :new.nr_atendimento;

if (nvl(nr_atendimento_w, 0) = 0) then

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	paciente_setor
	where	nr_seq_paciente = :new.nr_seq_paciente;

	select	max(nr_atendimento)
	into 	nr_atendimento_w
	from 	atendimento_paciente
	where 	cd_pessoa_fisica = cd_pessoa_fisica_w;
end if;

if (:old.dt_prevista <> :new.dt_prevista) and
	(nvl(nr_atendimento_w,0) > 0) then
	begin
			
	select	nvl(max(cd_estabelecimento),0),
		max(cd_pessoa_fisica)
	into	cd_estabelecimento_w,
		cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
	
	select	obter_desc_expressao(781783, 'MACRO1' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_prevista, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)|| 'MACRO2' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_prevista, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone))/*'Alterada a data de ' || to_char(:old.dt_prevista,'dd/mm/yyyy hh24:mi:ss') || ' para ' || to_char(:new.dt_prevista,'dd/mm/yyyy hh24:mi:ss')*/
	into	ds_informacao_w
	from	dual;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,:new.nm_usuario,ds_informacao_w);
		end;
	end loop;
	close C01;
	
	end;
end if;

exception
when others then
	null;

end;

<<final>>
null;

end;
/