create or replace trigger perfil_delegacao_before
before insert or update on perfil_delegacao
for each row

begin

    if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'N') = 'S' and
        :new.DT_INICIO_DELEGACAO is not null and :new.DT_FIM_DELEGACAO is not null
        and PKG_DATE_UTILS.get_DiffDate(:new.DT_FIM_DELEGACAO, :new.DT_INICIO_DELEGACAO ,'DAY') > 0) then
            wheb_mensagem_pck.exibir_mensagem_abort (212526);
    end if;
end;
/
