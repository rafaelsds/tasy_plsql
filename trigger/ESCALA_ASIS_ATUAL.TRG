create or replace trigger ESCALA_ASIS_ATUAL
before insert or update on ESCALA_ASIS
for each row

declare

begin
if	(:new.nr_hora is null) or   
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then 
	begin  
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end; 
end if; 



end;
/