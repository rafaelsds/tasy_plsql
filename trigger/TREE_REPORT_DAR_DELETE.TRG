create or replace trigger tree_report_dar_delete
  after delete on relatorio  
  for each row
begin

   if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
   
        delete 
        from tree_report_dar 
        where cd_relatorio = :new.cd_relatorio 
        and cd_classif_relat = :new.cd_classif_relat;
      
   end if;
   
end tree_report_dar_delete;
/
