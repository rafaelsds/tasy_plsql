create or replace trigger mxm_titulo_rec_classif_atual
after insert on titulo_receber_classif
for each row

declare
ie_situacao_w	varchar2(1);
begin
select	ie_situacao
into	ie_situacao_w
from	titulo_receber
where	nr_titulo = :new.nr_titulo;

if	(ie_situacao_w = 'A') then
	mxm_gerar_integracao(:new.nr_titulo,'TR',:new.nm_usuario);
end if;
end;
/