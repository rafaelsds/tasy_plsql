CREATE OR REPLACE TRIGGER pls_estipulante_web_insert
BEFORE INSERT OR UPDATE ON PLS_ESTIPULANTE_WEB
FOR EACH ROW

declare

begin

if	(length(:new.nm_usuario_web) > 15) then
	wheb_mensagem_pck.exibir_mensagem_abort( 244088, null); /* Nome de usu�rio com mais de 15 caracteres! */
end if;

END;
/