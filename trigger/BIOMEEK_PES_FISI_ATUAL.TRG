create or replace trigger BIOMEEK_PES_FISI_ATUAL
after update of	nr_ddd_celular,	 
		nr_telefone_celular  ON PESSOA_FISICA

REFERENCING OLD AS OLD NEW AS NEW

for each row

declare
qt_existe_w		number(10,0);
ds_erro_w		varchar2(255);
cd_usuario_convenio_w	varchar2(30);
dt_atual_w		date := trunc(sysdate);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	select	count(*),
		max(cd_usuario_convenio)
	into	qt_existe_w,
		cd_usuario_convenio_w
	from	pessoa_titular_convenio
	where	cd_pessoa_titular = :new.cd_pessoa_fisica
	and	dt_validade_carteira >= dt_atual_w;

	if	(qt_existe_w > 0) and
		(substr(cd_usuario_convenio_w,1,4) = '0064') then
		begin
		update	usuario_biomeek
		set	uss_ddd_celular		= :new.nr_ddd_celular,
			uss_fone_celular		= :new.nr_telefone_celular
		where   '0064'||rtrim(ltrim(to_char(uss_codigo,'0000000000000')))       = cd_usuario_convenio_w;

		exception
		when others then
			ds_erro_w := sqlerrm(sqlcode);
			insert into log_tasy (	dt_atualizacao,
						nm_usuario,
						cd_log,
						ds_log)
					values(	sysdate,
						:new.nm_usuario,
						19902,
						substr('biomeek_pes_fisi_atual: ' || cd_usuario_convenio_w || ',' ||
						ds_erro_w,1,255));
		end;
	end if;
end if;
end;
/