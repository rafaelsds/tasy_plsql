create or replace trigger REGRA_LEITOR_CART_PAC_BFT
before insert or update on REGRA_LEITOR_CARTAO_PAC
for each row

declare ds_senha_p      varchar2(255) := :new.ds_senha;
        result_duplicate_w    varchar2(2);
begin

	if    (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

        result_duplicate_w := is_duplic_regra_leitor_cart(  :new.nr_sequencia,
                                                            :new.nr_endereco_ip,
                                                            :new.nm_computador,
                                                            :new.ie_tipo_cartao,
                                                            :new.ie_fabricante,
                                                            :new.ie_interface,
                                                            :new.nr_seq_regra_conv);

		IF (:new.ds_senha is not null
		and     nvl(:old.ds_senha, 'null') <> nvl(:new.ds_senha, 'null')
		and     result_duplicate_w = 'N') then

			:new.ds_senha := wheb_seguranca.encrypt(ds_senha_p);
		end if;
	end if;
end;
/