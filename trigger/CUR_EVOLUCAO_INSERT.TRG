CREATE OR REPLACE TRIGGER Cur_Evolucao_Insert
AFTER INSERT ON Cur_Evolucao
FOR EACH ROW 
DECLARE

ie_lacto_w		varchar2(1);

BEGIN

if	(:new.dt_evolucao > sysdate) and
	(:new.nr_atendimento is not null) then
	--A data da evolu��o n�o pode ser maior que a data atual.#@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(262188);
end if;

begin
gerar_lancamento_automatico(:new.nr_atendimento, null, 545, :new.nm_usuario, :new.nr_sequencia, :new.dt_evolucao, :new.ie_evolucao_clinica, null, null, null);
exception
	when others then
	ie_lacto_w:= 'N';
end;


END;
/
