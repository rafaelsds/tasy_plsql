create or replace
trigger pls_regra_div_fat_prot_atual 
before insert or update on pls_regra_divisao_fat_prot
for each row 
begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') then

	:new.dt_inicio_vigencia_ref	:= pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
	:new.dt_fim_vigencia_ref	:= pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');
end if;

end pls_regra_div_fat_prot_atual;
/
