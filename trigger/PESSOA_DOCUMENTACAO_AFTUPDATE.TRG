create or replace trigger PESSOA_DOCUMENTACAO_AFTUPDATE
after update on PESSOA_DOCUMENTACAO
for each row

declare


ie_res_w   				varchar2(1);
cd_pessoa_usuario_w		varchar2(10);
cd_pf_usuario_w			varchar2(10);
ds_param_integ_hl7_w 	varchar2(4000);
cd_estabelecimento_w	number(5);

begin
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	if	(:new.IE_ENTREGUE <> :old.IE_ENTREGUE) and (:new.IE_ENTREGUE = 'S' )then

		
		Select 	nvl(max('S'),'N')
		into	ie_res_w
		from   	conversao_meio_externo b
		where  	nm_tabela = 'PESSOA_DOCUMENTACAO'
		and		nm_atributo = 'IE_DOC_EXTERNO'
		and		b.cd_interno  = :new.NR_SEQ_DOCUMENTO
		and		b.CD_EXTERNO = 'ACTPF'
		and    	b.ie_sistema_externo = 'TERUNIMED16';

		
		if (ie_res_w = 'S') then
		
			Select  max(Obter_Pf_Usuario(:new.nm_usuario, 'C')),
					max(wheb_usuario_pck.get_cd_estabelecimento)
			into	cd_pessoa_usuario_w,
					cd_estabelecimento_w
			from    dual;
		
			if (:new.cd_pessoa_fisica = cd_pessoa_usuario_w) then
		
			
					ds_param_integ_hl7_w := 'cd_transacao='              || '00880'|| ';' ||
											'cd_estabelecimento='        || cd_estabelecimento_w|| ';' ||
											'cd_pf_usuario='             || cd_pessoa_usuario_w      || ';' ;

											
					gravar_agend_integracao(682, ds_param_integ_hl7_w);
					
			end if;
		
		end if;
		
		
	end if;
	
end if;

exception
	when others then
	null;
end;

end;
/