CREATE OR REPLACE TRIGGER obj_sch_ativ_parm_check_attrib
BEFORE INSERT OR UPDATE on obj_schematic_ativ_param
FOR EACH ROW

DECLARE
  wrk_valid NUMBER(1);
BEGIN

  IF :new.ie_origem_param = 'O'  THEN
    BEGIN
      SELECT 1 into wrk_valid 
      FROM TABLE(obter_atributo_param_ativ(:new.NR_SEQ_OBJ_REF,:new.ie_origem_param,:new.nr_seq_objeto))
      WHERE ds_campo_valor = :new.nm_atributo_ref;
    EXCEPTION
      WHEN No_Data_Found THEN
        Wheb_mensagem_pck.Exibir_mensagem_abort(1076273);
     END;
  END IF;
END;
/