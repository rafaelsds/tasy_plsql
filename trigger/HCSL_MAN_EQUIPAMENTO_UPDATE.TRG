CREATE OR REPLACE 
TRIGGER hcsl_man_equipamento_update
BEFORE UPDATE on man_equipamento
for each row
declare

ds_mensagem_w			varchar2(4000);
nr_seq_classif_w		number(10);
ds_setor_new_w			varchar2(100);
ds_setor_old_w			varchar2(100);

BEGIN

if	(:new.nr_seq_local <> :old.nr_seq_local) then
	
	select	substr(obter_nome_setor(cd_setor),1,100)
	into	ds_setor_old_w
	from	man_localizacao
	where	nr_sequencia = :old.nr_seq_local;
	
	select	substr(obter_nome_setor(cd_setor),1,100)
	into	ds_setor_new_w
	from	man_localizacao
	where	nr_sequencia = :new.nr_seq_local;
	
	
	ds_mensagem_w := 	'O usu�rio ' || :new.nm_usuario || ' alterou a localiza��o do equipamento ' || :old.nr_sequencia || ' - ' || :old.ds_equipamento || '.' || chr(13) ||
				'Imobilizado: ' || :old.cd_imobilizado || '.' || chr(13) ||
				'De: ' || substr(obter_desc_man_localizacao(:old.nr_seq_local),1,80) || '/' || ds_setor_old_w || chr(13) ||
				'Para: '  || substr(obter_desc_man_localizacao(:new.nr_seq_local),1,80) || '/' || ds_setor_new_w || '.';

	select	obter_classif_comunic('F')
	into	nr_seq_classif_w
	from	dual;
			
	gerar_comunic_padrao(
		sysdate,
		'Altera��o na localiza��o do equipamento.',
		ds_mensagem_w,
		:new.nm_usuario,
		'N',
		'',
		'N',
		nr_seq_classif_w,
		2013,
		null,
		'',
		sysdate,
		'',
		'');	
end if;

END;
/