create or replace trigger medico_after_update
after update on medico
for each row

declare

reg_integracao_p	gerar_int_padrao.reg_integracao;
ds_param_integ_hl7_w			varchar2(4000);
ds_sep_bv_w				varchar2(100);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	ds_sep_bv_w := obter_separador_bv;
	send_physician_intpd(:new.cd_pessoa_fisica, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'U');
	ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w||
							'ie_acao='||'MUP'||ds_sep_bv_w;	
	gravar_agend_integracao(853, ds_param_integ_hl7_w); /* Tasy -> SCC (MFN_M02) - Doctor registration */
	end;
end if;
end;
/