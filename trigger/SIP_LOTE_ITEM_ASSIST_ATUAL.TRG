create or replace trigger sip_lote_item_assist_atual
before update or delete on sip_lote_item_assistencial
for each row

declare
ie_grava_log	varchar2(1);

begin

ie_grava_log := 'N';

-- se for a��o de delete ou se algum valor dos campos pertinentes a altera��o for modificado
if	(deleting) then
	ie_grava_log := 'S';

elsif	((nvl(:new.cd_classificacao_sip, 'A') != nvl(:old.cd_classificacao_sip, 'A')) or
	 (nvl(:new.dt_ocorrencia, sysdate) != nvl(:old.dt_ocorrencia, sysdate)) or
	 (nvl(:new.ie_benef_carencia, 'A') != nvl(:old.ie_benef_carencia, 'A')) or
	 (nvl(:new.ie_despesa, 'A') != nvl(:old.ie_despesa, 'A')) or
	 (nvl(:new.ie_evento, 'A') != nvl(:old.ie_evento, 'A')) or
	 (nvl(:new.ie_segmentacao_sip, 0) != nvl(:old.ie_segmentacao_sip, 0)) or
	 (nvl(:new.ie_tipo_contratacao, 'A') != nvl(:old.ie_tipo_contratacao, 'A')) or
	 (nvl(:new.nr_seq_apres, 0) != nvl(:old.nr_seq_apres, 0)) or
	 (nvl(:new.nr_seq_item_sip, 0) != nvl(:old.nr_seq_item_sip, 0)) or
	 (nvl(:new.nr_seq_lote, 0) != nvl(:old.nr_seq_lote, 0)) or
	 (nvl(:new.nr_seq_superior, 0) != nvl(:old.nr_seq_superior, 0)) or
	 (nvl(:new.qt_beneficiario, 0) != nvl(:old.qt_beneficiario, 0)) or
	 (nvl(:new.qt_evento, 0) != nvl(:old.qt_evento, 0)) or
	 (nvl(:new.sg_uf, 'A') != nvl(:old.sg_uf, 'A')) or
	 (nvl(:new.vl_despesa, 0) != nvl(:old.vl_despesa, 0))) then
	ie_grava_log := 'S';
end if;

-- grava o log               
if	(ie_grava_log = 'S') then

	insert into sip_lote_item_assist_log 	
		( 	nr_sequencia, dt_atualizacao_nrec, nm_usuario_nrec,
			cd_classificacao_sip, dt_atualizacao_log, dt_ocorrencia,
			ie_benef_carencia, ie_despesa, ie_evento,
			ie_segmentacao_sip, ie_tipo_contratacao, nm_usuario_log,
			nr_seq_apres, nr_seq_item_sip, nr_seq_lote,
			nr_seq_superior, nr_sequencia_log, qt_beneficiario,
			qt_evento, sg_uf, vl_despesa)
	values	( 	sip_lote_item_assist_log_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado'),1,14),
			:old.cd_classificacao_sip, :old.dt_atualizacao, :old.dt_ocorrencia,
			:old.ie_benef_carencia, :old.ie_despesa, :old.ie_evento,
			:old.ie_segmentacao_sip, :old.ie_tipo_contratacao, :old.nm_usuario,
			:old.nr_seq_apres, :old.nr_seq_item_sip, :old.nr_seq_lote,
			:old.nr_seq_superior, :old.nr_sequencia, :old.qt_beneficiario,
			:old.qt_evento, :old.sg_uf, :old.vl_despesa);
end if;                 

end sip_lote_item_assist_atual;
/