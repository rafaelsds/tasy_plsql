create or replace trigger sip_item_assist_regra_nv_bi
before update on sip_item_assist_regra_nv
for each row
declare
begin

:new.ds_controle_regra_duplic :=	substr('cd_area_procedimento='||:new.cd_area_procedimento||';'||
					'cd_categoria_cid='||:new.cd_categoria_cid||';'||
					'cd_cbo='||:new.cd_cbo||';'||
					'cd_doenca_cid='||:new.cd_doenca_cid||';'||
					'cd_especialidade='||:new.cd_especialidade||';'||
					'cd_grupo_proc='||:new.cd_grupo_proc||';'||
					'cd_procedimento='||:new.cd_procedimento||';'||
					'ie_considerar_conta='||:new.ie_considerar_conta||';'||
					'ie_considerar_guia_ref='||:new.ie_considerar_guia_ref||';'||
					'ie_materiais='||:new.ie_materiais||';'||
					'ie_nascido_vivo='||:new.ie_nascido_vivo||';'||
					'ie_origem_proced='||:new.ie_origem_proced||';'||
					'ie_parto_normal='||:new.ie_parto_normal||';'||
					'ie_regime_internacao='||:new.ie_regime_internacao||';'||
					'ie_sexo='||:new.ie_sexo||';'||
					'ie_tipo_guia='||:new.ie_tipo_guia||';'||
					'ie_unid_tempo_idade='||:new.ie_unid_tempo_idade||';'||
					'nr_seq_cbo_saude='||:new.nr_seq_cbo_saude||';'||
					'nr_seq_clinica='||:new.nr_seq_clinica||';'||
					'nr_seq_grupo_servico='||:new.nr_seq_grupo_servico||';'||
					'nr_seq_prestador_exec='||:new.nr_seq_prestador_exec||';'||
					'nr_seq_tipo_atendimento='||:new.nr_seq_tipo_atendimento||';'||
					'qt_idade_final='||:new.qt_idade_final||';'||
					'qt_idade_inicial='||:new.qt_idade_inicial||';',1,4000);
end;
/