create or replace trigger proj_projeto_atual
before insert or update on proj_projeto
for each row

declare

nr_seq_ordem_serv_pai_w		man_ordem_servico.nr_seq_ordem_serv_pai%type;

begin

select	max(mos.nr_seq_ordem_serv_pai)
into	nr_seq_ordem_serv_pai_w
from	man_ordem_servico mos
where	mos.nr_sequencia = :new.nr_seq_ordem_serv;

if	(nr_seq_ordem_serv_pai_w is not null) then
	begin
		wheb_mensagem_pck.exibir_mensagem_abort(1085000);
	end;
end if;

if	(:new.nr_seq_estagio is not null) and
	(:new.nr_seq_estagio <> nvl(:old.nr_seq_estagio,0)) then
	insert into proj_estagio_hist
		(nr_sequencia, nr_seq_proj, nr_seq_estagio,
		dt_historico, dt_atualizacao, nm_usuario,
		ie_status)
	values(	proj_estagio_hist_seq.nextval, :new.nr_sequencia, :new.nr_seq_estagio,
		sysdate, sysdate, :new.nm_usuario,
		:new.ie_status);
end if;

end;
/
