create or replace trigger pls_alt_regra_preco_ordem_serv
before update or insert or delete on pls_regra_preco_ordem_serv
for each row

declare
qt_registro_w	pls_integer;
begin
-- esta trigger tem apenas a finalidade de especificar na tabela pls_regra_preco_controle se as views de pre�o precisam ser geradas novamente
select	count(1)
into	qt_registro_w
from	pls_regra_preco_controle;

if	(qt_registro_w > 0) then
	update	pls_regra_preco_controle set
		ie_gera_novamente = 'S';
else
	insert into pls_regra_preco_controle (ie_gera_novamente) values ('S');
end if;

end;
/