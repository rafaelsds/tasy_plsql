create or replace trigger pls_segurado_doc_arq_atual
before insert or update on pls_segurado_doc_arq
for each row

declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo
:new.dt_ini_apresenta_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_ini_apresenta, 'i');
:new.dt_fin_apresenta_ref    := pls_util_pck.obter_dt_vigencia_null( :new.dt_fin_apresenta, 'f');

end pls_segurado_doc_arq_atual;
/
