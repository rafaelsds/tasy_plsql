create or replace trigger compl_pf_tel_adic_pls_rps
after update on compl_pf_tel_adic
for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 
declare
ie_alterou_w 		varchar2(1) := 'N';
cd_cgc_cpf_old_w	pls_alt_prest_rps.cd_cgc_cpf_old%type;

begin

begin
	if	(nvl(:new.cd_municipio_ibge,'0')!= nvl(:old.cd_municipio_ibge,'0')) or
		(nvl(:new.sg_estado,'0')	!= nvl(:old.sg_estado,'0')) or
		(nvl(:new.ds_bairro,'0')	!= nvl(:old.ds_bairro,'0')) or
		(nvl(:new.ds_endereco,'0')	!= nvl(:old.ds_endereco,'0')) or
		(nvl(:new.ds_municipio,'0')	!= nvl(:old.ds_municipio,'0')) or
		(nvl(:new.nr_endereco,0)	!= nvl(:old.nr_endereco,0)) or
		(nvl(:new.cd_cep,'0')		!= nvl(:old.cd_cep,'0')) or
		(nvl(:new.nr_seq_ident_cnes,0)	!= nvl(:old.nr_seq_ident_cnes,0))then
		ie_alterou_w := 'S';
	end if;

	if	(ie_alterou_w = 'S') then
		select	max(nr_cpf)
		into	cd_cgc_cpf_old_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica;
	
		pls_gerar_alt_prest_rps(null, :new.cd_pessoa_fisica, null, cd_cgc_cpf_old_w, :old.cd_municipio_ibge, null, :new.nm_usuario, :new.nr_sequencia, null, null);
	end if;
exception
when others then
	null;
end; 

end;
/