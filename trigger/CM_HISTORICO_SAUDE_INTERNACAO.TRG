create or replace trigger cm_historico_saude_internacao
before insert or update on historico_saude_internacao
for each row 

declare
cd_expression	dic_objeto.nr_sequencia%type := 0;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	if 	(:new.dt_internacao is not null) and
		(pkg_date_utils.start_of(:new.dt_internacao, 'DAY') > sysdate) then
		begin
		cd_expression := 1046797;
		end;
	end if;
	
	if	(cd_expression > 0) then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(cd_expression);
		end;
	end if;
end if;
end;
/