create or replace trigger atend_referral_doc
after insert on atendimento_paciente_anexo
for each row 
declare
begin

	if	((nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') and 
		((:new.nr_sequencia <> :old.nr_sequencia) or 
			:old.nr_sequencia is null)) then
		gerar_paciente_ext_ref( :new.nr_atendimento, :new.nr_sequencia);
	end if;
	
exception when others then
	null;
end;
/
