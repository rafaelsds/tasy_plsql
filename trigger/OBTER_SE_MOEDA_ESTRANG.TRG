create or replace
function obter_se_moeda_estrang (cd_moeda_estrang_p	number,
				cd_moeda_empresa_p	number)
				return varchar2 is

ds_retorno_w	varchar2(1);

begin

if ((cd_moeda_estrang_p is not null) and (cd_moeda_estrang_p <> cd_moeda_empresa_p)) then
	ds_retorno_w := 'S';
else
	ds_retorno_w := 'N';
end if;

return ds_retorno_w;

end obter_se_moeda_estrang;
/