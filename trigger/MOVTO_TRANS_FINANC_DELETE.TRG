CREATE OR REPLACE TRIGGER Movto_Trans_Financ_Delete
before delete ON Movto_Trans_Financ
FOR EACH ROW

declare

nr_seq_saldo_banco_w		Number(10,0);
ie_acao_w			Number(5,0);
ds_historico_w			varchar2(4000);
/* Projeto Multimoeda - Variaveis */
vl_transacao_estrang_w		number(15,2);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
begin

if	(:old.dt_fechamento_lote is not null) then
	/* O lote de caixa ja foi fechado, o lancamento nao pode ser excluido!
	Lote: :old.nr_seq_lote
	Seq: :old.nr_sequencia
	Dt fechamento: :old.dt_fechamento_lote */
	wheb_mensagem_pck.exibir_mensagem_abort(244040, 'NR_SEQ_LOTE_W=' || :old.nr_seq_lote ||
							';NR_SEQUENCIA_W=' || :old.nr_sequencia ||
							';DT_FECHAMENTO_LOTE_W=' || :old.dt_fechamento_lote);
end if;

if	(:old.nr_lote_contabil <> 0) then
	/* Este lancamento ja esta contablizado, nao pode ser excluido! */
	wheb_mensagem_pck.exibir_mensagem_abort(244042);
end if;

if	(:old.nr_seq_banco	is not null) then
	begin

	select	ie_acao
	into	ie_acao_w
	from	transacao_financeira
	where	nr_sequencia = :old.nr_seq_trans_financ;

	if	(ie_acao_w <> 0) then
		/* A acao desta transacao nao permite que o lancamento seja excluido! */
		wheb_mensagem_pck.exibir_mensagem_abort(244043);
	end if;

	/* Projeto Multimoeda - Verifica se a transacao e moeda estrangeira */
	if (nvl(:old.vl_transacao_estrang,0) <> 0) then
		vl_transacao_estrang_w := :old.vl_transacao_estrang * -1;
	else
		vl_transacao_estrang_w := null;
	end if;
	
	Atualizar_Saldo_Movto_Bco(	:old.nr_sequencia,
					:old.nm_usuario,
					(:old.vl_transacao * -1),
					:old.nr_seq_banco,
					:old.dt_transacao,
					:old.nr_seq_trans_financ,
					nr_seq_saldo_banco_w,
					vl_transacao_estrang_w);
	end;
end if;

/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_sequencia,null,'CBT',:old.dt_transacao,'E',:old.nm_usuario);
if (:old.nr_seq_banco is not null) then
	gravar_agend_fluxo_caixa(:old.nr_sequencia,:old.nr_seq_banco,'CBS',:old.dt_transacao,'E',:old.nm_usuario);
end if;

exception
when	others then
	begin

	ds_historico_w	:=	substr(wheb_mensagem_pck.get_texto(304242),1,255) || ': ' || trim(to_char(:old.vl_transacao,'999999990.00')) || chr(13) || chr(10) ||
				substr(wheb_mensagem_pck.get_texto(304243),1,255) || ': ' || :old.nr_seq_trans_financ || chr(13) || chr(10) ||
				substr(wheb_mensagem_pck.get_texto(304244),1,255) || ': ' || :old.nr_sequencia;

	ds_historico_w	:= substr(sqlerrm,1,2000) || chr(13) || chr(10) || ds_historico_w;

	gerar_banco_caixa_hist(	:old.nr_seq_banco,
				:old.nr_seq_caixa,
				ds_historico_w,
				:old.nm_usuario,
				'S',
				'S');

	/* ds_historico_w */
	wheb_mensagem_pck.exibir_mensagem_abort(244091,'DS_HISTORICO_W=' || ds_historico_w);

	end;

end;
end if;

END;
/
