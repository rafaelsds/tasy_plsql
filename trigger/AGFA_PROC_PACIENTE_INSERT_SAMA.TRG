create or replace trigger AGFA_Proc_Paciente_Insert
after Insert on procedimento_paciente
for each row

declare

cd_pessoa_fisica_w 	varchar2(10);
ie_tipo_atendimento_w	varchar2(2);
dt_prev_execucao_w	date;
ds_proc_exame_w 	varchar2(100);
nr_seq_classif_w	number(10);
ie_retorno_w 		varchar(256);
--ie_retorno_w 		Boolean;

begin

if	(:new.qt_procedimento > 0) and
	(:new.nr_seq_proc_interno is not null) and
	((:new.nr_prescricao is null) or (:new.nr_sequencia_prescricao is null)) then

	select	b.cd_pessoa_fisica,
		decode(b.ie_tipo_atendimento, 1, 'H1', 3, 'A2', 8, 'AA', 'AE')
	into	cd_pessoa_fisica_w,
		ie_tipo_atendimento_w
	from	atendimento_paciente b
	where b.nr_atendimento = :new.nr_atendimento;

	if	(ie_tipo_atendimento_w <> 'AE') then
		dt_prev_execucao_w := :new.dt_procedimento;

		select	ds_proc_exame,
			nvl(nr_seq_classif,0)
		into	ds_proc_exame_w,
			nr_seq_classif_w
		from proc_interno
		where nr_sequencia = :new.nr_seq_proc_interno;

		if	(nr_seq_classif_w = 5) then
			AGFA_Gera_Solicitacao(	to_char(:new.nr_sequencia),	'0^REQUESTED'     , 		'N',
						dt_prev_execucao_w, 		dt_prev_execucao_w, 		'IMGHS',
						:new.nm_usuario, 		:new.cd_medico_executor,  	cd_pessoa_fisica_w,
						:new.nr_atendimento, 		ie_tipo_atendimento_w,  	to_char(:new.nr_seq_proc_interno),
						ds_proc_exame_w,		to_char(:new.nr_sequencia), 	null,
		    				null, 				null, 				null,
						null,				ie_retorno_w);
		end if;

		if	(ie_retorno_w is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort('Retorno AGFA:' || ie_retorno_w);
		end if;
	end if;
end if;

end AGFA_Proc_Paciente_Insert;
/
