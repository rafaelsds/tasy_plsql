create or replace trigger escala_man_lf_atual
before insert or update on escala_man_lf
for each row
declare
  peso_parte_w   number(10, 3);
  qt_imc_w       number(4, 1);
  qt_pontuacao_w number(5, 2);
  sql_imc_w      varchar2(200);
  sql_man_w      varchar2(200);
  ds_erro_w      varchar2(4000);
  ds_parametro_w varchar2(4000);
begin

	if (wheb_usuario_pck.get_ie_executar_trigger	= 'S') then   
	  /** Medical Device **/
	  begin
	    peso_parte_w := null;
	    sql_imc_w := 'call obter_valor_imc_md(:1, :2, :3) into :qt_imc_w';
	    execute immediate sql_imc_w using 
        in :new.qt_peso,
        in peso_parte_w,
        in :new.qt_altura,
        out qt_imc_w;
	  exception
	    when others then
	      qt_imc_w := null;
        ds_erro_w := sqlerrm;
        ds_parametro_w := ':new.nr_atendimento: ' || :new.nr_atendimento || ' - :new.ie_situacao: ' || :new.ie_situacao || ' - :new.cd_profissional: ' || :new.cd_profissional;
        ds_parametro_w := ds_parametro_w || ' - :new.qt_peso: ' || :new.qt_peso || ' - peso_parte_w: ' || peso_parte_w || ' - :new.qt_altura: ' || :new.qt_altura || ' - qt_imc_w: ' || qt_imc_w;
        gravar_log_medical_device('ESCALA_MAN_LF_ATUAL', 'OBTER_VALOR_IMC_MD', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');
	  end;                                 
	  :new.qt_imc := qt_imc_w;
	  
	  begin  
		sql_man_w := 'call obter_score_escala_man_md(:1, :2, :3, :4, :5, :6, :7) into :qt_pontuacao_w';  
		execute immediate sql_man_w using in qt_imc_w,
										  in :new.qt_circun_panturrilha,
										  in :new.ie_estresse,
										  in :new.ie_diminuicao_alimentar,
										  in :new.ie_perda_peso,
										  in :new.ie_mobilidade,
										  in :new.ie_neuropsicologicos,
                      out qt_pontuacao_w;
	  exception
		when others then
		  qt_pontuacao_w := null;
      ds_erro_w := sqlerrm;
      ds_parametro_w := ':new.nr_atendimento: ' || :new.nr_atendimento || ' - :new.ie_situacao: ' || :new.ie_situacao || ' - :new.cd_profissional: ' || :new.cd_profissional;
      ds_parametro_w := ds_parametro_w || ' - qt_imc_w: ' || qt_imc_w || ' - :new.qt_circun_panturrilha: ' || :new.qt_circun_panturrilha || ' - :new.ie_estresse: ' || :new.ie_estresse;
      ds_parametro_w := ds_parametro_w || ' - :new.ie_diminuicao_alimentar: ' || :new.ie_diminuicao_alimentar || ' - :new.ie_perda_peso: ' || :new.ie_perda_peso || ' - :new.ie_mobilidade: ' || :new.ie_mobilidade || ' - :new.ie_neuropsicologicos: ' || :new.ie_neuropsicologicos;
      ds_parametro_w := ds_parametro_w || ' - qt_pontuacao_w: ' || qt_pontuacao_w;
      gravar_log_medical_device('ESCALA_MAN_LF_ATUAL', 'OBTER_SCORE_ESCALA_MAN_MD', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');
	  end;  
	  :new.qt_pontuacao	:= qt_pontuacao_w;
  end if;
end;
/