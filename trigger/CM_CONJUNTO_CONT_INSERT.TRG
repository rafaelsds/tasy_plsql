create or replace trigger cm_conjunto_cont_insert
after insert on cm_conjunto_cont
for each row
declare
nr_seq_conj_movto_w		number(10);
qt_usuario_preparo_w	        cm_ciclo.nm_usuario_preparo%type;

begin

select	cm_conjunto_movto_seq.nextval
into	nr_seq_conj_movto_w
from	dual;

if	(nvl(nr_seq_conj_movto_w,0) > 0) then
	begin

	insert into cm_conjunto_movto(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_conjunto,
		cd_estab_origem,
		cd_estab_atual) values (
			nr_seq_conj_movto_w,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			:new.cd_estabelecimento,
			:new.cd_estabelecimento);

	end;	
end if;


select 	max(nm_usuario_preparo)
into	qt_usuario_preparo_w
from 	cm_ciclo 
where 	nr_sequencia = :new.nr_seq_ciclo;

if (nvl(qt_usuario_preparo_w,'X') = 'X') then

	update 	cm_ciclo
	set 	nm_usuario_preparo = :new.nm_usuario
	where 	nr_sequencia = :new.nr_seq_ciclo;
	
end if;

end;
/
