CREATE OR REPLACE TRIGGER Pessoa_Fisica_AfterInsert
AFTER INSERT ON Pessoa_Fisica
FOR EACH ROW 

DECLARE

cd_setor_atendimento_w	Number(5);
ie_setor_origem_w	varchar2(1); --Ivan em 10/08/2007 OS60862
nr_seq_set_origem_w	number(10);  --Ivan em 10/08/2007 OS60862
ie_regra_pront_w	varchar2(15);
sg_estado_w		compl_pessoa_fisica.sg_estado%type;

begin

if	(:new.nr_prontuario is not null) then
	
	select	nvl(max(vl_parametro),'BASE')
	into	ie_regra_pront_w
	from	funcao_parametro
	where	cd_funcao	= 0
	and	nr_sequencia	= 120;

	if	(ie_regra_pront_w <> 'ESTAB') then 
		gerar_same_cpi_prontuario(	:new.nr_prontuario, :new.cd_pessoa_fisica, :new.cd_estabelecimento,:new.nm_usuario);
	end if;
end if;

gerar_regra_prontuario_gestao(null, :new.cd_estabelecimento, null, :new.cd_pessoa_fisica, :new.nm_usuario, null, null, null, null, null, null, null);

/* In�cio - Ivan em 10/08/2007 OS60862 */
Obter_Param_Usuario(32,19,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_setor_origem_w);

if	(ie_setor_origem_w = 'S') then
	begin

	select	nvl(max(cd_setor_atendimento),0)
	into	cd_setor_atendimento_w
	from	usuario
	where	nm_usuario	= :new.nm_usuario;

	if	(cd_setor_atendimento_w > 0) then
		begin

		select	pf_setor_prontuario_seq.nextval
		into	nr_seq_set_origem_w
		from	dual;

		insert into pf_setor_prontuario (
			nr_sequencia,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			cd_setor_atendimento,
			dt_inicio_cinculo)
		values (	
			nr_seq_set_origem_w,
			sysdate,
			sysdate,
			:new.nm_usuario,
			:new.nm_usuario,
			:new.cd_pessoa_fisica,
			cd_setor_atendimento_w,
			sysdate
		);

		end;
	end if;

	end;
end if;

/* Projeto MXM (7077)  - Exportar cadastro pessoa fisica */
select 	max(sg_estado)
into	sg_estado_w
from 	compl_pessoa_fisica
where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
and 	nvl(ie_tipo_complemento,1) = 1;
if (:new.nr_cpf is not null and sg_estado_w is not null) then
	gravar_agend_integracao(556,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';CD_PESSOA_JURIDICA='||null||';'); --Fornecedor
	gravar_agend_integracao(562,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';'); --Cliente
end if;
/* T�rmino - Ivan em 10/08/2007 OS60862 */

recent_patients_found_handler(null, :new.cd_pessoa_fisica, :new.nm_usuario, 'S');

end;
/