CREATE OR REPLACE TRIGGER Agenda_Paciente_After_Insert
AFTER INSERT OR UPDATE ON Agenda_Paciente
FOR EACH ROW
DECLARE

cd_tipo_agenda_w		number(10,0);
cd_estabelecimento_w		number(5);
ie_prontuario_w			varchar2(1);
ie_gerar_w			varchar2(1);
nr_prontuario_w			number(10,0);
cd_agenda_exame_w		number(10,0);
qt_aval_lista_espera		number(5);
nr_req_cme_w			number(10);
nr_seq_conj_real_w		number(10);
qt_sur_schedule_rule_w	number(3);
ds_retorno_integracao_w 	varchar2(4000);
ds_hist_w		varchar2(2000);
cd_funcao_ativa_w	number(10);

cursor c01 is
	select	cd_agenda_exame
	from	regra_prontuario
	where	cd_estabelecimento						= cd_estabelecimento_w
	and	ie_tipo_regra							= 6;

cursor c02 is
	select	nr_seq_conj_real
	from	cm_requisicao_item a,
		cm_requisicao_conj b
	where	a.nr_seq_requisicao = nr_req_cme_w
	and	a.nr_sequencia = b.nr_seq_item_req;
	
BEGIN

cd_funcao_ativa_w := obter_funcao_ativa;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	select	max(cd_tipo_agenda),
			max(cd_estabelecimento)
	into	cd_tipo_agenda_w,
			cd_estabelecimento_w
	from	agenda
	where 	cd_agenda	= :new.cd_agenda;

	if ((cd_tipo_agenda_w = 1) and
		(:new.cd_pessoa_fisica is not null) and	
		(:old.cd_pessoa_fisica is null)) then
		
		gerar_regra_prontuario_gestao(null, cd_estabelecimento_w, null, :new.cd_pessoa_fisica, :new.nm_usuario,:new.nr_sequencia, null, null, null, null, null, :new.cd_agenda);
		nr_prontuario_w	:= nvl(obter_prontuario_pf(cd_estabelecimento_w, :new.cd_pessoa_fisica),0);
		
		if 	(nr_prontuario_w = 0) then
			select	obter_valor_param_usuario(0,32,0,:new.nm_usuario,cd_estabelecimento_w)
			into	ie_prontuario_w
			from	dual;

			if	(ie_prontuario_w = 'R') then
				select	count(1)
				into 	qt_sur_schedule_rule_w
				from	regra_prontuario
				where	cd_estabelecimento 	= cd_estabelecimento_w
				and		ie_tipo_regra 		= 7;
				
				if (qt_sur_schedule_rule_w > 0) then
					gerar_prontuario_pac(cd_estabelecimento_w, :new.cd_pessoa_fisica, 'N', :new.nm_usuario, nr_prontuario_w);
				end if;
			end if;
		end if;
	elsif ((cd_tipo_agenda_w = 2) and
		(:new.cd_pessoa_fisica is not null) and	
		(:old.cd_pessoa_fisica is null)) then
		gerar_regra_prontuario_gestao(null, cd_estabelecimento_w, null, :new.cd_pessoa_fisica, :new.nm_usuario,:new.nr_sequencia, null, null, null, null, null, :new.cd_agenda);
	
		/* Matheus OS 182242
		select 		nvl(max(nr_prontuario),0)
		into		nr_prontuario_w
		from		Pessoa_fisica
		where		cd_pessoa_fisica	= :new.cd_pessoa_fisica;*/
		
		nr_prontuario_w	:= nvl(obter_prontuario_pf(cd_estabelecimento_w, :new.cd_pessoa_fisica),0);
		
		if 	(nr_prontuario_w = 0) then
		
			SELECT	Obter_Valor_Param_Usuario(0,32,0,:NEW.nm_usuario,cd_estabelecimento_w)
			into	ie_prontuario_w
			FROM	dual;
			
			if	(ie_prontuario_w = 'R') then
				ie_gerar_w := 'N';
				OPEN C01;
				LOOP
				FETCH C01 into cd_agenda_exame_w;
				exit when c01%notfound;
					begin
					if	(cd_agenda_exame_w is null) or
						((cd_agenda_exame_w is not null) and
						(cd_agenda_exame_w = :new.cd_agenda)) then
						ie_gerar_w 	:= 'S';
						exit;
					end if;
					end;
				END LOOP;
				CLOSE C01;	
			end if;
			if	(ie_gerar_w = 'S') then
				/* Matheus OS 182242
				select	prontuario_seq.nextval
				into		nr_prontuario_w
				from		dual;

				update 	pessoa_fisica
				set		nr_prontuario	= nr_prontuario_w
				where		cd_pessoa_fisica	= :new.cd_pessoa_fisica;*/
				gerar_prontuario_pac(cd_estabelecimento_w, :new.cd_pessoa_fisica, 'N', :new.nm_usuario, nr_prontuario_w);
			end if;
		end if;
	end if;
	
	
	if (:new.nr_seq_lista is not null) then
	
		select count(*)
		into   qt_aval_lista_espera 
		from   aval_pre_anestesica
		where  nr_seq_lista_espera = :new.nr_seq_lista;
		
		if (qt_aval_lista_espera > 0) then
	
			update  aval_pre_anestesica
			set 	nr_seq_agenda =	:new.nr_sequencia
			where 	nr_seq_lista_espera = :new.nr_seq_lista;
		end if;
	end if;

	if	(updating) and
		(:new.nr_cirurgia is not null)	then
		
		select	substr(obter_requisicao_agenda(:new.nr_sequencia),1,90)
		into	nr_req_cme_w
		from	dual;
		
		if	(nvl(nr_req_cme_w,0) > 0) then
			open c02;
			loop
			fetch c02 into	
				nr_seq_conj_real_w;
			exit when c02%notfound;
				begin
				update	cm_conjunto_cont
				set	nr_cirurgia = :new.nr_cirurgia
				where	nr_sequencia = nr_seq_conj_real_w;
				end;
			end loop;
			close c02;
		end if;

    if (cd_tipo_agenda_w = 1) and (:new.nr_atendimento is not null) and (:new.ie_status_agenda <> 'C') then
         select   BIFROST.SEND_INTEGRATION( 'SurgerySchedulingUpdate',
          'com.philips.tasy.integration.atepac.surgery.scheduling.SurgerySchedulingRequest',
          '{"nrSequencia" : '||:new.nr_sequencia||'}',
          wheb_usuario_pck.get_nm_usuario)
          into     ds_retorno_integracao_w
          from     dual;
    end if;
	end if;
	
	if(inserting and	
		(:new.cd_pessoa_fisica is not null 
	or (:new.nm_paciente is not null and :new.ie_status_agenda <> 'B'))
		and :new.cd_agenda is not null
		and cd_tipo_agenda_w = 2
		and :new.ie_encaixe = 'S') then
		
			ds_hist_w := substr(WHEB_MENSAGEM_PCK.get_texto(1071609, 'ds_funcao='||OBTER_DESC_FUNCAO(obter_funcao_ativa)||' - '||WHEB_MENSAGEM_PCK.get_texto(42957)||';obter_funcao_ativa='||obter_funcao_ativa||';call_stack='||dbms_utility.format_call_stack),1,2000);
		
			gerar_agenda_paciente_hist(:new.cd_agenda,
									  :new.nr_sequencia,
									  'AGM',
									  :new.nm_usuario,
									  ds_hist_w,
									  :new.cd_pessoa_fisica,
									  :new.nm_paciente,
									  :new.hr_inicio,
									  obter_perfil_ativo,
									  null,
									  null,
									  :new.dt_agendamento);
	end if;
	
end if;

/*desbloquear agendamentos bloqueados para edicao*/
if (nvl(obter_tipo_agenda(:new.cd_agenda),1) = 1) then
	bloquear_agenda_edicao (:new.nr_sequencia, 'T', wheb_usuario_pck.get_nm_usuario);
end if;

END;
/
