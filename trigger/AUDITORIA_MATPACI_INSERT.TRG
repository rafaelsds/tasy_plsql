create or replace trigger AUDITORIA_MATPACI_INSERT
before insert on AUDITORIA_MATPACI
for each row

declare

nr_seq_pend_w		Number(10,0);
nr_seq_estagio_pend_w	Number(10,0);
vl_item_w		number(15,2);
nr_interno_conta_w	number(10,0);
nr_atendimento_w	number(10,0);
nr_conta_audit_w	number(10,0);
ds_call_stack_w		varchar2(2000);
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;
dt_conta_w		date;
dt_atendimento_w	date;
qt_item_periodo_w	number(10,0);

begin

select 	max(nr_sequencia)
into	nr_seq_pend_w
from 	cta_pendencia
where 	nr_seq_auditoria = :new.nr_seq_auditoria;

if	(nvl(nr_seq_pend_w,0) > 0) then
	
	select 	max(nr_seq_estagio)
	into	nr_seq_estagio_pend_w
	from  	cta_pendencia
	where 	nr_sequencia = nr_seq_pend_w;
	
	if	(nvl(nr_seq_estagio_pend_w,0) > 0) then
	
		:new.nr_seq_estagio_pend:= nr_seq_estagio_pend_w;
		
	end if;	
end if;

begin
select	nvl(a.vl_material,0)	
into	vl_item_w
from	material_atend_paciente a
where	a.nr_sequencia		= :new.nr_seq_matpaci
and	a.cd_motivo_exc_conta is null
and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia;
exception
	when others then
	:new.vl_total_ajuste:= 0;
	vl_item_w:= 0;
end;

:new.vl_total_ajuste:= vl_item_w;

:new.ie_exige_auditoria := obter_se_exige_auditoria(:new.nr_seq_matpaci, 1);

select	max(nr_interno_conta),
	max(nr_atendimento),
	max(dt_conta),
	max(dt_atendimento)
into	nr_interno_conta_w,
	nr_atendimento_w,
	dt_conta_w,
	dt_atendimento_w
from	material_atend_paciente
where	nr_sequencia = :new.nr_seq_matpaci;

select	max(nr_interno_conta),
	max(dt_periodo_inicial),
	max(dt_periodo_final)
into	nr_conta_audit_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w
from	auditoria_conta_paciente
where	nr_sequencia = :new.nr_seq_auditoria;

qt_item_periodo_w	:= 1;
begin
select	nvl(max(1),0)
into	qt_item_periodo_w
from	dual
where	((dt_conta_w between dt_periodo_inicial_w and dt_periodo_final_w) or
	 (dt_atendimento_w between dt_periodo_inicial_w and dt_periodo_final_w));
exception
	when others then
	qt_item_periodo_w	:= 1;
end;

if	((nvl(nr_interno_conta_w,0) <> 0) and
	 (nvl(nr_conta_audit_w,0) <> 0) and
	 (nvl(nr_interno_conta_w,0) <> nvl(nr_conta_audit_w,0))) or
	(qt_item_periodo_w = 0) then

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);
	
	insert into w_log_atend_audit (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_audit_item,
		nr_seq_auditoria,
		ds_call_stack,
		ie_tipo_item,
		nr_seq_item,
		dt_periodo_inicial,
		dt_periodo_final,
		ds_observacao
	) values (
		w_log_atend_audit_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		:new.nr_seq_auditoria,
		ds_call_stack_w,
		'M',
		:new.nr_seq_matpaci,
		dt_periodo_inicial_w,
		dt_periodo_final_w,
		decode(qt_item_periodo_w, 0, substr(wheb_mensagem_pck.get_texto(309510),1,255), substr(wheb_mensagem_pck.get_texto(309511),1,255))
	);
	
end if;

end;
/
