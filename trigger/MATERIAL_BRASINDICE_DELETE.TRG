create or replace trigger material_brasindice_delete
before delete on material_brasindice
for each row

declare

qt_existe_regra_w		varchar2(1);
ds_erro_w			varchar2(255) := '0';

begin

select	nvl(max('S'),'N')
into	qt_existe_regra_w
from	conv_regra_vinc_mat;

if	(qt_existe_regra_w = 'S') then

	obter_parametro_vinculo_mat(:old.cd_material, wheb_usuario_pck.get_nm_usuario,	ds_erro_w, 'B', 3);
	
	if	(ds_erro_w <> '0') then	
		--R.aise_application_error(-20011,ds_erro_w);
		wheb_mensagem_pck.exibir_mensagem_abort(263411,'ds_erro_w='||ds_erro_w);
		
	end if;
	
end if;

end;
/ 
