CREATE OR REPLACE TRIGGER block_pfcs_time_avail_hours
before insert ON pfcs_time_available_hours
FOR EACH ROW
DECLARE

records_quantity_w			integer;

BEGIN

    select	count(*)
	into	records_quantity_w
	from 	pfcs_time_available_hours;
 
  if  (:new.ds_rule is not null and records_quantity_w > 0)  then
      wheb_mensagem_pck.Exibir_Mensagem_Abort(1139766); 
  end if;

END;
/ 