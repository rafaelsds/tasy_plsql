CREATE OR REPLACE TRIGGER Convenio_Retorno_Update
BEFORE UPDATE ON Convenio_Retorno
FOR EACH ROW
BEGIN

if	(:old.IE_STATUS_RETORNO = 'F') and
	(:new.IE_STATUS_RETORNO <> 'F') then
	-- Voc� n�o pode fazer update do status do retorno via banco de dados!
	wheb_mensagem_pck.exibir_mensagem_abort(266940);
end if;

END;
/