create or replace trigger pls_simulacao_resumo_delete
before delete on pls_simulpreco_individual
for each row
declare

qt_reg_benef_del_w	number(10);
qt_reg_benef_del_ww	number(10);

begin

select	count(*)
into	qt_reg_benef_del_w
from	pls_simulacao_resumo
where	nr_seq_simulacao	= :old.nr_seq_simulacao
and	nr_seq_ordem		in (1,2,3,4,5)
and	nr_seq_segurado_simul	= :old.nr_sequencia;

if	(qt_reg_benef_del_w > 0) then
	delete	pls_simulacao_resumo
	where	nr_seq_simulacao	= :old.nr_seq_simulacao
	and	nr_seq_ordem		in (1,2,3,4,5)
	and	nr_seq_segurado_simul	= :old.nr_sequencia;
	
	delete	pls_bonificacao_vinculo
	where	nr_seq_segurado_simul	= :old.nr_sequencia;
	
	delete	pls_sca_vinculo
	where	nr_seq_segurado_simul	= :old.nr_sequencia;
end if;	

end;
/