CREATE OR REPLACE TRIGGER agenda_controle_horario_delete
  AFTER DELETE ON agenda_controle_horario
DECLARE
  PRAGMA AUTONOMOUS_TRANSACTION;

  qt_dias_sugestao_w number;
  qt_reg_w number(1) := 0;
BEGIN

  select nvl(max(qt_dias_sugestao), 0)
  into qt_dias_sugestao_w
  from parametro_agenda_integrada;
  
  select nvl(max(1), 0)
  into qt_reg_w
  from user_scheduler_running_jobs
  where job_name like 'AGINT_GERA_JOB%';

  IF (qt_dias_sugestao_w > 0) and (qt_reg_w = 0) THEN
    dbms_scheduler.create_job(job_name   => 'AGINT_GERA_JOB' ||
                                            to_char(SYSDATE+3/24/60/60, 'JHH24MISS'),
                              job_type   => 'PLSQL_BLOCK',
                              job_action => 'Begin AGEINT_SUGERIR_HORARIOS_PCK.AGEINT_VERIFICA_DIAS_LIBERADOS; END;',
                              enabled    => TRUE,
                              auto_drop  => TRUE);
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    NULL;
  
END;
/
