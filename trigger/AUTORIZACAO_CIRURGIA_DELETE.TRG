create or replace trigger AUTORIZACAO_CIRURGIA_DELETE
before delete on AUTORIZACAO_CIRURGIA
for each row

declare

ie_excluir_autor_conv_w		varchar2(10);
qt_reg_w			number(1);

pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

Obter_Param_Usuario(3006,73, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_excluir_autor_conv_w );

if	(nvl(ie_excluir_autor_conv_w,'S') = 'N') then
	
	update	autorizacao_convenio
	set	nr_seq_autor_cirurgia	= null		
	where	nr_seq_autor_cirurgia	= :old.nr_sequencia;
	
end if;

<<Final>>
qt_reg_w	:= 0;

commit;

end;
/