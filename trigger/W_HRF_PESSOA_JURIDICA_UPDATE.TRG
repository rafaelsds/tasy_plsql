create or replace trigger W_HRF_PESSOA_JURIDICA_UPDATE
before update on W_HRF_PESSOA_JURIDICA
for each row

declare
cd_cgc_w			varchar2(14)	:= null;
ds_erro_w			varchar2(2000)	:= '';

begin

select	max(cd_cgc)
into	cd_cgc_w
from	pessoa_juridica
where	cd_cgc	= replace(replace(replace(:new.cd_cgc,'.',''),'-',''),'/','');

if	(cd_cgc_w is not null) then

	begin
	update	pessoa_juridica
	set	nm_usuario	= 'TopSaude',
		dt_atualizacao	= sysdate,
		ds_razao_social	= :new.ds_razao_social,
		nm_fantasia	= nvl(:new.nm_fantasia,:new.ds_razao_social),
		cd_cep		= :new.cd_cep,
		ds_endereco	= :new.ds_endereco,
		ds_bairro	= :new.ds_bairro,
		ds_municipio	= :new.ds_municipio, 
		sg_estado	= :new.sg_estado,
		ds_complemento	= :new.ds_complemento, 
		nr_telefone	= :new.nr_telefone,
		nr_endereco	= :new.nr_endereco,
		cd_tipo_pessoa	= :new.cd_tipo_pessoa
	where	cd_cgc		= cd_cgc_w
	and	nm_usuario_nrec	= 'TopSaude';
	exception
		when others then
				
		ds_erro_w := SQLERRM(sqlcode);
		insert into log_tasy(
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values(
			sysdate,
			'TopSaude',
			55758,
			'N�o foi atualizada a PJ: ' ||
			'cd_cgc = ' || cd_cgc_w ||
			'ds_razao_social = ' || :new.ds_razao_social ||
			'DS_ERRO = ' || ds_erro_w);
	end;

	if	(:new.cd_banco 	is not null) and
		(:new.cd_agencia_bancaria is not null) and
		(:new.nr_conta	is not null) then

		begin
		update	pessoa_juridica_conta
		set	cd_banco		= :new.cd_banco,
			cd_agencia_bancaria	= :new.cd_agencia_bancaria,
			nr_conta		= :new.nr_conta,
			nr_digito_conta		= :new.nr_digito_conta,
			ie_conta_pagamento	= :new.ie_conta_pagamento,
			ie_digito_agencia	= :new.ie_digito_agencia,
			nm_usuario		= 'TopSaude',
			dt_atualizacao		= sysdate
		where	cd_cgc		= cd_cgc_w
		and	nm_usuario	= 'TopSaude';
		exception
			when others then
				
			ds_erro_w := SQLERRM(sqlcode);

			insert into log_tasy(
				dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
			values(
				sysdate,
				'TopSaude',
				55759,
				'N�o foi atualizada a conta de PJ: ' ||
				'cd_cgc = ' || cd_cgc_w ||
				'ds_razao_social = ' || :new.ds_razao_social ||
				'DS_ERRO = ' || ds_erro_w);
		end;
	end if;

		
	if	(:new.cd_conta_contab_rec is not null) and
		(:new.cd_conta_contab_rec <> :old.cd_conta_contab_rec) then

		update	pessoa_jur_conta_cont
		set	nm_usuario	= 'TopSaude',
			dt_atualizacao	= sysdate,
			cd_conta_contabil	= :new.cd_conta_contab_rec
		where	cd_cgc		= cd_cgc_w
		and	nm_usuario_nrec	= 'TopSaude'
		and	ie_tipo_conta	= 'R';
	end if;

	if	(:new.cd_conta_contab_pag is not null) and
		(:new.cd_conta_contab_pag <> :old.cd_conta_contab_pag) then

		update	pessoa_jur_conta_cont
		set	nm_usuario	= 'TopSaude',
			dt_atualizacao	= sysdate,
			cd_conta_contabil	= :new.cd_conta_contab_pag
		where	cd_cgc		= cd_cgc_w
		and	nm_usuario_nrec	= 'TopSaude'
		and	ie_tipo_conta	= 'P';
	end if;
end if;

end;
/