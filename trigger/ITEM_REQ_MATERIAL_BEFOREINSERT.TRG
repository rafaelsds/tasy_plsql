create or replace trigger item_req_material_beforeinsert
before insert on item_requisicao_material
for each row

declare
dt_liberacao_w			date;
ie_consignado_w			varchar2(1);
cd_local_estoque_w		number(5);
cd_local_estoque_destino_w		number(5);

begin

select	max(dt_liberacao),
	max(cd_local_estoque),
	nvl(max(cd_local_estoque_destino),0)
into	dt_liberacao_w,
	cd_local_estoque_w,
	cd_local_estoque_destino_w
from	requisicao_material
where	nr_requisicao = :new.nr_requisicao;

select 	nvl(max(a.ie_consignado),'0')
into	ie_consignado_w
from 	operacao_estoque a,
	requisicao_material b
where 	b.nr_requisicao = :new.nr_requisicao
and	a.cd_operacao_estoque = b.cd_operacao_estoque;


/* Retirado por Fabio - OS 386419 - Foi colocado na procedure CONSISTIR_REQUISICAO
if	(:new.qt_saldo_estoque is null) then
	begin

	if	(ie_consignado_w <> '0') then
		:new.qt_saldo_estoque	:= obter_saldo_estoque_consig(:new.cd_estabelecimento, :new.cd_cgc_fornecedor, :new.cd_material, cd_local_estoque_w);
	else
		:new.qt_saldo_estoque	:= obter_saldo_disp_estoque(:new.cd_estabelecimento, :new.cd_material, cd_local_estoque_w, trunc(sysdate,'mm'));
	end if;

	end;
end if;

if	(:new.qt_saldo_estoque_dest is null) and
	(cd_local_estoque_destino_w > 0) then
	begin

	if	(ie_consignado_w <> '0') then
		:new.qt_saldo_estoque_dest	:= obter_saldo_estoque_consig(:new.cd_estabelecimento, :new.cd_cgc_fornecedor, :new.cd_material, cd_local_estoque_destino_w);
	else
		:new.qt_saldo_estoque_dest	:= obter_saldo_disp_estoque(:new.cd_estabelecimento, :new.cd_material, cd_local_estoque_destino_w, trunc(sysdate,'mm'));
	end if;

	end;
end if;*/

:new.QT_MATERIAL_ATENDIDA := nvl(:new.QT_MATERIAL_ATENDIDA, 0);
end;
/