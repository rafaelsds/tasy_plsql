create or replace trigger tempo_procedimento_update
before update on tempo_procedimento
for each row
DECLARE
qt_reg_w	number(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.cd_procedimento is null) and
	(:new.ie_origem_proced is not null) then
	:new.ie_origem_proced := null;
end if;
<<Final>>
qt_reg_w	:= 0;
end;
/
