CREATE OR REPLACE TRIGGER PARAMETROS_EPISODIO_ATUAL
BEFORE INSERT OR UPDATE ON PARAMETROS_EPISODIO
FOR EACH ROW
DECLARE

qt_registros_w	number(10);

BEGIN
	if (:new.CD_PERFIL is null and :new.CD_ESTABELECIMENTO is not null) then
		select	count(1)
		into	qt_registros_w
		from	parametros_episodio
		where	CD_PERFIL is null
		and		CD_ESTABELECIMENTO = :new.CD_ESTABELECIMENTO;
	elsif (:new.CD_PERFIL is not null and :new.CD_ESTABELECIMENTO is null) then
		select	count(1)
		into	qt_registros_w
		from	parametros_episodio
		where	CD_ESTABELECIMENTO is null
		and		CD_PERFIL = :new.CD_PERFIL;
	elsif (:new.CD_PERFIL is not null and :new.CD_ESTABELECIMENTO is not null) then
		select	count(1)
		into	qt_registros_w
		from	parametros_episodio
		where	CD_ESTABELECIMENTO = :new.CD_ESTABELECIMENTO
		and		CD_PERFIL = :new.CD_PERFIL;
	end if;
	
	if (qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1070645);
	end if;
END;
/