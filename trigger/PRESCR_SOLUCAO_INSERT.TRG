CREATE OR REPLACE TRIGGER Prescr_solucao_Insert
BEFORE INSERT ON Prescr_solucao
FOR EACH ROW
DECLARE

dt_lib_w	date;
cd_funcao_origem_w		prescr_medica.cd_funcao_origem%type;

BEGIN

if	(:new.nr_seq_interno is null) then
	select	Prescr_solucao_seq.NextVal
	into	:new.nr_seq_interno
	from	dual;
end if;

select	max(nvl(dt_liberacao, dt_liberacao_medico)),
		max(cd_funcao_origem)
into	dt_lib_w,
		cd_funcao_origem_w
from	prescr_medica
where	nr_prescricao	= :new.nr_prescricao;

if	(dt_lib_w is not null) and
	(cd_funcao_origem_w	<> 2314) then
	Wheb_mensagem_pck.exibir_mensagem_abort(182660);
end if;

:new.ie_bomba_elastomerica	:= obter_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao);

if	(:new.ie_bomba_elastomerica is not null) and
	(nvl(obter_vol_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao),0) > 0) then
	:new.qt_dosagem			:= obter_vol_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao);
	:new.qt_solucao_total		:= Obter_vel_sol_mlh(:new.ie_tipo_dosagem, :new.qt_dosagem) * :new.qt_tempo_aplicacao * :new.nr_etapas;
end if;

if	(:new.nr_etapas = 0) and
	((:new.ie_acm = 'S') or
	 (:new.IE_SOLUCAO_PCA = 'S') or
	 (:new.ie_se_necessario = 'S')) and
	(nvl(:new.ie_hemodialise,'N') = 'N') then
	:new.nr_etapas	:= null;
end if;

:new.nr_etapas_medico	:= :new.nr_etapas;

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

END;
/
