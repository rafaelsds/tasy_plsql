create or replace trigger MANCHESTER_FLUXOGRAMA_ATUAL
before insert or update or delete on MANCHESTER_FLUXOGRAMA
for each row

begin
	
	if ('N' = obter_se_os_manchester) then
		wheb_mensagem_pck.exibir_mensagem_abort(1111609);
	end if;

end;
/