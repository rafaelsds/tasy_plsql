create or replace trigger radix_parecer_req_aftinsert
after insert or update on parecer_medico_req
for each row

declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
nr_seq_conv_w			atend_categoria_convenio.nr_seq_interno%type;
Ie_liberando_w 			boolean := (:old.DT_LIBERACAO is null) and (:new.DT_LIBERACAO is not null);
Ie_inativando_w 		boolean := (:old.DT_INATIVACAO is null) and (:new.DT_INATIVACAO is not null);

begin

if  (Ie_liberando_w) or
	(Ie_inativando_w) then

	nr_seq_conv_w := obter_atecaco_atendimento(:new.nr_atendimento);

	select 	max(b.cd_convenio),
    max(b.cd_categoria),
    max(b.cd_plano_convenio),
    max(a.ie_tipo_atendimento),
    max(a.cd_estabelecimento)
  into  reg_integracao_w.cd_convenio,
    reg_integracao_w.cd_categoria,
    reg_integracao_w.cd_plano_convenio,
    reg_integracao_w.ie_tipo_atendimento,
    reg_integracao_w.cd_estab_documento
  from   atendimento_paciente a,
      atend_categoria_convenio b
  where   a.nr_atendimento = b.nr_atendimento
  and    b.nr_seq_interno = nr_seq_conv_w;

  if  (Ie_liberando_w) then
    gerar_int_padrao.gravar_integracao('69', :new.nr_parecer, :new.nm_usuario, reg_integracao_w);
    --INSERT INTO TMP_MONITORADO SELECT 'ENTROU NO IF DE LIBERACAO - '||reg_integracao_w.cd_estab_documento FROM DUAL;
  elsif (Ie_inativando_w)  then
    gerar_int_padrao.gravar_integracao('123', :new.nr_parecer, :new.nm_usuario, reg_integracao_w);
    --INSERT INTO TMP_MONITORADO SELECT 'ENTROU NO IF DE INATIVACAO - '||:new.nr_parecer FROM DUAL;
  end if;

end if;
end;
/