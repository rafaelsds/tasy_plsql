create or replace
trigger prescr_proc_antic_update
before update on prescr_proc_result_antic
for each row

declare                                                                         
                                                                                                                                                                                           
begin                                                                           
                                                                                
if	:old.DT_LIBERACAO is null and 
	:new.DT_LIBERACAO is not null and
	(:new.DS_RESULTADO is null) and 
	(:new.IE_RESULTADO is null) then
	---Exame n�o possui resultado, n�o � poss�vel a sua libera��o!
	Wheb_mensagem_pck.exibir_mensagem_abort(261433);
end if;
                                                                                
end prescr_proc_antic_update;
/