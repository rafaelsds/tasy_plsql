CREATE OR REPLACE TRIGGER proc_criterio_horario_insert
BEFORE INSERT ON proc_criterio_horario
FOR EACH ROW
DECLARE

Contador_w	number(3) := 0;

BEGIN

if	(:new.CD_AREA_PROCED is not null) then
	Contador_w := 1;
end if;

if	(:new.CD_ESPECIAL_PROCED is not null) then
	Contador_w := Contador_w + 1;
end if;

if	(:new.CD_GRUPO_PROCED is not null) then
	Contador_w := Contador_w + 1;
end if;

if	(:new.CD_PROCEDIMENTO is not null) then
	Contador_w := Contador_w + 1;
end if;

if	(:new.CD_PROCEDIMENTO is null) then
	:new.ie_origem_proced	:= null;
end if;

if	(Contador_w > 1) then
	--r.aise_application_error(-20011,'Deve ser informado somente o procedimento ou grupo ou �rea ou especialidade!');
	wheb_mensagem_pck.exibir_mensagem_abort(263420);
end if;

END;
/