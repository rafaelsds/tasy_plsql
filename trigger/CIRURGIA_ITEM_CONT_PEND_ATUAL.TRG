create or replace
trigger cirurgia_item_cont_pend_atual
after insert or update on cirurgia_item_controle
for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
ie_libera_cavidade_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_libera_cavidade)
into	ie_libera_cavidade_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_libera_cavidade_w,'N') = 'S') then
	select	max(c.nr_atendimento),
				max(c.cd_pessoa_fisica)
	into		nr_atendimento_w,
				cd_pessoa_fisica_w
	from		cirurgia c
	where		c.nr_cirurgia = :new.nr_cirurgia;

	if 	(cd_pessoa_fisica_w is null) then
		select	max(c.nr_atendimento),
					max(c.cd_pessoa_fisica)
		into		nr_atendimento_w,
					cd_pessoa_fisica_w
		from		pepo_cirurgia c
		where		c.nr_sequencia = :new.nr_seq_pepo;
	end if;

	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'LCC';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XLCC';
	end if;
			
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/