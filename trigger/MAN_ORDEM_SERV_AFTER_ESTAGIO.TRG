create or replace trigger man_ordem_serv_after_estagio
after insert or update of nr_seq_estagio on man_ordem_servico
for each row

declare
dt_estagio_anterior_w		date;
nr_seq_gerencia_w		number(10);
cd_pessoa_gerente_w		varchar2(10);
qt_minuto_pre_analise_w		number(15);
ie_origem_alteracao_w		varchar2(1);

begin

if	(user = 'CORP') or
	(obter_se_base_wheb = 'S') then

	if	(:old.nr_seq_estagio is null) and
		(:new.nr_seq_estagio in (1051,231,1731)) then --Desenv aguardando triagem, Aguardando triagem, Tec aguardando triagem

		if	(:new.nr_seq_grupo_des is not null) then

			select	max(nr_seq_gerencia)
			into	nr_seq_gerencia_w
			from	grupo_desenvolvimento
			where	nr_sequencia = :new.nr_seq_grupo_des;

		elsif	(:new.nr_seq_grupo_sup is not null) then

			select	max(nr_seq_gerencia_sup)
			into	nr_seq_gerencia_w
			from	grupo_suporte
			where	nr_sequencia = :new.nr_seq_grupo_sup;

		end if;

		select	substr(obter_responsavel_gerencia(nr_seq_gerencia_w), 1, 10),
			decode(upper(:new.nm_usuario), 'WEBSERVICE', 'E', 'WEBSERVICE 2', 'E', 'WEB', 'E', 'WEBSERVER', 'E', 'I')
		into	cd_pessoa_gerente_w,
			ie_origem_alteracao_w
		from	dual;

		insert into man_ordem_serv_log_triagem (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario, 
			dt_atualizacao_nrec, 
			nm_usuario_nrec, 
			nr_seq_ordem_serv, 
			nr_seq_localizacao, 
			nr_seq_grupo_des, 
			nr_seq_grupo_sup,
			nr_seq_gerencia, 
			cd_pessoa_gerente, 
			nr_seq_estagio_anterior, 
			dt_estagio_anterior, 
			nr_seq_estagio_atual, 
			dt_estagio_atual, 
			ie_classificacao_cliente, 
			ie_classificacao_philips, 
			ie_prioridade_cliente, 
			ie_prioridade_philips, 
			qt_minuto_pre_analise, 
			dt_ordem_servico, 
			dt_inicio_real, 
			ie_origem_alteracao)
		values (	man_ordem_serv_log_triagem_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			:new.nr_seq_localizacao,
			:new.nr_seq_grupo_des,
			:new.nr_seq_grupo_sup,
			nr_seq_gerencia_w,
			cd_pessoa_gerente_w,
			null,
			null,
			:new.nr_seq_estagio,
			sysdate,
			:new.ie_classificacao_cliente,
			:new.ie_classificacao,
			:new.ie_prioridade_cliente,
			:new.ie_prioridade,
			0,
			:new.dt_ordem_servico,
			sysdate,
			ie_origem_alteracao_w);

	elsif	(:old.nr_seq_estagio in (1051,231,1731)) and --Desenv aguardando triagem, Aguardando triagem, Tec aguardando triagem
		(:new.nr_seq_estagio is not null) and
		(:new.nr_seq_estagio <> :old.nr_seq_estagio) then

		select	max(dt_atualizacao)
		into	dt_estagio_anterior_w
		from	man_ordem_serv_estagio
		where	nr_seq_ordem = :new.nr_sequencia
		and	nr_seq_estagio = :old.nr_seq_estagio;

		if	(:old.nr_seq_grupo_des is not null) then

			select	max(nr_seq_gerencia)
			into	nr_seq_gerencia_w
			from	grupo_desenvolvimento
			where	nr_sequencia = :old.nr_seq_grupo_des;

		elsif	(:old.nr_seq_grupo_sup is not null) then

			select	max(nr_seq_gerencia_sup)
			into	nr_seq_gerencia_w
			from	grupo_suporte
			where	nr_sequencia = :old.nr_seq_grupo_sup;

		end if;

		select	substr(obter_responsavel_gerencia(nr_seq_gerencia_w), 1, 10),
			decode(upper(:new.nm_usuario), 'WEBSERVICE', 'E', 'WEBSERVICE 2', 'E', 'WEB', 'E', 'WEBSERVER', 'E', 'I')
		into	cd_pessoa_gerente_w,
			ie_origem_alteracao_w
		from	dual;

		if	(trunc(dt_estagio_anterior_w, 'dd') = trunc(sysdate, 'dd')) or
			(obter_cod_dia_semana(sysdate) in (7, 1) or
			 obter_se_feriado(1, sysdate) > 0) then
			select	(sysdate - dt_estagio_anterior_w) * 1440
			into	qt_minuto_pre_analise_w
			from	dual;
		else
			select	man_obter_min_com(1, dt_estagio_anterior_w, sysdate)
			into	qt_minuto_pre_analise_w
			from	dual;
		end if;

		insert into man_ordem_serv_log_triagem (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario, 
			dt_atualizacao_nrec, 
			nm_usuario_nrec, 
			nr_seq_ordem_serv, 
			nr_seq_localizacao, 
			nr_seq_grupo_des, 
			nr_seq_grupo_sup,
			nr_seq_gerencia, 
			cd_pessoa_gerente, 
			nr_seq_estagio_anterior, 
			dt_estagio_anterior, 
			nr_seq_estagio_atual, 
			dt_estagio_atual, 
			ie_classificacao_cliente, 
			ie_classificacao_philips, 
			ie_prioridade_cliente, 
			ie_prioridade_philips, 
			qt_minuto_pre_analise, 
			dt_ordem_servico, 
			dt_inicio_real, 
			ie_origem_alteracao)
		values (	man_ordem_serv_log_triagem_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			:new.nr_seq_localizacao,
			:old.nr_seq_grupo_des,
			:old.nr_seq_grupo_sup,
			nr_seq_gerencia_w,
			cd_pessoa_gerente_w,
			:old.nr_seq_estagio,
			dt_estagio_anterior_w,
			:new.nr_seq_estagio,
			sysdate,
			:old.ie_classificacao_cliente,
			:old.ie_classificacao,
			:old.ie_prioridade_cliente,
			:old.ie_prioridade,
			nvl(qt_minuto_pre_analise_w,0),
			:new.dt_ordem_servico,
			sysdate,
			ie_origem_alteracao_w);
	end if;
end if;

end;
/