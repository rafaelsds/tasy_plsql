create or replace trigger material_afatual
after insert or update on material
for each row

declare

begin
if	(1 = 2) then --De acordo com OS 1704282, o usu�rio ir� cadastrar os princ�pios ativos adicionais, n�o � necess�rio a trigger gerar eles automaticamente. Tamb�m ser� importado via ABDATA (GERMANY)

if	((inserting) and (:new.nr_seq_ficha_tecnica is not null)) or
	((updating) and (nvl(:old.nr_seq_ficha_tecnica, 0) <> nvl(:new.nr_seq_ficha_tecnica, 0))) then
	begin
	delete	material_princ_ativo
	where	cd_material = :new.cd_material;
	
	insert into material_princ_ativo(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_material,
		nr_seq_medic_ficha_tecnica,
		qt_conversao_mg,
		cd_unid_med_concetracao,
		cd_unid_med_base_conc,
		qt_concentracao_total,
		cd_unid_med_conc_total)
	select	material_princ_ativo_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.cd_material,
		nr_sequencia,
		null qt_conversao_mg,
		null cd_unid_med_concetracao,
		null cd_unid_med_base_conc,
		null qt_concentracao_total,
		null cd_unid_med_conc_total
	from	medic_ficha_tecnica
	where	nr_seq_superior = :new.nr_seq_ficha_tecnica;
	end;
end if;

end if;

if	(updating) and
	((nvl(:new.cd_material_generico,0) <> nvl(:old.cd_material_generico,0)) or
	(nvl(:new.cd_unidade_medida_consumo,'X') <> nvl(:old.cd_unidade_medida_consumo,'X')) or
	(nvl(:new.ie_tipo_material,'X') <> nvl(:old.ie_tipo_material,'X')) or
	(nvl(:new.ds_material,'X') <> nvl(:old.ds_material,'X')) or
	(nvl(:new.ie_situacao,'X') <> nvl(:old.ie_situacao,'X'))) then
	gerar_int_dankia_pck.dankia_atualiza_material(:old.cd_material,:new.cd_material_generico,:new.cd_unidade_medida_consumo,:new.ie_tipo_material,:new.ie_situacao,:new.ds_material,:new.nm_usuario);
end if;

end material_afatual;
/