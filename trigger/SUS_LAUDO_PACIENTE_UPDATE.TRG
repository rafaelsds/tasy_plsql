create or replace trigger sus_laudo_paciente_update
before update on sus_laudo_paciente
for each row

declare

Cursor C01 is
	select	nr_sequencia
	from	sus_apac_unif
	where	cd_procedimento = :new.cd_procedimento_solic
	and	dt_inicio_validade = :new.dt_inicio_val_apac
	and	nr_atendimento = :new.nr_atendimento;

ie_diagnostico_w			varchar2(1 char)	:= 'N';
cd_doenca_cid_w			procedimento.cd_doenca_cid%type;
cd_cid_secundario_w		procedimento.cd_cid_secundario%type;
--nr_interno_conta_w			number(10);
ie_permite_w			varchar2(1 char)	:= 'S';
cd_estabelecimento_w		number(4);
ie_proc_sus_antigo_w		parametro_faturamento.ie_proc_sus_antigo%type;
ie_gerar_diag_w			varchar2(1 char);
ie_gerar_diag_atend_atual_w		varchar2(1 char);
cd_cid_atualizar_w			diagnostico_doenca.cd_doenca%type;
cd_estab_ativo_w			number(5);
nm_usuario_ativo_w		varchar2(20 char);
ie_atualizar_periodo_apac_w		varchar2(15 char)	:= 'N';
nr_seq_apac_w			C01%rowtype;
qt_meses_val_laudo_w		number(10);
dt_inicio_val_apac_w		date;
dt_fim_val_apac_w			date;
cd_procedimento_solic_w		sus_laudo_paciente.cd_procedimento_solic%type;
ie_origem_proced_w		sus_laudo_paciente.ie_origem_proced%type;
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_activated_w			varchar2(1);
qt_laudo_medicamento_w		number(5);
ds_retorno_integracao_w		varchar2(4000);

begin

begin
select	cd_estabelecimento
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;
exception
when others then
	cd_estabelecimento_w := 0;
end;

select	nvl(max(ie_proc_sus_antigo),'N')
into	ie_proc_sus_antigo_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w; 

if	(ie_proc_sus_antigo_w = 'S') and
	(:old.ie_origem_proced not in (2,3)) and (:new.ie_origem_proced in (2,3)) then
	begin
	wheb_mensagem_pck.exibir_mensagem_abort(186393);
	/* Este procedimento nao pode ser lancado pois e de origem antiga do SUS. */
	end;
end if;

cd_estab_ativo_w   := wheb_usuario_pck.get_cd_estabelecimento;
nm_usuario_ativo_w := wheb_usuario_pck.get_nm_usuario;

obter_param_usuario(916,653,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_gerar_diag_w);
obter_param_usuario(916,654,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_gerar_diag_atend_atual_w);
obter_param_usuario(1124,123,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_atualizar_periodo_apac_w);
obter_param_usuario(9041,10,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_activated_w);

If	(Obter_funcao_ativa = 916)
And	(ie_gerar_diag_atend_Atual_w = 'S') then
	select	max(b.cd_doenca)
	into	cd_cid_atualizar_w
	from	diagnostico_doenca b
	where	b.nr_atendimento = :new.nr_atendimento
	and	b.dt_diagnostico = (	select	min(a.dt_diagnostico) 
					from	diagnostico_doenca a
					where	a.nr_atendimento = :new.nr_Atendimento);		
						
	If	(cd_cid_atualizar_w is not null) then
		:new.cd_cid_principal := cd_cid_atualizar_w;
	End if;
End if;	

if	(:new.ie_tipo_laudo_sus = 0) and
	(:new.cd_procedimento_solic is not null) then
	
	select	max(cd_doenca_cid),
		max(cd_cid_secundario)
	into	cd_doenca_cid_w,
		cd_cid_secundario_w
	from 	procedimento
	where 	cd_procedimento 	= :new.cd_procedimento_solic
	and	ie_origem_proced	= :new.ie_origem_proced;	
	
	If	((Obter_funcao_ativa <> 916) and
		 (ie_diagnostico_w	= 'N'))
	or	((Obter_funcao_ativa = 916) and
		 (ie_gerar_diag_w = 'S')) then
		begin
		insert into diagnostico_medico(
			nr_atendimento, dt_diagnostico, ie_tipo_diagnostico,
			cd_medico, dt_atualizacao, nm_usuario, ds_diagnostico)
		values(
			:new.nr_atendimento, :new.dt_emissao, 2, 
			:new.cd_medico_requisitante, sysdate, :new.nm_usuario, null);	
		if	(cd_doenca_cid_w is not null) then
			insert into diagnostico_doenca(
				nr_atendimento, dt_diagnostico, cd_doenca, dt_atualizacao,
				nm_usuario, ds_diagnostico, ie_classificacao_doenca, IE_TIPO_DIAGNOSTICO)
			values(
				:new.nr_atendimento, :new.dt_emissao, cd_doenca_cid_w, 
				sysdate, :new.nm_usuario, null, 'P', 2);	
		end if;
		if	(cd_cid_secundario_w is not null) then
			insert into diagnostico_doenca(
				nr_atendimento, dt_diagnostico, cd_doenca, dt_atualizacao,
				nm_usuario, ds_diagnostico, ie_classificacao_doenca, IE_TIPO_DIAGNOSTICO)
			values(
				:new.nr_atendimento, :new.dt_emissao, cd_cid_secundario_w, sysdate,  :new.nm_usuario, null, 'S', 2);	
		end if;
		exception
			when others then
				ie_diagnostico_w := 'S';
		end;
	End if;
end if;

/*if	(nvl(:new.nr_aih,0) <> nvl(:old.nr_aih,1)) then
	select	nvl(max(nr_interno_conta),null)
	into	nr_interno_conta_w
	from	sus_aih
	where	nr_aih = nvl(:new.nr_aih,0);

	:new.nr_interno_conta := nr_interno_conta_w;
end if;*/

if	((:new.nr_seq_aih is null) or 
	(:new.nr_seq_aih = 0)) and
	(:new.nr_aih is not null) then
	begin
	select	nr_sequencia
	into	:new.nr_seq_aih
	from	sus_aih
	where	nr_aih	= :new.nr_aih;
	exception	
	when others then
		:new.nr_seq_aih := :new.nr_seq_aih;
	end;
end if;	

if	(:new.nr_apac is not null) and
	(:old.nr_apac is null) then
	begin
	:new.ie_status_processo	:= 2;
	
	reg_integracao_p.nr_atendimento		:=	:new.nr_atendimento; 

	gerar_int_padrao.gravar_integracao(ie_evento_p => '395',nr_seq_documento_p => :new.nr_seq_interno, nm_usuario_p => :new.nm_usuario,reg_integracao_p => reg_integracao_p);
	
	if	(ie_activated_w = 'S') then
		begin
		
		begin
		select	count(1)
		into	qt_laudo_medicamento_w
		from	sus_laudo_medicamento
		where	nr_seq_laudo_sus = :new.nr_seq_interno;
		exception
		when no_data_found then
			qt_laudo_medicamento_w := 0;
		end;
		
		if	(nvl(qt_laudo_medicamento_w,0) > 0) then
			begin
			
			select	bifrost.send_integration(
			'authorizationManagement.send.request', 
			'com.philips.tasy.integration.sus.authorizationmanagementsus.outbound.AuthorizationManagementSusCallback',
			'{"encounter" : '||:new.nr_atendimento||', "internalSequenceReport": ' || :new.nr_seq_interno || ', "event": 395, "userName": "' || :new.nm_usuario || '" }',
			:new.nm_usuario)
			into	ds_retorno_integracao_w
			from	dual;
			
			end;
		end if;
		
		end;
	end if;
	
	end;
end if;

select	Obter_Se_solic_laudo(:new.nr_atendimento,:new.ie_classificacao,:new.cd_medico_requisitante)
into	ie_permite_w
from	dual;

if	(ie_permite_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(186392);
	/* O medico requisitante nao tem permissao para gerar laudo. 
	Verificar regra solicitantes de laudos nos Cadastros gerais/Aplicacao principal/Cadastros SUS. */
end if;

if	(nvl(ie_atualizar_periodo_apac_w,'N') = 'S') and
	(:new.dt_emissao is not null) and
	(nvl(:old.dt_emissao,:new.dt_emissao) <> :new.dt_emissao) then
	begin
	
	select	nvl(max(b.qt_meses_val_laudo - 1),0)
	into	qt_meses_val_laudo_w
	from	parametro_medico b,
		atendimento_paciente a
	where	a.cd_estabelecimento	= b.cd_estabelecimento
	and	a.nr_atendimento	= :new.nr_atendimento;

	dt_inicio_val_apac_w	:= :new.dt_emissao;
	
	if	(qt_meses_val_laudo_w	> 0) then		
		dt_fim_val_apac_w	:= last_day(add_months(:new.dt_emissao,qt_meses_val_laudo_w));
	end if;
	
    
    for nr_seq_apac_w in C01 loop
			begin
		
            update 	sus_apac_unif
            set 	dt_inicio_validade	= nvl(dt_inicio_val_apac_w,dt_inicio_validade),
                dt_fim_validade	= nvl(dt_fim_val_apac_w,dt_fim_validade)
            where 	nr_sequencia	= nr_seq_apac_w.nr_sequencia;
		
            end;
	end loop;
    
	
	:new.dt_inicio_val_apac	:= dt_inicio_val_apac_w;
	:new.dt_fim_val_apac	:= dt_fim_val_apac_w;
	
	end;
end if;

if 	(:new.nr_seq_proc_interno is not null) then
	begin
	if	((:new.cd_procedimento_solic is null) or
		(:new.nr_seq_proc_interno <> :old.nr_seq_proc_interno)) then
		begin
		obter_proc_tab_interno(:new.nr_seq_proc_interno, null, :new.nr_atendimento, null, cd_procedimento_solic_w, ie_origem_proced_w);
		if	(cd_procedimento_solic_w is not null) then
			:new.cd_procedimento_solic := cd_procedimento_solic_w;
			:new.ie_origem_proced := ie_origem_proced_w;
		end if;
		end;
	end if;
	end;
end if;

if	(:old.ie_classificacao = 15) and
	(:new.ie_classificacao <> 15) and
	(sus_obter_tiporeg_proc(:new.cd_procedimento_solic,:new.ie_origem_proced,'C',12) = 2) then
	:new.ie_classificacao := :old.ie_classificacao;
end if;

end;
/
