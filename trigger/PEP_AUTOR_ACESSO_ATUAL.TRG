create or replace trigger PEP_AUTOR_ACESSO_ATUAL
before insert or update on PEP_AUTOR_ACESSO
for each row

declare

nr_seq_perfil_w		number(10);
ie_autoriza_w		varchar(5);

begin

select	max(nr_seq_perfil)
into	nr_seq_perfil_w
from	pessoa_fisica
where	cd_pessoa_fisica = :new.cd_paciente;

select	max(ie_autorizar_acesso)
into	ie_autoriza_w
from	pep_perfil_paciente	
where	nr_sequencia = nr_seq_perfil_w;


if	(nvl(ie_autoriza_w,'S') = 'N') then
	--	N�o � poss�vel alterar a autoriza��o de acesso para este perfil do paciente. Gest�o de acessos PEP.#@#@
	Wheb_mensagem_pck.exibir_mensagem_abort(264546);
end if;


end;
/