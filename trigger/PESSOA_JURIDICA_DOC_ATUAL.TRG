create or replace trigger pessoa_juridica_doc_atual
before insert or update on pessoa_juridica_doc
for each row

declare

begin
if	(:new.dt_emissao is not null) and
	(:new.dt_emissao > sysdate) then	
	wheb_mensagem_pck.exibir_mensagem_abort(887581);
	--A data de emiss�o deve ser igual ou menor a data atual!
end if;

if	(:new.dt_emissao is not null) and
	(:new.dt_revisao is not null) and
	(:new.dt_revisao < :new.dt_emissao) then	
	wheb_mensagem_pck.exibir_mensagem_abort(900320);
	--A data de validade deve ser maior ou igual que data de emiss�o!'
end if;
end;
/
