CREATE OR REPLACE TRIGGER PROT_INT_EVENTO_AFTER_DELETE
BEFORE DELETE ON protocolo_integrado_evento
FOR EACH ROW
declare
    pragma autonomous_transaction;
    NR_SEQ_PROT_INT_W protocolo_integrado.NR_SEQUENCIA%TYPE;
BEGIN
    if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
        if (obtain_user_locale(wheb_usuario_pck.get_nm_usuario()) = 'ja_JP' ) THEN 
            
            SELECT
                MAX(nr_seq_protocolo_integrado)
            INTO NR_SEQ_PROT_INT_W
            FROM protocolo_integrado_etapa a 
            WHERE
                a.NR_SEQUENCIA = :OLD.NR_SEQ_ETAPA;
            
            IF (:OLD.NR_SEQ_PLANO is not null) THEN
            
                UPDATE_GQA_PENDENCIA_REGRA(:OLD.NR_SEQ_PLANO,:OLD.NR_SEQUENCIA,NR_SEQ_PROT_INT_W);
                
            END IF;
        END IF ;
    END IF;
END PROT_INT_EVENTO_AFTER_DELETE;
/