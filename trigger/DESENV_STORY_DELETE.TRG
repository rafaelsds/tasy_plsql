create or replace trigger desenv_story_delete
after delete on desenv_story
for each row

declare

begin

insert into desenv_atividade(
	nr_sequencia,
	nr_epic,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_sprint_destino,
	nr_release,
	nr_release_destino,
	nr_art,
	nr_art_destino,
	cd_tipo,
	nr_feature,
	nr_story,
	nr_sprint)
values(	desenv_atividade_seq.nextval, 
	null, 
	sysdate, 
	:old.nm_usuario,
	sysdate,
	:old.nm_usuario,
	null,
	null,
	null,
	null,
	null,
	'SD',
	null,
	:old.nr_sequencia,
	null);

end;
/