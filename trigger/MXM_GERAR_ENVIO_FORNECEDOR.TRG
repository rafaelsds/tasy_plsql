create or replace trigger MXM_GERAR_ENVIO_FORNECEDOR
after insert or update on PESSOA_JURIDICA
for each row

declare
tafo_sqprocesso_w	number(9)    := null;
tafo_sqregistro_w	number(9);



begin

select	integra_sq_gerafornec.nextval@mxmhomol
into	tafo_sqprocesso_w
from	dual;

begin
insert	into ti_gerafornec_tgfo@mxmhomol (
		tgfo_sqprocesso,
		tgfo_dtgeracao,
		tgfo_usuariogeracao,
		tgfo_cdsistema)
	values(	tafo_sqprocesso_w,
		sysdate,
		'TASY',
		'1');
exception
when	others then
	raise_application_error(-20011,'Falha ao gravar ti_gerafornec_tgfo@mxmhomol' || chr(13) || chr(10) || sqlerrm);
end;

select	nvl(max(a.tafo_sqregistro),0) + 1
into	tafo_sqregistro_w
from	ti_arqfornec_tafo@mxmhomol a
where	a.tafo_sqprocesso = tafo_sqprocesso_w;

begin
insert	into ti_arqfornec_tafo@mxmhomol
	(tafo_sqprocesso,
	tafo_sqregistro,
	tafo_codigo,
	tafo_nome,
	tafo_endereco,
	tafo_tipessoa,
	tafo_bairro,
	tafo_cidade,
	tafo_uf,
	tafo_cep,
	tafo_tel,
	tafo_fax,
	tafo_cgc,
	tafo_inscricao,
	tafo_endereco1,
	tafo_dtmov,
	tafo_dtcad,
	tafo_dtnasc,
	tafo_nomefant,
	tafo_ativo,
	tafo_pais,
	tafo_inscinss,
	tafo_classeinss,
	tafo_email,
	tafo_inscmunip,
	tafo_inscsuframa,
	tafo_cooperativa,
	tafo_stoperacao,
	tafo_numendereco,
	tafo_compendereco)
values(	tafo_sqprocesso_w,
	tafo_sqregistro_w,
	:new.cd_cgc,
	substr(:new.ds_razao_social,1,40),
	substr(:new.ds_endereco,1,80),
	'J',
	substr(:new.ds_bairro,1,20),
	substr(:new.ds_municipio,1,20),
	:new.sg_estado,
	:new.cd_cep,
	substr(:new.nr_ddd_telefone || :new.nr_telefone,1,40),
	substr(:new.nr_ddd_fax || :new.nr_fax,1,20),
	:new.cd_cgc,
	substr(:new.nr_inscricao_estadual,1,50),
	substr(:new.ds_endereco,1,60),
	trunc(sysdate),
	trunc(sysdate),
	null,
	substr(:new.nm_fantasia,1,40),
	:new.ie_situacao,
	:new.nr_seq_pais,
	'',
	'',
	'',
	substr(:new.nr_inscricao_municipal,1,20),
	'',
	'',
	0,
	:new.nr_endereco,
	substr(:new.ds_complemento,1,50));
exception
when	others then
	raise_application_error(-20011,'Falha ao gravar ti_arqfornec_tafo@mxmhomol' || chr(13) || chr(10) || sqlerrm);
end;

end;
/