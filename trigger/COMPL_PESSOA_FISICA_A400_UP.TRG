create or replace trigger compl_pessoa_fisica_a400_up
before update on compl_pessoa_fisica
for each row

declare
nr_seq_prestador_w	pls_prestador.nr_sequencia%type;
ie_divulga_email_w	pls_prestador.ie_divulga_email%type;
qt_email_w		pls_integer;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ie_param_6_w		varchar2(1);
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	if	(:new.ie_tipo_complemento in(1, 2, 9)) and
		((:new.ds_email <> :old.ds_email) or
		 (:new.ds_email is null and :old.ds_email is not null))then
		
		select	max(ie_divulga_email),
			min(nr_sequencia)
		into	ie_divulga_email_w,
			nr_seq_prestador_w
		from	pls_prestador
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and	ie_divulga_email	= 'S'
		and	ie_ptu_a400		= 'S';
		
		select	max(cd_estabelecimento)
		into	cd_estabelecimento_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

		obter_param_usuario( 1323,  6, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_W, ie_param_6_w); 
		
		if	(nr_seq_prestador_w is not null) and
			(nvl(ie_divulga_email_w, 'N') = 'S') and
			(nvl(ie_param_6_w, 'S') = 'N') then

			case 	(:new.ie_tipo_complemento)
				when 	1 then -- Endere�o Residencial

					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email is not null
					and	b.ie_tipo_endereco_original = 'PFR';
				when	2	then --Endere�o comercial
					
					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email is not null
					and	b.ie_tipo_endereco_original = 'PFC';
				when	9	then -- Endere�o adicional
					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email is not null
					and	b.ie_tipo_endereco_original = 'PFA';
				else
					qt_email_w := 0;
			end case;	

			if	(qt_email_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(387909);
			end if;
		end if;	
	end if;
end if;
end;
/
