CREATE OR REPLACE TRIGGER Atend_Categ_Convenio_BefInsert
BEFORE INSERT ON Atend_Categoria_Convenio
FOR EACH ROW
DECLARE

ie_tipo_convenio_w		number(2) := 2;
ie_plano_w				number(1);
ie_regulacao_gerint_w	parametro_atendimento.ie_regulacao_gerint%type;
nr_cerih_w				gerint_solic_internacao.nr_protocolo_solicitacao%type;

BEGIN

if ('00/00/0000 00:00:00' = to_char(:new.dt_validade_carteira,'dd/mm/yyyy hh24:mi:ss')) then
	:new.dt_validade_carteira := null;
end if;

select nvl(max(1), 0)
into ie_plano_w
from convenio_plano
where cd_plano = :new.cd_plano_convenio;

if (ie_plano_w = 0) then
	:new.cd_plano_convenio := null;
end if;

select	nvl(max(ie_regulacao_gerint),'N')
into	ie_regulacao_gerint_w
from	parametro_atendimento
where	cd_estabelecimento	= (	select	max(cd_estabelecimento)
								from	atendimento_paciente
								where 	nr_atendimento =  :new.nr_atendimento);

if (ie_regulacao_gerint_w = 'S') then
	select	max(nr_protocolo_solicitacao)
	into	nr_cerih_w
	from	gerint_solic_internacao
	where	nr_atendimento = :new.nr_atendimento;
	
	if (nr_cerih_w is not null) and (:new.cd_senha is null) then
		:new.cd_senha := nr_cerih_w;
	end if;
end if;

END;
/