create or replace trigger nut_orientacao_list_aftupdate
after update on nut_orientacao_list
for each row

declare
json_data_w clob;
ds_param_integration_w  varchar2(500);

begin
	if ((nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') and (nvl(:old.ie_status, 'X') <> nvl(:new.ie_status, 'X') and :new.ie_status in ('A', 'S')) and (nvl(:old.ie_guidance_fee, 'X') <> nvl(:new.ie_guidance_fee, 'X') and :new.ie_guidance_fee = 'S')) then
		ds_param_integration_w :=  '{"recordId" : "' || :new.nr_sequencia|| '"' || '}';
		json_data_w := bifrost.send_integration_content('nais.mla.nutrition.guidancefee', ds_param_integration_w, wheb_usuario_pck.get_nm_usuario);
	end if;
end;
/
