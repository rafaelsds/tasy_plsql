create or replace trigger SETOR_ATENDIMENTO_INSERT
before insert on SETOR_ATENDIMENTO
for each row
Declare
dt_inicio_prescricao_w	varchar2(100) := '30/12/1899';

begin
if (:new.hr_inicio_prescricao is not null) and
   (:new.hr_inicio_prescricao > to_date('01/01/2013 00:00:01','dd/mm/yyyy hh24:mi:ss')) then
	-- Tratamento feito, pois o componente no delphi � colocado data padr�o abaixo do ano 1900, e no java pega a atual -- Richart
	
	:new.hr_inicio_prescricao := to_date(dt_inicio_prescricao_w||' '||to_char(:new.hr_inicio_prescricao,'hh24:mi;ss'),'dd/mm/yyyy hh24:mi:ss');
end if;


end;
/
