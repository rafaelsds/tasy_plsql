create or replace trigger mprev_atendimento_insert
before insert on mprev_atendimento
for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Apenas verificar se para o atendimento do tipo Coletivo foi informado uma turma.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

if	(:new.ie_individual_coletivo = 'C') and
	(:new.nr_seq_turma is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(322956);
end if;

end;
/