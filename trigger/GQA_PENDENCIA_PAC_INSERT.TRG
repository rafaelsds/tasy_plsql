create or replace trigger gqa_pendencia_pac_insert
before insert on gqa_pendencia_pac
for each row

begin
 :new.nr_seq_origem := nvl(:new.nr_seq_curativo   , 0) +
                       nvl(:new.nr_seq_diag_doenca, 0) +
                       nvl(:new.nr_seq_escala     , 0) +
                       nvl(:new.nr_seq_escala_tev , 0) +
                       nvl(:new.nr_seq_protocolo  , 0) +
                       nvl(:new.nr_seq_qua_evento , 0) +
                       nvl(:new.nr_seq_sinal_vital, 0) +
                       nvl(:new.nr_seq_triagem    , 0);
                       
  if (:new.nr_seq_origem = 0) then
    :new.nr_seq_origem := null;
  end if;
end;
/