create or replace TRIGGER man_ordem_servico_bf_vv before
  INSERT OR
  UPDATE ON man_ordem_servico FOR EACH row DECLARE ie_base_philips_w VARCHAR2(1);
  ie_existe_hist_w                                                   VARCHAR2(1);
  ie_estagio_vv_w                                                    VARCHAR2(1);
  /* DMB */
  ie_hist_risco_w        VARCHAR2(1);
  ie_hist_agreement_w    VARCHAR2(1);
  ie_hist_disagreement_w VARCHAR2(1);
  ie_hist_meeting_w      VARCHAR2(1);
  BEGIN
    IF(:new.nr_seq_equipamento = 7402 and :old.ie_classificacao <> :new.ie_classificacao AND :old.ie_classificacao = 'A' and :new.ie_plataforma = 'H') THEN
      SELECT NVL(MAX('S'), 'N')
      INTO ie_hist_risco_w
      FROM man_ordem_serv_tecnico most
      WHERE most.nr_seq_ordem_serv = :new.nr_sequencia
      AND most.nr_seq_tipo         = 220 -- DMB VeV -  Potential Risk
      AND most.dt_liberacao       IS NOT NULL;
      
      SELECT NVL(MAX('S'), 'N')
      INTO ie_hist_meeting_w
      FROM man_ordem_serv_tecnico most
      WHERE most.nr_seq_ordem_serv = :new.nr_sequencia
      AND most.nr_seq_tipo         = 211 -- DMB Meeting
      AND most.dt_liberacao       IS NOT NULL;
      
      SELECT NVL(MAX('S'), 'N')
      INTO ie_hist_agreement_w
      FROM man_ordem_serv_tecnico most
      WHERE most.nr_seq_ordem_serv = :new.nr_sequencia
      AND most.nr_seq_tipo         = 207 -- DMB Developemnt - Agrees with VeV DMB
      AND most.dt_liberacao       IS NOT NULL;
      
      SELECT NVL(MAX('S'), 'N')
      INTO ie_hist_disagreement_w
      FROM man_ordem_serv_tecnico most
      WHERE most.nr_seq_ordem_serv = :new.nr_sequencia
      AND most.nr_seq_tipo         = 208 -- DMB Business Analyst - Disagrees with VeV DMB
      AND most.dt_liberacao       IS NOT NULL;
      
      IF ((ie_hist_risco_w         = 'S' OR ie_hist_disagreement_w = 'S') AND ie_hist_meeting_w = 'N') THEN
        BEGIN
          wheb_mensagem_pck.exibir_mensagem_abort(1089633);
        END;
      elsif (ie_hist_risco_w = 'N' AND ie_hist_agreement_w = 'N' AND ie_hist_disagreement_w = 'N') THEN
        BEGIN
          wheb_mensagem_pck.exibir_mensagem_abort(1089633);
        END;
      END IF;
    END IF;
    SELECT NVL(MAX('S'), 'N')
    INTO ie_base_philips_w
    FROM dual
    WHERE obter_se_base_corp = 'S'
    OR obter_se_base_wheb    = 'S';
    IF (ie_base_philips_w    = 'S') THEN
      BEGIN
        IF (:new.ie_classificacao                           = 'A'  -- Anomalia
          AND :new.ie_plataforma                            = 'H'  -- Plataforma HTML5
          AND :new.nr_seq_equipamento                       = 7402) THEN -- Verification Department HTML5
          BEGIN
            IF (inserting) THEN
              BEGIN
                IF (:new.nr_seq_severidade = 1 -- Risco a seguranca do paciente
                  ) THEN
                  BEGIN
                    :new.nr_seq_estagio := 2563;-- DMB waiting Meeting
                  END;
                END IF;
              END;
            elsif (updating) THEN
              BEGIN
                IF (:new.nr_seq_estagio <> 2563 AND :old.nr_seq_estagio = 2563) THEN--tratamento trigger mutante
                
                  SELECT NVL(MAX('S'), 'N')
                  INTO ie_hist_risco_w
                  FROM man_ordem_serv_tecnico most
                  WHERE most.nr_seq_ordem_serv = :new.nr_sequencia
                  AND most.nr_seq_tipo         = 220 -- DMB VeV -  Potential Risk
                  AND most.dt_liberacao       IS NOT NULL;
      
                  SELECT NVL(MAX('S'), 'N')
                  INTO ie_hist_disagreement_w
                  FROM man_ordem_serv_tecnico most
                  WHERE most.nr_seq_ordem_serv = :new.nr_sequencia
                  AND most.nr_seq_tipo         = 208 -- DMB Business Analyst - Disagrees with VeV DMB
                  AND most.dt_liberacao       IS NOT NULL;
                                
                  SELECT NVL(MAX('S'),'N')
                  INTO ie_existe_hist_w
                  FROM man_ordem_serv_tecnico most
                  WHERE most.nr_seq_ordem_serv = :new.nr_sequencia
                  AND most.nr_seq_tipo         = 211 --DMB Meeting
                  AND most.dt_liberacao       IS NOT NULL;

                  IF (:old.nr_seq_estagio     <> :new.nr_seq_estagio AND :old.nr_seq_estagio = 2563--- DMB waiting Meeting
                    AND (ie_existe_hist_w       = 'N' and  (ie_hist_disagreement_w = 'S' or ie_hist_risco_w = 'S'))) THEN
                    BEGIN
                      wheb_mensagem_pck.exibir_mensagem_abort(1089707);
                    END;
                  END IF;
                END IF;
                
                IF (:new.nr_seq_estagio <> 2563 AND :old.nr_seq_estagio <> 2563) THEN--tratamento trigger mutante
                  SELECT NVL(MAX('S'),'N')
                  INTO ie_existe_hist_w
                  FROM man_ordem_serv_tecnico most
                  WHERE most.nr_seq_ordem_serv = :new.nr_sequencia
                  AND most.nr_seq_tipo        IN (220, 221)--DMB assessment
                  AND most.dt_liberacao       IS NOT NULL;
                  
                  SELECT NVL(MAX('S'), 'N')
                  INTO ie_estagio_vv_w
                  FROM man_estagio_processo mep
                  WHERE mep.nr_sequencia          = :new.nr_seq_estagio
                  AND NVL(mep.ie_desenv, 'N')     = 'N'
                  AND NVL(mep.ie_suporte, 'N')    = 'N'
                  AND NVL(mep.ie_tecnologia, 'N') = 'N'
                  AND NVL(mep.ie_situacao, 'A')   = 'A';
                  IF ( :old.nr_seq_estagio <> :new.nr_seq_estagio AND ie_existe_hist_w = 'N' AND ie_estagio_vv_w = 'N') THEN
                    BEGIN
                      wheb_mensagem_pck.exibir_mensagem_abort(1089708);
                    END;
                  END IF;
                END IF;
              END;
            END IF;
          END;
        END IF;
      END;
    END IF;
  END;
  /