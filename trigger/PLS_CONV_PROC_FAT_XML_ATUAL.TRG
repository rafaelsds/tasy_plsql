create or replace trigger pls_conv_proc_fat_xml_atual
before insert or update on pls_conv_proc_fat_xml
for each row

declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899 desta forma podemos utilizar um between ou fazer uma compara��o com este campo
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia, 'F');

end pls_conv_proc_fat_xml_atual;
/