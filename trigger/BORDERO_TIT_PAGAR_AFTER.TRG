create or replace trigger bordero_tit_pagar_after
after insert or update on bordero_tit_pagar
for each row

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',:new.dt_atualizacao,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',:new.dt_atualizacao,'A',:new.nm_usuario);
end if;
end if;

end;
/
