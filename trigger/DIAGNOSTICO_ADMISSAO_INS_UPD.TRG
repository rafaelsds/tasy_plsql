create or replace TRIGGER diagnostico_admissao_ins_upd
  AFTER INSERT OR UPDATE ON diagnostico_admissao_atend
  FOR EACH ROW

DECLARE

  json_aux_bb                 philips_json;
  envio_integracao_bb         clob;
  retorno_integracao_bb       clob;
  ds_integracao_orgao_bb      ORGAO_DIAGNOSTICO_ADMISSAO.DS_INTEGRACAO%TYPE;
  ds_integracao_admissao_bb   DIAGNOSTICO_ADMISSAO.DS_INTEGRACAO%TYPE;
  ie_operacao_bb              DIAGNOSTICO_ADMISSAO.IE_OPERACAO%TYPE;

BEGIN
IF (wheb_usuario_pck.get_ie_executar_trigger = 'S') THEN
  IF (OBTER_SE_INTEGRACAO_ATIVA(968, 245) = 'S') THEN
    BEGIN
      SELECT  ds_integracao
      INTO    ds_integracao_orgao_bb
      FROM    ORGAO_DIAGNOSTICO_ADMISSAO
      WHERE   NR_SEQUENCIA = :new.nr_seq_orgao;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ds_integracao_orgao_bb := NULL;
    END;

    BEGIN
      SELECT  ds_integracao, ie_operacao
      INTO    ds_integracao_admissao_bb, ie_operacao_bb
      FROM    DIAGNOSTICO_ADMISSAO
      WHERE   NR_SEQUENCIA = :new.nr_seq_diagnostico;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ds_integracao_admissao_bb := NULL;
        ie_operacao_bb := NULL;
    END;

    json_aux_bb := philips_json();
    json_aux_bb.put('typeID', 'ADMITDIAG');
    json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD HH24:MI:SS.SSSSS'));
    json_aux_bb.put('patientHealthSystemStayID', LPAD(:new.nr_atendimento, 32, 0));
    json_aux_bb.put('admissionDiagnosisID', LPAD(:new.nr_sequencia, 32, 0));

    IF (:new.ie_operacao = 'S') THEN
      json_aux_bb.put('admittedFromOR', 'True');
    ELSE
      json_aux_bb.put('admittedFromOR', 'False');
    END IF;

    IF (:new.ie_eletiva = 'S') THEN
      json_aux_bb.put('admittedElective', 'True');
    ELSE
      json_aux_bb.put('admittedElective', 'False');
    END IF;

    json_aux_bb.put('objectID1', ds_integracao_orgao_bb);
    json_aux_bb.put('objectID2', ds_integracao_admissao_bb);

    IF (ie_operacao_bb IS NOT NULL AND ie_operacao_bb = 'S') THEN
      json_aux_bb.put('objectID3', 'fce7534340a84c5788463850b29a9c43');
    ELSE
      json_aux_bb.put('objectID3', 'cb3a7d6ae4ec431a8c7e3273dd118a8e');
    END IF;

    dbms_lob.createtemporary(envio_integracao_bb, TRUE);
    json_aux_bb.to_clob(envio_integracao_bb);
    SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Admission_Diagnosis',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
  END IF;
END IF;
END;
/