create or replace trigger atual_regra_indic_cli_obrig
before insert or update on regra_indic_clinica_obrig
for each row

declare

begin

if (:new.hr_prev_inicial is not null) and 
   ((:new.hr_prev_inicial <> :old.hr_prev_inicial) or (:old.dt_prev_inicial is null)) then
	:new.dt_prev_inicial := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prev_inicial || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

if (:new.hr_prev_final is not null) and 
   ((:new.hr_prev_final <> :old.hr_prev_final) or (:old.dt_prev_final is null)) then
	:new.dt_prev_final := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prev_final || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

if (:new.cd_procedimento is null or :new.cd_procedimento = '') then
	:new.ie_origem_proced := null;
end if;

end;
/
