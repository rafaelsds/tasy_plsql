CREATE OR REPLACE TRIGGER GESTAO_VAGA_ANEXO_after_insert
AFTER INSERT ON GESTAO_VAGA_ANEXO
FOR EACH ROW
DECLARE

cd_pessoa_fisica_w		varchar2(10);

BEGIN

select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from 	gestao_vaga
where	nr_sequencia = :new.nr_seq_gestao;


gerar_evento_gestao_vaga_hist(cd_pessoa_fisica_w,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,:new.nr_seq_gestao);

END;
/
