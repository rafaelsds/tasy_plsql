CREATE OR REPLACE TRIGGER ctb_crit_rateio_item_update
BEFORE UPDATE ON ctb_criterio_rateio_item
FOR EACH ROW
DECLARE

ds_erro_w			varchar2(2000);

BEGIN

con_consiste_vigencia_conta(:new.cd_conta_contabil, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(217552,'DS_ERRO_W='||ds_erro_w);
end if;

END;
/

