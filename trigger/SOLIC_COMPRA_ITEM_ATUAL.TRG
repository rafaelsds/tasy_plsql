create or replace
trigger solic_compra_item_atual
before update on solic_compra_item
for each row
declare

cd_estabelecimento_w	number(4);
cd_comprador_resp_w	varchar2(10);
cd_perfil_w		number(5);
cd_operacao_estoque_w	requisicao_material.cd_operacao_estoque%type;
cd_motivo_baixa_w		sup_motivo_baixa_req.nr_sequencia%type;
ds_erro_w		varchar2(2000);
qt_pendentes_w		number(10);
nr_seq_motivo_cancel_w	solic_compra.nr_seq_motivo_cancel %type;
cd_comprador_w		comprador.cd_pessoa_fisica%type;
qt_registros_w		number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

	if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado. 
	end if;

	select	cd_estabelecimento,
		cd_comprador_resp,
		nr_seq_motivo_cancel
	into	cd_estabelecimento_w,
		cd_comprador_resp_w,
		nr_seq_motivo_cancel_w
	from	solic_compra
	where	nr_solic_compra = :new.nr_solic_compra;

	if	(cd_comprador_resp_w is null) or
		(cd_comprador_resp_w = '') then	
		
		select	obter_comprador_material(:new.cd_material, ie_urgente, cd_estabelecimento_w)
		into	cd_comprador_w
		from	solic_compra
		where	nr_solic_compra = :new.nr_solic_compra;
		
		select	count(*)
		into	qt_registros_w
		from	comprador
		where	cd_estabelecimento = cd_estabelecimento_w
		and	cd_pessoa_fisica = cd_comprador_w;
		
		if	(qt_registros_w > 0) then
			compras_pck.set_is_sci_insert('S');
			compras_pck.set_nr_seq_proj_rec(:new.nr_seq_proj_rec);
		
			update	solic_compra
			set	cd_comprador_resp = cd_comprador_w
			where	nr_solic_compra = :new.nr_solic_compra;
		end if;
	end if;

	cd_perfil_w	:= nvl(obter_perfil_ativo,0);

	if	(:old.dt_autorizacao is null) and
		(:new.dt_autorizacao is not null) and
		(cd_perfil_w > 0) then
		:new.cd_perfil_aprov := cd_perfil_w;
	end if;

	if	(:old.dt_autorizacao is not null) and
		(:new.dt_autorizacao is null) then
		:new.cd_perfil_aprov := '';
	end if;

end if;

compras_pck.set_nr_seq_proj_rec(null);
compras_pck.set_is_sci_insert('N');

end;
/
