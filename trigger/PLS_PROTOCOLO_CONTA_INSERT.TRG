create or replace
trigger pls_protocolo_conta_insert
before insert on pls_protocolo_conta
for each row

declare

ds_log_call_w	varchar2(1500);
ds_observacao_w	varchar2(4000);

begin
/* A vers�o TISS vem no XML, PTU ou pode ser informada em tela,
por�m temos que garantir que caso n�o informada, busque a vers�o vigente
pois h� v�rias vis�es de telas tratadas pela vers�o TISS */
if	(:new.cd_versao_tiss is null) then
	:new.cd_versao_tiss	:= pls_obter_versao_tiss;
end if;


if (:new.ie_apresentacao is null) then

	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
	ds_observacao_w := 'Protocolo: '||:old.nr_sequencia||' sendo inserido sem informa��o de tipo de apresenta��o';

	insert into plsprco_cta(
		nr_sequencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
		ds_log, ds_log_call, ds_funcao_ativa, 
		ie_aplicacao_tasy, nm_maquina, ie_opcao
	) values (
		plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
		sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROTOCOLO_CONTA', 
		ds_observacao_w, ds_log_call_w, obter_funcao_ativa, 
		'N', wheb_usuario_pck.get_machine, '0'
	);	

end if;

end;
/