create or replace trigger compl_pessoa_fisica_update_hl7
before update on compl_pessoa_fisica
for each row

declare

ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
qt_atendimento_w     number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then

	ds_sep_bv_w := obter_separador_bv;
	
	if	(nvl(:old.ds_endereco,'X') 	<> nvl(:new.ds_endereco,'X')) 	or
		(nvl(:old.ds_municipio,'X') <> nvl(:new.ds_municipio,'X')) 	or
		(nvl(:old.sg_estado,'X') 	<> nvl(:new.sg_estado,'X')) 	or
		(nvl(:old.cd_cep,'X') 		<> nvl(:new.cd_cep,'X')) 		or
		(nvl(:old.nr_ddi_telefone,'X') 	<> nvl(:new.nr_ddi_telefone,'X')) or
		(nvl(:old.nr_ddd_telefone,'X') 	<> nvl(:new.nr_ddd_telefone,'X')) or
		(nvl(:old.nr_telefone,'X') 	<> nvl(:new.nr_telefone,'X')) 	then
		begin
		
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ;
		gravar_agend_integracao(145, ds_param_integ_hl7_w);
		
	--HCOR ADT A08

		if (wheb_usuario_pck.is_evento_ativo(783) = 'S') then

			qt_atendimento_w	:= obter_qtd_atendimentos(:new.cd_pessoa_fisica);
		
			if (qt_atendimento_w > 0) then
				ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ;
				gravar_agend_integracao(783, ds_param_integ_hl7_w);
			end if;
		end if;

		-- For CareStream Interface, Japan
        call_interface_file(933, 'carestream_ris_japan_l10n_pck.patient_change_info( ' || :new.cd_pessoa_fisica || ', ''' || :new.nm_usuario || ''' , 0 , null, ' || obter_estabelecimento_ativo || ');' , :new.nm_usuario);  

		end;
	end if;
	
end if;

    --Outbound ADT_A08
	call_bifrost_content('patient.registration.update','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);

end;
/