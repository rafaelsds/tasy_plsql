create or replace trigger cma_pessoa_fisica_insert
before insert on pessoa_fisica
for each row

declare
nr_seq_person_name_w		person_name.nr_sequencia%type;
ie_person_enabled_w			varchar2(1) := 'N';

begin
ie_person_enabled_w := person_name_enabled();

if	(:new.nr_seq_person_name is null) and
	(ie_person_enabled_w = 'S') then
	begin
	select 	person_name_seq.nextval
	into	nr_seq_person_name_w
	from 	dual;

	insert into person_name (
		nr_sequencia,
		ds_type,
		ds_given_name,
		ds_family_name,
		ds_component_name_1,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values 	(
		nr_seq_person_name_w,	
		'main',
		:new.nm_primeiro_nome, 		
		:new.nm_sobrenome_pai, 		
		:new.nm_sobrenome_mae, 
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario);
	
	:new.nr_seq_person_name := nr_seq_person_name_w;
	end;
end if;
end;
/