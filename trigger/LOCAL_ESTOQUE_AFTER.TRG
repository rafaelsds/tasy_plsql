create or replace trigger local_estoque_after
after insert or update or delete on local_estoque
for each row

declare

reg_integracao_p	gerar_int_padrao.reg_integracao;

begin

if	(inserting) then
	reg_integracao_p.ie_operacao	:=	'I';
elsif	(updating) then
	reg_integracao_p.ie_operacao	:=	'A';
elsif	(deleting) then
	reg_integracao_p.ie_operacao	:=	'E';
end if;

reg_integracao_p.cd_estab_documento	:=	nvl(:new.cd_estabelecimento, :old.cd_estabelecimento); 
reg_integracao_p.ie_tipo_local	:=	nvl(:new.ie_tipo_local, :old.ie_tipo_local);

gerar_int_padrao.gravar_integracao('8', nvl(:new.cd_local_estoque, :old.cd_local_estoque), nvl(:new.nm_usuario, :old.nm_usuario), reg_integracao_p);

end local_estoque_after;
/