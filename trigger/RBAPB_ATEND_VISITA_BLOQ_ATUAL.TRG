create or replace trigger RBAPB_ATEND_VISITA_BLOQ_ATUAL
before INSERT OR UPDATE OR DELETE on ATENDIMENTO_VISITA_BLOQ
for each row

declare
-- integra��o Winspector

ds_caractere_enter_w	varchar2(10) := chr(13) || chr(10);

nm_pessoa_fisica_new_w	pessoa_fisica.nm_pessoa_fisica%type;
nm_pessoa_fisica_old_w	pessoa_fisica.nm_pessoa_fisica%type;

begin

if	(inserting) then
--						insert: PESOBSERVACAO := PESOBSERVACAO + chr(13) + OBTER_NOME_PF(:new.cd_pessoa) OU :new.NM_PESSOA_BLOQUEIO

	if	(:new.NM_PESSOA_BLOQUEIO is not null) then
		nm_pessoa_fisica_new_w	:= :new.NM_PESSOA_BLOQUEIO;
	elsif	(:new.CD_PESSOA_BLOQUEIO is not null) then
		select	max(nm_pessoa_fisica)
		into	nm_pessoa_fisica_new_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.CD_PESSOA_BLOQUEIO;
	end if;

	if	(nm_pessoa_fisica_new_w is not null) then -- s� atualiza se tiver algo pra atualizar
		update	wsp.PESSOAS@sgh.beneficencia.org set
			PESOBSERVACAO	= PESOBSERVACAO || ds_caractere_enter_w || nm_pessoa_fisica_new_w
		where	PIN		= obter_pessoa_atendimento(:new.nr_atendimento,'C');
	end if;

elsif	(updating) then
--						update: PESOBSERVACAO := replace(PESOBSERVACAO,
--									OBTER_NOME_PF(:old.cd_pessoa) OU :old.NM_PESSOA_BLOQUEIO, 
--									OBTER_NOME_PF(:new.cd_pessoa) OU :new.NM_PESSOA_BLOQUEIO)
	if	(:old.NM_PESSOA_BLOQUEIO is not null) then
		nm_pessoa_fisica_old_w	:= :old.NM_PESSOA_BLOQUEIO;
	elsif	(:old.CD_PESSOA_BLOQUEIO is not null) then
		select	max(nm_pessoa_fisica)
		into	nm_pessoa_fisica_old_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :old.CD_PESSOA_BLOQUEIO;
	end if;
	
	if	(:new.NM_PESSOA_BLOQUEIO is not null) then
		nm_pessoa_fisica_new_w	:= :new.NM_PESSOA_BLOQUEIO;
	elsif	(:new.CD_PESSOA_BLOQUEIO is not null) then
		select	max(nm_pessoa_fisica)
		into	nm_pessoa_fisica_new_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.CD_PESSOA_BLOQUEIO;
	end if;
	
	if	(nm_pessoa_fisica_old_w <> nm_pessoa_fisica_new_w) then -- s� atualiza se tiver algo pra atualizar
		update	wsp.PESSOAS@sgh.beneficencia.org set
			PESOBSERVACAO	= replace(PESOBSERVACAO, nm_pessoa_fisica_old_w, nm_pessoa_fisica_new_w)
		where	PIN		= obter_pessoa_atendimento(:new.nr_atendimento,'C');
	end if;

elsif	(deleting) then

--						delete: PESOBSERVACAO := replace(PESOBSERVACAO, chr(13) + 
--									OBTER_NOME_PF(:old.cd_pessoa) OU :old.NM_PESSOA_BLOQUEIO, 
--									'')


	if	(:old.NM_PESSOA_BLOQUEIO is not null) then
		nm_pessoa_fisica_old_w	:= :old.NM_PESSOA_BLOQUEIO;
	elsif	(:old.CD_PESSOA_BLOQUEIO is not null) then
		select	max(nm_pessoa_fisica)
		into	nm_pessoa_fisica_old_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :old.CD_PESSOA_BLOQUEIO;
	end if;

	update	wsp.PESSOAS@sgh.beneficencia.org set
		PESOBSERVACAO	= replace(
					replace(PESOBSERVACAO, nm_pessoa_fisica_old_w, ''), -- remove o nome do acompanhante
					ds_caractere_enter_w || ds_caractere_enter_w, ds_caractere_enter_w) -- remove as linhas em branco
	where	PIN		= obter_pessoa_atendimento(:old.nr_atendimento,'C');


end if;
end;
/