create or replace trigger lote_audit_hist_item_bef
before insert on lote_audit_hist_item
for each row
declare
pragma autonomous_transaction;
nm_usuario_resp_w varchar2(15);
begin

if	(:new.nm_usuario_previsto is null) then
	usuario_resp_pck.obter_usuario_responsavel(null,null,:new.nr_seq_propaci,:new.nr_seq_matpaci,null,null,null,null,null,null,:new.nm_usuario,nm_usuario_resp_w);
	:new.nm_usuario_previsto := nm_usuario_resp_w;
end if;



end;
/
