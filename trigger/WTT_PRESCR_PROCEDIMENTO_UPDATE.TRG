create or replace trigger wtt_prescr_procedimento_update
before update on prescr_procedimento
for each row

declare

ds_dir_destino_w	varchar2(255) := '/Wheb/imagens/'; --Substituir por um caminho para uma pasta onde exista a permi��o para a grava��o dos arquivos. Definidio no banco pelo DBA
arq_texto_w		utl_file.file_type;

ds_accnum_w		varchar2(255);
ds_cmd_w		varchar2(255);
ds_insurance_w		varchar2(255);
ds_modality_w		varchar2(255);
ds_patage_w		varchar2(255);
ds_patbd_w		varchar2(255);	
ds_patid_w		varchar2(255);
ds_patname_w		varchar2(255);
ds_patsex_w		varchar2(255);
ds_reqphy_w		varchar2(255);
ds_reqphyid_w		varchar2(255);
ds_stddate_w		varchar2(255);
ds_stdtime_w		varchar2(255);
ds_spslocation_w	varchar2(255);
ds_stddesc_w		varchar2(255);


begin

if	((:new.ie_suspenso = 'S') and((:old.ie_suspenso = 'N') or (:old.ie_suspenso is null)))then

	begin

	select	'DELETE' CMD,
		--elimina_acentuacao(p.cd_pessoa_fisica) PATID,
		obter_prontuario_paciente(p.cd_pessoa_fisica) PATID,
		elimina_acentuacao(p.nm_pessoa_fisica) PATNAME,
		p.ie_sexo	PATSEX,
		to_char(p.dt_nascimento, 'YYYYMMDD') PATBD,
		obter_idade(p.dt_nascimento,sysdate,'A') PATAGE,
		--:new.nr_acesso_dicom ACCNUM,
		--pm.nr_atendimento||'-'||decode(b.cd_tipo_procedimento,'34','1',b.cd_tipo_procedimento)||'/'||:new.nr_acesso_dicom ACCNUM,
		:new.nr_acesso_dicom ACCNUM,
		DECODE(CD_TIPO_PROCEDIMENTO, 
					'1', 'CR',
					'2', 'US',
					'3', 'CT',
					'4', 'MR',
					'5', 'MR',
					'34','CR') modality,
		elimina_acentuacao(ds_procedimento) STDDESC,
		to_char(:new.dt_prev_execucao, 'YYYYMMDD') SDTDATE,
		to_char(:new.dt_prev_execucao, 'HH24MISS') SDTTIME,
		elimina_acentuacao(substr(obter_nome_pf(m.cd_pessoa_fisica),1,255)) reqphy,
		m.nr_crm REQPHYID,
		elimina_acentuacao(s.ds_setor_atendimento) SPSLOCATION,
		elimina_acentuacao(substr(obter_nome_convenio(obter_convenio_atendimento(9316)),1,255)) INSURENCE
	into	ds_cmd_w,
		ds_patid_w,
		ds_patname_w,
		ds_patsex_w,
		ds_patbd_w,
		ds_patage_w,
		ds_accnum_w,
		ds_modality_w,
		ds_stddesc_w,
		ds_stddate_w,
		ds_stdtime_w,
		ds_reqphy_w,
		ds_reqphyid_w,
		ds_spslocation_w,
		ds_insurance_w
	from	setor_atendimento s,
		procedimento b,
		pessoa_fisica p,
		medico m,
		prescr_medica pm
	where	:new.cd_procedimento	= b.cd_procedimento
	and	:new.ie_origem_proced	= b.ie_origem_proced
	and	:new.cd_setor_Atendimento = s.cd_setor_atendimento
	and 	p.cd_pessoa_fisica	= pm.cd_pessoa_fisica
	and 	m.cd_pessoa_fisica	= pm.cd_medico
	and 	b.CD_TIPO_PROCEDIMENTO in ('1','2','3','4','5','34')
	and	:new.nr_prescricao	= pm.nr_prescricao;
	
	exception
	when no_data_found then
		null;
	end;

	
	if	(ds_cmd_w is not null) then
	
		arq_texto_w := utl_file.fopen(DS_DIR_DESTINO_W,ds_ACCNUM_w||'C.txt','W');
		utl_file.put_line(arq_texto_w,	'CMD='		|| ds_CMD_w		||chr(13)||chr(10)||
						'PATID='	|| ds_PATID_w		||chr(13)||chr(10)||	
						'PATNAME='	|| ds_PATNAME_w		||chr(13)||chr(10)||
						'PATSEX='	|| ds_PATSEX_w		||chr(13)||chr(10)||
						'PATBD='	|| ds_PATBD_w		||chr(13)||chr(10)||
						'PATAGE='	|| ds_PATAGE_w 		||chr(13)||chr(10)||
						'ACCNUM='	|| ds_ACCNUM_w		||chr(13)||chr(10)||
						'MODALITY='	|| ds_modality_w	||chr(13)||chr(10)||
						'STDDESC='	|| ds_STDDESC_w		||chr(13)||chr(10)||
						'STDDATE='	|| ds_STDDATE_w		||chr(13)||chr(10)||
						'STDTIME='	|| ds_STDTIME_w 	||chr(13)||chr(10)||
						'REQPHY='	|| ds_reqphy_w 		||chr(13)||chr(10)||
						'REQPHYID='	|| ds_REQPHYID_w	||chr(13) );
		utl_file.fflush(arq_texto_w);
		utl_file.fclose(arq_texto_w);	
	end if;
	

end if;

end;
/