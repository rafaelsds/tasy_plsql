CREATE OR REPLACE TRIGGER escala_stratify_atual BEFORE
    INSERT OR UPDATE ON escala_stratify
    FOR EACH ROW
DECLARE
    sql_w            VARCHAR(250);
    qt_score_w       NUMBER(3);
    ds_erro_w        VARCHAR2(4000);
    ds_parametros_w  VARCHAR2(4000);
BEGIN
    IF ( wheb_usuario_pck.get_ie_executar_trigger = 'S' ) THEN
        BEGIN
            sql_w := 'CALL OBTER_SCORE_STRATIFY_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11) INTO :qt_score_w';
            EXECUTE IMMEDIATE sql_w
                USING IN :new.ie_queda, IN :new.ie_queda_6_meses, IN :new.ie_confuso, IN :new.ie_agitado, IN :new.ie_desorientado,
                IN :new.ie_oculos, IN :new.ie_visao_turva, IN :new.ie_glaucoma, IN :new.ie_alteracao_diurese, IN :new.ie_mobilidade,
                IN :new.ie_transferencia, OUT qt_score_w;

        EXCEPTION
            WHEN OTHERS THEN
      ds_erro_w := sqlerrm;
      ds_parametros_w := (':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.cd_profissional: '||:new.cd_profissional||'-'||':new.ie_situacao: '||:new.ie_situacao||'-'||
                          ':new.ie_queda: '||:new.ie_queda||'-'||':new.ie_queda_6_meses: '||:new.ie_queda_6_meses||'-'||':new.ie_confuso: '||:new.ie_confuso||'-'||
                          ':new.ie_agitado: '||:new.ie_agitado||'-'||':new.ie_desorientado: '||:new.ie_desorientado||'-'||':new.ie_oculos: '||:new.ie_oculos||'-'||
                          ':new.ie_visao_turva: '||:new.ie_visao_turva||'-'||':new.ie_glaucoma: '||:new.ie_glaucoma||'-'||
                          ':new.ie_alteracao_diurese: '||:new.ie_alteracao_diurese||'-'||':new.ie_mobilidade: '||:new.ie_mobilidade||'-'||
                          ':new.ie_transferencia: '||:new.ie_transferencia||'-'||'qt_score_w: '||qt_score_w);
      gravar_log_medical_device ('escala_stratify_atual','OBTER_SCORE_STRATIFY_MD'
                                 ,ds_parametros_w,substr(ds_erro_w, 4000),:new.nm_usuario,'N');  
            
                :new.qt_score := NULL;
        END;

        :new.qt_score := qt_score_w;
    END IF;
END;
/