create or replace
trigger man_ordem_servico_insatisfacao
before insert on man_ordem_servico
for each row
begin

if (OBTER_SE_GERENTE_OU_SUBSTIT(wheb_usuario_pck.get_nm_usuario) <> 'S') AND (:NEW.DS_ANALISE_INSATISFACAO IS NOT NULL) then
	wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(857575);    
  
end if;

end;
/