create or replace trigger atendimento_acompanhante_atual
before insert or update on atendimento_acompanhante
for each row

declare
ie_atend_visit_bloq_w varchar(01) := '';
ie_atend_visit_lib_w varchar(01) := '';
ie_atend_visit_pessoa_w varchar(01) := '';
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then	
    begin
        select 'S'
        into	ie_atend_visit_bloq_w
        from	atendimento_acomp_bloq
        where	nr_atendimento	= :new.nr_atendimento
        and   (cd_pessoa_fisica	= :new.cd_pessoa_fisica or NM_PESSOA_FISICA = :new.nm_acompanhante)
        and   rownum = 1;
    exception
    when others then
        ie_atend_visit_bloq_w := 'N';
    end;
    if (ie_atend_visit_bloq_w = 'S') then    
        Wheb_mensagem_pck.exibir_mensagem_abort(1045155);
    end if;
    
    select	decode(count(*), 0, 'N', 'S')
    into	ie_atend_visit_lib_w
    from	acompanhante_visita_lib
    where	nr_atendimento	= :new.nr_atendimento
    and	(cd_pessoa_lib	is not null or nm_pessoa_liberacao is not null);
    
    select	decode(count(*), 0, 'S', 'N')
    into	ie_atend_visit_pessoa_w
    from	acompanhante_visita_lib a
    where	a.nr_atendimento	= :new.nr_atendimento
    and	(a.cd_pessoa_lib	= :new.cd_pessoa_fisica or upper(a.nm_pessoa_liberacao) = upper(:new.nm_acompanhante))
    and	not exists( select 	1 
                    from	atendimento_acomp_bloq b
                    where	(b.cd_pessoa_fisica	= :new.cd_pessoa_fisica or b.NM_PESSOA_FISICA = :new.nm_acompanhante)
                    and		a.nr_atendimento = b.nr_atendimento
                    );
    
    if	(ie_atend_visit_lib_w = 'S' and
        ie_atend_visit_pessoa_w = 'S') then
        wheb_mensagem_pck.exibir_mensagem_abort(1045155);
    end if;
    
    if 	(:new.nr_controle is not null) and
        ((:old.nr_controle is null) or (:new.nr_controle != :old.nr_controle)) then
        valida_cracha_integracao(:new.nm_usuario, somente_numero(:new.nr_controle));
    end if;
end if;        
    end;
/
