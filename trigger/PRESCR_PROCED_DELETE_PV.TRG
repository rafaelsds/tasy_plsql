CREATE OR REPLACE TRIGGER PRESCR_PROCED_DELETE_PV
BEFORE DELETE ON Prescr_Procedimento
FOR EACH ROW
DECLARE

ie_agrupada_w		pre_venda.ie_agrupada%type;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
qt_registros_w		number(10);

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

select	count(*)
into	qt_registros_w
from	pre_venda_item
where	nr_seq_interno = :old.nr_seq_interno;

if	(qt_registros_w > 0) then
	begin
	
	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = :old.nr_prescricao;
	
	select	nvl(max(a.ie_agrupada),'N')
	into	ie_agrupada_w
	from	pre_venda a,
		pre_venda_item b			
	where	a.nr_sequencia = b.nr_seq_pre_venda
	and	b.nr_prescricao = :old.nr_prescricao;

	if	(ie_agrupada_w = 'S') then
		select	count(*)
		into	qt_registros_w
		from	prescr_procedimento a,
			pre_venda_item b
		where	a.nr_seq_interno = b.nr_seq_interno
		and   	a.nr_prescricao = b.nr_prescricao
		and	b.nr_atendimento = nr_atendimento_w
		and	a.ie_suspenso = 'N'
		and	a.nr_seq_interno <> :old.nr_seq_interno;
		
		/*Se todos os itens foram suspensos, limpa o vinculo da prevenda com o atendimento e prescricao
		Mas faz isso somente se a prevenda for do tipo Agrupada*/
		if	(qt_registros_w = 0) then
			update	pre_venda_item
			set	nr_atendimento = null,
				nr_prescricao = null,
				nr_seq_interno = null
			where	nr_atendimento = nr_atendimento_w;
			commit;
		end if;
	else
		/*Desvincular a prescricao e o atendimento da prevenda, quando deleta*/
		update	pre_venda_item
		set	nr_atendimento = null,
			nr_prescricao = null,
			nr_seq_interno = null
		where	nr_seq_interno = :old.nr_seq_interno;
		commit;
	end if;	
	end;
end if;
end;
/
