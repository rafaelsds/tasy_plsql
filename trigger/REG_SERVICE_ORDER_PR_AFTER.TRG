create or replace trigger reg_service_order_pr_after
after insert on reg_service_order_pr
for each row

declare

ie_plataforma_w		man_ordem_servico.ie_plataforma%type;

begin

select	max(ie_plataforma)
into	ie_plataforma_w
from	man_ordem_servico
where	nr_sequencia = :new.nr_service_order;

if (ie_plataforma_w = 'H') then
	reg_test_plan_pck.add_pendency('S', :new.nr_service_order, null, :new.nm_usuario_nrec);
end if;

end;
/