create or replace trigger san_producao_delete
before delete on san_producao
for each row

declare

begin

if  ((:old.dt_liberacao is not null) or (:old.dt_fim_producao is not null)) and (:old.nr_seq_doacao is not null) then
	--N�o � poss�vel excluir um hemocomponente liberado ou finalizada a produ��o!
	Wheb_mensagem_pck.exibir_mensagem_abort(264222);
end if;


end;
/
