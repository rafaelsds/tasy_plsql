create or replace TRIGGER LOG_BASE_CORP
  AFTER UPDATE or INSERT or DELETE ON MAN_ORDEM_SERVICO
  FOR EACH ROW
DECLARE
  l_action varchar2(200);
  l_client_info varchar2(4000);
  l_session_user varchar2(200);
  l_host varchar2(200);
  l_module varchar2(200);
  l_program varchar2(200);
  l_osuser varchar2(200);
  l_operation varchar2(100);
  l_sid number;
  nr_sequencia_w number(10);

BEGIN
  CASE
    WHEN INSERTING THEN
      l_operation := 'INSERT';
    WHEN UPDATING THEN
      l_operation := 'UPDATE';
    WHEN DELETING THEN
      l_operation := 'DELETE';
  END CASE;
  l_sid:= to_number(sys_context('USERENV','SID'));
  EXECUTE IMMEDIATE 'SELECT action, client_info, username, osuser, module, program, machine FROM v$session WHERE sid = :b1'
  INTO l_action, l_client_info, l_session_user, l_osuser, l_module, l_program, l_host
  USING l_sid;

select	man_ordem_log_seq.NextVal
into	nr_sequencia_w
from	dual;
  
  insert into MAN_ORDEM_LOG (nr_sequencia,nr_seq_origem, nr_seq_origem_old,  DS_OPERATION, NR_SEQ_ORDEM,NR_SEQ_ORDEM_OLD,DS_ACAO,DS_CLIENT_INFO, DS_USER_SESSION, DS_HOST, DS_MODULO, NM_USUARIO, DS_COMANDO, dt_atualizacao, ds_program)     
  values(nr_sequencia_w,nvl(:NEW.nr_seq_origem,null),nvl(:OLD.nr_seq_origem,null) , l_operation,nvl(:NEW.nr_sequencia,null),nvl(:OLD.nr_sequencia,null),l_action, l_client_info, l_session_user, l_host, l_module, l_osuser, substr(dbms_utility.format_call_stack,1,4000), sysdate,l_program);
END;
/