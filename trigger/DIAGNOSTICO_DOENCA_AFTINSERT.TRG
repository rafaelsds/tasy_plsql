CREATE OR REPLACE TRIGGER DIAGNOSTICO_DOENCA_AFTINSERT
AFTER INSERT ON DIAGNOSTICO_DOENCA
FOR EACH ROW

DECLARE

nr_sequencia_w			dados_envio_cross.nr_sequencia%type;
ie_integra_cross_w		varchar2(1);
cd_classif_setor_cross_w 	number(10);
qt_conv_int_cross_w		number(10);
cd_Setor_atendimento_w	setor_Atendimento.cd_setor_atendimento%type;
nr_Seq_atepacu_w		atend_paciente_unidade.nr_Seq_interno%type;

begin

nr_Seq_atepacu_w := obter_atepacu_paciente(:new.nr_atendimento, 'IA');

select	max(cd_setor_Atendimento)
into	cd_Setor_atendimento_w
from	atend_paciente_unidade
where	nr_Seq_interno	= nr_Seq_atepacu_w;

cd_classif_setor_cross_w := obter_classif_setor_cross(cd_Setor_atendimento_w);
qt_conv_int_cross_w := obter_conv_int_cross(:new.nr_atendimento);

select  nvl(max(ie_integra_cross),'N') 
into	ie_integra_cross_w
from 	parametro_atendimento
where 	CD_ESTABELECIMENTO = wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_integra_cross_w = 'S') and 
	(obter_funcao_ativa = 916) and 
	(cd_classif_setor_cross_w > 0) and 
	(qt_conv_int_cross_w > 0) then
	--internacao
	gravar_integracao_cross(277, 'NR_ATENDIMENTO='|| :new.nr_atendimento || ';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
end if;

END;
/