create or replace trigger San_Producao_Update
BEFORE UPDATE ON San_Producao
FOR EACH ROW

declare
qt_procedimento_w		number(9,3);
nr_seq_reserva_w		number(10);
nr_sequencia_w			number(10);
qt_reg_w			number(1);
ie_alterar_usuario_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);	
dt_emprestimo_w			date;
ie_consite_data_emp_w		varchar2(1);
excluir_todos_proc_w		Varchar(1);
ie_realiza_nat_w		varchar2(1);
nr_seq_reserva_transf_old_w	number(10);
ie_status_hemocomp_res_w	varchar2(15);


BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


obter_param_usuario(450,72, obter_perfil_ativo, wheb_usuario_pck.GET_NM_USUARIO, 0, ie_alterar_usuario_w);
obter_param_usuario(450,91, obter_perfil_ativo, wheb_usuario_pck.GET_NM_USUARIO, 0, ie_consite_data_emp_w);
obter_param_usuario(450,257, obter_perfil_ativo, Wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, excluir_todos_proc_w);
/*Par�metro Hemoterapia, [489] - Status do hemocomponente na reserva ap�s cancelar seu uso na transfus�o */
obter_param_usuario(450,489, obter_perfil_ativo, Wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_status_hemocomp_res_w);

if	(ie_consite_data_emp_w = 'N') and
	(:new.nr_seq_emp_ent is not null) and
	(:new.dt_utilizacao is not null) and
	(:new.dt_utilizacao <> nvl(:old.dt_utilizacao,sysdate-1)) then
	
	select 	max(dt_emprestimo)
	into	dt_emprestimo_w
	from 	san_emprestimo
	where	nr_sequencia = :new.nr_seq_emp_ent;
	
	if	(:new.dt_utilizacao < dt_emprestimo_w) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192250);
	
	end if;	
end if;

if	(:new.nr_seq_transfusao is not null) and
	(:old.nr_seq_transfusao is null) then
	select	nvl(max(nr_seq_reserva),0)
	into	nr_seq_reserva_w
	from	san_transfusao
	where	nr_sequencia = :new.nr_seq_transfusao;

	if	(nr_seq_reserva_w <> 0) then
		update san_exame_lote
		set 	nr_seq_producao = :new.nr_sequencia,
			ie_origem = 'T'
		where nr_seq_res_prod = (select max(nr_sequencia)
					 from san_reserva_prod
					 where nr_seq_reserva	= nr_seq_reserva_w
					   and nr_seq_producao	= :new.nr_sequencia);
	end if;
	
	
	if (ie_alterar_usuario_w = 'S') then
		cd_pessoa_fisica_w := Obter_Dados_Usuario_Opcao(wheb_usuario_pck.GET_NM_USUARIO,'C');
	else
		cd_pessoa_fisica_w := :new.cd_pf_realizou;
	end if;
	
	if (cd_pessoa_fisica_w = '') then
		cd_pessoa_fisica_w := :new.cd_pf_realizou;
	end if;
	
	select	max(ie_realiza_nat)
	into	ie_realiza_nat_w
	from	san_doacao a
	where	a.nr_sequencia = :new.nr_seq_doacao;
	
	
	gerar_exame_realizado(2, :new.nr_sequencia, null, cd_pessoa_fisica_w, :new.nm_usuario,
				 :new.ie_irradiado, :new.ie_lavado, :new.ie_filtrado, :new.ie_aliquotado,:new.nr_seq_derivado, :new.nr_seq_doacao, null, ie_realiza_nat_w, :new.ie_aferese, wheb_usuario_pck.get_cd_estabelecimento);
elsif ((:new.nr_seq_reserva is not null) and
	 (:old.nr_seq_reserva is null)) or
	((:new.nr_seq_reserva is not null) and
	 (:old.nr_seq_reserva is not null) and
	 (:new.nr_seq_reserva <> :old.nr_seq_reserva)) then
	 
	select	max(ie_realiza_nat)
	into	ie_realiza_nat_w
	from	san_doacao a
	where	a.nr_sequencia = :new.nr_seq_doacao;
	 
	gerar_exame_realizado(9, :new.nr_sequencia, :new.nr_seq_reserva, cd_pessoa_fisica_w, :new.nm_usuario,
				 :new.ie_irradiado, :new.ie_lavado, :new.ie_filtrado, :new.ie_aliquotado,:new.nr_seq_derivado, :new.nr_seq_doacao, null, ie_realiza_nat_w, :new.ie_aferese, wheb_usuario_pck.get_cd_estabelecimento);
				 
elsif	(:new.nr_seq_transfusao is null) and
	(:old.nr_seq_transfusao is not null) then
	
	:new.ds_justificativa := null;
	
	delete san_exame_lote
	where nr_seq_producao = :new.nr_sequencia
	  and ie_origem = 'T'
	  and nr_seq_res_prod is null;

	if	(:new.nr_seq_doacao is not null) then
		update	san_exame_realizado a
		set	nr_seq_propaci = null
		where	a.nr_seq_exame_lote in (	select nr_sequencia
						from san_exame_lote x
						where nr_seq_doacao = :new.nr_seq_doacao)
		  and	a.nr_seq_propaci is not null;
		  
	elsif	(:new.nr_seq_emp_ent is not null) then
		update	san_exame_realizado a
		set	nr_seq_propaci = null
		where	a.nr_seq_exame_lote in (	select nr_sequencia
						from san_exame_lote x
						where nr_seq_producao = :new.nr_sequencia)
		  and	a.nr_seq_propaci is not null;
	end if;

	update san_exame_realizado
	set	nr_seq_propaci = null
	where	nr_seq_propaci is not null
	  and	nr_seq_exame_lote in (	select nr_sequencia
					from san_exame_lote
					where nr_seq_res_prod in (	select nr_sequencia
									from san_reserva_prod
									where nr_seq_producao = :new.nr_sequencia));
elsif	((:new.nr_seq_reserva is null) and
	 (:old.nr_seq_reserva is not null)) then
	delete san_exame_lote
	where nr_seq_producao = :new.nr_sequencia
	  and nr_seq_reserva = :old.nr_seq_reserva;

	update	san_exame_realizado a
	set	nr_seq_propaci = null
	where	a.nr_seq_exame_lote = (	select nr_sequencia
					from san_exame_lote x
					where nr_seq_doacao = :new.nr_seq_doacao)
	  and	a.nr_seq_propaci is not null;
end if;

if	(:new.nr_seq_transfusao is null) and
	(:old.nr_seq_transfusao is not null) then
	
	select	max(nr_seq_reserva)
	into	nr_seq_reserva_transf_old_w
	from 	san_transfusao
	where	nr_sequencia = :old.nr_seq_transfusao;
	
	update san_reserva_prod
	set	ie_status = ie_status_hemocomp_res_w,
		nr_seq_propaci = null	
	where nr_seq_producao = :new.nr_sequencia
	and  nr_seq_reserva = nr_seq_reserva_transf_old_w;

	if	(:new.nr_seq_propaci is not null) then
		:new.nr_seq_propaci := null;
	end if;
end if;

if	(:old.nr_seq_propaci is not null) and
	(:new.nr_seq_propaci is null) then
	select nvl(max(qt_procedimento),0)
	into	qt_procedimento_w
	from procedimento_paciente
	where nr_sequencia = :old.nr_seq_propaci;

	if	(qt_procedimento_w > 1) and (excluir_todos_proc_w = 'N') then
		update procedimento_paciente
		set qt_procedimento = qt_procedimento - 1
		where nr_sequencia = :old.nr_seq_propaci;

		update procedimento_paciente
		set qt_procedimento = qt_procedimento - 1
		where nr_seq_proc_princ = :old.nr_seq_propaci;
		
		update material_atend_paciente
		set qt_material = qt_material - 1
		where nr_seq_proc_princ = :old.nr_seq_propaci;
	else
		delete procedimento_paciente
		where nr_seq_proc_princ = :old.nr_seq_propaci;
		
		delete material_atend_paciente
		where nr_seq_proc_princ = :old.nr_seq_propaci;
		
		delete procedimento_paciente
		where nr_sequencia = :old.nr_seq_propaci;
	end if;
end if;

if	(:new.nr_seq_equipo > 0) and
	(:old.nr_seq_equipo <> :new.nr_seq_equipo) then
	san_gravar_log_equipamento(:new.nr_sequencia, :new.nr_seq_equipo, :new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/