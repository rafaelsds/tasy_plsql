CREATE OR REPLACE TRIGGER funcao_param_usuario_subject FOR
    DELETE OR INSERT OR UPDATE ON funcao_param_usuario
COMPOUND TRIGGER
    apply_sync BOOLEAN := false;
    
    TYPE changed_subjects_type IS TABLE OF funcao_param_usuario.nm_usuario_param%TYPE;
    changed_subjects changed_subjects_type := changed_subjects_type ();
    
    AFTER EACH ROW IS BEGIN
        apply_sync := psa_is_auth_parameter(:new.cd_funcao,:new.nr_sequencia) = 'TRUE' OR apply_sync;

        changed_subjects.extend;
        IF
            inserting OR updating
        THEN
            changed_subjects(changed_subjects.last) :=:new.nm_usuario_param;
        ELSE
            changed_subjects(changed_subjects.last) :=:old.nm_usuario_param;
        END IF;
    END AFTER EACH ROW;
    
    AFTER STATEMENT IS BEGIN
        IF
            apply_sync
        THEN
            FOR i IN 1..changed_subjects.count LOOP
                psa_synchronize_subject(changed_subjects(i));
            END LOOP;

        END IF;
    END AFTER STATEMENT;
END;
/