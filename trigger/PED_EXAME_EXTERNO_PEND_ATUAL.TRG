create or replace trigger ped_exame_externo_pend_atual
after insert or update or delete on PEDIDO_EXAME_EXTERNO
for each row

declare
qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_solic_externo_w	varchar2(10) := 'S';
nr_seq_reg_elemento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

begin

	if	(inserting) or
		(updating) then
		
		select	max(ie_lib_solic_externo)
		into	ie_lib_solic_externo_w
		from	parametro_medico
		where   cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

		if	(nvl(ie_lib_solic_externo_w,'N') = 'S') then
			if	(:new.dt_liberacao is null) then
				ie_tipo_w := 'PEE';
			elsif	(:old.dt_liberacao is null) and
					(:new.dt_liberacao is not null) then
				ie_tipo_w := 'XPEE';
			end if;


			if	(ie_tipo_w	is not null) then
				Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_reg_elemento_w);
			end if;

			
		end if;
	elsif	(deleting) then
		
		delete	from pep_item_pendente
		where 	IE_TIPO_REGISTRO = 'PEE'
		and	nr_seq_registro = :old.nr_sequencia
		and	nvl(IE_TIPO_PENDENCIA,'L')	= 'L';
		
	end if;

exception
when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;

end;
/
