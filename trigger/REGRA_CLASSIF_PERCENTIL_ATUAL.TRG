CREATE OR REPLACE TRIGGER regra_classif_percentil_atual
BEFORE INSERT or Update ON REGRA_CLASSIF_PERCENTIL
FOR EACH ROW

DECLARE

BEGIN

If (:new.ie_referencia in ('F','F3')) and
	(nvl(:new.QT_IDADE_MAX,75) > 74) Then
	
	Wheb_mensagem_pck.exibir_mensagem_abort(454747);
	
elsif (:new.ie_referencia = 'P') and
	    (nvl(:new.QT_IDADE_MIN,0) < 65) then

	Wheb_mensagem_pck.exibir_mensagem_abort(454748);	

elsif (:new.ie_referencia = 'F2') and
	    (nvl(:new.QT_IDADE_MAX,91) > 90) then

	Wheb_mensagem_pck.exibir_mensagem_abort(1053219);		
		
End if;

END;
/
