create or replace trigger prescr_procedimento_upd_tie
after update on prescr_procedimento
for each row

declare

ie_status_envio_w lab_parametro.ie_status_envio%type;
ie_integra_w varchar2(1);
jobno number;

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	begin
        if  (:old.ie_status_atend < 35) and (:new.ie_status_atend >= 35) then
            if (Obter_Equipamento_Exame(:new.nr_seq_exame, null, 'COVID19') is not null) then  
                send_Exam_Result_DataSus(:new.nr_prescricao, :new.nr_seq_exame, :new.NR_SEQUENCIA, 'COVID19');
            end if;
    
            send_oru_prescricao(:new.nr_prescricao, :new.nr_seq_exame, :new.nr_sequencia, :new.ie_status_atend);
        end if;
		
		SELECT decode(count(*),0,'N','S') ie_integra
		into ie_integra_w
		FROM exame_laboratorio     a,
		     lab_exame_equip       c
		WHERE a.nr_seq_exame = c.nr_seq_exame
		AND nvl(a.ie_anatomia_patologica, 'N') = 'N'
		AND obter_equipamento_exame(a.nr_seq_exame, NULL, lab_obter_conversao_externa(NULL, 'EQUIPAMENTO_LAB', 'CD_EQUIPAMENTO', c.cd_equipamento)) IS NOT NULL
		AND a.nr_seq_exame = :new.nr_seq_exame;
    
        if ((ie_integra_w = 'S') and (:new.ie_status_atend <= 30)) then
            select  b.ie_status_envio
            into    ie_status_envio_w
            from    lab_parametro b
            where   b.cd_estabelecimento = obter_estabelecimento_ativo;
    
            if (:old.ie_status_atend <> :new.ie_status_atend) and (:new.ie_status_atend = ie_status_envio_w) then
                dbms_job.submit(jobno, 'LAB_INTEGRA_REQUEST_SEND(' || :new.nr_prescricao ||','|| :new.nr_sequencia||','''||:new.nm_usuario||''');');
            end if;
        end if;
    exception
        when others then
            gravar_log_lab_pragma(6644, 'ERRO prescr_procedimento_upd_tie - '+sqlerrm, 'TASY', :new.nr_prescricao);
    end;
	end if;
end;
/