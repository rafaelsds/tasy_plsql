create or replace TRIGGER PACIENTE_OCORRENCIA_ATUAL
BEFORE INSERT OR UPDATE ON PACIENTE_OCORRENCIA
FOR EACH ROW
DECLARE

cd_estabelecimento_w	number(4);
nr_seq_evento_w		number(10);
cd_setor_paciente_w	number(10);
qt_idade_w		number(10);
qt_reg_w			number(1);

Cursor C01 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	   =	'LO'
	and	nvl(NR_SEQ_TIPO_OCORRENCIA,:new.NR_SEQ_TIPO_OCORRENCIA) = :new.NR_SEQ_TIPO_OCORRENCIA
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(cd_setor_atendimento,cd_setor_paciente_w)	= cd_setor_paciente_w
	and	nvl(ie_situacao,'A') = 'A';

BEGIN
qt_idade_w	:= nvl(obter_idade_pf(:new.CD_PESSOA_FISICA,sysdate,'A'),0);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.nr_seq_tipo_ocorrencia is not null) and
	(:new.dt_fim is not null) and
	(:old.dt_fim is null or :old.dt_fim <> :new.dt_fim)
	then
		:new.nm_usuario_termino := wheb_usuario_pck.get_nm_usuario;
end if;

if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.dt_fim is null) and
	(:old.dt_fim is not null)then
	:new.nm_usuario_termino := '';
end if;

if 	((:new.dt_liberacao is not null) and  (:old.dt_liberacao is null)) then
	begin
		cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
		begin
		cd_setor_paciente_w	:= obter_setor_atendimento(:new.nr_atendimento);
		exception
			when others then
			null;
		end;
		open C01;
			loop
			fetch C01 into
				nr_seq_evento_w;
			exit when C01%notfound;
				begin
				gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,:new.cd_pessoa_fisica,null,:new.nm_usuario,:new.ds_comentario,:new.dt_registro);
				end;
			end loop;
		close C01;
	end;
end if;

if	(:new.DT_FIM is not null) and
	(:old.DT_FIM is null) then
	:new.NM_USUARIO_TERMINO := obter_usuario_ativo;
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/
