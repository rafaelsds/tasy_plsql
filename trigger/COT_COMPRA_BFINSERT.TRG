create or replace trigger cot_compra_bfinsert
before insert on cot_compra
for each row

declare
nr_cot_compra_atual_w	cot_compra.nr_cot_compra%type;
nr_cot_compra_proximo_w	cot_compra.nr_cot_compra%type;
nr_cot_compra_livre_w		cot_compra.nr_cot_compra%type;
nr_cot_compra_aux_w		cot_compra.nr_cot_compra%type	:=	0;

qt_min_livre_faixa_w	number(5)	:=	50;
qt_min_livre_atual_w	number(5)	:=	5;

pragma autonomous_transaction;
begin

nr_cot_compra_atual_w	:=	:new.nr_cot_compra; 

select	min(nr_cot_compra)
into	nr_cot_compra_proximo_w
from	cot_compra
where	nr_cot_compra >= nr_cot_compra_atual_w;

if	((nr_cot_compra_proximo_w - nr_cot_compra_atual_w) < qt_min_livre_atual_w) then
	begin
	select	nr_cot_compra
 	  into	nr_cot_compra_livre_w
	  from (select nr_cot_compra,
			       nr_cot_compra_prox - nr_cot_compra - 1 qt_livre
		      from (select a.nr_cot_compra,
				           (select nvl(min(x.nr_cot_compra),9999999999)
				              from	cot_compra x
				             where	x.nr_cot_compra > a.nr_cot_compra) nr_cot_compra_prox
			          from	cot_compra a
			         where	nr_cot_compra > nr_cot_compra_atual_w)
		     where	nr_cot_compra_prox - nr_cot_compra - 1 > qt_min_livre_faixa_w
		     order by 1)
	 where	rownum = 1;
	
	while	(nr_cot_compra_aux_w < nr_cot_compra_livre_w) loop 
		begin
		select	cot_compra_seq.nextval
		into	nr_cot_compra_aux_w
		from	dual;
		end;
	end loop;
	
	end;
end if;

end;
/
