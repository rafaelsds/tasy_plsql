create or replace trigger pls_contestacao_proc_update
before insert or update on pls_contestacao_proc
for each row

declare
nr_seq_proc_w		number(15,0);
nr_seq_analise_w	number(15,0);

begin
select	max(b.nr_sequencia),
	max(a.nr_seq_analise)
into	nr_seq_proc_w,
	nr_seq_analise_w
from	pls_conta		a,
	pls_conta_proc		b
where	b.nr_seq_conta		= a.nr_sequencia
and	b.nr_sequencia		= :old.nr_seq_conta_proc;

if	((:new.vl_contestado > :old.vl_procedimento) and ((:old.vl_contestado <> :new.vl_contestado) or (:old.vl_procedimento <> :new.vl_procedimento))) then
	wheb_mensagem_pck.exibir_mensagem_abort(170908, 'NR_SEQ_ANALISE_W=' || nr_seq_analise_w ||';'||
							'NR_SEQ_PROC_W=' || nr_seq_proc_w);
end if;

if	((:new.qt_contestada > :old.qt_procedimento) and ((:old.qt_contestada <> :new.qt_contestada) or (:old.qt_procedimento <> :new.qt_procedimento))) then
	wheb_mensagem_pck.exibir_mensagem_abort(170909, 'NR_SEQ_ANALISE_W=' || nr_seq_analise_w ||';'||
							'NR_SEQ_PROC_W=' || nr_seq_proc_w);
end if;

end;
/