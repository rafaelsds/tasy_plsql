create or replace trigger pf_alteracao_before_insert
before insert or update on pessoa_fisica_alteracao
for each row
begin
    if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
        if (NVL(pkg_i18n.get_user_locale,'pt_BR') <> 'pt_BR') and
         (:new.ds_senha is not null) then        
            :new.ds_senha := ' ';
        end if;
    end if;
end;
/