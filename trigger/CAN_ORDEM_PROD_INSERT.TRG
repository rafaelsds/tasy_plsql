CREATE OR REPLACE TRIGGER CAN_ORDEM_PROD_INSERT
BEFORE INSERT ON CAN_ORDEM_PROD
FOR EACH ROW

declare
nr_atendimento_w	Number(10);
ds_origem_w		varchar2(4000);
cd_pessoa_fisica_atend_w			pessoa_fisica.cd_pessoa_fisica%type;

BEGIN

if	(:new.nr_prescricao is not null) then
	select	nvl(max(nr_atendimento),0)
	into	nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao	= :new.nr_prescricao;
	
	
	if	(nr_atendimento_w > 0) then
		:new.nr_atendimento	:= nr_atendimento_w;
	end if;
end if;

	:new.ds_stack_origem := substr(dbms_utility.format_call_stack,1,4000);

if	(:new.DT_GERACAO_ORDEM is null) then
	:new.DT_GERACAO_ORDEM := sysdate;
end if;

if (:new.nr_atendimento is not null) and
	(:new.cd_pessoa_fisica is not null) then
	
	select  max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_atend_w
	from 	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;
	
	if (:new.cd_pessoa_fisica <> nvl(cd_pessoa_fisica_atend_w,:new.cd_pessoa_fisica)) then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(1005227);
	end if;	
end if;

END;
/