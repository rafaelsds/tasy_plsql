create or replace trigger mxm_titulo_pagar_baixa_insert
after insert on mxm_titulo_pagar_baixa
for each row

declare

begin
insert into mxm_integracao(
	nr_sequencia,
	nr_seq_tab_orig,
	ie_tipo_integracao,
	dt_atualizacao,
	nm_usuario,
	ie_status,
	ie_acao)
values(	mxm_integracao_seq.nextval,
	:new.nr_sequencia,
	'BTP',
	sysdate,
	'Tasy',
	'P',
	'I');
end;
/