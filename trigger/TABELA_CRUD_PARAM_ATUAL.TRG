create or replace trigger tabela_crud_param_atual
before insert or update on tabela_crud_param
for each row 
declare

qt_records_w	number(10);

--Please, be careful
pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	select	count(*)
	into	qt_records_w
	from	tabela_crud_param
	where	nm_tabela		= :new.nm_tabela
	and	nr_seq_visao		= :new.nr_seq_visao
	and	nr_seq_obj_schematic	= :new.nr_seq_obj_schematic
	and	((cd_estabelecimento	= :new.cd_estabelecimento)	or (cd_estabelecimento is null 	and :new.cd_estabelecimento is null))
	and	((cd_perfil			= :new.cd_perfil) 		or (cd_perfil is null 		and :new.cd_perfil is null))
	and	((nm_usuario_param		= :new.nm_usuario_param) 	or (nm_usuario_param is null 	and :new.nm_usuario_param is null))
	and	nr_sequencia 		<> :new.nr_sequencia;

	if (qt_records_w > 0) then

		--J� existe uma regra cadastrada com esses atributos.
		wheb_mensagem_pck.exibir_mensagem_abort(991975);

	end if;

end if;

end;
/
