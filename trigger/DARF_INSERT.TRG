create or replace trigger darf_insert
before insert or update on darf
for each row

declare

begin

if (philips_param_pck.get_cd_pais = 13 and :new.cd_darf is null) then
	:new.cd_darf := '1';
end if;

end;
/