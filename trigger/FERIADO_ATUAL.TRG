CREATE OR REPLACE TRIGGER Feriado_Atual
before insert or update ON Feriado
FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= trunc(sysdate)
and	dt_agenda between trunc(:new.dt_feriado) and trunc(:new.dt_feriado) + 86399 / 86400;

:new.dt_feriado := trunc(:new.dt_feriado);

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/
