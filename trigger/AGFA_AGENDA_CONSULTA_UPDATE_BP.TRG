CREATE OR REPLACE TRIGGER AGFA_AGENDA_CONSULTA_UPDATE
before update on agenda_consulta
for each row

declare

ie_utiliza_agfa_w		varchar2(255);
ds_erro_w				varchar2(2000);

BEGIN
begin
Obter_Param_Usuario(869,31,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_utiliza_agfa_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:old.ie_status_agenda is not null) and
		(:new.ie_status_agenda is not null) and
		(:old.ie_status_agenda <> :new.ie_status_agenda) and
		(:new.ie_status_agenda = 'C') and -- cancelamento
		(ie_utiliza_agfa_w = 'S') then
		
		Gravar_Agfa_Siu_In(:new.cd_pessoa_fisica,
					NULL,
					NULL,
					:new.nm_usuario,
					wheb_usuario_pck.get_cd_estabelecimento,
					:new.nr_sequencia,
					NULL,
					:new.cd_medico_req,
					null,
					sysdate,
					:new.nr_minuto_duracao,
					null,
					:new.cd_agenda,
					'S15',
					:new.ie_autorizacao,
					:new.ds_observacao,
					:new.dt_agenda,
					null);
	end if;
end if;

exception
when others then
	ds_erro_w := substr(sqlerrm,1,2000);
	insert into log_tasy(dt_atualizacao,nm_usuario,cd_log,ds_log) values (sysdate,'AGFA',-33340,ds_erro_w);
end;

END;
/