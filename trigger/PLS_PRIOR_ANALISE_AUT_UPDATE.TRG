create or replace trigger pls_prior_analise_aut_update
before update on pls_prioridade_analise_aut
for each row

declare
qt_analise_w			number(10)	:= 0;

begin
if	(:new.ie_prioridade <> :old.ie_prioridade) then
	select	count(1)
	into	qt_analise_w
	from	pls_auditoria	a
	where	a.nr_seq_prioridade_analise	= :new.nr_sequencia;
	
	if	(qt_analise_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(253365);
	end if;
end if;

end;
/