create or replace trigger agenda_int_item_after_Atual
after insert or update on agenda_integrada_item
for each row

declare

cd_pessoa_fisica_w		varchar2(10);
qt_medico_item_w		number(10);
qt_medico_item_inserido_w	number(10);

begin

select	count(*)
into	qt_medico_item_w
from	ageint_medico_item
where	nr_seq_item	= :new.nr_sequencia;

if	(qt_medico_item_w	= 1) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	ageint_medico_item
	where	nr_seq_item			= :new.nr_sequencia
	and	nvl(ie_inserir_prof, 'N')	= 'S';
	
	if	(cd_pessoa_fisica_w is not null) then
		select	count(*)
		into	qt_medico_item_inserido_w
		from	agenda_integrada_prof_item
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	nr_seq_agenda_item	= :new.nr_sequencia;
		
		if	(qt_medico_item_inserido_w	= 0) then
			insert into agenda_integrada_prof_item
				(nr_sequencia, 
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_agenda_item,
				cd_pessoa_fisica)
			values
				(agenda_integrada_prof_item_seq.nextval,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario, 
				:new.nr_sequencia,
				cd_pessoa_fisica_w);
		end if;
	end if;
end if;

end;
/