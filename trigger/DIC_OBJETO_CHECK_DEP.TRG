create or replace trigger DIC_OBJETO_CHECK_DEP
after insert or delete or update on DIC_OBJETO
for each row

declare 

jobno number;

begin

if	(deleting) and
	(:old.ds_sql is not null) then
	dbms_job.submit(jobno, 'CHECK_DIC_OBJ_DEPEND('|| to_char(:old.nr_sequencia) || ',''DELETE'');');
elsif	((updating) or (inserting)) and
	((:new.ds_sql is not null) or (:old.ds_sql is not null and :new.ds_sql is null)) then
	dbms_job.submit(jobno, 'CHECK_DIC_OBJ_DEPEND('|| to_char(nvl(:new.nr_sequencia, :old.nr_sequencia)) || ',''UPDATE'');');	
end if;

end;
/