create or replace trigger pls_evento_movimento_del
before delete on pls_evento_movimento
for each row

declare
ie_origem_w		varchar2(1);
nr_seq_lote_pgto_w	number(15);
qt_rotina_w		number(15);


begin
select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= 	(select userenv('sessionid') 
			from 	dual)
and	username = 	(select username 
			from 	v$session 
			where 	audsid = (	select userenv('sessionid') 
						from dual))
and	(action like 'PLS_DESFAZER_LOTE_PAGAMENTO%'
or	action like 'PLS_CANCELAR_PAGTO_PRESTADOR%'
or	action like 'PLS_LIMPAR_PAGTO_ZERADOS_LOTE%');

if	(qt_rotina_w = 0) and
	(:old.nr_seq_lote_pgto is not null) then
	select	ie_origem,
		nr_seq_lote_pagamento
	into	ie_origem_w,
		nr_seq_lote_pgto_w
	from	pls_lote_evento a
	where	a.nr_sequencia = :old.nr_seq_lote;
	
	if	(ie_origem_w = 'A') and
		(nr_seq_lote_pgto_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(271814, 'NR_SEQ_LOTE_PGTO=' || :old.nr_seq_lote_pgto);
	end if;
end if;

end;
/