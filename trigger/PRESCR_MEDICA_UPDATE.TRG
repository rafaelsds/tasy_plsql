create or replace trigger prescr_medica_update
before update on prescr_medica
for each row

declare
ie_tipo_atendimento_w		number(05,0);
cd_convenio_w				convenio.cd_convenio%type;
cd_categoria_w				categoria_convenio.cd_categoria%type;
cd_setor_ps_w				number(05,0);
qt_setor_int_w				number(05,0);
dt_entrada_w				date;
nr_seq_exame_w				number(10);
cd_material_exame_w			varchar2(20);
nr_seq_mat_w				number(10,0);
qt_coleta_w					number(4,0);
qt_medico_w					number(10);
cd_estabelecimento_w		number(4);
nr_seq_grupo_w				number(10);
ie_padrao_amostra_w			varchar2(5);
VarObrigaMedico_w			varchar2(1);
cd_barras_w					varchar2(30);
qt_volume_padrao_w			number(5);
qt_tempo_padrao_w			number(4,2);
nr_seq_origem_w				number(6);
nr_seq_prescr_w				number(6);
nr_seq_prescr_proc_mat_w	number(10);
ie_sexo_w					varchar2(1);
cd_perfil_ativo_w			number(5);
ds_maq_user_w				varchar2(80);
ie_prescr_rn_w				varchar(1);
ds_prescr_rn_w				varchar2(255);
qt_reg_w					number(1);
ie_atualiza_estab_w			varchar2(03);
ds_origem_w					varchar2(1800);
hr_prescr_copia_w			varchar2(10);
cd_pessoa_cirurgia_w		varchar2(10);
ie_vincula_atend_w			varchar2(10);
nr_seq_interno_w			number(10);
ie_integracao_ativa_w		varchar2(1);
ie_considera_minuto_w		varchar2(1);
ie_nova_prescr_val_w		varchar2(1);
ie_exclui_atend_prescr_w	varchar2(1);
dt_rep_pt_w					date;
dt_rep_pt2_w				date;
dt_prim_setor_w				date;
ie_inicia_dia_plano_w		varchar2(1);
ie_atualiza_w				varchar2(1);
ie_atualizar_data_hemodi_w	varchar2(1);
ie_calcula_validade_hem_w	varchar2(1);
nr_seq_status_padrao_oft_w	number(10)	:= null;
ie_permite_guia_senha_atend_w	varchar2(1);
ie_recalc_val_w				varchar2(1);
ds_stack_w					varchar2(2000);
ds_alteracao_w				varchar2(1800);
ie_tipo_lib_w				varchar2(1) := 'M';
dt_liberacao_w				date := sysdate;
cont_w						number(10);
qt_exame_lab_w				number(10) := 0;
reg_integracao_w			gerar_int_padrao.reg_integracao;
ds_param_integ_hl7_w		varchar2(2000);

cursor c01 is
	select	a.nr_sequencia,
		a.nr_seq_exame,
		a.cd_material_exame,
		b.nr_seq_grupo,
		a.nr_seq_origem
	from	prescr_procedimento a,
		exame_laboratorio b
	where 	a.nr_prescricao = :new.nr_prescricao
	  and 	a.nr_seq_exame 	= b.nr_seq_exame
	  and 	a.nr_seq_exame 	is not null;

cursor c02 is
	select	distinct
		b.nr_sequencia,
		nvl(c.qt_coleta,1),
		nvl(b.qt_volume_padrao,0),
		nvl(b.qt_tempo_padrao,0)
	from	exame_lab_material c,
		material_exame_lab b
	where 	cd_material_exame_w = b.cd_material_exame
	  and 	c.nr_seq_material = b.nr_sequencia
	  and 	c.nr_seq_exame = nr_seq_exame_w
	  and 	(b.ie_volume_tempo = 'S' or nvl(c.qt_coleta,1) > 0)
	  and 	not exists (	select 	1
							from 	material_exame_lab y,
								prescr_proc_material x
							where 	x.nr_prescricao = :new.nr_prescricao
							and 	x.nr_seq_material = y.nr_sequencia
							and 	((x.nr_seq_grupo = nr_seq_grupo_w) or (ie_padrao_amostra_w in ('PM','PM11','PM13')))
							and 	y.cd_material_exame = cd_material_exame_w);

cursor c03 is --diagnostika anatomo
	select	nr_seq_interno
	from	prescr_procedimento
	where 	nr_prescricao = :new.nr_prescricao
	and 	nr_seq_proc_interno is not null
	and	obter_se_integr_proc_interno(nr_seq_proc_interno,1,1,ie_lado,:new.cd_estabelecimento) = 'S'
	and	cd_cgc_laboratorio = '55578504000185';

cursor c04 is --diagnostika colpo
	select	nr_seq_interno
	from	prescr_procedimento
	where 	nr_prescricao = :new.nr_prescricao
	and 	nr_seq_proc_interno is not null
	and	obter_se_integr_proc_interno(nr_seq_proc_interno,1,2,ie_lado,:new.cd_estabelecimento) = 'S'
	and	cd_cgc_laboratorio = '55578504000185';

cursor c05 is
	select	a.cd_procedimento,
		a.nr_seq_exame,
		a.nr_seq_interno
	from	prescr_procedimento a,
		exame_laboratorio b
	where 	a.nr_prescricao = :new.nr_prescricao
	  and 	a.dt_suspensao 	is null
	  and 	a.nr_seq_exame 	is not null
	  and 	a.nr_seq_exame 	= b.nr_seq_exame
	  and 	b.ie_anatomia_patologica = 'S';

begin

begin
Obter_Param_Usuario(-15, 6,obter_perfil_ativo, :new.nm_usuario, 0, ie_atualiza_estab_w);

wheb_assist_pck.set_informacoes_usuario(:new.cd_estabelecimento,obter_perfil_ativo,:new.nm_usuario);

wheb_assist_pck.obterValorParametroREP(972,ie_atualizar_data_hemodi_w);
wheb_assist_pck.obterValorParametroREP(1016,ie_calcula_validade_hem_w);

wheb_assist_pck.obterValorParametroREP(753,ie_nova_prescr_val_w);
if	(:new.ie_prescr_emergencia = 'S') then
	if	(ie_nova_prescr_val_w = 'S') then
		ie_nova_prescr_val_w	:= 'N';
	end if;
end if;

wheb_assist_pck.obterValorParametroREP(689,ie_considera_minuto_w);
if	(ie_considera_minuto_w = 'D') then
	select	nvl(max(ie_minutos_rep),'S')
	into	ie_considera_minuto_w
	from	setor_atendimento
	where	cd_setor_atendimento = :new.cd_setor_atendimento;
end if;

exception
when others then
	ie_atualiza_estab_w	:= 'N';
end;

if	(:new.nr_controle is not null) and
	(:old.nr_controle is null) then

	:new.dt_controle_prescr := sysdate;

end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(to_char(:new.dt_primeiro_horario,'ss') <> '00') then
	:new.dt_primeiro_horario	:= trunc(:new.dt_primeiro_horario,'mi') + 1/1440;
end if;

if	(:new.ie_recem_nato = 'S') then
	obter_se_pf_prescr_rn(:new.cd_estabelecimento, :new.cd_pessoa_fisica, :new.nm_usuario, ie_prescr_rn_w, ds_prescr_rn_w);
	if	(ie_prescr_rn_w = 'N') then
		-- #@DS_MENSAGEM#@
		Wheb_mensagem_pck.exibir_mensagem_abort(173258, 'DS_MENSAGEM=' || ds_prescr_rn_w);
	end if;
end if;

if	((:new.cd_medico <> :old.cd_medico) or
	 ((:new.cd_medico is null) and
	  (:old.cd_medico is not null))) then

	ds_stack_w := substr(dbms_utility.format_call_stack,1,1800);
	-- Excluir o log da trigger apos encontrar o dano da OS 1378860 - Solicitacao feita pela analista Claudiomar

	gravar_log_tasy(10007, substr('PRESCR_MEDICA_UPDATE ALTERACAO MEDICO :new.nr_prescricao : ' || :new.nr_prescricao 
						|| ' :new.cd_medico : ' || :new.cd_medico
						|| ' :old.cd_medico : ' || :old.cd_medico
						|| ' ds_stack_w : ' || ds_stack_w,1,1800), :new.nm_usuario);

	if	(:new.cd_medico is not null) then
		Consistir_impedimento_pf(:new.cd_medico,'REP',:new.nm_usuario);
	end if;
end if;

if	((:new.cd_prescritor is not null) and (:old.cd_prescritor is null)) or
	((:new.cd_prescritor is not null) and (:old.cd_prescritor <> :new.cd_prescritor)) then
	Consistir_impedimento_pf(:new.cd_prescritor,'REP',:new.nm_usuario);
end if;

if	(:new.dt_liberacao_farmacia is not null) and
	(:old.dt_liberacao_farmacia is not null) and
	(:new.dt_liberacao_farmacia <> :old.dt_liberacao_farmacia) then
	:new.dt_liberacao_farmacia	:= :old.dt_liberacao_farmacia;
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	begin

	select	nvl(max('S'),'N')
	into	ie_integracao_ativa_w
	from 	empresa_integr_dados
	where 	rownum = 1
	and	nr_seq_empresa_integr = 13
	and 	ie_situacao = 'A';

	if (ie_integracao_ativa_w = 'S') then
		open c03;
		loop
		fetch c03 into
			nr_seq_interno_w;
		exit when c03%notfound;
			begin
			gerar_dados_diagnostika(:new.nr_prescricao, nr_seq_interno_w, 1, :new.nm_usuario);--gera xml diagnostika anatomo
			end;
		end loop;
		close c03;

		open c04;
		loop
		fetch c04 into
			nr_seq_interno_w;
		exit when c04%notfound;
			begin
			gerar_dados_diagnostika(:new.nr_prescricao, nr_seq_interno_w, 2, :new.nm_usuario);--gera xml diagnostika colpo
			end;
		end loop;
		close c04;
	end if;

	end;
end if;

if 	(wheb_usuario_pck.is_evento_ativo(288) = 'S') and
		(((obter_valor_param_usuario(924, 1179, wheb_usuario_pck.Get_cd_perfil, wheb_usuario_pck.Get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento) = 'S') and
		(:old.dt_liberacao_medico is null and :new.dt_liberacao_medico is not null)) or
		((:old.dt_liberacao is null and :new.dt_liberacao is not null) and 
		(obter_valor_param_usuario(924, 1179, wheb_usuario_pck.Get_cd_perfil, wheb_usuario_pck.Get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento) <> 'S'))) then
	integrar_softlab_ws(288, :new.cd_pessoa_fisica, :new.nr_prescricao, null, null, :new.nm_usuario, 1, 'N');
end if;

if	((:new.dt_liberacao is not null) and	 (:old.dt_liberacao is null)) or
	((:new.dt_liberacao is null) and (:new.dt_liberacao_medico is not null) and	 (:old.dt_liberacao_medico is null)) then
	if	(nvl(:new.nr_seq_consulta_oft,0) > 0) then
		Obter_Param_Usuario(3010,72,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,nr_seq_status_padrao_oft_w);
		if	(nvl(nr_seq_status_padrao_oft_w,0) > 0) then
			begin

			update	oft_consulta
			set		nr_seq_status		=	decode(nr_seq_status_padrao_oft_w,null,nr_seq_status,nr_seq_status_padrao_oft_w)
			where	nr_sequencia		=	:new.nr_seq_consulta_oft;

			exception
			when others then
				nr_seq_status_padrao_oft_w := null;
			end;
		end if;
	end if;
end if;

if	(to_char(:new.dt_prescricao,'dd/mm/yyyy') = '30/12/1899') then
	--A data da prescricao informada nao e valida. #@DT_PRESCRICAO#@
	Wheb_mensagem_pck.exibir_mensagem_abort(173273, 'DT_PRESCRICAO=' || to_char(:new.dt_prescricao,'dd/mm/yyyy hh24:mi:ss'));
end if;

if	(:old.nr_atendimento is null) and
	(:new.nr_atendimento is not null) then
	update	can_ordem_prod
	set	nr_atendimento	= :new.nr_atendimento
	where	nr_prescricao	= :new.nr_prescricao
	and	nr_atendimento is null;
end if;

select	count(*)
into	qt_medico_w
from	medico
where	rownum = 1
and		cd_pessoa_fisica	= :new.cd_medico;

if	((qt_medico_w = 0) and 
	 ((:old.cd_medico <> :new.cd_medico) or 
	  ((:old.cd_medico is not null) and
	   (:new.cd_medico is null)))) then

	wheb_assist_pck.obterValorParametroREP(1037,VarObrigaMedico_w);
	if	(VarObrigaMedico_w = 'S') then
		--A pessoa informada no campo Medico nao esta cadastrada como medico.
		Wheb_mensagem_pck.exibir_mensagem_abort(173291);
	end if;
end if;

if	((:old.dt_liberacao is not null) or (:old.dt_liberacao_medico is not null)) and
	(:old.nr_atendimento is not null) and
	(:new.nr_atendimento is null) then
	obter_param_usuario(924, 1075, wheb_usuario_pck.Get_cd_perfil, wheb_usuario_pck.Get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_exclui_atend_prescr_w);
	if	(nvl(ie_exclui_atend_prescr_w,'N') = 'N') then
		--Uma prescricao liberada nao pode ser desvinculada de seu atendimento.
		Wheb_mensagem_pck.exibir_mensagem_abort(173292);
	end if;
end if;

if	(:new.cd_estabelecimento is not null) and
	(:new.nr_atendimento is not null) and
	(nvl(:old.nr_atendimento,0) <> :new.nr_atendimento) and
	(ie_atualiza_estab_w = 'N') then
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	if	(cd_estabelecimento_w <> :new.cd_estabelecimento) then
		--O estabelecimento da prescricao nao pode ser diferente do estabelecimento do atendimento. (Parametro [6] da funcao "Transferencia de gastos")
		Wheb_mensagem_pck.exibir_mensagem_abort(173297);
	end if;
end if;

if	(:new.cd_setor_atendimento is null) and
	(:new.nr_atendimento is not null) then
	select	nvl(max(ie_tipo_atendimento),0)
	into	ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	if	(ie_tipo_atendimento_w = 3) then
		select	max(a.cd_setor_atendimento)
		into	cd_setor_ps_w
		from	setor_atendimento b,
				atend_paciente_unidade a
		where	a.nr_atendimento		= :new.nr_atendimento
		and		a.cd_setor_atendimento	= b.cd_setor_atendimento
		and		b.cd_classif_setor		= 1;

		if	(cd_setor_ps_w is not null) then
			select	count(*)
			into	qt_setor_int_w
			from	setor_atendimento b,
					atend_paciente_unidade a
			where	rownum = 1
			and		a.nr_atendimento		= :new.nr_atendimento
			and		a.cd_setor_atendimento	= b.cd_setor_atendimento
			and		b.cd_classif_setor		in(3,4,8);

			if	(qt_setor_int_w = 0) then
				:new.cd_setor_atendimento	:= cd_setor_ps_w;
			end if;
		end if;
	end if;
end if;

if	(:new.cd_setor_atendimento is null) and
	(:old.nr_atendimento is null) and
	(:new.nr_atendimento is not null) then
	wheb_assist_pck.obterValorParametroREP(100,ie_vincula_atend_w);
	if	(ie_vincula_atend_w = 'S') then
		:new.cd_setor_atendimento	:= Obter_setor_atendimento(:new.nr_atendimento);
	end if;
end if;

if	(:new.cd_setor_atendimento is not null) and
	(:new.cd_setor_atendimento <> :old.cd_setor_atendimento) then
	begin
	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
	update	ap_lote
	set	cd_setor_ant		= :old.cd_setor_atendimento
	where	nr_prescricao		= :new.nr_prescricao
	and	ie_status_lote		= 'G';

	update	ap_lote
	set	cd_setor_atendimento	= :new.cd_setor_atendimento
	where	nr_prescricao		= :new.nr_prescricao
	and	ie_status_lote 		= 'G';
	fleury_limpa_validacao_item(:new.nr_prescricao, null);

	end;
end if;

if	(:new.dt_suspensao is not null) and
	(:old.dt_suspensao is null) then
	begin
	ds_maq_user_w	:= substr(obter_inf_sessao(0) ||' - ' || obter_inf_sessao(1),1,80);

	cd_perfil_ativo_w	:= obter_perfil_ativo;

	update	ap_lote
	set	dt_cancelamento		= :new.dt_suspensao,
		nm_usuario_cancelamento	= :new.nm_usuario_susp,
		ds_maquina_cancelamento 	= ds_maq_user_w,
		cd_perfil_cancel		= cd_perfil_ativo_w,
		ie_status_lote		= 'C'
	where	nr_prescricao		= :new.nr_prescricao
	and	ie_status_lote in ('G','A','D','E');
	end;
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	:new.dt_emissao_farmacia        := null;
end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	gerar_status_solucao(:new.nr_prescricao, nvl(:new.dt_inicio_prescr,sysdate),:new.nm_usuario, :new.cd_funcao_origem, :new.nr_horas_validade);
end if;

if	(:old.dt_liberacao_medico is null) and
	((nvl(:new.cd_funcao_origem,924) <> 950) or
	 (nvl(:new.ie_prescr_emergencia,'N') = 'S')) then

	 wheb_assist_pck.obterValorParametroREP(532,hr_prescr_copia_w);
	if	((hr_prescr_copia_w is null) or
		 (to_char(:new.dt_primeiro_horario,'hh24:mi') <> hr_prescr_copia_w)) then


		if	(ie_nova_prescr_val_w = 'N') or
			(((:new.nr_horas_validade <> :old.nr_horas_validade) and
			  (:old.nr_horas_validade is not null)) or
			  ((:new.dt_primeiro_horario <> :old.dt_primeiro_horario) or
			   (:new.dt_primeiro_horario is null))) then
			if	(:new.dt_primeiro_horario is not null) then
				if	(to_char(:new.dt_primeiro_horario,'hh24:mi') < to_char(:new.dt_prescricao,'hh24:mi')) then
					:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao+1,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
				else
					:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
				end if;
			else
				:new.dt_inicio_prescr := :new.dt_prescricao;
			end if;

		elsif	((:new.dt_prescricao <> :old.dt_prescricao) and
			 (:old.dt_prescricao is not null)) then
			if	(:new.dt_primeiro_horario is not null) then
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			else
				:new.dt_inicio_prescr := :new.dt_prescricao;
			end if;
		end if;

	else

		if	(ie_nova_prescr_val_w = 'N') or
			(((:new.nr_horas_validade <> :old.nr_horas_validade) and
			  (:old.nr_horas_validade is not null)) or
			  ((:new.dt_primeiro_horario <> :old.dt_primeiro_horario) or
			   (:new.dt_primeiro_horario is null))) then
			if	(:new.dt_primeiro_horario is not null) then
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			else
				:new.dt_inicio_prescr := :new.dt_prescricao;
			end if;
		elsif	((:new.dt_prescricao <> :old.dt_prescricao) and
			 (:old.dt_prescricao is not null)) then
			if	(:new.dt_primeiro_horario is not null) then --OS195107
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			else
				:new.dt_inicio_prescr := :new.dt_prescricao;
			end if;
		end if;
	end if;
end if;

if	(ie_nova_prescr_val_w = 'N') or
	((:new.nr_horas_validade <> :old.nr_horas_validade) and
	 (:old.nr_horas_validade is not null)) or
	  ((:new.dt_primeiro_horario <> :old.dt_primeiro_horario) or
	   (:new.dt_primeiro_horario is null)) then

	if	(nvl(:new.ie_hemodialise,'N') not in ('E','S')) or
		(ie_calcula_validade_hem_w	= 'S') then
		:new.dt_validade_prescr := :new.dt_inicio_prescr + nvl(:new.nr_horas_validade,24) / 24;
		if	(ie_considera_minuto_w = 'S') then
			:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'mi') - 1/86400;
		else
			:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'hh24') - 1/86400;
		end if;
	elsif	(ie_atualizar_data_hemodi_w = 'S') and
		(nvl(:old.nr_horas_validade,0) = nvl(:new.nr_horas_validade,0)) and
		(:old.dt_inicio_prescr	= :new.dt_inicio_prescr) and
		(:old.dt_validade_prescr is null) and
		(:new.dt_validade_prescr is not null) then

		:new.dt_validade_prescr := :new.dt_validade_prescr;
	else
		:new.dt_validade_prescr := null;
	end if;
elsif	((:new.dt_prescricao <> :old.dt_prescricao) and
		 (:old.dt_prescricao is not null)) then

	if	(nvl(:new.ie_hemodialise,'N') not in ('E','S')) or
		(ie_calcula_validade_hem_w	= 'S') then
		:new.dt_validade_prescr := :new.dt_inicio_prescr + nvl(:new.nr_horas_validade,24) / 24;
		if	(ie_considera_minuto_w = 'S') then
			:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'mi') - 1/86400;
		else
			:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'hh24') - 1/86400;
		end if;
	elsif	(ie_atualizar_data_hemodi_w = 'S') and
		(nvl(:old.nr_horas_validade,0) = nvl(:new.nr_horas_validade,0)) and
		(:old.dt_inicio_prescr	= :new.dt_inicio_prescr) and
		(:old.dt_validade_prescr is null) and
		(:new.dt_validade_prescr is not null) then

		:new.dt_validade_prescr := :new.dt_validade_prescr;
	else
		:new.dt_validade_prescr := null;
	end if;
end if;

/*Almir em 28/01/08 OS80755 */
if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	:new.nm_usuario_lib_enf        := :new.nm_usuario;
end if;

if	(:old.nr_seq_transcricao is null) and
	(:new.nr_seq_transcricao is not null) then

	update	transcricao_prescricao
	set	ie_status	= 'E'
	where	nr_sequencia	= :new.nr_seq_transcricao;

end if;

if	((:old.dt_liberacao_medico is not null) or (:old.dt_liberacao is not null)) and
	(:old.cd_setor_atendimento <> :new.cd_setor_atendimento) and
	(nvl(:old.ie_adep,'S') = 'E')then
	begin
	:new.ie_adep := 'N';
	end;
end if;

select	max(hr_inicio_prescricao)
into	dt_prim_setor_w
from	setor_atendimento
where	cd_setor_atendimento = :new.cd_setor_atendimento;

if	(dt_prim_setor_w is not null) and
	((:new.nr_horas_validade	<> :old.nr_horas_validade) or
	 (:new.dt_inicio_prescr		<> :old.dt_inicio_prescr) or
	 (nvl(:new.cd_setor_atendimento,0) <> nvl(:old.cd_setor_atendimento,0))) then

	select	nvl(max(vl_parametro),max(vl_parametro_padrao))
	into	ie_inicia_dia_plano_w
	from	funcao_parametro
	where	cd_funcao	= 950
	and	nr_sequencia	= 66;

	dt_rep_pt_w		:= trunc(:new.dt_inicio_prescr);

	dt_prim_setor_w		:= to_date(to_char(:new.dt_inicio_prescr,'dd/mm/yyyy') || ' ' || to_char(dt_prim_setor_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

	if	(:new.dt_inicio_prescr	< dt_prim_setor_w) then
		dt_rep_pt_w	:= trunc(:new.dt_inicio_prescr - 1);
		if	(:new.dt_validade_prescr > dt_prim_setor_w) then
			dt_rep_pt2_w	:= (dt_rep_pt_w + 1);
		else
			dt_rep_pt2_w	:= null;
		end if;
	elsif	(:new.nr_horas_validade	> 24) then
		dt_rep_pt2_w	:= (dt_rep_pt_w + 1);
	else
		dt_rep_pt2_w	:= null;
	end if;

	if	(ie_inicia_dia_plano_w = 'N') then
		dt_rep_pt_w := dt_rep_pt_w + 1;
		if	(dt_rep_pt2_w is not null) then
			dt_rep_pt2_w := dt_rep_pt2_w + 1;
		end if;
	end if;

	:new.dt_rep_pt	:= dt_rep_pt_w;
	:new.dt_rep_pt2	:= dt_rep_pt2_w;

	update	prescr_dieta
	set	dt_rep_pt	= :new.dt_rep_pt,
		dt_rep_pt2	= :new.dt_rep_pt2
	where	nr_prescricao	= :new.nr_prescricao;

elsif	(:new.dt_rep_pt is null) and
	(:new.dt_rep_pt2 is null) then

	:new.dt_rep_pt	:= trunc(nvl(:new.dt_inicio_prescr,:new.dt_prescricao));
	:new.dt_rep_pt2	:= trunc(nvl(:new.dt_validade_prescr,:new.dt_prescricao));

end if;

if	(:new.nr_horas_validade <> :old.nr_horas_validade) and
	(:new.Dt_liberacao_medico	is not null) and
	(obter_funcao_ativa <> 252) then

	wheb_assist_pck.obterValorParametroREP(722,ie_recalc_val_w);
	if	(ie_recalc_val_w = 'N') then
		-- Voce nao pode alterar a validade desta prescricao pois a mesma ja esta liberada!
		Wheb_mensagem_pck.exibir_mensagem_abort(185128);
	end if;
end if;

if	((:new.nr_doc_conv is not null) or (:new.cd_senha is not null)) then
	wheb_assist_pck.obterValorParametroREP(1018,ie_permite_guia_senha_atend_w);
	if	(ie_permite_guia_senha_atend_w = 'N') then

		if 	(obter_se_somente_numero(:new.nr_doc_conv) = 'S') then
			if	(:new.nr_atendimento = somente_numero(:new.nr_doc_conv)) then
				-- O numero da Guia nao pode ser igual ao numero do Atendimento.
				Wheb_mensagem_pck.exibir_mensagem_abort(223884);
			end if;
		end if;

		if 	(obter_se_somente_numero(:new.cd_senha) = 'S') then
			if	(:new.nr_atendimento = somente_numero(:new.cd_senha)) then
				-- O numero da Senha nao pode ser igual ao numero do Atendimento.
				Wheb_mensagem_pck.exibir_mensagem_abort(223885);
			end if;
		end if;
	end if;
end if;

if	(:new.dt_emissao_farmacia is null) and
	(:old.dt_emissao_farmacia is not null) then
	ds_stack_w := substr(dbms_utility.format_call_stack,1,1800);

	gravar_log_tasy(18,obter_desc_expressao(296208)/*'Prescr = '*/ ||' = '|| :new.nr_prescricao || ' | Call stack = ' || ds_stack_w,:new.nm_usuario);
end if;

if 	(nvl(:new.dt_entrega, :new.dt_prescricao) < :new.dt_prescricao) then
	-- A data de entrega nao pode ser menor que a data da prescricao.
	--  Data de entrega: #@DT_ENTREGA#@
	--  Data da prescricao: #@DT_PRESCRICAO#@
	Wheb_mensagem_pck.exibir_mensagem_abort(267493, 'DT_ENTREGA='||to_char(:new.dt_entrega, 'dd/mm/yyyy hh24:mi:ss')||';DT_PRESCRICAO='||to_char(:new.dt_prescricao, 'dd/mm/yyyy hh24:mi:ss'));
end if;

if	(:new.dt_entrada_unidade <> :old.dt_entrada_unidade) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_entrada_unidade(' || to_char(:old.dt_entrada_unidade, 'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_entrada_unidade, 'dd/mm/yyyy hh24:mi:ss') ||'); ',1,1800);
end if;

if	(nvl(:new.cd_setor_atendimento,0) <> nvl(:old.cd_setor_atendimento,0)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' cd_setor_atendimento(' || nvl(:old.cd_setor_atendimento,'0') || '/' || nvl(:new.cd_setor_atendimento,'0')||'); ',1,1800);
end if;

if	(nvl(:new.cd_estabelecimento,0) <> nvl(:old.cd_estabelecimento,0)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' cd_estabelecimento(' || nvl(:old.cd_estabelecimento,'0') || '/' || nvl(:new.cd_estabelecimento,'0')||'); ',1,1800);
end if;

if	(:old.dt_liberacao_farmacia is null) and (:new.dt_liberacao_farmacia is not null) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_liberacao_farmacia: ' || to_char(:new.dt_liberacao_farmacia, 'dd/mm/yyyy hh24:mi:ss'),1,1800);
end if;

if	(nvl(:new.dt_primeiro_horario,sysdate) <> nvl(:old.dt_primeiro_horario,sysdate)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_primeiro_horario(' || to_char(:old.dt_primeiro_horario,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_primeiro_horario,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(nvl(:new.dt_prescricao, sysdate) <> nvl(:old.dt_prescricao, sysdate)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_prescricao(' || to_char(:old.dt_prescricao,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_prescricao,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(:old.dt_suspensao is null) and (:new.dt_suspensao is not null) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_suspensao: ' || to_char(:new.dt_suspensao, 'dd/mm/yyyy hh24:mi:ss'),1,1800);
end if;

if	(nvl(:old.nr_atendimento,0) <> nvl(:new.nr_atendimento,0)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' nr_atendimento(OLD/NEW): (' || :old.nr_atendimento || '/' || :new.nr_atendimento || ')',1,1800);
end if;

if	(nvl(:new.dt_liberacao, sysdate - 1) <> nvl(:old.dt_liberacao, sysdate - 1)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_liberacao(' || to_char(:old.dt_liberacao,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_liberacao,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(nvl(:new.nm_usuario_lib_enf,'null') <> nvl(:old.nm_usuario_lib_enf,'null')) then
	ds_alteracao_w := substr(ds_alteracao_w || ' nm_usuario_lib_enf(' || nvl(:old.nm_usuario_lib_enf,'null') || '/' || nvl(:new.nm_usuario_lib_enf,'null')||'); ',1,1800);
end if;

if (nvl(:old.nr_horas_validade,0) <> nvl(:new.nr_horas_validade,0)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' nr_horas_validade(old/new): (' || :old.nr_horas_validade || '/' || :new.nr_horas_validade || ')',1,1800);
end if;

if	(nvl(:new.dt_validade_prescr, sysdate) <> nvl(:old.dt_validade_prescr, sysdate)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_validade_prescr(old/new): (' || to_char(:old.dt_validade_prescr,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_validade_prescr,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(nvl(:new.dt_inicio_analise_farm, sysdate - 1) <> nvl(:old.dt_inicio_analise_farm, sysdate - 1)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_inicio_analise_farm(' || to_char(:old.dt_inicio_analise_farm,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_inicio_analise_farm,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(nvl(:new.nm_usuario_analise_farm, 'null') <> nvl(:old.nm_usuario_analise_farm, 'null')) then
	ds_alteracao_w := substr(ds_alteracao_w || ' nm_usuario_analise_farm(' || nvl(:old.nm_usuario_analise_farm,'null') || '/' || nvl(:new.nm_usuario_analise_farm,'null')||'); ',1,1800);
end if;

if	(nvl(:new.ds_itens_prescr,'null') <> nvl(:old.ds_itens_prescr,'null')) then
	ds_alteracao_w := substr(ds_alteracao_w || ' ds_itens_prescr(' || nvl(:old.ds_itens_prescr,'null') || '/' || nvl(:new.ds_itens_prescr,'null')||'); ',1,1800);
end if;

if	(ds_alteracao_w is not null) then
	ds_alteracao_w := obter_desc_expressao(296208)/*'Prescricao - '*/ ||' - '|| :new.nr_prescricao || ' - ' || ';Func=' || obter_funcao_ativa || ' - ' || ds_alteracao_w || ' - ' || substr(dbms_utility.format_call_stack,1,1800);
    gravar_log_tasy(-5, substr(ds_alteracao_w,1,1800), :new.nm_usuario);
end if;

select	count(b.nr_sequencia)
into	cont_w
from	intpd_eventos a,
		intpd_eventos_sistema b
where	a.nr_sequencia = b.nr_seq_evento
and		a.ie_evento = 143
and		a.ie_situacao = 'A'
and		b.ie_situacao = 'A'
and		rownum = 1;

if ((cont_w = 1) and
		(:new.dt_liberacao is not null) and
		(:old.dt_liberacao is null)) then

	select Count(1)
	into qt_exame_lab_w
	from exame_laboratorio a, prescr_procedimento b
	where b.nr_prescricao = :new.nr_prescricao
	and b.dt_suspensao is null
	and b.nr_seq_exame is not null
	and a.ie_anatomia_patologica = 'N'
	and a.nr_seq_exame = b.nr_seq_exame;

	if (qt_exame_lab_w > 0) then

		select	max(ie_tipo_atendimento),
			max(obter_convenio_atendimento(nr_atendimento)),
			max(obter_categoria_atendimento(nr_atendimento)),
			max(cd_estabelecimento)
		into	ie_tipo_atendimento_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_estabelecimento_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

		reg_integracao_w.cd_estab_documento := cd_estabelecimento_w;
		reg_integracao_w.cd_convenio := cd_convenio_w;
		reg_integracao_w.cd_categoria := cd_categoria_w;
		reg_integracao_w.ie_tipo_atendimento := ie_tipo_atendimento_w;
		gerar_int_padrao.gravar_integracao('143', :new.nr_prescricao, :new.nm_usuario, reg_integracao_w);

	end if;
end if;

select	count(b.nr_sequencia)
into	cont_w
from	intpd_eventos a,
		intpd_eventos_sistema b
where	a.nr_sequencia = b.nr_seq_evento
and		a.ie_evento = 174
and		a.ie_situacao = 'A'
and		b.ie_situacao = 'A'
and		rownum = 1;

if ((cont_w = 1) and
	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null)) then

	select	max(ie_tipo_atendimento),
		max(obter_convenio_atendimento(nr_atendimento)),
		max(obter_categoria_atendimento(nr_atendimento)),
		max(cd_estabelecimento)
	into	ie_tipo_atendimento_w,
		cd_convenio_w,
		cd_categoria_w,
		cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	for c_05_w in c05 loop

		reg_integracao_w.cd_estab_documento := cd_estabelecimento_w;
		reg_integracao_w.cd_convenio := cd_convenio_w;
		reg_integracao_w.cd_categoria := cd_categoria_w;
		reg_integracao_w.cd_procedimento := c_05_w.cd_procedimento;
		reg_integracao_w.nr_seq_exame := c_05_w.nr_seq_exame;
		reg_integracao_w.ie_tipo_atendimento := ie_tipo_atendimento_w;
		gerar_int_padrao.gravar_integracao('174', c_05_w.nr_seq_interno, :new.nm_usuario, reg_integracao_w);

	end loop;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/