create or replace trigger pe_prescr_proc_horario
before insert or update on pe_prescr_proc
for each row

declare
qt_reg_w	number(10);

begin

begin	
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (:new.DT_PRIMEIRO_HORARIO IS NOT NULL AND (:new.DT_PRIMEIRO_HORARIO <> :old.DT_PRIMEIRO_HORARIO OR :old.DT_PRIMEIRO_HORARIO IS NULL)) then

	:new.HR_PRIM_HORARIO := LPAD(PKG_DATE_UTILS.extract_field('HOUR', :new.dt_primeiro_horario), 2, 0) || ':' || LPAD(PKG_DATE_UTILS.extract_field('MINUTE', :new.dt_primeiro_horario), 2, 0);

elsif (:new.HR_PRIM_HORARIO is not null and (:new.HR_PRIM_HORARIO <> :old.HR_PRIM_HORARIO OR :old.HR_PRIM_HORARIO IS NULL))then

    :new.dt_primeiro_horario := PKG_DATE_UTILS.get_time(:new.dt_atualizacao, :new.HR_PRIM_HORARIO || ':00');

end if;

if (:new.HR_HORARIO_ESPEC is null) then

		:new.dt_horario_espec := null;

elsif ((:new.HR_HORARIO_ESPEC <> :old.HR_HORARIO_ESPEC) or
	((:new.HR_HORARIO_ESPEC is not null) and (:old.HR_HORARIO_ESPEC is null))) then

		:new.dt_horario_espec :=  to_date(to_char(sysdate,'dd/mm/yyyy') ||':'|| :new.HR_HORARIO_ESPEC,'dd/mm/yyyy hh24:mi');

end if;

exception
	when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;

end;
/
