create or replace trigger pls_regra_tx_opme_atual
before insert or update on pls_regra_tx_opme
for each row

/* esta trigger foi criada para alimentar o campo de data  fim de vigencia de referencia, isto por quest�es de performance
 para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
o campo fim vigencia ref � alimentado com o campo fim ou se o mesmo for nulo
 � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
sem precisar se preocupar se o campo vai estar nulo
*/

declare

begin

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end;
/