CREATE OR REPLACE TRIGGER PFCS_AFTER_UPT_SETOR_ATEND
AFTER UPDATE ON SETOR_ATENDIMENTO
FOR EACH ROW

DECLARE
ds_log_w    varchar2(4000);
qt_reg_w    number(1);

BEGIN
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
        goto Final;
    end if;

    if (:old.nm_usuario_nrec = 'PFCS') then
        ds_log_w := (
            concat('cd_setor_atendimento: ',                :old.cd_setor_atendimento || CHR(10))
            || concat('ds_setor_atendimento: ',             :old.ds_setor_atendimento || CHR(10))
            || concat('cd_estabelecimento: ',               :old.cd_estabelecimento || CHR(10))
            || concat('old cd_classif_setor: ',             :old.cd_classif_setor)
            || concat(' | new cd_classif_setor: ',          :new.cd_classif_setor || CHR(10))
            || concat('old ie_semi_intensiva: ',            nvl(:old.ie_semi_intensiva, 'null'))
            || concat(' | new ie_semi_intensiva: ',         nvl(:new.ie_semi_intensiva, 'null') || CHR(10))
            || concat('old ie_situacao: ',                  :old.ie_situacao)
            || concat(' | new ie_situacao: ',               :new.ie_situacao || CHR(10))
            || concat('old ie_ocupacao_hospitalar: ',       nvl(:old.ie_ocup_hospitalar, 'null'))
            || concat(' | new ie_ocupacao_hospitalar: ',    nvl(:new.ie_ocup_hospitalar, 'null'))
        );

        pfcs_log_pck.pfcs_insert_org_struc_log(
            ie_type_p => 'UPDATE',
            nm_table_p => 'SETOR_ATENDIMENTO',
            ds_log_p => ds_log_w,
            nm_usuario_p => :new.nm_usuario
        );
    end if;

<<Final>>
qt_reg_w := 0;
END;
/
