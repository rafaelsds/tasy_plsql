create or replace trigger funcao_popup_regra_atual
before insert or update on funcao_popup_regra
for each row 
declare

qt_records_w	number(10);

--Please, be careful
pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	select	count(*)
	into	qt_records_w
	from	funcao_popup_regra
	where	nr_seq_objeto		= :new.nr_seq_objeto
	and	((cd_estabelecimento	= :new.cd_estabelecimento) 	or (cd_estabelecimento is null 	and :new.cd_estabelecimento is null))
	and	((cd_perfil		= :new.cd_perfil) 		or (cd_perfil is null 		and :new.cd_perfil is null))
	and	((nm_usuario_regra	= :new.nm_usuario_regra) 	or (nm_usuario_regra is null 	and :new.nm_usuario_regra is null))
	and	nr_sequencia 		<> :new.nr_sequencia;

	if (qt_records_w > 0) then

		--J� existe uma regra cadastrada com esses atributos.
		wheb_mensagem_pck.exibir_mensagem_abort(991975);

	end if;

end if;

end;
/
