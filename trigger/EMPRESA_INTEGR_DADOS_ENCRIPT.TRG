create or replace
TRIGGER EMPRESA_INTEGR_DADOS_ENCRIPT
before insert or update ON EMPRESA_INTEGR_DADOS
for each row
BEGIN
if (nvl(:new.ds_ftp_senha,0) <> nvl(:old.ds_ftp_senha,0) ) then
	:new.ds_ftp_senha := WHEB_SEGURANCA.ENCRYPT(:new.ds_ftp_senha, 'XxFtpPassxX');
end if;	

if (nvl(:new.ds_senha,0) <> nvl(:old.ds_senha,0)) then
	:new.ds_senha := WHEB_SEGURANCA.ENCRYPT(:new.ds_senha, 'XxPasswordxX');
end if;

END;

/