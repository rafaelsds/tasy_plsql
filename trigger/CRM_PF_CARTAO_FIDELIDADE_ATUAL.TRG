create or replace trigger CRM_PF_CARTAO_FIDELIDADE_ATUAL
before insert or update on PF_CARTAO_FIDELIDADE
for each row


declare

ie_status_w		varchar2(01) := 1;


/*Dados pessoa*/
ie_funcionario_w		varchar2(01);
nm_pessoa_fisica_w	varchar2(60);
ie_sexo_w		varchar2(01);
dt_nascimento_w		date;
nr_identidade_w		varchar2(15);
nr_cpf_w			varchar2(11);
nr_telefone_celular_w	varchar2(40);
ie_estado_civil_w		varchar2(02);
nr_prontuario_w		number(10,0);
dt_atual_nrec_w		date;
dt_admissao_hosp_w	date;
cd_cargo_w		number(10,0);
dt_demissao_hosp_w	date;
qt_existe_w		number(10,0);


/*Dados complemento*/
ds_endereco_w		varchar2(100);
ds_bairro_w		varchar2(40);
ds_municipio_w		varchar2(40);
sg_estado_w		varchar2(2);
cd_cep_w		varchar2(15);
nr_telefone_w		varchar2(255);
nr_telefone_comercial_w	varchar2(255);
nr_seq_pais_w		number(10,0);
ds_email_w		varchar2(40);
nr_endereco_w		number(05,0);
ds_complemento_w		varchar2(40);
cd_profissao_w		number(10,0);

/*Dados m�dico*/
ie_medico_w		varchar2(01)	:= 'N';
ie_corpo_assistencial_w	varchar2(01);
ie_corpo_clinico_w		varchar2(01);
ie_retaguarda_w		varchar2(01);
ie_tsa_w			varchar2(01);
ie_coordenador_w		varchar2(01);
ie_vinculo_medico_w	number(02,0);
nr_seq_categoria_w	number(10,0);
nr_crm_w			varchar2(20);

/*Dados usu�rio*/
cd_setor_atendimento_w	number(05,0);


/*Carteira usu�rio*/
nr_seq_cartao_fid_w	number(10,0);
dt_inicio_valid_cartao_fid_w	date;
dt_fim_valid_cartao_fid_w	date;


ie_paciente_hospital_w	varchar2(01) := 'N';
ie_paciente_w		varchar2(01) := 'N';

begin

begin
select	ie_funcionario,
	nm_pessoa_fisica,
	ie_sexo,
	dt_nascimento,
	nr_identidade,
	nr_cpf,
	nr_telefone_celular,
	ie_estado_civil,
	nr_prontuario,
	nvl(dt_atualizacao_nrec,sysdate),
	dt_admissao_hosp,
	decode(ie_funcionario,'S',cd_cargo,''),
	dt_demissao_hosp
into	ie_funcionario_w,
	nm_pessoa_fisica_w,
	ie_sexo_w,
	dt_nascimento_w,
	nr_identidade_w,
	nr_cpf_w,
	nr_telefone_celular_w,
	ie_estado_civil_w,
	nr_prontuario_w,
	dt_atual_nrec_w,
	dt_admissao_hosp_w,
	cd_cargo_w,
	dt_demissao_hosp_w
from	pessoa_fisica
where	cd_pessoa_fisica = :new.cd_pessoa_fisica;	
exception
when others then
	ie_funcionario_w 		:= '';
	nm_pessoa_fisica_w	:= '';
	ie_sexo_w		:= '';
	dt_nascimento_w		:= '';
	nr_identidade_w		:= '';
	nr_cpf_w			:= '';
	nr_telefone_celular_w	:= '';
	ie_estado_civil_w		:= '';
	nr_prontuario_w		:= '';
	dt_atual_nrec_w		:= '';
	dt_admissao_hosp_w	:= '';
	cd_cargo_w		:= '';
	dt_demissao_hosp_w	:= '';
end;

begin
select	'S',
	ie_corpo_assist,
	ie_corpo_clinico,
	ie_retaguarda,
	ie_tsa,
	ie_coordenador,
	ie_vinculo_medico,
	nr_seq_categoria,
	nr_crm
into	ie_medico_w,
	ie_corpo_assistencial_w,
	ie_corpo_clinico_w,
	ie_retaguarda_w,
	ie_tsa_w,
	ie_coordenador_w,
	ie_vinculo_medico_w,
	nr_seq_categoria_w,
	nr_crm_w
from	medico
where	cd_pessoa_fisica = :new.cd_pessoa_fisica
and	rownum < 2;
exception
when others then
	ie_medico_w 		:= 'N';
	ie_corpo_assistencial_w	:= 'N';
	ie_corpo_clinico_w		:= 'N';
	ie_retaguarda_w		:= 'N';
	ie_tsa_w			:= 'N';
	ie_coordenador_w		:= 'N';
	ie_vinculo_medico_w	:= '';
	nr_seq_categoria_w	:= '';
	nr_crm_w			:= '';
end;

begin
select	ds_email,
	ds_endereco,
	nr_endereco,
	ds_complemento,
	cd_cep,
	nr_seq_pais,
	sg_estado,
	ds_municipio,
	nr_telefone
into	ds_email_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	cd_cep_w,
	nr_seq_pais_w,
	sg_estado_w,
	ds_municipio_w,
	nr_telefone_w
from	compl_pessoa_fisica	
where	cd_pessoa_fisica 	= :new.cd_pessoa_fisica
and	ie_tipo_complemento 	= 1
and	rownum < 2;
exception
when others then
	ds_email_w		:= '';
	ds_endereco_w		:= '';
	nr_endereco_w		:= '';
	ds_complemento_w		:= '';
	cd_cep_w		:= '';
	nr_seq_pais_w		:= '';
	sg_estado_w		:= '';
	ds_municipio_w		:= '';
	nr_telefone_w		:= '';
end;


begin
select	cd_profissao,
	nr_telefone
into	cd_profissao_w,
	nr_telefone_comercial_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica 	= :new.cd_pessoa_fisica
and	ie_tipo_complemento 	= 2
and	rownum < 2;
exception
when others then
	cd_profissao_w		:= '';
	nr_telefone_comercial_w	:= '';
end;


begin
select	cd_setor_atendimento
into	cd_setor_atendimento_w
from	usuario
where	cd_pessoa_fisica = :new.cd_pessoa_fisica
and	rownum < 2;
exception
when others then
	cd_setor_atendimento_w := null;
end;

nr_seq_cartao_fid_w	:= :new.nr_seq_cartao;
dt_inicio_valid_cartao_fid_w	:= :new.dt_inicio_validade;
dt_fim_valid_cartao_fid_w	:= :new.dt_fim_validade;

begin
select	'S'
into	ie_paciente_w
from	atendimento_paciente
where	cd_pessoa_fisica = :new.cd_pessoa_fisica
and	rownum < 2;
exception
when others then
	ie_paciente_w := 'N';
end;


begin
select	'S'
into	ie_paciente_hospital_w
from	atendimento_paciente
where	cd_pessoa_fisica = :new.cd_pessoa_fisica
and	dt_alta is null
and	rownum < 2;
exception
when others then
	ie_paciente_hospital_w := 'N';
end;

CRM_gerar_pessoa_contato(	ie_paciente_w,
			ie_medico_w,
			ie_funcionario_w,
			nm_pessoa_fisica_w,
			:new.cd_pessoa_fisica,
			ds_email_w,
			ds_endereco_w,
			nr_endereco_w,
			ds_complemento_w,
			cd_cep_w,
			nr_seq_pais_w,
			sg_estado_w,
			'',
			ds_municipio_w,
			ie_sexo_w,
			dt_nascimento_w,
			nr_identidade_w,
			nr_cpf_w,
			nr_telefone_w,
			nr_telefone_celular_w,
			'',
			nr_telefone_comercial_w,
			ie_estado_civil_w,
			cd_profissao_w,
			nr_prontuario_w,
			dt_atual_nrec_w,
			nr_crm_w,
			dt_admissao_hosp_w,
			'',
			'',
			'',
			'',
			cd_setor_atendimento_w,
			cd_cargo_w,
			ie_paciente_hospital_w,
			nr_seq_cartao_fid_w,
			dt_inicio_valid_cartao_fid_w,
			dt_fim_valid_cartao_fid_w,
			ie_corpo_assistencial_w,
			ie_corpo_clinico_w,
			'A',
			ie_retaguarda_w,
			ie_tsa_w,
			ie_coordenador_w,
			ie_vinculo_medico_w,
			nr_seq_categoria_w,
			dt_demissao_hosp_w,
			:new.nm_usuario,
			0,
			'',
			ie_status_w);
end;
/