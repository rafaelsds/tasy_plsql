CREATE OR REPLACE TRIGGER pf_familia_insert_update
before INSERT or update ON pf_familia
FOR EACH ROW

DECLARE
qtd_member_w number(1);
dependence_w number(1);
ie_grau_parentesco_w GRAU_PARENTESCO.IE_GRAU_PARENTESCO%type;
pragma autonomous_transaction;

function get_qtd_member_family(member_type_p number,
							   ie_gender_p varchar default null)
return number 
as 
qtd_w number(1) := 0;
begin
	
	if (ie_gender_p is not null) then
		select count(*)
		into  qtd_w
		from pf_familia f, 
		grau_parentesco g 
		where f.cd_pessoa_fisica = :new.cd_pessoa_fisica	
		and f.NR_SEQ_GRAU_PARENTESCO = g.nr_sequencia
		and f.ie_gender = ie_gender_p
		and f.nr_sequencia <> :new.nr_sequencia
		and g.IE_GRAU_PARENTESCO = member_type_p;
	else
		select count(*)
		into  qtd_w
		from pf_familia f, 
		grau_parentesco g 
		where f.cd_pessoa_fisica = :new.cd_pessoa_fisica	
		and f.NR_SEQ_GRAU_PARENTESCO = g.nr_sequencia
		and f.nr_sequencia <> :new.nr_sequencia
		and g.IE_GRAU_PARENTESCO = member_type_p;
	end if;
	
	
	return qtd_w;

end get_qtd_member_family;

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
		if (inserting or ((updating) and ((:new.ie_gender <> :old.ie_gender) or (:new.NR_SEQ_GRAU_PARENTESCO <> :old.NR_SEQ_GRAU_PARENTESCO)))) then
			select    max(IE_GRAU_PARENTESCO) ie_grau_parentesco
			into    ie_grau_parentesco_w
			from    grau_parentesco g
			where   nr_sequencia = :new.NR_SEQ_GRAU_PARENTESCO;
			
			if (ie_grau_parentesco_w = 5) then -- 5 = Mae
			
				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w);
			
				if (qtd_member_w > 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153133);
			end if;
			elsif (ie_grau_parentesco_w = 6) then -- 6 - Pai
			
				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w);			
				
				if (qtd_member_w > 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153135);
				end if;	
			elsif (ie_grau_parentesco_w = 9) then -- 9 = Avo paterna(o)
			
				dependence_w := get_qtd_member_family(6); -- 6 = pai
				
				if (dependence_w = 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153113);
				end if;
				
				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w, :new.ie_gender);
				
				if (qtd_member_w > 0) then
					if (:new.ie_gender = 'M') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153118);
					elsif (:new.ie_gender = 'F') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153127);
					end if;
				end if;
				
			elsif (ie_grau_parentesco_w = 12) then -- 12 = Avo(o) materna(o)
				
				dependence_w := get_qtd_member_family(5); -- 5 = mae
				
				if (dependence_w = 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153116);
				end if;
				
				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w, :new.ie_gender);
				
				if (qtd_member_w > 0) then
					if (:new.ie_gender = 'M') then 
						Wheb_mensagem_pck.exibir_mensagem_abort(1153128);
					elsif (:new.ie_gender = 'F') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153131);
					end if;
				end if;

			elsif (ie_grau_parentesco_w = 13) then -- 13 - Esposa(o)
				
				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w);
				
				if (qtd_member_w > 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153136);
				end if;
			
			elsif (ie_grau_parentesco_w = 15) then -- 15 = Sogra(o)
				
				dependence_w := get_qtd_member_family(13); --13 - Esposa(o)
				
				if (dependence_w = 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153117);
				end if;
				
				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w, :new.ie_gender);
				
				if (qtd_member_w > 0) then
					if (:new.ie_gender = 'M') then 
						Wheb_mensagem_pck.exibir_mensagem_abort(1153582);
					elsif (:new.ie_gender = 'F') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153583);
					end if;
				end if;
			end if;
		end if;
	end if;	

end;
/
