create or replace trigger pls_conta_medica_resumo_lib
before update of vl_liberado on pls_conta_medica_resumo
for each row

declare

begin

if	(:new.nr_seq_lote_pgto is not null) and (nvl(:old.vl_liberado, 0) <> nvl(:new.vl_liberado, 0)) then
	pls_grava_log_autonomus('pls_conta_medica_resumo_lib', 'Resumo: ' || :new.nr_sequencia || ' Old: ' || :old.vl_liberado || ' New: ' || :new.vl_liberado, nvl(:new.nm_usuario, :old.nm_usuario));
end if;

end;
/