create or replace trigger w_ctb_aux_contra_emit_log
before delete or insert or update on w_ctb_aux_contra_emit
for each row

declare

nm_usuario_w			usuario.nm_usuario%type;
ds_log_w			ctb_reg_auxiliar_log.ds_log%type;
ds_alt_reg_w			varchar2(4000);
qt_registros_w			number(10);
			
begin

nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

if deleting then
	ds_log_w := wheb_mensagem_pck.get_texto(392981, 'NR_SEQUENCIA=' || :old.nr_sequencia || ';NR_DOCUMENTO=' || :old.nr_documento || ';VALOR=' || :old.vl_parcela || ';CD_CONTA_CONTAB=' || :old.cd_conta_contabil || ';CLASSIFICACAO=' || substr(ctb_obter_classif_conta(:old.cd_conta_contabil, null, :old.dt_referencia),1,255));
	
	wheb_usuario_pck.set_ie_executar_trigger('N');
	
	insert into	ctb_reg_auxiliar_log(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_reg_auxiliar,
					ie_tipo_log,
					ds_log)
				values(ctb_reg_auxiliar_log_seq.nextval,
					sysdate,
					nm_usuario_w,
					sysdate,
					nm_usuario_w,
					:old.nr_seq_reg_auxiliar,
					'E',
					ds_log_w);

elsif inserting then

	if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then 

		ds_log_w := wheb_mensagem_pck.get_texto(394713, 'NR_SEQUENCIA=' || :new.nr_sequencia || ';NR_DOCUMENTO=' || :new.nr_documento || ';VALOR=' || :new.vl_parcela || ';CD_CONTA_CONTAB=' || :new.cd_conta_contabil || ';CLASSIFICACAO=' || substr(ctb_obter_classif_conta(:new.cd_conta_contabil, null, :new.dt_referencia),1,255));
		
		wheb_usuario_pck.set_ie_executar_trigger('N');
		
		insert into	ctb_reg_auxiliar_log(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_reg_auxiliar,
						ie_tipo_log,
						ds_log)
					values(ctb_reg_auxiliar_log_seq.nextval,
						sysdate,
						nm_usuario_w,
						sysdate,
						nm_usuario_w,
						:new.nr_seq_reg_auxiliar,
						'I',
						ds_log_w);
						
		wheb_usuario_pck.set_ie_executar_trigger('S');
					
	end if;

elsif updating then

	if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	
		ds_alt_reg_w := :new.nr_sequencia;
		
		if (nvl(:old.nr_contrato,0) <> nvl(:new.nr_contrato,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Nr contrato: ' || :old.nr_contrato;
		end if;
		
		if (nvl(:old.nr_titulo,0) <> nvl(:new.nr_titulo,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Nr t�tulo: ' || :old.nr_titulo;
		end if;
		
		if ((:old.dt_contrato <> :new.dt_contrato) or (:old.dt_contrato is null and :new.dt_contrato is not null) or (:old.dt_contrato is not null and :new.dt_contrato is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data contrato: ' || :old.dt_contrato;
		end if;
		
		if ((:old.dt_rescisao <> :new.dt_rescisao) or (:old.dt_rescisao is null and :new.dt_rescisao is not null) or (:old.dt_rescisao is not null and :new.dt_rescisao is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data rescis�o: ' || :old.dt_rescisao;
		end if;
		
		if ((:old.dt_referencia <> :new.dt_referencia) or (:old.dt_referencia is null and :new.dt_referencia is not null) or (:old.dt_referencia is not null and :new.dt_referencia is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data refer�ncia: ' || :old.dt_referencia;
		end if;
		
		if ((:old.dt_rec_antec <> :new.dt_rec_antec) or (:old.dt_rec_antec is null and :new.dt_rec_antec is not null) or (:old.dt_rec_antec is not null and :new.dt_rec_antec is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data recebimento: ' || :old.dt_rec_antec;
		end if;
		
		if ((:old.dt_inicio_cobertura <> :new.dt_inicio_cobertura) or (:old.dt_inicio_cobertura is null and :new.dt_inicio_cobertura is not null) or (:old.dt_inicio_cobertura is not null and :new.dt_inicio_cobertura is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data in�cio cobertura: ' || :old.dt_inicio_cobertura;
		end if;
		
		if ((:old.dt_fim_cobertura <> :new.dt_fim_cobertura) or (:old.dt_fim_cobertura is null and :new.dt_fim_cobertura is not null) or (:old.dt_fim_cobertura is not null and :new.dt_fim_cobertura is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data fim cobertura: ' || :old.dt_fim_cobertura;
		end if;
		
		if (nvl(:old.ds_tipo_contratacao,0) <> nvl(:new.ds_tipo_contratacao,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Tipo contrata��o: ' || :old.ds_tipo_contratacao;
		end if;
		
		if (nvl(:old.ds_modalidade_contrat,0) <> nvl(:new.ds_modalidade_contrat,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Modalidade contrata��o: ' || :old.ds_modalidade_contrat;
		end if;
		
		if (nvl(:old.ds_regulamentacao,0) <> nvl(:new.ds_regulamentacao,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Regulamenta��o: ' || :old.ds_regulamentacao;
		end if;
		
		if (nvl(:old.ds_segmentacao,0) <> nvl(:new.ds_segmentacao,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Segmenta��o: ' || :old.ds_segmentacao;
		end if;
		
		if (nvl(:old.ds_definicao_atos,0) <> nvl(:new.ds_definicao_atos,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Ato cooperado: ' || :old.ds_definicao_atos;
		end if;
		
		if (nvl(:old.nm_usuario_principal,0) <> nvl(:new.nm_usuario_principal,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Usu�rio principal: ' || :old.nm_usuario_principal;
		end if;
		
		if (nvl(:old.cd_cpf_cnpj,0) <> nvl(:new.cd_cpf_cnpj,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', CPF/CNPJ: ' || :old.cd_cpf_cnpj;
		end if;
		
		if ((:old.dt_emissao <> :new.dt_emissao) or (:old.dt_emissao is null and :new.dt_emissao is not null) or (:old.dt_emissao is not null and :new.dt_emissao is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data emiss�o: ' || :old.dt_emissao;
		end if;
		
		if (nvl(:old.ds_tipo_documento,0) <> nvl(:new.ds_tipo_documento,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Tipo documento: ' || :old.ds_tipo_documento;
		end if;
		
		if (nvl(:old.nr_documento,0) <> nvl(:new.nr_documento,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Nr Documento: ' || :old.nr_documento;
		end if;
		
		if (nvl(:old.nr_parcela,0) <> nvl(:new.nr_parcela,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Nr parcela: ' || :old.nr_parcela;
		end if;
		
		if ((:old.dt_vencimento <> :new.dt_vencimento) or (:old.dt_vencimento is null and :new.dt_vencimento is not null) or (:old.dt_vencimento is not null and :new.dt_vencimento is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data vencimento: ' || :old.dt_vencimento;
		end if;
		
		if (nvl(:old.vl_parcela,0) <> nvl(:new.vl_parcela,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Valor parcela: ' || :old.vl_parcela;
		end if;
		
		if (nvl(:old.vl_apropriado,0) <> nvl(:new.vl_apropriado,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Valor apropriado: ' || :old.vl_apropriado;
		end if;
		
		if (nvl(:old.vl_antecipado,0) <> nvl(:new.vl_antecipado,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Valor antecipado: ' || :old.vl_antecipado;
		end if;
		
		if (nvl(:old.vl_fat_antec,0) <> nvl(:new.vl_fat_antec,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Valor faturamento antecipado: ' || :old.vl_fat_antec;
		end if;
		
		if (nvl(:old.vl_rec_cobertura,0) <> nvl(:new.vl_rec_cobertura,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Valor receita cobertura: ' || :old.vl_rec_cobertura;
		end if;
		
		if ((:old.dt_baixa <> :new.dt_baixa) or (:old.dt_baixa is null and :new.dt_baixa is not null) or (:old.dt_baixa is not null and :new.dt_baixa is null)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Data baixa: ' || :old.dt_baixa;
		end if;
		
		if (nvl(:old.cd_beneficiario,0) <> nvl(:new.cd_beneficiario,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Nr carteira: ' || :old.cd_beneficiario;
		end if;
		
		if (nvl(:old.nr_protocolo_ans,0) <> nvl(:new.nr_protocolo_ans,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Nr protocolo: ' || :old.nr_protocolo_ans;
		end if;
		
		if (nvl(:old.cd_conta_debito,0) <> nvl(:new.cd_conta_debito,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Conta d�bito: ' || :old.cd_conta_debito;
		end if;
		
		if (nvl(:old.cd_conta_contabil,0) <> nvl(:new.cd_conta_contabil,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Conta cont�bil: ' || :old.cd_conta_contabil;
		end if;
		
		if (nvl(:old.cd_conta_cred_encargos,0) <> nvl(:new.cd_conta_cred_encargos,0)) then
			ds_alt_reg_w := ds_alt_reg_w || ', Conta cr�dito encargos: ' || :old.cd_conta_cred_encargos;
		end if;
 
		ds_log_w := substr(wheb_mensagem_pck.get_texto(394796, 'NR_SEQUENCIA=' || ds_alt_reg_w),1,255);

		wheb_usuario_pck.set_ie_executar_trigger('N');

		insert into	ctb_reg_auxiliar_log(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_reg_auxiliar,
						ie_tipo_log,
						ds_log)
					values(ctb_reg_auxiliar_log_seq.nextval,
						sysdate,
						nm_usuario_w,
						sysdate,
						nm_usuario_w,
						:new.nr_seq_reg_auxiliar,
						'A',
						ds_log_w);
					
		wheb_usuario_pck.set_ie_executar_trigger('S');
		
	end if;

end if;

end;
/