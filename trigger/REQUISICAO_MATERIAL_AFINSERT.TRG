create or replace trigger requisicao_material_afinsert
after insert on requisicao_material
for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_executa_trigger	varchar2(1);

begin

if	(:new.dt_aprovacao is not null) then
	
	begin

	reg_integracao_p.ie_operacao			:=	'I';
	reg_integracao_p.cd_estab_documento		:=	:new.cd_estabelecimento; 
	reg_integracao_p.cd_operacao_estoque		:=	:new.cd_operacao_estoque;
	reg_integracao_p.cd_local_estoque		:=	:new.cd_local_estoque;
	reg_integracao_p.cd_local_estoque_destino	:=	:new.cd_local_estoque_destino;
	reg_integracao_p.cd_centro_custo		:=	:new.cd_centro_custo;

ie_executa_trigger	:=	wheb_usuario_pck.get_ie_executar_trigger;
	
		begin	
		wheb_usuario_pck.set_ie_executar_trigger('S');
		gerar_int_padrao.gravar_integracao('30', :new.nr_requisicao, :new.nm_usuario, reg_integracao_p);	
		wheb_usuario_pck.set_ie_executar_trigger(ie_executa_trigger);
		exception
		when others then
		wheb_usuario_pck.set_ie_executar_trigger(ie_executa_trigger);	
		end;
	
	end;
end if;
end;
/
