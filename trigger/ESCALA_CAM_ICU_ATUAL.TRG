create or replace trigger escala_cam_icu_atual
before insert or update on escala_cam_icu
for each row

declare

qt_pens_desorg_presente  	Number(10);


begin

:new.ie_inicio_agudo_presente := 'A';
if	(nvl(:new.ie_alt_estado_mental,'N') = 'S') or
	(nvl(:new.ie_comport_anormal,'N') = 'S') then
	:new.ie_inicio_agudo_presente := 'P';
end if;

:new.ie_atencao_presente := 'A';
if	(nvl(:new.ie_focar_atencao,'N') = 'S') or
	(nvl(:new.ie_distraido,'N') = 'S') then
	:new.ie_atencao_presente := 'P';
end if;

:new.ie_pens_desorg_presente := 'A';
qt_pens_desorg_presente:= 0;
if	(nvl(:new.ie_pedra_flutuam,'N') = 'S') then
	qt_pens_desorg_presente := qt_pens_desorg_presente + 1;
end if;
if	(nvl(:new.ie_peixe_mar,'N') = 'S') then
	qt_pens_desorg_presente := qt_pens_desorg_presente + 1;
end if;
if (nvl(:new.ie_quilograma,'N') = 'S') then
	qt_pens_desorg_presente := qt_pens_desorg_presente + 1;
end if;
if (nvl(:new.ie_martelo_madeira,'N') = 'S') then
	qt_pens_desorg_presente := qt_pens_desorg_presente + 1;
end if;
if	(qt_pens_desorg_presente > 1) then	
	:new.ie_pens_desorg_presente := 'P';
end if;

:new.ie_nivel_conc_presente := 'A';
if	(nvl(:new.ie_agitado,'N') = 'S') or
	(nvl(:new.ie_letagico,'N') = 'S') or
	(nvl(:new.ie_estuporoso,'N') = 'S') or
	(nvl(:new.ie_camatoso,'N') = 'S') then
	:new.ie_nivel_conc_presente := 'P';
end if;

end;
/