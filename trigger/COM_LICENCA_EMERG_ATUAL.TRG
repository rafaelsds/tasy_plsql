create or replace
trigger com_licenca_emerg_atual
before insert or update on com_licenca_emerg
for each row
declare
ds_hash_w varchar2(32);
begin
if	(:old.dt_liberacao is not null and :old.ds_hash is not null) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(281051);
end if;
if	(:old.cd_licenca is null ) then
	:new.cd_licenca := gerar_codigo_licenca;
end if;
end com_licenca_emerg_atual;
/