create or replace TRIGGER ATEND_ESCALA_TENETTI_ATUAL 
before insert or update on ESCALA_TENETTI 
for each row 
 
declare 
  sql_w           varchar2(200);
  qt_pontuacao_w  number(5, 0); 
begin
    if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
		/** Medical Device **/
		begin
			sql_w := 'call calcula_escala_tenetti_md(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12) into :qt_pontuacao_w';

			execute immediate sql_w using in :new.ie_equilibrio_sentado,
										  in :new.ie_levantar_cadeira,
										  in :new.ie_equilibrio_imediato,
										  in :new.ie_equilibrio_em_pe,
										  in :new.ie_equilibrio_olho_fechado,
										  in :new.ie_equilibrio_girar,
										  in :new.ie_empurrao_torax,
										  in :new.ie_girar_pescoco,
										  in :new.ie_equilibrio_apoio,
										  in :new.ie_extensao_tronco,
										  in :new.ie_alcancar,
										  in :new.ie_sentar,
										  out qt_pontuacao_w;
		exception
		when others then
			qt_pontuacao_w := null;
		end;
									
		:new.qt_pontuacao := qt_pontuacao_w;
    end if;
end;
/