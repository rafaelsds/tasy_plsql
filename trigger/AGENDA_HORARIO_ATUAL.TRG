CREATE OR REPLACE TRIGGER Agenda_Horario_Atual
before insert or update ON Agenda_Horario
FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
dt_final_w	date;
Cursor C01 is
	select	*
	from	agenda_horario
	where	hr_inicial > hr_final;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
and	cd_agenda	 = :new.cd_agenda;

for r_c01 in c01 loop
	begin
	dt_final_w	:= to_date(to_char( r_c01.hr_inicial,'dd/mm/yyyy') ||' '|| to_char( r_c01.hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
	
	update	agenda_horario
	set	hr_final = dt_final_w
	where	nr_sequencia = r_c01.nr_sequencia;
	end;
end loop;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/
