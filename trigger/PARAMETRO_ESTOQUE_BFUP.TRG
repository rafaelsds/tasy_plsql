create or replace trigger parametro_estoque_bfup
before update or insert on parametro_estoque
for each row

declare

begin
if	(:new.DT_MESANO_VIGENTE <> :old.DT_MESANO_VIGENTE) then
	:new.ds_stack	:=	substr(dbms_utility.format_call_stack,1,2000);
end if;
end;
/
