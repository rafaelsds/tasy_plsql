create or replace trigger W_HRF_PESSOA_JURIDICA_INSERT
before insert on W_HRF_PESSOA_JURIDICA
for each row

declare
cd_cgc_w			varchar2(20)	:= null;
qt_item_w			number(3);
ds_erro_w			varchar2(255);
qt_registro_w			number(10);

begin

if	(upper(:new.ie_importado) = Upper('N'))	Then
	/* Cgc v�lido */		
	if	(nvl(somente_numero(:new.cd_cgc),0) > 0) then

		select 	replace(replace(replace(:new.cd_cgc,'.',''),'-',''),'/','')
		into	cd_cgc_w
		from	dual;
		
		select	count(*)
		into	qt_item_w
		from	pessoa_juridica
		where	cd_cgc		= cd_cgc_w;
		
		/* Se ainda n�o h� a pessoa */
		if	(qt_item_w = 0) then

			begin
			insert into	pessoa_juridica(
				cd_cgc,
				ds_razao_social,
				nm_fantasia,
				cd_cep,
				ds_endereco,
				ds_bairro,
				ds_municipio,
				sg_estado,
				dt_atualizacao,
				nm_usuario,
				ds_complemento,
				nr_telefone,
				nr_endereco,
				cd_tipo_pessoa,
				ie_prod_fabric,
				ie_situacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec)
			values(
				cd_cgc_w,
				:new.ds_razao_social,
				nvl(:new.nm_fantasia,:new.ds_razao_social),
				:new.cd_cep,
				:new.ds_endereco,
				:new.ds_bairro,
				:new.ds_municipio,
				:new.sg_estado,
				sysdate,
				'TopSaude',
				:new.ds_complemento,
				:new.nr_telefone,
				:new.nr_endereco,
				:new.cd_tipo_pessoa,
				'N',
				'A',
				'TopSaude',
				sysdate);				
			:new.ie_importado	:= 'S';
			
			exception
				when others then
				:new.ie_importado := 'E';
				
				ds_erro_w := SQLERRM(sqlcode);
					insert into log_tasy(
						dt_atualizacao,
						nm_usuario,
						cd_log,
						ds_log)
					values(
						sysdate,
						'TopSaude',
						55717,
						'N�o foi importado a PJ: ' ||
						'cd_cgc = ' || cd_cgc_w ||
						'ds_razao_social = ' || :new.ds_razao_social ||
						'DS_ERRO = ' || ds_erro_w);
			end;
		/* Se j� ecnontrou a pessoa */	
		else
			:new.ie_importado	:= 'S';

			update	pessoa_juridica
			set	nm_usuario_nrec		= 'TopSaude',
				dt_atualizacao_nrec	= sysdate
			where	cd_cgc			= cd_cgc_w;
		end if;
	/* Cgc inv�lido */
	else
		:new.ie_importado	:= 'N';

		insert into log_tasy(
				dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
			values(
				sysdate,
				'TopSaude',
				55717,
				'Cgc inv�lido! ' ||
				'cd_cgc = ' || cd_cgc_w ||
				'ds_razao_social = ' || :new.ds_razao_social);
	end if;
	
	if	(:new.ie_importado = 'S') and
		(cd_cgc_w is not null) then
	
		select	count(*)
		into	qt_registro_w
		from	pessoa_juridica_conta
		where	cd_cgc	= cd_cgc_w;
				
		if	(qt_registro_w = 0) and
			(:new.cd_banco 	is not null) and
			(:new.cd_agencia_bancaria 	is not null) and
			(:new.nr_conta	is not null)	then
			Begin				
			insert into	pessoa_juridica_conta(
				cd_cgc,
				dt_atualizacao,
				nm_usuario,
				cd_banco,
				cd_agencia_bancaria,
				nr_conta,
				nr_digito_conta,	
				ie_conta_pagamento,
				ie_digito_agencia,
				ie_situacao)
			values(
				cd_cgc_w,
				sysdate,
				'TopSaude',
				:new.cd_banco,
				:new.cd_agencia_bancaria,
				:new.nr_conta,
				:new.nr_digito_conta,
				:new.ie_conta_pagamento,
				:new.ie_digito_agencia,
				'A');
				
			exception
				when others then
				:new.ie_importado	:= 'E';	

					ds_erro_w := SQLERRM(sqlcode);
					insert into log_tasy(
						dt_atualizacao,
						nm_usuario,
						cd_log,
						ds_log)
					values(
						sysdate,
						'TopSaude',
						55717,
						'N�o foi importado o Complemento de PJ: ' ||
						'cd_cgc = ' || cd_cgc_w ||
						'ds_razao_social = ' || :new.ds_razao_social ||
						'DS_ERRO = ' || ds_erro_w);
			end;
		end if;

		select	count(*)
		into	qt_registro_w
		from	pessoa_jur_conta_cont
		where	cd_cgc	= cd_cgc_w
		and	ie_tipo_conta		= 'R';

		if	(qt_registro_w = 0) and
			(:new.cd_conta_contab_rec is not null) then
			insert	into	pessoa_jur_conta_cont
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				cd_conta_contabil,
				ie_tipo_conta,
				dt_inicio_vigencia,
				dt_fim_vigencia,
				cd_cgc,
				cd_empresa)
			values	(pessoa_jur_conta_cont_seq.nextval,
				'TopSaude',
				sysdate,
				'TopSaude',
				sysdate,
				:new.cd_conta_contab_rec,
				'R',
				null,
				null,
				cd_cgc_w,
				1);
		end if;

		select	count(*)
		into	qt_registro_w
		from	pessoa_jur_conta_cont
		where	cd_cgc	= cd_cgc_w
		and	ie_tipo_conta		= 'P';

		if	(qt_registro_w = 0) and
			(:new.cd_conta_contab_pag is not null) then
			insert	into	pessoa_jur_conta_cont
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				cd_conta_contabil,
				ie_tipo_conta,
				dt_inicio_vigencia,
				dt_fim_vigencia,
				cd_cgc,
				cd_empresa)
			values	(pessoa_jur_conta_cont_seq.nextval,
				'TopSaude',
				sysdate,
				'TopSaude',
				sysdate,
				:new.cd_conta_contab_pag,
				'P',
				null,
				null,
				cd_cgc_w,
				1);
		end if;
	end if;
end if;

end;
/