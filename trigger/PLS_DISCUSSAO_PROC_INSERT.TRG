create or replace trigger pls_discussao_proc_insert
before insert or update on pls_discussao_proc
for each row
declare

vl_total_w		number(15,2);
qt_disc_proc_w		pls_integer;
	
pragma autonomous_transaction;

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	((:new.vl_aceito <> :old.vl_aceito) or
		(:new.vl_negado <> :old.vl_negado)) and
		((:new.vl_aceito <> 0) or (:new.vl_negado <> 0)) then
		
		vl_total_w := (nvl(:new.vl_aceito,0) + nvl(:new.vl_negado,0));
		
		if	(nvl(:old.vl_recurso,0) <> vl_total_w) and
			(nvl(:old.vl_contestado,0) <> vl_total_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(148398);
		end if;
	end if;
end if;

select	count(1)
into	qt_disc_proc_w
from	pls_discussao_proc 		d,
	pls_contestacao_discussao	c,
	pls_lote_discussao 		b,
	pls_lote_contestacao		a
where	b.nr_sequencia			= c.nr_seq_lote
and	c.nr_sequencia			= d.nr_seq_discussao
and	a.nr_sequencia			= b.nr_seq_lote_contest
and	d.ie_tipo_acordo 		= '00'
and	b.ie_tipo_arquivo 		= 1
and	a.ie_envio_recebimento		= 'R'
and	b.ie_status			<> 'C' -- Cancelado
and	d.nr_seq_a500 			= :new.nr_seq_a500
and	d.nr_seq_conta_proc		= :new.nr_seq_conta_proc
and	d.vl_contestado 		<> :new.vl_contestado
and	d.nr_sequencia 			<> :new.nr_sequencia
and	d.vl_contestado is not null
and	a.ie_status			<> 'C' -- Concluido
and	a.nr_seq_pls_fatura is not null
and exists 	(select 1
		from 	pls_conta_proc x
		where 	x.nr_sequencia = d.nr_seq_conta_proc
		and 	x.nr_seq_pacote is null);

if	(nvl(qt_disc_proc_w, 0) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(307305);
end if;

end;
/