CREATE OR replace TRIGGER atend_sumario_alta_afupdate 
  AFTER UPDATE ON atend_sumario_alta 
  FOR EACH ROW 
DECLARE 
    CURSOR c01 IS 
      SELECT nr_sequencia 
      FROM   wl_worklist 
      WHERE  nr_seq_disc_summary = :new.nr_sequencia 
             AND dt_final_real IS NULL; 
    c01_w c01%ROWTYPE; 
BEGIN 
    IF ( :new.ie_situacao = 'I' ) 
       AND ( :old.ie_situacao = 'A' ) THEN 
      OPEN c01; 

      LOOP 
          FETCH c01 INTO c01_w; 

          exit WHEN c01%NOTFOUND; 

          BEGIN 
              UPDATE wl_worklist 
              SET    dt_final_real = SYSDATE, 
                     nm_usuario = :new.nm_usuario, 
                     dt_atualizacao = SYSDATE 
              WHERE  nr_sequencia = c01_w.nr_sequencia 
                     AND dt_final_real IS NULL; 

              INSERT INTO wl_worklist_historico 
                          (nr_sequencia, 
                           nr_seq_worklist, 
                           ds_motivo, 
                           nm_usuario, 
                           dt_atualizacao) 
              VALUES     ( wl_worklist_historico_seq.NEXTVAL, 
                          c01_w.nr_sequencia, 
                          Obter_desc_expressao(965060), 
                          :new.nm_usuario, 
                          SYSDATE); 
          END; 
      END LOOP; 

      CLOSE c01; 
    END IF; 
END;
/
