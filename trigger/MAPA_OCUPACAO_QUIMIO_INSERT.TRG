create or replace trigger mapa_ocupacao_quimio_insert
before update on mapa_ocupacao_quimio
for each row

declare

nr_sequencia_w		number(10);
nr_seq_mapa_onco_w	number(10);
begin

nr_seq_mapa_onco_w := :new.nr_sequencia;

if 	(:new.nr_seq_local <> :old.nr_seq_local) then 
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao, 
		nm_usuario,
		nr_seq_local,
		nr_seq_mapa_ocup)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.nr_seq_local,
		nr_seq_mapa_onco_w);
end if;

if	(:new.dt_mapa <> :old.dt_mapa) then 
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao, 
		nm_usuario,
		dt_mapa,
		nr_seq_mapa_ocup)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.dt_mapa,
		nr_seq_mapa_onco_w);
end if;

if	(:new.dt_mapa <> :old.dt_mapa) then 
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao, 
		nm_usuario,
		dt_mapa,
		nr_seq_mapa_ocup)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.dt_mapa,
		nr_seq_mapa_onco_w);
end if;

if	(:new.ie_exibir_mapa <> :old.ie_exibir_mapa) then 
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao, 
		nm_usuario,
		ie_exibir_mapa,
		nr_seq_mapa_ocup)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.ie_exibir_mapa,
		nr_seq_mapa_onco_w);
end if;

end;
/
