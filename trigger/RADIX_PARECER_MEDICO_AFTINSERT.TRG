create or replace trigger radix_parecer_medico_aftinsert
after insert or update on parecer_medico
for each row

declare

reg_integracao_w    gerar_int_padrao.reg_integracao;
nr_seq_conv_w      atend_categoria_convenio.nr_seq_interno%type;
nr_atendimento_w    parecer_medico_req.nr_atendimento%type;

begin

if  (:old.DT_LIBERACAO is null) and
  (:new.DT_LIBERACAO is not null) then

  select   nr_atendimento
  into  nr_atendimento_w
  from  parecer_medico_req
  where  nr_parecer = :new.nr_parecer;

  nr_seq_conv_w := obter_atecaco_atendimento(nr_atendimento_w);

  select   max(b.cd_convenio),
      max(b.cd_categoria),
      max(b.cd_plano_convenio),
      max(a.ie_tipo_atendimento),
      max(a.cd_estabelecimento)
  into  reg_integracao_w.cd_convenio,
      reg_integracao_w.cd_categoria,
      reg_integracao_w.cd_plano_convenio,
      reg_integracao_w.ie_tipo_atendimento,
      reg_integracao_w.cd_estab_documento
  from   atendimento_paciente a,
      atend_categoria_convenio b
  where   a.nr_atendimento = b.nr_atendimento
  and    a.nr_atendimento = nr_atendimento_w
  and    b.nr_seq_interno = nr_seq_conv_w;

  gerar_int_padrao.gravar_integracao('70', :new.nr_parecer, :new.nm_usuario, reg_integracao_w);
  --INSERT INTO TMP_MONITORADO SELECT 'ENTROU NA LIBERACAO - '||reg_integracao_w.cd_estab_documento FROM DUAL;
  -- 70 - Ponto de integracao
  -- :new.nr_parecer - nr_seq_documento
  -- reg_integracao_w - Retorno padrao

end if;
end;
/
