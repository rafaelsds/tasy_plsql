CREATE OR REPLACE TRIGGER CUR_CURATIVO_ATUAL
BEFORE INSERT OR UPDATE ON CUR_CURATIVO
FOR EACH ROW

BEGIN

:new.qt_escore :=	:new.ie_epitelizacao +
			:new.ie_tecido_granulacao +
			:new.ie_endurecimento +
			:new.ie_edema_tecido +
			:new.ie_cor_pele_redor +
			:new.ie_qt_exsudato +
			:new.ie_tipo_exsudato +
			:new.ie_qt_tecido +
			:new.ie_tipo_tecido +
			:new.ie_deslocamento +
			:new.ie_borda +
			:new.ie_profundidade +
			:new.ie_dor_redor +
			:new.ie_grangrena +
			:new.ie_maceracao +
			:new.ie_tamanho;
			
if	(nvl(:old.DT_CURATIVO,sysdate+10) <> :new.DT_CURATIVO) and
	(:new.DT_CURATIVO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_CURATIVO, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;


END;
/
