create or replace trigger ptu_prod_lote_restricao_atual
before insert or update on PTU_PROD_LOTE_RESTRICAO
for each row

declare

nr_seq_contrato_w	pls_contrato.nr_sequencia%type;

begin

if	(:new.nr_contrato is not null) and 
	(:new.nr_contrato <> nvl(:old.nr_contrato,0)) then
	
	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato = :new.nr_contrato;
	
	if	(nr_seq_contrato_w is not null) then
		:new.nr_seq_contrato := nr_seq_contrato_w;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(853575); --O contrato informado n�o existe. Verifique!
	end if;
else
	:new.nr_seq_contrato	:= null;
end if;

end;
/