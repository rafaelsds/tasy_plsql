create or replace trigger pls_lote_prot_conta_alt
before update on pls_lote_protocolo_conta
for each row

declare
qt_rejeitado_w	pls_integer;
ds_log_call_w	varchar2(1500);
ds_log_w		varchar2(4000);
nm_usuario_w	usuario.nm_usuario%type;

begin 

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') then
	select	count(1)
	into	qt_rejeitado_w
	from	pls_protocolo_conta
	where	nr_seq_lote_conta = :new.nr_sequencia
	and	ie_situacao	= 'RE';

	if	(qt_rejeitado_w > 0) and
		(:new.dt_confirmacao is not null) and
		(:old.dt_confirmacao is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(273280);
	end if;

	if (:old.ie_status = 'A' and nvl(:new.ie_status, 'Z') != 'A') then
		
		ds_log_call_w := substr(	''|| chr(13) ||chr(10)||
									' Funcao ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
									' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);

		ds_log_w := ' Mudou status. Anterior: '||:old.ie_status ||' novo: '||:new.ie_status||''||ds_log_call_w;
		nm_usuario_w := substr(nvl(wheb_usuario_pck.get_nm_usuario,'Nao identif.'),1,14); 

		insert into pls_log_lote_prot_conta	(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
						nm_usuario_nrec, ds_log, nr_seq_lote_protocolo)
			values		(	pls_log_lote_prot_conta_seq.nextval, sysdate, nm_usuario_w, sysdate,
						nm_usuario_w, ds_log_w , :new.nr_sequencia);

	end if;
end if;


end;
/
