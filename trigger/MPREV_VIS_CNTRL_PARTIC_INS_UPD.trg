create or replace trigger MPREV_VIS_CNTRL_PARTIC_INS_UPD
  before insert or update on mprev_visual_contrl_partic  
  for each row
DECLARE
BEGIN

    IF :new.cd_estabelecimento IS NULL AND
       :new.cd_perfil IS NULL AND
       :new.nm_usuario_regra IS NULL THEN
        wheb_mensagem_pck.exibir_mensagem_abort(1142262);
    END IF;

END mprev_vis_cntrl_partic_ins_upd;
/
