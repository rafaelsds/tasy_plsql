create or replace trigger PERFIL_ITEM_PRONT_PASTA_ATUAL
before INSERT OR UPDATE OR DELETE on PERFIL_ITEM_PRONT_PASTA
for each row

declare

nr_pasta_filha_w number(10);
qt_registro_sup_w number(10);
nr_seq_pasta_sup_w number(10);
nr_seq_dic_obj_w number(10);
qt_reg_config_comp_w number(10);

Cursor C01 is
	SELECT  a.NR_SEQUENCIA
	FROM 	perfil_item_pront_pasta a,
			PERFIL_ITEM_PRONT b
	WHERE	a.NR_SEQ_ITEM_PERFIL = b.nr_sequencia
	AND		b.nr_sequencia = :old.NR_SEQ_ITEM_PERFIL
	AND		a.nr_sequencia IN (
		SELECT x.nr_sequencia
		FROM	perfil_item_pront_pasta x
		WHERE	x.nr_seq_item_pasta IN (
			SELECT 	  p.nr_sequencia
			FROM	  prontuario_item_pasta p
			WHERE	  p.nr_seq_pasta IN (
				SELECT z.NR_sequencia
				FROM	prontuario_pasta z
				WHERE	z.nr_seq_pasta_superior IN (obter_seq_pasta_superior(:old.nr_seq_item_pasta, 'P'))
			)
		)
	);

pragma autonomous_transaction;
	
begin

if (deleting) then
	open C01;
		loop
		fetch C01 into	
			nr_pasta_filha_w;
		exit when C01%notfound;
			begin

				if (nvl(nr_pasta_filha_w, 0) > 0) then
					delete from perfil_item_pront_pasta where nr_sequencia = nr_pasta_filha_w;
					COMMIT;
				end if;

			end;
		end loop;
	close C01;

elsif (inserting or updating) then

	select nvl(obter_seq_pasta_superior(:new.nr_seq_item_pasta, 'F'),0)
	into nr_seq_pasta_sup_w
	from dual;
	
	if (nr_seq_pasta_sup_w > 0) then
	
		SELECT  COUNT(*)
		into	qt_registro_sup_w
		FROM 	perfil_item_pront_pasta a,
				PERFIL_ITEM_PRONT b
		WHERE	a.NR_SEQ_ITEM_PERFIL = b.nr_sequencia
		AND		b.nr_sequencia = :new.NR_SEQ_ITEM_PERFIL
		AND		a.nr_sequencia IN (
				SELECT x.nr_sequencia
				FROM	perfil_item_pront_pasta x
				WHERE	x.nr_seq_item_pasta IN (
					SELECT 	  p.nr_sequencia
					FROM	  prontuario_item_pasta p
					WHERE	  p.nr_seq_pasta IN (nr_seq_pasta_sup_w))
		);
		
		if (qt_registro_sup_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(449071);
		end if;
	end if;
	
end if;
end;
/