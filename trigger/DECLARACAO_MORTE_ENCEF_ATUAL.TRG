create or replace trigger DECLARACAO_MORTE_ENCEF_atual
before insert or update on DECLARACAO_MORTE_ENCEF
for each row

declare
dt_nascimento_w		date;
begin

select	max(a.dt_nascimento)
into	dt_nascimento_w
from	pessoa_fisica a,
		atendimento_paciente b
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and		b.nr_atendimento	= :new.nr_atendimento;

if	(dt_nascimento_w	is not null) and
	(:new.DT_DECLARACAO is not null) and
	(:new.DT_DECLARACAO	< dt_nascimento_w)then
	wheb_mensagem_pck.exibir_mensagem_abort(356157);
end if;


if	(:new.DT_DECLARACAO is not null) and
	(:new.DT_AVALIACAO_1 is not null) and
	(:new.DT_AVALIACAO_1	> :new.DT_DECLARACAO) then
	wheb_mensagem_pck.exibir_mensagem_abort(356162);
end if;


if	(:new.DT_DECLARACAO is not null) and
	(:new.DT_AVALIACAO_2 is not null) and
	(:new.DT_AVALIACAO_2	> :new.DT_DECLARACAO) then
	wheb_mensagem_pck.exibir_mensagem_abort(356162);
end if;


end;
/