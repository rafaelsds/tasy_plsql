create or replace trigger CIRURGIA_DESCRICAO_INSUP
before insert or update on CIRURGIA_DESCRICAO
for each row
declare


begin
if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if (inserting and :new.IE_SITUACAO <> 'A') then
	:new.IE_SITUACAO := 'A';
end if;

end;
/