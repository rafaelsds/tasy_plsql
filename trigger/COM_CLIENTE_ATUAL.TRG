CREATE OR REPLACE TRIGGER COM_CLIENTE_ATUAL
AFTER INSERT OR UPDATE ON COM_CLIENTE
FOR EACH ROW 
DECLARE

nr_sequencia_w		number(10);

begin		

if	(nvl(:new.ie_fase_venda,0) <> nvl(:old.ie_fase_venda,0)) then
	begin

	select	com_cliente_log_seq.NextVal
	into	nr_sequencia_w
	from	dual;

	insert into com_cliente_log
	(	nr_sequencia,
		nr_seq_cliente,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_log,
		dt_log,
		ie_classificacao,
		ie_fase_venda,
		nr_seq_canal)
	values	(
		nr_sequencia_w,
		:new.nr_sequencia,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		1,
		sysdate,
		null,
		:new.ie_fase_venda,
		null);
	
end;

end if;

if	(nvl(:new.ie_classificacao,0) <> nvl(:old.ie_classificacao,0)) then
	begin

	select	com_cliente_log_seq.NextVal
	into	nr_sequencia_w
	from	dual;

	insert into com_cliente_log
	(	nr_sequencia,
		nr_seq_cliente,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_log,
		dt_log,
		ie_classificacao,
		ie_fase_venda,
		nr_seq_canal)
	values	(
		nr_sequencia_w,
		:new.nr_sequencia,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		2,
		sysdate,
		:new.ie_classificacao,
		null,
		null);
		
	end;
	
end if;

end;
/
