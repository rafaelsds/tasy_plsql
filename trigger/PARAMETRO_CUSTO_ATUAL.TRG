CREATE OR REPLACE TRIGGER Parametro_Custo_Atual
BEFORE INSERT OR UPDATE ON Parametro_custo
FOR EACH ROW
DECLARE

BEGIN

:new.cd_comp_custo_ind_fixo		:= nvl(:new.cd_comp_custo_ind_fixo,30);
:new.cd_comp_desp_fixas		:= nvl(:new.cd_comp_desp_fixas, 40);
:new.cd_comp_perc_margem		:= nvl(:new.cd_comp_perc_margem,70);

END;
/

