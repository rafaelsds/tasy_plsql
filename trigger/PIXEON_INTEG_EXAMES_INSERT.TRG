create or replace trigger pixeon_integ_exames_insert
after insert on pixeon_integ_exames
for each row

declare

dt_baixa_w		date;

begin

if	(:new.na_accessionnumber is not null) and
	(:new.na_datetimeperformed is not null) then
	
	dt_baixa_w :=  to_date(:new.na_datetimeperformed, 'YYYYMMDDHH24MISS');
	
	update	prescr_procedimento
	set	dt_baixa = dt_baixa_w,
		cd_motivo_baixa = 1
	where	nr_acesso_dicom = :new.na_accessionnumber;
	
end if;


end;
/


