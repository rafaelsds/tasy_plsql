create or replace trigger evolucao_paciente_delete
before delete on evolucao_paciente
for each row

declare

PRAGMA AUTONOMOUS_TRANSACTION;

ie_perm_exc_evol_lib_w	varchar2(1);
ie_possui_w varchar2(1);
qt_reg_w	number(1);
soap_data_w number;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

Obter_param_Usuario(916, 1077,  obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_perm_exc_evol_lib_w);

if	( :old.dt_liberacao is not null) and
	(ie_perm_exc_evol_lib_w = 'N') then
	Wheb_mensagem_pck.exibir_mensagem_abort(248453);
end if;

select 	nvl(max('S'),'N')
into	ie_possui_w
from 	evolucao_paciente_compl
where 	nr_seq_evo_paciente = :old.cd_evolucao;

if(ie_possui_w = 'S') then

	delete from evolucao_paciente_compl
	where nr_seq_evo_paciente = :old.cd_evolucao;
	
	commit;
	
end if;

select   count(*)
into soap_data_w
from
    clinical_note_soap_data
where
    cd_evolucao = :old.cd_evolucao;

if ( soap_data_w > 0 ) then
    clinical_notes_pck.update_clinical_note_code(:old.cd_evolucao, null);
    commit;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/