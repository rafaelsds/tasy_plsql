create or replace trigger cep_bairro_insert
before insert or update on cep_bairro
for each row

begin

if	not obter_se_uf_valido(:NEW.ds_uf) then
	wheb_mensagem_pck.exibir_mensagem_abort(1023731, null);
end if;	

end;
/