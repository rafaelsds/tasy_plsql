CREATE OR REPLACE TRIGGER Atendimento_perda_ganho_update
BEFORE UPDATE ON atendimento_perda_ganho
FOR EACH ROW
DECLARE

cd_turno_w			Varchar2(11) := 'a';
cd_turno_ww			Varchar2(11);
cd_setor_atendimento_w		Number(05,0);
cd_estabelecimento_w		Number(05,0);
dt_inicial_w			Date;
dt_final_w			Date;
dt_atual_w			Date;
dt_entrada_w			Date;
ie_volume_ocorrencia_w		varchar2(1);
qt_conv_peso_volume_w		number(15,4);
qt_hora_retroativa_w		Number(15,4);

ds_hora_w			varchar2(20);
dt_registro_w			date;
dt_apap_w			date;
qt_hora_w			number(15,2);
qt_reg_w	number(1);
nr_medida_w			number(10);
ie_regra_apap_ganho_perda_w	varchar2(10);
ie_perda_ganho_dt_evento_w	varchar2(1);

cursor C01 is
	Select	to_date('01/01/1999' || ' ' || to_char(dt_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_inicial,
		to_date('01/01/1999' || ' ' || to_char(dt_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_final,
		to_date('01/01/1999' || ' ' || to_char(:new.dt_medida,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_atual, 
		cd_turno
	from	regra_turno_gp
	where	cd_estabelecimento	= cd_estabelecimento_w
	and		(cd_setor_atendimento is null or cd_setor_atendimento = cd_setor_atendimento_w)
	order by nvl(cd_setor_atendimento,0);
	
	
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if(:new.dt_liberacao is null) then
	Obter_Param_Usuario(1113, 690, obter_perfil_ativo, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_perda_ganho_dt_evento_w);
end if;

select	dt_entrada
into	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;
	
if	((dt_entrada_w > :new.dt_medida) or
	(sysdate < :new.dt_medida)) and
	(wheb_usuario_pck.get_cd_funcao <> 88) and
	(ie_perda_ganho_dt_evento_w <> 'S')	then
	--'A data da medida deve estar entre a data de inicio do atendimento e a data atual');
	Wheb_mensagem_pck.exibir_mensagem_abort(236035);
end if;

if	(:old.dt_liberacao is null) then
	qt_hora_retroativa_w		:= Obter_Valor_Param_Usuario(281,1069,obter_Perfil_Ativo,:new.nm_usuario,0);
	if	(qt_hora_retroativa_w is not null) and
		(:new.dt_medida < (sysdate - qt_hora_retroativa_w/24)) then
		--'A data da medida nao pode ser menor que '|| to_char(qt_hora_retroativa_w)||' hora(s) em relacao a data atual.#@#@');
		Wheb_mensagem_pck.exibir_mensagem_abort(236036,'QT_HORAS='||qt_hora_retroativa_w);
	end if;
end if;

select	max(a.ie_volume_ocorrencia),
	max(a.qt_conv_peso_volume)
into	ie_volume_ocorrencia_w,
	qt_conv_peso_volume_w
from	tipo_perda_ganho a
where	a.nr_sequencia = :new.nr_seq_tipo;

if	(:new.dt_liberacao is null) and
	(ie_volume_ocorrencia_w = 'P') and
	(qt_conv_peso_volume_w > 0) and
	(:new.qt_peso > 0) then
	begin
	:new.qt_volume	:= :new.qt_volume + (:new.qt_peso * qt_conv_peso_volume_w);
	end;
end if;	
	


select	cd_estabelecimento,
	Obter_Setor_Atendimento(:new.nr_atendimento)
into	cd_estabelecimento_w,
	cd_setor_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

OPEN  C01;
LOOP
FETCH C01 into	dt_inicial_w,
		dt_final_w,
		dt_atual_w,
		cd_turno_ww;
EXIT when C01%notfound;

	if	((dt_atual_w >= dt_inicial_w) and (dt_atual_w <= dt_final_w)) or
		((dt_final_w < dt_inicial_w) and
		 ((dt_atual_w >= dt_inicial_w) or (dt_atual_w <= dt_final_w))) then
		cd_turno_w := cd_turno_ww;
	end if;
		
END LOOP;
CLOSE C01;

if	(cd_turno_w = 'a') then
	cd_turno_w := cd_turno_ww;
end if;

:new.cd_turno			:= cd_turno_w;
:new.cd_setor_atendimento	:= cd_setor_atendimento_w;

if	(:new.nr_hora is null) or 
	(:new.dt_medida <> :old.dt_medida) then
	begin
	:new.nr_hora	:= Obter_Hora_Apap_GP(:new.dt_medida);
	end;
end if;

select	nvl(max(ie_regra_apap_ganho_perda),'R')
into	ie_regra_apap_ganho_perda_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;


if	(ie_regra_apap_ganho_perda_w	= 'R') then

	nr_medida_w	:= to_number(to_char(round(:new.dt_medida,'hh24'),'hh24'));

else
	nr_medida_w	:= to_number(to_char(trunc(:new.dt_medida,'hh24'),'hh24'));

end if;


if	(:new.nr_hora not between nr_medida_w - 1 and nr_medida_w + 1) then
	--A hora informada para APAP nao condiz com a data da medida.#@#@
	Wheb_mensagem_pck.exibir_mensagem_abort(263489);
end if;

if	(:new.nr_hora is not null) and
	((:old.nr_hora is null) or
	 (:old.dt_medida is null) or
	(:new.nr_hora <> :old.nr_hora) or
	 (:new.dt_medida <> :old.dt_medida)) then
	begin
	ds_hora_w	:= substr(obter_valor_dominio(2119,:new.nr_hora),1,2);
	dt_registro_w	:= trunc(:new.dt_medida,'hh24');
	dt_apap_w	:= to_date(to_char(:new.dt_medida,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss');
	if	(to_char(round(:new.dt_medida,'hh24'),'hh24') = ds_hora_w) then
		:new.dt_apap	:= round(:new.dt_medida,'hh24'); 
	else
		begin
		qt_hora_w	:= (trunc(:new.dt_medida,'hh24') - to_date(to_char(:new.dt_medida,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss')) * 24;
		if	(qt_hora_w > 12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida + 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w > 0) and
			(qt_hora_w <= 12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w >= -12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w < -12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida - 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end if;
	end;
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/
