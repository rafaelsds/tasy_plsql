create or replace trigger desenv_story_sprint_atual
after insert or update on desenv_story_sprint
for each row

declare

cd_tipo_w	varchar2(3);

begin

if	(inserting) then
	cd_tipo_w := 'SC';
else
	cd_tipo_w := 'SU';
end if;

insert into desenv_atividade(
	nr_sequencia,
	nr_epic,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_release,
	nr_release_destino,
	nr_art,
	nr_art_destino,
	cd_tipo,
	nr_feature,
	nr_story,
	cd_status,
	nr_sprint)
values(	desenv_atividade_seq.nextval, 
	null, 
	sysdate, 
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	null,
	null,
	null,
	null,
	cd_tipo_w,
	null,
	:new.nr_story,
	:new.cd_status,
	:new.nr_sprint);

end;
/