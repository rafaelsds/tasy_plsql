create or replace trigger Movto_Trans_Financ_atual
before insert or update on Movto_Trans_Financ
for each row

declare

nm_tabela_w      ctb_documento.nm_tabela%type;
vl_movimento_w    ctb_documento.vl_movimento%type;
nr_seq_tab_compl_w  ctb_documento.nr_seq_doc_compl%type;
nr_seq_info_w    ctb_documento.nr_seq_info%type;
nr_documento_w    ctb_documento.nr_documento%type;
nr_doc_analitico_w  ctb_documento.nr_doc_analitico%type;

cursor c01 is
  select  a.nm_atributo,
      a.cd_tipo_lote_contab
  from  atributo_contab a
  where   a.cd_tipo_lote_contab = 10
  and   a.nm_atributo in ( 'VL_TRANSACAO', 'VL_CHEQUE_DEPOSITO', 'VL_ESPECIE_DEPOSITO')
  and    :new.nr_seq_saldo_caixa is not null
  and    :new.dt_fechamento_lote is not null
  and not exists (select 1
                 from    ctb_documento b
                 where   b.nr_documento = :new.nr_seq_saldo_caixa
                 and     b.nr_seq_doc_compl = :new.nr_sequencia
				 and	 b.nr_doc_analitico is null
                 and     b.cd_tipo_lote_contabil = a.cd_tipo_lote_contab
                 and     b.vl_movimento = :new.vl_transacao);

c01_w    c01%rowtype;

cursor c02 is
  select  a.vl_cheque * decode(:new.ie_estorno, 'E', -1, 1) vl_movimento,
      a.nr_seq_cheque
  from  cheque_cr a,
      deposito_cheque f
  where  f.nr_seq_cheque    = a.nr_seq_cheque
  and   f.nr_seq_deposito  = :new.nr_seq_deposito
  and   a.vl_cheque <> 0
  and not exists (select 1
                 from    ctb_documento b
                 where   b.nr_documento = :new.nr_seq_saldo_caixa
                 and     b.nr_seq_doc_compl = :new.nr_sequencia
				 and	 b.nr_doc_analitico = a.nr_seq_cheque
                 and     b.cd_tipo_lote_contabil = 10
                 and     b.vl_movimento = a.vl_cheque * decode(:new.ie_estorno, 'E', -1, 1));

c02_w    c02%rowtype;

cursor c04 is
  select  a.vl_especie * decode(:new.ie_estorno, 'E', -1, 1) vl_movimento,
      a.nr_sequencia
  from  deposito a
  where  a.nr_sequencia = :new.nr_seq_deposito
  and  nvl(a.vl_especie,0)  > 0
  and not exists (select 1
                 from    ctb_documento b
                 where   b.nr_documento = :new.nr_seq_saldo_caixa
                 and     b.nr_seq_doc_compl = :new.nr_sequencia
				 and	 b.nr_doc_analitico = a.nr_sequencia
                 and     b.cd_tipo_lote_contabil = 10
                 and     b.vl_movimento = a.vl_especie * decode(:new.ie_estorno, 'E', -1, 1));

c04_w    c04%rowtype;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

  if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado. 
  end if;

  if  (inserting) and (:new.DT_TRANSACAO < to_date('01/01/1980','dd/mm/yyyy')) then
    wheb_mensagem_pck.exibir_mensagem_abort(190183,'DT_TRANSACAO=' || to_char(:new.DT_TRANSACAO, 'dd/mm/yyyy'));
    --r aise_application_error(-20011, 'Nao e possivel gerar um saldo com data de ' || to_char(:new.DT_TRANSACAO, 'dd/mm/yyyy'));
  end if;

  if  (:new.nr_seq_pagador is not null) and
    (:new.nr_seq_pagador <> :old.nr_seq_pagador)then
    begin
    select  max(a.nr_sequencia)
    into  :new.nr_seq_segurado
    from  pls_segurado  a
    where  a.nr_seq_pagador  = :new.nr_seq_pagador;
    exception
    when others then
      null;
    end;
  end if;

  open c01;
  loop
  fetch c01 into
    c01_w;
  exit when c01%notfound;
    begin

    nm_tabela_w :=   case c01_w.nm_atributo
        when 'VL_TRANSACAO' then 'CAIXA_SALDO_DIARIO'
        when 'VL_CHEQUE_DEPOSITO' then 'MOVTO_TRANS_FINANC'
        when 'VL_ESPECIE_DEPOSITO' then 'MOVTO_TRANS_FINANC'
        end;

    vl_movimento_w    := 0;
    nr_documento_w    := :new.nr_seq_saldo_caixa;
    nr_seq_tab_compl_w  := :new.nr_sequencia;
    nr_doc_analitico_w  := null;

    if  (c01_w.nm_atributo = 'VL_TRANSACAO') then
      begin
      vl_movimento_w  := :new.vl_transacao;
      nr_seq_info_w  := 4;
      end;
    elsif  (c01_w.nm_atributo = 'VL_CHEQUE_DEPOSITO') then
      begin
      vl_movimento_w  := null;

      if  (nvl(:new.nr_seq_deposito, 0) <> 0) and (nvl(:new.nr_seq_trans_financ, 0) <> 0) then
        begin

        begin

        nr_seq_info_w  := 61;
        vl_movimento_w  := null;

        open c02;
        loop
        fetch c02 into
          c02_w;
        exit when c02%notfound;
          begin
          if  (c02_w.vl_movimento <> 0) then
            ctb_concil_financeira_pck.ctb_gravar_documento  (  :new.cd_estabelecimento,
                          trunc(nvl(:new.dt_referencia_saldo,:new.dt_transacao)),
                          c01_w.cd_tipo_lote_contab,
                          :new.nr_seq_trans_financ,
                          nr_seq_info_w,
                          nr_documento_w,
                          nr_seq_tab_compl_w,
                          c02_w.nr_seq_cheque,
                          c02_w.vl_movimento,
                          nm_tabela_w,
                          c01_w.nm_atributo,
                          :new.nm_usuario);
          end if;
          end;
        end loop;
        close c02;

        exception
        when others then
          nr_seq_info_w:=  null;
          vl_movimento_w:= null;
        end;

        end;
      end if;
      end;
    elsif  (c01_w.nm_atributo = 'VL_ESPECIE_DEPOSITO') then
      begin
      if  (nvl(:new.nr_seq_deposito, 0) <> 0) and (nvl(:new.nr_seq_trans_financ, 0) <> 0) then
        begin

        begin

        nr_seq_info_w  := 62;
        vl_movimento_w := null;

        open c04;
        loop
        fetch c04 into
          c04_w;
        exit when c04%notfound;
          begin
          if  (nvl(c02_w.vl_movimento,0) <> 0) then
            ctb_concil_financeira_pck.ctb_gravar_documento  (  :new.cd_estabelecimento,
                          trunc(nvl(:new.dt_referencia_saldo,:new.dt_transacao)),
                          c01_w.cd_tipo_lote_contab,
                          :new.nr_seq_trans_financ,
                          nr_seq_info_w,
                          nr_documento_w,
                          nr_seq_tab_compl_w,
                          c04_w.nr_sequencia,
                          c04_w.vl_movimento,
                          nm_tabela_w,
                          c01_w.nm_atributo,
                          :new.nm_usuario);
          end if;
          end;
        end loop;
        close c04;

        exception
        when others then
          nr_seq_info_w  := null;
          vl_movimento_w  := null;
        end;

        end;
      end if;
      end;
    end if;


    if  (nvl(vl_movimento_w, 0) <> 0) then
      begin

      ctb_concil_financeira_pck.ctb_gravar_documento  (  nvl(:new.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento),
                    trunc(nvl(:new.dt_referencia_saldo,:new.dt_transacao)),
                    c01_w.cd_tipo_lote_contab,
                    :new.nr_seq_trans_financ,
                    nr_seq_info_w,
                    nr_documento_w,
                    nr_seq_tab_compl_w,
                    nr_doc_analitico_w,
                    vl_movimento_w,
                    nm_tabela_w,
                    c01_w.nm_atributo,
                    :new.nm_usuario);
      end;
    end if;

    end;
  end loop;
  close c01;
end if;

end;
/
