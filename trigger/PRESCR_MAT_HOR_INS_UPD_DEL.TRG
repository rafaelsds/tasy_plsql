create or replace trigger prescr_mat_hor_ins_upd_del
before insert or update or delete on prescr_mat_hor
for each row
declare
	cd_estabelecimento_w 		estabelecimento.cd_estabelecimento%type;
	cd_perfil_w					perfil.cd_perfil%type;
    nm_usuario_w    			usuario.nm_usuario%type;
	ie_ajustar_disp_w			varchar2(255 byte);
	ie_info_rastre_prescr_w		varchar2(1 char);
	ds_alteracao_rastre_w		varchar2(2000 char);
	qt_itens_lote_w				number(5);
begin	
	cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
	cd_perfil_w				:= obter_perfil_ativo;
	nm_usuario_w			:= wheb_usuario_pck.get_nm_usuario;

	ie_info_rastre_prescr_w := obter_se_info_rastre_prescr('L', nm_usuario_w, cd_perfil_w, cd_estabelecimento_w);
	
	if (ie_info_rastre_prescr_w = 'S') then
		obter_param_usuario(924, 179, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_ajustar_disp_w);
				
		if (not deleting) then
			if ((:new.qt_dispensar_hor is null) or 
				(ie_ajustar_disp_w <> 'N' and trunc(:new.qt_dispensar_hor) <> :new.qt_dispensar_hor) or
				(updating and nvl(:old.qt_dispensar_hor,-1) <> nvl(:new.qt_dispensar_hor,-1))) then
				ds_alteracao_rastre_w := substr(ds_alteracao_rastre_w || 'qt_dispensar_hor(',1,2000);
				
				if (updating) then
					ds_alteracao_rastre_w := substr(ds_alteracao_rastre_w || to_char(:old.qt_dispensar_hor) || '/',1,2000);
				end if;
				
				ds_alteracao_rastre_w := substr(ds_alteracao_rastre_w || to_char(:new.qt_dispensar_hor) || '); ' || chr(13),1,2000);
			end if;
		else
			ds_alteracao_rastre_w := 'DELETE' || chr(13);
		end if;

		if (ds_alteracao_rastre_w is not null) then
			ds_alteracao_rastre_w := substr('Alteracoes / prescr_mat_hor_ins_upd_del = ' || chr(13) ||
				ds_alteracao_rastre_w ||
				'FUNCAO(' || to_char(obter_funcao_ativa) || '); ' ||
				'PERFIL(' || to_char(obter_perfil_ativo) || ')',1,2000);
			
			gerar_log_prescricao(
				nr_prescricao_p		=> :new.nr_prescricao,
				nr_seq_item_p		=> :new.nr_seq_material,
				ie_agrupador_p		=> :new.ie_agrupador,
				nr_seq_horario_p	=> :new.nr_sequencia,
				ie_tipo_item_p		=> null,
				ds_log_p			=> ds_alteracao_rastre_w,
				nm_usuario_p		=> nm_usuario_w,
				nr_seq_objeto_p		=> 80667,
				ie_commit_p			=> 'N');
		end if; 
	end if;
	
	if	(deleting) and 
		(:old.nr_seq_lote is not null) then
		
		excluir_horario_lote_prescr(:old.nr_seq_lote,:old.nr_sequencia,nvl(nm_usuario_w,:old.nm_usuario));
	end if;
exception
when others then
	gravar_log_tasy(88098, to_char(sqlerrm), nm_usuario_w);
end;
/
