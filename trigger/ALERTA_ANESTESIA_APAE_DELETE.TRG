create or replace trigger ALERTA_ANESTESIA_APAE_DELETE
after delete on ALERTA_ANESTESIA_APAE
for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_registro = 'XAPN'
and	nr_seq_registro  = :old.nr_sequencia
and     nvl(ie_tipo_pendencia, 'L') =  'L';

commit;		

<<Final>>
qt_reg_w	:= 0;
end;
/
