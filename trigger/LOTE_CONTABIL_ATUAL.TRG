create or replace trigger lote_contabil_atual
before insert or update on lote_contabil
for each row

declare

qt_registro_w		number(10);
cd_empresa_w		ctb_mes_ref.cd_empresa%type;
dt_referencia_w		ctb_mes_ref.dt_referencia%type;

begin

if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then
	if (nvl(:new.nr_seq_mes_ref, 0) <> 0) then
		select  a.cd_empresa,
			a.dt_referencia
		into	cd_empresa_w,
			dt_referencia_w
		from    ctb_mes_ref a
		where   a.nr_sequencia = :new.nr_seq_mes_ref;

		if	(obter_empresa_estab(:new.cd_estabelecimento) <>  cd_empresa_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(1076351);
		end if;

		if (to_char(:new.dt_referencia, 'mm/yyyy') <> to_char(dt_referencia_w, 'mm/yyyy')) then
			wheb_mensagem_pck.exibir_mensagem_abort(1153801,'DT_INICIAL=' 	|| to_char(pkg_date_utils.start_of(dt_referencia_w, 'MONTH'),'dd/mm/yyyy') ||
									';DT_FINAL=' 	|| to_char(pkg_date_utils.end_of(dt_referencia_w, 'MONTH'),'dd/mm/yyyy'));
		end if;
	end if;

	if	(updating) then
		if	(:new.nr_seq_mes_ref is null) and
			(:old.nr_seq_mes_ref is not null) then
			select	count(*)
			into	qt_registro_w
			from 	ctb_movimento
			where 	nr_lote_contabil = :new.nr_lote_contabil;

			if	(qt_registro_w > 0) then
				/* O lote possui movimento na contabilidade, reatualize a consulta da tela.*/
				wheb_mensagem_pck.exibir_mensagem_abort(266446);
			end if;
		end if;
	end if;
end if;
end;
/
