create or replace trigger bef_upd_ins_parametro_nfs_cred
before update or insert on parametro_nfs_credito
for each row

declare
qt_registros_tipo_fatura_w	number(2) := 0;

pragma autonomous_transaction;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if (inserting) then

		select count(*) qt
		into qt_registros_tipo_fatura_w
		from parametro_nfs_credito
		where cd_convenio = :new.cd_convenio
		and cd_estabelecimento = :new.cd_estabelecimento
		and ie_tipo_fatura = :new.ie_tipo_fatura;
		
	elsif (updating) then
	
		select count(*) qt
		into qt_registros_tipo_fatura_w
		from parametro_nfs_credito
		where cd_convenio = :new.cd_convenio
		and cd_estabelecimento = :new.cd_estabelecimento
		and ie_tipo_fatura = :new.ie_tipo_fatura
		and nr_sequencia <> :new.nr_sequencia;
	
	end if;
	
	if (qt_registros_tipo_fatura_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1169838);
	end if;

end if;


end;
/
