CREATE OR REPLACE 
TRIGGER solic_compra_item_afeterinsert
AFTER INSERT on solic_compra_item
for each row

declare
ds_compl_item_w		gpi_orc_item.ds_compl_item%type;


begin

if	(:new.nr_seq_orc_item_gpi > 0) then
	
	select	ds_compl_item
	into	ds_compl_item_w
	from	gpi_orc_item
	where	nr_sequencia = :new.nr_seq_orc_item_gpi;
	
	if	(ds_compl_item_w is not null) then
	
		insert into solic_compra_item_detalhe(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_solic_compra,
			nr_item_solic_compra,
			ds_detalhe)
		values(	solic_compra_item_detalhe_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario_nrec,
			:new.nr_solic_compra,
			:new.nr_item_solic_compra,
			ds_compl_item_w);
	end if;
end if;

END;
/
