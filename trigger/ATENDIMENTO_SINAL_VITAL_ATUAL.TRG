CREATE OR REPLACE TRIGGER Atendimento_sinal_vital_Atual
BEFORE INSERT OR UPDATE ON Atendimento_sinal_vital
FOR EACH ROW
DECLARE

ds_hora_w	varchar2(20);
dt_registro_w	date;
dt_apap_w	date;
qt_hora_w	number(15,2);
qt_reg_w	number(1);
cd_estabelecimento_w	number(4);
nr_seq_evento_w			number(10);
total_escore_alerta_w	number(10);
qt_idade_w		number(10);
cd_setor_paciente_w	number(10);
ds_alerta_modificado_w	varchar2(512);
ds_retorno_w		varchar2(4000);
ie_retorno_w		varchar2(10);
ie_motivo_dor_inapto_w	varchar2(1);
ds_Alerta_sinais_w	varchar2(4000);
ie_setor_w		varchar2(1);
cd_funcao_ativa_w	number(5);
ie_sepse_lib_sv_w	varchar2(1);
qt_horas_passado_sv_w	number(15,5);
cd_pessoa_fisica_w		atendimento_sinal_vital.cd_pessoa_fisica%type;
nr_atendimento_w		atendimento_sinal_vital.nr_atendimento%type;


Cursor C01 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	=	'APM'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	total_escore_alerta_w between nvl(qt_pto_min,0)	and nvl(qt_pto_max,9999)	
	and	nvl(cd_setor_atendimento,cd_setor_paciente_w)	= cd_setor_paciente_w
	and	nvl(ie_situacao,'A') = 'A';
	
Cursor C02 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	=  'CSVR'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(cd_setor_atendimento,cd_setor_paciente_w)	= cd_setor_paciente_w
	and	nvl(ie_situacao,'A') = 'A';	

BEGIN
cd_funcao_ativa_w := obter_funcao_ativa;
Obter_Param_Usuario(872,485,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_setor_w);
ds_alerta_modificado_w	:= '';
qt_idade_w	:= nvl(obter_idade_pf(:new.CD_PACIENTE,sysdate,'A'),0);
:new.ie_rn	:= nvl(:new.ie_rn,'N');

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

-- Verificar se os valores das variaveis correspondem ao agendamento do paciente selecionado caso nao limpara as variaveis
if ((:new.nr_seq_agendamento is not null) and (:new.nr_tipo_agenda is not null)) then
	
	if (:new.nr_tipo_agenda in (3, 5)) then 
		
		select 	cd_pessoa_fisica,
				nr_atendimento
		into	cd_pessoa_fisica_w,
				nr_atendimento_w
		from	agenda_consulta
		where 	nr_sequencia = :new.nr_seq_agendamento;
		
	elsif (:new.nr_tipo_agenda in (1, 2)) then
		
		select  cd_pessoa_fisica,
				nr_atendimento
		into	cd_pessoa_fisica_w,
				nr_atendimento_w
		from    agenda_paciente
		where 	nr_sequencia = :new.nr_seq_agendamento;
	
	end if;
	
	if ((cd_pessoa_fisica_w is not null) and (nr_atendimento_w is null or nr_atendimento_w <> :new.nr_atendimento)) then
		:new.nr_seq_agendamento := null;
		:new.nr_tipo_agenda := null;
	end if;

end if;


if	(nvl(:old.dt_sinal_vital,sysdate+10) <> :new.dt_sinal_vital) and
	(:new.dt_sinal_vital is not null) then
	:new.ds_utc		:= obter_data_utc(:new.dt_sinal_vital,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

select 	max(ie_motivo_dor_inapto),
	nvl(max(qt_horas_passado_sv),0)
into	ie_motivo_dor_inapto_w,
	qt_horas_passado_sv_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if 	((:new.dt_liberacao is not null) and  (:old.dt_liberacao is null)) and
	( ie_motivo_dor_inapto_w = 'S') and
	((:new.cd_escala_dor = 'IA') and (:new.nr_seq_inapto_dor is null))then
	-- E necessario informar o motivo do paciente estar inapto para a avaliacao da dor.
	Wheb_mensagem_pck.exibir_mensagem_abort(264596);
end if;

if	((:new.dt_liberacao is not null) and  
	 (:old.dt_liberacao is null)) then
	 	 
	 If	(:new.cd_escala_dor is null) then
	 
		:new.nr_seq_result_dor  	:= null;
		:new.nr_seq_topografia_dor 	:= null;
		:new.ie_lado 			:= null;
		:new.nr_seq_condicao_dor 	:= null;
		:new.qt_escala_dor		:= null;
		:new.nr_seq_inapto_dor		:= null;
		
		
	else    
                    if :new.cd_escala_dor = 'CFT' then      
                       update ESCALA_COMFORT_B
                       set  dt_liberacao = :new.dt_liberacao
                       where nr_seq_sv = :new.nr_sequencia;    
                    end if;
               end  if;
	
	if	(:new.cd_escala_dor <> 'IA') and
		(:new.cd_escala_dor is not null) and
		(:new.qt_escala_dor is null) then
		-- E necessario informar a pontuacao da escala de dor selecionada.
		Wheb_mensagem_pck.exibir_mensagem_abort(264598);
		
		/*:new.nr_seq_result_dor  	:= null;
		:new.nr_seq_topografia_dor 	:= null;
		:new.ie_lado 			:= null;
		:new.nr_seq_condicao_dor 	:= null;
		:new.cd_escala_dor		:= null;
		:new.nr_seq_inapto_dor		:= null;*/	
	
	end if;
	
	if	(:new.cd_escala_dor = 'IA')  then
		
		--:new.cd_escala_dor		:= null;
		:new.nr_seq_result_dor  	:= null;
		:new.nr_seq_topografia_dor 	:= null;
		:new.ie_lado 			:= null;
		:new.nr_seq_condicao_dor 	:= null;				
	
	end if;
		
end if;

if	(:new.cd_escala_dor is not null) and
	(:new.qt_escala_dor is not null) and
	(:new.nr_seq_result_dor is null) then
	
	:new.nr_seq_result_dor := Obter_Seq_Result_Dor(:new.cd_escala_dor,:new.qt_escala_dor);
	
end if;

 
if	(:new.nr_Atendimento is not null) and
	(((nvl(cd_funcao_ativa_w,0) = 872) and (ie_setor_w = 'N')) or (nvl(cd_funcao_ativa_w,0) <> 872)) and
	(:new.cd_setor_atendimento is null)then
	begin
	:new.cd_setor_atendimento 	:= obter_setor_atendimento(:new.nr_atendimento);
	exception
	when others then
		null;
	end;
end if;

if	(:new.cd_paciente is null) and
	(:new.nr_atendimento is not null) then
	:new.cd_paciente	:= obter_Pessoa_Atendimento(:new.nr_atendimento,'C');
end if;

if	(:new.nr_hora is null) or 
	(:new.dt_sinal_vital <> :old.dt_sinal_vital) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.dt_sinal_vital,'hh24'),'hh24'));
	end;
end if;

if	(:new.nr_hora is not null) and
	((:old.nr_hora is null) or
	 (:old.dt_sinal_vital is null) or
	 (:new.nr_hora <> :old.nr_hora) or
	 (:new.dt_sinal_vital <> :old.dt_sinal_vital)) then
	begin
	ds_hora_w	:= lpad(:new.nr_hora,2,'0');
	dt_registro_w	:= trunc(:new.dt_sinal_vital,'hh24');
	dt_apap_w	:= to_date(to_char(:new.dt_sinal_vital,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss');
	if	(to_char(round(:new.dt_sinal_vital,'hh24'),'hh24') = ds_hora_w) then
		:new.dt_referencia	:= round(:new.dt_sinal_vital,'hh24'); 
	else
		begin
		qt_hora_w	:= (trunc(:new.dt_sinal_vital,'hh24') - to_date(to_char(:new.dt_sinal_vital,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss')) * 24;
		if	(qt_hora_w > 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital + 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w > 0) and
			(qt_hora_w <= 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w >= -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w < -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital - 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end if;
	end;
end if;
total_escore_alerta_w:=0;
if 	((:new.qt_freq_cardiaca >= 51) and
	(:new.qt_freq_cardiaca 	<= 100)) then
		total_escore_alerta_w:=0;
elsif	((:new.qt_freq_cardiaca >= 41)	and
	(:new.qt_freq_cardiaca	<= 50))	or
	((:new.qt_freq_cardiaca >= 101)	and
	(:new.qt_freq_cardiaca	<= 110))	then
		total_escore_alerta_w:= total_escore_alerta_w + 1;
		ds_alerta_modificado_w := Obter_desc_expressao(290490,'Freq. cardiaca') || ' = ' ||  :new.qt_freq_cardiaca; 
elsif	(:new.qt_freq_cardiaca 	<= 40)	or
	((:new.qt_freq_cardiaca >= 111)	and
	(:new.qt_freq_cardiaca	<= 120))	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := Obter_desc_expressao(290490,'Freq. cardiaca') || ' = ' || :new.qt_freq_cardiaca;
elsif	(:new.qt_freq_cardiaca 	>  120)	then
		total_escore_alerta_w:= total_escore_alerta_w + 3;
		ds_alerta_modificado_w := Obter_desc_expressao(290490,'Freq. cardiaca') || ' = ' || :new.qt_freq_cardiaca;
end if;

if 	((:new.qt_freq_resp 	>= 10) and
	(:new.qt_freq_resp 	<= 14)) then
		total_escore_alerta_w:= total_escore_alerta_w + 0;
elsif	((:new.qt_freq_resp 	>= 15)	and
	(:new.qt_freq_resp	<= 20))	then
		total_escore_alerta_w:= total_escore_alerta_w + 1;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(557498,'Freq. respiratoria') || ' = ' || :new.qt_freq_resp;
elsif	(:new.qt_freq_resp 	<= 9)	or
	((:new.qt_freq_resp 	>= 21)	and
	(:new.qt_freq_resp	<= 29))	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(557498,'Freq. respiratoria') || ' = ' || :new.qt_freq_resp;
elsif	(:new.qt_freq_resp 	>  30)	then
		total_escore_alerta_w:= total_escore_alerta_w + 3;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(557498,'Freq. respiratoria') || ' = ' || :new.qt_freq_resp;
end if;

if 	((:new.qt_pa_sistolica >= 101) and
	(:new.qt_pa_sistolica <= 199)) then
		total_escore_alerta_w:= total_escore_alerta_w + 0;
elsif	((:new.qt_pa_sistolica 	>= 81)	and
	(:new.qt_pa_sistolica	<= 100))	then
		total_escore_alerta_w:= total_escore_alerta_w + 1;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(310302,'Pa. sistolica') || ' = ' || :new.qt_pa_sistolica;
elsif	((:new.qt_pa_sistolica 	>= 71)	and
	(:new.qt_pa_sistolica 	<= 80))	or
	(:new.qt_pa_sistolica 	>= 200)	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(310302,'Pa. sistolica') || ' = ' || :new.qt_pa_sistolica;
elsif	(:new.qt_pa_sistolica 	<= 70)	then
		total_escore_alerta_w:= total_escore_alerta_w + 3;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(310302,'Pa. sistolica') || ' = ' || :new.qt_pa_sistolica;
end if;

if 	(:new.ie_nivel_consciencia 	= '0') then
		total_escore_alerta_w:= total_escore_alerta_w + 0;
elsif	(:new.ie_nivel_consciencia 	= '1')	then
		total_escore_alerta_w:= total_escore_alerta_w + 1;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) ||  Obter_desc_expressao(294136,null) || ' = ' || :new.ie_nivel_consciencia;
elsif	(:new.ie_nivel_consciencia 	= '2')	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) ||  Obter_desc_expressao(294136,null) || ' = ' || :new.ie_nivel_consciencia;
elsif	(:new.ie_nivel_consciencia 	= '3')	then
		total_escore_alerta_w:= total_escore_alerta_w + 3;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) ||  Obter_desc_expressao(294136,null) || ' = ' || :new.ie_nivel_consciencia;
end if;

if 	((:new.qt_temp >= 35.1) and
	(:new.qt_temp <= 37.8)) then
		total_escore_alerta_w:= total_escore_alerta_w + 0;
elsif	((:new.qt_temp 	<= 35)	or
	(:new.qt_temp	>= 37.9))	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(299207,'Temperatura') || ' = ' || :new.qt_temp;
end if;

if	(:new.qt_pa_diastolica is not null) and
	(:new.qt_pa_sistolica is not null) and
	(:new.qt_pam is null) then
	:new.qt_pam 	:=	((:new.qt_pa_sistolica +(:new.qt_pa_diastolica * 2)) / 3);
end if;

if 	((:new.dt_liberacao is not null) and  (:old.dt_liberacao is null)) 	then
	begin
		cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
		begin
		cd_setor_paciente_w	:= obter_setor_atendimento(:new.nr_atendimento);
		exception
			when others then
			null;
		end;
		open C01;
			loop
			fetch C01 into
				nr_seq_evento_w;
			exit when C01%notfound;
				begin
				gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,:new.cd_paciente,null,:new.nm_usuario,ds_alerta_modificado_w,:new.dt_sinal_vital);
				end;
			end loop;
		close C01;
	end;
end if;


if (((:new.dt_liberacao is not null) and  (:old.dt_liberacao is null)) and 
		(:new.QT_TEMP > 38 OR :new.QT_TEMP < 36) OR
		(:new.QT_FREQ_RESP > 20) OR
		(:new.QT_FREQ_CARDIACA > 100) OR
		(:new.QT_PA_SISTOLICA < 90) OR
		(:new.QT_PAM < 65)) then
	begin
		cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
		SELECT	SUBSTR(CASE WHEN (:new.QT_TEMP > 38 OR :new.QT_TEMP < 36 ) THEN Obter_desc_expressao(299207,'Temperatura') || ': ' || :new.QT_TEMP || 
		SUBSTR(Obter_Atrib_Unid_Med_Unica('ATENDIMENTO_SINAL_VITAL','QT_TEMP',:new.DT_ATUALIZACAO, 1),1,30) || CHR(13) || CHR(10) ELSE '' END ||
		CASE WHEN (:new.QT_FREQ_RESP > 20) THEN Obter_desc_expressao(557498,null) || ': ' || :new.QT_FREQ_RESP || ' ('||Obter_desc_expressao(558642,'FR') || ')' || CHR(13) || CHR(10)  ELSE '' END ||
		CASE WHEN (:new.QT_FREQ_CARDIACA > 100) THEN Obter_desc_expressao(290490,null) || ': ' || :new.QT_FREQ_CARDIACA || ' ('||Obter_desc_expressao(311848,'FR') || ')' ||  CHR(13) || CHR(10) ELSE '' END||
		CASE WHEN (:new.QT_PA_SISTOLICA < 90) THEN Obter_desc_expressao(295132,'PA max') || ': ' || :new.QT_PA_SISTOLICA || ' (mmHg)' || CHR(13) || CHR(10)  ELSE ''  END ||
		CASE WHEN (:new.QT_PAM < 65) THEN Obter_desc_expressao(295239,'PAM') || ': ' || :new.QT_PAM || ' '|| Obter_desc_expressao(727862,'(mmHg)')|| CHR(13) || CHR(10) ELSE '' END,1,4000) DESCR
		INTO ds_Alerta_sinais_w
		FROM dual;
					
		begin
		cd_setor_paciente_w	:= obter_setor_atendimento(:new.nr_atendimento);
		exception
			when others then
			null;
		end;
		open C02;
			loop
			fetch C02 into
				nr_seq_evento_w;
			exit when C02%notfound;
				begin
				gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,:new.cd_paciente,null,:new.nm_usuario,ds_Alerta_sinais_w,:new.dt_sinal_vital);
				end;
			end loop;
		close C02;
	end;
end if;


if	(:new.qt_peso	is not null) then
	consiste_sinal_vital(	:new.cd_paciente,
				:new.qt_peso,
				12,
				ds_retorno_w,
				ie_retorno_w,
				:new.qt_peso_um,
				:new.nr_atendimento,
				null,
				:new.ie_rn);
	if	(ie_retorno_w	= 'E') then
		Wheb_mensagem_pck.exibir_mensagem_abort(264607,	'DS_RETORNO_W='||DS_RETORNO_W);
	end if;
end if;

if (nvl(:new.qt_peso,0) <> nvl(:old.qt_peso,0)) then
	cpoe_update_peso_atual_dialise( :new.nr_atendimento, :new.dt_sinal_vital, :new.qt_peso);
end if;


if	(:new.qt_peso is not null) and
	(:new.qt_altura_cm is not null) then
	begin
	if	(:new.qt_imc is null) then
		:new.qt_imc	:= Obter_IMC(:new.qt_peso,:new.qt_altura_cm);
	end if;
	
	exception
		when others then
		null;
	end;
	begin
	if	(:new.QT_SUPERF_CORPORIA is null) then
		:new.QT_SUPERF_CORPORIA	:= Obter_Superficie_Corporea(:new.qt_altura_cm,:new.qt_peso);
	end if;
	exception
		when others then
		null;
	end;
	
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.nr_atendimento is not null) then
	
	begin
		gravar_agend_rothman(:new.nr_atendimento,:new.nr_sequencia,'SV',:new.nm_usuario);
	exception
	when others then
		null;
	end;
/*
select	max(nvl(ie_sepse_lib_sv,'N'))
into	ie_sepse_lib_sv_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
	
if	(ie_sepse_lib_sv_w = 'S') and
	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	begin
	gerar_escala_sepse_js(:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nr_sequencia,:new.nm_usuario);
	end;
end if;
*/
end if;

if	(:new.dt_liberacao is null) and
	(:new.dt_inativacao is null) and
	(nvl(:new.ie_integracao,'N')	= 'N') and
	((:new.dt_sinal_vital		> sysdate) or
	(qt_horas_passado_sv_w > 0 and :new.dt_sinal_vital	< sysdate - (qt_horas_passado_sv_w/24) )) then
	Wheb_mensagem_pck.exibir_mensagem_abort(15222,	'QT_HR_PASSADO_SV='||qt_horas_passado_sv_w);
end if;

if (:old.dt_liberacao is null and :new.dt_liberacao is not null) then --Apenas calcular quando liberar


	if (nvl(:new.qt_glicemia_capilar,0) > 0 and nvl(:new.qt_glicemia_mmol,0) = 0) then
		:new.qt_glicemia_mmol := converter_mg_mmol(:new.qt_glicemia_capilar, 'MMOL');
	elsif (nvl(:new.qt_glicemia_mmol,0) > 0 and nvl(:new.qt_glicemia_capilar,0) = 0) then
		:new.qt_glicemia_capilar := converter_mg_mmol(:new.qt_glicemia_mmol, 'MG');
	end if;
	
end if;

if ((:old.dt_liberacao is null and :new.dt_liberacao is not null) or
	(:old.dt_inativacao is null and :new.dt_inativacao is not null)) then 

	send_physio_data_integration(null, null, :new.nr_atendimento, :new.cd_pessoa_fisica, :new.nr_sequencia, :new.dt_sinal_vital, 
	:new.qt_pa_sistolica, :new.qt_pa_diastolica, :new.qt_freq_cardiaca, :new.qt_temp, :new.dt_atualizacao);
	
end if;

if (inserting) and (nvl(:new.ie_integracao,'N')	= 'S') then
   record_integration_notify(:new.cd_paciente,:new.nr_atendimento,'SV',:new.nr_sequencia,null,'S');
end if;   

<<Final>>
qt_reg_w	:= 0;
END;
/