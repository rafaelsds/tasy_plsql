CREATE OR REPLACE TRIGGER PRESCR_PROCEDIMENTO_DELETE
BEFORE DELETE ON Prescr_Procedimento
FOR EACH ROW
DECLARE
ie_erro_w	varchar2(1);
ie_agrupada_w	pre_venda.ie_agrupada%type;
ie_liberada_w				varchar2(1 char);
ie_info_rastre_prescr_w		varchar2(1 char);
ds_alteracao_rastre_w		varchar2(1800 char);
nm_usuario_w				usuario.nm_usuario%type;
cd_perfil_w					perfil.cd_perfil%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

nm_usuario_w			:= nvl(obter_usuario_ativo, 'TASY');
cd_perfil_w				:= obter_perfil_ativo;
cd_estabelecimento_w	:= obter_estabelecimento_ativo;

if (cd_perfil_w = 0 or cd_estabelecimento_w = 0) then

	select	cd_perfil_ativo,
			cd_estabelecimento
	into	cd_perfil_w,
			cd_estabelecimento_w
	from	prescr_medica
	where	nr_prescricao = :old.nr_prescricao;

end if;

delete from PRESCR_PROCED_INF_ADIC
where nr_prescricao = :old.NR_PRESCRICAO
and nr_seq_prescricao = :old.NR_SEQUENCIA;

ie_info_rastre_prescr_w := obter_se_info_rastre_prescr('O', nm_usuario_w, cd_perfil_w, cd_estabelecimento_w);


if	(:old.NR_SEQ_EXAME is not null) then
	begin
	DELETE EXAME_LAB_RESULT_ITEM A
	WHERE A.NR_SEQ_RESULTADO  in (SELECT B.NR_SEQ_RESULTADO 
					     FROM EXAME_LAB_RESULTADO B
					     WHERE B.NR_PRESCRICAO = :old.NR_PRESCRICAO)
	  AND A.NR_SEQ_PRESCR   = :old.NR_SEQUENCIA;
	exception
		when others then
			ie_erro_w := 'S';
	end;
end if;

begin
update	frasco_pato_loc
set	nr_prescr_vinc_patol = null,
	nr_seq_prescr_vinc = null
where	nr_prescr_vinc_patol = :old.nr_prescricao
and	nr_seq_prescr_vinc = :old.nr_sequencia;
exception
	when others then
		ie_erro_w := 'S';
end;

update	ageint_exame_lab
set	nr_prescricao = null
where	nr_sequencia = :old.nr_seq_ageint_exame_lab
and	cd_procedimento = :old.cd_procedimento;


if (ie_info_rastre_prescr_w = 'S') or (nm_usuario_w = 'TASY') then

	select	nvl(max('S'), 'N')
	into	ie_liberada_w
	from	prescr_medica
	where	nr_prescricao = :old.nr_prescricao
	and		nvl(dt_liberacao_medico, dt_liberacao) is not null;

	if (ie_liberada_w = 'S') then

		ds_alteracao_rastre_w := substr('DELETE PRESCR_PROCEDIMENTO - '||pls_util_pck.enter_w
										||'nr_prescricao: '||:old.nr_prescricao||pls_util_pck.enter_w
										||'cd_procedimento: '||:old.cd_procedimento||pls_util_pck.enter_w
										||'dt_atualizacao: '||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_atualizacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||pls_util_pck.enter_w
										||'nm_usuario: '||:old.nm_usuario||pls_util_pck.enter_w
										||'cd_intervalo: '||:old.cd_intervalo||pls_util_pck.enter_w
										||'ds_horarios: '||:old.ds_horarios||pls_util_pck.enter_w
										||'cd_setor_atendimento: '||:old.cd_setor_atendimento||pls_util_pck.enter_w
										||'nr_seq_exame: '||:old.nr_seq_exame||pls_util_pck.enter_w
										||'nr_seq_interno: '||:old.nr_seq_interno||pls_util_pck.enter_w
										||'nr_seq_proc_interno: '||:old.nr_seq_proc_interno||pls_util_pck.enter_w
										||'ie_erro: '||:old.ie_erro||pls_util_pck.enter_w
										||'nr_prescricao_original: '||:old.nr_prescricao_original||pls_util_pck.enter_w
										||'cd_funcao_origem: '||:old.cd_funcao_origem||pls_util_pck.enter_w
										||'nr_seq_proc_cpoe: '||:old.nr_seq_proc_cpoe||pls_util_pck.enter_w
										||'nr_sequencia: '||:old.nr_sequencia||pls_util_pck.enter_w
										||'ds_stack: '||:old.ds_stack||pls_util_pck.enter_w
										||'Stack exclusao: '||substr(dbms_utility.format_call_stack,1,1800),1,2000);

		gravar_log_tasy	(	cd_log_p => 10007,
							ds_log_p => ds_alteracao_rastre_w, 
							nm_usuario_p => nm_usuario_w
						);

	end if;

end if;

end if;

END;
/

