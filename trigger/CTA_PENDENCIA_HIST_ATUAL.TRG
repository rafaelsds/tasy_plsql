CREATE OR REPLACE TRIGGER CTA_PENDENCIA_HIST_ATUAL
BEFORE INSERT ON CTA_PENDENCIA_HIST
FOR EACH ROW
DECLARE

nr_interno_conta_w	Number(10,0);

BEGIN

select 	nvl(max(nr_interno_conta),0)
into	nr_interno_conta_w
from 	cta_pendencia
where 	nr_sequencia = :new.nr_seq_pend;

if	(nr_interno_conta_w > 0) then
	:NEW.VL_CONTA_ESTAGIO:= obter_valor_conta(nr_interno_conta_w,0);
else
	:NEW.VL_CONTA_ESTAGIO:= 0;
end if;

END;
/