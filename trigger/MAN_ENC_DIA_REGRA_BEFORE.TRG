create or replace trigger man_enc_dia_regra_before
before insert or update on man_enc_dia_regra
for each row
declare

qt_dias_regra_w	man_encerrar_os_estag.qt_dias%type;

begin
	if	(phi_is_base_philips = 'S') then
		begin
			begin
				select	nvl(meoe.qt_dias, 0)
				into	qt_dias_regra_w
				from	man_encerrar_os_estag meoe
				where	meoe.nr_sequencia = :new.nr_seq_regra_encerra;
			exception
			when others then
				qt_dias_regra_w := 0;
			end;
			
			if	(qt_dias_regra_w = 0) then
				begin
					wheb_mensagem_pck.exibir_mensagem_abort(1143436);
				end;
			elsif 	(qt_dias_regra_w < :new.qt_dia_comunicado) then
				begin
					wheb_mensagem_pck.exibir_mensagem_abort(1143441);				
				end;
			end if;
		end;
	end if;
end;
/
