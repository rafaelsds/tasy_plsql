create or replace trigger sup_int_pj_atual
before update or insert on sup_int_pj
for each row

declare
qt_existe_erros_w				number(10);
qt_existe_w				number(10);
begin

select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a,
	sup_int_regra_pj b
where	a.nr_sequencia = b.nr_seq_integracao
and	a.ie_evento = 'PJ'
and	a.ie_forma = 'R'
and	a.ie_situacao = 'A'
and	b.ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:new.dt_liberacao is not null) and	
	(:new.dt_confirma_integracao is null) and
	(:new.ie_forma_integracao = 'R') then
	
	:new.dt_leitura := sysdate;
	
	consiste_sup_int_pj(
		:new.nr_sequencia,
		:new.cd_cgc,
		:new.cd_estabelecimento,
		:new.cd_municipio_ibge,
		:new.cd_pf_resp_tecnico,
		:new.cd_cnes);
		
	select	count(*)
	into	qt_existe_erros_w
	from	sup_int_pj_consist
	where	nr_sequencia = :new.nr_sequencia;
	
	if	(qt_existe_erros_w = 0) then
		
		gerar_pessoa_jur_integracao(	
			:new.cd_cgc,
			:new.cd_estabelecimento,
			'INTEGR_TASY',
			:new.ds_razao_social,
			:new.nm_fantasia,
			:new.ds_nome_abrev,
			:new.cd_municipio_ibge,
			:new.cd_cep,
			:new.ds_endereco,
			:new.nr_endereco,
			:new.ds_complemento,
			:new.ds_bairro,
			:new.ds_municipio,
			:new.sg_estado,
			:new.nr_telefone,
			:new.nr_fax,
			:new.nr_inscricao_estadual,
			:new.nr_inscricao_municipal,
			:new.cd_internacional,
			:new.ds_site_internet,
			:new.cd_pf_resp_tecnico,
			:new.ds_orgao_reg_resp_tecnico,
			:new.nr_autor_func,
			:new.nr_alvara_sanitario,
			:new.nr_alvara_sanitario_munic,
			:new.nr_certificado_boas_prat,
			:new.cd_ans,
			:new.dt_validade_autor_func,
			:new.dt_validade_alvara_sanit,
			:new.dt_validade_cert_boas_prat,
			:new.cd_cnes,
			:new.cd_referencia_fornec,
			:new.cd_sistema_ant,
			:new.ds_email,
			:new.dt_validade_resp_tecnico,
			:new.nm_pessoa_contato,
			:new.nr_ramal_contato,
			:new.nr_registro_resp_tecnico,
			:new.qt_dia_prazo_entrega,
			:new.vl_minimo_nf,
			:new.dt_validade_alvara_munic,
			:new.ds_resp_tecnico,
			:new.ie_situacao,
			:new.ie_status_apenado);

		:new.dt_confirma_integracao 	:= sysdate;
	end if;
end if;
end;
/
