create or replace trigger pe_prescr_proc_atual
before insert or update on pe_prescr_proc
for each row

declare
ie_situacao_w	 varchar2(2);
qt_reg_w		number(10);
begin


if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.ie_faose is null) then
	begin
	select	max(ie_faose)
	into	:new.ie_faose
	from	pe_procedimento
	where	nr_sequencia = :new.NR_SEQ_PROC;
	exception
		when others then
		null;
	end;
end if;	



if	(:new.ie_profissional is null) then
	begin
	select	max(ie_profissional)
	into	:new.ie_profissional
	from	pe_procedimento
	where	nr_sequencia = :new.NR_SEQ_PROC;
	exception
		when others then
		null;
	end;
end if;	


select max(ie_situacao)
into ie_situacao_w
from pe_procedimento
where nr_sequencia = :new.nr_seq_proc;
if (ie_situacao_w = 'I') then
	Wheb_mensagem_pck.exibir_mensagem_abort(349391);
end if;

begin
if (:new.hr_horario_espec is not null) and ((:new.hr_horario_espec <> :old.hr_horario_espec) or (:old.dt_fim is null)) then
		:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_horario_espec,'dd/mm/yyyy hh24:mi');
end if;	
if (:new.hr_prim_horario is not null) and ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_inicio is null)) then
		:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario,'dd/mm/yyyy hh24:mi');
end if;

if	(INSERTING and nvl(:new.ds_horarios,'XPTO') = 'XPTO' and nvl(:new.ie_se_necessario,'N') = 'S') then
	:new.ds_horarios	:= 'SN';
end if;

exception
	when others then
	null;
end;


<<Final>>
qt_reg_w	:= 0;

end;
/
