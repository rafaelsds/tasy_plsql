create or replace trigger pls_prestador_pls_rps
after update on pls_prestador
for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

declare
ie_alterou_w 		varchar2(1) := 'N';
cd_cgc_cpf_old_w	pls_alt_prest_rps.cd_cgc_cpf_old%type;

begin
begin	
	if	(nvl(:new.cd_cgc,'0')			!= nvl(:old.cd_cgc,'0')) or
		(nvl(:new.cd_pessoa_fisica,'0')		!= nvl(:old.cd_pessoa_fisica,'0')) or
		(nvl(:new.dt_inicio_servico,sysdate)	!= nvl(:old.dt_inicio_servico,sysdate))or
		(nvl(:new.dt_inicio_contrato,sysdate)	!= nvl(:old.dt_inicio_contrato,sysdate)) or
		(nvl(:new.ie_tipo_relacao,'0')		!= nvl(:old.ie_tipo_relacao,'0')) or
		(nvl(:new.ie_urgencia_emergencia,'0')	!= nvl(:old.ie_urgencia_emergencia,'0')) or
		(nvl(:new.ie_tipo_classif_ptu,0)	!= nvl(:old.ie_tipo_classif_ptu,0)) or
		(nvl(:new.ie_disponibilidade_serv,'0')	!= nvl(:old.ie_disponibilidade_serv,'0')) or
		(nvl(:new.sg_uf_sip,'0')		!= nvl(:old.sg_uf_sip,'0')) then
		ie_alterou_w := 'S';	
	end if;

	if	(ie_alterou_w = 'S') then
		if	(:new.cd_cgc is not null) or 
			(:old.cd_cgc is not null) then
			cd_cgc_cpf_old_w := :old.cd_cgc;
		
		elsif	(:new.cd_pessoa_fisica is not null) or 
			(:old.cd_pessoa_fisica is not null) then
			select	max(nr_cpf)
			into	cd_cgc_cpf_old_w
			from	pessoa_fisica
			where	cd_pessoa_fisica = :new.cd_pessoa_fisica;
		end if;
	
		pls_gerar_alt_prest_rps( null, null, :new.nr_sequencia, cd_cgc_cpf_old_w, null, null, :new.nm_usuario, null, null, null);
	end if;
exception
when others then
	null;
end;	
end;
/