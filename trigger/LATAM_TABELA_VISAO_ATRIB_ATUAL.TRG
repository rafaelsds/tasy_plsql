CREATE OR REPLACE TRIGGER latam_tabela_visao_atrib_atual 
before insert or update on tabela_visao_atributo 
for each row
DECLARE 
ds_user_w varchar2(100);
PRAGMA AUTONOMOUS_TRANSACTION; 
BEGIN 
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then
 
if	--(:new.DS_LABEL is not null) and 
	(nvl(:new.DS_LABEL,'XPTO') <> nvl(:old.DS_LABEL,'XPTO')) then 
	latam_gerar_dic_palavra(:new.DS_LABEL, 'L', :new.nr_sequencia, :new.nm_atributo, null, null,null,null,null,null,null,null,null,null,null,null, 
	null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null); 
	commit; 
end if; 
 
if	--(:new.DS_LABEL_GRID is not null) and 
	(nvl(:new.DS_LABEL_GRID,'XPTO') <> nvl(:old.DS_LABEL_GRID,'XPTO')) then 
	latam_gerar_dic_palavra(:new.DS_LABEL_GRID, 'G', :new.nr_sequencia, :new.nm_atributo, null, null,null,null,null,null,null,null,null,null,null,null, 
	null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null); 
	commit; 
end if; 
 
if	--(:new.DS_LABEL_GRID is not null) and 
	(nvl(:new.ds_valores,'XPTO') <> nvl(:old.ds_valores,'XPTO')) and 
	(:new.ie_componente = 'rg') then 
	latam_gerar_dic_palavra(:new.ds_valores, 'AR', :new.nr_sequencia, :new.nm_atributo, null, null,null,null,null,null,null,null,null,null,null,null, 
	null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null); 
	commit; 
end if; 
 
if	--(:new.DS_LABEL_GRID is not null) and 
	(nvl(:new.ds_label_longo,'XPTO') <> nvl(:old.ds_label_longo,'XPTO')) then 
	latam_gerar_dic_palavra(:new.ds_label_longo, 'LW', :new.nr_sequencia, :new.nm_atributo, null, null,null,null,null,null,null,null,null,null,null,null, 
	null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null); 
	commit; 
end if; 
end if; 
end;
/