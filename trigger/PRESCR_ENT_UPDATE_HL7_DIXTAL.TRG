CREATE OR REPLACE TRIGGER prescr_ent_update_hl7_dixtal
AFTER UPDATE ON prescr_material
FOR EACH ROW

declare

nr_seq_interno_w		number(10);
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w	number(10);
ie_leito_monit		varchar2(1);

BEGIN

if	(nvl(:old.ie_suspenso,'N') = 'N') and
	(:new.ie_suspenso = 'S') and
	(:new.ie_agrupador = 8) then

	ds_sep_bv_w := obter_separador_bv;
	
	select	nr_atendimento, cd_pessoa_fisica
	into	nr_atendimento_w, cd_pessoa_fisica_w
	from 	prescr_medica
	where 	nr_prescricao = :new.nr_prescricao;
	
	select	max(obter_atepacu_paciente(nr_atendimento_w ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	nvl(obter_se_leito_atual_monit(nr_atendimento_w),'N')
	into	ie_leito_monit
	from 	dual;

	if (ie_leito_monit = 'S') then
		begin		
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w   || ds_sep_bv_w ||
					'nr_atendimento='    || nr_atendimento_w      || ds_sep_bv_w ||
					'nr_seq_interno='     || nr_seq_interno_w       || ds_sep_bv_w ||
					'nr_prescricao='       || :new.nr_prescricao     || ds_sep_bv_w ||
					'nr_seq_enteral='     || :new.nr_sequencia     || ds_sep_bv_w;	
		gravar_agend_integracao(50, ds_param_integ_hl7_w);	
		end;
	end if;	
	
end if;

END;
/