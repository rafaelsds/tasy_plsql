create or replace trigger part_carta_medica_update
before update on PARTICIPANTE_CARTA_MEDICA
for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;

begin			
	if ((:new.dt_assinatura is not null
		and :old.dt_assinatura is not null
		and :new.dt_assinatura != :old.dt_assinatura)
		or (:new.dt_assinatura is not null
			and :old.dt_assinatura is null)) then
		if (obter_permissao_ass_med(:new.nm_usuario_resp, :new.nm_usuario, 'S') = 'S') then
			:new.nm_usuario_assinat := :new.nm_usuario;
		end if;	
	elsif (:new.dt_assinatura is null
			and :old.dt_assinatura is not null) then
		:new.nm_usuario_assinat := null;	
	end if;	

	if (:new.dt_assinatura is not null and :old.dt_assinatura is null) then
		select	max(a.nr_seq_regra)
		into	nr_seq_regra_w
		from	wl_worklist a,
				wl_item b,
				wl_regra_item c
		where	a.nr_seq_carta_mae = :new.nr_seq_carta_mae
		and		b.nr_sequencia = a.nr_seq_item
		and		c.nr_sequencia = a.nr_seq_regra
		and		a.dt_final_real is null
		and		b.cd_categoria = 'ML'
		and		c.ie_tipo_pend_carta = 'A'
		and		b.ie_situacao = 'A'
		and		c.ie_situacao = 'A';

		wl_gerar_finalizar_tarefa('ML','F',null,null,wheb_usuario_pck.get_nm_usuario,null,'N',null,null,null,null,null,:new.nr_seq_carta_mae,null,null,null,nr_seq_regra_w,
									null,null,null,null,null,null,null,null,null,null,obter_pf_usuario(:new.nm_usuario_resp,'C'));
	end if;
end;
/
