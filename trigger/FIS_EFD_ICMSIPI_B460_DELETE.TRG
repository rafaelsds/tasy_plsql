create or replace trigger fis_efd_icmsipi_b460_delete
  before delete on fis_efd_icmsipi_b460    
  for each row
declare
PRAGMA AUTONOMOUS_TRANSACTION;
  -- local variables here
  cd_obs_w fis_efd_icmsipi_0460.cd_obs%type;
begin

  begin
    select count(a.cd_obs)
      into cd_obs_w
      from fis_efd_icmsipi_b460 a
     where a.nr_seq_controle = :old.nr_seq_controle;     
  end;
  
  if cd_obs_w < 2 then
    begin    
    
      delete fis_efd_icmsipi_0460 a
      where a.cd_obs = :old.cd_obs; 
    end;
    
    commit;
    
  end if;

  
end fis_efd_icmsipi_b460_delete;
/