create or replace trigger escala_pbss_atual
before insert or update on escala_pbss
for each row

declare
  qt_score_w number(3, 0) := 0;
  sql_w      varchar2(400);
  ds_erro_w   varchar2(4000);
  ds_parametro_w  varchar2(4000);

begin
    
  if (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
      begin
          sql_w := 'call calcular_score_escala_pbss_md(:1, :2, :3, :4, :5, :6) into :qt_score_w';
          
          execute immediate sql_w using in :new.ie_reflexo_chicote,
                                        in :new.ie_reflexo_cornea,
                                        in :new.ie_olhar_boneca,
                                        in :new.ie_reacao_luz_pupila_dir,
                                        in :new.ie_reacao_luz_pupila_esq,
                                        in :new.ie_reflexo_vomito,
                                        out qt_score_w;
      exception 
          when others then
            qt_score_w := null;
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametro_w := substr(':new.nr_atendimento: ' || :new.nr_atendimento || ' - :new.ie_situacao: ' || :new.ie_situacao || ' - :new.cd_profissional: ' || :new.cd_profissional
            || ' - :new.ie_reflexo_chicote: ' || :new.ie_reflexo_chicote || ' - :new.ie_reflexo_cornea: ' || :new.ie_reflexo_cornea || '- :new.ie_olhar_boneca: ' || :new.ie_olhar_boneca || ' - :new.ie_reacao_luz_pupila_dir: ' || :new.ie_reacao_luz_pupila_dir
            || ' - :new.ie_reacao_luz_pupila_esq: ' || :new.ie_reacao_luz_pupila_esq || ' - :new.ie_reflexo_vomito: ' || :new.ie_reflexo_vomito || ' - qt_score_w: ' || qt_score_w, 1, 4000);
            gravar_log_medical_device('ESCALA_PBSS_ATUAL', 'CALCULAR_SCORE_ESCALA_PBSS_MD', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');
      end;
      
      :new.qt_score := qt_score_w;
      
  end if;	

end;
/