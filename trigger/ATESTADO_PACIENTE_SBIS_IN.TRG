create or replace trigger ATESTADO_PACIENTE_SBIS_IN
before insert or update on atestado_paciente
for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'pt_BR') then
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual; 
  
  if (INSERTING) then
    select 	log_alteracao_prontuario_seq.nextval
    into 	nr_log_seq_w
    from 	dual; 
    
    insert into log_alteracao_prontuario (nr_sequencia, 
                       dt_atualizacao, 
                       ds_evento, 
                       ds_maquina, 
                       cd_pessoa_fisica, 
                       ds_item, 
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values 
                       (nr_log_seq_w, 
                       sysdate, 
                       obter_desc_expressao(656665),  
                       wheb_usuario_pck.get_nm_maquina, 
                       nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.cd_pessoa_fisica), 
                       obter_desc_expressao(283879), 
                       :new.nr_atendimento,
                       :new.dt_liberacao, 
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
  else 
    ie_inativacao_w := 'N';
    
    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';	
    end if;    
    
    insert into log_alteracao_prontuario (nr_sequencia, 
                       dt_atualizacao, 
                       ds_evento, 
                       ds_maquina, 
                       cd_pessoa_fisica, 
                       ds_item, 
                       nr_atendimento, 
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values 
                       (nr_log_seq_w, 
                       sysdate, 
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ), 
                       wheb_usuario_pck.get_nm_maquina, 
                       nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.cd_pessoa_fisica), 
                       obter_desc_expressao(283879), 
                       :new.nr_atendimento,
                       :new.dt_liberacao, 
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
  end if;
end if;
end;
/