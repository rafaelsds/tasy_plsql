CREATE OR REPLACE TRIGGER PLS_PRESTADOR_TAXA_ADM_ATUAL
BEFORE INSERT OR UPDATE ON PLS_PRESTADOR_TAXA_ADM
FOR EACH ROW

declare

qt_material_w		pls_integer;

begin
--OBSERVAÇÃO:
--Retirado a tratativa de preenchimento do campo cd_material
-- e passsado pra realizar esse processo no atrib change do nr_sequencia no delphi

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:new.nr_seq_material is not null) then
		select	count(1)
		into	qt_material_w
		from	pls_material a
		where	a.nr_sequencia	= :new.nr_seq_material;
		
		if	(qt_material_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(451885);
		end if;
	end if;
end if;

end;
/
