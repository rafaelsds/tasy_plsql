create or replace trigger icc_man_ordem_servico_atual
before update on man_ordem_servico
for each row
declare

begin

if  (nvl(obter_perfil_ativo,0) > 0) then
  if  (substr(obter_desc_perfil(obter_perfil_ativo),1,255) = 'Adm Tasy') and
    (:new.nr_seq_causa_dano is null) then
    Raise_application_error(-20011,'Neste perfil deve ser informada a causa dano');
  elsif  (substr(obter_desc_perfil(obter_perfil_ativo),1,255) != 'Adm Tasy') and
    (:new.nr_grupo_trabalho = 28) and
    (:new.nr_seq_causa_dano is null) then
    Raise_application_error(-20011,'Para este grupo de trabalho deve ser informada a causa dano');
  end if;
end if;

end;

