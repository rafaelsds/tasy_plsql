create or replace trigger pls_pp_cta_combinada_atual
before insert or update on pls_pp_cta_combinada
for each row

declare

qt_registro_w	pls_integer;

cursor c01(	nr_seq_pp_combinada_pc	pls_pp_cta_combinada.nr_sequencia%type) is
	select	nr_sequencia
	from	pls_pp_cta_filtro
	where	nr_seq_pp_combinada = nr_seq_pp_combinada_pc;
	
begin

-- previnir que seja gravada a hora junto na regra. 
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;

if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

-- percorre todos os filtros filhos
for r_c01_w in c01(:new.nr_sequencia) loop

	case (:new.ie_tipo_regra)
		when 'P' then -- procedimento
			
			update 	pls_pp_cta_filtro set
				ie_filtro_material = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;
			
		when 'M' then -- material
		
			update 	pls_pp_cta_filtro set
				ie_filtro_procedimento = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;
			
		when 'C' then -- material
		
			update 	pls_pp_cta_filtro set
				ie_filtro_material = 'N',
				ie_filtro_procedimento = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;
			
		else
			null;
	end case;
end loop;

end pls_pp_cta_combinada_atual;
/