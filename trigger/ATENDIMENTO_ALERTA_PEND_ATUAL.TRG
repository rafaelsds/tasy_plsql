create or replace
trigger atendimento_alerta_pend_atual
after insert or update on atendimento_alerta
for each row
declare

qt_reg_w				number(1);
ie_lib_alerta_pac_w		parametro_medico.ie_lib_alerta_pac%type;
cd_pessoa_fisica_w		pep_item_pendente.cd_pessoa_fisica%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

select	max(ie_lib_alerta_pac)
into	ie_lib_alerta_pac_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if (nvl(ie_lib_alerta_pac_w,'N') = 'S') then

if (:new.cd_paciente is not null) then
	cd_pessoa_fisica_w := :new.cd_paciente;
else
	select 	substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)
	into	cd_pessoa_fisica_w
	from	dual;
end if;

	if (:new.dt_liberacao is null) then
		Gerar_registro_pendente_PEP('ALA', :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario);
	elsif (:old.dt_liberacao is null) and (:new.dt_liberacao is not null) then
		Gerar_registro_pendente_PEP('XALA', :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario);
	end if;
	
end if;
	
<<Final>>
qt_reg_w := 0;

end;
/