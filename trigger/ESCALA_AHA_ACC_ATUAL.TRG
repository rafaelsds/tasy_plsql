create or replace trigger escala_aha_acc_atual
before insert or update on  escala_aha_acc
for each row
declare
    qt_reg_w     number(1);
    sql_w        varchar2(300);
    ds_retorno_w varchar2(255);
    ds_erro_w   varchar2(2000);
    ds_parametro_w  varchar2(2000);
begin

    if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
        goto Final;
    end if;

    :new.ie_risco := null;
    
    /** Medical Device **/
    begin
        sql_w := 'begin calcula_aha_acc_md(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14); end;';
        execute immediate sql_w using in out :new.ie_cirurgia_emergencia,
                                      in out :new.ie_condicao_cardiaca,
                                      in out :new.ie_cir_baixo_risco,
                                      in out :new.ie_classe_funcional,
                                      in out :new.ie_fat_risco,
                                      in :new.ie_cardiomiopatia, 
                                      in :new.ie_isquemica,
                                      in :new.ie_insuf_cardiaca,
                                      in :new.ie_diabete,
                                      in :new.ie_insuf_renal,
                                      in :new.ie_cerebrovascular,
                                      in :new.ie_tipo_cirurgia,
                                      out :new.ie_risco,
                                      out ds_retorno_w;
    exception
        when others then
            :new.ie_cirurgia_emergencia := null;
            :new.ie_condicao_cardiaca := null;
            :new.ie_cir_baixo_risco := null;
            :new.ie_classe_funcional := null;
            :new.ie_fat_risco := null;
            :new.ie_risco := null;
            ds_retorno_w := null;

            ds_erro_w := sqlerrm;ds_parametro_w := ':new.nr_sequencia: ' || :new.nr_sequencia || ' - :new.nr_atendimento: ' || :new.nr_atendimento || ' - :new.ie_situacao: ' || :new.ie_situacao || ' - :new.cd_profissional: ' || :new.cd_profissional;
            ds_parametro_w := ds_parametro_w || ' - :new.ie_cirurgia_emergencia: ' || :new.ie_cirurgia_emergencia || ' - :new.ie_condicao_cardiaca: ' || :new.ie_condicao_cardiaca || ' - :new.ie_cir_baixo_risco: ' || :new.ie_cir_baixo_risco || ' - :new.ie_classe_funcional: ' || :new.ie_classe_funcional;
            ds_parametro_w := ds_parametro_w || ' - :new.ie_fat_risco: ' || :new.ie_fat_risco || ' - :new.ie_risco: ' || :new.ie_risco || ' - ds_retorno_w: ' || ds_retorno_w;
            gravar_log_medical_device('escala_aha_acc_atual', 'calcula_aha_acc_md', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');
    end;

    :new.ds_resultado := substr(ds_retorno_w, 1, 255);

    <<Final>>
    qt_reg_w := 0;

end;
/