create or replace trigger regra_aloc_custo_setor_insert
before insert or update on regra_aloc_custo_setor
for each row

declare

begin

if	(:new.ie_calculo_custo = 'F') then
	begin
	if	(:new.cd_grupo_material is not null) or
		(:new.cd_subgrupo_material is not null) or
		(:new.cd_classe_material is not null) or 
		(:new.cd_material is not null) then
		/*'Para F�rmula de c�lculo: Custo baseado em Tabela de pre�o AMB,' || chr(13) || 'deve ser informado somente estrutura de  procedimento!#@#@');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266374);
	end if;
	end;
elsif	(:new.cd_edicao_amb is not null) then
	:new.cd_edicao_amb	:= null;
end if;
end regra_aloc_custo_setor_insert;
/