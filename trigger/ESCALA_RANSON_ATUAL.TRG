create or replace trigger escala_ranson_atual
before insert or update on escala_ranson
for each row

declare
  sql_w            varchar2(800);
  qt_pontuacao_w   number(10);
  ds_erro_w        VARCHAR2(4000);
  ds_parametros_w  VARCHAR2(4000);
  
begin

if (wheb_usuario_pck.get_ie_executar_trigger  = 'S')  then   
   
  if (:new.nr_hora is null) or
    (:new.dt_avaliacao <> :old.dt_avaliacao) then
    begin
    :new.nr_hora	:= to_number(to_char(round(:new.dt_avaliacao,'hh24'),'hh24'));
    end;
  end if;

  
  begin
  
      sql_w := 'CALL OBTER_SCORE_RANSON_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12) INTO :qt_pontuacao_w';
      EXECUTE IMMEDIATE sql_w USING IN :new.ie_pancreatite_aguda,
                                    IN :new.qt_idade,
                                    IN :new.qt_leucocitos,
                                    IN :new.qt_glicemia,
                                    IN :new.qt_dhl,
                                    IN :new.qt_tgo,
                                    IN :new.pr_queda_hematocrito,
                                    IN :new.qt_aumento_bun,
                                    IN :new.qt_calcemia,
                                    IN :new.qt_pao2,
                                    IN :new.qt_base_excess,
                                    IN :new.qt_sequestro_liquido,                                    
                                    OUT qt_pontuacao_w;    

  exception
    when others then
      ds_erro_w := sqlerrm;
      ds_parametros_w := (':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.cd_profissional: '||:new.cd_profissional||'-'||':new.ie_situacao: '||:new.ie_situacao||'-'||
                          ':new.ie_pancreatite_aguda: '||:new.ie_pancreatite_aguda||'-'||':new.qt_idade: '||:new.qt_idade||'-'||':new.qt_leucocitos: '||:new.qt_leucocitos||'-'||
                          ':new.qt_glicemia: '||:new.qt_glicemia||'-'||':new.qt_dhl: '||:new.qt_dhl||'-'||':new.qt_tgo: '||:new.qt_tgo||'-'||
                          ':new.pr_queda_hematocrito: '||:new.pr_queda_hematocrito||'-'||':new.qt_aumento_bun: '||:new.qt_aumento_bun||'-'||':new.qt_calcemia: '||:new.qt_calcemia||'-'||
                          ':new.qt_pao2: '||:new.qt_pao2||'-'||':new.qt_base_excess: '||:new.qt_base_excess||'-'||':new.qt_sequestro_liquido: '||:new.qt_sequestro_liquido||'-'||
                          'qt_pontuacao_w: '||qt_pontuacao_w);

      gravar_log_medical_device ('escala_ranson_atual','OBTER_SCORE_RANSON_MD'
                                 ,ds_parametros_w,substr(ds_erro_w, 4000),:new.nm_usuario,'N');  
    
      qt_pontuacao_w := null;
  end;
      :new.qt_pontuacao	:= qt_pontuacao_w;

end if;
end;
/
