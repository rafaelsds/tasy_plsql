create or replace trigger pessoa_titular_convenio_update
after update on pessoa_titular_convenio 
for each row

declare

	
begin
		
	if ((nvl(:old.cd_convenio,0) <> nvl(:new.cd_convenio,0)) or 
		(nvl(:old.cd_categoria,'X') <> nvl(:new.cd_categoria,'X')) or
		(nvl(:old.cd_plano_convenio,'X') <> nvl(:new.cd_plano_convenio,'X')) or
    (nvl(:old.ie_tipo_conveniado,0) <> nvl(:new.ie_tipo_conveniado,0)) or
		(nvl(:old.cd_pessoa_titular,0) <> nvl(:new.cd_pessoa_titular,0)) or
		(nvl(:old.ie_grau_dependencia,'X') <> nvl(:new.ie_grau_dependencia,'X')) or
		(nvl(:old.cd_usuario_convenio,'X') <> nvl(:new.cd_usuario_convenio,'X')) or
		
		(nvl(:old.dt_inicio_vigencia,sysdate) <> nvl(:new.dt_inicio_vigencia,sysdate)) or
		(nvl(:old.dt_fim_vigencia,sysdate) <> nvl(:new.dt_fim_vigencia,sysdate)) or
		(nvl(:old.dt_validade_carteira,sysdate) <> nvl(:new.dt_validade_carteira,sysdate)) or
		(nvl(:old.cd_usuario_convenio_tit,'X') <> nvl(:new.cd_usuario_convenio_tit,'X')) or
		(nvl(:old.cd_empresa_refer,0) <> nvl(:new.cd_empresa_refer,0))) then
		
		call_bifrost_content('patient.insurance.update','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);
		
	end if;

end;
/