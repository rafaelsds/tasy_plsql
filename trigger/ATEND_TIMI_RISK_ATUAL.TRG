create or replace 
trigger atend_timi_risk_atual 
before insert or update on atend_timi_risk
for each row
declare
qt_reg_w         	number(1);
ds_sql_w            varchar2(500);
ds_erro_w        	varchar(4000);
ds_parametros_w  	varchar(4000);
begin
    if ( :new.nr_hora is null ) or ( :new.dt_avaliacao <> :old.dt_avaliacao ) then
        begin
            :new.nr_hora := to_number(to_char(round(:new.dt_avaliacao, 'hh24'), 'hh24'));
        end;
    end if;

    if ( wheb_usuario_pck.get_ie_executar_trigger = 'N' ) then
        goto final;
    end if;

    begin
        ds_sql_w := 'begin calcula_timi_risk_md(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17); end;';
        execute immediate ds_sql_w using in :new.qt_ano, 
										 in :new.ie_fator_risco, 
										 in :new.ie_dac, 
										 in :new.ie_aas, 
										 in :new.ie_angina_24h, 
										 in :new.ie_elev_enzima,
										 in :new.ie_desvio_st, 
										 out :new.qt_escore_ano, 
										 out :new.qt_escore_fat_risco, 
										 out :new.qt_escore_dac, 
										 out :new.qt_escore_aas,
										 out :new.qt_escore_angina, 
										 out :new.qt_escore_elev_enzima, 
										 out :new.qt_escore_desvio, 
										 out :new.qt_escore, 
										 out :new.pr_morte_infarto,
										 out :new.pr_morte_infarto_revasc;

    exception
        when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr( ':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.qt_ano: '||:new.qt_ano||'-'||'new.ie_fator_risco: '||:new.ie_fator_risco||'-'||':new.ie_dac: '||:new.ie_dac||
									   '-'||':new.ie_aas: '||:new.ie_aas||'-'||':new.ie_angina_24h: '||:new.ie_angina_24h||'-'||':new.ie_elev_enzima: '||:new.ie_elev_enzima||'-'||':new.ie_desvio_st: '||:new.ie_desvio_st||
									   '-'||':new.qt_escore_ano: '||:new.qt_escore_ano||'-'||'new.qt_escore_fat_risco: '||:new.qt_escore_fat_risco||'-'||':new.qt_escore_dac: '||:new.qt_escore_dac||'-'||':new.qt_escore_aas: '||:new.qt_escore_aas||
									   '-'||':new.qt_escore_angina: '||:new.qt_escore_angina||'-'||':new.qt_escore_elev_enzima: '||:new.qt_escore_elev_enzima||'-'||':new.qt_escore_desvio: '||:new.qt_escore_desvio||'-'||':new.qt_escore: '||:new.qt_escore||
									   '-'||':new.pr_morte_infarto: '||:new.pr_morte_infarto||'-'||':new.pr_morte_infarto_revasc: '||:new.pr_morte_infarto_revasc, 1, 4000);

            gravar_log_medical_device('atend_timi_risk_atual', 'CALCULA_TIMI_RISK_MD', ds_parametros_w, ds_erro_w, :new.nm_usuario,
                                     'N');
        
            :new.qt_escore_ano := 0;
            :new.qt_escore_fat_risco := 0;
            :new.qt_escore_dac := 0;
            :new.qt_escore_aas := 0;
            :new.qt_escore_angina := 0;
            :new.qt_escore_elev_enzima := 0;
            :new.qt_escore_desvio := 0;
            :new.qt_escore := 0;
            :new.pr_morte_infarto := 0;
            :new.pr_morte_infarto_revasc := 0;
    end;

    << final >> 
	qt_reg_w := 0;
end;
/
