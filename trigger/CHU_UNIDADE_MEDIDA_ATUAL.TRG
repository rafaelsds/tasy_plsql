create or replace trigger chu_unidade_medida_atual
before insert or update on unidade_medida
for each row

declare
ds_unidade_medida_w	varchar2(40);
cd_unidade_medida_w	varchar2(30);
cd_unidade_medida_ww	varchar2(30);
cd_unidade_medida_www	varchar2(30);
ds_unidade_medida_ww	varchar2(40);
ds_unidade_medida_www	varchar2(40);

Cursor C01 is
select	cd_unidade_medida,
	ds_unidade_medida,
	upper(REPLACE(TISS_ELIMINAR_CARACTERE(elimina_acentuacao(cd_unidade_medida)),' ',NULL)),
	upper(REPLACE(TISS_ELIMINAR_CARACTERE(elimina_acentuacao(ds_unidade_medida)),' ',NULL))
from	unidade_medida
where	cd_unidade_medida <> :new.cd_unidade_medida;

pragma autonomous_transaction;
begin
if	(:new.ie_situacao = 'A') then
	begin
	select	upper(REPLACE(TISS_ELIMINAR_CARACTERE(elimina_acentuacao(:new.cd_unidade_medida)),' ',NULL)),
		upper(REPLACE(TISS_ELIMINAR_CARACTERE(elimina_acentuacao(:new.ds_unidade_medida)),' ',NULL))
	into	cd_unidade_medida_w,
		ds_unidade_medida_w
	from	dual;
	
	open C01;
	loop
	fetch C01 into	
		cd_unidade_medida_ww,
		ds_unidade_medida_ww,
		cd_unidade_medida_www,
		ds_unidade_medida_www;
	exit when C01%notfound;
		begin
		if	(ds_unidade_medida_w = ds_unidade_medida_www) then
			Raise_application_error(-20011,'A unidade de medida ['||cd_unidade_medida_ww||'] - ' || ds_unidade_medida_ww || ' possui a mesma descri��o desta. Verifique!');
		elsif	(cd_unidade_medida_w = cd_unidade_medida_www) then
			Raise_application_error(-20011,'A unidade de medida ['||cd_unidade_medida_ww||'] - ' || ds_unidade_medida_ww || ' possui o mesmo c�digo desta. Verifique!');
		end if;
		end;
	end loop;
	close C01;
	end;
end if;

end chu_unidade_medida_atual;
/