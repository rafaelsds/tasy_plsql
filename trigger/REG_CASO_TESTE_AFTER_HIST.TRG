create or replace trigger reg_caso_teste_after_hist
after update on reg_caso_teste
for each row

declare

ie_ct_lancado_w			varchar2(1);
ie_tipo_alteracao_w		reg_caso_teste_hist.ie_tipo_alteracao%type;
ds_descricao_w			reg_caso_teste_hist.ds_descricao%type;
ds_pre_condicao_w		reg_caso_teste_hist.ds_pre_condicao%type;
dt_ultima_aprovacao_w		reg_caso_teste_hist.dt_aprovacao%type;
ie_acao_teste_alterada_w	varchar2(1);

begin

select	decode(count(1), 0, 'N', 'S')
into	ie_ct_lancado_w -- Determinar se o CT j� foi liberado em alguma revis�o do documento de CT
from	reg_caso_teste_hist
where	nr_seq_caso_teste = :new.nr_sequencia
and	ie_tipo_alteracao = 'I'
and	nr_seq_reg_version_rev is not null;

if	(:new.dt_aprovacao is not null) and
	(:new.nm_usuario_aprovacao is not null) then

	delete	reg_caso_teste_hist
	where	nr_seq_caso_teste = :new.nr_sequencia
	and	nr_seq_reg_version_rev is null;

	reg_gravar_hist_acao_teste(:new.nr_sequencia, :new.nm_usuario, ie_acao_teste_alterada_w);

	if (ie_ct_lancado_w = 'S') then

		select	ds_descricao,
			ds_pre_condicao,
			dt_aprovacao
		into	ds_descricao_w,
			ds_pre_condicao_w,
			dt_ultima_aprovacao_w
		from	reg_caso_teste_hist
		where	nr_sequencia = (	select	max(nr_sequencia)
						from	reg_caso_teste_hist
						where	nr_seq_caso_teste = :new.nr_sequencia
						and	ie_tipo_alteracao in ('I', 'A')
						and	nr_seq_reg_version_rev is not null
					);

		if	(nvl(:old.ie_situacao, 'A') = 'A') and
			(:new.ie_situacao = 'I') then
			ie_tipo_alteracao_w := 'E';
		elsif	(:new.ds_descricao <> ds_descricao_w) or
			(ie_acao_teste_alterada_w = 'S') then
			ie_tipo_alteracao_w := 'A';
		end if;
	else
		if (nvl(:new.ie_situacao, 'A') = 'A') then
			ie_tipo_alteracao_w := 'I';
		end if;
	end if;
end if;

if (ie_tipo_alteracao_w is not null) then

	insert into reg_caso_teste_hist (
		nr_sequencia,
		nr_seq_customer,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_product,
		ds_descricao,
		ie_tipo_documento,
		dt_liberacao_vv,
		ds_pre_condicao,
		nr_tempo_estimado,
		nr_seq_feature,
		ie_tipo_execucao,
		ie_obrigatorio,
		ie_tipo_teste,
		cd_versao,
		nr_seq_usability_principle,
		nr_seq_reg_version_rev,
		nr_seq_estagio,
		nr_seq_caso_teste,
		ie_tipo_alteracao,
		dt_revisao,
		ie_tipo_revisao,
		nr_seq_revisao,
		ie_situacao,
		dt_aprovacao,
		nm_usuario_aprovacao,
		nm_usuario_liberacao,
		dt_inclusao_hist,
		dt_inativacao,
		nm_usuario_inativacao,
		ds_motivo_inativacao,
		nr_seq_agrupador,
		nr_seq_test_script,
		nr_seq_intencao_uso,
		cd_ct_id)
	values (
		reg_caso_teste_hist_seq.nextval,
		:new.nr_seq_customer,
		:new.dt_atualizacao,
		:new.nm_usuario,
		:new.dt_atualizacao_nrec,
		:new.nm_usuario_nrec,
		:new.nr_seq_product,
		:new.ds_descricao,
		:new.ie_tipo_documento,
		:new.dt_liberacao_vv,
		:new.ds_pre_condicao,
		:new.nr_tempo_estimado,
		:new.nr_seq_feature,
		:new.ie_tipo_execucao,
		:new.ie_obrigatorio,
		:new.ie_tipo_teste,
		:new.cd_versao,
		:new.nr_seq_usability_principle,
		null,
		:new.nr_seq_estagio,
		:new.nr_sequencia,
		ie_tipo_alteracao_w,
		:new.dt_revisao,
		:new.ie_tipo_revisao,
		:new.nr_seq_revisao,
		:new.ie_situacao,
		:new.dt_aprovacao,
		:new.nm_usuario_aprovacao,
		:new.nm_usuario_liberacao,
		sysdate,
		:new.dt_inativacao,
		:new.nm_usuario_inativacao,
		:new.ds_motivo_inativacao,
		:new.nr_seq_agrupador,
		:new.nr_seq_test_script,
		:new.nr_seq_intencao_uso,
		:new.cd_ct_id);

end if;

end;
/
