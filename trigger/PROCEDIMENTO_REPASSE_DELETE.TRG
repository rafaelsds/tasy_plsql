CREATE OR REPLACE TRIGGER Procedimento_Repasse_Delete
BEFORE DELETE ON Procedimento_Repasse
FOR EACH ROW

declare

ie_status_w	varchar2(255);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:old.nr_repasse_terceiro is not null) then

		select 	ie_status
		into 	ie_status_w
		from 	repasse_terceiro
		where 	nr_repasse_terceiro = :old.nr_repasse_terceiro;

		if 	(ie_status_w = 'F') then
			/* Este procedimento j� est� vinculado � um repasse!
			N�o � poss�vel excluir este procedimento.
			Repasse: #@NR_REPASSE_TERCEIRO#@ */
			wheb_mensagem_pck.exibir_mensagem_abort(266950, 'NR_REPASSE_TERCEIRO=' || :old.nr_repasse_terceiro);
		end if;
	end if;
end if;
end;
/