create or replace trigger nota_fiscal_item_beforeInsert
before insert or update on nota_fiscal_item
for each row


declare


cd_natureza_operacao_w		number(4);
nr_seq_variacao_fiscal_w	number(10);
nr_seq_produto_w		number(10);
nr_seq_grupo_prod_w		number(10);
nr_ordem_compra_w		number(10);
qt_existe_w			number(5);
cd_ext_material_w		number(10);
nr_solic_compra_w		solic_compra.nr_solic_compra%type;
nr_item_solic_compra_w		solic_compra_item.nr_item_solic_compra%type;
nr_seq_orc_item_gpi_w		nota_fiscal_item.nr_seq_orc_item_gpi%type;
nr_seq_orc_item_gpi_ww		nota_fiscal_item.nr_seq_orc_item_gpi%type;
ie_tipo_nota_w			nota_fiscal.ie_tipo_nota%type;
ie_entrada_saida_w		varchar2(10);
ie_tipo_operacao_w		operacao_nota.ie_tipo_operacao%type;
cd_grupo_material_w     	grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w     	subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w     	classe_material.cd_classe_material%type;
nr_seq_familia_w     		material.nr_seq_familia%type;


begin

select	obter_dados_variacao_fiscal(:new.nr_Sequencia, :new.cd_material, 'N'),
	obter_dados_variacao_fiscal(:new.nr_Sequencia, :new.cd_material, 'S')
into	cd_natureza_operacao_w,
	nr_seq_variacao_fiscal_w
from	dual;

if	(cd_natureza_operacao_w > 0) then
	:new.cd_natureza_operacao		:= cd_natureza_operacao_w;
end if;

if	(nr_seq_variacao_fiscal_w > 0) then
	:new.nr_seq_variacao_fiscal		:= nr_seq_variacao_fiscal_w;
end if;

obter_produto_financeiro(:new.cd_estabelecimento,null,null,nr_seq_produto_w,null,null,nr_seq_grupo_prod_w);

if	(nvl(nr_seq_produto_w,0) > 0) then
	:new.nr_seq_produto			:= nr_seq_produto_w;
end if;

if	(inserting) then
	begin
	--Trazer dados do material
    if	(:new.cd_material IS NOT NULL) and
		(:new.cd_material > 0) then
    begin
        select
            gm.cd_grupo_material,
            sm.cd_subgrupo_material,
            cm.cd_classe_material,
            nvl(m.nr_seq_familia, 0) as nr_seq_familia	
        into cd_grupo_material_w,
             cd_subgrupo_material_w,
             cd_classe_material_w,
             nr_seq_familia_w
        from material m,
            classe_material cm,
            subgrupo_material sm,
            grupo_material gm
        where m.cd_classe_material = cm.cd_classe_material
          and cm.cd_subgrupo_material = sm.cd_subgrupo_material
          and sm.cd_grupo_material = gm.cd_grupo_material
          and m.cd_material = :new.cd_material;
        
        if (:new.ie_tipo_programa is null) THEN
            :new.ie_tipo_programa := obter_regra_prog_saude(:new.cd_material, :new.cd_estabelecimento, cd_grupo_material_w, cd_subgrupo_material_w,cd_classe_material_w,nr_seq_familia_w);
        end if;
        -- Buscar IE_CLASSIF_RENAME pelo caso venha vazio
        if (:new.ie_classif_rename is null) THEN
            :new.ie_classif_rename := obter_regra_rename(:new.cd_material, :new.cd_estabelecimento, cd_grupo_material_w, cd_subgrupo_material_w,cd_classe_material_w,nr_seq_familia_w);
        end if;
	end;
    end if;
    
	if	(:new.nr_solic_compra is not null) and
		(:new.nr_item_solic_compra is not null) then
		
		select  max(nr_seq_orc_item_gpi)
		into	nr_seq_orc_item_gpi_w
		from	solic_compra_item
		where	nr_solic_compra		= :new.nr_solic_compra
		and	nr_item_solic_compra	= :new.nr_item_solic_compra;

		:new.nr_seq_orc_item_gpi := nr_seq_orc_item_gpi_w;
	end if;
	end;
else
	begin
	if	(:new.nr_solic_compra is not null) and
		(:new.nr_item_solic_compra is not null) and
		(:new.nr_seq_orc_item_gpi is null) and
		(:old.nr_seq_orc_item_gpi is null) then
		
		select  max(nr_seq_orc_item_gpi)
		into	nr_seq_orc_item_gpi_w
		from	solic_compra_item
		where	nr_solic_compra	= :new.nr_solic_compra
		and	nr_item_solic_compra = :new.nr_item_solic_compra;

		:new.nr_seq_orc_item_gpi := nr_seq_orc_item_gpi_w;
	end if;
	end;
end if;

if	(inserting) then

	select	nvl(max(a.nr_ordem_compra),0),
		max(a.ie_tipo_nota),
		max(obter_se_Nota_entrada_saida(a.nr_sequencia)),
		max(b.ie_tipo_operacao)
	into	nr_ordem_compra_w,
		ie_tipo_nota_w,
		ie_entrada_saida_w,
		ie_tipo_operacao_w
	from	nota_fiscal a,
		operacao_nota b
	where	a.cd_operacao_nf = b.cd_operacao_nf
	and	a.nr_sequencia = :new.nr_sequencia;
	
	select	count(*)

	into	qt_existe_w
	from	ordem_compra
	where	ie_origem_imp = 'MercadoEletronico'
	and	nr_ordem_compra = nr_ordem_compra_w;
	
	if	(:new.nr_solic_compra is not null) and
		(:new.nr_item_solic_compra is not null)then
		
		select  max(nr_seq_orc_item_gpi)
		into	nr_seq_orc_item_gpi_w
		from	solic_compra_item
		where	nr_solic_compra = :new.nr_solic_compra
		and	nr_item_solic_compra = :new.nr_item_solic_compra;
	
			:new.nr_seq_orc_item_gpi := nr_seq_orc_item_gpi_w;
	end if;
	
	if	(qt_existe_w = 0) and 
		(ie_tipo_nota_w <> 'ST') and 
		(ie_entrada_saida_w = 'E') and 
		(ie_tipo_operacao_w = '1') then
	
		select	nvl(to_number(obter_cod_ext_material(:new.cd_material,:new.cd_estabelecimento)),0)
		into	cd_ext_material_w
		from dual;
	
		if	(cd_ext_material_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(216284,'CD_MATERIAL=' || :new.cd_material ||';'|| 'DS_MATERIAL=' || obter_desc_material(:new.cd_material));
		end if;
	
	end if;
	
end if;

end;
/