create or replace trigger protocolo_solic_indic_atual
before insert or update on protocolo_solic_indicacao
for each row

declare

cd_protocolo_w	number(10);
nr_sequencia_w	number(10);

begin

if	(obter_se_prot_lib_regras = 'S') then
	select	cd_protocolo,
		nr_seq_protocolo
	into	cd_protocolo_w,
		nr_sequencia_w
	from	prot_solic_bco_sangue
	where	nr_sequencia	= :new.nr_seq_solic_bs;

	update	protocolo_medicacao
	set	nm_usuario_aprov	=	null,
		dt_aprovacao		=	null,
		ie_status		=	'PA'
	where	cd_protocolo		=	cd_protocolo_w
	and	nr_sequencia		=	nr_sequencia_w;
end if;

end;
/
