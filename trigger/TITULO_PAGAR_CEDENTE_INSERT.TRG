CREATE OR REPLACE TRIGGER Titulo_Pagar_Cedente_Insert
BEFORE INSERT ON Titulo_Pagar_Cedente
FOR EACH ROW

DECLARE
ie_situacao_w		varchar2(1);
dt_liquidacao_w		date;

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	ie_situacao,
	dt_liquidacao
into	ie_situacao_w,
	dt_liquidacao_w
from	titulo_pagar
where	nr_titulo = :new.nr_titulo;

if 	((:new.cd_pessoa_fisica is null) and (:new.cd_cgc is null)) or
	((:new.cd_pessoa_fisica is not null) and (:new.cd_cgc is not null)) then
	-- O titulo deve ter apenas um cedente!
	wheb_mensagem_pck.exibir_mensagem_abort(267029);

elsif	(ie_situacao_w <> 'A') or (dt_liquidacao_w is not null) then
	-- O titulo deve estar aberto!
	wheb_mensagem_pck.exibir_mensagem_abort(267030);

else
	update	titulo_pagar
	set	cd_pessoa_fisica 	= :new.cd_pessoa_fisica,
		cd_cgc 			= :new.cd_cgc,
		nm_usuario		= :new.nm_usuario,
		dt_atualizacao		= sysdate
	where	nr_titulo 		= :new.nr_titulo;
end if;
end if;

END;
/
