create or replace trigger RBAPB_ATENDIMENTO_VISITA_ATUAL
before INSERT or UPDATE on ATENDIMENTO_VISITA
for each row

declare
-- integra��o Winspector

begin

if	(:new.cd_pessoa_fisica is not null) and
	(:new.nr_seq_controle is not null) then
	
	if	(inserting) then

		insert into wsp.IDENTIFICADORES@sgh.beneficencia.org (
			IDENUMINTERNO,
			IDENUMEXTERNO,
			IDETIPO,
			IDECATEGORIA,
			IDEHABILITADO,
			IDEVALIDADE,
			PIN)
		values (
			lpad(:new.nr_seq_controle,20,'0'),
			:new.cd_pessoa_fisica,
			1,
			1,
			1,
			sysdate+1,
			null);

		insert into wsp.LOGMENSAGEM@sgh.beneficencia.org (
			LMSNUMERO,
			LMSDATA,
			LMSHORA,
			LMSCODIGO,
			LMSMENSAGEM,
			CONNUMERO)
		select	null, --  n�o precisa passar, pois uma trigger do winspector alimenta.
			to_char(sysdate,'yyyymmdd'),
			to_char(sysdate,'hhmiss'),
			8002,
			lpad(:new.nr_seq_controle,20,'0'),
			CONNUMERO
		from	wsp.CONCENTRADORES@sgh.beneficencia.org
		where	CONEXISTE = 1;


	elsif	(updating) and
		(:old.DT_SAIDA is null and :new.DT_SAIDA is not null) then

		update	wsp.IDENTIFICADORES@sgh.beneficencia.org set
			IDEHABILITADO = 0
		where	IDENUMINTERNO = lpad(:new.NR_SEQ_CONTROLE,20,'0')
		and	IDENUMEXTERNO = :new.cd_pessoa_fisica;
	end if;
end if;

end;
/