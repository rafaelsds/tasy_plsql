create or replace trigger HRAV_MAN_ORDEM_SERVICO_INSERT
after insert on MAN_ORDEM_SERVICO
for each row

begin
if	((to_char(:new.dt_ordem_servico,'d') in ('1','7')) or
	(to_date(to_char(:new.dt_ordem_servico,'dd/mm/yyyy hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <
	to_date(to_char(:new.dt_ordem_servico,'dd/mm/yyyy') || ' 07:30:00','dd/mm/yyyy hh24:mi:ss')) or
	(to_date(to_char(:new.dt_ordem_servico,'dd/mm/yyyy hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >
	to_date(to_char(:new.dt_ordem_servico,'dd/mm/yyyy') || ' 18:00:00','dd/mm/yyyy hh24:mi:ss'))) and
	(:new.nr_grupo_trabalho = 1) then
	begin
	enviar_email(	'Ordem de servi�o aberta',
			'A ordem de servi�o n� ' || :new.nr_sequencia || ' foi aberta.' || chr(13) || chr(10) || 
			'Descri��o: ' || :new.ds_dano_breve || chr(13) || chr(10) ||
			'Dano: ' || :new.ds_dano,
			null,
			'ti@hrav.com.br',
			'GABRIELFE',
			'M');
	end;
end if;

end;
/
