create or replace
trigger inventario_bfinsert
before insert on inventario_material
for each row

declare
nr_seq_inv_ciclico_w			inventario.nr_seq_inv_ciclico%type;
ie_novo_item_inv_ciclico_w		varchar2(1) := nvl(obter_valor_param_usuario(143, 386, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'N');
action_w				v$session.action%type;

begin
begin
select	upper(action)
into	action_w
from	v$session
where	audsid = (select userenv('sessionid') from dual)
and	upper(action) = 'GERAR_INVENTARIO_CICLICO'
and	rownum = 1;
exception
when others then
	action_w	:=	'X';
end;

if	(action_w = 'X') then
	begin
	begin
	select	nvl(nr_seq_inv_ciclico,0)
	into	nr_seq_inv_ciclico_w
	from	inventario
	where	nr_sequencia = :new.nr_seq_inventario;
	exception
	when others then
		nr_seq_inv_ciclico_w	:=	0;
	end;
	
	if	(nr_seq_inv_ciclico_w > 0) and (ie_novo_item_inv_ciclico_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(791240);
	end if;
	end;
end if;
end;
/