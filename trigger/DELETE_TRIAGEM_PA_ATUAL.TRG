create or replace trigger DELETE_TRIAGEM_PA_ATUAL
before delete on TRIAGEM_PRONTO_ATEND
for each row

declare
ie_gerar_clinical_notes_w varchar2(1);
nm_user_w varchar(100) :=wheb_usuario_pck.get_nm_usuario;
begin

obter_param_usuario(281, 1643, Obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_clinical_notes_w);
if(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:old.cd_evolucao is not null and ie_gerar_clinical_notes_w ='S') then
	delete from clinical_note_soap_data where cd_evolucao = :old.cd_evolucao and ie_med_rec_type = 'EMERG_SERV_CONS' and ie_stage = 1  and ie_soap_type = 'P' and nr_seq_med_item=:old.nr_sequencia ;
end if;
end if;
end;
/
