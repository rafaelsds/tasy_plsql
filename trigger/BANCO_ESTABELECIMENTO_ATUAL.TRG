create or replace trigger banco_estabelecimento_atual
before insert or update on banco_estabelecimento
for each row
declare

cd_estabelecimento_w	number(4);
cd_empresa_w			number(4);

ds_conta_contabil_w		varchar2(40);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
ds_conta_contabil_w		:= substr(wheb_mensagem_pck.get_texto(354087,''),1,40);

ctb_consistir_conta_titulo(:new.cd_conta_contabil, ds_conta_contabil_w);

ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_contabil, ds_conta_contabil_w);

end if;
end;
/
