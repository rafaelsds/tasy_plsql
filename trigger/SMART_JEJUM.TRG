CREATE OR REPLACE TRIGGER SMART_JEJUM
after update on rep_jejum
FOR EACH ROW

declare
	reg_integracao_w		gerar_int_padrao.reg_integracao;

BEGIN
begin
	if 	((:old.ie_suspenso = 'N') and (:new.ie_suspenso = 'S')) or
	     ((:new.dt_fim 	<> :old.dt_fim)) then 
		begin
			reg_integracao_w.cd_estab_documento := wheb_usuario_pck.get_cd_estabelecimento;
			gerar_int_padrao.gravar_integracao('306', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';NR_SEQUENCIA=' || :new.NR_SEQUENCIA || ';DS_OPERACAO=UPDATE');
		END;	
	end if;
exception when others then
	null;
end;

end;
/