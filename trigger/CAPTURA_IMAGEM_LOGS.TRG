create or replace
TRIGGER captura_imagem_logs
BEFORE INSERT OR UPDATE OR DELETE ON prescr_proc_captura_img
FOR EACH ROW

DECLARE

nr_sequencia_w NUMBER(10);

BEGIN 
  
  SELECT prescr_proc_captura_log_seq.NEXTVAL
  INTO nr_sequencia_w
  FROM dual;

  IF (INSERTING) THEN
  BEGIN
    INSERT INTO prescr_proc_captura_log(NR_SEQUENCIA,
                                        DS_CALL_STACK,
                                        DT_ATUALIZACAO,
                                        NM_USUARIO,
                                        DT_ATUALIZACAO_NREC,
                                        NM_USUARIO_NREC,
                                        NR_PRESCRICAO,
                                        NR_SEQ_PRESCRICAO,
                                        NR_SEQ_CAPTURA_IMAGEM,
                                        DS_OPERACAO)
    VALUES(nr_sequencia_w,
           substr(dbms_utility.format_call_stack,1,3000),
           SYSDATE,
           :NEW.nm_usuario,
           SYSDATE,
           :NEW.nm_usuario,
           :NEW.nr_prescricao,
           :NEW.nr_sequencia_prescricao,
           :NEW.nr_seq_captura,
           'INSERT');
  END;
  ELSIF (UPDATING) THEN
  BEGIN
    INSERT INTO prescr_proc_captura_log(NR_SEQUENCIA,
                                        DS_CALL_STACK,
                                        DT_ATUALIZACAO,
                                        NM_USUARIO,
                                        DT_ATUALIZACAO_NREC,
                                        NM_USUARIO_NREC,
                                        NR_PRESCRICAO,
                                        NR_SEQ_PRESCRICAO,
                                        NR_SEQ_CAPTURA_IMAGEM,
                                        DS_OPERACAO)
    VALUES(nr_sequencia_w,
           substr(dbms_utility.format_call_stack,1,3000),
           SYSDATE,
           :NEW.nm_usuario,
           SYSDATE,
           :NEW.nm_usuario,
           :NEW.nr_prescricao,
           :NEW.nr_sequencia_prescricao,
           :NEW.nr_seq_captura,
           'UPDATE');
  END;
  ELSE 
  BEGIN
    INSERT INTO prescr_proc_captura_log(NR_SEQUENCIA,
                                        DS_CALL_STACK,
                                        DT_ATUALIZACAO,
                                        NM_USUARIO,
                                        DT_ATUALIZACAO_NREC,
                                        NM_USUARIO_NREC,
                                        NR_PRESCRICAO,
                                        NR_SEQ_PRESCRICAO,
                                        NR_SEQ_CAPTURA_IMAGEM,
                                        DS_OPERACAO)
    VALUES(nr_sequencia_w,
           substr(dbms_utility.format_call_stack,1,3000),
           SYSDATE,
           :OLD.nm_usuario,
           SYSDATE,
           :OLD.nm_usuario,
           :OLD.nr_prescricao,
           :OLD.nr_sequencia_prescricao,
           :OLD.nr_seq_captura,
           'DELETE');
  END;
  END IF;
END;
/
 