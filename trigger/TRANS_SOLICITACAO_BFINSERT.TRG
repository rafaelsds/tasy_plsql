CREATE OR REPLACE TRIGGER trans_solicitacao_bfinsert
BEFORE INSERT ON trans_solicitacao
FOR EACH ROW
DECLARE

nr_sequencia_w	number(10);
nr_idade_pf_w	number(10);

BEGIN

/* Gravar histórico de status da solicitação de transporte */
select	trans_historico_seq.nextval
into	nr_sequencia_w
from	dual;

insert into trans_historico(
	nr_sequencia,           
	dt_atualizacao,         
	nm_usuario,             
	dt_atualizacao_nrec,    
	nm_usuario_nrec,        
	nr_seq_solicitacao,     
	ie_status,              
	dt_inicio,              
	dt_final)
values (nr_sequencia_w,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	:new.nr_sequencia,
	:new.ie_status,
	sysdate,
	null);

/*  Tratar se a solicitação se encaixa em alguma regra de prioridade */
nr_idade_pf_w := Obter_Idade_PF(:new.cd_pessoa_fisica, sysdate, 'A');

begin
	select	nr_sequencia
	into	nr_sequencia_w
	from	trans_regra_prioridade
	where	cd_setor_atendimento = :new.cd_setor_origem
	and	nr_idade_pf_w < qt_menor_faixa
	and	nr_idade_pf_w > qt_maior_faixa;
exception
	when	no_data_found then
		nr_sequencia_w := 0;
end;

If	(nr_sequencia_w > 0) then
	:new.ie_regra_prioridade := 'S';
else
	:new.ie_regra_prioridade := 'N';
end if;
	
END;
/
