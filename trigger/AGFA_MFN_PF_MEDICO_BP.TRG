CREATE OR REPLACE TRIGGER AGFA_MFN_PF_MEDICO
BEFORE UPDATE OF
	NM_PESSOA_FISICA,
	NR_TELEFONE_CELULAR
ON PESSOA_FISICA
FOR EACH ROW
DECLARE

nr_sequencia_w		AGFA_MFN.MSH_10_ID%type; --Number(08,0);
ie_operacao_w			Number(1)		:= 1;
cd_pessoa_fisica_w		Varchar2(10);
nr_crm_w			Varchar2(25);
uf_crm_w			varchar2(2);
nm_sobrenome_w		Varchar2(80);
nm_nome_w			Varchar2(80);
nr_celular_w			Varchar2(15);
nr_telefone_w			Varchar2(15);
ds_email_w			Varchar2(100);
ds_endereco_w			Varchar2(250);
dt_inativacao_w		Date;
ie_status_w			Varchar2(20)		:= 'N';
nm_iniciais_w			varchar2(15);
nm_tratamento_w			varchar2(15);
ds_especialidade_w		varchar2(15);
ie_origem_inf_w			varchar2(5);
qt_reg_w				number(10,0)	:= 0;
sg_conselho_w			varchar2(10);
ds_erro_w				varchar2(2000);

BEGIN

begin

sg_conselho_w	:= substr(obter_conselho_profissional(:new.nr_seq_conselho, 'S'),1,10);

if	((nvl(:new.NM_PESSOA_FISICA,'0') <> nvl(:old.NM_PESSOA_FISICA,'0')) or
	(nvl(:new.NR_TELEFONE_CELULAR,'0') <> nvl(:old.NR_TELEFONE_CELULAR,'0'))) then

cd_pessoa_fisica_w		:= :new.cd_pessoa_fisica;
select	max(nr_crm),
	max(uf_crm),
	max(dt_desligamento),
	max(ie_origem_inf)
into	nr_crm_w,
	uf_crm_w,
	dt_inativacao_w,
	ie_origem_inf_w
from	medico
where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

if	(nr_crm_w is not null)  and
	(nvl(ie_origem_inf_w,'X') <> 'AD') then

	select Agfa_MFN_seq.nextval
	into	nr_sequencia_w
	from	dual;
	nm_sobrenome_w		:= obter_parte_nome(:new.nm_pessoa_fisica,'RestoNome');
	nm_nome_w		:= obter_parte_nome(:new.nm_pessoa_fisica,'Nome');
	nm_iniciais_w		:= obter_iniciais_nome(:new.cd_pessoa_fisica, :new.nm_pesSoa_fisica);
	nr_celular_w		:= substr(:new.nr_telefone_celular,1,15);

	if	(:new.ie_sexo = 'M') then
		nm_tratamento_w		:= 'Dr.';
	elsif	(:new.ie_sexo = 'F') then
		nm_tratamento_w		:= 'Dra.';
	else
		nm_tratamento_w		:= 'Dr(a).';
	end if;

	ds_especialidade_w	:= substr(Obter_Especialidade_medico(:new.cd_pessoa_fisica,'D'),1,15);

	ie_operacao_w		:= 3;
	select substr(max(ds_endereco || '^' || nr_endereco || '^' || ds_bairro || '^' || ds_municipio || '^' || sg_estado),1,250),
		max(nr_telefone),
		max(ds_email)
	into	ds_endereco_w,
		nr_telefone_w,
		ds_email_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	ie_tipo_complemento	= 1;

	/*select	count(*)
	into	qt_reg_w
	from	Agfa_MFN_v
	where	cd_medico	= cd_pessoa_fisica_w;*/

	if	(qt_reg_w = 0) then
		begin

		insert into Agfa_MFN_v(
			dt_atualizacao,
			ie_tipo_pessoa,
			nr_seq_mensagem,
			ie_operacao,
			cd_medico,
			nr_crm,
			nm_sobrenome,
			nm_primeiro_nome,
			nr_telefone_celular,
			nr_telefone,
			ds_email,
			ds_endereco,
			dt_inativacao,
			ie_sistema,
			ie_status,
			nm_iniciais,
			nm_tratamento,
			ds_especialidade)
		values(
			sysdate,
			1,
			nr_sequencia_w,
			ie_operacao_w,
			cd_pessoa_fisica_w,
			sg_conselho_w || '-' || nr_crm_w || '-' || uf_crm_w,
			nm_sobrenome_w,
			nm_nome_w,
			nr_celular_w,
			nr_telefone_w,
			ds_email_w,
			ds_endereco_w,
			dt_inativacao_w,
			'SHS',
			ie_status_w,
			nm_iniciais_w,
			nm_tratamento_w,
			ds_especialidade_w);
		end;
	end if;
end if;
end if;

exception
when others then
	ds_erro_w := substr(sqlerrm,1,2000);
	insert into log_tasy(dt_atualizacao,nm_usuario,cd_log,ds_log) values (sysdate,'AGFA',-33348,ds_erro_w);
end;

END;
/
