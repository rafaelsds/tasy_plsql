create or replace trigger tx_doador_potencial_insert
before insert on tx_doador_potencial
for each row
declare
ie_tipo_orgao_receptor_w	varchar2(15);
ie_tipo_orgao_doador_w	varchar2(15);

begin

/* Verifica compatibilidade entre �rg�os */
select	substr(tx_obter_dados_receptor(:new.nr_seq_receptor, 'TO'),1,15)
into	ie_tipo_orgao_receptor_w
from	dual;

select	tx_obter_dados_orgao(nr_seq_orgao, 'T')
into	ie_tipo_orgao_doador_w
from	tx_doador_orgao
where	nr_sequencia	= :new.nr_seq_doador_orgao;

if	(ie_tipo_orgao_receptor_w <> ie_tipo_orgao_doador_w) then
	--Os �rg�os do doador e receptor s�o incompat�veis.
	Wheb_mensagem_pck.exibir_mensagem_abort(264223);
	
end if;

end;
/
