create or replace trigger classif_unidade_atend_update
before update on classif_unidade_atend
for each row

declare
ie_possui_registro_w	number(10);
ds_setor_atendimento_w	varchar2(255);
ds_setor_setor_msg_w	varchar2(4000);
cd_setor_atendimento_w	number(10);
ie_acao_inativar_unidade_w	varchar2(1);

Cursor C01 is
		select	distinct(cd_setor_atendimento),
				substr(obter_nome_setor(cd_setor_atendimento),1,40)
		from	unidade_atendimento
		where	nr_seq_classif = :new.nr_sequencia;

begin

Obter_param_Usuario(1, 27, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_acao_inativar_unidade_w);

if	(:new.ie_situacao <> :old.ie_situacao) and
	(:new.ie_situacao = 'I') then
	select	count(*)
	into	ie_possui_registro_w
	from	unidade_atendimento
	where	nr_seq_classif = :new.nr_sequencia;
	
	if	(nvl(ie_possui_registro_w,0) > 0) then
		open C01;
		loop
		fetch C01 into	
			cd_setor_atendimento_w,
			ds_setor_atendimento_w;
		exit when C01%notfound;
			begin
			ds_setor_setor_msg_w	:= ds_setor_setor_msg_w||', '||ds_setor_atendimento_w;
			end;
		end loop;
		if	(ie_acao_inativar_unidade_w <> 'N') then
			if	(ie_acao_inativar_unidade_w = 'B') then
				Wheb_mensagem_pck.exibir_mensagem_abort(242734,'DS_SETOR_ATEND=' ||substr(ds_setor_setor_msg_w,2,4000));
			elsif (ie_acao_inativar_unidade_w = 'A') then
				update	unidade_atendimento
				set		nr_seq_classif = null
				where	nr_seq_classif = :new.nr_sequencia;
			end if;
		end if;
		close C01;
	end if;
end if;

end;
/
