CREATE OR REPLACE TRIGGER prescr_solic_bco_sangue_delete
before delete ON prescr_solic_bco_sangue
FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
pragma autonomous_transaction;
BEGIN 
begin
delete	med_avaliacao_paciente
where	nr_seq_prescr	= :old.nr_sequencia;

COMMIT;
exception
	when others then
      	dt_atualizacao := sysdate;
end;

end;
/