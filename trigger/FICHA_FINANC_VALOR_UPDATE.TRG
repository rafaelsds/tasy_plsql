create or replace trigger ficha_financ_valor_update
after update on ficha_financ_valor
for each row

declare

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

if	(:old.ds_macro <> :new.ds_macro) then
	ficha_financ_altera_macro(:old.ds_macro, :new.ds_macro);
end if;
end if;
end;
/