create or replace trigger proj_dependencia_insert
after insert on proj_dependencia
for each row

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	if	(:new.nr_seq_ordem_serv is not null) then
		begin
		gerar_nec_vinc_dep_proj(:new.nr_seq_projeto, :new.nr_sequencia, :new.nr_seq_ordem_serv, wheb_usuario_pck.get_nm_usuario);
		end;
	end if;
	end;
end if;
end;
/