create or replace trigger agenda_pac_opme_tie
after insert or update or delete on agenda_pac_opme
for each row

declare

json_w            philips_json;
json_data_w       clob;
opsm_w            varchar2(5);
nr_seq_agenda_w   agenda_pac_opme.nr_seq_agenda%type;

pragma autonomous_transaction;

cursor c01 is
select  a.nr_sequencia schedule_id,
        obter_nome_agenda(a.cd_agenda) schedule,
        b.nr_seq_classif schedule_classification_id,
        substr(obter_desc_classif_agenda_cir(b.nr_seq_classif),1,255) schedule_classification,
        a.ie_status_agenda schedule_status_id,
        substr(obter_status_agenda_paciente(a.nr_sequencia),1,255) schedule_status,
        a.dt_agenda schedule_date,
        a.dt_agendamento scheduling_date,
        a.cd_agenda schedule_code,
        a.cd_procedimento procedure_id,
        substr(obter_descricao_procedimento(a.cd_procedimento, a.ie_origem_proced),1,100) procedure_description,
        wheb_mensagem_pck.get_texto(163013) procedure_status,
        a.nr_reserva reservation,
        c.dt_entrada admission_date,
        a.dt_chegada_prev estimated_admission_date,
        obter_estagio_autor_agepac(a.nr_sequencia,'C') authorization_status_id,
        obter_estagio_autor_agepac(a.nr_sequencia,'D') authorization_status,
        a.hr_inicio start_time,
        a.qt_idade_gestacional gestational_age,
        a.qt_idade_paciente age,
        substr(obter_nome_medico(a.cd_medico,'NC'),1,255) surgeon,
        c.nr_atendimento encounter,
        obter_unid_atend_setor_atual(c.nr_atendimento, obter_setor_atendimento(a.nr_atendimento), 'U') room,
        substr(obter_desc_convenio(a.cd_convenio),1,255) insurance,
        a.cd_usuario_convenio insurance_user,
        obter_valor_dominio(1545, a.ie_reserva_leito) admission,
        a.nr_telefone telephone,
        a.nr_minuto_duracao procedure_estimated_duration,
        (to_char((a.hr_inicio + to_number(a.nr_minuto_duracao)/(24*60)),'hh24:mi:ss')) procedure_duration,
        a.cd_pessoa_fisica patient_id,
        d.nm_pessoa_fisica patient,
        d.dt_nascimento birth,
        d.nr_prontuario medical_record,
        d.ie_tipo_sangue || ' ' || d.ie_fator_rh blood,
        d.nr_telefone_celular cellphone,
        obter_nome_pessoa_fisica(d.cd_pessoa_mae,'') mother,
        decode(d.ie_sexo, 'M', wheb_mensagem_pck.get_texto(354750), 'F', wheb_mensagem_pck.get_texto(354751)) gender,
        decode(a.ie_anestesia, 'N', wheb_mensagem_pck.get_texto(1118500), 'S', wheb_mensagem_pck.get_texto(307592), 'X', wheb_mensagem_pck.get_texto(763069)) anaesthesia,
        decode(a.ie_carater_cirurgia, 'A', wheb_mensagem_pck.get_texto(796908), 'E', wheb_mensagem_pck.get_texto(312715), 'M', wheb_mensagem_pck.get_texto(415759), 'U', wheb_mensagem_pck.get_texto(309481)) surgery_nature,
        nvl((select max(wheb_mensagem_pck.get_texto(94754)) from agenda_pac_sangue h where h.nr_seq_agenda = a.nr_sequencia), wheb_mensagem_pck.get_texto(94755)) preorder_blood,
        nvl((nvl((select max(wheb_mensagem_pck.get_texto(94754)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 7), 
        (select max(wheb_mensagem_pck.get_texto(1118702)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 8))), wheb_mensagem_pck.get_texto(94755)) recommendation,
        (select max(h.ds_classif_paciente) from tipo_classificao_paciente h, agenda_paciente i where h.nr_sequencia = i.nr_seq_tipo_classif_pac and i.nr_sequencia = a.nr_sequencia) patient_classification,
        (select max(h.nr_cirurgia) from cirurgia h where h.nr_seq_agenda = a.nr_sequencia) surgery,
        (select max(h.ds_cobertura) from convenio_cobertura h where h.nr_sequencia = a.nr_seq_cobertura) insurance_coverage,
        (select max(h.ds_observacao) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 10) notes,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico = 6818), wheb_mensagem_pck.get_texto(94755)) ICU,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6319), wheb_mensagem_pck.get_texto(94755)) NICU,
        nvl((select     max(substr(obter_valor_dominio(3195,h.ie_status),1,255))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6817), wheb_mensagem_pck.get_texto(94755)) surgical_services_status
from    agenda_paciente a,
        agenda b,
        atendimento_paciente c,
        pessoa_fisica d,
        pessoa_classif e,
        classif_agenda_cirurgica f,
        autorizacao_convenio g
where   a.cd_pessoa_fisica = e.cd_pessoa_fisica(+)
and     g.nr_seq_agenda(+) = a.nr_sequencia
and     a.nr_atendimento = c.nr_atendimento(+)
and     a.cd_agenda = b.cd_agenda
and     f.nr_sequencia = b.nr_seq_classif(+)
and     d.cd_pessoa_fisica = a.cd_pessoa_fisica
and     a.ie_status_agenda not in ('L', 'B', 'C')
and     a.hr_inicio between sysdate 
and     sysdate + to_number(obter_valor_param_usuario(410, 64, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento))/24
and     b.cd_tipo_agenda = 1
and     a.nr_sequencia = nr_seq_agenda_w
union
select  a.nr_sequencia schedule_id,
        obter_nome_agenda(a.cd_agenda) schedule,
        b.nr_seq_classif schedule_classification_id,
        substr(obter_desc_classif_agenda_cir(b.nr_seq_classif),1,255) schedule_classification,
        a.ie_status_agenda schedule_status_id,
        substr(obter_status_agenda_paciente(a.nr_sequencia),1,255) schedule_status,
        a.dt_agenda schedule_date,
        a.dt_agendamento scheduling_date,
        a.cd_agenda schedule_code,
        j.cd_procedimento procedure_id,
        substr(obter_descricao_procedimento(j.cd_procedimento, j.ie_origem_proced),1,100) procedure_description,
        wheb_mensagem_pck.get_texto(163013) procedure_status,
        a.nr_reserva reservation,
        c.dt_entrada admission_date,
        a.dt_chegada_prev estimated_admission_date,
        obter_estagio_autor_agepac(a.nr_sequencia,'C') authorization_status_id,
        obter_estagio_autor_agepac(a.nr_sequencia,'D') authorization_status,
        a.hr_inicio start_time,
        a.qt_idade_gestacional gestational_age,
        a.qt_idade_paciente age,
        substr(obter_nome_medico(a.cd_medico,'NC'),1,255) surgeon,
        c.nr_atendimento encounter,
        obter_unid_atend_setor_atual(c.nr_atendimento, obter_setor_atendimento(a.nr_atendimento), 'U') room,
        substr(obter_desc_convenio(a.cd_convenio),1,255) insurance,
        a.cd_usuario_convenio insurance_user,
        obter_valor_dominio(1545, a.ie_reserva_leito) admission,
        a.nr_telefone telephone,
        a.nr_minuto_duracao procedure_estimated_duration,
        (to_char((a.hr_inicio + to_number(a.nr_minuto_duracao)/(24*60)),'hh24:mi:ss')) procedure_duration,
        a.cd_pessoa_fisica patient_id,
        d.nm_pessoa_fisica patient,
        d.dt_nascimento birth,
        d.nr_prontuario medical_record,
        d.ie_tipo_sangue || ' ' || d.ie_fator_rh blood,
        d.nr_telefone_celular cellphone,
        obter_nome_pessoa_fisica(d.cd_pessoa_mae,'') mother,
        decode(d.ie_sexo, 'M', wheb_mensagem_pck.get_texto(354750), 'F', wheb_mensagem_pck.get_texto(354751)) gender,
        decode(a.ie_anestesia, 'N', wheb_mensagem_pck.get_texto(1118500), 'S', wheb_mensagem_pck.get_texto(307592), 'X', wheb_mensagem_pck.get_texto(763069)) anaesthesia,
        decode(a.ie_carater_cirurgia, 'A', wheb_mensagem_pck.get_texto(796908), 'E', wheb_mensagem_pck.get_texto(312715), 'M', wheb_mensagem_pck.get_texto(415759), 'U', wheb_mensagem_pck.get_texto(309481)) surgery_nature,
        nvl((select max(wheb_mensagem_pck.get_texto(94754)) from agenda_pac_sangue h where h.nr_seq_agenda = a.nr_sequencia), wheb_mensagem_pck.get_texto(94755)) preorder_blood,
        nvl((nvl((select max(wheb_mensagem_pck.get_texto(94754)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 7), 
        (select max(wheb_mensagem_pck.get_texto(1118702)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 8))), wheb_mensagem_pck.get_texto(94755)) recommendation,
        (select max(h.ds_classif_paciente) from tipo_classificao_paciente h, agenda_paciente i where h.nr_sequencia = i.nr_seq_tipo_classif_pac and i.nr_sequencia = a.nr_sequencia) patient_classification,
        (select max(h.nr_cirurgia) from cirurgia h where h.nr_seq_agenda = a.nr_sequencia) surgery,
        (select max(h.ds_cobertura) from convenio_cobertura h where h.nr_sequencia = a.nr_seq_cobertura) insurance_coverage,
        (select max(h.ds_observacao) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 10) notes,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico = 6818), wheb_mensagem_pck.get_texto(94755)) ICU,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6319), wheb_mensagem_pck.get_texto(94755)) NICU,
        nvl((select     max(substr(obter_valor_dominio(3195,h.ie_status),1,255))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6817), wheb_mensagem_pck.get_texto(94755)) surgical_services_status
from    agenda_paciente a,
        agenda b,
        atendimento_paciente c,
        pessoa_fisica d,
        pessoa_classif e,
        classif_agenda_cirurgica f,
        autorizacao_convenio g,
        agenda_paciente_proc j
where   a.cd_pessoa_fisica = e.cd_pessoa_fisica(+)
and     g.nr_seq_agenda(+) = a.nr_sequencia
and     a.nr_atendimento = c.nr_atendimento(+)
and     a.cd_agenda = b.cd_agenda
and     f.nr_sequencia = b.nr_seq_classif(+)
and     d.cd_pessoa_fisica = a.cd_pessoa_fisica
and     j.nr_sequencia = a.nr_sequencia
and     a.dt_agenda >= sysdate
and     a.ie_status_agenda not in ('L', 'B', 'C')
and     a.hr_inicio between sysdate
and     (sysdate + to_number(obter_valor_param_usuario(410, 64, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento))/24)
and     b.cd_tipo_agenda = 1
and     a.nr_sequencia = nr_seq_agenda_w
order by schedule_date, start_time, patient;

begin

if (inserting or updating) then

    nr_seq_agenda_w := :new.nr_seq_agenda;

    if (:new.dt_exclusao is null) then
        opsm_w := wheb_mensagem_pck.get_texto(94754);
    else
        select nvl(max(wheb_mensagem_pck.get_texto(94754)),wheb_mensagem_pck.get_texto(94755))
        into opsm_w
        from agenda_pac_opme h 
        where h.nr_seq_agenda = nr_seq_agenda_w
        and h.nr_sequencia <> :new.nr_sequencia
        and h.dt_exclusao is null;
    end if;

elsif (deleting) then

    nr_seq_agenda_w := :old.nr_seq_agenda;  

    select nvl(max(wheb_mensagem_pck.get_texto(94754)),wheb_mensagem_pck.get_texto(94755))
    into opsm_w
    from agenda_pac_opme h 
    where h.nr_seq_agenda = nr_seq_agenda_w
    and h.nr_sequencia <> :old.nr_sequencia
    and h.dt_exclusao is null;

end if;

if (wheb_usuario_pck.get_nm_usuario is not null) then

	for c01_w in c01 loop

		json_w := philips_json();
		json_w.put('schedule_id', c01_w.schedule_id);
		json_w.put('schedule', c01_w.schedule);
		json_w.put('schedule_classification_id', c01_w.schedule_classification_id);
		json_w.put('schedule_classification', c01_w.schedule_classification);
		json_w.put('schedule_status_id', c01_w.schedule_status_id);
		json_w.put('schedule_status', c01_w.schedule_status);
		json_w.put('schedule_date', c01_w.schedule_date);
		json_w.put('scheduling_date', c01_w.scheduling_date);
		json_w.put('schedule_code', c01_w.schedule_code);
		json_w.put('procedure_id', c01_w.procedure_id);
		json_w.put('procedure_description', c01_w.procedure_description);
		json_w.put('procedure_status', c01_w.procedure_status);
		json_w.put('reservation', c01_w.reservation);
		json_w.put('admission_date', c01_w.admission_date);
		json_w.put('estimated_admission_date', c01_w.estimated_admission_date);
		json_w.put('authorization_status_id', c01_w.authorization_status_id);
		json_w.put('authorization_status', c01_w.authorization_status);
		json_w.put('start_time', c01_w.start_time);
		json_w.put('gestational_age', c01_w.gestational_age);
		json_w.put('age', c01_w.age);
		json_w.put('surgeon', c01_w.surgeon);
		json_w.put('encounter', c01_w.encounter);
		json_w.put('room', c01_w.room);
		json_w.put('insurance', c01_w.insurance);
		json_w.put('insurance_user', c01_w.insurance_user);
		json_w.put('admission', c01_w.admission);
		json_w.put('telephone', c01_w.telephone);
		json_w.put('procedure_estimated_duration', c01_w.procedure_estimated_duration);
		json_w.put('procedure_duration', c01_w.procedure_duration);
		json_w.put('patient_id', c01_w.patient_id);
		json_w.put('patient', c01_w.patient);
		json_w.put('birth', c01_w.birth);
		json_w.put('medical_record', c01_w.medical_record);
		json_w.put('blood', c01_w.blood);
		json_w.put('cellphone', c01_w.cellphone);
		json_w.put('mother', c01_w.mother);
		json_w.put('gender', c01_w.gender);
		json_w.put('anaesthesia', c01_w.anaesthesia);
		json_w.put('surgery_nature', c01_w.surgery_nature);
		json_w.put('preorder_blood', c01_w.preorder_blood);
		json_w.put('OPSM', opsm_w);
		json_w.put('recommendation', c01_w.recommendation);
		json_w.put('patient_classification', c01_w.patient_classification);
		json_w.put('surgery', c01_w.surgery);
		json_w.put('insurance_coverage', c01_w.insurance_coverage);
		json_w.put('notes', c01_w.notes);
		json_w.put('ICU', c01_w.ICU);
		json_w.put('NICU', c01_w.NICU);
		json_w.put('surgical_services_status', c01_w.surgical_services_status);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		json_data_w := bifrost.send_integration_content('cssd.management.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);

	end loop;

end if;

end;
/
