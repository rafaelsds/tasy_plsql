create or replace trigger reg_caso_teste_before_insert
  before insert on reg_caso_teste
  for each row
declare

  nr_seq_intencao_uso_w number(10);
  ds_prefixo_w varchar2(15);
  nr_sequencia_w varchar2(15);
  nr_seq_ct_w number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

--Caso de teste de validacao
if	(:new.ie_tipo_documento = 'V') then

nr_seq_intencao_uso_w := :new.nr_seq_intencao_uso;

  if (:new.nr_seq_customer is not null ) then
	select iu.nr_sequencia,
	     iu.ds_prefixo
	into nr_seq_intencao_uso_w,
	     ds_prefixo_w
	from reg_area_customer ac,
	     reg_features_customer fc,
	     reg_intencao_uso iu,
	     reg_customer_requirement cr
	where ac.nr_sequencia = fc.nr_seq_area_customer
	and ac.nr_seq_intencao_uso = iu.nr_sequencia
	and fc.nr_sequencia = cr.nr_seq_features
	and cr.nr_sequencia = :new.nr_seq_customer;
  end if;

  if (:new.nr_seq_customer is not null and
	nr_seq_intencao_uso_w is not null and
	nr_seq_intencao_uso_w <> 2
     --:new.nr_seq_intencao_uso is not null and
     --:new.nr_seq_intencao_uso <> 2
     ) then

    --select iu.nr_sequencia, iu.ds_prefixo
    --  into nr_seq_intencao_uso_w, ds_prefixo_w
    --  from reg_customer_requirement urs, reg_intencao_uso iu
   --  where urs.nr_sequencia = :new.nr_seq_customer
   --    and urs.nr_seq_intencao_uso = iu.nr_sequencia;

    select max(a.nr_sequencia)
      into nr_seq_ct_w
      from reg_caso_teste a
     where a.nr_seq_intencao_uso = nr_seq_intencao_uso_w
     and   a.ie_tipo_documento = 'V';

    if nr_seq_ct_w is not null then

      select nvl(obter_somente_numero(cd_ct_id), 0) + 1
        into nr_sequencia_w
        from reg_caso_teste
       where nr_sequencia = nr_seq_ct_w;

    end if;

    :new.cd_ct_id := nvl(ds_prefixo_w, nr_seq_intencao_uso_w) || '_TCVAL_' ||
                     nvl(nr_sequencia_w, '1');


  elsif (:new.nr_seq_customer is not null and
        nr_seq_intencao_uso_w is not null and
        nr_seq_intencao_uso_w = 2) then

    :new.cd_ct_id            := 'TASY_VTC_' || :new.nr_sequencia;
    --:new.nr_seq_intencao_uso := 2;

    end if;


end if;

--Caso de teste de verificacao
if	(:new.ie_tipo_documento = 'T') then

nr_seq_intencao_uso_w := :new.nr_seq_intencao_uso;

  if (:new.nr_seq_product is not null and :new.nr_seq_intencao_uso is not null) then

	select iu.nr_sequencia,
           iu.ds_prefixo
      into nr_seq_intencao_uso_w,
           ds_prefixo_w
      from reg_product_requirement prs,
           reg_intencao_uso iu
     where prs.nr_sequencia = :new.nr_seq_product
       and prs.nr_seq_intencao_uso = iu.nr_sequencia;

   end if;

if (:new.nr_seq_product is not null and
        nr_seq_intencao_uso_w is not null and
        nr_seq_intencao_uso_w <> 2) then

    select max(a.nr_sequencia)
      into nr_seq_ct_w
      from reg_caso_teste a
     where a.nr_seq_intencao_uso = nr_seq_intencao_uso_w
       and a.ie_tipo_documento = 'T';

    if (nr_seq_ct_w is not null) then

      select nvl(obter_somente_numero(cd_ct_id), 0) + 1
        into nr_sequencia_w
        from reg_caso_teste
       where nr_sequencia = nr_seq_ct_w;

    end if;

    :new.cd_ct_id := nvl(ds_prefixo_w, nr_seq_intencao_uso_w) || '_TCVER_' ||
                     nvl(nr_sequencia_w, '1');

  elsif (:new.nr_seq_product is not null and
        (nr_seq_intencao_uso_w is not null and
        nr_seq_intencao_uso_w = 2) or :new.nr_seq_intencao_uso is null ) then

    :new.cd_ct_id            := 'TASY_TC_' || :new.nr_sequencia;
    --:new.nr_seq_intencao_uso := 2;

  end if;
end if;
--necessario para atribuir intencao de uso para quando esta inserindo pela funcao antiga

<<Final>>

:new.nr_seq_intencao_uso := nr_seq_intencao_uso_w;

end;
/