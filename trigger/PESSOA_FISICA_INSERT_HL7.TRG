create or replace
trigger pessoa_fisica_insert_hl7
after insert on pessoa_fisica	
for each row

declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

begin
ds_sep_bv_w := obter_separador_bv;

ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
gravar_agend_integracao(71, ds_param_integ_hl7_w);

ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
gravar_agend_integracao(144, ds_param_integ_hl7_w);

if (:new.cd_pessoa_fisica is not null) then  
  call_bifrost_content('patient.create','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);
end if;

end;
/