create or replace trigger prescr_procedimento_update_hl7
after update on prescr_procedimento
for each row

declare                                                                         
ds_sep_bv_w			varchar2(100);                                                     
ds_param_integ_hl7_w		varchar2(4000);                                            
nr_seq_interno_w		number(10);                                                    
nr_atendimento_w		number(10);                                                    
cd_pessoa_fisica_w		varchar2(60);                                                
ie_order_control_w	  	varchar2(2);                                               
ie_order_status_w	  	varchar2(2);                                                
ie_permite_proc_interno_w 	varchar2(2);                                          
ie_permite_proc_interno2_w 	varchar2(2);
ie_permite_proc_interno_cw_w	varchar2(2);
ie_permite_proc_interno_rc_w	varchar2(2);                                        
cd_estabelecimento_w		number(10);                                                
ie_status_envio_w		number(5);                                                    
ie_formato_resultado_w		varchar2(10);                                            
qt_equip_lab_w			number(10);                                                     
ie_envia_w			varchar2(1);                                                        
qt_proc_laudo_w			number(10);                                                    
nr_seq_proc_status_w		number(10);         
qt_prescr_proc_material_w	number(10);
qt_status_amostra_w		number(10);
nr_seq_grupo_w			number(10);
nr_seq_grupo_imp_w		number(10);
ie_grupo_imp_amostra_seq_w	varchar2(1);
nr_seq_prescr_w			number(10);
nr_seq_mat_item_w		number(10);
ie_integra_tasylab_w	varchar2(1);
cancela_procedimento_pacsge_w varchar2(1);
ie_permite_proc_interno_ods_w	varchar2(255);
ie_permite_proc_interno_cb_w	varchar2(255);
ie_integra_laboredo_w	varchar2(1);
w_qtd 				number(10);
q_qtd 				number(10);
qt_reg_w			number(1);
proc_hor_sequence_w  prescr_proc_hor.nr_sequencia%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

if ((:old.dt_cancelamento is null) and (:new.dt_cancelamento is not null)) or  ((:old.dt_suspensao is null) and (:new.dt_suspensao is not null)) then
  -- Laboredo (S.O.1820028)
  select 	permite_proc_interno_integ(:new.nr_seq_proc_interno,184)
	into	ie_integra_laboredo_w
	from 	dual;
  if (ie_integra_laboredo_w = 'S') then

  ds_sep_bv_w := obter_separador_bv;

	select	max(nr_atendimento),                                                   
			max(cd_pessoa_fisica)                                                        
	into	nr_atendimento_w,                                                        
		    cd_pessoa_fisica_w                                                          
	from	prescr_medica                                                            
	where	nr_prescricao	= :new.nr_prescricao;   
	ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w 	|| ds_sep_bv_w  ||
							'nr_atendimento='   || nr_atendimento_w   	|| ds_sep_bv_w ||
							'nr_prescricao='    || :new.nr_prescricao 	|| ds_sep_bv_w  ||
							'nr_seq_presc='	    || :new.nr_sequencia  	|| ds_sep_bv_w  ||	
							'nr_acesso_dicom='  || :new.nr_acesso_dicom || ds_sep_bv_w ; 

    gravar_agend_integracao(804, ds_param_integ_hl7_w);
  end if;
end if;

--Synapse (FUJI)
if	(:new.dt_cancelamento is not null) or   (:new.dt_suspensao is not null) and
	(verifica_proced_integracao(:new.nr_seq_proc_interno,:new.cd_procedimento,:new.ie_origem_proced, 91) = 'S') then
	
		select	max(nr_atendimento),                                                   
			    max(cd_pessoa_fisica)                                                        
		into	nr_atendimento_w,                                                        
			    cd_pessoa_fisica_w                                                           
		from	prescr_medica                                                            
		where	nr_prescricao	= :new.nr_prescricao;                                     

		select	max(obter_atepacu_paciente( nr_atendimento_w ,'IAA'))
		into	nr_seq_interno_w
		from	dual;                                                                    

		ds_sep_bv_w := obter_separador_bv;

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||                                                                          
						'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||               
						'nr_seq_interno='   || nr_seq_interno_w   || ds_sep_bv_w ||               
						'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||               
						'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ||               
						'order_control='    || 'CA' || ds_sep_bv_w ||               
						'order_status='     || 'CA'  || ds_sep_bv_w ;                

		gravar_agend_integracao(530, ds_param_integ_hl7_w);                           
end if;

	select	decode(count(1),0,'N','S')
                                  into cancela_procedimento_pacsge_w
	from	
		proc_interno_integracao b
	where	1 = 1
	and	b.nr_seq_proc_interno = :new.nr_seq_proc_interno 
	and	nvl(b.cd_estabelecimento,nvl(obter_estabelecimento_ativo,0)) = nvl(obter_estabelecimento_ativo,0)	
	and	b.nr_seq_sistema_integ =108 ;		-- GE-Centricity-RISi
                                                                           

if	((:new.dt_cancelamento is not null) or  (:new.dt_suspensao is not null)) and	(cancela_procedimento_pacsge_w = 'S') then                                                                           
		ds_sep_bv_w := obter_separador_bv;
  		select	max(nr_atendimento),
			        max(cd_pessoa_fisica)                                                        
		    into	nr_atendimento_w,  
			        cd_pessoa_fisica_w 
		from	prescr_medica  
		where	nr_prescricao	= :new.nr_prescricao;  

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica='        ||cd_pessoa_fisica_w|| ds_sep_bv_w ||

					'nr_atendimento='   || nr_atendimento_w      || ds_sep_bv_w ||

					'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||

					'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||

					'nr_seq_presc='	    || :new.nr_sequencia	     || ds_sep_bv_w ||	

					'order_control='    || 'CA'    		           || ds_sep_bv_w ||	

					'order_status='     || 'c'    		           || ds_sep_bv_w ||

          'nr_seq_anamese='   || ''   	 || ds_sep_bv_w ;
					

		gravar_agend_integracao(667, ds_param_integ_hl7_w);
end if;

if	(:new.nr_seq_exame is null) and                                              
	(((:new.ie_status_execucao	=  '20') and                                        
	(somente_numero(:old.ie_status_execucao) < 20) and                             
	(somente_numero(:old.ie_status_execucao) > 0)) or                              
	(--(:new.ie_status_execucao	=  '10') and      -removido pois no processo de cliente que utiliza o Isite o status se mantem em 20                                   
	(:new.ie_suspenso		=  'S') and                                                 
	(:old.ie_suspenso		=  'N'))) then                                              

	select 	permite_proc_interno_integ(:new.nr_seq_proc_interno,11)
	into	ie_permite_proc_interno_w
	from 	dual;
	
	select 	permite_proc_interno_integ(:new.nr_seq_proc_interno,66)
	into	ie_permite_proc_interno2_w
	from 	dual;

	select 	permite_proc_interno_integ(:new.nr_seq_proc_interno,139)
	into	ie_permite_proc_interno_cw_w
	from 	dual;
	
	select 	permite_proc_interno_integ(:new.nr_seq_proc_interno,140)
	into	ie_permite_proc_interno_rc_w
	from 	dual;
	
	select 	permite_proc_interno_integ(:new.nr_seq_proc_interno,152)
	into	ie_permite_proc_interno_ods_w
	from 	dual;
	
	select 	permite_proc_interno_integ(:new.nr_seq_proc_interno,153)
	into	ie_permite_proc_interno_cb_w
	from 	dual;
	
	select	max(cd_estabelecimento),
			max(nr_atendimento)
	into	cd_estabelecimento_w,
			nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = :new.nr_prescricao;

	if	(ie_permite_proc_interno_w  <>'S') then
		
		/* iSite */
		select	Obter_Se_Integr_Proc_Interno(:new.nr_seq_proc_interno,2,null,:new.ie_lado, cd_estabelecimento_w)
		into	ie_permite_proc_interno_w
		from	dual;
			
	end if;

	if	('S' in (ie_permite_proc_interno_w,ie_permite_proc_interno2_w,ie_permite_proc_interno_cw_w,ie_permite_proc_interno_rc_w,ie_permite_proc_interno_ods_w,ie_permite_proc_interno_cb_w)) then

		select	max(nr_atendimento),                                                   
			max(cd_pessoa_fisica)                                                        
		into	nr_atendimento_w,                                                        
			cd_pessoa_fisica_w                                                           
		from	prescr_medica                                                            
		where	nr_prescricao	= :new.nr_prescricao;                                     

		select	max(obter_atepacu_paciente( nr_atendimento_w ,'A'))
		into	nr_seq_interno_w
		from	dual;                                                                    

		ds_sep_bv_w := obter_separador_bv;

		ie_envia_w := 'S';

		if 	((:new.ie_suspenso = 'S') and 
			(:old.ie_suspenso <>'S')) or 
			(:new.ie_status_execucao = '10') then --suspenso
			
			ie_order_control_w := 'OC';                                                  
			ie_order_status_w  := 'CA';
			
			if	(ie_permite_proc_interno2_w = 'S') then
				begin
				ie_order_control_w := 'CA';                                                  
				ie_order_status_w  := 'OC';
				end;
			end if;
			
			if	(ie_permite_proc_interno_cw_w = 'S') then
				begin
				ie_order_control_w := 'CA';                                                  
				ie_order_status_w  := 'OC';
				end;
			end if;
			
			if	(ie_permite_proc_interno_rc_w = 'S') then
				begin
				ie_order_control_w := 'CA';                                                  
				ie_order_status_w  := 'OC';
				end;
			end if;
			
			if	(ie_permite_proc_interno_ods_w = 'S') then
				begin
				ie_order_control_w := 'CA';                                                  
				ie_order_status_w  := 'OC';
				end;
			end if;
			
			if	(ie_permite_proc_interno_cb_w = 'S') then
				begin
				ie_order_control_w := 'CA';                                                  
				ie_order_status_w  := 'OC';
				end;
			end if;
		
			select	count(*)                                                              
			into	qt_proc_laudo_w                                                         
			from	prescr_proc_status                                                      
			where	nr_prescricao	= :new.nr_prescricao                                     
			and	nr_seq_prescr	= :new.nr_sequencia                                        
			and	ie_status_exec 	= '40';                                                    

			if	(qt_proc_laudo_w > 0) then                                                
				ie_envia_w := 'N';                                                          
			end if;         
		
		elsif	(:new.ie_status_execucao = '20') then --Executado                          

			ie_order_control_w := 'SC';                                                  
			ie_order_status_w  := 'CM';                                                  

			select	max(nr_sequencia)                                                     
			into	nr_seq_proc_status_w                                                    
			from	prescr_proc_status                                                      
			where	nr_prescricao	= :new.nr_prescricao                                     
			and	nr_seq_prescr	= :new.nr_sequencia                                        
			and	ie_status_exec 	= '20';                                                    

			select	count(*)                                                              
			into	qt_proc_laudo_w                                                         
			from	prescr_proc_status                                                      
			where	nr_prescricao	=  :new.nr_prescricao                                    
			and	nr_seq_prescr	=  :new.nr_sequencia                                       
			and	nr_sequencia 	<>nr_seq_proc_status_w                                    
			and	ie_status_exec 	=  '20';                                                   

			if	(qt_proc_laudo_w > 0) then                                                
				ie_envia_w := 'N';                                                          
			end if;                                                 

		end if;                                                                       

		if	(ie_envia_w = 'S') then                                                    

			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||                                                                          
						'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||               
						'nr_seq_interno='   || nr_seq_interno_w   || ds_sep_bv_w ||               
						'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||               
						'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ||               
						'order_control='    || ie_order_control_w || ds_sep_bv_w ||               
						'order_status='     || ie_order_status_w  || ds_sep_bv_w ;                

			gravar_agend_integracao(23, ds_param_integ_hl7_w);

			if	(ie_permite_proc_interno_cw_w = 'S') then
				begin
				--CDW
				ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||                                                                          
						'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||               
						'nr_seq_interno='   || nr_seq_interno_w   || ds_sep_bv_w ||               
						'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||               
						'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ||               
						'order_control='    || ie_order_control_w || ds_sep_bv_w ||               
						'order_status='     || ie_order_status_w  || ds_sep_bv_w ||
						'receiving_app='    || 'CDW' || ds_sep_bv_w;   
                        
                    if	(substr(l10nger_integrar_adt_orm(cd_pessoa_fisica_w, nr_atendimento_w, null, null),1,1) = 'S') then
                        gravar_agend_integracao(664, ds_param_integ_hl7_w);
                    end if;
				end;
			end if;
			
			if	(ie_permite_proc_interno_rc_w = 'S') then
				begin
				--
				ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||                                                                          
						'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||               
						'nr_seq_interno='   || nr_seq_interno_w   || ds_sep_bv_w ||               
						'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||               
						'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ||               
						'order_control='    || ie_order_control_w || ds_sep_bv_w ||               
						'order_status='     || ie_order_status_w  || ds_sep_bv_w ||
						'receiving_app='    || 'RADCENTRE' || ds_sep_bv_w;  
                
                    if	(substr(l10nger_integrar_adt_orm(cd_pessoa_fisica_w, nr_atendimento_w, null, null),1,1) = 'S') then
                        gravar_agend_integracao(666, ds_param_integ_hl7_w);
                    end if;
				end;
			end if;

			if	(ie_permite_proc_interno_ods_w = 'S') then
				begin
				--
				ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||                                                                          
						'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||               
						'nr_seq_interno='   || nr_seq_interno_w   || ds_sep_bv_w ||               
						'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||               
						'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ||               
						'order_control='    || ie_order_control_w || ds_sep_bv_w ||               
						'order_status='     || ie_order_status_w  || ds_sep_bv_w ||
						'receiving_app='    || 'CUSTODIAG' || ds_sep_bv_w;    
                
                    if	(substr(l10nger_integrar_adt_orm(cd_pessoa_fisica_w, nr_atendimento_w, null, null),1,1) = 'S') then
                        gravar_agend_integracao(689, ds_param_integ_hl7_w);
                    end if;    
				end;
			end if;	

			if	(ie_permite_proc_interno_cb_w = 'S') then
				begin
				--
				ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||                                                                          
						'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||               
						'nr_seq_interno='   || nr_seq_interno_w   || ds_sep_bv_w ||               
						'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||               
						'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ||               
						'order_control='    || ie_order_control_w || ds_sep_bv_w ||               
						'order_status='     || ie_order_status_w  || ds_sep_bv_w ||
						'receiving_app='    || 'CARDIOBASE' || ds_sep_bv_w;    
                        
                    if	(substr(l10nger_integrar_adt_orm(cd_pessoa_fisica_w, nr_atendimento_w, null, null),1,1) = 'S') then
                        gravar_agend_integracao(695, ds_param_integ_hl7_w);
                    end if;    
				end;
			end if;

		end if;                                                                       

	end if;
	
end if;                                                                         

if	(:new.nr_seq_exame is not null) then                                         

	select	count(*)                                                                
	into	qt_equip_lab_w                                                            
	from	lab_exame_equip l,                                                        
		equipamento_lab	e                                                             
	where	l.cd_equipamento = e.cd_equipamento                                      
	and	e.ds_sigla  	 = 'DTINNOV'                                                  
	and	l.nr_seq_exame 	 = :new.nr_seq_exame;

	if	(qt_equip_lab_w > 0) then                                                   

	
		select	max(cd_estabelecimento),                                               
			max(nr_atendimento)                                                          
		into	cd_estabelecimento_w,                                                    
			nr_atendimento_w                                                             
		from	prescr_medica                                                            
		where	nr_prescricao = :new.nr_prescricao;                                     
		
		
		select	max(ie_status_envio),
				max(ie_gerar_padrao_grupo_imp)
		into	ie_status_envio_w,
				ie_grupo_imp_amostra_seq_w
		from	lab_parametro
		where	cd_estabelecimento = cd_estabelecimento_w;

		if	((:new.ie_status_atend	=  ie_status_envio_w) and (:old.ie_status_atend <>ie_status_envio_w)) then                                                   

			select	max(ie_formato_resultado)
			into	ie_formato_resultado_w
			from	exame_laboratorio
			where	nr_seq_exame = :new.nr_seq_exame;
			
			ds_sep_bv_w := obter_separador_bv;
				
				
			if	(ie_formato_resultado_w in ('SM','SDM')) then    -- (SM)Selecao Microbiologia  /  (SDM) Selecao de microorganismo  
			
				--gerar agendamento HL7 microbiology                                        

				ds_param_integ_hl7_w :=	'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||                                                                         
							'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||              
							'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ;               
				
                                
				gravar_agend_integracao(74, ds_param_integ_hl7_w);                          
                                
			else
			
				/*select	max(nr_sequencia)
				into	nr_seq_mat_item_w
				from	prescr_proc_mat_item
				where	nr_prescricao = :new.nr_prescricao
				and	nr_seq_prescr = :new.nr_sequencia;*/

				select	max(a.nr_sequencia)
				into	nr_seq_mat_item_w
				from 	prescr_proc_material a, 
					prescr_proc_mat_item b 
				where 	b.nr_seq_prescr_proc_mat = a.nr_sequencia
				and	a.nr_prescricao = :new.nr_prescricao
				and 	b.nr_seq_prescr = :new.nr_sequencia;
				
				select 	count(*)
				into 	qt_prescr_proc_material_w
				from 	prescr_proc_material a, 
					prescr_proc_mat_item b 
				where 	b.nr_seq_prescr_proc_mat = a.nr_sequencia
				and	a.nr_prescricao = :new.nr_prescricao
				and 	b.nr_seq_prescr = :new.nr_sequencia;


				
				/*if	(qt_prescr_proc_material_w > 1) then
					
					--gerar agendamento HL7 Lab - curva
					ds_param_integ_hl7_w :=	'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||                                                                         
								'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||              
								'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ;               

					
					gravar_agend_integracao(84, ds_param_integ_hl7_w);
					
					
				else*/
				if	(qt_prescr_proc_material_w <= 1) then

					select 	max(nr_seq_grupo),
						max(nr_seq_grupo_imp)
					into	nr_seq_grupo_w,
						nr_seq_grupo_imp_w
					from	exame_laboratorio
					where	nr_seq_exame = :new.nr_seq_exame;
								 
					select	count(*)
					into	qt_status_amostra_w
					from	prescr_proc_mat_item a, prescr_proc_material b, material_exame_lab c
					where	a.nr_seq_prescr_proc_mat = b.nr_sequencia
					and	c.nr_sequencia = b.nr_seq_material
					and	b.nr_sequencia = nr_seq_mat_item_w
					and	a.nr_prescricao = :new.nr_prescricao
					and	a.nr_seq_prescr <> :new.nr_sequencia
					and	c.cd_material_exame = :new.cd_material_exame
					and	nvl(a.ie_suspenso,'N') = 'N'
					and	(((b.nr_seq_grupo = nr_seq_grupo_w) and (nvl(ie_grupo_imp_amostra_seq_w,'N') = 'N')) or
					((b.nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (nvl(ie_grupo_imp_amostra_seq_w,'N') = 'S')))
					and exists (	select	1                                                                                                                          
									from	lab_exame_equip l,                                                        
											equipamento_lab	e                                                             
									where	l.cd_equipamento = e.cd_equipamento                                      
									and		e.ds_sigla  	 = 'DTINNOV'                                                  
									and		l.nr_seq_exame 	 = obter_dados_prescr_proc_hl7(:new.nr_prescricao,a.nr_seq_prescr,'E'))
					and	ie_status_envio_w > (select ie_etapa
								     from prescr_proc_etapa
								     where nr_prescricao = :new.nr_prescricao
								     and nr_sequencia = (select max(nr_sequencia) 
											 from prescr_proc_etapa 
											 where nr_prescricao = :new.nr_prescricao
											 and nr_seq_prescricao = a.nr_seq_prescr));
											 
					
					if (qt_status_amostra_w = 0) then
					--gerar agendamento HL7 Lab    
						ds_param_integ_hl7_w :=	'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||                                                                         
									'nr_prescricao='    || :new.nr_prescricao || ds_sep_bv_w ||              
									'nr_seq_presc='	    || :new.nr_sequencia  || ds_sep_bv_w ||
									'nr_seq_prescr_proc_mat=' || nr_seq_mat_item_w || ds_sep_bv_w;               
						
						gravar_agend_integracao(73, ds_param_integ_hl7_w); 
					end if;

				end if;
			end if;

		end if;
	end if;

	if 	(wheb_usuario_pck.is_evento_ativo(221) = 'S') and
		(nvl(:new.ie_status_atend, 0) <> nvl(:old.ie_status_atend, 0)) then
		
		select 	decode(count(1),0,'N','S')
		into	ie_integra_tasylab_w
		from	LAB_TASYLAB_CLIENTE a,
				LAB_TASYLAB_CLI_PRESCR b
		where	a.nr_sequencia = b.NR_SEQ_TASYLAB_CLI
		and		b.nr_prescricao = :new.nr_prescricao
		and		nvl(a.IE_STATUS_ATEND,35) = :new.ie_status_atend;
		
		if (ie_integra_tasylab_w = 'S') then
			
			
			integrar_tasylab(	:new.nr_prescricao,
								:new.nr_sequencia,
								221,
								null,
								null,
								obter_funcao_ativa,
								obter_perfil_ativo,
								'TASYLAB',
								obter_estabelecimento_ativo,
								null,
								null,
								null,
								'N');
			
		end if;
	
	end if;
	
end if;

-- Carestream integration
if ((( :old.dt_suspensao is null  and  :new.dt_suspensao is not null ) or (:old.dt_cancelamento is null and :new.dt_cancelamento is not null)) and ( :new.nr_seq_exame is null )) then
    
	select     max(cd_estabelecimento),
                        max(nr_atendimento)
    into           cd_estabelecimento_w,
                         nr_atendimento_w
    from          prescr_medica
    where       nr_prescricao = :new.nr_prescricao;


    select carestream_japan_l10n_pck.get_integrated_hor_seq(:new.nr_prescricao, :new.nr_sequencia, :new.nr_seq_proc_interno, cd_estabelecimento_w, 233) 
	into proc_hor_sequence_w
	from dual;
    

    if ( proc_hor_sequence_w > 0 ) then
        call_interface_file(931, 'carestream_ris_japan_l10n_pck.patient_order_info (' || :new.nr_prescricao  || ',' || :new.nr_sequencia || ',' || proc_hor_sequence_w  || ',''3F'' , 3, ''' || :new.nm_usuario
                                    || ''' , 0, null, ' || cd_estabelecimento_w   || ',''N''' || ' );', :new.nm_usuario);
    end if;

    select carestream_japan_l10n_pck.get_integrated_hor_seq(:new.nr_prescricao, :new.nr_sequencia, :new.nr_seq_proc_interno, cd_estabelecimento_w, 234) 
	into proc_hor_sequence_w
	from dual;

    if ( proc_hor_sequence_w > 0 ) then
        call_interface_file(930, 'carestream_phys_japan_l10n_pck.patient_order_info (' || :new.nr_prescricao  || ', '  || :new.nr_sequencia  || ', '  || proc_hor_sequence_w  || ', 3, '''   || :new.nm_usuario
                                     || ''' , 0, null, ' || cd_estabelecimento_w || ');', :new.nm_usuario);
    end if;

end if;

<<Final>>
qt_reg_w := 0;
end;
/