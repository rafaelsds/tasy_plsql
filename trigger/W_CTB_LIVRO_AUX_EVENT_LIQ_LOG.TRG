create or replace trigger w_ctb_livro_aux_event_liq_log
before delete or insert or update on w_ctb_livro_aux_event_liq
for each row

declare

nm_usuario_w			usuario.nm_usuario%type;
ds_log_w			ctb_reg_auxiliar_log.ds_log%type;
ie_tipo_reg_auxiliar_w		ctb_livro_auxiliar.ie_tipo_reg_auxiliar_ans%type;
vl_log_w			number(15,2);
ds_alt_reg_w			varchar2(4000);
qt_registros_w			number(10);
			
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	
	select	ie_tipo_reg_auxiliar_ans
	into	ie_tipo_reg_auxiliar_w
	from	ctb_livro_auxiliar
	where	nr_sequencia = nvl(:old.nr_seq_reg_auxiliar,:new.nr_seq_reg_auxiliar);

	if deleting and ie_tipo_reg_auxiliar_w in ('8000','8001','8011','8012','8020','8022') then

		vl_log_w := :old.vl_pagar;

	elsif inserting and ie_tipo_reg_auxiliar_w in ('8000','8001','8011','8012','8020','8022') then
		
		vl_log_w := :new.vl_pagar;

	elsif deleting and ie_tipo_reg_auxiliar_w in ('8002','8003','8005') then

		vl_log_w := :old.vl_glosa;

	elsif inserting and ie_tipo_reg_auxiliar_w in ('8002','8003','8005') then
		
		vl_log_w := :new.vl_glosa;

	elsif deleting and ie_tipo_reg_auxiliar_w in ('8004','8013') then

		vl_log_w := :old.vl_evento;

	elsif inserting and ie_tipo_reg_auxiliar_w in ('8004','8013') then
		
		vl_log_w := :new.vl_evento;
			
	end if;

	nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

	if deleting then

		ds_log_w := wheb_mensagem_pck.get_texto(392981, 'NR_SEQUENCIA=' || :old.nr_sequencia || ';NR_DOCUMENTO=' || :old.nr_documento || ';VALOR=' || vl_log_w || ';CD_CONTA_CONTAB=' || :old.cd_conta_contabil || ';CLASSIFICACAO=' || substr(ctb_obter_classif_conta(:old.cd_conta_contabil, null, :old.dt_referencia),1,255));
		
		wheb_usuario_pck.set_ie_executar_trigger('N');
		
		insert into	ctb_reg_auxiliar_log(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_reg_auxiliar,
						ie_tipo_log,
						ds_log)
					values(ctb_reg_auxiliar_log_seq.nextval,
						sysdate,
						nm_usuario_w,
						sysdate,
						nm_usuario_w,
						:old.nr_seq_reg_auxiliar,
						'E',
						ds_log_w);

	elsif inserting then

		if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then 

			ds_log_w := wheb_mensagem_pck.get_texto(394713, 'NR_SEQUENCIA=' || :new.nr_sequencia || ';NR_DOCUMENTO=' || :new.nr_documento || ';VALOR=' || vl_log_w || ';CD_CONTA_CONTAB=' || :new.cd_conta_contabil || ';CLASSIFICACAO=' || substr(ctb_obter_classif_conta(:new.cd_conta_contabil, null, :new.dt_referencia),1,255));
			
			wheb_usuario_pck.set_ie_executar_trigger('N');
			
			insert into	ctb_reg_auxiliar_log(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_reg_auxiliar,
							ie_tipo_log,
							ds_log)
						values(ctb_reg_auxiliar_log_seq.nextval,
							sysdate,
							nm_usuario_w,
							sysdate,
							nm_usuario_w,
							:new.nr_seq_reg_auxiliar,
							'I',
							ds_log_w);
							
			wheb_usuario_pck.set_ie_executar_trigger('S');				
										
		end if;

	elsif updating then

		if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
		
			ds_alt_reg_w := :new.nr_sequencia;
			
			if (nvl(:old.nm_prestador,0) <> nvl(:new.nm_prestador,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Prestador: ' || :old.nm_prestador;
			end if;
			
			if (nvl(:old.nr_cpf_cnpj,0) <> nvl(:new.nr_cpf_cnpj,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', CPF/CNPJ: ' || :old.nr_cpf_cnpj;
			end if;
			
			if (nvl(:old.nr_titulo,0) <> nvl(:new.nr_titulo,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Nr t�tulo: ' || :old.nr_titulo;
			end if;
			
			if (nvl(:old.nr_evento,0) <> nvl(:new.nr_evento,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Nr evento: ' || :old.nr_evento;
			end if;
			
			if (nvl(:old.ds_item_mat_proc,0) <> nvl(:new.ds_item_mat_proc,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Item/Procedimento: ' || :old.ds_item_mat_proc;
			end if;
			
			if (nvl(:old.ds_tipo_relacao,0) <> nvl(:new.ds_tipo_relacao,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Tipo rela��o: ' || :old.ds_tipo_relacao;
			end if;
			
			if (nvl(:old.ds_ato_cooperado,0) <> nvl(:new.ds_ato_cooperado,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Ato cooperado: ' || :old.ds_ato_cooperado;
			end if;
			
			if (nvl(:old.ds_modalidade_contrat,0) <> nvl(:new.ds_modalidade_contrat,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Modalidade contrata��o: ' || :old.ds_modalidade_contrat;
			end if;
			
			if (nvl(:old.ds_tipo_regulamentacao,0) <> nvl(:new.ds_tipo_regulamentacao,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Regulamenta��o: ' || :old.ds_tipo_regulamentacao;
			end if;
			
			if (nvl(:old.cd_beneficiario,0) <> nvl(:new.cd_beneficiario,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Nr carteira: ' || :old.cd_beneficiario;
			end if;
			
			if ((:old.dt_vencimento <> :new.dt_vencimento) or (:old.dt_vencimento is null and :new.dt_vencimento is not null) or (:old.dt_vencimento is not null and :new.dt_vencimento is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data vencimento: ' || :old.dt_vencimento;
			end if;
			
			if ((:old.dt_conhecimento <> :new.dt_conhecimento) or (:old.dt_conhecimento is null and :new.dt_conhecimento is not null) or (:old.dt_conhecimento is not null and :new.dt_conhecimento is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data conhecimento: ' || :old.dt_conhecimento;
			end if;
			
			if ((:old.dt_contabilizacao <> :new.dt_contabilizacao) or (:old.dt_contabilizacao is null and :new.dt_contabilizacao is not null) or (:old.dt_contabilizacao is not null and :new.dt_contabilizacao is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data contabiliza��o: ' || :old.dt_contabilizacao;
			end if;
			
			if ((:old.dt_referencia <> :new.dt_referencia) or (:old.dt_referencia is null and :new.dt_referencia is not null) or (:old.dt_referencia is not null and :new.dt_referencia is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data refer�ncia: ' || :old.dt_referencia;
			end if;
			
			if ((:old.dt_recuperacao <> :new.dt_recuperacao) or (:old.dt_recuperacao is null and :new.dt_recuperacao is not null) or (:old.dt_recuperacao is not null and :new.dt_recuperacao is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data recupera��o: ' || :old.dt_recuperacao;
			end if;
			
			if (nvl(:old.cd_conta_contabil,0) <> nvl(:new.cd_conta_contabil,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Conta cont�bil: ' || :old.cd_conta_contabil;
			end if;
			
			if (nvl(:old.nr_documento,0) <> nvl(:new.nr_documento,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Nr documento: ' || :old.nr_documento;
			end if;
			
			if (nvl(:old.vl_evento,0) <> nvl(:new.vl_evento,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Valor evento: ' || :old.vl_evento;
			end if;
			
			if (nvl(:old.cd_beneficiario,0) <> nvl(:new.cd_beneficiario,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Nr carteira: ' || :old.cd_beneficiario;
			end if;
			
			if (nvl(:old.nm_usuario_evento,0) <> nvl(:new.nm_usuario_evento,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Usu�rio evento: ' || :old.nm_usuario_evento;
			end if;
			
			if (nvl(:old.ds_classif_pel,0) <> nvl(:new.ds_classif_pel,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Classifica��o PEL: ' || :old.ds_classif_pel;
			end if;
			
			if (nvl(:old.ds_classif_diops,0) <> nvl(:new.ds_classif_diops,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Classifica��o DIOPS: ' || :old.ds_classif_diops;
			end if;
			
			if (nvl(:old.ds_tipo_documento,0) <> nvl(:new.ds_tipo_documento,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Tipo documento: ' || :old.ds_tipo_documento;
			end if;
			
			if (nvl(:old.nr_contrato,0) <> nvl(:new.nr_contrato,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Nr contrato: ' || :old.nr_contrato;
			end if;
			
			if ((:old.dt_emissao <> :new.dt_emissao) or (:old.dt_emissao is null and :new.dt_emissao is not null) or (:old.dt_emissao is not null and :new.dt_emissao is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data emiss�o: ' || :old.dt_emissao;
			end if;
			
			if (nvl(:old.ds_tipo_contratacao,0) <> nvl(:new.ds_tipo_contratacao,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Tipo contrata��o: ' || :old.ds_tipo_contratacao;
			end if;
			
			if (nvl(:old.ds_segmentacao,0) <> nvl(:new.ds_segmentacao,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Segmenta��o: ' || :old.ds_segmentacao;
			end if;
			
			if (nvl(:old.nm_usuario_princ,0) <> nvl(:new.nm_usuario_princ,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Usu�rio principal: ' || :old.nm_usuario_princ;
			end if;
			
			if ((:old.dt_rescisao <> :new.dt_rescisao) or (:old.dt_rescisao is null and :new.dt_rescisao is not null) or (:old.dt_rescisao is not null and :new.dt_rescisao is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data rescis�o: ' || :old.dt_rescisao;
			end if;
			
			if ((:old.dt_ocorrencia <> :new.dt_ocorrencia) or (:old.dt_ocorrencia is null and :new.dt_ocorrencia is not null) or (:old.dt_ocorrencia is not null and :new.dt_ocorrencia is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data ocorr�ncia: ' || :old.dt_ocorrencia;
			end if;
			
			if ((:old.dt_pagamento <> :new.dt_pagamento) or (:old.dt_pagamento is null and :new.dt_pagamento is not null) or (:old.dt_pagamento is not null and :new.dt_pagamento is null)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Data pagamento: ' || :old.dt_pagamento;
			end if;
			
			if (nvl(:old.vl_glosa,0) <> nvl(:new.vl_glosa,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Valor glosa: ' || :old.vl_glosa;
			end if;
			
			if (nvl(:old.vl_pagar,0) <> nvl(:new.vl_pagar,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Valor pagar: ' || :old.vl_pagar;
			end if;
			
			if (nvl(:old.vl_ajuste,0) <> nvl(:new.vl_ajuste,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Valor ajuste: ' || :old.vl_ajuste;
			end if;
			
			if (nvl(:old.vl_coparticipacao,0) <> nvl(:new.vl_coparticipacao,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Valor coparticipa��o: ' || :old.vl_coparticipacao;
			end if;
			
			if (nvl(:old.nr_protocolo_ans,0) <> nvl(:new.nr_protocolo_ans,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Nr protocolo: ' || :old.nr_protocolo_ans;
			end if;
			
			if (nvl(:old.ds_grupo_ans,0) <> nvl(:new.ds_grupo_ans,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Grupo ANS: ' || :old.ds_grupo_ans;
			end if;
			
			if (nvl(:old.ds_tipo_protocolo,0) <> nvl(:new.ds_tipo_protocolo,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Tipo protocolo: ' || :old.ds_tipo_protocolo;
			end if;
			
			if (nvl(:old.ds_tipo_segurado,0) <> nvl(:new.ds_tipo_segurado,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Tipo segurado: ' || :old.ds_tipo_segurado;
			end if;
			
			if (nvl(:old.ds_tipo_vinculo_operadora,0) <> nvl(:new.ds_tipo_vinculo_operadora,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Tipo vinculo: ' || :old.ds_tipo_vinculo_operadora;
			end if;
			
			if (nvl(:old.cd_conta_contrapartida,0) <> nvl(:new.cd_conta_contrapartida,0)) then
				ds_alt_reg_w := ds_alt_reg_w || ', Conta contrapartida: ' || :old.cd_conta_contrapartida;
			end if;

			ds_log_w := substr(wheb_mensagem_pck.get_texto(394796, 'NR_SEQUENCIA=' || ds_alt_reg_w),1,255);

			wheb_usuario_pck.set_ie_executar_trigger('N');

			insert into	ctb_reg_auxiliar_log(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_reg_auxiliar,
							ie_tipo_log,
							ds_log)
						values(ctb_reg_auxiliar_log_seq.nextval,
							sysdate,
							nm_usuario_w,
							sysdate,
							nm_usuario_w,
							:new.nr_seq_reg_auxiliar,
							'A',
							ds_log_w);
							
			wheb_usuario_pck.set_ie_executar_trigger('S');
						
		end if;
						
	end if;

	end;
end if;

end;
/
