create or replace trigger ctb_orcamento_custo_iud
before insert or update or delete on ORCAMENTO_CUSTO
for each row

declare

qt_registro_w	number(10);

begin

if	(inserting) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:new.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
elsif	(updating) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:new.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
elsif	(deleting) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:old.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
end if;

end ctb_orcamento_custo_iud;
/
