create or replace trigger pls_ame_regra_banda_befinsup
before insert or update on pls_ame_regra_banda
for each row

declare
  
    qt_atributos_w NUMBER(10);
    qt_banda_w NUMBER(10);

pragma autonomous_transaction;
begin

select count(1) 
into qt_banda_w
from pls_ame_regra_banda a 
where a.ie_tipo_banda = :new.ie_tipo_banda 
and a.nr_seq_regra = :new.nr_seq_regra
and :old.nr_sequencia <> :new.nr_sequencia;

if(qt_banda_w > 0) then
  if(:new.ie_tipo_banda = 1) then
    wheb_mensagem_pck.exibir_mensagem_abort(1056922);
  elsif (:new.ie_tipo_banda = 4) then
    wheb_mensagem_pck.exibir_mensagem_abort(1056923);
  end if;
end if;

select count(1) 
into qt_atributos_w
from pls_ame_regra_atrib a 
where a.nr_seq_banda = :old.nr_sequencia;

if(nvl(:old.ie_tipo_banda, 0) <> :new.ie_tipo_banda and qt_atributos_w > 0) then
  wheb_mensagem_pck.exibir_mensagem_abort(1056924);
end if;

end pls_ame_regra_banda_befinsup;
/
