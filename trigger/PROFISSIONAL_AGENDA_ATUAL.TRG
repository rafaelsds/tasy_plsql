CREATE OR REPLACE TRIGGER PROFISSIONAL_AGENDA_ATUAL
before insert or update ON PROFISSIONAL_AGENDA
FOR EACH ROW
DECLARE

ie_permite_alt_executada_w	varchar2(1);
ie_status_agenda_w		varchar2(3);
cd_tipo_agenda_w		number(10);		

pragma autonomous_transaction;

BEGIN
begin

obter_param_usuario(871, 758, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alt_executada_w);


select  max(a.ie_status_agenda),
	max(b.cd_tipo_agenda)
into	ie_status_agenda_w,
	cd_tipo_agenda_w
from    agenda_paciente a,
	agenda b
where   a.cd_agenda = b.cd_agenda
and	nr_sequencia = :new.nr_seq_agenda;

exception
	when others then
      	null;
end;

if	(ie_permite_alt_executada_w = 'N') and
	(cd_tipo_agenda_w = 1) and
	(ie_status_agenda_w = 'E') then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(236679);
	
end if;
END;
/