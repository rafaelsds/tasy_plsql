CREATE OR replace TRIGGER prescr_proc_mat_item_upd
  BEFORE UPDATE ON prescr_proc_mat_item
  FOR EACH ROW
DECLARE
    nr_prescricao_w           prescr_medica.nr_prescricao%TYPE;
    cd_estabelecimento_w      estabelecimento.cd_estabelecimento%TYPE;
    nr_seq_exame_w            exame_laboratorio.nr_seq_exame%TYPE;
    nr_atendimento_w          atendimento_paciente.nr_atendimento%TYPE;
    ie_status_envio_w         lab_parametro.ie_status_envio%TYPE;
    nr_ordem_amostra_w        exame_laboratorio.nr_ordem_amostra%TYPE;
    ie_formato_resultado_w    VARCHAR2(10);
    qt_prescr_proc_material_w NUMBER(10);
    ds_sep_bv_w               VARCHAR2(100);
    ds_param_integ_hl7_w      VARCHAR2(2000);
    ie_status_envio_abbot_w   lab_parametro.ie_status_envio_abbot%TYPE;
    qt_reg_w    			  NUMBER(1);
BEGIN

    IF (wheb_usuario_pck.get_ie_executar_trigger  = 'N') THEN
       GOTO Final;
    END IF;
    
    IF ( wheb_usuario_pck.Is_evento_ativo(84) = 'S' ) THEN
    
      SELECT Max(nr_prescricao)
      INTO   nr_prescricao_w
      FROM   prescr_proc_material
      WHERE  nr_sequencia = :NEW.nr_seq_prescr_proc_mat;

      SELECT Max(nr_atendimento),
             Max(cd_estabelecimento)
      INTO   nr_atendimento_w, cd_estabelecimento_w
      FROM   prescr_medica a
      WHERE  a.nr_prescricao = nr_prescricao_w;

      SELECT Max(ie_status_envio)
      INTO   ie_status_envio_w
      FROM   lab_parametro
      WHERE  cd_estabelecimento = cd_estabelecimento_w;

      IF ( ( :NEW.ie_status = ie_status_envio_w )
           AND ( :OLD.ie_status < ie_status_envio_w ) ) THEN
        /*SELECT  MAX(a.nr_seq_exame)
        INTO  nr_seq_exame_w
        FROM  prescR_procedimento a
        WHERE  a.nr_prescricao = nr_prescricao_w
        AND  a.nr_sequencia = :NEW.nr_seq_prescr;*/
        nr_seq_exame_w := Lab_obter_exame_pragma(nr_prescricao_w,
                          :NEW.nr_seq_prescr);

        SELECT Max(ie_formato_resultado)
        INTO   ie_formato_resultado_w
        FROM   exame_laboratorio
        WHERE  nr_seq_exame = nr_seq_exame_w;

        SELECT Count(*)
        INTO   nr_ordem_amostra_w
        FROM   exame_laboratorio
        WHERE  nr_seq_superior = nr_seq_exame_w
               AND nr_ordem_amostra IS NOT NULL;

        ds_sep_bv_w := obter_separador_bv;

        IF ( ie_formato_resultado_w NOT IN ( 'SM', 'SDM' ) )
           AND ( nr_ordem_amostra_w > 0 ) THEN
          ds_param_integ_hl7_w := 'nr_atendimento='
                                  || nr_atendimento_w
                                  || ds_sep_bv_w
                                  || 'nr_prescricao='
                                  || nr_prescricao_w
                                  || ds_sep_bv_w
                                  || 'nr_seq_presc='
                                  || :NEW.nr_seq_prescr
                                  || ds_sep_bv_w
                                  || 'nr_seq_prescr_proc_mat='
                                  ||:new.nr_seq_prescr_proc_mat
                                  || ds_sep_bv_w;

          Gravar_agend_integracao(84, ds_param_integ_hl7_w);
        END IF;
      END IF;
    END IF;

    IF ( wheb_usuario_pck.Is_evento_ativo(705) = 'S' ) THEN
      SELECT Max(nr_prescricao)
      INTO   nr_prescricao_w
      FROM   prescr_proc_material
      WHERE  nr_sequencia = :NEW.nr_seq_prescr_proc_mat;

      SELECT Max(nr_atendimento),
             Max(cd_estabelecimento)
      INTO   nr_atendimento_w, cd_estabelecimento_w
      FROM   prescr_medica a
      WHERE  a.nr_prescricao = nr_prescricao_w;

      SELECT Max(ie_status_envio),
             ie_status_envio_abbot
      INTO   ie_status_envio_w, ie_status_envio_abbot_w
      FROM   lab_parametro
      WHERE  cd_estabelecimento = cd_estabelecimento_w
      GROUP  BY ie_status_envio_abbot;

      IF ( ie_status_envio_abbot_w = 'S' ) THEN
        IF ( :new.ie_status = ie_status_envio_w )
           AND ( :old.ie_status < ie_status_envio_w ) THEN
          Abbott_enviar_prescr(:new.nr_prescricao, :new.nr_seq_prescr,
          :new.nr_seq_prescr_proc_mat);
        END IF;
      ELSE
        IF ( :new.ie_status = 20 )
           AND ( :old.ie_status < 20 ) THEN
          Abbott_enviar_prescr(:new.nr_prescricao, :new.nr_seq_prescr,
          :new.nr_seq_prescr_proc_mat);
        ELSIF ( :new.ie_status = 10 )
              AND ( :old.ie_status > :new.ie_status ) THEN
          Abbott_enviar_prescr(:new.nr_prescricao, :new.nr_seq_prescr,
          :new.nr_seq_prescr_proc_mat, 'RR');
        END IF;
      END IF;
    END IF;
    
<<Final>>
qt_reg_w :=0;
END;
/
