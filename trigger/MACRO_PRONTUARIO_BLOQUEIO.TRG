CREATE OR REPLACE TRIGGER macro_prontuario_bloqueio
BEFORE INSERT OR UPDATE ON macro_prontuario FOR EACH ROW
DECLARE

BEGIN
  IF (TRIM(UPPER(:NEW.nm_macro)) = '@TESTE') THEN
    wheb_mensagem_pck.exibir_mensagem_abort(798803);
  END IF;
END;
/
