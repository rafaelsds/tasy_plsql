CREATE OR REPLACE TRIGGER INSPECAO_REGISTRO_DELETE
BEFORE DELETE ON INSPECAO_REGISTRO
FOR EACH ROW
DECLARE

qt_existe_w		number(10);

BEGIN

if	(:old.ie_origem = 'NF') then
	
	update	nota_fiscal_item
	set	nr_seq_inspecao = null
	where	nr_sequencia in (
		select	nr_sequencia
		from	nota_fiscal
		where	nr_seq_reg_inspecao = :old.nr_sequencia);
	
	update	nota_fiscal
	set	nr_seq_reg_inspecao = null
	where	nr_seq_reg_inspecao = :old.nr_sequencia;
	
end if;

END;
/
