create or replace trigger INSPECAO_REC_ASPECTO_ATUAL
before insert or update on INSPECAO_REC_ASPECTO
for each row
declare

begin

if	(:new.cd_grupo_material is not null) then
	begin
	if	(:new.cd_subgrupo_material is not null) or
		(:new.cd_classe_material is not null) or
		(:new.cd_material is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266075);
		--'Favor informar somente uma informa��o (S� o Grupo, ou s� o Subgrupo, ou s� a Classe, ou s� o Material).');
	end if;
	end;
elsif	(:new.cd_subgrupo_material is not null) then
	begin
	if	(:new.cd_grupo_material is not null) or
		(:new.cd_classe_material is not null) or
		(:new.cd_material is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266075);
		--'Favor informar somente uma informa��o (S� o Grupo, ou s� o Subgrupo, ou s� a Classe, ou s� o Material).');
	end if;
	end;
elsif	(:new.cd_classe_material is not null) then
	begin
	if	(:new.cd_grupo_material is not null) or
		(:new.cd_subgrupo_material is not null) or
		(:new.cd_material is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266075);
		--'Favor informar somente uma informa��o (S� o Grupo, ou s� o Subgrupo, ou s� a Classe, ou s� o Material).');
	end if;	
	end;
elsif	(:new.cd_material is not null) then
	begin
	if	(:new.cd_grupo_material is not null) or
		(:new.cd_subgrupo_material is not null) or
		(:new.cd_classe_material is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266075);
		--'Favor informar somente uma informa��o (S� o Grupo, ou s� o Subgrupo, ou s� a Classe, ou s� o Material).');
	end if;
	end;
end if;	

end;
/