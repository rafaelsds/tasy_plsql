create or replace TRIGGER Procedimento_Repasse_Atual
BEFORE UPDATE ON Procedimento_Repasse
FOR EACH ROW

declare

cont_w				number(1, 0);
nr_seq_repasse_w		Number(10,0);
qt_reg_w			number(1);
ie_vincular_rep_proc_pos_w	varchar2(1);
ie_vinc_repasse_ret_w		varchar2(1);
ie_conta_repasse_w		parametro_faturamento.ie_conta_repasse%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

begin

:new.ds_log := substr(:old.ds_log || chr(13) || chr(10) || ' Atualizacao no repasse: ' || substr(dbms_utility.format_call_stack,1,4000),1,4000);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

cd_estabelecimento_w	:= obter_estabelecimento_ativo;

select	max(IE_CONTA_REPASSE)
into	ie_conta_repasse_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

CONSISTIR_VENC_REPASSE(
	:old.nr_repasse_terceiro,
	:new.nr_repasse_terceiro,
	:old.vl_repasse,
	:new.vl_repasse,
	:old.vl_liberado,
	:new.vl_liberado);


-- se esta desvinculando
if	(:new.nr_repasse_terceiro is null) and
	(:old.nr_repasse_terceiro is not null) then
	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= :old.nr_repasse_terceiro
	and	ie_status		= 'F';

	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(220250,'NR_REPASSE_P='||:old.nr_repasse_terceiro);
		/*'O repasse deste procedimento ja esta fechado!' || chr(13) ||
						'Repasse: ' || :old.nr_repasse_terceiro);*/
	end if;
end if;

-- se esta vinculando
if	(:new.nr_repasse_terceiro is not null) and
	(:old.nr_repasse_terceiro is null) then
	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= :new.nr_repasse_terceiro
	and	ie_status		= 'F';

	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(220250,'NR_REPASSE_P='||:old.nr_repasse_terceiro);
		/*'O repasse deste procedimento ja esta fechado!' || chr(13) ||
						'Repasse: ' || :old.nr_repasse_terceiro);*/
	end if;
end if;

if	(:new.ie_status <> :old.ie_status) and
	(((ie_conta_repasse_w = 'S') and
	((:new.ie_status = 'S') or
	(:new.ie_status = 'L'))) or
	((ie_conta_repasse_w = 'R') and
	((:new.ie_status = 'R') or
	(:new.ie_status = 'S') or
	(:new.ie_status = 'L')))) then
	begin
	if	(:new.dt_liberacao is null) then
		:new.dt_liberacao	:= sysdate;
	end if;

	if	(:new.nr_repasse_terceiro is null) and
		(:old.nr_repasse_terceiro is null) then

		obter_repasse_terceiro(
			:new.dt_liberacao,
			:new.nr_seq_terceiro,
			:new.nm_usuario,
			:new.nr_seq_procedimento,
			'P',
			nr_seq_repasse_w,
			null,
			null);

		select	nvl(max(b.ie_vincular_rep_proc_pos),'S'),
			nvl(max(b.ie_vinc_repasse_ret),'S')
		into	ie_vincular_rep_proc_pos_w,
			ie_vinc_repasse_ret_w
		from	parametro_faturamento b,
			terceiro a
		where	a.cd_estabelecimento	= b.cd_estabelecimento
		and	a.nr_sequencia		= :new.nr_seq_terceiro;

		if	(nvl(nr_seq_repasse_w,0) > 0) then
			if	((ie_vincular_rep_proc_pos_w = 'N') and
				 (trunc(:new.dt_liberacao, 'dd') > trunc(sysdate, 'dd'))) or
				(ie_vinc_repasse_ret_w = 'N') then
				:new.nr_repasse_terceiro	:= null;
			else
				:new.nr_repasse_terceiro	:= nr_seq_repasse_w;
			end if;
		end if;

	end if;
	end;
end if;

if	(:new.ie_status <> :old.ie_status) and
	((:new.ie_status = 'R') or
	 (:new.ie_status = 'S') or
	 (:new.ie_status = 'L')) then

	CONSISTIR_ITEM_REPASSE_TIT(:new.nr_seq_procedimento,null);

end if;

if  (:new.ie_status <> :old.ie_status) then
	:new.ds_status := substr(obter_valor_dominio(1129, :new.ie_status),1,50);
end if;

if  (:new.nr_seq_trans_fin <> :old.nr_seq_trans_fin) then
    :new.ds_transacao := substr(obter_desc_trans_financ(:new.nr_seq_trans_fin),1,100);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/
