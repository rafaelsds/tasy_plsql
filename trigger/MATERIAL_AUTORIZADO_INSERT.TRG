create or replace trigger material_autorizado_insert
before insert or update on material_autorizado
for each row
declare

ie_tipo_atendimento_w	number(10);
cd_mat_convenio_w	varchar2(20)	:= '';
cd_grupo_w		varchar2(20)	:= '';
cd_cgc_w		varchar2(20);
cd_cgc_convert_w		varchar2(20);
nr_seq_conversao_w	number(10);
nr_atendimento_w		number(10);
cd_estabelecimento_w	number(10);
cd_convenio_w		number(10);
ds_mat_convenio_w	varchar2(255) := '';
ie_gerar_historico_w	varchar2(15) := 'N';
ie_carater_inter_sus_w	varchar2(2);
cd_categoria_w		varchar2(10);
tx_conversao_w		number(15,4) := 0;
cd_setor_origem_w	number(5,0);
cd_unidade_convenio_w	varchar2(5);
nr_seq_tuss_mat_item_w	number(10);
ie_clinica_w		atendimento_paciente.ie_clinica%type;
cd_material_tuss_w	material_tuss.cd_material_tuss%type;
ds_material_tuss_w	tuss_material_item.ds_material%type;
ds_historico_w		varchar(2000);

ie_aplicar_tx_conversao_w	varchar2(15);
begin
obter_param_usuario(3004,139, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.Get_cd_estabelecimento, ie_gerar_historico_w);
obter_param_usuario(3004,163, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.Get_cd_estabelecimento, ie_aplicar_tx_conversao_w);

/* os 51884 - gravar proc convenio */
select	a.cd_convenio,
	a.nr_atendimento,
	Nvl(a.cd_estabelecimento,to_number(obter_dados_autor_convenio(a.nr_sequencia, 'E'))),
	b.cd_cgc,
	to_number(substr(obter_tipo_atend_autor(a.nr_atendimento,a.nr_seq_agenda,a.nr_seq_agenda_consulta,nr_seq_age_integ,a.nr_seq_paciente_setor,'C'),1,1)),
	substr(Obter_dados_autor_convenio(a.nr_sequencia,'CC'),1,10),
	a.cd_setor_origem
into	cd_convenio_w,
	nr_atendimento_w,
	cd_estabelecimento_w,
	cd_cgc_w,
	ie_tipo_atendimento_w,
	cd_categoria_w,
	cd_setor_origem_w
from	convenio b,
	autorizacao_convenio a
where	a.nr_sequencia	= :new.nr_sequencia_autor
and	a.cd_convenio	= b.cd_convenio;

if	(nr_atendimento_w is not null) then
	select	ie_tipo_atendimento,
		ie_carater_inter_sus,
		ie_clinica
	into	ie_tipo_atendimento_w,
		ie_carater_inter_sus_w,
		ie_clinica_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;

end if;

if	(:new.cd_material is not null) then

	converte_material_convenio
		(cd_convenio_w,
		:new.cd_material,
		null,
		null,
		nvl(:new.cd_cgc_fabricante, cd_cgc_w),
		cd_estabelecimento_w,
		sysdate,
		null,
		cd_mat_convenio_w,
		cd_grupo_w,
		nr_seq_conversao_w,
		ie_tipo_atendimento_w,
		null,
		0,
		null,
		ie_carater_inter_sus_w,
		null,
		null,
		ie_clinica_w,
		:new.ie_tipo_prec_especial);

	:new.vl_ultima_compra		:= to_number(obter_dados_ultima_compra(cd_estabelecimento_w,:new.cd_material,'VU'));
	:new.cd_cgc_ultima_compra		:= obter_dados_ultima_compra(cd_estabelecimento_w,:new.cd_material,'PJ');
	:new.dt_ultima_compra		:= Obter_Data_Ultima_Compra(:new.cd_material,cd_estabelecimento_w); --to_date(obter_dados_ultima_compra(cd_estabelecimento_w,:new.cd_material,'DT'),'dd/mm/yyyy');
	
	DEFINE_MATERIAL_TUSS_AUTOR
		(cd_estabelecimento_w,
		:new.cd_material,
		cd_convenio_w,
		cd_categoria_w,
		ie_tipo_atendimento_w,
		:new.ie_origem_preco,
		sysdate,
		cd_material_tuss_w,
		nr_seq_tuss_mat_item_w,
		ds_material_tuss_w,
		:new.cd_cgc_fabricante,
		:new.nr_seq_marca,
		nr_atendimento_w);
		
		select decode(cd_material_tuss_w,'0',null, cd_material_tuss_w) 
		into cd_material_tuss_w
		from dual;

	:new.nr_seq_tuss_mat_item		:= nr_seq_tuss_mat_item_w;	
	:new.cd_material_tuss		:= cd_material_tuss_w;
	:new.ds_material_tuss		:= ds_material_tuss_w;

end if;

if	(nr_seq_conversao_w is not null) then

	begin
	select	nvl(a.tx_conversao_qtde,0),
		upper(a.cd_unidade_convenio),
		substr(a.ds_material_convenio,1,255)
	into	tx_conversao_w,
		cd_unidade_convenio_w,
		ds_mat_convenio_w
	from	conversao_material_convenio a
	where	a.nr_sequencia = nr_seq_conversao_w;
	exception
	when others then
		tx_conversao_w := 0;
		cd_unidade_convenio_w := null;
		ds_mat_convenio_w     := null;
	end;
	
	if 	(nvl(ie_aplicar_tx_conversao_w,'N') in ('S','A')) and
		(nvl(tx_conversao_w,0) > 0) and
		(:new.qt_solicitada > 0) and
        (Inserting or :old.qt_solicitada <> :new.qt_solicitada) then
		:new.qt_solicitada := dividir(:new.qt_solicitada,tx_conversao_w);
	end if;
	
	if	(cd_mat_convenio_w is not null) then
		:new.cd_material_convenio	:= cd_mat_convenio_w;
	end if;
		

	if	(ds_mat_convenio_w is not null) then
		:new.ds_mat_convenio		:= ds_mat_convenio_w;
	end if;


end if;

if	(nvl(ie_gerar_historico_w, 'N') = 'S') and
	(Inserting) then
	
	ds_historico_w := substr(:new.cd_material || ' - ' || substr(obter_desc_material(:new.cd_material),1,100) || ' - ' || :new.cd_cgc_fabricante || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(311725) || :new.qt_solicitada,1,2000);
		
	gravar_autor_conv_log_alter(:new.nr_sequencia_autor, substr(wheb_mensagem_pck.get_texto(311727),1,100), substr(ds_historico_w,1,2000),:new.nm_usuario);

	insert into autorizacao_convenio_hist
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_sequencia_autor)
	values	(
		autorizacao_convenio_hist_seq.nextval,
		sysdate,
		:new.nm_usuario,
		:new.nr_atendimento,
		substr(WHEB_MENSAGEM_PCK.get_texto(311727) || chr(13) || chr(10) || --Incluido material na autorizacao 
		:new.cd_material || ' - ' || substr(obter_desc_material(:new.cd_material),1,100) || chr(13) || chr(10) ||
		WHEB_MENSAGEM_PCK.get_texto(311725) || :new.qt_solicitada,1,4000), --Qtde solicitada: 
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia_autor);

end if;

end;
/
