create or replace trigger hd_dial_dialisador_befup_sign
before update on hd_dialise_dialisador
for each row

declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_dialise_dialisador = :new.nr_sequencia
and 	ie_opcao = 'MD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_montagem 		<> :new.dt_montagem
		or :old.cd_pf_montagem 		<> :new.cd_pf_montagem
		or :old.nr_seq_dialisador 	<> :new.nr_seq_dialisador
		or :old.nr_seq_maquina 		<> :new.nr_seq_maquina
		or :old.nr_seq_dialise 		<> :new.nr_seq_dialise
		or :old.nr_seq_ponto_acesso 	<> :new.nr_seq_ponto_acesso
		or :old.ie_troca_emergencia 	<> :new.ie_troca_emergencia
		or :old.nr_seq_unid_dialise 	<> :new.nr_seq_unid_dialise
		or :old.qt_reuso_atual 		<> :new.qt_reuso_atual
		or :old.nr_seq_osmose 		<> :new.nr_seq_osmose
		or (:old.dt_montagem		is null		and	:new.dt_montagem		is not null)
		or (:old.cd_pf_montagem		is null		and	:new.cd_pf_montagem		is not null)
		or (:old.nr_seq_dialisador	is null		and	:new.nr_seq_dialisador		is not null)
		or (:old.nr_seq_maquina		is null		and	:new.nr_seq_maquina		is not null)
		or (:old.nr_seq_dialise		is null		and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_ponto_acesso	is null		and	:new.nr_seq_ponto_acesso	is not null)
		or (:old.ie_troca_emergencia	is null		and	:new.ie_troca_emergencia	is not null)
		or (:old.nr_seq_unid_dialise	is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.qt_reuso_atual		is null		and	:new.qt_reuso_atual		is not null)
		or (:old.nr_seq_osmose		is null		and	:new.nr_seq_osmose		is not null)
		or (:old.dt_montagem		is not null	and	:new.dt_montagem		is null)
		or (:old.cd_pf_montagem		is not null	and	:new.cd_pf_montagem		is null)
		or (:old.nr_seq_dialisador	is not null	and	:new.nr_seq_dialisador		is null)
		or (:old.nr_seq_maquina		is not null	and	:new.nr_seq_maquina		is null)
		or (:old.nr_seq_dialise		is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_ponto_acesso	is not null	and	:new.nr_seq_ponto_acesso	is null)
		or (:old.ie_troca_emergencia	is not null	and	:new.ie_troca_emergencia	is null)
		or (:old.nr_seq_unid_dialise	is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.qt_reuso_atual		is not null	and	:new.qt_reuso_atual		is null)
		or (:old.nr_seq_osmose		is not null	and	:new.nr_seq_osmose		is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'C'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.ds_motivo_subst		<> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_dialise_dialisador = :new.nr_sequencia
and 	ie_opcao = 'TI'
and	dt_liberacao is not null
and	:old.dt_retirada is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.ds_motivo_subst		<> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'SD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada          <> :new.cd_pf_retirada
		or :old.ds_motivo_subst         <> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_dialise_dialisador = :new.nr_sequencia
and 	ie_opcao = 'SD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_montagem		<> :new.dt_montagem
		or :old.cd_pf_montagem          <> :new.cd_pf_montagem
		or :old.nr_seq_dialisador       <> :new.nr_seq_dialisador
		or :old.nr_seq_maquina		<> :new.nr_seq_maquina
		or :old.nr_seq_dialise          <> :new.nr_seq_dialise
		or :old.nr_seq_ponto_acesso     <> :new.nr_seq_ponto_acesso
		or :old.ie_troca_emergencia     <> :new.ie_troca_emergencia
		or :old.nr_seq_unid_dialise     <> :new.nr_seq_unid_dialise
		or :old.qt_reuso_atual          <> :new.qt_reuso_atual
		or :old.nr_seq_osmose           <> :new.nr_seq_osmose
		or (:old.dt_montagem		is null		and	:new.dt_montagem		is not null)
		or (:old.cd_pf_montagem		is null		and	:new.cd_pf_montagem		is not null)
		or (:old.nr_seq_dialisador	is null		and	:new.nr_seq_dialisador		is not null)		
		or (:old.nr_seq_maquina		is null		and	:new.nr_seq_maquina		is not null)
		or (:old.nr_seq_dialise		is null		and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_ponto_acesso	is null		and	:new.nr_seq_ponto_acesso	is not null)
		or (:old.ie_troca_emergencia	is null		and	:new.ie_troca_emergencia	is not null)
		or (:old.nr_seq_unid_dialise	is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.qt_reuso_atual		is null		and	:new.qt_reuso_atual		is not null)
		or (:old.nr_seq_osmose		is null		and	:new.nr_seq_osmose		is not null)
		or (:old.dt_montagem		is not null	and	:new.dt_montagem		is null)
		or (:old.cd_pf_montagem		is not null	and	:new.cd_pf_montagem		is null)
		or (:old.nr_seq_dialisador	is not null	and	:new.nr_seq_dialisador		is null)
		or (:old.nr_seq_maquina		is not null	and	:new.nr_seq_maquina		is null)		
		or (:old.nr_seq_dialise		is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_ponto_acesso	is not null	and	:new.nr_seq_ponto_acesso	is null)
		or (:old.ie_troca_emergencia	is not null	and	:new.ie_troca_emergencia	is null)
		or (:old.nr_seq_unid_dialise	is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.qt_reuso_atual		is not null	and	:new.qt_reuso_atual		is null)
		or (:old.nr_seq_osmose		is not null	and	:new.nr_seq_osmose		is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'RD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.ds_motivo_subst		<> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'SM'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.nr_seq_motivo_sub	<> :new.nr_seq_motivo_sub
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.nr_seq_motivo_sub	is null		and	:new.nr_seq_motivo_sub	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.nr_seq_motivo_sub	is not null	and	:new.nr_seq_motivo_sub	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_dialise_dialisador = :new.nr_sequencia
and 	ie_opcao = 'SM'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_montagem		<> :new.dt_montagem
		or :old.cd_pf_montagem          <> :new.cd_pf_montagem
		or :old.nr_seq_dialisador       <> :new.nr_seq_dialisador
		or :old.nr_seq_maquina		<> :new.nr_seq_maquina
		or :old.nr_seq_dialise          <> :new.nr_seq_dialise
		or :old.nr_seq_ponto_acesso     <> :new.nr_seq_ponto_acesso
		or :old.ie_troca_emergencia     <> :new.ie_troca_emergencia
		or :old.nr_seq_unid_dialise     <> :new.nr_seq_unid_dialise
		or :old.qt_reuso_atual          <> :new.qt_reuso_atual
		or :old.nr_seq_osmose           <> :new.nr_seq_osmose
		or (:old.dt_montagem		is null		and	:new.dt_montagem		is not null)
		or (:old.cd_pf_montagem		is null		and	:new.cd_pf_montagem		is not null)
		or (:old.nr_seq_dialisador	is null		and	:new.nr_seq_dialisador		is not null)		
		or (:old.nr_seq_maquina		is null		and	:new.nr_seq_maquina		is not null)
		or (:old.nr_seq_dialise		is null		and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_ponto_acesso	is null		and	:new.nr_seq_ponto_acesso	is not null)
		or (:old.ie_troca_emergencia	is null		and	:new.ie_troca_emergencia	is not null)
		or (:old.nr_seq_unid_dialise	is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.qt_reuso_atual		is null		and	:new.qt_reuso_atual		is not null)
		or (:old.nr_seq_osmose		is null		and	:new.nr_seq_osmose		is not null)
		or (:old.dt_montagem		is not null	and	:new.dt_montagem		is null)
		or (:old.cd_pf_montagem		is not null	and	:new.cd_pf_montagem		is null)
		or (:old.nr_seq_dialisador	is not null	and	:new.nr_seq_dialisador		is null)
		or (:old.nr_seq_maquina		is not null	and	:new.nr_seq_maquina		is null)		
		or (:old.nr_seq_dialise		is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_ponto_acesso	is not null	and	:new.nr_seq_ponto_acesso	is null)
		or (:old.ie_troca_emergencia	is not null	and	:new.ie_troca_emergencia	is null)
		or (:old.nr_seq_unid_dialise	is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.qt_reuso_atual		is not null	and	:new.qt_reuso_atual		is null)
		or (:old.nr_seq_osmose		is not null	and	:new.nr_seq_osmose		is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'FD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.ds_motivo_subst		<> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/