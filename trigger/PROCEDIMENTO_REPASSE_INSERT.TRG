create or replace trigger procedimento_repasse_insert
before insert on procedimento_repasse
for each row

declare

qt_reg_w			number(1);

begin

:new.ds_log := substr('Processo de geracao do Repasse: ' || dbms_utility.format_call_stack,1,4000);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_atualizacao_nrec is null) then
	begin
	:new.dt_atualizacao_nrec := sysdate;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/
