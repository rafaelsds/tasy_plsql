create or replace trigger agenda_paciente_afterupdate
after update on agenda_paciente
for each row
declare
nr_telefone_w		varchar2(40);
ds_remetente_sms_w	varchar2(255);
ds_mensagem_sms_w	varchar2(2000);
ie_consiste_destinatario_sms	varchar2(1);
ds_param_integration_w	varchar2(255);

begin
begin
ie_consiste_destinatario_sms := OBTER_VALOR_PARAM_USUARIO(0,214,0,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);


if	(:new.ie_status_agenda = 'N') and
	(:new.ie_status_agenda <> :old.ie_status_agenda) and
	(obter_se_banco_sangue(:new.nr_sequencia) = 'S') then
	
	if (ie_consiste_destinatario_sms  = 'N') then
		select	distinct
		max(b.nr_ddi_celular)||max(b.nr_telefone_celular)
		into	nr_telefone_w
		from	pessoa_fisica b	
		where	b.cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	else
		select	distinct
		max(b.nr_ddd_celular)|| max(b.nr_telefone_celular)
		into	nr_telefone_w
		from	pessoa_fisica b	
		where	b.cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	end if;
	
	select	max(substr(nvl(ds_remetente_sms, obter_nome_estabelecimento(wheb_usuario_pck.get_cd_estabelecimento)),1,255))
	into	ds_remetente_sms_w
	from	parametro_agenda	
	where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;
	
	ds_mensagem_sms_w :=	SUBSTR( ' Sr(a). ' || substr(obter_nome_pf(:new.cd_pessoa_fisica),1,255) || ' , favor comparecer ao Hospital Santa Paula para coleta de sangue para verificacao da tipagem sanguinea.',1,1900);
	begin
	enviar_sms_agenda(ds_remetente_sms_w, nr_telefone_w, ds_mensagem_sms_w, :new.cd_agenda, :new.nr_sequencia, 'Tasy');
	exception
	when others then
	null;
	end;
	
	 
end if;	

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if (:new.ie_status_agenda <> :old.ie_status_agenda) and (:new.ie_status_agenda != 'N') then
		ds_param_integration_w := '{"recordId" : "' || :new.nr_sequencia|| '"' || '}';
		execute_bifrost_integration(256,ds_param_integration_w);
	end if;
end if;

exception
when others then
	null;
end;

end;
/