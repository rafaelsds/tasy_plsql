CREATE OR REPLACE TRIGGER PACIENTE_SETOR_LIB_UPDATE
BEFORE UPDATE ON paciente_setor_lib
FOR EACH ROW
DECLARE
qt_reg_w	number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.dt_cancelamento is not null) and
	(:old.dt_cancelamento is null) then
	Atualizar_dependente_protocolo(:new.nr_seq_paciente,:new.nr_ciclo);
end if;
<<Final>>
qt_reg_w	:= 0;
END;
/
