create or replace trigger disp_padrao_estoque_delete
before delete on padrao_estoque_local
for each row

declare
ie_tipo_local_w	varchar2(5);

begin

select	nvl(max(ie_tipo_local),'0')
into	ie_tipo_local_w
from	local_estoque
where	cd_local_estoque = :old.cd_local_estoque;

if	(ie_tipo_local_w = '11') then
	insert into dispensario_padrao_local(
		nr_sequencia,
		cd_estabelecimento,
		cd_local_estoque,
		cd_material,
		minimo,
		maximo,
		dt_registro,
		dt_atualizacao,
		dt_exclusao,
		ie_lido)
	values(DISPENSARIO_PADRAO_LOCAL_seq.nextval,
		:old.cd_estabelecimento,
		:old.cd_local_estoque,
		:old.cd_material,
		:old.qt_estoque_minimo,
		:old.qt_estoque_maximo,
		sysdate,
		sysdate,
		sysdate,
		'');
end if;

end disp_padrao_estoque_delete;
/