create or replace trigger pls_fatura_proc_ins
before insert on pls_fatura_proc
for each row

declare

qt_registro_pos_w	number(10);
qt_registro_w		number(10);

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	select	count(1)
	into	qt_registro_pos_w
	from	pls_conta_pos_estabelecido a
	where	nr_sequencia = :new.nr_seq_conta_pos_estab
	and	nr_seq_lote_fat is null
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido x
				where	x.nr_seq_conta = a.nr_seq_conta
				and	x.nr_sequencia <> a.nr_sequencia
				and	nr_seq_lote_fat is not null);

	select	count(1)
	into	qt_registro_w
	from	pls_fatura_conta c,
		pls_fatura_evento z,
		pls_fatura s
	where	s.nr_sequencia	= z.nr_seq_fatura
	and	z.nr_sequencia	= c.nr_seq_fatura_evento
	and	c.nr_sequencia	= :new.nr_seq_fatura_conta
	and	nvl(s.ie_cancelamento,'X') in ('C','E');

	if	(qt_registro_pos_w > 0) and
		(qt_registro_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(264165,'NR_SEQ_CONTA_PROC=' || :new.nr_seq_conta_proc);
	end if;
end if;

end;
/