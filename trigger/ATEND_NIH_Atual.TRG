create or replace trigger atend_nih_atual
before insert or update on atend_nih
for each row

declare
  sql_w        varchar2(200);
  qt_escore_w  number(15, 0);
  cd_paciente_w           number(15);

BEGIN

  if (:new.cd_paciente is null) then
    select max(cd_pessoa_fisica)
    into cd_paciente_w
    from atendimento_paciente
    where nr_atendimento = :new.nr_atendimento;

    :new.cd_paciente := cd_paciente_w;

  end if;

  /** Medical Device **/
  if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
    sql_w := 'call calcula_nih_md(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15) into :qt_escore_w';
	  begin
		  execute immediate sql_w using  in :new.ie_nivel_consciencia,
										 in :new.ie_orientacao,
										 in :new.ie_comando,
										 in :new.ie_motricidade_ocular,
										 in :new.ie_campo_visual,
										 in :new.ie_paresia_facial,
										 in :new.ie_motor_ms,
										 in :new.ie_motor_ms_d,
										 in :new.ie_motor_mi,
										 in :new.ie_motor_mi_d,
										 in :new.ie_ataxia,
										 in :new.ie_sensibilidade,
										 in :new.ie_linguagem,
										 in :new.ie_disartria,
										 in :new.ie_negligencia,
										out qt_escore_w;
	  exception
		when others then
		  qt_escore_w := null;
	  end;
									
	  :new.qt_escore := qt_escore_w; 
  end if;
end;
/