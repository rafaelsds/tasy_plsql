create or replace trigger update_param_estab
before update or insert on FUNCAO_PARAM_ESTAB
for each row

declare


pragma autonomous_transaction;


begin

IF INSTR(:NEW.VL_PARAMETRO, '#@') > 0 THEN
  wheb_mensagem_pck.exibir_mensagem_abort(1040016);
END IF;

atualizar_log_alt_parametros(
			:new.nm_usuario,
			:new.cd_estabelecimento,
			:new.cd_estabelecimento,
			:old.cd_estabelecimento,
			null,
			null,
			null,
			null,
			:new.nr_seq_param,
			:new.vl_parametro,
			:old.vl_parametro,
			'E',
			:new.cd_funcao);
commit;			

end;
/			
