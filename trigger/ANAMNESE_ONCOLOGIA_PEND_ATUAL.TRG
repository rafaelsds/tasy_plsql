create or replace
trigger anamnese_oncologia_pend_atual
after insert or update or delete on anamnese_oncologia
for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_anamnese_onc_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

begin

    if	(inserting) or (updating) then
        
        select	max(ie_lib_anamnese_onc)
        into	ie_lib_anamnese_onc_w
        from	parametro_medico
        where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
        
        if	(nvl(ie_lib_anamnese_onc_w,'N') = 'S') then
        
            if	(:new.dt_liberacao is null) then
                ie_tipo_w := 'AON';
            elsif	(:old.dt_liberacao is null) and (:new.dt_liberacao is not null) then
                ie_tipo_w := 'XAON';
            end if;
            
            if	(ie_tipo_w	is not null) then
        
                Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario);
            end if;
        end if;

        elsif	(deleting) then
            
            delete	from pep_item_pendente
            where 	IE_TIPO_REGISTRO = 'AON'
            and	nr_seq_registro = :old.nr_sequencia
            and	nvl(IE_TIPO_PENDENCIA,'L')	= 'L';	
            
    end if; 

exception
when others then
	null;
end;
   

<<Final>>
qt_reg_w	:= 0;
end;
/
