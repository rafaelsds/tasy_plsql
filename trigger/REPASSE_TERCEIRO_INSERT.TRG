create or replace trigger REPASSE_TERCEIRO_INSERT
before insert on REPASSE_TERCEIRO 
for each row

declare
cd_condicao_pagamento_w	number(10,0);
nr_seq_tipo_repasse_w	number(10,0);
begin

select	max(cd_condicao_pagamento),
	max(nr_seq_tipo_rep)
into	cd_condicao_pagamento_w,
	nr_seq_tipo_repasse_w
from	terceiro
where	nr_sequencia		= :new.nr_seq_terceiro;

if	(cd_condicao_pagamento_w is not null) and
	(:new.cd_condicao_pagamento is null) then
	:new.cd_condicao_pagamento := cd_condicao_pagamento_w;
end if;

if	(nr_seq_tipo_repasse_w is not null) and
	(:new.nr_seq_tipo is null) then
	:new.nr_seq_tipo := nr_seq_tipo_repasse_w;
end if;

end;
/
