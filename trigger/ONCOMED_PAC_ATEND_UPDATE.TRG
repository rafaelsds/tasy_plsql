create or replace trigger oncomed_pac_atend_update
before update on paciente_atendimento
for each row

declare

ie_claffis_cons_w	varchar2(10);
ds_comunicacao_w	varchar2(2000);

begin

select 	nvl(max(ie_classif_agenda),'xpto')
into	ie_claffis_cons_w
from	agenda_consulta
where	nr_atendimento = :new.nr_atendimento;

if 	(:old.DT_CHEGADA is null) and
	(:new.DT_CHEGADA is not null) and
	(ie_claffis_cons_w = 'PQUI') then
	
	
	ds_comunicacao_w := 	' 	Chegada do paciente na quimoiterapia: ' ||  chr(10)||   chr(13)||  ' ' ||  chr(10)||   chr(13)||
							' 	Paciente: ' || substr(obter_nome_paciente_setor(:new.nr_seq_paciente),1,60) ||  chr(10) ||
							' 	Chegada: '  || to_char(:new.dt_chegada,'dd/mm/yyyy hh24:mi:ss') ||  chr(10) ||    chr(13)||
						    ' 	Atendimento: ' || :new.nr_atendimento ||  chr(10)||  chr(13)||
						    ' 	Prescrição: ' || :new.nr_prescricao;

	
	Gerar_Comunic_Padrao (	sysdate,	
								'Chegada do paciente na quimioterapia',
								ds_comunicacao_w,
								:new.nm_usuario,
								'S',
								null,	
								'N',
								1,	
								'1864',	
								1,	
								null,	
								sysdate,
								null,	
								null);

	
end if;

end;
/