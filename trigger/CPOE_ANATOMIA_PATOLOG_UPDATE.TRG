create or replace trigger cpoe_anatomia_patolog_update
before update on cpoe_anatomia_patologica
for each row

declare
begin


if	(:new.dt_fim is null) or 	
	(:new.dt_inicio <> :old.dt_inicio) then	
	:new.dt_fim :=  :new.dt_inicio +1-1/(24*60*60);	
end if;

end;
/
