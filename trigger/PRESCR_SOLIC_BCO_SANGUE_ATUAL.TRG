CREATE OR REPLACE TRIGGER prescr_solic_bco_sangue_atual
BEFORE INSERT OR UPDATE ON prescr_solic_bco_sangue
FOR EACH ROW
declare

ie_prescr_emergencia_w		varchar2(1);
cd_funcao_origem_w		prescr_medica.cd_funcao_origem%type;

BEGIN

if (inserting) then

	begin

	if	(:new.dt_programada is not null) and
		(:new.dt_programada < sysdate) then
		
		select	nvl(max(ie_prescr_emergencia),'N'),	
				max(cd_funcao_origem)
		into	ie_prescr_emergencia_w,
				cd_funcao_origem_w
		from	prescr_medica
		where	nr_prescricao = :new.nr_prescricao;
		
		if	(ie_prescr_emergencia_w	<> 'S') and
			(cd_funcao_origem_w	<> 2314)  then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266264); 
		end if;	
	end if;

	exception
	when others then
		
	if	(:new.dt_programada is not null) and
		(:new.dt_programada < sysdate) and
		(cd_funcao_origem_w	<> 2314) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266264); 
	end if;	
	
	end;

else
	begin
	
	if	(((:old.dt_programada is null) and
		(:new.dt_programada is not null)) or
		((:new.dt_programada is not null) and
		(:new.dt_programada <> :old.dt_programada))) and 
		(:new.dt_programada < sysdate) then
		
		select	nvl(max(ie_prescr_emergencia),'N'),	
				max(cd_funcao_origem)
		into	ie_prescr_emergencia_w,
				cd_funcao_origem_w
		from	prescr_medica
		where	nr_prescricao = :new.nr_prescricao;
		
		if	(ie_prescr_emergencia_w	<> 'S') and
			(cd_funcao_origem_w	<> 2314)  then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266264); 
		end if;	
	end if;

	exception
	when others then
		
	if	(((:old.dt_programada is null) and
		(:new.dt_programada is not null)) or
		((:new.dt_programada is not null) and
		(:new.dt_programada <> :old.dt_programada))) and 
		(:new.dt_programada < sysdate) and
		(cd_funcao_origem_w	<> 2314) then
		
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266264); 
	end if;	
	
	end;
	
end if;	

END;
/