create or replace trigger SBIS_MED_AVALIACAO_PACIENTE
before insert or update on MED_AVALIACAO_PACIENTE
for each row

declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/
