create or replace
TRIGGER ageint_quest_perguntas_atual
BEFORE UPDATE ON ageint_quest_perguntas
FOR EACH ROW
DECLARE

qt_reg_w	number(10);


BEGIN

	if :new.ie_situacao <> 'A' then
	
		select	count(1)
		into 		qt_reg_w
		from		ageint_quest_estrutura
		where 	nr_seq_pergunta = :new.nr_sequencia 
		and 		ie_situacao = 'A';
		
		if qt_reg_w > 0 then 
		
			wheb_mensagem_pck.Exibir_Mensagem_Abort(1085539);
		
		end if;
	
	end if;

END;
/