CREATE OR REPLACE TRIGGER DIAGNOSTICO_DOENCA_HIST_ATUAL
BEFORE INSERT OR UPDATE ON DIAGNOSTICO_DOENCA_HIST
FOR EACH ROW
DECLARE
dt_diagnostico_w	Date;
nr_atendimento_w	Number(10);
qt_reg_w		number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.ie_status in ('P','S')) then
	begin
	select	max(a.dt_diagnostico),
		max(a.nr_atendimento)
	into	dt_diagnostico_w,
		nr_atendimento_w
	from	diagnostico_doenca a,
		diagnostico_medico b,
		atendimento_paciente x
	where	a.nr_atendimento	= x.nr_atendimento
	and	x.nr_atendimento	= b.nr_atendimento
	and	x.cd_pessoa_fisica	= :new.cd_pessoa_fisica
	and	a.cd_doenca		= :new.cd_doenca
	and	a.nr_atendimento	= b.nr_atendimento
	and	a.dt_diagnostico	= b.dt_diagnostico;
	exception
		when others then
		dt_diagnostico_w	:= to_date('02/01/1800','DD/MM/YYYY');
		nr_atendimento_w	:= 0;
	end;

	if	(:new.ie_status = 'P') then
		update	diagnostico_doenca
		set	ie_classificacao_doenca	= 'P'
		where	nr_atendimento	= nr_atendimento_w
		and	cd_doenca	= :new.cd_doenca
		and	dt_diagnostico	= dt_diagnostico_w;
	elsif	(:new.ie_status = 'S') then
		update	diagnostico_doenca
		set	ie_classificacao_doenca	= 'S'
		where	nr_atendimento	= nr_atendimento_w
		and	cd_doenca	= :new.cd_doenca
		and	dt_diagnostico	= dt_diagnostico_w;
	end if;
end if;
<<Final>>
qt_reg_w	:= 0;



END;
/
