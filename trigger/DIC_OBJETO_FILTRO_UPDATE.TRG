CREATE OR REPLACE TRIGGER dic_objeto_filtro_update 
after update on dic_objeto_filtro 
for each row
DECLARE 

ds_user_w varchar2(100);
BEGIN 
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then 
 
if	(:new.ds_label <> :old.ds_label) then 
	begin 
	update	dic_objeto_filtro_idioma 
	set	ds_descricao = :new.ds_label, 
		ie_necessita_revisao = 'S', 
		dt_atualizacao = sysdate, 
		nm_usuario = :new.nm_usuario 
	where	nr_seq_objeto = :new.nr_sequencia 
	and	nm_atributo = 'DS_LABEL'; 
	end; 
end if; 
end if;
end;
/