CREATE OR REPLACE 
TRIGGER HRP_TERCEIRO_UPDATE
AFTER UPDATE ON TERCEIRO
FOR EACH ROW

declare
nr_seq_terceiro_w	number(10,0);

BEGIN

if (:new.ie_situacao = 'I') then

	delete	from controle_pessoa
	where	:new.cd_pessoa_fisica = cd_pessoa_fisica
	or	:new.cd_cgc = cd_cgc;

end if;

END;
/
