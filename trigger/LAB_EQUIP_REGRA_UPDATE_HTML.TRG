create or replace TRIGGER LAB_EQUIP_REGRA_UPDATE_HTML

BEFORE INSERT OR UPDATE ON EXAME_LAB_EQUIP_REGRA

FOR EACH ROW

DECLARE

cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type;

nm_usuario_w      usuario.nm_usuario%type;

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

if (:new.DT_HORA_INICIO_HTML5 is not null and (:new.DT_HORA_INICIO_HTML5 <> :old.DT_HORA_INICIO_HTML5 or :old.DT_HORA_INICIO_HTML5 is null)) then

 begin
 
  cd_estabelecimento_w  := wheb_usuario_pck.get_cd_estabelecimento;

  nm_usuario_w    := wheb_usuario_pck.get_nm_usuario;

  :new.DS_HORA_INICIO    := substr(to_char(:new.DT_HORA_INICIO_HTML5, pkg_date_formaters.localize_mask('shortTime', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_w))),1,5);
  
 end;

end if;

if (:new.DT_HORA_FIM_HTML5 is not null and (:new.DT_HORA_FIM_HTML5 <> :old.DT_HORA_FIM_HTML5 or :old.DT_HORA_FIM_HTML5 is null)) then

 begin
 
  cd_estabelecimento_w  := wheb_usuario_pck.get_cd_estabelecimento;

  nm_usuario_w    := wheb_usuario_pck.get_nm_usuario;
  
  :new.DS_HORA_FIM       := substr(to_char(:new.DT_HORA_FIM_HTML5, pkg_date_formaters.localize_mask('shortTime', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_w))),1,5);

 end;

end if;

end if;

END;

/