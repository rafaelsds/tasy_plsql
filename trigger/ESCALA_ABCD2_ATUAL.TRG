create or replace trigger escala_abcd2_atual
before insert or update on escala_abcd2
for each row
declare
    EXEC_w         varchar2(200);
    qt_pontuacao_w number;
begin
    if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
      --INCIO MD
      begin
        EXEC_w := 'CALL CALCULA_ABCD2_MD(:1,:2,:3,:4,:5) INTO :result';
      
        EXECUTE IMMEDIATE EXEC_w USING IN :new.ie_age,
                                       IN :new.ie_blood_pressure, 
                                       IN :new.ie_clinical_features, 
                                       IN :new.ie_symptoms_duration, 
                                       IN :new.ie_diabetes,
                                       OUT qt_pontuacao_w;
      exception
        when others then
            qt_pontuacao_w := null;
      end;

      :new.qt_abcd2	:= qt_pontuacao_w;
      --FIM MD
    end if;
end;
/
