CREATE OR REPLACE TRIGGER escala_mirels_atual BEFORE
    INSERT OR UPDATE ON escala_mirels
    FOR EACH ROW
DECLARE
    sql_w            VARCHAR2(200);
    qt_pontuacao_w   NUMBER(10);
    qt_reg_w         NUMBER(1);
    ds_erro_w        VARCHAR2(4000);
    ds_parametros_w  VARCHAR2(4000);
BEGIN
    IF ( wheb_usuario_pck.get_ie_executar_trigger = 'N' ) THEN
        GOTO final;
    END IF;
    BEGIN
        sql_w := 'CALL OBTER_SCORE_ESCALA_MIRELS_MD(:1, :2, :3, :4) INTO :qt_pontuacao_w';
        EXECUTE IMMEDIATE sql_w
            USING IN :new.ie_local, IN :new.ie_dor, IN :new.ie_lesao, IN :new.ie_tamanho, OUT qt_pontuacao_w;

    EXCEPTION
        WHEN OTHERS THEN
      ds_erro_w := sqlerrm;
      ds_parametros_w := (':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.cd_profissional: '||:new.cd_profissional||'-'||':new.ie_situacao: '||:new.ie_situacao||'-'||
                          ':new.ie_local: '||:new.ie_local||'-'||':new.ie_dor: '||:new.ie_dor||'-'||':new.ie_lesao: '||:new.ie_lesao||'-'||
                          ':new.ie_tamanho: '||:new.ie_tamanho||'-'||'qt_pontuacao_w: '||qt_pontuacao_w);

      gravar_log_medical_device ('escala_mirels_atual','OBTER_SCORE_ESCALA_MIRELS_MD'
                                 ,ds_parametros_w,substr(ds_erro_w, 4000),:new.nm_usuario,'N');  
        
            qt_pontuacao_w := NULL;
    END;

    :new.qt_pontuacao := qt_pontuacao_w;
    << final >> qt_reg_w := 0;
END;
/