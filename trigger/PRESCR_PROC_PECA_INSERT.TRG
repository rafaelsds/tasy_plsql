create or replace trigger prescr_proc_peca_insert
before insert on prescr_proc_peca
for each row

declare

begin
if	(:new.ie_status is null) then
	:new.ie_status	:= 'P';
end if;

if 	(:new.ie_peca_principal is null) then
	 :new.ie_peca_principal := 'N';
end if;

end;
/
