CREATE OR REPLACE TRIGGER Evento_Paciente_Insert
AFTER Insert ON hd_dialise_evento
FOR EACH ROW
DECLARE

nr_seq_evento_pac_w		number(10);
nr_atendimento_w			number(10);
cd_setor_atendimento_w		number(10);
cd_estabelecimento_w		number(10);
nr_sequencia_w			number(10);
nr_seq_tipo_nao_conformidade_w  	number(10);

begin

if	(:new.nr_seq_dialise_peritonial is null) then
	nr_seq_evento_pac_w := 0;
	
	select	nvl(nr_seq_evento_pac,0),
		nvl(nr_seq_tipo_nao_conformidade,0)
	into	nr_seq_evento_pac_w,
		nr_seq_tipo_nao_conformidade_w
	from	HD_TIPO_EVENTO
	where	nr_sequencia = :new.nr_seq_tipo_evento;

	if	(nr_seq_evento_pac_w > 0) then
		select	nvl(nr_atendimento,0),
			nvl(obter_setor_atendimento(nr_atendimento),0)
		into	nr_atendimento_w,
			cd_setor_atendimento_w
		from	hd_dialise
		where	nr_sequencia = :new.nr_seq_dialise;
		
		select 	nvl(cd_estabelecimento,0)
		into	cd_estabelecimento_w
		from   	hd_unidade_dialise
		where  	nr_sequencia = :new.nr_seq_unid_dialise;
		
		select	qua_evento_paciente_seq.nextval
		into	nr_sequencia_w
		from	dual;


		insert into qua_evento_paciente
			(
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			nr_atendimento,
			nr_seq_evento,
			dt_evento,
			ds_evento,
			cd_setor_atendimento,
			dt_cadastro,
			nm_usuario_origem,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_usuario_reg,
			nr_seq_classif_evento,
			dt_liberacao,
			cd_pessoa_fisica,
			ie_situacao,
			ie_status,
			ie_origem,
			nr_seq_tipo_evento,
			ie_tipo_evento,
			cd_funcao_ativa
			)
		values
			(
			nr_sequencia_w,
			cd_estabelecimento_w,
			sysdate,
			:new.nm_usuario,
			nr_atendimento_w,
			nr_seq_evento_pac_w,
			sysdate,
			substr(wheb_mensagem_pck.get_texto(305932),1,100),
			cd_setor_atendimento_w,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nm_usuario,
			null,
			sysdate,
			obter_pessoa_atendimento(nr_atendimento_w,'C'),
			'A',
			'1',
			'S',
			:new.nr_seq_tipo_evento,
			'E',
			obter_funcao_ativa
			);	
			
		if	(nr_seq_tipo_nao_conformidade_w > 0) then
			qua_gerar_nao_conformidade(	nr_sequencia_w,
							nr_seq_tipo_nao_conformidade_w,
							cd_estabelecimento_w,
							obter_pessoa_atendimento(nr_atendimento_w,'C'),
							sysdate,
							'S',
							:new.nm_usuario,
							'N');
							
		end if;
	end if;
end if;

end;
/
