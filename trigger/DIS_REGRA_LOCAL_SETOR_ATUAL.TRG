create or replace trigger dis_regra_local_setor_atual
before insert or delete or update on dis_regra_local_setor
for each row

declare
begin

if	(inserting)then
	gerar_int_dankia_pck.dankia_regra_local_setor(:new.nr_seq_dis_regra_setor,:new.cd_local_estoque,'I',:new.nm_usuario);
elsif(deleting) then
	gerar_int_dankia_pck.dankia_regra_local_setor(:old.nr_seq_dis_regra_setor,:old.cd_local_estoque,'E',:old.nm_usuario);
elsif(updating) then
	if	(:old.cd_local_estoque is not null) and
		(:new.cd_local_estoque is not null) and
		(:old.cd_local_estoque <> :new.cd_local_estoque) then
		gerar_int_dankia_pck.dankia_regra_local_setor(:old.nr_seq_dis_regra_setor,:old.cd_local_estoque,'E',:new.nm_usuario);
		gerar_int_dankia_pck.dankia_regra_local_setor(:new.nr_seq_dis_regra_setor,:new.cd_local_estoque,'I',:new.nm_usuario);
	end if;
end if;

end;
/