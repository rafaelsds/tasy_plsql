create or replace trigger diops_movto_cedida_atual
before update on diops_movto_corresp_cedida
for each row

declare

dt_mes_competencia_w	date;
nr_campo_alteracao_w	number(1)	:= null;
vl_evento_w		number(15,2) 	:= 0;
nm_usuario_w		varchar(255);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if	(:old.vl_mes_n	<> :new.vl_mes_n) then
		vl_evento_w		:= :new.vl_mes_n;
		nr_campo_alteracao_w	:= 0;
	elsif	(:old.vl_mes_n1	<> :new.vl_mes_n1) then
		vl_evento_w		:= :new.vl_mes_n1;
		nr_campo_alteracao_w	:= -1;
	elsif	(:old.vl_mes_n2	<> :new.vl_mes_n2) then
		vl_evento_w		:= :new.vl_mes_n2;
		nr_campo_alteracao_w	:= -2;
	end if;

	if	(nr_campo_alteracao_w is not null) then
		select	add_months(trunc(a.dt_periodo_final,'month'),nr_campo_alteracao_w)
		into	dt_mes_competencia_w
		from	diops_periodo	a
		where	a.nr_sequencia	= :new.nr_seq_periodo;

		nm_usuario_w	:= wheb_usuario_pck.get_nm_usuario;

		update 	w_diops_movto_pel
		set	vl_evento 				= vl_evento_w,
			dt_atualizacao 				= sysdate,
			nm_usuario 				= nm_usuario_w
		where	nr_seq_periodo 				= :new.nr_seq_periodo
		and	nr_seq_tipo_movto			= :new.nr_seq_tipo_movto
		and	trunc(dt_mes_competencia, 'month')	= dt_mes_competencia_w
		and	ie_tipo_segurado			= 'R';
								
		:new.nm_usuario		:= nm_usuario_w;
		:new.dt_atualizacao	:= sysdate;
	end if;
end if;

end;
/
