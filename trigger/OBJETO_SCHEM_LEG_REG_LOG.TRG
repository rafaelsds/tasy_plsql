create or replace trigger OBJETO_SCHEM_LEG_REG_LOG
before insert or update or delete on OBJETO_SCHEMATIC_LEGENDA
for each row

declare

operation_id_w	varchar2(1);
nr_sequencia_w	number(10);
cd_funcao_w	objeto_schematic.cd_funcao%type;

begin

if	inserting then
	operation_id_w 	:= 'I';
	nr_sequencia_w	:= :new.NR_SEQ_OBJETO;
elsif	updating then
	operation_id_w := 'U';
	nr_sequencia_w	:= :new.NR_SEQ_OBJETO;
elsif	deleting then
	operation_id_w := 'D';
	nr_sequencia_w	:= :old.NR_SEQ_OBJETO;
end if;

if (deleting) then
	cd_funcao_w := obter_funcao_objeto_schematic('S', nr_sequencia_w);
else
	select	max(cd_funcao)
	into	cd_funcao_w
	from	objeto_schematic
	where	nr_sequencia = nr_sequencia_w;
end if;

GENERATE_REG_OBJECT_LOG(wheb_usuario_pck.get_nm_usuario, 
			operation_id_w, 
			null,
			nr_sequencia_w,
			cd_funcao_w,
			'OBJETO_SCHEMATIC_LEGENDA',
			nvl(:old.nr_sequencia,:new.nr_sequencia));

end;
/