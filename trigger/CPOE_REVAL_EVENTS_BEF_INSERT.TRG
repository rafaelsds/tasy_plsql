create or replace trigger cpoe_reval_events_bef_insert
before insert on cpoe_revalidation_events for each row
declare

begin

	:new.ds_stack := substr(dbms_utility.format_call_stack, 1, 3000);

end;
/