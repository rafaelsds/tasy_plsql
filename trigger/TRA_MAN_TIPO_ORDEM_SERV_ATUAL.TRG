create or replace trigger tra_man_tipo_ordem_serv_atual
before insert or update on man_tipo_ordem_servico
for each row

declare
qt_corp_w		number(10);
qt_wheb_w		number(10);
cd_expressao_w		dic_expressao.cd_expressao%type;
ds_expressao_br_w	dic_expressao.ds_expressao_br%type;

begin
man_traduzir_texto(	:new.ds_tipo,
			:new.nm_usuario,
			cd_expressao_w,
			ds_expressao_br_w);

:new.cd_exp_tipo	:= cd_expressao_w;

end;
/