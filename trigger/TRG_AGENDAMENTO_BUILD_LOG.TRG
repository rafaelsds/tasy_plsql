create or replace TRIGGER TRG_AGENDAMENTO_BUILD_LOG after
  INSERT OR UPDATE OR DELETE  ON agendamento_build
  FOR EACH ROW
DECLARE

  action_w varchar2(2);	
	
  change_request_action CONSTANT varchar2(2 char) := 'CR';
  pending_approval_action CONSTANT varchar2(2 char) := 'PA';
  scheduled_build_action CONSTANT varchar2(2 char) := 'SB';
  canceled_action CONSTANT varchar2(2 char) := 'C';
  executed_build_action CONSTANT varchar2(2 char) := 'EB';  
  change_request_refused_action CONSTANT varchar2(2 char) := 'RR';
  change_request_approved_action CONSTANT varchar2(2 char) := 'RA';
  priority_build_action CONSTANT varchar2(2 char) := 'P';
  rescheduled_build_action CONSTANT varchar2(2 char) := 'RB';
  undefined_action CONSTANT varchar2(2 char) := 'U';
  delete_action CONSTANT varchar2(2 char) := 'D';
  lost_priority_action CONSTANT varchar2(2 char) := 'LP';
  
  troca_pendente_aprovacao CONSTANT varchar2(2 char) := 'TA';
  cancelado CONSTANT varchar2(2 char) := 'C';
  build_agendado CONSTANT varchar2(2 char) := 'BA'; 
  build_executado CONSTANT varchar2(2 char) := 'BE';
  pendente_liberacao CONSTANT varchar2(2 char) := 'PL';
  periodo_prioritario CONSTANT varchar2(2 char) := 'P';
  periodo_vespertino CONSTANT varchar2(2 char) := 'V';
  periodo_matutino CONSTANT varchar2(2 char) := 'M';

  
BEGIN
	if (inserting) then
		action_w := pending_approval_action;
	elsif (deleting) then 
		action_w := delete_action;
		insert into agendamento_build_log 
		(nr_sequencia,
		 nm_usuario,
		 nr_seq_agendamento_build,
		 dt_atualizacao,
		 dt_agendamento,
		 ie_acao,
		 cd_versao,
		 ie_aplicacao,
		 ie_periodo
		 )
		values
		(AGENDAMENTO_BUILD_LOG_SEQ.nextval, 
		 :old.nm_usuario,
		 :old.nr_sequencia,
		 sysdate,
		 :old.dt_agendamento,
	     delete_action,
		 :old.cd_versao,
		 :old.ie_aplicacao,
		 :old.ie_periodo
		);
	elsif (updating) then    
	    if (:old.ie_status = pendente_liberacao and :new.ie_status = build_agendado) then 
			action_w :=  scheduled_build_action;
			
		elsif (:new.ie_status = build_executado) then
			action_w :=  executed_build_action;
			 
		elsif (:new.ie_status = troca_pendente_aprovacao) then 
			action_w := change_request_action;
		elsif (:old.ie_status = troca_pendente_aprovacao) then 
			if (:new.ie_status = cancelado) then
				action_w := change_request_refused_action;			
			elsif (nvl(:old.dt_agendamento, to_date('1999-01-01', 'yyyy-MM-dd')) = nvl(:new.dt_agendamento, to_date('1999-01-01', 'yyyy-MM-dd'))) then 
				action_w := change_request_approved_action;
			end if;
		elsif (:new.ie_status = cancelado) then
			action_w := canceled_action;
		elsif (:new.ie_status = build_agendado and :new.ie_periodo = periodo_prioritario and :old.ie_periodo in (periodo_vespertino, periodo_matutino)) then
			action_w := priority_build_action;
		elsif(:old.ie_periodo <> :new.ie_periodo and :new.ie_periodo <> periodo_prioritario)then 
			action_w := rescheduled_build_action;
		elsif(:new.ie_periodo <> :old.ie_periodo and :old.ie_periodo = periodo_prioritario)then 
			action_w := lost_priority_action;
		else
			action_w :=  undefined_action;
		end if;
	end if;
		
	if (action_w <> delete_action) then
		insert into agendamento_build_log 
		(nr_sequencia,
		 nm_usuario,
		 nr_seq_agendamento_build,
		 dt_atualizacao,
		 dt_agendamento,
		 ie_acao,
		 cd_versao,
		 ie_aplicacao,
		 ie_periodo
		 )
		values
		(AGENDAMENTO_BUILD_LOG_SEQ.nextval, 
		 :new.nm_usuario,
		 :new.nr_sequencia,
		 sysdate,
		 :new.dt_agendamento,
		 action_w,
		 :new.cd_versao,
		 :new.ie_aplicacao,
		 :new.ie_periodo
		);
	end if;
end;

/