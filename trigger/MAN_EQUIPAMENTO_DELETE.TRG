CREATE OR REPLACE 
TRIGGER man_equipamento_Delete
BEFORE DELETE on man_equipamento
for each row

BEGIN
update	nf_integracao_pat_equip
set	nr_seq_equipamento	= null
where	nr_seq_equipamento	= :old.nr_sequencia;

END;
/