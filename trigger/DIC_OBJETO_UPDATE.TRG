CREATE OR REPLACE TRIGGER dic_objeto_update 
after update on dic_objeto 
for each row
declare
ds_user_w varchar2(100);

begin
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then
 
if	(:new.ie_tipo_objeto = 'P') and 
	(:new.ds_texto <> :old.ds_texto) then 
	begin 
	update	dic_objeto_idioma 
	set	ds_descricao = :new.ds_texto, 
		ie_necessita_revisao = 'S', 
		dt_atualizacao = sysdate, 
		nm_usuario = :new.nm_usuario 
	where	nr_seq_objeto = :new.nr_sequencia 
	and	nm_atributo = 'DS_TEXTO'; 
	end; 
elsif	(:new.ie_tipo_objeto = 'AC') and 
	(:new.nm_campo_tela <> :old.nm_campo_tela) then 
	begin 
	update	dic_objeto_idioma 
	set	ds_descricao = :new.ds_texto, 
		ie_necessita_revisao = 'S', 
		dt_atualizacao = sysdate, 
		nm_usuario = :new.nm_usuario 
	where	nr_seq_objeto = :new.nr_sequencia 
	and	nm_atributo = 'NM_CAMPO_TELA'; 
	end; 
end if; 
end if; 
end;
/