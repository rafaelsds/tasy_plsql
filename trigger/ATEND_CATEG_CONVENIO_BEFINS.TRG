CREATE OR REPLACE TRIGGER ATEND_CATEG_CONVENIO_BEFINS
BEFORE INSERT ON Atend_Categoria_Convenio
FOR EACH ROW
DECLARE

ie_tipo_convenio_w		number(2) := 2;
ie_tipo_convenio_atend_w	number(2);
cd_estabelecimento_w		Number(05,0);
ie_categ_lib_w			varchar2(1);
ie_conv_lib_w			varchar2(1);
ie_atualizar_tipo_conv_atend_w	varchar2(1) := 'S';
ie_atualiza_guia_princ_w	varchar2(1);
qt_crm_atendimentos_w		number(10);
ie_integracao_dynamics_w	varchar2(1);
ie_tipo_guia_regra_w		varchar(1);
ie_tipo_guia_atend_w		varchar(5);
cd_perfil_w					perfil.cd_perfil%type;
ie_cat_lib_pf_w			varchar2(1);
ie_entrada_vigencia_w 		varchar2(1);
dt_entrada_w			Date;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

select	nvl(max(cd_estabelecimento),wheb_usuario_pck.get_cd_estabelecimento),
	max(ie_tipo_convenio),
	wheb_usuario_pck.get_cd_perfil,
	max(dt_entrada)
into	cd_estabelecimento_w,
	ie_tipo_convenio_atend_w,
	cd_perfil_w,
	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;


Obter_param_Usuario(916, 637, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_entrada_vigencia_w);
Obter_param_Usuario(916, 688, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_atualiza_guia_princ_w);
Obter_param_Usuario(916, 1016, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_tipo_guia_regra_w);

if	(ie_entrada_vigencia_w = 'S') and
	(dt_entrada_w < :new.dt_inicio_vigencia) then
	Wheb_mensagem_pck.exibir_mensagem_abort( 262357 , 'DT_ENTRADA='||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss')||';DT_VIGENCIA='||to_char(:new.dt_inicio_vigencia,'dd/mm/yyyy hh24:mi:ss'));
end if;

begin
select ie_tipo_convenio
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio = :new.cd_convenio;
exception
	when others then
      	ie_tipo_convenio_w := 2;
end;

if	(:new.nm_usuario_original is null) then
	:new.nm_usuario_original := :new.nm_usuario;
end if;

/*     Retiado o comando abaixo pois estava gernado DEADLOCK na base do cliente Richart*/
/*select	nvl(max(ie_atualizar_tipo_conv_atend),'S')
into	ie_atualizar_tipo_conv_atend_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_w;

if	(ie_atualizar_tipo_conv_atend_w = 'S') then
	begin
		update	atendimento_paciente
		set	ie_tipo_convenio = ie_tipo_convenio_w
		where	nr_atendimento 	 = :new.nr_atendimento; 
	exception
		when others then
	      	ie_tipo_convenio_w := 2;
	end;
end if;*/

/* Rafael em 30/04/2008 OS91546 */
select	obter_se_categoria_lib_estab(cd_estabelecimento_w,:new.cd_convenio,:new.cd_categoria)
into	ie_categ_lib_w
from	dual;

if	(ie_categ_lib_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(196844);
end if;

/* Fim Rafael em 30/04/2008 OS91546 */

/* Fabricio em 05/09/2008 OS 107386*/
select	nvl(max('S'),'N')
into	ie_conv_lib_w
from 	convenio a
where 	exists (select 	1 
		from 	convenio_estabelecimento b
		where 	b.cd_estabelecimento = cd_estabelecimento_w
		and 	b.cd_convenio = a.cd_convenio)
and 	a.ie_situacao = 'A'
and 	a.cd_convenio = :new.cd_convenio;

if	(ie_conv_lib_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(196845);
end if;	  
/* Fim Fabricio em 05/09/2008 OS 107386*/

If	(ie_atualiza_guia_princ_w = 'S') then
	:new.nr_doc_conv_principal := :new.nr_doc_convenio;
end if;

select	nvl(max(ie_integracao_dynamics),'N')
into	ie_integracao_dynamics_w
from	parametro_atendimento
where	cd_estabelecimento = cd_estabelecimento_w;

-- Atualiza tabela de integracao CRM_ATENDIMENTOS
if	(ie_integracao_dynamics_w = 'S') then
	select	count(*)
	into	qt_crm_atendimentos_w
	from	crm_atendimentos
	where	nr_atendimento = :new.nr_atendimento;

	if 	(qt_crm_atendimentos_w > 0) then
		update	crm_atendimentos
		set	ds_convenio 	= substr(obter_nome_convenio(:new.cd_convenio),1,255),
			ds_categoria 	= substr(obter_categoria_convenio(:new.cd_convenio,:new.cd_categoria),1,80),
			ds_plano_convenio = substr(Obter_Desc_Plano_Conv(:new.cd_convenio,:new.cd_plano_convenio),1,80),
			ie_status	= 2,
			dt_atualizacao	= sysdate,
			nm_usuario	= :new.nm_usuario
		where	nr_atendimento 	= :new.nr_atendimento;
	end if;
end if;

if	(ie_tipo_guia_regra_w = 'S') then
	select 	max(a.ie_tipo_guia)
	into 	ie_tipo_guia_atend_w
	from	(
		select	ie_tipo_guia 
		from	tipo_guia_atend
		where 	ie_tipo_atendimento = (
						select	ie_tipo_atendimento 
						from	atendimento_paciente
						where	nr_atendimento = :new.nr_atendimento)
		and		((cd_estabelecimento = (
						select	cd_estabelecimento 
						from	atendimento_paciente
						where	nr_atendimento = :new.nr_atendimento))
				or	(cd_estabelecimento is null))
		and		((ie_clinica = (
						select	nvl(ie_clinica,0)
						from	atendimento_paciente
						where	nr_atendimento = :new.nr_atendimento))
				or 	(ie_clinica is null))
        order by  ie_guia_padrao desc) a
	where
	rownum <= 1;
	
	if ( ie_tipo_guia_atend_w <> '') or ( ie_tipo_guia_atend_w is not null) then
		:new.ie_tipo_guia := ie_tipo_guia_atend_w ;
	end if;
end if;

end if;

END;
/
