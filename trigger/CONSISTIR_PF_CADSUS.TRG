CREATE OR REPLACE PROCEDURE consistir_pf_cadsus (
    cd_pessoa_fisica_p    VARCHAR2,
    nm_usuario_p          VARCHAR2,
    qt_inconsistencia_p   OUT NUMBER
) IS
    qt_inconsistencia_w   NUMBER(10);
    pf_rec                PESSOA_FISICA%rowtype;
    ds_termo_w            VARCHAR2(255);
	ie_possui_w			  varchar2(1);
	cd_raca_sus_w		  cor_pele.cd_raca_cor_sus%type;
BEGIN
    DELETE FROM cadsus_inconsistencias WHERE nm_usuario = nm_usuario_p;
        
    SELECT	*
    INTO    pf_rec
    FROM    pessoa_fisica
    WHERE   cd_pessoa_fisica = cd_pessoa_fisica_p;
        
    /*---------------------*/
    /*inconsistencias begin*/

    IF ( pf_rec.nm_pessoa_fisica IS NULL ) THEN
        add_inconsistencia_pf_cadsus(nm_usuario_p,'Nome n�o pode ser v�zio.');
    ELSE
        IF ( regexp_like(pf_rec.nm_pessoa_fisica,'.*?{3,}\s+?.*') ) THEN
            SELECT	MAX(ds_termo)
            INTO    ds_termo_w
            FROM    sus_termo_invalido
            WHERE	REGEXP_LIKE ( pf_rec.nm_pessoa_fisica, ds_termo, 'i' );

            IF ( ds_termo_w IS NOT NULL ) THEN
                add_inconsistencia_pf_cadsus(nm_usuario_p,'Nomes de pessoa n�o devem possuir o termo: ' || ds_termo_w);
            END IF;

            IF ( not regexp_like(pf_rec.nm_pessoa_fisica,'^[a-zA-Z�^~� '']+$') ) THEN
                add_inconsistencia_pf_cadsus(nm_usuario_p,'Nomes podem conter somente letras, �, ^, ~ e � ');
            END IF;
        ELSE
            add_inconsistencia_pf_cadsus(nm_usuario_p,'Nome n�o pode ter menos que 3 caracteres e deve possuir mais de um termo.');
        END IF;
				
		begin
			select	'S'
			into	ie_possui_w
			from 	compl_pessoa_fisica
			where	nm_contato is not null
			and		cd_pessoa_fisica = pf_rec.cd_pessoa_fisica
			and	 	ie_tipo_complemento = 5;
		exception
		when others then
			add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o do complemento m�e. Caso n�o tenha a informa��o em m�os colocar como SEM INFORMA��O');
		end;
		
		begin
			select	'S'
			into	ie_possui_w
			from 	compl_pessoa_fisica
			where	nm_contato is not null
			and		cd_pessoa_fisica = pf_rec.cd_pessoa_fisica
			and	 	ie_tipo_complemento = 4;
		exception
		when others then
			add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o do complemento pai. Caso n�o tenha a informa��o em m�os colocar como SEM INFORMA��O');
		end;		

		if (pf_rec.ie_sexo is null) then
			add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o do sexo do paciente.');
		end if;
		
		if (pf_rec.nr_seq_cor_pele is null) then
			add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o da cor do paciente.');
		end if;
		
		if (pf_rec.nr_seq_cor_pele is not null) then
			select	Sus_Obter_Cor_Pele(pf_rec.cd_pessoa_fisica,'C')
			into	cd_raca_sus_w
			from 	dual;
			
			if (cd_raca_sus_w = '5') and (pf_rec.nr_seq_etnia is null) then
				add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o da ra�a do paciente, quando o mesmo for indigena.');
			end if;
		end if;
		
		if (pf_rec.dt_nascimento is null) then
			add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o da data de nascimento do paciente.');
		end if;
		
		if (pf_rec.dt_obito is not null) then
			begin
				select	'S'
				into	ie_possui_w
				from 	declaracao_obito
				where	nr_atendimento = ( select max(nr_atendimento) from atendimento_paciente where cd_pessoa_fisica = pf_rec.cd_pessoa_fisica)
				and		nr_seq_causa_morte is not null;			
				
			exception
			when others then
				add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o da Causa direta da morte do paciente na Movimenta��o de Paciente para o �ltimo atendimento do paciente.');
			end;			
		end if;
		
		if (pf_rec.cd_nacionalidade is null) then
			add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o da nacionalidade do paciente.');
		else
			begin
				select	'S'
				into	ie_possui_w
				from	nacionalidade
				where	ie_brasileiro = 'S'
				and		cd_nacionalidade = pf_rec.cd_nacionalidade;
				
				if (ie_possui_w = 'S') then
					begin
						select	'S'
						into	ie_possui_w
						from	pessoa_fisica
						where	cd_pessoa_fisica = pf_rec.cd_pessoa_fisica
						and		nr_cep_cidade_nasc is not null;
					exception
					when others then
						add_inconsistencia_pf_cadsus(nm_usuario_p,'� obrigat�rio a informa��o da cidade natal.');
					end;
				end if;			
			exception
			when others then
				null;
			end;
		end if;	
		
    END IF;   	
	
    /*inconsistencias end*/
    /*-------------------*/

    SELECT
        COUNT(*)
    INTO
        qt_inconsistencia_w
    FROM
        cadsus_inconsistencias
    WHERE
        nm_usuario = nm_usuario_p;

    qt_inconsistencia_p := qt_inconsistencia_w;
    COMMIT;
END consistir_pf_cadsus;
/