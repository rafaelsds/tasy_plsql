CREATE OR REPLACE TRIGGER EME_CHAMADO_INSERT
BEFORE INSERT OR UPDATE ON EME_CHAMADO
FOR EACH ROW
DECLARE

vl_custo_w	Number(15,4);
km_minima_w	Number(10);

BEGIN

vl_custo_w:= obter_eme_valor_custo_km(:new.nr_seq_veiculo, :new.nr_seq_custo_km);

:new.vl_km_unit		:= vl_custo_w;
if	(:new.qt_km_saida is not null) and
	(:new.qt_km_retorno is not null)  then
	:new.qt_km_percorrido	:= :new.qt_km_retorno - :new.qt_km_saida;
end if;

km_minima_w := 	obter_eme_km_minima(:new.nr_seq_veiculo, :new.nr_seq_custo_km);

if 	(km_minima_w is not null) then

	if (:new.qt_km_percorrido < km_minima_w) then
		:new.qt_km_cobrar := 0;
	else 
		:new.qt_km_cobrar	:= round(:new.qt_km_percorrido - km_minima_w);
	end if;
	:new.vl_km_total	:= :new.qt_km_cobrar * :new.vl_km_unit;
else
	:new.qt_km_cobrar		:= :new.qt_km_percorrido;
	:new.vl_km_total		:= :new.qt_km_percorrido * :new.vl_km_unit;
end if;

END;
/