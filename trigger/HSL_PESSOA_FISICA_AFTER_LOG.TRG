create or replace trigger hsl_pessoa_fisica_after_log
after update on pessoa_fisica
for each row
declare

ds_dados_tasy_w		varchar2(4000);
ds_call_stack_w		varchar2(4000);
ds_session_tasy_w		varchar2(4000);
nm_usuario_w		varchar2(15);
ds_enter_w		varchar2(10) := chr(13) || chr(10);

begin

if	(nvl(:new.cd_pessoa_fisica,'0') <> '0') and
	(nvl(:old.nm_pessoa_fisica,'0') <> nvl(:new.nm_pessoa_fisica,'0')) then
	begin
	nm_usuario_w	:= nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario);
	
	if	(nvl(wheb_usuario_pck.get_cd_estabelecimento,0) > 0) then
		begin
		ds_dados_tasy_w	:= substr('# Dados Tasy #' || ds_enter_w ||
					'Estabelecimento: ' || wheb_usuario_pck.get_cd_estabelecimento || ds_enter_w ||
					'Fun��o: ' || wheb_usuario_pck.get_cd_funcao || ds_enter_w ||
					'Perfil: ' || wheb_usuario_pck.get_cd_perfil || ds_enter_w ||
					'Usu�rio: ' || wheb_usuario_pck.get_nm_usuario || ds_enter_w ||
					'Form: ' || wheb_usuario_pck.get_ds_form || ds_enter_w ||
					'M�quina: ' || wheb_usuario_pck.get_nm_maquina || ds_enter_w ||
					'Esta��o: ' || wheb_usuario_pck.get_nm_estacao || ds_enter_w || ds_enter_w,1,4000);
		end;
	end if;

	select	substr('old.nm_pessoa_fisica: ' || :old.nm_pessoa_fisica || ds_enter_w || 'new.nm_pessoa_fisica: ' || :new.nm_pessoa_fisica || ds_enter_w || ds_enter_w ||
		ds_dados_tasy_w ||
		'# Dados sess�o DB #' || ds_enter_w ||
		'sid: ' || sid || ds_enter_w ||
		'serial: ' || serial# || ds_enter_w ||
		'logon_time: ' || to_char(logon_time,'dd/mm/yyyy hh24:mi:ss') || ds_enter_w ||
		'username: ' || username || ds_enter_w ||
		'service_name: ' || service_name || ds_enter_w ||
		'schemaname: ' || schemaname || ds_enter_w ||
		'machine: ' || machine || ds_enter_w ||
		'terminal: ' || terminal || ds_enter_w ||
		'osuser: ' || osuser || ds_enter_w ||
		'program: ' || program || ds_enter_w ||
		'module: ' || module,1,4000)
	into	ds_session_tasy_w
	from	v$session
	where	audsid = (select userenv('SESSIONID') from dual);
	
	insert into pessoa_fisica_historico(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			nm_pessoa_fisica,
			nr_seq_tipo_hist,
			dt_liberacao,
			ds_historico,
			cd_estabelecimento)
		values(	pessoa_fisica_historico_seq.nextval,
			sysdate,
			nm_usuario_w,
			sysdate,
			'OS551659',			
			:new.cd_pessoa_fisica,
			:new.nm_pessoa_fisica,
			null,
			sysdate,
			ds_session_tasy_w,
			wheb_usuario_pck.get_cd_estabelecimento);

	ds_call_stack_w := substr(dbms_utility.format_call_stack,1,4000);
	
	insert into pessoa_fisica_historico(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			nm_pessoa_fisica,
			nr_seq_tipo_hist,
			dt_liberacao,
			ds_historico,
			cd_estabelecimento)
		values(	pessoa_fisica_historico_seq.nextval,
			sysdate,
			nm_usuario_w,
			sysdate,
			'OS551659',
			:new.cd_pessoa_fisica,
			:new.nm_pessoa_fisica,
			null,
			sysdate,
			ds_call_stack_w,
			wheb_usuario_pck.get_cd_estabelecimento);
	end;
end if;

end;
/