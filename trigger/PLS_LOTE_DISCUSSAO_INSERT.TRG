create or replace trigger pls_lote_discussao_insert
after insert on pls_lote_discussao
for each row

declare
ie_status_w		pls_lote_contestacao.ie_status%type := 'D'; -- Contesta��o Em discuss�o
nr_seq_pls_fatura_w	pls_lote_contestacao.nr_seq_pls_fatura%type;
nr_seq_ptu_fatura_w	pls_lote_contestacao.nr_seq_ptu_fatura%type;
qt_integral_w		pls_integer;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	select	max(nr_seq_pls_fatura),
		max(nr_seq_ptu_fatura)
	into	nr_seq_pls_fatura_w,
		nr_seq_ptu_fatura_w
	from	pls_lote_contestacao
	where	nr_sequencia	= :new.nr_seq_lote_contest;

	-- No momento que importa um A550 de faturamento e gerar discuss�o - OS 605067
	-- Contesta��o da OPS - Faturamento
	if	(nr_seq_pls_fatura_w is not null) then
		if	(:new.ie_status = 'A') then
			-- Contesta��o Enviada/Recebida
			ie_status_w := 'E';
		end if;
		
		-- Quando incluir uma nova discuss�o, mudar o status do lote de contesta��o caso esteja em aberto
		update	pls_lote_contestacao
		set	ie_status	= ie_status_w,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= :new.dt_atualizacao
		where	nr_sequencia	= :new.nr_seq_lote_contest
		and	ie_status	= 'E';
		
		-- compatibilidade com a rotina de altera��o de status, se chamar ela aqui pode gerar o erro de mutante
		if	(ie_status_w = 'C') then
		
			update	pls_contestacao_discussao
			set	ie_status	= 'E',
				nm_usuario	= :new.nm_usuario,
				dt_atualizacao	= sysdate
			where	nr_seq_lote	= :new.nr_sequencia;
		end if;
	end if;
	
	-- Contesta��o da OPS - Contas de Interc�mbio (A500)
	if	(nr_seq_ptu_fatura_w is not null) then
		qt_integral_w := 0;
		if	(:new.ie_tipo_arquivo in (5,6,7,8,9)) then			
			-- Verificar se o decurso de prazo � complementar ou parcial
			if	(:new.ie_tipo_arquivo in (9)) then
				-- Se existir ao menos um questionamento com tipo de acordo 10, ent�o a discuss�o dever� ser concluida
				-- Feito na OS 909630 - dia 16-03-2017
				select	count(1)
				into	qt_integral_w
				from	ptu_camara_contestacao	c,
					ptu_questionamento	q
				where	c.nr_sequencia		= q.nr_seq_contestacao
				and	c.nr_seq_lote_contest	= :new.nr_seq_lote_contest
				and	q.ie_tipo_acordo	in ('10', '15'); -- Encerrado pelo administrador 
			end if;
			
			-- Contesta��o Concluida
			if	(qt_integral_w > 0) then
				ie_status_w := 'C';
			end if;
		end if;
		
		update	pls_lote_contestacao
		set	ie_status	= ie_status_w,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= :new.dt_atualizacao
		where	nr_sequencia	= :new.nr_seq_lote_contest
		and	ie_status	in ('E','D');
		
		
		-- compatibilidade com a rotina de altera��o de status, se chamar ela aqui pode gerar o erro de mutante
		if	(ie_status_w = 'C') then
		
			update	pls_contestacao_discussao
			set	ie_status	= 'E',
				nm_usuario	= :new.nm_usuario,
				dt_atualizacao	= sysdate
			where	nr_seq_lote	= :new.nr_sequencia;
		end if;
	end if;

end if;

end;
/
