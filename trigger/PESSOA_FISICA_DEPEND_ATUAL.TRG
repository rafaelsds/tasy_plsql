create or replace trigger pessoa_fisica_depend_atual
before insert or update on pessoa_fisica_dependente
for each row

declare
begin
if	(:new.cd_pessoa_dependente is null) and
	(:new.nm_dependente is null) then
	/*Deve ser informado o nome ou pessoa dependente*/
	Wheb_mensagem_pck.exibir_mensagem_abort(207806);
end if;
end;
/