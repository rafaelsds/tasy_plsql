CREATE OR REPLACE TRIGGER TASY_UP_PESSOA_FISICA_ATUAL
BEFORE INSERT OR UPDATE ON PESSOA_FISICA
FOR EACH ROW

BEGIN

insert	into TASY_UP_PESSOA_FISICA
	(CD_PESSOA_FISICA,
	NM_PESSOA_FISICA,
	DT_NASCIMENTO,
	IE_SEXO,
	NR_CPF,
	NR_IDENTIDADE,
	IE_STATUS,
	IE_ACAO,
	DS_MOTIVO_CANCELAMENTO)
values	(:new.cd_pessoa_fisica,
	:new.nm_pessoa_fisica,
	:new.dt_nascimento,
	:new.ie_sexo,
	:new.nr_cpf,
	:new.nr_identidade,
	'N',
	'01',
	NULL);
END;
/
