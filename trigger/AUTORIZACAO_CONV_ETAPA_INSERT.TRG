create or replace
trigger autorizacao_conv_etapa_insert
before insert on autorizacao_conv_etapa
for each row
declare

dt_inicio_vigencia_w	date;
dt_fim_vigencia_w	date;
nr_seq_autor_conv_w	number(10);
ie_param_w		varchar2(1);


pragma autonomous_transaction;

begin

if	(:new.nr_seq_autor_conv is null) and
	(:new.nr_seq_autor_cir is not null) then
	begin
	
	select	max(nr_seq_autor_conv)
	into	nr_seq_autor_conv_w
	from	autorizacao_cirurgia
	where	nr_sequencia = :new.nr_seq_autor_cir;
	
	if	(nr_seq_autor_conv_w is not null) then
		begin
		:new.nr_seq_autor_conv  := nr_seq_autor_conv_w;
		end;
	end if;
	
	end;
end if;

select	max(dt_inicio_vigencia),
	max(dt_fim_vigencia)
into	dt_inicio_vigencia_w,
	dt_fim_vigencia_w
from	autorizacao_convenio
where 	nr_sequencia	= :new.nr_seq_autor_conv;

if	(dt_inicio_vigencia_w is not null) and
	(:new.dt_inicio_vigencia is null) then
	:new.dt_inicio_vigencia	:= dt_inicio_vigencia_w;
end if;

if	(dt_fim_vigencia_w is not null) and
	(:new.dt_fim_vigencia is null) then
	:new.dt_fim_vigencia	:= dt_fim_vigencia_w;
end if;

if	(:new.nr_seq_autor_conv is not null) then
	begin
	:new.vl_autorizacao		:=  nvl(obter_valor_autorizacao(:new.nr_seq_autor_conv),0);
	exception
		when others then
		:new.vl_autorizacao		:= 0;
	end;
end if;

obter_param_usuario(3004, 116, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_param_w);

if (ie_param_w = 'S') then
	
	update	autorizacao_conv_etapa 
	set     dt_fim_etapa = sysdate 
	where   nr_seq_autor_conv = :new.nr_seq_autor_conv
	and     dt_fim_etapa is null
	and     nr_sequencia <> :new.nr_sequencia;
	
end if;

commit;

end;
/