CREATE OR REPLACE TRIGGER Protocolo_convenio_AfterUpdate
AFTER UPDATE ON Protocolo_convenio
FOR EACH ROW
DECLARE

nr_sequencia_w	Number(10,0);
ie_historico_w	Varchar2(01);
dt_referencia_w	Date;
nr_interno_conta_w	Number(10,0);
vl_conta_w		Number(15,2);
nr_atendimento_w	Number(10,0);
vl_protocolo_w		number(15,2);
ds_observacao_w		varchar2(255);
ie_grava_log_w		varchar2(01);
enviar_fat_int_w 		varchar2(1);

nr_seq_status_fat_w Number(10,0);
nr_seq_status_mob_w Number(10,0);
nr_seq_regra_fluxo_w Number(10,0);

cursor c01 is
select	nr_interno_conta,
	nr_atendimento,
	vl_conta
from	conta_paciente
where	nr_seq_protocolo	= :new.nr_seq_protocolo
and	nvl(vl_conta,0)	> 0;

cursor c02 is
	select 	nr_interno_conta
	from conta_paciente
	where nr_seq_protocolo 	= :new.nr_seq_protocolo;

BEGIN

Obter_Param_Usuario(85,141, obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento , ie_grava_log_w);
Obter_Param_Usuario(85,258, obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento , enviar_fat_int_w);

ie_historico_w	:= 'N';
if	(:new.ie_status_protocolo <> :old.ie_status_protocolo) then
	select nvl(max(ie_historico_conta),'N')
	into	ie_historico_w
	from	parametro_faturamento
	where	cd_estabelecimento	= :new.cd_estabelecimento;
	if	(ie_historico_w = 'S') then
		OPEN C01;
		LOOP
		FETCH C01 into 
			nr_interno_conta_w,
			nr_atendimento_w,
			vl_conta_w;
		exit when c01%notfound;
			select	conta_paciente_hist_seq.nextval
			into	nr_sequencia_w
			from	dual;
			insert into conta_paciente_hist(
				nr_sequencia, dt_atualizacao, nm_usuario,
				vl_conta, nr_seq_protocolo, nr_interno_conta,
				nr_nivel_anterior, nr_nivel_atual, dt_referencia,
				nr_atendimento, cd_convenio)
			values	(
				nr_sequencia_w, sysdate, :new.nm_usuario,
				vl_conta_w, :new.nr_seq_protocolo, nr_interno_conta_w,
				decode(:old.ie_status_protocolo,2, 10, 8), 
				decode(:new.ie_status_protocolo,2, 10, 8), 
				trunc(sysdate,'dd'), nr_atendimento_w, :new.cd_convenio);
		END LOOP;
		CLOSE C01;
	end if;
end if;

if	(ie_grava_log_w = 'N') or -- Se ie_grava_log_w = 'S' ent�o o sistema j� gravou o log na procedure  Atualizar_Ref_Protocolo_Conv
	(Obter_Funcao_Ativa <> 85) then

	if	(:new.DT_MESANO_REFERENCIA <> :old.DT_MESANO_REFERENCIA) then
		
		select 	nvl(obter_total_protocolo(:new.nr_seq_protocolo),0)		
		into	vl_protocolo_w
		from 	dual;
		
		ds_observacao_w	:= wheb_mensagem_pck.get_texto(298741,	'DT_MESANO_REFERENCIA_OLD_W='||to_char(:old.DT_MESANO_REFERENCIA,'dd/mm/yyyy')||
									';DT_MESANO_REFERENCIA_NEW_W='||to_char(:new.DT_MESANO_REFERENCIA,'dd/mm/yyyy'));		
		--ds_observacao_w:= 'Data de Refer�ncia ' || '(De: ' || :old.DT_MESANO_REFERENCIA || '  p/ ' || :new.DT_MESANO_REFERENCIA || ')';

		insert into protocolo_convenio_log
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_protocolo,
			ds_arquivo_envio,
			vl_protocolo,
			ie_tipo_log,
			ds_observacao)
		values	(protocolo_convenio_log_seq.NextVal,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_seq_protocolo,
			:new.ds_arquivo_envio,
			vl_protocolo_w,
			'D',
			ds_observacao_w);

	end if;
end if;

if(:old.ie_status_protocolo = 2 and :new.ie_status_protocolo = 1) then
	OPEN C02;
	LOOP
	FETCH C02 into 
		 nr_interno_conta_w;
	exit when c02%notfound;
	
	cf_retornar_status_fat(nr_interno_conta_w, 
				 'P',
				 nr_seq_status_fat_w,
				 nr_seq_status_mob_w,
				 :new.nm_usuario);
			 
	END LOOP;
	CLOSE C02;
	
end if;

WHEB_USUARIO_PCK.set_ie_commit('N');
if	((nvl(:old.ie_status_protocolo,0) <> nvl(:new.ie_status_protocolo,0))
	 and (nvl(:new.ie_status_protocolo,0) = 2) and (enviar_fat_int_w = 'S'))then
	enviar_fat_intercompany(:new.nr_seq_protocolo, wheb_usuario_pck.get_nm_usuario);
end if;
WHEB_USUARIO_PCK.set_ie_commit('S');

END;
/