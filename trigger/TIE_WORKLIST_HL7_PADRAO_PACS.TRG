create or replace TRIGGER TIE_WORKLIST_HL7_PADRAO_PACS
  BEFORE UPDATE ON PRESCR_MEDICA FOR EACH ROW
DECLARE
    QTD_PRC_W                     NUMBER(10);
    nr_prescricao_w                 prescr_procedimento.nr_prescricao%TYPE;
    nr_seq_interno_w                prescr_procedimento.nr_seq_interno%TYPE;
    nr_acess_number_w               prescr_procedimento.nr_acesso_dicom%TYPE;    
    nr_sequencia_w                  prescr_procedimento.nr_sequencia%TYPE;
    cd_proced_tasy_w                prescr_procedimento.nr_seq_proc_interno%TYPE;
    cd_proced_tasy_lado_w           VARCHAR2(200 CHAR);
    ie_lado_w                       prescr_procedimento.ie_lado%TYPE;
    dt_prev_execucao_w              prescr_procedimento.dt_prev_execucao%TYPE;
    cd_setor_execucao_w             prescr_procedimento.cd_setor_atendimento%TYPE;
    qt_prescrito_w                  prescr_procedimento.qt_procedimento%TYPE;
    dt_resultado_w                  prescr_procedimento.dt_resultado%TYPE;
    ie_suspenso_w                   prescr_procedimento.ie_suspenso%TYPE;
    cd_procedimento_w               prescr_procedimento.cd_procedimento%TYPE;
    cd_setor_atendimento_w          prescr_procedimento.cd_setor_atendimento%TYPE;
    qt_procedimento_w               prescr_procedimento.qt_procedimento%TYPE;
    ie_origem_proced_w              prescr_procedimento.ie_origem_proced%TYPE;
    cd_material_exame_w             prescr_procedimento.cd_material_exame%TYPE;
    na_accessionnumber_w            prescr_procedimento.nr_acesso_dicom%TYPE;
    nm_usuario_w                    prescr_procedimento.nm_usuario%TYPE;
    nr_seq_exame_w                  prescr_procedimento.nr_seq_exame%TYPE;
    ie_executar_leito_w             prescr_procedimento.ie_executar_leito%TYPE;
    nr_seq_proc_interno_w           prescr_procedimento.nr_seq_proc_interno%TYPE;
    ds_horarios_w                   prescr_procedimento.ds_horarios%TYPE;
    ds_dado_clinico_w               prescr_procedimento.ds_dado_clinico%TYPE;
    ie_urgencia_w                   prescr_procedimento.ie_urgencia%TYPE;
    ds_observacao_w                 prescr_procedimento.ds_observacao%TYPE;
    dt_atualizacao_w                prescr_procedimento.dt_atualizacao%TYPE;
    ds_material_especial_w          prescr_procedimento.ds_material_especial%TYPE;
    ie_amostra_entregue_w           prescr_procedimento.ie_amostra%TYPE;
    nm_pessoa_fisica_w              pessoa_fisica.nm_pessoa_fisica%TYPE;     
    ie_sexo_w                       pessoa_fisica.ie_sexo%TYPE;
    nr_prontuario_w                 pessoa_fisica.nr_prontuario%TYPE;
    dt_nascimento_w                 pessoa_fisica.dt_nascimento%TYPE;
    dt_admissao_hosp_w              pessoa_fisica.dt_admissao_hosp%TYPE;
    qt_idade_pac_w                  pessoa_fisica.dt_nascimento%TYPE;
    nr_cpf_paciente_w               pessoa_fisica.nr_cpf%TYPE;
    nr_identidade_paciente_w        pessoa_fisica.nr_identidade%TYPE;
    ds_observacao_pf_w              pessoa_fisica.ds_observacao%TYPE;
    nm_sobrenome_pai_w              pessoa_fisica.nm_sobrenome_pai%TYPE;
    nm_sobrenome_mae_w              pessoa_fisica.nm_sobrenome_mae%TYPE;
    nm_primeiro_nome_w              pessoa_fisica.nm_primeiro_nome%TYPE;
    nm_paciente_w                   pessoa_fisica.nm_pessoa_fisica%TYPE;
    nr_cpf_w                        pessoa_fisica.nr_cpf%TYPE;
    ds_procedimento_w               proc_interno.ds_proc_exame%TYPE;
    cd_integracao_proc_interno_w    proc_interno.cd_integracao%TYPE;
    ds_endereco_w                   compl_pessoa_fisica.ds_endereco%TYPE;
    cd_cep_w                        compl_pessoa_fisica.cd_cep%TYPE;
    ds_endereco_paciente_w          compl_pessoa_fisica.ds_endereco%TYPE;
    nr_endereco_paciente_w          VARCHAR2(200 CHAR);
    nr_endereco_w                   VARCHAR2(200 CHAR);
    ds_bairro_paciente_w            compl_pessoa_fisica.ds_bairro%TYPE;
    ds_bairro_w                     compl_pessoa_fisica.ds_bairro%TYPE;
    ds_municipio_paciente_w         compl_pessoa_fisica.ds_municipio%TYPE;
    ds_municipio_w                  compl_pessoa_fisica.ds_municipio%TYPE;
    uf_paciente_w                   compl_pessoa_fisica.sg_estado%TYPE;
    sg_estado_w                     compl_pessoa_fisica.sg_estado%TYPE;
    cd_cep_paciente_w               compl_pessoa_fisica.cd_cep%TYPE;
    nr_telefone_paciente_w          compl_pessoa_fisica.nr_telefone%TYPE;
    ds_complemento_w                compl_pessoa_fisica.ds_complemento%TYPE;
    nr_telefone_w                   compl_pessoa_fisica.nr_telefone%TYPE;
	ds_email_w                      compl_pessoa_fisica.ds_email%TYPE;
    cd_convenio_w                   atend_categoria_convenio.cd_convenio%TYPE;
    ds_categoria_convenio_w         categoria_convenio.ds_categoria%TYPE;
    cd_plano_convenio_w             atend_categoria_convenio.cd_plano_convenio%TYPE;
    cd_plano_w                      atend_categoria_convenio.cd_plano_convenio%TYPE;
    cd_categoria_w                  atend_categoria_convenio.cd_categoria%TYPE;
    cd_usuario_convenio_w           atend_categoria_convenio.cd_usuario_convenio%TYPE;
    cd_compl_conv_w                 atend_categoria_convenio.cd_complemento%TYPE;
    dt_validade_carteira_w          atend_categoria_convenio.dt_validade_carteira%TYPE;
    ds_plano_w                      convenio_plano.ds_plano%TYPE;
    ds_convenio_w                   convenio.ds_convenio%TYPE;
    cd_cgc_w                        convenio.cd_cgc%TYPE;
    cd_unidade_w                    VARCHAR2(200 CHAR);
    nr_crm_prescritor_w             medico.nr_crm%TYPE;
    uf_crm_prescritor_w             medico.uf_crm%TYPE;
    cd_procedencia_w                procedencia.cd_procedencia%TYPE;
    ds_procedencia_w                procedencia.ds_procedencia%TYPE;
    cd_tipo_procedimento_w          procedimento.cd_tipo_procedimento%TYPE;
    ds_motivo_atend_w               queixa_paciente.ds_queixa%TYPE;
    ds_motivo_suspensao_w           motivo_suspensao_prescr.ds_motivo%TYPE;
    ds_setor_paciente_w             setor_atendimento.ds_setor_atendimento%TYPE;
    ie_tipo_atendimento_w           atendimento_paciente.ie_tipo_atendimento%TYPE;
    cd_estab_atend_w                atendimento_paciente.cd_estabelecimento%TYPE;
    dt_entrada_w                    atendimento_paciente.dt_entrada%TYPE;
    ds_tipo_atendimento_w           VARCHAR2(200 CHAR);
    nr_seq_motivo_atend_w           atendimento_paciente.nr_seq_queixa%TYPE;
    ds_senha_w                      atendimento_paciente.ds_senha%TYPE;
    cd_agenda_w                     agenda_paciente.cd_agenda%TYPE;
    ds_agenda_w                     agenda.ds_agenda%TYPE;
    ds_unidade_atend_w              unidade_atendimento.ds_unidade_atend%TYPE;
    ds_local_w                      VARCHAR2(200 CHAR);
    ds_modalidade_w                 VARCHAR2(200 CHAR);
    nm_medico_w                     pessoa_fisica.nm_pessoa_fisica%TYPE;
    nr_cpf_medico_w                 pessoa_fisica.nr_cpf%TYPE;


   CURSOR c01 IS  
        SELECT  a.nr_prescricao,
			a.nr_seq_interno,
			a.nr_acesso_dicom,
            a.nr_sequencia,
            a.nr_seq_proc_interno,
            (a.nr_seq_proc_interno||a.ie_lado),
            a.ie_lado,
            a.dt_prev_execucao,
            a.cd_setor_atendimento,
            a.qt_procedimento,
            a.dt_resultado,
            a.ie_suspenso,
            a.cd_procedimento,
            a.cd_setor_atendimento,
            a.qt_procedimento,
            a.ie_origem_proced,
            a.cd_material_exame,
            a.nr_acesso_dicom,
            a.nm_usuario,
            a.nr_seq_exame,
            a.ie_executar_leito,
            a.nr_seq_proc_interno,
            a.ds_horarios,
            elimina_caractere_especial(a.ds_dado_clinico),
            nvl(a.ie_urgencia,'N'),
            elimina_caractere_especial(a.ds_observacao),
			a.dt_atualizacao,
            elimina_caractere_especial(a.ds_material_especial),
			nvl(a.ie_amostra,'N'),
			elimina_caractere_especial(c.nm_pessoa_fisica),
			c.ie_sexo,
			c.nr_prontuario,
			c.dt_nascimento,
            c.dt_admissao_hosp,
           /*obter_idade(c.dt_nascimento, nvl(c.dt_obito,sysdate),'A') qt_idade_pac,*/
            c.nr_cpf,
            c.nr_identidade,
            elimina_caractere_especial(c.ds_observacao),
            elimina_caractere_especial(c.nm_sobrenome_pai),
            elimina_caractere_especial(c.nm_sobrenome_mae),
            elimina_caractere_especial(c.nm_primeiro_nome),
            elimina_caractere_especial(c.nm_pessoa_fisica),
            c.nr_cpf,
            elimina_caractere_especial(d.ds_proc_exame),
            d.cd_integracao,
			elimina_caractere_especial(g.ds_endereco),
			g.cd_cep,
            elimina_caractere_especial(g.ds_endereco),
            nvl(g.nr_endereco, g.ds_compl_end),
            nvl(g.nr_endereco, g.ds_compl_end),
			g.ds_bairro,
            elimina_caractere_especial(g.ds_bairro),
            elimina_caractere_especial(g.ds_municipio),
            elimina_caractere_especial(g.ds_municipio),
            g.sg_estado,
            g.sg_estado,
            g.cd_cep,
			decode(g.nr_ddd_telefone,null,g.nr_telefone,g.nr_ddd_telefone||' '||g.nr_telefone),
			elimina_caractere_especial(g.ds_complemento),
            g.nr_telefone,
			g.ds_email,
            h.cd_convenio,
            elimina_caractere_especial(substr(obter_categoria_convenio(h.cd_convenio,h.cd_categoria),1,100)),
			h.cd_plano_convenio,
            h.cd_plano_convenio,
            h.cd_categoria,
            h.cd_usuario_convenio,
            h.cd_complemento,
            h.dt_validade_carteira,
			elimina_caractere_especial(substr(obter_desc_plano_conv(h.cd_convenio, h.cd_plano_convenio),1,255)),
            elimina_caractere_especial(i.ds_convenio),
            i.cd_cgc,
            j.cd_unidade_basica || ' ' || j.cd_unidade_compl,
            m.nr_crm,
            m.uf_crm,
            nvl(n.cd_procedencia_externo, n.cd_procedencia),
            elimina_caractere_especial(n.ds_procedencia),
            p.cd_tipo_procedimento,
            elimina_caractere_especial(substr(q.ds_queixa,1,80)),
            elimina_caractere_especial(substr(r.ds_motivo,1,255)),
            elimina_caractere_especial(s.ds_setor_atendimento),
  			t.ie_tipo_atendimento,
            t.cd_estabelecimento,
            t.dt_entrada,
            elimina_caractere_especial(substr(obter_valor_dominio(12,t.ie_tipo_atendimento),1,255)),
			t.nr_seq_queixa,
			t.ds_senha,
            (select  x.cd_agenda from   agenda_paciente x where   x.nr_sequencia = a.nr_seq_agenda ),
            (select  elimina_caractere_especial(x.ds_agenda) from agenda x, agenda_paciente y 
                 where x.cd_agenda = y.cd_agenda and  y.nr_sequencia = a.nr_seq_agenda),
			(select elimina_caractere_especial(max(z.ds_unidade_atend)) from unidade_atendimento z 
                where z.nr_atendimento =  :NEW.nr_atendimento),
            elimina_caractere_especial((decode(obter_classif_setor(a.cd_setor_atendimento), 1, 
                substr(obter_nome_setor(a.cd_setor_atendimento) || ' ' 	|| obter_desc_abrev_local_pa(t.nr_seq_local_pa),1,80), 5, 
                decode(t.nr_seq_local_pa, null, substr(obter_unidade_atendimento(:NEW.nr_atendimento, 'IAA', 'SU'),1,80), 
                substr(obter_nome_setor(a.cd_setor_atendimento) || ' ' || obter_desc_abrev_local_pa(t.nr_seq_local_pa),1,80)),
                substr(obter_unidade_atendimento(:NEW.nr_atendimento, 'IAA', 'SU'),1,80)))),
            elimina_caractere_especial(decode(nvl(obter_se_modalidade_propria(:NEW.cd_estabelecimento), 'N'), 'N', 
                obter_modalidade_wtt(p.cd_tipo_procedimento), obter_modalidade_propria(p.cd_tipo_procedimento))),
            elimina_caractere_especial(mp.nm_pessoa_fisica),
            mp.nr_cpf
		FROM prescr_procedimento a,
             pessoa_fisica c,
             proc_interno d,
             compl_pessoa_fisica g,
             atend_categoria_convenio h,
             convenio i,
             atend_paciente_unidade j,
             medico m,
             procedencia n,
             procedimento p,
             queixa_paciente q,
             motivo_suspensao_prescr r,
             setor_atendimento s,
			 atendimento_paciente t,
             pessoa_fisica mp
		WHERE a.nr_prescricao               = :new.nr_prescricao
			AND     c.cd_pessoa_fisica      = :new.cd_pessoa_fisica
			AND     d.nr_sequencia          = a.nr_seq_proc_interno
			AND     p.cd_procedimento       = a.cd_procedimento
			AND     p.ie_origem_proced      = a.ie_origem_proced
			AND  	h.cd_convenio           = i.cd_convenio
			AND   	j.nr_seq_interno        = obter_atepacu_paciente(:new.nr_atendimento,'A')
			AND  	t.nr_atendimento        = :new.nr_atendimento
			AND  	h.nr_atendimento(+)     = :new.nr_atendimento
			AND  	t.cd_procedencia        = n.cd_procedencia
			AND   	j.cd_setor_atendimento  = s.cd_setor_atendimento
			AND  	h.dt_inicio_vigencia  = (SELECT MAX(w.dt_inicio_vigencia)
						FROM atend_categoria_convenio w
						WHERE w.nr_atendimento  = :new.nr_atendimento)
			AND     m.cd_pessoa_fisica(+)   = :new.cd_prescritor
			AND  	c.cd_pessoa_fisica      = g.cd_pessoa_fisica(+)
			AND  	mp.cd_pessoa_fisica(+)  = :new.cd_medico
			AND  	g.ie_tipo_complemento(+)= 1
			AND     a.cd_motivo_baixa       = 0
			AND  	a.cd_motivo_suspensao   = r.nr_sequencia(+)
			AND   	t.nr_seq_queixa         = q.nr_sequencia(+)
			AND     a.dt_integracao         IS NULL
			AND     a.dt_suspensao          IS NULL
            AND     a.nr_prescricao         = :new.nr_prescricao
            AND     obter_se_integr_proc_interno(a.nr_seq_proc_interno, 30 /*Carestream padrao HL7*/, null, a.ie_lado, :new.cd_estabelecimento) = 'S';

	PROCEDURE PROCESS_EVENT_SEND_WORKLIST IS
		JSON_W                  PHILIPS_JSON := PHILIPS_JSON();
		JSON_PID_W              PHILIPS_JSON := PHILIPS_JSON();
		JSON_PV1_W              PHILIPS_JSON := PHILIPS_JSON();
        JSON_IN1_W              PHILIPS_JSON := PHILIPS_JSON();
		JSON_ORC_W              PHILIPS_JSON := PHILIPS_JSON();
        JSON_OBR_W              PHILIPS_JSON := PHILIPS_JSON();
		JSON_LIST_ORC_W         PHILIPS_JSON_LIST := PHILIPS_JSON_LIST();
		JSON_DATA_W              CLOB;      

		BEGIN
			JSON_W.PUT('sendingApplication', 'TASY');
			JSON_W.PUT('sendingFacility', 'PHILIPS');
			JSON_W.PUT('receivingApplication', 'VUEPACS');
			JSON_W.PUT('receivingFacility', 'PHILIPS');
			JSON_W.PUT('processingID', 'P');
            JSON_W.PUT('characterSet', '8859/1');

			JSON_PID_W.PUT('internalID', :NEW.cd_pessoa_fisica);
            JSON_PID_W.PUT('assigningAuthority', :NEW.cd_estabelecimento);
            JSON_PID_W.PUT('familyName', nm_sobrenome_pai_w);
            JSON_PID_W.PUT('givenName', nm_primeiro_nome_w);
            JSON_PID_W.PUT('familyNameMother', nm_sobrenome_mae_w);
            JSON_PID_W.PUT('dateOfBirth', TO_CHAR(dt_nascimento_w, 'MM/DD/YYYY'));
            JSON_PID_W.PUT('sex', ie_sexo_w);
            JSON_PID_W.PUT('streetAddress', ds_endereco_w ||' - '|| nr_endereco_paciente_w ||' - '|| ds_complemento_w);
            JSON_PID_W.PUT('otherDesignation', ds_bairro_paciente_w);
            JSON_PID_W.PUT('city', ds_municipio_paciente_w);
            JSON_PID_W.PUT('stateOrProvince', uf_paciente_w);
            JSON_PID_W.PUT('telephoneNumberHome', nr_telefone_paciente_w);
            JSON_PID_W.PUT('emailAddressPatient', ds_email_w);
            JSON_PID_W.PUT('telephoneNumberWork',nr_telefone_w);
            JSON_PID_W.PUT('SSNnumberPatient', nr_cpf_paciente_w);
            JSON_PID_W.PUT('licenseNumber', nr_identidade_paciente_w);

			JSON_W.PUT('PID', JSON_PID_W.TO_JSON_VALUE());

			JSON_PV1_W.PUT('patientClass', ie_tipo_atendimento_w);
            JSON_PV1_W.PUT('idNumberDoctorSoliPV1',nr_crm_prescritor_w);
            JSON_PV1_W.PUT('familyNameDoctorSoliPV1', Obter_Parte_Nome_phillips_pf(nm_medico_w,'sobrenome'));
            JSON_PV1_W.PUT('givenNameDoctorSoliPV1', Obter_Parte_Nome_phillips_pf(nm_medico_w,'nome'));
            JSON_PV1_W.PUT('namespaceIdEstabPV1', :NEW.cd_estabelecimento);
            JSON_PV1_W.PUT('identifierCheckDigitPV1',nr_crm_prescritor_w);
            JSON_PV1_W.PUT('namespaceIdUfCRMPV1',uf_crm_prescritor_w);
            JSON_PV1_W.PUT('idNumberVisitNumber', :new.nr_atendimento);
            JSON_PV1_W.PUT('assigningAuthorityIdEstab', :NEW.cd_estabelecimento);
            JSON_PV1_W.PUT('accountStatusDtAtend', TO_CHAR(dt_entrada_w, 'MM/DD/YYYY HH24:MI:SS'));

			JSON_W.PUT('PV1', JSON_PV1_W.TO_JSON_VALUE());

            JSON_IN1_W.PUT('identifierPlanHealth', cd_plano_convenio_w);
            JSON_IN1_W.PUT('textPlanHealth', ds_plano_w);
            JSON_IN1_W.PUT('idConvenioCard', cd_usuario_convenio_w);
            JSON_IN1_W.PUT('organizationName', ds_convenio_w);
            JSON_IN1_W.PUT('idNumberOrganization', cd_convenio_w);

            JSON_W.PUT('IN1', JSON_IN1_W.TO_JSON_VALUE());

			JSON_ORC_W.PUT('orderControlORC', 'NW');
            JSON_ORC_W.PUT('entityIdentifierIdPedidoORC', nr_prescricao_w);
            JSON_ORC_W.PUT('entityIdentifierAcessNumberORC', nr_acess_number_w);
            JSON_ORC_W.PUT('namespaceIdEstabORC', :NEW.cd_estabelecimento);
            JSON_ORC_W.PUT('orderStatusUF', 'SC');
            JSON_ORC_W.PUT('quantityTiming', '1');
            JSON_ORC_W.PUT('endDateTime',TO_CHAR(dt_prev_execucao_w, 'MM/DD/YYYY HH24:MI:SS'));
            JSON_ORC_W.PUT('priority','R');
            JSON_ORC_W.PUT('personIdentifier', cd_setor_atendimento_w);
            JSON_ORC_W.PUT('namespaceIdEstab', :NEW.cd_estabelecimento);

			JSON_LIST_ORC_W.APPEND(JSON_ORC_W.TO_JSON_VALUE());
			JSON_W.PUT('ORC', JSON_LIST_ORC_W);

            JSON_OBR_W.PUT('entityIdentifierIdPedidoOBR', nr_prescricao_w);
            JSON_OBR_W.PUT('entityIdentifierAcessNumberOBR', NR_ACESS_NUMBER_W);
            JSON_OBR_W.PUT('namespaceIdEstabOBR', :NEW.cd_estabelecimento);
            JSON_OBR_W.PUT('identifierIdExam', nvl(nr_seq_exame_w, cd_procedimento_w));
            JSON_OBR_W.PUT('textExam', ds_procedimento_w);
            JSON_OBR_W.PUT('nameOfCodingSystem', :NEW.cd_estabelecimento);
            JSON_OBR_W.PUT('idNumberDoctorSoliOBR',:new.cd_medico);
            JSON_OBR_W.PUT('familyNameDoctorSoliOBR', Obter_Parte_Nome_phillips_pf(nm_medico_w,'sobrenome'));
            JSON_OBR_W.PUT('givenNameDoctorSoliOBR', Obter_Parte_Nome_phillips_pf(nm_medico_w,'nome'));
            JSON_OBR_W.PUT('namespaceIdEstabOBR2', :NEW.cd_estabelecimento);
            JSON_OBR_W.PUT('identifierCheckDigitOBR', nr_crm_prescritor_w);
            JSON_OBR_W.PUT('namespaceIdUfCRMOBR',uf_crm_prescritor_w);
            JSON_OBR_W.PUT('namespaceId', nr_acess_number_w);
            JSON_OBR_W.PUT('placerField2', nr_seq_interno_w);
            JSON_OBR_W.PUT('fillerField1MoreIdPedido',nr_prescricao_w);
            JSON_OBR_W.PUT('fillerField2More', nr_sequencia_w);
            JSON_OBR_W.PUT('diagnosticServSectID', ds_modalidade_w);
            JSON_OBR_W.PUT('quantityOBR', '1');
            JSON_OBR_W.PUT('startDateTimeOBR',TO_CHAR(dt_prev_execucao_w, 'MM/DD/YYYY HH24:MI:SS'));
            JSON_OBR_W.PUT('priorityOBR','R');
            JSON_OBR_W.PUT('endDateTimeOBR',TO_CHAR(dt_resultado_w, 'MM/DD/YYYY HH24:MI:SS'));

            JSON_W.PUT('OBR', JSON_OBR_W.TO_JSON_VALUE());

			SYS.DBMS_LOB.CREATETEMPORARY(JSON_DATA_W, TRUE);
			JSON_W.TO_CLOB(JSON_DATA_W);
            
			JSON_DATA_W := BIFROST.SEND_INTEGRATION_CONTENT('send.hl7.default.pacs.worklist', JSON_DATA_W, :NEW.NM_USUARIO);
	END PROCESS_EVENT_SEND_WORKLIST;


 
	BEGIN
		IF(:new.dt_liberacao IS NOT NULL AND :old.dt_liberacao IS NULL) THEN
			SELECT COUNT(*)
			INTO qtd_prc_w
			FROM prescr_procedimento pp
			WHERE pp.nr_prescricao = :new.nr_prescricao
				AND obter_se_integr_proc_interno(pp.nr_seq_proc_interno, 30 /*Carestream padrao HL7*/, null, pp.ie_lado, :new.cd_estabelecimento) = 'S'
				AND pp.nr_seq_exame IS NULL
				AND ROWNUM <= 1;

			IF (qtd_prc_w > 0) THEN

				OPEN c01;
					LOOP
						FETCH c01 INTO
                            nr_prescricao_w,    
                            nr_seq_interno_w,
                            nr_acess_number_w,
                            nr_sequencia_w,
							cd_proced_tasy_w,
							cd_proced_tasy_lado_w,
							ie_lado_w,
							dt_prev_execucao_w,
							cd_setor_execucao_w,
							qt_prescrito_w,
							dt_resultado_w,
							ie_suspenso_w,
							cd_procedimento_w,
							cd_setor_atendimento_w,
							qt_procedimento_w,
							ie_origem_proced_w,
							cd_material_exame_w,
							na_accessionnumber_w,
							nm_usuario_w,
							nr_seq_exame_w,
							ie_executar_leito_w,
							nr_seq_proc_interno_w,
							ds_horarios_w,
							ds_dado_clinico_w,
							ie_urgencia_w,
							ds_observacao_w,
							dt_atualizacao_w,
							ds_material_especial_w,
							ie_amostra_entregue_w,
                            nm_pessoa_fisica_w,
                            ie_sexo_w,
                            nr_prontuario_w,
                            dt_nascimento_w,      
                            dt_admissao_hosp_w,
                           /* qt_idade_pac_w,*/
							nr_cpf_paciente_w,
							nr_identidade_paciente_w,
							ds_observacao_pf_w,
							nm_sobrenome_pai_w,
							nm_sobrenome_mae_w,
							nm_primeiro_nome_w,
							nm_paciente_w,
							nr_cpf_w,
							ds_procedimento_w,
							cd_integracao_proc_interno_w,
                            ds_endereco_w,         
                            cd_cep_w,    
                            ds_endereco_paciente_w,
							nr_endereco_paciente_w,
							nr_endereco_w,
							ds_bairro_paciente_w,
							ds_bairro_w,
							ds_municipio_paciente_w,
							ds_municipio_w,
							uf_paciente_w,
							sg_estado_w,
							cd_cep_paciente_w,
							nr_telefone_paciente_w,
							ds_complemento_w,
							nr_telefone_w,
							ds_email_w,
							cd_convenio_w,
							ds_categoria_convenio_w,
							cd_plano_convenio_w,
							cd_plano_w,
							cd_categoria_w,
							cd_usuario_convenio_w,
							cd_compl_conv_w,
							dt_validade_carteira_w,
							ds_plano_w,
							ds_convenio_w,
							cd_cgc_w,
							cd_unidade_w,
							nr_crm_prescritor_w,
							uf_crm_prescritor_w,
							cd_procedencia_w,
							ds_procedencia_w,
							cd_tipo_procedimento_w,
							ds_motivo_atend_w,
							ds_motivo_suspensao_w,
							ds_setor_paciente_w,
                            ie_tipo_atendimento_w,
                            cd_estab_atend_w,
                        	dt_entrada_w,
							ds_tipo_atendimento_w,
							nr_seq_motivo_atend_w,
							ds_senha_w,
							cd_agenda_w,
							ds_agenda_w,
							ds_unidade_atend_w,
							ds_local_w,
							ds_modalidade_w,
							nm_medico_w,
							nr_cpf_medico_w;
						EXIT WHEN c01%notfound;
							BEGIN
								PROCESS_EVENT_SEND_WORKLIST;
							END;
					END LOOP;
				close c01;
			END IF;
		END IF;
END TIE_WORKLIST_HL7_PADRAO_PACS;
/