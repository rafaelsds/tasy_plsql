create or replace trigger reg_des_requisito_before
	before update on des_requisito
for each row 
declare
	ie_html5_w	varchar(1);
	nr_seq_pr_w	reg_product_requirement.nr_sequencia%type;
begin
	if	(:old.dt_aprovacao is null) and (:new.dt_aprovacao is not null) then
		select
			case
				when ie_html5           = 'S'
				or (nvl(ie_java, 'N')   = 'N'
				and nvl(ie_delphi, 'N') = 'N')
				then 'S'
				else ie_html5
			end
		into ie_html5_w
		from proj_projeto
		where nr_sequencia = :new.nr_seq_projeto;
		
		if	ie_html5_w = 'S' then

			select	max(pr.nr_sequencia)
			into	nr_seq_pr_w
			from	des_requisito_item ri
			left join reg_caso_teste tc on tc.nr_seq_product = ri.nr_seq_pr
			join reg_product_requirement pr on pr.nr_sequencia = ri.nr_seq_pr
			where	ri.nr_seq_requisito	= :new.nr_sequencia
			and	tc.nr_sequencia is null;
			
			if	nr_seq_pr_w is not null then
				wheb_mensagem_pck.exibir_mensagem_abort(997073);
			end if;
		end if;
	end if;
end;
/