create or replace trigger
reg_service_pack_after 
before insert on reg_service_pack
for each row
declare

begin

	select	ds_version,
		ds_build,
		ds_hotfix
	into	:new.cd_versao,
		:new.cd_build,
		:new.ds_hotfix
	from	table(phi_ordem_servico_pck.get_version_info2(:new.ds_version));

end reg_service_pack_after;
/
