create or replace trigger pls_conta_pos_estab_contab_alt
before update on pls_conta_pos_estab_contab
for each row
 
declare

ds_log_call_w			Varchar2(1500);
ds_observacao_w			Varchar2(4000);
nr_seq_prest_pgto_resumo_w	pls_conta_medica_resumo.nr_seq_prestador_pgto%type;

begin

	if (nvl(:new.nr_seq_conta_resumo, -1) != nvl(:old.nr_seq_conta_resumo, -1)) then

		select 	max(nr_seq_prestador_pgto)
		into	nr_seq_prest_pgto_resumo_w
		from	pls_conta_medica_resumo
		where	nr_seq_conta = :new.nr_seq_conta
		and	nr_sequencia = :new.nr_seq_conta_resumo;
		
		if ( nvl(:new.nr_seq_prestador_pgto, -1) != nvl(nr_seq_prest_pgto_resumo_w,-1)) then
	
			ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

			ds_observacao_w := 'Conta: '||:new.nr_seq_conta;	
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Conta resumo anterior: '||:old.nr_seq_conta_resumo ||chr(13)||chr(10)||
						'Conta resumo novo: '||:new.nr_seq_conta_resumo ||chr(13)||chr(10)||
						'Prestador pagto contab: '||:new.nr_seq_prestador_pgto||' Prestador pagto resumo: '||nr_seq_prest_pgto_resumo_w||chr(13)||chr(10);
						
			insert into plsprco_cta(nr_sequencia, dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
						ds_log, ds_log_call, ds_funcao_ativa, 
						ie_aplicacao_tasy, nm_maquina, ie_opcao,
						nr_seq_conta)
			values (plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
				sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_CONTA_POS_ESTAB', 
				ds_observacao_w, ds_log_call_w, obter_funcao_ativa, 
				pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, 'POS',
				:old.nr_seq_conta);
		end if;

	end if;
	    

end;
/