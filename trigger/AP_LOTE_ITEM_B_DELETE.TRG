create or replace trigger ap_lote_item_b_delete
before delete on ap_lote_item
for each row
declare

begin

if  (wheb_usuario_pck.get_ie_executar_trigger  = 'N')  then
  goto Fim;
end if;

gravar_log_tasy(1712,substr('Delete item=' || :new.nr_sequencia || ' mat hor=' || :new.nr_seq_mat_hor ||
' Stack=' || dbms_utility.format_call_stack,1,2000),:new.nr_seq_lote);
<<Fim>>
null;
end;
/