create or replace trigger pls_conta_copartic_alt
before update or insert on pls_conta_coparticipacao
for each row

declare

ds_log_call_w 		Varchar2(1500);
ds_observacao_w		Varchar2(2000);
ie_opcao_w		Varchar2(3);
nr_seq_conta_w		pls_conta_proc.nr_sequencia%type;

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	(:new.nr_seq_regra_exclusao is not null) and
		(:new.vl_coparticipacao	> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(259055);
	end if;
end if;


if	( updating )then	 
	 

	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
	
	if	( pls_se_aplicacao_tasy = 'N') then
		ds_observacao_w	:= 'Alteração efetuada via update fora do Tasy.';
		if	(nvl(:new.vl_coparticipacao,0) <> nvl(:old.vl_coparticipacao,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Valor coparticipação: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.vl_coparticipacao||' - Modificado: '||:new.vl_coparticipacao||chr(13)||chr(10);
		end if;
		
		if	(nvl(:new.nr_seq_mensalidade_seg,0) <> nvl(:old.nr_seq_mensalidade_seg,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Seq mensalidade seg.: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_mensalidade_seg||' - Modificado: '||:new.nr_seq_mensalidade_seg||chr(13)||chr(10);
		end if;
		
		if	(nvl(:new.ie_status_mensalidade,'X') <> nvl(:old.ie_status_mensalidade,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Status mensalidade: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_status_mensalidade||' - Modificado: '||:new.ie_status_mensalidade||chr(13)||chr(10);
		end if;
		
		if	(nvl(:new.ie_status_coparticipacao,'X') <> nvl(:old.ie_status_coparticipacao,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Status coparticipação alterado fora do tasy: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_status_coparticipacao||' - Modificado: '||:new.ie_status_coparticipacao||chr(13)||chr(10);
		end if;
		
		if	(nvl(:new.ie_gerar_mensalidade,'X') <> nvl(:old.ie_gerar_mensalidade,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Mensalidade gerada: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_gerar_mensalidade||' - Modificado: '||:new.ie_gerar_mensalidade||chr(13)||chr(10);
		end if;
	end if;
	
	if ((:old.dt_estorno is null) and (:new.dt_estorno is not null)) then
	
		ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'dt_estorno: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.dt_estorno||' - Modificado: '||:new.dt_estorno||chr(13)||chr(10)||
							'Status coparticipação: '||chr(13)||chr(10)||
								chr(9)||'Anterior: '||:old.ie_status_coparticipacao||' - Modificado: '||:new.ie_status_coparticipacao||chr(13)||chr(10)||
							'Status mensalidade: '||chr(13)||chr(10)||
								chr(9)||'Anterior(cancelada): '||:old.ie_status_mensalidade||' - Modificado: '||:new.ie_status_mensalidade||chr(13)||chr(10);	
							
	
	else
	
		if	( (:new.ie_status_coparticipacao = 'N') and   (:new.ie_status_coparticipacao <> nvl(:old.ie_status_coparticipacao,'N'))) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
								'Status coparticipação: '||chr(13)||chr(10)||
								chr(9)||'Anterior: '||:old.ie_status_coparticipacao||' - Modificado: '||:new.ie_status_coparticipacao||chr(13)||chr(10);
		end if;
		
		if	((nvl(:new.ie_status_mensalidade,'X') <> nvl(:old.ie_status_mensalidade,'X')) and (nvl(:old.ie_status_mensalidade,'X') = 'C')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
								'Status mensalidade: '||chr(13)||chr(10)||
								chr(9)||'Anterior(cancelada): '||:old.ie_status_mensalidade||' - Modificado: '||:new.ie_status_mensalidade||chr(13)||chr(10);
		end if;
		
		if	(nvl(:new.nr_seq_conta,0) <> nvl(:old.nr_seq_conta,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Conta: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_conta||' - Modificado: '||:new.nr_seq_conta||chr(13)||chr(10);
		end if;
		
		if	(nvl(:new.nr_seq_conta_proc,0) <> nvl(:old.nr_seq_conta_proc,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Conta proc: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_conta_proc||' - Modificado: '||:new.nr_seq_conta_proc||chr(13)||chr(10);
		end if;
		
		if	(nvl(:new.nr_seq_conta_mat,0) <> nvl(:old.nr_seq_conta_mat,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Conta mat: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_conta_mat||' - Modificado: '||:new.nr_seq_conta_mat||chr(13)||chr(10);
		end if;
		
	end if;
	
	if	(ds_observacao_w is not null) then
		insert	into	pls_conta_copartic_log
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_conta_copartic,nr_seq_conta,nr_seq_conta_proc,nr_seq_conta_mat,vl_coparticipacao,
				vl_coparticipacao_old,nr_seq_mensalidade_seg,nr_seq_mensalidade_seg_old,ie_status_mensalidade,ie_status_mensalidade_old,
				ie_status_coparticipacao,ie_status_coparticipacao_old,ie_gerar_mensalidade,ie_gerar_mensalidade_old,
				nm_maquina,ds_log_call,ds_log,
				ie_tipo_registro)
		values	(	pls_conta_copartic_log_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
				:new.nr_sequencia,:new.nr_seq_conta,:new.nr_seq_conta_proc,:new.nr_seq_conta_mat,:new.vl_coparticipacao,
				:old.vl_coparticipacao,:new.nr_seq_mensalidade_seg,:old.nr_seq_mensalidade_seg,:new.ie_status_mensalidade,:old.ie_status_mensalidade,
				:new.ie_status_coparticipacao,:old.ie_status_coparticipacao,:new.ie_gerar_mensalidade,:old.ie_gerar_mensalidade,
				wheb_usuario_pck.get_machine,ds_log_call_w,substr(ds_observacao_w,1,2000),
				'A');
	end if;
end if;



if	(inserting) then
	if	(:new.nr_seq_conta_proc is not null) then
		select	max(nr_seq_conta)
		into	nr_seq_conta_w
		from	pls_conta_proc
		where	nr_sequencia	= :new.nr_seq_conta_proc;
		
		if	(nr_seq_conta_w <> :new.nr_seq_conta) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Conta insert: '||chr(13)||chr(10)||
						chr(9)||'Conta proc: '||nr_seq_conta_w||' - Conta copart: '||:new.nr_seq_conta||chr(13)||chr(10);
		end if;
	end if;
	
	if	(:new.nr_seq_conta_mat is not null) then
		select	max(nr_seq_conta)
		into	nr_seq_conta_w
		from	pls_conta_mat
		where	nr_sequencia	= :new.nr_seq_conta_mat;
		
		if	(nr_seq_conta_w <> :new.nr_seq_conta) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Conta insert: '||chr(13)||chr(10)||
						chr(9)||'Conta mat: '||nr_seq_conta_w||' - Conta copart: '||:new.nr_seq_conta||chr(13)||chr(10);
		end if;
	end if;
	
	if	(ds_observacao_w is not null) then
		
		ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
	
		insert	into	pls_conta_copartic_log
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_conta_copartic,nr_seq_conta,nr_seq_conta_proc,nr_seq_conta_mat,
				nm_maquina,ds_log_call,ds_log,
				ie_tipo_registro)
		values	(	pls_conta_copartic_log_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
				:new.nr_sequencia,:new.nr_seq_conta,:new.nr_seq_conta_proc,:new.nr_seq_conta_mat,
				wheb_usuario_pck.get_machine,ds_log_call_w,substr(ds_observacao_w,1,2000),
				'A');
	end if;
	
end if;

end;
/
