CREATE OR REPLACE TRIGGER HNSG_TITULO_RECEBER_INSERT
BEFORE INSERT ON Titulo_Receber
FOR EACH ROW

DECLARE

vl_protocolo_w		number(15,2);
nr_seq_lote_protocolo_w	number(10);

BEGIN

select	obter_total_protocolo(max(a.nr_seq_protocolo)),
	max(a.nr_seq_lote_protocolo)
into	vl_protocolo_w,
	nr_seq_lote_protocolo_w
from	protocolo_convenio a
where	a.nr_seq_protocolo	= :new.nr_seq_protocolo;

if	(nr_seq_lote_protocolo_w is not null) and
	(vl_protocolo_w <> :new.vl_titulo) then

	raise_application_error(-20011,	'O valor do t�tulo � diferente do valor do protocolo!' || chr(13) || chr(10) ||
					'T�tulo: ' || :new.nr_titulo || '. Valor: ' || :new.vl_titulo || chr(13) || chr(10) ||
					'. Protocolo: ' || :new.nr_seq_protocolo || '. Valor: ' || vl_protocolo_w || chr(13) || chr(10) ||
					'Favor retornar print para a Wheb na OS 406571. Respons�vel Marcelo Rodrigo Polli');

end if;

END;
/