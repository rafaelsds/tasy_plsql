create or replace trigger gerencia_wheb_after_update
after update on gerencia_wheb  
for each row 
declare

    nr_seq_grupo_des_w number(10);
    nr_seq_usu_grupo_des_w number(10);
    nr_seq_grupo_sup_w number(10);
    nr_seq_usu_grupo_sup_w number(10);
    
    cursor c01(nr_seq_w in number) is
    select *
      from grupo_suporte
     where nr_seq_gerencia_sup = nr_seq_w;

    cursor c02(nr_seq_w in number) is
    select *
      from grupo_desenvolvimento
     where nr_seq_gerencia = nr_seq_w;
     
    cursor c03(nr_seq_w in number) is
    select *
      from usuario_grupo_sup
     where nr_seq_grupo = nr_seq_w;
     
    cursor c04(nr_seq_w in number) is
    select *
      from usuario_grupo_des
     where nr_seq_grupo = nr_seq_w;

    r01 c01%rowtype;
    r02 c02%rowtype;
    r03 c03%rowtype;
    r04 c04%rowtype;

begin

    if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
		null;
	end if;

    if (updating) and (:old.ie_tipo_grupo <> :new.ie_tipo_grupo) then

        if (:old.ie_tipo_grupo = 'S' and :new.ie_tipo_grupo = 'D') then

            update grupo_suporte
               set nr_seq_grupo_gerencia = null
             where nr_seq_gerencia_sup = :new.nr_sequencia;

            delete from gerencia_wheb_grupo_usu u
             where u.nr_seq_grupo in (select g.nr_sequencia
                                        from gerencia_wheb_grupo g
                                       where g.nr_seq_gerencia = :new.nr_sequencia);
            
            delete from gerencia_wheb_grupo
                 where nr_seq_gerencia = :old.nr_sequencia;

            for r01 in c01(:new.nr_sequencia) loop
            
                select grupo_desenvolvimento_seq.nextval
                  into nr_seq_grupo_des_w
                  from dual;
            
                insert into grupo_desenvolvimento
                (CD_EXP_GRUPO,
                DS_GRUPO,
                DS_GRUPO_EXTERNO,
                DS_TIME_ZONE,
                DT_ATENDIMENTO_FIM,
                DT_ATENDIMENTO_INI,
                DT_ATUALIZACAO,
                DT_ATUALIZACAO_NREC,
                IE_GERENCIA,
                IE_PREV_DIA,
                IE_SITUACAO,
                NM_USUARIO,
                NM_USUARIO_LIDER,
                NM_USUARIO_MANUTENCAO,
                NM_USUARIO_NEGONIO,
                NM_USUARIO_NREC,
                NR_SEQ_ESTAGIO_PADRAO,
                NR_SEQ_GERENCIA,
                NR_SEQ_GERENCIA_SUP,
                NR_SEQ_GRUPO_GERENCIA,
                NR_SEQUENCIA,
                NR_SEQ_VERSAO,
                QT_DIA_PREV_OS)
                values
                (r01.CD_EXP_GRUPO,
                r01.DS_GRUPO,
                null,
                null,
                null,
                null,
                sysdate,
                sysdate,
                null,
                null,
                r01.IE_SITUACAO,
                r01.NM_USUARIO,
                r01.NM_USUARIO_LIDER,
                null,
                null,
                r01.NM_USUARIO_NREC,
                null,
                r01.NR_SEQ_GERENCIA_SUP,
                null,
                r01.NR_SEQ_GRUPO_GERENCIA,
                nr_seq_grupo_des_w,
                null,
                r01.QT_DIA_PREV_OS);
                
                for r03 in c03(r01.nr_sequencia) loop
                
                    select usuario_grupo_des_seq.nextval
                      into nr_seq_usu_grupo_des_w
                      from dual;

                    insert into usuario_grupo_des
                    (DT_ATUALIZACAO,
                    DT_ATUALIZACAO_NREC,
                    DT_FIM_VIGENCIA,
                    DT_INICIO_VIGENCIA,
                    IE_DEFEITO,
                    IE_FUNCAO_USUARIO,
                    IE_SOLICITACAO,
                    NM_USUARIO,
                    NM_USUARIO_GRUPO,
                    NM_USUARIO_NREC,
                    NR_SEQ_GRUPO,
                    NR_SEQ_LOCALIZACAO,
                    NR_SEQUENCIA) 
                    values
                    (sysdate,
                    sysdate,
                    r03.DT_FIM_VIGENCIA,
                    r03.DT_INICIO_VIGENCIA,
                    null,
                    r03.IE_FUNCAO_USUARIO,
                    null,
                    r03.NM_USUARIO,
                    r03.NM_USUARIO_GRUPO,
                    r03.NM_USUARIO_NREC,
                    nr_seq_grupo_des_w,
                    null,
                    nr_seq_usu_grupo_des_w);
                
                end loop;
      
            end loop;
            
            delete from usuario_grupo_sup u where u.nr_seq_grupo in (select g.nr_sequencia from grupo_suporte g where g.nr_seq_gerencia_sup = :new.nr_sequencia);
            delete from grupo_suporte where nr_seq_gerencia_sup = :new.nr_sequencia;

        elsif (:old.ie_tipo_grupo = 'D' and :new.ie_tipo_grupo = 'S') then
            
            update grupo_desenvolvimento
               set nr_seq_grupo_gerencia = null
             where nr_seq_gerencia = :new.nr_sequencia;
            
            delete from gerencia_wheb_grupo_usu u
             where u.nr_seq_grupo in (select g.nr_sequencia
                                        from gerencia_wheb_grupo g
                                       where g.nr_seq_gerencia = :old.nr_sequencia);
            
            delete from gerencia_wheb_grupo
                 where nr_seq_gerencia = :old.nr_sequencia;
                 
            for r02 in c02(:new.nr_sequencia) loop

                select grupo_suporte_seq.nextval
                  into nr_seq_grupo_sup_w
                  from dual;
                  
                INSERT INTO grupo_suporte (
                    cd_exp_grupo,
                    ds_grupo,
                    dt_atualizacao,
                    dt_atualizacao_nrec,
                    ie_situacao,
                    nm_usuario,
                    nm_usuario_lider,
                    nm_usuario_nrec,
                    nr_seq_gerencia_sup,
                    nr_seq_grupo_gerencia,
                    nr_sequencia,
                    qt_dia_prev_os
                ) VALUES (
                    r02.cd_exp_grupo,
                    r02.ds_grupo,
                    r02.dt_atualizacao,
                    r02.dt_atualizacao_nrec,
                    r02.ie_situacao,
                    r02.nm_usuario,
                    r02.nm_usuario_lider,
                    r02.nm_usuario_nrec,
                    r02.nr_seq_gerencia,
                    r02.nr_seq_grupo_gerencia,
                    nr_seq_grupo_sup_w,
                    r02.qt_dia_prev_os
                );
                
                for r04 in c04(r02.nr_sequencia) loop
                
                    select usuario_grupo_sup_seq.nextval
                      into nr_seq_usu_grupo_sup_w
                      from dual;
                      
                    insert into usuario_grupo_sup
                    (DT_ATUALIZACAO,
                    DT_ATUALIZACAO_NREC,
                    DT_FIM_VIGENCIA,
                    DT_INICIO_VIGENCIA,
                    IE_FUNCAO_USUARIO,
                    NM_USUARIO,
                    NM_USUARIO_GRUPO,
                    NM_USUARIO_NREC,
                    NR_SEQ_GRUPO,
                    NR_SEQUENCIA)
                    values
                    (r04.DT_ATUALIZACAO,
                    r04.DT_ATUALIZACAO_NREC,
                    r04.DT_FIM_VIGENCIA,
                    r04.DT_INICIO_VIGENCIA,
                    r04.IE_FUNCAO_USUARIO,
                    r04.NM_USUARIO,
                    r04.NM_USUARIO_GRUPO,
                    r04.NM_USUARIO_NREC,
                    nr_seq_grupo_sup_w,
                    nr_seq_usu_grupo_sup_w);
                      
                end loop;
                
            end loop;
            
            delete from usuario_grupo_des u where u.nr_seq_grupo in (select g.nr_sequencia from grupo_desenvolvimento g where g.nr_seq_gerencia = :new.nr_sequencia);
            delete from grupo_desenvolvimento where nr_seq_gerencia = :new.nr_sequencia;

        end if;

    end if;

end;
/