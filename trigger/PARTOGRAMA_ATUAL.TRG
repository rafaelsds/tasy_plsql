create or replace trigger partograma_atual
before insert or update on partograma
for each row

declare

ie_ultima_hora_registrada_w	number(2);
dt_registro_hora_w		date;
novo_ie_hora_w			number(10);

pragma autonomous_transaction;

begin

if	(:new.ie_hora is null) then
	select	max(ie_hora)
	into	ie_ultima_hora_registrada_w
	from	partograma
	where	nr_atendimento = :new.nr_atendimento;

	if	(ie_ultima_hora_registrada_w is null) then
		:new.ie_hora	:= 0;
	else
		select	min(dt_registro)
		into	dt_registro_hora_w
		from	partograma
		where	nr_atendimento = :new.nr_atendimento
		and	ie_hora = ie_ultima_hora_registrada_w;
		
		novo_ie_hora_w	:= ie_ultima_hora_registrada_w + trunc((:new.dt_registro - dt_registro_hora_w)*24);
		
		if	(novo_ie_hora_w < 0) then
			:new.ie_hora	:= 0;
		elsif	(novo_ie_hora_w > 23) then
			:new.ie_hora	:= null;
		else
			:new.ie_hora	:= novo_ie_hora_w;
		end if;
	end if;
end if;
end;
/
