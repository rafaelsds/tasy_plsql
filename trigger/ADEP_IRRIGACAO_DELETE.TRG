CREATE OR REPLACE TRIGGER adep_irrigacao_Delete
before delete ON adep_irrigacao
FOR EACH ROW
DECLARE

BEGIN

update	adep_irrigacao_etapa
set		nr_seq_horario	= null
where	nr_seq_irrigacao = :old.nr_sequencia;

END;
/
