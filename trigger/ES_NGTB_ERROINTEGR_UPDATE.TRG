create or replace trigger ES_NGTB_ERROINTEGR_UPDATE
before update on ES_NGTB_ERROINTEGR_ERIN
for each row

declare

ds_log_w	varchar2(4000)	:= '';

begin

if	(:old.erin_dh_status <> :new.erin_dh_status) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_dh_status' || ';' || 'VL_ANTIGO=' || to_char(:old.erin_dh_status,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.erin_dh_status,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_codigocliente <> :new.erin_ds_codigocliente) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_codigocliente' || ';' || 'VL_ANTIGO=' || :old.erin_ds_codigocliente || ';' || 'VL_NOVO=' || :new.erin_ds_codigocliente) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_codoperacao <> :new.erin_ds_codoperacao) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_codoperacao' || ';' || 'VL_ANTIGO=' || :old.erin_ds_codoperacao || ';' || 'VL_NOVO=' || :new.erin_ds_codoperacao) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_cpfcnpj <> :new.erin_ds_cpfcnpj) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_cpfcnpj' || ';' || 'VL_ANTIGO=' || :old.erin_ds_cpfcnpj || ';' || 'VL_NOVO=' || :new.erin_ds_cpfcnpj) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_msgerro <> :new.erin_ds_msgerro) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_msgerro' || ';' || 'VL_ANTIGO=' || :old.erin_ds_msgerro || ';' || 'VL_NOVO=' || :new.erin_ds_msgerro) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_numerofatura <> :new.erin_ds_numerofatura) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_numerofatura' || ';' || 'VL_ANTIGO=' || :old.erin_ds_numerofatura || ';' || 'VL_NOVO=' || :new.erin_ds_numerofatura) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_sistemaorigem <> :new.erin_ds_sistemaorigem) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_sistemaorigem' || ';' || 'VL_ANTIGO=' || :old.erin_ds_sistemaorigem || ';' || 'VL_NOVO=' || :new.erin_ds_sistemaorigem) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_in_status <> :new.erin_in_status) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_in_status' || ';' || 'VL_ANTIGO=' || :old.erin_in_status || ';' || 'VL_NOVO=' || :new.erin_in_status) || chr(13) || chr(10),1,4000);
end if;

GERAR_CRM_COBRANCA_LOG(ds_log_w,'ES_NGTB_ERROINTEGR_ERIN',:new.nm_usuario);

end;
/