create or replace trigger PLS_REQUISICAO_UPDATE
before update on PLS_REQUISICAO
for each row

declare
nr_seq_prot_atend_w	pls_protocolo_atendimento.nr_sequencia%type;

begin
begin
	select	nr_sequencia
	into	nr_seq_prot_atend_w
	from	pls_protocolo_atendimento
	where 	nr_seq_requisicao	= :new.nr_sequencia;
exception
when others then
	nr_seq_prot_atend_w	:= null;
end;
	
if (nr_seq_prot_atend_w is not null) then
	if 	(nvl(:old.ie_estagio, 0) <> nvl(:new.ie_estagio, 0)) and
		(:old.ie_estagio in (4,5,8,10)) and
		(:new.ie_estagio in (2,6,7)) then
		
		update	pls_protocolo_atendimento
		set	ie_status	= 3,
			nm_usuario 	= :new.nm_usuario,
			dt_atualizacao 	= sysdate
		where	nr_sequencia	= nr_seq_prot_atend_w;	
		
	elsif 	(nvl(:old.ie_estagio, 0) <> nvl(:new.ie_estagio, 0)) and	
		(:new.ie_estagio = 3) then
		
		update	pls_protocolo_atendimento
		set	ie_status	= 4,
			nm_usuario 	= :new.nm_usuario,
			dt_atualizacao 	= sysdate
		where	nr_sequencia	= nr_seq_prot_atend_w;
	end if;
end if;

end;
/