create or replace trigger ESCALA_WAT_ATUAL
before update or insert on escala_wat
for each row

declare
  qt_pontos_w    number(10);
  sql_w          varchar(250);
begin
--- Inicio MD1
  if (wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
    begin
      sql_w := 'CALL OBTER_SCORE_ESCALA_WAT_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11) INTO :qt_pontos_w';
      EXECUTE IMMEDIATE sql_w USING IN nvl(:new.IE_DIARREIA, 0), 
                                    IN nvl(:new.IE_ESTADO_COMPORT, 0),
                                    IN nvl(:new.IE_BOCEJOS, 0), 
                                    IN nvl(:new.IE_MOV_DESCOOR, 0),
                                    IN nvl(:new.IE_SUDORESE, 0),
                                    IN nvl(:new.IE_TATIL, 0),
                                    IN nvl(:new.IE_TEMPERATURA, 0),
                                    IN nvl(:new.IE_TEMPO_TRANQ, 0),
                                    IN nvl(:new.IE_TONUS, 0),
                                    IN nvl(:new.IE_TREMOR, 0),
                                    IN nvl(:new.IE_VOMITO_NAUSEA, 0),
                                    OUT qt_pontos_w;
    exception
      when others then 
        :new.qt_score := null;
     end;

     :new.QT_SCORE := nvl(qt_pontos_w, 0);
  end if;
--- Fim MD1
end;
/