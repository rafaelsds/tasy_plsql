create or replace trigger pf_familia_before_del
before delete on pf_familia
for each row

declare
qtd_member_w		number(10) := 0;
ie_grau_parentesco_w	grau_parentesco.ie_grau_parentesco%type;

pragma autonomous_transaction;


function get_qtd_member_family(member_type_p number, nr_seq_member_sup_p number)
return number 
as 
qtd_w number(1) := 0;

begin
	
if	(nr_seq_member_sup_p is null) then	
	select count(*)
	into	qtd_w
	from	pf_familia f, 
		grau_parentesco g 
	where	f.cd_pessoa_fisica = :old.cd_pessoa_fisica	
	and	f.nr_seq_grau_parentesco = g.nr_sequencia
	and	f.nr_sequencia <> :old.nr_sequencia
	and	g.ie_grau_parentesco = member_type_p;
		
else
	select count(*)
	into	qtd_w
	from	pf_familia f, 
		grau_parentesco g 
	where	f.cd_pessoa_fisica = :old.cd_pessoa_fisica	
	and	f.nr_seq_grau_parentesco = g.nr_sequencia
	and	f.nr_sequencia <> :old.nr_sequencia
	and	g.ie_grau_parentesco = member_type_p
	and	f.nr_seq_member_sup = nr_seq_member_sup_p;

end if;

return qtd_w;

end get_qtd_member_family;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	select	max(ie_grau_parentesco) ie_grau_parentesco
	into	ie_grau_parentesco_w
	from	grau_parentesco g
	where	nr_sequencia = :old.nr_seq_grau_parentesco;
	
	
		
	if	(ie_grau_parentesco_w = '6') then /*If delete a Father*/
		
		qtd_member_w := get_qtd_member_family('9',null); /*And exists Paternal grandfather/grandmother, show a message*/
		if	(qtd_member_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1159074);
		end if;
	
	elsif	(ie_grau_parentesco_w = '5') then /*If delete a Mother*/
		
		qtd_member_w := get_qtd_member_family('12',null); /*And exists Maternal grandfather/grandmother, show a message*/
		
		
		if	(qtd_member_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1159075);
		end if;	
	
	elsif	(ie_grau_parentesco_w = '13') then /*If delete a Spouse*/
		
		qtd_member_w := get_qtd_member_family('15',null); /*and exist a Father-in-law/Mother-in-low, show a message*/
		if	(qtd_member_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1159076);
		end if;
	
	
	elsif	(ie_grau_parentesco_w = '3') then /*Id delete a Son/Daugther*/
	
		qtd_member_w := get_qtd_member_family('14',:old.nr_sequencia); /*and exist a Grandchild, show a message*/
		if	(qtd_member_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1159077);
		end if;
	end if;
end if;
end;
/

