CREATE OR REPLACE TRIGGER gestao_vaga_update
AFTER UPDATE ON gestao_vaga
FOR EACH ROW

declare

cd_paciente_reserva_w	varchar2(10);	
ie_status_unidade_w	varchar2(3);
ie_atualiza_status_w 	varchar2(1);
qt_reg_w		number(1);
ie_atualiza_agecir_w	varchar2(1);
ds_log_w				varchar2(2000);
nm_pac_reserva_w	varchar2(255);
gestao_vaga_r gestao_vaga%rowtype;
BEGIN

select	nvl(max(Obter_Valor_Param_Usuario(1002, 39, Obter_Perfil_Ativo, :NEW.nm_usuario, 0)), 'S')
into	ie_atualiza_status_w
from 	dual;

select	nvl(max(Obter_Valor_Param_Usuario(1002, 84, Obter_Perfil_Ativo, :NEW.nm_usuario, 0)), 'S')
into	ie_atualiza_agecir_w
from 	dual;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.ie_status = 'D') or (:new.ie_status = 'N') or (:new.ie_status = 'F') then
	begin	

	begin
	select	nvl(max(ie_status_unidade),'L'),
		max(cd_paciente_reserva),
		max(nm_pac_reserva)
	into	ie_status_unidade_w,
		cd_paciente_reserva_w,
		nm_pac_reserva_w
	from	unidade_atendimento
	where	cd_unidade_basica	= :new.cd_unidade_basica	
	and	cd_unidade_compl	= :new.cd_unidade_compl
	and	cd_setor_atendimento	= :new.cd_setor_desejado;
	exception
	when others then
		ie_status_unidade_w 	:= 'L';
		cd_paciente_reserva_w 	:= null;
		nm_pac_reserva_w	:= null;
	end;
		
	if	((:new.cd_pessoa_fisica = cd_paciente_reserva_w) or
		 (:new.nm_paciente = nm_pac_reserva_w)) then
		begin
		if	(ie_status_unidade_w = 'R')	then
			update	unidade_atendimento
			set	ie_status_unidade	= 'L',
				cd_paciente_reserva	= null,	
				ds_observacao		= null,
				nm_usuario_reserva	= null,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate,
				nm_pac_reserva		= null
			where	cd_unidade_basica	= :new.cd_unidade_basica	
			and	cd_unidade_compl	= :new.cd_unidade_compl
			and	cd_setor_atendimento	= :new.cd_setor_desejado;
		else
			update	unidade_atendimento
			set	cd_paciente_reserva	= null,	
				ds_observacao		= null,
				nm_usuario_reserva	= null,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate,
				nm_pac_reserva		= null
			where	cd_unidade_basica	= :new.cd_unidade_basica	
			and	cd_unidade_compl	= :new.cd_unidade_compl
			and	cd_setor_atendimento	= :new.cd_setor_desejado;
		end if;
		end;	
	end if;
	
	end;
end if;


if (:new.ie_status <> :old.ie_status) then
	
	insert into gestao_vaga_hist_status
		(nr_sequencia,
		 dt_atualizacao,
	     	 nm_usuario,
		 dt_atualizacao_nrec,
	  	 nm_usuario_nrec,
		 nr_seq_gestao_vaga,
		 ie_status,
		 nm_usuario_resp_transf)
	values
		(gestao_vaga_hist_status_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
	  	:new.nm_usuario,
		:new.nr_sequencia,
		:new.ie_status,
		:new.nm_resp_transf		
		);

	begin
	ds_log_w := substr(	' '||wheb_mensagem_pck.get_texto(795194)||' : '|| :new.nr_sequencia || chr(13) ||chr(10)||
						' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);

	insert into log_mov(
		dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values (
		sysdate,
		:new.nm_usuario,
		88911,
		ds_log_w);

	exception
		when others then
		ds_log_w	:=	'';
	end;

end if;

if	(ie_atualiza_status_w = 'S') and 
	(:new.nr_seq_agenda <> 0) and
	(:new.ie_status <> :old.ie_status) and
	((:new.ie_status = 'F') or (:new.ie_status = 'P'))  then
		
	update	agenda_paciente
	set	ie_status_agenda = 'N'
	where	nr_sequencia = :new.nr_seq_agenda
	and	ie_status_agenda not in ('C','E');
	
end if;

if 	(:new.cd_pessoa_fisica is not null) and
	(:old.cd_pessoa_fisica is null) and
	(ie_atualiza_agecir_w = 'S') then
	update	agenda_paciente
	set	ie_status_agenda = 'N'
	where	nr_sequencia = :new.nr_seq_agenda;
end if;

if 	(nvl(:new.cd_pessoa_fisica, 0) <> nvl(:old.cd_pessoa_fisica, 0)) then
	  
	update	autorizacao_convenio
	set	cd_pessoa_fisica 	= :new.cd_pessoa_fisica,
		nm_usuario 		= :new.nm_usuario
	where	nr_seq_gestao 		= :new.nr_sequencia;
  
end if;

if (:old.ie_status <> :new.ie_status) then

	gestao_vaga_r.nr_sequencia := :new.nr_sequencia;
	gestao_vaga_r.cd_convenio := :new.cd_convenio;
	gestao_vaga_r.ie_tipo_vaga := :new.ie_tipo_vaga;
	gestao_vaga_r.ie_status := :new.ie_status;
	gestao_vaga_r.ie_solicitacao := :new.ie_solicitacao;
	gestao_vaga_r.cd_setor_atual := :new.cd_setor_atual;
	gestao_vaga_r.cd_setor_desejado := :new.cd_setor_desejado;
	gestao_vaga_r.cd_tipo_acomod_desej := :new.cd_tipo_acomod_desej;
	gestao_vaga_r.nr_seq_status_pac := :new.nr_seq_status_pac;
	gestao_vaga_r.nr_atendimento := :new.nr_atendimento;
	gestao_vaga_r.dt_prevista	:=	:new.dt_prevista;
	gestao_vaga_r.nm_paciente	:= :new.nm_paciente;
	
	gerar_evento_gestao_vaga_auto(:new.cd_pessoa_fisica,:new.nm_usuario,:new.cd_estabelecimento,:new.nr_sequencia,:new.nm_paciente,'AAV', :new.ie_status, gestao_vaga_r);
end if;

if((:new.ie_status ='F' and :new.ie_solicitacao='I') and 
  (:old.ie_status <> :new.ie_status or :old.ie_solicitacao <> :new.ie_solicitacao)) then
      
    execute_bifrost_integration(251, '{"recordId" : "' || :new.nr_sequencia|| '"' || '}');
    
	call_interface_file(932, 'carestream_ris_japan_l10n_pck.patient_admission_info(' || :new.nr_sequencia || ',''3H'' , 1, ''' || :new.nm_usuario || ''' , 0, null, ' || :new.cd_estabelecimento || ',''N'');', :new.nm_usuario);
	call_interface_file(948, 'tosho_pck.patient_admission_info(' || :new.nr_sequencia || ',''3H'' , 1, ''' || :new.nm_usuario || ''' , 0, null, ' || :new.cd_estabelecimento ||  ', ''N'' );' , :new.nm_usuario);	 
end if;

if((:new.ie_status in ('C','N', 'A') and :old.ie_status in ('F') and :new.ie_solicitacao='I') and 
  (:old.ie_status <> :new.ie_status or :old.ie_solicitacao <> :new.ie_solicitacao)) then 

	call_interface_file(932, 'carestream_ris_japan_l10n_pck.patient_admission_info(' || :new.nr_sequencia || ',''3I'' , 3, ''' || :new.nm_usuario || ''' , 0, null, ' || :new.cd_estabelecimento || ',''N'');' , :new.nm_usuario);
	call_interface_file(948, 'tosho_pck.patient_admission_info(' || :new.nr_sequencia || ',''3I'' , 3, ''' || :new.nm_usuario || ''' , 0, null, ' || :new.cd_estabelecimento ||  ', ''N'' );' , :new.nm_usuario);	

end if;

<<Final>>
qt_reg_w := 0;
END;
/
