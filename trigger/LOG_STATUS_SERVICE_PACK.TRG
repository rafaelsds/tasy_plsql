CREATE OR REPLACE TRIGGER LOG_STATUS_SERVICE_PACK
  AFTER UPDATE ON MAN_VERSAO_CALENDARIO
  FOR EACH ROW

DECLARE
BEGIN

  insert into LOG_STATUS_SP
  (nr_sequencia,
  nm_usuario,
  nr_seq_v_calendario,
  dt_atualizacao,
  nr_seq_status_sp,
  ie_status)
  values
  (log_status_sp_seq.nextval,
  :old.nm_usuario,
  :old.nr_sequencia,
  :old.dt_atualizacao,
  :old.nr_seq_status_sp,
  :old.ie_status_atividade);

END;

/