CREATE OR REPLACE TRIGGER Cirurgia_Participante_Delete
BEFORE DELETE ON Cirurgia_Participante
FOR EACH ROW

DECLARE
ie_gerar_particip_w	varchar2(255);

BEGIN

ie_gerar_particip_w := Obter_Valor_Param_Usuario(872, 448, Obter_Perfil_Ativo, :new.nm_usuario, 0);

if	(ie_gerar_particip_w = 'S') and
	(wheb_usuario_pck.get_cd_funcao = 872) then
	
	if	(:old.cd_pessoa_fisica is not null) then

		Ajustar_Participante_Proc(
				'DELETE',
				:old.nr_cirurgia,
				:old.nr_sequencia,
				:old.ie_funcao,
				:old.cd_pessoa_fisica,
				:old.nm_participante,
				:old.nr_seq_procedimento,
				'');
	end if;
elsif (nvl(ie_gerar_particip_w,'X') <> 'U') then
	
	Ajustar_Participante_Proc(
			'DELETE',
			:old.nr_cirurgia,
			:old.nr_sequencia,
			:old.ie_funcao,
			:old.cd_pessoa_fisica,
			:old.nm_participante,
			:old.nr_seq_procedimento,
			'');
end if;
END;
/
