create or replace trigger pessoa_juridica_estab_after
after insert or update  on pessoa_juridica_estab
for each row

declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_opcao_w	varchar2(1) := 'I';
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

begin

if 	(updating) then
	ie_opcao_w	:= 'A';
end if;

select 	max(cd_tipo_pessoa)
into	cd_tipo_pessoa_w
from 	pessoa_juridica
where 	cd_cgc = :new.cd_cgc
and 	ie_situacao = 'A';

reg_integracao_p.ie_operacao		:=	ie_opcao_w;
reg_integracao_p.cd_tipo_pessoa		:=	cd_tipo_pessoa_w;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :new.cd_cgc,:new.nm_usuario, reg_integracao_p);

end;
/
