create or replace TRIGGER MED_AVALIACAO_PACIENTE_UPDATE
AFTER
UPDATE ON MED_AVALIACAO_PACIENTE FOR EACH ROW
DECLARE
qt_reg_w	number(1);
cd_evolucao_pendencia_w 	evolucao_paciente.cd_evolucao%type;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Almir em 15/07/2008 OS89916  */
if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then

	Gerar_comunic_aval(:new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nr_seq_tipo_avaliacao, :new.dt_avaliacao, :new.nm_usuario);

	if(:new.nr_seq_contato is not null)then
		update	cih_contato a
		set	dt_lib_avaliacao	= sysdate,
			nm_usuario_lib_aval	= :new.nm_usuario
		where 	a.nr_sequencia 		= :new.nr_seq_contato;
	end if;
end if;

if	(:old.dt_inativacao is null) and
	(:new.dt_inativacao is not null) then
	update	evolucao_paciente
	set	dt_inativacao = :new.dt_inativacao,
		IE_SITUACAO = 'I',
		NM_USUARIO_INATIVACAO = :new.NM_USUARIO_INATIVACAO,
		DS_JUSTIFICATIVA = :new.DS_JUSTIFICATIVA
	where	nr_seq_avaliacao = :new.nr_sequencia;
	
	select 	max(cd_evolucao)
	into   	cd_evolucao_pendencia_w
	from   	evolucao_paciente
	where	nr_seq_avaliacao = :new.nr_sequencia;
	
	if(cd_evolucao_pendencia_w > 0) then
		Gerar_registro_pendente_PEP('XE', cd_evolucao_pendencia_w, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nm_usuario,'A');
	end if;

	if(:new.nr_seq_contato is not null)then
		update	cih_contato a
		set	dt_inativacao_aval	= sysdate,
			nm_usuario_inat_aval	= :new.nm_usuario
		where 	a.nr_sequencia 		= :new.nr_seq_contato;
	end if;
end if;


<<Final>>
qt_reg_w	:= 0;
END;
/