create or replace trigger scmpg_material_bfinsert
before insert on material
for each row

/*
select 	cd_material,
	(qt_livre - 5) qt_reg_util
from (	select	cd_material,
		cd_material_prox - cd_material - 1 qt_livre
	from (	select	a.cd_material,
			(select	nvl(min(x.cd_material),999999)
			from 	material x
			where	x.cd_material > a.cd_material) cd_material_prox
		from 	material a)
	where	cd_material_prox - cd_material - 1 > 50
	order by 1)
where	rownum = 1;

select	cd_material+1 cd_mat_ini,
	cd_material_prox-5 cd_mat_fim,
	qt_livre - 5 qt_reg_util
from (	select	cd_material,
		cd_material_prox - cd_material - 1 qt_livre,
		cd_material_prox
	from (	select	a.cd_material,
			(select	nvl(min(x.cd_material),999999)
			from 	material x
			where	x.cd_material > a.cd_material) cd_material_prox
		from 	material a)
	where	cd_material_prox - cd_material - 1 > 50
	order by 1);

drop sequence material_seq;

CREATE SEQUENCE material_seq
 START WITH     1736
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
*/

declare
cd_material_atual_w	material.cd_material%type;
cd_material_proximo_w	material.cd_material%type;
cd_material_livre_w		material.cd_material%type;
cd_material_seq_w		material.cd_material%type	:=	0;

qt_min_livre_faixa_w	number(5)	:=	50;
qt_min_livre_atual_w	number(5)	:=	5;

pragma autonomous_transaction;
begin

/*select	material_seq.currval
into	cd_material_atual_w
from	dual;*/

cd_material_atual_w	:=	:new.cd_material; 

select	min(cd_material)
into	cd_material_proximo_w
from	material
where	cd_material >= cd_material_atual_w;

if	((cd_material_proximo_w - cd_material_atual_w) < qt_min_livre_atual_w) then
	begin
	select	cd_material
	into	cd_material_livre_w
	from(	select	cd_material,
			cd_material_prox - cd_material - 1 qt_livre
		from (	select	a.cd_material,
				(select nvl(min(x.cd_material),999999)
				from	material x
				where	x.cd_material > a.cd_material) cd_material_prox
			from	material a
			where	cd_material > cd_material_atual_w)
		where	cd_material_prox - cd_material - 1 > qt_min_livre_faixa_w
		order by 1)
	where	rownum = 1;
	
	while	(cd_material_seq_w < cd_material_livre_w) loop 
		begin
		select	material_seq.nextval
		into	cd_material_seq_w
		from	dual;
		end;
	end loop;
	
	end;
end if;

end;
/