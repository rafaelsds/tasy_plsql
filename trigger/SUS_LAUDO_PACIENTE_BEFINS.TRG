CREATE OR REPLACE TRIGGER SUS_LAUDO_PACIENTE_BEFINS
BEFORE INSERT ON SUS_LAUDO_PACIENTE
FOR EACH ROW

DECLARE

ie_permite_w		Varchar2(1) := 'S';
ds_retorno_w		varchar2(255)	:= '';
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

BEGIN

if	(:new.nr_seq_interno is null) then
	select	sus_laudo_paciente_seq.nextVal
	into	:new.nr_seq_interno
	from	dual;
end if;

select	Obter_Se_solic_laudo(:new.nr_atendimento,:new.ie_classificacao,coalesce(:new.cd_medico_requisitante,:new.cd_profis_requisitante))
into	ie_permite_w
from	dual;

if	(ie_permite_w = 'N') then
	--'O medico requisitante nao tem permissao para gerar laudo. Verificar regra solicitantes de laudos nos Cadastros gerais/Aplicacao principal/Cadastros SUS.');
	wheb_mensagem_pck.exibir_mensagem_abort(263527);
end if;

begin
cd_estabelecimento_w := nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
exception
when others then
	cd_estabelecimento_w := 0;
end;

sus_consiste_regra_laudo(cd_estabelecimento_w, :new.cd_procedimento_solic, :new.ie_origem_proced, ds_retorno_w);

if	(nvl(ds_retorno_w,'X') <> 'X') then
	ds_retorno_w := substr(substr(ds_retorno_w,1,220) || chr(13) || chr(10) || WHEB_MENSAGEM_PCK.get_texto(454285),1,255);
	Wheb_mensagem_pck.exibir_mensagem_abort(262328 , 'DS_MENSAGEM='||ds_retorno_w);
end if;

END;
/
