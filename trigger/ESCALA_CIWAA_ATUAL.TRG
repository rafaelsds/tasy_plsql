create or replace trigger ESCALA_CIWAA_ATUAL
before insert or update on ESCALA_CIWAA
for each row
declare
	EXEC_w          varchar2(200);
	qt_resultado_w	number(10) := 0;
begin
    if  (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
        begin
            EXEC_w := 'CALL OBTER_SCORE_ESCALA_CIWAA_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10) INTO :qt_resultado_w';
            
            EXECUTE IMMEDIATE EXEC_w USING  IN nvl(:new.IE_MAL_ESTAR,0), 
                                            IN nvl(:new.IE_TREMOR_BRACO,0), 
                                            IN nvl(:new.IE_SUDORESE,0), 
                                            IN nvl(:new.IE_PERTURBACAO_COCEIRA,0), 
                                            IN nvl(:new.IE_PERTURBACAO_AUDITIVA,0), 
                                            IN nvl(:new.IE_PERTURBACAO_VISUAL,0), 
                                            IN nvl(:new.IE_NERVOSISMO,0), 
                                            IN nvl(:new.IE_DOR_CABECA,0), 
                                            IN nvl(:new.IE_AGITACAO,0), 
                                            IN nvl(:new.IE_ALT_ORIENTACAO,0), 
                                            OUT qt_resultado_w;
        exception
            when others then
                qt_resultado_w := null;
        end;

        :new.QT_SCORE  := qt_resultado_w;
    end if;
end;
/