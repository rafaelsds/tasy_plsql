create or replace trigger nota_fiscal_trib_insert
before insert or update on nota_fiscal_trib
for each row
begin

if	(:new.vl_tributo < 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(282343,'VL_TRIBUTO= ' || :new.vl_tributo);
end if;

end;
/
