create or replace trigger CHEQUE_AFTER_ATUAL
after insert or update on CHEQUE
for each row

declare
nr_cheque_atual_w	varchar2(30);
qt_registro_w		number(10);

begin

if (updating) then
	if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
		begin
		nr_cheque_atual_w	:= :new.nr_cheque;
		
		select	count(*)
		into	qt_registro_w
		from	cheque_bordero_titulo a
		where	a.nr_seq_cheque	= :new.nr_sequencia
		and	a.nr_cheque <> nr_cheque_atual_w
		and	a.nr_cheque is not null;
		
		if	(qt_registro_w > 0) then
		
			update	cheque_bordero_titulo a
			set	nr_cheque 		= nr_cheque_atual_w
			where	a.nr_seq_cheque	= :new.nr_sequencia
			and	a.nr_cheque <> nr_cheque_atual_w
			and	a.nr_cheque is not null;
		end if;
		end;
	end if;
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_sequencia,null,'CP',nvl(:new.dt_prev_pagamento,:new.dt_vencimento),'A',:new.nm_usuario);
elsif (inserting) then
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_sequencia,null,'CP',nvl(:new.dt_prev_pagamento,:new.dt_vencimento),'I',:new.nm_usuario);
end if;

end;
/
