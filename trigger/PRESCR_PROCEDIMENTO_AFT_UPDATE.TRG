create or replace trigger prescr_procedimento_aft_update
before update on prescr_procedimento
for each row

declare
ie_envia_mensagem_w        varchar2(1);
ie_matrix_status_envio_integ_w  lab_parametro.ie_matrix_status_envio_integ%type;
ie_agrupada_w          varchar2(1);

begin

if  (wheb_usuario_pck.is_evento_ativo(288) = 'S') and
  (:new.ie_status_atend < 20) and
  (nvl(:old.ie_suspenso,'N') = 'N') and
  (nvl(:new.ie_suspenso,'N') = 'S') then

  integrar_softlab_ws(  288,
              null,
              :new.nr_prescricao,
              :new.nr_sequencia,
              :new.nr_seq_exame,
              :new.nm_usuario,
              2,
              'N');
end if;

if  ((wheb_usuario_pck.is_evento_ativo(414) = 'S') or (obter_se_integracao_ativa(886,53) = 'S')) and
  (:new.nr_seq_exame is not null) and
  (nvl(:old.ie_suspenso,'N') = 'N') and
  (nvl(:new.ie_suspenso,'N') = 'S') then

  select  max(a.IE_MATRIX_STATUS_ENVIO_INTEG)
  into  ie_matrix_status_envio_integ_w
  from  lab_parametro a
  where  a.cd_estabelecimento = obter_valores_prescr_trigger(:new.nr_prescricao,'E');

  if  (ie_matrix_status_envio_integ_w is null) or
    (:new.ie_status_atend >= ie_matrix_status_envio_integ_w) then
    if (wheb_usuario_pck.is_evento_ativo(414) = 'S') then
           integrar_matrix_ws(414,
                              :new.nr_prescricao,
                              :new.nr_sequencia,
                              :new.nr_seq_exame,
                              :new.nm_usuario,
                              'N');
    end if;
  if (obter_se_integracao_ativa(886,53) = 'S') then
       if (obter_se_contido(:old.ie_status_atend,Wheb_assist_pck.obterParametroFuncao(722,261)) = 'S') or
          (Wheb_assist_pck.obterParametroFuncao(722,261) is null) then
				  
          integrar_matrix_ws_v7(886, :new.nr_prescricao, :new.nr_sequencia,0,:new.nr_seq_exame);
       else
          wheb_mensagem_pck.exibir_mensagem_abort(1098715,'DS_STATUS='||OBTER_DS_STATUS_ATEND(:new.ie_status_atend));          
       end if;
    end if;
  end if;
end if;

if  (wheb_usuario_pck.is_evento_ativo(541) = 'S') and
  (:new.nr_seq_exame is not null) and
  (nvl(:old.ie_suspenso,'N') = 'N') and
  (nvl(:new.ie_suspenso,'N') = 'S') then

    select  decode(count(*),0,'N','S')
    into  ie_envia_mensagem_w
    from  lab_exame_equip b,
        equipamento_lab c,
        material_exame_lab d
    where  :new.nr_seq_exame    = b.nr_seq_exame
    and    b.cd_equipamento  = c.cd_equipamento
    and    :new.cd_material_exame = d.cd_material_exame
    and    (b.nr_seq_material = d.nr_sequencia or b.nr_seq_material is null)
    and    c.ds_sigla = 'AGFAWS'
    and    :new.ie_suspenso = 'S';

  if (ie_envia_mensagem_w = 'S') then

    integrar_agfa_ws(  null,
              :new.nr_prescricao,
              :new.nr_sequencia,
              :new.nm_usuario,
              'S',
              'N');
  end if;
end if;

/*Desvincular a prescrição e o atendimento da prevenda, quando suspende*/
if  (:old.dt_suspensao is null) and
  (:new.dt_suspensao is not null) then

  select  nvl(max(a.ie_agrupada),'N')
  into  ie_agrupada_w
  from  pre_venda a,
      pre_venda_item b
  where  a.nr_sequencia = b.nr_seq_pre_venda
  and    b.nr_prescricao = :new.nr_prescricao;

  if  (ie_agrupada_w = 'N') then
    update  pre_venda_item
    set    nr_atendimento = null,
        nr_prescricao = null,
        nr_seq_interno = null
    where  nr_seq_interno = :new.nr_seq_interno;
  end if;
end if;

end;
/ 