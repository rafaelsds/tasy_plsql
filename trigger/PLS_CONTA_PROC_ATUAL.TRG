create or replace trigger pls_conta_proc_atual
before insert or update on pls_conta_proc
for each row
 
declare

cursor C01(	ds_lista_p		varchar2,
		ds_separador_p	varchar2) is
	select	ds_valor_vchr2
	from	table(pls_util_pck.converter_lista_valores(ds_lista_p, ds_separador_p));

cursor C02(	ds_lista_p		varchar2,
		ds_separador_p	varchar2) is
	select	ds_valor_vchr2,
		dt_valor_date,
		nr_valor_number
	from	table(pls_util_pck.converter_lista_valores(ds_lista_p, ds_separador_p));

sql_err_w			varchar2(4000);
cd_guia_w		varchar2(30);
ie_origem_conta_w		varchar2(5);
ie_tipo_intercambio_w	varchar2(5);
ie_regra_preco_w		varchar2(3)	:= 'N';
ie_internado_w		varchar2(3);
sg_estado_w		pessoa_juridica.sg_estado%type;
sg_estado_int_w		pessoa_juridica.sg_estado%type;
ie_tipo_despesa_w		varchar2(1);
vl_pacote_out_w		number(15,2)	:= 0;
vl_medico_out_w		number(15,2)	:= 0;
vl_anestesista_out_w	number(15,2)	:= 0;
vl_auxiliares_out_w		number(15,2)	:= 0;
vl_custo_operacional_out_w	number(15,2)	:= 0;
vl_materiais_out_w		number(15,2)	:= 0;
cd_proc_pacote_out_w	number(15)	:= null;
ie_origem_pacote_out_w	number(15)	:= null;
cd_procedimento_w	number(15);
ie_origem_proced_w	number(15);
nr_seq_prestador_w	number(10);
nr_seq_pacote_w		number(10);
nr_seq_regra_pacote_out_w	number(10);
nr_seq_intercambio_w	number(10);
nr_seq_classificacao_prest_w	number(10);
nr_seq_protocolo_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_prestador_prot_w		number(10);
nr_seq_tipo_acomod_w		number(10);
nr_seq_plano_w			number(10);
nr_contrato_w			number(10);
nr_seq_congenere_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_prestador_conta_w	number(10);
cd_estabelecimento_w		number(4);
dt_procedimento_w		date;
dt_procedimento_imp_w		date;
dt_conta_guia_w			date;
dt_pacote_w			date;
ie_desc_item_glosa_atend_w	pls_parametros.ie_desc_item_glosa_atend%type;
 

dt_atendimento_referencia_w	pls_conta.dt_atendimento_referencia%type;
nr_seq_prestador_exec_w		pls_conta.nr_seq_prestador_exec%type;
dt_protocolo_w			pls_protocolo_conta.dt_protocolo%type;
dt_mes_competencia_w		pls_protocolo_conta.dt_mes_competencia%type;
nr_contador_w			pls_integer;
ds_campo_w			varchar2(100);
ds_log_call_w			varchar2(1500);
ds_log_glosa_w			varchar2(100);
ds_log_w			plsprco_cta.ds_log%type;


begin
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') and
	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N')  then
	-- essa parte da trigger n�o ir� rodar se for necess�rio fazer algum trabalho de redund�ncia para evitar trigger mutante
	if	(pls_util_pck.get_ie_executar_redundancia_w	= 'N')  then
	
		if (((:new.vl_liberado + :new.vl_glosa) <> :new.vl_procedimento_imp) and (:new.vl_glosa <> 0) and (:new.vl_procedimento_imp <> 0)) then
		
			ds_log_call_w := substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
							' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);
		
			ds_log_w := 	'Verificar valores de glosa e liberado em rela��o ao valor apresentado.' || pls_util_pck.enter_w ||
					'Valor liberado: '||:new.vl_liberado || pls_util_pck.enter_w ||
					'Vl apresentado: ' || :new.vl_procedimento_imp || pls_util_pck.enter_w ||
					'Vl calculado: ' || :new.vl_procedimento|| pls_util_pck.enter_w ||
					'Vl glosa: ' || :new.vl_glosa;
		
			insert	into	plsprco_cta 	
				( 	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
					ds_log, ds_log_call, ds_funcao_ativa, 
					ie_aplicacao_tasy, nm_maquina, ie_opcao,
					pls_conta_proc)
			values	( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_CONTA', 
					ds_log_w, ds_log_call_w, obter_funcao_ativa, 
					pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, '1',
					:new.nr_sequencia);
		
		end if;
	
		
		if	((nvl(:new.vl_liberado_hi,0) <> nvl(:old.vl_liberado_hi,0)) or
			(nvl(:new.vl_liberado_co,0) <> nvl(:old.vl_liberado_co,0)) or
			(nvl(:new.vl_liberado_material,0) <> nvl(:old.vl_liberado_material,0)) or
			(nvl(:new.vl_lib_taxa_servico,0) <> nvl(:old.vl_lib_taxa_servico,0)) or
			(nvl(:new.vl_lib_taxa_co,0) <> nvl(:old.vl_lib_taxa_co,0)) or
			(nvl(:new.vl_lib_taxa_material,0) <> nvl(:old.vl_lib_taxa_material,0))) and
			(nvl(:new.tx_intercambio,0) > 0) and
			(nvl(:new.vl_liberado,0) > 0) and
			((nvl(:new.vl_liberado_hi,0) + nvl(:new.vl_lib_taxa_servico,0) +
			nvl(:new.vl_liberado_co,0) + nvl(:new.vl_lib_taxa_co,0) +
			nvl(:new.vl_liberado_material,0) + nvl(:new.vl_lib_taxa_material,0)) <> :new.vl_liberado) then
			
			ds_log_w := 	'Diferen�a nos valores individuais com o valor total.' || pls_util_pck.enter_w ||
					'Valores Novos: ' || pls_util_pck.enter_w ||
					'Vl hi: ' || :new.vl_liberado_hi || pls_util_pck.enter_w ||
					'Vl co: ' || :new.vl_liberado_co || pls_util_pck.enter_w ||
					'Vl mat: ' || :new.vl_liberado_material || pls_util_pck.enter_w ||
					'Vl taxa hi: ' || :new.vl_lib_taxa_servico || pls_util_pck.enter_w ||
					'Vl taxa co: ' || :new.vl_lib_taxa_co || pls_util_pck.enter_w ||
					'Vl taxa mat: ' || :new.vl_lib_taxa_material || pls_util_pck.enter_w ||
					'Vl liberado: ' || :new.vl_liberado || pls_util_pck.enter_w ||
					'Tx intercambio: ' || :new.tx_intercambio || pls_util_pck.enter_w ||
					'Valores antigos: ' || pls_util_pck.enter_w ||
					'Vl hi: ' || :old.vl_liberado_hi || pls_util_pck.enter_w ||
					'Vl co: ' || :old.vl_liberado_co || pls_util_pck.enter_w ||
					'Vl mat: ' || :old.vl_liberado_material || pls_util_pck.enter_w ||
					'Vl taxa hi: ' || :old.vl_lib_taxa_servico || pls_util_pck.enter_w ||
					'Vl taxa co: ' || :old.vl_lib_taxa_co || pls_util_pck.enter_w ||
					'Vl taxa mat: ' || :old.vl_lib_taxa_material || pls_util_pck.enter_w ||
					'Vl liberado: ' || :old.vl_liberado || pls_util_pck.enter_w ||
					'Tx intercambio: ' || :old.tx_intercambio;
			
			ds_log_call_w := substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
							' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);
			
			insert	into	plsprco_cta 	
				( 	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
					ds_log, ds_log_call, ds_funcao_ativa, 
					ie_aplicacao_tasy, nm_maquina, ie_opcao,
					pls_conta_proc)
			values	( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_CONTA', 
					ds_log_w, ds_log_call_w, obter_funcao_ativa, 
					pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, '1',
					:new.nr_sequencia);
			
		end if;
		
		if	(nvl(:new.ds_justificativa, 'A')  != nvl(:old.ds_justificativa, 'A')) then
		
			ds_log_call_w := substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
							' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);

							
			ds_log_w := 'Alterado a justificativa do procedimento'|| pls_util_pck.enter_w ||' - de: '|| :old.ds_justificativa || pls_util_pck.enter_w || pls_util_pck.enter_w ||' - para: '||:new.ds_justificativa;
			insert	into	plsprco_cta 	
				( 	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
					ds_log, ds_log_call, ds_funcao_ativa, 
					ie_aplicacao_tasy, nm_maquina, ie_opcao,
					pls_conta_proc)
			values	( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_CONTA', 
					ds_log_w, ds_log_call_w, obter_funcao_ativa, 
					pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, '1',
					:new.nr_sequencia);
					
		end if;
		
		if	(nvl(:new.ds_especif_material, 'A')  != nvl(:old.ds_especif_material, 'A')) then
		
			ds_log_call_w := substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
							' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);

							
			ds_log_w := 'Alterado a especifica��o do procedimento'|| pls_util_pck.enter_w ||' - de: '|| :old.ds_especif_material || pls_util_pck.enter_w || pls_util_pck.enter_w ||' - para: '||:new.ds_especif_material;
			insert	into	plsprco_cta 	
				( 	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
					ds_log, ds_log_call, ds_funcao_ativa, 
					ie_aplicacao_tasy, nm_maquina, ie_opcao,
					pls_conta_proc)
			values	( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_CONTA', 
					ds_log_w, ds_log_call_w, obter_funcao_ativa, 
					pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, '1',
					:new.nr_sequencia);
					
		end if;
		
		begin
		dt_procedimento_w	:= to_date(to_char(:new.dt_procedimento,'dd/mm/yyyy') || ' ' || to_char(:new.dt_inicio_proc,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		:new.dt_procedimento	:= dt_procedimento_w;
		exception
		when others then
			sql_err_w	:= substr(sqlerrm, 1, 1800);
		end;
		
		begin
		dt_procedimento_imp_w		:= to_date(to_char(:new.dt_procedimento_imp,'dd/mm/yyyy') || ' ' || to_char(:new.dt_inicio_proc_imp,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		:new.dt_procedimento_imp	:= dt_procedimento_imp_w;
		exception
		when others then
			sql_err_w	:= substr(sqlerrm, 1, 1800);
		end;

		/* Verificar se o procedimento � um pacote */
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_pacote_w
		from	pls_pacote
		where	cd_procedimento		= :new.cd_procedimento
		and	ie_origem_proced	= :new.ie_origem_proced
		and	ie_situacao		= 'A';

		/* Francisco - 16/05/2012 - OS 447352 */
		if	(nr_seq_pacote_w > 0) then
			select	nvl(ie_regra_preco,'N')
			into	ie_regra_preco_w
			from	pls_pacote a
			where	a.nr_sequencia	= nr_seq_pacote_w;
			
			if	(ie_regra_preco_w = 'S') then
				nr_seq_conta_w		:= :new.nr_seq_conta;
				cd_procedimento_w	:= :new.cd_procedimento;
				ie_origem_proced_w	:= :new.ie_origem_proced;
				
				select	nvl(nr_seq_prestador_exec,0),
					nr_seq_protocolo,
					cd_estabelecimento,
					nr_seq_tipo_acomodacao,
					nvl(dt_atendimento_referencia, nvl(dt_autorizacao, nvl(dt_entrada, sysdate))),
					nr_seq_plano,
					--substr(pls_obter_se_internado(nr_sequencia,'X'),1,1),
					nr_seq_segurado,
					nvl(cd_guia,'X'),
					ie_origem_conta
				into	nr_seq_prestador_conta_w,
					nr_seq_protocolo_w,
					cd_estabelecimento_w,
					nr_seq_tipo_acomod_w,
					dt_conta_guia_w,
					nr_seq_plano_w,
					--ie_internado_w,
					nr_seq_segurado_w,
					cd_guia_w,
					ie_origem_conta_w
				from	pls_conta
				where	nr_sequencia	= nr_seq_conta_w;
				
				ie_internado_w	:= substr(pls_obter_se_internado(nr_seq_conta_w,'X'),1,1);
				
				dt_pacote_w	:= nvl(dt_procedimento_w, dt_conta_guia_w);
				
				/* Obter o prestador do protocolo */
				select	nr_seq_prestador
				into	nr_seq_prestador_prot_w
				from	pls_protocolo_conta
				where	nr_sequencia	= nr_seq_protocolo_w;

				if	(nr_seq_prestador_conta_w > 0) then
					nr_seq_prestador_w	:= nr_seq_prestador_conta_w;
				else
					nr_seq_prestador_w	:= nr_seq_prestador_prot_w;
				end if;
				
				if	(nr_seq_plano_w is null) then		
					begin
					select	pls_obter_produto_benef(a.nr_sequencia,dt_conta_guia_w)
					into	nr_seq_plano_w
					from	pls_segurado a	
					where	a.nr_sequencia	= nr_seq_segurado_w;	
					exception
					when others then
						nr_seq_plano_w	:= null;
					end;
				end if;
				
				begin
				select	nvl(a.nr_contrato,0),
					b.nr_seq_congenere
				into	nr_contrato_w,
					nr_seq_congenere_w
				from	pls_contrato	a,
					pls_segurado	b
				where	a.nr_sequencia	= b.nr_seq_contrato
				and	b.nr_sequencia	= nr_seq_segurado_w;
				exception
				when others then
					nr_contrato_w	:= 0;
				end;
				
				if	(nr_seq_congenere_w is null) then
					select	max(b.nr_seq_congenere)
					into	nr_seq_congenere_w
					from	pls_segurado	b
					where	b.nr_sequencia	= nr_seq_segurado_w;
				end if;
				
				select	max(nr_seq_intercambio)
				into	nr_seq_intercambio_w
				from	pls_segurado
				where	nr_sequencia	= nr_seq_segurado_w;
				
				select	max(nr_seq_classificacao)
				into	nr_seq_classificacao_prest_w
				from	pls_prestador
				where	nr_sequencia = nr_seq_prestador_w;
				
				select	nvl(max(sg_estado),'X')
				into	sg_estado_w
				from	pessoa_juridica
				where	cd_cgc	= (	select	max(cd_cgc_outorgante)
							from	pls_outorgante
							where	cd_estabelecimento	= cd_estabelecimento_w);
				
				begin
				select	nvl(max(a.sg_estado),'X')
				into	sg_estado_int_w
				from	pessoa_juridica	a,
					pls_congenere	b
				where	a.cd_cgc	= b.cd_cgc
				and	b.nr_sequencia	= nr_seq_congenere_w;
				exception
				when others then
					sg_estado_int_w	:= 0;
				end;

				if	(sg_estado_w <> 'X') and
					(sg_estado_int_w <> 'X') then
					if	(sg_estado_w	= sg_estado_int_w) then
						ie_tipo_intercambio_w	:= 'E';
					else	
						ie_tipo_intercambio_w	:= 'N';
					end if;
				else
					ie_tipo_intercambio_w	:= 'A';
				end if;

				pls_define_preco_pacote(cd_estabelecimento_w,
							nr_seq_prestador_w,
							nr_seq_tipo_acomod_w,
							dt_pacote_w,
							cd_procedimento_w,
							ie_origem_proced_w,
							ie_internado_w,
							nr_seq_plano_w,
							nr_contrato_w,
							nr_seq_congenere_w,
							:new.nm_usuario,
							ie_origem_conta_w,
							ie_tipo_intercambio_w,
							nr_seq_pacote_w,
							nr_seq_regra_pacote_out_w,
							cd_proc_pacote_out_w,
							ie_origem_pacote_out_w,
							vl_pacote_out_w,
							vl_medico_out_w,
							vl_anestesista_out_w,
							vl_auxiliares_out_w,
							vl_custo_operacional_out_w,
							vl_materiais_out_w,
							nr_seq_intercambio_w,
							nr_seq_classificacao_prest_w,
							null,
							nr_seq_segurado_w,
							'N',
							'S',
							null,
							1);
			
				if	(nvl(nr_seq_regra_pacote_out_w,0) = 0) then
					nr_seq_pacote_w := 0;
				
				end if;
			end if; /* Se validar por regra de pre�o */
		end if;

		if	(nr_seq_pacote_w > 0) then
			:new.ie_tipo_despesa	:= '4';
			:new.nr_seq_pacote	:= nr_seq_pacote_w;
		else
			/* Buscar a classifica��o do procedimento e atualizar na tabela */
			select 	substr(nvl(max(ie_classificacao),nvl(:new.ie_tipo_despesa,'1')),1,1)
			into	ie_tipo_despesa_w
			from	procedimento
			where	cd_procedimento		= :new.cd_procedimento
			and	ie_origem_proced	= :new.ie_origem_proced;
			
			:new.nr_seq_pacote	:= null;
			:new.ie_tipo_despesa	:= substr(ie_tipo_despesa_w,1,1);
		end if;

		if	(updating) and
			((:old.nr_id_analise is not null) and ((:new.nr_id_analise is null) or (:old.nr_id_analise != :new.nr_id_analise)))then

			wheb_mensagem_pck.exibir_mensagem_abort(247472); 
		end if;

		if	(:new.vl_liberado < 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(271939); 
		end if;
		
		-- busca os dados para tratar as redund�ncias logo abaixo
		select	max(a.dt_atendimento_referencia),
			max(a.nr_seq_prestador_exec),
			max(b.dt_protocolo),
			max(b.dt_mes_competencia),
			max(a.cd_estabelecimento)
		into	dt_atendimento_referencia_w,
			nr_seq_prestador_exec_w,
			dt_protocolo_w,
			dt_mes_competencia_w,
			cd_estabelecimento_w
		from	pls_conta a,
			pls_protocolo_conta b
		where	a.nr_sequencia = :new.nr_seq_conta
		and	b.nr_sequencia = a.nr_seq_protocolo;
		
		if	(dt_atendimento_referencia_w is null) then
			ds_log_call_w := substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
							' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);
			
			insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
								dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
								ds_log, ds_log_call, ds_funcao_ativa, 
								ie_aplicacao_tasy, nm_maquina, pls_conta_proc, ie_opcao )
					values		( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
								sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_CONTA_PROC', 
								'Item sem data de atendimento refer�ncia', ds_log_call_w, obter_funcao_ativa, 
								'N', wheb_usuario_pck.get_machine, :old.nr_sequencia, '0');
		end if;
	end if;
	
	-- se foi alimentado o campo com os valores da redund�ncia, extrai os valores dele e alimenta as vari�veis
	if	(:new.ds_redundancia is not null) then
		
		-- percorre para trazer campo=valor em cada registro
		for r_C01_w in C01(:new.ds_redundancia, ';') loop
			
			-- percorre para trazer campo em um registro e valor em outro
			nr_contador_w := 0;
			ds_campo_w := null;
			for r_C02_w in C02(r_C01_w.ds_valor_vchr2, '=') loop
				
				-- significa que tenho o campo
				if	(nr_contador_w = 0) then
					ds_campo_w := r_C02_w.ds_valor_vchr2;
				else
					-- atribui os valores para as vari�veis
					case (ds_campo_w) 
						when 'dt_atendimento_referencia' then
							dt_atendimento_referencia_w := r_C02_w.dt_valor_date;
						
						when 'nr_seq_prestador_exec' then
							nr_seq_prestador_exec_w := r_C02_w.nr_valor_number;
						
						when 'dt_protocolo' then
							dt_protocolo_w := r_C02_w.dt_valor_date;
							
						when 'dt_mes_competencia' then
							dt_mes_competencia_w := r_C02_w.dt_valor_date;
							
						else
							null;
					end case;
				end if;
				
				nr_contador_w := nr_contador_w + 1;
			end loop;
		end loop;
		:new.ds_redundancia := null;
	end if;

	-- in�cio redund�ncias e formata��es
	:new.dt_procedimento_referencia := pls_obter_data_conta_proc(	nr_seq_prestador_exec_w, :new.ie_tipo_despesa, 
									:new.dt_procedimento, dt_atendimento_referencia_w,
									dt_protocolo_w, dt_mes_competencia_w);
									
	-- se houve mudan�a na data de refer�ncia alimenta a mesma sem a hora no campo dt_procedimento_referencia_sh
	-- criado para quest�es de performance quando existe a necessidade de filtrar tudo o que � do dia ignorando a hora
	if	(nvl(:new.dt_procedimento_referencia, to_date('01/01/2000', 'dd/mm/yyyy')) != 
		 nvl(:old.dt_procedimento_referencia, to_date('01/01/2000', 'dd/mm/yyyy'))) then
		:new.dt_procedimento_referencia_sh := trunc(:new.dt_procedimento_referencia, 'dd');
	end if;
									
	if	(:new.cd_procedimento 	is not null) 	and
		(:new.ie_origem_proced	is null)	and
		(:old.ie_origem_proced	is not null)	then
		wheb_mensagem_pck.exibir_mensagem_abort(288127); 
	elsif	(inserting) and
		(:new.cd_procedimento 	is not null) and
		(:new.ie_origem_proced	is null) then
		ds_log_glosa_w := 'Procedimento informado no entanto origem do mesmo nula ';
		  
		insert into	pls_conta_log	(	nr_sequencia,dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
							nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
							dt_alteracao, ds_alteracao)
				values		(	pls_conta_log_seq.nextval, sysdate, :new.nm_usuario,
							sysdate, :new.nm_usuario, :new.nr_seq_conta,
							:new.nr_sequencia, null, :new.nm_usuario,
							sysdate, ds_log_glosa_w ||dbms_utility.format_call_stack);
	end if;
	
	
	if	(((:old.vl_glosa > 0 and :new.vl_glosa = 0) and (:new.ie_glosa = 'S')) or
		 ((:new.vl_glosa = 0) and (:new.ie_glosa = 'S') and (:old.ie_glosa = 'N'))) then
		 
		ds_log_glosa_w := 'new.vl_glosa = '||:new.vl_glosa||' Old.vl_glosa = '||:old.vl_glosa||' new.ie_glosa ='||:new.ie_glosa||' old.ie_glosa = '||:old.ie_glosa;
		  
		insert into	pls_conta_log	(	nr_sequencia,dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
							nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
							dt_alteracao, ds_alteracao)
				values		(	pls_conta_log_seq.nextval, sysdate, :new.nm_usuario,
							sysdate, :new.nm_usuario, :new.nr_seq_conta,
							:new.nr_sequencia, null, :new.nm_usuario,
							sysdate, ds_log_glosa_w ||dbms_utility.format_call_stack);
	end if;
	
	-- OS 1900828, log para registar quando um proc fica com ie_glosa = 'S', vl_glosa = 0 e com o vl_liberado > 0
	-- podera ser removido se desejado, apos a solucao da os 1900828
	if	((:new.ie_glosa =  'S') and 
		 (:new.vl_glosa = 0) and 
		 (:new.vl_Liberado > 0)) then
	
		ds_log_glosa_w := substr(':new.ie_glosa: '||:new.ie_glosa||' - old.ie_glosa: '||:old.ie_glosa||' - :new.vl_glosa: '||:new.vl_glosa||' - :old.vl_glosa: '||:old.vl_glosa||' - :new.vl_liberado: '||:new.vl_liberado||' - :old.vl_liberado: '||:old.vl_liberado, 1, 100);
		insert into	pls_conta_log	(	nr_sequencia,dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
							nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
							dt_alteracao, ds_alteracao)
				values		(	pls_conta_log_seq.nextval, sysdate, :new.nm_usuario,
							sysdate, :new.nm_usuario, :new.nr_seq_conta,
							:new.nr_sequencia, null, :new.nm_usuario,
							sysdate, ds_log_glosa_w ||dbms_utility.format_call_stack);
	end if;
	
	-- controle OS 737623
	
	if	(:old.dt_inicio_proc is not null and :new.dt_inicio_proc is null) and
		(:new.ie_situacao	= 'D') then
		insert into	pls_conta_log	(	nr_sequencia,dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
							nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
							dt_alteracao, ds_alteracao)
				values		(	pls_conta_log_seq.nextval, sysdate, :new.nm_usuario,
							sysdate, :new.nm_usuario, :new.nr_seq_conta,
							:new.nr_sequencia, null, :new.nm_usuario,
							sysdate, 'Hora nula '||dbms_utility.format_call_stack);
	end if;
	
	if	(:new.ie_ato_cooperado is null) and
		(:new.ie_status	= 'M') and
		(updating) then
		insert into	pls_conta_log	(	nr_sequencia,dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
							nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
							dt_alteracao, ds_alteracao)
				values		(	pls_conta_log_seq.nextval, sysdate, :new.nm_usuario,
							sysdate, :new.nm_usuario, :new.nr_seq_conta,
							:new.nr_sequencia, null, :new.nm_usuario,
							sysdate, 'Ato cooperado n�o identificado '||dbms_utility.format_call_stack);
	end if;
	
	-- Adiciona a data do procedimento no dt_inicio_proc
	-- Necess�rio pois na gest�o de contas m�dicas a data de inicio pode vir como 30/12/1899, e a data de fim como a data atual.
	-- Na valoriza��o essa situa��o � problematica quando existe regra para verificar a diferen�a entre minutos, gerando um travamento devido 
	-- a grande diferen�a entre as datas.
	
	if	((:new.dt_inicio_proc is not null) and (:new.dt_procedimento is not null)) then
	
		:new.dt_inicio_proc	:= to_date(to_char(:new.dt_procedimento, 'dd/mm/yyyy')|| ' ' || nvl(to_char(:new.dt_inicio_proc, 'hh24:mi:ss'), '00:00:00'), 'dd/mm/yyyy hh24:mi:ss');	
	end if;
	
	-- Adiciona a data do procedimento no dt_fim_proc
	-- Necess�rio pois na gest�o de contas m�dicas a data de inicio pode vir como 30/12/1899, e a data de fim como a data atual.
	-- Na valoriza��o essa situa��o � problematica quando existe regra para verificar a diferen�a entre minutos, gerando um travamento devido 
	-- a grande diferen�a entre as datas.
	if	((:new.dt_fim_proc is not null) and (:new.dt_procedimento is not null)) then
	
		:new.dt_fim_proc	:= to_date(to_char(:new.dt_procedimento, 'dd/mm/yyyy')|| ' ' || nvl(to_char(:new.dt_fim_proc, 'hh24:mi:ss'), '00:00:00'), 'dd/mm/yyyy hh24:mi:ss');
	end if;
	
	if	((:old.vl_pag_medico_conta <= :old.vl_liberado)) and
		((:new.vl_pag_medico_conta > :new.vl_liberado) and (:new.vl_liberado > 0 )) then
		insert into	pls_conta_log	(	nr_sequencia,dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
							nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
							dt_alteracao, ds_alteracao)
				values		(	pls_conta_log_seq.nextval, sysdate, :new.nm_usuario,
							sysdate, :new.nm_usuario, :new.nr_seq_conta,
							:new.nr_sequencia, null, :new.nm_usuario,
							sysdate, 'Pag m�dico superior vl liberado '||:new.vl_pag_medico_conta||' vl medico '||:new.vl_liberado||' '||dbms_utility.format_call_stack);
	end if;
		
	/* Francisco - OS 812883 - Se mudar a data de um procedimento precisa recalcular a data do atendimento */
	if	(:new.dt_procedimento <> :old.dt_procedimento) or
		(:old.dt_procedimento is null and :new.dt_procedimento is not null) then
		
		wheb_usuario_pck.set_ie_executar_trigger('N');
		
		update	pls_conta
		set		dt_atendimento_referencia	= null
		where	nr_sequencia	= :new.nr_seq_conta;
		
		wheb_usuario_pck.set_ie_executar_trigger('S');
		
	end if;
	
	select	nvl(max(ie_desc_item_glosa_atend),'N')
	into	ie_desc_item_glosa_atend_w
	from	pls_parametros
	where	cd_estabelecimento = cd_estabelecimento_w;

	-- OS 1100687 - Se o item for glosado 100% e se a data dele for a data da conta	
	if 	((((nvl(:old.ie_glosa,'N') = 'N') and (:new.ie_glosa = 'S')) or 
		((nvl(:old.ie_glosa,'N') = 'S') and (nvl(:new.ie_glosa,'N') = 'N'))) and	
		(ie_desc_item_glosa_atend_w = 'S') and
		(:new.dt_procedimento_referencia = dt_atendimento_referencia_w)) then
	
		wheb_usuario_pck.set_ie_executar_trigger('N');
		
		update	pls_conta 
		set	dt_atendimento_referencia =	null
		where	nr_sequencia = :new.nr_seq_conta;

		wheb_usuario_pck.set_ie_executar_trigger('S');

	end if;  
		
	--Durante o processo de importa��o o campo dt_atendimento_imp_referencia n�o estava sendo atualizado � medida que os procedimentos eram inseridos.
	if	(:new.dt_procedimento_imp <> :old.dt_procedimento_imp) or
		(:old.dt_procedimento_imp is null and :new.dt_procedimento_imp is not null) then
		
		wheb_usuario_pck.set_ie_executar_trigger('N');
		
		update	pls_conta
		set	dt_atendimento_imp_referencia	= null
		where	nr_sequencia	= :new.nr_seq_conta;
		
		wheb_usuario_pck.set_ie_executar_trigger('S');
	end if;
	
	if	(inserting) and
		(:new.ie_via_acesso is not null and :new.ie_via_acesso_imp is null) then
		:new.ie_via_acesso_imp := :new.ie_via_acesso;
	end if;
end if;
--� necess�rio que estes controles fiquem aqui fora pois independente do processo precisa ser mantida a consist�ncia
if	(:old.ie_status = 'D') and
	((:new.ie_status <> 'D') or
	(:new.ie_status is null)) and
	(:new.nr_seq_regra_canc_item_orig is not null) then
	:new.ie_status := 'D';
end if;
/*O item com status faturamento manual, n�o pode ser alterado de modo a n�o interferir no pagamento*/
if	(:old.ie_status	= 'M') and
	(:old.ie_status	!= :new.ie_status) then
	:new.ie_status	:= 'M';
end if;
		
end;
/
