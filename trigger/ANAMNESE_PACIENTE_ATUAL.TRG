create or replace trigger anamnese_paciente_atual
before insert or update on anamnese_paciente
for each row

declare

qt_reg_w	number(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(:old.DT_ANANMESE,sysdate+10) <> :new.DT_ANANMESE) and
	(:new.DT_ANANMESE is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_ANANMESE,'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

begin
	if	(:new.nr_atendimento is not null) and
		(:new.cd_pessoa_fisica is null) then
		:new.cd_pessoa_fisica := obter_pessoa_Atendimento(:new.nr_atendimento,'C');
	end if;

exception
	when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;

end;
/
