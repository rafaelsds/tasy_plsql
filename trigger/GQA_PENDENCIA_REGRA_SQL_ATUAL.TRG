create or replace trigger gqa_pendencia_regra_sql_atual
before insert or update on gqa_pendencia_regra_sql
for each row
declare
customSQL_w     varchar2(4000);
ie_resultado_w  varchar2(02);
begin
 
  if (:new.ie_situacao = 'A') then 
          
    customSQL_w :=  ' select nvl(max(''S''), ''N'') ' ||
                    ' from  atendimento_paciente a'     ||
                    ' where  a.nr_atendimento = -1  '   ||
                    :new.DS_SQL ;

    begin
      execute immediate customSQL_w
      into   ie_resultado_w;
   
    exception
    when others then
      Wheb_mensagem_pck.exibir_mensagem_abort(751853, 'SQL=' || customSQL_w || ';ERRO='||sqlerrm);
    end;
  
  		
  end if;

end;
/