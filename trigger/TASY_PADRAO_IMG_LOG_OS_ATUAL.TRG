CREATE OR REPLACE trigger TASY_PADRAO_IMG_LOG_OS_ATUAL
before insert or update or delete on TASY_PADRAO_IMAGEM
for each row

declare

operation_id_w	varchar2(1);
nr_sequencia_w	reg_object_log_os.nr_seq_documento%type;

begin

if	inserting then
	operation_id_w 	:= 'I';
	nr_sequencia_w	:= :new.nr_sequencia;
elsif	updating then
	operation_id_w := 'U';
	nr_sequencia_w	:= :new.nr_sequencia;
elsif	deleting then
	operation_id_w := 'D';
	nr_sequencia_w	:= :old.nr_sequencia;
end if;

GENERATE_REG_OBJECT_LOG_OS(wheb_usuario_pck.get_nm_usuario,
			operation_id_w,
			'TASY_PADRAO_IMAGEM',
			nr_sequencia_w);

end;
/