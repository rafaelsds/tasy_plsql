create or replace trigger atual_reg_consis_inf_rep
before insert or update on regra_consiste_inf_rep
for each row

declare

begin

if (:new.hr_inicial is not null) and
   ((:new.hr_inicial <> :old.hr_inicial) or (:old.dt_inicial is null)) then
	:new.dt_inicial := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicial || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

end;
/