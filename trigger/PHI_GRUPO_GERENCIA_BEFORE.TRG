create or replace trigger phi_grupo_gerencia_before
before insert or update or delete on gerencia_wheb_grupo
for each row

declare

nr_sequencia_w		number(10);
qt_reg_w		number(10);
ds_quebra_linha_w	varchar2(10) := chr(13) || chr(10);
ds_call_stack_w		varchar2(4096) := substr(dbms_utility.format_call_stack,1,4096);
ds_usuario_w		usuario.ds_usuario%type;

Cursor c01 (nr_seq_grupo_p number) is
	select	a.nr_sequencia,
		a.nm_usuario_grupo,
		b.ds_usuario
	from	usuario b,
		gerencia_wheb_grupo_usu a
	where	b.nm_usuario = a.nm_usuario_grupo
	and	a.nr_seq_grupo = nr_seq_grupo_p
	and	nvl(a.ie_emprestimo,'N') = 'N'
	and	nvl(a.dt_fim,trunc(sysdate)+1) > trunc(sysdate);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	null;
end if;

if	(:old.nr_sequencia is not null) then
	begin
	nr_sequencia_w := substr(nvl(phi_obter_dados_grupo_usu(:old.nr_sequencia,null,null,null,null,null,null,'GD'),
					phi_obter_dados_grupo_usu(:old.nr_sequencia,null,null,null,null,null,null,'GS')),1,10);

	if	(nr_sequencia_w is not null) and
		(instr(ds_call_stack_w,'PHI_GRUPO_DESENV_BEFORE') = 0) and
		(instr(ds_call_stack_w,'PHI_GRUPO_DESENV_AFTER') = 0) and
		(instr(ds_call_stack_w,'PHI_GRUPO_SUPORTE_BEFORE') = 0) and
		(instr(ds_call_stack_w,'PHI_GRUPO_SUPORTE_AFTER') = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1174199); --Changing the group is not allowed.
	end if;
	end;
end if;

if	(nr_sequencia_w is null) then
	begin
	if	(updating) and
		(:new.ie_situacao <> :old.ie_situacao) and
		(:new.ie_situacao = 'I') then
		begin
		select	count(1)
		into	qt_reg_w
		from	gerencia_wheb_grupo_usu
		where	nr_seq_grupo = :new.nr_sequencia
		and	ie_emprestimo = 'S'
		and	dt_fim > trunc(sysdate)
		and	rownum = 1;

		if	(qt_reg_w = 1) then
			wheb_mensagem_pck.exibir_mensagem_abort(1174774); --Cannot deactivate the group, as there are valid user loans that need to be adjusted first.
		end if;

		begin
		for c01_w in c01 (:new.nr_sequencia) loop
			begin
			ds_usuario_w := c01_w.ds_usuario;

			update	gerencia_wheb_grupo_usu
			set	dt_atualizacao = sysdate,
				nm_usuario = :new.nm_usuario,
				dt_fim = trunc(sysdate)
			where	nr_sequencia = c01_w.nr_sequencia;
			end;
		end loop;
		exception
		when others then --Cannot deactivate this record.
			if	(sqlcode = -20011) then
				wheb_mensagem_pck.exibir_mensagem_abort(obter_desc_expressao(1058685) || ds_quebra_linha_w || ds_quebra_linha_w || obter_desc_expressao(328225) || ' ' || ds_usuario_w || ds_quebra_linha_w || dbms_utility.format_error_stack);
			else
				wheb_mensagem_pck.exibir_mensagem_abort(1174775);
			end if;
		end;

		end;
	elsif	(deleting) then
		begin
		delete from gerencia_wheb_grupo_usu where nr_seq_grupo = :old.nr_sequencia;
		exception
		when others then --Cannot delete this record as there are dependent records.
			wheb_mensagem_pck.exibir_mensagem_abort(1174776);
		end;
	end if;
	end;
end if;

end;
/