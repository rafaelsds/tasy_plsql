create or replace trigger REGRA_CONDICAO_REG_LOG
before insert or update or delete on REGRA_CONDICAO
for each row

declare

operation_id_w		varchar2(1);
nr_seq_objeto_w		number(10);
nr_seq_obj_sch_leg_w	number(10);
nr_seq_obj_sch_w	number(10);
nr_seq_opcao_crud_w	number(10);
cd_funcao_w		funcao.cd_funcao%type;


begin

if	inserting then
	operation_id_w 		:= 'I';
	nr_seq_objeto_w		:= :new.nr_seq_objeto;
	nr_seq_obj_sch_leg_w	:= :new.nr_seq_obj_sch_leg;
	nr_seq_opcao_crud_w	:= :new.nr_seq_opcao_crud;
elsif	updating then
	operation_id_w 		:= 'U';
	nr_seq_objeto_w		:= :new.nr_seq_objeto;
	nr_seq_obj_sch_leg_w	:= :new.nr_seq_obj_sch_leg;
	nr_seq_opcao_crud_w	:= :new.nr_seq_opcao_crud;
elsif	deleting then
	operation_id_w 		:= 'D';
	nr_seq_objeto_w		:= :old.nr_seq_objeto;
	nr_seq_obj_sch_leg_w	:= :old.nr_seq_obj_sch_leg;
	nr_seq_opcao_crud_w	:= :old.nr_seq_opcao_crud;
end if;

if	(nr_seq_obj_sch_leg_w is not null) then

	begin
	select	nr_seq_objeto
	into	nr_seq_obj_sch_w
	from	objeto_schematic_legenda
	where	nr_sequencia	= nr_seq_obj_sch_leg_w
	and	rownum		= 1;	
	exception
	when others then
		nr_seq_obj_sch_w := null;
	end;
	
	if	(nr_seq_obj_sch_w is not null) then

		select	max(cd_funcao)
		into	cd_funcao_w
		from	objeto_schematic
		where	nr_sequencia = nr_seq_obj_sch_w;

		GENERATE_REG_OBJECT_LOG(wheb_usuario_pck.get_nm_usuario, 
					operation_id_w, 
					null,
					nr_seq_obj_sch_w,
					cd_funcao_w,
					'REGRA_CONDICAO',
					nvl(:old.nr_sequencia,:new.nr_sequencia));
	end if;
	
elsif	(nr_seq_opcao_crud_w is not null) then

	begin
	select	nr_seq_objeto_schematic
	into	nr_seq_obj_sch_w
	from	opcoes_crud
	where	nr_sequencia	= nr_seq_opcao_crud_w
	and	rownum		= 1;	
	exception
	when others then
		nr_seq_opcao_crud_w := null;
	end;
	
	if	(nr_seq_obj_sch_w is not null) then

		select	max(cd_funcao)
		into	cd_funcao_w
		from	objeto_schematic
		where	nr_sequencia = nr_seq_obj_sch_w;

		GENERATE_REG_OBJECT_LOG(wheb_usuario_pck.get_nm_usuario, 
					operation_id_w, 
					null,
					nr_seq_obj_sch_w,
					cd_funcao_w,
					'REGRA_CONDICAO',
					nvl(:old.nr_sequencia,:new.nr_sequencia));
	end if;
	
elsif	(nr_seq_objeto_w is not null) then

	select	max(cd_funcao)
	into	cd_funcao_w
	from	dic_objeto
	where	nr_sequencia = nr_seq_objeto_w;

	GENERATE_REG_OBJECT_LOG(wheb_usuario_pck.get_nm_usuario, 
				operation_id_w, 
				nr_seq_objeto_w,
				null,
				cd_funcao_w,
				'REGRA_CONDICAO',
				nvl(:old.nr_sequencia,:new.nr_sequencia));	
end if;

end;
/