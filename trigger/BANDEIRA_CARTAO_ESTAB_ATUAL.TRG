create or replace trigger bandeira_cartao_estab_atual
before insert or update on bandeira_cartao_estab
for each row
declare

ds_conta_contabil_w		varchar2(40);

begin
	IF(WHEB_USUARIO_PCK.GET_IE_EXECUTAR_TRIGGER = 'N')THEN 
		RETURN;
	END IF;


ds_conta_contabil_w		:= substr(wheb_mensagem_pck.get_texto(354087,''),1,40);

ctb_consistir_conta_titulo(:new.cd_conta_contabil, ds_conta_contabil_w);

ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_contabil, ds_conta_contabil_w);

end;
/