create or replace TRIGGER KM_PRODUCT_CHECKLIST
AFTER UPDATE ON KM_PRODUCT_CHECKLIST
FOR EACH ROW 
DECLARE
   BEGIN
	
	if(wheb_usuario_pck.get_ie_executar_trigger    = 'S')  then
		IF (:OLD.NR_SCORE != :NEW.NR_SCORE) THEN 
		UPDATE KM_PROD_CHKLIST_BY_USER
		set		IE_SITUACAO	= 'I' ,ANSWER ='N'
        where	NR_SEQ_CHECK_LIST = :NEW.NR_SEQUENCIA;
		END IF;
	end if;
END;
/
