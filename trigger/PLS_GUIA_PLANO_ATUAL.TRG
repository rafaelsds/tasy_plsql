create or replace trigger pls_guia_plano_atual
before insert or update on pls_guia_plano
for each row

declare

vl_procedimentos_w	Number(15,2);
vl_materiais_w		Number(15,4);

begin

if	(:new.nr_seq_segurado is not null) then

	select	nvl(max(ie_tipo_segurado),'B')
	into	:new.ie_tipo_segurado
	from	pls_segurado
	where	nr_sequencia	= :new.nr_seq_segurado;
	
	/* Francisco - 23/05/2012 - Performance */
	if	(nvl(:new.cd_guia, 'X') <> nvl(:old.cd_guia, 'X')) then
		:new.cd_guia_pesquisa	:= pls_converte_cd_guia_pesquisa(:new.cd_guia);
	end if;
end if;
	
-- se for guia de SP/SADT e prorroga��o de interna��o verifica se tem uma guia principal
-- se tiver ela deve ser considerada
if	(:new.ie_tipo_guia in ('2', '8')) then
	if	(:new.nr_seq_guia_principal is not null) then
		:new.nr_seq_guia_ok := :new.nr_seq_guia_principal;
	end if;
end if;
-- se acima n�o atribuiu valor lan�a o valor da sequencia
if	(:new.nr_seq_guia_ok is null) then
	:new.nr_seq_guia_ok := :new.nr_sequencia;
end if;

select	nvl(sum(vl_procedimento*qt_autorizada),0)
into	vl_procedimentos_w
from	pls_guia_plano_proc
where	nr_seq_guia	= :new.nr_sequencia;

if (vl_procedimentos_w = 0) then
	select	nvl(sum(vl_procedimento*qt_solicitada),0)
	into	vl_procedimentos_w
	from	pls_guia_plano_proc
	where	nr_seq_guia	= :new.nr_sequencia;
end if;

select	nvl(sum(vl_material*qt_autorizada),0)
into	vl_materiais_w
from	pls_guia_plano_mat
where	nr_seq_guia	= :new.nr_sequencia;

if (vl_materiais_w = 0) then
	select	nvl(sum(vl_material*qt_solicitada),0)
	into	vl_materiais_w
	from	pls_guia_plano_mat
	where	nr_seq_guia	= :new.nr_sequencia;
end if;

:new.vl_autorizacao	:= vl_procedimentos_w + vl_materiais_w;

end pls_guia_plano_atual;
/
