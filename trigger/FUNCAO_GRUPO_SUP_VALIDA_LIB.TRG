create or replace trigger funcao_grupo_sup_valida_lib
before insert or delete or update on funcao_grupo_sup
for each row

declare
qt_reg_w		number(10);
nr_seq_gerencia_w 	grupo_suporte.nr_seq_gerencia_sup%type;

pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin

	if	((updating and :old.cd_funcao <> :new.cd_funcao) or deleting) then
		begin
		select	count(1)
		into	qt_reg_w
		from	funcao f
		where	f.cd_funcao = :old.cd_funcao
		and 	f.ie_situacao <> 'I';
	
		if	(qt_reg_w > 0) then
			begin
			select	count(1)
			into	qt_reg_w
			from	funcao_grupo_sup fgs,
				grupo_suporte gs
			where	fgs.nr_seq_grupo = gs.nr_sequencia
			and	fgs.cd_funcao = :old.cd_funcao
			and	gs.ie_situacao =  'A'
			and	fgs.nr_sequencia <> :old.nr_sequencia;
	
			if	(qt_reg_w = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(1117670);
			end if;
			end;
		end if;
		end;
	end if;
	
	if	(inserting) then
		begin
		select 	gs.nr_seq_gerencia_sup 
		into    nr_seq_gerencia_w
		from    grupo_suporte gs
		where   gs.nr_sequencia = :new.nr_seq_grupo;
	
		select  count(*) 
		into    qt_reg_w
		from    usuario_grupo_sup ugs,
			grupo_suporte gs
		where   gs.nr_sequencia = ugs.nr_seq_grupo
		and     gs.ie_situacao = 'A'
		and     gs.nr_seq_gerencia_sup = nr_seq_gerencia_w
		and     (ugs.nm_usuario_grupo = :new.nm_usuario_nrec or gs.nm_usuario_lider = :new.nm_usuario_nrec);
		
		if 	(qt_reg_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1117757);
		end if;
		end;
	end if;
	end;
end if;

end;
/