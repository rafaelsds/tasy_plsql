create or replace
trigger compl_pessoa_fisica_atual_hl7
after insert or update on compl_pessoa_fisica
for each row

declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
ie_existe_atend_w 	number(10);
ds_param_integration_w	varchar2(500);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	ds_sep_bv_w := obter_separador_bv;

	ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
				'nr_atendimento=0' || ds_sep_bv_w ||
				'nr_seq_interno=0' || ds_sep_bv_w;
	gravar_agend_integracao(18, ds_param_integ_hl7_w);
	
	
	select 	count(*)
	into 	ie_existe_atend_w
	from	atendimento_paciente
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;
	
	if	(ie_existe_atend_w > 0) then
		gravar_agend_integracao(426, ds_param_integ_hl7_w);
	end if;
	
	if (wheb_usuario_pck.is_evento_ativo(853) = 'S') then
		enviar_pf_medico_hl7(:new.cd_pessoa_fisica,'MUP');		
	end if;
	
	call_bifrost_content('patient.address.update','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);
	
end if;
end;
/
