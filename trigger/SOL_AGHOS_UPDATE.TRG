create or replace trigger sol_aghos_update
before update on solicitacao_aghos_imp
for each row


declare

nr_seq_solicit_w	number(10);

begin

begin
	select	nr_internacao
	into	nr_seq_solicit_w
	from	solicitacao_tasy_aghos
	where	nr_internacao = :new.nr_internacao
	and	ie_situacao = 'AU'
	and	rownum = 1;
exception
	when	no_data_found then
		nr_seq_solicit_w := 0;
end;

If	(nvl(nr_seq_solicit_w, 0) > 0) then

	update	solicitacao_tasy_aghos
	set	nr_atendimento = :new.nr_atend_int,
		ie_situacao = 'I',
		ds_motivo_situacao = 'Atendimento internado no Tasy'
	where	nr_internacao = nr_seq_solicit_w;

	:new.ie_integrada := 'S';
	
end if;

end;
/




