create or replace
trigger ATEND_PACIENTE_UND_INF_ATUAL
after insert or update on ATEND_PACIENTE_UNIDADE_INF
for each row

declare

CD_MEDICO_ATENDIMENTO_W		ATEND_PACIENTE_UNIDADE_INF.CD_PROF_MEDICO%type;
NR_SEQ_ATEND_PAC_UNIDADE_W		ATEND_PACIENTE_UNIDADE_INF.NR_SEQ_ATEND_PAC_UNIDADE%type;

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
    		NR_SEQ_ATEND_PAC_UNIDADE_W	:=	nvl(:new.NR_SEQ_ATEND_PAC_UNIDADE, :old.NR_SEQ_ATEND_PAC_UNIDADE);
    		CD_MEDICO_ATENDIMENTO_W		:=	nvl(:new.CD_PROF_MEDICO, :old.CD_PROF_MEDICO);
    
		if 	(CD_MEDICO_ATENDIMENTO_W is not null)	then
			update 	ATENDIMENTO_PACIENTE ap
			set	ap.CD_MEDICO_RESP = CD_MEDICO_ATENDIMENTO_W
			where	ap.NR_ATENDIMENTO = (select   	apu.NR_ATENDIMENTO
							from      	ATEND_PACIENTE_UNIDADE apu
							where     	apu.NR_SEQ_INTERNO = NR_SEQ_ATEND_PAC_UNIDADE_W);
		end if;
	end if;
end ATEND_PACIENTE_UND_INF_ATUAL;
/
