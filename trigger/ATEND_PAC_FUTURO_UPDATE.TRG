create or replace trigger atend_pac_futuro_update
before update on atend_pac_futuro
for each row


begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	if 	(:old.nr_seq_status is not null) and
		(:new.nr_seq_status is not null) and
		(:new.nr_seq_status <> :old.nr_seq_status) then
	
		gerar_atend_futuro_status_hist(:new.nr_sequencia,:new.nr_seq_status,:new.nm_usuario);
	end if;
end if;

end;
/