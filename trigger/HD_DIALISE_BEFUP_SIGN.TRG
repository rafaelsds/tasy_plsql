create or replace trigger hd_dialise_befup_sign
before update on hd_dialise
for each row

declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'C'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_cancelamento		<> :new.dt_cancelamento
		or :old.ds_observacao		<> :new.ds_observacao		
		or :old.nr_seq_motivo_fim	<> :new.nr_seq_motivo_fim
		or :old.cd_pf_cancelamento	<> :new.cd_pf_cancelamento
		or (:old.dt_cancelamento 	is null		and :new.dt_cancelamento 	is not null)
		or (:old.ds_observacao 		is null		and :new.ds_observacao 		is not null)
		or (:old.nr_seq_motivo_fim 	is null		and :new.nr_seq_motivo_fim 	is not null)
		or (:old.cd_pf_cancelamento 	is null		and :new.cd_pf_cancelamento 	is not null)
		or (:old.dt_cancelamento 	is not null	and :new.dt_cancelamento 	is null)
		or (:old.ds_observacao 		is not null	and :new.ds_observacao 		is null)
		or (:old.nr_seq_motivo_fim 	is not null	and :new.nr_seq_motivo_fim 	is null)
		or (:old.cd_pf_cancelamento 	is not null	and :new.cd_pf_cancelamento 	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'CP'
and	dt_liberacao is not null
and	dt_inativacao is null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.qt_pa_sist_pre_deitado		<> :new.qt_pa_sist_pre_deitado
		or :old.qt_pa_diast_pre_deitado         <> :new.qt_pa_diast_pre_deitado
		or :old.qt_pa_sist_pre_pe               <> :new.qt_pa_sist_pre_pe
		or :old.qt_pa_diast_pre_pe              <> :new.qt_pa_diast_pre_pe
		or :old.qt_peso_pre                     <> :new.qt_peso_pre
		or :old.qt_peso_ideal                   <> :new.qt_peso_ideal
		or :old.dt_instalacao                   <> :new.dt_instalacao
		or :old.cd_pf_instalacao                <> :new.cd_pf_instalacao
		or :old.nr_seq_motivo_peso_pre          <> :new.nr_seq_motivo_peso_pre
		or :old.nr_seq_motivo_pa_deitado_pre    <> :new.nr_seq_motivo_pa_deitado_pre
		or :old.nr_seq_motivo_pa_pe_pre         <> :new.nr_seq_motivo_pa_pe_pre
		or :old.qt_soro_reposicao               <> :new.qt_soro_reposicao
		or :old.qt_soro_devolucao               <> :new.qt_soro_devolucao
		or :old.nr_seq_tipo_soro                <> :new.nr_seq_tipo_soro
		or :old.qt_gpid                         <> :new.qt_gpid
		or :old.qt_valor_gpid                   <> :new.qt_valor_gpid
		or (:old.qt_pa_sist_pre_deitado 	is null		and :new.qt_pa_sist_pre_deitado 	is not null)
		or (:old.qt_pa_diast_pre_deitado 	is null		and :new.qt_pa_diast_pre_deitado 	is not null)
		or (:old.qt_pa_sist_pre_pe 		is null		and :new.qt_pa_sist_pre_pe 		is not null)
		or (:old.qt_pa_diast_pre_pe 		is null		and :new.qt_pa_diast_pre_pe 		is not null)
		or (:old.qt_peso_pre 			is null		and :new.qt_peso_pre 			is not null)
		or (:old.qt_peso_ideal 			is null		and :new.qt_peso_ideal 			is not null)
		or (:old.dt_instalacao 			is null		and :new.dt_instalacao 			is not null)
		or (:old.cd_pf_instalacao 		is null		and :new.cd_pf_instalacao 		is not null)
		or (:old.nr_seq_motivo_peso_pre 	is null		and :new.nr_seq_motivo_peso_pre 	is not null)
		or (:old.nr_seq_motivo_pa_deitado_pre 	is null		and :new.nr_seq_motivo_pa_deitado_pre 	is not null)
		or (:old.nr_seq_motivo_pa_pe_pre 	is null		and :new.nr_seq_motivo_pa_pe_pre 	is not null)
		or (:old.qt_soro_reposicao 		is null		and :new.qt_soro_reposicao 		is not null)
		or (:old.qt_soro_devolucao 		is null		and :new.qt_soro_devolucao 		is not null)
		or (:old.nr_seq_tipo_soro 		is null		and :new.nr_seq_tipo_soro 		is not null)
		or (:old.qt_gpid 			is null		and :new.qt_gpid 			is not null)
		or (:old.qt_valor_gpid 			is null		and :new.qt_valor_gpid 			is not null)
		or (:old.qt_pa_sist_pre_deitado 	is not null	and :new.qt_pa_sist_pre_deitado 	is null)
		or (:old.qt_pa_diast_pre_deitado 	is not null	and :new.qt_pa_diast_pre_deitado 	is null)
		or (:old.qt_pa_sist_pre_pe 		is not null	and :new.qt_pa_sist_pre_pe 		is null)
		or (:old.qt_pa_diast_pre_pe 		is not null	and :new.qt_pa_diast_pre_pe 		is null)
		or (:old.qt_peso_pre 			is not null	and :new.qt_peso_pre 			is null)
		or (:old.qt_peso_ideal 			is not null	and :new.qt_peso_ideal 			is null)
		or (:old.dt_instalacao 			is not null	and :new.dt_instalacao 			is null)
		or (:old.cd_pf_instalacao 		is not null	and :new.cd_pf_instalacao 		is null)
		or (:old.nr_seq_motivo_peso_pre 	is not null	and :new.nr_seq_motivo_peso_pre 	is null)
		or (:old.nr_seq_motivo_pa_deitado_pre 	is not null	and :new.nr_seq_motivo_pa_deitado_pre 	is null)
		or (:old.nr_seq_motivo_pa_pe_pre 	is not null	and :new.nr_seq_motivo_pa_pe_pre 	is null)
		or (:old.qt_soro_reposicao 		is not null	and :new.qt_soro_reposicao 		is null)
		or (:old.qt_soro_devolucao 		is not null	and :new.qt_soro_devolucao 		is null)
		or (:old.nr_seq_tipo_soro 		is not null	and :new.nr_seq_tipo_soro 		is null)
		or (:old.qt_gpid 			is not null	and :new.qt_gpid 			is null)
		or (:old.qt_valor_gpid 			is not null	and :new.qt_valor_gpid 			is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'ID'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_inicio_dialise		<> :new.dt_inicio_dialise
		or :old.cd_pf_inicio_dialise    <> :new.cd_pf_inicio_dialise
		or (:old.dt_inicio_dialise 	is null		and :new.dt_inicio_dialise 	is not null)
		or (:old.cd_pf_inicio_dialise 	is null		and :new.cd_pf_inicio_dialise 	is not null)
		or (:old.dt_inicio_dialise 	is not null	and :new.dt_inicio_dialise 	is null)
		or (:old.cd_pf_inicio_dialise 	is not null	and :new.cd_pf_inicio_dialise 	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'FD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.qt_pa_sist_pos_pe			<> :new.qt_pa_sist_pos_pe
		or :old.qt_pa_diast_pos_pe              <> :new.qt_pa_diast_pos_pe
		or :old.qt_pa_sist_pos_deitado          <> :new.qt_pa_sist_pos_deitado
		or :old.qt_pa_diast_pos_deitado         <> :new.qt_pa_diast_pos_deitado
		or :old.qt_peso_pos                     <> :new.qt_peso_pos
		or :old.dt_fim_dialise                  <> :new.dt_fim_dialise
		or :old.cd_pf_fim_dialise               <> :new.cd_pf_fim_dialise
		or :old.nr_seq_motivo_peso_pos          <> :new.nr_seq_motivo_peso_pos
		or :old.nr_seq_motivo_pa_deitado_pos    <> :new.nr_seq_motivo_pa_deitado_pos
		or :old.nr_seq_motivo_pa_pe_pos         <> :new.nr_seq_motivo_pa_pe_pos
		or :old.qt_freq_cardiaca                <> :new.qt_freq_cardiaca
		or :old.qt_temp_axiliar_pos             <> :new.qt_temp_axiliar_pos
		or :old.qt_ultrafiltracao               <> :new.qt_ultrafiltracao
		or :old.qt_tempo_dialise                <> :new.qt_tempo_dialise
		or (:old.qt_pa_sist_pos_pe		is null		and :new.qt_pa_sist_pos_pe		is not null)
		or (:old.qt_pa_diast_pos_pe		is null		and :new.qt_pa_diast_pos_pe		is not null)
		or (:old.qt_pa_sist_pos_deitado		is null		and :new.qt_pa_sist_pos_deitado		is not null)
		or (:old.qt_pa_diast_pos_deitado	is null		and :new.qt_pa_diast_pos_deitado	is not null)
		or (:old.qt_peso_pos			is null		and :new.qt_peso_pos			is not null)
		or (:old.dt_fim_dialise			is null		and :new.dt_fim_dialise			is not null)
		or (:old.cd_pf_fim_dialise		is null		and :new.cd_pf_fim_dialise		is not null)
		or (:old.nr_seq_motivo_peso_pos		is null		and :new.nr_seq_motivo_peso_pos		is not null)
		or (:old.nr_seq_motivo_pa_deitado_pos	is null		and :new.nr_seq_motivo_pa_deitado_pos	is not null)
		or (:old.nr_seq_motivo_pa_pe_pos	is null		and :new.nr_seq_motivo_pa_pe_pos	is not null)
		or (:old.qt_freq_cardiaca		is null		and :new.qt_freq_cardiaca		is not null)
		or (:old.qt_temp_axiliar_pos		is null		and :new.qt_temp_axiliar_pos		is not null)
		or (:old.qt_ultrafiltracao		is null		and :new.qt_ultrafiltracao		is not null)
		or (:old.qt_tempo_dialise		is null		and :new.qt_tempo_dialise		is not null)
		or (:old.qt_pa_sist_pos_pe		is not null	and :new.qt_pa_sist_pos_pe		is null)
		or (:old.qt_pa_diast_pos_pe		is not null	and :new.qt_pa_diast_pos_pe		is null)
		or (:old.qt_pa_sist_pos_deitado		is not null	and :new.qt_pa_sist_pos_deitado		is null)
		or (:old.qt_pa_diast_pos_deitado	is not null	and :new.qt_pa_diast_pos_deitado	is null)
		or (:old.qt_peso_pos			is not null	and :new.qt_peso_pos			is null)
		or (:old.dt_fim_dialise			is not null	and :new.dt_fim_dialise			is null)
		or (:old.cd_pf_fim_dialise		is not null	and :new.cd_pf_fim_dialise		is null)
		or (:old.nr_seq_motivo_peso_pos		is not null	and :new.nr_seq_motivo_peso_pos		is null)
		or (:old.nr_seq_motivo_pa_deitado_pos	is not null	and :new.nr_seq_motivo_pa_deitado_pos	is null)
		or (:old.nr_seq_motivo_pa_pe_pos	is not null	and :new.nr_seq_motivo_pa_pe_pos	is null)
		or (:old.qt_freq_cardiaca		is not null	and :new.qt_freq_cardiaca		is null)
		or (:old.qt_temp_axiliar_pos		is not null	and :new.qt_temp_axiliar_pos		is null)
		or (:old.qt_ultrafiltracao		is not null	and :new.qt_ultrafiltracao		is null)
		or (:old.qt_tempo_dialise		is not null	and :new.qt_tempo_dialise		is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'GH'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia		<> :new.nr_sequencia
		or :old.dt_atualizacao_nrec	<> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec		<> :new.nm_usuario_nrec
		or :old.cd_pessoa_fisica	<> :new.cd_pessoa_fisica
		or :old.dt_dialise		<> :new.dt_dialise
		or :old.nr_seq_unid_dialise	<> :new.nr_seq_unid_dialise
		or :old.nr_atendimento		<> :new.nr_atendimento
		or :old.nr_dialise_atend_pac	<> :new.nr_dialise_atend_pac
		or :old.ie_tipo_dialise		<> :new.ie_tipo_dialise
		or :old.ie_paciente_agudo	<> :new.ie_paciente_agudo
		or (:old.nr_sequencia		is null		and :new.nr_sequencia		is not null)
		or (:old.dt_atualizacao_nrec	is null		and :new.dt_atualizacao_nrec	is not null)
		or (:old.nm_usuario_nrec	is null		and :new.nm_usuario_nrec	is not null)
		or (:old.cd_pessoa_fisica	is null		and :new.cd_pessoa_fisica	is not null)
		or (:old.dt_dialise		is null		and :new.dt_dialise		is not null)
		or (:old.nr_seq_unid_dialise	is null		and :new.nr_seq_unid_dialise	is not null)
		or (:old.nr_atendimento		is null		and :new.nr_atendimento		is not null)
		or (:old.nr_dialise_atend_pac	is null		and :new.nr_dialise_atend_pac	is not null)
		or (:old.ie_tipo_dialise	is null		and :new.ie_tipo_dialise	is not null)
		or (:old.ie_paciente_agudo	is null		and :new.ie_paciente_agudo	is not null)
		or (:old.nr_sequencia		is not null	and :new.nr_sequencia		is null)
		or (:old.dt_atualizacao_nrec	is not null	and :new.dt_atualizacao_nrec	is null)
		or (:old.nm_usuario_nrec	is not null	and :new.nm_usuario_nrec	is null)
		or (:old.cd_pessoa_fisica	is not null	and :new.cd_pessoa_fisica	is null)
		or (:old.dt_dialise		is not null	and :new.dt_dialise		is null)
		or (:old.nr_seq_unid_dialise	is not null	and :new.nr_seq_unid_dialise	is null)
		or (:old.nr_atendimento		is not null	and :new.nr_atendimento		is null)
		or (:old.nr_dialise_atend_pac	is not null	and :new.nr_dialise_atend_pac	is null)
		or (:old.ie_tipo_dialise	is not null	and :new.ie_tipo_dialise	is null)
		or (:old.ie_paciente_agudo	is not null	and :new.ie_paciente_agudo	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'GM'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia		<> :new.nr_sequencia
		or :old.dt_atualizacao_nrec	<> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec		<> :new.nm_usuario_nrec
		or :old.cd_pessoa_fisica	<> :new.cd_pessoa_fisica
		or :old.dt_dialise		<> :new.dt_dialise
		or :old.nr_seq_unid_dialise	<> :new.nr_seq_unid_dialise
		or :old.nr_atendimento		<> :new.nr_atendimento
		or :old.nr_dialise_atend_pac	<> :new.nr_dialise_atend_pac
		or :old.ie_tipo_dialise		<> :new.ie_tipo_dialise
		or :old.ie_paciente_agudo	<> :new.ie_paciente_agudo
		or (:old.nr_sequencia		is null		and :new.nr_sequencia		is not null)
		or (:old.dt_atualizacao_nrec	is null		and :new.dt_atualizacao_nrec	is not null)
		or (:old.nm_usuario_nrec	is null		and :new.nm_usuario_nrec	is not null)
		or (:old.cd_pessoa_fisica	is null		and :new.cd_pessoa_fisica	is not null)
		or (:old.dt_dialise		is null		and :new.dt_dialise		is not null)
		or (:old.nr_seq_unid_dialise	is null		and :new.nr_seq_unid_dialise	is not null)
		or (:old.nr_atendimento		is null		and :new.nr_atendimento		is not null)
		or (:old.nr_dialise_atend_pac	is null		and :new.nr_dialise_atend_pac	is not null)
		or (:old.ie_tipo_dialise	is null		and :new.ie_tipo_dialise	is not null)
		or (:old.ie_paciente_agudo	is null		and :new.ie_paciente_agudo	is not null)
		or (:old.nr_sequencia		is not null	and :new.nr_sequencia		is null)
		or (:old.dt_atualizacao_nrec	is not null	and :new.dt_atualizacao_nrec	is null)
		or (:old.nm_usuario_nrec	is not null	and :new.nm_usuario_nrec	is null)
		or (:old.cd_pessoa_fisica	is not null	and :new.cd_pessoa_fisica	is null)
		or (:old.dt_dialise		is not null	and :new.dt_dialise		is null)
		or (:old.nr_seq_unid_dialise	is not null	and :new.nr_seq_unid_dialise	is null)
		or (:old.nr_atendimento		is not null	and :new.nr_atendimento		is null)
		or (:old.nr_dialise_atend_pac	is not null	and :new.nr_dialise_atend_pac	is null)
		or (:old.ie_tipo_dialise	is not null	and :new.ie_tipo_dialise	is null)
		or (:old.ie_paciente_agudo	is not null	and :new.ie_paciente_agudo	is null)) then	
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/