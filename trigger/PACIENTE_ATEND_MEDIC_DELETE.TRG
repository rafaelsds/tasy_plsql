create or replace 
trigger paciente_atend_medic_delete 
before delete on paciente_atend_medic 
for each row
declare

ie_atualizar_autor_conv_w		varchar2(15) := 'N';
nr_seq_autor_w			number(10,0);
nr_ciclo_w			number(3,0);
ds_dia_ciclo_w			varchar2(5);
nr_seq_pend_Agenda_w		number(10);

qt_dose_total_w			paciente_atend_medic.qt_dose_prescricao%type;
qt_solicitada_w			material_autorizado.qt_solicitada%type;
qt_itens_autor_w		integer;
qt_itens_tratamento_w		integer;

begin 
obter_param_usuario(281,1001,obter_perfil_ativo,:old.nm_usuario,0,ie_atualizar_autor_conv_w);

if	(ie_atualizar_autor_conv_w = 'S') then
	begin

	select	max(nr_ciclo),
		max(ds_dia_ciclo)
	into	nr_ciclo_w,
		ds_dia_ciclo_w
	from	paciente_atendimento
	where	nr_seq_atendimento = :old.nr_seq_atendimento;


	select	max(a.nr_sequencia)
	into	nr_seq_autor_w
	from	autorizacao_convenio a,
		estagio_autorizacao b
	where	a.nr_ciclo			= nr_ciclo_w
	and	((a.ds_dia_ciclo		= ds_dia_ciclo_w) or 
		(nvl(ds_dia_ciclo, 'X')	= 'X'))
	and	a.nr_seq_paciente_setor	= :old.nr_seq_paciente
	and	a.nr_seq_estagio		= b.nr_sequencia
	and	b.ie_interno 		= '1';


	if	(nvl(nr_seq_autor_w,0) <> 0) then
		begin

			select	sum(qt_dose_prescricao),
			count(1)
		into	qt_dose_total_w,
			qt_itens_tratamento_w
		from	paciente_atend_medic
		where	nr_seq_atendimento = :old.nr_seq_atendimento
		and	cd_material = :old.cd_material;
		
		select 	sum(qt_solicitada),
			count(1)
		into	qt_solicitada_w,
			qt_itens_autor_w
		from	material_autorizado
		where	nr_sequencia_autor 	= nr_seq_autor_w
		and	cd_material		= :old.cd_material;

		if	((qt_dose_total_w = qt_solicitada_w) and (qt_itens_autor_w = 1)) then
			
		delete	from material_autorizado
		where	nr_sequencia_autor 	= nr_seq_autor_w
		and	cd_material		= :old.cd_material;

		elsif 	((qt_solicitada_w > nvl(:old.qt_dose_prescricao,0)) and (qt_itens_tratamento_w > 1) and (qt_itens_autor_w = 1)) then
			
			update 	material_autorizado set
				qt_solicitada 		= qt_solicitada - nvl(:old.qt_dose_prescricao,0),
				dt_atualizacao 		= sysdate,
				nm_usuario		= :new.nm_usuario
			where	nr_sequencia_autor 	= nr_seq_autor_w
			and	cd_material		= :old.cd_material;
			
		end if;	
		end;
	end if;
	
	exception
	when others then
		null;
	end;
end if;

end;
/
