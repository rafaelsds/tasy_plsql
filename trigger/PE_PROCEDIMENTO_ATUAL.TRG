CREATE OR REPLACE TRIGGER PE_Procedimento_Atual
BEFORE INSERT OR UPDATE ON PE_Procedimento
FOR EACH ROW

declare

begin
if	(:new.ds_procedimento_pesquisa is null) or
	(:new.ds_procedimento <> :old.ds_procedimento ) then
	:new.ds_procedimento_pesquisa	:= upper(elimina_acentuacao(:new.ds_procedimento));
end if;


END;
/
