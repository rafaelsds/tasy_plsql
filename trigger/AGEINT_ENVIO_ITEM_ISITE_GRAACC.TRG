create or replace trigger ageint_envio_item_isite_graacc
after update on AGENDA_PACIENTE
for each row

declare
ds_param_integ_hl7_w	varchar2(4000 CHAR);
ds_sep_bv_w				varchar2(100 CHAR);
nr_prescricao_w 		prescr_procedimento.nr_prescricao%TYPE;
nr_seq_agenda_W 		number(30);
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%TYPE;
nr_atendimento_w 		prescr_medica.nr_atendimento%TYPE;
nr_seq_prescr_w 		number (30);
cd_pessoa_fisica_w 		atendimento_paciente.cd_pessoa_fisica%TYPE;
nr_seq_interno_w 		number(20);
dt_agendamento_w 		varchar2(400 CHAR);

Cursor C01 is
	select	a.nr_sequencia	
		from	prescr_procedimento a,
		proc_interno_integracao b
	where	a.nr_prescricao	= nr_prescricao_w
	and	b.nr_seq_proc_interno = a.nr_seq_proc_interno 
	and	nvl(b.cd_estabelecimento,nvl(1,0)) = nvl(1,0)
	and	a.nr_seq_exame is null
	and	b.nr_seq_sistema_integ in (11,66,91,103,108) 		--Philips-DCX, GE-Centricity-PACSIW, Synapse-Fuji, Philips-IBE, GE-Centricity-RISi
	union 
	select	nr_sequencia
	from	prescr_procedimento a
	where	a.nr_prescricao	= nr_prescricao_w		--Philips-Isite
	and	Obter_Se_Integr_Proc_Interno(a.nr_seq_proc_interno, 2, null, a.ie_lado, 1) = 'S'		 
	and	a.nr_seq_exame is null;

begin

if (:new.dt_confirmacao is not null) and (:new.dt_cancelamento is null) then

select max(nr_prescricao)
into nr_prescricao_W
from agenda_integrada_item
where nr_seq_agenda_exame = :new.nr_sequencia;

select max(a.nr_atendimento)
into nr_atendimento_w
from prescr_medica a 
where a.nr_prescricao = nr_prescricao_w;

select max(ie_tipo_Atendimento)
into ie_tipo_atendimento_w
from atendimento_paciente
where nr_atendimento = nr_atendimento_w;

select	max(obter_atepacu_paciente(nr_atendimento_w,'A'))
into	nr_seq_interno_w
from	dual;

select max(a.cd_pessoa_Fisica)
into cd_pessoa_fisica_w
from atendimento_paciente a, 
	 prescr_medica b
where a.nr_atendimento = b.nr_Atendimento
and b.nr_prescricao = nr_prescricao_w;

select to_char(:new.hr_inicio,'dd/mm/yyyy hh24:mi:ss')
into dt_agendamento_w
from dual;

--if (ie_tipo_atendimento_w in (1,3)) and (dt_agendamento_w is not null) then --Removido o tipo de atendimento, todos devem gerar HL7 
if (dt_agendamento_w is not null) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_prescr_w;
	exit when C01%notfound;
		begin

   update prescr_procedimento
  		set DT_PREV_EXECUCAO = to_Date(dt_Agendamento_w,'dd/mm/yyyy hh24:mi:ss')
		where nr_prescricao = nr_prescricao_w
		and cd_procedimento = :new.cd_procedimento; 
		ds_sep_bv_w := obter_separador_bv;

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w; -- Bruno orientou

       
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||
					'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||
					'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
					'nr_prescricao='    || nr_prescricao_w    || ds_sep_bv_w ||
					'nr_seq_presc='	    || nr_seq_prescr_w	     || ds_sep_bv_w ||	
					'order_control='    || 'NW'    		     || ds_sep_bv_w ||	
					'order_status='     || 'SC'    		           || ds_sep_bv_w ||
				    'nr_seq_anamese='   || null   || ds_sep_bv_w;

	   
     gravar_agend_integracao(23, ds_param_integ_hl7_w);
     
     gravar_agend_integracao(530, ds_param_integ_hl7_w);
    
		end;
	end loop;
	close C01;

end if;	

end if;

exception
when others then
gravar_log_tasy(1249,' sql erm: '||sqlerrm,'tasy');

end;
/
