create or replace trigger log_retorno_sms_after_ins
after insert on log_retorno_sms
for each row

declare

nr_seq_agenda_w			number(10);
nr_seq_agenda_pac_w		number(10);
cd_pessoa_fisica_w		varchar2(20);
nr_seq_forma_confirmacao_w		number(10);
cd_estabelecimento_w		number(10);
nr_celular_w			varchar2(50);
nr_seq_ageint_w			number(10);

nr_ddd_celular_w			varchar2(50);
nr_telefone_celular_w		varchar2(50);
ie_retorno_canc_w			varchar2(1);
qt_hor_cancel_w			number(10,0);

cd_agenda_w			agenda_consulta.cd_agenda%type;
dt_agenda_w			date;
ie_classif_agenda_w agenda_consulta.ie_classif_agenda%type;
ie_tipo_retorno_w		varchar2(1);
qt_dias_confirmacao_w	ageint_texto_confirm_sms.qt_dias_confirmacao%type;
qt_dias_confirmacao_ww	ageint_texto_confirm_sms.qt_dias_confirmacao%type;
ds_motivo_cancelamento_w ageint_texto_confirm_sms.ds_motivo_cancelamento%type;
ie_altera_status_confirmada_w	parametro_agenda.ie_altera_status_confirmada%TYPE;
ie_confirm_ret_sms_w				parametro_agenda.ie_confirm_ret_sms%type;
ie_status_confirmado_w			VARCHAR2(2) := 'CN';

cursor c01 is
	select	nr_sequencia,
			cd_agenda,
			dt_agenda,
			ie_classif_agenda
	from	agenda_consulta
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and		ie_status_agenda = 'N'
	and		dt_confirmacao is null
	and		dt_agenda >= sysdate
	and 	((qt_dias_confirmacao_w = 0) or (trunc(dt_agenda) <= trunc(sysdate + qt_dias_confirmacao_w)))
	order 	by nr_sequencia;

Cursor C02 is
	select	nr_sequencia,
			cd_agenda,
			hr_inicio
	from	agenda_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and		ie_status_agenda = 'N'
	and		dt_confirmacao is null
	and		hr_inicio >= sysdate
	and 	((qt_dias_confirmacao_w = 0) or (trunc(hr_inicio) <= trunc(sysdate + qt_dias_confirmacao_w)))
	order 	by nr_sequencia;

BEGIN

  wheb_usuario_pck.set_ie_commit('N');

	select 	nvl(max(ie_tipo_retorno),'O')
	into	ie_tipo_retorno_w
	from	ageint_texto_retorno_sms
	where	trim(upper(ds_texto)) = trim(upper(:new.ds_resposta));
  
  
	select 	nvl(max(qt_dias_confirmacao),0) , max(ds_motivo_cancelamento)
	into	qt_dias_confirmacao_w,
  ds_motivo_cancelamento_w
	from	ageint_texto_confirm_sms
	where	ie_tipo_retorno    = ie_tipo_retorno_w
	and		cd_estabelecimento = nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

	if (nvl(qt_dias_confirmacao_w,0) = 0) then
		qt_dias_confirmacao_ww := 30;
	else
		qt_dias_confirmacao_ww := qt_dias_confirmacao_w+1;
	end if;

if	(instr(upper(:new.ds_resposta), 'SIM') > 0) or
	(obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'S'
	or obter_tipo_resposta_sms(:new.ds_resposta) = 'N') then

	nr_celular_w := :new.nr_celular;

	if	(nvl(pkg_i18n.get_user_locale, 'pt_BR')	 <> 'en_AU') then
		if	(substr(nr_celular_w,1,2) <> '55') then
			nr_celular_w :=  '55'||:new.nr_celular;
		end if;
	end if;

	nr_ddd_celular_w		:= trim(substr(nr_celular_w,3,2));
	nr_telefone_celular_w	:= trim(substr(nr_celular_w,5,50));

	begin
		select	cd_pessoa_fisica,
				cd_estabelecimento
		into	cd_pessoa_fisica_w,
				cd_estabelecimento_w
		from	pessoa_fisica a
		where	nr_telefone_celular	= nr_celular_w
		and 	dt_obito is null
		and	(exists (select 1
				from agenda_consulta x
				where a.cd_pessoa_fisica = x.cd_pessoa_fisica
				and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
				and rownum = 1)
		or 	exists (select 1
				from agenda_paciente x
				where a.cd_pessoa_fisica = x.cd_pessoa_fisica
				and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
				and rownum = 1))
		and	rownum = 1;
 
	exception
	when no_data_found then

		begin
			select	cd_pessoa_fisica,
					cd_estabelecimento
			into	cd_pessoa_fisica_w,
					cd_estabelecimento_w
			from	pessoa_fisica a
			where	nr_ddd_celular		= nr_ddd_celular_w
			and		nr_telefone_celular = nr_telefone_celular_w
			and 	dt_obito is null
			and	(exists (select 1
					from agenda_consulta x
					where a.cd_pessoa_fisica = x.cd_pessoa_fisica
					and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
					and rownum = 1)
			or 	exists (select 1
					from agenda_paciente x
					where a.cd_pessoa_fisica = x.cd_pessoa_fisica
					and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
					and rownum = 1))
			and		rownum = 1;

		exception
		when no_data_found then

			begin
				select	cd_pessoa_fisica,
						cd_estabelecimento
				into	cd_pessoa_fisica_w,
						cd_estabelecimento_w
				from	pessoa_fisica a
				where	somente_numero(nr_telefone_celular)	= somente_numero(nr_ddd_celular_w||nr_telefone_celular_w)
				and 	dt_obito is null
				and	(exists (select 1
						from agenda_consulta x
						where a.cd_pessoa_fisica = x.cd_pessoa_fisica
						and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
						and rownum = 1)
				or 	exists (select 1
						from agenda_paciente x
						where a.cd_pessoa_fisica = x.cd_pessoa_fisica
						and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
						and rownum = 1))
				and		rownum = 1;

				exception
				when no_data_found then
					begin
						select	max(cd_pessoa_fisica),
								max(cd_estabelecimento)
						into	cd_pessoa_fisica_w,
								cd_estabelecimento_w
						from	pessoa_fisica a
						where	somente_numero(nr_telefone_celular)	= somente_numero(nr_celular_w)
						and 	dt_obito is null
						and	(exists (select 1
								from agenda_consulta x
								where a.cd_pessoa_fisica = x.cd_pessoa_fisica
								and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
								and rownum = 1)
						or 	exists (select 1
								from agenda_paciente x
								where a.cd_pessoa_fisica = x.cd_pessoa_fisica
								and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
								and rownum = 1));

						exception
						when no_data_found then
						begin
							select	cd_pessoa_fisica,
									cd_estabelecimento
							into	cd_pessoa_fisica_w,
									cd_estabelecimento_w
							from	pessoa_fisica a
							where	nr_ddd_celular		= nr_ddd_celular_w
							and		nr_telefone_celular = substr(trim(nr_telefone_celular_w),2,50)  -- older number format with 8 digits
							and 	dt_obito is null
							and	(exists (select 1
									from agenda_consulta x
									where a.cd_pessoa_fisica = x.cd_pessoa_fisica
									and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
									and rownum = 1)
							or 	exists (select 1
									from agenda_paciente x
									where a.cd_pessoa_fisica = x.cd_pessoa_fisica
									and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
									and rownum = 1))
							and		rownum = 1;
						end;
					end;
			end;
		end;
	end;

	if	(cd_pessoa_fisica_w is not null) then

		select	nvl(max(a.nr_sequencia),0)
		into	nr_seq_ageint_w
		from 	agenda_integrada a,
				agenda_integrada_item b,
				agenda_paciente c
		where	a.cd_pessoa_fisica 	= cd_pessoa_fisica_w
		and		c.nr_sequencia 	   	= b.nr_seq_agenda_exame
		and		a.nr_sequencia 	   	= b.nr_seq_agenda_int
		and		c.hr_inicio	  		>= trunc(sysdate)
		and		c.ie_status_agenda 	= 'N'
		and		c.dt_confirmacao   	is null;

		if (nvl(nr_seq_ageint_w,0) = 0) then

			select	nvl(max(a.nr_sequencia),0)
			into	nr_seq_ageint_w
			from 	agenda_integrada a,
					agenda_integrada_item b,
					agenda_consulta c
			where	a.cd_pessoa_fisica 	= cd_pessoa_fisica_w
			and		c.nr_sequencia 	   	= b.nr_seq_agenda_cons
			and		a.nr_sequencia 	   	= b.nr_seq_agenda_int
			and		c.dt_agenda	  		>= trunc(sysdate)
			and		c.ie_status_agenda 	= 'N'
			and		c.dt_confirmacao   	is null;
		end if;

		/*if (nvl(nr_seq_ageint_w,0) > 0) then
			:new.nr_seq_ageint := nr_seq_ageint_w;
		end if;*/

		SELECT MAX(nr_seq_forma_confirmacao_sms),
		       MAX(NVL(ie_altera_status_confirmada,'N')),
		       MAX(NVL(IE_CONFIRM_RET_SMS,'N'))
		  INTO nr_seq_forma_confirmacao_w,
		       ie_altera_status_confirmada_w,
		       ie_confirm_ret_sms_w
		  FROM parametro_agenda
		 WHERE cd_estabelecimento = nvl(cd_estabelecimento_w, cd_estabelecimento);

		open c01;
		loop
		fetch c01 into	
			nr_seq_agenda_w,
			cd_agenda_w,
			dt_agenda_w,
         ie_classif_agenda_w;
		exit when c01%notfound;
			begin
    
        IF ((IE_CONFIRM_RET_SMS_w = 'N')  OR
            (IE_CONFIRM_RET_SMS_w = 'S' AND
             valida_sms_agenda_regra(ie_classif_agenda_p   => ie_classif_agenda_w,
                                     cd_agenda_p           => cd_agenda_w) = 'S')) THEN
			if (instr(upper(:new.ds_resposta), 'SIM') > 0) or
				(obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'S') then
				update	agenda_consulta
				set		dt_confirmacao			= sysdate,
						nm_usuario_confirm		= 'TASY',
						nm_usuario				= 'TASY',
						nr_seq_forma_confirmacao= nr_seq_forma_confirmacao_w,
						ds_confirmacao			= wheb_mensagem_pck.get_texto(795424),
						ie_status_agenda         = decode(ie_altera_status_confirmada_w,
						                            'S',
						                            ie_status_confirmado_w,
						                            ie_status_agenda)
				where	nr_sequencia			= nr_seq_agenda_w;
      elsif((instr(upper(:new.ds_resposta), 'NAO') > 0) or (obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'N')) then
         if(length(ds_motivo_cancelamento_w) > 0 ) then
           wheb_usuario_pck.set_ie_commit('N');
           begin
             alterar_status_agecons(cd_agenda_w,
                                    nr_seq_agenda_w,
                                    'C',
                                    null,
                                    ds_motivo_cancelamento_w,
                                    'N',
                                    'TASY',
                                    '');
           exception
             when others then
               null;
           end;
           
           wheb_usuario_pck.set_ie_commit('S');
         end if;
        insert into AGENDA_RETORNO_SMS
          (NR_SEQUENCIA,
           DT_ATUALIZACAO_NREC,
           DT_ATUALIZACAO,
           NM_USUARIO_NREC,
           NM_USUARIO,
           IE_STATUS,
           NR_SEQ_AGEPAC)
        values
          (AGENDA_RETORNO_SMS_seq.nextval,
           sysdate,
           sysdate,
           'TASY',
           'TASY',
           'N',
           nr_seq_agenda_pac_w);
			else
            
				insert into AGENDA_RETORNO_SMS (
					NR_SEQUENCIA,
					DT_ATUALIZACAO_NREC,
					DT_ATUALIZACAO,
					NM_USUARIO_NREC,
					NM_USUARIO,
					IE_STATUS,
					NR_SEQ_AGECONS
				) values (
					agenda_retorno_sms_seq.nextval,
					sysdate,
					sysdate,
					'TASY',
					'TASY',
					'N',
					nr_seq_agenda_w
				);
			end if;

        END IF;
			end;
		end loop;
		close c01;


		open C02;
		loop
		fetch C02 into
			nr_seq_agenda_pac_w,
			cd_agenda_w,
			dt_agenda_w;
		exit when C02%notfound;

      BEGIN
        IF ((IE_CONFIRM_RET_SMS_w = 'N')  OR
            (IE_CONFIRM_RET_SMS_w = 'S' AND
             valida_sms_agenda_regra(ie_classif_agenda_p   => ie_classif_agenda_w,
                                     cd_agenda_p           => cd_agenda_w) = 'S')) THEN

			if (instr(upper(:new.ds_resposta), 'SIM') > 0) or
        (obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'S') then
        update  agenda_paciente
        set    dt_confirmacao        = sysdate,
            nm_usuario_confirm      = 'TASY',
            nm_usuario          = 'TASY',
            nr_seq_forma_confirmacao  = nr_seq_forma_confirmacao_w,
            ds_confirmacao        = wheb_mensagem_pck.get_texto(795424),
            ie_status_agenda         = decode(ie_altera_status_confirmada_w,
                                        'S',
                                        ie_status_confirmado_w,
                                        ie_status_agenda)

        where  nr_sequencia        = nr_seq_agenda_pac_w;
      elsif((instr(upper(:new.ds_resposta), 'NAO') > 0) or (obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'N')) then
         if(length(ds_motivo_cancelamento_w) > 0 ) then
           wheb_usuario_pck.set_ie_commit('N');
           begin
             alterar_status_agenda(cd_agenda_w,
                                   nr_seq_agenda_pac_w,
                                   'C',
                                   null,
                                   ds_motivo_cancelamento_w,
                                   'N',
                                   'TASY',
                                   '');
           exception
             when others then
               null;
           end;
           wheb_usuario_pck.set_ie_commit('S');
         end if;
        insert into AGENDA_RETORNO_SMS
          (NR_SEQUENCIA,
           DT_ATUALIZACAO_NREC,
           DT_ATUALIZACAO,
           NM_USUARIO_NREC,
           NM_USUARIO,
           IE_STATUS,
           NR_SEQ_AGEPAC)
        values
          (AGENDA_RETORNO_SMS_seq.nextval,
           sysdate,
           sysdate,
           'TASY',
           'TASY',
           'N',
           nr_seq_agenda_pac_w);
      else
        insert into AGENDA_RETORNO_SMS (
          NR_SEQUENCIA,
          DT_ATUALIZACAO_NREC,
          DT_ATUALIZACAO,
          NM_USUARIO_NREC,
          NM_USUARIO,
          IE_STATUS,
          NR_SEQ_AGEPAC
        ) values (
          AGENDA_RETORNO_SMS_seq.nextval,
          sysdate,
          sysdate,
          'TASY',
          'TASY',
          'N',
          nr_seq_agenda_pac_w
        );
      end if;

        END IF;
      end;
    end loop;
    close C02;

  end if;
end if;
wheb_usuario_pck.set_ie_commit('S');
end;
/
