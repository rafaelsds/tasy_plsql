create or replace trigger PACIENTE_ATENDIMENTO_ATUAL
before insert or update on PACIENTE_ATENDIMENTO
for each row

declare
qt_altura_w				Number(5,0);
qt_redutor_sc_w			Number(15,4);
ie_formula_sc_w			varchar2(10);
qt_superf_corporal_w	number(15,5);
cd_pessoa_fisica_w   	varchar2(20);
ie_registro_liberado_w 	varchar2(1);
begin
if	(:new.dt_cancelamento is null) then
	Abortar_Se_Protoc_Inativo(:new.nr_seq_paciente);
end if;

if	(nvl(:old.DT_PREVISTA,sysdate+10) <> :new.DT_PREVISTA) and
	(:new.DT_PREVISTA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_PREVISTA, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(NVL(:new.qt_peso,0) <> NVL(:OLD.qt_peso,0)) OR
    (NVL(:new.qt_altura,0) <> NVL(:old.qt_altura,0))then
    
	select	nvl(qt_altura,0),
		nvl(qt_redutor_sc,0),
		ie_formula_sup_corporea,
      cd_pessoa_fisica
	into qt_altura_w,
		qt_redutor_sc_w,
		ie_formula_sc_w,
      cd_pessoa_fisica_w
	from	paciente_setor
	where	nr_seq_paciente = :new.nr_seq_paciente;
	
	if	(nvl(:new.qt_altura,0) > 0) then	
		qt_altura_w := :new.qt_altura;	
	end if;
	
	qt_superf_corporal_w := round(obter_superficie_corp_red_ped(:new.qt_peso, qt_altura_w,qt_redutor_sc_w, cd_pessoa_fisica_w, obter_usuario_ativo,ie_formula_sc_w),obter_numero_casas_sc);
	if	(nvl(qt_superf_corporal_w,0) > 0) then
		:new.qt_superf_corporal	:= qt_superf_corporal_w;
	end if;
    
        
end if;

if	(:new.dt_cancelamento is not null) and
	(:old.dt_cancelamento is null) and
	(:new.NM_USUARIO_CANCEL is null) then
	:new.NM_USUARIO_CANCEL	:= obter_usuario_ativo;
end if;

if	(:old.ie_exige_liberacao = 'N') and
	(:new.ie_exige_liberacao = 'S') and
	(:new.nr_prescricao is not null) then
	select 	nvl(max('S'),'N') 
	into	ie_registro_liberado_w
	from 	prescr_material 
	where 	nr_prescricao = :new.nr_prescricao 
	and 	dt_baixa  is not null;
	
	if (ie_registro_liberado_w = 'S') then
		Wheb_mensagem_pck.exibir_mensagem_abort(326065);
	end if;	
end if;

end;
/
