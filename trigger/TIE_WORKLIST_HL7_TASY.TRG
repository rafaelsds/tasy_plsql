create or replace TRIGGER TIE_WORKLIST_HL7_TASY
  BEFORE UPDATE ON PRESCR_MEDICA FOR EACH ROW
DECLARE
    qtd_prc_w                   INTEGER;
    nm_social_w                 pessoa_fisica.nm_social%TYPE;
    dt_nascimento_w             pessoa_fisica.dt_nascimento%TYPE;
    ie_sexo_w                   pessoa_fisica.ie_sexo%TYPE;
    ds_orgao_emissor_ci_w       pessoa_fisica.ds_orgao_emissor_ci%TYPE;
    nr_identidade_w             pessoa_fisica.nr_identidade%TYPE;
    nr_cpf_w                    pessoa_fisica.nr_cpf%TYPE;
    nr_cert_nasc_w              pessoa_fisica.nr_cert_nasc%TYPE;
    ie_estado_civil_w           pessoa_fisica.ie_estado_civil%TYPE;
    nr_cep_cidade_nasc_w        pessoa_fisica.nr_cep_cidade_nasc%TYPE;
    cd_nacionalidade_w          pessoa_fisica.cd_nacionalidade%TYPE;
    nr_telefone_celular_w       pessoa_fisica.nr_telefone_celular%TYPE;
    nr_prontuario_w             pessoa_fisica.nr_prontuario%TYPE;
    cd_cep_w                    compl_pessoa_fisica.cd_cep%TYPE;
    ds_endereco_w               compl_pessoa_fisica.ds_endereco%TYPE;
    nr_endereco_w               compl_pessoa_fisica.nr_endereco%TYPE;
    ds_complemento_w            compl_pessoa_fisica.ds_complemento%TYPE;
    ds_bairro_w                 compl_pessoa_fisica.ds_bairro%TYPE;
    cd_tipo_logradouro_w        compl_pessoa_fisica.cd_tipo_logradouro%TYPE;
    cd_municipio_ibge_w         compl_pessoa_fisica.cd_municipio_ibge%TYPE;
    ds_municipio_w              compl_pessoa_fisica.ds_municipio%TYPE;
    sg_estado_w                 compl_pessoa_fisica.sg_estado%TYPE;
    ds_email_w                  compl_pessoa_fisica.ds_email%TYPE;
    nr_telefone_w               compl_pessoa_fisica.nr_telefone%TYPE;
    nm_contato_um_w             compl_pessoa_fisica.nm_contato%TYPE;
    nm_contato_dois_w           compl_pessoa_fisica.nm_contato%TYPE;
    dt_entrada_w                atendimento_paciente.dt_entrada%TYPE;
    nr_atendimento_w            atendimento_paciente.nr_atendimento%TYPE;
    cd_procedencia_w            atendimento_paciente.cd_procedencia%TYPE;
    cd_medico_resp_w            ATENDIMENTO_PACIENTE.cd_medico_resp%TYPE;
    nr_crm_w                    medico.nr_crm%TYPE;
    uf_crm_w                    medico.uf_crm%TYPE;
    cd_especialidade_ext_w      especialidade_medica.cd_especialidade_externo%TYPE;
    cd_estabelecimento_w        atendimento_paciente.cd_estabelecimento%TYPE;
    cd_convenio_w               atend_categoria_convenio.cd_convenio%TYPE;
    cd_categoria_w              atend_categoria_convenio.cd_categoria%TYPE;
    cd_tipo_acomodacao_w        atend_categoria_convenio.cd_tipo_acomodacao%TYPE;
    cd_usuario_convenio_w       atend_categoria_convenio.cd_usuario_convenio%TYPE;   
    dt_validade_carteira_w      atend_categoria_convenio.dt_validade_carteira%TYPE;
    cd_plano_convenio_w         atend_categoria_convenio.cd_plano_convenio%TYPE;
    ie_responsavel_w            atendimento_paciente.ie_responsavel%TYPE;
    cd_pessoa_responsavel_w     atendimento_paciente.cd_pessoa_responsavel%TYPE;
    nr_seq_proc_interno_w       prescr_procedimento.nr_seq_proc_interno%TYPE;
    ie_lado_w                   prescr_procedimento.ie_lado%TYPE;
    qt_procedimento_w           prescr_procedimento.qt_procedimento%TYPE;
    nr_acesso_dicom_w           prescr_procedimento.nr_acesso_dicom%TYPE;
    nr_seq_prescricao_w         prescr_procedimento.nr_sequencia%TYPE;
    ds_obs_proced_w             prescr_procedimento_obs.ds_observacao%TYPE;

   CURSOR c01 IS  
        SELECT  a.nm_social,
                a.dt_nascimento,
                a.ie_sexo,
                a.ds_orgao_emissor_ci,
                a.nr_identidade,
                a.nr_cpf,
                a.nr_cert_nasc,
                a.ie_estado_civil,
                a.nr_cep_cidade_nasc,
                a.cd_nacionalidade,
                a.nr_telefone_celular,
                a.nr_prontuario,
                b.cd_cep,
                b.ds_endereco,
                b.nr_endereco,
                b.ds_complemento,
                b.ds_bairro,
                b.cd_tipo_logradouro,
                b.cd_municipio_ibge,
                b.ds_municipio,
                b.sg_estado,
                b.ds_email,
                b.nr_telefone,
                (SELECT nm_contato FROM compl_pessoa_fisica WHERE ie_tipo_complemento = 4 and cd_pessoa_fisica = :NEW.cd_pessoa_fisica) nm_contato_um_w,
                (SELECT nm_contato FROM compl_pessoa_fisica WHERE ie_tipo_complemento = 5 and cd_pessoa_fisica = :NEW.cd_pessoa_fisica) nm_contato_dois_w,
                c.dt_entrada,
                c.nr_atendimento,
                c.cd_procedencia,
                c.cd_medico_resp,
                (SELECT cd_interno FROM estabelecimento WHERE cd_estabelecimento = c.cd_estabelecimento) cd_estabelecimento,
                f.cd_integracao cd_convenio,
                g.cd_integracao cd_categoria,
                e.cd_tipo_acomodacao, 
                e.cd_usuario_convenio,
                e.dt_validade_carteira,
                h.cd_plano_referencia cd_plano_convenio,
                c.ie_responsavel,
                c.cd_pessoa_responsavel, 
                j.cd_integracao nr_seq_proc_interno,
                i.ie_lado,
                i.qt_procedimento,
                i.nr_acesso_dicom,
                i.nr_sequencia,
                d.nr_crm,
                d.uf_crm,
                (SELECT b.cd_especialidade_externo
                FROM medico_especialidade a,
                    especialidade_medica b
                WHERE cd_pessoa_fisica = c.cd_medico_resp
                    and a.cd_especialidade = b.cd_especialidade
                    and rownum = 1
                    and nr_seq_prioridade = (SELECT min(t.nr_seq_prioridade) 
                                            FROM  medico_especialidade t
                                            WHERE t.cd_pessoa_fisica = c.cd_medico_resp)) as cd_especialidade_externo,
                SUBSTR(Obter_desc_protocolo(i.cd_protocolo),1,100) ds_obs_proced
            FROM 
                pessoa_fisica a, 
                compl_pessoa_fisica b,
                atendimento_paciente c,
                medico d,
                atend_categoria_convenio e,
                convenio f,
                categoria_convenio g,
                convenio_plano h,
                prescr_procedimento i,
                proc_interno j
            WHERE a.cd_pessoa_fisica = b.cd_pessoa_fisica(+)
                AND b.ie_tipo_complemento(+)= 1
                AND a.cd_pessoa_fisica = :NEW.cd_pessoa_fisica
                AND a.cd_pessoa_fisica = c.cd_pessoa_fisica
                AND c.nr_atendimento = :NEW.nr_atendimento
                AND d.cd_pessoa_fisica = c.cd_medico_resp
                AND c.nr_atendimento = e.nr_atendimento
                AND e.cd_convenio = f.cd_convenio
                AND g.cd_convenio = f.cd_convenio
                AND g.cd_categoria = e.cd_categoria
                AND g.cd_convenio = h.cd_convenio
                AND e.cd_plano_convenio = h.cd_plano
                AND i.nr_seq_proc_interno = j.nr_sequencia
                AND i.nr_prescricao = :NEW.nr_prescricao
                AND i.dt_suspensao  IS NULL
                and	i.nr_seq_exame  IS NULL
                AND i.cd_motivo_baixa = 0
                and (:NEW.dt_liberacao_medico is not null or  :NEW.dt_liberacao is not null)
                AND i.dt_integracao is null
                AND obter_se_integr_proc_interno(i.nr_seq_proc_interno, 40 /*CMC x Proton*/, null, i.ie_lado, :NEW.cd_estabelecimento) = 'S';

	PROCEDURE PROCESS_EVENT_SEND_WORKLIST IS
		JSON_W                  PHILIPS_JSON := PHILIPS_JSON();
		JSON_PID_W              PHILIPS_JSON := PHILIPS_JSON();
        JSON_PV1_W              PHILIPS_JSON := PHILIPS_JSON();
        JSON_IN1_W              PHILIPS_JSON := PHILIPS_JSON();
        JSON_ORC_W              PHILIPS_JSON := PHILIPS_JSON();
        JSON_OBR_W              PHILIPS_JSON := PHILIPS_JSON();
		JSON_DATA_W              CLOB;      

		BEGIN
			JSON_W.PUT(PAIR_NAME => 'sendingApplication',                 PAIR_VALUE => 'TASY');
			JSON_W.PUT(PAIR_NAME => 'sendingFacility',                    PAIR_VALUE => 'PHILIPS');
			JSON_W.PUT(PAIR_NAME => 'receivingApplication',               PAIR_VALUE => 'TASY');
			JSON_W.PUT(PAIR_NAME => 'receivingFacility',                  PAIR_VALUE => 'PHILIPS');
			JSON_W.PUT(PAIR_NAME => 'processingID',                       PAIR_VALUE => 'P');
            JSON_W.PUT(PAIR_NAME => 'characterSet',                       PAIR_VALUE => '8859/1');

			JSON_PID_W.PUT(PAIR_NAME => 'GivenName',                      PAIR_VALUE => nm_social_w);
            JSON_PID_W.PUT(PAIR_NAME => 'Surname',                        PAIR_VALUE => obter_nome_pf(:NEW.cd_pessoa_fisica));
            JSON_PID_W.PUT(PAIR_NAME => 'dateOfBirth',                    PAIR_VALUE => TO_CHAR(dt_nascimento_w, 'MM/DD/YYYY HH24:MI:SS'));
            JSON_PID_W.PUT(PAIR_NAME => 'sex',                            PAIR_VALUE => ie_sexo_w);
            JSON_PID_W.PUT(PAIR_NAME => 'IssuingStateProvinceCountry',    PAIR_VALUE => ds_orgao_emissor_ci_w);
            JSON_PID_W.PUT(PAIR_NAME => 'licenseNumber',                  PAIR_VALUE => nr_identidade_w);
            JSON_PID_W.PUT(PAIR_NAME => 'SSNnumberPatient',               PAIR_VALUE => nr_cpf_w);
            JSON_PID_W.PUT(PAIR_NAME => 'MultipleBirthIndicator',         PAIR_VALUE => nr_cert_nasc_w);
            JSON_PID_W.PUT(PAIR_NAME => 'MaritalStatus',                  PAIR_VALUE => ie_estado_civil_w);
            JSON_PID_W.PUT(PAIR_NAME => 'BirthPlace',                     PAIR_VALUE => nr_cep_cidade_nasc_w);
            JSON_PID_W.PUT(PAIR_NAME => 'Nationality',                    PAIR_VALUE => cd_nacionalidade_w);
            JSON_PID_W.PUT(PAIR_NAME => 'telephoneNumberHome',            PAIR_VALUE => nr_telefone_celular_w);
            JSON_PID_W.PUT(PAIR_NAME => 'PatientAccountNumber',           PAIR_VALUE => nr_prontuario_w);
            JSON_PID_W.PUT(PAIR_NAME => 'ZipOrPostalCode',                PAIR_VALUE => cd_cep_w );
            JSON_PID_W.PUT(PAIR_NAME => 'streetAddress',                  PAIR_VALUE => ds_endereco_w);
            JSON_PID_W.PUT(PAIR_NAME => 'DwellingNumber',                 PAIR_VALUE => nr_endereco_w);
            JSON_PID_W.PUT(PAIR_NAME => 'AddressType',                    PAIR_VALUE => ds_complemento_w );
            JSON_PID_W.PUT(PAIR_NAME => 'otherDesignation',               PAIR_VALUE => ds_bairro_w);
            JSON_PID_W.PUT(PAIR_NAME => 'CountyParishCode',               PAIR_VALUE => cd_tipo_logradouro_w);
            JSON_PID_W.PUT(PAIR_NAME => 'OtherGeographicDesignation',     PAIR_VALUE => cd_municipio_ibge_w);
            JSON_PID_W.PUT(PAIR_NAME => 'city',                           PAIR_VALUE => ds_municipio_w);
            JSON_PID_W.PUT(PAIR_NAME => 'stateOrProvince',                PAIR_VALUE => sg_estado_w);
            JSON_PID_W.PUT(PAIR_NAME => 'emailAddressPatient',            PAIR_VALUE => ds_email_w);
            JSON_PID_W.PUT(PAIR_NAME => 'telephoneNumberWork',            PAIR_VALUE => nr_telefone_w);
            JSON_PID_W.PUT(PAIR_NAME => 'AssigningAuthority',             PAIR_VALUE => nm_contato_um_w);
            JSON_PID_W.PUT(PAIR_NAME => 'AssigningFacility',              PAIR_VALUE => nm_contato_dois_w);
            JSON_PID_W.PUT(PAIR_NAME => 'PatientID',                      PAIR_VALUE => :NEW.cd_pessoa_fisica);

			JSON_W.PUT(PAIR_NAME => 'PID', PAIR_VALUE => JSON_PID_W.TO_JSON_VALUE());

            JSON_PV1_W.PUT(PAIR_NAME => 'accountStatusDtAtend',           PAIR_VALUE => TO_CHAR(dt_entrada_w, 'MM/DD/YYYY HH24:MI:SS'));
            JSON_PV1_W.PUT(PAIR_NAME => 'VisitNumberID',                  PAIR_VALUE => nr_atendimento_w);
            JSON_PV1_W.PUT(PAIR_NAME => 'PersonLocationType',             PAIR_VALUE => cd_procedencia_w);
            JSON_PV1_W.PUT(PAIR_NAME => 'ReferringDoctorID',              PAIR_VALUE => cd_medico_resp_w);
            JSON_PV1_W.PUT(PAIR_NAME => 'SurnameDoctor',                  PAIR_VALUE => obter_nome_pf(cd_medico_resp_w));
            JSON_PV1_W.PUT(PAIR_NAME => 'IdentifierCheckDigit',           PAIR_VALUE => nr_crm_w);
            JSON_PV1_W.PUT(PAIR_NAME => 'CheckDigitScheme',               PAIR_VALUE => uf_crm_w);
            JSON_PV1_W.PUT(PAIR_NAME => 'DegreeEgMD',                     PAIR_VALUE => obter_dados_pf(cd_medico_resp_w, 'SE'));
            JSON_PV1_W.PUT(PAIR_NAME => 'SourceTable',                    PAIR_VALUE => cd_especialidade_ext_w);
            JSON_PV1_W.PUT(PAIR_NAME => 'PointOfCare',                    PAIR_VALUE => cd_estabelecimento_w);
            JSON_PV1_W.PUT(PAIR_NAME => 'IdentifierTypeCode',             PAIR_VALUE => ie_responsavel_w);

            JSON_W.PUT(PAIR_NAME => 'PV1', PAIR_VALUE => JSON_PV1_W.TO_JSON_VALUE());

            JSON_IN1_W.PUT(PAIR_NAME => 'IDNumberInsuranceCompanyName',   PAIR_VALUE => cd_convenio_w);
            JSON_IN1_W.PUT(PAIR_NAME => 'CheckDigitSchemeInsurance',      PAIR_VALUE => cd_categoria_w);
            JSON_IN1_W.PUT(PAIR_NAME => 'IdentifierTypeCodeInsurance',    PAIR_VALUE => cd_tipo_acomodacao_w);
            JSON_IN1_W.PUT(PAIR_NAME => 'IDNumberInsuranceCompanyID',     PAIR_VALUE => cd_usuario_convenio_w);
            JSON_IN1_W.PUT(PAIR_NAME => 'ExpirationDate',                 PAIR_VALUE => TO_CHAR(dt_validade_carteira_w, 'MM/DD/YYYY HH24:MI:SS'));
            JSON_IN1_W.PUT(PAIR_NAME => 'IdentifierInsurance',            PAIR_VALUE => cd_plano_convenio_w);

            JSON_W.PUT(PAIR_NAME => 'IN1', PAIR_VALUE => JSON_IN1_W.TO_JSON_VALUE());       

            JSON_ORC_W.PUT(PAIR_NAME => 'orderControlORC',                PAIR_VALUE =>'NW');
            JSON_ORC_W.PUT(PAIR_NAME => 'entityIdentifierIdPedidoORC',    PAIR_VALUE => :NEW.nr_prescricao);

            JSON_W.PUT(PAIR_NAME => 'ORC', PAIR_VALUE => JSON_ORC_W.TO_JSON_VALUE());      

            JSON_OBR_W.PUT(PAIR_NAME => 'PlacerField2',                   PAIR_VALUE => nr_seq_proc_interno_w);
            JSON_OBR_W.PUT(PAIR_NAME => 'Denomination',                   PAIR_VALUE => ie_lado_w);
            JSON_OBR_W.PUT(PAIR_NAME => 'QuantityMonetaryAmount',         PAIR_VALUE => qt_procedimento_w);
            JSON_OBR_W.PUT(PAIR_NAME => 'EntityIdentifierOBR',            PAIR_VALUE => nr_acesso_dicom_w);
            JSON_OBR_W.PUT(PAIR_NAME => 'FillerField2',                   PAIR_VALUE => nr_seq_prescricao_w);
            JSON_OBR_W.PUT(PAIR_NAME => 'FillerField1',                   PAIR_VALUE => ds_obs_proced_w);

            JSON_W.PUT(PAIR_NAME => 'OBR',PAIR_VALUE =>  JSON_OBR_W.TO_JSON_VALUE());    

			SYS.DBMS_LOB.CREATETEMPORARY(JSON_DATA_W, TRUE);
			JSON_W.TO_CLOB(JSON_DATA_W);

			JSON_DATA_W := BIFROST.SEND_INTEGRATION_CONTENT(NM_EVENT => 'send.hl7.tasy_worklist', DS_CONTENT => JSON_DATA_W, NM_USER => :NEW.NM_USUARIO);
	END PROCESS_EVENT_SEND_WORKLIST;

	BEGIN
    
        IF (wheb_usuario_pck.get_ie_executar_trigger  = 'S') THEN
            IF(:new.dt_liberacao IS NOT NULL AND :old.dt_liberacao IS NULL) THEN
                    SELECT nvl((SELECT pp.nr_prescricao
                            FROM prescr_procedimento pp
                            WHERE pp.nr_prescricao = :new.nr_prescricao
                                AND obter_se_integr_proc_interno(pp.nr_seq_proc_interno, 40 /*CMC x Proton*/, null, pp.ie_lado, :new.cd_estabelecimento) = 'S'
                                AND pp.nr_seq_exame IS NULL
                                AND ROWNUM <= 1), null)
                    INTO qtd_prc_w
                    FROM DUAL;
    
                IF (qtd_prc_w is not null) THEN
                    
                    OPEN c01;
                        LOOP
                            FETCH c01 INTO
                                nm_social_w,
                                dt_nascimento_w,
                                ie_sexo_w,
                                ds_orgao_emissor_ci_w,
                                nr_identidade_w,
                                nr_cpf_w,
                                nr_cert_nasc_w,
                                ie_estado_civil_w,
                                nr_cep_cidade_nasc_w,
                                cd_nacionalidade_w,
                                nr_telefone_celular_w,
                                nr_prontuario_w,
                                cd_cep_w,
                                ds_endereco_w,
                                nr_endereco_w,
                                ds_complemento_w,
                                ds_bairro_w,
                                cd_tipo_logradouro_w,
                                cd_municipio_ibge_w,
                                ds_municipio_w,
                                sg_estado_w,
                                ds_email_w,
                                nr_telefone_w,
                                nm_contato_um_w,
                                nm_contato_dois_w,
                                dt_entrada_w,
                                nr_atendimento_w,
                                cd_procedencia_w,
                                cd_medico_resp_w,
                                cd_estabelecimento_w,
                                cd_convenio_w,
                                cd_categoria_w,
                                cd_tipo_acomodacao_w,
                                cd_usuario_convenio_w,
                                dt_validade_carteira_w,
                                cd_plano_convenio_w,
                                ie_responsavel_w,
                                cd_pessoa_responsavel_w,                            
                                nr_seq_proc_interno_w,
                                ie_lado_w,
                                qt_procedimento_w,
                                nr_acesso_dicom_w,
                                nr_seq_prescricao_w,
                                nr_crm_w,
                                uf_crm_w,
                                cd_especialidade_ext_w,
                                ds_obs_proced_w;
                            EXIT WHEN c01%notfound;
                                BEGIN
                                    PROCESS_EVENT_SEND_WORKLIST;
                                END;
                        END LOOP;
                    close c01;
                END IF;
            END IF;
        END IF;
END TIE_WORKLIST_HL7_TASY;
/
