create or replace 
trigger smart_tipo_exame_afterinsert
after insert or update on proc_interno
for each row

declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
ie_action_w			varchar2(255);

begin

if	(inserting) and 
	(:new.ie_tipo_util = 'E') then
	ie_action_w := 'IE_ACTION_P=CREATE';
	gerar_int_padrao.gravar_integracao('329', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
elsif	(inserting) and 
	(:new.nr_seq_exame_lab is not null) then
	ie_action_w := 'IE_ACTION_P=CREATE';
	gerar_int_padrao.gravar_integracao('309', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
end if;

if	(updating)  and 
	(:new.ie_tipo_util = 'E') and 
	(:new.ie_situacao = 'A') then
	ie_action_w := 'IE_ACTION_P=UPDATE';
	gerar_int_padrao.gravar_integracao('329', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
	--insert into temp_saps (estabelecimento) values ('9');
elsif	(updating) and 
	(:new.nr_seq_exame_lab is not null) and 
	(:new.ie_situacao = 'A') then
	ie_action_w := 'IE_ACTION_P=UPDATE';
	gerar_int_padrao.gravar_integracao('309', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
end if;

if	(updating) and 
	(:new.ie_tipo_util = 'E') and 
	(:new.ie_situacao = 'I') then
	ie_action_w := 'IE_ACTION_P=DELETE';
	gerar_int_padrao.gravar_integracao('329', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
elsif	(updating) and 
	(:new.nr_seq_exame_lab is not null) 
	and (:new.ie_situacao = 'I') then
	ie_action_w := 'IE_ACTION_P=DELETE';
	gerar_int_padrao.gravar_integracao('309', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
end if;

end;
/