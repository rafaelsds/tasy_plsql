create or replace TRIGGER ESCALA_AUDIT_ATUAL 
BEFORE INSERT OR UPDATE ON ESCALA_AUDIT 
FOR EACH ROW 
DECLARE 
  qt_reg_w    number(1);
  sql_w       varchar2(200);
  qt_pontos_w number(3, 0);
BEGIN
    if (:new.nr_hora is null) or (:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then 
        begin  
            :new.nr_hora := to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24')); 
        end; 
    end if;
    
    if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then 
        goto Final; 
    end if; 
 
    if (:new.cd_pessoa_fisica is null) and (:new.nr_atendimento is not null) then 
        :new.cd_pessoa_fisica := obter_pessoa_atendimento(:new.nr_atendimento, 'C'); 
    end if;
    
    /** Medical Device **/
    begin
        sql_w := 'call CALCULA_AUDIT_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10) into :qt_pontos_w';        
        execute immediate sql_w using in :new.qt_frequencia,
                                      in :new.qt_dose_dia,
                                      in :new.qt_freq_cinco_dose,
                                      in :new.qt_nao_consegue_fazer,
                                      in :new.qt_beber_manha,
                                      in :new.qt_culpado_beber,
                                      in :new.qt_nao_lembrar,
                                      in :new.qt_ferimento_prej,
                                      in :new.qt_sugeriu_parar,
                                      in :new.qt_nao_consegue_parar,
                                      out qt_pontos_w;
    exception
        when others then
            qt_pontos_w := null;    
    end;
    :new.qt_pontos := qt_pontos_w;    
 
    <<Final>> 
    qt_reg_w := 0; 
END;
/