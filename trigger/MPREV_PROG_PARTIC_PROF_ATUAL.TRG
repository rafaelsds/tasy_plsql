CREATE OR REPLACE TRIGGER MPREV_PROG_PARTIC_PROF_ATUAL
before insert or update ON MPREV_PROG_PARTIC_PROF
FOR EACH ROW
DECLARE

BEGIN

if	(trunc(:new.dt_fim_acomp) < trunc(:new.dt_inicio_acomp)) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(832205);
end if;

END;
/