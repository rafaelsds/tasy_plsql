create or replace trigger pls_proc_participante_delete
before delete on pls_proc_participante
for each row

declare
	ds_log_call_w 	Varchar2(1500);
	ds_observacao_w	Varchar2(4000);
	ie_opcao_w	Varchar2(3);
begin
if	(pls_se_aplicacao_tasy = 'S')then
	ie_opcao_w := '2';
else
	ie_opcao_w := '1';
end if;

ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

ds_observacao_w := 	'Conta partic: ' || :old.nr_sequencia || pls_util_pck.enter_w ||
			'Conta proc: ' || :old.nr_seq_conta_proc || pls_util_pck.enter_w ||
			'M�dico: ' || :old.cd_medico || pls_util_pck.enter_w ||
			'M�dico imp: ' || :old.cd_medico_imp || pls_util_pck.enter_w ||
			'Prestador: ' || :old.nr_seq_prestador;
 
insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
					ds_log, ds_log_call, ds_funcao_ativa, 
					ie_aplicacao_tasy, nm_maquina,ie_opcao,
					nr_seq_conta_proc_partic, pls_conta_proc)
		values		( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROC_PARTICIPANTE', 
					ds_observacao_w, ds_log_call_w, obter_funcao_ativa, 
					pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, ie_opcao_w,
					:old.nr_sequencia, :old.nr_seq_conta_proc);

end pls_proc_participante_delete;
/
