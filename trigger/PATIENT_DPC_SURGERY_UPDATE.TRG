create or replace trigger PATIENT_DPC_SURGERY_UPDATE
before insert or update on PATIENT_DPC_SURGERY
for each row
begin
declare 
cd_classification_w         	dpc_surgery.cd_classification%type;
cd_mdc_w            	 	dpc_surgery.cd_mdc%type;
nr_seq_edition_w        	dpc_surgery.nr_seq_edition%type;
cd_k_surgery_1_w        	dpc_surgery.cd_k_surgery_1%type;
cd_k_surgery_2_w        	dpc_surgery.cd_k_surgery_2%type;
cd_k_surgery_3_w        	dpc_surgery.cd_k_surgery_3%type;
cd_k_surgery_4_w        	dpc_surgery.cd_k_surgery_4%type;
cd_k_surgery_5_w        	dpc_surgery.cd_k_surgery_5%type;

begin
if    (:new.nr_seq_dpc_surgery is not null) then
  select    	a.cd_classification,
		a.cd_mdc,
		a.nr_seq_edition,
		a.cd_k_surgery_1,
		a.cd_k_surgery_2,
		a.cd_k_surgery_3,
		a.cd_k_surgery_4,
		a.cd_k_surgery_5
    into     	cd_classification_w,
		cd_mdc_w,
		nr_seq_edition_w,
		cd_k_surgery_1_w,
		cd_k_surgery_2_w,
		cd_k_surgery_3_w,
		cd_k_surgery_4_w,
		cd_k_surgery_5_w
    from    dpc_surgery a
    where    a.nr_sequencia = :new.nr_seq_dpc_surgery;
end if;
end;  
end;
/
