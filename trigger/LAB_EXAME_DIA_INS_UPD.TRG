create or replace trigger LAB_EXAME_DIA_INS_UPD
before insert or update or delete on LAB_EXAME_DIA
for each row

declare

ie_operacao_p	number(1);

begin

if	(wheb_usuario_pck.is_evento_ativo(296) = 'S') then

	IF 	INSERTING THEN 
		ie_operacao_p	:= 1;
	elsif	UPDATING then
		ie_operacao_p	:= 2;
	elsif	DELETING then
		ie_operacao_p	:= 3;
	end if;

	integrar_tasylab(	
				null, 
				null, 
				296, 
				null, 
				null, 
				null, 
				null, 
				null, 
				null, 
				:new.nr_sequencia, 
				'LAB_EXAME_DIA',
				ie_operacao_p,
				'N');
	
end if;

end;
/