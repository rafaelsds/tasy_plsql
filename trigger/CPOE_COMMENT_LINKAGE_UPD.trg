CREATE OR REPLACE TRIGGER cpoe_comment_linkage_upd BEFORE
    INSERT OR UPDATE ON cpoe_comment_linkage
    FOR EACH ROW
DECLARE
    si_format_value_w    cpoe_std_comment.si_format_value%TYPE;
    ds_text_interface_w  VARCHAR2(20);
BEGIN
    IF ( nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S' ) THEN
        IF
            :new.ds_additional_text IS NOT NULL
            AND :new.nr_seq_std_comment IS NOT NULL
        THEN
            :new.ds_text_interface := NULL;
            SELECT
                MAX(si_format_value)
            INTO si_format_value_w
            FROM
                cpoe_std_comment a
            WHERE
                a.nr_sequencia = :new.nr_seq_std_comment;

            IF si_format_value_w IS NOT NULL THEN
                BEGIN
                    :new.ds_text_interface := obter_formato_interface(ds_valor_p => :new.ds_additional_text,
                                                                     ie_formato_p => si_format_value_w);

                EXCEPTION
                    WHEN OTHERS THEN
                        NULL;
                END;
            END IF;

        END IF;

    END IF;
END;
/
