create or replace trigger pls_solicitacao_rescisao_ins
before insert or update on pls_solicitacao_rescisao
for each row
declare
qt_registro_w		pls_integer;
nr_seq_contrato_w	pls_contrato.nr_sequencia%type;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(updating) and
		(:new.nr_contrato <> :old.nr_contrato) then
		select	count(1)
		into	qt_registro_w
		from	pls_solic_rescisao_benef
		where	nr_seq_solicitacao	= :new.nr_sequencia;
		
		if	(qt_registro_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(783818); --Ja foram selecionados os beneficiarios. Nao sera possivel alterar o contrato!
		end if;
	end if;

	--Atualizar o contrato
	if	(:new.nr_contrato is not null) then
		select	max(nr_sequencia)
		into	nr_seq_contrato_w
		from	pls_contrato
		where	nr_contrato = :new.nr_contrato;
	
	
		if	(nr_seq_contrato_w is not null) then
			:new.nr_seq_contrato := nr_seq_contrato_w;
		else
			wheb_mensagem_pck.exibir_mensagem_abort(853575); --O contrato informado nao existe. Verifique!
		end if;
	else
		:new.nr_seq_contrato	:= null;
	end if;

	if	(:new.nr_seq_contrato is not null) then
		:new.ie_tipo_contratacao := pls_obter_dados_contrato(:new.nr_seq_contrato,'TC');
	else	
		:new.ie_tipo_contratacao := pls_obter_dados_intercambio(:new.nr_seq_intercambio,'TC');
	end if;	
end if;

end;
/