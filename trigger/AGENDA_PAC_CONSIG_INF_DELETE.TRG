create or replace trigger agenda_pac_consig_inf_delete
before delete on agenda_pac_consig_inf
for each row

declare
ds_alteracao_w		varchar2(4000) := null;
nr_seq_agenda_w		number(10);
solicitacao_medica_w	varchar2(255);

begin

begin
select max(nr_seq_agenda),
       max(ds_material)
into   nr_seq_agenda_w,
       solicitacao_medica_w
from   agenda_pac_consignado
where  nr_sequencia = :old.nr_seq_age_consig;
exception
when others then
	nr_seq_agenda_w := 0;
end;

if	(nr_seq_agenda_w > 0) then

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'C') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'EC',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	
	
	if	(:old.qt_material is not null) and
		(nvl(:old.ie_tipo_informacao,'X') = 'DE') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796525,
								'QT_MATERIAL='||:old.qt_material||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'EDE',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	
	
	if	(:old.qt_material is not null) and
		(nvl(:old.ie_tipo_informacao,'X') = 'DI') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796525,
								'QT_MATERIAL='||:old.qt_material||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'EDI',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	
	
	if	(:old.qt_material is not null) and
		(nvl(:old.ie_tipo_informacao,'X') = 'CO') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796525,
								'QT_MATERIAL='||:old.qt_material||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ECO',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	
	
	if	(:old.ie_retirada is not null) and
		(nvl(:old.ie_tipo_informacao,'X') = 'RE') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796527,
								'IE_RETIRADA='||:old.ie_retirada||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ERE',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	
	

	if	((nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') or (nvl(:old.DS_CME_PREPARADO,'XPTO') <> 'XPTO')) and
		(nvl(:old.ie_tipo_informacao,'X') = 'M') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796528,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_CME_PREPARADO='||:old.DS_CME_PREPARADO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'ECM',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'A') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'EAG',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'E') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'EE',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'F') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'EF',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'R') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'ER',ds_alteracao_w,:old.nm_usuario);
		end if;	
	end if;	
end if;	


end;
/
