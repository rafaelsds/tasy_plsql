create or replace trigger mprev_prog_partic_prof_insert
before insert on mprev_prog_partic_prof
for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Apenas tratar para alimentar tamb�m o sequencial do participante para que apare�a no profissional respons�vel
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

declare

nr_seq_participante_w	mprev_participante.nr_sequencia%type;

begin

if	(:new.nr_seq_participante is null) and
	(:new.nr_seq_programa_partic is not null) then
	select	a.nr_seq_participante
	into	nr_seq_participante_w
	from	mprev_programa_partic a
	where	a.nr_sequencia = :new.nr_seq_programa_partic;
	
	:new.nr_seq_participante := nr_seq_participante_w;
end if;

end;
/