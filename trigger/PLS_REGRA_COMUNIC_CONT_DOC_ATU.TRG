create or replace trigger pls_regra_comunic_cont_doc_atu
before insert or update on pls_regra_comunic_cont_doc
for each row
declare
begin

if	(:new.nr_seq_relatorio is not null) then
	select	cd_classif_relat,
		cd_relatorio
	into	:new.cd_classif_relat,
		:new.cd_relatorio
	from	relatorio
	where	nr_sequencia	= :new.nr_seq_relatorio;
else
	:new.cd_classif_relat	:= null;
	:new.cd_relatorio	:= null;
end if;

end;
/