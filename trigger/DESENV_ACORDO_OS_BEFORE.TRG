create or replace trigger DESENV_ACORDO_OS_BEFORE
before update or insert on DESENV_ACORDO_OS
for each row

declare

begin
if	(updating and (:old.ie_status_acordo = 'I') and
	((:new.ie_status_acordo = 'A') or 
	(:new.ie_status_acordo = 'R'))) then
	:new.dt_posicionamento	:= sysdate;
end if;

if	(not (substr(dbms_utility.format_call_stack,1,4000) like '%MAN_ORDEM_SERVICO_ATUAL%'))then
	update	man_ordem_servico 
	set 	dt_externa_acordo	= :new.dt_entrega_prev,
		dt_atualizacao		= sysdate,
		nm_usuario		= wheb_usuario_pck.get_nm_usuario
	where 	nr_sequencia		= :new.nr_seq_ordem_servico;
end if;

end;
/