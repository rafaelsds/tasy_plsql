create or replace trigger protocolo_medicacao_update
before update on protocolo_medicacao
for each row

declare

begin

if	(:old.ie_status = :new.ie_status) and
	(obter_se_prot_lib_regras = 'S') then
	:new.ie_status		:= 'PA';
	:new.nm_usuario_aprov	:= null;
	:new.dt_aprovacao	:= null;
end if;

end;
/
