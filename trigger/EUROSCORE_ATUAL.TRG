CREATE OR REPLACE TRIGGER EUROSCORE_ATUAL
BEFORE INSERT OR UPDATE ON EUROSCORE
FOR EACH ROW

DECLARE
qt_reg_w	number(1);
qt_score_w			number(22,14);
qt_score2_w     number(22,14);
qt_score_ano_w number(15,8);

sql_w varchar2(32000);
BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_score_ano				:= 0;
:new.qt_sexo					:= 0;
:new.qt_doenca_pulm_cronica			:= 0;
:new.qt_arteriopatia_extracardiaca		:= 0;
:new.qt_disfuncao_neurologica		:= 0;
:new.qt_cirurgia_cardiaca_previa		:= 0;
:new.qt_creatinina_200			:= 0;
:new.qt_endocartite_ativa			:= 0;
:new.qt_preoperativo_critico		:= 0;
:new.qt_angina_instavel			:= 0;
:new.qt_funcao_ve				:= 0;
:new.qt_infarto_miocardio			:= 0;
:new.qt_hipertensao_pulmonar		:= 0;
:new.qt_emergencia				:= 0;
:new.qt_outro_proc_revasc			:= 0;
:new.qt_cirurgia_aorta			:= 0;
:new.qt_ruptura_septal			:= 0;
:new.qt_score					:= 0;

:new.qt_ano					:= nvl(:new.qt_ano,0);

  begin 
      sql_w := 'CALL OBTER_SCORE_ANO_EUROSCORE_MD(:1) INTO :qt_score_ano_w';
      EXECUTE IMMEDIATE sql_w USING IN :new.qt_ano,                                   
                                    OUT qt_score_ano_w;    
  exception
    when others then
      qt_score_ano_w := null;
  end;
      :new.qt_score_ano	:= qt_score_ano_w;
      sql_w := null;


  begin
    sql_w := 'BEGIN OBTER_TOTAL_EUROSCORE_ADIT_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35); END;';
    
    EXECUTE IMMEDIATE sql_w USING IN :new.qt_score_ano,
                                  IN :new.ie_calculo,
                                  IN :new.ie_sexo,
                                  IN :new.ie_doenca_pulm_cronica,
                                  IN :new.ie_arteriopatia_extracardiaca,
                                  IN :new.ie_disfuncao_neurologica,
                                  IN :new.ie_cirurgia_cardiaca_previa,
                                  IN :new.ie_creatinina_200,
                                  IN :new.ie_endocartite_ativa,
                                  IN :new.ie_preoperativo_critico,
                                  IN :new.ie_angina_instavel,
                                  IN :new.ie_funcao_ve,
                                  IN :new.ie_infarto_miocardio,
                                  IN :new.ie_hipertensao_pulmonar,
                                  IN :new.ie_emergencia,
                                  IN :new.ie_outro_proc_revasc,
                                  IN :new.ie_cirurgia_aorta,
                                  IN :new.ie_ruptura_septal,
                                  IN OUT :new.qt_sexo,
                                  IN OUT :new.qt_doenca_pulm_cronica,
                                  IN OUT :new.qt_arteriopatia_extracardiaca,
                                  IN OUT :new.qt_disfuncao_neurologica,
                                  IN OUT :new.qt_cirurgia_cardiaca_previa,
                                  IN OUT :new.qt_creatinina_200,
                                  IN OUT :new.qt_endocartite_ativa,
                                  IN OUT :new.qt_preoperativo_critico,
                                  IN OUT :new.qt_angina_instavel,
                                  IN OUT :new.qt_funcao_ve,
                                  IN OUT :new.qt_infarto_miocardio,
                                  IN OUT :new.qt_hipertensao_pulmonar,
                                  IN OUT :new.qt_emergencia,
                                  IN OUT :new.qt_outro_proc_revasc,
                                  IN OUT :new.qt_cirurgia_aorta,
                                  IN OUT :new.qt_ruptura_septal,
                                  OUT qt_score_w;    
  exception
    when others then
      qt_score_w     := null;     
  end;
      :new.qt_score	:= qt_score_w;
      sql_w := null;
      
  begin
    sql_w := 'BEGIN OBTER_TOTAL_EUROSCORE_LOGIS_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37); END;';
    
    EXECUTE IMMEDIATE sql_w USING IN :new.qt_ano,
                                  IN :new.ie_calculo,
                                  IN :new.ie_sexo,
                                  IN :new.ie_doenca_pulm_cronica,
                                  IN :new.ie_arteriopatia_extracardiaca,
                                  IN :new.ie_disfuncao_neurologica,
                                  IN :new.ie_cirurgia_cardiaca_previa,
                                  IN :new.ie_creatinina_200,
                                  IN :new.ie_endocartite_ativa,
                                  IN :new.ie_preoperativo_critico,
                                  IN :new.ie_angina_instavel,
                                  IN :new.ie_funcao_ve,
                                  IN :new.ie_infarto_miocardio,
                                  IN :new.ie_hipertensao_pulmonar,
                                  IN :new.ie_emergencia,
                                  IN :new.ie_outro_proc_revasc,
                                  IN :new.ie_cirurgia_aorta,
                                  IN :new.ie_ruptura_septal,
                                  IN OUT :new.qt_score_ano,
                                  IN OUT :new.qt_sexo,
                                  IN OUT :new.qt_doenca_pulm_cronica,
                                  IN OUT :new.qt_arteriopatia_extracardiaca,
                                  IN OUT :new.qt_disfuncao_neurologica,
                                  IN OUT :new.qt_cirurgia_cardiaca_previa,
                                  IN OUT :new.qt_creatinina_200,
                                  IN OUT :new.qt_endocartite_ativa,
                                  IN OUT :new.qt_preoperativo_critico,
                                  IN OUT :new.qt_angina_instavel,
                                  IN OUT :new.qt_funcao_ve,
                                  IN OUT :new.qt_infarto_miocardio,
                                  IN OUT :new.qt_hipertensao_pulmonar,
                                  IN OUT :new.qt_emergencia,
                                  IN OUT :new.qt_outro_proc_revasc,
                                  IN OUT :new.qt_cirurgia_aorta,
                                  IN OUT :new.qt_ruptura_septal,
                                  IN OUT qt_score_w,
                                  OUT qt_score2_w;    
  exception
    when others then
      qt_score_w     := null;    
      qt_score2_w    := null;  
  end;
      :new.qt_score	:= qt_score_w;
      :new.qt_score2	:= qt_score2_w;
      sql_w := null;  

<<Final>>
qt_reg_w	:= 0;

END;
/
