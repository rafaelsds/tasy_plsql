create or replace trigger Medic_Uso_Continuo_AftUpdate
after update on medic_uso_continuo
for each row
Declare
qt_reg_w	number(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Gera��o do Hist�rico de Altera��o do Medicamento */
if	(:new.cd_pessoa_fisica <> :old.cd_pessoa_fisica) or
	(nvl(:new.nm_usuario_susp,'X') <> nvl(:old.nm_usuario_susp,'X')) or
	(:new.cd_material <> :old.cd_material) or
	(:new.qt_dose <> :old.qt_dose) or
	(:new.cd_unidade_medida <> :old.cd_unidade_medida) or
	(:new.nr_dias_uso <> :old.nr_dias_uso) or
	(:new.ie_uso_continuo <> :old.ie_uso_continuo) or
	(:new.ie_laudo_lme <> :old.ie_laudo_lme) or
	(:new.cd_cid_principal <> :old.cd_cid_principal) or
 	(:new.cd_cid_secundario <> :old.cd_cid_secundario) or
	(:new.cd_intervalo <> :old.cd_intervalo) or
	(:new.ie_via_aplicacao <> :old.ie_via_aplicacao) or
	(:new.ds_observacao <> :old.ds_observacao) or 
	(:new.dt_inicio <> :old.dt_inicio)then

	
	insert into medic_uso_continuo_hist (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		dt_inicio,
		cd_material,
		qt_dose,
		cd_unidade_medida,
		nr_dias_uso,
		ie_uso_continuo,
		ie_laudo_lme,
		cd_cid_principal,
		cd_cid_secundario,
		cd_intervalo,
		ie_via_aplicacao,
		ds_observacao,
		dt_suspensao,
		nm_usuario_susp,
		nr_seq_medic,
		nr_seq_motivo_susp,
		nr_seq_medic_subst
	) values (
		medic_uso_continuo_hist_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:old.cd_pessoa_fisica,
		:old.dt_inicio,
		:old.cd_material,
		:old.qt_dose,
		:old.cd_unidade_medida,
		:old.nr_dias_uso,
		:old.ie_uso_continuo,
		:old.ie_laudo_lme,
		:old.cd_cid_principal,
		:old.cd_cid_secundario,
		:old.cd_intervalo,
		:old.ie_via_aplicacao,
		:old.ds_observacao,
		:old.dt_suspensao,
		:old.nm_usuario_susp,
		:old.nr_sequencia,
		:old.nr_seq_motivo_susp,
		:old.nr_seq_medic_subst
	);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/
