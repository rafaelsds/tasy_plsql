create or replace trigger ATEND_AVAL_NUT_ENF_ATUAL
before insert or update on ATEND_AVAL_NUT_ENF
for each row
declare


begin
if	(nvl(:old.DT_AVALIACAO,sysdate+10) <> :new.DT_AVALIACAO) and
	(:new.DT_AVALIACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_AVALIACAO, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/
