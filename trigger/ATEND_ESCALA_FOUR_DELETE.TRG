create or replace trigger atend_escala_four_delete
after delete on atend_escala_four
for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '223'	
and		nr_seq_escala  = :old.nr_sequencia;

commit;	

<<final>>
qt_reg_w	:= 0;
end;
/