create or replace trigger movto_cartao_cr_baixa_after
after insert or update on movto_cartao_cr_baixa
for each row

declare

qt_reg_w                number(10);
reg_integracao_p        gerar_int_padrao.reg_integracao;

cd_estabelecimento_w	ctb_documento.cd_estabelecimento%type;
vl_movimento_w		ctb_documento.vl_movimento%type;
ie_contab_cartao_cr_w	parametro_contas_receber.ie_contab_cartao_cr%type;

cursor c01 is
	select	a.nm_atributo
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 5
	and 	a.nm_atributo in ('VL_BAIXA_CARTAO_CR', 'VL_DESP_EQUIP_CARTAO_CR', 'VL_DESPESA_CARTAO_CR');

c01_w		c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select  count(*)
into    qt_reg_w
from    intpd_fila_transmissao
where   nr_seq_documento        = to_char(:new.nr_sequencia)
and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
and     ie_evento       in ('324');


if (qt_reg_w    = 0) then
        gerar_int_padrao.gravar_integracao('324', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);
end if;

if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_movto,:new.nr_seq_parcela,'CCB',:new.dt_baixa,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_movto,:new.nr_seq_parcela,'CCB',:new.dt_baixa,'A',:new.nm_usuario);
end if;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	movto_cartao_cr
where	nr_sequencia = :new.nr_seq_movto;

begin
select	nvl(ie_contab_cartao_cr, 'CB')
into	ie_contab_cartao_cr_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_w;
exception when others then
        ie_contab_cartao_cr_w := 'CB';
end;

begin
select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_cartao_cr, 'CB'), ie_contab_cartao_cr_w)
into    ie_contab_cartao_cr_w
from    ctb_param_lote_contas_rec a
where   a.cd_empresa = obter_empresa_estab(cd_estabelecimento_w)
and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w;
exception when others then
        null;
end;

open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin
	
	vl_movimento_w  :=      case	c01_w.nm_atributo
				when	'VL_BAIXA_CARTAO_CR'      then :new.vl_baixa
				when	'VL_DESP_EQUIP_CARTAO_CR' then :new.vl_desp_equip
				when	'VL_DESPESA_CARTAO_CR'    then :new.vl_despesa
				else
					null
				end;
	
	if	(nvl(vl_movimento_w, 0) <> 0) and (ie_contab_cartao_cr_w = 'CR') and (nvl(:new.nr_seq_trans_financ, 0) <> 0) then
		begin
			
		ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
									:new.dt_baixa,
									5,
									:new.nr_seq_trans_financ,
									44,
									:new.nr_seq_movto,
									:new.nr_sequencia,
									0,
									vl_movimento_w,
									'MOVTO_CARTAO_CR_BAIXA',
									c01_w.nm_atributo,
									:new.nm_usuario);
		
		end;
	end if;

	end;
end loop;
close c01;
end if;

end;
/
