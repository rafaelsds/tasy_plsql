create or replace trigger escala_phq9_atual
before insert or update on escala_phq9
for each row

declare
qt_reg_w	number(1);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto Final;
end if;

:new.qt_pontuacao := (nvl(:new.ie_interesse,0) +
	nvl(:new.ie_deprimido,0) +
	nvl(:new.ie_sono,0) +
	nvl(:new.ie_cansado,0) +
	nvl(:new.ie_apetite,0) +
	nvl(:new.ie_fracasso,0) +
	nvl(:new.ie_concentrar,0) +
	nvl(:new.ie_movimentar,0) +
	nvl(:new.ie_ferir,0));

<<Final>>
qt_reg_w := 0;

end;
/