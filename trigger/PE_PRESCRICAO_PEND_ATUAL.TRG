create or replace
trigger pe_prescricao_pend_atual
after insert or update on pe_prescricao
for each row
declare

qt_reg_w	number(1);
ie_tipo_w	varchar2(10);
nm_usuario_w	varchar2(15);
ie_tipo_reg_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_liberacao is null) then

	if (nvl(:new.IE_AVALIADOR_AUX,'N') = 'S') and
		   (:new.DT_LIBERACAO_AUX is not null)then

			select 	obter_usuario_pf(:new.cd_prescritor)
			into	nm_usuario_w
			from	dual;

			Update    pep_item_pendente
			set	nm_usuario = nm_usuario_w
			where	nr_seq_sae = :new.nr_sequencia;

		end if;
end if;		
	
	if	(nvl(:new.ie_tipo,'SAE') = 'SAPS') then
		ie_tipo_reg_w := 'SAPS';
	elsif (nvl(:new.ie_tipo,'SAE') = 'SAE') then
		ie_tipo_reg_w := 'SAE';
	end if;

if	(:new.dt_liberacao is null) then
		ie_tipo_w := ie_tipo_reg_w;
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'X' || ie_tipo_reg_w;
	end if;
		
if	(ie_tipo_w	is not null)then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nm_usuario);
end if;
<<Final>>
qt_reg_w	:= 0;
end;
/