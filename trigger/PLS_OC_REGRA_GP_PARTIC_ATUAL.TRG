create or replace trigger pls_oc_regra_gp_partic_atual
before insert or update or delete on pls_oc_regra_grupo_partic
for each row

declare
nr_seq_regra_w	pls_oc_regra_participante.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_seq_regra_w := :old.nr_seq_regra_partic;

elsif	(updating) then
	-- hoje, 16/02/2015 n�o existe nenhuma situa��o de atualiza��o que cause alguma
	-- mudan�a nos itens da regra, por isso n�o � necess�rio atualizar nada
	nr_seq_regra_w := null;
	
else
	-- inser��o ou atualiza��o
	nr_seq_regra_w := :new.nr_seq_regra_partic;
end if;

-- se tiver a regra sinaliza que � necess�rio atualizar a tabela
if	(nr_seq_regra_w is not null) then
	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_PARTIC_TM', wheb_usuario_pck.get_nm_usuario, 
								'PLS_OC_REGRA_GP_PARTIC_ATUAL',
								'nr_seq_regra_p=' || nr_seq_regra_w);
end if;

end pls_oc_regra_gp_partic_atual;
/