create or replace trigger ptu_nota_servico_rrs_atual
before insert or update on ptu_nota_servico_rrs
for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

declare

begin

if	(inserting) then
	:new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);
end if;

:new.nr_nota_numerico := somente_numero(nvl(:new.nr_nota,:old.nr_nota));

end;
/
