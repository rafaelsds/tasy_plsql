create or replace trigger escala_padua_atual
before insert or update on escala_padua
for each row

declare
  qt_score_w number(3);
  sql_w      varchar2(300);
  ds_erro_w      varchar2(4000);
  ds_parametro_w varchar2(4000);
begin

  if (wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then   
  
  begin
    
      sql_w := 'CALL OBTER_SCORE_PADUA_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11) INTO :qt_score_w';
      EXECUTE IMMEDIATE sql_w USING IN :new.ie_cancer_ativo,
                                    IN :new.ie_tev_previo,
                                    IN :new.ie_mobilidade_reduzida,
                                    IN :new.ie_condicao_trombofilia,
                                    IN :new.ie_trauma_cirurgia_recente,
                                    IN :new.ie_idade_avancada,
                                    IN :new.ie_parada_card_resp,
                                    IN :new.ie_infarto_avc,
                                    IN :new.ie_infeccao_reumato,
                                    IN :new.ie_obesidade,
                                    IN :new.ie_tratamento_hormonal,
                                    OUT qt_score_w;

  exception
    when others then
      qt_score_w := null;
      ds_erro_w := sqlerrm;
      ds_parametro_w := ':new.nr_atendimento: ' || :new.nr_atendimento || ' - :new.ie_situacao: ' || :new.ie_situacao || ' - :new.cd_profissional: ' || :new.cd_profissional;
      ds_parametro_w := ds_parametro_w || ' - :new.ie_cancer_ativo: ' || :new.ie_cancer_ativo || ' - :new.ie_tev_previo: ' || :new.ie_tev_previo || ' - :new.ie_mobilidade_reduzida: ' || :new.ie_mobilidade_reduzida;
      ds_parametro_w := ds_parametro_w || ' - :new.ie_condicao_trombofilia: ' || :new.ie_condicao_trombofilia || ' - :new.ie_trauma_cirurgia_recente: ' || :new.ie_trauma_cirurgia_recente || ' - :new.ie_idade_avancada: ' || :new.ie_idade_avancada;
      ds_parametro_w := ds_parametro_w || ' - :new.ie_parada_card_resp: ' || :new.ie_parada_card_resp || ' - :new.ie_infarto_avc: ' || :new.ie_infarto_avc || ' - :new.ie_infeccao_reumato: ' || :new.ie_infeccao_reumato || ' - :new.ie_obesidade: ' || :new.ie_obesidade || ' - :new.ie_tratamento_hormonal: ' || :new.ie_tratamento_hormonal;
      ds_parametro_w := ds_parametro_w || ' - qt_score_w: ' || qt_score_w;
      gravar_log_medical_device('ESCALA_PADUA_ATUAL', 'OBTER_SCORE_PADUA_MD', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');
  end;
  :new.qt_score := qt_score_w;

  end if;
end;
/
