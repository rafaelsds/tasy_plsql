create or replace trigger intpd_fila_transmissao_atual
before insert or update on intpd_fila_transmissao
for each row

declare

begin
if	(nvl(:old.ie_status,'NULL') <> nvl(:new.ie_status,'NULL')) then
	:new.dt_status	:=	sysdate;
	
	/*'Tratamento para erro fake, ou seja, sistema externo retorna erro, mas deve ser tratado como sucesso'*/
	if	(:new.ie_status = 'E') and
		(nvl(:old.ie_tipo_erro, 'NULL') <> nvl(:new.ie_tipo_erro, 'NULL')) and
		(:new.ie_tipo_erro = 'K') then
		:new.ie_status	:=	'S';
	end if;
	
	if	(:new.ie_status <> 'E') then
		:new.ie_tipo_erro	:=	null;
	end if;	
end if;

if	(inserting) then
	:new.ds_stack := substr(dbms_utility.format_call_stack,1,4000);
end if;

if	(:new.nr_seq_documento is null) then
	:new.nr_seq_documento	:=	0;
end if;

if	(nvl(:new.ie_geracao,'X') = 'X') then
	:new.ie_geracao	:=	'S';
end if;
end;
/