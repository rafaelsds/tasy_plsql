create or replace TRIGGER ESCALA_CRIB_ATUAL
BEFORE INSERT OR UPDATE ON ESCALA_CRIB
FOR EACH ROW
declare
  qt_reg_w    number(1);
  sql_w       varchar2(200);
  qt_pontos_w number(3, 0);
  ds_erro_w   varchar2(4000);
  ds_parametro_w  varchar2(4000);
BEGIN
  if (:new.nr_hora is null) or (:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	  :new.nr_hora := to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
  end if;

  if (wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
  end if;
  
  /** Medical Device **/
  begin
    sql_w := 'call OBTER_SCORE_ESCALA_CRIB_MD(:1, :2, :3, :4, :5, :6) into :qt_pontos_w';
    execute immediate sql_w using in :new.ie_peso_nascimento,
			                      in :new.ie_idade_gestacional,
			                      in :new.ie_malform_congenita,
			                      in :new.ie_be_max,
                            in :new.ie_fio2_min,
                            in :new.ie_fio2_max,			                    
                            out qt_pontos_w;
  exception
    when others then
      qt_pontos_w := 0;
      ds_erro_w := sqlerrm;
      ds_parametro_w := ':new.nr_atendimento: ' || :new.nr_atendimento || ' - :new.ie_situacao: ' || :new.ie_situacao || ' - :new.cd_profissional: ' || :new.cd_profissional;
      ds_parametro_w := ds_parametro_w || ' - :new.ie_idade_gestacional: ' || :new.ie_idade_gestacional || ' - :new.ie_malform_congenita: ' || :new.ie_malform_congenita || ' - :new.ie_be_max: ' || :new.ie_be_max || ' - :new.ie_fio2_min: ' || :new.ie_fio2_min || ' - :new.ie_fio2_max: ' || :new.ie_fio2_max || ' - qt_pontos_w: ' || qt_pontos_w;
      gravar_log_medical_device('ESCALA_CRIB_ATUAL', 'OBTER_SCORE_ESCALA_CRIB_MD', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');
  end;
  
  :new.qt_pontos := qt_pontos_w;

  <<Final>>
  qt_reg_w := 0;

END;
/