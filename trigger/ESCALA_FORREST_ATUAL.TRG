create or replace trigger escala_forrest_atual
before update or insert on escala_forrest
for each row

declare

begin
if	(:new.ie_forrest = '1A') then
	:new.pr_reavaliacao	:= 55;
	:new.pr_mortalidade	:= 11;
elsif	(:new.ie_forrest = '1B') then
	:new.pr_reavaliacao	:= 55;
	:new.pr_mortalidade	:= 11;
elsif	(:new.ie_forrest = '2A') then
	:new.pr_reavaliacao	:= 43;
	:new.pr_mortalidade	:= 11;
elsif	(:new.ie_forrest = '2B') then
	:new.pr_reavaliacao	:= 22;
	:new.pr_mortalidade	:= 7;
elsif	(:new.ie_forrest = '2C') then
	:new.pr_reavaliacao	:= 10;
	:new.pr_mortalidade	:= 3;
elsif	(:new.ie_forrest = '3') then
	:new.pr_reavaliacao	:= 5;
	:new.pr_mortalidade	:= 2;
end if;

end;
/
