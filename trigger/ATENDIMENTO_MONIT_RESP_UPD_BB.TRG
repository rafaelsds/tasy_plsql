CREATE OR REPLACE TRIGGER ATENDIMENTO_MONIT_RESP_UPD_BB 
AFTER UPDATE ON ATENDIMENTO_MONIT_RESP 
FOR EACH ROW

DECLARE

/* Integracao com o sistema blackboard */
json_aux_bb philips_json;
envio_integracao_bb clob;
retorno_integracao_bb clob;

bb_valueID VARCHAR2(32);

integrar_vent_w VARCHAR2(1);
integrar_sv_w VARCHAR2(1);

BEGIN

integrar_vent_w := OBTER_SE_INTEGRACAO_ATIVA(964, 245);
integrar_sv_w := OBTER_SE_INTEGRACAO_ATIVA(963, 245);

if (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

  IF	(:old.DT_LIBERACAO is NULL) AND
      (:new.DT_LIBERACAO is not NULL) AND 
      ((NVL(:new.IE_INTEGRACAO, 'N') <> 'S')) THEN

      IF (integrar_vent_w = 'S') THEN
      
        IF (:new.IE_RESPIRACAO = 'ESPONT') THEN
            bb_valueID := '41289726de804449a70252cbc820c3c7';

        ELSIF (:new.IE_RESPIRACAO = 'VMNI') OR
              (:new.IE_RESPIRACAO = 'BORB') THEN
            bb_valueID := 'f485c826290e48a6a4c86be64c9f4d47';

        ELSIF (:new.IE_RESPIRACAO = 'VMI') OR
              (:new.IE_RESPIRACAO = 'VMIFB') THEN
            bb_valueID := 'e890895cccd84d569462da49c0cdca35';

        ELSE
            bb_valueID := 'e213c60fd7d74093955adaa6a657ae16';

        END IF;
        
        json_aux_bb := philips_json();
        json_aux_bb.put('typeID', 'CAREPLAN');
        json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'MM-DD-YYYY HH24:MI:SS.SSSSS'));
        json_aux_bb.put('patientHealthSystemStayID', LPAD(COALESCE(TO_CHAR(:new.NR_ATENDIMENTO), ''), 32, '0'));
        json_aux_bb.put('historyID', LPAD('F' || :new.NR_SEQUENCIA, 32, '0'));
        json_aux_bb.put('saveDateTimeUtc', TO_CHAR(f_extract_utc_bb(:new.DT_MONITORIZACAO), 'MM-DD-YYYY HH:MI:SS'));
        json_aux_bb.put('saveDateTimeUtcOffset', '0');
        json_aux_bb.put('groupTypeID', '1f82689907194030876a9f393eade2d9');
        json_aux_bb.put('groupID', LPAD(COALESCE(TO_CHAR(:new.NR_SEQUENCIA), ''), 32, '0'));
        json_aux_bb.put('valueID', TO_CHAR(bb_valueID));
        
        dbms_lob.createtemporary(envio_integracao_bb, TRUE);
        json_aux_bb.to_clob(envio_integracao_bb);
        
        SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Patient_Ventilation',envio_integracao_bb, wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
      END IF;

      IF (integrar_sv_w = 'S') THEN

        IF ((:new.QT_FIO2 is not NULL) AND (:new.QT_FIO2 > 0)) OR 
          ((:new.QT_PEEP is not NULL) AND (:new.QT_PEEP > 0)) OR 
          ((:new.QT_SATURACAO_O2 is not NULL) AND (:new.QT_SATURACAO_O2 > 0)) OR 
          ((:new.QT_FLUXO_OXIGENIO is not NULL) AND (:new.QT_FLUXO_OXIGENIO > 0)) OR 
          ((:new.QT_FREQ_RESP is not NULL) AND (:new.QT_FREQ_RESP > 0)) OR 
          ((:new.QT_VC_PROG is not NULL) AND (:new.QT_VC_PROG > 0)) THEN
          
          INTEGRAR_SINAIS_VITAIS_RESP_BB(:new.NR_SEQUENCIA,
              :new.NR_ATENDIMENTO,
              :new.DT_MONITORIZACAO,
              :new.QT_FIO2,
              :new.QT_PEEP,
              :new.QT_SATURACAO_O2,
              :new.QT_FLUXO_OXIGENIO,
              :new.QT_FREQ_RESP,
              :new.QT_VC_PROG);

        END IF;
      END IF; 
    END IF;

    IF (integrar_vent_w = 'S') THEN
      IF	(:old.dt_inativacao is NULL) AND 
        (:new.dt_inativacao is not NULL) AND
          ((:new.QT_FREQ_RESP is not NULL) AND (:new.QT_FREQ_RESP > 0)) THEN
          json_aux_bb := philips_json();
          json_aux_bb.put('typeID', 'CAREPLAN');
          json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'DD/MM/YYYY HH24:MI:SS'));
          json_aux_bb.put('patientHealthSystemStayID', LPAD(TO_CHAR(:new.NR_ATENDIMENTO), 32, '0'));
          json_aux_bb.put('historyID', LPAD('F' || TO_CHAR(:new.NR_SEQUENCIA), 32, '0'));
          json_aux_bb.put('saveDateTimeUtc', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI:SS'));
          json_aux_bb.put('saveDateTimeUtcOffset', '0');
          json_aux_bb.put('clearForReadmit', '');
          
          dbms_lob.createtemporary(envio_integracao_bb, TRUE);
          json_aux_bb.to_clob(envio_integracao_bb);
          
          SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Cancel_Ventilation_Status',envio_integracao_bb, wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
      END IF;
    END IF;

    IF (integrar_sv_w = 'S') THEN
      IF (:old.dt_inativacao is NULL) AND 
          (:new.dt_inativacao is not NULL) AND 
          (((:new.QT_FIO2 is not NULL) AND (:new.QT_FIO2 > 0)) OR 
          ((:new.QT_PEEP is not NULL) AND (:new.QT_PEEP > 0)) OR 
          ((:new.QT_SATURACAO_O2 is not NULL) AND (:new.QT_SATURACAO_O2 > 0)) OR 
          ((:new.QT_FLUXO_OXIGENIO is not NULL) AND (:new.QT_FLUXO_OXIGENIO > 0)) OR 
          ((:new.QT_FREQ_RESP is not NULL) AND (:new.QT_FREQ_RESP > 0)) OR 
          ((:new.QT_VC_PROG is not NULL) AND (:new.QT_VC_PROG > 0))) THEN
          p_cancelar_flowsheet(:new.nr_sequencia, :new.nr_atendimento, 'E');
      END IF;
    END IF;
end if;

END;
/
