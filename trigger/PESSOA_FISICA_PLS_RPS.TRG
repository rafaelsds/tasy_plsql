create or replace trigger pessoa_fisica_pls_rps
after update on pessoa_fisica
for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

declare
ie_alterou_w		varchar2(1) := 'N';

begin
begin
	if	(nvl(:new.nr_cpf,'0') 		!= nvl(:old.nr_cpf,'0')) or
		(nvl(:new.nm_pessoa_fisica,'0')	!= nvl(:old.nm_pessoa_fisica,'0')) or
		(nvl(:new.cd_cnes,'0')		!= nvl(:old.cd_cnes,'0')) or
		(nvl(:new.cd_municipio_ibge,'0')!= nvl(:old.cd_municipio_ibge,'0')) then
		ie_alterou_w := 'S';
	end if;

	if 	(ie_alterou_w = 'S') then
		pls_gerar_alt_prest_rps(null, :new.cd_pessoa_fisica, null, :old.nr_cpf, :old.cd_municipio_ibge, :old.cd_cnes, :new.nm_usuario, null, null, null);
	end if;
exception
when others then
	null;
end;
end;
/