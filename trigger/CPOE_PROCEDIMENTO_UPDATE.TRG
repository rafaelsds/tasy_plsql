create or replace trigger cpoe_procedimento_update
before update on cpoe_procedimento
for each row

declare

ds_stack_w				varchar2(2000);
ie_possui_niveis_glic_w char(1);
cd_intervalo_w			intervalo_prescricao.cd_intervalo%type;

begin

if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then

if	(:old.nr_seq_prot_glic is null) and
	(:new.nr_seq_prot_glic is not null) then
	
	select	max(nvl('S','N'))
	into	ie_possui_niveis_glic_w
	from	cpoe_proc_glic
	where	nr_seq_procedimento = :new.nr_sequencia;
	
	if	(nvl(ie_possui_niveis_glic_w,'N') = 'N') then
		cpoe_gerar_proc_glic (:new.nr_sequencia, :new.nr_seq_prot_glic, :new.nm_usuario, :new.nr_atendimento, :new.cd_pessoa_fisica, 'N');
	end if;
	
end if;	

if (:new.cd_intervalo is null and :new.nr_seq_prot_glic is not null)  then	
	cd_intervalo_w := obter_intervalo_ccg(:new.nr_seq_prot_glic);	
		
	if (cd_intervalo_w is not null) then
		:new.cd_intervalo :=  cd_intervalo_w;
	end if;
end if;

if	(:new.ie_evento_unico = 'S') then
	if 	(:new.dt_fim is null) then
		:new.ie_duracao := 'P';
		:new.dt_fim := (:new.dt_inicio + 1) -1/86400;
	end if;
	
	if (:new.dt_fim > :new.dt_inicio + 1) then
		:new.ie_duracao := 'P';
		:new.dt_fim := (:new.dt_inicio + 1) -1/86400;
	end if;
end if;

if (nvl(:old.cd_setor_atendimento,0) <> nvl(:new.cd_setor_atendimento,0)) then
	:new.cd_setor_liberacao := null;
end if;

end if;

end;
/
