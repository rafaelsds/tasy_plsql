create or replace trigger OBJETO_SCHEMATIC_INSERT
before INSERT or update ON OBJETO_SCHEMATIC
FOR EACH ROW

DECLARE
ds_w			varchar2(50);
qt_obj_w		number(10);
ds_pasta_w		varchar2(255);
qt_existe_tab_w	number(10);
nr_seq_obj_param_w	OBJETO_SCHEMATIC_PARAM.nr_sequencia%type;
nr_seq_funcao_schematic_w	number(10);
ie_tipo_obj_sup_w	varchar2(10);

/*created by thilma*/

pragma autonomous_transaction;

begin

--if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then

	if  	(:new.IE_TIPO_COMPONENTE = 'WDBP') and
		(:new.NM_TABELA is not null) and
		(:new.ds_objeto = 'WDBPanel') then

		ds_w := substr(initcap(:new.NM_TABELA),1,50);

		select REPLACE(initcap(REPLACE(ds_w,'_',' ')),' ','') || 'WDBP'
		into ds_w
		from  dual;

		:new.DS_OBJETO := ds_w;
		:new.CD_EXP_DESC_OBJ := null;

	elsif 	(:new.IE_TIPO_COMPONENTE = 'WCP') and
		(:new.NR_SEQ_DIC_OBJETO is not null) and
		(:new.ds_objeto = 'WCPanel') then

		ds_w :=  substr(initcap(obter_descricao_padrao('DIC_OBJETO','NM_OBJETO',:new.NR_SEQ_DIC_OBJETO)),1,50);

		select	REPLACE(initcap(REPLACE(ds_w,'_',' ')),' ','')
		into	ds_w
		from	dual;

		:new.DS_OBJETO := ds_w;
		:new.CD_EXP_DESC_OBJ := null;

	elsif 	(:new.IE_TIPO_COMPONENTE = 'WF') and
		(:new.NR_SEQ_DIC_OBJETO is not null) and
		(:new.ds_objeto = 'WFiltro') then

		ds_w := substr(initcap(obter_descricao_padrao('DIC_OBJETO','NM_OBJETO',:new.NR_SEQ_DIC_OBJETO)),1,50);

		select	REPLACE(initcap(REPLACE(ds_w,'_',' ')),' ','')
		into	ds_w
		from	dual;

		:new.DS_OBJETO := ds_w;
		:new.CD_EXP_DESC_OBJ := null;

	elsif 	(:new.IE_TIPO_COMPONENTE = 'WPOPUP') and
		(:new.NR_SEQ_DIC_OBJETO is not null) and
		(:new.ds_objeto = 'WPopupMenu') then

		ds_w := substr(initcap(obter_descricao_padrao('DIC_OBJETO','NM_OBJETO',:new.NR_SEQ_DIC_OBJETO)),1,50);

		select	REPLACE(initcap(REPLACE(ds_w,'_',' ')),' ','')
		into	ds_w
		from	dual;

		:new.DS_OBJETO := ds_w;
		:new.CD_EXP_DESC_OBJ := null;
		
	elsif 	(:new.IE_TIPO_COMPONENTE = 'WDF') and
		(:new.NR_SEQ_DIC_OBJETO is not null) and
		(:new.ds_objeto = 'WDinamicForm') then

		ds_w := substr(initcap(obter_descricao_padrao('DIC_OBJETO','NM_OBJETO',:new.NR_SEQ_DIC_OBJETO)),1,50);

		select	REPLACE(initcap(REPLACE(ds_w,'_',' ')),' ','')
		into	ds_w
		from	dual;

		:new.DS_OBJETO := ds_w;
		:new.CD_EXP_DESC_OBJ := null;
	elsif 	(:new.IE_TIPO_COMPONENTE = 'WSCB') and
		(:new.NR_SEQ_DIC_OBJETO is not null) and
		(:new.ds_objeto = 'WSelecaoCB') then

		ds_w := substr(obter_descricao_padrao('DIC_OBJETO','NM_OBJETO',:new.NR_SEQ_DIC_OBJETO),1,50);

		select	REPLACE(REPLACE(ds_w,'_',' '),' ','')
		into	ds_w
		from	dual;

		:new.DS_OBJETO := ds_w;
		:new.CD_EXP_DESC_OBJ := null;
	end if;

	if 	(:new.IE_TIPO_OBJETO = 'BTN') then

		select	count(*)
		into	qt_obj_w
		from	objeto_schematic
		where	nr_seq_obj_sup	= :new.nr_seq_obj_sup
		and	nr_sequencia	<> :new.nr_sequencia
		and ie_tipo_objeto = 'BTN';
		
		if	(qt_obj_w >= 5) then
			wheb_mensagem_pck.exibir_mensagem_abort(359565); --Quantidade limite de bot�es do WDLG excedida
		end if;

	end if;

	if	(:new.nr_seq_obj_sup is not null) and
		(:new.nr_seq_funcao_schematic is null) then
		
		select	max(nr_seq_funcao_schematic)
		into	nr_seq_funcao_schematic_w
		from	objeto_schematic
		where	nr_sequencia	= :new.nr_seq_obj_sup;
		
		if	(nr_seq_funcao_schematic_w is not null) then
			:new.nr_seq_funcao_schematic := nr_seq_funcao_schematic_w;
		end if;
		
	end if;

	if	((:new.IE_TIPO_OBJETO = 'DDM') or
		 (:new.IE_TIPO_OBJETO = 'T') or 
		 (:new.IE_TIPO_OBJETO = 'IT')) and
		(:new.IE_CONFIGURAVEL <> 'N') then
		:new.IE_CONFIGURAVEL := 'S';
	end if;	
	
	if	(:new.nr_seq_funcao_schematic is null) and
		(:new.nr_seq_bo is null) then
		Raise_application_error(-20011,'This object is without Function Schematic or Business Object!');
	end if;
	
	--OS 1439614
	if	(:new.IE_TIPO_OBJETO = 'C') and
		(:new.ie_tipo_componente = 'WDBP') then
		
		select	max(ie_tipo_objeto)
		into	ie_tipo_obj_sup_w
		from	objeto_schematic
		where	nr_sequencia	= :new.nr_seq_obj_sup;
		
		if	(ie_tipo_obj_sup_w = 'R') then
		
			select	count(*)
			into	qt_obj_w
			from	objeto_schematic
			where	nr_seq_obj_sup = :new.nr_seq_obj_sup
			and	ie_tipo_componente = :new.ie_tipo_componente
			and	nr_sequencia <> :new.nr_sequencia;
			
			if	(qt_obj_w > 0 ) then
				--� permitido apenas 1 DBPanel por regi�o
				wheb_mensagem_pck.exibir_mensagem_abort(840040);
			end if;
		
		end if;
	end if;
	
	if	(:new.IE_TIPO_OBJETO = 'C') and	
		(:new.IE_TIPO_COMPONENTE = 'WDBP') and
		(:new.nm_tabela is not null) and
		(:new.nr_seq_visao is not null) and
		(:new.nr_seq_bo is not null) then
		GERAR_OBJ_SCHEMATIC_VISAO(:new.nr_sequencia,:new.nr_seq_visao,:new.nm_usuario);
	end if;

	--Criar cadastro de restri��es de pastas automaticamente
	/*if	(:new.IE_TIPO_OBJETO = 'DDM') or
		(:new.IE_TIPO_OBJETO = 'T') or 
		(:new.IE_TIPO_OBJETO = 'IT') or
		(:old.IE_TIPO_OBJETO = 'DDM') or
		(:old.IE_TIPO_OBJETO = 'T') or 
		(:old.IE_TIPO_OBJETO = 'IT') then
		
		select	substr(obter_desc_expressao(:new.CD_EXP_DESC_OBJ),1,254)
		into	ds_pasta_w
		from	dual;
		
		if	(ds_pasta_w <> 'Tab') and
			(ds_pasta_w <> 'Tab item') and
			(ds_pasta_w <> 'In Tab') and
			(ds_pasta_w <> 'In Tab item') and
			(ds_pasta_w <> 'Drop down menu') and
			(substr(ds_pasta_w,1,10) <> 'Schematic') and
			(substr(ds_pasta_w,1,2) <> 'MR') and
			(substr(ds_pasta_w,1,7) <> 'Regi�o') and
			(substr(ds_pasta_w,1,10) <> 'Navegador')then
		
			select	COUNT(1)
			into	qt_existe_tab_w
			from	OBJETO_SCHEMATIC_PARAM
			where	OBTER_DESC_EXPRESSAO(CD_EXP_SCHEM_PARAM) = ds_pasta_w
			and	cd_funcao 		= :new.cd_funcao;
			
			if	(ds_pasta_w is not null) and
				(qt_existe_tab_w = 0)then
				
				select	OBJETO_SCHEMATIC_PARAM_seq.nextval
				into	nr_seq_obj_param_w
				from	dual;
				
				insert into OBJETO_SCHEMATIC_PARAM (	
					CD_EXP_SCHEM_PARAM,
					CD_FUNCAO,
					DT_ATUALIZACAO,
					NM_USUARIO,
					NR_SEQUENCIA,
					VL_PARAMETRO_PADRAO
					)
				values	(
					:new.CD_EXP_DESC_OBJ,
					:new.cd_funcao,
					sysdate,
					nvl(:old.nm_usuario, :new.nm_usuario),
					nr_seq_obj_param_w,
					'S'
					);
					
				:new.NR_SEQ_OBJ_SCHEM_PARAM	:= nr_seq_obj_param_w;
				commit;
				
			end if;
		end if;
		
	end if;*/


	/*exception
	when others then
		null;
	end;*/

	commit;

--end if;	
	
end OBJETO_SCHEMATIC_INSERT;
/