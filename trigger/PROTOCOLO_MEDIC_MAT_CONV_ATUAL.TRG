create or replace trigger protocolo_medic_mat_conv_atual
before insert or update on protocolo_medic_mat_conv
for each row

declare

begin

if	(obter_se_prot_lib_regras = 'S') then
	update	protocolo_medicacao
	set	nm_usuario_aprov	=	null,
		dt_aprovacao		=	null,
		ie_status		=	'PA'
	where	cd_protocolo		=	:new.cd_protocolo
	and	nr_sequencia		=	:new.nr_seq_medicacao;
end if;

end;
/
