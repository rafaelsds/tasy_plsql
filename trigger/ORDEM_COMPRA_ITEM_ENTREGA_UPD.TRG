CREATE OR REPLACE TRIGGER ORDEM_COMPRA_ITEM_ENTREGA_UPD
BEFORE UPDATE ON ORDEM_COMPRA_ITEM_ENTREGA
FOR EACH ROW

DECLARE

ds_log_w	varchar2(2000);
dt_liberacao_w	date;

begin

select	max(dt_liberacao)
into	dt_liberacao_w
from	ordem_compra
where	nr_ordem_compra = :new.nr_ordem_compra;

if	(dt_liberacao_w is not null) and
	(:old.qt_prevista_entrega <> :new.qt_prevista_entrega) then
	gravar_log_tasy(27, substr(dbms_utility.format_call_stack,1,2000), :new.nm_usuario);
end if;

end;
/