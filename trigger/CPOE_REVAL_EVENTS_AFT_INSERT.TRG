create or replace trigger cpoe_reval_events_aft_insert
after insert or update on cpoe_revalidation_events for each row
declare

	ds_stack_w		varchar2(2000);
	ds_log_cpoe_w	varchar(2000);
	dt_min_date_w	date := to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss');

begin

	if	(nvl(:new.nr_sequencia,0) <> nvl(:old.nr_sequencia,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_sequencia(' || nvl(:old.nr_sequencia,0) || '/' || nvl(:new.nr_sequencia,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_atendimento(' || nvl(:old.nr_atendimento,0) || '/' || nvl(:new.nr_atendimento,0)||'); ',1,2000);
	end if;
	
	if	(nvl(:new.cd_medico,0) <> nvl(:old.cd_medico,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_medico(' || nvl(:old.cd_medico,0) || '/' || nvl(:new.cd_medico,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_medico_anterior,0) <> nvl(:old.cd_medico_anterior,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_medico_anterior(' || nvl(:old.cd_medico_anterior,0) || '/' || nvl(:new.cd_medico_anterior,0)||'); ',1,2000);
	end if;
	
	if	(nvl(:new.dt_validacao, dt_min_date_w) <> nvl(:old.dt_validacao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_validacao(' || nvl(to_char(:old.dt_validacao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_validacao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_envio_comunicado, dt_min_date_w) <> nvl(:old.dt_envio_comunicado, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_envio_comunicado(' || nvl(to_char(:old.dt_envio_comunicado,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_envio_comunicado,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_dialysis,0) <> nvl(:old.nr_seq_dialysis,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_dialysis(' || nvl(:old.nr_seq_dialysis,0) || '/' || nvl(:new.nr_seq_dialysis,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_diet,0) <> nvl(:old.nr_seq_diet,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_diet(' || nvl(:old.nr_seq_diet,0) || '/' || nvl(:new.nr_seq_diet,0)||'); ',1,2000);
	end if;
	
	if	(nvl(:new.nr_seq_hemotherapy,0) <> nvl(:old.nr_seq_hemotherapy,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_hemotherapy(' || nvl(:old.nr_seq_hemotherapy,0) || '/' || nvl(:new.nr_seq_hemotherapy,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_material,0) <> nvl(:old.nr_seq_material,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_material(' || nvl(:old.nr_seq_material,0) || '/' || nvl(:new.nr_seq_material,0)||'); ',1,2000);
	end if;
	
	if	(nvl(:new.nr_seq_exam,0) <> nvl(:old.nr_seq_exam,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_exam(' || nvl(:old.nr_seq_exam,0) || '/' || nvl(:new.nr_seq_exam,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_recomendation,0) <> nvl(:old.nr_seq_recomendation,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_recomendation(' || nvl(:old.nr_seq_recomendation,0) || '/' || nvl(:new.nr_seq_recomendation,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_gasotherapy,0) <> nvl(:old.nr_seq_gasotherapy,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_gasotherapy(' || nvl(:old.nr_seq_gasotherapy,0) || '/' || nvl(:new.nr_seq_gasotherapy,0)||'); ',1,2000);
	end if;	

	if	(nvl(:new.nr_seq_revalidation_rule,0) <> nvl(:old.nr_seq_revalidation_rule,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_revalidation_rule(' || nvl(:old.nr_seq_revalidation_rule,0) || '/' || nvl(:new.nr_seq_revalidation_rule,0)||'); ',1,2000);
	end if;	

	if	(ds_log_cpoe_w is not null) then
		
		if (nvl(:old.nr_sequencia, 0) > 0) then
			ds_log_cpoe_w := substr('Altera��es(old/new)= ' || ds_log_cpoe_w,1,2000);
		else
			ds_log_cpoe_w := substr('Cria��o(old/new)= ' || ds_log_cpoe_w,1,2000);
		end if;

		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		ds_log_cpoe_w := substr('CPOE_REVAL_EVENTS_AFT_INSERT: ' || ds_log_cpoe_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')',1,2000);

		insert into log_cpoe(	nr_sequencia,
								nr_atendimento,
								dt_atualizacao, 
								nm_usuario,
								nr_seq_material,
								nr_seq_dieta, 
								nr_seq_gasoterapia, 
								nr_seq_recomendacao, 
								nr_seq_procedimento, 
								nr_seq_dialise, 
								nr_seq_hemoterapia,
								ds_log,
								ds_stack) 
					values (	log_cpoe_seq.nextval, 
								:new.nr_atendimento, 
								sysdate,
								:new.nm_usuario,
								:new.nr_seq_material,
								:new.nr_seq_diet,
								:new.nr_seq_gasotherapy,
								:new.nr_seq_recomendation,
								:new.nr_seq_exam,
								:new.nr_seq_dialysis,
								:new.nr_seq_hemotherapy,
								ds_log_cpoe_w,
								ds_stack_w);
	end if;

exception
	when others then
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		ds_log_cpoe_w := substr('EXCEPTION CPOE_REVAL_EVENTS_AFT_INSERT ' || to_char(sqlerrm),1, 2000);
		
		insert into log_cpoe(nr_sequencia, 
							nr_atendimento, 
							dt_atualizacao, 
							nm_usuario, 
							ds_log, 
							ds_stack) 
		values (			log_cpoe_seq.nextval, 
							:new.nr_atendimento, 
							sysdate, 
							:new.nm_usuario, 
							ds_log_cpoe_w, 
							ds_stack_w);
end;
/