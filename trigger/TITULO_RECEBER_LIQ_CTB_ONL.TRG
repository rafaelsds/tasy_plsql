create or replace trigger titulo_receber_liq_ctb_onl
after insert or update on titulo_receber_liq
for each row

declare

ie_contab_cr_w          ctb_param_lote_tesouraria.ie_contab_cr%type;
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type;
nr_multiplicador_w      number(1);
vl_movimento_w          ctb_documento.vl_movimento%type;
dt_movimento_w          ctb_documento.dt_competencia%type;
nr_seq_trans_financ_w   ctb_documento.nr_seq_trans_financ%type;
nr_seq_info_w           ctb_documento.nr_seq_info%type;
nr_documento_w          ctb_documento.nr_documento%type;
nr_seq_doc_compl_w      ctb_documento.nr_seq_doc_compl%type;
nr_doc_analitico_w      ctb_documento.nr_doc_analitico%type;
nm_tabela_w             ctb_documento.nm_tabela%type;
qt_registros_w          number(10);
ie_pls_w                number(10);

cursor 	c01 is
select  a.nm_atributo,
        10 cd_tipo_lote_contab
from    atributo_contab a
where   (a.cd_tipo_lote_contab = 5
and     a.nm_atributo in (  'VL_DESCONTOS', 'VL_JUROS', 'VL_MULTA', 
                            'VL_DESPESA_BANCARIA',  'VL_ORIGINAL_BAIXA', 
                            'VL_CAMBIAL_ATIVO', 'VL_CAMBIAL_PASSIVO' ))
or      (a.cd_tipo_lote_contab = 10
and     a.nm_atributo in (  'VL_RECEBIDO_TESOURARIA' ));

c01_w c01%rowtype;

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select  cd_estabelecimento
into    cd_estabelecimento_w
from    titulo_receber
where   nr_titulo = :new.nr_titulo;

begin
select  x.ie_contab_cr
into    ie_contab_cr_w
from(
    select  nvl(ie_contab_cr,'N') ie_contab_cr
    from    ctb_param_lote_tesouraria a
    where   a.cd_empresa    = obter_empresa_estab(cd_estabelecimento_w)
    and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w
order by nvl(a.cd_estab_exclusivo,0) desc) x
where  rownum = 1;
exception
  when others then
    ie_contab_cr_w := 'N';
end;

select  count(1)
into    ie_pls_w
from    titulo_receber t
where   t.nr_titulo = :new.nr_titulo
and not exists (select  1
                from    movto_trans_financ x
                where   x.nr_sequencia = :new.nr_seq_movto_trans_fin)
and exists (select  1
            from    pls_titulo_rec_liq_mens x
            where   x.nr_titulo = :new.nr_titulo
            and     x.nr_seq_baixa = :new.nr_sequencia)
and exists (select  1
            from    pls_mensalidade x,
                    titulo_receber  y
            where   y.nr_seq_mensalidade = x.nr_sequencia
            and     y.nr_titulo = :new.nr_titulo );

if (    ie_contab_cr_w = 'S'
        and ie_pls_w = 0
        and :new.ie_lib_caixa = 'S'
        and :new.nr_seq_conta_banco is null
        and :new.nr_seq_lote_enc_contas is null /* Francisco - 30/10/2010 - Vai entrar em outro lote */
        and :new.nr_seq_pls_lote_camara is null /* Francisco - 30/10/2010 - Vai entrar em outro lote */
        and :new.nr_seq_movto_trans_fin is not null -- Edgar 27/05/2008, OS 93779 
        ) then
        
        begin     
        open c01;
        loop
        fetch c01 into  
                c01_w;
        exit when c01%notfound;
          begin
              
            nr_multiplicador_w := 1;
            
            if  (:new.ie_acao <> 'I') then
              nr_multiplicador_w := -1;
            end if;
            
            vl_movimento_w:=        case c01_w.nm_atributo 
                        when 'VL_DESCONTOS' then :new.vl_descontos * nr_multiplicador_w
                        when 'VL_JUROS' then :new.vl_juros * nr_multiplicador_w
                        when 'VL_MULTA'  then :new.vl_multa * nr_multiplicador_w
                        when 'VL_DESPESA_BANCARIA'  then :new.vl_despesa_bancaria * nr_multiplicador_w
                        when 'VL_RECEBIDO_TESOURARIA'  then :new.vl_recebido * nr_multiplicador_w
                        when 'VL_ORIGINAL_BAIXA'  then :new.vl_recebido + :new.vl_juros + :new.vl_multa + :new.vl_descontos
                        when 'VL_CAMBIAL_ATIVO'  then :new.vl_cambial_ativo * nr_multiplicador_w
                        when 'VL_CAMBIAL_PASSIVO'  then :new.vl_cambial_passivo * nr_multiplicador_w
                        end;
                        
            dt_movimento_w          := :new.dt_recebimento;
            nr_seq_trans_financ_w   := :new.nr_seq_trans_fin;
            nr_seq_info_w           := 14;
            nr_documento_w          := :new.nr_titulo;
            nr_seq_doc_compl_w      := :new.nr_sequencia;
            nr_doc_analitico_w      := null;
            nm_tabela_w             := 'TITULO_RECEBER_LIQ_CONTAB_V2';
            
            select count(1)
            into   qt_registros_w
            from   ctb_documento a
            where  a.nr_documento = nr_documento_w
            and    a.nr_seq_doc_compl = nr_seq_doc_compl_w
            and    a.cd_tipo_lote_contabil = c01_w.cd_tipo_lote_contab
            and    a.cd_estabelecimento = cd_estabelecimento_w
            and    a.dt_competencia = dt_movimento_w
            and    a.nr_seq_trans_financ = nr_seq_trans_financ_w
            and    a.nr_seq_info = nr_seq_info_w
            and    a.nm_tabela = nm_tabela_w
            and    a.nm_atributo = c01_w.nm_atributo
            and    a.vl_movimento = vl_movimento_w;
            
            if     (qt_registros_w = 0) and (nvl(vl_movimento_w, 0) <> 0 and nvl(nr_seq_trans_financ_w, 0) <> 0) then
                   begin 

                   ctb_concil_financeira_pck.ctb_gravar_documento  (   cd_estabelecimento_w,
                                                                       dt_movimento_w,
                                                                       c01_w.cd_tipo_lote_contab,
                                                                       nr_seq_trans_financ_w,
                                                                       nr_seq_info_w,
                                                                       nr_documento_w,
                                                                       nr_seq_doc_compl_w,
                                                                       nr_doc_analitico_w,
                                                                       vl_movimento_w,
                                                                       nm_tabela_w,
                                                                       c01_w.nm_atributo,
                                                                       :new.nm_usuario);
                   end;
            end if;
          end;
        end loop;
        close c01;
        end;
end if;
end if;
end;
/