CREATE OR REPLACE TRIGGER Rep_jejum_Insert
BEFORE INSERT ON Rep_jejum
FOR EACH ROW

DECLARE

dt_inicio_prescr_w	date;
dt_validade_prescr_w	date;	

BEGIN

if	(:new.dt_inicio is null) then

	select	max(dt_inicio_prescr),
		max(dt_validade_prescr)
	into	dt_inicio_prescr_w,
		dt_validade_prescr_w
	from	prescr_medica
	where	nr_prescricao = :new.nr_prescricao;
	
	:new.dt_inicio	:= dt_inicio_prescr_w;
	--:new.dt_fim	:= dt_validade_prescr_w;
	
end if;

END;
/
