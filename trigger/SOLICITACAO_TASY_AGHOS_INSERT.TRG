create or replace trigger solicitacao_tasy_aghos_insert
before insert on solicitacao_tasy_aghos
for each row

declare
	cd_pessoa_fisica_w	varchar2(10);

begin

begin
	if	(:new.cd_pessoa_fisica is null) then
		
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	pessoa_fisica
		where	trim(upper(nm_pessoa_fisica)) = trim(upper(:new.nm_paciente));
		
		if	(cd_pessoa_fisica_w is not null) then
			:new.cd_pessoa_fisica := cd_pessoa_fisica_w;
		end if;	
	end if;
exception
	when	others then
		null;
end;
	
end;
/
