create or replace trigger MAN_ORDEM_SERVICO_EXEC_LOG
before insert on man_ordem_servico_exec
for each row

declare

ds_dados_tasy_w		varchar2(4000);
ds_call_stack_w		varchar2(4000);
ds_session_tasy_w	varchar2(4000);
ds_enter_w		varchar2(10) := chr(13) || chr(10);

begin

if	(nvl(wheb_usuario_pck.get_cd_estabelecimento,0) > 0) then
	begin
	ds_dados_tasy_w	:= substr('# Dados Tasy #'  || ds_enter_w ||
				'Estabelecimento: ' || wheb_usuario_pck.get_cd_estabelecimento || ds_enter_w ||
				'Funcao: ' 	|| wheb_usuario_pck.get_cd_funcao || ds_enter_w ||
				'Perfil: ' 	|| wheb_usuario_pck.get_cd_perfil || ds_enter_w ||
				'Usuario: ' 	|| wheb_usuario_pck.get_nm_usuario || ds_enter_w ||
				'Form: ' 	|| wheb_usuario_pck.get_ds_form || ds_enter_w ||
				'Maquina: ' 	|| wheb_usuario_pck.get_nm_maquina || ds_enter_w ||
				'Estacao: ' 	|| wheb_usuario_pck.get_nm_estacao || ds_enter_w || ds_enter_w,1,2000);
	end;
end if;

select	substr('# Dados sessao DB #' || ds_enter_w ||
		'sid: ' 	|| sid || ds_enter_w ||
		'serial: ' 	|| serial# || ds_enter_w ||
		'logon_time: ' 	|| to_char(logon_time,'dd/mm/yyyy hh24:mi:ss') || ds_enter_w ||
		'username: ' 	|| username || ds_enter_w ||
		'service_name: ' || service_name || ds_enter_w ||
		'schemaname: ' 	|| schemaname || ds_enter_w ||
		'machine: ' 	|| machine || ds_enter_w ||
		'terminal: ' 	|| terminal || ds_enter_w ||
		'osuser: ' 	|| osuser || ds_enter_w ||
		'program: ' 	|| program || ds_enter_w ||
		'module: ' 	|| module,1,2000)
into	ds_session_tasy_w
from	v$session
where	audsid = (select userenv ('SESSIONID') from dual);

ds_session_tasy_w	:= substr(ds_dados_tasy_w || ds_session_tasy_w,1,2000);

insert into LOG_TASY(
		nr_sequencia,
		dt_atualizacao,
		cd_log,
		nm_usuario,
		ds_log)
	values (LOG_TASY_SEQ.nextval,	
		sysdate,
		1770,
		wheb_usuario_pck.get_nm_usuario,
		ds_session_tasy_w);

ds_call_stack_w	:= substr('LOG_HIST| NEW.NR_SEQ_ORDEM: ' 	|| :new.nr_seq_ordem ||
				' | NEW.NR_SEQUENCIA: ' 	|| :new.nr_sequencia ||
				' | NEW.DT_ATUALIZACAO: ' 	|| to_char(:new.dt_atualizacao,'dd/mm/yyyy hh24:mi:ss') ||
				' | CALL_STACK: ' 			|| dbms_utility.format_call_stack,1,2000);

insert into LOG_TASY(
		nr_sequencia,
		dt_atualizacao,
		cd_log,
		nm_usuario,
		ds_log)
	values (LOG_TASY_SEQ.nextval,	
		sysdate,
		1770,
		wheb_usuario_pck.get_nm_usuario,
		ds_call_stack_w);
end;
/