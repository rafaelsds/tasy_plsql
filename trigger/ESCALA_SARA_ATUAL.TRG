create or replace trigger ESCALA_SARA_ATUAL
before insert or update on escala_sara
for each row

declare
qt_rel_pao_w		number(10) := 0;
qt_topografia_w		number(10) := 0;
qt_cmhml_w			number(10) := 0;
qt_presssao_w		number(10) := 0;
qt_divisor_w		number(10) := 0;
qt_total_w			number(10,2) := 0;

begin

if	(:new.ie_radiografia_torax >= 0 ) then
	qt_topografia_w	:= :new.ie_radiografia_torax;
end if;	

if	(:new.QT_REL_PAO2_FIO2 >= 300 ) then
	qt_rel_pao_w	:= 0;
elsif	(:new.QT_REL_PAO2_FIO2 between 225 and 299) then
	qt_rel_pao_w	:= 1;
elsif	(:new.QT_REL_PAO2_FIO2 between 175 and	224) then
	qt_rel_pao_w	:= 2;
elsif	(:new.QT_REL_PAO2_FIO2 between 100 and	174) then
	qt_rel_pao_w	:= 3;	
elsif	(:new.QT_REL_PAO2_FIO2 < 100) then
	qt_rel_pao_w	:= 4;
end if; 	

if	(:new.qt_compl_cmh2o >= 80) then
	qt_cmhml_w := 0;
elsif	(:new.qt_compl_cmh2o between 60 and 79) then	
		qt_cmhml_w := 1;
elsif	(:new.qt_compl_cmh2o between 40 and 59) then	
		qt_cmhml_w := 2;		
elsif	(:new.qt_compl_cmh2o between 20 and 39) then	
		qt_cmhml_w := 3;
elsif	(:new.qt_compl_cmh2o <= 20) then	
		qt_cmhml_w := 4;		
end if;

if	(:new.qt_pressao_cmh2o <= 5) then
	qt_presssao_w := 0;
elsif	(:new.qt_pressao_cmh2o between 6 and 8) then
	qt_presssao_w := 1;
elsif	(:new.qt_pressao_cmh2o between 9 and 11) then
	qt_presssao_w := 2;	
elsif	(:new.qt_pressao_cmh2o between 12 and 14) then
	qt_presssao_w := 3;		
elsif	(:new.qt_pressao_cmh2o >=15) then
	qt_presssao_w := 4;		
end if;	

if	(:new.ie_radiografia_torax is not null) then
	qt_divisor_w := qt_divisor_w + 1;
end if;
if	(:new.qt_rel_pao2_fio2 is not null) then
	qt_divisor_w := qt_divisor_w + 1;
end if;
if	(:new.qt_compl_cmh2o is not null) then
	qt_divisor_w := qt_divisor_w + 1;
end if;
if	(:new.qt_pressao_cmh2o is not null) then
	qt_divisor_w := qt_divisor_w + 1;
end if;	

if	(qt_divisor_w = 0) then
	qt_divisor_w 	:= 1;
end if;
qt_total_w	:= 	((nvl(qt_topografia_w,0) + nvl(qt_rel_pao_w,0) + nvl(qt_cmhml_w,0) + nvl(qt_presssao_w,0))/qt_divisor_w);

:new.qt_pontuacao := round(qt_total_w,2);	
end;
/
