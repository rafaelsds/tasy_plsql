create or replace trigger DECLARACAO_OBITO_SBIS_IN
before insert or update on DECLARACAO_OBITO
for each row

declare 
ie_inativacao_w		varchar2(1);
nr_log_seq_w		number(10);
begin
	IF (INSERTING) THEN
		select 	log_alteracao_prontuario_seq.nextval
		into 	nr_log_seq_w
		from 	dual; 
		
		insert into log_alteracao_prontuario (nr_sequencia, 
											 dt_atualizacao, 
											 ds_evento, 
											 ds_maquina, 
											 cd_pessoa_fisica, 
											 ds_item, 
											 nr_atendimento,
											 dt_liberacao,
											 dt_inativacao,
											 nm_usuario) values 
											 (nr_log_seq_w, 
											 sysdate, 
											 obter_desc_expressao(656665) ,  
											 wheb_usuario_pck.get_nm_maquina, 
											 obter_pessoa_atendimento(:new.nr_atendimento,'C'), 
											 obter_desc_expressao(314115), 
											 :new.nr_atendimento,
											 :new.dt_liberacao, 
											 :new.dt_inativacao,
											 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
	else 
		ie_inativacao_w := 'N';
		
		if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
			ie_inativacao_w := 'S';	
		end if;
		
		select 	log_alteracao_prontuario_seq.nextval
		into 	nr_log_seq_w
		from 	dual; 
		
		insert into log_alteracao_prontuario (nr_sequencia, 
											 dt_atualizacao, 
											 ds_evento, 
											 ds_maquina, 
											 cd_pessoa_fisica, 
											 ds_item, 
											 nr_atendimento, 
											 dt_liberacao,
											 dt_inativacao,
											 nm_usuario) values 
											 (nr_log_seq_w, 
											 sysdate, 
											 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ), 
											 wheb_usuario_pck.get_nm_maquina, 
											 obter_pessoa_atendimento(:new.nr_atendimento,'C'), 
											 obter_desc_expressao(314115), 
											 :new.nr_atendimento,
											 :new.dt_liberacao, 
											 :new.dt_inativacao,
											 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
	end if;
end;
/
