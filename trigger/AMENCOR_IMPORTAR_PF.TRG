create or replace
procedure amencor_importar_pf( cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

				
ds_operacao_w		varchar2(255);
ds_erro_w		varchar2(2000);
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
cd_sistema_ant_w	pessoa_fisica.cd_sistema_ant%type;
nr_seq_compl_w		compl_pessoa_fisica.nr_sequencia%type;
nm_usuario_w		usuario.nm_usuario%type := nvl(nm_usuario_p,'OS1284412');
				

Cursor C01 is
	select	y.*
	from	(select	substr(x.cd_pessoa_,1,10)	cd_pessoa_fisica,--
			substr(x.nm_pessoa_,1,60)	nm_pessoa_fisica,--
			trunc(x.dt_nascime)			dt_nascimento,--
			substr(decode(x.ie_sexo,'Masculino','M','Feminino','F'),1,1)	ie_sexo,--
			substr(x.nr_identid,1,15)	nr_identidade,--
			substr(x.nr_cpf,1,11)		nr_cpf,--
			substr(x.nr_prontua,1,10)	nr_prontuario,
			substr(x.nm_pai,1,60)		nm_contato_pai, --complemento pai
			substr(x.nm_mae,1,60)		nm_contato_mae, --complemento mae
			'S' ie_reg_imp
		from	pessoa_fisica_amencor x
		where	nvl(x.ie_reg_imp,'N') = 'N'
		order by nvl(x.ie_reg_imp,' ')) y
	where	rownum <= 10000;
	
c01_w	c01%rowtype;
			
begin

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	
	ds_erro_w		:= null;
	cd_sistema_ant_w	:= c01_w.cd_pessoa_fisica;
	select	pessoa_fisica_seq.nextval
	into	cd_pessoa_fisica_w
	from	dual;
	ds_operacao_w		:= 'INSERT PESSOA_FISICA';
		
		insert into pessoa_fisica (
					cd_pessoa_fisica,--
					ie_tipo_pessoa,--
					dt_atualizacao,--
					nm_usuario,--
					dt_atualizacao_nrec,--
					nm_usuario_nrec,--
					nm_pessoa_fisica,--
					cd_sistema_ant,--
					dt_nascimento,--
					ie_sexo,--
					nr_cpf,--
					nr_identidade,--
					cd_estabelecimento,
					cd_empresa,
					nr_prontuario)--
				values (cd_pessoa_fisica_w,
					1,
					sysdate,
					nm_usuario_w,
					sysdate,
					nm_usuario_w,
					c01_w.nm_pessoa_fisica,
					cd_sistema_ant_w,
					c01_w.dt_nascimento,
					c01_w.ie_sexo,
					c01_w.nr_cpf,
					c01_w.nr_identidade,
					cd_estabelecimento_p,
					obter_empresa_estab(cd_estabelecimento_p),
					c01_w.nr_prontuario);
				
			if	(c01_w.nm_contato_mae is not null) then
				begin
				ds_operacao_w	:= 'INSERT COMPL_PESSOA_FISICA - M�E (5)';
				
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_seq_compl_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica = cd_pessoa_fisica_w;

				insert into compl_pessoa_fisica (
						cd_pessoa_fisica,
						nr_sequencia,
						ie_tipo_complemento,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nm_contato
						)
					values (cd_pessoa_fisica_w,
						nr_seq_compl_w,
						'5',
						sysdate,
						nm_usuario_w,
						sysdate,
						nm_usuario_w,
						c01_w.nm_contato_mae
						);
				exception
				when others then
					c01_w.ie_reg_imp	:= 'P';
					ds_erro_w		:= substr(ds_operacao_w || ': ' || sqlerrm(sqlcode) || chr(13) || chr(10) || ds_erro_w,1,2000);
				end;
			end if;
		
			if	(c01_w.nm_contato_pai is not null) then
				begin
				ds_operacao_w	:= 'INSERT COMPL_PESSOA_FISICA - PAI (4)';
				
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_seq_compl_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica = cd_pessoa_fisica_w;
			
				insert into compl_pessoa_fisica (
						cd_pessoa_fisica,
						nr_sequencia,
						ie_tipo_complemento,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nm_contato
						)
					values (cd_pessoa_fisica_w,
						nr_seq_compl_w,
						'4',
						sysdate,
						nm_usuario_w,
						sysdate,
						nm_usuario_w,
						c01_w.nm_contato_pai
						);
				exception
				when others then
					c01_w.ie_reg_imp	:= 'P';
					ds_erro_w		:= substr(ds_operacao_w || ': ' || sqlerrm(sqlcode) || chr(13) || chr(10) || ds_erro_w,1,2000);
				end;
			end if;
		exception
		when others then
			c01_w.ie_reg_imp	:= 'N';
			ds_erro_w		:= substr(ds_operacao_w || ': ' || sqlerrm(sqlcode),1,2000);
		end;
	update	pessoa_fisica_amencor
	set	ie_reg_imp	= c01_w.ie_reg_imp,
		ds_erro		= ds_erro_w
	where	cd_pessoa_	= c01_w.cd_pessoa_fisica;
	
	end loop;
close C01;

--commit;
end amencor_importar_pf;
/