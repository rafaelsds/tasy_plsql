create or replace trigger pls_rec_glosa_proc_atual
before update on pls_rec_glosa_proc
for each row
 
declare

ds_log_call_w	varchar2(1500);
ds_log_w 	varchar2(4000);
nr_seq_conta_w		pls_conta.nr_sequencia%type;

begin

if ( nvl(:new.vl_acatado,0) != nvl(:old.vl_acatado,0)) then

	select nr_seq_conta
	into	nr_seq_conta_w
	from	pls_conta_proc
	where 	nr_sequencia = :new.nr_seq_conta_proc;

	ds_log_w := ' seq proc rec = '||:new.nr_sequencia||' new vl_acatado = '||nvl(:new.vl_acatado,0)||' old vl_acatado = '||nvl(:old.vl_acatado,0);
	ds_log_call_w := substr(	' Funcao ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
							' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);
							
	ds_log_w := ds_log_w ||' -> '||ds_log_call_w;
	--nr_seq_conta, nr_seq_conta_proc, nr_seq_conta_mat referem-se aos registros da pls_conta, pls_conta_proc e pls_conta_mat
	insert into pls_rec_glosa_item_log(nr_sequencia, nr_seq_conta, nr_seq_conta_proc, nr_seq_conta_mat, ds_log, dt_atualizacao_nrec, dt_atualizacao, nm_usuario, nm_usuario_nrec) 
	values( pls_rec_glosa_item_log_seq.nextval, nr_seq_conta_w, :new.nr_seq_conta_proc, null, ds_log_w, sysdate, sysdate, :new.nm_usuario, :new.nm_usuario);
end if;

							
end;
/
