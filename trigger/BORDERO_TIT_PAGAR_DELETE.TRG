create or replace trigger bordero_tit_pagar_delete
before delete on bordero_tit_pagar
for each row

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TP',:old.dt_atualizacao,'E',:old.nm_usuario);
end if;

end;
/
