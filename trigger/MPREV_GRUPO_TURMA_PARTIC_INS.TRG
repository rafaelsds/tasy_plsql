CREATE OR REPLACE TRIGGER MPREV_GRUPO_TURMA_PARTIC_INS
AFTER INSERT ON MPREV_GRUPO_TURMA_PARTIC
FOR EACH ROW
DECLARE

nr_sequencia_w		comunic_interna.nr_sequencia%type;
ds_comunicado_w		comunic_interna.ds_comunicado%type;
nm_turma_w		varchar(255);
ds_titulo_w		varchar2(400);
ie_ci_incluir_w		varchar2(1);
ie_email_incluir_w		varchar2(1);
ie_ci_excluir_w		varchar2(1);
ie_email_excluir_w		varchar2(1);
usuario_destino_w		varchar2(255);
ds_email_destino_w		varchar2(255);
ds_email_origem_w		varchar2(255) := null;
ie_email_valido_w		varchar2(255) := 'N';

begin

hdm_obter_dados_turma(:new.nr_sequencia, :new.nr_seq_turma, ie_ci_incluir_w, ie_email_incluir_w, ie_ci_excluir_w, ie_email_excluir_w, usuario_destino_w, ds_email_destino_w);

select 	max(a.nm_turma)
into 	nm_turma_w
from   	mprev_grupo_col_turma a
where  	a.nr_sequencia = :new.nr_seq_turma;

ds_comunicado_w	:= 	wheb_mensagem_pck.get_texto(416810, 'NM_PARTICIPANTE='||SUBSTR(mprev_obter_nm_participante(:new.nr_seq_participante),1,255)||';NM_TURMA='||nm_turma_w);
ds_titulo_w 	:= 	wheb_mensagem_pck.get_texto(416824, 'NM_TURMA='||nm_turma_w);

if (ie_ci_incluir_w = 'S') then

	if (usuario_destino_w is not null) then
	begin

		insert	into comunic_interna
				(dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao)
	    	values		(sysdate,
				ds_titulo_w,
				ds_comunicado_w,
				wheb_usuario_pck.get_nm_usuario,
				sysdate,
				'N',
				usuario_destino_w,
				comunic_interna_seq.nextval,
				'N',
				sysdate);
	    exception
	    when others then
			insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,
					wheb_usuario_pck.get_nm_usuario,
					 33,
					 wheb_mensagem_pck.get_texto(419754, 'NM_TURMA='||nm_turma_w||';NM_PARTICIPANTE='||SUBSTR(mprev_obter_nm_participante(:old.nr_seq_participante),1,255)));
	end;
	end if;
end if;

if (ie_email_incluir_w = 'S') then

	if (ds_email_destino_w is not null) then

		select 	substr(obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'E'),1,255)
		into 	ds_email_origem_w
		from 	dual;

		if (ds_email_origem_w is null) then

			select 	substr(obter_compl_pf(obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'C'), 1, 'M'),1,255)
			into 	ds_email_origem_w
			from 	dual;

		end if;

		select 	obter_se_email_valido(ds_email_destino_w)
		into 	ie_email_valido_w
		from	dual;

		if (ie_email_valido_w = 'S') then
		begin
			enviar_email(ds_titulo_w, ds_comunicado_w, ds_email_origem_w, ds_email_destino_w, wheb_usuario_pck.get_nm_usuario, 'M');
		exception
	   	when others then
			insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log)  values (sysdate,
			         	 	wheb_usuario_pck.get_nm_usuario,
			         		33,
			         		wheb_mensagem_pck.get_texto(419745, 'NM_TURMA='||nm_turma_w||';DS_TITULO='||ds_titulo_w||';DS_COMUNICADO='||ds_comunicado_w||';DS_EMAIL_ORIGEM='||ds_email_origem_w||';DS_EMAIL_DESTINO='||ds_email_destino_w));
		end;
		end if;
	end if;
end if;

end;
/
