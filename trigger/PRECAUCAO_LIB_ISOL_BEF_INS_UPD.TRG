create or replace trigger precaucao_lib_isol_bef_ins_upd
before insert or update on cih_precaucao_lib_isol
for each row

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if :new.cd_perfil is null then
		if :new.nm_usuario_lib is null then
			wheb_mensagem_pck.exibir_mensagem_abort(1043214);
		end if;
	end if;
end if;
end precaucao_lib_isol_bef_ins_upd;
/