create or replace trigger preco_mat_status_hist_update
before update on preco_mat_status_hist
for each row
declare
cd_tab_mat_atualizacao_w	number(4);
begin
		
if	(:new.IE_STATUS_ITEM = 1) then

	select	nvl(max(cd_tab_mat_atualizacao),0)
	into	cd_tab_mat_atualizacao_w
	from	convenio_preco_mat
	where	cd_tab_preco_mat = :new.cd_tab_preco_mat;

	if	(cd_tab_mat_atualizacao_w > 0) then
		update	preco_material a
		set	ie_preco_venda = 'N'
		where	a.cd_tab_preco_mat		= cd_tab_mat_atualizacao_w
		and	a.cd_material        		= :new.cd_material
		and	a.dt_inicio_vigencia		< :new.dt_vigencia
		and	a.vl_preco_venda		< nvl(:new.vl_material,0);
	end if;
end if;

end;
/