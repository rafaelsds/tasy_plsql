CREATE OR REPLACE TRIGGER MOVTO_CARTAO_CR_DELETE
BEFORE DELETE ON movto_cartao_cr
FOR EACH ROW

DECLARE

dt_fechamento_w		date	:= null;
nr_recibo_w		number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:old.nr_seq_caixa_rec is not null) then
	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :old.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/* Nao e possivel excluir esse cartao!
		O recibo #@NR_RECIBO#@ ja foi fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(267091, 'NR_RECIBO=' || nr_recibo_w);
	end if;
end if;
end if;

end;
/
