create or replace trigger banco_estab_pix_before_insert
before insert or update
on banco_estab_pix 
for each row
begin
    if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
        pix_pck.valida_chave( ds_chave_p => :new.ds_chave,
                              ie_tipo_chave_p => :new.ie_tipo_chave);
    end if;
end banco_estab_pix_before_insert;
/
