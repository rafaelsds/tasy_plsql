create or replace
trigger cadastro_coletivo_consulta
after insert on atendimento_paciente
for each row

declare

nr_min_duracao_divido_w		number;
nm_usuario_origem_w		agenda_consulta.nm_usuario_origem%type;
dt_agenda_w			agenda_consulta.dt_agenda%type;
nr_minuto_duracao_w		agenda_consulta.nr_minuto_duracao%type;
cd_agenda_w			agenda_consulta.cd_agenda%type;
cd_turno_w			agenda_consulta.cd_turno%type;
ie_status_agenda_w		agenda_consulta.ie_status_agenda%type;
cd_medico_w			agenda_consulta.cd_medico%type;
cd_especialidade_w		agenda_consulta.cd_especialidade%type;
cd_convenio_w			agenda_consulta.cd_convenio%type;
cd_tipo_acomodacao_w		agenda_consulta.cd_tipo_acomodacao%type;
nr_seq_hora_w			agenda_consulta.nr_seq_hora%type;
nr_sequencia_w			agenda_consulta.nr_sequencia%type;
ie_agend_coletivo_w		agenda_consulta.ie_agend_coletivo%type;
nr_seq_age_cons_w		agenda_integrada_item.nr_seq_agenda_cons%type;
qt_age_coletivo_w		number(15);


begin
			
if	(Obter_Valor_Param_Usuario(869, 410, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo) = 'S') then
	
	select	max(nr_seq_agenda_cons)
	into	nr_seq_age_cons_w
	from	agenda_integrada_item a,
			agenda_integrada b
	WHERE	a.nr_seq_agenda_int = b.nr_sequencia
	AND	b.nr_sequencia = (	SELECT	MAX(nr_seq_ag_integrada) 
							FROM	agendamento_coletivo 
							WHERE	cd_pessoa_fisica = :new.cd_pessoa_fisica
							and	nm_usuario = :new.nm_usuario_atend)
	AND	SYSDATE BETWEEN NVL(dt_inicio_agendamento, SYSDATE) AND NVL(dt_fim_agendamento + 1/24, SYSDATE);
		
	if (nr_seq_age_cons_w is not null) then
		select	nm_usuario_origem,
			dt_agenda,
			nr_minuto_duracao,
			cd_agenda,
			cd_turno,
			ie_status_agenda,
			cd_medico,
			cd_especialidade,
			cd_convenio,
			nr_sequencia,
			ie_agend_coletivo
		into	nm_usuario_origem_w,
			dt_agenda_w,
			nr_minuto_duracao_w,
			cd_agenda_w,
			cd_turno_w,
			ie_status_agenda_w,
			cd_medico_w,
			cd_especialidade_w,
			cd_convenio_w,
			nr_sequencia_w,
			ie_agend_coletivo_w
		from	agenda_consulta
		where	nr_sequencia = nr_seq_age_cons_w;

		select	count(*)
		into	qt_age_coletivo_w
		from	agendamento_coletivo 
		where	nr_seq_agenda_int = (	select	max(nr_seq_agenda_int) 
						from	agendamento_coletivo
						where	cd_pessoa_fisica = :new.cd_pessoa_fisica) 
		and	ie_status_agenda <> 'C';
				
		if	(qt_age_coletivo_w > 0) then
		
			select	(nr_minuto_duracao_w / qt_age_coletivo_w)
			into	nr_min_duracao_divido_w
			from	dual;

			select	max(nr_seq_hora)+1
			into	nr_seq_hora_w
			from	agenda_consulta
			where	cd_agenda = cd_agenda_w
			and		ie_status_agenda = ie_status_agenda_w
			and		dt_agenda = dt_agenda_w;
		

			
			insert into agenda_consulta (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nm_usuario_origem,
				dt_agenda,
				nr_minuto_duracao,
				cd_agenda,
				cd_turno,
				ie_status_agenda,
				cd_pessoa_fisica,
				nm_paciente,
				cd_medico,
				cd_especialidade,
				nr_seq_agend_coletiva,
				ie_agend_coletivo,
				nr_seq_hora,
				cd_convenio,
				nr_atendimento,
				cd_tipo_acomodacao)
			values(	 agenda_consulta_seq.nextval,
				sysdate,
				wheb_usuario_pck.get_nm_usuario,
				nm_usuario_origem_w,
				dt_agenda_w,
				nr_min_duracao_divido_w,
				cd_agenda_w,
				cd_turno_w,
				ie_status_agenda_w,     
				:new.cd_pessoa_fisica,
				obter_nome_pf(:new.cd_pessoa_fisica),
				cd_medico_w,
				cd_especialidade_w,
				(select max(nr_sequencia) from agendamento_coletivo where cd_pessoa_fisica = :new.cd_pessoa_fisica),
				'S',
				nr_seq_hora_w,
				cd_convenio_w,
				:new.nr_atendimento,
				cd_tipo_acomodacao_w);
				

			if	(ie_agend_coletivo_w is null) then
				update	agenda_consulta
				set	    ie_agend_coletivo = 'S' 
				where	nr_sequencia = nr_sequencia_w; 
			end if;
		
			
		end if;
	end if;
end if;      

end;
/