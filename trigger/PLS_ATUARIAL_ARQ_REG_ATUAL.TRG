create or replace trigger pls_atuarial_arq_reg_atual 
before insert or update on pls_atuarial_arq_reg 
for each row

begin

-- Ao atualizar ou inserir um novo registro, a regra inteira ser� marcada como inconsistente
update	pls_atuarial_arq_regra
set	ie_consistente		= 'N',
	dt_atualizacao		= sysdate,
	nm_usuario		= :new.nm_usuario
where	nr_sequencia		= :new.nr_seq_regra;


end pls_atuarial_arq_reg_atual;
/