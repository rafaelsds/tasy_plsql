CREATE OR REPLACE TRIGGER adep_irrigacao_etapa_update
after UPDATE ON adep_irrigacao_etapa
FOR EACH ROW

begin

if	(:new.ie_evento_valido = 'N') and
	(:old.ie_evento_valido = 'S') then
	update	atendimento_perda_ganho a
	set	ie_situacao = 'I'
	where	a.nr_seq_evento_ivc = :new.nr_sequencia;
end if;

END;
/
