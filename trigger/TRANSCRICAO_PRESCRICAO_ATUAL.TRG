create or replace trigger TRANSCRICAO_PRESCRICAO_ATUAL
before insert or update on TRANSCRICAO_PRESCRICAO
for each row
declare


begin
if	(nvl(:old.DT_ANEXO,sysdate+10) <> :new.DT_ANEXO) and
	(:new.DT_ANEXO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ANEXO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/
