create or replace trigger PROC_PAC_DESCRICAO_PEND
before insert or update on PROC_PAC_DESCRICAO
for each row
declare

cd_procediemnto_w   varchar(100);
ie_origem_proced_w  varchar(100);

begin
    if (:new.nm_usuario = 'integration' and wheb_usuario_pck.get_ie_executar_trigger	= 'S' ) then
       begin  
         select cd_procedimento,
                 ie_origem_proced
            into cd_procediemnto_w,
                 ie_origem_proced_w
          from proc_interno 
          where nr_sequencia = :new.nr_seq_proc_interno;
        exception
            when no_data_found then
                cd_procediemnto_w   :=  null;
                ie_origem_proced_w  :=  null;
        end;                        
    :new.cd_procedimento  := cd_procediemnto_w;
    :new.ie_origem_proced := ie_origem_proced_w;
    end if;

end;
/