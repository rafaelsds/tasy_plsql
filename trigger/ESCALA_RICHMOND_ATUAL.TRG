create or replace trigger ESCALA_RICHMOND_ATUAL
before insert or update on ESCALA_RICHMOND
for each row

declare

begin
  if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
    if (OBTER_SE_INTEGRACAO_ATIVA(977, 245) = 'S') then
      if	(:old.DT_LIBERACAO is NULL) and
        (:new.DT_LIBERACAO is NOT NULL) then
        integrar_sinais_vitais_bb(
            nr_sequencia_p => :new.nr_sequencia,
            nr_atendimento_p => :new.nr_atendimento,
            dt_sinal_vital_p => :new.dt_avaliacao,
            ie_rass_p => :new.ie_rass);
      end if;

      if (:old.dt_inativacao is null AND :new.dt_inativacao is not null) then
        p_cancelar_flowsheet(:new.nr_sequencia, :new.nr_atendimento, 'F');
      end if;
    end if;

    if	(:new.nr_hora is null) or   
      (:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then 
      begin  
      :new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
      end; 
    end if;
  end if;
  
end;
/