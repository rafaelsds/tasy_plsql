create or replace trigger unitarizacao_atual
before insert or update on unitarizacao
for each row

declare
ie_manipulacao_w	varchar2(1);
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;
begin
if	(:new.cd_pessoa_manipulacao is not null) then
	select	nvl(min(ie_manipulacao),'N')
	into	ie_manipulacao_w
	from	sup_pessoa_unitarizacao
	where	cd_estabelecimento = cd_estabelecimento_w
	and	cd_pessoa_fisica = :new.cd_pessoa_manipulacao
	and	ie_situacao = 'A';
	
	if	(ie_manipulacao_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(266345);
		--'Pessoa f�sica n�o permitida para manipula��o!');
	end if;
end if;

end;
/