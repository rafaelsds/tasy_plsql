create or replace trigger nut_local_ref_insert_update
before insert or update on nut_local_refeicao
for each row

declare

pragma autonomous_transaction;
ie_possui_local_pac_estab_w varchar2(1);

begin

select count(1)
into	ie_possui_local_pac_estab_w
from	nut_local_refeicao
where	cd_estabelecimento = :new.cd_estabelecimento
and	(ie_local_paciente = 'S' and :new.ie_local_paciente = 'S')
and 	ie_situacao = 'A';


if 	(ie_possui_local_pac_estab_w >= 1 and :new.ie_situacao = 'A') then

	wheb_mensagem_pck.exibir_mensagem_abort(1145703);

end if;

end;
/
