create or replace trigger dar_tables_control_before
before insert or update
   on dar_tables_control
   for each row

declare
   sql_stmt_w          dar_tables_control.ds_sql%type;
   message_w           dar_consist_sql.ds_consistencia%type;
   message_aux_w       varchar2(4000);
   nr_posicao_espaco_w number(10);
   nr_posicao_from_w   number(10);
   nr_posicao_where_w  number(10);
   nr_tamanho_string_w number(10);
   nr_qtd_dahsboard_w  number(10);
   ds_tabela_w         varchar2(4000);
   lista_tabela_w      lista_varchar_pck.tabela_varchar;
   ds_sql_1_w          varchar2(32767);
   ds_sql_2_w          varchar2(32767);
   ds_sql_3_w          varchar2(32767);
   ds_sql_4_w          varchar2(32767);
   qt_tamanho_sql_w    number(10);

begin

   if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
   
      if updating then
         --Se possuir dependencias, nao pode alterar o ds_sql
         select count(a.nr_sequencia)
           into nr_qtd_dahsboard_w
           from dar_dashboard a, dar_app b, dar_app_datamodels c
          where a.nr_seq_app = b.nr_sequencia
            and c.nr_seq_app = b.nr_sequencia
            and c.nr_seq_table_control = :new.nr_sequencia;
            
        if (nr_qtd_dahsboard_w > 0 ) then
          wheb_mensagem_pck.exibir_mensagem_abort(1150676);
        end if;
      end if;
   
      delete from dar_consist_sql
       where nr_seq_tab_controle = :new.nr_sequencia;
   
      sql_stmt_w := :new.ds_sql;
   
      if (nvl(:new.IE_MANUAL, 'N') = 'S') then

         :new.ie_inconsistencia := 'N';

         -- Tamanho em caracteres do sql gerado
         qt_tamanho_sql_w := nvl(dbms_lob.getlength(sql_stmt_w), 0);
      
         -- Primeira parte da string
         ds_sql_1_w := dbms_lob.substr(sql_stmt_w, 32767, 1);
      
         -- Segunda parte da string
         if (qt_tamanho_sql_w > 32767) then
            --
            ds_sql_2_w := dbms_lob.substr(sql_stmt_w, 32767, 32768);
         end if;
         -- Terceira parte da string
         if (qt_tamanho_sql_w > 65534) then
            -- 
            ds_sql_3_w := dbms_lob.substr(sql_stmt_w, 32767, 65535);
         end if;
         -- Quarta parte da string
         if (qt_tamanho_sql_w > 98302) then
            --
            ds_sql_4_w := dbms_lob.substr(sql_stmt_w, 32767, 98303);
         end if;
      
         -- Bloco de exception
         begin
            -- execucao do script de criacao da tabela
            EXECUTE IMMEDIATE ds_sql_1_w || ds_sql_2_w || ds_sql_3_w ||
                              ds_sql_4_w;
         exception
            when others then
               -- Sql invalido 
               :new.ie_inconsistencia := 'S';
            
               select obter_desc_expressao(1032844)
                 into message_w
                 from dual;
            
               message_aux_w := message_w || '  ';
            
         end;
      
         nr_posicao_espaco_w    := instr(sql_stmt_w, '(');
      
         if (nr_posicao_espaco_w = 0) then
            nr_posicao_espaco_w := instr(sql_stmt_w, ')');
         end if;
      
         if (nr_posicao_espaco_w > 0) then
            -- Function ou procedure
         
            :new.ie_inconsistencia := 'S';
         
            select obter_desc_expressao(1042220) into message_w from dual;
         
            message_aux_w := message_aux_w || message_w || '  ';
         
         end if;
      
         nr_posicao_espaco_w := instr(sql_stmt_w, '*');
         if (nr_posicao_espaco_w > 0) then
            -- Uso de Asterisco
            :new.ie_inconsistencia := 'S';
         
            select obter_desc_expressao(1042224) into message_w from dual;
         
            message_aux_w := message_aux_w || message_w || '  ';
         
         end if;
      
         select instr(upper(:new.ds_sql), 'FROM'),
                instr(upper(:new.ds_sql), 'WHERE'),
                length(:new.ds_sql)
           into nr_posicao_from_w, nr_posicao_where_w, nr_tamanho_string_w
           from dual;
      
         if (nr_posicao_where_w = 0) then
            select trim(substr(:new.ds_sql,
                               nr_posicao_from_w + 4,
                               nr_tamanho_string_w - (nr_posicao_from_w)))
              into ds_tabela_w
              from dual;
         else
            select trim(substr(:new.ds_sql,
                               nr_posicao_from_w + 4,
                               nr_posicao_where_w - (nr_posicao_from_w + 5)))
              into ds_tabela_w
              from dual;
         end if;
      
         if (instr(ds_tabela_w, ',') > 0) then
         
            lista_tabela_w := obter_lista_string2(ds_tabela_w, ',');
         
            for i in 1 .. lista_tabela_w.last loop
               ds_tabela_w := trim(replace(trim(lista_tabela_w(i)),
                                           chr(10),
                                           ' '));
               if (instr(upper(ds_tabela_w), '_V') = 0) then
                  -- Uso de tabela sem acesso por view
                  :new.ie_inconsistencia := 'S';
               
                  select obter_desc_expressao(1042222)
                    into message_w
                    from dual;
               
                  message_aux_w := message_aux_w || message_w || '  ';
               
               end if;
            end loop;
         else
            if (instr(upper(ds_tabela_w), '_V') = 0) then
               -- Uso de tabela sem acesso por view
               :new.ie_inconsistencia := 'S';
            
               select obter_desc_expressao(1042222)
                 into message_w
                 from dual;
            
               message_aux_w := message_aux_w || message_w || '  ';
            end if;
         end if;
      
         if (:new.ie_inconsistencia = 'S') then
         
            insert into dar_consist_sql
               (nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                nr_seq_tab_controle,
                ds_consistencia,
                nr_seq_sql)
            values
               (dar_consist_sql_seq.nextval,
                sysdate,
                wheb_usuario_pck.get_nm_usuario,
                :new.nr_sequencia,
                message_aux_w,
                :new.nr_seq_sql);
         end if;
      end if;
   end if;
end dar_tables_control_before;
/
