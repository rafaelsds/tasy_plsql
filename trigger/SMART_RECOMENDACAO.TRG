CREATE OR REPLACE TRIGGER SMART_RECOMENDACAO
after update on PRESCR_RECOMENDACAO
FOR EACH ROW

declare
	reg_integracao_w		gerar_int_padrao.reg_integracao;

BEGIN
begin
	if 	(:old.DT_SUSPENSAO is null) and
		(:new.DT_SUSPENSAO is not null) then 
		begin
			gerar_int_padrao.gravar_integracao('300', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';NR_SEQUENCIA=' || :new.NR_SEQUENCIA|| ';DS_OPERACAO=UPDATE');
		END;	
	end if;
exception when others then
	null;
end;

END;
/
