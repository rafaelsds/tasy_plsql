create or replace trigger saldo_estoque_log
before update or insert or delete on saldo_estoque 
for each row

begin

if	(inserting) then
	begin
	insert into saldo_estoque_log (
		CD_ESTABELECIMENTO,       
		CD_LOCAL_ESTOQUE,         
		CD_MATERIAL,              
		DT_MESANO_REFERENCIA,     
		QT_ESTOQUE,               
		VL_ESTOQUE,               
		QT_RESERVADA_REQUISICAO,  
		QT_RESERVADA,             
		DT_ATUALIZACAO,           
		NM_USUARIO,               
		VL_CUSTO_MEDIO,           
		VL_PRECO_ULT_COMPRA,      
		DT_ULT_COMPRA,            
		IE_STATUS_VALORIZACAO,    
		IE_BLOQUEIO_INVENTARIO,   
		QT_CONSUMO,               
		QT_ESTOQUE_MEDIO,         
		IE_ACAO,              
		DS_STACK,                 
		QT_OCOR_INVENT,           
		QT_OCOR_INVENT_OK,        
		QT_INVENTARIO) 
	values (
		:new.CD_ESTABELECIMENTO,       
		:new.CD_LOCAL_ESTOQUE,         
		:new.CD_MATERIAL,              
		:new.DT_MESANO_REFERENCIA,     
		:new.QT_ESTOQUE,               
		:new.VL_ESTOQUE,               
		:new.QT_RESERVADA_REQUISICAO,  
		:new.QT_RESERVADA,             
		:new.DT_ATUALIZACAO,          
		:new.NM_USUARIO,               
		:new.VL_CUSTO_MEDIO,           
		:new.VL_PRECO_ULT_COMPRA,      
		:new.DT_ULT_COMPRA,            
		:new.IE_STATUS_VALORIZACAO,    
		:new.IE_BLOQUEIO_INVENTARIO,   
		:new.QT_CONSUMO,               
		:new.QT_ESTOQUE_MEDIO,         
		'I',              
		 substr(dbms_utility.format_call_stack,1,4000),                
		:new.QT_OCOR_INVENT,           
		:new.QT_OCOR_INVENT_OK,        
		:new.QT_INVENTARIO);
	
	end;
elsif	(updating) then
	begin
	
	insert into saldo_estoque_log (
		CD_ESTABELECIMENTO,       
		CD_LOCAL_ESTOQUE,         
		CD_MATERIAL,              
		DT_MESANO_REFERENCIA,     
		QT_ESTOQUE,               
		VL_ESTOQUE,               
		QT_RESERVADA_REQUISICAO,  
		QT_RESERVADA,             
		DT_ATUALIZACAO,           
		NM_USUARIO,               
		VL_CUSTO_MEDIO,           
		VL_PRECO_ULT_COMPRA,      
		DT_ULT_COMPRA,            
		IE_STATUS_VALORIZACAO,    
		IE_BLOQUEIO_INVENTARIO,   
		QT_CONSUMO,               
		QT_ESTOQUE_MEDIO,         
		IE_ACAO,              
		DS_STACK,                 
		QT_OCOR_INVENT,           
		QT_OCOR_INVENT_OK,        
		QT_INVENTARIO)
	values (
		:old.CD_ESTABELECIMENTO,       
		:old.CD_LOCAL_ESTOQUE,         
		:old.CD_MATERIAL,              
		:old.DT_MESANO_REFERENCIA,     
		:old.QT_ESTOQUE,               
		:old.VL_ESTOQUE,               
		:old.QT_RESERVADA_REQUISICAO,  
		:old.QT_RESERVADA,             
		sysdate,     
		:old.NM_USUARIO,               
		:old.VL_CUSTO_MEDIO,           
		:old.VL_PRECO_ULT_COMPRA,      
		:old.DT_ULT_COMPRA,            
		:old.IE_STATUS_VALORIZACAO,    
		:old.IE_BLOQUEIO_INVENTARIO,   
		:old.QT_CONSUMO,               
		:old.QT_ESTOQUE_MEDIO,         
		'U',              
		 substr(dbms_utility.format_call_stack,1,4000),                
		:old.QT_OCOR_INVENT,           
		:old.QT_OCOR_INVENT_OK,        
		:old.QT_INVENTARIO);
	
	end;
else
	begin
	
	insert into saldo_estoque_log (
		CD_ESTABELECIMENTO,       
		CD_LOCAL_ESTOQUE,         
		CD_MATERIAL,              
		DT_MESANO_REFERENCIA,     
		QT_ESTOQUE,               
		VL_ESTOQUE,               
		QT_RESERVADA_REQUISICAO,  
		QT_RESERVADA,             
		DT_ATUALIZACAO,           
		NM_USUARIO,               
		VL_CUSTO_MEDIO,           
		VL_PRECO_ULT_COMPRA,      
		DT_ULT_COMPRA,            
		IE_STATUS_VALORIZACAO,    
		IE_BLOQUEIO_INVENTARIO,   
		QT_CONSUMO,               
		QT_ESTOQUE_MEDIO,         
		IE_ACAO,              
		DS_STACK,                 
		QT_OCOR_INVENT,           
		QT_OCOR_INVENT_OK,        
		QT_INVENTARIO)
	values (
		:old.CD_ESTABELECIMENTO,       
		:old.CD_LOCAL_ESTOQUE,         
		:old.CD_MATERIAL,              
		:old.DT_MESANO_REFERENCIA,     
		:old.QT_ESTOQUE,               
		:old.VL_ESTOQUE,               
		:old.QT_RESERVADA_REQUISICAO,  
		:old.QT_RESERVADA,             
		:old.DT_ATUALIZACAO,          
		:old.NM_USUARIO,               
		:old.VL_CUSTO_MEDIO,           
		:old.VL_PRECO_ULT_COMPRA,      
		:old.DT_ULT_COMPRA,            
		:old.IE_STATUS_VALORIZACAO,    
		:old.IE_BLOQUEIO_INVENTARIO,   
		:old.QT_CONSUMO,               
		:old.QT_ESTOQUE_MEDIO,         
		'D',              
		 substr(dbms_utility.format_call_stack,1,4000),                
		:old.QT_OCOR_INVENT,           
		:old.QT_OCOR_INVENT_OK,        
		:old.QT_INVENTARIO);
	
	end;
end if;

end;
/