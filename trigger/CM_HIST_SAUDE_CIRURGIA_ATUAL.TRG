create or replace trigger cm_hist_saude_cirurgia_atual
before insert or update on historico_saude_cirurgia
for each row 

declare
cd_expression	dic_objeto.nr_sequencia%type := 0;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	if 	(:new.dt_inicio is not null) and
		(pkg_date_utils.start_of(:new.dt_inicio, 'DAY') > sysdate) then
		begin
		cd_expression := 716755;
		end;
	elsif 	(:new.dt_fim is not null) then
		begin
		if (pkg_date_utils.start_of(:new.dt_fim, 'DAY') > sysdate) then
			begin
			cd_expression := 189691;
			end;
		elsif (:new.dt_inicio is not null) and
			(pkg_date_utils.start_of(:new.dt_inicio, 'DAY') > pkg_date_utils.start_of(:new.dt_fim, 'DAY')) then
			begin
			cd_expression := 21124;
			end;
		end if;
		end;
	end if;
	
	if 	(:new.dt_cirurgia is not null) and
		(pkg_date_utils.start_of(:new.dt_cirurgia, 'DAY') > sysdate) then
		begin
		cd_expression := 1046795;
		end;
	end if;
	
	if	(cd_expression > 0) then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(cd_expression);
		end;
	end if;
end if;
end;
/