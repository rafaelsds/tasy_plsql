CREATE OR REPLACE TRIGGER PFCS_AFTER_UPD_ORG_ADDRESS
AFTER UPDATE ON PFCS_ADDRESS
FOR EACH ROW

DECLARE
cd_cgc_w    pessoa_juridica.cd_cgc%type;

BEGIN

    if (:new.nr_seq_organization is not null) then
        select max(cd_cgc)
        into cd_cgc_w
        from estabelecimento
        where cd_estabelecimento = (
            select aux.cd_estabelecimento
            from pfcs_organization aux
            where aux.nr_sequencia = :new.nr_seq_organization
            and aux.nr_seq_organization_sup is not null
        );

        if (cd_cgc_w is not null) then
            update pessoa_juridica
            set ds_endereco = nvl(:new.addr_line, ' '),
                cd_cep = nvl(:new.postal_code, ' '),
                ds_municipio = nvl(:new.city, ' '),
                dt_atualizacao = SYSDATE
            where cd_cgc = cd_cgc_w;
        end if;
    end if;

END;
/