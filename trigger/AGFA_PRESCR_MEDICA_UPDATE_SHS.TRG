create or replace
TRIGGER TASY.AGFA_Prescr_Medica_Update                                                  
before update ON TASY.PRESCR_MEDICA for each row                                        
declare                                                                                 

ie_tipo_atendimento_w	varchar2(2);                                                      

nr_sequencia_w		number(10);                                                             
nr_seq_proc_interno_w	number(10);                                                       
dt_prev_execucao_w	date;                                                                
ie_urgencia_w		varchar2(1);                                                             
nm_usuario_w		varchar2(15);                                                             
ds_obs_prescr_w		varchar2(255);                                                         
ds_dado_clinico_w	varchar2(255);                                                        
ds_proc_exame_w		varchar2(100);                                                         
ie_lado_w		varchar2(1);                                                                 

ie_retorno_w		Varchar2(256);                                                            
cd_setor_w		number(5);                                                                  
ie_autoriz_w		varchar2(20);                                                             
nr_prescricao_w		number(14);                                                            
--ie_retorno_w		Boolean;                                                                

qt_reg_agfa_w		number(3);                                                               
qt_agendamento_w	number(1);                                                             

cursor c01 is                                                                           
	select	a.nr_sequencia,                                                                 
			a.nr_seq_proc_interno,                                                               
			nvl(a.dt_prev_execucao, :new.dt_prescricao),                                         
			decode(a.ie_urgencia, 'S', 'A', 'N'),                                                
			a.nm_usuario,                                                                        
			substr(a.ds_observacao,1,254),                                                       
			substr(a.ds_dado_clinico,1,254),                                                     
			b.ds_proc_exame,                                                                     
			decode(a.ie_lado,'N','X',decode(nvl(a.ie_lado,'X'), 'A', 'B', nvl(a.ie_lado,'X'))),  
			a.cd_setor_atendimento,                                                              
			decode(nvl(a.ie_autorizacao,'X'),'A','1^Authorized',                                 
							 'L','1^Authorized',                                                             
							 'X','1^Authorized',                                                             
							 'PA','4^Waiting',                                                               
							 '2^Unauthorized'),                                                              
		a.nr_prescricao                                                                       
			--decode(a.ie_lado, 'A', 'X', a.ie_lado)                                             
	from	proc_interno b,                                                                   
		prescr_procedimento a                                                                
	where	a.nr_seq_proc_interno = b.nr_sequencia                                           
	  and	a.nr_prescricao = :new.nr_prescricao                                             
	  and	a.nr_seq_agenda is null                                                          
	  and	a.nr_Seq_Agenda_cons is null                                                     
	  and	a.dt_suspensao is null
	  and	nvl(b.nr_seq_classif,0) = 5;                                                     

begin                                                                                   


if	(:old.dt_liberacao is null) and                                                      
	(:new.dt_liberacao is not null) then                                                   

	qt_reg_agfa_w	:= 0;                                                                    
	if	(:new.nr_controle is not null) then                                                 
		select	count(*)                                                                       
		into	qt_reg_agfa_w                                                                    
		from	agfa_dft_v                                                                       
		where	nr_seq_req_agfa = :new.nr_controle;                                             
	end if;                                                                                

	if	(qt_reg_agfa_w = 0) then                                                            
		select	decode(max(nvl(ie_tipo_atendimento,1)), 1, 'H1', 3, 'A2', 8, 'AA', 'AE') /*Bruna Os104943*/                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
		into	ie_tipo_atendimento_w                                                            
		from	atendimento_paciente                                                             
		where	nr_atendimento = :new.nr_atendimento;                                           

		if	((ie_tipo_Atendimento_w <> 'AE') or (:new.nr_seq_externo is null)) then            
			open c01;                                                                            
			loop                                                                                 
			fetch c01 into	nr_sequencia_w,                                                       
					nr_seq_proc_interno_w,                                                             
					dt_prev_execucao_w,                                                                
					ie_urgencia_w,                                                                     
					nm_usuario_w,                                                                      
					ds_obs_prescr_w,                                                                   
					ds_dado_clinico_w,                                                                 
					ds_proc_exame_w,                                                                   
					ie_lado_w,                                                                         
					cd_setor_w,                                                                        
					ie_autoriz_w,                                                                      
					nr_prescricao_w;                                                                   
				exit when c01%notfound;                                                             

				begin                                                                               
				select	1                                                                            
				into	qt_agendamento_w                                                               
				from	prescr_procedimento a,                                                         
					agenda_paciente b,                                                                 
					agenda_integrada_item c                                                            
				where	a.nr_seq_agenda	= b.nr_sequencia                                              
				and	b.nr_sequencia	= c.nr_seq_agenda_exame                                          
				and	a.nr_prescricao	= nr_prescricao_w                                               
				and	rownum 		= 1;                                                                   
				exception                                                                           
					when no_Data_found then                                                            
						begin                                                                             
						select	1                                                                          
						into	qt_agendamento_w                                                             
						from	prescr_procedimento a,                                                       
							agenda_consulta b,                                                               
							agenda_integrada_item c                                                          
						where	a.nr_seq_agenda_cons	= b.nr_sequencia                                       
						and	b.nr_sequencia	= c.nr_seq_agenda_cons                                         
						and	a.nr_prescricao	= nr_prescricao_w                                             
						and	rownum 		= 1;                                                                 
						exception                                                                         
							when others then                                                                 
								qt_agendamento_w	:= 0;                                                          
						end;                                                                              
				end;                                                                                

				insert into log_tasy values (sysdate, 'Debug', 11111,                               
							substr('AGFA_Gera_Solicitacao(	' || :new.nr_prescricao || ',0^REQUESTED,'||      
							ie_urgencia_w ||',' || dt_prev_execucao_w ||','|| dt_prev_execucao_w ||','||     
							nr_seq_proc_interno_w ||','|| nm_usuario_w ||','|| :new.cd_medico ||','||        
							:new.cd_pessoa_fisica ||','|| :new.nr_atendimento ||','||                        
							ie_tipo_atendimento_w ||','|| nr_seq_proc_interno_w ||','||                      
							ds_proc_exame_w ||','||	nr_sequencia_w ||','|| :new.ds_observacao ||','||        
							ds_obs_prescr_w ||','|| ie_autoriz_w ||','|| ie_retorno_w ||')',1,2000));        


				if	(qt_agendamento_w = 0) then                                                      
					AGFA_Gera_Solicitacao(	to_char(:new.nr_prescricao),                                
										'0^REQUESTED',                                                                
										ie_urgencia_w,                                                                
										dt_prev_execucao_w,                                                           
										dt_prev_execucao_w,                                                           
										'IMGHS',                                                                      
										nm_usuario_w,                                                                 
										:new.cd_medico,                                                               
										:new.cd_pessoa_fisica,                                                        
										to_char(:new.nr_atendimento),                                                 
										ie_tipo_atendimento_w,                                                        
										to_char(nr_seq_proc_interno_w) || ie_lado_w,                                  
										ds_proc_exame_w,                                                              
										to_char(nr_sequencia_w),                                                      
										substr(:new.ds_observacao,1,500),                                             
										ds_dado_clinico_w || chr(10) ||                                               
										ds_obs_prescr_w	  || chr(10) ||                                               
										'Data prev. Execu��o:' || to_char(dt_prev_execucao_w,'dd/mm/yyyy hh24:mi:ss'),
										cd_setor_w,                                                                   
										'Peso^' || to_char(:new.qt_peso),                                             
										ie_autoriz_w, /*autoriza��o*/                                                 
										ie_retorno_w);                                                                

					if	(ie_retorno_w is not null) then                                                 
						raise_application_error(-20011, 'Retorno AGFA:' || ie_retorno_w);                 
					end if;                                                                            
				end if;                                                                             
			end loop;                                                                            
			close c01;                                                                           
		end if;                                                                               
	end if;                                                                                
end if;                                                                                 

end AGFA_Prescr_Medica_Update; 
/
