CREATE OR REPLACE TRIGGER Convenio_Retorno_Glosa_Update
BEFORE Update ON Convenio_Retorno_Glosa
FOR EACH ROW

declare

qt_reg_w	number(1);
ie_varios_itens_glosados_w	varchar2(255);
ie_acao_glosa_novo_w	varchar2(255);
cont_w		number(15);
ds_observacao_w		varchar2(255) := null;

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

ATUALIZAR_CONVENIO_RET_GLOSA(	:new.nr_seq_ret_item, 
				:old.cd_motivo_glosa,
				:new.cd_motivo_glosa,
				:old.vl_glosa,
				:new.vl_glosa,
				:old.vl_amaior,
				:new.vl_amaior,
				:new.nm_usuario,
				:old.ie_acao_glosa,
				:new.ie_acao_glosa,
				ds_observacao_w);
				
if	(ds_observacao_w is not nulL) then

	if	(:new.ds_observacao_interna is not null) then

		:new.ds_observacao_interna := :new.ds_observacao_interna ||chr(13)|| ds_observacao_w;
	else
		:new.ds_observacao_interna :=  ds_observacao_w;
	end if;
	
end if;				

<<Final>>
qt_reg_w	:= 0;


END;
/
