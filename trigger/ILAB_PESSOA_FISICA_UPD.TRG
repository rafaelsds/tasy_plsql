create or replace trigger ilab_pessoa_fisica_upd
after update on pessoa_fisica
for each row

declare
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then	
	ilab_enviar_pessoa_conv(:new.cd_pessoa_fisica,:new.nm_usuario);
end if;
end;
/