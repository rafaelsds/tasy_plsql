create or replace trigger escala_gold_atual
before insert or update on escala_gold
for each row
declare
  qt_reg_w    number(1);
  sql_w       varchar2(200);
  ie_grupo_w  varchar2(1);
  ds_erro_w      varchar2(4000);
  ds_parametro_w varchar2(4000);
begin

  if (wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
  end if;

  /** Medical Device **/
  sql_w := 'begin OBTER_SCORE_ESCALA_GOLD_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13); end;';
  begin
	  execute immediate sql_w using in :new.qt_irritado,
									in :new.ie_irritado_adm,
									in :new.QT_CAT_01,
									in :new.QT_CAT_02,
									in :new.QT_CAT_03,
									in :new.QT_CAT_04,
                  in :new.QT_CAT_05,
                  in :new.QT_CAT_06,
                  in :new.QT_CAT_07,
                  in :new.QT_CAT_08,
                  in :new.ie_mmrc,
                  out :new.QT_TOTAL_CAT,
                  out ie_grupo_w;
  exception
    when others then
    ie_grupo_w := null;
    ds_erro_w := sqlerrm;
    ds_parametro_w := ':new.nr_atendimento: ' || :new.nr_atendimento || ' - :new.ie_situacao: ' || :new.ie_situacao || ' - :new.cd_profissional: ' || :new.cd_profissional;
    ds_parametro_w := ds_parametro_w || ' - :new.qt_irritado: ' || :new.qt_irritado || ' - :new.ie_irritado_adm: ' || :new.ie_irritado_adm || ' - :new.QT_CAT_01: ' || :new.QT_CAT_01;
    ds_parametro_w := ds_parametro_w || ' - :new.QT_CAT_02: ' || :new.QT_CAT_02 || ' - :new.QT_CAT_03: ' || :new.QT_CAT_03 || ' - :new.QT_CAT_04: ' || :new.QT_CAT_04 || ' - :new.QT_CAT_05: ' || :new.QT_CAT_05;
    ds_parametro_w := ds_parametro_w || ' - :new.QT_CAT_06: ' || :new.QT_CAT_06 || ' - :new.QT_CAT_07: ' || :new.QT_CAT_07 || ' - :new.QT_CAT_08: ' || :new.QT_CAT_08 || ' - :new.ie_mmrc: ' || :new.ie_mmrc || ' - :new.QT_TOTAL_CAT: ' || :new.QT_TOTAL_CAT || ' - ie_grupo_w: ' || ie_grupo_w;
    gravar_log_medical_device('ESCALA_GOLD_ATUAL', 'OBTER_SCORE_ESCALA_GOLD_MD', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');
  end;

  <<Final>>
  qt_reg_w := 0;

  :new.ie_grupo := ie_grupo_w;

end Escala_gold_atual;
/
