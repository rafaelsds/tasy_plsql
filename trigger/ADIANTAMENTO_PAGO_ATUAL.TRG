create or replace trigger adiantamento_pago_atual
before insert or update on adiantamento_pago
for each row
declare

ie_deduzir_ordem_adiant_w	varchar2(1);
ie_situacao_w varchar2(1);
dt_baixa_w		date;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:new.cd_pessoa_fisica is null) and
		(:new.cd_cgc is null) then		
		wheb_mensagem_pck.exibir_mensagem_abort(226813);
	end if;


if	(:new.cd_cgc is not null) and (inserting) then    
	select	nvl(max(ie_situacao),'A')
	into	ie_situacao_w
	from	pessoa_juridica
	where	cd_cgc	= :new.cd_cgc;	
	if	(ie_situacao_w = 'I') then    
        wheb_mensagem_pck.exibir_mensagem_abort(1173628);    
    end if;    
end if;

if	(:new.vl_saldo = 0) and (:new.dt_baixa is null) then
	begin
	select	nvl(max(ie_deduzir_ordem_adiant),'S')
	into	ie_deduzir_ordem_adiant_w
	from	parametros_contas_pagar
	where	cd_estabelecimento	= :new.cd_estabelecimento;
	
	if	(ie_deduzir_ordem_adiant_w = 'N') then
		select	nvl(max(a.dt_contabil),sysdate)
		into	dt_baixa_w
		from	titulo_pagar b,
			titulo_pagar_adiant a
		where	a.nr_adiantamento	= :new.nr_adiantamento
		and	b.nr_titulo		= a.nr_titulo;
	else
		select	nvl(max(a.dt_contabil),sysdate)
		into	dt_baixa_w
		from	titulo_pagar b,
			titulo_pagar_adiant a
		where	a.nr_adiantamento	= :new.nr_adiantamento
		and	b.nr_titulo		= a.nr_titulo
		and	not exists(	select	1
				from	nota_fiscal_item y,
					ordem_compra_adiant_pago x
				where	x.nr_adiantamento	= a.nr_adiantamento
				and	x.nr_ordem_compra	= y.nr_ordem_compra
				and	y.nr_sequencia	= b.nr_seq_nota_fiscal);
	end if;
	
	:new.dt_baixa	:= dt_baixa_w;
	end;
end if;

if (updating) then
	if (:new.dt_baixa <> :old.dt_baixa)  then
		/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_baixa);    
	end if;
else 	if (inserting) then
		/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_baixa);    
	end if;
end if;

end if;

end;
/
