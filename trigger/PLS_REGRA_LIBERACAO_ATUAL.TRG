CREATE OR REPLACE TRIGGER PLS_REGRA_LIBERACAO_ATUAL
BEFORE INSERT OR UPDATE ON PLS_REGRA_LIBERACAO
FOR EACH ROW

declare

cd_material_w	number(6);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:new.nr_seq_material is not null) then
		select	cd_material
		into	cd_material_w
		from	pls_material a
		where	a.nr_sequencia	= :new.nr_seq_material;

		:new.cd_material	:= cd_material_w;
	end if;
end if;

END;
/