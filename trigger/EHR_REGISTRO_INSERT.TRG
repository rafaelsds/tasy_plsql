create or replace
trigger ehr_registro_insert
before insert on ehr_registro
for each row

declare

begin
if	(:new.cd_perfil_ativo is null) and
	(obter_perfil_ativo 	> 0)then
	:new.cd_perfil_ativo	:= obter_perfil_ativo;
end if;

:new.cd_unidade := nvl(Obter_Ult_Unid_Atendimento(:new.nr_atendimento),0);

end;
/