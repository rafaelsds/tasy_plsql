create or replace trigger lote_ent_secretaria_afterup
after update on LOTE_ENT_SECRETARIA
for each row

declare
nr_ultimo_lote_w	number(10);

begin

if 	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.ie_lote_interno <> 'S') then

	select	nvl(max(nr_ultimo_lote),0)
	into	nr_ultimo_lote_w
	from	lote_ent_inst_posto
	where	nr_seq_instituicao = :new.nr_seq_instituicao
	and		nr_sequencia = :new.nr_seq_posto;
	
	if (:new.nr_lote > 0) then
	
		update 	lote_ent_inst_posto
		set	nr_ultimo_lote = :new.nr_lote
		where	nr_seq_instituicao = :new.nr_seq_instituicao
		and	nr_sequencia = :new.nr_seq_posto;
	
	end if;

end if;

end;
/