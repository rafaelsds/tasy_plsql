create or replace trigger fa_receita_farmacia_update
before update on FA_RECEITA_FARMACIA
for each row

declare

cd_pessoa_fisica_w	varchar2(10);

begin

	if (:new.nr_atendimento is not null) then

		select 	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;
		
		if (cd_pessoa_fisica_w <> :new.cd_pessoa_fisica) then
			-- O atendimento '||:new.nr_atendimento||' n�o pertence a seguinte pessoa: '||obter_nome_pf(:new.cd_pessoa_fisica)||'. Favor verificar#@#@'
			Wheb_mensagem_pck.exibir_mensagem_abort(263626,	'NR_ATENDIMENTO_W='||TO_CHAR(:NEW.NR_ATENDIMENTO)|| ';' ||
															'CD_PESSOA_FISICA_W= '|| obter_nome_pf(:NEW.CD_PESSOA_FISICA));
		end if;
	end if;

end;
/