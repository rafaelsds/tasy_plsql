CREATE OR REPLACE TRIGGER qua_doc_revisao_atual 
BEFORE UPDATE ON qua_doc_revisao
FOR EACH ROW 
DECLARE

nm_user_w			varchar2(50);
nr_seq_ordem_w			man_ordem_servico.nr_sequencia%type;
nr_seq_doc_pai_w		qua_documento.nr_sequencia%type;
cd_documento_w			qua_documento.cd_documento%type;
nm_documento_w			qua_documento.nm_documento%type;
nr_seq_classif_w		qua_documento.nr_seq_classif%type;
nr_seq_tipo_w			qua_documento.nr_seq_tipo%type;

nr_seq_documento_w		qms_treinamento.nr_seq_documento%type;
cd_pessoa_fisica_w		qms_treinamento.cd_pessoa_fisica%type;
cd_pessoa_resp_w		qms_treinamento.cd_responsavel%type;
cd_gerente_w			qms_treinamento.cd_gerente_pessoa%type;
cd_revisao_w			qua_doc_revisao.cd_revisao%type;
nr_seq_cargo_w			qms_treinamento_cargo.nr_seq_cargo%type;
nr_seq_estagio_w		man_ordem_servico.nr_seq_estagio%type;
qt_existe_pendencias_w	number(10);
qt_existe_nova_rev_w	number(10);


Cursor	c01 is
select	mos.nr_sequencia
from	man_ordem_servico mos
where	mos.nr_seq_documento = :new.nr_seq_doc
and	mos.ie_tipo_ordem = 12
and	mos.ie_status_ordem <> '3'
and	not exists	(
				select	1
				from	qua_doc_arquivo qda
				where	qda.nr_seq_doc =	(
									select	min(doc.nr_seq_documento)
									from	(
											select	qd.nr_sequencia nr_seq_documento
											from	qua_documento qd
											where	qd.nr_sequencia = :new.nr_seq_doc
											and	qd.nr_seq_superior is null
											union	
											select	qd.nr_seq_superior nr_seq_documento
											from	qua_documento qd
											where	nr_seq_superior is not null
											and	nr_sequencia = :new.nr_seq_doc
										) doc
								)
			);

begin	

select	username
into	nm_user_w
from	user_users;

if	(nm_user_w = 'CORP') and
	(:new.dt_aprovacao is not null) and 
	(:old.dt_aprovacao is null) then
	begin		
	
	select	cd_documento,
		substr(obter_desc_expressao(cd_exp_documento, nm_documento), 1, 255) nm_documento,
		nr_seq_classif,
		nr_seq_tipo
	into	cd_documento_w,
		nm_documento_w,
		nr_seq_classif_w,
		nr_seq_tipo_w
	from	qua_documento
	where	nr_sequencia	= :new.nr_seq_doc;
	
	open C01;
	loop
	fetch C01 into
		nr_seq_ordem_w;	
	exit when C01%notfound;
		begin
		
			if	(	(nr_seq_classif_w = 1)
				and	nr_seq_tipo_w in (2, 3, 5, 15)
				and	(obter_se_base_corp = 'S'
					or	obter_se_base_wheb = 'S')) then
				begin
					select	max(mes.nr_sequencia)
					into	nr_seq_estagio_w
					from	man_estagio_processo mes
					where	mes.nr_sequencia = 1871;
					
					if	(nr_seq_estagio_w is not null) then
						begin
							update	man_ordem_servico
							set	nr_seq_estagio = nr_seq_estagio_w
							where	nr_sequencia = nr_seq_ordem_w;
						end;
					end if;
				end;
			else
				begin
					man_encerrar_OS(nr_seq_ordem_w,9,:new.nm_usuario,substr(Wheb_mensagem_pck.get_texto(799312) || ' ' ||cd_documento_w||' - '||nm_documento_w,1,255),'N');
				end;
			end if;
		
		end;
	end loop;
	close C01;
	
	end;
end if;
END;
/
