create or replace trigger UC_ESCALA_TEV_UPDATE
after update on ESCALA_TEV
for each row

declare
ds_texto_w	varchar2(32000);
quebra_w	varchar2(255)  := wheb_rtf_pck.get_quebra_linha;
negrito_inicio varchar2(10) := wheb_rtf_pck.get_negrito(true);
negrito_final varchar2(10) := wheb_rtf_pck.get_negrito(false);
ds_inf_adic varchar2(255);
IE_TIPO_EVOLUCAO_w	varchar2(10);

pragma autonomous_transaction;

begin

IF (:OLD.DT_LIBERACAO IS NULL AND :NEW.DT_LIBERACAO IS NOT NULL) THEN

ds_texto_w := ds_texto_w || negrito_inicio || 'M�todo n�o se aplica: ' || negrito_final;
ds_texto_w := ds_texto_w || :new.IE_NAO_SE_APLICA || quebra_w;

if (:new.IE_NAO_SE_APLICA <> 'N') then
	select DS_MOTIVO
	into ds_inf_adic
	from TEV_MOTIVO
	where NR_SEQUENCIA = :new.NR_SEQ_MOTIVO_TEV;
	
	ds_texto_w := ds_texto_w || negrito_inicio || 'Motivo: ' || negrito_final;
	ds_texto_w := ds_texto_w || ds_inf_adic || quebra_w;
end if;

ds_texto_w := ds_texto_w || negrito_inicio || 'Peso (kg): ' || negrito_final;
ds_texto_w := ds_texto_w || :new.QT_PESO || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'Altura (cm): ' || negrito_final;
ds_texto_w := ds_texto_w || :new.QT_ALTURA_CM || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'IMC (Kg/m�): ' || negrito_final;
ds_texto_w := ds_texto_w || :new.QT_IMC || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'Paciente: ' || negrito_final;
ds_texto_w := ds_texto_w || Obter_Descricao_Dominio(3103,:new.IE_CLINICO_CIRURGICO ) || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'Idade (anos): ' || negrito_final;
ds_texto_w := ds_texto_w || :new.QT_IDADE || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'Clearance creatinina (ml/min): ' || negrito_final;
ds_texto_w := ds_texto_w || :new.QT_CLEARENCE || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'Mobilidade reduzida (mais de 50% do tempo de vig�lia restrito ao leito ou cadeira): ' || negrito_final;
ds_texto_w := ds_texto_w || :new.IE_MOBILIDADE_REDUZIDA || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'Reclassifica��o: ' || negrito_final;
ds_texto_w := ds_texto_w || Obter_Descricao_Dominio(2900,:new.IE_RISCO_RECLASSIF) || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'Itens selecionados: ' || negrito_final;
ds_texto_w := ds_texto_w || obter_selecao_tev(:new.nr_sequencia,:new.nm_usuario) || quebra_w;

ds_texto_w := ds_texto_w || negrito_inicio || 'Resultado: ' || negrito_final;
ds_texto_w := ds_texto_w || negrito_inicio || :new.DS_RESULTADO  || negrito_final || quebra_w;

if (:new.DS_OBSERVACAO  is not null) then 
ds_texto_w := ds_texto_w || negrito_inicio || 'Observa��o: ' || negrito_final;
ds_texto_w := ds_texto_w || :new.DS_OBSERVACAO || quebra_w;
end if;

ds_texto_w	:= replace(ds_texto_w,chr(13),' \par ');
ds_texto_w  := wheb_rtf_pck.get_cabecalho || ds_texto_w ||wheb_rtf_pck.get_rodape;

		select	max(IE_TIPO_EVOLUCAO)
		into	IE_TIPO_EVOLUCAO_w
		from	usuario
		where	nm_usuario = :new.nm_usuario;

		
insert into evolucao_paciente(	cd_evolucao,
				dt_evolucao,
				ie_tipo_evolucao,
				cd_pessoa_fisica,
				ie_situacao,
				dt_atualizacao,
				nm_usuario,
				nr_atendimento,
				ds_evolucao,					
				ie_evolucao_clinica,
				dt_liberacao,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_medico)
		values	(	evolucao_paciente_seq.nextval,
				sysdate,
				nvl(IE_TIPO_EVOLUCAO_w,1),
				obter_pessoa_atendimento(:new.nr_atendimento,'C'),
				'A',
				sysdate,
				:new.nm_usuario,
				:new.nr_atendimento,
				ds_texto_w,			
				'E',				
				sysdate,
				sysdate,
				:new.nm_usuario,
				Obter_Pessoa_Fisica_Usuario(:NEW.nm_usuario,'C'));

				commit;	

END IF;


end;
/
