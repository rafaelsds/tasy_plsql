create or replace trigger pls_plano_online_atual
before insert or update on pls_plano_online
for each row

declare

ie_tipo_operacao_grupo_w	pls_grupo_plano_online.ie_tipo_operacao%type;
ie_tipo_operacao_plano_w	pls_plano.ie_tipo_operacao%type;
qt_registro_w	pls_integer;
qt_nr_seq_grupo_plano_w		number(1);

begin
select	ie_tipo_operacao
into	ie_tipo_operacao_plano_w
from	pls_plano
where	nr_sequencia	= :new.nr_seq_plano;

select	ie_tipo_operacao
into	ie_tipo_operacao_grupo_w
from	pls_grupo_plano_online
where	nr_sequencia	= :new.nr_seq_grupo_plano;

if	(inserting) and
	(ie_tipo_operacao_grupo_w = 'A') then
	select	count(1)
	into	qt_nr_seq_grupo_plano_w
	from	pls_plano_online
	where	nr_seq_grupo_plano = :new.nr_seq_grupo_plano;
	
	if	(qt_nr_seq_grupo_plano_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1084171);
	end if;
end if;

if	(ie_tipo_operacao_grupo_w is not null) and
	(ie_tipo_operacao_plano_w is not null) and
	(ie_tipo_operacao_grupo_w <> ie_tipo_operacao_plano_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(1084154);
end if;

if	(updating) and
	(:new.nr_seq_plano <> :old.nr_seq_plano) then
	select	count(*)
	into	qt_registro_w
	from	pls_plano_tabela_online
	where	nr_seq_plano_online	= :new.nr_sequencia;
	
	if	(qt_registro_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1093684); --N�o � poss�vel alterar o produto, pois j� existe tabela de pre�o vinculada.
	end if;
end if;

select	count(*)
into	qt_registro_w
from	pls_plano
where	nr_sequencia	= :new.nr_seq_plano
and	ie_tipo_contratacao = 'I';

if	(qt_registro_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1093685); --Somente � permitida a inclus�o de produtos com tipo de contrata��o Individual/Familiar
end if;

end;
/