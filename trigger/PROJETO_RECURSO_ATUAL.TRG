CREATE OR REPLACE TRIGGER projeto_recurso_atual
AFTER INSERT OR UPDATE ON projeto_recurso
FOR EACH ROW
DECLARE

BEGIN

if	(:new.IE_TIPO_PROJETO = 'P') and
	(:new.NR_SEQ_SUPERIOR is not null) then	
	wheb_mensagem_pck.Exibir_Mensagem_Abort(249182);
end if;

if	(:new.IE_TIPO_PROJETO = 'S') and
	(:new.NR_SEQ_SUPERIOR is null) then	
	wheb_mensagem_pck.Exibir_Mensagem_Abort(249180);
end if;

END;
/
