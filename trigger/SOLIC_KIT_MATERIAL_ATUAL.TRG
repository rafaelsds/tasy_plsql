create or replace trigger solic_kit_material_atual
before insert or update on solic_kit_material
for each row

declare
ie_situacao_w	kit_material.ie_situacao%type;
begin
if	(inserting) or 
	((updating) and (:new.cd_kit_material <> :old.cd_kit_material)) then
	begin
	select	ie_situacao
	into	ie_situacao_w
	from	kit_material
	where	cd_kit_material = :new.cd_kit_material;
	exception
	when others then
		ie_situacao_w	:= 'A';
	end;
	
	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(271162);
	end if;
end if;
end;
/