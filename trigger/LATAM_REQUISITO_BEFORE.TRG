CREATE OR REPLACE TRIGGER latam_requisito_before
BEFORE INSERT OR UPDATE ON latam_requisito
FOR EACH ROW

DECLARE
  
qt_reg_w		number(10);
  
BEGIN
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
		goto Final;
	end if;
	
	if (nvl(:new.ie_sob_medida,'N') = 'N') then
		begin
			:new.qt_tempo_horas :=  nvl(calcular_esforco_requisito(:new.nr_sequencia,
									:new.nr_seq_gap,
									:new.nr_seq_prs,
									:new.ie_esforco_desenv,
									:new.ie_complexidade,
									:new.pr_confianca),0);
		end;
	end if;
	
	if (:new.ie_necessita_tecnologia = 'S') then
		begin
			:new.qt_horas_tec := nvl(calcular_esforco_area(:new.nr_sequencia,
								:new.nr_seq_gap,
								:new.nr_seq_prs,
								'TE',
								:new.ie_necessita_design,
								:new.ie_necessita_tecnologia,
								:new.qt_tempo_horas),0);
		end;
	end if;
	
	if (:new.ie_necessita_design = 'S') then
		begin
			:new.qt_horas_design := nvl(calcular_esforco_area(:new.nr_sequencia,
									:new.nr_seq_gap,
									:new.nr_seq_prs,
									'DE',
									:new.ie_necessita_design,
									:new.ie_necessita_tecnologia,
									:new.qt_tempo_horas),0);
		end;
	end if;
	
	<<Final>>
	qt_reg_w	:= 0;

end;
/