create or replace trigger agenda_quimio_delete 
before delete on agenda_quimio
for each row

declare

atrib_oldvalue_w		varchar2(255);
atrib_newvalue_w		varchar2(255);


begin

	atrib_oldvalue_w := substr(obter_valor_dominio(3192,:old.ie_status_agenda),1,255);
	atrib_newvalue_w := substr(obter_valor_dominio(3192,:new.ie_status_agenda),1,255);
	
	/*Controle de exclus�o para que n�o fique registros na tabela Agenda_Quimio_Marcacao*/
	if (:old.nr_seq_atendimento is not null) then
		delete agenda_quimio_marcacao where nr_seq_atendimento = :old.nr_seq_atendimento;
		
		delete w_agenda_quimio where nr_seq_atendimento = :old.nr_seq_atendimento;
	end if;
	
	if	(:old.nr_seq_pend_agenda is not null) then
		/*gravar log's de altera��es(ICESP)*/
		insert into agenda_quimio_log	(NR_SEQUENCIA,
						 DT_ATUALIZACAO,
						 NM_USUARIO,         
						 DT_ATUALIZACAO_NREC,
						 NM_USUARIO_NREC,   
						 DS_LOG,       
						 NR_SEQ_PEND_AGENDA,
						 DT_LOG,
						 DS_STACK)
					values
						(agenda_quimio_log_seq.nextval,
						sysdate,
						:old.nm_usuario,
						sysdate,
						:old.nm_usuario,
						substr(wheb_mensagem_pck.get_texto(800192,
										'ATRIB_OLDVALUE='||atrib_oldvalue_w||
										';ATRIB_NEWVALUE='||atrib_newvalue_w||
										';DS_MOTIVO_STATUS_OLD='||:old.ds_motivo_status||
										';CD_PESSOA_FISICA_OLD='||:old.cd_pessoa_fisica||
										';NR_SEQ_PEND_AGENDA_OLD='||:old.nr_seq_pend_agenda||
										';NR_SEQ_LOCAL_OLD='||:old.nr_seq_local||
										';DT_CANCELADA_OLD='||:old.dt_cancelada||
										';NR_SEQ_MOT_CANCELAMENTO_OLD='||:old.nr_seq_mot_cancelamento),1,4000),						
						:old.nr_seq_pend_agenda,
						sysdate,
						substr(wheb_mensagem_pck.get_texto(800207,
										'DS_FUNCAO_ATIVA='||obter_funcao_Ativa||
										';DS_PERFIL='||obter_perfil_ativo) || ' -  Stack - ' || dbms_utility.format_call_stack, 1,4000));
	end if;
end;
/
