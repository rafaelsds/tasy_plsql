create or replace trigger titulo_receber_classif_insert
after insert on titulo_receber_classif
for each row

declare

cd_estabelecimento_w            ctb_documento.cd_estabelecimento%type;
nr_seq_trans_financ_w           ctb_documento.nr_seq_trans_financ%type;
dt_movimento_w                  ctb_documento.dt_competencia%type; 
ie_contab_classif_tit_rec_w     parametro_contas_receber.ie_contab_classif_tit_rec%type;

cursor c01 is
        select  a.nm_atributo,
                a.cd_tipo_lote_contab
        from    atributo_contab a
        where   a.cd_tipo_lote_contab = 5
        and     a.nm_atributo in ('VL_TITULO_RECEBER');

c01_w           c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select  cd_estabelecimento,
        nr_seq_trans_fin_contab,
        dt_contabil
into    cd_estabelecimento_w,
        nr_seq_trans_financ_w,
        dt_movimento_w
from    titulo_receber
where   nr_titulo = :new.nr_titulo;


begin
select  nvl(ie_contab_classif_tit_rec, 'N')
into    ie_contab_classif_tit_rec_w
from    parametro_contas_receber
where   cd_estabelecimento = cd_estabelecimento_w;
exception when others then
        ie_contab_classif_tit_rec_w := 'N';
end;

begin
select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_classif_tit_rec, 'N'), ie_contab_classif_tit_rec_w)
into    ie_contab_classif_tit_rec_w
from    ctb_param_lote_contas_rec a
where   a.cd_empresa = obter_empresa_estab(cd_estabelecimento_w)
and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w;
exception when others then
        null;
end;

open c01;
loop
fetch c01 into  
        c01_w;
exit when c01%notfound;
        begin
        
        if      (nvl(:new.vl_classificacao, 0) <> 0 and ie_contab_classif_tit_rec_w = 'S') then
                begin
                
                ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
                                                                        trunc(dt_movimento_w),
                                                                        c01_w.cd_tipo_lote_contab,
                                                                        nr_seq_trans_financ_w,
                                                                        3,
                                                                        :new.nr_titulo,
                                                                        :new.nr_sequencia,
                                                                        0,
                                                                        :new.vl_classificacao,
                                                                        'TITULO_RECEBER_CLASSIF',
                                                                        c01_w.nm_atributo,
                                                                        :new.nm_usuario);

                end;
        end if;
        
        end;
end loop;
close c01;
end if;
end;
/