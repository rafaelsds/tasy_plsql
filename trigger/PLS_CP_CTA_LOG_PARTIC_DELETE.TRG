create or replace trigger pls_cp_cta_log_partic_delete
before delete on pls_cp_cta_log_partic
for each row

declare

begin

-- ao deletar um registro na tabela de log principal o mesmo � salvo em uma tabela de
-- hist�rico, desta forma na tabela principal guardamos apenas o ultimo recalculo feito
-- todos os demais ficam armazenados na tabela de hist�rico
insert into pls_cp_cta_log_partic_hist (
	nr_sequencia, ds_log, dt_atualizacao, 
	ie_destino_regra, nm_usuario, nr_seq_proc_partic, 
	dt_atualizacao_nrec, nm_usuario_nrec, vl_anestesista,
	vl_auxiliares, vl_medico, vl_negociado
) values (
	pls_cp_cta_log_partic_hist_seq.nextval, :old.ds_log, :old.dt_atualizacao,
	:old.ie_destino_regra, :old.nm_usuario, :old.nr_seq_proc_partic,
	sysdate, nvl(wheb_usuario_pck.get_nm_usuario, 'naoidentificado'), :old.vl_anestesista,
	:old.vl_auxiliares, :old.vl_medico, :old.vl_negociado
	);

end pls_cp_cta_log_partic_delete;
/