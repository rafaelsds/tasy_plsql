create or replace 
trigger mat_autor_cirur_before_delete
before delete on material_autor_cirurgia
for each row
declare

ie_permite_mat_especiais_w	Varchar2(1);
qt_registro_w			number(10);

begin

begin

obter_param_usuario(871, 333, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_permite_mat_especiais_w);

if	(ie_permite_mat_especiais_w = 'N') and
	(:old.ie_gerado_agenda = 'S') then
	select 	count(*)
	into	qt_registro_w
	from	autorizacao_cirurgia
	where	nr_seq_agenda is not null
	and	nr_sequencia 		= :old.nr_seq_autorizacao;

	if	(qt_registro_w > 0) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267483);
	end if;	
end if;	


exception
	when others then
	null;
end;

end;
/