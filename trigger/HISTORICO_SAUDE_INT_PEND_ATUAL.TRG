create or replace
trigger historico_saude_int_pend_atual
after insert or update on historico_saude_internacao
for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_liberar_hist_saude_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_liberar_hist_saude)
into	ie_liberar_hist_saude_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'HSI';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XHSI';
	end if;
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario);
	end if;
end if;
	
<<Final>>
qt_reg_w	:= 0;
end;
/