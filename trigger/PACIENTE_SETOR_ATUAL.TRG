create or replace
trigger PACIENTE_SETOR_ATUAL
before insert or update on PACIENTE_SETOR
for each row

declare
qt_tempo_medic_w	number(10);
nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(10);
nr_atendimento_W        number(10);
ie_cancelar_agend_w	varchar2(1);
nm_usuario_w		varchar2(15);

cursor C01 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	= 'IPO'
	and	nvl(cd_protocolo,nvl(:new.cd_protocolo,0)) = nvl(:new.cd_protocolo,0)
	and	nvl(nr_seq_protocolo,nvl(:new.nr_seq_medicacao,0)) = nvl(:new.nr_seq_medicacao,0)
	and	nvl(ie_situacao,'A') = 'A';

cursor C02 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	= 'APO'
	and	nvl(cd_protocolo,nvl(:new.cd_protocolo,0)) = nvl(:new.cd_protocolo,0)
	and	nvl(nr_seq_protocolo,nvl(:new.nr_seq_medicacao,0)) = nvl(:new.nr_seq_medicacao,0)
	and	nvl(ie_situacao,'A') = 'A';

cursor C03 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	= 'EPT'
	and	nvl(cd_protocolo,nvl(:new.cd_protocolo,0)) = nvl(:new.cd_protocolo,0)
	and	nvl(nr_seq_protocolo,nvl(:new.nr_seq_medicacao,0)) = nvl(:new.nr_seq_medicacao,0)
	and	nvl(ie_situacao,'A') = 'A';

begin
If	(:new.ie_status is not null) and
	(:old.ie_status <>  :new.ie_status) and
	(:new.ie_status <> 'I') and
	(:new.ie_status <> 'F') then
	Abortar_Se_Protoc_Inativo(:new.nr_seq_paciente);
end if;

if	(:old.ie_status is not null) and
	(:old.ie_status <>  :new.ie_status) and
	(:new.ie_status = 'F') then
	Abortar_se_dia_ciclo_ativo(:new.nr_seq_paciente,:new.cd_pessoa_fisica);
end if;

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(:old.DT_PROTOCOLO,sysdate+10) <> :new.DT_PROTOCOLO) and
	(:new.DT_PROTOCOLO is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_PROTOCOLO, 'HV');	
end if;

if	(nvl(:old.DT_LIB_AUTORIZADOR,sysdate+10) <> :new.DT_LIB_AUTORIZADOR) and
	(:new.DT_LIB_AUTORIZADOR is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIB_AUTORIZADOR,'HV');
end if;

if	(:new.nr_seq_medicacao is not null) and
	((:old.nr_seq_medicacao is null) or
	 (:new.nr_seq_medicacao <> :old.nr_seq_medicacao)) and
	(:new.qt_tempo_medic	is null )then
	begin
	select	max(qt_tempo_medic)
	into	:new.qt_tempo_medic
	from	protocolo_medicacao
	where	cd_protocolo	= :new.cd_protocolo
	and	nr_sequencia	= :new.nr_seq_medicacao;
	end;
end if;

nm_usuario_w :=wheb_usuario_pck.get_nm_usuario;
Obter_Param_Usuario(281,1255,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,ie_cancelar_agend_w );

if	(:old.ie_status is not null) and
	(:old.ie_status <>  :new.ie_status) and
	((:new.ie_status = 'I') or
	((:new.ie_status = 'F') and
	 (:old.ie_status <> 'I')))then
		if	(ie_cancelar_agend_w = 'S') or
			((:new.ie_status = 'I') and (ie_cancelar_agend_w = 'I')) or
			((:new.ie_status = 'F') and (ie_cancelar_agend_w = 'F')) then
				inativar_dia_tratamento_onco(:new.nr_seq_paciente, :new.nm_usuario);
		end if;
end if;

if	(inserting) then

	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_protocolo_onc( 	nr_seq_evento_w,
						:new.cd_pessoa_fisica,
						:new.cd_protocolo,
						:new.nr_seq_medicacao,
						:new.nm_usuario);
		end;
	end loop;
	close C01;
	exception
		when others then
		null;
	end;
end if;

if	(updating) then

	begin
	open C02;
	loop
	fetch C02 into
		nr_seq_evento_w;
	exit when C02%notfound;
		begin
		gerar_evento_protocolo_onc( 	nr_seq_evento_w,
						:new.cd_pessoa_fisica,
						:new.cd_protocolo,
						:new.nr_seq_medicacao,
						:new.nm_usuario);
		end;
	end loop;
	close C02;
	exception
		when others then
		null;
	end;

end if;

if	(inserting) then
	begin
	SELECT MAX(NR_ATENDIMENTO)
	INTO nr_atendimento_W
	FROM ATENDIMENTO_PACIENTE
	WHERE CD_PESSOA_FISICA = :NEW.cd_PESSOA_FISICA;
	open C03;
	loop
	fetch C03 into
		nr_seq_evento_w;
	exit when C03%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_w,nr_atendimento_W,:new.cd_pessoa_fisica,null,:new.nm_usuario,null,:new.DT_PROTOCOLO,NULL);
		end;
	end loop;
	close C03;
	end;

end if;

end;
/
