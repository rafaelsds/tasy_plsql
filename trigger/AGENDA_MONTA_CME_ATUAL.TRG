create or replace trigger agenda_monta_cme_atual
before insert or update on agenda_monta_cme
for each row

declare
ds_alteracao_w		varchar2(4000) 	:= null;
ds_equipamento_w	varchar2(255) 	:= null;

begin

if	(updating) then
	if	(nvl(:old.nr_seq_classificacao,0) <> nvl(:new.nr_seq_classificacao,0)) then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791491,
							'DS_CLASSIF_OLD='||substr(cme_obter_desc_classif_conj(:old.nr_seq_classificacao),1,125)||
							';DS_CLASSIF_NEW='||substr(cme_obter_desc_classif_conj(:new.nr_seq_classificacao),1,125)),1,4000);					
					
	end if;
	
	if	(nvl(:old.ds_observacao,'X') <> nvl(:new.ds_observacao,'X')) then
		ds_alteracao_w	:=	substr(ds_alteracao_w || CHR(10) || CHR(13) ||
					wheb_mensagem_pck.get_texto(791471,
							'DS_OBSERVACAO_OLD='||:old.ds_observacao||
							';DS_OBSERVACAO_NEW='||:new.ds_observacao),1,4000);
					
	end if;

	if	(ds_alteracao_w is not null) then
		gravar_historico_montagem(:new.nr_seq_agenda,'AC',ds_alteracao_w,:new.nm_usuario);
	end if;	
else
	
	ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791490,
							'DS_CLASSIF='||substr(cme_obter_desc_classif_conj(:new.nr_seq_classificacao),1,125)),1,4000);
	
	if	(ds_alteracao_w is not null) then
		gravar_historico_montagem(:new.nr_seq_agenda,'IC',ds_alteracao_w,:new.nm_usuario);
	end if;
end if;	


end;
/
