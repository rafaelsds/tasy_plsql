create or replace
trigger cirurgia_particip_pend_atual
after insert or update on cirurgia_participante
for each row
declare

qt_reg_w  		number(1);
ie_tipo_w  		varchar2(10);
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ie_libera_partic_w	varchar2(5);

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

select	nvl(max(ie_libera_partic),'N')
into		ie_libera_partic_w
from		parametro_medico
where		cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_partic_w = 'S') then	
	if (:new.dt_liberacao is null) then
		ie_tipo_w := 'CP';
	elsif (:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCP';
	end if;
	
	select max(c.nr_atendimento),
		max(c.cd_pessoa_fisica)
	into	nr_atendimento_w, 
		cd_pessoa_fisica_w
	from	cirurgia c
	where	c.nr_cirurgia = :new.nr_cirurgia;
	
	
	if ((ie_tipo_w is not null) and (nvl(cd_pessoa_fisica_w,0) > 0)) then
		gerar_registro_pendente_pep(ie_tipo_w, :new.nr_seq_interno, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w := 0;

end;
/