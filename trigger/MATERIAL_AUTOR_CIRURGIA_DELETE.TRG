create or replace 
trigger material_autor_cirurgia_delete
after delete on material_autor_cirurgia
for each row
declare

nr_sequencia_autor_w	number(10);
nr_seq_material_autor_w	number(10);
nr_seq_estagio_w	number(10);
ie_interno_w		varchar2(2);
ie_lib_materiais_esp_w	varchar2(1)	:= 'N';

begin

begin

select	nvl(max(ie_lib_materiais_esp),'N')
into	ie_lib_materiais_esp_w
from	parametro_faturamento b,
	autorizacao_cirurgia a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_sequencia		= :old.nr_seq_autorizacao;

if	(nvl(ie_lib_materiais_esp_w,'N') = 'N')  then
	
	select	max(nr_sequencia),
		max(nr_seq_estagio)
	into	nr_sequencia_autor_w,
		nr_seq_estagio_w
	from	autorizacao_convenio
	where	nr_seq_autor_cirurgia	= :old.nr_seq_autorizacao;

	if	(nr_sequencia_autor_w is not null) then
	
		select	max(ie_interno)
		into	ie_interno_w
		from	estagio_autorizacao
		where	nr_sequencia	= nvl(nr_seq_estagio_w,0)
		and		OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
		
		select	max(nr_sequencia)
		into	nr_seq_material_autor_w
		from	material_autorizado
		where	cd_material		= :old.cd_material
		and	nr_sequencia_autor	= nr_sequencia_autor_w;
	
		if	(nr_seq_material_autor_w is not null) and
			(ie_interno_w = '1' or nr_seq_estagio_w is null) then
	
			delete	from	material_autorizado
			where	nr_sequencia	= nr_seq_material_autor_w;
		end if;
	end if;
end if;

exception
	when others then
	null;
end;

end;
/