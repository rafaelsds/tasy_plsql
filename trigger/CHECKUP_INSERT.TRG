CREATE OR REPLACE TRIGGER Checkup_Insert
After INSERT ON Checkup
FOR EACH ROW
DECLARE

qt_idade_w		number(3,0);
ie_sexo_w		varchar2(1);
nr_seq_avaliacao_w	number(10,0);
nr_sequencia_w		number(10,0);


cursor c01 is
	select	nr_seq_avaliacao
	from	checkup_avaliacao
	where	cd_estabelecimento = :new.cd_estabelecimento
	and	qt_idade_w between nvl(qt_idade_min,qt_idade_w) and nvl(qt_idade_max,qt_idade_w)
	and	ie_sexo_w = nvl(ie_sexo, ie_sexo_w);

BEGIN

select	nvl(obter_idade_pf(:new.cd_pessoa_fisica, sysdate, 'A'),0),
	obter_sexo_pf(:new.cd_pessoa_fisica, 'C')
into	qt_idade_w,
	ie_sexo_w
from	dual;


open c01;
loop
fetch c01 into
	nr_seq_avaliacao_w;
exit when c01%notfound;

	select	med_avaliacao_paciente_seq.nextval
	into	nr_sequencia_w
	from	dual;
		
	insert into med_avaliacao_paciente(
			nr_sequencia,
			cd_pessoa_fisica,
			cd_medico,
			dt_avaliacao,
			dt_atualizacao,
			nm_usuario,
			ds_observacao,
			nr_seq_tipo_avaliacao,
			ie_avaliacao_parcial,
			nr_atendimento,
			nr_prescricao,
			nr_seq_aval_compl,
			dt_liberacao,
			nr_seq_checkup)
	values		(nr_sequencia_w,
			:new.cd_pessoa_fisica,
			:new.cd_pessoa_fisica,
			sysdate,
			sysdate,
			:new.nm_usuario,
			null,
			nr_seq_avaliacao_w,
			'N',
			:new.nr_atendimento,
			:new.nr_prescricao,
			null,
			null,
			:new.nr_sequencia);
end loop;
close c01;

END;
/