CREATE OR REPLACE
TRIGGER ATEND_ESCALA_COMFORT_B
BEFORE INSERT OR UPDATE ON ESCALA_COMFORT_B
FOR EACH ROW
declare

BEGIN

:new.QT_PONTUACAO := 	nvl(:new.IE_CONCIENCIA, -1) +1 +
						nvl(:new.IE_AGITACAO, -1) +1 +
						nvl(:new.IE_VENTILACAO, -1) +1 +
						nvl(:new.IE_CHORO, -1) +1 +
						nvl(:new.IE_FISICO, -1) +1 +
						nvl(:new.IE_TONUS, -1) +1 +
						nvl(:new.IE_TENSAO, -1) +1;
						
END;
/