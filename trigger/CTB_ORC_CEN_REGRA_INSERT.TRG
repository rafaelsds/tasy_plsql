create or replace
trigger ctb_orc_cen_regra_insert
before insert or update on ctb_orc_cen_regra
for each row
declare

ie_conta_vigente_w		Varchar2(1);
ie_tipo_w			varchar2(1);
ie_situacao_w			varchar2(1);
ie_permite_w			varchar2(1);
ie_resultado_w		varchar2(1);

BEGIN
if	(:new.ie_regra_valor = 'VF') then
	begin
	:new.pr_aplicar			:= null;
	:new.nr_seq_mes_ref_orig		:= null;
	if	(:new.vl_fixo is null) then
		/*Para regra de valor fixo deve haver um valor informado, mesmo que seja zero!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266569);
	end if;
	if	(:new.nr_seq_mes_ref_orig is not null) then
		/*'Para valores fixos informar a data inicial e final e n�o o m�s de origem!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266570);
	end if;
	if	(:new.dt_mes_inic is null) or
		(:new.dt_mes_fim is null) then
		/*'Para valores fixos informar a data inicial e final!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266571);
	end if;
	if	(:new.cd_centro_custo is null) and 
		(:new.nr_seq_criterio_rateio is null) then
		/*Para valores fixos deve-se informar um crit�rio de rateio ou um Centro de custo!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266572);
	end if;
	if	(:new.cd_conta_contabil is null) then
		/*Para valores fixos informar a conta contabil!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266573);
	end if;
	if	(:new.cd_centro_custo is not null) and
		(:new.nr_seq_criterio_rateio is not null) then
		/*Para valores fixos deve-se informar somente um crit�rio de rateio ou um Centro de custo!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266574);
	end if;
	end;
elsif	(:new.ie_regra_valor = 'PV') or
	(:new.ie_regra_valor = 'PAV') then
	begin
	if	(:new.dt_mes_inic is null) or
		(:new.dt_mes_fim is null) then
		/*Para este tipo de regra deve ser informado m�s inicial e final!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266575);
	end if;
	if	(:new.pr_aplicar is null) then
		/*Para este tipo de regra deve ser informado uma % APLICAR v�lida!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266576);
	end if;
	end;
end if;

if	(:new.ie_regra_valor = 'PV') then
	:new.vl_fixo			:= null;
	:new.nr_seq_mes_ref_orig	:= null;
	
	select	nvl(max(obter_valor_param_usuario(925, 30, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento)), 'N')
	into	ie_permite_w	
	from	dual;
	/* Matheus OS 84429 28/02/2008*/
	if	(ie_permite_w = 'N') and
		(nvl(:new.cd_centro_origem,0) = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(266577);
	end if;
elsif	(:new.ie_regra_valor = 'PAV') then
	:new.vl_fixo			:= null;
elsif	(:new.ie_regra_valor = 'CM') then
	:new.vl_fixo			:= null;
	:new.pr_aplicar			:= null;
	:new.nr_seq_mes_ref_orig	:= null;
end if;

/* Consist�ncia centro destino*/
if	(:new.cd_centro_custo is not null) then
	select	nvl(max(ie_tipo),'X'),
		nvl(max(ie_situacao),'I')
	into	ie_tipo_w,
		ie_situacao_w
	from	centro_custo
	where	cd_centro_custo = :new.cd_centro_custo;
	
	if	(ie_tipo_w <> 'A') then
		wheb_mensagem_pck.exibir_mensagem_abort(266580);
		/*S� pode ser informado centro de custo do tipo anal�tico!');*/
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266581);
		/*O centro de custo informado est� inativo!');*/
	end if;

end if;
/* Consist�ncia centro origem*/
if	(:new.cd_centro_origem is not null) then
	select	nvl(max(ie_tipo),'X'),
		nvl(max(ie_situacao),'I')
	into	ie_tipo_w,
		ie_situacao_w
	from	centro_custo
	where	cd_centro_custo = :new.cd_centro_origem;
	
	if	(ie_tipo_w <> 'A') then
		/*S� pode ser informado centro de custo Origem do tipo anal�tico!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266582);
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266583);
		/*O centro de custo origem informado est� inativo!');*/
	end if;
end if;

if	(:new.cd_conta_contabil is not null) then
	select	nvl(max(a.ie_tipo),'X'),
		nvl(max(a.ie_situacao),'I'),
		max(b.ie_tipo)
	into	ie_tipo_w,
		ie_situacao_w,
		ie_resultado_w
	from	ctb_grupo_conta b,
		conta_contabil a
	where	a.cd_grupo		= b.cd_grupo
	and	a.cd_conta_contabil	= :new.cd_conta_contabil;

	if	(ie_tipo_w <> 'A') then
		/*S� pode ser informado Conta cont�bil anal�tica!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266584);
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266585);
		/*A conta cont�bil destino informada est� inativa!');*/
	end if;
	if	(ie_resultado_w not in ('R','C','D')) then
		wheb_mensagem_pck.exibir_mensagem_abort(266586);
		/*A conta cont�bil destino informada n�o � do tipo resultado!');*/
	end if;

end if;

/* Consistencia conta origem*/
if	(:new.cd_conta_origem is not null) then
	select	nvl(max(a.ie_tipo),'X'),
		nvl(max(a.ie_situacao),'I'),
		max(b.ie_tipo)
	into	ie_tipo_w,
		ie_situacao_w,
		ie_resultado_w
	from	ctb_grupo_conta b,
		conta_contabil a
	where	a.cd_grupo		= b.cd_grupo
	and	a.cd_conta_contabil	= :new.cd_conta_origem;

	if	(ie_tipo_w <> 'A') then
		wheb_mensagem_pck.exibir_mensagem_abort(266584);
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266587);
	end if;
	if	(ie_resultado_w not in ('R','C','D')) then
		wheb_mensagem_pck.exibir_mensagem_abort(266588);
	end if;

end if;
END;
/