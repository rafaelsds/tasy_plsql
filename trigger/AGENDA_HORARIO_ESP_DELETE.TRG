CREATE OR REPLACE TRIGGER Agenda_Horario_Esp_Delete
before delete ON Agenda_Horario_Esp
FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

begin

delete	from agenda_controle_horario
where	dt_agenda >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
and	dt_agenda between ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:old.dt_Agenda) and ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(:old.dt_Agenda)
and	cd_agenda	 = :old.cd_agenda;

exception
	when others then
      	dt_atualizacao := sysdate;
end;

<<final>>
null;

END;
/
