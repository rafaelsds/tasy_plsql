CREATE OR REPLACE TRIGGER BANDA_RELAT_CAMPO_LONGO
	BEFORE INSERT OR DELETE OR UPDATE ON BANDA_RELAT_CAMPO_LONGO
	FOR EACH ROW

BEGIN
	IF(WHEB_USUARIO_PCK.GET_IE_EXECUTAR_TRIGGER = 'N')THEN 
		RETURN;
	END IF;

	exec_sql_dinamico(  '',
						' UPDATE RELATORIO '||
						' SET 	IE_GERAR_RELATORIO = ''S'' ,'||
						' DT_LAST_MODIFICATION     = SYSDATE,'||
						' DT_ATUALIZACAO     = SYSDATE,'||
						' NM_USUARIO         = ' || CHR(39) ||NVL(:NEW.NM_USUARIO,:OLD.NM_USUARIO) || CHR(39) ||
						' WHERE 	NR_SEQUENCIA 	   = (SELECT	b.nr_seq_relatorio ' ||
						' FROM	BANDA_RELAT_CAMPO a, ' ||
						' 		BANDA_RELATORIO b ' ||
						' WHERE	a.nr_seq_banda = b.nr_sequencia ' ||
						' AND 	a.nr_Sequencia = ' || NVL(:NEW.nr_Seq_banda_relat_campo, :OLD.nr_Seq_banda_relat_campo) || ')');
											
											
END;
/