CREATE OR REPLACE TRIGGER laudo_paciente_loc_insert
AFTER INSERT ON laudo_paciente_loc
FOR EACH ROW

declare
qt_regra_w		number(10,0);
nr_seq_regra_w		number(10,0);
ds_assunto_w		varchar2(100);
ds_email_origem_w	varchar2(100);
nm_usuario_ref_w	varchar2(15);
ds_conteudo_w		varchar2(2000);
cd_pessoa_fisica_w	varchar2(10);
ds_email_destino_w	varchar2(255);
ds_titulo_laudo_w	varchar2(255);
ds_local_w		varchar2(255);
ds_setor_atendimento_w	varchar2(255);
dt_exame_w		date;
ds_exames_w		varchar2(1000) := '';
ds_procedimento_w	varchar2(255);
ie_email_valido_w	varchar2(2);

Cursor C01 is
	select  substr(obter_desc_procedimento(a.cd_procedimento, a.ie_origem_proced),1,255)	
	from  	procedimento_paciente a
	where	nr_laudo = :new.nr_sequencia;

BEGIN

select	count(*)
into	qt_regra_w
from	regra_envio_loc_laudo
where	ie_tipo_local = :new.ie_tipo_local
and	ie_situacao = 'A';

if	(qt_regra_w > 0) then
	begin
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_regra_w
	from	regra_envio_loc_laudo
	where	ie_tipo_local = :new.ie_tipo_local
	and	ie_situacao = 'A';
	
	if	(nr_seq_regra_w <> 0) then
		begin
		
		select	ds_titulo,
			ds_email_origem,
			nvl(nm_usuario_ref, :new.nm_usuario),
			ds_email
		into	ds_assunto_w,
			ds_email_origem_w,
			nm_usuario_ref_w,
			ds_conteudo_w
		from	regra_envio_loc_laudo
		where	nr_sequencia = nr_seq_regra_w;
		
		select	max(nvl(nvl(obter_dados_atendimento(nr_atendimento, 'CP'), obter_dados_prescricao(nr_prescricao, 'P')), cd_pessoa_fisica)),
			max(ds_titulo_laudo),
			max(dt_exame)
		into	cd_pessoa_fisica_w,
			ds_titulo_laudo_w,
			dt_exame_w
		from	laudo_paciente
		where	nr_sequencia = :new.nr_sequencia;
		
		if	(cd_pessoa_fisica_w is not null) then
			begin
			
			ds_email_destino_w	:= substr(obter_compl_pf(cd_pessoa_fisica_w, 1, 'M'),1,255);
			
			if	(ds_assunto_w is not null) and
				(ds_conteudo_w is not null) and				
				(ds_email_destino_w is not null) and
				(nm_usuario_ref_w is not null) then
				begin
				
				select substr(obter_valor_dominio(138,:new.ie_tipo_local),1,255)
				into   ds_local_w
				from   dual;
				
				select  nvl(max(ds_setor_atendimento),'')
				into	ds_setor_atendimento_w
				from 	setor_atendimento 
				where 	cd_setor_atendimento = :new.cd_setor_registro;
				
				open C01;
				loop
				fetch C01 into	
					ds_procedimento_w;
				exit when C01%notfound;
					begin 
					ds_exames_w := ds_exames_w || ' ' ||  ds_procedimento_w;
					end;
				end loop;
				close C01;
								
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@TITULO@',ds_titulo_laudo_w),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@LOCAL@',ds_local_w),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@SETOR@',ds_setor_atendimento_w),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@PACIENTE@', obter_nome_pf(cd_pessoa_fisica_w)),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@DTEXAME@', to_char(dt_exame_w,'DD/MM/YYYY HH24:MI:SS')),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@EXAME@', ds_exames_w),1,2000);
				
				select obter_se_email_valido(ds_email_destino_w)
				into	ie_email_valido_w
				from	dual;
				
				if	(ie_email_valido_w = 'S') then
					enviar_email(ds_assunto_w, ds_conteudo_w, ds_email_origem_w, ds_email_destino_w, nm_usuario_ref_w, 'M');
				end if;
				
				insert into laudo_paciente_loc_envio (
						nr_sequencia,
						dt_envio,
						nm_usuario,
						ds_email_origem,
						ds_email_destino,      
						ds_conteudo,            
						dt_atualizacao,         
						dt_atualizacao_nrec,    
						nm_usuario_nrec,
						nr_seq_loc)
				values(		laudo_paciente_loc_envio_seq.nextval,
						sysdate,
						nm_usuario_ref_w,
						ds_email_origem_w,
						ds_email_destino_w,
						ds_conteudo_w,
						sysdate,
						sysdate,
						:new.nm_usuario,
						:new.nr_seq_loc);
				end;
			end if;
			end;
		end if;		
		end;
	end if;	
	end;
end if;

END;
/