create or replace trigger ctb_orc_cenario_atual
before insert or update on ctb_orc_cenario
for each row
declare
-- local variables here
begin
if (:new.ie_tipo_cenario = 'P' and nvl(:new.nr_ano, 0) <> 0) then
	if inserting then
		begin
			--  Mes Inicio
			select	a.nr_sequencia
			into	:new.nr_seq_mes_inicio
			from	ctb_mes_ref a
			where	to_char(a.dt_referencia, 'MM/YYYY')	= '01/' || :new.nr_ano
			and	a.cd_empresa				= :new.cd_empresa;

			--  Mes Fim
			select	nr_sequencia
			into	:new.nr_seq_mes_fim
			from	ctb_mes_ref
			where	to_char(dt_referencia, 'MM/YYYY')	= '12/' || :new.nr_ano
			and	cd_empresa				= :new.cd_empresa;
		exception
			when no_data_found then
			wheb_mensagem_pck.exibir_mensagem_abort(1111918);
		end;
	elsif (:new.nr_ano <> :old.nr_ano) then
		begin
			--  Mes Inicio
			select	a.nr_sequencia
			into	:new.nr_seq_mes_inicio
			from	ctb_mes_ref a
			where	to_char(a.dt_referencia, 'MM/YYYY')	= '01/' || :new.nr_ano
			and	a.cd_empresa				= :new.cd_empresa;

			--  Mes Fim
			select	nr_sequencia
			into	:new.nr_seq_mes_fim
			from	ctb_mes_ref
			where	to_char(dt_referencia, 'MM/YYYY')	= '12/' || :new.nr_ano
			and	cd_empresa				= :new.cd_empresa;
		exception
			when no_data_found then
			wheb_mensagem_pck.exibir_mensagem_abort(1111918);
		end;
	end if;
end if;

end ctb_orc_cenario_atual;
/