CREATE OR REPLACE TRIGGER Item_Req_Material_Update
BEFORE UPDATE ON Item_Requisicao_Material
FOR EACH ROW

declare
nr_seq_terceiro_w				Number(10,0);
nr_sequencia_w				Number(10,0);
nr_seq_operacao_w			Number(10,0);
nr_seq_oper_terc_w			Number(10,0) 	:= 0;
ie_entrada_saida_w				Varchar2(01)	:= 'S';
ie_existe_w				Number(5);
qt_reg_w					number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(nvl(obter_tipo_motivo_baixa_req(nvl(:new.cd_motivo_baixa,0)),0) > 0) and
	((:new.dt_atendimento is null ) and (:new.dt_reprovacao is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(266078);
	--'N�o pode ter codigo de baixa com data de atendimento nula!!! ');
end if;

if	(nvl(obter_tipo_motivo_baixa_req(nvl(:new.cd_motivo_baixa,0)),0) = 0) and
	(:new.dt_atendimento is not null ) then
	wheb_mensagem_pck.exibir_mensagem_abort(395271);
	--'N�o pode ter data de atendimento sem motivo de baixa.');
end if;

begin
select	nvl(max(b.nr_seq_terceiro),0),
	max(c.ie_entrada_saida)
into	nr_seq_terceiro_w,
	ie_entrada_saida_w
from 	Operacao_estoque c,
	centro_custo b, 
	requisicao_material a
where	a.cd_centro_custo 	= b.cd_centro_custo
and	a.nr_requisicao		= :new.nr_requisicao
and	a.cd_operacao_estoque	= c.cd_operacao_estoque;
exception
	when others then
		nr_seq_terceiro_w	:= 0;
end;

if	(nr_seq_terceiro_w > 0) then
	begin
	begin
	select nr_sequencia
	into nr_seq_oper_terc_w
	from terceiro_operacao
	where nr_seq_terceiro 	= nr_seq_terceiro_w
	  and nr_doc			= :new.nr_requisicao
	  and nr_seq_doc		= :new.nr_sequencia;
	exception
		when others then	
			nr_seq_oper_terc_w	:= 0;
	end;
		
	if  	(nvl(obter_tipo_motivo_baixa_req(:new.cd_motivo_baixa),0) in (1,5)) and /* Matheus OS61284 11/07/07 inclui motivo 5*/
	  	(nr_seq_oper_terc_w = 0) then

		obter_param_usuario(907,60,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,nr_seq_operacao_w);
		select	count(*)
		into	ie_existe_w
		from	operacao_terceiro
		where	nr_sequencia = nvl(nr_seq_operacao_w,1);	
		if	(ie_existe_w	= 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(266079);
			--'N�o existe opera��o de terceiro cadastrada.' || chr(10) || chr(13));
		end if;
		
		begin

		select terceiro_operacao_seq.nextval
		into	nr_sequencia_w
		from	dual;
		insert into terceiro_operacao(
			nr_sequencia,
			cd_estabelecimento,
			nr_seq_terceiro,
			nr_seq_operacao,
			tx_operacao,
			dt_atualizacao,
			nm_usuario,
			nr_doc,
			nr_seq_doc,
			dt_operacao,
			nr_seq_conta,
			cd_material,
			ie_origem_proced,
			cd_procedimento,
			qt_operacao,
			vl_operacao)
		values(nr_sequencia_w,
			:new.cd_estabelecimento,
			nr_seq_terceiro_w,
			nvl(nr_seq_operacao_w,1),
			100,
			sysdate,
			:new.nm_usuario,
			:new.nr_requisicao,
			:new.nr_sequencia,
			:new.dt_atendimento,
			null,
 			:new.cd_material,
			null,
			null, 
			decode(ie_entrada_saida_w,'S', :new.qt_material_atendida, :new.qt_material_atendida * -1),
			0);
		atualizar_operacao_terceiro(nr_sequencia_w, :new.nm_usuario);
		end;
	elsif  	(nvl(obter_tipo_motivo_baixa_req(:new.cd_motivo_baixa),0) = 0) then
		begin
		delete from terceiro_operacao
		where	nr_sequencia = nr_seq_oper_terc_w;
		end;
	end if;
	end;
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/