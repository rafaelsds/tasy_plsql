CREATE OR REPLACE TRIGGER PROCED_MEDIC_REACAO_BEFINSERT
BEFORE INSERT OR UPDATE ON PROCED_MEDIC_REACAO
FOR EACH ROW

BEGIN

if (:new.cd_material is null) and
	(:new.nr_seq_ficha_tecnica is null) then
	--Necessário informar o material ou a ficha técnica do material.
	wheb_mensagem_pck.exibir_mensagem_abort(804057);
end if;

END;
/
