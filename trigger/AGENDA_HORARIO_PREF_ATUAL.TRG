CREATE OR REPLACE TRIGGER agenda_horario_pref_Atual
before insert or update ON agenda_horario_pref
FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
cd_agenda_w	number(10);
BEGIN
begin

select	max(cd_agenda)
into	cd_agenda_w
from 	agenda_turno
where	nr_sequencia = :new.nr_seq_turno;

if (nvl(cd_agenda_w,0) > 0) then
	delete	from agenda_controle_horario
	where	dt_agenda >= trunc(sysdate)
	and	cd_agenda	 = cd_agenda_w;
end if;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/
