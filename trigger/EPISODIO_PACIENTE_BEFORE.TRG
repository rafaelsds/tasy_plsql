create or replace trigger EPISODIO_PACIENTE_BEFORE
before insert on episodio_paciente
for each row

begin
if(nvl(pkg_i18n.get_user_locale, 'pt_BR') in ('de_DE', 'de_AT'))then
   cancelar_atend_controle(:new.cd_pessoa_fisica, 'S');
end if;
end;
/
