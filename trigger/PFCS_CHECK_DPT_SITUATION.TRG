CREATE OR REPLACE TRIGGER PFCS_CHECK_DPT_SITUATION
BEFORE INSERT OR UPDATE ON PFCS_SERVICE_REQUEST
FOR EACH ROW

DECLARE
nm_usuario_w    setor_atendimento.nm_usuario%type := 'PFCS';

/*
    This trigger will check if a department is inactive when there is a request for it.
    The IBE integration does not send the situation of a department, just for the bed. The client can disable a department
    throught the EMR or PFCS interface. It's also possible to enable again on PFCS, but if, for some reason, it's not and
    a request for a bed in that department comes, it is implicit that the department is active again.
*/

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

    if (:new.nr_seq_location is not null) then
        update setor_atendimento set
            IE_SITUACAO = 'A',
            NM_USUARIO = nm_usuario_w,
			IE_OCUP_HOSPITALAR = 'S',
            DT_ATUALIZACAO = SYSDATE
        where NM_USUARIO_NREC = nm_usuario_w
        and cd_setor_atendimento = (
            select max(cd_setor_atendimento)
            from unidade_atendimento
            where nr_seq_location = :new.nr_seq_location
        )
        and ie_situacao = 'I';
    end if;
	
end if;

END;
/