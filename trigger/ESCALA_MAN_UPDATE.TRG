create or replace trigger escala_man_update
after update on escala_man
for each row

declare
nr_sequencia_w	atendimento_sinal_vital.nr_sequencia%type;

begin
if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	((:new.qt_peso > 0) or (:new.qt_altura > 0)) then

	select	atendimento_sinal_vital_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into atendimento_sinal_vital(
				nr_sequencia,
				cd_pessoa_fisica,
				nr_atendimento,
				dt_sinal_vital,
				dt_atualizacao,
				nm_usuario,
				qt_peso,
				qt_altura_cm,
				ds_observacao,
				dt_liberacao,
				ie_situacao)
	values(		nr_sequencia_w,
				:new.cd_profissional,
				:new.nr_atendimento,
				sysdate,
				sysdate,
				:new.nm_usuario,
				:new.qt_peso,
				:new.qt_altura,
				substr(obter_texto_dic_objeto(292100, wheb_usuario_pck.get_nr_seq_idioma, null), 1, 2000),
				sysdate,
				'A');
end if;
end;
/