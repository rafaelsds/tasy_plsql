create or replace trigger titulo_receber_classif_integr
after insert or update on titulo_receber_classif
for each row

declare

qt_reg_w	number(10);
reg_integracao_w		gerar_int_padrao.reg_integracao;

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:new.nr_titulo is not null) then

	/*Esse select e para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');
	
	if (qt_reg_w = 0) then

		gerar_int_padrao.gravar_integracao('26', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);
		
	end if;

end if;

if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TR',:new.dt_atualizacao,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TR',:new.dt_atualizacao,'A',:new.nm_usuario);
end if;
end if;
end;
/