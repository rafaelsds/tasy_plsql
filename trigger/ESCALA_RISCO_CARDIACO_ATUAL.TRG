CREATE OR REPLACE TRIGGER escala_risco_cardiaco_atual BEFORE
    INSERT OR UPDATE ON escala_risco_cardiaco
    FOR EACH ROW
DECLARE
    qt_risco_w       NUMBER := 0;
    qt_pontuacao_w   NUMBER := 0;
    sql_w            VARCHAR2(200);
    ds_erro_w        VARCHAR2(4000);
    ds_parametros_w  VARCHAR2(4000);
BEGIN
    IF ( wheb_usuario_pck.get_ie_executar_trigger = 'S' ) THEN
        BEGIN
            sql_w := 'begin obter_score_ris_card_pontua_md(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10); end;';
            EXECUTE IMMEDIATE sql_w
                USING IN :new.ie_iam_menor_6, IN :new.ie_angina_iii, IN :new.ie_edema_pul_menor, IN :new.ie_estenose, IN :new.ie_ritmo,
                IN :new.ie_5_extra_sistoles, IN :new.ie_result_exame, IN :new.ie_idade_maior_70, IN :new.ie_cir_emergencia, OUT qt_pontuacao_w;

        EXCEPTION
            WHEN OTHERS THEN
      ds_erro_w := sqlerrm;
      ds_parametros_w := (':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.cd_profissional: '||:new.cd_profissional||'-'||':new.ie_situacao: '||:new.ie_situacao||'-'||
                          ':new.ie_iam_menor_6: '||:new.ie_iam_menor_6||'-'||':new.ie_angina_iii: '||:new.ie_angina_iii||'-'||':new.ie_edema_pul_menor: '||:new.ie_edema_pul_menor||'-'||
                          ':new.ie_estenose: '||:new.ie_estenose||'-'||':new.ie_ritmo: '||:new.ie_ritmo||'-'||':new.ie_5_extra_sistoles: '||:new.ie_5_extra_sistoles||'-'||
                          ':new.ie_result_exame: '||:new.ie_result_exame||'-'||':new.ie_idade_maior_70: '||:new.ie_idade_maior_70||'-'||':new.ie_cir_emergencia: '||:new.ie_cir_emergencia||'-'||
                          'qt_pontuacao_w: '||qt_pontuacao_w);

      gravar_log_medical_device ('escala_risco_cardiaco_atual','obter_score_ris_card_pontua_md'
                                 ,ds_parametros_w,substr(ds_erro_w, 4000),:new.nm_usuario,'N');  
            
                qt_pontuacao_w := NULL;
        END;

        :new.qt_pontuacao := qt_pontuacao_w;
        BEGIN
            sql_w := 'begin obter_score_ris_card_risco_md(:1, :2, :3, :4, :5, :6, :7, :8, :9); end;';
            EXECUTE IMMEDIATE sql_w
                USING IN :new.ie_fat_diabete, IN :new.ie_fat_extra_sistoles, IN :new.ie_fat_onda_q, IN :new.ie_fat_angina, IN :new.ie_fat_hist_iam, 
                IN :new.ie_fat_hist_icc, IN :new.ie_fat_hipertensao, IN :new.ie_idade_maior_70, OUT qt_risco_w;

        EXCEPTION
            WHEN OTHERS THEN
      ds_erro_w := sqlerrm;
      ds_parametros_w := (':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.cd_profissional: '||:new.cd_profissional||'-'||':new.ie_situacao: '||:new.ie_situacao||'-'||
                          ':new.ie_fat_diabete: '||:new.ie_fat_diabete||'-'||':new.ie_fat_extra_sistoles: '||:new.ie_fat_extra_sistoles||'-'||':new.ie_fat_onda_q: '||:new.ie_fat_onda_q||'-'||
                          ':new.ie_fat_angina: '||:new.ie_fat_angina||'-'||':new.ie_fat_hist_iam: '||:new.ie_fat_hist_iam||'-'||':new.ie_fat_hist_icc: '||:new.ie_fat_hist_icc||'-'||
                          ':new.ie_fat_hipertensao: '||:new.ie_fat_hipertensao||'-'||':new.ie_idade_maior_70: '||:new.ie_idade_maior_70||'-'||'qt_risco_w: '||qt_risco_w);

      gravar_log_medical_device ('escala_risco_cardiaco_atual','obter_score_ris_card_risco_md'
                                 ,ds_parametros_w,substr(ds_erro_w, 4000),:new.nm_usuario,'N');  
            
                qt_risco_w := NULL;
        END;

        :new.qt_risco := qt_risco_w;
    END IF;
END;
/