create or replace trigger Protocolo_Medic_Proc_Atual
before insert or update on Protocolo_Medic_Proc
for each row

declare
ie_situacao_w	varchar2(1);
begin
begin

if	(:new.cd_procedimento is null) and
	(:new.nr_seq_proc_interno is null) and
	(:new.nr_seq_exame is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(201161);
end if;

if	(obter_se_prot_lib_regras = 'S') then
	update	protocolo_medicacao
	set	nm_usuario_aprov	=	null,
		dt_aprovacao		=	null,
		ie_status		=	'PA'
	where	cd_protocolo		=	:new.cd_protocolo
	and	nr_sequencia		=	:new.nr_sequencia;
end if;

if	(:new.NR_SEQ_EXAME is not null) then
	select	nvl(max(ie_situacao),'A')
	into	ie_situacao_w
	from	exame_laboratorio
	where	nr_seq_exame = :new.NR_SEQ_EXAME;
	
	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(201160);
	end if;
end if;

if (:new.hr_prim_horario is not null) and ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_prim_horario is null)) then
	:new.dt_prim_horario := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario,'dd/mm/yyyy hh24:mi');
end if;	
exception
	when others then
	null;
end;

end;
/
