create or replace trigger repasse_terc_venc_trib_delete
before delete on repasse_terc_venc_trib
for each row

declare
ds_benefic_w			varchar2(80);
dt_vencimento_w			date;
dt_emissao_w			date;
vl_vencimento_w			number(15,2);
ds_tributo_w			varchar2(40);
ds_log_w			varchar2(4000);
qt_registro_w			pls_integer;
begin


select	substr(max(obter_nome_terceiro(b.nr_seq_terceiro)),1,80),
	max(a.dt_vencimento),
	max(a.vl_vencimento)
into	ds_benefic_w,
	dt_vencimento_w,
	vl_vencimento_w
from	repasse_terceiro b,
	repasse_terceiro_venc a
where	a.nr_repasse_terceiro	= b.nr_repasse_terceiro
and	nr_sequencia		= :old.nr_seq_rep_venc;

select	max(ds_tributo)
into	ds_tributo_w
from	tributo
where	cd_tributo = :old.cd_tributo;

--ds_log_w	:= substr(	'Terceiro repasse: ' || ds_benefic_w || chr(13) ||
--				'Vencimento: ' || dt_vencimento_w || chr(13) ||
--				'Cria��o tributo: ' || to_char(:old.dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss') || chr(13) ||
--				'Valor venc: ' || vl_vencimento_w || chr(13) ||
--				'Tributo: ' || ds_tributo_w || chr(13) ||
--				'Valor tributo ' || :old.vl_imposto,1,4000);

ds_log_w	:= substr(	wheb_mensagem_pck.get_texto(304660) || ds_benefic_w || chr(13) ||
				wheb_mensagem_pck.get_texto(304662) || dt_vencimento_w || chr(13) ||
				wheb_mensagem_pck.get_texto(304663) || to_char(:old.dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss') || chr(13) ||
				wheb_mensagem_pck.get_texto(304664) || vl_vencimento_w || chr(13) ||
				wheb_mensagem_pck.get_texto(304665) || ds_tributo_w || chr(13) ||
				wheb_mensagem_pck.get_texto(304667) || :old.vl_imposto,1,4000);
				
fin_gerar_log_controle_banco(100, ds_log_w, 'Tasy','N');

select	count(1)
into	qt_registro_w
from	pls_pp_base_acum_trib
where	nr_seq_vl_repasse = :old.nr_sequencia;

if	(qt_registro_w > 0) then
	-- Existem registros dependentes
	wheb_mensagem_pck.exibir_mensagem_abort(457570);
end if;

select	count(1)
into	qt_registro_w
from	pls_pp_lr_base_trib
where	nr_seq_vl_repasse = :old.nr_sequencia;

if	(qt_registro_w > 0) then
	-- Existem registros dependentes
	wheb_mensagem_pck.exibir_mensagem_abort(457570);
end if;

end;
/
