CREATE OR REPLACE TRIGGER UBELEM_ALTERACAO_VENC_INSERT
BEFORE INSERT ON ALTERACAO_VENCIMENTO
FOR EACH ROW

DECLARE

ie_integra_unimed_w		varchar2(1);

begin

select	nvl(max(ie_integra_unimed),'N')
into	ie_integra_unimed_w
from	titulo_receber
where	nr_titulo	= :new.nr_titulo;

/* Bloqueia altera��es de t�tulos importados de integra��o com o sistema da Unimed */
if	(nvl(ie_integra_unimed_w,'N') = 'S') then
	-- N�o � poss�vel alterar t�tulos com origem integra��o.
	wheb_mensagem_pck.exibir_mensagem_abort(387695);
end if;

end;
/
