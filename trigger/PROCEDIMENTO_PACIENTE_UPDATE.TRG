create or replace trigger procedimento_paciente_update
before update on procedimento_paciente
for each row

declare

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ X ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
	IE_CONSISTE_SETOR_PROCED_W
		Parametro 942 - [351] - Consistir regra de liberacao de procedimento por setor ao altera-los
		
	IE_CONVENIO_CONTA_W
		Parametro 67 - [464] - Utiliza o convenio da conta para consistir a Regra medico (Cadastro de convenio)
		
	IE_ALTERA_CBO_BPA_W
		Parametro 1125 - [132] - Ajustar o CBO automaticamente ao alterar um procedimento.
	
	IE_GRAVA_LOG_MED_W
		Parametro 1125 - [138] - Gravar log dos executores dos procedimentos, ao executar a prescricao.
	
	Parametro 67 - [269] - Bloquear a troca de conta dos itens quando estiver em um periodo de auditoria
	
	IE_AJUSTA_EXEC_CONSULTA_BPA_W
		Parametro 1125 - [149] - Salvar o executor sempre no campo medico para o procedimento 301010048
-------------------------------------------------------------------------------------------------------------------
Referencias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_estabelecimento_w		number(5);
ie_tipo_atendimento_w		number(3);
vl_repasse_w			number(15,2);
ie_regra_w			varchar2(1);
ds_convenio_w			varchar2(255);
ds_module_w 			varchar2(255);
osuser_w				varchar2(100);
ie_proc_sus_antigo_w		varchar2(1);
ie_tipo_convenio_w		number(2);
cd_setor_w			number(5);
cd_procedimento_w			number(15);
ie_origem_proced_w		number(10);
ds_erro_w			varchar2(255);
ie_converte_proc_conv_w		varchar2(1);
qt_registro_w			number(15);
cont_w				number(15);
cd_cbo_w				varchar2(6);
qt_reg_w				number(1);
dt_entrada_w			date;
ie_atualiza_cbo_medico_w		varchar2(15);
ie_ordem_converte_proc_w		varchar2(2):= 'EP';
qt_regra_tipo_fatura_w		number(10,0);
nr_seq_tipo_fatura_w		number(10,0);
ie_codigo_autorizacao_w		number(3):= 0;
cd_convenio_w			number(5,0);
ie_convenio_conta_w		varchar2(1);
ds_retorno_fpo_w			varchar2(255):= 'X';
ie_consiste_fpo_w			varchar2(15)	:= 'N';
dt_referencia_w			date;
nr_seq_protocolo_w		number(10,0);
ds_log_w				varchar2(2000);
nr_seq_proc_interno_aux_w		number(10);
ie_altera_cbo_bpa_w		varchar2(15):= 'S';
ie_log_valor_proc_w		varchar2(1);
cd_plano_w			varchar2(10);
cd_tipo_acomodacao_w		number(4,0);
cd_material_exame_w		varchar(20);
dt_geracao_resumo_new_w		date;
dt_geracao_resumo_old_w		date;
nr_seq_servico_w			procedimento_paciente.nr_seq_servico%type := null;
qt_reg_apac_w			number(10);
ds_call_stack_w			varchar2(2000);
qt_proc_bpa_apac_w		number(10);
ie_grava_log_med_w		varchar2(15):= 'N';
ie_ajusta_exec_consulta_bpa_w	varchar2(15):= 'N';
ie_consiste_setor_proced_w		varchar2(2):= 'N';
ie_setor_proced_lib_w		varchar2(2):= 'S';
cd_procedencia_w			number(5);
ds_module_log_w			varchar2(255);
qt_pacote_orig_w			number(10,0);
qt_lote_pre_fat_w			number(10);
ie_excluir_proc_repasse_w		parametro_faturamento.ie_excluir_proc_repasse%type;
ie_cancela_autor_exec_w		parametro_faturamento.ie_cancela_autor_exec%type;
vl_repasse_pend_w			procedimento_repasse.vl_repasse%type;
nr_seq_servico_classif_w		procedimento_paciente.nr_seq_servico_classif%type := null;
ie_log_medico_exec_proc_w		varchar2(1) := 'N';
nr_sequencia_autor_w		number(10);
qt_proc_autor_w			number(13,4);
qt_autor_w			number(10);
nr_seq_estagio_w			number(10);
nr_seq_case_w		    episodio_paciente.nr_sequencia%type;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto final;
end if;

if	(:new.cd_motivo_exc_conta is not null) then
	:new.nr_interno_conta	:= null;
end if;

if	(nvl(:old.nr_interno_conta,0) <> 0) and
	(nvl(:new.nr_interno_conta,0) <> nvl(:old.nr_interno_conta,0)) and
	(:new.cd_motivo_exc_conta is null) then
	begin
	
	select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
	into	ds_module_log_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);
	
	insert into propaci_conta_log
		(nr_sequencia,           
		dt_atualizacao,         
		nm_usuario,             
		dt_atualizacao_nrec,    
		nm_usuario_nrec,        
		nr_atendimento,         
		nr_interno_conta,       
		nr_conta_origem,        
		nr_seq_procedimento,
		ds_module,
		ds_call_stack)
	values	(propaci_conta_log_seq.nextval,           
		sysdate,         
		:new.nm_usuario,             
		sysdate,    
		:new.nm_usuario,        
		:new.nr_atendimento,         
		:new.nr_interno_conta,       
		:old.nr_interno_conta,        
		:new.nr_sequencia,
		ds_module_log_w,
		ds_call_stack_w);
	exception
	when others then
		ds_log_w	:= '';
	end;
			
end if;

/*if	((:new.cd_setor_atendimento) <> (:old.cd_setor_atendimento)) and
	((:new.dt_entrada_unidade) = (:old.dt_entrada_unidade)) and
	((:new.nr_seq_atepacu) = (:old.nr_seq_atepacu)) then

	insert 	into logxxxxx_tasy
			(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
		values	(sysdate, :new.nm_usuario, 98822, ' Atend: ' || :new.nr_atendimento || 
							 ' Setor Novo: ' || :new.cd_setor_atendimento ||
							 ' Setor Anterior: ' || :old.cd_setor_atendimento ||
							 ' Funcao: ' || obter_funcao_ativa ||
							 ' Perfil: ' || obter_perfil_ativo);
end if;*/

/*if	((:new.cd_setor_atendimento) = (:old.cd_setor_atendimento)) and
	((:new.dt_entrada_unidade) <> (:old.dt_entrada_unidade)) and
	((:new.nr_seq_atepacu) <> (:old.nr_seq_atepacu)) then

	insert 	into logxxxxx_tasy
			(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
		values	(sysdate, :new.nm_usuario, 98822, ' Atend: ' || :new.nr_atendimento || 
							 ' Setor Novo: ' || :new.cd_setor_atendimento ||
							 ' Setor Anterior: ' || :old.cd_setor_atendimento ||
							 ' Funcao: ' || obter_funcao_ativa ||
							 ' Perfil: ' || obter_perfil_ativo);
end if;*/

select	cd_estabelecimento,
	ie_tipo_atendimento,
	dt_entrada,
	obter_plano_atendimento(nr_atendimento, 'C') cd_plano,
	obter_tipo_acomod_atend(nr_atendimento, 'C') cd_tipo_acomodacao,
	nvl(cd_procedencia,0) cd_procedencia
into	cd_estabelecimento_w,
	ie_tipo_atendimento_w,
	dt_entrada_w,
	cd_plano_w,
	cd_tipo_acomodacao_w,
	cd_procedencia_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

select	nvl(max(ie_proc_sus_antigo),'N'),
	nvl(max(ie_converte_proc_conv),'N'),
	nvl(max(ie_ordem_converte_proc),'EP'),
	nvl(max(ie_consiste_fpo_execucao),'N'),
	nvl(max(ie_log_valor_proc),'N'),
	max(ie_excluir_proc_repasse),
	nvl(max(ie_log_medico_exec_proc),'N'),
	nvl(max(ie_cancela_autor_exec),'N')
into	ie_proc_sus_antigo_w,
	ie_converte_proc_conv_w,
	ie_ordem_converte_proc_w,
	ie_consiste_fpo_w,
	ie_log_valor_proc_w,
	ie_excluir_proc_repasse_w,
	ie_log_medico_exec_proc_w,
	ie_cancela_autor_exec_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

ie_excluir_proc_repasse_w	:= nvl(ie_excluir_proc_repasse_w,'N');

if	(ie_proc_sus_antigo_w = 'S') and
	(:new.ie_origem_proced in (2,3)) and
	(obter_funcao_ativa	<> 291) then
	begin
	wheb_mensagem_pck.exibir_mensagem_abort(188911);
	/*Este procedimento nao pode ser lancado pois e de origem antiga do SUS. */
	end;
end if;

ie_consiste_setor_proced_w := nvl(obter_valor_param_usuario(942, 351, obter_perfil_ativo, :new.nm_usuario, 0),'N');

if	(nvl(ie_consiste_setor_proced_w,'N') = 'S') and
	(((:new.cd_setor_atendimento) <> (:old.cd_setor_atendimento)) or
	((:new.cd_procedimento) <> (:old.cd_procedimento)) or
	((:new.nr_seq_proc_interno) <> (:old.nr_seq_proc_interno))) then
	begin
	ie_setor_proced_lib_w	:= obter_se_setor_exec(	:new.cd_setor_atendimento,
							cd_estabelecimento_w,
							:new.cd_procedimento,
							:new.ie_origem_proced,
							:new.nr_seq_proc_interno);
	
	if	(ie_setor_proced_lib_w = 'N') then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(230640);
		end;
	end if;
	
	end;	
end if;

begin
ie_convenio_conta_w	:= nvl(obter_valor_param_usuario(67, 464, obter_perfil_ativo, :new.nm_usuario, 0),'N');
ie_altera_cbo_bpa_w	:= nvl(obter_valor_param_usuario(1125, 132, obter_perfil_ativo, :new.nm_usuario, 0),'S');
ie_grava_log_med_w	:= nvl(obter_valor_param_usuario(1125, 138, obter_perfil_ativo, :new.nm_usuario, 0),'N');
exception
when others then
	ie_convenio_conta_w	:= 'N';
	ie_altera_cbo_bpa_w	:= 'S';
	ie_grava_log_med_w	:= 'N';
end;

if	(:new.cd_medico_executor is not null) and
	(:new.cd_convenio is not null) and
	(nvl(:new.cd_medico_executor,0) <> nvl(:old.cd_medico_executor,0)) then
	/* Inicio alteracao OS 251278 21/09/2010 */
	cd_convenio_w:=  :new.cd_convenio;

	if	(ie_convenio_conta_w = 'S') and
		(:new.nr_interno_conta is not null) then
		begin
		select 	nvl(max(cd_convenio_parametro), :new.cd_convenio)
		into	cd_convenio_w
		from 	conta_paciente
		where 	nr_interno_conta	= :new.nr_interno_conta;
		exception
		when others then
			cd_convenio_w	:= :new.cd_convenio;
		end;
	end if;
	/* Fim alteracao OS 251278 21/09/2010 */

	select	obter_convenio_regra_atend(	:new.cd_medico_executor,
						cd_convenio_w,
						ie_tipo_atendimento_w,
						cd_estabelecimento_w,
						'E',
						null,
						null)
	into	ie_regra_w
	from	dual;
	
	if	(ie_regra_w = 'N') then
		select	substr(obter_nome_convenio(cd_convenio_w),1,40)
		into	ds_convenio_w
		from	dual;
		
		wheb_mensagem_pck.exibir_mensagem_abort(188912,'DS_CONVENIO='||ds_convenio_w);
		/* O medico nao esta cadastrado para executar procedimentos para o convenio #@CD_CONVENIO#@ */
	end if;
end if;

if	(:new.cd_medico_executor <> :old.cd_medico_executor) then
	:new.cd_medico_convenio	:= null;
end if;

if	(:new.nr_interno_conta is null) and
	(:old.nr_interno_conta is not null) and
	(:new.cd_motivo_exc_conta is not null) then
	:new.nr_seq_conta_origem	:= :old.nr_interno_conta;
end if;

if	(:old.nr_interno_conta is null) and
	(:new.nr_interno_conta is not null) then
	:new.nr_seq_conta_origem	:= null;
end if;

-- Marcus 4/1/2006
if	(:new.cd_motivo_exc_conta is not null) then
	:new.ie_emite_conta		:= null;
	:new.ie_emite_conta_honor	:= null;
end if;

if	(:old.ie_valor_informado = 'N') and
	(:new.ie_valor_informado = 'S') then
	insert into log_valor_informado
		(nr_sequencia,
		nr_seq_item,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_item,
		nr_atendimento,
		nr_interno_conta,
		ie_acao,
		vl_atual,
		vl_anterior)
	values	(log_valor_informado_seq.NextVal,
		:new.nr_sequencia,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		'P',
		:new.nr_atendimento,
		:new.nr_interno_conta,
		'1',
		:new.vl_procedimento,
		:old.vl_procedimento);
	/*gravar_log_valor_informado(1, :new.nr_sequencia, :new.nr_interno_conta, :new.nr_atendimento, :new.nm_usuario);*/
end if;

if	(:old.ie_valor_informado = 'S') and
	(:new.ie_valor_informado = 'N') then
	insert into log_valor_informado
		(nr_sequencia,
		nr_seq_item,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_item,
		nr_atendimento,
		nr_interno_conta,
		ie_acao)
	values	(log_valor_informado_seq.NextVal,
		:new.nr_sequencia,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		'P',
		:new.nr_atendimento,
		:new.nr_interno_conta,
		'2');	
	/*insert into log_tasy(cd_log, ds_log, dt_atualizacao, nm_usuario) values	
			(1519, :new.nr_sequencia || ' - ' || :new.nr_interno_conta || ' - ' || :new.nr_atendimento, sysdate,:new.nm_usuario);*/
end if;

if	(:new.cd_convenio <> :old.cd_convenio) and
	(ie_converte_proc_conv_w = 'S') then
	cd_procedimento_w	:= 0;

	if	(ie_ordem_converte_proc_w = 'EP') then -- Exame de laboratorio / Proc. Interno
		if	(:new.nr_seq_exame is not null) then
			select	max(ie_tipo_convenio)
			into	ie_tipo_convenio_w
			from	convenio
			where	cd_convenio	= :new.cd_convenio;

			cd_setor_w	:= :new.cd_setor_atendimento;
	
			if	(:new.nr_seq_material is not null) then	
				select	max(cd_material_exame)
				into	cd_material_exame_w
				from	material_exame_lab
				where	nr_sequencia = :new.nr_seq_material;
			end if;
						
			obter_exame_lab_convenio(	:new.nr_seq_exame,
							:new.cd_convenio,
							:new.cd_categoria,
							ie_tipo_atendimento_w,
							cd_estabelecimento_w,
							ie_tipo_convenio_w,
							:new.nr_seq_proc_interno,
							cd_material_exame_w,
							cd_plano_w,
							cd_setor_w,
							cd_procedimento_w,
							ie_origem_proced_w,
							ds_erro_w,
							nr_seq_proc_interno_aux_w);
		elsif	(:new.nr_seq_proc_interno is not null) then
			if	(:new.nr_interno_conta is null) then
				obter_proc_tab_interno_conv(	:new.nr_seq_proc_interno,
								cd_estabelecimento_w,
								:new.cd_convenio,
								:new.cd_categoria,
								cd_plano_w,
								:new.cd_setor_atendimento,
								cd_procedimento_w,
								ie_origem_proced_w,
								:new.cd_setor_atendimento,
								:new.dt_procedimento,
								cd_tipo_acomodacao_w,
								null,
								null,
								null,
								null,
								null,
								null,
								null);
			else			
				obter_proc_tab_interno(	:new.nr_seq_proc_interno,
							:new.nr_prescricao,
							:new.nr_atendimento,
							:new.nr_interno_conta,
							cd_procedimento_w,
							ie_origem_proced_w,
							:new.cd_setor_atendimento,
							null);
			end if;
		end if;
	elsif	(ie_ordem_converte_proc_w = 'PE') then --  Proc. Interno / Exame de laboratorio
		if	(:new.nr_seq_proc_interno is not null) then
			if	(:new.nr_interno_conta is null) then
				obter_proc_tab_interno_conv(	:new.nr_seq_proc_interno,
								cd_estabelecimento_w,
								:new.cd_convenio,
								:new.cd_categoria,
								cd_plano_w,
								:new.cd_setor_atendimento,
								cd_procedimento_w,
								ie_origem_proced_w,
								:new.cd_setor_atendimento,
								:new.dt_procedimento,
								cd_tipo_acomodacao_w,
								null,
								null,
								null,
								null,
								null,
								null,
								null);		
			
			else	
				obter_proc_tab_interno(	:new.nr_seq_proc_interno,
							:new.nr_prescricao,
							:new.nr_atendimento,
							:new.nr_interno_conta,
							cd_procedimento_w,
							ie_origem_proced_w,
							:new.cd_setor_atendimento,
							null);
			end if;
		elsif	(:new.nr_seq_exame is not null) then
			select	max(ie_tipo_convenio)
			into	ie_tipo_convenio_w
			from	convenio
			where	cd_convenio	= :new.cd_convenio;

			cd_setor_w	:= :new.cd_setor_atendimento;
			
			if	(:new.nr_seq_material is not null) then			
				select	max(cd_material_exame)
				into	cd_material_exame_w
				from	material_exame_lab
				where	nr_sequencia	= :new.nr_seq_material;
			end if;
				
			obter_exame_lab_convenio(	:new.nr_seq_exame,
							:new.cd_convenio,
							:new.cd_categoria,
							ie_tipo_atendimento_w,
							cd_estabelecimento_w,
							ie_tipo_convenio_w,
							:new.nr_seq_proc_interno,
							cd_material_exame_w,
							cd_plano_w,
							cd_setor_w,
							cd_procedimento_w,
							ie_origem_proced_w,
							ds_erro_w,
							nr_seq_proc_interno_aux_w);
		end if;
	end if;

	if 	(nr_seq_proc_interno_aux_w > 0) then
		:new.nr_seq_proc_interno	:= nr_seq_proc_interno_aux_w;
	end if;
	
	if	(nvl(cd_procedimento_w,0) > 0) then
		:new.cd_procedimento	:= cd_procedimento_w;
		:new.ie_origem_proced	:= ie_origem_proced_w;
	end if;
end if;

if	(nvl(:old.nr_interno_conta,0)	<>  nvl(:new.nr_interno_conta,0)) and
	(obter_funcao_ativa <> 1116) then
	select	count(*)
	into	qt_registro_w
	from	auditoria_propaci a,
		auditoria_conta_paciente b
	where	b.nr_sequencia		= a.nr_seq_auditoria
	and	a.nr_seq_propaci	= :new.nr_sequencia
	and	b.dt_liberacao is null;

	if	(qt_registro_w	> 0) and
		(obter_valor_param_usuario(67,269,obter_perfil_ativo,:new.nm_usuario,0)	= 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(188913);
		/* Nao e possivel alterar a conta do procedimento, pois o mesmo se encontra em um periodo de auditoria! */
	end if;


	select	max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),
		max(substr(osuser,1,15))
	into	ds_module_w,
		osuser_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual)
	and 	upper(program) not like '%TASY%';

	/*if	(ds_module_w is not null) then
		insert into logxxxx_tasy (cd_log, dt_atualizacao, nm_usuario, ds_log)
			values (88890, sysdate, osuser_w, 'Procedimento mudando conta: ' ||
					:old.nr_interno_conta || '  para ' || :new.nr_interno_conta || '  ' || ds_module_w);
	end if;*/
end if;

/*Matheus */
/*if	(nvl(:old.nr_interno_conta,0)	<>  nvl(:new.nr_interno_conta,0)) and
	(nvl(:new.nr_lote_contabil,0) <> 0) then

	select	max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),
		max(substr(osuser,1,15))
	into	ds_module_w,
		osuser_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);

	ds_log_w	:= substr(dbms_utility.format_call_stack,1,1800);
	ds_log_w	:= substr(	ds_log_w || 'Procedimento mudando conta c/lote ctb: ' || 
					:old.nr_interno_conta || '  para ' || :new.nr_interno_conta || '  ' || 'L:' || :new.nr_lote_contabil || ' ' || ds_module_w,1,2000);
	insert into logxxxxx_tasy(cd_log, dt_atualizacao, nm_usuario, ds_log) 
	values (88919, sysdate, osuser_w, ds_log_w);
	
end if;*/

/*Fim alteracao Matheus*/

/*if	(:old.nr_atendimento	<>  :new.nr_atendimento) then
	select	max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),
		max(substr(osuser,1,15))
	into	ds_module_w,
		osuser_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual)
	and 	upper(program) not like '%TASY%';

	if	(ds_module_w is not null) then
		insert into logxxxxx_tasy (cd_log, dt_atualizacao, nm_usuario, ds_log)
			values (88891, sysdate, osuser_w, 'Procedimento mudando atendimento: ' ||
					:old.nr_atendimento || '  para ' || :new.nr_atendimento || '  ' || ds_module_w);
	end if;
end if;*/

if	(((:new.cd_cbo is not null) and (:old.cd_cbo is null)) or 
	((:new.cd_medico_executor <> :old.cd_medico_executor) or 
	(:new.cd_pessoa_fisica <> :old.cd_pessoa_fisica)) or
	(:new.cd_cbo is null)) and
	((:new.cd_medico_executor is not null) or (:new.cd_pessoa_fisica is not null)) and
	(:new.ie_origem_proced = 7)  then
	begin
	if	(ie_tipo_atendimento_w = 1) then
		begin
		select	max(ie_atualiza_cbo_medico)
		into	ie_atualiza_cbo_medico_w
		from	sus_procedimento
		where	cd_procedimento			= :new.cd_procedimento
		and	ie_origem_proced		= 7;
		
		if	(nvl(ie_atualiza_cbo_medico_w,'N') = 'S') then
			begin
			:new.cd_medico_executor 	:= null;
			:new.cd_pessoa_fisica		:= null;
			:new.cd_cbo			:= null;
			:new.ds_observacao		:= substr(:new.ds_observacao||'ie_atualiza_cbo_medico = S',1,255);
			end;
		else
			begin
			begin
			select	nvl(max(ie_codigo_autorizacao),0)
			into	ie_codigo_autorizacao_w
			from	sus_aih_unif
			where	nr_interno_conta	= :new.nr_interno_conta;
			exception
				when others then
				ie_codigo_autorizacao_w := 0;
				end;

			if	(nvl(sus_verificar_codigo_autor(nvl(ie_codigo_autorizacao_w,0),'CBO'),'N') = 'N') and
				(nvl(sus_obter_se_detalhe_proc(:new.cd_procedimento,:new.ie_origem_proced,'10039',:new.dt_procedimento),0) = 0) then /*PROCEDIMENTO QUE NAO PERMITE O PREENCHIMENTO DE CPF E CBO*/
				cd_cbo_w	:= substr(sus_obter_cbo_medico(	nvl(:new.cd_medico_executor, :new.cd_pessoa_fisica),
										:new.cd_procedimento,
										:new.dt_procedimento,
										null),1,6);
				:new.cd_cbo	:= cd_cbo_w;
			end if;
			end;
		end if;	
		end;
	else
		begin
		if	(obter_funcao_ativa = 1125) then
			begin
			if	(ie_altera_cbo_bpa_w = 'S') then
				begin
				cd_cbo_w	:= substr(sus_obter_cbo_medico(	nvl(:new.cd_medico_executor, :new.cd_pessoa_fisica),
										:new.cd_procedimento,
										:new.dt_procedimento,
										null),1,6);
				:new.cd_cbo	:= cd_cbo_w;
				end;
			elsif	(ie_altera_cbo_bpa_w = 'I') and
				(sus_obter_secbo_compativel(	nvl(:new.cd_medico_executor,
								:new.cd_pessoa_fisica),
								:new.cd_procedimento,
								:new.ie_origem_proced,
								:new.dt_procedimento,
								:new.cd_cbo,
								:new.ie_funcao_medico) = 'N') then
				begin
				cd_cbo_w	:= substr(sus_obter_cbo_medico(	nvl(:new.cd_medico_executor,
										:new.cd_pessoa_fisica),
										:new.cd_procedimento,
										:new.dt_procedimento,
										null),1,6);
										
				:new.cd_cbo	:= cd_cbo_w;
				end;
			end if;				
			end;
		else
			if (sus_obter_secbo_compativel(cd_profissional_p => nvl(:new.cd_medico_executor, :new.cd_pessoa_fisica),
				cd_procedimento_p => :new.cd_procedimento,
				ie_origem_proced_p => :new.ie_origem_proced,
				dt_procedimento_p => :new.dt_procedimento,
				cd_cbo_p => :new.cd_cbo,
				ie_funcao_p => :new.ie_funcao_medico) = 'N') then

				cd_cbo_w	:= substr(sus_obter_cbo_medico(	nvl(:new.cd_medico_executor, :new.cd_pessoa_fisica),
										:new.cd_procedimento,
										:new.dt_procedimento,
										null),1,6);
				:new.cd_cbo	:= cd_cbo_w;
			end if;
		end if;	
		end;	
	end if;
	end;
end if;

if	(:new.ie_origem_proced = 7) then
	begin	
	if	(:old.cd_procedimento <> :new.cd_procedimento) then
		begin
		:new.nr_seq_servico 		:= null;
		:new.nr_seq_servico_classif	:= null;
		end;
	end if;
	
	if	(ie_tipo_atendimento_w = 1) then
		begin
		if	(sus_obter_tiporeg_proc(:new.cd_procedimento,:new.ie_origem_proced,'C',2) in (3,4)) then
			begin
			if	(:new.nr_seq_servico is null) then

				:new.nr_seq_servico_classif := null;
				
				begin				
				nr_seq_servico_w	:= sus_obter_servico_proc(	:new.cd_procedimento,
											:new.ie_origem_proced,
											cd_estabelecimento_w,
											ie_tipo_atendimento_w);
				exception
				when others then
					nr_seq_servico_w	:= null;
				end;
				
				:new.nr_seq_servico := nr_seq_servico_w;
			else
				nr_seq_servico_w	:= :new.nr_seq_servico;
			end if;
			
			if	(:new.nr_seq_servico_classif is null) and
				(nr_seq_servico_w is not null) then
				begin
				nr_seq_servico_classif_w	:= sus_obter_classif_proc(	:new.cd_procedimento,
												:new.ie_origem_proced,
												nr_seq_servico_w,
												cd_estabelecimento_w,:new.dt_procedimento);
				exception
				when others then
					nr_seq_servico_classif_w	:= null;
				end;
				
				:new.nr_seq_servico_classif := nr_seq_servico_classif_w;
			end if;
			end;
		else
			:new.nr_seq_servico 		:= null;
			:new.nr_seq_servico_classif	:= null;
		end if;
		end;
	elsif	(ie_tipo_atendimento_w <> 1) then
		begin		
		if 	(sus_obter_tiporeg_proc(:new.cd_procedimento,:new.ie_origem_proced,'C',1) = 1) then
			begin	
			begin
			select	count(1)
			into	qt_proc_bpa_apac_w
			from	sus_procedimento a
			where	a.cd_procedimento	= :new.cd_procedimento
			and	a.ie_origem_proced	= :new.ie_origem_proced
			and	exists	(select  1
					from    sus_procedimento_registro x
					where   x.cd_procedimento	=  a.cd_procedimento
					and     x.ie_origem_proced	= a.ie_origem_proced
					and     x.cd_registro in (6,7))
			and	rownum = 1;			
			exception
			when others then
				qt_proc_bpa_apac_w	:= 0;
			end;
			
			if	(qt_proc_bpa_apac_w > 0) then
				begin
				begin
				select	count(1)
				into	qt_reg_apac_w
				from	sus_apac_unif
				where	nr_interno_conta	= nvl(:new.nr_interno_conta,0)
				and	rownum  = 1;
				exception
				when others then
					qt_reg_apac_w	:= 0;	
				end;
				
				if	(qt_reg_apac_w = 0) then
					begin
					select	count(1)
					into	qt_reg_apac_w
					from	sus_laudo_paciente
					where	nr_atendimento	= :new.nr_atendimento
					and	ie_classificacao in (11,12,13,14)
					and	rownum = 1;
					exception
					when others then
						qt_reg_apac_w	:= 0;
					end;			
				end if;
				
				if	(qt_reg_apac_w > 0) then
					begin
					if	(:new.nr_seq_servico is null) then
					
						:new.nr_seq_servico_classif := null;
						
						begin
						nr_seq_servico_w := sus_obter_servico_proc(	:new.cd_procedimento,
												:new.ie_origem_proced,
												cd_estabelecimento_w,
												ie_tipo_atendimento_w);
						exception
						when others then
							nr_seq_servico_w	:= null;
						end;
						
						:new.nr_seq_servico	:= nr_seq_servico_w;
					else
						nr_seq_servico_w	:= :new.nr_seq_servico;
					end if;
					
					if	(:new.nr_seq_servico_classif is null) and
						(nr_seq_servico_w is not null) then
						begin
						nr_seq_servico_classif_w	:= sus_obter_classif_proc(	:new.cd_procedimento,
														:new.ie_origem_proced,
														nr_seq_servico_w,
														cd_estabelecimento_w,:new.dt_procedimento);
						exception
						when others then
							nr_seq_servico_classif_w	:= null;
						end;
						
						:new.nr_seq_servico_classif	:= nr_seq_servico_classif_w;
					end if;							
					end;
				end if;
			
				end;
			else
				:new.nr_seq_servico 		:= null;
				:new.nr_seq_servico_classif	:= null;				
			end if;
			end;
		else
			begin
			if	(:new.nr_seq_servico is null) then
				
				:new.nr_seq_servico_classif := null;
				
				begin
				nr_seq_servico_w := sus_obter_servico_proc(	:new.cd_procedimento,
										:new.ie_origem_proced,
										cd_estabelecimento_w,
										ie_tipo_atendimento_w);
				exception
				when others then
					nr_seq_servico_w	:= null;
				end;
				
				:new.nr_seq_servico	:= nr_seq_servico_w;	
			else
				nr_seq_servico_w	:= :new.nr_seq_servico;
			end if;	
			
			if	(:new.nr_seq_servico_classif is null) and
				(nr_seq_servico_w is not null) then
				begin
				nr_seq_servico_classif_w	:= sus_obter_classif_proc(	:new.cd_procedimento,
												:new.ie_origem_proced,
												nr_seq_servico_w,
												cd_estabelecimento_w,:new.dt_procedimento);
				exception
				when others then
					nr_seq_servico_classif_w	:= null;
				end;

				:new.nr_seq_servico_classif	:= nr_seq_servico_classif_w;
			end if;	
			end;
		end if;		
		end;
	end if;		
	end;
end if;

if	(:old.cd_motivo_exc_conta is null) and
	(:new.cd_motivo_exc_conta is not null) then
	qt_lote_pre_fat_w	:= 0;
	
	if	(ie_excluir_proc_repasse_w = 'S') then
		select	count(1)
		into	qt_lote_pre_fat_w
		from	pre_fatur_conta	a
		where	a.nr_interno_conta	= :old.nr_interno_conta
		and	rownum			= 1;
		
		select	nvl(sum(a.vl_repasse),0)
		into	vl_repasse_pend_w
		from	procedimento_repasse	a
		where	a.nr_repasse_terceiro is not null
		and	a.nr_seq_procedimento	= :new.nr_sequencia;
	end if;
	
	select	nvl(sum(a.vl_repasse),0)
	into	vl_repasse_w
	from	procedimento_repasse	a
	where	a.nr_seq_procedimento	= :new.nr_sequencia;
	
	if	((ie_excluir_proc_repasse_w = 'N') or
		(vl_repasse_pend_w > 0) or
		(qt_lote_pre_fat_w = 0)) and
		(vl_repasse_w > 0) then

		wheb_mensagem_pck.exibir_mensagem_abort(188917,	'nr_sequencia=' || :new.nr_sequencia ||
								';nr_interno_conta=' || :new.nr_interno_conta ||
								';vl_repasse=' || vl_repasse_w ||
								';vl_repasse_pend=' || vl_repasse_pend_w ||
								';qt_lote_pre_fat=' || qt_lote_pre_fat_w);
		/* Sequencia: #@nr_sequencia#@
		Conta: #@nr_interno_conta#@
		Este procedimento possui repasse gerado!
		Nao e possivel excluir o procedimento.
		Valor do repasse: #@vl_repasse#@ */

	elsif	(ie_excluir_proc_repasse_w	= 'S') and
		(nvl(qt_lote_pre_fat_w,0)	> 0) then

		delete	from procedimento_repasse
		where	nr_seq_procedimento	= :new.nr_sequencia;

	end if;
	
	if	(:new.ie_origem_proced in ('11','15')) then --Somente 11-OPS,15-DRG interfere na DRG
		INATIVAR_DRG_EPISODIO_PACIENTE(:new.nr_atendimento, :new.cd_procedimento, 'PROCEDIMENTO_PACIENTE', :new.nm_usuario, 'TPP'); --Quando algum item excluido da conta, deve inativar a DRG se ja calculado
	end if;
	/*if 	(billing_i18n_pck.obter_se_calcula_drg = 'S') then		
		select 	max(a.nr_seq_episodio)
		into   	nr_seq_case_w
		from   	atendimento_paciente a,
			episodio_paciente_drg b
		where  	a.nr_atendimento 	= :new.nr_atendimento
		and	a.nr_seq_episodio 	= b.nr_seq_episodio_paciente
		and	b.ie_situacao 		= 'A';
		
		if	(nr_seq_case_w is not null) then		
		
			INATIVAR_EPISODIO_PACIENTE_DRG(nr_seq_case_w, :new.nr_sequencia,:new.nr_interno_conta, :new.nm_usuario);		
			
		end if;	
	end if;	*/
	
	
end if;

if	(:old.cd_motivo_exc_conta is null) and
	(:new.cd_motivo_exc_conta is not null) and 
	(ie_cancela_autor_exec_w = 'S')then
	
	select 	nvl(max(a.nr_sequencia),0)
	into 	nr_sequencia_autor_w
	from 	autorizacao_convenio a,
		estagio_autorizacao b
	where	a.nr_seq_estagio	= b.nr_sequencia
	and 	b.ie_interno		= '1' --Necessidade
	and	a.nr_sequencia		= (select	max(x.nr_sequencia_autor)	
					   from		procedimento_autorizado x
					   where		x.nr_sequencia	= :old.nr_seq_proc_autor);
	
	if	(nr_sequencia_autor_w > 0 ) then

		select 	nvl(sum(a.qt_solicitada),0)
		into 	qt_proc_autor_w
		from 	procedimento_autorizado a
		where	a.nr_sequencia_autor	= nr_sequencia_autor_w
		and 	a.cd_procedimento		= :old.cd_procedimento
		and 	a.ie_origem_proced	= :old.ie_origem_proced;	
		
					
		select 	count(*)
		into 	qt_autor_w
		from 	procedimento_autorizado a
		where	a.nr_sequencia_autor 	= nr_sequencia_autor_w
		and 	a.nr_sequencia = (select	x.nr_sequencia	
					from	procedimento_autorizado x
					where	a.nr_sequencia 	   = x.nr_sequencia
					and 	a.nr_sequencia_autor = x.nr_sequencia_autor
					and 	x.qt_solicitada > 0);
		

		select 	max(nr_sequencia)
		into 	nr_seq_estagio_w
		from 	estagio_autorizacao
		where	ie_interno		= '70'--Cancelado
		and 	nvl(ie_situacao,'A')	= 'A'
		and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
	
		if	(nr_seq_estagio_w is not null) then
			if	(qt_proc_autor_w = :old.qt_procedimento)then
				update	autorizacao_convenio
				set 	nr_seq_estagio	= nr_seq_estagio_w
				where	nr_sequencia	= nr_sequencia_autor_w;
			else
				update	procedimento_autorizado
				set 	qt_solicitada		= qt_solicitada - :old.qt_procedimento
				where	nr_sequencia_autor	= nr_sequencia_autor_w
				and 	nr_sequencia		= :old.nr_seq_proc_autor;
			end if;
		end if;
	end if;
	
end if;

if	(:new.ie_proc_princ_atend = 'S') then
	begin
	select	count(*)
	into	qt_regra_tipo_fatura_w
	from	fatur_tipo_fatura
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(qt_regra_tipo_fatura_w > 0) then
		select	substr(obter_tipo_fatura_procedimento(	:new.cd_procedimento,
								:new.ie_origem_proced,
								cd_estabelecimento_w,
								'S',
								ie_tipo_atendimento_w,
								:new.cd_setor_atendimento),1,10)
		into	nr_seq_tipo_fatura_w
		from	dual;

		if	(nr_seq_tipo_fatura_w > 0) then
			update	conta_paciente
			set	nr_seq_tipo_fatura 	= nr_seq_tipo_fatura_w
			where	nr_interno_conta 	= :new.nr_interno_conta;
		end if;
	end if;
	end;
end if;

if	(ie_consiste_fpo_w <> 'N') and
	(:new.ie_origem_proced	= 7) then
	begin
	begin
	select	nvl(dt_mesano_referencia,sysdate),
		nvl(nr_seq_protocolo,0)
	into	dt_referencia_w,
		nr_seq_protocolo_w
	from	conta_paciente
	where	nr_interno_conta 	= :new.nr_interno_conta;
	exception
	when others then
		dt_referencia_w 	:= sysdate;
		nr_seq_protocolo_w	:= 0;
	end;

	if	(nr_seq_protocolo_w <> 0) then
		begin
		sus_consiste_fpo_prot_unif(	dt_referencia_w,
						:new.cd_procedimento,
						:new.ie_origem_proced,
						:new.qt_procedimento,
						:new.nr_atendimento,
						cd_estabelecimento_w,
						ie_tipo_atendimento_w,
						:new.cd_cbo,
						ie_consiste_fpo_w,
						ds_retorno_fpo_w,
						:new.cd_setor_atendimento,
						nvl(cd_procedencia_w,0));
						
		if	(nvl(ds_retorno_fpo_w,'X') <> 'X') then
			wheb_mensagem_pck.exibir_mensagem_abort(188919,'ds_retorno_fpo='||ds_retorno_fpo_w);
		end if;
		end;
	end if;
	end;
end if;

/*
if	(:old.nr_interno_conta	is not null) and
	(:new.nr_interno_conta is null) and
	(:old.cd_motivo_exc_conta is null) and
	(:new.cd_motivo_exc_conta is not null) then

	select	max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),
		max(substr(osuser,1,15))
	into	ds_module_w,
		osuser_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);

	ds_log_w	:= substr(dbms_utility.format_call_stack,1,1800);
	ds_log_w	:= substr(	ds_log_w || 'Procedimento excluido: ' || :new.cd_procedimento || ' conta: ' || 
					:old.nr_interno_conta || ' Motivo: ' || :new.cd_motivo_exc_conta || ' ' || ds_module_w,1,2000);
	
	insert into log_tasy(
		cd_log,
		dt_atualizacao,
		nm_usuario,
		ds_log) 
	values(	88921,
		sysdate,
		osuser_w,
		ds_log_w);
	
end if;
*/
/*if	(:new.ie_origem_proced = 7) and
	(:new.cd_cbo is not null) and 
	(:new.cd_medico_executor is null) and
	(:new.cd_pessoa_fisica is null) then
	
	:new.cd_cbo := null;
	
	if	(ie_grava_log_med_w = 'S') and
		((nvl(:old.cd_medico_executor,'X') <> 'X') or
		(nvl(:old.cd_pessoa_fisica,'X') <> 'X')) then
		begin
		ds_log_w := '';
		
		select	max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),
			max(substr(osuser,1,15))
		into	ds_module_w,
			osuser_w
		from	v$session
		where	audsid = (select userenv('sessionid') from dual);
		
		ds_log_w := substr('Medico:'||:old.cd_medico_executor||' Prof:'||:old.cd_pessoa_fisica||' CBO:'||:old.cd_cbo||' '||ds_module_w,1,2000);
		
		insert into logxxxxxx_tasy(
			cd_log,
			dt_atualizacao,
			nm_usuario,
			ds_log) 
		values(	10,
			sysdate,
			osuser_w,
			ds_log_w);
		end;
	end if;
	
end if;*/

if	(nvl(:new.vl_procedimento, 0) <> nvl(:old.vl_procedimento, 0)) and
	(ie_log_valor_proc_w = 'S') then
	
	select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
	into	ds_module_log_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);
	
	insert into proc_pac_log
		(cd_funcao,
		cd_perfil,
		cd_setor_atendimento,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_entrada_unidade,
		ie_acao,
		ie_valor_informado,
		nm_usuario,
		nm_usuario_nrec,
		nr_atendimento,
		nr_cirurgia,
		nr_prescricao,
		nr_seq_atepacu,
		nr_seq_propaci,
		nr_sequencia,
		qt_procedimento,
		tx_procedimento,
		vl_custo_operacional,
		vl_medico,
		vl_procedimento,
		cd_procedimento,
		ie_origem_proced,
		ds_observacao,
		ds_call_stack,
		ds_module)
	values	(obter_funcao_ativa, 		
		obter_perfil_ativo, 
		:new.cd_setor_atendimento,
		sysdate,
		sysdate,
		:new.dt_entrada_unidade,
		'VA',
		:new.ie_valor_informado,
		:new.nm_usuario,
		:new.nm_usuario,
		:new.nr_atendimento,
		:new.nr_cirurgia,
		:new.nr_prescricao,
		:new.nr_seq_atepacu,
		:new.nr_sequencia,
		proc_pac_log_seq.nextval,
		:new.qt_procedimento,
		:new.tx_procedimento,
		:new.vl_custo_operacional,
		:new.vl_medico,              
		:new.vl_procedimento,
		:new.cd_procedimento,
		:new.ie_origem_proced,
		--'Valor anterior do procedimento: ' || campo_mascara_virgula(:old.vl_procedimento),   -- 298658
		WHEB_MENSAGEM_PCK.get_texto(298658)|| ': ' || campo_mascara_virgula(:old.vl_procedimento), 
		ds_call_stack_w,
		ds_module_log_w);
	
end if;

qt_pacote_orig_w := obter_qt_pacote_orig_pragma(:new.nr_sequencia, :new.nr_atendimento, 'T');

if	((qt_pacote_orig_w > 0) or
	(:new.nr_seq_proc_pacote is not null)) and
	(:new.cd_motivo_exc_conta is not null) then
	
	select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
	into	ds_module_log_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);
	
	insert into proc_pac_log
		(cd_funcao,
		cd_perfil,
		cd_setor_atendimento,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_entrada_unidade,
		ie_acao,
		ie_valor_informado,
		nm_usuario,
		nm_usuario_nrec,
		nr_atendimento,
		nr_cirurgia,
		nr_prescricao,
		nr_seq_atepacu,
		nr_seq_propaci,
		nr_sequencia,
		qt_procedimento,
		tx_procedimento,
		vl_custo_operacional,
		vl_medico,
		vl_procedimento,
		cd_procedimento,
		ie_origem_proced,
		ds_observacao,
		ds_call_stack,
		ds_module)
	values	(obter_funcao_ativa,
		obter_perfil_ativo, 
		:new.cd_setor_atendimento,
		sysdate,
		sysdate,
		:new.dt_entrada_unidade,
		'P',
		:new.ie_valor_informado,
		:new.nm_usuario,
		:new.nm_usuario,
		:new.nr_atendimento,
		:new.nr_cirurgia,
		:new.nr_prescricao,
		:new.nr_seq_atepacu,
		:new.nr_sequencia,
		proc_pac_log_seq.nextval,
		:new.qt_procedimento,
		:new.tx_procedimento,
		:new.vl_custo_operacional,
		:new.vl_medico,              
		:new.vl_procedimento,
		:new.cd_procedimento,
		:new.ie_origem_proced,
		--'Procedimento origem do pacote excluido da conta.',
		WHEB_MENSAGEM_PCK.get_texto(298660),
		ds_call_stack_w,
		ds_module_log_w);
		
end if;

-- OS 465559
select	max(dt_geracao_resumo)
into	dt_geracao_resumo_new_w
from 	conta_paciente
where 	nr_interno_conta	= nvl(:new.nr_interno_conta,0);

select	max(dt_geracao_resumo)
into	dt_geracao_resumo_old_w
from 	conta_paciente
where 	nr_interno_conta	= nvl(:old.nr_interno_conta,0);

if	(dt_geracao_resumo_new_w is not null) then
	update	conta_paciente
	set 	dt_geracao_resumo	= null
	where 	nr_interno_conta	= :new.nr_interno_conta;		
end if;	

if	(dt_geracao_resumo_old_w is not null) then
	update	conta_paciente
	set 	dt_geracao_resumo	= null
	where 	nr_interno_conta	= :old.nr_interno_conta;		
end if;

begin
ie_ajusta_exec_consulta_bpa_w	:= nvl(obter_valor_param_usuario(1125, 149, obter_perfil_ativo, :new.nm_usuario, 0),'N');
exception
when others then
	ie_ajusta_exec_consulta_bpa_w	:= 'N';
end;

if	(ie_ajusta_exec_consulta_bpa_w = 'S') and
	(ie_tipo_atendimento_w <> 1) and
	(:new.cd_procedimento = 301010048) and
	(:new.cd_medico_executor is null) and
	(:new.cd_pessoa_fisica is not null) then
	begin
	:new.cd_medico_executor	:= :new.cd_pessoa_fisica;
	:new.cd_pessoa_fisica	:= '';
	end;
end if;

if	(nvl(:new.cd_medico_executor,0) <> nvl(:old.cd_medico_executor,0)) and
	(ie_log_medico_exec_proc_w = 'S') then
	
	select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
	into	ds_module_log_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);
	
	insert into proc_pac_log
		(cd_funcao,
		cd_perfil,
		cd_setor_atendimento,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_entrada_unidade,
		ie_acao,
		ie_valor_informado,
		nm_usuario,
		nm_usuario_nrec,
		nr_atendimento,
		nr_cirurgia,
		nr_prescricao,
		nr_seq_atepacu,
		nr_seq_propaci,
		nr_sequencia,
		qt_procedimento,
		tx_procedimento,
		vl_custo_operacional,
		vl_medico,
		vl_procedimento,
		cd_procedimento,
		ie_origem_proced,
		ds_observacao,
		ds_call_stack,
		ds_module,
                cd_medico_executor)
	values	(obter_funcao_ativa, 
		obter_perfil_ativo, 
		:new.cd_setor_atendimento,
		sysdate,
		sysdate,
		:new.dt_entrada_unidade,
		'ME',
		:new.ie_valor_informado,
		:new.nm_usuario,
		:new.nm_usuario,
		:new.nr_atendimento,
		:new.nr_cirurgia,
		:new.nr_prescricao,
		:new.nr_seq_atepacu,
		:new.nr_sequencia,
		proc_pac_log_seq.nextval,
		:new.qt_procedimento,
		:new.tx_procedimento,
		:new.vl_custo_operacional,
		:new.vl_medico,              
		:new.vl_procedimento,
		:new.cd_procedimento,
		:new.ie_origem_proced,
		--'Valor anterior do medico executor: ' 
		WHEB_MENSAGEM_PCK.get_texto(444193)|| ': ' || campo_mascara_virgula(:old.cd_medico_executor),
		ds_call_stack_w,
		ds_module_log_w,
                :new.cd_medico_executor);
	
end if;


<<final>>

qt_reg_w	:= 0;

end;
/