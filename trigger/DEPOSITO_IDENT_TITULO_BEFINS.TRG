create or replace trigger deposito_ident_titulo_befins
before insert on deposito_ident_titulo
for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ X ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

declare
nr_identificacao_w		varchar2(30);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	max(a.nr_identificacao)
into	nr_identificacao_w
from	deposito_identificado	a
where	a.nr_sequencia	= :new.nr_seq_deposito;

if	(nr_identificacao_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(225746);
end if;

end if;

end;
/