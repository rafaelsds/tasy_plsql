create or replace trigger km_questionario_item_before
before update on km_questionario_item
for each row

declare

ds_variavel_w	varchar2(4000);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	begin
	if	(phi_is_base_philips = 'S') then
		begin
		if	(nvl(:old.cd_exp_item, -1) <> nvl(:new.cd_exp_item, -1)) then
			begin
			:new.nr_seq_item_origem := null;
			end;
		elsif	((:old.cd_exp_item is null and :new.cd_exp_item is null) and
			nvl(:old.ds_item, 'X') <> nvl(:new.ds_item, 'X')) then
			begin
			:new.nr_seq_item_origem := null;
			end;
		end if;
		end;
	end if;
	end;
end if;

end;
/