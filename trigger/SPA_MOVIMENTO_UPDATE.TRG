create or replace trigger spa_movimento_update
before update on spa_movimento
for each row

declare

cd_convenio_w	number(5,0);

begin

if	(:new.cd_classificacao = 1) then
	
	select	max(obter_convenio_atendimento(:new.nr_atendimento))
	into	cd_convenio_w
	from	dual;
	
elsif	(:new.cd_classificacao = 2) then
	
	select	max(cd_convenio_parametro)
	into	cd_convenio_w
	from	conta_paciente
	where	nr_interno_conta = :new.nr_interno_conta;
	
elsif	(:new.cd_classificacao = 3) then

	select	max(obter_convenio_nf(nr_sequencia))
	into	cd_convenio_w
	from	nota_fiscal
	where	nr_sequencia = :new.nr_seq_nf;

elsif	(:new.cd_classificacao = 4) then

	select	max(Obter_Convenio_Tit_Rec(nr_titulo))
	into	cd_convenio_w
	from	titulo_receber
	where	nr_titulo = :new.nr_titulo;
	
elsif	(:new.cd_classificacao = 5) then

	select	max(cd_convenio)
	into	cd_convenio_w
	from	protocolo_convenio
	where	nr_seq_protocolo = :new.nr_seq_protocolo;
	
end if;

if	(nvl(cd_convenio_w,0) <> 0) and
	(nvl(:new.cd_convenio,0) <> nvl(cd_convenio_w,0)) then
	:new.cd_convenio	:= cd_convenio_w;
end if;

end;
/