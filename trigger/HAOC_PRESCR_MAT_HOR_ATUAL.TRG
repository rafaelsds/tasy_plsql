CREATE OR REPLACE 
TRIGGER HAOC_Prescr_mat_hor_atual
BEFORE INSERT OR UPDATE ON Prescr_mat_hor
FOR EACH ROW

BEGIN

update	HAOC_PRESCR_MEDIC_HOR
set	dt_suspensao		= :new.dt_suspensao
where	nr_seq_prescr_hor	= :new.nr_sequencia;

END;
/