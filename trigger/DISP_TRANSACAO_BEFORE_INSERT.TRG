CREATE OR REPLACE TRIGGER disp_transacao_before_insert
before insert on dispensario_transacao
for each row

BEGIN

:new.ie_lido	    := 'S';
:NEW.dt_atualizacao := SYSDATE;

END;
/