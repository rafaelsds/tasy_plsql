create or replace trigger parametro_estoque_atual
after update or insert on parametro_estoque
for each row

declare
qt_existe_w		number(10);
qt_estoque_lote_w	number(10);
begin
select	count(*)
into	qt_existe_w
from	regra_valorizacao_estoque
where	cd_estabelecimento = :new.cd_estabelecimento;

if	(nvl(:old.ie_estoque_lote,'N') = 'S') and
	(:new.ie_estoque_lote = 'N') then
	select	count(*)
	into	qt_estoque_lote_w
	from	material_estab
	where	cd_estabelecimento = :new.cd_estabelecimento
	and	ie_estoque_lote = 'S';
	
	if	(qt_estoque_lote_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(263046);
	end if;
end if;

if	((qt_existe_w = 0) and (:new.ie_metodo_valorizacao = 'MPM')) or 
	(:old.ie_metodo_valorizacao <> :new.ie_metodo_valorizacao) then
	begin
	update	regra_valorizacao_estoque
	set	ie_metodo_valorizacao = :new.ie_metodo_valorizacao,
		nm_usuario = :new.nm_usuario,
		dt_atualizacao = :new.dt_atualizacao
	where	dt_mes_inicio_vigencia = trunc(sysdate,'mm')
	and	cd_estabelecimento = :new.cd_estabelecimento;
	if	(sql%notfound) then
		insert into regra_valorizacao_estoque(
			cd_estabelecimento,
			dt_mes_inicio_vigencia,
			ie_metodo_valorizacao,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec)
		values (	:new.cd_estabelecimento,
			trunc(sysdate,'mm'),
			:new.ie_metodo_valorizacao,
			sysdate,
			sysdate,
			:new.nm_usuario,
			:new.nm_usuario);
	end if;
	end;
end if;
end;
/