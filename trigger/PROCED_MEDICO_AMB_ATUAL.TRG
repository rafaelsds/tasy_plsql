CREATE OR REPLACE TRIGGER Proced_Medico_Amb_Atual
BEFORE Insert or update ON Proced_Medico_Ambulatorial
FOR EACH ROW
DECLARE

ie_situacao_w	varchar2(1);

begin

select	nvl(max(ie_situacao),'A')
into	ie_situacao_w
from	procedimento
where 	cd_procedimento = :new.cd_procedimento
and	ie_origem_proced = :new.ie_origem_proced ;

if	(ie_situacao_w <> 'A')  then
	Wheb_mensagem_pck.exibir_mensagem_abort(279413);
end if;
END;
/