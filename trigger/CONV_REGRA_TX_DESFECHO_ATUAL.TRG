create or replace trigger conv_regra_tx_desfecho_atual
before insert or update on conv_regra_tx_desfecho
for each row

declare

qt_preenchido_w		number(10) := 0;

begin

if	(:new.cd_classe_material is not null) then
	qt_preenchido_w	:= qt_preenchido_w + 1;
end if;

if	(:new.cd_subgrupo_material is not null)  then
	qt_preenchido_w	:= qt_preenchido_w + 1;
end if;

if	(:new.cd_grupo_material is not null) then
	qt_preenchido_w	:= qt_preenchido_w + 1;
end if;

if	(:new.cd_material is not null) then
	qt_preenchido_w	:= qt_preenchido_w + 1;
end if;

if	(qt_preenchido_w > 1) then
	-- Apenas uma estrutura do material deve ser selecionada!
	Wheb_mensagem_pck.exibir_mensagem_abort(232683);
elsif	(qt_preenchido_w = 0) then
	-- Uma estrutura do material deve ser selecionada!
	Wheb_mensagem_pck.exibir_mensagem_abort(232684);
end if;

end;
/