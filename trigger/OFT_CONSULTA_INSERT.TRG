CREATE OR REPLACE TRIGGER oft_consulta_insert
after INSERT ON oft_consulta
FOR EACH ROW

declare

nr_sequencia_w 	number(10);
ie_atualiza_triagem_w   varchar2(1);
cd_perfil_w          perfil.cd_perfil%type                     := wheb_usuario_pck.get_cd_perfil;
nm_usuario_w         usuario.nm_usuario%type                   := wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w estabelecimento.cd_estabelecimento%type   := wheb_usuario_pck.get_cd_estabelecimento;

begin
obter_param_usuario(3010, 150, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_atualiza_triagem_w);

begin
if (ie_atualiza_triagem_w = 'S') and 
   (:new.nr_atendimento is not null) then
   update   atendimento_paciente
   set      dt_inicio_atendimento   = :new.dt_consulta,
            dt_fim_triagem          = :new.dt_consulta
   where    nr_atendimento          = :new.nr_atendimento
   and      dt_inicio_atendimento   is null
   and      dt_fim_triagem          is null;
end if;   
exception
when others then
	null;
end;

select	oft_log_status_consulta_seq.nextval
into	nr_sequencia_w
from 	dual;

if (nvl(:new.nr_seq_status,0) > 0) then
	insert into oft_log_status_consulta(
					nr_sequencia,
					nr_seq_status,
					nr_seq_consulta,
					dt_atualizacao,
					nm_usuario)
	values 			(nr_sequencia_w,
					:new.nr_seq_status,
					:new.nr_sequencia, --antes era :old.nr_sequencia
					sysdate, 
					:new.nm_usuario);
		
	insert into	oft_status_consulta_hist(
					nr_sequencia,
					nr_seq_consulta,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_status,
					dt_status)
	values			(oft_status_consulta_hist_seq.nextval,
					:new.nr_sequencia,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					:new.nr_seq_status,
					sysdate);	
end if;					

end;
/