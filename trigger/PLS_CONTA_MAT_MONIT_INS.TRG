create or replace trigger pls_conta_mat_monit_ins
before insert on pls_conta_mat
for each row

declare

qt_reg_monit_w		pls_integer;

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') and (:new.ie_status <> 'M') then
	select	count(1)
	into	qt_reg_monit_w
	from	pls_monitor_tiss_guia
	where	nr_seq_conta = :new.nr_seq_conta;

	if	(qt_reg_monit_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(819672, 'CONTA=' || :new.nr_seq_conta);
	end if;
end if;

end;
/