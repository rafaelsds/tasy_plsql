CREATE OR REPLACE TRIGGER ESCALA_DAR_ATUAL
BEFORE INSERT OR UPDATE ON ESCALA_DAR
FOR EACH ROW 
declare
qt_reg_w	number(1);
BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:new.qt_pontos	:= 	nvl(:new.IE_RAIVA,0) +
			nvl(:new.IE_FURIOSO,0) +
			nvl(:new.IE_CONT_RAIVA,0) +        
			nvl(:new.IE_BATER,0) +
			nvl(:new.IE_JUNTO,0);

<<Final>>
qt_reg_w	:= 0;
			
END;
/
