create or replace
trigger san_derivado_insert_update
before insert or update on SAN_DERIVADO
for each row
declare

begin

if	(:new.CD_PROCEDIMENTO is null) and
	(:new.NR_SEQ_PROC_INTERNO is null) then
	exibir_erro_abortar(obter_desc_expressao(937263) || chr(13) || chr(10),null);
end if;

end;
/