create or replace 
trigger funcao_param_usuario_dt_param
after INSERT OR DELETE OR UPDATE ON Funcao_param_usuario
for each row

declare
	ie_consistir_valor_w	varchar2(1);
	vl_parametro_padrao_w	varchar2(255);
begin

update	funcao
set	dt_atualizacao_param = sysdate
where	cd_funcao = nvl(:new.cd_funcao,:old.cd_funcao);

if (inserting or updating) then
	select	ie_consistir_valor,
		vl_parametro_padrao
	into	ie_consistir_valor_w,
		vl_parametro_padrao_w
	from	funcao_parametro
	where	cd_funcao = nvl(:new.cd_funcao, :old.cd_funcao)
	and	nr_sequencia = nvl(:new.nr_sequencia, :old.nr_sequencia);	

	if	(ie_consistir_valor_w = 'S') and		
		(somente_numero(:new.vl_parametro) < somente_numero(vl_parametro_padrao_w)) then	
		wheb_mensagem_pck.exibir_mensagem_abort(55208);
	end if;
end if;

end;
/
