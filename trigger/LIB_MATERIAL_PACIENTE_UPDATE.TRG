CREATE OR REPLACE TRIGGER Lib_Material_Paciente_Update
BEFORE UPDATE ON Lib_Material_Paciente
FOR EACH ROW

declare
nr_sequencia_w	number(10);

BEGIN

select	lib_material_paciente_hist_seq.NextVal
into	nr_sequencia_w
from	dual;	

if	(:new.dt_suspenso is not null)	and
    	(:old.dt_suspenso is null)		then
	insert into lib_material_paciente_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica,
		cd_material,
		dt_acao,
		cd_pessoa_acao,
		ie_tipo_acao,
		qt_dias_tratamento,
		dt_inicio_validade)
	values(
		nr_sequencia_w,
		sysdate,
		:new.nm_usuario,
		:new.cd_pessoa_fisica,
		:new.cd_material,
		sysdate,
		substr(obter_dados_usuario_opcao(:new.nm_usuario,'C'),1,100),
		'S',
		:new.qt_dias_tratamento,
		:new.dt_inicio_validade);

elsif	(:new.dt_suspenso is null)		and
    	(:old.dt_suspenso is not null)	then
	insert into lib_material_paciente_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica,
		cd_material,
		dt_acao,
		cd_pessoa_acao,
		ie_tipo_acao,
		qt_dias_tratamento,
		dt_inicio_validade)
	values(
		nr_sequencia_w,
		sysdate,
		:new.nm_usuario,
		:new.cd_pessoa_fisica,
		:new.cd_material,
		sysdate,
		substr(obter_dados_usuario_opcao(:new.nm_usuario,'C'),1,100),
		'L',
		:new.qt_dias_tratamento,
		:new.dt_inicio_validade);

elsif	(:new.qt_dias_tratamento <> :old.qt_dias_tratamento)	then
	insert into lib_material_paciente_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica,
		cd_material,
		dt_acao,
		cd_pessoa_acao,
		ie_tipo_acao,
		qt_dias_tratamento,
		dt_inicio_validade)
	values(
		nr_sequencia_w,
		sysdate,
		:new.nm_usuario,
		:new.cd_pessoa_fisica,
		:new.cd_material,
		sysdate,
		substr(obter_dados_usuario_opcao(:new.nm_usuario,'C'),1,100),
		'A',
		:new.qt_dias_tratamento,
		:new.dt_inicio_validade);
end if;

if	(:new.dt_liberacao is not null) then
	update	prescr_material
	set	QT_TOTAL_DIAS_LIB 	= :new.QT_DIAS_TRATAMENTO
	where	nr_prescricao		= :new.NR_PRESCRICAO
	and	CD_MATERIAL		= :new.CD_MATERIAL;
end if;

END;
/
