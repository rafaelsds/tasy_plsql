CREATE OR REPLACE TRIGGER agenda_hc_paciente_AfterPost
AFTER INSERT or update ON agenda_hc_paciente
FOR EACH ROW
DECLARE

ie_gerar_solic_pront_w	varchar2(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then


	select 	max(ie_gerar_solic_pront)
	into 	ie_gerar_solic_pront_w
	from 	agenda_home_care
	where 	nr_sequencia = :new.nr_seq_agenda;


	if (ie_gerar_solic_pront_w = 'S') and
	   ((:old.cd_pessoa_fisica is null) or
	   (:old.cd_pessoa_fisica <> :new.cd_pessoa_fisica)) then	
	   hc_Gerar_Solic_Pront_Agenda(:new.cd_pessoa_fisica, 
				       :new.nr_seq_agenda, 
				       :new.nr_sequencia, 
				       :new.dt_agenda, 
				       :new.nm_usuario
				      );
	end if;
end if;
END;
/
