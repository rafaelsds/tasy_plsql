create or replace trigger movto_trans_financ_update
  before update on movto_trans_financ  
  for each row
declare
  -- local variables here
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:new.dt_transacao <> :old.dt_transacao) then
		/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_transacao);
	end if;
end if;
  
end movto_trans_financ_update;
/