create or replace trigger rxt_integracao_acesso_befinsup
before insert or update on rxt_integracao_acesso
for each row

begin

if 	(:new.cd_perfil		is null) and
	(:new.nm_usuario_acesso	is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(389692);
end if;

end;
/