create or replace 
trigger HRP_PESSOA_JURIDICA_ATUAL
after insert on Pessoa_Juridica
for each row

declare
nr_seq_terceiro_w	number(10,0);
qt_reg_w		number(10,0);

begin

select	count(*)
into	qt_reg_w
from	controle_pessoa
where	cd_cgc = :new.cd_cgc
and	cd_tipo_controle = 9;

if (qt_reg_w = 0) then

	insert into controle_pessoa (nr_sequencia,
				cd_tipo_controle,
				dt_atualizacao,
				nm_usuario,
				cd_cgc,
				ie_situacao)
			values	(controle_pessoa_seq.nextval,
				9,
				sysdate,
				:new.nm_usuario,
				:new.cd_cgc,
				'A');
end if;

end;
/

