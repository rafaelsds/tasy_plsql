create or replace trigger disp_material_delete
before delete on material
for each row
declare
qt_registro_w		number(5);
begin

select	count(*)
into	qt_registro_w
from	dispensario_liberacao_local
where	cd_material = :old.cd_material;

if	(qt_registro_w > 0) then
	insert into dispensario_material(
		nr_sequencia,
		cd_estabelecimento,
		cd_material,
		ds_material,
		ie_status,
		dt_exclusao,
		dt_registro,
		dt_atualizacao,
		ie_lido)
	values(	disp_material_SEQ.nextval,
		wheb_usuario_pck.get_cd_estabelecimento,
		:old.cd_material,
		:old.ds_material,
		:old.ie_situacao,
		sysdate,
		sysdate,
		sysdate,
		'');
end if;
end disp_material_delete;
/