create or replace trigger DIC_OBJETO_sche
  after insert or delete or update on DIC_OBJETO
  for each row
begin
  schematics_alt_pck.prc_dic_objeto(cd_funcao_p => nvl(:new.cd_funcao,
                                                       :old.cd_funcao));
end;
/
