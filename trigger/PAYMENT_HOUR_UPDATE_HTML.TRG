create or replace TRIGGER PAYMENT_HOUR_UPDATE_HTML BEFORE
  INSERT OR
  UPDATE ON TITULO_PAGAR_ESCRIT FOR EACH ROW DECLARE cd_estabelecimento_w estabelecimento.cd_estabelecimento%type;
  nm_usuario_w usuario.nm_usuario%type;
  BEGIN
    /*rotina específica para a plataforma HTML5 */
    /* campos ds_horarios date (para utilização do dateTimePickerr) em HTML x ds_horarios String em Java*/
    IF (:new.hr_pagamento_html5 IS NOT NULL AND (:new.hr_pagamento_html5 <> :old.hr_pagamento_html5 OR :old.hr_pagamento_html5 IS NULL)) THEN
      BEGIN
        cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
        nm_usuario_w         := wheb_usuario_pck.get_nm_usuario;
       :new.hr_pagamento    := REPLACE(SUBSTR(TO_CHAR(:new.hr_pagamento_html5, pkg_date_formaters.localize_mask('shortTime', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_w))),1,5),':','');
      END;
    END IF;
  END;

/