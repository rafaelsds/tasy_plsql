CREATE OR REPLACE TRIGGER man_ordem_serv_tecnico_after
AFTER INSERT ON man_ordem_serv_tecnico
FOR EACH ROW
DECLARE

nr_seq_estagio_w		number(10);
nr_grupo_trabalho_w	number(10);

BEGIN
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
	goto Final;
end if;

if	(nvl(:new.nr_seq_tipo, 0) > 0) then
	begin
	select	nvl(max(nr_seq_estagio),0),
		nvl(max(nr_grupo_trabalho_atualiz),0)
	into	nr_seq_estagio_w,
		nr_grupo_trabalho_w
	from	man_tipo_hist
	where	nr_sequencia	= :new.nr_seq_tipo;
	
	if	(nr_seq_estagio_w > 0) then
		update	man_ordem_servico
		set	nr_seq_estagio	= nr_seq_estagio_w,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= :new.dt_atualizacao
		where	nr_sequencia	= :new.nr_seq_ordem_serv;
	end if;
	if	(nr_grupo_trabalho_w > 0) then
		update	man_ordem_servico
		set	nr_grupo_trabalho		= nr_grupo_trabalho_w,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= :new.dt_atualizacao
		where	nr_sequencia		= :new.nr_seq_ordem_serv;
	end if; 
	end;
end if;
<<Final>>
null;
END;
/