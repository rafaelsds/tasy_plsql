create or replace trigger hcm_agenda_int_insert
before insert on agenda_integrada
for each row
declare

cd_perfil_w			perfil.cd_perfil%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_tipo_atendimento_w		agenda_integrada.ie_tipo_atendimento%type;
nr_seq_tipo_classif_pac_w	agenda_integrada.nr_seq_tipo_classif_pac%type;
cd_procedencia_w		agenda_integrada.cd_procedencia%type;
ie_tipo_acomod_w		agenda_integrada.ie_tipo_acomod%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

   cd_perfil_w := obter_perfil_ativo;
   cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
   
   /*IE_TIPO_ATENDIMENTO*/
   if (:new.ie_tipo_atendimento is null) then
	select  max(obter_regra_atributo(cd_estabelecimento_w, cd_perfil_w,:new.nm_usuario, 'AGENDA_INTEGRADA', 'IE_TIPO_ATENDIMENTO', 'V', '', 0, 12744, 869, null, 0))
	into	ie_tipo_atendimento_w
	from 	dual;
	
	if (ie_tipo_atendimento_w is not null) then
	    :new.ie_tipo_atendimento := ie_tipo_atendimento_w;
	end if;
   end if;
   
    /*NR_SEQ_TIPO_CLASSIF_PAC*/
   if (:new.nr_seq_tipo_classif_pac is null) then
	select  max(obter_regra_atributo(cd_estabelecimento_w, cd_perfil_w,:new.nm_usuario, 'AGENDA_INTEGRADA', 'NR_SEQ_TIPO_CLASSIF_PAC', 'V', '', 0, 12744, 869, null, 0))
	into	nr_seq_tipo_classif_pac_w
	from 	dual;
	
	if (nr_seq_tipo_classif_pac_w is not null) then
	    :new.nr_seq_tipo_classif_pac := nr_seq_tipo_classif_pac_w;
	end if;
   end if;
   
    /*CD_PROCEDENCIA*/
   if (:new.cd_procedencia is null) then
	select  max(obter_regra_atributo(cd_estabelecimento_w, cd_perfil_w,:new.nm_usuario, 'AGENDA_INTEGRADA', 'CD_PROCEDENCIA', 'V', '', 0, 12744, 869, null, 0))
	into	cd_procedencia_w
	from 	dual;
	
	if (cd_procedencia_w is not null) then
	    :new.cd_procedencia := cd_procedencia_w;
	end if;
   end if;
   
    /*IE_TIPO_ACOMOD*/
   if (:new.ie_tipo_acomod is null) then
	select  max(obter_regra_atributo(cd_estabelecimento_w, cd_perfil_w,:new.nm_usuario, 'AGENDA_INTEGRADA', 'IE_TIPO_ACOMOD', 'V', '', 0, 12744, 869, null, 0))
	into	ie_tipo_acomod_w
	from 	dual;
	
	if (ie_tipo_acomod_w is not null) then
	    :new.ie_tipo_acomod := ie_tipo_acomod_w;
	end if;
   end if;
   
end if;	
					
end;
/