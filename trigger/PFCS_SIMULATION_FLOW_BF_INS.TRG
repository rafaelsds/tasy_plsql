CREATE OR REPLACE TRIGGER PFCS_SIMULATION_FLOW_BF_INS
BEFORE INSERT ON PFCS_SIMULATION_FLOW
FOR EACH ROW

DECLARE
qt_reg_w number(1);

BEGIN
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
        goto Final;
    end if;

    delete from pfcs_simulation_flow
    where nr_hour = :new.nr_hour
        and dt_simulation = :new.dt_simulation
        and cd_unit = :new.cd_unit
        and nr_seq_organization = :new.nr_seq_organization;

<<Final>>
qt_reg_w := 0;
END;
/