create or replace trigger pls_intercambio_insert
before insert or update on pls_intercambio
for each row

declare

nr_seq_congenere_w		number(10);
nr_seq_oper_congenere_w		number(10);

begin

if	(:new.nr_seq_congenere is null) and
	(:new.nr_seq_oper_congenere is null) and
	(:new.ie_tipo_contrato <> 'S') then --aaschlote 08/02/2011 - Permitir cadastrar tipos de contrato por obito sem informar a cooperativa ou o congenere
	wheb_mensagem_pck.exibir_mensagem_abort( 267255, null ); /* Deve ser informada a cooperativa ou operadora congênere para o intercâmbio. Favor verifique! */
elsif	(:new.nr_seq_congenere is not null) and
	(:new.nr_seq_oper_congenere is not null) and
	(:new.ie_tipo_contrato <> 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort( 267256, null ); /* Apenas um dos campos: Cooperativa ou Operadoras congêneres pode ser preenchido! */
end if;


end pls_intercambio_insert;
/