create or replace trigger escala_japan_coma_atual

before insert or update on escala_japan_coma

for each row

declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	:new.qt_score :=  nvl(:new.IE_DIGIT_GRADE_1,0)  +  nvl(:new.IE_DIGIT_GRADE_2,0) +  nvl(:new.IE_DIGIT_GRADE_3,0) ;

end if;

end;

/
