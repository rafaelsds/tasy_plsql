create or replace trigger ESCALA_FUJINAGA_ATUAL
before update or insert on ESCALA_FUJINAGA
for each row
declare
    EXEC_W               varchar2(300);
    ie_sinal_estresse_w	 number(10);
    qt_pontuacao_w       number; 
    qt_reg_w             number(1);
begin
    if (wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
      goto Final;
    end if;

    begin
        EXEC_W := 'CALL OBTER_ESTRES_E_FUJI_ATU_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10) INTO :ie_sinal_estresse_w';
        
        EXECUTE IMMEDIATE EXEC_W USING  IN nvl(:new.IE_VAR_TONUS,0), 
                                        IN nvl(:new.IE_VAR_POSTURA,0), 
                                        IN nvl(:new.IE_VAR_COLORACAO,0), 
                                        IN nvl(:new.IE_BAT_ASA_NASAL,0), 
                                        IN nvl(:new.IE_TIRAGEM,0), 
                                        IN nvl(:new.IE_APNEIA,0), 
                                        IN nvl(:new.IE_ACOMULO_SALIVA,0), 
                                        IN nvl(:new.IE_TREMORES_LINGUA,0), 
                                        IN nvl(:new.IE_SOLUCO,0), 
                                        IN nvl(:new.IE_CHORO,0), 
                                        OUT ie_sinal_estresse_w;
    exception
        when others then
            ie_sinal_estresse_w := null;
    end;
    
    :new.IE_SINAL_ESTRESSE	:= ie_sinal_estresse_w;

    begin
        EXEC_W := 'CALL OBTER_PTO_TOT_ESC_FUJ_ATU_MD(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10,
                                                     :11,:12,:13,:14,:15,:16,:17,:18) INTO :qt_pontuacao_w';
        
        EXECUTE IMMEDIATE EXEC_W USING  IN nvl(:new.IE_IDADE_CORRIGIDA,0), 
                                        IN nvl(:new.IE_ESTADO_CONSCIENCIA,0), 
                                        IN nvl(:new.IE_POSTURA_GLOBAL,0), 
                                        IN nvl(:new.IE_TONUS_GLOBAL,0),
                                        IN nvl(:new.IE_POSTURA_LABIO,0),
                                        IN nvl(:new.IE_POSTURA_LINGUA,0),
                                        IN nvl(:new.IE_REFLEXO_PROCURA,0),
                                        IN nvl(:new.IE_REFLEXO_SUCCAO,0),
                                        IN nvl(:new.IE_REFLEXO_MORDIDA,0),
                                        IN nvl(:new.IE_REFLEXO_VOMITO,0),
                                        IN nvl(:new.IE_MOV_LINGUA,0),
                                        IN nvl(:new.IE_CANOLAMENTO_LINGUA,0),
                                        IN nvl(:new.IE_MOV_MANDIBULA,0),
                                        IN nvl(:new.IE_FORCA_SUCCAO,0),
                                        IN nvl(:new.IE_SUCCAO_PAUSA,0),
                                        IN nvl(:new.IE_MANUT_RITMO,0),
                                        IN nvl(:new.IE_MANUT_ALERTA,0),
                                        IN nvl(ie_sinal_estresse_w,0),
                                        OUT qt_pontuacao_w;
    exception
        when others then
             qt_pontuacao_w := null;
    end;

    :new.QT_PONTUACAO := qt_pontuacao_w;                                       

    <<Final>>
    qt_reg_w	:= 0;
end;
/