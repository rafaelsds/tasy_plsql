CREATE OR REPLACE TRIGGER PFCS_AFTER_UPT_EMPRESA
AFTER UPDATE ON EMPRESA
FOR EACH ROW

DECLARE
ds_log_w    varchar2(4000);
qt_reg_w    number(1);

BEGIN
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
        goto Final;
    end if;

    if (:old.nm_usuario_nrec = 'PFCS') then
        ds_log_w := (
            concat('cd_empresa: ',                  :old.cd_empresa || CHR(10))
            || concat('cd_base_cgc: ',              :old.cd_base_cgc || CHR(10))
            || concat('old nm_razao_social: ',      :old.nm_razao_social)
            || concat(' | new nm_razao_social: ',   :new.nm_razao_social || CHR(10))
            || concat('old ie_situacao: ',          nvl(:old.ie_situacao,'null'))
            || concat(' | new ie_situacao: ',       nvl(:new.ie_situacao,'null'))
        );
        pfcs_log_pck.pfcs_insert_org_struc_log(
            ie_type_p => 'UPDATE',
            nm_table_p => 'EMPRESA',
            ds_log_p => ds_log_w,
            nm_usuario_p => :new.nm_usuario
        );
    end if;

<<Final>>
qt_reg_w := 0;
END;
/
