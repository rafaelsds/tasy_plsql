create or replace 
trigger REGRA_NUMERACAO_DECLA_BEFINS
before insert or update on REGRA_NUMERACAO_DECLARACAO 
for each row

declare
retorno_w number (10);

PRAGMA AUTONOMOUS_TRANSACTION;

begin

select count(*)
into retorno_w
from REGRA_NUMERACAO_DEC_ITEM x
where NR_DECLARACAO >= :new.NR_INICIAL 
and NR_DECLARACAO <= :new.NR_FINAL
and (SELECT IE_TIPO_NUMERACAO 
		FROM REGRA_NUMERACAO_DEC_ITEM R, 
			REGRA_NUMERACAO_DECLARACAO RN 
		WHERE R.NR_SEQ_REGRA_NUM = RN.NR_SEQUENCIA
        AND R.NR_SEQUENCIA = X.NR_SEQUENCIA ) = :NEW.IE_TIPO_NUMERACAO;

if (retorno_w > 0) then
	Wheb_mensagem_pck.exibir_mensagem_abort(1092632);
end if;


end;
/