CREATE OR REPLACE
TRIGGER AGFA_AUTOR_CONVENIO_UPDATE
AFTER UPDATE OF NR_SEQ_ESTAGIO
ON AUTORIZACAO_CONVENIO FOR EACH ROW DECLARE

cd_pessoa_fisica_w              varchar2(20);
ie_tipo_atendimento_w           varchar2(2);
nr_seq_proc_interno_w   number(10);
nr_seq_prescricao_w             number(10);
ds_proc_exame_w                 varchar2(100);
dt_prev_execucao_w              date;
ie_urgencia_w                   varchar2(1);
cd_setor_w                      number(5);
qt_peso_w                       number(6,3);
cd_medico_w                     varchar2(20);
ie_lado_w                       varchar2(1);
ds_obs_prescr_w                 varchar2(500);
ds_obs_item_w                   varchar2(254);
ds_dados_clinicos_w             varchar2(254);

ie_interno_w                    varchar2(255);
ie_retorno_w                    varchar2(255);
ds_erro_w						varchar2(2000);

qt_regra_w			number(10);
qt_regra_exame_w	number(10);

cd_integracao_w		REGRA_PROC_INTERNO_INTEGRA.CD_INTEGRACAO%TYPE;

cursor c01 is
        select  a.nr_seq_proc_interno,
                        a.nr_seq_prescricao,
                        b.ds_proc_exame,
                        nvl(d.dt_prev_execucao, c.dt_prescricao),
                        decode(d.ie_urgencia, 'S', 'A', 'N'),
                        d.cd_setor_atendimento,
                        c.qt_peso,
                        c.cd_medico,
                        decode(nvl(d.ie_lado,'X'), 'A', 'B', nvl(d.ie_lado,'X')),
                        substr(c.ds_observacao,1,254),
                        substr(d.ds_observacao,1,254),
                        substr(d.ds_dado_clinico,1,254)
        from    proc_interno b,
                prescr_medica c,
                prescr_procedimento d,
                procedimento_autorizado a
        where   a.nr_seq_proc_interno   = b.nr_sequencia
          and   a.nr_prescricao         = c.nr_prescricao
          and   a.nr_prescricao         = d.nr_prescricao
          and   a.nr_seq_prescricao     = d.nr_sequencia
          and   a.nr_sequencia_autor    = :new.nr_sequencia
          and   nvl(b.nr_seq_classif,0) = 5;
BEGIN

begin

if      (:new.nr_atendimento is not null) and
        (:new.nr_seq_estagio is not null) and
        (:new.nr_seq_estagio <> :old.nr_seq_estagio) then

        begin
        select  cd_pessoa_fisica,
                decode(nvl(ie_tipo_atendimento,1), 1, 'H1', 3, 'A2', 8, 'AA', 'AE')
        into    cd_pessoa_fisica_w,
                ie_tipo_atendimento_w
        from    atendimento_paciente
        where   nr_atendimento = :new.nr_atendimento;
        exception
                when others then
                        wheb_mensagem_pck.mensagem(-20011,'Atendimento n�o encontrado! N�mero:' || to_char(:new.nr_atendimento));
        end;
        if      (ie_tipo_atendimento_w <> 'AE') then
                select  max(ie_interno)
                into    ie_interno_w
                from    estagio_autorizacao
                where   nr_sequencia    = :new.nr_seq_estagio;

                open c01;
                loop
                        fetch c01 into  nr_seq_proc_interno_w,
                                                        nr_seq_prescricao_w,
                                                        ds_proc_exame_w,
                                                        dt_prev_execucao_w,
                                                        ie_urgencia_w,
                                                        cd_setor_w,
                                                        qt_peso_w,
                                                        cd_medico_w,
                                                        ie_lado_w,
                                                        ds_obs_prescr_w,
                                                        ds_obs_item_w,
                                                        ds_dados_clinicos_w;
                        exit when c01%notfound;
                        insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log)
                                      values (sysdate, :new.nm_usuario, 12345, ie_interno_w || ';'|| to_char(nr_seq_proc_interno_w));

									  
						SELECT COUNT(1)
						INTO   qt_regra_w
						FROM   REGRA_PROC_INTERNO_INTEGRA
						WHERE  IE_TIPO_INTEGRACAO = 11
						AND	   nvl(CD_ESTABELECIMENTO,:new.cd_estabelecimento) = :new.cd_estabelecimento;

						if (qt_regra_w > 0) then			
							SELECT 	count(1)
							INTO	qt_regra_exame_w
							FROM	REGRA_PROC_INTERNO_INTEGRA
							WHERE	IE_TIPO_INTEGRACAO = 11
							AND		nvl(CD_ESTABELECIMENTO,:new.cd_estabelecimento) = :new.cd_estabelecimento
							AND		NR_SEQ_PROC_INTERNO = nr_seq_proc_interno_w;

							if (qt_regra_exame_w > 0) then
								SELECT	max(cd_integracao)
								INTO	cd_integracao_w
								FROM	REGRA_PROC_INTERNO_INTEGRA
								WHERE	IE_TIPO_INTEGRACAO = 11
								AND		nvl(CD_ESTABELECIMENTO,:new.cd_estabelecimento) = :new.cd_estabelecimento
								AND		NR_SEQ_PROC_INTERNO = nr_seq_proc_interno_w;						
							
								if (nvl(cd_integracao_w,'') = '') then
									cd_integracao_w := to_char(nr_seq_proc_interno_w);					
								end if;
							end if;
						end if;		

						if ((qt_regra_w = 0) or ((qt_regra_w > 0) and (qt_regra_exame_w > 0))) then											  

							if      (ie_interno_w = '10') then -- autorizado
									AGFA_Gera_Solicitacao(  to_char(:new.nr_prescricao),
															'2^UPDATED',
															ie_urgencia_w,
															dt_prev_execucao_w,
															dt_prev_execucao_w,
															'IMGHS',
															:new.nm_usuario,
															cd_medico_w,
															cd_pessoa_fisica_w,
															to_char(:new.nr_atendimento),
															ie_tipo_atendimento_w,
															cd_integracao_w,
															ds_proc_exame_w,
															to_char(nr_seq_prescricao_w),
															ds_obs_prescr_w,
															ds_dados_clinicos_w     || chr(10) ||
															ds_obs_item_w           || chr(10) ||
															'Data prev. Execu��o:'  || to_char(dt_prev_execucao_w,'dd/mm/yyyy hh24:mi:ss'),
															cd_setor_w,
															'Peso^' || to_char(qt_peso_w),
															'1^Authorized',
															ie_retorno_w);
							/*elsif   (ie_interno_w = '90') then -- negado
									AGFA_Gera_Solicitacao(  to_char(:new.nr_prescricao),
															'2^UPDATED',
															ie_urgencia_w,
															dt_prev_execucao_w,
															dt_prev_execucao_w,
															'IMGHS',
															:new.nm_usuario,
															cd_medico_w,
															cd_pessoa_fisica_w,
															to_char(:new.nr_atendimento),
															ie_tipo_atendimento_w,
															cd_integracao_w,
															ds_proc_exame_w,
															to_char(nr_seq_prescricao_w),
															ds_obs_prescr_w,
															ds_dados_clinicos_w     || chr(10) ||
															ds_obs_item_w           || chr(10) ||
															'Data prev. Execu��o:'  || to_char(dt_prev_execucao_w,'dd/mm/yyyy hh24:mi:ss'),
															cd_setor_w,
															'Peso^' || to_char(qt_peso_w),
															'2^Unauthorized',
															ie_retorno_w);             -- Ronaldo Wheb OS 439100*/
							end if;
						end if;
                        if      (ie_retorno_w is not null) then
                                wheb_mensagem_pck.mensagem(-20011, 'Retorno AGFA:' || ie_retorno_w);
                        end if;
                end loop;
        end if;
end if;

exception
when others then
	ds_erro_w := substr(sqlerrm,1,2000);
	insert into log_tasy(dt_atualizacao,nm_usuario,cd_log,ds_log) values (sysdate,'AGFA',-33342,ds_erro_w);
end;

END AGFA_AUTOR_CONVENIO_UPDATE;
/
