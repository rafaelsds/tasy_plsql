create or replace trigger pls_conv_serv_extrat_mensatual
before insert or update on pls_conv_serv_extrat_mens
for each row

declare

begin

if	((:new.cd_procedimento 		is not null) or 
	(:new.cd_area_procedimento 	is not null) or
	(:new.cd_especialidade 		is not null) or
	(:new.cd_grupo_proc 		is not null) or
	(:new.ie_tipo_despesa 		is not null)) and
	(:new.nr_seq_material 		is not null) then
	-- N�o pode ser criada uma regra com material e procedimento
	wheb_mensagem_pck.exibir_mensagem_abort(266790);
end if;

if	(:new.cd_procedimento 		is null) and
	(:new.cd_area_procedimento 	is null) and
	(:new.cd_especialidade 		is null) and
	(:new.cd_grupo_proc 		is null) and
	(:new.ie_tipo_despesa 		is null) and
	(:new.nr_seq_material 		is null) and
	(nvl(:new.ie_atend_internacao,'N') = 'N') then
	-- N�o pode ser criada uma regra vazia.
	wheb_mensagem_pck.exibir_mensagem_abort(266791);
end if;

if	(:new.ie_atend_internacao = 'S') and
	((:new.cd_procedimento 		is not null) or
	(:new.cd_area_procedimento 	is not null) or
	(:new.cd_especialidade 		is not null) or
	(:new.cd_grupo_proc 		is not null) or
	(:new.ie_tipo_despesa 		is not null) or
	(:new.nr_seq_material 		is not null) or
	(:new.ie_benef_cooperado <> 'A') or
	(:new.ie_prestador_exec_cooperado <> 'A')) then
	--N�o � possivel criar uma regra com mais de um campo marcado al�m do campo "Identificar atendimento interna��o"
	wheb_mensagem_pck.exibir_mensagem_abort(388483);
end if;

end;
/
