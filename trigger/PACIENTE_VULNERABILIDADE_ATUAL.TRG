CREATE OR REPLACE TRIGGER PACIENTE_VULNERABILIDADE_ATUAL
BEFORE INSERT OR UPDATE ON paciente_vulnerabilidade	
FOR EACH ROW

DECLARE


BEGIN
										
if	(:new.DT_ATUALIZACAO_NREC is not null) then
	:new.ds_utc		:= substr(obter_data_utc(:new.DT_ATUALIZACAO_NREC, 'HV'),1,50);	
end if;

if	((:old.DT_LIBERACAO is null) or (:old.DT_LIBERACAO <> :new.DT_LIBERACAO)) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= substr(obter_data_utc(:new.DT_LIBERACAO,'HV'),1,50);
end if;


END;
/
