create or replace trigger aval_nutricao_atual
before insert or update on aval_nutricao
for each row
declare
    cd_pessoa_fisica_w VARCHAR2(10);
    ie_sexo_w VARCHAR2(1);
    qt_fator_ativ_w NUMBER(15, 4);
    qt_fator_stress_w NUMBER(15, 4);
    qt_obesidade NUMBER(1);
    qt_pont_sexo NUMBER(1);
    qt_pont_queimadura NUMBER(1);
    qt_pont_trauma NUMBER(1);
    ie_raca_negra_w VARCHAR2(1);
    qt_nec_proteica_w NUMBER(10, 2);
    sql_w VARCHAR2(4000);
    qt_razao_cintura_quadril_w NUMBER(15, 4);
    qt_imc_w 				number(6,1);
    qt_altura_estimada_w 	number(3);
    qt_altura_w 			number(5,2);
    qt_peso_atual_w 		number(7,3);
    qt_peso_atual_ww 		number(7,3);
    qt_peso_ajustado_w 		number(7,3);
    pr_gordura_w 			number(7,3);
    qt_reg_w 				NUMBER(1);
    qt_perc_circ_braco_w 		NUMBER(5,2);
    qt_perc_prega_cut_tricip_w NUMBER(5,2);
    qt_perc_circ_musc_braco_w NUMBER(5,2);
    ds_erro_w             VARCHAR2(4000);
    ds_parametros_w       VARCHAR2(4000);
    qt_peso_habit_i_w     number(5);
    ie_form_hamwi_w       varchar2(1);
	qt_caloria_w		NUMBER(6,1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select cd_pessoa_fisica
into   cd_pessoa_fisica_w
from   atendimento_paciente
where  nr_atendimento = :new.nr_atendimento;

select obter_sexo_pf(cd_pessoa_fisica_w,'C')
into   ie_sexo_w
from   dual;

begin
sql_w := 'begin obter_perda_aval_nutri_md  (:1, :2, :3, :4, :5); end;';
execute immediate sql_w
	using in :new.qt_peso_atual, in :new.qt_peso_habit,
		 out :new.pr_perda, out :new.qt_perda, out :new.pr_perda_peso;

exception
	when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr( ':new.nr_atendimento: '
                                 || :new.nr_atendimento
                                 || '-'
                                 || ':new.qt_peso_atual: '
                                 || :new.qt_peso_atual
                                 || '-'
                                 || 'new.qt_peso_habit: '
                                 || :new.qt_peso_habit
                                 || '-'
                                 || ':new.pr_perda: '
                                 || :new.pr_perda
                                 || '-'
                                 || ':new.qt_perda: '
                                 || :new.qt_perda
                                 || '-'
                                 || ':new.pr_perda_peso: '
                                 || :new.pr_perda_peso, 1, 4000);
            gravar_log_medical_device('aval_nutricao_atual',
                                     'obter_perda_aval_nutri_md',
                                     ds_parametros_w,
                                     ds_erro_w,
                                     :new.nm_usuario,
                                     'N');
		:new.pr_perda := null;
		:new.qt_perda := null;
		:new.pr_perda_peso := null;
end;

if	(nvl(:old.dt_avaliacao,sysdate+10) <> :new.dt_avaliacao) and
	(:new.dt_avaliacao is not null) then
	:new.ds_utc		:= obter_data_utc(:new.dt_avaliacao, 'hv');
end if;
if	(nvl(:old.dt_liberacao,sysdate+10) <> :new.dt_liberacao) and
	(:new.dt_liberacao is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.dt_liberacao,'hv');
end if;

if	(:new.qt_altura is not null) then
    begin
        sql_w := 'call obter_valor_imc_md(:1, :2, :3) into :qt_imc_w';
        execute immediate sql_w
            using	in :new.qt_peso_atual ,
					in 0 ,
					in :new.qt_altura,
					out qt_imc_w;
    exception
        when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr( ':new.nr_atendimento: '
                                 || :new.nr_atendimento
                                 || '-'
                                 || ':new.qt_peso_atual: '
                                 || :new.qt_peso_atual
                                 || '-'
                                 || 'new.qt_altura: '
                                 || :new.qt_altura
                                 || '-'
                                 || 'qt_imc_w: '
                                 || qt_imc_w, 1, 4000);
            gravar_log_medical_device('aval_nutricao_atual',
                                     'obter_valor_imc_md',
                                     ds_parametros_w,
                                     ds_erro_w,
                                     :new.nm_usuario,
                                     'N');

            qt_imc_w := null;
    end;

    :new.qt_imc	:= qt_imc_w;
end if;

obter_param_usuario(281, 1666, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_form_hamwi_w);

if(ie_form_hamwi_w = 'S'           and
  :new.qt_altura is not null       and
  :new.qt_peso_habit_i is null     and
  ie_sexo_w in ('M', 'F')) then
    begin
        sql_w := 'call calculate_weight_form_hamwi(:1, :2) into :qt_peso_habit_i_w';
        execute immediate sql_w
          using	in :new.qt_altura,
                in ie_sexo_w,
                out qt_peso_habit_i_w;
  exception
      when others then
          ds_erro_w := substr(sqlerrm, 1, 4000);
          ds_parametros_w := substr( ':new.nr_atendimento: '
                                || :new.nr_atendimento
                                || '-'
                                || ':new.qt_altura: '
                                || :new.qt_altura
                                || '-'
                                || 'ie_sexo_w: '
                                || ie_sexo_w
                                || '-'
                                || 'qt_peso_habit_i_w: '
                                || qt_peso_habit_i_w, 1, 4000);
          gravar_log_medical_device('aval_nutricao_atual',
                                    'calculate_weight_form_hamwi',
                                    ds_parametros_w,
                                    ds_erro_w,
                                    :new.nm_usuario,
                                    'N');

          qt_peso_habit_i_w := :new.qt_peso_habit_i;
  end;

  :new.qt_peso_habit_i := qt_peso_habit_i_w;
end if;

if	(:new.qt_circ_quadril	is not null) and
	(:new.qt_circ_cintura is not null)then

    begin
        sql_w := 'call dividir_md(:1, :2) into :qt_razao_cintura_quadril_w';
        execute immediate sql_w
            using	in :new.qt_circ_cintura,
					in :new.qt_circ_quadril,
					out qt_razao_cintura_quadril_w;
    exception
        when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr( ':new.nr_atendimento: '
                                 || :new.nr_atendimento
                                 || '-'
                                 || ':new.qt_circ_cintura: '
                                 || :new.qt_circ_cintura
                                 || '-'
                                 || 'new.qt_circ_quadril: '
                                 || :new.qt_circ_quadril
                                 || '-'
                                 || 'qt_razao_cintura_quadril_w: '
                                 || qt_razao_cintura_quadril_w, 1, 4000);
            gravar_log_medical_device('aval_nutricao_atual',
                                     'dividir_md',
                                     ds_parametros_w,
                                     ds_erro_w,
                                     :new.nm_usuario,
                                     'N');

            :new.qt_razao_cintura_quadril := null;
    end;

    :new.qt_razao_cintura_quadril	:= qt_razao_cintura_quadril_w;
end if;

/* Obter se raca negra */
select	nvl(max(ie_negro),'N')
into	ie_raca_negra_w
from	cor_pele a,
		pessoa_fisica b
where	b.nr_seq_cor_pele 	= a.nr_sequencia
and		b.cd_pessoa_fisica 	= cd_pessoa_fisica_w;

begin
	sql_w := 'call obter_altura_estimada_md(:1, :2, :3) into :qt_altura_estimada_w';
	execute immediate sql_w
		using	in ie_sexo_w ,
				in :new.qt_idade,
				in :new.qt_altura_joelho,
				out qt_altura_estimada_w;
exception
	when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr( ':new.nr_atendimento: '
                                 || :new.nr_atendimento
                                 || '-'
                                 || 'ie_sexo_w: '
                                 || ie_sexo_w
                                 || '-'
                                 || 'new.qt_idade: '
                                 || :new.qt_idade
                                 || '-'
                                 || 'new.qt_altura_joelho: '
                                 || :new.qt_altura_joelho
                                 || '-'
                                 || 'qt_altura_estimada_w: '
                                 || qt_altura_estimada_w, 1, 4000);
            gravar_log_medical_device('aval_nutricao_atual',
                                     'obter_altura_estimada_md',
                                     ds_parametros_w,
                                     ds_erro_w,
                                     :new.nm_usuario,
                                     'N');

		qt_altura_estimada_w := null;
end;

:new.qt_altura_estimada	:= qt_altura_estimada_w;

begin
	sql_w := 'begin obter_alt_peso_aval_nutri_md  (:1, :2, :3, :4, :5, :6, :7, :8, :9 ,:10, :11, :12); end;';
	execute immediate sql_w
		using	in :new.ie_origem_altura,
				in :new.qt_altura_joelho,
				in ie_sexo_w,
				in :new.qt_idade,
				in :new.ie_origem_peso,
				in :new.qt_prega_cut_subesc,
				in :new.qt_circ_panturrilha,
				in :new.qt_circ_braco,
				in :new.qt_circ_abdomen,
				in ie_raca_negra_w,
				out qt_altura_w,
				out qt_peso_atual_w;
exception
	when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr( ':new.nr_atendimento: '
                                 || :new.nr_atendimento
                                 || '-'
                                 || ':new.ie_origem_altura: '
                                 || :new.ie_origem_altura
                                 || '-'
                                 || 'ie_sexo_w: '
                                 || ie_sexo_w
                                 || '-'
                                 || 'new.qt_idade: '
                                 || :new.qt_idade
                                 || '-'
                                 || ':new.ie_origem_peso: '
                                 || :new.ie_origem_peso
                                 || '-'
                                 || ':new.qt_prega_cut_subesc: '
                                 || :new.qt_prega_cut_subesc
                                 || '-'
                                 || ':new.qt_circ_panturrilha: '
                                 || :new.qt_circ_panturrilha
                                 || '-'
                                 || 'new.qt_circ_braco: '
                                 || :new.qt_circ_braco
                                 || '-'
                                 || ':new.qt_circ_abdomen: '
                                 || :new.qt_circ_abdomen
                                 || '-'
                                 || 'ie_raca_negra_w: '
                                 || ie_raca_negra_w
                                 || '-'
                                 || 'qt_altura_w: '
                                 || qt_altura_w
                                 || '-'
                                 || 'qt_peso_atual_w: '
                                 || qt_peso_atual_w, 1, 4000);


            gravar_log_medical_device('aval_nutricao_atual',
                                     'obter_alt_peso_aval_nutri_md',
                                     ds_parametros_w,
                                     ds_erro_w,
                                     :new.nm_usuario,
                                     'N');

		qt_altura_w := null;
		qt_peso_atual_w := null;
end;

if (qt_altura_w is not null) then
	:new.qt_altura := qt_altura_w;
end if;

if (qt_peso_atual_w is not null) then
	:new.qt_peso_atual	:= qt_peso_atual_w;
end if;

begin

qt_perc_circ_braco_w := obter_classif_percentil(ie_raca_negra_w, ie_sexo_w, :new.qt_idade, 'CB');
qt_perc_prega_cut_tricip_w := obter_classif_percentil(ie_raca_negra_w, ie_sexo_w, :new.qt_idade, 'PCT');
qt_perc_circ_musc_braco_w := obter_classif_percentil(ie_raca_negra_w, ie_sexo_w, :new.qt_idade, 'CMB');

:new.QT_PERC_CIRC_BRACO := qt_perc_circ_braco_w;
:new.qt_perc_prega_cut_tricip := qt_perc_prega_cut_tricip_w;
:new.qt_perc_circ_musc_braco := qt_perc_circ_musc_braco_w;

qt_caloria_w := :new.qt_caloria;

sql_w := 'begin calc_percent_aval_nutri_md  (:1, :2, :3, :4, :5, :6, :7, :8, :9 ,:10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23); end;';
execute immediate sql_w
	using in ie_sexo_w,
		  in :new.qt_idade, in :new.qt_circ_braco, in :new.qt_prega_cut_tricip,
		  in :new.qt_peso_atual, in :new.qt_altura, in :new.qt_altura_estimada,
		  in :new.qt_proteina, in :old.qt_caloria, in :new.qt_perc_circ_braco,
		  in :new.qt_perc_prega_cut_tricip, in :new.qt_perc_circ_musc_braco,
		  out :new.pr_circ_braco, out :new.pr_prega_cut_tricip,
		  out :new.pr_circ_musc_braco, out :new.qt_gasto_ener_repouso,
		  out :new.qt_nec_proteica, out :new.qt_gasto_ener_repouso_kj,
		  out :new.qt_nec_calorica, out :new.qt_nec_calorica_kj, out :new.qt_circ_musc_braco,
		  in out qt_caloria_w, in out :new.qt_caloria_kj;

:new.qt_caloria :=  qt_caloria_w;
		  
exception
	when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr( ':new.nr_atendimento: '
                                 || :new.nr_atendimento
                                 || '-'
                                 || ':new.qt_idade: '
                                 || :new.qt_idade
                                 || '-'
                                 || 'qt_circ_braco: '
                                 || :new.qt_circ_braco
                                 || '-'
                                 || 'new.qt_idade: '
                                 || :new.qt_idade
                                 || '-'
                                 || ':new.qt_prega_cut_tricip: '
                                 || :new.qt_prega_cut_tricip
                                 || '-'
                                 || ':new.qt_peso_atual: '
                                 || :new.qt_peso_atual
                                 || '-'
                                 || ':new.qt_altura: '
                                 || :new.qt_altura
                                 || '-'
                                 || 'new.qt_altura_estimada: '
                                 || :new.qt_altura_estimada
                                 || '-'
                                 || ':new.qt_proteina: '
                                 || :new.qt_proteina
                                 || '-'
                                 || ':old.qt_caloria: '
                                 || :old.qt_caloria
                                 || '-'
                                 || ':new.qt_perc_circ_braco: '
                                 || :new.qt_perc_circ_braco
                                 || '-'
                                 || ':new.qt_perc_prega_cut_tricip: '
                                 || :new.qt_perc_prega_cut_tricip
                                 || '-'
                                 || ':new.qt_perc_circ_musc_braco: '
                                 || :new.qt_perc_circ_musc_braco
                                 || '-'
                                 || 'pr_circ_braco: '
                                 || :new.pr_circ_braco
                                 || '-'
                                 || 'new.pr_prega_cut_tricip: '
                                 || :new.pr_prega_cut_tricip
                                 || '-'
                                 || ':new.pr_circ_musc_braco: '
                                 || :new.pr_circ_musc_braco
                                 || '-'
                                 || ':new.qt_gasto_ener_repouso: '
                                 || :new.qt_gasto_ener_repouso
                                 || '-'
                                 || ':new.qt_nec_proteica: '
                                 || :new.qt_nec_proteica
                                 || '-'
                                 || 'new.qt_gasto_ener_repouso_kj: '
                                 || :new.qt_gasto_ener_repouso_kj
                                 || '-'
                                 || ':new.qt_nec_calorica: '
                                 || :new.qt_nec_calorica
                                 || '-'
                                 || ':new.qt_nec_calorica_kj: '
                                 || :new.qt_nec_calorica_kj
                                 || '-'
                                 || ':new.qt_circ_musc_braco: '
                                 || :new.qt_circ_musc_braco
                                 || '-'
                                 || ':new.qt_caloria: '
                                 || :new.qt_caloria
                                 || '-'
                                 || ':new.qt_caloria_kj: '
                                 || :new.qt_caloria_kj, 1, 4000);

           gravar_log_medical_device('aval_nutricao_atual',
                                     'calc_percent_aval_nutri_md',
                                     ds_parametros_w,
                                     ds_erro_w,
                                     :new.nm_usuario,
                                     'N');

		:new.pr_circ_braco := null;
		:new.pr_prega_cut_tricip := null;
		:new.pr_circ_musc_braco := null;
		:new.qt_gasto_ener_repouso := null;
		:new.qt_gasto_ener_repouso_kj := null;
		:new.qt_nec_proteica := null;
		:new.qt_caloria_kj := null;
		:new.qt_caloria := null;
		:new.qt_nec_calorica := null;
		:new.qt_nec_calorica_kj := null;
		:new.qt_circ_musc_braco := null;
end;

begin
if	(:new.QT_GASTO_ENER_REPOUSO is not null or :new.QT_GASTO_ENER_REPOUSO_KJ is not null ) and
	(:new.NR_SEQ_FATOR_ATIV is not null) and
	(:new.NR_SEQ_FATOR_STRESS is not null) then

	select	nvl(max(qt_fator),1)
	into	qt_fator_stress_w
	from	nut_fator_stress
	where	nr_sequencia	= :new.NR_SEQ_FATOR_STRESS;
	select	nvl(max(qt_fator),1)
	into	qt_fator_ativ_w
	from	nut_fator_ativ
	where	nr_sequencia	= :new.NR_SEQ_FATOR_ATIV ;

    begin
        sql_w := 'begin obter_gasto_aval_nutri_md  (:1, :2, :3, :4, :5, :6); end;';
        execute immediate sql_w using in :new.qt_gasto_ener_repouso, in :new.qt_gasto_ener_repouso_kj, in qt_fator_ativ_w,
									  in qt_fator_stress_w,out :new.qt_gasto_ener_total, out :new.qt_gasto_ener_total_kj;

    exception
        when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr( ':new.nr_atendimento: '||:new.nr_atendimento||'-'||'qt_gasto_ener_repouso: '||:new.qt_gasto_ener_repouso||'-'||'qt_gasto_ener_repouso_kj: '||:new.qt_gasto_ener_repouso_kj
										||'-'||'qt_fator_ativ_w: '||qt_fator_ativ_w||'-'||'qt_fator_stress_w: '||qt_fator_stress_w||'-'||'new.qt_gasto_ener_total: '||:new.qt_gasto_ener_total||'-'||':new.qt_gasto_ener_total_kj: '||:new.qt_gasto_ener_total_kj, 1, 4000);
            gravar_log_medical_device('aval_nutricao_atual', 'OBTER_GASTO_AVAL_NUTRI_MD', ds_parametros_w, ds_erro_w, :new.nm_usuario, 'N');
            :new.qt_gasto_ener_total := null;
            :new.qt_gasto_ener_total_kj := null;
         end;

end if;
exception
when others then
	null;
end;
begin
    begin
        sql_w := 'begin obter_gasto_mif_aval_nutri_md  (:1, :2, :3, :4, :5, :6); end;';
        execute immediate sql_w using in :new.qt_altura ,in ie_sexo_w, in :new.qt_peso_atual, in :new.qt_idade,
									  out :new.qt_gasto_geb_mifflin, out :new.qt_gasto_geb_mifflin_kj ;
    exception
        when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr(':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.qt_altura: '||:new.qt_altura||'-'||'ie_sexo_w: '||ie_sexo_w||'-'||
									  'new.qt_peso_atual: '||:new.qt_peso_atual||'-'||':new.qt_idade: '||:new.qt_idade||'-'||':new.qt_gasto_geb_mifflin: '||:new.qt_gasto_geb_mifflin||'-'||
									  ':new.qt_gasto_geb_mifflin_kj: '||:new.qt_gasto_geb_mifflin_kj, 1, 4000);
            gravar_log_medical_device('aval_nutricao_atual', 'OBTER_GASTO_MIF_AVAL_NUTRI_MD', ds_parametros_w, ds_erro_w, :new.nm_usuario, 'N');
            :new.qt_gasto_geb_mifflin := null;
            :new.qt_gasto_geb_mifflin_kj := null;
    end;

    begin
        sql_w := 'call obter_peso_ajustado_md(:1, :2, :3, :4) into :qt_peso_ajustado_w';
        execute immediate sql_w using in :new.qt_peso_atual ,in :new.qt_peso_habit, in :new.qt_imc,
									  in :new.qt_peso_habit_i , out qt_peso_ajustado_w;
    exception
        when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr(  ':new.nr_atendimento: '||:new.nr_atendimento||'-'|| ':new.QT_PESO_ATUAL: '||:new.QT_PESO_ATUAL||'-'||':new.QT_PESO_HABIT: '||:new.QT_PESO_HABIT||'-'||
										'new.QT_IMC: '||:new.QT_IMC||'-'||':new.QT_PESO_HABIT_I: '||:new.QT_PESO_HABIT_I||'-'||'QT_PESO_AJUSTADO_W: '||QT_PESO_AJUSTADO_W, 1, 4000);
            gravar_log_medical_device('AVAL_NUTRICAO_ATUAL', 'OBTER_PESO_AJUSTADO_MD', ds_parametros_w, ds_erro_w, :new.nm_usuario, 'N');
            qt_peso_ajustado_w := null;
    end;

    :new.qt_peso_ajustado	:= qt_peso_ajustado_w;

    begin
        sql_w := 'call obter_perc_gordura_md(:1, :2, :3, :4) into :pr_gordura_w';
        execute immediate sql_w using in :new.qt_prega_cut_tricip , in :new.qt_prega_cut_suprailiaca,
									  in :new.qt_prega_cut_subesc, in :new.qt_prega_cut_abd , out pr_gordura_w;
    exception
        when others then
            ds_erro_w := substr(sqlerrm,1 , 4000);
            ds_parametros_w := substr(':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.qt_prega_cut_tricip: '||:new.qt_prega_cut_tricip||'-'||':new.qt_prega_cut_suprailiaca: '||:new.qt_prega_cut_suprailiaca||'-'||
									  'new.qt_prega_cut_subesc: '||:new.qt_prega_cut_subesc||'-'||':new.qt_prega_cut_abd: '||:new.qt_prega_cut_abd||'-'||'pr_gordura_w: '||pr_gordura_w, 1, 4000);
            gravar_log_medical_device('AVAL_NUTRICAO_ATUAL', 'OBTER_PERC_GORDURA_MD', ds_parametros_w, ds_erro_w, :new.nm_usuario, 'N');
            pr_gordura_w := null;
    END;

    :new.pr_gordura	:= pr_gordura_w;

    begin
        sql_w := 'begin obter_pont_aval_nutri_md(:1, :2, :3, :4, :5, :6, :7, :8, :9); end;';
        execute immediate sql_w
            using in :new.ie_respiracao ,in :new.qt_imc, in :new.qt_idade, in :new.qt_peso_atual,
                  in :new.ie_queimadura, in :new.ie_trauma, in ie_sexo_w,
                  out :new.qt_gast_ener_total_ireton, out :new.qt_gast_ener_total_ireton_kj ;
    exception
        when others then
            ds_erro_w := substr(sqlerrm, 1, 4000);
            ds_parametros_w := substr(':new.nr_atendimento: '||:new.nr_atendimento||'-'||':new.IE_RESPIRACAO: '||:new.IE_RESPIRACAO||'-'||':new.QT_IMC: '||:new.QT_IMC||'-'||
								      'new.qt_idade: '||:new.qt_idade||'-'||':new.qt_peso_atual: '||:new.qt_peso_atual||'-'||':new.IE_QUEIMADURA: '||:new.IE_QUEIMADURA||'-'||
									  ':new.IE_TRAUMA: '||:new.IE_TRAUMA||'-'||'ie_sexo_w: '||ie_sexo_w||'-'||':new.QT_GAST_ENER_TOTAL_IRETON: '||:new.QT_GAST_ENER_TOTAL_IRETON||'-'||
									  ':new.QT_GAST_ENER_TOTAL_IRETON_KJ: '||:new.QT_GAST_ENER_TOTAL_IRETON_KJ, 1, 4000);
            gravar_log_medical_device('AVAL_NUTRICAO_ATUAL', 'OBTER_PONT_AVAL_NUTRI_MD', ds_parametros_w, ds_erro_w, :new.nm_usuario, 'N');
            :new.QT_GAST_ENER_TOTAL_IRETON := NULL;
            :new.QT_GAST_ENER_TOTAL_IRETON_KJ := NULL;
    END;
exception
when others then
	null;
end;
<<Final>>
qt_reg_w	:= 0;
end;
/
