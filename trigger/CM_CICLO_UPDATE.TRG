create or replace trigger cm_ciclo_update
before update on cm_ciclo
for each row
declare

nr_seq_conj_cont_w		cm_conjunto_cont.nr_sequencia%type;

cursor c01 is
	select	nr_sequencia
	from	cm_conjunto_cont
	where	nr_seq_ciclo = :new.nr_sequencia;

begin

	--finalizando um ciclo, geramos histórico/contagem para todos os conjuntos do ciclo finalizado.
	if	((:new.dt_fim is not null) and (:old.dt_fim is null)) then
	
		open c01;
		loop
		fetch c01 into	
			nr_seq_conj_cont_w;
		exit when c01%notfound;
			begin
				insert into cm_contagem_ciclo (
						  nr_sequencia,        
						  dt_atualizacao,
						  nm_usuario,
						  dt_atualizacao_nrec,
						  nm_usuario_nrec,
						  nr_seq_ciclo,
						  nr_seq_conj_cont) 
				values (
						  cm_contagem_ciclo_seq.nextval, 
						  sysdate,
						  :new.nm_usuario,
						  sysdate,
						  :new.nm_usuario,
						  :new.nr_sequencia,
						  nr_seq_conj_cont_w);
			end;
		end loop;
		close c01;

	end if;

end;
/