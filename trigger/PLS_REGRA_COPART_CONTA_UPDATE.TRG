create or replace trigger pls_regra_copart_conta_update
before update on pls_regra_copartic_conta
for each row

declare

begin
if	(:new.vl_item_minimo <> :old.vl_item_minimo) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.vl_item_minimo, :new.vl_item_minimo, 'PLS_REGRA_COPARTIC_CONTA', 'VL_ITEM_MINIMO', :new.nm_usuario);
end if;
if	(:new.vl_item_maximo <> :old.vl_item_maximo) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.vl_item_maximo, :new.vl_item_maximo, 'PLS_REGRA_COPARTIC_CONTA', 'VL_ITEM_MAXIMO', :new.nm_usuario);
end if;
if	(:new.nr_seq_tipo_conta <> :old.nr_seq_tipo_conta) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_tipo_conta, :new.nr_seq_tipo_conta, 'PLS_REGRA_COPARTIC_CONTA', 'NR_SEQ_TIPO_CONTA', :new.nm_usuario);
end if;
if	(:new.ie_cobranca_prevista <> :old.ie_cobranca_prevista) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_cobranca_prevista, :new.ie_cobranca_prevista, 'PLS_REGRA_COPARTIC_CONTA', 'IE_COBRANCA_PREVISTA', :new.nm_usuario);
end if;
end;
/