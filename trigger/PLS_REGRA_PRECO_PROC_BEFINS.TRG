create or replace trigger pls_regra_preco_proc_befins
before insert on pls_regra_preco_proc
for each row

declare
ie_classificacao_w	Varchar2(1);
ie_verifica_proc_w	Varchar2(1);

begin
/* Obter o valor do par�metro 7 da fun��o OPS - Regras e Crit�rios de Pre�o - 1298 */
ie_verifica_proc_w	:= nvl(obter_valor_param_usuario(1298, 7, Obter_Perfil_Ativo, :new.nm_usuario, :new.cd_estabelecimento), 'N');

if	(ie_verifica_proc_w	= 'S') then
	select	nvl(max(ie_classificacao),1)
	into	ie_classificacao_w
	from	procedimento
	where	:new.cd_procedimento = cd_procedimento
	and	ie_origem_proced = :new.ie_origem_proced;
	
	if	(ie_classificacao_w <> '1') then
		wheb_mensagem_pck.exibir_mensagem_abort( 267215, null ); /* N�o � permitido cadastrar di�rias ou servi�os na pasta Procedimentos! [7] */
	end if;
end if;

end;
/