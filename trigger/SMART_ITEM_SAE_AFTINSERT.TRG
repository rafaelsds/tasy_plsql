create or replace trigger Smart_ITEM_SAE_aftinsert
after insert or update on PE_PRESCR_PROC
for each row

declare

reg_integracao_w		gerar_int_padrao.reg_integracao;

begin
reg_integracao_w.cd_estab_documento := OBTER_ESTABELECIMENTO_ATIVO;

	if  (:old.DT_SUSPENSAO is null) and
		(:new.DT_SUSPENSAO is not null) then
		BEGIN		
				gerar_int_padrao.gravar_integracao('389', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w, 'DS_OPERACAO_P=SUSPENCAO');
		END;	
	end if;
end;
/
