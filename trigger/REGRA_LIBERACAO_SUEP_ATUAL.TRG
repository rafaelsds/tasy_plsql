create or replace trigger regra_liberacao_suep_atual
before insert or update on regra_liberacao_suep
for each row
declare

ds_sql_w		varchar2(4000);
ie_result_w		varchar2(2);

begin
 
  if (:new.ds_consulta is not null) then
  
    ds_sql_w :=	' select	nvl(max(''S''),''N'') ' ||
				' from		atendimento_paciente a ' ||
				' where		a.nr_atendimento = -1 ' ||
				:new.ds_consulta;
  
  	begin
		execute immediate ds_sql_w
		into ie_result_w;   
    exception
    when others then
		Wheb_mensagem_pck.exibir_mensagem_abort(1089864, 'SQL=' || ds_sql_w || ';ERRO='||sqlerrm);
    end;  
  		
  end if;

end;
/