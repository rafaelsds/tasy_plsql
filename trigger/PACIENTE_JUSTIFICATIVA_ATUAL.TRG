create or replace trigger PACIENTE_JUSTIFICATIVA_ATUAL
before insert or update on PACIENTE_JUSTIFICATIVA
for each row
declare


begin
if	(nvl(:old.DT_JUSTIFICATIVA,sysdate+10) <> :new.DT_JUSTIFICATIVA) and
	(:new.DT_JUSTIFICATIVA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_JUSTIFICATIVA, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/
