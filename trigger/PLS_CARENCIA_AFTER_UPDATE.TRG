create or replace trigger pls_carencia_after_update
after update on pls_carencia
for each row

declare

ds_alteracao_w	varchar(4000) := ' ';
ds_anterior_w	varchar(255);
ds_novo_w	varchar(255);
	
begin
if	((:new.nr_seq_grupo_carencia  <> :old.nr_seq_grupo_carencia ) or 
	(:new.nr_seq_grupo_carencia  is not null and :old.nr_seq_grupo_carencia is null) or 
	(:new.nr_seq_grupo_carencia is null and :old.nr_seq_grupo_carencia is not null))then	
	
	select 	substr(pls_obter_dados_grupo_carencia(:new.nr_seq_grupo_carencia,'N'),1,255),
		substr(pls_obter_dados_grupo_carencia(:old.nr_seq_grupo_carencia,'N'),1,255)
	into	ds_novo_w,
		ds_anterior_w
	from 	dual;	
	
	--Altera��o do grupo de car�ncia
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352337 , 'NR_CARENCIA_OLD=' || :old.nr_seq_grupo_carencia||';'||'DS_CARENCIA_OLD='||ds_anterior_w||';'|| 'NR_CARENCIA_NEW='||:new.nr_seq_grupo_carencia||';'||'DS_CARENCIA_NEW='||ds_novo_w);	
end if;

if	((trunc(:new.dt_fim_vig_plano,'DAY') <> trunc(:old.dt_fim_vig_plano,'DAY')) or
	(:new.dt_fim_vig_plano is not null  and :old.dt_fim_vig_plano is null) or
	(:new.dt_fim_vig_plano is null and :old.dt_fim_vig_plano is not null))then
	
	--Altera��o da data de fim de vig�ncia do plano.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352340 , 'DT_CARENCIA_OLD=' ||:old.dt_fim_vig_plano||';'||'DT_CARENCIA_NEW='||:new.dt_fim_vig_plano);
end if;

if	((trunc(:new.dt_inicio_vig_plano,'DAY') <> trunc(:old.dt_inicio_vig_plano,'DAY')) or
	(:new.dt_inicio_vig_plano is not null  and :old.dt_inicio_vig_plano is null) or
	(:new.dt_inicio_vig_plano is null and :old.dt_inicio_vig_plano is not null))then
	
	--Altera��o da data de in�cio de vig�ncia do plano.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352341 , 'DT_CARENCIA_OLD=' ||:old.dt_inicio_vig_plano||';'||'DT_CARENCIA_NEW='||:new.dt_inicio_vig_plano);
end if;

if 	((:new.nr_seq_plano_contrato <> :old.nr_seq_plano_contrato) or
	(:new.nr_seq_plano_contrato is not null and :old.nr_seq_plano_contrato is null) or
	(:new.nr_seq_plano_contrato is null and :old.nr_seq_plano_contrato is not null))then
	
	select 	substr(pls_obter_dados_produto(:new.nr_seq_plano_contrato,'N'),1,255),
		substr(pls_obter_dados_produto(:old.nr_seq_plano_contrato,'N'),1,255)
	into	ds_novo_w,
		ds_anterior_w
	from 	dual;	
	--Altera��o do produto do contrato que a car�ncia � v�lida.	
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352345 , 'NR_CARENCIA_OLD=' || :old.nr_seq_plano_contrato ||';'||'DS_CARENCIA_OLD='||ds_anterior_w||';'|| 'NR_CARENCIA_NEW='||:new.nr_seq_plano_contrato||';'||'DS_CARENCIA_NEW='||ds_novo_w);	
end if;

if	((:new.nr_seq_tipo_carencia <> :old.nr_seq_tipo_carencia) or
	(:new.nr_seq_tipo_carencia is not null and :old.nr_seq_tipo_carencia is null) or
	(:new.nr_seq_tipo_carencia is null and :old.nr_seq_tipo_carencia is not null))then
	
	select	nvl(max(ds_carencia),'')
	into	ds_anterior_w
	from	pls_tipo_carencia
	where	nr_sequencia	= :old.nr_seq_tipo_carencia;
	
	select	nvl(max(ds_carencia),'')
	into	ds_novo_w
	from	pls_tipo_carencia
	where	nr_sequencia	= :new.nr_seq_tipo_carencia;
	
	--Altera��o do tipo de car�ncia do contrato. 
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352346 , 'NR_CARENCIA_OLD=' || :old.nr_seq_tipo_carencia ||';'||'DS_CARENCIA_OLD='||ds_anterior_w||';'|| 'NR_CARENCIA_NEW='||:new.nr_seq_tipo_carencia||';'||'DS_CARENCIA_NEW='||ds_novo_w);	
end if;

if	((:new.ds_observacao <> :old.ds_observacao) or
	(:new.ds_observacao is not null and :old.ds_observacao is null) or
	(:new.ds_observacao is null and :old.ds_observacao is not null))then
	
	--Altera��o da observa��o.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352348 , 'DS_OBSERVACAO_OLD=' ||:old.ds_observacao||';'||'DS_OBSERVACAO_NEW='||:new.ds_observacao);	
end if;

if 	((:new.qt_dias_fora_abrang_ant <> :old.qt_dias_fora_abrang_ant) or 
	(:new.qt_dias_fora_abrang_ant is not null and :old.qt_dias_fora_abrang_ant is null) or
	(:new.qt_dias_fora_abrang_ant is null and :old.qt_dias_fora_abrang_ant is not null))then
	
	--Altera��o na contagem de dias fora abrang�ncia anterior.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352350 , 'QT_DIAS_OLD=' ||:old.qt_dias_fora_abrang_ant||';'||'QT_DIAS_NEW='||:new.qt_dias_fora_abrang_ant);
end if;

if	((trunc(:new.dt_fim_vigencia, 'DAY') <> trunc(:old.dt_fim_vigencia,'DAY'))or
	(:new.dt_fim_vigencia is not null and :old.dt_fim_vigencia is null) or
	(:new.dt_fim_vigencia is null and :old.dt_fim_vigencia is not null))then
	
	--Altera��o da data de fim de vig�ncia para a car�ncia.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352342 , 'DT_CARENCIA_OLD=' ||:old.dt_fim_vigencia||';'||'DT_CARENCIA_NEW='||:new.dt_fim_vigencia);	
end if;

if 	((trunc(:new.dt_inicio_vigencia,'DAY') <> trunc(:old.dt_inicio_vigencia,'DAY')) or
	(:new.dt_inicio_vigencia is not null and :old.dt_inicio_vigencia is null) or
	(:new.dt_inicio_vigencia is null and :old.dt_inicio_vigencia is not null))then
	
	--Altera��o da data de in�cio de vig�ncia para a car�ncia.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352343 , 'DT_CARENCIA_OLD=' ||:old.dt_inicio_vigencia||';'||'DT_CARENCIA_NEW='||:new.dt_inicio_vigencia);	
end if;

if	(:new.qt_dias <> :old.qt_dias) then

	--Altera��o da quantidade de dias para a regra de car�ncia.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352351 , 'QT_DIAS_OLD=' ||:old.qt_dias||';'||'QT_DIAS_NEW='||:new.qt_dias);
end if;

if 	(:new.ie_mes_posterior <> :old.ie_mes_posterior) then
	
	select 	decode(:new.ie_mes_posterior,'S','Sim','N','N�o', ' '),
		decode(:old.ie_mes_posterior,'S','Sim','N','N�o', ' ')
	into	ds_novo_w,
		ds_anterior_w
	from 	dual;
	
	--Altera��o da vig�ncia a partir do 1� dia do m�s subsequente.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352353 , 'DS_OLD=' ||ds_anterior_w||';'||'DS_NEW='||ds_novo_w);
end if;	

if 	(ds_alteracao_w	<> ' ') then
	insert into pls_carencia_log 
		(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec, ds_alteracao, nr_seq_carencia)
	values	(pls_carencia_log_seq.NextVal, sysdate, null,:new.nm_usuario, null, ds_alteracao_w, :old.nr_sequencia);
end if;

end;
/
