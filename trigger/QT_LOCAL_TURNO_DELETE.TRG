CREATE OR REPLACE TRIGGER qt_local_turno_Delete
before delete ON qt_local_turno
FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from quimio_controle_horario
where	dt_agenda >= trunc(sysdate)
and	nr_seq_local	 = :old.nr_seq_local;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/
