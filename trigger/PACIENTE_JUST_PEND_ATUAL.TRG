create or replace
trigger paciente_just_pend_atual
after insert or update or delete on PACIENTE_JUSTIFICATIVA
for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_justificativa_w	varchar2(10);
nr_seq_reg_elemento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select  max(IE_LIB_JUSTIFICATIVA)
into	   ie_lib_justificativa_w
from	   parametro_medico
where   cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

begin

	if	(inserting) or
		(updating) then

		if	(nvl(ie_lib_justificativa_w,'N') = 'S') then
			if	(:new.dt_liberacao is null) then
				ie_tipo_w := 'JS';
			elsif	(:old.dt_liberacao is null) and
					(:new.dt_liberacao is not null) then
				ie_tipo_w := 'XJS';
			end if;


			if	(ie_tipo_w	is not null) then
				Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_reg_elemento_w);
			end if;

			
		end if;
	elsif	(deleting) then
	
		delete from pep_item_pendente
		where 	IE_TIPO_REGISTRO = 'JS'
		and	nr_seq_registro = :old.nr_sequencia
		and	nvl(IE_TIPO_PENDENCIA,'L')	= 'L';
		
		commit;
		
	end if;

exception
when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/
