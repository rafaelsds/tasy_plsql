create or replace trigger smart_atendimento_pac_alta
after insert or update on atendimento_paciente
for each row

declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
nr_seq_interno_w		NUMBER(10);
atend_atepacu_w			atendimento_paciente.nr_Atendimento%type;
	
begin
if (updating) and (:new.dt_cancelamento is null) and 
	(((:new.dt_saida_real is not null) and (:old.dt_saida_real is null)) or ((:new.dt_saida_real is null) and (:old.dt_saida_real is not null))) then
	
	if	(updating) and (:new.dt_saida_real is not null) and (:old.dt_saida_real is null) then
		reg_integracao_p.ie_operacao		:=	'A';
	elsif	(updating) and (:new.dt_saida_real is null) and (:old.dt_saida_real is not null) then
		reg_integracao_p.ie_operacao		:=	'E';
	end if;

	reg_integracao_p.nr_atendimento			:=	:new.nr_atendimento;
	reg_integracao_p.cd_pessoa_fisica		:=	:new.cd_pessoa_fisica;
	reg_integracao_p.ie_tipo_atendimento		:= 	:new.ie_tipo_atendimento;
	reg_integracao_p.nr_seq_tipo_admissao_fat	:= 	:new.nr_seq_tipo_admissao_fat;
	reg_integracao_p.cd_motivo_alta			:= 	:new.cd_motivo_alta;
	
	select obter_atepacu_paciente(:new.nr_Atendimento,'IAA')
	into atend_atepacu_w
	from dual;
	
	if (atend_atepacu_w <> 0) then
		gerar_int_padrao.gravar_integracao('114', :new.nr_atendimento, nvl(obter_usuario_ativo, :new.nm_usuario), reg_integracao_p);
	end if;
elsif (updating) and (((:old.dt_cancelamento is null) and (:new.dt_cancelamento is not null)) or
		      (nvl(:old.cd_medico_referido,'0') <> nvl(:new.cd_medico_referido,'0')) or
		      (nvl(:old.cd_medico_resp,'0') <> nvl(:new.cd_medico_resp,'0'))) then
	
	select obter_atepacu_paciente(:new.nr_atendimento,'IAA')
	into	nr_seq_interno_w
	from	dual;
	if (nr_seq_interno_w <> 0) then
		reg_integracao_p.ie_operacao				:=	'A';
		reg_integracao_p.nr_atendimento				:=	:new.nr_atendimento;
		reg_integracao_p.cd_pessoa_fisica			:=	:new.cd_pessoa_fisica;
		reg_integracao_p.ie_tipo_atendimento		:= 	:new.ie_tipo_atendimento;
		reg_integracao_p.nr_seq_tipo_admissao_fat	:= 	:new.nr_seq_tipo_admissao_fat;	
		reg_integracao_p.cd_motivo_alta			:= 	:new.cd_motivo_alta;
		
		gerar_int_padrao.gravar_integracao('308',nr_seq_interno_w,nvl(obter_usuario_ativo, :new.nm_usuario), reg_integracao_p);	
	end if;
	
end if;

end smart_atendimento_pac_alta;
/