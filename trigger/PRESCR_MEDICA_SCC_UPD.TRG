create or replace TRIGGER prescr_medica_scc_upd BEFORE
    UPDATE OF cd_medico ON prescr_medica
    FOR EACH ROW
DECLARE
    ds_param_integ_hl7_scc_w   VARCHAR2(2000);
    nr_sequencia_scc_w         prescr_procedimento.nr_sequencia%TYPE;
BEGIN
    IF (wheb_usuario_pck.get_ie_executar_trigger  = 'N') THEN
       GOTO Final;
    END IF;
    <<Final>>
    IF ( wheb_usuario_pck.is_evento_ativo(942) = 'S' ) AND ( :new.dt_liberacao IS NOT NULL ) AND ( :new.cd_medico <> :old.cd_medico ) THEN
        SELECT MAX(a.nr_sequencia)
        INTO nr_sequencia_scc_w
        FROM
            prescr_procedimento   a,
            lab_exame_equip       e,
            equipamento_lab       f,
            exame_laboratorio     c
        WHERE
            a.nr_seq_exame = c.nr_seq_exame
            AND e.cd_equipamento = f.cd_equipamento
            AND f.ds_sigla = 'SCC'
            AND a.nr_seq_exame = e.nr_seq_exame
            AND a.nr_prescricao = :new.nr_prescricao
            AND a.dt_cancelamento IS NULL
            AND a.dt_suspensao IS NULL
            AND a.dt_envio_integracao IS NOT NULL
            AND ROWNUM = 1;

        IF ( nr_sequencia_scc_w IS NOT NULL ) THEN
            ds_param_integ_hl7_scc_w := 'NR_PRESCRICAO_P='
                                        || :new.nr_prescricao
                                        || '#@#@IE_ALTERACAO_MEDICO_P='
                                        || 'S'
                                        || '#@#@';

            gravar_agend_integracao(942, ds_param_integ_hl7_scc_w);
        END IF;

    END IF;
END;
/
