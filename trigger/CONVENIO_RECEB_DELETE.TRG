CREATE OR REPLACE TRIGGER CONVENIO_RECEB_DELETE
BEFORE DELETE ON CONVENIO_RECEB
FOR EACH ROW

DECLARE

NR_SEQ_MOVTO_W		NUMBER(10,0);
IE_STATUS_W		VARCHAR2(01);
DT_FECHAMENTO_W		DATE := NULL;

BEGIN


IF	(:OLD.IE_STATUS <> 'N') THEN
	--N�o pode ser excluido um recebimendo j� vinculado');
	Wheb_mensagem_pck.exibir_mensagem_abort(261630);
END IF;

/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_sequencia,null,'RC',:old.dt_recebimento,'E',:old.nm_usuario);

END;
/
