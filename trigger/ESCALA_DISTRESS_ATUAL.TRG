create or replace trigger escala_distress_atual
before insert or update on escala_distress_k6
for each row

declare

  qt_pontuacao_w number(10, 0);
  sql_w          varchar2(200);
  
begin
  /** Medical Device **/
  if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	  begin
		sql_w := 'call obter_score_escala_distress_md(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10) into :qt_pontuacao_w';

		execute immediate sql_w using  in :new.ie_exausto,
									   in :new.ie_nervoso,
									   in :new.ie_nervoso_acalma,
									   in :new.ie_sem_esperanca,
									   in :new.ie_inquieto,
									   in :new.ie_inquieto_parado,
									   in :new.ie_deprimido,
									   in :new.ie_deprimido_animar,
									   in :new.ie_esforco,
									   in :new.ie_sem_valor,			                    
									  out qt_pontuacao_w;
	  exception
		when others then
		  qt_pontuacao_w := null;  
	  end;
									
	  :new.qt_pontuacao := qt_pontuacao_w;
  end if;
end;
/