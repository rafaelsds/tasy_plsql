create or replace trigger bandeira_cartao_cr_atual
before insert or update on bandeira_cartao_cr
for each row
declare

ds_conta_contabil_w		varchar2(40);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
ds_conta_contabil_w		:= substr(wheb_mensagem_pck.get_texto(354087,''),1,40);

ctb_consistir_conta_titulo(:new.cd_conta_contabil, ds_conta_contabil_w);
end if;

end;
/
