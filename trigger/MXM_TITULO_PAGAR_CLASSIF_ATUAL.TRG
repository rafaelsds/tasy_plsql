create or replace trigger mxm_titulo_pagar_classif_atual
after insert on titulo_pagar_classif
for each row

declare
ie_situacao_w	varchar2(1);
begin
select	ie_situacao
into	ie_situacao_w
from	titulo_pagar
where	nr_titulo = :new.nr_titulo;

if	(ie_situacao_w = 'A') then
	mxm_gerar_integracao(:new.nr_titulo,'TP',:new.nm_usuario);
end if;
end;
/