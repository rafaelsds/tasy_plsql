CREATE OR REPLACE TRIGGER man_ordem_servico_update
BEFORE UPDATE ON man_ordem_servico
FOR EACH ROW
DECLARE
nm_user_w			Varchar2(50);
qt_doc_erro_w			Number(10);
qt_existe_orc_w			number(10,0);
qt_reg_w				number(10);
ie_gerar_grupo_des_w		varchar2(01) := 'S';
NR_SEQ_GERENCIA_W		number(10,0);
nm_usuario_lider_sup_w		varchar2(15);
ie_desenv_w             		varchar2(01) := 'S';
nr_seq_plano_w      	    	number(10,0);
nr_seq_meta_os_w 	       		number(10,0);
ie_tipo_os_meta_w			varchar2(03);
dt_geracao_meta_w		date;

nr_seq_proj_vinc_w			number(10,0);
nr_seq_proj_orig_w			number(10,0);
nr_seq_proj_acomp_w		number(10,0);
ie_produto_w			varchar2(15);
cd_setor_usuario_w			number(05,0) := wheb_usuario_pck.get_cd_setor_atendimento;
qt_ativ_programacao_w		number(10,0);
qt_hist_code_review_w		number(10,0);


BEGIN
ie_gerar_grupo_des_w	:= nvl(Obter_Valor_Param_Usuario(299,228,Obter_Perfil_Ativo,:new.nm_usuario,0),'S');

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
select	username
into	nm_user_w
from	user_users;

if	(:new.ie_classificacao <> :old.ie_classificacao) then
	:new.nm_usuario_classif	:= :new.nm_usuario;
end if;

if	(:new.ie_prioridade <> :old.ie_prioridade) then
	:new.nm_usuario_prioridade	:= :new.nm_usuario;
	if (nm_user_w = 'CORP') then
		sla_dashboard_pck.validar_alt_prioridade_sla(
					:new.nr_sequencia,:new.nm_usuario,
					:new.ie_prioridade, :new.ie_classificacao, 
					:new.nr_seq_localizacao, :new.nr_grupo_trabalho, :new.dt_ordem_servico, 
					:new.dt_inicio_real, :new.nr_seq_estagio, :new.nr_seq_equipamento, :new.nr_seq_classif, :new.ie_parado);
	end if;
end if;

if	(nm_user_w = 'CORP') and
	(:new.ie_classificacao = 'E') and
	(:new.nr_seq_meta_pe is null) and
	(upper(:new.nm_usuario) <> 'WEBSERVICE') and
	(:new.nr_seq_estagio in (2,9,791,1511)) then
	select	count(*)
	into	qt_doc_erro_w
	from	man_doc_erro
	where	nr_seq_ordem	= :new.nr_sequencia
	and	dt_liberacao is not null;

	if	(qt_doc_erro_w = 0) then 
			wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(173237); 
	end if;
end if;

if	(:new.nr_seq_grupo_sup is null) and
	(:new.cd_funcao is not null) then
	select	min(b.nr_sequencia)
	into	:new.nr_seq_grupo_sup
	from	grupo_suporte b,
		funcao_grupo_sup a
	where	a.nr_seq_grupo	= b.nr_sequencia
	and	a.cd_funcao	= :new.cd_funcao;
	
	if	(:new.nr_seq_grupo_sup is not null) then
		select	max(a.nm_usuario_lider)
		into	nm_usuario_lider_sup_w
		from	grupo_suporte a
		where	a.nr_sequencia = :new.nr_seq_grupo_sup;
	end if;
end if;

if	(nm_user_w = 'CORP') then
	begin
	
	if	(nvl(:old.ie_grau_satisfacao,'XX') <> nvl(:new.ie_grau_satisfacao,'XX')) and
		(:new.ie_grau_satisfacao is not null) and
		(:new.ie_status_ordem = '3') and
		(:NEW.NR_SEQ_ESTAGIO	not in (9)) and --Se ja estiver encerrada, nao alterar o estagio para Encerramento solicitado pelo cliente
		(:new.dt_fim_real is not null) then
		:new.dt_fim_real	:= null;
		:new.ie_status_ordem	:= '2';
			:new.nr_seq_estagio	:= 511;
		end if;	

	if	(:new.nr_seq_estagio <> :old.nr_seq_estagio) then
		begin

		select	max(ie_desenv)
		into	ie_desenv_w
		from	man_estagio_processo
		where	nr_sequencia    = :new.nr_seq_estagio;

		select	substr(proj_obter_dados_ordem_serv(:new.nr_sequencia, 'PROJ'),1,10) nr_seq_proj_vinc,
			substr(proj_obter_dados_ordem_serv(:new.nr_sequencia, 'PROJD'),1,10) nr_seq_proj_orig,
			substr(proj_obter_dados_ordem_serv(:new.nr_sequencia, 'PROJA'),1,10) nr_seq_proj_acomp
		into	nr_seq_proj_vinc_w,
			nr_seq_proj_orig_w,
			nr_seq_proj_acomp_w
		from	dual;
		
		if	(ie_desenv_w = 'S') and
			(nvl(nr_seq_proj_vinc_w,'0') = '0') and
			(nvl(nr_seq_proj_orig_w,'0') = '0') and
			(nvl(nr_seq_proj_acomp_w,'0') = '0') then
			
			select  max(nr_seq_gerencia)
			into    nr_seq_gerencia_w
			from    grupo_desenvolvimento
			where   nr_sequencia            = :new.nr_seq_grupo_des;

			select  nvl(max(nr_sequencia),0)
			into    nr_seq_plano_w
			from    desenvolvimento_plano_meta
			where   nr_seq_gerencia         = nr_seq_gerencia_w
			and     dt_meta_real is null;
						
			select	max(dt_geracao)
			into	dt_geracao_meta_w
			from	desenvolvimento_plano_meta
			where	nr_sequencia	= nr_seq_plano_w;

			if	(nr_seq_plano_w > 0) and
				(dt_geracao_meta_w >= :new.dt_ordem_servico) then
				select  count(*)
				into    qt_reg_w
				from    desenv_plano_meta_os
				where   nr_seq_plano            = nr_seq_plano_w
				and	NR_SEQ_ORDEM_SERV       = :new.nr_sequencia;

				if	(qt_reg_w = 0) then
					select  desenv_plano_meta_os_seq.nextval
					into    nr_seq_meta_os_w
					from    dual;

					if	(obter_se_data_periodo(trunc(:new.dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), sysdate -60) = 'S') then
						ie_tipo_os_meta_w	:= '0';
					elsif	(obter_se_data_periodo(trunc(:new.dt_ordem_servico, 'dd'), sysdate - 59, sysdate -45) = 'S') then
						ie_tipo_os_meta_w	:= '1';
					elsif	(obter_se_data_periodo(trunc(:new.dt_ordem_servico, 'dd'), sysdate - 44, sysdate -30) = 'S') then
						ie_tipo_os_meta_w	:= '2';
					elsif	(obter_se_data_periodo(trunc(:new.dt_ordem_servico, 'dd'), sysdate - 29, sysdate -15) = 'S') then
						ie_tipo_os_meta_w	:= '3';
					elsif	(obter_se_data_periodo(trunc(:new.dt_ordem_servico, 'dd'), sysdate - 14, sysdate -7) = 'S') then
						ie_tipo_os_meta_w	:= '4';
					elsif	(obter_se_data_periodo(trunc(:new.dt_ordem_servico, 'dd'), sysdate - 6, sysdate) = 'S') then
						ie_tipo_os_meta_w	:= '5';
					end if;
					
					if	(ie_tipo_os_meta_w is not null) then
						insert  into desenv_plano_meta_os
								(nr_sequencia,
								nr_seq_plano,
								nr_seq_ordem_serv,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ie_tipo_os_meta,
								nr_seq_estagio_orig,
								nr_seq_estagio_meta,
								ie_recorrente)
						values		(nr_seq_meta_os_w,
								nr_seq_plano_w,
								:new.nr_sequencia,
								sysdate,
								:new.nm_usuario,
								sysdate,
								:new.nm_usuario,
								ie_tipo_os_meta_w,
								:new.nr_seq_estagio,
								null, 'S');
					end if;
				end if;
			end if;
		
		
		end if;
		sla_dashboard_pck.registrar_alt_estag_sla(
					:new.nr_sequencia,:new.nm_usuario,
					:new.ie_prioridade, :new.ie_classificacao, 
					:new.nr_seq_localizacao, :new.nr_grupo_trabalho, :new.dt_ordem_servico, 
					:new.dt_inicio_real, :new.nr_seq_estagio, :new.nr_seq_equipamento, :new.nr_seq_classif, :new.ie_parado);
		end;
	end if;

	/*if 	(:new.nr_seq_estagio = 231 ) and --Aguardando Triagem	OS968691
		(:new.ie_classificacao in ('T','S')) then --Solicitacao / Sugestao
		begin
		select	max(nr_seq_gerencia)
		into	nr_seq_gerencia_w
		from	grupo_desenvolvimento
		where	nr_sequencia	= :new.nr_seq_grupo_des;
		
		if	(nr_seq_gerencia_w in (2,3,4,7,11,17,20,21)) then
			:new.nr_seq_estagio	:= 1051; --Aguardando triagem desenvolvimento
			
			if	(nr_seq_gerencia_w = 2) then
				:new.nr_seq_estagio := 131;
			end if;
		end if;
		end;
	end if;*/
	
	if 	(:new.nr_seq_estagio = 231) and /* Aguardando Triagem*/
		(nvl(nm_usuario_lider_sup_w,'X') <> 'X') then
		begin
		select	count(*)
		into	qt_reg_w
		from	man_ordem_servico_exec
		where	nm_usuario_exec	= nm_usuario_lider_sup_w
		and	nr_seq_ordem	= :new.nr_sequencia;

		if	(qt_reg_w = 0) then
			insert into man_ordem_servico_exec(
				nr_sequencia,
				nr_seq_ordem,
				dt_atualizacao,
				nm_usuario,
				nm_usuario_exec,
				qt_min_prev,
				dt_recebimento,
				nr_seq_funcao)
			select	man_ordem_servico_exec_seq.nextval,
				:new.nr_sequencia,
				sysdate,
				'Tasy',
				nm_usuario_lider_sup_w,
				45,
				sysdate,
				31
			from	dual;
		end if;
		
		if	(:new.nm_usuario_exec is null) then
			:new.nm_usuario_exec := nm_usuario_lider_sup_w;
		end if;
		end;
	end if;

	if	(:new.ie_classificacao = 'E') and
		(:new.ie_prioridade_sup is null) then /*Anderson  OS223881 - Conforme IT suporte*/
		:new.ie_prioridade_sup := 10;
	end if;

	/*if	(:new.nr_seq_estagio <> :old.nr_seq_estagio) and
		(:new.nr_seq_estagio = 511) and -- Encerramento solicitado pelo cliente
		(:new.ie_classificacao in ('T','S')) and --Solicitacao / Sugestao
		(nvl(:new.ie_grau_satisfacao,'N') in ('O','B','E','N')) and
		(:new.nr_seq_tipo_solucao is not null) then
		begin
		select	count(*)
		into	qt_existe_orc_w
		from	man_ordem_servico_orc
		where	nr_seq_ordem = :new.nr_sequencia
		and	dt_aprovacao is null 
		and	dt_reprovacao is null;
		
		if	(qt_existe_orc_w = 0) then
			:new.dt_inicio_real	:= :new.dt_inicio_previsto;
			:new.dt_fim_real	:= sysdate;
			:new.nr_seq_estagio	:= 9;
			:new.ie_status_ordem	:= 3;
			:new.nm_usuario		:= 'WebService';
			:new.dt_atualizacao	:= sysdate;
			:new.nm_usuario_encer	:= 'WebService';
		end if;
		end;
	end if;*/ -- 972316
	
	end;
end if;

if	(:old.nr_seq_meta_pe is not null) and
	(:old.ie_status_ordem = 3) and
	(:old.ie_status_ordem <> :new.ie_status_ordem) then
	:new.dt_validacao_pe := '';
end if;

if	(nm_user_w = 'CORP') and
	(:new.ie_status_ordem = 3) and
	(:old.ie_status_ordem <> :new.ie_status_ordem) and
	(upper(:new.nm_usuario) <> 'WEBSERVICE') then

	/*Verifica se existe atividade de programacao para esta OS*/
	select	count(1) 
	into	qt_ativ_programacao_w
	from	man_ordem_serv_ativ
	where	nr_seq_funcao in (1288,11,551)
	and		nr_seq_ordem_serv = :new.nr_sequencia;

	if	(qt_ativ_programacao_w > 0)then

	/*Verifica se existe historico do tipo Code Review*/
		select	count(1) 
		into 	qt_hist_code_review_w
		from	man_ordem_serv_tecnico
		where	nr_seq_tipo = 163
		and		nr_seq_ordem_serv = :new.nr_sequencia;
	end if;

	if (:new.dt_ordem_servico > to_date('11/05/2016', 'dd/mm/yyyy')) then --Somente ira pedir aprovacao de code review se a OS foi criada DEPOIS que foi implementado essa rotina de Code Review (11/05/2016) . Mais detalhes verificar OS: 1402146
		if	(qt_hist_code_review_w = 0) then
			wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(458500); /*Atencao! Para cada atividade de programacao deve existir um historico de "Aprovacao de Code Review"*/
		end if;
	end if;		
end if;

if	(nm_user_w = 'CORP') and
	(:new.ie_status_ordem = 3) and
	(:old.ie_status_ordem <> :new.ie_status_ordem) and
	(upper(:new.nm_usuario) <> 'WEBSERVICE') then

	/*Verifica se existe atividade de programacao para esta OS*/
	select	count(1) 
	into	qt_ativ_programacao_w
	from	man_ordem_serv_ativ
	where	nr_seq_funcao in (1288,11,551)
	and		nr_seq_ordem_serv = :new.nr_sequencia;

	if	(qt_ativ_programacao_w > 0)then

	/*Verifica se existe historico do tipo "Teste do programador" */
		select	count(1) 
		into 	qt_hist_code_review_w
		from	man_ordem_serv_tecnico
		where	nr_seq_tipo = 12
		and		nr_seq_ordem_serv = :new.nr_sequencia;

		if	(qt_hist_code_review_w = 0) then
			wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(798715); /*E obrigatorio o registro de ao menos um historico do tipo "Teste do programador" para encerrar esta OS! */
		end if;

		/*Verifica se existe historico do tipo "Teste do analista" */
		select	count(1) 
		into 	qt_hist_code_review_w
		from	man_ordem_serv_tecnico
		where	nr_seq_tipo = 31
		and		nr_seq_ordem_serv = :new.nr_sequencia;	

		if	(qt_hist_code_review_w = 0) then
			wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(798729); /*E obrigatorio o registro de ao menos um historico do tipo "Teste do analista" para encerrar esta OS!*/
		end if;
	end if;
end if;


<<Final>>
qt_reg_w	:= 0;
END;
/
