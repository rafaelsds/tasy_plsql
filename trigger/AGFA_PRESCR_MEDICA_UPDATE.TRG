CREATE OR REPLACE TRIGGER TASY.AGFA_Prescr_Medica_Update
before update ON TASY.PRESCR_MEDICA for each row
declare

ie_tipo_atendimento_w	varchar2(2);

nr_sequencia_w		number(10);
nr_seq_proc_interno_w	number(10);
cd_estabelecimento_w	number(10);
dt_prev_execucao_w	date;
ie_urgencia_w		varchar2(1);
nm_usuario_w		varchar2(15);
ds_obs_prescr_w		varchar2(255);
ds_proc_exame_w		varchar2(100);
ie_lado_w		varchar2(1);

ie_retorno_w		Varchar2(256);
--ie_retorno_w		Boolean;

cursor c01 is
		select	a.nr_sequencia,
			a.nr_seq_proc_interno,
			nvl(a.dt_prev_execucao,:new.dt_prescricao),
			decode(a.ie_urgencia, 'S', 'A', 'N'),
			a.nm_usuario,
			substr(a.ds_observacao,1,254),
			b.ds_proc_exame,
			decode(decode(a.ie_lado,null,'A',a.ie_lado), 'A', 'X', a.ie_lado)
		from	procedimento c,
			proc_interno b,
			prescr_procedimento a
		where	a.nr_seq_proc_interno	= b.nr_sequencia
		  and	a.cd_procedimento	= c.cd_procedimento
		  and	a.ie_origem_proced	= c.ie_origem_proced
		  and	c.ie_classificacao	= '1'
		  and	(a.nr_acesso_dicom	is null or
			a.ie_origem_inf <> '3')
		  and	a.nr_prescricao		= :new.nr_prescricao
		  and	not exists (	select	1
					from 	proc_int_proc_prescr c
					where	c.nr_seq_proc_int_adic	= b.nr_sequencia
					and	a.nr_seq_proc_princ is not null
					and	nvl(c.ie_proced_vinculado,'N') = 'S');

begin

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then

	select	max(decode(ie_tipo_atendimento, 1, 'H1', 3, 'A2', 'AE'))
	into	ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	open c01;
	loop
	fetch c01 into	nr_sequencia_w,
			nr_seq_proc_interno_w,
			dt_prev_execucao_w,
			ie_urgencia_w,
			nm_usuario_w,
			ds_obs_prescr_w,
			ds_proc_exame_w,
			ie_lado_w;
		exit when c01%notfound;

	insert into log_tasy (DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG) values (sysdate, 'Debug', 11111,
					'AGFA_Gera_Solicitacao(	' || :new.nr_prescricao || ',0^REQUESTED,' ||
					ie_urgencia_w ||',' || dt_prev_execucao_w ||','|| dt_prev_execucao_w ||','||
					nr_seq_proc_interno_w ||','|| nm_usuario_w ||','|| :new.cd_medico ||','||
					:new.cd_pessoa_fisica ||','|| :new.nr_atendimento ||','||
					ie_tipo_atendimento_w ||','|| nr_seq_proc_interno_w ||','||
					ds_proc_exame_w ||','||	nr_sequencia_w ||','|| :new.ds_observacao ||','||
					ds_obs_prescr_w ||','|| ie_retorno_w ||')');

		AGFA_Gera_Solicitacao(	to_char(:new.nr_prescricao),
								'0^REQUESTED',
								ie_urgencia_w,
								dt_prev_execucao_w,
								dt_prev_execucao_w,
								to_char(:new.cd_estabelecimento),
								nm_usuario_w,
								:new.cd_medico,
								:new.cd_pessoa_fisica,
								to_char(:new.nr_atendimento),
								ie_tipo_atendimento_w,
								to_char(nr_seq_proc_interno_w) || ie_lado_w,
								ds_proc_exame_w,
								to_char(nr_sequencia_w),
								:new.ds_observacao,
								ds_obs_prescr_w,
								ie_retorno_w);

		if	(ie_retorno_w is not null) then
			--Retorno AGFA:' || ie_retorno_w);
			Wheb_mensagem_pck.exibir_mensagem_abort(261432, 'RETORNO='|| ie_retorno_w);
		end if;
	end loop;
	close c01;

end if;

end AGFA_Prescr_Medica_Update;
/