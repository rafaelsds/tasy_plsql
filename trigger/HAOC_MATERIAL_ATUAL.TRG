CREATE OR REPLACE 
TRIGGER HAOC_Material_Atual
BEFORE INSERT OR UPDATE ON Material
FOR EACH ROW
DECLARE
qt_itens_w		Number(15,0);

BEGIN

if	(:new.CD_SISTEMA_ANT is null) and
	(:new.ie_situacao 	= 'A') then
	raise_application_error(-20011,'O material deve ter o codigo do HAOC informado para estar ativo.#@#@');
end if;

END;
/