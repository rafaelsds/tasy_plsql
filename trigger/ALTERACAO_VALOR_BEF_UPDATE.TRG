create or replace trigger alteracao_valor_bef_update
before update on alteracao_valor
for each row

declare
cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if 	(nvl(:new.nr_titulo, 0) <> 0) then

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	titulo_receber
	where	nr_titulo = :new.nr_titulo;
	
	/* Projeto Davita - Nao deixa gerar movimento contabil apos fechamento da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w), :new.dt_alteracao);
	
end if;
end if;

end;
/


