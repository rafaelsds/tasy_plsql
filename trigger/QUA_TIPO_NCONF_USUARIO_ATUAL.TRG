create or replace trigger qua_tipo_nconf_usuario_atual
before insert or update on qua_tipo_nconf_usuario
for each row

declare

begin
if	(:new.cd_perfil is not null) and
	(:new.nm_usuario_lib is not null) then
	/*(-20011,'Deve ser selecionado apenas o perfil ou usu�rio para esta regra.');*/
	wheb_mensagem_pck.exibir_mensagem_abort(263180);
end if;

end;
/
