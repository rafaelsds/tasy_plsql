create or replace trigger CORPO_HUMANO_ATEND_BEF_DELETE
before delete on CORPO_HUMANO_ATEND
for each row
declare

begin
  delete from CORPO_HUMANO_ATEND_REPRES
  where NR_SEQ_ATEND = :old.NR_SEQ_ATEND;
end;
/