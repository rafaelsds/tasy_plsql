create or replace trigger pls_conta_copart_aprop_alt_pag
before insert on pls_conta_copartic_aprop
for each row

declare

nr_seq_pagador_conta_copart_w	pls_contrato_pagador.nr_sequencia%type;
nr_seq_pag_conta_copar_aprop_w	pls_contrato_pagador.nr_sequencia%type;

begin

nr_seq_pagador_conta_copart_w	:= null;

select	max(b.nr_seq_pagador)
into	nr_seq_pagador_conta_copart_w
from	pls_conta_coparticipacao a,
	pls_segurado b
where	b.nr_sequencia = a.nr_seq_segurado
and	a.nr_sequencia = :new.nr_seq_conta_coparticipacao;

if	(nr_seq_pagador_conta_copart_w is not null) then
	select	max(nr_seq_pagador_item)
	into	nr_seq_pag_conta_copar_aprop_w
	from	pls_pagador_item_mens
	where	nr_seq_pagador = nr_seq_pagador_conta_copart_w
	and	ie_tipo_item = '3'
	and	nr_seq_centro_apropriacao = :new.nr_seq_centro_apropriacao;

	if	(nr_seq_pag_conta_copar_aprop_w is null) then
		select	max(nr_seq_pagador_item)
		into	nr_seq_pag_conta_copar_aprop_w
		from	pls_pagador_item_mens
		where	nr_seq_pagador = nr_seq_pagador_conta_copart_w
		and	ie_tipo_item = '3'
		and	nr_seq_centro_apropriacao is null;

		if	(nr_seq_pag_conta_copar_aprop_w is null) then
			nr_seq_pag_conta_copar_aprop_w	:= nr_seq_pagador_conta_copart_w;
		else
			nr_seq_pagador_conta_copart_w	:= nr_seq_pag_conta_copar_aprop_w;
		end if;
	else
		nr_seq_pagador_conta_copart_w	:= nr_seq_pag_conta_copar_aprop_w;
	end if;
end if;

:new.nr_seq_pagador := nr_seq_pagador_conta_copart_w;

end pls_conta_copart_aprop_alt_pag;
/
