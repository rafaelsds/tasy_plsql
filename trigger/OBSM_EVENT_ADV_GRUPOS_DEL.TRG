create or replace trigger OBSM_EVENT_ADV_GRUPOS_DEL
before delete on EVENTOS_ADVERSOS_GRUPOS
for each row

declare
ds_log_w	varchar2(1500);

begin

ds_log_w := 'CD_GRUPO='||:old.CD_GRUPO||';CD_EXP_GRUPO='||:old.CD_EXP_GRUPO||';DT_ATUALIZACAO='||to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss');
ds_log_w := ds_log_w || ';NM_USUARIO='||:old.NM_USUARIO||';DT_ATUALIZACAO_NREC='||to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss')||';DS_DESCRICAO_CLIENTE='||:old.DS_DESCRICAO_CLIENTE;
ds_log_w := substr(ds_log_w || ';Stack='||dbms_utility.format_call_stack,1,1500);

gravar_log_tasy(51385, ds_log_w, :old.nm_usuario);

end;
/