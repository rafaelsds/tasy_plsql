create or replace trigger alteracao_portador_after
after insert on alteracao_portador
for each row

declare

cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;
vl_movimento_w          ctb_documento.vl_movimento%type;

cursor c01 is
        select  a.nm_atributo,
                a.cd_tipo_lote_contab
        from    atributo_contab a
        where   a.cd_tipo_lote_contab = 5
        and     a.nm_atributo in ( 'VL_ALTERACAO_PORTADOR', 'VL_TIT_ALT_PORTADOR');

c01_w           c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if      ( nvl(:new.nr_titulo, 0) <> 0 ) then
        begin

        select  cd_estabelecimento
        into    cd_estabelecimento_w
        from    titulo_Receber
        where   nr_titulo = :new.nr_titulo;

	/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_alteracao);
	
        open c01;
        loop
        fetch c01 into  
                c01_w;
        exit when c01%notfound;
                begin
                
                vl_movimento_w  :=      case    c01_w.nm_atributo
                                        when    'VL_ALTERACAO_PORTADOR'    then :new.vl_despesa
                                        when    'VL_TIT_ALT_PORTADOR'      then :new.vl_saldo_titulo
                                        else
                                                null
                                        end;
                
                if      (nvl(vl_movimento_w, 0) <> 0) and (nvl(:new.nr_seq_trans_fin, 0) <> 0) then
                        begin
                        
                        ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
                                                                                trunc(:new.dt_alteracao),
                                                                                c01_w.cd_tipo_lote_contab,
                                                                                :new.nr_seq_trans_fin,
                                                                                43,
                                                                                :new.nr_titulo,
                                                                                :new.nr_sequencia,
                                                                                0,
                                                                                vl_movimento_w,
                                                                                'ALTERACAO_PORTADOR',
                                                                                c01_w.nm_atributo,
                                                                                :new.nm_usuario);

                        end;
                end if;
                
                
                
                
                end;
        end loop;
        close c01;

        end;
end if;
end if;

end;
/
