create or replace
trigger atualizar_estab_estr_org
after update on estabelecimento for each row

begin
if	(Obter_Valor_Param_Usuario(911, 52, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo) = 'S') then
	begin
	  if (:new.ie_situacao = 'I') then
	   begin
	     update estrutura_organizacional set IE_SITUACAO = :NEW.IE_SITUACAO
		   where cd_estabelecimento = :new.cd_estabelecimento;
	   end;	
     end if;
	end;
end if;
end;
/