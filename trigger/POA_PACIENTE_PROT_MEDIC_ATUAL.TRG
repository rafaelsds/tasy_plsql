create or replace trigger POA_PACIENTE_PROT_MEDIC_atual
before insert or update on PACIENTE_PROTOCOLO_MEDIC
for each row

declare

qt_minuto_aplicacao_w	number(10);
qt_minuto_atual_w	number(10);
qt_hora_aplicacao_w		number(10);
qt_hora_atual_w		number(10);
qt_total_aplicacao_w	number(10,4);

pragma autonomous_transaction;

begin

if  (:new.nr_seq_diluicao is not null) and
	(:new.cd_material is not null) then

	if (obter_se_permite_diluente(:new.cd_material) = 'N') then
	
		exibir_erro_abortar('Esse medicamento n�o pode ser utilizado como diluente.',null);
	
	end if;

end if;


If (:new.nr_seq_diluicao is not null) and (:new.QT_MIN_APLICACAO is null) then

	Select  max(QT_MIN_APLICACAO),
		nvl(max(QT_HORA_APLICACAO),0)
	into	qt_minuto_atual_w,
		qt_hora_atual_w
	from	paciente_protocolo_medic
	Where 	nr_seq_paciente = :new.nr_seq_paciente
	and	NR_SEQ_MATERIAL = :new.nr_seq_diluicao
	and   	nr_seq_diluicao is null;
	
		qt_total_aplicacao_w 	:= (nvl(qt_hora_atual_w,0) * 60) + nvl(qt_minuto_atual_w,0);		
		qt_minuto_aplicacao_w := :new.QT_MIN_APLICACAO;
		
		if ( nvl(qt_minuto_aplicacao_w,0) <> qt_total_aplicacao_w ) then					
			:new.QT_MIN_APLICACAO := qt_total_aplicacao_w;
		end if;		

elsif	(:new.nr_seq_diluicao is null) and
	((:new.QT_HORA_APLICACAO is not null) or
	(:new.QT_MIN_APLICACAO is not null))then
	
	qt_total_aplicacao_w := (nvl(:new.QT_HORA_APLICACAO,0) * 60) + nvl(:new.QT_MIN_APLICACAO,0);
	
	
	qt_hora_aplicacao_w	:= 	trunc(dividir(qt_total_aplicacao_w,60));		
	qt_minuto_aplicacao_w	:= (qt_total_aplicacao_w - (qt_hora_aplicacao_w * 60));		
	
	
	if	(nvl(qt_minuto_aplicacao_w,0) > 0) or
		(nvl(qt_hora_aplicacao_w,0) > 0) then				
						
			Select  max(QT_MIN_APLICACAO)
			into	qt_minuto_atual_w
			from	paciente_protocolo_medic
			Where 	nr_seq_paciente = :new.nr_seq_paciente
			and	nr_seq_diluicao = :new.NR_SEQ_MATERIAL
			and   	nr_seq_diluicao is not null; 						
			

			if ( qt_minuto_atual_w <> qt_total_aplicacao_w ) then					
			
				update 	paciente_protocolo_medic
				set	QT_MIN_APLICACAO = qt_total_aplicacao_w							
				Where 	nr_seq_paciente = :new.nr_seq_paciente
				and	nr_seq_diluicao = :new.NR_SEQ_MATERIAL
				and   	nr_seq_diluicao is not null;

				commit;					
			end if;		
		
	end if;
	
			
end if;	
	

end;
/