create or replace
TRIGGER agenda_integrada_item_update
after insert or update ON agenda_integrada_item
FOR EACH ROW
DECLARE

cd_estabelecimento_w	Number(4);
cd_convenio_w		Number(5);
cd_categoria_w		Varchar2(10);
dt_inicio_agendamento_w	Date;
cd_plano_w		Varchar2(10);
vl_procedimento_w	Number(15,2);
vl_aux_w		Number(15,4);
ds_aux_w		Varchar2(10);
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(10);
cd_convenio_ww		Number(5);
cd_categoria_ww		Varchar2(10);
cd_plano_ww		Varchar2(10);
ie_tipo_convenio_w	Number(3);
ie_regra_w		Number(5);
ie_glosa_w		Varchar2(1);
cd_usuario_convenio_w	Varchar2(30);
qt_idade_w		Number(3);
dt_nascimento_w		Date;
ie_Sexo_w		Varchar2(1);
cd_pessoa_fisica_w	Varchar2(10);
CD_EDICAO_AMB_w         Number(6);
VL_LANC_AUTOM_W         Number(15,2);
ie_calcula_lanc_auto_w	Varchar2(1);
ie_atualiza_medico_req_w Varchar2(1);
vl_custo_operacional_w	Number(15,2);
vl_anestesista_w	Number(15,2);
vl_medico_w		Number(15,2);
vl_auxiliares_w		Number(15,2);
vl_materiais_w		Number(15,2);
cd_medico_item_w	Varchar2(10);
ie_tipo_Atendimento_w	Number(3);
nr_seq_ageint_w		number(10);
cd_conv_item_w		number(5);
cd_categ_item_w		varchar2(10);
cd_plano_item_w		varchar2(10);
nr_seq_regra_w		number(10);
nr_minuto_duracao_w	number(10);
ie_resp_autor_w		varchar2(10);
cd_medico_solicitante_w varchar2(10);

ie_edicao_w                  varchar2(1);
cd_edicao_ajuste_w      number(10);
qt_item_edicao_w         number(10);
cd_estab_agenda_w	number(5);
ie_perm_agenda_w	varchar2(1)	:= 'S';
qt_estab_user_w		number(10);
ie_estab_usuario_w	varchar2(1);
nm_paciente_w		varchar2(60);
cd_paciente_agenda_w	varchar2(10);
nm_paciente_agenda_w	varchar2(60);
ie_pacote_w		varchar2(1);
ie_inserir_prof_w	varchar2(1);
ds_erro_w		varchar2(255);
dt_nascimento_Ww	date;
nr_seq_tipo_classif_pac_w	number(10);
ie_orient_long_w	varchar2(1);

Cursor C01 is
	select	a.cd_medico,
		c.cd_estabelecimento,
		ageint_obter_se_regra_med_ex(a.cd_medico, c.cd_Agenda, :new.nr_seq_proc_interno, :new.nr_seq_agenda_int, cd_procedimento_w, ie_origem_proced_w)
	from    agenda c,
		pessoa_fisica b,
		agenda_medico a
	where   Obter_Se_Ageint_Item(a.cd_agenda, :new.nr_seq_agenda_int, cd_estabelecimento_w, :new.nr_seq_proc_interno, :new.cd_Especialidade,

:new.cd_medico, a.cd_medico, cd_procedimento_w, ie_origem_proced_w,:new.nr_sequencia, qt_idade_w) = 'S'
	and     a.cd_medico    			= b.cd_pessoa_fisica
	and	c.cd_agenda			= a.cd_agenda
	and	nvl(c.ie_agenda_integrada,'N')	= 'S'
	and     a.ie_situacao  			= 'A'
	and	c.ie_situacao			= 'A'
	and	c.cd_tipo_Agenda = 2
	and	((qt_idade_w	>= nvl(qt_idade_min,0)
	and	qt_idade_w	<= nvl(qt_idade_max,999))
	or	qt_idade_w = 0);

BEGIN

select	nvl(max(Obter_Valor_Param_Usuario(869, 7, Obter_Perfil_Ativo, :new.nm_usuario, 0)), 'S'),
	nvl(max(Obter_Valor_Param_Usuario(869, 81, Obter_Perfil_Ativo, :new.nm_usuario, 0)), 'N')
into	ie_calcula_lanc_auto_w,
	ie_atualiza_medico_req_w
from 	dual;
begin
select	cd_estabelecimento,
	cd_convenio,
	cd_categoria,
	dt_inicio_agendamento,
	cd_plano,
	cd_usuario_convenio,
	cd_pessoa_fisica,
	ie_tipo_Atendimento,
	dt_nascimento,
	nr_sequencia,
	nm_paciente,
	cd_medico_solicitante,
	nr_seq_tipo_classif_pac
into	cd_estabelecimento_w,
	cd_convenio_ww,
	cd_categoria_ww,
	dt_inicio_agendamento_w,
	cd_plano_ww,
	cd_usuario_convenio_w,
	cd_pessoa_fisica_w,
	ie_tipo_Atendimento_w,
	dt_nascimento_w,
	nr_seq_ageint_w,
	nm_paciente_w,
	cd_medico_solicitante_w,
	nr_seq_tipo_classif_pac_w
from	agenda_integrada
where	nr_sequencia	= :new.nr_seq_agenda_int;
exception
when others then
	nr_seq_ageint_w	:= null;
end;

ie_estab_usuario_w	:= nvl(Obter_Valor_Param_Usuario(869, 1, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w), 'S');
ie_orient_long_w	:= nvl(Obter_Valor_Param_Usuario(869, 176, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w), 'N');

if	(ie_orient_long_w	= 'S') then
	select	max(cd_convenio),
		max(cd_categoria),
		max(cd_plano)
	into	cd_conv_item_w,
		cd_categ_item_w,
		cd_plano_item_w
	from	agenda_integrada_conv_item
	where	nr_seq_agenda_item	= :new.nr_sequencia;

	if	(cd_conv_item_w is not null) then
		cd_convenio_ww	:= cd_conv_item_w;
		cd_categoria_ww	:= cd_categ_item_w;
		cd_plano_ww	:= cd_plano_item_w;
	end if;

	if	(:new.cd_estabelecimento is not null) then
		cd_estabelecimento_w	:= :new.cd_estabelecimento;
	end if;

	cd_procedimento_w	:= :new.cd_procedimento;
	ie_origem_proced_w	:= :new.ie_origem_proced;

	if	((:old.nr_seq_proc_interno is null) and															
		(:new.nr_seq_proc_interno is not null)) or
		((:old.nr_seq_proc_interno is not null) and
		(:new.nr_seq_proc_interno is not null) and
		(:new.nr_seq_proc_interno	<> :old.nr_seq_proc_interno)) or
		(((:old.cd_medico is null) and (:new.cd_medico is not null)) or
		((:old.cd_medico is not null) and (:new.cd_medico is null)) or
		(:new.cd_medico <> :old.cd_medico))then		
		Ageint_Gerar_Orient_Item(cd_pessoa_fisica_w, :new.nr_sequencia, :new.nr_seq_proc_interno, :new.cd_procedimento, :new.ie_origem_proced,
								cd_convenio_ww, nr_seq_tipo_classif_pac_w, cd_estabelecimento_w, :new.nm_usuario, :new.cd_medico);
	end if;
end if;

END;
/
