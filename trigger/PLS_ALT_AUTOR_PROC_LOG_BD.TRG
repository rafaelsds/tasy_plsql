create or replace trigger pls_alt_autor_proc_log_bd
before update or delete on pls_guia_plano_proc
for each row

declare

nm_machine_w		varchar2(255);
ie_tipo_alteracao_w	varchar2(1)	:= null;
ds_log_w		varchar2(2000)	:= null;
qt_rotina_w		number(10);

begin

if	(pls_se_aplicacao_tasy = 'N') then
	begin
	SELECT	machine
	INTO	nm_machine_w
	FROM	v$session
	WHERE	audsid = USERENV('sessionid');
	exception
		when others then
		nm_machine_w	:= null;
	end;
	
	/* Verificação de SCS */
	select	count(1)
	into	qt_rotina_w
	from 	v$session
	where	audsid	= (select userenv('sessionid') from dual)
	and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	and	action like 'TASY_SCS%';
	
	if	(qt_rotina_w = 0) then
		if	(updating) then
			if	(nvl(:new.qt_solicitada,0) <>  nvl(:old.qt_solicitada,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'qt_solicitada_old= ' || :old.qt_solicitada || ' qt_solicitada_new= ' || :new.qt_solicitada;
			end if;
		
			if	(nvl(:new.qt_autorizada,0) <>  nvl(:old.qt_autorizada,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'qt_autorizada_old= ' || :old.qt_autorizada || ' qt_autorizada_new= ' || :new.qt_autorizada;
			end if;
		
			if	(nvl(:new.ie_status,0) <>  nvl(:old.ie_status,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'ie_status_old= ' || :old.ie_status || ' ie_estagio_new= ' || :new.ie_status;
			end if;
		
			if	(ds_log_w is not null) then
				ie_tipo_alteracao_w	:= 'A';
			end if;
		elsif	(deleting) then
			ie_tipo_alteracao_w	:= 'E';
			
			ds_log_w	:= 'Procedimento: ' || :new.cd_procedimento;
		end if;

		if	(ie_tipo_alteracao_w is not null) then
			insert into pls_log_alt_autor_bd
				(nr_sequencia,
				dt_alteracao,
				nr_seq_guia_plano,
				nr_seq_guia_plano_proc,
				nr_seq_guia_plano_mat,
				ie_tipo_alteracao,
				ds_log,
				nm_maquina)
			values	(pls_log_alt_autor_bd_seq.nextval,
				sysdate,
				:new.nr_seq_guia,
				:new.nr_sequencia,
				null,
				ie_tipo_alteracao_w,
				ds_log_w,
				nm_machine_w);
		end if;
	end if;
end if;

end;
/