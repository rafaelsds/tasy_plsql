create or replace trigger pls_pag_prest_vencimento_atual
before update on pls_pag_prest_vencimento
for each row

declare

nr_seq_protocolo_w		pls_protocolo_conta.nr_sequencia%type;
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
nr_seq_lote_w			pls_lote_pagamento.nr_sequencia%type;

begin

if	(:new.nr_titulo is null) and
	(:old.nr_titulo is not null) then
	select	max(c.nr_seq_protocolo),
		max(b.nr_seq_prestador),
		max(b.nr_seq_lote)
	into	nr_seq_protocolo_w,
		nr_seq_prestador_w,
		nr_seq_lote_w
	from	pls_pagamento_prestador		b,
		pls_conta_medica_resumo		c
	where	b.nr_seq_prestador	= c.nr_seq_prestador_pgto
	and	b.nr_seq_lote		= c.nr_seq_lote_pgto
	and	((c.ie_situacao = 'A') or (c.ie_situacao is null))
	and	b.nr_sequencia		= :new.nr_seq_pag_prestador;
	
	if	(nr_seq_protocolo_w is not null) then
		insert into log_tasy
			(dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values	(sysdate,
			'Trigger',
			99601,
			' Lote pgto prod m�dica: ' || nr_seq_lote_w || chr(13) ||
			' Prestador: ' || nr_seq_prestador_w || chr(13) ||
			' Protocolo ref: ' || nr_seq_protocolo_w);
	end if;
end if;

end;
/