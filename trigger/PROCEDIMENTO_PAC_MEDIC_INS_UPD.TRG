create or replace
trigger PROCEDIMENTO_PAC_MEDIC_INS_UPD
before insert or update on PROCEDIMENTO_PAC_MEDICO
for each row

declare
ie_situacao_w	procedimento.ie_situacao%type;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	if	(:new.cd_procedimento is not null) and
		(:new.ie_origem_proced is not null) then
		
		select	max(ie_situacao)
		into	ie_situacao_w
		from	procedimento
		where	cd_procedimento		= :new.cd_procedimento
		and	ie_origem_proced	= :new.ie_origem_proced;
		
		if	(nvl(ie_situacao_w,'A') = 'I') then
			wheb_mensagem_pck.exibir_mensagem_abort(1090883);
		end if;		
	end if;
end if;

end PROCEDIMENTO_PAC_MEDIC_INS_UPD;
/