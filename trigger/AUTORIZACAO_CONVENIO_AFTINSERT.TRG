CREATE OR REPLACE TRIGGER AUTORIZACAO_CONVENIO_AFTINSERT
AFTER Insert ON AUTORIZACAO_CONVENIO
FOR EACH ROW
DECLARE
qt_existe_w	Number(10);
nr_sequencia_w	Number(10);
ie_interno_w	varchar2(255);
ds_estagio_w	varchar2(255);
ie_medico_w   	varchar2(1);
ie_agua_just_med_w	varchar2(1) := '';
ds_texto_padrao_w	long;
ie_save_insurance_holder_w 	varchar2(1);
qt_anexo_agenda_w	number(3);
nr_seq_anexo_agenda_w	anexo_agenda.nr_sequencia%type;


Cursor C01 is
	select	ds_texto_padrao
	from	texto_padrao_justif_solic
	where	ie_tipo_justificativa = :new.ie_tipo_autorizacao
	and 	ie_situacao = 'A';

--TEXTO_PADRAO_JUSTIF_SOLIC

Cursor c02 is
	select	ds_arquivo,
				ds_observacao
	from	ged_atendimento
	where	nr_seq_agenda = :new.nr_seq_agenda;

c02_w		c02%rowtype;

begin

--Added below code for request SO#1996135
ie_save_insurance_holder_w := obter_dados_param_atend(wheb_usuario_pck.get_cd_estabelecimento, 'SI');

if 	(ie_save_insurance_holder_w = 'S') and
	(:new.cd_pessoa_fisica is not null) and
	(:new.cd_convenio is not null) and
	((nvl(:new.cd_convenio, 0) <> nvl(:old.cd_convenio, 0)) or
	(nvl(:new.cd_pessoa_fisica, '0') <> nvl(:old.cd_pessoa_fisica, '0'))) then
	insere_atualiza_titular_conv(
				:new.nm_usuario,
				:new.cd_convenio,
				null,
				:new.cd_pessoa_fisica,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				'N',
				'2');
end if;


Obter_Param_Usuario(3004, 236, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_agua_just_med_w); 

if (ie_agua_just_med_w = 'S') then
   select	count(*)
   into	   qt_existe_w
   from	   paciente_justificativa
   where	   nr_atendimento		= :new.nr_atendimento
   and	   nr_seq_autorizacao	= :new.nr_seq_autorizacao;
elsif (ie_agua_just_med_w = 'P') then
   if (:new.nr_seq_autorizacao is not null) and
      (:new.nr_atendimento is not null) then
      select	count(*)
      into	   qt_existe_w
      from	   paciente_justificativa
      where	   nr_seq_autorizacao	= :new.nr_seq_autorizacao
      and      nr_atendimento		   = :new.nr_atendimento;

   else
      select	count(*)
      into	   qt_existe_w
      from	   paciente_justificativa
      where	   nr_sequencia_autor	= :new.nr_sequencia;
   end if;   
end if;   

begin
select	ie_interno
into	ie_interno_w
from	estagio_autorizacao
where	nr_sequencia = :new.nr_seq_estagio
and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
exception
when no_data_found then
	ie_interno_w	:= null;
end;
	
if	(qt_existe_w = 0) and
	(ie_interno_w = '2') and
   ((ie_agua_just_med_w = 'P') or (ie_agua_just_med_w = 'S' and :new.nr_atendimento is not null)) and
	(:new.ie_tipo_autorizacao is not null) then
	
	select	paciente_justificativa_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	SELECT obter_se_usuario_medico(:NEW.nm_usuario) 
	into	ie_medico_w
	FROM dual;
	
	ds_texto_padrao_w:= '';
	open C01;
	loop
	fetch C01 into	
		ds_texto_padrao_w;
	exit when C01%notfound;
		begin
		ds_texto_padrao_w:= ds_texto_padrao_w;
		end;
	end loop;
	close C01;
	
	insert into paciente_justificativa(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_autorizacao,     
		nr_atendimento,
		dt_justificativa,
		ie_tipo_justificativa,
		qt_dia_prorrogacao,
		nr_sequencia_autor,
		ie_situacao,
		ds_justificativa,
		cd_profissional,
		ie_gerado_job,
		cd_pessoa_fisica,-- Francisco - 23/10/06 - Inclui sequencia
		dt_liberacao_parcial) 
	values(	nr_sequencia_w,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_seq_autorizacao,     
		:new.nr_atendimento,
		sysdate,
		:new.ie_tipo_autorizacao,
		:new.qt_dia_autorizado,
		:new.nr_sequencia,
		'A',
		ds_texto_padrao_w,
		nvl(decode(ie_medico_w,'S',Obter_Medico_Prescricao(:new.nr_prescricao,'C'),:new.cd_medico_solicitante) ,null),
		'S',
		:new.cd_pessoa_fisica,
		sysdate);
end if;

select	max(ds_estagio)
into	ds_estagio_w
from	estagio_autorizacao
where	nr_sequencia	= :new.nr_seq_estagio
and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;

if	(ds_estagio_w is not null) then
	insert into autorizacao_convenio_hist
		(nr_sequencia,
		 dt_atualizacao,
		 nm_usuario,
		 nr_atendimento,
		 nr_seq_autorizacao,
		 ds_historico,
		 nr_sequencia_autor,
		 nr_seq_estagio)
	values (autorizacao_convenio_hist_seq.nextval,
		 sysdate,
		 :new.nm_usuario,
		 :new.nr_atendimento,
		 :new.nr_seq_autorizacao,
		 ds_estagio_w,
		 :new.nr_sequencia,
		 :new.nr_seq_estagio);
end if;

/* OS 2007482 - Insercao de anexo agenda da Gestao da Agenda Cirurgica - Jeferson Job (jjrsantos) */
select	count(*)
into	qt_anexo_agenda_w
from	ged_atendimento
where	nr_seq_agenda	= :new.nr_seq_agenda;

if	(qt_anexo_agenda_w > 0) then
	open c02;
	loop
	fetch c02 into	
		c02_w;
	exit when c02%notfound;
		select	count(*)
		into		qt_anexo_agenda_w
		from	anexo_agenda
		where	ds_arquivo = c02_w.ds_arquivo;
		
		if	( qt_anexo_agenda_w = 0) then
			
			select	anexo_agenda_seq.nextval
			into	nr_seq_anexo_agenda_w
			from	dual;
			
			insert into anexo_agenda (
					nr_sequencia,
					ds_arquivo,
					nr_seq_agenda,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_observacao,
					ie_situacao
				)
			values (
				nr_seq_anexo_agenda_w,
				c02_w.ds_arquivo,
				:new.nr_seq_agenda,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario_nrec,
				c02_w.ds_observacao,
				'A'
			);
		end if;
	end loop;
	close c02;
end if;
/* FIM - Insercao de anexo agenda da Gestao da Agenda Cirurgica */
END;
/
