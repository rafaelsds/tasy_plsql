create or replace trigger mxm_material_atual
after insert or update on material
for each row

declare

begin 
mxm_gerar_integracao(:new.cd_material, 'MAT', :new.nm_usuario);
end;
/