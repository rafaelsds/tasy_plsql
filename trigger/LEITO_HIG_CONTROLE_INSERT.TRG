create or replace trigger leito_hig_controle_insert
after insert on leito_hig_controle
for each row

declare

begin

if	(:new.dt_inicio is not null) and
	(nvl(:new.nr_Seq_interno,0)	> 0) then
	update	unidade_atendimento
	set	ie_status_unidade	= 'H',
		dt_higienizacao		= null,
		dt_inicio_higienizacao	= sysdate,
		nm_usuario		= :new.nm_usuario
	where	nr_seq_interno		= :new.nr_Seq_interno;
end if;


end;
/