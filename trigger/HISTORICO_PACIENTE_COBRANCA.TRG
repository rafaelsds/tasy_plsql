CREATE OR REPLACE TRIGGER HISTORICO_PACIENTE_COBRANCA
AFTER INSERT ON COBRANCA
FOR EACH ROW

DECLARE

nr_interno_conta_w          titulo_receber.nr_interno_conta%type;
nr_sequencia_hist_cob_w     tipo_hist_cob.nr_sequencia%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
    select max(nr_interno_conta)
    into nr_interno_conta_w
    from titulo_receber
    where nr_titulo = :NEW.nr_titulo;

    if (nr_interno_conta_w is not null) then
        select max(nr_sequencia)
        into nr_sequencia_hist_cob_w
        from tipo_hist_cob 
        where nvl(ie_obs_conta_paciente,'N') = 'S'
        and nvl(ie_situacao, 'I') = 'A';
    end if;

    if (nr_sequencia_hist_cob_w is not null) then
        for i in (select cpo.ds_observacao
        from conta_paciente_observacao cpo
        where nr_interno_conta = nr_interno_conta_w
        and dt_liberacao is not null) loop

            insert into cobranca_historico (nr_sequencia,
                                            nr_seq_cobranca,
                                            dt_atualizacao,
                                            nm_usuario,
                                            nr_seq_historico,
                                            vl_historico,
                                            ds_historico,
                                            dt_historico,
                                            dt_atualizacao_nrec,
                                            nm_usuario_nrec,
                                            nr_seq_retorno,
                                            id_importacao)
            values (cobranca_historico_seq.nextval,
                    :NEW.nr_sequencia,
                    sysdate,
                    obter_usuario_ativo,
                    nr_sequencia_hist_cob_w,
                    null,
                    i.ds_observacao,
                    sysdate,
                    null,
                    null,
                    null,
                    null);
        end loop;
    end if;
end if;
end;
/
