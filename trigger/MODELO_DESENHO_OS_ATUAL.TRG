create or replace trigger modelo_desenho_os_atual
before insert or update on MODELO_DESENHO_OS
for each row

declare

begin
update	modelo_os
set	dt_atualizacao = sysdate,
	nm_usuario = :new.nm_usuario
where	nr_sequencia = :new.nr_seq_modelo;

end;
/