create or replace trigger pls_prestador_plano_atual
before insert or update on pls_prestador_plano
for each row

declare
cd_estabelecimento_plano_w      pls_plano.cd_estabelecimento%type;
cd_estabelecimento_prestador_w  pls_prestador.cd_estabelecimento%type;

begin
if	(:new.nr_seq_plano is not null) then
	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_plano_w
	from	pls_plano a
	where	a.nr_sequencia = :new.nr_seq_plano;
end if;

if	(:new.nr_seq_prestador is not null) then
	select  max(b.cd_estabelecimento)
	into    cd_estabelecimento_prestador_w
	from    pls_prestador b
	where   b.nr_sequencia = :new.nr_seq_prestador;
end if;

if      (cd_estabelecimento_plano_w != cd_estabelecimento_prestador_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(1063764);
end if;
	
end pls_prestador_plano_atual;
/