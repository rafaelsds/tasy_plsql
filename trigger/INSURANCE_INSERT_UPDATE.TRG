create or replace trigger 	insurance_insert_update
before insert or update on 	pessoa_titular_convenio 
	for each row

declare
medicare_count_w  varchar2(10);
pragma autonomous_transaction;

begin
  
	if	((:new.cd_usuario_convenio is not null
		and 	:new.cd_usuario_convenio <> :old.cd_usuario_convenio
		and 	:old.cd_usuario_convenio is not null)
		or  	(:old.cd_usuario_convenio is null
		and 	:new.cd_usuario_convenio is not null)) then

		select 	count(*)
		into 	medicare_count_w
		from  	pessoa_titular_convenio a,
			convenio b
		where 	replace(a.cd_usuario_convenio,' ','') = replace(:new.cd_usuario_convenio,' ','')
		and 	a.cd_convenio = :new.cd_convenio
		and 	a.cd_convenio = b.cd_convenio
		and 	b.ie_tipo_convenio = 12;

		if 	(medicare_count_w != 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1111951);
		end if;
	end if;
end;
/
