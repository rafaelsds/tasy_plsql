create or replace trigger sl_unid_atend_update
before update on sl_unid_atend			
for each row

declare
ie_status_unidade_w		varchar2(3);
ie_status_ant_unidade_w		varchar2(3);
ie_higienizar_unidade_w		varchar2(1) := 'E';
ie_atualizar_status_ant_w	varchar2(1);
nr_seq_evento_lider_w		number(10);
nr_seq_evento_gerencia_w	number(10);
cd_setor_atendimento_w		number(5,0);	
qt_reg_w			number(1);
ie_ignorar_checklist_w		varchar2(1);
ie_exige_check_list_aprov_w	varchar2(1);
qt_itens_w			number(1);
ie_status_ocup_w		varchar2(5);
ie_feriado_w			varchar2(1);
qt_regra_feriado_w		number(10);
nr_seq_w			number(10);
ie_log_w			varchar2(1);
ds_w				varchar2(255);
ds_s_w				varchar2(255);
ds_c_w				varchar2(255);
ie_dbms_alert_w			varchar2(1);
ie_aguardando_higi_manut_w	varchar2(1);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);
ie_higienizacao_ocup_w		varchar2(1);
ie_livre_ocup_w			varchar2(1);
ie_status_fim_w			varchar2(1);
ie_atualizar_unidade_livre_w	varchar2(1);
ie_permite_inicio_antes_prev_w	varchar2(1);
ie_permite_aprov_antes_fim_w	varchar2(1);
ie_alterar_livre_job_w	varchar2(1);

Cursor C01 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= :new.cd_estabelecimento
	and	a.ie_evento_disp	= 'FSL'
	and	nvl(a.ie_situacao,'A') = 'A';
	
Cursor C02 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= :new.cd_estabelecimento
	and	a.ie_evento_disp	= 'ASL'
	and	nvl(a.ie_situacao,'A') = 'A';
	
pragma autonomous_transaction;	

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(nvl(ie_status_unidade,'L')),
	max(nvl(ie_status_ant_unidade,'L'))
into	ie_status_unidade_w,
	ie_status_ant_unidade_w
from	unidade_atendimento
where	nr_seq_interno = :new.nr_seq_unidade;

Obter_Param_Usuario(75, 16, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_higienizar_unidade_w);
Obter_Param_Usuario(75, 34, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_atualizar_status_ant_w);
Obter_Param_Usuario(75, 37, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_status_ocup_w);
Obter_param_Usuario(75, 63, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_dbms_alert_w);
Obter_param_Usuario(75, 89, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_higienizacao_ocup_w);
Obter_param_Usuario(75, 90, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_livre_ocup_w);
Obter_param_Usuario(75, 97, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_atualizar_unidade_livre_w);
Obter_param_Usuario(75, 110, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_permite_inicio_antes_prev_w);
Obter_param_Usuario(75, 111, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_permite_aprov_antes_fim_w);
Obter_param_Usuario(75, 112, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_alterar_livre_job_w);

if	((ie_permite_inicio_antes_prev_w = 'N') and
	(:new.dt_inicio is not null) and
	(:new.dt_inicio < :new.dt_prevista)) then 
	wheb_mensagem_pck.exibir_mensagem_abort(728462, 'DS_DATA_MENOR=' || obter_desc_expressao(286737) || 
							';DS_DATA_MAIOR=' || obter_desc_expressao(287126) || 
							';NR_PARAMETRO=110'); -- Data de in�cio n�o pode ser inferior a Data prevista! Par�metro [110]. 
end if;

if 	(:new.dt_fim is not null) and
	(:new.dt_fim < :new.dt_inicio) then
	wheb_mensagem_pck.exibir_mensagem_abort(728452); -- A data fim n�o pode ser menor que a data in�cio!
end if;

if 	((ie_permite_aprov_antes_fim_w = 'N') and
	(:new.dt_aprovacao is not null) and
	(:new.dt_aprovacao < :new.dt_fim)) then 
	wheb_mensagem_pck.exibir_mensagem_abort(728462, 'DS_DATA_MENOR=' || obter_desc_expressao(286713) || 
							';DS_DATA_MAIOR=' || obter_desc_expressao(286879) || 
							';NR_PARAMETRO=111'); -- Data de aprova��o n�o pode ser inferior a Data fim! Par�metro [111].
end if;

if	((:old.dt_inicio is not null) and (nvl(:new.dt_inicio,sysdate+1) <> nvl(:old.dt_inicio,sysdate+1)) and (:new.dt_fim is not null))  OR 
	((:new.IE_STATUS_SERV = 'EE') and (:new.dt_fim is not null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(213231);
end if;


if	(:new.ie_status_serv <> :old.ie_status_serv) and
	(:new.ie_status_serv = 'C' and 
	((ie_alterar_livre_job_w = 'S') or 
	 (ie_alterar_livre_job_w = 'N' and :new.ie_evento <> 'J'))) then

	update	unidade_atendimento 
	set		ie_status_unidade	= decode(	nr_atendimento,	
								null,	
								decode(ie_status_ocup_w, null, decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R') ,ie_status_ocup_w),
								ie_status_unidade),										
			nm_usuario = :new.nm_usuario	 	
	where	nr_seq_interno		= :new.nr_seq_unidade;
	
end if;
if	(:old.dt_fim is null) and (:new.dt_fim is not null) and (:new.ie_status_serv <> 'E') then
	wheb_mensagem_pck.exibir_mensagem_abort(213232);
end if;

if	(ie_status_unidade_w in ('A','H','L','R','S','G','C','E')) then

	if	(:new.dt_inicio is not null) and
		(:old.dt_inicio is null) then		
		
		update	unidade_atendimento 
		set	dt_inicio_higienizacao	= :new.dt_inicio,
			dt_higienizacao		= null,
			nm_usuario_fim_higienizacao = null,
			ie_status_unidade	= decode(ie_status_unidade_w, 'C', 'E', decode(ie_higienizacao_ocup_w, 'S', 'H', ie_status_unidade)),
			nm_usuario_higienizacao	= :new.nm_usuario,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where	nr_seq_interno		= :new.nr_seq_unidade;
		
	end if;
	
	if	(:new.dt_fim is not null) and
		(:old.dt_fim is null) and
		(ie_higienizar_unidade_w = 'F') then
		
		if	(ie_status_unidade_w = 'E') then
			
			select	max(cd_unidade_basica),
				max(cd_unidade_compl),
				max(cd_setor_atendimento)
			into	cd_unidade_basica_w,
				cd_unidade_compl_w,
				cd_setor_atendimento_w
			from	unidade_atendimento
			where	nr_seq_interno = :new.nr_seq_unidade;
			
			
			atualizar_leito_manutencao(:new.nm_usuario, cd_unidade_basica_w, cd_unidade_compl_w, cd_setor_atendimento_w, 'G', 'S');
			
		else
			select	nvl(max(ie_status_fim),'N')
			into	ie_status_fim_w
			from	sl_servico
			where 	nr_sequencia = nvl(:new.nr_seq_servico_exec, :new.nr_seq_servico);
			
			if (nvl(ie_status_fim_w,'N') = 'N') then
				update	unidade_atendimento 
				set	dt_higienizacao		= :new.dt_fim,	 
					ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R'), 
					nm_usuario_higienizacao	= nvl(nm_usuario_higienizacao,:new.nm_usuario),
					nm_usuario_fim_higienizacao = :new.nm_usuario,
					nm_usuario		= :new.nm_usuario,
					dt_atualizacao		= sysdate
				where	nr_seq_interno		= :new.nr_seq_unidade;
			end if;
		end if;
		
	end if;

	if	(:new.dt_aprovacao is not null) and
		(:old.dt_aprovacao is null) and
		(ie_higienizar_unidade_w = 'A') then
		
		if	(ie_atualizar_unidade_livre_w = 'S')	then
		
			update	unidade_atendimento 
			set	dt_higienizacao		= :new.dt_fim,	 
				ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R'), 
				nm_usuario_higienizacao	= :new.nm_usuario,
				nm_usuario_fim_higienizacao = :new.nm_usuario,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate
			where	nr_seq_interno		= :new.nr_seq_unidade;
		
		elsif	(ie_status_unidade_w = 'E') then
		
			select	max(cd_unidade_basica),
				max(cd_unidade_compl),
				max(cd_setor_atendimento)
			into	cd_unidade_basica_w,
				cd_unidade_compl_w,
				cd_setor_atendimento_w
			from	unidade_atendimento
			where	nr_seq_interno = :new.nr_seq_unidade;
			
			
			atualizar_leito_manutencao(:new.nm_usuario, cd_unidade_basica_w, cd_unidade_compl_w, cd_setor_atendimento_w, 'G', 'S');
			
		else
			
			update	unidade_atendimento 
			set	dt_higienizacao		= :new.dt_fim,	 
				ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R'), 
				nm_usuario_higienizacao	= :new.nm_usuario,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate
			where	nr_seq_interno		= :new.nr_seq_unidade;
			
		end if;
	end if;
	
	if	(:new.dt_inicio is null) and
		(:old.dt_inicio is not null) and
		(ie_atualizar_status_ant_w = 'S') then
		
		update	unidade_atendimento 
		set	dt_inicio_higienizacao	= :new.dt_inicio,
			dt_higienizacao		= null,
			nm_usuario_fim_higienizacao = null,
			ie_status_unidade	= decode(ie_status_ant_unidade_w,'P',decode(nr_atendimento,null,'H','P'),ie_status_ant_unidade_w),
			nm_usuario_higienizacao	= null,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where	nr_seq_interno		= :new.nr_seq_unidade;
		
	end if;
	
	if	(:new.dt_fim is not null) and
		(:old.dt_fim is null) then
		begin
		select	cd_setor_atendimento
		into	cd_setor_atendimento_w
		from	unidade_atendimento
		where	nr_seq_interno = :new.nr_seq_unidade;
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_evento_lider_w;
		exit when C01%notfound;
			begin
			gerar_evento_lider_escala(nr_seq_evento_lider_w,null,null,null,:new.nm_usuario,:new.dt_fim,cd_setor_atendimento_w,:new.nr_seq_unidade);
			end;
		end loop;
		close C01;
		end;
	end if;
	
	if	(:new.dt_aprovacao is not null) and
		(:old.dt_aprovacao is null) then
		
		select	nvl(max(ie_ignorar_checklist),'N')
		into	ie_ignorar_checklist_w
		from	unidade_atendimento
		where	nr_seq_interno = :new.nr_seq_unidade;
		
		if	(ie_ignorar_checklist_w = 'N') then
		
			select 	max(nvl(ie_exige_check_list_aprov,'N'))
			into	ie_exige_check_list_aprov_w
			from	sl_servico
			where	nr_sequencia = :new.nr_seq_servico;
		
			if	(ie_exige_check_list_aprov_w = 'S') then
				
				select 	count(*)
				into	qt_itens_w
				from	sl_check_list_unid
				where	nr_seq_sl_unid = :new.nr_sequencia;
				
				if	(qt_itens_w = 0) then
					wheb_mensagem_pck.exibir_mensagem_abort(213233);
				end if;
			elsif	(ie_exige_check_list_aprov_w = 'N') then
				
				select	count(*)
				into	qt_regra_feriado_w
				from	sl_servico_regra_check
				where	nr_seq_servico = :new.nr_seq_servico;
				
				select 	count(*)
				into	qt_itens_w
				from	sl_check_list_unid
				where	nr_seq_sl_unid = :new.nr_sequencia;
				
				if	(qt_regra_feriado_w > 0)  then 
					if	(obter_se_feriado(:new.cd_estabelecimento,:new.dt_inicio) = 0) and (qt_itens_w = 0) then
						wheb_mensagem_pck.exibir_mensagem_abort(213234);			
					elsif	(obter_se_feriado(:new.cd_estabelecimento,:new.dt_inicio) > 0) then
					
						select	max(nvl(ie_feriado,'N'))
						into	ie_feriado_w
						from	sl_servico_regra_check
						where	nr_seq_servico = :new.nr_seq_servico;	
						
						if	(ie_feriado_w = 'S') and (qt_itens_w = 0)  then
							wheb_mensagem_pck.exibir_mensagem_abort(213241);					
						end if;
					end if;
				end if;	
			end if;
		end if;
		
		begin
		open c02;
		loop
		fetch C02 into
			nr_seq_evento_gerencia_w;
		exit when C02%notfound;
			begin
			gerar_evento_gerencia_escala(nr_seq_evento_gerencia_w,null,null,null,:new.nm_usuario,:new.nr_seq_unidade);
			end;
		end loop;
		close c02;
		end;
	end if;

if	(ie_higienizacao_ocup_w = 'S') and 
	(:new.ie_status_serv <> :old.ie_status_serv) and
	(:new.ie_status_serv = 'EE') then

	update	unidade_atendimento 
	set		ie_status_unidade	= 'H'
	where	nr_seq_interno		= :new.nr_seq_unidade;
end if;

if	(ie_livre_ocup_w = 'S') and
	(:new.ie_status_serv <> :old.ie_status_serv) and
	(:new.ie_status_serv = 'A') then

	update	unidade_atendimento 
	set		ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R')
	where	nr_seq_interno		= :new.nr_seq_unidade;
end if;		

end if;

<<Final>>
If	(:old.ie_prioridade <> :new.ie_prioridade) then
	gravar_log_alteracao(substr(:old.ie_prioridade,1,1), substr(:new.ie_prioridade,1,1), :new.nm_usuario,nr_seq_w,'IE_PRIORIDADE',ie_log_w,ds_w,'sl_unid_atend',ds_s_w,ds_c_w);
End if;
If	(:old.nr_seq_unidade <> :new.nr_seq_unidade) then
	gravar_log_alteracao(substr(:old.nr_seq_unidade,1,10), substr(:new.nr_seq_unidade,1,10), :new.nm_usuario,nr_seq_w,'NR_SEQ_UNIDADE',ie_log_w,ds_w,'sl_unid_atend',ds_s_w,ds_c_w);
End if;
qt_reg_w	:= 0;

If	(ie_dbms_alert_w = 'S') then
	Gestao_Serv_leito_alert_signal(nvl(:new.nm_usuario,'Tasy'));
End if;

commit;

end;
/