CREATE OR REPLACE TRIGGER trg_restrict_compile_obj BEFORE CREATE OR DROP ON DATABASE
    WHEN ( ora_dict_obj_owner = 'TASY'
           AND ora_login_user NOT IN ( 'SYS', 'SYSTEM', 'GGADM', 'TASYDBA' )
               AND ora_dict_obj_type IN ( 'PACKAGE', 'PACKAGE BODY', 'FUNCTION', 'PROCEDURE', 'VIEW',
                                          'TYPE' ) )
BEGIN
    IF (tasy.wheb_usuario_pck.get_ie_executar_trigger = 'S') THEN
        IF ( sys_context('USERENV', 'OS_USER') IN ( '320100528', '320108485', '320058511', '320110979', '320112508',
                                                    '320095382',
                                                    '320100919',
                                                    '300223917',
                                                    'jpweiss',
                                                    'jrfusinato',
                                                    'ascarneiro',
                                                    'rmcorrea' ) ) THEN
            IF ( tasy.wheb_usuario_pck.get_nm_usuario IS NULL ) THEN
                tasy.wheb_mensagem_pck.exibir_mensagem_abort(chr(13)
                                                             || 'Tasy''s user didn''t set!'
                                                             || chr(13)
                                                             || chr(13)
                                                             || 'Please perform this command on your SQL session before proceed:'
                                                             || chr(13)
                                                             || ' --> exec wheb_usuario_pck.set_nm_usuario('''||chr(38)||'username_tasy'');');
            END IF;


            IF NOT check_user_edit_object(tasy.wheb_usuario_pck.get_nm_usuario, ora_dict_obj_name) THEN
                tasy.wheb_mensagem_pck.exibir_mensagem_abort('Permission denied, please request the grant for the object''s owner.'
                                                             || chr(13)
                                                             || 'For more information consult the rule "Owner Dictionary" on tasy system.');
            END IF;
        END IF;
    END IF;
END trg_restrict_compile_obj;
/
