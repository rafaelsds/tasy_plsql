create or replace
trigger HCP_BATCH_SENDING_BEFINS
before insert or update on HCP_BATCH_SENDING
for each row
declare

begin

	if (:new.cd_convenio is not null and :new.cd_convenio_group is not null) or
		(:new.cd_convenio is null and :new.cd_convenio_group is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(1117524);
	end if;

end;
/