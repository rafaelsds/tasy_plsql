create or replace trigger fa_paciente_entrega_update
before update on fa_paciente_entrega
for each row

declare

begin

if (:old.ie_status_paciente = 'PR') and (:new.ie_status_paciente = 'TR') then
	wheb_mensagem_pck.exibir_mensagem_abort(264189);
end if;

end;
/