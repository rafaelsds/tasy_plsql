create or replace trigger atual_regra_periodo_sem_hor
before insert or update on regra_periodo_sem_hor
for each row

declare

begin

if (:new.hr_inicio is not null) and 
   ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
	:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

if (:new.hr_fim is not null) and 
   ((:new.hr_fim <> :old.hr_fim) or (:old.dt_fim is null)) then
	:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_fim || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

end;
/