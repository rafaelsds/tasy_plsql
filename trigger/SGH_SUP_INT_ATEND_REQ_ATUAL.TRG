create or replace
trigger sgh_sup_int_atend_req_atual
after insert or update on sup_int_atend_req
for each row

declare

	nr_sequencia_w		item_requisicao_material.nr_sequencia%type;
		
	
begin

if	(:new.dt_recebimento is not null) and
	(:old.dt_recebimento is null) then
	begin
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	item_requisicao_material
	where	nr_requisicao = :new.nr_requisicao
	and	qt_material_atendida = :new.qt_material_atendida
	and	dt_recebimento is null;
	
	if	(nr_sequencia_w is not null) then
		gerar_movto_estoque_receb_req(	
			:new.nr_requisicao,
			nr_sequencia_w,
			'N',
			:new.qt_material_atendida,
			:new.nm_usuario);
	end if;
	end;
end if;
end;
/