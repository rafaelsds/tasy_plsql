create or replace trigger protocolo_convenio_afinsert
after insert on protocolo_convenio
for each row
declare
ds_observacao_w varchar2(255);
begin
ds_observacao_w := wheb_mensagem_pck.get_texto(352443);
insert into protocolo_convenio_log
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_protocolo,
	ds_arquivo_envio,
	vl_protocolo,
	ie_tipo_log,
	ds_observacao)
values	(protocolo_convenio_log_seq.NextVal,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	:new.nr_seq_protocolo,
	:new.ds_arquivo_envio,
	0,
	'C',
	ds_observacao_w);
end;
/