create or replace trigger pls_monitor_tiss_cta_val_atual
before update on pls_monitor_tiss_cta_val
for each row

declare

begin
-- Caso tenha alguma altera��o no valor apresentado ou pago, zera os valores totais do lote
if	(:new.vl_cobranca_guia <> :old.vl_cobranca_guia) or
	(:new.vl_total_pago <> :old.vl_total_pago) then
	pls_gerencia_envio_ans_pck.atualiza_valores_totais_lote(:new.nr_seq_lote_monitor, :new.nm_usuario, 'Z');
end if;

end;
/