CREATE OR REPLACE TRIGGER TRA_MAN_NIVEL_VALOR_INS_UPD
BEFORE INSERT OR UPDATE ON MAN_NIVEL_VALOR
FOR EACH ROW

declare

cd_expressao_w		dic_expressao.CD_EXPRESSAO%type;
ds_expressao_br_w	dic_expressao.DS_EXPRESSAO_BR%type;

begin

	man_traduzir_texto(	:new.ds_nivel, :new.nm_usuario, :new.cd_exp_nivel, ds_expressao_br_w);

end;
/