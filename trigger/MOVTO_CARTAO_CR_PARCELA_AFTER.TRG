create or replace trigger movto_cartao_cr_parcela_after
after insert on movto_cartao_cr_parcela
for each row

declare

ie_contab_desp_cartao_w		parametro_contas_receber.ie_contab_desp_cartao%type;
cd_estabelecimento_w		movto_cartao_cr.cd_estabelecimento%type;
dt_transacao_w			movto_cartao_cr.dt_transacao%type;
dt_transacao_tr_w		movto_cartao_cr.dt_transacao%type;
nr_seq_trans_caixa_w		movto_cartao_cr.nr_seq_trans_caixa%type;
nr_sequencia_w			movto_cartao_cr.nr_sequencia%type;
vl_movimento_w			ctb_documento.vl_movimento%type;
nr_seq_info_w			ctb_documento.nr_seq_info%type;
nm_tabela_w			ctb_documento.nm_tabela%type;
nr_seq_caixa_rec_w 		movto_cartao_cr.nr_seq_caixa_rec%type;
nr_seq_banco_w			movto_trans_financ.nr_seq_banco%type;
nr_seq_caixa_w			movto_trans_financ.nr_seq_caixa%type;
nr_documento_w			ctb_documento.nr_documento%type;
nr_seq_doc_compl_w		ctb_documento.nr_seq_doc_compl%type;
nr_doc_analitico_w		ctb_documento.nr_doc_analitico%type;

cursor c01 is
	select	a.nm_atributo,
		a.cd_tipo_lote_contab
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 10
	and 	a.nm_atributo in ( 'VL_PARCELA_CARTAO', 'VL_DESPESA_CARTAO', 'VL_LIQUIDO_CARTAO')
	and	nr_seq_caixa_rec_w is not null
	union all
	select	a.nm_atributo,
		a.cd_tipo_lote_contab
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 18
	and 	a.nm_atributo in ( 'VL_PARCELA_CARTAO', 'VL_DESPESA_CARTAO', 'VL_LIQUIDO_CARTAO', 'VL_LIQUIDO_CARTAO_TR')
	and	nr_seq_caixa_rec_w is null;

c01_w		c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	cd_estabelecimento,
	dt_transacao,
	nr_seq_trans_caixa,
	nr_sequencia,
	nr_seq_caixa_rec
into	cd_estabelecimento_w,
	dt_transacao_tr_w,
	nr_seq_trans_caixa_w,
	nr_sequencia_w,
	nr_seq_caixa_rec_w
from	movto_cartao_cr
where	nr_sequencia = :new.nr_seq_movto;

open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin
	dt_transacao_w	:= dt_transacao_tr_w;
	if	(c01_w.cd_tipo_lote_contab = 10) then
		begin
		
		begin
		
		select	nvl(ie_contab_desp_cartao,'N')
		into	ie_contab_desp_cartao_w
		from	parametro_contas_receber
		where	cd_estabelecimento	= cd_estabelecimento_w;
		
		exception
		when others then
			ie_contab_desp_cartao_w:= 'N';
		end;
		
		if	(ctb_online_pck.get_modo_lote(10,cd_estabelecimento_w) = 'S') then
			begin
			select	nvl(x.ie_contab_desp_cartao,'N')
			into	ie_contab_desp_cartao_w
			from	(select	a.ie_contab_desp_cartao
				 from	ctb_param_lote_contas_rec a
				 where	a.cd_empresa	= obter_empresa_estab(cd_estabelecimento_w)
				 and	nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w
				 order by nvl(a.cd_estab_exclusivo,0) desc) x
			where	rownum = 1;
			exception when others then
			ie_contab_desp_cartao_w:= 'N';
			end;
		end if;
		
		if	(ie_contab_desp_cartao_w = 'S') then
			begin

			vl_movimento_w := case	c01_w.nm_atributo
					 when	'VL_PARCELA_CARTAO' then :new.vl_parcela
					 when	'VL_DESPESA_CARTAO' then :new.vl_despesa
					 when	'VL_LIQUIDO_CARTAO' then :new.vl_liquido
					 else
						null
					 end;
			nr_seq_info_w		:= 64;
			nm_tabela_w		:= 'MOVTO_CARTAO_CR_PARCELA';
			nr_documento_w		:= nr_sequencia_w;
			nr_seq_doc_compl_w	:= :new.nr_sequencia;
			nr_doc_analitico_w	:= 0;
			
			end;
		end if;
	
		end;
	elsif	(c01_w.cd_tipo_lote_contab = 18) then
		begin
		
		begin
		
		select	nvl(ie_contab_desp_cartao,'N')
		into	ie_contab_desp_cartao_w
		from	parametro_contas_receber
		where	cd_estabelecimento	= cd_estabelecimento_w;
		
		exception
		when others then
			ie_contab_desp_cartao_w:= 'N';
		end;
		
		if	(ctb_online_pck.get_modo_lote(18,cd_estabelecimento_w) = 'S') then
			begin
			select	nvl(x.ie_contab_desp_cartao,'N')
			into	ie_contab_desp_cartao_w
			from	(select	a.ie_contab_desp_cartao
				 from	ctb_param_lote_contas_rec a
				 where	a.cd_empresa	= obter_empresa_estab(cd_estabelecimento_w)
				 and	nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w
				 order by nvl(a.cd_estab_exclusivo,0) desc) x
			where	rownum = 1;
			exception when others then
			ie_contab_desp_cartao_w:= 'N';
			end;
		end if;
		
		if	(ie_contab_desp_cartao_w = 'S') then
			begin
			
			begin
			
			select	nr_seq_banco,
				nr_seq_caixa,
				nr_sequencia,
				nr_seq_trans_financ
			into	nr_seq_banco_w,
				nr_seq_caixa_w,
				nr_documento_w,
				nr_seq_trans_caixa_w
			from	movto_trans_financ
			where	nr_seq_movto_cartao = :new.nr_seq_movto;
			
			exception
			when others then
				nr_seq_banco_w:= null;
				nr_seq_caixa_w:= null;
			end;
			
			if	(nr_seq_caixa_rec_w is null) and 
				(nr_seq_banco_w is not null) and 
				(nr_seq_caixa_w is null) then
				begin
				
				vl_movimento_w:=case	c01_w.nm_atributo
						when	'VL_PARCELA_CARTAO' then :new.vl_parcela
						when	'VL_DESPESA_CARTAO' then :new.vl_despesa
						when	'VL_LIQUIDO_CARTAO' then :new.vl_liquido
						when	'VL_LIQUIDO_CARTAO_TR' then :new.vl_liquido
						else
							null
						end;
				nr_seq_info_w:=	5;
				nm_tabela_w:=	'MOVTO_CARTAO_CR_PARCELA';
				nr_seq_doc_compl_w:=	nr_sequencia_w;
				nr_doc_analitico_w:=	:new.nr_sequencia;
				
				if	(c01_w.nm_atributo in ('VL_PARCELA_CARTAO', 'VL_LIQUIDO_CARTAO')) then
					dt_transacao_w	:= :new.dt_parcela;
				end if;
				
				
				end;
			end if;
		
			end;
		end if;
		end;
	end if;
		
	if	(nvl(vl_movimento_w, 0) <> 0) then
	begin
				
	ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
								dt_transacao_w,
								c01_w.cd_tipo_lote_contab,
								nr_seq_trans_caixa_w,
								nr_seq_info_w,
								nr_documento_w,
								nr_seq_doc_compl_w,
								nr_doc_analitico_w,
								vl_movimento_w,
								nm_tabela_w,
								c01_w.nm_atributo,
								:new.nm_usuario);
		end;
	end if;
		
	end;
end loop;
close c01;


if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_movto,:new.nr_sequencia,'CC',:new.dt_parcela,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_movto,:new.nr_sequencia,'CC',:new.dt_parcela,'A',:new.nm_usuario);
end if;
end if;

end;
/
