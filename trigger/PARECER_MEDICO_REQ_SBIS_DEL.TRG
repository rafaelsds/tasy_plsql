create or replace trigger PARECER_MEDICO_REQ_SBIS_DEL
before delete on PARECER_MEDICO_REQ
for each row

declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual; 

insert into log_alteracao_prontuario (nr_sequencia, 
									 dt_atualizacao, 
									 ds_evento, 
									 ds_maquina, 
									 cd_pessoa_fisica, 
									 ds_item, 
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values 
									 (nr_log_seq_w, 
									 sysdate, 
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina, 
									 obter_pessoa_atendimento(:old.nr_atendimento,'C'), 
									 obter_desc_expressao(487400), 
									  :old.nr_atendimento,
									 :old.dt_liberacao, 
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));


end;
/
