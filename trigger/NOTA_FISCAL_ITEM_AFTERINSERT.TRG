create or replace trigger nota_fiscal_item_AfterInsert
after insert or update on nota_fiscal_item
for each row

declare

cd_estabelecimento_w		number(4);
dt_atualizacao_estoque_w		date;
ie_variacao_fiscal_nf_w		varchar2(1);
qt_existe_w			number(10);

begin

select	cd_estabelecimento,
	dt_atualizacao_estoque
into	cd_estabelecimento_w,
	dt_atualizacao_estoque_w
from	nota_fiscal
where	nr_sequencia = :new.nr_sequencia;

select	count(*)
into	qt_existe_w
from	fis_variacao_fiscal
where	ie_situacao = 'A';

select	nvl(max(ie_variacao_fiscal_nf),'N')
into	ie_variacao_fiscal_nf_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;

if	(dt_atualizacao_estoque_w is null) and
	(ie_variacao_fiscal_nf_w = 'S') and
	(qt_existe_w > 0) then
	
	gerar_tributos_item_nf(	:new.nr_sequencia, :new.nr_item_nf, :new.nr_seq_variacao_fiscal, :new.vl_liquido, :new.nm_usuario);
	
end if;

end;
/