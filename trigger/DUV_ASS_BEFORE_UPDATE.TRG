create or replace trigger DUV_ASS_BEFORE_UPDATE
before insert or update on DUV_ASS
for each row

declare

vl_soma_w	number(2);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	return;
end if;

vl_soma_w	:=	nvl(:new.IE_SEXO,0) + nvl(:new.NR_FAIXA_ETARIA,0) + nvl(:new.IE_TRAUMA_INALACAO,0) + nvl(:new.IE_QUEIMADURA_TERCEIRO_GRAU,0) + nvl(:new.IE_PERCENT_CORPO_QUEIMADO,0);

:new.QT_SCORE_TOTAL := vl_soma_w;

end;
/
