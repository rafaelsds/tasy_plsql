create or replace trigger hmc_regra_lanc_aut_befdel
before delete on regra_lanc_automatico
for each row

declare

begin
  if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
    dbms_application_info.SET_ACTION('GERAR_LOG_EXCLUSAO');
  end if;
end;
/
