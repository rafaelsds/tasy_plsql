create or replace trigger PEP_PAC_CI_ATUAL
before insert or update on PEP_PAC_CI
for each row
declare


begin
if	(nvl(:old.DT_ATUALIZACAO_NREC,sysdate+10) <> :new.DT_ATUALIZACAO_NREC) and
	(:new.DT_ATUALIZACAO_NREC is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ATUALIZACAO_NREC,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;


end;
/
