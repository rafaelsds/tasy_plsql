CREATE OR REPLACE TRIGGER Prescr_solucao_Update
BEFORE UPDATE ON Prescr_solucao
FOR EACH ROW
DECLARE

ie_lib_medico_w		varchar2(1);
qt_hora_fase_w		number(15,4);
nr_etapas_w			number(15,4);
ds_alteracao_w		varchar2(1800);
ie_nova_calc_terap_w	varchar2(1);
ie_acm_protocolo_w		protocolo_npt.ie_acm%type;
cd_funcao_origem_w  	prescr_medica.cd_funcao_origem%type;
qt_horas_adicional_w	number(18,6);
ie_operacao_w			intervalo_prescricao.ie_operacao%type;
ie_intervalo_24_w		intervalo_prescricao.ie_24_h%type;
dt_inicio_prescr_w		prescr_medica.dt_inicio_prescr%type;
hr_prim_horario_w		prescr_solucao.hr_prim_horario%type;
dt_inicio_item_w		date;
dt_copia_cpoe_w			cpoe_material.dt_prox_geracao%type;
nr_seq_mat_cpoe_w		cpoe_material.nr_sequencia%type;
qt_horas_ant_copia_cpoe_w	number(18);
ds_log_w				varchar2(2000);
nm_usuario_w			usuario.nm_usuario%type;

procedure gerar_log_alteracoes is 
begin
	if	(nvl(:new.qt_solucao_total,-1) <> nvl(:old.qt_solucao_total,-1)) then
		ds_alteracao_w := ds_alteracao_w || 'QT_SOLUCAO_TOTAL(' || to_char(:old.qt_solucao_total) || '/' || to_char(:new.qt_solucao_total) ||'); ';
	end if;
	
	if	(ds_alteracao_w is not null) then
		ds_alteracao_w := substr('Alterações(old/new)= ' || ds_alteracao_w || 'NR_SEQ_SOLUCAO('||to_char(:new.nr_seq_solucao)||'); FUNCAO('||to_char(wheb_usuario_pck.get_cd_funcao)||'); PERFIL('||to_char(wheb_usuario_pck.get_cd_perfil)||')',1,1800);
		
		gerar_log_prescricao(
			nr_prescricao_p	=> :new.nr_prescricao,
			nr_seq_item_p	=> :new.nr_seq_solucao,
			ie_agrupador_p	=> null,
			nr_seq_horario_p => null,
			ie_tipo_item_p	=> 'SOL',
			ds_log_p		=> ds_alteracao_w,
			nm_usuario_p	=> nm_usuario_w,
			nr_seq_objeto_p	=> 3566,
			ie_commit_p		=> 'N'
		);
	end if;
end;

BEGIN
	Obter_Param_Usuario(924, 1151,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento, ie_nova_calc_terap_w);
	dt_inicio_prescr_w	:= obter_datas_prescricao(:new.nr_prescricao,'I');
	cd_funcao_origem_w	:= obter_funcao_origem_prescr(:new.nr_prescricao);
	nm_usuario_w := nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario);
	
	gerar_log_alteracoes;

update	prescr_material
set		ie_suspenso		= :new.ie_suspenso,
		nm_usuario_susp		= :new.nm_usuario_susp
where	nr_prescricao		= :new.nr_prescricao
and		nr_sequencia_solucao	= :new.nr_seq_solucao
and	ie_agrupador		in(4,13)
and	nvl(ie_suspenso,'N')	= 'N'
and	nr_seq_substituto	is null;

update 	prescr_material
set	ie_acm 			= :new.ie_acm,
	ie_se_necessario	= :new.ie_se_necessario
where	nr_prescricao		= :new.nr_prescricao
and	nr_sequencia_solucao	= :new.nr_seq_solucao
and	ie_agrupador		in(4,13);


select max(nr_seq_mat_cpoe)
  into nr_seq_mat_cpoe_w
  from prescr_material
 where nr_prescricao = :new.nr_prescricao
   and nr_sequencia_solucao = :new.nr_seq_solucao;


if	(nvl(:new.ie_suspenso, 'N') = 'S') then
	begin
	:new.ie_status := 'S';
	end;
end if;

if	(nvl(:old.ie_suspenso, 'N') = 'S') and
	(nvl(:new.ie_suspenso,'N') = 'N') then
	begin
	
	update 	prescr_material
	set	ie_suspenso = null,
		dt_suspensao = null,
		nm_usuario_susp = null
	where	nr_prescricao = :new.nr_prescricao
	and	nr_sequencia_solucao = :new.nr_seq_solucao;
	
	update 	prescr_mat_hor
	set	dt_suspensao = null,
		nm_usuario_susp = null
	where	nr_prescricao = :new.nr_prescricao
	and	nr_seq_solucao = :new.nr_seq_solucao
	and	dt_fim_horario is null;
	
	:new.ie_status := 'S';
	
	end;
end if;

if	(nvl(:new.ie_solucao_especial,:old.ie_solucao_especial) = 'N') then
	:new.ie_bomba_elastomerica	:= obter_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao);

	if	(:new.ie_bomba_elastomerica is not null) and
		(nvl(obter_vol_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao),0) > 0) then
		:new.qt_dosagem			:= obter_vol_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao);
		:new.qt_solucao_total		:= Obter_vel_sol_mlh(:new.ie_tipo_dosagem, :new.qt_dosagem) * :new.qt_tempo_aplicacao * :new.nr_etapas;
	end if;
end if;

--if	(nvl(:new.qt_volume,0) = 0) then

--end if;

if	(:new.nr_etapas = 0) and
	((:new.ie_acm = 'S') or
	 (:new.IE_SOLUCAO_PCA = 'S') or
	 (:new.ie_se_necessario = 'S')) and
	(nvl(:new.ie_hemodialise,'N') = 'N') then
	:new.nr_etapas	:= null;
end if;

if	(:new.nr_seq_dialise is not null) then	
	--if	(ie_lib_medico_w = 'N') then
		:new.nr_etapas_medico := :new.nr_etapas;
	--end if;
end if;


if (nvl(cd_funcao_origem_w,9999) <> 2314) or (nvl(:new.qt_volume,0) = 0) then
	if	(:new.qt_solucao_total <> :old.qt_solucao_total) then
		select	nvl(:new.qt_solucao_total,sum(nvl(qt_solucao,0)))
		into	:new.qt_volume
		from	prescr_material
		where	nr_prescricao		= :new.nr_prescricao
		and	nr_sequencia_solucao	= :new.nr_seq_solucao
		and		nr_seq_substituto is null;
	else
		select	sum(nvl(qt_solucao,0))
		into	:new.qt_volume
		from	prescr_material
		where	nr_prescricao		= :new.nr_prescricao
		and	nr_sequencia_solucao	= :new.nr_seq_solucao
		and		nr_seq_substituto is null;
	end if;
end if;

if	(:new.cd_intervalo 		is not null) and
	(nvl(:new.IE_CALC_AUT,'N')	= 'S') and
	(:new.qt_hora_fase 		is not null) then
	Calcular_etapas_interv_solucao(:new.cd_intervalo, :new.qt_tempo_aplicacao, 'S', qt_hora_fase_w, nr_etapas_w,null,null);
	if	(:new.qt_hora_fase	<> qt_hora_fase_w) then
		Calcular_etapas_interv_solucao(:new.cd_intervalo, :new.qt_tempo_aplicacao, 'N', qt_hora_fase_w, nr_etapas_w,null,null);
	end if;
end if;

if	(:new.nr_seq_protocolo is not null) and
	(:new.nr_seq_dialise is not null) then	

	select	nvl(max(ie_acm),'N')
	into	ie_acm_protocolo_w
	from	protocolo_npt
	where	nr_sequencia = :new.nr_seq_protocolo;

	:new.ie_acm := ie_acm_protocolo_w;

end if;

if (nvl(:old.ie_bomba_infusao,'XPTO') <> nvl(:new.ie_bomba_infusao,'XPTO')) then
	:new.ie_item_disp_gerado := 'N';
end if;

if	((nvl(:new.nr_etapas_suspensa,0) <> nvl(:old.nr_etapas_suspensa,0)) or ((nvl(:new.ie_suspenso,'N') = 'S') and (nvl(:old.ie_suspenso,'N') = 'N'))) then
	ds_log_w	:= 'Prescr='||:new.nr_prescricao||';Seq='||:new.nr_seq_solucao||';Old et='||nvl(:old.nr_etapas_suspensa,0)||';New et='||nvl(:new.nr_etapas_suspensa,0)||
				   ';Susp='||nvl(:new.ie_suspenso,'N')||';Stack='||substr(dbms_utility.format_call_stack,1,1700);
	gravar_log_tasy(4589, ds_log_w, :new.nm_usuario);
end if;

if	(:new.ds_horarios <> :old.ds_horarios) and
	(:new.ds_horarios is not null) and 
	(cd_funcao_origem_w = 2314) and
	((obter_funcao_ativa = 7015) or
	 (obter_funcao_ativa = 7010) or
	 (obter_funcao_ativa = 252)) and 
	(obter_datas_prescricao(:new.nr_prescricao,'L') is not null) then

	qt_horas_adicional_w := 24;
	if (:new.cd_intervalo is not null) then
		select	nvl(max(ie_24_h),'N')
		into	ie_intervalo_24_w
		from	intervalo_prescricao
		where	cd_intervalo = :new.cd_intervalo
		and    	nvl(ie_operacao,'X') = 'H'
		and    	nvl(qt_operacao,1) > 24;	
	end if;

	qt_horas_ant_copia_cpoe_w := get_qt_hours_after_copy_cpoe( obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo);

	if (ie_intervalo_24_w = 'S') then
		qt_horas_adicional_w := Obter_ocorrencia_intervalo(:new.cd_intervalo,24,'H');
	else

		select 	nvl(max(ie_operacao),'X')
		into	ie_operacao_w
		from	intervalo_prescricao
		where	cd_intervalo = :new.cd_intervalo;

		qt_horas_adicional_w := Obter_ocorrencia_intervalo(:new.cd_intervalo,24,'H');		
		if (ie_operacao_w = 'H' and qt_horas_adicional_w > 12 and qt_horas_adicional_w < 24) then
			qt_horas_adicional_w := qt_horas_adicional_w + qt_horas_adicional_w;
		else 
			qt_horas_adicional_w := 24;
		end if;
	end if;

	hr_prim_horario_w := obter_prim_dshorarios(:new.ds_horarios);
	dt_inicio_item_w := to_date(to_char(dt_inicio_prescr_w, 'dd/mm/yyyy') || ' ' || hr_prim_horario_w || ':00', 'dd/mm/yyyy hh24:mi:ss');

	if (dt_inicio_item_w < dt_inicio_prescr_w) then
		dt_inicio_item_w := dt_inicio_item_w + 1;
	end if;

	dt_copia_cpoe_w	:= (dt_inicio_item_w + ((qt_horas_adicional_w - qt_horas_ant_copia_cpoe_w)/24));

	update	cpoe_material
	set		dt_prox_geracao = trunc(dt_copia_cpoe_w,'hh')
	where	nr_sequencia = nr_seq_mat_cpoe_w;

end if;


END;
/
