CREATE OR REPLACE TRIGGER latam_funcao_parametro_atual 
before insert or update on funcao_parametro 
for each row
DECLARE 

ds_user_w varchar2(100); 
PRAGMA AUTONOMOUS_TRANSACTION; 
BEGIN 
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then  
 
if	(nvl(:new.ds_parametro,'XPTO') <> nvl(:old.ds_parametro,'XPTO')) then 
	latam_gerar_dic_palavra(:new.ds_parametro, 'PA', null, null, null, null,null,null,null,null,:new.cd_funcao,:new.nr_sequencia,null,null,null,null, 
			null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null); 
	commit; 
end if; 
 
if	(nvl(:new.ds_observacao,'XPTO') <> nvl(:old.ds_observacao,'XPTO')) then 
	latam_gerar_dic_palavra(:new.ds_observacao, 'PO', null, null, null, null,null,null,null,null,:new.cd_funcao,:new.nr_sequencia,null,null,null,null, 
			null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null); 
	commit; 
end if; 
 
if	(nvl(:new.ds_dependencia,'XPTO') <> nvl(:old.ds_dependencia,'XPTO')) then 
	latam_gerar_dic_palavra(:new.ds_dependencia, 'PD', null, null, null, null,null,null,null,null,:new.cd_funcao,:new.nr_sequencia,null,null,null,null, 
			null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null); 
	commit; 
end if; 
end if; 
end;
/