create or replace trigger hd_acesso_befinsupd
before insert or update on hd_acesso
for each row

declare

begin

if	(:new.dt_perda_retirada is not null) and (:new.nr_seq_motivo_fim is null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(264191);
end if;

end;
/
