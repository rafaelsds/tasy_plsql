create or replace trigger compl_pessoa_fisica_AfterUp
after update or insert on compl_pessoa_fisica
for each row

declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_existe_medico_p		number(10) := 0;
nr_identidade_w				varchar2(15);
nm_pessoa_fisica_w			varchar2(60);
nr_telefone_celular_w			varchar2(40);
ie_grau_instrucao_w			number(2);
nr_cep_cidade_nasc_w			varchar2(15);
nr_prontuario_w				number(10);
cd_religiao_w				number(5);
nr_pis_pasep_w				varchar2(11);
cd_nacionalidade_w			varchar2(8);
ie_dependencia_sus_w			varchar2(1);
qt_altura_cm_w				number(4,1);
ie_tipo_sangue_w				varchar2(2);
ie_fator_rh_w				varchar2(1);
dt_nascimento_w				date;
dt_obito_w				date;
ie_sexo_w				varchar2(1);
nr_iss_w					varchar2(20);
ie_estado_civil_w				varchar2(2);
nr_inss_w					varchar2(20);
nr_cpf_w					varchar2(11);
nr_cert_nasc_w				varchar2(255);
cd_cargo_w				number(10);
nr_cert_casamento_w			varchar2(255);
ds_codigo_prof_w				varchar2(15);
cd_empresa_w				number(4);
ie_funcionario_w				varchar2(1);
nr_seq_cor_pele_w				number(10);
ds_orgao_emissor_ci_w			pessoa_fisica.ds_orgao_emissor_ci%type;
nr_cartao_nac_sus_w			varchar2(20);
cd_cbo_sus_w				number(6);
cd_atividade_sus_w			number(3);
ie_vinculo_sus_w				number(1);
cd_estabelecimento_w			number(4);
cd_sistema_ant_w				varchar2(20);
ie_frequenta_escola_w			varchar2(1);
cd_funcionario_w        varchar2(15);
nr_pager_bip_w        varchar2(25);
nr_transacao_sus_w      varchar2(20);
cd_medico_w        varchar2(10);
ie_tipo_prontuario_w      number(3);
dt_emissao_ci_w        date;
nr_seq_conselho_w        number(10);
dt_admissao_hosp_w      date;
ie_fluencia_portugues_w      varchar2(5);
nr_titulo_eleitor_w        varchar2(20);
nr_zona_w        varchar2(5);
nr_secao_w        varchar2(15);
nr_cartao_estrangeiro_w      varchar2(30);
nr_reg_geral_estrang_w      varchar2(30);
dt_chegada_brasil_w      date;
sg_emissora_ci_w        varchar2(2);
dt_naturalizacao_pf_w      date;
nr_ctps_w        pessoa_fisica.nr_ctps%type;
nr_serie_ctps_w        pessoa_fisica.nr_serie_ctps%type;
uf_emissora_ctps_w      pessoa_fisica.uf_emissora_ctps%type;
dt_emissao_ctps_w        date;
nr_portaria_nat_w        varchar2(16);
nr_seq_cartorio_nasc_w      number(10);
nr_seq_cartorio_casamento_w    number(10);
dt_emissao_cert_nasc_w      date;
dt_emissao_cert_casamento_w    date;
nr_livro_cert_nasc_w      number(10);
nr_livro_cert_casamento_w      number(10);
dt_cadastro_original_w      date;
nr_folha_cert_nasc_w      number(10);
nr_folha_cert_casamento_w      number(10);
nr_same_w        number(10);
ds_observacao_w        varchar2(2000);
qt_dependente_w        number(2);
nr_transplante_w        number(10);
dt_validade_rg_w        date;
dt_revisao_w        date;
dt_demissao_hosp_w      date;
cd_puericultura_w        number(6);
cd_cnes_w        varchar2(20);
ds_apelido_w        varchar2(60);
ie_endereco_correspondencia_w    number(2);
qt_peso_nasc_w        number(6,3);
uf_conselho_w        pessoa_fisica.uf_conselho%type;
nm_abreviado_w        varchar2(80);
nr_ccm_w        number(10);
dt_fim_experiencia_w      date;
dt_validade_conselho_w      date;
ds_profissao_w        varchar2(255);
ds_empresa_pf_w        varchar2(255);
nr_passaporte_w        varchar2(255);
nr_cnh_w          varchar2(255);
nr_cert_militar_w        varchar2(255);
nr_pront_ext_w        varchar2(100);
ie_escolaridade_cns_w      varchar2(10);
ie_situacao_conj_cns_w      varchar2(10);
nr_folha_cert_div_w      number(10);
nr_cert_divorcio_w        varchar2(20);
nr_seq_cartorio_divorcio_w      number(10);
dt_emissao_cert_divorcio_w      date;
nr_livro_cert_divorcio_w      number(10);
dt_alta_institucional_w      date;
dt_transplante_w        date;
nr_matricula_nasc_w      varchar2(32);
ie_doador_w        varchar2(15);
dt_vencimento_cnh_w      date;
ds_categoria_cnh_w      varchar2(30);
qt_existe_w        number(10);
ie_opcao_w        varchar2(1) := 'A';
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

select  count(*)
into  qt_existe_w
from  sup_parametro_integracao a,
  sup_int_regra_pf b
where  a.nr_sequencia = b.nr_seq_integracao
and  a.ie_evento = 'PF'
and  a.ie_forma = 'E'
and  a.ie_situacao = 'A'
and  b.ie_situacao = 'A';

if  (qt_existe_w > 0) and
  (:new.nm_usuario <> 'INTEGR_TASY') then
  
  select  nr_identidade,
    substr(obter_nome_pf(:new.cd_pessoa_fisica),1,60),
    nr_telefone_celular,
    ie_grau_instrucao,
    nr_cep_cidade_nasc,
    nr_prontuario,
    cd_religiao,
    nr_pis_pasep,
    cd_nacionalidade,
    ie_dependencia_sus,
    qt_altura_cm,
    ie_tipo_sangue,
    ie_fator_rh,
    dt_nascimento,
    dt_obito,
    ie_sexo,
    nr_iss,
    ie_estado_civil,
    nr_inss,
    nr_cpf,
    nr_cert_nasc,
    cd_cargo,
    nr_cert_casamento,
    ds_codigo_prof,
    cd_empresa,
    ie_funcionario,
    nr_seq_cor_pele,
    ds_orgao_emissor_ci,
    nr_cartao_nac_sus,
    cd_cbo_sus,
    cd_atividade_sus,
    ie_vinculo_sus,
    cd_sistema_ant,
    ie_frequenta_escola,
    cd_funcionario,
    nr_pager_bip,
    nr_transacao_sus,
    cd_medico,
    ie_tipo_prontuario,
    dt_emissao_ci,
    nr_seq_conselho,
    dt_admissao_hosp,
    ie_fluencia_portugues,
    nr_titulo_eleitor,
    nr_zona,
    nr_secao,
    nr_cartao_estrangeiro,
    nr_reg_geral_estrang,
    dt_chegada_brasil,
    sg_emissora_ci,
    dt_naturalizacao_pf,
    nr_ctps,
    nr_serie_ctps,
    uf_emissora_ctps,
    dt_emissao_ctps,
    nr_portaria_nat,
    nr_seq_cartorio_nasc,
    nr_seq_cartorio_casamento,
    dt_emissao_cert_nasc,
    dt_emissao_cert_casamento,
    nr_livro_cert_nasc,
    nr_livro_cert_casamento,
    dt_cadastro_original,
    nr_folha_cert_nasc,
    nr_folha_cert_casamento,
    nr_same,
    ds_observacao,
    qt_dependente,
    nr_transplante,
    dt_validade_rg,
    dt_revisao,
    dt_demissao_hosp,
    cd_puericultura,
    cd_cnes,
    ds_apelido,
    ie_endereco_correspondencia,
    qt_peso_nasc,
    uf_conselho,
    nm_abreviado,
    nr_ccm,
    dt_fim_experiencia,
    dt_validade_conselho,
    ds_profissao,
    ds_empresa_pf,
    nr_passaporte,
    nr_cnh,
    nr_cert_militar,
    nr_pront_ext,
    ie_escolaridade_cns,
    ie_situacao_conj_cns,
    nr_folha_cert_div,
    nr_cert_divorcio,
    nr_seq_cartorio_divorcio,
    dt_emissao_cert_divorcio,
    nr_livro_cert_divorcio,
    dt_alta_institucional,
    dt_transplante,
    nr_matricula_nasc,
    ie_doador,
    dt_vencimento_cnh,
    ds_categoria_cnh
  into  nr_identidade_w,
    nm_pessoa_fisica_w,
    nr_telefone_celular_w,
    ie_grau_instrucao_w,
    nr_cep_cidade_nasc_w,
    nr_prontuario_w,
    cd_religiao_w,
    nr_pis_pasep_w,
    cd_nacionalidade_w,
    ie_dependencia_sus_w,
    qt_altura_cm_w,
    ie_tipo_sangue_w,
    ie_fator_rh_w,
    dt_nascimento_w,
    dt_obito_w,
    ie_sexo_w,
    nr_iss_w,
    ie_estado_civil_w,
    nr_inss_w,
    nr_cpf_w,
    nr_cert_nasc_w,
    cd_cargo_w,
    nr_cert_casamento_w,
    ds_codigo_prof_w,
    cd_empresa_w,
    ie_funcionario_w,
    nr_seq_cor_pele_w,
    ds_orgao_emissor_ci_w,
    nr_cartao_nac_sus_w,
    cd_cbo_sus_w,
    cd_atividade_sus_w,
    ie_vinculo_sus_w,
    cd_sistema_ant_w,
    ie_frequenta_escola_w,
    cd_funcionario_w,
    nr_pager_bip_w,
    nr_transacao_sus_w,
    cd_medico_w,
    ie_tipo_prontuario_w,
    dt_emissao_ci_w,
    nr_seq_conselho_w,
    dt_admissao_hosp_w,
    ie_fluencia_portugues_w,
    nr_titulo_eleitor_w,
    nr_zona_w,
    nr_secao_w,
    nr_cartao_estrangeiro_w,
    nr_reg_geral_estrang_w,
    dt_chegada_brasil_w,
    sg_emissora_ci_w,
    dt_naturalizacao_pf_w,
    nr_ctps_w,
    nr_serie_ctps_w,
    uf_emissora_ctps_w,
    dt_emissao_ctps_w,
    nr_portaria_nat_w,
    nr_seq_cartorio_nasc_w,
    nr_seq_cartorio_casamento_w,
    dt_emissao_cert_nasc_w,
    dt_emissao_cert_casamento_w,
    nr_livro_cert_nasc_w,
    nr_livro_cert_casamento_w,
    dt_cadastro_original_w,
    nr_folha_cert_nasc_w,
    nr_folha_cert_casamento_w,
    nr_same_w,
    ds_observacao_w,
    qt_dependente_w,
    nr_transplante_w,
    dt_validade_rg_w,
    dt_revisao_w,
    dt_demissao_hosp_w,
    cd_puericultura_w,
    cd_cnes_w,
    ds_apelido_w,
    ie_endereco_correspondencia_w,
    qt_peso_nasc_w,
    uf_conselho_w,
    nm_abreviado_w,
    nr_ccm_w,
    dt_fim_experiencia_w,
    dt_validade_conselho_w,
    ds_profissao_w,
    ds_empresa_pf_w,
    nr_passaporte_w,
    nr_cnh_w,
    nr_cert_militar_w,
    nr_pront_ext_w,
    ie_escolaridade_cns_w,
    ie_situacao_conj_cns_w,
    nr_folha_cert_div_w,
    nr_cert_divorcio_w,
    nr_seq_cartorio_divorcio_w,
    dt_emissao_cert_divorcio_w,
    nr_livro_cert_divorcio_w,
    dt_alta_institucional_w,
    dt_transplante_w,
    nr_matricula_nasc_w,
    ie_doador_w,
    dt_vencimento_cnh_w,
    ds_categoria_cnh_w
  from  pessoa_fisica
  where  cd_pessoa_fisica = :new.cd_pessoa_fisica;


  envia_sup_int_pf(
    :new.cd_pessoa_fisica,
    nr_identidade_w,
    nm_pessoa_fisica_w,
    nr_telefone_celular_w,
    ie_grau_instrucao_w,
    nr_cep_cidade_nasc_w,
    nr_prontuario_w,
    :new.nm_usuario,
    cd_religiao_w,
    nr_pis_pasep_w,
    cd_nacionalidade_w,
    ie_dependencia_sus_w,
    qt_altura_cm_w,
    ie_tipo_sangue_w,
    ie_fator_rh_w,
    dt_nascimento_w,
    dt_obito_w,
    ie_sexo_w,
    nr_iss_w,
    ie_estado_civil_w,
    nr_inss_w,
    nr_cpf_w,
    nr_cert_nasc_w,
    cd_cargo_w,
    nr_cert_casamento_w,
    ds_codigo_prof_w,
    cd_empresa_w,
    ie_funcionario_w,
    nr_seq_cor_pele_w,
    ds_orgao_emissor_ci_w,
    nr_cartao_nac_sus_w,
    cd_cbo_sus_w,
    cd_atividade_sus_w,
    ie_vinculo_sus_w,
    cd_sistema_ant_w,
    ie_frequenta_escola_w,
    cd_funcionario_w,
    nr_pager_bip_w,
    nr_transacao_sus_w,
    cd_medico_w,
    ie_tipo_prontuario_w,
    dt_emissao_ci_w,
    nr_seq_conselho_w,
    dt_admissao_hosp_w,
    ie_fluencia_portugues_w,
    nr_titulo_eleitor_w,
    nr_zona_w,
    nr_secao_w,
    nr_cartao_estrangeiro_w,
    nr_reg_geral_estrang_w,
    dt_chegada_brasil_w,
    sg_emissora_ci_w,
    dt_naturalizacao_pf_w,
    nr_ctps_w,
    nr_serie_ctps_w,
    uf_emissora_ctps_w,
    dt_emissao_ctps_w,
    nr_portaria_nat_w,
    nr_seq_cartorio_nasc_w,
    nr_seq_cartorio_casamento_w,
    dt_emissao_cert_nasc_w,
    dt_emissao_cert_casamento_w,
    nr_livro_cert_nasc_w,
    nr_livro_cert_casamento_w,
    dt_cadastro_original_w,
    nr_folha_cert_nasc_w,
    nr_folha_cert_casamento_w,
    nr_same_w,
    ds_observacao_w,
    qt_dependente_w,
    nr_transplante_w,
    dt_validade_rg_w,
    dt_revisao_w,
    dt_demissao_hosp_w,
    cd_puericultura_w,
    cd_cnes_w,
    ds_apelido_w,
    ie_endereco_correspondencia_w,
    qt_peso_nasc_w,
    uf_conselho_w,
    nm_abreviado_w,
    nr_ccm_w,
    dt_fim_experiencia_w,
    dt_validade_conselho_w,
    ds_profissao_w,
    ds_empresa_pf_w,
    nr_passaporte_w,
    nr_cnh_w,
    nr_cert_militar_w,
    nr_pront_ext_w,
    ie_escolaridade_cns_w,
    ie_situacao_conj_cns_w,
    nr_folha_cert_div_w,
    nr_cert_divorcio_w,
    nr_seq_cartorio_divorcio_w,
    dt_emissao_cert_divorcio_w,
    nr_livro_cert_divorcio_w,
    dt_alta_institucional_w,
    dt_transplante_w,
    nr_matricula_nasc_w,
    ie_doador_w,
    dt_vencimento_cnh_w,
    ds_categoria_cnh_w,
    
    :new.nm_contato,
    :new.ds_endereco,
    :new.cd_cep,
    :new.nr_endereco,
    :new.ds_complemento,
    :new.ds_municipio,
    substr(:new.ds_bairro,1,40),
    :new.sg_estado,
    :new.nr_telefone,
    :new.nr_ramal,
    :new.ds_fone_adic,
    :new.ds_email,
    :new.cd_profissao,
    :new.cd_empresa_refer,
    :new.ds_setor_trabalho,
    :new.ds_horario_trabalho,
    :new.nr_matricula_trabalho,
    :new.cd_municipio_ibge,
    :new.ds_fax,
    :new.cd_tipo_logradouro,
    :new.nr_ddd_telefone,
    :new.nr_ddi_telefone,
    :new.nr_ddi_fax,
    :new.nr_ddd_fax,
    :new.ds_website,
    :new.nm_contato_pesquisa,
    :new.ie_tipo_complemento);
end if;

reg_integracao_p.ie_operacao    :=  'A';
reg_integracao_p.nr_prontuario       :=   nr_prontuario_w;
reg_integracao_p.cd_pessoa_fisica       :=   :new.cd_pessoa_fisica;
reg_integracao_p.ie_funcionario    :=  nvl(ie_funcionario_w,'N');
reg_integracao_p.nr_seq_conselho  :=  nr_seq_conselho_w;

SELECT decode(COUNT(*), 0, 'N', 'S')
  INTO reg_integracao_p.ie_pessoa_atend
  FROM pessoa_fisica_aux
 WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica
   AND nr_primeiro_atend IS NOT NULL;
   
if nr_cpf_w is not null then
   reg_integracao_p.ie_possui_cpf := 'S';
end if;

if  (wheb_usuario_pck.get_ie_lote_contabil = 'N') then
  gerar_int_padrao.gravar_integracao('12', :new.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
end if;  

if  (ie_funcionario_w = 'S') then
  gerar_int_padrao.gravar_integracao('304',:new.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
end if;

send_physician_intpd(:new.cd_pessoa_fisica, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'U');

end if;
end;
/
