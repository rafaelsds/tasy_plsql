CREATE OR REPLACE TRIGGER Agenda_Bloqueio_Delete
before delete ON Agenda_Bloqueio
FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
and		dt_agenda between :old.dt_inicial and :old.dt_final
and		cd_agenda	 = :old.cd_agenda;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/
