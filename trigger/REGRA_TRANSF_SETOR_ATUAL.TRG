create or replace trigger regra_transf_setor_atual
before insert or update on regra_transferencia_setor
for each row

declare

begin
if (:new.ds_mensagem_bloqueio is not null and nvl(:new.ie_permite_transferencia,'S') = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort( 1046036);
end if;

end;
/