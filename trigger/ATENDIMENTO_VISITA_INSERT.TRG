CREATE OR REPLACE TRIGGER atendimento_visita_insert
before INSERT ON atendimento_visita
FOR EACH ROW

declare
nr_seq_controle_w	number(15,0);
ie_controla_w		varchar2(5);
ie_considera_estab_w	varchar2(5);
ie_controle_diferente_w	varchar2(5);
ie_possui_w		varchar2(5);
ie_possui_alta_w	varchar2(2);
ie_permite_alta_w     varchar(2);
nr_controle_unico_w		varchar(2);
ds_origem_w			varchar2(1800);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	Obter_Param_Usuario(44, 10, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_controla_w);
	Obter_Param_Usuario(8014, 59, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_considera_estab_w);
	Obter_Param_Usuario(8014, 97, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_controle_diferente_w);
	Obter_Param_Usuario(8014, 112, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_permite_alta_w);
	Obter_Param_Usuario(8014, 119, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, nr_controle_unico_w);

	Select 	nvl(max('S'),'N')
	into 	ie_possui_alta_w
	from 	atendimento_paciente a
	where 	:new.nr_atendimento = a.nr_atendimento
	and 	a.dt_alta is not null;

	if 	(ie_possui_alta_w = 'S') and
		(ie_permite_alta_w = 'N')	then
		wheb_mensagem_pck.exibir_mensagem_abort(284133);
	end if;


	if	(ie_controla_w = 'S') and
		(:new.nr_seq_controle is null) then

			if (ie_considera_estab_w = 'S') then
				nr_seq_controle_w	:=	obter_nr_controle_estab(wheb_usuario_pck.get_cd_estabelecimento);
			
			elsif(nr_controle_unico_w = 'S') then
					select	CONTROLE_VISITA_UNICO_SEQ.nextval
					into	nr_seq_controle_w
					from	dual;
			else
				select	atendimento_visita_seq2.nextval
				into	nr_seq_controle_w
				from	dual;
			end if;

		:new.nr_seq_controle	:= nr_seq_controle_w;
	end if;

	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
		
	insert into log_mov(
		dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values(	sysdate,
		'Tasy',
		131713,
		'obter_perfil_ativo: '||obter_perfil_ativo||
		' - :new.nm_usuario: '||:new.nm_usuario||
		' - :new.nr_seq_controle: '||:new.nr_seq_controle||
		' - Stack: ' || ds_origem_w);

	select	controle_visita_seq.nextval
	into	:new.nr_controle_acesso
	from	dual;

	if (ie_controle_diferente_w = 'S') then

		nr_seq_controle_w	:= obter_novo_numero_controle(:new.nr_seq_controle);
		:new.nr_seq_controle	:= nr_seq_controle_w;

	end if;
end if;	
END;
/
