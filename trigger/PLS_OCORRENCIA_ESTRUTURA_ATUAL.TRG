create or replace trigger pls_ocorrencia_estrutura_atual
before insert or update or delete on pls_ocorrencia_estrutura
for each row

declare 
nr_sequencia_w	pls_ocorrencia_estrutura.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_sequencia_w := :old.nr_sequencia;
else
	nr_sequencia_w := :new.nr_sequencia;
end if;

pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_ESTRUTURA_OCOR_TM', wheb_usuario_pck.get_nm_usuario,
							'PLS_OCORRENCIA_ESTRUTURA_ATUAL',
							'nr_seq_ocor_estrut_p=' || nr_sequencia_w);

end;
/
