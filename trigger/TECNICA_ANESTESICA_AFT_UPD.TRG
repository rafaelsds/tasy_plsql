CREATE OR REPLACE TRIGGER TECNICA_ANESTESICA_AFT_UPD 
before insert or update on TECNICA_ANESTESICA 
FOR EACH ROW 
DECLARE
/* OBS: Ao editar pelo Java/HTML (tipo CLOB), so ira sobrescrever o conteudo do campo em Delphi (tipo Varchar2) quando o conteudo do campo CLOB for igual ou inferior a 4000 contando os caracteres do RTF .
** Quando editar via Delphi, sempre ira sobrescrever o conteudo do campo usado em Java/HTML para nao haver impactos quando houver migracao de plataforma ou quando utilizado em contingencia
*/
begin
if (updating) then
	if ((nvl(:old.ds_texto_padrao_clob, 'XPTO') <> nvl(:new.ds_texto_padrao_clob, 'XPTO')) 
		and DBMS_LOB.getLength(nvl(:new.ds_texto_padrao_clob, 'XPTO')) <= 4000)then /*quando alterado pelo Java ou HTML*/
		:new.ds_texto_padrao := SUBSTR(:new.ds_texto_padrao_clob,1,4000);
	elsif (:old.ds_texto_padrao <> :new.ds_texto_padrao) then /*quando alterado pelo Delphi*/
		:new.ds_texto_padrao_clob := :new.ds_texto_padrao;
	end if;
elsif (inserting) then
	if ((nvl(:new.ds_texto_padrao_clob,'XPTO') <> 'XPTO' or :new.ds_texto_padrao_clob is not null) 
		and DBMS_LOB.getLength(:new.ds_texto_padrao_clob) <= 4000) then /*quando alterado pelo Java ou HTML*/
		:new.ds_texto_padrao := SUBSTR(:new.ds_texto_padrao_clob,1,4000);
	elsif (nvl(:new.ds_texto_padrao,'XPTO') <> 'XPTO') then /*quando alterado pelo Delphi*/
		:new.ds_texto_padrao_clob := :new.ds_texto_padrao;
	end if;
end if;

end;
/
