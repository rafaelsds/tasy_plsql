create or replace trigger sup_regra_fornec_compras_atual
before insert or update on regra_fornecedor_compras
for each row

begin
if	(:new.cd_cgc_fornecedor is not null and :new.cd_pessoa_fisica is not null) then
	begin
	-- 462188 - Favor informar somente uma pessoa jur�dica ou f�sica!
	Wheb_mensagem_pck.exibir_mensagem_abort(462188);
	end;
end if;
end;
/