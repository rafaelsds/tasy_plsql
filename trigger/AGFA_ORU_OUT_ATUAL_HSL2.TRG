CREATE OR REPLACE TRIGGER TASY.AGFA_ORU_OUT_ATUAL
BEFORE INSERT or UPDATE ON AGFA_ORU_OUT
FOR EACH ROW
declare

/*�ltima altera��o: OS313121 - 22/06/2011 - takano/tbschulz*/

nm_usuario_w        varchar2(15);
nr_prescricao_w        number(14);
nr_seq_prescr_w        number(6);
nr_atendimento_w    number(10);
cd_setor_atendimento_w    number(5);
dt_entrada_unidade_w    date;
nr_seq_propaci_w    number(10);
nr_laudo_w        number(10);
ds_titulo_laudo_w    varchar2(255)    := 'Integra��o TasyXAGFA';
cd_laudo_padrao_w    number(5)    := null;
ds_laudo_w        long;
cd_protocolo_w        number(10)    := null;
cd_projeto_w        number(6)    := null;
dt_prev_entrega_w    date        := null;
dt_entrega_real_w    date        := null;
nr_controle_w        number(15)    := null;
nr_seq_motivo_parada_w    number(10)    := null;
ds_erro_w        varchar2(2000);
nr_seq_laudo_w	number(10)	:= 0;

ie_cod_novo_status_w	varchar2(5);


dt_aprovacao_w		date;
dt_seg_aprovacao_w	date;
dt_liberacao_w		date;

begin

if	(:new.ORU_02_STATUS_HIS = 'N') then
    begin
    select	nvl(max(nr_prescricao),null),
        	nvl(max(nr_sequencia),null),
		nvl(max(substr(obter_desc_prescr_proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,255)), ds_titulo_laudo_w)
    into    nr_prescricao_w,
        nr_seq_prescr_w,
	ds_titulo_laudo_w
    from    prescr_procedimento
    where    nr_acesso_dicom = :new.OBR_03_ACC_NUMBER;

	/*Elemar - 16/04/2008 - OS88839*/
    if	(nr_prescricao_w is null) and
    	(:New.OBR_02_QP_EXAM_ID Is Not Null) and
	(Instr(:New.OBR_02_QP_EXAM_ID,'-') > 0) Then
	  nr_prescricao_w	:= substr(:New.OBR_02_QP_EXAM_ID,1,instr(:New.OBR_02_QP_EXAM_ID,'-') - 1);
	  nr_seq_prescr_w	:= substr(:New.OBR_02_QP_EXAM_ID,instr(:New.OBR_02_QP_EXAM_ID,'-') + 1,length(:New.OBR_02_QP_EXAM_ID));

	  Agfa_Prescr_Proc_Acc_Number(	nr_prescricao_w,
					nr_seq_prescr_w,
 					:New.OBR_03_ACC_NUMBER,
					ds_erro_w);

	select	nvl(max(substr(obter_desc_prescr_proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,255)), ds_titulo_laudo_w)
    into    ds_titulo_laudo_w
    from    prescr_procedimento
    where    nr_acesso_dicom = :new.OBR_03_ACC_NUMBER;

      if    (ds_erro_w Is Not Null) Then
          :New.ORU_02_STATUS_HIS := 'E';
      end If;
    end if;


    if  (nr_prescricao_w is not null) and
    (:new.ORU_02_STATUS_HIS = 'N') then
        select    nvl(max(nr_atendimento), null),
            nvl(max(cd_setor_atendimento), null),
            nvl(max(dt_entrada_unidade), null),
            nvl(max(nr_sequencia), null)
        into    nr_atendimento_w,
            cd_setor_atendimento_w,
            dt_entrada_unidade_w,
            nr_seq_propaci_w
        from    procedimento_paciente
        where    nr_prescricao = nr_prescricao_w
          and    nr_sequencia_prescricao = nr_seq_prescr_w;
    end if;

    if    (nr_atendimento_w is not null) then
        select    nvl(max(nm_usuario),'AGFA_ORU_OUT')
        into    nm_usuario_w
        from    usuario
        where    cd_pessoa_fisica = :new.OBR_32_AUTHOR_ID;

    select    nvl(max(nr_sequencia),0)
    into    nr_seq_laudo_w
    from    laudo_paciente
    where    nr_atendimento    = nr_atendimento_w
      and    nr_seq_proc    = nr_seq_propaci_w;


    ie_cod_novo_status_w := substr(:new.OBR_25_STATUS_REP,1, instr(:new.OBR_25_STATUS_REP,'^') -1);

    if    (nr_seq_laudo_w > 0) then

        GERAR_COPIA_LAUDO(nr_seq_laudo_w, nm_usuario_w);


        /*if    (ie_cod_novo_status_w = '3' ) then --Parcialmente validado

            update    laudo_paciente
            set    dt_aprovacao     = sysdate,
                dt_atualizacao     = sysdate,
                nm_usuario     = nm_usuario_w
            where    nr_sequencia     = nr_seq_laudo_w;

        else*/


        delete ENVELOPE_LAUDO_ITEM where nr_seq_laudo = nr_seq_laudo_w;


        delete laudo_paciente where nr_sequencia = nr_seq_laudo_w;

        --end if;

        if    (ie_cod_novo_status_w = '0') then
            update    laudo_paciente_copia
            set    ds_titulo_laudo = substr(ds_titulo_laudo || ' - Cancelado',1,255)
            where    nr_seq_laudo = nr_seq_laudo_w;

            nr_seq_laudo_w := -1;
        end if;
    end if;

    if    (ie_cod_novo_status_w = '0') and
        (nr_seq_laudo_w    = 0)then
        nr_seq_laudo_w := -2;
    end if;

    if    (nr_seq_laudo_w > -1) then
            select    nvl(max(nr_laudo),0) + 1
            into    nr_laudo_w
            from    laudo_paciente
            where    nr_atendimento = nr_atendimento_w;

        begin
        select    dbms_lob.substr(:new.OBX_05_OBSERVATION, dbms_lob.GetLength(:new.OBX_05_OBSERVATION), 1)
        into    ds_laudo_w
        from    dual;
        exception
            when others then
                select    dbms_lob.substr(:new.OBX_05_OBSERVATION, 4000, 1)
                into    ds_laudo_w
                from    dual;
        end;


        if    (ie_cod_novo_status_w = '3') then   --Parcialmente validado

            dt_liberacao_w     := null;
            dt_aprovacao_w        := sysdate;
            dt_seg_aprovacao_w := null;

        else
            dt_liberacao_w       := :new.OBR_27_VALIDATE_DATE;
            dt_aprovacao_w       := :new.OBR_27_VALIDATE_DATE;
            dt_seg_aprovacao_w := :new.OBR_27_VALIDATE_DATE;

        end if;


            insert into laudo_paciente (    NR_SEQUENCIA,        NR_ATENDIMENTO,
                            DT_ENTRADA_UNIDADE,    NR_LAUDO,
                            NM_USUARIO,        DT_ATUALIZACAO,
                            CD_MEDICO_RESP,        DS_TITULO_LAUDO,
                            DT_LAUDO,        CD_LAUDO_PADRAO,
                            IE_NORMAL,        DT_EXAME,
                            NR_PRESCRICAO,        DS_LAUDO,
                            DT_APROVACAO,        NM_USUARIO_APROVACAO,
                            CD_PROTOCOLO,        CD_PROJETO,
                            NR_SEQ_PROC,        NR_SEQ_PRESCRICAO,
                            DT_LIBERACAO,        DT_PREV_ENTREGA,
                            DT_REAL_ENTREGA,    QT_IMAGEM,
                            NR_CONTROLE,        DT_SEG_APROVACAO,
                            NM_USUARIO_SEG_APROV,    NR_SEQ_MOTIVO_PARADA,
                            CD_SETOR_ATENDIMENTO,    NR_EXAME,
                            CD_MEDICO_AUX,        DT_ENVELOPADO,
                            DT_IMPRESSAO,        DT_FIM_DIGITACAO,
                            NM_USUARIO_DIGITACAO,    DT_INICIO_DIGITACAO,
                            DT_INTEGRACAO,        NM_MEDICO_SOLICITANTE,
                            IE_MIDIA_ENTREGUE,    CD_TECNICO_RESP,
                            CD_SETOR_USUARIO,    NM_USUARIO_CANCEL,
                            DT_CANCELAMENTO,    NM_USUARIO_LIBERACAO, IE_FORMATO)
            values (laudo_paciente_seq.NextVal,    nr_atendimento_w,
                dt_entrada_unidade_w,        nr_laudo_w,
                nm_usuario_w,            :new.MSH_07_DATE,
                :new.OBR_32_AUTHOR_ID,        ds_titulo_laudo_w,
                :new.MSH_07_DATE,        cd_laudo_padrao_w,
                'S',                :new.MSH_07_DATE,
                nr_prescricao_w,        ds_laudo_w,
                dt_aprovacao_w,    nm_usuario_w,
                cd_protocolo_w,            cd_projeto_w,
                nr_seq_propaci_w,        nr_seq_prescr_w,
                dt_liberacao_w,    dt_prev_entrega_w,
                dt_entrega_real_w,        0,
                nr_controle_w,            dt_seg_aprovacao_w,
                nm_usuario_w,            nr_seq_motivo_parada_w,
                cd_setor_atendimento_w,        :new.ORU_03_ID_REPORT,
                null,                null,
                null,                null,
                null,                null,
                sysdate,            null,
                null,                null,
            null,                null,
                null,                null,2);
    end if;
        :new.ORU_02_STATUS_HIS    := 'S';
    if    (nr_seq_laudo_w    = -2) then
        :new.ORU_02_STATUS_HIS    := 'I';
    end if;
    end if;
    exception
        when others then
            :new.ORU_02_STATUS_HIS    := 'E';
            ds_erro_w        := substr(sqlerrm,1,2000);
            insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
                    values    (sysdate, nm_usuario_w, -33333, ds_erro_w || :new.OBR_03_ACC_NUMBER);
    end;
end if;

end AGFA_ORU_OUT_ATUAL;
/
