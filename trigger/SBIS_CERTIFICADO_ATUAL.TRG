CREATE OR REPLACE 
TRIGGER SBIS_CERTIFICADO_ATUAL
BEFORE INSERT OR UPDATE ON SBIS_CERTIFICADO
FOR EACH ROW
DECLARE

dt_default_w   date  := sysdate+10;

BEGIN

if	(nvl(:old.DT_CERTIFICADO,dt_default_w) <> NVL(:new.DT_CERTIFICADO, dt_default_w) and
	(:new.DT_CERTIFICADO is not null)) then
	:new.dt_certificado_utc    := obter_data_utc(:new.DT_certificado, 'HV');	
end if;	

END;
/
