create or replace trigger PRP_ETAPA_PROCESSO_UPDATE
before update on PRP_ETAPA_PROCESSO
for each row

declare
qt_registros_w	number(10);

begin

if (:new.ie_situacao = 'I') then

	select	count(*)
	into	qt_registros_w
	from	PRP_ETAPA_DEPENDENCIA
	where	NR_SEQ_ETAPA_PROCESSO = :new.nr_sequencia;

	if (qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(309514);
	end if;
	
	select	count(*)
	into	qt_registros_w
	from	PRP_NECESSIDADE_ETAPA
	where	NR_SEQ_ETAPA_PROCESSO = :new.nr_sequencia;

	if (qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(309514);
	end if;

end if;

end;
/
