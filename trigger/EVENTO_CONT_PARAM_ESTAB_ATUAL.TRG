create or replace trigger evento_cont_param_estab_atual
before insert or update on evento_contabil_param_estab
for each row
declare
ie_conta_vigente_w		varchar2(1);
ie_tipo_w		conta_contabil.ie_tipo%type;

BEGIN

ie_conta_vigente_w	:= substr(obter_se_conta_vigente(:new.cd_conta_contabil, sysdate),1,1);

if	(:new.cd_conta_contabil is not null) then
	begin
	ie_tipo_w	:= substr(obter_dados_conta_contabil(:new.cd_conta_contabil,null,'T'),1,1);
	if	(ie_tipo_w = 'T') then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(43666, 'CTA=' || :new.CD_CONTA_CONTABIL);
	end if;
	end;
end if;

if	(ie_conta_vigente_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(232821,'CD_CONTA_CONTABIL_W=' || :new.cd_conta_contabil);
end if;

END;
/