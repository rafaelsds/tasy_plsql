create or replace trigger alteracao_tasy_insert
before update on alteracao_tasy
for each row

declare

begin
:new.ie_interno_externo := nvl(:new.ie_interno_externo,'E');

end;
/