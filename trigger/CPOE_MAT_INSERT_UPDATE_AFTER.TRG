create or replace trigger cpoe_mat_insert_update_after
after insert or update on cpoe_material
for each row
declare

ds_stack_w				varchar2(2000);
dt_inicio_medic_w 		date;
hr_prim_horario_w		varchar2(20);
ds_horarios_w			varchar2(2000);
ds_horarios_medico_w	varchar2(2000);
ds_log_cpoe_w			varchar2(2000);
dt_min_date_w			date := to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss');

ie_order_integr_type_w 	varchar2(10);
nr_entity_identifier_w	cpoe_integracao.nr_sequencia%type;
ie_use_integration_w	varchar2(10);

begin

if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') then

	begin

	if	(nvl(:new.nr_sequencia,0) <> nvl(:old.nr_sequencia,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_sequencia(' || nvl(:old.nr_sequencia,0) || '/' || nvl(:new.nr_sequencia,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_cpoe_anterior,0) <> nvl(:old.nr_seq_cpoe_anterior,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_cpoe_anterior(' || nvl(:old.nr_seq_cpoe_anterior,0) || '/' || nvl(:new.nr_seq_cpoe_anterior,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_adicional,0) <> nvl(:old.nr_seq_adicional,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_adicional(' || nvl(:old.nr_seq_adicional,0) || '/' || nvl(:new.nr_seq_adicional,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_material,0) <> nvl(:old.cd_material,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_material(' || nvl(:old.cd_material,0) || '/' || nvl(:new.cd_material,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_mat_dil,0) <> nvl(:old.cd_mat_dil,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_mat_dil(' || nvl(:old.cd_mat_dil,0) || '/' || nvl(:new.cd_mat_dil,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_mat_red,0) <> nvl(:old.cd_mat_red,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_mat_red(' || nvl(:old.cd_mat_red,0) || '/' || nvl(:new.cd_mat_red,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao, dt_min_date_w) <> nvl(:old.dt_liberacao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_suspensao, dt_min_date_w) <> nvl(:old.dt_suspensao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_suspensao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_forma_suspensao,'XPTO') <> nvl(:old.ie_forma_suspensao,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_forma_suspensao(' || nvl(:old.ie_forma_suspensao,'<NULL>') || '/' || nvl(:new.ie_forma_suspensao,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_lib_suspensao, dt_min_date_w) <> nvl(:old.dt_lib_suspensao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_lib_suspensao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_lib_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_lib_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao_enf, dt_min_date_w) <> nvl(:old.dt_liberacao_enf, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao_enf(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao_enf, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao_enf, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao_farm, dt_min_date_w) <> nvl(:old.dt_liberacao_farm, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao_farm(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao_farm, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao_farm, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_prox_geracao, sysdate) <> nvl(:old.dt_prox_geracao, sysdate)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_prox_geracao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_prox_geracao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_prox_geracao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_inicio, sysdate) <> nvl(:old.dt_inicio, sysdate)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_inicio(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_inicio, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_inicio, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_fim, sysdate) <> nvl(:old.dt_fim, sysdate)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_fim(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_fim, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_fim, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ds_horarios,'XPTO') <> nvl(:old.ds_horarios,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ds_horarios(' || nvl(:old.ds_horarios,'<NULL>') || '/' || nvl(:new.ds_horarios,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_evento_unico,'XPTO') <> nvl(:old.ie_evento_unico,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_evento_unico(' || nvl(:old.ie_evento_unico,'<NULL>') || '/' || nvl(:new.ie_evento_unico,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_administracao,'XPTO') <> nvl(:old.ie_administracao,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_administracao(' || nvl(:old.ie_administracao,'<NULL>') || '/' || nvl(:new.ie_administracao,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ds_justificativa,'XPTO') <> nvl(:old.ds_justificativa,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ds_justificativa(' || nvl(length(:old.ds_justificativa),0) || '/' || nvl(length(:new.ds_justificativa),0)||'); ',1,2000);
	end if;

	if	(nvl(:new.hr_prim_horario,'XPTO') <> nvl(:old.hr_prim_horario,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' hr_prim_horario(' || nvl(:old.hr_prim_horario,'<NULL>') || '/' || nvl(:new.hr_prim_horario,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_setor_atendimento,0) <> nvl(:old.cd_setor_atendimento,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_setor_atendimento(' || nvl(:old.cd_setor_atendimento,0) || '/' || nvl(:new.cd_setor_atendimento,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_intervalo,'XPTO') <> nvl(:old.cd_intervalo,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_intervalo(' || nvl(:old.cd_intervalo,'<NULL>') || '/' || nvl(:new.cd_intervalo,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_baixado_por_alta,'XPTO') <> nvl(:old.ie_baixado_por_alta,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_baixado_por_alta(' || nvl(:old.ie_baixado_por_alta,'<NULL>') || '/' || nvl(:new.ie_baixado_por_alta,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_alta_medico, dt_min_date_w) <> nvl(:old.dt_alta_medico, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_alta_medico(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_alta_medico, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_alterou_horario,'XPTO') <> nvl(:old.ie_alterou_horario,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_alterou_horario(' || nvl(:old.ie_alterou_horario,'<NULL>') || '/' || nvl(:new.ie_alterou_horario,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_fim_cih, dt_min_date_w) <> nvl(:old.dt_fim_cih, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_fim_cih(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_fim_cih, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_fim_cih, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_dia_util,0) <> nvl(:old.nr_dia_util,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_dia_util(' || nvl(:old.nr_dia_util,0) || '/' || nvl(:new.nr_dia_util,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_ocorrencia,0) <> nvl(:old.nr_ocorrencia,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_ocorrencia(' || nvl(:old.nr_ocorrencia,0) || '/' || nvl(:new.nr_ocorrencia,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.qt_dias_solicitado,0) <> nvl(:old.qt_dias_solicitado,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_dias_solicitado(' || nvl(:old.qt_dias_solicitado,0) || '/' || nvl(:new.qt_dias_solicitado,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.qt_dias_liberado,0) <> nvl(:old.qt_dias_liberado,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_dias_liberado(' || nvl(:old.qt_dias_liberado,0) || '/' || nvl(:new.qt_dias_liberado,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.qt_total_dias_lib,0) <> nvl(:old.qt_total_dias_lib,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_total_dias_lib(' || nvl(:old.qt_total_dias_lib,0) || '/' || nvl(:new.qt_total_dias_lib,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.ds_dose_diferenciada,'XPTO') <> nvl(:old.ds_dose_diferenciada,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ds_dose_diferenciada(' || nvl(:old.ds_dose_diferenciada,'<NULL>') || '/' || nvl(:new.ds_dose_diferenciada,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_tipo_solucao,'XPTO') <> nvl(:old.ie_tipo_solucao,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_tipo_solucao(' || nvl(:old.ie_tipo_solucao,'<NULL>') || '/' || nvl(:new.ie_tipo_solucao,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_etapas,0) <> nvl(:old.nr_etapas,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_etapas(' || nvl(:old.nr_etapas,0) || '/' || nvl(:new.nr_etapas,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_medicacao_paciente,'N') <> nvl(:old.ie_medicacao_paciente,'N')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' IE_MEDICACAO_PACIENTE(' || nvl(:old.ie_medicacao_paciente,'N') || '/' || nvl(:new.ie_medicacao_paciente,'N')||'); ',1,1800);
	end if;

	if	(nvl(:new.cd_perfil_ativo,0) <> nvl(:old.cd_perfil_ativo,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_perfil_ativo(' || nvl(:old.cd_perfil_ativo,0) || '/' || nvl(:new.cd_perfil_ativo,0)||'); ',1,2000);
	end if;
	
	if	(nvl(:new.nm_usuario_lib_enf,'XPTO') <> nvl(:old.nm_usuario_lib_enf,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nm_usuario_lib_enf(' || nvl(:old.nm_usuario_lib_enf,'<NULL>') || '/' || nvl(:new.nm_usuario_lib_enf,'<NULL>')||'); ',1,2000);
	end if;
	
	if	(nvl(:new.cd_unid_med_dose_comp1,'XPTO') <> nvl(:old.cd_unid_med_dose_comp1,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_unid_med_dose_comp1(' || nvl(:old.cd_unid_med_dose_comp1,'<NULL>') || '/' || nvl(:new.cd_unid_med_dose_comp1,'<NULL>')||'); ',1,2000);
	end if;
	
	if	(nvl(:new.cd_unid_med_dose_comp2,'XPTO') <> nvl(:old.cd_unid_med_dose_comp2,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_unid_med_dose_comp2(' || nvl(:old.cd_unid_med_dose_comp2,'<NULL>') || '/' || nvl(:new.cd_unid_med_dose_comp2,'<NULL>')||'); ',1,2000);
	end if;
	
	if	(nvl(:new.cd_unid_med_dose_comp3,'XPTO') <> nvl(:old.cd_unid_med_dose_comp3,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_unid_med_dose_comp3(' || nvl(:old.cd_unid_med_dose_comp3,'<NULL>') || '/' || nvl(:new.cd_unid_med_dose_comp3,'<NULL>')||'); ',1,2000);
	end if;
	
	if	(nvl(:new.cd_unid_med_dose_comp4,'XPTO') <> nvl(:old.cd_unid_med_dose_comp4,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_unid_med_dose_comp4(' || nvl(:old.cd_unid_med_dose_comp4,'<NULL>') || '/' || nvl(:new.cd_unid_med_dose_comp4,'<NULL>')||'); ',1,2000);
	end if;
	
	if	(nvl(:new.cd_unid_med_dose_comp5,'XPTO') <> nvl(:old.cd_unid_med_dose_comp5,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_unid_med_dose_comp5(' || nvl(:old.cd_unid_med_dose_comp5,'<NULL>') || '/' || nvl(:new.cd_unid_med_dose_comp5,'<NULL>')||'); ',1,2000);
	end if;
	
	if	(nvl(:new.cd_unid_med_dose_comp6,'XPTO') <> nvl(:old.cd_unid_med_dose_comp6,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_unid_med_dose_comp6(' || nvl(:old.cd_unid_med_dose_comp6,'<NULL>') || '/' || nvl(:new.cd_unid_med_dose_comp6,'<NULL>')||'); ',1,2000);
	end if;
	
	if	(nvl(:new.cd_unid_med_dose_comp7,'XPTO') <> nvl(:old.cd_unid_med_dose_comp7,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_unid_med_dose_comp7(' || nvl(:old.cd_unid_med_dose_comp7,'<NULL>') || '/' || nvl(:new.cd_unid_med_dose_comp7,'<NULL>')||'); ',1,2000);
	end if;
	
	if	(:new.dt_inicio is not null) and (:old.dt_inicio is not null) and (ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_inicio) > ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:old.dt_inicio)) then

		select max(dt_inicio_medic),
			max(hr_prim_horario),
			max(ds_horarios),
			max(ds_horarios_medico)
		into dt_inicio_medic_w,
			hr_prim_horario_w,
			ds_horarios_w,
			ds_horarios_medico_w
		from prescr_material
		where nr_seq_mat_cpoe = :new.nr_sequencia;

		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || 
					' PRESCR_MATERIAL ' ||
					' dt_inicio_medic_w(' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_inicio_medic_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)|| ');'||
					' hr_prim_horario_w(' || hr_prim_horario_w ||');'||
					' ds_horarios_w(' || ds_horarios_w ||');'||
					' ds_horarios_medico_w: ' || ds_horarios_medico_w,1,2000);
	end if;

	if	(ds_log_cpoe_w is not null) then
		
		if (nvl(:old.nr_sequencia, 0) > 0) then
			ds_log_cpoe_w := substr('Alteracoes(old/new)= ' || ds_log_cpoe_w,1,2000);
		else
			ds_log_cpoe_w := substr('Criacao(old/new)= ' || ds_log_cpoe_w,1,2000);
		end if;
		
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		ds_log_cpoe_w := substr(ds_log_cpoe_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')',1,2000);
		
		insert into log_cpoe(nr_sequencia, nr_atendimento, dt_atualizacao, nm_usuario, nr_seq_material, ds_log, ds_stack) values (log_cpoe_seq.nextval, :new.nr_atendimento, sysdate, :new.nm_usuario, :new.nr_sequencia, ds_log_cpoe_w, ds_stack_w);
	end if;

	exception
	when others then
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);

		insert into log_cpoe(nr_sequencia, 
							nr_atendimento, 
							dt_atualizacao, 
							nm_usuario, 
							nr_seq_material, 
							ds_log, 
							ds_stack) 
		values (			log_cpoe_seq.nextval, 
							:new.nr_atendimento, 
							sysdate, 
							:new.nm_usuario, 
							:new.nr_sequencia, 
							'EXCEPTION CPOE_MAT_INSERT_UPDATE_AFTER', 
							ds_stack_w);
	end;
	
	begin
	if (:new.dt_liberacao is not null and ((:old.dt_liberacao_enf is null and  :new.dt_liberacao_enf is not null) or (:old.dt_liberacao_farm is null and :new.dt_liberacao_farm is not null))) then
		cpoe_atualizar_inf_adic(:new.nr_atendimento, :new.nr_sequencia, 'M', :new.dt_liberacao_enf, :new.dt_liberacao_farm, null, null, null, null, null, :new.nr_seq_cpoe_order_unit);
	end if;
	exception
	when others then
		gravar_log_cpoe('CPOE_MAT_INSERT_UPDATE_AFTER - CPOE_ATUALIZAR_INF_ADIC - Erro: ' || substr(sqlerrm(sqlcode),1,1500) || ' :new.nr_sequencia '|| :new.nr_sequencia, :new.nr_atendimento);
	end;

  obter_param_usuario(9041, 10, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_use_integration_w);
	
  if (ie_use_integration_w = 'S')
    and ((:OLD.DT_LIBERACAO IS NULL AND :NEW.DT_LIBERACAO IS NOT NULL)
      or (:OLD.DT_LIB_SUSPENSAO IS NULL AND :NEW.DT_LIB_SUSPENSAO IS NOT NULL )) then
          
    if (:new.ie_controle_tempo = 'S') then
      select obter_cpoe_regra_ator('IP')
      into ie_order_integr_type_w
      from dual;			
    else
      select obter_cpoe_regra_ator('M')
      into ie_order_integr_type_w
      from dual;
    end if;			
    
    if (ie_order_integr_type_w = 'OI') then
    
      cpoe_mat_json_pck.getCpoeIntegracaoMat(:new.nr_sequencia, nr_entity_identifier_w);
    
      if (:OLD.DT_LIBERACAO IS NULL AND :NEW.DT_LIBERACAO IS NOT NULL) then
        
        call_bifrost_content('prescription.medicine.order.request','cpoe_mat_json_pck.get_message_clob(' || :new.nr_sequencia || ', ''NW'', ' || nr_entity_identifier_w || ')', :new.nm_usuario);
        
      elsif (:OLD.DT_LIB_SUSPENSAO IS NULL AND :NEW.DT_LIB_SUSPENSAO IS NOT NULL ) then
      
        call_bifrost_content('prescription.medicine.order.request','cpoe_mat_json_pck.get_message_clob(' || :new.nr_sequencia || ', ''CA'', ' || nr_entity_identifier_w || ')', :new.nm_usuario);
        
      end if;
    
    end if;						
    
  end if;

end if;
end;
/
