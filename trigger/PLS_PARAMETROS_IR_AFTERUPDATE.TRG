create or replace trigger pls_parametros_ir_afterupdate
after update on pls_parametros_ir
for each row
declare

begin

if	(wheb_usuario_pck.get_cd_estabelecimento is not null) then -- N�o tem o campo CD_ESTABELECIMENTO na tabela pls_parametros_ir, deve ser criado o mesmo para retirar est� restri��o
	if	(:new.ie_baixa_negociacao <> :old.ie_baixa_negociacao) then
		pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_IR', 'IE_BAIXA_NEGOCIACAO', :old.ie_baixa_negociacao, 
						:new.ie_baixa_negociacao, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	end if;

	if	(:new.ie_considerar_baixas <> :old.ie_considerar_baixas) then
		pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_IR', 'IE_CONSIDERAR_BAIXAS', :old.ie_considerar_baixas, 
						:new.ie_considerar_baixas, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	end if;

	if	(:new.ie_considerar_jurmul_neg <> :old.ie_considerar_jurmul_neg) then
		pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_IR', 'IE_CONSIDERAR_JURMUL_NEG', :old.ie_considerar_jurmul_neg, 
						:new.ie_considerar_jurmul_neg, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	end if;
end if;

end;
/
