create or replace trigger GESTAO_VAGA_ATUAL
before insert or update on GESTAO_VAGA
for each row
declare

begin

if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') then
  consistir_classif_med_retag(:new.nr_seq_classif_medico, :new.cd_medico, :new.nm_usuario,:new.cd_estabelecimento);

  if (:new.nr_seq_agenda <> :old.nr_seq_agenda and :new.nr_seq_agenda is not null) or
     (:new.cd_tipo_agenda <> :old.cd_tipo_agenda and :new.cd_tipo_agenda is not null) then
	gravar_log_tasy(8415,'nr_seq_agenda = ' || :new.nr_seq_agenda || ' cd_tipo_agenda= ' || :new.cd_tipo_agenda || ' callstack= ' || dbms_utility.format_call_stack, :new.nm_usuario);
  end if;
end if;
end;
/
