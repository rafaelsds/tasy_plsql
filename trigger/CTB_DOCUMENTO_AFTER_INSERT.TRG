create or replace trigger ctb_documento_after_insert 
after insert on ctb_documento
for each row

declare

   ie_job_w                varchar2(25);
   ds_action_w             varchar2(255);
   pragma autonomous_transaction;

begin

   ie_job_w := 'DOC_PENDENTE_' || :new.nr_sequencia;
   
   ds_action_w := 'BEGIN CTB_CONTAB_ONL_LOTE_FIN_PCK.CTB_CONTABILIZA_DOCUMENTO(null,''' 
                   || :new.nm_usuario || ''',' || :new.nr_sequencia ||'); END;';

   --Criar job para processo da contabilizacao on line
   dbms_scheduler.create_job(job_name   => ie_job_w,
                             job_type   => 'PLSQL_BLOCK',
                             job_action => ds_action_w,
                             start_date => sysdate + 5 / 86400,
                             auto_drop  => TRUE,
                             enabled    => TRUE,
                             comments   => 'Contabilidade On Line de ctb_documento');

end;
/