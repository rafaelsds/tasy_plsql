CREATE OR REPLACE TRIGGER titulo_a_pagar_ATUAL
BEFORE INSERT OR UPDATE ON TITULO_PAGAR
FOR EACH ROW

DECLARE

cd_historico_w		number(10);
qt_titulo_w		number(10);
cd_sistema_ant_w	varchar2(20);
nr_sequencia_w		number(10);
nr_seq_hist_w		number(10);
nr_seq_titulo_w		number(10);
ie_situacao_w		varchar2(1);
cd_empresa_w		varchar2(5);

begin

select	count(*)
into	qt_titulo_w
from	producao.titulo_a_pagar@totvs
where	NRDOCUMENTO	= to_char(:new.nr_titulo);

if	(:new.cd_pessoa_fisica is null) then
	select	max(cd_sistema_ant)
	into	cd_sistema_ant_w
	from	pessoa_juridica_estab
	where	cd_cgc = :new.cd_cgc;
else
	select	max(cd_sistema_ant)
	into	cd_sistema_ant_w
	from	pessoa_Fisica
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica;
end if;

IF	(:new.ie_situacao = 'A') then
	ie_situacao_w := '0';
elsif (:new.ie_situacao = 'L') then
	ie_situacao_w := '1';
else
	ie_situacao_w := '0';
end if;


select 	max(e.cdempresa)
into	cd_empresa_w
from 	producao.empresa@totvs e 
where 	e.nrcgc = obter_cgc_estabelecimento(:new.cd_estabelecimento);

select	producao.SEQ_NRTITULO_PAGAR.nextval@totvs
into	nr_sequencia_w
from 	dual;

select	producao.SEQ_NRMOVHIST_PAGAR.nextval@totvs
into	nr_seq_hist_w
from 	dual;

if	(cd_sistema_ant_w is not null) then

	if 	((inserting) or (updating and qt_titulo_w = 0)) then
		:new.dt_integracao_externa := sysdate;
		insert 	into 	producao.titulo_a_pagar@totvs	(
						VLBRUTO,
						DTTITULO,
						DTVENCIMENTO,
						DTDIGITACAO,
						NRNOSSONUMERO,
						NRDOCUMENTO,
						NRREGISTRO_TITULO,
						TXHISTORICO,
						VLTITULO,
						VLJUROS,
						VLMULTA,
						CDFORNECEDOR,
						CDSITUACAO,
						CDEMPRESA,
						cdfuncionario)
				values	(
						:new.VL_TITULO,
						:new.DT_EMISSAO,
						:new.DT_VENCIMENTO_ATUAL,
						:new.DT_EMISSAO,
						:new.NR_NOSSO_NUMERO,
						:new.NR_DOCUMENTO,
						nr_sequencia_w,
						:new.DS_OBSERVACAO_TITULO,
						:new.VL_TITULO,
						:new.VL_SALDO_JUROS,
						:new.VL_SALDO_MULTA,
						CD_SISTEMA_ANT_W,
						ie_situacao_w,
						cd_empresa_w,
						'tasy');

		insert 	into 	producao.HISTORICO_DO_TITULO_PAGAR@totvs	(
						VLTITULO,
						CDSITUACAO,
						DTTITULO,
						DTVENCIMENTO,
						NRNOSSONUMERO,
						NRDOCUMENTO,
						NRREGISTRO_TITULO,
						TXHISTORICO,
						VLJUROS,
						VLMULTA,
						CDFUNCIONARIO,
						NRMOVIMENTO,
						CDEMPRESA,
						CDFORNECEDOR)
				values	(
						:new.VL_TITULO,
						ie_situacao_w,
						:new.DT_EMISSAO,
						:new.DT_VENCIMENTO_ATUAL,
						:new.NR_NOSSO_NUMERO,
						:new.NR_DOCUMENTO,
						nr_sequencia_w,
						:new.DS_OBSERVACAO_TITULO,
						:new.VL_SALDO_JUROS,
						:new.VL_SALDO_MULTA,
						'tasy',
						nr_seq_hist_w,
						cd_empresa_w,
						CD_SISTEMA_ANT_W);
							
	end if;


	if 	(updating) then

	
		select	max(NRREGISTRO_TITULO)
		into	nr_seq_titulo_w
		from	producao.titulo_a_pagar@totvs
		where	nrdocumento = :new.nr_titulo;
	
		update 	producao.titulo_a_pagar@totvs
		set	VLBRUTO					= :new.VL_TITULO,
			CDSITUACAO				= ie_situacao_w,
			DTTITULO				= :new.DT_EMISSAO,
			DTVENCIMENTO			= :new.DT_VENCIMENTO_ATUAL,
			DTDIGITACAO				= :new.DT_EMISSAO,
			NRNOSSONUMERO			= :new.NR_NOSSO_NUMERO,
			NRDOCUMENTO				= :new.NR_DOCUMENTO,
			TXHISTORICO				= :new.DS_OBSERVACAO_TITULO,
			VLTITULO				= :new.VL_TITULO,
			VLJUROS					= :new.VL_SALDO_JUROS,
			VLMULTA					= :new.VL_SALDO_MULTA,
			cdfornecedor			= CD_SISTEMA_ANT_W,
			CDEMPRESA				= cd_empresa_w,
			cdfuncionario				= 'tasy'
		where	NRREGISTRO_TITULO	= nr_seq_titulo_w;

		insert 	into 	producao.HISTORICO_DO_TITULO_PAGAR@totvs	(
						VLTITULO,
						CDSITUACAO,
						DTTITULO,
						DTVENCIMENTO,
						NRNOSSONUMERO,
						NRDOCUMENTO,
						NRREGISTRO_TITULO,
						TXHISTORICO,
						VLJUROS,
						VLMULTA,
						CDFUNCIONARIO,
						NRMOVIMENTO,
						CDEMPRESA,
						CDFORNECEDOR)
				values	(
						:new.VL_TITULO,
						ie_situacao_w,
						:new.DT_EMISSAO,
						:new.DT_VENCIMENTO_ATUAL,
						:new.NR_NOSSO_NUMERO,
						:new.NR_DOCUMENTO,
						nr_seq_titulo_w,
						:new.DS_OBSERVACAO_TITULO,
						:new.VL_SALDO_JUROS,
						:new.VL_SALDO_MULTA,
						'tasy',
						nr_seq_hist_w,
						cd_empresa_w,
						CD_SISTEMA_ANT_W);
			
		
	end if;
else

	insert 	into	log_tasy
					(cd_log,
					nm_usuario,
					dt_atualizacao,
					ds_log)
			values	(55801,
					:new.nm_usuario,
					sysdate,
					'N�o foi poss�vel inserir o t�tulo '|| :new.nr_titulo || chr(13) || chr(10) ||
					'A pessoa n�o possui o c�digo do sistema Totvs informado.');
end if;

end;
/
