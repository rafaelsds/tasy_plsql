create or replace trigger pls_membro_grupo_aud_insert
before insert or update on pls_membro_grupo_aud
for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

declare
nm_auditor_w			varchar2(255);
count_w				number(10);
nr_seq_grupo_w			number(10);

pragma autonomous_transaction;
begin
nm_auditor_w	:= :new.nm_usuario_exec;
nr_seq_grupo_w	:= :new.nr_seq_grupo;

if(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if(:new.nr_contrato is not null) then
		:new.nr_seq_contrato := substr(pls_obter_seq_contrato(:new.nr_contrato),1,255);
	else 
		:new.nr_seq_contrato := null;
	end if;

	if	(inserting and :new.ie_situacao = 'A') then
		select	count(1)
		into	count_w
		from	pls_membro_grupo_aud
		where	nm_usuario_exec	= nm_auditor_w
		and	nr_seq_grupo	= nr_seq_grupo_w
		and	ie_situacao	= 'A';

		if	(count_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(218290);
		end if;
	end if;
end if;

end;
/