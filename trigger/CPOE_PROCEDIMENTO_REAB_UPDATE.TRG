create or replace trigger cpoe_procedimento_reab_update 
after update on cpoe_procedimento 
for each row 
 
declare 

nr_seq_w number(10); 
ie_gerar_reabilitacao_w	varchar2(10); 
cd_estabelecimento_w	number(4);
nr_seq_paciente_reab_w 	rp_paciente_reabilitacao.nr_sequencia%type;
nr_seq_rp_tratamento_w 	rp_tratamento.nr_sequencia%type;
ie_tipo_w   			varchar2(10);
cd_medico_setor_w       rp_tratamento.cd_medico_setor%type;
qt_reg_w				number(1);

begin 
	if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
		goto Final;
	end if;
	
	select max(wheb_usuario_pck.get_cd_estabelecimento)
	into cd_estabelecimento_w
	from dual;
	
	obter_param_usuario(2314, 40,  obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_gerar_reabilitacao_w);

	if (ie_gerar_reabilitacao_w = 'S' and :old.dt_liberacao is null and :new.dt_liberacao is not null) then
		select max(dm.ds_depto)
        into cd_medico_setor_w
        from depto_medico dm,
            medico_especialidade mc,
            medico m,
            depto_medico_espec dme
        where mc.cd_pessoa_fisica = m.cd_pessoa_fisica
            and mc.cd_especialidade = dme.cd_especialidade
            and dme.nr_seq_depto = dm.nr_sequencia
            and mc.cd_pessoa_fisica = :new.cd_solicitante_medico
            and mc.nr_seq_prioridade = ( select min(mde.nr_seq_prioridade)
                        from depto_medico dpm,
                            medico_especialidade mde,
                            medico medc,
                            depto_medico_espec dpme
                        where mde.cd_pessoa_fisica = medc.cd_pessoa_fisica
                            and mde.cd_especialidade = dpme.cd_especialidade
                            and dpme.nr_seq_depto = dpm.nr_sequencia
                            and medc.cd_pessoa_fisica = m.cd_pessoa_fisica);
        
        select max(upper(ie_tipo))
		into ie_tipo_w
		from proc_interno
		where nr_sequencia = :new.nr_seq_proc_interno;
	
		if (ie_tipo_w = 'REAB') then
			select max(nr_sequencia)
			into nr_seq_paciente_reab_w
			from rp_paciente_reabilitacao
			where cd_pessoa_fisica = :new.cd_pessoa_fisica
				  and ie_situacao = 'A';

			if (nr_seq_paciente_reab_w is null) then
				select rp_paciente_reabilitacao_seq.nextval
				into nr_seq_paciente_reab_w
				from dual;
				
				insert into rp_paciente_reabilitacao (
					nr_sequencia,
					cd_pessoa_fisica,
					dt_atualizacao, 
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					cd_estabelecimento,
					ie_situacao,
					cd_medico_resp)
				values (
					nr_seq_paciente_reab_w,
					:new.cd_pessoa_fisica,
					sysdate,
					sysdate,
					:new.nm_usuario,
					:new.nm_usuario,
					cd_estabelecimento_w,
					'A',
					:new.cd_medico);
			end if;
			
			select rp_tratamento_seq.nextval
			into nr_seq_rp_tratamento_w
			from dual;
            
			insert into rp_tratamento (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_pac_reav,
				nr_seq_tipo_tratamento,
				dt_inicio_tratamento,
				dt_fim_tratamento,
				dt_prevista_alta,
				ie_calculation_date,
				nr_seq_calc_classif,
				ds_diagnosis,
				dt_diagnostico,
				nr_seq_dis_classif,
				ds_complicacao,
                dt_periodo_inicial_prev,
                cd_solicitante_medico,
                cd_medico_setor,
				rp_disability_status,
				rp_instruct_prescript,
				rp_comments,
                nr_seq_cpoe_procedimento)
			values (
				nr_seq_rp_tratamento_w,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				nr_seq_paciente_reab_w,
				:new.nr_seq_tipo_tratamento,
				:new.dt_inicio,
				:new.dt_fim,
				:new.dt_prevista_alta,
				:new.ie_calculation_date,
				:new.nr_seq_calc_classif,
				:new.ds_diagnosis,
				:new.dt_diagnostico,
				:new.nr_seq_dis_classif,
				:new.ds_complicacao,
                :new.dt_periodo_inicial_prev,
                :new.cd_solicitante_medico,
                cd_medico_setor_w,
				:new.disablity_status,
				:new.priscription_instruction,
				:new.rp_comments,
                :new.nr_sequencia);
		end if;
	end if;

	<<Final>>
	qt_reg_w := 0;
end;
/
