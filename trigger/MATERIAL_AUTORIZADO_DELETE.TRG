create or replace trigger material_autorizado_delete
After delete on material_autorizado
for each row

declare
ie_gerar_historico_w	varchar2(15) := 'N';
begin

obter_param_usuario(3004,139, obter_perfil_ativo, :old.nm_usuario, wheb_usuario_pck.Get_cd_estabelecimento, ie_gerar_historico_w);

if	(nvl('N','N') = 'S') then

	insert into AUTORIZACAO_CONVENIO_HIST
		(NR_SEQUENCIA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		NR_ATENDIMENTO,
		DS_HISTORICO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		NR_SEQUENCIA_AUTOR)
	values	(
		autorizacao_convenio_hist_seq.nextval,
		sysdate,
		:old.nm_usuario,
		:old.nr_atendimento,
		substr(WHEB_MENSAGEM_PCK.get_texto(311724) || chr(13) || chr(10) || --Material exclu�do da autoriza��o 
		:old.cd_material || ' - ' || substr(obter_desc_material(:old.cd_material),1,100) || chr(13) || chr(10) || 
		WHEB_MENSAGEM_PCK.get_texto(311725) || :old.qt_solicitada || chr(13) || chr(10) || --Qtde solicitada: 
		WHEB_MENSAGEM_PCK.get_texto(311726) || :old.qt_autorizada,1,4000), ---Qtde autorizada: 
		sysdate,
		:old.nm_usuario,
		:old.nr_sequencia_autor);
end if;

end;
/
