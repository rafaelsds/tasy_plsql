create or replace trigger pessoa_juridica_compl_afterDel
after delete on pessoa_juridica_compl
for each row

declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

begin

select  obter_tipo_pessoa_juridica(:old.cd_cgc) 
into	cd_tipo_pessoa_w
from    dual;

reg_integracao_p.ie_operacao		:=	'I';
reg_integracao_p.cd_tipo_pessoa		:=	cd_tipo_pessoa_w;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :old.cd_cgc,:old.nm_usuario, reg_integracao_p);

end;
/
