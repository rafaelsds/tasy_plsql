create or replace trigger orcamento_custo_atual
before insert or update on ORCAMENTO_CUSTO
for each row

declare

cd_empresa_w	number(10);
nr_seq_ng_w	number(10);

begin

if	(:new.nr_seq_ng is null) then
	begin
	cd_empresa_w	:= obter_empresa_estab(:new.cd_estabelecimento);

	select	max(nr_sequencia)
	into	nr_seq_ng_w
	from	natureza_gasto
	where	cd_empresa		= cd_empresa_w
	and	nvl(cd_estabelecimento, :new.cd_estabelecimento)	= :new.cd_estabelecimento
	and	cd_natureza_gasto	= :new.cd_natureza_gasto;
	
	if	(nr_seq_ng_w is not null) then
		:new.nr_seq_ng	:= nr_seq_ng_w;
	end if;
	end;
elsif	(:new.cd_natureza_gasto is null) then
	select	max(cd_natureza_gasto)
	into	:new.cd_natureza_gasto
	from	natureza_gasto
	where	nr_sequencia	= :new.nr_seq_ng;
end if;
end orcamento_custo_atual;
/