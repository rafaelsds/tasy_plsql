create or replace trigger ISCV_AGENDA_PAC_PROC_AFTINSUP
after update or insert on AGENDA_PACIENTE_PROC
for each row

declare
cd_pessoa_fisica_w			agenda_paciente.cd_pessoa_fisica%type;
cd_setor_atendimento_w		agenda_paciente.cd_setor_atendimento%type;
ds_sep_bv_w					varchar2(100)	:= obter_separador_bv;
	
	/* Verifica se existe alguma integra��o cadastrada para o dom�nio 17, para certificar que h� o que ser procurado, para a� ent�o executar todas as verifica��es. */
	function permiteIntegrarISCV
	return boolean is
	qt_resultado_w	number;

	begin	
	select	count(*) qt_resultado
	into	qt_resultado_w
	from 	regra_proc_interno_integra
	where	ie_tipo_integracao = 17; --Dominio criado para a integra��o

	return qt_resultado_w > 0;
	end;
	
	function retornaAcaoHistorico(nr_sequencia_p number)
	return varchar is
		ds_retorno_w	agenda_historico_acao.IE_ACAO%type;
	begin
		select	nvl(max(IE_ACAO), ' ')
		into	ds_retorno_w
		from	agenda_historico_acao
		where	nr_Sequencia =  nr_sequencia_p;
	return	ds_retorno_w;
	end;
	
	function transferidoAgenda(nr_sequencia_p number)
	return boolean is	
	ds_retorno_w	agenda_paciente.ie_transferido%type;
	begin
	
	select	max(ie_transferido),
			max(cd_pessoa_fisica)
	into	ds_retorno_w,
			cd_pessoa_fisica_w
	from	agenda_paciente
	where	nr_sequencia = nr_sequencia_p;
		
	return ds_retorno_w = 'S';
	end;
	

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	permiteIntegrarISCV then
		if	((:old.nr_seq_proc_interno is null) and (:new.nr_seq_proc_interno is not null)) 
		and	transferidoAgenda(:new.nr_sequencia) 
		and	(retornaAcaoHistorico(:new.nr_sequencia) = 'T')
		and	(Obter_Se_Integr_Proc_Interno(:new.nr_seq_proc_interno, 17,null,:new.ie_lado,wheb_usuario_pck.get_cd_estabelecimento) = 'S')
		then
			gravar_agend_integracao(742, 'nr_sequencia=' || :new.nr_sequencia || ds_sep_bv_w || 'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w, cd_setor_atendimento_w);
		end if;
	end if;
end if;	

end;
/
