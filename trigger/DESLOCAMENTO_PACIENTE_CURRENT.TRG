create or replace 
trigger DESLOCAMENTO_PACIENTE_current
before insert or update on DESLOCAMENTO_PACIENTE
for each row


begin

if	((:new.DT_RETORNO is not null) and (:new.DT_SAIDA is not null)) and
	(:new.DT_RETORNO < :new.DT_SAIDA) then
	wheb_mensagem_pck.exibir_mensagem_abort(100328848);
end if;

end;
/