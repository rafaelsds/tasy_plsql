CREATE OR REPLACE TRIGGER TASY_MS_TITULO_REC_LIQ_UP
AFTER UPDATE ON TITULO_RECEBER_LIQ
FOR EACH ROW

declare
PRAGMA AUTONOMOUS_TRANSACTION;

nr_interno_conta_w		number(15);
cd_estabelecimento_w	number(5);
ds_parametros_w		varchar2(255);

begin


select	max(nr_interno_conta),
	max(cd_estabelecimento)
into	nr_interno_conta_w,
	cd_estabelecimento_w
from	titulo_receber
where	nr_titulo	= :new.nr_titulo;

if	(nr_interno_conta_w is not null) and
	(:new.nr_seq_caixa_rec is not null) and
	(:new.ie_lib_caixa = 'S') then

	ds_parametros_w	:=	'nr_titulo='||:new.nr_titulo||';'||
			   	'cd_tipo_recebimento='||:new.cd_tipo_recebimento||';'||
				'nr_seq_caixa_rec='||:new.nr_seq_caixa_rec||';'||
				'vl_recebido='||:new.vl_recebido||';'||
				'dt_recebimento='||:new.dt_recebimento||';';

	tasy_ms_grava_integracao(cd_estabelecimento_w, '37', 'P', :new.nr_titulo, 'A', 150, ds_parametros_w, :new.nm_usuario);

end if;

commit;

end;
/