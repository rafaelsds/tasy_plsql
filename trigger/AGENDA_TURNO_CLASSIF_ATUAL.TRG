CREATE OR REPLACE TRIGGER Agenda_Turno_Classif_Atual
before insert or update ON Agenda_Turno_Classif
FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
cd_agenda_w	number(10);
BEGIN
begin

select	max(cd_agenda)
into	cd_agenda_w
from	agenda_turno
where	nr_sequencia	= :new.nr_seq_turno;

if	(cd_agenda_w is not null) then
	delete	from agenda_controle_horario
	where	dt_agenda >= trunc(sysdate)
	and	cd_agenda	 = cd_agenda_w;
end if;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/
