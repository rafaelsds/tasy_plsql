create or replace trigger disp_transacao_insert
after insert on dispensario_transacao for each ROW

declare
nr_sequencia_w		    number(10, 0);
nr_seq_atepacu_w		number(10, 0);
dt_entrada_unidade_w	date;
cd_setor_atendimento_w	number(10, 0);
cd_unidade_medida_w		varchar2(30);
cd_convenio_w			number(05, 0);
cd_categoria_w			varchar2(10);
nr_doc_convenio_w		varchar2(20);
ie_tipo_guia_w			varchar2(2);
cd_senha_w				varchar2(20);
nm_usuario_w			varchar2(15);
nr_seq_lote_fornec_w	number(10, 0);
qt_material_w        	number(15, 4);
qt_material_kit_w    	number(15, 4);
cd_estabelecimento_w 	number(10, 0);
cd_material_barra_w 	MATERIAL_COD_BARRA.CD_BARRA_MATERIAL%TYPE;
cd_kit_material_w 		number(10, 0);
cd_material_w           number(6);
cd_unidade_medida_consumo_w varchar2(30);
ie_via_aplicacao_w      varchar2(10);
ie_estoque_disp_w       varchar2(01);
ie_entra_conta_w        varchar2(01);
ds_erro_w		        varchar2(1000);
NR_SEQUENCIA_ww      		prescr_mat_hor.nr_sequencia%type;
NR_PRESCRICAO_ww     		prescr_mat_hor.NR_PRESCRICAO%type;
NR_SEQ_MATERIAL_ww   		prescr_mat_hor.NR_SEQ_MATERIAL%type;
IE_AGRUPADOR_ww      		prescr_mat_hor.IE_AGRUPADOR%type;
IE_SITUACAO_ww       		prescr_mat_hor.IE_SITUACAO%type;
CD_UNIDADE_MEDIDA_ww 		prescr_mat_hor.CD_UNIDADE_MEDIDA%type;
CD_UNIDADE_MEDIDA_DOSE_ww   prescr_mat_hor.CD_UNIDADE_MEDIDA_DOSE%type;
DT_SUSPENSAO_ww             prescr_mat_hor.DT_SUSPENSAO%type;
DT_DISP_FARMACIA_ww         prescr_mat_hor.DT_DISP_FARMACIA%type;
NR_SEQ_LOTE_ww              prescr_mat_hor.NR_SEQ_LOTE%type;
ie_status_ww                AP_LOTE.IE_STATUS_LOTE%TYPE;
ie_pas_dis_ww               NUMBER (20, 0);
CD_MATERIAL_ww              AP_LOTE_ITEM.CD_MATERIAL%TYPE;
cd_unid_medida_ww           AP_LOTE_ITEM.CD_UNIDADE_MEDIDA%TYPE;
cd_material_baixa_ww        AP_LOTE_ITEM.CD_MATERIAL%TYPE;
qt_atendido_ww              AP_LOTE_ITEM.QT_DISPENSAR%TYPE;
cd_unidade_med_ww           AP_LOTE_ITEM.CD_UNIDADE_MEDIDA%TYPE;
nr_seq_mat_hor_ww           AP_LOTE_ITEM.NR_SEQ_MAT_HOR%TYPE;
nr_seqq_material_ww         prescr_mat_hor.NR_SEQ_MATERIAL%TYPE;
nr_sq_mat_hor_ww            AP_LOTE_ITEM.NR_SEQ_MAT_HOR%TYPE;
nr_sq_mat_ww                prescr_mat_hor.NR_SEQ_MATERIAL%TYPE;
nr_seq_w                    ATEND_PACIENTE_UNIDADE.NR_SEQUENCIA%TYPE;
CD_TIPO_ACOMODACAO_W        UNIDADE_ATENDIMENTO.CD_TIPO_ACOMODACAO%TYPE;
CD_UNIDADE_BASICA_W         UNIDADE_ATENDIMENTO.CD_UNIDADE_BASICA%TYPE;
CD_UNIDADE_COMPL_W          UNIDADE_ATENDIMENTO.CD_UNIDADE_COMPL%TYPE;
CD_SETOR_ATENDIMENTO_WW     UNIDADE_ATENDIMENTO.CD_SETOR_ATENDIMENTO%TYPE;

Cursor c001 is

	select	a.cd_material,
			a.qt_material,
			b.cd_unidade_medida_consumo,
			b.ie_via_aplicacao,
			a.ie_entra_conta
        from 	material b, componente_kit a
	where 	(a.cd_material = b.cd_material)
        and 	a.cd_kit_material = cd_kit_material_w
        and 	nvl(a.cd_convenio, nvl(cd_convenio_w, 0)) = nvl(cd_convenio_w, 0)
        and 	a.ie_situacao = 'A'
        and 	b.ie_situacao = 'A';

CURSOR c002 IS

		SELECT nvl(max(y.NR_SEQUENCIA),0),
			   max(y.NR_PRESCRICAO),
			   max(y.NR_SEQ_MATERIAL),
			   max(y.IE_AGRUPADOR),
			   max(y.IE_SITUACAO),
			   max(y.CD_UNIDADE_MEDIDA),
			   max(y.CD_UNIDADE_MEDIDA_DOSE),
			   max(y.DT_SUSPENSAO),
			   max(y.DT_DISP_FARMACIA),
			   max(y.NR_SEQ_LOTE)
			FROM DISPENSARIO_MAT_HOR y
	    WHERE y.NR_SEQ_MAT_HOR   = nvl(:NEW.NR_SEQ_MAT_HOR,0)
			AND y.NR_ATENDIMENTO = nvl(:NEW.NR_ATENDIMENTO_PACIENTE,0)
			AND y.CD_MATERIAL    = nvl(:NEW.CD_MATERIAL,0)
		   	AND y.IE_AGRUPADOR   IN (1,4)
			AND y.NR_SEQ_LOTE IS NOT NULL
			GROUP BY y.NR_SEQUENCIA,
			   y.NR_PRESCRICAO,
			   y.NR_SEQ_MATERIAL,
			   y.IE_AGRUPADOR,
			   y.IE_SITUACAO,
			   y.CD_UNIDADE_MEDIDA,
			   y.CD_UNIDADE_MEDIDA_DOSE,
			   y.DT_SUSPENSAO,
			   y.DT_DISP_FARMACIA,
			   y.NR_SEQ_LOTE;

CURSOR c003 IS

		 SELECT nvl(b.CD_MATERIAL,0),
	   			nvl(b.cd_unidade_medida,0),
	   			nvl(b.nr_seq_mat_hor,0),
	   			(SELECT nvl(c.nr_seq_material,0) FROM PRESCR_MAT_HOR c
	   			 WHERE c.NR_SEQUENCIA  = nvl(b.nr_seq_mat_hor,0)) sequencia

		 FROM PRESCR_MAT_HOR a,
     		  AP_LOTE_ITEM b
		 WHERE a.NR_ATENDIMENTO = nvl(:NEW.NR_ATENDIMENTO_PACIENTE,0)
		 AND a.NR_SEQUENCIA     = nvl(:NEW.NR_SEQ_MAT_HOR,0)
		 AND a.NR_SEQ_LOTE      = b.NR_SEQ_LOTE
		 ORDER BY a.CD_MATERIAL;

BEGIN

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	null;
end if;

grava_log_tasy(4565,'disp_transacao_insert :new.cd_material=' || :new.cd_material || ' :new.nr_sequencia=' || :new.nr_sequencia || 
' :new.cd_estabelecimento=' || :new.cd_estabelecimento || ' :new.cd_lote_material=' || :new.cd_lote_material || 
' :new.cd_operacao=' || :new.cd_operacao || ' :new.cd_local_estoque=' || :new.cd_local_estoque || 
' :new.qt_material=' || :new.qt_material || ' :new.cd_usuario=' || :new.cd_usuario ||
' :new.dt_transacao=' || to_char(:new.dt_transacao,'dd/mm/yy hh24:mi:ss') || 
' :new.ie_lido=' || :new.ie_lido || ' :new.nr_sequencia_dankia=' || :new.nr_sequencia_dankia ||
' :new.nr_seq_mat_hor=' || :new.nr_seq_mat_hor,substr(:new.nr_atendimento_paciente,1,15));

if (:new.cd_operacao = 30)then
	qt_material_w := :new.qt_material * (-1);
else
	qt_material_w := :new.qt_material;
end if;

select	nvl(max(nm_usuario),'0')
into	nm_usuario_w
from 	usuario
where	cd_barras = :new.cd_usuario;

if	( nm_usuario_w = '0' ) then
	raise_application_error(-20011, 'Nao encontrado o usuario com codigo de barras = ' || :new.cd_usuario);
end if;

if (:new.cd_operacao <> 10) THEN

select	nvl(max(nr_sequencia),0)
into 	nr_seq_lote_fornec_w
from 	material_lote_fornec
where 	nr_sequencia = somente_numero(substr(:new.cd_lote_material, 1, length(:new.cd_lote_material) - 1))
OR (CD_BARRA_MATERIAL = :new.cd_lote_material OR DS_BARRAS = :new.cd_lote_material)
AND CD_MATERIAL = :new.cd_material;

if	(nr_seq_lote_fornec_w = 0) then

	select 	nvl(max(cd_barra_material), 0)
	into	cd_material_barra_w
	from 	material_cod_barra
	where 	cd_barra_material = :new.cd_lote_material
	AND CD_MATERIAL = :new.cd_material;

	if	(cd_material_barra_w = '0') then
		raise_application_error(-20011,'Nao existe o lote fornecedor, nem codigo de barras ' || substr(:new.cd_lote_material, 1, 11));
	end if;
end if;

END IF;

select	nvl(max(cd_estabelecimento),0)
into 	cd_estabelecimento_w
from 	estabelecimento
where 	cd_estabelecimento = :new.cd_estabelecimento;

if 	(cd_estabelecimento_w = 0)then
	raise_application_error(-20011, 'Nao existe o estabelecimento ' || :new.cd_estabelecimento);
end if;


if	(:new.cd_operacao <> 10) then

	select	max(obter_atepacu_paciente(:new.nr_atendimento_paciente, 'A'))
	into 	nr_seq_atepacu_w
	from 	dual;

	select	nvl(max(cd_setor_atendimento),0),
		    max(dt_entrada_unidade)

	into
	        cd_setor_atendimento_w,
	        dt_entrada_unidade_w

	from 	atend_paciente_unidade
	where 	nr_seq_interno = nr_seq_atepacu_w;

	if	((nr_seq_atepacu_w = 0) OR (cd_setor_atendimento_w = 0)) then
		raise_application_error(-20011, 'Nao existe movimentacao para este paciente. Verifique o atendimento');
	end if;

    if (:NEW.CD_SETOR_ATEND IS NOT NULL) then

        SELECT    nvl(max(nr_sequencia),0) + 1
             into
                  nr_seq_w
        FROM ATEND_PACIENTE_UNIDADE
        WHERE NR_ATENDIMENTO = :new.nr_atendimento_paciente;

        if	(nr_seq_w = 1) then
			raise_application_error(-20011, 'Nao existe movimentacao para este paciente. Verifique o atendimento');
		end if;


		SELECT 	NVL(MAX(B.CD_TIPO_ACOMODACAO),0),
				MAX(B.CD_UNIDADE_BASICA),
				MAX(B.CD_UNIDADE_COMPL)
  		INTO
  				CD_TIPO_ACOMODACAO_W,
				CD_UNIDADE_BASICA_W,
				CD_UNIDADE_COMPL_W

  		FROM SETOR_ATENDIMENTO A,
       		 UNIDADE_ATENDIMENTO B
		WHERE A.CD_SETOR_ATENDIMENTO = :NEW.CD_SETOR_ATEND
		  AND A.IE_SITUACAO = 'A'
		  AND A.CD_SETOR_ATENDIMENTO = B.CD_SETOR_ATENDIMENTO
		  AND B.IE_SITUACAO = 'A';

	    if	(CD_TIPO_ACOMODACAO_W = 0) then
			raise_application_error(-20011, 'Nao existe setor ou tipo acomodacao. Verifique o cadastro de setor');
		end if;

        SELECT  NVL(max(cd_setor_atendimento),0),
		        max(dt_entrada_unidade),
		        max(NR_SEQ_INTERNO)
	       into
	            cd_setor_atendimento_w,
		        dt_entrada_unidade_w,
		        nr_seq_atepacu_w

        FROM ATEND_PACIENTE_UNIDADE
        WHERE NR_ATENDIMENTO            = :new.nr_atendimento_paciente
        AND trunc(DT_ENTRADA_UNIDADE)   = TRUNC(sysdate)
        and cd_setor_atendimento        = :NEW.CD_SETOR_ATEND;

         if	(cd_setor_atendimento_w = 0) THEN

                SELECT nvl(max(nr_sequencia),0) + 1
             		  into
                  	   	nr_seq_w
	        FROM ATEND_PACIENTE_UNIDADE
	        WHERE NR_ATENDIMENTO = :new.nr_atendimento_paciente;

	  			INSERT INTO ATEND_PACIENTE_UNIDADE (NR_SEQ_INTERNO,NR_ATENDIMENTO, NR_SEQUENCIA, CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL, DT_ENTRADA_UNIDADE, DT_ATUALIZACAO, NM_USUARIO, CD_TIPO_ACOMODACAO, DT_SAIDA_UNIDADE, NR_ATEND_DIA, DS_OBSERVACAO, NM_USUARIO_ORIGINAL, DT_SAIDA_INTERNO, IE_PASSAGEM_SETOR, NR_ACOMPANHANTE, IE_CALCULAR_DIF_DIARIA, NR_SEQ_MOTIVO_TRANSF, DT_ENTRADA_REAL, NM_USUARIO_REAL, IE_RADIACAO, NR_SEQ_MOTIVO_DIF, NR_SEQ_MOT_DIF_DIARIA, CD_UNIDADE_EXTERNA, DT_ALTA_MEDICO_SETOR, CD_MOTIVO_ALTA_SETOR, CD_PROCEDENCIA_SETOR, NR_SEQ_MOTIVO_INT, NR_SEQ_MOTIVO_INT_SUB, NR_SEQ_MOTIVO_PERM, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NR_CIRURGIA, NR_SEQ_PEPO, NR_SEQ_AGRUPAMENTO, QT_TEMPO_PREV, NR_SEQ_UNID_ANT, DT_RETORNO_SAIDA_TEMPORARIA, DT_SAIDA_TEMPORARIA, ID_LEITO_TEMP_CROSS, CD_DEPARTAMENTO)
	  			VALUES (ATEND_PACIENTE_UNIDADE_SEQ.NEXTVAL,:new.nr_atendimento_paciente, nr_seq_w, :NEW.CD_SETOR_ATEND, CD_UNIDADE_BASICA_W,CD_UNIDADE_COMPL_W,SYSDATE, SYSDATE, nm_usuario_w,CD_TIPO_ACOMODACAO_W, SYSDATE, NULL, NULL, nm_usuario_w, SYSDATE, 'S', 0, 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, SYSDATE, nm_usuario_w, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

  		        SELECT  NVL(max(cd_setor_atendimento),0),
		        	 max(dt_entrada_unidade),
		        	 max(NR_SEQ_INTERNO)
	       		 into
	             	cd_setor_atendimento_w,
		         	dt_entrada_unidade_w,
		         	nr_seq_atepacu_w

		        FROM ATEND_PACIENTE_UNIDADE
		        WHERE NR_ATENDIMENTO            = :new.nr_atendimento_paciente
		        AND trunc(DT_ENTRADA_UNIDADE)   = TRUNC(sysdate)
		        and cd_setor_atendimento        = :NEW.CD_SETOR_ATEND;

  		 end if;

    end if;

	select	nvl(max(cd_unidade_medida_consumo),0)
	into 	cd_unidade_medida_w
	from 	material
	where 	cd_material = :new.cd_material;

	select 	nvl(max(cd_convenio), 0),
		    nvl(max(cd_categoria), '0'),
		    max(nr_doc_convenio),
		    max(ie_tipo_guia),
	      	max(cd_senha)
	into 	cd_convenio_w,
		    cd_categoria_w,
			nr_doc_convenio_w,
			ie_tipo_guia_w,
			cd_senha_w
	from 	atend_categoria_convenio a
	where 	a.nr_atendimento = :new.nr_atendimento_paciente
	and 	a.dt_inicio_vigencia = (select max(dt_inicio_vigencia)
		                            from atend_categoria_convenio b
				                    where nr_atendimento = :new.nr_atendimento_paciente);

	if 	(cd_convenio_w = 0) then
		raise_application_error(-20011, 'Nao existe cadastro do convenio para este paciente. Verifique o atendimento');
	end if;

	select	nvl(max(cd_kit_material), 0)
	into 	cd_kit_material_w
	from 	material_estab
	where 	cd_material = :new.cd_material
	and 	cd_estabelecimento = cd_estabelecimento_w;

	if	(cd_kit_material_w = 0) THEN

		select 	nvl(max(cd_kit_material), 0)
		into 	cd_kit_material_w
		from 	material
		where 	cd_material = :new.cd_material;

	end if;

	if 	(cd_kit_material_w = 0) THEN

	ie_pas_dis_ww := 0;

	open c002;
	loop
	fetch c002
	into
	     NR_SEQUENCIA_ww,
		 NR_PRESCRICAO_ww,
		 NR_SEQ_MATERIAL_ww,
		 IE_AGRUPADOR_ww,
		 IE_SITUACAO_ww,
		 CD_UNIDADE_MEDIDA_ww,
		 CD_UNIDADE_MEDIDA_DOSE_ww,
		 DT_SUSPENSAO_ww,
		 DT_DISP_FARMACIA_ww,
		 NR_SEQ_LOTE_ww;

	 exit when c002%notfound;

	 ie_pas_dis_ww := NVL(IE_AGRUPADOR_ww,0);

	 IF ((ie_pas_dis_ww > 0) AND (:new.cd_operacao <> 30)) THEN


	 SELECT IE_STATUS_LOTE
	    INTO ie_status_ww
	  FROM AP_LOTE
      WHERE NR_SEQUENCIA = NR_SEQ_LOTE_ww;


   IF (ie_status_ww NOT IN ('A','S','C','CA','CO','D','E','R')) THEN

       UPDATE AP_LOTE
			SET ie_status_lote = 'A',
    			dt_atend_farmacia = SYSDATE,
    			nm_usuario_atend = nm_usuario_w,
    			cd_local_estoque = :new.cd_local_estoque,
    			dt_inicio_dispensacao = SYSDATE,
    			nm_usuario_ini_disp = nm_usuario_w,
    			ds_maquina_ini_disp = 'Dispensario Integracao'
	   		WHERE NR_SEQUENCIA = NR_SEQ_LOTE_ww;

	open c003;
	loop
	fetch c003
	into
	     CD_MATERIAL_ww,
		 cd_unid_medida_ww,
		 nr_seq_mat_hor_ww,
		 nr_seqq_material_ww;

	 exit when c003%notfound;

	   IF (:new.cd_material = CD_MATERIAL_ww) THEN

	     cd_material_baixa_ww := :new.cd_material;
		 qt_atendido_ww       := qt_material_w;
		 cd_unidade_med_ww    := cd_unidade_medida_w;
		 nr_sq_mat_hor_ww     := :new.nr_seq_mat_hor;
		 nr_sq_mat_ww         := NR_SEQ_MATERIAL_ww;

		 	select 	material_atend_paciente_seq.nextval
				into 	nr_sequencia_w
			from 	dual;

       	insert into material_atend_paciente
			    (nr_sequencia,
			     nr_atendimento,
			     dt_entrada_unidade,
			     cd_material,
			     cd_material_exec,
			     dt_conta,
			     dt_atendimento,
			     qt_material,
			     qt_executada,
			     dt_atualizacao,
			     nm_usuario,
			     cd_unidade_medida,
			     cd_convenio,
			     cd_categoria,
			     cd_acao,
			     cd_local_estoque,
			     cd_setor_atendimento,
			     ie_valor_informado,
			     nm_usuario_original,
			     cd_setor_receita,
			     cd_situacao_glosa,
			     nr_seq_atepacu,
			     ie_auditoria,
			     ie_via_aplicacao,
			     ie_guia_informada,
			     ie_tipo_guia,
			     nr_doc_convenio,
			     cd_senha,
			     nr_seq_cor_exec,
			     nr_seq_lote_fornec,
			     ds_observacao,
			     nr_seq_mat_hor,
                 nr_seq_lote_ap,
                 nr_sequencia_prescricao,
                 nr_prescricao)
			 values
			    (nr_sequencia_w,
			     :new.nr_atendimento_paciente,
			     dt_entrada_unidade_w,
			     cd_material_baixa_ww,
		         cd_material_baixa_ww,
			     sysdate,
			     sysdate,
			     qt_atendido_ww,
			     qt_atendido_ww,
			     sysdate,
			     nm_usuario_w,
			     cd_unidade_med_ww,
			     cd_convenio_w,
			     cd_categoria_w,
			     '1',
			     :new.cd_local_estoque,
			     cd_setor_atendimento_w,
			     'N',
			     nvl(nm_usuario_w, :new.cd_usuario),
			     cd_setor_atendimento_w,
			     0,
			     nr_seq_atepacu_w,
			     'N',
			     null,
			     'N',
			     ie_tipo_guia_w,
			     nr_doc_convenio_w,
			     cd_senha_w,
			     663,
			     decode(nr_seq_lote_fornec_w, 0, null, nr_seq_lote_fornec_w),
			     'Dispensario Eletronico Atendimento do Lote: '||NR_SEQ_LOTE_ww,
			     decode(nr_sq_mat_hor_ww,0,NULL,nr_sq_mat_hor_ww),
			     decode(NR_SEQ_LOTE_ww,0,NULL,NR_SEQ_LOTE_ww),
			     decode(nr_sq_mat_ww,0,NULL,nr_sq_mat_ww),
			     decode(NR_PRESCRICAO_ww,0,NULL,NR_PRESCRICAO_ww));

				atualiza_preco_material(nr_sequencia_w, nvl(nm_usuario_w, :new.cd_usuario));

			  END IF;
			  end loop;
			  close c003;

	END IF;
    END IF;
	end loop;
	close c002;

   IF ((ie_pas_dis_ww = 0) OR (ie_status_ww IN ('A','S','C','CA','CO','D','E','R'))) THEN

   		 	select 	material_atend_paciente_seq.nextval
				into 	nr_sequencia_w
			from 	dual;

		insert into material_atend_paciente
			    (nr_sequencia,
			     nr_atendimento,
			     dt_entrada_unidade,
			     cd_material,
			     cd_material_exec,
			     dt_conta,
			     dt_atendimento,
			     qt_material,
			     qt_executada,
			     dt_atualizacao,
			     nm_usuario,
			     cd_unidade_medida,
			     cd_convenio,
			     cd_categoria,
			     cd_acao,
			     cd_local_estoque,
			     cd_setor_atendimento,
			     ie_valor_informado,
			     nm_usuario_original,
			     cd_setor_receita,
			     cd_situacao_glosa,
			     nr_seq_atepacu,
			     ie_auditoria,
			     ie_via_aplicacao,
			     ie_guia_informada,
			     ie_tipo_guia,
			     nr_doc_convenio,
			     cd_senha,
			     nr_seq_cor_exec,
			     nr_seq_lote_fornec,
			     ds_observacao,
			     nr_seq_mat_hor)
			 values
			    (nr_sequencia_w,
			     :new.nr_atendimento_paciente,
			     dt_entrada_unidade_w,
			     :new.cd_material,
		         :new.cd_material,
			     sysdate,
			     sysdate,
			     qt_material_w,
			     qt_material_w,
			     sysdate,
			     nm_usuario_w,
			     cd_unidade_medida_w,
			     cd_convenio_w,
			     cd_categoria_w,
			     '1',
			     :new.cd_local_estoque,
			     cd_setor_atendimento_w,
			     'N',
			     nvl(nm_usuario_w, :new.cd_usuario),
			     cd_setor_atendimento_w,
			     0,
			     nr_seq_atepacu_w,
			     'N',
			     null,
			     'N',
			     ie_tipo_guia_w,
			     nr_doc_convenio_w,
			     cd_senha_w,
			     663,
			     decode(nr_seq_lote_fornec_w, 0, null, nr_seq_lote_fornec_w),
			     'Dispensario Eletronico Atendimento Avulso',
			     decode(0,0,NULL,0));

		atualiza_preco_material(nr_sequencia_w, nvl(nm_usuario_w, :new.cd_usuario));

    END IF;
	END IF;

	open c001;
	loop
	fetch c001
	into 	cd_material_w,
		qt_material_kit_w,
		cd_unidade_medida_consumo_w,
		ie_via_aplicacao_w,
		ie_entra_conta_w;
	 exit when c001%notfound;

	if 	(ie_entra_conta_w = 'S') then

		select material_atend_paciente_seq.nextval
		into nr_sequencia_w
		from dual;

		if (:new.cd_operacao = 30) then
			qt_material_w := qt_material_kit_w * :new.qt_material * (-1);
		else
			qt_material_w := qt_material_kit_w * :new.qt_material;
		end if;

		insert into material_atend_paciente
			       (nr_sequencia,
				nr_atendimento,
				dt_entrada_unidade,
				cd_material,
				cd_material_exec,
				dt_conta,
				dt_atendimento,
				qt_material,
				qt_executada,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida,
				cd_convenio,
				cd_categoria,
				cd_acao,
				cd_local_estoque,
				cd_setor_atendimento,
				ie_valor_informado,
				nm_usuario_original,
				cd_setor_receita,
				cd_situacao_glosa,
				nr_seq_atepacu,
				ie_auditoria,
				ie_via_aplicacao,
				ie_guia_informada,
				ie_tipo_guia,
				nr_doc_convenio,
				cd_senha,
				nr_seq_cor_exec,
				nr_seq_lote_fornec,
				ds_observacao)
			    values
			       (nr_sequencia_w,
				:new.nr_atendimento_paciente,
				dt_entrada_unidade_w,
				cd_material_w,
				cd_material_w,
				sysdate,
				sysdate,
				qt_material_w,
				qt_material_w,
				sysdate,
				nm_usuario_w,
				cd_unidade_medida_consumo_w,
				cd_convenio_w,
				cd_categoria_w,
				'1',
				:new.cd_local_estoque,
				cd_setor_atendimento_w,
				'N',
				nvl(nm_usuario_w, :new.cd_usuario),
				cd_setor_atendimento_w,
				0,
				nr_seq_atepacu_w,
				'N',
				null,
				'N',
				ie_tipo_guia_w,
				nr_doc_convenio_w,
				cd_senha_w,
				663,
				decode(nr_seq_lote_fornec_w, 0, null, nr_seq_lote_fornec_w),
				'Dispensario Eletronico Atendimento Kit');

				atualiza_preco_material(nr_sequencia_w, nvl(nm_usuario_w, :new.cd_usuario));

	end if;
	end loop;
	close c001;

end if;
exception
when others then

	ds_erro_w := substr(sqlerrm, 1, 1000);

	gerar_disp_transacao_log(:new.nr_sequencia,
				:new.nr_atendimento_paciente,
				:new.cd_estabelecimento,
				:new.cd_material,
				 nvl(:new.cd_lote_material,0),
				:new.cd_operacao,
				:new.cd_local_estoque,
				:new.qt_material,
				:new.cd_usuario,
				:new.dt_transacao,
				:new.dt_atualizacao,
				:new.ie_lido,
				 ds_erro_w||' Setor Atendimento: '||:new.CD_SETOR_ATEND||' Prescricao: '||:NEW.NR_SEQ_MAT_HOR||' Sequencia Dankia: '||:new.NR_SEQUENCIA_DANKIA);

	raise_application_error(-20011, ds_erro_w);

end;
if	(:new.cd_operacao = 10) then

	disp_transacao_inventario (:new.cd_local_estoque,:new.cd_material,:new.qt_material,null, nm_usuario_w, cd_estabelecimento_w, ds_erro_w);

end if;

end;
/