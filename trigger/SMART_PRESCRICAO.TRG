create or replace trigger smart_prescricao
after insert or update on prescr_medica
for each row

declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
qt_medicamento_w        number(10);
qt_recomendacao_w		number(10);
qt_jejum_w				number(10);
qt_glicemia_w			number(10);
qt_procedimento_w 		number(10);
qt_dieta_w				number(10);

begin
	if  (:old.DT_LIBERACAO is null) and
		(:new.DT_LIBERACAO is not null) then
		BEGIN	
		reg_integracao_w.cd_estab_documento := wheb_usuario_pck.get_cd_estabelecimento;		
		SELECT
			(SELECT COUNT(*)
			FROM PRESCR_MATERIAL A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO
				AND A.NR_SEQUENCIA_DIETA IS NULL
				AND A.NR_SEQUENCIA_DILUICAO IS NULL
				AND obter_se_medic_cih(CD_MATERIAL) = 'S'
				AND OBTER_SE_ACM_SN(IE_ACM,IE_SE_NECESSARIO) <> 'S'
				AND A.IE_AGRUPADOR IN (1,4)) qt_medicamento,
			(SELECT COUNT(*)
			FROM PRESCR_RECOMENDACAO A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO
				AND obter_se_envia_recomendacao(:NEW.cd_estabelecimento, a.cd_recomendacao) = 'S') qt_recomendacao,
			(SELECT COUNT(*)
			FROM REP_JEJUM A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO) qt_jejum,
			(SELECT COUNT(*)
			FROM PRESCR_PROCEDIMENTO A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO
				AND NR_SEQ_PROT_GLIC IS NOT NULL 
				AND OBTER_SE_ACM_SN(IE_ACM,IE_SE_NECESSARIO) <> 'S') qt_glicemia,
			(SELECT COUNT(*)
			FROM PRESCR_PROCEDIMENTO A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO
				AND NR_SEQ_PROT_GLIC  IS NULL
				AND ((Obter_dados_proc_interno(nr_seq_proc_interno,'TU') = 'E')
					OR obter_cod_exame_lab(NR_SEQ_EXAME) IS NOT NULL)) qt_procedimento, 
			(SELECT COUNT(*)
				FROM PRESCR_DIETA  A
				WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO) qt_dieta				   INTO qt_medicamento_w,
																							qt_recomendacao_w,
																							qt_jejum_w,
																							qt_glicemia_w,
																							qt_procedimento_w, 
																							qt_dieta_w
		FROM dual;
			
		if (qt_medicamento_w > 0) then --Medicamentos  - 305
			gerar_int_padrao.gravar_integracao('305', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE' );
		end if;
		if (qt_recomendacao_w > 0) then  --Recomendacao - 300
		 gerar_int_padrao.gravar_integracao('300', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
		if (qt_procedimento_w > 0 ) then --Procedimento  - 310
			gerar_int_padrao.gravar_integracao('310', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
		if (qt_jejum_w > 0) then --Jejun  - 306
			gerar_int_padrao.gravar_integracao('306', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
		if (qt_glicemia_w > 0) then --Glicemia  - 328
			gerar_int_padrao.gravar_integracao('328', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
		if (qt_dieta_w > 0) then -- Dieta oral  - 320
			gerar_int_padrao.gravar_integracao('320', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
	 end;	
	end if;
end;
/
