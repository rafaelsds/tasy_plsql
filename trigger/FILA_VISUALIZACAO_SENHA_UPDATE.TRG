create or replace 
trigger fila_visualizacao_senha_update
before update on fila_visualizacao_senha
for each row
begin

if ((:old.ie_situacao = 'A') and
    (:new.ie_situacao = 'I')) then
	limpar_fila_visualizacao_senha(:new.nr_seq_monitor, :new.nr_seq_pac_senha_fila, :new.nr_sequencia);
end if;
end;
/