create or replace trigger sus_laudo_paciente_aftins
after insert on sus_laudo_paciente
for each row

declare

cd_cgc_prestador_w		varchar2(14);
cd_setor_atendimento_w		number(5);
dt_entrada_unidade_w		date;
nr_seq_atepacu_w			number(10);
cd_medico_responsavel_w		varchar2(10);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
dt_periodo_inicial_w		date;
dt_entrada_w			date;
dt_alta_w				date;
nr_seq_propaci_w			number(10);
cd_estabelecimento_w		number(4);
nr_interno_conta_w			number(10);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
qt_procedimento_w			number(5);
qt_regra_laudo_w			number(5);
ie_proc_laudo_w			varchar2(15) := 'N';


begin

select	count(*)
into	qt_regra_laudo_w
from	sus_regra_gerar_proc_laudo;

if	(qt_regra_laudo_w > 0) then

	begin
	select	nvl(sus_obter_se_gerar_proc_laudo(:new.cd_procedimento_solic,:new.ie_origem_proced,:new.ie_classificacao),'N')
	into	ie_proc_laudo_w
	from 	dual;
	exception
	when others then
		ie_proc_laudo_w := 'N';
	end;
	
else
	ie_proc_laudo_w := 'N';
end if;

if	(ie_proc_laudo_w = 'S') then
	begin
	
	begin
	select	a.cd_convenio,
		a.cd_categoria,
		b.cd_estabelecimento,
		b.dt_entrada,
		b.dt_alta
	into	cd_convenio_w,
		cd_categoria_w,
		cd_estabelecimento_w,
		dt_entrada_w,
		dt_alta_w
	from	atend_categoria_convenio a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	b.nr_atendimento = :new.nr_atendimento
	and	rownum = 1;
	
	select	max(cd_cgc)
	into	cd_cgc_prestador_w
	from	estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_w;

	exception
	when others then
		cd_convenio_w		:= null;
		cd_categoria_w		:= null;
		cd_estabelecimento_w	:= null;
		dt_entrada_w		:= null;
		dt_alta_w 		:= null;
		cd_cgc_prestador_w	:= null;
	end;
	
	begin
	select	a.cd_setor_atendimento,
		a.dt_entrada_unidade,
		a.nr_seq_interno
	into	cd_setor_atendimento_w,
		dt_entrada_unidade_w,
		nr_seq_atepacu_w
	from	atend_paciente_unidade a
	where	a.nr_atendimento	= :new.nr_atendimento
	and	a.dt_entrada_unidade 	= (	select max(x.dt_entrada_unidade)
						from atend_paciente_unidade x
						where x.nr_atendimento = a.nr_atendimento);
	exception
	when others then
		cd_setor_atendimento_w	:= null;
		dt_entrada_unidade_w	:= null;
		nr_seq_atepacu_w	:= null;
	end;

	if	(nr_seq_atepacu_w is not null)  then

		select	procedimento_paciente_seq.nextval
		into	nr_seq_propaci_w
		from	dual;	
	
		begin
		insert into procedimento_paciente(
			nr_sequencia, nr_atendimento, dt_entrada_unidade, cd_procedimento,
			dt_procedimento, qt_procedimento, dt_atualizacao, nm_usuario,
			cd_medico, cd_convenio, cd_categoria, cd_pessoa_fisica,
			dt_prescricao, ds_observacao, vl_procedimento, 	vl_medico,
			vl_anestesista, vl_materiais, cd_edicao_amb, cd_tabela_servico,
			dt_vigencia_preco, cd_procedimento_princ, dt_procedimento_princ, dt_acerto_conta,
			dt_acerto_convenio, dt_acerto_medico, vl_auxiliares, vl_custo_operacional,
			tx_medico, tx_anestesia, nr_prescricao, nr_sequencia_prescricao,
			cd_motivo_exc_conta, ds_compl_motivo_excon, cd_acao, qt_devolvida,
			cd_motivo_devolucao, nr_cirurgia, nr_doc_convenio, cd_medico_executor,
			ie_cobra_pf_pj, nr_laudo, dt_conta, cd_setor_atendimento,
			cd_conta_contabil, cd_procedimento_aih, ie_origem_proced, nr_aih,
			ie_responsavel_credito, tx_procedimento, cd_equipamento, ie_valor_informado,
			cd_estabelecimento_custo, cd_tabela_custo, cd_situacao_glosa, nr_lote_contabil,
			cd_procedimento_convenio, nr_seq_autorizacao, 	ie_tipo_servico_sus, ie_tipo_ato_sus,
			cd_cgc_prestador, nr_nf_prestador, cd_atividade_prof_bpa, nr_interno_conta,
			nr_seq_proc_princ, ie_guia_informada, dt_inicio_procedimento, ie_emite_conta,
			ie_funcao_medico, ie_classif_sus, 	cd_especialidade, nm_usuario_original,
			nr_seq_proc_pacote, ie_tipo_proc_sus, cd_setor_receita, vl_adic_plant,
			nr_seq_atepacu, ie_auditoria)
		values	(nr_seq_propaci_w, :new.nr_atendimento,dt_entrada_unidade_w,:new.cd_procedimento_solic,
			dt_entrada_unidade_w, nvl(:new.qt_procedimento_solic,1), sysdate, :new.nm_usuario,
			null, cd_convenio_w, cd_categoria_w, null,
			null,'Regra geracao proc laudo', 0, 0,
			0, 0,null,null,
			null, null, null, null,
			null, null, 0, 0,
			1, 1, null, null,
			null, null, null, null,
			null, null, null, coalesce(:new.cd_medico_requisitante,:new.cd_profis_requisitante),
			null, null, null, cd_setor_atendimento_w,
			null, null, :new.ie_origem_proced, null,
			null, 100, null, 'N',
			cd_estabelecimento_w, null, null, null,		 
			null, null, null, null,
			cd_cgc_prestador_w, null, null, null,
			null, null, null, null,
			null, null, null, null,
			null, null, cd_setor_atendimento_w, 0,
			nr_seq_atepacu_w,null);	
			
		atualiza_preco_procedimento(nr_seq_propaci_w,cd_convenio_w,:new.nm_usuario);	
		
		
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(188082,'SQL_ERRO='||substr(sqlerrm,1,255));
			/* Ocorreu o seguinte problema ao inserir registro na procedimento paciente:#@SQL_ERRO#@ */
		end;
	
	end if;
	
	end;
end if;

end;
/
