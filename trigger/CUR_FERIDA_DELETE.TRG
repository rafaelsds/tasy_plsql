create or replace trigger CUR_FERIDA_DELETE	
after delete on cur_ferida
for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	nr_seq_item_pront = 53	
and		nr_seq_registro  = :old.nr_sequencia;

commit;	

<<Final>>
qt_reg_w	:= 0;
end;
/
