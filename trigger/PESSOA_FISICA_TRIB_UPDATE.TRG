create or replace trigger pessoa_fisica_trib_update
before insert or update on pessoa_fisica_trib
for each row
declare

qt_registro_w 	number(5);

pragma autonomous_transaction;

begin

select	count(*)
into 	qt_registro_w
from 	pessoa_fisica_trib
where 	trunc(dt_inicio_vigencia) = trunc(:new.dt_inicio_vigencia)
and 	trunc(dt_fim_vigencia) = trunc(:new.dt_fim_vigencia)
and 	cd_tributo = :new.cd_tributo
and 	nvl(ds_emp_retencao,'0') = nvl(:new.ds_emp_retencao,'0')
and 	cd_pessoa_fisica = :new.cd_pessoa_fisica
and 	nr_sequencia <> :new.nr_sequencia;

if	(qt_registro_w > 0) then
	Wheb_mensagem_pck.exibir_mensagem_abort(688042); 
end if;

end;
/