create or replace
trigger atendimento_paciente_upd_hl7
after update on atendimento_paciente
for each row

declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

begin
ds_sep_bv_w := obter_separador_bv;

/*
if	(:old.dt_alta is null) and
	(:new.dt_alta is null) and
	(:new.ie_tipo_atendimento = 1) and
	(:old.nr_seq_unid_int = :new.nr_seq_unid_int) then
	begin
	ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
				'nr_atendimento=' || :new.nr_atendimento|| ds_sep_bv_w ||
				'nr_seq_interno=' || :new.nr_seq_unid_int || ds_sep_bv_w;
	gravar_agend_integracao(18, ds_param_integ_hl7_w);
	end;
end if;
*/

if	(:old.ie_tipo_atendimento != 1 ) and
	(:new.ie_tipo_atendimento = 1) then
	ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||                                                                          
							'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ;                
                            
    if	(substr(l10nger_integrar_adt_orm(:new.cd_pessoa_fisica, :new.nr_atendimento, null, null),1,1) = 'S') then
        gravar_agend_integracao(693, ds_param_integ_hl7_w);
    end if;     
end if;

end;
/