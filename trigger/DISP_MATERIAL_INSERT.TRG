create or replace trigger disp_material_insert
after insert or update on material
for each row
declare

qt_registro_w	number(5);

begin

select	count(*)
into	qt_registro_w
from	dispensario_liberacao_local
where	cd_material = :new.cd_material;

if	(qt_registro_w > 0) then

	insert into dispensario_material(
		nr_sequencia,
		cd_estabelecimento,
		cd_material,
		ds_material,
		ie_status,
		dt_exclusao,
		dt_registro,
		dt_atualizacao,
		cd_material_generico,
		cd_material_estoque,
		ie_tipo_material,
		ie_lido)
	values(disp_material_SEQ.nextval,
		null,
		:new.cd_material,
		:new.ds_material,
		:new.ie_situacao,
		null,
		sysdate,
		sysdate,
		:new.cd_material_generico,
		:new.cd_material_estoque,
		:new.ie_tipo_material,
		'');
end if;
end disp_material_insert;
/