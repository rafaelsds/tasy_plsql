create or replace trigger nut_elem_material_atual
before insert or update on nut_elem_material
for each row

declare

begin

if 	(:new.qt_min is not null) and
	(:new.qt_max is not null) and
	(:new.qt_min <> 0) and
	(:new.qt_max <> 0) and
	(:new.qt_min > :new.qt_max) then
	begin
	Wheb_mensagem_pck.exibir_mensagem_abort(392333);	
	end;
end if;

end;
/