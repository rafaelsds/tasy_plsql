create or replace trigger pls_lote_evento_del
before delete on pls_lote_evento
for each row

declare
qt_rotina_w		number(15);


begin
select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= 	(select userenv('sessionid') 
			from 	dual)
and	username = 	(select username 
			from 	v$session 
			where 	audsid = (	select userenv('sessionid') 
						from dual))
and	action like 'PLS_DESFAZER_LOTE_PAGAMENTO%';

if	(qt_rotina_w = 0) and
	(:old.nr_seq_lote_pagamento is not null) and
	(:old.ie_origem = 'A') then
	wheb_mensagem_pck.exibir_mensagem_abort(271816, 'NR_SEQ_LOTE_PGTO=' || :old.nr_seq_lote_pagamento);
end if;

end;
/