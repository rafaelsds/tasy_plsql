CREATE OR REPLACE TRIGGER sus_apac_movto_Insert
AFTER INSERT ON sus_apac_movto
FOR EACH ROW

DECLARE

a	varchar2(1);	

/*
nr_sequencia_w			number(10);
cd_procedimento_w			number(15);
ie_origem_proced_w			number(1);
cd_atividade_prof_bpa_w		number(3);
qt_procedimento_w			number(9,3);
nr_atendimento_w			number(10);
nr_seq_propaci_w			number(10);

cursor C01 is
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_atividade_prof_bpa,
	a.qt_procedimento,
	a.nr_atendimento,
	a.nr_sequencia
from	conta_paciente c,
	procedimento_paciente a
where	a.nr_atendimento	= :new.nr_atendimento
and	a.nr_interno_conta	= c.nr_interno_conta 
and	c.ie_cancelamento is null
and	not exists
	(select	1
	from	sus_apac_proc b
	where	b.nr_seq_propaci = a.nr_sequencia);  */

BEGIN

a	:= 'c';

/*  Retirei a trigger e criei a procedure Adicionar_sus_apac_proc

open C01;
loop
fetch c01 into
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_atividade_prof_bpa_w,
	qt_procedimento_w,
	nr_atendimento_w,
	nr_seq_propaci_w;
	exit when c01%notfound;
	BEGIN

	
	select	sus_apac_proc_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into 	sus_apac_proc(
		NR_SEQUENCIA,
		NR_SEQ_APAC,
		CD_PROCEDIMENTO,
		IE_ORIGEM_PROCED,
		CD_ATIVIDADE_PROF,
		QT_PROCEDIMENTO,
		DT_ATUALIZACAO,
		NM_USUARIO,
		CD_CGC_FORNECEDOR,
		NR_NOTA_FISCAL,
		NR_SEQ_PROPACI)
	values(
		nr_sequencia_w,
		:new.nr_sequencia,
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_atividade_prof_bpa_w,		
		qt_procedimento_w,
		sysdate,
		:new.nm_usuario,
		null,
		null,
		nr_seq_propaci_w);
	END;
end loop;
close C01;

*/
END;
/