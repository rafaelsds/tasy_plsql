CREATE OR REPLACE TRIGGER config_proxy_captcha_befins
BEFORE INSERT or UPDATE
ON CONFIG_PROXY_CAPTCHA
FOR EACH ROW

BEGIN  

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if (NVL(pkg_i18n.get_user_locale, 'pt_BR') <> 'pt_BR') and (:new.ds_senha is not null) then
		:new.ds_senha := null;
	end if;
end if;


END;
/