create or replace trigger agenda_paciente_after_update
after update on agenda_paciente
for each row
declare
qt_tempo_regra_w     		wl_regra_item.qt_tempo_regra%type;
qt_tempo_normal_w			wl_regra_item.qt_tempo_normal%type;
nr_seq_regra_w				wl_regra_item.nr_sequencia%type;
ie_escala_w					wl_regra_item.ie_escala%type;
qt_tarefa_w	                number(10);
ie_se_regra_geracao_w       varchar(1);
ie_proced_cih_w             varchar(1) := 'N';

cursor C01 is
  select  nvl(b.qt_tempo_regra, 0),
      nvl(b.qt_tempo_normal, 0),
      nvl(b.nr_sequencia, 0)
  from  wl_regra_worklist a,
        wl_regra_item b,
        wl_regra_geracao c
  where  a.nr_sequencia = b.nr_seq_regra
  and    b.nr_sequencia = c.nr_seq_regra_item (+)
  and    b.ie_situacao = 'A'
  and    c.cd_setor_atendimento is null
  and    b.ie_opcao_wl = 'H'
  and    a.nr_seq_item = (  select  max(x.nr_sequencia)
                from  wl_item x
                where  x.nr_sequencia = a.nr_seq_item
                and    x.cd_categoria = 'IH'
                and    x.ie_situacao = 'A'); 
begin
if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then
begin
select ie_proced_cih
into ie_proced_cih_w
from proc_interno
where nr_sequencia = :new.nr_seq_proc_interno;
exception
when others then
    ie_proced_cih_w := 'N';
end;

if(ie_proced_cih_w = 'S' and :new.nr_atendimento is not null) then
        open C01;  
		loop
		fetch C01 into           
			qt_tempo_regra_w,
            qt_tempo_normal_w,
            nr_seq_regra_w;
		exit when C01%notfound;
			begin
            
            select	count(*)
            into	qt_tarefa_w
            from	wl_worklist
            where	nr_atendimento = :new.nr_atendimento  
            and     nr_seq_regra = nr_seq_regra_w
            and     dt_final_real is null;
            
            select obter_se_regra_geracao(nr_seq_regra_w, null, null)
            into ie_se_regra_geracao_w
            from dual;
                            
			if (ie_se_regra_geracao_w = 'S' and qt_tarefa_w = 0) then
                wl_gerar_finalizar_tarefa('IH','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,sysdate+(qt_tempo_normal_w/24),'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,sysdate,null,null,null,null,null,null);                            
			end if;
			
			end;
		end loop;
		close C01;
end if; 
end if;
end;
/