create or replace trigger ESCALA_APGAR_ATUAL
before insert or update on ESCALA_APGAR
for each row

declare
  sql_w varchar2(800);
  qt_pontuacao_w number;
  ds_erro_w   varchar2(2000);
  ds_parametro_w  varchar2(2000);
begin
  if (wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
    begin
      if	(:new.nr_hora is null) or (:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
        begin
          :new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
        end;
      end if;

      sql_w := 'CALL OBTER_SCORE_APGAR_ATUAL_MD(:1, :2, :3, :4, :5) INTO :qt_pontuacao_w';
      EXECUTE IMMEDIATE sql_w USING IN :new.qt_freq_cardiaca,
                                    IN :new.qt_esforco,
                                    IN :new.qt_tonus_muscular,
                                    IN :new.qt_irritabilidade_reflexa,
                                    IN :new.qt_cor,
                                    OUT qt_pontuacao_w;
    exception
      when others then
        :new.QT_PONTUACAO := null;
        ds_erro_w := sqlerrm;
        ds_parametro_w := ':new.nr_atendimento: ' || :new.nr_atendimento || ' - :new.cd_profissional: ' || :new.cd_profissional || ' - :new.ie_situacao: ' || :new.ie_situacao;
        ds_parametro_w := ds_parametro_w || ' - :new.qt_freq_cardiaca: ' || :new.qt_freq_cardiaca || ' - :new.qt_esforco: ' || :new.qt_esforco || ' - :new.qt_tonus_muscular: ' || :new.qt_tonus_muscular || ' - :new.qt_irritabilidade_reflexa: ' || :new.qt_irritabilidade_reflexa || ' - :new.qt_cor: ' || :new.qt_cor || ' - qt_pontuacao_w: ' || qt_pontuacao_w;
        gravar_log_medical_device('ESCALA_APGAR_ATUAL', 'OBTER_SCORE_APGAR_ATUAL_MD', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');

      end;

      :new.QT_PONTUACAO := qt_pontuacao_w;
  end if;
end;
/
