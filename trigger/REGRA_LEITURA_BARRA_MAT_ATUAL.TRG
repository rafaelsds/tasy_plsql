create or replace trigger regra_leitura_barra_mat_atual
before insert or update on regra_leitura_barra_mat
for each row
declare

begin

if	(:new.ie_tipo_requisicao is not null) then
	begin
	
	if	(nvl(:new.cd_funcao,0) <> 109) then
		begin
		:new.ie_tipo_requisicao := null;
		end;
	end if;
	
	end;
end if;

end;
/