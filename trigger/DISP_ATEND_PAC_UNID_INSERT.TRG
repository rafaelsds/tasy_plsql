create or replace trigger disp_atend_pac_unid_insert
before insert on atend_paciente_unidade
for each row
declare

nm_pessoa_fisica_w		varchar2(60);
nr_sequencia_w		dispensario_atend_paciente.nr_sequencia%type;
cd_pessoa_fisica_w		varchar2(10);
dt_nascimento_w		date;
ie_tipo_atendimento_w	number(3);
cd_estabelecimento_w		Number(4);
ie_alta_w			varchar2(1);
qt_registro_w			Number(10);

BEGIN

select	count(*)
into	qt_registro_w
from	setor_atendimento
where	cd_setor_atendimento	= :new.cd_setor_atendimento;
--and	cd_classif_setor in (1,3,4,8);

if	(qt_registro_w > 0) and	(nvl(:new.ie_passagem_setor,'N') = 'N') and
	(:new.cd_unidade_basica is not null) then
	begin
	select	cd_pessoa_fisica,
		cd_estabelecimento,
		ie_tipo_atendimento,
		decode(dt_alta,null,'N','S')
	into	cd_pessoa_fisica_w,
		cd_estabelecimento_w,
		ie_tipo_atendimento_w,
		ie_alta_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;
	
	/*Dados do Paciente */
	select	dt_nascimento,
		nm_pessoa_fisica
	into	dt_nascimento_w,
		nm_pessoa_fisica_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	select	count(*)
	into	qt_registro_w
	from	dispensario_paciente
	where	nr_prontuario	=	to_number(cd_pessoa_fisica_w);
	
	if	(qt_registro_w = 0) then
		begin
		/* Inserir Paciente */
		insert	into dispensario_paciente(
			nr_sequencia,
			nm_pessoa_fisica,
			cd_estabelecimento,
			nr_prontuario,
			dt_nascimento,
			dt_registro,
			dt_atualizacao,
			ie_lido)
		values(disp_paciente_seq.nextval,
			nm_pessoa_fisica_w,
			cd_estabelecimento_w,
			to_number(cd_pessoa_fisica_w),
			dt_nascimento_w,
			sysdate,
			sysdate,
			'');
		end;
	else
		update	dispensario_paciente
		set	nm_pessoa_fisica = nm_pessoa_fisica_w,
			nr_prontuario	= to_number(cd_pessoa_fisica_w), 
			dt_atualizacao	= sysdate,
			ie_lido		= ''
		where	nr_prontuario = to_number(cd_pessoa_fisica_w);
	end if;

	/* Aten_pac_unidade*/
	select	disp_atend_paciente_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into dispensario_atend_paciente(
		nr_sequencia,
		nr_atendimento_paciente,
		nr_prontuario,
		cd_estabelecimento,
		tipo_atendimento,
		cd_setor_atendimento,
		leito,
		alta,
		dt_registro,
		dt_atualizacao,
		ie_lido)
	values(nr_sequencia_w,
		:new.nr_atendimento,
		to_number(cd_pessoa_fisica_w),
		cd_estabelecimento_w,
		substr(to_char(ie_tipo_atendimento_w),1,1),
		:new.cd_setor_atendimento,
		:new.cd_unidade_basica,
		nvl(ie_alta_w,'N'),
		sysdate,
		sysdate,
		'');
	end;
end if;

END disp_atend_pac_unid_insert;
/
