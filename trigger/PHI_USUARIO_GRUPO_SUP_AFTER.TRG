create or replace trigger phi_usuario_grupo_sup_after
after insert or update or delete on usuario_grupo_sup
for each row

declare

nr_sequencia_w		number(10);
ds_call_stack_w		varchar2(4096) := substr(dbms_utility.format_call_stack,1,4096);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	null;
end if;

if	(:new.nm_usuario_grupo <> :old.nm_usuario_grupo) then
	wheb_mensagem_pck.exibir_mensagem_abort(1174198); --Changing the user is not allowed.
elsif	(:new.nr_seq_grupo <> :old.nr_seq_grupo) then
	wheb_mensagem_pck.exibir_mensagem_abort(1174199); --Changing the group is not allowed.
end if;

if	(inserting) and
	(instr(ds_call_stack_w,'PHI_AUSENCIA_TASY_BEFORE') = 0) then
	nr_sequencia_w := substr(phi_obter_dados_grupo_usu(null,null,:new.nr_seq_grupo,:new.nm_usuario_grupo,:new.dt_inicio_vigencia,:new.dt_fim_vigencia,'N','UG'),1,10);
elsif	(updating or deleting) then
	nr_sequencia_w := substr(phi_obter_dados_grupo_usu(null,null,:old.nr_seq_grupo,:old.nm_usuario_grupo,:old.dt_inicio_vigencia,:old.dt_fim_vigencia,'N','UG'),1,10);
end if;

if	(nr_sequencia_w is not null) then
	begin
	if	(deleting) then
		delete from gerencia_wheb_grupo_usu where nr_sequencia = nr_sequencia_w;
	else
		update	gerencia_wheb_grupo_usu
		set	dt_atualizacao = sysdate,
			nm_usuario = :new.nm_usuario,
			dt_inicio = :new.dt_inicio_vigencia,
			dt_fim = :new.dt_fim_vigencia,
			ie_emprestimo = :new.ie_emprestimo
		where	nr_sequencia = nr_sequencia_w;
	end if;
	end;
elsif	(inserting or updating) then
	begin
	nr_sequencia_w := substr(phi_obter_dados_grupo_usu(null,null,:new.nr_seq_grupo,null,null,null,null,'GG'),1,10);

	if	(nr_sequencia_w is not null) then
		insert into gerencia_wheb_grupo_usu (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_grupo,
				nm_usuario_grupo,
				dt_inicio,
				dt_fim,
				ie_emprestimo)
			values (gerencia_wheb_grupo_usu_seq.nextval,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				nr_sequencia_w,
				:new.nm_usuario_grupo,
				:new.dt_inicio_vigencia,
				:new.dt_fim_vigencia,
				:new.ie_emprestimo);
	end if;
	end;
end if;

if	(instr(ds_call_stack_w,'PHI_AUSENCIA_TASY_BEFORE') = 0) then
	phi_atualizar_alocacao_usuario('A',nvl(:new.nm_usuario_grupo,:old.nm_usuario_grupo),null,null,null,null,null,nvl(:new.nm_usuario,:old.nm_usuario),'N');
end if;

end;
/