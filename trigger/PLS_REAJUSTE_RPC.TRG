create or replace trigger pls_reajuste_rpc
before update or insert or delete on pls_reajuste
for each row
declare
ie_consistir_envio_rpc_w	varchar2(1);

begin

select	max(ie_consistir_envio_rpc)
into	ie_consistir_envio_rpc_w
from	pls_parametros
where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_consistir_envio_rpc_w,'N') = 'S') and
	(:new.ie_tipo_reajuste = 'C') then
	if	(updating) then
		if	(:new.ie_status <> :old.ie_status) and
			(:new.ie_status = '2') and
			(pls_obter_se_rpc_enviado(:new.dt_aplicacao_reajuste) = 'S') then
			wheb_mensagem_pck.exibir_mensagem_abort(360248);
		end if;
	elsif	(inserting) and
		(pls_obter_se_rpc_enviado(:new.dt_aplicacao_reajuste) = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(360248); --N�o � poss�vel gerar reajuste pois j� foi realizado o envio do RPC para o per�odo.
	end if;
end if;

end;
/