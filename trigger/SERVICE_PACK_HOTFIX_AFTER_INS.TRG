create or replace trigger service_pack_hotfix_after_ins
after insert on service_pack_hotfix
for each row

declare

begin

  insert_service_order_sp_hotfix(:new.nr_service_order, :new.nr_sequencia, :new.nm_usuario, 'N');

end;
/