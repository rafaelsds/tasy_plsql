create or replace trigger pls_gravar_nr_seq_contr
before insert or update on pls_emissao_carteira_regra
for each row

declare

nr_seq_contrato_w 	pls_contrato.nr_sequencia%type;

begin

if 	(:new.nr_contrato is not null) and 
	(:new.nr_contrato <> nvl(:old.nr_contrato,0)) then

	select 	max(nr_sequencia)
	into	nr_seq_contrato_w
	from 	pls_contrato
	where 	nr_contrato = :new.nr_contrato;

	if	(nr_seq_contrato_w is not null) then
		:new.nr_seq_contrato := nr_seq_contrato_w;
	end if;
end if;

end;
/
