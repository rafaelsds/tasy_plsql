CREATE OR REPLACE TRIGGER AGFA_ORM_OUT_ATUAL
BEFORE INSERT or UPDATE ON AGFA_ORM_OUT
FOR EACH ROW
declare

/*ORM_02_STATUS_HIS   =   T - Tratado   N - Novo  E - Erro */
/*ORC_10_USER_RIS - O Usu�rio deve ser igual ao usu�rio do TASY, devido ao par�metro,   Estabelecimento fixo 1 */

nr_prescricao_w            number(14,0);
nr_sequencia_w            number(6,0);
nr_seq_w            number(6,0);
ds_erro_w            varchar2(2000);
nr_seq_proc_interno_w        number(10,0);
cd_procedimento_w        number(15,0);
ie_origem_proced_w        number(10);
qt_procedimento_w        number(8);
cd_setor_atendimento_w        number(5);
dt_procedimento_w        date;
nr_seq_exame_w            number(10,0);
ie_lado_w            varchar2(1);
nr_seq_agenda_w            number(10);
ie_atualiza_status_agenda_w    varchar2(1);
nr_atendimento_w        number(10);
qt_executado_w            number(10,0);
nr_seq_interno_w        number(10,0);
nr_seq_interno_atend_w        number(10,0);
qt_reg_prescr_proc_w        number(10,0);
quebra_w            varchar2(10)    := chr(13)||chr(10);
dt_entrada_w            date;
ie_suspenso_w            varchar2(1);
dt_fim_conta_w            date:= null;

nr_seq_proc_interno_ww        number(10,0);
cd_procedimento_ww        number(15,0);
ie_origem_proced_ww        number(10);
qt_procedimento_ww        number(8);
cd_setor_atendimento_ww        number(5);
nr_seq_exame_ww            number(10,0);
ie_lado_ww            varchar2(1);
qt_conta_w        number(5,0);

dt_entrada_unidade_w        date;

cursor c01 is
select    nr_sequencia,
    nr_seq_proc_interno,
    cd_procedimento,
    ie_origem_proced,
    qt_procedimento,
    cd_setor_atendimento,
    nr_seq_exame,
    ie_lado
from    prescr_procedimento
where   nr_prescricao            = nr_prescricao_w
and    nr_seq_proc_princ    = nr_seq_interno_w;

begin

begin

select     somente_numero(substr(:new.OBR_02_EXAM_ID, 1, instr(:new.OBR_02_EXAM_ID,'-') - 1)),
    somente_numero(substr(:new.OBR_02_EXAM_ID, instr(:new.OBR_02_EXAM_ID,'-') + 1, length(:new.OBR_02_EXAM_ID)))
into    nr_prescricao_w,
    nr_sequencia_w
from    dual;

select     count(*)
into    qt_reg_prescr_proc_w
from     prescr_procedimento     a,
    prescr_medica         b
where     b.nr_prescricao = a.nr_prescricao
and     a.nr_prescricao = nr_prescricao_w
and     a.nr_sequencia  = nr_sequencia_w;

--Existem registros que vem pela integra��o DFT que n�o possuem n�mero da prescri��o, por�m o acession number est� gravado na prescr_procedimento...
if    (qt_reg_prescr_proc_w = 0) then

    select     max(nr_prescricao),
        max(nr_sequencia)
    into    nr_prescricao_w,
        nr_sequencia_w
    from    prescr_procedimento
    where     nr_acesso_dicom = :new.OBR_18_ACC_NUMBER
    and     dt_prev_execucao > sysdate - 30;

end if;

if    (nr_sequencia_w    is null) and
    (nr_prescricao_w is null) then
    ds_erro_w    := 'Prescri��o n�o encontrada. ' || ' OBR_18_ACC_NUMBER: '||:new.OBR_18_ACC_NUMBER;
    :new.ORM_02_STATUS_HIS    := 'E';
    insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
                values  (sysdate, :new.ORC_10_USER_RIS, -44444, ds_erro_w);

    goto final;
end if;

select     a.nr_prescricao,
    a.nr_sequencia,
    a.nr_seq_proc_interno,
    a.cd_procedimento,
    a.ie_origem_proced,
    a.qt_procedimento,
    a.cd_setor_atendimento,
    a.dt_prev_execucao,
    a.nr_seq_exame,
    nvl(a.ie_lado,'A'),
    b.nr_atendimento,
    nvl(a.ie_suspenso,'N')
into    nr_prescricao_w,
    nr_sequencia_w,
    nr_seq_proc_interno_w,
    cd_procedimento_w,
    ie_origem_proced_w,
    qt_procedimento_w,
    cd_setor_atendimento_w,
    dt_procedimento_w,
    nr_seq_exame_w,
    ie_lado_w,
    nr_atendimento_w,
    ie_suspenso_w
from     prescr_procedimento     a,
    prescr_medica         b
where     b.nr_prescricao = a.nr_prescricao
and     a.nr_prescricao = nr_prescricao_w
and     a.nr_sequencia  = nr_sequencia_w;

if    (ie_suspenso_w = 'S') then

    ds_erro_w    := 'O item foi suspenso, prescri��o: ' || nr_prescricao_w || ' sequ�ncia: ' || nr_sequencia_w;
    :new.ORM_02_STATUS_HIS    := 'E';
    insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
                values  (sysdate, :new.ORC_10_USER_RIS, -44444, ds_erro_w);

    goto final;

end if;


select     count(*)
into    qt_executado_w
from    prescr_procedimento a,
    procedimento_paciente b
where     a.nr_prescricao = b.nr_prescricao
and    a.nr_sequencia = b.nr_sequencia_prescricao
and     b.nr_prescricao    = nr_prescricao_w
and     b.nr_sequencia_prescricao    = nr_sequencia_w
and     a.nr_prescricao    = nr_prescricao_w
and     a.nr_sequencia    = nr_sequencia_w
and    b.cd_motivo_exc_conta is null;

dt_fim_conta_w:= null;

if    (nr_atendimento_w    is not null) then

    select    max(dt_entrada)
    into    dt_entrada_w
    from    atendimento_paciente
    where     nr_atendimento    = nr_atendimento_w;

    if    (dt_procedimento_w    < dt_entrada_w) then /*Felipe martini em 16/04/2009  OS137872*/
        dt_procedimento_w    := dt_entrada_w;
    end if;

    select     max(dt_fim_conta)
    into    dt_fim_conta_w
    from     atendimento_paciente
    where     nr_atendimento = nr_atendimento_w;

end if;

if    (dt_procedimento_w    > sysdate) then
    dt_procedimento_w    := sysdate;
end if;

select    max(nr_seq_interno)
into    nr_seq_interno_w
from    prescr_procedimento
where    nr_prescricao = nr_prescricao_w
and    nr_sequencia = nr_sequencia_w;


if    (qt_executado_w = 0) then

    insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
            values  (sysdate, :new.ORC_10_USER_RIS, -44430, 'Prescr' || nr_prescricao_w || 'Seq prescr' || nr_sequencia_w || ' Status' || :new.ORC_25_STATUS );


    Agfa_Prescr_Proc_Acc_Number(nr_prescricao_w,   nr_sequencia_w,  :new.OBR_18_ACC_NUMBER,  ds_erro_w);

    if    (ds_erro_w is Not Null) then
        :new.ORM_02_STATUS_HIS := 'E';
    else
        if    ((:new.ORC_25_STATUS = '3') or
            (:new.ORC_25_STATUS like '%Started%')) then

            /* Mudar o status na Gest�o de Exames para "Em EXAME"                           */
            /* atualizar_status_exame_prescr(nr_prescricao_w, nr_sequencia_w, 'I', :new.ORC_10_USER_RIS, 1);  */
            /* Retirei a chamada da procedure acima devido � mesma fazer commit em seu final, por esse motivo */
            /* coloquei os tratamentos na pr�pria trigger                              */

            update   prescr_procedimento
            set      ie_status_execucao       =  15,
                 dt_inicio_exame          =  sysdate
            where    nr_prescricao            =  nr_prescricao_w
            and      nr_sequencia             =  nr_sequencia_w
            and      ie_status_execucao    = 10;

            update   prescr_procedimento
            set      ie_status_execucao       =  15,
                 dt_inicio_exame          =  sysdate
            where    nr_prescricao            =  nr_prescricao_w
            and     nr_seq_proc_princ    = nr_seq_interno_w
            and      ie_status_execucao    = 10;

            obter_param_usuario(942, 90, obter_perfil_ativo, :new.ORC_10_USER_RIS , 1, ie_atualiza_status_agenda_w);

            if    (ie_atualiza_status_agenda_w = 'S') then

                select    nvl(max(nr_seq_agenda),0)
                into    nr_seq_agenda_w
                from    prescr_medica
                where    nr_prescricao            =  nr_prescricao_w;

                if    (nr_seq_agenda_w = 0) then

                    select    max(nr_atendimento)
                    into    nr_atendimento_w
                    from    prescr_medica
                    where    nr_prescricao = nr_prescricao_w;

                    select    nvl(max(nr_sequencia),0)
                    into    nr_seq_agenda_w
                    from    agenda_paciente
                    where    nr_atendimento            =  nr_atendimento_w
                    and    ie_status_agenda  not in ('B','C','E');

                end if;

                if    (nr_seq_agenda_w > 0) then

                    update     agenda_paciente
                    set        ie_status_agenda = 'EE',
                        dt_em_exame     = sysdate
                    where    nr_sequencia    = nr_seq_agenda_w
                    and    ie_status_agenda  not in ('B','C','E');

                end if;
            end if;


        elsif    (((:new.ORC_25_STATUS = '5') or (:new.ORC_25_STATUS like '%Finished%')) and (dt_fim_conta_w is null)) then

            select    max(nr_seq_interno),
                max(dt_entrada_unidade)
            into    nr_seq_interno_atend_w,
                dt_entrada_unidade_w
            from    atend_paciente_unidade
            where    nr_atendimento        = nr_atendimento_w
            and    cd_setor_atendimento    = cd_setor_atendimento_w;

            if    (nr_seq_interno_atend_w     is  null) or
                (dt_entrada_unidade_w     is  null) then

                gerar_passagem_setor_s_agfa( nr_atendimento_w, cd_setor_atendimento_w, :new.ORC_10_USER_RIS);

            end if;
            
       select count(*) 
       into    qt_conta_w
       from      procedimento_paciente
       where  nr_prescricao    = nr_prescricao_w
            and     nr_sequencia_prescricao    = nr_sequencia_w
       and    cd_motivo_exc_conta is null;
       
       if    (qt_conta_w = 0) then
       
        /*Mudar o status para "EXECUTADO"  e lan�ar na conta */
        Gerar_Proc_Pac_Item_Prescr(nr_prescricao_w,  nr_sequencia_w,  null,null, nr_seq_proc_interno_w, cd_procedimento_w,  ie_origem_proced_w,
            qt_procedimento_w, cd_setor_atendimento_w,  9, dt_procedimento_w, :new.ORC_10_USER_RIS , :new.OBR_32_DOCT, nr_seq_exame_w, ie_lado_w, null);

        

       end if;
       
       AGFA_ADEP_admin_procedimentos(nr_prescricao_w,nr_sequencia_w, sysdate, :new.ORC_10_USER_RIS);
       
       update    prescr_procedimento
       set       ie_status_execucao     = '20',
            cd_motivo_baixa     = 1,
            dt_baixa        = sysdate
       where    nr_prescricao    = nr_prescricao_w
       and     nr_sequencia    = nr_sequencia_w;
       
            open C01;
            loop
            fetch C01 into
                nr_seq_w,
                nr_seq_proc_interno_ww,
                cd_procedimento_ww,
                ie_origem_proced_ww,
                qt_procedimento_ww,
                cd_setor_atendimento_ww,
                nr_seq_exame_ww,
                ie_lado_ww;
            exit when C01%notfound;
                select    max(nr_seq_interno),
                    max(dt_entrada_unidade)
                into    nr_seq_interno_atend_w,
                    dt_entrada_unidade_w
                from    atend_paciente_unidade
                where    nr_atendimento        = nr_atendimento_w
                and    cd_setor_atendimento    = cd_setor_atendimento_ww;

                if    (nr_seq_interno_atend_w     is  null) or
                    (dt_entrada_unidade_w     is  null) then

                    gerar_passagem_setor_s_agfa( nr_atendimento_w, cd_setor_atendimento_w, :new.ORC_10_USER_RIS);

                end if;

        select count(*) 
        into    qt_conta_w
        from      procedimento_paciente
        where  nr_prescricao    = nr_prescricao_w
        and     nr_sequencia_prescricao    = nr_seq_w
        and    cd_motivo_exc_conta is null;


        if    (qt_conta_w = 0) then
            /*Mudar o status para "EXECUTADO"  e lan�ar na conta em todos os proc associados ao procedimento principal */
            Gerar_Proc_Pac_Item_Prescr(nr_prescricao_w,  nr_seq_w,  null,null, nr_seq_proc_interno_ww, cd_procedimento_ww,  ie_origem_proced_ww,
                        qt_procedimento_ww, cd_setor_atendimento_ww,  9, dt_procedimento_w, :new.ORC_10_USER_RIS , :new.OBR_32_DOCT, nr_seq_exame_ww, ie_lado_ww, null);
                        
        end if;
            end loop;
            close C01;

            update   prescr_procedimento
            set      ie_status_execucao       = '20',
                 cd_motivo_baixa     = 1,
                 dt_baixa        = sysdate
            where    nr_prescricao            = nr_prescricao_w
            and     nr_seq_proc_princ    = nr_seq_interno_w;

        end if;

        :new.ORM_02_STATUS_HIS:=    'T';

    end if;

elsif    (qt_executado_w > 0) then
    :new.ORM_02_STATUS_HIS:=    'T';
end if;

exception
    when others then
        :new.ORM_02_STATUS_HIS    := 'E';
        ds_erro_w    := ' OBR_18_ACC_NUMBER: '||:new.OBR_18_ACC_NUMBER;
        ds_erro_w        :=ds_erro_w||'    '|| substr(sqlerrm(sqlcode),1,1600);
        insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
                values  (sysdate, :new.ORC_10_USER_RIS, -44444, substr(ds_erro_w,1,1999));
end;


<<Final>>

quebra_w    := quebra_w;

end AGFA_ORM_OUT_ATUAL;
/
