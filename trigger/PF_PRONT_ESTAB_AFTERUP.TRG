create or replace TRIGGER PF_PRONT_ESTAB_AFTERUP 
AFTER INSERT OR UPDATE ON PESSOA_FISICA_PRONT_ESTAB 
FOR EACH ROW 
DECLARE 
reg_integracao_w   gerar_int_padrao.reg_integracao;
qt_existe_w        PLS_INTEGER;
nr_cpf_w           pessoa_fisica.nr_cpf%TYPE;
nr_seq_conselho_w  pessoa_fisica.nr_seq_conselho%TYPE;
nr_prontuario_w    pessoa_fisica.nr_prontuario%TYPE;
ie_funcionario_w   reg_integracao_w.ie_funcionario%TYPE;

BEGIN 
    IF (wheb_usuario_pck.get_ie_executar_trigger = 'S' ) THEN
        SELECT CASE WHEN EXISTS (SELECT * FROM sup_parametro_integracao  a,
                   sup_int_regra_pf          b
              WHERE a.nr_sequencia = b.nr_seq_integracao
                    AND a.ie_evento = 'PF'
                    AND a.ie_forma = 'E'
                    AND a.ie_situacao = 'A'
                    AND b.ie_situacao = 'A')
            THEN 1
            ELSE 0 END
            INTO qt_existe_w
            FROM dual;
              
        IF (qt_existe_w = 1 AND :new.nm_usuario <> 'INTEGR_TASY') THEN 
            SELECT nr_prontuario,
                    ie_funcionario,
                    nr_cpf,
                    nr_seq_conselho_w
            INTO nr_prontuario_w, ie_funcionario_w, nr_cpf_w, nr_seq_conselho_w
            FROM pessoa_fisica
            WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica;
    
            reg_integracao_w.ie_operacao := 'A';
            reg_integracao_w.nr_prontuario := nvl(nr_prontuario_w, :new.nr_prontuario);
            reg_integracao_w.cd_pessoa_fisica := :new.cd_pessoa_fisica;
            reg_integracao_w.ie_funcionario := nvl(ie_funcionario_w, 'N');
            reg_integracao_w.nr_seq_conselho := nr_seq_conselho_w;
            
            SELECT decode(COUNT(*), 0, 'N', 'S')
            INTO reg_integracao_w.ie_pessoa_atend
            FROM pessoa_fisica_aux
            WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica
                  AND nr_primeiro_atend IS NOT NULL;
    
            IF (nr_cpf_w IS NOT NULL) 
                THEN reg_integracao_w.ie_possui_cpf := 'S';
            END IF;
            
            IF (wheb_usuario_pck.get_ie_lote_contabil = 'N') THEN 
                gerar_int_padrao.gravar_integracao(ie_evento_p => '12', nr_seq_documento_p => :new.cd_pessoa_fisica, nm_usuario_p => :new.nm_usuario, reg_integracao_p => reg_integracao_w);
            END IF;
    
        END IF;
    END IF;
END;
/
