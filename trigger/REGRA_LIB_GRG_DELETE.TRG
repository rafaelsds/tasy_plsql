CREATE OR REPLACE TRIGGER regra_lib_grg_delete
before delete ON regra_lib_grg
FOR EACH ROW
DECLARE

qt_regra_vinculada_w number(10) := 0;
BEGIN

select	count(*)
into	qt_regra_vinculada_w
from	LOTE_AUDIT_HIST_LIB 
where	NR_SEQ_REGRA_LIB = :OLD.nr_sequencia;

if	(qt_regra_vinculada_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(904024);
end if;



END;
/
