CREATE OR REPLACE TRIGGER Prescr_proc_hor_Insert
BEFORE INSERT ON Prescr_proc_hor
FOR EACH ROW

DECLARE

BEGIN

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

END;
/
