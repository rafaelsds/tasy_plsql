create or replace trigger material_brasindice_after
after insert or update or delete on material_brasindice
for each row

declare

ds_retorno_integracao_w clob;
event_w					varchar2(30);
event_class_w			varchar2(100);
nr_sequencia_w 			material_brasindice.nr_sequencia%type;
qt_retorno_w        varchar2(1);

begin

select	obter_valor_param_usuario(9041, 
                                  10, 
                                  obter_perfil_ativo, 
                                  wheb_usuario_pck.get_nm_usuario(), 
                                  obter_estabelecimento_ativo())
into	qt_retorno_w
from	dual; 

if (qt_retorno_w = 'S')then
  /*Quando Incluir, alterar e excluir as informa��es da material_brasindice chama a integra��o padr�o*/
  if	(inserting) then
    nr_sequencia_w			:= :new.nr_sequencia;
    event_w					:= 'materialbrasindice.added'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialbrasindice.outbound.MaterialBrasindiceAddedCallback'; -- event class created in tasy-interfaces(material module).
  elsif	(updating) then
    nr_sequencia_w			:= :new.nr_sequencia;
    event_w					:= 'materialbrasindice.updated'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialbrasindice.outbound.MaterialBrasindiceUpdatedCallback'; -- event class created in tasy-interfaces(material module).
  else
    nr_sequencia_w			:= :old.nr_sequencia;
    event_w					:= 'materialbrasindice.deleted'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialbrasindice.outbound.MaterialBrasindiceDeletedCallback'; -- event class created in tasy-interfaces(material module).
  end if;
  
  SELECT BIFROST.SEND_INTEGRATION(
      event_w,
      event_class_w,
      '{"code" : '||nr_sequencia_w||'}',
      'integration')
  INTO ds_retorno_integracao_w
  FROM dual;
end if;

end;
/
