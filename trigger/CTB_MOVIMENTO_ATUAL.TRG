create or replace trigger ctb_movimento_atual
before insert or update or delete on ctb_movimento
for each row
declare

nr_seq_conta_ans_cred_w         number(10);
nr_seq_conta_ans_deb_w          number(10);
nr_versao_w                                     number(10);
qtd_reg_w                                       number(10);
cd_empresa_w                    ctb_mes_ref.cd_empresa%type;

begin
if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then
        if      (philips_contabil_pck.get_ie_consistindo_lote = 'N') then
                if      (philips_contabil_pck.get_ie_integrando_lote = 'N') then
                        if      (:new.nr_lote_contabil is not null) and
                                (:new.nm_usuario <> wheb_mensagem_pck.get_texto(798482)) then
                                update  lote_contabil
                                set     dt_consistencia  = null
                                where   nr_lote_contabil = :new.nr_lote_contabil;
                        end if;
                end if;

                if      (:new.nr_lote_contabil is not null) and
                        ((inserting) or (updating)) then

                        if      (nvl(:new.ie_status_origem, '0') <> 'SO' ) and
                                (nvl(:new.cd_conta_credito,'0') = '0') and
                                (nvl(:new.cd_conta_debito,'0') = '0') then
                                wheb_mensagem_pck.exibir_mensagem_abort(225337,'CD_CONTA_CREDITO_NOVA=' || :new.cd_conta_credito || ';CD_CONTA_DEBITO_NOVA=' || :new.cd_conta_debito || ';CD_CONTA_CREDITO_ANT=' || :old.cd_conta_credito|| ';CD_CONTA_DEBITO_ANT=' || :old.cd_conta_debito);
                        end if;

                        select  nvl(max(nr_sequencia),0)
                        into    nr_versao_w
                        from    ctb_versao_plano_ans
                        where   :new.dt_movimento between dt_inicio_vigencia and nvl(dt_fim_vigencia, sysdate);

                        begin
                        select  cd_empresa
                        into    cd_empresa_w
                        from    ctb_mes_ref
                        where   nr_sequencia = :new.nr_seq_mes_ref;
                        exception when others then
                                if      (:new.cd_estabelecimento is not null) then
                                        cd_empresa_w    := obter_empresa_estab(:new.cd_estabelecimento);
                                elsif   (nvl(wheb_usuario_pck.get_cd_estabelecimento,0) != 0) then
                                        cd_empresa_w    := obter_empresa_estab(wheb_usuario_pck.get_cd_estabelecimento);
                                end if;
                        end;

                        PHILIPS_CONTABIL_PCK.valida_se_dia_fechado(cd_empresa_w, :new.dt_movimento);

                        if      (nr_versao_w > 0) then
                                select  count(*)
                                into    qtd_reg_w
                                from    conta_contabil_ans c,
                                        ctb_plano_ans p
                                where   p.nr_sequencia          = c.nr_seq_conta_ans
                                and     p.nr_seq_versao_plano   = nr_versao_w
                                and     ((c.cd_conta_contabil   = :new.cd_conta_debito) or
                                        (c.cd_conta_contabil    = :new.cd_conta_credito));

                                if      (qtd_reg_w > 0) then
                                        /* pega a sequencia do plano ans para gravar a conta debito*/
                                        select  nvl(max(c.nr_seq_conta_ans),0)
                                        into    nr_seq_conta_ans_deb_w
                                        from    conta_contabil_ans c,
                                                        ctb_plano_ans p
                                        where   p.nr_sequencia  = c.nr_seq_conta_ans
                                        and             c.cd_conta_contabil = :new.cd_conta_debito
                                        and             p.nr_seq_versao_plano   =  nr_versao_w;

                                        if      (nr_seq_conta_ans_deb_w <> 0) then
                                                :new.nr_seq_conta_ans_deb := nr_seq_conta_ans_deb_w;
                                        end if;

                                        /* pega a sequencia do plano ans para gravar a conta credito*/
                                        select  nvl(max(c.nr_seq_conta_ans),0)
                                        into    nr_seq_conta_ans_cred_w
                                        from    conta_contabil_ans c,
                                                ctb_plano_ans p
                                        where   p.nr_sequencia = c.nr_seq_conta_ans
                                        and     c.cd_conta_contabil = :new.cd_conta_credito
                                        and     p.nr_seq_versao_plano = nr_versao_w;

                                        if      (nr_seq_conta_ans_cred_w <> 0) then
                                                :new.nr_seq_conta_ans_cred := nr_seq_conta_ans_cred_w;
                                        end if;
                                end if;
                        end if;
                end if;

                begin
                if      (deleting) and
                        (:old.nr_lote_contabil is not null) and
                        (nvl(:old.nr_seq_regra_lanc,0) <> 0)then
                        update  ctb_regra_movto_prog
                        set     vl_saldo = vl_saldo + :old.vl_movimento
                        where   nr_sequencia = :old.nr_seq_regra_lanc;
                end if;
                exception when others then
                        null;
                end;
        end if;
end if;
end;
/