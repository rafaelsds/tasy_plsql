CREATE OR REPLACE TRIGGER privacy_notice_customer_before before
  INSERT OR
  UPDATE ON privacy_notice_customer FOR EACH row DECLARE qt_blocker_w NUMBER(1);
  qt_nr_sequencia_w  NUMBER(1);
  NR_SEQ_OUT_COMUNIC NUMBER(10);
  pragma autonomous_transaction;
  BEGIN
    IF (:new.nr_sequencia >0 AND :new.dt_initial_vigencia <= :new.dt_final_vigencia) THEN
      SELECT NVL(MAX(1),0)
      INTO qt_blocker_w
      FROM PRIVACY_NOTICE_CUSTOMER
      WHERE nr_sequencia          <> :new.nr_sequencia
      AND ie_situacao              = 'A'
      AND (trunc(:new.dt_final_vigencia) < trunc(SYSDATE)
      OR trunc(:new.dt_initial_vigencia) < trunc(SYSDATE)
      OR :new.dt_initial_vigencia BETWEEN DT_INITIAL_VIGENCIA AND DT_FINAL_VIGENCIA
      OR :new.dt_final_vigencia BETWEEN DT_INITIAL_VIGENCIA AND DT_FINAL_VIGENCIA);
    ELSIF (:new.dt_initial_vigencia <= :new.dt_final_vigencia) THEN
      SELECT NVL(MAX(1),0)
      INTO qt_blocker_w
      FROM PRIVACY_NOTICE_CUSTOMER
      WHERE trunc(:new.dt_initial_vigencia) <= trunc(SYSDATE)
      OR trunc(:new.dt_final_vigencia)      <= trunc(SYSDATE)
      OR :new.dt_initial_vigencia BETWEEN DT_INITIAL_VIGENCIA AND DT_FINAL_VIGENCIA
      OR :new.dt_final_vigencia BETWEEN DT_INITIAL_VIGENCIA AND DT_FINAL_VIGENCIA
      AND ie_situacao = 'A';
    ELSE
      qt_blocker_w:= 1;
    END IF;
    IF (qt_blocker_w = 1) THEN
      wheb_mensagem_pck.exibir_mensagem_abort(1062913);
    END IF;
--used in delphi/java
if (:new.ds_privacidade_varchar is not null) then
	if ((INSTR(upper(:new.ds_privacidade_varchar), '.PDF', -1)) = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1118334);
	end if;
end if;
    IF ((:NEW.ie_situacao   = 'I') AND (:OLD.ie_situacao IS NOT NULL)) THEN
      :NEW.IE_NOTIFICATION := 'N';
    END IF;
    IF ((qt_blocker_w = 0) AND (:NEW.IE_NOTIFICATION <> NVL(:OLD.IE_NOTIFICATION,'X'))) THEN
      INSERT_COMUNIC_PRIVACY_NOTICE(:NEW.DT_INITIAL_VIGENCIA, :NEW.nm_usuario, :NEW.IE_NOTIFICATION, :OLD.NR_SEQUENCIA_IC, NR_SEQ_OUT_COMUNIC);
      :new.NR_SEQUENCIA_IC := NR_SEQ_OUT_COMUNIC;
    END IF;
    COMMIT;
  END privacy_notice_customer_before;
/