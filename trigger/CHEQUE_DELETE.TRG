create or replace trigger CHEQUE_DELETE
before delete on CHEQUE 
for each row

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_sequencia,null,'CP',nvl(:old.dt_prev_pagamento,:old.dt_vencimento),'E',:old.nm_usuario);
end if;

end;
/
