create or replace trigger phi_gerencia_wheb_before
before insert or update or delete on gerencia_wheb
for each row

declare

ds_quebra_linha_w	varchar2(10) := chr(13) || chr(10);
ds_grupo_w		varchar2(255);

Cursor c01 (nr_seq_gerencia_p number) is
	select	a.nr_sequencia,
		substr(obter_desc_expressao(a.cd_exp_grupo,a.ds_grupo),1,255) ds_grupo
	from	grupo_desenvolvimento a
	where	a.nr_seq_gerencia = nr_seq_gerencia_p
	and	a.ie_situacao = 'A';

Cursor c02 (nr_seq_gerencia_p number) is
	select	a.nr_sequencia,
		substr(obter_desc_expressao(a.cd_exp_grupo,a.ds_grupo),1,255) ds_grupo
	from	grupo_suporte a
	where	a.nr_seq_gerencia_sup = nr_seq_gerencia_p
	and	a.ie_situacao = 'A';

Cursor c03 (nr_seq_gerencia_p number) is
	select	a.nr_sequencia,
		substr(obter_desc_expressao(a.cd_exp_grupo,a.ds_grupo),1,255) ds_grupo
	from	gerencia_wheb_grupo a
	where	a.nr_seq_gerencia = nr_seq_gerencia_p
	and	a.ie_situacao = 'A'
	and not exists (select	1
			from	grupo_desenvolvimento x
			where	x.nr_seq_grupo_gerencia = a.nr_sequencia)
	and not exists (select	1
			from	grupo_suporte x
			where	x.nr_seq_grupo_gerencia = a.nr_sequencia);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	null;
end if;

if	(updating) and
	(:new.ie_situacao <> :old.ie_situacao) and
	(:new.ie_situacao = 'I') then
	begin
	for c01_w in c01 (:new.nr_sequencia) loop
		begin
		ds_grupo_w := c01_w.ds_grupo;

		update	grupo_desenvolvimento
		set	dt_atualizacao = sysdate,
			nm_usuario = :new.nm_usuario,
			ie_situacao = 'I'
		where	nr_sequencia = c01_w.nr_sequencia;
		end;
	end loop;

	for c02_w in c02 (:new.nr_sequencia) loop
		begin
		ds_grupo_w := c02_w.ds_grupo;

		update	grupo_suporte
		set	dt_atualizacao = sysdate,
			nm_usuario = :new.nm_usuario,
			ie_situacao = 'I'
		where	nr_sequencia = c02_w.nr_sequencia;
		end;
	end loop;

	for c03_w in c03 (:new.nr_sequencia) loop
		begin
		ds_grupo_w := c03_w.ds_grupo;

		update	gerencia_wheb_grupo
		set	dt_atualizacao = sysdate,
			nm_usuario = :new.nm_usuario,
			ie_situacao = 'I'
		where	nr_sequencia = c03_w.nr_sequencia;
		end;
	end loop;
	exception
	when others then --Cannot deactivate this record.
		if	(sqlcode = -20011) then
			wheb_mensagem_pck.exibir_mensagem_abort(obter_desc_expressao(1058685) || ds_quebra_linha_w || ds_quebra_linha_w || obter_desc_expressao(330233) || ' ' || ds_grupo_w || ds_quebra_linha_w || dbms_utility.format_error_stack);
		else
			wheb_mensagem_pck.exibir_mensagem_abort(1174775);
		end if;
	end;
end if;

if	(deleting) then
	begin
	delete from grupo_desenvolvimento where nr_seq_gerencia = :old.nr_sequencia;
	delete from grupo_suporte where nr_seq_gerencia_sup = :old.nr_sequencia;
	delete from gerencia_wheb_grupo where nr_seq_gerencia = :old.nr_sequencia;
	exception
	when others then --Cannot delete this record as there are dependent records.
		wheb_mensagem_pck.exibir_mensagem_abort(1174776);
	end;
end if;

end;
/