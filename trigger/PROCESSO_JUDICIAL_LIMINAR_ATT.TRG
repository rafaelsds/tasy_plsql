create or replace trigger processo_judicial_liminar_att
before insert or update on processo_judicial_liminar
for each row
declare
qt_registro_w	pls_integer;
begin

if	(:new.ie_impacto_reajuste = 'S') and
	(:new.nr_seq_contrato is not null) and
	(1 = 2)then
	wheb_mensagem_pck.exibir_mensagem_abort(1033848); --N�o � poss�vel informar contrato em processo com impacto no reajuste de pre�o!
end if;

end;
/
