create or replace trigger ATEND_SUMARIO_ALTA_ATUAL
before insert or update on ATEND_SUMARIO_ALTA
for each row
declare

nr_seq_mhr_doc_w			mhr_doc_meta.nr_sequencia%TYPE;
patient_code_w			mhr_doc_meta.cd_pessoa_fisica%TYPE;



begin
if	(nvl(:old.DT_ALTA,sysdate+10) <> :new.DT_ALTA) and
	(:new.DT_ALTA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ALTA, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/
