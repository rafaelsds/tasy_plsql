create or replace trigger pls_analise_conta_atual
before insert or update on pls_analise_conta
for each row

declare
qt_conta_fechada_w	pls_integer;
ie_status_lote_w	pls_lote_protocolo_conta.ie_status%type;
ds_observacao_w		varchar2(4000);
qt_registro_w		pls_integer;

begin

--Se an�lise estava cancelada e por alguma raz�o teve seu status alterado
if ( ( nvl(:new.ie_status,'X')) <> 'C' and :old.ie_status = 'C') then



	ds_observacao_w := ' Status da an�lise era '||:old.ie_status||' e foi alterado para: '||:new.ie_status;
	


	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	ds_observacao_w ,sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);

end if;

if	((:new.ie_status = 'T') and (:old.ie_status	!= 'T')) and
	(:new.dt_final_analise	is null) then
	begin
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'Sem data Final ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
	end;
end if;

if	((:new.ie_status = 'T') and (:old.ie_status	!= 'T')) then
	begin
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'An�lise encerrada ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
	end;
end if;

select	count(1)
into	qt_conta_fechada_w
from	pls_conta
where	nr_seq_analise = :new.nr_sequencia
and	ie_status = 'F';

if	(:new.ie_status != 'T') and (qt_conta_fechada_w > 0) and
	(:new.ie_status <> :old.ie_status) then
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'An�lise aberta com contas fechadas ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
end if;

if	(:new.ie_status = 'T') and (:new.ie_pre_analise = 'S') and (:old.ie_pre_analise != 'S') then
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'Retorno da an�lise para pr�-an�lise ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
end if;

select	max(ie_status)
into	ie_status_lote_w
from	pls_lote_protocolo_conta
where	nr_sequencia = :new.nr_seq_lote_protocolo;

-- Caso a an�lise entre em um lote "Aguardando guia" mas a an�lise n�o esteja com esse status.
if	(:new.ie_status <> :old.ie_status) and
	(:new.ie_status <> 'X') and (ie_status_lote_w = 'G') then
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'An�lise com status diferente de Aguardando guia em lote Aguardando guia ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
end if;

select 	count(1) 
into	qt_registro_w
from 	pls_cta_analise_cons c
where 	c.ie_status != 'F';

if	(qt_registro_w > 0) and
	(:old.ie_status = 'D') and 
	(:new.ie_status != 'D') then
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'An�lise pendente de processamento com status liberado para auditoria ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
end if;

-- somente atualizacao
if	(updating) then
	if	((:new.ie_status != 'T') and (:old.ie_status	= 'T')) then
		
		insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
							ie_tipo_historico, ie_tipo_item, nm_usuario,
							nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
							nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
							nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
							nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
							ds_call_stack)
					values 	(	'An�lise reaberta ',sysdate,sysdate,
							'24',null,:new.nm_usuario,
							:new.nm_usuario,:new.nr_sequencia,null,
							null,null,null,
							null, null,null,null,
							null,null,pls_hist_analise_conta_seq.nextval,
							dbms_utility.format_call_stack);

	end if;
end if;
end;
/
