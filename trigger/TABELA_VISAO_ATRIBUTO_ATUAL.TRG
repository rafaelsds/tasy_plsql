CREATE OR REPLACE TRIGGER TABELA_VISAO_ATRIBUTO_ATUAL 
BEFORE INSERT OR UPDATE ON TABELA_VISAO_ATRIBUTO 
FOR EACH ROW
DECLARE 
ds_user_w varchar2(100);
nm_tabela_w		varchar2(255); 
nr_sequencia_w		number(10); 
ie_obrigatorio_w		varchar2(1); 
ds_expressao_br_w		DIC_EXPRESSAO.ds_expressao_br%type; 
cd_expressao_w		DIC_EXPRESSAO.cd_expressao%type; 
nm_coluna_desc_w		varchar2(50); 
nm_coluna_exp_w		varchar2(50); 
 
 
BEGIN 
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then
	 
	if	((inserting or updating) and :new.ie_obrigatorio = 'N') then 
		 
		select	a.nm_tabela 
		into	nm_tabela_w 
		from	tabela_visao a 
		where	a.nr_sequencia	= :new.nr_sequencia; 
	 
	 /*
		select	a.ie_obrigatorio 
		into	ie_obrigatorio_w 
		from	tabela_atributo a 
		where	a.nm_tabela	= nm_tabela_w 
		and	a.nm_atributo	= :new.nm_atributo; 
	 
		if	(ie_obrigatorio_w = 'S') then 
			Wheb_mensagem_pck.exibir_mensagem_abort(289891); 
		end if;*/
		 
	end if; 
	 
	 
	begin 
		if	(:new.cd_exp_label is not null or 
			:old.cd_exp_label is not null) then 
			 
			nm_coluna_desc_w		:= 'DS_LABEL'; 
			nm_coluna_exp_w		:= 'CD_EXP_LABEL'; 
			cd_expressao_w		:= :new.cd_exp_label; 
			 
			ds_expressao_br_w		:= obter_desc_expressao_br(cd_expressao_w); 
			:new.ds_label 		:= ds_expressao_br_w; 
		end if; 
	 
		if	(:new.cd_exp_label_grid is not null or 
			:old.cd_exp_label_grid is not null) then 
			 
			nm_coluna_desc_w		:= 'DS_LABEL_GRID'; 
			nm_coluna_exp_w		:= 'CD_EXP_LABEL_GRID'; 
			cd_expressao_w		:= :new.cd_exp_label_grid; 
			 
			ds_expressao_br_w		:= obter_desc_expressao_br(cd_expressao_w); 
			:new.ds_label_grid		:= ds_expressao_br_w; 
		end if; 
		 
		if	(:new.ie_componente = 'rg' and( :new.cd_exp_valores is not null or 
							:old.cd_exp_valores is not null)) then 
			 
			nm_coluna_desc_w		:= 'DS_VALORES'; 
			nm_coluna_exp_w		:= 'CD_EXP_VALORES'; 
			cd_expressao_w		:= :new.cd_exp_valores; 
			 
			ds_expressao_br_w		:= obter_desc_expressao_br(cd_expressao_w);					 
			:new.ds_valores		:= ds_expressao_br_w; 
		end if; 
		 
		if	(:new.cd_exp_label_longo is not null or 
			 :old.cd_exp_label_longo is not null) then 
			 
			nm_coluna_desc_w		:= 'DS_LABEL_LONGO'; 
			nm_coluna_exp_w		:= 'CD_EXP_LABEL_LONGO'; 
			 
			cd_expressao_w		:= :new.cd_exp_label_longo; 
			 
			ds_expressao_br_w		:= obter_desc_expressao_br(cd_expressao_w); 
			:new.ds_label_longo	:= ds_expressao_br_w; 
		end if; 
	exception when others then 
		-- Caso a expressao nao couber no campo, avisa usu�rio 
		if	(sqlcode = -6502) then 
			wheb_mensagem_pck.exibir_mensagem_abort(290787, 'CD_EXPRESSAO_P='||cd_expressao_w|| 
									';DS_ATRIBUTO_P='||nm_coluna_desc_w|| 
									';NM_ATRIBUTO_EXP_P='||nm_coluna_exp_w); 
		end if; 
	end; 
	 
	 
 
:new.dt_atualizacao := sysdate; 
end if; 
end TABELA_VISAO_ATRIBUTO_ATUAL;
/	