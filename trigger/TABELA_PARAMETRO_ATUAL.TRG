create or replace trigger tabela_parametro_atual
before insert or update on tabela_parametro
for each row

declare

cd_tabela_param_new_w	number(5);

begin
select	max(x.cd_tabela_custo)
into	cd_tabela_param_new_w
from	tabela_custo x
where	x.nr_sequencia = :new.nr_seq_tabela_param;

if	(nvl(cd_tabela_param_new_w,0) <> nvl(:new.cd_tabela_parametro,0)) then
	:new.cd_tabela_parametro := cd_tabela_param_new_w;
end if;	

end;
/