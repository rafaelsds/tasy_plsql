CREATE OR REPLACE TRIGGER ag_lista_espera_estagio_Atual
BEFORE INSERT OR UPDATE ON AG_LISTA_ESPERA_ESTAGIO
FOR EACH ROW 

DECLARE
qt_reg_w	number(10);
pragma autonomous_transaction;	

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

SELECT	count(*)
into	qt_reg_w
from	ag_lista_espera_estagio
where	nr_sequencia <> :new.nr_sequencia
and		ie_acao = :new.ie_acao;

if (qt_reg_w > 0) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(1040663);
end if;

<<Final>>
qt_reg_w := 0;

END;
/