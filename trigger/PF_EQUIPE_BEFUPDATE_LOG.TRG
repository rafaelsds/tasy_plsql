create or replace trigger pf_equipe_befupdate_log
before update on pf_equipe
for each row

declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into pf_equipe_log(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_equipe,
		nr_seq_motivo_parada,
		cd_pessoa_fisica,
		ie_almoco,
		ie_situacao,
		ie_exibir_pep,
		ie_exibir_regulacao,
		nr_seq_tipo_equipe)
	values( pf_equipe_log_seq.NextVal,
		:old.cd_estabelecimento,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:old.ds_equipe,
		:old.nr_seq_motivo_parada,
		:new.cd_pessoa_fisica,
		:old.ie_almoco,
		:old.ie_situacao,
		:old.ie_exibir_pep,
		:old.ie_exibir_regulacao,
		:old.nr_seq_tipo_equipe);
end if;

end;
/