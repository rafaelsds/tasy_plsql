create or replace trigger lab_macro_propria_update
before insert or update or delete on lab_macro_propria
for each row

begin

	if (regexp_like(:new.nm_macro, '[^[:alpha:]]')) then
		wheb_mensagem_pck.exibir_mensagem_abort(945475);
	end if;	

end;
/