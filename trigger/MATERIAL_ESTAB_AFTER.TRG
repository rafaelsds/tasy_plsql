create or replace trigger material_estab_after
after insert or update or delete on material_estab
for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_operacao_w			varchar2(1);
ie_material_estoque_w		varchar2(1);
ie_padronizado_w		varchar2(1);
cd_material_w			material.cd_material%type;
nm_usuario_w			usuario.nm_usuario%type;
cd_estabelecimento_w	material_estab.cd_estabelecimento%type;
ds_retorno_integracao_w clob;
event_w					varchar2(35);
event_class_w			varchar2(100);
ie_integrar_w			varchar2(1) := 'S';
qt_retorno_w        varchar2(1);

pragma autonomous_transaction;

begin

select	obter_valor_param_usuario(9041, 
                                  10, 
                                  obter_perfil_ativo, 
                                  wheb_usuario_pck.get_nm_usuario(), 
                                  obter_estabelecimento_ativo())
into	qt_retorno_w
from	dual; 

if (qt_retorno_w = 'S')then
  /* Inclus�o do if wheb_usuario_pck.get_ie_executar_trigger devido ao grande volume de dados  do cliente RedeDor, ao dar d isable e e nable na  trigger , causava  extrema lentid�o OS 1945059*/
  
  if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
    /*Quando Incluir, alterar e excluir as informa��es da material,_estab chama a integra��o padr�o*/
    if	(inserting) then
      ie_operacao_w			:= 'A';
      ie_material_estoque_w		:= :new.ie_material_estoque;
      ie_padronizado_w		:= :new.ie_padronizado;
      cd_material_w			:= :new.cd_material;
      nm_usuario_w			:= :new.nm_usuario;
      cd_estabelecimento_w	:= :new.cd_estabelecimento;
      event_w					:= 'establishmentmaterial.added'; -- event register in bifrost
      event_class_w			:= 'com.philips.tasy.integration.establishmentmaterial.outbound.EstablishmentMaterialAddedCallback'; -- event class created in tasy-interfaces(material module).
      ie_integrar_w			:= 'S';
    elsif	(updating) then	
      ie_operacao_w			:= 'A';
      ie_material_estoque_w		:= :new.ie_material_estoque;
      ie_padronizado_w		:= :new.ie_padronizado	;
      cd_material_w			:= :new.cd_material;
      nm_usuario_w			:= :new.nm_usuario;
      cd_estabelecimento_w	:= :new.cd_estabelecimento;
      event_w					:= 'establishmentmaterial.updated'; -- event register in bifrost
      event_class_w			:= 'com.philips.tasy.integration.establishmentmaterial.outbound.EstablishmentMaterialUpdatedCallback'; -- event class created in tasy-interfaces(material module).
      ie_integrar_w			:= 'S';
      if	(:new.dt_atual_consumo <> :old.dt_atual_consumo) then
        ie_integrar_w			:= 'N';
      end if;	
    else
      ie_operacao_w			:= 'A';
      ie_material_estoque_w		:= :old.ie_material_estoque;
      ie_padronizado_w		:= :old.ie_padronizado;
      cd_material_w			:= :old.cd_material;
      nm_usuario_w			:= :old.nm_usuario;
      cd_estabelecimento_w	:= :old.cd_estabelecimento;
      event_w					:= 'establishmentmaterial.deleted'; -- event register in bifrost
      event_class_w			:= 'com.philips.tasy.integration.establishmentmaterial.outbound.EstablishmentMaterialDeletedCallback'; -- event class created in tasy-interfaces(material module).
      if	(:new.dt_atual_consumo <> :old.dt_atual_consumo) then
        ie_integrar_w			:= 'N';
      end if;
    end if;
  
    reg_integracao_p.ie_operacao		:=	ie_operacao_w;
    reg_integracao_p.ie_material_estoque	:=	ie_material_estoque_w;
    reg_integracao_p.ie_padronizado		:=	ie_padronizado_w;
  
    if	(ie_integrar_w = 'S') then
      reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
      gerar_int_padrao.gravar_integracao('1', cd_material_w,nm_usuario_w,reg_integracao_p);
    end if;
  
    SELECT BIFROST.SEND_INTEGRATION(
        event_w,
        event_class_w,
        '{"material" : '||cd_material_w||', "establishment":' || cd_estabelecimento_w||'}',
        'integration')
    INTO ds_retorno_integracao_w
    FROM dual;
  
    commit;
    
  end if;	
end if;
end;
/
