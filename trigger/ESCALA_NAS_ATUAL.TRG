create or replace TRIGGER escala_nas_atual
before insert or update on escala_nas
for each row
declare

	qt_pontuacao_w	number(15,2) := 0;
	sql_w           varchar2(300);
	qt_reg_w	number(1);
	cd_setor_atendimento_w	number(10);
  ds_erro_w   varchar2(2000);
  ds_parametro_w  varchar2(2000);
begin
  if (:new.nr_hora is null) or (:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then 
	begin  
	  :new.nr_hora := to_number(to_char(round(:new.DT_AVALIACAO, 'hh24'), 'hh24'));
	end; 
  end if; 
  if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
    goto Final;
  end if;

  /** Inicio Medical Device **/
  begin
	  sql_w := 'call obter_score_nas_md(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16,
										:17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30,
										:31, :32) into :qt_pontuacao_w';
										
	  execute immediate sql_w using in :new.ds_item_1a, in :new.ds_item_1b, in :new.ds_item_1c, in :new.ds_item_2,
									in :new.ds_item_3, in :new.ds_item_4a, in :new.ds_item_4b, in :new.ds_item_4c,
									in :new.ds_item_5, in :new.ds_item_6a, in :new.ds_item_6b, in :new.ds_item_6c, 
									in :new.ds_item_7a, in :new.ds_item_7b, in :new.ds_item_8a, in :new.ds_item_8b,
									in :new.ds_item_8c, in :new.ds_item_9, in :new.ds_item_10, in :new.ds_item_11,
									in :new.ds_item_12, in :new.ds_item_13, in :new.ds_item_14, in :new.ds_item_15,
									in :new.ds_item_16, in :new.ds_item_17, in :new.ds_item_18, in :new.ds_item_19,
									in :new.ds_item_20, in :new.ds_item_21, in :new.ds_item_22, in :new.ds_item_23,
									out qt_pontuacao_w;
  exception
    when others then
      qt_pontuacao_w := null;
      ds_erro_w := sqlerrm;
      ds_parametro_w := ':new.ds_item_1a: ' || :new.ds_item_1a || ' - :new.ds_item_1b: ' || :new.ds_item_1b
      || ' - :new.ds_item_1c: ' || :new.ds_item_1c || ' - :new.ds_item_2: ' || :new.ds_item_2
      || ' - :new.ds_item_3: ' || :new.ds_item_3 || ' - :new.ds_item_4a: ' || :new.ds_item_4a
      || ' - :new.ds_item_4b: ' || :new.ds_item_4b || ' - :new.ds_item_4c: ' || :new.ds_item_4c
      || ' - :new.ds_item_5: ' || :new.ds_item_5 || ' - :new.ds_item_6a: ' || :new.ds_item_6a
      || ' - :new.ds_item_6b: ' || :new.ds_item_6b || ' - :new.ds_item_6c: ' || :new.ds_item_6c
      || ' - :new.ds_item_7a: ' || :new.ds_item_7a || ' - :new.ds_item_7b: ' || :new.ds_item_7b
      || ' - :new.ds_item_8a: ' || :new.ds_item_8a || ' - :new.ds_item_8b: ' || :new.ds_item_8b
      || ' - :new.ds_item_8c: ' || :new.ds_item_8c || ' - :new.ds_item_9: ' || :new.ds_item_9
      || ' - :new.ds_item_10: ' || :new.ds_item_10 || ' - :new.ds_item_11: ' || :new.ds_item_11
      || ' - :new.ds_item_12: ' || :new.ds_item_12 || ' - :new.ds_item_13: ' || :new.ds_item_13
      || ' - :new.ds_item_14: ' || :new.ds_item_14 || ' - :new.ds_item_15: ' || :new.ds_item_15
      || ' - :new.ds_item_16: ' || :new.ds_item_16 || ' - :new.ds_item_17: ' || :new.ds_item_17
      || ' - :new.ds_item_18: ' || :new.ds_item_18 || ' - :new.ds_item_19: ' || :new.ds_item_19
      || ' - :new.ds_item_20: ' || :new.ds_item_20 || ' - :new.ds_item_21: ' || :new.ds_item_21
      || ' - :new.ds_item_22: ' || :new.ds_item_22 || ' - :new.ds_item_23: ' || :new.ds_item_23
      || ' - qt_pontuacao_w: ' || qt_pontuacao_w;
      gravar_log_medical_device('ESCALA_NAS_ATUAL', 'OBTER_SCORE_NAS_MD', ds_parametro_w, ds_erro_w, wheb_usuario_pck.get_nm_usuario, 'N');
  end;
  
  :new.qt_pontuacao := qt_pontuacao_w;
  
  /** fim Medical Device **/
					 
  if (:new.nr_atendimento	is not null) then
	begin
	cd_setor_atendimento_w	:= obter_setor_atendimento(:new.nr_atendimento);

	exception
	when others then
		cd_setor_atendimento_w	:= 0;
	end;
	
	if	(cd_setor_atendimento_w	is not null) and
		(cd_setor_atendimento_w	> 0) then
		:new.cd_setor_atendimento := cd_setor_atendimento_w;
	end if;

end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	send_nas_integration(:new.nr_atendimento, :new.nr_sequencia, :new.dt_atualizacao, :new.dt_avaliacao,
					:new.dt_liberacao, :new.ie_situacao, :new.ds_item_1a, :new.ds_item_1b,
					:new.ds_item_1c, :new.ds_item_2, :new.ds_item_3, :new.ds_item_4a, :new.ds_item_4b,
					:new.ds_item_4c, :new.ds_item_5, :new.ds_item_6a, :new.ds_item_6b, :new.ds_item_6c,
					:new.ds_item_7a, :new.ds_item_7b, :new.ds_item_8a, :new.ds_item_8b, :new.ds_item_8c,
					:new.ds_item_9, :new.ds_item_10, :new.ds_item_11, :new.ds_item_12, :new.ds_item_13,
					:new.ds_item_14, :new.ds_item_15, :new.ds_item_16, :new.ds_item_17, :new.ds_item_18,
					:new.ds_item_19, :new.ds_item_20, :new.ds_item_21, :new.ds_item_22, :new.ds_item_23);	
end if;

if	(:old.dt_inativacao is null) and
	(:new.dt_inativacao is not null) then
	send_nas_integration(:new.nr_atendimento, :new.nr_sequencia, :new.dt_atualizacao, :new.dt_avaliacao,
					:new.dt_liberacao, :new.ie_situacao, :new.ds_item_1a, :new.ds_item_1b,
					:new.ds_item_1c, :new.ds_item_2, :new.ds_item_3, :new.ds_item_4a, :new.ds_item_4b,
					:new.ds_item_4c, :new.ds_item_5, :new.ds_item_6a, :new.ds_item_6b, :new.ds_item_6c,
					:new.ds_item_7a, :new.ds_item_7b, :new.ds_item_8a, :new.ds_item_8b, :new.ds_item_8c,
					:new.ds_item_9, :new.ds_item_10, :new.ds_item_11, :new.ds_item_12, :new.ds_item_13,
					:new.ds_item_14, :new.ds_item_15, :new.ds_item_16, :new.ds_item_17, :new.ds_item_18,
					:new.ds_item_19, :new.ds_item_20, :new.ds_item_21, :new.ds_item_22, :new.ds_item_23);	
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/
