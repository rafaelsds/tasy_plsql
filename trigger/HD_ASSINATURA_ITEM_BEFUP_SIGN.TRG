create or replace trigger hd_assinatura_item_befup_sign
before update on hd_assinatura_item
for each row

begin

if 	(:old.nr_sequencia		<> :new.nr_sequencia
	or :old.dt_atualizacao		<> :new.dt_atualizacao
	or :old.nm_usuario		<> :new.nm_usuario
	or :old.dt_atualizacao_nrec	<> :new.dt_atualizacao_nrec
	or :old.nm_usuario_nrec		<> :new.nm_usuario_nrec
	or :old.nr_seq_assinatura	<> :new.nr_seq_assinatura
	or :old.nr_seq_evento		<> :new.nr_seq_evento
	or :old.nr_seq_concentrado	<> :new.nr_seq_concentrado
	or :old.nr_seq_concentrado_ret	<> :new.nr_seq_concentrado_ret
	or (:old.nr_sequencia		is null and	:new.nr_sequencia          	is not null)
	or (:old.dt_atualizacao		is null and	:new.dt_atualizacao        	is not null)
	or (:old.nm_usuario		is null and	:new.nm_usuario            	is not null)
	or (:old.dt_atualizacao_nrec	is null and	:new.dt_atualizacao_nrec   	is not null)
	or (:old.nm_usuario_nrec	is null and	:new.nm_usuario_nrec       	is not null)
	or (:old.nr_seq_assinatura	is null and	:new.nr_seq_assinatura     	is not null)
	or (:old.nr_seq_evento		is null and	:new.nr_seq_evento         	is not null)
	or (:old.nr_seq_concentrado	is null and	:new.nr_seq_concentrado    	is not null)
	or (:old.nr_seq_concentrado_ret	is null and	:new.nr_seq_concentrado_ret	is not null)
	or (:old.nr_sequencia		is not null and		:new.nr_sequencia          	is null)
	or (:old.dt_atualizacao		is not null and		:new.dt_atualizacao        	is null)
	or (:old.nm_usuario		is not null and		:new.nm_usuario            	is null)
	or (:old.dt_atualizacao_nrec	is not null and		:new.dt_atualizacao_nrec   	is null)
	or (:old.nm_usuario_nrec	is not null and		:new.nm_usuario_nrec       	is null)
	or (:old.nr_seq_assinatura	is not null and		:new.nr_seq_assinatura     	is null)
	or (:old.nr_seq_evento		is not null and		:new.nr_seq_evento         	is null)
	or (:old.nr_seq_concentrado	is not null and		:new.nr_seq_concentrado    	is null)
	or (:old.nr_seq_concentrado_ret	is not null and		:new.nr_seq_concentrado_ret	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/