CREATE OR REPLACE TRIGGER agenda_hc_paciente_Atual
BEFORE Insert or update ON agenda_hc_paciente
FOR EACH ROW
DECLARE

nr_controle_w			number(10);
ie_perm_dt_ret_w 		varchar2(1);
cd_estabelecimento_w	number(4);
ie_cons_pac_hc_w	varchar2(1);

begin
obter_param_usuario(867, 43, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_cons_pac_hc_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	select	Count(*)
	into	nr_controle_w
	from	Paciente_home_care
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	if	(nr_controle_w = 0) and
		(ie_cons_pac_hc_w = 'S') then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(262781);
	end if;
	
	obter_param_usuario(867, 6, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_perm_dt_ret_w);
		
	If	(ie_perm_dt_ret_w = 'N') and
	    (:new.dt_agenda < sysdate - 240/84600 ) then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(262780);
	end if;
		
end if;
END;
/
