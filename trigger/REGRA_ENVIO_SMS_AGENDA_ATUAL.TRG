create or replace trigger regra_envio_sms_agenda_atual
before insert or update on regra_envio_sms_agenda
for each row
declare

begin

if (:new.ie_classif_agenda is not null) then
	if (:new.ie_tipo_agendamento is not null) then
		if ((:new.ie_tipo_agendamento <> 'S') and (:new.ie_tipo_agendamento <> 'C')) then
			-- "A classifica��o � permitida somente para agendas de consulta e servi�o."
			Wheb_mensagem_pck.exibir_mensagem_abort(1045925);
		end if;
	end if;
end if;

end;
/