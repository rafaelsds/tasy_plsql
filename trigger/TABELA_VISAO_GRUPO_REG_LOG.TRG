create or replace trigger TABELA_VISAO_GRUPO_REG_LOG
before insert or update or delete on TABELA_VISAO_GRUPO
for each row

declare

operation_id_w	varchar2(1);
nr_sequencia_w	reg_object_log_os.nr_seq_documento%type;

begin

if	inserting then
	operation_id_w 	:= 'I';
	nr_sequencia_w	:= :new.nr_sequencia;
elsif	updating then
	operation_id_w := 'U';
	nr_sequencia_w	:= :new.nr_sequencia;
elsif	deleting then
	operation_id_w := 'D';
	nr_sequencia_w	:= :old.nr_sequencia;
end if;

GENERATE_REG_OBJECT_LOG_OS(wheb_usuario_pck.get_nm_usuario, 
			operation_id_w,
			'TABELA_VISAO_GRUPO',
			nr_sequencia_w);

end;
/
