create or replace
trigger pessoa_fisica_update_hl7
after update on pessoa_fisica
for each row

declare
ds_sep_bv_w				varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
ie_integrado_w			varchar2(1);
ie_existe_atend_w		number(10);
qt_atendimento_w		number(10);
ie_medico_w				varchar2(1);
ds_guampa_w		varchar2(10)	:= chr(39);
ie_condition_w varchar2(1) := 'N';
	
	PROCEDURE INTEGRA_BIFROST IS 
	  JSON_ADT_A08_W			PHILIPS_JSON;
	  DS_EVENT_W				VARCHAR2(100) := 'patient.changeInformation_ADT_A08';
	  DS_MESSAGE_RESPONSE_W	CLOB;
	BEGIN
	  JSON_ADT_A08_W := PHILIPS_JSON();
	  JSON_ADT_A08_W.PUT('CD_PESSOA_FISICA', :NEW.CD_PESSOA_FISICA);
	  JSON_ADT_A08_W.PUT('NM_PESSOA_FISICA', :NEW.NM_PESSOA_FISICA);
	  JSON_ADT_A08_W.PUT('DT_NASCIMENTO',	:NEW.DT_NASCIMENTO);
	  JSON_ADT_A08_W.PUT('IE_SEXO', :NEW.IE_SEXO);
	  
	  DBMS_LOB.CREATETEMPORARY(DS_MESSAGE_RESPONSE_W, TRUE);
	  JSON_ADT_A08_W.TO_CLOB(DS_MESSAGE_RESPONSE_W);
	  
	  DS_MESSAGE_RESPONSE_W := BIFROST.SEND_INTEGRATION_CONTENT(DS_EVENT_W,
																DS_MESSAGE_RESPONSE_W,
																OBTER_USUARIO_ATIVO);
	END;
  
  PROCEDURE HL7_ADT_47(CD_PESSOA_FISICA_P PESSOA_FISICA.CD_PESSOA_FISICA%TYPE, 
                       NR_PRONT_EXT_OLD_P PESSOA_FISICA.NR_PRONT_EXT%TYPE, 
                       NM_PESSOA_FISICA_OLD_P PESSOA_FISICA.NM_PESSOA_FISICA%TYPE) IS
    ie_use_integration_w varchar2(10);
    ds_comando_w         varchar2(4000);
    ds_guampa_w          varchar2(10) := chr(39);
    jobno                number;
  
  begin
    obter_param_usuario(9041,
                        10,
                        obter_perfil_ativo,
                        :NEW.NM_USUARIO,
                        obter_estabelecimento_ativo,
                        ie_use_integration_w);
  
    if (ie_use_integration_w = 'S') then
      begin
        ds_comando_w	:= ' declare '||CHR(10)||
                         ' ds_result_w clob; '||CHR(10)||
                         ' ds_params_w clob; '||CHR(10)||
                         ' begin '||CHR(10)||
                         ' ds_params_w := person_json_pck.get_patient_message_clob('||CD_PESSOA_FISICA_P||');'|| CHR(10)||
                         ' ds_params_w := REPLACE(ds_params_w,'
                          ||CHR(39)||'"externalAccountNumber"'||CHR(39)
                          ||','||CHR(39)||'"externalMedicalRecordIdOld":"'
                          || NR_PRONT_EXT_OLD_P||'"'
                          ||',"patientNameOld":"'
                          || NM_PESSOA_FISICA_OLD_P||'"'
                          ||',"externalAccountNumber"'|| CHR(39)||');'||CHR(10)||

                    ' if (ds_params_w is not null) then '||
                    ' wheb_usuario_pck.set_cd_estabelecimento('||obter_estabelecimento_ativo||');' ||                    
                    ' wheb_usuario_pck.set_nm_usuario('||CHR(39)||:NEW.NM_USUARIO||CHR(39)||');' ||
                    ' ds_result_w := bifrost.send_integration_content('
                    ||CHR(39)||'patient.identifier.update'||CHR(39)
                    ||',ds_params_w,'
                    ||CHR(39)||:NEW.NM_USUARIO||CHR(39)||','                
                    ||CHR(39)||'N'||CHR(39)||');'||
                    ' end if; '||
                    ' end;';                 
      
        dbms_job.submit(jobno, ds_comando_w);
      end;
    end if;
  
  end HL7_ADT_47;   
	
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then

	ds_sep_bv_w := obter_separador_bv;	
	
	if	(:old.dt_obito is null)
	and	(:new.dt_obito is null) then
		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
					'nr_atendimento=0'  || ds_sep_bv_w ||
					'nr_seq_interno=0'  || ds_sep_bv_w;
		gravar_agend_integracao(18, ds_param_integ_hl7_w);
		end;
	end if;
	
	if	(nvl(:old.nm_pessoa_fisica,'X') <> :new.nm_pessoa_fisica ) or
		(nvl(:old.ie_sexo,'X') 	<> :new.ie_sexo) 	or	
		(:old.dt_nascimento 	<> :new.dt_nascimento) 	or
		(nvl(:old.nr_identidade,'X') 	<> :new.nr_identidade) 	or
		((:old.dt_obito is null) and(:new.dt_obito is null)) then
		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ;
		gravar_agend_integracao(145, ds_param_integ_hl7_w);
		
		Mirth_ADT_A08_atualizaao_pac(:new.cd_pessoa_fisica);
		
		end;
	end if;
		
	if	(nvl(:old.nm_pessoa_fisica,'X') <> nvl(:new.nm_pessoa_fisica,'X')) or
		(nvl(:old.dt_nascimento,sysdate) <> nvl(:new.dt_nascimento,sysdate)) or
		(nvl(:old.ie_sexo,'X') <> nvl(:new.ie_sexo,'X')) then

		select	nvl(max('S'),'N')
		into	ie_integrado_w
		from	pf_codigo_externo
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica
		and	IE_TIPO_CODIGO_EXTERNO = 'DI'
		AND nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;
	
		if	(ie_integrado_w = 'S') then

			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
			gravar_agend_integracao(191, ds_param_integ_hl7_w);
	
		end if;

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
		gravar_agend_integracao(214, ds_param_integ_hl7_w);
	
	end if;
	
	if	(nvl(:old.nm_pessoa_fisica,'X') <> nvl(:new.nm_pessoa_fisica,'X')) or
		(nvl(:old.dt_nascimento,sysdate) <> nvl(:new.dt_nascimento,sysdate)) or
		(nvl(:old.ie_sexo,'X') <> nvl(:new.ie_sexo,'X')) or
		(nvl(:old.dt_obito,sysdate) <> nvl(:new.dt_obito,sysdate)) then	
	
		ger_pessoa_fisica_update(:new.cd_pessoa_fisica);
		
	end if;
	
	--HCOR ADT A08

   if (wheb_usuario_pck.is_evento_ativo(783) = 'S') then

      qt_atendimento_w  := OBTER_QTD_ATENDIMENTOS(:new.cd_pessoa_fisica);

      if (qt_atendimento_w > 0) then
         ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ;
         gravar_agend_integracao(783, ds_param_integ_hl7_w);
      end if;

   end if;
	
	if (wheb_usuario_pck.is_evento_ativo(853) = 'S') and
		((nvl(:old.nm_pessoa_fisica,'X') <> nvl(:new.nm_pessoa_fisica,'X')) or
		(nvl(:old.ie_sexo,'X') <> nvl(:new.ie_sexo,'X')) or
		(nvl(:old.dt_nascimento,sysdate) <> nvl(:new.dt_nascimento,sysdate)) or
		(nvl(:old.nr_seq_cor_pele,0) <> nvl(:new.nr_seq_cor_pele,0)) or
		(nvl(:old.ie_estado_civil,0) <> nvl(:new.ie_estado_civil,0)) or
		(nvl(:old.cd_religiao,0) <> nvl(:new.cd_religiao,0)) or
		(nvl(:old.nr_prontuario,0) <> nvl(:new.nr_prontuario,0)) or
		(nvl(:old.nr_cpf,'X') <> nvl(:new.nr_cpf,'X')) or
		(nvl(:old.nr_telefone_celular,'X') <> nvl(:new.nr_telefone_celular,'X')) or
		(nvl(:old.nr_ddd_celular,'X') <> nvl(:new.nr_ddd_celular,'X')) or
		(nvl(:old.nr_ddi_celular,'X') <> nvl(:new.nr_ddi_celular,'X')) or
		(nvl(:old.dt_obito,sysdate) <> nvl(:new.dt_obito,sysdate))) then
		
		enviar_pf_medico_hl7(:new.cd_pessoa_fisica,'MUP');	
		
		INTEGRA_BIFROST;
	end if;
	if ((nvl(:old.IE_TIPO_SANGUE, 'X') <> nvl(:new.IE_TIPO_SANGUE, 'X')) or
       (nvl(:old.IE_FATOR_RH, 'X') <> nvl(:new.IE_FATOR_RH, 'X')) or
       (nvl(:old.QT_PESO, 0) <> nvl(:new.QT_PESO, 0)) or
       (nvl(:old.QT_ALTURA_CM, 0) <> nvl(:new.QT_ALTURA_CM, 0)) or
	(nvl(:old.nm_pessoa_fisica,'X') <> nvl(:new.nm_pessoa_fisica,'X')) or
	(nvl(:old.ie_sexo,'X') <> nvl(:new.ie_sexo,'X')) or
	(nvl(:old.dt_nascimento,sysdate) <> nvl(:new.dt_nascimento,sysdate)) or
        (nvl(:old.nr_telefone_celular,'X') <> nvl(:new.nr_telefone_celular,'X')) or
	(nvl(:old.nr_ddd_celular,'X') <> nvl(:new.nr_ddd_celular,'X')) or
	(nvl(:old.nr_ddi_celular,'X') <> nvl(:new.nr_ddi_celular,'X'))) then
	   
		call_interface_file(927, 'itech_pck.send_patient_attribute_info('||ds_guampa_w|| :new.cd_pessoa_fisica ||ds_guampa_w||','||ds_guampa_w|| :new.nm_usuario ||ds_guampa_w||','||0||','||ds_guampa_w||null||ds_guampa_w||','||obter_estabelecimento_ativo||');', :new.nm_usuario);
		
		call_interface_file(933, 'carestream_ris_japan_l10n_pck.patient_change_info( ' || :new.cd_pessoa_fisica || ', ''' || :new.nm_usuario || ''' , 0 , null, ' || obter_estabelecimento_ativo || ');' , :new.nm_usuario);  
	end if;
	call_bifrost_content('patient.registration.update','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);
  
  begin
    -- INICIO ADT_A47
    IF (:NEW.NR_PRONT_EXT <> :OLD.NR_PRONT_EXT) THEN
      HL7_ADT_47(:OLD.CD_PESSOA_FISICA, :OLD.NR_PRONT_EXT, :OLD.NM_PESSOA_FISICA);
    END IF;
    -- FIM ADT_A47*/
  end;
  
end if;
end;
/
