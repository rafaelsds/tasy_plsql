create or replace trigger ALERTA_PACIENTE_atual
before insert or update on ALERTA_PACIENTE
for each row
declare


begin
if	(nvl(:old.DT_ALERTA,sysdate+10) <> :new.DT_ALERTA) and
	(:new.DT_ALERTA is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_ALERTA,'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/
