create or replace trigger bsc_ind_inf_update
before update on bsc_ind_inf
for each row

declare

ie_regra_result_w	varchar2(15);
nm_indicador_w	varchar2(255);

begin

select	nvl(max(ie_regra_result),'MA'),
	max(nm_indicador)
into	ie_regra_result_w,
	nm_indicador_w
from	bsc_indicador
where	nr_sequencia = :new.nr_seq_indicador;

if	(ie_regra_result_w = 'MA') then
	begin
	if	(nvl(:new.qt_limite,0) > nvl(:new.qt_meta,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(281869,'NR_SEQ_INDICADOR='||:new.nr_seq_indicador ||';NM_INDICADOR='|| nm_indicador_w);
	end if;
	end;
elsif	(ie_regra_result_w = 'ME') then
	begin
	if	(nvl(:new.qt_limite,0) < nvl(:new.qt_meta,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(281870,'NR_SEQ_INDICADOR='||:new.nr_seq_indicador ||';NM_INDICADOR='|| nm_indicador_w);
	end if;
	end;
end if;
end bsc_ind_inf_update;
/