create or replace trigger obj_schem_order_insert
after insert on obj_schem_order
for each row

declare

begin

if	(:new.nr_seq_obj_schematic is not null) then
	begin
	insert into obj_schem_order_item
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_regra,
		nr_seq_obj_schematic,
		nr_ordem)
	select	obj_schem_order_item_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		a.nr_sequencia,
		a.nr_seq_apres
	from	objeto_schematic a
	where	a.nr_seq_obj_sup = :new.nr_seq_obj_schematic;
	end;
elsif	(:new.nr_seq_funcao_schematic is not null) then
	begin
	insert into obj_schem_order_item
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_regra,
		nr_seq_obj_schematic,
		nr_ordem)
	select	obj_schem_order_item_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		x.nr_sequencia,
		rownum * 3 nr_seq_apres
	from	(select	a.nr_sequencia
		from	funcao b,
			objeto_schematic a
		where	b.cd_funcao = a.cd_funcao_externa
		and	a.nr_seq_funcao_schematic = :new.nr_seq_funcao_schematic
		and	a.ie_tipo_componente = 'WAE'
		order by substr(obter_desc_expressao(a.cd_exp_conf_usage,obter_desc_expressao(a.cd_exp_desc_obj,obter_desc_expressao(b.cd_exp_funcao,a.ds_objeto))),1,255)) x;
	end;
end if;

end;
/