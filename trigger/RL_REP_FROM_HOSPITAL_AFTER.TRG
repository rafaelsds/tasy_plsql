create or replace trigger rl_rep_from_hospital_after
after insert or update on rl_rep_from_hospital
for each row
declare

count_history_w	number(10);

begin
	if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	
		select	count(*)
		into	count_history_w
		from	rl_history
		where	nr_seq_rl_rep_from_hosp = :new.nr_sequencia;
		
		if (count_history_w > 0) then
			update rl_history
			set ds_referring_hosp = (select ds_razao_social from  pessoa_juridica where cd_cgc = :new.cd_medical_institution), 
			    ds_referred_hosp = (select ds_departamento ds from departamento_medico where cd_departamento = :new.cd_referred_department)
			where nr_seq_rl_rep_from_hosp = :new.nr_sequencia;
		else
			insert into rl_history	(
				nr_sequencia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_rl_rep_from_hosp,
				dt_history,
				ds_referring_hosp,
				ds_referred_hosp,
				ie_type,
				nm_recorded_by,
				ie_status
				)
			values(
				rl_history_seq.nextval,
				:new.dt_atualizacao,
				:new.dt_atualizacao_nrec,
				:new.nm_usuario,
				:new.nm_usuario_nrec,
				:new.nr_sequencia,
				sysdate,
				(select ds_razao_social from  pessoa_juridica where cd_cgc = :new.cd_medical_institution),
				(select ds_departamento ds from departamento_medico where cd_departamento = :new.cd_referred_department),
				'2',
				:new.nm_usuario,
				'1');
		end if;
	end if;

end;
/