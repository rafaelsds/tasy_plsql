create or replace trigger cpoe_gas_insert_update_after
after insert or update on cpoe_gasoterapia
for each row
declare

ds_stack_w		varchar2(2000);
ds_log_cpoe_w	varchar2(2000);
dt_min_date_w	date := to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss');

ie_order_integr_type_w 	varchar2(10);
nr_entity_identifier_w	cpoe_integracao.nr_sequencia%type;
ie_use_integration_w	varchar2(10);
nr_seq_sub_grp_w varchar2(2);
ie_transmit_special_order varchar2(1);
json_data_w           	clob;
ds_param_integration_w  varchar2(20000);
nr_prescricao_w  		number;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

	begin

	if	(nvl(:new.nr_sequencia,0) <> nvl(:old.nr_sequencia,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_sequencia(' || nvl(:old.nr_sequencia,0) || '/' || nvl(:new.nr_sequencia,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_cpoe_anterior,0) <> nvl(:old.nr_seq_cpoe_anterior,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_cpoe_anterior(' || nvl(:old.nr_seq_cpoe_anterior,0) || '/' || nvl(:new.nr_seq_cpoe_anterior,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao, dt_min_date_w) <> nvl(:old.dt_liberacao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_suspensao, dt_min_date_w) <> nvl(:old.dt_suspensao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_suspensao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_forma_suspensao,'XPTO') <> nvl(:old.ie_forma_suspensao,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_forma_suspensao(' || nvl(:old.ie_forma_suspensao,'<NULL>') || '/' || nvl(:new.ie_forma_suspensao,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_lib_suspensao, dt_min_date_w) <> nvl(:old.dt_lib_suspensao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_lib_suspensao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_lib_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_lib_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao_enf, dt_min_date_w) <> nvl(:old.dt_liberacao_enf, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao_enf(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao_enf, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao_enf, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao_farm, dt_min_date_w) <> nvl(:old.dt_liberacao_farm, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao_farm(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao_farm, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao_farm, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_prox_geracao, dt_min_date_w) <> nvl(:old.dt_prox_geracao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_prox_geracao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_prox_geracao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_prox_geracao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_inicio, dt_min_date_w) <> nvl(:old.dt_inicio, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_inicio(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_inicio, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_inicio, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_fim, dt_min_date_w) <> nvl(:old.dt_fim, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_fim(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_fim, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_fim, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ds_horarios,'XPTO') <> nvl(:old.ds_horarios,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ds_horarios(' || nvl(:old.ds_horarios,'<NULL>') || '/' || nvl(:new.ds_horarios,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_evento_unico,'XPTO') <> nvl(:old.ie_evento_unico,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_evento_unico(' || nvl(:old.ie_evento_unico,'<NULL>') || '/' || nvl(:new.ie_evento_unico,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_administracao,'XPTO') <> nvl(:old.ie_administracao,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_administracao(' || nvl(:old.ie_administracao,'<NULL>') || '/' || nvl(:new.ie_administracao,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ds_justificativa,'XPTO') <> nvl(:old.ds_justificativa,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ds_justificativa(' || nvl(length(:old.ds_justificativa),0) || '/' || nvl(length(:new.ds_justificativa),0)||'); ',1,2000);
	end if;

	if	(nvl(:new.hr_prim_horario,'XPTO') <> nvl(:old.hr_prim_horario,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' hr_prim_horario(' || nvl(:old.hr_prim_horario,'<NULL>') || '/' || nvl(:new.hr_prim_horario,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_setor_atendimento,0) <> nvl(:old.cd_setor_atendimento,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_setor_atendimento(' || nvl(:old.cd_setor_atendimento,0) || '/' || nvl(:new.cd_setor_atendimento,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_intervalo,'XPTO') <> nvl(:old.cd_intervalo,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_intervalo(' || nvl(:old.cd_intervalo,'<NULL>') || '/' || nvl(:new.cd_intervalo,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_baixado_por_alta,'XPTO') <> nvl(:old.ie_baixado_por_alta,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_baixado_por_alta(' || nvl(:old.ie_baixado_por_alta,'<NULL>') || '/' || nvl(:new.ie_baixado_por_alta,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_ocorrencia,0) <> nvl(:old.nr_ocorrencia,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_ocorrencia(' || nvl(:old.nr_ocorrencia,0) || '/' || nvl(:new.nr_ocorrencia,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_alta_medico, dt_min_date_w) <> nvl(:old.dt_alta_medico, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_alta_medico(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_alta_medico, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_alta_medico, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;
	
--***Trigger Point Cancel UnexecutedOrder Treatment Interface Starts***--
    if ((nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') and :new.ie_forma_suspensao = 'I' and :old.ie_forma_suspensao is null and wheb_usuario_pck.get_ie_executar_trigger = 'S') then
        select  distinct ct.nr_seq_sub_grp
        into    nr_seq_sub_grp_w
        from    cpoe_order_unit co,
                cpoe_tipo_pedido ct
        where   co.nr_seq_cpoe_tipo_pedido = ct.nr_sequencia
        and     co.nr_sequencia = :old.nr_seq_cpoe_order_unit;
    
        select  is_special_order_rule(nr_seq_sub_grp_w,'A',wheb_usuario_pck.get_cd_estabelecimento) --select  is_special_order_rule('PR','A',wheb_usuario_pck.get_cd_estabelecimento)
        into    ie_transmit_special_order 
        from    dual;
        if(ie_transmit_special_order = 'S' and nr_seq_sub_grp_w = 'PC') then
            select  nr_prescricao 
            into    nr_prescricao_w 
            from    prescr_gasoterapia b 
            where   b.nr_seq_gas_cpoe = :old.nr_sequencia;                      
            ds_param_integration_w := '{"recordId" : "' || nr_prescricao_w || '"' || ',"typePrescription" : "' || 'PR' || '"}';                       
            json_data_w := bifrost.send_integration_content(nm_event =>'nais.unexecutedOrderPrescriptionCancel.message', ds_content => ds_param_integration_w, nm_user => wheb_usuario_pck.get_nm_usuario);
         end if;
    end if;	
--***Trigger Point Cancel UnexecutedOrder Treatment Interface Ends***--  

	if	(ds_log_cpoe_w is not null) then
		
		if (nvl(:old.nr_sequencia, 0) > 0) then
			ds_log_cpoe_w := substr('Alteracoes(old/new)= ' || ds_log_cpoe_w,1,2000);
		else
			ds_log_cpoe_w := substr('Criacao(old/new)= ' || ds_log_cpoe_w,1,2000);
		end if;
		
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		ds_log_cpoe_w := substr(ds_log_cpoe_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')',1,2000);
		
		insert into log_cpoe(nr_sequencia, nr_atendimento, dt_atualizacao, nm_usuario, nr_seq_gasoterapia, ds_log, ds_stack) values (log_cpoe_seq.nextval, :new.nr_atendimento, sysdate, :new.nm_usuario, :new.nr_sequencia, ds_log_cpoe_w, ds_stack_w);
	end if;

	exception
	when others then
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		
		insert into log_cpoe(nr_sequencia, 
							nr_atendimento, 
							dt_atualizacao, 
							nm_usuario, 
							nr_seq_dieta, 
							ds_log, 
							ds_stack) 
		values (			log_cpoe_seq.nextval, 
							:new.nr_atendimento, 
							sysdate, 
							:new.nm_usuario, 
							:new.nr_sequencia, 
							'EXCEPTION CPOE_GAS_INSERT_UPDATE_AFTER', 
							ds_stack_w);
	end;
	
	begin
		if (:new.dt_liberacao is not null and ((:old.dt_liberacao_enf is null and  :new.dt_liberacao_enf is not null) or (:old.dt_liberacao_farm is null and :new.dt_liberacao_farm is not null))) then
			cpoe_atualizar_inf_adic(:new.nr_atendimento, :new.nr_sequencia, 'G', :new.dt_liberacao_enf, :new.dt_liberacao_farm, null, null, null, null, null, :new.nr_seq_cpoe_order_unit);
		end if;
	exception
		when others then
			gravar_log_cpoe('CPOE_GAS_INSERT_UPDATE_AFTER - CPOE_ATUALIZAR_INF_ADIC - Erro: ' || substr(sqlerrm(sqlcode),1,1500) || ' :new.nr_sequencia '|| :new.nr_sequencia, :new.nr_atendimento);
	end;
	
  obter_param_usuario(9041, 10, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_use_integration_w);
  
  if (ie_use_integration_w = 'S')
    and ((:OLD.DT_LIBERACAO IS NULL AND :NEW.DT_LIBERACAO IS NOT NULL)
      or (:OLD.DT_LIB_SUSPENSAO IS NULL AND :NEW.DT_LIB_SUSPENSAO IS NOT NULL )) then
    
    select obter_cpoe_regra_ator('G')
    into ie_order_integr_type_w
    from dual;
    
    if (ie_order_integr_type_w = 'OI') then
    
      cpoe_gas_order_json_pck.getCpoeIntegracaoGas(:new.nr_sequencia, nr_entity_identifier_w);
    
      if (:OLD.DT_LIBERACAO IS NULL AND :NEW.DT_LIBERACAO IS NOT NULL) then
        
        call_bifrost_content('prescription.gas.order.request','cpoe_gas_order_json_pck.get_message_clob(' || :new.nr_sequencia || ', ''NW'', ' || nr_entity_identifier_w || ')', :new.nm_usuario);
        
      elsif (:OLD.DT_LIB_SUSPENSAO IS NULL AND :NEW.DT_LIB_SUSPENSAO IS NOT NULL ) then
      
        call_bifrost_content('prescription.gas.order.request','cpoe_gas_order_json_pck.get_message_clob(' || :new.nr_sequencia || ', ''CA'', ' || nr_entity_identifier_w || ')', :new.nm_usuario);
        
      end if;
    
    end if;						
    
  end if;

<<final>>
null;	
	
end;
/
