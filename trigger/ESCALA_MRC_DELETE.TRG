create or replace trigger ESCALA_MRC_DELETE
after delete on escala_mrc
for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '243'	
and		nr_seq_escala  = :old.nr_sequencia;

commit;		

<<Final>>
qt_reg_w	:= 0;
end;
/