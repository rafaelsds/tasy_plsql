create or replace trigger pls_regra_ins_insert_update
before insert or update on pls_regra_inscricao
for each row

declare
qt_subestipulante_w	number(10);
begin

if 	(:new.nr_seq_subestipulante is not null) then
	select count(1)
	into	qt_subestipulante_w
	from	pls_contrato a,
		pls_sub_estipulante b
	where	a.nr_sequencia = b.nr_seq_contrato 
	and	b.nr_seq_contrato = :new.nr_seq_contrato
	and	b.nr_sequencia = :new.nr_seq_subestipulante;

	if	(qt_subestipulante_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1144978);	
		/* O subestipulante informado nao esta cadastrado no contrato. */
	end if;
end if;
end;
/
