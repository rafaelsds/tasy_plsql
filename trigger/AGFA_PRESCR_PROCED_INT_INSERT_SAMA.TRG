create or replace TRIGGER AGFA_PRESCR_PROCED_INT_INSERT_SAMA
before insert on prescr_procedimento
for each row
declare

cd_pessoa_fisica_w	varchar2(10);
cd_estabelecimento_w	number(5,0);
ie_gerar_w		varchar2(1)	:= 'S';
nr_seq_agenda_integrada_w	number(10,0);
cd_agenda_integracao_w	varchar2(30);
ie_modo_integracao_w	varchar2(5);
dt_agenda_w		date;

begin

if (obter_funcao_ativa = 950) then

	if	(:new.nr_Seq_solic_sangue is null) and
		(:new.nr_seq_derivado is null)  and
		(:new.nr_seq_exame is null) then

		select	max(cd_pessoa_fisica),
			max(cd_estabelecimento),
			max(ie_modo_integracao)
		into	cd_pessoa_fisica_w,
			cd_estabelecimento_w,
			ie_modo_integracao_w
		from	prescr_medica
		where	nr_prescricao = :new.nr_prescricao;

		select 	max(a.nr_sequencia)
		into	nr_seq_agenda_integrada_w
		from	agenda_integrada a
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and	a.nr_seq_status in (select nr_sequencia from agenda_integrada_status where ie_status_tasy = 'AG')
		and	exists (	select	1
					from 	agenda_integrada_item b
					where	a.nr_sequencia = b.nr_seq_Agenda_int
					and	b.nr_seq_proc_interno = :new.nr_seq_proc_interno
					and 	trunc(Obter_Horario_item_Ageint(b.nr_seq_agenda_cons, b.nr_Seq_Agenda_exame, b.nr_sequencia)) = trunc(sysdate));

		select	decode(count(*),0,'S','N')
		into	ie_gerar_w
		from	agenda_integrada_item
		where	nr_seq_agenda_int 	= nr_seq_agenda_integrada_w
		and	nr_seq_proc_interno	= :new.nr_seq_proc_interno;

		select	max(AIL_03_EXAM_ROOM)
		into	cd_agenda_integracao_w
		from	agfa_siu_in
		where	SCH_26_APP_BATCH = nr_seq_agenda_integrada_w;

		select	min(hr_inicio)
		into	dt_agenda_w
		from	agenda_paciente a,
			agenda_integrada_item b
		where	a.nr_sequencia		= b.nr_seq_Agenda_exame
		and	b.nr_seq_agenda_int	= nr_seq_agenda_integrada_w;
		if	(dt_agenda_w is null) then
			select	min(dt_agenda)
			into	dt_agenda_w
			from	agenda_consulta a,
				agenda_integrada_item b
			where	a.nr_sequencia		= b.nr_seq_Agenda_cons
			and	b.nr_seq_agenda_int	= nr_seq_agenda_integrada_w;
		end if;


		if	(ie_gerar_w = 'S') and
			(ie_modo_integracao_w = 'A') then
			Gravar_Agfa_Siu_Integracao(cd_pessoa_fisica_w,
					  :new.nm_usuario,
					  cd_estabelecimento_w,
					  null,--:new.cd_medico_exec,
					  :new.nr_seq_proc_interno,
					  sysdate,
					  null,
					  :new.ie_lado,
					  null,
					  'S12',
					  :new.ie_autorizacao,
					  :new.ds_observacao,
					  nvl(:new.dt_prev_execucao,dt_agenda_w),
					  :new.nr_seq_interno,
					  null,
					  cd_agenda_integracao_w,
					  :new.dt_prev_execucao,
					  :new.dt_prev_execucao + 30/1440);
		end if;

	end if;

end if;	
end;
/