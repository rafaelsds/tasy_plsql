create or replace trigger escala_isi_atual
before insert or update on ESCALA_ISI
for each row

declare

begin

:new.qt_pontuacao := nvl(:new.IE_DIF_PEGAR_SONO,0) +
		nvl(:new.IE_DIF_MANTER_SONO,0) +
		nvl(:new.IE_DESPERTAR_CEDO,0) +
		nvl(:new.IE_SATISFACAO_SONO,0) +
		nvl(:new.IE_INTERFERE_ATIV,0) +
		nvl(:new.IE_PERCEBE_QUALIDADE,0) +
		nvl(:new.IE_NIVEL_ESTRES,0);

end;
/