create or replace trigger agenda_paciente_proc_delete
before delete on agenda_paciente_proc
for each row

declare
ie_exclui_proc_autor_w	varchar2(5);
cd_estabelecimento_w	number(10);
nr_seq_proc_autor_w	number(15);
nr_seq_autor_w		number(15);
cd_agenda_w		number(10,0);
ds_obs_exclusao_w	varchar2(4000) := substr(wheb_mensagem_pck.get_texto(326971),1,3800);
/*lhalves OS 217619 em 02/06/2010 - excluir o procedimento da autorização se a autorização estiver pendente*/
cd_pessoa_fisica_w		varchar2(10);

cursor c01 is
select	a.nr_sequencia,
	b.nr_sequencia
from	procedimento_autorizado a,
	autorizacao_convenio b,
	estagio_autorizacao c
where	b.nr_seq_agenda		= :old.nr_sequencia
and	b.nr_sequencia		= a.nr_sequencia_autor
and	a.cd_procedimento	= :old.cd_procedimento
and	a.ie_origem_proced	= :old.ie_origem_proced
and	b.nr_seq_estagio	= c.nr_sequencia
and	c.ie_interno		= 1;


cursor c02 is
select	a.nr_sequencia,
	b.nr_sequencia
from	procedimento_autorizado a,
	autorizacao_convenio b,
	estagio_autorizacao c
where	b.nr_seq_agenda		= :old.nr_sequencia
and	b.nr_sequencia		= a.nr_sequencia_autor
and	a.cd_procedimento	= :old.cd_procedimento
and	a.ie_origem_proced	= :old.ie_origem_proced
and	b.nr_seq_estagio	= c.nr_sequencia
and	nvl(c.ie_excluir_proc_adic_agenda,'N') = 'S';


cursor c03 is
select	a.nr_sequencia,
	b.nr_sequencia
from	procedimento_autorizado a,
	autorizacao_convenio b,
	estagio_autorizacao c
where	b.nr_seq_agenda		= :old.nr_sequencia
and	b.nr_sequencia		= a.nr_sequencia_autor
and	a.cd_procedimento	= :old.cd_procedimento
and	a.ie_origem_proced	= :old.ie_origem_proced
and	b.nr_seq_estagio	= c.nr_sequencia;


begin

if	(:old.nr_seq_proc_interno is not null) then

	delete agenda_pac_equip
	where  nr_seq_proc_interno = :old.nr_seq_proc_interno
	and    nr_seq_agenda = :old.nr_sequencia;

	delete agenda_pac_cme
	where  nr_seq_proc_interno = :old.nr_seq_proc_interno
	and    nr_seq_agenda = :old.nr_sequencia;

end if;

select 	a.cd_estabelecimento,
         a.cd_agenda,
         b.cd_pessoa_fisica
into	   cd_estabelecimento_w,
         cd_agenda_w,
         cd_pessoa_fisica_w
from 	agenda a,
	agenda_paciente b
where	b.nr_sequencia		= :old.nr_sequencia
and	b.cd_agenda		= a.cd_agenda;

obter_param_usuario(871,315,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w, ie_exclui_proc_autor_w);


if	(nvl(ie_exclui_proc_autor_w, 'N') = 'S') then
	open c01;
	loop
	fetch c01 into
		nr_seq_proc_autor_w,
		nr_seq_autor_w;
	exit when c01%notfound;	
		update	procedimento_paciente
		set 	nr_seq_proc_autor	= null
		where	nr_seq_proc_autor	= nr_seq_proc_autor_w;

		update	autorizacao_convenio
		set	nr_seq_agenda_proc	= null
		where	nr_sequencia		= nr_seq_autor_w;
	
		delete 	procedimento_autorizado
		where	nr_sequencia		= nr_seq_proc_autor_w;
	end loop;
	close c01;
elsif	(nvl(ie_exclui_proc_autor_w, 'N') = 'E') then

	open c02;
	loop
	fetch c02 into
		nr_seq_proc_autor_w,
		nr_seq_autor_w;
	exit when c02%notfound;	
		update	procedimento_paciente
		set 	nr_seq_proc_autor	= null
		where	nr_seq_proc_autor	= nr_seq_proc_autor_w;

		update	autorizacao_convenio
		set	nr_seq_agenda_proc	= null
		where	nr_sequencia		= nr_seq_autor_w;
	
		delete 	procedimento_autorizado
		where	nr_sequencia		= nr_seq_proc_autor_w;
	end loop;
	close c02;
elsif	(nvl(ie_exclui_proc_autor_w, 'N') = 'N') then

	open c03;
	loop
	fetch c03 into
		nr_seq_proc_autor_w,
		nr_seq_autor_w;
	exit when c03%notfound;	

		update	autorizacao_convenio
		set	nr_seq_agenda_proc	= null
		where	nr_sequencia		= nr_seq_autor_w;
	
		update	procedimento_autorizado
		set	nm_usuario 		= nvl(wheb_usuario_pck.get_nm_usuario(),'TASY'),
			dt_atualizacao		= sysdate,
			ds_observacao		= substr(decode(nvl(ds_observacao,'X'),'X',ds_obs_exclusao_w, ds_observacao ||chr(13)||ds_obs_exclusao_w),1,3900)
		where	nr_sequencia		= nr_seq_proc_autor_w;
	end loop;
	close c03;
		
end if;

atualiza_pedido_kit_agenda(	:old.nr_sequencia,
				:old.cd_procedimento,
				:old.ie_origem_proced,
				null,
				null,
				null,
				null,
				:old.nm_usuario,
				cd_estabelecimento_w,
				cd_agenda_w,
            cd_pessoa_fisica_w);


end;
/