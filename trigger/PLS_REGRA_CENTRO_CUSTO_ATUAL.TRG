create or replace trigger pls_regra_centro_custo_atual
before insert or update on pls_regra_centro_custo
for each row

declare
nr_seq_contrato_w	pls_contrato.nr_sequencia%type;

begin

if (:new.ie_prestador_codificacao is null and :new.nr_seq_prestador is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1110523);
end if;

select 	max(nr_sequencia) nr_seq_contrato
into	nr_seq_contrato_w
from 	pls_contrato
where 	nr_contrato = :new.nr_contrato;

:new.nr_seq_contrato := nr_seq_contrato_w;

end pls_regra_centro_custo_atual;
/