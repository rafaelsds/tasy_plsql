create or replace TRIGGER funcao_param_perfil_subject FOR
  DELETE OR INSERT OR UPDATE ON funcao_param_perfil
COMPOUND TRIGGER
  apply_sync   BOOLEAN := false;
  AFTER EACH ROW IS BEGIN
    apply_sync := psa_is_auth_parameter(
      :new.cd_funcao,
      :new.nr_sequencia 
    ) = 'TRUE' OR apply_sync;
  END AFTER EACH ROW;
  AFTER STATEMENT IS BEGIN
    IF
      apply_sync
    THEN
      psa_synchronize_subject;
    END IF;
  END AFTER STATEMENT;
END;
/