create or replace trigger agenda_paciente_update_hl7
after update on agenda_paciente
for each row

declare
ds_sep_bv_w		        varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
ie_gerar_integracao_w	varchar2(1);
ie_integrado_w		    varchar2(1);
cd_tipo_agenda_w	    number(10);
ie_permite_ag_ep12_w    varchar2(1);

parametros 		LT_PARAM_INTEGRACAO;
param_element 	T_PARAM_INTEGRACAO;

--pragma autonomous_transaction;
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

    
    select	nvl(max(IE_PER_AGEND_EP12),'N')	
    into	ie_permite_ag_ep12_w
    from	PARAMETRO_MEDICO
    where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
    
    
    select	nvl(max(ie_integra_rel_dixtal),'N'),
        max(cd_tipo_agenda)
    into	ie_gerar_integracao_w,
        cd_tipo_agenda_w
    from	agenda
    where	cd_agenda = :new.cd_agenda;
    
    ds_sep_bv_w := obter_separador_bv;
    
    if	(ie_gerar_integracao_w = 'S') then
    
        parametros := LT_PARAM_INTEGRACAO();
    
        if	(:old.dt_confirmacao is null) and 
            (:new.dt_confirmacao is not null) and
            (:new.cd_pessoa_fisica is not null) then
            
            select	nvl(max('S'),'N')
            into	ie_integrado_w
            from	pf_codigo_externo
            where	cd_pessoa_fisica = :new.cd_pessoa_fisica
            and	ie_tipo_codigo_externo = 'DI';
            
            --ie_integrado_w := 'N';
            
            if	(ie_integrado_w = 'N') then
                insert into	pf_codigo_externo(
                    nr_sequencia,
                    dt_atualizacao,
                    nm_usuario,
                    dt_atualizacao_nrec,
                    nm_usuario_nrec,
                    ie_tipo_codigo_externo,
                    cd_pessoa_fisica,
                    cd_pessoa_fisica_externo,
                    cd_estabelecimento)
                values(
                    pf_codigo_externo_seq.NextVal,
                    sysdate,
                    'Tasy',
                    sysdate,
                    'Tasy',
                    'DI',
                    :new.cd_pessoa_fisica,
                    '0',
                    wheb_usuario_pck.get_cd_estabelecimento);
    
                ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
    
                param_element := T_PARAM_INTEGRACAO(25, ds_param_integ_hl7_w);
                parametros.extend(1);
                parametros(parametros.count) :=  param_element;
            end if;		
    
            ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
                        'nr_sequencia=' || :new.nr_sequencia || ds_sep_bv_w;
    
            param_element := T_PARAM_INTEGRACAO(190, ds_param_integ_hl7_w);
            parametros.extend(1);
            parametros(parametros.count) :=  param_element;
    
            ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
    
            param_element := T_PARAM_INTEGRACAO(191, ds_param_integ_hl7_w);
            parametros.extend(1);
            parametros(parametros.count) :=  param_element;
            gravar_agend_integracao_lista(parametros);
    
        elsif	(:new.dt_confirmacao is not null) then
            
            if	(:old.ie_status_agenda <>'C') and 
                (:new.ie_status_agenda = 'C') and
                (:new.cd_pessoa_fisica is not null) then
    
                ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
                            'nr_sequencia=' || :new.nr_sequencia || ds_sep_bv_w;
                gravar_agend_integracao(192, ds_param_integ_hl7_w);
            end if;
    
            if	(nvl(:old.cd_procedimento,0) <>nvl(:new.cd_procedimento,0)) or
                (nvl(:old.ie_origem_proced,0) <>nvl(:new.ie_origem_proced,0)) or
                (nvl(:old.nr_minuto_duracao,0) <>nvl(:new.nr_minuto_duracao,0)) or
                (nvl(:old.hr_inicio,sysdate) <>nvl(:new.hr_inicio,sysdate)) or
                (nvl(:old.nm_pessoa_contato,'X') <>nvl(:new.nm_pessoa_contato,'X')) or
                (nvl(:old.cd_medico,'X') <>nvl(:new.cd_medico,'X')) or
                (nvl(:old.cd_anestesista,'X') <>nvl(:new.cd_anestesista,'X')) or
                (nvl(:old.cd_agenda,0) <>nvl(:new.cd_agenda,'0')) or
                (nvl(:old.nm_usuario_orig,'X') <>nvl(:new.nm_usuario_orig,'X')) then
                
                ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
                            'nr_sequencia=' || :new.nr_sequencia || ds_sep_bv_w;
                gravar_agend_integracao(193, ds_param_integ_hl7_w);		
            end if;
        
        end if;	
    end if;
    
    
    if 	(cd_tipo_agenda_w = 2) and ((ie_permite_ag_ep12_w = 'S') or (wheb_usuario_pck.get_cd_estabelecimento = 11)) then 
    
    
    
        if	(verifica_proced_integracao(:new.nr_seq_proc_interno,:new.cd_procedimento,:new.ie_origem_proced, 46) = 'S') then
                
                
            if 	(:new.cd_pessoa_Fisica is not null) and
                (:old.cd_pessoa_Fisica is null) then
                
                select	nvl(max('S'),'N')
                into	ie_integrado_w
                from	pf_codigo_externo
                where	cd_pessoa_fisica = :new.cd_pessoa_fisica
                and	ie_tipo_codigo_externo = 'DI';
                
                if (ie_integrado_w = 'N') then
                    ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
                    gravar_agend_integracao(216, ds_param_integ_hl7_w);
                end if;
                
                ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
                gravar_agend_integracao(214, ds_param_integ_hl7_w);
                
                ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
                            'nr_sequencia=' || :new.nr_sequencia || ds_sep_bv_w;
                
                gravar_agend_integracao(219, ds_param_integ_hl7_w);
            elsif	(:new.ie_status_agenda in('C', 'F', 'I', 'S')) then
                ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
                            'nr_sequencia=' || :new.nr_sequencia || ds_sep_bv_w;
                
                gravar_agend_integracao(220, ds_param_integ_hl7_w);
            end if;
            
        end if;
        
    end if;
    
    
    
    
    if 	(cd_tipo_agenda_w = 2) and
        (:old.dt_confirmacao is null) and
        (:new.dt_confirmacao is not null) and
        (wheb_usuario_pck.is_evento_ativo(531) = 'S') then
    
        if	(verifica_proced_integracao(:new.nr_seq_proc_interno,:new.cd_procedimento,:new.ie_origem_proced, 91) = 'S') then
    
            ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
                                    'nr_seq_agenda='   	|| :new.nr_sequencia   || ds_sep_bv_w ;
    
            gravar_agend_integracao(531, ds_param_integ_hl7_w);
    
        end if;
    
    end if;
    
    --Cancelacion de Examenes
    if 	(cd_tipo_agenda_w = 2) and
        (:new.ie_status_agenda = 'C') and
        (wheb_usuario_pck.is_evento_ativo(531) = 'S') then
    
            ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :old.cd_pessoa_fisica || ds_sep_bv_w ||
                                    'nr_seq_agenda='   	|| :old.nr_sequencia   || ds_sep_bv_w ;
    
            gravar_agend_integracao(531, ds_param_integ_hl7_w);
    
    end if;
    
    --Cambio de Pacientes
    if 	(cd_tipo_agenda_w = 2) and
        (:new.cd_pessoa_fisica <>:old.cd_pessoa_fisica) and
        (wheb_usuario_pck.is_evento_ativo(531) = 'S') then
    
            ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
                                    'nr_seq_agenda='   	|| :old.nr_sequencia   || ds_sep_bv_w ;
    
            gravar_agend_integracao(531, ds_param_integ_hl7_w);
    
    end if;
    
    --Cambio de Procedimientos
    if 	(cd_tipo_agenda_w = 2) and
        (:new.nr_seq_proc_interno <>:old.nr_seq_proc_interno) and
        (wheb_usuario_pck.is_evento_ativo(531) = 'S') then
    
            ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :old.cd_pessoa_fisica || ds_sep_bv_w ||
                                    'nr_seq_agenda='   	|| :old.nr_sequencia   || ds_sep_bv_w ;
    
            gravar_agend_integracao(531, ds_param_integ_hl7_w);
    
    end if;

end if;
end;
/
