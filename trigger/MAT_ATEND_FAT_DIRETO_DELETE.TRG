create or replace trigger mat_atend_fat_direto_delete
before delete on mat_atend_valor_fat_direto
for each row

declare

nr_seq_proc_princ_w	procedimento_paciente.nr_sequencia%type;

begin

if	(:old.nr_seq_proc_princ is not null) then

	update	procedimento_paciente
	set	vl_procedimento = vl_procedimento - :old.vl_material
	where	nr_sequencia = :old.nr_seq_proc_princ;

end if;

end;
/