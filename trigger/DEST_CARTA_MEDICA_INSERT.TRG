CREATE OR REPLACE TRIGGER DEST_CARTA_MEDICA_INSERT
AFTER INSERT ON DESTINATARIO_CARTA_MEDICA
FOR EACH ROW 
DECLARE

nr_seq_modelo_w 		carta_medica.nr_sequencia%type;
ds_chave_w				carta_medica_modelo_macro.ds_chave%type;
ie_macro_pf_w			carta_medica_modelo_macro.ie_macro_pf%type;
ie_sexo_pf_w			varchar2(1);
ds_info_pf_w			varchar2(2000);
nr_seq_person_name_w	pessoa_fisica.nr_seq_person_name%type;

Cursor C01 is
select 	ds_chave, ie_macro_pf
from	carta_medica_modelo_macro
where 	nr_seq_modelo = nr_seq_modelo_w;

begin

select 	max(nr_seq_modelo)
into 	nr_seq_modelo_w
from 	carta_medica
where 	nr_sequencia = :new.nr_seq_carta_mae;

begin
open C01;
loop
fetch C01 into
	ds_chave_w,
	ie_macro_pf_w;
exit when C01%notfound;
	begin

	if (:new.cd_pessoa_fisica is not null) then

		if (ie_macro_pf_w = 'NM') then
			if (person_name_enabled = 'S') then
				select 	max(nr_seq_person_name)
				into 	nr_seq_person_name_w
				from 	pessoa_fisica
				where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

				select max(nm_pessoa_fisica)
				into ds_info_pf_w
				from table(search_names_dev(null, null, nr_seq_person_name_w, 'givenName', 'social,main'));
			else
				select 	max(nm_primeiro_nome)
				into 	ds_info_pf_w
				from	pessoa_fisica
				where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;
			end if;
		elsif (ie_macro_pf_w = 'SN') then
			if (person_name_enabled = 'S') then
				select 	max(nr_seq_person_name)
				into 	nr_seq_person_name_w
				from 	pessoa_fisica
				where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

				select max(nm_pessoa_fisica)
				into ds_info_pf_w
				from table(search_names_dev(null, null, nr_seq_person_name_w, 'familyName', 'social,main'));
			else
				select 	max(nm_sobrenome_pai)
				into 	ds_info_pf_w
				from	pessoa_fisica
				where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;
			end if;
		elsif (ie_macro_pf_w = 'PF') then
			select 	max(DS_COMPONENT_NAME_1)
			into 	ds_info_pf_w
			from   	person_name
			where 	nr_sequencia = (select nr_seq_person_name
									from   pessoa_fisica
									where cd_pessoa_fisica = :new.cd_pessoa_fisica);
		elsif (ie_macro_pf_w = 'SF') then
			select 	max(DS_COMPONENT_NAME_3)
			into 	ds_info_pf_w
			from   	person_name
			where 	nr_sequencia = (select nr_seq_person_name
									from   pessoa_fisica
									where cd_pessoa_fisica = :new.cd_pessoa_fisica);	
		elsif (ie_macro_pf_w = 'ST') then
			ds_info_pf_w := obter_compl_pf(:new.cd_pessoa_fisica, 1, 'E');
		elsif (ie_macro_pf_w = 'SA') then
			ds_info_pf_w := obter_pronome_pf(:new.cd_pessoa_fisica, 'N');
		elsif (ie_macro_pf_w = 'ED') then
			select 	max(ds_endereco)
			into 	ds_info_pf_w
			from 	compl_pessoa_fisica a, pais b
			where 	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
			and   	a.ie_tipo_complemento = 1
			and   	a.nr_seq_pais = b.nr_sequencia(+);
		elsif (ie_macro_pf_w = 'CD') then
			select 	max(ds_municipio)
			into 	ds_info_pf_w
			from 	compl_pessoa_fisica
			where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
			and   	ie_tipo_complemento = 1;
		elsif (ie_macro_pf_w = 'CP') then
			select 	max(cd_cep)
			into 	ds_info_pf_w
			from 	compl_pessoa_fisica
			where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
			and   	ie_tipo_complemento = 1;
		elsif (ie_macro_pf_w = 'NE') then
			select 	max(ds_compl_end)
			into 	ds_info_pf_w
			from 	compl_pessoa_fisica
			where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
			and   	ie_tipo_complemento = 1;
		elsif (ie_macro_pf_w = 'PA') then
			select 	max(nm_pais)
			into 	ds_info_pf_w
			from 	compl_pessoa_fisica a, pais b
			where 	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
			and   	a.ie_tipo_complemento = 1
			and   	a.nr_seq_pais = b.nr_sequencia(+);
		elsif (ie_macro_pf_w = 'GT') then
			ie_sexo_pf_w := obter_sexo_pf(:new.cd_pessoa_fisica, 'C');
			if	(obter_se_medico(:new.cd_pessoa_fisica, 'M') = 'S') then
				if (ie_sexo_pf_w = 'M') then
					ds_info_pf_w := obter_desc_expressao(890031);
				else
					ds_info_pf_w := obter_desc_expressao(890029);
				end if;
			else
				if (ie_sexo_pf_w = 'M') then
					ds_info_pf_w := obter_desc_expressao(890027);
				else
					ds_info_pf_w := obter_desc_expressao(890025);
				end if;
				
			end if;
		elsif (ie_macro_pf_w = 'NC') then
			if (person_name_enabled = 'S') then
				select 	max(nr_seq_person_name)
				into 	nr_seq_person_name_w
				from 	pessoa_fisica
				where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

				select max(nm_pessoa_fisica)
				into ds_info_pf_w
				from table(search_names_dev(null, null, nr_seq_person_name_w, 'full', 'social,main'));
			else
				select 	max(nm_pessoa_fisica)
				into 	ds_info_pf_w
				from	pessoa_fisica
				where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;
			end if;
		elsif (ie_macro_pf_w = 'TA') then
			select 	max(b.ds_forma_tratamento)
			into	ds_info_pf_w
			from	pessoa_fisica a, PF_FORMA_TRATAMENTO b
			where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
			and 	a.nr_seq_forma_trat = b.nr_sequencia;

		else
			ds_info_pf_w := null;
		end if;
	else
		ds_info_pf_w := null;	
	end if;

	insert into dest_carta_medica_macro(NR_SEQUENCIA,
										DT_ATUALIZACAO,
										NM_USUARIO,
										DT_ATUALIZACAO_NREC,
										NM_USUARIO_NREC,
										DS_CHAVE,
										NR_SEQ_DESTINATARIO,
										DS_VALOR)
								values(dest_carta_medica_macro_seq.nextval,
										sysdate,
										:new.nm_usuario,
										sysdate,
										:new.nm_usuario,
										ds_chave_w,
										:new.nr_sequencia,
										ds_info_pf_w);

	end;
end loop;
close C01;
end;

end;
/