CREATE OR REPLACE TRIGGER Titulo_Receber_Insert
BEFORE INSERT ON Titulo_Receber
FOR EACH ROW

DECLARE

ie_titulo_prot_zerado_w		varchar2(20);
ie_historico_w			varchar2(2);
nr_sequencia_w			number(15);
cd_convenio_parametro_w		number(5);
nr_nivel_anterior_w		number(5);
ie_valor_tit_protocolo_w		varchar2(2);
cd_convenio_w			number(5);
tx_juros_w			number(7,4);
tx_multa_w			number(7,4);
ie_dt_contabil_ops_w	varchar2(1);
dt_contabil_ops_w		date;
qt_provisorio_w			number(10) := 0;

ds_module_log_w conta_paciente_hist.ds_module%type;
ds_call_stack_w conta_paciente_hist.ds_call_stack%type;

BEGIN

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

select	max(a.cd_convenio_parametro)
into	cd_convenio_w
from	conta_paciente a
where	a.nr_interno_conta	= :new.nr_interno_conta;

if 	(:new.vl_outras_despesas = 0) then

	OBTER_ACRESC_CR (:new.cd_estabelecimento,:new.cd_cgc,:new.vl_outras_despesas);

end if;

if	(cd_convenio_w	is null) then

	select	max(a.cd_convenio)
	into	cd_convenio_w
	from	protocolo_convenio a
	where	a.nr_seq_protocolo	= :new.nr_seq_protocolo;

end if;

select	nvl(max(ie_historico_conta),'N')
into	ie_historico_w
from	parametro_faturamento
where	cd_estabelecimento	= :new.cd_estabelecimento;

if	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, :new.dt_emissao,'CR',null,cd_convenio_w,null,null) = 'N') then
	--O mes/dia financeiro de emissao do titulo ja esta fechado! Nao e possivel incluir novos titulos neste mes.
	Wheb_mensagem_pck.exibir_mensagem_abort(222067);
end if;

if	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, nvl(:new.dt_contabil,:new.dt_emissao),'CR',null,cd_convenio_w,null,null) = 'N') then
	--O mes/dia financeiro da data contabil do titulo ja esta fechado! Nao e possivel incluir novos titulos neste mes. Data: #@DT_CONTABIL#@
	Wheb_mensagem_pck.exibir_mensagem_abort(222068,'DT_CONTABIL='||nvl(:new.dt_contabil,:new.dt_emissao));
end if;

if	(:new.vl_saldo_titulo < 0) or (:new.vl_titulo < 0) then
	--O valor do titulo nao pode ser negativo!
	Wheb_mensagem_pck.exibir_mensagem_abort(222069);
end if;

select	nvl(max(ie_titulo_prot_zerado),'N'),
	nvl(max(ie_valor_tit_protocolo),'S')	
into	ie_titulo_prot_zerado_w,
	ie_valor_tit_protocolo_w
from	parametro_contas_receber
where	cd_estabelecimento		= :new.cd_estabelecimento;

-- Edgar 31/01/2008, OS 81434, coloquei consistencia abaixo ate achar o problema de geracao de titulos do protooclo com valor zerado
if	(ie_titulo_prot_zerado_w = 'P') and
	(:new.vl_titulo = 0)  and
	(:new.nr_seq_protocolo is not null) then
	--O valor do titulo de protocolo nao pode ser zero!
	Wheb_mensagem_pck.exibir_mensagem_abort(222070);
end if;

if	(ie_titulo_prot_zerado_w = 'N') and (:new.vl_titulo = 0) and (:new.ie_origem_titulo <> '3')  then
	--O valor do titulo nao pode ser zero!
	Wheb_mensagem_pck.exibir_mensagem_abort(222071);
end if;

--lhalves 19/10/2010, OS 259145.
if	(ie_valor_tit_protocolo_w = 'N') and
	(:new.nr_seq_protocolo is not null) and
	(:new.nr_interno_conta is null) then
	if	(:new.vl_titulo <> obter_total_protocolo(:new.nr_seq_protocolo)) then
		--O valor do titulo nao pode ser diferente do protocolo!
		Wheb_mensagem_pck.exibir_mensagem_abort(222072);
	end if;
end if;


if	(:new.dt_vencimento < to_date('01/01/1900', 'dd/mm/yyyy')) then
	--Nao e possivel incluir um titulo com vencimento inferior a 01/01/1900!
	Wheb_mensagem_pck.exibir_mensagem_abort(222073);
end if;

if	((:new.cd_cgc is null) and (:new.cd_pessoa_fisica is null)) 
	or
	((:new.cd_cgc is not null) and (:new.cd_pessoa_fisica is not null)) then
	--O titulo deve ser de uma pessoa fisica ou juridica!
	Wheb_mensagem_pck.exibir_mensagem_abort(222074);
end if;

qt_provisorio_w := 0;

if	(:new.nr_seq_protocolo is not null) then
	
	select	count(*)
	into	qt_provisorio_w
	from	protocolo_convenio x
	where	x.nr_seq_protocolo = :new.nr_seq_protocolo
	and	x.ie_status_protocolo = 1;

elsif	 (:new.nr_seq_lote_prot is not null) then
	
	select	count(*)
	into	qt_provisorio_w
	from	protocolo_convenio w
	where	w.nr_seq_lote_protocolo = :new.nr_seq_lote_prot
	and	w.ie_status_protocolo = 1;

end if;

if	(qt_provisorio_w > 0) then

	--Nao e permitido gerar o titulo para um protocolo com status Provisorio.
	wheb_mensagem_pck.exibir_mensagem_abort(338789);
end if;

:new.dt_contabil := nvl(:new.dt_contabil, :new.dt_emissao);

/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_contabil);


if	(ie_historico_w		= 'S') and
	( :new.NR_INTERNO_CONTA  is not null) then
	begin
	select	conta_paciente_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	select	cd_convenio_parametro,
		decode(nr_seq_protocolo,null,6,8)
	into	cd_convenio_parametro_w,
		nr_nivel_anterior_w
	from	conta_paciente
	where 	nr_interno_conta	= :new.NR_INTERNO_CONTA;
	
	select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
	into	ds_module_log_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);
	
	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);
	
	insert into conta_paciente_hist(
		nr_sequencia, dt_atualizacao, nm_usuario,
		vl_conta, nr_seq_protocolo, nr_interno_conta,
		nr_nivel_anterior, nr_nivel_atual, dt_referencia,
		nr_atendimento, cd_convenio, dt_conta_protocolo,
		cd_funcao, ds_module, ds_call_stack)
	values	(
		nr_sequencia_w, sysdate, :new.nm_usuario,
		obter_valor_conta(:new.nr_interno_conta,0), null, :new.nr_interno_conta,
		nr_nivel_anterior_w, 
		nr_nivel_anterior_w , sysdate,
		obter_atendimento_conta(:new.nr_interno_conta), 
		cd_convenio_parametro_w, 
		null,
		obter_funcao_Ativa, ds_module_log_w, ds_call_stack_w);
	exception
		when others then
		nr_nivel_anterior_w	:=nr_nivel_anterior_w;
	end;
	
end if;

if	(Obter_Valor_Param_Usuario(85,89,obter_Perfil_Ativo,:new.nm_usuario,0)	='S') and
	(:new.nr_seq_protocolo is not null) then
	update	protocolo_convenio a
	set	a.dt_vencimento		= :new.dt_vencimento
	where	nr_seq_protocolo	= :new.nr_seq_protocolo;
end if;

if	(trunc(:new.dt_pagamento_previsto, 'dd') < trunc(:new.dt_emissao, 'dd')) then
	--A data de vencimento nao pode ser menor que a data de emissao do titulo!
	--Dt emissao: #@DT_EMISSAO#@
	--Dt vencimento: #@DT_VENCIMENTO#@
	--Seq protocolo: #@NR_SEQ_PROTOCOLO#@
	--Pessoa: #@DS_PESSOA#@
	Wheb_mensagem_pck.exibir_mensagem_abort(222075,	'DT_EMISSAO='||:new.dt_emissao||
							';DT_VENCIMENTO='||trunc(:new.dt_pagamento_previsto, 'dd')||
							';NR_SEQ_PROTOCOLO='||:new.nr_seq_protocolo||
							';DS_PESSOA='||obter_nome_pf_pj(:new.cd_pessoa_fisica, :new.cd_cgc));
end if;

obter_tx_juros_multa_cre(:new.cd_estabelecimento,
			:new.ie_origem_titulo,
			:new.ie_tipo_titulo,
			tx_juros_w,
			tx_multa_w);

if	(tx_juros_w is not null) then
	:new.tx_juros	:= tx_juros_w;
end if;

if	(tx_multa_w is not null) then
	:new.tx_multa	:= tx_multa_w;
end if; 

/*AAMFIRMO OS 925991 - Para titulos originados de OPS Mensalidade(3)  e OPS Faturamento(13), a data contabil do titulo deve ser a data contabil no OPS*/
Obter_Param_Usuario(801, 204, obter_perfil_ativo, :new.nm_usuario,:new.cd_estabelecimento, ie_dt_contabil_ops_w);

if ( nvl(ie_dt_contabil_ops_w,'N') = 'S' ) and ( :new.ie_origem_titulo = '3' ) then /*Aqui somente titulo com origem OPS - Mensalidade. Para origem OPS Faturamento foi tratado na proc pls_gerar_titulos_fatura*/
	
	/*Esse select para buscar a data e o mesmo utilizado na funcao Titulo_receber, na function que busca o valor para o campo  DT_MENSALIDADE no titulo*/
	begin

	select 	substr(pls_obter_dados_mensalidade(:new.nr_seq_mensalidade,'D'),1,10)
	into	dt_contabil_ops_w
	from 	dual;
	if (dt_contabil_ops_w is null) then
	
		select 	max(x.dt_mes_competencia)
		into	dt_contabil_ops_w
		from 	pls_fatura  x
		where 	x.nr_titulo = :new.nr_titulo;

		if (dt_contabil_ops_w is null) then
		
			select 	max(x.dt_mes_competencia)
			into	dt_contabil_ops_w
			from 	pls_fatura  x
			where  	x.nr_titulo_ndc = :new.nr_titulo;

		end if;
	
	end if;	

	if (dt_contabil_ops_w is not null) then
		:new.dt_contabil := nvl(dt_contabil_ops_w,nvl(:new.dt_contabil, :new.dt_emissao));
	end if;
	exception when others then
		:new.dt_contabil := nvl(:new.dt_contabil, :new.dt_emissao);
	end;

end if;
/*AAMFIRMO Fim tratativa OS 925991*/
:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);
end if;

END;
/
