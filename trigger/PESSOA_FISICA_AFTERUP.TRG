create or replace trigger pessoa_fisica_AfterUp
after insert or update on pessoa_fisica
for each row

declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
nm_contato_w			varchar2(60);
ds_endereco_w			varchar2(100);
cd_cep_w			varchar2(15);
nr_endereco_w			number(5);
ds_complemento_w		varchar2(40);
ds_municipio_w			varchar2(40);
ds_bairro_w			varchar2(40);
nr_telefone_w			varchar2(15);
nr_ramal_w			number(5);
ds_fone_adic_w			varchar2(80);
ds_email_w			varchar2(255);
cd_profissao_w			number(10);
cd_empresa_refer_w		number(10);
ds_setor_trabalho_w		varchar2(30);
ds_horario_trabalho_w		varchar2(30);
nr_matricula_trabalho_w		varchar2(20);
cd_municipio_ibge_w		varchar2(6);
ds_fax_w			varchar2(80);
cd_tipo_logradouro_w		varchar2(3);
nr_ddd_telefone_w		varchar2(3);
nr_ddi_telefone_w		varchar2(3);
nr_ddi_fax_w			varchar2(3);
nr_ddd_fax_w			varchar2(3);
ds_website_w			varchar2(255);
nm_contato_pesquisa_w		varchar2(60);
ie_tipo_complemento_w		number(2);
qt_existe_w			number(10);
ds_mensagem_w			varchar2(255);
ds_email_origem_w		varchar2(255);
qt_reg_w			number(1);
ie_opcao_w			varchar2(1) := 'I';
sg_estado_w		compl_pessoa_fisica.sg_estado%type;
ie_cad_completo_w	FUNCAO_PARAMETRO.VL_PARAMETRO_PADRAO%type;
ie_cad_simplif_w	FUNCAO_PARAMETRO.VL_PARAMETRO_PADRAO%type;
IE_ATUAL_PAC_AGEINT_w	parametro_agenda.IE_ATUAL_PAC_AGEINT%type;
IE_ATUAL_PAC_AGECON_AGESERV_w	parametro_agenda.IE_ATUAL_PAC_AGECON_AGESERV%type;
ie_existe_w	varchar2(1);
IE_STATUS_CPF_W VARCHAR2(1);
NR_QTD_REG_EST_W NUMBER(10);

cursor C01 is
	select	ds_email_envio
	from	regra_aviso_pessoa_cns
	where	ie_situacao = 'A';

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

Obter_param_Usuario(5, 164, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_cad_completo_w);
Obter_param_Usuario(32, 44, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_cad_simplif_w);

select  nvl(max(IE_ATUAL_PAC_AGEINT), 'N'),
	nvl(max(IE_ATUAL_PAC_AGECON_AGESERV), 'N')
into	IE_ATUAL_PAC_AGEINT_w,
	IE_ATUAL_PAC_AGECON_AGESERV_w
from 	PARAMETRO_AGENDA
where 	cd_estabelecimento = :new.cd_estabelecimento;

if	(:new.nm_pessoa_fisica	<> :old.nm_pessoa_fisica and updating) then
	--(OBTER_FUNCAO_ATIVA = 871) then -- OS 626900
	
	wheb_usuario_pck.set_ie_executar_trigger('N');
	
	update	agenda_paciente
	set	nm_paciente		= :new.nm_pessoa_fisica,
		nm_usuario		= 'Tasy',
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;				
	
	if (ie_cad_completo_w = 'N') and (ie_cad_simplif_w = 'N') then			
		if (IE_ATUAL_PAC_AGECON_AGESERV_w = 'S') then
			update	agenda_consulta
			set	nm_paciente		= :new.nm_pessoa_fisica,
				nm_usuario		= 'Tasy',
				dt_atualizacao		= sysdate
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
		end if;
	end if;	
	if (IE_ATUAL_PAC_AGEINT_w = 'S') then
		update	agenda_integrada
		set	NM_PACIENTE		= :new.nm_pessoa_fisica,
			nm_usuario		= 'Tasy',
			dt_atualizacao		= sysdate
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	end if;	
	
	begin
		select	'S'
		into	ie_existe_w
		from	gerint_solic_internacao
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and		rownum = 1;

		update	gerint_solic_internacao
		set		NM_PESSOA_FISICA	= :new.nm_pessoa_fisica,
				nm_usuario			= 'Tasy',
				dt_atualizacao		= sysdate
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;	
	exception
	when others then
		null;
	end;	
	

	
	wheb_usuario_pck.set_ie_executar_trigger('S');		
end if;


select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a,
	sup_int_regra_pf b
where	a.nr_sequencia = b.nr_seq_integracao
and	a.ie_evento = 'PF'
and	a.ie_forma = 'E'
and	a.ie_situacao = 'A'
and	b.ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:new.nm_usuario <> 'INTEGR_TASY') then
	
	envia_sup_int_pf(
		:new.cd_pessoa_fisica,
		:new.nr_identidade,
		:new.nm_pessoa_fisica,
		:new.nr_telefone_celular,
		:new.ie_grau_instrucao,
		:new.nr_cep_cidade_nasc,
		:new.nr_prontuario,
		:new.nm_usuario,
		:new.cd_religiao,
		:new.nr_pis_pasep,
		:new.cd_nacionalidade,
		:new.ie_dependencia_sus,
		:new.qt_altura_cm,
		:new.ie_tipo_sangue,
		:new.ie_fator_rh,
		:new.dt_nascimento,
		:new.dt_obito,
		:new.ie_sexo,
		:new.nr_iss,
		:new.ie_estado_civil,
		:new.nr_inss,
		:new.nr_cpf,
		:new.nr_cert_nasc,
		:new.cd_cargo,
		:new.nr_cert_casamento,
		:new.ds_codigo_prof,
		:new.cd_empresa,
		:new.ie_funcionario,
		:new.nr_seq_cor_pele,
		:new.ds_orgao_emissor_ci,
		:new.nr_cartao_nac_sus,
		:new.cd_cbo_sus,
		:new.cd_atividade_sus,
		:new.ie_vinculo_sus,
		:new.cd_sistema_ant,
		:new.ie_frequenta_escola,
		:new.cd_funcionario,
		:new.nr_pager_bip,
		:new.nr_transacao_sus,
		:new.cd_medico,
		:new.ie_tipo_prontuario,
		:new.dt_emissao_ci,
		:new.nr_seq_conselho,
		:new.dt_admissao_hosp,
		:new.ie_fluencia_portugues,
		:new.nr_titulo_eleitor,
		:new.nr_zona,
		:new.nr_secao,
		:new.nr_cartao_estrangeiro,
		:new.nr_reg_geral_estrang,
		:new.dt_chegada_brasil,
		:new.sg_emissora_ci,
		:new.dt_naturalizacao_pf,
		:new.nr_ctps,
		:new.nr_serie_ctps,
		:new.uf_emissora_ctps,
		:new.dt_emissao_ctps,
		:new.nr_portaria_nat,
		:new.nr_seq_cartorio_nasc,
		:new.nr_seq_cartorio_casamento,
		:new.dt_emissao_cert_nasc,
		:new.dt_emissao_cert_casamento,
		:new.nr_livro_cert_nasc,
		:new.nr_livro_cert_casamento,
		:new.dt_cadastro_original,
		:new.nr_folha_cert_nasc,
		:new.nr_folha_cert_casamento,
		:new.nr_same,
		:new.ds_observacao,
		:new.qt_dependente,
		:new.nr_transplante,
		:new.dt_validade_rg,
		:new.dt_revisao,
		:new.dt_demissao_hosp,
		:new.cd_puericultura,
		:new.cd_cnes,
		:new.ds_apelido,
		:new.ie_endereco_correspondencia,
		:new.qt_peso_nasc,
		:new.uf_conselho,
		:new.nm_abreviado,
		:new.nr_ccm,
		:new.dt_fim_experiencia,
		:new.dt_validade_conselho,
		:new.ds_profissao,
		:new.ds_empresa_pf,
		:new.nr_passaporte,
		:new.nr_cnh,
		:new.nr_cert_militar,
		:new.nr_pront_ext,
		:new.ie_escolaridade_cns,
		:new.ie_situacao_conj_cns,
		:new.nr_folha_cert_div,
		:new.nr_cert_divorcio,
		:new.nr_seq_cartorio_divorcio,
		:new.dt_emissao_cert_divorcio,
		:new.nr_livro_cert_divorcio,
		:new.dt_alta_institucional,
		:new.dt_transplante,
		:new.nr_matricula_nasc,
		:new.ie_doador,
		:new.dt_vencimento_cnh,
		:new.ds_categoria_cnh,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null);
end if;

if	((:old.nr_cartao_nac_sus is null) and (:new.nr_cartao_nac_sus is not null)) or
	((:new.nr_cartao_nac_sus is not null) and (:old.nr_cartao_nac_sus <> :new.nr_cartao_nac_sus)) then
	
	open C01;
	loop
	fetch C01 into	
		ds_email_w;
	exit when C01%notfound;
		begin
		if	(ds_email_w is not null) then
			ds_mensagem_w := OBTER_DESC_EXPRESSAO(327262) || ' ' || :new.cd_pessoa_fisica || chr(13) || chr(10) ||
					'Pessoa: ' || :new.nm_pessoa_fisica || chr(13) || chr(10) ||
					'CNS:'	|| :new.nr_cartao_nac_sus;
			enviar_email('Cadastro de Pessoa com CNS', ds_mensagem_w, ds_email_origem_w, ds_email_w, :new.nm_usuario, 'M');
		end if;
		end;
	end loop;
	close C01;
	
end if;

/* Projeto MXM (7077)  - Exportar cadastro pessoa fisica */
if (:new.nr_cpf is not null) then
	gravar_agend_integracao(556,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';CD_PESSOA_JURIDICA='||null||';'); --Fornecedor
	gravar_agend_integracao(562,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';'); --Cliente
end if;

if	(updating) and (nvl(:old.nm_pessoa_fisica, :new.nm_pessoa_fisica) <> :new.nm_pessoa_fisica) then
	if	(wheb_usuario_pck.is_evento_ativo(634) = 'S') then
		supplypoint_atual_inf_pac(:new.cd_pessoa_fisica, :new.nm_usuario);
	end if;
	if	(wheb_usuario_pck.is_evento_ativo(919) = 'S') then
		integracao_athena_disp_pck.atualiza_info_paciente(:new.cd_pessoa_fisica, :new.nm_usuario);
	end if;
    gerar_int_dankia_pck.dankia_disp_atualiza_paciente(nvl(:new.cd_estabelecimento,obter_estabelecimento_ativo),
                                                       :new.nm_usuario,
                                                       :new.cd_pessoa_fisica,
                                                       :new.nm_pessoa_fisica,
                                                       :new.ie_sexo,
                                                       :new.dt_nascimento);
end if;

	select	max(ie_motivo_sem_cpf)
	into	ie_status_cpf_w
	from	pessoa_fisica_aux
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

  IF (IE_STATUS_CPF_W = '4'
  AND :NEW.NR_CARTAO_ESTRANGEIRO IS NULL
  AND :NEW.NR_REG_GERAL_ESTRANG IS NULL) THEN
    SELECT COUNT(*)
      INTO NR_QTD_REG_EST_W
      FROM PESSOA_FISICA_ESTRANGEIRO PFE
     WHERE PFE.CD_PESSOA_FISICA = :NEW.CD_PESSOA_FISICA;

    IF (NR_QTD_REG_EST_W = 0) THEN
      ATUALIZA_MOTIVO_SEM_CPF_PF(:NEW.CD_PESSOA_FISICA, NULL, :NEW.NM_USUARIO, 'N');

      IE_STATUS_CPF_W := NULL;
    END IF;
  END IF;

  SELECT CASE WHEN :NEW.NR_CPF IS NOT NULL THEN '7'
              WHEN :NEW.NR_CARTAO_ESTRANGEIRO IS NOT NULL
                OR :NEW.NR_REG_GERAL_ESTRANG IS NOT NULL
                OR (SELECT COUNT(*) 
                      FROM PESSOA_FISICA_ESTRANGEIRO PFE 
                     WHERE PFE.CD_PESSOA_FISICA = :NEW.CD_PESSOA_FISICA) > 0 THEN '4'
              WHEN OBTER_IDADE(:NEW.DT_NASCIMENTO, SYSDATE, 'A') < 18
        and  (ie_status_cpf_w <> '1' or OBTER_IDADE(:NEW.DT_NASCIMENTO, SYSDATE, 'A') > 0) THEN '2'
              WHEN OBTER_IDADE(:NEW.DT_NASCIMENTO, SYSDATE, 'A') > 60 THEN '3'
         ELSE '6' END
    INTO IE_STATUS_CPF_W
    FROM DUAL;

  ATUALIZA_MOTIVO_SEM_CPF_PF(:NEW.CD_PESSOA_FISICA, IE_STATUS_CPF_W, :NEW.NM_USUARIO, 'N');

if 	(updating) then
	ie_opcao_w	:= 'A';
end if;
reg_integracao_p.ie_operacao		:= ie_opcao_w;
reg_integracao_p.nr_prontuario		:= :new.nr_prontuario;
reg_integracao_p.cd_pessoa_fisica	:= :new.cd_pessoa_fisica;
reg_integracao_p.ie_funcionario 	:= nvl(:new.ie_funcionario,'N');
reg_integracao_p.nr_seq_conselho 	:= :new.nr_seq_conselho;

SELECT decode(COUNT(*), 0, 'N', 'S')
  INTO reg_integracao_p.ie_pessoa_atend
  FROM pessoa_fisica_aux
 WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica
   AND nr_primeiro_atend IS NOT NULL;
   
   if :new.nr_cpf is not null then
      reg_integracao_p.ie_possui_cpf := 'S';
   end if;

if	(wheb_usuario_pck.get_ie_lote_contabil = 'N') then
	gerar_int_padrao.gravar_integracao('12',:new.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
end if;

if (:new.ie_funcionario = 'S') then
  gerar_int_padrao.gravar_integracao('304',:new.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
end if;

if :new.dt_obito is not null then
  finaliza_evento_pj_obito(:new.cd_pessoa_fisica, :new.dt_obito,:new.nm_usuario, 'N');
end if;

<<Final>>
qt_reg_w  := 0;


end;
/
