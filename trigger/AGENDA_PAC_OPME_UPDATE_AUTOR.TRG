CREATE OR REPLACE TRIGGER AGENDA_PAC_OPME_UPDATE_AUTOR
BEFORE UPDATE ON AGENDA_PAC_OPME
FOR EACH ROW

DECLARE

nr_seq_autorizacao_w		number(15,0);

cursor c01 is
select	a.nr_sequencia
from	autorizacao_convenio a
where	a.nr_seq_agenda	= :new.nr_seq_agenda;

BEGIN

if	((wheb_usuario_pck.get_ie_executar_trigger = 'S') and (:old.ie_gerar_autor <> :new.ie_gerar_autor))  then

	open c01;
	loop
	fetch c01 into
		nr_seq_autorizacao_w;
	exit when c01%notfound;
		begin

 		delete
		from	material_autorizado
		where	cd_material	= :new.cd_material
		and	:new.ie_gerar_autor	= 'N'
		and	nr_sequencia_autor	= nr_seq_autorizacao_w;
		end;	

	end loop;
	close c01;

end if;

END;
/