create or replace trigger cpoe_recomendacao_bef_delete
before delete on cpoe_recomendacao for each row
declare

begin

	update cpoe_revalidation_events
	set    nr_seq_recomendation = null
	where  nr_seq_recomendation = :old.nr_sequencia;
	
exception
when others then
	null;
end;
/
