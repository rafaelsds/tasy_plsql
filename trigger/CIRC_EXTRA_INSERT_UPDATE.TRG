create or replace
trigger circ_extra_insert_update
before insert or update on circulacao_extracorporea
for each row
declare

begin

:new.qt_pressao_trans_membrana := :new.qt_pressao_pre_membrana - :new.qt_pressao_pos_membrana;

end;
/
