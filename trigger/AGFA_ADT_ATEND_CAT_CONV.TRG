CREATE OR REPLACE 
TRIGGER Agfa_ADT_Atend_Cat_Conv
BEFORE INSERT OR UPDATE ON ATEND_CATEGORIA_CONVENIO
FOR EACH ROW
DECLARE 

nr_sequencia_w		Number(08,0);
nm_mae_w			Varchar2(80);
nm_pai_w			Varchar2(80);
ie_evento_w			Varchar2(03)		:= 'A08';
ds_endereco_w		Varchar2(100);/*OS98068*/
nr_endereco_w			Varchar2(15);
ds_bairro_w			Varchar2(80);
ds_municipio_w		Varchar2(40);
nr_telefone_w			Varchar2(40);
ds_nacionalidade_w		Varchar2(100);
ie_estado_civil_w		Varchar2(30);
nm_pessoa_fisica_w		Varchar2(80);
nm_nome_w			Varchar2(80);
nm_resto_nome_w		Varchar2(80);
dt_nascimento_w		Date;
ie_sexo_w			Varchar2(01);
cd_nacionalidade_w		Number(15,0);

nr_atendimento_w		Number(15,0);
ie_tipo_admissao_w		Varchar2(02)		:= 'R';
ie_tipo_atendimento_w	Varchar2(02);		/* AA-Ambulatorio AE-Amb. Externo A2-Urgencia H1-Hospitalizado*/
cd_setor_atendimento_w	Varchar2(25);		/* C�digo^Descri��o*/
ds_setor_atendimento_w	Varchar2(250);
ds_dado_medico_resp_w	Varchar2(15);		/* C�digo^Nome^Especialidade*/

dt_entrada_w			Date;
dt_alta_w			Date;
cd_pessoa_fisica_w		Varchar2(10);
cd_medico_resp_w		Varchar2(10);

cd_unidade_basica_w		Varchar2(25);
cd_unidade_compl_w		Varchar2(25);
cd_plano_w			Varchar2(10);
ds_plano_w			Varchar2(80);
cd_convenio_w			Number(5);
ds_convenio_w			Varchar2(80);

nr_atepacu_w			Number(15,0);

nr_cpf_w				Varchar2(11);
nr_identidade_w			Varchar2(15);
ds_orgao_emissor_ci_w	varchar2(10);
ds_compl_info_w			varchar2(50); /*OS123463*/

BEGIN

select Agfa_adt_seq.nextval
into	nr_sequencia_w
from	dual;

select	dt_entrada,
	dt_alta,
	cd_pessoa_fisica,
	cd_medico_resp,
	ie_tipo_atendimento,
	nr_atendimento
into	dt_entrada_w,
	dt_alta_w,
	cd_pessoa_fisica_w,
	cd_medico_resp_w,
	ie_tipo_atendimento_w,
	nr_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

begin
nr_atepacu_w :=	obter_atepacu_paciente(:new.nr_atendimento, 'A');
if	(nr_atepacu_w is not null) then
	select	cd_unidade_basica,
		cd_unidade_compl,
		cd_setor_atendimento,
		substr(obter_nome_setor(cd_setor_atendimento),1,250)
	into	cd_unidade_basica_w,
		cd_unidade_compl_w,
		cd_setor_atendimento_w,
		ds_setor_atendimento_w
	from	atend_paciente_unidade
	where	nr_seq_interno	= nr_atepacu_w;
	ds_setor_atendimento_w	:= substr(cd_setor_atendimento_w || '^' || ds_setor_atendimento_w,1,250);
end if;
exception
	when no_data_found then
		ds_setor_atendimento_w := '';
end;

cd_convenio_w			:= :new.cd_convenio;
cd_plano_w			:= :new.cd_plano_convenio;
select	obter_nome_convenio(cd_convenio_w),
	obter_desc_plano(cd_convenio_w, cd_plano_w)
into	ds_convenio_w,
	ds_plano_w
from	dual;

select	substr(obter_nome_pf(cd_pessoa_fisica),1,80),
		ie_sexo,	
		ie_estado_civil,
		cd_nacionalidade,
		dt_nascimento,
		substr(obter_parte_nome(substr(obter_nome_pf(cd_pessoa_fisica),1,255),'Nome'),1,80),
		substr(obter_parte_nome(substr(obter_nome_pf(cd_pessoa_fisica),1,255),'RestoNome'),1,80),
		nr_cpf,
		nr_identidade,
		ds_orgao_emissor_ci
into	nm_pessoa_fisica_w,
		ie_sexo_w,
		ie_estado_civil_w,
		cd_nacionalidade_w,
		dt_nascimento_w,
		nm_nome_w,
		nm_resto_nome_w,
		nr_cpf_w,
		nr_identidade_w,
		ds_orgao_emissor_ci_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

select Decode(ie_tipo_atendimento_w,1,'H1',3,'A2',7,'AE',8,'AA')
into	ie_tipo_atendimento_w
from	dual;

select	substr(max(cd_pessoa_fisica || '^' || substr(obter_nome_pf(cd_pessoa_fisica),1,255) || '^' || Obter_Especialidade_medico(cd_pessoa_fisica,'D')),1,15)
into	ds_dado_medico_resp_w
from 	pessoa_fisica
where	cd_pessoa_fisica	= cd_medico_resp_w;

select max(ds_endereco), 
	max(nr_endereco), 
	max(ds_bairro), 
	max(ds_municipio),
	max(nr_telefone)
into	ds_endereco_w,
	nr_endereco_w,
	ds_bairro_w,
	ds_municipio_w,
	nr_telefone_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_w
and	ie_tipo_complemento	= 1;

select max(nm_contato)
into	nm_pai_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_w
and	ie_tipo_complemento	= 4;

select max(nm_contato)
into	nm_mae_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_w
and	ie_tipo_complemento	= 5;

select	max(ds_nacionalidade)
into	ds_nacionalidade_w
from	nacionalidade
where	cd_nacionalidade	= cd_nacionalidade_w;

if	(ie_estado_civil_w = 2) then
	ie_estado_civil_w	:= 'M';
elsif	(ie_estado_civil_w = 2) then
	ie_estado_civil_w	:= 'W';
else
	ie_estado_civil_w	:= 'S';
end if;

/*OS98068*/
ds_compl_info_w := '##' || nr_identidade_w || ' ' || ds_orgao_emissor_ci_w ||'##'|| nr_cpf_w;
ds_endereco_w := substr(ds_endereco_w,1,100-length(ds_compl_info_w)) || ds_compl_info_w ;

insert into Agfa_Adt_v(
	ie_sistema,
	dt_insert,
	nr_seq_hl7,
	ie_status,
	ie_evento,
	dt_transacao,
	cd_pessoa_fisica,
	nm_sobrenome,
	nm_primeiro_nome,
	dt_nascimento,
	ie_sexo,
	ds_endereco,
	nr_endereco,
	ds_bairro,
	ds_municipio,
	nr_telefone,
	ie_estado_civil,
	ds_nacionalidade,
	nm_mae,
	ds_mae,
	nm_pai,
	ds_pai,
	ie_tipo_atendimento,
	cd_setor_atendimento,
	cd_unidade_basica,
	cd_unidade_compl,
	ie_tipo_admissao,
	ds_dado_medico_resp,
	nr_atendimento,
	dt_entrada,
	dt_alta,
	cd_plano,
	ds_plano,
	cd_convenio,
	ds_convenio,
	dt_inicio_vigencia,
	dt_final_vigencia,
	cd_plano2	)
values(
	'HSL',
	sysdate,
	nr_sequencia_w,
	'N',
	ie_evento_w,
	sysdate,
	cd_pessoa_fisica_w,
	nm_resto_nome_w,
	nm_nome_w,
	dt_nascimento_w,
	ie_sexo_w,
	ds_endereco_w,
	nr_endereco_w,
	substr(ds_bairro_w,1,40),
	ds_municipio_w,
	nr_telefone_w,
	ie_estado_civil_w,
	ds_nacionalidade_w,
	nm_mae_w,
	wheb_mensagem_pck.get_texto(306993, null), -- M�e
	nm_pai_w,
	wheb_mensagem_pck.get_texto(306992, null), -- Pai
	ie_tipo_atendimento_w,
	ds_setor_atendimento_w,
	cd_unidade_basica_w,
	cd_unidade_compl_w,
	ie_tipo_admissao_w,
	ds_dado_medico_resp_w,
	nr_atendimento_w,
	dt_entrada_w,
	dt_alta_w,
	cd_plano_w,
	ds_plano_w,
	cd_convenio_w,
	ds_convenio_w,
	:new.dt_inicio_vigencia,
	:new.dt_final_vigencia,
	cd_plano_w);

END;
/