CREATE OR REPLACE TRIGGER PLS_EXCECAO_OCORRENCIA_ATUAL
BEFORE INSERT OR UPDATE ON PLS_EXCECAO_OCORRENCIA
FOR EACH ROW

declare

cd_material_w	number(6);
qt_regra_comp_w	Number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	if	(:new.nr_seq_prestador is null and :old.nr_seq_prestador is not null) then
		select	count(1)
		into	qt_regra_comp_w
		from	PLS_OCOR_REGRA_PREST
		where	NR_SEQ_OCOR_EXCECAO     = :new.nr_sequencia
		and	rownum < 2;
		
		if	(qt_regra_comp_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(233687); 
		end if;
	end if;
end if;

END;
/
