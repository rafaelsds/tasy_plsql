CREATE OR REPLACE TRIGGER Can_ordem_prod_mat_delete
after delete ON Can_ordem_prod_mat
FOR EACH ROW
DECLARE
nr_seq_cabine_w		Number(10);
cd_estab_w			Number(4);
ie_baixa_kit_w varchar2(1 char);

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	return;
end if;

obter_param_usuario(3130, 511, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_baixa_kit_w);

select	b.nr_seq_cabine
into	nr_seq_cabine_w
from	far_etapa_producao b,
	can_ordem_prod a
where	a.nr_sequencia = :old.nr_seq_ordem
and	b.nr_sequencia = a.nr_seq_etapa_prod;

select	cd_estabelecimento
into	cd_estab_w
from	far_cabine_seg_biol
where	nr_sequencia = nr_seq_cabine_w;

begin
if 	(nvl(:old.ie_devolve_cabine,'S') <> 'N') and
	(:old.nr_seq_kit is null or ie_baixa_kit_w = 'S') and
	(:old.IE_SOBRA_OVERFILL not in ('Y','X','Z')) then --nao tratar diluentes dos medic diluidos
	wheb_usuario_pck.set_ie_commit('N');
	Adiciona_item_cabine(:old.cd_material, :old.qt_dose_real, :old.nr_seq_lote_fornec, :old.nm_usuario, nr_seq_cabine_w, :old.IE_SOBRA_OVERFILL, :old.NR_SEQ_ORDEM_ORIGEM);
	wheb_usuario_pck.set_ie_commit('S');
end if;
if 	(nvl(:old.ie_devolve_cabine,'S') <> 'N') and
	(:old.nr_seq_kit is null or ie_baixa_kit_w = 'S') and
	(:old.IE_SOBRA_OVERFILL in ('Y','X')) then
--restaura a dose do medicamento diluido
	update	quimio_sobra_overfill
	set		qt_saldo = qt_saldo + round(Obter_conversao_ml(:old.cd_material, :old.qt_dose_real, :old.cd_unidade_medida_real),3)
	where	nr_sequencia = :old.nr_seq_quimio_sobra_overfill;
end if;
exception
	when others then
      	nr_seq_cabine_w := 1;
end;
END;
/
