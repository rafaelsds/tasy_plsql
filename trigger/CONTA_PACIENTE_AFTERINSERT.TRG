create or replace trigger conta_paciente_afterinsert
after insert on conta_paciente
for each row

declare

nr_sequencia_w		number(10,0);
nr_seq_etapa_padrao_w	number(10,0);
cd_convenio_w		number(6,0);
cd_estabelecimento_w	number(10,0);
ie_tipo_atendimento_w	number(3,0);
nr_seq_classificacao_w	number(10,0);
cd_categoria_w		varchar2(10);
ie_clinica_w		number(5,0);
cd_setor_atendimento_w	number(10,0);
nr_seq_motivo_dev_w	number(10,0);
cd_setor_atend_etapa_w	number(10);
ie_pessoa_etapa_w	varchar2(3);
ie_etapa_critica_w	varchar2(1);
nm_usuario_original_w	varchar2(15);
cd_pessoa_fisica_w	varchar2(10);
cd_perfil_w		perfil.cd_perfil%type := obter_perfil_ativo;
qt_regra_restringe_w	number(10,0);
nr_seq_w		fatur_etapa_conta.nr_sequencia%type;
qt_regra_w		Number(1);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_etapa,
		nr_seq_motivo_dev,
		cd_setor_atend_etapa,
		nvl(ie_pessoa_etapa,'L'),
		nvl(ie_etapa_critica,'N')
	from	fatur_etapa_conta
	where	ie_situacao = 'A'
	and 	nvl(cd_convenio, nvl(cd_convenio_w,0)) = nvl(cd_convenio_w,0)
	and 	nvl(cd_estabelecimento, nvl(cd_estabelecimento_w,0)) = nvl(cd_estabelecimento_w,0)
	and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,0)
	and	nvl(nr_seq_classificacao, nvl(nr_seq_classificacao_w,0)) = nvl(nr_seq_classificacao_w,0)
	and	nvl(cd_categoria, nvl(cd_categoria_w,'0')) = nvl(cd_categoria_w,'0')
	and	nvl(ie_clinica, nvl(ie_clinica_w,0)) = nvl(ie_clinica_w,0)
	and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
	and 	((nvl(IE_CONTA_AJUSTE_CANCEL,'N') = 'N') or ((nvl(IE_CONTA_AJUSTE_CANCEL,'N') = 'X') and (nvl(:new.nr_seq_conta_origem,0) = 0)))
	and	((nvl(ie_conta_desdobramento,'S') = 'S') or
		((nvl(ie_conta_desdobramento,'S') = 'N') and (:new.nr_conta_orig_desdob is null)))
	and	nvl(cd_perfil,nvl(cd_perfil_w,0))	= nvl(cd_perfil_w,0)
	order by nvl(cd_convenio,0),
		nvl(cd_estabelecimento,0),
		nvl(ie_tipo_atendimento,0),
		nvl(nr_seq_classificacao,0),
		nvl(cd_categoria,'0'),
		nvl(ie_clinica,0),
		nvl(cd_setor_atendimento,0),
		nvl(cd_perfil,0);

begin

cd_convenio_w		:= :new.cd_convenio_parametro;
cd_estabelecimento_w	:= :new.cd_estabelecimento;
cd_categoria_w		:= :new.cd_categoria_parametro;
cd_setor_atendimento_w	:= nvl(obter_setor_atendimento(:new.nr_atendimento),0);


nr_seq_etapa_padrao_w:= null;

select	nvl(max(ie_tipo_atendimento),0),
	nvl(max(nr_seq_classificacao),0),
	nvl(max(ie_clinica),0)
into	ie_tipo_atendimento_w,
	nr_seq_classificacao_w,
	ie_clinica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

open C01;
loop
fetch C01 into
	nr_seq_w,
	nr_seq_etapa_padrao_w,
	nr_seq_motivo_dev_w,
	cd_setor_atend_etapa_w,
	ie_pessoa_etapa_w,
	ie_etapa_critica_w;
exit when C01%notfound;
	begin
	nr_seq_etapa_padrao_w	:= nr_seq_etapa_padrao_w;
	nr_seq_motivo_dev_w	:= nr_seq_motivo_dev_w;
	cd_setor_atend_etapa_w	:= cd_setor_atend_etapa_w;
	ie_pessoa_etapa_w	:= ie_pessoa_etapa_w;
	ie_etapa_critica_w	:= ie_etapa_critica_w;
	end;
end loop;
close C01;

select 	count(*)
into	qt_regra_restringe_w
from 	fatur_etapa_conta_conv
where 	nr_seq_regra = nr_seq_w
and 	cd_convenio = cd_convenio_w;

if	(qt_regra_restringe_w > 0) then
	nr_seq_etapa_padrao_w:= null;
end if;


if	(nr_seq_etapa_padrao_w is not null) then

	select	substr(Obter_Pessoa_Fisica_Usuario(decode(ie_pessoa_etapa_w, 'L', wheb_usuario_pck.get_nm_usuario , :new.nm_usuario_original),'C'),1,10) 
	into	cd_pessoa_fisica_w
	from	dual;

	select	conta_paciente_etapa_seq.NextVal
	into	nr_sequencia_w
	from 	dual;

	insert into conta_paciente_etapa (
		nr_sequencia,
		nr_interno_conta,
		dt_atualizacao,
		nm_usuario,
		dt_etapa,
		nr_seq_etapa,
		cd_setor_atendimento,
		cd_pessoa_fisica,
		nr_seq_motivo_dev,
		ds_observacao,
		nr_lote_barras,
		ie_etapa_critica)
	values	(nr_sequencia_w,
		:new.nr_interno_conta,
		sysdate,
		:new.nm_usuario,
		sysdate,
		nr_seq_etapa_padrao_w,
		cd_setor_atend_etapa_w,
		cd_pessoa_fisica_w,
		nr_seq_motivo_dev_w,
		substr(wheb_mensagem_pck.get_texto(304654),1,1999), --Etapa padr�o gerada na cria��o da conta
		null,
		ie_etapa_critica_w);

end if;

select	count(1)
into	qt_regra_w
from	regra_prontuario_gestao
where	cd_estabelecimento					= cd_estabelecimento_w
and	nvl(IE_FORMA_GERACAO,'A') = 'O';

if	(qt_regra_w > 0) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;
	
	gerar_regra_prontuario_gestao(ie_tipo_atendimento_w,nvl(cd_estabelecimento_w,0),:new.nr_atendimento,cd_pessoa_fisica_w,wheb_usuario_pck.get_nm_usuario,
					null,
					null,
					null,
					ie_clinica_w,
					cd_setor_atend_etapa_w,
					null,
					null,
					nvl(:new.nr_interno_conta,:old.nr_interno_conta),
					:new.dt_periodo_inicial,
					:new.dt_periodo_final);
end if;

end;
/