create or replace
procedure desfazer_lib_emprestimo ( nr_emprestimo_p number ) is

begin

update 	emprestimo 
set 	dt_liberacao = null 
where 	nr_emprestimo = nr_emprestimo_p;

commit;

end desfazer_lib_emprestimo;
/