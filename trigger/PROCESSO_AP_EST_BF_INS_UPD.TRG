create or replace trigger processo_ap_est_bf_ins_upd
before insert or update on processo_aprov_estrut
for each row

begin

    if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado. 
    end if;

end;
/
