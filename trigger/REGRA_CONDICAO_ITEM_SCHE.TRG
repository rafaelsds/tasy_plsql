create or replace trigger REGRA_CONDICAO_ITEM_sche
after insert or delete or update on REGRA_CONDICAO_ITEM
for each row
begin
  schematics_alt_pck.prc_regra_condicao_item(nr_seq_obj_schem_p => nvl(:new.nr_seq_objeto_schematic,
                                                                       :old.nr_seq_objeto_schematic));
end;
/
