create or replace trigger exame_lab_result_item_bef_upd
before update on exame_lab_result_item
for each row

declare
nr_prescricao_w		exame_lab_resultado.nr_prescricao%type;
cd_estabelecimento_w	prescr_medica.cd_estabelecimento%type;
nr_atendimento_w	prescr_medica.nr_atendimento%type;
ie_imp_cultura_aprov_w	cih_parametros.ie_imp_cultura_aprov%type;
cd_medico_resp_w	atendimento_paciente.cd_medico_resp%type;
ie_clinica_w		atendimento_paciente.ie_clinica%type;
nr_ficha_ocorrencia_w	cih_ficha_ocorrencia.nr_ficha_ocorrencia%type;
ie_libera_ficha_w	Varchar2(255);
qt_ficha_ocorrencia_w	Number(5);

pragma autonomous_transaction;

begin
if	((:new.dt_aprovacao is not null) and
	(:old.dt_aprovacao is null)) then

	select 	max(nr_prescricao)
	into	nr_prescricao_w
	from 	exame_lab_resultado
	where 	nr_seq_resultado = :new.nr_seq_resultado;

	select	nvl(max(cd_estabelecimento),1),
		max(nr_atendimento)
	into	cd_estabelecimento_w,
		nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_w;

	select	nvl(max(ie_imp_cultura_aprov),'N')
	into	ie_imp_cultura_aprov_w
	from	cih_parametros
	where	cd_estabelecimento = cd_estabelecimento_w;		

	if	(ie_imp_cultura_aprov_w = 'S') then

		select	count(*)
		into	qt_ficha_ocorrencia_w
		from	cih_ficha_ocorrencia
		where	nr_atendimento = nr_atendimento_w;			

		if	(qt_ficha_ocorrencia_w = 0) then
		
			select	max(cd_medico_resp),
				max(ie_clinica)
			into	cd_medico_resp_w,
				ie_clinica_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_w;
			
			Obter_Param_Usuario(936,23,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_libera_ficha_w);
						
			ccih_gerar_ficha_ocorrencia(	nr_atendimento_w,
							null,
							cd_medico_resp_w,
							:new.nm_usuario,
							ie_libera_ficha_w,
							ie_clinica_w);
		end if;
		
		select	max(nr_ficha_ocorrencia)
		into	nr_ficha_ocorrencia_w
		from	cih_ficha_ocorrencia
		where	nr_atendimento = nr_atendimento_w;
			
		if	(nvl(nr_ficha_ocorrencia_w,0) > 0) then
			cih_importar_cultura(	nr_ficha_ocorrencia_w, :new.nm_usuario);
		end if;
		commit;	
	end if;
end if;
end;
/
