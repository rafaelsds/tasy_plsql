create or replace trigger pls_regra_copartic_util_update
before update on pls_regra_copartic_util
for each row

declare

begin
if	(:new.ie_eventos_incidencia <> :old.ie_eventos_incidencia) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_eventos_incidencia, :new.ie_eventos_incidencia, 'PLS_REGRA_COPARTIC_UTIL', 'IE_EVENTOS_INCIDENCIA', :new.nm_usuario);
end if;
if	(:new.qt_evento_minimo <> :old.qt_evento_minimo) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_evento_minimo, :new.qt_evento_minimo, 'PLS_REGRA_COPARTIC_UTIL', 'QT_EVENTO_MINIMO', :new.nm_usuario);
end if;
if	(:new.nr_seq_grupo_serv <> :old.nr_seq_grupo_serv) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_grupo_serv, :new.nr_seq_grupo_serv, 'PLS_REGRA_COPARTIC_UTIL', 'NR_SEQ_GRUPO_SERV', :new.nm_usuario);
end if;
if	(:new.qt_evento_maximo <> :old.qt_evento_maximo) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_evento_maximo, :new.qt_evento_maximo, 'PLS_REGRA_COPARTIC_UTIL', 'QT_EVENTO_MAXIMO', :new.nm_usuario);
end if;
if	(:new.ie_tipo_data_consistencia <> :old.ie_tipo_data_consistencia) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_tipo_data_consistencia, :new.ie_tipo_data_consistencia, 'PLS_REGRA_COPARTIC_UTIL', 'IE_TIPO_DATA_CONSISTENCIA', :new.nm_usuario);
end if;
if	(:new.qt_periodo_ocorrencia <> :old.qt_periodo_ocorrencia) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_periodo_ocorrencia, :new.qt_periodo_ocorrencia, 'PLS_REGRA_COPARTIC_UTIL', 'QT_PERIODO_OCORRENCIA', :new.nm_usuario);
end if;
if	(:new.ie_tipo_periodo_ocor <> :old.ie_tipo_periodo_ocor) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_tipo_periodo_ocor, :new.ie_tipo_periodo_ocor, 'PLS_REGRA_COPARTIC_UTIL', 'IE_TIPO_PERIODO_OCOR', :new.nm_usuario);
end if;
end;
/