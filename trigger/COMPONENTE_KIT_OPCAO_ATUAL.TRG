create or replace trigger componente_kit_opcao_atual
before insert or update on componente_kit_opcao
for each row

declare

nr_seq_interna_w	number(10);

begin

if	(obter_se_prot_lib_regras = 'S') then
	select	nr_seq_protocolo
	into	nr_seq_interna_w
	from	kit_material
	where	cd_kit_material	= :new.cd_kit_material;

	update	protocolo_medicacao
	set	nm_usuario_aprov	=	null,
		dt_aprovacao		=	null,
		ie_status		=	'PA'
	where	nr_seq_interna		=	nr_seq_interna_w;
end if;

end;
/
