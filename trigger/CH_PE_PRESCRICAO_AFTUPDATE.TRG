create or replace trigger CH_PE_PRESCRICAO_aftupdate
after update on PE_PRESCRICAO
for each row

declare
qt_reg_w	number(10);
qt_acomp_w	number(10);
ie_lib_dieta_w 		varchar2(15);
ie_idade_w		number(10);
qt_atend_categ		number(10);

begin
if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	begin
	
	select	nvl(obter_idade_pf(obter_pessoa_atendimento(:new.nr_atendimento,'C'),sysdate,'A'),0)
	into	ie_idade_w
	from 	dual;
		
	if 	((ie_idade_w <= 8) or (obter_se_pac_isolamento(:new.nr_atendimento) = 'S')) then
		ie_lib_dieta_w := 'T';
	else	ie_lib_dieta_w := 'L';
	end if; 

	
	select	count(*)
	into	qt_reg_w
	from	PE_PRESCR_DIAG a
	where	a.nr_seq_diag	= 87
	and	a.nr_seq_prescr = :new.nr_sequencia;
	
	
	if	(qt_reg_w > 0) then
		
		select 	nvl(nr_acompanhante,0)
		into	qt_acomp_w
		from	atend_categoria_convenio
		where 	nr_atendimento = :new.nr_atendimento
		and	nr_seq_interno = obter_atecaco_atendimento(:new.nr_atendimento);

	
		if      (qt_acomp_w = 0 ) then 
			if ((obter_se_pac_isolamento(:new.nr_atendimento) = 'N') or 
			    (ie_idade_w > 18) or 
			    (ie_idade_w < 59)) then
			update  atend_categoria_convenio
			set 	nr_acompanhante = 1,
				qt_dieta_acomp = 1,
				ie_lib_dieta = ie_lib_dieta_w
			where   nr_atendimento =  :new.nr_atendimento;
			end if;
		
		end if;
	else 	
		atualizar_dados_convenio(:new.nr_atendimento,obter_setor_atendimento(:new.nr_atendimento),'N');
	end if;
	
	end;
end if;
end;
/

