create or replace trigger medico_vinculo_befupdate_log
before update on medico_vinculo
for each row

declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into medico_vinculo_log(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_medico,
		ie_vinculo_medico,
		dt_admissao,
		dt_efetivacao,
		dt_desligamento,
		ds_observacao,
		nr_seq_categoria)
	values( medico_vinculo_log_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.cd_medico,
		:old.ie_vinculo_medico,
		:old.dt_admissao,
		:old.dt_efetivacao,
		:old.dt_desligamento,
		:old.ds_observacao,
		:old.nr_seq_categoria);
end if;

end;
/