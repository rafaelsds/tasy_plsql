create or replace trigger pls_conta_delete
before delete on pls_conta
for each row

declare
Ds_aplicacao			Varchar2(50);
ds_log_w			Varchar2(1500);
ie_aplicacao_tasy_w		Varchar2(1);
nr_protocolo_prestador_w	pls_protocolo_conta.nr_protocolo_prestador%type;
nr_seq_prestador_prot_w		pls_protocolo_conta.nr_seq_prestador%type;
nr_seq_lote_conta_w		pls_protocolo_conta.nr_seq_lote_conta%type;
nr_protocolo_w			pls_protocolo_conta.nr_sequencia%type;
cd_estabelecimento_w		pls_protocolo_conta.cd_estabelecimento%type;
ie_permi_apres_exclui_w		pls_parametros.ie_permi_apres_exclui%type;

begin
if	(pls_se_aplicacao_tasy = 'S') then
	Ds_aplicacao 		:= 'Aplicacao TASY ;'; 
	ie_aplicacao_tasy_w	:= 'S';
else
	Ds_aplicacao 		:= 'Banco ;';
	ie_aplicacao_tasy_w	:= 'N';
end if;

begin
	select 	a.nr_seq_prestador,
		a.nr_seq_lote_conta,
		a.nr_protocolo_prestador,
		a.nr_sequencia,
		a.cd_estabelecimento
	into	nr_seq_prestador_prot_w,
		nr_seq_lote_conta_w,
		nr_protocolo_prestador_w,
		nr_protocolo_w,
		cd_estabelecimento_w
	from   	pls_protocolo_conta a
	where  	a.nr_sequencia  = :old.nr_seq_protocolo;
exception 
when others then
	nr_seq_prestador_prot_w := null;
	nr_seq_lote_conta_w := null;
	nr_protocolo_prestador_w := null;
	nr_protocolo_w := null;
end;

ds_log_w := substr(pls_obter_detalhe_exec(false),1,1500);

insert into pls_conta_exclusao
	(nr_sequencia, 
	nm_usuario, 
	nm_usuario_nrec,
	dt_atualizacao, 
	dt_atualizacao_nrec, 
	ds_funcao_ativa,
	ie_aplicacao_tasy, 
	nm_maquina, 
	ds_log,
	ds_log_call, 
	nr_seq_prestador_prot, 
	nr_seq_prestador_exec, 
	nr_seq_lote_conta, 
	nr_seq_conta, 
	nr_seq_segurado, 
	nr_seq_protocolo_origem, 
	cd_guia_referencia, 
	nr_protocolo_prestador, 
	nr_seq_protocolo,
	cd_estabelecimento)
values	(pls_conta_exclusao_seq.nextval,
	substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
	substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
	sysdate, 
	sysdate, 
	obter_funcao_ativa,
	ie_aplicacao_tasy_w, 
	wheb_usuario_pck.get_machine, 
	Ds_aplicacao	||' Seq.: ' || to_char(:old.nr_sequencia) || '; Seq. protocolo: ' || to_char(:old.nr_seq_protocolo) || '; ' || ' Beneficiario: ' || to_char(:old.nr_seq_segurado)
			||'; Seq. Prot. original '||to_char(:old.nr_seq_protocolo_origem)||'; Seq. prestador exec '||to_char(:old.nr_seq_prestador_exec)||'; CD guia '||:old.cd_guia
			||'; Guia refer�ncia '||:old.cd_guia_referencia, 
	ds_log_w, 
	nr_seq_prestador_prot_w, 
	:old.nr_seq_prestador_exec, 
	nr_seq_lote_conta_w, 
	:old.nr_sequencia, 
	:old.nr_seq_segurado, 
	:old.nr_seq_protocolo_origem, 
	:old.cd_guia, 
	nr_protocolo_prestador_w, 
	nr_protocolo_w,
	:old.cd_estabelecimento);

if	(:old.nr_seq_guia	is not null) and
	(:old.ie_origem_conta	= 'T') then
	
	select	max(ie_permi_apres_exclui)
	into	ie_permi_apres_exclui_w
	from	pls_parametros
	where	cd_estabelecimento	= cd_estabelecimento_w;
	
	if	(ie_permi_apres_exclui_w	= 'S') then
		delete 	pls_apres_automatica_guia
		where	nr_seq_conta = :old.nr_sequencia;
	end if;
	
	update	pls_guia_plano
	set	ie_pagamento_automatico = 'PA',
		ie_status 		= '1'
	where	nr_sequencia		= :old.nr_seq_guia
	and	ie_pagamento_automatico	= 'CG';
end if;

end;
/