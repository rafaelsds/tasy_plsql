create or replace trigger REGRA_NUMERACAO_DEC_ITEM_UP
before insert or update on REGRA_NUMERACAO_DEC_ITEM
for each row

declare

begin
if	((:old.IE_DISPONIVEL = 'S') or
	(:old.IE_DISPONIVEL = 'N')) and
	(:new.IE_DISPONIVEL = 'C')then 
	:new.dt_cancelamento := sysdate;
elsif	(:new.IE_DISPONIVEL <> 'C') then
	:new.dt_cancelamento := null;
end if;

end;
/