create or replace trigger conta_contabil_update
before update on conta_contabil
for each row

declare

ds_log_w	varchar2(2000);
qt_registro_w	number(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	if	not (:new.ie_centro_custo in ('S','N')) then
		/* O campo Exige Centro de Custo deve ter valor S, N */
		wheb_mensagem_pck.exibir_mensagem_abort(266390);
	end if;

	if	(obter_pais_sistema(wheb_usuario_pck.get_cd_perfil,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento) = 1) and
		(nvl(:new.ie_centro_custo,'X') <> nvl(:old.ie_centro_custo,'X')) then
		begin
		select	count(*)
		into	qt_registro_w
		from	(select	1
			from	ctb_movimento a
			where	a.cd_conta_credito = :new.cd_conta_contabil
			and	rownum = 1
			union all
			select	1
			from	ctb_movimento a
			where	a.cd_conta_debito = :new.cd_conta_contabil
			and	rownum = 1);

		if	(qt_registro_w > 0) then
			begin
			/* Esta conta contabil ja possui movimentacao na contabilidade, desta forma o detalhamento por centro de custo nao pode ser alterado */
			wheb_mensagem_pck.exibir_mensagem_abort(1009703);
			end;
		end if;

		end;
	end if;
	
	if	((:new.cd_classificacao <> :old.cd_classificacao) OR (:new.cd_classif_superior <> :old.cd_classif_superior)) then
		begin
		select	count(*)
		into	qt_registro_w
		from	(select	1
			from	ctb_saldo c
			where	c.cd_conta_contabil = :new.cd_conta_contabil
			and	rownum = 1);

		if	(qt_registro_w > 0) then
			begin			
			/*Nao eh permitido alterar a classificacao e classificacao superior da conta contabil #@CD_CONTA_CONTABIL#@ - #@DS_CONTA_CONTABIL#@, porque ela ja   possui movimentacao na contabilidade.
			Utilize a regra de classificacao por periodo para modificar a classificacao e classificacao superior desta conta.*/
			wheb_mensagem_pck.exibir_mensagem_abort(1141110, substr('CD_CONTA_CONTABIL=' || :new.cd_conta_contabil || ';' ||
										'DS_CONTA_CONTABIL=' || :new.ds_conta_contabil, 1, 2000));
			end;
		end if;

		end;
	end if;

	if	(:new.ie_tipo <> :old.ie_tipo) then
	/* Alteracao do TIPO da conta. De: #@OLD_IE_TIPO#@ Para: #@NEW_IE_TIPO#@ */
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(818830, 'OLD_IE_TIPO=' || :old.ie_tipo || ';NEW_IE_TIPO=' || :new.ie_tipo),1,500);
		ctb_gravar_log_conta(:new.cd_conta_contabil, ds_log_w, :new.nm_usuario);
	end if;

	if	(:new.cd_classificacao <> :old.cd_classificacao) then
	/* Alteracao da CLASSIFICACAO da conta. De: #@OLD_CD_CLASSIFICACAO#@ Para: #@NEW_CD_CLASSIFICACAO#@ */
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(818831, 'OLD_CD_CLASSIFICACAO=' || :old.cd_classificacao || ';NEW_CD_CLASSIFICACAO=' || :new.cd_classificacao),1,2000);
		ctb_gravar_log_conta(:new.cd_conta_contabil, ds_log_w, :new.nm_usuario);
	end if;

	if	(:new.cd_classif_superior <> :old.cd_classif_superior) then
	/* Alteracao da CLASSIFICACAO SUPERIOR da conta. De: #@OLD_CD_CLASSIF_SUPERIOR#@ Para: #@NEW_CD_CLASSIF_SUPERIOR#@ */
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(818832, 'OLD_CD_CLASSIF_SUPERIOR=' || :old.cd_classif_superior || ';NEW_CD_CLASSIF_SUPERIOR=' || :new.cd_classif_superior),1,2000);
		ctb_gravar_log_conta(:new.cd_conta_contabil, ds_log_w, :new.nm_usuario);
	end if;

	if	(:new.cd_grupo <> :old.cd_grupo) then
	/* Alteracao do GRUPO da conta. De: #@OLD_CD_GRUPO#@ Para: #@NEW_CD_GRUPO#@ */
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(818833, 'OLD_CD_GRUPO=' || :old.cd_grupo || ';NEW_CD_GRUPO=' || :new.cd_grupo),1,2000);
		ctb_gravar_log_conta(:new.cd_conta_contabil, ds_log_w, :new.nm_usuario);
	end if;
	
	end;
end if;

end;
/
