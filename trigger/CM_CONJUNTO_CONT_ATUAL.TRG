create or replace trigger cm_conjunto_cont_atual
before update on cm_conjunto_cont
for each row

declare
ds_status_ant_w		varchar2(255);
ds_status_novo_w		varchar2(255);
ds_local_estoque_w	varchar2(255);
ds_equipamento_w		varchar2(255);
nr_seq_equipamento_w	cm_equipamento.nr_sequencia%type;
ie_se_conjunto_equip_w	varchar2(1);

begin

if	(nvl(:old.DT_ORIGEM,sysdate+10) <> :new.DT_ORIGEM) and
	(:new.DT_ORIGEM is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ORIGEM, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.ie_status_conjunto <> :old.ie_status_conjunto) then
	begin
	
	select	substr(obter_valor_dominio(403,:old.ie_status_conjunto),1,255),
		substr(obter_valor_dominio(403,:new.ie_status_conjunto),1,255)
	into	ds_status_ant_w,
		ds_status_novo_w
	from	dual;
	
	cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311255,'DS_STATUS_ANT='||ds_status_ant_w||';DS_STATUS_NOVO='||ds_status_novo_w),:new.nm_usuario);
	
	update	cm_expurgo_retirada
	set	ie_status = :new.ie_status_conjunto
	where	nr_conjunto_cont = :new.nr_sequencia;

	update	cm_expurgo_receb
	set	ie_status = :new.ie_status_conjunto
	where	nr_conjunto_cont = :new.nr_sequencia;

	end;
end if;

if	(:new.cd_local_estoque <> :old.cd_local_estoque) then
	begin

	select	ds_local_estoque
	into	ds_local_estoque_w
	from	local_estoque
	where	cd_local_estoque = :new.cd_local_estoque;

	if	(ds_local_estoque_w is not null) then
		begin
		
		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311257,'DS_LOCAL_ESTOQUE='||ds_local_estoque_w),:new.nm_usuario);

		:new.nm_usuario_repasse	:= wheb_usuario_pck.get_nm_usuario;
		
		if	(:new.nr_seq_ciclo is not null) then
			cm_gerar_hist_ciclo(wheb_mensagem_pck.get_texto(311258,'NR_SEQUENCIA='||:new.nr_sequencia||';DS_LOCAL_ESTOQUE='||ds_local_estoque_w),'E',:new.nr_seq_ciclo,null,:new.nm_usuario);
		elsif	(:new.nr_seq_ciclo_lav is not null) then
			cm_gerar_hist_ciclo(wheb_mensagem_pck.get_texto(311258,'NR_SEQUENCIA='||:new.nr_sequencia||';DS_LOCAL_ESTOQUE='||ds_local_estoque_w),'E',null,:new.nr_seq_ciclo,:new.nm_usuario);
		end if;

		end;
	end if;

	end;
end if;

if	(nvl(:new.nr_seq_ciclo,0) <> nvl(:old.nr_seq_ciclo,0)) then
	begin

	if	(:old.nr_seq_ciclo is null) and
		(:new.nr_seq_ciclo is not null) then
		begin
		
		select	substr(cme_obter_desc_equip(nr_seq_equipamento),1,150),
			nr_seq_equipamento
		into	ds_equipamento_w,
			nr_seq_equipamento_w
		from	cm_ciclo
		where	nr_sequencia = :new.nr_seq_ciclo;
		
		ie_se_conjunto_equip_w := obter_se_conjunto_equip(:new.nr_seq_conjunto, nr_seq_equipamento_w);
		
		if	(ie_se_conjunto_equip_w = 'N') then
			wheb_mensagem_pck.exibir_mensagem_abort(767182);
		end if;
		
		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311325) || ' ' || :new.nr_seq_ciclo ||
							' - ' || wheb_mensagem_pck.get_texto(311259) || ': ' || ds_equipamento_w,:new.nm_usuario);
		
		end;
	elsif	(:old.nr_seq_ciclo is not null) and
		(:new.nr_seq_ciclo is null) then
		begin
		
		select	substr(cme_obter_desc_equip(nr_seq_equipamento),1,150)
		into	ds_equipamento_w
		from	cm_ciclo
		where	nr_sequencia = :old.nr_seq_ciclo;

		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311330) || ' ' || :old.nr_seq_ciclo ||
							' - ' || wheb_mensagem_pck.get_texto(311259) || ': ' || ds_equipamento_w,:new.nm_usuario);

		end;
	elsif	(:old.nr_seq_ciclo_lav is null) and
		(:new.nr_seq_ciclo_lav is not null) and (1=2) then
		begin
		
		select	substr(cme_obter_desc_equip(nr_seq_equipamento),1,150)
		into	ds_equipamento_w
		from	cm_ciclo_lavacao
		where	nr_sequencia = :new.nr_seq_ciclo_lav;

		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311325) || ' ' || :new.nr_seq_ciclo_lav ||
							' - ' || wheb_mensagem_pck.get_texto(311259) || ': ' || ds_equipamento_w,:new.nm_usuario);
		
		end;
	elsif	(:old.nr_seq_ciclo_lav is not null) and
		(:new.nr_seq_ciclo_lav is null) then
		begin
		
		select	substr(cme_obter_desc_equip(nr_seq_equipamento),1,150)
		into	ds_equipamento_w
		from	cm_ciclo_lavacao
		where	nr_sequencia = :old.nr_seq_ciclo_lav;

		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311330) || ' ' || :old.nr_seq_ciclo_lav ||
							' - ' || wheb_mensagem_pck.get_texto(311259) || ': ' || ds_equipamento_w,:new.nm_usuario);
		
		end;
	end if;

	end;
end if;		

if	(:new.cd_pessoa_receb is not null) and
	(:new.cd_pessoa_receb <> :old.cd_pessoa_receb) then
	begin

	cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311332,'NM_PESSOA='||substr(obter_nome_pf(:new.cd_pessoa_receb),1,180)),:new.nm_usuario);

	end;
end if;

if	(:new.nm_pessoa_receb is not null) and
	(:new.nm_pessoa_receb <> :old.nm_pessoa_receb) then
	begin

	cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311332,'NM_PESSOA='||substr(:new.nm_pessoa_receb,1,180)),:new.nm_usuario);

	end;
end if;

end;
/