CREATE OR REPLACE TRIGGER TIE_HL7_WORKLIST_PROCED_SUSP
  BEFORE UPDATE ON PRESCR_PROCEDIMENTO FOR EACH ROW
DECLARE
  
    nr_prescricao_w        prescr_medica.nr_prescricao%TYPE;
    cd_pessoa_fisica_w     prescr_medica.cd_pessoa_fisica%TYPE;
    nr_atendimento_w       prescr_medica.nr_atendimento%TYPE;
    cd_medico_w            prescr_medica.cd_medico%TYPE;
    cd_setor_atendimento_w prescr_medica.cd_setor_atendimento%TYPE;
    nr_seq_agenda_w        prescr_medica.nr_seq_agenda%TYPE;
    cd_estabelecimento_w   prescr_medica.cd_estabelecimento%TYPE;
    cd_identificador_w     VARCHAR(20 BYTE);
 
  PROCEDURE process_event_send_proced_susp IS
    JSON_W                   PHILIPS_JSON := PHILIPS_JSON();
    JSON_PATIENT_W           PHILIPS_JSON := PHILIPS_JSON();
    JSON_PRESC_MED_W         PHILIPS_JSON := PHILIPS_JSON();
    JSON_PRESC_PROCED_W      PHILIPS_JSON := PHILIPS_JSON();
    JSON_OBR_W               PHILIPS_JSON := PHILIPS_JSON();
    JSON_LIST_PRESC_PROCED_W PHILIPS_JSON_LIST := PHILIPS_JSON_LIST();
    JSON_DATA_W              CLOB;
    
    nm_pessoa_fisica_w       pessoa_fisica.nm_pessoa_fisica%TYPE;
    dt_nascimento_w          pessoa_fisica.dt_nascimento%TYPE;
    ie_sexo_w                pessoa_fisica.ie_sexo%TYPE;

    ds_given_name_w          person_name.ds_given_name%TYPE;
    ds_family_name_w         person_name.ds_family_name%TYPE;
    ds_component_name_1_w    person_name.ds_component_name_1%TYPE;

    ie_tipo_atendimento_w    atendimento_paciente.ie_tipo_atendimento%TYPE;
    hr_inicio_w              agenda_paciente.hr_inicio%TYPE;
    nr_prontuario_w          pessoa_fisica.nr_prontuario%TYPE;
    
    PROCEDURE obter_dados_pf(cd_pessoa_fisica_p pessoa_fisica.cd_pessoa_fisica%TYPE) IS
      nr_seq_person_name_w pessoa_fisica.nr_seq_person_name%TYPE;
    BEGIN
      nm_pessoa_fisica_w    := NULL;
      dt_nascimento_w       := NULL;
      Ie_Sexo_W             := NULL;
      ds_given_name_w       := NULL;
      ds_family_name_w      := NULL;
      ds_component_name_1_w := NULL;

      SELECT pf.nm_pessoa_fisica, pf.dt_nascimento, pf.ie_sexo, pf.nr_seq_person_name
        INTO nm_pessoa_fisica_w, dt_nascimento_w, ie_sexo_w, nr_seq_person_name_w
        FROM pessoa_fisica pf
       WHERE pf.cd_pessoa_fisica = cd_pessoa_fisica_p;

      IF (nr_seq_person_name_w IS NOT NULL) THEN
        SELECT MAX(pn.ds_given_name), MAX(pn.ds_family_name), MAX(pn.ds_component_name_1)
          INTO ds_given_name_w, ds_family_name_w, ds_component_name_1_w
          FROM person_name pn
         WHERE pn.nr_sequencia = nr_seq_person_name_w;
      END IF;

      IF (TRIM(ds_given_name_w) IS NULL
      AND TRIM(ds_family_name_w) IS NULL
      AND TRIM(ds_component_name_1_w) IS NULL) THEN
        SELECT obter_parte_nome_phillips_pf(nm_pessoa_fisica_w, 'nome'),
               Obter_Parte_Nome_Phillips_Pf(Nm_Pessoa_Fisica_W, 'sobrenome')
          INTO ds_given_name_w, ds_family_name_w
          FROM dual;
      END IF;
    END OBTER_DADOS_PF;
    
   BEGIN 
   
    JSON_W.PUT('sendingApplication', 'TASY');
	JSON_W.PUT('sendingFacility', 'PHILIPS');
	JSON_W.PUT('receivingApplication', 'VUEPACS');
	JSON_W.PUT('receivingFacility', 'PHILIPS');
	JSON_W.PUT('processingID', 'P');
    JSON_W.PUT('characterSet', '8859/1');

    obter_dados_pf(cd_pessoa_fisica_w);

    JSON_PATIENT_W.PUT('internalID', cd_pessoa_fisica_w);
    JSON_PATIENT_W.PUT('givenName', NVL(ds_given_name_w, nm_pessoa_fisica_w));
    JSON_PATIENT_W.PUT('surname', ds_family_name_w);
    JSON_PATIENT_W.PUT('dateOfBirth', TO_CHAR(dt_nascimento_w, 'MM/DD/YYYY'));
    JSON_PATIENT_W.PUT('sex', ie_sexo_w);
    JSON_W.PUT('PID', JSON_PATIENT_W.TO_JSON_VALUE());
    
     SELECT ap.ie_tipo_atendimento
      INTO ie_tipo_atendimento_w
      FROM atendimento_paciente ap
     WHERE ap.nr_atendimento = nr_atendimento_w;

    obter_dados_pf(cd_medico_w);

    JSON_PRESC_MED_W.PUT('patientClass', ie_tipo_atendimento_w);
    JSON_PRESC_MED_W.PUT('patientLocation', cd_setor_atendimento_w);
    JSON_PRESC_MED_W.PUT('attendingDoctor', cd_medico_w);
    JSON_PRESC_MED_W.PUT('doctorGivenName', NVL(ds_given_name_w, nm_pessoa_fisica_w));
    JSON_PRESC_MED_W.PUT('doctorSurname', ds_family_name_w);
    JSON_PRESC_MED_W.PUT('doctorOwnSurname', ds_component_name_1_w);
    JSON_PRESC_MED_W.PUT('patientType', 'N');
    JSON_PRESC_MED_W.PUT('visitNumber', nr_atendimento_w);
    JSON_W.PUT('PV1', JSON_PRESC_MED_W.TO_JSON_VALUE());
    
    IF (nr_seq_agenda_w IS NOT NULL) THEN
      SELECT ap.hr_inicio
        INTO hr_inicio_w
        FROM agenda_paciente ap
       WHERE ap.nr_sequencia = nr_seq_agenda_w;
    END IF;
    
    JSON_PRESC_PROCED_W.PUT('orderControl', 'CA');
    JSON_PRESC_PROCED_W.PUT('placerOrderNumber', :NEW.nr_acesso_dicom);
    JSON_PRESC_PROCED_W.PUT('fillerOrderNumber', :NEW.nr_acesso_dicom);
    JSON_PRESC_PROCED_W.PUT('placerGroupNumber', nr_prescricao_w);
    JSON_PRESC_PROCED_W.PUT('enteredBy', :NEW.nm_usuario_nrec);
    JSON_PRESC_PROCED_W.PUT('surname', obter_nome_usuario(:NEW.nm_usuario_nrec));
    JSON_PRESC_PROCED_W.PUT('orderingProvider', cd_medico_w);
    JSON_PRESC_PROCED_W.PUT('surnameORC', obter_nome_pf(cd_medico_w));
    
    JSON_LIST_PRESC_PROCED_W.APPEND(JSON_PRESC_PROCED_W.TO_JSON_VALUE());
    
    JSON_W.PUT('ORC', JSON_LIST_PRESC_PROCED_W);
    
    JSON_OBR_W.PUT('setIDOBR', '1');
    JSON_OBR_W.PUT('placerOrderNumberEntityIdentifier', :NEW.nr_prescricao);
    JSON_OBR_W.PUT('fillerOrderNumberEntityIdentifier', :NEW.nr_acesso_dicom);
    JSON_OBR_W.PUT('universalServiceIdentifier', :NEW.nr_seq_proc_interno);
    JSON_OBR_W.PUT('universalServiceName', obter_desc_proc_interno(:NEW.nr_seq_proc_interno));
    JSON_OBR_W.PUT('observationDateTime', TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
    JSON_OBR_W.PUT('collectionVolume', '1');
    IF (hr_inicio_w IS NOT NULL) THEN
      JSON_OBR_W.PUT('scheduledDateTime', TO_CHAR(hr_inicio_w, 'MM/DD/YYYY HH24:MI:SS'));
    END IF;
    
    JSON_W.PUT('OBR', JSON_OBR_W.TO_JSON_VALUE());

    SYS.DBMS_LOB.CREATETEMPORARY(JSON_DATA_W, TRUE);
    JSON_W.TO_CLOB(JSON_DATA_W);

    JSON_DATA_W := BIFROST.SEND_INTEGRATION_CONTENT('send.hl7.default.pacs.worklist_susp', JSON_DATA_W, :NEW.NM_USUARIO);
  END process_event_send_proced_susp;

BEGIN   
                            
  IF((:OLD.dt_suspensao IS NULL) AND (:NEW.dt_suspensao IS NOT NULL)) THEN
    
    SELECT pm.nr_prescricao,
           pm.cd_pessoa_fisica,
           pm.nr_atendimento,
           pm.cd_medico,
           pm.cd_setor_atendimento,
           pm.nr_seq_agenda,
           pm.cd_estabelecimento
      INTO nr_prescricao_w,
           cd_pessoa_fisica_w,
           nr_atendimento_w,
           cd_medico_w,
           cd_setor_atendimento_w,
           nr_seq_agenda_w,
           cd_estabelecimento_w
      FROM prescr_medica pm
	  WHERE pm.nr_prescricao = :NEW.nr_prescricao;
            
    IF (obter_se_integr_proc_interno(:NEW.nr_seq_proc_interno, 30 /*Carestream padrao HL7*/, null, :NEW.ie_lado, cd_estabelecimento_w) = 'S') THEN
      process_event_send_proced_susp;
    END IF;
  END IF;
END TIE_HL7_WORKLIST_PROCED_SUSP;
/