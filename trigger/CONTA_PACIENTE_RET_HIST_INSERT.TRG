create or replace trigger conta_paciente_ret_hist_insert
before insert or update on conta_paciente_ret_hist
for each row

declare

ie_acao_w		number(1);
vl_hist_old_w		number(15,2);
vl_hist_atual_w		number(15,2);

vl_saldo_w		number(15,2);

ie_situacao_w		varchar2(1);
vl_pendente_w		number(15,2);
vl_reapresentacao_w	number(15,2);
vl_glosa_devida_w	number(15,2);
vl_glosa_indevida_w	number(15,2);
vl_nao_auditado_w	number(15,2);
vl_inicial_w		number(15,2);
vl_recebido_w		number(15,2);
vl_perdas_w		number(15,2);
vl_nota_credito_w	number(15,2);

nr_interno_conta_w	number(10);
cd_autorizacao_w	varchar2(20);
nr_seq_ret_item_w	number(10);
ie_valor_amaior_w	varchar2(1);
cd_estabelecimento_w	number(4);
ds_consistencia_w	varchar2(255);
vl_soma_itens_w		number(15,2);

begin

select	nvl(max(ie_acao),-1)
into	ie_acao_w
from	hist_audit_conta_paciente
where	nr_sequencia	= :new.nr_seq_hist_audit;

select	max(a.nr_interno_conta)
into	nr_interno_conta_w
from	conta_paciente_retorno a
where	a.nr_sequencia	= :new.nr_seq_conpaci_ret;

if	(ie_acao_w = -1) then
	/* N�o existe hist�rico da auditoria.
	Verifique cadastro de hist�rico ou desabilite a gera��o de auditoria!
	Conta: NR_INTERNO_CONTA_W */
	wheb_mensagem_pck.exibir_mensagem_abort(180961,'NR_INTERNO_CONTA_W='||nr_interno_conta_w);
end if;

select	a.ie_situacao,
	a.vl_pendente,
	a.vl_reapresentacao,
	a.vl_glosa_devida,
	a.vl_glosa_indevida,
	a.vl_nao_auditado,
	a.vl_inicial,
	a.nr_interno_conta,
	a.cd_autorizacao,
	a.vl_recebido,
	nvl(a.vl_perdas, 0),
	b.cd_estabelecimento,
	nvl(a.vl_nota_credito, 0)
into	ie_situacao_w,
	vl_pendente_w,
	vl_reapresentacao_w,
	vl_glosa_devida_w,
	vl_glosa_indevida_w,
	vl_nao_auditado_w,
	vl_inicial_w,
	nr_interno_conta_w,
	cd_autorizacao_w,
	vl_recebido_w,
	vl_perdas_w,
	cd_estabelecimento_w,
	vl_nota_credito_w
from 	conta_paciente b,
	conta_paciente_retorno a
where 	a.nr_sequencia		= :new.nr_seq_conpaci_ret
and	a.nr_interno_conta	= b.nr_interno_conta;

Obter_Param_Usuario(66,18,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_valor_amaior_w);

vl_hist_old_w		:= nvl(:old.vl_historico,0);
vl_hist_atual_w		:= :new.vl_historico - vl_hist_old_w;

if	(vl_hist_atual_w = 0) then
	goto final;
end if;

if	(ie_acao_w <> 8) then -- afstringari 179913 22/01/2010

	if	(ie_acao_w = 5) then
		vl_pendente_w	:= vl_pendente_w + vl_hist_atual_w;
		if 	(vl_recebido_w  >= vl_hist_atual_w) then	
			vl_recebido_w		:= vl_recebido_w - vl_hist_atual_w;
			vl_hist_atual_w	:= 0;
		else	
			vl_hist_atual_w	:= vl_hist_atual_w - vl_recebido_w;
			vl_recebido_w		:= 0;
			vl_inicial_w		:= vl_inicial_w + vl_hist_atual_w;
			vl_hist_atual_w 	:= 0;
		end if;	
	end if;

	vl_saldo_w	:= vl_pendente_w + vl_glosa_indevida_w + vl_reapresentacao_w + vl_nao_auditado_w;

	if (nvl(vl_pendente_w,0) = 0) then
		--A conta #@NR_INTERNO_CONTA_P#@ j� est� em auditoria. O processo deve ser efetuado pela Conta Paciente Auditoria!
		ds_consistencia_w	:= wheb_mensagem_pck.get_texto(304653,'NR_INTERNO_CONTA_P='|| nr_interno_conta_w);
	end if;

	if	(vl_saldo_w < vl_hist_atual_w) then
		/* Valor do hist�rico supera o valor pendente!
		Guia: cd_autorizacao_w
		Vl saldo: vl_saldo_w
		Vl hist atual: vl_hist_atual_w */
		wheb_mensagem_pck.exibir_mensagem_abort(180962,	'DS_CONSISTENCIA_W='||ds_consistencia_w||';'||
								'CD_AUTORIZACAO_W='||cd_autorizacao_w||';'||
								'VL_SALDO_W='||vl_saldo_w||';'||
								'VL_HIST_ATUAL_W='||vl_hist_atual_w);
	else
		if	(ie_acao_w = 0) then
			if	(vl_pendente_w < vl_hist_atual_w) then
				/* O hist�rico de n�o auditado s� pode ser lan�ado caso n�o tenha sido lan�ado outros hist�ricos.
				Guia: cd_autorizacao_w */
				wheb_mensagem_pck.exibir_mensagem_abort(180967,	'CD_AUTORIZACAO_W='||cd_autorizacao_w);
			else
				vl_pendente_w	:= vl_pendente_w - vl_hist_atual_w;
			end if;
		elsif 	(ie_acao_w = 7) then
			vl_pendente_w		:= vl_pendente_w - vl_hist_atual_w;
		elsif 	(ie_acao_w = 9) then
			vl_pendente_w		:= vl_pendente_w - vl_hist_atual_w;	
		elsif 	(ie_acao_w <> 5) then
			if	(vl_pendente_w >= vl_hist_atual_w) then
				vl_pendente_w	:= vl_pendente_w - vl_hist_atual_w;
				vl_hist_atual_w	:= 0;
			else
				vl_hist_atual_w	:= vl_hist_atual_w - vl_pendente_w;
				vl_pendente_w	:= 0;

				if	(ie_acao_w <> 2) and
					(vl_reapresentacao_w >= vl_hist_atual_w) then
					vl_reapresentacao_w	:= vl_reapresentacao_w - vl_hist_atual_w;
					vl_hist_atual_w		:= 0;
				else
					if	(ie_acao_w <> 2) then
						vl_hist_atual_w		:= vl_hist_atual_w - vl_reapresentacao_w;
						vl_reapresentacao_w	:= 0;
					end if;

					if	(ie_acao_w <> 4) and
						(vl_glosa_indevida_w >= vl_hist_atual_w) then
						vl_glosa_indevida_w	:= vl_glosa_indevida_w - vl_hist_atual_w;
						vl_hist_atual_w		:= 0;
					else
						if	(ie_acao_w <> 4) then
							vl_hist_atual_w	:= vl_hist_atual_w - vl_reapresentacao_w;
							vl_glosa_indevida_w	:= 0;
						end if;		

						if	(vl_nao_auditado_w >= vl_hist_atual_w) then
							vl_nao_auditado_w	:= vl_nao_auditado_w - vl_hist_atual_w;
							vl_hist_atual_w	:= 0;
						else
							/* Valor do hist�rico supera o valor pendente!
							Guia: cd_autorizacao_w */
							wheb_mensagem_pck.exibir_mensagem_abort(180970,	'CD_AUTORIZACAO_W='||cd_autorizacao_w);
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;

	if	(ie_acao_w = 0) then
		vl_nao_auditado_w	:= vl_nao_auditado_w + :new.vl_historico - vl_hist_old_w;
	elsif	(ie_acao_w = 1) then
		vl_recebido_w		:= vl_recebido_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 2) then
		vl_reapresentacao_w	:= vl_reapresentacao_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 3) then
		vl_glosa_devida_w	:= vl_glosa_devida_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 4) then
		vl_glosa_indevida_w	:= vl_glosa_indevida_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 7) then
		vl_perdas_w		:= vl_perdas_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 9) then
		vl_nota_credito_w	:= :new.vl_historico - vl_hist_old_w;		
	end if;

	vl_soma_itens_w	:= vl_pendente_w + vl_glosa_devida_w + vl_glosa_indevida_w + vl_recebido_w + vl_reapresentacao_w + vl_nao_auditado_w + vl_perdas_w + vl_nota_credito_w;

	if	(vl_soma_itens_w <> vl_inicial_w) and
		(nvl(ie_valor_amaior_w,'N') = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(180971,	'CD_AUTORIZACAO_W='||cd_autorizacao_w||';'||
								'NR_INTERNO_CONTA_W='||nr_interno_conta_w||';'||
								'VL_SOMA_ITENS_W='||vl_soma_itens_w||';'||
								'VL_INICIAL_W='||vl_inicial_w||';'||
								'VL_PENDENTE_W='||vl_pendente_w||';'||
								'VL_GLOSA_DEVIDA_W='||vl_glosa_devida_w||';'||
								'VL_GLOSA_INDEVIDA_W='||vl_glosa_indevida_w||';'||
								'VL_RECEBIDO_W='||vl_recebido_w||';'||
								'VL_REAPRESENTACAO_W='||vl_reapresentacao_w||';'||
								'VL_NAO_AUDITADO_W='||vl_nao_auditado_w||';'||
								'VL_PERDAS_W='||vl_perdas_w||';'||
								'VL_NOTA_CREDITO_W='||vl_nota_credito_w);
	end if;
end if;

if	(vl_pendente_w = 0) and
	(vl_glosa_indevida_w = 0) and
	(vl_reapresentacao_w = 0)  and
	(vl_nao_auditado_w = 0) then
	ie_situacao_w := 'F';
end if;

if	(:new.nr_seq_ret_item is null) then
	select 	max(a.nr_sequencia) 
	into 	nr_seq_ret_item_w
	from	convenio_retorno b,
		convenio_retorno_item a
	where 	a.nr_seq_retorno	= b.nr_sequencia
	and 	b.ie_status_retorno	= 'F'
	and 	nr_interno_conta	= nr_interno_conta_w
	and 	cd_autorizacao		= cd_autorizacao_w;
	
	:new.nr_seq_ret_item 		:= nr_seq_ret_item_w; 
end if;

update conta_paciente_retorno
set	ie_situacao 		= ie_situacao_w,
	vl_pendente 		= vl_pendente_w,
	vl_reapresentacao	= vl_reapresentacao_w,
	vl_glosa_devida		= vl_glosa_devida_w,
	vl_glosa_indevida	= vl_glosa_indevida_w,
	vl_nao_auditado 	= vl_nao_auditado_w,
	vl_recebido		= vl_recebido_w,
	vl_inicial		= vl_inicial_w,
	vl_perdas		= vl_perdas_w,
	vl_nota_credito		= vl_nota_credito_w
where nr_sequencia 		= :new.nr_seq_conpaci_ret;

 <<final>>

vl_inicial_w := vl_inicial_w;

end;
/
