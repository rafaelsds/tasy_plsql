create or replace trigger regra_pf_codigo_externo
before insert on  pf_codigo_externo
for each row

declare

nr_ano_w			        number(4);
nr_seq_controle_w		    controle_pf_codigo_externo.nr_sequencia%type;
cd_estabelecimento_w	    controle_pf_codigo_externo.cd_estabelecimento%type;
de_mascara_cd_externo_w	    controle_pf_codigo_externo.ds_mascara_cd_externo%type;
ie_tipo_codigo_externo_w	controle_pf_codigo_externo.ie_tipo_codigo_externo%type;
cd_ultimo_pf_externo_w	    controle_pf_codigo_externo.cd_ultimo_pf_externo%type;

ind_w	                    int default 1;
count_e	                    int default 0;
count_y	                    int default 0;
count_d	                    int default 0;

begin

if(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

	if(:new.cd_estabelecimento is null) then
		:new.cd_estabelecimento:= wheb_usuario_pck.get_cd_estabelecimento;
	end if;

	if	(gerar_int_padrao.get_executando_recebimento = 'N') then

		if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_AT') then

			lock table controle_pf_codigo_externo in exclusive mode;

			select	max(cce.nr_sequencia),
					max(UPPER(cce.ds_mascara_cd_externo)),
					max(cce.ie_tipo_codigo_externo),
					max(cce.cd_estabelecimento),
					pkg_date_utils.extract_field('YEAR', :new.dt_atualizacao_nrec, 0),
					nvl(max(cce.cd_ultimo_pf_externo), 0) +1
			into	nr_seq_controle_w,
					de_mascara_cd_externo_w,
					ie_tipo_codigo_externo_w,
					cd_estabelecimento_w,
					nr_ano_w,
					cd_ultimo_pf_externo_w
			from	controle_pf_codigo_externo cce
			where	cce.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
			and     cce.ie_tipo_codigo_externo = :new.ie_tipo_codigo_externo
			and		nvl(cce.dt_inicio_vigencia, sysdate) <= nvl(cce.dt_fim_vigencia, sysdate)
			and		nvl(cce.dt_inicio_vigencia, truncar_data(sysdate,'D')) <= truncar_data(sysdate,'D')
			and		nvl(cce.dt_fim_vigencia, truncar_data(sysdate,'D')) >= truncar_data(sysdate,'D');

			if (cd_estabelecimento_w is not null) then

				while (length(de_mascara_cd_externo_w) >= ind_w) loop
					if (substr(de_mascara_cd_externo_w, ind_w, 1) = 'E') then
						count_e := count_e + 1;
					elsif (substr(de_mascara_cd_externo_w, ind_w, 1) = 'Y') then
						count_y := count_y + 1;
					elsif (substr(de_mascara_cd_externo_w, ind_w, 1) = 'D') then
						count_d := count_d + 1;
					end if;

					ind_w := ind_w + 1;
				end loop;

				if (count_y < 4) then
					nr_ano_w := to_number(substr(nr_ano_w,5-count_y,count_y));
				end if;

				if (cd_ultimo_pf_externo_w <= 1) then

					select	(lpad(cd_estabelecimento_w,count_e,'0') || nr_ano_w || lpad(cd_ultimo_pf_externo_w,count_d,'0'))
					into	:new.cd_pessoa_fisica_externo
					from	dual;

					update	controle_pf_codigo_externo
					set		cd_ultimo_pf_externo = :new.cd_pessoa_fisica_externo,
							dt_atualizacao = sysdate,
							nm_usuario = :new.nm_usuario
					where	nr_sequencia = nr_seq_controle_w;

				else

					select	lpad(cd_ultimo_pf_externo_w, (count_e+count_y+count_d), '0')
					into	:new.cd_pessoa_fisica_externo
					from	dual;

					update	controle_pf_codigo_externo
					set		cd_ultimo_pf_externo = :new.cd_pessoa_fisica_externo,
							dt_atualizacao = sysdate,
							nm_usuario = :new.nm_usuario
					where	nr_sequencia = nr_seq_controle_w; 

				end if; 

			end if;

		end if;
	end if;
	
end if;	
	
end;
/

