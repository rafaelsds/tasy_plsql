create or replace trigger prescr_procedimento_aftupdate
after update on prescr_procedimento
for each row
declare
reg_integracao_w	gerar_int_padrao.reg_integracao;
nr_seq_exame_w		prescr_procedimento.nr_seq_exame%type;
nr_atendimento_w	prescr_medica.nr_atendimento%type;
ie_anatomia_patologica_w exame_laboratorio.ie_anatomia_patologica%type;
cd_perfil_w					number(15);
cd_estabelecimento_w		number(15);

	procedure gerar_log_rastre_prescr is 
		ds_alteracao_rastre_w		varchar2(1800);	
	begin
		if (obter_se_info_rastre_prescr('D', wheb_usuario_pck.get_nm_usuario, cd_perfil_w, cd_estabelecimento_w) = 'S') then
			if	(nvl(:new.dt_prev_execucao, sysdate-1) <> nvl(:old.dt_prev_execucao, sysdate-1)) then
				ds_alteracao_rastre_w := substr(ds_alteracao_rastre_w || ' DT_PREV_EXECUCAO(' 
					|| nvl(to_char(:old.dt_prev_execucao, 'YYYY-MM-DD HH24:MI:SS'), '0') || '/' || nvl(to_char(:new.dt_prev_execucao, 'YYYY-MM-DD HH24:MI:SS'), '0')||'); ',1,1800);
			end if;

			if	(ds_alteracao_rastre_w is not null) then
				ds_alteracao_rastre_w := substr('Rastreabilidade DT_PREV_EXECUCAO Alterações(old/new)= ' || 'NR_SEQUENCIA(' || :new.nr_sequencia || ') ' || ds_alteracao_rastre_w 
					|| ' FUNCAO('||to_char(wheb_usuario_pck.get_cd_funcao)||'); PERFIL('||to_char(cd_perfil_w)||')',1,1800);
				gerar_log_prescricao(:new.nr_prescricao, null, null, null, null, ds_alteracao_rastre_w, wheb_usuario_pck.get_nm_usuario, 70497, 'N');
			end if;
		end if;
	end;

begin

cd_perfil_w				:= obter_perfil_ativo;
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

gerar_log_rastre_prescr;

if	(:new.dt_suspensao is not null) and
	(:old.dt_suspensao is null) and
	(:new.nr_seq_exame is not null) then	
	begin
	select 	max(nvl(ie_anatomia_patologica,'N'))
	into	ie_anatomia_patologica_w
	from 	exame_laboratorio
	where	nr_seq_exame = :new.nr_seq_exame;
	
	if 	(ie_anatomia_patologica_w <> 'S') and
		(obter_qtd_exame_lab_pragma(:new.nr_prescricao, :new.nr_seq_interno) = 0) then
		begin
		nr_atendimento_w := obter_valores_prescr_trigger(:new.nr_prescricao,'A');
		
		select	max(ie_tipo_atendimento),
				max(obter_convenio_atendimento(nr_atendimento)),
				max(obter_categoria_atendimento(nr_atendimento))
		into	reg_integracao_w.ie_tipo_atendimento,
				reg_integracao_w.cd_convenio,
				reg_integracao_w.cd_categoria
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_w;

		reg_integracao_w.cd_estab_documento := nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
		reg_integracao_w.cd_procedimento 	:= :new.cd_procedimento;
		reg_integracao_w.nr_seq_exame 		:= :new.nr_seq_exame;
		gerar_int_padrao.gravar_integracao('144', :new.nr_prescricao, :new.nm_usuario, reg_integracao_w);
		end;
	end if;
	end;
end if;
end;
/
