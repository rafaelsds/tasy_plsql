create or replace trigger funcao_param_perfil_delete
before delete on funcao_param_perfil
for each row
declare

ie_protocolo_vig_w	varchar2(1);
	
Cursor C01 is
	select 	distinct 
			nr_seq_protocolo
	from   	concorde_perfil
	where  	cd_perfil = :old.cd_perfil
	order by 1;	

c01_w				C01%rowtype;
	
begin

if 	(:old.vl_parametro is not null) then
	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		begin
			select 	decode(count(*),0,'N','S')
			into   	ie_protocolo_vig_w
			from	concorde_protocolo
			where   nr_sequencia = c01_w.nr_seq_protocolo
			and		sysdate between dt_inicio_vigencia and dt_fim_vigencia; 
			
			if (ie_protocolo_vig_w = 'S') then
				update 	concorde_protocolo
				set		dt_alteracao = sysdate,
						dt_atualizacao = sysdate,
						nm_usuario = :old.nm_usuario
				where 	nr_sequencia = c01_w.nr_seq_protocolo;
				
				update 	perfil
				set		dt_alteracao = sysdate,
						dt_atualizacao = sysdate,
						nm_usuario = :old.nm_usuario
				where 	cd_perfil = :old.cd_perfil;
				
			end if;
		end;
	end loop;
	close C01;
end if;
end;
/