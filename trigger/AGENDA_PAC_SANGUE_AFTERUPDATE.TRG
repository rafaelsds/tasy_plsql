create or replace trigger agenda_pac_sangue_afterupdate
after update on agenda_pac_sangue
for each row
declare
ds_acao_w	varchar2(255);
begin
if	(:old.nr_seq_status is not null) and
	(:new.nr_seq_status <> :old.nr_seq_status) then
	ds_acao_w := Wheb_mensagem_pck.get_texto(307613, 'DS_STATUS_SANGUE_OLD='||obter_desc_status_sangue(:old.nr_seq_status)||';DS_STATUS_SANGUE_NEW='||obter_desc_status_sangue(:new.nr_seq_status));  --'Altera��o do status do hemocomponente de '||obter_desc_status_sangue(:old.nr_seq_status)||' para '||obter_desc_status_sangue(:new.nr_seq_status);
	gerar_hist_agenda_pac_sangue(:new.nr_sequencia,1,ds_acao_w,:new.nm_usuario);
end if;	

end;
/