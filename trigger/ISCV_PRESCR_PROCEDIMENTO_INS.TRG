create or replace trigger ISCV_PRESCR_PROCEDIMENTO_INS
after insert on prescr_procedimento
for each row

declare
ie_proc_integra_w	varchar2(10);
ds_sep_bv_w			varchar2(100)	:= obter_separador_bv;
cd_pessoa_fisica_w	atendimento_paciente.cd_pessoa_fisica%type;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;

function permiteIntegrarISCV
return boolean is
qt_resultado_w	number;
begin

select	count(*) qt_resultado
into	qt_resultado_w
from 	regra_proc_interno_integra
where	ie_tipo_integracao = 17; --Dominio criado para a integra��o

return qt_resultado_w > 0;
end;

begin
--N�o considera libera��o da prescri��o pois na cirurgia a prescri��o n�o � liberada
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') and
	(permiteIntegrarISCV()) and
	((:new.cd_funcao_origem = 900) or
	(:new.cd_funcao_origem = 872) or
	(:new.cd_funcao_origem = 871)) then
	ie_proc_integra_w := Obter_Se_Integr_Proc_Interno(:new.nr_seq_proc_interno, 17,null,:new.ie_lado,wheb_usuario_pck.get_cd_estabelecimento);
	if	(ie_proc_integra_w = 'S') and
		(:new.nr_acesso_dicom is not null) then	
		select	max(a.cd_pessoa_fisica),
				max(a.nr_atendimento)
		into	cd_pessoa_fisica_w,
				nr_atendimento_w
		from	atendimento_paciente a,
				prescr_medica b
		where	a.nr_atendimento = b.nr_atendimento
		and		b.nr_prescricao = :new.nr_prescricao;
		
		
		gravar_agend_integracao(745, 'nr_atendimento=' || nr_atendimento_w || ds_sep_bv_w || 'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w, :new.cd_setor_atendimento);		
		gravar_agend_integracao(749,'cd_pessoa_fisica='	|| cd_pessoa_fisica_w	|| ds_sep_bv_w ||
									'nr_atendimento='	|| nr_atendimento_w		|| ds_sep_bv_w ||
									'nr_prescricao='	|| :new.nr_prescricao		|| ds_sep_bv_w ||
									'nr_acesso_dicom='	|| :new.nr_acesso_dicom		|| ds_sep_bv_w ||
									'order_control=NW'	|| ds_sep_bv_w				||
									'order_status=SC'	|| ds_sep_bv_w
									, :new.cd_setor_atendimento);				
	end if;
end if;

end;
/