create or replace trigger sismama_mam_anamnese_atual
before insert or update on SISMAMA_MAM_ANAMNESE
for each row

declare

nr_data_mamo_w	number(10);
nr_atendimento_w	number(10);
dt_nascimento_w		date;

begin

if	(:new.DT_ULTIMA_MAMOGRAFIA	is not null) then
	nr_data_mamo_w	:= somente_numero(:new.DT_ULTIMA_MAMOGRAFIA);
	
	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	SISMAMA_ATENDIMENTO
	where	nr_sequencia	= :new.nr_seq_sismama;
	
	select	max(a.dt_nascimento)
	into	dt_nascimento_w
	from	atendimento_paciente b,
		pessoa_fisica a
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	b.nr_atendimento	= nr_atendimento_w;	
	
	
	if	(nr_data_mamo_w	> 0) then
	   	
		if	((nr_data_mamo_w	> to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(sysdate),'yyyy')) or 
			(dt_nascimento_w is not null and nr_data_mamo_w	< to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_nascimento_w),'yyyy'))) then
			WHEB_MENSAGEM_PCK.EXIBIR_MENSAGEM_ABORT(235879); 
		
		end if;
		
	end if;
	
	
end if;

end;
/