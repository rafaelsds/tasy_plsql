create or replace
trigger atendimento_paciente_insert
after insert on atendimento_paciente
for each row

declare
nr_prontuario_w    number(10,0);
ie_prontuario_w    varchar2(01);
ie_data_futura_w    varchar2(01);
qt_registro_w    number(10,0);
cd_medico_w    number(10,0);
ds_erro_w    varchar2(500);
ie_gerar_w    varchar2(01);
qt_validade_w    number(15,2);
ie_vincular_w    varchar2(01);
nr_prescricao_w    number(14);
ie_gerar_ccih_w    varchar2(1);
ds_perfil_w    varchar2(30);
qt_dias_w    number(5);
ie_permissao_aten_w  varchar2(1);
qt_disp_diaria_w    number(15,0);
nr_seq_troca_w    number(10);
nr_seq_orcamento_w  number(10);
ie_vincular_orcamento_w  varchar2(1);
dt_alta_institucional_w  date;
ie_prontuario_anual_w  varchar2(10);
ie_gera_prontuario_w  varchar2(1);
ie_gerar_anual_w    varchar2(1);
ie_forma_gerar_w    varchar2(1);
ie_gera_pront_rn_w   varchar2(1);
qt_medico_ativo_w      number(5);
qt_medico_w        number(5);
Ie_permite_pessoa_resp_w  varchar2(1);
ie_gerar_acesso_atend_w    varchar2(1);
ie_tipo_atend_pront_w  varchar2(1);
ie_escolhe_especialidade_w  varchar2(1);
qt_especialidade_w    number(10);
cd_especialidade_w    number(10);
ie_perm_data_menor_nasc_w  varchar2(1);
dt_nasc_paciente_w    date;
ie_integracao_aghos_w    varchar2(1);
ie_integra_regul_w    varchar2(1);
nr_seq_solici_aghos_w    number(10);
nr_internacao_w      number(10);
nr_seq_pac_visita_w    number(10);
ie_atend_sem_autorizacao_w  varchar2(1);
nm_pessoa_fisica_w    varchar2(60);
ie_inicia_atend_vinc_senha_w  varchar2(1);
ie_mostra_erro_aghos_w    varchar2(1);
ds_sqlerror_w      varchar2(2000);
ds_erro_aghos_w      varchar2(2000);
qt_dias_dispensa_w    number(10,0);
cd_pessoa_usuario_w    varchar2(10);
ie_vincular_senha_w    varchar2(1);
VarRestringInformacoes_w  varchar2(255);
ie_regra_gerar_pac_vacina_w  varchar2(1);
ie_solic_aghos_w    varchar2(1);
nr_grupo_medico_novo_w    NUMBER (10,0);
nr_seq_regulacao_w    Number(10);
ie_vinculo_medico_ref_w  Number(2);
ie_possui_autor_gerint_w  varchar2(1);
ie_admission_diagnosis_w  varchar2(1);
qt_tempo_regra_w     wl_regra_item.qt_tempo_regra%type;
cd_tipo_evolucao_w    wl_regra_item.ie_evolucao_clinica%type;
qt_tempo_atraso_w    wl_regra_item.qt_tempo_atraso%type;
qt_tempo_normal_w    wl_regra_item.qt_tempo_normal%type;
ie_opcao_wl_w      wl_regra_item.ie_opcao_wl%type;
qt_regra_escalas_w  number(10);
nr_seq_regra_w    wl_regra_item.nr_sequencia%type;
ie_escala_w      wl_regra_item.ie_escala%type;
ie_tipo_diagnostico_w    wl_regra_item.ie_tipo_diagnostico%type;
nm_tabela_w    vice_escala.nm_tabela%type;
ie_classif_diag_w  wl_regra_item.ie_classif_diag%type;
ie_cons_episodio_w    varchar2(1);
ie_usa_case_w      varchar2(1);
ATEND_PACIENTE_ADIC_W ATEND_PACIENTE_ADIC%ROWTYPE;
IE_MOTIVO_SEM_CPF_W   PESSOA_FISICA_AUX.IE_MOTIVO_SEM_CPF%TYPE;
IE_CONTROL_EXT_W      PESSOA_FISICA_AUX.IE_CONTROL_EXT%TYPE;

cursor c01 is
  select  'S'
  from  regra_prontuario
  where  cd_estabelecimento            = :new.cd_estabelecimento
  and  nvl(ie_clinica, nvl(:new.ie_clinica,0))        = nvl(:new.ie_clinica,0)
  and  nvl(ie_tipo_atendimento, :new.ie_tipo_atendimento)    = :new.ie_tipo_atendimento
  and  ie_tipo_regra            = 1;

cursor c02 is
  select  qt_dias
  from   regra_dispensa_diaria
  where   nvl(ie_tipo_atendimento, :new.ie_tipo_atendimento) = :new.ie_tipo_atendimento
  and   nvl(ie_carater_inter_sus, nvl(:new.ie_carater_inter_sus,'0')) = nvl(:new.ie_carater_inter_sus,'0')
  and   :new.dt_entrada between ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(:new.dt_entrada, nvl(hr_inicio, '00:00')) and
            ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(:new.dt_entrada, nvl(hr_final, '00:00'))
  and   ie_situacao = 'A'
  order by nvl(ie_tipo_atendimento,0),
    nvl(ie_carater_inter_sus, '0');
cursor c03 is
  select  'S'
  from  regra_gerar_pac_vacina
  where  nvl(ie_situacao,'A')           = 'A'
  and  nvl(cd_estabelecimento, :new.cd_estabelecimento)  = :new.cd_estabelecimento
  and  nvl(ie_tipo_atendimento, :new.ie_tipo_atendimento)  = :new.ie_tipo_atendimento;

cursor c04 is
  select  nvl(b.qt_tempo_regra, 0),
      nvl(b.qt_tempo_normal, 0),
      nvl(b.nr_sequencia, 0),
      nvl(b.ie_escala, ''),
      b.ie_opcao_wl,
      (select  max(nm_tabela)
      from  vice_escala
      where  ie_escala = b.ie_escala)
  from   wl_regra_worklist a,
      wl_regra_item b,
      wl_regra_geracao c
  where  a.nr_sequencia = b.nr_seq_regra
  and    b.nr_sequencia = c.nr_seq_regra_item (+)
  and    b.ie_situacao = 'A'
  and    c.cd_setor_atendimento is null
  and    a.nr_seq_item = (  select  max(x.nr_sequencia)
                from  wl_item x
                where  x.nr_sequencia = a.nr_seq_item
                and    x.cd_categoria = 'S'
                and    x.ie_situacao = 'A');

cursor c06 is
  select  nvl(a.nr_sequencia, 0),
      nvl(b.qt_tempo_regra, 0),
      nvl(b.qt_tempo_normal, 0),
      nvl(b.qt_tempo_atraso, 0),
      nvl(b.nr_sequencia, 0),
      b.ie_opcao_wl
  from   wl_regra_worklist a,
      wl_regra_item b
  where  a.nr_sequencia = b.nr_seq_regra
  and    b.ie_situacao = 'A'
  and    a.nr_seq_item = (  select  max(x.nr_sequencia)
                from  wl_item x
                where  x.nr_sequencia = a.nr_seq_item
                and    x.cd_categoria = 'ED'
                and    x.ie_situacao = 'A');

cursor c07 is
  select  nvl(b.qt_tempo_normal, 0),
      nvl(b.nr_sequencia, 0),
      b.ie_tipo_diagnostico,
      b.ie_classif_diag
  from   wl_regra_worklist a,
      wl_regra_item b
  where  a.nr_sequencia = b.nr_seq_regra
  and    b.ie_situacao = 'A'
  and    a.nr_seq_item = (  select  max(x.nr_sequencia)
                from  wl_item x
                where  x.nr_sequencia = a.nr_seq_item
                and    x.cd_categoria = 'DG'
                and    x.ie_situacao = 'A');

cursor c08 is
  select  b.ie_evolucao_clinica,
      b.ie_opcao_wl,
      nvl(b.qt_tempo_regra, 0),
      nvl(b.qt_tempo_normal, 0),
      nvl(b.qt_tempo_atraso, 0),
      nvl(b.nr_sequencia, 0)
  from   wl_regra_worklist a,
      wl_regra_item b
  where  a.nr_sequencia = b.nr_seq_regra
  and    b.ie_situacao = 'A'
  and    a.nr_seq_item = (  select  max(x.nr_sequencia)
                from  wl_item x
                where  x.nr_sequencia = a.nr_seq_item
                and    x.cd_categoria = 'CN'
                and    x.ie_situacao = 'A');

cursor c09 is
  select  b.ie_opcao_wl,
      nvl(b.qt_tempo_regra, 0),
      nvl(b.qt_tempo_normal, 0),
      nvl(b.qt_tempo_atraso, 0),
      nvl(b.nr_sequencia, 0)
  from   wl_regra_worklist a,
      wl_regra_item b
  where  a.nr_sequencia = b.nr_seq_regra
  and    b.ie_situacao = 'A'
  and    a.nr_seq_item = (  select  max(x.nr_sequencia)
                from  wl_item x
                where  x.nr_sequencia = a.nr_seq_item
                and    x.cd_categoria = 'WO'
                and    x.ie_situacao = 'A');

cursor c10 is
  select  nvl(b.qt_tempo_normal, 0) ,
      nvl(b.nr_sequencia, 0)
  from   wl_regra_worklist a,
      wl_regra_item b
  where  a.nr_sequencia = b.nr_seq_regra
  and    b.ie_situacao = 'A'
  and    a.nr_seq_item = (  select  max(x.nr_sequencia)
                from  wl_item x
                where  x.nr_sequencia = a.nr_seq_item
                and    x.cd_categoria = 'ML'
                and    x.ie_situacao = 'A')
  and    nvl(b.ie_tipo_pend_carta,'N') = 'N';



begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
obter_param_usuario(916, 909, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_escolhe_especialidade_w);
obter_param_usuario(916, 990, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_perm_data_menor_nasc_w);
obter_param_usuario(916, 1097, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, VarRestringInformacoes_w);

obter_param_usuario(10021, 68, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_inicia_atend_vinc_senha_w);

dt_nasc_paciente_w := to_date(obter_dados_pf(:new.cd_pessoa_fisica,'DN'),'dd/mm/yyyy');

if  (:new.dt_entrada > sysdate) then
  begin

  obter_param_usuario(916, 52, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_data_futura_w);

  if  (ie_data_futura_w = 'N') then
    wheb_mensagem_pck.Exibir_Mensagem_Abort(198278);

  end if;
  end;
elsif   (:new.dt_entrada < sysdate - 999999) then
  wheb_mensagem_pck.Exibir_Mensagem_Abort(198279);

elsif   (:new.dt_entrada < dt_nasc_paciente_w and ie_perm_data_menor_nasc_w = 'N') then
  wheb_mensagem_pck.Exibir_Mensagem_Abort(215575);
end if;

if  (:new.cd_medico_resp is not null) then

  select  atendimento_troca_medico_seq.nextval
  into  nr_seq_troca_w
  from  dual;

  select  count(*) qt_especialidade
  into  qt_especialidade_w
  from  especialidade_medica b,
    medico_especialidade a
  where  a.cd_especialidade  = b.cd_especialidade
  and  a.cd_pessoa_fisica  = :new.cd_medico_resp;

  If  (qt_especialidade_w > 1) and
    (ie_escolhe_especialidade_w = 'S') then
    cd_especialidade_w := null;
  end if;

  if   (qt_especialidade_w = 1) then
    cd_especialidade_w := obter_especialidade_medico(:new.cd_medico_resp,'C');
  end if;

  if  ((qt_especialidade_w = 0) and
    (ie_escolhe_especialidade_w = 'O')) then
    wheb_mensagem_pck.Exibir_Mensagem_Abort(210182);
  end if;

  insert into atendimento_troca_medico (
    nr_sequencia,
    nr_atendimento,
    dt_atualizacao,
    nm_usuario,
    dt_troca,
    cd_medico_anterior,
    cd_medico_atual,
    ie_forma_aviso,
    cd_setor_atendimento,
    ie_tipo_atendimento,
    nr_seq_classif_medico,
    cd_especialidade,
    nm_usuario_nrec,
    dt_atualizacao_nrec)
  values(
    nr_seq_troca_w,
    :new.nr_atendimento,
    sysdate,
    :new.nm_usuario,
    sysdate,
    :new.cd_medico_resp,
    :new.cd_medico_resp,
    'T',
    null,
    :new.ie_tipo_atendimento,
    :new.nr_seq_classif_medico,
    cd_especialidade_w,
    :new.nm_usuario,
    sysdate);
end if;

IF  (:NEW.cd_medico_resp is not null ) THEN

  select  nvl(max(nr_seq_grupo),0)
  into  nr_grupo_medico_novo_w
  from  med_pac_grupo
  where  cd_pessoa_fisica = :NEW.cd_medico_resp;

  if  (nr_grupo_medico_novo_w <> 0) then

    update  pac_grupo_atend
    set  dt_saida = sysdate,
      nm_usuario = :NEW.nm_usuario
    where  cd_pessoa_fisica = :NEW.cd_pessoa_fisica
    and  dt_saida is null;

      if  (nr_grupo_medico_novo_w <> 0) then
        begin
        insert into pac_grupo_atend(
          nr_sequencia,
          dt_atualizacao,
          nm_usuario,
          nr_seq_grupo,
          nr_atendimento,
          ie_situacao,
          dt_entrada,
          cd_pessoa_fisica)
        select  pac_grupo_atend_seq.nextval,
          sysdate,
          :NEW.nm_usuario,
          nr_grupo_medico_novo_w,
          :NEW.nr_atendimento,
          'A',
          sysdate,
          :NEW.cd_pessoa_fisica
        from  dual
        where  not exists (  select  1
              from  pac_grupo_atend
              where  nr_atendimento   = :NEW.nr_atendimento
              and  nr_seq_grupo  = nr_grupo_medico_novo_w
              and  dt_saida is null
              union
              select  1
              from  pac_grupo_atend
              where  cd_pessoa_fisica   = :NEW.cd_pessoa_fisica
              and  nr_seq_grupo    = nr_grupo_medico_novo_w
              and  dt_saida is null);
        end;
      end if;
  end if;
END IF;

select  obter_valor_param_usuario(0,32,0,:new.nm_usuario,:new.cd_estabelecimento)
into  ie_prontuario_w
from  dual;

select  obter_valor_param_usuario(0,56,0,:new.nm_usuario,:new.cd_estabelecimento)
into  ie_gerar_ccih_w
from  dual;


obter_param_usuario(916, 132, 0, :new.nm_usuario, :new.cd_estabelecimento, ds_perfil_w);
begin
obter_param_usuario(916, 133, 0, :new.nm_usuario, :new.cd_estabelecimento, qt_dias_w);
exception
when others then
  qt_dias_w := null;
end;

if  (ds_perfil_w is not null) and
  (qt_dias_w is not null) and
  ((VarRestringInformacoes_w = '' or VarRestringInformacoes_w is null) or
  (obter_se_contido(:new.ie_tipo_atendimento, VarRestringInformacoes_w) = 'N')) then
  enviar_comunic_resp_cih(:new.nr_atendimento, :new.cd_pessoa_fisica, ds_perfil_w, qt_dias_w, :new.nm_usuario, :new.cd_estabelecimento);
end  if;



nr_prontuario_w  := nvl(obter_prontuario_pf(:new.cd_estabelecimento, :new.cd_pessoa_fisica),0);

select  max(dt_alta_institucional)
into  dt_alta_institucional_w
from  pessoa_fisica
where  cd_pessoa_fisica  = :new.cd_pessoa_fisica;

if  (dt_alta_institucional_w is not null) then
  wheb_mensagem_pck.Exibir_Mensagem_Abort(198280);

end if;


ds_erro_w  := '';

if  (nvl(:new.ie_clinica, 0) > 0) then

  consiste_clinica_tipo_atend(:new.cd_estabelecimento,:new.ie_clinica,
    :new.ie_tipo_atendimento, :new.nr_seq_regra_funcao, nvl(wheb_usuario_pck.get_cd_setor_atendimento,obter_setor_usuario(:new.nm_usuario)), ds_erro_w);
  if  (ds_erro_w is not null) or
    (ds_erro_w <> '') then
    Wheb_mensagem_pck.exibir_mensagem_abort(198281, 'DS_ERRO_W='||ds_erro_w);
  end if;
end if;


select  count(*)
into  qt_medico_w
from  medico
where  cd_pessoa_fisica = :new.cd_medico_resp;

select  count(*)
into  qt_medico_ativo_w
from  medico
where  cd_pessoa_fisica = :new.cd_medico_resp
and  nvl(ie_situacao,'A') = 'A';


if  (qt_medico_w > 0) and
  (qt_medico_ativo_w = 0) and
  (:new.cd_medico_resp is not null) then
  obter_param_usuario(916, 789, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, Ie_permite_pessoa_resp_w);
  if  (Ie_permite_pessoa_resp_w <> 'S') then
    Wheb_mensagem_pck.exibir_mensagem_abort(198282, 'NR_ATENDIMENTO='||:new.nr_atendimento);

  end if;
end if;

obter_param_usuario(916, 819, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_gerar_acesso_atend_w);

if  (ie_gerar_acesso_atend_w = 'S') then
  consistir_pep_acesso_atend(:new.nr_atendimento,:new.cd_pessoa_fisica);
end if;



if   (nr_prontuario_w = 0) then
  begin
  insert into log_atendimento(
    dt_atualizacao,
    nm_usuario,
    cd_log,
    nr_atendimento,
    ds_log)
  values  (sysdate,
    :new.nm_usuario,
    1,
    :new.nr_atendimento,
    wheb_mensagem_pck.get_texto(803179) || ': ' || chr(13) ||
    wheb_mensagem_pck.get_texto(803183) || ' ' || chr(13) ||
    wheb_mensagem_pck.get_texto(301081) || ' ' || :new.cd_pessoa_fisica || chr(13) ||
    wheb_mensagem_pck.get_texto(800315) || ': ' || :new.nr_atendimento || chr(13) ||
    'Obj: atendimento_paciente_insert' || chr(13) ||
    wheb_mensagem_pck.get_texto(802054) || ' ' || :new.ie_tipo_atendimento || chr(13) ||
    wheb_mensagem_pck.get_texto(803181) || ' ' || :new.ie_clinica);
  exception
  when others then
    null;
  end;
  obter_param_usuario(916, 523, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_prontuario_anual_w );
  obter_param_usuario(916, 533, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_gera_prontuario_w);
  obter_param_usuario(916, 672, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_gera_pront_rn_w);
  ie_gerar_w    := 'N';
  ie_gerar_anual_w  := 'N';
  if  (:new.ie_tipo_atendimento = 1) and
    ( ie_prontuario_anual_w  = 'N') and
    (ie_gera_prontuario_w = 'S') then
    ie_gerar_w  := 'S';
  elsif  ( ie_prontuario_w = 'N') and
    ( ie_prontuario_anual_w  = 'N') then
    ie_gerar_w  := 'S';
  elsif  (ie_prontuario_w = 'R') then
    open c01;
    loop
    fetch c01 into ie_gerar_w;
    exit when c01%notfound;
    end loop;
    close c01;
  elsif   ( ie_prontuario_anual_w  = 'S') then
    ie_gerar_anual_w := 'S';
    ie_gerar_w   := 'S';
  end if;

  if (:new.nr_atendimento_mae is not null) and (ie_gera_pront_rn_w = 'N') then
    ie_gerar_w   := 'N';
  end if;

  if  (ie_gerar_w = 'S') then

    if (ie_gerar_anual_w = 'S') then
      obter_param_usuario(5,24,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_forma_gerar_w);

      if (ie_forma_gerar_w <> 'N') then
        gerar_prontuario_anual(ie_forma_gerar_w,:new.nm_usuario,nr_prontuario_w);

        update pessoa_fisica
        set    nr_prontuario = nr_prontuario_w
        where  cd_pessoa_fisica = :new.cd_pessoa_fisica;
      end if;

    else
      gerar_prontuario_pac(:new.cd_estabelecimento, :new.cd_pessoa_fisica, 'N', :new.nm_usuario, nr_prontuario_w);
    end if;
    begin
    insert into log_atendimento(
      dt_atualizacao,
      nm_usuario,
      cd_log,
      nr_atendimento,
      ds_log)
    values  (sysdate,
      :new.nm_usuario,
      1,
      :new.nr_atendimento,
      wheb_mensagem_pck.get_texto(803179) || ': ' || chr(13) ||
      wheb_mensagem_pck.get_texto(803180) || ' ' || nr_prontuario_w || chr(13) ||
      wheb_mensagem_pck.get_texto(301081) || ' ' || :new.cd_pessoa_fisica || chr(13) ||
      wheb_mensagem_pck.get_texto(800315) || ': ' || :new.nr_atendimento || chr(13) ||
      'Obj: atendimento_paciente_insert' || chr(13) ||
      wheb_mensagem_pck.get_texto(802054) || ' ' || :new.ie_tipo_atendimento || chr(13) ||
      wheb_mensagem_pck.get_texto(803181) || ' ' || :new.ie_clinica);
    exception
    when others then
      null;
    end;
  end if;
end if;


if  (((ie_gerar_ccih_w = 'S') and
   (:new.ie_tipo_atendimento = 1)) or
  ((ie_gerar_ccih_w = 'E') and
   (:new.ie_tipo_atendimento in (1,7))) or
   ((ie_gerar_ccih_w = 'A') and
   (:new.ie_tipo_atendimento in (1,8))) or
   (ie_gerar_ccih_w = 'T')) then
  ccih_gerar_ficha_ocorrencia(:new.nr_atendimento,:new.dt_entrada, :new.cd_medico_resp, :new.nm_usuario,'N',:new.ie_clinica);
end if;


select  obter_se_assumir_paciente(:new.cd_medico_resp, :new.cd_estabelecimento)
into  ie_permissao_aten_w
from  dual;

if  (ie_permissao_aten_w = 'N') then
  wheb_mensagem_pck.Exibir_Mensagem_Abort(198283);

end if;

if  (:new.nr_seq_pac_senha_fila is not null) then
  update  paciente_senha_fila
  set  dt_vinculacao_senha   = sysdate,
    nm_usuario    = nvl(:new.nm_usuario, wheb_usuario_pck.get_nm_usuario),
    nm_paciente    = decode(:new.cd_pessoa_fisica,null,substr(obter_nome_pf(:new.cd_pessoa_fisica),1,255),null),
    cd_pessoa_fisica   = :new.cd_pessoa_fisica
  where  nr_sequencia     = :new.nr_seq_pac_senha_fila;

  if  (ie_inicia_atend_vinc_senha_w = 'S') then
    iniciar_finalizar_atend_senha(:new.nr_seq_pac_senha_fila, 'I', :new.nm_usuario, :new.cd_estabelecimento, 'N');
  end if;

  obter_param_usuario(916, 1089, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_vincular_senha_w);
  if  (ie_vincular_senha_w = 'C')  then

    insert into senhas_atendimento (
      nr_sequencia,
      nr_seq_pac_senha_fila,
      nr_atendimento,
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec
    ) values(  senhas_atendimento_seq.NextVal,
      :new.nr_seq_pac_senha_fila,
      :new.nr_atendimento,
      sysdate,
      :new.nm_usuario,
      sysdate,
      :new.nm_usuario);
  end if;

end if;

select  count(*)
into  qt_disp_diaria_w
from  dispensa_cob_diaria
where  cd_pessoa_fisica  = :new.cd_pessoa_fisica
and  cd_estabelecimento  = :new.cd_estabelecimento
and  nr_atendimento    is null;

if  (qt_disp_diaria_w > 0) then
  update  dispensa_cob_diaria
  set  nr_atendimento    = :new.nr_atendimento
  where  cd_pessoa_fisica  = :new.cd_pessoa_fisica
  and  cd_estabelecimento  = :new.cd_estabelecimento
  and  nr_atendimento    is null;
end if;


qt_dias_dispensa_w:= 0;
open C02;
loop
fetch C02 into
  qt_dias_dispensa_w;
exit when C02%notfound;
  begin
  qt_dias_dispensa_w:= qt_dias_dispensa_w;
  end;
end loop;
close C02;

if  (qt_dias_dispensa_w > 0) then

  select   nvl(max(cd_pessoa_fisica),:new.cd_pessoa_fisica)
  into  cd_pessoa_usuario_w
  from   usuario
  where   nm_usuario = :new.nm_usuario;

  insert into dispensa_cob_diaria
    (nr_sequencia,
    cd_estabelecimento,
    cd_pessoa_fisica,
    dt_atualizacao,
    nm_usuario,
    dt_atualizacao_nrec,
    nm_usuario_nrec,
    dt_inicio,
    dt_fim,
    cd_pessoa_resp,
    nr_atendimento,
    dt_liberacao,
    ds_justificativa)
  values   (dispensa_cob_diaria_seq.nextval,
    :new.cd_estabelecimento,
    :new.cd_pessoa_fisica,
    sysdate,
    :new.nm_usuario,
    sysdate,
    :new.nm_usuario,
    :new.dt_entrada,
    :new.dt_entrada + qt_dias_dispensa_w,
    cd_pessoa_usuario_w,
    :new.nr_atendimento,
    sysdate,
    wheb_mensagem_pck.get_texto(795787));
end if;


gerar_regra_prontuario_gestao(:new.ie_tipo_atendimento, :new.cd_estabelecimento, :new.nr_atendimento, :new.cd_pessoa_fisica, :new.nm_usuario, null, null, null, :new.ie_clinica, null, null, null);
obter_param_usuario(941, 162, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_tipo_atend_pront_w);

if (ie_tipo_atend_pront_w = 'S') then
  update  same_prontuario
  set  ie_tipo_atend_prontuario = :new.ie_tipo_atendimento
  where  nr_atendimento = :new.nr_atendimento
  and  ie_tipo_atend_prontuario <> 1;
end if;

ie_vincular_orcamento_w  := nvl(obter_valor_param_usuario(916,271,obter_perfil_ativo,:new.nm_usuario,0),'N');

if  (ie_vincular_orcamento_w = 'S') then
  select  nvl(max(nr_sequencia_orcamento),0)
  into  nr_seq_orcamento_w
  from  orcamento_paciente
  where  cd_pessoa_fisica  = :new.cd_pessoa_fisica
  and  nr_atendimento  is null;

  if  (nr_seq_orcamento_w  > 0) then
    update  orcamento_paciente
    set  nr_atendimento    = :new.nr_atendimento
    where  nr_sequencia_orcamento  = nr_seq_orcamento_w;
  end if;

elsif  (ie_vincular_orcamento_w = 'C') then
  select  nvl(max(nr_sequencia_orcamento),0)
  into  nr_seq_orcamento_w
  from  orcamento_paciente
  where  cd_pessoa_fisica  = :new.cd_pessoa_fisica
  and  ie_clinica    = :new.ie_clinica
  and  ie_tipo_atendimento  = :new.ie_tipo_atendimento
  and  nr_atendimento  is null;

  if  (nr_seq_orcamento_w  > 0) then
    update  orcamento_paciente
    set  nr_atendimento    = :new.nr_atendimento
    where  nr_sequencia_orcamento  = nr_seq_orcamento_w;
  end if;
end if;

if  (:new.ie_carater_inter_sus is null) then

  begin
  insert into log_atendimento(
    dt_atualizacao,
    nm_usuario,
    cd_log,
    nr_atendimento,
    ds_log)
  values  (sysdate,
    :new.nm_usuario,
    2,
    :new.nr_atendimento,
    wheb_mensagem_pck.get_texto(803155) || ': ' || chr(13) ||
    upper(wheb_mensagem_pck.get_texto(301081)) || ' ' || :new.cd_pessoa_fisica || chr(13) ||
    upper(wheb_mensagem_pck.get_texto(800315)) || ': ' || :new.nr_atendimento || chr(13) ||
    'OBJ: ATENDIMENTO_PACIENTE_INSERT ' || chr(13) ||
    wheb_mensagem_pck.get_texto(803152) || ': ' || obter_perfil_ativo || chr(13) ||
    wheb_mensagem_pck.get_texto(796343) || ': ' || :new.cd_estabelecimento || chr(13) ||
    'IE_CARATER_INTER_SUS ' || wheb_mensagem_pck.get_texto(803159) || ' ' || chr(13));
  exception
  when others then
    null;
  end;

end if;

if  (:new.ie_paciente_isolado = 'S') then
  insert into atend_paciente_hist
    (nr_sequencia,
    nr_atendimento,
    cd_pessoa_fisica,
    dt_inicial,
    dt_final,
    ie_evento,
    nm_usuario,
    dt_atualizacao,
    nm_usuario_nrec,
    dt_atualizacao_nrec)
  values
    (atend_paciente_hist_seq.nextval,
    :new.nr_atendimento,
    :new.cd_pessoa_fisica,
    sysdate,
    null,
    'I',
    :new.nm_usuario,
    sysdate,
    :new.nm_usuario,
    sysdate);
end  if;

select  max(nr_sequencia)
into  nr_seq_pac_visita_w
from  atendimento_visita
where  cd_pessoa_fisica  = :new.cd_pessoa_fisica
and  ie_paciente    = 'S'
and  nr_atendimento is null
and  ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_entrada) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_entrada);


if  (nvl(nr_seq_pac_visita_w,0) > 0) then
  update  atendimento_visita
  set  nr_atendimento  = :new.nr_atendimento
  where  nr_sequencia  = nr_seq_pac_visita_w;
end if;

open C03;
loop
fetch C03 into
  ie_regra_gerar_pac_vacina_w;
exit when C03%notfound;
  begin
  Inserir_pac_lista_espera_atend(:NEW.nr_atendimento, :new.cd_estabelecimento, :new.cd_pessoa_fisica, :NEW.nm_usuario);
  end;
end loop;
close C03;


ie_integracao_aghos_w := obter_dados_param_atend(:new.cd_estabelecimento, 'AG');


select  nvl(max(ie_integracao_ativa),'N')
into  ie_integra_regul_w
from  regulacao_parametro;

if  (:new.ie_tipo_atendimento = 1) and
  (:new.ie_tipo_convenio = 3) then

  obter_param_usuario(916, 1008, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_atend_sem_autorizacao_w);

  if  (ie_integra_regul_w = 'S') then

    select  max(nr_sequencia)
    into  nr_seq_regulacao_w
    from  regulacao_atendimento
    where  cd_pessoa_fisica  = :new.cd_pessoa_fisica
    and  ((ie_status_regulacao = '13') or
       ((ie_status_regulacao = '5') and
        (cd_cnes_executante is null)))
    and  nr_atendimento is null;

    if  (nvl(nr_seq_regulacao_w,0) > 0) then
      reg_internar_paciente(nr_seq_regulacao_w, :new.nr_atendimento, :new.cd_medico_resp, :NEW.nm_usuario);
    end if;
  end if;


  if  (obter_dados_param_atend(:new.cd_estabelecimento,'GI') = 'S') and
    (:new.ie_tipo_atendimento = 1) and
    (:new.ie_tipo_convenio = 3) then

    select  decode(count(*),0,'N','S')
    into  ie_possui_autor_gerint_w
    from  gerint_solic_internacao
    where  cd_pessoa_fisica = :new.cd_pessoa_fisica
    and    ie_situacao = 'A'
    and    nr_protocolo_solicitacao is not null
    and    nr_atendimento is null;

    if   (ie_possui_autor_gerint_w = 'N') and
      (ie_atend_sem_autorizacao_w = 'N') then
      Wheb_mensagem_pck.exibir_mensagem_abort(988519);
    end if;

  end if;


  if  (ie_integracao_aghos_w = 'S') then

    obter_param_usuario(916, 1068, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_mostra_erro_aghos_w);

    nm_pessoa_fisica_w  := substr(obter_nome_pf(:new.cd_pessoa_fisica),1,60);

    select  max(nr_internacao)
    into  nr_internacao_w
    from  solicitacao_tasy_aghos
    where  ((cd_pessoa_fisica = :new.cd_pessoa_fisica)
    or   (nm_paciente = nm_pessoa_fisica_w))
    and  ie_situacao = 'AU'
    and  nr_atendimento is null;

    if  (nr_internacao_w is null) then

      select   decode(count(*),0,'N','S')
      into  ie_solic_aghos_w
      from    solicitacao_tasy_aghos
      where  nr_atendimento = :new.nr_atendimento
      and    ie_situacao = 'I';

      If  (ie_atend_sem_autorizacao_w = 'N') and
        (ie_solic_aghos_w = 'N') then
        Wheb_mensagem_pck.exibir_mensagem_abort(222091);
      end if;
    else
      begin
        Aghos_internacao_solicitacao(nr_internacao_w, :new.nr_atendimento, :new.nm_usuario, :new.cd_estabelecimento);
      exception
        when  others then
          begin
          if  (ie_mostra_erro_aghos_w = 'S') then

            ds_sqlerror_w := sqlerrm;
            ds_erro_aghos_w := substr(Ajusta_mensagem_erro_aghos(ds_sqlerror_w), 1, 2000);

            If  (ds_erro_aghos_w is not null) then
              wheb_mensagem_pck.exibir_mensagem_abort(228493, 'DS_MENSAGEM=' || ds_erro_aghos_w);
            else
              wheb_mensagem_pck.exibir_mensagem_abort(228493, 'DS_MENSAGEM=' || ds_sqlerror_w);
            end if;

          end if;
          end;
      end;
    end if;
  end if;
end if;


if  (:new.CD_MEDICO_REFERIDO is not null) then

  INSERT INTO PF_MEDICO_EXTERNO (
    NR_SEQUENCIA,
    DT_ATUALIZACAO,
    NM_USUARIO,
    DT_ATUALIZACAO_NREC,
    NM_USUARIO_NREC,
    CD_MEDICO,
    CD_PESSOA_FISICA)
  SELECT  PF_MEDICO_EXTERNO_SEQ.nextval,
    sysdate,
    :new.nm_usuario,
    sysdate,
    :new.nm_usuario,
    :new.CD_MEDICO_REFERIDO,
    :new.cd_pessoa_fisica
  FROM  MEDICO
  where  CD_PESSOA_FISICA = :new.CD_MEDICO_REFERIDO
  and     IE_VINCULO_MEDICO = 14
  and     not exists (  select  1
        from   PF_MEDICO_EXTERNO x
        where   x.CD_MEDICO = :new.CD_MEDICO_REFERIDO
        and   x.CD_PESSOA_FISICA = :new.cd_pessoa_fisica);

  if   (:new.nr_seq_episodio is not null) then

    update	episodio_paciente
    set	cd_medico_referido = nvl(cd_medico_referido,:new.cd_medico_referido),
	cd_estabelecimento = nvl(cd_estabelecimento,:new.cd_estabelecimento)
    where   nr_sequencia = :new.nr_seq_episodio;

  end if;

end if;

gerar_reconciliacao_paciente(:new.nm_usuario, :new.nr_atendimento, nvl(obter_perfil_ativo,:new.cd_perfil_ativo), 'E', 'N', null,'N');

select  nvl2(:new.nr_atendimento_mae,'N','S')
into  ie_cons_episodio_w
from   dual;

open c06;
loop
fetch c06 into
  qt_registro_w,
  qt_tempo_regra_w,
  qt_tempo_normal_w,
  qt_tempo_atraso_w,
  nr_seq_regra_w,
  ie_opcao_wl_w;
exit when c06%notfound;
  begin
    if  (:new.dt_alta is null and :new.dt_previsto_alta is null and obter_se_regra_geracao(nr_seq_regra_w,:new.nr_seq_episodio,:new.nr_seq_tipo_admissao_fat) = 'S') then

      if  (qt_tempo_regra_w > 0 and ie_opcao_wl_w = 'D') then
        wl_gerar_finalizar_tarefa('ED','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_regra_w/24),'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
      elsif(qt_tempo_normal_w > 0 and (ie_opcao_wl_w = 'A' or ie_opcao_wl_w = 'E')) then
        wl_gerar_finalizar_tarefa('ED','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_normal_w/24),'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
      end if;
    end if;
  end;
end loop;
close c06;

qt_tempo_regra_w :=0;

open c04;
loop
fetch c04 into
  qt_tempo_regra_w,
  qt_tempo_normal_w,
  nr_seq_regra_w,
  ie_escala_w,
  ie_opcao_wl_w,
  nm_tabela_w;
exit when c04%notfound;
  begin
  if(obter_se_regra_geracao(nr_seq_regra_w,:new.nr_seq_episodio,:new.nr_seq_tipo_admissao_fat) = 'S') then

    if (qt_tempo_normal_w > 0 and (ie_opcao_wl_w = 'A' or ie_opcao_wl_w = 'E')) then
      wl_gerar_finalizar_tarefa('S','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_normal_w/24),'N',null,null,null,null,null,null,ie_escala_w,null,null,nr_seq_regra_w,null,null,null,null,null,nm_tabela_w,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
    elsif (qt_tempo_regra_w > 0 and ie_opcao_wl_w = 'D') then
      wl_gerar_finalizar_tarefa('S','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_regra_w/24),'N',null,null,null,null,null,null,ie_escala_w,null,null,nr_seq_regra_w,null,null,null,null,null,nm_tabela_w,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
    end if;
  end if;
  end;
end loop;
close c04;

open c07;
loop
fetch c07 into
  qt_tempo_normal_w,
  nr_seq_regra_w,
  ie_tipo_diagnostico_w,
  ie_classif_diag_w;
exit when c07%notfound;
  begin
    if  (qt_tempo_normal_w > 0 and (ie_tipo_diagnostico_w > 0 or ie_classif_diag_w is not null) and obter_se_regra_geracao(nr_seq_regra_w,:new.nr_seq_episodio,:new.nr_seq_tipo_admissao_fat) = 'S') then

      wl_gerar_finalizar_tarefa('DG','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_normal_w/24),'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,ie_tipo_diagnostico_w,null,null,null,null,null,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w, ie_classif_diag_w);
    end if;
  end;
end loop;
close c07;

open c08;
loop
fetch c08 into
  cd_tipo_evolucao_w,
  ie_opcao_wl_w,
  qt_tempo_regra_w,
  qt_tempo_normal_w,
  qt_tempo_atraso_w,
  nr_seq_regra_w;
exit when c08%notfound;
  begin
    if(obter_se_regra_geracao(nr_seq_regra_w,:new.nr_seq_episodio,:new.nr_seq_tipo_admissao_fat) = 'S') then

      if  (qt_tempo_regra_w > 0 and ie_opcao_wl_w = 'D') then
        wl_gerar_finalizar_tarefa('CN','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_regra_w/24),'N', cd_tipo_evolucao_w, null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
      elsif(qt_tempo_normal_w > 0 and (ie_opcao_wl_w = 'A' or ie_opcao_wl_w = 'E')) then
        wl_gerar_finalizar_tarefa('CN','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_normal_w/24),'N', cd_tipo_evolucao_w, null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
      end if;
    end if;
  end;
end loop;
close c08;

open c09;
loop
fetch c09 into
  ie_opcao_wl_w,
  qt_tempo_regra_w,
  qt_tempo_normal_w,
  qt_tempo_atraso_w,
  nr_seq_regra_w;
exit when c09%notfound;
  begin
    if(obter_se_regra_geracao(nr_seq_regra_w,:new.nr_seq_episodio,:new.nr_seq_tipo_admissao_fat) = 'S') then

      if  (qt_tempo_regra_w > 0 and ie_opcao_wl_w = 'D') then
        wl_gerar_finalizar_tarefa('WO','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_regra_w/24),'N', null,null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
      elsif(qt_tempo_normal_w > 0 and (ie_opcao_wl_w = 'A' or ie_opcao_wl_w = 'E')) then
        wl_gerar_finalizar_tarefa('WO','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_normal_w/24),'N', null,null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
      end if;
    end if;
  end;
end loop;
close c09;

open c10;
loop
fetch c10 into
  qt_tempo_normal_w,
  nr_seq_regra_w;
exit when c10%notfound;
  begin
    if(obter_se_regra_geracao(nr_seq_regra_w,:new.nr_seq_episodio,:new.nr_seq_tipo_admissao_fat) = 'S') then

      wl_gerar_finalizar_tarefa('ML','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,:new.dt_entrada+(qt_tempo_normal_w/24),'N', null,null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_entrada,:new.nr_seq_episodio,null,null,null,null,ie_cons_episodio_w);
    end if;
  end;
end loop;
close c10;

BEGIN
  SELECT PFA.IE_MOTIVO_SEM_CPF,
         PFA.IE_CONTROL_EXT
    INTO IE_MOTIVO_SEM_CPF_W,
         IE_CONTROL_EXT_W
    FROM PESSOA_FISICA_AUX PFA
   WHERE PFA.CD_PESSOA_FISICA = :NEW.CD_PESSOA_FISICA
     AND ROWNUM <= 1;

  BEGIN
    SELECT APA.*
      INTO ATEND_PACIENTE_ADIC_W
      FROM ATEND_PACIENTE_ADIC APA
     WHERE APA.NR_ATENDIMENTO = :NEW.NR_ATENDIMENTO;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
         ATEND_PACIENTE_ADIC_W := NULL;
  END;

  ATEND_PACIENTE_ADIC_W.NR_ATENDIMENTO    := :NEW.NR_ATENDIMENTO;
  ATEND_PACIENTE_ADIC_W.IE_MOTIVO_SEM_CPF := IE_MOTIVO_SEM_CPF_W;
  ATEND_PACIENTE_ADIC_W.IE_CONTROLE_EXT   := IE_CONTROL_EXT_W;
  ATEND_PACIENTE_ADIC_W.CD_PESSOA_FISICA  := :NEW.CD_PESSOA_FISICA;
  ATEND_PACIENTE_ADIC_W.DT_ATUALIZACAO    := SYSDATE;
  ATEND_PACIENTE_ADIC_W.NM_USUARIO        := :NEW.NM_USUARIO;

  IF (ATEND_PACIENTE_ADIC_W.NM_USUARIO_NREC IS NULL) THEN
     ATEND_PACIENTE_ADIC_W.NM_USUARIO_NREC := ATEND_PACIENTE_ADIC_W.NM_USUARIO;
  END IF;

  IF (ATEND_PACIENTE_ADIC_W.DT_ATUALIZACAO_NREC IS NULL) THEN
     ATEND_PACIENTE_ADIC_W.DT_ATUALIZACAO_NREC := ATEND_PACIENTE_ADIC_W.DT_ATUALIZACAO;
  END IF;

  IF (ATEND_PACIENTE_ADIC_W.NR_SEQUENCIA IS NULL) THEN
     SELECT ATEND_PACIENTE_ADIC_SEQ.NEXTVAL
       INTO ATEND_PACIENTE_ADIC_W.NR_SEQUENCIA
       FROM DUAL;

     INSERT INTO ATEND_PACIENTE_ADIC
     VALUES ATEND_PACIENTE_ADIC_W;
  ELSE
     UPDATE ATEND_PACIENTE_ADIC APA
        SET ROW = ATEND_PACIENTE_ADIC_W
      WHERE APA.NR_SEQUENCIA = ATEND_PACIENTE_ADIC_W.NR_SEQUENCIA;
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
       NULL;
END;

UPDATE pessoa_fisica_aux
 SET nr_primeiro_atend = :new.nr_atendimento
WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica
 AND nr_primeiro_atend IS NULL;

if ((:new.dt_previsto_alta is not null) or (:new.ie_probabilidade_alta is not null)) then
	insert into encounter_discharge_prob (
			NR_SEQUENCIA,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			DT_PROBABILIDADE_ALTA,
			IE_PROBABILIDADE_ALTA,
			NR_ATENDIMENTO)
		values(	encounter_discharge_prob_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.dt_previsto_alta,
			:new.ie_probabilidade_alta,
			:new.nr_atendimento
			);
end if;

if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') then
  GENERATE_CPOE_SLIP_NUMBER(:new.nr_atendimento,:new.ie_tipo_atendimento,:new.cd_pessoa_fisica,null,null,:new.nm_usuario);
end if;

end if;

end;
/
