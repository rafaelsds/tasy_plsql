create or replace 
package pls_pp_nota_fiscal_pck as

	-- Vincular nota fiscal
	procedure vincular_nota_lote_prod(nr_seq_lote_p			pls_pp_lote.nr_sequencia%type,
					nr_seq_prestador_p		pls_prestador.nr_sequencia%type,
					cd_serie_nf_p			nota_fiscal.cd_serie_nf%type,
					cd_operacao_nf_p		nota_fiscal.cd_operacao_nf%type,
					dt_emissao_p			date,
					cd_natureza_operacao_p		number,
					ds_observacao_p			varchar2,
					ds_complemento_p		varchar2,
					dt_base_venc_p			date,
					nr_nota_fiscal_p		nota_fiscal.nr_nota_fiscal%type,
					nm_usuario_p			usuario.nm_usuario%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					ie_base_venc_p		 	varchar2,
					nr_seq_nota_fiscal_p	out	nota_fiscal.nr_sequencia%type);
	
	-- Revincular nota fiscal
	procedure revincular_nota_prestador(	nr_seq_lote_p		in	number,
						nr_seq_prestador_p	in	number,
						cd_serie_nf_p		in	Varchar2,
						cd_operacao_nf_p	in	Number,
						dt_emissao_p		in	Date,
						cd_natureza_operacao_p	in	Number,
						ds_observacao_p		in	Varchar2,
						ds_complemento_p	in	Varchar2,
						dt_base_venc_p		in	Date,
						nr_nota_fiscal_p	in	Varchar2,
						nm_usuario_p		in	Varchar2,
						cd_estabelecimento_p	in	Number,
						ie_base_venc_p		in 	Varchar2,
						nr_seq_nota_fiscal_p	out	Number);
						
	-- Consistir o valor e alguns dados da nota vinculada manualmente
	procedure pls_consistir_prest_nota_pgto(nr_seq_prestador_p	in	pls_prestador.nr_sequencia%type,
						nr_seq_nota_fiscal_p	in	nota_fiscal.nr_sequencia%type,
						nr_seq_pp_prest_nota_p	in	pls_pp_prest_nota.nr_sequencia%type,
						vl_vinculado_p		in	pls_pp_prest_nota.vl_vinculado%type);

end pls_pp_nota_fiscal_pck;
/

create or replace 
package body pls_pp_nota_fiscal_pck as

	-- Gerar nota fiscal
	procedure gerar_notas_lote_prod(	nr_seq_lote_p			number,
						cd_serie_nf_p			varchar2,
						cd_operacao_nf_p		number,
						dt_emissao_p			date,
						cd_natureza_operacao_p		number,
						ds_observacao_p			varchar2,
						ds_complemento_p		varchar2,
						dt_base_venc_p			date,
						nr_nota_fiscal_p		varchar2,
						nm_usuario_p			usuario.nm_usuario%type,
						ie_somente_nota_p		varchar2,
						nr_seq_prestador_p		number,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
						ie_base_venc_p			varchar2,
						nr_seq_nota_fiscal_p	out	number);

procedure vincular_nota_lote_prod(nr_seq_lote_p			pls_pp_lote.nr_sequencia%type,
				nr_seq_prestador_p		pls_prestador.nr_sequencia%type,
				cd_serie_nf_p			nota_fiscal.cd_serie_nf%type,
				cd_operacao_nf_p		nota_fiscal.cd_operacao_nf%type,
				dt_emissao_p			date,
				cd_natureza_operacao_p		number,
				ds_observacao_p			varchar2,
				ds_complemento_p		varchar2,
				dt_base_venc_p			date,
				nr_nota_fiscal_p		nota_fiscal.nr_nota_fiscal%type,
				nm_usuario_p			usuario.nm_usuario%type,
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
				ie_base_venc_p		 	varchar2,
				nr_seq_nota_fiscal_p	out	nota_fiscal.nr_sequencia%type) is 

nr_seq_nota_fiscal_w		nota_fiscal.nr_sequencia%type;
ds_consistencia_w		varchar2(255);
ie_perm_vin_nf_fechada_w	varchar2(1);

Cursor C01 is
	select	b.nr_titulo
	from	titulo_pagar b,
		pls_pp_prestador a
	where	b.nr_titulo	= a.nr_titulo_pagar
	and	b.ie_tipo_titulo	<> '4'
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	a.nr_seq_prestador	= nr_seq_prestador_p
	and	ie_perm_vin_nf_fechada_w = 'N' 
	and 	b.ie_situacao = 'A'
	union all
	select	b.nr_titulo
	from	titulo_pagar b,
		pls_pp_prestador a
	where	b.nr_titulo	= a.nr_titulo_pagar
	and	b.ie_tipo_titulo	<> '4'
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	a.nr_seq_prestador	= nr_seq_prestador_p
	and	ie_perm_vin_nf_fechada_w = 'S';
	
Cursor C02 is
	select	nvl(sum(decode(b.ie_situacao,'A',1,0)),0) qt_aberto,
		nvl(sum(decode(b.ie_situacao,'A',0,1)),0) qt_nao_aberto
	from	titulo_pagar 		b,
		pls_pp_prestador a
	where	b.nr_titulo	= a.nr_titulo_pagar
	and	b.ie_tipo_titulo 	<> '4'
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	a.nr_seq_prestador	= nr_seq_prestador_p;
	
begin
ie_perm_vin_nf_fechada_w := obter_valor_param_usuario(1282, 30, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);

if	(ie_perm_vin_nf_fechada_w = 'N') then
	for r_C02_w in C02 loop
		if	(r_C02_w.qt_aberto = 0) and 
			(r_C02_w.qt_nao_aberto > 0) then
			--N�o � poss�vel vincular notas fiscais em t�tulos a pagar que n�o estejam com a situa��o em "Aberto".
			wheb_mensagem_pck.exibir_mensagem_abort(355986);
		end if;
	end loop;
end if;

-- Verificado se j� existe uma nota fiscal gerada para este vencimento
/*select	max(nr_sequencia)
into	nr_seq_nota_fiscal_w
from	nota_fiscal
where	nr_seq_pgto_prest = nr_seq_prestador_pgto_p;

if	(nr_seq_nota_fiscal_w is not null) then
	--Mensagem: J� existe nota fiscal gerada para este vencimento!
	wheb_mensagem_pck.exibir_mensagem_abort(265325,'');
end if;*/

update 	pls_pp_prestador
set	nr_nota_fiscal		= nr_nota_fiscal_p
where	nr_seq_lote		= nr_seq_lote_p
and	nr_seq_prestador	= nr_seq_prestador_p;

gerar_notas_lote_prod(	nr_seq_lote_p, cd_serie_nf_p, cd_operacao_nf_p,
			dt_emissao_p, cd_natureza_operacao_p, ds_observacao_p,
			ds_complemento_p, dt_base_venc_p, nr_nota_fiscal_p,
			nm_usuario_p, 'S', nr_seq_prestador_p,
			cd_estabelecimento_p, ie_base_venc_p, nr_seq_nota_fiscal_w);

if	(nr_seq_nota_fiscal_w is null) then
	--Mensagem: N�o foi gerada nota fiscal para o lote!
	wheb_mensagem_pck.exibir_mensagem_abort(265323,'');
end if;

nr_seq_nota_fiscal_p := nr_seq_nota_fiscal_w;

for r_c01_w in c01 loop
	vincular_titulo_pagar_nf( nr_seq_nota_fiscal_w, r_c01_w.nr_titulo, nm_usuario_p, 'A','S', ds_consistencia_w);
end loop;

commit;

end vincular_nota_lote_prod;

procedure gerar_notas_lote_prod(	nr_seq_lote_p			number,
					cd_serie_nf_p			varchar2,
					cd_operacao_nf_p		number,
					dt_emissao_p			date,
					cd_natureza_operacao_p		number,
					ds_observacao_p			varchar2,
					ds_complemento_p		varchar2,
					dt_base_venc_p			date,
					nr_nota_fiscal_p		varchar2,
					nm_usuario_p			usuario.nm_usuario%type,
					ie_somente_nota_p		varchar2,
					nr_seq_prestador_p		number,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					ie_base_venc_p			varchar2,
					nr_seq_nota_fiscal_p	out	number) is 

ds_tipo_despesa_w		varchar2(255);
cd_conta_cred_w			varchar2(20);
cd_cgc_prestador_w		varchar2(14);
cd_pf_prestador_w		varchar2(10);
ie_tipo_nota_w			varchar2(3);
ie_geracao_nota_titulo_w	varchar2(2);
ie_tipo_item_w			varchar2(1);
ie_tipo_despesa_w		varchar2(1);
ie_gerou_tributo_w		varchar2(1)	:= 'N';
nr_nota_fiscal_w		number(20) := nr_nota_fiscal_p;
vl_resumo_w			number(15,2);
vl_mercadoria_w			number(15,2);
vl_total_nota_w			number(15,2);
vl_liberado_w			number(15,2);
vl_liquido_nota_w		number(15,2);
vl_imposto_w			number(15,2);
vl_reducao_base_w		number(15,2);
vl_trib_nao_retido_w		number(15,2);
vl_base_nao_retido_w		number(15,2);
vl_trib_adic_w			number(15,2);
vl_base_adic_w			number(15,2);
cd_procedimento_w		number(15);
nr_seq_trans_fin_baixa_conta_w	number(10);
cd_condicao_pagamento_w		number(10);
nr_seq_nota_fiscal_w		number(10);
nr_sequencia_nf_w		number(10)	:= 9;
ie_origem_proced_w		number(10);
cd_conta_financ_w		number(10);
nr_titulo_w			number(10);
qt_dia_vencimento_w		number(10);
qt_existe_w			number(10);
nr_seq_evento_w			number(10);
pr_tributo_w			number(7,4);
cd_material_w			number(6);
nr_item_nf_w			number(5);
qt_notas_w			number(5)	:= 0;
ie_forma_pagamento_w		number(2);
dt_base_venc_w			date;
dt_entrada_saida_w		date;
vl_base_calculo_w		number(15,2);
nr_seq_conta_w			number(10);
ie_natureza_w			pls_evento.ie_natureza%type;
ie_parametro_34_w		varchar2(1);
cd_local_estoque_w		nota_fiscal_item.cd_local_estoque%type;
cd_unidade_medida_estoque_w	nota_fiscal_item.cd_unidade_medida_estoque%type;
cd_unidade_medida_compra_w	nota_fiscal_item.cd_unidade_medida_compra%type;

Cursor C01 is
	select	'P' ie_tipo_item,
		a.ie_tipo_despesa,
		sum(a.vl_liberado) vl_liberado,
		decode(a.ie_tipo_despesa,1,'Procedimentos',2,'Taxas',3,'Di�rias',4,'Pacotes','') ds_tipo_despesa,
		b.cd_conta_cred,
		null nr_seq_evento
	from	pls_conta_medica_resumo a,
		pls_conta_proc b
	where	a.nr_seq_item = b.nr_sequencia
	and 	a.vl_liberado <> 0
	and	a.cd_procedimento is not null
	and	a.nr_seq_pp_lote = nr_seq_lote_p
	and	a.nr_seq_prestador_pgto = nr_seq_prestador_p
	and	((a.ie_situacao is null) or (a.ie_situacao != 'I'))
	group by a.ie_tipo_despesa,
		 b.cd_conta_cred
	union all
	-- procedimento recurso de glosa
	select	'P' ie_tipo_item,
		b.ie_tipo_despesa,
		sum(a.vl_liberado) vl_liberado,
		decode(b.ie_tipo_despesa,1,'Procedimentos',2,'Taxas',3,'Di�rias',4,'Pacotes','') ds_tipo_despesa,
		a.cd_conta_cred,
		null nr_seq_evento
	from	pls_conta_rec_resumo_item a,
		pls_conta_proc b,
		pls_rec_glosa_proc c
	where	a.nr_seq_pp_lote = nr_seq_lote_p
	and	a.nr_seq_prestador_pgto = nr_seq_prestador_p
	and	c.nr_sequencia = a.nr_seq_proc_rec
	and	b.nr_sequencia = c.nr_seq_conta_proc
	and	((a.ie_situacao is null) or (a.ie_situacao != 'I'))
	and 	a.vl_liberado <> 0
	group by b.ie_tipo_despesa,
		 a.cd_conta_cred
	union all
	select	'M' ie_tipo_item,
		a.ie_tipo_despesa,
		sum(a.vl_liberado) vl_liberado,
		decode(a.ie_tipo_despesa,'1','Gases medicinais','2','Medicamentos','3','Materiais','7','OPM','') ds_tipo_despesa,
		b.cd_conta_cred,
		null nr_seq_evento
	from	pls_conta_medica_resumo a,
		pls_conta_mat b
	where	a.nr_seq_item = b.nr_sequencia
	and	a.cd_procedimento is null
	and	a.nr_seq_pp_lote = nr_seq_lote_p
	and	a.nr_seq_prestador_pgto = nr_seq_prestador_p
	and	((a.ie_situacao is null) or (a.ie_situacao != 'I'))
	and 	a.vl_liberado <> 0
	group by a.ie_tipo_despesa,
		 b.cd_conta_cred
	union all
	-- material recurso de glosa
	select	'M' ie_tipo_item,
		b.ie_tipo_despesa,
		sum(a.vl_liberado) vl_liberado,
		decode(b.ie_tipo_despesa,1,'Procedimentos',2,'Taxas',3,'Di�rias',4,'Pacotes','') ds_tipo_despesa,
		a.cd_conta_cred,
		null nr_seq_evento
	from	pls_conta_rec_resumo_item a,
		pls_conta_mat b,
		pls_rec_glosa_mat c
	where	a.nr_seq_pp_lote = nr_seq_lote_p
	and	a.nr_seq_prestador_pgto = nr_seq_prestador_p
	and	c.nr_sequencia = a.nr_seq_mat_rec
	and	b.nr_sequencia = c.nr_seq_conta_mat
	and	((a.ie_situacao is null) or (a.ie_situacao != 'I'))
	and 	a.vl_liberado <> 0
	group by b.ie_tipo_despesa,
		 a.cd_conta_cred
	union all
	select	'E' ie_tipo_item,
		'E' ie_tipo_despesa,
		sum(a.vl_item) vl_liberado,
		'Eventos' ds_tipo_despesa,
		a.cd_conta_credito,
		a.nr_seq_evento nr_seq_evento
	from	pls_pp_item_lote a
	where	a.nr_seq_prestador	= nr_seq_prestador_p
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	a.ie_tipo_item		not in ('1','8')
	and 	a.vl_item		<> 0
	group by a.nr_seq_evento, a.cd_conta_credito;
	
Cursor C02 is
	select	a.cd_tributo
	from	pls_pp_valor_trib_pessoa a
	where	a.nr_seq_prestador	= nr_seq_prestador_p
	and	a.nr_seq_lote		= nr_seq_lote_p
	group by a.cd_tributo;

begin
Obter_Param_Usuario(1282, 34, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_parametro_34_w);

cd_local_estoque_w 		:= obter_regra_atributo2('NOTA_FISCAL_ITEM', 'CD_LOCAL_ESTOQUE', 0, 'VLD', wheb_usuario_pck.get_cd_estabelecimento, obter_perfil_ativo, obter_funcao_ativa, nm_usuario_p);
cd_unidade_medida_estoque_w 	:= obter_regra_atributo2('NOTA_FISCAL_ITEM', 'CD_UNIDADE_MEDIDA_ESTOQUE', 0, 'VLD', wheb_usuario_pck.get_cd_estabelecimento, obter_perfil_ativo, obter_funcao_ativa, nm_usuario_p);
cd_unidade_medida_compra_w	:= obter_regra_atributo2('NOTA_FISCAL_ITEM', 'CD_UNIDADE_MEDIDA_COMPRA', 0, 'VLD', wheb_usuario_pck.get_cd_estabelecimento, obter_perfil_ativo, obter_funcao_ativa, nm_usuario_p);
cd_condicao_pagamento_w		:= obter_regra_atributo2('NOTA_FISCAL', 'CD_CONDICAO_PAGAMENTO', 0, 'VLD', wheb_usuario_pck.get_cd_estabelecimento, obter_perfil_ativo, obter_funcao_ativa, nm_usuario_p);

select	max(nr_seq_conta)
into	nr_seq_conta_w
from	(select	max(a.nr_seq_conta) nr_seq_conta
	from	pls_conta_medica_resumo a,
		pls_conta_proc b
	where	a.nr_seq_item = b.nr_sequencia
	and	a.cd_procedimento is not null
	and	a.nr_seq_pp_lote = nr_seq_lote_p
	and	a.nr_seq_prestador_pgto = nr_seq_prestador_p
	and	a.ie_tipo_despesa is null
	and	((a.ie_situacao is null) or (a.ie_situacao != 'I'))
	union		 
	select	max(a.nr_seq_conta) nr_seq_conta
	from	pls_conta_medica_resumo a,
		pls_conta_mat b
	where	a.nr_seq_item = b.nr_sequencia
	and	a.cd_procedimento is null
	and	a.nr_seq_pp_lote = nr_seq_lote_p
	and	a.nr_seq_prestador_pgto = nr_seq_prestador_p
	and	a.ie_tipo_despesa is null
	and	((a.ie_situacao is null) or (a.ie_situacao != 'I')));
		 
if	(nr_seq_conta_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(192541,'NR_SEQ_CONTA=' || nr_seq_conta_w);
end if;

if	(dt_emissao_p is not null) then
	dt_entrada_saida_w	:= to_date(to_char(dt_emissao_p, 'dd/mm/yyyy') || ' ' ||  to_char(sysdate,'hh24:mi:ss') ,'dd/mm/yyyy hh24:mi:ss');
end if;

select	sum(vl_liquido)
into	vl_resumo_w
from	pls_pp_prestador
where	nr_seq_prestador	= nr_seq_prestador_p
and	nr_seq_lote		= nr_seq_lote_p;

select	max(nr_seq_trans_fin_baixa_conta)
into	nr_seq_trans_fin_baixa_conta_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

/*select	count(*)
into	qt_notas_w
from	nota_fiscal
where	nr_seq_pgto_prest	= nr_seq_prestador_pgto_p
and	ie_situacao	= '1';

if 	(qt_notas_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(192542);
end if;*/

select	cd_pessoa_fisica,
	cd_cgc
into	cd_pf_prestador_w,
	cd_cgc_prestador_w
from	pls_prestador
where	nr_sequencia	= nr_seq_prestador_p;

select	nvl(cd_condicao_pagamento_w,max(cd_condicao_pagamento)),
	max(ie_geracao_nota_titulo),
	max(qt_dia_vencimento)
into	cd_condicao_pagamento_w,
	ie_geracao_nota_titulo_w,
	qt_dia_vencimento_w
from	pls_prestador_pagto
where	nr_seq_prestador	= nr_seq_prestador_p;

if 	(nvl(cd_condicao_pagamento_w,0) = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(192543,'NR_SEQ_PREST=' || nr_seq_prestador_p || ';NM_PREST=' || substr(pls_obter_dados_prestador(nr_seq_prestador_p, 'N'),1,255));
end if;

if	(nvl(ie_base_venc_p, 'N') = 'N') then
	-- Fazer tratamento para adicionar 1 m�s, j� faz isso para os vencimentos do pagamento
	dt_base_venc_w	:= add_months(dt_base_venc_p,1);

	-- Obter dia ap�s adicionar o m�s, para que n�o ocorra de obter dia 31 em m�s que n�o h� dia 31.
	if	(nvl(qt_dia_vencimento_w,0) = 0) then
		qt_dia_vencimento_w	:= to_char(dt_base_venc_w,'dd');
	end if;

	-- Tratar fevereiro
	if	(to_char(trunc(dt_base_venc_w,'dd'),'mm') = '02') then
		if	(qt_dia_vencimento_w >= 29) then
			select	to_number(to_char(last_day(to_date(('01/' || to_char(trunc(dt_base_venc_w,'dd'),'mm/yyyy')),'dd/mm/yyyy')),'dd'))
			into	qt_dia_vencimento_w
			from	dual;
		end if;
	end if;

	begin
	dt_base_venc_w	:= to_date(lpad(to_char(qt_dia_vencimento_w),2,0) || to_char(trunc(dt_base_venc_w,'dd'),'mm/yyyy'),'dd/mm/yyyy');
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(192540,'NR_SEQ_PREST=' || nr_seq_prestador_p || ';NM_PREST=' || substr(pls_obter_dados_prestador(nr_seq_prestador_p, 'N'),1,255));
	end;
else
	dt_base_venc_w	:= dt_base_venc_p;
end if;

if 	(cd_pf_prestador_w is null) then
	ie_tipo_nota_w := 'EN';
else
	ie_tipo_nota_w := 'EF';
end if;

select	nvl(max(nr_sequencia_nf),0) + 1
into	nr_sequencia_nf_w
from	nota_fiscal
where	nr_nota_fiscal	= nr_nota_fiscal_p;

vl_mercadoria_w		:= vl_resumo_w;
vl_total_nota_w		:= vl_resumo_w;

/*Criar a nota fiscal*/
insert into nota_fiscal
		(nr_sequencia,				cd_estabelecimento,		cd_cgc_emitente,
		cd_serie_nf,				nr_nota_fiscal,			nr_sequencia_nf,
		cd_operacao_nf,				dt_emissao,			dt_entrada_saida,
		ie_acao_nf,				ie_emissao_nf,			ie_tipo_frete,
		vl_mercadoria,				vl_total_nota,			qt_peso_bruto,
		qt_peso_liquido,			dt_atualizacao,			nm_usuario,
		ie_tipo_nota,				cd_condicao_pagamento,		cd_natureza_operacao,
		nr_seq_classif_fiscal,			ds_observacao,			vl_ipi,
		vl_descontos,				vl_frete,			vl_seguro,
		vl_despesa_acessoria,			cd_pessoa_fisica,		cd_cgc,
		nr_seq_pgto_prest,			ie_situacao,			nr_lote_contabil,
		ie_entregue_bloqueto)
	values	(nota_fiscal_seq.nextval,		cd_estabelecimento_p,		cd_cgc_prestador_w,
		cd_serie_nf_p,				nr_nota_fiscal_p,		nr_sequencia_nf_w,
		cd_operacao_nf_p,			trunc(dt_emissao_p,'dd'),	dt_entrada_saida_w,
		'1',					'0',				'0',
		vl_mercadoria_w,			vl_total_nota_w,		0,
		0,					sysdate,			nm_usuario_p,
		ie_tipo_nota_w,				cd_condicao_pagamento_w,	cd_natureza_operacao_p,
		null,					ds_observacao_p,		0,
		0,					0,				0,
		0,					cd_pf_prestador_w,		cd_cgc_prestador_w,
		null,					'1',				0,
		'N') returning nr_sequencia into nr_seq_nota_fiscal_w;

open C01; /* Obter itens da conta */
loop
fetch C01 into	
	ie_tipo_item_w,
	ie_tipo_despesa_w,
	vl_liberado_w,
	ds_tipo_despesa_w,
	cd_conta_cred_w,
	nr_seq_evento_w;
exit when C01%notfound;
	begin
	if	(ie_tipo_despesa_w is not null) then
		if 	(ie_tipo_item_w	= 'P') then
			select	nvl(max(cd_procedimento),0),
				nvl(max(ie_origem_proced),0)
			into	cd_procedimento_w,
				ie_origem_proced_w
			from	pls_tipo_desp_proc
			where	ie_tipo_despesa	= ie_tipo_despesa_w
			and	cd_estabelecimento	= cd_estabelecimento_p;
			
			if	(cd_procedimento_w = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(192544,'DS_DESP=' || ds_tipo_despesa_w);
			end if;
			
			pls_obter_conta_financ_regra(	'PP', 		null,			cd_estabelecimento_p,
							null,		null,			null,
							null,		nr_seq_prestador_p,	null,
							null,		null,			null,
							null,		null,			null,
							null,		null,			cd_conta_financ_w); /* Obter primeiro a conta financeira do plano */
							
			if	(cd_conta_financ_w is null) then
				obter_conta_financeira('S', cd_estabelecimento_p, null,
							cd_procedimento_w, ie_origem_proced_w, null,
							null, null, null,
							cd_conta_financ_w, null, cd_operacao_nf_p, null, 
							null, null, null, null, null, null, null,
							null, null, null, null, null, null, null, null, null);
			end if;
		elsif	(ie_tipo_item_w	= 'M') then
			begin
			select	max(b.cd_material)
			into	cd_material_w
			from	pls_tipo_desp_mat	a,
				pls_material 		b
			where	a.nr_seq_material	= b.nr_sequencia
			and	a.ie_tipo_despesa 	= ie_tipo_despesa_w
			and	a.cd_estabelecimento	= cd_estabelecimento_p;
			exception
			when others then
				wheb_mensagem_pck.exibir_mensagem_abort(192545,'DS_DESP=' || ds_tipo_despesa_w);
			end;
			
			pls_obter_conta_financ_regra(	'PP',		null,			cd_estabelecimento_p,
							null,		null,			null,
							null,		nr_seq_prestador_p,	null,
							null,		null,			null,
							null,		null,			null,
							null,		null,			cd_conta_financ_w); /* Obter primeiro a conta financeira do plano */
							
			if	(cd_conta_financ_w is null) then
				obter_conta_financeira('S', cd_estabelecimento_p, cd_material_w, null, null, null, null, null, null,
							cd_conta_financ_w, null, cd_operacao_nf_p, null, null, null,
							null, null, null, null, null, null, null, null, null, null, null, null, null, null);
			end if;

		elsif	(ie_tipo_item_w	= 'E') then
			select	max(ie_natureza)
			into	ie_natureza_w
			from	pls_evento
			where	nr_sequencia	= nr_seq_evento_w;

			if	(nvl(ie_natureza_w,'X') = 'D') and (nvl(vl_liberado_w,0) > 0) then
				vl_liberado_w	:= vl_liberado_w * (-1);
			end if;
			
			-- como n�o tem uma ordena��o e n�o � usado em mais de um lugar, n�o tem motivo de ser um cursor
			select	max(a.cd_procedimento),
				max(a.ie_origem_proced),
				max(a.cd_material)
			into	cd_procedimento_w,
				ie_origem_proced_w,
				cd_material_w
			from	pls_evento_regra_item_nf a
			where	a.nr_seq_evento	= nr_seq_evento_w
			and	dt_base_venc_w	between trunc(a.dt_inicio_vigencia,'dd') and fim_dia(nvl(a.dt_fim_vigencia,dt_base_venc_w));
			
			if	(cd_procedimento_w is null) and
				(cd_material_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(192546);
			end if;
			
			pls_obter_conta_financ_regra(	'PP',		null,			cd_estabelecimento_p,
							null,		null,			null,
							null,		nr_seq_prestador_p,	null,
							null,		null,			null,
							null,		null,			null,
							null,		null,			cd_conta_financ_w); /* Obter primeiro a conta financeira do plano */
							
			if	(cd_conta_financ_w is null) then
				obter_conta_financeira('S', cd_estabelecimento_p, cd_material_w, cd_procedimento_w, ie_origem_proced_w, null,
							null, null, null, cd_conta_financ_w, null, cd_operacao_nf_p, null, null, null,
							null, null, null, null, null, null, null, null, null, null, null, null, null, null);
			end if;
		end if;
			
		select	nvl(max(nr_item_nf),0) + 1
		into	nr_item_nf_w
		from	nota_fiscal_item
		where	nr_sequencia	= nr_seq_nota_fiscal_w;
		
		insert into nota_fiscal_item	
				(nr_sequencia,						cd_estabelecimento,					cd_cgc_emitente,
				cd_serie_nf,						nr_nota_fiscal,						nr_sequencia_nf,
				nr_item_nf,						cd_natureza_operacao,					qt_item_nf,
				vl_unitario_item_nf,					vl_total_item_nf,					vl_liquido,
				vl_frete,						vl_desconto,						vl_despesa_acessoria,
				vl_desconto_rateio,					vl_seguro,						nm_usuario,
				dt_atualizacao,						ds_complemento,						cd_procedimento,
				ie_origem_proced,					nr_seq_conta_financ,					cd_material,
				cd_conta_contabil,					cd_local_estoque,					cd_unidade_medida_estoque,
				cd_unidade_medida_compra)
			values	(nr_seq_nota_fiscal_w,					cd_estabelecimento_p,					cd_cgc_prestador_w,
				cd_serie_nf_p,						nr_nota_fiscal_w,					nr_sequencia_nf_w,
				nr_item_nf_w,						cd_natureza_operacao_p,					1,
				vl_liberado_w,						vl_liberado_w,						vl_liberado_w,
				0,							0,							0,
				0,							0,							nm_usuario_p,
				sysdate,						ds_complemento_p, 					decode(ie_tipo_item_w,'M',null,cd_procedimento_w),
				decode(ie_tipo_item_w,'M',null,ie_origem_proced_w),	decode(cd_conta_financ_w,0,null,cd_conta_financ_w),	decode(ie_tipo_item_w,'P',null,cd_material_w),
				cd_conta_cred_w,					cd_local_estoque_w,					cd_unidade_medida_estoque_w,
				cd_unidade_medida_compra_w);
		end if;
	end;
end loop;
close C01;

for r_c02_w in C02 loop
	ie_gerou_tributo_w	:= 'S';
	
	select	sum(a.pr_aliquota),
		sum(a.vl_tributo),
		sum(a.vl_reducao),
		sum(a.vl_nao_retido),
		sum(a.vl_base_nao_retido),
		sum(a.vl_trib_adic),
		sum(a.vl_base_adic),
		sum(a.vl_base_calculo)
	into	pr_tributo_w,
		vl_imposto_w,
		vl_reducao_base_w,
		vl_trib_nao_retido_w,
		vl_base_nao_retido_w,
		vl_trib_adic_w,
		vl_base_adic_w,
		vl_base_calculo_w
	from	pls_pp_valor_trib_pessoa a
	where	a.nr_seq_prestador	= nr_seq_prestador_p
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	a.cd_tributo		= r_c02_w.cd_tributo;	

	select	count(*)
	into	qt_existe_w
	from	nota_fiscal_trib
	where	nr_sequencia	= nr_seq_nota_fiscal_w
	and	cd_tributo	= r_c02_w.cd_tributo;
	
	if	(qt_existe_w = 0) then
	
		insert into nota_fiscal_trib(
				nr_sequencia,				cd_tributo,				vl_tributo,
				dt_atualizacao,				nm_usuario,				vl_base_calculo,
				tx_tributo,				vl_reducao_base,			vl_trib_nao_retido,
				vl_base_nao_retido,			vl_trib_adic,				vl_base_adic,
				dt_atualizacao_nrec,			nm_usuario_nrec,			ie_origem_trib,
				nr_seq_interno
		) values(	nr_seq_nota_fiscal_w,			r_c02_w.cd_tributo,			vl_imposto_w,
				sysdate,				nm_usuario_p,				vl_base_calculo_w,
				pr_tributo_w,				vl_reducao_base_w,			vl_trib_nao_retido_w,
				vl_base_nao_retido_w,			vl_trib_adic_w,				vl_base_adic_w,
				sysdate,				nm_usuario_p,				'LP',
				nota_fiscal_trib_seq.nextval);
	end if;
end loop;

if	(ie_gerou_tributo_w = 'N') then
	gerar_tributos_fornecedor(nr_seq_nota_fiscal_w, null, nm_usuario_p, dt_emissao_p);
end if;

--gerar_imposto_nf(nr_seq_nota_fiscal_w, nm_usuario_p, null, null);
atualiza_total_nota_fiscal(nr_seq_nota_fiscal_w, nm_usuario_p);

select	nvl(max(ie_forma_pagamento),0)
into	ie_forma_pagamento_w
from	condicao_pagamento
where	cd_condicao_pagamento	= cd_condicao_pagamento_w;

-- busca o nr_titulo para vincular aos vencimentos da nota
select	max(nr_titulo_pagar)
into	nr_titulo_w
from	pls_pp_prestador
where	nr_seq_prestador= nr_seq_prestador_p
and	nr_seq_lote	= nr_seq_lote_p;

if	(ie_forma_pagamento_w = 10) then -- Conforme vencimentos 

	select	nvl(vl_total_nota,0)
	into	vl_liquido_nota_w
	from	nota_fiscal
	where	nr_sequencia	= nr_seq_nota_fiscal_w;
	
	insert into nota_fiscal_venc
			(nr_sequencia,			nm_usuario,			dt_atualizacao,
			cd_estabelecimento,		cd_cgc_emitente,		cd_serie_nf,
			nr_nota_fiscal,			nr_sequencia_nf,		dt_vencimento,
			vl_vencimento,			ie_origem,			nr_titulo_pagar)
		values	(nr_seq_nota_fiscal_w,		nm_usuario_p,			sysdate,
			cd_estabelecimento_p,		cd_cgc_prestador_w,		cd_serie_nf_p,
			nr_nota_fiscal_w,		1,				dt_base_venc_w, 
			vl_liquido_nota_w,		'N',				nr_titulo_w);
else
	gerar_nota_fiscal_venc(nr_seq_nota_fiscal_w, nvl(dt_base_venc_p,dt_base_venc_w));
	
	update 	nota_fiscal_venc
	set	nr_titulo_pagar = nr_titulo_w
	where	nr_sequencia	= nr_seq_nota_fiscal_w;
end if;

if	(coalesce(ie_parametro_34_w,'S') <> 'S') then
	update	nota_fiscal
	set	dt_atualizacao_estoque	= sysdate
	where	nr_sequencia		= nr_seq_nota_fiscal_w;
end if;

if	(ie_somente_nota_p = 'N') then
	if	(nvl(nr_titulo_w,0) = 0) then -- Se ainda n�o tem t�tulo gerado
		gerar_titulo_pagar_nf(nr_seq_nota_fiscal_w, nm_usuario_p);
		
		select	max(nr_titulo_pagar)
		into	nr_titulo_w
		from	pls_pp_prestador
		where	nr_seq_prestador= nr_seq_prestador_p
		and	nr_seq_lote	= nr_seq_lote_p;
		
		if	(nr_titulo_w is not null) then
			gerar_bloqueto_tit_rec(nr_titulo_w, 'OPSPPM');
		end if;
	else
		update	titulo_pagar
		set	nr_seq_nota_fiscal	= nr_seq_nota_fiscal_w
		where	nr_titulo		= nr_titulo_w;
	end if;
	
	update	titulo_pagar
	set	nr_seq_trans_fin_baixa	= nr_seq_trans_fin_baixa_conta_w
	where	nr_titulo in (	select	nr_titulo_pagar
				from	pls_pp_prestador
				where	nr_seq_prestador= nr_seq_prestador_p
				and	nr_seq_lote	= nr_seq_lote_p);
end if;

nr_seq_nota_fiscal_p := nr_seq_nota_fiscal_w;

end gerar_notas_lote_prod;

procedure revincular_nota_prestador(	nr_seq_lote_p		in	number,
					nr_seq_prestador_p	in	number,
					cd_serie_nf_p		in	Varchar2,
					cd_operacao_nf_p	in	Number,
					dt_emissao_p		in	Date,
					cd_natureza_operacao_p	in	Number,
					ds_observacao_p		in	Varchar2,
					ds_complemento_p	in	Varchar2,
					dt_base_venc_p		in	Date,
					nr_nota_fiscal_p	in	Varchar2,
					nm_usuario_p		in	Varchar2,
					cd_estabelecimento_p	in	Number,
					ie_base_venc_p		in 	Varchar2,
					nr_seq_nota_fiscal_p	out	Number) is 

nr_nota_fiscal_w		varchar2(255);
nr_lote_contabil_w		number(10);
nr_seq_nota_fiscal_w		number(10);

begin
select	max(nr_nota_fiscal)
into	nr_nota_fiscal_w
from	pls_pp_prestador
where	nr_seq_lote		= nr_seq_lote_p
and	nr_seq_prestador	= nr_seq_prestador_p;

select	max(nr_lote_contabil),
	max(nr_sequencia)
into	nr_lote_contabil_w,
	nr_seq_nota_fiscal_w
from	nota_fiscal
where	nr_nota_fiscal		= nr_nota_fiscal_w;
--and	nr_seq_pgto_prest	= nr_seq_prestador_pgto_p;

if	(nr_lote_contabil_w is not null) and
	(nr_lote_contabil_w <> 0) then
	--Mensagem: A nota fiscal vinculada j� foi contabilizada.
	wheb_mensagem_pck.exibir_mensagem_abort(265320,'');
else
	estornar_nota_fiscal(nr_seq_nota_fiscal_w, nm_usuario_p);
	
	/*update	nota_fiscal
	set	nr_seq_pgto_prest	= null
	where	nr_seq_pgto_prest	= nr_seq_prestador_pgto_p;*/
	
	update 	pls_pp_prestador
	set	nr_nota_fiscal		= null
	where	nr_seq_lote		= nr_seq_lote_p
	and	nr_seq_prestador	= nr_seq_prestador_p;
	
	vincular_nota_lote_prod(nr_seq_lote_p, nr_seq_prestador_p, cd_serie_nf_p,
				cd_operacao_nf_p, dt_emissao_p, cd_natureza_operacao_p,
				ds_observacao_p, ds_complemento_p, dt_base_venc_p,
				nr_nota_fiscal_p, nm_usuario_p,	cd_estabelecimento_p,
				ie_base_venc_p,	nr_seq_nota_fiscal_p);
end if;

commit;

end revincular_nota_prestador;

-- Consistir o valor e alguns dados da nota vinculada manualmente
procedure pls_consistir_prest_nota_pgto
			(	nr_seq_prestador_p		pls_prestador.nr_sequencia%type,
				nr_seq_nota_fiscal_p		nota_fiscal.nr_sequencia%type,
				nr_seq_pp_prest_nota_p		pls_pp_prest_nota.nr_sequencia%type,
				vl_vinculado_p			pls_pp_prest_nota.vl_vinculado%type) is 

cd_cgc_prest_w			pessoa_juridica.cd_cgc%type;
cd_cgc_nota_w			pessoa_juridica.cd_cgc%type;
cd_pessoa_fisica_prest_w	pessoa_fisica.cd_pessoa_fisica%type;
cd_pessoa_fisica_nota_w		pessoa_fisica.cd_pessoa_fisica%type;
vl_total_nota_w			nota_fiscal.vl_total_nota%type := 0;
vl_vinculado_w			pls_pp_prest_nota.vl_vinculado%type := 0;

begin

select	max(vl_total_nota)
into	vl_total_nota_w
from	nota_fiscal
where	nr_sequencia	= nr_seq_nota_fiscal_p;

select	nvl(sum(vl_vinculado),0)
into	vl_vinculado_w
from	pls_pp_prest_nota
where	nr_seq_nota_fiscal	= nr_seq_nota_fiscal_p
and	nr_sequencia		!= nr_seq_pp_prest_nota_p;

if	(vl_total_nota_w < (vl_vinculado_p + vl_vinculado_w)) then
	wheb_mensagem_pck.exibir_mensagem_abort(114035,'VL_VINC=' || (vl_vinculado_p + vl_vinculado_w) || ';' || 'VL_NOTA=' || vl_total_nota_w);
end if;

end pls_consistir_prest_nota_pgto;

end pls_pp_nota_fiscal_pck;
/