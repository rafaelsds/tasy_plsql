create or replace
package wheb_integr_sen_contab is

procedure gerar_ctb_movimento(	nr_lote_contabil_p in 	varchar2,
				dt_mes_referencia_p	date,
				cd_empresa_p		number,
				cd_estabelecimento_p	number,
				dt_movimento_p		date,
				cd_conta_debito_p		varchar2,
				cd_conta_credito_p		varchar2,
				vl_movimento_p		number,
				cd_centro_custo_p		varchar2,
				cd_historico_p		varchar2,
				ds_compl_historico_p	varchar2,
				nm_usuario_p		varchar2,
				nr_seq_agrupamento_p	number default null,
				ds_erro_p	out		varchar2);
		
procedure excluir_lote_contabil(nr_lote_contabil_p	varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p	out		varchar2,
				cd_estabelecimento_p	number default null);

procedure desfazer_lote_contabil(nr_lote_contabil_p		varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p	out		varchar2,
				cd_estabelecimento_p	number default null);

end wheb_integr_sen_contab;
/
create or replace
package body wheb_integr_sen_contab is

	procedure gerar_ctb_movimento(	nr_lote_contabil_p	in	varchar2,
					dt_mes_referencia_p	date,
					cd_empresa_p		number,
					cd_estabelecimento_p	number,
					dt_movimento_p		date,
					cd_conta_debito_p		varchar2,
					cd_conta_credito_p		varchar2,
					vl_movimento_p		number,
					cd_centro_custo_p		varchar2,
					cd_historico_p		varchar2,
					ds_compl_historico_p	varchar2,
					nm_usuario_p		varchar2,
					nr_seq_agrupamento_p  	number default null,
					ds_erro_p	out		varchar2) is
	
	nr_lote_contabil_w		number(10);
	nr_seq_mes_ref_w		number(10);
	nr_seq_movimento_w	number(10);
	dt_referencia_w		date;
	cd_tipo_lote_w		number(10);
	cd_historico_w		varchar2(10);
	nr_seq_mov_cc_w		ctb_movto_centro_custo.nr_sequencia%type;
	
	cd_conta_credito_w	varchar2(20);
	cd_conta_debito_w		varchar2(20);
	cd_conta_credito_ww	varchar2(20);
	cd_conta_debito_ww	varchar2(20);
	cd_centro_custo_w		varchar2(20);
	cd_centro_custo_ww	varchar2(20);
	ds_erro_w		varchar2(255);
	dt_fechamento_w		date;
	cd_classif_debito_w	varchar2(40);
	cd_classif_credito_w	varchar2(40);
	cd_empresa_conta_w	number(4);	
	
	nr_seq_agrupamento_w	ctb_movimento.nr_seq_agrupamento%type;
	
	begin
	
	nr_seq_agrupamento_w := nvl(nr_seq_agrupamento_p,somente_numero(to_char(dt_movimento_p, 'MMYYYY')));
	
	dt_referencia_w		:= trunc(dt_mes_referencia_p,'mm');
	cd_conta_debito_ww	:= replace(cd_conta_debito_p,' ','');
	cd_conta_credito_ww	:= replace(cd_conta_credito_p,' ','');
	cd_centro_custo_ww	:= replace(cd_centro_custo_p,' ','');
	
	select	nvl(max(cd_historico),0)
	into	cd_historico_w
	from	historico_padrao
	where	cd_empresa	= cd_empresa_p
	and	nvl(cd_sistema_contabil,cd_historico)	= cd_historico_p;
		
	if	(cd_historico_w = 0) then
		ds_erro_w	:= substr('Nao existe o historico: ' || cd_historico_p || ' cadastrado/vinculado no Tasy.',1,255);
	end if;	

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_mes_ref_w
	from	ctb_mes_ref
	where	cd_empresa		= cd_empresa_p
	and	dt_referencia		= dt_referencia_w;
	
	if	(nr_seq_mes_ref_w = 0) then
		ds_erro_w	:= substr('Nao existe o mes: ' || to_char(dt_referencia_w,'dd/mm/yyyy') || ' cadastrado' || chr(13) || chr(10) ||
					'na empresa: ' || cd_empresa_p || ' na base do Tasy.',1,255);
	else
        	select    max(dt_fechamento)
        	into    dt_fechamento_w
        	from    ctb_mes_ref
        	where    nr_sequencia    = nr_seq_mes_ref_w;
       	
       	 	if    (dt_fechamento_w is not null) then
       	 	    ds_erro_w    := 'O mes' || to_char(dt_referencia_w,'dd/mm/yyyy') || ' ja esta fechado na Contabilidade.';
        	end if;
   
    	end if;
	/* tipo de lote digitacao */
	select	max(campo_numerico(nvl(vl_parametro, vl_parametro_padrao)))
	into	cd_tipo_lote_w
	from	funcao_parametro
	where	cd_funcao = 923
	and	nr_sequencia = 3;
	
	select	nvl(max(nr_lote_contabil),0)
	into	nr_lote_contabil_w
	from	lote_contabil
	where	nr_lote_externo	= nr_lote_contabil_p 
	and	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(nvl(ds_erro_w,'X') = 'X') and
		(nr_lote_contabil_w = 0) then
		
		select	nvl(max(nr_lote_contabil),0) + 1
		into	nr_lote_contabil_w
		from	lote_contabil;
		
		begin
		insert into lote_contabil(
			nr_lote_contabil,
			dt_referencia,
			cd_tipo_lote_contabil,
			dt_atualizacao,
			nm_usuario,
			cd_estabelecimento,
			ie_situacao,
			vl_debito,
			vl_credito,
			dt_integracao,
			dt_atualizacao_saldo,
			dt_consistencia,
			nm_usuario_original,
			nr_seq_mes_ref,
			ie_encerramento,
			ds_observacao,
			nr_lote_externo)
		values(	nr_lote_contabil_w,
			dt_referencia_w,
			cd_tipo_lote_w,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			'A', 0, 0,
			null,
			null,
			null, 
			'IntegrSen',
			nr_seq_mes_ref_w,
			'N',
			'Lote gerado atraves de integracao',
			nr_lote_contabil_p);
		exception when others then
			ds_erro_w	:= sqlerrm(sqlcode);
		end;
	end if;

	
	if	(nvl(cd_conta_debito_ww,'X') <> 'X') then
		select	max(cd_conta_contabil)
		into	cd_conta_debito_w
		from	conta_contabil
		where	nvl(cd_sistema_contabil,cd_conta_contabil)	= cd_conta_debito_ww;
		
		if	(nvl(cd_conta_debito_w,'X') = 'X') then
			 ds_erro_w	:= substr('Conta contabil nao cadastrada/vinculada no Tasy: (' || cd_conta_debito_ww||')',1,255);
		else
			begin
			select	max(cd_empresa)
			into	cd_empresa_conta_w
			from	conta_contabil
			where	cd_conta_contabil = cd_conta_debito_w;
			
			if	(cd_empresa_conta_w <> cd_empresa_p) then
				ds_erro_w	:= substr('A empresa ('||cd_empresa_conta_w||') da conta contabil: (' || cd_conta_debito_ww||')'||
							' nao corresponde a empresa do movimento (' ||cd_empresa_p||')',1,255);
			end if;
			end;
		end if;
	end if;
	
	if	(nvl(cd_conta_credito_ww,'X') <> 'X') then
		select	max(cd_conta_contabil)
		into	cd_conta_credito_w
		from	conta_contabil
		where	nvl(cd_sistema_contabil,cd_conta_contabil)	= cd_conta_credito_ww;
		
		if	(nvl(cd_conta_credito_w,'X') = 'X') then
			ds_erro_w	:= substr('Conta contabil nao cadastrada/vinculada no Tasy: (' || cd_conta_credito_ww||')',1,255);
		else
			begin
			select	max(cd_empresa)
			into	cd_empresa_conta_w
			from	conta_contabil
			where	cd_conta_contabil = cd_conta_credito_w;
			
			if	(cd_empresa_conta_w <> cd_empresa_p) then
				ds_erro_w	:= substr('A empresa ('||cd_empresa_conta_w||') da conta contabil: (' || cd_conta_credito_ww||')'||
							' nao corresponde a empresa do movimento (' ||cd_empresa_p||')',1,255);
			end if;
			end;
		end if;
	end if;
	
	if	(nvl(ds_erro_w,'X') = 'X') then
		begin
		nr_seq_movimento_w	:= null;
		
		if	(nvl(cd_conta_debito_w,'0') <> '0') then
			cd_classif_debito_w	:= ctb_obter_classif_conta(cd_conta_debito_w, null, dt_movimento_p);
			select	max(nr_sequencia)
			into	nr_seq_movimento_w
			from	ctb_movimento
			where	nr_lote_contabil	= nr_lote_contabil_w
			and	dt_movimento		= dt_movimento_p
			and	cd_conta_debito		= cd_conta_debito_w
			and	cd_historico		= cd_historico_w;
		end if;
		
		if	(nvl(cd_conta_credito_w,'0') <> '0') then
			cd_classif_credito_w	:= ctb_obter_classif_conta(cd_conta_credito_w, null, dt_movimento_p);
			select	max(nr_sequencia)
			into	nr_seq_movimento_w
			from	ctb_movimento
			where	nr_lote_contabil	= nr_lote_contabil_w
			and	dt_movimento		= dt_movimento_p
			and	cd_conta_credito	= cd_conta_credito_w
			and	cd_historico		= cd_historico_w;
		end if;
		
		if	(nr_seq_movimento_w is null) then
			begin
		
			select	ctb_movimento_seq.nextval
			into 	nr_seq_movimento_w
			from 	dual;
			
			insert into ctb_movimento(
				nr_sequencia,
				nr_lote_contabil,
				nr_seq_mes_ref,
				dt_movimento, 
				vl_movimento, 
				dt_atualizacao,
				nm_usuario, 
				cd_historico, 
				cd_conta_debito,
				cd_conta_credito,
				ds_compl_historico, 
				nr_seq_agrupamento,
				ie_revisado, 
				cd_estabelecimento,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				cd_classif_debito,
				cd_classif_credito)
			values(	nr_seq_movimento_w,
				nr_lote_contabil_w,
				nr_seq_mes_ref_w,
				dt_movimento_p,
				vl_movimento_p,
				sysdate,
				nm_usuario_p,
				cd_historico_w,
				cd_conta_debito_w,
				cd_conta_credito_w,
				ds_compl_historico_p,
				nr_seq_agrupamento_w,
				'N',
				null,
				'IntegrSen',
				sysdate,
				cd_classif_debito_w,
				cd_classif_credito_w);
				
			if	(nvl(ds_erro_w,'X') = 'X') then
				
				select	max(cd_centro_custo)
				into	cd_centro_custo_w
				from	centro_custo
				where	cd_estabelecimento	= cd_estabelecimento_p
				and	nvl(cd_sistema_contabil,cd_centro_custo) = cd_centro_custo_ww;
				
				select 	max(nr_sequencia)
				into	nr_seq_mov_cc_w
				from	ctb_movto_centro_custo
				where	nr_seq_movimento = nr_seq_movimento_w
				and		cd_centro_custo	= cd_centro_custo_w;			
								
				if	(nvl(nr_seq_mov_cc_w,0) = 0) then			
				
					if	(cd_centro_custo_w is not null) then
						begin
						insert into ctb_movto_centro_custo(
							nr_sequencia,
							nr_seq_movimento,
							cd_centro_custo,
							dt_atualizacao,
							nm_usuario,
							vl_movimento,
							pr_rateio)
						values(	ctb_movto_centro_custo_seq.nextval,
							nr_seq_movimento_w,
							cd_centro_custo_w,
							sysdate,
							nm_usuario_p,
							vl_movimento_p,
							100);
						exception when others then
							ds_erro_w	:= sqlerrm(sqlcode);
							ds_erro_w	:= substr('Erro ao gerar o movto por centro de custo: ' || ds_erro_w,1,255);
						end;
					elsif	(cd_centro_custo_w is null) and
						(cd_centro_custo_ww is not null) then
						ds_erro_w	:= substr('Centro de custo nao cadastrado/vinculado no Tasy: ' || cd_centro_custo_p,1,255);
					end if;
				elsif	(nvl(nr_seq_mov_cc_w,0) <> 0) and
					(nvl(cd_centro_custo_w,0) <> 0) then
					update	ctb_movto_centro_custo
					set		vl_movimento	= vl_movimento + nvl(vl_movimento_p,0)
					where	nr_sequencia	= nr_seq_mov_cc_w;
				end if;			
				
			end if;
			exception when others then
				ds_erro_w	:= substr(sqlerrm(sqlcode),1,255);
				ds_erro_w	:= substr(ds_erro_w || ' Erro ao gerar movto contabil',1,255);
			end;
		elsif	(nvl(nr_seq_movimento_w,0) <> 0) then
			begin
			update	ctb_movimento
			set	vl_movimento	= vl_movimento + nvl(vl_movimento_p,0)
			where	nr_sequencia	= nr_seq_movimento_w;
			
		
			select	max(cd_centro_custo)
			into	cd_centro_custo_w
			from	centro_custo
			where	cd_estabelecimento	= cd_estabelecimento_p
			and	nvl(cd_sistema_contabil,cd_centro_custo) = cd_centro_custo_ww;
			
			select 	max(nr_sequencia)
			into	nr_seq_mov_cc_w
			from	ctb_movto_centro_custo
			where	nr_seq_movimento = nr_seq_movimento_w
			and		cd_centro_custo	= cd_centro_custo_w;
					
			if	(nvl(nr_seq_mov_cc_w,0) = 0) then
			
				if	(cd_centro_custo_w is not null) then
				begin
					insert into ctb_movto_centro_custo(
						nr_sequencia,
						nr_seq_movimento,
						cd_centro_custo,
						dt_atualizacao,
						nm_usuario,
						vl_movimento,
						pr_rateio)
					values(	ctb_movto_centro_custo_seq.nextval,
						nr_seq_movimento_w,
						cd_centro_custo_w,
						sysdate,
						nm_usuario_p,
						vl_movimento_p,
						100);
					exception when others then
						ds_erro_w	:= sqlerrm(sqlcode);
						ds_erro_w	:= substr('Erro ao gerar o movto por centro de custo: ' || ds_erro_w,1,255);
					end;
				elsif	(cd_centro_custo_w is null) and
					(cd_centro_custo_ww is not null) then
					ds_erro_w	:= substr('Centro de custo nao cadastrado/vinculado no Tasy: ' || cd_centro_custo_p,1,255);
				end if;
			elsif	(nvl(nr_seq_mov_cc_w,0) <> 0) and
				(nvl(cd_centro_custo_w,0) <> 0) then
				update	ctb_movto_centro_custo
				set		vl_movimento	= vl_movimento + nvl(vl_movimento_p,0)
				where	nr_sequencia	= nr_seq_mov_cc_w;			
			end if;	
			end;			
		end if;
		end;

	end if;
	
	
	ds_erro_p	:= ds_erro_w;
/*	commit;*/
	end;
	
	procedure excluir_lote_contabil(nr_lote_contabil_p	varchar2,
					nm_usuario_p		varchar2,
					ds_erro_p	out	varchar2,
					cd_estabelecimento_p	number default null) is
	
	cd_log_w			number(10);
	ds_erro_w			varchar2(255);
	nr_lote_contabil_w		number(10);
	
	begin
	select	nvl(max(nr_lote_contabil),0)
	into	nr_lote_contabil_w
	from	lote_contabil
	where	nr_lote_externo		= nr_lote_contabil_p 
	and	cd_estabelecimento	= cd_estabelecimento_p;
	
	begin
	ctb_excluir_lote(nr_lote_contabil_w,nm_usuario_p);
	exception when others then
		ds_erro_w	:= substr('Erro ao excluir o lote: ' || nr_lote_contabil_w ||  ' - ' || sqlerrm(sqlcode),1,255);
	end;
	ds_erro_p		:= ds_erro_w;
	end;
	
	procedure desfazer_lote_contabil(	nr_lote_contabil_p	varchar2,
					nm_usuario_p	varchar2,
					ds_erro_p	out	varchar2,
					cd_estabelecimento_p	number default null ) is
	
	cd_log_w			number(10);
	ds_erro_w			varchar2(255);
	dt_atualizacao_saldo_w		date;
	nr_lote_contabil_w		number(10);
	
	begin
	
	select	nvl(max(nr_lote_contabil),0)
	into	nr_lote_contabil_w
	from	lote_contabil
	where	nr_lote_externo		= nr_lote_contabil_p 
	and	cd_estabelecimento	= cd_estabelecimento_p;
	
	select	max(dt_atualizacao_saldo)
	into	dt_atualizacao_saldo_w
	from	lote_contabil
	where	nr_lote_contabil	= nr_lote_contabil_w;
	
	if	(dt_atualizacao_saldo_w is not null) then
		ds_erro_w	:= substr('O lote (W/S): ' || nr_lote_contabil_w || '/' || nr_lote_contabil_p || ' ja foi atualizado no Tasy!',1,255);
	end if;
	
	if	(nvl(ds_erro_w,'X') = 'X') then
		begin
		delete	ctb_movimento
		where	nr_lote_contabil	= nr_lote_contabil_w
		and	nm_usuario_nrec	= 'IntegrSen';
		
		update	lote_contabil
		set	vl_debito		= 0,
			vl_credito		= 0,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_lote_contabil	= nr_lote_contabil_w;
			
		exception when others then
		ds_erro_w	:= substr('Erro ao excluir o lote: ' || nr_lote_contabil_p ||  ' - ' || sqlerrm(sqlcode),1,255);
		end;
	end if;
	ds_erro_p		:= ds_erro_w;
	end;
end wheb_integr_sen_contab;
/
