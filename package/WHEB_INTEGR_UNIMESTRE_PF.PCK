create or replace
package WHEB_INTEGR_UNIMESTRE_PF is

procedure integra_pf_unimestre(	nm_usuario_p	varchar2);

procedure excluir_pf(	cd_pessoa_fisica_p		varchar2);

end WHEB_INTEGR_UNIMESTRE_PF;
/
create or replace
package body WHEB_INTEGR_UNIMESTRE_PF is

	procedure integra_pf_unimestre(nm_usuario_p	varchar2) is 

	cd_municipio_ibge_w	varchar2(6);
	cd_nacionalidade_w	varchar2(8);
	cd_pessoa_fisica_w	varchar2(10);
	ds_erro_w		varchar2(2000);
	ie_estado_civil_w	varchar2(2);
	nr_sequencia_w		number(10)	:= 0;
	nr_seq_pais_w		number(10);
	nr_cep_cidade_nasc_w	varchar2(15);
	
	cursor C01 is
	select	distinct
		cd_integracao,
		ds_acao,
		cd_pessoa,
		cd_pai,
		cd_mae,
		substr(nm_pessoa,1,60) nm_pessoa,
		dt_nascimento,
		ds_cidade_nasc,
		ds_estado_nasc,
		ds_logradouro,
		ds_logradouro_nro,
		ds_complemento,
		ds_cep,
		ds_bairro,
		ds_cidade,
		ds_estado,
		ds_pais,
		ds_sexo,
		ds_nacionalidade,
		ds_identidade,
		ds_identidade_orgao_exp,
		ds_cpf,
		nm_pai,
		nm_mae,
		cd_estado_civil,
		nm_conjuge
	from	vx_integra_tasy_pessoa_f@mysql;

	Vet01	C01%Rowtype;

	begin

	open C01;
	loop
	fetch C01 into	
		Vet01;
	exit when C01%notfound;
		begin
		
		cd_municipio_ibge_w	:= '';
		cd_nacionalidade_w	:= '';
		ds_erro_w		:= '';
		ie_estado_civil_w	:= '';
		nr_cep_cidade_nasc_w	:= '';
		nr_seq_pais_w		:= null;
		nr_sequencia_w		:= 0;
		
		/*Cadastro de municipio IBGE */
		if	(nvl(vet01.ds_cidade,'X') <> 'X') then
			
			select	nvl(max(cd_municipio_ibge),'')
			into	cd_municipio_ibge_w
			from	sus_municipio
			where	upper(ds_municipio) = upper(vet01.ds_cidade);
			
		end if;
		
		/*Pa�s - complemento*/
		if	(nvl(vet01.ds_pais,'X') <> 'X') then
			
			select	max(nr_sequencia)
			into	nr_seq_pais_w
			from	pais
			where	upper(nm_pais) = upper(vet01.ds_pais);
			
		end if;
		
		/*Cep da cidade natal */
		if	(nvl(vet01.ds_cidade_nasc,'X') <> 'X') then
		
			select	nvl(max(cd_cep),'')
			into	nr_cep_cidade_nasc_w
			from	cep_localidade_v
			where	upper(nm_localidade)	= upper(vet01.ds_cidade_nasc)
			and	upper(ds_uf)		= upper(vet01.ds_estado_nasc);
		
		end if;
		
		/* Nacionalidade*/
		if	(nvl(vet01.ds_nacionalidade,'X') <> 'X') then
			select	nvl(max(cd_nacionalidade),'')
			into	cd_nacionalidade_w
			from	nacionalidade
			where	upper(ds_nacionalidade) = upper(vet01.ds_nacionalidade);
			
		end if;
		
		/* Estado civil */
		if	(nvl(vet01.cd_estado_civil,0) <> 0) then
			ie_estado_civil_w	:= to_char(vet01.cd_estado_civil);
		end if;
		
		/* Inserir cadastro da pessoa*/
		if	(vet01.ds_acao = 'I') and
			(nvl(vet01.nm_pessoa,'X') <> 'X') then
		
			
			select	pessoa_fisica_seq.nextval
			into	cd_pessoa_fisica_w
			from	dual;
			
			
			begin
			insert into pessoa_fisica(
				cd_pessoa_fisica,
				nm_pessoa_fisica,
				dt_nascimento,
				ie_sexo,
				cd_nacionalidade,
				nr_identidade,
				ds_orgao_emissor_ci,
				nr_cpf,
				ie_estado_civil,
				cd_sistema_ant,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nm_usuario_original,
				nm_pessoa_fisica_sem_acento,
				nm_pessoa_pesquisa,
				ie_tipo_pessoa)
			values(	cd_pessoa_fisica_w,
				vet01.nm_pessoa,
				vet01.dt_nascimento,
				vet01.ds_sexo,
				cd_nacionalidade_w,
				vet01.ds_identidade,
				substr(vet01.ds_identidade_orgao_exp,1,10),
				vet01.ds_cpf,
				ie_estado_civil_w,
				vet01.cd_pessoa,
				nm_usuario_p,
				sysdate,
				'Unimestre',
				sysdate,
				'Unimestre',
				elimina_acentuacao(vet01.nm_pessoa),
				padronizar_nome(vet01.nm_pessoa),
				2);
			exception when others then
				ds_erro_w	:= substr('Erro ao inserir a Pessoa: ' || vet01.cd_pessoa || ' ERRO: ' || sqlerrm(sqlcode),1,2000);
				Raise_application_error(-20011,ds_erro_w);
			end;
			/* Complemento Residencial */
			if	(nvl(vet01.ds_logradouro,'X') <> 'X') then
			
				nr_sequencia_w	:= nr_sequencia_w + 1;
				
				begin
				insert into compl_pessoa_fisica(
					cd_pessoa_fisica,
					nr_sequencia,
					ie_tipo_complemento,
					dt_atualizacao,
					nm_usuario,
					nm_contato,
					ds_endereco,
					cd_cep,
					nr_endereco,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					cd_municipio_ibge,
					sg_estado,
					nr_seq_pais,
					nr_telefone,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values(	cd_pessoa_fisica_w,
					nr_sequencia_w,
					1,
					sysdate,
					'Unimestre',
					'',
					vet01.ds_logradouro,
					vet01.ds_cep,
					vet01.ds_logradouro_nro,
					vet01.ds_complemento,
					vet01.ds_bairro,
					vet01.ds_cidade,
					cd_municipio_ibge_w,
					vet01.ds_estado,
					nr_seq_pais_w,
					'',
					sysdate,
					'Unimestre');
				exception when others then
					ds_erro_w	:= substr('Erro Insert Compl Resid-PF: ' || vet01.cd_pessoa || ' ERRO: ' || sqlerrm(sqlcode),1,2000);
				end;
			end if;
			
			/* Complemento do M�E*/
			if	(nvl(vet01.nm_mae,'X') <> 'X') then
			
				nr_sequencia_w	:= nr_sequencia_w + 1;
				
				begin
				insert into compl_pessoa_fisica(
					cd_pessoa_fisica,
					nr_sequencia,
					ie_tipo_complemento,
					dt_atualizacao,
					nm_usuario,
					nm_contato,
					ds_endereco,
					cd_cep,
					nr_endereco,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					cd_municipio_ibge,
					sg_estado,
					nr_seq_pais,
					nr_telefone,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values(	cd_pessoa_fisica_w,
					nr_sequencia_w,
					5,
					sysdate,
					'Unimestre',
					vet01.nm_mae,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					null,
					'',
					sysdate,
					'Unimestre');
				exception when others then
					ds_erro_w	:= substr('Erro Insert Compl Resid-M�e: ' || vet01.cd_pessoa || ' ERRO: ' || sqlerrm(sqlcode),1,2000);
				end;
			end if;
			/* Complemento do PAI*/
			if	(nvl(vet01.nm_pai,'X') <> 'X') then
			
				nr_sequencia_w	:= nr_sequencia_w + 1;
				
				begin
				insert into compl_pessoa_fisica(
					cd_pessoa_fisica,
					nr_sequencia,
					ie_tipo_complemento,
					dt_atualizacao,
					nm_usuario,
					nm_contato,
					ds_endereco,
					cd_cep,
					nr_endereco,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					cd_municipio_ibge,
					sg_estado,
					nr_seq_pais,
					nr_telefone,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values(	cd_pessoa_fisica_w,
					nr_sequencia_w,
					4,
					sysdate,
					'Unimestre',
					vet01.nm_pai,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					null,
					'',
					sysdate,
					'Unimestre');
				exception when others then
					ds_erro_w	:= substr('Erro Insert Compl Resid-Pai: ' || vet01.cd_pessoa || ' ERRO: ' || sqlerrm(sqlcode),1,2000);
				end;
			end if;
			
			/* Complemento do C�njugue */
			
			if	(nvl(vet01.nm_conjuge,'X') <> 'X') then
			
				nr_sequencia_w	:= nr_sequencia_w + 1;
				begin
				insert into compl_pessoa_fisica(
					cd_pessoa_fisica,
					nr_sequencia,
					ie_tipo_complemento,
					dt_atualizacao,
					nm_usuario,
					nm_contato,
					ds_endereco,
					cd_cep,
					nr_endereco,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					cd_municipio_ibge,
					sg_estado,
					nr_seq_pais,
					nr_telefone,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values(	cd_pessoa_fisica_w,
					nr_sequencia_w,
					6,
					sysdate,
					'Unimestre',
					vet01.nm_conjuge,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					null,
					'',
					sysdate,
					'Unimestre');
				exception when others then
					ds_erro_w	:= substr('Erro Insert Compl Resid-M�e: ' || vet01.cd_pessoa || ' ERRO: ' || sqlerrm(sqlcode),1,2000);
				end;
			end if;
		elsif	(vet01.ds_acao = 'A') then /* Atualiza��o do Cadastro */
			begin
			
			select	nvl(max(cd_pessoa_fisica),'X')
			into	cd_pessoa_fisica_w
			from	pessoa_fisica
			where	cd_sistema_ant	= vet01.cd_pessoa
			and	nm_usuario_original	= 'Unimestre';
			
			if	(cd_pessoa_fisica_w <> 'X') then
				
				begin
				update	pessoa_fisica
				set	nm_pessoa_fisica	= vet01.nm_pessoa,
					dt_nascimento		= vet01.dt_nascimento,
					ie_sexo			= vet01.ds_sexo,
					cd_nacionalidade	= cd_nacionalidade_w,
					nr_identidade		= vet01.ds_identidade,
					ie_estado_civil		= ie_estado_civil_w,
					nr_cpf			= vet01.ds_cpf,
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
				exception when others then
					ds_erro_w	:= substr('Erro atualizar Cad. PF: ' || cd_pessoa_fisica_w || '/' || vet01.cd_pessoa || ': ' || sqlerrm(sqlcode),1,2000);
				end;
				
				select	nvl(max(nr_sequencia),0)
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	ie_tipo_complemento	= 1
				and	cd_pessoa_fisica	= cd_pessoa_fisica_w;
				
				if	(nr_sequencia_w <> 0) then
				
					begin
					update	compl_pessoa_fisica
					set	ds_endereco		= vet01.ds_logradouro,
						cd_cep			= substr(vet01.ds_cep,1,15),
						nr_endereco		= somente_numero(substr(vet01.ds_logradouro_nro,1,5)),
						ds_complemento		= substr(vet01.ds_complemento,1,40),
						ds_bairro		= substr(vet01.ds_bairro,1,40),
						ds_municipio		= substr(vet01.ds_cidade,1,40),
						cd_municipio_ibge	= cd_municipio_ibge_w,
						sg_estado		= vet01.ds_estado,
						nr_seq_pais		= nr_seq_pais_w,
						nm_usuario		= nm_usuario_p,
						dt_atualizacao		= sysdate
					where	cd_pessoa_fisica	= cd_pessoa_fisica_w
					and	nr_sequencia		= nr_sequencia_w;
					exception when others then
						ds_erro_w := substr('Erro atualizar Compl. PF: ' || cd_pessoa_fisica_w || '/' || vet01.cd_pessoa || ': ' || sqlerrm(sqlcode),1,2000);
					end;
				end if;
			end if;
			end;
		elsif	(vet01.ds_acao = 'R') then
			excluir_pf(vet01.cd_pessoa);
		end if;
		/*execute immediate 'begin spa_fin_integracao@mysql(' || vet01.cd_integracao || ',substr(' || ds_erro_w || ',1,255)); end;';*/
		/*spa_fin_integracao@mysql(vet01.cd_integracao,substr(ds_erro_w,1,255));*/
		commit;
		end;
	end loop;
	close C01;
	
	commit;

	end integra_pf_unimestre;
	
	procedure excluir_pf(	cd_pessoa_fisica_p		varchar2) is
	
	cd_pessoa_fisica_w	varchar2(10);
	nm_pessoa_fisica_w	varchar2(60);
	ds_chave_w		varchar2(255);
	ds_erro_w		varchar2(255);
	qt_registro_w		number(10);
	
	begin
	
	select	nvl(max(cd_pessoa_fisica),'')
	into	cd_pessoa_fisica_w
	from	pessoa_fisica
	where	cd_sistema_ant	= cd_pessoa_fisica_p
	and	nm_usuario_original	= 'Unimestre';
	
	if	(nvl(cd_pessoa_fisica_w,'0') <> '0') then
	
		select	nm_pessoa_fisica
		into	nm_pessoa_fisica_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		ds_chave_w	:= substr('CD_PESSOA_FISICA= ' || cd_pessoa_fisica_w || ' NM_PESSOA_FISICA= ' || nm_pessoa_fisica_w || ' CD_SISTEMA_ANT=' || cd_pessoa_fisica_p,1,255);
	
		select	count(*)
		into	qt_registro_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		if	(qt_registro_w > 0) then
		
			begin
			delete from compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
	
			gravar_log_exclusao('COMPL_PESSOA_FISICA','Unimestre',ds_chave_w,'N');
			exception when others then
				ds_erro_w	:= substr('Erro ao excluir Complemento da PF: ' || cd_pessoa_fisica_p || '=' || sqlerrm(sqlcode),1,255);
			end;
		end if;
		
		begin
		delete	from pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		gravar_log_exclusao('PESSOA_FISICA','Unimestre',ds_chave_w,'N');
		
		exception when others then
				ds_erro_w	:= substr('Erro ao excluir Complemento da PF: ' || cd_pessoa_fisica_p || '=' || sqlerrm(sqlcode),1,255);
		end;
		
	end if;
	commit;
	/*spa_fin_integracao@mysql(vet01.cd_integracao,substr(ds_erro_w,1,255));*/
	
	end excluir_pf;
	
end WHEB_INTEGR_UNIMESTRE_PF;
/