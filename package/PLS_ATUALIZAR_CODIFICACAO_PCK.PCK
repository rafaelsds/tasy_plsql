create or replace package pls_atualizar_codificacao_pck as

	procedure pls_atualizar_codificacao	(dt_referencia_p	date);
	
	function get_formacao_preco(ie_preco_p			varchar2) return varchar2;
	function get_regulamentacao(ie_regulamentacao_p		varchar2) return varchar2;
	function get_ato_cooperado(ie_ato_cooperado_p		varchar2) return varchar2;
	function get_tipo_contratacao(ie_tipo_contratacao_p	varchar2) return varchar2;
	function get_tipo_pessoa_contrato(ie_tipo_pessoa_p	varchar2) return varchar2;
	function get_tipo_relacao(ie_tipo_relacao_p		varchar2) return varchar2;
        function get_tipo_despesa(ie_tipo_despesa_p             varchar2) return varchar2;
	function get_contratacao_regulamentacao(ie_tipo_contratacao_p varchar2, ie_regulamentacao_p varchar2) return varchar2;
	
end pls_atualizar_codificacao_pck;
/

create or replace package body pls_atualizar_codificacao_pck as
	dt_referencia_w		date;
	nr_seq_cod_versao_w	number(10);
	
	/* Cursor 1 */
	nr_seq_codificacao_w	number(10);
	ie_codificacao_w	varchar2(2);
	
	/* Cursor 2 */
	ds_valor_w		varchar2(2);
	ie_preco_w		varchar2(2);
	ie_regulamentacao_w	varchar2(2);
	ie_ato_cooperado_w	varchar2(10);
	ie_tipo_contratacao_w	varchar2(2);
	ie_tipo_pessoa_w	varchar2(2);
	ie_tipo_relacao_w	varchar2(2);
        ie_tipo_despesa_w       varchar2(2);
	
	/* Forma��o de pre�o */
	ie_form_misto_w		varchar2(2);
	ie_form_pos_custo_w	varchar2(2);
	ie_form_pos_rateio_w	varchar2(2);
	ie_form_pre_w		varchar2(2);
	
	/* Regulamenta��o */
	ie_regul_adaptado_w	varchar2(2);
	ie_regul_pos_w		varchar2(2);
	ie_regul_pre_w		varchar2(2);
	
	/* Tipo de ato cooperado */
	ie_ato_coop_aux_w	varchar2(10);
	ie_ato_coop_prin_w	varchar2(10);
	ie_ato_nao_coop_w	varchar2(10);
	
	/* Tipo de contrata��o */
	ie_colet_empres_w	varchar2(2);
	ie_colet_adesao_w	varchar2(2);
	ie_indiv_familiar_w	varchar2(2);
	
	/* Tipo de contrato (PF e PJ) */
	ie_contrato_pf_w	varchar2(2);
	ie_contrato_pj_w	varchar2(2);
	
	/* Tipo de rela��o com a OPS */
	ie_relac_cooper_w	varchar2(2);
	ie_relac_fornec_w	varchar2(2);
	ie_relac_rede_dir_w	varchar2(2);
	ie_relac_rede_indir_w	varchar2(2);
	ie_relac_rede_prop_w	varchar2(2);
	ie_relac_nao_w		varchar2(2);
	
	/* Tipo de contrata��o / Regulamenta��o */
	ie_individual_pre_w		varchar2(2);
	ie_individual_pos_w		varchar2(2);
	ie_individual_adaptado_w	varchar2(2);
	ie_empresarial_pre_w		varchar2(2);
	ie_empresarial_pos_w		varchar2(2);
	ie_empresarial_adaptado_w	varchar2(2);
	ie_adesao_pre_w			varchar2(2);
	ie_adesao_pos_w			varchar2(2);
	ie_adesao_adaptado_w		varchar2(2);

        /* Tipo de despesa */
        ie_despesa_proced_w             varchar2(2);
        ie_despesa_taxas_w              varchar2(2);
        ie_despesa_diarias_w            varchar2(2);
        ie_despesa_pacotes_w            varchar2(2);
	
	Cursor C01 is
		select	a.nr_sequencia,
			a.ie_codificacao
		from	pls_contab_codificacao	a
		where	a.nr_seq_versao_contab	= nr_seq_cod_versao_w;
	
	Cursor C02 is
		select	a.ds_valor,
			a.ie_preco,
			a.ie_regulamentacao,
			a.ie_ato_cooperado,
			a.ie_tipo_contratacao,
			a.ie_tipo_pessoa_contrato,
			a.ie_tipo_relacao,
                        a.ie_tipo_despesa
		from	pls_contab_codific_item	a
		where	a.nr_seq_codificacao = nr_seq_codificacao_w
		group by	
			a.ds_valor,
			a.ie_preco,
			a.ie_regulamentacao,
			a.ie_ato_cooperado,
			a.ie_tipo_contratacao,
			a.ie_tipo_pessoa_contrato,
			a.ie_tipo_relacao,
                        a.ie_tipo_despesa;
	
	procedure pls_atualizar_codificacao
			(	dt_referencia_p		date) as
	
	begin
	dt_referencia_w	:= dt_referencia_p;
	
	/* Forma��o de pre�o */
	ie_form_misto_w		:= null;
	ie_form_pos_custo_w	:= null;
	ie_form_pos_rateio_w	:= null;
	ie_form_pre_w		:= null;
	
	/* Regulamenta��o */
	ie_regul_adaptado_w	:= null;
	ie_regul_pos_w		:= null;
	ie_regul_pre_w		:= null;
	
	/* Tipo de ato cooperado */
	ie_ato_coop_aux_w	:= null;
	ie_ato_coop_prin_w	:= null;
	ie_ato_nao_coop_w	:= null;
	
	/* Tipo de contrata��o */
	ie_colet_empres_w	:= null;
	ie_colet_adesao_w	:= null;
	ie_indiv_familiar_w	:= null;
	
	/* Tipo de contrato (PF e PJ) */
	ie_contrato_pf_w	:= null;
	ie_contrato_pj_w	:= null;
	
	/* Tipo de rela��o com a OPS */
	ie_relac_cooper_w	:= null;
	ie_relac_fornec_w	:= null;
	ie_relac_rede_dir_w	:= null;
	ie_relac_rede_indir_w	:= null;
	ie_relac_rede_prop_w	:= null;
	ie_relac_nao_w		:= null;
	
	/* Tipo de contrata��o / Regulamenta��o */
	ie_individual_pre_w		:= null;
	ie_individual_pos_w		:= null;
	ie_individual_adaptado_w	:= null;
	ie_empresarial_pre_w		:= null;
	ie_empresarial_pos_w		:= null;
	ie_empresarial_adaptado_w	:= null;
	ie_adesao_pre_w			:= null;
	ie_adesao_pos_w			:= null;
	ie_adesao_adaptado_w		:= null;

        /* Tipo de despesa */
        ie_despesa_proced_w             := null;
        ie_despesa_taxas_w              := null;
        ie_despesa_diarias_w            := null;
        ie_despesa_pacotes_w            := null;
	
	begin
		select	nr_sequencia
		into	nr_seq_cod_versao_w
		from	pls_contab_versao a
		where	dt_referencia_w between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_referencia_w);
	exception
	when no_data_found then
		/*N�o foi poss�vel executar a a��o, motivo: 
		N�o foi poss�vel encotrar uma vers�o para a codifica��o cont�bil em vig�ncia dispon�vel para a data em refer�ncia !
		Solicite uma atualiza��o do cadastro.*/
		wheb_mensagem_pck.exibir_mensagem_abort(224729);
	when too_many_rows then
		/*N�o foi poss�vel executar a a��o, motivo: 
		Existe mais de uma vers�o de codifica��o cont�bil que est� em vig�ncia!
		Solicite uma verifica��o dos cadastros.*/
		wheb_mensagem_pck.exibir_mensagem_abort(224731);
	end;
		
	open C01;
	loop
	fetch C01 into	
		nr_seq_codificacao_w,
		ie_codificacao_w;
	exit when C01%notfound;
		begin
		open C02;
		loop
		fetch C02 into	
			ds_valor_w,
			ie_preco_w,
			ie_regulamentacao_w,
			ie_ato_cooperado_w,
			ie_tipo_contratacao_w,
			ie_tipo_pessoa_w,
			ie_tipo_relacao_w,
                        ie_tipo_despesa_w;
		exit when C02%notfound;
			begin
			if	(ie_codificacao_w = 'FP') then
				if	(ie_preco_w = '1') then
					ie_form_pre_w		:= ds_valor_w;
				elsif	(ie_preco_w = '2') then
					ie_form_pos_rateio_w	:= ds_valor_w;
				elsif	(ie_preco_w = '3') then
					ie_form_pos_custo_w	:= ds_valor_w;
				elsif	(ie_preco_w = '4') then
					ie_form_misto_w		:= ds_valor_w;
				end if;
			elsif	(ie_codificacao_w = 'R') then
				if	(ie_regulamentacao_w = 'P') then
					ie_regul_pos_w		:= ds_valor_w;
				elsif	(ie_regulamentacao_w = 'A') then
					ie_regul_adaptado_w	:= ds_valor_w;
				elsif	(ie_regulamentacao_w = 'R') then
					ie_regul_pre_w		:= ds_valor_w;
				end if;
			elsif	(ie_codificacao_w = 'TA') then
				if	(ie_ato_cooperado_w = '1') then
					ie_ato_coop_prin_w	:= ds_valor_w;
				elsif	(ie_ato_cooperado_w = '2') then
					ie_ato_coop_aux_w	:= ds_valor_w;
				elsif	(ie_ato_cooperado_w = '3') then
					ie_ato_nao_coop_w	:= ds_valor_w;
				end if;
			elsif	(ie_codificacao_w = 'TC') then
				if	(ie_tipo_contratacao_w = 'I') then
					ie_indiv_familiar_w	:= ds_valor_w;
				elsif	(ie_tipo_contratacao_w = 'CE') then
					ie_colet_empres_w	:= ds_valor_w;
				elsif	(ie_tipo_contratacao_w = 'CA') then
					ie_colet_adesao_w	:= ds_valor_w;
				end if;
			elsif	(ie_codificacao_w = 'TP') then
				if	(ie_tipo_pessoa_w = 'PF') then
					ie_contrato_pf_w	:= ds_valor_w;
				elsif	(ie_tipo_pessoa_w = 'PJ') then
					ie_contrato_pj_w	:= ds_valor_w;
				end if;
			elsif	(ie_codificacao_w = 'TR') then
				if	(ie_tipo_relacao_w = 'P') then
					ie_relac_rede_prop_w	:= ds_valor_w;
				elsif	(ie_tipo_relacao_w = 'D') then
					ie_relac_rede_dir_w	:= ds_valor_w;
				elsif	(ie_tipo_relacao_w = 'I') then
					ie_relac_rede_indir_w	:= ds_valor_w;
				elsif	(ie_tipo_relacao_w = 'C') then
					ie_relac_cooper_w	:= ds_valor_w;
				elsif	(ie_tipo_relacao_w = 'F') then
					ie_relac_fornec_w	:= ds_valor_w;
				elsif	(ie_tipo_relacao_w = 'N') then
					ie_relac_nao_w		:= ds_valor_w;
				end if;
			elsif	(ie_codificacao_w = 'RC') then
				if	(ie_tipo_contratacao_w = 'I') then
					if	(ie_regulamentacao_w = 'P') then
						ie_individual_pos_w		:= ds_valor_w;
					elsif	(ie_regulamentacao_w = 'A') then
						ie_individual_adaptado_w	:= ds_valor_w;
					elsif	(ie_regulamentacao_w = 'R') then
						ie_individual_pre_w		:= ds_valor_w;
					end if;
				elsif	(ie_tipo_contratacao_w = 'CE') then
					if	(ie_regulamentacao_w = 'P') then
						ie_empresarial_pos_w		:= ds_valor_w;
					elsif	(ie_regulamentacao_w = 'A') then
						ie_empresarial_adaptado_w	:= ds_valor_w;
					elsif	(ie_regulamentacao_w = 'R') then
						ie_empresarial_pre_w		:= ds_valor_w;
					end if;
				elsif	(ie_tipo_contratacao_w = 'CA') then
					if	(ie_regulamentacao_w = 'P') then
						ie_adesao_pos_w		:= ds_valor_w;
					elsif	(ie_regulamentacao_w = 'A') then
						ie_adesao_adaptado_w	:= ds_valor_w;
					elsif	(ie_regulamentacao_w = 'R') then
						ie_adesao_pre_w		:= ds_valor_w;
					end if;
				end if;
                        elsif   (ie_codificacao_w = 'TD') then
                                if      (ie_tipo_despesa_w = 1) then
                                        ie_despesa_proced_w             := ds_valor_w;
                                elsif   (ie_tipo_despesa_w = 2) then
                                        ie_despesa_taxas_w              := ds_valor_w;
                                elsif   (ie_tipo_despesa_w = 3) then
                                        ie_despesa_diarias_w            := ds_valor_w;
                                elsif   (ie_tipo_despesa_w = 4) then
                                        ie_despesa_pacotes_w            := ds_valor_w;
                                end if;
			end if;
			end;
		end loop;
		close C02;
		end;
	end loop;
	close C01;
	end;
	
	/* Getters e Setters */
	function get_formacao_preco(ie_preco_p varchar2) return varchar2 as
	begin
	if	(ie_preco_p = '1') then
		if	(ie_form_pre_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221877);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Forma��o de pre�o" para produtos pr�-estabelecidos! Favor verificar. */
		else
			return ie_form_pre_w;
		end if;
	elsif	(ie_preco_p = '2') then
		if	(ie_form_pos_rateio_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221878);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Forma��o de pre�o" para produtos p�s-estabelecidos por rateio! Favor verificar. */
		else
			return ie_form_pos_rateio_w;
		end if;
	elsif	(ie_preco_p = '3') then
		if	(ie_form_pos_custo_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221881);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Forma��o de pre�o" para produtos p�s-estabelecidos por custo operacional! Favor verificar. */
		else
			return ie_form_pos_custo_w;
		end if;
	elsif	(ie_preco_p = '4') then
		if	(ie_form_misto_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221882);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Forma��o de pre�o" para produtos mistos! Favor verificar. */
		else
			return ie_form_misto_w;
		end if;
	else
		return 'FP';
	end if;
	end;
	
	function get_regulamentacao(ie_regulamentacao_p varchar2) return varchar2 as
	begin
	if	(ie_regulamentacao_p = 'P') then
		if	(ie_regul_pos_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221908);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Regulamenta��o" para produtos p�s! Favor verificar. */
		else
			return ie_regul_pos_w;
		end if;
	elsif	(ie_regulamentacao_p = 'A') then
		if	(ie_regul_adaptado_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221909);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Regulamenta��o" para produtos adaptados! Favor verificar. */
		else
			return ie_regul_adaptado_w;
		end if;
	elsif	(ie_regulamentacao_p = 'R') then
		if	(ie_regul_pre_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221910);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Regulamenta��o" para produtos pr�! Favor verificar. */
		else
			return ie_regul_pre_w;
		end if;
	else
		return 'R';
	end if;
	end;
	
	function get_ato_cooperado(ie_ato_cooperado_p varchar2) return varchar2 as
	begin
	if	(ie_ato_cooperado_p = '1') then
		if	(ie_ato_coop_prin_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221914);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de ato cooperado" para ato cooperativo principal! Favor verificar. */
		else
			return ie_ato_coop_prin_w;
		end if;
	elsif	(ie_ato_cooperado_p = '2') then
		if	(ie_ato_coop_aux_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221915);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de ato cooperado" para ato cooperativo auxiliar! Favor verificar. */
		else
			return ie_ato_coop_aux_w;
		end if;
	elsif	(ie_ato_cooperado_p = '3') then
		if	(ie_ato_nao_coop_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221916);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de ato cooperado" para ato n�o cooperativo! Favor verificar. */
		else
			return ie_ato_nao_coop_w;
		end if;
	else
		return 'TA';
	end if;
	end;
	
	function get_tipo_contratacao(ie_tipo_contratacao_p varchar2) return varchar2 as
	begin
	if	(ie_tipo_contratacao_p = 'I') then
		if	(ie_indiv_familiar_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221911);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o" para produtos individual/familiar! Favor verificar. */
		else
			return ie_indiv_familiar_w;
		end if;
	elsif	(ie_tipo_contratacao_p = 'CE') then
		if	(ie_colet_empres_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221912);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o" para produtos coletivos empresariais! Favor verificar. */
		else
			return ie_colet_empres_w;
		end if;
	elsif	(ie_tipo_contratacao_p = 'CA') then
		if	(ie_colet_adesao_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221913);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o" para produtos coletivos por ades�o! Favor verificar. */
		else
			return ie_colet_adesao_w;
		end if;
	else
		return 'TC';
	end if;
	end;
	
	function get_tipo_pessoa_contrato(ie_tipo_pessoa_p varchar2) return varchar2 as
	begin
	if	(ie_tipo_pessoa_p = 'PF') then
		if	(ie_contrato_pf_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221917);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrato (Pessoa f�sica ou Jur�dica)" para pessoa f�sica! Favor verificar. */
		else
			return ie_contrato_pf_w;
		end if;
	elsif	(ie_tipo_pessoa_p = 'PJ') then
		if	(ie_contrato_pj_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221918);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrato (Pessoa f�sica ou Jur�dica)" para pessoa jur�dica! Favor verificar. */
		else
			return ie_contrato_pj_w;
		end if;
	else
		return 'TP';
	end if;
	end;
	
	function get_tipo_relacao(ie_tipo_relacao_p varchar2) return varchar2 as
	begin
	if	(ie_tipo_relacao_p = 'P') then
		if	(ie_relac_rede_prop_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221920);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de rela��o com a OPS" para rede pr�pria! Favor verificar. */
		else
			return ie_relac_rede_prop_w;
		end if;
	elsif	(ie_tipo_relacao_p = 'D') then
		if	(ie_relac_rede_dir_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221921);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de rela��o com a OPS" para rede contratualizada direta! Favor verificar. */
		else
			return ie_relac_rede_dir_w;
		end if;
	elsif	(ie_tipo_relacao_p = 'I') then
		if	(ie_relac_rede_indir_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221922);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de rela��o com a OPS" para rede contratualizada indireta! Favor verificar. */
		else
			return ie_relac_rede_indir_w;
		end if;
	elsif	(ie_tipo_relacao_p = 'C') then
		if	(ie_relac_cooper_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221923);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de rela��o com a OPS" para cooperados! Favor verificar. */
		else
			return ie_relac_cooper_w;
		end if;
	elsif	(ie_tipo_relacao_p = 'F') then
		if	(ie_relac_fornec_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(221924);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de rela��o com a OPS" para fornecedores! Favor verificar. */
		else
			return ie_relac_fornec_w;
		end if;
	elsif	(ie_tipo_relacao_p = 'N') then
		if	(ie_relac_nao_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(1043084);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de rela��o com a OPS" para rede n�o contratualizada! Favor verificar. */
		else
			return ie_relac_nao_w;
		end if;
	else
		return 'TR';
	end if;
	end;
	
	function get_contratacao_regulamentacao(ie_tipo_contratacao_p varchar2, ie_regulamentacao_p varchar2) return varchar2 as
	begin
	if	(ie_tipo_contratacao_p = 'I') then
		if	(ie_regulamentacao_p = 'R') then
			if	(ie_individual_pre_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222875);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto n�o regulamentado individual/familiar! Favor verificar. */
			else
				return ie_individual_pre_w;
			end if;
		elsif	(ie_regulamentacao_p = 'P') then
			if	(ie_individual_pos_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222876);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto regulamentado individual/familiar! Favor verificar. */
			else
				return ie_individual_pos_w;
			end if;
		elsif	(ie_regulamentacao_p = 'A') then
			if	(ie_individual_adaptado_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222877);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto adaptado individual/familiar! Favor verificar. */
			else
				return ie_individual_adaptado_w;
			end if;
		else
			return 'RC';
		end if;
	elsif	(ie_tipo_contratacao_p = 'CE') then
		if	(ie_regulamentacao_p = 'R') then
			if	(ie_empresarial_pre_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222878);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto n�o regulamentado coletivo empresarial! Favor verificar. */
			else
				return ie_empresarial_pre_w;
			end if;
		elsif	(ie_regulamentacao_p = 'P') then
			if	(ie_empresarial_pos_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222879);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto regulamentado coletivo empresarial! Favor verificar. */
			else
				return ie_empresarial_pos_w;
			end if;
		elsif	(ie_regulamentacao_p = 'A') then
			if	(ie_empresarial_adaptado_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222880);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto adaptado coletivo empresarial! Favor verificar. */
			else
				return ie_empresarial_adaptado_w;
			end if;
		else
			return 'RC';
		end if;
	elsif	(ie_tipo_contratacao_p = 'CA') then
		if	(ie_regulamentacao_p = 'R') then
			if	(ie_adesao_pre_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222881);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto n�o regulamentado coletivo por ades�o! Favor verificar. */
			else
				return ie_adesao_pre_w;
			end if;
		elsif	(ie_regulamentacao_p = 'P') then
			if	(ie_adesao_pos_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222882);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto regulamentado coletivo por ades�o! Favor verificar. */
			else
				return ie_adesao_pos_w;
			end if;
		elsif	(ie_regulamentacao_p = 'A') then
			if	(ie_adesao_adaptado_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(222883);
				/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de contrata��o / Regulamenta��o" para produto adaptado coletivo por ades�o! Favor verificar. */
			else
				return ie_adesao_adaptado_w;
			end if;
		else
			return 'RC';
		end if;
	else
		return 'RC';
	end if;
	end;


        function get_tipo_despesa(ie_tipo_despesa_p varchar2) return varchar2 as
	begin
	if	(ie_tipo_despesa_p = 1) then
		if	(ie_despesa_proced_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(1055884);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de despesa" para procedimentos! Favor verificar. */
		else
			return ie_despesa_proced_w;
		end if;
	elsif	(ie_tipo_despesa_p = 2) then
		if	(ie_despesa_taxas_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(1055885);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de despesa" para taxas! Favor verificar. */
		else
			return ie_despesa_taxas_w;
		end if;
	elsif	(ie_tipo_despesa_p = 3) then
		if	(ie_despesa_diarias_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(1055886);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de despesa" para di�rias! Favor verificar. */
		else
			return ie_despesa_diarias_w;
		end if;
	elsif	(ie_tipo_despesa_p = 4) then
		if	(ie_despesa_pacotes_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(1055887);
			/* Mensagem: N�o foi encontrada uma regra de codifica��o do tipo "Tipo de despesa" para pacotes! Favor verificar. */
		else
			return ie_despesa_pacotes_w;
		end if;
	else
		return 'TD';
	end if;
	end;

end pls_atualizar_codificacao_pck;
/