create or replace 
package pharm_item_pck as

type t_objeto_row is record (
	cd	number(15),
	ds	varchar2(2000),
	qt	number(10,5),
	sl	number(10,5),
	fn	varchar2(200),
	nr_seq_lf number(15),
	dt_suspensao date,
	nr_seq_horario number(15)
     );	
     
type t_objeto_row_data is table of t_objeto_row;
     
     
     function obter_lista (	nr_processo_p	number,
				ie_param_lf_p	varchar2,
				ie_param_70_p	varchar2) return t_objeto_row_data pipelined;
					
end pharm_item_pck;
/

create or replace package body pharm_item_pck as

function obter_lista (	nr_processo_p	number,
			ie_param_lf_p	varchar2,
			ie_param_70_p	varchar2) return t_objeto_row_data pipelined is
t_objeto_row_w		t_objeto_row;
cd_w	number(15);
ds_w	varchar2(2000);
qt_w	number(10,5);
sl_w	number(10,5);
fn_w	varchar2(200);
nr_seq_lf_w number(15);
dt_suspensao_w date;
nr_seq_horario_w number(15);

cursor C01 is
	select	to_char(a.cd_material) cd,
		decode(obter_se_item_frac(a.nr_sequencia),'N',substr(obter_desc_material(a.cd_material),1,100),'Frac - '||substr(obter_desc_material(a.cd_material),1,95)) ds,
		decode(obter_se_item_frac(a.nr_sequencia),'N',sum(nvl(qt_dispensar_hor,0)),1) qt,
		decode(obter_se_item_frac(a.nr_sequencia),'N',sum(nvl(qt_dispensar_hor,0)),1) sl, '' fn,
		b.nr_seq_lote_fornec nr_seq_lf,
		a.dt_suspensao dt_suspensao,
		a.nr_sequencia nr_seq_horario
	from    prescr_material b,
		prescr_mat_hor a
	where	b.nr_prescricao = a.nr_prescricao
	and 	b.nr_sequencia = a.nr_seq_material
	and 	((a.nr_seq_etiqueta is null) or (ie_param_lf_p in ('C', 'X')))
	and 	b.dt_suspensao is null
	and 	a.dt_suspensao is null
	and 	nvl(b.ie_regra_disp,'X') <> 'N'
	and 	a.nr_seq_processo = nr_processo_p
	and	obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
	and 	obter_se_conferencia_barras(a.cd_material, obter_setor_atendimento(obter_atendimento_prescr(b.nr_prescricao))) = 'S'
	and	nvl(a.ie_situacao,'A') <> 'I'
	and	nvl(b.ie_medicacao_paciente,'N') = 'N'
	and	nvl(ie_param_70_p, 'N') = 'N'
	group by a.cd_material,a.dt_suspensao,b.nr_seq_lote_fornec,a.nr_sequencia,decode(obter_se_item_frac(a.nr_sequencia),'N',substr(obter_desc_material(a.cd_material),1,100),'Frac - '||substr(obter_desc_material(a.cd_material),1,100))
	union
	select	to_char(a.cd_material) cd,
		substr(obter_desc_material(a.cd_material),1,100) ds,
		0 qt,0 sl,'' fn,
		b.nr_seq_lote_fornec nr_seq_lf,
		a.dt_suspensao,
		a.nr_sequencia nr_seq_horario
	from	prescr_material b,
		prescr_mat_hor a
	where 	b.nr_prescricao = a.nr_prescricao
	and	b.nr_sequencia = a.nr_seq_material
	and	((a.nr_seq_etiqueta is null) or (ie_param_lf_p in ('C', 'X')))
	and	b.dt_suspensao is null
	and	a.dt_suspensao is null
	and	nvl(b.ie_regra_disp,'X') = 'N'
	and	a.nr_seq_processo = nr_processo_p
	and	obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
	and	obter_se_conferencia_barras(a.cd_material, obter_setor_atendimento(obter_atendimento_prescr(b.nr_prescricao))) = 'S'
	and	nvl(a.ie_situacao, 'A') <> 'I'
	and	nvl(b.ie_medicacao_paciente,'N') = 'N'
	and	nvl(ie_param_70_p, 'N') = 'N'
	group by a.cd_material, a.dt_suspensao, b.nr_seq_lote_fornec, a.nr_sequencia
	union
	select 	obter_codigo_barras_processo(a.nr_seq_etiqueta, a.cd_material, 'F') cd,
		substr(obter_desc_expressao(346632)/*'manip/frac : '*/ || obter_desc_material(a.cd_material),1,100) ds,
		1 qt, 1 sl, '' fn,
		b.nr_seq_lote_fornec nr_seq_lf,
		a.dt_suspensao,
		a.nr_sequencia nr_seq_horario
	from 	prescr_material b,
		prescr_mat_hor a
	where	b.nr_prescricao = a.nr_prescricao
	and	b.nr_sequencia = a.nr_seq_material
	and	a.nr_seq_superior is null
	and	((a.nr_seq_etiqueta is not null) and (ie_param_lf_p = 'N'))
	and	a.nr_seq_processo = nr_processo_p
	and	obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
	and	obter_se_conferencia_barras(a.cd_material, obter_setor_atendimento(obter_atendimento_prescr(b.nr_prescricao))) = 'S'
	and 	nvl(a.ie_situacao, 'A') <> 'I'
	and	nvl(ie_param_70_p, 'N') = 'N'
	group by a.nr_seq_etiqueta, a.cd_material, a.dt_suspensao, b.nr_seq_lote_fornec, a.nr_sequencia
	union
	select 	obter_codigo_barras_processo(a.nr_seq_etiqueta, a.cd_material, 'F') cd,
		substr(obter_desc_expressao(346632)|| obter_Concat_comp_sol(a.nr_prescricao, a.nr_seq_solucao),1,100) ds,
		1 qt, 1 sl, '' fn,
		b.nr_seq_lote_fornec nr_seq_lf,
		a.dt_suspensao,
		null nr_seq_horario
	from 	prescr_material b,
		prescr_mat_hor a
	where	b.nr_prescricao = a.nr_prescricao
	and	b.nr_sequencia = a.nr_seq_material
	and	a.nr_seq_superior is null
	and	((a.nr_seq_etiqueta is not null) and (ie_param_lf_p = 'N'))
	and	a.nr_seq_processo = nr_processo_p
	and	obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
	and	obter_se_conferencia_barras(a.cd_material, obter_setor_atendimento(obter_atendimento_prescr(b.nr_prescricao))) = 'S'
	and 	nvl(a.ie_situacao, 'A') <> 'I'
	and	nvl(ie_param_70_p, 'N') = 'S'
	group by obter_codigo_barras_processo(a.nr_seq_etiqueta, a.cd_material, 'F'),
		substr(obter_desc_expressao(346632)|| obter_Concat_comp_sol(a.nr_prescricao, a.nr_seq_solucao),1,100),
		a.dt_suspensao,
		b.nr_seq_lote_fornec
	union
	select	to_char(a.cd_material) cd,
		substr(obter_desc_material(a.cd_material),1,100) ds,
		1 qt, 1 sl, '' fn,
		b.nr_seq_lote_fornec nr_seq_lf,
		a.dt_suspensao, a.nr_sequencia nr_seq_horario
	from 	prescr_material b,
		prescr_mat_hor a
	where 	b.nr_prescricao = a.nr_prescricao
	and	b.nr_sequencia = a.nr_seq_material
	and	((a.nr_seq_etiqueta is null) or (ie_param_lf_p in ('C', 'X')))
	and	b.dt_suspensao is null
	and	a.dt_suspensao is null
	and	nvl(b.ie_regra_disp,'X') = 'N'
	and	a.nr_seq_processo = nr_processo_p
	and	obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
	and	obter_se_conferencia_barras(a.cd_material, obter_setor_atendimento(obter_atendimento_prescr(b.nr_prescricao))) = 'S'
	and	nvl(a.ie_situacao,'A') <> 'I'
	and	nvl(b.ie_medicacao_paciente,'N') = 'S'
	and	nvl(ie_param_70_p, 'N') = 'N'
	group by a.cd_material, a.dt_suspensao, b.nr_seq_lote_fornec, a.nr_sequencia
	union
	select	to_char(a.cd_material) cd,
		substr(obter_desc_material(a.cd_material),1,100) ds,
		sum(nvl(decode(a.nr_seq_solucao, null, qt_horario, decode(a.cd_unidade_medida, a.cd_unidade_medida_dose, qt_horario, ceil(qt_horario))),0)) qt,
		sum(nvl(decode(a.nr_seq_solucao, null, qt_horario, decode(a.cd_unidade_medida, a.cd_unidade_medida_dose, qt_horario, ceil(qt_horario))),0)) sl,
		'' fn,
		b.nr_seq_lote_fornec nr_seq_lf,
		a.dt_suspensao dt_suspensao,
		a.nr_sequencia nr_seq_horario
	from    prescr_material b,
		prescr_mat_hor a
	where	b.nr_prescricao = a.nr_prescricao
	and 	b.nr_sequencia = a.nr_seq_material
	and 	((a.nr_seq_etiqueta is null) or (ie_param_lf_p in ('C', 'X')))
	and 	b.dt_suspensao is null
	and 	a.dt_suspensao is null
	and 	nvl(b.ie_regra_disp,'X') <> 'N'
	and 	a.nr_seq_processo = nr_processo_p
	and 	obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
	and	obter_se_conferencia_barras(a.cd_material, obter_setor_atendimento(obter_atendimento_prescr(b.nr_prescricao))) = 'S'
	and	nvl(a.ie_situacao,'A') <> 'I'
	and	nvl(b.ie_medicacao_paciente,'N') = 'N'
	and	nvl(ie_param_70_p, 'N') = 'S'
	group by a.cd_material,a.dt_suspensao,b.nr_seq_lote_fornec,a.nr_sequencia,substr(obter_desc_material(a.cd_material),1,100)
	order by ds;

begin

open C01;
loop
fetch C01 into	
	cd_w,
	ds_w,
	qt_w,
	sl_w,
	fn_w,
	nr_seq_lf_w,
	dt_suspensao_w,
	nr_seq_horario_w;
exit when C01%notfound;
	begin
	t_objeto_row_w.cd	:= cd_w;
	t_objeto_row_w.ds	:= ds_w;
	t_objeto_row_w.qt	:= qt_w;
	t_objeto_row_w.sl	:= sl_w;
	t_objeto_row_w.fn	:= fn_w;
	t_objeto_row_w.nr_seq_lf	:= nr_seq_lf_w;
	t_objeto_row_w.dt_suspensao	:= dt_suspensao_w;
	t_objeto_row_w.nr_seq_horario	:= nr_seq_horario_w;
	pipe row (t_objeto_row_w);
	end;
end loop;
close C01;

return;

end obter_lista;

end pharm_item_pck;
/
