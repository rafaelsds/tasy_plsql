create or replace 
package cpoe_mat_json_pck  as
   	
	function get_mat_message(nr_cpoe_mat_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json;
	
	function get_message_clob(nr_cpoe_mat_p		number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob;

  function get_new_or_replace_message(nr_cpoe_mat_p		number, nr_seq_prescr_changes_p	number) return clob;
	
	procedure getCpoeIntegracaoMat(nr_seq_mat_cpoe_p number, nr_entity_identifier_p out number);

end cpoe_mat_json_pck;
/

create or replace 
package body cpoe_mat_json_pck as
		
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			varchar2) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;
	
	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			number) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;

	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			date) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,pkg_date_utils.get_isoformat(value_p));
	end if;	
	end add_json_value;

  function escapeHL7(hl7_message_p varchar2) return varchar2 is

  escape_char_w                 varchar2(1) := '\';
  field_separator_char_w        varchar2(1) := '|';
  repetition_separator_char_w   varchar2(1) := '~';
  component_separator_char_w    varchar2(1) := '^';
  line_feed_w                   varchar2(1) := chr(10);
  carriage_return_w             varchar2(1) := chr(13);

  ds_escaped_msg_w  varchar2(4000);

  begin

  ds_escaped_msg_w := replace(hl7_message_p, escape_char_w, '\E\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, field_separator_char_w, '\F\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, repetition_separator_char_w, '\R\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, component_separator_char_w, '\S\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, line_feed_w, '\.br\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, carriage_return_w, '\.br\');

  return ds_escaped_msg_w;

  end escapeHL7;
	
	function get_default_message(	nr_cpoe_mat_p	number ) return philips_json is
	
	json_encounter_w		philips_json;
	json_patient_w			philips_json;
	cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
	json_return_w			philips_json;
	json_allergy_w   		philips_json_list;
	
	nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
	
	begin
	
	json_return_w	:= philips_json();
	
	select max(nr_atendimento)
	into nr_atendimento_w
	from CPOE_MATERIAL
	where nr_sequencia = nr_cpoe_mat_p;
	
	SELECT MAX(CD_PESSOA_FISICA)
	INTO CD_PESSOA_FISICA_W
	FROM ATENDIMENTO_PACIENTE
	WHERE NR_ATENDIMENTO = nr_atendimento_w;
	
	json_encounter_w		:= encounter_json_pck.get_encounter(nr_atendimento_w);
	json_patient_w			:= person_json_pck.get_person(cd_pessoa_fisica_w);
	json_allergy_w			:= person_json_pck.get_allergy(cd_pessoa_fisica_w);
	
	json_return_w.put('patientIdentification',json_patient_w.to_json_value);
	json_return_w.put('patientVisit',json_encounter_w.to_json_value);
	json_return_w.put('patientAllergy',json_allergy_w.to_json_value);
	
	return json_return_w;
	
	end get_default_message;	
	

  function get_prescr_changes(nr_seq_prescr_changes_p	number) return philips_json_list is
  json_item_w		philips_json;
	json_item_list_w	philips_json_list;

	Cursor C01 is
		select 
        dt_atualizacao,
        nm_usuario,
        dt_alteracao,
        ie_alteracao,
        ds_justificativa,
        dt_horario,
        qt_dose_adm,
        cd_um_dose_adm,
        obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', cd_um_dose_adm, 'S')  cd_um_dose_adm_code_system,
        obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', cd_um_dose_adm, 'E')  cd_um_dose_adm_code_value, 
        qt_dose_original,
        ds_observacao,
        nr_seq_motivo_susp,
        ds_observacao_item
    from prescr_mat_alteracao a
    where NR_SEQUENCIA = nr_seq_prescr_changes_p;

	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();

    add_json_value(json_item_w, 'dtAtualizacao', r_c01.DT_ATUALIZACAO);
    add_json_value(json_item_w, 'nmUsuario', r_c01.NM_USUARIO);
    add_json_value(json_item_w, 'dtAlteracao', r_c01.DT_ALTERACAO);
    add_json_value(json_item_w, 'ieAlteracao', r_c01.IE_ALTERACAO);
    add_json_value(json_item_w, 'dsJustificativa', r_c01.DS_JUSTIFICATIVA);
    add_json_value(json_item_w, 'dtHorario', r_c01.DT_HORARIO);
    add_json_value(json_item_w, 'qtDoseAdm', r_c01.QT_DOSE_ADM);
    add_json_value(json_item_w, 'cdUmDoseAdm', r_c01.CD_UM_DOSE_ADM);
    add_json_value(json_item_w, 'cdUmDoseAdmCodeSystem', r_c01.CD_UM_DOSE_ADM_CODE_SYSTEM);
    add_json_value(json_item_w, 'cdUmDoseAdmCodeValue', r_c01.CD_UM_DOSE_ADM_CODE_VALUE); 
    add_json_value(json_item_w, 'qtDoseOriginal', r_c01.QT_DOSE_ORIGINAL);
    add_json_value(json_item_w, 'dsObservacao', r_c01.DS_OBSERVACAO);
    add_json_value(json_item_w, 'nrSeqMotivoSusp', r_c01.NR_SEQ_MOTIVO_SUSP);
    add_json_value(json_item_w, 'dsObservacaoItem', r_c01.DS_OBSERVACAO_ITEM);
		
		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;

  end get_prescr_changes;


	function get_material(nr_cpoe_mat_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
  json_component_item_list_w	philips_json_list;	
	json_component_item_w		philips_json;

	Cursor C01 is
		select NR_SEQUENCIA,
          IE_TIPO_SOLUCAO, 
          IE_EVENTO_UNICO,
          IE_DURACAO, 
          IE_URGENCIA, 
          IE_TIPO_DOSAGEM,
          IE_BOMBA_INFUSAO, 
          IE_VIA_APLICACAO, 
          DT_LIBERACAO      requested_date_time, 
          DT_LIB_SUSPENSAO  suspended_time, 
          DT_INICIO, 
          DT_FIM, 
          DT_ATUALIZACAO,
          CD_MATERIAL,
          NR_ETAPAS,
          QT_DOSE_TERAPEUTICA,
          QT_DOSE,
          DS_DOSE_DIFERENCIADA,
          QT_DOSE_RANGE_MIN,
          QT_DOSE_RANGE_MAX,
          QT_SOLUCAO,
          QT_SOLUCAO_TOTAL,
          QT_HORA_FASE,
          QT_DOSAGEM_DIFERENCIADA,
          QT_DOSE_ATAQUE,
          IE_TIPO_DOSAGEM_ATAQUE,
          QT_MIN_FASE_ATAQUE,
          QT_DOSAGEM_ATAQUE,
          QT_DOSAGEM_MIN,
          QT_DOSAGEM_MAX,
          IE_APLIC_BOLUS,
          IE_APLIC_LENTA,
          DS_SOLUCAO,
          HR_MIN_APLICACAO,
          DS_HORARIOS,
          DS_REF_CALCULO,
          ie_controle_tempo,
          ie_administracao,
          adep_obter_desc_info_material(nr_sequencia, decode(ie_controle_tempo, 'N', 'M', 'S', 'SOL', null)) desc_info_item,
          --PCA
          IE_TIPO_ANALGESIA, 
          Obter_Descricao_Dominio(2715,IE_TIPO_ANALGESIA) IE_TIPO_ANALGESIA_text,
          obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_ANALGESIA', IE_TIPO_ANALGESIA, 'S')  IE_TIPO_ANALGESIA_code_system,
          obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_ANALGESIA', IE_TIPO_ANALGESIA, 'E')  IE_TIPO_ANALGESIA_code_value, 
          IE_PCA_MODO_PROG, 
          Obter_Descricao_Dominio(2714,IE_PCA_MODO_PROG) IE_PCA_MODO_PROG_text,
          obter_conv_envio('CPOE_MATERIAL', 'IE_PCA_MODO_PROG', IE_PCA_MODO_PROG, 'S')  IE_PCA_MODO_PROG_code_system,
          obter_conv_envio('CPOE_MATERIAL', 'IE_PCA_MODO_PROG', IE_PCA_MODO_PROG, 'E')  IE_PCA_MODO_PROG_code_value, 
          QT_VOL_INFUSAO_PCA,
          obter_desc_unidade_medida(IE_UM_FLUXO_PCA)  IE_UM_FLUXO_PCA_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_FLUXO_PCA, 'S')  IE_UM_FLUXO_PCA_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_FLUXO_PCA, 'E')  IE_UM_FLUXO_PCA_code_value, 
          QT_BOLUS_PCA,
          obter_desc_unidade_medida(IE_UM_BOLUS_PCA)  IE_UM_BOLUS_PCA_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_BOLUS_PCA, 'S')  IE_UM_BOLUS_PCA_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_BOLUS_PCA, 'E')  IE_UM_BOLUS_PCA_code_value, 
          QT_INTERVALO_BLOQUEIO,
          QT_DOSE_INICIAL_PCA,
          obter_desc_unidade_medida(IE_UM_DOSE_INICIO_PCA)  IE_UM_DOSE_INI_PCA_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_DOSE_INICIO_PCA, 'S')  IE_UM_DOSE_INI_PCA_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_DOSE_INICIO_PCA, 'E')  IE_UM_DOSE_INI_PCA_code_value, 
          QT_FINAL_CONCENTRATION,
          obter_desc_unidade_medida(IE_UM_FINAL_CONC_PCA)  IE_UM_FIN_CONC_PCA_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_FINAL_CONC_PCA, 'S')  IE_UM_FIN_CONC_PCA_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_FINAL_CONC_PCA, 'E')  IE_UM_FIN_CONC_PCA_code_value, 
          QT_LIMITE_QUATRO_HORA,
          obter_desc_unidade_medida(IE_UM_LIMITE_PCA)  IE_UM_LIMITE_PCA_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_LIMITE_PCA, 'S')  IE_UM_LIMITE_PCA_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_LIMITE_PCA, 'E')  IE_UM_LIMITE_PCA_code_value, 
          QT_LIMITE_UMA_HORA,
          obter_desc_unidade_medida(IE_UM_LIMITE_HORA_PCA)  IE_UM_LIM_HORA_PCA_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_LIMITE_HORA_PCA, 'S')  IE_UM_LIM_HORA_PCA_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', IE_UM_LIMITE_HORA_PCA, 'E')  IE_UM_LIM_HORA_code_value, 
          ------------
          cpoe_obter_dt_suspensao(nr_sequencia,'N') dt_suspensao,
          substr(OBTER_DESC_material(CD_MATERIAL),1,200) desc_material, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MATERIAL, 'E')    code_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MATERIAL, 'S')    code_system, 
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNIDADE_MEDIDA, 'S')  unidade_medida_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNIDADE_MEDIDA, 'E')  unidade_medida_code_value, 
          obter_desc_unidade_medida(CD_UNIDADE_MEDIDA)  unidade_medida_text,
          obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', IE_VIA_APLICACAO, 'S')  via_code_system,
          obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', IE_VIA_APLICACAO, 'E')  via_code_vale, 
          Obter_Desc_via(IE_VIA_APLICACAO)   desc_via,
          CD_INTERVALO,
          obter_conv_envio('INTERVALO_PRESCRICAO', 'CD_INTERVALO', CD_INTERVALO, 'E')  interval_code_vale, 
          QT_DOSAGEM,
          obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', IE_TIPO_DOSAGEM, 'S')  rate_unit_code_system,
          obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', IE_TIPO_DOSAGEM, 'E') rate_unit_code_vale,
          Obter_Descricao_Dominio(93,IE_TIPO_DOSAGEM) rate_unit_text,
          obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO, 'S')  infusion_pump_code_system,
          obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO, 'E')  infusion_pump_code_vale,
          Obter_Descricao_Dominio(1537,IE_BOMBA_INFUSAO) infusion_pump_text,
          obter_conv_envio('CPOE_MATERIAL', 'IE_LADO', IE_LADO, 'S')  site_code_system,
          obter_conv_envio('CPOE_MATERIAL', 'IE_LADO', IE_LADO, 'E')  site_code_value,
          Obter_Descricao_Dominio(1372,IE_LADO) desc_site_text,
          obter_minutos_hora(HR_MIN_APLICACAO)  HR_MIN_APLICACAO_min_hora,
          obter_minutos_hora(QT_HORA_FASE)  HR_MIN_HORA_FASE,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'S')  minute_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'E')  minute_code_value,
        -- Componente 1
          QT_DOSE_COMP1, 
          DS_DOSE_DIFERENCIADA_COMP1,
          substr(OBTER_DESC_material(CD_MAT_COMP1),1,200) desc_material_comp1, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP1, 'E')    code_comp1_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP1, 'S')    code_comp1_system, 
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_COMP1)  unidade_medida_comp1_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP1, 'S')  unid_med_comp1_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP1, 'E')  unid_med_comp1_code_value, 
          -- Componente 2
          QT_DOSE_COMP2, 
          DS_DOSE_DIFERENCIADA_COMP2,
          substr(OBTER_DESC_material(CD_MAT_COMP2),1,200) desc_material_comp2, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP2, 'E')    code_comp2_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP2, 'S')    code_comp2_system, 
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_COMP2)  unidade_medida_comp2_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP2, 'S')  unid_med_comp2_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP2, 'E')  unid_med_comp2_code_value, 	   
          -- Componente 3
          QT_DOSE_COMP3, 
          DS_DOSE_DIFERENCIADA_COMP3,
          substr(OBTER_DESC_material(CD_MAT_COMP3),1,200) desc_material_comp3, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP3, 'E')    code_comp3_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP3, 'S')    code_comp3_system, 
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_COMP3)  unidade_medida_comp3_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP3, 'S')  unid_med_comp3_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP3, 'E')  unid_med_comp3_code_value, 
        -- Componente 4
          QT_DOSE_COMP4, 
          DS_DOSE_DIFERENCIADA_COMP4,
          substr(OBTER_DESC_material(CD_MAT_COMP4),1,200) desc_material_comp4, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP4, 'E')    code_comp4_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP4, 'S')    code_comp4_system, 
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_COMP4)  unidade_medida_comp4_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP4, 'S')  unid_med_comp4_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP4, 'E')  unid_med_comp4_code_value, 
        -- Componente 5
          QT_DOSE_COMP5, 
          DS_DOSE_DIFERENCIADA_COMP5,
          substr(OBTER_DESC_material(CD_MAT_COMP5),1,200) desc_material_comp5, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP5, 'E')    code_comp5_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP5, 'S')    code_comp5_system,
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_COMP5)  unidade_medida_comp5_text, 
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP5, 'S')  unid_med_comp5_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP5, 'E')  unid_med_comp5_code_value, 
        -- Componente 6
          QT_DOSE_COMP6, 
          DS_DOSE_DIFERENCIADA_COMP6,
          substr(OBTER_DESC_material(CD_MAT_COMP6),1,200) desc_material_comp6, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP6, 'E')    code_comp6_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP6, 'S')    code_comp6_system, 
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_COMP6)  unidade_medida_comp6_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP6, 'S')  unid_med_comp6_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP6, 'E')  unid_med_comp6_code_value, 
        -- Componente 7
          QT_DOSE_COMP7, 
          DS_DOSE_DIFERENCIADA_COMP7,
          substr(OBTER_DESC_material(CD_MAT_COMP7),1,200) desc_material_comp7, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP7, 'E')    code_comp7_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_COMP7, 'S')    code_comp7_system, 
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_COMP7)  unidade_medida_comp7_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP7, 'S')  unid_med_comp7_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_COMP7, 'E')  unid_med_comp7_code_value, 
          -- Diluente
          CD_MAT_DIL,
          QT_DOSE_DIL,
          substr(OBTER_DESC_material(CD_MAT_DIL),1,200) desc_diluente, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_DIL, 'E')    dil_code_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_DIL, 'S')    dil_code_system, 
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_DIL)  unid_dil_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_DIL, 'S')  unid_dil_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_DIL, 'E')  unid_dil_code_value,
          --Rediluente
          QT_DOSE_RED,
          substr(OBTER_DESC_material(CD_MAT_RED),1,200) desc_rediluente, 
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_RED, 'E')    red_code_value,
          obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_RED, 'S')    red_code_system, 
          obter_desc_unidade_medida(CD_UNID_MED_DOSE_RED)  unidade_medida_red_text,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_RED, 'S')  unid_red_code_system,
          obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_RED, 'E')  unid_red_code_value,   
          decode(DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(DS_OBSERVACAO)) DS_OBSERVACAO, 
          decode(DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA,       
          cd_medico ordering_provider_id_number,
          obter_dados_pf(cd_medico,'PNG') ordering_provider_given_name,
          obter_dados_pf(cd_medico,'PNL') ordering_provider_last_name,
          obter_dados_pf(cd_medico,'PNM') ordering_provider_middle_name, 
          Obter_Pessoa_Fisica_Usuario(nm_usuario_susp,'C') ordering_prov_susp_id_number,
          substr(obter_nome_pf(cd_medico),1,80) nm_medico_solicitante,
          Obter_Pessoa_Fisica_Usuario(nm_usuario_nrec,'C') ordering_user_id_number,
          Obter_Pessoa_Fisica_Usuario(nm_usuario_susp,'C') order_prov_susp_id_number
    from CPOE_MATERIAL a
    where NR_SEQUENCIA = nr_cpoe_mat_p;

	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();

    add_json_value(json_item_w, 'nrSequencia', r_c01.NR_SEQUENCIA);
    add_json_value(json_item_w, 'ieTipoSolucao', r_c01.IE_TIPO_SOLUCAO);
    add_json_value(json_item_w, 'ieEventoUnico', r_c01.IE_EVENTO_UNICO);
    add_json_value(json_item_w, 'ieDuracao', r_c01.IE_DURACAO);
    add_json_value(json_item_w, 'ieUrgencia', r_c01.IE_URGENCIA);
    add_json_value(json_item_w, 'ieTipoDosagem', r_c01.IE_TIPO_DOSAGEM);
    add_json_value(json_item_w, 'ieBombaInfusao', r_c01.IE_BOMBA_INFUSAO);
    add_json_value(json_item_w, 'ieViaAplicacao', r_c01.IE_VIA_APLICACAO);
    add_json_value(json_item_w, 'requestedDateTime', r_c01.requested_date_time);
    add_json_value(json_item_w, 'suspendedTime', r_c01.suspended_time);
    add_json_value(json_item_w, 'dtInicio', r_c01.DT_INICIO);
    add_json_value(json_item_w, 'dtFim', r_c01.DT_FIM);
    add_json_value(json_item_w, 'dtAtualizacao', r_c01.DT_ATUALIZACAO);
    add_json_value(json_item_w, 'cdMaterial', r_c01.CD_MATERIAL);
    add_json_value(json_item_w, 'nrEtapas', r_c01.NR_ETAPAS);
    add_json_value(json_item_w, 'qtDoseTerapeutica', r_c01.QT_DOSE_TERAPEUTICA);
    add_json_value(json_item_w, 'qtDose', r_c01.QT_DOSE);
    add_json_value(json_item_w, 'dsDoseDiferenciada', r_c01.DS_DOSE_DIFERENCIADA);
    add_json_value(json_item_w, 'qtDoseRangeMin', r_c01.QT_DOSE_RANGE_MIN);
    add_json_value(json_item_w, 'qtDoseRangeMax', r_c01.QT_DOSE_RANGE_MAX);
    add_json_value(json_item_w, 'qtSolucao', r_c01.QT_SOLUCAO);
    add_json_value(json_item_w, 'qtSolucaoTotal', r_c01.QT_SOLUCAO_TOTAL);
    add_json_value(json_item_w, 'qtHoraFase', r_c01.QT_HORA_FASE);
    add_json_value(json_item_w, 'qtDosagemDiferenciada', r_c01.QT_DOSAGEM_DIFERENCIADA);
    add_json_value(json_item_w, 'qtDoseAtaque', r_c01.QT_DOSE_ATAQUE);
    add_json_value(json_item_w, 'ieTipoDosagemAtaque', r_c01.IE_TIPO_DOSAGEM_ATAQUE);
    add_json_value(json_item_w, 'qtMinFaseAtaque', r_c01.QT_MIN_FASE_ATAQUE);
    add_json_value(json_item_w, 'qtDosagemAtaque', r_c01.QT_DOSAGEM_ATAQUE);
    add_json_value(json_item_w, 'qtDosagemMin', r_c01.QT_DOSAGEM_MIN);
    add_json_value(json_item_w, 'qtDosagemMax', r_c01.QT_DOSAGEM_MAX);
    add_json_value(json_item_w, 'ieAplicBolus', r_c01.IE_APLIC_BOLUS);
    add_json_value(json_item_w, 'ieAplicLenta', r_c01.IE_APLIC_LENTA);
    add_json_value(json_item_w, 'dsSolucao', r_c01.DS_SOLUCAO);
    add_json_value(json_item_w, 'hrMinAplicacao', r_c01.HR_MIN_APLICACAO);
    add_json_value(json_item_w, 'dsHorarios', r_c01.DS_HORARIOS);
    --PCA
    add_json_value(json_item_w, 'ieTipoAnalgesia', r_c01.IE_TIPO_ANALGESIA);
    add_json_value(json_item_w, 'ieTipoAnalgesia_text', r_c01.IE_TIPO_ANALGESIA_text);
    add_json_value(json_item_w, 'ieTipoAnalgesiaCode_system', r_c01.IE_TIPO_ANALGESIA_code_system);
    add_json_value(json_item_w, 'ieTipoAnalgesiaCode_value', r_c01.IE_TIPO_ANALGESIA_code_value);
    add_json_value(json_item_w, 'iePcaModoProg', r_c01.IE_PCA_MODO_PROG);
    add_json_value(json_item_w, 'iePcaModoProgText', r_c01.IE_PCA_MODO_PROG_text);
    add_json_value(json_item_w, 'iePcaModoProgCodeSystem', r_c01.IE_PCA_MODO_PROG_code_system);
    add_json_value(json_item_w, 'iePcaModoProgCodeValue', r_c01.IE_PCA_MODO_PROG_code_value);
    add_json_value(json_item_w, 'qtVolInfusaoPca', r_c01.QT_VOL_INFUSAO_PCA);
    add_json_value(json_item_w, 'ieUmFluxoPcaCodeSystem', r_c01.IE_UM_FLUXO_PCA_code_system);
    add_json_value(json_item_w, 'ieUmFluxoPcaCodeValue', r_c01.IE_UM_FLUXO_PCA_code_value);
    add_json_value(json_item_w, 'qtBolusPca', r_c01.QT_BOLUS_PCA);
    add_json_value(json_item_w, 'ieUmBolusPcaCodeSystem', r_c01.IE_UM_BOLUS_PCA_code_system);
    add_json_value(json_item_w, 'ieUmBolusPcaCodeValue', r_c01.IE_UM_BOLUS_PCA_code_value);
    add_json_value(json_item_w, 'qtIntervaloBloqueio', r_c01.QT_INTERVALO_BLOQUEIO);
    add_json_value(json_item_w, 'qtDoseInicialPca', r_c01.QT_DOSE_INICIAL_PCA);
    add_json_value(json_item_w, 'ieUmDoseIniPcaCodeSystem', r_c01.IE_UM_DOSE_INI_PCA_code_system);
    add_json_value(json_item_w, 'ieUmDoseIniPcaCodeValue', r_c01.IE_UM_DOSE_INI_PCA_code_value);
    add_json_value(json_item_w, 'qtFinalConcentration', r_c01.QT_FINAL_CONCENTRATION);
    add_json_value(json_item_w, 'ieUmFinConcPcaCodeSystem', r_c01.IE_UM_FIN_CONC_PCA_code_system);
    add_json_value(json_item_w, 'ieUmFinConcPcaCodeValue', r_c01.IE_UM_FIN_CONC_PCA_code_value);
    add_json_value(json_item_w, 'qt_limite_quatroHora', r_c01.QT_LIMITE_QUATRO_HORA);
    add_json_value(json_item_w, 'ieUmLimitePcaCodeSystem', r_c01.IE_UM_LIMITE_PCA_code_system);
    add_json_value(json_item_w, 'ieUmLimitePcaCodeValue', r_c01.IE_UM_LIMITE_PCA_code_value);
    add_json_value(json_item_w, 'qt_limite_umaHora', r_c01.QT_LIMITE_UMA_HORA);
    add_json_value(json_item_w, 'ieUmLimHoraPcaCode_system', r_c01.IE_UM_LIM_HORA_PCA_code_system);
    add_json_value(json_item_w, 'ieUmLimHoraCodeValue', r_c01.IE_UM_LIM_HORA_code_value);
    ------------
    add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);
    add_json_value(json_item_w, 'descMaterial', r_c01.desc_material);
    add_json_value(json_item_w, 'codeValue', r_c01.code_value);
    add_json_value(json_item_w, 'codeSystem', r_c01.code_system);
    add_json_value(json_item_w, 'unidadeMedidaCodeSystem', r_c01.unidade_medida_code_system);
    add_json_value(json_item_w, 'unidadeMedidaCodeValue', r_c01.unidade_medida_code_value);
    add_json_value(json_item_w, 'unidadeMedidaText', r_c01.unidade_medida_text);
    add_json_value(json_item_w, 'viaCodeSystem', r_c01.via_code_system);
    add_json_value(json_item_w, 'viaCodeVale', r_c01.via_code_vale);
    add_json_value(json_item_w, 'descVia', r_c01.desc_via);
    add_json_value(json_item_w, 'cdIntervalo', r_c01.CD_INTERVALO);
    add_json_value(json_item_w, 'intervalCodeVale', r_c01.interval_code_vale);
    add_json_value(json_item_w, 'qtDosagem', r_c01.QT_DOSAGEM);
    add_json_value(json_item_w, 'rateUnitCodeSystem', r_c01.rate_unit_code_system);
    add_json_value(json_item_w, 'rateUnitCodeVale', r_c01.rate_unit_code_vale);
    add_json_value(json_item_w, 'rateUnitText', r_c01.rate_unit_text);
    add_json_value(json_item_w, 'infusionPumpCodeSystem', r_c01.infusion_pump_code_system);
    add_json_value(json_item_w, 'infusionPumpCodeVale', r_c01.infusion_pump_code_vale);
    add_json_value(json_item_w, 'infusionPumpRext', r_c01.infusion_pump_text);
    add_json_value(json_item_w, 'hrMinAplicacaoMinHora', r_c01.HR_MIN_APLICACAO_min_hora);
    add_json_value(json_item_w, 'minuteCodeSystem', r_c01.minute_code_system);
    add_json_value(json_item_w, 'minuteCodeValue', r_c01.minute_code_value);
    
    -- Componente 1
    add_json_value(json_item_w, 'qtDoseComp1', r_c01.QT_DOSE_COMP1);
    add_json_value(json_item_w, 'dsDoseDiferenciadaComp1', r_c01.DS_DOSE_DIFERENCIADA_COMP1);
    add_json_value(json_item_w, 'descMaterialComp1', r_c01.desc_material_comp1);
    add_json_value(json_item_w, 'codeComp1Value', r_c01.code_comp1_value);
    add_json_value(json_item_w, 'codeComp1System', r_c01.code_comp1_system);
    add_json_value(json_item_w, 'unidadeMedidaComp1Text', r_c01.unidade_medida_comp1_text);
    add_json_value(json_item_w, 'unidMedComp1CodeSystem', r_c01.unid_med_comp1_code_system);
    add_json_value(json_item_w, 'unidMedComp1CodeValue', r_c01.unid_med_comp1_code_value);
    -- Componente 2
    add_json_value(json_item_w, 'qtDoseComp2', r_c01.QT_DOSE_COMP2);
    add_json_value(json_item_w, 'dsDoseDiferenciadaComp2', r_c01.DS_DOSE_DIFERENCIADA_COMP2);
    add_json_value(json_item_w, 'descMaterialComp2', r_c01.desc_material_comp2);
    add_json_value(json_item_w, 'codeComp2Value', r_c01.code_comp2_value);
    add_json_value(json_item_w, 'codeComp2System', r_c01.code_comp2_system);
    add_json_value(json_item_w, 'unidadeMedidaComp2Text', r_c01.unidade_medida_comp2_text);
    add_json_value(json_item_w, 'unidMedComp2CodeSystem', r_c01.unid_med_comp2_code_system);
    add_json_value(json_item_w, 'unidMedComp2CodeValue', r_c01.unid_med_comp2_code_value);
    -- Componente 3
    add_json_value(json_item_w, 'qtDoseComp3', r_c01.QT_DOSE_COMP3);
    add_json_value(json_item_w, 'dsDoseDiferenciadaComp3', r_c01.DS_DOSE_DIFERENCIADA_COMP3);
    add_json_value(json_item_w, 'descMaterialComp3', r_c01.desc_material_comp3);
    add_json_value(json_item_w, 'codeComp3Value', r_c01.code_comp3_value);
    add_json_value(json_item_w, 'codeComp3System', r_c01.code_comp3_system);
    add_json_value(json_item_w, 'unidadeMedidaComp3Text', r_c01.unidade_medida_comp3_text);
    add_json_value(json_item_w, 'unidMedComp3CodeSystem', r_c01.unid_med_comp3_code_system);
    add_json_value(json_item_w, 'unidMedComp3CodeValue', r_c01.unid_med_comp3_code_value);
    -- Componente 4
    add_json_value(json_item_w, 'qtDoseComp4', r_c01.QT_DOSE_COMP4);
    add_json_value(json_item_w, 'dsDoseDiferenciadaComp4', r_c01.DS_DOSE_DIFERENCIADA_COMP4);
    add_json_value(json_item_w, 'descMaterialComp4', r_c01.desc_material_comp4);
    add_json_value(json_item_w, 'codeComp4Value', r_c01.code_comp4_value);
    add_json_value(json_item_w, 'codeComp4System', r_c01.code_comp4_system);
    add_json_value(json_item_w, 'unidadeMedidaComp4Text', r_c01.unidade_medida_comp4_text);
    add_json_value(json_item_w, 'unidMedComp4CodeSystem', r_c01.unid_med_comp4_code_system);
    add_json_value(json_item_w, 'unidMedComp4CodeValue', r_c01.unid_med_comp4_code_value);
    -- Componente 5
    add_json_value(json_item_w, 'qtDoseComp5', r_c01.QT_DOSE_COMP5);
    add_json_value(json_item_w, 'dsDoseDiferenciadaComp5', r_c01.DS_DOSE_DIFERENCIADA_COMP5);
    add_json_value(json_item_w, 'descMaterialComp5', r_c01.desc_material_comp5);
    add_json_value(json_item_w, 'codeComp5Value', r_c01.code_comp5_value);
    add_json_value(json_item_w, 'codeComp5System', r_c01.code_comp5_system);
    add_json_value(json_item_w, 'unidMedComp5CodeSystem', r_c01.unid_med_comp5_code_system);
    add_json_value(json_item_w, 'unidMedComp5CodeValue', r_c01.unid_med_comp5_code_value);
    -- Componente 6
    add_json_value(json_item_w, 'qtDoseComp6', r_c01.QT_DOSE_COMP6);
    add_json_value(json_item_w, 'dsDoseDiferenciadaComp6', r_c01.DS_DOSE_DIFERENCIADA_COMP6);
    add_json_value(json_item_w, 'descMaterialComp6', r_c01.desc_material_comp6);
    add_json_value(json_item_w, 'codeComp6Value', r_c01.code_comp6_value);
    add_json_value(json_item_w, 'codeComp6System', r_c01.code_comp6_system);
    add_json_value(json_item_w, 'unidadeMedidaComp6Text', r_c01.unidade_medida_comp6_text);
    add_json_value(json_item_w, 'unidMedComp6CodeSystem', r_c01.unid_med_comp6_code_system);
    add_json_value(json_item_w, 'unidMedComp6CodeValue', r_c01.unid_med_comp6_code_value);
    -- Componente 7
    add_json_value(json_item_w, 'qtDoseComp7', r_c01.QT_DOSE_COMP7);
    add_json_value(json_item_w, 'dsDoseDiferenciadaComp7', r_c01.DS_DOSE_DIFERENCIADA_COMP7);
    add_json_value(json_item_w, 'descMaterialComp7', r_c01.desc_material_comp7);
    add_json_value(json_item_w, 'codeComp7Value', r_c01.code_comp7_value);
    add_json_value(json_item_w, 'codeComp7System', r_c01.code_comp7_system);
    add_json_value(json_item_w, 'unidadeMedidaComp7Text', r_c01.unidade_medida_comp7_text);
    add_json_value(json_item_w, 'unidMedComp7CodeSystem', r_c01.unid_med_comp7_code_system);
    add_json_value(json_item_w, 'unidMedComp7CodeValue', r_c01.unid_med_comp7_code_value);
    -- Diluente
    add_json_value(json_item_w, 'cdMatDil', r_c01.CD_MAT_DIL);
    add_json_value(json_item_w, 'qtDoseDil', r_c01.QT_DOSE_DIL);
    add_json_value(json_item_w, 'descDiluente', r_c01.desc_diluente);
    add_json_value(json_item_w, 'dilCodeValue', r_c01.dil_code_value);
    add_json_value(json_item_w, 'dilCodeSystem', r_c01.dil_code_system);
    add_json_value(json_item_w, 'unidadeMedidaDilText', r_c01.unid_dil_text);
    add_json_value(json_item_w, 'unidMedDilCodeSystem', r_c01.unid_dil_code_system);
    add_json_value(json_item_w, 'unidMedDilCodeValue', r_c01.unid_dil_code_value);
    --Rediluente
    add_json_value(json_item_w, 'qtDoseRed', r_c01.QT_DOSE_RED);
    add_json_value(json_item_w, 'descRediluente', r_c01.desc_rediluente);
    add_json_value(json_item_w, 'redCodeValue', r_c01.red_code_value);
    add_json_value(json_item_w, 'redCodeSystem', r_c01.red_code_system);
    add_json_value(json_item_w, 'unidadeMedidaRedText', r_c01.unidade_medida_red_text);
    add_json_value(json_item_w, 'unidRedCodeSystem', r_c01.unid_red_code_system);
    add_json_value(json_item_w, 'unidRedCodeValue', r_c01.unid_red_code_value);
    add_json_value(json_item_w, 'dsObservacao', r_c01.DS_OBSERVACAO);
    add_json_value(json_item_w, 'dsJustificativa', r_c01.DS_JUSTIFICATIVA);
    add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ordering_provider_id_number);
    add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
    add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
    add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
    add_json_value(json_item_w, 'orderingProvSuspIdNumber', r_c01.ordering_prov_susp_id_number);
    add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
    add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);
    add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
    add_json_value(json_item_w, 'descInfoItem', escapeHL7(r_c01.desc_info_item));
    add_json_value(json_item_w, 'dsRefCalculo', escapeHL7(r_c01.DS_REF_CALCULO));

		if (r_c01.DT_FIM is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.dt_fim));
		end if;
		
    if (r_c01.ie_administracao in ('N', 'C')) then
      add_json_value(json_item_w, 'priority', 'PRN');
    elsif (r_c01.ie_urgencia = 0) then
      add_json_value(json_item_w, 'priority', 'S');
    elsif (r_c01.ie_urgencia = 5) then
      add_json_value(json_item_w, 'priority', 'TM5');
    elsif (r_c01.ie_urgencia = 10) then
      add_json_value(json_item_w, 'priority', 'TM10');
    elsif (r_c01.ie_urgencia = 15) then
      add_json_value(json_item_w, 'priority', 'TM15');
    else
      add_json_value(json_item_w, 'priority', 'R');
    end if;

    if (r_c01.HR_MIN_APLICACAO_min_hora is not null 
        and r_c01.ie_controle_tempo = 'N') then
      add_json_value(json_item_w, 'occurrenceDurationId', r_c01.HR_MIN_APLICACAO_min_hora || r_c01.hr_min_aplicacao);
    elsif (r_c01.ie_tipo_solucao = 'I' 
        and r_c01.ie_controle_tempo = 'S'
        and r_c01.qt_hora_fase is not null) then
      add_json_value(json_item_w, 'occurrenceDurationId', r_c01.HR_MIN_APLICACAO_min_hora || r_c01.HR_MIN_HORA_FASE);
    end if;

    if ((r_c01.HR_MIN_APLICACAO_min_hora is not null 
        and r_c01.ie_controle_tempo = 'N')
          or (r_c01.ie_tipo_solucao = 'I' and r_c01.ie_controle_tempo = 'S' and r_c01.qt_hora_fase is not null)) then
      add_json_value(json_item_w, 'occurrenceDurationCodeSystem', r_c01.minute_code_system);
    end if;


		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.suspended_time, r_c01.requested_date_time));
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		

    json_component_item_list_w	:= philips_json_list();	

    /* Diluente */
    if (r_c01.qt_dose_dil is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'B');
			add_json_value(json_component_item_w, 'codeValue', r_c01.dil_code_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_diluente);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.dil_code_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_dil);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_dil_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unid_dil_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_dil_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;

    /* Re-Diluente */
    if (r_c01.qt_dose_red is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.red_code_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_rediluente);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.red_code_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_red);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_red_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_red_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_red_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;


    /* Components */
    if (r_c01.qt_dose_comp1 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.code_comp1_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material_comp1);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.code_comp1_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_comp1);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_med_comp1_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_comp1_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_med_comp1_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;

    if (r_c01.qt_dose_comp2 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.code_comp2_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material_comp2);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.code_comp2_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_comp2);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_med_comp2_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_comp2_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_med_comp2_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;

    if (r_c01.qt_dose_comp3 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.code_comp3_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material_comp3);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.code_comp3_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_comp3);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_med_comp3_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_comp3_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_med_comp3_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;

    if (r_c01.qt_dose_comp4 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.code_comp4_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material_comp4);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.code_comp4_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_comp4);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_med_comp4_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_comp4_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_med_comp4_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;

    if (r_c01.qt_dose_comp5 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.code_comp5_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material_comp5);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.code_comp5_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_comp5);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_med_comp5_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_comp5_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_med_comp5_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;

    if (r_c01.qt_dose_comp6 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.code_comp6_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material_comp6);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.code_comp6_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_comp6);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_med_comp6_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_comp6_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_med_comp6_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;

    if (r_c01.qt_dose_comp7 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.code_comp7_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material_comp7);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.code_comp7_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_comp7);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_med_comp7_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_comp7_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_med_comp7_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;
		
		json_item_w.put('componentList', json_component_item_list_w.to_json_value());

		
		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_material;

	
	function get_mat_message(nr_cpoe_mat_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json is
	json_return_w		philips_json;
	json_item_list_w   	philips_json_list;
	ie_hl7_msg_type_w	varchar(3);
	begin
	
	json_item_list_w	:= get_material(nr_cpoe_mat_p);
	ie_hl7_msg_type_w := 'OMP';
	
	if	(json_item_list_w.count > 0) then
		json_return_w	:= philips_json();
		json_return_w		:= get_default_message(nr_cpoe_mat_p);
		add_json_value(json_return_w, 'orderControl', ie_order_control_p);
		add_json_value(json_return_w, 'entityidentifier', nr_entity_identifier_p);
		add_json_value(json_return_w, 'hl7MsgType', ie_hl7_msg_type_w);
		
		json_return_w.put('medList', json_item_list_w.to_json_value());
	end if;
	
	return json_return_w;
	
	end get_mat_message;
					

	function get_message_clob(nr_cpoe_mat_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob is
	ds_json_out_w		clob;
	json_mat_w	philips_json;
	
	begin
		
	json_mat_w		:= get_mat_message(nr_cpoe_mat_p, ie_order_control_p, nr_entity_identifier_p);
	
	if	(json_mat_w is null) then
		return null;
	end if;
	
	dbms_lob.createtemporary( ds_json_out_w, true);
	json_mat_w.to_clob(ds_json_out_w);
	
	return ds_json_out_w;
	end get_message_clob;


  function existsCpoeIntegracaoMat(nr_seq_mat_cpoe_p number) return boolean is
  nr_entity_identifier_w number;

  begin

	select max(nr_sequencia)
	into nr_entity_identifier_w
	from cpoe_integracao
	where nr_seq_mat_cpoe = nr_seq_mat_cpoe_p;

  if (nr_entity_identifier_w is null) then
    return false;
  else
    return true;
  end if;
	end existsCpoeIntegracaoMat;


  /* To be used when you dont know if the message should be NW or XO */
  function get_new_or_replace_message(nr_cpoe_mat_p	number, nr_seq_prescr_changes_p	number) return clob is
	ds_json_out_w		clob;
	json_mat_w	philips_json;
  json_item_list_w   	philips_json_list;

  ie_order_control_w varchar2(2);
  nr_entity_identifier_w number;
	
	begin

  if (cpoe_mat_json_pck.existsCpoeIntegracaoMat(nr_cpoe_mat_p)) then
    ie_order_control_w := 'XO';
  else
    ie_order_control_w := 'NW';
  end if;

  cpoe_mat_json_pck.getCpoeIntegracaoMat(nr_cpoe_mat_p, nr_entity_identifier_w);
		
	json_mat_w		:= get_mat_message(nr_cpoe_mat_p, ie_order_control_w, nr_entity_identifier_w);
	
	if	(json_mat_w is null) then
		return null;
	end if;

  json_item_list_w	:= get_prescr_changes(nr_seq_prescr_changes_p);
  json_mat_w.put('changeList', json_item_list_w.to_json_value());
	
	dbms_lob.createtemporary( ds_json_out_w, true);
	json_mat_w.to_clob(ds_json_out_w);
	
	return ds_json_out_w;
	end get_new_or_replace_message;


	procedure getCpoeIntegracaoMat(nr_seq_mat_cpoe_p number, nr_entity_identifier_p out number) is
  pragma autonomous_transaction;
	begin

	select max(nr_sequencia)
	into nr_entity_identifier_p
	from cpoe_integracao
	where nr_seq_mat_cpoe = nr_seq_mat_cpoe_p;
	
	if (nr_entity_identifier_p is null) then
		select CPOE_INTEGRACAO_SEQ.nextval
		into nr_entity_identifier_p
		from dual;

		insert into cpoe_integracao (
			nr_sequencia,
			nr_seq_mat_cpoe
			)
		values (
			nr_entity_identifier_p,
			nr_seq_mat_cpoe_p
		);
		commit;
	end if;	

	end getCpoeIntegracaoMat;	

end cpoe_mat_json_pck;
/
