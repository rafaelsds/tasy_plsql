create or replace 
package search_chart_ehro_pck as


type t_objeto_row is record (
	code_group			varchar2(15),
	ie_union 				number(10),
	name				varchar2(255),
	comercial_name			varchar2(255),
	ie_medic_nao_inform		varchar2(15),
	description			varchar2(255),
	color				varchar2(15),
	style 				varchar2(30),
	shape 				varchar2(30),
	qt_size 				number(5),
	fill_color 				varchar2(15),
	shaded 				varchar2(15),
	fixed_scale 			number(2),
	init_date 				date,
	end_date 			date,
	code_chart 			number(10),
	seq_apresentacao 			number(10),
	nr_cirurgia 			number(10),
	nr_seq_pepo 			number(10),
	nm_usuario 			varchar2(15),
	nm_tabela			varchar2(50),
	nm_atributo 			varchar2(50),
	ie_sinal_vital 			varchar2(15),
	nr_seq_superior 			number(10),
	end_of_administration 		varchar2(15),
	cd_material 			number(6),
	cd_unid_medida_adm 		varchar2(30),
	ie_modo_adm 			varchar2(15),
	nr_seq_agente 			number(10),
	ds_dosagem 			varchar2(15),
	ie_tipo				varchar2(15)
);

type t_objeto_row_data is table of t_objeto_row;

function get_data (	nr_cirurgia_p		number,
		nr_seq_pepo_p		number,
		code_group_p		varchar) return t_objeto_row_data pipelined;
					
end search_chart_ehro_pck;
/


create or replace PACKAGE BODY search_chart_ehro_pck AS

FUNCTION get_data (	nr_cirurgia_p		number,
			nr_seq_pepo_p		number,
			code_group_p		varchar) RETURN t_objeto_row_data pipelined IS
							
t_objeto_row_w		t_objeto_row;

cursor	vital_signs is
	select	distinct /* Necessary because of Blood pressure fields that will repeat the same vital sign */
		1 ie_union, 
		substr(obter_desc_expressao(e.cd_exp_grafico,null), 0, 255) name,
		substr((select max(ds_unid_med_sv) from pepo_sv r where r.ie_sinal_vital = c.ie_sinal_vital), 0, 255) description,
		substr(nvl(e.ds_cor_grafico_html,'#E5BAFF'), 0, 15) color,	
		substr(nvl(e.ie_estilo_grafico_html,'points'), 0, 30) style,
		substr(e.ie_desenho_grafico, 0, 30) shape,
		e.qt_tamanho_grafico qt_size,
		substr(decode(e.ie_transparente,'S','transparent',nvl(e.ds_cor_grafico_html,'#E5BAFF')), 0, 15) fill_color, /* White */
		obter_data_grafico_pepo('I',d.nr_cirurgia) init_date,
		obter_data_grafico_pepo('F',d.nr_cirurgia) end_date,
		d.nr_sequencia code_chart,
		d.nr_seq_apresentacao seq_apresentacao,
		d.nr_cirurgia nr_cirurgia,
		d.nr_seq_pepo nr_seq_pepo,
		substr(d.nm_usuario, 0, 15) nm_usuario,
		substr(c.ie_sinal_vital, 0, 15) ie_sinal_vital
	from	pepo_config_chart_html e,
		pepo_item_grafico d,
		pepo_sinal_vital c
	where	c.ie_sinal_vital		= e.ie_sinal_vital
	and		d.nr_seq_sinal_vital 	= c.nr_sequencia
	and 	nvl(c.ie_situacao,'A') 	= 'A'
	and		d.ie_grafico 			= 'S'
	and		c.ie_sinal_vital NOT IN ('QTSEGMENTOST','QTNITROSO')
	and		(((nvl(nr_cirurgia_p,0) > 0) and (d.nr_cirurgia = nr_cirurgia_p)) or
			((nvl(nr_seq_pepo_p,0) > 0) and (d.nr_seq_pepo = nr_seq_pepo_p)));

	
cursor	anesthesic_agents is
	/* Anesthesic Agents - Medications - Hidroeletric Teraphy  */
	select 	distinct
		2 ie_union, 
		substr(c.ds_agente, 0, 255) name,
		substr(obter_desc_material(cd_material), 0, 255) comercial_name,
		substr(c.ie_medic_nao_inform, 0, 15) ie_medic_nao_inform,
		substr(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,''), 0, 255) description,
		substr('#008C23', 0, 15) color,	
		substr(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'style'), 0, 30) style,
		substr('circle', 0, 30) shape,
		to_number(get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'qt_size')) qt_size,
		substr('#008C23', 0, 15) fill_color, 
		substr('true', 0, 15) shaded,
		0 fixed_scale,
		obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
		obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,	  
		a.nr_sequencia CODE_CHART, 		
		a.nr_seq_apresentacao seq_apresentacao,
		a.nr_cirurgia nr_cirurgia,
		a.nr_seq_pepo nr_seq_pepo,
		substr(a.nm_usuario, 0, 15) nm_usuario,
		substr('CIRURGIA_AGENTE_ANESTESICO', 0, 50) nm_tabela,
		substr('DS_AGENTE' || '_' || get_type_chart_surgery(d.nr_sequencia,b.nr_sequencia,'style'), 0, 50) nm_atributo,
		b.nr_sequencia nr_seq_superior,
		substr(obter_se_fim_administracao(b.nr_sequencia), 0, 15) end_of_administration,
		b.cd_material,
		substr(b.cd_unid_medida_adm, 0, 30) cd_unid_medida_adm,
		substr(obter_modo_medicamento_cirur(b.nr_sequencia), 0, 15) ie_modo_adm,
		c.nr_sequencia nr_seq_agente,
		substr(b.ie_tipo_dosagem, 0, 15) ds_dosagem,
		b.ie_tipo
	from 	pepo_item_grafico a, 
		cirurgia_agente_anest_ocor d,
		cirurgia_agente_anestesico b, 
		agente_anestesico c
	where 	a.nr_seq_cirur_agente = b.nr_sequencia
	and 	b.nr_seq_agente = c.nr_sequencia
	and 	b.nr_sequencia = d.nr_seq_cirur_agente(+)
	and	b.ie_tipo in (1,2,3)
	and	a.ie_grafico = 'S' 
	and 	nvl(b.ie_situacao,'A') = 'A'
	and	nvl(d.ie_situacao,'A') = 'A'
	and	(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
		 ((nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)));
	
cursor	blood_components is
	select	distinct
		3 ie_union,
		substr(c.ds_derivado, 0, 255) name,
		substr(obter_volume_hemocomponente(NR_SEQ_DERIVADO,'D'),1,255) description,
		substr('#A81923', 0, 15) color,	
		substr('line', 0, 30) style,
		2 qt_size,
		substr('#FFF', 0, 15) fill_color, /* White */
		substr('true', 0, 15) shaded,
		0 fixed_scale,
		obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
		obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,
		a.nr_sequencia CODE_CHART, 
		a.nr_seq_apresentacao seq_apresentacao,
		b.nr_cirurgia nr_cirurgia,
		b.nr_seq_pepo nr_seq_pepo,
		substr(a.nm_usuario, 0, 15) nm_usuario,
		substr('CIRURGIA_AGENTE_ANESTESICO', 0, 50) nm_tabela,
		substr('DS_AGENTE_HEMO', 0, 50) nm_atributo,
		b.nr_sequencia nr_seq_superior,
		substr(obter_se_fim_administracao(b.nr_sequencia), 0, 15) end_of_administration,
		b.cd_material,
		substr(b.cd_unid_medida_adm, 0, 30) cd_unid_medida_adm,
		substr('H', 0, 15) ie_modo_adm
	from 	pepo_item_grafico a, 
		cirurgia_agente_anestesico b, 
		san_derivado c 
	where 	a.nr_seq_cirur_agente = b.nr_sequencia
	and 	c.nr_sequencia = b.nr_seq_derivado(+)
	and 	nvl(b.ie_situacao,'A') = 'A'
	and	a.ie_grafico = 'S'
	and	(((nvl(nr_cirurgia_p,0) > 0) and (b.nr_cirurgia = nr_cirurgia_p)) or
		 ((nvl(nr_seq_pepo_p,0) > 0) and (b.nr_seq_pepo = nr_seq_pepo_p)));
	
/* Hidro balance - Wins and losses */
cursor	hidro_balance is
	select 	distinct
		4 ie_union,
		substr(c.ds_tipo, 0, 255) name,
		substr('ml', 0, 255) description,
		substr('#3064FF', 0, 15) color,	
		substr('points', 0, 30) style,
		10 qt_size,
		substr(decode(obter_tipo_perda_ganho(b.nr_seq_tipo,'C'),'G','#3064FF','P','transparent'), 0, 15) fill_color, /* White */
		substr('true', 0, 15) shaded,
		0 fixed_scale,
		obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
		obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,
		a.nr_sequencia CODE_CHART, 
		nvl(a.nr_seq_apresentacao,0) seq_apresentacao,
		b.nr_cirurgia nr_cirurgia,
		b.nr_seq_pepo nr_seq_pepo,
		substr(a.nm_usuario, 0, 15) nm_usuario,
		substr('ATENDIMENTO_PERDA_GANHO', 0, 50) nm_tabela,
		substr('DS_TIPO', 0, 50) nm_atributo,
		b.nr_seq_tipo nr_seq_superior
	from 	pepo_item_grafico a, 
		tipo_perda_ganho c,
		atendimento_perda_ganho b
	where 	b.nr_seq_tipo = c.nr_sequencia
	and 	b.nr_sequencia = a.nr_seq_atend_pg(+)
	and	c.ie_exibir_grafico = 'S'
	and 	nvl(b.ie_situacao,'A') = 'A'
	and	nvl(a.ie_grafico,'S') = 'S'
	and	(((nvl(nr_cirurgia_p,0) > 0) and (b.nr_cirurgia = nr_cirurgia_p)) or
		 ((nvl(nr_seq_pepo_p,0) > 0) and (b.nr_seq_pepo = nr_seq_pepo_p)))
	union all
	/* Hidro balance - Hidro balance */
	select 	distinct
		5 ie_union,
		substr(obter_desc_expressao(284223,null), 0, 255) name,
		substr('ml', 0, 255) description,
		substr('#3064FF', 0, 15) color,	
		substr('line', 0, 30) style,
		3 qt_size,
		null fill_color, /* White */
		substr('true', 0, 15) shaded,
		0 fixed_scale,
		obter_data_grafico_pepo('I',a.nr_cirurgia) init_date,
		obter_data_grafico_pepo('F',a.nr_cirurgia) end_date,
		a.nr_sequencia CODE_CHART, 
		nvl(a.nr_seq_apresentacao,999) seq_apresentacao,
		b.nr_cirurgia nr_cirurgia,
		b.nr_seq_pepo nr_seq_pepo,
		substr(a.nm_usuario, 0, 15) nm_usuario,
		substr('ATENDIMENTO_PERDA_GANHO', 0, 50) nm_tabela,
		substr('BALANCO', 0, 50) nm_atributo,
		null nr_seq_superior
	from 	pepo_item_grafico a, 
		tipo_perda_ganho c,
		atendimento_perda_ganho b
	where 	b.nr_seq_tipo = c.nr_sequencia
	and 	b.nr_sequencia = a.nr_seq_atend_pg(+)
	and	c.ie_exibir_grafico = 'S'
	and 	nvl(b.ie_situacao,'A') = 'A'
	and	nvl(a.ie_grafico,'S') = 'S'
	and	(((nvl(nr_cirurgia_p,0) > 0) and (b.nr_cirurgia = nr_cirurgia_p)) or
		 ((nvl(nr_seq_pepo_p,0) > 0) and (b.nr_seq_pepo = nr_seq_pepo_p)));
	
cursor	events_graph is
	select	91 ie_union,
		substr(obter_desc_expressao(295156,null), 0, 255) name,
		substr(obter_desc_expressao(295156,null), 0, 255) description,
		substr('#8EC200', 0, 15) color,	
		substr('line', 0, 30) style,
		substr('#FFF', 0, 15) fill_color,
		10 fixed_scale,
		obter_data_etapa_evento(nr_cirurgia_p,nr_seq_pepo_p,1) init_date,
		obter_data_etapa_evento(nr_cirurgia_p,nr_seq_pepo_p,6) end_date,
		-1 CODE_CHART, 
		1 seq_apresentacao,
		nr_cirurgia_p nr_cirurgia,
		nr_seq_pepo_p nr_seq_pepo,
		substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
		substr('DS_PACIENTE', 0, 50) nm_atributo
	from	cirurgia a
	where	(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
		 ((nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)))
	/* Events graph - Anesthesia */
	union
	select	92 ie_union,
		substr(obter_desc_expressao(283468,null), 0, 255) name,
		substr(obter_desc_expressao(283468,null), 0, 255) description,
		substr('#FACD52', 0, 15) color,	
		substr('line', 0, 30) style,
		substr('#FFF', 0, 15) fill_color, /* White */
		10 fixed_scale,
		obter_data_etapa_evento(nr_cirurgia_p,nr_seq_pepo_p,2) init_date,
		obter_data_etapa_evento(nr_cirurgia_p,nr_seq_pepo_p,5) end_date,
		-2 CODE_CHART, 
		2 seq_apresentacao,
		nr_cirurgia_p nr_cirurgia,
		nr_seq_pepo_p nr_seq_pepo,
		substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
		substr('DS_ANESTESIA', 0, 50) nm_atributo
	from	cirurgia a
	where	(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
		 ((nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)))
	union
	/* Events graph - Procedure */
	select	93 ie_union,
		substr(obter_exame_agenda(cd_procedimento_princ, ie_origem_proced, nr_seq_proc_interno),1,40) name,
		substr(obter_exame_agenda(cd_procedimento_princ, ie_origem_proced, nr_seq_proc_interno),1,40) description,
		substr('#F7727B', 0, 15) color,	
		substr('line', 0, 30) style,
		substr('#FFF', 0, 15) fill_color, /* White */
		10 fixed_scale,
		obter_data_etapa_evento(nr_cirurgia,nr_seq_pepo,3) init_date,
		obter_data_etapa_evento(nr_cirurgia,nr_seq_pepo,4) end_date,
		-3 CODE_CHART, 
		3 seq_apresentacao,
		a.nr_cirurgia,
		a.nr_seq_pepo,
		substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
		substr('DS_PROCEDIMENTO', 0, 50) nm_atributo
	from	cirurgia a
	where	(((nvl(nr_cirurgia_p,0) > 0) and (a.nr_cirurgia = nr_cirurgia_p)) or
		 ((nvl(nr_seq_pepo_p,0) > 0) and (a.nr_seq_pepo = nr_seq_pepo_p)))
	union
	--Fanep
	select 	91 ie_union,
		substr(obter_desc_expressao(295156,null), 0, 255) name,
		substr(obter_desc_expressao(295156,null), 0, 255) description,
		substr('#8EC200', 0, 15) color,	
		substr('line', 0, 30) style,
		substr('#FFF', 0, 15) fill_color, /* White */
		10 fixed_scale,
		obter_data_etapa_evento(null,nr_sequencia,1) init_date,
		obter_data_etapa_evento(null,nr_sequencia,6) end_date,
		-1 CODE_CHART, 
		1 seq_apresentacao,
		null nr_cirurgia,
		a.nr_sequencia nr_seq_pepo,
		substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
		substr('DS_PACIENTE', 0, 50) nm_atributo
	from	pepo_cirurgia a
	where	not exists (select 1 from cirurgia b where b.nr_seq_pepo = a.nr_sequencia)
	and	a.nr_sequencia = nr_seq_pepo_p
	/* Events graph - Anesthesia */
	union
	--Fanep
	select 	92 ie_union,
		substr(obter_desc_expressao(283468,null), 0, 255) name,
		substr(obter_desc_expressao(283468,null), 0, 255) description,
		substr('#FACD52', 0, 15) color,	
		substr('line', 0, 30) style,
		substr('#FFF', 0, 15) fill_color, /* White */
		10 fixed_scale,
		obter_data_etapa_evento(null,nr_sequencia,2) init_date,
		obter_data_etapa_evento(null,nr_sequencia,5) end_date,
		-2 CODE_CHART, 
		2 seq_apresentacao,
		null nr_cirurgia,
		a.nr_sequencia nr_seq_pepo,
		substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
		substr('DS_ANESTESIA', 0, 50) nm_atributo
	from	pepo_cirurgia a
	where	not exists (select 1 from cirurgia b where b.nr_seq_pepo = a.nr_sequencia)
	and	a.nr_sequencia = nr_seq_pepo_p
	union
	/* Events graph - Procedure */
	--Fanep
	select 	93 ie_union,
		substr(obter_desc_expressao(720319,null), 0, 255) name,
		substr(obter_desc_expressao(720319,null), 0, 255) description,
		substr('#F7727B', 0, 15) color,	
		substr('line', 0, 30) style,
		substr('#FFF', 0, 15) fill_color, /* White */
		10 fixed_scale,
		obter_data_etapa_evento(null,nr_sequencia,3) init_date,
		obter_data_etapa_evento(null,nr_sequencia,4) end_date,
		-3 CODE_CHART, 
		3 seq_apresentacao,
		null nr_cirurgia,
		nr_sequencia nr_seq_pepo,
		substr('EVENTO_CIRURGIA_PACIENTE', 0, 50) nm_tabela,
		substr('DS_PROCEDIMENTO', 0, 50) nm_atributo
	from	pepo_cirurgia a
	where	not exists (select 1 from cirurgia b where b.nr_seq_pepo = a.nr_sequencia)
	and	a.nr_sequencia = nr_seq_pepo_p;
	
begin
if	(code_group_p = 'SV') then
	FOR r_c01 IN vital_signs LOOP
		BEGIN
		t_objeto_row_w.code_group			:= 	code_group_p;
		t_objeto_row_w.ie_union			:= 	r_c01.ie_union;
		t_objeto_row_w.name			:=	r_c01.name;
		t_objeto_row_w.comercial_name		:=	null;
		t_objeto_row_w.ie_medic_nao_inform		:=	null;
		t_objeto_row_w.description			:=	r_c01.description;
		t_objeto_row_w.color			:=	r_c01.color;
		t_objeto_row_w.style			:=	r_c01.style;
		t_objeto_row_w.shape			:=	r_c01.shape;
		t_objeto_row_w.qt_size			:=	r_c01.qt_size;
		t_objeto_row_w.fill_color			:=	r_c01.fill_color;
		t_objeto_row_w.shaded			:=	null;
		t_objeto_row_w.fixed_scale			:=	null;
		t_objeto_row_w.init_date			:= 	r_c01.init_date;
		t_objeto_row_w.end_date			:=	r_c01.end_date;
		t_objeto_row_w.code_chart			:=	r_c01.code_chart;
		t_objeto_row_w.seq_apresentacao		:=	r_c01.seq_apresentacao;
		t_objeto_row_w.nr_cirurgia			:=	r_c01.nr_cirurgia;
		t_objeto_row_w.nr_seq_pepo			:=	r_c01.nr_seq_pepo;
		t_objeto_row_w.nm_usuario			:= 	r_c01.nm_usuario;
		t_objeto_row_w.nm_tabela			:= 	null;
		t_objeto_row_w.nm_atributo			:=	null;
		t_objeto_row_w.ie_sinal_vital			:=	r_c01.ie_sinal_vital;
		t_objeto_row_w.nr_seq_superior		:=	null;
		t_objeto_row_w.end_of_administration		:=	null;
		t_objeto_row_w.cd_material			:=	null;
		t_objeto_row_w.cd_unid_medida_adm		:=	null;	
		t_objeto_row_w.ie_modo_adm		:=	null;
		t_objeto_row_w.nr_seq_agente		:=	null;
		t_objeto_row_w.ds_dosagem			:=	null;
		t_objeto_row_w.ie_tipo			:=	null;
		pipe ROW(t_objeto_row_w);
		END;
	END LOOP;
elsif (code_group_p = 'AG') then
	FOR r_c02 IN anesthesic_agents LOOP
		BEGIN
		t_objeto_row_w.code_group			:=	code_group_p;
		t_objeto_row_w.ie_union			:= 	r_c02.ie_union;
		t_objeto_row_w.name			:=	r_c02.name;
		t_objeto_row_w.comercial_name		:=	r_c02.comercial_name;
		t_objeto_row_w.ie_medic_nao_inform		:=	r_c02.ie_medic_nao_inform;
		t_objeto_row_w.description			:=	r_c02.description;
		t_objeto_row_w.color			:=	r_c02.color;
		t_objeto_row_w.style			:=	r_c02.style;
		t_objeto_row_w.shape			:=	r_c02.shape;
		t_objeto_row_w.qt_size			:=	r_c02.qt_size;
		t_objeto_row_w.fill_color			:=	r_c02.fill_color;
		t_objeto_row_w.shaded			:=	r_c02.shaded;
		t_objeto_row_w.fixed_scale			:=	r_c02.fixed_scale;
		t_objeto_row_w.init_date			:= 	r_c02.init_date;
		t_objeto_row_w.end_date			:=	r_c02.end_date;
		t_objeto_row_w.code_chart			:=	r_c02.code_chart;
		t_objeto_row_w.seq_apresentacao		:=	r_c02.seq_apresentacao;
		t_objeto_row_w.nr_cirurgia			:=	r_c02.nr_cirurgia;
		t_objeto_row_w.nr_seq_pepo			:=	r_c02.nr_seq_pepo;
		t_objeto_row_w.nm_usuario			:= 	r_c02.nm_usuario;
		t_objeto_row_w.nm_tabela			:= 	r_c02.nm_tabela;
		t_objeto_row_w.nm_atributo			:=	r_c02.nm_atributo;
		t_objeto_row_w.ie_sinal_vital			:=	null;
		t_objeto_row_w.nr_seq_superior		:=	r_c02.nr_seq_superior;
		t_objeto_row_w.end_of_administration		:=	r_c02.end_of_administration;
		t_objeto_row_w.cd_material			:=	r_c02.cd_material;
		t_objeto_row_w.cd_unid_medida_adm		:=	r_c02.cd_unid_medida_adm;	
		t_objeto_row_w.ie_modo_adm		:=	r_c02.ie_modo_adm;
		t_objeto_row_w.nr_seq_agente		:=	r_c02.nr_seq_agente;
		t_objeto_row_w.ds_dosagem			:=	r_c02.ds_dosagem;
		t_objeto_row_w.ie_tipo			:=	r_c02.ie_tipo;
		pipe ROW(t_objeto_row_w);
		END;
	END LOOP;
elsif (code_group_p = 'SH') then
	FOR r_c03 IN blood_components LOOP
		BEGIN
		t_objeto_row_w.code_group			:= 	code_group_p;
		t_objeto_row_w.ie_union			:=	r_c03.ie_union;
		t_objeto_row_w.name			:=	r_c03.name;
		t_objeto_row_w.comercial_name		:=	null;
		t_objeto_row_w.ie_medic_nao_inform		:=	null;
		t_objeto_row_w.description			:=	r_c03.description;
		t_objeto_row_w.color			:=	r_c03.color;
		t_objeto_row_w.style			:=	r_c03.style;
		t_objeto_row_w.shape			:=	null;
		t_objeto_row_w.qt_size			:=	r_c03.qt_size;
		t_objeto_row_w.fill_color			:=	r_c03.fill_color;
		t_objeto_row_w.shaded			:=	r_c03.shaded;
		t_objeto_row_w.fixed_scale			:=	r_c03.fixed_scale;
		t_objeto_row_w.init_date			:= 	r_c03.init_date;
		t_objeto_row_w.end_date			:=	r_c03.end_date;
		t_objeto_row_w.code_chart			:=	r_c03.code_chart;
		t_objeto_row_w.seq_apresentacao		:=	r_c03.seq_apresentacao;
		t_objeto_row_w.nr_cirurgia			:=	r_c03.nr_cirurgia;
		t_objeto_row_w.nr_seq_pepo			:=	r_c03.nr_seq_pepo;
		t_objeto_row_w.nm_usuario			:= 	r_c03.nm_usuario;
		t_objeto_row_w.nm_tabela			:= 	r_c03.nm_tabela;
		t_objeto_row_w.nm_atributo			:=	r_c03.nm_atributo;
		t_objeto_row_w.ie_sinal_vital			:=	null;
		t_objeto_row_w.nr_seq_superior		:=	r_c03.nr_seq_superior;
		t_objeto_row_w.end_of_administration		:=	r_c03.end_of_administration;
		t_objeto_row_w.cd_material			:=	r_c03.cd_material;
		t_objeto_row_w.cd_unid_medida_adm		:=	r_c03.cd_unid_medida_adm;	
		t_objeto_row_w.ie_modo_adm		:=	r_c03.ie_modo_adm;
		t_objeto_row_w.nr_seq_agente		:=	null;
		t_objeto_row_w.ds_dosagem			:=	null;
		t_objeto_row_w.ie_tipo			:=	null;
		pipe ROW(t_objeto_row_w);
		END;
	END LOOP;
elsif (code_group_p = 'BH') then
	FOR r_c04 IN hidro_balance LOOP
		BEGIN
		t_objeto_row_w.code_group			:= 	code_group_p;
		t_objeto_row_w.ie_union			:=	r_c04.ie_union;
		t_objeto_row_w.name			:=	r_c04.name;
		t_objeto_row_w.comercial_name		:=	null;
		t_objeto_row_w.ie_medic_nao_inform		:=	null;
		t_objeto_row_w.description			:=	r_c04.description;
		t_objeto_row_w.color			:=	r_c04.color;
		t_objeto_row_w.style			:=	r_c04.style;
		t_objeto_row_w.shape			:=	null;
		t_objeto_row_w.qt_size			:=	r_c04.qt_size;
		t_objeto_row_w.fill_color			:=	r_c04.fill_color;
		t_objeto_row_w.shaded			:=	r_c04.shaded;
		t_objeto_row_w.fixed_scale			:=	r_c04.fixed_scale;
		t_objeto_row_w.init_date			:= 	r_c04.init_date;
		t_objeto_row_w.end_date			:=	r_c04.end_date;
		t_objeto_row_w.code_chart			:=	r_c04.code_chart;
		t_objeto_row_w.seq_apresentacao		:=	r_c04.seq_apresentacao;
		t_objeto_row_w.nr_cirurgia			:=	r_c04.nr_cirurgia;
		t_objeto_row_w.nr_seq_pepo			:=	r_c04.nr_seq_pepo;
		t_objeto_row_w.nm_usuario			:= 	r_c04.nm_usuario;
		t_objeto_row_w.nm_tabela			:= 	r_c04.nm_tabela;
		t_objeto_row_w.nm_atributo			:=	r_c04.nm_atributo;
		t_objeto_row_w.ie_sinal_vital			:=	null;
		t_objeto_row_w.nr_seq_superior		:=	r_c04.nr_seq_superior;
		t_objeto_row_w.end_of_administration		:=	null;
		t_objeto_row_w.cd_material			:=	null;
		t_objeto_row_w.cd_unid_medida_adm		:=	null;	
		t_objeto_row_w.ie_modo_adm		:=	null;
		t_objeto_row_w.nr_seq_agente		:=	null;
		t_objeto_row_w.ds_dosagem			:=	null;
		t_objeto_row_w.ie_tipo			:=	null;
		pipe ROW(t_objeto_row_w);
		END;
	END LOOP;
elsif (code_group_p = 'NAV') then
	FOR r_c05 IN events_graph LOOP
		BEGIN
		t_objeto_row_w.code_group			:= 	code_group_p;
		t_objeto_row_w.ie_union			:= 	r_c05.ie_union;
		t_objeto_row_w.name			:=	r_c05.name;
		t_objeto_row_w.comercial_name		:=	null;
		t_objeto_row_w.ie_medic_nao_inform		:=	null;
		t_objeto_row_w.description			:=	r_c05.description;
		t_objeto_row_w.color			:=	r_c05.color;
		t_objeto_row_w.style			:=	r_c05.style;
		t_objeto_row_w.shape			:=	null;
		t_objeto_row_w.qt_size			:=	null;
		t_objeto_row_w.fill_color			:=	r_c05.fill_color;
		t_objeto_row_w.shaded			:=	null;
		t_objeto_row_w.fixed_scale			:=	r_c05.fixed_scale;
		t_objeto_row_w.init_date			:= 	r_c05.init_date;
		t_objeto_row_w.end_date			:=	r_c05.end_date;
		t_objeto_row_w.code_chart			:=	r_c05.code_chart;
		t_objeto_row_w.seq_apresentacao		:=	r_c05.seq_apresentacao;
		t_objeto_row_w.nr_cirurgia			:=	r_c05.nr_cirurgia;
		t_objeto_row_w.nr_seq_pepo			:=	r_c05.nr_seq_pepo;
		t_objeto_row_w.nm_usuario			:= 	null;
		t_objeto_row_w.nm_tabela			:= 	r_c05.nm_tabela;
		t_objeto_row_w.nm_atributo			:=	r_c05.nm_atributo;
		t_objeto_row_w.ie_sinal_vital			:=	null;
		t_objeto_row_w.nr_seq_superior		:=	null;
		t_objeto_row_w.end_of_administration		:=	null;
		t_objeto_row_w.cd_material			:=	null;
		t_objeto_row_w.cd_unid_medida_adm		:=	null;	
		t_objeto_row_w.ie_modo_adm		:=	null;
		t_objeto_row_w.nr_seq_agente		:=	null;
		t_objeto_row_w.ds_dosagem			:=	null;
		t_objeto_row_w.ie_tipo			:=	null;
		pipe ROW(t_objeto_row_w);
		END;
	END LOOP;
end if;

RETURN;

END get_data;

end search_chart_ehro_pck;
/
