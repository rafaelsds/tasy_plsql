create or replace
package sgh_atualizar_resultados_pck is

    -- rf 04 ? criar function que ir� verificar se existe algum resultado pendente
    function verifica_resultados_pendentes return varchar2;

    -- rf 05 ? criar procedure que ir� inserir os resultados na tabela integradora
    procedure atualizar_registros_resultados( cd_estabelecimento_p in number);

    -- rf 06 ? criar uma procedure para iniciar o processo de integra��o
    procedure iniciar_integracao( cd_estab_p in number);

end sgh_atualizar_resultados_pck;
/
create or replace
package body sgh_atualizar_resultados_pck is

    -- rf 04 ? criar function que ir� verificar se existe algum resultado pendente
    function verifica_resultados_pendentes return varchar2 is

        ds_retorno_w varchar2(1);

	begin
		begin
		    select  distinct	'S'
			into	ds_retorno_w
			from	prescr_procedimento a,
					prescr_medica b
			where	a.nr_prescricao = b.nr_prescricao
			and		a.ie_status_atend >= 35
			and		not exists (select  1
								from    sgh_exame_resultados c
								where   c.nr_seq_exame = nvl(b.nr_controle_lab,a.nr_prescricao));
		exception
		when no_data_found then
			ds_retorno_w := 'N';
		end;

        return(ds_retorno_w);

	end verifica_resultados_pendentes;

	procedure atualizar_registros_resultados (cd_estabelecimento_p in number) is

		nr_requisicao_w	prescr_medica.nr_controle_lab%type;
		nr_seq_exame_c1_w	prescr_procedimento.nr_seq_exame%type;
		nr_seq_exame_c2_w	exame_laboratorio.nr_seq_exame%type;
		nm_exame_w		exame_laboratorio.nm_exame%type;
		ds_observacao_w	exame_lab_result_item.ds_observacao%type;
		cd_material_exame_w	prescr_procedimento.cd_material_exame%type;
		nr_doc_convenio_w	atend_categoria_convenio.nr_doc_convenio%type;
		cd_senha_w	atend_categoria_convenio.cd_senha%type;
		nr_atendimento_w	prescr_medica.nr_atendimento%type;
		ds_referencia_w	exame_lab_result_item.ds_referencia%type;
		nr_seq_unid_med_w	exame_lab_result_item.nr_seq_unid_med%type;
		nr_seq_resultado_w	exame_lab_result_item.nr_seq_resultado%type;
		vl_resultado_w	exame_lab_result_item.ds_resultado%type;
		ds_unidade_medida_w	lab_unidade_medida.ds_unidade_medida%type;
		nr_doc_convenio_c2_w prescr_procedimento.nr_doc_convenio%type;
		cd_senha_c2_w prescr_procedimento.cd_senha%type;

		cursor c1 is
            select	distinct
	                nvl(a.nr_controle_lab, a.nr_prescricao) nr_requisicao,
					b.nr_seq_exame,
					c.nm_exame,
					e.ds_observacao,
					b.cd_material_exame,
					f.nr_doc_convenio,
					f.cd_senha,
					f.nr_atendimento,
					d.nr_seq_resultado
			from	prescr_medica a,
					prescr_procedimento b,
					exame_laboratorio c,
					exame_lab_resultado d,
					exame_lab_result_item e,
					atend_categoria_convenio f
			where	a.nr_prescricao = b.nr_prescricao
			and		a.nr_atendimento = f.nr_atendimento
			and		a.nr_prescricao = d.nr_prescricao
			and		b.nr_seq_exame = c.nr_seq_exame
			and		c.nr_seq_exame = e.nr_seq_exame
			and		d.nr_seq_resultado = e.nr_seq_resultado
			and		b.nr_sequencia = e.nr_seq_prescr
			and		f.nr_seq_interno = ( select nvl(max(nr_seq_interno),0)
										 from	atend_categoria_convenio z
										 where	z.nr_atendimento = a.nr_atendimento
										 and	z.dt_inicio_vigencia = (select	max(x.dt_inicio_vigencia)
																		from	atend_categoria_convenio x
																		where	x.nr_atendimento = a.nr_atendimento))
			and not exists (select	1
							from	sgh_exame_resultados s
							where	s.nr_requisicao = nvl(a.nr_controle_lab, a.nr_prescricao)
							and		s.nr_seq_exame = b.nr_seq_exame)
			and		b.ie_status_atend >= 35
			and		e.nr_seq_material is not null
			and		a.cd_estabelecimento = cd_estabelecimento_p;

		cursor c2 (	nr_requisicao_p prescr_medica.nr_prescricao%type,
					nr_seq_exame_p prescr_procedimento.nr_seq_exame%type,
					cd_material_exame_p prescr_procedimento.cd_material_exame%type) is
			select	distinct c.nr_seq_exame,
					c.nm_exame,
					e.ds_observacao,
					e.ds_referencia,
					b.nr_doc_convenio,
					b.cd_senha,
					e.nr_seq_unid_med,
					e.nr_seq_resultado,
					coalesce(e.ds_resultado, to_char(e.qt_resultado), to_char(e.pr_resultado)) vl_resultado,
					nvl(f.ds_unidade_medida, c.ds_unidade_medida) ds_unidade_medida
			from	prescr_medica a,
					prescr_procedimento b,
					exame_laboratorio     c,
					exame_lab_resultado   d,
					exame_lab_result_item e,
					lab_unidade_medida    f
			where	a.nr_prescricao = b.nr_prescricao
			and		e.nr_seq_exame = c.nr_seq_exame
			and		d.nr_seq_resultado = e.nr_seq_resultado
			and		a.nr_prescricao = d.nr_prescricao
			and		b.nr_sequencia = e.nr_seq_prescr
			and		e.nr_seq_unid_med = f.nr_sequencia(+)
			and		nvl(a.nr_controle_lab, a.nr_prescricao) = nr_requisicao_p
			and		b.nr_seq_exame = nr_seq_exame_p
			and		b.cd_material_exame = cd_material_exame_p
			and		b.ie_status_atend >= 35
			and 	coalesce(e.ds_resultado, to_char(e.qt_resultado), to_char(e.pr_resultado)) is not null
			and		a.cd_estabelecimento = cd_estabelecimento_p;

	begin
		open c1;
		loop
		fetch c1 into
		    nr_requisicao_w,
		    nr_seq_exame_c1_w,
		    nm_exame_w,
		    ds_observacao_w,
    		cd_material_exame_w,
	    	nr_doc_convenio_w,
		    cd_senha_w,
		    nr_atendimento_w,
		    nr_seq_resultado_w;
		exit when c1%notfound;
		    begin
                insert into sgh_exame_resultados (
                        nr_requisicao,
					    nr_seq_exame,
					    nr_atendimento,
                        nm_exame,
                        cd_guia_convenio,
                        cd_senha_convenio,
                        ie_status,
                        ds_referencia,
                        ds_observacao,
						nr_seq_resultado,
                        ds_erro)
                values(
                    nr_requisicao_w,
                    nr_seq_exame_c1_w,
                    nr_atendimento_w,
                    nm_exame_w,
                    null,
                    null,
                    'N',
                    'REFER�NCIA',
                    null,
					nr_seq_resultado_w,
                    null);
		    
		    
		    

			    open c2( nr_requisicao_w,
                    nr_seq_exame_c1_w,
                    cd_material_exame_w);
			    loop
			    fetch c2 into
                    nr_seq_exame_c2_w,
                    nm_exame_w,
                    ds_observacao_w,
                    ds_referencia_w,
                    nr_doc_convenio_c2_w,
                    cd_senha_c2_w,
                    nr_seq_unid_med_w,
                    nr_seq_resultado_w,
                    vl_resultado_w,
                    ds_unidade_medida_w;
			    exit when c2%notfound;
                    begin
		    
		    

			            insert into sgh_resultados (
                                        nm_exame,
										nr_seq_exame,
										nr_seq_superior,
										vl_resultado,
										vl_unid_med,
										ds_referencia,
										cd_guia_convenio,
										cd_senha_convenio,
										ds_observacao,
										nr_seq_resultado)
                            values(
                                nm_exame_w,
                                nr_seq_exame_c2_w,
                                nr_seq_exame_c1_w,
                                vl_resultado_w,
                                ds_unidade_medida_w,
                                ds_referencia_w,
                                null,
                                cd_senha_c2_w,
                                ds_observacao_w,
								nr_seq_resultado_w);
                    end;
		    commit;
                end loop;
		
			    close c2;

			    update	sgh_exame_resultados
			    set		ie_status = 'P'
			    where	nr_requisicao = nr_requisicao_w
			    and		nr_seq_exame = nr_seq_exame_c1_w
			    and	    nr_seq_resultado = nr_seq_resultado_w;
            end;
	 commit;
        end loop;
		close c1;
	end;

    -- rf 06 ? criar uma procedure para iniciar o processo de integra��o
	procedure iniciar_integracao( cd_estab_p number) is

		ds_retorno_w varchar2(1);

	begin
        ds_retorno_w := null;

        ds_retorno_w := verifica_resultados_pendentes;

        if ds_retorno_w = 'S' then
            atualizar_registros_resultados( cd_estab_p);
		end if;
	end;

end sgh_atualizar_resultados_pck;
/
