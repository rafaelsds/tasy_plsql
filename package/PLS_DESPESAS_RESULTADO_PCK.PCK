create or replace 
package pls_despesas_resultado_pck as

type t_valores_despesas_row is record (
	vl_consulta			number(15,2),
	vl_exame			number(15,2),
	vl_terapia			number(15,2),
	vl_internacao			number(15,2),
	vl_outro_atendimento		number(15,2),
	vl_demais_despesas		number(15,2),
	vl_sem_grupo			number(15,2),
	vl_total			number(15,2)
	);

type t_valores_despesas is table of t_valores_despesas_row;

qt_registro_transacao_w		pls_integer := 2000;

function obter_valores_despesas(ie_dimensao_um_p	varchar2,
				ie_dimensao_dois_p	varchar2,
				ds_vl_dimensao_um_p	varchar2,
				ds_vl_dimensao_dois_p	varchar2,
				dt_inicial_p		varchar2,
				dt_final_p		varchar2,
				ds_filtros_p		varchar2,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				ie_tipo_valor_p		varchar2)
				return t_valores_despesas pipelined;

end pls_despesas_resultado_pck;
/

create or replace
package body pls_despesas_resultado_pck as

function obter_valores_despesas(ie_dimensao_um_p	varchar2,
				ie_dimensao_dois_p	varchar2,
				ds_vl_dimensao_um_p	varchar2,
				ds_vl_dimensao_dois_p	varchar2,
				dt_inicial_p		varchar2,
				dt_final_p		varchar2,
				ds_filtros_p		varchar2,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				ie_tipo_valor_p		varchar2)
				return t_valores_despesas pipelined is

sql_base_w			varchar2(32000);
sql_restricao_w			varchar2(32000);
dt_mesano_referencia_w		varchar2(10);
ie_forma_pagamento_w		pls_parametro_pagamento.ie_forma_pagamento%type;
nr_seq_contrato_w		pls_contrato.nr_sequencia%type;

t_valores_despesas_w		t_valores_despesas;

type retorno_valores_despesas is ref cursor;
retorno_valores_despesas_w	retorno_valores_despesas;

dt_leitura_w			date;
dt_mes_referencia_w		date;
dados_restricao_w 		pls_util_pck.dados_select;

begin
if	(nvl(upper(ie_dimensao_um_p), 'X') not in ('ANO', 'MES', 'T', 'S')) and
	(nvl(upper(ie_dimensao_dois_p), 'X') not in ('ANO', 'MES', 'T', 'S')) then
	sql_restricao_w := sql_restricao_w || ' and	lote.dt_mes_competencia between to_date(''' || dt_inicial_p || ''') and to_date(''' || nvl(dt_final_p,fim_mes(dt_inicial_p)) || ''')' || pls_util_pck.enter_w;
end if;

/* Produto */
if	(ie_dimensao_um_p = 'P') or
	(ie_dimensao_dois_p = 'P') then
	if	(ie_dimensao_um_p = 'P') then
		sql_restricao_w := sql_restricao_w || 	' and	a.nr_seq_plano = ' || ds_vl_dimensao_um_p || pls_util_pck.enter_w;
	else
		sql_restricao_w := sql_restricao_w || 	' and	a.nr_seq_plano = ' || ds_vl_dimensao_dois_p || pls_util_pck.enter_w;
	end if;
end if;

/* Tipo de contratacao */
if	(ie_dimensao_um_p = 'TC') or
	(ie_dimensao_dois_p = 'TC') then
	if	(ie_dimensao_um_p = 'TC') then
		sql_restricao_w := sql_restricao_w || 	' and	a.ie_tipo_contratacao = ''' || ds_vl_dimensao_um_p || '''' || pls_util_pck.enter_w;
	else
		sql_restricao_w := sql_restricao_w || 	' and	a.ie_tipo_contratacao = ''' || ds_vl_dimensao_dois_p || '''' || pls_util_pck.enter_w;
	end if;
end if;

/* Contrato */
if	(ie_dimensao_um_p = 'C') or
	(ie_dimensao_dois_p = 'C') then
	
	if	(ie_dimensao_um_p = 'C') and
		(ds_vl_dimensao_um_p is not null) then
		select	max(nr_sequencia)
		into	nr_seq_contrato_w
		from	pls_contrato
		where	nr_contrato = ds_vl_dimensao_um_p;
		if	(nr_seq_contrato_w is not null) then
			sql_restricao_w := sql_restricao_w || 	' and	a.nr_seq_contrato = ' || nr_seq_contrato_w || pls_util_pck.enter_w;
		else
			sql_restricao_w := sql_restricao_w || 	' and	a.nr_seq_intercambio = ' || ds_vl_dimensao_um_p || pls_util_pck.enter_w;
		end if;
	elsif	(ie_dimensao_dois_p = 'C') and
		(ds_vl_dimensao_dois_p is not null) then
		select	max(nr_sequencia)
		into	nr_seq_contrato_w
		from	pls_contrato
		where	nr_contrato = ds_vl_dimensao_dois_p;
		if	(nr_seq_contrato_w is not null) then
			sql_restricao_w := sql_restricao_w || 	' and	a.nr_seq_contrato = ' || nr_seq_contrato_w || pls_util_pck.enter_w;
		else
			sql_restricao_w := sql_restricao_w || 	' and	a.nr_seq_intercambio = ' || ds_vl_dimensao_dois_p || pls_util_pck.enter_w;
		end if;
	else
		sql_restricao_w := sql_restricao_w || ' and a.nr_seq_contrato is null and a.nr_seq_intercambio is null' || pls_util_pck.enter_w;
	end if;
end if;

/* Ano */
if	(upper(ie_dimensao_um_p) = 'ANO') or
	(upper(ie_dimensao_dois_p) = 'ANO') then
	if	(upper(ie_dimensao_um_p) = 'ANO') then
		dt_mesano_referencia_w := '01/01/' || ds_vl_dimensao_um_p;
	else
		dt_mesano_referencia_w := '01/01/' || ds_vl_dimensao_dois_p;
	end if;
	sql_restricao_w := sql_restricao_w || ' and	lote.dt_mes_competencia between to_date(''' || dt_mesano_referencia_w || ''') and to_date(''' || to_char(last_day(add_months(to_date(dt_mesano_referencia_w),11)),'dd/mm/yyyy') || ''')' || pls_util_pck.enter_w;
end if;

/* Mes */	
if	(upper(ie_dimensao_um_p) = 'MES') or
	(upper(ie_dimensao_dois_p) = 'MES') then
	if	(upper(ie_dimensao_um_p) = 'MES') then
		dt_mesano_referencia_w := ds_vl_dimensao_um_p;
	else
		dt_mesano_referencia_w := ds_vl_dimensao_dois_p;
	end if;
	
	sql_restricao_w := sql_restricao_w || ' and	lote.dt_mes_competencia between to_date(''' || '01/' || dt_mesano_referencia_w || ''') and to_date(''' || to_char(last_day(to_date('01/' || dt_mesano_referencia_w)), 'dd/mm/yyyy') || ''')'|| pls_util_pck.enter_w;		
end if;

/* Trimestre*/
if	(ie_dimensao_um_p = 'T') or
	(ie_dimensao_dois_p = 'T') then
	if	(ie_dimensao_um_p = 'T') then
		dt_mesano_referencia_w := ds_vl_dimensao_um_p;
	else
		dt_mesano_referencia_w := ds_vl_dimensao_dois_p;
	end if;
	
	sql_restricao_w := sql_restricao_w || ' and	lote.dt_mes_competencia between to_date(''' || dt_mesano_referencia_w || ''') and to_date(''' || to_char(last_day(add_months(to_date(dt_mesano_referencia_w),2)), 'dd/mm/yyyy') || ''')' || pls_util_pck.enter_w;
end if;

/* Faixa etaria */
if	(ie_dimensao_um_p = 'F') or 
	(ie_dimensao_dois_p = 'F') then
	if	(ie_dimensao_um_p = 'F') then
		sql_restricao_w := sql_restricao_w || ' and	a.nr_seq_faixa_etaria = ' || ds_vl_dimensao_um_p || pls_util_pck.enter_w;
	else
		sql_restricao_w := sql_restricao_w || ' and	a.nr_seq_faixa_etaria = ' || ds_vl_dimensao_dois_p || pls_util_pck.enter_w;
	end if;
end if;

/* Pagador */
if	(upper(ie_dimensao_um_p) = 'PAG') or
	(upper(ie_dimensao_dois_p) = 'PAG') then
	if	(upper(ie_dimensao_um_p) = 'PAG') and
		(ds_vl_dimensao_um_p is not null) then
		sql_restricao_w := sql_restricao_w || ' and	a.nr_seq_pagador = ' || ds_vl_dimensao_um_p || pls_util_pck.enter_w;
	elsif	(upper(ie_dimensao_dois_p) = 'PAG') and
		(ds_vl_dimensao_dois_p is not null) then
		sql_restricao_w := sql_restricao_w || ' and	a.nr_seq_pagador = ' || ds_vl_dimensao_dois_p || pls_util_pck.enter_w;
	else
		sql_restricao_w := sql_restricao_w || ' and	a.nr_seq_pagador is null ' || pls_util_pck.enter_w;
	end if;
end if;

/* Semestre */
if	(ie_dimensao_um_p = 'S') or
	(ie_dimensao_dois_p = 'S') then
	if	(ie_dimensao_um_p = 'S') then
		dt_mesano_referencia_w := ds_vl_dimensao_um_p;
	else
		dt_mesano_referencia_w := ds_vl_dimensao_dois_p;
	end if;
	
	if	(substr(dt_mesano_referencia_w,1,2) = '01') then
		sql_restricao_w := sql_restricao_w || ' and	lote.dt_mes_competencia between to_date(''' || '01/01/' || substr(dt_mesano_referencia_w, 4, 4) || ''') and to_date(''' || '30/06/' || substr(dt_mesano_referencia_w, 4, 4) || ''')' || pls_util_pck.enter_w;
	else
		sql_restricao_w := sql_restricao_w || ' and	lote.dt_mes_competencia between to_date(''' || '01/07/' || substr(dt_mesano_referencia_w, 4, 4) || ''') and to_date(''' || '31/12/' || substr(dt_mesano_referencia_w, 4, 4) || ''')' || pls_util_pck.enter_w;
	end if;
end if;

/* Beneficiario */
if	(ie_dimensao_um_p = 'B') or
	(ie_dimensao_dois_p = 'B') then
	if	(ie_dimensao_um_p = 'B') then
		sql_restricao_w := sql_restricao_w || ' and	a.nr_seq_segurado = ' || ds_vl_dimensao_um_p || pls_util_pck.enter_w;
	else
		sql_restricao_w := sql_restricao_w || ' and	a.nr_seq_segurado = ' || ds_vl_dimensao_dois_p || pls_util_pck.enter_w;
	end if;
end if;

sql_restricao_w	:= ' and a.ie_tipo_valor = ''' || ie_tipo_valor_p || '''' || pls_util_pck.enter_w;

/* Obter restricao dos filtros da analise. */
sql_restricao_w := sql_restricao_w || pls_ar_montar_resultado_pck.obter_dados_restricao(ds_filtros_p, 'A');

sql_base_w := 	'select	nvl(sum(decode(ie_tipo_grupo_ans, ''10'', vl_liberado)),0) vl_consulta, ' || pls_util_pck.enter_w ||
		'	nvl(sum(decode(ie_tipo_grupo_ans, ''20'', vl_liberado)),0) vl_exame, ' || pls_util_pck.enter_w ||
		'	nvl(sum(decode(ie_tipo_grupo_ans, ''30'', vl_liberado)),0) vl_terapia, ' || pls_util_pck.enter_w ||
		'	nvl(sum(	case when (ie_tipo_grupo_ans in (''41'',''42'',''43'',''44'',''45'',''46'',''49'')) then ' || pls_util_pck.enter_w ||
		'			vl_liberado ' || pls_util_pck.enter_w ||
		'		else ' || pls_util_pck.enter_w ||
		'			0 ' || pls_util_pck.enter_w ||
		'		end),0) vl_internacao, ' || pls_util_pck.enter_w ||
		'	nvl(sum(decode(ie_tipo_grupo_ans, ''50'', vl_liberado)),0) vl_outro_atendimento, ' || pls_util_pck.enter_w ||
		'	nvl(sum(decode(ie_tipo_grupo_ans, ''60'', vl_liberado)),0) vl_demais_despesas, ' || pls_util_pck.enter_w ||
		'	nvl(sum(	case when (nvl(ie_tipo_grupo_ans, ''X'') not in (''10'',''20'',''30'',''41'',''42'',''43'',''44'',''45'',''46'',''49'',''50'',''60'')) then ' || pls_util_pck.enter_w ||
		'			vl_liberado ' || pls_util_pck.enter_w ||
		'		else ' || pls_util_pck.enter_w ||
		'			0 ' || pls_util_pck.enter_w ||
		'		end),0) vl_sem_grupo, ' || pls_util_pck.enter_w ||
		'	nvl(sum(vl_liberado),0) vl_total ' || pls_util_pck.enter_w ||
		'from	(select	nvl(sum(d.vl_liberado),0) vl_liberado, ' || pls_util_pck.enter_w ||
		'		c.ie_tipo_grupo_ans ' || pls_util_pck.enter_w ||
		'	from	pls_ar_lote		lote, ' || pls_util_pck.enter_w ||
		'		pls_ar_conta		a, ' || pls_util_pck.enter_w ||
		'		pls_conta_v		b, ' || pls_util_pck.enter_w ||
		'		ans_grupo_despesa 	c, ' || pls_util_pck.enter_w ||
		'		pls_conta_proc		d, ' || pls_util_pck.enter_w ||
		'		pls_segurado		s  ' || pls_util_pck.enter_w ||
		'	where	a.nr_seq_lote 		= lote.nr_sequencia ' || pls_util_pck.enter_w ||
		'	and	c.nr_sequencia(+)	= d.nr_seq_grupo_ans ' || pls_util_pck.enter_w ||
		'	and	b.nr_sequencia		= a.nr_seq_conta ' || pls_util_pck.enter_w ||
		'	and	s.nr_sequencia 		= a.nr_seq_segurado ' || pls_util_pck.enter_w ||
		'	and	b.nr_sequencia		= d.nr_seq_conta ' || pls_util_pck.enter_w ||
		'	and	d.ie_status 	not in  (''M'',''D'') ' || pls_util_pck.enter_w ||
		sql_restricao_w || pls_util_pck.enter_w ||
		'	group by ' || pls_util_pck.enter_w ||
		'		c.ie_tipo_grupo_ans ' || pls_util_pck.enter_w ||
		'	union all ' || pls_util_pck.enter_w ||
		'	select	nvl(sum(d.vl_liberado),0) vl_liberado, ' || pls_util_pck.enter_w ||
		'		c.ie_tipo_grupo_ans ' || pls_util_pck.enter_w ||
		'	from	pls_ar_lote		lote, ' || pls_util_pck.enter_w ||
		'		pls_ar_conta		a, ' || pls_util_pck.enter_w ||
		'		pls_conta_v		b, ' || pls_util_pck.enter_w ||
		'		ans_grupo_despesa 	c, ' || pls_util_pck.enter_w ||
		'		pls_conta_mat		d, ' || pls_util_pck.enter_w ||
		'		pls_segurado		s  ' || pls_util_pck.enter_w ||
		'	where	a.nr_seq_lote 		= lote.nr_sequencia ' || pls_util_pck.enter_w ||
		'	and	c.nr_sequencia(+)	= d.nr_seq_grupo_ans ' || pls_util_pck.enter_w ||
		'	and	b.nr_sequencia		= a.nr_seq_conta ' || pls_util_pck.enter_w ||
		'	and	s.nr_sequencia 		= a.nr_seq_segurado ' || pls_util_pck.enter_w ||
		'	and	b.nr_sequencia		= d.nr_seq_conta ' || pls_util_pck.enter_w ||
		'	and	d.ie_status 	not in  (''M'',''D'') ' || pls_util_pck.enter_w ||
		sql_restricao_w || pls_util_pck.enter_w ||
		'	group by ' || pls_util_pck.enter_w ||
		'		c.ie_tipo_grupo_ans) ';

open retorno_valores_despesas_w for sql_base_w;	
fetch retorno_valores_despesas_w bulk collect into t_valores_despesas_w limit qt_registro_transacao_w;
close retorno_valores_despesas_w;

for i in t_valores_despesas_w.first..t_valores_despesas_w.last loop
	pipe row(t_valores_despesas_w(i));
end loop;

end;

end pls_despesas_resultado_pck;
/