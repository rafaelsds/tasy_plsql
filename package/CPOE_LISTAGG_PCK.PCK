create or replace package cpoe_listagg_pck as

	function tab_to_string (varchar2_tab_p  in  strarray,
                        	delimiter_p     in  varchar2 default ';') return varchar2;

end cpoe_listagg_pck;
/

create or replace package body cpoe_listagg_pck as
  
	function tab_to_string (varchar2_tab_p  in  strarray,
    						delimiter_p     in  varchar2 default ';') return varchar2 is
		l_string     varchar2(32767);
	begin
		for i in varchar2_tab_p.first .. varchar2_tab_p.last loop
	      	if i != varchar2_tab_p.first then
		        l_string := l_string || delimiter_p;
			end if;
		    l_string := l_string || varchar2_tab_p(i);
	    end loop;

	    return l_string;
	end tab_to_string;

end cpoe_listagg_pck;
/