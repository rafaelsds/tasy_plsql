create or replace
package pcs_sup_gerar_planej_pck is

/* Procedure Principal */
/* Realiza a chamada das procedures na ordem necess�ria para gera��o das informa��es */
procedure pcs_executar_formula(					ds_macro_p		varchar2 default null,
								nr_seq_grupamento_p	number,
								nr_seq_tipo_lista_p	number,
								ie_origem_pcs_p		varchar2,
								nr_seq_relatorio_p	number,
								cd_responsavel_p	varchar2,
								nr_ordem_servico_p	number,
								nm_usuario_p		Varchar2);

procedure obter_se_condicao_verdadeira(
	ds_condicao_p			varchar2,
	ie_retorno_p		out	varchar2,
	nm_usuario_p			varchar2);

procedure pcs_montar_consulta(cd_material_p	number, 
		cd_estabelecimento_p	number,
		cd_empresa_p		number,
		nm_usuario_p		varchar2);

procedure pcs_gerar_vl_formula( ds_macro_p				varchar2 default null,
								cd_material_p			number,
								nr_seq_grupamento_p		number,
								nr_Seq_segmento_p		number,
								cd_local_estoque_p		number,
								cd_empresa_p			number,
								cd_estabelecimento_p	number,
								nm_usuario_p			varchar2);

end pcs_sup_gerar_planej_pck;
/

CREATE OR REPLACE package body pcs_sup_gerar_planej_pck is

procedure pcs_executar_formula(					ds_macro_p		varchar2 default null,
								nr_seq_grupamento_p	number,
								nr_seq_tipo_lista_p	number,
								ie_origem_pcs_p		varchar2,
								nr_seq_relatorio_p	number,
								cd_responsavel_p	varchar2,
								nr_ordem_servico_p	number,
								nm_usuario_p		Varchar2) is

cd_material_w				number(6);
cd_empresa_w				number(4);
cd_estabelecimento_w		number(4);
nr_seq_segmento_w			number(10);
cd_local_estoque_w			number(4);

Cursor C01 is
	select  distinct cd_material,
		cd_empresa,
		cd_estabelecimento,
		nr_seq_segmento
	from	pcs_seg_compras_rel_item
	where	nr_seq_relatorio = nr_seq_relatorio_p
	and	nr_seq_grupamento = nvl(nr_seq_grupamento_p,0)
	and	nvl(nr_seq_grupamento_p,0) > 0
	and	((pcs_obter_tipo_lista(nr_seq_tipo_lista_p) not in ('LC','MLC')) or (pcs_obter_tipo_lista(nr_seq_tipo_lista_p) is null))
	and	ie_origem_pcs_p = 'C'
	union all
	select  distinct cd_material,
		cd_empresa,
		cd_estabelecimento,
		nr_seq_segmento
	from	pcs_seg_compras_rel_item
	where	nr_seq_relatorio = nr_seq_relatorio_p
	and	nr_seq_tipo_lista_p is not null
	and	cd_pessoa_resp_lc = cd_responsavel_p
	and	(((obter_dias_estoque_material(cd_estabelecimento,cd_material,nm_usuario_p) <= nvl(qt_dias_corte_mlc,0)) and (pcs_obter_tipo_lista(nr_seq_tipo_lista_p) = 'MLC'))
	or	((obter_dias_estoque_material(cd_estabelecimento,cd_material,nm_usuario_p) <= nvl(qt_dias_corte_lc,0)) and (pcs_obter_tipo_lista(nr_seq_tipo_lista_p) = 'LC')))
	and	ie_origem_pcs_p = 'C'
	union all
	select  distinct b.cd_material,
		b.cd_empresa,
		b.cd_estabelecimento,
		b.nr_seq_segmento
	from	pcs_segmento_compras_rel a,
		pcs_seg_compras_rel_item b
	where	a.nr_sequencia = b.nr_seq_relatorio
	and	b.nr_seq_relatorio = nr_seq_relatorio_p
	and	nr_seq_tipo_lista_p is not null
	and	b.cd_pessoa_resp_lc = cd_responsavel_p
	and	b.dt_entrega_prevista_oc is not null
	and	((pkg_date_utils.get_time(nvl(b.dt_entrega_prevista_oc,sysdate), '00:00:00')	<= nvl(dt_geracao,sysdate)) and (pcs_obter_tipo_lista(nr_seq_tipo_lista_p) = 'PV'))
	and	ie_origem_pcs_p = 'C'
	union all
	select	distinct c.cd_material,
		a.cd_empresa,
		c.cd_estabelecimento,
		b.nr_sequencia
	from	pcs_grupamentos a,
		pcs_segmento b,
		pcs_estrutura_segmento c
	where	a.nr_sequencia = b.nr_seq_grupamento
	and	a.nr_sequencia = c.nr_seq_grupamento
	and	b.nr_sequencia = c.nr_seq_segmento
	and	ie_origem_pcs_p = 'M'
	and	exists (select	1
			from	man_ordem_servico_mat x
			where	x.nr_seq_ordem_serv = nr_ordem_servico_p
			and	x.cd_material = c.cd_material);


begin

delete	w_pcs_atributo_formula
where	nm_usuario = nm_usuario_p;


open C01;
loop
fetch C01 into
	cd_material_w,
	cd_empresa_w,
	cd_estabelecimento_w,
	nr_seq_segmento_w;
exit when C01%notfound;
	begin

	pcs_montar_consulta(cd_material_w,cd_estabelecimento_w,cd_empresa_w,nm_usuario_p);


	select max(cd_local_estoque)
	into   cd_local_estoque_w
	from   pcs_local_estoque_grup
	where  nr_seq_grupamento = nr_seq_grupamento_p
	and	   cd_estab_regra = cd_estabelecimento_w;

	pcs_gerar_vl_formula(	ds_macro_p,
							cd_material_w,
							nr_seq_grupamento_p,
							nr_seq_segmento_w,
							cd_local_estoque_w,
							cd_empresa_w,
							cd_estabelecimento_w,
							nm_usuario_p);

	end;
end loop;
close C01;

commit;

end pcs_executar_formula;

/* Procedure para identificar as condi��es l�gicas das f�rmulas */
procedure obter_se_condicao_verdadeira(
	ds_condicao_p			varchar2,
	ie_retorno_p		out	varchar2,
	nm_usuario_p			varchar2) is

c001				integer;
ds_sql_w			varchar2(2000);
vl_coluna_w			varchar2(255);
ds_retorno_w			varchar2(1);
ds_condicao_original_w		varchar2(2000);
ds_informacao_w			varchar2(255);
vl_informacao_w			number(13,4);
vl_atributo_w			varchar2(50);
ds_condicao_w			varchar2(2000);
tam_condicao_w			number(10);
qt_existe_w			number(10);
ie_pos_separador_w		number(10);
vl_retorno_atrib_w		number(10);

begin

ds_condicao_w				:= '';
ds_condicao_original_w		:= ds_condicao_p;

/* Verifica se existe algum atributo e/ou f�rmula dentro da condi��o para substituir */
select	count(*)
into	qt_existe_w
from	dual
where	ds_condicao_original_w like '%#%';


/* Monta o SQL para verificar se a condi��o � True ou False */ --importante

if (ds_condicao_original_w is null) then
	ds_condicao_original_w := '1 = 1';
end if;

ds_sql_w	:= 'select count(*) from dual where ' || ds_condicao_original_w;
c001	:= dbms_sql.open_cursor;
dbms_sql.parse(c001,ds_sql_w,dbms_sql.native);
dbms_sql.define_column(c001, 1, vl_coluna_w, 255);
vl_retorno_atrib_w	:= dbms_sql.execute(c001);
vl_retorno_atrib_w	:= dbms_sql.fetch_rows(c001);
dbms_sql.column_value(c001, 1, vl_coluna_w);
dbms_sql.close_cursor(c001);

if	(vl_coluna_w = '0') then
	ie_retorno_p	:= 'N';
else
	ie_retorno_p	:= 'S';
end if;

end obter_se_condicao_verdadeira;

procedure pcs_montar_consulta( 	cd_material_p number,				--aqui pega o sql que foi constru�do no pcs_montar_sql_atrib_formula
			cd_estabelecimento_p number,
			cd_empresa_p number,
			nm_usuario_p		Varchar2) is 	-- vai executar esse sqlk e gravar os valores em uma tabela W para posteriormente substitu�rem as macros.

c001				integer;
ds_sql_w			varchar2(4000);	-- Este � o SQL gerado confome a tabela, atributo, e restri��es informados na mesma tabela. O SQL � gerado pela procedure pcs_montar_sql_atrib_formula
ds_sql_informado_w	varchar2(4000);  --Este � o SQL informado manualmente pelo usu�rio
ds_macro_w			varchar2(50);
vl_resultado_w		number(18,4);
ds_resultado_w		varchar2(255);
dt_inicial_w		varchar2(255);
dt_inicial_ww		date;
dt_final_w			varchar2(255);
dt_final_ww			date;
vl_coluna_w			varchar2(255);
nm_campo_w			varchar2(50);
nm_tabela_w			varchar2(50);
ie_periodo_w		varchar2(15);
qt_periodo_w		number(10);
ie_periodo_atual_w	varchar2(1);
nr_seq_atributo_w	number(10);

cursor c01 is							--vai pegar o sql montado de todos os atributos e inserir na tabela W
	select	ds_macro,
			upper(ds_sql),
			ie_periodo,
			qt_periodo,
			nvl(ie_periodo_atual,'N'),
			nr_sequencia
	from	pcs_atributo
	where	ds_sql is not null;

begin

open C01;
loop
fetch C01 into
	ds_macro_w,
	ds_sql_w,
	ie_periodo_w,
	qt_periodo_w,
	ie_periodo_atual_w,
	nr_seq_atributo_w;
exit when C01%notfound;
	begin

	if (ie_periodo_w is not null) and
	(qt_periodo_w is not null) and
	(ie_periodo_atual_w = 'S') then
		begin
		dt_final_ww := sysdate;
		if (ie_periodo_w = 'A') then
			qt_periodo_w	:= qt_periodo_w * 12;
			dt_inicial_ww	:= PKG_DATE_UTILS.ADD_MONTH(sysdate, -qt_periodo_w, 0);
		elsif (ie_periodo_w = 'M') then
			dt_inicial_ww	:= PKG_DATE_UTILS.ADD_MONTH(sysdate, -qt_periodo_w, 0);
		elsif (ie_periodo_w = 'S') then
			qt_periodo_w	:= qt_periodo_w * 7;
			dt_inicial_ww	:= sysdate - qt_periodo_w;
		elsif (ie_periodo_w = 'D') then
			dt_final_ww	:= sysdate;
			dt_inicial_ww	:= sysdate - qt_periodo_w/24;
		end if;

		dt_inicial_w	:= 'to_date('||''''||to_char(dt_inicial_ww,'dd/mm/yyyy')||''','''||'dd/mm/yyyy'||''''||')';
		dt_final_w	:= 'to_date('||''''||to_char(fim_dia(dt_final_ww),'dd/mm/yyyy hh24:mi:ss')||''','''||'dd/mm/yyyy hh24:mi:ss'||''''||')';

		end;
	elsif (ie_periodo_w is not null) and
	(qt_periodo_w is not null) and
	(ie_periodo_atual_w = 'N') then
		begin
		dt_final_ww := sysdate;
		if (ie_periodo_w = 'A') then
			qt_periodo_w	:= qt_periodo_w * 12;
			dt_final_ww	:= PKG_DATE_UTILS.start_of(sysdate, 'YEAR', 0);
			dt_inicial_ww	:= PKG_DATE_UTILS.ADD_MONTH(pkg_date_utils.start_of(sysdate,'DD',0), -qt_periodo_w, 0);
		elsif (ie_periodo_w = 'M') then
			dt_final_ww	:= PKG_DATE_UTILS.start_of(sysdate, 'MONTH', 0);
			dt_inicial_ww	:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(sysdate, 'MONTH', 0), -qt_periodo_w, 0);
		elsif (ie_periodo_w = 'S') then
			qt_periodo_w	:= qt_periodo_w * 7;
			dt_final_ww	:= pkg_date_utils.start_of(sysdate,'DD',0);
			dt_inicial_ww	:= pkg_date_utils.start_of(sysdate,'DD',0) - qt_periodo_w;
		elsif (ie_periodo_w = 'D') then
			dt_final_w	:= sysdate;
			dt_inicial_ww	:= sysdate - qt_periodo_w/24;
		end if;

		dt_inicial_w	:= 'to_date('||''''||to_char(dt_inicial_ww,'dd/mm/yyyy')||''','''||'dd/mm/yyyy'||''''||')';
		dt_final_w	:= 'to_date('||''''||to_char(fim_dia(dt_final_ww),'dd/mm/yyyy hh24:mi:ss')||''','''||'dd/mm/yyyy hh24:mi:ss'||''''||')';

		end;
	end if;

	DS_SQL_W := REPLACE(DS_SQL_W, ':DT_INI', DT_INICIAL_W);
	DS_SQL_W := REPLACE(DS_SQL_W, ':DT_FIM',  DT_FINAL_W);
	DS_SQL_W := REPLACE(DS_SQL_W, ':CD_MATERIAL',  CD_MATERIAL_P);
	DS_SQL_W := REPLACE(DS_SQL_W, ':CD_ESTABELECIMENTO', CD_ESTABELECIMENTO_P);
	DS_SQL_W := REPLACE(DS_SQL_W, ':CD_EMPRESA',  CD_EMPRESA_P);

	begin

	c001	:= dbms_sql.open_cursor;
	dbms_sql.parse(c001,ds_sql_w,dbms_sql.native);
	dbms_sql.define_column(c001, 1, vl_coluna_w, 255);
	ds_resultado_w	:= dbms_sql.execute(c001);
	ds_resultado_w	:= dbms_sql.fetch_rows(c001);
	dbms_sql.column_value(c001,1,vl_coluna_w);
	dbms_sql.close_cursor(c001);


	ds_resultado_w := nvl(vl_coluna_w,0);

	ds_macro_w := '#A_'||ds_macro_w||'@';

	exception
	when others then
		begin
		ds_resultado_w := null;
		ds_macro_w := '#E_'||ds_macro_w||'@';
		end;
	end;

	insert into w_pcs_atributo_formula(
		ds_macro,
		ds_resultado,
		nr_seq_formula,
		ds_sql,
		cd_material,
		nm_usuario)
	values (ds_macro_w,
		ds_resultado_w,
		null,
		ds_sql_w,
		cd_material_p,
		nm_usuario_p);
	end;
end loop;
close C01;

commit;

end pcs_montar_consulta;

procedure pcs_gerar_vl_formula( ds_macro_p				varchar2 default null,
								cd_material_p			number,
								nr_seq_grupamento_p		number,
								nr_Seq_segmento_p		number,
								cd_local_estoque_p		number,
								cd_empresa_p			number,
								cd_estabelecimento_p	number,
								nm_usuario_p			varchar2) is --aqui ir� substiu�r as macros da f�rmula.


nr_sequencia_w		number(10);
ds_macro_w		varchar2(50);
ds_formula_w		varchar2(4000);
ds_sql_w		varchar2(4000);
ds_macro_ww		varchar2(50);
vl_resultado_w		number(18,4);
ds_resultado_w		varchar2(17);
vl_coluna_w		varchar2(255);
ie_cond_verdadeira_w	varchar2(1);
tam_formula_w		number(10);
ds_informacao_w		varchar2(255);
ie_pos_separador_w	number(10);
ds_formula_orig_w	varchar2(4000);
ds_atrib_padrao_w	varchar2(255);
qt_form_substituida_w	number(10);
nm_macro_padrao_original_w	varchar2(255);
nm_macro_padrao_w	varchar2(255);
nm_macro_parametro_w	varchar2(255);
nm_macro_w		varchar2(255);
nm_macro_padrao_substituida_w	varchar2(255);
qt_tamanho_form_w	number(10);
qt_macro_padrao_A_w	number(10);
qt_macro_padrao_F_w	number(10);
qt_macro_padrao_arroba_w number(10);
cd_dominio_w		varchar2(255);
qt_condicao_se_w	number(10);
i number(5);

c001		integer;

cursor c01 is

select	a.nr_sequencia,
	a.ds_macro,
	a.ds_composicao
from	pcs_formulas a,
	pcs_reg_calc_com_formula c
where	a.nr_sequencia = c.nr_seq_formula_planej
and	a.ds_composicao is not null
and	a.ds_macro = nvl(ds_macro_p,ds_macro)
order by nr_ordem_calculo;--nome da macro


cursor c02 is

select	ds_macro,
		ds_resultado
from 	w_pcs_atributo_formula
where	nm_usuario = nm_usuario_p
and		cd_material = cd_material_p;

Cursor C03 is

select	cd_dominio
from	pcs_atributos_v
where	instr(ds_formula_orig_w,'#A') > 0
and	cd_dominio = nm_macro_parametro_w
order by cd_dominio;

begin

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	ds_macro_w,
	ds_formula_orig_w;
exit when c01%notfound;
	begin

	open c02;
	loop
	fetch c02 into
		ds_macro_ww,
		ds_resultado_w;
	exit when c02%notfound;
		ds_formula_orig_w := replace(ds_formula_orig_w, ds_macro_ww, replace(to_char(ds_resultado_w),',','.'));
	end loop;
	close c02;

	i := 0;
	while  instr(ds_formula_orig_w,'#A') > 0 loop
		begin
		i := i + 1;

		if (i > 40000) then
		wheb_mensagem_pck.exibir_mensagem_abort(323770);
			-- Feito este controle afim de segurar que n�o fique em loop.
			--Ultrapassou a quantidade limite de loops. Favor contatar a Philips para verifica��es.
			-- Caso entrar aqui � porque houve algum problema na substitui��o das macros, deve haver algum tipo de problema na escrita das f�rmulas.
		end if;

		qt_macro_padrao_A_w := null;
		qt_macro_padrao_arroba_w := null;
		nm_macro_w := null;
		nm_macro_parametro_w := null;
		--Verificado se existe alguma macro n�o substituida, caso for verdadeiro, ser� buscado como atributo padr�o - (Pasta atributos padr�es).
		qt_tamanho_form_w  := length(ds_formula_orig_w);
		qt_macro_padrao_A_w := instr(ds_formula_orig_w,'#A');
		qt_macro_padrao_arroba_w := instr(ds_formula_orig_w,'@');

		--nm_macro_padrao_w :=  substr(ds_formula_orig_w,qt_macro_padrao_A_w + 3, qt_tamanho_form_w);
		if (instr(ds_formula_orig_w,'@') > 0) then

			qt_macro_padrao_arroba_w := instr(ds_formula_orig_w,'@');
			nm_macro_parametro_w := substr(ds_formula_orig_w,qt_macro_padrao_A_w+3, qt_macro_padrao_arroba_w - qt_macro_padrao_A_w - 3);
			nm_macro_w := substr(ds_formula_orig_w,qt_macro_padrao_A_w, qt_macro_padrao_arroba_w - qt_macro_padrao_A_w + 1);

		end if;

		open C03;
		loop
		fetch C03 into
			cd_dominio_w;
		exit when C03%notfound;
			begin


			if (cd_dominio_w = nm_macro_parametro_w) then
				ds_atrib_padrao_w := obter_dados_pcs(cd_empresa_p,null,nr_Seq_segmento_p,cd_material_p,2,cd_estabelecimento_p,nm_macro_parametro_w);

			ds_formula_orig_w := replace(ds_formula_orig_w, nm_macro_w, replace(to_char(ds_atrib_padrao_w),',','.'));

			end if;

			end;
		end loop;
		close C03;

		end;
	end loop;

		qt_tamanho_form_w	:= null;
		qt_macro_padrao_F_w := null;
		qt_macro_padrao_arroba_w := null;

		--Verificado se existe alguma macro n�o substituida, caso for verdadeiro, ser� buscado como atributo padr�o - (Pasta atributos padr�es).
		qt_tamanho_form_w	:= length(ds_formula_orig_w);
		qt_macro_padrao_F_w := instr(ds_formula_orig_w,'#F');

		if (qt_macro_padrao_F_w > 0) then

			qt_macro_padrao_arroba_w := instr(substr(ds_formula_orig_w, qt_macro_padrao_F_w,qt_tamanho_form_w),'@');

			--nm_macro_parametro_w := substr(ds_formula_orig_w,qt_macro_padrao_F_w+3, qt_macro_padrao_arroba_w - qt_macro_padrao_F_w - 3);
			nm_macro_parametro_w := substr(ds_formula_orig_w,qt_macro_padrao_F_w + 1, qt_macro_padrao_arroba_w);
			nm_macro_w := substr(ds_formula_orig_w,qt_macro_padrao_F_w, qt_macro_padrao_arroba_w - qt_macro_padrao_F_w + 1);

			commit;

			pcs_gerar_vl_formula(nm_macro_parametro_w,
								 cd_material_p,
								 nr_seq_grupamento_p,
								 nr_Seq_segmento_p,
								 cd_local_Estoque_p,
								 cd_empresa_p,
								 cd_estabelecimento_p,
								 nm_usuario_p); -- CHAMADA RECURSIVA

		end if;

	/* Caso identifique a palavra "SE", compreende que � uma f�rmula com condi��o l�gica */
	/* e executa a rotina para validar se a condi��o � verdadeira */

	--if (instr(ds_formula_orig_w,'#') = 0) then

		if	(substr(ds_formula_orig_w,1,2) = 'SE') then
			begin
			ie_cond_verdadeira_w	:= 'N';

			/* Enquanto a condi��o n�o for verdadeira, continua testando */
			while	(ie_cond_verdadeira_w <> 'S') loop
				begin

				/* Desmonta a condi��o para executar a rotina de valida��o */
				ds_formula_orig_w	:= substr(ds_formula_orig_w,4,length(ds_formula_orig_w)); --vai cortar a formula depouis do 'SE'
				tam_formula_w		:= length(ds_formula_orig_w); --vai pegar o tamanho depois de cortado a f�rmula
				ie_pos_separador_w	:= instr(ds_formula_orig_w,'ENTAO'); --identifica a posi��o do entao

				/* Joga a condi��o para a variavel ds_informacao_w e o restante da f�rmula para a ds_formula_w */
				ds_informacao_w		:= substr(ds_formula_orig_w,1,(ie_pos_separador_w - 1)); -- s� ir� pegar a condicao 'SE' sem o 'ENTAO'   ex (1 = 1)

				if instr(ds_formula_orig_w,'ENTAO') > 0  then
					ds_formula_orig_w	:= substr(ds_formula_orig_w,(ie_pos_separador_w + 5),tam_formula_w); --Pega depois do entao o resto da f�rmula
				else
					ds_formula_orig_w	:= substr(ds_formula_orig_w,(ie_pos_separador_w + 2),tam_formula_w); --Pega depois do entao o resto da f�rmula
				end if;

				obter_se_condicao_verdadeira(ds_informacao_w,
							ie_cond_verdadeira_w,
							nm_usuario_p);

				/* Caso a condi��o n�o seja verdadeira, verifica se existem outras op��es */
				/* Se existir, apaga a parte da f�rmula (na variavel) que estava dentro da condi��o falsa, e continua adiante */
				/* Caso n�o houver novas condi��es, retorna 0 */
				if	(ie_cond_verdadeira_w = 'N') then
					begin

					tam_formula_w			:= length(ds_formula_orig_w);
					ie_pos_separador_w		:= instr(ds_formula_orig_w,'SE');

					/* Para retornar zero na f�rmula, pois nenhuma condi��o foi v�lida (ao acabar as op��es)*/
					if	(ie_pos_separador_w = 0) then
						ds_formula_orig_w       := '';
						ds_formula_w		:= '0';
						ie_cond_verdadeira_w	:= 'S';
					else
						ds_formula_orig_w		:= substr(ds_formula_orig_w,ie_pos_separador_w,tam_formula_w);
					end if;

					end;
				/* Se a condi��o for verdadeira, busca a parte da f�rmula referente a esta condi��o, apagando todo o resto (da variavel) */
				else
					begin

					ie_pos_separador_w	:= instr(ds_formula_w,'SE');

					if	(ie_pos_separador_w > 0) then
						ds_formula_orig_w	:= substr(ds_formula_orig_w,1,(ie_pos_separador_w - 1));
					end if;

					ds_formula_orig_w		:= trim(ds_formula_orig_w);
					end;
				end if;

				end;
			end loop;

			end;
		end if;

--preciso arranjar uma forma de verificar aqui, se existe outro se

/*if (cd_material_p = 44) and
	    (ds_macro_w = 'PCS_TESTE') then
			insert into mhbarth3 (
				ds
				)
			values(	'cd_material_p712= ' || cd_material_p ||
				'nm_macro_w = ' || nm_macro_w ||
				'ds_atrib_padrao_w = ' || ds_atrib_padrao_w ||
				'nm_macro_parametro_w = ' || nm_macro_parametro_w ||
			'ds_macro_w = ' || ds_macro_w ||
			'ds_formula_orig_w = ' || ds_formula_orig_w);
			commit;
		end if;			*/

		if (ds_formula_orig_w is not null) then
			begin
			begin
			--SE ( 1 = 1 ) ENTAO 2 * 2     o ds_formula_orig_w fica desta maneira
			qt_condicao_se_w := instr(ds_formula_orig_w,'SE');

			if (qt_condicao_se_w > 0) then
				ds_formula_orig_w := substr(ds_formula_orig_w,1,qt_condicao_se_w - 2);
			end if;

			ds_sql_w := 'select ' || trim(ds_formula_orig_w) || ' from dual';

			c001	:= dbms_sql.open_cursor;
			dbms_sql.parse(c001,ds_sql_w,dbms_sql.native);
			dbms_sql.define_column(c001, 1, vl_coluna_w, 17);
			ds_resultado_w	:= dbms_sql.execute(c001);
			ds_resultado_w	:= dbms_sql.fetch_rows(c001);
			dbms_sql.column_value(c001,1,vl_coluna_w);
			dbms_sql.close_cursor(c001);

			ds_resultado_w := nvl(vl_coluna_w,0);

			ds_macro_w := '#F_'||ds_macro_w||'@';
			exception
				when others then
				begin
				ds_resultado_w := null;
				ds_macro_w := '#E_'|| ds_macro_w ||'@';
				end;
			end;

			insert into w_pcs_atributo_formula(
						ds_macro,
						ds_resultado,
						nr_seq_formula,
						ds_sql,
						cd_material,
						nm_usuario)
			values (	ds_macro_w,
						ds_resultado_w,
						nr_sequencia_w,
						ds_sql_w,
						cd_material_p,
						nm_usuario_p);

			end;
		end if;
	end;

end loop;
close c01;

commit;

end pcs_gerar_vl_formula;


end pcs_sup_gerar_planej_pck;

/