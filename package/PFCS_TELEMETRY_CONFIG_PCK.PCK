create or replace package pfcs_telemetry_config_pck as

	function get_patient_review(dt_monitor_entrance_p date) return varchar;
	function get_transition_review(qt_red_alamrs_p number, qt_yellow_alarms_p number, trs_p number, dt_entrance_p date) return varchar;
	function get_request_waiting_time(nr_seq_encounter_p number) return number;
	function get_average_waiting_time(cd_establishment_p number, cd_department_p varchar2) return number;
	function get_discharge_orders(cd_establishment_p number, nr_seq_encounter_p number) return varchar;
	function get_avg_waiting_time_on_tele(cd_establishment_p number, cd_department_p varchar2) return number;

	procedure telemetry_time_rules(	qt_time_telemetry_rule_p			out	nocopy number,
									qt_trans_review_rule_p				out	nocopy number,
									qt_tr_trs_rule_p					out nocopy number,
									qt_tr_red_alrm_rule_p				out nocopy number,
									qt_tr_yllw_alrm_rule_p				out nocopy number);
end pfcs_telemetry_config_pck;
/

create or replace PACKAGE BODY pfcs_telemetry_config_pck AS
	qt_trans_review_rule_w			pfcs_telemetry_config.qt_trans_review_rule%type;
	qt_time_telemetry_rule_w		pfcs_telemetry_config.qt_time_on_tele__rule%type;
	qt_tr_trs_rule_w				pfcs_telemetry_config.qt_tr_trs_rule%type;
	qt_tr_red_alrm_rule_w			pfcs_telemetry_config.qt_tr_red_alrm_rule%type;
	qt_tr_yllw_alrm_rule_w			pfcs_telemetry_config.qt_tr_yllw_alrm_rule%type;
	ie_rule_return_w				varchar2(1) := 'N';

	cursor cursor_average_waiting_time(cd_establishment_p number, cd_department_p varchar2) is
		select	get_request_waiting_time(pdp.nr_seq_encounter) qt_waiting_time
		  from	pfcs_panel_detail ppd,
				pfcs_detail_patient pdp,
				pfcs_detail_bed pdb
		 where	ppd.nr_seq_indicator = 40
		   and	ppd.nr_sequencia = pdp.nr_seq_detail
		   and	ppd.nr_sequencia = pdb.nr_seq_detail
		   and	ppd.nr_seq_operational_level = cd_establishment_p
		   and	(cd_department_p is null or pdb.cd_department = cd_department_p or ds_department = cd_department_p)
		   and	ppd.ie_situation = 'A';
	
	procedure telemetry_time_rules(	qt_time_telemetry_rule_p			out	nocopy number,
									qt_trans_review_rule_p				out	nocopy number,
									qt_tr_trs_rule_p					out nocopy number,
									qt_tr_red_alrm_rule_p				out nocopy number,
									qt_tr_yllw_alrm_rule_p				out nocopy number) is
	begin
		select	nvl(max(qt_time_on_tele__rule),12),
				max(qt_trans_review_rule),
				nvl(max(qt_tr_trs_rule),50),
				max(qt_tr_red_alrm_rule),
				max(qt_tr_yllw_alrm_rule)
		  into	qt_time_telemetry_rule_p,
				qt_trans_review_rule_p,
				qt_tr_trs_rule_p,
				qt_tr_red_alrm_rule_p,
				qt_tr_yllw_alrm_rule_p
		  from	pfcs_telemetry_config;
	end;

	function get_patient_review(dt_monitor_entrance_p date) return varchar as
		ie_rule_return_w				varchar2(1) := 'N';
	begin
		telemetry_time_rules(qt_time_telemetry_rule_w, qt_trans_review_rule_w, qt_tr_trs_rule_w, qt_tr_red_alrm_rule_w, qt_tr_yllw_alrm_rule_w);

		if (round((sysdate - dt_monitor_entrance_p) * 24 * 60) > (qt_time_telemetry_rule_w * 60)) then
			ie_rule_return_w := 'S';
		end if;

		return ie_rule_return_w;
	end get_patient_review;

	function get_transition_review(qt_red_alamrs_p number, qt_yellow_alarms_p number, trs_p number, dt_entrance_p date) return varchar as
		ie_rule_return_w				varchar2(1) := 'N';
		qt_time_on_tele_w				number(10);
	begin
		telemetry_time_rules(qt_time_telemetry_rule_w, qt_trans_review_rule_w, qt_tr_trs_rule_w, qt_tr_red_alrm_rule_w, qt_tr_yllw_alrm_rule_w);
		
		select ((sysdate - dt_entrance_p) * 1440) into qt_time_on_tele_w from dual;

		
		if ((qt_tr_red_alrm_rule_w is null or (qt_red_alamrs_p <= qt_tr_red_alrm_rule_w)) 		and
			(qt_tr_yllw_alrm_rule_w is null or (qt_yellow_alarms_p <= qt_tr_yllw_alrm_rule_w)) 	and
			(trs_p >= qt_tr_trs_rule_w) 														and
			(qt_trans_review_rule_w is null or (qt_time_on_tele_w >= (qt_trans_review_rule_w * 60)))) 	then
				ie_rule_return_w := 'S';
		end if;

		return ie_rule_return_w;
	end get_transition_review;

	function get_request_waiting_time(nr_seq_encounter_p number) return number as
		nr_request_time_w			number(10) := 0;
	begin
		select	round((sysdate - nvl(sr.dt_authored_on,sysdate)) * 24 * 60)
		  into	nr_request_time_w
		  from	pfcs_service_request sr
		 where	sr.nr_seq_encounter = nr_seq_encounter_p
		   and	sr.si_status = 'ACTIVE'
		   and	sr.cd_service = 'E0404';

		return nr_request_time_w;

		exception
		when no_data_found then
		  return 0;
	end get_request_waiting_time;

	function get_average_waiting_time(	cd_establishment_p	number,
										cd_department_p		varchar2) return number as
		qt_waiting_time_w			number(15) := 0;
		qt_total_records_w			number(15) := 0;
	begin
		for c01_w in cursor_average_waiting_time(cd_establishment_p, cd_department_p)  loop
			qt_total_records_w := qt_total_records_w + 1;
			qt_waiting_time_w := qt_waiting_time_w + c01_w.qt_waiting_time;
		end loop;

		return round(dividir_sem_round(qt_waiting_time_w, qt_total_records_w));
	end get_average_waiting_time;

	function get_discharge_orders(cd_establishment_p number, nr_seq_encounter_p number) return varchar as
		ie_discharge_written_w				varchar2(1) := 'N';
	begin
		select  nvl(max('S'), 'N')
		  into	ie_discharge_written_w
		  from	pfcs_panel_detail ppd,
				pfcs_detail_patient pdp,
				pfcs_detail_bed pdb,
				pfcs_detail_device pdd
		 where	ppd.nr_seq_indicator = 50
		   and	ppd.nr_sequencia = pdp.nr_seq_detail (+)
		   and	ppd.nr_sequencia = pdb.nr_seq_detail
		   and	ppd.nr_sequencia = pdd.nr_seq_detail
		   and	pdp.nr_seq_encounter = nr_seq_encounter_p
		   and	ppd.nr_seq_operational_level =  cd_establishment_p
		   and	ppd.ie_situation = 'A';

		return ie_discharge_written_w;
	end get_discharge_orders;
	
	function get_avg_waiting_time_on_tele(cd_establishment_p number, cd_department_p varchar2) return number as
		nr_time_on_tele_w number(15) := 0;
		nr_total_patients_w number(15) := 0;
	begin
		select	sum(round(((sysdate - pdp.dt_monitor_entrance) * 1440 ))) nr_time_on_tele,
				count(1) nr_total_patients
		into	nr_time_on_tele_w,
				nr_total_patients_w
		from	pfcs_panel_detail ppd,
				pfcs_detail_patient pdp,
				pfcs_detail_bed pdb
		where	ppd.nr_seq_indicator = 46
		and	ppd.nr_sequencia = pdp.nr_seq_detail
		and	ppd.nr_sequencia = pdb.nr_seq_detail
		and	ppd.nr_seq_operational_level = cd_establishment_p
		and	(cd_department_p is null or pdb.cd_department = cd_department_p or ds_department = cd_department_p)
		and	ppd.ie_situation = 'A';
		
		return round(dividir_sem_round(nr_time_on_tele_w, nr_total_patients_w));
		
	end get_avg_waiting_time_on_tele;
end pfcs_telemetry_config_pck;
/
