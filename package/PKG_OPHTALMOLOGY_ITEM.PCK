create or replace 
package pkg_ophtalmology_item as

function get_code_by_reg_type (	reg_type_p varchar2 ) return number;
function get_reg_type (	item_code_p number ) return varchar2;
function get_code_by_emr_item (	emr_item_p number ) return number;
					
end pkg_ophtalmology_item;
/

create or replace PACKAGE BODY pkg_ophtalmology_item AS

FUNCTION get_code_by_reg_type (	reg_type_p varchar2 ) RETURN number IS

item_code_w oftalmologia_item.nr_sequencia%type;
	
BEGIN
	
	case reg_type_p
		when 'ONL' then item_code_w 	:= 2;
		when 'EOEL' then item_code_w 	:= 6;
		when 'MOOL' then item_code_w 	:= 7;
		when 'TPNL' then item_code_w 	:= 8;
		when 'FNCL' then item_code_w 	:= 9;
		when 'BICL' then item_code_w 	:= 10;
		when 'ODL' then item_code_w 	:= 12;
		when 'REOL' then item_code_w 	:= 13;
		when 'DNPL' then item_code_w 	:= 17;
		when 'AVNL' then item_code_w 	:= 19;
		when 'CNL' then item_code_w 	:= 21;
		when 'TAPL' then item_code_w 	:= 23;
		when 'OCL' then item_code_w 	:= 24;
		when 'OAL' then item_code_w 	:= 31;
		when 'CTL' then item_code_w 	:= 69;
		when 'AGRL' then item_code_w 	:= 71;
		when 'GNOL' then item_code_w 	:= 73;
		when 'BIOL' then item_code_w 	:= 78;
		when 'MIEL' then item_code_w 	:= 80;
		when 'PUPL' then item_code_w 	:= 82;
		when 'PAQL' then item_code_w 	:= 84;
		when 'PQUL' then item_code_w 	:= 84;
		when 'MOCL' then item_code_w 	:= 86;
		when 'OGL' then item_code_w 	:= 160;
		when 'OIL' then item_code_w 	:= 168;
		when 'CAPL' then item_code_w 	:= 171;
		when 'TOGL' then item_code_w 	:= 173;
		when 'CERL' then item_code_w 	:= 175;
		when 'TOCL' then item_code_w 	:= 177;
		when 'PAVL' then item_code_w 	:= 236;		
		when 'IRIL' then item_code_w 	:= 242;
		when 'AVAL' then item_code_w 	:= 244;
		when 'DATL' then item_code_w 	:= 245;
		when 'REFL' then item_code_w 	:= 247;
		when 'AURL' then item_code_w 	:= 248;
		when 'FOTL' then item_code_w 	:= 254;
		when 'SOHL' then item_code_w 	:= 255;
		when 'CASL' then item_code_w 	:= 256;
		when 'MARL' then item_code_w 	:= 276;
		when 'ULTL' then item_code_w 	:= 277;
      		when 'ARTL' then item_code_w 	:= 298;
      		when 'ABRO' then item_code_w 	:= 302;
		else item_code_w := null;
	end case;
	
	RETURN item_code_w;

END get_code_by_reg_type;

FUNCTION get_reg_type (	item_code_p number ) RETURN varchar2 IS

reg_type_w pep_item_pendente.ie_tipo_registro%type;
	
BEGIN
	
	case item_code_p
		when 2 then reg_type_w 		:= 'ONL';
		when 6 then reg_type_w 		:= 'EOEL';
		when 7 then reg_type_w 		:= 'MOOL';
		when 8 then reg_type_w 		:= 'TPNL';
		when 9 then reg_type_w 		:= 'FNCL';
		when 10 then reg_type_w 	:= 'BICL';
		when 12 then reg_type_w 	:= 'ODL';
		when 13 then reg_type_w 	:= 'REOL';
		when 17 then reg_type_w 	:= 'DNPL';
		when 19 then reg_type_w 	:= 'AVNL';
		when 21 then reg_type_w 	:= 'CNL';
		when 23 then reg_type_w 	:= 'TAPL';
		when 24 then reg_type_w 	:= 'OCL';
		when 31 then reg_type_w 	:= 'OAL';
		when 69 then reg_type_w 	:= 'CTL';
		when 71 then reg_type_w 	:= 'AGRL';
		when 73 then reg_type_w 	:= 'GNOL';
		when 78 then reg_type_w 	:= 'BIOL';
		when 80 then reg_type_w 	:= 'MIEL';
		when 82 then reg_type_w 	:= 'PUPL';
		when 84 then reg_type_w 	:= 'PAQL';
		when 86 then reg_type_w 	:= 'MOCL';
		when 160 then reg_type_w 	:= 'OGL';
		when 168 then reg_type_w 	:= 'OIL';
		when 171 then reg_type_w 	:= 'CAPL';
		when 173 then reg_type_w 	:= 'TOGL';
		when 175 then reg_type_w 	:= 'CERL';
		when 177 then reg_type_w 	:= 'TOCL';
		when 236 then reg_type_w 	:= 'PAVL';
		when 242 then reg_type_w 	:= 'IRIL';
		when 244 then reg_type_w 	:= 'AVAL';
		when 245 then reg_type_w 	:= 'DATL';
		when 247 then reg_type_w 	:= 'REFL';
		when 248 then reg_type_w 	:= 'AURL';
		when 254 then reg_type_w 	:= 'FOTL';
		when 255 then reg_type_w 	:= 'SOHL';
		when 256 then reg_type_w 	:= 'CASL';
		when 276 then reg_type_w 	:= 'MARL';
		when 277 then reg_type_w 	:= 'ULTL';
      		when 298 then reg_type_w 	:= 'ARTL';
      		when 302 then reg_type_w 	:= 'ABRO';
		else reg_type_w := null;
	end case;
	
	RETURN reg_type_w;

END get_reg_type;

FUNCTION get_code_by_emr_item (	emr_item_p number ) RETURN number IS

item_code_w oftalmologia_item.nr_sequencia%type;
	
BEGIN
	
	case emr_item_p
		when 1 then item_code_w 	:= 12;
		when 5 then item_code_w 	:= 162;
		when 6 then item_code_w 	:= 15;
		when 7 then item_code_w 	:= 220;
		when 73 then item_code_w 	:= 16;
		when 74 then item_code_w 	:= 28;
		when 83 then item_code_w 	:= 2;
		when 93 then item_code_w 	:= 231;
		when 385 then item_code_w 	:= 75;
		else item_code_w := null;
	end case;
	
	RETURN item_code_w;

END get_code_by_emr_item;

END pkg_ophtalmology_item;

/