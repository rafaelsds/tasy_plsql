create or replace package bed_request_gantt_pck as

type gantt_record is record (
    cd_tipo_acomodacao  tipo_acomodacao.cd_tipo_acomodacao%type,
    text                tipo_acomodacao.ds_tipo_acomodacao%type,
    start_date          regra_reserva_vaga_acomod.dt_inicio%type,
    duracao             int, --alterar no front
    dt_inicio           regra_reserva_vaga_acomod.dt_inicio%type,
    dt_fim              regra_reserva_vaga_acomod.dt_fim%type,
    ie_dia_semana       regra_reserva_vaga_acomod.ie_dia_semana%type,
    qtd_total           regra_reserva_vaga_acomod.qt_vagas%type,
    qtd_ocup            int,
    progress            number,
    porcentagem         varchar2(255),
    porcentagem2        varchar2(255)
);

type gantt_table is table of gantt_record;

function get_data
    return gantt_table pipelined;

end bed_request_gantt_pck;
/


create or replace package body bed_request_gantt_pck as

function get_data
    return gantt_table pipelined is

    gantt_record_w gantt_record;
    nova_data_w DATE;
    qtd_dias_w NUMBER(4,0) := 0;
    qtd_pessoas_entrou_apu_w NUMBER(4,0) := 0;
    qtd_pessoas_entrou_gv_w int := 0;
    qtd_pessoas_saiu_w int := 0;
    qtd_pessoas_total_w int := 0;

    cursor regra_acomodacao is 
        SELECT 
            trunc(nvl(ra.dt_fim, sysdate) - ra.dt_inicio) as duration,
            ra.dt_inicio as DT_INICIO,
            nvl(ra.dt_fim, sysdate) as DT_FIM,
            ra.cd_tipo_acomodacao as cd_tipo_acomodacao,
            ra.IE_DIA_SEMANA AS IE_DIA_SEMANA,
            ra.qt_vagas AS qt_vagas,
            ta.ds_tipo_acomodacao text
        FROM TIPO_ACOMODACAO ta
        right join REGRA_RESERVA_VAGA_ACOMOD ra on ta.cd_tipo_acomodacao = ra.cd_tipo_acomodacao
        where ta.ie_situacao = 'A' and ra.dt_inicio is not null;

    begin
        for recordx in regra_acomodacao loop
            qtd_dias_w := 0;
            qtd_pessoas_entrou_apu_w := 0;
            qtd_pessoas_entrou_gv_w := 0;
            qtd_pessoas_saiu_w := 0;
            qtd_pessoas_total_w := 0;

            for x in qtd_dias_w .. recordx.duration loop
                nova_data_w := recordx.dt_inicio  + x;

                /*get number of people who entered through the single entrance*/ 
                SELECT count(apu.cd_tipo_acomodacao) into qtd_pessoas_entrou_apu_w FROM ATEND_PACIENTE_UNIDADE apu 
                where apu.cd_tipo_acomodacao = recordx.cd_tipo_acomodacao
                and to_char(apu.dt_entrada_unidade, 'DD/MM/YYYY') = nova_data_w;

                /*obtain the number of people who joined through the management of vacancies*/
                SELECT count(gv.nr_sequencia) into qtd_pessoas_entrou_gv_w FROM gestao_vaga gv 
                where gv.cd_tipo_acomod_desej = recordx.cd_tipo_acomodacao
                and to_char(gv.dt_prevista, 'DD/MM/YYYY') = nova_data_w;

                /*get amount of people who left*/
                SELECT count(apu.cd_tipo_acomodacao) into qtd_pessoas_saiu_w FROM ATEND_PACIENTE_UNIDADE apu 
                where apu.cd_tipo_acomodacao = recordx.cd_tipo_acomodacao
                and to_char(apu.dt_saida_unidade, 'DD/MM/YYYY') = nova_data_w;

                qtd_pessoas_total_w := (qtd_pessoas_total_w + qtd_pessoas_entrou_apu_w + qtd_pessoas_entrou_gv_w) - qtd_pessoas_saiu_w;

                gantt_record_w.cd_tipo_acomodacao := recordx.cd_tipo_acomodacao;
                gantt_record_w.text               := recordx.text;
                gantt_record_w.start_date         := nova_data_w;
                gantt_record_w.duracao            := 1;
                gantt_record_w.dt_inicio          := nova_data_w; 
                gantt_record_w.dt_fim             := nova_data_w; 
                gantt_record_w.IE_DIA_SEMANA      := recordx.IE_DIA_SEMANA; 
                gantt_record_w.qtd_total          := recordx.qt_vagas;
                gantt_record_w.qtd_ocup           := qtd_pessoas_total_w;
                gantt_record_w.progress           := ROUND(qtd_pessoas_total_w  / recordx.qt_vagas ,2);
                gantt_record_w.porcentagem        := qtd_pessoas_total_w  || '/' ||  recordx.qt_vagas;
                gantt_record_w.porcentagem2       := NVL(ROUND(((qtd_pessoas_total_w * 100) / recordx.qt_vagas),2),0) || '%';
                pipe row(gantt_record_w);

            end loop;
        end loop;

    return;
end get_data;

end bed_request_gantt_pck;
/