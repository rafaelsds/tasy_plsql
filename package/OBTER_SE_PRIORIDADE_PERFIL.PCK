CREATE OR REPLACE
function obter_se_prioridade_perfil (nr_seq_prioridade_p	number)
												return varchar2 is

/*
retorna 'S' se prioridade liberada para o perfil ativo
*/

ds_retorno_w varchar2(1) := 'N';
cd_perfil_w  number(5);
qt_perfil_w number(10) := 0;

cursor c01 is
select cd_perfil 
from   prioridade_regra_perfil
where  nr_seq_prioridade = nr_seq_prioridade_p;

begin
if	(nr_seq_prioridade_p is not null) then
	open c01;
	loop
	fetch c01 into	
		cd_perfil_w;
	exit when c01%notfound;
		begin
		
		qt_perfil_w := qt_perfil_w + 1;
		if (obter_perfil_ativo = cd_perfil_w) then
			ds_retorno_w := 'S';
		end if;
		
		end;
	end loop;
	close c01;
end if;

if (qt_perfil_w = 0) then
	ds_retorno_w := 'S';
end if;

return	ds_retorno_w;

end obter_se_prioridade_perfil;
/