CREATE OR REPLACE PACKAGE SECURITY_LEVEL_PCK AS
    TYPE SECURITY_LEVEL_TYPE IS RECORD ( nr_sequence NUMBER, ds_description VARCHAR2(255));
    TYPE SECURITY_LEVEL_TABLE_TYPE IS TABLE OF SECURITY_LEVEL_TYPE;
    PROCEDURE UPDATE_SECURITY_LEVEL(ie_last_action_p in USER_SECURITY_LEVEL.ie_last_action%type, NM_USUARIO_P in usuario.nm_usuario%type, ds_listrules IN VARCHAR2);
    FUNCTION FUNC_CHECK_LEVEL(nm_usuario_p IN VARCHAR2, cd_establecimiento_p IN VARCHAR2, ie_plataforma_p IN VARCHAR2) RETURN VARCHAR2;
    FUNCTION FUNC_LIST_SECURITY_LEVEL(ds_listrules IN VARCHAR2) RETURN SECURITY_LEVEL_TABLE_TYPE PIPELINED;
END SECURITY_LEVEL_PCK;
/

CREATE OR REPLACE PACKAGE BODY SECURITY_LEVEL_PCK AS
    TYPE invalid_list  IS TABLE OF NUMBER INDEX BY binary_integer;
    /**
     * Function to return a query with the invilid rules passed as parameters.
     * Example to call this function and return the rules 19, 16 and 2:
     * SELECT * FROM TABLE(SECURITY_LEVEL_PCK.FUNC_LIST_SECURITY_LEVEL('19,16,2'));
     */
    FUNCTION FUNC_LIST_SECURITY_LEVEL(
        ds_listrules IN VARCHAR2
    ) RETURN SECURITY_LEVEL_TABLE_TYPE PIPELINED IS
        l_row  SECURITY_LEVEL_TYPE;
        IDs    invalid_list;
        l_count     binary_integer;
        l_array     dbms_utility.lname_array;
    BEGIN
        IF LENGTH(ds_listrules) IS NOT NULL THEN
            dbms_output.put_line('There are invalid rules ...');
            dbms_utility.comma_to_table(list => regexp_replace(ds_listrules,'(^|,)','\1x'), tablen => l_count, tab => l_array);
            For i in 1 .. l_count Loop
                SELECT  nr_sequencia, OBTER_DESC_EXPRESSAO(cd_exp_description, DS_DESCRIPTION) ds_description 
                        INTO l_row.nr_sequence, l_row.ds_description
                    FROM    SECURITY_LEVEL_RULE
                    WHERE   nr_sequencia = To_Number(Substr(l_array(i),2));
                PIPE ROW (l_row);
            END LOOP;
        ELSE
            dbms_output.put_line('There are not invalid rules.');
        END IF;
        RETURN;
    END FUNC_LIST_SECURITY_LEVEL;

    /*
     * Function to return a list of invalid rules as String separate by commas
     */
    FUNCTION FUNC_CHECK_LEVEL(
        nm_usuario_p IN VARCHAR2, 
        cd_establecimiento_p IN VARCHAR2, 
        ie_plataforma_p IN VARCHAR2
    ) RETURN VARCHAR2 IS
        /**
         * Types used in the function
         */
        TYPE columns_record IS RECORD (nr_sequence_col NUMBER(10,0), ie_new_value NUMBER(10,0));
        TYPE invalid_table  IS TABLE OF columns_record INDEX BY binary_integer;
        /*
        * Cursor to return the list of security rules enabled
        */
        CURSOR security_rules_cur IS
            SELECT  nr_sequencia,
                    ds_sql,
                    dt_last_update,
                    ie_last_value
                FROM    security_level_rule
                WHERE   ie_active = 'A';
        /**
         * Variables used in the fucntion
         */
        listInvalidRules    VARCHAR2(255);
        agree_last_date_user DATE;
        close_last_date_user DATE;
        l_count             NUMBER;
        max_num_days        NUMBER;
        num_days            NUMBER;
        nr_sequence_last    NUMBER;
        ds_description_last VARCHAR2(255);
        sql_stmt            VARCHAR2(4000);
        rules_invalid_changed invalid_table;

        /*
        * Procedure to add the invalid rules detected in the validation that need to be updated as where modified
        */
        PROCEDURE add_invalid_changed(nr_sequence_p NUMBER, ie_new_value NUMBER) IS
            qt_registro_w NUMBER;
            new_record    NUMBER;
        BEGIN
            qt_registro_w := 0;
            FOR k IN 1..rules_invalid_changed.count LOOP
                IF (nr_sequence_p = rules_invalid_changed(k).nr_sequence_col) THEN
                    qt_registro_w := 1;
                    EXIT;
                END IF;
            END LOOP;

            IF (qt_registro_w = 0) THEN
                new_record := rules_invalid_changed.Count + 1;
                rules_invalid_changed(new_record).nr_sequence_col := nr_sequence_p;
                rules_invalid_changed(new_record).ie_new_value := ie_new_value;
            END IF;
        END;

        /**
        * Procedure to save the new values for rules those changed the value, only if are invalid.
        */
        PROCEDURE save_invalid IS
            pragma autonomous_transaction;
        BEGIN
            FOR i IN 1..rules_invalid_changed.Count LOOP
                DBMS_OUTPUT.put_line('Update rule: ' || rules_invalid_changed(i).nr_sequence_col || ' with value: ' || rules_invalid_changed(i).ie_new_value);
                UPDATE security_level_rule
                    SET     ie_last_value  = rules_invalid_changed(i).ie_new_value,
                            dt_last_update = sysdate
                    WHERE   nr_sequencia    = rules_invalid_changed(i).nr_sequence_col;
            END LOOP;
            COMMIT;
        END;

    BEGIN
        listInvalidRules := '';
        -- Verify last date where validation was acepted
        SELECT MAX(dt_last_update) INTO agree_last_date_user
            FROM user_security_level
            WHERE nm_usuario_reading = nm_usuario_p AND ie_last_action = 'A';
        DBMS_OUTPUT.put_line('Last date where validation was acepted: ' || agree_last_date_user);
        -- Verify last date where validation was closed
        SELECT MAX(dt_last_update) INTO close_last_date_user 
            FROM user_security_level
            WHERE nm_usuario_reading = nm_usuario_p AND ie_last_action = 'C';
        DBMS_OUTPUT.put_line('Last date where validation was closed: ' || close_last_date_user);
        
        -- Verify days from last option saved as agree after get number of days trunc the value
        SELECT sysdate - agree_last_date_user INTO num_days FROM dual;
        num_days := TRUNC(num_days, 0);
        DBMS_OUTPUT.put_line('Number of days from last validation acepted: ' || num_days);
        
        -- If last date closed does not exist or it is greater than last agree date calculate the max_num_days if not return 0
        IF close_last_date_user IS NULL OR TRUNC(close_last_date_user) <= TRUNC(agree_last_date_user) THEN
            -- Verify number of days to revalidate rules
            select MAX(QT_DAYS_VALIDATION) INTO max_num_days FROM PRIVACIDADE_PARAMETROS;
            DBMS_OUTPUT.put_line('Max number of days to revalidate the rules: ' || max_num_days);

            -- Verify if value is null or 0, in that case set to 90 days
            IF max_num_days IS NULL OR max_num_days = 0 THEN
                max_num_days := 90;
            END IF;
        ELSE
            -- This means that rules must be validated again
            max_num_days := 0;
        END IF;

        -- Look in the cursor with the enabled rules the query to execute and verify if it is valid or not
        FOR security_rules IN security_rules_cur LOOP
            -- Casting original SQL to include variables before execute it
            sql_stmt := security_rules.ds_sql;
            DBMS_OUTPUT.put_line('Original SQL: ' || sql_stmt);
            sql_stmt:=REPLACE(sql_stmt, ':IE_PLATAFORMA', '''' || ie_plataforma_p || '''');
            sql_stmt:=REPLACE(sql_stmt, ':NM_USUARIO', '''' || nm_usuario_p || '''');
            sql_stmt:=REPLACE(sql_stmt, ':CD_ESTABELECIMENTO', cd_establecimiento_p);
            DBMS_OUTPUT.put_line('SQL to execute: ' || sql_stmt);
            -- Executing query stored in the database
            EXECUTE IMMEDIATE sql_stmt INTO l_count;
            -- Validate if result is greatter than 0, so the rule is invalid
            IF l_count = 0 THEN
                -- If l_count is 0 the rule is invalid so need to verify if it is shown to user
                IF LENGTH(listInvalidRules) IS NOT NULL THEN
                    listInvalidRules := listInvalidRules || ',';
                END IF;
                listInvalidRules := listInvalidRules || security_rules.nr_sequencia;
                dbms_output.put_line('Rule is invalid');
            END IF;
            -- Validate if the value changed to update it in the database
            IF security_rules.ie_last_value IS NULL OR security_rules.ie_last_value != l_count THEN
                add_invalid_changed(security_rules.nr_sequencia, l_count);
                dbms_output.put_line('Rule changed value');
            END IF;
        END LOOP;

        -- Updating invalid rules with new values
        save_invalid;

        -- Return only if exists invalid rules modified or is a new user or expire the time to revalid the list
        IF rules_invalid_changed.Count > 0 OR agree_last_date_user IS NULL OR (num_days IS NOT NULL AND num_days > max_num_days) THEN
            RETURN listInvalidRules;
        ELSE
            RETURN '';
        END IF;
    END FUNC_CHECK_LEVEL;

    /*
     * Procedure to update the answer of user about the security level screen
     */
    PROCEDURE UPDATE_SECURITY_LEVEL(
        ie_last_action_p in USER_SECURITY_LEVEL.ie_last_action%type,
        NM_USUARIO_P in usuario.nm_usuario%type,
        ds_listrules IN VARCHAR2
    ) IS
        seqUser     NUMBER;
        seqUserRule NUMBER;
        IDs         invalid_list;
        l_count     binary_integer;
        l_array     dbms_utility.lname_array;
    BEGIN
        IF  (ie_last_action_p is not null) AND LENGTH(ds_listrules) IS NOT NULL THEN
            dbms_utility.comma_to_table(list => regexp_replace(ds_listrules,'(^|,)','\1x'), tablen => l_count, tab => l_array);
            SELECT USER_SECURITY_LEVEL_SEQ.NEXTVAL into seqUser from dual;
            INSERT INTO USER_SECURITY_LEVEL(NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NM_USUARIO_READING, DT_LAST_UPDATE, IE_LAST_ACTION)
                VALUES(seqUser, sysdate, NM_USUARIO_P, null, null, NM_USUARIO_P, sysdate, ie_last_action_p);
            For i in 1 .. l_count Loop
                SELECT USER_SEC_LEVEL_RULES_SEQ.NEXTVAL into seqUserRule from dual;
                INSERT INTO USER_SEC_LEVEL_RULES(NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NR_SEQ_USER_SEC_LEVEL, NR_SEQ_SEC_LEVEL_RULE)
                    VALUES(seqUserRule, sysdate, NM_USUARIO_P, null, null, seqUser, To_Number(Substr(l_array(i),2)));
            END LOOP;
        END IF;

        IF (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') THEN
            commit;
        END IF;
    END UPDATE_SECURITY_LEVEL;

END SECURITY_LEVEL_PCK;
/