create or replace package nais_nutrition_pck
as
    procedure nais_common_header(
		message_type_p   varchar2,
		cd_classif_p     varchar2,
		cd_contin_flag_p varchar2,
		cd_estab_p       varchar2,
		nr_tele_length_p number );
    procedure nais_common_body(
        nr_atendimento_p varchar2);
    procedure meals_calculation(
		nr_atendimento_p   varchar2,
		ds_file_output_p out clob      
      );
end nais_nutrition_pck;
/

create or replace package body nais_nutrition_pck
as
	ds_line_w           varchar2(32767);
	nm_usuario_w        varchar2(20) := 'NAIS';
	ds_message_w        varchar2(4000);
	ds_error_w          varchar2(4000);
	filename_sequence_w varchar2(255);
  json_output_list_w philips_json_list;
  json_output_w philips_json;
  /**
  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  json_
  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  **/
procedure add_json_value(
    json_p  in out nocopy philips_json,
    name_p  varchar2,
    value_p varchar2)
is
    begin
		if (value_p is not null) then
		    json_p.put(name_p,value_p);
		end if;
end add_json_value;

procedure headerBody
		(
		  nr_atendimento_p varchar2
		)
is
    begin
    nais_common_header('NS','01', 'E',1,162); 		
	nais_common_body(nr_atendimento_p);		
    if(length(ds_line_w) > 64)then        
        add_json_value(json_output_w, 'message', ds_line_w);
        json_output_list_w.append(json_output_w.to_json_value());
    end if;   
  end headerBody;

procedure append_text(
		ds_text_p       varchar2,
		qt_size_p       number,
		ie_left_rigth_p varchar2 default 'L',
		ds_default_p    varchar2 default ' ' )
is
    begin
		if ( ie_left_rigth_p = 'R' ) then
		    ds_line_w := ds_line_w || rpad(ds_text_p, qt_size_p, ds_default_p);
		else
		    ds_line_w := ds_line_w || lpad(ds_text_p, qt_size_p, ds_default_p);
		end if;
end append_text;

procedure meals_calculation(
        nr_atendimento_p   varchar2,
        ds_file_output_p out clob
)
    is
    -- oral diet and special meals
    	
    cursor c01
    is
		select distinct a.nr_atendimento nr_atendimento
		from    atendimento_paciente a,
			    prescr_medica b,
			    nut_atend_serv_dia_rep d,
			    nut_atend_serv_dia e
		where  a.ie_tipo_atendimento = 1
		and    a.nr_atendimento        = b.nr_atendimento
		and    b.nr_prescricao         in (d.nr_prescr_jejum,d.nr_prescr_oral)
		and    a.dt_alta               is null     --without discharge
		and    a.dt_cancelamento       is null     -- encouter not cancelled
		and    b.dt_suspensao          is null     -- prescription not cancelled
		and    b.dt_liberacao          is not null -- prescription released
		and    e.nr_sequencia          = d.nr_seq_serv_dia
		and    trunc(e.dt_servico)     = trunc(sysdate)		
        union all	
		select distinct a.nr_atendimento
		from   atendimento_paciente a,
		       cpoe_dieta c
		where  a.ie_tipo_atendimento   = 1
		and    a.nr_atendimento        = c.nr_atendimento
		and    a.dt_alta              is null --without discharge
		and    a.dt_cancelamento      is null -- encouter not cancelled
		and    trunc(c.dt_inicio)      = trunc(sysdate)
		and    c.dt_liberacao         is not null;
    r_c01_w c01%rowtype;
	
  begin
    ds_line_w          := null;
    json_output_w      := philips_json();
    json_output_list_w := philips_json_list(); 
	
    if(nr_atendimento_p is null) then
		open c01;	
			loop
				fetch c01 into r_c01_w;
				exit
				when c01%notfound;    
				headerBody(r_c01_w.nr_atendimento);
			end loop;    
		close c01;
    else    
        headerBody(nr_atendimento_p);
    end if;
    dbms_lob.createtemporary( ds_file_output_p, true);
    json_output_list_w.to_clob(ds_file_output_p);	
end meals_calculation;
  
	procedure nais_common_body
		(
		  nr_atendimento_p varchar2
		)
	is
    department_code_w bft_encounter_v.medical_department_id%type;
    date_of_admission_w bft_encounter_v.encouter_admit_date%type;
    patient_id_w bft_encounter_v.patient_id%type;
    json_output_list_w philips_json_list;
    json_output_w philips_json;
    cursor c02
    is
        select  count(*) over() totalcount,
		        '01' update_class,
				'01' processing_class,
				nvl(b.patient_id, 0) patient_id,
				nvl(b.medical_department_id,'01') calculation_department_code,
				to_char(c.dt_inicio,'yyyymmdd') calculation_start_date,
				nvl(get_day_clasification(c.nr_atendimento,sysdate),'0') on_the_day_classification,
				nvl(get_day_clasification(c.nr_atendimento,sysdate+1),'0') next_day_classification,
				nvl(decode(c.ie_duracao,'P', to_char(last_day(c.dt_inicio),'yyyymmdd'),to_char(c.dt_fim,'yyyymmdd')),99999999) calculation_end_date,
				nvl(get_day_clasification(c.nr_atendimento,nvl(decode(c.ie_duracao,'P', last_day(c.dt_inicio),c.dt_fim),sysdate)),'0') end_date_classification,
				get_day_meal_count(c.nr_atendimento,sysdate,'L') liquid_mealcount_on_the_day,
				get_day_meal_count(c.nr_atendimento,sysdate+1,'L') liquid_mealcount_next_day,
				get_day_meal_count(c.nr_atendimento,nvl(decode(c.ie_duracao,'P', last_day(c.dt_inicio),c.dt_fim),sysdate),'L') liquid_mealcount_end_day,
				get_day_meal_count(c.nr_atendimento,sysdate,'M') on_the_day_mealcount,
				get_day_meal_count(c.nr_atendimento,sysdate,'S') on_the_day_special_mealcount,
				get_day_meal_count(c.nr_atendimento,sysdate+1,'M') next_day_meal_count,
				get_day_meal_count(c.nr_atendimento,sysdate+1,'S') next_day_special_meal_count,
				get_day_meal_count(c.nr_atendimento,nvl(decode(c.ie_duracao,'P', last_day(c.dt_inicio),c.dt_fim),sysdate),'M') end_day_meal_coount,
				get_day_meal_count(c.nr_atendimento,nvl(decode(c.ie_duracao,'P', last_day(c.dt_inicio),c.dt_fim),sysdate),'S') end_day_special_mealcount,
				nvl(to_char(b.encouter_admit_date, 'yyyymmdd'),' ') date_of_admission,
				nvl(to_char(b.encouter_admit_date, 'hh24mi'),' ') time_of_admission,
				'00' mnn_classification,
				' ' spare,
				' ' eot_two
        from    cpoe_dieta c ,
                bft_encounter_v b
        where   c.nr_atendimento   = b.nr_atendimento
        and     trunc(c.dt_inicio) = trunc(sysdate)
        and     c.dt_liberacao    is not null
        and     c.dt_suspensao    is null
        and     c.nr_atendimento   = nr_atendimento_p
        and     rownum             = 1
        order by dt_atualizacao asc;
		
    r_c02_w c02%rowtype;
    begin
    json_output_w      := philips_json();
    json_output_list_w := philips_json_list();
    open c02;
		loop
		    fetch c02 into r_c02_w;
		exit 
		when c02%notfound;
		    append_text(r_c02_w.update_class, 2, 'L','0');
			append_text(r_c02_w.processing_class, 2, 'L','0');
			append_text(r_c02_w.patient_id, 10, 'L','0');
			append_text(r_c02_w.calculation_department_code, 2, 'L','0');
			append_text(r_c02_w.calculation_start_date, 8, 'L');
			append_text(r_c02_w.on_the_day_classification, 2, 'L');
			append_text(r_c02_w.next_day_classification, 2, 'L');
			append_text(r_c02_w.calculation_end_date, 8, 'L');
			append_text(r_c02_w.end_date_classification, 2, 'L');
			append_text(' ', 1, 'L');
			append_text(r_c02_w.liquid_mealcount_on_the_day, 2, 'L','0');
			append_text(r_c02_w.liquid_mealcount_next_day, 2, 'L','0');
			append_text(r_c02_w.liquid_mealcount_end_day, 2, 'L','0');
			append_text(r_c02_w.on_the_day_mealcount, 1, 'L');
			append_text(r_c02_w.on_the_day_special_mealcount, 1, 'L');
			append_text(r_c02_w.next_day_meal_count, 1, 'L');
			append_text(r_c02_w.next_day_special_meal_count, 1, 'L');
			append_text(r_c02_w.end_day_meal_coount, 1, 'L' );
			append_text(r_c02_w.end_day_special_mealcount, 1, 'L');
			append_text(r_c02_w.date_of_admission, 8, 'L');
			append_text(r_c02_w.time_of_admission, 4, 'L');
			append_text(r_c02_w.mnn_classification, 1, 'L','0');
			append_text(r_c02_w.spare, 33, 'R', ' ');
			append_text(r_c02_w.eot_two, 1, 'L');
        end loop;
    close c02;
end nais_common_body;
  
procedure nais_common_header(
		message_type_p   varchar2,
		cd_classif_p     varchar2,
		cd_contin_flag_p varchar2,
		cd_estab_p       varchar2,
		nr_tele_length_p number 
	)
is
    cursor c01
    is
        select 'H' system_code,
			message_type_p message_type,
			cd_contin_flag_p continuation_flag,
			'M' destination_code,
			'D' origin_code,
			sysdate processing_date,
			sysdate processing_time,
			' ' workstation_name,
			' ' user_number,
			cd_classif_p processing_classification,
			' ' response_type,
			nr_tele_length_p message_length,
			' ' eot,
			' ' medical_institution_code,
			' ' blank
    from dual;
    begin
    ds_line_w := null;
    generate_int_serial_number(1, message_type_p, cd_estab_p, nm_usuario_w, filename_sequence_w,162);
    for r_c01 in c01
    loop
        begin
			append_text(to_char(filename_sequence_w), 5, 'L', '0');
			append_text(r_c01.system_code, 1, 'R', ' ');
			append_text(r_c01.message_type, 2, 'R', ' ');
			append_text(r_c01.continuation_flag, 1, 'R', ' ');
			append_text(r_c01.destination_code, 1, 'R', ' ');
			append_text(r_c01.origin_code, 1, 'R', ' ');
			append_text(to_char(r_c01.processing_date, 'yyyymmdd'), 8, 'L', '0');
			append_text(to_char(r_c01.processing_time,'hh24miss'),6, 'L', '0');
			append_text(r_c01.workstation_name, 8, 'R', ' ');
			append_text(r_c01.user_number, 8, 'R', ' ');
			append_text(r_c01.processing_classification, 2, 'R', ' ');
			append_text(r_c01.response_type, 2, 'R', ' ');
			append_text(r_c01.message_length, 5, 'L', '0');
			append_text(r_c01.eot, 1, 'R', ' ');
			append_text(r_c01.medical_institution_code, 2, 'R', ' ');
			append_text(r_c01.blank, 11, 'R', ' ');
        end;
    end loop;
end nais_common_header;    
 
end nais_nutrition_pck;
/
