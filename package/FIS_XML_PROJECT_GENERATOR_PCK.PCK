create or replace 
package fis_xml_project_generator_pck as

	procedure fis_start_project_generation(	name_p 			in		varchar2, 
											header_p 		in		varchar2, 
											definition_p 	in		varchar2, 
											username_p 		in		varchar2,
											content_p		in		varchar2,
											nr_seq_proj_p 	out 	varchar2);

end fis_xml_project_generator_pck;
/

create or replace 
package body fis_xml_project_generator_pck as
	
	type attribute_type is record (	ds_attribute	varchar2(100),
									seq_attribute	varchar2(255) );							
	type reference_attributes is table of attribute_type index by binary_integer;
	vector_attributes_w reference_attributes;
	count_attributes number(10) := 0;
		

	/*
		Valid string list content
		vector_p 	 - string list content
		ds_content_p - list description
	*/
	procedure validate_vector(	vector_p dbms_sql.varchar2_table,
								ds_content_p varchar2 ) is
	begin
		if	(vector_p is null) or (vector_p.count = 0) then
			raise_application_error(-20011, ds_content_p || ' conversion error!'); 
		end if;
	end validate_vector;					
	
	/*
		Checks for an attribute in the list. 
		If you have reference to the element to send the structure 
		of XML and, if not, it is because the element is root.
	*/
	procedure get_attribute(ds_tag_p  varchar2,
							seq_tag_p out varchar2) is
							
	seq_tag_w 				varchar2(255);
	nr_seq_atrib_elem_w 	xml_atributo.nr_seq_atrib_elem%type;
	
	begin
		seq_tag_w := null;
		
		for i in 1 .. vector_attributes_w.count loop
			begin
				if (ds_tag_p = vector_attributes_w(i).ds_attribute) then
					seq_tag_w :=  vector_attributes_w(i).seq_attribute;
												
					select 	max(nr_seq_atrib_elem)
					into	nr_seq_atrib_elem_w
					from 	xml_atributo 
					where 	nr_sequencia = seq_tag_w;
					
					if (nr_seq_atrib_elem_w is null) then 
						exit;
					end if;
					
					seq_tag_w := null;
					
				end if;
			end;
		end loop;
		
		seq_tag_p := seq_tag_w;
		
	end get_attribute;
	
	/*
		Previous attribute update assigning to child element
	*/
	procedure update_attribute_prev(nr_seq_elemnt_p	varchar2,
									seq_attr_prev_p varchar2) is
	begin
		
		update 	xml_atributo 
		set 	nr_seq_atrib_elem = nr_seq_elemnt_p
		where 	nr_sequencia = seq_attr_prev_p;
		
		commit;
		
	end;
	
	/*
		Built-in routine for xml project insertion
	*/
	procedure create_project(	name_p 			varchar2, 
								header_p 		varchar2, 
								definition_p 	varchar2, 
								username_p 		varchar2,
								nr_seq_proj_p	out varchar2 ) is
								
	nr_seq_proj_w	xml_projeto.nr_sequencia%type;
		
	begin
		select xml_projeto_seq.nextval
		into nr_seq_proj_w
		from dual;

		insert into xml_projeto ( 	nr_sequencia, 
									ds_projeto, 
									dt_atualizacao, 
									nm_usuario, 
									dt_atualizacao_nrec, 
									nm_usuario_nrec, 
									ds_cabecalho, 
									ds_observacao, 
									ie_tipo_complexo, 
									nr_seq_proj_complexo, 
									ie_hosp_pls, 
									ds_versao, 
									ie_tiss, 
									ie_situacao, 
									ie_gerar_quebra 						
						) values (	nr_seq_proj_w, 
									name_p,  
									sysdate, 
									username_p,  
									sysdate, 
									username_p,
									header_p, 
									definition_p, 
									'N', 
									null, 
									'H', 
									null, 
									'S', 
									'A', 
									null ); 
		commit;
		
		nr_seq_proj_p := nr_seq_proj_w;
		
	end create_project;
							
	procedure create_element(	nr_seq_apres_p 	 number,
								nr_seq_proj_p 	 varchar2,
								ds_element_p	 varchar2,
								username_p		 varchar2,
								nr_seq_element_p out varchar2) is
	
	seq_attr_prev_w 	varchar2(255);								
	nr_seq_element_w	xml_elemento.nr_sequencia%type;
		
	begin
		get_attribute(ds_element_p, seq_attr_prev_w);
		
		select xml_elemento_seq.nextval
		into nr_seq_element_w
		from dual;
		
		insert into xml_elemento ( 	nr_sequencia, 
									nr_seq_apresentacao, 
									nr_seq_projeto, 
									nm_elemento, 
									ds_elemento, 
									ds_cabecalho, 
									ds_sql, 
									ds_grupo, 
									nm_usuario, 
									dt_atualizacao, 
									ie_criar_nulo,
									nm_usuario_nrec, 
									dt_atualizacao_nrec, 
									ie_tipo_elemento, 
									ie_criar_elemento, 
									ie_tipo_complexo, 
									ds_sql_2, 
									ds_namespace, 
									qt_min_ocor, 
									qt_max_ocor, 
									nr_seq_item_xsd 
						) values ( 	nr_seq_element_w, 
									nr_seq_apres_p, 
									nr_seq_proj_p, 
									ds_element_p, 
									ds_element_p, 
									null, 
									'', 
									null, 
									username_p,  
									sysdate,
									'S', 
									username_p,
									sysdate, 
									'E', 
									'S', 
									'S', 
									null, 
									null, 
									null, 
									null, 
									null ); 
		commit;
		
		update_attribute_prev(nr_seq_element_w, seq_attr_prev_w);
		nr_seq_element_p := nr_seq_element_w;
		
	end create_element;
							
	procedure create_attribute(	nr_seq_element_p 	varchar2,
								nr_seq_apres_p 		number,
								ds_attribute_p 		varchar2,
								username_p			varchar2 ) is						
	
	nr_seq_attribute_w	xml_atributo.nr_sequencia%type;
	
	begin		
		count_attributes := count_attributes + 1;
		
		select xml_atributo_seq.nextval
		into nr_seq_attribute_w
		from dual;
		
		vector_attributes_w(count_attributes).ds_attribute  := ds_attribute_p;
		vector_attributes_w(count_attributes).seq_attribute := nr_seq_attribute_w;
				
		insert into xml_atributo ( 	nr_sequencia, 
									nr_seq_elemento, 
									nr_seq_apresentacao, 
									nm_atributo_xml, 
									nm_atributo, 
									ie_criar_nulo, 
									ie_obrigatorio, 
									ie_tipo_atributo, 
									ds_mascara, 
									nm_usuario_nrec, 
									dt_atualizacao_nrec, 
									nm_usuario, 
									dt_atualizacao, 
									nr_seq_atrib_elem, 
									ie_criar_atributo, 
									ds_cabecalho, 
									ie_controle_pb, 
									ie_remover_espaco_branco, 
									ds_namespace, 
									nm_tabela_def_banco, 
									nm_atributo_xml_pesquisa, 
									cd_exp_observacao, 
									nm_tabela_base, 
									nm_atributo_base, 
									qt_tamanho, 
									qt_decimais, 
									cd_dominio, 
									nr_seq_item_xsd, 
									ie_atrib_cdata 
						) values ( 	nr_seq_attribute_w, 
									nr_seq_element_p, 
									nr_seq_apres_p, 
									ds_attribute_p, 
									ds_attribute_p, 
									'N', 
									'N', 
									null, 
									null, 
									username_p,  
									sysdate, 
									username_p,  
									sysdate,
									null, 
									'S', 
									null, 
									'N', 
									null, 
									null, 
									null, 
									ds_attribute_p, 
									null, 
									null, 
									null, 
									null, 
									null, 
									null, 
									null, 
									null ); 
		commit;
		
	end create_attribute;
	
	procedure fis_generate_project(	name_p 				varchar2, 
									header_p 			varchar2, 
									definition_p 		varchar2, 
									username_p 			varchar2,
									content_p			varchar2,
									nr_seq_proj_p out 	varchar2,
									sep_list_p			varchar2 default '|',
									sep_data_p			varchar2 default '>',
									sep_subdata_p		varchar2 default ',') is
		
	content_w 		dbms_sql.varchar2_table;
	data_w 			dbms_sql.varchar2_table;
	attributes_w	dbms_sql.varchar2_table;
	nr_seq_elem_w	xml_elemento.nr_sequencia%type;
	attribute_w		varchar2(15);
	nr_seq_proj_w	xml_projeto.nr_sequencia%type;
		
	begin		
		/* xml project creation */
		create_project(name_p, header_p, definition_p, username_p, nr_seq_proj_w);
	
		/* separation of element tags */
		content_w := obter_lista_string(content_p, sep_list_p);
		validate_vector(content_w, 'Content');
		
		for	i in 1 .. content_w.count loop
			begin
				/* separation of element tags and attribute tags */
				data_w	:= obter_lista_string(content_w(i), sep_data_p);
				validate_vector(data_w, 'Data');
				create_element(i, nr_seq_proj_w, data_w(1), username_p, nr_seq_elem_w);
				
				/*	
					separation of attributes tags
					data_w(2) == List of attribute
				*/
				attributes_w := obter_lista_string(data_w(2), sep_subdata_p); 
				validate_vector(attributes_w, 'Subdata');
						
				for	j in 1 .. attributes_w.count loop
					begin
						create_attribute(nr_seq_elem_w, j, attributes_w(j), username_p);					
					end;
				end loop;
			end;
		end loop;
		
		nr_seq_proj_p := nr_seq_proj_w;
		
	end fis_generate_project;
	
	/*
		Main routine for separating elements and 
		attributes for project generation xml
	*/
	procedure fis_start_project_generation(	name_p 			in		varchar2, 
											header_p 		in		varchar2, 
											definition_p 	in		varchar2, 
											username_p 		in		varchar2,
											content_p		in		varchar2,
											nr_seq_proj_p 	out 	varchar2) is
	
	nr_seq_proj_w	xml_projeto.nr_sequencia%type;
	
	begin
	
	fis_generate_project(name_p, header_p, definition_p, username_p, content_p, nr_seq_proj_w);
	nr_seq_proj_p := nr_seq_proj_w;
	
	end fis_start_project_generation;
	
end fis_xml_project_generator_pck;
/
