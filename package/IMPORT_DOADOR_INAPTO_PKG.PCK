create or replace package Import_Doador_Inapto_pkg is

  type t_Doadores is record(nm_pessoa_fisica  san_doador_inapto.nm_pessoa_fisica %Type,
                            ie_sexo    san_doador_inapto.ie_sexo %Type,
                            dt_nascimento  san_doador_inapto.dt_nascimento %Type,
                            ie_definitivo  san_doador_inapto.ie_definitivo %Type,
                            dt_limite_inaptidao  san_doador_inapto.dt_limite_inaptidao %Type,
                            dt_geracao_arquivo  san_doador_inapto.dt_geracao_arquivo %Type default null,
                            nm_usuario  san_doador_inapto.nm_usuario %Type
                           );
  type t_listaDoadores is table of t_Doadores INDEX BY BINARY_INTEGER;

  procedure inserirDoadoresInaptos(listaInaptos t_listaDoadores);

end Import_Doador_Inapto_pkg;
/
create or replace package body Import_Doador_Inapto_pkg is

       procedure inserirDoadoresInaptos(listaInaptos t_listaDoadores) as
         listaInaptosaAux t_listaDoadores;
         cursor c01 is
           select * from
              table(listaInaptos);

       begin

         if  listaInaptos.first is not null then
               open c01;
               loop
                 fetch c01 bulk collect into listaInaptosaAux limit 1000;
                 forall i in listaInaptosaAux.first .. listaInaptosaAux.last
                 insert into os_1802650 (nr_sequencia,
                                         dt_atualizacao,
                                         nm_usuario,
                                         cd_pessoa_fisica,
                                         nm_pessoa_fisica,
                                         ie_sexo,
                                         dt_nascimento,
                                         ie_definitivo,
                                         dt_limite_inaptidao,
                                         dt_geracao_arquivo)
                                 values (os_1802650_sq.nextval,
                                         sysdate,
                                         listaInaptosaAux(i).nm_usuario,
                                         null,
                                         listaInaptosaAux(i).nm_pessoa_fisica,
                                         listaInaptosaAux(i).ie_sexo,
                                         listaInaptosaAux(i).dt_nascimento,
                                         listaInaptosaAux(i).ie_definitivo,
                                         listaInaptosaAux(i).dt_limite_inaptidao,
                                         listaInaptosaAux(i).dt_geracao_arquivo
                                         );
                 commit;
                 exit when listaInaptosaAux.count = 0;
               end loop;
               close c01;
         end if;

       end inserirDoadoresInaptos;

end Import_Doador_Inapto_pkg;
/
