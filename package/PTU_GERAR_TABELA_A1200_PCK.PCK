create or replace
package ptu_gerar_tabela_A1200_pck as 

-- dados da regra do pacote
type dados_reg is record (	nr_sequencia		ptu_pacote_reg.nr_sequencia%type,
				nr_seq_pacote		ptu_pacote_reg.nr_seq_pacote%type,
				cd_unimed_prestador	ptu_pacote_reg.cd_unimed_prestador%type,
				cd_prestador		ptu_pacote_reg.cd_prestador%type,
				nm_prestador		ptu_pacote_reg.nm_prestador%type,
				dt_negociacao		ptu_pacote_reg.dt_negociacao%type,
				dt_publicacao		ptu_pacote_reg.dt_publicacao%type
			);

-- dados dos servi�os do pacote			
type dados_servico is record (
				ie_tipo_tabela		pls_util_cta_pck.t_number_table,
				cd_servico		pls_util_cta_pck.t_number_table,
				ie_honorario		pls_util_cta_pck.t_varchar2_table_1,
				ie_principal		pls_util_cta_pck.t_number_table, 	
				ie_tipo_item		pls_util_cta_pck.t_number_table,
				nr_seq_pacote_reg	pls_util_cta_pck.t_number_table,
				vl_servico		pls_util_cta_pck.t_number_table
			);

-- Gera A120 para AMB
procedure gerar_a1200_amb (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type);
	
--Gera A1200 para material	
procedure gerar_a1200_mat (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type);

-- Gera A1200 para servi�o				
procedure gerar_a1200_serv (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type);

-- Converte o tipo de despesa conforme tabela de dom�nio				
function obter_tipo_item (	ie_tipo_despesa_p	varchar2,
				ie_item_p		varchar2) 
			return number;

-- Obter o pre�o vigente do material			
function obter_preco_material (	nr_seq_preco_item_p	pls_material_preco_item.nr_sequencia%type)
 		    	return pls_material_valor_item.vl_material%type;

end ptu_gerar_tabela_A1200_pck;
/

create or replace
package body ptu_gerar_tabela_A1200_pck as 

-- Deleta regras que ficaram sem itens
procedure deleta_reg_sem_item (	nr_seq_pacote_p		ptu_pacote.nr_sequencia%type) is 

qt_registro_w	pls_integer;

begin
-- Feito conforme era realizado na procedure ptu_gerar_tabela_A1200
select	count(1)
into	qt_registro_w
from	ptu_pacote_servico	a,
	ptu_pacote_reg		b
where	b.nr_sequencia	= a.nr_seq_pacote_reg
and	b.nr_seq_pacote	= nr_seq_pacote_p;

delete	ptu_pacote_reg x
where	x.nr_seq_pacote	= nr_seq_pacote_p
and	not exists	(select	1
			from	ptu_pacote_servico a
			where	x.nr_sequencia	= a.nr_seq_pacote_reg);

if	(qt_registro_w = 0) then
	delete	ptu_pacote_reg
	where	nr_seq_pacote	= nr_seq_pacote_p;
end if;

end deleta_reg_sem_item;

-- Insere a regra do pacote
procedure insere_pacote_reg (	dados_reg_p	in out	dados_reg,
				nm_usuario_p	in	usuario.nm_usuario%type) is 

begin

insert	into	ptu_pacote_reg
	(	nr_sequencia, nr_seq_pacote, cd_unimed_prestador,
		cd_prestador, nm_prestador, dt_negociacao,
		dt_atualizacao, nm_usuario, dt_publicacao,
		ds_observacao, ie_tipo_informacao, nm_usuario_nrec,
		dt_atualizacao_nrec, ie_tipo_internacao, vl_tot_taxas,
		vl_tot_diarias, vl_tot_gases, vl_tot_mat,
		vl_tot_med, vl_tot_proc, vl_tot_opme,
		vl_tot_pacote, ie_honorario)
values	(	ptu_pacote_reg_seq.nextval, dados_reg_p.nr_seq_pacote, dados_reg_p.cd_unimed_prestador,
		dados_reg_p.cd_prestador, dados_reg_p.nm_prestador, dados_reg_p.dt_negociacao,
		sysdate, nm_usuario_p, dados_reg_p.dt_publicacao,
		null, 2, nm_usuario_p,
		sysdate, null, 0,
		0, 0, 0,
		0, 0, 0,
		0, 'N') returning nr_sequencia into dados_reg_p.nr_sequencia;

end insere_pacote_reg;

-- Insere os servi�os do pacote, no primeiro momento passa para limpar as vari�veis
procedure insere_pacote_servico (	dados_servico_p		in out	dados_servico,
					nm_usuario_p		in	usuario.nm_usuario%type) is 
					
begin

if	(dados_servico_p.ie_tipo_tabela.count > 0) then
	
	forall i in dados_servico_p.ie_tipo_tabela.first .. dados_servico_p.ie_tipo_tabela.last
		insert	into	ptu_pacote_servico
			(	nr_sequencia, ie_tipo_tabela, cd_servico,
				ie_honorario, ie_principal, ie_tipo_item,
				dt_atualizacao, nm_usuario, nr_seq_pacote_reg,
				vl_servico, nm_usuario_nrec, dt_atualizacao_nrec)
		values	(	ptu_pacote_servico_seq.nextval, dados_servico_p.ie_tipo_tabela(i), dados_servico_p.cd_servico(i),
				dados_servico_p.ie_honorario(i), dados_servico_p.ie_principal(i), dados_servico_p.ie_tipo_item(i),
				sysdate, nm_usuario_p, dados_servico_p.nr_seq_pacote_reg(i),
				dados_servico_p.vl_servico(i), nm_usuario_p, sysdate);
	commit;
end if;

dados_servico_p.ie_tipo_tabela.delete;
dados_servico_p.cd_servico.delete;
dados_servico_p.ie_honorario.delete;
dados_servico_p.ie_principal.delete;
dados_servico_p.ie_tipo_item.delete;
dados_servico_p.nr_seq_pacote_reg.delete;
dados_servico_p.vl_servico.delete;

commit;

end insere_pacote_servico;

-- Insere o pacote na tabela
procedure insere_pacote_a1200(	cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
				nm_usuario_p			usuario.nm_usuario%type,
				nr_seq_ptu_pacote_p	out	ptu_pacote.nr_sequencia%type,
				cd_unimed_origem_p	out	pls_congenere.cd_cooperativa%type) is

cd_unimed_origem_w	pls_congenere.cd_cooperativa%type;
nr_seq_congenere_w	pls_congenere.nr_sequencia%type;
cd_interface_w		ptu_regra_interface.cd_interface%type;
cd_versao_w		varchar2(5);
nr_versao_transacao_w	varchar2(5);

begin
-- Unimed de Origem
cd_unimed_origem_w 	:= pls_obter_unimed_estab(cd_estabelecimento_p);
nr_seq_congenere_w 	:= pls_obter_cd_seq_congenere(lpad(cd_unimed_origem_w,4,'0'),'NR');
cd_interface_w 		:= nvl(pls_obter_interf_ptu( cd_estabelecimento_p, nr_seq_congenere_w, sysdate, 'A1200'), '2183');
cd_versao_w		:= ptu_obter_versao('A1200',cd_interface_w);
nr_versao_transacao_w	:= ptu_obter_versao_transacao('A1200',cd_versao_w);

insert	into	ptu_pacote
	(	nr_sequencia, cd_unimed_origem, dt_atualizacao,
		dt_geracao, ie_tipo_carga, ie_tipo_informacao,         
		cd_estabelecimento, nm_usuario, nm_usuario_nrec,
		dt_atualizacao_nrec, nr_versao_transacao)
values	(	ptu_pacote_seq.nextval, cd_unimed_origem_w, sysdate,               
		sysdate, 1, 2,
		cd_estabelecimento_p, nm_usuario_p, nm_usuario_p,
		sysdate, nr_versao_transacao_w) returning nr_sequencia into nr_seq_ptu_pacote_p;

cd_unimed_origem_p := cd_unimed_origem_w;
		
end insere_pacote_a1200;

-- Obtem o �ltimo valor vigente do material
function obter_preco_material (	nr_seq_preco_item_p	pls_material_preco_item.nr_sequencia%type)
 		    	return pls_material_valor_item.vl_material%type is
			
vl_material_w	pls_material_valor_item.vl_material%type;

begin

select	max(vl_material)
into	vl_material_w
from	pls_material_valor_item
where	nr_seq_preco_item = nr_seq_preco_item_p
and	dt_inicio_vigencia = (	select	max(dt_inicio_vigencia)
				from	pls_material_valor_item
				where	nr_seq_preco_item = nr_seq_preco_item_p
				and	dt_inicio_vigencia < sysdate);

return	vl_material_w;

end obter_preco_material;

-- Converte o tipo de despesa Tasy para o tipo de item Unimed
function obter_tipo_item (	ie_tipo_despesa_p	varchar2,
				ie_item_p		varchar2) 
			return number is

ie_tipo_item_w		number(1);
				
begin

if	(ie_item_p = 'P') then
	
	if	(ie_tipo_despesa_p = 3) then
		ie_tipo_item_w := 2;
	elsif	(ie_tipo_despesa_p = 2) then
		ie_tipo_item_w := 1;
	else
		ie_tipo_item_w := 6;
	end if;
	
elsif	(ie_item_p = 'M') then

	if	(ie_tipo_despesa_p = 1) then
		ie_tipo_item_w := 3;
	elsif	(ie_tipo_despesa_p = 2) then
		ie_tipo_item_w := 5;
	elsif	(ie_tipo_despesa_p = 7) then
		ie_tipo_item_w := 7;
	else
		ie_tipo_item_w := 4;
	end if;
end if;

return ie_tipo_item_w;

end obter_tipo_item;

-- Gera para AMB
procedure gerar_a1200_amb (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

nr_seq_ptu_pacote_w	ptu_pacote.nr_sequencia%type;
cd_unimed_origem_w	pls_congenere.cd_cooperativa%type;
dados_reg_w		dados_reg;
dados_servico_w		dados_servico;
qt_registro_w		pls_integer;

-- Pacote
Cursor C00 is 
	select	a.cd_edicao_amb,
		a.dt_negociacao,
		a.dt_publicacao
	from 	edicao_amb	a
	where 	a.ie_gerar_a1200 = 'S'
	and	a.dt_negociacao is not null
	and	a.dt_publicacao is not null;
	
-- Regras do pacote
Cursor C01 (	cd_edicao_amb_pc	edicao_amb.cd_edicao_amb%type ) is
	select	a.nr_seq_prestador,
		nvl(pls_obter_dados_prestador(a.nr_seq_prestador,'A400'),'0') cd_prest_a400,
		substr(pls_obter_dados_prestador(a.nr_seq_prestador,'N'),1,40) nm_prestador
	from	ptu_prestador_amb a
	where	a.cd_edicao_amb	= cd_edicao_amb_pc
	and	a.nr_seq_prestador is not null
	union
	select	g.nr_seq_prestador,
		nvl(pls_obter_dados_prestador(g.nr_seq_prestador,'A400'),'0') cd_prest_a400,
		substr(pls_obter_dados_prestador(g.nr_seq_prestador,'N'),1,40) nm_prestador
	from	ptu_prestador_amb	a,
		pls_preco_prestador	g
	where	a.nr_seq_grupo_prestador	= g.nr_seq_grupo
	and	a.cd_edicao_amb			= cd_edicao_amb_pc
	and	a.nr_seq_grupo_prestador is not null
	group by a.nr_seq_prestador,
		g.nr_seq_prestador;

-- Servi�os do pacote		
cursor C02 (	cd_edicao_amb_pc	edicao_amb.cd_edicao_amb%type,
		nr_seq_pacote_reg_pc	ptu_pacote_reg.nr_Sequencia%type) is --ptu_pacote_servico
	select 	1 ie_tipo_tabela,
		a.cd_procedimento,
		'N' ie_honorario,
		1 ie_principal,
		(select	obter_tipo_item(nvl(max(x.ie_classificacao),0),'P')
		 from	procedimento x
	 	 where	x.cd_procedimento = a.cd_procedimento
		 and	x.ie_origem_proced = a.ie_origem_proced) ie_tipo_item,
		nr_seq_pacote_reg_pc,
		nvl(a.vl_procedimento,0) vl_negociado		
	from 	preco_amb a
	where 	a.cd_edicao_amb	= cd_edicao_amb_pc;
	
begin
-- Primeiro gera o pacote
insere_pacote_a1200(cd_estabelecimento_p, nm_usuario_p, nr_seq_ptu_pacote_w, cd_unimed_origem_w);

for r_C00_w in C00 loop
	
	for r_C01_w in C01 (r_C00_w.cd_edicao_amb) loop
		-- Pega as informa��es da regra
		dados_reg_w.nr_seq_pacote	:= nr_seq_ptu_pacote_w;
		dados_reg_w.cd_unimed_prestador	:= cd_unimed_origem_w;
		dados_reg_w.cd_prestador	:= r_C01_w.cd_prest_a400;
		dados_reg_w.nm_prestador	:= r_C01_w.nm_prestador;
		dados_reg_w.dt_negociacao	:= r_C00_w.dt_negociacao;
		dados_reg_w.dt_publicacao	:= r_C00_w.dt_publicacao;
		
		-- Insere a regra
		insere_pacote_reg (dados_reg_w, nm_usuario_p);
		
		-- Cursor de servi�os
		Open C02 (r_C00_w.cd_edicao_amb, dados_reg_w.nr_sequencia);
		loop
			-- Limpa as vari�veis
			insere_pacote_servico(dados_servico_w, nm_usuario_p);
			
			-- Feito com fetch por quest�es de performance
			-- Tem um par�metro de entrada no cursor que � feito select em cima dele
			-- para continuar utilizando o fetch, devido o n�mero de servi�os que podem ser gerados
			-- � interessante utilizar essa estrutura
			fetch C02 bulk collect into 	dados_servico_w.ie_tipo_tabela,
							dados_servico_w.cd_servico,
							dados_servico_w.ie_honorario,
							dados_servico_w.ie_principal,
							dados_servico_w.ie_tipo_item,
							dados_servico_w.nr_seq_pacote_reg,
							dados_servico_w.vl_servico
			limit pls_util_pck.qt_registro_transacao_w;
			
			exit when dados_servico_w.ie_tipo_tabela.count = 0;
			
			-- Insere na tabela e limpo as vari�veis
			insere_pacote_servico(dados_servico_w, nm_usuario_p);
		end loop;
		close C02;		
	end loop;
	-- Deleta regras sem itens
	deleta_reg_sem_item(nr_seq_ptu_pacote_w);
end loop;

select	count(1)
into	qt_registro_w
from	ptu_pacote_reg
where	nr_seq_pacote	= nr_seq_ptu_pacote_w;

if	(qt_registro_w = 0) then
	delete	ptu_pacote
	where	nr_sequencia	= nr_seq_ptu_pacote_w;
end if;

end gerar_a1200_amb;

-- Gera para Mat
procedure gerar_a1200_mat (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

nr_seq_ptu_pacote_w	ptu_pacote.nr_sequencia%type;
cd_unimed_origem_w	pls_congenere.cd_cooperativa%type;
dados_reg_w		dados_reg;
dados_servico_w		dados_servico;
qt_registro_w		pls_integer;

-- Pacote				
Cursor C01 is
	select	a.nr_sequencia,
		a.dt_negociacao,
		a.dt_publicacao
	from	pls_material_preco a
	where	ie_gerar_a1200 = 'S'
	and	a.dt_negociacao is not null
	and	a.dt_publicacao is not null
	and	a.ie_situacao = 'A';

-- Regras do pacote	
Cursor C02 (nr_seq_preco_mat_pc		pls_material_preco.nr_sequencia%type) is
	select	a.nr_seq_prestador,
		nvl(pls_obter_dados_prestador(a.nr_seq_prestador,'A400'),'0') cd_prest_a400,
		substr(pls_obter_dados_prestador(a.nr_seq_prestador,'N'),1,40) nm_prestador
	from	ptu_prestador_material a
	where	a.nr_seq_material_preco	= nr_seq_preco_mat_pc
	and	a.nr_seq_prestador is not null
	union
	select	g.nr_seq_prestador,
		nvl(pls_obter_dados_prestador(g.nr_seq_prestador,'A400'),'0') cd_prest_a400,
		substr(pls_obter_dados_prestador(g.nr_seq_prestador,'N'),1,40) nm_prestador
	from	ptu_prestador_material a,
		pls_preco_prestador g
	where	a.nr_seq_grupo_prestador = g.nr_seq_grupo
	and	a.nr_seq_material_preco = nr_seq_preco_mat_pc
	and	a.nr_seq_grupo_prestador is not null
	group by a.nr_seq_prestador,
		g.nr_seq_prestador;
		
-- Servi�os do pacote			
cursor C03 (	nr_seq_preco_mat_pc	pls_material_preco.nr_sequencia%type,
		nr_seq_pacote_reg_pc	ptu_pacote_reg.nr_Sequencia%type) is --ptu_pacote_servico
	select 	1 ie_tipo_tabela,
		b.cd_material_ops,
		'N' ie_honorario,
		1 ie_principal,
		obter_tipo_item(b.ie_tipo_despesa, 'M') ie_tipo_item,
		nr_seq_pacote_reg_pc,
		obter_preco_material(a.nr_sequencia) vl_negociado
	from 	pls_material_preco_item	a,
		pls_material b
	where 	b.nr_sequencia = a.nr_seq_material
	and	a.nr_seq_material_preco	= nr_seq_preco_mat_pc
	and	a.ie_situacao = 'A';
				
begin
-- Primeiro gera o pacote
insere_pacote_a1200(cd_estabelecimento_p, nm_usuario_p, nr_seq_ptu_pacote_w, cd_unimed_origem_w);

for r_C01_w in C01 loop
	
	for r_C02_w in C02 (r_C01_w.nr_sequencia) loop
		-- Pega as informa��es da regra
		dados_reg_w.nr_seq_pacote	:= nr_seq_ptu_pacote_w;
		dados_reg_w.cd_unimed_prestador	:= cd_unimed_origem_w;
		dados_reg_w.cd_prestador	:= r_C02_w.cd_prest_a400;
		dados_reg_w.nm_prestador	:= r_C02_w.nm_prestador;
		dados_reg_w.dt_negociacao	:= r_C01_w.dt_negociacao;
		dados_reg_w.dt_publicacao	:= r_C01_w.dt_publicacao;
		
		-- Insere a regra
		insere_pacote_reg (dados_reg_w, nm_usuario_p);
		
		-- Cursor de servi�os
		Open C03 (r_C01_w.nr_sequencia, dados_reg_w.nr_sequencia);
		loop
			-- Limpa as vari�veis
			insere_pacote_servico(dados_servico_w, nm_usuario_p);
			
			-- Feito com fetch por quest�es de performance
			-- Tem um par�metro de entrada no cursor que � feito select em cima dele
			-- para continuar utilizando o fetch, devido o n�mero de servi�os que podem ser gerados
			-- � interessante utilizar essa estrutura
			fetch C03 bulk collect into 	dados_servico_w.ie_tipo_tabela,
							dados_servico_w.cd_servico,
							dados_servico_w.ie_honorario,
							dados_servico_w.ie_principal,
							dados_servico_w.ie_tipo_item,
							dados_servico_w.nr_seq_pacote_reg,
							dados_servico_w.vl_servico
			limit pls_util_pck.qt_registro_transacao_w;
			
			exit when dados_servico_w.ie_tipo_tabela.count = 0;
			
			-- Insere na tabela e limpo as vari�veis
			insere_pacote_servico(dados_servico_w, nm_usuario_p);
		end loop;
		close C03;
	end loop;
	-- Deleta regras sem itens
	deleta_reg_sem_item(nr_seq_ptu_pacote_w);
end loop;

select	count(1)
into	qt_registro_w
from	ptu_pacote_reg
where	nr_seq_pacote	= nr_seq_ptu_pacote_w;

if	(qt_registro_w = 0) then
	delete	ptu_pacote
	where	nr_sequencia	= nr_seq_ptu_pacote_w;
end if;

end gerar_a1200_mat;

-- Gera para Servi�os
procedure gerar_a1200_serv (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

nr_seq_ptu_pacote_w	ptu_pacote.nr_sequencia%type;
cd_unimed_origem_w	pls_congenere.cd_cooperativa%type;
dados_reg_w		dados_reg;
dados_servico_w		dados_servico;
qt_registro_w		pls_integer;
	
-- Pacote	
Cursor C01 is
	select	a.cd_tabela_servico,
		a.dt_negociacao,
		a.dt_publicacao
	from	tabela_servico a
	where	ie_gerar_a1200 = 'S'
	and	a.dt_negociacao is not null
	and	a.dt_publicacao is not null
	and	a.ie_situacao = 'A';

-- Regras do pacote	
Cursor C02 (cd_tabela_servico_pc	tabela_servico.cd_tabela_servico%type) is
	select	a.nr_seq_prestador,
		nvl(pls_obter_dados_prestador(a.nr_seq_prestador,'A400'),'0') cd_prest_a400,
		substr(pls_obter_dados_prestador(a.nr_seq_prestador,'N'),1,40) nm_prestador
	from	ptu_prestador_servico a
	where	a.cd_tabela_servico	= cd_tabela_servico_pc
	and	a.nr_seq_prestador is not null
	union
	select	g.nr_seq_prestador,
		nvl(pls_obter_dados_prestador(g.nr_seq_prestador,'A400'),'0') cd_prest_a400,
		substr(pls_obter_dados_prestador(g.nr_seq_prestador,'N'),1,40) nm_prestador
	from	ptu_prestador_servico a,
		pls_preco_prestador g
	where	a.nr_seq_grupo_prestador = g.nr_seq_grupo
	and	a.cd_tabela_servico = cd_tabela_servico_pc
	and	a.nr_seq_grupo_prestador is not null
	group by a.nr_seq_prestador,
		g.nr_seq_prestador;

-- Servi�os do pacote			
cursor C03 (	cd_tabela_servico_pc	tabela_servico.cd_tabela_servico%type,
		nr_seq_pacote_reg_pc	ptu_pacote_reg.nr_sequencia%type) is --ptu_pacote_servico
	select 	1 ie_tipo_tabela,
		a.cd_procedimento,
		'N' ie_honorario,
		1 ie_principal,
		(select	obter_tipo_item(nvl(max(x.ie_classificacao),0),'P')
		 from	procedimento x
	 	 where	x.cd_procedimento = a.cd_procedimento
		 and	x.ie_origem_proced = a.ie_origem_proced) ie_tipo_item,
		nr_seq_pacote_reg_pc,
		nvl(a.vl_servico,0) vl_negociado		
	from 	preco_servico a
	where 	a.cd_tabela_servico = cd_tabela_servico_pc;
				
begin
-- Primeiro gera o pacote
insere_pacote_a1200(cd_estabelecimento_p, nm_usuario_p, nr_seq_ptu_pacote_w, cd_unimed_origem_w);

for r_C01_w in C01 loop
	
	for r_C02_w in C02 (r_C01_w.cd_tabela_servico) loop
		-- Pega as informa��es da regra
		dados_reg_w.nr_seq_pacote	:= nr_seq_ptu_pacote_w;
		dados_reg_w.cd_unimed_prestador	:= cd_unimed_origem_w;
		dados_reg_w.cd_prestador	:= r_C02_w.cd_prest_a400;
		dados_reg_w.nm_prestador	:= r_C02_w.nm_prestador;
		dados_reg_w.dt_negociacao	:= r_C01_w.dt_negociacao;
		dados_reg_w.dt_publicacao	:= r_C01_w.dt_publicacao;
		
		-- Insere a regra
		insere_pacote_reg (dados_reg_w, nm_usuario_p);
		
		-- Cursor de servi�os
		Open C03 (r_C01_w.cd_tabela_servico, dados_reg_w.nr_sequencia);
		loop
			-- Limpa as vari�veis
			insere_pacote_servico(dados_servico_w, nm_usuario_p);
			
			-- Feito com fetch por quest�es de performance
			-- Tem um par�metro de entrada no cursor que � feito select em cima dele
			-- para continuar utilizando o fetch, devido o n�mero de servi�os que podem ser gerados
			-- � interessante utilizar essa estrutura
			fetch C03 bulk collect into 	dados_servico_w.ie_tipo_tabela,
							dados_servico_w.cd_servico,
							dados_servico_w.ie_honorario,
							dados_servico_w.ie_principal,
							dados_servico_w.ie_tipo_item,
							dados_servico_w.nr_seq_pacote_reg,
							dados_servico_w.vl_servico
			limit pls_util_pck.qt_registro_transacao_w;
			
			exit when dados_servico_w.ie_tipo_tabela.count = 0;
			
			-- Insere na tabela e limpo as vari�veis
			insere_pacote_servico(dados_servico_w, nm_usuario_p);
		end loop;
		close C03;	
	end loop;
	-- Deleta regras sem itens
	deleta_reg_sem_item(nr_seq_ptu_pacote_w);
end loop;

select	count(1)
into	qt_registro_w
from	ptu_pacote_reg
where	nr_seq_pacote	= nr_seq_ptu_pacote_w;

if	(qt_registro_w = 0) then
	delete	ptu_pacote
	where	nr_sequencia	= nr_seq_ptu_pacote_w;
end if;

end gerar_a1200_serv;

end ptu_gerar_tabela_A1200_pck;
/
