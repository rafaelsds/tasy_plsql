CREATE OR REPLACE PACKAGE PDE IS

  TYPE NUMBER_ROW_T IS RECORD(
    NR_ATENDIMENTO NUMBER(10),
    LIST_ID        VARCHAR2(4000));

  TYPE NUMBER_ROW_DATA_T IS TABLE OF NUMBER_ROW_T;

  TYPE VL_FK_ROW_T IS RECORD(
    NM_TABELA VARCHAR2(50),
    VL_FK_1   VARCHAR2(50),
    VL_FK_2   VARCHAR2(50),
    VL_FK_3   VARCHAR2(50),
    VL_FK_4   VARCHAR2(50));

  TYPE VL_FK_ROW_DATA_T IS TABLE OF VL_FK_ROW_T;

  FUNCTION GET_PIPE_OF_NUMBER_ROW(NUMBER_ROW_DATA_W NUMBER_ROW_DATA_T)
    RETURN NUMBER_ROW_DATA_T
    PIPELINED DETERMINISTIC;

  FUNCTION GET_PIPE_OF_VL_FK_ROW(VL_FK_ROW_DATA_W VL_FK_ROW_DATA_T)
    RETURN VL_FK_ROW_DATA_T
    PIPELINED DETERMINISTIC;

  PROCEDURE CREATE_TRIGGERS_BY_PDE_MAIN(NR_SEQ_PDE_MAIN_P NUMBER);

  PROCEDURE DROP_INVALID_TRIGGERS;

  PROCEDURE INSERT_PDE_STAGE(CD_PESSOA_FISICA_P  VARCHAR2,
                             NR_ATENDIMENTO_P    NUMBER,
                             NR_PRESCRICAO_P     NUMBER,
                             NR_SEQ_PRESCRICAO_P VARCHAR2,
                             NM_TABELA_P         VARCHAR2,
                             NM_ATTRIBUTO_P      VARCHAR2,
                             VL_ATRIBUTO_P       VARCHAR2,
                             VL_FK_1_P           VARCHAR2,
                             VL_FK_2_P           VARCHAR2,
                             VL_FK_3_P           VARCHAR2,
                             VL_FK_4_P           VARCHAR2,
                             NM_USUARIO_P        VARCHAR2);

  PROCEDURE PROCESSA_DADOS_PDE_MAIN(NR_SEQ_PDE_MAIN_P NUMBER);

  PROCEDURE GERAR_JSON(CD_PESSOA_FISICA_P VARCHAR2);

  PROCEDURE OBTER_DESCRICAO_CAMPO(NM_TABELA_P   VARCHAR2,
                                  NM_ATRIBUTO_P VARCHAR2,
                                  VALOR_P       VARCHAR2,
                                  RET_VALOR_P   OUT VARCHAR2);

  PROCEDURE PATIENT_FINDER(IDENTIFIER_P VARCHAR2,
                           TYPE_IN_P    CHAR,
                           TYPE_OUT_P   CHAR,
                           PATIENT_P    OUT VARCHAR2);

  FUNCTION VALOR_EXPRESSAO(DS_STRING_P VARCHAR2, VL_DOMINIO_P VARCHAR2)
    RETURN VARCHAR;

  PROCEDURE ENVIAR_INTEGRACAO(NR_SEQ_PDE_LOG_P NUMBER);

  PROCEDURE ENVIAR_INTEGRACAO_BY_PDE_MAIN(NR_SEQ_PDE_MAIN_P NUMBER);

  PROCEDURE DELETE_ATTRIBUTE(NR_SEQ_PDE_ATTRIBUTE_P IN NUMBER);

  PROCEDURE GET_ONTOLOGY_CODE(CD_SEQ_RECORD_P IN VARCHAR2,
                              IE_GROUP_P      IN VARCHAR2,
                              DS_TYPE_P       IN VARCHAR2,
                              CD_ONTOLOGY_P   OUT VARCHAR2,
                              CD_ONTOLOGY2_P  OUT VARCHAR2,
                              CD_ONTOLOGY3_P  OUT VARCHAR2,
                              CD_ONTOLOGY4_P  OUT VARCHAR2,
                              CD_ONTOLOGY5_P  OUT VARCHAR2,
                              CD_ONTOLOGY6_P  OUT VARCHAR2,
                              CD_ONTOLOGY7_P  OUT VARCHAR2);

  PROCEDURE INSERT_ATTRIBUTE(NR_SEQ_PDE_MAIN_P        IN NUMBER,
                             NR_SEQ_PARENT_P          IN NUMBER,
                             VL_GROUP_DOMAIN_P        IN VARCHAR2,
                             NR_SEQ_ONTOL_TAB_P       IN NUMBER,
                             NR_SEQ_ONTOL_TAB_ATT_P   IN NUMBER,
                             NR_SEQ_ONTOL_ATT_GROUP_P IN NUMBER,
                             NM_TABLE_P               IN VARCHAR2,
                             NM_ATTRIBUTE_P           IN VARCHAR2,
                             CD_ONTOLOGY_P            IN VARCHAR2,
                             CD_ONTOLOGY2_P           IN VARCHAR2,
                             CD_ONTOLOGY3_P           IN VARCHAR2,
                             CD_ONTOLOGY4_P           IN VARCHAR2,
                             CD_ONTOLOGY5_P           IN VARCHAR2,
                             CD_ONTOLOGY6_P           IN VARCHAR2,
                             CD_ONTOLOGY7_P           IN VARCHAR2,
                             NM_USER_P                IN VARCHAR2,
                             NR_NEW_SEQUENCE_P        OUT NUMBER);

  PROCEDURE INSERT_ATRIB_OBRIG(NR_SEQ_PDE_MAIN_P    IN NUMBER,
                               VL_GROUP_DOMAIN_P    IN VARCHAR2,
                               CD_EXP_TABLE_GROUP_W IN NUMBER,
                               CD_EXP_TABLE_W       IN NUMBER,
                               NM_TABLE_P           IN VARCHAR2,
                               NM_ATRIBUTO_P        IN VARCHAR2,
                               NM_USER_P            IN VARCHAR2);

  PROCEDURE GET_FOREIGN_TABLE(NM_TABLE_P         IN VARCHAR2,
                              NM_ATTRIBUTE_P     IN VARCHAR2,
                              IE_DOMAIN_P        OUT NUMBER,
                              NM_FOREIGN_TABLE_P OUT VARCHAR2);

  PROCEDURE SAVE_ATTRIBUTE(NR_SEQ_PDE_ATTRIBUTE_P   IN NUMBER,
                           NR_SEQ_PDE_MAIN_P        IN NUMBER,
                           IE_TYPE_P                IN VARCHAR2,
                           NR_SEQ_PARENT_P          IN NUMBER,
                           VL_GROUP_DOMAIN_P        IN VARCHAR2,
                           NR_SEQ_ONTOL_TAB_P       IN NUMBER,
                           NR_SEQ_ONTOL_TAB_ATT_P   IN NUMBER,
                           NR_SEQ_ONTOL_ATT_GROUP_P IN NUMBER,
                           NM_USER_P                IN VARCHAR2,
                           NR_NEW_SEQUENCE_P        OUT NUMBER);

  PROCEDURE CLEAN_STAGE;

  PROCEDURE GRAVA_LOG_ERRO(DS_REF_P  IN VARCHAR2,
                           DS_ERRO_P IN CLOB,
                           SQLCODE_P IN NUMBER DEFAULT NULL,
                           SQLERRM_P IN VARCHAR2 DEFAULT NULL);

END PDE;
/
CREATE OR REPLACE PACKAGE BODY PDE IS

  /*******************************************************************************/
  /******************************* PRIVATE METHODS *******************************/
  /*******************************************************************************/

  PROCEDURE PRC_BUILD_TRG_DINAMIC(NM_TABELA_P    VARCHAR2,
                                  IE_IS_FATHER_P VARCHAR2,
                                  IE_IS_CHILD_P  VARCHAR2) IS
    NM_ATRIBUTO_W       VARCHAR2(100) := 'NULL';
    NR_PRESCRICAO_W     VARCHAR2(100) := 'NULL';
    NR_ATENDIMENTO_W    VARCHAR2(100) := 'NULL';
    CD_PESSOA_FISICA_W  VARCHAR2(100) := 'NULL';
    NR_SEQ_PRESCRICAO_W VARCHAR2(100) := 'NULL';
    VL_FK_1_W           VARCHAR2(100) := 'NULL';
    VL_FK_2_W           VARCHAR2(100) := 'NULL';
    VL_FK_3_W           VARCHAR2(100) := 'NULL';
    VL_FK_4_W           VARCHAR2(100) := 'NULL';
    INSERT_W            VARCHAR2(32767);
    AUX_TXT_W           VARCHAR2(32767);
    TRIGGER_NAME_W      VARCHAR2(40);
    TRIGGER_W           VARCHAR2(32767);
    TRIGGER_COLUMNS_W   VARCHAR2(32767);
    LAST_TABLE_REF_W    VARCHAR2(100);
  
    PROCEDURE PROCESS_FK_VALUES(TABLE_NAME_P      VARCHAR2,
                                CONSTRAINT_NAME_P VARCHAR2) IS
    BEGIN
      FOR C IN (SELECT UC.COLUMN_NAME, UC.POSITION
                  FROM USER_CONS_COLUMNS UC
                 WHERE UC.TABLE_NAME = TABLE_NAME_P
                   AND UC.CONSTRAINT_NAME = CONSTRAINT_NAME_P
                 ORDER BY UC.POSITION) LOOP
        IF (C.POSITION = 1) THEN
          VL_FK_1_W := ':NEW.' || C.COLUMN_NAME;
        ELSIF (C.POSITION = 2) THEN
          VL_FK_2_W := ':NEW.' || C.COLUMN_NAME;
        ELSIF (C.POSITION = 3) THEN
          VL_FK_3_W := ':NEW.' || C.COLUMN_NAME;
        ELSIF (C.POSITION = 4) THEN
          VL_FK_4_W := ':NEW.' || C.COLUMN_NAME;
        END IF;
      END LOOP;
    END PROCESS_FK_VALUES;
  
    PROCEDURE PROCESS_FIELDS_FATHER(NM_TAB_FATHER_P VARCHAR2) IS
      NM_TAB_CHILD_W       PDE_ATTRIBUTE.NM_TABELA_FK%TYPE;
      NM_CONS_TAB_CHILD_W  PDE_ATTRIBUTE.NM_FK_TABELA_FK%TYPE;
      NM_CONS_TAB_FATHER_W PDE_ATTRIBUTE.NM_FK_TABELA_FK%TYPE;
    BEGIN
      SELECT PA.NM_TABELA, PA.NM_FK_TABELA_FK
        INTO NM_TAB_CHILD_W, NM_CONS_TAB_CHILD_W
        FROM PDE_ATTRIBUTE PA
       WHERE NVL(PA.IE_SITUACAO, 'A') = 'A'
         AND PA.NM_TABELA_FK = NM_TAB_FATHER_P
         AND PA.NM_FK_TABELA_FK IS NOT NULL
         AND PA.NM_TABELA IS NOT NULL
         AND PA.NM_ATRIBUTO IS NOT NULL
         AND ROWNUM <= 1;
    
      SELECT UC.R_CONSTRAINT_NAME
        INTO NM_CONS_TAB_FATHER_W
        FROM USER_CONSTRAINTS UC
       WHERE UC.TABLE_NAME = NM_TAB_CHILD_W
         AND UC.CONSTRAINT_NAME = NM_CONS_TAB_CHILD_W;
    
      PROCESS_FK_VALUES(NM_TAB_FATHER_P, NM_CONS_TAB_FATHER_W);
    END PROCESS_FIELDS_FATHER;
  
    PROCEDURE PROCESS_FIELDS_CHILD(NM_TAB_CHILD_P VARCHAR2) IS
      NM_CONS_TAB_CHILD_W PDE_ATTRIBUTE.NM_FK_TABELA_FK%TYPE;
    BEGIN
      SELECT PA.NM_FK_TABELA_FK
        INTO NM_CONS_TAB_CHILD_W
        FROM PDE_ATTRIBUTE PA
       WHERE NVL(PA.IE_SITUACAO, 'A') = 'A'
         AND PA.NM_TABELA = NM_TAB_CHILD_P
         AND PA.NM_ATRIBUTO IS NOT NULL
         AND PA.NM_TABELA_FK IS NOT NULL
         AND PA.NM_FK_TABELA_FK IS NOT NULL
         AND ROWNUM <= 1;
    
      PROCESS_FK_VALUES(NM_TAB_CHILD_P, NM_CONS_TAB_CHILD_W);
    END PROCESS_FIELDS_CHILD;
  
  BEGIN
    FOR A IN (SELECT NM_ATRIBUTO
                FROM PDE_ATTRIBUTE
               WHERE NM_TABELA = NM_TABELA_P
                 AND NVL(IE_SITUACAO, 'A') = 'A'
                 AND CD_ONTOLOGIA IS NOT NULL
                 AND NM_ATRIBUTO IS NOT NULL
               GROUP BY NM_ATRIBUTO
               ORDER BY NM_ATRIBUTO) LOOP
      IF NM_TABELA_P = 'PRESC_PROCEDIMENTO' AND
         A.NM_ATRIBUTO IN ('NR_SEQUENCIA', 'NR_SEQ_PRESC') THEN
        NM_ATRIBUTO_W := 'NEW.NR_SEQ_PRESCRICAO';
      END IF;
    
      IF A.NM_ATRIBUTO = 'NR_PRESCRICAO' THEN
        NR_PRESCRICAO_W := ':NEW.NR_PRESCRICAO';
      ELSIF A.NM_ATRIBUTO = 'NR_SEQ_PRESCRICAO' THEN
        NR_SEQ_PRESCRICAO_W := ':NEW.NR_SEQ_PRESCRICAO';
      ELSIF A.NM_ATRIBUTO = 'NR_ATENDIMENTO' THEN
        NR_ATENDIMENTO_W := ':NEW.NR_ATENDIMENTO';
      ELSIF A.NM_ATRIBUTO = 'CD_PESSOA_FISICA' THEN
        CD_PESSOA_FISICA_W := ':NEW.CD_PESSOA_FISICA';
      END IF;
    END LOOP;
  
    IF (NVL(IE_IS_FATHER_P, 'F') = 'S') THEN
      PROCESS_FIELDS_FATHER(NM_TABELA_P);
    ELSIF (NVL(IE_IS_CHILD_P, 'F') = 'S') THEN
      PROCESS_FIELDS_CHILD(NM_TABELA_P);
    END IF;
  
    FOR A IN (SELECT PA.NM_ATRIBUTO, MAX(PA.NM_TABELA_FK) NM_TABELA_REF
                FROM PDE_ATTRIBUTE PA
               WHERE PA.NM_TABELA = NM_TABELA_P
                 AND NVL(PA.IE_SITUACAO, 'A') = 'A'
                 AND PA.CD_ONTOLOGIA IS NOT NULL
                 AND PA.NM_ATRIBUTO IS NOT NULL
               GROUP BY PA.NM_ATRIBUTO
               ORDER BY PA.NM_ATRIBUTO) LOOP
      IF (TRIM(TRIGGER_COLUMNS_W) IS NOT NULL) THEN
        TRIGGER_COLUMNS_W := TRIGGER_COLUMNS_W || ', ' || CHR(13) ||
                             '                ';
      END IF;
    
      TRIGGER_COLUMNS_W := TRIGGER_COLUMNS_W || A.NM_ATRIBUTO;
    
      AUX_TXT_W := '  IF (INSERTING' || CHR(13) ||
                   '  OR ((:OLD.#@#@COLUMN#@#@ IS NULL AND :NEW.#@#@COLUMN#@#@ IS NOT NULL)' ||
                   CHR(13) ||
                   '  OR (:OLD.#@#@COLUMN#@#@ IS NOT NULL AND :NEW.#@#@COLUMN#@#@ IS NULL)' ||
                   CHR(13) ||
                   '  OR (:OLD.#@#@COLUMN#@#@ <> :NEW.#@#@COLUMN#@#@))) THEN' ||
                   CHR(13);
    
      INSERT_W := INSERT_W ||
                  REPLACE(AUX_TXT_W, '#@#@COLUMN#@#@', A.NM_ATRIBUTO);
    
      IF ((TRIM(A.NM_TABELA_REF) IS NOT NULL) AND
         ((TRIM(LAST_TABLE_REF_W) IS NULL) OR
         (LAST_TABLE_REF_W <> A.NM_TABELA_REF))) THEN
        LAST_TABLE_REF_W := A.NM_TABELA_REF;
      
        INSERT_W := INSERT_W || '    /* Parent table: ' || A.NM_TABELA_REF ||
                    ' */' || CHR(13);
      END IF;
    
      INSERT_W := INSERT_W || '    PDE.INSERT_PDE_STAGE(' ||
                  CD_PESSOA_FISICA_W || ', ' || NR_ATENDIMENTO_W || ', ' ||
                  NR_PRESCRICAO_W || ', ' || NR_SEQ_PRESCRICAO_W || ', ''' ||
                  NM_TABELA_P || ''', ''' || A.NM_ATRIBUTO || ''', :NEW.' ||
                  A.NM_ATRIBUTO || ', ' || VL_FK_1_W || ', ' || VL_FK_2_W || ', ' ||
                  VL_FK_3_W || ', ' || VL_FK_4_W || ', ' ||
                  ':NEW.NM_USUARIO);' || CHR(13) || '  END IF;' || CHR(13) ||
                  CHR(13);
    END LOOP;
  
    SELECT REPLACE('TRG_' ||
                   SUBSTR(NVL(MAX(TS.DS_SHORTNAME || '_' || TS.NM_TABELA),
                              NM_TABELA_P),
                          1,
                          18) || '_DYN_PDE',
                   '__',
                   '_') TRIGGER_NAME
      INTO TRIGGER_NAME_W
      FROM TABELA_SISTEMA TS
     WHERE TS.NM_TABELA = NM_TABELA_P;
  
    IF (TRIM(TRIGGER_COLUMNS_W) IS NOT NULL) THEN
      TRIGGER_W := 'CREATE OR REPLACE TRIGGER #@#@TRGNAME#@#@
AFTER INSERT
   OR UPDATE OF #@#@COLUMNS#@#@
   ON #@#@TABLE#@#@ FOR EACH ROW
  /* Trigger generated dynamically by the HL7 Interfaces function. Patient Data Export integration. */
BEGIN
#@#@TRGCODE#@#@
EXCEPTION
  WHEN OTHERS THEN
    PDE.GRAVA_LOG_ERRO(''#@#@TRGNAME#@#@'', #@#@LOGERRO#@#@, SQLCODE, SQLERRM);
END #@#@TRGNAME#@#@;';
    
      TRIGGER_W := REPLACE(TRIGGER_W, '#@#@TRGNAME#@#@', TRIGGER_NAME_W);
      TRIGGER_W := REPLACE(TRIGGER_W, '#@#@COLUMNS#@#@', TRIGGER_COLUMNS_W);
      TRIGGER_W := REPLACE(TRIGGER_W, '#@#@TABLE#@#@', NM_TABELA_P);
      TRIGGER_W := REPLACE(TRIGGER_W, '#@#@TRGCODE#@#@', INSERT_W);
      TRIGGER_W := REPLACE(TRIGGER_W,
                           '#@#@LOGERRO#@#@',
                           '''CD_PESSOA_FISICA: '' || ' ||
                           CD_PESSOA_FISICA_W ||
                           ' || '', NR_ATENDIMENTO: '' || ' ||
                           NR_ATENDIMENTO_W ||
                           ' || '', NR_PRESCRICAO: '' || ' ||
                           NR_PRESCRICAO_W ||
                           ' || '', NR_SEQ_PRESCRICAO: '' || ' ||
                           NR_SEQ_PRESCRICAO_W);
    
      EXEC_SQL_DINAMICO('PDE.newTrigger', TRIGGER_W);
    END IF;
  END PRC_BUILD_TRG_DINAMIC;

  /*******************************************************************************/
  /******************************* PUBLIC METHODS *******************************/
  /*******************************************************************************/

  FUNCTION GET_PIPE_OF_NUMBER_ROW(NUMBER_ROW_DATA_W NUMBER_ROW_DATA_T)
    RETURN NUMBER_ROW_DATA_T
    PIPELINED DETERMINISTIC IS
  BEGIN
    FOR I IN 1 .. NUMBER_ROW_DATA_W.COUNT LOOP
      PIPE ROW(NUMBER_ROW_DATA_W(I));
    END LOOP;
  
    RETURN;
  END GET_PIPE_OF_NUMBER_ROW;

  FUNCTION GET_PIPE_OF_VL_FK_ROW(VL_FK_ROW_DATA_W VL_FK_ROW_DATA_T)
    RETURN VL_FK_ROW_DATA_T
    PIPELINED DETERMINISTIC IS
  BEGIN
    FOR I IN 1 .. VL_FK_ROW_DATA_W.COUNT LOOP
      PIPE ROW(VL_FK_ROW_DATA_W(I));
    END LOOP;
  
    RETURN;
  END GET_PIPE_OF_VL_FK_ROW;

  PROCEDURE CREATE_TRIGGERS_BY_PDE_MAIN(NR_SEQ_PDE_MAIN_P NUMBER) IS
  
    PROCEDURE PROCESS_TABLE(NM_TABELA_P VARCHAR2) IS
      COUNT_COLS_W   NUMBER;
      IE_IS_FATHER_W VARCHAR2(1);
      IE_IS_CHILD_W  VARCHAR2(1);
    BEGIN
      SELECT COUNT(*)
        INTO COUNT_COLS_W
        FROM USER_TAB_COLS
       WHERE UPPER(TABLE_NAME) = NM_TABELA_P
         AND (UPPER(COLUMN_NAME) = 'NR_ATENDIMENTO' OR
              UPPER(COLUMN_NAME) = 'NR_PRESCRICAO' OR
              UPPER(COLUMN_NAME) = 'CD_PESSOA_FISICA');
    
      IF (COUNT_COLS_W > 0) THEN
        SELECT NVL2(MAX(PA.NM_TABELA), 'S', 'N')
          INTO IE_IS_FATHER_W
          FROM PDE_ATTRIBUTE PA
         WHERE PA.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_P
           AND NVL(PA.IE_SITUACAO, 'A') = 'A'
           AND PA.NM_TABELA_FK = NM_TABELA_P
           AND PA.NM_FK_TABELA_FK IS NOT NULL
           AND PA.NM_TABELA IS NOT NULL
           AND PA.NM_ATRIBUTO IS NOT NULL;
      
        SELECT NVL2(MAX(PA.NM_TABELA), 'S', 'N')
          INTO IE_IS_CHILD_W
          FROM PDE_ATTRIBUTE PA
         WHERE PA.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_P
           AND NVL(PA.IE_SITUACAO, 'A') = 'A'
           AND PA.NM_TABELA = NM_TABELA_P
           AND PA.NM_ATRIBUTO IS NOT NULL
           AND PA.NM_TABELA_FK IS NOT NULL
           AND PA.NM_FK_TABELA_FK IS NOT NULL;
      
        PRC_BUILD_TRG_DINAMIC(NM_TABELA_P, IE_IS_FATHER_W, IE_IS_CHILD_W);
      END IF;
    END;
  
  BEGIN
    FOR X IN (SELECT DISTINCT PA.NM_TABELA
                FROM PDE_ATTRIBUTE PA
               WHERE PA.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_P
                 AND NVL(PA.IE_SITUACAO, 'A') = 'A'
                 AND PA.NM_TABELA IS NOT NULL
                 AND PA.NM_ATRIBUTO IS NOT NULL) LOOP
      PROCESS_TABLE(X.NM_TABELA);
    END LOOP;
  
    DROP_INVALID_TRIGGERS;
  
    UPDATE PDE_MAIN PM
       SET PM.IE_STATUS = 'M'
     WHERE PM.NR_SEQUENCIA = NR_SEQ_PDE_MAIN_P;
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        PDE.GRAVA_LOG_ERRO('CREATE.TRIGGERS.ERROR',
                           'NR_SEQ_PDE_MAIN_P: ' || NR_SEQ_PDE_MAIN_P,
                           SQLCODE,
                           SQLERRM);
      
        UPDATE PDE_MAIN PM
           SET PM.IE_STATUS = 'E'
         WHERE PM.NR_SEQUENCIA = NR_SEQ_PDE_MAIN_P;
      
        COMMIT;
      END;
  END CREATE_TRIGGERS_BY_PDE_MAIN;

  PROCEDURE DROP_INVALID_TRIGGERS IS
  BEGIN
    FOR D IN (SELECT 'DROP TRIGGER ' || UT.TRIGGER_NAME DROP_COMMAND
                FROM USER_TRIGGERS UT
               WHERE UT.TRIGGER_NAME LIKE 'TRG_%_DYN_PDE'
                 AND UT.TRIGGERING_EVENT = 'INSERT OR UPDATE'
                 AND UT.TRIGGER_TYPE LIKE '%AFTER%'
                 AND UT.STATUS = 'ENABLED'
                 AND UT.TABLE_NAME NOT IN
                     (SELECT DISTINCT PA.NM_TABELA FROM PDE_ATTRIBUTE PA)
                 AND NOT EXISTS
               (SELECT 1
                        FROM OBJETO_SISTEMA OS
                       WHERE UPPER(OS.IE_TIPO_OBJETO) = 'TRIGGER'
                         AND OS.NM_OBJETO = UT.TRIGGER_NAME
                         AND ROWNUM <= 1)) LOOP
      BEGIN
        EXEC_SQL_DINAMICO('PDE.dropTrigger', D.DROP_COMMAND);
      EXCEPTION
        WHEN OTHERS THEN
          PDE.GRAVA_LOG_ERRO('DROP.TRIGGERS.ERROR',
                             D.DROP_COMMAND,
                             SQLCODE,
                             SQLERRM);
      END;
    END LOOP;
  END DROP_INVALID_TRIGGERS;

  PROCEDURE INSERT_PDE_STAGE(CD_PESSOA_FISICA_P  VARCHAR2,
                             NR_ATENDIMENTO_P    NUMBER,
                             NR_PRESCRICAO_P     NUMBER,
                             NR_SEQ_PRESCRICAO_P VARCHAR2,
                             NM_TABELA_P         VARCHAR2,
                             NM_ATTRIBUTO_P      VARCHAR2,
                             VL_ATRIBUTO_P       VARCHAR2,
                             VL_FK_1_P           VARCHAR2,
                             VL_FK_2_P           VARCHAR2,
                             VL_FK_3_P           VARCHAR2,
                             VL_FK_4_P           VARCHAR2,
                             NM_USUARIO_P        VARCHAR2) IS
    NR_SEQ_PDE_LOG_W   PDE_LOG.NR_SEQUENCIA%TYPE;
    NR_SEQ_PDE_STAGE_W PDE_STAGE.NR_SEQUENCIA%TYPE;
    CD_PESSOA_FISICA_W PESSOA_FISICA.CD_PESSOA_FISICA%TYPE;
  
    PROCEDURE INSERT_LOG(NR_SEQ_PDE_MAIN_P PDE_ATTRIBUTE.NR_SEQ_PDE_MAIN%TYPE) IS
    BEGIN
      SELECT PDE_LOG_SEQ.NEXTVAL INTO NR_SEQ_PDE_LOG_W FROM DUAL;
    
      INSERT INTO PDE_LOG
        (NR_SEQUENCIA,
         NR_SEQ_PDE_MAIN,
         CD_PESSOA_FISICA,
         NR_ATENDIMENTO,
         NR_PRESCRICAO,
         DT_ATUALIZACAO,
         NM_USUARIO,
         DT_ATUALIZACAO_NREC,
         NM_USUARIO_NREC)
      VALUES
        (NR_SEQ_PDE_LOG_W,
         NR_SEQ_PDE_MAIN_P,
         CD_PESSOA_FISICA_W,
         NR_ATENDIMENTO_P,
         NR_PRESCRICAO_P,
         SYSDATE,
         NVL(WHEB_USUARIO_PCK.GET_NM_USUARIO, NM_USUARIO_P),
         SYSDATE,
         NVL(WHEB_USUARIO_PCK.GET_NM_USUARIO, NM_USUARIO_P));
    
      INSERT INTO PDE_STAGE_LOG
        (NR_SEQUENCIA, NR_SEQ_STAGE, NR_SEQ_LOG)
      VALUES
        (PDE_STAGE_LOG_SEQ.NEXTVAL, NR_SEQ_PDE_STAGE_W, NR_SEQ_PDE_LOG_W);
    END;
  
  BEGIN
    IF (NM_TABELA_P IS NOT NULL AND NM_ATTRIBUTO_P IS NOT NULL AND
       VL_ATRIBUTO_P IS NOT NULL AND
       (CD_PESSOA_FISICA_P IS NOT NULL OR NR_ATENDIMENTO_P IS NOT NULL OR
       NR_PRESCRICAO_P IS NOT NULL)) THEN
      IF (CD_PESSOA_FISICA_P IS NOT NULL) THEN
        CD_PESSOA_FISICA_W := CD_PESSOA_FISICA_P;
      ELSIF NR_PRESCRICAO_P IS NOT NULL THEN
        PATIENT_FINDER(NR_PRESCRICAO_P, 'PR', 'PF', CD_PESSOA_FISICA_W);
      ELSIF NR_ATENDIMENTO_P IS NOT NULL THEN
        PATIENT_FINDER(NR_ATENDIMENTO_P, 'AT', 'PF', CD_PESSOA_FISICA_W);
      END IF;
    
      SELECT PDE_STAGE_SEQ.NEXTVAL INTO NR_SEQ_PDE_STAGE_W FROM DUAL;
    
      INSERT INTO PDE_STAGE
        (NR_SEQUENCIA,
         CD_PESSOA_FISICA,
         NR_ATENDIMENTO,
         NR_PRESCRICAO,
         NR_SEQ_PRESCRICAO,
         NM_TABELA,
         NM_ATRIBUTO,
         VL_ATRIBUTO,
         VL_FK_1,
         VL_FK_2,
         VL_FK_3,
         VL_FK_4,
         DT_ATUALIZACAO,
         NM_USUARIO,
         DT_ATUALIZACAO_NREC,
         NM_USUARIO_NREC)
      VALUES
        (NR_SEQ_PDE_STAGE_W,
         CD_PESSOA_FISICA_W,
         NR_ATENDIMENTO_P,
         NR_PRESCRICAO_P,
         NR_SEQ_PRESCRICAO_P,
         NM_TABELA_P,
         NM_ATTRIBUTO_P,
         VL_ATRIBUTO_P,
         VL_FK_1_P,
         VL_FK_2_P,
         VL_FK_3_P,
         VL_FK_4_P,
         SYSDATE,
         NVL(WHEB_USUARIO_PCK.GET_NM_USUARIO, NM_USUARIO_P),
         SYSDATE,
         NVL(WHEB_USUARIO_PCK.GET_NM_USUARIO, NM_USUARIO_P));
    
      FOR A IN (SELECT DISTINCT PA.NR_SEQ_PDE_MAIN
                  FROM PDE_ATTRIBUTE PA
                 WHERE PA.NM_TABELA = NM_TABELA_P
                   AND PA.NM_ATRIBUTO = NM_ATTRIBUTO_P
                 ORDER BY PA.NR_SEQ_PDE_MAIN) LOOP
        BEGIN
          SELECT PL.NR_SEQUENCIA
            INTO NR_SEQ_PDE_LOG_W
            FROM PDE_LOG PL
           WHERE PL.NR_SEQ_PDE_MAIN = A.NR_SEQ_PDE_MAIN
             AND PL.DS_JSON IS NULL
             AND NVL(PL.CD_PESSOA_FISICA, '#@#@') =
                 NVL(CD_PESSOA_FISICA_W, '#@#@')
             AND NVL(PL.NR_ATENDIMENTO, -123) = NVL(NR_ATENDIMENTO_P, -123)
             AND NVL(PL.NR_PRESCRICAO, -123) = NVL(NR_PRESCRICAO_P, -123);
        
          INSERT INTO PDE_STAGE_LOG
            (NR_SEQUENCIA, NR_SEQ_STAGE, NR_SEQ_LOG)
          VALUES
            (PDE_STAGE_LOG_SEQ.NEXTVAL,
             NR_SEQ_PDE_STAGE_W,
             NR_SEQ_PDE_LOG_W);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            INSERT_LOG(A.NR_SEQ_PDE_MAIN);
        END;
      END LOOP;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      PDE.GRAVA_LOG_ERRO('INSERT.STAGE.ERROR',
                         'CD_PESSOA_FISICA_P: ' || CD_PESSOA_FISICA_P ||
                         ', NR_ATENDIMENTO_P: ' || NR_ATENDIMENTO_P ||
                         ', NR_PRESCRICAO_P: ' || NR_PRESCRICAO_P ||
                         ', NR_SEQ_PRESCRICAO_P: ' || NR_SEQ_PRESCRICAO_P ||
                         ', NM_TABELA_P: ' || NM_TABELA_P ||
                         ', NM_ATTRIBUTO_P: ' || NM_ATTRIBUTO_P ||
                         ', VL_ATRIBUTO_P: ' || VL_ATRIBUTO_P ||
                         ', VL_FK_1_P: ' || VL_FK_1_P || ', VL_FK_2_P: ' ||
                         VL_FK_2_P || ', VL_FK_3_P: ' || VL_FK_3_P ||
                         ', VL_FK_4_P: ' || VL_FK_4_P || ', NM_USUARIO_P: ' ||
                         NM_USUARIO_P,
                         SQLCODE,
                         SQLERRM);
  END INSERT_PDE_STAGE;

  PROCEDURE PROCESSA_DADOS_PDE_MAIN(NR_SEQ_PDE_MAIN_P NUMBER) IS
  BEGIN
    UPDATE PDE_LOG PL
       SET PL.DS_JSON = TO_DATE('01/01/1900', 'DD/MM/YYYY'),
           PL.DT_JSON = TO_DATE('01/01/1900', 'DD/MM/YYYY')
     WHERE PL.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_P
       AND DT_ENVIO IS NULL
       AND PL.DT_JSON IS NULL
       AND PL.DS_JSON IS NULL
       AND NOT EXISTS
     (SELECT 1
              FROM PESSOA_FISICA PF
             WHERE PF.CD_PESSOA_FISICA = PL.CD_PESSOA_FISICA);
  
    FOR L IN (SELECT PL.CD_PESSOA_FISICA
                FROM PDE_LOG PL
               WHERE PL.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_P
                 AND DT_ENVIO IS NULL
                 AND PL.DT_JSON IS NULL
                 AND PL.DS_JSON IS NULL
                 AND PL.CD_PESSOA_FISICA IS NOT NULL
               GROUP BY PL.CD_PESSOA_FISICA
               ORDER BY PL.CD_PESSOA_FISICA) LOOP
      GERAR_JSON(L.CD_PESSOA_FISICA);
    END LOOP;
  END PROCESSA_DADOS_PDE_MAIN;

  PROCEDURE GERAR_JSON(CD_PESSOA_FISICA_P VARCHAR2) IS
    JSON_W                   PHILIPS_JSON := PHILIPS_JSON();
    JSON_ORDENS_W            PHILIPS_JSON_LIST := PHILIPS_JSON_LIST();
    DS_JSON_W                CLOB;
    JSON_PATIENT_W           PHILIPS_JSON;
    JSON_ENCOUNTER_W         PHILIPS_JSON;
    JSON_PERSON_IDENTIFIER_W PHILIPS_JSON;
    IE_POSSUI_ATRIB_W        VARCHAR2(1) := 'N';
    NR_SEQ_PDE_MAIN_W        PDE_LOG.NR_SEQ_PDE_MAIN%TYPE;
    CD_PESSOA_FISICA_W       PDE_LOG.CD_PESSOA_FISICA%TYPE;
    NR_ATENDIMENTO_W         PDE_LOG.NR_ATENDIMENTO%TYPE;
    NR_PRESCRICAO_W          PDE_LOG.NR_PRESCRICAO%TYPE;
    DS_PEDIDO_EXT_W          PRESCR_MEDICA.DS_PEDIDO_EXT%TYPE;
    NUMBER_SET_W             PDE.NUMBER_ROW_DATA_T := PDE.NUMBER_ROW_DATA_T();
    LAST_NR_SEQ_W            NUMBER(10);
  
    PROCEDURE TRATA_ARRAY IS
      COUNT_WITH_ENC_W    NUMBER(10) := 0;
      COUNT_WITHOUT_ENC_W NUMBER(10) := 0;
      INDEX_DEL_W         NUMBER(10);
      LIST_ID_W           VARCHAR2(4000);
    BEGIN
      FOR I IN 1 .. NUMBER_SET_W.COUNT LOOP
        IF (NUMBER_SET_W(I).NR_ATENDIMENTO IS NOT NULL) THEN
          COUNT_WITH_ENC_W := COUNT_WITH_ENC_W + 1;
        ELSE
          COUNT_WITHOUT_ENC_W := COUNT_WITHOUT_ENC_W + 1;
        END IF;
      END LOOP;
    
      IF (COUNT_WITH_ENC_W = 1 AND COUNT_WITHOUT_ENC_W = 1) THEN
        FOR I IN 1 .. NUMBER_SET_W.COUNT LOOP
          IF (NUMBER_SET_W(I).NR_ATENDIMENTO IS NULL) THEN
            INDEX_DEL_W := I;
            LIST_ID_W   := NUMBER_SET_W(I).LIST_ID;
          END IF;
        END LOOP;
      
        NUMBER_SET_W.DELETE(INDEX_DEL_W);
        NUMBER_SET_W(NUMBER_SET_W.COUNT).LIST_ID := NUMBER_SET_W(NUMBER_SET_W.COUNT)
                                                   .LIST_ID || ',' ||
                                                    LIST_ID_W;
      END IF;
    END;
  
    PROCEDURE BUSCA_ATRIBUTOS(NR_SEQ_SUP_P       NUMBER,
                              JSON_ATRIBUTOS_P   IN OUT PHILIPS_JSON_LIST,
                              VL_FK_ROW_DATA_P   IN OUT PDE.VL_FK_ROW_DATA_T,
                              IE_FILHO_P         VARCHAR2 DEFAULT 'N',
                              VL_FK_ROW_FILHOS_P PDE.VL_FK_ROW_DATA_T DEFAULT NULL) IS
      JSON_ATRIBUTO_W PHILIPS_JSON;
      RET_VALOR_W     RES_CADASTRO_ONTOLOGIA_PHI.CD_VALOR_TASY%TYPE;
    
      PROCEDURE ADD_CODICOS_COMPLEMENTARES(CD_ONTOLOGIA_W PDE_ATTRIBUTE.CD_ONTOLOGIA%TYPE) IS
      BEGIN
        IF (TRIM(CD_ONTOLOGIA_W) IS NOT NULL) THEN
          JSON_ATRIBUTO_W := PHILIPS_JSON();
          JSON_ATRIBUTO_W.PUT('code', CD_ONTOLOGIA_W);
          JSON_ATRIBUTO_W.PUT('system', 'SNM');
          JSON_ATRIBUTO_W.PUT('date',
                              TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        
          JSON_ATRIBUTOS_P.APPEND(JSON_ATRIBUTO_W.TO_JSON_VALUE());
        END IF;
      END ADD_CODICOS_COMPLEMENTARES;
    
      PROCEDURE ADD_VL_FK(NM_TABELA_P PDE_STAGE.NM_TABELA%TYPE,
                          VL_FK_1_P   PDE_STAGE.VL_FK_1%TYPE,
                          VL_FK_2_P   PDE_STAGE.VL_FK_2%TYPE,
                          VL_FK_3_P   PDE_STAGE.VL_FK_3%TYPE,
                          VL_FK_4_P   PDE_STAGE.VL_FK_4%TYPE) IS
        COUNT_W NUMBER(10) := 0;
      BEGIN
        IF (NM_TABELA_P IS NOT NULL AND
           (VL_FK_1_P IS NOT NULL OR VL_FK_2_P IS NOT NULL OR
           VL_FK_3_P IS NOT NULL OR VL_FK_4_P IS NOT NULL)) THEN
          FOR I IN 1 .. VL_FK_ROW_DATA_P.COUNT LOOP
            IF (VL_FK_ROW_DATA_P(I).NM_TABELA = NM_TABELA_P AND
                NVL(VL_FK_ROW_DATA_P(I).VL_FK_1, '#@#@') =
                NVL(VL_FK_1_P, '#@#@') AND
                NVL(VL_FK_ROW_DATA_P(I).VL_FK_2, '#@#@') =
                NVL(VL_FK_2_P, '#@#@') AND
                NVL(VL_FK_ROW_DATA_P(I).VL_FK_3, '#@#@') =
                NVL(VL_FK_3_P, '#@#@') AND
                NVL(VL_FK_ROW_DATA_P(I).VL_FK_4, '#@#@') =
                NVL(VL_FK_4_P, '#@#@')) THEN
              COUNT_W := COUNT_W + 1;
            END IF;
          END LOOP;
        
          IF (COUNT_W = 0) THEN
            VL_FK_ROW_DATA_P.EXTEND;
            VL_FK_ROW_DATA_P(VL_FK_ROW_DATA_P.COUNT).NM_TABELA := NM_TABELA_P;
            VL_FK_ROW_DATA_P(VL_FK_ROW_DATA_P.COUNT).VL_FK_1 := VL_FK_1_P;
            VL_FK_ROW_DATA_P(VL_FK_ROW_DATA_P.COUNT).VL_FK_2 := VL_FK_2_P;
            VL_FK_ROW_DATA_P(VL_FK_ROW_DATA_P.COUNT).VL_FK_3 := VL_FK_3_P;
            VL_FK_ROW_DATA_P(VL_FK_ROW_DATA_P.COUNT).VL_FK_4 := VL_FK_4_P;
          END IF;
        END IF;
      END ADD_VL_FK;
    
      PROCEDURE ADD_ATTRIB(CD_ONTOLOGIA_P    PDE_ATTRIBUTE.CD_ONTOLOGIA%TYPE,
                           CD_ONTOLOGIA2_P   PDE_ATTRIBUTE.CD_ONTOLOGIA2%TYPE,
                           CD_ONTOLOGIA3_P   PDE_ATTRIBUTE.CD_ONTOLOGIA3%TYPE,
                           CD_ONTOLOGIA4_P   PDE_ATTRIBUTE.CD_ONTOLOGIA4%TYPE,
                           CD_ONTOLOGIA5_P   PDE_ATTRIBUTE.CD_ONTOLOGIA5%TYPE,
                           CD_ONTOLOGIA6_P   PDE_ATTRIBUTE.CD_ONTOLOGIA6%TYPE,
                           CD_ONTOLOGIA7_P   PDE_ATTRIBUTE.CD_ONTOLOGIA7%TYPE,
                           CD_EXP_ATRIBUTO_P PDE_ATTRIBUTE.CD_EXP_ATRIBUTO%TYPE,
                           NM_TABELA_P       PDE_STAGE.NM_TABELA%TYPE,
                           NM_ATRIBUTO_P     PDE_STAGE.NM_ATRIBUTO%TYPE,
                           VL_ATRIBUTO_P     PDE_STAGE.VL_ATRIBUTO%TYPE,
                           VL_FK_1_P         PDE_STAGE.VL_FK_1%TYPE,
                           VL_FK_2_P         PDE_STAGE.VL_FK_2%TYPE,
                           VL_FK_3_P         PDE_STAGE.VL_FK_3%TYPE,
                           VL_FK_4_P         PDE_STAGE.VL_FK_4%TYPE,
                           DT_ATUALIZACAO_P  PDE_STAGE.DT_ATUALIZACAO%TYPE) IS
      BEGIN
        ADD_VL_FK(NM_TABELA_P, VL_FK_1_P, VL_FK_2_P, VL_FK_3_P, VL_FK_4_P);
      
        IE_POSSUI_ATRIB_W := 'S';
      
        PDE.OBTER_DESCRICAO_CAMPO(NM_TABELA_P,
                                  NM_ATRIBUTO_P,
                                  VL_ATRIBUTO_P,
                                  RET_VALOR_W);
      
        JSON_ATRIBUTO_W := PHILIPS_JSON();
        JSON_ATRIBUTO_W.PUT('code', CD_ONTOLOGIA_P);
        --JSON_ATRIBUTO_W.PUT('name', A.NM_TABELA || '.' || A.NM_ATRIBUTO);
        JSON_ATRIBUTO_W.PUT('name',
                            EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(CD_EXP_ATRIBUTO_P));
        JSON_ATRIBUTO_W.PUT('system', 'SNM');
        JSON_ATRIBUTO_W.PUT('date',
                            TO_CHAR(DT_ATUALIZACAO_P,
                                    'MM/DD/YYYY HH24:MI:SS'));
        JSON_ATRIBUTO_W.PUT('value', RET_VALOR_W);
      
        JSON_ATRIBUTOS_P.APPEND(JSON_ATRIBUTO_W.TO_JSON_VALUE());
      
        ADD_CODICOS_COMPLEMENTARES(CD_ONTOLOGIA2_P);
        ADD_CODICOS_COMPLEMENTARES(CD_ONTOLOGIA3_P);
        ADD_CODICOS_COMPLEMENTARES(CD_ONTOLOGIA4_P);
        ADD_CODICOS_COMPLEMENTARES(CD_ONTOLOGIA5_P);
        ADD_CODICOS_COMPLEMENTARES(CD_ONTOLOGIA6_P);
        ADD_CODICOS_COMPLEMENTARES(CD_ONTOLOGIA7_P);
      END ADD_ATTRIB;
    
    BEGIN
      JSON_ATRIBUTOS_P := PHILIPS_JSON_LIST();
    
      IF (NVL(IE_FILHO_P, 'N') = 'S') THEN
        FOR I IN 1 .. VL_FK_ROW_FILHOS_P.COUNT LOOP
          FOR A IN (SELECT PA.CD_ONTOLOGIA,
                           PA.CD_ONTOLOGIA2,
                           PA.CD_ONTOLOGIA3,
                           PA.CD_ONTOLOGIA4,
                           PA.CD_ONTOLOGIA5,
                           PA.CD_ONTOLOGIA6,
                           PA.CD_ONTOLOGIA7,
                           PA.CD_EXP_ATRIBUTO,
                           PS.NR_SEQUENCIA NR_SEQ_STAGE,
                           PS.NM_TABELA,
                           PS.NM_ATRIBUTO,
                           PS.VL_ATRIBUTO,
                           PS.VL_FK_1,
                           PS.VL_FK_2,
                           PS.VL_FK_3,
                           PS.VL_FK_4,
                           PS.DT_ATUALIZACAO
                      FROM PDE_ATTRIBUTE PA
                     INNER JOIN PDE_STAGE PS ON (PS.NM_TABELA = PA.NM_TABELA AND
                                                PS.NM_ATRIBUTO =
                                                PA.NM_ATRIBUTO)
                     INNER JOIN PDE_STAGE_LOG PSL ON (PSL.NR_SEQ_STAGE =
                                                     PS.NR_SEQUENCIA)
                     INNER JOIN PDE_LOG PL ON (PL.NR_SEQUENCIA =
                                              PSL.NR_SEQ_LOG)
                     WHERE PA.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_W
                       AND PA.NR_SEQUENCIA_SUP = NR_SEQ_SUP_P
                       AND PA.NM_ATRIBUTO IS NOT NULL
                       AND NVL(PA.IE_SITUACAO, 'A') = 'A'
                       AND PA.NM_TABELA_FK = VL_FK_ROW_FILHOS_P(I)
                    .NM_TABELA
                       AND NVL(PS.VL_FK_1, '#@#@') =
                           NVL(VL_FK_ROW_FILHOS_P(I).VL_FK_1, '#@#@')
                       AND NVL(PS.VL_FK_2, '#@#@') =
                           NVL(VL_FK_ROW_FILHOS_P(I).VL_FK_2, '#@#@')
                       AND NVL(PS.VL_FK_3, '#@#@') =
                           NVL(VL_FK_ROW_FILHOS_P(I).VL_FK_3, '#@#@')
                       AND NVL(PS.VL_FK_4, '#@#@') =
                           NVL(VL_FK_ROW_FILHOS_P(I).VL_FK_4, '#@#@')
                       AND PL.DT_JSON IS NULL
                       AND PL.DT_ENVIO IS NULL
                     ORDER BY PA.NR_SEQUENCIA, PS.DT_ATUALIZACAO) LOOP
            ADD_ATTRIB(A.CD_ONTOLOGIA,
                       A.CD_ONTOLOGIA2,
                       A.CD_ONTOLOGIA3,
                       A.CD_ONTOLOGIA4,
                       A.CD_ONTOLOGIA5,
                       A.CD_ONTOLOGIA6,
                       A.CD_ONTOLOGIA7,
                       A.CD_EXP_ATRIBUTO,
                       A.NM_TABELA,
                       A.NM_ATRIBUTO,
                       A.VL_ATRIBUTO,
                       A.VL_FK_1,
                       A.VL_FK_2,
                       A.VL_FK_3,
                       A.VL_FK_4,
                       A.DT_ATUALIZACAO);
          END LOOP;
        END LOOP;
      ELSIF (NVL(IE_FILHO_P, 'N') = 'N') THEN
        FOR A IN (SELECT PA.CD_ONTOLOGIA,
                         PA.CD_ONTOLOGIA2,
                         PA.CD_ONTOLOGIA3,
                         PA.CD_ONTOLOGIA4,
                         PA.CD_ONTOLOGIA5,
                         PA.CD_ONTOLOGIA6,
                         PA.CD_ONTOLOGIA7,
                         PA.CD_EXP_ATRIBUTO,
                         PS.NR_SEQUENCIA NR_SEQ_STAGE,
                         PS.NM_TABELA,
                         PS.NM_ATRIBUTO,
                         PS.VL_ATRIBUTO,
                         PS.VL_FK_1,
                         PS.VL_FK_2,
                         PS.VL_FK_3,
                         PS.VL_FK_4,
                         PS.DT_ATUALIZACAO
                    FROM PDE_ATTRIBUTE PA
                   INNER JOIN PDE_STAGE PS ON (PS.NM_TABELA = PA.NM_TABELA AND
                                              PS.NM_ATRIBUTO =
                                              PA.NM_ATRIBUTO)
                   INNER JOIN PDE_STAGE_LOG PSL ON (PSL.NR_SEQ_STAGE =
                                                   PS.NR_SEQUENCIA)
                   INNER JOIN PDE_LOG PL ON (PL.NR_SEQUENCIA =
                                            PSL.NR_SEQ_LOG)
                   WHERE PA.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_W
                     AND PA.NR_SEQUENCIA_SUP = NR_SEQ_SUP_P
                     AND PA.NM_ATRIBUTO IS NOT NULL
                     AND NVL(PA.IE_SITUACAO, 'A') = 'A'
                     AND ((CD_PESSOA_FISICA_W IS NULL AND
                         PS.CD_PESSOA_FISICA IS NULL) OR
                         PS.CD_PESSOA_FISICA = CD_PESSOA_FISICA_W)
                     AND ((NR_ATENDIMENTO_W IS NULL AND
                         PS.NR_ATENDIMENTO IS NULL) OR
                         PS.NR_ATENDIMENTO = NR_ATENDIMENTO_W)
                     AND ((NR_PRESCRICAO_W IS NULL AND
                         PS.NR_PRESCRICAO IS NULL) OR
                         PS.NR_PRESCRICAO = NR_PRESCRICAO_W)
                     AND PL.DT_JSON IS NULL
                     AND PL.DT_ENVIO IS NULL
                   ORDER BY PA.NR_SEQUENCIA, PS.DT_ATUALIZACAO) LOOP
          ADD_ATTRIB(A.CD_ONTOLOGIA,
                     A.CD_ONTOLOGIA2,
                     A.CD_ONTOLOGIA3,
                     A.CD_ONTOLOGIA4,
                     A.CD_ONTOLOGIA5,
                     A.CD_ONTOLOGIA6,
                     A.CD_ONTOLOGIA7,
                     A.CD_EXP_ATRIBUTO,
                     A.NM_TABELA,
                     A.NM_ATRIBUTO,
                     A.VL_ATRIBUTO,
                     A.VL_FK_1,
                     A.VL_FK_2,
                     A.VL_FK_3,
                     A.VL_FK_4,
                     A.DT_ATUALIZACAO);
        END LOOP;
      END IF;
    END BUSCA_ATRIBUTOS;
  
    PROCEDURE PROCESSA_ORDENS(PDE_ATTRIBUTE_P    PDE_ATTRIBUTE%ROWTYPE,
                              IE_FILHO_P         VARCHAR2 DEFAULT 'N',
                              VL_FK_ROW_FILHOS_P PDE.VL_FK_ROW_DATA_T DEFAULT NULL) IS
      JSON_ATRIBUTOS_W PHILIPS_JSON_LIST;
      JSON_ORDEM_W     PHILIPS_JSON := PHILIPS_JSON();
      VL_FK_ROW_DATA_W PDE.VL_FK_ROW_DATA_T := PDE.VL_FK_ROW_DATA_T();
      TABLES_W         VARCHAR2(4000) := '';
    BEGIN
      JSON_ORDEM_W.PUT('externalOrderNumber', DS_PEDIDO_EXT_W);
      JSON_ORDEM_W.PUT('code', PDE_ATTRIBUTE_P.CD_ONTOLOGIA);
      IF (PDE_ATTRIBUTE_P.NM_ATRIBUTO IS NULL) THEN
        JSON_ORDEM_W.PUT('name',
                         EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(NVL(PDE_ATTRIBUTE_P.CD_EXP_TABELA,
                                                                PDE_ATTRIBUTE_P.CD_EXP_TABELA_GRUPO)));
      ELSE
        JSON_ORDEM_W.PUT('name',
                         EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(NVL(PDE_ATTRIBUTE_P.CD_EXP_ATRIBUTO,
                                                                PDE_ATTRIBUTE_P.CD_EXP_ATRIBUTO_GRUPO)));
      
      END IF;
    
      JSON_ORDEM_W.PUT('system', 'SNM');
      JSON_ORDEM_W.PUT('date', TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
    
      IF (NVL(IE_FILHO_P, 'N') = 'N') THEN
        BUSCA_ATRIBUTOS(PDE_ATTRIBUTE_P.NR_SEQUENCIA,
                        JSON_ATRIBUTOS_W,
                        VL_FK_ROW_DATA_W);
      ELSE
        BUSCA_ATRIBUTOS(PDE_ATTRIBUTE_P.NR_SEQUENCIA,
                        JSON_ATRIBUTOS_W,
                        VL_FK_ROW_DATA_W,
                        'S',
                        VL_FK_ROW_FILHOS_P);
      END IF;
    
      IF (JSON_ATRIBUTOS_W.COUNT() > 0) THEN
        JSON_ORDEM_W.PUT('results', JSON_ATRIBUTOS_W.TO_JSON_VALUE());
      END IF;
    
      JSON_ORDENS_W.APPEND(JSON_ORDEM_W.TO_JSON_VALUE());
    
      FOR I IN 1 .. VL_FK_ROW_DATA_W.COUNT LOOP
        IF (TRIM(TABLES_W) IS NOT NULL) THEN
          TABLES_W := TABLES_W || ',';
        END IF;
      
        TABLES_W := TABLES_W || VL_FK_ROW_DATA_W(I).NM_TABELA;
      END LOOP;
    
      FOR F IN (SELECT CD_REGISTRO NM_TABELA
                  FROM TABLE(LISTA_PCK.OBTER_LISTA_CHAR(TABLES_W))) LOOP
        FOR G IN (SELECT PA.*
                    FROM PDE_ATTRIBUTE PA
                   WHERE PA.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_W
                     AND PA.NR_SEQUENCIA_SUP IS NULL
                     AND PA.NM_TABELA_FK = F.NM_TABELA
                     AND PA.NM_ATRIBUTO IS NULL
                     AND PA.NM_FK_TABELA_FK IS NOT NULL
                     AND NVL(PA.IE_SITUACAO, 'A') = 'A'
                     AND NVL(PA.IE_ENVIAR, 'S') = 'S'
                   ORDER BY PA.NR_SEQUENCIA) LOOP
          PROCESSA_ORDENS(G, 'S', VL_FK_ROW_DATA_W);
        END LOOP;
      END LOOP;
    
      IF (NVL(IE_FILHO_P, 'N') = 'N') THEN
        FOR G IN (SELECT PA.*
                    FROM PDE_ATTRIBUTE PA
                   WHERE PA.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_W
                     AND PA.NR_SEQUENCIA_SUP = PDE_ATTRIBUTE_P.NR_SEQUENCIA
                     AND PA.NM_ATRIBUTO IS NULL
                     AND NVL(PA.IE_SITUACAO, 'A') = 'A'
                     AND NVL(PA.IE_ENVIAR, 'S') = 'S'
                     AND PA.NM_TABELA_FK IS NULL
                     AND PA.NM_FK_TABELA_FK IS NULL
                   ORDER BY PA.NR_SEQUENCIA) LOOP
          PROCESSA_ORDENS(G);
        END LOOP;
      END IF;
    END PROCESSA_ORDENS;
  
  BEGIN
    LAST_NR_SEQ_W := NULL;
    FOR E IN (SELECT PL.NR_ATENDIMENTO, PL.NR_SEQUENCIA
                FROM PDE_LOG PL
               WHERE PL.CD_PESSOA_FISICA = CD_PESSOA_FISICA_P
                 AND PL.DT_JSON IS NULL
               ORDER BY PL.NR_ATENDIMENTO NULLS LAST, PL.NR_SEQUENCIA) LOOP
      IF (NVL(LAST_NR_SEQ_W, -123) <> NVL(E.NR_ATENDIMENTO, -123)) THEN
        NUMBER_SET_W.EXTEND;
        NUMBER_SET_W(NUMBER_SET_W.COUNT).NR_ATENDIMENTO := E.NR_ATENDIMENTO;
        NUMBER_SET_W(NUMBER_SET_W.COUNT).LIST_ID := E.NR_SEQUENCIA;
      ELSE
        IF (NUMBER_SET_W.COUNT = 0) THEN
          NUMBER_SET_W.EXTEND;
        END IF;
      
        NUMBER_SET_W(NUMBER_SET_W.COUNT).LIST_ID := NUMBER_SET_W(NUMBER_SET_W.COUNT)
                                                   .LIST_ID || ',' ||
                                                    E.NR_SEQUENCIA;
      END IF;
    END LOOP;
  
    TRATA_ARRAY;
  
    FOR I IN 1 .. NUMBER_SET_W.COUNT LOOP
      SELECT MAX(NR_REGISTRO)
        INTO LAST_NR_SEQ_W
        FROM TABLE(LISTA_PCK.OBTER_LISTA(NUMBER_SET_W(I).LIST_ID));
    
      FOR L IN (SELECT PL.NR_SEQ_PDE_MAIN,
                       PL.CD_PESSOA_FISICA,
                       PL.NR_ATENDIMENTO,
                       PL.NR_PRESCRICAO
                  FROM PDE_LOG PL
                 WHERE PL.NR_SEQUENCIA IN
                       (SELECT NR_REGISTRO
                          FROM TABLE(LISTA_PCK.OBTER_LISTA(NUMBER_SET_W(I)
                                                           .LIST_ID)))) LOOP
        NR_SEQ_PDE_MAIN_W  := L.NR_SEQ_PDE_MAIN;
        CD_PESSOA_FISICA_W := L.CD_PESSOA_FISICA;
        NR_ATENDIMENTO_W   := L.NR_ATENDIMENTO;
        NR_PRESCRICAO_W    := L.NR_PRESCRICAO;
        DS_PEDIDO_EXT_W    := NULL;
      
        IF (NVL(NR_PRESCRICAO_W, 0) <> 0) THEN
          SELECT PM.DS_PEDIDO_EXT
            INTO DS_PEDIDO_EXT_W
            FROM PRESCR_MEDICA PM
           WHERE PM.NR_PRESCRICAO = NR_PRESCRICAO_W;
        END IF;
      
        FOR G IN (SELECT PA.*
                    FROM PDE_ATTRIBUTE PA
                   WHERE PA.NR_SEQ_PDE_MAIN = L.NR_SEQ_PDE_MAIN
                     AND PA.NR_SEQUENCIA_SUP IS NULL
                     AND NVL(PA.IE_SITUACAO, 'A') = 'A'
                     AND NVL(PA.IE_ENVIAR, 'S') = 'S'
                     AND PA.NM_TABELA_FK IS NULL
                     AND PA.NM_FK_TABELA_FK IS NULL
                   ORDER BY PA.NR_SEQUENCIA) LOOP
          PROCESSA_ORDENS(G);
        END LOOP;
      END LOOP;
    
      IF (IE_POSSUI_ATRIB_W = 'S') THEN
        JSON_PATIENT_W           := PERSON_JSON_PCK.GET_PERSON(CD_PESSOA_FISICA_P);
        JSON_ENCOUNTER_W         := ENCOUNTER_JSON_PCK.GET_ENCOUNTER(NUMBER_SET_W(I)
                                                                     .NR_ATENDIMENTO);
        JSON_PERSON_IDENTIFIER_W := PERSON_JSON_PCK.GET_PERSON_IDENTIFIER(CD_PESSOA_FISICA_P);
      
        JSON_W.PUT('patient', JSON_PATIENT_W.TO_JSON_VALUE());
        JSON_W.PUT('patientIdentifier',
                   JSON_PERSON_IDENTIFIER_W.TO_JSON_VALUE());
        JSON_W.PUT('visit', JSON_ENCOUNTER_W.TO_JSON_VALUE());
        JSON_W.PUT('orders', JSON_ORDENS_W.TO_JSON_VALUE());
      
        DBMS_LOB.CREATETEMPORARY(DS_JSON_W, TRUE);
        JSON_W.TO_CLOB(DS_JSON_W);
      
        UPDATE PDE_LOG PL
           SET PL.DS_JSON        = DS_JSON_W,
               PL.DT_JSON        = SYSDATE,
               PL.DT_ENVIO       = DECODE(LAST_NR_SEQ_W,
                                          PL.NR_SEQUENCIA,
                                          NULL,
                                          SYSDATE),
               PL.NM_USUARIO     = NVL(WHEB_USUARIO_PCK.GET_NM_USUARIO,
                                       PL.NM_USUARIO),
               PL.DT_ATUALIZACAO = SYSDATE
         WHERE PL.NR_SEQUENCIA IN
               (SELECT NR_REGISTRO
                  FROM TABLE(LISTA_PCK.OBTER_LISTA(NUMBER_SET_W(I).LIST_ID)));
      
        COMMIT;
      END IF;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      PDE.GRAVA_LOG_ERRO('GERAR.JSON.ERROR',
                         'CD_PESSOA_FISICA_P: ' || CD_PESSOA_FISICA_P,
                         SQLCODE,
                         SQLERRM);
  END GERAR_JSON;

  PROCEDURE OBTER_DESCRICAO_CAMPO(NM_TABELA_P   VARCHAR2,
                                  NM_ATRIBUTO_P VARCHAR2,
                                  VALOR_P       VARCHAR2,
                                  RET_VALOR_P   OUT VARCHAR2) IS
    DOMINIO_W NUMBER;
  BEGIN
    CASE DOMINIO_W
      WHEN 1 THEN
        SELECT A.CD_DOMINIO
          INTO DOMINIO_W
          FROM TABELA_ATRIBUTO A
         WHERE A.NM_TABELA = NM_TABELA_P
           AND A.NM_ATRIBUTO = NM_ATRIBUTO_P;
      
        RET_VALOR_P := NVL(OBTER_DESCRICAO_DOMINIO(DOMINIO_W, VALOR_P),
                           VALOR_P);
      WHEN 2 THEN
        BEGIN
          SELECT CASE
                   WHEN OBTER_DESC_EXPRESSAO(ATRIB.CD_EXP_VALORES) = 'S,N' THEN
                    DECODE(VALOR_P,
                           'S',
                           OBTER_DESC_EXPRESSAO(327113),
                           'N',
                           OBTER_DESC_EXPRESSAO(327114),
                           NULL)
                   ELSE
                    VALOR_EXPRESSAO(OBTER_DESC_EXPRESSAO(ATRIB.CD_EXP_VALORES),
                                    VALOR_P)
                 END AS VALOR_EXPRESSAO
            INTO RET_VALOR_P
            FROM RES_CADASTRO_ONTOLOGIA_PHI PHI
            JOIN ONTOLOGIA_TABELA_ATRIBUTO ATRIB ON ATRIB.NM_TABELA =
                                                    PHI.NM_TABELA
                                                AND ATRIB.NM_ATRIBUTO =
                                                    PHI.NM_ATRIBUTO
           WHERE PHI.NM_TABELA = NM_TABELA_P
             AND PHI.NM_ATRIBUTO = NM_ATRIBUTO_P
             AND PHI.CD_VALOR_TASY = VALOR_P
             AND ATRIB.CD_EXP_VALORES IS NOT NULL
           GROUP BY PHI.CD_VALOR_ONTOLOGIA, ATRIB.CD_EXP_VALORES;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            RET_VALOR_P := VALOR_P;
        END;
      ELSE
        RET_VALOR_P := VALOR_P;
    END CASE;
  END OBTER_DESCRICAO_CAMPO;

  PROCEDURE PATIENT_FINDER(IDENTIFIER_P VARCHAR2,
                           TYPE_IN_P    CHAR,
                           TYPE_OUT_P   CHAR,
                           PATIENT_P    OUT VARCHAR2) IS
    /* TYPE_IN_P / TYPE_OUT_P:
      PF - PESSOA FISICA
      AT - ATENDIMENTO
      PR - PRESCRICAO
      SP - SEQUENCIA PRESCRICAO
    */
  BEGIN
    IF TYPE_IN_P = 'PF' AND TYPE_OUT_P = 'AT' THEN
      PATIENT_P := OBTER_ATENDIMENTO_PACIENTE(IDENTIFIER_P,
                                              NVL(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO,
                                                  1));
    ELSIF TYPE_IN_P = 'AT' AND TYPE_OUT_P = 'PF' THEN
      PATIENT_P := OBTER_PESSOA_ATENDIMENTO(IDENTIFIER_P, 'C');
    ELSIF TYPE_IN_P = 'PR' AND TYPE_OUT_P = 'PF' THEN
      SELECT MAX(CD_PESSOA_FISICA)
        INTO PATIENT_P
        FROM PRESCR_MEDICA
       WHERE CD_ESTABELECIMENTO =
             NVL(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, 1)
         AND NR_PRESCRICAO = TO_NUMBER(IDENTIFIER_P);
    ELSIF TYPE_IN_P = 'PR' AND TYPE_OUT_P = 'AT' THEN
      SELECT MAX(NR_ATENDIMENTO)
        INTO PATIENT_P
        FROM PRESCR_MEDICA
       WHERE CD_ESTABELECIMENTO =
             NVL(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, 1)
         AND NR_PRESCRICAO = TO_NUMBER(IDENTIFIER_P);
    END IF;
  END PATIENT_FINDER;

  FUNCTION VALOR_EXPRESSAO(DS_STRING_P VARCHAR2, VL_DOMINIO_P VARCHAR2)
    RETURN VARCHAR IS
    STR_W        VARCHAR2(1000);
    ROW_W        VARCHAR2(1000);
    DS_RETORNO_W VARCHAR2(1000);
    GROUP1_W     VARCHAR2(1000);
    GROUP2_W     VARCHAR2(1000);
  BEGIN
    SELECT SUBSTR(DS_STRING_P, 1, INSTR(DS_STRING_P, ')'))
      INTO GROUP1_W
      FROM DUAL;
  
    SELECT SUBSTR(DS_STRING_P, LENGTH(GROUP1_W) + 1, LENGTH(DS_STRING_P))
      INTO GROUP2_W
      FROM DUAL;
  
    SELECT *
      INTO STR_W, ROW_W
      FROM (WITH DATA AS (SELECT (SELECT SUBSTR(GROUP1_W,
                                                2,
                                                INSTR(GROUP1_W, ')') - 2)
                                    FROM DUAL) STR
                            FROM DUAL)
             SELECT TRIM(REGEXP_SUBSTR(STR, '[^,]+', 1, LEVEL)) STR, ROWNUM
               FROM DATA
             CONNECT BY INSTR(STR, ',', 1, LEVEL - 1) > 0)
              WHERE STR = VL_DOMINIO_P;
  
  
    SELECT *
      INTO DS_RETORNO_W, ROW_W
      FROM (WITH DATA AS (SELECT (SELECT SUBSTR(GROUP2_W,
                                                2,
                                                INSTR(GROUP2_W, ')') - 2)
                                    FROM DUAL) STR
                            FROM DUAL)
             SELECT TRIM(REGEXP_SUBSTR(STR, '[^,]+', 1, LEVEL)) STR,
                    ROWNUM AS LINHA
               FROM DATA
             CONNECT BY INSTR(STR, ',', 1, LEVEL - 1) > 0)
              WHERE LINHA = ROW_W;
  
  
    RETURN DS_RETORNO_W;
  END VALOR_EXPRESSAO;

  PROCEDURE ENVIAR_INTEGRACAO(NR_SEQ_PDE_LOG_P NUMBER) IS
    DS_JSON_W    PDE_LOG.DS_JSON%TYPE;
    NM_USUARIO_W PDE_LOG.NM_USUARIO%TYPE;
  BEGIN
    SELECT PL.DS_JSON, PL.NM_USUARIO
      INTO DS_JSON_W, NM_USUARIO_W
      FROM PDE_LOG PL
     WHERE PL.NR_SEQUENCIA = NR_SEQ_PDE_LOG_P
       AND PL.DS_JSON IS NOT NULL
       AND PL.DT_JSON IS NOT NULL;
  
    DS_JSON_W := BIFROST.SEND_INTEGRATION_CONTENT('patient.data.export',
                                                  DS_JSON_W,
                                                  NVL(NM_USUARIO_W,
                                                      WHEB_USUARIO_PCK.GET_NM_USUARIO));
  
    UPDATE PDE_LOG PL
       SET PL.DT_ENVIO       = SYSDATE,
           PL.NM_USUARIO     = NVL(WHEB_USUARIO_PCK.GET_NM_USUARIO,
                                   PL.NM_USUARIO),
           PL.DT_ATUALIZACAO = SYSDATE
     WHERE PL.NR_SEQUENCIA = NR_SEQ_PDE_LOG_P;
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      PDE.GRAVA_LOG_ERRO('ENVIAR.INTEGRACAO.ERROR',
                         'NR_SEQ_PDE_LOG_P: ' || NR_SEQ_PDE_LOG_P,
                         SQLCODE,
                         SQLERRM);
  END ENVIAR_INTEGRACAO;

  PROCEDURE ENVIAR_INTEGRACAO_BY_PDE_MAIN(NR_SEQ_PDE_MAIN_P NUMBER) IS
  BEGIN
    FOR X IN (SELECT PL.NR_SEQUENCIA
                FROM PDE_LOG PL
               WHERE PL.NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_P
                 AND PL.DT_JSON IS NOT NULL
                 AND PL.DT_ENVIO IS NULL
               ORDER BY PL.NR_SEQUENCIA) LOOP
      ENVIAR_INTEGRACAO(X.NR_SEQUENCIA);
    END LOOP;
  END ENVIAR_INTEGRACAO_BY_PDE_MAIN;

  PROCEDURE DELETE_ATTRIBUTE(NR_SEQ_PDE_ATTRIBUTE_P IN NUMBER) IS
    NR_SEQ_GROUP_W       PDE_ATTRIBUTE.NR_SEQUENCIA%TYPE;
    NR_SEQ_TABLE_W       PDE_ATTRIBUTE.NR_SEQUENCIA%TYPE;
    QT_ATTRIBUTE_FOUND_W NUMBER(10);
    QT_TABLE_FOUND_W     NUMBER(10);
    NR_SEQUENCIA_W       PDE_ATTRIBUTE.NR_SEQUENCIA%TYPE;
    VALID_TABLE_W        PDE_ATTRIBUTE.NM_TABELA%TYPE;
    VALID_PDE_MAIN_W     PDE_ATTRIBUTE.NR_SEQ_PDE_MAIN%TYPE;
    VALID_QT_ATRIB_W     NUMBER(10);
  
    CURSOR CUR_ATT_GROUP_CHILDREN IS
      SELECT NR_SEQUENCIA NR_SEQ_ATTRIBUTE
        FROM PDE_ATTRIBUTE
       WHERE NR_SEQUENCIA_SUP = NR_SEQ_PDE_ATTRIBUTE_P;
  
    ROW_ATT_GROUP_CHILD CUR_ATT_GROUP_CHILDREN%ROWTYPE;
  BEGIN
    --VALIDAR TABELA E PDE_MAIN REFERENTES
    SELECT DISTINCT MAX(NR_SEQ_PDE_MAIN), MAX(NM_TABELA)
      INTO VALID_PDE_MAIN_W, VALID_TABLE_W
      FROM PDE_ATTRIBUTE
     WHERE NR_SEQUENCIA = NR_SEQ_PDE_ATTRIBUTE_P;
  
    SELECT MAX(GRP.NR_SEQUENCIA) NR_SEQ_GROUP,
           MAX(TBL.NR_SEQUENCIA) NR_SEQ_TABLE
      INTO NR_SEQ_GROUP_W, NR_SEQ_TABLE_W
      FROM PDE_ATTRIBUTE ATT
      LEFT JOIN PDE_ATTRIBUTE TBL ON ATT.NR_SEQUENCIA_SUP =
                                     TBL.NR_SEQUENCIA
      LEFT JOIN PDE_ATTRIBUTE GRP ON GRP.NR_SEQUENCIA =
                                     TBL.NR_SEQUENCIA_SUP
     WHERE ATT.NR_SEQUENCIA = NR_SEQ_PDE_ATTRIBUTE_P;
  
    OPEN CUR_ATT_GROUP_CHILDREN;
    LOOP
      FETCH CUR_ATT_GROUP_CHILDREN
        INTO ROW_ATT_GROUP_CHILD;
      EXIT WHEN CUR_ATT_GROUP_CHILDREN%NOTFOUND;
      --VERIFICAR SE O ATRIBUTO A SER DELETADO E UM ATRIBUTO OBRIGATORIO.
      SELECT MAX(NR_SEQUENCIA)
        INTO NR_SEQUENCIA_W
        FROM PDE_ATTRIBUTE
       WHERE NR_SEQUENCIA = ROW_ATT_GROUP_CHILD.NR_SEQ_ATTRIBUTE
         AND NM_ATRIBUTO IN ('NR_ATENDIMENTO', 'CD_PESSOA_FISICA',
              'NR_PRESCRICAO', 'NR_SEQ_PRESCR')
         AND IE_ENVIAR = 'S';
      --CASO NAO, UPDATE DO IE_ENVIAR PARA NAO
      IF NR_SEQUENCIA_W IS NOT NULL THEN
        UPDATE PDE_ATTRIBUTE
           SET IE_ENVIAR = 'N'
         WHERE NR_SEQUENCIA = NR_SEQUENCIA_W;
      ELSE
        DELETE FROM PDE_ATTRIBUTE
         WHERE NR_SEQUENCIA = ROW_ATT_GROUP_CHILD.NR_SEQ_ATTRIBUTE;
      END IF;
    END LOOP;
  
    CLOSE CUR_ATT_GROUP_CHILDREN;
  
    --VERIFICAR SE O ATRIBUTO A SER DELETADO E UM ATRIBUTO OBRIGATORIO.
    SELECT MAX(NR_SEQUENCIA)
      INTO NR_SEQUENCIA_W
      FROM PDE_ATTRIBUTE
     WHERE NR_SEQUENCIA = NR_SEQ_PDE_ATTRIBUTE_P
       AND NM_ATRIBUTO IN ('NR_ATENDIMENTO', 'CD_PESSOA_FISICA',
            'NR_PRESCRICAO', 'NR_SEQ_PRESCR')
       AND IE_ENVIAR = 'S';
    --FACA UM UPDATE DO IE_ENVIAR PARA N
    IF NR_SEQUENCIA_W IS NOT NULL THEN
      UPDATE PDE_ATTRIBUTE
         SET IE_ENVIAR = 'N'
       WHERE NR_SEQUENCIA = NR_SEQUENCIA_W;
    ELSE
      DELETE FROM PDE_ATTRIBUTE
       WHERE NR_SEQUENCIA = NR_SEQ_PDE_ATTRIBUTE_P;
    END IF;
  
    SELECT COUNT(1)
      INTO QT_ATTRIBUTE_FOUND_W
      FROM PDE_ATTRIBUTE
     WHERE NR_SEQUENCIA_SUP = NR_SEQ_TABLE_W;
  
    IF QT_ATTRIBUTE_FOUND_W = 0 THEN
      DELETE FROM PDE_ATTRIBUTE WHERE NR_SEQUENCIA = NR_SEQ_TABLE_W;
    END IF;
  
    SELECT COUNT(1)
      INTO QT_TABLE_FOUND_W
      FROM PDE_ATTRIBUTE
     WHERE NR_SEQUENCIA_SUP = NR_SEQ_GROUP_W;
  
    IF QT_TABLE_FOUND_W = 0 THEN
      DELETE FROM PDE_ATTRIBUTE WHERE NR_SEQUENCIA = NR_SEQ_GROUP_W;
    END IF;
  
    SELECT COUNT(1)
      INTO VALID_QT_ATRIB_W
      FROM PDE_ATTRIBUTE
     WHERE NM_TABELA = VALID_TABLE_W
       AND NM_ATRIBUTO IS NOT NULL
       AND IE_ENVIAR = 'S';
  
    IF VALID_QT_ATRIB_W = 0 THEN
      DELETE FROM PDE_ATTRIBUTE
       WHERE NR_SEQ_PDE_MAIN = VALID_PDE_MAIN_W
         AND NM_TABELA = VALID_TABLE_W;
    
      DROP_INVALID_TRIGGERS;
    END IF;
  
    FOR X IN (SELECT DISTINCT PA.NR_SEQ_PDE_MAIN
                FROM PDE_ATTRIBUTE PA
               WHERE PA.NM_TABELA = VALID_TABLE_W) LOOP
      UPDATE PDE_MAIN PM
         SET PM.IE_STATUS = 'P'
       WHERE PM.NR_SEQUENCIA = X.NR_SEQ_PDE_MAIN;
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      PDE.GRAVA_LOG_ERRO('DELETE.ATTRIBUTE.ERROR',
                         'NR_SEQ_PDE_ATTRIBUTE_P: ' ||
                         NR_SEQ_PDE_ATTRIBUTE_P,
                         SQLCODE,
                         SQLERRM);
  END DELETE_ATTRIBUTE;

  PROCEDURE SAVE_ATTRIBUTE(NR_SEQ_PDE_ATTRIBUTE_P   IN NUMBER,
                           NR_SEQ_PDE_MAIN_P        IN NUMBER,
                           IE_TYPE_P                IN VARCHAR2,
                           NR_SEQ_PARENT_P          IN NUMBER,
                           VL_GROUP_DOMAIN_P        IN VARCHAR2,
                           NR_SEQ_ONTOL_TAB_P       IN NUMBER,
                           NR_SEQ_ONTOL_TAB_ATT_P   IN NUMBER,
                           NR_SEQ_ONTOL_ATT_GROUP_P IN NUMBER,
                           NM_USER_P                IN VARCHAR2,
                           NR_NEW_SEQUENCE_P        OUT NUMBER) IS
  
    NM_TABLE_W               PDE_ATTRIBUTE.NM_TABELA%TYPE;
    CD_ONTOLOGY_W            PDE_ATTRIBUTE.CD_ONTOLOGIA%TYPE;
    CD_ONTOLOGY2_W           PDE_ATTRIBUTE.CD_ONTOLOGIA2%TYPE;
    CD_ONTOLOGY3_W           PDE_ATTRIBUTE.CD_ONTOLOGIA3%TYPE;
    CD_ONTOLOGY4_W           PDE_ATTRIBUTE.CD_ONTOLOGIA4%TYPE;
    CD_ONTOLOGY5_W           PDE_ATTRIBUTE.CD_ONTOLOGIA5%TYPE;
    CD_ONTOLOGY6_W           PDE_ATTRIBUTE.CD_ONTOLOGIA6%TYPE;
    CD_ONTOLOGY7_W           PDE_ATTRIBUTE.CD_ONTOLOGIA7%TYPE;
    CD_EXP_ATTRIBUTE_GROUP_W PDE_ATTRIBUTE.CD_EXP_ATRIBUTO_GRUPO%TYPE;
    CD_EXP_TABLE_W           PDE_ATTRIBUTE.CD_EXP_TABELA%TYPE;
    CD_EXP_ATTRIBUTE_W       PDE_ATTRIBUTE.CD_EXP_ATRIBUTO%TYPE;
    NR_SEQ_PDE_ATT_CHILD_W   PDE_ATTRIBUTE.NR_SEQUENCIA%TYPE;
  
    CURSOR CUR_ATT_GROUP IS
      SELECT OTA.NR_SEQUENCIA           NR_SEQ_TAB_ATTRIBUTE,
             ONTPHI.CD_VALOR_ONTOLOGIA  CD_ONTOLOGY,
             ONTPHI.CD_VALOR_ONTOLOGIA2 CD_ONTOLOGY2,
             ONTPHI.CD_VALOR_ONTOLOGIA3 CD_ONTOLOGY3,
             ONTPHI.CD_VALOR_ONTOLOGIA4 CD_ONTOLOGY4,
             ONTPHI.CD_VALOR_ONTOLOGIA5 CD_ONTOLOGY5,
             ONTPHI.CD_VALOR_ONTOLOGIA6 CD_ONTOLOGY6,
             ONTPHI.CD_VALOR_ONTOLOGIA7 CD_ONTOLOGY7
        FROM ONTOLOGIA_TABELA_ATRIBUTO OTA
       INNER JOIN RES_CADASTRO_ONTOLOGIA_PHI ONTPHI ON OTA.NM_TABELA =
                                                       ONTPHI.NM_TABELA
                                                   AND OTA.NM_ATRIBUTO =
                                                       ONTPHI.NM_ATRIBUTO
       WHERE NR_SEQ_ONTOLO_TAB_ATRIB_GRUPO = NR_SEQ_ONTOL_ATT_GROUP_P;
  
    ROW_ATT_GROUP CUR_ATT_GROUP%ROWTYPE;
  BEGIN
    IF NR_SEQ_ONTOL_TAB_P IS NOT NULL THEN
      SELECT NM_TABELA, CD_EXP_CADASTRO
        INTO NM_TABLE_W, CD_EXP_TABLE_W
        FROM ONTOLOGIA_TABELA
       WHERE NR_SEQUENCIA = NR_SEQ_ONTOL_TAB_P;
    END IF;
  
    IF NR_SEQ_ONTOL_TAB_ATT_P IS NOT NULL THEN
      SELECT CD_EXP_DESC
        INTO CD_EXP_ATTRIBUTE_W
        FROM ONTOLOGIA_TABELA_ATRIBUTO
       WHERE NR_SEQUENCIA = NR_SEQ_ONTOL_TAB_ATT_P;
    END IF;
  
    IF NR_SEQ_ONTOL_ATT_GROUP_P IS NOT NULL THEN
      SELECT CD_EXP_ATRIBUTO_GRUPO
        INTO CD_EXP_ATTRIBUTE_GROUP_W
        FROM ONTOLOGIA_TAB_ATRIB_GRUPO
       WHERE NR_SEQUENCIA = NR_SEQ_ONTOL_ATT_GROUP_P;
    END IF;
  
    IF IE_TYPE_P = 'GROUP' THEN
      GET_ONTOLOGY_CODE(NULL,
                        VL_GROUP_DOMAIN_P,
                        IE_TYPE_P,
                        CD_ONTOLOGY_W,
                        CD_ONTOLOGY2_W,
                        CD_ONTOLOGY3_W,
                        CD_ONTOLOGY4_W,
                        CD_ONTOLOGY5_W,
                        CD_ONTOLOGY6_W,
                        CD_ONTOLOGY7_W);
    
      INSERT_ATTRIBUTE(NR_SEQ_PDE_MAIN_P,
                       NR_SEQ_PARENT_P,
                       VL_GROUP_DOMAIN_P,
                       NR_SEQ_ONTOL_TAB_P,
                       NULL,
                       NR_SEQ_ONTOL_ATT_GROUP_P,
                       NULL,
                       NULL,
                       CD_ONTOLOGY_W,
                       CD_ONTOLOGY2_W,
                       CD_ONTOLOGY3_W,
                       CD_ONTOLOGY4_W,
                       CD_ONTOLOGY5_W,
                       CD_ONTOLOGY6_W,
                       CD_ONTOLOGY7_W,
                       NM_USER_P,
                       NR_NEW_SEQUENCE_P);
    ELSIF IE_TYPE_P = 'TABLE' THEN
      GET_ONTOLOGY_CODE(NR_SEQ_ONTOL_TAB_P,
                        VL_GROUP_DOMAIN_P,
                        IE_TYPE_P,
                        CD_ONTOLOGY_W,
                        CD_ONTOLOGY2_W,
                        CD_ONTOLOGY3_W,
                        CD_ONTOLOGY4_W,
                        CD_ONTOLOGY5_W,
                        CD_ONTOLOGY6_W,
                        CD_ONTOLOGY7_W);
    
      INSERT_ATTRIBUTE(NR_SEQ_PDE_MAIN_P,
                       NR_SEQ_PARENT_P,
                       VL_GROUP_DOMAIN_P,
                       NR_SEQ_ONTOL_TAB_P,
                       NULL,
                       NR_SEQ_ONTOL_ATT_GROUP_P,
                       NULL,
                       NULL,
                       CD_ONTOLOGY_W,
                       CD_ONTOLOGY2_W,
                       CD_ONTOLOGY3_W,
                       CD_ONTOLOGY4_W,
                       CD_ONTOLOGY5_W,
                       CD_ONTOLOGY6_W,
                       CD_ONTOLOGY7_W,
                       NM_USER_P,
                       NR_NEW_SEQUENCE_P);
    ELSIF IE_TYPE_P = 'ATTRIBUTE_GROUP' THEN
      GET_ONTOLOGY_CODE(TO_CHAR(NR_SEQ_ONTOL_ATT_GROUP_P),
                        VL_GROUP_DOMAIN_P,
                        IE_TYPE_P,
                        CD_ONTOLOGY_W,
                        CD_ONTOLOGY2_W,
                        CD_ONTOLOGY3_W,
                        CD_ONTOLOGY4_W,
                        CD_ONTOLOGY5_W,
                        CD_ONTOLOGY6_W,
                        CD_ONTOLOGY7_W);
    
      INSERT_ATTRIBUTE(NR_SEQ_PDE_MAIN_P,
                       NR_SEQ_PARENT_P,
                       VL_GROUP_DOMAIN_P,
                       NR_SEQ_ONTOL_TAB_P,
                       NULL,
                       NR_SEQ_ONTOL_ATT_GROUP_P,
                       NULL,
                       NULL,
                       CD_ONTOLOGY_W,
                       CD_ONTOLOGY2_W,
                       CD_ONTOLOGY3_W,
                       CD_ONTOLOGY4_W,
                       CD_ONTOLOGY5_W,
                       CD_ONTOLOGY6_W,
                       CD_ONTOLOGY7_W,
                       NM_USER_P,
                       NR_NEW_SEQUENCE_P);
    
      OPEN CUR_ATT_GROUP;
      LOOP
        FETCH CUR_ATT_GROUP
          INTO ROW_ATT_GROUP;
        EXIT WHEN CUR_ATT_GROUP%NOTFOUND;
        INSERT_ATTRIBUTE(NR_SEQ_PDE_MAIN_P,
                         NR_NEW_SEQUENCE_P,
                         VL_GROUP_DOMAIN_P,
                         NR_SEQ_ONTOL_TAB_P,
                         ROW_ATT_GROUP.NR_SEQ_TAB_ATTRIBUTE,
                         NR_SEQ_ONTOL_ATT_GROUP_P,
                         NULL,
                         NULL,
                         ROW_ATT_GROUP.CD_ONTOLOGY,
                         ROW_ATT_GROUP.CD_ONTOLOGY2,
                         ROW_ATT_GROUP.CD_ONTOLOGY3,
                         ROW_ATT_GROUP.CD_ONTOLOGY4,
                         ROW_ATT_GROUP.CD_ONTOLOGY5,
                         ROW_ATT_GROUP.CD_ONTOLOGY6,
                         ROW_ATT_GROUP.CD_ONTOLOGY7,
                         NM_USER_P,
                         NR_SEQ_PDE_ATT_CHILD_W);
      END LOOP;
    
      CLOSE CUR_ATT_GROUP;
    ELSE
      GET_ONTOLOGY_CODE(TO_CHAR(NR_SEQ_ONTOL_TAB_ATT_P),
                        VL_GROUP_DOMAIN_P,
                        'ATTRIBUTE',
                        CD_ONTOLOGY_W,
                        CD_ONTOLOGY2_W,
                        CD_ONTOLOGY3_W,
                        CD_ONTOLOGY4_W,
                        CD_ONTOLOGY5_W,
                        CD_ONTOLOGY6_W,
                        CD_ONTOLOGY7_W);
    
      INSERT_ATTRIBUTE(NR_SEQ_PDE_MAIN_P,
                       NR_SEQ_PARENT_P,
                       VL_GROUP_DOMAIN_P,
                       NR_SEQ_ONTOL_TAB_P,
                       NR_SEQ_ONTOL_TAB_ATT_P,
                       NR_SEQ_ONTOL_ATT_GROUP_P,
                       NULL,
                       NULL,
                       CD_ONTOLOGY_W,
                       CD_ONTOLOGY2_W,
                       CD_ONTOLOGY3_W,
                       CD_ONTOLOGY4_W,
                       CD_ONTOLOGY5_W,
                       CD_ONTOLOGY6_W,
                       CD_ONTOLOGY7_W,
                       NM_USER_P,
                       NR_NEW_SEQUENCE_P);
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PDE.GRAVA_LOG_ERRO('SAVE.ATTRIBUTE.ERROR',
                         'NR_SEQ_PDE_ATTRIBUTE_P: ' ||
                         NR_SEQ_PDE_ATTRIBUTE_P || ', NR_SEQ_PDE_MAIN_P: ' ||
                         NR_SEQ_PDE_MAIN_P || ', IE_TYPE_P: ' || IE_TYPE_P ||
                         ', NR_SEQ_PARENT_P: ' || NR_SEQ_PARENT_P ||
                         ', VL_GROUP_DOMAIN_P: ' || VL_GROUP_DOMAIN_P ||
                         ', NR_SEQ_ONTOL_TAB_P: ' || NR_SEQ_ONTOL_TAB_P ||
                         ', NR_SEQ_ONTOL_TAB_ATT_P: ' ||
                         NR_SEQ_ONTOL_TAB_ATT_P ||
                         ', NR_SEQ_ONTOL_ATT_GROUP_P: ' ||
                         NR_SEQ_ONTOL_ATT_GROUP_P || ', NM_USER_P: ' ||
                         NM_USER_P,
                         SQLCODE,
                         SQLERRM);
  END SAVE_ATTRIBUTE;

  PROCEDURE GET_ONTOLOGY_CODE(CD_SEQ_RECORD_P IN VARCHAR2,
                              IE_GROUP_P      IN VARCHAR2,
                              DS_TYPE_P       IN VARCHAR2,
                              CD_ONTOLOGY_P   OUT VARCHAR2,
                              CD_ONTOLOGY2_P  OUT VARCHAR2,
                              CD_ONTOLOGY3_P  OUT VARCHAR2,
                              CD_ONTOLOGY4_P  OUT VARCHAR2,
                              CD_ONTOLOGY5_P  OUT VARCHAR2,
                              CD_ONTOLOGY6_P  OUT VARCHAR2,
                              CD_ONTOLOGY7_P  OUT VARCHAR2) IS
  BEGIN
    IF DS_TYPE_P = 'GROUP' THEN
      SELECT DISTINCT MAX(PHI.CD_VALOR_ONTOLOGIA),
                      MAX(PHI.CD_VALOR_ONTOLOGIA2),
                      MAX(PHI.CD_VALOR_ONTOLOGIA3),
                      MAX(PHI.CD_VALOR_ONTOLOGIA4),
                      MAX(PHI.CD_VALOR_ONTOLOGIA5),
                      MAX(PHI.CD_VALOR_ONTOLOGIA6),
                      MAX(PHI.CD_VALOR_ONTOLOGIA7)
        INTO CD_ONTOLOGY_P,
             CD_ONTOLOGY2_P,
             CD_ONTOLOGY3_P,
             CD_ONTOLOGY4_P,
             CD_ONTOLOGY5_P,
             CD_ONTOLOGY6_P,
             CD_ONTOLOGY7_P
        FROM RES_CADASTRO_ONTOLOGIA_PHI PHI
       INNER JOIN VALOR_DOMINIO_V VD ON VD.DS_VALOR_DOMINIO =
                                        PHI.NM_ATRIBUTO
       WHERE VD.VL_DOMINIO = IE_GROUP_P
         AND PHI.IE_ONTOLOGIA = 'SNO'
         AND PHI.NM_TABELA = 'ONTOLOGIA_LISTA_W'
         AND PHI.NM_ATRIBUTO = 'NULL'
         AND PHI.NR_SEQ_ATRIB_GRUPO IS NULL
         AND PHI.CD_VALOR_TASY IS NULL
         AND ROWNUM = 1;
    
    ELSIF DS_TYPE_P = 'TABLE' THEN
      SELECT DISTINCT MAX(PHI.CD_VALOR_ONTOLOGIA),
                      MAX(PHI.CD_VALOR_ONTOLOGIA2),
                      MAX(PHI.CD_VALOR_ONTOLOGIA3),
                      MAX(PHI.CD_VALOR_ONTOLOGIA4),
                      MAX(PHI.CD_VALOR_ONTOLOGIA5),
                      MAX(PHI.CD_VALOR_ONTOLOGIA6),
                      MAX(PHI.CD_VALOR_ONTOLOGIA7)
        INTO CD_ONTOLOGY_P,
             CD_ONTOLOGY2_P,
             CD_ONTOLOGY3_P,
             CD_ONTOLOGY4_P,
             CD_ONTOLOGY5_P,
             CD_ONTOLOGY6_P,
             CD_ONTOLOGY7_P
        FROM RES_CADASTRO_ONTOLOGIA_PHI PHI
       INNER JOIN ONTOLOGIA_TABELA OT ON PHI.NM_TABELA = OT.NM_TABELA
       WHERE OT.NR_SEQUENCIA = TO_NUMBER(CD_SEQ_RECORD_P)
         AND IE_ONTOLOGIA = 'SNO'
         AND PHI.NM_ATRIBUTO = 'NULL'
         AND PHI.NR_SEQ_ATRIB_GRUPO IS NULL
         AND PHI.CD_VALOR_TASY IS NULL;
    
    ELSIF DS_TYPE_P = 'ATTRIBUTE_GROUP' THEN
      SELECT DISTINCT MAX(PHI.CD_VALOR_ONTOLOGIA),
                      MAX(PHI.CD_VALOR_ONTOLOGIA2),
                      MAX(PHI.CD_VALOR_ONTOLOGIA3),
                      MAX(PHI.CD_VALOR_ONTOLOGIA4),
                      MAX(PHI.CD_VALOR_ONTOLOGIA5),
                      MAX(PHI.CD_VALOR_ONTOLOGIA6),
                      MAX(PHI.CD_VALOR_ONTOLOGIA7)
        INTO CD_ONTOLOGY_P,
             CD_ONTOLOGY2_P,
             CD_ONTOLOGY3_P,
             CD_ONTOLOGY4_P,
             CD_ONTOLOGY5_P,
             CD_ONTOLOGY6_P,
             CD_ONTOLOGY7_P
        FROM RES_CADASTRO_ONTOLOGIA_PHI PHI
       INNER JOIN ONTOLOGIA_TABELA_ATRIBUTO OTA ON PHI.NM_TABELA =
                                                   OTA.NM_TABELA
                                               AND PHI.NM_ATRIBUTO =
                                                   OTA.NM_ATRIBUTO
       INNER JOIN ONTOLOGIA_TABELA OT ON OT.NM_TABELA = OTA.NM_TABELA
       INNER JOIN ONTOLOGIA_TAB_ATRIB_GRUPO OTAG ON OTAG.NR_SEQUENCIA =
                                                    OTA.NR_SEQ_ONTOLO_TAB_ATRIB_GRUPO
       WHERE OTAG.NR_SEQUENCIA = TO_NUMBER(CD_SEQ_RECORD_P)
         AND PHI.IE_ONTOLOGIA = 'SNO'
         AND PHI.NM_TABELA IS NOT NULL
         AND PHI.NM_ATRIBUTO = 'NULL'
         AND PHI.CD_VALOR_TASY IS NULL
         AND ROWNUM = 1;
    
    ELSE
      SELECT DISTINCT MAX(PHI.CD_VALOR_ONTOLOGIA),
                      MAX(PHI.CD_VALOR_ONTOLOGIA2),
                      MAX(PHI.CD_VALOR_ONTOLOGIA3),
                      MAX(PHI.CD_VALOR_ONTOLOGIA4),
                      MAX(PHI.CD_VALOR_ONTOLOGIA5),
                      MAX(PHI.CD_VALOR_ONTOLOGIA6),
                      MAX(PHI.CD_VALOR_ONTOLOGIA7)
        INTO CD_ONTOLOGY_P,
             CD_ONTOLOGY2_P,
             CD_ONTOLOGY3_P,
             CD_ONTOLOGY4_P,
             CD_ONTOLOGY5_P,
             CD_ONTOLOGY6_P,
             CD_ONTOLOGY7_P
        FROM RES_CADASTRO_ONTOLOGIA_PHI PHI
       INNER JOIN ONTOLOGIA_TABELA_ATRIBUTO OTA ON PHI.NM_TABELA =
                                                   OTA.NM_TABELA
                                               AND PHI.NM_ATRIBUTO =
                                                   OTA.NM_ATRIBUTO
       INNER JOIN ONTOLOGIA_TABELA OT ON OT.NM_TABELA = OTA.NM_TABELA
       WHERE OTA.NR_SEQUENCIA = TO_NUMBER(CD_SEQ_RECORD_P)
         AND PHI.IE_ONTOLOGIA = 'SNO'
         AND PHI.CD_VALOR_TASY IS NULL
         AND ROWNUM = 1;
    
    END IF;
  END GET_ONTOLOGY_CODE;

  PROCEDURE INSERT_ATTRIBUTE(NR_SEQ_PDE_MAIN_P        IN NUMBER,
                             NR_SEQ_PARENT_P          IN NUMBER,
                             VL_GROUP_DOMAIN_P        IN VARCHAR2,
                             NR_SEQ_ONTOL_TAB_P       IN NUMBER,
                             NR_SEQ_ONTOL_TAB_ATT_P   IN NUMBER,
                             NR_SEQ_ONTOL_ATT_GROUP_P IN NUMBER,
                             NM_TABLE_P               IN VARCHAR2,
                             NM_ATTRIBUTE_P           IN VARCHAR2,
                             CD_ONTOLOGY_P            IN VARCHAR2,
                             CD_ONTOLOGY2_P           IN VARCHAR2,
                             CD_ONTOLOGY3_P           IN VARCHAR2,
                             CD_ONTOLOGY4_P           IN VARCHAR2,
                             CD_ONTOLOGY5_P           IN VARCHAR2,
                             CD_ONTOLOGY6_P           IN VARCHAR2,
                             CD_ONTOLOGY7_P           IN VARCHAR2,
                             NM_USER_P                IN VARCHAR2,
                             NR_NEW_SEQUENCE_P        OUT NUMBER) IS
    NM_TABLE_W               PDE_ATTRIBUTE.NM_TABELA%TYPE;
    NM_ATTRIBUTE_W           PDE_ATTRIBUTE.NM_ATRIBUTO%TYPE;
    CD_EXP_TABLE_GROUP_W     PDE_ATTRIBUTE.CD_EXP_TABELA_GRUPO%TYPE;
    CD_EXP_TABLE_W           PDE_ATTRIBUTE.CD_EXP_TABELA%TYPE;
    CD_EXP_ATTRIBUTE_GROUP_W PDE_ATTRIBUTE.CD_EXP_ATRIBUTO_GRUPO%TYPE;
    CD_EXP_ATTRIBUTE_W       PDE_ATTRIBUTE.CD_EXP_ATRIBUTO%TYPE;
    IE_DOMAIN_W              PDE_ATTRIBUTE.IE_DOMINIO%TYPE;
    NM_TABLE_FK_W            PDE_ATTRIBUTE.NM_TABELA_FK%TYPE;
    VALID_ATTRIB_W           PDE_ATTRIBUTE.NR_SEQUENCIA%TYPE;
    NM_TABELA_SUP_W          ONTOLOGIA_TABELA.NM_TABELA_SUP%TYPE;
    NM_FK_TABELA_SUP_W       ONTOLOGIA_TABELA.NM_FK_TABELA_SUP%TYPE;
  BEGIN
    IF VL_GROUP_DOMAIN_P IS NOT NULL THEN
      SELECT CD_EXP_VALOR_DOMINIO
        INTO CD_EXP_TABLE_GROUP_W
        FROM VALOR_DOMINIO_V D
       WHERE D.CD_DOMINIO = 9781
         AND D.VL_DOMINIO = VL_GROUP_DOMAIN_P;
    END IF;
  
    IF NR_SEQ_ONTOL_TAB_P IS NOT NULL THEN
      SELECT NM_TABELA, CD_EXP_CADASTRO
        INTO NM_TABLE_W, CD_EXP_TABLE_W
        FROM ONTOLOGIA_TABELA
       WHERE NR_SEQUENCIA = NR_SEQ_ONTOL_TAB_P;
    END IF;
  
    IF NR_SEQ_ONTOL_TAB_ATT_P IS NOT NULL THEN
      SELECT NM_ATRIBUTO, CD_EXP_DESC
        INTO NM_ATTRIBUTE_W, CD_EXP_ATTRIBUTE_W
        FROM ONTOLOGIA_TABELA_ATRIBUTO
       WHERE NR_SEQUENCIA = NR_SEQ_ONTOL_TAB_ATT_P;
    
      GET_FOREIGN_TABLE(NM_TABLE_W,
                        NM_ATTRIBUTE_W,
                        IE_DOMAIN_W,
                        NM_TABLE_FK_W);
    END IF;
  
    IF NR_SEQ_ONTOL_ATT_GROUP_P IS NOT NULL THEN
      SELECT CD_EXP_ATRIBUTO_GRUPO
        INTO CD_EXP_ATTRIBUTE_GROUP_W
        FROM ONTOLOGIA_TAB_ATRIB_GRUPO
       WHERE NR_SEQUENCIA = NR_SEQ_ONTOL_ATT_GROUP_P;
    END IF;
  
    SELECT PDE_ATTRIBUTE_SEQ.NEXTVAL INTO NR_NEW_SEQUENCE_P FROM DUAL;
  
    IF NM_TABLE_P IS NOT NULL AND NM_ATTRIBUTE_P IS NOT NULL THEN
      NM_TABLE_W     := NM_TABLE_P;
      NM_ATTRIBUTE_W := NM_ATTRIBUTE_P;
      SELECT CD_EXP_DESC
        INTO CD_EXP_ATTRIBUTE_W
        FROM TABELA_ATRIBUTO
       WHERE NM_TABELA = NM_TABLE_P
         AND NM_ATRIBUTO = NM_ATTRIBUTE_P;
    END IF;
  
    --INSERT MANDATORY ATTRIBUTES
    IF NM_TABLE_W IS NOT NULL AND
       NM_ATTRIBUTE_W NOT IN ('NR_ATENDIMENTO', 'CD_PESSOA_FISICA',
        'NR_PRESCRICAO', 'NR_SEQ_PRESCR') THEN
      INSERT_ATRIB_OBRIG(NR_SEQ_PDE_MAIN_P,
                         VL_GROUP_DOMAIN_P,
                         CD_EXP_TABLE_GROUP_W,
                         CD_EXP_TABLE_W,
                         NM_TABLE_W,
                         NM_ATTRIBUTE_W,
                         NM_USER_P);
    END IF;
  
    VALID_ATTRIB_W := NULL;
  
    IF NM_TABLE_W IS NOT NULL AND
       NM_ATTRIBUTE_W IN ('NR_ATENDIMENTO', 'CD_PESSOA_FISICA',
        'NR_PRESCRICAO', 'NR_SEQ_PRESCR') THEN
    
      SELECT MAX(NR_SEQUENCIA)
        INTO VALID_ATTRIB_W
        FROM PDE_ATTRIBUTE
       WHERE NM_ATRIBUTO = NM_ATTRIBUTE_W
         AND NM_TABELA = NM_TABLE_W;
    END IF;
  
    IF NM_TABLE_W IS NOT NULL THEN
      SELECT NM_TABELA_SUP, NM_FK_TABELA_SUP
        INTO NM_TABELA_SUP_W, NM_FK_TABELA_SUP_W
        FROM ONTOLOGIA_TABELA
       WHERE NM_TABELA = NM_TABLE_W;
    END IF;
  
    IF VALID_ATTRIB_W IS NULL THEN
      INSERT INTO PDE_ATTRIBUTE
        (NR_SEQUENCIA,
         NR_SEQUENCIA_SUP,
         NR_SEQ_PDE_MAIN,
         IE_GRUPO,
         NR_SEQ_ATRIB_GRUPO,
         NM_TABELA,
         NM_ATRIBUTO,
         CD_ONTOLOGIA,
         CD_ONTOLOGIA2,
         CD_ONTOLOGIA3,
         CD_ONTOLOGIA4,
         CD_ONTOLOGIA5,
         CD_ONTOLOGIA6,
         CD_ONTOLOGIA7,
         CD_EXP_TABELA_GRUPO,
         CD_EXP_ATRIBUTO_GRUPO,
         CD_EXP_TABELA,
         CD_EXP_ATRIBUTO,
         IE_SITUACAO,
         IE_DOMINIO,
         IE_ENVIAR,
         NM_TABELA_FK,
         NM_FK_TABELA_FK,
         NM_USUARIO_NREC,
         DT_ATUALIZACAO_NREC,
         NM_USUARIO,
         DT_ATUALIZACAO)
      VALUES
        (NR_NEW_SEQUENCE_P,
         NR_SEQ_PARENT_P,
         NR_SEQ_PDE_MAIN_P,
         VL_GROUP_DOMAIN_P,
         NR_SEQ_ONTOL_ATT_GROUP_P,
         NM_TABLE_W,
         NM_ATTRIBUTE_W,
         CD_ONTOLOGY_P,
         CD_ONTOLOGY2_P,
         CD_ONTOLOGY3_P,
         CD_ONTOLOGY4_P,
         CD_ONTOLOGY5_P,
         CD_ONTOLOGY6_P,
         CD_ONTOLOGY7_P,
         CD_EXP_TABLE_GROUP_W,
         CD_EXP_ATTRIBUTE_GROUP_W,
         CD_EXP_TABLE_W,
         CD_EXP_ATTRIBUTE_W,
         'A',
         IE_DOMAIN_W,
         'S',
         NM_TABELA_SUP_W,
         NM_FK_TABELA_SUP_W,
         NM_USER_P,
         SYSDATE,
         NM_USER_P,
         SYSDATE);
    ELSE
      UPDATE PDE_ATTRIBUTE PA
         SET PA.IE_ENVIAR      = 'S',
             PA.DT_ATUALIZACAO = SYSDATE,
             PA.NM_USUARIO     = NM_USER_P
       WHERE PA.NR_SEQUENCIA = VALID_ATTRIB_W;
    END IF;
  
    FOR X IN (SELECT DISTINCT PA.NR_SEQ_PDE_MAIN
                FROM PDE_ATTRIBUTE PA
               WHERE PA.NM_TABELA = NM_TABLE_W) LOOP
      UPDATE PDE_MAIN PM
         SET PM.IE_STATUS = 'P'
       WHERE PM.NR_SEQUENCIA = X.NR_SEQ_PDE_MAIN;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        PDE.GRAVA_LOG_ERRO('INSERT.ATTRIBUTE.ERROR',
                           'NR_SEQ_PDE_MAIN_P: ' || NR_SEQ_PDE_MAIN_P ||
                           ', NR_SEQ_PARENT_P: ' || NR_SEQ_PARENT_P ||
                           ', VL_GROUP_DOMAIN_P: ' || VL_GROUP_DOMAIN_P ||
                           ', NR_SEQ_ONTOL_TAB_P: ' || NR_SEQ_ONTOL_TAB_P ||
                           ', NR_SEQ_ONTOL_TAB_ATT_P: ' ||
                           NR_SEQ_ONTOL_TAB_ATT_P ||
                           ', NR_SEQ_ONTOL_ATT_GROUP_P: ' ||
                           NR_SEQ_ONTOL_ATT_GROUP_P || ', NM_TABLE_P: ' ||
                           NM_TABLE_P || ', NM_ATTRIBUTE_P: ' ||
                           NM_ATTRIBUTE_P || ', CD_ONTOLOGY_P: ' ||
                           CD_ONTOLOGY_P || ', CD_ONTOLOGY2_P: ' ||
                           CD_ONTOLOGY2_P || ', CD_ONTOLOGY3_P: ' ||
                           CD_ONTOLOGY3_P || ', CD_ONTOLOGY4_P: ' ||
                           CD_ONTOLOGY4_P || ', CD_ONTOLOGY5_P: ' ||
                           CD_ONTOLOGY5_P || ', CD_ONTOLOGY6_P: ' ||
                           CD_ONTOLOGY6_P || ', CD_ONTOLOGY7_P: ' ||
                           CD_ONTOLOGY7_P || ', NM_USER_P: ' || NM_USER_P,
                           SQLCODE,
                           SQLERRM);
      
        UPDATE PDE_MAIN PM
           SET PM.IE_STATUS = 'E'
         WHERE PM.NR_SEQUENCIA = NR_SEQ_PDE_MAIN_P;
        COMMIT;
      END;
  END INSERT_ATTRIBUTE;

  PROCEDURE INSERT_ATRIB_OBRIG(NR_SEQ_PDE_MAIN_P    IN NUMBER,
                               VL_GROUP_DOMAIN_P    IN VARCHAR2,
                               CD_EXP_TABLE_GROUP_W IN NUMBER,
                               CD_EXP_TABLE_W       IN NUMBER,
                               NM_TABLE_P           IN VARCHAR2,
                               NM_ATRIBUTO_P        IN VARCHAR2,
                               NM_USER_P            IN VARCHAR2) IS
    NR_NEW_SEQUENCE_W  NUMBER;
    ATRIBUTO_W         VARCHAR2(255);
    CD_EXP_ATTRIBUTE_W NUMBER;
    ONTOLOGIA_COD_W    NUMBER;
    NM_FK_TABELA_SUP_W ONTOLOGIA_TABELA.NM_FK_TABELA_SUP%TYPE;
    NM_TABELA_SUP_W    ONTOLOGIA_TABELA.NM_TABELA_SUP%TYPE;
  
    CURSOR ATRIB IS
      SELECT DISTINCT NM_ATRIBUTO
        FROM TABELA_ATRIBUTO
       WHERE NM_TABELA = NM_TABLE_P
         AND NM_ATRIBUTO IN ('NR_ATENDIMENTO', 'CD_PESSOA_FISICA',
              'NR_PRESCRICAO', 'NR_SEQ_PRESCR')
       ORDER BY NM_ATRIBUTO;
  
    --VERIFICAR QUAIS ATRIBUTOS EXISTEM NA TABELA REF AO PDE_MAIN
  BEGIN
    FOR A IN ATRIB LOOP
      IF A.NM_ATRIBUTO IS NOT NULL AND NM_ATRIBUTO_P <> A.NM_ATRIBUTO THEN
        BEGIN
          SELECT DISTINCT NM_ATRIBUTO
            INTO ATRIBUTO_W
            FROM PDE_ATTRIBUTE
           WHERE NM_TABELA = NM_TABLE_P
             AND NM_ATRIBUTO = A.NM_ATRIBUTO
             AND NR_SEQ_PDE_MAIN = NR_SEQ_PDE_MAIN_P;
        
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ATRIBUTO_W := NULL;
        END;
      END IF;
    
      --RESGATAR CODIGO ONTOLOGIA DO ATRIBUTO
      IF ATRIBUTO_W IS NULL AND
         NM_ATRIBUTO_P NOT IN ('NR_ATENDIMENTO', 'CD_PESSOA_FISICA',
          'NR_PRESCRICAO', 'NR_SEQ_PRESCR') THEN
        SELECT MAX(CD_VALOR_ONTOLOGIA)
          INTO ONTOLOGIA_COD_W
          FROM RES_CADASTRO_ONTOLOGIA_PHI
         WHERE NM_TABELA = NM_TABLE_P
           AND NM_ATRIBUTO = A.NM_ATRIBUTO;
      
        --PEGAR CODIGO DE EXPRESSAO DO ATRIBUTO
        SELECT MAX(CD_EXP_DESC)
          INTO CD_EXP_ATTRIBUTE_W
          FROM ONTOLOGIA_TABELA_ATRIBUTO
         WHERE NM_ATRIBUTO = A.NM_ATRIBUTO
           AND NM_TABELA = NM_TABLE_P;
      
        --PEGAR PROXIMA SEQUENCIA DE PDE_ATTRIBUTE
        SELECT PDE_ATTRIBUTE_SEQ.NEXTVAL INTO NR_NEW_SEQUENCE_W FROM DUAL;
      
        IF ATRIBUTO_W IS NULL AND ONTOLOGIA_COD_W IS NOT NULL AND
           CD_EXP_ATTRIBUTE_W IS NOT NULL AND
           NM_ATRIBUTO_P NOT IN ('NR_ATENDIMENTO', 'CD_PESSOA_FISICA',
            'NR_PRESCRICAO', 'NR_SEQ_PRESCR') THEN
        
          IF NM_TABLE_P IS NOT NULL THEN
            SELECT MAX(NM_TABELA_SUP), MAX(NM_FK_TABELA_SUP)
              INTO NM_TABELA_SUP_W, NM_FK_TABELA_SUP_W
              FROM ONTOLOGIA_TABELA
             WHERE NM_TABELA = NM_TABLE_P;
          END IF;
        
          INSERT INTO PDE_ATTRIBUTE
            (NR_SEQUENCIA,
             NR_SEQ_PDE_MAIN,
             IE_GRUPO,
             NR_SEQ_ATRIB_GRUPO,
             NM_TABELA,
             NM_ATRIBUTO,
             CD_ONTOLOGIA,
             CD_EXP_TABELA_GRUPO,
             CD_EXP_ATRIBUTO_GRUPO,
             CD_EXP_TABELA,
             CD_EXP_ATRIBUTO,
             IE_SITUACAO,
             IE_DOMINIO,
             IE_ENVIAR,
             NM_TABELA_FK,
             NM_FK_TABELA_FK,
             NM_USUARIO_NREC,
             DT_ATUALIZACAO_NREC,
             NM_USUARIO,
             DT_ATUALIZACAO)
          VALUES
            (NR_NEW_SEQUENCE_W,
             NR_SEQ_PDE_MAIN_P,
             VL_GROUP_DOMAIN_P,
             NULL,
             NM_TABLE_P,
             A.NM_ATRIBUTO,
             ONTOLOGIA_COD_W,
             CD_EXP_TABLE_GROUP_W,
             NULL,
             CD_EXP_TABLE_W,
             CD_EXP_ATTRIBUTE_W,
             'A',
             NULL,
             'N',
             NM_TABELA_SUP_W,
             NM_FK_TABELA_SUP_W,
             NM_USER_P,
             SYSDATE,
             NM_USER_P,
             SYSDATE);
        END IF;
      END IF;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      PDE.GRAVA_LOG_ERRO('INSERT.ATRIB.OBRIG.ERROR',
                         'NR_SEQ_PDE_MAIN_P: ' || NR_SEQ_PDE_MAIN_P ||
                         ', VL_GROUP_DOMAIN_P: ' || VL_GROUP_DOMAIN_P ||
                         ', CD_EXP_TABLE_GROUP_W: ' || CD_EXP_TABLE_GROUP_W ||
                         ', CD_EXP_TABLE_W: ' || CD_EXP_TABLE_W ||
                         ', NM_TABLE_P: ' || NM_TABLE_P ||
                         ', NM_ATRIBUTO_P: ' || NM_ATRIBUTO_P ||
                         ', NM_USER_P: ' || NM_USER_P,
                         SQLCODE,
                         SQLERRM);
  END INSERT_ATRIB_OBRIG;

  PROCEDURE GET_FOREIGN_TABLE(NM_TABLE_P         IN VARCHAR2,
                              NM_ATTRIBUTE_P     IN VARCHAR2,
                              IE_DOMAIN_P        OUT NUMBER,
                              NM_FOREIGN_TABLE_P OUT VARCHAR2) IS
    CD_DOMAIN_W    TABELA_ATRIBUTO.CD_DOMINIO%TYPE;
    DS_VALORES_W   TABELA_ATRIBUTO.DS_VALORES%TYPE;
    NM_TABLE_W     TABELA_SISTEMA.NM_TABELA%TYPE;
    NM_ATTRIBUTE_W TABELA_ATRIBUTO.NM_ATRIBUTO%TYPE;
  BEGIN
    SELECT DISTINCT TA.CD_DOMINIO, TA.DS_VALORES
      INTO CD_DOMAIN_W, DS_VALORES_W
      FROM OBJETO_SCHEMATIC OS
     INNER JOIN TABELA_SISTEMA TS ON OS.NM_TABELA = TS.NM_TABELA
     INNER JOIN TABELA_ATRIBUTO TA ON TS.NM_TABELA = TA.NM_TABELA
     INNER JOIN ONTOLOGIA_TABELA_ATRIBUTO OA ON OA.NM_TABELA = TA.NM_TABELA
     WHERE TA.NM_TABELA = NM_TABLE_P
       AND TA.NM_ATRIBUTO = NM_ATTRIBUTE_P;
  
    IE_DOMAIN_P := 0;
    IF CD_DOMAIN_W IS NOT NULL THEN
      IE_DOMAIN_P := 1;
    ELSIF DS_VALORES_W IS NOT NULL THEN
      IE_DOMAIN_P := 2;
    ELSE
      BEGIN
        SELECT DISTINCT CLI.NM_TABELA, CLI.NM_ATRIBUTO
          INTO NM_TABLE_W, NM_ATTRIBUTE_W
          FROM INTEGRIDADE_ATRIBUTO A
          JOIN INTEGRIDADE_REFERENCIAL R ON R.NM_INTEGRIDADE_REFERENCIAL =
                                            A.NM_INTEGRIDADE_REFERENCIAL
          JOIN RES_CADASTRO_ONTOLOGIA_CLI CLI ON CLI.NM_TABELA =
                                                 R.NM_TABELA_REFERENCIA
         WHERE A.NM_TABELA = NM_TABLE_P
           AND A.NM_ATRIBUTO = NM_ATTRIBUTE_P;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    END IF;
  
    NM_FOREIGN_TABLE_P := NM_TABLE_W;
  EXCEPTION
    WHEN OTHERS THEN
      PDE.GRAVA_LOG_ERRO('GET.FOREIGN.TABLE.ERROR',
                         'NM_TABLE_P: ' || NM_TABLE_P ||
                         ', NM_ATTRIBUTE_P: ' || NM_ATTRIBUTE_P ||
                         ', IE_DOMAIN_P: ' || IE_DOMAIN_P ||
                         ', NM_FOREIGN_TABLE_P: ' || NM_FOREIGN_TABLE_P,
                         SQLCODE,
                         SQLERRM);
  END GET_FOREIGN_TABLE;

  PROCEDURE CLEAN_STAGE IS
    LIST_ID_W VARCHAR2(32767) := '';
  BEGIN
    FOR X IN (SELECT PL.NR_SEQUENCIA
                FROM PDE_LOG PL
               WHERE PL.DT_ENVIO IS NOT NULL
                 AND PL.DT_JSON IS NOT NULL
                 AND TRUNC(PL.DT_ENVIO) <= TRUNC(SYSDATE - 7)) LOOP
      IF (TRIM(LIST_ID_W) IS NOT NULL) THEN
        LIST_ID_W := LIST_ID_W || ',';
      END IF;
    
      LIST_ID_W := LIST_ID_W || X.NR_SEQUENCIA;
    END LOOP;
  
    IF (TRIM(LIST_ID_W) IS NOT NULL) THEN
      DELETE FROM PDE_STAGE_LOG PSL
       WHERE PSL.NR_SEQ_LOG IN
             (SELECT V.NR_REGISTRO
                FROM TABLE(LISTA_PCK.OBTER_LISTA(LIST_ID_W)) V);
    
      DELETE FROM PDE_LOG PL
       WHERE PL.NR_SEQUENCIA IN
             (SELECT V.NR_REGISTRO
                FROM TABLE(LISTA_PCK.OBTER_LISTA(LIST_ID_W)) V);
    
      DELETE FROM PDE_STAGE PS
       WHERE PS.NR_SEQUENCIA NOT IN
             (SELECT DISTINCT PSL.NR_SEQ_STAGE FROM PDE_STAGE_LOG PSL);
    
      COMMIT;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      PDE.GRAVA_LOG_ERRO('CLEAN.STAGE.ERROR',
                         'LIST_ID_W: ' || LIST_ID_W,
                         SQLCODE,
                         SQLERRM);
  END CLEAN_STAGE;

  PROCEDURE GRAVA_LOG_ERRO(DS_REF_P  IN VARCHAR2,
                           DS_ERRO_P IN CLOB,
                           SQLCODE_P IN NUMBER DEFAULT NULL,
                           SQLERRM_P IN VARCHAR2 DEFAULT NULL) IS
    DS_ORA_W VARCHAR2(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    IF (SQLCODE_P IS NOT NULL AND SQLERRM_P IS NOT NULL) THEN
      DS_ORA_W := '(' || SQLCODE_P || ') ' || SQLERRM_P || '#@#@BR#@#@';
    END IF;
  
    INSERT INTO PDE_ERRO_LOG
      (NR_SEQUENCIA,
       DT_ATUALIZACAO,
       NM_USUARIO,
       DT_ATUALIZACAO_NREC,
       NM_USUARIO_NREC,
       NM_REFERENCIA,
       DS_ERRO,
       DS_ERRO_FULL)
    VALUES
      (PDE_ERRO_LOG_SEQ.NEXTVAL,
       SYSDATE,
       NVL(WHEB_USUARIO_PCK.GET_NM_USUARIO, 'integration'),
       SYSDATE,
       NVL(WHEB_USUARIO_PCK.GET_NM_USUARIO, 'integration'),
       DS_REF_P,
       SUBSTR(REPLACE(DS_ORA_W, '#@#@BR#@#@', ' - ') || DS_ERRO_P, 1, 4000),
       REPLACE(DS_ORA_W, '#@#@BR#@#@', CHR(10) || CHR(10)) || DS_ERRO_P ||
       CHR(10) || CHR(10) || '***** CALL STACK: ' || CHR(10) ||
       DBMS_UTILITY.FORMAT_CALL_STACK);
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END GRAVA_LOG_ERRO;

END PDE;
/
