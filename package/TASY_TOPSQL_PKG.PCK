CREATE OR REPLACE PACKAGE tasy_topsql_pkg AS
PROCEDURE create_snapshot;
END tasy_topsql_pkg;
/

CREATE OR REPLACE PACKAGE BODY tasy_topsql_pkg AS

PROCEDURE transfer_to_remote_db_proc IS
    l_remote_db_username        VARCHAR2(128);
    l_remote_db_password        VARCHAR2(128);
    l_remote_db_sqlnet_config   VARCHAR2(256);
    l_create_db_link            VARCHAR2(1000);
    l_db_link_exists            NUMBER;
    l_dest_table_name           VARCHAR2(128);
    l_db_name                   VARCHAR2(8);
    l_con_name                  VARCHAR2(30);
    l_tasy_version              VARCHAR2(10);
    l_query                     VARCHAR2(1000);
    l_oracle_version            PLS_INTEGER := DBMS_DB_VERSION.VERSION;
    l_oracle_release            PLS_INTEGER := DBMS_DB_VERSION.RELEASE;
    l_oracle_full_version       VARCHAR2(10) := TO_CHAR(l_oracle_version) || '.' || TO_CHAR(l_oracle_release);
    l_remove_insert_sql         VARCHAR(2000) :=
            'INSERT INTO tasy_topsql_sqlarea_rep_cust@tasy_topsql_copy('
            || ' customer_name'
            || ',database_name'
            || ',tasy_version'
            || ',oracle_version'
            || ',creation_date'
            || ',sql_id'
            || ',occurences_top_n'
            || ',object_name'
            || ',module'
            || ',action'
            || ',first_sample'
            || ',last_sample'
            || ',last_executions'
            || ',last_elapsed_time'
            || ',last_cpu_time'
            || ',last_disk_reads'
            || ',last_buffer_gets'
            || ',last_user_io_wait_time'
            || ',last_rows_processed'
            || ',total_executions'
            || ',total_elapsed_time'
            || ',total_cpu_time'
            || ',total_disk_reads'
            || ',total_buffer_gets'
            || ',total_user_io_wait_time'
            || ',total_rows_processed'
            || ',sql_text'
            || ',sql_fulltext'
            || ',last_sql_plans'
            || ') SELECT '
            || '''Philips'''
            || ',nvl2(:l_con_name,:l_db_name || ' || ''':''' || ' || :l_con_name,:l_db_name),'
            || ':l_tasy_version,'
            || ':l_oracle_full_version,'
            || 'sysdate'
            || ',sql_id'
            || ',occurences_top_n'
            || ',object_name'
            || ',module'
            || ',action'
            || ',first_sample'
            || ',last_sample'
            || ',last_executions'
            || ',last_elapsed_time'
            || ',last_cpu_time'
            || ',last_disk_reads'
            || ',last_buffer_gets'
            || ',last_user_io_wait_time'
            || ',last_rows_processed'
            || ',total_executions'
            || ',total_elapsed_time'
            || ',total_cpu_time'
            || ',total_disk_reads'
            || ',total_buffer_gets'
            || ',total_user_io_wait_time'
            || ',total_rows_processed'
            || ',sql_text'
            || ',sql_fulltext'
            || ',last_sql_plans'
            || ' FROM tasy_topsql_sqlarea_report';
            
    missing_sys_priv EXCEPTION;
    PRAGMA EXCEPTION_INIT(missing_sys_priv, -1031);
BEGIN
    -- Get database link connection attributes
    SELECT
        remote_db_username,
        remote_db_password,
        remote_db_sqlnet_config
    INTO
        l_remote_db_username,
        l_remote_db_password,
        l_remote_db_sqlnet_config
    FROM
        tasy_topsql_control_tab;
        
    -- Get database name
    SELECT sys_context('USERENV','DB_NAME') 
    INTO l_db_name
    FROM dual;
    
    -- Get container name
    IF l_oracle_version >= 12 THEN
        SELECT sys_context('USERENV','CON_NAME') 
        INTO l_con_name
        FROM dual;
    END IF;
    
    -- Get tasy version
    SELECT MAX(SUBSTR(at.cd_versao_atual, -4, 4) || '_' || TO_CHAR(nvl(avc.nr_service_pack, 0), 'FM00')) cd_versao
    INTO l_tasy_version
    FROM ajuste_versao_cliente avc, aplicacao_tasy at 
    WHERE avc.cd_versao(+) = at.cd_versao_atual 
    AND at.cd_aplicacao_tasy = 'Tasy';
    
    IF(l_remote_db_username IS NOT NULL AND l_remote_db_password IS NOT NULL AND l_remote_db_sqlnet_config IS NOT NULL) THEN
        -- Check if database link exists, otherwise creates it
        SELECT COUNT(*)
        INTO l_db_link_exists
        FROM user_db_links
        WHERE db_link = 'TASY_TOPSQL_COPY';
        
        IF l_db_link_exists = 0 THEN
            l_create_db_link := 'create database link tasy_topsql_copy connect to ' || 
                l_remote_db_username || ' identified by "' || l_remote_db_password || 
                '" using ''' || l_remote_db_sqlnet_config || '''';
            BEGIN
                EXECUTE IMMEDIATE l_create_db_link;
            EXCEPTION
                WHEN missing_sys_priv THEN
                    raise_application_error(-20001,'Unable to create the database link: ' || sqlerrm);
            END;
        END IF;
        
        BEGIN
            -- Check if the database link is working, otherwise, raise exception
            l_query := 'SELECT table_name '
            || 'FROM user_tables@tasy_topsql_copy '
            || 'WHERE table_name = :b1';
            
            EXECUTE IMMEDIATE l_query INTO l_dest_table_name USING 'TASY_TOPSQL_SQLAREA_REP_CUST';
        EXCEPTION
            WHEN no_data_found THEN
                raise_application_error(-20003,'Destination table "TASY_TOPSQL_SQLAREA_REP_CUST" was not craeted: ' || sqlerrm);        
        END;
        
        -- If all went well, insert data in remote database
        EXECUTE IMMEDIATE l_remove_insert_sql 
        USING l_con_name, l_db_name, l_con_name, l_db_name, l_tasy_version, l_oracle_full_version;
    END IF;
END transfer_to_remote_db_proc;


/* 
    Gathers data about SQLs that are not using binds (that are using literal values in the SQL text - repeated SQLs)
*/
PROCEDURE gather_nobind_sql (p_snap_nobind_sql_maxcount NUMBER) IS

    missing_privilege EXCEPTION;
    PRAGMA EXCEPTION_INIT(missing_privilege, -942);
BEGIN
    -- Sanity check
    IF p_snap_nobind_sql_maxcount < 1 THEN
        raise_application_error(-20005,'You must try to collect at least 1 SQL! Check the config table and try again.');
    END IF;
    
    -- If the force_matching signature exists, just update the number of occurrences and counts
    -- Limit the amount of data to be returned to the parameter
    MERGE INTO tasy_topsql_nobind_sql B
    USING (
        SELECT * FROM (    
            WITH c AS (
                SELECT
                    force_matching_signature,
                    COUNT(*) cnt
                FROM
                    sys.gv_$sqlarea
                WHERE
                    force_matching_signature != 0
                AND 
                    parsing_user_id IN(
                        SELECT user_id
                        FROM user_users
                    )
                GROUP BY
                    force_matching_signature
                HAVING
                    COUNT(*) > 10
            ), sq AS (
                SELECT
                    sql_text,
                    force_matching_signature,
                    program_id,
                    program_line# program_line,
                    module,
                    action,
                    last_load_time,
                    ROW_NUMBER() OVER(
                        PARTITION BY force_matching_signature
                        ORDER BY
                            last_load_time DESC
                    ) p
                FROM
                    sys.gv_$sqlarea s
                WHERE
                    force_matching_signature IN (
                        SELECT
                            force_matching_signature
                        FROM
                            c
                    )
                AND 
                    parsing_user_id IN(
                        SELECT user_id
                        FROM user_users
                    )
            )
            SELECT
                sq.force_matching_signature,
                o.object_name   AS program_name,
                sq.program_line,
                sq.module,
                sq.action,
                sq.last_load_time,
                c.cnt           AS unshared_count,
                sq.sql_text
            FROM
                c,
                sq,
                user_objects o
            WHERE
                sq.force_matching_signature = c.force_matching_signature
                AND o.object_id (+) = sq.program_id
                AND sq.p = 1
            ORDER BY
                c.cnt DESC
        )
        WHERE ROWNUM <= p_snap_nobind_sql_maxcount) Q
    ON (B.force_matching_signature = Q.force_matching_signature)
    WHEN MATCHED THEN UPDATE SET
        B.number_of_occurences = B.number_of_occurences + 1,
        B.last_load_time = Q.last_load_time,
        B.program_name = Q.program_name,
        B.program_line = Q.program_line,
        B.module = Q.module,
        B.action = Q.action,  
        B.last_unshared_count = Q.unshared_count,
        B.total_unshared_count = B.total_unshared_count + Q.unshared_count,
        B.sql_text = Q.sql_text
    WHEN NOT MATCHED THEN INSERT (
        B.force_matching_signature,
        B.number_of_occurences,
        B.last_load_time,
        B.program_name,
        B.program_line,
        B.module,
        B.action,  
        B.last_unshared_count,
        B.total_unshared_count,
        B.sql_text
    )
    VALUES
    (
        Q.force_matching_signature,
        1,
        Q.last_load_time,
        Q.program_name,
        Q.program_line,
        Q.module,
        Q.action,  
        Q.unshared_count,
        Q.unshared_count,
        Q.sql_text    
    );
    
EXCEPTION
    WHEN missing_privilege THEN
        raise_application_error(-20006,'Missing privilege on view "sys.gv_$sqlarea".'
            || ' Please grant SELECT on sys.gv_$sqlarea to TASY: ' || sqlerrm);
END gather_nobind_sql;


/*  
    Cleanup routine to remove old data from TASY_TOPSQL_SQLAREA
    This procedure uses the column NUMBER_OF_SNAPSHOTS_TO_RETAIN from table TASY_TOPSQL_CONTROL_TAB
*/
PROCEDURE remove_snap_data IS
    l_number_of_snaps_to_retain NUMBER;
    l_max_nobind_rows_top_keep NUMBER;
    l_snap_tab_size NUMBER;
    
BEGIN
    SELECT
        number_of_snapshots_to_retain,
        max_nobind_rows_top_keep
    INTO
        l_number_of_snaps_to_retain,
        l_max_nobind_rows_top_keep
    FROM
        tasy_topsql_control_tab
    -- Sanity check: if more than one configuration is present, do not abort
    -- In case of no data found, abort
    WHERE rownum = 1;
                
    BEGIN
        -- Check the size of table
        SELECT bytes/1024/1024
        INTO l_snap_tab_size
        FROM user_segments 
        WHERE segment_name = 'TASY_TOPSQL_SQLAREA';
    EXCEPTION
        WHEN no_data_found THEN
            raise_application_error(-20013,'Table "TASY_TOPSQL_SQLAREA" does not exists: ' || sqlerrm);
    END;    
    
    -- If table is bigger than 1GB, truncate all data, skip DELETE command (it would be inneficient)
    IF l_snap_tab_size > 1024 THEN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE tasy_topsql_sqlarea';
    ELSE
        -- Delete retain N snapshots accordingly to the configuration table attribute
        DELETE FROM tasy_topsql_sqlarea
        WHERE snap_id IN(
            SELECT snap_id 
            FROM(
                SELECT snap_id, row_number() OVER (ORDER BY snap_id DESC) AS row_number 
                FROM (
                    SELECT DISTINCT snap_id AS snap_id 
                    FROM tasy_topsql_sqlarea 
                )
            )
            WHERE row_number > l_number_of_snaps_to_retain
        );
    END IF;
    
    BEGIN
        -- Check the size of the table
        SELECT bytes/1024/1024
        INTO l_snap_tab_size
        FROM user_segments 
        WHERE segment_name = 'TASY_TOPSQL_NOBIND_SQL';
    EXCEPTION
        WHEN no_data_found THEN
            raise_application_error(-20014,'Table "TASY_TOPSQL_NOBIND_SQL" does not exists: ' || sqlerrm);
    END;  
    
    -- If table is bigger than 512MB, truncate all data, skip DELETE command (it would be inneficient)
    IF l_snap_tab_size > 512 THEN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE tasy_topsql_nobind_sql';
    ELSE
        -- Delete keeps N rows accordingly to the configuration; Older data is removed first (LAST_LOAD_TIME column)
        DELETE FROM tasy_topsql_nobind_sql
        WHERE force_matching_signature IN(
            SELECT force_matching_signature 
            FROM (
                SELECT force_matching_signature, row_number() OVER (ORDER BY last_load_time DESC) AS row_number
                FROM(
                    SELECT force_matching_signature, last_load_time
                    FROM tasy_topsql_nobind_sql
                )
            )
            WHERE row_number >= l_max_nobind_rows_top_keep
        );
    END IF;
END remove_snap_data;


/*  
    Remove data from TASY_TOPSQL_SQLAREA_REPORT
    Keeps only the top-n lines for SQLs report accordingly to a certain number of days (parameters)
*/
PROCEDURE remove_report_data(sample_date DATE DEFAULT TRUNC(SYSDATE)-15) IS
BEGIN
    -- Remove report data that was not found in the last X days 
    DELETE FROM tasy_topsql_sqlarea_report
    WHERE sql_id in(
        SELECT sql_id
        FROM(
            SELECT sql_id, row_number() over ( order by total_elapsed_time DESC) as row_number
            FROM (
                SELECT sql_id, total_elapsed_time
                FROM tasy_topsql_sqlarea_report
                WHERE last_sample < sample_date
                )
            )
    );
END remove_report_data;


-- Get SQL full text and SQL plans
PROCEDURE populate_fulltext_and_plans IS
    l_sql_plan CLOB;
    l_full_sqltext CLOB;
    l_clob_length INTEGER;
    
    missing_privilege EXCEPTION;
    PRAGMA EXCEPTION_INIT(missing_privilege, -942);    
BEGIN
    DBMS_LOB.CREATETEMPORARY (l_sql_plan,false);
    
    FOR sqls IN (
        -- For each sql that was reported (insert or update into tasy_topsql_sqlarea_report) today
        SELECT sql_id 
        FROM tasy_topsql_sqlarea_report
        WHERE TRUNC(last_sample) = TRUNC(sysdate)
        ) LOOP
            l_clob_length := DBMS_LOB.GETLENGTH(l_sql_plan);
            IF l_clob_length > 0 THEN
                DBMS_LOB.ERASE (l_sql_plan,l_clob_length);
            END IF;
        
            -- ### TODO: analyze the sql statement plan
            FOR sql_childs IN(
                -- Get all its active child numbers
                SELECT DISTINCT sql_id, plan_hash_value, MAX(child_number) AS child_number
                FROM sys.gv_$sql
                WHERE sql_id = sqls.sql_id
                AND command_type IN (3,6,2,7) -- In order: SELECT, UPDATE, INSERT, DELETE
                GROUP BY sql_id, plan_hash_value
                ) LOOP
                    FOR sql_plan_line IN(
                        SELECT plan_table_output
                        FROM TABLE(dbms_xplan.display_cursor(sql_childs.sql_id, sql_childs.child_number, 'ALL -PROJECTION -ALIAS +NOTE'))
                        ) LOOP
                            -- For each child number, get the sql plan
                            DBMS_LOB.APPEND(l_sql_plan,sql_plan_line.plan_table_output || chr(10));
                    END LOOP;
            END LOOP;
        
            BEGIN
                -- Get the SQL full text
                SELECT sql_fulltext
                INTO l_full_sqltext
                FROM sys.gv_$sqlarea
                WHERE sql_id = sqls.sql_id
                AND ROWNUM = 1;
            EXCEPTION
                WHEN no_data_found THEN
                    l_full_sqltext := NULL;
            END;
            
            IF l_full_sqltext IS NOT NULL THEN
                UPDATE tasy_topsql_sqlarea_report
                SET sql_fulltext = l_full_sqltext
                WHERE sql_id = sqls.sql_id;
            END IF;
            
            -- If the sql plan was found, update the respective column
            IF DBMS_LOB.GETLENGTH(l_sql_plan) > 0 AND l_sql_plan NOT LIKE '%cannot be found%' THEN
                UPDATE tasy_topsql_sqlarea_report
                SET last_sql_plans = trim(l_sql_plan)
                WHERE sql_id = sqls.sql_id;
            END IF;
            
    END LOOP;
EXCEPTION
    WHEN missing_privilege THEN
        raise_application_error(-20016,'Missing privilege on view "sys.gv_$sqlarea".'
        || ' Please grant SELECT on sys.gv_$sqlarea to TASY: ' || sqlerrm);
END populate_fulltext_and_plans;


/*  
    Computes the TOP SQLs between the last two snapshots 
    Reads from table TASY_TOPSQL_SQLAREA and writes to table TASY_TOPSQL_SQLAREA_REPORT
    Uses ELAPSED TIME as the column to order the results and classify the TOP-N queries
*/
PROCEDURE report_data IS
    l_report_sql_minimal_exec       NUMBER;
    l_report_sql_minimal_elapsed    NUMBER;
    l_number_of_sqls_per_report     NUMBER;
    l_last_snap_id_reported         NUMBER;
    l_end_snap_id                   NUMBER;
    l_begin_snap_id                 NUMBER;
    l_transfer_to_remote_db         NUMBER;
    l_end_date                      DATE;
    l_begin_date                    DATE;
    l_number_rows_merged            NUMBER;

    value_too_big EXCEPTION;
    PRAGMA EXCEPTION_INIT(value_too_big , -1438);    
    
BEGIN
    -- Get configuration values
    SELECT
        number_of_sqls_per_report,
        report_sql_minimal_executions,
        report_sql_minimal_elapsed,
        last_snap_id_reported,
        transfer_to_remote_db
    INTO
        l_number_of_sqls_per_report,
        l_report_sql_minimal_exec,
        l_report_sql_minimal_elapsed,
        l_last_snap_id_reported,
        l_transfer_to_remote_db
    FROM
        tasy_topsql_control_tab
    -- avoids failures if the customer inserted more than 1 line
    WHERE rownum = 1;
    
    -- Select the last snapshot id available
    SELECT NVL(MAX(snap_id),0)
    INTO l_end_snap_id
    FROM tasy_topsql_sqlarea;
    
    -- If the last snapshot were already analyzed, stop processing
    IF l_end_snap_id <= l_last_snap_id_reported THEN
        raise_application_error(-20018,'Last snapshot id have already being reported. Last snapshot id reported: ' 
            || l_last_snap_id_reported || '. Last snapshot available: ' || l_end_snap_id);
    ELSE
        -- Select the penultimate snapshot id (to compose the window to report)
        SELECT MAX(snap_id) AS snap_id
        INTO l_begin_snap_id
        FROM tasy_topsql_sqlarea
        WHERE snap_id != l_end_snap_id;
    END IF;
    
    -- Select the snapshot date from both snapshot ids
    SELECT MAX(sample_date)
    INTO l_begin_date
    FROM tasy_topsql_sqlarea
    WHERE snap_id = l_begin_snap_id;
    
    SELECT MAX(sample_date)
    INTO l_end_date
    FROM tasy_topsql_sqlarea
    WHERE snap_id = l_end_snap_id;
    
    /* 
        Only compute snapshots within the same day
        The objective is to exclude overnight processing jobs that run on less critical hours
    */
    IF (trunc(l_end_date) = trunc(l_begin_date) AND l_end_date IS NOT NULL AND l_begin_date IS NOT NULL) THEN
        /* 
            Merge query computes the top SQLs comparing the last two snapshots from table TASY_TOPSQL_SQLAREA
            SQL metrics should exists in both snapshots or at the final snapshot (table B)
        */
        BEGIN
            MERGE INTO tasy_topsql_sqlarea_report S
            USING (
                SELECT
                    sql_id,
                    command_type,
                    object_name,
                    module,
                    action,
                    begin_sample_date,
                    end_sample_date,
                    executions,
                    elapsed_time,
                    cpu_time,
                    disk_reads,
                    buffer_gets,
                    user_io_wait_time,
                    rows_processed,
                    sql_text
                FROM(
                    SELECT
                        b.sql_id AS sql_id,
                        b.command_type AS command_type,
                        b.object_name AS object_name,
                        b.module AS module,
                        b.action AS action,
                        nvl(a.sample_date,b.sample_date) AS begin_sample_date,
                        b.sample_date AS end_sample_date,
                        b.executions - nvl(a.executions,0) AS executions,
                        b.elapsed_time - nvl(a.elapsed_time,0) AS elapsed_time, 
                        b.cpu_time - nvl(a.cpu_time,0) AS cpu_time,
                        b.disk_reads - nvl(a.disk_reads,0) AS disk_reads,
                        b.buffer_gets - nvl(a.buffer_gets,0) AS buffer_gets,
                        b.user_io_wait_time - nvl(a.user_io_wait_time,0) AS user_io_wait_time,
                        b.rows_processed - nvl(a.rows_processed,0) AS rows_processed,
                        b.sql_text AS sql_text
                    FROM tasy_topsql_sqlarea a
                    RIGHT OUTER JOIN tasy_topsql_sqlarea b ON (a.sql_id = b.sql_id)
                    WHERE a.snap_id = l_begin_snap_id
                    AND b.snap_id = l_end_snap_id
                    -- Config checks
                    AND b.executions - nvl(a.executions,0) >= l_report_sql_minimal_exec
                    AND b.elapsed_time - nvl(a.elapsed_time,0) >= l_report_sql_minimal_elapsed
                    -- Sanity checks
                    AND b.cpu_time - nvl(a.cpu_time,0) >= 0
                    AND b.disk_reads - nvl(a.disk_reads,0) >= 0
                    AND b.buffer_gets - nvl(a.buffer_gets,0) >= 0
                    AND b.user_io_wait_time - nvl(a.user_io_wait_time,0) >= 0
                    AND b.rows_processed - nvl(a.rows_processed,0) >= 0
                    -- TOP SQLs classified by Elapsed Time
                    ORDER BY b.elapsed_time DESC, b.sql_id ASC
                ) 
                -- Limit on the number SQLs involved
                WHERE ROWNUM <= l_number_of_sqls_per_report) Q
            ON (S.sql_id = Q.sql_id)
            -- When sql id exists, update the last% columns and add the value in total% columns
            WHEN MATCHED THEN UPDATE SET S.last_sample = Q.end_sample_date, 
                S.last_executions = Q.executions,
                S.last_elapsed_time = Q.elapsed_time,
                S.last_cpu_time = Q.cpu_time,
                S.last_disk_reads = Q.disk_reads,
                S.last_buffer_gets = Q.buffer_gets,
                S.last_user_io_wait_time = Q.user_io_wait_time,
                S.last_rows_processed = Q.rows_processed,
                S.total_executions = S.total_executions + Q.executions,
                S.total_elapsed_time = S.total_elapsed_time + Q.elapsed_time,
                S.total_cpu_time = S.total_cpu_time + Q.cpu_time,
                S.total_disk_reads = S.total_disk_reads + Q.disk_reads,
                S.total_buffer_gets = S.total_buffer_gets + Q.buffer_gets,
                S.total_user_io_wait_time = S.total_user_io_wait_time + Q.user_io_wait_time,
                S.total_rows_processed = S.total_rows_processed + Q.rows_processed,
                S.occurences_top_n = S.occurences_top_n + 1
            WHERE (Q.end_sample_date > S.last_sample)
            WHEN NOT MATCHED THEN INSERT (
                S.sql_id,
                S.command_type,
                S.object_name,
                S.module,
                S.action,
                S.first_sample,
                S.last_sample,
                S.last_executions,
                S.last_elapsed_time,
                S.last_cpu_time,
                S.last_disk_reads,
                S.last_buffer_gets,
                S.last_user_io_wait_time,
                S.last_rows_processed,
                S.total_executions,
                S.total_elapsed_time,
                S.total_cpu_time,
                S.total_disk_reads,
                S.total_buffer_gets,
                S.total_user_io_wait_time,
                S.total_rows_processed,
                S.sql_text,
                S.sql_fulltext,
                S.last_sql_plans,
                S.occurences_top_n
            ) 
            VALUES
            (
                Q.sql_id,
                Q.command_type,
                Q.object_name,
                Q.module,
                Q.action,
                Q.begin_sample_date,
                Q.end_sample_date,
                Q.executions,
                Q.elapsed_time,
                Q.cpu_time,
                Q.disk_reads,
                Q.buffer_gets,
                Q.user_io_wait_time,
                Q.rows_processed,
                Q.executions,
                Q.elapsed_time,
                Q.cpu_time,
                Q.disk_reads,
                Q.buffer_gets,
                Q.user_io_wait_time,
                Q.rows_processed,
                Q.sql_text,
                null,
                null,
                1
            );
            
            l_number_rows_merged := SQL%ROWCOUNT;
        EXCEPTION
            -- If the update/insert fails because the numbers are too big for columns, reset report table
            WHEN value_too_big THEN
                EXECUTE IMMEDIATE 'TRUNCATE TABLE tasy_topsql_sqlarea';
                EXECUTE IMMEDIATE 'TRUNCATE TABLE tasy_topsql_sqlarea_report';
                l_number_rows_merged := 0;
        END;
        -- If rows were merged        
        IF l_number_rows_merged > 0 THEN
            -- Get SQL full text and plans
            populate_fulltext_and_plans;
            -- Keep only fresh data, remove old report data
            remove_report_data;
        END IF;
    END IF;
    
    -- Update control table last analyzed snapshot id
    UPDATE tasy_topsql_control_tab
    SET last_snap_id_reported = l_end_snap_id;
    -- Commit here to avoid losing the work if the destination database link is not working 
    COMMIT;
    
    -- If enabled, copy result to remote destination database via database link
    IF l_transfer_to_remote_db = 1 AND l_number_rows_merged > 0 THEN
        -- It may have an autocommit (if the database link needs to be created = DDL)
        transfer_to_remote_db_proc;
        COMMIT;
    END IF;
    
EXCEPTION
    WHEN others THEN
        raise_application_error(-20019,'Error trying to report last two snapshots: ' || sqlerrm);
END report_data;

/*
    Create snapshot at specified intervals (by a job)
    Snapshot id comes from the sequence "tasy_topsql_snap_seq"
    It is also responsible to call cleanup and report procedures

*/
PROCEDURE create_snapshot IS
    l_snap_sql_minimal_executions NUMBER;
    l_snap_sql_minimal_elapsed NUMBER;
    l_number_of_sqls_per_snap NUMBER;
    l_number_of_snapshots_taken NUMBER;
    l_snap_sequence NUMBER;
    l_start_time NUMBER;
    l_capture_nobind_sql_data NUMBER;
    l_snap_nobind_sql_maxcount NUMBER;
    l_number_of_rows_created NUMBER;
    
    missing_privilege EXCEPTION;
    PRAGMA EXCEPTION_INIT(missing_privilege, -942);
BEGIN
    BEGIN
        l_start_time := DBMS_UTILITY.GET_TIME;
        SELECT
            number_of_sqls_per_snap,
            snap_sql_minimal_executions,
            snap_sql_minimal_elapsed,
            number_of_snapshots_taken,
            capture_nobind_sql_data,
            snap_nobind_sql_maxcount
        INTO
            l_number_of_sqls_per_snap,
            l_snap_sql_minimal_executions,
            l_snap_sql_minimal_elapsed,
            l_number_of_snapshots_taken,
            l_capture_nobind_sql_data,
            l_snap_nobind_sql_maxcount            
        FROM
            tasy_topsql_control_tab
        -- Do not raise if there are more than one line in config table
        -- The only active configuration is sequence_number = 1
        WHERE sequence_number = 1;
    EXCEPTION
        WHEN no_data_found THEN
            INSERT INTO tasy_topsql_control_tab (
                sequence_number,
                last_updated,
                last_snap_id_reported,
                webservice_last_time,
                capture_nobind_sql_data,
                number_of_sqls_per_snap,
                number_of_sqls_per_report,
                snap_sql_minimal_executions,
                snap_sql_minimal_elapsed,
                snap_nobind_sql_maxcount,
                report_sql_minimal_executions,
                report_sql_minimal_elapsed,
                number_of_snapshots_to_retain,
                max_nobind_rows_top_keep,
                number_of_snapshots_taken,
                total_time_creating_snapthots,
                last_time_creating_snapthot,
                transfer_to_remote_db,
                remote_db_username,
                remote_db_password,
                remote_db_sqlnet_config
            ) VALUES (
                1, -- sequence_number
                sysdate, -- last_updated
                0, -- last_snap_id_reported
                sysdate, -- webservice_last_time
                1, -- capture_nobind_sql_data
                500, -- number_of_sqls_per_snap
                50, -- number_of_sqls_per_report
                5, -- snap_sql_minimal_executions
                900, -- snap_sql_minimal_elapsed
                100, -- snap_nobind_sql_maxcount
                5, -- report_sql_minimal_executions
                600, -- report_sql_minimal_elapsed
                2, -- number_of_snapshots_to_retain
                200, -- max_nobind_rows_top_keep
                0, -- number_of_snapshots_taken
                0, -- total_time_creating_snapthots
                0, -- last_time_creating_snapthot
                0, -- transfer_to_remote_db
                NULL, -- remote_db_username
                NULL, -- remote_db_password
                NULL -- remote_db_sqlnet_config
            );
            COMMIT;
            SELECT
                number_of_sqls_per_snap,
                snap_sql_minimal_executions,
                snap_sql_minimal_elapsed,
                number_of_snapshots_taken,
                capture_nobind_sql_data,
                snap_nobind_sql_maxcount
            INTO
                l_number_of_sqls_per_snap,
                l_snap_sql_minimal_executions,
                l_snap_sql_minimal_elapsed,
                l_number_of_snapshots_taken,
                l_capture_nobind_sql_data,
                l_snap_nobind_sql_maxcount            
            FROM
                tasy_topsql_control_tab
            -- Do not raise if there are more than one line in config table
            WHERE ROWNUM = 1;
    END;
    
    SELECT tasy_topsql_snap_seq.NEXTVAL
    INTO l_snap_sequence
    FROM dual;
    
    INSERT INTO tasy_topsql_sqlarea
    (
        snap_id,
        sample_date,
        sql_id,
        object_name,
        module,
        action,
        executions,
        elapsed_time,
        cpu_time,
        disk_reads,
        buffer_gets,
        user_io_wait_time,
        rows_processed,
        command_type,
        sql_text
    )
    SELECT
        l_snap_sequence,
        sysdate,
        sql_id,
        object_name,
        module,
        action,
        executions,
        elapsed_time,
        cpu_time,
        disk_reads,
        buffer_gets,
        user_io_wait_time,
        rows_processed,
        command_type,
        sql_text
    FROM (
        SELECT
            a.sql_id AS sql_id,
            MAX(a.command_type) AS command_type,
            MAX(b.object_name) AS object_name,
            MAX(a.module) AS module,
            MAX(a.action) AS action,
            SUM(a.executions) AS executions,
            round(SUM(a.elapsed_time) / 1000000) elapsed_time,
            round(SUM(a.cpu_time) / 1000000) cpu_time,
            SUM(a.disk_reads) AS disk_reads,
            SUM(a.buffer_gets) AS buffer_gets,
            round(SUM(a.user_io_wait_time) / 1000000) user_io_wait_time,
            SUM(a.rows_processed) AS rows_processed,
            MAX(a.sql_text) AS sql_text
        FROM
            sys.gv_$sqlarea a 
        LEFT OUTER JOIN dba_objects b ON(a.program_id = b.object_id)
        -- only SQLs that have executed at least X number of times
        WHERE a.executions >= l_snap_sql_minimal_executions
        -- only SQLs that were parsed by Tasy schema      
        AND a.parsing_user_id IN(
                SELECT user_id
                FROM user_users)
        -- only SQLs that consumed at least X seconds
        AND a.elapsed_time/1000000 > l_snap_sql_minimal_elapsed
        GROUP BY 
            sql_id
        ORDER BY elapsed_time DESC)
        WHERE rownum <= l_number_of_sqls_per_snap;
        
    l_number_of_rows_created := SQL%ROWCOUNT;
    
    -- If enabled, get "no bind" SQL data
    IF l_capture_nobind_sql_data = 1 THEN
        gather_nobind_sql(l_snap_nobind_sql_maxcount);
    END IF;
    
    -- If rows were created, report/analyze data
    -- report_data commits
    IF l_number_of_rows_created > 0 THEN
        report_data;
    END IF;
    
    -- Keep only data for the top-n last snapshots (depends on the values present in the configuration table)
    remove_snap_data;
    
    -- Computes statstics about the current execution and global totals
    UPDATE tasy_topsql_control_tab
    SET number_of_snapshots_taken = l_number_of_snapshots_taken + 1,
    total_time_creating_snapthots = total_time_creating_snapthots + round((DBMS_UTILITY.GET_TIME - l_start_time) * 0.01,2),
    last_time_creating_snapthot = round((DBMS_UTILITY.GET_TIME - l_start_time) * 0.01,2);
    
    COMMIT;
EXCEPTION
    WHEN missing_privilege THEN
        raise_application_error(-20011,'Missing privilege on view "sys.gv_$sqlarea".'
        || ' Please grant SELECT on sys.gv_$sqlarea to TASY');
END create_snapshot;



END tasy_topsql_pkg;
/