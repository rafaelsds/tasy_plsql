create or replace package pls_alt_dados_analise_pck as
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Esta PACKAGE, tem por objetivo controlar as rotinas de altera��o de valores da 
an�lise, gerenciado as suas a��es pelas  a��es cadastradas nos Cadastros gerais > Plano de sa�de
> OPS - Contas M�dicas > Cadastro de a��es da an�lise de contas m�dicas.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
procedure pls_gerencia_alt_dados(	cd_acao_analise_p		pls_acao_analise.cd_acao%type,			
					nr_seq_analise_p		pls_analise_conta.nr_sequencia%type,
					nr_seq_conta_p			pls_conta.nr_sequencia%type, 
					nr_seq_conta_proc_p		pls_conta_proc.nr_sequencia%type, 
					nr_seq_conta_mat_p		pls_conta_mat.nr_sequencia%type, 
					nr_seq_proc_partic_p		pls_proc_participante.nr_sequencia%type,
					nr_seq_ocorrencia_p		pls_ocorrencia.nr_sequencia%type,
					nr_seq_motivo_glosa_p		tiss_motivo_glosa.nr_sequencia%type,
					ds_observacao_p			pls_conta_glosa.ds_observacao%type,
					qt_liberada_p			pls_conta_proc.qt_procedimento%type,
					vl_liberado_p			pls_conta_proc.vl_liberado%type,
					vl_liberado_hi_p		pls_conta_proc.vl_liberado_hi%type, 
					vl_liberado_mat_p		pls_conta_proc.vl_liberado_material%type,
					vl_liberado_co_p		pls_conta_proc.vl_liberado_co%type,
					vl_lib_taxa_hi_p		pls_conta_proc.vl_lib_taxa_servico%type,
					vl_lib_taxa_mat_p		pls_conta_proc.vl_lib_taxa_material%type,
					vl_lib_taxa_co_p		pls_conta_proc.vl_taxa_co%type,
					nr_seq_grupo_atual_p		pls_auditoria_conta_grupo.nr_sequencia%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type);




end pls_alt_dados_analise_pck;
/
create or replace package body pls_alt_dados_analise_pck as

procedure pls_gerencia_alt_dados(	cd_acao_analise_p		pls_acao_analise.cd_acao%type,			
					nr_seq_analise_p		pls_analise_conta.nr_sequencia%type,
					nr_seq_conta_p			pls_conta.nr_sequencia%type, 
					nr_seq_conta_proc_p		pls_conta_proc.nr_sequencia%type, 
					nr_seq_conta_mat_p		pls_conta_mat.nr_sequencia%type, 
					nr_seq_proc_partic_p		pls_proc_participante.nr_sequencia%type,
					nr_seq_ocorrencia_p		pls_ocorrencia.nr_sequencia%type,
					nr_seq_motivo_glosa_p		tiss_motivo_glosa.nr_sequencia%type,
					ds_observacao_p			pls_conta_glosa.ds_observacao%type,
					qt_liberada_p			pls_conta_proc.qt_procedimento%type,
					vl_liberado_p			pls_conta_proc.vl_liberado%type,
					vl_liberado_hi_p		pls_conta_proc.vl_liberado_hi%type, 
					vl_liberado_mat_p		pls_conta_proc.vl_liberado_material%type,
					vl_liberado_co_p		pls_conta_proc.vl_liberado_co%type,
					vl_lib_taxa_hi_p		pls_conta_proc.vl_lib_taxa_servico%type,
					vl_lib_taxa_mat_p		pls_conta_proc.vl_lib_taxa_material%type,
					vl_lib_taxa_co_p		pls_conta_proc.vl_taxa_co%type,
					nr_seq_grupo_atual_p		pls_auditoria_conta_grupo.nr_sequencia%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type) is

begin
/*DOCUMENTADO COM NULL POIS A MESMA FOI SUBSTITUIDA 
PELA PACKAGE PLS_CTA_ALT_DADOS_PCK*/
null;
end pls_gerencia_alt_dados; 

end pls_alt_dados_analise_pck;
/