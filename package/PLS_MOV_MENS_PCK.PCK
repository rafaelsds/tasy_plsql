create or replace package pls_mov_mens_pck is

-- Vari�veis globais
pls_mov_mens_lote_w		pls_mov_mens_lote%rowtype;
pls_mov_mens_regra_w		pls_mov_mens_regra%rowtype;

procedure gerar_lote	(	nr_seq_lote_p		pls_mov_mens_lote.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type);
					
procedure desfazer_lote	(	nr_seq_lote_p		pls_mov_mens_lote.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type);

procedure gerar_titulos	(	nr_seq_lote_p		pls_mov_mens_lote.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type);
				
procedure gerar_titulo_pagar_classif ( 	nr_titulo_pagar_p	titulo_pagar.nr_titulo%type,
					nr_seq_lote_p		pls_mov_mens_lote.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type);

end pls_mov_mens_pck;
/

create or replace
package body pls_mov_mens_pck as

procedure carregar_dados_lote_regra (	nr_seq_lote_p	pls_mov_mens_lote.nr_sequencia%type) is
begin
select	*
into	pls_mov_mens_lote_w 
from	pls_mov_mens_lote
where	nr_sequencia = nr_seq_lote_p;

select	*
into	pls_mov_mens_regra_w
from	pls_mov_mens_regra
where	nr_sequencia = pls_mov_mens_lote_w.nr_seq_regra;
end carregar_dados_lote_regra;


procedure gerar_vencimentos (	nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
dt_vencimento_w		pls_mov_mens_operador_venc.dt_vencimento%type;
Cursor C01 is
	select	a.nr_sequencia nr_seq_mov_operadora,
		(	select	sum(x.vl_item)
			from	pls_mov_mens_benef_item x,
				pls_mov_mens_benef y
			where	y.nr_sequencia	= x.nr_seq_mov_benef
			and	y.nr_seq_mov_operadora = a.nr_sequencia) vl_vencimento
	from	pls_mov_mens_operadora a
	where	a.nr_seq_lote	= pls_mov_mens_lote_w.nr_sequencia;
begin
begin
dt_vencimento_w	:= trunc(pls_mov_mens_lote_w.dt_referencia,'month')+(pls_mov_mens_regra_w.nr_dia_vencimento-1);
exception
when others then
	dt_vencimento_w	:= last_day(pls_mov_mens_lote_w.dt_referencia);
end;
for r_c01_w in C01 loop
	begin
	insert	into	pls_mov_mens_operador_venc
		(	nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_mov_operadora,
			dt_vencimento, vl_vencimento )
		values(	pls_mov_mens_operador_venc_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, r_c01_w.nr_seq_mov_operadora,
			dt_vencimento_w, nvl(r_c01_w.vl_vencimento, 0));
	end;
end loop;
end gerar_vencimentos;


procedure inserir_mov_itens (	nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
Cursor C01 is
	select	b.nr_sequencia nr_seq_mov_benef,
		e.nr_sequencia nr_seq_item_mens,
		e.ie_tipo_item,
		nvl(e.dt_inicio_cobertura,d.dt_inicio_cobertura) dt_inicio_cobertura,
		nvl(e.dt_fim_cobertura,d.dt_fim_cobertura) dt_fim_cobertura,
		nvl(e.vl_item, '0') vl_item,
		nvl(e.vl_antecipacao, '0') vl_antecipacao,
		nvl(e.vl_pro_rata_dia, '0') vl_pro_rata_dia,
		nvl(e.vl_ato_cooperado, '0') vl_ato_cooperado,
		nvl(e.vl_ato_cooperado_antec, '0') vl_ato_cooperado_antec,
		nvl(e.vl_ato_cooperado_pro_rata, '0') vl_ato_cooperado_pro_rata,
		nvl(e.vl_ato_auxiliar, '0') vl_ato_auxiliar,
		nvl(e.vl_ato_auxiliar_antec, '0') vl_ato_auxiliar_antec,
		nvl(e.vl_ato_auxiliar_pro_rata, '0') vl_ato_auxiliar_pro_rata,
		nvl(e.vl_ato_nao_cooperado, '0') vl_ato_nao_cooperado,
		nvl(e.vl_ato_nao_coop_antec, '0') vl_ato_nao_coop_antec,
		nvl(e.vl_ato_nao_coop_pro_rata, '0') vl_ato_nao_coop_pro_rata
	from	pls_mov_mens_operadora	a,
		pls_mov_mens_benef	b,
		pls_mov_mens_temp	c,
		pls_mensalidade_segurado d,
		pls_mensalidade_seg_item e,
		pls_mensalidade		f
	where	a.nr_seq_lote		= pls_mov_mens_lote_w.nr_sequencia
	and	a.nr_sequencia		= b.nr_seq_mov_operadora
	and	b.nr_seq_segurado	= d.nr_seq_segurado
	and	d.nr_sequencia		= c.nr_seq_mensalidade_seg
	and	d.nr_sequencia		= e.nr_seq_mensalidade_seg
	and	f.nr_sequencia		= d.nr_seq_mensalidade
	and	f.ie_cancelamento is null
	and	e.ie_tipo_item	= '1' --Conforme defini��o de neg�cio, no momento ser� gerado apenas o pre�o pr�-estabelecido
	group by
		b.nr_sequencia,
		e.nr_sequencia,
		e.ie_tipo_item,
		e.dt_inicio_cobertura,
		e.dt_fim_cobertura,
		d.dt_inicio_cobertura,
		d.dt_fim_cobertura,
		e.vl_item,
		e.vl_pro_rata_dia,
		e.vl_antecipacao,
		e.vl_ato_auxiliar,
		e.vl_ato_auxiliar_antec,
		e.vl_ato_auxiliar_pro_rata,
		e.vl_ato_cooperado,
		e.vl_ato_cooperado_antec,
		e.vl_ato_cooperado_pro_rata,
		e.vl_ato_nao_cooperado,
		e.vl_ato_nao_coop_antec,
		e.vl_ato_nao_coop_pro_rata;
begin

for r_c01_w in C01 loop
	begin
	insert	into	pls_mov_mens_benef_item
		(	nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_mov_benef,
			dt_inicio_cobertura, dt_fim_cobertura, ie_tipo_item,
			vl_antecipacao, vl_ato_auxiliar, vl_ato_auxiliar_antec,
			vl_ato_auxiliar_pro_rata, vl_ato_cooperado, vl_ato_cooperado_antec,
			vl_ato_cooperado_pro_rata, vl_ato_nao_coop_antec, vl_ato_nao_cooperado,
			vl_ato_nao_coop_pro_rata, vl_item, vl_pro_rata_dia,
			nr_seq_item_mens)
		values(	pls_mov_mens_benef_item_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, r_c01_w.nr_seq_mov_benef,
			r_c01_w.dt_inicio_cobertura, r_c01_w.dt_fim_cobertura, r_c01_w.ie_tipo_item,
			r_c01_w.vl_antecipacao, r_c01_w.vl_ato_auxiliar, r_c01_w.vl_ato_auxiliar_antec,
			r_c01_w.vl_ato_auxiliar_pro_rata, r_c01_w.vl_ato_cooperado, r_c01_w.vl_ato_cooperado_antec,
			r_c01_w.vl_ato_cooperado_pro_rata, r_c01_w.vl_ato_nao_coop_antec , r_c01_w.vl_ato_nao_cooperado,
			r_c01_w.vl_ato_nao_coop_pro_rata, r_c01_w.vl_item, r_c01_w.vl_pro_rata_dia,
			r_c01_w.nr_seq_item_mens);
	end;
end loop;

commit;

end inserir_mov_itens;

procedure inserir_mov_benef (	nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
Cursor C01 is
	select	c.nr_sequencia nr_seq_operadora,
		b.nr_seq_segurado,
		nvl(pls_obter_dados_segurado(b.nr_seq_segurado, 'C'), '') cd_usuario_plano
	from	pls_mov_mens_temp		a,
		pls_mensalidade_segurado	b,
		pls_mov_mens_operadora		c,
		pls_mensalidade			d
	where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
	and	c.nr_seq_congenere	= a.nr_seq_congenere
	and	d.nr_sequencia		= b.nr_seq_mensalidade
	and	c.nr_seq_lote		= pls_mov_mens_lote_w.nr_sequencia
	and	d.ie_cancelamento is null
	group by
		c.nr_sequencia,
		b.nr_seq_segurado;
begin
for r_c01_w in C01 loop
	begin
	insert	into	pls_mov_mens_benef
		(	nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_lote,
			nr_seq_segurado, nr_seq_mov_operadora, cd_usuario_plano)
		values(	pls_mov_mens_benef_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, pls_mov_mens_lote_w.nr_sequencia,
			r_c01_w.nr_seq_segurado, r_c01_w.nr_seq_operadora, r_c01_w.cd_usuario_plano);
	end;
end loop;
commit;

end inserir_mov_benef;

procedure inserir_mov_operadora (	nm_usuario_p		usuario.nm_usuario%type,
					cd_estabelenerecimento_p	estabelecimento.cd_estabelecimento%type) is
Cursor C01 is
	select	nr_seq_congenere
	from	pls_mov_mens_temp
	group by nr_seq_congenere;
begin
for r_c01_w in C01 loop
	begin
	insert	into	pls_mov_mens_operadora
		(	nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_lote,
			nr_seq_congenere)
		values(	pls_mov_mens_operadora_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, pls_mov_mens_lote_w.nr_sequencia,
			r_c01_w.nr_seq_congenere);
	end;
end loop;
commit;
end inserir_mov_operadora;


---------------- INSERE TODOS OS DADOS EM UMA TABELA TEMPORARIA.
procedure gerar_dados_temp is 

ds_sql_w			VARCHAR2(32000);

tb_nr_seq_congenere_w	pls_util_cta_pck.t_number_table;
tb_nr_mensalidade_seg_w	pls_util_cta_pck.t_number_table;

valor_bind_w			sql_pck.t_dado_bind;
cursor_w			sql_pck.t_cursor;

begin

ds_sql_w := '	select	a.nr_seq_congenere,
			c.nr_sequencia nr_seq_mensalidade_seg
		from	pls_segurado_repasse a,
			pls_segurado b,
			pls_mensalidade_segurado c,
			pls_mensalidade d,
			pls_lote_mensalidade e,
			pls_segurado_carteira f
		where	b.nr_sequencia	= a.nr_seq_segurado
		and	b.nr_sequencia	= c.nr_seq_segurado
		and	d.nr_sequencia	= c.nr_seq_mensalidade
		and	e.nr_sequencia	= d.nr_seq_lote
		and	b.nr_sequencia  = f.nr_seq_segurado
		and	a.cd_estabelecimento	= :CD_ESTABELECIMENTO
		and	a.dt_liberacao is not null
		and	f.cd_usuario_plano is not null
		and	a.ie_tipo_repasse = ''P''
		and	d.ie_cancelamento is null
		and	:DT_INICIAL between a.dt_repasse and nvl(a.dt_fim_repasse,:DT_INICIAL)
		and	d.dt_referencia between :DT_INICIAL and :DT_FINAL ' ;
		
if	(pls_mov_mens_regra_w.nr_seq_congenere is not null) then
	ds_sql_w := ds_sql_w || '	and a.nr_seq_congenere	= :NR_SEQ_CONGENERE ';
	sql_pck.bind_variable(':NR_SEQ_CONGENERE', pls_mov_mens_regra_w.nr_seq_congenere, valor_bind_w);
end if;

sql_pck.bind_variable(':DT_INICIAL', pls_mov_mens_lote_w.dt_movimento_inicial, valor_bind_w);
sql_pck.bind_variable(':DT_FINAL', pls_mov_mens_lote_w.dt_movimento_final, valor_bind_w);
sql_pck.bind_variable(':CD_ESTABELECIMENTO', pls_mov_mens_regra_w.cd_estabelecimento, valor_bind_w);

cursor_w := sql_pck.executa_sql_cursor(ds_sql_w, valor_bind_w);

loop
	fetch cursor_w bulk collect into tb_nr_seq_congenere_w, tb_nr_mensalidade_seg_w
	limit pls_util_pck.qt_registro_transacao_w;
	exit when tb_nr_mensalidade_seg_w.count = 0;
	
	forall i in tb_nr_mensalidade_seg_w.first .. tb_nr_mensalidade_seg_w.last
	
		insert	into	pls_mov_mens_temp
			(	nr_seq_congenere,
				nr_seq_mensalidade_seg)
			values (tb_nr_seq_congenere_w(i),
				tb_nr_mensalidade_seg_w(i) );
	commit;
end loop;

close cursor_w;

end gerar_dados_temp;

---------------- INICIO DO PROCESSO ----------------------
procedure gerar_lote	(	nr_seq_lote_p		pls_mov_mens_lote.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
qt_registros_w	pls_integer;
begin
carregar_dados_lote_regra(nr_seq_lote_p);

if	(nvl(pls_mov_mens_regra_w.nr_dia_vencimento,0) = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1076944); --Dia de vencimento n�o informado na regra utilizada para gera��o do lote!
end if;

exec_sql_dinamico(nm_usuario_p, 'truncate table pls_mov_mens_temp');

pls_mov_mens_lote_w.dt_movimento_inicial	:= trunc(pls_mov_mens_lote_w.dt_movimento_inicial,'dd');
pls_mov_mens_lote_w.dt_movimento_final		:= fim_dia(pls_mov_mens_lote_w.dt_movimento_final);

gerar_dados_temp;

select	count(1)
into	qt_registros_w
from	pls_mov_mens_temp;

if	(qt_registros_w > 0) then
	inserir_mov_operadora(nm_usuario_p,cd_estabelecimento_p);
	inserir_mov_benef(nm_usuario_p,cd_estabelecimento_p);
	inserir_mov_itens(nm_usuario_p,cd_estabelecimento_p);
	
	gerar_vencimentos(nm_usuario_p,cd_estabelecimento_p);
	
	update	pls_mov_mens_lote 
	set	dt_geracao_lote	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia = nr_seq_lote_p;
else
	wheb_mensagem_pck.exibir_mensagem_abort(1067661); -- 'N�o existem informa��es para gerar o lote.';
end if;

commit;

end gerar_lote;

---------------- INICIO DO PROCESSO DESFAZER O LOTE ----------------------
procedure desfazer_lote	(	nr_seq_lote_p		pls_mov_mens_lote.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
begin

delete	from pls_mov_mens_benef
where	nr_seq_lote =	nr_seq_lote_p;

delete	from	pls_mov_mens_operador_venc a
where	exists (select	1
		from	pls_mov_mens_operadora x
		where	x.nr_sequencia	= a.nr_seq_mov_operadora
		and	x.nr_seq_lote	= nr_seq_lote_p);

delete	from pls_mov_mens_operadora
where	nr_seq_lote =	nr_seq_lote_p;

update	pls_mov_mens_lote 
set	dt_geracao_lote	= null,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia = nr_seq_lote_p;

commit;

end desfazer_lote;

procedure gerar_titulos	(	nr_seq_lote_p		pls_mov_mens_lote.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
nr_titulo_pagar_w	titulo_pagar.nr_titulo%type;
dt_contabil_w		pls_mensalidade.dt_referencia%type;
Cursor C01 is
	select	a.nr_sequencia nr_seq_vencimento,
		a.dt_vencimento,
		a.vl_vencimento,
		c.cd_cgc
	from	pls_mov_mens_operador_venc	a,
		pls_mov_mens_operadora		b,
		pls_congenere			c
	where	b.nr_sequencia	= a.nr_seq_mov_operadora
	and	c.nr_sequencia	= b.nr_seq_congenere
	and	b.nr_seq_lote	= nr_seq_lote_p;

begin
carregar_dados_lote_regra(nr_seq_lote_p);

if	(pls_mov_mens_regra_w.cd_moeda is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1076946); --Moeda n�o informada na regra utilizada para gera��o do lote!
end if;

if	(pls_mov_mens_regra_w.nr_seq_trans_fin_baixa is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1076947); --Transa��o financeira de baixa n�o informada na regra utilizada para gera��o do lote!
end if;

if	(pls_mov_mens_regra_w.nr_seq_trans_fin_contab is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1076948); --Transa��o financeira cont�bil n�o informada na regra utilizada para gera��o do lote!
end if;

for r_c01_w in C01 loop
	begin
	
	select	max(e.dt_referencia)
	into	dt_contabil_w
	from	pls_mensalidade_seg_item	a,
		pls_mov_mens_benef_item		b,
		pls_mov_mens_benef		c,
		pls_mov_mens_operadora		o,
		pls_mov_mens_operador_venc	l,
		pls_mensalidade_segurado	s,
		pls_mensalidade			e
	where	b.nr_seq_item_mens		= a.nr_sequencia
	and	b.nr_seq_mov_benef		= c.nr_sequencia
	and	c.nr_seq_mov_operadora		= o.nr_sequencia
	and	o.nr_sequencia			= l.nr_seq_mov_operadora
	and	a.nr_seq_mensalidade_seg	= s.nr_sequencia
	and	s.nr_seq_mensalidade		= e.nr_sequencia
	and	o.nr_seq_lote			= nr_seq_lote_p;
	
	insert	into	titulo_pagar
	(	nr_titulo, cd_estabelecimento, dt_atualizacao,
		nm_usuario, dt_emissao, dt_vencimento_original,
		dt_vencimento_atual, dt_contabil, vl_titulo, vl_saldo_titulo,
		vl_saldo_juros, vl_saldo_multa, cd_moeda,
		tx_juros, tx_multa, cd_tipo_taxa_juro,
		cd_tipo_taxa_multa, ie_situacao, ie_origem_titulo,
		ie_tipo_titulo, cd_cgc, nr_seq_trans_fin_baixa,
		nr_seq_trans_fin_contab)
	values(	titulo_pagar_seq.nextval, cd_estabelecimento_p, sysdate,
		nm_usuario_p, sysdate, r_c01_w.dt_vencimento,
		r_c01_w.dt_vencimento, dt_contabil_w, r_c01_w.vl_vencimento, r_c01_w.vl_vencimento,
		0, 0, pls_mov_mens_regra_w.cd_moeda,
		0, 0, 1,
		1, 'A', '26',
		10, r_c01_w.cd_cgc, pls_mov_mens_regra_w.nr_seq_trans_fin_baixa,
		pls_mov_mens_regra_w.nr_seq_trans_fin_contab)
	returning nr_titulo into nr_titulo_pagar_w;
	
	atualizar_inclusao_tit_pagar(nr_titulo_pagar_w,nm_usuario_p);
	
	update	pls_mov_mens_operador_venc
	set	nr_titulo_pagar	= nr_titulo_pagar_w,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= r_c01_w.nr_seq_vencimento;
	
	gerar_titulo_pagar_classif(nr_titulo_pagar_w, nr_seq_lote_p, nm_usuario_p, cd_estabelecimento_p);
	
	gerar_tributo_titulo(nr_titulo_pagar_w, nm_usuario_p, 'N', null, null, null, null, null, cd_estabelecimento_p, null);
	
	end;
end loop;

update	pls_mov_mens_lote 
set	dt_geracao_titulo	= sysdate,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_lote_p;

commit;

end gerar_titulos;

procedure gerar_titulo_pagar_classif ( 	nr_titulo_pagar_p	titulo_pagar.nr_titulo%type,
					nr_seq_lote_p		pls_mov_mens_lote.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type)is
nr_contador_w				number(10) := 0;
nr_seq_classif_w			number(5) := 0;

Cursor c_itens is
	select	a.cd_conta_ato_cooperado,
		a.cd_conta_ato_auxiliar,
		a.cd_conta_ato_nao_coop,
		a.cd_conta_rec,
		sum(a.vl_ato_auxiliar) vl_ato_auxiliar,
		sum(a.vl_ato_cooperado) vl_ato_cooperado,
		sum(a.vl_ato_nao_cooperado) vl_ato_nao_cooperado,
		sum(a.vl_item) vl_item
	from	pls_mensalidade_seg_item	a,
		pls_mov_mens_benef_item		b,
		pls_mov_mens_benef		c,
		pls_mov_mens_operadora		o,
		pls_mov_mens_operador_venc	l
	where	b.nr_seq_item_mens	= a.nr_sequencia
	and	b.nr_seq_mov_benef	= c.nr_sequencia
	and	c.nr_seq_mov_operadora	= o.nr_sequencia
	and	o.nr_sequencia		= l.nr_seq_mov_operadora
	and	l.nr_titulo_pagar	= nr_titulo_pagar_p
	group by a.cd_conta_ato_cooperado,
		a.cd_conta_ato_auxiliar,
		a.cd_conta_ato_nao_coop,
		a.cd_conta_rec;

c_itens_w	c_itens%rowtype;
	
Cursor	c_valor is
	-- Valor ato cooperado
	select	'AC' ie_ato_cooperado
	from	dual
	where	((nvl(c_itens_w.vl_ato_cooperado,0) <> 0)
	or	(nvl(c_itens_w.vl_ato_auxiliar,0) <> 0)
	or	(nvl(c_itens_w.vl_ato_nao_cooperado,0) <> 0))
	union all
	-- Valor ato auxiliar
	select	'AA' ie_ato_cooperado
	from	dual
	where	((nvl(c_itens_w.vl_ato_cooperado,0) <> 0)
	or	(nvl(c_itens_w.vl_ato_auxiliar,0) <> 0)
	or	(nvl(c_itens_w.vl_ato_nao_cooperado,0) <> 0))
	union all
	-- Valor ato n�o cooperado
	select	'AN' ie_ato_cooperado
	from	dual
	where	((nvl(c_itens_w.vl_ato_cooperado,0) <> 0)
	or	(nvl(c_itens_w.vl_ato_auxiliar,0) <> 0)
	or	(nvl(c_itens_w.vl_ato_nao_cooperado,0) <> 0))
	union all
	-- Sem valor de ato cooperado
	select	'N' ie_ato_cooperado
	from	dual
	where	((nvl(c_itens_w.vl_ato_cooperado,0) = 0)
	or	(nvl(c_itens_w.vl_ato_auxiliar,0) = 0)
	or	(nvl(c_itens_w.vl_ato_nao_cooperado,0) = 0));
	
c_valor_w	c_valor%rowtype;

begin

open c_itens;
loop
fetch c_itens into
	c_itens_w;
exit when c_itens%notfound;
	begin
	nr_contador_w 		:= nr_contador_w + 1;
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_classif_w
	from	titulo_pagar_classif
	where	nr_titulo = nr_titulo_pagar_p;
	
	open c_valor;
	loop
	fetch c_valor into
		c_valor_w;
	exit when c_valor%notfound;
		begin		
		nr_seq_classif_w		:= nr_seq_classif_w + 1;
		if	(c_valor_w.ie_ato_cooperado = 'AC') then
			c_itens_w.cd_conta_rec		:= c_itens_w.cd_conta_ato_cooperado;
			c_itens_w.vl_item		:= c_itens_w.vl_ato_cooperado;
		elsif	(c_valor_w.ie_ato_cooperado = 'AA') then
			c_itens_w.cd_conta_rec		:= c_itens_w.cd_conta_ato_auxiliar;
			c_itens_w.vl_item		:= c_itens_w.vl_ato_auxiliar;
		elsif	(c_valor_w.ie_ato_cooperado = 'AN') then
			c_itens_w.cd_conta_rec		:= c_itens_w.cd_conta_ato_nao_coop;
			c_itens_w.vl_item		:= c_itens_w.vl_ato_nao_cooperado;
		elsif	(c_valor_w.ie_ato_cooperado = 'N') then
			c_itens_w.cd_conta_rec		:= c_itens_w.cd_conta_rec;
			c_itens_w.vl_item		:= c_itens_w.vl_item;
		end if;
		
		insert into titulo_pagar_classif(
			nr_sequencia, 
			dt_atualizacao, 
			nm_usuario, 
			cd_conta_contabil, 
			vl_titulo,
			nr_titulo)
		values( nr_seq_classif_w, 
			sysdate, 
			nm_usuario_p, 
			c_itens_w.cd_conta_rec, 
			c_itens_w.vl_item,
			nr_titulo_pagar_p);
			
		if 	(mod(nr_contador_w,1000) = 0) then
			commit;
		end if;
		
		end;
	end loop;
	close c_valor;	
	end;
end loop;
close c_itens;

commit;

end gerar_titulo_pagar_classif;

end pls_mov_mens_pck;
/