create or replace 
package recommendation_json_pck  as
   
	function get_recomm_times				(	nr_cpoe_recomm_p		number) return philips_json_list;
	
	function get_recomm_message			(	nr_cpoe_recomm_p		number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json;		
	
	function get_recomm_message_clob(nr_cpoe_recomm_p		number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob;
	
	procedure getCpoeIntegracaoRecom(nr_seq_rec_cpoe_p number, nr_entity_identifier_p out number);

end recommendation_json_pck;
/

create or replace 
package body recommendation_json_pck as
		
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			varchar2) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;
	
	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			number) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;

	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			date) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,pkg_date_utils.get_isoformat(value_p));
	end if;	
	end add_json_value;
	
	function get_default_message(	nr_cpoe_recomm_p	number ) return philips_json is
	
	json_encounter_w		philips_json;
	json_patient_w			philips_json;
	cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
	json_return_w			philips_json;
	json_allergy_w   		philips_json_list;
	
	nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
	
	begin
	
	json_return_w	:= philips_json();
	
	select max(nr_atendimento)
	into nr_atendimento_w
	from cpoe_recomendacao
	where nr_sequencia = nr_cpoe_recomm_p;
	
	SELECT MAX(CD_PESSOA_FISICA)
	INTO CD_PESSOA_FISICA_W
	FROM ATENDIMENTO_PACIENTE
	WHERE NR_ATENDIMENTO = nr_atendimento_w;
	
	json_encounter_w		:= encounter_json_pck.get_encounter(nr_atendimento_w);
	json_patient_w			:= person_json_pck.get_person(cd_pessoa_fisica_w);
	json_allergy_w			:= person_json_pck.get_allergy(cd_pessoa_fisica_w);
	
	json_return_w.put('patientIdentification',json_patient_w.to_json_value);
	json_return_w.put('patientVisit',json_encounter_w.to_json_value);
	json_return_w.put('patientAllergy',json_allergy_w.to_json_value);
	
	return json_return_w;
	
	end get_default_message;	
	
	function get_recomm_times				(nr_cpoe_recomm_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	Cursor C01 is
		select  a.nr_sequencia prescription_item_sequence,
				a.cd_recomendacao recommendation_code,
				elimina_acentuacao(OBTER_DESC_RECOMENDACAO(a.CD_RECOMENDACAO)) recommendation_name,
				Obter_Pessoa_Fisica_Usuario(a.nm_usuario_nrec,'C') ordering_user_id_number,
				obter_conv_envio('INTERVALO_PRESCRICAO', 'CD_INTERVALO', a.CD_INTERVALO, 'E')  interval_code_value,
				elimina_acentuacao(OBTER_DESC_TOPOGRAFIA(a.NR_SEQ_TOPOGRAFIA))  topography_name,
				a.NR_SEQ_TOPOGRAFIA topography_code,
				decode(a.DS_RECOMENDACAO,null,'','. ' ||elimina_acentuacao(a.DS_RECOMENDACAO)) relevant_clinical_information,
				a.cd_medico ordering_provider_id_number,
				obter_dados_pf(a.cd_medico,'PNG') ordering_provider_given_name,
				obter_dados_pf(a.cd_medico,'PNL') ordering_provider_last_name,
				obter_dados_pf(a.cd_medico,'PNM') ordering_provider_middle_name,
				cpoe_obter_dt_suspensao(nr_sequencia,'R') dt_suspensao,
				Obter_Pessoa_Fisica_Usuario(a.nm_usuario_susp,'C') order_prov_susp_id_number,
				a.dt_liberacao	requested_date_time,
				a.dt_inicio,
				a.dt_fim end_date_time,
				a.dt_lib_suspensao suspended_lib_time,
				substr(obter_nome_pf(a.cd_medico),1,80) nm_medico_solicitante,
				sysdate actual_date,
				a.ie_urgencia ie_urgencia,
				a.ie_administracao,
				a.dt_atualizacao,
				a.cd_setor_atendimento recomm_department_code,				
				obter_conv_envio('TIPO_RECOMENDACAO', 'CD_TIPO_RECOMENDACAO', a.CD_RECOMENDACAO, 'E')  code_value,
				obter_conv_envio('TIPO_RECOMENDACAO', 'CD_TIPO_RECOMENDACAO', a.CD_RECOMENDACAO, 'S')  code_system
		FROM cpoe_recomendacao a
		WHERE nr_sequencia = nr_cpoe_recomm_p;
		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();
		add_json_value(json_item_w, 'prescriptionItemSequence', r_c01.prescription_item_sequence);
		add_json_value(json_item_w, 'recommendationCode', r_c01.recommendation_code);
		add_json_value(json_item_w, 'recommendationName', r_c01.recommendation_name);
		add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ordering_provider_id_number);
		add_json_value(json_item_w, 'intervalCodeValue', r_c01.interval_code_value);
		add_json_value(json_item_w, 'topographyName', r_c01.topography_name);
		add_json_value(json_item_w, 'topographyCode', r_c01.topography_code);
		add_json_value(json_item_w, 'relevantClinicalInformation', r_c01.relevant_clinical_information);
		add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);
		add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
		add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
		add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
		add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);
		add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
		add_json_value(json_item_w, 'requestedDateTime', r_c01.requested_date_time);
		add_json_value(json_item_w, 'dtInicio', r_c01.dt_inicio);
		add_json_value(json_item_w, 'endDateTime', r_c01.end_date_time);
		
		if (r_c01.end_date_time is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.end_date_time));
		end if;
		
		add_json_value(json_item_w, 'suspendedLibTime', r_c01.suspended_lib_time);
		add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
		
		add_json_value(json_item_w, 'actualDate', r_c01.actual_date);
		add_json_value(json_item_w, 'ieUrgencia', r_c01.ie_urgencia);
		add_json_value(json_item_w, 'ieDdministracao', r_c01.ie_administracao);
		add_json_value(json_item_w, 'dtAtualizacao', r_c01.dt_atualizacao);
		add_json_value(json_item_w, 'recommDepartmentCode', r_c01.recomm_department_code);
		add_json_value(json_item_w, 'codeValue', r_c01.code_value);
		add_json_value(json_item_w, 'codeSystem', r_c01.code_system);
		
		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.suspended_lib_time, r_c01.requested_date_time));
		
		add_json_value(json_item_w, 'cdPrescritor', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		
		if (r_c01.ie_administracao in ('N', 'C')) then
			add_json_value(json_item_w, 'priority', 'PRN');
		elsif (r_c01.ie_urgencia = '0') then
			add_json_value(json_item_w, 'priority', 'S');
		elsif (r_c01.ie_urgencia = '5') then
			add_json_value(json_item_w, 'priority', 'TM5');
		elsif (r_c01.ie_urgencia = '10') then
			add_json_value(json_item_w, 'priority', 'TM10');
		elsif (r_c01.ie_urgencia = '15') then
			add_json_value(json_item_w, 'priority', 'TM15');
		else
			add_json_value(json_item_w, 'priority', 'R');
		end if;
		
		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_recomm_times;
	
	
	function get_recomm_message			(nr_cpoe_recomm_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json is
	json_return_w		philips_json;
	json_item_list_w   	philips_json_list;
	
	begin
	
	json_item_list_w	:= get_recomm_times(nr_cpoe_recomm_p);
	
	if	(json_item_list_w.count > 0) then
		json_return_w	:= philips_json();
		json_return_w		:= get_default_message(nr_cpoe_recomm_p);
		add_json_value(json_return_w, 'orderControl', ie_order_control_p);
		
		add_json_value(json_return_w, 'entityidentifier', nr_entity_identifier_p);
		
		json_return_w.put('recommendationList', json_item_list_w.to_json_value());
	end if;
	
	return json_return_w;
	
	end get_recomm_message;
					

	function get_recomm_message_clob	(nr_cpoe_recomm_p		number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob is
	ds_json_out_w		clob;
	json_recomm_w	philips_json;
	
	begin
	
	json_recomm_w		:= get_recomm_message(nr_cpoe_recomm_p, ie_order_control_p, nr_entity_identifier_p);
	
	if	(json_recomm_w is null) then
		return null;
	end if;
	
	dbms_lob.createtemporary( ds_json_out_w, true);
	json_recomm_w.to_clob(ds_json_out_w);
	
	return ds_json_out_w;
	end get_recomm_message_clob;


	procedure getCpoeIntegracaoRecom(nr_seq_rec_cpoe_p number, nr_entity_identifier_p out number) is
	begin

	select max(nr_sequencia)
	into nr_entity_identifier_p
	from cpoe_integracao
	where nr_seq_rec_cpoe = nr_seq_rec_cpoe_p;
	
	if (nr_entity_identifier_p is null) then
		select CPOE_INTEGRACAO_SEQ.nextval
		into nr_entity_identifier_p
		from dual;

		insert into cpoe_integracao (
			nr_sequencia,
			nr_seq_rec_cpoe
			)
		values (
			nr_entity_identifier_p,
			nr_seq_rec_cpoe_p
		);
		/* No commit so it can be used inside trigger */
	end if;	

	end getCpoeIntegracaoRecom;	

end recommendation_json_pck;
/
