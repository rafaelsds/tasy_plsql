CREATE OR REPLACE PACKAGE clinical_pathway_preview_pck AS

    cd_pessoa_fisica_w atendimento_paciente.cd_pessoa_fisica%TYPE;

    function Get_encounter_type(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE,dt_encounter_date_p in protocolo_int_pac_evento.dt_prevista%TYPE)
    RETURN VARCHAR2;

    function Get_cd_pessoa_fisica(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE)
    RETURN atendimento_paciente.cd_pessoa_fisica%TYPE;

    TYPE header_row_t IS RECORD(
        SEQ_EVENTO VARCHAR2(255),
        SEQ_ETAPA protocolo_int_pac_etapa.nr_sequencia%TYPE,
        DS_EVENTO protocolo_int_pac_evento.ds_evento_plano%TYPE,
        DS_ETAPA protocolo_int_pac_etapa.ds_etapa%TYPE,
        IE_OCULTAR_EVENTO protocolo_int_pac_evento.ie_ocultar_visao_paciente%TYPE,
        IE_OCULTAR_ETAPA protocolo_int_pac_etapa.ie_ocultar_visao_paciente%TYPE,
        NR_DIAS_ETAPA protocolo_int_pac_etapa.nr_dias_real%TYPE,
        NR_DIA_EVENTO protocolo_int_pac_evento.nr_dia_protocolo%TYPE,
        NR_DIA_INICIAL protocolo_int_pac_etapa.nr_dia_inicial%TYPE,
        NR_DIA_FINAL protocolo_int_pac_etapa.nr_dia_final%TYPE,
        NR_SEQ_PROTOCOLO_INTEGRADO protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE,
        IE_EVENTO_SUMARIO protocolo_int_pac_etapa.ie_evento_sumario%TYPE,
        DT_PREVISTA protocolo_int_pac_evento.dt_prevista%TYPE,
        CD_DPC VARCHAR2(255),
        DS_OUTCOMES VARCHAR2(2000),
        QTD_OUTCOMES VARCHAR2(2000),
        COLUMN_ID VARCHAR2(255),
        ENCOUNTER_TYPE VARCHAR2(255)
    );

    TYPE header_table_t IS TABLE OF header_row_t;

    TYPE column_row_t IS RECORD(
        NR_SEQUENCIA protocolo_int_pac_etapa.nr_sequencia%TYPE,
        NR_SEQ_EVENTO protocolo_int_pac_evento.nr_sequencia%TYPE,
        IE_TIPO_EVENTO protocolo_int_pac_evento.ie_tipo_evento%TYPE,
        DS_TIPO VARCHAR2(255),
        NR_SEQ_ACAO gqa_acao.nr_sequencia%TYPE,
        DS_ACAO gqa_acao.ds_acao%TYPE,
        NR_SEQ_PROTOCOLO_INT_PAC protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE,
        NR_ORDEM_TIPO_EVENTO protocolo_int_pac_tp_event.nr_ordem%TYPE,
        NR_ORDEM_PLANO protocolo_int_pac_categori.nr_ordem%TYPE,
        NR_SEQ_ORIGEM protocolo_int_pac_evento.nr_seq_origem%TYPE,
        COLUMN_ID VARCHAR2(255),
        ENCOUNTER_TYPE VARCHAR2(255)
    );

    TYPE column_table_t IS TABLE OF column_row_t;

    TYPE plan_row_t IS RECORD(
        NR_SEQ_EVENTO protocolo_int_pac_evento.nr_sequencia%TYPE,
        IE_TIPO_EVENTO protocolo_int_pac_evento.ie_tipo_evento%TYPE,
        NR_SEQ_ACAO gqa_acao.nr_sequencia%TYPE,
        DS_PLANO gqa_acao.ds_plano%TYPE,
        DS_USUARIO pessoa_fisica.nm_pessoa_fisica%TYPE,
        IE_HAS_EXECUTED VARCHAR2(1),
        IE_HAS_EXECUTED_EXTERNAL_CP VARCHAR2(1),
        IE_HAS_VARIANCE_CANDIDATE VARCHAR2(1),
        IE_HAS_VARIANCE VARCHAR2(1),
        IE_FINISHED VARCHAR2(1),
        COLUMN_ID VARCHAR2(255)
    );

    TYPE plan_table_t IS TABLE OF plan_row_t;

    FUNCTION get_patient_preview_header(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE, ie_nome_amigavel_p VARCHAR2 := 'N')
    RETURN header_table_t PIPELINED;

    FUNCTION get_header_event_id(ds_evento_p protocolo_int_pac_evento.ds_evento_plano%TYPE, nr_dia_evento_p protocolo_int_pac_evento.nr_dia_protocolo%TYPE, nr_ordem_p protocolo_int_pac_evento.nr_seq_ordem%TYPE)
    RETURN VARCHAR2;

    FUNCTION get_header_events_id(seq_etapa_p protocolo_int_pac_etapa.nr_sequencia%TYPE, nr_dia_evento_p protocolo_int_pac_evento.nr_dia_protocolo%TYPE, nr_ordem_p protocolo_int_pac_evento.nr_seq_ordem%TYPE)
    RETURN VARCHAR2;

    FUNCTION get_patient_preview_column(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE)
    RETURN column_table_t PIPELINED;

    FUNCTION get_preview_plan(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE, nr_seq_acao_p gqa_acao.nr_sequencia%TYPE, ie_tipo_evento_p in protocolo_int_pac_evento.ie_tipo_evento%TYPE, ie_nome_amigavel_p VARCHAR2 := 'N')
    RETURN plan_table_t PIPELINED;

    FUNCTION get_preview_patient_outcome(nr_seq_etapa_p NUMBER, ie_count_p VARCHAR2 := 'N', ie_nome_amigavel_p VARCHAR2 := 'N')
    RETURN VARCHAR2;

END clinical_pathway_preview_pck;
/

CREATE OR REPLACE PACKAGE BODY clinical_pathway_preview_pck AS

    function Get_cd_pessoa_fisica(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE) 
    RETURN atendimento_paciente.cd_pessoa_fisica%TYPE
    IS
    BEGIN
      IF cd_pessoa_fisica_w IS null THEN
        select max(pp.cd_pessoa_fisica) into cd_pessoa_fisica_w
        FROM protocolo_int_pac_etapa a
        LEFT JOIN protocolo_int_paciente PP ON
             pp.nr_sequencia = a.nr_seq_protocolo_int_pac
        WHERE
        a.nr_seq_protocolo_int_pac = nr_seq_protocolo_int_pac_p; 
        RETURN cd_pessoa_fisica_w;        
      ELSE
        RETURN cd_pessoa_fisica_w;
      END IF;
    END Get_cd_pessoa_fisica;

function Get_encounter_type(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE,dt_encounter_date_p in protocolo_int_pac_evento.dt_prevista%TYPE)
    RETURN VARCHAR2
    IS
      pessoa_fisica_w atendimento_paciente.cd_pessoa_fisica%TYPE;
      ie_tipo_atendimento_w  atendimento_paciente.ie_tipo_atendimento%TYPE;
    BEGIN
      pessoa_fisica_w := Get_cd_pessoa_fisica(nr_seq_protocolo_int_pac_p);
      select NVL(max(ie_tipo_atendimento),-1) into ie_tipo_atendimento_w
      from atendimento_paciente ap
      where TRUNC(ap.dt_entrada) = TRUNC(dt_encounter_date_p)
      and ap.cd_pessoa_fisica = pessoa_fisica_w;

      if ie_tipo_atendimento_w <> -1 then
        RETURN obter_valor_dominio(12, ie_tipo_atendimento_w); 
      end if;

      RETURN NULL;
    END Get_encounter_type;


    FUNCTION get_header_event_id(ds_evento_p protocolo_int_pac_evento.ds_evento_plano%TYPE, nr_dia_evento_p protocolo_int_pac_evento.nr_dia_protocolo%TYPE, nr_ordem_p protocolo_int_pac_evento.nr_seq_ordem%TYPE)
    RETURN VARCHAR2
    IS
    BEGIN
        RETURN ds_evento_p || '_' || nr_dia_evento_p || '_' || nr_ordem_p;
    END;

    FUNCTION get_header_events_id(seq_etapa_p protocolo_int_pac_etapa.nr_sequencia%TYPE, nr_dia_evento_p protocolo_int_pac_evento.nr_dia_protocolo%TYPE, nr_ordem_p protocolo_int_pac_evento.nr_seq_ordem%TYPE)
    RETURN VARCHAR2
    IS
        ids_w VARCHAR2(255) := '';
    BEGIN

        FOR i IN ( select nr_sequencia
                     from protocolo_int_pac_evento
                    where nr_seq_prt_int_pac_etapa = seq_etapa_p
                      and nr_seq_ordem = nr_ordem_p
                      and nr_dia_protocolo = nr_dia_evento_p ) LOOP

        ids_w := ids_w || i.nr_sequencia || '_';

        END LOOP;

        RETURN ids_w;
    END;

    FUNCTION get_patient_preview_header(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE, ie_nome_amigavel_p VARCHAR2 := 'N')
    RETURN header_table_t PIPELINED
    IS

        CURSOR header_data_c IS
            SELECT
                COLUMN_ID,
                SEQ_ETAPA,
                DS_EVENTO,
                DS_ETAPA,
                IE_OCULTAR_EVENTO,
                IE_OCULTAR_ETAPA,
                NR_DIAS_ETAPA,
                NR_DIA_EVENTO,
                NR_DIA_INICIAL,
                NR_DIA_FINAL,
                NR_SEQ_PROTOCOLO_INTEGRADO,
                IE_EVENTO_SUMARIO,
                DT_PREVISTA,
                get_preview_patient_outcome(SEQ_ETAPA, 'N', ie_nome_amigavel_p) DS_OUTCOMES,
                CD_DPC,
                ENCOUNTER_TYPE
            FROM (
              SELECT
                b.nr_sequencia SEQ_ETAPA,
                Nvl( Decode(ie_nome_amigavel_p, 'S', ds_evento_plano_paciente, ds_evento_plano), ds_evento_plano ) DS_EVENTO,
                Nvl( Decode(ie_nome_amigavel_p, 'S', ds_etapa_paciente, ds_etapa), ds_etapa ) DS_ETAPA,
                a.ie_ocultar_visao_paciente IE_OCULTAR_EVENTO,
                b.ie_ocultar_visao_paciente IE_OCULTAR_ETAPA,
                b.nr_dias_real NR_DIAS_ETAPA,
                a.nr_dia_protocolo NR_DIA_EVENTO,
                b.nr_dia_inicial,
                b.nr_dia_final,
                b.nr_seq_protocolo_int_pac NR_SEQ_PROTOCOLO_INTEGRADO,
                b.ie_evento_sumario,
                a.dt_prevista,
                a.nr_seq_ordem NR_ORDEM,
                obter_codigo_dpc(d.nr_atendimento_origem, a.dt_prevista) CD_DPC,
                a.ds_evento_plano || '_' || a.nr_dia_protocolo || '_' || a.nr_seq_ordem || '_' || Nvl(a.ie_ocultar_visao_paciente, 'N') COLUMN_ID,
                Get_encounter_type(nr_seq_protocolo_int_pac_p,a.dt_prevista) ENCOUNTER_TYPE
            FROM protocolo_int_pac_evento a
            INNER JOIN protocolo_int_pac_etapa b ON
                b.nr_sequencia = a.nr_seq_prt_int_pac_etapa
            INNER JOIN protocolo_int_paciente d ON
                b.nr_seq_protocolo_int_pac = d.nr_sequencia
            LEFT JOIN protocolo_int_etapa_resul c ON
                c.nr_seq_etapa = b.nr_sequencia
            WHERE
                b.nr_seq_protocolo_int_pac = nr_seq_protocolo_int_pac_p
             )
            GROUP BY
                COLUMN_ID,
                SEQ_ETAPA,
                DS_EVENTO,
                DS_ETAPA,
                IE_OCULTAR_EVENTO,
                IE_OCULTAR_ETAPA,
                NR_DIAS_ETAPA,
                NR_DIA_EVENTO,
                NR_DIA_INICIAL,
                NR_DIA_FINAL,
                NR_SEQ_PROTOCOLO_INTEGRADO,
                IE_EVENTO_SUMARIO,
                DT_PREVISTA,
                get_preview_patient_outcome(SEQ_ETAPA, 'N', ie_nome_amigavel_p),
                CD_DPC,
                get_header_events_id(SEQ_ETAPA, NR_DIA_EVENTO, NR_ORDEM),
                NR_ORDEM,
                ENCOUNTER_TYPE
            ORDER BY nr_dia_evento, nr_ordem, nr_seq_protocolo_integrado;

            header_row_w header_row_t;

    BEGIN

        FOR header_data_r IN header_data_c LOOP

            header_row_w.column_id := header_data_r.column_id;
            header_row_w.seq_etapa := header_data_r.seq_etapa;
            header_row_w.ds_evento := header_data_r.ds_evento;
            header_row_w.ds_etapa := header_data_r.ds_etapa;
            header_row_w.ie_ocultar_evento := header_data_r.ie_ocultar_evento;
            header_row_w.ie_ocultar_etapa := header_data_r.ie_ocultar_etapa;
            header_row_w.nr_dias_etapa := header_data_r.nr_dias_etapa;
            header_row_w.nr_dia_evento := header_data_r.nr_dia_evento;
            header_row_w.nr_dia_inicial := header_data_r.nr_dia_inicial;
            header_row_w.nr_dia_final := header_data_r.nr_dia_final;
            header_row_w.nr_seq_protocolo_integrado := header_data_r.nr_seq_protocolo_integrado;
            header_row_w.ie_evento_sumario := header_data_r.ie_evento_sumario;
            header_row_w.dt_prevista := header_data_r.dt_prevista;
            header_row_w.ds_outcomes := header_data_r.ds_outcomes;
            header_row_w.ENCOUNTER_TYPE := header_data_r.ENCOUNTER_TYPE;
            pipe row(header_row_w);

        END LOOP;
        RETURN;
    END get_patient_preview_header;

    FUNCTION get_patient_preview_column(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE)
    RETURN column_table_t PIPELINED
    IS

        CURSOR column_data_c IS
            SELECT
                a.nr_sequencia,
                b.ie_tipo_evento,
                obter_descricao_dominio(8828, b.ie_tipo_evento) DS_TIPO,
                d.nr_sequencia NR_SEQ_ACAO,
                d.ds_acao,
                a.nr_seq_protocolo_int_pac,
                MAX(e.nr_ordem) NR_ORDEM_TIPO_EVENTO,
                MAX(f.nr_ordem) NR_ORDEM_PLANO,
                b.nr_seq_origem,
                b.nr_sequencia NR_SEQ_EVENTO,
                b.ds_evento_plano || '_' || b.nr_dia_protocolo || '_' || b.nr_seq_ordem || '_' || Nvl(b.ie_ocultar_visao_paciente, 'N') COLUMN_ID,
                Get_encounter_type(nr_seq_protocolo_int_pac_p,b.dt_prevista) ENCOUNTER_TYPE,
                b.dt_prevista
            FROM protocolo_int_pac_etapa a
            INNER JOIN protocolo_int_pac_evento b ON
                a.nr_sequencia = b.nr_seq_prt_int_pac_etapa
            LEFT JOIN gqa_pendencia_regra c ON
                c.nr_sequencia = b.nr_seq_plano AND
                c.ie_situacao = 'A'
            LEFT JOIN gqa_acao d ON
                d.nr_seq_pend_regra = c.nr_sequencia AND
                d.ie_situacao = 'A'
            LEFT JOIN protocolo_int_pac_tp_event e ON
               e.nr_seq_protocolo = a.nr_seq_protocolo_int_pac AND
               e.cd_dominio_tp_evento = b.ie_tipo_evento
            LEFT JOIN protocolo_int_pac_categori f ON
                e.nr_sequencia = f.nr_seq_tp_evento AND
                d.nr_sequencia = f.nr_seq_acao
             WHERE
                a.nr_seq_protocolo_int_pac = nr_seq_protocolo_int_pac_p
            GROUP BY
                a.nr_sequencia,
                b.ie_tipo_evento,
                a.nr_seq_protocolo_int_pac,
                obter_descricao_dominio(8828, b.ie_tipo_evento),
                d.nr_sequencia,
                d.ds_acao,
                b.nr_seq_origem,
                b.nr_sequencia,
                b.ds_evento_plano || '_' || b.nr_dia_protocolo || '_' || b.nr_seq_ordem || '_' || Nvl(b.ie_ocultar_visao_paciente, 'N'),
                Get_encounter_type(nr_seq_protocolo_int_pac_p,b.dt_prevista),
                b.dt_prevista
            ORDER BY nr_ordem_tipo_evento, nr_ordem_plano, d.ds_acao;

            column_row_w column_row_t;

    BEGIN

        FOR column_data_r IN column_data_c LOOP

            column_row_w.nr_sequencia := column_data_r.nr_sequencia;
            column_row_w.ie_tipo_evento := column_data_r.ie_tipo_evento;
            column_row_w.ds_tipo := column_data_r.ds_tipo;
            column_row_w.nr_seq_acao := column_data_r.nr_seq_acao;
            column_row_w.ds_acao := column_data_r.ds_acao;
            column_row_w.nr_seq_protocolo_int_pac := column_data_r.nr_seq_protocolo_int_pac;
            column_row_w.nr_ordem_tipo_evento := column_data_r.nr_ordem_tipo_evento;
            column_row_w.nr_ordem_plano := column_data_r.nr_ordem_plano;
            column_row_w.nr_seq_origem := column_data_r.nr_seq_origem;
            column_row_w.nr_seq_evento := column_data_r.nr_seq_evento;
            column_row_w.column_id := column_data_r.column_id;
            column_row_w.ENCOUNTER_TYPE := column_data_r.ENCOUNTER_TYPE;            
            pipe row(column_row_w);

        END LOOP;
        RETURN;
    END get_patient_preview_column;

    FUNCTION get_preview_plan(nr_seq_protocolo_int_pac_p in protocolo_int_pac_etapa.nr_seq_protocolo_int_pac%TYPE, nr_seq_acao_p gqa_acao.nr_sequencia%TYPE, ie_tipo_evento_p in protocolo_int_pac_evento.ie_tipo_evento%TYPE, ie_nome_amigavel_p VARCHAR2 := 'N')
    RETURN plan_table_t PIPELINED
    IS

        CURSOR plan_c IS
            SELECT c.nr_sequencia NR_SEQ_EVENTO,
                   c.ie_tipo_evento IE_TIPO_EVENTO,
                   e.nr_sequencia NR_SEQ_ACAO,
                   Nvl(Decode(ie_nome_amigavel_p, 'S', e.ds_plano_amigavel, e.ds_plano), e.ds_plano) DS_PLANO,
                (SELECT MAX((select substr(p.nm_pessoa_fisica, 1,255) from pessoa_fisica p where p.cd_pessoa_fisica = u.cd_pessoa_fisica)) ds_usuario
                   FROM gqa_pendencia_pac f,
                        usuario u
                  WHERE f.nr_seq_protocolo_int_pac_even = c.nr_sequencia
                   AND f.nm_usuario = u.nm_usuario) DS_USUARIO,
                (SELECT Nvl(Decode(MAX(obter_informacoes_pendencia(h.nr_sequencia,'D')),null,'N','S'),'N')
                  FROM protocolo_int_pac_evento f,
                       gqa_pendencia_pac        g,
                       gqa_pend_pac_acao        h
                 WHERE f.nr_sequencia      = g.nr_seq_protocolo_int_pac_even
                   AND g.nr_sequencia      = h.nr_seq_pend_pac
                   AND f.nr_sequencia      = c.nr_sequencia
                   AND h.nr_seq_regra_acao = e.nr_sequencia) IE_HAS_EXECUTED,
                Decode(c.nr_seq_origem, null, 'N', 'S') IE_HAS_EXECUTED_EXTERNAL_CP,
                Nvl(ie_evento_manual, 'N') IE_HAS_VARIANCE_CANDIDATE,
                (SELECT Decode(Count(1),0,'N','S')
                  FROM protocolo_int_pac_variance
                 WHERE nr_seq_evento = c.nr_sequencia
                   AND nr_seq_acao   = e.nr_sequencia
                   AND ie_situacao   = 'A') IE_HAS_VARIANCE,
                 c.ds_evento_plano || '_' || c.nr_dia_protocolo || '_' || c.nr_seq_ordem || '_' || Nvl(c.ie_ocultar_visao_paciente, 'N') COLUMN_ID,
                 Decode(c.dt_real, null, 'N', 'S') IE_FINISHED
              FROM protocolo_int_pac_etapa    b,
                   protocolo_int_pac_evento   c,
                   gqa_pendencia_regra        d,
                   gqa_acao                   e
            WHERE b.nr_sequencia = c.nr_seq_prt_int_pac_etapa
              AND c.nr_seq_plano = d.nr_sequencia (+)
              AND d.nr_sequencia = e.nr_seq_pend_regra (+)
              AND (nr_seq_acao_p is null or d.ie_situacao  = 'A')
              AND (nr_seq_acao_p is null or e.ie_situacao  = 'A')
              AND c.ie_tipo_evento = ie_tipo_evento_p
              AND e.nr_sequencia (+) = nr_seq_acao_p
              AND b.nr_seq_protocolo_int_pac = nr_seq_protocolo_int_pac_p;

        plan_row_w plan_row_t;

    BEGIN

        FOR plan_c_w IN plan_c LOOP

            plan_row_w.nr_seq_evento := plan_c_w.nr_seq_evento;
            plan_row_w.ie_tipo_evento := plan_c_w.ie_tipo_evento;
            plan_row_w.nr_seq_acao := plan_c_w.nr_seq_acao;
            plan_row_w.ds_plano := plan_c_w.ds_plano;
            plan_row_w.ds_usuario := plan_c_w.ds_usuario;
            plan_row_w.ie_has_executed := plan_c_w.ie_has_executed;
            plan_row_w.ie_has_executed_external_cp := plan_c_w.ie_has_executed_external_cp;
            plan_row_w.ie_has_variance_candidate := plan_c_w.ie_has_variance_candidate;
            plan_row_w.ie_has_variance := plan_c_w.ie_has_variance;
            plan_row_w.ie_finished := plan_c_w.ie_finished;
            plan_row_w.column_id := plan_c_w.column_id;
            pipe row(plan_row_w);

        END LOOP;
        RETURN;

    END get_preview_plan;

    FUNCTION get_preview_patient_outcome(nr_seq_etapa_p NUMBER, ie_count_p VARCHAR2 := 'N', ie_nome_amigavel_p VARCHAR2 := 'N')
    RETURN VARCHAR2
    IS

        CURSOR c1 IS
            SELECT Nvl( Decode(ie_nome_amigavel_p, 'S', c.ds_resultado_paciente, c.ds_resultado), c.ds_resultado ) DS_RESULTADO
              FROM
                   protocolo_int_pac_etapa  b,
                   protocolo_int_pac_resul  c
            WHERE  b.nr_sequencia = c.nr_seq_etapa
              AND b.nr_sequencia = nr_seq_etapa_p
              AND c.ie_situacao = 'A'
              AND nvl(b.ie_evento_sumario,'N') = 'N'
            ORDER BY c.nr_ordem_apresentacao;

        outcomes_w VARCHAR(2000) := '';

    BEGIN

        FOR outcome IN c1 LOOP
            outcomes_w := outcomes_w || ' ' || outcome.ds_resultado || ',';
        END LOOP;
            outcomes_w := substr(outcomes_w, 0, Length(outcomes_w) - 1);
        RETURN outcomes_w;

    END get_preview_patient_outcome;

END clinical_pathway_preview_pck;
/
