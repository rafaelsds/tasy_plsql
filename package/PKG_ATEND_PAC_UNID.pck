CREATE OR REPLACE PACKAGE PKG_ATEND_PAC_UNID AS
  --
  FUNCTION GET_ESTAB_DT(P_ESTAB NUMBER, P_DT DATE) RETURN DATE;
  --
  FUNCTION GET_SYS_DT(P_ESTAB NUMBER, P_DT DATE) RETURN DATE;
  --
  FUNCTION GET_PATIENT_NAME(P_NR_ATEND NUMBER) RETURN VARCHAR2; 
  --
  FUNCTION GET_REFACTORY_PERSON_NAME(NM_PERSON_P VARCHAR2) RETURN VARCHAR2; 
  --
  FUNCTION GET_MIN_ENTRY_NEXT_UNIT(P_NR_ATEND NUMBER, 
                                   P_DT_ENTRY DATE,
                                   P_CD_SETOR NUMBER) RETURN DATE;
  --
  FUNCTION GET_MAX_EXIT_UNIT(P_NR_ATEND NUMBER, 
                             P_DT_ENTRY DATE,
                             P_CD_SETOR NUMBER) RETURN DATE;
  --
  FUNCTION GET_NEXT_UNIT(P_NR_ATEND NUMBER, 
                         P_DT_EXIT DATE,
                         P_CD_SETOR NUMBER) RETURN NUMBER;
  --
  FUNCTION GET_PREV_UNIT(P_NR_ATEND NUMBER, 
                         P_DT_ENTRY DATE,
                         P_CD_SETOR NUMBER) RETURN NUMBER;
  --
  FUNCTION GET_DT_DIGNOSIS_ADMIT(P_NR_ATEND NUMBER, 
                                 P_DT_ENTRY DATE) RETURN DATE;
  --
  FUNCTION GET_DIGNOSIS_ADMIT_DATA(P_NR_ATEND NUMBER, 
                                   P_DT_ENTRY DATE,
                                   P_TYPE VARCHAR2 DEFAULT 'D') RETURN VARCHAR2;
  --
  FUNCTION GET_ADMISSIONS_COUNT(P_START_DATE DATE,
                                P_END_DATE   DATE,
                                P_CD_ESTAB   NUMBER DEFAULT 0,
                                P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER;
  --
  FUNCTION GET_STAY_COUNT(P_START_DATE DATE,
                          P_END_DATE   DATE,
                          P_CD_ESTAB   NUMBER DEFAULT 0,
                          P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER;
  --
  FUNCTION GET_DEVICE_COUNT(P_START_DATE DATE,
                            P_END_DATE   DATE,
                            P_CLASS_DISP VARCHAR2,
                            P_CD_ESTAB   NUMBER DEFAULT 0,
                            P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER;
  --
  FUNCTION GET_STAY_AMOUNT(P_START_DATE DATE,
                           P_END_DATE   DATE,
                           P_CD_ESTAB   NUMBER DEFAULT 0,
                           P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER;
  --
  FUNCTION GET_STAY_DEVICE(P_START_DATE DATE,
                           P_END_DATE   DATE,
                           P_CLASS_DISP VARCHAR2,
                           P_CD_ESTAB   NUMBER DEFAULT 0,
                           P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER;
  --
  FUNCTION GET_STAY_PROTOCOL(P_START_DATE DATE,
                             P_END_DATE   DATE,
                             P_PROTOCOL   NUMBER,
                             P_CD_ESTAB   NUMBER DEFAULT 0,
                             P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER;
  --
  FUNCTION GET_MEDIAN_DEVICE(P_START_DATE DATE,
                             P_END_DATE   DATE,
                             P_CLASS_DISP VARCHAR2,
                             P_CD_ESTAB   NUMBER DEFAULT 0,
                             P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER;
  --
  FUNCTION GET_STAY_COUNT_AND_AMOUNT(P_START_DATE DATE,
                                     P_END_DATE   DATE,
                                     P_CD_ESTAB   NUMBER DEFAULT 0,
                                     P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN VARCHAR2;  
END PKG_ATEND_PAC_UNID;
/
CREATE OR REPLACE PACKAGE BODY PKG_ATEND_PAC_UNID AS
  --
  FUNCTION GET_ESTAB_TZ(P_ESTAB NUMBER, P_DT DATE DEFAULT SYSDATE)
    RETURN VARCHAR2 IS
  
    IE_UTC_V VARCHAR(15);
  
  BEGIN
  
    BEGIN
      SELECT IE_UTC
        INTO IE_UTC_V
        FROM ESTAB_HORARIO_VERAO
       WHERE CD_ESTABELECIMENTO = P_ESTAB
         AND P_DT BETWEEN DT_INICIAL AND DT_FINAL;
    EXCEPTION
      WHEN OTHERS THEN
        IE_UTC_V := NULL;
    END;
      
    IF IE_UTC_V IS NULL THEN
    --
      BEGIN
        SELECT NVL(IE_UTC, TO_CHAR(SYSTIMESTAMP, 'TZR'))
          INTO IE_UTC_V
          FROM ESTABELECIMENTO
         WHERE CD_ESTABELECIMENTO = P_ESTAB;
      EXCEPTION
        WHEN OTHERS THEN
          IE_UTC_V := TO_CHAR(SYSTIMESTAMP, 'TZR');
      END;
    --
    END IF;
  
    RETURN IE_UTC_V;
  
  END GET_ESTAB_TZ;
  --
  FUNCTION GET_ESTAB_DT(P_ESTAB NUMBER, P_DT DATE) RETURN DATE IS
  
    ESTAB_TZ_V VARCHAR2(15);
    SYS_TZ_V   VARCHAR2(15);
    DT_V       DATE;
    TZ_V       TIMESTAMP WITH TIME ZONE;
    CMD        VARCHAR2(255);
  
  BEGIN
  
    IF P_DT IS NULL THEN 
    --
      DT_V := NULL;
    --
    ELSIF P_ESTAB IS NULL THEN
      DT_V := P_DT;
    --
    ELSE
    --  
      ESTAB_TZ_V := GET_ESTAB_TZ(P_ESTAB, P_DT);
      SYS_TZ_V   := TO_CHAR(SYSTIMESTAMP, 'TZR');
    
      IF ESTAB_TZ_V = SYS_TZ_V THEN
      --
        DT_V := P_DT;
      --
      ELSE
      --
        BEGIN
          
          CMD := 'SELECT TO_TIMESTAMP_TZ(TO_CHAR(:1,''DD/MM/YYYY HH24:MI:SS'')||'''||SYS_TZ_V||''',''DD/MM/YYYY HH24:MI:SSTZH:TZM'') AT TIME ZONE '''|| ESTAB_TZ_V ||''' FROM DUAL'; 
          
          EXECUTE IMMEDIATE CMD INTO TZ_V USING P_DT;
          
          DT_V := TO_DATE(TO_CHAR(TZ_V, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS');
          
        EXCEPTION WHEN OTHERS THEN
          
          DT_V := P_DT;
          
        END;
      --
      END IF;
    --
    END IF;
      
    RETURN DT_V;
  
  END GET_ESTAB_DT;
  --
  FUNCTION GET_SYS_DT(P_ESTAB NUMBER, P_DT DATE) RETURN DATE IS
  
    ESTAB_TZ_V VARCHAR2(15);
    SYS_TZ_V   VARCHAR2(15);
    DT_V       DATE;
    TZ_V       TIMESTAMP WITH TIME ZONE;
    CMD        VARCHAR2(255);
  
  BEGIN
  
    IF P_DT IS NULL THEN 
    --
      DT_V := NULL;
    --
    ELSIF P_ESTAB IS NULL THEN
      DT_V := P_DT;
    --
    ELSE
    --  
      ESTAB_TZ_V := GET_ESTAB_TZ(P_ESTAB, P_DT);
      SYS_TZ_V   := TO_CHAR(SYSTIMESTAMP, 'TZR');
    
      IF ESTAB_TZ_V = SYS_TZ_V THEN
      --
        DT_V := P_DT;
      --
      ELSE
      --
        BEGIN
          
          CMD := 'SELECT TO_TIMESTAMP_TZ(TO_CHAR(:1,''DD/MM/YYYY HH24:MI:SS'')||'''||ESTAB_TZ_V||''',''DD/MM/YYYY HH24:MI:SSTZH:TZM'') AT TIME ZONE '''|| SYS_TZ_V ||''' FROM DUAL'; 
          
          EXECUTE IMMEDIATE CMD INTO TZ_V USING P_DT;
          
          DT_V := TO_DATE(TO_CHAR(TZ_V, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS');
          
        EXCEPTION WHEN OTHERS THEN
  
          DT_V := P_DT;
          
        END;
      --
      END IF;
    --
    END IF;
      
    RETURN DT_V;
  
  END GET_SYS_DT;
  --
  FUNCTION GET_PATIENT_NAME(P_NR_ATEND NUMBER) RETURN VARCHAR2 IS
  
    RET VARCHAR2(255) := NULL;
  
  BEGIN
  
    IF P_NR_ATEND IS NOT NULL THEN
      BEGIN
        SELECT GET_REFACTORY_PERSON_NAME(OBTER_NOME_PACIENTE(P_NR_ATEND))
          INTO RET
          FROM DUAL;
      EXCEPTION WHEN OTHERS THEN
        RET := NULL;
      END;
    END IF;
  
    RETURN RET;
  
  END GET_PATIENT_NAME;
  --
  FUNCTION GET_REFACTORY_PERSON_NAME(NM_PERSON_P VARCHAR2) RETURN VARCHAR2 IS

    RET VARCHAR2(255) := NULL;

  BEGIN

    IF NM_PERSON_P IS NOT NULL THEN
      SELECT DECODE(INSTR(T.NM, ',', 1),
                    0,
                    DECODE(INSTR(T.NM, ' ', -1),
                           0,
                           T.NM,
                           SUBSTR(T.NM, INSTR(T.NM, ' ', -1) + 1) || ', ' ||
                           SUBSTR(T.NM, 1, INSTR(T.NM, ' ', -1) - 1)),
                    T.NM)
        INTO RET
        FROM (SELECT TRIM(NM_PERSON_P) NM FROM DUAL) T;
    END IF;

    RETURN RET;
    
  END GET_REFACTORY_PERSON_NAME;
  --
  FUNCTION GET_MIN_ENTRY_NEXT_UNIT(P_NR_ATEND NUMBER, 
                                   P_DT_ENTRY DATE,
                                   P_CD_SETOR NUMBER) RETURN DATE IS
  DT DATE;

  BEGIN

    SELECT MIN(DT_ENTRADA_UNIDADE)
      INTO DT
      FROM ATEND_PACIENTE_UNIDADE U
     WHERE U.NR_ATENDIMENTO = P_NR_ATEND
       AND U.DT_ENTRADA_UNIDADE > P_DT_ENTRY
       AND U.CD_SETOR_ATENDIMENTO <> P_CD_SETOR
       AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
       AND NOT EXISTS 
               (SELECT 1 
                  FROM SETOR_ATENDIMENTO S
                 WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                   AND S.CD_CLASSIF_SETOR IN ('6', '7', '10'));
                   
     IF DT IS NOT NULL THEN
       DT := DT - 1/(24*60*60);
     END IF;
  
    RETURN DT;
  
  END GET_MIN_ENTRY_NEXT_UNIT;
  --
  FUNCTION GET_MAX_EXIT_UNIT(P_NR_ATEND NUMBER, 
                             P_DT_ENTRY DATE,
                             P_CD_SETOR NUMBER) RETURN DATE IS
  DT DATE;
  NR NUMBER;

  BEGIN
      
    DT := GET_MIN_ENTRY_NEXT_UNIT(P_NR_ATEND, P_DT_ENTRY, P_CD_SETOR);
    
    IF DT IS NULL THEN
      SELECT MAX(DT_SAIDA_UNIDADE)
        INTO DT
        FROM ATEND_PACIENTE_UNIDADE U
       WHERE U.NR_ATENDIMENTO = P_NR_ATEND
         AND U.DT_SAIDA_UNIDADE >= P_DT_ENTRY
         AND U.CD_SETOR_ATENDIMENTO = P_CD_SETOR
         AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
         AND NOT EXISTS 
                 (SELECT 1 
                    FROM SETOR_ATENDIMENTO S
                   WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                     AND S.CD_CLASSIF_SETOR IN ('6', '7', '10'));    
      
      IF DT IS NOT NULL THEN
        SELECT COUNT(1)
          INTO NR
          FROM ATEND_PACIENTE_UNIDADE U
         WHERE U.NR_ATENDIMENTO = P_NR_ATEND
           AND U.DT_ENTRADA_UNIDADE = DT
           AND U.DT_SAIDA_UNIDADE IS NULL
           AND U.CD_SETOR_ATENDIMENTO = P_CD_SETOR
           AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
           AND NOT EXISTS 
                   (SELECT 1 
                      FROM SETOR_ATENDIMENTO S
                     WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                       AND S.CD_CLASSIF_SETOR IN ('6', '7', '10'));
        IF NR > 0 THEN
          DT := NULL;
        END IF;    
      END IF;
    END IF;
  
    RETURN DT;
  
  END GET_MAX_EXIT_UNIT;
  --
  FUNCTION GET_NEXT_UNIT(P_NR_ATEND NUMBER, 
                         P_DT_EXIT  DATE,
                         P_CD_SETOR NUMBER) RETURN NUMBER IS

    CD NUMBER(5);
    DT DATE;
  
  BEGIN
    CD := NULL;
    
    IF P_NR_ATEND IS NOT NULL AND P_DT_EXIT IS NOT NULL AND P_CD_SETOR IS NOT NULL THEN
      --
      DT := GET_MIN_ENTRY_NEXT_UNIT(P_NR_ATEND, P_DT_EXIT, P_CD_SETOR);
      
      IF DT IS NOT NULL THEN
        DT := DT + 1/(24*60*60);
        --
        SELECT U.CD_SETOR_ATENDIMENTO
          INTO CD
          FROM ATEND_PACIENTE_UNIDADE U
         WHERE U.NR_ATENDIMENTO = P_NR_ATEND
           AND U.DT_ENTRADA_UNIDADE = DT;
        --
      END IF;
      --
    END IF;
    
    RETURN CD;
    
  END GET_NEXT_UNIT;                         
  --
  FUNCTION GET_PREV_UNIT(P_NR_ATEND NUMBER, 
                         P_DT_ENTRY DATE,
                         P_CD_SETOR NUMBER) RETURN NUMBER IS
                         
  CD NUMBER(5);
  DT DATE;
  
  BEGIN
  
    CD := NULL;
    
    IF P_NR_ATEND IS NOT NULL AND P_DT_ENTRY IS NOT NULL AND P_CD_SETOR IS NOT NULL THEN
      --
      SELECT MAX(DT_ENTRADA_UNIDADE)
        INTO DT
        FROM ATEND_PACIENTE_UNIDADE U
       WHERE U.NR_ATENDIMENTO = P_NR_ATEND
         AND U.DT_ENTRADA_UNIDADE < P_DT_ENTRY
         AND U.CD_SETOR_ATENDIMENTO <> P_CD_SETOR
         AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
         AND NOT EXISTS 
                 (SELECT 1 
                    FROM SETOR_ATENDIMENTO S
                   WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                     AND S.CD_CLASSIF_SETOR IN ('6', '7', '10'));
      
      IF DT IS NOT NULL THEN
        --
        SELECT U.CD_SETOR_ATENDIMENTO
          INTO CD
          FROM ATEND_PACIENTE_UNIDADE U
         WHERE U.NR_ATENDIMENTO = P_NR_ATEND
           AND U.DT_ENTRADA_UNIDADE = DT;        
        --
      END IF;                 
      --
    END IF;
    
    RETURN CD;
    
  END GET_PREV_UNIT;
  --  
  FUNCTION GET_DT_DIGNOSIS_ADMIT(P_NR_ATEND NUMBER, 
                                 P_DT_ENTRY DATE) RETURN DATE IS
    DT DATE;
  
  BEGIN
    
    DT := NULL;
    
    IF NVL(P_NR_ATEND, 0) <> 0 AND P_DT_ENTRY IS NOT NULL THEN
      
      BEGIN
        SELECT MAX(D.DT_DIAGNOSTICO)
          INTO DT
          FROM DIAGNOSTICO_DOENCA D
         WHERE D.NR_ATENDIMENTO = P_NR_ATEND
           AND D.DT_DIAGNOSTICO <= P_DT_ENTRY
           AND D.IE_SITUACAO = 'A'
           AND D.IE_TIPO_DIAGNOSTICO = 2;
      EXCEPTION WHEN OTHERS THEN
        NULL;
      END;
    
    END IF;    
    
    RETURN DT;
    
  END GET_DT_DIGNOSIS_ADMIT;
  --
  FUNCTION GET_DIGNOSIS_ADMIT_DATA(P_NR_ATEND NUMBER, 
                                   P_DT_ENTRY DATE,
                                   P_TYPE VARCHAR2 DEFAULT 'D') RETURN VARCHAR2 IS
    DT  DATE;
    NR  NUMBER;
    RET VARCHAR2(255);
  
  BEGIN
    
    NR := NULL;
    RET := NULL;
      
    IF NVL(P_NR_ATEND, 0) <> 0 AND P_DT_ENTRY IS NOT NULL THEN
      --
      DT := GET_DT_DIGNOSIS_ADMIT(P_NR_ATEND, P_DT_ENTRY); 
      
      IF DT IS NOT NULL THEN
        --
        SELECT MAX(D.NR_SEQ_INTERNO)
          INTO NR
          FROM DIAGNOSTICO_DOENCA D
         WHERE D.NR_ATENDIMENTO = P_NR_ATEND
           AND D.DT_DIAGNOSTICO = DT
           AND D.IE_SITUACAO = 'A'
           AND D.IE_TIPO_DIAGNOSTICO = 2;
        --
        IF NR IS NOT NULL THEN
          --
          IF NVL(P_TYPE, 'D') = 'D' THEN
            --
            SELECT MAX(D.CD_DOENCA)
              INTO RET
              FROM DIAGNOSTICO_DOENCA D
             WHERE D.NR_SEQ_INTERNO = NR;
            --
          ELSE
            --
            SELECT MAX(D.NR_CIRURGIA)
              INTO NR
              FROM DIAGNOSTICO_DOENCA D
             WHERE D.NR_SEQ_INTERNO = NR;
            --
            IF NR IS NOT NULL THEN
              --
              IF NVL(P_TYPE, 'D') = 'E' THEN
                --
                SELECT DECODE(MAX(C.IE_CARATER_CIRURGIA), 'E', 'Y', 'N')
                  INTO RET
                  FROM CIRURGIA C
                 WHERE C.NR_CIRURGIA = NR;
                --
              ELSE
                --
                RET := 'Y';
                --
              END IF;
              --
            END IF;
            --
          END IF;
          --
        END IF;
        --
      END IF;
      --
    END IF;    
    --
    RETURN RET;
    
  END GET_DIGNOSIS_ADMIT_DATA;
  --
  FUNCTION GET_ADMISSIONS_COUNT(P_START_DATE DATE,
                                P_END_DATE   DATE,
                                P_CD_ESTAB   NUMBER DEFAULT 0,
                                P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER IS
    
  NR NUMBER;
  
  BEGIN

    IF P_START_DATE IS NOT NULL AND 
       P_END_DATE IS NOT NULL AND 
       P_START_DATE <= P_END_DATE THEN

      BEGIN
      
        SELECT COUNT(1)
          INTO NR
          FROM (SELECT U.NR_ATENDIMENTO,
                       MIN(U.DT_ENTRADA_UNIDADE),
                       S.CD_ESTABELECIMENTO,
                       U.CD_SETOR_ATENDIMENTO,
                       PKG_ATEND_PAC_UNID.GET_MIN_ENTRY_NEXT_UNIT(U.NR_ATENDIMENTO,
                                                                  U.DT_ENTRADA_UNIDADE,
                                                                  U.CD_SETOR_ATENDIMENTO)
                  FROM ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                 WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                   AND DECODE(NVL(P_CD_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_CD_ESTAB, 0)
                   AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                   AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                   AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                   AND U.DT_ENTRADA_UNIDADE BETWEEN
                       PKG_ATEND_PAC_UNID.GET_SYS_DT(DECODE(NVL(P_CD_ESTAB, 0), 0, S.CD_ESTABELECIMENTO, P_CD_ESTAB), P_START_DATE) AND
                       PKG_ATEND_PAC_UNID.GET_SYS_DT(DECODE(NVL(P_CD_ESTAB, 0), 0, S.CD_ESTABELECIMENTO, P_CD_ESTAB), LEAST(FIM_DIA(P_END_DATE), SYSDATE))
                 GROUP BY U.NR_ATENDIMENTO,
                          S.CD_ESTABELECIMENTO,
                          U.CD_SETOR_ATENDIMENTO,
                          PKG_ATEND_PAC_UNID.GET_MIN_ENTRY_NEXT_UNIT(U.NR_ATENDIMENTO,
                                                                     U.DT_ENTRADA_UNIDADE,
                                                                     U.CD_SETOR_ATENDIMENTO));

      EXCEPTION WHEN OTHERS THEN
        NR := 0;
       END;
    
    END IF;

    RETURN NVL(NR, 0);  
  
  END GET_ADMISSIONS_COUNT;
  --
  FUNCTION GET_STAY_COUNT(P_START_DATE DATE,
                          P_END_DATE   DATE,
                          P_CD_ESTAB   NUMBER DEFAULT 0,
                          P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER IS
    
    V_ESTAB  NUMBER;
    NR       NUMBER;
   
  BEGIN
  
    IF P_START_DATE IS NOT NULL AND 
       P_END_DATE IS NOT NULL AND 
       P_START_DATE <= P_END_DATE THEN
  
      IF NVL(P_CD_SETOR, 0) = 0 THEN
        V_ESTAB := NVL(P_CD_ESTAB, 0);
      ELSE
        V_ESTAB := NVL(obter_estabelecimento_setor(P_CD_SETOR), 0);
      END IF;
      
      IF V_ESTAB <> 0 THEN
        BEGIN
          DECLARE
            V_DT_INI DATE;
            V_DT_FIM DATE;          
          
          BEGIN
            V_DT_INI := PKG_ATEND_PAC_UNID.GET_SYS_DT(V_ESTAB, P_START_DATE);
            V_DT_FIM := PKG_ATEND_PAC_UNID.GET_SYS_DT(V_ESTAB, LEAST(P_END_DATE + 1, SYSDATE));

            SELECT COUNT(1)
              INTO NR
              FROM (SELECT /*U.NR_ATENDIMENTO,
                           S.CD_ESTABELECIMENTO,
                           U.CD_SETOR_ATENDIMENTO,*/
                           GREATEST(MIN(U.DT_ENTRADA_UNIDADE), V_DT_INI)  DT_ENTRADA,
                           LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                          U.DT_ENTRADA_UNIDADE,
                                                                          U.CD_SETOR_ATENDIMENTO),
                                     V_DT_FIM),
                                 V_DT_FIM) DT_SAIDA
                      FROM ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                     WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                       AND S.CD_ESTABELECIMENTO = V_ESTAB
                       AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                       AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                       AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                       AND U.DT_ENTRADA_UNIDADE < V_DT_FIM 
                       AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                    U.DT_ENTRADA_UNIDADE,
                                                                    U.CD_SETOR_ATENDIMENTO),
                               V_DT_INI) >= V_DT_INI
                     GROUP BY U.NR_ATENDIMENTO,
                              S.CD_ESTABELECIMENTO,
                              U.CD_SETOR_ATENDIMENTO,
                              PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                   U.DT_ENTRADA_UNIDADE,
                                                                   U.CD_SETOR_ATENDIMENTO)) T;

          EXCEPTION WHEN OTHERS THEN
            NR := 0;            
          END;
        END;

      ELSE
        
        BEGIN
        
          SELECT COUNT(1)
            INTO NR
            FROM (SELECT /*U.NR_ATENDIMENTO,
                         S.CD_ESTABELECIMENTO,
                         U.CD_SETOR_ATENDIMENTO,*/
                         GREATEST(MIN(U.DT_ENTRADA_UNIDADE),
                                  PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) DT_ENTRADA,
                         LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                              U.DT_ENTRADA_UNIDADE,
                                                              U.CD_SETOR_ATENDIMENTO),
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))),
                               PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))) DT_SAIDA                                              
                    FROM ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                   WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                     AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                     AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                     AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                     AND U.DT_ENTRADA_UNIDADE < PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))
                     AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                  U.DT_ENTRADA_UNIDADE,
                                                                  U.CD_SETOR_ATENDIMENTO),
                             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                   GROUP BY U.NR_ATENDIMENTO,
                            S.CD_ESTABELECIMENTO,
                            U.CD_SETOR_ATENDIMENTO,
                            PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                 U.DT_ENTRADA_UNIDADE,
                                                                 U.CD_SETOR_ATENDIMENTO)) T;

        EXCEPTION WHEN OTHERS THEN
          NR := 0;
        END;
               
      END IF;
     
    END IF;

    RETURN NVL(NR, 0);  
  
  END GET_STAY_COUNT;
  --
  FUNCTION GET_DEVICE_COUNT(P_START_DATE DATE,
                            P_END_DATE   DATE,
                            P_CLASS_DISP VARCHAR2,
                            P_CD_ESTAB   NUMBER DEFAULT 0,
                            P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER IS
    
    NR     NUMBER;
   
  BEGIN
  
    IF P_START_DATE IS NOT NULL AND 
       P_END_DATE IS NOT NULL AND 
       TRIM(P_CLASS_DISP) IS NOT NULL AND 
       P_START_DATE <= P_END_DATE THEN
  
      BEGIN
      
        SELECT COUNT(DISTINCT NR_ATENDIMENTO || '-' || CD_SETOR_ATENDIMENTO)
          INTO NR
          FROM (SELECT UN.NR_ATENDIMENTO,
                       UN.CD_ESTABELECIMENTO,
                       UN.CD_SETOR_ATENDIMENTO,
                       /*AD.NR_SEQUENCIA,*/
                       GREATEST(UN.DT_ENTRADA, AD.DT_INSTALACAO) DT_ENTRADA,
                       LEAST(UN.DT_SAIDA, NVL(AD.DT_RETIRADA, UN.DT_SAIDA)) DT_SAIDA
                  FROM (SELECT U.NR_ATENDIMENTO,
                               S.CD_ESTABELECIMENTO,
                               U.CD_SETOR_ATENDIMENTO,
                               GREATEST(MIN(U.DT_ENTRADA_UNIDADE),
                                        PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) DT_ENTRADA,
                               LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                    U.DT_ENTRADA_UNIDADE,
                                                                    U.CD_SETOR_ATENDIMENTO),
                                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))),
                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))) DT_SAIDA                                              
                          FROM (SELECT DISTINCT A.NR_ATENDIMENTO 
                                  FROM ATEND_PAC_DISPOSITIVO A,
                                       ATENDIMENTO_PACIENTE P 
                                 WHERE A.NR_ATENDIMENTO = P.NR_ATENDIMENTO   
                                   AND A.DT_INSTALACAO < PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))
                                   AND (A.DT_RETIRADA IS NULL OR
                                        A.DT_RETIRADA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, P_START_DATE))
                                   AND EXISTS 
                                       (SELECT 1
                                          FROM DISPOSITIVO D
                                         WHERE D.NR_SEQUENCIA = A.NR_SEQ_DISPOSITIVO
                                           AND D.IE_CLASSIF_DISP_NISS = P_CLASS_DISP)) AP, ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                         WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                           AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                           AND DECODE(NVL(P_CD_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_CD_ESTAB, 0)
                           AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                           AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                           AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                           AND U.DT_ENTRADA_UNIDADE < PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))         
                           AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                        U.DT_ENTRADA_UNIDADE,
                                                                        U.CD_SETOR_ATENDIMENTO),
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
                               PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                         GROUP BY U.NR_ATENDIMENTO,
                                  S.CD_ESTABELECIMENTO,
                                  U.CD_SETOR_ATENDIMENTO,
                                  PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                       U.DT_ENTRADA_UNIDADE,
                                                                       U.CD_SETOR_ATENDIMENTO)) UN,
                       ATEND_PAC_DISPOSITIVO AD
                 WHERE AD.NR_ATENDIMENTO = UN.NR_ATENDIMENTO
                   AND EXISTS 
                       (SELECT 1
                          FROM DISPOSITIVO D
                         WHERE D.NR_SEQUENCIA = AD.NR_SEQ_DISPOSITIVO
                           AND D.IE_CLASSIF_DISP_NISS = P_CLASS_DISP)
                   AND AD.DT_INSTALACAO < UN.DT_SAIDA
                   AND NVL(AD.DT_RETIRADA, UN.DT_ENTRADA) >= UN.DT_ENTRADA);
      
      EXCEPTION WHEN OTHERS THEN
        NR := 0;
      END;
    
    END IF;
      
    RETURN NVL(NR, 0);  
  
  END GET_DEVICE_COUNT;
  --
  FUNCTION GET_STAY_AMOUNT(P_START_DATE DATE,
                           P_END_DATE   DATE,
                           P_CD_ESTAB   NUMBER DEFAULT 0,
                           P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER IS
    
    V_ESTAB  NUMBER;
    NR       NUMBER;
   
  BEGIN
  
    IF P_START_DATE IS NOT NULL AND 
       P_END_DATE IS NOT NULL AND 
       P_START_DATE <= P_END_DATE THEN
  
      IF NVL(P_CD_SETOR, 0) = 0 THEN
        V_ESTAB := NVL(P_CD_ESTAB, 0);
      ELSE
        V_ESTAB := NVL(obter_estabelecimento_setor(P_CD_SETOR), 0);
      END IF;
      
      IF V_ESTAB <> 0 THEN
        BEGIN
          DECLARE
            V_DT_INI DATE;
            V_DT_FIM DATE;          
          
          BEGIN
            V_DT_INI := PKG_ATEND_PAC_UNID.GET_SYS_DT(V_ESTAB, P_START_DATE);
            V_DT_FIM := PKG_ATEND_PAC_UNID.GET_SYS_DT(V_ESTAB, LEAST(P_END_DATE + 1, SYSDATE));

            SELECT SUM(DT_SAIDA - DT_ENTRADA)
              INTO NR
              FROM (SELECT /*U.NR_ATENDIMENTO,
                           S.CD_ESTABELECIMENTO,
                           U.CD_SETOR_ATENDIMENTO,*/
                           GREATEST(MIN(U.DT_ENTRADA_UNIDADE), V_DT_INI)  DT_ENTRADA,
                           LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                          U.DT_ENTRADA_UNIDADE,
                                                                          U.CD_SETOR_ATENDIMENTO),
                                     V_DT_FIM),
                                 V_DT_FIM) DT_SAIDA
                      FROM ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                     WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                       AND S.CD_ESTABELECIMENTO = V_ESTAB
                       AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                       AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                       AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                       AND U.DT_ENTRADA_UNIDADE < V_DT_FIM 
                       AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                    U.DT_ENTRADA_UNIDADE,
                                                                    U.CD_SETOR_ATENDIMENTO),
                               V_DT_INI) >= V_DT_INI
                     GROUP BY U.NR_ATENDIMENTO,
                              S.CD_ESTABELECIMENTO,
                              U.CD_SETOR_ATENDIMENTO,
                              PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                   U.DT_ENTRADA_UNIDADE,
                                                                   U.CD_SETOR_ATENDIMENTO)) T;

          EXCEPTION WHEN OTHERS THEN
            NR := 0;            
          END;
        END;

      ELSE
        
        BEGIN
        
          SELECT SUM(DT_SAIDA - DT_ENTRADA)
            INTO NR
            FROM (SELECT /*U.NR_ATENDIMENTO,
                         S.CD_ESTABELECIMENTO,
                         U.CD_SETOR_ATENDIMENTO,*/
                         GREATEST(MIN(U.DT_ENTRADA_UNIDADE),
                                  PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) DT_ENTRADA,
                         LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                              U.DT_ENTRADA_UNIDADE,
                                                              U.CD_SETOR_ATENDIMENTO),
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))),
                               PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))) DT_SAIDA                                              
                    FROM ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                   WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                     AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                     AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                     AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                     AND U.DT_ENTRADA_UNIDADE < PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))
                     AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                  U.DT_ENTRADA_UNIDADE,
                                                                  U.CD_SETOR_ATENDIMENTO),
                             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                   GROUP BY U.NR_ATENDIMENTO,
                            S.CD_ESTABELECIMENTO,
                            U.CD_SETOR_ATENDIMENTO,
                            PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                 U.DT_ENTRADA_UNIDADE,
                                                                 U.CD_SETOR_ATENDIMENTO)) T;

        EXCEPTION WHEN OTHERS THEN
          NR := 0;
        END;
               
      END IF;
     
    END IF;

    RETURN NVL(NR, 0);  
  
  END GET_STAY_AMOUNT;
  --
  FUNCTION GET_STAY_DEVICE(P_START_DATE DATE,
                           P_END_DATE   DATE,
                           P_CLASS_DISP VARCHAR2,
                           P_CD_ESTAB   NUMBER DEFAULT 0,
                           P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER IS
    
    NR     NUMBER;
   
  BEGIN
  
    IF P_START_DATE IS NOT NULL AND 
       P_END_DATE IS NOT NULL AND 
       TRIM(P_CLASS_DISP) IS NOT NULL AND 
       P_START_DATE <= P_END_DATE THEN
  
      BEGIN
      
        SELECT SUM(DT_SAIDA - DT_ENTRADA)
          INTO NR
          FROM (SELECT /*UN.NR_ATENDIMENTO,
                       UN.CD_ESTABELECIMENTO,
                       UN.CD_SETOR_ATENDIMENTO,
                       AD.NR_SEQUENCIA,*/
                       GREATEST(UN.DT_ENTRADA, AD.DT_INSTALACAO) DT_ENTRADA,
                       LEAST(UN.DT_SAIDA, NVL(AD.DT_RETIRADA, UN.DT_SAIDA)) DT_SAIDA
                  FROM (SELECT U.NR_ATENDIMENTO,
                               S.CD_ESTABELECIMENTO,
                               U.CD_SETOR_ATENDIMENTO,
                               GREATEST(MIN(U.DT_ENTRADA_UNIDADE),
                                        PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) DT_ENTRADA,
                               LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                    U.DT_ENTRADA_UNIDADE,
                                                                    U.CD_SETOR_ATENDIMENTO),
                                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))),
                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))) DT_SAIDA                                              
                          FROM (SELECT DISTINCT A.NR_ATENDIMENTO 
                                  FROM ATEND_PAC_DISPOSITIVO A,
                                       ATENDIMENTO_PACIENTE P 
                                 WHERE A.NR_ATENDIMENTO = P.NR_ATENDIMENTO   
                                   AND A.DT_INSTALACAO < PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))
                                   AND (A.DT_RETIRADA IS NULL OR
                                        A.DT_RETIRADA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, P_START_DATE))
                                   AND EXISTS 
                                       (SELECT 1
                                          FROM DISPOSITIVO D
                                         WHERE D.NR_SEQUENCIA = A.NR_SEQ_DISPOSITIVO
                                           AND D.IE_CLASSIF_DISP_NISS = P_CLASS_DISP)) AP, ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                         WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                           AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                           AND DECODE(NVL(P_CD_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_CD_ESTAB, 0)
                           AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                           AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                           AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                           AND U.DT_ENTRADA_UNIDADE < PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))         
                           AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                        U.DT_ENTRADA_UNIDADE,
                                                                        U.CD_SETOR_ATENDIMENTO),
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
                               PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                         GROUP BY U.NR_ATENDIMENTO,
                                  S.CD_ESTABELECIMENTO,
                                  U.CD_SETOR_ATENDIMENTO,
                                  PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                       U.DT_ENTRADA_UNIDADE,
                                                                       U.CD_SETOR_ATENDIMENTO)) UN,
                       ATEND_PAC_DISPOSITIVO AD
                 WHERE AD.NR_ATENDIMENTO = UN.NR_ATENDIMENTO
                   AND EXISTS 
                       (SELECT 1
                          FROM DISPOSITIVO D
                         WHERE D.NR_SEQUENCIA = AD.NR_SEQ_DISPOSITIVO
                           AND D.IE_CLASSIF_DISP_NISS = P_CLASS_DISP)
                   AND AD.DT_INSTALACAO < UN.DT_SAIDA
                   AND NVL(AD.DT_RETIRADA, UN.DT_ENTRADA) >= UN.DT_ENTRADA);
      
      EXCEPTION WHEN OTHERS THEN
        NR := 0;
      END;
    
    END IF;
      
    RETURN NVL(NR, 0);  
  
  END GET_STAY_DEVICE;
  --
  FUNCTION GET_STAY_PROTOCOL(P_START_DATE DATE,
                             P_END_DATE   DATE,
                             P_PROTOCOL   NUMBER,
                             P_CD_ESTAB   NUMBER DEFAULT 0,
                             P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER IS
    
    NR     NUMBER;
   
  BEGIN
  
    IF P_START_DATE IS NOT NULL AND 
       P_END_DATE IS NOT NULL AND 
       P_PROTOCOL IS NOT NULL AND 
       P_START_DATE <= P_END_DATE THEN
    
      BEGIN
      
        SELECT SUM(DT_SAIDA - DT_ENTRADA)
          INTO NR
          FROM (SELECT /*UN.NR_ATENDIMENTO,
                       UN.CD_ESTABELECIMENTO,
                       UN.CD_SETOR_ATENDIMENTO,
                       GPP.NR_SEQUENCIA,*/
                       GREATEST(UN.DT_ENTRADA, GPP.DT_INCLUSAO) DT_ENTRADA,
                       LEAST(UN.DT_SAIDA, NVL(GPP.DT_TERMINO, UN.DT_SAIDA)) DT_SAIDA
                  FROM (SELECT U.NR_ATENDIMENTO,
                               S.CD_ESTABELECIMENTO,
                               U.CD_SETOR_ATENDIMENTO,
                               GREATEST(MIN(U.DT_ENTRADA_UNIDADE),
                                        PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) DT_ENTRADA,
                               LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                    U.DT_ENTRADA_UNIDADE,
                                                                    U.CD_SETOR_ATENDIMENTO),
                                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))),
                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))) DT_SAIDA                                              
                          FROM (SELECT DISTINCT P.NR_ATENDIMENTO
                                  FROM GQA_PROTOCOLO_PAC P,
                                       ATENDIMENTO_PACIENTE A
                                 WHERE P.NR_ATENDIMENTO = A.NR_ATENDIMENTO
                                   AND P.NR_SEQ_PROTOCOLO = P_PROTOCOL
                                   AND P.IE_SITUACAO = 'A'
                                   AND P.DT_INCLUSAO < PKG_ATEND_PAC_UNID.GET_SYS_DT(A.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))
                                   AND (P.DT_TERMINO IS NULL OR
                                        P.DT_TERMINO >= PKG_ATEND_PAC_UNID.GET_SYS_DT(A.CD_ESTABELECIMENTO, P_START_DATE))) AP, ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                         WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                           AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                           AND DECODE(NVL(P_CD_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_CD_ESTAB, 0)
                           AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                           AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                           AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                           AND U.DT_ENTRADA_UNIDADE < PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))         
                           AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                        U.DT_ENTRADA_UNIDADE,
                                                                        U.CD_SETOR_ATENDIMENTO),
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
                               PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                         GROUP BY U.NR_ATENDIMENTO,
                                  S.CD_ESTABELECIMENTO,
                                  U.CD_SETOR_ATENDIMENTO,
                                  PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                       U.DT_ENTRADA_UNIDADE,
                                                                       U.CD_SETOR_ATENDIMENTO)) UN,
                       GQA_PROTOCOLO_PAC GPP
                 WHERE GPP.NR_ATENDIMENTO = UN.NR_ATENDIMENTO
                   AND GPP.NR_SEQ_PROTOCOLO = P_PROTOCOL
                   AND GPP.IE_SITUACAO = 'A'
                   AND GPP.DT_INCLUSAO < UN.DT_SAIDA
                   AND NVL(GPP.DT_TERMINO, UN.DT_ENTRADA) >= UN.DT_ENTRADA);
      
      EXCEPTION WHEN OTHERS THEN
        NR := 0;
      END;
    
    END IF;
      
    RETURN NVL(NR, 0);  
  
  END GET_STAY_PROTOCOL;
  --
  FUNCTION GET_MEDIAN_DEVICE(P_START_DATE DATE,
                             P_END_DATE   DATE,
                             P_CLASS_DISP VARCHAR2,
                             P_CD_ESTAB   NUMBER DEFAULT 0,
                             P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN NUMBER IS
    
    NR     NUMBER;
   
  BEGIN
  
    IF P_START_DATE IS NOT NULL AND 
       P_END_DATE IS NOT NULL AND 
       TRIM(P_CLASS_DISP) IS NOT NULL AND 
       P_START_DATE <= P_END_DATE THEN
  
      BEGIN
        
        SELECT MEDIAN(SOMA)
          INTO NR
          FROM (SELECT NR_ATENDIMENTO,
                       CD_ESTABELECIMENTO,
                       CD_SETOR_ATENDIMENTO,
                       SUM(DT_SAIDA - DT_ENTRADA) SOMA          
                  FROM (SELECT UN.NR_ATENDIMENTO,
                               UN.CD_ESTABELECIMENTO,
                               UN.CD_SETOR_ATENDIMENTO,
                               AD.NR_SEQUENCIA,
                               GREATEST(UN.DT_ENTRADA, AD.DT_INSTALACAO) DT_ENTRADA,
                               LEAST(UN.DT_SAIDA, NVL(AD.DT_RETIRADA, UN.DT_SAIDA)) DT_SAIDA
                          FROM (SELECT U.NR_ATENDIMENTO,
                                       S.CD_ESTABELECIMENTO,
                                       U.CD_SETOR_ATENDIMENTO,
                                       GREATEST(MIN(U.DT_ENTRADA_UNIDADE),
                                                PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) DT_ENTRADA,
                                       LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                            U.DT_ENTRADA_UNIDADE,
                                                                            U.CD_SETOR_ATENDIMENTO),
                                                 PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))),
                                             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))) DT_SAIDA                                              
                                  FROM (SELECT DISTINCT A.NR_ATENDIMENTO 
                                          FROM ATEND_PAC_DISPOSITIVO A,
                                               ATENDIMENTO_PACIENTE P 
                                         WHERE A.NR_ATENDIMENTO = P.NR_ATENDIMENTO   
                                           AND A.DT_INSTALACAO < PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))
                                           AND (A.DT_RETIRADA IS NULL OR
                                                A.DT_RETIRADA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, P_START_DATE))
                                           AND EXISTS 
                                               (SELECT 1
                                                  FROM DISPOSITIVO D
                                                 WHERE D.NR_SEQUENCIA = A.NR_SEQ_DISPOSITIVO
                                                   AND D.IE_CLASSIF_DISP_NISS = P_CLASS_DISP)) AP, ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                                 WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                                   AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                                   AND DECODE(NVL(P_CD_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_CD_ESTAB, 0)
                                   AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                                   AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                                   AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                                   AND U.DT_ENTRADA_UNIDADE < PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))         
                                   AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                U.DT_ENTRADA_UNIDADE,
                                                                                U.CD_SETOR_ATENDIMENTO),
                                           PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
                                       PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                                 GROUP BY U.NR_ATENDIMENTO,
                                          S.CD_ESTABELECIMENTO,
                                          U.CD_SETOR_ATENDIMENTO,
                                          PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                               U.DT_ENTRADA_UNIDADE,
                                                                               U.CD_SETOR_ATENDIMENTO)) UN,
                               ATEND_PAC_DISPOSITIVO AD
                         WHERE AD.NR_ATENDIMENTO = UN.NR_ATENDIMENTO
                           AND EXISTS 
                               (SELECT 1
                                  FROM DISPOSITIVO D
                                 WHERE D.NR_SEQUENCIA = AD.NR_SEQ_DISPOSITIVO
                                   AND D.IE_CLASSIF_DISP_NISS = P_CLASS_DISP)
                           AND AD.DT_INSTALACAO < UN.DT_SAIDA
                           AND NVL(AD.DT_RETIRADA, UN.DT_ENTRADA) >= UN.DT_ENTRADA) 
                 ORDER BY 4);
              
      EXCEPTION WHEN OTHERS THEN
        NR := 0;
      END;
    
    END IF;
      
    RETURN NVL(NR, 0);  
  
  END GET_MEDIAN_DEVICE;
  --
  FUNCTION GET_STAY_COUNT_AND_AMOUNT(P_START_DATE DATE,
                                     P_END_DATE   DATE,
                                     P_CD_ESTAB   NUMBER DEFAULT 0,
                                     P_CD_SETOR   NUMBER DEFAULT 0)
    RETURN VARCHAR2 IS
    
    V_ESTAB  NUMBER;
    NR       VARCHAR2(255);
   
  BEGIN
  
    IF P_START_DATE IS NOT NULL AND 
       P_END_DATE IS NOT NULL AND 
       P_START_DATE <= P_END_DATE THEN
  
      IF NVL(P_CD_SETOR, 0) = 0 THEN
        V_ESTAB := NVL(P_CD_ESTAB, 0);
      ELSE
        V_ESTAB := NVL(obter_estabelecimento_setor(P_CD_SETOR), 0);
      END IF;
      
      IF V_ESTAB <> 0 THEN
        BEGIN
          DECLARE
            V_DT_INI DATE;
            V_DT_FIM DATE;          
          
          BEGIN
            V_DT_INI := PKG_ATEND_PAC_UNID.GET_SYS_DT(V_ESTAB, P_START_DATE);
            V_DT_FIM := PKG_ATEND_PAC_UNID.GET_SYS_DT(V_ESTAB, LEAST(P_END_DATE + 1, SYSDATE));

            SELECT COUNT(1)||';'||ROUND(NVL(SUM(DT_SAIDA - DT_ENTRADA), 0), 8)
              INTO NR
              FROM (SELECT /*U.NR_ATENDIMENTO,
                           S.CD_ESTABELECIMENTO,
                           U.CD_SETOR_ATENDIMENTO,*/
                           GREATEST(MIN(U.DT_ENTRADA_UNIDADE), V_DT_INI)  DT_ENTRADA,
                           LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                          U.DT_ENTRADA_UNIDADE,
                                                                          U.CD_SETOR_ATENDIMENTO),
                                     V_DT_FIM),
                                 V_DT_FIM) DT_SAIDA
                      FROM ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                     WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                       AND S.CD_ESTABELECIMENTO = V_ESTAB
                       AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                       AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                       AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                       AND U.DT_ENTRADA_UNIDADE < V_DT_FIM 
                       AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                    U.DT_ENTRADA_UNIDADE,
                                                                    U.CD_SETOR_ATENDIMENTO),
                               V_DT_INI) >= V_DT_INI
                     GROUP BY U.NR_ATENDIMENTO,
                              S.CD_ESTABELECIMENTO,
                              U.CD_SETOR_ATENDIMENTO,
                              PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                   U.DT_ENTRADA_UNIDADE,
                                                                   U.CD_SETOR_ATENDIMENTO)) T;

          EXCEPTION WHEN OTHERS THEN
            NR := '0;0';            
          END;
        END;

      ELSE
        
        BEGIN
        
          SELECT COUNT(1)||';'||ROUND(NVL(SUM(DT_SAIDA - DT_ENTRADA), 0), 8)
            INTO NR
            FROM (SELECT /*U.NR_ATENDIMENTO,
                         S.CD_ESTABELECIMENTO,
                         U.CD_SETOR_ATENDIMENTO,*/
                         GREATEST(MIN(U.DT_ENTRADA_UNIDADE),
                                  PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) DT_ENTRADA,
                         LEAST(NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                              U.DT_ENTRADA_UNIDADE,
                                                              U.CD_SETOR_ATENDIMENTO),
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))),
                               PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))) DT_SAIDA                                              
                    FROM ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
                   WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                     AND DECODE(NVL(P_CD_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_CD_SETOR, 0)
                     AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                     AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                     AND U.DT_ENTRADA_UNIDADE < PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE))
                     AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                  U.DT_ENTRADA_UNIDADE,
                                                                  U.CD_SETOR_ATENDIMENTO),
                             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                   GROUP BY U.NR_ATENDIMENTO,
                            S.CD_ESTABELECIMENTO,
                            U.CD_SETOR_ATENDIMENTO,
                            PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                 U.DT_ENTRADA_UNIDADE,
                                                                 U.CD_SETOR_ATENDIMENTO)) T;

        EXCEPTION WHEN OTHERS THEN
          NR := '0;0';
        END;
               
      END IF;
     
    END IF;

    RETURN NVL(NR, '0;0');  
  
  END GET_STAY_COUNT_AND_AMOUNT;
  --
END PKG_ATEND_PAC_UNID;
/
