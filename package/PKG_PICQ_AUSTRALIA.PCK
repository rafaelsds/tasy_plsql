create or replace package pkg_picq_australia
is
/*
Performance Indicators for Coding Quality (Australia)
*/
    type picq is record(
        UEID 		varchar2(255),
        HOSPITAL 	varchar2(255),
		MRN			varchar2(255),
		DOA			DATE,
		DOS			DATE,
		DOB			DATE,
		SEX			varchar(15),
		D01			varchar(15),
		COF01		number(2),
		D02			varchar(15),
		COF02		number(2),
		D03			varchar(15),
		COF03		number(2),
		D04			varchar(15),
		COF04		number(2),
		D05			varchar(15),
		COF05		number(2),
		D06			varchar(15),
		COF06		number(2),
		D07			varchar(15),
		COF07		number(2),
		D08			varchar(15),
		COF08		number(2),
		D09			varchar(15),
		COF09		number(2),
		D10			varchar(15),
		COF10		number(2),
		D11			varchar(15),
		COF11		number(2),
		D12			varchar(15),
		COF12		number(2),
		D13			varchar(15),
		COF13		number(2),
		D14			varchar(15),
		COF14		number(2),
		D15			varchar(15),
		COF15		number(2),
		D16			varchar(15),
		COF16		number(2),
		D17			varchar(15),
		COF17		number(2),
		D18			varchar(15),
		COF18		number(2),
		D19			varchar(15),
		COF19		number(2),
		D20			varchar(15),
		COF20		number(2),
		D21			varchar(15),
		COF21		number(2),
		D22			varchar(15),
		COF22		number(2),
		D23			varchar(15),
		COF23		number(2),
		D24			varchar(15),
		COF24		number(2),
		D25			varchar(15),
		COF25		number(2),
		D26			varchar(15),
		COF26		number(2),
		D27			varchar(15),
		COF27		number(2),
		D28			varchar(15),
		COF28		number(2),
		D29			varchar(15),
		COF29		number(2),
		D30			varchar(15),
		COF30		number(2),
		D31			varchar(15),
		COF31		number(2),
		D32			varchar(15),
		COF32		number(2),
		D33			varchar(15),
		COF33		number(2),
		D34			varchar(15),
		COF34		number(2),
		D35			varchar(15),
		COF35		number(2),
		D36			varchar(15),
		COF36		number(2),
		D37			varchar(15),
		COF37		number(2),
		D38			varchar(15),
		COF38		number(2),
		D39			varchar(15),
		COF39		number(2),
		D40			varchar(15),
		COF40		number(2),
		D41			varchar(15),
		COF41		number(2),
		D42			varchar(15),
		COF42		number(2),
		D43			varchar(15),
		COF43		number(2),
		D44			varchar(15),
		COF44		number(2),
		D45			varchar(15),
		COF45		number(2),
		D46			varchar(15),
		COF46		number(2),
		D47			varchar(15),
		COF47		number(2),
		D48			varchar(15),
		COF48		number(2),
		D49			varchar(15),
		COF49		number(2),
		D50			varchar(15),
		COF50		number(2),
		P01			varchar2(20),
		P02			varchar2(20),
		P03			varchar2(20),
		P04			varchar2(20),
		P05			varchar2(20),
		P06			varchar2(20),
		P07			varchar2(20),
		P08			varchar2(20),
		P09			varchar2(20),
		P10			varchar2(20),
		P11			varchar2(20),
		P12			varchar2(20),
		P13			varchar2(20),
		P14			varchar2(20),
		P15			varchar2(20),
		P16			varchar2(20),
		P17			varchar2(20),
		P18			varchar2(20),
		P19			varchar2(20),
		P20			varchar2(20),
		P21			varchar2(20),
		P22			varchar2(20),
		P23			varchar2(20),
		P24			varchar2(20),
		P25			varchar2(20),
		P26			varchar2(20),
		P27			varchar2(20),
		P28			varchar2(20),
		P29			varchar2(20),
		P30			varchar2(20),
		P31			varchar2(20),
		P32			varchar2(20),
		P33			varchar2(20),
		P34			varchar2(20),
		P35			varchar2(20),
		P36			varchar2(20),
		P37			varchar2(20),
		P38			varchar2(20),
		P39			varchar2(20),
		P40			varchar2(20),
		P41			varchar2(20),
		P42			varchar2(20),
		P43			varchar2(20),
		P44			varchar2(20),
		P45			varchar2(20),
		P46			varchar2(20),
		P47			varchar2(20),
		P48			varchar2(20),
		P49			varchar2(20),
		P50			varchar2(20),
		CARETYPE	number(2),
		ADMMODE		number(2),
		SEPMODE		number(2),
		DRG			varchar2(20),
		ICUDAYS		number(3),
		ILOS		number(2),
		WEIGHT		number(4),
		VENT		number(10),
		LEAVE		number(2),
		MSTATUS		number(2),
		THEATRE		number(2),
		UNPLANNEDICU	number(2),
		CODERID			varchar2(255),
		CODINGDATE		date,
		CODINGTIME		date,
		DRGVERSION		varchar2(255),
		CLINICIANID		varchar2(255),
		SPECIALTY		varchar2(255),
		WARD			varchar2(255),
		FUND			varchar2(255)		
    );
     
    type picq_arr is table of picq;
     
    function get_data(
					dt_initial_p	date, 
					dt_final_p 		date) return picq_arr pipelined;
     
end;

/


create or replace package body pkg_picq_australia is
   
    function get_data(
					dt_initial_p	date, 
					dt_final_p 		date) 
										return picq_arr pipelined is
    
	data_arr picq_arr := picq_arr();


	-- Encounter Data
	cursor c01 is
	select	distinct
			a.nr_atendimento UEID, --Unique episode ID 
			a.cd_estabelecimento HOSPITAL, --Hospital ID
			b.nr_prontuario MRN, -- Medical record number 
			a.dt_entrada DOA, --Date of admission Format DD/MM/YYYY
			a.dt_alta DOS, -- Date of separation Format DD/MM/YYYY 
			b.dt_nascimento DOB, -- Date of birth Format DD/MM/YYYY
			b.ie_sexo SEX, -- Sex 1 or M = Male 2 or F = Female 3 or I = Other 9 or U = Not stated/inadequately described https://meteor.aihw.gov.au/content/index.phtml/itemId/635126 
			/*
			D01, -- Principal diagnosis code All diagnosis codes must conform to ICD-10-AM standards, except that all dots, dashes, slashes, daggers, and asterisks must be removed.
			D02toD99, -- Additional diagnoses codes All diagnosis codes must conform to ICD-10-AM standards, except that all dots, dashes, slashes, daggers, and asterisks must be removed. The order of the users data must be preserved with no gaps (that is, no blank columns) between consecutive codes.
			COF01toCOF99, --Condition Onset Flag Codes
			*/
			/*
				NB: PICQ follows the National standard, however the meaning of COF values varies by State. 
				Therefore you may need to map from your current COF values to these National standard values. Refer to page 2 for mapping details
				1 = Condition with onset during episode of admitted patient care
				2 = Condition not noted as arising during episode of admitted patient care
				9 = not reported
				Blank = if the corresponding diagnosis code is blank
				Should immediately follow the Diagnosis (D??) code to which it refers.
			*/
			/*
			P01toP99, --Procedure (intervention) codes All intervention codes must conform to ACHI standards, except that all dots, dashes, slashes, daggers, and asterisks must be removed. The order of the users data must be preserved with no gaps (that is, no blank columns) between consecutive codes.
			*/
			obter_conversao_externa_int(null,'ATENDIMENTO_PACIENTE','NR_SEQ_CLASSIFICACAO',a.nr_seq_classificacao,'PICQ')  CARETYPE, -- Care type Number 1 to 88 As per applicable NHDD version. https://meteor.aihw.gov.au/content/index.phtml/itemId/711010
			obter_conversao_externa_int(null,'ATENDIMENTO_PACIENTE','CD_PROCEDENCIA',a.cd_procedencia,'PICQ')  ADMMODE, -- Mode of Admission 1 = Admitted patient transferred from another hospital 2 = Statistical admission episode type change 3 = Other http://meteor.aihw.gov.au/content/index.phtml/itemId/269976
			obter_conversao_externa_int(null,'ATENDIMENTO_PACIENTE','CD_MOTIVO_ALTA',a.cd_motivo_alta,'PICQ')  SEPMODE, --Mode of separation Digit 1 to 9 as per applicable NHDD version. http://meteor.aihw.gov.au/content/index.phtml/itemId/270094			
			(select	max(y.cd_procedimento_loc) 
			from   	procedimento y,
					procedimento_paciente x
			where	y.cd_procedimento  = x.cd_procedimento
			and		y.ie_origem_proced = x.ie_origem_proced
			and		x.ie_origem_proced = 15
			and     x.nr_atendimento  = a.nr_atendimento) DRG, -- DRG Can be AN-DRG v3.1, or AR-DRG v4.1, 4.2, 5.0, 5.1, 5.2. 6.0, 6.0x, 7.0, 8.0 or 9.0
			nvl(get_days_by_classification(a.nr_atendimento,4),0) ICUDAYS, -- Days in ICU  Valid values: Blank (empty field), 0, or an integer with two digits
			a.ie_tipo_atendimento ILOS, --Intended length of stay 1 = Intended same day 2 = Intended overnight or longer
			null WEIGHT, -- Neonatal admission weight in grams Valid values: Blank (empty field), 0, or an integer with two to four digits (10-9999)
			0 VENT, -- Hours on mechanical ventilation Treated as zero if hours not present.
			0 LEAVE, --Leave days  Treated as zero if days not present.
			9 MSTATUS, -- Mental health legal status 1 = Involuntary 2 = Voluntary 3 or 9 = Not reported/Unknown. As per applicable NHDD version. https://meteor.aihw.gov.au/content/index.phtml/itemId/722675 
			9 THEATRE, -- Unplanned return to operating theatre 1 = Yes 2 = No 9 = Not stated/unknown As per applicable NHDD version. http://meteor.aihw.gov.au/content/index.phtml/itemId/578317
			9 UNPLANNEDICU,  -- Unplanned ICU admission following other surgery 1 = Yes 2 = No 9 = Not stated/Unknown As per applicable NHDD version. http://meteor.aihw.gov.au/content/index.phtml/itemId/608995		
			--null CODERID, -- Coder ID Maximum length 255
			/*
			The is the identifier that is used in your health information/coding systems to identify the user that coded the episode
			NB: We strongly recommend against using a value that may change, such as the Coders full name.
			*/
			--null CODINGDATE, -- Date the episode was coded or modified  Format DD/MM/YYYY
			--null CODINGTIME, -- Time the episode was coded or modified  Format HH:MM:SS
			--obtain_drg_version (a.nr_atendimento,null) DRGVERSION, --DRG version  Valid values: Blank (empty field) or N.N[x]
			null DRGVERSION, --DRG version  Valid values: Blank (empty field) or N.N[x]
			a.cd_medico_resp CLINICIANID, -- Attending Medical Officer    Maximum length 255     The ID used for the doctor in the health information system.
			substr(obter_especialidade_medico(a.cd_medico_resp, 'D'),1,255) SPECIALTY, --Specialty  Maximum length 255
			substr((select obter_nome_setor(x.cd_setor_atendimento) 
				from atend_paciente_unidade x
				where x.nr_seq_interno = Obter_Atepacu_paciente(a.nr_atendimento,'AA')),1,255) WARD, -- Ward  Maximum length 255 The ward code used in the health information system. Default is to use the discharge ward if patient was in more than one ward during episode.
			substr((select obter_nome_convenio(x.cd_convenio) 
				from atend_categoria_convenio x 
				where x.nr_seq_interno = obter_atecaco_atendimento(a.nr_atendimento)),1,255) FUND -- Health Fund   Maximum length 255  Valid values: Health fund identifier or blank field
			--Your Choice User Defined Fields Maximum length 255 
	from	atendimento_paciente a, 
			pessoa_fisica b,
			patient_coding c
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and		a.nr_atendimento = c.nr_encounter
	and		c.ie_status = 'F'
	and		a.dt_entrada  between dt_initial_p and dt_final_p;
	c01_w	c01%rowtype;
	
	-- Coding Data
	cursor c02 is
	select 	a.nm_usuario CODERID,
  
      (select to_date(to_char(max(p.dt_process),'dd/MM/yyyy'),'dd/MM/yyyy')
		from 	patient_coding p 
		where 	p.nr_encounter = a.nr_encounter 
		and		p.dt_inactive is null
		and 	p.ie_status = 'F') CODINGDATE,
    (select to_date(to_char(max(p.dt_process),'hh24:mi'),'hh24:mi')
		from 	patient_coding p 
		where 	p.nr_encounter = a.nr_encounter 
		and		p.dt_inactive is null
		and 	p.ie_status = 'F') CODINGTIME
			--pkg_date_formaters.to_varchar(a.dt_start_coding,'shortDate',wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario) CODINGDATE,
	--		pkg_date_formaters.to_varchar(a.dt_start_coding,'shortTime',wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario) CODINGTIME	
	from	patient_coding a
	where	a.ie_status = 'F'
  and		a.dt_inactive is null
	and		a.nr_encounter = c01_w.UEID;
	
	c02_w	c02%rowtype;
	
-- Diagnose Data
	/*
		NB: PICQ follows the National standard, however the meaning of COF values varies by State. 
		Therefore you may need to map from your current COF values to these National standard values. Refer to page 2 for mapping details
		1 = Condition with onset during episode of admitted patient care
		2 = Condition not noted as arising during episode of admitted patient care
		9 = not reported
		Blank = if the corresponding diagnosis code is blank
		Should immediately follow the Diagnosis (D??) code to which it refers.
	*/			
	cursor c03 is
	select rownum row_id ,x.icd_code,x.code_identifier
from        
( select a.cd_doenca icd_code,
decode(a.ie_tipo_diagnostico,1,1,9) code_identifier --Condition Onset Flag Codes        
	from    diagnostico_doenca a
	where   a.nr_atendimento = c01_w.UEID
  and a.ie_classificacao_doenca ='P'    -- prinicipal
  and a.dt_liberacao is not null
  and a.ie_situacao = 'A'
  order by a.ie_classificacao_doenca) x;
	
	c03_w	c03%rowtype;	
	
	-- Procedure Data
	cursor c04 is
	select rownum row_id,  replace(replace(replace(procedure_code,'-',''),'*',''),'.','') procedure_code
	from (
	select 	a.cd_procedimento_loc procedure_code
	from	procedimento a,
			procedimento_pac_medico b
	where	a.cd_procedimento  = b.cd_procedimento
	and		a.ie_origem_proced = b.ie_origem_proced
	and		b.ie_origem_proced = 19 --ACHI
  and b.ie_situacao ='A'
	and		b.nr_atendimento = c01_w.UEID)
  where procedure_code is not null;
	c04_w	c04%rowtype;		
	count_w		number(2) := 1;
	
	begin
		open C01;
		loop
		fetch C01 into	
			c01_w;
		exit when C01%notfound;
			begin
			
			data_arr.extend;
            data_arr(data_arr.last).UEID 			:= c01_w.UEID;
            data_arr(data_arr.last).HOSPITAL		:= c01_w.HOSPITAL;
			data_arr(data_arr.last).MRN				:= c01_w.MRN;
			data_arr(data_arr.last).DOA				:= c01_w.DOA;
			data_arr(data_arr.last).DOS				:= c01_w.DOS;
			data_arr(data_arr.last).DOB				:= c01_w.DOB;
			data_arr(data_arr.last).SEX				:= c01_w.SEX;
			data_arr(data_arr.last).CARETYPE		:= c01_w.CARETYPE;
			data_arr(data_arr.last).ADMMODE			:= c01_w.ADMMODE;
			data_arr(data_arr.last).SEPMODE			:= c01_w.SEPMODE;
			data_arr(data_arr.last).DRG				:= c01_w.DRG;
			data_arr(data_arr.last).ICUDAYS			:= c01_w.ICUDAYS;
			data_arr(data_arr.last).ILOS			:= c01_w.ILOS;
			data_arr(data_arr.last).WEIGHT			:= c01_w.WEIGHT;
			data_arr(data_arr.last).VENT			:= c01_w.VENT;
			data_arr(data_arr.last).LEAVE			:= c01_w.LEAVE;
			data_arr(data_arr.last).MSTATUS			:= c01_w.MSTATUS;
			data_arr(data_arr.last).THEATRE			:= c01_w.THEATRE;
			data_arr(data_arr.last).UNPLANNEDICU	:= c01_w.UNPLANNEDICU;
			data_arr(data_arr.last).DRGVERSION		:= c01_w.DRGVERSION;
			data_arr(data_arr.last).CLINICIANID		:= c01_w.CLINICIANID;
			data_arr(data_arr.last).SPECIALTY		:= c01_w.SPECIALTY;
			data_arr(data_arr.last).WARD			:= c01_w.WARD;
			data_arr(data_arr.last).FUND			:= c01_w.FUND;			
					
			
			/* Coding information*/
			open C02;
			loop
			fetch C02 into	
				c02_w;
			exit when C02%notfound;
				begin			
			
				data_arr(data_arr.last).CODERID			:= c02_w.CODERID;
				data_arr(data_arr.last).CODINGDATE		:= c02_w.CODINGDATE;
				data_arr(data_arr.last).CODINGTIME		:= c02_w.CODINGTIME;
			
				end;
			end loop;
			close C02;			


			/* Procedure information*/
			open C04;
			loop
			fetch C04 into	
				c04_w;
			exit when C04%notfound;
				begin			
				
				if (c04_w.row_id = 1) then
					data_arr(data_arr.last).P01			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 2) then
					data_arr(data_arr.last).P02			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 3) then
					data_arr(data_arr.last).P03			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 4) then
					data_arr(data_arr.last).P04			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 5) then
					data_arr(data_arr.last).P05			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 6) then
					data_arr(data_arr.last).P06			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 7) then
					data_arr(data_arr.last).P07 		:= c04_w.procedure_code;
				elsif (c04_w.row_id = 8) then
					data_arr(data_arr.last).P08			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 9) then
					data_arr(data_arr.last).P09			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 10) then
					data_arr(data_arr.last).P10			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 11) then
					data_arr(data_arr.last).P11			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 12) then
					data_arr(data_arr.last).P12			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 13) then
					data_arr(data_arr.last).P13			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 14) then
					data_arr(data_arr.last).P14			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 15) then
					data_arr(data_arr.last).P15			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 16) then
					data_arr(data_arr.last).P16			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 17) then
					data_arr(data_arr.last).P17			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 18) then
					data_arr(data_arr.last).P18			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 19) then
					data_arr(data_arr.last).P19			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 20) then
					data_arr(data_arr.last).P20			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 21) then
					data_arr(data_arr.last).P21			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 22) then
					data_arr(data_arr.last).P22			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 23) then
					data_arr(data_arr.last).P23			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 24) then
					data_arr(data_arr.last).P24			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 25) then
					data_arr(data_arr.last).P25			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 26) then
					data_arr(data_arr.last).P26			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 27) then
					data_arr(data_arr.last).P27			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 28) then
					data_arr(data_arr.last).P28			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 29) then
					data_arr(data_arr.last).P29			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 30) then
					data_arr(data_arr.last).P30			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 31) then
					data_arr(data_arr.last).P31			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 32) then
					data_arr(data_arr.last).P32			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 33) then
					data_arr(data_arr.last).P33			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 34) then
					data_arr(data_arr.last).P34			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 35) then
					data_arr(data_arr.last).P35			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 36) then
					data_arr(data_arr.last).P36			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 37) then
					data_arr(data_arr.last).P37			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 38) then
					data_arr(data_arr.last).P38			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 39) then
					data_arr(data_arr.last).P39			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 40) then
					data_arr(data_arr.last).P40			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 41) then
					data_arr(data_arr.last).P41			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 42) then
					data_arr(data_arr.last).P42			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 43) then
					data_arr(data_arr.last).P43			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 44) then
					data_arr(data_arr.last).P44			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 45) then
					data_arr(data_arr.last).P45			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 46) then
					data_arr(data_arr.last).P46			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 47) then
					data_arr(data_arr.last).P47			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 48) then
					data_arr(data_arr.last).P48			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 49) then
					data_arr(data_arr.last).P49			:= c04_w.procedure_code;
				elsif (c04_w.row_id = 50) then
					data_arr(data_arr.last).P50			:= c04_w.procedure_code;
				end if;
				
				end;
			end loop;
			close C04;



			/* Diagnose information*/
			open c03;
			loop
			fetch c03 into	
				c03_w;
			exit when c03%notfound;
				begin		

				if (c03_w.row_id = 1) then
					data_arr(data_arr.last).D01			:= c03_w.icd_code;
					data_arr(data_arr.last).COF01			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 2) then
					data_arr(data_arr.last).D02			:= c03_w.icd_code;
					data_arr(data_arr.last).COF02			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 3) then
					data_arr(data_arr.last).D03			:= c03_w.icd_code;
					data_arr(data_arr.last).COF03			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 4) then
					data_arr(data_arr.last).D04			:= c03_w.icd_code;
					data_arr(data_arr.last).COF04			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 5) then
					data_arr(data_arr.last).D05			:= c03_w.icd_code;
					data_arr(data_arr.last).COF05			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 6) then
					data_arr(data_arr.last).D06			:= c03_w.icd_code;
					data_arr(data_arr.last).COF06			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 7) then
					data_arr(data_arr.last).D07 		:= c03_w.icd_code;
					data_arr(data_arr.last).COF07 			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 8) then
					data_arr(data_arr.last).D08			:= c03_w.icd_code;
					data_arr(data_arr.last).COF08			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 9) then
					data_arr(data_arr.last).D09			:= c03_w.icd_code;
					data_arr(data_arr.last).COF09			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 10) then
					data_arr(data_arr.last).D10			:= c03_w.icd_code;
					data_arr(data_arr.last).COF10			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 11) then
					data_arr(data_arr.last).D11			:= c03_w.icd_code;
					data_arr(data_arr.last).COF11			:= c03_w.code_identifier;					
				elsif (c03_w.row_id = 12) then
					data_arr(data_arr.last).D12			:= c03_w.icd_code;
					data_arr(data_arr.last).COF12			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 13) then
					data_arr(data_arr.last).D13			:= c03_w.icd_code;
					data_arr(data_arr.last).COF13			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 14) then
					data_arr(data_arr.last).D14			:= c03_w.icd_code;
					data_arr(data_arr.last).COF14			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 15) then
					data_arr(data_arr.last).D15			:= c03_w.icd_code;
					data_arr(data_arr.last).COF15			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 16) then
					data_arr(data_arr.last).D16			:= c03_w.icd_code;
					data_arr(data_arr.last).COF16			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 17) then
					data_arr(data_arr.last).D17			:= c03_w.icd_code;
					data_arr(data_arr.last).COF17			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 18) then
					data_arr(data_arr.last).D18			:= c03_w.icd_code;
					data_arr(data_arr.last).COF18			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 19) then
					data_arr(data_arr.last).D19			:= c03_w.icd_code;
					data_arr(data_arr.last).COF19			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 20) then
					data_arr(data_arr.last).D20			:= c03_w.icd_code;
					data_arr(data_arr.last).COF20			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 21) then
					data_arr(data_arr.last).D21			:= c03_w.icd_code;
					data_arr(data_arr.last).COF21			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 22) then
					data_arr(data_arr.last).D22			:= c03_w.icd_code;
					data_arr(data_arr.last).COF22			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 23) then
					data_arr(data_arr.last).D23			:= c03_w.icd_code;
					data_arr(data_arr.last).COF23			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 24) then
					data_arr(data_arr.last).D24			:= c03_w.icd_code;
					data_arr(data_arr.last).COF24			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 25) then
					data_arr(data_arr.last).D25			:= c03_w.icd_code;
					data_arr(data_arr.last).COF25			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 26) then
					data_arr(data_arr.last).D26			:= c03_w.icd_code;
					data_arr(data_arr.last).COF26			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 27) then
					data_arr(data_arr.last).D27			:= c03_w.icd_code;
					data_arr(data_arr.last).COF27			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 28) then
					data_arr(data_arr.last).D28			:= c03_w.icd_code;
					data_arr(data_arr.last).COF28			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 29) then
					data_arr(data_arr.last).D29			:= c03_w.icd_code;
					data_arr(data_arr.last).COF29			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 30) then
					data_arr(data_arr.last).D30			:= c03_w.icd_code;
					data_arr(data_arr.last).COF30			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 31) then
					data_arr(data_arr.last).D31			:= c03_w.icd_code;
					data_arr(data_arr.last).COF31			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 32) then
					data_arr(data_arr.last).D32			:= c03_w.icd_code;
					data_arr(data_arr.last).COF32			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 33) then
					data_arr(data_arr.last).D33			:= c03_w.icd_code;
					data_arr(data_arr.last).COF33			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 34) then
					data_arr(data_arr.last).D34			:= c03_w.icd_code;
					data_arr(data_arr.last).COF34			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 35) then
					data_arr(data_arr.last).D35			:= c03_w.icd_code;
					data_arr(data_arr.last).COF35			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 36) then
					data_arr(data_arr.last).D36			:= c03_w.icd_code;
					data_arr(data_arr.last).COF36			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 37) then
					data_arr(data_arr.last).D37			:= c03_w.icd_code;
					data_arr(data_arr.last).COF37			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 38) then
					data_arr(data_arr.last).D38			:= c03_w.icd_code;
					data_arr(data_arr.last).COF38			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 39) then
					data_arr(data_arr.last).D39			:= c03_w.icd_code;
					data_arr(data_arr.last).COF39			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 40) then
					data_arr(data_arr.last).D40			:= c03_w.icd_code;
					data_arr(data_arr.last).COF40			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 41) then
					data_arr(data_arr.last).D41			:= c03_w.icd_code;
					data_arr(data_arr.last).COF41			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 42) then
					data_arr(data_arr.last).D42			:= c03_w.icd_code;
					data_arr(data_arr.last).COF42			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 43) then
					data_arr(data_arr.last).D43			:= c03_w.icd_code;
					data_arr(data_arr.last).COF43			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 44) then
					data_arr(data_arr.last).D44			:= c03_w.icd_code;
					data_arr(data_arr.last).COF44			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 45) then
					data_arr(data_arr.last).D45			:= c03_w.icd_code;
					data_arr(data_arr.last).COF45			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 46) then
					data_arr(data_arr.last).D46			:= c03_w.icd_code;
					data_arr(data_arr.last).COF46			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 47) then
					data_arr(data_arr.last).D47			:= c03_w.icd_code;
					data_arr(data_arr.last).COF47			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 48) then
					data_arr(data_arr.last).D48			:= c03_w.icd_code;
					data_arr(data_arr.last).COF48			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 49) then
					data_arr(data_arr.last).D49			:= c03_w.icd_code;
					data_arr(data_arr.last).COF49			:= c03_w.code_identifier;
				elsif (c03_w.row_id = 50) then
					data_arr(data_arr.last).D50			:= c03_w.icd_code;
					data_arr(data_arr.last).COF50			:= c03_w.code_identifier;

				end if;
				
				end;
			end loop;
			close c03;				
			
			
            pipe row(data_arr(data_arr.last));			
			
			end;
		end loop;
		close C01;
		return;
    end;
       
end;
/