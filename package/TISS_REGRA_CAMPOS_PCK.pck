create or replace package tiss_regra_campos_pck as

	
procedure gerar_regra_campos(cd_convenio_p		number,
			     cd_estabelecimento_p	number,
			     qt_existentes_p		out number,
			     qt_geradas_p		out number);	


function obter_regra_campos (	ie_tipo_guia_p		varchar2,
				nm_atributo_p		varchar2,
				cd_convenio_p		number,
				vl_campo_p		varchar2,
				ie_tipo_atendimento_p	number,
				cd_categoria_conv_p	varchar2,
				ie_tipo_despesa_p	varchar2,
				vl_max_campo_p		number,
				cd_estabelecimento_p	number,
				ie_clinica_p		number,
				nr_seq_classificacao_p	number,
				cd_setor_atendimento_p	number,
				ds_parametros_p		varchar2,
				cd_setor_execucao_p	number default null) return varchar2;
				

procedure listar_vetor;
procedure limpar_vetor;
			
			
end tiss_regra_campos_pck;
/
create or replace package body tiss_regra_campos_pck as

type regra_campos_tb is record (ie_envio		tiss_regra_campo_conv.ie_envio%type,
				ds_padrao		tiss_regra_campo_conv.ds_padrao%type,
				vl_max_campo		tiss_regra_campo_conv.vl_max_campo%type,
				ds_valor_gerado		tiss_regra_campo_conv.ds_valor_gerado%type,
				ds_mascara		tiss_regra_campo_conv.ds_mascara%type,
				cd_convenio		convenio.cd_convenio%type,
				cd_estabelecimento	estabelecimento.cd_estabelecimento%type,
				ds_versao		varchar2(50),
				ie_tiss_tipo_guia	tiss_lista_campo.ie_tiss_tipo_guia%type,
				nm_atributo		tiss_lista_campo.nm_atributo%type,
				ie_tipo_atendimento	atendimento_paciente.ie_tipo_atendimento%type,
				cd_categoria		categoria_convenio.cd_categoria%type,
				ie_gases		tiss_regra_campo_conv.ie_gases%type,
				ie_taxas		tiss_regra_campo_conv.ie_taxas%type,
				ie_medicamento		tiss_regra_campo_conv.ie_medicamento%type,
				ie_material		tiss_regra_campo_conv.ie_material%type,
				ie_taxas_diversas	tiss_regra_campo_conv.ie_taxas_diversas%type,
				ie_diarias		tiss_regra_campo_conv.ie_diarias%type,
				ie_alugueis		tiss_regra_campo_conv.ie_alugueis%type,
				ie_clinica		tiss_regra_campo_conv.ie_clinica%type,
				nr_seq_classificacao	tiss_regra_campo_conv.nr_seq_classificacao%type,
				cd_setor_entrada	tiss_regra_campo_conv.cd_setor_entrada%type,
				cd_setor_exec		tiss_regra_campo_conv.cd_setor_exec%type,
				ie_tipo_atend_tiss	tiss_regra_campo_conv.ie_tipo_atend_tiss%type,
				ie_tipo_atend_proc_tiss	tiss_regra_campo_conv.ie_tipo_atend_proc_tiss%type,
				cd_perfil		tiss_regra_campo_conv.cd_perfil%type,
				indice			binary_integer,
				ie_conv_sem_regra	varchar2(1));
				
type regra_campos_tb_v  is table of regra_campos_tb index 	by binary_integer;
regra_campos_tb_w		regra_campos_tb_v;

procedure gerar_regra_campos(cd_convenio_p		number,
			     cd_estabelecimento_p	number,
			     qt_existentes_p		out number,
			     qt_geradas_p		out number) as

i				binary_integer;	
ds_versao_tiss_w		varchar2(50);	
qt_existe_regra_conv_w		number(5);				 
begin

if	(cd_convenio_p is not null and 
	cd_estabelecimento_p is not null) then
	
	begin
	ds_versao_tiss_w		:= tiss_obter_versao(cd_convenio_p,cd_estabelecimento_p);
	exception
	when others then
		ds_versao_tiss_w	:= null;
	end;
	qt_existe_regra_conv_w := 0;
	i := 0;
	if	regra_campos_tb_w.count > 0 then
		for i in regra_campos_tb_w.first .. regra_campos_tb_w.last  loop
			begin
			if 	(regra_campos_tb_w(i).cd_convenio	= cd_convenio_p) and
				(regra_campos_tb_w(i).cd_estabelecimento = cd_estabelecimento_p) then
				qt_existe_regra_conv_w := qt_existe_regra_conv_w + 1;
			end if;
			end;
		end loop;
	end if;
	
	qt_existentes_p := qt_existe_regra_conv_w;
	
	i := regra_campos_tb_w.count;
	if	qt_existe_regra_conv_w = 0 then 
	
		for	r in (	select	nvl(a.ie_envio, 'E')ie_envio,
					a.ds_padrao,
					a.vl_max_campo,
					a.ds_valor_gerado,
					a.ds_mascara,
					b.ie_tiss_tipo_guia,
					b.nm_atributo,
					a.ie_tipo_atendimento,
					a.cd_categoria,
					a.ie_gases,
					a.ie_taxas,
					a.ie_medicamento,
					a.ie_material,
					a.ie_taxas_diversas,
					a.ie_diarias,
					a.ie_alugueis,
					a.ie_clinica,
					a.nr_seq_classificacao,
					a.cd_setor_entrada,
					a.cd_setor_exec,
					a.ie_tipo_atend_tiss,
					a.ie_tipo_atend_proc_tiss,
					a.cd_perfil
				from	tiss_lista_campo b,
					tiss_regra_campo_conv a
				where	a.nr_seq_campo		= b.nr_sequencia
				and	a.cd_convenio		= cd_convenio_p
				and	a.cd_estabelecimento	= cd_estabelecimento_p
				order by	a.ie_taxas,
					nvl(cd_setor_exec, 0),
					nvl(ie_tipo_atendimento, 0),
					nvl(ie_clinica, 0),
					nvl(cd_categoria, '0'),
					nvl(cd_setor_entrada, '0'),
					nvl(a.nr_seq_classificacao,0),
					nvl(a.ie_tipo_atend_tiss,'0'),
					nvl(a.ie_tipo_atend_proc_tiss,'0'),
					nvl(a.cd_perfil,0)) loop
					
			i := i + 1;
					
			regra_campos_tb_w(i).ie_envio			:= r.ie_envio;		
			regra_campos_tb_w(i).ds_padrao			:= r.ds_padrao;	
			regra_campos_tb_w(i).vl_max_campo		:= r.vl_max_campo;	
			regra_campos_tb_w(i).ds_valor_gerado		:= r.ds_valor_gerado;	
			regra_campos_tb_w(i).ds_mascara			:= r.ds_mascara;	
			regra_campos_tb_w(i).cd_convenio		:= cd_convenio_p;	
			regra_campos_tb_w(i).cd_estabelecimento		:= cd_estabelecimento_p;
			regra_campos_tb_w(i).ds_versao			:= ds_versao_tiss_w;
			regra_campos_tb_w(i).ie_tiss_tipo_guia		:= r.ie_tiss_tipo_guia;
			regra_campos_tb_w(i).nm_atributo		:= r.nm_atributo;
			regra_campos_tb_w(i).ie_tipo_atendimento	:= r.ie_tipo_atendimento;
			regra_campos_tb_w(i).cd_categoria		:= r.cd_categoria;	
			regra_campos_tb_w(i).ie_gases			:= r.ie_gases;
			regra_campos_tb_w(i).ie_taxas			:= r.ie_taxas;
			regra_campos_tb_w(i).ie_medicamento		:= r.ie_medicamento;
			regra_campos_tb_w(i).ie_material		:= r.ie_material;
			regra_campos_tb_w(i).ie_taxas_diversas		:= r.ie_taxas_diversas;
			regra_campos_tb_w(i).ie_diarias			:= r.ie_diarias;
			regra_campos_tb_w(i).ie_alugueis		:= r.ie_alugueis;
			regra_campos_tb_w(i).ie_clinica			:= r.ie_clinica;
			regra_campos_tb_w(i).nr_seq_classificacao	:= r.nr_seq_classificacao;
			regra_campos_tb_w(i).cd_setor_entrada		:= r.cd_setor_entrada;
			regra_campos_tb_w(i).cd_setor_exec		:= r.cd_setor_exec;
			regra_campos_tb_w(i).ie_tipo_atend_tiss		:= r.ie_tipo_atend_tiss;
			regra_campos_tb_w(i).ie_tipo_atend_proc_tiss	:= r.ie_tipo_atend_proc_tiss;
			regra_campos_tb_w(i).cd_perfil			:= r.cd_perfil;
			regra_campos_tb_w(i).indice			:= i;
			qt_existe_regra_conv_w				:= qt_existe_regra_conv_w + 1;		
		end loop;

		qt_geradas_p := qt_existe_regra_conv_w;
	end if;
	
	if	qt_existe_regra_conv_w = 0 then
		i := regra_campos_tb_w.count + 1;
		regra_campos_tb_w(i).cd_convenio		:= cd_convenio_p;	
		regra_campos_tb_w(i).cd_estabelecimento		:= cd_estabelecimento_p;
		regra_campos_tb_w(i).ie_conv_sem_regra		:= 'S';
		regra_campos_tb_w(i).indice			:= regra_campos_tb_w.count + 1;
	end if;
end if;
end;

function obter_regra_campos (	ie_tipo_guia_p		varchar2,
				nm_atributo_p		varchar2,
				cd_convenio_p		number,
				vl_campo_p		varchar2,
				ie_tipo_atendimento_p	number,
				cd_categoria_conv_p	varchar2,
				ie_tipo_despesa_p	varchar2,
				vl_max_campo_p		number,
				cd_estabelecimento_p	number,
				ie_clinica_p		number,
				nr_seq_classificacao_p	number,
				cd_setor_atendimento_p	number,
				ds_parametros_p		varchar2,
				cd_setor_execucao_p	number default null) return varchar2 as


j				number(10);
ie_envio_w			varchar2(4000);
ds_padrao_w			varchar2(4000);
vl_permitido_w			number(10,2);
ds_retorno_w			varchar2(4000);
ds_temp_w			varchar2(4000);
dt_retorno_aux_w		date;
ds_valor_gerado_w		varchar2(4000);
ds_conteudo_w			varchar2(4000);
vl_atributo_w			varchar2(255);
ie_tipo_atend_tiss_w		varchar2(255);
ie_tipo_atend_tiss_proc_w	varchar2(255);
ds_mascara_w			varchar2(255);
qt_digitos_mascara_w		number(10);
qt_digitos_valor_w		number(10);
qt_registros_vetor_w		number(15);
i				binary_integer;
cd_perfil_w			number(5,0) := wheb_usuario_pck.get_cd_perfil;

ds_versao_tiss_w		varchar2(20);
nm_atributo_w			varchar2(255);
qt_regra_w			number(10);	
ie_versao_3_w			varchar2(1);		
begin
ie_envio_w 		:= null;
ds_valor_gerado_w 	:= null;
ds_padrao_w		:= null;
i := 0;

ds_conteudo_w			:= ds_parametros_p;
j 				:= instr(ds_conteudo_w,'#@');
ie_tipo_atend_tiss_w		:= ltrim(substr(substr(ds_conteudo_w,1, j - 1),1,255),'0');
ds_conteudo_w			:= substr(ds_conteudo_w, j + 2, 4000);
ie_tipo_atend_tiss_proc_w	:= ltrim(substr(substr(ds_conteudo_w,1, j - 1),1,255),'0');
ds_conteudo_w			:= substr(ds_conteudo_w, j + 2, 4000);

if	(regra_campos_tb_w.count > 0) then
	for i in regra_campos_tb_w.first .. regra_campos_tb_w.last loop
			begin
		if 	(regra_campos_tb_w(i).cd_convenio		= cd_convenio_p) and
			(regra_campos_tb_w(i).cd_estabelecimento 	= cd_estabelecimento_p) and
			(regra_campos_tb_w(i).nm_atributo 		= nm_atributo_p) and
			(regra_campos_tb_w(i).ie_tiss_tipo_guia 	= ie_tipo_guia_p) then
			
			ie_versao_3_w := obter_se_projeto_versao(0,12,regra_campos_tb_w(i).ds_versao,0);
			
			if	((regra_campos_tb_w(i).ie_gases = 'S') 		or ((regra_campos_tb_w(i).ie_gases = 'N') 	and (ie_tipo_despesa_p <> '1'))) and
				((regra_campos_tb_w(i).ie_medicamento = 'S')	or ((regra_campos_tb_w(i).ie_medicamento = 'N') and (ie_tipo_despesa_p <> '2'))) and
				((regra_campos_tb_w(i).ie_material = 'S') 	or ((regra_campos_tb_w(i).ie_material = 'N') 	and (ie_tipo_despesa_p <> '3'))) and
				((regra_campos_tb_w(i).ie_diarias = 'S') 	or ((regra_campos_tb_w(i).ie_diarias = 'N') 	and (ie_tipo_despesa_p <> '5'))) and
				(nvl(regra_campos_tb_w(i).ie_tipo_atendimento, nvl(ie_tipo_atendimento_p, 0)) 			= nvl(ie_tipo_atendimento_p,0))and
				(nvl(regra_campos_tb_w(i).ie_clinica, nvl(ie_clinica_p, 0)) 					= nvl(ie_clinica_p, 0)) and
				(nvl(regra_campos_tb_w(i).cd_categoria, nvl(cd_categoria_conv_p, '0')) 				= nvl(cd_categoria_conv_p, '0')) and
				(nvl(regra_campos_tb_w(i).nr_seq_classificacao, nvl(nr_seq_classificacao_p, '0'))		= nvl(nr_seq_classificacao_p, '0')) and
				(nvl(regra_campos_tb_w(i).cd_setor_entrada, nvl(cd_setor_atendimento_p, '0'))			= nvl(cd_setor_atendimento_p, '0')) and
				(nvl(regra_campos_tb_w(i).cd_setor_exec, nvl(cd_setor_execucao_p, '0'))				= nvl(cd_setor_execucao_p, '0'))and
				(nvl(regra_campos_tb_w(i).ie_tipo_atend_tiss, nvl(ie_tipo_atend_tiss_w,'0'))			= nvl(ie_tipo_atend_tiss_w,'0'))and
				(nvl(regra_campos_tb_w(i).ie_tipo_atend_proc_tiss, nvl(ie_tipo_atend_tiss_proc_w,'0'))		= nvl(ie_tipo_atend_tiss_proc_w,'0'))and
				(nvl(regra_campos_tb_w(i).cd_perfil,nvl(cd_perfil_w,0))						= nvl(cd_perfil_w,0)) and
				((ie_versao_3_w = 'N') and ((regra_campos_tb_w(i).ie_taxas = 'N') or (regra_campos_tb_w(i).ie_taxas = 'S' and ie_tipo_despesa_p in ('4','5'))) or
				(ie_versao_3_w = 'S')  and ((regra_campos_tb_w(i).ie_taxas = 'N') 		or (regra_campos_tb_w(i).ie_taxas = 'S' and ie_tipo_despesa_p in ('5','7'))))	and
				((ie_versao_3_w = 'N') and ((regra_campos_tb_w(i).ie_taxas_diversas = 'S') or ((regra_campos_tb_w(i).ie_taxas_diversas = 'N') and (ie_tipo_despesa_p <> '4'))) or 
				(ie_versao_3_w = 'S')  and ((regra_campos_tb_w(i).ie_taxas_diversas = 'S') or ((regra_campos_tb_w(i).ie_taxas_diversas = 'N') and (ie_tipo_despesa_p <> '7')))) and
				((ie_versao_3_w = 'N') and (regra_campos_tb_w(i).ie_alugueis = 'S') or ((regra_campos_tb_w(i).ie_alugueis = 'N') and (ie_tipo_despesa_p <> '6')) or
				(ie_versao_3_w = 'S') and (regra_campos_tb_w(i).ie_alugueis = 'S') or ((regra_campos_tb_w(i).ie_alugueis = 'N') and (ie_tipo_despesa_p <> '7'))) then
							
				ie_envio_w 		:= regra_campos_tb_w(i).ie_envio;
				ds_valor_gerado_w	:= regra_campos_tb_w(i).ds_valor_gerado;
				ds_padrao_w		:= regra_campos_tb_w(i).ds_padrao;
				
			end if;
		
			
			
			
		end if;
		end;
	end loop;
end if;

if	(nvl(ie_envio_w, 'E') = 'PG') and (nvl(ds_valor_gerado_w,'X') = nvl(vl_campo_p,'Z')) then
	ds_retorno_w	:= ds_padrao_w;
end if;

if	(ds_retorno_w is null) then

	if	(nvl(vl_max_campo_p, 0) > 0) and
		(nvl(vl_permitido_w, 0) > 0) then
		if	(vl_max_campo_p	> vl_permitido_w) then
			ds_retorno_w	:= vl_permitido_w;
		else
			ds_retorno_w	:= vl_max_campo_p;
		end if;
	elsif	(nvl(ie_envio_w,'E') = 'E')  then

		ds_retorno_w		:= vl_campo_p;
		
		if	(instr(ds_padrao_w,'@sem_quebra') > 0) then
			ds_retorno_w	:= replace(replace(ds_retorno_w,chr(13),' '),chr(10),' ');
			ds_padrao_w	:= replace(ds_padrao_w,'@sem_quebra','');
		end if;	
		
	elsif	(nvl(ie_envio_w,'E') = 'T')  then
		ds_retorno_w		:= ds_padrao_w;
	elsif	(nvl(ie_envio_w,'E') = 'DP') and
		(ds_padrao_w <> vl_campo_p) then
		ds_retorno_w		:= vl_campo_p;
	elsif	(nvl(ie_envio_w,'E') = 'PG') then
		if	(nvl(ds_valor_gerado_w,'XYZ')   = nvl(vl_campo_p,'XYZ')) then
			ds_retorno_w		:= ds_padrao_w;
		else
			ds_retorno_w		:= vl_campo_p;
		end if;
	elsif	(nvl(ie_envio_w,'E') = 'DZ')  then
		if	(somente_numero(vl_campo_p) <> 0) then
			ds_retorno_w		:= vl_campo_p;
		end if;
	else
		ds_retorno_w	:= null;
	end if;

	--se soh veio a hora e o atributo eh um horario e estiver sem a data
	ds_temp_w := ds_retorno_w;
	if 	(instr(lower(nm_atributo_p),'hora') > 0) and (length(ds_retorno_w) < 10) then
		begin
			if (vl_campo_p is not null) then
				ds_retorno_w 	:= substr(vl_campo_p,1,10) || ' ' || ds_retorno_w;
			else
				ds_retorno_w 	:= '01/01/2000 ' || ds_retorno_w;			
			end if;
		exception
			when others then
			ds_retorno_w := ds_temp_w;
		end;
	end if;

	begin
	dt_retorno_aux_w := to_date(ds_retorno_w,'dd/mm/yyyy');
	exception
		when others then
		dt_retorno_aux_w := null;
	end;

	if	(ds_mascara_w is not null) and
		(ds_retorno_w is not null) and	
		(dt_retorno_aux_w is null) then
		begin

		if	(length(ds_mascara_w) > length(ds_retorno_w)) then
			qt_digitos_mascara_w	:= length(ds_mascara_w);
			qt_digitos_valor_w	:= length(ds_retorno_w);
			ds_retorno_w		:= rpad(ds_retorno_w,qt_digitos_mascara_w,'Z');
		end if;
		
		select	TISS_OBTER_COD_USUARIO_MASC(ds_retorno_w, ds_mascara_w)
		into	ds_retorno_w
		from	dual;
		
		if	(length(ds_retorno_w) >  qt_digitos_valor_w) then
			ds_retorno_w	:= rtrim(ds_retorno_w,'Z');
		end if;

		exception
			when others then
			ds_retorno_w	:= null;
		end;
	end if;
end if;	

return ds_retorno_w;

end;	

procedure listar_vetor as
i binary_integer := 0;
begin
dbms_output.put_line('DBMS - OK');
if	regra_campos_tb_w.count > 0 then
	for i in regra_campos_tb_w.first .. regra_campos_tb_w.last loop
		dbms_output.put_line(regra_campos_tb_w(i).cd_convenio || ' - ' || regra_campos_tb_w(i).nm_atributo || ' - ' ||regra_campos_tb_w(i).ie_tiss_tipo_guia);
	end loop;
end if;
end;		

procedure limpar_vetor as
begin
regra_campos_tb_w.delete;
end;
end tiss_regra_campos_pck;
/
