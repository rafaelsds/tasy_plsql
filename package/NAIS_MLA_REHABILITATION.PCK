create or replace PACKAGE NAIS_MLA_REHABILITATION AS

    TYPE accInfoRecTyp IS RECORD (
            process_classif        Varchar(100),
            order_class                Varchar(100),
            order_number               Varchar(100),
            patient_identifier        Varchar(100),
            consultation_date        Varchar(10),
            department_code          Varchar(100),
            patient_classification    Varchar(100),
            ward_code              Varchar(100),
            room_number             Varchar(100),
            blood_tranfusion_type     Varchar(100),
            insurance_number         Varchar(100),
            doctor_code              Varchar(100),
            patient_newborn           Varchar(100),
            prescription_classfication Varchar(100),
            requestor_code           Varchar(100),  
            mutual_exclusion_rule     Varchar(100),
            division_number           Varchar(100),
            blank_one                 Varchar(100),
            blank_two                  Varchar(100)
            );

     Type execInfoRedTyp is record (
            execution_classification    Varchar(1),
            execution_date                  Varchar(8),
            execution_time                  Varchar(4),
            execution_op_code            Varchar(3),
            execution_up_flag              Varchar(1),
            execution_weight               Varchar(6),
            out_of_hours_flag               Varchar(1),
            execution_blank                  Varchar(8)  
     );

     Type medTreatmentInfoRedTyp is record (
            count_medi_details         Varchar(2),
            slip_code                  Varchar(3),
            nr_data_class              Varchar(2), 
            internal_code              Varchar(6),
            dosage                     Varchar(9),
            unit                       Varchar(2),
            times_num                  Varchar(2),
            free_input_flag            Varchar(1),
            free_comments              Varchar(40),
            med_change_impossible_flg  Varchar(1),
            general_name_med_flg       Varchar(1),
            days                       Varchar(2),
            times_num_04               Varchar(2),
            days_num                   Varchar(2),
            digit_times_num            Varchar(3),
            digit_days_num             Varchar(3),
            medi_blank                 Varchar(4),
            eot                        Varchar(1)
     );
     
     procedure send_rehabilitation_data (
        nr_prescricao_p    number,
        cd_classif_p       varchar2,
        file_output_p       out  clob 
    ) ;

    procedure common_execution_info(
        r_c02 execInfoRedTyp
    ); 

    procedure common_accounting_info(
        r_c01 accInfoRecTyp 
    );

    procedure common_med_treatment_info(
        r_c03 medTreatmentInfoRedTyp
    );

    procedure get_action_code_med_detail(
        messageType_p varchar2,
        record_p medtreatmentinforedtyp,
        message_data out varchar2
    );
    procedure add_date_class_med_detail (
            messageType_p varchar2,
            record_p medtreatmentinforedtyp,
            message_data out varchar2
            );
    procedure add_med_treatment_detail_array(
        message_p varchar2,
        index_p number
        );
END NAIS_MLA_REHABILITATION;
/
create or replace package body NAIS_MLA_REHABILITATION as

    ds_line_w                           varchar2(32767);
    ds_local_w                          varchar2(32767);
    nm_usuario_w                        varchar2(20) := 'NAIS';
    ds_message_w                        varchar2(4000);
    ds_error_w                          varchar2(4000);
    filename_sequence_w                 varchar2(255);
    index_counter_w             number(3) := 0;
    TYPE med_treamtent_array IS TABLE OF VARCHAR2(32767);
    med_treamtent                      med_treamtent_array := med_treamtent_array();

	/** 
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Append data with values
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	**/

    procedure append_text(
		ds_text_p       varchar2,
		qt_size_p       number,
		ie_left_rigth_p varchar2 default 'L',
		ds_default_p    varchar2 default ' ' )
	is
		begin
			if ( ie_left_rigth_p = 'R' ) then
				ds_line_w := ds_line_w || rpad(ds_text_p, qt_size_p, ds_default_p);
			else
				ds_line_w := ds_line_w || lpad(ds_text_p, qt_size_p, ds_default_p);
			end if;
	end append_text;


    /** 
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	NAIS MLA Common Header
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	**/

    procedure nais_common_header (
        message_type_p    varchar2,
        nr_prescricao_p     number,
        cd_classif_p            varchar2,
        cd_contin_flag_p    varchar2,
        cd_estab_p             varchar2,
        nr_tele_length_p     number 
    ) is

        cursor c01 is
        select
            'H'                             system_code,
            message_type_p     message_type,
            cd_contin_flag_p     continuation_flag,
            'M'                             destination_code,
            'D'                             origin_code,
            sysdate                     processing_date,
            sysdate                     processing_time,
            ' '                               workstation_name,
            ' '                               user_number,
            cd_classif_p             processing_classification,
            ' ' response_type,
            nr_tele_length_p   message_length, 
            ' ' eot,
            ' ' medical_institution_code,
            ' ' blank
        from
            dual;

    begin
        ds_line_w := null;
        nm_usuario_w := nvl(wheb_usuario_pck.get_nm_usuario, nm_usuario_w);
        generate_int_serial_number(nr_prescricao_p, message_type_p, cd_estab_p, nm_usuario_w, filename_sequence_w, 930);
        for r_c01 in c01 loop begin
            append_text(to_char(filename_sequence_w), 5, 'L', '0');
            append_text(r_c01.system_code, 1, 'R', ' ');
            append_text(r_c01.message_type, 2, 'R', ' ');
            append_text(r_c01.continuation_flag, 1, 'R', ' ');
            append_text(r_c01.destination_code, 1, 'R', ' ');
            append_text(r_c01.origin_code, 1, 'R', ' ');
            append_text(to_char(r_c01.processing_date, 'YYYYMMDD'), 8, 'L', '0');
            append_text(to_char(r_c01.processing_time, 'HH24MISS'), 6, 'L', '0');
            append_text(r_c01.workstation_name, 8, 'R', ' ');
            append_text(r_c01.user_number, 8, 'R', ' ');
            append_text(r_c01.processing_classification, 2, 'R', ' ');
            append_text(r_c01.response_type, 2, 'R', ' ');
            append_text(r_c01.message_length, 5, 'L', '0');
            append_text(r_c01.eot, 1, 'R', ' ');
            append_text(r_c01.medical_institution_code, 2, 'R', ' ');
            append_text(r_c01.blank, 11, 'R', ' ');
        end;
        end loop;

    end nais_common_header;  

    /** 
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	NAIS MLA Common Accounting information
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	**/

    procedure common_accounting_info (
        r_c01 accinforectyp
    ) as
    begin
        append_text(r_c01.process_classif, 2, 'L', '0');
        append_text(r_c01.order_class, 2, 'L', ' ');
        append_text(to_char(r_c01.order_number), 8, 'L', '0');
        append_text(r_c01.patient_identifier, 10, 'L', '0');
        append_text(r_c01.consultation_date, 8, 'L', '0');
        append_text(r_c01.department_code, 2, 'L', '0');
        append_text(r_c01.patient_classification, 1, 'L', ' ');
        append_text(r_c01.ward_code, 3, 'R', ' ');
        append_text(r_c01.room_number, 5, 'R', ' ');
        append_text(r_c01.blood_tranfusion_type, 1, 'L');
        append_text(r_c01.insurance_number, 2, 'L', '0');
        append_text(r_c01.doctor_code, 8, 'L', '0');
        append_text(r_c01.patient_newborn, 1, 'L');
        append_text(r_c01.prescription_classfication, 1, 'R', ' ');
        append_text(r_c01.requestor_code, 3, 'L', '0');
        append_text(r_c01.mutual_exclusion_rule, 1, 'R', ' ');
        append_text(r_c01.division_number, 4, 'L', '0'); 
        append_text(r_c01.blank_one, 1, 'L');
        append_text(r_c01.blank_two, 1, 'L');
    end common_accounting_info; 

    /** 
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	NAIS MLA Common Execution information
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	**/

    procedure common_execution_info (
        r_c02 execinforedtyp
    ) as
    begin
        append_text(r_c02.execution_classification, 1, 'L');
        append_text(r_c02.execution_date, 8, 'L', '0');
        append_text(r_c02.execution_time, 4, 'L', '0');
        append_text(r_c02.execution_op_code, 3, 'L');
        append_text(r_c02.execution_up_flag, 1, 'L');
        append_text(r_c02.execution_weight, 6, 'L');
        append_text(r_c02.out_of_hours_flag, 1, 'L');
        append_text(r_c02.execution_blank, 8, 'R', ' ');
    end common_execution_info;

     /** 
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	NAIS MLA Common Medical treatment details information
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	**/

    procedure common_med_treatment_info (
        r_c03 medtreatmentinforedtyp
    ) as
        medical_affair_code NAIS_CONVERSION_MASTER%rowtype;
    begin

       
        if ( r_c03.nr_data_class = '04' ) then
        
            append_text(r_c03.nr_data_class, 2, 'L', '0');
            append_text(r_c03.days, 2, 'L', '0');
            append_text(r_c03.times_num_04, 2, 'L', '0');
            append_text(r_c03.days_num, 2, 'L', '0');
            append_text(r_c03.digit_times_num, 3, 'L', '0');
            append_text(r_c03.digit_days_num, 3, 'L', '0');
            append_text(' ', 50);
        else         
            
            append_text(r_c03.nr_data_class, 2, 'L', '0');           
            IF substr(r_c03.internal_code,1,1)  <> '.'  and substr(r_c03.internal_code,1,1)  <> '@'  then
            medical_affair_code :=get_medicalaffair_code('RR','rp_implemen_item_reab','NR_SEQUENCIA',r_c03.internal_code,null,null);
            append_text(medical_affair_code.CD_MEDICAL_AFFAIR, 6, 'L', '0');
            else
            append_text(r_c03.internal_code, 6, 'L', '0');
            end if;
            append_text(nvl(r_c03.dosage,'0'), 9, 'L', '0');
            append_text(nvl(r_c03.unit,' '), 2, 'L', ' ');
            append_text(nvl(r_c03.times_num,'0'), 2, 'L', '0');
            append_text(nvl(r_c03.free_input_flag, ' '), 1, 'L', ' ');
            append_text(nvl(r_c03.free_comments, ' '), 40, 'R', ' ');
            append_text(nvl(r_c03.med_change_impossible_flg, ' '), 1, 'R', ' ');
            append_text(nvl(r_c03.general_name_med_flg,' '), 1, 'R', ' ');
        end if;
    end common_med_treatment_info;
  
    
     /** 
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	json_
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	**/
    
    procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			varchar2) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;

/** 
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	NAIS MLA Event - REHABILITATION Test Starts Here
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	**/
        
      procedure send_rehabilitation_data (
       nr_prescricao_p    number,
        cd_classif_p       varchar2,
        file_output_p       out  clob  
    ) as
     countitemrow varchar2(30);
     loopcount integer(30);
     countr pls_integer:=0;

       cursor c01 return accinforectyp is
                select
            cd_classif_p     process_classif, 
            'R0'     order_class,              
            b.nr_prescricao   order_number,            
            nvl(i.PATIENT_ID,'0000131033')       patient_identifier,        
            nvl(to_char(g.DT_START_TIME, 'YYYYMMDD'),'20200201') consultation_date,
            nvl(i.department_id,'05') department_code,           -- Mandatory (2)
            decode(get_patient_type(i.encounter_id, NULL), 'IN', 1, 'OP', 2) patient_classification, 
            nvl(i.bed_id,'95 ') ward_code,                
            i.room_id room_number,            
            '1' blood_tranfusion_type,    
            nvl(a.NR_SEQ_NAIS_INSURANCE,'00') insurance_number,       
            nvl(a.cd_medico,'001794  ') doctor_code,              
            '0' patient_newborn,        
            ' ' prescription_classfication,
            e.NM_USUARIO requestor_code,          
            ' ' mutual_exclusion_rule,     
            '0123' division_number,          
            ' ' blank_one,              
            ' ' blank_two                  
            from    cpoe_procedimento a, 
            prescr_procedimento b, 
            rp_tratamento c, 
            prescr_medica d, 
            rp_pac_agend_individual e, 
            agenda_consulta f, 
            rp_implementation_reab g,            
            bft_encounter_v i
            where   a.nr_sequencia = b.nr_seq_proc_cpoe
        and     a.nr_atendimento =  i.encounter_id
        and     b.nr_prescricao = d.nr_prescricao
        and     c.nr_seq_cpoe_procedimento = a.nr_sequencia
        and     e.nr_seq_tratamento = c.nr_sequencia
        and     f.nr_seq_rp_item_ind = e.nr_sequencia
        and     g.nr_seq_impl_agenda = f.nr_sequencia
	    and     g.ie_exec_status='S' and g.ie_info_acc='S'
        and     b.nr_prescricao = nr_prescricao_p
        order by dt_agenda desc; 
        
    --  End of Accounting Information

        cursor c02 return execInfoRedTyp is
        select
            '0' execution_classification,   -- Not Used  (1)
            nvl(to_char(g.DT_START_TIME, 'YYYYMMDD'),'20200201') execution_date,            
            nvl(to_char(g.DT_START_TIME,'hhmi'),'1234') execution_time,            
            '000' execution_op_code,         
            '0' execution_up_flag,       
            '000000' execution_weight,        
            '0' out_of_hours_flag,       
            ' ' execution_blank            
            from    cpoe_procedimento a,
            prescr_procedimento b, 
            rp_tratamento c,
            prescr_medica d,
            rp_pac_agend_individual e, 
            agenda_consulta f, 
            rp_implementation_reab g
            where   a.nr_sequencia = b.nr_seq_proc_cpoe
        and     b.nr_prescricao = d.nr_prescricao
        and     c.nr_seq_cpoe_procedimento = a.nr_sequencia
        and     e.nr_seq_tratamento = c.nr_sequencia
        and     f.nr_seq_rp_item_ind = e.nr_sequencia
        and     g.nr_seq_impl_agenda = f.nr_sequencia
        and     b.nr_prescricao = nr_prescricao_p
        order by dt_agenda desc; 
         
    cursor c03 return medTreatmentInfoRedTyp  is 
    select
            count(*) over() count_medi_details,            
            '000' slip_code,                 
            '01' nr_data_class,              -- Mandatory (2) 
            h.nr_sequencia internal_code,              --  Mandatory(6)
            h.NR_UNIT dosage,                     -- Mandatory (9)
            ' ' unit,                       -- Optional  (2)
            '01' times_num,                  -- Mandatory(2)
            '0' free_input_flag,            -- Mandatory(1) , 0 : no free comments , 1 : free comments
            ' ' free_comments,              -- Optional (40)
            ' ' med_change_impossible_flg,  -- Optional (1)
            ' ' general_name_med_flg,       -- Optional (1)
            '00' days,                       --Optional (2) , if  data_class_no = '04'
            '00' times_num_04,               -- Mandatory (2) , if  data_class_no = '04'
            '00' days_num,                   -- Mandatory (2), if  data_class_no = '04'
            '001' digit_times_num,            -- Mandatory (3), if  data_class_no = '04'
            '001' digit_days_num,             -- Mandatory (3), if  data_class_no = '04'
            ' ' medi_blank,                 -- ALL SPACE (4)
            ' ' eot                         -- ALL SPACE (1)
            from            cpoe_procedimento a, 
                            prescr_procedimento b,
                            rp_tratamento c,
                            prescr_medica d,
                            rp_pac_agend_individual e, 
                            agenda_consulta f, 
                            rp_implementation_reab g, 
                            rp_implemen_item_reab h,
                            proc_interno pi, 
                            prescr_medica m,  
                            cpoe_order_unit co,
                            cpoe_tipo_pedido ct
            where           a.nr_sequencia = b.nr_seq_proc_cpoe
            and             b.nr_prescricao = d.nr_prescricao
            and             c.nr_seq_cpoe_procedimento = a.nr_sequencia
            and             e.nr_seq_tratamento = c.nr_sequencia
            and             f.nr_seq_rp_item_ind = e.nr_sequencia
            and             g.nr_seq_impl_agenda = f.nr_sequencia
            and             g.nr_seq_tratamento = c.nr_sequencia
            and             h.nr_seq_impl_reab = g.nr_sequencia
            and             b.nr_prescricao = nr_prescricao_p
            and             g.ie_exec_status='S' and g.ie_info_acc='S'   
            and             b.nr_prescricao = m.nr_prescricao
            and             co.nr_seq_cpoe_tipo_pedido   = ct.nr_sequencia 
            and             co.nr_sequencia              = a.nr_seq_cpoe_order_unit
            and             obter_conversao_externa_int(null,'PROC_INTERNO_CLASSIF','NR_SEQUENCIA', pi.nr_seq_classif, 'NAIS' ) = 'R0'
            and             ct.nr_seq_sub_grp            = 'P'  
            and             pi.nr_sequencia = a.nr_seq_proc_interno 
            order by h.nr_sequencia;


        r_c03                 c03%rowtype;
        r_c01_w               accinforectyp;
        r_c02_w               execinforedtyp;
        r_c03_w               medtreatmentinforedtyp;
        cd_contin_flag_w      varchar(1) := 'C';
        loop_count_w          number(3) := 0;
        inside_loop_count_w   number(3) := 1;
        counter_w               number(2) := 0;
        json_output_w		    philips_json;
        json_output_list_w  philips_json_list;
        
         
    begin 
    
    ds_line_w := null;
        counter_w := 0;
        inside_loop_count_w :=0;
        index_counter_w := 0;
        loop_count_w := 0;
       json_output_w   := philips_json();
       json_output_list_w	:= philips_json_list();
        open c03;
            
        loop
            fetch c03 into r_c03_w;
             
            exit when c03%notfound;
            
            if  c03%rowcount = 1 then
            get_action_code_med_detail('R0', r_c03_w, ds_line_w);
            add_med_treatment_detail_array(ds_line_w, index_counter_w);
            end if;
            
            common_med_treatment_info(r_c03_w);
            add_med_treatment_detail_array(ds_line_w, index_counter_w);             

        end loop;
            add_date_class_med_detail('R0', r_c03_w, ds_line_w);
            add_med_treatment_detail_array(ds_line_w, index_counter_w);
        close c03;
        loop_count_w := ceil(index_counter_w / 10);

        
        
        for i in 1..loop_count_w loop begin
            if ( i = loop_count_w ) then
                cd_contin_flag_w := 'E';
            end if;
            nais_common_header('KK', nr_prescricao_p, cd_classif_p, cd_contin_flag_w, 1,
                               810);
            open c01;
            loop
                fetch c01 into r_c01_w;
                exit when c01%notfound;
                common_accounting_info(r_c01_w);
            end loop;

            close c01;
            open c02;
            loop
                fetch c02 into r_c02_w;
                exit when c02%notfound;
                common_execution_info(r_c02_w);
            end loop;

            close c02;
            open c03;
            fetch c03 into r_c03;
            append_text(r_c03.slip_code, 3, 'L');
            append_text(nvl(r_c03.count_medi_details, 0), 2, 'L', '0');
            close c03;
            inside_loop_count_w := counter_w + 1;
            for x in inside_loop_count_w..inside_loop_count_w + 9 loop
                counter_w := counter_w + 1;
                
                med_treamtent.extend;
                if ( cd_contin_flag_w = 'E' and med_treamtent(x) is null ) then
                    append_text(' ', 64);
                else
                    append_text(med_treamtent(x), 64, 'L', ' ');
                end if;

            end loop;

            open c03;
            fetch c03 into r_c03;
            append_text(r_c03.medi_blank, 4, 'R', ' ');
            append_text(r_c03.eot, 1, 'L');
            close c03;             
           add_json_value(json_output_w, 'message', ds_line_w);   
           json_output_list_w.append(json_output_w.to_json_value());
              
        end;

        end loop;        
        dbms_lob.createtemporary( file_output_p, true);
        json_output_list_w.to_clob(file_output_p);
    end send_rehabilitation_data;
     
     
     procedure get_action_code_med_detail (
        messagetype_p   varchar2,
        record_p        medtreatmentinforedtyp,
        message_data    out varchar2
    ) as
        record_w     medtreatmentinforedtyp;
        actioncode   varchar2(10);
    begin
        ds_line_w := null;
        record_w := record_p;
        record_w.nr_data_class := '01';
        case
            when ( messagetype_p = 'R0' ) then
                
                record_w.internal_code := rpad('.8000', 6, ' ');
          
        end case;

        record_w.dosage := '000001000';
        record_w.unit := ' ';
        record_w.free_comments := ' ';
        common_med_treatment_info(record_w);
        message_data := ds_line_w;
    end get_action_code_med_detail;
    
    procedure add_date_class_med_detail (
        messagetype_p   varchar2,
        record_p        medtreatmentinforedtyp,
        message_data    out varchar2
    ) as
        record_w medtreatmentinforedtyp;
    begin
        ds_line_w := null;
        record_w := record_p;
        record_w.nr_data_class := '04';
        common_med_treatment_info(record_w);
        message_data := ds_line_w;
    end add_date_class_med_detail;
    
    procedure add_med_treatment_detail_array (
        message_p   varchar2,
        index_p     number
    ) as
    begin
        med_treamtent.extend;
        index_counter_w := index_p + 1;
        med_treamtent(index_counter_w) := message_p;
        ds_line_w := null;
    end add_med_treatment_detail_array;
END NAIS_MLA_REHABILITATION;
/
