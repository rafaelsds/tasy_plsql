create or replace package Calcula_regra_tx_pck as
	
	procedure 	set_calcula_regra_tx(ie_calcula_regra_tx_p varchar2);
	function 	get_calcula_regra_tx return varchar2;
	
end Calcula_regra_tx_pck;
/
create or replace package body Calcula_regra_tx_pck as
	ie_calcula_regra_tx_w varchar2(1):= 'S';

	procedure set_calcula_regra_tx (ie_calcula_regra_tx_p varchar2) as
	begin
		ie_calcula_regra_tx_w := ie_calcula_regra_tx_p;
	end;
	
	function get_calcula_regra_tx return varchar2 as
	begin
		return ie_calcula_regra_tx_w;
	end;	

end Calcula_regra_tx_pck;
/