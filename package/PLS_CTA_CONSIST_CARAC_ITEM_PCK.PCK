create or replace
package pls_cta_consist_carac_item_pck as 

--Vari�veis de acesso interno e externo.

-- assinatura de functions e procedures publicas de acesso externo
procedure gerencia_liberacao_itens(	nr_seq_lote_prot_p	pls_lote_protocolo_conta.nr_sequencia%type,
					nr_seq_protocolo_p	pls_protocolo_conta.nr_sequencia%type,
					nr_seq_conta_p		pls_conta.nr_sequencia%type,
					nr_seq_lote_processo_p	pls_cta_lote_processo.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type);
					
procedure limpar_itens_liberados(	nr_seq_conta_p	pls_conta.nr_sequencia%type);

end pls_cta_consist_carac_item_pck;
/

create or replace
package body pls_cta_consist_carac_item_pck as 

type dados_regra is record (
	nr_sequencia		pls_lib_item_regra.nr_sequencia%type,
	nr_seq_regra_grupo	pls_lib_item_regra_grupo.nr_sequencia%type,
	nr_seq_tipo_acomodacao	pls_lib_item_regra.nr_seq_tipo_acomodacao%type,
	qt_procedimento_lib	pls_integer
);

type dados_parametros is record (
	nr_seq_lote		pls_lote_protocolo_conta.nr_sequencia%type,
	nr_seq_protocolo	pls_protocolo_conta.nr_sequencia%type,
	nr_seq_conta		pls_conta.nr_sequencia%type,
	nr_seq_lote_processo	pls_cta_lote_processo.nr_sequencia%type,
	cd_estabelecimento	estabelecimento.cd_estabelecimento%type,
	nm_usuario		usuario.nm_usuario%type
);

type dados_restricao is record (
	ds_restricao_lote		varchar2(4000),
	ds_restricao_protocolo		varchar2(4000),
	ds_restricao_conta		varchar2(4000),
	ds_restricao_lote_processo	varchar2(4000),
	ds_restricao_proc		varchar2(4000),
	ds_campos_sql			varchar2(4000),
	ds_campos_minus			varchar2(4000),
	ds_restricao_minus		varchar2(4000)
);

dados_parametros_w	dados_parametros;

tb_proc_w		dbms_sql.number_table;
tb_mat_w		dbms_sql.number_table;
tb_regra_w		dbms_sql.number_table;
tb_regra_carac_w	dbms_sql.number_table;
tb_liberado_w		dbms_sql.varchar2_table;

num_tb_vazia_w		dbms_sql.number_table;
vchr2_tb_vazia_w	dbms_sql.varchar2_table;

qt_reg_transacao_w	pls_integer := 1000;

procedure tratar_erro(	ds_sql_p	varchar2,
			dados_regra_p	dados_regra) is

ds_erro_w	varchar2(4000);
ds_trace_w	varchar2(4000);

begin

ds_trace_w := dbms_utility.format_call_stack;

-- Desfazer as altera��es feitas.
rollback;

-- Montar a descri��o do erro.
ds_erro_w :=	'Erro: ' || sqlerrm || pls_util_pck.enter_w || 
		pls_util_pck.enter_w || 
		'SQL executado: ' || pls_util_pck.enter_w || 
		ds_sql_p || pls_util_pck.enter_w || 
		pls_util_pck.enter_w || 
		'Stack: ' || pls_util_pck.enter_w || 
		ds_trace_w;
		
-- Exibir a mensagem de erro: 
--Ocorreu um erro ao efetuar a verifica��o da regra de Caracter�siticas da conta X Caracter�siticas do item n� #@REGRA#@. Favor entrar em contato com o suporte a apresentar a seguinte mensagem:

--#@ERRO#@.
wheb_mensagem_pck.exibir_mensagem_abort(268334, 'REGRA=' || dados_regra_p.nr_sequencia || ';ERRO=' || ds_erro_w, -20012);

end tratar_erro;

procedure limpar_listas is
begin

tb_proc_w		:= num_tb_vazia_w;
tb_mat_w		:= num_tb_vazia_w;
tb_regra_w		:= num_tb_vazia_w;
tb_regra_carac_w	:= num_tb_vazia_w;
tb_liberado_w		:= vchr2_tb_vazia_w;

end limpar_listas;

procedure atualizar_listas is
begin

forall i in tb_regra_w.first..tb_regra_w.last
	update	pls_conta_proc_regra_lib
	set	ie_liberado		= tb_liberado_w(i),
		nm_usuario_nrec		= nm_usuario,
		dt_atualizacao_nrec	= dt_atualizacao,
		nm_usuario		= dados_parametros_w.nm_usuario,
		dt_atualizacao		= sysdate
	where	nr_seq_regra_lib	= tb_regra_w(i)
	and	nr_seq_regra_carac	= tb_regra_carac_w(i)
	and	nr_seq_conta_proc	= tb_proc_w(i);

limpar_listas;

end atualizar_listas;

function obter_restricao_regra(	ie_opcao_p	varchar2,
				nr_cursor_p	pls_integer,
				ie_proc_mat_p	varchar2,
				dados_regra_p	dados_regra)
				return dados_restricao is

dados_restricao_w	dados_restricao;

begin

-------------------------------------------------------------------------------------------- CAMPOS -------------------------------------------------------------------------------------------------------------------
-- Montar os campos, est� sendo feito nesta parte para poder montar o minus, que n�o ir� permitir gravar um proceidmento que j� foi liberado para a regra passada.
if	(ie_opcao_p = 'RESTRICAO') then

	if	(ie_proc_mat_p = 'P') then

		dados_restricao_w.ds_campos_sql :=	dados_restricao_w.ds_campos_sql || pls_util_pck.enter_w || 
							'	proc.nr_sequencia nr_seq_conta_proc, ' || pls_util_pck.enter_w || 
							'	null nr_seq_conta_mat, ' || pls_util_pck.enter_w || 
							'	:nr_seq_regra_lista, ' || pls_util_pck.enter_w || 
							'	''N'' ie_liberado, ' || pls_util_pck.enter_w || 
							'	:nr_seq_regra_carac ';
	end if;
else
	dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_regra_lista', dados_regra_p.nr_seq_regra_grupo);
	dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_regra_carac', dados_regra_p.nr_sequencia);
end if;

-------------------------------------------------------------------------------------------- PARAMETROS -------------------------------------------------------------------------------------------------------------------
-- Restri��o quando for passado o lote por par�metro.
if	(dados_parametros_w.nr_seq_lote is not null) then

	if	(ie_opcao_p = 'RESTRICAO') then
	
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_lote :=	dados_restricao_w.ds_restricao_lote || pls_util_pck.enter_w ||
								'and	proc.nr_seq_lote_conta = :nr_seq_lote ';
		end if;
	else
		dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_lote', dados_parametros_w.nr_seq_lote);
	end if;
end if;

-- Realizar o acesso aos procedimentos do protocolo passado por par�metro.
if	(dados_parametros_w.nr_seq_protocolo is not null) then

	if	(ie_opcao_p = 'RESTRICAO') then
	
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_protocolo :=	dados_restricao_w.ds_restricao_protocolo || pls_util_pck.enter_w ||
									'and	proc.nr_seq_protocolo = :nr_seq_protocolo ';
		end if;
	else
		dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_protocolo', dados_parametros_w.nr_seq_protocolo);
	end if;
end if;

-- Realizar o acesso aos procedimentos pela conta passada por par�metro.
if	(dados_parametros_w.nr_seq_conta is not null) then
	
	if	(ie_opcao_p = 'RESTRICAO') then
	
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_conta :=	dados_restricao_w.ds_restricao_conta || pls_util_pck.enter_w ||
								'and	proc.nr_seq_conta = :nr_seq_conta ';
		end if;
	else
		dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_conta', dados_parametros_w.nr_seq_conta);
	end if;
end if;

-- Realizar o acesso aos procedimentos pela conta passada por par�metro.
if	(dados_parametros_w.nr_seq_lote_processo is not null) then
	
	if	(ie_opcao_p = 'RESTRICAO') then
		
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_conta :=	dados_restricao_w.ds_restricao_conta || pls_util_pck.enter_w || 
								'and     exists (	select 1 ' || pls_util_pck.enter_w || 
								'			from   pls_cta_lote_proc_conta processo ' || pls_util_pck.enter_w || 
								'			where  processo.nr_seq_lote_processo = :nr_seq_lote_processo ' || pls_util_pck.enter_w || 
								'			and    processo.nr_seq_conta = proc.nr_seq_conta)' ;
		end if;
	else	
		dbms_sql.bind_variable(nr_cursor_p, ':nr_seq_lote_processo', dados_parametros_w.nr_seq_lote_processo);
	end if;
end if;

-------------------------------------------------------------------------------------------- REGRA ----------------------------------------------------------------------------------------------------------------------
-- Se for uma regra por tipo de acomoda��o ent�o verifica apenas as contas do tipo de acomoda��o da regra.
if	(dados_regra_p.nr_seq_tipo_acomodacao is not null) then
	
	if	(ie_opcao_p = 'RESTRICAO') then
		
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_conta :=	dados_restricao_w.ds_restricao_conta || pls_util_pck.enter_w || 
								'and	proc.nr_seq_tipo_acomodacao = :nr_seq_tipo_acomodacao ';
		end if;
	else	
		dbms_sql.bind_variable(nr_cursor_p, ':nr_seq_tipo_acomodacao', dados_regra_p.nr_seq_tipo_acomodacao);
	end if;
end if;

-- Montar a restri��o pelos itens que est�o liberados para esta regra.
if	(ie_opcao_p = 'RESTRICAO') then
		
	if	(ie_proc_mat_p = 'P') then
	
		dados_restricao_w.ds_restricao_proc :=	dados_restricao_w.ds_restricao_proc || pls_util_pck.enter_w || 
							'and	not exists ( ' || pls_util_pck.enter_w || 
							'		select	1 ' || pls_util_pck.enter_w ||
							'		from	pls_proced_lib_regra_carac_v itens_lib ' || pls_util_pck.enter_w ||
							'		where	itens_lib.nr_seq_regra = :nr_seq_regra ' || pls_util_pck.enter_w ||
							'		and	itens_lib.ie_origem_proced = proc.ie_origem_proced ' || pls_util_pck.enter_w ||
							'		and	itens_lib.cd_procedimento = proc.cd_procedimento ' || pls_util_pck.enter_w ||
							'	) ' ;
	end if;
else	
	dbms_sql.bind_variable(nr_cursor_p, ':nr_seq_regra', dados_regra_p.nr_sequencia);
end if;

return dados_restricao_w;

end obter_restricao_regra;

procedure processar_procedimentos(	dados_regra_p	dados_regra) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Ler a regra passada por par�metro e process�-la, gravando o v�nculo entre as 
	regras e os procedimentos liberados na tabela PLS_CONTA_PROC_REGRA_LIB.
	Ir� buscar os procedimentos da conta ou protocolo ou ainda de um lote inteiro,
	conforme passado por par�metro.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 629123 - 11/11/2013 -  Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_sql_w		varchar2(4000);

var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;

dados_restricao_w	dados_restricao;

begin

-- Obter a restri��o que ser� montada para buscar os procedimentos liberados para a regra.
dados_restricao_w := obter_restricao_regra('RESTRICAO', var_cur_w, 'P', dados_regra_p);

-- S� faz o select na PLS_CONTA_PROC_V  se tiver alguma restric��o montada. Se n�o existir significa que deu algum problema na montagem da restri��o.
if	((dados_restricao_w.ds_restricao_lote is not null or
	dados_restricao_w.ds_restricao_protocolo is not null or
	dados_restricao_w.ds_restricao_conta is not null or
	dados_restricao_w.ds_restricao_lote_processo is not null or
	dados_restricao_w.ds_restricao_proc is not null) and
	dados_restricao_w.ds_campos_sql is not null) then
	
	-- Montar o select com as restri��es da regra.
	ds_sql_w :=	'select ' || dados_restricao_w.ds_campos_sql || pls_util_pck.enter_w ||
			'from	pls_conta_proc_v proc ' || pls_util_pck.enter_w ||
			'where	1 = 1 ' || 
			dados_restricao_w.ds_restricao_lote ||
			dados_restricao_w.ds_restricao_protocolo ||
			dados_restricao_w.ds_restricao_conta ||
			dados_restricao_w.ds_restricao_lote_processo ||
			dados_restricao_w.ds_restricao_proc;
	
	var_cur_w := dbms_sql.open_cursor;
	begin
		-- Fazer o parse no comando
		dbms_sql.parse(var_cur_w, ds_sql_w, 1);
		
		-- Atualizar o valor das binds para executar o comando.
		dados_restricao_w := obter_restricao_regra('BINDS', var_cur_w, 'P', dados_regra_p);
		
		-- Defifnir que o retorno do select ser� gravado em listas.
		dbms_sql.define_array(var_cur_w, 1, tb_proc_w, qt_reg_transacao_w, 1);
		dbms_sql.define_array(var_cur_w, 2, tb_mat_w, qt_reg_transacao_w, 1);
		dbms_sql.define_array(var_cur_w, 3, tb_regra_w, qt_reg_transacao_w, 1);
		dbms_sql.define_array(var_cur_w, 4, tb_liberado_w, qt_reg_transacao_w, 1);
		dbms_sql.define_array(var_cur_w, 5, tb_regra_carac_w, qt_reg_transacao_w, 1);
		
		-- Executar o comando.
		var_exec_w := dbms_sql.execute(var_cur_w);
		loop
		-- O fetch rows ir� preencher os buffers do Oracle com as linhas que ser�o passadas para a lista quando o COLUMN_VALUE for chamado.
		var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
		
			-- Zerar as listas a cada execu��o. para que o mesmo registro n�o seja gravado novamente na tabela.
			limpar_listas;
			
			-- Obter as listas que foram populadas.
			dbms_sql.column_value(var_cur_w, 1, tb_proc_w);
			dbms_sql.column_value(var_cur_w, 2, tb_mat_w);
			dbms_sql.column_value(var_cur_w, 3, tb_regra_w);
			dbms_sql.column_value(var_cur_w, 4, tb_liberado_w);
			dbms_sql.column_value(var_cur_w, 5, tb_regra_carac_w);
			
			-- Grava as listas no banco em um �nico comando DML.
			atualizar_listas;
			
			-- Quando n�mero de linhas que foram aplicadas no array for diferente do definido significa que esta foi a �ltima itera��o do loop e que todas as linhas foram
			-- passadas.
			exit when var_retorno_w != qt_reg_transacao_w;
		end loop; -- Contas filtradas
		dbms_sql.close_cursor(var_cur_w);
	exception
		when others then
		
		-- Fechar os cursores que continuam abertos, os cursores que utilizam FOR - LOOP n�o necessitam serem fechados, ser�o fechados autom�ticamente.
		dbms_sql.close_cursor(var_cur_w);
		
		-- Formata a mensagem de erro que ser� gerada para o usu�rio.
		tratar_erro(ds_sql_w, dados_regra_p);
	end;
end if;
end processar_procedimentos;


function obter_restricao_alimentacao(	ie_opcao_p	varchar2,
					nr_cursor_p	pls_integer,
					ie_proc_mat_p	varchar2,
					dados_regra_p	dados_regra)
					return dados_restricao is

dados_restricao_w	dados_restricao;

begin

-------------------------------------------------------------------------------------------- CAMPOS -------------------------------------------------------------------------------------------------------------------
-- Montar os campos, est� sendo feito nesta parte para poder montar o minus, que n�o ir� permitir gravar um proceidmento que j� foi liberado para a regra passada.
if	(ie_opcao_p = 'RESTRICAO') then

	if	(ie_proc_mat_p = 'P') then

		dados_restricao_w.ds_campos_sql :=	dados_restricao_w.ds_campos_sql || pls_util_pck.enter_w || 
							'	proc.nr_sequencia nr_seq_conta_proc, ' || pls_util_pck.enter_w || 
							'	null nr_seq_conta_mat, ' || pls_util_pck.enter_w || 
							'	:nr_seq_regra_lista,  ' || pls_util_pck.enter_w || 
							'	:nr_seq_regra_carac ';
	end if;
else
	dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_regra_lista', dados_regra_p.nr_seq_regra_grupo);
	dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_regra_carac', dados_regra_p.nr_sequencia);
end if;

-------------------------------------------------------------------------------------------- PARAMETROS -------------------------------------------------------------------------------------------------------------------
-- Restri��o quando for passado o lote por par�metro.
if	(dados_parametros_w.nr_seq_lote is not null) then

	if	(ie_opcao_p = 'RESTRICAO') then
	
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_lote :=	dados_restricao_w.ds_restricao_lote || pls_util_pck.enter_w ||
								'and	proc.nr_seq_lote_conta = :nr_seq_lote ';
		end if;
	else
		dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_lote', dados_parametros_w.nr_seq_lote);
	end if;
end if;

-- Realizar o acesso aos procedimentos do protocolo passado por par�metro.
if	(dados_parametros_w.nr_seq_protocolo is not null) then

	if	(ie_opcao_p = 'RESTRICAO') then
	
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_protocolo :=	dados_restricao_w.ds_restricao_protocolo || pls_util_pck.enter_w ||
									'and	proc.nr_seq_protocolo = :nr_seq_protocolo ';
		end if;
	else
		dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_protocolo', dados_parametros_w.nr_seq_protocolo);
	end if;
end if;

-- Realizar o acesso aos procedimentos pela conta passada por par�metro.
if	(dados_parametros_w.nr_seq_conta is not null) then
	
	if	(ie_opcao_p = 'RESTRICAO') then
	
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_conta :=	dados_restricao_w.ds_restricao_conta || pls_util_pck.enter_w ||
								'and	proc.nr_seq_conta = :nr_seq_conta ';
		end if;
	else
		dbms_sql.bind_variable( nr_cursor_p, ':nr_seq_conta', dados_parametros_w.nr_seq_conta);
	end if;
end if;

-- Realizar o acesso aos procedimentos pela conta passada por par�metro.
if	(dados_parametros_w.nr_seq_lote_processo is not null) then
	
	if	(ie_opcao_p = 'RESTRICAO') then
		
		if	(ie_proc_mat_p = 'P') then
		
			dados_restricao_w.ds_restricao_conta :=	dados_restricao_w.ds_restricao_conta || pls_util_pck.enter_w || 
								'and     exists (select 1 ' || pls_util_pck.enter_w || 
								'		from   pls_cta_lote_proc_conta processo ' || pls_util_pck.enter_w || 
								'		where  processo.nr_seq_lote_processo = :nr_seq_lote_processo ' || pls_util_pck.enter_w || 
								'		and    processo.nr_seq_conta = proc.nr_seq_conta)' ;
		end if;
	else	
		dbms_sql.bind_variable(nr_cursor_p, ':nr_seq_lote_processo', dados_parametros_w.nr_seq_lote_processo);
	end if;
end if;

return dados_restricao_w;

end obter_restricao_alimentacao;

procedure gravar_listas is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Gravar as listas que est�o na mem�ria na tabela para fazer a vincula��o entre procedimento e regra.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 629123 - 11/11/2013 -  Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

forall i in tb_regra_w.first..tb_regra_w.last
	insert into pls_conta_proc_regra_lib
		(	nr_sequencia, nr_seq_conta_proc, nr_seq_regra_lib, nr_seq_regra_carac,
			ie_liberado, nm_usuario, dt_atualizacao)
	values	(	pls_conta_proc_regra_lib_seq.nextval, tb_proc_w(i), tb_regra_w(i), tb_regra_carac_w(i),
			'S', dados_parametros_w.nm_usuario, sysdate);

limpar_listas;

end gravar_listas;

procedure buscar_procedimentos(	dados_regra_p	dados_regra) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Ler a regra passada por par�metro e process�-la, gravando o v�nculo entre as 
	regras e os procedimentos liberados na tabela PLS_CONTA_PROC_REGRA_LIB.
	Ir� buscar os procedimentos da conta ou protocolo ou ainda de um lote inteiro,
	conforme passado por par�metro.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 629123 - 11/11/2013 -  Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_sql_w		varchar2(4000);

var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;

dados_restricao_w	dados_restricao;

begin

-- Obter a restri��o que ser� montada para buscar os procedimentos liberados para a regra.
dados_restricao_w := obter_restricao_alimentacao('RESTRICAO', var_cur_w, 'P', dados_regra_p);

-- S� faz o select na PLS_CONTA_PROC_V  se tiver alguma restric��o montada. Se n�o existir significa que deu algum problema na montagem da restri��o.
if	((dados_restricao_w.ds_restricao_lote is not null or
	dados_restricao_w.ds_restricao_protocolo is not null or
	dados_restricao_w.ds_restricao_conta is not null or
	dados_restricao_w.ds_restricao_lote_processo is not null or
	dados_restricao_w.ds_restricao_proc is not null) and
	dados_restricao_w.ds_campos_sql is not null) then
	
	-- Montar o select com as restri��es da regra.
	ds_sql_w :=	'select ' || dados_restricao_w.ds_campos_sql || pls_util_pck.enter_w ||
			'from	pls_conta_proc_v proc ' || pls_util_pck.enter_w ||
			'where	1 = 1 ' || 
			dados_restricao_w.ds_restricao_lote ||
			dados_restricao_w.ds_restricao_protocolo ||
			dados_restricao_w.ds_restricao_conta ||
			dados_restricao_w.ds_restricao_lote_processo ||
			dados_restricao_w.ds_restricao_proc;
	
	var_cur_w := dbms_sql.open_cursor;
	begin
		-- Fazer o parse no comando
		dbms_sql.parse(var_cur_w, ds_sql_w, 1);
		
		-- Atualizar o valor das binds para executar o comando.
		dados_restricao_w := obter_restricao_alimentacao('BINDS', var_cur_w, 'P', dados_regra_p);
		
		-- Defifnir que o retorno do select ser� gravado em listas.
		dbms_sql.define_array(var_cur_w, 1, tb_proc_w, qt_reg_transacao_w, 1);
		dbms_sql.define_array(var_cur_w, 2, tb_mat_w, qt_reg_transacao_w, 1);
		dbms_sql.define_array(var_cur_w, 3, tb_regra_w, qt_reg_transacao_w, 1);
		dbms_sql.define_array(var_cur_w, 4, tb_regra_carac_w, qt_reg_transacao_w, 1);
		
		-- Executar o comando.
		var_exec_w := dbms_sql.execute(var_cur_w);
		loop
		-- O fetch rows ir� preencher os buffers do Oracle com as linhas que ser�o passadas para a lista quando o COLUMN_VALUE for chamado.
		var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
		
			-- Zerar as listas a cada execu��o. para que o mesmo registro n�o seja gravado novamente na tabela.
			limpar_listas;
			
			-- Obter as listas que foram populadas.
			dbms_sql.column_value(var_cur_w, 1, tb_proc_w);
			dbms_sql.column_value(var_cur_w, 2, tb_mat_w);
			dbms_sql.column_value(var_cur_w, 3, tb_regra_w);
			dbms_sql.column_value(var_cur_w, 4, tb_regra_carac_w);
			
			-- Grava as listas no banco em um �nico comando DML.
			gravar_listas;
			
			-- Quando n�mero de linhas que foram aplicadas no array for diferente do definido significa que esta foi a �ltima itera��o do loop e que todas as linhas foram
			-- passadas.
			exit when var_retorno_w != qt_reg_transacao_w;
		end loop; -- Contas filtradas
		dbms_sql.close_cursor(var_cur_w);
	exception
		when others then
		
		-- Fechar os cursores que continuam abertos, os cursores que utilizam FOR - LOOP n�o necessitam serem fechados, ser�o fechados autom�ticamente.
		dbms_sql.close_cursor(var_cur_w);
		
		-- Formata a mensagem de erro que ser� gerada para o usu�rio.
		tratar_erro(ds_sql_w, null);
	end;
end if;
end buscar_procedimentos;

procedure alimentar_liberacao (	dados_regra_p	dados_regra) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Ler as regras de determinado agrupamento e process�-las, gravando o v�nculo entre as 
	regras e os procedimentos liberados na tabela PLS_CONTA_PROC_REGRA_LIB.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 629123 - 08/11/2013 -  Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

-- Joga todos os procedimentos como sendo v�lidos para esta regra.
buscar_procedimentos(dados_regra_p);

end alimentar_liberacao;

procedure processar_regra(	dados_regra_p	dados_regra) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Processar a regra passada por par�metro.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 629123 - 08/11/2013 -  Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

-- S� ser� processada alguma regra v�lida.
if	(dados_regra_p.nr_sequencia is not null) then

	-- Alimentar todos os procedimentos como sendo v�lidos para a regra.
	alimentar_liberacao(dados_regra_p);
	
	-- Este controle � feito para que n�o seja feito  um select em cima da view PLS_CONTA_PROC_V sem ter nenhum procedimento liberado para 
	-- esta regra.
	if	(dados_regra_p.qt_procedimento_lib > 0) then
	
		-- Busca os procedimentos e faz o v�nculo com as regras.
		processar_procedimentos(dados_regra_p);
		commit;
	end if;
end if;
end processar_regra;
	
procedure processar_regras_grupo(	nr_seq_grupo_p	pls_lib_item_regra_grupo.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Ler as regras de determinado agrupamento e process�-las, gravando o v�nculo entre as 
	regras e os procedimentos liberados na tabela PLS_CONTA_PROC_REGRA_LIB.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 629123 - 08/11/2013 -  Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_regra_w	dados_regra;

-- Obt�m todas as regras do grupo passado por par�metro.
cursor cs_regra_grupo(nr_seq_grupo_pc	pls_lib_item_regra_grupo.nr_sequencia%type) is
	select	a.nr_sequencia,
		a.nr_seq_tipo_acomodacao,
		-- Este campo � utilizado como controle, para que s� seja feito algum select
		-- na PLS_CONTA_PROC_V se for retornado algum registro neste campo.
		(select	count(1)
		from	pls_lib_item_regra_proc x
		where	x.nr_seq_regra = a.nr_sequencia) qt_proc_lib
	from	pls_lib_item_regra a
	where	a.nr_seq_grupo_regra = nr_seq_grupo_pc;
begin

-- S� ir� buscar as regras se for passado um grupo v�lido.
if	(nr_seq_grupo_p is not null) then
	
	-- Varrer as regras para o grupo passado por par�metro.
	for	rw_regra_grupo_w in cs_regra_grupo(nr_seq_grupo_p) loop
		
		-- Alimentar os dados da regra que ir� ser verficiada.
		dados_regra_w.nr_sequencia		:= rw_regra_grupo_w.nr_sequencia;
		dados_regra_w.nr_seq_regra_grupo	:= nr_seq_grupo_p;
		dados_regra_w.nr_seq_tipo_acomodacao	:= rw_regra_grupo_w.nr_seq_tipo_acomodacao;
		dados_regra_w.qt_procedimento_lib	:= rw_regra_grupo_w.qt_proc_lib;
		
		-- Busca os procedimentos que est�o liberados para a regra atual.
		processar_regra(dados_regra_w);
	end loop; -- cs_regra_grupo
end if;

end processar_regras_grupo;

procedure gerencia_liberacao_itens(	nr_seq_lote_prot_p	pls_lote_protocolo_conta.nr_sequencia%type,
					nr_seq_protocolo_p	pls_protocolo_conta.nr_sequencia%type,
					nr_seq_conta_p		pls_conta.nr_sequencia%type,
					nr_seq_lote_processo_p	pls_cta_lote_processo.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Ler as regras de Carascter�sticas da conta X Caracter�sticas do item e vincular 
	os itens liberados atrav�s da tabela pls_conta_proc_regra_lib.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 629123 - 08/11/2013 -  Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cursor cs_grupos is
	select	a.nr_sequencia,
		a.ie_tipo_validacao
	from	pls_lib_item_regra_grupo a
	where	a.ie_situacao = 'A';
begin

-- Alimentar os parametros.
dados_parametros_w.nr_seq_lote		:= nr_seq_lote_prot_p;
dados_parametros_w.nr_seq_protocolo	:= nr_seq_protocolo_p;
dados_parametros_w.nr_seq_conta		:= nr_seq_conta_p;
dados_parametros_w.nr_seq_lote_processo	:= nr_seq_lote_processo_p;
dados_parametros_w.cd_estabelecimento	:= cd_estabelecimento_p;
dados_parametros_w.nm_usuario		:= nm_usuario_p;

-- Limpar as listas.
limpar_listas;

-- Varrer as regras.
for	rw_grupos_w in cs_grupos loop
	
	-- Processar as regras do grupo atual. Aqui ser� filtrado os itens conforme passado por par�metro e ir� ser confrontadas as caracter�sticas da conta e do item e 
	-- ent�o inseridos na tabela que define quem foi liberado e quem n�o foi.
	processar_regras_grupo(rw_grupos_w.nr_sequencia);
end loop; -- cs_grupos

-- Mandar as listas para o banco
gravar_listas;
atualizar_listas;

commit;

end gerencia_liberacao_itens;

procedure limpar_itens_liberados(	nr_seq_conta_p	pls_conta.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Limpar a tabela dos itens liberados para a conta.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 629123 - 08/11/2013 -  Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

tb_reg_lib_w	dbms_sql.number_table;
i		pls_integer;

begin

-- Inicializar a lista que ir� conter os registros a serem deletados.
tb_reg_lib_w := num_tb_vazia_w;

-- Zerar o �ndice. 
i := 0;
-- Varrer os registros de libera��o de itens da conta passada por par�metro.
for	rw_tb_lib_w in (	select	a.nr_sequencia
				from	pls_conta_proc_regra_lib a
				where	a.nr_seq_conta_proc in (select	x.nr_sequencia
								from	pls_conta_proc x
								where	x.nr_seq_conta = nr_seq_conta_p
								and	x.ie_status <> 'C')) loop
	-- Armazenar a sequencia atual.
	tb_reg_lib_w(i) :=	rw_tb_lib_w.nr_sequencia;
	
	i := i + 1;
end loop;

-- Deletar todos os registros em um �nico statement.
forall i in tb_reg_lib_w.first..tb_reg_lib_w.last 
	delete	pls_conta_proc_regra_lib
	where	nr_sequencia = tb_reg_lib_w(i);
	
commit;

end limpar_itens_liberados;

end pls_cta_consist_carac_item_pck;
/
