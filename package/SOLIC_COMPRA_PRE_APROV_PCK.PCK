create or replace package solic_compra_pre_aprov_pck as

type t_objeto_row is record (
	nr_solic_compra		number(10,0),
	dt_liberacao		date,
	ds_solicitante		varchar(255),
	ie_urgente		varchar(1),
	dt_pre_aprovacao	date
     );

type t_objeto_row_data is table of t_objeto_row;

function obter_dados (	ie_mostrar_p		varchar2,
			dt_inicial_p		date,
			dt_final_p		date,
			nm_usuario_p		varchar2)
 		    	return t_objeto_row_data pipelined;



end solic_compra_pre_aprov_pck;
/



create or replace package body solic_compra_pre_aprov_pck as

	t_objeto_row_w			t_objeto_row;

	function obter_dados (	ie_mostrar_p		varchar2,
				dt_inicial_p		date,
				dt_final_p		date,
				nm_usuario_p		varchar2)
				return t_objeto_row_data pipelined is

	t_objeto_row_w			t_objeto_row;
				
	cursor c01 is
	select	nr_solic_compra,
		dt_liberacao,
		obter_nome_pf(cd_pessoa_solicitante) ds_solicitante,
		ie_urgente,
		dt_pre_aprovacao
	from	solic_compra
	where   obter_se_pre_aprovacao(nr_solic_compra) = 'S'		
	order by nr_solic_compra;
	
	cursor c02 is
	select	nr_solic_compra,
		dt_liberacao,
		obter_nome_pf(cd_pessoa_solicitante) ds_solicitante,
		ie_urgente,
		dt_pre_aprovacao
	from	solic_compra
	where   nm_usuario_pre_aprov = nm_usuario_p
	and 	dt_pre_aprovacao between pkg_date_utils.start_of(dt_inicial_p,'DD') AND pkg_date_utils.end_of(dt_final_p,'DAY')
	order by nr_solic_compra;
	
				
				
	begin

	if (ie_mostrar_p = '0') then
	
		for r_c01_w in c01 loop
			t_objeto_row_w.nr_solic_compra	:= r_c01_w.nr_solic_compra;
			t_objeto_row_w.dt_liberacao	:= r_c01_w.dt_liberacao;
			t_objeto_row_w.ds_solicitante	:= r_c01_w.ds_solicitante;
			t_objeto_row_w.ie_urgente	:= r_c01_w.ie_urgente;
			t_objeto_row_w.dt_pre_aprovacao := r_c01_w.dt_pre_aprovacao;
			
			pipe row (t_objeto_row_w);

		end loop;
	
	elsif (ie_mostrar_p = '1') then
	
		for r_c02_w in c02 loop
			t_objeto_row_w.nr_solic_compra	:= r_c02_w.nr_solic_compra;
			t_objeto_row_w.dt_liberacao	:= r_c02_w.dt_liberacao;
			t_objeto_row_w.ds_solicitante	:= r_c02_w.ds_solicitante;
			t_objeto_row_w.ie_urgente	:= r_c02_w.ie_urgente;
			t_objeto_row_w.dt_pre_aprovacao := r_c02_w.dt_pre_aprovacao;
			
			pipe row (t_objeto_row_w);

		end loop;
	end if;

	return;

	end obter_dados;

end solic_compra_pre_aprov_pck;
/