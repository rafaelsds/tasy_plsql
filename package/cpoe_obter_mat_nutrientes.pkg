create or replace package cpoe_obter_mat_nutrientes as

type t_nutrientes is record (
ds_elemento				varchar2(255),
ds_unidade_medida		varchar2(255),
qt_conv_unid_cons		number(20),
qt_conversao_ml 		nut_elem_material.qt_conversao_ml%type,
qt_dose_result_round 	number(20,10),
ds_qt_dose_conversao 	varchar2(255));

type t_objeto_nutrientes is table of t_nutrientes;

function put_item(cd_material_p	number, qt_dose_p number, cd_unidade_medida_p varchar2) return t_objeto_nutrientes pipelined;

end cpoe_obter_mat_nutrientes;
/

create or replace
package body cpoe_obter_mat_nutrientes as

function put_item(	cd_material_p		number,
					qt_dose_p			number,
					cd_unidade_medida_p	varchar2) return t_objeto_nutrientes pipelined is
    
t_objeto_row_w		t_nutrientes;
qt_conversao_w      number := obter_conversao_ml(cd_material_p, qt_dose_p, cd_unidade_medida_p);
qt_dose_result_w    number;
nr_casas_diluicao_w	number;
    
cursor c01 is 
select  a.ds_elemento,
		a.cd_unidade_medida,
        obter_desc_unidade_medida(a.cd_unidade_medida) ds_unidade_medida, 
        b.qt_conv_unid_cons, 
        b.qt_conversao_ml,
		b.ie_sem_volume,
		b.qt_conversao,
		b.cd_unid_med_conv
from    nut_elemento a,
		nut_elem_material b
where   a.nr_sequencia = b.nr_seq_elemento
and 	a.ie_situacao = 'A'
and 	b.ie_situacao = 'A'
and 	nvl(b.ie_tipo, 'NPT') = 'NPT'
and 	b.cd_material = cd_material_p
and		nvl(ie_medic_solucao, 'N') = 'S';

begin

	select p.nr_casas_diluicao into nr_casas_diluicao_w from parametro_medico p where p.cd_estabelecimento = nvl(wheb_usuario_pck.get_cd_estabelecimento, 1);
  
	for c01_w in c01
	loop

		t_objeto_row_w.ds_elemento       :=	c01_w.ds_elemento;
		t_objeto_row_w.ds_unidade_medida :=	c01_w.ds_unidade_medida;
		t_objeto_row_w.qt_conv_unid_cons := c01_w.qt_conv_unid_cons;
			
		if (c01_w.ie_sem_volume = 'S') then
      
			t_objeto_row_w.qt_conversao_ml := obter_dose_convertida(cd_material_p, qt_dose_p, cd_unidade_medida_p, c01_w.cd_unid_med_conv);
			qt_dose_result_w := (qt_dose_p / t_objeto_row_w.qt_conversao_ml) * c01_w.qt_conv_unid_cons;
			t_objeto_row_w.qt_dose_result_round := qt_dose_result_w;

		else			
				
			t_objeto_row_w.qt_conversao_ml :=	c01_w.qt_conversao_ml;
			qt_dose_result_w := (qt_conversao_w / c01_w.qt_conversao_ml) * c01_w.qt_conv_unid_cons;
			t_objeto_row_w.qt_dose_result_round := round(qt_dose_result_w, nvl(nr_casas_diluicao_w, 0));        
        
		end if;
      
		t_objeto_row_w.ds_qt_dose_conversao := concat(concat(t_objeto_row_w.qt_dose_result_round, ' '), t_objeto_row_w.ds_unidade_medida);  
      
		pipe row (t_objeto_row_w);
		
	end loop;
  
	return;

end put_item;

end cpoe_obter_mat_nutrientes;
/  