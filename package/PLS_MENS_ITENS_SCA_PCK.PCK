create or replace
package pls_mens_itens_sca_pck as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Calcular os valores de SCA:
15	Servicos e coberturas adicionais
35	Reajuste - Variacao de custo - SCA
------------------------------------------------------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

procedure gerar_sca(	nr_seq_mensalidade_p		pls_mensalidade.nr_sequencia%type,
			nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type,
			dt_referencia_p			pls_mensalidade_segurado.dt_mesano_referencia%type,
			qt_idade_p			pls_mensalidade_segurado.qt_idade%type,
			ie_tipo_estipulante_p		pls_mensalidade_segurado.ie_tipo_estipulante%type,
			nr_seq_lote_p			pls_lote_mensalidade.nr_sequencia%type,
			dt_referencia_lote_p		pls_lote_mensalidade.dt_mesano_referencia%type,
			nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
			dt_contratacao_p		pls_segurado.dt_contratacao%type,
			dt_rescisao_p			pls_segurado.dt_rescisao%type,
			dt_reativacao_p			pls_segurado.dt_reativacao%type,
			nr_seq_contrato_p		pls_contrato.nr_sequencia%type,
			nr_seq_intercambio_p		pls_intercambio.nr_sequencia%type,
			nr_seq_pagador_p		pls_contrato_pagador.nr_sequencia%type,
			ie_mensalidade_proporcional_p	pls_segurado.ie_mensalidade_proporcional%type,
			ie_titularidade_p		varchar2,
			cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
			ie_pce_proporcional_p		pls_mensalidade_segurado.ie_pce_proporcional%type,
			dt_inclusao_pce_p		pls_segurado.dt_inclusao_pce%type,
			ie_reativacao_proporcional_p	pls_mensalidade_segurado.ie_reativacao_proporcional%type,
			dt_rescisao_ant_p		pls_segurado.dt_rescisao_ant%type,
			nm_usuario_p			usuario.nm_usuario%type);

function obter_tipo_lancamento_mens(	ie_lancamento_mensalidade_p	pls_sca_vinculo.ie_lancamento_mensalidade%type,
					ie_embutido_produto_p		pls_sca_vinculo.ie_embutido_produto%type,
					ie_lancamento_mens_plano_p	pls_plano.ie_lancamento_mensalidade%type) return varchar2;
			
end pls_mens_itens_sca_pck;
/
create or replace package body pls_mens_itens_sca_pck as

function obter_tipo_lancamento_mens(	ie_lancamento_mensalidade_p	pls_sca_vinculo.ie_lancamento_mensalidade%type,
					ie_embutido_produto_p		pls_sca_vinculo.ie_embutido_produto%type,
					ie_lancamento_mens_plano_p	pls_plano.ie_lancamento_mensalidade%type)
 		    	return varchar2 is

ie_retorno_lancamento_mens_w	varchar2(1);
			
begin

if	(ie_lancamento_mensalidade_p is null) then
	if	(ie_embutido_produto_p = 'N') then
		ie_retorno_lancamento_mens_w	:= ie_lancamento_mens_plano_p;
	elsif	(ie_embutido_produto_p = 'S') then
		ie_retorno_lancamento_mens_w	:= 'E';
	end if;
else
	ie_retorno_lancamento_mens_w	:= ie_lancamento_mensalidade_p;
end if;

return	ie_retorno_lancamento_mens_w;

end obter_tipo_lancamento_mens;

procedure obter_data_cobertura(	nr_parcela_sca_p		in	pls_mensalidade_seg_item.nr_parcela_sca%type,
				dt_adesao_sca_p			in	pls_sca_vinculo.dt_inicio_vigencia%type,
				dt_rescisao_sca_p		in	pls_sca_vinculo.dt_fim_vigencia%type,
				dt_reativacao_p			in	pls_segurado.dt_reativacao%type,
				dt_referencia_p			in	pls_mensalidade_segurado.dt_mesano_referencia%type,
				nr_seq_pagador_p		in	pls_contrato_pagador.nr_sequencia%type,
				nr_seq_pagador_sca_p		in	pls_sca_vinculo.nr_seq_pagador%type,
				nr_seq_contrato_p		in	pls_contrato.nr_sequencia%type,
				nr_seq_intercambio_p		in	pls_intercambio.nr_sequencia%type,
				ie_tipo_estipulante_p		in	pls_mensalidade_segurado.ie_tipo_estipulante%type,
				ie_mensalidade_proporcional_p	in	pls_segurado.ie_mensalidade_proporcional%type,
				cd_estabelecimento_p		in	estabelecimento.cd_estabelecimento%type,
				ie_pce_proporcional_p		in	pls_mensalidade_segurado.ie_pce_proporcional%type,
				dt_inclusao_pce_p		in	pls_segurado.dt_inclusao_pce%type,
				dt_rescisao_ant_p		in	pls_segurado.dt_rescisao_ant%type,
				ie_rescisao_proporcional_p	out	varchar2,
				dt_inicio_cobertura_p		out	pls_mensalidade_seg_item.dt_inicio_cobertura%type,
				dt_fim_cobertura_p		out	pls_mensalidade_seg_item.dt_fim_cobertura%type) is

dt_inicio_cobertura_w		pls_mensalidade_seg_item.dt_inicio_cobertura%type;
dt_fim_cobertura_w		pls_mensalidade_seg_item.dt_fim_cobertura%type;
ie_mensalidade_proporcional_w	pls_segurado.ie_mensalidade_proporcional%type;
ie_calc_primeira_mens_w		pls_contrato_pagador.ie_calc_primeira_mens%type;
ie_rescisao_proporcional_w	varchar2(1);
nr_dia_contratacao_w		number(2);

begin
ie_rescisao_proporcional_w	:= 'N';

select	max(ie_calc_primeira_mens)
into	ie_calc_primeira_mens_w
from	pls_contrato_pagador
where	nr_sequencia	= nr_seq_pagador_p;

if	(ie_mensalidade_proporcional_p is null) then
	if	(ie_calc_primeira_mens_w = 'I') then
		ie_mensalidade_proporcional_w	:= 'N';
	else
		ie_mensalidade_proporcional_w	:= 'S';
	end if;
else
	ie_mensalidade_proporcional_w	:= ie_mensalidade_proporcional_p;
end if;

if	((nr_parcela_sca_p = 1) or
	 (trunc(dt_reativacao_p,'month') = dt_referencia_p and pls_mensalidade_util_pck.get_ie_att_cobertura_reativ = 'P')) and
	(ie_pce_proporcional_p = 'N' or dt_referencia_p <> trunc(dt_inclusao_pce_p,'month')) then
	if	(nr_parcela_sca_p = 1) then
		dt_inicio_cobertura_w	:= dt_adesao_sca_p;
	else
		dt_inicio_cobertura_w	:= dt_reativacao_p;
	end if;

	if	(ie_mensalidade_proporcional_w = 'S') then
		dt_fim_cobertura_w	:= last_day(dt_inicio_cobertura_w);
	else
		if	(pls_mensalidade_util_pck.get_ie_att_cobertura_reativ = 'P') and
			(trunc(dt_reativacao_p,'month') = dt_referencia_p) then
			nr_dia_contratacao_w	:= to_number(to_char(dt_adesao_sca_p,'dd'))-1;
			dt_fim_cobertura_w	:= add_months(trunc(dt_inicio_cobertura_w,'month')+nr_dia_contratacao_w,1)-1;
		else
			dt_fim_cobertura_w	:= add_months(dt_inicio_cobertura_w,1)-1;
		end if;
	end if;
elsif	(trunc(dt_inclusao_pce_p,'month') = dt_referencia_p) and
	(ie_pce_proporcional_p = 'S') and
	(nr_seq_pagador_sca_p is null) then
	
	dt_inicio_cobertura_w	:= dt_inclusao_pce_p;
	dt_fim_cobertura_w	:= last_day(dt_inclusao_pce_p);
else
	if	(ie_mensalidade_proporcional_w = 'S') then
		dt_inicio_cobertura_w	:= trunc(dt_referencia_p,'month');
		dt_fim_cobertura_w	:= last_day(dt_referencia_p);
	else
		begin
		dt_inicio_cobertura_w	:= to_date(to_char(dt_adesao_sca_p,'dd') || '/'|| to_char(dt_referencia_p,'mm/yyyy'));
		exception
		when others then
			dt_inicio_cobertura_w	:= last_day(dt_referencia_p);
		end;
		
		dt_fim_cobertura_w	:= add_months(dt_inicio_cobertura_w,1)-1;
	end if;

	if	(ie_pce_proporcional_p = 'E') and
		(dt_inclusao_pce_p < dt_fim_cobertura_w)then
		dt_fim_cobertura_w	:= fim_dia(nvl(dt_rescisao_ant_p, dt_inclusao_pce_p-1));
	end if;
end if;

if	(dt_rescisao_sca_p is not null) then
	if	(dt_rescisao_sca_p between dt_inicio_cobertura_w and dt_fim_cobertura_w) then
		if	(pls_obter_se_rescisao_proporc(nr_seq_contrato_p, nr_seq_intercambio_p, ie_tipo_estipulante_p, dt_referencia_p, cd_estabelecimento_p) = 'S') then
			dt_fim_cobertura_w		:= dt_rescisao_sca_p;
			ie_rescisao_proporcional_w	:= 'S';
		end if;
	end if;
end if;

ie_rescisao_proporcional_p	:= ie_rescisao_proporcional_w;
dt_inicio_cobertura_p		:= dt_inicio_cobertura_w;
dt_fim_cobertura_p		:= dt_fim_cobertura_w;

end obter_data_cobertura;

procedure gerar_taxa_inscricao(	nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type,
				nr_seq_vinculo_sca_p		pls_sca_vinculo.nr_sequencia%type,
				nr_seq_plano_p			pls_plano.nr_sequencia%type,
				nr_parcela_sca_p		number,
				vl_preco_sca_p			pls_mensalidade_seg_item.vl_item%type,
				dt_referencia_p			pls_mensalidade_segurado.dt_mesano_referencia%type,
				ie_titularidade_p		varchar2) is

vl_inscricao_w		pls_regra_inscricao.vl_inscricao%type;
tx_inscricao_w		pls_regra_inscricao.tx_inscricao%type;
vl_item_inscricao_w	pls_mensalidade_seg_item.vl_item%type;
ie_inseriu_item_w	varchar2(1);

begin
--Gerar valor de inscricao do SCA
begin
select	nvl(vl_inscricao,0),
	nvl(tx_inscricao,0)
into	vl_inscricao_w,
	tx_inscricao_w
from	pls_regra_inscricao
where	nr_seq_plano = nr_seq_plano_p
and	nr_parcela_sca_p between qt_parcela_inicial and qt_parcela_final
and	(ie_grau_dependencia = ie_titularidade_p or ie_grau_dependencia = 'A')
and	dt_referencia_p between trunc(nvl(dt_inicio_vigencia,dt_referencia_p),'month') and trunc(nvl(dt_fim_vigencia,dt_referencia_p),'month');
exception
when others then
	vl_inscricao_w := 0;
	tx_inscricao_w := 0;
end;

vl_item_inscricao_w := ((tx_inscricao_w * vl_preco_sca_p) / 100) + vl_inscricao_w;

if	(nvl(vl_item_inscricao_w,0) <> 0) then
	pls_mens_itens_pck.add_item_taxa_insc(nr_seq_mensalidade_seg_p, '2', vl_item_inscricao_w, 'N', nr_seq_vinculo_sca_p, nr_parcela_sca_p, wheb_mensagem_pck.get_texto(1116072)||' referente ao SCA', ie_inseriu_item_w);
end if;

end gerar_taxa_inscricao;

procedure calcular_valor_reajuste(	nr_seq_sca_vinculo_p	in	pls_sca_vinculo.nr_sequencia%type,
					nr_seq_segurado_p	in	pls_segurado.nr_sequencia%type,
					dt_referencia_p		in	pls_mensalidade_segurado.dt_mesano_referencia%type,
					ie_lancamento_mensalidade_p in	pls_sca_vinculo.ie_lancamento_mensalidade%type,
					ie_sca_proporcional_p	in	varchar2,
					tx_proporcional_mens_p	in	number,
					vl_preco_sca_p		in out	pls_segurado_preco_origem.vl_preco_atual%type,
					vl_reajuste_indice_p	out	pls_segurado_preco_origem.vl_preco_atual%type,
					nr_seq_reajuste_preco_p	out	pls_segurado_preco_origem.nr_seq_reajuste_preco%type) is

nr_seq_preco_indice_w		pls_segurado_preco_origem.nr_sequencia%type;
nr_seq_reajuste_preco_w		pls_segurado_preco_origem.nr_seq_reajuste_preco%type;
vl_preco_sca_w			pls_segurado_preco_origem.vl_preco_atual%type;
vl_reajuste_indice_w		pls_segurado_preco_origem.vl_preco_atual%type;
dt_inicio_w			date;
dt_fim_w			date;
qt_registros_w			pls_integer;
qt_reajuste_desfazer_w		pls_integer;
qt_reajuste_deflator_w		pls_integer;
tx_reajuste_correto_w		pls_reajuste.tx_reajuste_correto%type;

begin
vl_preco_sca_w		:= vl_preco_sca_p;
vl_reajuste_indice_w	:= 0;
dt_inicio_w		:= trunc(dt_referencia_p, 'month');
dt_fim_w		:= fim_dia(last_day(dt_inicio_w));

select	max(nr_sequencia)
into	nr_seq_preco_indice_w
from	pls_segurado_preco_origem
where	nr_seq_vinculo_sca	= nr_seq_sca_vinculo_p
and	dt_liberacao is not null
and	cd_motivo_reajuste	= 'I'
and	dt_reajuste between dt_inicio_w and dt_fim_w;

--Caso nao tiver reajuste no mes atual, verifica se nos ultimos meses teve reajuste que ainda nao foi destacado na mensalidade
if	(nr_seq_preco_indice_w is null) then
	for i in 1..4 loop
		begin
		dt_inicio_w	:= trunc(add_months(dt_referencia_p,-i), 'month');
		dt_fim_w	:= fim_dia(last_day(dt_inicio_w));
		
		select	max(nr_sequencia)
		into	nr_seq_preco_indice_w
		from	pls_segurado_preco_origem
		where	nr_seq_vinculo_sca		= nr_seq_sca_vinculo_p
		and	dt_liberacao	is not null
		and	cd_motivo_reajuste	= 'I'
		and	dt_reajuste between dt_inicio_w and dt_fim_w;
		
		--Caso tenha reajuste no mes passado, verifica se ja foi colocado o item 25 ou 35, se foi entao nao e necessario gerar novamente
		if	(nr_seq_preco_indice_w is not null) then
			if	(ie_lancamento_mensalidade_p = 'C') then
				select	count(1)
				into	qt_registros_w
				from	pls_mensalidade_seg_item	c,
					pls_mensalidade_segurado	b,
					pls_mensalidade			a
				where	c.nr_seq_mensalidade_seg	= b.nr_sequencia
				and	b.nr_seq_mensalidade		= a.nr_sequencia
				and	b.nr_seq_segurado		= nr_seq_segurado_p
				and	a.ie_cancelamento is null
				and	c.ie_tipo_item			= '25'
				and	b.dt_mesano_referencia		>= dt_inicio_w
				and	rownum				= 1;
				
				if	(qt_registros_w = 0) and (pls_mens_itens_pck.obter_se_item_vetor(nr_seq_segurado_p, '25', null, nr_seq_sca_vinculo_p, null) = 'S') then
					qt_registros_w := 1;
				end if;
			elsif	(ie_lancamento_mensalidade_p = 'D') then
				select	count(1)
				into	qt_registros_w
				from	pls_mensalidade_seg_item	c,
					pls_mensalidade_segurado	b,
					pls_mensalidade			a
				where	c.nr_seq_mensalidade_seg	= b.nr_sequencia
				and	b.nr_seq_mensalidade		= a.nr_sequencia
				and	b.nr_seq_segurado		= nr_seq_segurado_p
				and	a.ie_cancelamento is null
				and	c.ie_tipo_item			= '35'
				and	c.nr_seq_vinculo_sca		= nr_seq_sca_vinculo_p
				and	b.dt_mesano_referencia		>= dt_inicio_w
				and	rownum				= 1;
				
				if	(qt_registros_w = 0) and (pls_mens_itens_pck.obter_se_item_vetor(nr_seq_segurado_p, '35', null, nr_seq_sca_vinculo_p, null) = 'S') then
					qt_registros_w := 1;
				end if;
			end if;
			
			if	(qt_registros_w > 0) then
				nr_seq_preco_indice_w	:= null; --Se o reajuste ja foi destacado na mensalidade, entao nao deve destacar novamente
			end if;
			
			exit; --Sair do loop se encontrou o reajuste
		end if;
		end;
	end loop;
end if;

if	(nr_seq_preco_indice_w is not null) then
	select	a.vl_preco_atual - a.vl_preco_ant,
		a.nr_seq_reajuste_preco,
		(select	count(1)
		from	pls_reajuste x
		where	x.nr_sequencia = a.nr_seq_reajuste
		and	x.nr_seq_reajuste_desfazer is not null) qt_reajuste_desfazer,
		(select	count(1)
		from	pls_reajuste x,
			pls_reajuste y
		where	x.nr_sequencia = a.nr_seq_reajuste
		and	x.nr_sequencia = y.nr_seq_lote_deflator) qt_reajuste_deflator,
		nvl((	select	max(x.tx_reajuste_correto)
			from	pls_reajuste x
			where	x.nr_sequencia = a.nr_seq_reajuste),0) tx_reajuste_correto
	into	vl_reajuste_indice_w,
		nr_seq_reajuste_preco_w,
		qt_reajuste_desfazer_w,
		qt_reajuste_deflator_w,
		tx_reajuste_correto_w
	from	pls_segurado_preco_origem a
	where	a.nr_sequencia = nr_seq_preco_indice_w;

	if	(pls_mens_itens_pck.verificar_se_permite_reajuste(qt_reajuste_desfazer_w, qt_reajuste_deflator_w, tx_reajuste_correto_w) = 'S') then
		if	(ie_sca_proporcional_p = 'S') then
			vl_reajuste_indice_w := tx_proporcional_mens_p * nvl(vl_reajuste_indice_w,0);
		end if;
		
		if	(vl_reajuste_indice_w > 0) then
			vl_preco_sca_w	:= nvl(vl_preco_sca_w,0) - nvl(vl_reajuste_indice_w,0);
		end if;
	else
		vl_reajuste_indice_w	:= 0;
	end if;
end if;

vl_preco_sca_p		:= vl_preco_sca_w;
vl_reajuste_indice_p	:= vl_reajuste_indice_w;
nr_seq_reajuste_preco_p	:= nr_seq_reajuste_preco_w;

end calcular_valor_reajuste;

procedure gerar_sca(	nr_seq_mensalidade_p		pls_mensalidade.nr_sequencia%type,
			nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type,
			dt_referencia_p			pls_mensalidade_segurado.dt_mesano_referencia%type,
			qt_idade_p			pls_mensalidade_segurado.qt_idade%type,
			ie_tipo_estipulante_p		pls_mensalidade_segurado.ie_tipo_estipulante%type,
			nr_seq_lote_p			pls_lote_mensalidade.nr_sequencia%type,
			dt_referencia_lote_p		pls_lote_mensalidade.dt_mesano_referencia%type,
			nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
			dt_contratacao_p		pls_segurado.dt_contratacao%type,
			dt_rescisao_p			pls_segurado.dt_rescisao%type,
			dt_reativacao_p			pls_segurado.dt_reativacao%type,
			nr_seq_contrato_p		pls_contrato.nr_sequencia%type,
			nr_seq_intercambio_p		pls_intercambio.nr_sequencia%type,
			nr_seq_pagador_p		pls_contrato_pagador.nr_sequencia%type,
			ie_mensalidade_proporcional_p	pls_segurado.ie_mensalidade_proporcional%type,
			ie_titularidade_p		varchar2,
			cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
			ie_pce_proporcional_p		pls_mensalidade_segurado.ie_pce_proporcional%type,
			dt_inclusao_pce_p		pls_segurado.dt_inclusao_pce%type,
			ie_reativacao_proporcional_p	pls_mensalidade_segurado.ie_reativacao_proporcional%type,
			dt_rescisao_ant_p		pls_segurado.dt_rescisao_ant%type,
			nm_usuario_p			usuario.nm_usuario%type) is

qt_sca_benef_w			pls_integer;
ie_gerar_sca_w			varchar2(1);
qt_regra_limite_sca_w		pls_integer;
qt_parcela_sca_w		pls_integer;
cd_motivo_reajuste_w		pls_segurado_preco_origem.cd_motivo_reajuste%type;
tx_proporcional_mens_w		number(15,8);
qt_dias_w			pls_integer;
dt_ultimo_dia_mes_w		pls_integer;

ie_pce_proporcional_w		varchar2(1);
ie_lancamento_mensalidade_w	pls_sca_vinculo.ie_lancamento_mensalidade%type;
dt_referencia_w			pls_mensalidade_segurado.dt_mesano_referencia%type;
qt_sca_mes_w			pls_integer;
nr_parcela_sca_w		number(10);
dt_inicio_cobertura_w		pls_mensalidade_seg_item.dt_inicio_cobertura%type;
dt_fim_cobertura_w		pls_mensalidade_seg_item.dt_fim_cobertura%type;
ie_rescisao_proporcional_w	varchar2(1);
nr_indice_preco_pre_w		pls_integer;
nr_indice_preco_reaj_w		pls_integer;

nr_seq_segurado_preco_w		pls_segurado_preco_origem.nr_sequencia%type;
vl_preco_sca_w			pls_segurado_preco_origem.vl_preco_atual%type;
vl_reajuste_indice_w		pls_segurado_preco_origem.vl_preco_atual%type;
nr_seq_reajuste_preco_w		pls_segurado_preco_origem.nr_seq_reajuste_preco%type;
ds_observacao_w			pls_mensalidade_seg_item.ds_observacao%type;
ie_inseriu_item_w		varchar2(1);

nr_seq_regra_limite_w		pls_regra_mens_contrato.nr_sequencia%type;
ie_agrupar_valor_lim_w		pls_regra_mens_contrato.ie_agrupar_valor%type;
qt_dias_vencimento_w		pls_regra_mens_contrato.qt_dias_vencimento%type;
ie_primeira_mensalidade_w	pls_regra_mens_contrato.ie_primeira_mensalidade%type;
ie_data_base_adesao_w		pls_regra_mens_contrato.ie_data_base_adesao%type;
ie_sca_proporcional_w		varchar2(1);

Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_plano,
		nvl(a.ie_embutido_produto,'N') ie_embutido_produto,
		a.ie_lancamento_mensalidade,
		nvl(b.ie_lancamento_mensalidade,'D') ie_lancamento_mens_plano,
		a.nr_seq_pagador,
		a.dt_inicio_vigencia dt_adesao_sca,
		nvl(a.dt_fim_vigencia,dt_rescisao_p) dt_rescisao_sca,
		nvl(b.qt_idade_min_sca,qt_idade_p) qt_idade_min_sca,
		nvl(b.qt_idade_sca,qt_idade_p) qt_idade_max_sca,
		nvl(b.ie_consistir_idade_sca_mens,'N') ie_consistir_idade_sca_mens,
		nvl(a.ie_importacao,'N') ie_importacao,
		b.ds_plano nm_sca,
		b.qt_parcelas_cobranca,
		nvl(b.ie_mensalidade_rescisao,'N') ie_mensalidade_rescisao
	from	pls_sca_vinculo a,
		pls_plano	b
	where	a.nr_seq_plano		= b.nr_sequencia
	and	a.nr_seq_segurado	= nr_seq_segurado_p
	and	a.dt_liberacao is not null
	and	a.nr_seq_tabela is not null
	and	dt_referencia_p	between trunc(a.dt_inicio_vigencia,'month') and fim_dia(nvl(a.dt_fim_vigencia,dt_referencia_p));

begin

select	count(1)
into	qt_sca_benef_w
from	dual
where	exists (select	1
		from	pls_sca_vinculo
		where	nr_seq_segurado	= nr_seq_segurado_p);

if	(qt_sca_benef_w > 0) then
	for r_c01_w in C01 loop
		begin
		ie_gerar_sca_w	:= 'N';
		
		ie_lancamento_mensalidade_w	:= obter_tipo_lancamento_mens(r_c01_w.ie_lancamento_mensalidade, r_c01_w.ie_embutido_produto, r_c01_w.ie_lancamento_mens_plano);
		
		if	(r_c01_w.nr_seq_pagador is not null) then
			if	(r_c01_w.nr_seq_pagador = nr_seq_pagador_p) then
				ie_gerar_sca_w	:= 'S';
			end if;
		elsif	(pls_mens_itens_pck.obter_se_pagador_item(nr_seq_segurado_p, nr_seq_pagador_p, '15', null, ie_pce_proporcional_p, null) = 'S') then
			ie_gerar_sca_w	:= 'S';
		end if;
		
		if	(ie_gerar_sca_w = 'S') then
			if	(ie_lancamento_mensalidade_w = 'D') and
				(r_c01_w.dt_rescisao_sca is not null) and
				(trunc(r_c01_w.dt_adesao_sca,'dd') = trunc(r_c01_w.dt_rescisao_sca,'dd')) then
				ie_gerar_sca_w	:= 'N';
			elsif	(dt_referencia_p < dt_referencia_lote_p) then
				pls_mensalidade_util_pck.obter_regra_data_limite(nr_seq_contrato_p, nr_seq_intercambio_p, ie_tipo_estipulante_p, dt_referencia_p,
										nr_seq_regra_limite_w, ie_agrupar_valor_lim_w, qt_dias_vencimento_w, ie_primeira_mensalidade_w, ie_data_base_adesao_w);
				
				select	count(1)
				into	qt_regra_limite_sca_w
				from	pls_regra_mens_cont_sca
				where	nr_seq_regra	= nr_seq_regra_limite_w;
				
				if	(qt_regra_limite_sca_w > 0) then
					select	count(1)
					into	qt_regra_limite_sca_w
					from	pls_regra_mens_cont_sca
					where	nr_seq_regra		= nr_seq_regra_limite_w
					and	nr_seq_plano_sca	= r_c01_w.nr_seq_plano;
					
					if	(qt_regra_limite_sca_w > 0) then
						ie_gerar_sca_w	:= 'S';
					else
						ie_gerar_sca_w	:= 'N';
					end if;
				else
					ie_gerar_sca_w	:= 'S';
				end if;
			else
				ie_gerar_sca_w	:= 'S';
			end if;
		end if;
		
		if	(ie_gerar_sca_w = 'S') and
			((r_c01_w.ie_consistir_idade_sca_mens = 'N') or (qt_idade_p between r_c01_w.qt_idade_min_sca and r_c01_w.qt_idade_max_sca)) then
			
			qt_parcela_sca_w	:= trunc(months_between(dt_referencia_p,trunc(r_c01_w.dt_adesao_sca,'month')));
			if	(qt_parcela_sca_w > 2) or --Somente verificar anteriores se for ate a terceira parcela
				(r_c01_w.ie_importacao = 'S') then --Se o SCA for importacao de base, entao nao deve gerar para meses anteriores
				qt_parcela_sca_w	:= 0;
			end if;
			
			for i in 0..qt_parcela_sca_w loop
				begin
				dt_referencia_w		:= add_months(dt_referencia_p,-i);
				nr_parcela_sca_w	:= trunc(months_between(dt_referencia_w,trunc(r_c01_w.dt_adesao_sca,'month'))) + 1;
				
				obter_data_cobertura(	nr_parcela_sca_w, r_c01_w.dt_adesao_sca, r_c01_w.dt_rescisao_sca, dt_reativacao_p, dt_referencia_w, nr_seq_pagador_p, r_c01_w.nr_seq_pagador,
							nr_seq_contrato_p, nr_seq_intercambio_p, ie_tipo_estipulante_p, ie_mensalidade_proporcional_p, cd_estabelecimento_p,
							ie_pce_proporcional_p, dt_inclusao_pce_p, dt_rescisao_ant_p, ie_rescisao_proporcional_w, dt_inicio_cobertura_w, dt_fim_cobertura_w);
				
				ie_pce_proporcional_w	:= null;
				
				if	((dt_inclusao_pce_p is not null) and 
					(pls_mensalidade_util_pck.get_ie_mens_prop_pce = 'S')) then
					for i in 0..3 loop
						begin
						if	(trunc(add_months(dt_inclusao_pce_p,i),'mm') = trunc(dt_referencia_p,'mm')) then
							ie_pce_proporcional_w := 'S';
						end if;
						end;
					end loop;
				end if;
				
				if	((ie_pce_proporcional_p = 'S' and dt_inclusao_pce_p between dt_inicio_cobertura_w and dt_fim_cobertura_w) or
					(ie_pce_proporcional_w = 'S' and dt_inicio_cobertura_w >= dt_inclusao_pce_p)) then
					select	count(1)
					into	qt_sca_mes_w
					from	pls_mensalidade_sca		a,
						pls_mensalidade_seg_item	b,
						pls_mensalidade_segurado	c,
						pls_mensalidade			d
					where	b.nr_sequencia	= a.nr_seq_item_mens
					and	c.nr_sequencia	= b.nr_seq_mensalidade_seg
					and	d.nr_sequencia	= c.nr_seq_mensalidade
					and	a.nr_seq_vinculo_sca	= r_c01_w.nr_sequencia
					and	a.nr_parcela		= nr_parcela_sca_w
					and	b.dt_fim_cobertura >= dt_inicio_cobertura_w
					and	b.dt_inicio_cobertura <= dt_fim_cobertura_w
					and	d.nr_seq_pagador = nr_seq_pagador_p
					and	d.ie_cancelamento is null;
				elsif	(ie_reativacao_proporcional_p = 'S' and dt_reativacao_p between dt_inicio_cobertura_w and dt_fim_cobertura_w) then
					select	count(1)
					into	qt_sca_mes_w
					from	pls_mensalidade_sca		a,
						pls_mensalidade_seg_item	b,
						pls_mensalidade_segurado	c,
						pls_mensalidade			d
					where	b.nr_sequencia	= a.nr_seq_item_mens
					and	c.nr_sequencia	= b.nr_seq_mensalidade_seg
					and	d.nr_sequencia	= c.nr_seq_mensalidade
					and	a.nr_seq_vinculo_sca	= r_c01_w.nr_sequencia
					and	a.nr_parcela		= nr_parcela_sca_w
					and	b.dt_fim_cobertura >= dt_inicio_cobertura_w
					and	b.dt_inicio_cobertura <= dt_fim_cobertura_w
					and	d.ie_cancelamento is null;
				else
					select	count(1)
					into	qt_sca_mes_w
					from	pls_mensalidade_sca		a,
						pls_mensalidade_seg_item	b,
						pls_mensalidade_segurado	c,
						pls_mensalidade			d
					where	b.nr_sequencia	= a.nr_seq_item_mens
					and	c.nr_sequencia	= b.nr_seq_mensalidade_seg
					and	d.nr_sequencia	= c.nr_seq_mensalidade
					and	a.nr_seq_vinculo_sca	= r_c01_w.nr_sequencia
					and	a.nr_parcela		= nr_parcela_sca_w
					and	d.ie_cancelamento is null;
				end if;

				if	(qt_sca_mes_w = 0) then --Se nao encontrar SCA na tabela de mensalidade, deve buscar nos vetores dos itens ainda nao inseridos
					qt_sca_mes_w	:= pls_mens_itens_pck.obter_se_sca_gerado(r_c01_w.nr_sequencia, nr_parcela_sca_w, ie_lancamento_mensalidade_w, dt_inicio_cobertura_w, dt_fim_cobertura_w);
				end if;

				if	(qt_sca_mes_w = 0) and
					(nvl(r_c01_w.qt_parcelas_cobranca,nr_parcela_sca_w) >= nr_parcela_sca_w) then
					if	(i = 0) then
						ds_observacao_w	:= '';
					else
						ds_observacao_w	:= wheb_mensagem_pck.get_texto(1116068, 'NR_PARCELA='||nr_parcela_sca_w||';NM_SCA='||r_c01_w.nm_sca);
					end if;
					
					select	max(nr_sequencia)
					into	nr_seq_segurado_preco_w
					from	pls_segurado_preco_origem
					where	nr_seq_vinculo_sca = r_c01_w.nr_sequencia
					and	dt_liberacao is not null
					and	(dt_referencia_w >= trunc(dt_reajuste, 'month'));
					
					if	(nr_seq_segurado_preco_w is not null) and
						((r_c01_w.dt_rescisao_sca is null) or
						 (r_c01_w.ie_mensalidade_rescisao = 'S' and r_c01_w.dt_rescisao_sca > dt_inicio_cobertura_w) or
						 (r_c01_w.ie_mensalidade_rescisao = 'N' and r_c01_w.dt_rescisao_sca > dt_fim_cobertura_w)) then
						
						select	decode(qt_item_destacado, 0, decode(qt_lote_taxa_negativa, 0, vl_preco_atual, vl_preco_ant), vl_preco_atual),
							cd_motivo_reajuste
						into	vl_preco_sca_w,
							cd_motivo_reajuste_w
						from	(select	nvl(a.vl_preco_atual,0) vl_preco_atual,
								nvl(a.vl_preco_ant,0) vl_preco_ant,
								(select	count(1)
								from	pls_reajuste x
								where	x.nr_sequencia = a.nr_seq_reajuste
								and	x.tx_reajuste < 0
								and	nvl(x.ie_tipo_lote, 'X') <> 'D') qt_lote_taxa_negativa,
								(select	count(1)
								from	pls_mensalidade_sca x,
									pls_mensalidade_seg_item y,
									pls_mensalidade w
								where	y.nr_sequencia = x.nr_seq_item_mens
								and	w.nr_sequencia = x.nr_seq_mensalidade
								and	a.nr_sequencia = x.nr_seq_segurado_preco
								and	y.ie_tipo_item in ('35','25')
								and	w.ie_cancelamento is null) qt_item_destacado,
								a.cd_motivo_reajuste
							from	pls_segurado_preco_origem a
							where	a.nr_sequencia	= nr_seq_segurado_preco_w);
						
						ie_sca_proporcional_w := 'N';
						
						--Calcular o valor proporcional para a primeira parcela
						if	((nr_parcela_sca_w = 1) and (ie_mensalidade_proporcional_p = 'S')) or
							(ie_rescisao_proporcional_w = 'S') or
							(ie_pce_proporcional_p in ('S','E')) or
							(ie_reativacao_proporcional_p = 'S') then
							qt_dias_w	:= obter_dias_entre_datas(dt_inicio_cobertura_w, dt_fim_cobertura_w) + 1;
							
							if	(pls_mensalidade_util_pck.get_ie_data_base_proporcional = 'T') then
								dt_ultimo_dia_mes_w	:= 30;
								--Se o dia da contratacao for o primeiro dia do mes, pega o valor total, (30 / 30 * valor)
								if	(nr_parcela_sca_w = 1) and
									(ie_rescisao_proporcional_w = 'N') and
									(trunc(r_c01_w.dt_adesao_sca,'dd') = trunc(r_c01_w.dt_adesao_sca,'month')) then
									qt_dias_w		:= 30;
								end if;
							else
								if	((nr_parcela_sca_w = 1) and (ie_mensalidade_proporcional_p = 'S')) then
									dt_ultimo_dia_mes_w	:= to_char(last_day(dt_inicio_cobertura_w),'dd');
								else
									dt_ultimo_dia_mes_w	:= to_char(last_day(dt_referencia_p),'dd');
								end if;
							end if;
							
							ie_sca_proporcional_w		:= 'S';
							tx_proporcional_mens_w		:= (qt_dias_w/to_number(dt_ultimo_dia_mes_w));
							vl_preco_sca_w			:= tx_proporcional_mens_w * vl_preco_sca_w;
						end if;
						
						--Calcular o valor de reajuste para destacar na mensalidade
						if	(pls_mensalidade_util_pck.get_ie_destacar_reajuste_sca = 'S') and
							(ie_lancamento_mensalidade_w <> 'E') and
							(cd_motivo_reajuste_w <> 'E') then
							calcular_valor_reajuste(r_c01_w.nr_sequencia, nr_seq_segurado_p, dt_referencia_w, ie_lancamento_mensalidade_w,
										ie_sca_proporcional_w, tx_proporcional_mens_w, vl_preco_sca_w, vl_reajuste_indice_w, 
										nr_seq_reajuste_preco_w);
						else
							vl_reajuste_indice_w := 0;
							nr_seq_reajuste_preco_w := null;
						end if;
						
						if	(ie_lancamento_mensalidade_w = 'D') then --Valor destacado na mensalidade
							pls_mens_itens_pck.add_item_sca(nr_seq_mensalidade_p, nr_seq_mensalidade_seg_p, '15', vl_preco_sca_w, r_c01_w.nr_sequencia, nr_parcela_sca_w,
											dt_inicio_cobertura_w, dt_fim_cobertura_w, nr_seq_segurado_p, nr_seq_segurado_preco_w, '', ds_observacao_w, null, ie_inseriu_item_w);
							
							if	(vl_reajuste_indice_w <> 0) then
								pls_mens_itens_pck.add_item_sca(nr_seq_mensalidade_p, nr_seq_mensalidade_seg_p, '35', vl_reajuste_indice_w, r_c01_w.nr_sequencia, nr_parcela_sca_w,
												dt_inicio_cobertura_w, dt_fim_cobertura_w, nr_seq_segurado_p, nr_seq_segurado_preco_w, '', '', nr_seq_reajuste_preco_w, ie_inseriu_item_w);
							end if;
						else --Valor embutido
							nr_indice_preco_pre_w	:= pls_mens_itens_pck.obter_indice_preco(nr_seq_mensalidade_seg_p,'1');
							
							if	(nr_indice_preco_pre_w = -1) then
								null;
								/*
								pls_gerar_mens_log_erro(nr_seq_lote_p, nr_seq_pagador_p, nr_seq_segurado_p, null,
									'Nao foi encontrado o item de Preco pre-estabelecido para embutir os valores de SCA do beneficiario!', cd_estabelecimento_p, nm_usuario_p);
								*/
							else
								pls_mens_itens_pck.add_sca_embutido(nr_indice_preco_pre_w, nr_seq_mensalidade_p, r_c01_w.nr_sequencia, nr_parcela_sca_w, vl_preco_sca_w,
													nr_seq_segurado_preco_w, ie_lancamento_mensalidade_w, dt_inicio_cobertura_w, dt_fim_cobertura_w);
							end if;
							
							if	(vl_reajuste_indice_w <> 0) then
								nr_indice_preco_reaj_w	:= pls_mens_itens_pck.obter_indice_preco(nr_seq_mensalidade_seg_p,'25');
								
								if	(nr_indice_preco_reaj_w = -1) then --Se nao encontrar o item Reajuste - Variacao de Custo, deve buscar o preco pre-estabelecido
									nr_indice_preco_reaj_w	:= pls_mens_itens_pck.obter_indice_preco(nr_seq_mensalidade_seg_p,'1');
								end if;
								
								if	(nr_indice_preco_reaj_w = -1) then --Se nao encontrar o item Reajuste - Variacao de Custo e nem o preco pre-estabelecido, ira criar um novo item para o reajuste do SCA
									pls_mens_itens_pck.add_item_sca(nr_seq_mensalidade_p, nr_seq_mensalidade_seg_p, '35', vl_reajuste_indice_w, r_c01_w.nr_sequencia, nr_parcela_sca_w,
												dt_inicio_cobertura_w, dt_fim_cobertura_w, nr_seq_segurado_p, nr_seq_segurado_preco_w, '', '', nr_seq_reajuste_preco_w, ie_inseriu_item_w);
								else
									pls_mens_itens_pck.add_sca_embutido(nr_indice_preco_reaj_w, nr_seq_mensalidade_p, r_c01_w.nr_sequencia, nr_parcela_sca_w, vl_reajuste_indice_w,
														nr_seq_segurado_preco_w, ie_lancamento_mensalidade_w, dt_inicio_cobertura_w, dt_fim_cobertura_w);
								end if;
							end if;
						end if;
						
						--Gerar taxa de inscricao do SCA referente a parcela que esta sendo gerada
						if	(pls_mens_itens_pck.obter_se_item_grupo('2',null) = 'S') then
							gerar_taxa_inscricao(nr_seq_mensalidade_seg_p, r_c01_w.nr_sequencia, r_c01_w.nr_seq_plano, nr_parcela_sca_w, vl_preco_sca_w, dt_referencia_w, ie_titularidade_p);
						end if;
					end if;
				end if;
				end;
			end loop;
		end if;
		end;
	end loop;
end if;

end gerar_sca;

end pls_mens_itens_sca_pck;
/