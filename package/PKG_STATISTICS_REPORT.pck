CREATE OR REPLACE PACKAGE PKG_STATISTICS_REPORT AS
  --
  FUNCTION GET_FIRST_ENTRY(P_NR_ATEND NUMBER, 
                           P_CD_SETOR NUMBER) RETURN DATE;
  --
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0);
  --
END PKG_STATISTICS_REPORT;
/
CREATE OR REPLACE PACKAGE BODY PKG_STATISTICS_REPORT AS

  PROCEDURE TABLE_CLEANING IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_STATISTICS_REPORT';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_STATISTICS_REPORT_ESTAB';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_STATISTICS_REPORT_UNIT';
  END TABLE_CLEANING;
  --
  FUNCTION GET_FIRST_ENTRY(P_NR_ATEND NUMBER, 
                           P_CD_SETOR NUMBER) RETURN DATE IS

  DT DATE;
  
  BEGIN
  
    IF P_NR_ATEND IS NOT NULL AND P_CD_SETOR IS NOT NULL THEN
      --
      SELECT MIN(DT_ENTRADA_UNIDADE)
        INTO DT
        FROM ATEND_PACIENTE_UNIDADE U
       WHERE U.NR_ATENDIMENTO = P_NR_ATEND
         AND DECODE(P_CD_SETOR, 0, 0, U.CD_SETOR_ATENDIMENTO) = P_CD_SETOR
         AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L');
      --
    END IF;
    
    RETURN DT;
    
  END GET_FIRST_ENTRY;
  --
  PROCEDURE PRE_PROCESS(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_CD_ESTAB IN NUMBER,
    P_CD_SETOR IN NUMBER) IS
    --
    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
    --
    CURSOR C_STATISTICS IS 
      SELECT CD_ESTABELECIMENTO,
             CD_SETOR_ATENDIMENTO,
             NR_ATENDIMENTO,
             DT_INI DT_START,
             DT_FIM DT_END,
             DT_ENTRY_UNIT,
             DT_EXIT_UNIT,
             MIN_ENTRY,
             MIN_ENTRY_UNIT,
             DECODE(SIGN((DT_EXIT_UNIT - DT_ENTRY_UNIT) - 6), 1, 0, 1) IS_SHORT_STAY,
             DECODE(SIGN((DT_EXIT_UNIT - DT_ENTRY_UNIT) - 6), 1, 1, 0) IS_LONG_STAY,
             DECODE(DT_ENTRADA_SETOR, MIN_ENTRY_UNIT, 1, 0) IS_ADMISSION,
             DECODE(DT_ENTRADA_SETOR, MIN_ENTRY_UNIT, 0, 1) IS_READMISSION,
             DECODE(DT_ENTRADA_SETOR, MIN_ENTRY, 0, 1) IS_TRANSFER,
             IS_DEATH,
             GENDER,
             AGE
        FROM (SELECT UN.CD_ESTABELECIMENTO,
                     UN.CD_SETOR_ATENDIMENTO,
                     UN.NR_ATENDIMENTO,
                     UN.DT_INI,
                     UN.DT_FIM,
                     UN.DT_ENTRADA_SETOR,
                     UN.DT_SAIDA_SETOR,
                     GREATEST(UN.DT_ENTRY_UNIT, UN.DT_INI) DT_ENTRY_UNIT,
                     LEAST(UN.DT_FIM, NVL(UN.DT_EXIT_UNIT, UN.DT_FIM)) DT_EXIT_UNIT,
                     PKG_STATISTICS_REPORT.GET_FIRST_ENTRY(UN.NR_ATENDIMENTO, 0) MIN_ENTRY,
                     PKG_STATISTICS_REPORT.GET_FIRST_ENTRY(UN.NR_ATENDIMENTO, UN.CD_SETOR_ATENDIMENTO) MIN_ENTRY_UNIT,
                     CASE 
                       WHEN DT_DEATH IS NULL THEN 0
                       WHEN DT_DEATH >= TRUNC(P_END_DATE, 'DD') + 1 THEN 0
                       ELSE 1
                     END IS_DEATH,
                     GENDER,
                     AGE
                FROM (SELECT UN.CD_ESTABELECIMENTO,
                             CD_SETOR_ATENDIMENTO,
                             UN.NR_ATENDIMENTO,
                             DT_INI,
                             DT_FIM,
                             DT_ENTRADA_SETOR,
                             DT_SAIDA_SETOR,
                             GREATEST(DT_ENTRADA_SETOR, DT_ENTRADA - .25) DT_ENTRY_UNIT,
                             LEAST(NVL(DT_SAIDA_SETOR, NVL2(DT_ALTA, DT_ALTA + .25, NULL)),
                                   NVL2(DT_ALTA, DT_ALTA + .25, DT_SAIDA_SETOR)) DT_EXIT_UNIT,
                             DECODE(OBTER_SEXO_PF(P.CD_PESSOA_FISICA, 'C'), 'M', 'M', 'F', 'F', 'I', 'U', 'D', 'O', 'N') GENDER,
                             OBTER_IDADE_PF(P.CD_PESSOA_FISICA, GREATEST(UN.DT_ENTRADA_SETOR, P_START_DATE), 'A') AGE,
                             OBTER_DATA_OBITO(P.CD_PESSOA_FISICA) DT_DEATH
                        FROM (SELECT S.CD_ESTABELECIMENTO,
                                     U.CD_SETOR_ATENDIMENTO,
                                     U.NR_ATENDIMENTO,
                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE) DT_INI,
                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE + 1, SYSDATE)) DT_FIM,
                                     MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                                     PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                          U.DT_ENTRADA_UNIDADE,
                                                                          U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR
                                FROM ATEND_PACIENTE_UNIDADE U,
                                     SETOR_ATENDIMENTO S
                               WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                                 AND DECODE(P_CD_ESTAB, 0, 0, S.CD_ESTABELECIMENTO) = P_CD_ESTAB
                                 AND DECODE(P_CD_SETOR, 0, 0, U.CD_SETOR_ATENDIMENTO) = P_CD_SETOR
                                 AND EXISTS(SELECT 1
                                              FROM USUARIO_SETOR US
                                             WHERE US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO
                                               AND US.NM_USUARIO_PARAM = NM_USUARIO_W)
                                 AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                                 AND S.CD_CLASSIF_SETOR = '4'
                                 AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(P_END_DATE), SYSDATE))         
                                 AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                              U.DT_ENTRADA_UNIDADE,
                                                                              U.CD_SETOR_ATENDIMENTO),
                                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                               GROUP BY U.NR_ATENDIMENTO,
                                        S.CD_ESTABELECIMENTO,
                                        U.CD_SETOR_ATENDIMENTO,
                                        PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                             U.DT_ENTRADA_UNIDADE,
                                                                             U.CD_SETOR_ATENDIMENTO)) UN,
                             ATENDIMENTO_PACIENTE P
                       WHERE UN.NR_ATENDIMENTO = P.NR_ATENDIMENTO
                         AND P.DT_ENTRADA <= UN.DT_FIM
                         AND (P.DT_ALTA IS NULL OR P.DT_ALTA >= UN.DT_INI) 
                         AND (P.DT_ALTA IS NULL OR DT_ENTRADA_SETOR <= DT_ALTA)
                         AND (DT_SAIDA_SETOR IS NULL OR DT_SAIDA_SETOR >= DT_ENTRADA)) UN) V;
    --
    TYPE T_STATISTICS IS TABLE OF C_STATISTICS%ROWTYPE;
    R_STATISTICS T_STATISTICS;
    --
    CURSOR C_STATISTICS_ESTAB IS
      SELECT T.CD_ESTABELECIMENTO,
             '' ESTAB_VALUES,
             0 TOTAL_STAYS,
             0 TOTAL_DAYS,
             0 AVG_DAYS,
             STDDEV(LEAST(T.FIM, NVL(P.DT_ALTA, T.FIM)) - 
                    GREATEST(T.INI, P.DT_ENTRADA)) STD_DEV_DAYS,
             MIN(LEAST(T.FIM, NVL(P.DT_ALTA, T.FIM)) - 
                 GREATEST(T.INI, P.DT_ENTRADA)) MIN_DAYS,
             MAX(LEAST(T.FIM, NVL(P.DT_ALTA, T.FIM)) - 
                 GREATEST(T.INI, P.DT_ENTRADA)) MAX_DAYS,
             SUM(CASE 
                   WHEN NVL(P.DT_ALTA, T.FIM + 1) > T.FIM THEN 0
                   ELSE 1
                 END) TOTAL_DISCHARGES,
             SUM(CASE
                   WHEN NVL(OBTER_DATA_OBITO(P.CD_PESSOA_FISICA), T.FIM) BETWEEN T.INI AND (T.FIM - (1/(24 * 60 * 60))) THEN 1
                   ELSE 0
                 END) TOTAL_DEATHS,
             0 RATE_DEATHS
        FROM (SELECT CD_ESTABELECIMENTO, MIN(DT_INICIO) INI, MAX(DT_FIM) FIM
                FROM W_STATISTICS_REPORT S
               GROUP BY CD_ESTABELECIMENTO) T,
             ATENDIMENTO_PACIENTE P
       WHERE T.CD_ESTABELECIMENTO = P.CD_ESTABELECIMENTO
         AND P.DT_ENTRADA < T.FIM
         AND (P.DT_ALTA IS NULL OR P.DT_ALTA >= T.INI)
       GROUP BY T.CD_ESTABELECIMENTO;
    --
    TYPE T_STATISTICS_ESTAB IS TABLE OF C_STATISTICS_ESTAB%ROWTYPE;
    R_STATISTICS_ESTAB T_STATISTICS_ESTAB;
    --
    CURSOR C_STATISTICS_UNIT IS 
      SELECT CD_ESTABELECIMENTO,
             CD_SETOR_ATENDIMENTO,
             SUM(TOTAL_STAYS) TOTAL_STAYS,
             SUM(TOTAL_DAYS) TOTAL_DAYS,
             SUM(AVG_DAYS) AVG_DAYS,
             SUM(STD_DEV_DAYS) STD_DEV_DAYS,
             SUM(MIN_DAYS) MIN_DAYS,
             SUM(MAX_DAYS) MAX_DAYS,
             SUM(AVG_DAYS_SHORT_STAY) AVG_DAYS_SHORT_STAY,
             SUM(STD_DEV_DAYS_SHORT_STAY) STD_DEV_DAYS_SHORT_STAY,
             SUM(MIN_DAYS_SHORT_STAY) MIN_DAYS_SHORT_STAY,
             SUM(MAX_DAYS_SHORT_STAY) MAX_DAYS_SHORT_STAY,
             SUM(AVG_DAYS_LONG_STAY) AVG_DAYS_LONG_STAY,
             SUM(STD_DEV_LONG_STAY) STD_DEV_LONG_STAY,
             SUM(MIN_DAYS_LONG_STAY) MIN_DAYS_LONG_STAY,
             SUM(MAX_DAYS_LONG_STAY) MAX_DAYS_LONG_STAY,
             SUM(TOTAL_ADMISSIONS) TOTAL_ADMISSIONS,
             SUM(TOTAL_READMISSIONS) TOTAL_READMISSIONS,
             SUM(TOTAL_TRANSFERS) TOTAL_TRANSFERS,
             SUM(TOTAL_DEATHS) TOTAL_DEATHS,
             SUM(RATE_DEATHS) RATE_DEATHS,
             SUM(AVG_AGE) AVG_AGE,
             SUM(STD_DEV_AGE) STD_DEV_AGE,
             SUM(MIN_AGE) MIN_AGE,
             SUM(MAX_AGE) MAX_AGE,
             SUM(TOTAL_MALE) TOTAL_MALE,
             SUM(TOTAL_FEMALE) TOTAL_FEMALE,
             SUM(TOTAL_UNKNOWN_GENDER) TOTAL_UNKNOWN_GENDER,
             SUM(TOTAL_OTHER_GENDER) TOTAL_OTHER_GENDER,
             SUM(TOTAL_NULL_GENDER) TOTAL_NULL_GENDER
        FROM (SELECT CD_ESTABELECIMENTO,
                     CD_SETOR_ATENDIMENTO,
                     COUNT(1) TOTAL_STAYS,
                     SUM(DT_SAIDA - DT_ENTRADA) TOTAL_DAYS,
                     SUM(DT_SAIDA - DT_ENTRADA) / COUNT(1) AVG_DAYS,
                     STDDEV(DT_SAIDA - DT_ENTRADA) STD_DEV_DAYS,
                     MIN(DT_SAIDA - DT_ENTRADA) MIN_DAYS,
                     MAX(DT_SAIDA - DT_ENTRADA) MAX_DAYS,
                     0 AVG_DAYS_SHORT_STAY,
                     0 STD_DEV_DAYS_SHORT_STAY,
                     0 MIN_DAYS_SHORT_STAY,
                     0 MAX_DAYS_SHORT_STAY,
                     0 AVG_DAYS_LONG_STAY,
                     0 STD_DEV_LONG_STAY,
                     0 MIN_DAYS_LONG_STAY,
                     0 MAX_DAYS_LONG_STAY,
                     SUM(IE_ADMISSION) TOTAL_ADMISSIONS,
                     SUM(IE_READMISSION) TOTAL_READMISSIONS,
                     SUM(IE_TRANSFER) TOTAL_TRANSFERS,
                     0 TOTAL_DEATHS,
                     0 RATE_DEATHS,
                     SUM(NR_IDADE) / COUNT(1) AVG_AGE,
                     STDDEV(NR_IDADE) STD_DEV_AGE,
                     MIN(NR_IDADE) MIN_AGE,
                     MAX(NR_IDADE) MAX_AGE,
                     SUM(DECODE(IE_GENERO, 'M', 1, 0)) TOTAL_MALE,
                     SUM(DECODE(IE_GENERO, 'F', 1, 0)) TOTAL_FEMALE,
                     SUM(DECODE(IE_GENERO, 'U', 1, 0)) TOTAL_UNKNOWN_GENDER,
                     SUM(DECODE(IE_GENERO, 'O', 1, 0)) TOTAL_OTHER_GENDER,
                     SUM(DECODE(IE_GENERO, 'N', 1, 0)) TOTAL_NULL_GENDER
                FROM W_STATISTICS_REPORT
               GROUP BY CD_ESTABELECIMENTO, CD_SETOR_ATENDIMENTO
              UNION
              SELECT CD_ESTABELECIMENTO,
                     CD_SETOR_ATENDIMENTO,
                     0 TOTAL_STAYS,
                     0 TOTAL_DAYS,
                     0 AVG_DAYS,
                     0 STD_DEV_DAYS,
                     0 MIN_DAYS,
                     0 MAX_DAYS,
                     SUM(DT_SAIDA - DT_ENTRADA) / COUNT(1) AVG_DAYS_SHORT_STAY,
                     STDDEV(DT_SAIDA - DT_ENTRADA) STD_DEV_DAYS_SHORT_STAY,
                     MIN(DT_SAIDA - DT_ENTRADA) MIN_DAYS_SHORT_STAY,
                     MAX(DT_SAIDA - DT_ENTRADA) MAX_DAYS_SHORT_STAY,
                     0 AVG_DAYS_LONG_STAY,
                     0 STD_DEV_LONG_STAY,
                     0 MIN_DAYS_LONG_STAY,
                     0 MAX_DAYS_LONG_STAY,
                     0 TOTAL_ADMISSIONS,
                     0 TOTAL_READMISSIONS,
                     0 TOTAL_TRANSFERS,
                     0 TOTAL_DEATHS,
                     0 RATE_DEATHS,
                     0 AVG_AGE,
                     0 STD_DEV_AGE,
                     0 MIN_AGE,
                     0 MAX_AGE,
                     0 TOTAL_MALE,
                     0 TOTAL_FEMALE,
                     0 TOTAL_UNKNOWN_GENDER,
                     0 TOTAL_OTHER_GENDER,
                     0 TOTAL_NULL_GENDER
                FROM W_STATISTICS_REPORT
               WHERE IE_SHORT_STAY = 1
               GROUP BY CD_ESTABELECIMENTO, CD_SETOR_ATENDIMENTO
              UNION
              SELECT CD_ESTABELECIMENTO,
                     CD_SETOR_ATENDIMENTO,
                     0 TOTAL_STAYS,
                     0 TOTAL_DAYS,
                     0 AVG_DAYS,
                     0 STD_DEV_DAYS,
                     0 MIN_DAYS,
                     0 MAX_DAYS,
                     0 AVG_DAYS_SHORT_STAY,
                     0 STD_DEV_DAYS_SHORT_STAY,
                     0 MIN_DAYS_SHORT_STAY,
                     0 MAX_DAYS_SHORT_STAY,
                     SUM(DT_SAIDA - DT_ENTRADA) / COUNT(1) AVG_DAYS_LONG_STAY,
                     STDDEV(DT_SAIDA - DT_ENTRADA) STD_DEV_LONG_STAY,
                     MIN(DT_SAIDA - DT_ENTRADA) MIN_DAYS_LONG_STAY,
                     MAX(DT_SAIDA - DT_ENTRADA) MAX_DAYS_LONG_STAY,
                     0 TOTAL_ADMISSIONS,
                     0 TOTAL_READMISSIONS,
                     0 TOTAL_TRANSFERS,
                     0 TOTAL_DEATHS,
                     0 RATE_DEATHS,
                     0 AVG_AGE,
                     0 STD_DEV_AGE,
                     0 MIN_AGE,
                     0 MAX_AGE,
                     0 TOTAL_MALE,
                     0 TOTAL_FEMALE,
                     0 TOTAL_UNKNOWN_GENDER,
                     0 TOTAL_OTHER_GENDER,
                     0 TOTAL_NULL_GENDER
                FROM W_STATISTICS_REPORT
               WHERE IE_LONG_STAY = 1
               GROUP BY CD_ESTABELECIMENTO, CD_SETOR_ATENDIMENTO
              UNION
              SELECT CD_ESTABELECIMENTO,
                     CD_SETOR_ATENDIMENTO,
                     0 TOTAL_STAYS,
                     0 TOTAL_DAYS,
                     0 AVG_DAYS,
                     0 STD_DEV_DAYS,
                     0 MIN_DAYS,
                     0 MAX_DAYS,
                     0 AVG_DAYS_SHORT_STAY,
                     0 STD_DEV_DAYS_SHORT_STAY,
                     0 MIN_DAYS_SHORT_STAY,
                     0 MAX_DAYS_SHORT_STAY,
                     0 AVG_DAYS_LONG_STAY,
                     0 STD_DEV_LONG_STAY,
                     0 MIN_DAYS_LONG_STAY,
                     0 MAX_DAYS_LONG_STAY,
                     0 TOTAL_ADMISSIONS,
                     0 TOTAL_READMISSIONS,
                     0 TOTAL_TRANSFERS,
                     SUM(DEATHS) TOTAL_DEATHS,
                     100 * SUM(DEATHS) / SUM(STAYS) RATE_DEATHS,
                     0 AVG_AGE,
                     0 STD_DEV_AGE,
                     0 MIN_AGE,
                     0 MAX_AGE,
                     0 TOTAL_MALE,
                     0 TOTAL_FEMALE,
                     0 TOTAL_UNKNOWN_GENDER,
                     0 TOTAL_OTHER_GENDER,
                     0 TOTAL_NULL_GENDER
                FROM (SELECT CD_ESTABELECIMENTO,
                             CD_SETOR_ATENDIMENTO,
                             NR_ATENDIMENTO,
                             COUNT(1) STAYS,
                             MAX(IE_DEATH) DEATHS
                        FROM W_STATISTICS_REPORT
                       GROUP BY CD_ESTABELECIMENTO,
                                CD_SETOR_ATENDIMENTO,
                                NR_ATENDIMENTO)
               GROUP BY CD_ESTABELECIMENTO,
                        CD_SETOR_ATENDIMENTO)
       GROUP BY CD_ESTABELECIMENTO, CD_SETOR_ATENDIMENTO;
    --
    TYPE T_STATISTICS_UNIT IS TABLE OF C_STATISTICS_UNIT%ROWTYPE;
    R_STATISTICS_UNIT T_STATISTICS_UNIT;
    --  
  BEGIN
    --
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    --
    OPEN C_STATISTICS;
    LOOP
      FETCH C_STATISTICS BULK COLLECT INTO R_STATISTICS LIMIT 1000;       
        BEGIN
          FOR i IN R_STATISTICS.FIRST..R_STATISTICS.LAST LOOP
            INSERT INTO W_STATISTICS_REPORT (
              cd_estabelecimento, 
              cd_setor_atendimento, 
              nr_atendimento, 
              dt_inicio, 
              dt_fim, 
              dt_entrada, 
              dt_saida, 
              dt_min_entrada, 
              dt_min_entrada_setor, 
              ie_short_stay, 
              ie_long_stay, 
              ie_admission, 
              ie_readmission, 
              ie_transfer, 
              ie_death, 
              ie_genero, 
              nr_idade)
            VALUES (
              R_STATISTICS(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
              R_STATISTICS(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
              R_STATISTICS(i).NR_ATENDIMENTO, --nr_atendimento, 
              R_STATISTICS(i).DT_START, --dt_inicio, 
              R_STATISTICS(i).DT_END, --dt_fim, 
              R_STATISTICS(i).DT_ENTRY_UNIT, --dt_entrada, 
              R_STATISTICS(i).DT_EXIT_UNIT, --dt_saida, 
              R_STATISTICS(i).MIN_ENTRY, --dt_min_entrada, 
              R_STATISTICS(i).MIN_ENTRY_UNIT, --dt_min_entrada_setor, 
              R_STATISTICS(i).IS_SHORT_STAY, --ie_short_stay, 
              R_STATISTICS(i).IS_LONG_STAY, --ie_long_stay, 
              R_STATISTICS(i).IS_ADMISSION, --ie_admission, 
              R_STATISTICS(i).IS_READMISSION, --ie_readmission, 
              R_STATISTICS(i).IS_TRANSFER, --ie_transfer, 
              R_STATISTICS(i).IS_DEATH, --ie_death, 
              R_STATISTICS(i).GENDER, --genero, 
              R_STATISTICS(i).AGE); --nr_idade)
          END LOOP;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
        COMMIT;
            
      EXIT WHEN C_STATISTICS%NOTFOUND; 
    END LOOP;
    CLOSE C_STATISTICS;

    OPEN C_STATISTICS_ESTAB;
    LOOP
      FETCH C_STATISTICS_ESTAB BULK COLLECT INTO R_STATISTICS_ESTAB LIMIT 1000;            
        BEGIN
          FOR i IN R_STATISTICS_ESTAB.FIRST..R_STATISTICS_ESTAB.LAST LOOP
                  
            R_STATISTICS_ESTAB(i).ESTAB_VALUES := PKG_ATEND_PAC_UNID.GET_STAY_COUNT_AND_AMOUNT(P_START_DATE, P_END_DATE, R_STATISTICS_ESTAB(i).CD_ESTABELECIMENTO, 0); 
            R_STATISTICS_ESTAB(i).TOTAL_STAYS := TO_NUMBER(REGEXP_SUBSTR(R_STATISTICS_ESTAB(i).ESTAB_VALUES, '[^;]+', 1, 1));
            R_STATISTICS_ESTAB(i).TOTAL_DAYS := TO_NUMBER(REGEXP_SUBSTR(R_STATISTICS_ESTAB(i).ESTAB_VALUES, '[^;]+', 1, 2));
                  
            IF R_STATISTICS_ESTAB(i).TOTAL_STAYS <> 0 AND R_STATISTICS_ESTAB(i).TOTAL_DAYS <> 0 THEN
              R_STATISTICS_ESTAB(i).AVG_DAYS := R_STATISTICS_ESTAB(i).TOTAL_DAYS / R_STATISTICS_ESTAB(i).TOTAL_STAYS;
            END IF;  
                  
            IF R_STATISTICS_ESTAB(i).TOTAL_DEATHS <> 0 AND R_STATISTICS_ESTAB(i).TOTAL_DISCHARGES <> 0 THEN
              R_STATISTICS_ESTAB(i).RATE_DEATHS := 100 * R_STATISTICS_ESTAB(i).TOTAL_DEATHS / R_STATISTICS_ESTAB(i).TOTAL_DISCHARGES;
            END IF;
            
            INSERT INTO W_STATISTICS_REPORT_ESTAB (
              cd_estabelecimento, 
              nr_total_stays, 
              nr_total_days, 
              nr_avg_days,
              nr_std_dev_days,
              nr_min_days,
              nr_max_days, 
              nr_total_discharges, 
              nr_total_deaths, 
              nr_rate_deaths)
            VALUES (
              R_STATISTICS_ESTAB(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
              R_STATISTICS_ESTAB(i).TOTAL_STAYS, --nr_total_stays, 
              R_STATISTICS_ESTAB(i).TOTAL_DAYS, --nr_total_days, 
              R_STATISTICS_ESTAB(i).AVG_DAYS, --nr_avg_days, 
              R_STATISTICS_ESTAB(i).STD_DEV_DAYS, --nr_std_dev_days, 
              R_STATISTICS_ESTAB(i).MIN_DAYS, --nr_min_days, 
              R_STATISTICS_ESTAB(i).MAX_DAYS, --nr_max_days, 
              R_STATISTICS_ESTAB(i).TOTAL_DISCHARGES, --nr_total_discharges, 
              R_STATISTICS_ESTAB(i).TOTAL_DEATHS, --nr_total_deaths, 
              R_STATISTICS_ESTAB(i).RATE_DEATHS); --nr_rate_deaths)

          END LOOP;
                
        EXCEPTION WHEN NO_DATA_FOUND THEN 
          NULL;
        END;
        COMMIT;
      EXIT WHEN C_STATISTICS_ESTAB%NOTFOUND; 
    END LOOP;
    CLOSE C_STATISTICS_ESTAB;

    OPEN C_STATISTICS_UNIT;
    LOOP
      FETCH C_STATISTICS_UNIT BULK COLLECT INTO R_STATISTICS_UNIT LIMIT 1000;
        
        BEGIN
          FOR i IN R_STATISTICS_UNIT.FIRST..R_STATISTICS_UNIT.LAST LOOP
          INSERT INTO W_STATISTICS_REPORT_UNIT (
            cd_estabelecimento, 
            cd_setor_atendimento, 
            nr_total_stays, 
            nr_total_days, 
            nr_avg_days, 
            nr_std_dev_days, 
            nr_min_days, 
            nr_max_days, 
            nr_avg_short_stay, 
            nr_std_dev_short_stay, 
            nr_min_short_stay, 
            nr_max_short_stay, 
            nr_avg_long_stay, 
            nr_std_dev_long_stay, 
            nr_min_long_stay, 
            nr_max_long_stay, 
            nr_total_admissions, 
            nr_total_readmissions, 
            nr_total_transfers, 
            nr_total_deaths, 
            nr_rate_deaths, 
            nr_avg_age, 
            nr_std_dev_age, 
            nr_min_age, 
            nr_max_age, 
            nr_total_male, 
            nr_total_female, 
            nr_total_unknown_gender, 
            nr_total_other_gender, 
            nr_total_null_gender)
          VALUES (
            R_STATISTICS_UNIT(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
            R_STATISTICS_UNIT(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
            R_STATISTICS_UNIT(i).TOTAL_STAYS, --nr_total_stays, 
            R_STATISTICS_UNIT(i).TOTAL_DAYS, --nr_total_days, 
            R_STATISTICS_UNIT(i).AVG_DAYS, --nr_avg_days, 
            R_STATISTICS_UNIT(i).STD_DEV_DAYS, --nr_std_dev_days, 
            R_STATISTICS_UNIT(i).MIN_DAYS, --nr_min_days, 
            R_STATISTICS_UNIT(i).MAX_DAYS, --nr_max_days, 
            R_STATISTICS_UNIT(i).AVG_DAYS_SHORT_STAY, --nr_avg_short_stay, 
            R_STATISTICS_UNIT(i).STD_DEV_DAYS_SHORT_STAY, --nr_std_dev_short_stay, 
            R_STATISTICS_UNIT(i).MIN_DAYS_SHORT_STAY, --nr_min_short_stay, 
            R_STATISTICS_UNIT(i).MAX_DAYS_SHORT_STAY, --nr_max_short_stay, 
            R_STATISTICS_UNIT(i).AVG_DAYS_LONG_STAY, --nr_avg_long_stay, 
            R_STATISTICS_UNIT(i).STD_DEV_LONG_STAY, --nr_std_dev_long_stay, 
            R_STATISTICS_UNIT(i).MIN_DAYS_LONG_STAY, --nr_min_long_stay, 
            R_STATISTICS_UNIT(i).MAX_DAYS_LONG_STAY, --nr_max_long_stay, 
            R_STATISTICS_UNIT(i).TOTAL_ADMISSIONS, --nr_total_admissions, 
            R_STATISTICS_UNIT(i).TOTAL_READMISSIONS, --nr_total_readmissions, 
            R_STATISTICS_UNIT(i).TOTAL_TRANSFERS, --nr_total_transfers, 
            R_STATISTICS_UNIT(i).TOTAL_DEATHS, --nr_total_deaths, 
            R_STATISTICS_UNIT(i).RATE_DEATHS, --nr_rate_deaths, 
            R_STATISTICS_UNIT(i).AVG_AGE, --nr_avg_age, 
            R_STATISTICS_UNIT(i).STD_DEV_AGE, --nr_std_dev_age, 
            R_STATISTICS_UNIT(i).MIN_AGE, --nr_min_age, 
            R_STATISTICS_UNIT(i).MAX_AGE, --nr_max_age, 
            R_STATISTICS_UNIT(i).TOTAL_MALE, --nr_total_male, 
            R_STATISTICS_UNIT(i).TOTAL_FEMALE, --nr_total_female, 
            R_STATISTICS_UNIT(i).TOTAL_UNKNOWN_GENDER, --nr_total_unknown_gender, 
            R_STATISTICS_UNIT(i).TOTAL_OTHER_GENDER, --nr_total_other_gender, 
            R_STATISTICS_UNIT(i).TOTAL_NULL_GENDER); --nr_total_null_gender)
          END LOOP;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
        COMMIT;
            
      EXIT WHEN C_STATISTICS_UNIT%NOTFOUND; 
    END LOOP;
    CLOSE C_STATISTICS_UNIT;

  END PRE_PROCESS;
  -- 
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0) IS
  BEGIN
    TABLE_CLEANING;
    PRE_PROCESS(P_START_DATE, LEAST(P_END_DATE, TRUNC(SYSDATE, 'DD')), P_CD_ESTAB, P_CD_SETOR);
  END RUN;
END PKG_STATISTICS_REPORT;
/
