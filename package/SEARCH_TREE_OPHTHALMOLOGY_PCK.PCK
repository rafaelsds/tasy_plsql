create or replace package search_tree_ophthalmology_pck as

type t_objeto_row is record (
	tree_node					number(10),
	nr_atendimento				number(10),
	nr_seq_status				number(10),
	dt_consulta					date,
	dt_inicio_consulta          date,
	ds_item						varchar2(500),
	nr_sequencia				number(10),
	nr_seq_item					number(10),
	nr_seq_consulta			number(10),
	cd_perfil					number(5),
	cd_pessoa_fisica			varchar2(10),
	ie_possui_regra_item		varchar2(15),
	ie_possui_fim_consulta	varchar2(15),
	nr_seq_apresentacao		number(10),
	ie_visualiza_inativo		varchar2(15),
	ie_quest_texto_padrao	varchar2(15),
	nr_tipo						number(1),
	nr_seq_superior			number(10),
	qt_registro					number(10),
	ds_tipo_consulta			varchar2(255),
	nr_seq_tipo_consulta		number(10),
	nr_seq_regra_form			number(10),
	ie_apresentacao			varchar2(15),
	ie_apresenta_visual_no_item	varchar2(15),
	ie_sempre_apresentar	   varchar2(15),
	ie_imprimir_liberar     varchar2(15),
	ie_forma_liberar        varchar2(15),
	cd_relatorio_regra_form	number(10),
	nr_ultima_cirurgia		number(10)
);

type t_objeto_row_data is table of t_objeto_row;
		

function get_data (	nr_level_p			number,
			ie_modo_visualizar_arvore_p	varchar2,
			nr_atendimento_p		number,
			cd_pessoa_fisica_p		varchar2,
			nr_seq_consulta_p		number,
			nr_seq_superior_p             	number,
			ie_possui_regra_item_p 		varchar2,
			ie_visualiza_canceladas_p 	varchar2 default 'N',
			ie_apenas_consultas_medico_p 	varchar2 default 'N') return t_objeto_row_data pipelined;

end search_tree_ophthalmology_pck;
/

CREATE OR REPLACE PACKAGE BODY search_tree_ophthalmology_pck AS

FUNCTION get_data (	nr_level_p							number,
			ie_modo_visualizar_arvore_p	varchar2,
			nr_atendimento_p					number,
			cd_pessoa_fisica_p				varchar2,
			nr_seq_consulta_p					number,
			nr_seq_superior_p             number,
			ie_possui_regra_item_p 			varchar2,
			ie_visualiza_canceladas_p varchar2 default 'N',
			ie_apenas_consultas_medico_p 	varchar2 default 'N') RETURN t_objeto_row_data pipelined IS

t_objeto_row_w					t_objeto_row;
cd_estabelecimento_w			number(5);
cd_perfil_w						number(5);
nm_usuario_w					varchar2(15);
ie_display_the_number_w		varchar2(15);
ie_visualizar_quantidade_w varchar2(15);
ie_possui_regra_w 			varchar2(15);
nr_seq_tipo_consulta_w		number(10);
i				               Integer := 0;
ie_fim_consulta_w          varchar2(15);
nr_seq_regra_form_w        number(10);
cd_relatorio_regra_form_w	number(10);
nr_ultima_cirurgia_w		number(10);
cd_pessoa_fisica_usuario_w	pessoa_fisica.cd_pessoa_fisica%type;

type nr_seq_superior is record (nr_seq_superior number(10));
type vetor_nr_seq_sup is table of nr_seq_superior index by binary_integer;
vetor_nr_seq_sup_w			vetor_nr_seq_sup;

cursor	medical_appointment_person is
	select	o.nr_atendimento nr_atendimento,
				o.nr_seq_status,
				o.dt_consulta,
                o.dt_inicio_consulta,
				decode(ie_display_the_number_w,'N',PKG_DATE_FORMATERS.to_varchar(dt_consulta,'shortDate',cd_estabelecimento_w,nm_usuario_w) || substr(obter_dados_oftalmologia(nr_sequencia, 'TCH'), 1, 255) ,
				PKG_DATE_FORMATERS.to_varchar(dt_consulta,'shortDate',cd_estabelecimento_w,nm_usuario_w) || ' - ' || o.nr_atendimento || substr(obter_dados_oftalmologia(nr_sequencia, 'TCN'), 1, 255))  ds_item,
				o.nr_sequencia,
				o.nr_sequencia nr_seq_consulta,
				o.cd_pessoa_fisica,
				decode(nvl(o.dt_fim_consulta, null), null,'N','S') ie_possui_fim_consulta,
				-1 nr_seq_apresentacao,
				obter_desc_tipo_consulta(o.nr_seq_tipo_consulta) ds_tipo_consulta,
            o.nr_seq_tipo_consulta,
            obter_possui_regra_item_cons(o.nr_sequencia, null) ie_possui_regra_item,
				oft_obter_formulario(o.nr_seq_tipo_consulta) nr_seq_regra_form
	from		oft_consulta o,
			atendimento_paciente a
	where		o.nr_atendimento 	= a.nr_atendimento
	and		a.cd_pessoa_fisica 	= cd_pessoa_fisica_p
	and		( (o.dt_cancelamento is null AND nvl(ie_visualiza_canceladas_p, 'N') = 'N') 
				or nvl(ie_visualiza_canceladas_p, 'S') = 'S')
	and		(cd_pessoa_fisica_usuario_w is null or o.cd_medico = cd_pessoa_fisica_usuario_w);

cursor	medical_appointment_encounter is
	SELECT	o.nr_atendimento nr_atendimento,
				o.nr_seq_status,
				o.dt_consulta,
                o.dt_inicio_consulta,
				decode(ie_display_the_number_w,'N',PKG_DATE_FORMATERS.to_varchar(dt_consulta,'shortDate',cd_estabelecimento_w,nm_usuario_w) || substr(obter_dados_oftalmologia(nr_sequencia, 'TCH'), 1, 255) ,
				PKG_DATE_FORMATERS.to_varchar(dt_consulta,'shortDate',cd_estabelecimento_w,nm_usuario_w) || ' - ' || o.nr_atendimento || substr(obter_dados_oftalmologia(nr_sequencia, 'TCN'), 1, 255))  ds_item,
				o.nr_sequencia,
				o.nr_sequencia nr_seq_consulta,
				o.cd_pessoa_fisica,
				DECODE(NVL(dt_fim_consulta, NULL), NULL, 'N', 'S') ie_possui_fim_consulta,
				-1 nr_seq_apresentacao,
				obter_desc_tipo_consulta(o.nr_seq_tipo_consulta) ds_tipo_consulta,
            o.nr_seq_tipo_consulta,
            obter_possui_regra_item_cons(o.nr_sequencia, null) ie_possui_regra_item,
				oft_obter_formulario(o.nr_seq_tipo_consulta) nr_seq_regra_form
	FROM		oft_consulta o
	WHERE		o.nr_atendimento = nr_atendimento_p
	and		( (o.dt_cancelamento is null AND nvl(ie_visualiza_canceladas_p, 'N') = 'N') 
				or nvl(ie_visualiza_canceladas_p, 'S') = 'S')
	and		(cd_pessoa_fisica_usuario_w is null or o.cd_medico = cd_pessoa_fisica_usuario_w);


cursor	separate_items	is
	select	a.nr_seq_painel tree_node,
			nvl(b.ds_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item)) ds_item,
			nvl(b.nr_seq_apresentacao,a.nr_seq_apresentacao) nr_seq_apresentacao,
			b.ie_apresentacao,
			nvl(b.ie_apresenta_visual_no_item,'S') ie_apresenta_visual_no_item,
			nvl(b.ie_sempre_apresentar,'N') ie_sempre_apresentar,
            nvl(b.ie_imprimir_liberar,'N') ie_imprimir_liberar,
            nvl(b.ie_forma_liberar,'Q') ie_forma_liberar,
			a.nr_sequencia nr_seq_item
	from   	perfil_item_oftalmologia b,
			oftalmologia_item a
	where  	((a.nr_seq_superior is null) and (b.nr_seq_superior is null))
	and    	a.nr_sequencia = b.nr_seq_item
	and    	nvl(a.ie_situacao_html5,'A') = 'A'
	and    	b.cd_perfil = cd_perfil_w
	and		a.nr_sequencia in (284,285,286,287,283,295,300);

cursor	medical_appointment_items is
	SELECT	a.nr_sequencia nr_seq_item,
				nvl(b.ds_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item)) ds_item,
				b.cd_perfil cd_perfil,
				NVL(b.ie_visualiza_inativo,'S') ie_visualiza_inativo,
				NVL(b.ie_quest_texto_padrao,'N') ie_quest_texto_padrao,
				a.nr_seq_painel tree_node,
				NVL(b.nr_seq_apresentacao,a.nr_seq_apresentacao) nr_seq_apresentacao,
				NVL(b.nr_seq_superior,a.nr_seq_superior) nr_seq_superior,
				nvl(c.amount_person,0) amount_person,
				nvl(c.amount_medical_appointment,0) amount_medical_appointment,
				decode(ie_modo_visualizar_arvore_p,'E',obter_se_visualiza_item_oft(nr_seq_consulta_p,a.nr_sequencia,''),'N') ie_visualiza,
				--obter_se_fim_consulta_oftalmo(nr_seq_consulta_p) ie_fim_consulta,
				obter_dados_oftalmologia(nr_seq_consulta_p,'TC') ds_tipo_consulta,
            decode(ie_modo_visualizar_arvore_p, 'F', get_tree_item_ophthalmology(NVL(b.nr_seq_superior,a.nr_seq_superior), cd_perfil_w, 'D'), null) ds_item_sup,
            decode(ie_modo_visualizar_arvore_p, 'F', get_tree_item_ophthalmology(NVL(b.nr_seq_superior,a.nr_seq_superior), cd_perfil_w, 'S'), null) tree_node_sup,
            decode(ie_modo_visualizar_arvore_p, 'F', get_tree_item_ophthalmology(NVL(b.nr_seq_superior,a.nr_seq_superior), cd_perfil_w, 'A'), null) nr_seq_apresentacao_sup,
				--oft_obter_formulario(nr_seq_tipo_consulta_w) nr_seq_regra_form,
            nr_seq_regra_form_w nr_seq_regra_form,
				nvl(b.ie_apresenta_visual_no_item,'S') ie_apresenta_visual_no_item,
				nvl(b.ie_sempre_apresentar,'N') ie_sempre_apresentar,
            nvl(b.ie_imprimir_liberar,'N') ie_imprimir_liberar,
            nvl(b.ie_forma_liberar,'Q') ie_forma_liberar,
			cd_relatorio_regra_form_w cd_relatorio_regra_form,
			nr_ultima_cirurgia_w nr_ultima_cirurgia
	FROM   	perfil_item_oftalmologia b,
				oftalmologia_item a,
				table(search_amount_records_pck.get_data(a.nr_sequencia,cd_pessoa_fisica_p,nr_seq_consulta_p, 0, NVL(b.ie_visualiza_inativo,'S'))) c
	WHERE  	a.nr_sequencia 					= b.nr_seq_item
	AND    	NVL(a.ie_situacao_html5,'A') 	= 'A'
	AND    	b.cd_perfil 						= cd_perfil_w
	AND		a.nr_sequencia 					not IN (284,285,286,287,283,295,300);

cursor	medical_appointment_last_items is
	SELECT	a.nr_sequencia nr_seq_item,
				nvl(b.ds_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item)) ds_item,
				b.cd_perfil cd_perfil,
				NVL(b.ie_visualiza_inativo,'S') ie_visualiza_inativo,
				NVL(b.ie_quest_texto_padrao,'N') ie_quest_texto_padrao,
				a.nr_seq_painel tree_node,
				NVL(b.nr_seq_apresentacao,a.nr_seq_apresentacao) nr_seq_apresentacao,
				NVL(b.nr_seq_superior,a.nr_seq_superior) nr_seq_superior,
				nvl(c.amount_person,0) amount_person,
				nvl(c.amount_medical_appointment,0) amount_medical_appointment,
				decode(ie_modo_visualizar_arvore_p,'E',obter_se_visualiza_item_oft(nr_seq_consulta_p,a.nr_sequencia,''),'N') ie_visualiza,
				--obter_se_fim_consulta_oftalmo(nr_seq_consulta_p) ie_fim_consulta,
				obter_dados_oftalmologia(nr_seq_consulta_p,'TC') ds_tipo_consulta,
				--oft_obter_formulario(nr_seq_tipo_consulta_w) nr_seq_regra_form,
            nr_seq_regra_form_w nr_seq_regra_form,
				nvl(b.ie_apresenta_visual_no_item,'S') ie_apresenta_visual_no_item,
				nvl(b.ie_sempre_apresentar,'N') ie_sempre_apresentar,
            nvl(b.ie_imprimir_liberar,'N') ie_imprimir_liberar,
            nvl(b.ie_forma_liberar,'Q') ie_forma_liberar,
            cd_relatorio_regra_form_w cd_relatorio_regra_form,
            nr_ultima_cirurgia_w nr_ultima_cirurgia
	FROM   	perfil_item_oftalmologia b,
				oftalmologia_item a,
				table(search_amount_records_pck.get_data(a.nr_sequencia,cd_pessoa_fisica_p,nr_seq_consulta_p, 0, NVL(b.ie_visualiza_inativo,'S'))) c
	WHERE  	a.nr_sequencia 					= b.nr_seq_item
	AND    	NVL(a.ie_situacao_html5,'A') 	= 'A'
	AND    	b.cd_perfil 						= cd_perfil_w
   AND     NVL(b.nr_seq_superior,a.nr_seq_superior) = nr_seq_superior_p
	AND		a.nr_sequencia 		not IN (284,285,286,287,283,295,300);



   function verify_seq_in_tree(nr_seq_superior_p number)
                                          return varchar2 is
   begin
      if (vetor_nr_seq_sup_w.count > 0) then
         for cont in vetor_nr_seq_sup_w.first..vetor_nr_seq_sup_w.last loop
            if (vetor_nr_seq_sup_w(cont).nr_seq_superior = nr_seq_superior_p) then
               return 'S';
            end if;
         end loop;
      end if;
      return 'N';
   end verify_seq_in_tree;

begin
cd_estabelecimento_w		:= wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w					:= wheb_usuario_pck.get_cd_perfil;
nm_usuario_w				:= wheb_usuario_pck.get_nm_usuario;
cd_relatorio_regra_form_w 	:= null;
nr_ultima_cirurgia_w 		:= null;
cd_pessoa_fisica_usuario_w 	:= null;

obter_param_usuario(3010, 115, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_display_the_number_w);
obter_param_usuario(3010, 83, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_visualizar_quantidade_w);

if (ie_apenas_consultas_medico_p = 'S') then
	cd_pessoa_fisica_usuario_w := OBTER_PESSOA_FISICA_USUARIO(nm_usuario_w, 'C');
end if;

if	(nvl(nr_seq_consulta_p,0) > 0) then
	select	max(oft_obter_formulario(nr_seq_tipo_consulta)),
            max(obter_se_fim_consulta_oftalmo(nr_seq_consulta_p)) ie_fim_consulta
	into		nr_seq_regra_form_w,
            ie_fim_consulta_w
	from		oft_consulta
	where		nr_sequencia = nr_seq_consulta_p;
end if;

if	(nvl(nr_atendimento_p,0) > 0) then
   SELECT 	MAX(a.nr_cirurgia)
	INTO 	   nr_ultima_cirurgia_w
	FROM   	cirurgia a,
            CIRURGIA_DESCRICAO b,
            atendimento_paciente c
	WHERE  	a.nr_cirurgia        = b.nr_cirurgia
   and      a.cd_pessoa_fisica   = c.cd_pessoa_fisica
   and      c.nr_atendimento     = nr_atendimento_p
	AND	   b.dt_liberacao IS NOT NULL
	AND	   b.dt_inativacao IS NULL
	AND	   a.ie_status_cirurgia = 2;
elsif (nvl(cd_pessoa_fisica_p, 0) > 0) then
	SELECT 	MAX(a.nr_cirurgia)
	INTO 	   nr_ultima_cirurgia_w
	FROM   	cirurgia a,
            CIRURGIA_DESCRICAO b
	WHERE  	a.nr_cirurgia = b.nr_cirurgia
	AND	   b.dt_liberacao IS NOT NULL
	AND	   b.dt_inativacao IS NULL
	AND	   a.ie_status_cirurgia = 2
	AND	   a.cd_pessoa_fisica = cd_pessoa_fisica_p;
end if;

if (nvl(nr_seq_regra_form_w,0) > 0) then
	SELECT 	max(cd_relatorio)
	INTO	   cd_relatorio_regra_form_w
	FROM 	   oft_formulario_regra
	WHERE 	nr_sequencia = nr_seq_regra_form_w;
end if;

case nr_level_p
	when	1 then
		FOR r_c03 IN separate_items LOOP
			BEGIN
			t_objeto_row_w.nr_atendimento				:= null;
			t_objeto_row_w.nr_seq_status				:= null;
			t_objeto_row_w.dt_consulta					:= null;
            t_objeto_row_w.dt_inicio_consulta           := null;
			t_objeto_row_w.nr_sequencia				:= null;
			t_objeto_row_w.nr_seq_consulta			:= null;
			t_objeto_row_w.cd_perfil					:= cd_perfil_w;
			t_objeto_row_w.cd_pessoa_fisica			:= null;
			t_objeto_row_w.ie_possui_fim_consulta	:= null;
			t_objeto_row_w.tree_node					:= r_c03.tree_node;
			t_objeto_row_w.ds_item						:= r_c03.ds_item;
			t_objeto_row_w.nr_seq_apresentacao		:= r_c03.nr_seq_apresentacao;
			t_objeto_row_w.nr_tipo						:= 2;
			t_objeto_row_w.ie_apresentacao			:= r_c03.ie_apresentacao;
			t_objeto_row_w.nr_seq_superior			:= null;
			t_objeto_row_w.ie_apresenta_visual_no_item	:= r_c03.ie_apresenta_visual_no_item;
			t_objeto_row_w.ie_sempre_apresentar	   := r_c03.ie_sempre_apresentar;
			t_objeto_row_w.ie_imprimir_liberar	   := r_c03.ie_imprimir_liberar;
			t_objeto_row_w.ie_forma_liberar	      := r_c03.ie_forma_liberar;
			t_objeto_row_w.nr_seq_item					:= r_c03.nr_seq_item;
			pipe ROW(t_objeto_row_w);
			END;
		END LOOP;
		if	(nvl(nr_atendimento_p,0) > 0) then
			FOR r_c02 IN medical_appointment_encounter LOOP
				BEGIN
				t_objeto_row_w.tree_node					:= 926584;
				t_objeto_row_w.nr_atendimento				:= r_c02.nr_atendimento;
				t_objeto_row_w.nr_seq_status				:= r_c02.nr_seq_status;
				t_objeto_row_w.dt_consulta					:= r_c02.dt_consulta;
                t_objeto_row_w.dt_inicio_consulta           := r_c02.dt_inicio_consulta;
				t_objeto_row_w.ds_item						:= r_c02.ds_item;
				if	(r_c02.ds_tipo_consulta is not null) then
					t_objeto_row_w.ds_item					:= r_c02.ds_item || ' - '||r_c02.ds_tipo_consulta;
				end if;
				t_objeto_row_w.nr_sequencia				:= r_c02.nr_sequencia;
				t_objeto_row_w.nr_seq_consulta			:= r_c02.nr_seq_consulta;
				t_objeto_row_w.cd_perfil					:= cd_perfil_w;
				t_objeto_row_w.cd_pessoa_fisica			:= r_c02.cd_pessoa_fisica;
				t_objeto_row_w.ie_possui_fim_consulta	:= r_c02.ie_possui_fim_consulta;
				t_objeto_row_w.nr_seq_apresentacao		:= r_c02.nr_seq_apresentacao;
				t_objeto_row_w.nr_tipo						:= 1;
				t_objeto_row_w.nr_seq_superior			:= null;
				t_objeto_row_w.ds_tipo_consulta			:= r_c02.ds_tipo_consulta;
            t_objeto_row_w.nr_seq_tipo_consulta    := r_c02.nr_seq_tipo_consulta;
            t_objeto_row_w.ie_possui_regra_item    := r_c02.ie_possui_regra_item;
				t_objeto_row_w.nr_seq_regra_form    	:= r_c02.nr_seq_regra_form;
			   t_objeto_row_w.cd_relatorio_regra_form    	:= null;
			   t_objeto_row_w.nr_ultima_cirurgia    	:= null;
				pipe ROW(t_objeto_row_w);
				END;
			END LOOP;
		elsif	(nvl(cd_pessoa_fisica_p,'0') <> '0') then
			FOR r_c01 IN medical_appointment_person LOOP
				BEGIN
				t_objeto_row_w.tree_node					:= 926584;
				t_objeto_row_w.nr_atendimento				:= r_c01.nr_atendimento;
				t_objeto_row_w.nr_seq_status				:= r_c01.nr_seq_status;
				t_objeto_row_w.dt_consulta					:= r_c01.dt_consulta;
                t_objeto_row_w.dt_inicio_consulta           := r_c01.dt_inicio_consulta;
				t_objeto_row_w.ds_item						:= r_c01.ds_item;
				if	(r_c01.ds_tipo_consulta is not null) then
					t_objeto_row_w.ds_item					:= r_c01.ds_item || ' - '||r_c01.ds_tipo_consulta;
				end if;
				t_objeto_row_w.nr_sequencia				:= r_c01.nr_sequencia;
				t_objeto_row_w.nr_seq_consulta			:= r_c01.nr_seq_consulta;
				t_objeto_row_w.cd_perfil					:= cd_perfil_w;
				t_objeto_row_w.cd_pessoa_fisica			:= r_c01.cd_pessoa_fisica;
				t_objeto_row_w.ie_possui_fim_consulta	:= r_c01.ie_possui_fim_consulta;
				t_objeto_row_w.nr_seq_apresentacao		:= r_c01.nr_seq_apresentacao;
				t_objeto_row_w.nr_tipo						:= 1;
				t_objeto_row_w.nr_seq_superior			:= null;
				t_objeto_row_w.ds_tipo_consulta			:= r_c01.ds_tipo_consulta;
            t_objeto_row_w.nr_seq_tipo_consulta    := r_c01.nr_seq_tipo_consulta;
            t_objeto_row_w.ie_possui_regra_item    := r_c01.ie_possui_regra_item;
				t_objeto_row_w.nr_seq_regra_form    	:= r_c01.nr_seq_regra_form;
			   t_objeto_row_w.cd_relatorio_regra_form    	:= null;
			   t_objeto_row_w.nr_ultima_cirurgia    	:= null;
				pipe ROW(t_objeto_row_w);
				END;
			END LOOP;
		end if;
	when	2 then
		FOR r_c04 IN medical_appointment_items LOOP
			BEGIN
				if	((r_c04.nr_seq_superior is null) and
                (((ie_modo_visualizar_arvore_p = 'E') and ((r_c04.ie_visualiza = 'S') or (r_c04.ds_tipo_consulta is null) or (ie_possui_regra_item_p = 'N'))) or
                ((ie_modo_visualizar_arvore_p = 'F') and (ie_fim_consulta_w = 'N')) or
                (ie_modo_visualizar_arvore_p = 'D'))) then
					t_objeto_row_w.nr_seq_item					:= r_c04.nr_seq_item;
					t_objeto_row_w.ds_item						:= r_c04.ds_item;
               if	(ie_visualizar_quantidade_w = 'S') and (r_c04.amount_person > 0) then
						t_objeto_row_w.ds_item					:= r_c04.ds_item ||' ('||r_c04.amount_person||')';
					end if;
					t_objeto_row_w.ie_visualiza_inativo		:= null;
					t_objeto_row_w.ie_quest_texto_padrao	:= null;
					t_objeto_row_w.tree_node					:= r_c04.tree_node;
					t_objeto_row_w.nr_seq_apresentacao		:= r_c04.nr_seq_apresentacao;
					t_objeto_row_w.cd_perfil					:= cd_perfil_w;
					t_objeto_row_w.nr_tipo						:= 3;
					t_objeto_row_w.nr_seq_superior			:= null;
					t_objeto_row_w.ds_tipo_consulta			:= null;
               t_objeto_row_w.nr_seq_tipo_consulta  	:= null;
					t_objeto_row_w.qt_registro					:= null;
					t_objeto_row_w.nr_seq_regra_form    	:= r_c04.nr_seq_regra_form;
					t_objeto_row_w.ie_apresenta_visual_no_item	:= r_c04.ie_apresenta_visual_no_item;
					t_objeto_row_w.ie_sempre_apresentar	   := r_c04.ie_sempre_apresentar;
               t_objeto_row_w.ie_imprimir_liberar	   := r_c04.ie_imprimir_liberar;
               t_objeto_row_w.ie_forma_liberar	      := r_c04.ie_forma_liberar;
			   t_objeto_row_w.cd_relatorio_regra_form    	:= r_c04.cd_relatorio_regra_form;
			   t_objeto_row_w.nr_ultima_cirurgia    	:= r_c04.nr_ultima_cirurgia;
					pipe ROW(t_objeto_row_w);
			elsif	((ie_modo_visualizar_arvore_p = 'F') and (ie_fim_consulta_w = 'S') and (r_c04.nr_seq_superior is not null) and (r_c04.amount_medical_appointment > 0) and (verify_seq_in_tree(r_c04.nr_seq_superior) = 'N')) then
               vetor_nr_seq_sup_w(i).nr_seq_superior:= r_c04.nr_seq_superior;
               i := i+1;
               t_objeto_row_w.nr_seq_item					:= r_c04.nr_seq_superior;
               t_objeto_row_w.ds_item						:= r_c04.ds_item_sup;
               t_objeto_row_w.ie_visualiza_inativo		:= null;
               t_objeto_row_w.ie_quest_texto_padrao	:= null;
               t_objeto_row_w.tree_node					:= r_c04.tree_node_sup;
               t_objeto_row_w.nr_seq_apresentacao		:= r_c04.nr_seq_apresentacao_sup;
               t_objeto_row_w.cd_perfil					:= cd_perfil_w;
               t_objeto_row_w.nr_tipo						:= 3;
               t_objeto_row_w.nr_seq_superior			:= null;
               t_objeto_row_w.ds_tipo_consulta			:= null;
               t_objeto_row_w.nr_seq_tipo_consulta    := null;
               t_objeto_row_w.qt_registro					:= null;
					t_objeto_row_w.nr_seq_regra_form    	:= r_c04.nr_seq_regra_form;
					t_objeto_row_w.ie_apresenta_visual_no_item	:= r_c04.ie_apresenta_visual_no_item;
					t_objeto_row_w.ie_sempre_apresentar	   := r_c04.ie_sempre_apresentar;
               t_objeto_row_w.ie_imprimir_liberar	   := r_c04.ie_imprimir_liberar;
               t_objeto_row_w.ie_forma_liberar	      := r_c04.ie_forma_liberar;
			   t_objeto_row_w.cd_relatorio_regra_form    	:= r_c04.cd_relatorio_regra_form;
			   t_objeto_row_w.nr_ultima_cirurgia    	:= r_c04.nr_ultima_cirurgia;
               pipe ROW(t_objeto_row_w);
            end if;
			END;
		END LOOP;
	when	3 then
		FOR r_c04 IN medical_appointment_last_items LOOP
			BEGIN
				if	(((ie_modo_visualizar_arvore_p = 'E') and ((r_c04.ie_visualiza = 'S') or (r_c04.ds_tipo_consulta is null) or (ie_possui_regra_item_p = 'N'))) or
					 ((ie_modo_visualizar_arvore_p = 'F') and ((ie_fim_consulta_w = 'N') or ((ie_fim_consulta_w = 'S') and (r_c04.amount_medical_appointment > 0)))) or
					 (ie_modo_visualizar_arvore_p = 'D')) then
					t_objeto_row_w.ds_item						:= r_c04.ds_item;
					if	(ie_visualizar_quantidade_w = 'S') and (r_c04.amount_person > 0) then
						t_objeto_row_w.ds_item					:= r_c04.ds_item ||' ('||r_c04.amount_person||')';
					end if;
					t_objeto_row_w.nr_seq_item					:= r_c04.nr_seq_item;
					t_objeto_row_w.ie_visualiza_inativo		:= r_c04.ie_visualiza_inativo;
					t_objeto_row_w.ie_quest_texto_padrao	:= r_c04.ie_quest_texto_padrao;
					t_objeto_row_w.tree_node					:= r_c04.tree_node;
					t_objeto_row_w.nr_seq_apresentacao		:= r_c04.nr_seq_apresentacao;
					t_objeto_row_w.cd_perfil					:= cd_perfil_w;
					t_objeto_row_w.nr_tipo						:= 4;
					t_objeto_row_w.nr_seq_superior			:= r_c04.nr_seq_superior;
					t_objeto_row_w.ds_tipo_consulta			:= null;
               t_objeto_row_w.nr_seq_tipo_consulta    := null;
					t_objeto_row_w.qt_registro					:= r_c04.amount_medical_appointment;
					t_objeto_row_w.nr_seq_regra_form    	:= r_c04.nr_seq_regra_form;
					t_objeto_row_w.ie_apresenta_visual_no_item	:= r_c04.ie_apresenta_visual_no_item;
					t_objeto_row_w.ie_sempre_apresentar	   := r_c04.ie_sempre_apresentar;
               t_objeto_row_w.ie_imprimir_liberar	   := r_c04.ie_imprimir_liberar;
               t_objeto_row_w.ie_forma_liberar	      := r_c04.ie_forma_liberar;
			   t_objeto_row_w.cd_relatorio_regra_form    	:= r_c04.cd_relatorio_regra_form;
			   t_objeto_row_w.nr_ultima_cirurgia    	:= r_c04.nr_ultima_cirurgia;
					pipe ROW(t_objeto_row_w);
				end if;
			END;
		END LOOP;

end case;

RETURN;

END get_data;

end search_tree_ophthalmology_pck;
/
