create or replace package get_horarios_flowsheet_pck as

type t_objeto_row is record (
	nr_sequencia			number,
	dt_horario				date,
	nr_prescricao 			number(10),
	dt_suspensao			date,
	nm_usuario_susp			usuario.ds_usuario%type,
	dt_fim_horario			date,
	dt_recusa               	date,
	ie_tipo_item			varchar2(2 char),
	qt_dose				prescr_mat_hor.qt_dose%type,
	cd_unidade_medida 		prescr_mat_hor.cd_unidade_medida%type,
	ds_dose_diferenciada 		cpoe_material.ds_dose_diferenciada%type,
	nr_seq_horario			prescr_mat_hor.nr_sequencia%type,
	ie_dose_range			cpoe_material.ie_dose_range%type,
	last_hour			date,
    dt_apap				date);

type t_objeto_row_data is table of t_objeto_row;

function get_horarios_flowsheet (	nr_atendimento_p	in	number,
						dt_inicio_p			in	date,
						dt_fim_p			in	date,
						ds_lista_itens_p	in	varchar2) return t_objeto_row_data pipelined;

end   get_horarios_flowsheet_pck;
/

create or replace PACKAGE BODY get_horarios_flowsheet_pck AS

function get_horarios_flowsheet (	nr_atendimento_p	in	number,
						dt_inicio_p			in	date,
						dt_fim_p			in	date,
						ds_lista_itens_p	in	varchar2) return t_objeto_row_data pipelined is

	ie_opcao_prescr_w	varchar2(255 char);
	item_w				varchar2(255 char);
	nr_seq_reg_item_w	cpoe_material.nr_sequencia%type;
	ie_opcao_prescr_ww	varchar2(2000 char);
	t_objeto_row_w		t_objeto_row;
	last_hour_w		date;

	PRAGMA AUTONOMOUS_TRANSACTION;

	Cursor C01  (nr_seq_item_p in number) is
		select	distinct
				b.dt_horario,
				b.nr_prescricao,
				b.dt_suspensao,
				obter_desc_usuario(b.nm_usuario_susp) nm_usuario_susp,
				b.dt_fim_horario,
				b.dt_recusa,
				c.nr_seq_mat_cpoe nr_seq_cpoe,
				b.qt_dose qt_dose,
				d.cd_unidade_medida cd_unidade_medida,
				d.ds_dose_diferenciada,
				b.nr_sequencia nr_seq_horario,
				d.ie_dose_range,
				c.cd_material, 
                b.dt_horario as dt_apap
		  from	prescr_medica a,
				prescr_mat_hor b,
				prescr_material c,
				cpoe_material d
		 where  d.nr_sequencia = c.nr_seq_mat_cpoe
		   and	c.cd_material = d.cd_material
		   and  c.nr_prescricao = b.nr_prescricao
		   and  c.nr_sequencia = b.nr_seq_material
		   and  c.nr_prescricao = a.nr_prescricao
		   and	d.nr_sequencia = nr_seq_item_p
		   and	b.dt_horario between dt_inicio_p and dt_fim_p
		   and	nvl(b.ie_horario_especial,'N') = 'N'
		   and	nvl(c.ie_administrar,'S') = 'S'
		   and	nvl(d.ie_material,'N') = 'N'
		   and	nvl(c.ie_agrupador,1) <> 4
		   and	nvl(b.ie_situacao,'A') = 'A'
        union
        select	distinct
				null dt_horario,
				null nr_prescricao,
				null dt_suspensao,
				null nm_usuario_susp,
				null dt_fim_horario,
				null dt_recusa,
				c.nr_seq_mat_cpoe nr_seq_cpoe,
				null qt_dose,
				d.cd_unidade_medida cd_unidade_medida,
				d.ds_dose_diferenciada,
				null nr_seq_horario,
				d.ie_dose_range,
				c.cd_material,
                sysdate as dt_apap
		  from	prescr_medica a,
				prescr_mat_hor b,
				prescr_material c,
				cpoe_material d
		 where  d.nr_sequencia = c.nr_seq_mat_cpoe
		   and	c.cd_material = d.cd_material
		   and  c.nr_prescricao = b.nr_prescricao
		   and  c.nr_sequencia = b.nr_seq_material
		   and  c.nr_prescricao = a.nr_prescricao
		   and	d.nr_sequencia = nr_seq_item_p
		   and	not (b.dt_horario between dt_inicio_p and dt_fim_p)
		   and	nvl(b.ie_horario_especial,'N') = 'N'
		   and	nvl(c.ie_administrar,'S') = 'S'
		   and	nvl(d.ie_material,'N') = 'N'
		   and	nvl(c.ie_agrupador,1) <> 4
		   and	nvl(b.ie_situacao,'A') = 'A'
		 order	by dt_horario, nr_prescricao;

	c01_w c01%rowtype;

	Cursor c02 (nr_seq_item_p in number) is
		select	distinct b.dt_horario,
				b.nr_prescricao,
				c.nr_seq_dieta_cpoe nr_seq_cpoe,
				b.dt_fim_horario,
				b.dt_suspensao,
				obter_desc_usuario(b.nm_usuario_susp) nm_usuario_susp,
                b.dt_horario as dt_apap
		  from	prescr_medica a,
				prescr_mat_hor b,
				prescr_material c
		 where 	c.nr_prescricao = b.nr_prescricao
		   and 	c.nr_sequencia = b.nr_seq_material
		   and 	c.nr_prescricao = a.nr_prescricao
		   and 	c.nr_seq_dieta_cpoe = nr_seq_item_p
		   and 	b.dt_horario between dt_inicio_p and dt_fim_p
		   and 	c.ie_agrupador  = 16
        union
        select	distinct null dt_horario,
				null nr_prescricao,
				c.nr_seq_dieta_cpoe nr_seq_cpoe,
				null dt_fim_horario,
				null dt_suspensao,
				null nm_usuario_susp,
                sysdate as dt_apap
		  from	prescr_medica a,
				prescr_mat_hor b,
				prescr_material c
		 where 	c.nr_prescricao = b.nr_prescricao
		   and 	c.nr_sequencia = b.nr_seq_material
		   and 	c.nr_prescricao = a.nr_prescricao
		   and 	c.nr_seq_dieta_cpoe = nr_seq_item_p
		   and 	not (b.dt_horario between dt_inicio_p and dt_fim_p)
		   and 	c.ie_agrupador  = 16
		 order 	by dt_horario, nr_prescricao;

	c02_w c02%rowtype;

	Cursor c03 (nr_seq_item_p in number) is
		select  distinct b.dt_horario,
				b.nr_prescricao,
				b.dt_suspensao,
				obter_desc_usuario(b.nm_usuario_susp) nm_usuario_susp,
				b.dt_fim_horario,
				c.nr_seq_dieta_cpoe nr_seq_cpoe,
                b.dt_horario as dt_apap
		  from	prescr_medica a,
				prescr_mat_hor b,
				prescr_material c
		 where	c.nr_prescricao = b.nr_prescricao
		   and	c.nr_sequencia = b.nr_seq_material
		   and 	c.nr_prescricao = a.nr_prescricao
		   and	c.nr_seq_dieta_cpoe = nr_seq_item_p
		   and	b.dt_horario between dt_inicio_p and dt_fim_p
        union
        select  distinct null dt_horario,
				null nr_prescricao,
				null dt_suspensao,
				null nm_usuario_susp,
				null dt_fim_horario,
				c.nr_seq_dieta_cpoe nr_seq_cpoe,
                sysdate as dt_apap
		  from	prescr_medica a,
				prescr_mat_hor b,
				prescr_material c
		 where	c.nr_prescricao = b.nr_prescricao
		   and	c.nr_sequencia = b.nr_seq_material
		   and 	c.nr_prescricao = a.nr_prescricao
		   and	c.nr_seq_dieta_cpoe = nr_seq_item_p
		   and	not (b.dt_horario between dt_inicio_p and dt_fim_p)
		 order 	by dt_horario, nr_prescricao;

	c03_w c03%rowtype;

begin
	ie_opcao_prescr_ww := replace(ds_lista_itens_p, ' ', '');

	while (ie_opcao_prescr_ww is not null) loop

		if	(instr(ie_opcao_prescr_ww,';') = 0) and
			(ie_opcao_prescr_ww is not null) then
			ie_opcao_prescr_ww	:= ie_opcao_prescr_ww||';';
		end if;

		item_w := substr(ie_opcao_prescr_ww, 1, instr(ie_opcao_prescr_ww,';') - 1);
		ie_opcao_prescr_w := substr(item_w, 1, instr(item_w,',') - 1);
		nr_seq_reg_item_w := to_number(substr(item_w, instr(item_w,',') + 1, length(item_w)));
		ie_opcao_prescr_ww := substr(ie_opcao_prescr_ww, instr(ie_opcao_prescr_ww, ';') + 1, length(ie_opcao_prescr_ww));

		if	(ie_opcao_prescr_w = 'M') or
			(ie_opcao_prescr_w = 'L') or
			(ie_opcao_prescr_w = 'DS') then

			last_hour_w := null;	

			case TRIM(ie_opcao_prescr_w)	
				when 'M' then	--Medicamento
					begin
						for c01_w in c01(nr_seq_reg_item_w) loop
						begin

							if (last_hour_w is null) then
								select 	max(b.dt_horario)
								into	last_hour_w
								from 	prescr_material a,
									prescr_mat_hor b
								where 	a.nr_prescricao = b.nr_prescricao
								and 	a.nr_sequencia = b.nr_seq_material
								and 	a.nr_seq_mat_cpoe = c01_w.nr_seq_cpoe
								and 	a.cd_material = c01_w.cd_material;
							end if;

							t_objeto_row_w.dt_horario		:=  c01_w.dt_horario;
							t_objeto_row_w.nr_prescricao	:=  c01_w.nr_prescricao;
							t_objeto_row_w.dt_suspensao		:=  c01_w.dt_suspensao;
							t_objeto_row_w.nm_usuario_susp	:=  c01_w.nm_usuario_susp;
							t_objeto_row_w.dt_fim_horario	:=  c01_w.dt_fim_horario;
							t_objeto_row_w.dt_recusa	    :=  c01_w.dt_recusa;
							t_objeto_row_w.nr_sequencia		:=  c01_w.nr_seq_cpoe;
							t_objeto_row_w.ie_tipo_item		:=  'M';
							t_objeto_row_w.qt_dose			:=  c01_w.qt_dose;
							t_objeto_row_w.cd_unidade_medida :=  c01_w.cd_unidade_medida;
							t_objeto_row_w.ds_dose_diferenciada :=  c01_w.ds_dose_diferenciada;
							t_objeto_row_w.nr_seq_horario :=  c01_w.nr_seq_horario;
							t_objeto_row_w.ie_dose_range := c01_w.ie_dose_range;
							t_objeto_row_w.last_hour :=  last_hour_w;
                            t_objeto_row_w.dt_apap :=  c01_w.dt_apap;
							pipe row (t_objeto_row_w);
						end;
						end loop;
					exception
					when others then
						apap_dinamico_pck.gera_log_apap('get_horarios - Medicamento: '||sqlerrm);
					end;
				when 'L' then -- Leites e Derivados
					begin
						for c02_w in c02(nr_seq_reg_item_w) loop
						begin
							t_objeto_row_w.dt_horario		:=  c02_w.dt_horario;
							t_objeto_row_w.nr_prescricao	:=  c02_w.nr_prescricao;
							t_objeto_row_w.dt_suspensao		:=  c02_w.dt_suspensao;
							t_objeto_row_w.nm_usuario_susp	:=  c02_w.nm_usuario_susp;
							t_objeto_row_w.dt_fim_horario	:=  c02_w.dt_fim_horario;
							t_objeto_row_w.nr_sequencia		:=  c02_w.nr_seq_cpoe;
							t_objeto_row_w.ie_tipo_item		:=  'L';
							t_objeto_row_w.qt_dose			:=  0;
							t_objeto_row_w.cd_unidade_medida :=  null;
							t_objeto_row_w.ds_dose_diferenciada :=  null;
                            t_objeto_row_w.dt_apap :=  c02_w.dt_apap;
							pipe row (t_objeto_row_w);
						end;
						end loop;
					exception
					when others then
						apap_dinamico_pck.gera_log_apap('get_horarios - Leite: '||sqlerrm);
					end;
				when 'DS' then --Suplemento
					begin
						for c03_w in c03(nr_seq_reg_item_w) loop
						begin
							t_objeto_row_w.dt_horario		:=  c03_w.dt_horario;
							t_objeto_row_w.nr_prescricao	:=  c03_w.nr_prescricao;
							t_objeto_row_w.dt_suspensao		:=  c03_w.dt_suspensao;
							t_objeto_row_w.nm_usuario_susp	:=  c03_w.nm_usuario_susp;
							t_objeto_row_w.dt_fim_horario	:=  c03_w.dt_fim_horario;
							t_objeto_row_w.nr_sequencia		:=  c03_w.nr_seq_cpoe;
							t_objeto_row_w.ie_tipo_item		:=  'DS';
							t_objeto_row_w.qt_dose			:=  0;
							t_objeto_row_w.cd_unidade_medida :=  null;
							t_objeto_row_w.ds_dose_diferenciada :=  null;
                            t_objeto_row_w.dt_apap :=  c03_w.dt_apap;
							pipe row (t_objeto_row_w);
						end;
						end loop;
					exception
					when others then
						apap_dinamico_pck.gera_log_apap('get_horarios - Sup: '||sqlerrm);
					end;
			end case;
		end if;
	end loop;
return;

end get_horarios_flowsheet;

end get_horarios_flowsheet_pck;
/
