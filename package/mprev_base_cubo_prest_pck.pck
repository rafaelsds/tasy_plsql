create or replace 
package mprev_base_cubo_prest_pck as
     
-- Atualiza a base de dados do cubo.
procedure atualizar_base(	dt_referencia_p		date,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type);

procedure gerencia_integridade(	nm_tabela_p	varchar2,
				ds_acao_p	varchar2);
				
end mprev_base_cubo_prest_pck;
/

create or replace
package body mprev_base_cubo_prest_pck as

qt_reg_commit_w		pls_integer := 5000;

type dados_regra_benef is record (
	nr_sequencia		mprev_regra_dados_cubo_ben.nr_sequencia%type,
	ie_preco		mprev_regra_dados_cubo_ben.ie_preco%type,
	ie_situacao_contrato	mprev_regra_dados_cubo_ben.ie_situacao_contrato%type,
	ie_situacao_atend	mprev_regra_dados_cubo_ben.ie_situacao_atend%type
      );
    
type table_pessoa is record (
	nr_sequencia            dbms_sql.number_table,
	dt_atualizacao          dbms_sql.date_table,
	nm_usuario              dbms_sql.varchar2_table,
	cd_pessoa_fisica        dbms_sql.varchar2_table,
	nm_pessoa_fisica        dbms_sql.varchar2_table,
	dt_nascimento           dbms_sql.date_table,
	ie_sexo			dbms_sql.varchar2_table
);

procedure gerencia_integridade(	nm_tabela_p	varchar2,
				ds_acao_p	varchar2) is
begin

-- Varrer as integridades que forem FK's da tabela.
for rw_integridade in (	select	cons.constraint_name nm_constraint
			from	user_constraints cons
			where	upper(cons.table_name) = upper(nm_tabela_p)
			and	cons.constraint_type = 'R') loop

	execute immediate 'alter table ' || nm_tabela_p || ' ' || ds_acao_p || ' constraint ' || rw_integridade.nm_constraint;

end loop; -- Integridades

end gerencia_integridade;

procedure limpar_base is

begin

-- Desabilitar FK's
gerencia_integridade('mprev_pop_alvo_selecao', 'DISABLE');
gerencia_integridade('mprev_conta_proc_cubo_ops', 'DISABLE');
gerencia_integridade('mprev_guia_proc_cubo_ops', 'DISABLE');
gerencia_integridade('mprev_diagnostico_cubo_ops', 'DISABLE');
gerencia_integridade('mprev_conta_cubo_ref', 'DISABLE');
gerencia_integridade('MPREV_GUIA_CUBO_REF', 'DISABLE');
gerencia_integridade('mprev_benef_custo_cubo_ops', 'DISABLE');
gerencia_integridade('mprev_conta_cubo_ops', 'DISABLE');
gerencia_integridade('mprev_guia_plano_cubo_ops', 'DISABLE'); 
gerencia_integridade('mprev_benef_cubo_ops', 'DISABLE');
gerencia_integridade('mprev_compl_pessoa_cubo', 'DISABLE');
gerencia_integridade('mprev_pessoa_cubo_ops', 'DISABLE');

-- Limpeza populacao alvo.
execute immediate 'truncate table mprev_pop_alvo_selecao';

-- Procedimentos
execute immediate 'truncate table mprev_conta_proc_cubo_ops';

-- Procedimentos guia
execute immediate 'truncate table mprev_guia_proc_cubo_ops';

-- Diagnostico.
execute immediate 'truncate table mprev_diagnostico_cubo_ops';

-- Contas
execute immediate 'truncate table mprev_conta_cubo_ref';

-- Referencias das guias
execute immediate 'truncate table MPREV_GUIA_CUBO_REF';

-- Custos
execute immediate 'truncate table mprev_benef_custo_cubo_ops';

--  Contas
execute immediate 'truncate table mprev_conta_cubo_ops';

-- guias
execute immediate 'truncate table mprev_guia_plano_cubo_ops'; 

-- Beneficiarios
execute immediate 'truncate table mprev_benef_cubo_ops';

-- Endereco pessoa fisica
execute immediate 'truncate table mprev_compl_pessoa_cubo';

-- Pessoas
execute immediate 'truncate table mprev_pessoa_cubo_ops';

-- Rehabilitar FK's;
gerencia_integridade('mprev_pop_alvo_selecao', 'ENABLE');
gerencia_integridade('mprev_conta_proc_cubo_ops', 'ENABLE');
gerencia_integridade('mprev_guia_proc_cubo_ops', 'ENABLE'); 
gerencia_integridade('mprev_diagnostico_cubo_ops', 'ENABLE');
gerencia_integridade('mprev_conta_cubo_ref', 'ENABLE');
gerencia_integridade('MPREV_GUIA_CUBO_REF', 'ENABLE'); 
gerencia_integridade('mprev_benef_custo_cubo_ops', 'ENABLE');
gerencia_integridade('mprev_conta_cubo_ops', 'ENABLE');
gerencia_integridade('mprev_guia_plano_cubo_ops', 'ENABLE'); -- @
gerencia_integridade('mprev_benef_cubo_ops', 'ENABLE');
gerencia_integridade('mprev_compl_pessoa_cubo', 'ENABLE');
gerencia_integridade('mprev_pessoa_cubo_ops', 'ENABLE');

end limpar_base;

procedure atualizar_pessoas(	dt_inicio_w date,
								dt_fim_w	date,
								nm_usuario_p	usuario.nm_usuario%type) is

cursor c_pessoas (dt_inicio_p date,
					dt_fim_p date) is
select distinct
		pessoa.cd_pessoa_fisica,
		pessoa.dt_nascimento dt_nascimento,
		pessoa.ie_sexo ie_sexo,
		pessoa.nm_pessoa_fisica nm_pessoa_fisica
from	pessoa_fisica		pessoa,
		compl_pessoa_fisica		compl
where	pessoa.cd_pessoa_fisica = compl.cd_pessoa_fisica
and		compl.ie_tipo_complemento = 1
and		pessoa.dt_nascimento is not null
and		pessoa.dt_atualizacao between dt_inicio_p and dt_fim_p;

				
ds_select_completo_w	varchar2(4000);	

cursor_benef_w		sql_pck.t_cursor;
table_pessoa_w		mprev_base_cubo_prest_pck.table_pessoa;
ie_origem_dados_w	mprev_config_geral.ie_origem_dados%type;
valor_bind_w	sql_pck.t_dado_bind;

begin

open c_pessoas(dt_inicio_w,dt_fim_w);
	loop
		fetch c_pessoas bulk collect 
		into	table_pessoa_w.cd_pessoa_fisica, table_pessoa_w.dt_nascimento,
		table_pessoa_w.ie_sexo, table_pessoa_w.nm_pessoa_fisica
	limit qt_reg_commit_w;
	
	exit when table_pessoa_w.cd_pessoa_fisica.count = 0;
	-- Criar pessoas dos beneficiarios
	forall i in table_pessoa_w.cd_pessoa_fisica.first .. table_pessoa_w.cd_pessoa_fisica.last
		insert into mprev_pessoa_cubo_ops
			(nr_sequencia, nm_usuario, dt_atualizacao,
			cd_pessoa_fisica, nm_pessoa_fisica, dt_nascimento, 
			ie_sexo)
		values	(mprev_pessoa_cubo_ops_seq.nextval, nm_usuario_p, sysdate, 
			table_pessoa_w.cd_pessoa_fisica(i), table_pessoa_w.nm_pessoa_fisica(i), 
			table_pessoa_w.dt_nascimento(i), table_pessoa_w.ie_sexo(i));
	commit;
end loop;
close c_pessoas;

end atualizar_pessoas;

procedure atualizar_base(	dt_referencia_p		date,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

dt_inicio_w		date;
dt_fim_w		date;
nr_sequencia_w	mprev_regra_dados_cubo_ops.nr_sequencia%type;
qt_meses_atras_w	mprev_regra_dados_cubo_ops.qt_meses_atras%type;

begin

select	a.nr_sequencia,
		nvl(a.qt_meses_atras, 12) qt_meses_atras
into	nr_sequencia_w,
		qt_meses_atras_w
from	mprev_regra_dados_cubo_ops a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

limpar_base; 

dt_inicio_w	:= pkg_date_utils.add_month(pkg_date_utils.start_of(nvl(dt_referencia_p, sysdate),'MONTH', 0),-qt_meses_atras_w, 0);
dt_fim_w	:= fim_dia(pkg_date_utils.end_of(nvl(dt_referencia_p, sysdate), 'MONTH', 0));

-- Atualizar a base de pessoas e beneficiarios.
atualizar_pessoas(dt_inicio_w, dt_fim_w, nm_usuario_p);
	
end atualizar_base;

end mprev_base_cubo_prest_pck;
/
