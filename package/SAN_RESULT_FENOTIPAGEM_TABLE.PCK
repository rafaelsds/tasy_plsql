create or replace PACKAGE SAN_RESULT_FENOTIPAGEM_TABLE AS

type result_fenotimagem_row is record (
nr_seq_transfusao san_transfusao.nr_sequencia%type,
	nr_sistema_sang VARCHAR2 (10),
    nr_antigeno VARCHAR2 (10),
    ds_resultado varchar2 (10)
);

type result_fenotimagem_tb is table of result_fenotimagem_row;

FUNCTION RETURN_FENOTIPAGEM(nr_seq_lote_p san_lote_item.nr_seq_lote%type)
   RETURN result_fenotimagem_tb pipelined;

end SAN_RESULT_FENOTIPAGEM_TABLE;
/

create or replace package body SAN_RESULT_FENOTIPAGEM_TABLE as

    FUNCTION RETURN_FENOTIPAGEM(nr_seq_lote_p  san_lote_item.nr_seq_lote%type)
       RETURN result_fenotimagem_tb pipelined
    IS
    
        result_fenotimagem_w result_fenotimagem_row;
        aux_w varchar2(4000);
        
        cursor C01 is
        SELECT distinct 
            a.cd_pessoa_fisica, d.nr_seq_lote, a.nr_sequencia,
            a.nr_seq_transfusao, b.ie_tipo_sangue, DS_M, DS_N, 
            DS_S, DS_S_MIN, DS_P1, DS_D,
            DS_C, DS_E, DS_C_MIN, DS_E_MIN, DS_F_MIN, DS_CW, DS_LUA,
            DS_LUB, DS_K, DS_K_MIN, DS_KPA, DS_KPB, DS_JSA, DS_JSB,
            DS_LEA, DS_LEB, DS_FYA, DS_FYB, DS_DIA, dt_liberacao
        FROM SAN_RESULT_FENOTIPAGEM a,
            pessoa_fisica b,
            san_lote_item d
        where a.cd_pessoa_fisica = b.cd_pessoa_fisica
        and a.nr_seq_transfusao = d.nr_seq_trans
        and d.nr_seq_lote = nr_seq_lote_p
        and a.dt_liberacao is not null
        order by dt_liberacao desc;

    BEGIN
        aux_w := '00';
        For r_C01 in C01 loop
	
	        result_fenotimagem_w.nr_seq_transfusao := r_C01.nr_seq_transfusao;
	
            if(r_C01.ie_tipo_sangue = 'A') then
                result_fenotimagem_w.nr_sistema_sang := '001';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS'; 
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.ie_tipo_sangue = 'B') then
                result_fenotimagem_w.nr_sistema_sang := '001';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'POS';     
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.ie_tipo_sangue = 'AB') then
                result_fenotimagem_w.nr_sistema_sang := '001';
                result_fenotimagem_w.nr_antigeno := '003';
                result_fenotimagem_w.ds_resultado := 'POS';     
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.ie_tipo_sangue = 'A1') then
                result_fenotimagem_w.nr_sistema_sang := '001';
                result_fenotimagem_w.nr_antigeno := '004';
                result_fenotimagem_w.ds_resultado := 'POS';     
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_M = 1 and instr(aux_w,'002,001') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '002';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '002,001;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_M = 0 and instr(aux_w,'002,001') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '002';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '002,001;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_N = 1 and instr(aux_w,'002,002') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '002';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '002,002;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_N = 0 and instr(aux_w,'002,002') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '002';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '002,002;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_S = 1 and instr(aux_w,'002,003') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '002';
                result_fenotimagem_w.nr_antigeno := '003';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '002,003;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_S = 0 and instr(aux_w,'002,003') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '002';
                result_fenotimagem_w.nr_antigeno := '003';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '002,003;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_S_MIN = 1 and instr(aux_w,'002,004') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '002';
                result_fenotimagem_w.nr_antigeno := '004';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '002,004;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_S_MIN = 0 and instr(aux_w,'002,004') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '002';
                result_fenotimagem_w.nr_antigeno := '004';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '002,004;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_P1 = 1 and instr(aux_w,'003,001') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '003';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '003,001;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_P1 = 0 and instr(aux_w,'003,001') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '003';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '003,001;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_D = 1 and instr(aux_w,'004,001') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '004,001;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_D = 0 and instr(aux_w,'004,001') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '004,001;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_C = 1 and instr(aux_w,'004,002') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '004,002;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_C = 0 and instr(aux_w,'004,002') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '004,002;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_E = 1 and instr(aux_w,'004,003') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '003';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '004,003;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_E = 0 and instr(aux_w,'004,003') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '003';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '004,003;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_C_MIN = 1 and instr(aux_w,'004,004') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '004';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '004,004;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_C_MIN = 0 and instr(aux_w,'004,004') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '004';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '004,004;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_E_MIN = 1 and instr(aux_w,'004,005') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '005';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '004,005;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_E_MIN = 0 and instr(aux_w,'004,005') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '005';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '004,005;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_F_MIN = 1 and instr(aux_w,'004,006') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '006';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '004,006;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_F_MIN = 0 and instr(aux_w,'004,006') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '006';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '004,006;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_CW = 1 and instr(aux_w,'004,008') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '008';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '004,008;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_CW = 0 and instr(aux_w,'004,008') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '004';
                result_fenotimagem_w.nr_antigeno := '008';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '004,008;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_LUA = 1 and instr(aux_w,'005,001') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '005';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '005,001;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_LUA = 0 and instr(aux_w,'005,001') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '005';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '005,001;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_LUB = 1 and instr(aux_w,'005,002') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '005';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '005,002;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_LUB = 0 and instr(aux_w,'005,002') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '005';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '005,002;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_K = 1 and instr(aux_w,'006,001') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '006,001;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_K = 0 and instr(aux_w,'006,001') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '006,001;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_K_MIN = 1 and instr(aux_w,'006,002') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '006,002;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_K_MIN = 0 and instr(aux_w,'006,002') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '006,002;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_KPA = 1 and instr(aux_w,'006,003') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '003';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '006,003;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_KPA = 0 and instr(aux_w,'006,003') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '003';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '006,003;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_KPB = 1 and instr(aux_w,'006,004') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '004';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '006,004;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_KPB = 0 and instr(aux_w,'006,004') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '004';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '006,004;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_JSA = 1 and instr(aux_w,'006,006') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '006';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '006,006;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_JSA = 0 and instr(aux_w,'006,006') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '006';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '006,006;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_JSB = 1 and instr(aux_w,'006,007') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '007';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '006,007;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_JSB = 0 and instr(aux_w,'006,007') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '006';
                result_fenotimagem_w.nr_antigeno := '007';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '006,007;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_LEA = 1 and instr(aux_w,'007,001') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '007';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '007,001;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_LEA = 0 and instr(aux_w,'007,001') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '007';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '007,001;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_LEB = 1 and instr(aux_w,'007,002') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '007';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '007,002;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_LEB = 0 and instr(aux_w,'007,002') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '007';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '007,002;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_FYA = 1 and instr(aux_w,'008,001') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '008';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '008,001;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_FYA = 0 and instr(aux_w,'008,001') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '008';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '008,001;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_FYB = 1 and instr(aux_w,'008,002') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '008';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '008,002;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_FYB = 0 and instr(aux_w,'008,002') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '008';
                result_fenotimagem_w.nr_antigeno := '002';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '008,002;';
                PIPE ROW (result_fenotimagem_w);
            end if;
            if(r_C01.DS_DIA = 1 and instr(aux_w,'010,001') = '0') then 
                result_fenotimagem_w.nr_sistema_sang := '010';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'POS';     
                aux_w := aux_w || '010,001;';
                PIPE ROW (result_fenotimagem_w);
            elsif(r_C01.DS_DIA = 0 and instr(aux_w,'010,001') = '0') then
                result_fenotimagem_w.nr_sistema_sang := '010';
                result_fenotimagem_w.nr_antigeno := '001';
                result_fenotimagem_w.ds_resultado := 'NEG';     
                aux_w := aux_w || '010,001;';
                PIPE ROW (result_fenotimagem_w);
            end if;
        end loop; 
        RETURN;
    end RETURN_FENOTIPAGEM;
end SAN_RESULT_FENOTIPAGEM_TABLE;
/
