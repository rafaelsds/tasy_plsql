create or replace package tiss_ins_ret_denial_pck as

/*
	Type that represents a PROCEDIMENTO_PACIENTE record.
	Refers to the medical procedures that were executed for a certain patient account.
*/

type procedimento is record (
	nr_sequencia			procedimento_paciente.nr_sequencia%type,
	cd_procedimento			procedimento_paciente.cd_procedimento%type,
	cd_procedimento_tuss		procedimento_paciente.cd_procedimento_tuss%type,
	cd_procedimento_convenio 	procedimento_paciente.cd_procedimento_convenio%type,
	cd_proc_tiss			proc_paciente_tiss.vl_campo%type,
	cd_proc_tiss_xml		proc_paciente_tiss.vl_campo%type,
	ie_origem_proced		procedimento_paciente.ie_origem_proced%type,
	cd_edicao_amb			procedimento_paciente.cd_edicao_amb%type,
	cd_setor_atendimento		procedimento_paciente.cd_setor_atendimento%type,
	nr_doc_convenio			procedimento_paciente.nr_doc_convenio%type,
	dt_procedimento			procedimento_paciente.dt_procedimento%type,
	dt_conta			procedimento_paciente.dt_conta%type,
	vl_procedimento			procedimento_paciente.vl_procedimento%type,
	vl_medico			procedimento_paciente.vl_medico%type,
	ds_proc_tiss			procedimento_paciente.ds_proc_tiss%type,
	ie_funcao_medico		procedimento_participante.ie_funcao%type,
	ie_status			varchar2(1)
);

/*
	Type that represents a MATERIAL_ATEND_PACIENTE record.
	Refers to the medical medicines and materials that were used for a certain patient account.
*/

type material is record (
	nr_sequencia		material_atend_paciente.nr_sequencia%type,
	cd_material		material_atend_paciente.cd_material%Type,
	cd_material_tiss	material_atend_paciente.cd_material_tiss%type,
	cd_material_tuss	material_atend_paciente.cd_material_tuss%type,
	cd_material_convenio	material_atend_paciente.cd_material_convenio%type,
	cd_edicao_amb		procedimento_paciente.cd_edicao_amb%type,
	cd_setor_atendimento	material_atend_paciente.cd_setor_atendimento%type,
	nr_doc_convenio		material_atend_paciente.nr_doc_convenio%type,
	dt_atendimento		material_atend_paciente.dt_atendimento%type,
	dt_conta		material_atend_paciente.dt_conta%type,
	vl_material		material_atend_paciente.vl_material%type,
	vl_unitario		material_atend_paciente.vl_unitario%type,	
	ie_status		varchar2(1)
);

/* 
	Table of procedimento type.
*/
type proc_v is table of procedimento index by pls_integer;


/*	
	Table of material type.
*/
type mat_v is table of material index by pls_integer;

proc_w	proc_v;
mat_w	mat_v;

/*
	Insurance return form sequence. Received from backend through TISS_INS_RET_DENIAL_LOAD_ITEMS procedure.
*/
nr_seq_guia_w	convenio_retorno_item.nr_sequencia%type;

/*
	Method responsible for reading all medical procedures, materials and medicines from a certain patient account and billing form.
	These records are used to conciliate the data that were previously billed (TISS_CONTA_PROC, TISS_CONTA_DESP) on the feature Insurance Protocol.
*/
procedure load_items(nr_seq_guia_p in convenio_retorno_item.nr_sequencia%type);
		
/*
	Method responsible to try to conciliate a certain billed item, tracing it back to a patient account item.
	If an item is conciliated, that being it was traced back to the patient account, it is created as a denied item.
*/
procedure generate(
		nr_seq_item_fatur_p	in number,
		ie_proc_desp_p		in varchar2,
		cd_item_p 		in varchar2, 
		dt_item_p 		in date, 
		vl_item_p 		in number,
		qt_item_p		in number,
		qt_item_orig_p		in number,
		cd_setor_p		in setor_atendimento.cd_setor_atendimento%type,
		vl_payment_p		in number,
		ie_funcao_medico_p	in varchar2
		);
		
/*
	Releases package memory.
*/
procedure free_pck_mem;

/*
	Method responsible for calculating a item total paid value.
	Queries all payments and accepted denials on features Insurance Return and Denial Appeal Management.
*/
function get_item_paid_value(
		nr_seq_tiss_conta_proc_p in number,
		nr_seq_tiss_conta_desp_p in number
		) return number;

/*
	Method responsible for calculating a item balance.
	Substracts the paid value from the item original billed.
*/
function get_item_balance(
		nr_seq_item_fatur_p 	in number,
		ie_proc_desp_p 		in varchar2) return number;

end tiss_ins_ret_denial_pck;
/

create or replace package body tiss_ins_ret_denial_pck as

procedure load_items(nr_seq_guia_p in convenio_retorno_item.nr_sequencia%type) as

/* 
	Counter. 
*/
i pls_integer;

/*
	Patient account number.
*/
nr_interno_conta_w	conta_paciente.nr_interno_conta%type;
/*
	Billing form identifier.
*/
cd_autorizacao_w	conta_paciente_guia.cd_autorizacao%Type;

/*
	Procedure code generated during billing generation.
*/
cd_proc_tiss_w		proc_paciente_tiss.vl_campo%type;
		
/*	Patient account's medical procedures. */
cursor procedimentos is
select	nr_sequencia,
	ie_origem_proced,
	cd_procedimento,	
	cd_procedimento_tuss,
	cd_procedimento_convenio,
	dt_procedimento,
	dt_conta,
	cd_setor_atendimento,
	vl_procedimento,
	vl_medico,
	vl_custo_operacional,
	cd_edicao_amb cd_edicao,
	ie_funcao_medico,  
	nvl(nr_doc_convenio,cd_autorizacao_w) 	nr_doc_convenio
from	procedimento_paciente
where	nr_interno_conta  			= nr_interno_conta_w
and	nvl(nr_doc_convenio,cd_autorizacao_w) 	= cd_autorizacao_w
and	cd_motivo_exc_conta 			is null
and	qt_procedimento 			> 0
union
select	p.nr_sequencia,
	p.cd_procedimento,
	p.ie_origem_proced,
	p.cd_procedimento_tuss,
	p.cd_procedimento_convenio,
	p.dt_procedimento,
	p.dt_conta,
	p.cd_setor_atendimento,
	pp.vl_conta,
	pp.vl_participante,
	0 vl_custo_operacional,
	p.cd_edicao_amb cd_edicao,
	pp.ie_funcao ie_funcao_medico,
	nvl(nr_doc_convenio,cd_autorizacao_w) 	nr_doc_convenio
from	procedimento_paciente p,
	procedimento_participante pp
where	p.nr_interno_conta  				= nr_interno_conta_w
and	p.nr_sequencia 					= pp.nr_sequencia
and	nvl(nr_doc_convenio,cd_autorizacao_w) 	= cd_autorizacao_w
and	p.cd_motivo_exc_conta 				is null
and	p.qt_procedimento 				> 0
order by 
	cd_procedimento, 
	nr_sequencia desc;

/*	Patient account's medical materials and medicines. */	
cursor materiais is
select	a.nr_sequencia 				nr_sequencia,
	a.cd_material 				cd_material, 
	a.cd_material_tiss 			cd_material_tiss,
	a.cd_material_tuss 			cd_material_tuss,
	a.cd_material_convenio 			cd_material_convenio,
	a.dt_atendimento 			dt_atendimento,
	a.dt_conta 				dt_conta,
	a.vl_material 				vl_material,
	a.qt_material 				qt_material,
	a.vl_unitario 				vl_unitario,
	nvl(tiss_obter_codigo_tabela(a.nr_seq_tiss_tabela),'00') cd_edicao,
	nvl(nr_doc_convenio,cd_autorizacao_w) 	nr_doc_convenio,
	a.cd_setor_atendimento			cd_setor_atendimento
from	material_atend_paciente a
where	a.nr_interno_conta 			= nr_interno_conta_w
and	nvl(nr_doc_convenio,cd_autorizacao_w) 	= cd_autorizacao_w
and	cd_motivo_exc_conta 			is null
and	qt_material 				> 0
order by 
	cd_material, 
	nr_sequencia desc;
	
begin

	/* Find the patient account number and billing form identifier. */
	select	max(nr_interno_conta),
		max(cd_autorizacao)
	into	nr_interno_conta_w,
		cd_autorizacao_w
	from	convenio_retorno_item
	where	nr_sequencia = nr_seq_guia_p;
	
	nr_seq_guia_w := nr_seq_guia_p;
	
	
	/* Populate procedimento table proc_w for each patient account's medical procedure. */
	i := 0;
	for procedimento in procedimentos loop
		proc_w(i).nr_sequencia 			:= procedimento.nr_sequencia;
		proc_w(i).cd_procedimento 		:= procedimento.cd_procedimento;
		proc_w(i).cd_procedimento_tuss 		:= procedimento.cd_procedimento_tuss;
		proc_w(i).cd_procedimento_convenio 	:= procedimento.cd_procedimento_convenio;
		proc_w(i).cd_edicao_amb 		:= procedimento.cd_edicao;
		proc_w(i).cd_setor_atendimento 		:= procedimento.cd_setor_atendimento;
		proc_w(i).nr_doc_convenio		:= procedimento.nr_doc_convenio;
		proc_w(i).dt_procedimento		:= procedimento.dt_procedimento;
		proc_w(i).dt_conta			:= procedimento.dt_conta;
		proc_w(i).vl_procedimento		:= procedimento.vl_procedimento;
		proc_w(i).vl_medico			:= procedimento.vl_medico;
		proc_w(i).ie_funcao_medico		:= procedimento.ie_funcao_medico;
		proc_w(i).ie_origem_proced		:= procedimento.ie_origem_proced;
		proc_w(i).ie_status			:= 'N';
		
		select 	max(vl_campo)
		into	cd_proc_tiss_w
		from	proc_paciente_tiss
		where	nr_seq_procedimento = proc_w(i).nr_sequencia
		and	ds_campo = 'CD_PROC_TISS';
		
		if cd_proc_tiss_w is not null then
			proc_w(i).cd_proc_tiss_xml	:= cd_proc_tiss_w;

			/* TO_NUMBER to avoid left padded zeroes */
			begin
				proc_w(i).cd_proc_tiss		:= to_number(cd_proc_tiss_w);
			exception 
			when others then
				proc_w(i).cd_proc_tiss 		:= null;
			end;
		end if;

		i := i + 1;
	end loop;
	
	/* Populate procedimento table proc_w for each patient account's medical material and medicine. */
	i := 0;
	for material in materiais loop
		mat_w(i).nr_sequencia			:= material.nr_sequencia;
		mat_w(i).cd_material			:= material.cd_material;
		mat_w(i).cd_material_tiss		:= material.cd_material_tiss;
		mat_w(i).cd_material_tuss		:= material.cd_material_tuss;
		mat_w(i).cd_material_convenio		:= material.cd_material_convenio;
		mat_w(i).cd_edicao_amb			:= material.cd_edicao;
		mat_w(i).cd_setor_atendimento		:= material.cd_setor_atendimento;
		mat_w(i).nr_doc_convenio		:= material.nr_doc_convenio;
		mat_w(i).dt_atendimento			:= material.dt_atendimento;
		mat_w(i).dt_conta			:= material.dt_conta;
		mat_w(i).vl_material			:= material.vl_material;
		mat_w(i).vl_unitario			:= material.vl_unitario;
		mat_w(i).ie_status			:= 'N';
		i := i + 1;
	end loop;
end load_items;


function check_if_procedure_conciliate(
		procedimento_p 		in procedimento,
		cd_item_p 		in varchar2,
		dt_item_p 		in date,
		vl_item_p 		in number,
		cd_setor_p		in setor_atendimento.cd_setor_atendimento%type,
		ie_funcao_medico_p	in varchar2
		) return varchar2 is
		
ie_result_w varchar2(1);

ie_setor_w	boolean;
ie_dt_atend_w	boolean;
ie_dt_conta_w	boolean;
ie_vl_item_w	boolean;
ie_func_med_w 	boolean;

begin
	
	ie_result_w 	:= 'N';
	ie_setor_w 	:= false;
	ie_dt_atend_w 	:= false;
	ie_dt_conta_w 	:= false;
	ie_vl_item_w 	:= false;
	ie_func_med_w 	:= false;
	
	if 	procedimento_p.ie_status = 'N' 
	and (
		cd_item_p = procedimento_p.cd_procedimento
		or cd_item_p = procedimento_p.cd_procedimento_tuss
		or cd_item_p = procedimento_p.cd_procedimento_convenio
		or cd_item_p = procedimento_p.cd_proc_tiss
		or cd_item_p = procedimento_p.cd_proc_tiss_xml
	) then

		if procedimento_p.cd_setor_atendimento = cd_setor_p then
			ie_setor_w := true;
		end if;

		if procedimento_p.dt_procedimento = dt_item_p then
			ie_dt_atend_w := true;
		end if;

		if procedimento_p.dt_conta = dt_item_p then
			ie_dt_conta_w := true;
		end if;

		if 	procedimento_p.vl_procedimento = vl_item_p 
			or procedimento_p.vl_medico = vl_item_p then
			ie_vl_item_w := true;
		end if;

		if 	procedimento_p.ie_funcao_medico = ie_funcao_medico_p then
			ie_func_med_w := true;
		end if;

		if 	(ie_func_med_w
			and ie_vl_item_w
			and (ie_dt_conta_w or ie_dt_atend_w)) then
			ie_result_w 	:= 'S';
		elsif	(ie_func_med_w and ie_vl_item_w) then
		
			ie_result_w 	:= 'S';
			
		elsif	(ie_func_med_w and (ie_dt_conta_w or ie_dt_atend_w)) then
		
			ie_result_w 	:= 'S';
			
		elsif	((ie_dt_conta_w or ie_dt_atend_w) and ie_vl_item_w) then
		
			ie_result_w 	:= 'S';
			
		elsif	(ie_vl_item_w) then			
		
			ie_result_w 	:= 'S';
			
		elsif	(ie_dt_conta_w or ie_dt_atend_w) then
		
			ie_result_w 	:= 'S';
			
		end if;
			
	end if;
	
	return ie_result_w;

end check_if_procedure_conciliate;

function check_if_med_mat_conciliate(
		material_p 		in material,
		cd_item_p 		in varchar2,
		dt_item_p 		in date,
		vl_item_p 		in number,
		qt_item_p		in number,
		qt_item_orig_p		in number,
		cd_setor_p		in setor_atendimento.cd_setor_atendimento%type
		) return varchar2 is
	
ie_result_w	varchar2(1);
ie_setor_w	boolean;
ie_dt_atend_w	boolean;
ie_dt_conta_w	boolean;
ie_vl_item_w	boolean;
	
begin
	ie_result_w := 'N';
	ie_setor_w 	:= false;
	ie_dt_atend_w 	:= false;
	ie_dt_conta_w 	:= false;
	ie_vl_item_w 	:= false;
	
	if material_p.ie_status = 'N' 
	and (
		material_p.cd_material_tiss = cd_item_p
		or material_p.cd_material_tuss = cd_item_p
		or material_p.cd_material_convenio = cd_item_p
	) then

		if material_p.cd_setor_atendimento = cd_setor_p then
			ie_setor_w := true;
		end if;

		if material_p.dt_atendimento = dt_item_p then
			ie_dt_atend_w := true;
		end if;

		if material_p.dt_conta = dt_item_p then
			ie_dt_conta_w := true;
		end if;

		if 	material_p.vl_material = vl_item_p
			or material_p.vl_unitario = dividir(vl_item_p, qt_item_orig_p) then
			ie_vl_item_w := true;
		end if;
		
		if	(ie_dt_conta_w or ie_dt_atend_w) 
			and ie_vl_item_w
			and ie_setor_w then
			
			ie_result_w 	:= 'S';
			
		elsif	(ie_dt_conta_w or ie_dt_atend_w)
			and ie_setor_w then
			
			ie_result_w 	:= 'S';
			
		elsif	(ie_dt_conta_w or ie_dt_atend_w)
			and ie_vl_item_w then
			
			ie_result_w	:= 'S';
			
		elsif	(ie_dt_conta_w or ie_dt_atend_w) then
		
			ie_result_w 	:= 'S';
			
		elsif	(ie_vl_item_w) then
		
			ie_result_w	:= 'S';
			
		end if;
	
	end if;

	return ie_result_w;
		
end check_if_med_mat_conciliate;

procedure create_denied_item(
	nr_seq_item_fatur_p	in number,
	proc_p			in procedimento,
	mat_p			in material,
	cd_item_p 		in varchar2,
	vl_item_p 		in number, 
	qt_item_p 		in number, 
	cd_setor_p 		in setor_atendimento.cd_setor_atendimento%type,
	vl_payment_p 		in number,
	ie_funcao_medico_p 	in varchar2,
	ie_proc_desp_p		in varchar2	
	) as

/* Medical procedure related variables. */
cd_procedimento_w	convenio_retorno_glosa.cd_procedimento%type;
ie_origem_proced_w	procedimento_paciente.ie_origem_proced%type;
nr_seq_propaci_w	procedimento_paciente.nr_sequencia%type;	
nr_seq_tiss_proc_w	tiss_conta_proc.nr_sequencia%type;

/* Medical material/medicine related variables. */
cd_material_w		convenio_retorno_glosa.cd_material%type;
nr_seq_matpaci_w	material_atend_paciente.nr_sequencia%Type;
nr_seq_tiss_desp_w	tiss_conta_desp.nr_sequencia%type;

/* Sequence of the denied item that will be generated. */
nr_seq_item_w		convenio_retorno_glosa.nr_sequencia%Type;

/* Item values. */
vl_glosa_w		convenio_retorno_glosa.vl_glosa%type;
vl_amaior_w		convenio_retorno_glosa.vl_amaior%type;
vl_payment_w		number(15,2);
	
begin

	/* If a medical procedure was found, load procedure related variables. */
	if proc_p.nr_sequencia is not null then
		nr_seq_propaci_w 	:= proc_p.nr_sequencia;
		ie_origem_proced_w 	:= proc_p.ie_origem_proced;
		cd_procedimento_w 	:= proc_p.cd_procedimento;
	/* Otherwise, if a medical material/medicine was found, load material/medicine related variables. */
	elsif mat_p.nr_sequencia is not null then
		nr_seq_matpaci_w	:= mat_p.nr_sequencia;
		cd_material_w 		:= mat_p.cd_material;
	end if;
	
	/* 
		Load TISS_CONTA_PROC or TISS_CONTA_DESP sequences. 
	*/
	if ie_proc_desp_p = 'P' then
		nr_seq_tiss_proc_w	:= nr_seq_item_fatur_p;
	else
		nr_seq_tiss_desp_w	:= nr_seq_item_fatur_p;
	end if;
	
	$if dbms_db_version.version >= 11 $then 
		nr_seq_item_w := convenio_retorno_glosa_seq.nextval;
	$else
		select	convenio_retorno_glosa_seq.nextval
		into	nr_seq_item_w
		from 	dual;
	$end
	
	/* Calculate denial, additional and payment values. */
	vl_glosa_w 	:= 0;
	vl_amaior_w	:= 0;
	vl_payment_w := vl_payment_p;
	
	if vl_payment_p > vl_item_p then
		vl_amaior_w := vl_payment_p - vl_item_p;
		vl_payment_w := vl_item_p;
		vl_glosa_w := 0;
	elsif vl_payment_p < vl_item_p then
		vl_glosa_w := vl_item_p - vl_payment_p;
		vl_amaior_w := 0;
	end if;
	
	/* Create the denied item. */
	insert into convenio_retorno_glosa
	(
		dt_atualizacao,
		dt_atualizacao_nrec,
		ie_atualizacao,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_ret_item,
		nr_sequencia,
		qt_glosa,
		vl_glosa,
		vl_cobrado,
		vl_pago_digitado,
		vl_amaior,
		nr_seq_propaci,
		nr_seq_matpaci,
		cd_item_convenio,
		cd_procedimento,
		ie_origem_proced,
		cd_material,
		nr_seq_tiss_conta_proc,
		nr_seq_tiss_conta_desp
	) 
	values 
	( 
		sysdate,
		sysdate,
		'N',
		wheb_usuario_pck.get_nm_usuario,
		wheb_usuario_pck.get_nm_usuario,
		nr_seq_guia_w,
		nr_seq_item_w,
		qt_item_p,
		vl_glosa_w,
		vl_item_p,
		vl_payment_w,
		vl_amaior_w,
		nr_seq_propaci_w,
		nr_seq_matpaci_w,
		cd_item_p,
		cd_procedimento_w,		
		ie_origem_proced_w,
		cd_material_w,
		nr_seq_tiss_proc_w,
		nr_seq_tiss_desp_w
	);
	
	commit;

end create_denied_item;

procedure generate(
		nr_seq_item_fatur_p	in number,
		ie_proc_desp_p		in varchar2,
		cd_item_p 		in varchar2, 
		dt_item_p 		in date, 
		vl_item_p 		in number,
		qt_item_p		in number,
		qt_item_orig_p		in number,
		cd_setor_p		in setor_atendimento.cd_setor_atendimento%type,
		vl_payment_p		in number,
		ie_funcao_medico_p	in varchar2
		) as

proc_ins_w		procedimento;
mat_ins_w		material;
		
begin
	/* Try to conciliate/backtrace the item with a medical procedure. */
	if proc_w.count > 0 then
		for i in proc_w.first .. proc_w.last loop
			if check_if_procedure_conciliate(proc_w(i), cd_item_p, dt_item_p, 
				vl_item_p, cd_setor_p, ie_funcao_medico_p) = 'S' then

				proc_ins_w := proc_w(i);
				proc_w(i).ie_status 	:= 'S';
				exit;

			end if;
		end loop;
	end if;
	
	/* Try to conciliate/backtrace the item with a medical material/medicine. */
	if proc_ins_w.nr_sequencia is null and mat_w.count > 0 then
		for i in mat_w.first .. mat_w.last loop
			if check_if_med_mat_conciliate(mat_w(i), cd_item_p, dt_item_p, 
					vl_item_p, qt_item_p,  qt_item_orig_p, 
					cd_setor_p) = 'S' then

				mat_ins_w := mat_w(i);
				mat_w(i).ie_status	:= 'S';
				exit;

			end if;
		end loop;
	end if;
	
	/* If a item was found, create it. */
	if (proc_ins_w.nr_sequencia is not null) or (mat_ins_w.nr_sequencia is not null) then
		create_denied_item(nr_seq_item_fatur_p, proc_ins_w, mat_ins_w, 
			cd_item_p, vl_item_p, qt_item_p, 
			cd_setor_p, vl_payment_p, ie_funcao_medico_p, 
			ie_proc_desp_p);
	end if;

end generate;

procedure free_pck_mem as

begin
	proc_w.delete;
	mat_w.delete;
	nr_seq_guia_w := null;
end free_pck_mem;

function get_item_paid_value(
		nr_seq_tiss_conta_proc_p in number,
		nr_seq_tiss_conta_desp_p in number
		) return number as

result_w	number(15,2);
		
begin
	
	result_w := 0;
	
	if nr_seq_tiss_conta_proc_p is not null then
	
		/* Paid value from Insurance Return. */
		select 	result_w + nvl(sum(nvl(x.vl_pago_digitado, 0)), 0)
		into	result_w
		from	convenio_retorno_glosa x,
			convenio_retorno_item y,
			convenio_retorno z
		where	x.nr_seq_tiss_conta_proc = nr_seq_tiss_conta_proc_p
		and	x.nr_seq_ret_item = y.nr_sequencia
		and	y.nr_seq_retorno = z.nr_sequencia;
		--and	z.ie_status_retorno = 'F';
		
		/* Denial value that has been accepted from Insurance Return. */
		select 	result_w + nvl(sum(nvl(x.vl_glosa, 0)), 0)
		into	result_w
		from	convenio_retorno_glosa x,
			convenio_retorno_item y,
			convenio_retorno z
		where	x.nr_seq_tiss_conta_proc = nr_seq_tiss_conta_proc_p
		and	x.nr_seq_ret_item = y.nr_sequencia
		and	y.nr_seq_retorno = z.nr_sequencia
		--and	z.ie_status_retorno = 'F'
		and	x.ie_acao_glosa in ('A', 'P');
		
		/* Paid value from Denial Appeal Management. */
		select	result_w + nvl(sum(nvl(x.vl_pago, 0)),0)
		into	result_w
		from	lote_audit_hist_item x,
			lote_audit_hist_guia y,
			lote_audit_hist z
		where	x.nr_seq_tiss_conta_proc = nr_seq_tiss_conta_proc_p
		and	x.nr_seq_guia = y.nr_sequencia
		and	y.nr_seq_lote_hist = z.nr_sequencia;
		--and	z.dt_fechamento is not null;
		
		/* Denial value that has been accepted from Denial Appeal Management. */
		select	result_w + nvl(sum(nvl(x.vl_glosa, 0)),0)
		into	result_w
		from	lote_audit_hist_item x,
			lote_audit_hist_guia y,
			lote_audit_hist z
		where	x.nr_seq_tiss_conta_proc = nr_seq_tiss_conta_proc_p
		and	x.nr_seq_guia = y.nr_sequencia
		and	y.nr_seq_lote_hist = z.nr_sequencia
		-- and	z.dt_fechamento is not null
		and	x.ie_acao_glosa in ('A', 'P');
	
	
	elsif nr_seq_tiss_conta_desp_p is not null then
		
		/* Paid value from Insurance Return. */
		select 	result_w + nvl(sum(nvl(x.vl_pago_digitado, 0)), 0)
		into	result_w
		from	convenio_retorno_glosa x,
			convenio_retorno_item y,
			convenio_retorno z
		where	x.nr_seq_tiss_conta_desp = nr_seq_tiss_conta_desp_p
		and	x.nr_seq_ret_item = y.nr_sequencia
		and	y.nr_seq_retorno = z.nr_sequencia;
		-- and	z.ie_status_retorno = 'F';
		
		/* Denial value that has been accepted from Insurance Return. */
		select 	result_w + nvl(sum(nvl(x.vl_glosa, 0)), 0)
		into	result_w
		from	convenio_retorno_glosa x,
			convenio_retorno_item y,
			convenio_retorno z
		where	x.nr_seq_tiss_conta_desp = nr_seq_tiss_conta_desp_p
		and	x.nr_seq_ret_item = y.nr_sequencia
		and	y.nr_seq_retorno = z.nr_sequencia
		-- and	z.ie_status_retorno = 'F'
		and	x.ie_acao_glosa in ('A', 'P');
		
		/* Paid value from Denial Appeal Management. */
		select	result_w + nvl(sum(nvl(x.vl_pago, 0)),0)
		into	result_w
		from	lote_audit_hist_item x,
			lote_audit_hist_guia y,
			lote_audit_hist z
		where	x.nr_seq_tiss_conta_desp = nr_seq_tiss_conta_desp_p
		and	x.nr_seq_guia = y.nr_sequencia
		and	y.nr_seq_lote_hist = z.nr_sequencia;
		-- and	z.dt_fechamento is not null;
		
		/* Denial value that has been accepted from Denial Appeal Management. */
		select	result_w + nvl(sum(nvl(x.vl_glosa, 0)),0)
		into	result_w
		from	lote_audit_hist_item x,
			lote_audit_hist_guia y,
			lote_audit_hist z
		where	x.nr_seq_tiss_conta_desp = nr_seq_tiss_conta_desp_p
		and	x.nr_seq_guia = y.nr_sequencia
		and	y.nr_seq_lote_hist = z.nr_sequencia
		-- and	z.dt_fechamento is not null
		and	x.ie_acao_glosa in ('A', 'P');
		
	end if;


	return result_w;

end get_item_paid_value;

function get_item_balance(
		nr_seq_item_fatur_p 	in number,
		ie_proc_desp_p 		in varchar2
		) return number as
		
result_w	number(15,2);
vl_item_w	number(15,2);

begin

	result_w := 0;
	vl_item_w := 0;
	
	if ie_proc_desp_p = 'P' then
	
		select	x.vl_procedimento
		into	vl_item_w
		from	tiss_conta_proc x
		where	x.nr_sequencia = nr_seq_item_fatur_p;
		
		result_w := get_item_paid_value(nr_seq_item_fatur_p, null);
		
	elsif ie_proc_desp_p = 'M' then
	
		select	x.vl_item
		into	vl_item_w
		from	tiss_conta_desp x
		where	x.nr_sequencia = nr_seq_item_fatur_p;
	
		result_w := get_item_paid_value(null, nr_seq_item_fatur_p);
	
	end if;


	return vl_item_w - result_w;

end get_item_balance;

end tiss_ins_ret_denial_pck;
/
