create or replace
package pkg_valida_sped_ecd is

type r_validacao is record (
    nr_ordem number(10)
  , cd_erro varchar2(10)
  , ds_erro varchar2(4000)
  , ds_causa varchar2(4000)
  , ds_solucao varchar2(4000)
  );
type t_validacao is table of r_validacao;
l_validacao t_validacao;

    function get_validacoes(
        nm_usuario_p in usuario.nm_usuario%type
      , cd_empresa_p in empresa.cd_empresa%type
      , cd_estabelecimento_p in estabelecimento.cd_estabelecimento%type
      , nr_seq_sped_controle_p in ctb_sped_controle.nr_sequencia%type
      , ie_val_ap_bp_p in varchar2 default 'S' /* Validar o valor de ativo e passivo do balanco patrimonial */
      , ie_val_detalhe_bp_p in varchar2 default 'S' /* Validar o valor de ativo e passivo das contas analiticas do balanco patrimonial */
      , ie_val_qt_nivel_bp_p in varchar2 default 'S' /* Validar os niveis da estrutura do balanco patrimonial */
      , ie_val_tp_lin_sped_dre_p in varchar2 default 'S' /* Validar se o cadastro da DRE esta com o Tipo linha SPED informado */
      , ie_val_rub_sup_p in varchar2 default 'S' /* Validar informacao de rubrica superior */
      , ie_val_cad_cont_p in varchar2 default 'S' /* Validar cadastro do contador */
      , ie_val_inst_resp_p in varchar2 default 'S' /* Validar informacao de instituicao responsavel */
        )
        return t_validacao pipelined;

end pkg_valida_sped_ecd;
/

create or replace
package body pkg_valida_sped_ecd is

MSG_ESPERADO_W constant varchar2(4000) := 'Valor esperado: ';
MSG_ENCONTRADO_W constant varchar2(4000) := 'Valor encontrado: ';

COD_IE_VAL_AP_BP_W constant number(1) := 1;
ERR_IE_VAL_AP_BP_W constant varchar2(4000) := 'O saldo do ativo e diferente do valor do passivo';
CAU_IE_VAL_AP_BP_W constant varchar2(4000) := 'O saldo da rubrica referente ao ativo nao coincide com o valor da rubrica referente ao passivo no balanco patrimonial';
SOL_IE_VAL_AP_BP_W constant varchar2(4000) := 'Verificar o cadastro do balanco patrimonial(BP) e contas ou operacoes utilizadas para gerar o valor do ativo e passivo, alterar o formato de calculo ou ajustar as contas utilizadas e confirmar se os saldos estao de acordo com o balancete';

COD_IE_VAL_DETALHE_BP_W constant number(1) := 2;
ERR_IE_VAL_DETALHE_BP_W constant varchar2(4000) := 'A soma dos saldos das contas analiticas do ativo e diferente da soma das contas analiticas do passivo';
CAU_IE_VAL_DETALHE_BP_W constant varchar2(4000) := 'A soma dos saldos das contas utilizados no ultimo nivel do balanco patrimonial do ativo nao coincidem com a soma dos saldos das contas utlizadas no ultimo nivel do passivo';
SOL_IE_VAL_DETALHE_BP_W constant varchar2(4000) := 'Verificar as contas utilizadas e saldos no balancete, identificar as contas ou operacoes da rubrica que possam estar gerando diferenca';

COD_IE_VAL_QT_NIVEL_BP_W constant number(1) := 3;
ERR_IE_VAL_QT_NIVEL_BP_W constant varchar2(4000) := 'Os niveis utilizados no balanco patrimonial estao incorretos';
CAU_IE_VAL_QT_NIVEL_BP_W constant varchar2(4000) := 'Uma ou mais rubricas do balanco patrimonial(BP) esta com o nivel incorreto, nao coincidindo com o nivel real da rubrica';
SOL_IE_VAL_QT_NIVEL_BP_W constant varchar2(4000) := 'Verificar o cadastro do balanco patrimonial e ajustar o nivel utilizado nas rubricas, sendo que as rubricas inferiores nao podem ter nivel superior a nenhuma de suas rubricas superiores';

COD_IE_VAL_TP_LIN_SPED_DRE_W constant number(1) := 4;
ERR_IE_VAL_TP_LIN_SPED_DRE_W constant varchar2(4000) := 'Nao foi informado o tipo de linha no SPED';
CAU_IE_VAL_TP_LIN_SPED_DRE_W constant varchar2(4000) := 'Uma ou mais rubricas do demonstrativo de resultado utilizado no arquivo esta sem informacao de tipo de linha do SPED';
SOL_IE_VAL_TP_LIN_SPED_DRE_W constant varchar2(4000) := 'Verificar o cadastro do demonstrativo de resultado(DRE) e preencher em todas as rubricas o tipo de linha do SPED, no campo Tipo linha SPED(CTB_MODELO_RUBRICA.IE_TIPO_LINHA_SPED)';

COD_IE_VAL_RUB_SUP_W constant number(1) := 5;
ERR_IE_VAL_RUB_SUP_W constant varchar2(4000) := 'Nao foi informado a rubrica superior';
CAU_IE_VAL_RUB_SUP_W constant varchar2(4000) := 'Uma ou mais rubricas inferiores estao sem rubrica superior informada';
SOL_IE_VAL_RUB_SUP_W constant varchar2(4000) := 'Verificar o cadastro do demonstrativo de resultado(DRE) e balanco patrimonial utilizados para o arquivo e preencher a rubrica superior, no campo Rubrica superior(CTB_MODELO_RUBRICA.NR_SEQ_RUBRICA_SUP)';

COD_IE_VAL_CAD_CONT_W constant number(1) := 6;
ERR_IE_VAL_CAD_CONT_W constant varchar2(4000) := 'O cadastro do contador utilizado no arquivo esta incompleto';
CAU_IE_VAL_CAD_CONT_W constant varchar2(4000) := 'Uma ou mais informacoes necessarias para a entrega da escrituracao estao incompletas ou inexistentes no cadastro do(s) contador(es) utilizado(s) para este arquivo';
SOL_IE_VAL_CAD_CONT_W constant varchar2(4000) := 'Verificar o cadastro do(s) contador(es) utilizado(s) e preencher as informacoes necessarias';

COD_IE_VAL_INST_RESP_W constant number(1) := 7;
ERR_IE_VAL_INST_RESP_W constant varchar2(4000) := 'Nao foi informado a instituicao responsavel na regra';
CAU_IE_VAL_INST_RESP_W constant varchar2(4000) := 'Na regra utilizada para a geracao do arquivo, o campo Instituicao resp (CTB_REGRA_SPED.CD_INSTITUICAO_CAD) nao foi informado';
SOL_IE_VAL_INST_RESP_W constant varchar2(4000) := 'Informar a instituicao responsavel na regra utilizada no registro de controle utilizado para geracao do arquivo';


    function get_validacoes(
        nm_usuario_p in usuario.nm_usuario%type
      , cd_empresa_p in empresa.cd_empresa%type
      , cd_estabelecimento_p in estabelecimento.cd_estabelecimento%type
      , nr_seq_sped_controle_p in ctb_sped_controle.nr_sequencia%type
      , ie_val_ap_bp_p in varchar2 default 'S' /* Validar o valor de ativo e passivo do balanco patrimonial */
      , ie_val_detalhe_bp_p in varchar2 default 'S' /* Validar o valor de ativo e passivo das contas analiticas do balanco patrimonial */
      , ie_val_qt_nivel_bp_p in varchar2 default 'S' /* Validar os niveis da estrutura do balanco patrimonial */
      , ie_val_tp_lin_sped_dre_p in varchar2 default 'S' /* Validar se o cadastro da DRE esta com o Tipo linha SPED informado */
      , ie_val_rub_sup_p in varchar2 default 'S' /* Validar informacao de rubrica superior */
      , ie_val_cad_cont_p in varchar2 default 'S' /* Validar cadastro do contador */
      , ie_val_inst_resp_p in varchar2 default 'S' /* Validar informacao de instituicao responsavel */
        )
        return t_validacao pipelined is
        validacao_w r_validacao;
        nr_ordem_w pls_integer;
    begin
        validacao_w.nr_ordem := 0;
        if (ie_val_ap_bp_p = 'S') then
            validacao_w.nr_ordem   := validacao_w.nr_ordem + 1;
            validacao_w.cd_erro    := lpad(COD_IE_VAL_AP_BP_W,10,'0');
            validacao_w.ds_erro    := nvl(wheb_mensagem_pck.get_texto(000000),ERR_IE_VAL_AP_BP_W);
            validacao_w.ds_causa   := nvl(wheb_mensagem_pck.get_texto(000000),CAU_IE_VAL_AP_BP_W);
            validacao_w.ds_solucao := nvl(wheb_mensagem_pck.get_texto(000000),SOL_IE_VAL_AP_BP_W);
            pipe row(validacao_w);
        end if;

        if (ie_val_detalhe_bp_p = 'S') then
            validacao_w.nr_ordem   := validacao_w.nr_ordem + 1;
            validacao_w.cd_erro    := lpad(COD_IE_VAL_DETALHE_BP_W,10,'0');
            validacao_w.ds_erro    := nvl(wheb_mensagem_pck.get_texto(000000),ERR_IE_VAL_DETALHE_BP_W);
            validacao_w.ds_causa   := nvl(wheb_mensagem_pck.get_texto(000000),CAU_IE_VAL_DETALHE_BP_W);
            validacao_w.ds_solucao := nvl(wheb_mensagem_pck.get_texto(000000),SOL_IE_VAL_DETALHE_BP_W);
            pipe row(validacao_w);
        end if;

        if (ie_val_qt_nivel_bp_p = 'S') then
            validacao_w.nr_ordem   := validacao_w.nr_ordem + 1;
            validacao_w.cd_erro    := lpad(COD_IE_VAL_QT_NIVEL_BP_W,10,'0');
            validacao_w.ds_erro    := nvl(wheb_mensagem_pck.get_texto(000000),ERR_IE_VAL_QT_NIVEL_BP_W);
            validacao_w.ds_causa   := nvl(wheb_mensagem_pck.get_texto(000000),CAU_IE_VAL_QT_NIVEL_BP_W);
            validacao_w.ds_solucao := nvl(wheb_mensagem_pck.get_texto(000000),SOL_IE_VAL_QT_NIVEL_BP_W);
            pipe row(validacao_w);
        end if;

        if (ie_val_tp_lin_sped_dre_p = 'S') then
            validacao_w.nr_ordem   := validacao_w.nr_ordem + 1;
            validacao_w.cd_erro    := lpad(COD_IE_VAL_TP_LIN_SPED_DRE_W,10,'0');
            validacao_w.ds_erro    := nvl(wheb_mensagem_pck.get_texto(000000),ERR_IE_VAL_TP_LIN_SPED_DRE_W);
            validacao_w.ds_causa   := nvl(wheb_mensagem_pck.get_texto(000000),CAU_IE_VAL_TP_LIN_SPED_DRE_W);
            validacao_w.ds_solucao := nvl(wheb_mensagem_pck.get_texto(000000),SOL_IE_VAL_TP_LIN_SPED_DRE_W);
            pipe row(validacao_w);
        end if;

        if (ie_val_rub_sup_p = 'S') then
            validacao_w.nr_ordem   := validacao_w.nr_ordem + 1;
            validacao_w.cd_erro    := lpad(COD_IE_VAL_RUB_SUP_W,10,'0');
            validacao_w.ds_erro    := nvl(wheb_mensagem_pck.get_texto(000000),ERR_IE_VAL_RUB_SUP_W);
            validacao_w.ds_causa   := nvl(wheb_mensagem_pck.get_texto(000000),CAU_IE_VAL_RUB_SUP_W);
            validacao_w.ds_solucao := nvl(wheb_mensagem_pck.get_texto(000000),SOL_IE_VAL_RUB_SUP_W);
            pipe row(validacao_w);
        end if;

        if (ie_val_cad_cont_p = 'S') then
            validacao_w.nr_ordem   := validacao_w.nr_ordem + 1;
            validacao_w.cd_erro    := lpad(COD_IE_VAL_CAD_CONT_W,10,'0');
            validacao_w.ds_erro    := nvl(wheb_mensagem_pck.get_texto(000000),ERR_IE_VAL_CAD_CONT_W);
            validacao_w.ds_causa   := nvl(wheb_mensagem_pck.get_texto(000000),CAU_IE_VAL_CAD_CONT_W);
            validacao_w.ds_solucao := nvl(wheb_mensagem_pck.get_texto(000000),SOL_IE_VAL_CAD_CONT_W);
            pipe row(validacao_w);
        end if;

        if (ie_val_inst_resp_p = 'S') then
            validacao_w.nr_ordem   := validacao_w.nr_ordem + 1;
            validacao_w.cd_erro    := lpad(COD_IE_VAL_INST_RESP_W,10,'0');
            validacao_w.ds_erro    := nvl(wheb_mensagem_pck.get_texto(000000),ERR_IE_VAL_INST_RESP_W);
            validacao_w.ds_causa   := nvl(wheb_mensagem_pck.get_texto(000000),CAU_IE_VAL_INST_RESP_W);
            validacao_w.ds_solucao := nvl(wheb_mensagem_pck.get_texto(000000),SOL_IE_VAL_INST_RESP_W);
            pipe row(validacao_w);
        end if;

        return;
    end get_validacoes;

end pkg_valida_sped_ecd;
/