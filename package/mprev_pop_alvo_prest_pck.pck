create or replace
package mprev_pop_alvo_prest_pck as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
Sequencia para o nome das tabelas das regras do cubo.
---Tabela-----------------------------------------Seq-------------
MPREV_REGRA_CUBO_PROC 	- 	1
MPREV_REGRA_CUBO_DIAG		- 	2
MPREV_REGRA_CUBO_ATEND 	- 	3
MPREV_REGRA_CUBO_CUSTO 	-	4
MPREV_REGRA_CUBO_BENEF 	- 	5
HDM_REGRA_CUBO_MEDIC 		- 	6
HDM_REGRA_CUBO_COMPL		-	7
MPREV_REGRA_CUBO_PESSOA 	-	8
Essa sequencia e utilizada na function 'mprev_pop_alvo_prest_pck.obter_restricao_padrao_pop_alv' para
restringir. OS 1013009 25/02/2016.

Alteracoes:
------------------------------------------------------------------------------------------------------------------
jjung 24/02/2014 - Fechar os cursores do DBMS_SQL que estavam ficando abertos.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


--Variavel global para padronizar o enter em sql dinamicos
enter_w 		pls_util_pck.enter_w%type := pls_util_pck.enter_w;
aspas_w 		pls_util_pck.aspas_w%type := pls_util_pck.aspas_w;
bind_sql_w		sql_pck.t_dado_bind;

ds_select_w		varchar2(4000);
ds_restricao_w		varchar(4000);
ds_select_comp_w	varchar2(4000);
ret_null_w		varchar2(1);
var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;
dt_referencia_w		date;
qt_reg_commit_w		pls_integer := 5000;
ie_exec_select_princ_w	varchar2(1) := 'S';
ds_select_tabela_temp_w	varchar2(4000);

--DECLARACAO DE TIPO GERAR POPULACAO ALVO
type pop_alvo is record (
	nr_seq_populacao_alvo		number,
	ie_regra_pessoa			varchar2(1),
        ie_regra_exc_pessoa		varchar2(1),
        ie_regra_atend			varchar2(1),
        ie_regra_exc_atend		varchar2(1),
        ie_regra_proc			varchar2(1),
        ie_regra_exc_proc		varchar2(1),
		ie_regra_trat			varchar2(1),
        ie_regra_exc_trat		varchar2(1),
		ie_regra_lista_prob		varchar2(1),
        ie_regra_exc_lista_prob	varchar2(1),
		ie_regra_medic			varchar2(1),
        ie_regra_exc_medic		varchar2(1),
        ie_regra_diag			varchar2(1),
        ie_regra_exc_diag		varchar2(1),
	ie_regra_compl_pf		varchar2(1),
	ie_regra_exc_compl_pf		varchar2(1)	
	);

--DECLARACAO DE TIPO PESSOAS
type regra_pessoa is record (
	nr_sequencia			mprev_regra_cubo_pessoa.nr_sequencia%type,
	ie_sexo				mprev_regra_cubo_pessoa.ie_sexo%type,
	qt_idade_minima			mprev_regra_cubo_pessoa.qt_idade_minima%type,
	qt_idade_maxima			mprev_regra_cubo_pessoa.qt_idade_maxima%type,
	ie_participante			mprev_regra_cubo_pessoa.ie_participante%type,
	ie_grupo_controle		mprev_regra_cubo_pessoa.ie_grupo_controle%type,
	ie_incluir			mprev_regra_cubo_pessoa.ie_incluir%type
      );

--DECLARACAO DE TIPO DIAGNOSTICO
type regra_diag is record (
	nr_sequencia			mprev_regra_cubo_diag.nr_sequencia%type,
	cd_doenca			mprev_regra_cubo_diag.cd_doenca%type,
	cd_doenca_inicial		mprev_regra_cubo_diag.cd_doenca_inicial%type,
	cd_doenca_final			mprev_regra_cubo_diag.cd_doenca_final%type,
	qt_ocorrencia			mprev_regra_cubo_diag.qt_ocorrencia%type,
	ie_incluir			mprev_regra_cubo_diag.ie_incluir%type
      );

--DECLARACAO DE TIPO ATENDIMENTO
type regra_atend is record (
	nr_sequencia			HDM_REGRA_CUBO_ATEND_EUP.nr_sequencia%type,
	qt_ocorrencia_minima		HDM_REGRA_CUBO_ATEND_EUP.qt_ocorrencia_minima%type,
	qt_ocorrencia_maxima		HDM_REGRA_CUBO_ATEND_EUP.qt_ocorrencia_maxima%type,
	ie_tipo_atendimento		HDM_REGRA_CUBO_ATEND_EUP.ie_tipo_atendimento%type,
	cd_medico_resp		HDM_REGRA_CUBO_ATEND_EUP.cd_medico_resp%type,
	ie_incluir			HDM_REGRA_CUBO_ATEND_EUP.ie_incluir%type
      );

--DECLARACAO DE TIPO PROCEDIMENTO
type regra_proc is record (
	nr_sequencia			mprev_regra_cubo_proc.nr_sequencia%type,
	cd_procedimento			mprev_regra_cubo_proc.cd_procedimento%type,
	ie_origem_proced		mprev_regra_cubo_proc.ie_origem_proced%type,
	qt_ocorrencia			mprev_regra_cubo_proc.qt_ocorrencia%type,
	ie_incluir			mprev_regra_cubo_proc.ie_incluir%type
      );
--DECLARACAO DE TIPO TRATAMENTO	  
type regra_trat is record (
	nr_sequencia			hdm_regra_cubo_trat.nr_sequencia%type,
	ie_tipo_tratamento		hdm_regra_cubo_trat.ie_tipo_tratamento%type,
	ie_incluir				hdm_regra_cubo_trat.ie_incluir%type
      );
--DECLARACAO DE TIPO LISTA DE PROBLEMAS
type regra_lista_prob is record (
	nr_sequencia			hdm_regra_cubo_lista_prob.nr_sequencia%type,
	cd_ciap					hdm_regra_cubo_lista_prob.cd_ciap%type,
	cd_doenca				hdm_regra_cubo_lista_prob.cd_doenca%type,
	cd_doenca_inicial		hdm_regra_cubo_lista_prob.cd_doenca_inicial%type,
	cd_doenca_final			hdm_regra_cubo_lista_prob.cd_doenca_final%type,
	ie_status				hdm_regra_cubo_lista_prob.ie_status%type,
	qt_ocorrencia			hdm_regra_cubo_lista_prob.qt_ocorrencia%type,
	ie_incluir				hdm_regra_cubo_lista_prob.ie_incluir%type
      );

-- Tabela para a regra Complemento pessoa.
type table_regra_compl_pf is record (
	nr_sequencia		hdm_regra_cubo_compl.nr_sequencia%type,
	cd_municipio_ibge	hdm_regra_cubo_compl.cd_municipio_ibge%type,
  sg_estado              hdm_regra_cubo_compl.sg_estado%type,
	ie_tipo_complemento     hdm_regra_cubo_compl.ie_tipo_complemento%type,
	ie_incluir              hdm_regra_cubo_compl.ie_incluir%type
	);
	
-- Tabela para a regra Medicamento
type regra_medic is record (
	nr_sequencia		hdm_regra_cubo_medic.nr_sequencia%type,
	cd_material			hdm_regra_cubo_medic.cd_material%type,
	qt_dose_inicial		hdm_regra_cubo_medic.qt_dose_inicial%type,
	qt_dose_final		hdm_regra_cubo_medic.qt_dose_final%type,
	cd_unidade_medida	hdm_regra_cubo_medic.cd_unidade_medida%type,
	ie_incluir			hdm_regra_cubo_medic.ie_incluir%type
	);
      
type table_pessoas_pop_alvo is record (
	cd_pessoa_fisica	pls_util_cta_pck.t_varchar2_table_50,
	qt_idade_anos		pls_util_cta_pck.t_number_table,
	ie_partic_programa	pls_util_cta_pck.t_varchar2_table_1,
	ie_grupo_controle	pls_util_cta_pck.t_varchar2_table_1,
	nm_pessoa		pls_util_cta_pck.t_varchar2_table_255,
	ie_sexo			pls_util_cta_pck.t_varchar2_table_1
	);

type table_benef_pop_alvo is record (
	nr_seq_segurado		pls_util_cta_pck.t_number_table,
	cd_usuario_plano	pls_util_cta_pck.t_varchar2_table_50,
	ie_preco_plano		pls_util_cta_pck.t_varchar2_table_5,
	ds_preco_plano		pls_util_cta_pck.t_varchar2_table_255,
	dt_inclusao_plano	pls_util_cta_pck.t_date_table,
	ie_situacao_contrato	pls_util_cta_pck.t_varchar2_table_5,
	ie_situacao_atend	pls_util_cta_pck.t_varchar2_table_5,
	nr_seq_pessoa		pls_util_cta_pck.t_number_table
	);

type table_pessoas_cubo_ops is record (
	cd_pessoa_fisica	pls_util_cta_pck.t_varchar2_table_50,
	dt_nascimento           pls_util_cta_pck.t_date_table,
	nr_sequencia		pls_util_cta_pck.t_number_table
	);



--METODOS DO DESFAZER GERACAO POPULACAO ALVO
procedure desfazer_geracao_pop_alvo(nr_seq_populacao_alvo_p	number);

--METODOS DA GERAR POPULACAO ALVO
procedure gerar_populacao_alvo(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_alvo_p	number,
				nm_usuario_p		varchar2);

function obter_restricao_pop_alvo(	dados_pop_alvo_p	mprev_pop_alvo_prest_pck.pop_alvo,
					nm_tabela_regra_p	varchar2)
					return varchar2;

function obter_restricao_padrao_pop_alv(	ie_exists_p		varchar2,--S = exists / N = not exists
						nr_seq_tab_regra_cubo_p	number) --Sequencia fixa para as tabelas das regras.
						return varchar2;
-- TRATAMENTO DE DATAS
procedure inserir_data(		nr_seq_populacao_alvo_p		number,
				dt_referencia_p			date,
				nm_usuario_p			varchar2,
				nr_seq_pop_alvo_data_p	out	number);

--METODOS DAS PESSOAS
procedure selecionar_pessoa(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_p 	number,
				nm_usuario_p		varchar2);

function obter_restricao_pessoa(dados_regra_p		mprev_pop_alvo_prest_pck.regra_pessoa)
				return varchar2;

--METODOS DOS DIAGNOSTICOS
procedure selecionar_diag(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_p 	number,
				nm_usuario_p		varchar2);

function obter_restricao_diag(	dados_regra_p		mprev_pop_alvo_prest_pck.regra_diag)
				return varchar2;

--METODOS DOS ATENDIMENTO
procedure selecionar_atend(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_p 	number,
				nm_usuario_p		varchar2);

function obter_restricao_atend(	dados_regra_p		mprev_pop_alvo_prest_pck.regra_atend)
				return varchar2;

--METODOS DOS PROCEDIMENTO
procedure selecionar_proc(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_p 	number,
				nm_usuario_p		varchar2);

function obter_restricao_proc(	dados_regra_p		mprev_pop_alvo_prest_pck.regra_proc)
				return varchar2;

--METODOS DOS TRATAMENTOS
procedure selecionar_trat(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_p 	number,
				nm_usuario_p		varchar2);

function obter_restricao_trat(	dados_regra_p		mprev_pop_alvo_prest_pck.regra_trat)
				return varchar2;
				
--METODOS DAS LISTAS DE PROBLEMAS
procedure selecionar_lista_prob(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_p 	number,
				nm_usuario_p		varchar2);

procedure selecionar_medic(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_p 	number,
				nm_usuario_p		varchar2);

function obter_restricao_lista_prob(	dados_regra_p		mprev_pop_alvo_prest_pck.regra_lista_prob)
				return varchar2;

function obter_restricao_medic(	dados_regra_p		mprev_pop_alvo_prest_pck.regra_medic)
				return varchar2;
				
procedure gravar_pessoas_populacao(	nr_seq_populacao_alvo_p	number,
					nm_usuario_p		varchar2,
					ds_select_comp_p	varchar2);

--METODOS DO COMPLEMENTO DA PESSOA (ENDERECO)
procedure selecionar_compl_pf(	nr_seq_regra_cubo_p	number,
				nr_seq_populacao_p 	number,
				nm_usuario_p		varchar2);

function obter_restricao_compl_pf(	dados_regra_p		mprev_pop_alvo_prest_pck.table_regra_compl_pf)
				return varchar2;					

-- Limpa as tabelas (Record) para inserir novos registros.				
procedure limpar_tipo_tabelas(nm_tabela_p varchar2);

/*Procedure para gravar as pessoas conforme regras na tabela temporaria mprev_pessoa_cubo_ops_w*/
procedure gravar_pessoas_cubo_ops_w(nr_seq_populacao_alvo_p	number,
					nm_usuario_p		varchar2,
					nr_seq_controle_p	number,
					ds_select_comp_p	varchar2);

end	mprev_pop_alvo_prest_pck;
/

create or replace package body mprev_pop_alvo_prest_pck as

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>DESFAZER GERACAO POPULACAO ALVO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure desfazer_geracao_pop_alvo(nr_seq_populacao_alvo_p	number) is

	begin

	update	mprev_populacao_alvo
	set	dt_inicio_filtro_atend	= null,
		dt_inicio_filtro_benef	= null,
		dt_inicio_filtro_custo	= null,
		dt_inicio_filtro_pessoa	= null,
		dt_fim_filtro_atend	= null,
		dt_fim_filtro_benef	= null,
		dt_fim_filtro_custo	= null,
		dt_fim_filtro_pessoa	= null
	where	nr_sequencia	= nr_seq_populacao_alvo_p;

	delete 	mprev_pop_alvo_selecao
	where	nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;

	delete 	mprev_pop_alvo_data
	where 	nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;

	delete 	mprev_pop_alvo_pessoa
	where 	nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;

	delete 	mprev_pop_pes_atend
	where 	nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;
	
	delete 	mprev_pop_pes_diagnostico
	where 	nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;

	delete 	mprev_pop_pes_atend_proc
	where 	nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;
	
	delete 	mprev_pessoa_cubo_ops_w
	where	nr_seq_pop_alvo = nr_seq_populacao_alvo_p;
	
	/*Delete dos registros das tabelas filhas*/
	---Atendimentos
	delete from HDM_POP_ALVO_ATEND
	where nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;
	---Diagnosticos
	delete from HDM_POP_ALVO_DIAGNOSTICO
	where nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;
	---Lista problemas
	delete from HDM_POP_ALVO_PROBLEMA
	where nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;
	---Procedimentos
	delete from  HDM_POP_ALVO_PROCED
	where nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;
	---Tratamentos
	delete from HDM_POP_ALVO_TRATAMENTO
	where nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;
	---Medicamentos
	delete from HDM_POP_ALVO_MEDIC
	where nr_seq_populacao_alvo = nr_seq_populacao_alvo_p;

	commit;

	end desfazer_geracao_pop_alvo;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GERAR POPULACAO ALVO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure gerar_populacao_alvo(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_alvo_p	number,
					nm_usuario_p		varchar2) is

	Cursor c_regras_utilizadas (nr_seq_populacao_alvo_pc	number) is
		select	'MPREV_REGRA_CUBO_PESSOA' nm_tabela_regra_cubo,
			c.ie_incluir ie_selecionar,
			8 ordem_desc
		from	mprev_regra_cubo_pessoa c,
			mprev_regra_cubo b,
			mprev_populacao_alvo a
		where	a.nr_seq_regra_cubo = b.nr_sequencia
		and	b.nr_sequencia = c.nr_seq_regra_cubo
		and	a.nr_sequencia = nr_seq_populacao_alvo_pc
		group by
			c.ie_incluir
		union all
		select	'HDM_REGRA_CUBO_COMPL' nm_tabela_regra_cubo,
			c.ie_incluir ie_selecionar,
			7 ordem_desc
		from	hdm_regra_cubo_compl c,
			mprev_regra_cubo b,
			mprev_populacao_alvo a
		where	a.nr_seq_regra_cubo = b.nr_sequencia
		and	b.nr_sequencia = c.nr_seq_regra_cubo
		and	a.nr_sequencia = nr_seq_populacao_alvo_pc
		group by
			c.ie_incluir
		union all
		select	'HDM_REGRA_CUBO_MEDIC' nm_tabela_regra_cubo,
			c.ie_incluir ie_selecionar,
			6 ordem_desc
		from	hdm_regra_cubo_medic c,
			mprev_regra_cubo b,
			mprev_populacao_alvo a
		where	a.nr_seq_regra_cubo = b.nr_sequencia
		and	b.nr_sequencia = c.nr_seq_regra_cubo
		and	a.nr_sequencia = nr_seq_populacao_alvo_pc
		union all
		select	'HDM_REGRA_CUBO_ATEND_EUP' nm_tabela_regra_cubo,
			c.ie_incluir ie_selecionar,
			5 ordem_desc
		from	HDM_REGRA_CUBO_ATEND_EUP c,
			mprev_regra_cubo b,
			mprev_populacao_alvo a
		where	a.nr_seq_regra_cubo = b.nr_sequencia
		and	b.nr_sequencia = c.nr_seq_regra_cubo
		and	a.nr_sequencia = nr_seq_populacao_alvo_pc
		group by
			c.ie_incluir
		union all
		select	'MPREV_REGRA_CUBO_DIAG' nm_tabela_regra_cubo,
			c.ie_incluir ie_selecionar,
			4 ordem_desc
		from	mprev_regra_cubo_diag c,
			mprev_regra_cubo b,
			mprev_populacao_alvo a
		where	a.nr_seq_regra_cubo = b.nr_sequencia
		and	b.nr_sequencia = c.nr_seq_regra_cubo
		and	a.nr_sequencia = nr_seq_populacao_alvo_pc
		group by
			c.ie_incluir
		union all
		select	'HDM_REGRA_CUBO_LISTA_PROB' nm_tabela_regra_cubo,
			c.ie_incluir ie_selecionar,
			3 ordem_desc
		from	hdm_regra_cubo_lista_prob c,
			mprev_regra_cubo b,
			mprev_populacao_alvo a
		where	a.nr_seq_regra_cubo = b.nr_sequencia
		and	b.nr_sequencia = c.nr_seq_regra_cubo
		and	a.nr_sequencia = nr_seq_populacao_alvo_pc
		union all
		select	'HDM_REGRA_CUBO_TRAT' nm_tabela_regra_cubo,
			c.ie_incluir ie_selecionar,
			2 ordem_desc
		from	hdm_regra_cubo_trat c,
			mprev_regra_cubo b,
			mprev_populacao_alvo a
		where	a.nr_seq_regra_cubo = b.nr_sequencia
		and	b.nr_sequencia = c.nr_seq_regra_cubo
		and	a.nr_sequencia = nr_seq_populacao_alvo_pc
		union all
		select	'MPREV_REGRA_CUBO_PROC' nm_tabela_regra_cubo,
			c.ie_incluir ie_selecionar,
			1 ordem_desc
		from	mprev_regra_cubo_proc c,
			mprev_regra_cubo b,
			mprev_populacao_alvo a
		where	a.nr_seq_regra_cubo = b.nr_sequencia
		and	b.nr_sequencia = c.nr_seq_regra_cubo
		and	a.nr_sequencia = nr_seq_populacao_alvo_pc
		group by
			c.ie_incluir;

	Cursor c_regras_util_ordenadas (nr_seq_populacao_alvo_pc	number) is
		select	x.nm_tabela_regra_cubo,
			nr_seq_controle
		from	(select 'MPREV_REGRA_CUBO_PESSOA' nm_tabela_regra_cubo,
				8 nr_seq_controle
			from	mprev_regra_cubo_pessoa c,
				mprev_regra_cubo b,
				mprev_populacao_alvo a
			where	a.nr_seq_regra_cubo = b.nr_sequencia
			and	b.nr_sequencia = c.nr_seq_regra_cubo
			and	a.nr_sequencia = nr_seq_populacao_alvo_pc
			union all
			select	'HDM_REGRA_CUBO_COMPL' nm_tabela_regra_cubo,
				7 nr_seq_controle
			from	hdm_regra_cubo_compl c,
				mprev_regra_cubo b,
				mprev_populacao_alvo a
			where	a.nr_seq_regra_cubo = b.nr_sequencia
			and	b.nr_sequencia = c.nr_seq_regra_cubo
			and	a.nr_sequencia = nr_seq_populacao_alvo_pc
			union
			select	'HDM_REGRA_CUBO_MEDIC' nm_tabela_regra_cubo,
				6 nr_seq_controle
			from	HDM_REGRA_CUBO_MEDIC c,
				mprev_regra_cubo b,
				mprev_populacao_alvo a
			where	a.nr_seq_regra_cubo = b.nr_sequencia
			and	b.nr_sequencia = c.nr_seq_regra_cubo
			and	a.nr_sequencia = nr_seq_populacao_alvo_pc
			union
			select	'HDM_REGRA_CUBO_ATEND_EUP' nm_tabela_regra_cubo,
				5 nr_seq_controle
			from	HDM_REGRA_CUBO_ATEND_EUP c,
				mprev_regra_cubo b,
				mprev_populacao_alvo a
			where	a.nr_seq_regra_cubo = b.nr_sequencia
			and	b.nr_sequencia = c.nr_seq_regra_cubo
			and	a.nr_sequencia = nr_seq_populacao_alvo_pc
			union
			select	'MPREV_REGRA_CUBO_DIAG' nm_tabela_regra_cubo,
				4 nr_seq_controle
			from	mprev_regra_cubo_diag c,
				mprev_regra_cubo b,
				mprev_populacao_alvo a
			where	a.nr_seq_regra_cubo = b.nr_sequencia
			and	b.nr_sequencia = c.nr_seq_regra_cubo
			and	a.nr_sequencia = nr_seq_populacao_alvo_pc
			union
			select	'HDM_REGRA_CUBO_LISTA_PROB' nm_tabela_regra_cubo,
				3 nr_seq_controle
			from	hdm_regra_cubo_lista_prob c,
				mprev_regra_cubo b,
				mprev_populacao_alvo a
			where	a.nr_seq_regra_cubo = b.nr_sequencia
			and	b.nr_sequencia = c.nr_seq_regra_cubo
			and	a.nr_sequencia = nr_seq_populacao_alvo_pc
			union
			select	'HDM_REGRA_CUBO_TRAT' nm_tabela_regra_cubo,
				2 nr_seq_controle
			from	hdm_regra_cubo_trat c,
				mprev_regra_cubo b,
				mprev_populacao_alvo a
			where	a.nr_seq_regra_cubo = b.nr_sequencia
			and	b.nr_sequencia = c.nr_seq_regra_cubo
			and	a.nr_sequencia = nr_seq_populacao_alvo_pc
			union
			select	'MPREV_REGRA_CUBO_PROC' nm_tabela_regra_cubo,
				1 nr_seq_controle
			from	mprev_regra_cubo_proc c,
				mprev_regra_cubo b,
				mprev_populacao_alvo a
			where	a.nr_seq_regra_cubo = b.nr_sequencia
			and	b.nr_sequencia = c.nr_seq_regra_cubo
			and	a.nr_sequencia = nr_seq_populacao_alvo_pc) x
		order by 2;

	Cursor c_diagnostico
		(nr_seq_conta_cubo_pc	number) is
		select	a.cd_doenca
		from	mprev_diagnostico_cubo_ops a
		where	a.nr_seq_conta_cubo	= nr_seq_conta_cubo_pc;

	Cursor c_procedimento
		(nr_seq_conta_cubo_pc	number) is
		select	a.cd_procedimento,
			a.ie_origem_proced,
			a.qt_procedimento
		from	mprev_conta_proc_cubo_ops a
		where	a.nr_seq_conta_cubo_ops	= nr_seq_conta_cubo_pc
		union all
		select	a.cd_procedimento,
			a.ie_origem_proced,
			a.qt_procedimento
		from	mprev_guia_proc_cubo_ops a
		where	a.nr_seq_guia_cubo	= nr_seq_conta_cubo_pc;


	pop_alvo_w			pop_alvo;

	cd_pessoa_fisica_w 			mprev_pessoa_cubo_ops.cd_pessoa_fisica%type;
	cd_pessoa_fisica_old_w 		mprev_pessoa_cubo_ops.cd_pessoa_fisica%type;
	dt_nascimento_w				mprev_pessoa_cubo_ops.dt_nascimento%type	:= null;
	qt_idade_anos_w 			mprev_pop_alvo_pessoa.qt_idade_anos%type 	:= null;
	nr_seq_partic_programa_w 	number(20) 					:= null;
	ie_partic_programa_w 		mprev_pop_alvo_pessoa.ie_partic_programa%type 	:= null;
	ie_grupo_controle_w			mprev_pop_alvo_pessoa.ie_grupo_controle%type	:= null;
	nm_pessoa_w					mprev_pop_alvo_pessoa.nm_pessoa%type		:= null;
	ie_sexo_w					mprev_pop_alvo_pessoa.ie_sexo%type		:= null;
	nr_seq_pop_alvo_data_w		mprev_pop_alvo_data.nr_sequencia%type		:= null;
	/*Nome da pessoa em maisculo e sem acentuacao, para otimizar performance na pesquisa.*/
	nm_pessoa_pesquisa_w		mprev_pop_alvo_pessoa.nm_pessoa_pesquisa%type	:= null;
	/*Indices*/
	nr_seq_pessoa_w				mprev_pop_alvo_pessoa.nr_sequencia%type		:= null;
	nr_seq_atend_w				mprev_pop_pes_atend.nr_sequencia%type;
	cursor_w					sql_pck.t_cursor;
	nr_seq_controle_w			mprev_pessoa_cubo_ops_w.nr_seq_controle%type;
	qt_reg_w					number(10);

	begin

	select 	pkg_date_utils.add_month(pkg_date_utils.start_of(sysdate,'MONTH', 0),- nvl(qt_meses_atras,0), 0) dt_referencia
	into 	dt_referencia_w
	from 	mprev_regra_cubo
	where 	nr_sequencia = nr_seq_regra_cubo_p;

	selecionar_pessoa(nr_seq_regra_cubo_p,nr_seq_populacao_alvo_p,nm_usuario_p);

	selecionar_diag(nr_seq_regra_cubo_p,nr_seq_populacao_alvo_p,nm_usuario_p);

	selecionar_atend(nr_seq_regra_cubo_p,nr_seq_populacao_alvo_p,nm_usuario_p);

	selecionar_proc(nr_seq_regra_cubo_p,nr_seq_populacao_alvo_p,nm_usuario_p);
	
	selecionar_lista_prob(nr_seq_regra_cubo_p,nr_seq_populacao_alvo_p,nm_usuario_p);
	
	selecionar_medic(nr_seq_regra_cubo_p,nr_seq_populacao_alvo_p,nm_usuario_p);
	
	selecionar_trat(nr_seq_regra_cubo_p,nr_seq_populacao_alvo_p,nm_usuario_p);
		
	selecionar_compl_pf(nr_seq_regra_cubo_p,nr_seq_populacao_alvo_p,nm_usuario_p);

	pop_alvo_w.nr_seq_populacao_alvo := nr_seq_populacao_alvo_p;

	ds_select_w	:= 	' select 	distinct b.cd_pessoa_fisica,	'|| enter_w ||
				'		b.dt_nascimento,		'|| enter_w ||
				'		b.nr_sequencia			'|| enter_w ||
				' from 		mprev_pessoa_cubo_ops b 	'|| enter_w ||
				' where 	1 = 1 ';

	ds_select_tabela_temp_w	:=	' select 	distinct b.cd_pessoa_fisica,	'|| enter_w ||
					'		b.dt_nascimento,		'|| enter_w ||
					'		b.nr_sequencia			'|| enter_w ||
					' from 		mprev_pessoa_cubo_ops_w b 	'|| enter_w ||
					' where 	1 = 1 ';

	/* Verificar quais foram as regras alimentadas na selecao para esta populacao alvo */
	for r_c_regras_utilizadas in c_regras_utilizadas (nr_seq_populacao_alvo_p) loop
			--Verifica se deve incluir na pesquisa ou excluir da pesquisa.
		if	(r_c_regras_utilizadas.nm_tabela_regra_cubo = 'MPREV_REGRA_CUBO_PESSOA') then
			if	(r_c_regras_utilizadas.ie_selecionar = 'S') then
				pop_alvo_w.ie_regra_pessoa	:= 'S';
			else
				pop_alvo_w.ie_regra_exc_pessoa	:= 'S';
			end if;
		elsif	(r_c_regras_utilizadas.nm_tabela_regra_cubo = 'HDM_REGRA_CUBO_COMPL') then
			if	(r_c_regras_utilizadas.ie_selecionar = 'S') then
				pop_alvo_w.ie_regra_compl_pf		:= 'S';
			else
				pop_alvo_w.ie_regra_exc_compl_pf	:= 'S';
			end if;		
		elsif	(r_c_regras_utilizadas.nm_tabela_regra_cubo = 'HDM_REGRA_CUBO_ATEND_EUP') then
			if	(r_c_regras_utilizadas.ie_selecionar = 'S') then
				pop_alvo_w.ie_regra_atend	:= 'S';
			else
				pop_alvo_w.ie_regra_exc_atend	:= 'S';
			end if;
		elsif	(r_c_regras_utilizadas.nm_tabela_regra_cubo = 'MPREV_REGRA_CUBO_PROC') then
			if	(r_c_regras_utilizadas.ie_selecionar = 'S') then
				pop_alvo_w.ie_regra_proc	:= 'S';
			else
				pop_alvo_w.ie_regra_exc_proc	:= 'S';
			end if;
		elsif	(r_c_regras_utilizadas.nm_tabela_regra_cubo = 'HDM_REGRA_CUBO_LISTA_PROB') then
			if	(r_c_regras_utilizadas.ie_selecionar = 'S') then
				pop_alvo_w.ie_regra_lista_prob	:= 'S';
			else
				pop_alvo_w.ie_regra_exc_lista_prob	:= 'S';
			end if;
		elsif	(r_c_regras_utilizadas.nm_tabela_regra_cubo = 'HDM_REGRA_CUBO_MEDIC') then
			if	(r_c_regras_utilizadas.ie_selecionar = 'S') then
				pop_alvo_w.ie_regra_medic	:= 'S';
			else
				pop_alvo_w.ie_regra_exc_medic	:= 'S';
			end if;
		elsif	(r_c_regras_utilizadas.nm_tabela_regra_cubo = 'HDM_REGRA_CUBO_TRAT') then
			if	(r_c_regras_utilizadas.ie_selecionar = 'S') then
				pop_alvo_w.ie_regra_trat	:= 'S';
			else
				pop_alvo_w.ie_regra_exc_trat	:= 'S';
			end if;
		elsif	(r_c_regras_utilizadas.nm_tabela_regra_cubo = 'MPREV_REGRA_CUBO_DIAG') then
			if	(r_c_regras_utilizadas.ie_selecionar = 'S') then
				pop_alvo_w.ie_regra_diag	:= 'S';
			else
				pop_alvo_w.ie_regra_exc_diag	:= 'S';
			end if;
		end if;
	end loop;

	ie_exec_select_princ_w := 'S';
	
	/*Busca todas as regras informadas para buscar as pessoas */
	for r_c_regras_util_ordenadas in c_regras_util_ordenadas(nr_seq_populacao_alvo_p) loop
		begin

		ds_restricao_w	:= obter_restricao_pop_alvo (pop_alvo_w, r_c_regras_util_ordenadas.nm_tabela_regra_cubo);

		if	(ds_restricao_w is not null) then
			/*O primeiro que entrar executa o select principal*/
			if	(ie_exec_select_princ_w = 'S') then
				ie_exec_select_princ_w	:= 'N';
				ds_select_comp_w	:= ds_select_w || ds_restricao_w; -- || ' order by b.cd_pessoa_fisica ';
			else
				/*Busca o maior nr_seq_controle inserido na tabela mprev_pessoa_cubo_ops_w*/
				select	nvl(max(nr_seq_controle),0)
				into	nr_seq_controle_w
				from	mprev_pessoa_cubo_ops_w
				where	nm_usuario = nm_usuario_p
				and	nr_seq_pop_alvo = nr_seq_populacao_alvo_p;
				

				/*Restringe pela sequencia de controle para buscar apenas os registros ja selecionados pela ultima regra*/
				ds_select_comp_w := ds_select_tabela_temp_w || ds_restricao_w || enter_w ||
						' and b.nr_seq_controle = ' || nr_seq_controle_w || enter_w ||
						' and b.nr_seq_pop_alvo = ' || nr_seq_populacao_alvo_p;
			end if;
			
			gravar_pessoas_cubo_ops_w (	nr_seq_populacao_alvo_p, nm_usuario_p, r_c_regras_util_ordenadas.nr_seq_controle,
							ds_select_comp_w);

		end if;
		end;
	end loop;
	
	-- Chama a procedure gravar_pessoas_populacao() para alimentar as pessoas encontradas na tabela mprev_pop_alvo_pessoa_w
	ds_select_comp_w := 	'	select	distinct(cd_pessoa_fisica),										'||enter_w||
				'		trunc(months_between(sysdate,dt_nascimento)/12) qt_idade_anos,					'||enter_w||
				'		decode(nvl(mprev_obter_participante(cd_pessoa_fisica),0),0,''N'',''S'') ie_partic_programa,	'||enter_w||
				'		mprev_obter_se_part_grupo_cont(cd_pessoa_fisica,sysdate) ie_grupo_controle,			'||enter_w||
				'		substr(obter_nome_pf(cd_pessoa_fisica),1,60) nm_pessoa,						'||enter_w||
				'		nvl(obter_sexo_pf(cd_pessoa_fisica,''C''),''I'') ie_sexo					'||enter_w||
				'	from 	mprev_pessoa_cubo_ops_w										'||enter_w||
				'	where	nm_usuario = '|| aspas_w || nm_usuario_p || aspas_w || enter_w ||
				'	and	nr_seq_controle = :nr_seq_controle_pc								'||enter_w||
				'	and	nr_seq_pop_alvo = :nr_seq_populacao_alvo_pc ';

	gravar_pessoas_populacao (nr_seq_populacao_alvo_p, nm_usuario_p, ds_select_comp_w);

	/* Novo cursor para beneficiarios */
	ds_select_w := 	' select	a.nr_seq_segurado,				' || enter_w ||
			'		a.cd_usuario_plano, 				' || enter_w ||
			'		a.ie_preco_plano, 				' || enter_w ||
			'		a.ds_preco_plano, 				' || enter_w ||
			'		a.dt_inclusao_plano, 				' || enter_w ||
			'		a.ie_situacao_contrato, 			' || enter_w ||
			'		a.ie_situacao_atend, 				' || enter_w ||
			'		pes.nr_sequencia				' || enter_w ||
			' from 		mprev_pop_alvo_pessoa pes,			' || enter_w ||
			'		mprev_pessoa_cubo_ops b 			' || enter_w ||
			' where 	pes.cd_pessoa_fisica = b.cd_pessoa_fisica	' || enter_w ||
			' and		pes.nr_seq_populacao_alvo = :nr_seq_populacao_pc';

	--Adciona valor a bind :nr_seq_populacao_pc do select principal
	sql_pck.bind_variable(':nr_seq_populacao_pc', nr_seq_populacao_alvo_p, mprev_pop_alvo_prest_pck.bind_sql_w);

	ds_select_comp_w	:= ds_select_w;

	end gerar_populacao_alvo;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER RESTRICOES POPULACAO ALVO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_pop_alvo(	dados_pop_alvo_p	mprev_pop_alvo_prest_pck.pop_alvo,
						nm_tabela_regra_p	varchar2)
				return varchar2 is

	ds_retorno_w	varchar2(4000)	:= null;

	begin
	
	-- Aqui atribui o valor para a bind :nr_seq_populacao_pc para utilizar no select dentro da procedure obter_restrincao_padrao_pop_alv.
	sql_pck.bind_variable(':nr_seq_populacao_pc', dados_pop_alvo_p.nr_seq_populacao_alvo, mprev_pop_alvo_prest_pck.bind_sql_w);

	/* --------------------------------------  REGRA POR PESSOA  -------------------------------------- */
	if	(nm_tabela_regra_p = 'MPREV_REGRA_CUBO_PESSOA') then
		--EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_pessoa) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv ('S', 8);--'MPREV_REGRA_CUBO_PESSOA'

		end if;
		--NOT EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_exc_pessoa) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('N', 8);--'MPREV_REGRA_CUBO_PESSOA'
		end if;
	elsif	(nm_tabela_regra_p = 'HDM_REGRA_CUBO_COMPL') then
		--EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_compl_pf) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv ('S', 7);--'HDM_REGRA_CUBO_COMPL'

		end if;
		--NOT EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_exc_compl_pf) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv ('N', 7);--'HDM_REGRA_CUBO_COMPL'
		end if;	
	elsif	(nm_tabela_regra_p = 'HDM_REGRA_CUBO_ATEND_EUP') then
		/* --------------------------------------  REGRA POR ATENDIMENTO  -------------------------------------- */
		--EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_atend) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('S', 5);--'HDM_REGRA_CUBO_ATEND_EUP'
		end if;
		--NOT EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_exc_atend) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('N', 5);--'HDM_REGRA_CUBO_ATEND_EUP'
		end if;
	elsif	(nm_tabela_regra_p = 'HDM_REGRA_CUBO_LISTA_PROB') then
		/* --------------------------------------  REGRA POR DIAGNOSTICO  -------------------------------------- */
		--EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_lista_prob) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('S', 3);--'HDM_REGRA_CUBO_LISTA_PROB'
		end if;
		--NOT EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_exc_lista_prob) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('N', 3);--'HDM_REGRA_CUBO_LISTA_PROB'
		end if;
	elsif	(nm_tabela_regra_p = 'HDM_REGRA_CUBO_MEDIC') then
		/* --------------------------------------  REGRA POR DIAGNOSTICO  -------------------------------------- */
		--EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_medic) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('S', 6);--'HDM_REGRA_CUBO_MEDIC'
		end if;
		--NOT EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_exc_medic) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('N', 6);--'HDM_REGRA_CUBO_MEDIC'
		end if;
	elsif	(nm_tabela_regra_p = 'HDM_REGRA_CUBO_TRAT') then
		/* --------------------------------------  REGRA POR DIAGNOSTICO  -------------------------------------- */
		--EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_trat) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('S', 2);--'HDM_REGRA_CUBO_TRAT'
		end if;
		--NOT EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_exc_trat) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('N', 2);--'HDM_REGRA_CUBO_TRAT'
		end if;
	elsif	(nm_tabela_regra_p = 'MPREV_REGRA_CUBO_DIAG') then
		/* --------------------------------------  REGRA POR DIAGNOSTICO  -------------------------------------- */
		--EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_diag) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('S', 4);--'MPREV_REGRA_CUBO_DIAG'
		end if;
		--NOT EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_exc_diag) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('N', 4);--'MPREV_REGRA_CUBO_DIAG'
		end if;
	elsif	(nm_tabela_regra_p ='MPREV_REGRA_CUBO_PROC') then
		/* --------------------------------------  REGRA POR PROCEDIMENTO  -------------------------------------- */
		--EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_proc) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('S', 1);--'MPREV_REGRA_CUBO_PROC'
		end if;
		--NOT EXISTS
		if	(upper(dados_pop_alvo_p.ie_regra_exc_proc) = 'S') then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || enter_w || obter_restricao_padrao_pop_alv('N', 1);--'MPREV_REGRA_CUBO_PROC'
		end if;
	end if;

	return	ds_retorno_w;

	end obter_restricao_pop_alvo;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>MONTA restricao PADRAO POPULACAO ALVO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_padrao_pop_alv (	ie_exists_p		varchar2,--S = exists / N = not exists
							nr_seq_tab_regra_cubo_p	number)
						return varchar2 is

	ds_retorno_w	varchar2(4000)	:= null;

	begin

	if	(upper(ie_exists_p) = 'S') then
		ds_retorno_w := ' and exists ';
	else
		ds_retorno_w := ' and not exists ';
	end if;

	ds_retorno_w :=	ds_retorno_w || enter_w ||
			'	(	select	1 							'|| enter_w ||
			' 		from	mprev_pop_alvo_selecao x 				'|| enter_w ||
			' 		where	x.nr_seq_populacao_alvo = :nr_seq_populacao_pc		'|| enter_w ||
			' 		and	x.nr_seq_pessoa_cubo 	= b.nr_sequencia		'|| enter_w ||
			'		and	x.nr_seq_tabela_regra_cubo = '|| nr_seq_tab_regra_cubo_p || enter_w ||
			' 		and	x.ie_selecionar	= '|| aspas_w || ie_exists_p || aspas_w ||') ';

	
	return	ds_retorno_w;

	end obter_restricao_padrao_pop_alv;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>INSERIR PESSOAS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure selecionar_pessoa(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_p 	number,
					nm_usuario_p		varchar2) is

	Cursor 	c_regra_pessoa
		(nr_seq_regra_cubo_pc	number) is
		select	a.nr_sequencia,
		        a.ie_sexo,
		        a.qt_idade_minima,
		        a.qt_idade_maxima,
		        a.ie_participante,
		        a.ie_grupo_controle,
			      a.ie_incluir
		from	mprev_regra_cubo_pessoa a
		where	a.nr_seq_regra_cubo	= nr_seq_regra_cubo_pc;

	regra_pessoa_w			regra_pessoa;
	nr_seq_benef_w			number(10);
	cursor_w			sql_pck.t_cursor;

	-- Tabela para inserir a sequencia do beneficiario que retorna do select dinamico "ds_select_w".
	tb_pop_alvo_sel_pessoa_w	pls_util_cta_pck.t_number_table;

	begin

	update	mprev_populacao_alvo
	set	dt_inicio_filtro_pessoa	= sysdate
	where	nr_sequencia	= nr_seq_populacao_p;

	/* Montar select dinamico e criar pessoas que possuem beneficiarios */
	ds_select_w	:=	' select	pessoa.nr_sequencia ' || enter_w ||
				' from		mprev_pessoa_cubo_ops pessoa ' || enter_w ||
				' where 	1 = 1 ';

	if	(nr_seq_regra_cubo_p is not null)then
		for r_c_regra_pessoa in c_regra_pessoa(nr_seq_regra_cubo_p) loop
			regra_pessoa_w.ie_sexo		        := r_c_regra_pessoa.ie_sexo;
			regra_pessoa_w.qt_idade_minima          := r_c_regra_pessoa.qt_idade_minima;
			regra_pessoa_w.qt_idade_maxima          := r_c_regra_pessoa.qt_idade_maxima;
			regra_pessoa_w.ie_participante          := r_c_regra_pessoa.ie_participante;
			regra_pessoa_w.ie_grupo_controle	:= r_c_regra_pessoa.ie_grupo_controle;
			regra_pessoa_w.ie_incluir		:= r_c_regra_pessoa.ie_incluir;

			ds_restricao_w		:= obter_restricao_pessoa(regra_pessoa_w);

			ds_select_comp_w	:= ds_select_w || ds_restricao_w || ' group by pessoa.nr_sequencia ';

			cursor_w 		:= sql_pck.executa_sql_cursor(ds_select_comp_w, mprev_pop_alvo_prest_pck.bind_sql_w);

			loop

				tb_pop_alvo_sel_pessoa_w.delete;

				fetch	cursor_w bulk collect
				into	tb_pop_alvo_sel_pessoa_w
				limit	qt_reg_commit_w;

				exit when tb_pop_alvo_sel_pessoa_w.count = 0;

				-- Incluir as pessoas na selecao
				forall i in tb_pop_alvo_sel_pessoa_w.first .. tb_pop_alvo_sel_pessoa_w.last
					insert into mprev_pop_alvo_selecao
						(nr_sequencia, nm_usuario, dt_atualizacao,
						nr_seq_populacao_alvo, nr_seq_pessoa_cubo, ie_selecionar,
						nm_tabela_regra_cubo, nr_seq_tabela_regra_cubo)
					values	(mprev_pop_alvo_selecao_seq.nextval, nm_usuario_p, sysdate,
						nr_seq_populacao_p, tb_pop_alvo_sel_pessoa_w(i),
						regra_pessoa_w.ie_incluir, 'MPREV_REGRA_CUBO_PESSOA', 8);
				commit;
			end loop;
			close cursor_w;
		end loop;
		commit;



	end if;

	update	mprev_populacao_alvo
	set	dt_fim_filtro_pessoa	= sysdate
	where	nr_sequencia	= nr_seq_populacao_p;

	end selecionar_pessoa;


	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER RESTRICOES PESSOAS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_pessoa	(dados_regra_p	mprev_pop_alvo_prest_pck.regra_pessoa)
					return varchar2 is

	ds_retorno_w		varchar2(4000)	:= null;
	dt_idade_minima_w	date		:= null;
	dt_idade_maxima_w	date		:= null;

	begin

	--IE_SEXO
	if	(dados_regra_p.ie_sexo is not null) then
		-- Aqui verifica monta a restricao do select.
		if	(dados_regra_p.ie_sexo = 'F') then
			ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
							'and	pessoa.ie_sexo = ' || aspas_w || 'F' || aspas_w;
		elsif	(dados_regra_p.ie_sexo = 'M') then
			ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
							'and	pessoa.ie_sexo = ' || aspas_w || 'M' || aspas_w;
		else
			null;
		end if;
	end if;

	--QT_IDADE_MAXIMA
	if	(dados_regra_p.qt_idade_maxima is not null) then
		-- Converte a idade maxima em data
		dt_idade_maxima_w	:= pkg_date_utils.add_month(sysdate,  -((dados_regra_p.qt_idade_maxima *12)+12), 0);

		-- Aqui monta a restricao do select
		ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
						' and    pessoa.dt_nascimento >= :dt_idade_maxima_pc ';
		-- Aqui adiciona o valor da bild variable
		sql_pck.bind_variable(':dt_idade_maxima_pc', dt_idade_maxima_w, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	--QT_IDADE_MINIMA
	if	(dados_regra_p.qt_idade_minima is not null) then
		-- Converter a idade minima em data
		dt_idade_minima_w	:= pkg_date_utils.add_month(sysdate,  -(dados_regra_p.qt_idade_minima *12), 0);

		-- Aqui monta a restricao do select
		ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
						' and    pessoa.dt_nascimento <= :dt_idade_minima_pc ';
		-- Aqui adiciona o valor da bild variable
		sql_pck.bind_variable(':dt_idade_minima_pc', dt_idade_minima_w, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	--IE_PARTICIPANTE
	if	(dados_regra_p.ie_participante is not null) then
		-- Aqui monta o select com as restricoes
		if	(dados_regra_p.ie_participante = 'S') then
			ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
							' and	exists '|| enter_w ||
							' 	(select 1 '|| enter_w ||
							' 	from 	mprev_programa_partic p, '|| enter_w ||
							' 		mprev_participante a '|| enter_w ||
							' 	where 	pessoa.cd_pessoa_fisica = a.cd_pessoa_fisica '|| enter_w ||
							' 	and	p.nr_seq_participante = a.nr_sequencia '|| enter_w ||
							' 	and 	sysdate between p.dt_inclusao and nvl(p.dt_exclusao,sysdate) '|| enter_w ||
							' 	)';
		elsif	(dados_regra_p.ie_participante = 'N') then
			ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
							' and	not exists '|| enter_w ||
							' 	(select 1 '|| enter_w ||
							' 	from 	mprev_programa_partic p, '|| enter_w ||
							' 		mprev_participante a '|| enter_w ||
							' 	where 	pessoa.cd_pessoa_fisica = a.cd_pessoa_fisica '|| enter_w ||
							' 	and	p.nr_seq_participante = a.nr_sequencia '|| enter_w ||
							' 	and 	sysdate between p.dt_inclusao and nvl(p.dt_exclusao,sysdate) '|| enter_w ||
							' 	)';
		else
			null;
		end if;
	end if;

	--IE_GRUPO_CONTROLE
	if	(dados_regra_p.ie_grupo_controle is not null) then
		-- -- Aqui monta o select com as restricoes
		if	(dados_regra_p.ie_grupo_controle = 'S') then
			ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
							' and	exists '|| enter_w ||
							' 	(select 1 '|| enter_w ||
							' 	from 	mprev_grupo_cont_pessoa a '|| enter_w ||
							' 	where 	pessoa.cd_pessoa_fisica = a.cd_pessoa_fisica '|| enter_w ||
							' 	and 	sysdate between a.dt_inclusao and nvl(a.dt_exclusao,sysdate) '|| enter_w ||
							' 	)';
		elsif	(dados_regra_p.ie_grupo_controle = 'N') then
			ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w || enter_w ||
							' and	not exists '|| enter_w ||
							' 	(select 1 '|| enter_w ||
							' 	from 	mprev_grupo_cont_pessoa a '|| enter_w ||
							' 	where 	pessoa.cd_pessoa_fisica = a.cd_pessoa_fisica '|| enter_w ||
							' 	and 	sysdate between a.dt_inclusao and nvl(a.dt_exclusao,sysdate) '|| enter_w ||
							' 	)';
		else
			null;
		end if;
	end if;

	return	ds_retorno_w;

	end obter_restricao_pessoa;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>INSERIR DIAGNOSTICO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure selecionar_diag(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_p 	number,
					nm_usuario_p		varchar2) is

	Cursor 	c_regra_diag
		(nr_seq_regra_cubo_pc	number) is
		select	a.nr_sequencia,
		        a.cd_doenca,
		        a.cd_doenca_inicial,
		        a.cd_doenca_final,
		        a.qt_ocorrencia,
		        a.ie_incluir
		from	mprev_regra_cubo_diag a
		where	a.nr_seq_regra_cubo	= nr_seq_regra_cubo_pc;

	regra_diag_w	regra_diag;
	nr_seq_benef_w	number(10);
	cursor_w	sql_pck.t_cursor;

	-- Tabela para inserir a sequencia do beneficiario que retorna do select dinamico "ds_select_w".
	tb_pop_alvo_sel_diag_w	pls_util_cta_pck.t_number_table;

	begin

	/* Monta select dinamico */
		ds_select_w	:=	' select	pessoa.nr_sequencia 			' || enter_w ||
					' from		mprev_pessoa_cubo_ops pessoa, 		' || enter_w ||
					' 			atendimento_paciente atend,			' || enter_w ||
					' 			diagnostico_doenca diag				' || enter_w ||
					' where		pessoa.cd_pessoa_fisica = atend.cd_pessoa_fisica ' || enter_w ||
					' and		atend.nr_atendimento = diag.nr_atendimento ';

	if	(nr_seq_regra_cubo_p is not null)then
		for r_c_regra_diag in c_regra_diag(nr_seq_regra_cubo_p) loop
			regra_diag_w.cd_doenca		:= r_c_regra_diag.cd_doenca;
			regra_diag_w.cd_doenca_inicial  := r_c_regra_diag.cd_doenca_inicial;
			regra_diag_w.cd_doenca_final  	:= r_c_regra_diag.cd_doenca_final;
			regra_diag_w.qt_ocorrencia  	:= r_c_regra_diag.qt_ocorrencia;
			regra_diag_w.ie_incluir		:= r_c_regra_diag.ie_incluir;


			ds_restricao_w		:= obter_restricao_diag(regra_diag_w);

			ds_select_comp_w	:= ds_select_w || ds_restricao_w;

			cursor_w		:= sql_pck.executa_sql_cursor(ds_select_comp_w, mprev_pop_alvo_prest_pck.bind_sql_w);

			loop

				tb_pop_alvo_sel_diag_w.delete;

				fetch	cursor_w bulk collect
				into	tb_pop_alvo_sel_diag_w
				limit	qt_reg_commit_w;

				exit when tb_pop_alvo_sel_diag_w.count = 0;

				-- Incluir as pessoas na selecao
				forall i in tb_pop_alvo_sel_diag_w.first .. tb_pop_alvo_sel_diag_w.last
					insert into mprev_pop_alvo_selecao
						(nr_sequencia, nm_usuario, dt_atualizacao,
						nr_seq_populacao_alvo, nr_seq_pessoa_cubo, ie_selecionar,
						nm_tabela_regra_cubo, nr_seq_tabela_regra_cubo)
					values	(mprev_pop_alvo_selecao_seq.nextval, nm_usuario_p, sysdate,
						nr_seq_populacao_p, tb_pop_alvo_sel_diag_w(i),
						regra_diag_w.ie_incluir, 'MPREV_REGRA_CUBO_DIAG', 4);
				commit;
			end loop;
			close cursor_w;
		end loop;
		commit;
	end if;

	end selecionar_diag;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER restricoes DIAGNOSTICO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_diag(dados_regra_p	mprev_pop_alvo_prest_pck.regra_diag)
				return varchar2 is

	ds_retorno_w	varchar2(4000)	:= null;

	begin


	--CID (CD_DOENCA)
	if	(dados_regra_p.cd_doenca is not null) then
		-- Aqui monta a restricao e atualiza o valor das binds.
		ds_retorno_w :=	ds_retorno_w || enter_w ||
						' and	        (select count(1) ' || enter_w ||
						'		from 	diagnostico_doenca x ' || enter_w ||
						'		where 	x.nr_atendimento = atend.nr_atendimento ' || enter_w ||
						'		and		x.dt_diagnostico >= :dt_referencia_pc ' || enter_w ||
						'		and 	x.cd_doenca = :cd_doenca_pc) >= nvl(:qt_ocorrencia_pc,1) ';

		sql_pck.bind_variable(':cd_doenca_pc', dados_regra_p.cd_doenca, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':qt_ocorrencia_pc', dados_regra_p.qt_ocorrencia, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	--Ira ter dados no CID (CD_DOENCA) ou no CID (CD_DOENCA_INICIAL e CD_DOENCA_FINAL), o usuario nao pode informar os 2.

	--CID (CD_DOENCA_INICIAL e CD_DOENCA_FINAL)
	/*Exemplo:
	cd_doenca_inicial = C100 e cd_doenca_final = C105
	Diferente do bloco acima "CID (CD_DOENCA)" que verifica apenas o cd_doenca informado esta restricao pega todas as doencas entre os codigos passados:
	C100, C101, C102, C103, C104 e C105
	*/
	if	(dados_regra_p.cd_doenca_inicial is not null or dados_regra_p.cd_doenca_final is not null) then
		-- Aqui monta a restricao e atualiza o valor das binds.
		ds_retorno_w :=	ds_retorno_w || enter_w ||
						' and	        (select count(1) ' || enter_w ||
						'		from 	diagnostico_doenca x ' || enter_w ||
						'		where 	x.nr_atendimento = atend.nr_atendimento ' || enter_w ||
						'		and		x.dt_diagnostico >= :dt_referencia_pc  '|| enter_w ||
						'		and 	x.cd_doenca ';
		if	(dados_regra_p.cd_doenca_inicial is not null and dados_regra_p.cd_doenca_final is not null) then
			ds_retorno_w :=	ds_retorno_w || enter_w ||  ' between :cd_doenca_inicial_pc and :cd_doenca_final_pc) >= nvl(:qt_ocorrencia_pc,1) ';
		elsif	(dados_regra_p.cd_doenca_inicial is not null) then
			ds_retorno_w :=	ds_retorno_w || enter_w ||  ' > :cd_doenca_inicial_pc) >= nvl(:qt_ocorrencia_pc,1)';
		elsif	(dados_regra_p.cd_doenca_final is not null) then
			ds_retorno_w :=	ds_retorno_w || enter_w ||  ' < :cd_doenca_final_pc) >= nvl(:qt_ocorrencia_pc,1)';
		end if;

		if	(dados_regra_p.cd_doenca_inicial is not null and dados_regra_p.cd_doenca_final is not null) then
			sql_pck.bind_variable(':cd_doenca_inicial_pc', dados_regra_p.cd_doenca_inicial, mprev_pop_alvo_prest_pck.bind_sql_w);
			sql_pck.bind_variable(':cd_doenca_final_pc', dados_regra_p.cd_doenca_final, mprev_pop_alvo_prest_pck.bind_sql_w);
		elsif	(dados_regra_p.cd_doenca_inicial is not null) then
			sql_pck.bind_variable(':cd_doenca_inicial_pc', dados_regra_p.cd_doenca_inicial, mprev_pop_alvo_prest_pck.bind_sql_w);
		elsif	(dados_regra_p.cd_doenca_final is not null) then
			sql_pck.bind_variable(':cd_doenca_final_pc', dados_regra_p.cd_doenca_final, mprev_pop_alvo_prest_pck.bind_sql_w);
		end if;
		sql_pck.bind_variable(':qt_ocorrencia_pc', dados_regra_p.qt_ocorrencia, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	return	ds_retorno_w;

	end obter_restricao_diag;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>INSERIR ATENDIMENTO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure selecionar_atend(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_p 	number,
					nm_usuario_p		varchar2) is

	Cursor 	c_regra_atend
		(nr_seq_regra_cubo_pc	number) is
		select	a.nr_sequencia,
		        a.qt_ocorrencia_minima,
		        a.qt_ocorrencia_maxima,
			a.ie_tipo_atendimento,
			a.cd_medico_resp,
			a.ie_incluir
		from	HDM_REGRA_CUBO_ATEND_EUP a
		where	a.nr_seq_regra_cubo	= nr_seq_regra_cubo_pc;

	regra_atend_w	regra_atend;
	nr_seq_benef_w	number(10);
	cursor_w	sql_pck.t_cursor;

	-- Tabela para inserir a sequencia do beneficiario que retorna do select dinamico "ds_select_w".
	tb_pop_alvo_sel_atend_w	pls_util_cta_pck.t_number_table;

	begin

	update	mprev_populacao_alvo
	set	dt_inicio_filtro_atend	= sysdate
	where	nr_sequencia	= nr_seq_populacao_p;

	if	(nr_seq_regra_cubo_p is not null)then

		/* Monta select dinamico */
		ds_select_w	:=	' select	pessoa.nr_sequencia 				' || enter_w ||
					' from		mprev_pessoa_cubo_ops pessoa, 			' || enter_w ||
					' 		atendimento_paciente atend			' || enter_w ||
					' where		pessoa.cd_pessoa_fisica = atend.cd_pessoa_fisica	';

			
		for r_c_regra_atend in c_regra_atend(nr_seq_regra_cubo_p) loop
			regra_atend_w.qt_ocorrencia_minima	:= nvl(r_c_regra_atend.qt_ocorrencia_minima,0);
			regra_atend_w.qt_ocorrencia_maxima	:= nvl(r_c_regra_atend.qt_ocorrencia_maxima,99999);
			regra_atend_w.ie_tipo_atendimento	:= r_c_regra_atend.ie_tipo_atendimento;
			regra_atend_w.cd_medico_resp	:= r_c_regra_atend.cd_medico_resp;
			regra_atend_w.ie_incluir		:= r_c_regra_atend.ie_incluir;
			
			ds_restricao_w	 	:= obter_restricao_atend(regra_atend_w);

			ds_select_comp_w	:= ds_select_w || ds_restricao_w;

			cursor_w		:= sql_pck.executa_sql_cursor(ds_select_comp_w, mprev_pop_alvo_prest_pck.bind_sql_w);

			loop

				tb_pop_alvo_sel_atend_w.delete;

				fetch	cursor_w bulk collect
				into	tb_pop_alvo_sel_atend_w
				limit	qt_reg_commit_w;

				exit when tb_pop_alvo_sel_atend_w.count = 0;

				-- Incluir as pessoas na selecao
				forall i in tb_pop_alvo_sel_atend_w.first .. tb_pop_alvo_sel_atend_w.last
					insert into mprev_pop_alvo_selecao
						(nr_sequencia, nm_usuario, dt_atualizacao,
						nr_seq_populacao_alvo, nr_seq_pessoa_cubo, ie_selecionar,
						nm_tabela_regra_cubo, nr_seq_tabela_regra_cubo)
					values	(mprev_pop_alvo_selecao_seq.nextval, nm_usuario_p, sysdate,
						nr_seq_populacao_p, tb_pop_alvo_sel_atend_w(i),
						regra_atend_w.ie_incluir, 'HDM_REGRA_CUBO_ATEND_EUP', 5);

				commit;
			end loop;
			close cursor_w;
		end loop;


		commit;
	end if;

	update	mprev_populacao_alvo
	set	dt_fim_filtro_atend	= sysdate
	where	nr_sequencia	= nr_seq_populacao_p;

	end selecionar_atend;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER restricoes ATENDIMENTO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_atend(dados_regra_p	mprev_pop_alvo_prest_pck.regra_atend)
				return varchar2 is

	ds_retorno_w	varchar2(4000)	:= null;

	begin
		--ie_TIPO_ATENDIMENTO
		if	(dados_regra_p.ie_tipo_atendimento is not null) then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
							' and	atend.ie_tipo_atendimento = :ie_tipo_atendimento_pc ';

			sql_pck.bind_variable(':ie_tipo_atendimento_pc', dados_regra_p.ie_tipo_atendimento, mprev_pop_alvo_prest_pck.bind_sql_w);
		end if;

		--CD_MEDICO_RESP
		if	(dados_regra_p.cd_medico_resp is not null) then
			-- Aqui monta a restricao e atualiza o valor das binds.
			ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
							' and	atend.cd_medico_resp = :cd_medico_resp ';

			sql_pck.bind_variable(':cd_medico_resp', dados_regra_p.cd_medico_resp,  mprev_pop_alvo_prest_pck.bind_sql_w);
		end if;

		--DT_REFERENCIA_W nunca e tratado os filtros de data. Considerar sempre dentro do periodo do cubo.
		ds_retorno_w :=	ds_retorno_w || enter_w || 
								' and	atend.dt_entrada >= :dt_referencia_pc ' || enter_w ||
								' group by pessoa.nr_sequencia ' || enter_w ||
								' having count(pessoa.nr_sequencia) ';

		sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);
		
		--QT_OCORRENCIA_MINIMA e QT_OCORRENCIA_MAXIMA
		if	(dados_regra_p.qt_ocorrencia_minima is not null or dados_regra_p.qt_ocorrencia_maxima is not null) then
			-- Aqui monta a restricao e atualiza o valor das binds.
			if	(dados_regra_p.qt_ocorrencia_minima is not null and dados_regra_p.qt_ocorrencia_maxima is not null) then
				ds_retorno_w :=	ds_retorno_w || enter_w ||  ' between :qt_ocorrencia_minima_pc and :qt_ocorrencia_maxima_pc ';
			elsif	(dados_regra_p.qt_ocorrencia_minima is not null) then
				ds_retorno_w :=	ds_retorno_w || enter_w ||  ' >= :qt_ocorrencia_minima_pc ';
			elsif	(dados_regra_p.qt_ocorrencia_maxima is not null) then
				ds_retorno_w :=	ds_retorno_w || enter_w ||  ' <= :qt_ocorrencia_maxima_pc ';
			end if;
			
			if	(dados_regra_p.qt_ocorrencia_minima is not null and dados_regra_p.qt_ocorrencia_maxima is not null) then
				sql_pck.bind_variable(':qt_ocorrencia_minima_pc', dados_regra_p.qt_ocorrencia_minima, mprev_pop_alvo_prest_pck.bind_sql_w);
				sql_pck.bind_variable(':qt_ocorrencia_maxima_pc', dados_regra_p.qt_ocorrencia_maxima, mprev_pop_alvo_prest_pck.bind_sql_w);
			elsif	(dados_regra_p.qt_ocorrencia_minima is not null) then
				sql_pck.bind_variable(':qt_ocorrencia_minima_pc', dados_regra_p.qt_ocorrencia_minima, mprev_pop_alvo_prest_pck.bind_sql_w);
			elsif	(dados_regra_p.qt_ocorrencia_maxima is not null) then
				sql_pck.bind_variable(':qt_ocorrencia_maxima_pc', dados_regra_p.qt_ocorrencia_maxima, mprev_pop_alvo_prest_pck.bind_sql_w);
			end if;
		end if;

	return	ds_retorno_w;

	end obter_restricao_atend;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>INSERIR PROCEDIMENTO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure selecionar_proc(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_p 	number,
					nm_usuario_p		varchar2) is

	Cursor 	c_regra_proc
		(nr_seq_regra_cubo_pc	number) is
		select	a.nr_sequencia,
		        a.cd_procedimento,
		        a.ie_origem_proced,
			a.qt_ocorrencia,
			a.ie_incluir
		from	mprev_regra_cubo_proc a
		where	a.nr_seq_regra_cubo	= nr_seq_regra_cubo_pc;
		
	regra_proc_w		regra_proc;
	nr_seq_benef_w		number(10);
	cursor_w		sql_pck.t_cursor;

	-- Tabela para inserir a sequencia do beneficiario que retorna do select dinamico "ds_select_w".
	tb_pop_alvo_sel_proc_w	pls_util_cta_pck.t_number_table;

	begin

	
	if	(nr_seq_regra_cubo_p is not null)then
	
	/* Monta select dinamico*/
		ds_select_w	:=	' select	pessoa.nr_sequencia 				' || enter_w ||
						' from		mprev_pessoa_cubo_ops pessoa, 			' || enter_w ||
						' 			atendimento_paciente atend,			' || enter_w ||
						' 			procedimento_paciente proc			' || enter_w ||
						' where		pessoa.cd_pessoa_fisica = atend.cd_pessoa_fisica ' || enter_w ||
						' and		atend.nr_atendimento = proc.nr_atendimento ' || enter_w ||
						' and		proc.cd_motivo_exc_conta IS NULL ';

	
		for r_c_regra_proc in c_regra_proc(nr_seq_regra_cubo_p) loop
			ds_restricao_w := null;
			regra_proc_w.nr_sequencia	:= r_c_regra_proc.nr_sequencia;
			regra_proc_w.cd_procedimento	:= r_c_regra_proc.cd_procedimento;
			regra_proc_w.ie_origem_proced	:= r_c_regra_proc.ie_origem_proced;
			regra_proc_w.qt_ocorrencia	:= r_c_regra_proc.qt_ocorrencia;
			regra_proc_w.ie_incluir		:= r_c_regra_proc.ie_incluir;

			ds_restricao_w	 	:= obter_restricao_proc(regra_proc_w) ||' --'|| regra_proc_w.cd_procedimento;

			ds_select_comp_w 	:= ds_select_w ||enter_w|| ds_restricao_w;			
			
						
			cursor_w := sql_pck.executa_sql_cursor(ds_select_comp_w, mprev_pop_alvo_prest_pck.bind_sql_w);
			
			loop

				tb_pop_alvo_sel_proc_w.delete;

				fetch	cursor_w bulk collect
				into	tb_pop_alvo_sel_proc_w
				limit	qt_reg_commit_w;

				exit when tb_pop_alvo_sel_proc_w.count = 0;

				-- Incluir as pessoas na selecao
				forall i in tb_pop_alvo_sel_proc_w.first .. tb_pop_alvo_sel_proc_w.last
					insert into mprev_pop_alvo_selecao
						(nr_sequencia, nm_usuario, dt_atualizacao,
						nr_seq_populacao_alvo, nr_seq_pessoa_cubo, ie_selecionar,
						nm_tabela_regra_cubo, nr_seq_tabela_regra_cubo)
					values	(mprev_pop_alvo_selecao_seq.nextval, nm_usuario_p, sysdate,
						nr_seq_populacao_p, tb_pop_alvo_sel_proc_w(i),
						regra_proc_w.ie_incluir, 'MPREV_REGRA_CUBO_PROC', 1);
				commit;
			end loop;
			close cursor_w;
		end loop;
		commit;
	end if;

	end selecionar_proc;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER restricoes PROCEDIMENTO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_proc(dados_regra_p	mprev_pop_alvo_prest_pck.regra_proc)
				return varchar2 is

	ds_retorno_w		varchar2(4000)	:= null;
	ds_retorno_guia_w	varchar2(4000)	:= null;

	begin

	--Bloco inicial do select
	ds_retorno_w := 	' and	proc.dt_procedimento >= :dt_referencia_pc ';

	sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);

	--CD_PROCEDIMENTO
	if	(dados_regra_p.cd_procedimento is not null) then
		-- Aqui monta a restricao e atualiza o valor das binds.
		ds_retorno_w :=	ds_retorno_w || enter_w ||
				' 	and 	proc.cd_procedimento = :cd_procedimento_'||dados_regra_p.cd_procedimento||'_pc';

		sql_pck.bind_variable(':cd_procedimento_'||dados_regra_p.cd_procedimento||'_pc', dados_regra_p.cd_procedimento, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	--IE_ORIGEM_PROCED
	if	(dados_regra_p.ie_origem_proced is not null) then
		-- Aqui monta a restricao e atualiza o valor das binds.
		ds_retorno_w :=	ds_retorno_w || enter_w ||
				'	and 	proc.ie_origem_proced = :ie_origem_proced_'||dados_regra_p.ie_origem_proced||'_pc';

		sql_pck.bind_variable(':ie_origem_proced_'||dados_regra_p.ie_origem_proced||'_pc', dados_regra_p.ie_origem_proced, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	--QT_OCORRENCIA
	ds_retorno_w :=	ds_retorno_w || enter_w ||  
				' group by pessoa.nr_sequencia ' || enter_w || 
				' having count(proc.cd_procedimento) >= nvl(:qt_ocorrencia_'||dados_regra_p.qt_ocorrencia||'_pc'||',1) ';

	sql_pck.bind_variable(':qt_ocorrencia_'||dados_regra_p.qt_ocorrencia||'_pc', dados_regra_p.qt_ocorrencia, mprev_pop_alvo_prest_pck.bind_sql_w);

	return	ds_retorno_w;

	end obter_restricao_proc;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>INSERIR TRATAMENTO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure selecionar_trat(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_p 	number,
					nm_usuario_p		varchar2) is

	Cursor 	c_regra_trat
		(nr_seq_regra_cubo_pc	number) is
		select	a.nr_sequencia,
		        a.ie_tipo_tratamento,
				a.ie_incluir
		from	hdm_regra_cubo_trat a
		where	a.nr_seq_regra_cubo	= nr_seq_regra_cubo_pc;
		
	regra_trat_w		regra_trat;
	cursor_w			sql_pck.t_cursor;

	-- Tabela para inserir a sequencia do beneficiario que retorna do select dinamico "ds_select_w".
	tb_pop_alvo_sel_trat_w	pls_util_cta_pck.t_number_table;

	begin

	
	if	(nr_seq_regra_cubo_p is not null)then
	
	ds_select_w	:=	'';
	
		for r_c_regra_trat in c_regra_trat(nr_seq_regra_cubo_p) loop
			ds_restricao_w := null;
			regra_trat_w.nr_sequencia	:= r_c_regra_trat.nr_sequencia;
			regra_trat_w.ie_tipo_tratamento	:= r_c_regra_trat.ie_tipo_tratamento;
			regra_trat_w.ie_incluir		:= r_c_regra_trat.ie_incluir;

			ds_restricao_w	 	:= obter_restricao_trat(regra_trat_w) ||' --'|| regra_trat_w.ie_tipo_tratamento;

			ds_select_comp_w 	:= ds_select_w ||enter_w|| ds_restricao_w;			
			
			cursor_w := sql_pck.executa_sql_cursor(ds_select_comp_w, mprev_pop_alvo_prest_pck.bind_sql_w);
			
			loop

				tb_pop_alvo_sel_trat_w.delete;

				fetch	cursor_w bulk collect
				into	tb_pop_alvo_sel_trat_w
				limit	qt_reg_commit_w;

				exit when tb_pop_alvo_sel_trat_w.count = 0;

				-- Incluir as pessoas na selecao
				forall i in tb_pop_alvo_sel_trat_w.first .. tb_pop_alvo_sel_trat_w.last
					insert into mprev_pop_alvo_selecao
						(nr_sequencia, nm_usuario, dt_atualizacao,
						nr_seq_populacao_alvo, nr_seq_pessoa_cubo, ie_selecionar,
						nm_tabela_regra_cubo, nr_seq_tabela_regra_cubo)
					values	(mprev_pop_alvo_selecao_seq.nextval, nm_usuario_p, sysdate,
						nr_seq_populacao_p, tb_pop_alvo_sel_trat_w(i),
						regra_trat_w.ie_incluir, 'HDM_REGRA_CUBO_TRAT', 2);
				commit;
			end loop;
			close cursor_w;
		end loop;
		commit;
	end if;

	end selecionar_trat;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER restricoes TRATAMENTO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_trat(dados_regra_p	mprev_pop_alvo_prest_pck.regra_trat)
				return varchar2 is

	ds_retorno_w		varchar2(4000)	:= null;
	ds_retorno_guia_w	varchar2(4000)	:= null;

	begin
	
	--IE_TIPO_TRATAMENTO
	if	(dados_regra_p.IE_TIPO_TRATAMENTO = 1) then --Quimioterapia
		ds_retorno_w :=	' select	pessoa.nr_sequencia 			' || enter_w ||
						' from		mprev_pessoa_cubo_ops pessoa, 	' || enter_w ||
						' 			paciente_setor trat 			' || enter_w ||
						' where		pessoa.cd_pessoa_fisica = trat.cd_pessoa_fisica	' || enter_w ||
						' and		trat.dt_atualizacao_nrec >= :dt_referencia_pc ';

		sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);
		
	elsif (dados_regra_p.IE_TIPO_TRATAMENTO = 2) then -- radioterapia
		ds_retorno_w :=	' select	pessoa.nr_sequencia 			' || enter_w ||
						' from		mprev_pessoa_cubo_ops pessoa, 	' || enter_w ||
						' 			rxt_tumor trat 					' || enter_w ||
						' where		pessoa.cd_pessoa_fisica = trat.cd_pessoa_fisica	' || enter_w ||
						' and		trat.dt_atualizacao_nrec >= :dt_referencia_pc ';

		sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	return	ds_retorno_w;

	end obter_restricao_trat;	
	
	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>INSERIR LISTA PROBLEMA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure selecionar_lista_prob(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_p 	number,
					nm_usuario_p		varchar2) is

	Cursor 	c_regra_lista_prob
		(nr_seq_regra_cubo_pc	number) is
		select	a.nr_sequencia,
		        a.cd_ciap,
				cd_doenca,
				cd_doenca_inicial,
				cd_doenca_final,
				ie_status,
				qt_ocorrencia,
				ie_incluir
		from	hdm_regra_cubo_lista_prob a
		where	a.nr_seq_regra_cubo	= nr_seq_regra_cubo_pc;
		
	regra_lista_prob_w		regra_lista_prob;
	cursor_w		sql_pck.t_cursor;

	-- Tabela para inserir a sequencia do beneficiario que retorna do select dinamico "ds_select_w".
	tb_pop_alvo_sel_lista_prob_w	pls_util_cta_pck.t_number_table;

	begin

	
	if	(nr_seq_regra_cubo_p is not null)then
	
	/* Monta select dinamico*/
		ds_select_w	:=	' select	pessoa.nr_sequencia 				' || enter_w ||
						' from		mprev_pessoa_cubo_ops pessoa, 			' || enter_w ||
						' 			lista_problema_pac lista			' || enter_w ||
						' where		pessoa.cd_pessoa_fisica = lista.cd_pessoa_fisica ';
						
	
		for r_c_regra_lista_prob in c_regra_lista_prob(nr_seq_regra_cubo_p) loop
			ds_restricao_w := null;
			regra_lista_prob_w.nr_sequencia	:= r_c_regra_lista_prob.nr_sequencia;
			
			regra_lista_prob_w.cd_ciap	:= r_c_regra_lista_prob.cd_ciap;
			regra_lista_prob_w.cd_doenca	:= r_c_regra_lista_prob.cd_doenca;
			regra_lista_prob_w.cd_doenca_inicial	:= r_c_regra_lista_prob.cd_doenca_inicial;
			regra_lista_prob_w.cd_doenca_final	:= r_c_regra_lista_prob.cd_doenca_final;
			regra_lista_prob_w.ie_status	:= r_c_regra_lista_prob.ie_status;
			regra_lista_prob_w.qt_ocorrencia	:= r_c_regra_lista_prob.qt_ocorrencia;
			regra_lista_prob_w.ie_incluir		:= r_c_regra_lista_prob.ie_incluir;

			ds_restricao_w	 	:= obter_restricao_lista_prob(regra_lista_prob_w) ||' --'|| regra_lista_prob_w.nr_sequencia;

			ds_select_comp_w 	:= ds_select_w ||enter_w|| ds_restricao_w;			
			
						
			cursor_w := sql_pck.executa_sql_cursor(ds_select_comp_w, mprev_pop_alvo_prest_pck.bind_sql_w);
			
			loop

				tb_pop_alvo_sel_lista_prob_w.delete;

				fetch	cursor_w bulk collect
				into	tb_pop_alvo_sel_lista_prob_w
				limit	qt_reg_commit_w;

				exit when tb_pop_alvo_sel_lista_prob_w.count = 0;

				-- Incluir as pessoas na selecao
				forall i in tb_pop_alvo_sel_lista_prob_w.first .. tb_pop_alvo_sel_lista_prob_w.last
					insert into mprev_pop_alvo_selecao
						(nr_sequencia, nm_usuario, dt_atualizacao,
						nr_seq_populacao_alvo, nr_seq_pessoa_cubo, ie_selecionar,
						nm_tabela_regra_cubo, nr_seq_tabela_regra_cubo)
					values	(mprev_pop_alvo_selecao_seq.nextval, nm_usuario_p, sysdate,
						nr_seq_populacao_p, tb_pop_alvo_sel_lista_prob_w(i),
						regra_lista_prob_w.ie_incluir, 'HDM_REGRA_CUBO_LISTA_PROB', 3);
				commit;
			end loop;
			close cursor_w;
		end loop;
		commit;
	end if;

	end selecionar_lista_prob;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER restricoes LISTA PROBLEMA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_lista_prob(dados_regra_p	mprev_pop_alvo_prest_pck.regra_lista_prob)
				return varchar2 is

	ds_retorno_w		varchar2(4000)	:= null;
	ds_retorno_guia_w	varchar2(4000)	:= null;

	begin
	--CIAP
	if	(dados_regra_p.cd_ciap is not null) then

		ds_retorno_w :=	ds_retorno_w || enter_w ||
						' and	        (select count(1) ' || enter_w ||
						'		from 	lista_problema_pac lista ' || enter_w ||
						'		where 	pessoa.cd_pessoa_fisica = lista.cd_pessoa_fisica ' || enter_w ||
						'		and		lista.dt_inicio >= :dt_referencia_pc ' || enter_w ||
						'		and 	lista.cd_ciap = :cd_ciap) >= nvl(:qt_ocorrencia_pc,1) ';

		sql_pck.bind_variable(':cd_ciap', dados_regra_p.cd_ciap, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':qt_ocorrencia_pc', dados_regra_p.qt_ocorrencia, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);	
	
	end if;	
	
	
	--CID (CD_DOENCA)
	if	(dados_regra_p.cd_doenca is not null) then
		-- Aqui monta a restricao e atualiza o valor das binds.
		ds_retorno_w :=	ds_retorno_w || enter_w ||
						' and	        (select count(1) ' || enter_w ||
						'		from 	lista_problema_pac lista ' || enter_w ||
						'		where 	pessoa.cd_pessoa_fisica = lista.cd_pessoa_fisica ' || enter_w ||
						'		and		lista.dt_inicio >= :dt_referencia_pc ' || enter_w ||
						'		and 	lista.cd_doenca = :cd_doenca_pc) >= nvl(:qt_ocorrencia_pc,1) ';

		sql_pck.bind_variable(':cd_doenca_pc', dados_regra_p.cd_doenca, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':qt_ocorrencia_pc', dados_regra_p.qt_ocorrencia, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	if	(dados_regra_p.cd_doenca_inicial is not null or dados_regra_p.cd_doenca_final is not null) then
		-- Aqui monta a restricao e atualiza o valor das binds.
		ds_retorno_w :=	ds_retorno_w || enter_w ||
						' and	        (select count(1) ' || enter_w ||
						'		from 	lista_problema_pac x ' || enter_w ||
						'		where 	x.cd_pessoa_fisica = atend.cd_pessoa_fisica ' || enter_w ||
						'		and		x.dt_inicio >= :dt_referencia_pc  '|| enter_w ||
						'		and 	x.cd_doenca ';
		if	(dados_regra_p.cd_doenca_inicial is not null and dados_regra_p.cd_doenca_final is not null) then
			ds_retorno_w :=	ds_retorno_w || enter_w ||  ' between :cd_doenca_inicial_pc and :cd_doenca_final_pc) >= nvl(:qt_ocorrencia_pc,1) ';
		elsif	(dados_regra_p.cd_doenca_inicial is not null) then
			ds_retorno_w :=	ds_retorno_w || enter_w ||  ' > :cd_doenca_inicial_pc) >= nvl(:qt_ocorrencia_pc,1)';
		elsif	(dados_regra_p.cd_doenca_final is not null) then
			ds_retorno_w :=	ds_retorno_w || enter_w ||  ' < :cd_doenca_final_pc) >= nvl(:qt_ocorrencia_pc,1)';
		end if;

		if	(dados_regra_p.cd_doenca_inicial is not null and dados_regra_p.cd_doenca_final is not null) then
			sql_pck.bind_variable(':cd_doenca_inicial_pc', dados_regra_p.cd_doenca_inicial, mprev_pop_alvo_prest_pck.bind_sql_w);
			sql_pck.bind_variable(':cd_doenca_final_pc', dados_regra_p.cd_doenca_final, mprev_pop_alvo_prest_pck.bind_sql_w);
		elsif	(dados_regra_p.cd_doenca_inicial is not null) then
			sql_pck.bind_variable(':cd_doenca_inicial_pc', dados_regra_p.cd_doenca_inicial, mprev_pop_alvo_prest_pck.bind_sql_w);
		elsif	(dados_regra_p.cd_doenca_final is not null) then
			sql_pck.bind_variable(':cd_doenca_final_pc', dados_regra_p.cd_doenca_final, mprev_pop_alvo_prest_pck.bind_sql_w);
		end if;
		sql_pck.bind_variable(':qt_ocorrencia_pc', dados_regra_p.qt_ocorrencia, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':dt_referencia_pc', dt_referencia_w, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	return	ds_retorno_w;

	end obter_restricao_lista_prob;	
	
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>INSERIR MEDICAMENTO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure selecionar_medic(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_p 	number,
					nm_usuario_p		varchar2) is

	Cursor 	c_regra_medic
		(nr_seq_regra_cubo_pc	number) is
		select	a.nr_sequencia,
		        cd_material,
				qt_dose_inicial,
				qt_dose_final,
				cd_unidade_medida,
				ie_incluir
		from	hdm_regra_cubo_medic a
		where	a.nr_seq_regra_cubo	= nr_seq_regra_cubo_pc;
		
	regra_medic_w		regra_medic;
	cursor_w		sql_pck.t_cursor;

	-- Tabela para inserir a sequencia do beneficiario que retorna do select dinamico "ds_select_w".
	tb_pop_alvo_sel_medic_w	pls_util_cta_pck.t_number_table;

	begin

	
	if	(nr_seq_regra_cubo_p is not null)then
	
	/* Monta select dinamico*/
		ds_select_w	:=	' select	pessoa.nr_sequencia 									' || enter_w ||
						' from		mprev_pessoa_cubo_ops pessoa,							' || enter_w ||
						'			(SELECT	cd_pessoa_fisica seg_cd_pessoa_fisica,			' || enter_w ||
						'				cd_material med_cd_material,						' || enter_w ||
						'				NVL(dt_inicio,dt_registro) dt_medicamento_inicial,	' || enter_w ||
						'				NVL(dt_fim,dt_registro) dt_medicamento_final,		' || enter_w ||
						'				cast(qt_dose AS NUMBER(20,6)) AS qt_dose,			' || enter_w ||
						'				cd_unid_med cd_unid_medida							' || enter_w ||
						'			FROM	paciente_medic_uso								' || enter_w ||
						'			UNION													' || enter_w ||
						'			SELECT	cd_pessoa_fisica seg_cd_pessoa_fisica,			' || enter_w ||
						'				cd_material,										' || enter_w ||
						'				dt_inicio,											' || enter_w ||
						'				dt_inicio,											' || enter_w ||
						'				cast(qt_dose AS NUMBER(20,6)) AS qt_dose,			' || enter_w ||
						'				cd_unidade_medida									' || enter_w ||
						'			FROM	medic_uso_continuo								' || enter_w ||
						'			UNION													' || enter_w ||
						'			SELECT	b.cd_pessoa_fisica seg_cd_pessoa_fisica,		' || enter_w ||
						'				a.cd_material,										' || enter_w ||
						'				b.dt_prescricao,									' || enter_w ||
						'				b.dt_prescricao,									' || enter_w ||
						'				cast(a.qt_dose AS NUMBER(20,6)) AS qt_dose,			' || enter_w ||
						'				a.cd_unidade_medida_dose							' || enter_w ||
						'			FROM	prescr_material a,								' || enter_w ||
						'				prescr_medica b										' || enter_w ||
						'			WHERE	a.nr_prescricao = b.nr_prescricao				' || enter_w ||
						'			AND	a.ie_agrupador = 1									' || enter_w ||
						'			UNION													' || enter_w ||
						'			SELECT	b.cd_pessoa_fisica seg_cd_pessoa_fisica,		' || enter_w ||
						'				a.cd_material,										' || enter_w ||
						'				b.dt_periodo_inicial,								' || enter_w ||
						'				b.dt_periodo_final,									' || enter_w ||
						'				cast(a.qt_dose AS NUMBER(20,6)) AS qt_dose,			' || enter_w ||
						'				a.cd_unidade_baixa									' || enter_w ||
						'			FROM	fa_entrega_medicacao_item a,					' || enter_w ||
						'				fa_entrega_medicacao b								' || enter_w ||
						'			WHERE	a.nr_seq_fa_entrega = b.nr_sequencia) medic		' || enter_w ||
						'where		pessoa.cd_pessoa_fisica = medic.seg_cd_pessoa_fisica ';
						
	
		for r_c_regra_medic in c_regra_medic(nr_seq_regra_cubo_p) loop
			ds_restricao_w := null;
			regra_medic_w.nr_sequencia	:= r_c_regra_medic.nr_sequencia;
			
			regra_medic_w.cd_material	:= r_c_regra_medic.cd_material;
			regra_medic_w.cd_unidade_medida	:= r_c_regra_medic.cd_unidade_medida;
			regra_medic_w.qt_dose_inicial	:= r_c_regra_medic.qt_dose_inicial;
			regra_medic_w.qt_dose_final	:= r_c_regra_medic.qt_dose_final;
			regra_medic_w.ie_incluir		:= r_c_regra_medic.ie_incluir;

			ds_restricao_w	 	:= obter_restricao_medic(regra_medic_w) ||' --'|| regra_medic_w.nr_sequencia;

			ds_select_comp_w 	:= ds_select_w ||enter_w|| ds_restricao_w;			
			
						
			cursor_w := sql_pck.executa_sql_cursor(ds_select_comp_w, mprev_pop_alvo_prest_pck.bind_sql_w);
			
			loop

				tb_pop_alvo_sel_medic_w.delete;

				fetch	cursor_w bulk collect
				into	tb_pop_alvo_sel_medic_w
				limit	qt_reg_commit_w;

				exit when tb_pop_alvo_sel_medic_w.count = 0;

				-- Incluir as pessoas na selecao
				forall i in tb_pop_alvo_sel_medic_w.first .. tb_pop_alvo_sel_medic_w.last
					insert into mprev_pop_alvo_selecao
						(nr_sequencia, nm_usuario, dt_atualizacao,
						nr_seq_populacao_alvo, nr_seq_pessoa_cubo, ie_selecionar,
						nm_tabela_regra_cubo, nr_seq_tabela_regra_cubo)
					values	(mprev_pop_alvo_selecao_seq.nextval, nm_usuario_p, sysdate,
						nr_seq_populacao_p, tb_pop_alvo_sel_medic_w(i),
						regra_medic_w.ie_incluir, 'HDM_REGRA_CUBO_MEDIC', 6);
				commit;
			end loop;
			close cursor_w;
		end loop;
		commit;
	end if;

	end selecionar_medic;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER restricoes MEDICAMENTO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_medic(dados_regra_p	mprev_pop_alvo_prest_pck.regra_medic)
				return varchar2 is

	ds_retorno_w		varchar2(4000)	:= null;
	ds_retorno_guia_w	varchar2(4000)	:= null;

	begin
	--Medicamento
	if	(dados_regra_p.cd_material is not null) then
		-- Aqui monta o select com as restricoes
		ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
				'and	medic.med_cd_material = :cd_material ';
		-- Aqui adiciona o valor da bild variable
		sql_pck.bind_variable(':cd_material', dados_regra_p.cd_material, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;
	
	--Unidade de medida
	if	(dados_regra_p.cd_unidade_medida is not null) then
		
		ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
					'and	medic.cd_unid_medida = :cd_unidade_medida ';
		
		sql_pck.bind_variable(':cd_unidade_medida', dados_regra_p.cd_unidade_medida, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;
	
	--Dose
	if	(dados_regra_p.QT_DOSE_INICIAL is not null) and
		(dados_regra_p.QT_DOSE_FINAL is not null) then
		
		ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
					'and	medic.qt_dose between :qt_dose_inicial and :qt_dose_final ';
		
		sql_pck.bind_variable(':qt_dose_inicial', dados_regra_p.qt_dose_inicial, mprev_pop_alvo_prest_pck.bind_sql_w);
		sql_pck.bind_variable(':qt_dose_final', dados_regra_p.qt_dose_final, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;	

	return	ds_retorno_w;

	end obter_restricao_medic;	
	
	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TRATAMENTO DE DATA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure inserir_data(	nr_seq_populacao_alvo_p		number,
				dt_referencia_p			date,
				nm_usuario_p			varchar2,
				nr_seq_pop_alvo_data_p	out	number) is

	nr_seq_retorno_w	mprev_pop_alvo_data.nr_sequencia%type;

	begin

	begin
	select	a.nr_sequencia
	into	nr_seq_retorno_w
	from	mprev_pop_alvo_data a
	where	a.nr_seq_populacao_alvo = nr_seq_populacao_alvo_p
	and	a.nr_mes	= to_number(PKG_DATE_UTILS.extract_field('MONTH',dt_referencia_p))
	and	a.nr_ano	= to_number(PKG_DATE_UTILS.extract_field('YEAR',dt_referencia_p));
	exception
		when others then
		nr_seq_retorno_w	:= null;
	end;

	/* Se nao achou data ja na populacao, insere */
	if	(nr_seq_retorno_w is null) then

		insert into mprev_pop_alvo_data
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nr_seq_populacao_alvo,
			nr_mes,
			nr_trimestre,
			nr_semestre,
			nr_ano,
			dt_mes)
		values	(mprev_pop_alvo_data_seq.nextval,
			nm_usuario_p,
			sysdate,
			nr_seq_populacao_alvo_p,
			to_number(PKG_DATE_UTILS.extract_field('MONTH',dt_referencia_p)),
			to_number(to_char(dt_referencia_p,'q')),
			round((to_char(dt_referencia_p,'q')/2)),
			to_number(PKG_DATE_UTILS.extract_field('YEAR',dt_referencia_p)),
			pkg_date_utils.start_of(dt_referencia_p,'MONTH', 0))
		returning nr_sequencia into nr_seq_retorno_w;
	end if;

	nr_seq_pop_alvo_data_p	:= nr_seq_retorno_w;

	end inserir_data;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GERAR PESSOAS NA POPULACAO ALVO<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure gravar_pessoas_populacao (	nr_seq_populacao_alvo_p		number,
						nm_usuario_p			varchar2,
						ds_select_comp_p		varchar2) is


	tb_pessoas_pop_alvo_w	mprev_pop_alvo_prest_pck.table_pessoas_pop_alvo;
	nr_seq_controle_w	mprev_pessoa_cubo_ops_w.nr_seq_controle%type;
	cursor_w		sql_pck.t_cursor;
	qt_reg_w		number(10);
	begin

	select	max(nr_seq_controle)
	into	nr_seq_controle_w
	from 	mprev_pessoa_cubo_ops_w
	where 	nm_usuario = nm_usuario_p
	and 	nr_seq_pop_alvo = nr_seq_populacao_alvo_p;
	
	sql_pck.bind_variable(':nr_seq_controle_pc', nr_seq_controle_w, mprev_pop_alvo_prest_pck.bind_sql_w);
	sql_pck.bind_variable(':nr_seq_populacao_alvo_pc', nr_seq_populacao_alvo_p, mprev_pop_alvo_prest_pck.bind_sql_w);
	
	cursor_w	:= sql_pck.executa_sql_cursor(ds_select_comp_p, mprev_pop_alvo_prest_pck.bind_sql_w);

	loop
	
	
	
		limpar_tipo_tabelas ('tb_pessoas_pop_alvo_w');
		
		fetch	cursor_w bulk collect
		into	tb_pessoas_pop_alvo_w.cd_pessoa_fisica, tb_pessoas_pop_alvo_w.qt_idade_anos, tb_pessoas_pop_alvo_w.ie_partic_programa,
			tb_pessoas_pop_alvo_w.ie_grupo_controle, tb_pessoas_pop_alvo_w.nm_pessoa, tb_pessoas_pop_alvo_w.ie_sexo
		limit	qt_reg_commit_w;

		exit when tb_pessoas_pop_alvo_w.cd_pessoa_fisica.count = 0;

		forall i in tb_pessoas_pop_alvo_w.cd_pessoa_fisica.first .. tb_pessoas_pop_alvo_w.cd_pessoa_fisica.last
				
				
			insert into mprev_pop_alvo_pessoa
				(nr_sequencia, dt_atualizacao, nm_usuario,
				dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_populacao_alvo,
				cd_pessoa_fisica, qt_idade_anos, ie_partic_programa,
				ie_grupo_controle, nm_pessoa, ie_selecionada,
				ie_sexo, ie_sel_temp, nm_pessoa_pesquisa, cd_municipio_ibge, sg_estado)
			values (mprev_pop_alvo_pessoa_seq.nextval, sysdate, nm_usuario_p,
				sysdate, nm_usuario_p, nr_seq_populacao_alvo_p, tb_pessoas_pop_alvo_w.cd_pessoa_fisica(i),
				tb_pessoas_pop_alvo_w.qt_idade_anos(i), tb_pessoas_pop_alvo_w.ie_partic_programa(i), tb_pessoas_pop_alvo_w.ie_grupo_controle(i),
				tb_pessoas_pop_alvo_w.nm_pessoa(i), 'N', tb_pessoas_pop_alvo_w.ie_sexo(i),
				'N', upper(elimina_acentuacao(tb_pessoas_pop_alvo_w.nm_pessoa(i))), obter_cod_ibge_compl_pf(tb_pessoas_pop_alvo_w.cd_pessoa_fisica(i), '1'),
        obter_sg_estado_compl_pf(tb_pessoas_pop_alvo_w.cd_pessoa_fisica(i), '1'));
		commit;
	end loop;
	close cursor_w;
	
	end gravar_pessoas_populacao;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>limpar tipo tabelas<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure limpar_tipo_tabelas(nm_tabela_p varchar2) is

	tb_benef_pop_alvo_w	mprev_pop_alvo_prest_pck.table_benef_pop_alvo;
	tb_pessoas_pop_alvo_w	mprev_pop_alvo_prest_pck.table_pessoas_pop_alvo;
	tb_pessoas_cubo_ops_w	mprev_pop_alvo_prest_pck.table_pessoas_cubo_ops;

	begin
		if	(nm_tabela_p = 'tb_pessoas_pop_alvo_w') then
			tb_pessoas_pop_alvo_w.cd_pessoa_fisica.delete;
			tb_pessoas_pop_alvo_w.qt_idade_anos.delete;
			tb_pessoas_pop_alvo_w.ie_partic_programa.delete;
		        tb_pessoas_pop_alvo_w.ie_grupo_controle.delete;
		        tb_pessoas_pop_alvo_w.nm_pessoa.delete;
			tb_pessoas_pop_alvo_w.ie_sexo.delete;
		elsif	(nm_tabela_p = 'tb_benef_pop_alvo_w') then
			tb_benef_pop_alvo_w.nr_seq_segurado.delete;
		        tb_benef_pop_alvo_w.cd_usuario_plano.delete;
		        tb_benef_pop_alvo_w.ie_preco_plano.delete;
		        tb_benef_pop_alvo_w.ds_preco_plano.delete;
		        tb_benef_pop_alvo_w.dt_inclusao_plano.delete;
		        tb_benef_pop_alvo_w.ie_situacao_contrato.delete;
		        tb_benef_pop_alvo_w.ie_situacao_atend.delete;
			tb_benef_pop_alvo_w.nr_seq_pessoa.delete;
		elsif	(nm_tabela_p = 'tb_pessoas_cubo_ops_w') then
			tb_pessoas_cubo_ops_w.cd_pessoa_fisica.delete;
			tb_pessoas_cubo_ops_w.dt_nascimento.delete;
			tb_pessoas_cubo_ops_w.nr_sequencia.delete;
		end if;

	end limpar_tipo_tabelas;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Gravar pessoas na tabela temporaria mprev_pessoa_cubo_ops_w<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure gravar_pessoas_cubo_ops_w(nr_seq_populacao_alvo_p	number,
					nm_usuario_p			varchar2,
					nr_seq_controle_p		number,
					ds_select_comp_p		varchar2) is

	cursor_w		sql_pck.t_cursor;
	tb_pessoas_cubo_ops_w	mprev_pop_alvo_prest_pck.table_pessoas_cubo_ops;

	begin
	
	cursor_w	:= sql_pck.executa_sql_cursor(ds_select_comp_p, mprev_pop_alvo_prest_pck.bind_sql_w);
	
	loop
		limpar_tipo_tabelas('tb_pessoas_cubo_ops_w');

		fetch	cursor_w bulk collect
		into	tb_pessoas_cubo_ops_w.cd_pessoa_fisica, tb_pessoas_cubo_ops_w.dt_nascimento,
			tb_pessoas_cubo_ops_w.nr_sequencia

		limit	qt_reg_commit_w;

		exit when tb_pessoas_cubo_ops_w.cd_pessoa_fisica.count = 0;

		forall i in tb_pessoas_cubo_ops_w.cd_pessoa_fisica.first .. tb_pessoas_cubo_ops_w.cd_pessoa_fisica.last
			
			insert into mprev_pessoa_cubo_ops_w
				(dt_atualizacao, nm_usuario, dt_nascimento,
				cd_pessoa_fisica, nr_seq_controle, nr_sequencia,
				nr_seq_pop_alvo)
			values (sysdate, nm_usuario_p, tb_pessoas_cubo_ops_w.dt_nascimento(i),
				tb_pessoas_cubo_ops_w.cd_pessoa_fisica(i), nr_seq_controle_p,
				tb_pessoas_cubo_ops_w.nr_sequencia(i), nr_seq_populacao_alvo_p);
		commit;
	end loop;
	close cursor_w;
	
	end gravar_pessoas_cubo_ops_w;

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>INSERIR COMPLEMENTO PESSOAS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	procedure selecionar_compl_pf(	nr_seq_regra_cubo_p	number,
					nr_seq_populacao_p 	number,
					nm_usuario_p		varchar2) is

	Cursor 	c_regra_compl
		(nr_seq_regra_cubo_pc	number) is
		select	a.cd_municipio_ibge,
            a.sg_estado,
		        a.ie_tipo_complemento,
			      a.ie_incluir
		from	hdm_regra_cubo_compl a
		where	a.nr_seq_regra_cubo	= nr_seq_regra_cubo_pc;

	tabela_regra_compl_pf_w		table_regra_compl_pf;
	nr_seq_benef_w			number(10);
	cursor_w			sql_pck.t_cursor;

	-- Tabela para inserir a sequencia das que retorna do select dinamico "ds_select_w".
	tb_pop_alvo_sel_compl_pf_w	pls_util_cta_pck.t_number_table;	

	begin

	update	mprev_populacao_alvo
	set	dt_inicio_filtro_pessoa	= sysdate
	where	nr_sequencia	= nr_seq_populacao_p;

	/* Montar select dinamico e cria pessoas conforme regra de Complemento (Endereco) */
	ds_select_w	:=	' select	pessoa.nr_sequencia ' || enter_w ||
				' from		mprev_pessoa_cubo_ops pessoa, ' || enter_w ||
				'		compl_pessoa_fisica compl ' || enter_w ||
				' where 	pessoa.cd_pessoa_fisica = compl.cd_pessoa_fisica ';

	if	(nr_seq_regra_cubo_p is not null)then
	
		for r_c_regra_compl in c_regra_compl(nr_seq_regra_cubo_p) loop
		
			tabela_regra_compl_pf_w.cd_municipio_ibge	:= r_c_regra_compl.cd_municipio_ibge;
      tabela_regra_compl_pf_w.sg_estado	:= r_c_regra_compl.sg_estado;
			tabela_regra_compl_pf_w.ie_tipo_complemento     := r_c_regra_compl.ie_tipo_complemento;
			tabela_regra_compl_pf_w.ie_incluir		:= r_c_regra_compl.ie_incluir;

			ds_restricao_w		:= obter_restricao_compl_pf(tabela_regra_compl_pf_w);

			ds_select_comp_w	:= ds_select_w || ds_restricao_w || ' group by pessoa.nr_sequencia ';

			cursor_w 		:= sql_pck.executa_sql_cursor(ds_select_comp_w, mprev_pop_alvo_prest_pck.bind_sql_w);

			loop

				tb_pop_alvo_sel_compl_pf_w.delete;

				fetch	cursor_w bulk collect
				into	tb_pop_alvo_sel_compl_pf_w
				limit	qt_reg_commit_w;

				exit when tb_pop_alvo_sel_compl_pf_w.count = 0;

				-- Incluir as pessoas na selecao
				forall i in tb_pop_alvo_sel_compl_pf_w.first .. tb_pop_alvo_sel_compl_pf_w.last
					insert into mprev_pop_alvo_selecao
						(nr_sequencia, nm_usuario, dt_atualizacao,
						nr_seq_populacao_alvo, nr_seq_pessoa_cubo, ie_selecionar,
						nm_tabela_regra_cubo, nr_seq_tabela_regra_cubo)
					values	(mprev_pop_alvo_selecao_seq.nextval, nm_usuario_p, sysdate,
						nr_seq_populacao_p, tb_pop_alvo_sel_compl_pf_w(i),
						tabela_regra_compl_pf_w.ie_incluir, 'HDM_REGRA_CUBO_COMPL', 7);
				commit;
			end loop;
			close cursor_w;
		end loop;
		commit;

	end if;

	update	mprev_populacao_alvo
	set	dt_fim_filtro_pessoa	= sysdate
	where	nr_sequencia	= nr_seq_populacao_p;

	end selecionar_compl_pf;


	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>OBTER RESTRICOES COMPLEMENTO PESSOA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
	function obter_restricao_compl_pf (dados_regra_p	mprev_pop_alvo_prest_pck.table_regra_compl_pf)
					return varchar2 is

	ds_retorno_w		varchar2(4000)	:= null;

	begin

	--Codigo municipio IBGE
	if	(dados_regra_p.cd_municipio_ibge is not null) then
		-- Aqui monta o select com as restricoes
		ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
				'and	compl.cd_municipio_ibge = :cd_municipio_ibge_pc';
		-- Aqui adiciona o valor da bild variable
		sql_pck.bind_variable(':cd_municipio_ibge_pc', dados_regra_p.cd_municipio_ibge, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

  --Estado
	if	(dados_regra_p.sg_estado is not null and dados_regra_p.cd_municipio_ibge is null) then
		-- Aqui monta o select com as restricoessanta catarina
		ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
				'and	compl.sg_estado =:sg_estado_pc';
		-- Aqui adiciona o valor da bild variable
		sql_pck.bind_variable(':sg_estado_pc', dados_regra_p.sg_estado, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	--Tipo de complemento do endereco do cadastro de enderecos da pessoa.
	if	(dados_regra_p.ie_tipo_complemento is not null) then
		-- Aqui monta o select com as restricoes
		ds_retorno_w :=	ds_retorno_w || mprev_pop_alvo_prest_pck.enter_w ||
				'and	compl.ie_tipo_complemento = :ie_tipo_complemento_pc';
		-- Aqui adiciona o valor da bild variable
		sql_pck.bind_variable(':ie_tipo_complemento_pc', dados_regra_p.ie_tipo_complemento, mprev_pop_alvo_prest_pck.bind_sql_w);
	end if;

	return	ds_retorno_w;

	end obter_restricao_compl_pf;
	
end mprev_pop_alvo_prest_pck;
/
