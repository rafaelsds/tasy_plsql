create or replace
function man_obter_dados_equip_cal_html(	nr_seq_equip_calib_p	number,
				ie_opcao_p		varchar2)
 		    	return date is

ds_retorno_w		date;
dt_primeira_calib_w		date;
dt_ultima_calib_w		date;
ie_frequencia_w		varchar2(15);
qt_dias_freq_w		number(05,0);

/*
1 - Primeira calibra��o
2 - Ultima calibra��o
3 - Pr�xima calibra��o
*/

begin

select	min(a.dt_calibracao),
	max(a.dt_calibracao),
	max(b.ie_frequencia),
	nvl(max(c.qt_dia),0)
into	dt_primeira_calib_w,
	dt_ultima_calib_w,
	ie_frequencia_w,
	qt_dias_freq_w
from	man_freq_calib c,
	man_equip_calibracao b,
	man_calibracao a
where	a.nr_seq_equip_calib 	= b.nr_sequencia
and	c.nr_sequencia 		= b.nr_seq_frequencia
and	b.nr_sequencia		= nr_seq_equip_calib_p;

if	(ie_opcao_p = '1') then
	ds_retorno_w		:= dt_primeira_calib_w;
elsif	(ie_opcao_p = '2') then
	ds_retorno_w		:= dt_ultima_calib_w;
elsif	(ie_opcao_p = '3') then
	ds_retorno_w		:= dt_ultima_calib_w + qt_dias_freq_w;
end if;

return	ds_retorno_w;

end man_obter_dados_equip_cal_html;
/