create or replace 
package cpoe_gas_order_json_pck  as
   	
	function get_gas_message(nr_cpoe_gas_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json;
	
	function get_message_clob(nr_cpoe_gas_p		number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob;
	
	procedure getCpoeIntegracaoGas(nr_seq_gas_cpoe_p number, nr_entity_identifier_p out number);

end cpoe_gas_order_json_pck;
/

create or replace 
package body cpoe_gas_order_json_pck as
		
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			varchar2) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;
	
	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			number) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;

	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			date) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,pkg_date_utils.get_isoformat(value_p));
	end if;	
	end add_json_value;
	
	function get_default_message(	nr_cpoe_gas_p	number ) return philips_json is
	
	json_encounter_w		philips_json;
	json_patient_w			philips_json;
	cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
	json_return_w			philips_json;
	json_allergy_w   		philips_json_list;
	
	nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
	
	begin
	
	json_return_w	:= philips_json();
	
	select max(nr_atendimento)
	into nr_atendimento_w
	from CPOE_GASOTERAPIA
	where nr_sequencia = nr_cpoe_gas_p;
	
	SELECT MAX(CD_PESSOA_FISICA)
	INTO CD_PESSOA_FISICA_W
	FROM ATENDIMENTO_PACIENTE
	WHERE NR_ATENDIMENTO = nr_atendimento_w;
	
	json_encounter_w		:= encounter_json_pck.get_encounter(nr_atendimento_w);
	json_patient_w			:= person_json_pck.get_person(cd_pessoa_fisica_w);
	json_allergy_w			:= person_json_pck.get_allergy(cd_pessoa_fisica_w);
	
	json_return_w.put('patientIdentification',json_patient_w.to_json_value);
	json_return_w.put('patientVisit',json_encounter_w.to_json_value);
	json_return_w.put('patientAllergy',json_allergy_w.to_json_value);
	
	return json_return_w;
	
	end get_default_message;	
	
	function get_gas_data(nr_cpoe_gas_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	Cursor C01 is
		select NR_SEQUENCIA,
        DT_LIBERACAO, 
        DT_LIB_SUSPENSAO, 
        DT_INICIO, 
        DT_FIM, 
        IE_DURACAO, 
        IE_URGENCIA, 
        Dt_atualizacao,
        cpoe_obter_dt_suspensao(nr_sequencia,'G') dt_suspensao,
        obter_conv_envio('INTERVALO_PRESCRICAO', 'CD_INTERVALO', CD_INTERVALO, 'E')  interval_code_value,
        a.cd_medico ordering_provider_id_number,
        obter_dados_pf(a.cd_medico,'PNG') ordering_provider_given_name,
        obter_dados_pf(a.cd_medico,'PNL') ordering_provider_last_name,
        obter_dados_pf(a.cd_medico,'PNM') ordering_provider_middle_name, 
        substr(obter_nome_pf(a.cd_medico),1,80) nm_medico_solicitante,
        Obter_Pessoa_Fisica_Usuario(a.nm_usuario_nrec,'C') ordering_user_id_number,
        Obter_Pessoa_Fisica_Usuario(a.nm_usuario_susp,'C') order_prov_susp_id_number,
        substr(OBTER_DESCRICAO_DOMINIO(1299, IE_RESPIRACAO),1,80) desc_respiration_type,
        obter_conv_envio('CPOE_GASOTERAPIA', 'IE_RESPIRACAO', IE_RESPIRACAO, 'E')  code_value,
        obter_conv_envio('CPOE_GASOTERAPIA', 'IE_RESPIRACAO', IE_RESPIRACAO, 'S')  code_system,
        decode(a.DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(a.DS_OBSERVACAO)) DS_OBSERVACAO, /* OBR fields */
        decode(a.DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(a.DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA, 
        Obter_Desc_Mod_Ventilatoria(CD_MODALIDADE_VENT) DS_MOD_VENTILATORIA,
        Obter_Desc_Gas(NR_SEQ_GAS) DS_GAS,
        QT_VC_PROG,
        QT_TI_TE,
        QT_INSPIRATORIO_PORC,
        QT_TEMPO_BAIXO,
        QT_TEMPO_ALTO,
        QT_SENSIB_MIN,
        QT_TEMPO_INSP,
        QT_SENSIB_RESP,
        QT_SATO2,
        QT_ABAIXO_PEEP,
        QT_PIP,
        QT_PH,
        QT_PEEP,
        QT_PCO2,
        QT_PS,
        QT_ACIMA_PEEP,
        QT_MAP,
        QT_IPAP,
        QT_FREQ_VENT_HZ,
        QT_FREQ_VENT,
        QT_FLUXO_INSP,
        QT_FRACAO_OXIGENIO,
        QT_FIO2,
        QT_EPAP,
        QT_CPAP,
        QT_BIC,
        QT_BE,
        QT_AMPLITUDE,
        QT_AMPLITUDE_PORCENT,
        QT_GASOTERAPIA, 
        IE_UNIDADE_MEDIDA,
        IE_ADMINISTRACAO
  from CPOE_GASOTERAPIA a
  where NR_SEQUENCIA = nr_cpoe_gas_p;
		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();

    add_json_value(json_item_w, 'nrSequencia', r_c01.nr_sequencia);
    add_json_value(json_item_w, 'dtLiberacao', r_c01.dt_liberacao);
    add_json_value(json_item_w, 'dtLibSuspensao', r_c01.dt_lib_suspensao);
    add_json_value(json_item_w, 'dtInicio', r_c01.dt_inicio);
    add_json_value(json_item_w, 'dtFim', r_c01.dt_fim);
    add_json_value(json_item_w, 'ieDuracao', r_c01.ie_duracao);
    add_json_value(json_item_w, 'ieUrgencia', r_c01.ie_urgencia);
    add_json_value(json_item_w, 'dtAtualizacao', r_c01.dt_atualizacao);
    add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);
    add_json_value(json_item_w, 'intervalCodeValue', r_c01.interval_code_value);
    add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ordering_provider_id_number);
    add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
    add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
    add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
    add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
    add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);
    add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
    add_json_value(json_item_w, 'descRespirationType', r_c01.desc_respiration_type);
    add_json_value(json_item_w, 'codeValue', r_c01.code_value);
    add_json_value(json_item_w, 'codeSystem', r_c01.code_system);
    add_json_value(json_item_w, 'dsObservacao', r_c01.ds_observacao);
    add_json_value(json_item_w, 'dsJustificativa', r_c01.ds_justificativa);
    add_json_value(json_item_w, 'dsModVentilatoria', r_c01.DS_MOD_VENTILATORIA);
    add_json_value(json_item_w, 'dsGas', r_c01.DS_GAS);
    add_json_value(json_item_w, 'qtVcProg', r_c01.qt_vc_prog);
    add_json_value(json_item_w, 'qtTiTe', r_c01.qt_ti_te);
    add_json_value(json_item_w, 'qtInspiratorioPorc', r_c01.qt_inspiratorio_porc);
    add_json_value(json_item_w, 'qtTempoBaixo', r_c01.qt_tempo_baixo);
    add_json_value(json_item_w, 'qtTempoAlto', r_c01.qt_tempo_alto);
    add_json_value(json_item_w, 'qtSensibMin', r_c01.qt_sensib_min);
    add_json_value(json_item_w, 'qtTempoInsp', r_c01.qt_tempo_insp);
    add_json_value(json_item_w, 'qtSensibResp', r_c01.qt_sensib_resp);
    add_json_value(json_item_w, 'qtSato2', r_c01.qt_sato2);
    add_json_value(json_item_w, 'qtAbaixoPeep', r_c01.qt_abaixo_peep);
    add_json_value(json_item_w, 'qtPip', r_c01.qt_pip);
    add_json_value(json_item_w, 'qtPh', r_c01.qt_ph);
    add_json_value(json_item_w, 'qtPeep', r_c01.qt_peep);
    add_json_value(json_item_w, 'qtPco2', r_c01.qt_pco2);
    add_json_value(json_item_w, 'qtPs', r_c01.qt_ps);
    add_json_value(json_item_w, 'qtAcimaPeep', r_c01.qt_acima_peep);
    add_json_value(json_item_w, 'qtMap', r_c01.qt_map);
    add_json_value(json_item_w, 'qtIpap', r_c01.qt_ipap);
    add_json_value(json_item_w, 'qtFreqVentHz', r_c01.qt_freq_vent_hz);
    add_json_value(json_item_w, 'qtFreqVent', r_c01.qt_freq_vent);
    add_json_value(json_item_w, 'qtFluxoInsp', r_c01.qt_fluxo_insp);
    add_json_value(json_item_w, 'qtFracaoOxigenio', r_c01.qt_fracao_oxigenio);
    add_json_value(json_item_w, 'qtFio2', r_c01.qt_fio2);
    add_json_value(json_item_w, 'qtEpap', r_c01.qt_epap);
    add_json_value(json_item_w, 'qtCpap', r_c01.qt_cpap);
    add_json_value(json_item_w, 'qtBic', r_c01.qt_bic);
    add_json_value(json_item_w, 'qtBe', r_c01.qt_be);
    add_json_value(json_item_w, 'qtAmplitude', r_c01.qt_amplitude);
    add_json_value(json_item_w, 'qtAmplitude porcent', r_c01.qt_amplitude);
    add_json_value(json_item_w, 'qtGasoterapia', r_c01.qt_gasoterapia);
    add_json_value(json_item_w, 'ieUnidadeMedida', r_c01.ie_unidade_medida);

		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.dt_fim));
		end if;
		
		if (r_c01.IE_ADMINISTRACAO in ('N', 'C')) then
      add_json_value(json_item_w, 'priority', 'PRN');
    elsif (r_c01.IE_URGENCIA = '0') then
      add_json_value(json_item_w, 'priority', 'S');
    elsif (r_c01.IE_URGENCIA = '5') then
      add_json_value(json_item_w, 'priority', 'TM5');
    elsif (r_c01.IE_URGENCIA = '10') then
      add_json_value(json_item_w, 'priority', 'TM10');
    elsif (r_c01.IE_URGENCIA = '15') then
      add_json_value(json_item_w, 'priority', 'TM15');
    else
      add_json_value(json_item_w, 'priority', 'R');
    end if;

		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.dt_lib_suspensao, r_c01.dt_liberacao));
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		add_json_value(json_item_w, 'testInstruction', r_c01.ds_justificativa || '~' || r_c01.ds_observacao);
		
		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_gas_data;

	
	function get_gas_message(nr_cpoe_gas_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json is
	json_return_w		philips_json;
	json_item_list_w   	philips_json_list;
	ie_hl7_msg_type_w	varchar(3);
	begin
	
  json_item_list_w	:= get_gas_data(nr_cpoe_gas_p);
  ie_hl7_msg_type_w := 'ORM';
	
	if	(json_item_list_w.count > 0) then
		json_return_w	:= philips_json();
		json_return_w		:= get_default_message(nr_cpoe_gas_p);
		add_json_value(json_return_w, 'orderControl', ie_order_control_p);
		add_json_value(json_return_w, 'entityidentifier', nr_entity_identifier_p);
		add_json_value(json_return_w, 'hl7MsgType', ie_hl7_msg_type_w);
		
		json_return_w.put('gasList', json_item_list_w.to_json_value());
	end if;
	
	return json_return_w;
	
	end get_gas_message;
					

	function get_message_clob(nr_cpoe_gas_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob is
	ds_json_out_w		clob;
	json_gas_w	philips_json;
	
	begin
		
	json_gas_w		:= get_gas_message(nr_cpoe_gas_p, ie_order_control_p, nr_entity_identifier_p);
	
	if	(json_gas_w is null) then
		return null;
	end if;
	
	dbms_lob.createtemporary( ds_json_out_w, true);
	json_gas_w.to_clob(ds_json_out_w);
	
	return ds_json_out_w;
	end get_message_clob;


	procedure getCpoeIntegracaoGas(nr_seq_gas_cpoe_p number, nr_entity_identifier_p out number) is
	begin

	select max(nr_sequencia)
	into nr_entity_identifier_p
	from cpoe_integracao
	where NR_SEQ_GASO_CPOE = nr_seq_gas_cpoe_p;
	
	if (nr_entity_identifier_p is null) then
		select CPOE_INTEGRACAO_SEQ.nextval
		into nr_entity_identifier_p
		from dual;

		insert into cpoe_integracao (
			nr_sequencia,
			NR_SEQ_GASO_CPOE
			)
		values (
			nr_entity_identifier_p,
			nr_seq_gas_cpoe_p
		);
		/* No commit so it can be used inside trigger */
	end if;	

	end getCpoeIntegracaoGas;	

end cpoe_gas_order_json_pck;
/
