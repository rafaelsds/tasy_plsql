create or replace package pls_decl_segurado_web_pck as

procedure inativar_acesso_web(		nr_seq_decl_segurado_web_p		pls_decl_segurado_web.nr_sequencia%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nm_usuario_p				Varchar2,
					ie_commit_p				Varchar2 default 'N');

function validar_hash_acesso_web	(ds_hash_p				pls_decl_segurado_web.ds_hash%type) return number;

function validar_cd_acesso_web(		nr_seq_declaracao_segurado_p		pls_declaracao_segurado.nr_sequencia%type,
					cd_codigo_acesso_p			varchar2,
					dt_nascimento_p				date) return varchar2;

procedure gerar_acesso_web(		nr_seq_declaracao_segurado_p		pls_declaracao_segurado.nr_sequencia%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nm_usuario_p				Varchar2);

procedure salvar_liberar_declaracao(	nr_seq_declaracao_segurado_p		pls_declaracao_segurado.nr_sequencia%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nm_usuario_p				Varchar2,
					qt_peso_p				number,
					qt_altura_cm_p				number,
					qt_imc_p				number,
					ds_ip_usuario_p				Varchar2);

end pls_decl_segurado_web_pck;
/

create or replace package body pls_decl_segurado_web_pck as

procedure inativar_acesso_web	(nr_seq_decl_segurado_web_p		pls_decl_segurado_web.nr_sequencia%type,
				cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
				nm_usuario_p				Varchar2,
				ie_commit_p				Varchar2 default 'N') is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Inativar o acesso web gerado.
-------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [x] Tasy (Delphi/Java/HTML5) [  ] Portal [  ]  Relatorios [ ] Outros:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

qt_decl_segurado_web_w		pls_integer;

begin

update	pls_decl_segurado_web
set	dt_validade_hash	= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_decl_segurado_web_p;

if (ie_commit_p = 'S') then
	commit;
end if;

end inativar_acesso_web;

procedure enviar_email_acesso_web	(nr_seq_declaracao_segurado_p		pls_declaracao_segurado.nr_sequencia%type,
					cd_pessoa_fisica_p			pessoa_fisica.cd_pessoa_fisica%type,
					ds_email_pf_p				compl_pessoa_fisica.ds_email%type,
					qt_dias_limite_hash_p			pls_integer,
					ds_hash_p				pls_decl_segurado_web.ds_hash%type,
					cd_codigo_acesso_p			varchar2,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nm_usuario_p				Varchar2) is

ds_link_w		pls_web_param_geral.ds_link_portal_ops%type;
ds_mensagem_w		varchar2(4000);
ds_assinatura_w		varchar2(500);
ds_email_remetente_w	pls_web_param_geral.ds_email_remetente%type;
ds_email_pj_w		pessoa_juridica_compl.ds_email%type;
ds_endereco_w		varchar2(500);
ds_local_w		varchar2(1000);
ie_assina_email_w	varchar2(1);
ie_tipo_compl_w		varchar2(255);
nm_fantasia_w		pls_outorgante.nm_fantasia%type;
nm_pf_w			varchar2(60);
nr_telefone_w		pessoa_juridica.nr_telefone%type;
ds_assunto_w		varchar2(50);

begin
--  Parametros - funcao '1246': OPSW - Portal web
ie_assina_email_w	:= substr(pls_obter_param_padrao_funcao(3,'1246'),1,1); -- Assinar e-mails
ie_tipo_compl_w		:= substr(pls_obter_param_padrao_funcao(17,'1246'),1,255); -- Complemento de pessoa juridica utilizado no portal Web

ds_link_w		:= pls_parametro_operadora_web('LPO', cd_estabelecimento_p);
ds_email_remetente_w	:= pls_parametro_operadora_web('EREM', cd_estabelecimento_p);

nm_pf_w			:= tws_get_name_person(cd_pessoa_fisica_p, cd_estabelecimento_p);

if	(ds_email_remetente_w is null or ds_email_remetente_w = '') then
	--  Parametro - funcao: MENU DO SISTEMA, Nome do USERID do servidor de E-mail.
	ds_email_remetente_w := pls_obter_param_padrao_funcao(38,'0');
end if;

if	(ie_assina_email_w = 'S') then
	ds_assinatura_w := substr(pls_obter_assinatura_email_web(3, 'N', cd_estabelecimento_p),1,500);
	
	if	(ds_assinatura_w is null or  ds_assinatura_w = '') then
		-- Caso estiver informado um complemento no parametro 17, sera feito a  busca no complemento pessoa juridica, caso contrario sera buscado em Pessoa Juridica
		if	(ie_tipo_compl_w is not null and ie_tipo_compl_w <> 'X') then
			begin
				select  b.nm_fantasia,
					c.ds_endereco ||', '||c.nr_endereco ||', '|| c.ds_complemento ||' - '|| c.ds_bairro ||' - '|| c.cd_cep ds_endereco,
					c.ds_municipio ||' - '|| c.sg_estado ds_local,
					c.nr_telefone,
					c.ds_email
				into	nm_fantasia_w,
					ds_endereco_w,
					ds_local_w,
					nr_telefone_w,
					ds_email_pj_w
				from	pessoa_juridica a, 
					pls_outorgante b, 
					pessoa_juridica_compl c 
				where	a.cd_cgc = b.cd_cgc_outorgante 
				and	a.cd_cgc = c.cd_cgc 
				and	b.cd_cgc_outorgante = c.cd_cgc 
				and	b.cd_estabelecimento = cd_estabelecimento_p 
				and	c.ie_tipo_complemento = ie_tipo_compl_w 
				and	c.ds_email is not null
				and	rownum = 1;
			exception
			when others then
				nm_fantasia_w	:= null;
				ds_endereco_w	:= null;
				ds_local_w	:= null;
				nr_telefone_w	:= null;
				ds_email_pj_w	:= null;
			end;
		end if;	
		-- Caso nao estiver informado nenhum registro no complemento, sera feito a busca pelo Pessoa Juridica
		if	(nm_fantasia_w is null) then
			begin
				select	b.nm_fantasia,
					a.ds_endereco       ||', '   ||
					a.nr_endereco       ||', '   ||
					a.ds_complemento    ||' - '  ||
					a.ds_bairro         ||' - '  ||
					a.cd_cep ds_endereco,
					a.ds_municipio      || ' - '   ||
					a.sg_estado ds_local,
					a.nr_telefone,
					nvl(pls_obter_dados_pj_compl(b.cd_estabelecimento, a.cd_cgc, 'M'), a.ds_email) ds_email 
				into	nm_fantasia_w,
					ds_endereco_w,
					ds_local_w,
					nr_telefone_w,
					ds_email_pj_w
				from	pessoa_juridica a,
					pls_outorgante b
				where	a.cd_cgc = b.cd_cgc_outorgante
				and	b.cd_estabelecimento = cd_estabelecimento_p
				and	rownum = 1;
			exception
			when others then
				nm_fantasia_w	:= null;
				ds_endereco_w	:= null;
				ds_local_w	:= null;
				nr_telefone_w	:= null;
				ds_email_pj_w	:= null;
			end;
		end if;
		
		-- Monta a assinatura de email
		ds_assinatura_w := nm_fantasia_w				|| chr(13) || chr(10);
		ds_assinatura_w := ds_assinatura_w ||ds_endereco_w		|| chr(13) || chr(10);
		ds_assinatura_w := ds_assinatura_w ||ds_local_w			|| chr(13) || chr(10);
		ds_assinatura_w := ds_assinatura_w ||'Fone: '|| nr_telefone_w	|| chr(13) || chr(10);
		ds_assinatura_w := ds_assinatura_w || ds_email_pj_w;
	end if;
	
end if;

select	(ds_link_w || (decode(substr(ds_link_w,length(ds_link_w),length(ds_link_w)-1),'/','','/')))
into	ds_link_w
from	dual;

ds_link_w := ds_link_w || 'pls_acessoDeclaracaoSaude.jsp?h=' || ds_hash_p;

ds_assunto_w	:= wheb_mensagem_pck.get_texto(1129102); --Acesso preenchimento Declaracao de Saude.

/*Monta a mensagem do e-mail. 
Ola #@NM_PF#@
Este e o seu acesso para preencher a declaracao de saude.
O link devera ser utilizado em ate #@QT_DIAS#@ dia(s).
Codigo de acesso
Codigo #@CD_CODIGO#@ 
Clique no link abaixo para acessar a sua declaracao de saude: 
<a href=" #@DS_LINK#@ " target="_blank"> Acesso a declaracao de saude</a> */

ds_mensagem_w	:= wheb_mensagem_pck.get_texto(1129109,'NM_PF=' || nm_pf_w || ';' || 'QT_DIAS=' || qt_dias_limite_hash_p || ';' || 'CD_CODIGO=' || cd_codigo_acesso_p || ';' || 'DS_LINK=' || ds_link_w);

-- Caso possuir assinatura de email, sera concatenado a mensagem com a assinatura
if	(ie_assina_email_w = 'S' and ds_assinatura_w is not null) then
	ds_mensagem_w := ds_mensagem_w || ds_assinatura_w;
end if;

enviar_email(ds_assunto_w, ds_mensagem_w, ds_email_remetente_w, ds_email_pf_p, nm_usuario_p, 'A');

end enviar_email_acesso_web;


function validar_hash_acesso_web	(ds_hash_p	pls_decl_segurado_web.ds_hash%type)
					return number is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Validar se o hash do link utilizado para acessar a declaracao esta valido e ativo.
Esta eh a primeira etapa de validacao do acesso.
-------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ ] Tasy (Delphi/Java/HTML5) [x] Portal [  ]  Relatorios [ ] Outros:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w		pls_declaracao_segurado.nr_sequencia%type;

begin

select	max(nr_seq_declaracao_segurado)
into	ds_retorno_w
from	pls_decl_segurado_web
where	ds_hash	= ds_hash_p
and	dt_utilizacao_hash is null
and	sysdate <= fim_dia(dt_utilizacao_hash);

return	ds_retorno_w;

end validar_hash_acesso_web;


function validar_cd_acesso_web	(nr_seq_declaracao_segurado_p		pls_declaracao_segurado.nr_sequencia%type,
				cd_codigo_acesso_p			varchar2,
				dt_nascimento_p				date)
				return varchar2 is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Validar se codigo de acesso eh valido.
Esta eh a segunda validacao do acesso.
Se o codigo for valido, retorna o cd_pessoa_fisica para o acesso web.
-------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ ] Tasy (Delphi/Java/HTML5) [x] Portal [  ]  Relatorios [ ] Outros:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_acesso_w		pls_integer;
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
ds_retorno_w		pessoa_fisica.cd_pessoa_fisica%type;
ds_hash_cd_acesso_w	pls_decl_segurado_web.ds_hash_cd_acesso%type;
				
begin

ds_hash_cd_acesso_w	:= OBTER_MD5(cd_codigo_acesso_p);

select	count(1)
into	qt_acesso_w
from	pls_decl_segurado_web
where	nr_seq_declaracao_segurado	= nr_seq_declaracao_segurado_p
and	ds_hash_cd_acesso		= ds_hash_cd_acesso_w
and	dt_utilizacao_hash is null
and	sysdate <= fim_dia(dt_utilizacao_hash);

if (qt_acesso_w > 0) then
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pls_declaracao_segurado
	where	nr_sequencia	= nr_seq_declaracao_segurado_p
	and	ie_status	= 'E';
	
	if (cd_pessoa_fisica_w is not null) then
		select	cd_pessoa_fisica
		into	ds_retorno_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	dt_nascimento		= trunc(dt_nascimento_p);
	end if;
end if;

return	ds_retorno_w;

end validar_cd_acesso_web;


procedure gerar_acesso_web	(nr_seq_declaracao_segurado_p		pls_declaracao_segurado.nr_sequencia%type,
				cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
				nm_usuario_p				Varchar2) is
		
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Gerar o link e codigo de acesso a declaracao de saude e enviar os dados ao e-mail da PF.
-------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [x] Tasy (Delphi/Java/HTML5) [  ] Portal [  ]  Relatorios [ ] Outros:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
		
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
ds_email_w			compl_pessoa_fisica.ds_email%type;
ds_hash_cd_acesso_w		pls_decl_segurado_web.ds_hash_cd_acesso%type;
ds_hash_w			pls_decl_segurado_web.ds_hash%type;
qt_dias_limite_hash_w		pls_integer;
cd_codigo_acesso_w		varchar2(6);

Cursor C01 is
	select	nr_sequencia nr_seq_decl_segurado_web
	from	pls_decl_segurado_web
	where	nr_seq_declaracao_segurado	= nr_seq_declaracao_segurado_p
	and	dt_utilizacao_hash	is null
	and	sysdate <= dt_validade_hash;

begin

for c01_w in C01 loop
	begin
	inativar_acesso_web(c01_w.nr_seq_decl_segurado_web, cd_estabelecimento_p, nm_usuario_p, 'N');
	end;
end loop;

begin
	select	nvl(cd_pessoa_responsavel, cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pls_declaracao_segurado
	where	nr_sequencia	= nr_seq_declaracao_segurado_p
	and	ie_status	in ('L', 'E');
exception
when others then
	cd_pessoa_fisica_w	:= null;
end;

if	(cd_pessoa_fisica_w is not null) then
	begin
		select	ds_email
		into	ds_email_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	ie_tipo_complemento	= 1;
	exception
	when others then
		ds_email_w	:= null;
	end;

	if 	(ds_email_w is not null) then
		qt_dias_limite_hash_w	:= Obter_Valor_Param_Usuario(1234, 6, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p);
		cd_codigo_acesso_w	:= dbms_random.string('X', 6);
		ds_hash_cd_acesso_w	:= OBTER_MD5(cd_codigo_acesso_w);
		ds_hash_w		:= OBTER_MD5((nr_seq_declaracao_segurado_p || sysdate));
		
		insert into pls_decl_segurado_web(	nr_sequencia, dt_atualizacao, nm_usuario, 
							dt_atualizacao_nrec, nm_usuario_nrec, cd_estabelecimento, 
							ds_hash, nr_seq_declaracao_segurado, ds_observacao,
							dt_validade_hash, dt_geracao_hash, ds_hash_cd_acesso)
					values	(	pls_decl_segurado_web_seq.nextval, sysdate, nm_usuario_p,
							sysdate, nm_usuario_p, cd_estabelecimento_p,
							ds_hash_w, nr_seq_declaracao_segurado_p, '',
							fim_dia(sysdate + qt_dias_limite_hash_w), sysdate, ds_hash_cd_acesso_w);
							
		enviar_email_acesso_web	(nr_seq_declaracao_segurado_p, cd_pessoa_fisica_w, ds_email_w,
					qt_dias_limite_hash_w, ds_hash_w, cd_codigo_acesso_w,
					cd_estabelecimento_p, nm_usuario_p);
					
		update	pls_declaracao_segurado
		set	ie_status	= 'E',
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_declaracao_segurado_p	
		and	ie_status	= 'L';
	else
		wheb_mensagem_pck.exibir_mensagem_abort(1129056); --Beneficiario sem e-mail cadastrado. Favor verificar.
	end if;
end if;
		
commit;

end gerar_acesso_web;

procedure salvar_liberar_declaracao(	nr_seq_declaracao_segurado_p		pls_declaracao_segurado.nr_sequencia%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nm_usuario_p				Varchar2,
					qt_peso_p				number,
					qt_altura_cm_p				number,
					qt_imc_p				number,
					ds_ip_usuario_p				Varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Salvar os dados de peso, altura e IMC da pf na declaracao, alterar a declaracao para Preenchida
e marcar o hash como utilizado.
-------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [x] Tasy (Delphi/Java/HTML5) [ x ] Portal [  ]  Relatorios [ ] Outros:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_declaracao_segurado_w	pls_declaracao_segurado.nr_sequencia%type;
nr_seq_termo_dec_saude_w	pls_termo_responsabilidade.nr_sequencia%type;
nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type;
ds_mensagem_w			varchar2(2000);
qt_registro_w			pls_integer;
dt_atual_w			date;

begin

select	max(nr_sequencia)
into	nr_seq_declaracao_segurado_w
from	pls_declaracao_segurado
where	nr_sequencia	= nr_seq_declaracao_segurado_p
and	ie_status	= 'E';

select	max(nr_seq_termo_dec_saude)
into	nr_seq_termo_dec_saude_w
from	pls_web_param_geral
where	cd_estabelecimento = cd_estabelecimento_p;

if (nr_seq_declaracao_segurado_w is not null) then
	update	pls_declaracao_segurado
	set	qt_peso		= qt_peso_p,
		qt_altura_cm	= qt_altura_cm_p,
		qt_imc		= qt_imc_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_declaracao_segurado_w;
	
	select 	count(*)
	into 	qt_registro_w
	from 	pls_declaracao_resposta
	where 	nr_seq_declaracao	= nr_seq_declaracao_segurado_w;
	
	if	(qt_registro_w = 0) then 
		wheb_mensagem_pck.exibir_mensagem_abort(261857,'');
		--O questionario da declaracao de saude nao foi preenchido. Favor verificar.		
	end if;
	
	if	(nr_seq_termo_dec_saude_w is not null) then

		select	substr(obter_nome_pf(cd_pessoa_fisica),1,255),
			sysdate
		into	nm_pessoa_fisica_w,
			dt_atual_w
		from	pls_declaracao_segurado
		where	nr_sequencia	= nr_seq_declaracao_segurado_w;

		/* 	Usuario: 
			Nome: 
			Data de finalizacao:
			IP: 
			Aceitou os termos para preenchimento da Declaracao de Saude.*/
			
		ds_mensagem_w := (wheb_mensagem_pck.get_texto(1136655,'NM_USUARIO=' || nm_usuario_p || ';' ||
					'NM_PESSOA_FISICA=' || nm_pessoa_fisica_w || ';' || 
					'DT_ATUAL=' || to_char(sysdate, 'dd/MM/yyyy hh24:mi:ss') || ';' || 'DS_IP=' || ds_ip_usuario_p));

		insert into pls_declaracao_complemento ( nr_sequencia, nm_usuario, nm_usuario_nrec,
							dt_atualizacao, dt_atualizacao_nrec, nr_seq_declaracao,
							dt_complemento, ie_tipo_complemento, ds_complemento,
							nr_seq_termo_responsabilidade)
						values(	pls_declaracao_complemento_seq.nextval, nm_usuario_p, nm_usuario_p,
							sysdate, sysdate, nr_seq_declaracao_segurado_w,
							sysdate, 'L', ds_mensagem_w,
							nr_seq_termo_dec_saude_w);
	end if;
		
	update	pls_declaracao_segurado
	set	ie_status	= 'P',
		ie_forma_preenchimento	= 'B',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_declaracao_segurado_w;
	
	update	pls_decl_segurado_web
	set	dt_utilizacao_hash	= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate	
	where	nr_seq_declaracao_segurado	= nr_seq_declaracao_segurado_w
	and	dt_utilizacao_hash is null
	and	sysdate <= fim_dia(dt_utilizacao_hash);	
end if;

commit;

end salvar_liberar_declaracao;

end pls_decl_segurado_web_pck;
/