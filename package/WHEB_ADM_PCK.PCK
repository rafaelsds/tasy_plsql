create or replace 
package WHEB_ADM_PCK as	
		
	function	get_cd_estabelecimento return number;
	function	get_mat_regra_codigo_barras return number;
	function	get_se_avisa_validade_barras return varchar2;
	function	get_se_lote_fornec_barras return varchar2;
	function	get_se_bloqueio_lote return varchar2;
	function	get_se_permite_lote_inativo return varchar2;
	function	get_qt_dias_aviso_validade return number;
	function	get_ie_consiste_estab_lote return varchar2;
	function	get_ie_estoque_negativo return varchar2;

	procedure 	set_cd_estabelecimento(cd_estabelecimento_p in number);
	procedure 	set_mat_regra_codigo_barras(cd_estabelecimento_p number);
	procedure	set_se_avisa_validade_barras(cd_estabelecimento_p number);
	procedure	set_se_lote_fornec_barras(cd_estabelecimento_p number);
	procedure	set_se_bloqueio_lote(cd_estabelecimento_p number);
	procedure	set_se_permite_lote_inativo(cd_estabelecimento_p number);
	procedure	set_qt_dias_aviso_validade(cd_estabelecimento_p in number);
	procedure	set_ie_consiste_estab_lote(cd_estabelecimento_p in number);
	procedure	set_ie_estoque_negativo(cd_estabelecimento_p in number);
	
end WHEB_ADM_PCK;
/

create or replace 
package body WHEB_ADM_PCK as

	type numset_t2 is table of number;
	
	/*
	
	SEMPRE QUE ADICIONAR UMA NOVA VARI�VEL  DEVE-SE FAZER A LIMPEZA DA MESMA NO M�TODO SET_CD_ESTABELECIMENTO
	
	*/
	
	cd_estabelecimento_w		number(4);
	nr_seq_mat_regra_barra_w	number(10);
	ie_avisa_validade_lote_w	varchar2(15);
	ie_barras_lote_fornec_w		varchar2(15);
	ie_bloqueio_lote_w			varchar2(15);
	ie_permite_lote_inativo_w	varchar2(15);
	qt_dias_aviso_validade_w	number(5);
	ie_consiste_estab_lote_w	varchar2(1);
	ie_estoque_negativo_w		varchar2(5);
	
	procedure set_cd_estabelecimento(cd_estabelecimento_p number) as
	begin
		/*
		ESTE TRATEMENTO � NECESS�RIO DEVIDO AO JAVA QUE COMPARTILHA A CONEX�O DO BANCO
		*/
		if	(cd_estabelecimento_w is not null) and
			(cd_estabelecimento_w <> cd_estabelecimento_p) then
			nr_seq_mat_regra_barra_w	:= null;
			ie_avisa_validade_lote_w	:= null;
			ie_barras_lote_fornec_w		:= null;
			ie_bloqueio_lote_w		:= null;
			ie_permite_lote_inativo_w	:= null;
			qt_dias_aviso_validade_w	:= null;
			ie_consiste_estab_lote_w	:= null;
		end if;
		cd_estabelecimento_w 	:= cd_estabelecimento_p;		
	end;
	
	function get_cd_estabelecimento return number as
	begin
		return cd_estabelecimento_w;
	end;

	procedure set_mat_regra_codigo_barras(cd_estabelecimento_p number) as
	begin
	select	nvl(max(nr_seq_mat_regra_barra),2)
	into	nr_seq_mat_regra_barra_w
	from	parametro_estoque
	where	cd_estabelecimento	= cd_estabelecimento_p;
	end;

	function get_mat_regra_codigo_barras return number as
	begin
	if	(nr_seq_mat_regra_barra_w is null) then
		set_mat_regra_codigo_barras(cd_estabelecimento_w);
	end if;

	return nr_seq_mat_regra_barra_w;
	end;

	procedure set_se_avisa_validade_barras(cd_estabelecimento_p number) as
	begin
	select	nvl(max(ie_avisa_validade_lote),'N')
	into	ie_avisa_validade_lote_w
	from	parametro_estoque
	where	cd_estabelecimento	= cd_estabelecimento_p;
	end;

	function get_se_avisa_validade_barras return varchar2 as
	begin
	if	(ie_avisa_validade_lote_w is null) then
		set_se_avisa_validade_barras(cd_estabelecimento_w);
	end if;

	return ie_avisa_validade_lote_w;
	end;

	procedure set_se_lote_fornec_barras(cd_estabelecimento_p number) as
	begin
	select	nvl(max(ie_barras_lote_fornec),'N')
	into	ie_barras_lote_fornec_w
	from	parametro_estoque
	where	cd_estabelecimento	= cd_estabelecimento_p;
	end;

	function get_se_lote_fornec_barras return varchar2 as
	begin
	if	(ie_barras_lote_fornec_w is null) then
		set_se_lote_fornec_barras(cd_estabelecimento_w);
	end if;

	return ie_barras_lote_fornec_w;
	end;

	procedure set_se_bloqueio_lote(cd_estabelecimento_p number) as
	begin
	select	nvl(max(ie_bloqueio_lote),'N')
	into	ie_bloqueio_lote_w
	from	parametro_estoque
	where	cd_estabelecimento	= cd_estabelecimento_p;
	end;

	function get_se_bloqueio_lote return varchar2 as
	begin
	if	(ie_bloqueio_lote_w is null) then
		set_se_bloqueio_lote(cd_estabelecimento_w);
	end if;

	return ie_bloqueio_lote_w;
	end;

	procedure set_se_permite_lote_inativo(cd_estabelecimento_p number) as
	begin
	select	nvl(max(ie_permite_lote_inativo),'S')
	into	ie_permite_lote_inativo_w
	from	parametro_estoque
	where	cd_estabelecimento	= cd_estabelecimento_p;
	end;
	
	procedure set_ie_estoque_negativo(cd_estabelecimento_p number) as
	begin
	if	(nvl(cd_estabelecimento_w,0) = 0) then
		set_cd_estabelecimento(wheb_usuario_pck.get_cd_estabelecimento);
	end if;
	
	select	nvl(max(ie_permite_estoque_negativo),'S')
	into	ie_estoque_negativo_w
	from	parametro_estoque
	where	cd_estabelecimento	= cd_estabelecimento_p;
	end;
	
	function get_ie_estoque_negativo return varchar2 as
	begin
	if	(ie_estoque_negativo_w is null) then
		set_ie_estoque_negativo(cd_estabelecimento_w);
	end if;

	return ie_estoque_negativo_w;
	end;

	function get_se_permite_lote_inativo return varchar2 as
	begin
	if	(ie_permite_lote_inativo_w is null) then
		set_se_permite_lote_inativo(cd_estabelecimento_w);
	end if;

	return ie_permite_lote_inativo_w;
	end;
	
	function get_qt_dias_aviso_validade return number as
	begin
	if	(qt_dias_aviso_validade_w is null) then
		set_qt_dias_aviso_validade(cd_estabelecimento_w);
	end if;

	return qt_dias_aviso_validade_w;
	end;
	
	procedure set_qt_dias_aviso_validade(cd_estabelecimento_p in number) as
	begin
	select	nvl(max(qt_dias_validade_lote),0)
	into	qt_dias_aviso_validade_w
	from	parametro_estoque
	where	cd_estabelecimento	= cd_estabelecimento_p;
	end;
	
	procedure set_ie_consiste_estab_lote(cd_estabelecimento_p in number) as
	begin
	select	nvl(max(ie_consiste_estab_lote),'N')
	into	ie_consiste_estab_lote_w
	from	parametro_estoque
	where	cd_estabelecimento = cd_estabelecimento_p;
	end;
	
	function get_ie_consiste_estab_lote return varchar2 as
	begin
	if	(ie_consiste_estab_lote_w is null) then
		set_ie_consiste_estab_lote(cd_estabelecimento_w);
	end if;

	return ie_consiste_estab_lote_w;
	end;
	
end WHEB_ADM_PCK;
/