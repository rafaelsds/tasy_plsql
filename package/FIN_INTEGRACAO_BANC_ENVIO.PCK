CREATE OR REPLACE PACKAGE fin_integracao_banc_envio is

	DS_PATH_C	        constant file_storage_path.ds_path%type		:= 'PAGTO_REM'; --'FINANCIAL_BANK_FILES'; --
	IE_DRIVER_C	        constant file_storage_path.ie_driver%type	:= 'local';

	procedure controle;
end fin_integracao_banc_envio;

/
create or replace package body fin_integracao_banc_envio as
    function obter_extensao_arquivo(filename varchar2) return varchar2 as
    begin
        if ( instr(filename,'.',-1) = 0 ) then
            return null;
        end if;

        return UPPER(substr(filename,instr(filename,'.',-1)+1,length(filename)-instr(filename,'.',-1)));
    end;
    
	/***************************************************************************/
	/* Retorna lista de arquvios do diretorio */
	function obter_diretorio_arquivos  return varchar2 is
		ds_path_w file_storage_path.ds_path%type;
	begin
		select	max(ds_path)
		into	ds_path_w
		from	file_storage_path
		where	ie_driver	= ie_driver_c
		and	    nm_storage	= ds_path_c;

		return(ds_path_w);
	end;

	/***************************************************************************/
	/* Retorna lista de arquvios do diretorio */
	procedure obter_lista_arquivos(	p_directory	in	varchar2) is
	begin
		get_dir_list(p_directory );
	end;

	/***************************************************************************/
	/* Procedimento principal */
	procedure controle is

		ds_path_w varchar2(255);

		/* Cursor na tabela W_DIR_LIST */
		cursor c_arquivos is
		select	replace(replace(nm_arquivo,ds_path_w,''),'/','') nm_arquivo ,
			dt_atualizacao
		from	w_dir_list;
       
        cursor c_registros is
        select nr_sequencia, 
               SUBSTR(ds_arquivo,INSTR(ds_arquivo,'\',-1)+1,LENGTH(ds_arquivo)) ds_arquivo,
               ds_status_arquivo
        from BANCO_ESCRITURAL
        where dt_importacao is null
        and   ds_arquivo is not null;
        
		r_arquivos c_arquivos%rowtype;        
        r_registros c_registros%rowtype;
        
		/*Variaveis de controle*/
		pre_arquivo_w		    varchar2(3);
        v_criar_directory  VARCHAR2(200);
        v_achou_arquivo number(1);

	begin
		/* Busca diretorio de integrac?o */
		ds_path_w := obter_diretorio_arquivos ;

         v_criar_directory := 'CREATE OR REPLACE DIRECTORY ' || DS_PATH_C || ' AS ''' || ds_path_w || ''' ';
         EXECUTE IMMEDIATE v_criar_directory;

		/* Gera lista de arquivos do diretorio e disponibiliza na tabela W_DIR_LIST  */
		obter_lista_arquivos(ds_path_w);
        
        open c_registros;
        loop
        fetch c_registros into r_registros;
            exit when c_registros%notfound;

            v_achou_arquivo := 0;
            open c_arquivos;
            loop
            fetch c_arquivos into r_arquivos;
                exit when c_arquivos%notfound;
                pre_arquivo_w := obter_extensao_arquivo(r_arquivos.nm_arquivo);

                if upper(pre_arquivo_w) = 'TXT' and  trim(upper(r_arquivos.nm_arquivo)) = trim(upper(r_registros.ds_arquivo)) then
                    v_achou_arquivo := 1;
                    If r_registros.ds_status_arquivo is null then
                        update BANCO_ESCRITURAL set ds_status_arquivo = obter_desc_expressao(1054974) where nr_sequencia = r_registros.nr_sequencia;
                    end if;
                    exit;
                end if;
            end loop;
            close c_arquivos;  
            If r_registros.ds_status_arquivo =  obter_desc_expressao(1054974) then
                update BANCO_ESCRITURAL 
                set ds_status_arquivo = obter_desc_expressao(1054978)
                where nr_sequencia = r_registros.nr_sequencia;
            end if;            

        end loop;
		close c_registros;
        
		commit;
	end;

end fin_integracao_banc_envio;
/