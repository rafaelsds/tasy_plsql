CREATE OR REPLACE PACKAGE PKG_TRIMESTRE AS

TYPE TRIMESTRE_TP IS RECORD
(
 pr VARCHAR(7),
 seg  VARCHAR(7),
 ter  VARCHAR(7),
 qua  VARCHAR(7)
);

TYPE TRIMESTRE_TABLE_TP IS TABLE OF TRIMESTRE_TP;

FUNCTION BUSCAR_TRIMESTRES_PIPELINED(
    trimestre_selecionado NUMBER,
    ano NUMBER
) RETURN TRIMESTRE_TABLE_TP PIPELINED;


END PKG_TRIMESTRE;
/
CREATE OR REPLACE PACKAGE BODY PKG_TRIMESTRE AS

FUNCTION BUSCAR_TRIMESTRES_PIPELINED(
    trimestre_selecionado NUMBER,
    ano NUMBER
) RETURN TRIMESTRE_TABLE_TP PIPELINED IS
    
  CURSOR C_TRIMESTRE IS
    select 'Q' || CASE WHEN  - 3 > 1 THEN trimestre_selecionado ELSE CASE WHEN trimestre_selecionado > 3 THEN trimestre_selecionado - 3 ELSE trimestre_selecionado + 1 END END || ' ' || CASE WHEN trimestre_selecionado - 3 > 0 THEN ano ELSE ano - 1 END as pr,
            'Q' || CASE WHEN trimestre_selecionado - 3 > -1 THEN CASE WHEN trimestre_selecionado < 3 THEN trimestre_selecionado + 2 ELSE trimestre_selecionado - 2 END ELSE trimestre_selecionado + 2 END || ' ' || CASE WHEN trimestre_selecionado - 3 >= 0 THEN ano ELSE ano - 1 END as seg,
            'Q' || CASE WHEN trimestre_selecionado - 3 > -2 THEN trimestre_selecionado - 1 ELSE trimestre_selecionado + 3 END || ' ' || CASE WHEN trimestre_selecionado - 3 > 0 THEN ano ELSE CASE WHEN (trimestre_selecionado + 3) > 4 THEN ano ELSE ano - 1 END END as ter,
            'Q' || trimestre_selecionado || ' ' || ano as qua
       from dual;
  
TYPE T_TRIMESTRE_TABLE_TP IS TABLE OF C_TRIMESTRE%ROWTYPE;
R_TRIMESTRE T_TRIMESTRE_TABLE_TP;
  
BEGIN 
  OPEN C_TRIMESTRE;
  LOOP
    FETCH C_TRIMESTRE BULK COLLECT INTO R_TRIMESTRE LIMIT 1000;
      
      BEGIN
        FOR i IN R_TRIMESTRE.FIRST..R_TRIMESTRE.LAST LOOP
          PIPE ROW(R_TRIMESTRE(i));  
        END LOOP;
      EXCEPTION WHEN OTHERS THEN
        NULL;
      END;  

    EXIT WHEN C_TRIMESTRE%NOTFOUND; 
 
  END LOOP;
  CLOSE C_TRIMESTRE;                                  
  
  RETURN;
END BUSCAR_TRIMESTRES_PIPELINED;


END PKG_TRIMESTRE;
/
