create or replace 
package pls_mens_atualizacao_pck as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Atualizar os valores das mensalidades e do lote
------------------------------------------------------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

procedure atualizar_valores_lote(	nr_seq_lote_p		in pls_lote_mensalidade.nr_sequencia%type,
					nm_usuario_p		in usuario.nm_usuario%type,
					cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
					ie_tipo_atualizacao_p	in varchar2 default null);

end pls_mens_atualizacao_pck;
/

create or replace
package body pls_mens_atualizacao_pck as

qt_pagadores_total_lote_w	pls_integer := 0;
qt_registro_del_mens_w		pls_integer := 2000;

procedure deletar_mensalidade(	tb_delete_mensalidade_p	in out pls_util_cta_pck.t_number_table) is
begin

if	(tb_delete_mensalidade_p.count > 0) then
	forall i in tb_delete_mensalidade_p.first..tb_delete_mensalidade_p.last
		delete 	from pls_mensalidade_log
		where	nr_seq_mensalidade = tb_delete_mensalidade_p(i);
	commit;
	
	forall i in tb_delete_mensalidade_p.first..tb_delete_mensalidade_p.last
		delete	from pls_mensalidade
		where	nr_sequencia = tb_delete_mensalidade_p(i);
	commit;
end if;

tb_delete_mensalidade_p.delete;
end deletar_mensalidade;

procedure deletar_mensalidade_seg(	tb_delete_mensalidade_seg_p	in out pls_util_cta_pck.t_number_table) is
begin

if	(tb_delete_mensalidade_seg_p.count > 0) then
	forall i in tb_delete_mensalidade_seg_p.first..tb_delete_mensalidade_seg_p.last
		delete	from pls_mensalidade_segurado
		where	nr_sequencia = tb_delete_mensalidade_seg_p(i);
	commit;
end if;

tb_delete_mensalidade_seg_p.delete;
end deletar_mensalidade_seg;

procedure update_mensalidade (	tb_nr_seq_mensalidade_p		in out pls_util_cta_pck.t_number_table,
				tb_vl_mensalidade_p		in out pls_util_cta_pck.t_number_table,
				tb_vl_coparticipacao_p		in out pls_util_cta_pck.t_number_table,
				tb_vl_adicionais_p		in out pls_util_cta_pck.t_number_table,
				tb_vl_outros_p			in out pls_util_cta_pck.t_number_table,
				tb_vl_pos_estabelecido_p	in out pls_util_cta_pck.t_number_table,
				tb_vl_pre_estabelecido_p	in out pls_util_cta_pck.t_number_table,
				tb_vl_pro_rata_dia_p		in out pls_util_cta_pck.t_number_table,
				tb_vl_antecipacao_p		in out pls_util_cta_pck.t_number_table,
				tb_qt_beneficiarios_p		in out pls_util_cta_pck.t_number_table,
				tb_vl_taxa_boleto_p		in out pls_util_cta_pck.t_number_table,
				tb_vl_taxa_boleto_emb_p		in out pls_util_cta_pck.t_number_table,
				tb_vl_outros_pagador_p		in out pls_util_cta_pck.t_number_table) is
begin

if	(tb_nr_seq_mensalidade_p.count > 0) then
	forall i in tb_nr_seq_mensalidade_p.first..tb_nr_seq_mensalidade_p.last
		update	pls_mensalidade
		set	qt_beneficiarios	= tb_qt_beneficiarios_p(i),
			vl_mensalidade		= tb_vl_mensalidade_p(i) + tb_vl_outros_pagador_p(i) + tb_vl_taxa_boleto_p(i),
			vl_coparticipacao	= tb_vl_coparticipacao_p(i),
			vl_adicionais		= tb_vl_adicionais_p(i),
			vl_outros		= tb_vl_outros_p(i) + tb_vl_outros_pagador_p(i),
			vl_pos_estabelecido	= tb_vl_pos_estabelecido_p(i),
			vl_pre_estabelecido	= tb_vl_pre_estabelecido_p(i),
			vl_pro_rata_dia		= tb_vl_pro_rata_dia_p(i),
			vl_antecipacao		= tb_vl_antecipacao_p(i),
			vl_taxa_boleto		= tb_vl_taxa_boleto_p(i) + tb_vl_taxa_boleto_emb_p(i)
		where	nr_sequencia		= tb_nr_seq_mensalidade_p(i);
	commit;
end if;

tb_nr_seq_mensalidade_p.delete;
tb_vl_mensalidade_p.delete;
tb_vl_coparticipacao_p.delete;
tb_vl_adicionais_p.delete;
tb_vl_outros_p.delete;
tb_vl_pos_estabelecido_p.delete;
tb_vl_pre_estabelecido_p.delete;
tb_vl_pro_rata_dia_p.delete;
tb_vl_antecipacao_p.delete;
tb_qt_beneficiarios_p.delete;
tb_vl_taxa_boleto_p.delete;
tb_vl_taxa_boleto_emb_p.delete;
tb_vl_outros_pagador_p.delete;

end update_mensalidade;

procedure update_mensalidade_seg(	tb_nr_seq_mensalidade_seg_p	in out pls_util_cta_pck.t_number_table,
					tb_vl_mensalidade_p		in out pls_util_cta_pck.t_number_table,
					tb_vl_pre_estabelecido_p	in out pls_util_cta_pck.t_number_table,
					tb_vl_pro_rata_dia_p		in out pls_util_cta_pck.t_number_table,
					tb_vl_antecipacao_p		in out pls_util_cta_pck.t_number_table,
					tb_vl_coparticipacao_p		in out pls_util_cta_pck.t_number_table,
					tb_vl_adicional_p		in out pls_util_cta_pck.t_number_table,
					tb_vl_pos_estabelecido_p	in out pls_util_cta_pck.t_number_table) is
begin

if	(tb_nr_seq_mensalidade_seg_p.count > 0) then
	forall i in tb_nr_seq_mensalidade_seg_p.first..tb_nr_seq_mensalidade_seg_p.last
		update	pls_mensalidade_segurado
		set	vl_mensalidade		= tb_vl_mensalidade_p(i),
			vl_pre_estabelecido	= tb_vl_pre_estabelecido_p(i),
			vl_pro_rata_dia		= tb_vl_pro_rata_dia_p(i),
			vl_antecipacao		= tb_vl_antecipacao_p(i),
			vl_coparticipacao	= tb_vl_coparticipacao_p(i),
			vl_adicionais		= tb_vl_adicional_p(i),
			vl_pos_estabelecido	= tb_vl_pos_estabelecido_p(i),
			vl_outros		= tb_vl_mensalidade_p(i) - tb_vl_pre_estabelecido_p(i) - tb_vl_coparticipacao_p(i) - tb_vl_pos_estabelecido_p(i)
		where	nr_sequencia		= tb_nr_seq_mensalidade_seg_p(i);
	commit;
end if;

tb_nr_seq_mensalidade_seg_p.delete;
tb_vl_mensalidade_p.delete;
tb_vl_pre_estabelecido_p.delete;
tb_vl_pro_rata_dia_p.delete;
tb_vl_antecipacao_p.delete;
tb_vl_coparticipacao_p.delete;
tb_vl_adicional_p.delete;
tb_vl_pos_estabelecido_p.delete;

end update_mensalidade_seg;

procedure atualizar_mensalidade_segurado is
qt_itens_w			pls_integer;

--Variaveis para controlar o delete
nr_indice_del_w			pls_integer;
tb_delete_mensalidade_seg_w	pls_util_cta_pck.t_number_table;

--Variaveis para controlar o update
nr_indice_w			pls_integer;
tb_nr_seq_mensalidade_seg_w	pls_util_cta_pck.t_number_table;
tb_vl_mensalidade_w		pls_util_cta_pck.t_number_table;
tb_vl_pre_estabelecido_w	pls_util_cta_pck.t_number_table;
tb_vl_pro_rata_dia_w		pls_util_cta_pck.t_number_table;
tb_vl_antecipacao_w		pls_util_cta_pck.t_number_table;
tb_vl_coparticipacao_w		pls_util_cta_pck.t_number_table;
tb_vl_adicional_w		pls_util_cta_pck.t_number_table;
tb_vl_pos_estabelecido_w	pls_util_cta_pck.t_number_table;

Cursor C01 is
	select	nr_seq_mensalidade_seg
	from	pls_mensalidade_seg_tmp;

begin

nr_indice_w	:= 0;
nr_indice_del_w	:= 0;
tb_delete_mensalidade_seg_w.delete;
tb_nr_seq_mensalidade_seg_w.delete;
update_mensalidade_seg(	tb_nr_seq_mensalidade_seg_w,
			tb_vl_mensalidade_w,
			tb_vl_pre_estabelecido_w,
			tb_vl_pro_rata_dia_w,
			tb_vl_antecipacao_w,
			tb_vl_coparticipacao_w,
			tb_vl_adicional_w,
			tb_vl_pos_estabelecido_w);

for r_c01_w in C01 loop
	begin
	
	select	count(1)
	into	qt_itens_w
	from	pls_mensalidade_seg_item
	where	nr_seq_mensalidade_seg	= r_c01_w.nr_seq_mensalidade_seg;
	
	if	(qt_itens_w > 0) then
		tb_nr_seq_mensalidade_seg_w(nr_indice_w)	:= r_c01_w.nr_seq_mensalidade_seg;
		
		select	sum(vl_mensalidade),
			sum(vl_pro_rata_dia),
			sum(vl_antecipacao),
			sum(vl_adicional),
			sum(vl_pre_estabelecido),
			sum(vl_coparticipacao),
			sum(vl_pos_estabelecido)
		into	tb_vl_mensalidade_w(nr_indice_w),
			tb_vl_pro_rata_dia_w(nr_indice_w),
			tb_vl_antecipacao_w(nr_indice_w),
			tb_vl_adicional_w(nr_indice_w),
			tb_vl_pre_estabelecido_w(nr_indice_w),
			tb_vl_coparticipacao_w(nr_indice_w),
			tb_vl_pos_estabelecido_w(nr_indice_w)
		from(
			select	decode(ie_tipo_mensalidade,'P',0,sum(vl_item)) vl_mensalidade,
				decode(ie_tipo_mensalidade,'P',0,sum(vl_pro_rata_dia)) vl_pro_rata_dia,
				decode(ie_tipo_mensalidade,'P',0,sum(vl_antecipacao)) vl_antecipacao,
				decode(ie_tipo_mensalidade,'A',sum(vl_item),0) vl_adicional,
				decode(ie_tipo_item,'1',sum(vl_item),decode(ie_tipo_item,'5',sum(vl_item),decode(ie_tipo_item,'25',sum(vl_item),decode(ie_tipo_item,'12',sum(vl_item),decode(ie_tipo_item, '41',sum(vl_item),decode(ie_tipo_item,'31',sum(vl_item),0)))))) vl_pre_estabelecido,
				decode(ie_tipo_item,'3',sum(vl_item),0) vl_coparticipacao,
				decode(ie_tipo_item,'6',sum(vl_item),decode(ie_tipo_item,'7',sum(vl_item),decode(ie_tipo_item,'8',sum(vl_item),0))) vl_pos_estabelecido
			from	pls_mensalidade_seg_item
			where	nr_seq_mensalidade_seg	= r_c01_w.nr_seq_mensalidade_seg
			group by ie_tipo_mensalidade, ie_tipo_item);
		
		if	(tb_vl_pre_estabelecido_w(nr_indice_w) is null) then
			tb_vl_pre_estabelecido_w(nr_indice_w) := 0;
		end if;
		if	(tb_vl_pro_rata_dia_w(nr_indice_w) is null) then
			tb_vl_pro_rata_dia_w(nr_indice_w) := 0;
		end if;
		if	(tb_vl_antecipacao_w(nr_indice_w) is null) then
			tb_vl_antecipacao_w(nr_indice_w) := 0;
		end if;
		if	(tb_vl_coparticipacao_w(nr_indice_w) is null) then
			tb_vl_coparticipacao_w(nr_indice_w) := 0;
		end if;
		if	(tb_vl_adicional_w(nr_indice_w) is null) then
			tb_vl_adicional_w(nr_indice_w) := 0;
		end if;
		if	(tb_vl_pos_estabelecido_w(nr_indice_w) is null) then
			tb_vl_pos_estabelecido_w(nr_indice_w) := 0;
		end if;
		
		if	(nr_indice_w >= pls_util_pck.qt_registro_transacao_w) then
			update_mensalidade_seg(	tb_nr_seq_mensalidade_seg_w,
						tb_vl_mensalidade_w,
						tb_vl_pre_estabelecido_w,
						tb_vl_pro_rata_dia_w,
						tb_vl_antecipacao_w,
						tb_vl_coparticipacao_w,
						tb_vl_adicional_w,
						tb_vl_pos_estabelecido_w);
		else
			nr_indice_w	:= nr_indice_w + 1;
		end if;
	else
		tb_delete_mensalidade_seg_w(nr_indice_del_w) := r_c01_w.nr_seq_mensalidade_seg;
		
		if	(nr_indice_del_w >= qt_registro_del_mens_w) then
			deletar_mensalidade_seg(tb_delete_mensalidade_seg_w);
			nr_indice_del_w	:= 0;
		else
			nr_indice_del_w	:= nr_indice_del_w + 1;
		end if;
	end if;
	end;
end loop;

-- se sobrou alguma coisa manda para o banco
deletar_mensalidade_seg(tb_delete_mensalidade_seg_w);
update_mensalidade_seg(	tb_nr_seq_mensalidade_seg_w,
			tb_vl_mensalidade_w,
			tb_vl_pre_estabelecido_w,
			tb_vl_pro_rata_dia_w,
			tb_vl_antecipacao_w,
			tb_vl_coparticipacao_w,
			tb_vl_adicional_w,
			tb_vl_pos_estabelecido_w);

end atualizar_mensalidade_segurado;

procedure atualizar_mensalidade is
vl_taxa_boleto_seg_w		number(15,2);
vl_pro_rata_pag_w		pls_mensalidade_seg_item.vl_pro_rata_dia%type;
vl_antec_pag_w			pls_mensalidade_seg_item.vl_antecipacao%type;

--Variaveis para controlar o delete
nr_indice_del_w			pls_integer;
tb_delete_mensalidade_w		pls_util_cta_pck.t_number_table;

--Variaveis para controlar o update
nr_indice_w			pls_integer;
tb_nr_seq_mensalidade_w		pls_util_cta_pck.t_number_table;
tb_vl_mensalidade_w		pls_util_cta_pck.t_number_table;
tb_vl_coparticipacao_w		pls_util_cta_pck.t_number_table;
tb_vl_adicionais_w		pls_util_cta_pck.t_number_table;
tb_vl_outros_w			pls_util_cta_pck.t_number_table;
tb_vl_pos_estabelecido_w	pls_util_cta_pck.t_number_table;
tb_vl_pre_estabelecido_w	pls_util_cta_pck.t_number_table;
tb_vl_pro_rata_dia_w		pls_util_cta_pck.t_number_table;
tb_vl_antecipacao_w		pls_util_cta_pck.t_number_table;
tb_qt_beneficiarios_w		pls_util_cta_pck.t_number_table;
tb_vl_taxa_boleto_w		pls_util_cta_pck.t_number_table;
tb_vl_taxa_boleto_emb_w		pls_util_cta_pck.t_number_table;
tb_vl_outros_pagador_w		pls_util_cta_pck.t_number_table;

Cursor C01 is
	select	a.nr_seq_mensalidade,
		(	select	count(distinct x.nr_seq_segurado)
			from	pls_mensalidade_segurado x
			where	x.nr_seq_mensalidade = a.nr_seq_mensalidade) qt_benef
	from	pls_mensalidade_tmp a;

begin
qt_pagadores_total_lote_w	:= 0;
nr_indice_w := 0;
nr_indice_del_w := 0;
tb_nr_seq_mensalidade_w.delete;
tb_delete_mensalidade_w.delete;
update_mensalidade(	tb_nr_seq_mensalidade_w,
			tb_vl_mensalidade_w,
			tb_vl_coparticipacao_w,
			tb_vl_adicionais_w,
			tb_vl_outros_w,
			tb_vl_pos_estabelecido_w,
			tb_vl_pre_estabelecido_w,
			tb_vl_pro_rata_dia_w,
			tb_vl_antecipacao_w,
			tb_qt_beneficiarios_w,
			tb_vl_taxa_boleto_w,
			tb_vl_taxa_boleto_emb_w,
			tb_vl_outros_pagador_w);

for r_c01_w in C01 loop
	begin
	
	if	(r_c01_w.qt_benef > 0) then
		tb_nr_seq_mensalidade_w(nr_indice_w) := r_c01_w.nr_seq_mensalidade;
		
		tb_qt_beneficiarios_w(nr_indice_w) := r_c01_w.qt_benef;
		
		qt_pagadores_total_lote_w := qt_pagadores_total_lote_w + 1;
		
		select	sum(vl_adicionais),
			sum(vl_coparticipacao),
			sum(vl_mensalidade),
			sum(vl_outros),
			sum(vl_pos_estabelecido),
			sum(vl_pre_estabelecido),
			sum(vl_pro_rata_dia),
			sum(vl_antecipacao)
		into	tb_vl_adicionais_w(nr_indice_w),
			tb_vl_coparticipacao_w(nr_indice_w),
			tb_vl_mensalidade_w(nr_indice_w),
			tb_vl_outros_w(nr_indice_w),
			tb_vl_pos_estabelecido_w(nr_indice_w),
			tb_vl_pre_estabelecido_w(nr_indice_w),
			tb_vl_pro_rata_dia_w(nr_indice_w),
			tb_vl_antecipacao_w(nr_indice_w)
		from	pls_mensalidade_segurado
		where	nr_seq_mensalidade = r_c01_w.nr_seq_mensalidade;
		
		select	sum(vl_taxa_boleto_destacado),
			sum(vl_taxa_boleto_emb),
			sum(vl_pagador_outros),
			sum(vl_pro_rata_pag),
			sum(vl_antec_pag)
		into	tb_vl_taxa_boleto_w(nr_indice_w),
			tb_vl_taxa_boleto_emb_w(nr_indice_w), --como a taxa de boleto embutida nao e considerada na coluna "Vl outros", e necessario armazenar o valor para somar no valor da mensalidade
			tb_vl_outros_pagador_w(nr_indice_w),
			vl_pro_rata_pag_w,
			vl_antec_pag_w
		from	(select	nvl(sum(a.vl_taxa_boleto),0) vl_taxa_boleto_emb,
				decode(a.ie_tipo_item,'17',sum(a.vl_item),0) vl_taxa_boleto_destacado,
				decode(a.ie_tipo_mensalidade, 'P', decode(a.ie_tipo_item,'17',0,sum(a.vl_item))) vl_pagador_outros,
				decode(a.ie_tipo_mensalidade, 'P', sum(a.vl_pro_rata_dia)) vl_pro_rata_pag,
				decode(a.ie_tipo_mensalidade, 'P', sum(a.vl_antecipacao)) vl_antec_pag
			from	pls_mensalidade_seg_item a,
				pls_mensalidade_segurado b
			where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
			and	b.nr_seq_mensalidade	= r_c01_w.nr_seq_mensalidade
			and	a.ie_tipo_item not in ('42','43')
			group by a.ie_tipo_mensalidade, a.ie_tipo_item);
		
		tb_vl_pro_rata_dia_w(nr_indice_w)	:= tb_vl_pro_rata_dia_w(nr_indice_w) + nvl(vl_pro_rata_pag_w,0);
		tb_vl_antecipacao_w(nr_indice_w)	:= tb_vl_antecipacao_w(nr_indice_w) + nvl(vl_antec_pag_w,0);
		
		if	(tb_vl_taxa_boleto_w(nr_indice_w) is null) then
			tb_vl_taxa_boleto_w(nr_indice_w) := 0;
		end if;
		
		if	(tb_vl_outros_pagador_w(nr_indice_w) is null) then
			tb_vl_outros_pagador_w(nr_indice_w) := 0;
		end if;
		
		if	(nr_indice_w >= pls_util_pck.qt_registro_transacao_w) then
			update_mensalidade(	tb_nr_seq_mensalidade_w,
						tb_vl_mensalidade_w,
						tb_vl_coparticipacao_w,
						tb_vl_adicionais_w,
						tb_vl_outros_w,
						tb_vl_pos_estabelecido_w,
						tb_vl_pre_estabelecido_w,
						tb_vl_pro_rata_dia_w,
						tb_vl_antecipacao_w,
						tb_qt_beneficiarios_w,
						tb_vl_taxa_boleto_w,
						tb_vl_taxa_boleto_emb_w,
						tb_vl_outros_pagador_w);
		else
			nr_indice_w	:= nr_indice_w + 1;
		end if;
	else
		tb_delete_mensalidade_w(nr_indice_del_w) := r_c01_w.nr_seq_mensalidade;
		
		if	(nr_indice_del_w >= qt_registro_del_mens_w) then
			deletar_mensalidade(tb_delete_mensalidade_w);
			nr_indice_del_w	:= 0;
		else
			nr_indice_del_w	:= nr_indice_del_w + 1;
		end if;
	end if;
	end;
end loop;

-- se sobrou alguma coisa manda para o banco
deletar_mensalidade(	tb_delete_mensalidade_w);
update_mensalidade(	tb_nr_seq_mensalidade_w,
			tb_vl_mensalidade_w,
			tb_vl_coparticipacao_w,
			tb_vl_adicionais_w,
			tb_vl_outros_w,
			tb_vl_pos_estabelecido_w,
			tb_vl_pre_estabelecido_w,
			tb_vl_pro_rata_dia_w,
			tb_vl_antecipacao_w,
			tb_qt_beneficiarios_w,
			tb_vl_taxa_boleto_w,
			tb_vl_taxa_boleto_emb_w,
			tb_vl_outros_pagador_w);

end atualizar_mensalidade;

procedure delete_mens_vl_negativo (	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type)is

ie_gerou_log_benef_w	varchar2(1);
					
Cursor C01 is
	select	nr_sequencia nr_seq_mensalidade,
		nr_seq_pagador,
		vl_mensalidade
	from	pls_mensalidade
	where	nr_seq_lote	= nr_seq_lote_p
	and	vl_mensalidade	< 0;
	
cursor c02 (	nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	nr_seq_segurado,
		vl_mensalidade
	from	pls_mensalidade_segurado
	where	vl_mensalidade < 0
	and	nr_seq_mensalidade = nr_seq_mensalidade_pc;
	
begin

for r_c01_w in C01 loop
	begin
	ie_gerou_log_benef_w	:= 'N';
	
	for r_c02_w in c02 (r_c01_w.nr_seq_mensalidade) loop
		begin
		pls_gerar_mens_log_erro(nr_seq_lote_p,r_c01_w.nr_seq_pagador,r_c02_w.nr_seq_segurado,null, 'Mensalidade com valor negativo: R$ '||campo_mascara_virgula(r_c02_w.vl_mensalidade),cd_estabelecimento_p,nm_usuario_p);
		ie_gerou_log_benef_w	:= 'S';
		end;
	end loop;
	
	pls_deletar_mensalidade(r_c01_w.nr_seq_mensalidade, 'S', nm_usuario_p, 'S');
	
	if	(ie_gerou_log_benef_w = 'N') then
		pls_gerar_mens_log_erro(nr_seq_lote_p,r_c01_w.nr_seq_pagador,null,null, 'Mensalidade com valor negativo: R$ '||campo_mascara_virgula(r_c01_w.vl_mensalidade),cd_estabelecimento_p,nm_usuario_p);
	end if;
	end;
end loop;

end delete_mens_vl_negativo;

procedure atualizar_lote (nr_seq_lote_p	pls_lote_mensalidade.nr_sequencia%type)is

qt_beneficiarios_w		pls_integer;
vl_mensalidades_w		number(15,2)	:= 0;
vl_pre_estabelecido_w		number(15,2)	:= 0;
vl_pos_estabelecido_w		number(15,2)	:= 0;
vl_coparticipacao_w		number(15,2)	:= 0;
vl_outros_w			number(15,2)	:= 0;
vl_adicionais_w			number(15,2)	:= 0;
vl_cancelado_w			number(15,2)	:= 0;
vl_pro_rata_dia_w		number(15,2);
vl_antecipacao_w		number(15,2);

begin

select	sum(vl_pro_rata_dia),
	sum(vl_antecipacao),
	sum(qt_beneficiarios),
	sum(vl_mensalidade),
	sum(vl_pre_estabelecido),
	sum(vl_pos_estabelecido),
	sum(vl_coparticipacao),
	sum(vl_outros),
	sum(vl_adicionais)
into	vl_pro_rata_dia_w,
	vl_antecipacao_w,
	qt_beneficiarios_w,
	vl_mensalidades_w,
	vl_pre_estabelecido_w,
	vl_pos_estabelecido_w,
	vl_coparticipacao_w,
	vl_outros_w,
	vl_adicionais_w
from	pls_mensalidade
where	nr_seq_lote = nr_seq_lote_p
and	ie_cancelamento is null;

update	pls_lote_mensalidade
set	vl_lote			= nvl(vl_mensalidades_w,0),
	vl_pre_estabelecido	= nvl(vl_pre_estabelecido_w,0),
	vl_pos_estabelecido	= nvl(vl_pos_estabelecido_w,0),
	vl_coparticipacao	= nvl(vl_coparticipacao_w,0),
	vl_pro_rata_dia		= nvl(vl_pro_rata_dia_w,0),
	qt_beneficiario_lote	= qt_beneficiarios_w,
	qt_pagadores_lote	= qt_pagadores_total_lote_w,
	vl_antecipacao		= nvl(vl_antecipacao_w,0),
	vl_outros		= nvl(vl_outros_w,0),
	vl_adicionais		= nvl(vl_adicionais_w,0),
	vl_cancelado		= nvl(vl_cancelado_w,0)
where	nr_sequencia		= nr_seq_lote_p;

end atualizar_lote;

procedure carregar_tabelas_tmp(	nr_seq_lote_p	in pls_lote_mensalidade.nr_sequencia%type,
				nm_usuario_p	in varchar2) is
nr_seq_mens_seg_t	pls_util_cta_pck.t_number_table;
nr_seq_mensalidade_t	pls_util_cta_pck.t_number_table;
Cursor C01 is
	select	nr_sequencia
	from	pls_mensalidade_segurado
	where	nr_seq_lote	= nr_seq_lote_p;

Cursor C02 is
	select	nr_sequencia
	from	pls_mensalidade
	where	nr_seq_lote	= nr_seq_lote_p;

begin
exec_sql_dinamico(nm_usuario_p, 'truncate table pls_mensalidade_seg_tmp');
exec_sql_dinamico(nm_usuario_p, 'truncate table pls_mensalidade_tmp');

open C01;
loop
	fetch C01 bulk collect into nr_seq_mens_seg_t limit pls_util_cta_pck.qt_registro_transacao_w;
	exit when nr_seq_mens_seg_t.count = 0;
	
	forall i in nr_seq_mens_seg_t.first..nr_seq_mens_seg_t.last
		insert into pls_mensalidade_seg_tmp
		values(	nr_seq_mens_seg_t(i));
	commit;
	
end loop;
close C01;

open C02;
loop
	fetch C02 bulk collect into nr_seq_mensalidade_t limit pls_util_cta_pck.qt_registro_transacao_w;
	exit when nr_seq_mensalidade_t.count = 0;
	
	forall i in nr_seq_mensalidade_t.first..nr_seq_mensalidade_t.last
		insert into pls_mensalidade_tmp
		values(	nr_seq_mensalidade_t(i));
	commit;
	
end loop;
close C02;

end carregar_tabelas_tmp;

procedure atualizar_valores_lote(	nr_seq_lote_p		in pls_lote_mensalidade.nr_sequencia%type,
					nm_usuario_p		in usuario.nm_usuario%type,
					cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
					ie_tipo_atualizacao_p	in varchar2 default null) is
ie_tipo_atualizacao_w	varchar2(2);
begin

ie_tipo_atualizacao_w	:= nvl(ie_tipo_atualizacao_p, 'T');

if	(ie_tipo_atualizacao_w = 'T') then
	-- Carregar as tabelas temporarias
	carregar_tabelas_tmp(nr_seq_lote_p, nm_usuario_p);
end if;

--Atualizar valores das mensalidades
atualizar_mensalidade_segurado;
atualizar_mensalidade;

if	(ie_tipo_atualizacao_w = 'T') then
	--Deletar as mensalidades com valor negativo e gerar log de erro
	delete_mens_vl_negativo(nr_seq_lote_p, nm_usuario_p, cd_estabelecimento_p);
end if;

--Atualizar valor do lote
atualizar_lote(nr_seq_lote_p);

end;

end pls_mens_atualizacao_pck;
/