create or replace package hpms_configurar_portal_tws_pck as

				
procedure tws_configurar_beneficiario( 		cd_estabelecimento_p estabelecimento.cd_estabelecimento%type);

procedure tws_atualizar_diretorio_benef(	nm_usuario_p		usuario.nm_usuario%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type);
									
procedure tws_configurar_auditoria_conc( 	cd_estabelecimento_p estabelecimento.cd_estabelecimento%type);

procedure tws_configurar_estipulante( 		cd_estabelecimento_p estabelecimento.cd_estabelecimento%type);
					
procedure tws_atualizar_diretorio_estip(	nm_usuario_p		usuario.nm_usuario%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type);
									
end hpms_configurar_portal_tws_pck; 
/

create or replace package body  hpms_configurar_portal_tws_pck as


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar os diretorio de anexos no storage, usado somente no Delphi
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
procedure  tws_atualizar_diretorio_benef(	nm_usuario_p		usuario.nm_usuario%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type ) is


ds_diretorio_anexo_benef_w	pls_param_beneficiario_tws.ds_diretorio_anexo_benef%type;
ds_diretorio_anexo_pf_w		pls_param_beneficiario_tws.ds_diretorio_anexo_pf%type;
qt_storage_w			Number(2);

begin

select  max(ds_diretorio_anexo_benef),
        max(ds_diretorio_anexo_pf)
into	ds_diretorio_anexo_benef_w,
	ds_diretorio_anexo_pf_w
from    pls_param_beneficiario_tws 
where   cd_estabelecimento = cd_estabelecimento_p;

if	( ds_diretorio_anexo_benef_w is not null ) then
	select  count(1)
	into	qt_storage_w
	from    file_storage_path
	where   nm_storage = 'HPMS_INSURED_FILE_DOC';
	
	if	(qt_storage_w > 0) then
		
		update	file_storage_path
		set	ds_path = ds_diretorio_anexo_benef_w
		where   nm_storage = 'HPMS_INSURED_FILE_DOC'
		and	cd_uuid = '1236fdcf-5f8e-4a8e-95b7-43d3b646ffc4';
	else	
		insert into file_storage_path 
			(nm_storage, cd_uuid, cd_estabelecimento,
			ds_path, ie_writable, ie_readable,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, ie_driver)
		values ( 'HPMS_INSURED_FILE_DOC', '1236fdcf-5f8e-4a8e-95b7-43d3b646ffc4',cd_estabelecimento_p,
			ds_diretorio_anexo_benef_w, 'S', 'S',
			sysdate, nm_usuario_p, sysdate,
			nm_usuario_p, 'local');
	end if;
	
	commit;
end if;


qt_storage_w := 0;
if	( ds_diretorio_anexo_pf_w is not null ) then
	select  count(1)
	into	qt_storage_w
	from    file_storage_path
	where   nm_storage = 'PERSON_FILES';
	
	if	(qt_storage_w > 0) then
		
		update	file_storage_path
		set	ds_path = ds_diretorio_anexo_pf_w
		where   nm_storage = 'PERSON_FILES'
		and	cd_uuid = '5274025e-ae68-4b4b-99fa-ab0acddf7d20';
	else	
		insert into file_storage_path 
			(nm_storage, cd_uuid, cd_estabelecimento,
			ds_path, ie_writable, ie_readable,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, ie_driver)
		values ( 'PERSON_FILES', '5274025e-ae68-4b4b-99fa-ab0acddf7d20',cd_estabelecimento_p,
			ds_diretorio_anexo_pf_w, 'S', 'S',
			sysdate, nm_usuario_p, sysdate,
			nm_usuario_p, 'local');
	end if;
	
	commit;
end if;

end tws_atualizar_diretorio_benef;



/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finality: Configurar localidade para o Brasil
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
procedure configurar_localidade (cd_estabelecimento_p estabelecimento.cd_estabelecimento%type) is

qt_registro_w	Number(5);

begin

if 	(cd_estabelecimento_p is not null) then
	update 	pls_web_param_geral 
	set  	ie_plataforma_web = 2;
	
	select 	count(1)
	into	qt_registro_w
	from 	establishment_locale
	where 	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(qt_registro_w = 0) then
		insert into establishment_locale 
			(cd_estabelecimento, ds_locale, ds_calendar,
			ds_timezone , dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_first_week_day,
			ds_secondary_calendar, cd_default_currency, ds_locale_tws)
		values( cd_estabelecimento_p, 'pt_BR', 'iso8601',
			null, sysdate, 'TWS',
			sysdate,'TWS', null,
			null, null, 'pt_BR');
	else
		update 	establishment_locale
		set	ds_locale_tws = 'pt_BR',
			dt_atualizacao = sysdate,
			nm_usuario = 'TWS'
		where 	cd_estabelecimento = cd_estabelecimento_p;
	end if;
	
	commit;
end if; 

end configurar_localidade;



/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finality:  Carregar mensagens da funcao OPS - Comunicacao Externa
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
procedure atualizar_mensagens is

qt_registro_w	Number(5);

begin

update pls_comunic_ext_hist_web a
set    a.ds_login = (select max(b.ds_login) from wsuite_usuario b where b.cd_pessoa_fisica = a.cd_pessoa_fisica_resp and b.cd_pessoa_fisica is not null)
where  a.ds_login is null
and    a.ie_tipo_acesso = 'B'
and    a.cd_pessoa_fisica_resp is not null;

commit; 

end atualizar_mensagens;



/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Fazer a carga dos beneficiarios e demais dados necessarios no portal do beneficiario
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
procedure tws_configurar_beneficiario( 	cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type) is


ds_mensagem_w Varchar2(255);					
begin


if ( cd_estabelecimento_p is not null and cd_estabelecimento_p > 0 ) then

	configurar_localidade(cd_estabelecimento_p);	
	
	begin
		ds_mensagem_w := 'IMPORT_USERS_TO_PSA_TWS_PACK';
		import_users_to_psa_tws_pack.import_beneficiaries;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(1112449,'DS_ROTINA='||ds_mensagem_w);
	end;
	
	
	begin
		ds_mensagem_w := 'IMPORT_USERS_TO_PSA_TWS_PACK';
		atualizar_mensagens;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort( 1112449,'DS_ROTINA='||ds_mensagem_w);
	end;	
	
end if;

end tws_configurar_beneficiario; 



/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Configurar a base para o projeto da auditoria concorrente
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
procedure tws_configurar_auditoria_conc( 	cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type) is


ds_mensagem_w Varchar2(255);					
begin


if ( cd_estabelecimento_p is not null and cd_estabelecimento_p > 0 ) then

	configurar_localidade(cd_estabelecimento_p);	
		
end if;


end tws_configurar_auditoria_conc; 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finality:  Carregar mensagens da funcao OPS - Comunicacao Externa
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
procedure atualizar_mensagens_estip is

qt_registro_w	Number(5);

begin

update pls_comunic_ext_hist_web a
set    a.ds_login = (select max(b.ds_login) from wsuite_usuario b where b.cd_pessoa_fisica = a.cd_pessoa_fisica_resp and b.cd_pessoa_fisica is not null)
where  a.ds_login is null
and    a.ie_tipo_acesso = 'E'
and    a.cd_pessoa_fisica_resp is not null;

commit; 

end atualizar_mensagens_estip;

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar os diretorio de anexos no storage, usado somente no Delphi
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
procedure  tws_atualizar_diretorio_estip(	nm_usuario_p		usuario.nm_usuario%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type ) is


ds_diretorio_anexo_adesao_w	pls_param_estipulante_tws.ds_diretorio_anexo_adesao%type;
ds_diretorio_anexo_resscisao_w	pls_param_estipulante_tws.ds_diretorio_anexo_resscisao%type;
ds_dir_anexo_reinclusao_w	pls_param_estipulante_tws.ds_diretorio_anexo_reinclusao%type;
ds_dir_anexo_auditoria_w	pls_param_estipulante_tws.ds_diretorio_anexo_auditoria%type;
qt_storage_w			number(2);

begin

select  max(ds_diretorio_anexo_adesao),
	max(ds_diretorio_anexo_resscisao),
	max(ds_diretorio_anexo_reinclusao),
	max(ds_diretorio_anexo_auditoria)
into	ds_diretorio_anexo_adesao_w,
	ds_diretorio_anexo_resscisao_w,
	ds_dir_anexo_reinclusao_w,
	ds_dir_anexo_auditoria_w
from    pls_param_estipulante_tws 
where   cd_estabelecimento = cd_estabelecimento_p;

if	( ds_diretorio_anexo_adesao_w is not null ) then
	select  count(1)
	into	qt_storage_w
	from    file_storage_path
	where   nm_storage = 'HPMS_FILES_PROPOSAL';
	
	if	(qt_storage_w > 0) then
		
		update	file_storage_path
		set	ds_path		= ds_diretorio_anexo_adesao_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where   nm_storage	= 'HPMS_FILES_PROPOSAL'
		and	cd_uuid		= '78efd01a-a099-4f21-9f60-975367c7b1a4';
	else	
		insert into file_storage_path 
			(nm_storage, cd_uuid, cd_estabelecimento,
			ds_path, ie_writable, ie_readable,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, ie_driver)
		values ( 'HPMS_FILES_PROPOSAL', '78efd01a-a099-4f21-9f60-975367c7b1a4',cd_estabelecimento_p,
			ds_diretorio_anexo_adesao_w, 'S', 'S',
			sysdate, nm_usuario_p, sysdate,
			nm_usuario_p, 'local');
	end if;
end if;

qt_storage_w	:= 0;

if	( ds_diretorio_anexo_resscisao_w is not null ) then
	select  count(1)
	into	qt_storage_w
	from    file_storage_path
	where   nm_storage = 'HPMS_RESCISSION_ATTACHMENT';
	
	if	(qt_storage_w > 0) then
		
		update	file_storage_path
		set	ds_path		= ds_diretorio_anexo_resscisao_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where   nm_storage	= 'HPMS_RESCISSION_ATTACHMENT'
		and	cd_uuid		= 'ba4e3f3d-5dc5-46d7-9b55-943200cfa040';
	else	
		insert into file_storage_path 
			(nm_storage, cd_uuid, cd_estabelecimento,
			ds_path, ie_writable, ie_readable,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, ie_driver)
		values ( 'HPMS_RESCISSION_ATTACHMENT', 'ba4e3f3d-5dc5-46d7-9b55-943200cfa040',cd_estabelecimento_p,
			ds_diretorio_anexo_resscisao_w, 'S', 'S',
			sysdate, nm_usuario_p, sysdate,
			nm_usuario_p, 'local');
	end if;
end if;

qt_storage_w	:= 0;

if	( ds_dir_anexo_reinclusao_w is not null ) then
	select  count(1)
	into	qt_storage_w
	from    file_storage_path
	where   nm_storage = 'HPMS_REINCLUSION_REQUEST';
	
	if	(qt_storage_w > 0) then
		
		update	file_storage_path
		set	ds_path		= ds_dir_anexo_reinclusao_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where   nm_storage	= 'HPMS_REINCLUSION_REQUEST'
		and	cd_uuid		= '45364c84-b548-4c8b-80a9-847ad96b217e';
	else	
		insert into file_storage_path 
			(nm_storage, cd_uuid, cd_estabelecimento,
			ds_path, ie_writable, ie_readable,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, ie_driver)
		values ( 'HPMS_REINCLUSION_REQUEST', '45364c84-b548-4c8b-80a9-847ad96b217e',cd_estabelecimento_p,
			ds_dir_anexo_reinclusao_w, 'S', 'S',
			sysdate, nm_usuario_p, sysdate,
			nm_usuario_p, 'local');
	end if;
end if;

qt_storage_w	:= 0;

if	( ds_dir_anexo_auditoria_w is not null ) then
	select  count(1)
	into	qt_storage_w
	from    file_storage_path
	where   nm_storage = 'HPMS_AUT_ANALYSIS_AUDIT';
	
	if	(qt_storage_w > 0) then
		
		update	file_storage_path
		set	ds_path		= ds_dir_anexo_auditoria_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where   nm_storage	= 'HPMS_AUT_ANALYSIS_AUDIT'
		and	cd_uuid		= 'c12cd047-ba55-4661-8b48-7bd53cac7ad0';
	else	
		insert into file_storage_path 
			(nm_storage, cd_uuid, cd_estabelecimento,
			ds_path, ie_writable, ie_readable,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, ie_driver)
		values ( 'HPMS_AUT_ANALYSIS_AUDIT', 'c12cd047-ba55-4661-8b48-7bd53cac7ad0',cd_estabelecimento_p,
			ds_dir_anexo_auditoria_w, 'S', 'S',
			sysdate, nm_usuario_p, sysdate,
			nm_usuario_p, 'local');
	end if;
end if;

commit;

end tws_atualizar_diretorio_estip;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Fazer a carga dos estipulantes, grupos de contrato e demais dados necessarios no 
portal do estipulante.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
procedure tws_configurar_estipulante( 	cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type) is


ds_mensagem_w Varchar2(255);					
begin


if ( cd_estabelecimento_p is not null and cd_estabelecimento_p > 0 ) then

	configurar_localidade(cd_estabelecimento_p);	
	
	begin
		ds_mensagem_w := 'IMPORT_USERS_TO_PSA_TWS_PACK';
		update	pls_visible_false
		set	ie_tws_stipulator	= 'S';

		delete 	from pls_log_tws;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(1112449,'DS_ROTINA='||ds_mensagem_w);
	end;
		
	begin
		ds_mensagem_w := 'IMPORT_USERS_TO_PSA_TWS_PACK';
		import_users_to_psa_tws_pack.import_stipulators;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(1112449,'DS_ROTINA='||ds_mensagem_w);
	end;
	
	
	begin
		ds_mensagem_w := 'ATUALIZAR_MENSAGENS_ESTIPULANTE';
		atualizar_mensagens_estip;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort( 1112449,'DS_ROTINA='||ds_mensagem_w);
	end;	
	
end if;

end tws_configurar_estipulante; 

end hpms_configurar_portal_tws_pck; 
/