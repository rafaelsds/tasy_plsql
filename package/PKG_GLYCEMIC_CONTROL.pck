CREATE OR REPLACE PACKAGE PKG_GLYCEMIC_CONTROL AS
  --
  TYPE T_TAB_CD_PES_FIS IS TABLE OF PESSOA_FISICA.CD_PESSOA_FISICA%TYPE;
  --
  FUNCTION GET_TAB_CD_PES_FIS(P_START_DATE DATE,
                              P_END_DATE   DATE,
                              P_YEAR       NUMBER,
                              P_QUARTER    NUMBER,
                              P_ESTAB      NUMBER,
                              P_SETOR      NUMBER)
    RETURN T_TAB_CD_PES_FIS
    PIPELINED;  
  --
  FUNCTION GET_DISEASE_DKA_HHNC(P_CD_DOENCA CID_DOENCA.CD_DOENCA_CID%TYPE) RETURN NUMBER;
  --
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0,
                P_YEAR       IN NUMBER,
                P_QUARTER    IN NUMBER DEFAULT 1,
                P_TYPE       IN NUMBER DEFAULT 1,
                P_PATIENT    IN VARCHAR2);
  --
END PKG_GLYCEMIC_CONTROL;
/
CREATE OR REPLACE PACKAGE BODY PKG_GLYCEMIC_CONTROL AS

  PROCEDURE TABLE_CLEANING IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_GLYCEMIC_CONTROL';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_GLYCEMIC_CONTROL_ESTAB';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_GLYCEMIC_CONTROL_UNIT';
  END TABLE_CLEANING;
  --
  FUNCTION GET_TAB_CD_PES_FIS(P_START_DATE DATE,
                              P_END_DATE   DATE,
                              P_YEAR       NUMBER,
                              P_QUARTER    NUMBER,
                              P_ESTAB      NUMBER,
                              P_SETOR      NUMBER)
                              
    RETURN T_TAB_CD_PES_FIS PIPELINED IS
    
  V_START_DATE DATE;
  V_END_DATE   DATE;
  NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
  
  CURSOR C_CD_PES_FIS IS
    SELECT DISTINCT CD_PESSOA_FISICA
      FROM (SELECT DISTINCT UN.CD_PESSOA_FISICA
              FROM (SELECT NR_ATENDIMENTO,
                           CD_PESSOA_FISICA,
                           CD_ESTABELECIMENTO,
                           CD_SETOR_ATENDIMENTO,
                           DT_INI,
                           DT_FIM,
                           DT_ENTRADA,
                           DT_ALTA,
                           DT_ENTRADA_SETOR,
                           DT_SAIDA_SETOR
                      FROM (SELECT U.NR_ATENDIMENTO,
                                   AP.CD_PESSOA_FISICA,
                                   S.CD_ESTABELECIMENTO,
                                   U.CD_SETOR_ATENDIMENTO,
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE) DT_INI,
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(V_END_DATE + 1, SYSDATE)) DT_FIM,
                                   AP.DT_ENTRADA,
                                   AP.DT_ALTA,
                                   MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                                   PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                        U.DT_ENTRADA_UNIDADE,
                                                                        U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR                                              
                              FROM (SELECT DISTINCT P.NR_ATENDIMENTO, 
                                                    P.CD_PESSOA_FISICA,
                                                    P.DT_ENTRADA,
                                                    P.DT_ALTA 
                                      FROM ATENDIMENTO_GLICEMIA A,
                                           ATENDIMENTO_PACIENTE P 
                                     WHERE A.NR_ATENDIMENTO = P.NR_ATENDIMENTO   
                                       AND A.QT_GLICEMIA IS NOT NULL
                                       AND A.QT_GLICEMIA > 0
                                       AND A.QT_GLICEMIA <= 1500
                                       AND A.DT_CONTROLE BETWEEN P.DT_ENTRADA AND NVL(P.DT_ALTA, A.DT_CONTROLE)
                                       AND A.DT_CONTROLE BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, V_START_DATE)
                                                             AND PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                       AND P.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                       AND (P.DT_ALTA IS NULL OR
                                            P.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, V_START_DATE))) AP,
                                   ATEND_PACIENTE_UNIDADE U,
                                   SETOR_ATENDIMENTO S
                             WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                               AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                               AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO)
                               AND DECODE(NVL(P_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_ESTAB, 0)
                               AND DECODE(NVL(P_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_SETOR, 0)
                               AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                               AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                               AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))         
                               AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                            U.DT_ENTRADA_UNIDADE,
                                                                            U.CD_SETOR_ATENDIMENTO),
                                       PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)) >=
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)
                             GROUP BY U.NR_ATENDIMENTO,
                                      AP.CD_PESSOA_FISICA,
                                      S.CD_ESTABELECIMENTO,
                                      U.CD_SETOR_ATENDIMENTO,
                                      AP.DT_ENTRADA,
                                      AP.DT_ALTA,
                                      PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                           U.DT_ENTRADA_UNIDADE,
                                                                           U.CD_SETOR_ATENDIMENTO)) UN
                     WHERE (DT_ALTA IS NULL OR DT_ENTRADA_SETOR <= DT_ALTA)
                       AND (DT_SAIDA_SETOR IS NULL OR DT_SAIDA_SETOR >= DT_ENTRADA)) UN,
                   ATENDIMENTO_GLICEMIA AG
             WHERE AG.NR_ATENDIMENTO = UN.NR_ATENDIMENTO
               AND AG.QT_GLICEMIA IS NOT NULL
               AND AG.QT_GLICEMIA > 0
               AND AG.QT_GLICEMIA <= 1500
               AND AG.DT_CONTROLE BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(UN.CD_ESTABELECIMENTO, V_START_DATE)
                                      AND PKG_ATEND_PAC_UNID.GET_SYS_DT(UN.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
               AND AG.DT_CONTROLE BETWEEN UN.DT_ENTRADA_SETOR AND NVL(UN.DT_SAIDA_SETOR, AG.DT_CONTROLE)
            UNION
            SELECT DISTINCT UN.CD_PESSOA_FISICA
              FROM (SELECT NR_ATENDIMENTO,
                           CD_PESSOA_FISICA,
                           CD_ESTABELECIMENTO,
                           CD_SETOR_ATENDIMENTO,
                           DT_INI,
                           DT_FIM,
                           DT_ENTRADA,
                           DT_ALTA,
                           DT_ENTRADA_SETOR,
                           DT_SAIDA_SETOR
                      FROM (SELECT U.NR_ATENDIMENTO,
                                   AP.CD_PESSOA_FISICA,
                                   S.CD_ESTABELECIMENTO,
                                   U.CD_SETOR_ATENDIMENTO,
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE) DT_INI,
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(V_END_DATE + 1, SYSDATE)) DT_FIM,
                                   AP.DT_ENTRADA,
                                   AP.DT_ALTA,
                                   MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                                   PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                        U.DT_ENTRADA_UNIDADE,
                                                                        U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR                                              
                              FROM (SELECT DISTINCT P.NR_ATENDIMENTO, 
                                                    P.CD_PESSOA_FISICA,
                                                    P.DT_ENTRADA,
                                                    P.DT_ALTA 
                                      FROM EXAME_LABORATORIO EL,
                                           EXAME_LAB_RESULTADO ER,
                                           EXAME_LAB_RESULT_ITEM EI,
                                           ATENDIMENTO_PACIENTE P
                                     WHERE EL.IE_TIPO_EXAME_REL = 8
                                       AND EL.NR_SEQ_EXAME = EI.NR_SEQ_EXAME
                                       AND EI.NR_SEQ_RESULTADO = ER.NR_SEQ_RESULTADO
                                       AND ER.DT_RESULTADO IS NOT NULL
                                       AND EI.QT_RESULTADO IS NOT NULL
                                       AND EI.QT_RESULTADO > 0
                                       AND EI.QT_RESULTADO <= 1500
                                       AND NVL(ER.NR_ATENDIMENTO, OBTER_ATENDIMENTO_PRESCR(ER.NR_PRESCRICAO)) = P.NR_ATENDIMENTO   
                                       AND EI.DT_COLETA BETWEEN P.DT_ENTRADA AND NVL(P.DT_ALTA, EI.DT_COLETA)
                                       AND EI.DT_COLETA BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, V_START_DATE)
                                                            AND PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                       AND P.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                       AND (P.DT_ALTA IS NULL OR
                                            P.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, V_START_DATE))) AP,
                                   ATEND_PACIENTE_UNIDADE U,
                                   SETOR_ATENDIMENTO S
                             WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                               AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                               AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO)
                               AND DECODE(NVL(P_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_ESTAB, 0)
                               AND DECODE(NVL(P_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_SETOR, 0)
                               AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                               AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                               AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))         
                               AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                            U.DT_ENTRADA_UNIDADE,
                                                                            U.CD_SETOR_ATENDIMENTO),
                                       PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)) >=
                                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)
                             GROUP BY U.NR_ATENDIMENTO,
                                      AP.CD_PESSOA_FISICA,
                                      S.CD_ESTABELECIMENTO,
                                      U.CD_SETOR_ATENDIMENTO,
                                      AP.DT_ENTRADA,
                                      AP.DT_ALTA,
                                      PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                           U.DT_ENTRADA_UNIDADE,
                                                                           U.CD_SETOR_ATENDIMENTO)) UN
                     WHERE (DT_ALTA IS NULL OR DT_ENTRADA_SETOR <= DT_ALTA)
                       AND (DT_SAIDA_SETOR IS NULL OR DT_SAIDA_SETOR >= DT_ENTRADA)) UN,
                   EXAME_LABORATORIO L,
                   EXAME_LAB_RESULTADO R,
                   EXAME_LAB_RESULT_ITEM I
             WHERE L.IE_TIPO_EXAME_REL = 8
               AND NVL(R.NR_ATENDIMENTO, OBTER_ATENDIMENTO_PRESCR(R.NR_PRESCRICAO)) = UN.NR_ATENDIMENTO
               AND L.NR_SEQ_EXAME = I.NR_SEQ_EXAME
               AND I.NR_SEQ_RESULTADO = R.NR_SEQ_RESULTADO
               AND R.DT_RESULTADO IS NOT NULL
               AND I.QT_RESULTADO IS NOT NULL
               AND I.QT_RESULTADO > 0
               AND I.QT_RESULTADO <= 1500
               AND I.DT_COLETA BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(UN.CD_ESTABELECIMENTO, V_START_DATE)
                                   AND PKG_ATEND_PAC_UNID.GET_SYS_DT(UN.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
               AND I.DT_COLETA BETWEEN UN.DT_ENTRADA_SETOR AND NVL(UN.DT_SAIDA_SETOR, I.DT_COLETA))
       ;
  
  TYPE T_CD_PES_FIS IS TABLE OF C_CD_PES_FIS%ROWTYPE;
  R_CD_PES_FIS T_CD_PES_FIS;
  
  BEGIN
  
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
  
    IF P_YEAR IS NOT NULL AND P_QUARTER IS NOT NULL THEN
    
      SELECT GREATEST(INI, NVL(P_START_DATE, INI)) INI,
             LEAST(FIM, NVL(P_END_DATE, FIM)) FIM
        INTO V_START_DATE,
             V_END_DATE
        FROM (SELECT TO_DATE(LPAD((P_QUARTER * 3) - 2, 2, '0') || P_YEAR, 'MMYYYY') INI,
                     LAST_DAY(TO_DATE(LPAD((P_QUARTER * 3), 2, '0') || P_YEAR, 'MMYYYY')) FIM
                FROM DUAL)
       WHERE INI <= NVL(P_END_DATE, INI)
         AND FIM >= NVL(P_START_DATE, FIM);
  
      OPEN C_CD_PES_FIS;
      LOOP
        FETCH C_CD_PES_FIS BULK COLLECT INTO R_CD_PES_FIS LIMIT 1000;
          
          BEGIN
            FOR i IN R_CD_PES_FIS.FIRST..R_CD_PES_FIS.LAST LOOP
              PIPE ROW(R_CD_PES_FIS(i).CD_PESSOA_FISICA);  
            END LOOP;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;  

        EXIT WHEN C_CD_PES_FIS%NOTFOUND; 
     
      END LOOP;
      CLOSE C_CD_PES_FIS;                                  
  
    END IF;
    
    RETURN;
  END GET_TAB_CD_PES_FIS;
  --
  FUNCTION GET_DISEASE_DKA_HHNC(P_CD_DOENCA CID_DOENCA.CD_DOENCA_CID%TYPE) RETURN NUMBER IS
    
    NR NUMBER(1);
  
  BEGIN
    
    NR := 0;
    
    IF P_CD_DOENCA IS NOT NULL THEN
      SELECT COUNT(1)
        INTO NR
        FROM CID_DOENCA D
       WHERE D.CD_DOENCA_CID = P_CD_DOENCA
         AND D.IE_SITUACAO = 'A'
         AND D.IE_DOENCA_REL in (9, 11);
    END IF;
    
    RETURN NR;
    
  END GET_DISEASE_DKA_HHNC;
  --
  PROCEDURE PRE_PROCESS(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_CD_ESTAB IN NUMBER,
    P_CD_SETOR IN NUMBER,
    P_YEAR IN NUMBER,
    P_QUARTER IN NUMBER,
    P_TYPE IN NUMBER,
    P_PATIENT IN VARCHAR2) IS
    --
    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
    --
    CURSOR C_GLYCEMIC_CONTROL (V_START_DATE DATE,
                               V_END_DATE   DATE,
                               V_SETOR      NUMBER,
                               V_PATIENT    VARCHAR2) IS 
      SELECT T.*,
             PKG_GLYCEMIC_CONTROL.GET_DISEASE_DKA_HHNC(T.CD_DIAGNOSTICO_ADMIS) IE_DOENCA,
             OBTER_DESC_CID(T.CD_DIAGNOSTICO_ADMIS) DS_DIAGNOSTICO_ADMIS
        FROM (SELECT X.CD_ESTABELECIMENTO,
                     X.CD_SETOR_ATENDIMENTO,
                     X.NR_ATENDIMENTO,
                     X.DT_INI,
                     X.DT_FIM,
                     X.DT_ENTRADA_UNID,
                     X.DT_ENTRADA_SETOR,
                     X.DT_SAIDA_SETOR,
                     X.DT_EXAME,
                     X.QT_EXAME,
                     X.AVG_GLICEMIA,
                     CASE WHEN AVG_GLICEMIA <= 110 THEN 1 ELSE 0 END QT_AVG_1,
                     CASE WHEN AVG_GLICEMIA > 110 AND AVG_GLICEMIA <= 150 THEN 1 ELSE 0 END QT_AVG_2,
                     CASE WHEN AVG_GLICEMIA > 150 AND AVG_GLICEMIA <= 180 THEN 1 ELSE 0 END QT_AVG_3,
                     CASE WHEN AVG_GLICEMIA > 180 THEN 1 ELSE 0 END QT_AVG_4,
                     X.NR_GLICEMIA_1,
                     X.NR_GLICEMIA_2,
                     PKG_ATEND_PAC_UNID.GET_DIGNOSIS_ADMIT_DATA(X.NR_ATENDIMENTO, X.DT_ENTRADA_UNID, 'D') CD_DIAGNOSTICO_ADMIS
                FROM (SELECT V.CD_ESTABELECIMENTO,
                             V.CD_SETOR_ATENDIMENTO,
                             V.NR_ATENDIMENTO,
                             V.DT_INI,
                             V.DT_FIM,
                             V.DT_ENTRADA_UNID,
                             V.DT_ENTRADA_SETOR,
                             V.DT_SAIDA_SETOR,
                             TRUNC(V.DT_EXAME, 'DD') DT_EXAME,
                             COUNT(1) QT_EXAME,
                             SUM(QT_GLICEMIA) / COUNT(1) AVG_GLICEMIA,
                             SUM(CASE 
                                   WHEN QT_GLICEMIA < 50 THEN 1
                                   ELSE 0 
                                 END) NR_GLICEMIA_1,
                             SUM(CASE 
                                   WHEN QT_GLICEMIA BETWEEN 50 AND 79 THEN 1
                                   ELSE 0 
                                 END) NR_GLICEMIA_2
                        FROM (SELECT UN.CD_ESTABELECIMENTO,
                                     UN.CD_SETOR_ATENDIMENTO,
                                     UN.NR_ATENDIMENTO,
                                     UN.DT_INI,
                                     UN.DT_FIM,
                                     DT_ENTRADA_UNID,
                                     UN.DT_ENTRADA_SETOR,
                                     UN.DT_SAIDA_SETOR,
                                     G.DT_CONTROLE DT_EXAME,
                                     G.QT_GLICEMIA
                                FROM (SELECT NR_ATENDIMENTO,
                                             CD_ESTABELECIMENTO,
                                             CD_SETOR_ATENDIMENTO,
                                             DT_INI,
                                             DT_FIM,
                                             DT_ENTRADA_SETOR DT_ENTRADA_UNID,
                                             GREATEST(DT_ENTRADA_SETOR, DT_ENTRADA - .25) DT_ENTRADA_SETOR,
                                             LEAST(NVL(DT_SAIDA_SETOR, NVL2(DT_ALTA, DT_ALTA + .25, NULL)),
                                                   NVL2(DT_ALTA, DT_ALTA + .25, DT_SAIDA_SETOR)) DT_SAIDA_SETOR
                                        FROM (SELECT U.NR_ATENDIMENTO,
                                                     S.CD_ESTABELECIMENTO,
                                                     U.CD_SETOR_ATENDIMENTO,
                                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE) DT_INI,
                                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(V_END_DATE + 1, SYSDATE)) DT_FIM,
                                                     A.DT_ENTRADA,
                                                     A.DT_ALTA,
                                                     MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                                                     PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                          U.DT_ENTRADA_UNIDADE,
                                                                                          U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR                                              
                                                FROM (SELECT DISTINCT AP.NR_ATENDIMENTO,
                                                                      AP.DT_ENTRADA,
                                                                      AP.DT_ALTA
                                                        FROM ATENDIMENTO_GLICEMIA AG,
                                                             ATENDIMENTO_PACIENTE AP 
                                                       WHERE AG.NR_ATENDIMENTO = AP.NR_ATENDIMENTO   
                                                         AND AG.DT_CONTROLE BETWEEN AP.DT_ENTRADA AND NVL(AP.DT_ALTA, AG.DT_CONTROLE)
                                                         AND (V_PATIENT IS NULL OR V_PATIENT = AP.CD_PESSOA_FISICA)
                                                         AND AG.DT_CONTROLE BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(AP.CD_ESTABELECIMENTO, V_START_DATE)
                                                                                AND PKG_ATEND_PAC_UNID.GET_SYS_DT(AP.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                                         AND AP.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(AP.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                                         AND (AP.DT_ALTA IS NULL OR
                                                              AP.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(AP.CD_ESTABELECIMENTO, V_START_DATE))
                                                         AND AG.QT_GLICEMIA IS NOT NULL
                                                         AND AG.QT_GLICEMIA > 0
                                                         AND AG.QT_GLICEMIA <= 1500) A,
                                                     ATEND_PACIENTE_UNIDADE U,
                                                     SETOR_ATENDIMENTO S
                                               WHERE A.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                                                 AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                                                 AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO)
                                                 AND DECODE(P_CD_ESTAB, 0, 0, S.CD_ESTABELECIMENTO) = P_CD_ESTAB
                                                 AND DECODE(V_SETOR, 0, 0, S.CD_SETOR_ATENDIMENTO) = V_SETOR
                                                 AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                                                 AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                                                 AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))         
                                                 AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                              U.DT_ENTRADA_UNIDADE,
                                                                                              U.CD_SETOR_ATENDIMENTO),
                                                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)) >=
                                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)
                                               GROUP BY U.NR_ATENDIMENTO,
                                                        S.CD_ESTABELECIMENTO,
                                                        U.CD_SETOR_ATENDIMENTO,
                                                        A.DT_ENTRADA,
                                                        A.DT_ALTA,
                                                        PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                             U.DT_ENTRADA_UNIDADE,
                                                                                             U.CD_SETOR_ATENDIMENTO)) UN
                                       WHERE (DT_ALTA IS NULL OR DT_ENTRADA_SETOR <= DT_ALTA)
                                         AND (DT_SAIDA_SETOR IS NULL OR DT_SAIDA_SETOR >= DT_ENTRADA)) UN,
                                     ATENDIMENTO_GLICEMIA G
                               WHERE G.NR_ATENDIMENTO = UN.NR_ATENDIMENTO
                                 AND G.QT_GLICEMIA IS NOT NULL
                                 AND G.QT_GLICEMIA > 0
                                 AND G.QT_GLICEMIA <= 1500
                                 AND G.DT_CONTROLE BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(UN.CD_ESTABELECIMENTO, V_START_DATE)
                                                       AND PKG_ATEND_PAC_UNID.GET_SYS_DT(UN.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                 AND G.DT_CONTROLE BETWEEN UN.DT_ENTRADA_SETOR AND NVL(UN.DT_SAIDA_SETOR, G.DT_CONTROLE)
                              UNION
                              SELECT UN.CD_ESTABELECIMENTO,
                                     UN.CD_SETOR_ATENDIMENTO,
                                     UN.NR_ATENDIMENTO,
                                     UN.DT_INI,
                                     UN.DT_FIM,
                                     DT_ENTRADA_UNID,
                                     UN.DT_ENTRADA_SETOR,
                                     UN.DT_SAIDA_SETOR,
                                     I.DT_COLETA DT_EXAME,
                                     I.QT_RESULTADO QT_GLICEMIA
                                FROM (SELECT NR_ATENDIMENTO,
                                             CD_ESTABELECIMENTO,
                                             CD_SETOR_ATENDIMENTO,
                                             DT_INI,
                                             DT_FIM,
                                             DT_ENTRADA_SETOR DT_ENTRADA_UNID,
                                             GREATEST(DT_ENTRADA_SETOR, DT_ENTRADA - .25) DT_ENTRADA_SETOR,
                                             LEAST(NVL(DT_SAIDA_SETOR, NVL2(DT_ALTA, DT_ALTA + .25, NULL)),
                                                   NVL2(DT_ALTA, DT_ALTA + .25, DT_SAIDA_SETOR)) DT_SAIDA_SETOR
                                        FROM (SELECT U.NR_ATENDIMENTO,
                                                     S.CD_ESTABELECIMENTO,
                                                     U.CD_SETOR_ATENDIMENTO,
                                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE) DT_INI,
                                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(V_END_DATE + 1, SYSDATE)) DT_FIM,
                                                     A.DT_ENTRADA,
                                                     A.DT_ALTA,
                                                     MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                                                     PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                          U.DT_ENTRADA_UNIDADE,
                                                                                          U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR                                              
                                                FROM (SELECT DISTINCT AP.NR_ATENDIMENTO,
                                                                      AP.DT_ENTRADA,
                                                                      AP.DT_ALTA
                                                        FROM EXAME_LABORATORIO EL,
                                                             EXAME_LAB_RESULTADO ER,
                                                             EXAME_LAB_RESULT_ITEM EI,
                                                             ATENDIMENTO_PACIENTE AP
                                                       WHERE EL.IE_TIPO_EXAME_REL = 8
                                                         AND EL.NR_SEQ_EXAME = EI.NR_SEQ_EXAME
                                                         AND EI.NR_SEQ_RESULTADO = ER.NR_SEQ_RESULTADO
                                                         AND ER.DT_RESULTADO IS NOT NULL
                                                         AND EI.QT_RESULTADO IS NOT NULL
                                                         AND EI.QT_RESULTADO > 0
                                                         AND EI.QT_RESULTADO <= 1500
                                                         AND NVL(ER.NR_ATENDIMENTO, OBTER_ATENDIMENTO_PRESCR(ER.NR_PRESCRICAO)) = AP.NR_ATENDIMENTO   
                                                         AND EI.DT_COLETA BETWEEN AP.DT_ENTRADA AND NVL(AP.DT_ALTA, EI.DT_COLETA)
                                                         AND (V_PATIENT IS NULL OR V_PATIENT = AP.CD_PESSOA_FISICA)
                                                         AND EI.DT_COLETA BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(AP.CD_ESTABELECIMENTO, V_START_DATE)
                                                                              AND PKG_ATEND_PAC_UNID.GET_SYS_DT(AP.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                                         AND AP.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(AP.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                                         AND (AP.DT_ALTA IS NULL OR
                                                              AP.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(AP.CD_ESTABELECIMENTO, V_START_DATE))) A,
                                                     ATEND_PACIENTE_UNIDADE U,
                                                     SETOR_ATENDIMENTO S
                                               WHERE A.NR_ATENDIMENTO IS NOT NULL
                                                 AND A.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                                                 AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                                                 AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO)
                                                 AND DECODE(P_CD_ESTAB, 0, 0, S.CD_ESTABELECIMENTO) = P_CD_ESTAB
                                                 AND DECODE(V_SETOR, 0, 0, S.CD_SETOR_ATENDIMENTO) = V_SETOR
                                                 AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                                                 AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                                                 AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))         
                                                 AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                              U.DT_ENTRADA_UNIDADE,
                                                                                              U.CD_SETOR_ATENDIMENTO),
                                                         PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)) >=
                                                     PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)
                                               GROUP BY U.NR_ATENDIMENTO,
                                                        S.CD_ESTABELECIMENTO,
                                                        U.CD_SETOR_ATENDIMENTO,
                                                        A.DT_ENTRADA,
                                                        A.DT_ALTA,
                                                        PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                             U.DT_ENTRADA_UNIDADE,
                                                                                             U.CD_SETOR_ATENDIMENTO)) UN
                                       WHERE (DT_ALTA IS NULL OR DT_ENTRADA_SETOR <= DT_ALTA)
                                         AND (DT_SAIDA_SETOR IS NULL OR DT_SAIDA_SETOR >= DT_ENTRADA)) UN,
                                     EXAME_LABORATORIO L,
                                     EXAME_LAB_RESULTADO R,
                                     EXAME_LAB_RESULT_ITEM I
                               WHERE L.IE_TIPO_EXAME_REL = 8
                                 AND NVL(R.NR_ATENDIMENTO, OBTER_ATENDIMENTO_PRESCR(R.NR_PRESCRICAO)) = UN.NR_ATENDIMENTO
                                 AND L.NR_SEQ_EXAME = I.NR_SEQ_EXAME
                                 AND I.NR_SEQ_RESULTADO = R.NR_SEQ_RESULTADO
                                 AND R.DT_RESULTADO IS NOT NULL
                                 AND I.QT_RESULTADO IS NOT NULL
                                 AND I.QT_RESULTADO > 0
                                 AND I.QT_RESULTADO <= 1500
                                 AND I.DT_COLETA BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(UN.CD_ESTABELECIMENTO, V_START_DATE)
                                                     AND PKG_ATEND_PAC_UNID.GET_SYS_DT(UN.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                 AND I.DT_COLETA BETWEEN UN.DT_ENTRADA_SETOR AND NVL(UN.DT_SAIDA_SETOR, I.DT_COLETA)) V
                       GROUP BY V.CD_ESTABELECIMENTO,
                                V.CD_SETOR_ATENDIMENTO,
                                V.NR_ATENDIMENTO,
                                V.DT_INI,
                                V.DT_FIM,
                                V.DT_ENTRADA_UNID,
                                V.DT_ENTRADA_SETOR,
                                V.DT_SAIDA_SETOR,
                                TRUNC(V.DT_EXAME, 'DD')) X) T;
    --
    TYPE T_GLYCEMIC_CONTROL IS TABLE OF C_GLYCEMIC_CONTROL%ROWTYPE;
    R_GLYCEMIC_CONTROL T_GLYCEMIC_CONTROL;
    --
    CURSOR C_GLYCEMIC_CONTROL_ESTAB IS
      SELECT A.CD_ESTABELECIMENTO,
             '' ESTAB_VALUES,
             0 TOTAL_STAYS,
             0 TOTAL_DAYS,
             SUM(NR_EXAME) QT_EXAM,
             COUNT(1) QT_DAYS,
             SUM(IE_DOENCA) NR_DOENCA,
             ROUND(SUM(NR_AVG_GLICEMIA) / COUNT(1), 1) AVG_GLICEMIA,
             SUM(NR_AVG_1) QT_AVG_1,
             SUM(NR_AVG_2) QT_AVG_2,
             SUM(NR_AVG_3) QT_AVG_3,
             SUM(NR_AVG_4) QT_AVG_4,
             ROUND(STDDEV(NR_AVG_GLICEMIA), 1) STD_GLICEMIA,
             SUM(NR_GLICEMIA_1) NR_GLICEMIA_1,
             SUM(NR_GLICEMIA_2) NR_GLICEMIA_2
        FROM W_GLYCEMIC_CONTROL A
       WHERE NR_QUARTER = 4
       GROUP BY A.CD_ESTABELECIMENTO;
    --
    TYPE T_GLYCEMIC_CONTROL_ESTAB IS TABLE OF C_GLYCEMIC_CONTROL_ESTAB%ROWTYPE;
    R_GLYCEMIC_CONTROL_ESTAB T_GLYCEMIC_CONTROL_ESTAB;
    --
    CURSOR C_GLYCEMIC_CONTROL_UNIT(V_QUARTER NUMBER) IS 
      SELECT B.CD_ESTABELECIMENTO,
             B.CD_SETOR_ATENDIMENTO,
             B.QUARTER,
             '' UNIT_VALUES,
             0 TOTAL_STAYS,
             0 TOTAL_DAYS, 
             NVL(SUM(NR_EXAME), 0) QT_EXAM,
             SUM(NVL2(C.NR_ATENDIMENTO, 1, 0)) QT_DAYS,
             NVL(SUM(IE_DOENCA), 0) NR_DOENCA, 
             ROUND(DECODE(SUM(NVL2(C.NR_ATENDIMENTO, 1, 0)), 0, 0, SUM(NR_AVG_GLICEMIA) / SUM(NVL2(C.NR_ATENDIMENTO, 1, 0))), 1) AVG_GLICEMIA,
             NVL(SUM(NR_AVG_1), 0) QT_AVG_1,
             NVL(SUM(NR_AVG_2), 0) QT_AVG_2,
             NVL(SUM(NR_AVG_3), 0) QT_AVG_3,
             NVL(SUM(NR_AVG_4), 0) QT_AVG_4,
             NVL(ROUND(STDDEV(NR_AVG_GLICEMIA), 1), 0) STD_GLICEMIA,
             NVL(SUM(NR_GLICEMIA_1), 0) NR_GLICEMIA_1,
             NVL(SUM(NR_GLICEMIA_2), 0) NR_GLICEMIA_2,
             0 RATE_AVG_1,
             0 RATE_AVG_2,
             0 RATE_AVG_3,
             0 RATE_AVG_4
        FROM (SELECT DISTINCT A.CD_ESTABELECIMENTO,
                              A.CD_SETOR_ATENDIMENTO,
                              V_QUARTER QUARTER
                FROM W_GLYCEMIC_CONTROL A
               WHERE DECODE(P_CD_SETOR, 0, 0, A.CD_SETOR_ATENDIMENTO) = P_CD_SETOR) B
        LEFT JOIN W_GLYCEMIC_CONTROL C 
               ON B.QUARTER = C.NR_QUARTER
              AND B.CD_SETOR_ATENDIMENTO = C.CD_SETOR_ATENDIMENTO
       GROUP BY B.CD_ESTABELECIMENTO,
                B.CD_SETOR_ATENDIMENTO,
                B.QUARTER;
    --
    TYPE T_GLYCEMIC_CONTROL_UNIT IS TABLE OF C_GLYCEMIC_CONTROL_UNIT%ROWTYPE;
    R_GLYCEMIC_CONTROL_UNIT T_GLYCEMIC_CONTROL_UNIT;
    --  
  BEGIN
    --
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    --
    IF P_TYPE = 1 THEN
      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT 1 QUARTER,
                               TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3) -2, 2, '0') || SUBSTR(PR, -4), 'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3), 2, '0') || SUBSTR(PR, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 2 QUARTER,
                               TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3) -2, 2, '0') || SUBSTR(SEG, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3), 2, '0') || SUBSTR(SEG, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 3 QUARTER,
                               TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3) -2, 2, '0') || SUBSTR(TER, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3), 2, '0') || SUBSTR(TER, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 4 QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE)
      LOOP

        OPEN C_GLYCEMIC_CONTROL(N.INI, N.FIM, CASE WHEN N.QUARTER = 4 THEN 0 ELSE P_CD_SETOR END, NULL);
        LOOP
          FETCH C_GLYCEMIC_CONTROL BULK COLLECT INTO R_GLYCEMIC_CONTROL LIMIT 1000;
        
            BEGIN
              FOR i IN R_GLYCEMIC_CONTROL.FIRST..R_GLYCEMIC_CONTROL.LAST LOOP
                INSERT INTO W_GLYCEMIC_CONTROL (
                  cd_estabelecimento, 
                  cd_setor_atendimento, 
                  nr_quarter, 
                  nr_atendimento, 
                  dt_ini_quarter, 
                  dt_fim_quarter, 
                  dt_entrada_unid, 
                  dt_entrada_setor, 
                  dt_saida_setor, 
                  dt_exame, 
                  nr_exame, 
                  nr_avg_glicemia, 
                  nr_avg_1, 
                  nr_avg_2, 
                  nr_avg_3, 
                  nr_avg_4, 
                  nr_glicemia_1, 
                  nr_glicemia_2,
                  ie_doenca, 
                  cd_diagnostico_admis, 
                  ds_diagnostico_admis)
                VALUES (
                  R_GLYCEMIC_CONTROL(i).CD_ESTABELECIMENTO, -- cd_estabelecimento, 
                  R_GLYCEMIC_CONTROL(i).CD_SETOR_ATENDIMENTO, -- cd_setor_atendimento, 
                  N.QUARTER, -- nr_quarter, 
                  R_GLYCEMIC_CONTROL(i).NR_ATENDIMENTO, -- nr_atendimento, 
                  N.INI, -- dt_ini_quarter, 
                  N.FIM, -- dt_fim_quarter, 
                  R_GLYCEMIC_CONTROL(i).DT_ENTRADA_UNID, -- dt_entrada_unid, 
                  R_GLYCEMIC_CONTROL(i).DT_ENTRADA_SETOR, -- dt_entrada_setor, 
                  R_GLYCEMIC_CONTROL(i).DT_SAIDA_SETOR, -- dt_saida_setor, 
                  R_GLYCEMIC_CONTROL(i).DT_EXAME, -- dt_exame, 
                  R_GLYCEMIC_CONTROL(i).QT_EXAME, -- nr_exame, 
                  R_GLYCEMIC_CONTROL(i).AVG_GLICEMIA, -- nr_avg_glicemia, 
                  R_GLYCEMIC_CONTROL(i).QT_AVG_1, -- nr_avg_1, 
                  R_GLYCEMIC_CONTROL(i).QT_AVG_2, -- nr_avg_2, 
                  R_GLYCEMIC_CONTROL(i).QT_AVG_3, -- nr_avg_3, 
                  R_GLYCEMIC_CONTROL(i).QT_AVG_4, -- nr_avg_4, 
                  R_GLYCEMIC_CONTROL(i).NR_GLICEMIA_1, -- nr_glicemia_1, 
                  R_GLYCEMIC_CONTROL(i).NR_GLICEMIA_2, -- nr_glicemia_2, 
                  R_GLYCEMIC_CONTROL(i).IE_DOENCA, -- ie_doenca,
                  R_GLYCEMIC_CONTROL(i).CD_DIAGNOSTICO_ADMIS, -- cd_diagnostico_admis,
                  R_GLYCEMIC_CONTROL(i).DS_DIAGNOSTICO_ADMIS); -- ds_diagnostico_admis)
              END LOOP;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              NULL;
            END;
            COMMIT;
            
          EXIT WHEN C_GLYCEMIC_CONTROL%NOTFOUND; 
        END LOOP;
        CLOSE C_GLYCEMIC_CONTROL;

      END LOOP;
      
      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT 1 QUARTER,
                               TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3) -2, 2, '0') || SUBSTR(PR, -4), 'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3), 2, '0') || SUBSTR(PR, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 2 QUARTER,
                               TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3) -2, 2, '0') || SUBSTR(SEG, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3), 2, '0') || SUBSTR(SEG, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 3 QUARTER,
                               TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3) -2, 2, '0') || SUBSTR(TER, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3), 2, '0') || SUBSTR(TER, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 4 QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE) 
      LOOP
      
        OPEN C_GLYCEMIC_CONTROL_UNIT(N.QUARTER);
        LOOP
          FETCH C_GLYCEMIC_CONTROL_UNIT BULK COLLECT INTO R_GLYCEMIC_CONTROL_UNIT LIMIT 1000;            

            BEGIN
              FOR i IN R_GLYCEMIC_CONTROL_UNIT.FIRST..R_GLYCEMIC_CONTROL_UNIT.LAST LOOP
                    
                R_GLYCEMIC_CONTROL_UNIT(i).UNIT_VALUES := PKG_ATEND_PAC_UNID.GET_STAY_COUNT_AND_AMOUNT(N.INI, N.FIM, R_GLYCEMIC_CONTROL_UNIT(i).CD_ESTABELECIMENTO, R_GLYCEMIC_CONTROL_UNIT(i).CD_SETOR_ATENDIMENTO); 
                R_GLYCEMIC_CONTROL_UNIT(i).TOTAL_STAYS := TO_NUMBER(REGEXP_SUBSTR(R_GLYCEMIC_CONTROL_UNIT(i).UNIT_VALUES, '[^;]+', 1, 1));
                R_GLYCEMIC_CONTROL_UNIT(i).TOTAL_DAYS := TO_NUMBER(REGEXP_SUBSTR(R_GLYCEMIC_CONTROL_UNIT(i).UNIT_VALUES, '[^;]+', 1, 2));
                
                IF R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS > 0 AND R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_1 > 0 THEN
                  R_GLYCEMIC_CONTROL_UNIT(i).RATE_AVG_1 := ROUND(100 * R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_1 / R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS, 1);
                END IF; 
                
                IF R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS > 0 AND R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_2 > 0 THEN
                  R_GLYCEMIC_CONTROL_UNIT(i).RATE_AVG_2 := ROUND(100 * R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_2 / R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS, 1);
                END IF;
                
                IF R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS > 0 AND R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_3 > 0 THEN
                  R_GLYCEMIC_CONTROL_UNIT(i).RATE_AVG_3 := ROUND(100 * R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_3 / R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS, 1);
                END IF;
                
                IF R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS > 0 AND R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_4 > 0 THEN
                  R_GLYCEMIC_CONTROL_UNIT(i).RATE_AVG_4 := ROUND(100 * R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_4 / R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS, 1);
                END IF;
                
                INSERT INTO W_GLYCEMIC_CONTROL_UNIT (
                  cd_estabelecimento, 
                  cd_setor_atendimento, 
                  nr_quarter, 
                  nr_total_stays, 
                  nr_total_days, 
                  nr_exam, 
                  nr_days, 
                  nr_doenca,
                  nr_avg_glicemia, 
                  nr_avg_1, 
                  nr_avg_2, 
                  nr_avg_3, 
                  nr_avg_4, 
                  nr_std_glicemia, 
                  nr_glicemia_1, 
                  nr_glicemia_2, 
                  nr_rate_avg_1, 
                  nr_rate_avg_2, 
                  nr_rate_avg_3, 
                  nr_rate_avg_4)
                VALUES (
                  R_GLYCEMIC_CONTROL_UNIT(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                  R_GLYCEMIC_CONTROL_UNIT(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento,
                  N.QUARTER, --nr_quarter,
                  R_GLYCEMIC_CONTROL_UNIT(i).TOTAL_STAYS, --nr_total_stays, 
                  ROUND(R_GLYCEMIC_CONTROL_UNIT(i).TOTAL_DAYS, 1), --nr_total_days, 
                  R_GLYCEMIC_CONTROL_UNIT(i).QT_EXAM, --nr_exam, 
                  R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS, --nr_days, 
                  R_GLYCEMIC_CONTROL_UNIT(i).NR_DOENCA, --nr_doenca,
                  R_GLYCEMIC_CONTROL_UNIT(i).AVG_GLICEMIA, --nr_avg_glicemia, 
                  R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_1, --nr_avg_1, 
                  R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_2, --nr_avg_2, 
                  R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_3, --nr_avg_3, 
                  R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_4, --nr_avg_4, 
                  R_GLYCEMIC_CONTROL_UNIT(i).STD_GLICEMIA, --nr_std_glicemia, 
                  R_GLYCEMIC_CONTROL_UNIT(i).NR_GLICEMIA_1, --nr_glicemia_1, 
                  R_GLYCEMIC_CONTROL_UNIT(i).NR_GLICEMIA_2, --nr_glicemia_2
                  R_GLYCEMIC_CONTROL_UNIT(i).RATE_AVG_1, --nr_rate_avg_1,
                  R_GLYCEMIC_CONTROL_UNIT(i).RATE_AVG_2, --nr_rate_avg_2,
                  R_GLYCEMIC_CONTROL_UNIT(i).RATE_AVG_3, --nr_rate_avg_3,
                  R_GLYCEMIC_CONTROL_UNIT(i).RATE_AVG_4); --nr_rate_avg_4)
              END LOOP;
                  
            EXCEPTION WHEN NO_DATA_FOUND THEN 
              NULL;
            END;
            COMMIT;
          EXIT WHEN C_GLYCEMIC_CONTROL_UNIT%NOTFOUND; 
        END LOOP;
        CLOSE C_GLYCEMIC_CONTROL_UNIT;
     
        IF N.QUARTER = 4 THEN
          OPEN C_GLYCEMIC_CONTROL_ESTAB;
          LOOP
            FETCH C_GLYCEMIC_CONTROL_ESTAB BULK COLLECT INTO R_GLYCEMIC_CONTROL_ESTAB LIMIT 1000;            
              BEGIN
                FOR i IN R_GLYCEMIC_CONTROL_ESTAB.FIRST..R_GLYCEMIC_CONTROL_ESTAB.LAST LOOP
                      
                  R_GLYCEMIC_CONTROL_ESTAB(i).ESTAB_VALUES := PKG_ATEND_PAC_UNID.GET_STAY_COUNT_AND_AMOUNT(N.INI, N.FIM, R_GLYCEMIC_CONTROL_ESTAB(i).CD_ESTABELECIMENTO, 0); 
                  R_GLYCEMIC_CONTROL_ESTAB(i).TOTAL_STAYS := TO_NUMBER(REGEXP_SUBSTR(R_GLYCEMIC_CONTROL_ESTAB(i).ESTAB_VALUES, '[^;]+', 1, 1));
                  R_GLYCEMIC_CONTROL_ESTAB(i).TOTAL_DAYS := TO_NUMBER(REGEXP_SUBSTR(R_GLYCEMIC_CONTROL_ESTAB(i).ESTAB_VALUES, '[^;]+', 1, 2));
                       
                  INSERT INTO W_GLYCEMIC_CONTROL_ESTAB (
                    cd_estabelecimento, 
                    nr_total_stays, 
                    nr_total_days, 
                    nr_exam, 
                    nr_days, 
                    nr_doenca,
                    nr_avg_glicemia, 
                    nr_avg_1, 
                    nr_avg_2, 
                    nr_avg_3, 
                    nr_avg_4, 
                    nr_std_glicemia, 
                    nr_glicemia_1, 
                    nr_glicemia_2, 
                    nr_rate_avg_1, 
                    nr_rate_avg_2, 
                    nr_rate_avg_3, 
                    nr_rate_avg_4)
                  VALUES (
                    R_GLYCEMIC_CONTROL_ESTAB(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).TOTAL_STAYS, --nr_total_stays, 
                    ROUND(R_GLYCEMIC_CONTROL_ESTAB(i).TOTAL_DAYS, 1), --nr_total_days, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).QT_EXAM, --nr_exam, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).QT_DAYS, --nr_days, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).NR_DOENCA, --nr_doenca,
                    R_GLYCEMIC_CONTROL_ESTAB(i).AVG_GLICEMIA, --nr_avg_glicemia, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).QT_AVG_1, --nr_avg_1, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).QT_AVG_2, --nr_avg_2, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).QT_AVG_3, --nr_avg_3, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).QT_AVG_4, --nr_avg_4, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).STD_GLICEMIA, --nr_std_glicemia, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).NR_GLICEMIA_1, --nr_glicemia_1, 
                    R_GLYCEMIC_CONTROL_ESTAB(i).NR_GLICEMIA_2, --nr_glicemia_2
                    ROUND(100 * R_GLYCEMIC_CONTROL_ESTAB(i).QT_AVG_1 / R_GLYCEMIC_CONTROL_ESTAB(i).QT_DAYS, 1), --nr_rate_avg_1,
                    ROUND(100 * R_GLYCEMIC_CONTROL_ESTAB(i).QT_AVG_2 / R_GLYCEMIC_CONTROL_ESTAB(i).QT_DAYS, 1), --nr_rate_avg_2,
                    ROUND(100 * R_GLYCEMIC_CONTROL_ESTAB(i).QT_AVG_3 / R_GLYCEMIC_CONTROL_ESTAB(i).QT_DAYS, 1), --nr_rate_avg_3,
                    ROUND(100 * R_GLYCEMIC_CONTROL_ESTAB(i).QT_AVG_4 / R_GLYCEMIC_CONTROL_ESTAB(i).QT_DAYS, 1)); --nr_rate_avg_4)

                END LOOP;
                    
              EXCEPTION WHEN NO_DATA_FOUND THEN 
                NULL;
              END;
              COMMIT;
            EXIT WHEN C_GLYCEMIC_CONTROL_ESTAB%NOTFOUND; 
          END LOOP;
          CLOSE C_GLYCEMIC_CONTROL_ESTAB;
                    
        END IF;
        
      END LOOP;

    ELSE

      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT P_QUARTER QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE)
      LOOP

        OPEN C_GLYCEMIC_CONTROL(N.INI, N.FIM, P_CD_SETOR, CASE WHEN P_TYPE = 2 THEN NULL ELSE P_PATIENT END);
        LOOP
          FETCH C_GLYCEMIC_CONTROL BULK COLLECT INTO R_GLYCEMIC_CONTROL LIMIT 1000;

          BEGIN
              FOR i IN R_GLYCEMIC_CONTROL.FIRST..R_GLYCEMIC_CONTROL.LAST LOOP
                INSERT INTO W_GLYCEMIC_CONTROL (
                  cd_estabelecimento, 
                  cd_setor_atendimento, 
                  nr_quarter, 
                  nr_atendimento, 
                  dt_ini_quarter, 
                  dt_fim_quarter, 
                  dt_entrada_unid, 
                  dt_entrada_setor, 
                  dt_saida_setor, 
                  dt_exame, 
                  nr_exame, 
                  nr_avg_glicemia, 
                  nr_avg_1, 
                  nr_avg_2, 
                  nr_avg_3, 
                  nr_avg_4, 
                  nr_glicemia_1, 
                  nr_glicemia_2, 
                  ie_doenca,
                  cd_diagnostico_admis, 
                  ds_diagnostico_admis)
                VALUES (
                  R_GLYCEMIC_CONTROL(i).CD_ESTABELECIMENTO, -- cd_estabelecimento, 
                  R_GLYCEMIC_CONTROL(i).CD_SETOR_ATENDIMENTO, -- cd_setor_atendimento, 
                  P_QUARTER, -- quarter, 
                  R_GLYCEMIC_CONTROL(i).NR_ATENDIMENTO, -- nr_atendimento, 
                  N.INI, -- dt_ini_quarter, 
                  N.FIM, -- dt_fim_quarter, 
                  R_GLYCEMIC_CONTROL(i).DT_ENTRADA_UNID, -- dt_entrada_unid, 
                  R_GLYCEMIC_CONTROL(i).DT_ENTRADA_SETOR, -- dt_entrada_setor, 
                  R_GLYCEMIC_CONTROL(i).DT_SAIDA_SETOR, -- dt_saida_setor, 
                  R_GLYCEMIC_CONTROL(i).DT_EXAME, -- dt_exame, 
                  R_GLYCEMIC_CONTROL(i).QT_EXAME, -- nr_exame, 
                  R_GLYCEMIC_CONTROL(i).AVG_GLICEMIA, -- nr_avg_glicemia, 
                  R_GLYCEMIC_CONTROL(i).QT_AVG_1, -- nr_avg_1, 
                  R_GLYCEMIC_CONTROL(i).QT_AVG_2, -- nr_avg_2, 
                  R_GLYCEMIC_CONTROL(i).QT_AVG_3, -- nr_avg_3, 
                  R_GLYCEMIC_CONTROL(i).QT_AVG_4, -- nr_avg_4, 
                  R_GLYCEMIC_CONTROL(i).NR_GLICEMIA_1, -- nr_glicemia_1, 
                  R_GLYCEMIC_CONTROL(i).NR_GLICEMIA_2, -- nr_glicemia_2, 
                  R_GLYCEMIC_CONTROL(i).IE_DOENCA, -- ie_doenca,
                  R_GLYCEMIC_CONTROL(i).CD_DIAGNOSTICO_ADMIS, -- cd_diagnostico_admis,
                  R_GLYCEMIC_CONTROL(i).DS_DIAGNOSTICO_ADMIS); -- ds_diagnostico_admis)
              END LOOP;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              NULL;
            END;
            COMMIT;
            
          EXIT WHEN C_GLYCEMIC_CONTROL%NOTFOUND; 
        END LOOP;
        CLOSE C_GLYCEMIC_CONTROL;      

        IF P_TYPE = 2 THEN
          OPEN C_GLYCEMIC_CONTROL_UNIT(N.QUARTER);
          LOOP
            FETCH C_GLYCEMIC_CONTROL_UNIT BULK COLLECT INTO R_GLYCEMIC_CONTROL_UNIT LIMIT 1000;            

              BEGIN
                FOR i IN R_GLYCEMIC_CONTROL_UNIT.FIRST..R_GLYCEMIC_CONTROL_UNIT.LAST LOOP
                        
                  R_GLYCEMIC_CONTROL_UNIT(i).UNIT_VALUES := PKG_ATEND_PAC_UNID.GET_STAY_COUNT_AND_AMOUNT(N.INI, N.FIM, R_GLYCEMIC_CONTROL_UNIT(i).CD_ESTABELECIMENTO, R_GLYCEMIC_CONTROL_UNIT(i).CD_SETOR_ATENDIMENTO); 
                  R_GLYCEMIC_CONTROL_UNIT(i).TOTAL_STAYS := TO_NUMBER(REGEXP_SUBSTR(R_GLYCEMIC_CONTROL_UNIT(i).UNIT_VALUES, '[^;]+', 1, 1));
                  R_GLYCEMIC_CONTROL_UNIT(i).TOTAL_DAYS := TO_NUMBER(REGEXP_SUBSTR(R_GLYCEMIC_CONTROL_UNIT(i).UNIT_VALUES, '[^;]+', 1, 2));
                         
                  INSERT INTO W_GLYCEMIC_CONTROL_UNIT (
                    cd_estabelecimento, 
                    cd_setor_atendimento, 
                    nr_quarter, 
                    nr_total_stays, 
                    nr_total_days, 
                    nr_exam, 
                    nr_days, 
                    nr_doenca,
                    nr_avg_glicemia, 
                    nr_avg_1, 
                    nr_avg_2, 
                    nr_avg_3, 
                    nr_avg_4, 
                    nr_std_glicemia, 
                    nr_glicemia_1, 
                    nr_glicemia_2)
                  VALUES (
                    R_GLYCEMIC_CONTROL_UNIT(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                    R_GLYCEMIC_CONTROL_UNIT(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento,
                    N.QUARTER, --nr_quarter,
                    R_GLYCEMIC_CONTROL_UNIT(i).TOTAL_STAYS, --nr_total_stays, 
                    ROUND(R_GLYCEMIC_CONTROL_UNIT(i).TOTAL_DAYS, 1), --nr_total_days, 
                    R_GLYCEMIC_CONTROL_UNIT(i).QT_EXAM, --nr_exam, 
                    R_GLYCEMIC_CONTROL_UNIT(i).QT_DAYS, --nr_days, 
                    R_GLYCEMIC_CONTROL_UNIT(i).NR_DOENCA, --nr_doenca,
                    R_GLYCEMIC_CONTROL_UNIT(i).AVG_GLICEMIA, --nr_avg_glicemia, 
                    R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_1, --nr_avg_1, 
                    R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_2, --nr_avg_2, 
                    R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_3, --nr_avg_3, 
                    R_GLYCEMIC_CONTROL_UNIT(i).QT_AVG_4, --nr_avg_4, 
                    R_GLYCEMIC_CONTROL_UNIT(i).STD_GLICEMIA, --nr_std_glicemia, 
                    R_GLYCEMIC_CONTROL_UNIT(i).NR_GLICEMIA_1, --nr_glicemia_1, 
                    R_GLYCEMIC_CONTROL_UNIT(i).NR_GLICEMIA_2); --nr_glicemia_2

                END LOOP;
                      
              EXCEPTION WHEN NO_DATA_FOUND THEN 
                NULL;
              END;
              COMMIT;
            EXIT WHEN C_GLYCEMIC_CONTROL_UNIT%NOTFOUND; 
          END LOOP;
          CLOSE C_GLYCEMIC_CONTROL_UNIT;

        END IF;
      
      END LOOP;
      
    END IF;

  END PRE_PROCESS;
  -- 
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0,
                P_YEAR       IN NUMBER,
                P_QUARTER    IN NUMBER DEFAULT 1,
                P_TYPE       IN NUMBER DEFAULT 1,
                P_PATIENT    IN VARCHAR2) IS
  BEGIN  
    TABLE_CLEANING;
    PRE_PROCESS(P_START_DATE, LEAST(P_END_DATE, TRUNC(SYSDATE, 'DD')), P_CD_ESTAB, P_CD_SETOR, P_YEAR, P_QUARTER, P_TYPE, P_PATIENT);
  END RUN;
END PKG_GLYCEMIC_CONTROL;
/
