create or replace
package	ptu_retorno_benef_a200_90_pck is

/* ESTA PACKAGE SOMENTE DEVE SER USADA PARA GERAR O ARQUIVO DO A200 NA VERS�O 9.0 DO PTU */
	
	procedure gerar_arquivo_a200(	nr_seq_retorno_p		number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number,
					ds_erro_p		out	varchar2,
					ds_nome_arq_p 		out	varchar2,
					ds_caminho_rede_p	out	varchar2);					
	
end ptu_retorno_benef_a200_90_pck;
/

create or replace 
package body ptu_retorno_benef_a200_90_pck is

nm_usuario_w		usuario.nm_usuario%type;
cd_estabelecimento_w	number(10);
nm_arquivo_w		varchar2(255);
nr_sequencial_arq_w	pls_integer := 0;
ds_arquivo_w		clob;

/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

procedure incluir_linha_arq(	tp_reg_p	varchar2,
				ds_conteudo_p	varchar2,
				arq_texto_p	utl_file.file_type) as

ds_linha_w	varchar2(2000);
				
begin
nr_sequencial_arq_w := nr_sequencial_arq_w + 1;

ds_linha_w	:= lpad(nr_sequencial_arq_w,8,0) || tp_reg_p || ds_conteudo_p || chr(13);
ds_arquivo_w	:= ds_arquivo_w || ds_linha_w;	

utl_file.put_line(arq_texto_p, ds_linha_w);	
utl_file.fflush(arq_texto_p);
end;

/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

procedure processar_arquivo(	nr_seq_retorno_p	number,
				arq_texto_p		utl_file.file_type) as
				
ds_conteudo_w		varchar2(4000);
ds_hash_w		varchar2(32);
	
Cursor C01 (nr_seq_retorno_pc 	ptu_retorno_movimentacao.nr_sequencia%type) is
	select	*
	from	ptu_retorno_movimentacao_v
	where	nr_seq_retorno = nr_seq_retorno_pc
	order by 
		cd_unimed_ori, 
		cd_unimed;

begin
nr_sequencial_arq_w	:= 0;	

for r_C01_w in C01(nr_seq_retorno_p) loop
	ds_conteudo_w := '';
	
	if (r_C01_w.tp_registro = 1) then
		-- R201 - Header
		ds_conteudo_w :=	lpad(r_C01_w.cd_unimed_des,4,0) || lpad(r_C01_w.cd_unimed_ori,4,0) || to_char(r_C01_w.dt_geracao,'YYYYMMDD') ||
					lpad(r_C01_w.ie_tipo_mov,1,0) || to_char(r_C01_w.dt_mov_ini,'YYYYMMDD') || to_char(r_C01_w.dt_mov_final,'YYYYMMDD') || '07' || '  ';			
		incluir_linha_arq('201', ds_conteudo_w, arq_texto_p);
	elsif (r_C01_w.tp_registro = 2) then
		-- R202 - Benefici�rio
		ds_conteudo_w := 	lpad(r_C01_w.cd_unimed,4,0) || '   ' || lpad(r_C01_w.id_benef,13,' ') || lpad(r_C01_w.cd_mens_erro,4,0) || 
					lpad(r_C01_w.cd_unimed_cad,4,0) || lpad(r_C01_w.id_benef_cad,13,0) || lpad(r_C01_w.nr_via_carteira,2,0) || 
					to_char(r_C01_w.dt_validade_carteira,'YYYYMMDD');		
		incluir_linha_arq('202', ds_conteudo_w, arq_texto_p);
	elsif (r_C01_w.tp_registro = 3) then
		-- R209 - Trailer
		ds_conteudo_w := 	lpad(r_C01_w.qt_tot_r202,7,0) || lpad(r_C01_w.qt_tot_inclusao,7,0) || lpad(r_C01_w.qt_tot_exclusao,7,0) ||
					lpad(r_C01_w.qt_tot_alteracao,7,0) || lpad(r_C01_w.qt_tot_transf_plano,7,0) || lpad(r_C01_w.qt_tot_recusado,7,0);		
		incluir_linha_arq('209', ds_conteudo_w, arq_texto_p);
	end if;
end loop;

ds_hash_w 	:= pls_hash_ptu_pck.obter_hash_txt(ds_arquivo_w); 
ds_conteudo_w	:= lpad(ds_hash_w,32,' ');
incluir_linha_arq('998',ds_conteudo_w,arq_texto_p);	

update 	ptu_retorno_movimentacao
set 	ds_hash_a200	= ds_hash_w
where 	nr_sequencia 	= nr_seq_retorno_p;
commit;	

end;					

/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

procedure gerar_arquivo_a200(	nr_seq_retorno_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				ds_erro_p		out	varchar2,
				ds_nome_arq_p 		out	varchar2,
				ds_caminho_rede_p	out	varchar2) AS

arq_texto_w			utl_file.file_type;
ds_local_w			varchar2(255) := null;
ds_erro_w			varchar2(255);
nm_arquivo_w			varchar2(255);
ds_caminho_rede_w		varchar2(255) := null;

begin
ds_arquivo_w	:= null;

begin
select	max(ds_local),
	max(ds_local_rede)
into	ds_local_w,
	ds_caminho_rede_w
from	evento_tasy_utl_file
where	cd_evento	= 24 -- Gera��o de Arquivo A200
and 	ie_tipo 	= 'G';
exception
when others then
	ds_local_w 		:= null;
	ds_caminho_rede_w	:= null;
	ds_erro_w 		:= 'Erro ao buscar configura��es UTL_FILE';
end;

if	(ds_erro_w is not null) then
	ds_erro_p := ds_erro_w;
	goto error;
end if;

ds_caminho_rede_p := ds_caminho_rede_w;

begin
select	'V' || substr(nm_arquivo,2,15)
into	nm_arquivo_w
from	ptu_intercambio			a,
	ptu_retorno_movimentacao	b
where	a.nr_sequencia = b.nr_seq_intercambio
and	b.nr_sequencia = nr_seq_retorno_p;
exception
when others then
	nm_arquivo_w := null;
end;

begin
arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'w');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_p  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_p  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_p  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_p  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_p  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_p  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_p  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_p  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_p  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_p  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_p  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_p  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_p  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_p  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_p  := 'Erro desconhecido no package UTL_FILE.';
	end if;
	goto error;	
end;

processar_arquivo(nr_seq_retorno_p,arq_texto_w);

ds_nome_arq_p := nm_arquivo_w;
	
<<error>>
ds_erro_w  := '';	
end;

end ptu_retorno_benef_a200_90_pck;
/