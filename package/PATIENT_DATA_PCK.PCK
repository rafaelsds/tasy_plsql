create or replace package PATIENT_DATA_PCK as

	TYPE PATIENT_DATA_row is record (
		ds_especialidade 			varchar2(200),
		nm_medico 					varchar2(200),
		nr_telef_medico 			varchar2(200),
		ds_email_medico 			varchar2(200),
		ds_setor					varchar2(200),
		ds_unidade 					varchar2(200),
		ds_convenio 				varchar2(200),
		ds_tipo_sangue 				varchar2(200),
		nm_pessoa_fisica 			varchar2(200),
		ds_fator_rh 				varchar2(200),
		ds_estado_civil 			varchar2(200),
		ds_nacionalidade 			varchar2(200),
		ds_grau_instrucao 			varchar2(200),
		ds_cidade_natal 			varchar2(200),
		nr_telefone_celular 		varchar2(200),
		nr_dd_celular 				varchar2(200),
		qt_altura_cm 				varchar2(200),
		nr_telefone 				varchar2(200),
		ds_endereco 				varchar2(200),
		ds_cidade 					varchar2(200),
		ds_email 					varchar2(200),
		ds_profissao 				varchar2(200),
		ds_empresa 					varchar2(200),
		ds_religiao 				varchar2(200),
		ds_fluencia_port 			varchar2(200),
		ds_idioma_adic 				varchar2(200),
		nr_ramal 					varchar2(200),
		nm_responsavel 				varchar2(200),
		nr_crm 						varchar2(200),
		uf_crm 						varchar2(200),
		nm_pai 						varchar2(200),
		nm_mae 						varchar2(200),
		ds_grau_parentesco 			varchar2(200),
		nr_cpf 						varchar2(200),
		nr_identidade 				varchar2(200),
		ds_tipo_convenio 			varchar2(200),
		ie_tipo_prontuario 			varchar2(200),
		nr_tel_concat 				varchar2(200),
		nr_dd_telefone 				varchar2(200),
		ds_plano 					varchar2(200),
		ds_raca_cor 				varchar2(200),
		ds_inicio_vigencia 			varchar2(200),
		ds_pessoa_referencia 		varchar2(200),
		ds_esp_referencia 			varchar2(200),
		ds_conselho_profissional 	varchar2(200),
		ds_pflegegrad 				varchar2(200),
		ds_grau_parentconjuge		varchar2(200),
		ds_tipo_custodia			varchar2(200),
		ie_custodiante				varchar2(200),
		nm_conjuge					varchar2(200),
		nr_telefone_conjuge			varchar2(200),
		nr_tel_mae					varchar2(200),
		nr_tel_pai	    			varchar2(200),
		nr_tel_responsavel			varchar2(200),
		nm_pessoa_responsavel		varchar2(200),
		nr_tel_conjuge				varchar2(200),
		nm_medico_externo		varchar2(200),
		nr_telefone_medico_externo	varchar2(200),
		nm_contato_principal		varchar2(200),
		nr_telefone_principal		varchar2(200),
		ds_notes				 	varchar2(200),
		ds_medicare 			varchar2(200),
		ds_mediexp			varchar2(200)
	);
	
	TYPE PATIENT_DATA_T IS TABLE OF PATIENT_DATA_row;
	
	FUNCTION get_patient_data (nr_atendimento_p 	number,  
							cd_pessoa_fisica_p 		varchar2)
			return PATIENT_DATA_T pipelined;
end PATIENT_DATA_PCK;
/

create or replace package body PATIENT_DATA_PCK as

	FUNCTION get_patient_data (nr_atendimento_p number,
							cd_pessoa_fisica_p 	varchar2) 
		RETURN PATIENT_DATA_T pipelined is
			PATIENT_DATA_row_w			PATIENT_DATA_row;
			ds_especialidade_w			varchar(200);
			nm_medico_w					varchar2(200);	
			nr_telef_medico_w			varchar2(200);
			ds_email_medico_w			varchar2(200);
			ds_setor_w					varchar2(200);
			ds_unidade_w				varchar2(200);
			ds_convenio_w				varchar2(200);
			ds_tipo_sangue_w			varchar2(200);
			nm_pessoa_fisica_w			varchar2(200);
			ds_fator_rh_w				varchar2(200);
			ds_estado_civil_w			varchar2(200);
			ds_nacionalidade_w			varchar2(200); 
			ds_grau_instrucao_w			varchar2(200);
			ds_cidade_natal_w			varchar2(200);
			nr_telefone_celular_w		varchar2(200);
			nr_dd_celular_w     		varchar2(200);
			qt_altura_cm_w				varchar2(200);
			nr_telefone_w				varchar2(200);
			ds_endereco_w				varchar2(200);
			ds_cidade_w					varchar2(200);
			ds_email_w					varchar2(200);
			ds_profissao_w				varchar2(200);
			ds_empresa_w				varchar2(200);
			ds_religiao_w				varchar2(200);
			ds_fluencia_port_w			varchar2(200);
			ds_idioma_adic_w			varchar2(200);
			nr_ramal_w					varchar2(200);
			nm_responsavel_w			varchar2(200);
			nr_crm_w					varchar2(200);
			uf_crm_w					varchar2(200);
			nm_pai_w					varchar2(200);
			nm_mae_w					varchar2(200);
			ds_grau_parentesco_w		varchar2(200);
			nr_cpf_w					varchar2(200);
			nr_identidade_w				varchar2(200);
			ds_tipo_convenio_w			varchar2(200);
			ie_tipo_prontuario_w		varchar2(200);
			nr_tel_concat_w 			varchar2(200);
			nr_dd_telefone_w    		varchar2(200);
			ds_plano_w 	      			varchar2(200);
			ds_raca_cor_w				varchar2(200);
			ds_inicio_vigencia_w		varchar2(200);
			ds_pessoa_referencia_w		varchar2(200);
			ds_esp_referencia_w			varchar2(200);
			ds_conselho_profissional_w 	varchar2(200);
			ds_pflegegrad_w				varchar2(200);
			ds_grau_parentconjuge_w		varchar2(200);
			ds_tipo_custodia_w			varchar2(200);
			ie_custodiante_w			varchar2(200);
			nm_conjuge_w				varchar2(200);
			nr_telefone_conjuge_w		varchar2(200);
			nr_tel_mae_w				varchar2(200);
			nr_tel_pai_w		    	varchar2(200);
			nr_tel_responsavel_w		varchar2(200);
			nm_pessoa_responsavel_w		varchar2(200);
			nr_tel_conjuge_w			varchar2(200);
			nm_medico_externo_w 		varchar2(200);
			nr_telefone_medico_externo_w 	varchar2(200);
			nm_contato_principal_w 		varchar2(200);
			nr_telefone_principal_w 		varchar2(200);			
			ds_notes_w 			varchar2(200);
			ds_medicare_w     		varchar2(200);
			ds_mediexp_w     		varchar2(200);
      ds_especialidade_me_w   varchar2(200);
      ds_municipio_me_w   varchar2(200);
      ds_endereco_compl_me_w   varchar2(200);

		BEGIN
			Obter_dados_paciente_pep(nr_atendimento_p, cd_pessoa_fisica_p, ds_especialidade_w, nm_medico_w, nr_telef_medico_w, ds_email_medico_w, ds_setor_w, ds_unidade_w, ds_convenio_w, 
							ds_tipo_sangue_w, nm_pessoa_fisica_w, ds_fator_rh_w, ds_estado_civil_w, ds_nacionalidade_w, ds_grau_instrucao_w, 
							ds_cidade_natal_w, nr_telefone_celular_w, nr_dd_celular_w, qt_altura_cm_w, nr_telefone_w, ds_endereco_w, 
							ds_cidade_w, ds_email_w, ds_profissao_w, ds_empresa_w, ds_religiao_w, ds_fluencia_port_w, ds_idioma_adic_w, 
							nr_ramal_w, nm_responsavel_w, nr_crm_w, uf_crm_w, nm_pai_w, nm_mae_w, ds_grau_parentesco_w,  nr_cpf_w, 
							nr_identidade_w, ds_tipo_convenio_w, ie_tipo_prontuario_w, nr_tel_concat_w, nr_dd_telefone_w, ds_plano_w, 
							ds_raca_cor_w, ds_inicio_vigencia_w, ds_pessoa_referencia_w, ds_esp_referencia_w, ds_conselho_profissional_w, 
							ds_pflegegrad_w, ds_grau_parentconjuge_w, ds_tipo_custodia_w, ie_custodiante_w, nm_conjuge_w, nr_telefone_conjuge_w, nr_tel_mae_w, nr_tel_pai_w,
							nr_tel_responsavel_w, nm_pessoa_responsavel_w, nr_tel_conjuge_w, nm_medico_externo_w, nr_telefone_medico_externo_w,
							nm_contato_principal_w, nr_telefone_principal_w, ds_notes_w,ds_medicare_w,ds_mediexp_w,ds_especialidade_me_w,ds_municipio_me_w,ds_endereco_compl_me_w);
			
			PATIENT_DATA_row_w.ds_especialidade := ds_especialidade_w;
			PATIENT_DATA_row_w.nm_medico := nm_medico_w;
			PATIENT_DATA_row_w.nr_telef_medico := nr_telef_medico_w;
			PATIENT_DATA_row_w.ds_email_medico := ds_email_medico_w;
			PATIENT_DATA_row_w.ds_setor := ds_setor_w;
			PATIENT_DATA_row_w.ds_unidade := ds_unidade_w;
			PATIENT_DATA_row_w.ds_convenio := ds_convenio_w;
			PATIENT_DATA_row_w.ds_tipo_sangue := ds_tipo_sangue_w;
			PATIENT_DATA_row_w.nm_pessoa_fisica := nm_pessoa_fisica_w;
			PATIENT_DATA_row_w.ds_fator_rh := ds_fator_rh_w;
			PATIENT_DATA_row_w.ds_estado_civil := ds_estado_civil_w;
			PATIENT_DATA_row_w.ds_nacionalidade := ds_nacionalidade_w;
			PATIENT_DATA_row_w.ds_grau_instrucao := ds_grau_instrucao_w;
			PATIENT_DATA_row_w.ds_cidade_natal := ds_cidade_natal_w;
			PATIENT_DATA_row_w.nr_telefone_celular := nr_telefone_celular_w;
			PATIENT_DATA_row_w.nr_dd_celular := nr_dd_celular_w;
			PATIENT_DATA_row_w.qt_altura_cm := qt_altura_cm_w;
			PATIENT_DATA_row_w.nr_telefone := nr_telefone_w;
			PATIENT_DATA_row_w.ds_endereco := ds_endereco_w;
			PATIENT_DATA_row_w.ds_cidade := ds_cidade_w;
			PATIENT_DATA_row_w.ds_email := ds_email_w;
			PATIENT_DATA_row_w.ds_profissao := ds_profissao_w;
			PATIENT_DATA_row_w.ds_empresa := ds_empresa_w;
			PATIENT_DATA_row_w.ds_religiao := ds_religiao_w;
			PATIENT_DATA_row_w.ds_fluencia_port := ds_fluencia_port_w;
			PATIENT_DATA_row_w.ds_idioma_adic := ds_idioma_adic_w;
			PATIENT_DATA_row_w.nr_ramal := nr_ramal_w;
			PATIENT_DATA_row_w.nm_responsavel := nm_responsavel_w;
			PATIENT_DATA_row_w.nr_crm := nr_crm_w;
			PATIENT_DATA_row_w.uf_crm := uf_crm_w;
			PATIENT_DATA_row_w.nm_pai := nm_pai_w;
			PATIENT_DATA_row_w.nm_mae := nm_mae_w;
			PATIENT_DATA_row_w.ds_grau_parentesco := ds_grau_parentesco_w;
			PATIENT_DATA_row_w.nr_cpf := nr_cpf_w;
			PATIENT_DATA_row_w.nr_identidade := nr_identidade_w;
			PATIENT_DATA_row_w.ds_tipo_convenio := ds_tipo_convenio_w;
			PATIENT_DATA_row_w.ie_tipo_prontuario := ie_tipo_prontuario_w;
			PATIENT_DATA_row_w.nr_tel_concat := nr_tel_concat_w;
			PATIENT_DATA_row_w.nr_dd_telefone := nr_dd_telefone_w;
			PATIENT_DATA_row_w.ds_plano := ds_plano_w;
			PATIENT_DATA_row_w.ds_raca_cor := ds_raca_cor_w;
			PATIENT_DATA_row_w.ds_inicio_vigencia := ds_inicio_vigencia_w;
			PATIENT_DATA_row_w.ds_pessoa_referencia := ds_pessoa_referencia_w;
			PATIENT_DATA_row_w.ds_esp_referencia := ds_esp_referencia_w;
			PATIENT_DATA_row_w.ds_conselho_profissional := ds_conselho_profissional_w;
			PATIENT_DATA_row_w.ds_pflegegrad := ds_pflegegrad_w;
			PATIENT_DATA_row_w.ds_grau_parentconjuge 	:= ds_grau_parentconjuge_w;
			PATIENT_DATA_row_w.ds_tipo_custodia 		:= ds_tipo_custodia_w;
			PATIENT_DATA_row_w.ie_custodiante 			:= ie_custodiante_w;
			PATIENT_DATA_row_w.nm_conjuge 				:= nm_conjuge_w;
			PATIENT_DATA_row_w.nr_telefone_conjuge 		:= nr_telefone_conjuge_w;
			PATIENT_DATA_row_w.nm_medico_externo		:= nm_medico_externo_w;
			PATIENT_DATA_row_w.nr_telefone_medico_externo	:= nr_telefone_medico_externo_w;
			PATIENT_DATA_row_w.nm_contato_principal		:= nm_contato_principal_w;
			PATIENT_DATA_row_w.nr_telefone_principal	:= nr_telefone_principal_w;			
			PATIENT_DATA_row_w.ds_notes := ds_notes_w;
			PATIENT_DATA_row_w.ds_medicare := ds_medicare_w;
			PATIENT_DATA_row_w.ds_mediexp := ds_mediexp_w;
			pipe row(PATIENT_DATA_row_w);
		END;
end PATIENT_DATA_PCK;
/