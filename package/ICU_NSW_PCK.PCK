create or replace package ICU_NSW_PCK as

type t_objeto_row is record (
	row_number_report number,
  column_1_desc varchar2(255),
  column_1 varchar2(255),
  column_2 varchar2(255),
  column_3 varchar2(255),
  column_4 varchar2(255),
  column_5 varchar2(255),
  column_6 varchar2(255),
  column_7 varchar2(255));

type t_objeto_row_data is table of t_objeto_row;

function obter_dados (	nr_sequencia_p	number ) return t_objeto_row_data pipelined;

end ICU_NSW_PCK;
/
create or replace package body ICU_NSW_PCK as

t_objeto_row_w			t_objeto_row;
t_objeto_row_data_w t_objeto_row_data;

function obter_dados(	nr_sequencia_p	number) return t_objeto_row_data pipelined is
cursor c01 is
select 1 as row_number_report ,column_1_desc,column_1,column_2,column_3,column_4,column_5,column_6,column_7 from (SELECT 1  row_number_report ,'Date - (Write date at top of each column) / Day' column_1_desc,
  TO_CHAR(NVL(MAX(x.date_1), ''),'dd/mm/yyyy') column_1,
  TO_CHAR(NVL(MAX(x.date_2), ''),'dd/mm/yyyy') column_2,
  TO_CHAR(NVL(MAX(x.date_3), ''),'dd/mm/yyyy') column_3,
  TO_CHAR(NVL(MAX(x.date_4), ''),'dd/mm/yyyy') column_4,
  TO_CHAR(NVL(MAX(x.date_5), ''),'dd/mm/yyyy') column_5,
  TO_CHAR(NVL(MAX(x.date_6), ''),'dd/mm/yyyy') column_6,
  TO_CHAR(NVL(MAX(x.date_7), ''),'dd/mm/yyyy') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS date_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS date_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS date_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS date_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS date_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS date_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS date_7
  FROM
    (SELECT rownum id,
      dt_registration column_name
    FROM
      (SELECT b.dt_registration
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.dt_registration
      )
    ) b
  ) x
UNION
SELECT 2 as row_number_report, 'Interventions          (Pleas Tick each day)' column_1_desc,
  '' column_1,
  '' column_2,
  '' column_3,
  '' column_4,
  '' column_5,
  '' column_6,
  '' column_7
FROM
     (SELECT NULL
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.dt_registration
      )
  
UNION
SELECT 3 as row_number_report  ,'1/2 hourly Glasgow Coma Score (6 - 8 hours)' column_1_desc,
  DECODE(NVL(MAX(x.vl_glasgow_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.vl_glasgow_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.vl_glasgow_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.vl_glasgow_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.vl_glasgow_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.vl_glasgow_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.vl_glasgow_7), ''), 'S', 'YES','N','NO', '') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS vl_glasgow_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS vl_glasgow_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS vl_glasgow_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS vl_glasgow_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS vl_glasgow_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS vl_glasgow_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS vl_glasgow_7
  FROM
    (SELECT rownum id,
      IE_GLASGOW column_name
    FROM
      (SELECT b.ie_glasgow
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_glasgow
      )
    ) b
  ) x
UNION
SELECT 4 as row_number_report ,'Acute Haemodialysis' column_1_desc,
  DECODE(NVL(MAX(x.vl_haemodialysis_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.vl_haemodialysis_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.vl_haemodialysis_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.vl_haemodialysis_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.vl_haemodialysis_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.vl_haemodialysis_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.vl_haemodialysis_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS vl_haemodialysis_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS vl_haemodialysis_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS vl_haemodialysis_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS vl_haemodialysis_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS vl_haemodialysis_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS vl_haemodialysis_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS vl_haemodialysis_7
  FROM
    (SELECT rownum id,
      IE_HAEMODIALYSIS column_name
    FROM
      (SELECT b.ie_haemodialysis
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_haemodialysis
      )
    ) b
  ) x
UNION
SELECT 5 as row_number_report ,'CPAP / BiPAP(Excluding Sleep Apnoea)' column_1_desc,
  DECODE(NVL(MAX(x.ie_cpap_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_cpap_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_cpap_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_cpap_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_cpap_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_cpap_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_cpap_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_cpap_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_cpap_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_cpap_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_cpap_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_cpap_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_cpap_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_cpap_7
  FROM
    (SELECT rownum id,
      ie_cpap column_name
    FROM
      (SELECT b.ie_cpap
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_cpap
      )
    ) b
  ) x
UNION
SELECT 6 as row_number_report ,'Intra Aortic Balloon Pump' column_1_desc,
  DECODE(NVL(MAX(x.ie_baloon_pump_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_baloon_pump_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_baloon_pump_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_baloon_pump_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_baloon_pump_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_baloon_pump_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_baloon_pump_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_baloon_pump_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_baloon_pump_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_baloon_pump_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_baloon_pump_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_baloon_pump_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_baloon_pump_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_baloon_pump_7
  FROM
    (SELECT rownum id,
      ie_baloon_pump column_name
    FROM
      (SELECT b.ie_baloon_pump
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_baloon_pump
      )
    ) b
  ) x
UNION
SELECT 7 as row_number_report ,'Massive Blood Transfusion (5 Litres/10 Units)' column_1_desc,
  DECODE(NVL(MAX(x.ie_blood_transfusion_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_blood_transfusion_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_blood_transfusion_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_blood_transfusion_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_blood_transfusion_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_blood_transfusion_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_blood_transfusion_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_blood_transfusion_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_blood_transfusion_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_blood_transfusion_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_blood_transfusion_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_blood_transfusion_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_blood_transfusion_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_blood_transfusion_7
  FROM
    (SELECT rownum id,
      ie_blood_transfusion column_name
    FROM
      (SELECT b.ie_blood_transfusion
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_blood_transfusion
      )
    ) b
  ) x
UNION
SELECT 8 as row_number_report ,'Ventilation' column_1_desc,
  DECODE(NVL(MAX(x.ie_ventilation_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_ventilation_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_ventilation_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_ventilation_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_ventilation_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_ventilation_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_ventilation_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_ventilation_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_ventilation_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_ventilation_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_ventilation_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_ventilation_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_ventilation_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_ventilation_7
  FROM
    (SELECT rownum id,
      ie_ventilation column_name
    FROM
      (SELECT b.ie_ventilation
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_ventilation
      )
    ) b
  ) x
UNION
SELECT 9 as row_number_report,'Arterial Catheter' column_1_desc,
  DECODE(NVL(MAX(x.ie_arterial_catheter_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_arterial_catheter_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_arterial_catheter_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_arterial_catheter_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_arterial_catheter_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_arterial_catheter_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_arterial_catheter_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_arterial_catheter_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_arterial_catheter_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_arterial_catheter_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_arterial_catheter_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_arterial_catheter_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_arterial_catheter_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_arterial_catheter_7
  FROM
    (SELECT rownum id,
      ie_arterial_catheter column_name
    FROM
      (SELECT b.ie_arterial_catheter
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_arterial_catheter
      )
    ) b
  ) x
UNION
SELECT 10 as row_number_report ,'ICP Ventricular Catheter' column_1_desc,
  DECODE(NVL(MAX(x.ie_ventricular_catheter_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_ventricular_catheter_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_ventricular_catheter_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_ventricular_catheter_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_ventricular_catheter_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_ventricular_catheter_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_ventricular_catheter_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_ventricular_catheter_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_ventricular_catheter_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_ventricular_catheter_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_ventricular_catheter_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_ventricular_catheter_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_ventricular_catheter_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_ventricular_catheter_7
  FROM
    (SELECT rownum id,
      ie_ventricular_catheter column_name
    FROM
      (SELECT b.ie_ventricular_catheter
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_ventricular_catheter
      )
    ) b
  ) x
UNION
SELECT 11 as row_number_report, 'Continuous IV Antiarrhythmic' column_1_desc,
  DECODE(NVL(MAX(x.ie_cont_iv_antiarrhythmic_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_cont_iv_antiarrhythmic_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_cont_iv_antiarrhythmic_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_cont_iv_antiarrhythmic_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_cont_iv_antiarrhythmic_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_cont_iv_antiarrhythmic_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_cont_iv_antiarrhythmic_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_cont_iv_antiarrhythmic_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_cont_iv_antiarrhythmic_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_cont_iv_antiarrhythmic_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_cont_iv_antiarrhythmic_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_cont_iv_antiarrhythmic_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_cont_iv_antiarrhythmic_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_cont_iv_antiarrhythmic_7
  FROM
    (SELECT rownum id,
      ie_cont_iv_antiarrhythmic column_name
    FROM
      (SELECT b.ie_cont_iv_antiarrhythmic
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_cont_iv_antiarrhythmic
      )
    ) b
  ) x
UNION
SELECT 12 as row_number_report,'IV Inotropes' column_1_desc,
  DECODE(NVL(MAX(x.ie_iv_inotropes_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_iv_inotropes_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_iv_inotropes_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_iv_inotropes_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_iv_inotropes_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_iv_inotropes_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_iv_inotropes_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_iv_inotropes_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_iv_inotropes_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_iv_inotropes_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_iv_inotropes_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_iv_inotropes_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_iv_inotropes_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_iv_inotropes_7
  FROM
    (SELECT rownum id,
      ie_iv_inotropes column_name
    FROM
      (SELECT b.ie_iv_inotropes
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_iv_inotropes
      )
    ) b
  ) x
UNION
SELECT 13 as row_number_report ,'Swan Ganz Catheter' column_1_desc,
  DECODE(NVL(MAX(x.ie_swan_ganz_catheter_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_swan_ganz_catheter_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_swan_ganz_catheter_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_swan_ganz_catheter_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_swan_ganz_catheter_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_swan_ganz_catheter_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_swan_ganz_catheter_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_swan_ganz_catheter_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_swan_ganz_catheter_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_swan_ganz_catheter_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_swan_ganz_catheter_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_swan_ganz_catheter_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_swan_ganz_catheter_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_swan_ganz_catheter_7
  FROM
    (SELECT rownum id,
      ie_swan_ganz_catheter column_name
    FROM
      (SELECT b.ie_swan_ganz_catheter
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_swan_ganz_catheter
      )
    ) b
  ) x
UNION
SELECT 14 as row_number_report, 'IV Vasodilator' column_1_desc,
  DECODE(NVL(MAX(x.ie_iv_vasodilator_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_iv_vasodilator_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_iv_vasodilator_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_iv_vasodilator_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_iv_vasodilator_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_iv_vasodilator_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_iv_vasodilator_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_iv_vasodilator_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_iv_vasodilator_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_iv_vasodilator_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_iv_vasodilator_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_iv_vasodilator_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_iv_vasodilator_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_iv_vasodilator_7
  FROM
    (SELECT rownum id,
      ie_iv_vasodilator column_name
    FROM
      (SELECT b.ie_iv_vasodilator
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_iv_vasodilator
      )
    ) b
  ) x
UNION
SELECT 15 as row_number_report ,'Temporary Pacing' column_1_desc,
  DECODE(NVL(MAX(x.ie_temporary_pacing_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_temporary_pacing_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_temporary_pacing_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_temporary_pacing_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_temporary_pacing_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_temporary_pacing_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_temporary_pacing_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_temporary_pacing_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_temporary_pacing_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_temporary_pacing_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_temporary_pacing_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_temporary_pacing_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_temporary_pacing_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_temporary_pacing_7
  FROM
    (SELECT rownum id,
      ie_temporary_pacing column_name
    FROM
      (SELECT b.ie_temporary_pacing
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_temporary_pacing
      )
    ) b
  ) x
UNION
SELECT 16 as row_number_report,'ST Segment Cardiac / SpO2 Monitoring' column_1_desc,
  DECODE(NVL(MAX(x.ie_spo2_monitoring_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_spo2_monitoring_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_spo2_monitoring_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_spo2_monitoring_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_spo2_monitoring_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_spo2_monitoring_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_spo2_monitoring_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_spo2_monitoring_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_spo2_monitoring_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_spo2_monitoring_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_spo2_monitoring_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_spo2_monitoring_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_spo2_monitoring_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_spo2_monitoring_7
  FROM
    (SELECT rownum id,
      ie_spo2_monitoring column_name
    FROM
      (SELECT b.ie_spo2_monitoring
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_spo2_monitoring
      )
    ) b
  ) x
UNION
SELECT 17 as row_number_report ,'Other' column_1_desc,
  DECODE(NVL(MAX(x.ie_other_1), ''), 'S', 'YES','N','NO','') column_1,
  DECODE(NVL(MAX(x.ie_other_2), ''), 'S', 'YES','N','NO','') column_2,
  DECODE(NVL(MAX(x.ie_other_3), ''), 'S', 'YES','N','NO','') column_3,
  DECODE(NVL(MAX(x.ie_other_4), ''), 'S', 'YES','N','NO','') column_4,
  DECODE(NVL(MAX(x.ie_other_5), ''), 'S', 'YES','N','NO','') column_5,
  DECODE(NVL(MAX(x.ie_other_6), ''), 'S', 'YES','N','NO','') column_6,
  DECODE(NVL(MAX(x.ie_other_7), ''), 'S', 'YES','N','NO','') column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_other_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_other_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_other_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_other_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_other_5 ,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_other_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_other_7
  FROM
    (SELECT rownum id,
      ie_other column_name
    FROM
      (SELECT b.ie_other
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_other
      )
    ) b
  ) x
UNION
SELECT 18 as row_number_report,'NURSING RATIO (1:1, 1:2, 1:3)   (show ratio each day)' column_1_desc,
  MAX(obter_valor_dominio(9455, x.ie_nursing_ratio_1)) column_1,
  MAX(obter_valor_dominio(9455, x.ie_nursing_ratio_2)) column_2,
  MAX(obter_valor_dominio(9455, x.ie_nursing_ratio_3)) column_3,
  MAX(obter_valor_dominio(9455, x.ie_nursing_ratio_4)) column_4,
  MAX(obter_valor_dominio(9455, x.ie_nursing_ratio_5)) column_5,
  MAX(obter_valor_dominio(9455, x.ie_nursing_ratio_6)) column_6,
  MAX(obter_valor_dominio(9455, x.ie_nursing_ratio_7)) column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_nursing_ratio_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_nursing_ratio_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_nursing_ratio_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_nursing_ratio_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_nursing_ratio_5,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_nursing_ratio_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_nursing_ratio_7
  FROM
    (SELECT rownum id,
      ie_nursing_ratio column_name
    FROM
      (SELECT b.ie_nursing_ratio
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_nursing_ratio
      )
    ) b
  ) x
UNION
SELECT 19 as row_number_report ,'PRINCIPAL PATIENT CATEGORY (A, B, C - show each day)' column_1_desc,
  MAX(x.ie_patient_category_1) column_1,
  MAX(x.ie_patient_category_2) column_2,
  MAX(x.ie_patient_category_3) column_3,
  MAX(x.ie_patient_category_4) column_4,
  MAX(x.ie_patient_category_5) column_5,
  MAX(x.ie_patient_category_6) column_6,
  MAX(x.ie_patient_category_7) column_7
FROM
  (SELECT
    CASE b.id
      WHEN 1
      THEN b.column_name
    END AS ie_patient_category_1,
    CASE b.id
      WHEN 2
      THEN b.column_name
    END AS ie_patient_category_2,
    CASE b.id
      WHEN 3
      THEN b.column_name
    END AS ie_patient_category_3,
    CASE b.id
      WHEN 4
      THEN b.column_name
    END AS ie_patient_category_4,
    CASE b.id
      WHEN 5
      THEN b.column_name
    END AS ie_patient_category_5,
    CASE b.id
      WHEN 6
      THEN b.column_name
    END AS ie_patient_category_6,
    CASE b.id
      WHEN 7
      THEN b.column_name
    END AS ie_patient_category_7
  FROM
    (SELECT rownum id,
      ie_patient_category column_name
    FROM
      (SELECT b.ie_patient_category
      FROM icunsw_daily B,
        icunsw A
      WHERE A.nr_sequencia = B.nr_seq_icunsw
      AND A.nr_sequencia   = nr_sequencia_p
      ORDER BY b.ie_patient_category
      )
    ) b
  ) x
  
  ) order by row_number_report;

begin

for r_c01 in c01 loop
	begin
  t_objeto_row_w.row_number_report := r_c01.row_number_report;
  t_objeto_row_w.column_1_desc := r_c01.column_1_desc; 
  t_objeto_row_w.column_1 := r_c01.column_1;
  t_objeto_row_w.column_2 := r_c01.column_2; 
  t_objeto_row_w.column_3 := r_c01.column_3;
  t_objeto_row_w.column_4 := r_c01.column_4;
  t_objeto_row_w.column_5 := r_c01.column_5;
  t_objeto_row_w.column_6 := r_c01.column_6;
  t_objeto_row_w.column_7 := r_c01.column_7;
  pipe row(t_objeto_row_w);
	end;
end loop;


return ;

end obter_dados;

end ICU_NSW_PCK;
/
