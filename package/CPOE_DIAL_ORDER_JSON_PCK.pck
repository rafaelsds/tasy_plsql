create or replace 
package cpoe_dial_order_json_pck  as
   	
	function get_dial_message(nr_cpoe_dial_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json;
	
	function get_message_clob(nr_cpoe_dial_p		number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob;
	
	procedure getCpoeIntegracaoDial(nr_seq_dial_cpoe_p number, nr_entity_identifier_p out number);

end cpoe_dial_order_json_pck;
/

create or replace 
package body cpoe_dial_order_json_pck as
		
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			varchar2) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;
	
	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			number) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;

	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			date) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,pkg_date_utils.get_isoformat(value_p));
	end if;	
	end add_json_value;
	
	function get_default_message(	nr_cpoe_dial_p	number ) return philips_json is
	
	json_encounter_w		philips_json;
	json_patient_w			philips_json;
	cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
	json_return_w			philips_json;
	
	nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
	
	begin
	
	json_return_w	:= philips_json();
	
	select max(nr_atendimento)
	into nr_atendimento_w
	from cpoe_dialise
	where nr_sequencia = nr_cpoe_dial_p;
	
	SELECT MAX(CD_PESSOA_FISICA)
	INTO CD_PESSOA_FISICA_W
	FROM ATENDIMENTO_PACIENTE
	WHERE NR_ATENDIMENTO = nr_atendimento_w;
	
	json_encounter_w		:= encounter_json_pck.get_encounter(nr_atendimento_w);
	json_patient_w			:= person_json_pck.get_person(cd_pessoa_fisica_w);
	
	json_return_w.put('patientIdentification',json_patient_w.to_json_value);
	json_return_w.put('patientVisit',json_encounter_w.to_json_value);
	
	return json_return_w;
	
	end get_default_message;	
	
  function escapeHL7(hl7_message_p varchar2) return varchar2 is

  escape_char_w                 varchar2(1) := '\';
  field_separator_char_w        varchar2(1) := '|';
  repetition_separator_char_w   varchar2(1) := '~';
  component_separator_char_w    varchar2(1) := '^';
  line_feed_w                   varchar2(1) := chr(10);
  carriage_return_w             varchar2(1) := chr(13);

  ds_escaped_msg_w  varchar2(4000);

  begin

  ds_escaped_msg_w := replace(hl7_message_p, escape_char_w, '\E\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, field_separator_char_w, '\F\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, repetition_separator_char_w, '\R\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, component_separator_char_w, '\S\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, line_feed_w, '\.br\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, carriage_return_w, '\.br\');

  return ds_escaped_msg_w;

  end escapeHL7;

	function get_dial_data(nr_cpoe_dial_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;

	Cursor C01 is
    SELECT NR_SEQUENCIA,
      ie_tipo_dialise,
      DT_LIBERACAO           requested_date_time, 
      adep_obter_desc_info_material(nr_sequencia, 'DI')  desc_info_item,
      DT_LIB_SUSPENSAO       suspended_time, 
      cd_medico ordering_provider_id_number,
      obter_dados_pf(cd_medico,'PNG') ordering_provider_given_name,
      obter_dados_pf(cd_medico,'PNL') ordering_provider_last_name,
      obter_dados_pf(cd_medico,'PNM') ordering_provider_middle_name, 
      Obter_Pessoa_Fisica_Usuario(nm_usuario_susp,'C') ordering_prov_susp_id_number,
      substr(obter_nome_pf(cd_medico),1,80) nm_medico_solicitante,
      Obter_Pessoa_Fisica_Usuario(nm_usuario_nrec,'C') ordering_user_id_number,
      Obter_Pessoa_Fisica_Usuario(nm_usuario_susp,'C') order_prov_susp_id_number,
      QT_VOLUME_CICLO,
      NR_CICLOS, 
      QT_HORA_MIN_DURACAO,
      QT_HORA_MIN_SESSAO,
      QT_TOTAL_CICLOS,
      NR_ETAPAS,
      QT_VOLUME_TOTAL_DI, 
      IE_HEMODIALISE, 
      IE_CATEGORIA, 
      obter_conv_envio('CPOE_DIALISE', 'IE_CATEGORIA', IE_CATEGORIA, 'E')  ie_categoria_code_vale,  -- Intervalo para tipo hemodialise
      obter_conv_envio('CPOE_DIALISE', 'IE_TIPO_PERITONEAL', IE_TIPO_PERITONEAL, 'E')  ie_tipo_peritoneal_code_vale,  -- Intervalo para tipo dialise peritoneal
      IE_TIPO_HEMODIALISE, 
      Obter_Descricao_Dominio(1934,IE_TIPO_HEMODIALISE) DS_TIPO_HEMODIALISE,
      IE_TIPO_PERITONEAL, --Intervalo 
      IE_CATETER          , --Dominio(5466)
      IE_ULTRAFILTRACAO   , --Dominio(1936)
      IE_UNID_VEL_INF     , --Dominio(1954)
      NR_SEQ_MOD_DIALISADOR ,--[HD_MODELO_DIALISADOR]
      NR_SEQ_ULTRA          ,--[HD_PERFIL_ULTRA]
      NR_SEQ_PERFIL_SODIO   ,--[HD_PERFIL_SODIO]
      NR_SEQ_PERFIL_BIC     ,--[HD_PERFIL_BICARBONATO]
      QT_FLUXO_SANGUE,
      QT_ZERO_UM, 
      QT_UM_DOIS_BIC,
      QT_DOIS_TRES_BIC,
      QT_TRES_QUATRO_BIC,
      QT_QUATRO_CINCO_BIC,
      QT_CINCO_SEIS_BIC,
      QT_SODIO,
      QT_BICARBONATO, 
      QT_PESO_POS, 
      QT_PESO_ATUAL,
      QT_PESO_IDEAL, 
      QT_ULTRAFILTRACAO_TOTAL,
      QT_TEMP_SOLUCAO,
      IE_DURACAO, 
      cpoe_obter_dt_suspensao(nr_sequencia,'D') dt_suspensao,
      DT_INICIO, 
      DT_FIM, 
      decode(DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(DS_OBSERVACAO)) DS_OBSERVACAO,
      obter_minutos_hora(QT_HORA_MIN_SESSAO)  HR_QT_HORA_MIN_SESSAO,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'S')  minute_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'E')  minute_code_value,
      OBTER_DESC_UNIDADE_MEDIDA('Min') DESC_UNIDADE_MEDIDA, 
      -- Solucao 1
      IE_TIPO_SOLUCAO     ,--Dominio(1935)
      Obter_Descricao_Dominio(1935, IE_TIPO_SOLUCAO) DS_TIPO_SOLUCAO1,
      IE_BOMBA_INFUSAO,  --Dominio(1537)
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO, 'S')  infusion_pump1_code_system,
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO , 'E')  infusion_pump1_code_vale,
      Obter_Descricao_Dominio(1537,IE_BOMBA_INFUSAO) infusion_pump1_text,
      NR_SEQ_PROTOCOLO ,--[PROTOCOLO_NPT]
      OBTER_DESC_PROT_NPT(NR_SEQ_PROTOCOLO) DESC_PROT1,
      QT_DOSAGEM,
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_INF', IE_UNID_VEL_INF, 'S')  rate_unit_code_system,
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_INF', IE_UNID_VEL_INF, 'E') rate_unit_code_vale,
      Obter_Descricao_Dominio(1954,IE_UNID_VEL_INF) rate_unit_text,
      QT_DOSE_ATAQUE, 
      QT_SOLUCAO_TOTAL, 
      decode(DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(DS_OBSERVACAO)) DS_OBSERVACAO1,
      QT_TEMPO_INFUSAO,
      QT_TEMPO_PERMANENCIA, 
      QT_TEMPO_DRENAGEM,
      QT_VOLUME_SOL,
      -- Componente 1
      QT_DOSE_SOLUC1, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC1),1,200) desc_material_comp1, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1, 'E')    code_comp1_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1, 'S')    code_comp1_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL1)  unidade_medida_comp1_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1, 'S')  unid_med_comp1_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1, 'E')  unid_med_comp1_code_value, 
      -- Componente 2
      QT_DOSE_SOLUC2, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC2),1,200) desc_material_comp2, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2, 'E')    code_comp2_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2, 'S')    code_comp2_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL2)  unidade_medida_comp2_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2, 'S')  unid_med_comp2_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2, 'E')  unid_med_comp2_code_value, 	
      -- Componente 3
      QT_DOSE_SOLUC3, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC3),1,200) desc_material_comp3, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3, 'E')    code_comp3_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3, 'S')    code_comp3_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL3)  unidade_medida_comp3_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3, 'S')  unid_med_comp3_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3, 'E')  unid_med_comp3_code_value, 
      -- Componente 4
      QT_DOSE_SOLUC4, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC4),1,200) desc_material_comp4, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4, 'E')    code_comp4_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4, 'S')    code_comp4_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL4)  unidade_medida_comp4_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4, 'S')  unid_med_comp4_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4, 'E')  unid_med_comp4_code_value, 
      -- Componente 5
      QT_DOSE_SOLUC5, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC5),1,200) desc_material_comp5, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5, 'E')    code_comp5_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5, 'S')    code_comp5_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL5)  unidade_medida_comp5_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5, 'S')  unid_med_comp5_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5, 'E')  unid_med_comp5_code_value, 
      -- Componente 6
      QT_DOSE_SOLUC6, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC6),1,200) desc_material_comp6, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6, 'E')    code_comp6_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6, 'S')    code_comp6_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL6)  unidade_medida_comp6_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6, 'S')  unid_med_comp6_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6, 'E')  unid_med_comp6_code_value, 
      -- Componente 7
      QT_DOSE_SOLUC7, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC7),1,200) desc_material_comp7, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7, 'E')    code_comp7_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7, 'S')    code_comp7_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL7)  unidade_medida_comp7_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7, 'S')  unid_med_comp7_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7, 'E')  unid_med_comp7_code_value,
      -- Solucao 2
      IE_TIPO_SOLUCAO2     , --Dominio(1935)
      Obter_Descricao_Dominio(1935, IE_TIPO_SOLUCAO2) DS_TIPO_SOLUCAO2,
      IE_BOMBA_INFUSAO2    , --Dominio(1537), 
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO2, 'S')  infusion_pump2_code_system,
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO2 , 'E')  infusion_pump2_code_vale,
      Obter_Descricao_Dominio(1537,IE_BOMBA_INFUSAO2) infusion_pump2_text,
      NR_SEQ_PROTOCOLO2 , --[PROTOCOLO_NPT], 
      OBTER_DESC_PROT_NPT(NR_SEQ_PROTOCOLO2) DESC_PROT2,
      QT_DOSAGEM2, 
      IE_UNID_VEL_INF2 , 
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_ INF2', IE_UNID_VEL_INF2, 'S')  rate_unit2_code_system,
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_ INF2', IE_UNID_VEL_INF2, 'E') rate_unit2_code_vale,
      Obter_Descricao_Dominio(1954,IE_UNID_VEL_INF2) rate_unit2_text,
      QT_DOSE_ATAQUE2, 
      QT_SOLUCAO_TOTAL2, 
      decode(DS_ORIENTACAO2,null,'','. ' ||elimina_acentuacao(DS_ORIENTACAO2)) DS_OBSERVACAO2,
      QT_TEMPO_INFUSAO2,
      QT_TEMPO_PERMANENCIA2, 
      QT_TEMPO_DRENAGEM2,
      QT_TEMP_SOLUCAO2, 
      QT_VOLUME_SOL2,
      -- Componente 1
      QT_DOSE_SOLUC1_2, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC1_2),1,200) desc_material_comp1_2, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1_2, 'E')    code_comp1_2_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1_2, 'S')    code_comp1_2_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL1_2)  unidade_medida_comp1_2_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1_2, 'S')  unid_med_comp1_2_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1_2, 'E')  unid_med_comp1_2_code_value, 
      -- Componente 2
      QT_DOSE_SOLUC2_2, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC2_2),1,200) desc_material_comp2_2, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2_2, 'E')    code_comp2_2_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2_2, 'S')    code_comp2_2_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL2_2)  unidade_medida_comp2_2_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2_2, 'S')  unid_med_comp2_2_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2_2, 'E')  unid_med_comp2_2_code_value, 	
      -- Componente 3
      QT_DOSE_SOLUC3_2, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC3_2),1,200) desc_material_comp3_2, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3_2, 'E')    code_comp3_2_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3_2, 'S')    code_comp3_2_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL3_2)  unidade_medida_comp3_2_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3_2, 'S')  unid_med_comp3_2_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3_2, 'E')  unid_med_comp3_2_code_value, 
      -- Componente 4
      QT_DOSE_SOLUC4_2, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC4_2),1,200) desc_material_comp4_2, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4_2, 'E')    code_comp4_2_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4_2, 'S')    code_comp4_2_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL4_2)  unidade_medida_comp4_2_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4_2, 'S')  unid_med_comp4_2_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4_2, 'E')  unid_med_comp4_2_code_value, 
      -- Componente 5
      QT_DOSE_SOLUC5_2, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC5_2),1,200) desc_material_comp5_2, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5_2, 'E')    code_comp5_2_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5_2, 'S')    code_comp5_2_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL5_2)  unidade_medida_comp5_2_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5_2, 'S')  unid_med_comp5_2_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5_2, 'E')  unid_med_comp5_2_code_value, 
      -- Componente 6
      QT_DOSE_SOLUC6_2, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC6_2),1,200) desc_material_comp6_2, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6_2, 'E')    code_comp6_2_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6_2, 'S')    code_comp6_2_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL6_2)  unidade_medida_comp6_2_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6_2, 'S')  unid_med_comp6_2_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6_2, 'E')  unid_med_comp6_2_code_value, 
      -- Componente 7
      QT_DOSE_SOLUC7_2, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC7_2),1,200) desc_material_comp7_2, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7_2, 'E')    code_comp7_2_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7_2, 'S')    code_comp7_2_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL7_2)  unidade_medida_comp7_2_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7_2, 'S')  unid_med_comp7_2_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7_2, 'E')  unid_med_comp7_2_code_value, 
      -- Solucao 3
      IE_TIPO_SOLUCAO3,     --Dominio(1935)
      Obter_Descricao_Dominio(1935, IE_TIPO_SOLUCAO3) DS_TIPO_SOLUCAO3,
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO3, 'S')  infusion_pump3_code_system,
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO3 , 'E')  infusion_pump3_code_vale,
      Obter_Descricao_Dominio(1537,IE_BOMBA_INFUSAO3) infusion_pump3_text,
      IE_BOMBA_INFUSAO3   , -- Dominio(1537), 
      NR_SEQ_PROTOCOLO3 , --[PROTOCOLO_NPT], 
      OBTER_DESC_PROT_NPT(NR_SEQ_PROTOCOLO3) DESC_PROT3,
      QT_DOSAGEM3, 
      IE_UNID_VEL_INF3 , 
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_ INF3', IE_UNID_VEL_INF3, 'S')  rate_unit3_code_system,
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_ INF3', IE_UNID_VEL_INF3, 'E') rate_unit3_code_vale,
      Obter_Descricao_Dominio(1954,IE_UNID_VEL_INF3) rate_unit3_text,
      QT_DOSE_ATAQUE3, 
      QT_SOLUCAO_TOTAL3, 
      decode(DS_ORIENTACAO3,null,'','. ' ||elimina_acentuacao(DS_ORIENTACAO3)) DS_OBSERVACAO3,
      QT_TEMPO_INFUSAO3,
      QT_TEMPO_PERMANENCIA3, 
      QT_TEMPO_DRENAGEM3,
      QT_TEMP_SOLUCAO3, 
      QT_VOLUME_SOL3,
      -- Componente 1
      QT_DOSE_SOLUC1_3, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC1_3),1,200) desc_material_comp1_3, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1_3, 'E')    code_comp1_3_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1_3, 'S')    code_comp1_3_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL1_3)  unidade_medida_comp1_3_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1_3, 'S')  unid_med_comp1_3_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1_3, 'E')  unid_med_comp1_3_code_value, 
      -- Componente 2
      QT_DOSE_SOLUC2_3, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC2_3),1,200) desc_material_comp2_3, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2_3, 'E')    code_comp2_3_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2_3, 'S')    code_comp2_3_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL2_3)  unidade_medida_comp2_3_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2_3, 'S')  unid_med_comp2_3_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2_3, 'E')  unid_med_comp2_3_code_value, 	
      -- Componente 3
      QT_DOSE_SOLUC3_3, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC3_3),1,200) desc_material_comp3_3, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3_3, 'E')    code_comp3_3_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3_3, 'S')    code_comp3_3_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL3_3)  unidade_medida_comp3_3_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3_3, 'S')  unid_med_comp3_3_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3_3, 'E')  unid_med_comp3_3_code_value, 
      -- Componente 4
      QT_DOSE_SOLUC4_3, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC4_3),1,200) desc_material_comp4_3, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4_3, 'E')    code_comp4_3_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4_3, 'S')    code_comp4_3_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL4_3)  unidade_medida_comp4_3_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4_3, 'S')  unid_med_comp4_3_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4_3, 'E')  unid_med_comp4_3_code_value, 
      -- Componente 5
      QT_DOSE_SOLUC5_3, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC5_3),1,200) desc_material_comp5_3, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5_3, 'E')    code_comp5_3_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5_3, 'S')    code_comp5_3_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL5_3)  unidade_medida_comp5_3_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5_3, 'S')  unid_med_comp5_3_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5_3, 'E')  unid_med_comp5_3_code_value, 
      -- Componente 6
      QT_DOSE_SOLUC6_3, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC6_3),1,200) desc_material_comp6_3, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6_3, 'E')    code_comp6_3_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6_3, 'S')    code_comp6_3_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL6_3)  unidade_medida_comp6_3_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6_3, 'S')  unid_med_comp6_3_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6_3, 'E')  unid_med_comp6_3_code_value, 
      -- Componente 7
      QT_DOSE_SOLUC7_3, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC7_3),1,200) desc_material_comp7_3, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7_3, 'E')    code_comp7_3_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7_3, 'S')    code_comp7_3_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL7_3)  unidade_medida_comp7_3_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7_3, 'S')  unid_med_comp7_3_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7_3, 'E')  unid_med_comp7_3_code_value,
      -- Solucao 4
      IE_TIPO_SOLUCAO4,--     Dominio(1935)
      Obter_Descricao_Dominio(1935, IE_TIPO_SOLUCAO4) DS_TIPO_SOLUCAO4,
      IE_BOMBA_INFUSAO4 , -- Dominio(1537) 
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO4, 'S')  infusion_pump4_code_system,
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO4 , 'E')  infusion_pump4_code_vale,
      Obter_Descricao_Dominio(1537,IE_BOMBA_INFUSAO4) infusion_pump4_text,
      NR_SEQ_PROTOCOLO4 , --[PROTOCOLO_NPT], 
      OBTER_DESC_PROT_NPT(NR_SEQ_PROTOCOLO4) DESC_PROT4,
      QT_DOSAGEM4, 
      IE_UNID_VEL_INF4 , 
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_ INF4', IE_UNID_VEL_INF4, 'S')  rate_unit4_code_system,
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_ INF4', IE_UNID_VEL_INF4, 'E')  rate_unit4_code_vale,
      Obter_Descricao_Dominio(1954,IE_UNID_VEL_INF4) rate_unit4_text,
      QT_DOSE_ATAQUE4, 
      QT_SOLUCAO_TOTAL4, 
      decode(DS_ORIENTACAO4,null,'','. ' ||elimina_acentuacao(DS_ORIENTACAO4)) DS_OBSERVACAO4,
      QT_TEMPO_INFUSAO4,
      QT_TEMPO_PERMANENCIA4, 
      QT_TEMPO_DRENAGEM4,
      QT_TEMP_SOLUCAO4, 
      QT_VOLUME_SOL4,
      -- Componente 1
      QT_DOSE_SOLUC1_4, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC1_4),1,200) desc_material_comp1_4, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1_4, 'E')    code_comp1_4_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1_4, 'S')    code_comp1_4_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL1_4)  unidade_medida_comp1_4_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1_4, 'S')  unid_med_comp1_4_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1_4, 'E')  unid_med_comp1_4_code_value, 
      -- Componente 2
      QT_DOSE_SOLUC2_4, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC2_4),1,200) desc_material_comp2_4, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2_4, 'E')    code_comp2_4_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2_4, 'S')    code_comp2_4_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL2_4)  unidade_medida_comp2_4_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2_4, 'S')  unid_med_comp2_4_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2_4, 'E')  unid_med_comp2_4_code_value, 	
      -- Componente 3
      QT_DOSE_SOLUC3_4, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC3_4),1,200) desc_material_comp3_4, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3_4, 'E')    code_comp3_4_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3_4, 'S')    code_comp3_4_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL3_4)  unidade_medida_comp3_4_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3_4, 'S')  unid_med_comp3_4_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3_4, 'E')  unid_med_comp3_4_code_value, 
      -- Componente 4
      QT_DOSE_SOLUC4_4, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC4_4),1,200) desc_material_comp4_4, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4_4, 'E')    code_comp4_4_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4_4, 'S')    code_comp4_4_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL4_4)  unidade_medida_comp4_4_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4_4, 'S')  unid_med_comp4_4_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4_4, 'E')  unid_med_comp4_4_code_value, 
      -- Componente 5
      QT_DOSE_SOLUC5_4, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC5_4),1,200) desc_material_comp5_4, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5_4, 'E')    code_comp5_4_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5_4, 'S')    code_comp5_4_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL5_4)  unidade_medida_comp5_4_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5_4, 'S')  unid_med_comp5_4_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5_4, 'E')  unid_med_comp5_4_code_value, 
      -- Componente 6
      QT_DOSE_SOLUC6_4, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC6_4),1,200) desc_material_comp6_4, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6_4, 'E')    code_comp6_4_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6_4, 'S')    code_comp6_4_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL6_4)  unidade_medida_comp6_4_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6_4, 'S')  unid_med_comp6_4_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6_4, 'E')  unid_med_comp6_4_code_value, 
      -- Componente 7
      QT_DOSE_SOLUC7_4, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC7_4),1,200) desc_material_comp7_4, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7_4, 'E')    code_comp7_4_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7_4, 'S')    code_comp7_4_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL7_4)  unidade_medida_comp7_4_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7_4, 'S')  unid_med_comp7_4_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7_4, 'E')  unid_med_comp7_4_code_value,
	  -- Solucao 5
      IE_TIPO_SOLUCAO5,--     Dominio(1935)
      Obter_Descricao_Dominio(1935, IE_TIPO_SOLUCAO5) DS_TIPO_SOLUCAO5,
      IE_BOMBA_INFUSAO5 , -- Dominio(1537) 
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO5, 'S')  infusion_pump5_code_system,
      obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO5, 'E')  infusion_pump5_code_vale,
      Obter_Descricao_Dominio(1537,IE_BOMBA_INFUSAO5) infusion_pump5_text,
      NR_SEQ_PROTOCOLO5 , --[PROTOCOLO_NPT], 
      OBTER_DESC_PROT_NPT(NR_SEQ_PROTOCOLO5) DESC_PROT5,
      QT_DOSAGEM5, 
      IE_UNID_VEL_INF5 , 
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_ INF5', IE_UNID_VEL_INF5, 'S')  rate_unit5_code_system,
      obter_conv_envio('CPOE_DIALISE', 'IE_UNID_VEL_ INF5', IE_UNID_VEL_INF5, 'E')  rate_unit5_code_vale,
      Obter_Descricao_Dominio(1954,IE_UNID_VEL_INF5) rate_unit5_text,
      QT_DOSE_ATAQUE5, 
      QT_SOLUCAO_TOTAL5, 
      decode(DS_ORIENTACAO5,null,'','. ' ||elimina_acentuacao(DS_ORIENTACAO5)) DS_OBSERVACAO5,
      QT_TEMPO_INFUSAO5,
      QT_TEMPO_PERMANENCIA5, 
      QT_TEMPO_DRENAGEM5,
      QT_TEMP_SOLUCAO5, 
      QT_VOLUME_SOL5,
      -- Componente 1
      QT_DOSE_SOLUC1_5, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC1_5),1,200) desc_material_comp1_5, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1_5, 'E')    code_comp1_5_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC1_5, 'S')    code_comp1_5_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL1_5)  unidade_medida_comp1_5_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1_5, 'S')  unid_med_comp1_5_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL1_5, 'E')  unid_med_comp1_5_code_value, 
      -- Componente 2
      QT_DOSE_SOLUC2_5, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC2_5),1,200) desc_material_comp2_5, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2_5, 'E')    code_comp2_5_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC2_5, 'S')    code_comp2_5_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL2_5)  unidade_medida_comp2_5_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2_5, 'S')  unid_med_comp2_5_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL2_5, 'E')  unid_med_comp2_5_code_value, 	
      -- Componente 3
      QT_DOSE_SOLUC3_5, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC3_5),1,200) desc_material_comp3_5, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3_5, 'E')    code_comp3_5_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC3_5, 'S')    code_comp3_5_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL3_5)  unidade_medida_comp3_5_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3_5, 'S')  unid_med_comp3_5_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL3_5, 'E')  unid_med_comp3_5_code_value, 
      -- Componente 4
      QT_DOSE_SOLUC4_5, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC4_5),1,200) desc_material_comp4_5, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4_5, 'E')    code_comp4_5_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC4_5, 'S')    code_comp4_5_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL4_5)  unidade_medida_comp4_5_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4_5, 'S')  unid_med_comp4_5_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL4_5, 'E')  unid_med_comp4_5_code_value, 
      -- Componente 5
      QT_DOSE_SOLUC5_5, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC5_5),1,200) desc_material_comp5_5, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5_5, 'E')    code_comp5_5_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC5_5, 'S')    code_comp5_5_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL5_5)  unidade_medida_comp5_5_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5_5, 'S')  unid_med_comp5_5_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL5_5, 'E')  unid_med_comp5_5_code_value, 
      -- Componente 6
      QT_DOSE_SOLUC6_5, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC6_5),1,200) desc_material_comp6_5, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6_5, 'E')    code_comp6_5_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC6_5, 'S')    code_comp6_5_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL6_5)  unidade_medida_comp6_5_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6_5, 'S')  unid_med_comp6_5_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL6_5, 'E')  unid_med_comp6_5_code_value, 
      -- Componente 7
      QT_DOSE_SOLUC7_5, 
      substr(OBTER_DESC_material(CD_MAT_SOLUC7_5),1,200) desc_material_comp7_5, 
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7_5, 'E')    code_comp7_5_value,
      obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MAT_SOLUC7_5, 'S')    code_comp7_5_system, 
      obter_desc_unidade_medida(CD_UNID_MED_DOSE_SOL7_5)  unidade_medida_comp7_5_text,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7_5, 'S')  unid_med_comp7_5_code_system,
      obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNID_MED_DOSE_SOL7_5, 'E')  unid_med_comp7_5_code_value
    FROM CPOE_DIALISE
    WHERE NR_SEQUENCIA = nr_cpoe_dial_p;

		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();

    add_json_value(json_item_w, 'nrSequencia', r_c01.NR_SEQUENCIA);
    add_json_value(json_item_w, 'ieTipoDialise', r_c01.IE_TIPO_DIALISE);
    add_json_value(json_item_w, 'requestedDateTime', r_c01.REQUESTED_DATE_TIME);
    add_json_value(json_item_w, 'descInfoItem', r_c01.DESC_INFO_ITEM);
    add_json_value(json_item_w, 'suspendedTime', r_c01.SUSPENDED_TIME);
    add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ORDERING_PROVIDER_ID_NUMBER);
    add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ORDERING_PROVIDER_GIVEN_NAME);
    add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ORDERING_PROVIDER_LAST_NAME);
    add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ORDERING_PROVIDER_MIDDLE_NAME);
    add_json_value(json_item_w, 'orderingProv_susp_id_number', r_c01.ORDERING_PROV_SUSP_ID_NUMBER);
    add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.NM_MEDICO_SOLICITANTE);
    add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ORDERING_USER_ID_NUMBER);
    add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.ORDER_PROV_SUSP_ID_NUMBER);
    add_json_value(json_item_w, 'qtVolumeCiclo', r_c01.QT_VOLUME_CICLO);
    add_json_value(json_item_w, 'nrCiclos', r_c01.NR_CICLOS);
    add_json_value(json_item_w, 'qtHoraMinDuracao', r_c01.QT_HORA_MIN_DURACAO);
    add_json_value(json_item_w, 'qtHoraMinSessao', r_c01.QT_HORA_MIN_SESSAO);
    add_json_value(json_item_w, 'qtTotalCiclos', r_c01.QT_TOTAL_CICLOS);
    add_json_value(json_item_w, 'nrEtapas', r_c01.NR_ETAPAS);
    add_json_value(json_item_w, 'qtVolumeTotalDi', r_c01.QT_VOLUME_TOTAL_DI);
    add_json_value(json_item_w, 'ieHemodialise', r_c01.IE_HEMODIALISE);
    add_json_value(json_item_w, 'ieCategoria', r_c01.IE_CATEGORIA);
    add_json_value(json_item_w, 'ieCategoriaCodeVale', r_c01.IE_CATEGORIA_CODE_VALE);
    add_json_value(json_item_w, 'ieTipoPeritoneal_code_vale', r_c01.IE_TIPO_PERITONEAL_CODE_VALE);
    add_json_value(json_item_w, 'ieTipoHemodialise', r_c01.IE_TIPO_HEMODIALISE);
    add_json_value(json_item_w, 'dsTipoHemodialise', r_c01.DS_TIPO_HEMODIALISE);
    add_json_value(json_item_w, 'ieTipoPeritoneal', r_c01.IE_TIPO_PERITONEAL);
    add_json_value(json_item_w, 'ieCateter', r_c01.IE_CATETER);
    add_json_value(json_item_w, 'ieUltrafiltracao', r_c01.IE_ULTRAFILTRACAO);
    add_json_value(json_item_w, 'ieUnidVelInf', r_c01.IE_UNID_VEL_INF);
    add_json_value(json_item_w, 'nrSeqModDialisador', r_c01.NR_SEQ_MOD_DIALISADOR);
    add_json_value(json_item_w, 'nrSeqUltra', r_c01.NR_SEQ_ULTRA);
    add_json_value(json_item_w, 'nrSeqPerfilSodio', r_c01.NR_SEQ_PERFIL_SODIO);
    add_json_value(json_item_w, 'nrSeqPerfilBic', r_c01.NR_SEQ_PERFIL_BIC);
    add_json_value(json_item_w, 'qtFluxoSangue', r_c01.QT_FLUXO_SANGUE);
    add_json_value(json_item_w, 'qtZeroUm', r_c01.QT_ZERO_UM);
    add_json_value(json_item_w, 'qtUmDoisBic', r_c01.QT_UM_DOIS_BIC);
    add_json_value(json_item_w, 'qtDoisTresBic', r_c01.QT_DOIS_TRES_BIC);
    add_json_value(json_item_w, 'qtTresQuatroBic', r_c01.QT_TRES_QUATRO_BIC);
    add_json_value(json_item_w, 'qtQuatroCincoBic', r_c01.QT_QUATRO_CINCO_BIC);
    add_json_value(json_item_w, 'qtCincoSeisBic', r_c01.QT_CINCO_SEIS_BIC);
    add_json_value(json_item_w, 'qtSodio', r_c01.QT_SODIO);
    add_json_value(json_item_w, 'qtBicarbonato', r_c01.QT_BICARBONATO);
    add_json_value(json_item_w, 'qtPesoPos', r_c01.QT_PESO_POS);
    add_json_value(json_item_w, 'qtPesoAtual', r_c01.QT_PESO_ATUAL);
    add_json_value(json_item_w, 'qtPesoIdeal', r_c01.QT_PESO_IDEAL);
    add_json_value(json_item_w, 'qtUltrafiltracaoTotal', r_c01.QT_ULTRAFILTRACAO_TOTAL);
    add_json_value(json_item_w, 'qtSolucaoTotal', r_c01.QT_SOLUCAO_TOTAL);
    add_json_value(json_item_w, 'qtTempSolucao', r_c01.QT_TEMP_SOLUCAO);
    add_json_value(json_item_w, 'ieDuracao', r_c01.IE_DURACAO);
    add_json_value(json_item_w, 'dtSuspensao', r_c01.DT_SUSPENSAO);
    add_json_value(json_item_w, 'dtInicio', r_c01.DT_INICIO);
    add_json_value(json_item_w, 'dtFim', r_c01.DT_FIM);
    add_json_value(json_item_w, 'dsObservacao', r_c01.DS_OBSERVACAO);
    add_json_value(json_item_w, 'hrQtHoraMinSessao', r_c01.HR_QT_HORA_MIN_SESSAO);
    add_json_value(json_item_w, 'minuteCodeSystem', r_c01.MINUTE_CODE_SYSTEM);
    add_json_value(json_item_w, 'minuteCodeValue', r_c01.MINUTE_CODE_VALUE);
    add_json_value(json_item_w, 'descUnidadeMedida', r_c01.DESC_UNIDADE_MEDIDA);
    -- Solucao 1
    add_json_value(json_item_w, 'ieTipoSolucao', r_c01.IE_TIPO_SOLUCAO);
    add_json_value(json_item_w, 'dsTipoSolucao1', r_c01.DS_TIPO_SOLUCAO1);
    add_json_value(json_item_w, 'ieBombaInfusao', r_c01.IE_BOMBA_INFUSAO);
    add_json_value(json_item_w, 'infusionPump1CodeSystem', r_c01.INFUSION_PUMP1_CODE_SYSTEM);
    add_json_value(json_item_w, 'infusionPump1CodeVale', r_c01.INFUSION_PUMP1_CODE_VALE);
    add_json_value(json_item_w, 'infusionPump1Text', r_c01.INFUSION_PUMP1_TEXT);
    add_json_value(json_item_w, 'nrSeqProtocolo', r_c01.NR_SEQ_PROTOCOLO);
    add_json_value(json_item_w, 'descProt1', nvl(r_c01.DESC_PROT1, r_c01.DESC_INFO_ITEM));
    add_json_value(json_item_w, 'qtDosagem', r_c01.QT_DOSAGEM);
    add_json_value(json_item_w, 'ieUnidVelInf', r_c01.IE_UNID_VEL_INF);
    add_json_value(json_item_w, 'rateUnitCodeSystem', r_c01.RATE_UNIT_CODE_SYSTEM);
    add_json_value(json_item_w, 'rateUnitCodeVale', r_c01.RATE_UNIT_CODE_VALE);
    add_json_value(json_item_w, 'rateUnitText', r_c01.RATE_UNIT_TEXT);
    add_json_value(json_item_w, 'qtDoseAtaque', r_c01.QT_DOSE_ATAQUE);
    add_json_value(json_item_w, 'qtSolucaoTotal', r_c01.QT_SOLUCAO_TOTAL);
    add_json_value(json_item_w, 'dsObservacao1', r_c01.DS_OBSERVACAO1);
    add_json_value(json_item_w, 'qtTempoInfusao', r_c01.QT_TEMPO_INFUSAO);
    add_json_value(json_item_w, 'qtTempoPermanencia', r_c01.QT_TEMPO_PERMANENCIA);
    add_json_value(json_item_w, 'qtTempoDrenagem', r_c01.QT_TEMPO_DRENAGEM);
    add_json_value(json_item_w, 'qtTempSolucao', r_c01.QT_TEMP_SOLUCAO);
    add_json_value(json_item_w, 'qtVolumeSol', r_c01.QT_VOLUME_SOL);
    -- Componente 1
    add_json_value(json_item_w, 'qtDoseSoluc1', r_c01.QT_DOSE_SOLUC1);
    add_json_value(json_item_w, 'descMaterialComp1', r_c01.DESC_MATERIAL_COMP1);
    add_json_value(json_item_w, 'codeComp1Value', r_c01.CODE_COMP1_VALUE);
    add_json_value(json_item_w, 'codeComp1System', r_c01.CODE_COMP1_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp1Text', r_c01.UNIDADE_MEDIDA_COMP1_TEXT);
    add_json_value(json_item_w, 'unidMedComp1CodeSystem', r_c01.UNID_MED_COMP1_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp1CodeValue', r_c01.UNID_MED_COMP1_CODE_VALUE);
    -- Componente 2
    add_json_value(json_item_w, 'qtDoseSoluc2', r_c01.QT_DOSE_SOLUC2);
    add_json_value(json_item_w, 'descMaterialComp2', r_c01.DESC_MATERIAL_COMP2);
    add_json_value(json_item_w, 'codeComp2Value', r_c01.CODE_COMP2_VALUE);
    add_json_value(json_item_w, 'codeComp2System', r_c01.CODE_COMP2_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp2Text', r_c01.UNIDADE_MEDIDA_COMP2_TEXT);
    add_json_value(json_item_w, 'unidMedComp2CodeSystem', r_c01.UNID_MED_COMP2_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp2CodeValue', r_c01.UNID_MED_COMP2_CODE_VALUE);
    -- Componente 3
    add_json_value(json_item_w, 'qtDoseSoluc3', r_c01.QT_DOSE_SOLUC3);
    add_json_value(json_item_w, 'descMaterialComp3', r_c01.DESC_MATERIAL_COMP3);
    add_json_value(json_item_w, 'codeComp3Value', r_c01.CODE_COMP3_VALUE);
    add_json_value(json_item_w, 'codeComp3System', r_c01.CODE_COMP3_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp3Text', r_c01.UNIDADE_MEDIDA_COMP3_TEXT);
    add_json_value(json_item_w, 'unidMedComp3CodeSystem', r_c01.UNID_MED_COMP3_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp3CodeValue', r_c01.UNID_MED_COMP3_CODE_VALUE);
    -- Componente 4
    add_json_value(json_item_w, 'qtDoseSoluc4', r_c01.QT_DOSE_SOLUC4);
    add_json_value(json_item_w, 'descMaterialComp4', r_c01.DESC_MATERIAL_COMP4);
    add_json_value(json_item_w, 'codeComp4Value', r_c01.CODE_COMP4_VALUE);
    add_json_value(json_item_w, 'codeComp4System', r_c01.CODE_COMP4_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp4Text', r_c01.UNIDADE_MEDIDA_COMP4_TEXT);
    add_json_value(json_item_w, 'unidMedComp4CodeSystem', r_c01.UNID_MED_COMP4_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp4CodeValue', r_c01.UNID_MED_COMP4_CODE_VALUE);
    -- Componente 5
    add_json_value(json_item_w, 'qtDoseSoluc5', r_c01.QT_DOSE_SOLUC5);
    add_json_value(json_item_w, 'descMaterialComp5', r_c01.DESC_MATERIAL_COMP5);
    add_json_value(json_item_w, 'codeComp5Value', r_c01.CODE_COMP5_VALUE);
    add_json_value(json_item_w, 'codeComp5System', r_c01.CODE_COMP5_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp5Text', r_c01.UNIDADE_MEDIDA_COMP5_TEXT);
    add_json_value(json_item_w, 'unidMedComp5CodeSystem', r_c01.UNID_MED_COMP5_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp5CodeValue', r_c01.UNID_MED_COMP5_CODE_VALUE);
    -- Componente 6
    add_json_value(json_item_w, 'qtDoseSoluc6', r_c01.QT_DOSE_SOLUC6);
    add_json_value(json_item_w, 'descMaterialComp6', r_c01.DESC_MATERIAL_COMP6);
    add_json_value(json_item_w, 'codeComp6Value', r_c01.CODE_COMP6_VALUE);
    add_json_value(json_item_w, 'codeComp6System', r_c01.CODE_COMP6_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp6Text', r_c01.UNIDADE_MEDIDA_COMP6_TEXT);
    add_json_value(json_item_w, 'unidMedComp6CodeSystem', r_c01.UNID_MED_COMP6_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp6CodeValue', r_c01.UNID_MED_COMP6_CODE_VALUE);
    -- Componente 7
    add_json_value(json_item_w, 'qtDoseSoluc7', r_c01.QT_DOSE_SOLUC7);
    add_json_value(json_item_w, 'descMaterialComp7', r_c01.DESC_MATERIAL_COMP7);
    add_json_value(json_item_w, 'codeComp7Value', r_c01.CODE_COMP7_VALUE);
    add_json_value(json_item_w, 'codeComp7System', r_c01.CODE_COMP7_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp7Text', r_c01.UNIDADE_MEDIDA_COMP7_TEXT);
    add_json_value(json_item_w, 'unidMedComp7CodeSystem', r_c01.UNID_MED_COMP7_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp7CodeValue', r_c01.UNID_MED_COMP7_CODE_VALUE);
    -- Solucao 2
    add_json_value(json_item_w, 'ieTipoSolucao2', r_c01.IE_TIPO_SOLUCAO2);
    add_json_value(json_item_w, 'dsTipoSolucao2', r_c01.DS_TIPO_SOLUCAO2);
    add_json_value(json_item_w, 'ieBombaInfusao2', r_c01.IE_BOMBA_INFUSAO2);
    add_json_value(json_item_w, 'infusionPump2CodeSystem', r_c01.INFUSION_PUMP2_CODE_SYSTEM);
    add_json_value(json_item_w, 'infusionPump2CodeVale', r_c01.INFUSION_PUMP2_CODE_VALE);
    add_json_value(json_item_w, 'infusionPump2Text', r_c01.INFUSION_PUMP2_TEXT);
    add_json_value(json_item_w, 'nrSeqProtocolo2', r_c01.NR_SEQ_PROTOCOLO2);
    add_json_value(json_item_w, 'descProt2', nvl(r_c01.DESC_PROT2, r_c01.DESC_INFO_ITEM));
    add_json_value(json_item_w, 'qtDosagem2', r_c01.QT_DOSAGEM2);
    add_json_value(json_item_w, 'ieUnidVelInf2', r_c01.IE_UNID_VEL_INF2);
    add_json_value(json_item_w, 'rateUnit2CodeSystem', r_c01.RATE_UNIT2_CODE_SYSTEM);
    add_json_value(json_item_w, 'rateUnit2CodeVale', r_c01.RATE_UNIT2_CODE_VALE);
    add_json_value(json_item_w, 'rateUnit2Text', r_c01.RATE_UNIT2_TEXT);
    add_json_value(json_item_w, 'qtDoseAtaque2', r_c01.QT_DOSE_ATAQUE2);
    add_json_value(json_item_w, 'qtSolucaoTotal2', r_c01.QT_SOLUCAO_TOTAL2);
    add_json_value(json_item_w, 'dsObservacao2', r_c01.DS_OBSERVACAO2);
    add_json_value(json_item_w, 'qtTempoInfusao2', r_c01.QT_TEMPO_INFUSAO2);
    add_json_value(json_item_w, 'qtTempoPermanencia2', r_c01.QT_TEMPO_PERMANENCIA2);
    add_json_value(json_item_w, 'qtTempoDrenagem2', r_c01.QT_TEMPO_DRENAGEM2);
    add_json_value(json_item_w, 'qtTempSolucao2', r_c01.QT_TEMP_SOLUCAO2);
    add_json_value(json_item_w, 'qtVolumeSol2', r_c01.QT_VOLUME_SOL2);
    -- Componente 1
    add_json_value(json_item_w, 'qtDoseSoluc12', r_c01.QT_DOSE_SOLUC1_2);
    add_json_value(json_item_w, 'descMaterialComp12', r_c01.DESC_MATERIAL_COMP1_2);
    add_json_value(json_item_w, 'codeComp12Value', r_c01.CODE_COMP1_2_VALUE);
    add_json_value(json_item_w, 'codeComp12System', r_c01.CODE_COMP1_2_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp12Text', r_c01.UNIDADE_MEDIDA_COMP1_2_TEXT);
    add_json_value(json_item_w, 'unidMedComp12CodeSystem', r_c01.UNID_MED_COMP1_2_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp12CodeValue', r_c01.UNID_MED_COMP1_2_CODE_VALUE);
    -- Componente 2
    add_json_value(json_item_w, 'qtDoseSoluc22', r_c01.QT_DOSE_SOLUC2_2);
    add_json_value(json_item_w, 'descMaterialComp22', r_c01.DESC_MATERIAL_COMP2_2);
    add_json_value(json_item_w, 'codeComp22Value', r_c01.CODE_COMP2_2_VALUE);
    add_json_value(json_item_w, 'codeComp22System', r_c01.CODE_COMP2_2_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp22Text', r_c01.UNIDADE_MEDIDA_COMP2_2_TEXT);
    add_json_value(json_item_w, 'unidMedComp22CodeSystem', r_c01.UNID_MED_COMP2_2_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp22CodeValue', r_c01.UNID_MED_COMP2_2_CODE_VALUE);
    -- Componente 3
    add_json_value(json_item_w, 'qtDoseSoluc32', r_c01.QT_DOSE_SOLUC3_2);
    add_json_value(json_item_w, 'descMaterialComp32', r_c01.DESC_MATERIAL_COMP3_2);
    add_json_value(json_item_w, 'codeComp32Value', r_c01.CODE_COMP3_2_VALUE);
    add_json_value(json_item_w, 'codeComp32System', r_c01.CODE_COMP3_2_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp32Text', r_c01.UNIDADE_MEDIDA_COMP3_2_TEXT);
    add_json_value(json_item_w, 'unidMedComp32CodeSystem', r_c01.UNID_MED_COMP3_2_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp32CodeValue', r_c01.UNID_MED_COMP3_2_CODE_VALUE);
    -- Componente 4
    add_json_value(json_item_w, 'qtDoseSoluc42', r_c01.QT_DOSE_SOLUC4_2);
    add_json_value(json_item_w, 'descMaterialComp42', r_c01.DESC_MATERIAL_COMP4_2);
    add_json_value(json_item_w, 'codeComp42Value', r_c01.CODE_COMP4_2_VALUE);
    add_json_value(json_item_w, 'codeComp42System', r_c01.CODE_COMP4_2_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp42Text', r_c01.UNIDADE_MEDIDA_COMP4_2_TEXT);
    add_json_value(json_item_w, 'unidMedComp42CodeSystem', r_c01.UNID_MED_COMP4_2_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp42CodeValue', r_c01.UNID_MED_COMP4_2_CODE_VALUE);
    -- Componente 5
    add_json_value(json_item_w, 'qtDoseSoluc52', r_c01.QT_DOSE_SOLUC5_2);
    add_json_value(json_item_w, 'descMaterialComp52', r_c01.DESC_MATERIAL_COMP5_2);
    add_json_value(json_item_w, 'codeComp52Value', r_c01.CODE_COMP5_2_VALUE);
    add_json_value(json_item_w, 'codeComp52System', r_c01.CODE_COMP5_2_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp52Text', r_c01.UNIDADE_MEDIDA_COMP5_2_TEXT);
    add_json_value(json_item_w, 'unidMedComp52CodeSystem', r_c01.UNID_MED_COMP5_2_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp52CodeValue', r_c01.UNID_MED_COMP5_2_CODE_VALUE);
    -- Componente 6
    add_json_value(json_item_w, 'qtDoseSoluc62', r_c01.QT_DOSE_SOLUC6_2);
    add_json_value(json_item_w, 'descMaterialComp62', r_c01.DESC_MATERIAL_COMP6_2);
    add_json_value(json_item_w, 'codeComp62Value', r_c01.CODE_COMP6_2_VALUE);
    add_json_value(json_item_w, 'codeComp62System', r_c01.CODE_COMP6_2_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp62Text', r_c01.UNIDADE_MEDIDA_COMP6_2_TEXT);
    add_json_value(json_item_w, 'unidMedComp62CodeSystem', r_c01.UNID_MED_COMP6_2_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp62CodeValue', r_c01.UNID_MED_COMP6_2_CODE_VALUE);
    -- Componente 7
    add_json_value(json_item_w, 'qtDoseSoluc72', r_c01.QT_DOSE_SOLUC7_2);
    add_json_value(json_item_w, 'descMaterialComp72', r_c01.DESC_MATERIAL_COMP7_2);
    add_json_value(json_item_w, 'codeComp72Value', r_c01.CODE_COMP7_2_VALUE);
    add_json_value(json_item_w, 'codeComp72System', r_c01.CODE_COMP7_2_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp72Text', r_c01.UNIDADE_MEDIDA_COMP7_2_TEXT);
    add_json_value(json_item_w, 'unidMedComp72CodeSystem', r_c01.UNID_MED_COMP7_2_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp72CodeValue', r_c01.UNID_MED_COMP7_2_CODE_VALUE);
    -- Solucao 3
    add_json_value(json_item_w, 'ieTipoSolucao3', r_c01.IE_TIPO_SOLUCAO3);
    add_json_value(json_item_w, 'dsTipoSolucao3', r_c01.DS_TIPO_SOLUCAO3);
    add_json_value(json_item_w, 'infusionPump3CodeSystem', r_c01.INFUSION_PUMP3_CODE_SYSTEM);
    add_json_value(json_item_w, 'infusionPump3CodeVale', r_c01.INFUSION_PUMP3_CODE_VALE);
    add_json_value(json_item_w, 'infusionPump3Text', r_c01.INFUSION_PUMP3_TEXT);
    add_json_value(json_item_w, 'ieBombaInfusao3', r_c01.IE_BOMBA_INFUSAO3);
    add_json_value(json_item_w, 'nrSeqProtocolo3', r_c01.NR_SEQ_PROTOCOLO3);
    add_json_value(json_item_w, 'descProt3', nvl(r_c01.DESC_PROT3, r_c01.DESC_INFO_ITEM));
    add_json_value(json_item_w, 'qtDosagem3', r_c01.QT_DOSAGEM3);
    add_json_value(json_item_w, 'ieUnidVelInf3', r_c01.IE_UNID_VEL_INF3);
    add_json_value(json_item_w, 'rateUnit3CodeSystem', r_c01.RATE_UNIT3_CODE_SYSTEM);
    add_json_value(json_item_w, 'rateUnit3CodeVale', r_c01.RATE_UNIT3_CODE_VALE);
    add_json_value(json_item_w, 'rateUnit3Text', r_c01.RATE_UNIT3_TEXT);
    add_json_value(json_item_w, 'qtDoseAtaque3', r_c01.QT_DOSE_ATAQUE3);
    add_json_value(json_item_w, 'qtSolucaoTotal3', r_c01.QT_SOLUCAO_TOTAL3);
    add_json_value(json_item_w, 'dsObservacao3', r_c01.DS_OBSERVACAO3);
    add_json_value(json_item_w, 'qtTempoInfusao3', r_c01.QT_TEMPO_INFUSAO3);
    add_json_value(json_item_w, 'qtTempoPermanencia3', r_c01.QT_TEMPO_PERMANENCIA3);
    add_json_value(json_item_w, 'qtTempoDrenagem3', r_c01.QT_TEMPO_DRENAGEM3);
    add_json_value(json_item_w, 'qtTempSolucao3', r_c01.QT_TEMP_SOLUCAO3);
    add_json_value(json_item_w, 'qtVolumeSol3', r_c01.QT_VOLUME_SOL3);
    -- Componente 1
    add_json_value(json_item_w, 'qtDoseSoluc13', r_c01.QT_DOSE_SOLUC1_3);
    add_json_value(json_item_w, 'descMaterialComp13', r_c01.DESC_MATERIAL_COMP1_3);
    add_json_value(json_item_w, 'codeComp13Value', r_c01.CODE_COMP1_3_VALUE);
    add_json_value(json_item_w, 'codeComp13System', r_c01.CODE_COMP1_3_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp13Text', r_c01.UNIDADE_MEDIDA_COMP1_3_TEXT);
    add_json_value(json_item_w, 'unidMedComp13CodeSystem', r_c01.UNID_MED_COMP1_3_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp13CodeValue', r_c01.UNID_MED_COMP1_3_CODE_VALUE);
    -- Componente 2
    add_json_value(json_item_w, 'qtDoseSoluc23', r_c01.QT_DOSE_SOLUC2_3);
    add_json_value(json_item_w, 'descMaterialComp23', r_c01.DESC_MATERIAL_COMP2_3);
    add_json_value(json_item_w, 'codeComp23Value', r_c01.CODE_COMP2_3_VALUE);
    add_json_value(json_item_w, 'codeComp23System', r_c01.CODE_COMP2_3_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp23Text', r_c01.UNIDADE_MEDIDA_COMP2_3_TEXT);
    add_json_value(json_item_w, 'unidMedComp23CodeSystem', r_c01.UNID_MED_COMP2_3_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp23CodeValue', r_c01.UNID_MED_COMP2_3_CODE_VALUE);
    -- Componente 3
    add_json_value(json_item_w, 'qtDoseSoluc33', r_c01.QT_DOSE_SOLUC3_3);
    add_json_value(json_item_w, 'descMaterialComp33', r_c01.DESC_MATERIAL_COMP3_3);
    add_json_value(json_item_w, 'codeComp33Value', r_c01.CODE_COMP3_3_VALUE);
    add_json_value(json_item_w, 'codeComp33System', r_c01.CODE_COMP3_3_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp33Text', r_c01.UNIDADE_MEDIDA_COMP3_3_TEXT);
    add_json_value(json_item_w, 'unidMedComp33CodeSystem', r_c01.UNID_MED_COMP3_3_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp33CodeValue', r_c01.UNID_MED_COMP3_3_CODE_VALUE);
    -- Componente 4
    add_json_value(json_item_w, 'qtDoseSoluc43', r_c01.QT_DOSE_SOLUC4_3);
    add_json_value(json_item_w, 'descMaterialComp43', r_c01.DESC_MATERIAL_COMP4_3);
    add_json_value(json_item_w, 'codeComp43Value', r_c01.CODE_COMP4_3_VALUE);
    add_json_value(json_item_w, 'codeComp43System', r_c01.CODE_COMP4_3_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp43Text', r_c01.UNIDADE_MEDIDA_COMP4_3_TEXT);
    add_json_value(json_item_w, 'unidMedComp43CodeSystem', r_c01.UNID_MED_COMP4_3_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp43CodeValue', r_c01.UNID_MED_COMP4_3_CODE_VALUE);
    -- Componente 5
    add_json_value(json_item_w, 'qtDoseSoluc53', r_c01.QT_DOSE_SOLUC5_3);
    add_json_value(json_item_w, 'descMaterialComp53', r_c01.DESC_MATERIAL_COMP5_3);
    add_json_value(json_item_w, 'codeComp53Value', r_c01.CODE_COMP5_3_VALUE);
    add_json_value(json_item_w, 'codeComp53System', r_c01.CODE_COMP5_3_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp53Text', r_c01.UNIDADE_MEDIDA_COMP5_3_TEXT);
    add_json_value(json_item_w, 'unidMedComp53CodeSystem', r_c01.UNID_MED_COMP5_3_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp53CodeValue', r_c01.UNID_MED_COMP5_3_CODE_VALUE);
    -- Componente 6
    add_json_value(json_item_w, 'qtDoseSoluc63', r_c01.QT_DOSE_SOLUC6_3);
    add_json_value(json_item_w, 'descMaterialComp63', r_c01.DESC_MATERIAL_COMP6_3);
    add_json_value(json_item_w, 'codeComp63Value', r_c01.CODE_COMP6_3_VALUE);
    add_json_value(json_item_w, 'codeComp63System', r_c01.CODE_COMP6_3_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp63Text', r_c01.UNIDADE_MEDIDA_COMP6_3_TEXT);
    add_json_value(json_item_w, 'unidMedComp63CodeSystem', r_c01.UNID_MED_COMP6_3_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp63CodeValue', r_c01.UNID_MED_COMP6_3_CODE_VALUE);
    -- Componente 7
    add_json_value(json_item_w, 'qtDoseSoluc73', r_c01.QT_DOSE_SOLUC7_3);
    add_json_value(json_item_w, 'descMaterialComp73', r_c01.DESC_MATERIAL_COMP7_3);
    add_json_value(json_item_w, 'codeComp73Value', r_c01.CODE_COMP7_3_VALUE);
    add_json_value(json_item_w, 'codeComp73System', r_c01.CODE_COMP7_3_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp73Text', r_c01.UNIDADE_MEDIDA_COMP7_3_TEXT);
    add_json_value(json_item_w, 'unidMedComp73CodeSystem', r_c01.UNID_MED_COMP7_3_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp73CodeValue', r_c01.UNID_MED_COMP7_3_CODE_VALUE);
    -- Solucao 4
    add_json_value(json_item_w, 'ieTipoSolucao4', r_c01.IE_TIPO_SOLUCAO4);
    add_json_value(json_item_w, 'dsTipoSolucao4', r_c01.DS_TIPO_SOLUCAO4);
    add_json_value(json_item_w, 'ieBombaInfusao4', r_c01.IE_BOMBA_INFUSAO4);
    add_json_value(json_item_w, 'infusionPump4CodeSystem', r_c01.INFUSION_PUMP4_CODE_SYSTEM);
    add_json_value(json_item_w, 'infusionPump4CodeVale', r_c01.INFUSION_PUMP4_CODE_VALE);
    add_json_value(json_item_w, 'infusionPump4Text', r_c01.INFUSION_PUMP4_TEXT);
    add_json_value(json_item_w, 'nrSeqProtocolo4', r_c01.NR_SEQ_PROTOCOLO4);
    add_json_value(json_item_w, 'descProt4', nvl(r_c01.DESC_PROT4, r_c01.DESC_INFO_ITEM));
    add_json_value(json_item_w, 'qtDosagem4', r_c01.QT_DOSAGEM4);
    add_json_value(json_item_w, 'ieUnidVelInf4', r_c01.IE_UNID_VEL_INF4);
    add_json_value(json_item_w, 'rateUnit4CodeSystem', r_c01.RATE_UNIT4_CODE_SYSTEM);
    add_json_value(json_item_w, 'rateUnit4CodeVale', r_c01.RATE_UNIT4_CODE_VALE);
    add_json_value(json_item_w, 'rateUnit4Text', r_c01.RATE_UNIT4_TEXT);
    add_json_value(json_item_w, 'qtDoseAtaque4', r_c01.QT_DOSE_ATAQUE4);
    add_json_value(json_item_w, 'qtSolucaoTotal4', r_c01.QT_SOLUCAO_TOTAL4);
    add_json_value(json_item_w, 'dsObservacao4', r_c01.DS_OBSERVACAO4);
    add_json_value(json_item_w, 'qtTempoInfusao4', r_c01.QT_TEMPO_INFUSAO4);
    add_json_value(json_item_w, 'qtTempoPermanencia4', r_c01.QT_TEMPO_PERMANENCIA4);
    add_json_value(json_item_w, 'qtTempoDrenagem4', r_c01.QT_TEMPO_DRENAGEM4);
    add_json_value(json_item_w, 'qtTempSolucao4', r_c01.QT_TEMP_SOLUCAO4);
    add_json_value(json_item_w, 'qtVolumeSol4', r_c01.QT_VOLUME_SOL4);
    -- Componente 1
    add_json_value(json_item_w, 'qtDoseSoluc14', r_c01.QT_DOSE_SOLUC1_4);
    add_json_value(json_item_w, 'descMaterialComp14', r_c01.DESC_MATERIAL_COMP1_4);
    add_json_value(json_item_w, 'codeComp14Value', r_c01.CODE_COMP1_4_VALUE);
    add_json_value(json_item_w, 'codeComp14System', r_c01.CODE_COMP1_4_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp14Text', r_c01.UNIDADE_MEDIDA_COMP1_4_TEXT);
    add_json_value(json_item_w, 'unidMedComp14CodeSystem', r_c01.UNID_MED_COMP1_4_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp14CodeValue', r_c01.UNID_MED_COMP1_4_CODE_VALUE);
    -- Componente 2
    add_json_value(json_item_w, 'qtDoseSoluc24', r_c01.QT_DOSE_SOLUC2_4);
    add_json_value(json_item_w, 'descMaterialComp24', r_c01.DESC_MATERIAL_COMP2_4);
    add_json_value(json_item_w, 'codeComp24Value', r_c01.CODE_COMP2_4_VALUE);
    add_json_value(json_item_w, 'codeComp24System', r_c01.CODE_COMP2_4_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp24Text', r_c01.UNIDADE_MEDIDA_COMP2_4_TEXT);
    add_json_value(json_item_w, 'unidMedComp24CodeSystem', r_c01.UNID_MED_COMP2_4_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp24CodeValue', r_c01.UNID_MED_COMP2_4_CODE_VALUE);
    -- Componente 3
    add_json_value(json_item_w, 'qtDoseSoluc34', r_c01.QT_DOSE_SOLUC3_4);
    add_json_value(json_item_w, 'descMaterialComp34', r_c01.DESC_MATERIAL_COMP3_4);
    add_json_value(json_item_w, 'codeComp34Value', r_c01.CODE_COMP3_4_VALUE);
    add_json_value(json_item_w, 'codeComp34System', r_c01.CODE_COMP3_4_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp34Text', r_c01.UNIDADE_MEDIDA_COMP3_4_TEXT);
    add_json_value(json_item_w, 'unidMedComp34CodeSystem', r_c01.UNID_MED_COMP3_4_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp34CodeValue', r_c01.UNID_MED_COMP3_4_CODE_VALUE);
    -- Componente 4
    add_json_value(json_item_w, 'qtDoseSoluc44', r_c01.QT_DOSE_SOLUC4_4);
    add_json_value(json_item_w, 'descMaterialComp44', r_c01.DESC_MATERIAL_COMP4_4);
    add_json_value(json_item_w, 'codeComp44Value', r_c01.CODE_COMP4_4_VALUE);
    add_json_value(json_item_w, 'codeComp44System', r_c01.CODE_COMP4_4_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp44Text', r_c01.UNIDADE_MEDIDA_COMP4_4_TEXT);
    add_json_value(json_item_w, 'unidMedComp44CodeSystem', r_c01.UNID_MED_COMP4_4_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp44CodeValue', r_c01.UNID_MED_COMP4_4_CODE_VALUE);
    -- Componente 5
    add_json_value(json_item_w, 'qtDoseSoluc54', r_c01.QT_DOSE_SOLUC5_4);
    add_json_value(json_item_w, 'descMaterialComp54', r_c01.DESC_MATERIAL_COMP5_4);
    add_json_value(json_item_w, 'codeComp54Value', r_c01.CODE_COMP5_4_VALUE);
    add_json_value(json_item_w, 'codeComp54System', r_c01.CODE_COMP5_4_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp54Text', r_c01.UNIDADE_MEDIDA_COMP5_4_TEXT);
    add_json_value(json_item_w, 'unidMedComp54CodeSystem', r_c01.UNID_MED_COMP5_4_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp54CodeValue', r_c01.UNID_MED_COMP5_4_CODE_VALUE);
    -- Componente 6
    add_json_value(json_item_w, 'qtDoseSoluc64', r_c01.QT_DOSE_SOLUC6_4);
    add_json_value(json_item_w, 'descMaterialComp64', r_c01.DESC_MATERIAL_COMP6_4);
    add_json_value(json_item_w, 'codeComp64Value', r_c01.CODE_COMP6_4_VALUE);
    add_json_value(json_item_w, 'codeComp64System', r_c01.CODE_COMP6_4_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp64Text', r_c01.UNIDADE_MEDIDA_COMP6_4_TEXT);
    add_json_value(json_item_w, 'unidMedComp64CodeSystem', r_c01.UNID_MED_COMP6_4_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp64CodeValue', r_c01.UNID_MED_COMP6_4_CODE_VALUE);
    -- Componente 7
    add_json_value(json_item_w, 'qtDoseSoluc74', r_c01.QT_DOSE_SOLUC7_4);
    add_json_value(json_item_w, 'descMaterialComp74', r_c01.DESC_MATERIAL_COMP7_4);
    add_json_value(json_item_w, 'codeComp74Value', r_c01.CODE_COMP7_4_VALUE);
    add_json_value(json_item_w, 'codeComp74System', r_c01.CODE_COMP7_4_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp74Text', r_c01.UNIDADE_MEDIDA_COMP7_4_TEXT);
    add_json_value(json_item_w, 'unidMedComp74CodeSystem', r_c01.UNID_MED_COMP7_4_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp74CodeValue', r_c01.UNID_MED_COMP7_4_CODE_VALUE);
	-- Solucao 5
    add_json_value(json_item_w, 'ieTipoSolucao5', r_c01.IE_TIPO_SOLUCAO5);
    add_json_value(json_item_w, 'dsTipoSolucao5', r_c01.DS_TIPO_SOLUCAO5);
    add_json_value(json_item_w, 'ieBombaInfusao5', r_c01.IE_BOMBA_INFUSAO5);
    add_json_value(json_item_w, 'infusionPump5CodeSystem', r_c01.INFUSION_PUMP5_CODE_SYSTEM);
    add_json_value(json_item_w, 'infusionPump5CodeVale', r_c01.INFUSION_PUMP5_CODE_VALE);
    add_json_value(json_item_w, 'infusionPump5Text', r_c01.INFUSION_PUMP5_TEXT);
    add_json_value(json_item_w, 'nrSeqProtocolo5', r_c01.NR_SEQ_PROTOCOLO5);
    add_json_value(json_item_w, 'descProt5', nvl(r_c01.DESC_PROT5, r_c01.DESC_INFO_ITEM));
    add_json_value(json_item_w, 'qtDosagem5', r_c01.QT_DOSAGEM5);
    add_json_value(json_item_w, 'ieUnidVelInf5', r_c01.IE_UNID_VEL_INF5);
    add_json_value(json_item_w, 'rateUnit5CodeSystem', r_c01.RATE_UNIT5_CODE_SYSTEM);
    add_json_value(json_item_w, 'rateUnit5CodeVale', r_c01.RATE_UNIT5_CODE_VALE);
    add_json_value(json_item_w, 'rateUnit5Text', r_c01.RATE_UNIT5_TEXT);
    add_json_value(json_item_w, 'qtDoseAtaque5', r_c01.QT_DOSE_ATAQUE5);
    add_json_value(json_item_w, 'qtSolucaoTotal5', r_c01.QT_SOLUCAO_TOTAL5);
    add_json_value(json_item_w, 'dsObservacao5', r_c01.DS_OBSERVACAO5);
    add_json_value(json_item_w, 'qtTempoInfusao5', r_c01.QT_TEMPO_INFUSAO5);
    add_json_value(json_item_w, 'qtTempoPermanencia5', r_c01.QT_TEMPO_PERMANENCIA5);
    add_json_value(json_item_w, 'qtTempoDrenagem5', r_c01.QT_TEMPO_DRENAGEM5);
    add_json_value(json_item_w, 'qtTempSolucao5', r_c01.QT_TEMP_SOLUCAO5);
    add_json_value(json_item_w, 'qtVolumeSol5', r_c01.QT_VOLUME_SOL5);
    -- Componente 1
    add_json_value(json_item_w, 'qtDoseSoluc15', r_c01.QT_DOSE_SOLUC1_5);
    add_json_value(json_item_w, 'descMaterialComp15', r_c01.DESC_MATERIAL_COMP1_5);
    add_json_value(json_item_w, 'codeComp15Value', r_c01.CODE_COMP1_5_VALUE);
    add_json_value(json_item_w, 'codeComp15System', r_c01.CODE_COMP1_5_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp15Text', r_c01.UNIDADE_MEDIDA_COMP1_5_TEXT);
    add_json_value(json_item_w, 'unidMedComp15CodeSystem', r_c01.UNID_MED_COMP1_5_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp15CodeValue', r_c01.UNID_MED_COMP1_5_CODE_VALUE);
    -- Componente 2
    add_json_value(json_item_w, 'qtDoseSoluc25', r_c01.QT_DOSE_SOLUC2_5);
    add_json_value(json_item_w, 'descMaterialComp25', r_c01.DESC_MATERIAL_COMP2_5);
    add_json_value(json_item_w, 'codeComp25Value', r_c01.CODE_COMP2_5_VALUE);
    add_json_value(json_item_w, 'codeComp25System', r_c01.CODE_COMP2_5_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp25Text', r_c01.UNIDADE_MEDIDA_COMP2_5_TEXT);
    add_json_value(json_item_w, 'unidMedComp25CodeSystem', r_c01.UNID_MED_COMP2_5_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp25CodeValue', r_c01.UNID_MED_COMP2_5_CODE_VALUE);
    -- Componente 3
    add_json_value(json_item_w, 'qtDoseSoluc35', r_c01.QT_DOSE_SOLUC3_5);
    add_json_value(json_item_w, 'descMaterialComp35', r_c01.DESC_MATERIAL_COMP3_5);
    add_json_value(json_item_w, 'codeComp35Value', r_c01.CODE_COMP3_5_VALUE);
    add_json_value(json_item_w, 'codeComp35System', r_c01.CODE_COMP3_5_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp35Text', r_c01.UNIDADE_MEDIDA_COMP3_5_TEXT);
    add_json_value(json_item_w, 'unidMedComp35CodeSystem', r_c01.UNID_MED_COMP3_5_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp35CodeValue', r_c01.UNID_MED_COMP3_5_CODE_VALUE);
    -- Componente 4
    add_json_value(json_item_w, 'qtDoseSoluc45', r_c01.QT_DOSE_SOLUC4_5);
    add_json_value(json_item_w, 'descMaterialComp45', r_c01.DESC_MATERIAL_COMP4_5);
    add_json_value(json_item_w, 'codeComp45Value', r_c01.CODE_COMP4_5_VALUE);
    add_json_value(json_item_w, 'codeComp45System', r_c01.CODE_COMP4_5_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp45Text', r_c01.UNIDADE_MEDIDA_COMP4_5_TEXT);
    add_json_value(json_item_w, 'unidMedComp45CodeSystem', r_c01.UNID_MED_COMP4_5_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp45CodeValue', r_c01.UNID_MED_COMP4_5_CODE_VALUE);
    -- Componente 5
    add_json_value(json_item_w, 'qtDoseSoluc55', r_c01.QT_DOSE_SOLUC5_5);
    add_json_value(json_item_w, 'descMaterialComp55', r_c01.DESC_MATERIAL_COMP5_5);
    add_json_value(json_item_w, 'codeComp55Value', r_c01.CODE_COMP5_5_VALUE);
    add_json_value(json_item_w, 'codeComp55System', r_c01.CODE_COMP5_5_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp55Text', r_c01.UNIDADE_MEDIDA_COMP5_5_TEXT);
    add_json_value(json_item_w, 'unidMedComp55CodeSystem', r_c01.UNID_MED_COMP5_5_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp55CodeValue', r_c01.UNID_MED_COMP5_5_CODE_VALUE);
    -- Componente 6
    add_json_value(json_item_w, 'qtDoseSoluc65', r_c01.QT_DOSE_SOLUC6_5);
    add_json_value(json_item_w, 'descMaterialComp65', r_c01.DESC_MATERIAL_COMP6_5);
    add_json_value(json_item_w, 'codeComp65Value', r_c01.CODE_COMP6_5_VALUE);
    add_json_value(json_item_w, 'codeComp65System', r_c01.CODE_COMP6_5_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp65Text', r_c01.UNIDADE_MEDIDA_COMP6_5_TEXT);
    add_json_value(json_item_w, 'unidMedComp65CodeSystem', r_c01.UNID_MED_COMP6_5_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp65CodeValue', r_c01.UNID_MED_COMP6_5_CODE_VALUE);
    -- Componente 7
    add_json_value(json_item_w, 'qtDoseSoluc75', r_c01.QT_DOSE_SOLUC7_5);
    add_json_value(json_item_w, 'descMaterialComp75', r_c01.DESC_MATERIAL_COMP7_5);
    add_json_value(json_item_w, 'codeComp75Value', r_c01.CODE_COMP7_5_VALUE);
    add_json_value(json_item_w, 'codeComp75System', r_c01.CODE_COMP7_5_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaComp75Text', r_c01.UNIDADE_MEDIDA_COMP7_5_TEXT);
    add_json_value(json_item_w, 'unidMedComp75CodeSystem', r_c01.UNID_MED_COMP7_5_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidMedComp75CodeValue', r_c01.UNID_MED_COMP7_5_CODE_VALUE);


    if (r_c01.IE_TIPO_DIALISE = 'DI') then
			add_json_value(json_item_w, 'intervalCodeValue', r_c01.IE_CATEGORIA_CODE_VALE);
    elsif (r_c01.IE_TIPO_DIALISE = 'DP') then
      add_json_value(json_item_w, 'intervalCodeValue', r_c01.IE_TIPO_PERITONEAL_CODE_VALE);
		end if;

		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.DT_INICIO, r_c01.dt_fim));
		end if;
		
		if (r_c01.IE_HEMODIALISE = 'E' and r_c01.IE_TIPO_DIALISE = 'DI') then
      add_json_value(json_item_w, 'priority', 'S');
    else
      add_json_value(json_item_w, 'priority', 'R');
    end if;

    add_json_value(json_item_w, 'occurrenceDurationId', r_c01.MINUTE_CODE_VALUE || r_c01.HR_QT_HORA_MIN_SESSAO);
    add_json_value(json_item_w, 'occurrenceDurationText', r_c01.HR_QT_HORA_MIN_SESSAO || ' ' || r_c01.DESC_UNIDADE_MEDIDA);
    add_json_value(json_item_w, 'occurrenceDurationCodeSystem', r_c01.MINUTE_CODE_SYSTEM);
    
		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.SUSPENDED_TIME, r_c01.REQUESTED_DATE_TIME));
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));

		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_dial_data;

	
	function get_dial_message(nr_cpoe_dial_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json is
	json_return_w		philips_json;
	json_item_list_w   	philips_json_list;

	begin
	
  json_item_list_w	:= get_dial_data(nr_cpoe_dial_p);
	
	if	(json_item_list_w.count > 0) then
		json_return_w	:= philips_json();
		json_return_w		:= get_default_message(nr_cpoe_dial_p);
		add_json_value(json_return_w, 'orderControl', ie_order_control_p);
		add_json_value(json_return_w, 'entityidentifier', nr_entity_identifier_p);
		add_json_value(json_return_w, 'hl7MsgType', 'OMP');
		
		json_return_w.put('dialList', json_item_list_w.to_json_value());
	end if;
	
	return json_return_w;
	
	end get_dial_message;
					

	function get_message_clob(nr_cpoe_dial_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob is
	ds_json_out_w		clob;
	json_dial_w	philips_json;
	
	begin
		
	json_dial_w		:= get_dial_message(nr_cpoe_dial_p, ie_order_control_p, nr_entity_identifier_p);
	
	if	(json_dial_w is null) then
		return null;
	end if;
	
	dbms_lob.createtemporary( ds_json_out_w, true);
	json_dial_w.to_clob(ds_json_out_w);
	
	return ds_json_out_w;
	end get_message_clob;


	procedure getCpoeIntegracaoDial(nr_seq_dial_cpoe_p number, nr_entity_identifier_p out number) is
	begin

	select max(nr_sequencia)
	into nr_entity_identifier_p
	from cpoe_integracao
	where NR_SEQ_DIAL_CPOE = nr_seq_dial_cpoe_p;
	
	if (nr_entity_identifier_p is null) then
		select CPOE_INTEGRACAO_SEQ.nextval
		into nr_entity_identifier_p
		from dual;

		insert into cpoe_integracao (
			nr_sequencia,
			NR_SEQ_DIAL_CPOE
			)
		values (
			nr_entity_identifier_p,
			nr_seq_dial_cpoe_p
		);
		/* No commit so it can be used inside trigger */
	end if;	

	end getCpoeIntegracaoDial;	

end cpoe_dial_order_json_pck;
/
