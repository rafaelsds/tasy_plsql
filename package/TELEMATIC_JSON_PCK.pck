create or replace package telematic_json_pck as

	function get_muster_information_clob 	(nr_sequencia_p number) return clob;

	procedure send_to_bifrost		(	nm_evento_p	varchar2,
							ds_mensagem_p	clob,
							nm_usuario_p	varchar2);
	procedure send_muster_info_bifrost	(	cd_pessoa_fisica_p 	varchar2,
							nr_atendimento_p	number,
							nm_usuario_p		varchar2);
	
	function generate_uID return varchar2;
	
	function get_muster_information (nr_sequencia_p number) return philips_json;
		
end telematic_json_pck;
/
create or replace 
package body telematic_json_pck as

	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			varchar2) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;
	
	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			number) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;

	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			date) is
	
	begin
	if	(value_p is not null) then
		json_p.put(name_p,pkg_date_utils.get_isoformat(value_p));
	end if;
	end add_json_value;

	function get_muster_information( nr_sequencia_p number) return philips_json is
	
	json_w				philips_json;
	json_coverage_w		philips_json;
	json_patient_w		philips_json;
	json_condition_w	philips_json;
    json_practitioner_w	philips_json;
	json_practitioner_role_w	philips_json;
	json_composition_w	philips_json;
	json_service_request_w	philips_json;
	json_organization_w	philips_json;
	json_bundle_w		philips_json;
	json_icdList_w 		philips_json_list;
    json_item_w         philips_json;
	vl_url_w			varchar2(255);
	
	Cursor C01 is
	select	f.ds_convenio ds_convenio,
		f.ds_nome as PATIENT_NAME,
		f.ds_sobrenome as FAMILY_NAME,
		f.ds_cidade as PATIENT_CITY,
		f.dt_nascimento dt_nascimento,
		f.cd_usuario_conv cd_usuario_conv,
		f.cd_cnes cd_cnes,
		lpad(nvl(obter_asv_team(f.nr_atendimento),(select max(h.nr_bsnr) from medico_espec_bsnr h where h.cd_pessoa_fisica = d.cd_medico and cd_especialidade = d.nr_rqe)),9,0) cd_estab,
		(select  nr_rqe
			from    medico_especialidade
			where   cd_especialidade = d.nr_rqe
			and     cd_pessoa_fisica = d.cd_medico) nr_crm,
		to_char(d.dt_liberacao,'dd.mm.yyyy') dt_referencia,
		DECODE(d.ie_tipo_atestado,'E','ERST','') ie_tipo_atestado1,
		DECODE(d.ie_tipo_atestado,'F','FOLGE','') ie_tipo_atestado2,
		DECODE(d.ie_sequela_acidente,'S','2','') ie_sequela,
		DECODE(d.ie_acidente_trabalho,'S','d_arzt','') ie_acidente,
		d.dt_atestado as dt_atestado,
		d.dt_ausencia_trabalho as dt_ausencia,
		d.dt_diagnosticado as dt_diagnosticado,
		get_personal_title(c.nr_seq_forma_trat, 'T')  practitionerTitle,
		g.ds_given_name practitionerGivenName,
		g.ds_family_name practitionerLastName,
		nvl(obter_especialidade_medico(c.cd_pessoa_fisica, 'D'), get_medic_speciality_degress(c.cd_pessoa_fisica)) ds_esp_medico,
		e.ds_razao_social ds_razao_social,
		e.ds_endereco ds_ende_empr,
		e.ds_municipio ds_munc_empr,
		e.nr_telefone nr_telefone,
		(select    max(ab.ds_endereco) from compl_pessoa_fisica ab where ab.cd_pessoa_fisica = b.cd_pessoa_fisica) patientStreetName,
		(select    max(ab.nr_endereco) from compl_pessoa_fisica ab where ab.cd_pessoa_fisica = b.cd_pessoa_fisica) patientNumber,
		(select    max(ab.cd_cep) from compl_pessoa_fisica ab where ab.cd_pessoa_fisica = b.cd_pessoa_fisica) patientPostalCode,
		d.dt_atualizacao bundleLastUpdated
	from	atendimento_paciente    	a,
			pessoa_fisica 				b,
			pessoa_fisica             	c,
			atestado_paciente         	d,
			kv_form_stamp_v           	e,
			kv_form_header_v          	f,
			person_name               	g
	where 		b.cd_pessoa_fisica  = a.cd_pessoa_fisica
		and 	c.cd_pessoa_fisica = d.cd_medico
		and 	c.nr_seq_person_name = g.nr_sequencia
		and 	a.nr_atendimento = d.nr_atendimento
		and 	d.nr_atendimento = f.nr_atendimento
		and   	a.nr_atendimento = f.nr_atendimento
		and 	g.ds_type = 'main'
		and 	d.nr_sequencia = nr_sequencia_p;
		
		
	Cursor C02 is
	SELECT	CAP.cd_doenca cd_cid, to_char(CDV.dt_versao, 'yyyy') cid_version,  substr(cd.ds_doenca_cid, 1, instr(cd.ds_doenca_cid, ' ', 1, 1) - 1) cid
        FROM	atestado_paciente AP,
			cid_atestado_paciente CAP,
			cid_doenca_versao CDV,
			cid_doenca CD
		WHERE CAP.nr_seq_atestado_paciente = AP.nr_sequencia
			AND AP.nr_sequencia = nr_sequencia_p
			AND CDV.cd_doenca_cid = CAP.cd_doenca
			and cd.cd_doenca_cid = cdv.cd_doenca_cid
			and AP.dt_diagnosticado between cdv.dt_vigencia_inicial and nvl(cdv.dt_vigencia_final,sysdate)
		order by 1;
	
	Cursor C03 is
	select ds_obs_diagnostico
		from   atestado_paciente
		where  nr_sequencia = nr_sequencia;

	Cursor C04 is
	SELECT DECODE(AP.ie_outro_acidente,'S','1',' ') Outro_Acidente,
		DECODE(AP.ie_acidente_militar,'S','3',' ') Acidente_Militar,
		DECODE(AP.ie_reabilitacao_medica,'S','reha',' ') Reabilitacao,
		DECODE(AP.ie_passos_reintegracao,'S','wiedereingliederung',' ') Reintegracao,
		DECODE(AP.ie_outros,'S','sonstige',' ') Outros,
		DECODE(AP.ie_tipo_final,'A','ERST_END') Tipo_Final1,
		DECODE(AP.ie_tipo_final,'E','FOLGE_END') Tipo_Final2
	FROM atestado_paciente AP,
		pessoa_fisica PF
	WHERE AP.cd_pessoa_fisica = PF.cd_pessoa_fisica
		AND AP.nr_sequencia = nr_sequencia_p;
	
	Cursor C05 is
	select extensionWop, insuredType, specialPeopleGroup, DMPIndicator, legalBasis 
	from (
		select	
			a.zusatzinfos_wop extensionWop,
			decode(c.ie_tipo_conveniado,1,'1',2,'3',5,'5','') insuredType,
			lpad(nvl(a.besondere_personengruppe,0),2,0) specialPeopleGroup,
			lpad(nvl(a.dmp_kennzeichnung,0),2,0) DMPIndicator,
			decode(obter_asv_team(b.nr_atendimento), null, decode(d.ie_tipo_consentimento,'E','04','00'), lpad('01',2,0)) legalBasis
		from 	pessoa_fisica_egk a,
			atendimento_paciente b,
			atend_categoria_convenio c,
			pep_pac_ci d,
			atestado_paciente e
		where	a.cd_pessoa_fisica(+)	= b.cd_pessoa_fisica
			and	b.nr_atendimento	= c.nr_atendimento
			AND	c.nr_seq_interno	= ( 
			SELECT MAX(t.nr_seq_interno) 
			FROM atend_categoria_convenio t, 
				convenio x 
			WHERE t.nr_atendimento = c.nr_atendimento 
				AND t.cd_convenio = x.cd_convenio 
				AND x.ie_tipo_convenio = 11 )
			and	b.nr_atendimento	= d.nr_atendimento(+)
			and e.nr_atendimento = b.nr_atendimento
			and e.nr_sequencia = nr_sequencia_p
		order by a.dt_atualizacao desc)
	where ROWNUM = 1;
	
	
	begin

	json_coverage_w	:= philips_json();
	json_patient_w	:= philips_json();
	json_practitioner_w	:= philips_json();
	json_practitioner_role_w	:= philips_json();
	json_composition_w	:= philips_json();
	json_service_request_w	:= philips_json();
	json_condition_w	:= philips_json();
	json_organization_w	:= philips_json();
	json_bundle_w	:= philips_json();
	json_w				:= philips_json();
	json_icdList_w := philips_json_list();
	
	for r_c01 in c01 loop
		begin

		add_json_value(json_coverage_w, 'payorDisplay',r_c01.ds_convenio);
		add_json_value(json_coverage_w, 'payorAlternativeId',r_c01.cd_cnes);
		add_json_value(json_coverage_w, 'coverageId', generate_uID);
		
		add_json_value(json_patient_w, 'patientName', r_c01.PATIENT_NAME);
		add_json_value(json_patient_w, 'familyName', r_c01.FAMILY_NAME);
		add_json_value(json_patient_w, 'city', r_c01.PATIENT_CITY);
		add_json_value(json_patient_w, 'birthDate',r_c01.dt_nascimento);
		add_json_value(json_patient_w, 'insuredId_GKV',r_c01.cd_usuario_conv);
		add_json_value(json_patient_w, 'insuredNumber_PKV',r_c01.cd_usuario_conv);
		add_json_value(json_patient_w, 'streetName',r_c01.patientStreetName);
		add_json_value(json_patient_w, 'houseNumber',r_c01.patientNumber);
		add_json_value(json_patient_w, 'postalCode',r_c01.patientPostalCode);
		add_json_value(json_patient_w, 'patientId', generate_uID);
		
		add_json_value(json_condition_w, 'clinicalStatus','active');
		add_json_value(json_condition_w, 'verificationStatus','confirmed');
		add_json_value(json_condition_w, 'startDate', r_c01.dt_atestado);
		add_json_value(json_condition_w, 'endDate', r_c01.dt_ausencia);
		add_json_value(json_condition_w, 'extensionFoundOn', r_c01.dt_diagnosticado);
		add_json_value(json_condition_w, 'extension', r_c01.ie_sequela);
		add_json_value(json_condition_w, 'conditionId', generate_uID);
		
		add_json_value(json_organization_w, 'companyId', r_c01.cd_estab);
		add_json_value(json_organization_w, 'name', r_c01.ds_razao_social);
		add_json_value(json_organization_w, 'postalCity', r_c01.ds_munc_empr);
		add_json_value(json_organization_w, 'streetNumber', r_c01.ds_ende_empr);
		add_json_value(json_organization_w, 'telecom', r_c01.nr_telefone);
		add_json_value(json_organization_w, 'organizationId', generate_uID);
		
		add_json_value(json_practitioner_w, 'arnId', r_c01.nr_crm);
		add_json_value(json_practitioner_w, 'qualification', r_c01.ds_esp_medico);
		add_json_value(json_practitioner_w, 'givenName',r_c01.practitionerGivenName);
		add_json_value(json_practitioner_w, 'lastName',r_c01.practitionerLastName);
		add_json_value(json_practitioner_w, 'title',r_c01.practitionerTitle);
		add_json_value(json_practitioner_w, 'practitionerId', generate_uID);
		
		add_json_value(json_practitioner_role_w, 'practitionerRoleId', generate_uID);
		
		add_json_value(json_composition_w, 'date', r_c01.dt_referencia);
		if (r_c01.ie_tipo_atestado1 <> '') then
			add_json_value(json_composition_w, 'codingType', r_c01.ie_tipo_atestado1);
		end if;
		if (r_c01.ie_tipo_atestado2 <> '') then 
			add_json_value(json_composition_w, 'codingType', r_c01.ie_tipo_atestado2);
		end if;
		add_json_value(json_composition_w, 'compositionId', generate_uID);
		
		add_json_value(json_service_request_w, 'code', r_c01.ie_acidente);
		add_json_value(json_service_request_w, 'serviceRequestId', generate_uID);
		
		add_json_value(json_bundle_w, 'bundleId', generate_uID);
		add_json_value(json_bundle_w, 'lastUpdated', r_c01.bundleLastUpdated);
		
		
		
		end;
	end loop;
	
	for r_c02 in c02 loop
		begin
		json_item_w := philips_json();
		add_json_value(json_item_w, 'icd', r_c02.cid);
		add_json_value(json_item_w, 'icdId', generate_uID);
		add_json_value(json_item_w, 'icdVersion', r_c02.cid_version);
		add_json_value(json_item_w, 'clinicalStatus','active');
		add_json_value(json_item_w, 'verificationStatus','confirmed');
		json_icdList_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	for r_c03 in c03 loop
		begin
		add_json_value(json_condition_w, 'text', r_c03.ds_obs_diagnostico);
		end;
	end loop;
	
	for r_c04 in c04 loop
		begin
		
		add_json_value(json_condition_w, 'extensionCause', r_c04.Outro_Acidente);
		add_json_value(json_condition_w, 'weeks', r_c04.Tipo_Final1);
		add_json_value(json_condition_w, 'supplyAilments', r_c04.Acidente_Militar);
		
		add_json_value(json_service_request_w, 'rehab', r_c04.Reabilitacao);
		add_json_value(json_service_request_w, 'reintegration', r_c04.Reintegracao);
		add_json_value(json_service_request_w, 'otherMeasures', r_c04.Outros);
		
		add_json_value(json_composition_w, 'finalCertificate', r_c04.Tipo_Final2);

		end;
	end loop;
	
	for r_c05 in c05 loop
		begin
		
		add_json_value(json_coverage_w, 'extensionWOP', r_c05.extensionWop);
		add_json_value(json_coverage_w, 'insuredType', r_c05.insuredType);
		add_json_value(json_coverage_w, 'specialPeopleGroup', r_c05.specialPeopleGroup);
		add_json_value(json_coverage_w, 'DMPIndicator', r_c05.DMPIndicator);
		
		add_json_value(json_composition_w, 'legalBasis', r_c05.legalBasis);

		end;
	end loop;
	
	obter_param_usuario(9041, 11, obter_perfil_ativo, Obter_Usuario_Ativo, obter_estabelecimento_ativo, vl_url_w);
    json_w.put('urlParameter', vl_url_w);
	
	json_w.put('coverage',json_coverage_w.to_json_value());
	json_w.put('patient',json_patient_w.to_json_value());
	json_w.put('condition', json_condition_w.to_json_value());
	json_w.put('organization', json_organization_w.to_json_value());
	json_w.put('practitioner', json_practitioner_w.to_json_value());
	json_w.put('practitionerRole', json_practitioner_role_w.to_json_value());
	json_w.put('serviceRequest', json_service_request_w.to_json_value());
	json_w.put('composition', json_composition_w.to_json_value());
	json_w.put('icdList',json_icdList_w.to_json_value());
	json_w.put('bundle', json_bundle_w.to_json_value());
    
	return json_w;
	
	end get_muster_information;
	
	
	function get_muster_information_clob	(nr_sequencia_p number) return clob is
	ds_json_out_w	clob;
	json_muster_w		philips_json;
	begin
	
	json_muster_w		:= get_muster_information(nr_sequencia_p);
	dbms_lob.createtemporary( ds_json_out_w, true);
	json_muster_w.to_clob(ds_json_out_w);

	return ds_json_out_w;
	end get_muster_information_clob; 
	
	
	function generate_uID return varchar2 is
    uid varchar2(128);
	begin
	select regexp_replace(rawtohex(sys_guid()), '([A-F0-9]{8})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{12})', '\1-\2-\3-\4-\5') into uid from dual;
    return uid;
	end generate_uID;
	
	procedure send_to_bifrost (	nm_evento_p	varchar2,
							ds_mensagem_p	clob,
							nm_usuario_p	varchar2) is
	ds_result_w clob;
	begin
	
	ds_result_w := bifrost.send_integration_content(nm_evento_p,ds_mensagem_p,nm_usuario_p);
	
	end send_to_bifrost;
	
	
	
	procedure send_muster_info_bifrost	(	cd_pessoa_fisica_p 	varchar2,
							nr_atendimento_p	number,
							nm_usuario_p		varchar2) is
	
	begin
	send_to_bifrost('telematic.send.patient.data','',nm_usuario_p);
	end  send_muster_info_bifrost;

end telematic_json_pck;
/

