CREATE OR REPLACE PACKAGE PFCS_PCK_CENSUS AS

    PROCEDURE CALC_OCCUPANCY(cd_establishment_p number, nm_user_p varchar2);
    PROCEDURE CALC_HIST_OCCUPANCY(cd_establishment_p number, nm_user_p varchar2);
    PROCEDURE CALC_TOTAL_DISCHARGES_TODAY(cd_establishment_p number, nm_user_p varchar2);

    FUNCTION GET_DISCHARGES_BY_NOON(cd_establishment_p number) RETURN NUMBER;
    FUNCTION GET_PR_DISCHARGES_BY_NOON(cd_establishment_p number) RETURN NUMBER;
    FUNCTION GET_ANTICIPATED_DISCHARGES(
        cd_establishment_p number, cd_unit_p number default null, cd_unit_classification_p varchar2 default null,
        dt_predicted_discharge_p date, ie_discharge_order_status_p varchar2 default null
    ) RETURN NUMBER;
    FUNCTION GET_DISCHARGE_ORDERS(
        cd_establishment_p number, cd_unit_p number, cd_unit_classification_p varchar2,
        dt_period_start_p date, dt_period_end_p date, ie_over_threshold_p varchar2
    ) RETURN NUMBER;
    FUNCTION GET_AVG_DISCHARGE_ORDER_TIME(
        cd_establishment_p number, cd_unit_p number default null, cd_unit_classification_p varchar2 default null
    ) RETURN DATE;
    FUNCTION GET_AVG_DISCHARGE_TIME(
        cd_establishment_p number, cd_unit_p number default null, cd_unit_classification_p varchar2 default null
    ) RETURN DATE;
    FUNCTION GET_AVG_LAG_TIME(
        cd_establishment_p number, cd_unit_p number default null, cd_unit_classification_p varchar2 default null
    ) RETURN NUMBER;

    FUNCTION GET_CARE_STATUS(
        cd_establishment_p number, cd_unit_p number, cd_unit_classification_p varchar2,
        cd_category_p varchar2, ie_over_threshold_p varchar2
    ) RETURN NUMBER;

    FUNCTION GET_SPECIAL_REQUESTS(
        cd_establishment_p number, cd_unit_p number default null, cd_unit_classification_p varchar2 default null
    ) RETURN NUMBER;

END PFCS_PCK_CENSUS;
/

CREATE OR REPLACE PACKAGE BODY PFCS_PCK_CENSUS AS
    /*=========================================== Cursors ===========================================*/
    cursor cur_units(cd_establishment_p number) is
    select sec.cd_setor_atendimento cd_unit,
        sec.ds_setor_atendimento ds_unit,
        decode(sec.ie_semi_intensiva, PFCS_PCK_CONSTANTS.IE_YES_BR, PFCS_PCK_CONSTANTS.CD_TCU, sec.cd_classif_setor) cd_unit_classification
    from setor_atendimento sec
    where sec.ie_situacao = PFCS_PCK_CONSTANTS.IE_ACTIVE
        and sec.ie_ocup_hospitalar <> PFCS_PCK_CONSTANTS.IE_NO
        and sec.cd_estabelecimento_base = cd_establishment_p;

    cursor cur_beds(cd_establishment_p number, cd_unit_p number, ds_unit_p varchar2) is
    select (ds_unit_p || '-' || uni.cd_unidade_basica || '-' || uni.cd_unidade_compl) ds_location,
        uni.ie_status_unidade cd_status,
        uni.cd_unidade_basica ds_room,
        uni.cd_unidade_compl ds_bed,
        uni.nr_seq_interno nr_seq_interno,
        uni.nr_seq_location nr_seq_location,
        pfcs_get_bed_status(uni.ie_status_unidade, 'T', cd_establishment_p) ie_total_beds,
        pfcs_get_bed_status(uni.ie_status_unidade, 'C', cd_establishment_p) ie_census
    from unidade_atendimento uni
    where uni.cd_setor_atendimento = cd_unit_p
        and	uni.ie_situacao = PFCS_PCK_CONSTANTS.IE_ACTIVE;

    cursor cur_patients(nr_seq_interno_p number, cd_unit_classification_p varchar2) is
    select enc.id_encounter nr_encounter,
        pat.patient_id id_patient,
        pfcs_get_human_name(pat.nr_sequencia, 'Patient') nm_patient,
        pfcs_get_human_name(pfcs_get_practitioner_seq(enc.nr_sequencia, '405279007'), 'Practitioner') nm_physician,
        pat.gender ds_gender,
        pat.birthdate dt_birthdate,
        round(months_between(nvl(pat.deceased_date, sysdate), pat.birthdate)) qt_patient_age,
        enc.period_start dt_period_start,
        enc.dt_expected_discharge,
        enc.cd_discharge_disposition cd_disposition,
        enc.nr_sequencia nr_seq_encounter,
        enc.si_classif,
        pfcs_get_patient_diagnosis(enc.nr_sequencia) ds_problem,
        pfcs_get_code_status(pat.nr_sequencia, enc.nr_sequencia, 'S') cd_code_status,
        pfcs_get_special_requests(enc.nr_sequencia, uni.nr_seq_location) ds_special_requests,
        nvl(pfcs_get_checklist(enc.nr_sequencia, pat.nr_sequencia, 'MPL'),'') ds_care_status,
        nvl(pfcs_get_checklist(enc.nr_sequencia, pat.nr_sequencia, 'TOOLTIP'),'') ds_checklist,
        pfcs_get_edi_score(enc.nr_sequencia) qt_trs,
        pfcs_obs_contributor_pck.get_edi_vital_warnings(enc.nr_sequencia) ds_edi_vitals_warn,
        pfcs_obs_contributor_pck.get_edi_contributors(enc.nr_sequencia) ds_edi_contrb,
        pfcs_get_lace_plus(enc.nr_sequencia, enc.nr_seq_organization) ds_readmission_risk,
        pfcs_obs_contributor_pck.get_laceplus(enc.nr_sequencia) ds_readm_risk_contrb,
        pfcs_get_frequent_flyer(enc.nr_sequencia) ie_frequent_flyer,
        pfcs_obs_contributor_pck.get_freqflyer_visits(enc.nr_sequencia) ds_recur_pat_adm_data,
        pfcs_obs_contributor_pck.get_freqflyer_comorbidities(enc.nr_sequencia) ds_recur_pat_comorbd,
        pfcs_obs_contributor_pck.get_freqflyer_visit_reason(enc.nr_sequencia) ds_recur_pat_reasons,
        pfcs_pck_los.get_value(enc.nr_sequencia, cd_unit_classification_p) vl_los_remaining,
        svc.cd_service cd_svc_request,
        svc.si_status cd_svc_status,
        svc.si_intent cd_svc_intent,
        svc.dt_authored_on dt_svc_request,
        svc.nr_sequencia nr_seq_svc_request
    from pfcs_encounter enc,
        pfcs_patient pat,
        unidade_atendimento uni,
        pfcs_service_request svc
    where uni.nr_seq_location = nvl(enc.nr_seq_location, pfcs_get_pat_location(pat.nr_sequencia, enc.nr_sequencia))
        and uni.nr_seq_interno = nr_seq_interno_p
        and enc.nr_seq_patient = pat.nr_sequencia
        and enc.period_start is not null
        and svc.nr_seq_encounter = enc.nr_sequencia
        and svc.nr_seq_patient = pat.nr_sequencia
        and ((svc.cd_service = PFCS_PCK_CONSTANTS.CD_ADMISSION
                and svc.si_status = PFCS_PCK_CONSTANTS.SI_STATUS_COMPLETED
                and svc.si_intent = PFCS_PCK_CONSTANTS.SI_INTENT_ORDER) or
            svc.cd_service = PFCS_PCK_CONSTANTS.CD_TRANSFER or
            (svc.cd_service = PFCS_PCK_CONSTANTS.CD_DISCHARGE
                and svc.si_status <> PFCS_PCK_CONSTANTS.SI_STATUS_COMPLETED
                and svc.si_intent <> PFCS_PCK_CONSTANTS.SI_INTENT_ORDER))
        and rownum = 1;


    /*=========================================== Procedures ===========================================*/
    -- General Census
    PROCEDURE CALC_OCCUPANCY(cd_establishment_p number, nm_user_p varchar2) AS
        pfcs_panel_seq_w                pfcs_panel.nr_sequencia%type;
        nr_seq_indicator_w              pfcs_panel.nr_seq_indicator%type;
        pfcs_panel_detail_seq_w         pfcs_panel_detail.nr_sequencia%type;
        pfcs_detail_bed_seq_w           pfcs_detail_bed.nr_sequencia%type;

        ds_unit_w                       setor_atendimento.ds_setor_atendimento%type;
        cd_unit_w                       setor_atendimento.cd_setor_atendimento%type;
        cd_unit_classification_w        setor_atendimento.cd_classif_setor%type;

        qt_beds_w                       pls_integer  := 0;
        qt_census_beds_w                pls_integer  := 0;
        qt_occupied_beds_w              pls_integer  := 0;
        qt_blocked_beds_w               pls_integer  := 0;
        qt_available_beds_w             pls_integer  := 0;
        qt_discharge_orders_w           pls_integer  := 0;
        qt_discharge_delays_w           pls_integer  := 0;

        ie_occupied_w                   varchar2(1 char);
        ie_over_threshold_w             varchar2(1 char);
        ie_discharge_order_status_w     varchar2(1 char);
        ie_transfer_order_status_w      varchar2(1 char);

        qt_discharge_rule_w             pfcs_general_rule.qt_time_discharge%type;
        qt_transfer_rule_w              pfcs_general_rule.qt_time_transfer%type;
    BEGIN
        select nvl(max(qt_time_discharge),240)
        into qt_discharge_rule_w
        from pfcs_general_rule;

        select nvl(max(qt_time_transfer),240)
        into qt_transfer_rule_w
        from pfcs_general_rule;

        nr_seq_indicator_w := 100;
        for c01_w in cur_units(cd_establishment_p) loop
            ds_unit_w := c01_w.ds_unit;
            cd_unit_w := c01_w.cd_unit;
            cd_unit_classification_w := c01_w.cd_unit_classification;

            qt_beds_w := 0;
            qt_census_beds_w := 0;
            qt_occupied_beds_w := 0;
            qt_blocked_beds_w := 0;
            qt_available_beds_w := 0;
            qt_discharge_orders_w := 0;
            qt_discharge_delays_w := 0;

            for c02_w in cur_beds(cd_establishment_p, c01_w.cd_unit, c01_w.ds_unit) loop
                ie_occupied_w := PFCS_PCK_CONSTANTS.IE_NO;

                select pfcs_panel_detail_seq.nextval
                into pfcs_panel_detail_seq_w
                from dual;

                insert into pfcs_panel_detail(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    ie_situation,
                    nr_seq_indicator,
                    nr_seq_operational_level)
                values (
                    pfcs_panel_detail_seq_w,
                    nm_user_p,
                    sysdate,
                    nm_user_p,
                    sysdate,
                    'T',
                    nr_seq_indicator_w,
                    cd_establishment_p);

                insert into pfcs_detail_bed(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    nr_seq_detail,
                    ds_location,
                    cd_department,
                    ds_department,
                    cd_status,
                    ds_status,
                    ie_classification)
                values (
                    pfcs_detail_bed_seq.nextval,
                    nm_user_p,
                    sysdate,
                    nm_user_p,
                    sysdate,
                    pfcs_panel_detail_seq_w,
                    c02_w.ds_location,
                    c01_w.cd_unit,
                    c01_w.ds_unit,
                    c02_w.cd_status,
                    c02_w.ie_census,
                    cd_unit_classification_w);

                for c03_w in cur_patients(c02_w.nr_seq_interno, cd_unit_classification_w) loop
                    qt_occupied_beds_w := qt_occupied_beds_w + 1;
                    ie_occupied_w := PFCS_PCK_CONSTANTS.IE_YES;

                    ie_over_threshold_w := PFCS_PCK_CONSTANTS.IE_NO;
                    ie_discharge_order_status_w := PFCS_PCK_CONSTANTS.IE_NO;
                    ie_transfer_order_status_w := null;

                    if (c03_w.cd_svc_request = PFCS_PCK_CONSTANTS.CD_DISCHARGE and c03_w.cd_svc_intent = PFCS_PCK_CONSTANTS.SI_INTENT_PLAN) then
                        ie_discharge_order_status_w := PFCS_PCK_CONSTANTS.IE_YES;
                        qt_discharge_orders_w := qt_discharge_orders_w + 1;
                        if ( ((sysdate - c03_w.dt_svc_request) * 1440) > qt_discharge_rule_w ) then
                            qt_discharge_delays_w := qt_discharge_delays_w + 1;
                            ie_over_threshold_w := PFCS_PCK_CONSTANTS.IE_YES;
                        end if;
                    elsif (c03_w.cd_svc_request = PFCS_PCK_CONSTANTS.CD_TRANSFER) then
                        if (c03_w.cd_svc_intent = PFCS_PCK_CONSTANTS.SI_INTENT_PLAN) then
                            ie_transfer_order_status_w := 'O';
                            if ( ((sysdate - c03_w.dt_svc_request) * 1440) > qt_transfer_rule_w ) then
                                ie_over_threshold_w := PFCS_PCK_CONSTANTS.IE_YES;
                            end if;
                        elsif (c03_w.cd_svc_intent = PFCS_PCK_CONSTANTS.SI_INTENT_PROPOSAL) then
                            ie_transfer_order_status_w := 'P';
                        end if;
                    end if;

                    insert into pfcs_detail_patient(
                        nr_sequencia,
                        nm_usuario,
                        dt_atualizacao,
                        nm_usuario_nrec,
                        dt_atualizacao_nrec,
                        nr_seq_detail,
                        nr_encounter_varchar,
                        dt_entrance,
                        id_patient,
                        nm_patient,
                        ds_gender,
                        dt_birthdate,
                        ds_age_range,
                        ds_symptoms,
                        ds_dnr_status,
                        ds_physician,
                        ds_special_request,
                        ds_care_status,
                        ds_checklist,
                        ie_over_threshold,
                        dt_expected_discharge,
                        dt_request,
                        ds_discharge_disposition,
                        ie_discharge_order_status,
                        ie_transfer_order_status,
                        ie_freq_flyer,
                        ds_readmission_risk,
                        ds_readm_risk_contrb,
                        ds_rec_pat_adm_data,
                        ds_rec_pat_comorbd,
                        ds_rec_pat_reasons,
                        qt_edi_score,
                        ds_edi_vitals_warn,
                        ds_edi_contrb,
                        vl_los_remaining,
                        cd_classification,
                        nr_seq_encounter)
                    values (
                        pfcs_detail_patient_seq.nextval,
                        nm_user_p,
                        sysdate,
                        nm_user_p,
                        sysdate,
                        pfcs_panel_detail_seq_w,
                        c03_w.nr_encounter,
                        c03_w.dt_period_start,
                        c03_w.id_patient,
                        c03_w.nm_patient,
                        c03_w.ds_gender,
                        c03_w.dt_birthdate,
                        c03_w.qt_patient_age,
                        c03_w.ds_problem,
                        c03_w.cd_code_status,
                        c03_w.nm_physician,
                        c03_w.ds_special_requests,
                        c03_w.ds_care_status,
                        c03_w.ds_checklist,
                        ie_over_threshold_w,
                        c03_w.dt_expected_discharge,
                        c03_w.dt_svc_request,
                        c03_w.cd_disposition,
                        ie_discharge_order_status_w,
                        ie_transfer_order_status_w,
                        c03_w.ie_frequent_flyer,
                        c03_w.ds_readmission_risk,
                        c03_w.ds_readm_risk_contrb,
                        c03_w.ds_recur_pat_adm_data,
                        c03_w.ds_recur_pat_comorbd,
                        c03_w.ds_recur_pat_reasons,
                        c03_w.qt_trs,
                        c03_w.ds_edi_vitals_warn,
                        c03_w.ds_edi_contrb,
                        c03_w.vl_los_remaining,
                        c03_w.si_classif,
                        c03_w.nr_seq_encounter);
                end loop;

                if (c02_w.ie_census is not null) then
                    qt_beds_w := qt_beds_w + 1;
                end if;
                if (c02_w.ie_total_beds = 'Y') then
                    qt_census_beds_w := qt_census_beds_w + 1;
                end if;
                if (c02_w.ie_census = 'B') then
                    qt_blocked_beds_w := qt_blocked_beds_w + 1;
                end if;
                if (c02_w.ie_census = 'A' and ie_occupied_w = PFCS_PCK_CONSTANTS.IE_NO) then
                    qt_available_beds_w := qt_available_beds_w + 1;
                end if;
            end loop;

            pfcs_pck.pfcs_generate_results(
                ds_reference_value_p => ds_unit_w,
                cd_reference_value_p => cd_unit_w,
                cd_reference_aux_p => cd_unit_classification_w,
                vl_indicator_p => qt_census_beds_w,
                vl_indicator_help_p => qt_occupied_beds_w,
                nr_seq_indicator_p => nr_seq_indicator_w,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p,
                nr_seq_panel_p => pfcs_panel_seq_w);
            pfcs_pck.pfcs_generate_results(
                ds_reference_value_p => ds_unit_w,
                cd_reference_value_p => cd_unit_w,
                cd_reference_aux_p => cd_unit_classification_w,
                vl_indicator_p => qt_available_beds_w,
                vl_indicator_help_p => (qt_beds_w - qt_blocked_beds_w),
                nr_seq_indicator_p => 102,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p,
                nr_seq_panel_p => pfcs_panel_seq_w);
            pfcs_pck.pfcs_generate_results(
                ds_reference_value_p => ds_unit_w,
                cd_reference_value_p => cd_unit_w,
                cd_reference_aux_p => cd_unit_classification_w,
                vl_indicator_p => qt_blocked_beds_w,
                nr_seq_indicator_p => 111,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p,
                nr_seq_panel_p => pfcs_panel_seq_w);

            pfcs_pck.pfcs_generate_results(
                ds_reference_value_p => ds_unit_w,
                cd_reference_value_p => cd_unit_w,
                cd_reference_aux_p => cd_unit_classification_w,
                vl_indicator_p => qt_discharge_orders_w,
                nr_seq_indicator_p => 112,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p,
                nr_seq_panel_p => pfcs_panel_seq_w);
            pfcs_pck.pfcs_generate_results(
                ds_reference_value_p => ds_unit_w,
                cd_reference_value_p => cd_unit_w,
                cd_reference_aux_p => cd_unit_classification_w,
                vl_indicator_p => qt_discharge_delays_w,
                nr_seq_indicator_p => 110,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p,
                nr_seq_panel_p => pfcs_panel_seq_w);
        end loop;

        pfcs_pck.pfcs_update_detail(
                nr_seq_indicator_p => nr_seq_indicator_w,
                nr_seq_panel_p => pfcs_panel_seq_w,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p);

        pfcs_pck.pfcs_activate_records(
                nr_seq_indicator_p => nr_seq_indicator_w,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p);
        pfcs_pck.pfcs_activate_records(
                nr_seq_indicator_p => 102,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p);
        pfcs_pck.pfcs_activate_records(
                nr_seq_indicator_p => 111,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p);

        pfcs_pck.pfcs_activate_records(
                nr_seq_indicator_p => 112,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p);
        pfcs_pck.pfcs_activate_records(
                nr_seq_indicator_p => 110,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p);
    END CALC_OCCUPANCY;

    PROCEDURE CALC_HIST_OCCUPANCY(cd_establishment_p number, nm_user_p varchar2) AS
        pfcs_panel_seq_w    pfcs_panel.nr_sequencia%type;
        qt_total_unit_w     pls_integer := 0;
        qt_occupied_unit_w  pls_integer := 0;
    BEGIN
        select sum(vl_indicator)
        into qt_total_unit_w
        from pfcs_panel
        where nr_seq_indicator = 100
            and nr_seq_operational_level = cd_establishment_p;

        -- Occupancy for the last 5 hours
        for qt_hrs_inc in 0 .. 12 loop
            qt_occupied_unit_w := 0;

            select nvl(max(v.qt_value),0) into qt_occupied_unit_w
            from (
                select distinct count(hst.nr_seq_location) qt_value
                from pfcs_patient_loc_hist hst,
                    unidade_atendimento uni,
                    setor_atendimento	sec
                where uni.cd_setor_atendimento = sec.cd_setor_atendimento
                    and hst.nr_seq_location = uni.nr_seq_location
                    and sec.ie_situacao = PFCS_PCK_CONSTANTS.IE_ACTIVE
                    and uni.ie_situacao = PFCS_PCK_CONSTANTS.IE_ACTIVE
                    and sec.ie_ocup_hospitalar <> PFCS_PCK_CONSTANTS.IE_NO
                    and sec.cd_estabelecimento_base = cd_establishment_p
                    and hst.ie_situacao = PFCS_PCK_CONSTANTS.IE_ACTIVE
                    and hst.si_intent = PFCS_PCK_CONSTANTS.SI_INTENT_ORDER
                    and hst.dt_period_start is not null
                    and (sysdate - dividir(qt_hrs_inc,24)) between hst.dt_period_start and nvl(hst.dt_period_end, SYSDATE)
                    and hst.nr_seq_encounter is not null
                    and qt_hrs_inc > 0
                union all
                select sum(p.vl_indicator_help) qt_value
                from pfcs_panel p
                where p.nr_seq_operational_level = cd_establishment_p
                    and p.nr_seq_indicator = 100
                    and p.ie_situation = PFCS_PCK_CONSTANTS.IE_ACTIVE
                    and qt_hrs_inc = 0
            ) v;

            pfcs_pck.pfcs_generate_results(
                cd_reference_value_p => to_char(trunc((sysdate - (to_number(qt_hrs_inc))/24), 'hh24'), 'dd/mm/yyyy hh24:mi:ss'),
                ds_reference_value_p => 'rgb(33,185,255)',
                vl_indicator_p => pfcs_get_percentage_value(qt_occupied_unit_w, qt_total_unit_w),
                vl_indicator_help_p => qt_hrs_inc,
                cd_reference_aux_p => 'EV',
                nr_seq_indicator_p => 109,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p,
                nr_seq_panel_p => pfcs_panel_seq_w);
        end loop;

        -- Predicted occupancy for the next 12 hours
        for qt_hrs_inc in 1 .. 12 loop
            pfcs_pck.pfcs_generate_results(
            cd_reference_value_p => to_char(trunc((sysdate + (to_number(qt_hrs_inc))/24), 'hh24'), 'dd/mm/yyyy hh24:mi:ss'),
            ds_reference_value_p => 'rgb(40,124,166)',
            vl_indicator_p => pfcs_get_percentage_value(
                to_number(pfcs_pck_sim.get_simulation_value(cd_establishment_p, qt_hrs_inc, 'CEN', null, 'N', null)),
                qt_total_unit_w
            ),
            cd_reference_aux_p => 'EV',
            ds_reference_aux_p => 'SIM',
            nr_seq_indicator_p => 109,
            nr_seq_operational_level_p => cd_establishment_p,
            nm_usuario_p => nm_user_p,
            nr_seq_panel_p => pfcs_panel_seq_w);
        end loop;

        pfcs_pck.pfcs_activate_records(
            nr_seq_indicator_p => 109,
            nr_seq_operational_level_p => cd_establishment_p,
            nm_usuario_p => nm_user_p);
    END CALC_HIST_OCCUPANCY;

    PROCEDURE CALC_TOTAL_DISCHARGES_TODAY(cd_establishment_p number, nm_user_p varchar2) AS
        pfcs_panel_seq_w        pfcs_panel.nr_sequencia%type;
        qt_discharged_today_w   pls_integer;
    BEGIN
        for c01_w in cur_units(cd_establishment_p) loop
            select count(enc.nr_sequencia)
            into qt_discharged_today_w
            from pfcs_encounter enc,
                pfcs_patient pat,
                unidade_atendimento uni,
                setor_atendimento sec,
                pfcs_service_request svc
            where sec.cd_setor_atendimento = c01_w.cd_unit
                and sec.ie_ocup_hospitalar <> PFCS_PCK_CONSTANTS.IE_NO
                and uni.cd_setor_atendimento = sec.cd_setor_atendimento
                and uni.nr_seq_location = svc.nr_seq_location
                and enc.nr_seq_patient = pat.nr_sequencia
                and enc.period_start is not null
                and enc.period_end is not null
                and svc.nr_seq_encounter = enc.nr_sequencia
                and svc.nr_seq_patient = pat.nr_sequencia
                and svc.cd_service = PFCS_PCK_CONSTANTS.CD_DISCHARGE
                and svc.si_status = PFCS_PCK_CONSTANTS.SI_STATUS_COMPLETED
                and svc.si_intent = PFCS_PCK_CONSTANTS.SI_INTENT_ORDER
                and svc.dt_authored_on >= trunc(sysdate);

            pfcs_pck.pfcs_generate_results(
                ds_reference_value_p => c01_w.ds_unit,
                cd_reference_value_p => c01_w.cd_unit,
                cd_reference_aux_p => c01_w.cd_unit_classification,
                vl_indicator_p => qt_discharged_today_w,
                nr_seq_indicator_p => 215,
                nr_seq_operational_level_p => cd_establishment_p,
                nm_usuario_p => nm_user_p,
                nr_seq_panel_p => pfcs_panel_seq_w);
        end loop;

        pfcs_pck.pfcs_activate_records(
            nr_seq_indicator_p => 215,
            nr_seq_operational_level_p => cd_establishment_p,
            nm_usuario_p => nm_user_p);
    END CALC_TOTAL_DISCHARGES_TODAY;


    /*=========================================== Functions ===========================================*/
    FUNCTION GET_DISCHARGES_BY_NOON(
        cd_establishment_p number
    ) RETURN NUMBER AS
        qt_discharges_by_noon_w pls_integer;
        dt_start_time_w date;
        dt_end_time_w date;
    BEGIN
        select nvl2(max(hr_start_noon),
                trunc(sysdate)+(to_number(to_char(max(hr_start_noon), 'HH24'))/24),
                trunc(sysdate)+(6/24)
            ) dt_start_time,
            nvl2(max(hr_end_noon),
                trunc(sysdate)+(to_number(to_char(max(hr_end_noon), 'HH24'))/24),
                trunc(sysdate)+(6/24)
            ) dt_end_time
        into dt_start_time_w,
            dt_end_time_w
        from pfcs_general_rule;

        select count(enc.nr_sequencia)
        into qt_discharges_by_noon_w
        from pfcs_encounter enc,
            pfcs_patient pat,
            unidade_atendimento uni,
            setor_atendimento sec,
            pfcs_service_request svc
        where sec.cd_estabelecimento = cd_establishment_p
            and sec.ie_ocup_hospitalar <> PFCS_PCK_CONSTANTS.IE_NO
            and uni.cd_setor_atendimento = sec.cd_setor_atendimento
            and uni.nr_seq_location = svc.nr_seq_location
            and enc.nr_seq_patient = pat.nr_sequencia
            and enc.period_start is not null
            and enc.period_end is not null
            and svc.nr_seq_encounter = enc.nr_sequencia
            and svc.nr_seq_patient = pat.nr_sequencia
            and svc.cd_service = PFCS_PCK_CONSTANTS.CD_DISCHARGE
            and svc.si_status = PFCS_PCK_CONSTANTS.SI_STATUS_COMPLETED
            and svc.si_intent = PFCS_PCK_CONSTANTS.SI_INTENT_ORDER
            and (svc.dt_authored_on between dt_start_time_w and dt_end_time_w);
        return qt_discharges_by_noon_w;
    END GET_DISCHARGES_BY_NOON;

    FUNCTION GET_PR_DISCHARGES_BY_NOON(
        cd_establishment_p number
    ) RETURN NUMBER AS
        qt_discharges_today_w pls_integer;
    BEGIN
        select sum(vl_indicator)
        into qt_discharges_today_w
        from pfcs_panel
        where nr_seq_indicator = 215
            and nr_seq_operational_level = cd_establishment_p;
        return pfcs_get_percentage_value(get_discharges_by_noon(cd_establishment_p), nvl(qt_discharges_today_w, 0));
    END GET_PR_DISCHARGES_BY_NOON;

    FUNCTION GET_ANTICIPATED_DISCHARGES(
        cd_establishment_p number,
        cd_unit_p number,
        cd_unit_classification_p varchar2,
        dt_predicted_discharge_p date,
        ie_discharge_order_status_p varchar2
    ) RETURN NUMBER AS
        qt_anticipated_discharges_w pls_integer;
    BEGIN
        select count(pd.nr_sequencia)
        into qt_anticipated_discharges_w
        from pfcs_panel_detail pd,
            pfcs_detail_patient pat,
            pfcs_detail_bed bed
        where pd.ie_situation = PFCS_PCK_CONSTANTS.IE_ACTIVE
            and pd.nr_seq_indicator = 100
            and pd.nr_seq_operational_level = cd_establishment_p
            and (cd_unit_p is null or bed.cd_department = to_char(cd_unit_p))
            and (cd_unit_classification_p is null or bed.ie_classification = cd_unit_classification_p)
            and trunc(pat.dt_expected_discharge) = trunc(dt_predicted_discharge_p)
            and (ie_discharge_order_status_p is null or pat.ie_discharge_order_status = ie_discharge_order_status_p)
            and pat.nr_seq_detail = pd.nr_sequencia
            and bed.nr_seq_detail = pat.nr_seq_detail;
        return qt_anticipated_discharges_w;
    END GET_ANTICIPATED_DISCHARGES;

    FUNCTION GET_DISCHARGE_ORDERS(
        cd_establishment_p number,
        cd_unit_p number,
        cd_unit_classification_p varchar2,
        dt_period_start_p date,
        dt_period_end_p date,
        ie_over_threshold_p varchar2
    ) RETURN NUMBER AS
        qt_return_w pls_integer;
    BEGIN
        select count(pd.nr_sequencia)
        into qt_return_w
        from pfcs_panel_detail pd,
            pfcs_detail_patient pat,
            pfcs_detail_bed bed
        where pd.ie_situation = PFCS_PCK_CONSTANTS.IE_ACTIVE
            and pd.nr_seq_indicator = 100
            and pd.nr_seq_operational_level = cd_establishment_p
            and (cd_unit_p is null or bed.cd_department = to_char(cd_unit_p))
            and (cd_unit_classification_p is null or bed.ie_classification = cd_unit_classification_p)
            and pat.ie_discharge_order_status = 'Y'
            and ((ie_over_threshold_p is null) or (pat.ie_over_threshold = ie_over_threshold_p))
            and ((dt_period_start_p is null and dt_period_end_p is null) or (pat.dt_request between dt_period_start_p and dt_period_end_p))
            and pat.nr_seq_detail = pd.nr_sequencia
            and bed.nr_seq_detail = pat.nr_seq_detail;
        return qt_return_w;
    END GET_DISCHARGE_ORDERS;

    FUNCTION GET_AVG_DISCHARGE_ORDER_TIME(
        cd_establishment_p number,
        cd_unit_p number,
        cd_unit_classification_p varchar2
    ) RETURN DATE AS
        ie_stepdown_w varchar2(1);
        cd_unit_classification_w varchar2(2);
        dt_return_w date;
    BEGIN
        if (cd_unit_classification_p = PFCS_PCK_CONSTANTS.CD_TCU) then
            ie_stepdown_w := PFCS_PCK_CONSTANTS.IE_YES_BR;
            cd_unit_classification_w := PFCS_PCK_CONSTANTS.CD_ICU;
        else
            cd_unit_classification_w := cd_unit_classification_p;
        end if;

        select (sysdate - avg(sysdate - enc.dt_discharge_order))
        into dt_return_w
        from pfcs_encounter enc,
            pfcs_patient pat,
            unidade_atendimento uni,
            setor_atendimento sec
        where ((sec.cd_classif_setor = cd_unit_classification_w) or (cd_unit_classification_w is null and sec.ie_ocup_hospitalar <> PFCS_PCK_CONSTANTS.IE_NO))
            and (sec.cd_setor_atendimento = cd_unit_p or cd_unit_p is null)
            and sec.cd_estabelecimento = cd_establishment_p
            and (sec.ie_semi_intensiva = ie_stepdown_w or ie_stepdown_w is null)
            and uni.cd_setor_atendimento = sec.cd_setor_atendimento
            and uni.nr_seq_location = nvl(enc.nr_seq_location, pfcs_get_pat_location(pat.nr_sequencia, enc.nr_sequencia))
            and enc.nr_seq_patient = pat.nr_sequencia
            and enc.period_start is not null
            and enc.dt_discharge_order >= trunc(SYSDATE);
        return dt_return_w;
    END GET_AVG_DISCHARGE_ORDER_TIME;

    FUNCTION GET_AVG_DISCHARGE_TIME(
        cd_establishment_p number,
        cd_unit_p number,
        cd_unit_classification_p varchar2
    ) RETURN DATE AS
        ie_stepdown_w varchar2(1);
        cd_unit_classification_w varchar2(2);
        dt_average_time_w date;
    BEGIN
        if (cd_unit_classification_p = PFCS_PCK_CONSTANTS.CD_TCU) then
            ie_stepdown_w := PFCS_PCK_CONSTANTS.IE_YES_BR;
            cd_unit_classification_w := PFCS_PCK_CONSTANTS.CD_ICU;
        else
            cd_unit_classification_w := cd_unit_classification_p;
        end if;

        select (sysdate - avg(sysdate - svc.dt_authored_on))
        into dt_average_time_w
        from pfcs_encounter enc,
            pfcs_patient pat,
            unidade_atendimento uni,
            setor_atendimento sec,
            pfcs_service_request svc
        where ((sec.cd_classif_setor = cd_unit_classification_w) or (cd_unit_classification_w is null and sec.ie_ocup_hospitalar <> PFCS_PCK_CONSTANTS.IE_NO))
            and (sec.cd_setor_atendimento = cd_unit_p or cd_unit_p is null)
            and sec.cd_estabelecimento = cd_establishment_p
            and (sec.ie_semi_intensiva = ie_stepdown_w or ie_stepdown_w is null)
            and uni.cd_setor_atendimento = sec.cd_setor_atendimento
            and uni.nr_seq_location = svc.nr_seq_location
            and enc.nr_seq_patient = pat.nr_sequencia
            and enc.period_start is not null
            and enc.period_end is not null
            and svc.nr_seq_encounter = enc.nr_sequencia
            and svc.nr_seq_patient = pat.nr_sequencia
            and svc.cd_service = PFCS_PCK_CONSTANTS.CD_DISCHARGE
            and svc.si_status = PFCS_PCK_CONSTANTS.SI_STATUS_COMPLETED
            and svc.si_intent = PFCS_PCK_CONSTANTS.SI_INTENT_ORDER
            and svc.dt_authored_on >= trunc(sysdate);
        return dt_average_time_w;
    END GET_AVG_DISCHARGE_TIME;

    FUNCTION GET_AVG_LAG_TIME(
        cd_establishment_p number,
        cd_unit_p number,
        cd_unit_classification_p varchar2
    ) RETURN NUMBER AS
        vl_avg_lag_time_w pls_integer;
    BEGIN
        vl_avg_lag_time_w := ((
            nvl(
                GET_AVG_DISCHARGE_TIME(cd_establishment_p, cd_unit_p, cd_unit_classification_p),
                sysdate) - nvl(
                    GET_AVG_DISCHARGE_ORDER_TIME(cd_establishment_p, cd_unit_p, cd_unit_classification_p),
                    sysdate)
                ) * 1440);
        if (vl_avg_lag_time_w < 0) then
            vl_avg_lag_time_w := 0;
        end if;
        return vl_avg_lag_time_w;
    END GET_AVG_LAG_TIME;

    FUNCTION GET_CARE_STATUS(
        cd_establishment_p number,
        cd_unit_p number,
        cd_unit_classification_p varchar2,
        cd_category_p varchar2,
        ie_over_threshold_p varchar2
    ) RETURN NUMBER AS
        ie_stepdown_w varchar2(1);
        cd_unit_classification_w varchar2(2);
        qt_care_status_count_w pls_integer;
    BEGIN
        if (cd_unit_classification_p = PFCS_PCK_CONSTANTS.CD_TCU) then
            ie_stepdown_w := PFCS_PCK_CONSTANTS.IE_YES_BR;
            cd_unit_classification_w := PFCS_PCK_CONSTANTS.CD_ICU;
        else
            cd_unit_classification_w := cd_unit_classification_p;
        end if;

        select count(enc.nr_sequencia)
        into qt_care_status_count_w
        from pfcs_encounter enc,
            pfcs_patient pat,
            unidade_atendimento uni,
            setor_atendimento sec,
            pfcs_procedure prc,
            pfcs_service_request svc
        where uni.nr_seq_location = nvl(enc.nr_seq_location, pfcs_get_pat_location(pat.nr_sequencia, enc.nr_sequencia))
            and uni.cd_setor_atendimento = sec.cd_setor_atendimento
            and uni.ie_situacao = PFCS_PCK_CONSTANTS.IE_ACTIVE
            and sec.ie_situacao = PFCS_PCK_CONSTANTS.IE_ACTIVE
            and ((sec.cd_classif_setor = cd_unit_classification_w) or (cd_unit_classification_w is null and sec.ie_ocup_hospitalar <> 'N'))
            and ((sec.cd_setor_atendimento = cd_unit_p) or (cd_unit_p is null))
            and (sec.ie_semi_intensiva = ie_stepdown_w or ie_stepdown_w is null)
            and sec.cd_estabelecimento = cd_establishment_p
            and enc.nr_seq_patient = pat.nr_sequencia
            and enc.period_start is not null
            and prc.nr_seq_encounter = enc.nr_sequencia
            and prc.period_start is not null
            and upper(prc.si_status) = 'IN-PROGRESS'
            and svc.nr_seq_encounter = enc.nr_sequencia
            and svc.nr_seq_patient = pat.nr_sequencia
            and ((svc.cd_service = PFCS_PCK_CONSTANTS.CD_ADMISSION
                    and svc.si_status = PFCS_PCK_CONSTANTS.SI_STATUS_COMPLETED
                    and svc.si_intent = PFCS_PCK_CONSTANTS.SI_INTENT_ORDER) or
                svc.cd_service = PFCS_PCK_CONSTANTS.CD_TRANSFER or
                (svc.cd_service = PFCS_PCK_CONSTANTS.CD_DISCHARGE
                    and svc.si_status <> PFCS_PCK_CONSTANTS.SI_STATUS_COMPLETED
                    and svc.si_intent <> PFCS_PCK_CONSTANTS.SI_INTENT_ORDER))
            and ((prc.period_end < sysdate) or (nvl(ie_over_threshold_p, PFCS_PCK_CONSTANTS.IE_NO) = PFCS_PCK_CONSTANTS.IE_NO))
            and ((upper(prc.cd_category) = upper(cd_category_p)) or (cd_category_p is null));
        return qt_care_status_count_w;
    END GET_CARE_STATUS;

    FUNCTION GET_SPECIAL_REQUESTS(
        cd_establishment_p number,
        cd_unit_p number,
        cd_unit_classification_p varchar2
    ) RETURN NUMBER AS
        qt_special_requests_w pls_integer;
    BEGIN
        select count(pd.nr_sequencia)
        into qt_special_requests_w
        from pfcs_panel_detail pd,
            pfcs_detail_patient pat,
            pfcs_detail_bed bed
        where pat.nr_seq_detail = pd.nr_sequencia
            and bed.nr_seq_detail = pd.nr_sequencia
            and pat.ds_special_request is not null
            and pd.nr_seq_indicator = 100
            and (cd_unit_p is null or bed.cd_department = to_char(cd_unit_p))
            and (cd_unit_classification_p is null or bed.ie_classification = cd_unit_classification_p)
            and pd.nr_seq_operational_level = cd_establishment_p;
        return qt_special_requests_w;
    END GET_SPECIAL_REQUESTS;

END PFCS_PCK_CENSUS;
/
