create or replace
package tasy_integracao is

type campos_nome is record(
	identificador_dw           number(10),
	identificador_sigh		number,
	identificador_incor		number,
	identificador_icr		number,
	identificador_mmed		number,
	rghc				varchar2(20),
	digito_rghc			varchar2(1),
	nome				varchar2(100),
	sexo				varchar2(1),
	data_nascimento			date,
	cor				varchar2(10),
	estado_civil			varchar2(10),
	ocupacao			varchar2(120),
	numero_identidade		varchar2(30),
	orgao_emissor_identidade	varchar2(20),
	numero_cpf			varchar2(30),
	nacionalidade			varchar2(20),
	municipio_nascimento		varchar2(40),
	uf_nascimento			varchar2(20),
	endereco			varchar2(70),
	tipo_endereco			varchar2(10),
	numero_endereco			varchar2(12),
	complemento_endereco		varchar2(30),
	bairro_endereco			varchar2(40),
	distrito_endereco		varchar2(40),
	municipio_endereco		varchar2(40),
	cep_residencia			varchar2(8),
	uf_endereco			varchar2(20),
	pais_endereco			varchar2(20),
	data_rghc			date,
	nome_pai			varchar2(60),
	nome_mae			varchar2(60),
	telefone_residencial		varchar2(40),
	telefone_comercial		varchar2(40),
	telefone_celular		varchar2(40),
	email				varchar2(60),
	grau_instrucao			varchar2(20),
	religiao			varchar2(10),
	numero_cns			varchar2(30),
	nr_seq_cor_pele			number(10),
	nr_seq_religiao			number(10),
	ie_grau_instrucao		number(2),
	ie_estado_civil			varchar2(2),
	cd_nacionalidade		varchar2(8),
	cd_municipio_ibge		varchar2(6),
	cd_municipio_end		varchar2(6),
	nr_seq_pais			number(10));

type vetor_nome is table of campos_nome index by binary_integer;

TYPE campos_matricula is record(
	identificador_dw		number(10),
	rghc				varchar2(20),
	digito_rghc			varchar2(1),     
	nome				varchar2(100),        
	sexo				varchar2(1),         
	data_nascimento			date,            
	cor				varchar2(10),        
	estado_civil			varchar2(10),        
	ocupacao			varchar2(120),       
	numero_identidade		varchar2(30),
	orgao_emissor_identidade	varchar2(20),
	numero_cpf			varchar2(30),
	nacionalidade			varchar2(20),        
	municipio_nascimento		varchar2(40),        
	uf_nascimento			varchar2(2),         
	endereco			varchar2(70),
	tipo_endereco			varchar2(10),
	numero_endereco			varchar2(12),
	complemento_endereco		varchar2(30),
	bairro_endereco			varchar2(40),
	distrito_endereco		varchar2(40),        
	municipio_endereco		varchar2(40),
	cep_residencia			varchar2(8),
	uf_endereco			varchar2(20),
	pais_endereco			varchar2(20),
	data_rghc			date,            
	nome_pai			varchar2(60),
	nome_mae			varchar2(60),
	telefone_residencial		varchar2(40),
	telefone_comercial		varchar2(40),
	telefone_celular		varchar2(40),
	email				varchar2(60),
	grau_instrucao			varchar2(20),        
	religiao			varchar2(10),        
	numero_cns			varchar2(30),	
	ie_sigh         		number(10),
	ie_incor			number(10),
	ie_icr				number(10),
	ie_mmed				number(10),
	nr_seq_cor_pele			number(10),
	nr_seq_religiao			number(10),
	ie_grau_instrucao		number(2),
	ie_estado_civil			varchar2(2),
	cd_nacionalidade		varchar2(8),
	cd_municipio_ibge		varchar2(6),
	cd_municipio_end		varchar2(6),
	nr_seq_pais			number(10));

type vetor_matricula is table of campos_matricula index by binary_integer;

TYPE C01_REF is REF CURSOR;

procedure obter_cursor_matricula(nr_matricula_p	varchar2,
				 vetor_pessoa_p out vetor_matricula); 
				 
procedure obter_cursor_nome(	nm_pessoa_fisica_p	varchar2,
				nm_mae_p		varchar2,
				nm_pai_p		varchar2,
				dt_nascimento_p		date,
				vetor_pessoa_p out vetor_nome);


end tasy_integracao;
/
create or replace
package body tasy_integracao is

	procedure obter_cursor_matricula(	nr_matricula_p	varchar2,
						vetor_pessoa_p out vetor_matricula) is

	C01	neti.DW$consulta_paciente.RET_MATRICULA;
	vet01	C01%RowType;
	c02	c01_ref;
	nr_sequencia_w	number(10);
	i	integer	:= 0;
	begin

	neti.DW$CONSULTA_PACIENTE.DW$consulta_paciente_matricula(nr_matricula_p,C01);
	loop
	fetch c01 into
		vet01;
	exit when c01%notfound;
		i	:= i + 1;
		
		vetor_pessoa_p(i).nome				:= vet01.nome;
		vetor_pessoa_p(i).IDENTIFICADOR_DW		:= vet01.IDENTIFICADOR_DW;	
		vetor_pessoa_p(i).rghc				:= vet01.rghc;
		vetor_pessoa_p(i).digito_rghc			:= vet01.digito_rghc;
		vetor_pessoa_p(i).nome				:= vet01.nome;
		vetor_pessoa_p(i).sexo				:= vet01.sexo;
		vetor_pessoa_p(i).data_nascimento		:= vet01.data_nascimento;
		vetor_pessoa_p(i).cor				:= vet01.cor;
		vetor_pessoa_p(i).estado_civil			:= vet01.estado_civil;
		vetor_pessoa_p(i).ocupacao			:= vet01.ocupacao;
		vetor_pessoa_p(i).numero_identidade		:= vet01.numero_identidade;
		vetor_pessoa_p(i).orgao_emissor_identidade	:= vet01.orgao_emissor_identidade;
		vetor_pessoa_p(i).numero_cpf			:= vet01.numero_cpf;
		vetor_pessoa_p(i).nacionalidade			:= vet01.nacionalidade;
		vetor_pessoa_p(i).municipio_nascimento		:= vet01.municipio_nascimento;
		vetor_pessoa_p(i).uf_nascimento			:= vet01.uf_nascimento;
		vetor_pessoa_p(i).endereco			:= vet01.endereco;
		vetor_pessoa_p(i).tipo_endereco			:= vet01.tipo_endereco;
		vetor_pessoa_p(i).numero_endereco		:= vet01.numero_endereco;
		vetor_pessoa_p(i).complemento_endereco		:= vet01.complemento_endereco;
		vetor_pessoa_p(i).bairro_endereco		:= vet01.bairro_endereco;
		vetor_pessoa_p(i).distrito_endereco		:= vet01.distrito_endereco;
		vetor_pessoa_p(i).municipio_endereco		:= vet01.municipio_endereco;
		vetor_pessoa_p(i).cep_residencia		:= vet01.cep_residencia;
		vetor_pessoa_p(i).uf_endereco			:= vet01.uf_endereco;
		vetor_pessoa_p(i).pais_endereco			:= vet01.pais_endereco;
		vetor_pessoa_p(i).data_rghc			:= vet01.data_rghc;
		vetor_pessoa_p(i).nome_pai			:= vet01.nome_pai;
		vetor_pessoa_p(i).nome_mae			:= vet01.nome_mae;
		vetor_pessoa_p(i).telefone_residencial		:= vet01.telefone_residencial;
		vetor_pessoa_p(i).telefone_comercial		:= vet01.telefone_comercial;
		vetor_pessoa_p(i).telefone_celular		:= vet01.telefone_celular;
		vetor_pessoa_p(i).email				:= vet01.email;
		vetor_pessoa_p(i).grau_instrucao		:= vet01.grau_instrucao;
		vetor_pessoa_p(i).religiao			:= vet01.religiao;
		vetor_pessoa_p(i).numero_cns			:= vet01.numero_cns;
		vetor_pessoa_p(i).ie_sigh			:= vet01.identificador_sigh;
		vetor_pessoa_p(i).ie_incor			:= vet01.identificador_incor;
		vetor_pessoa_p(i).ie_icr			:= vet01.identificador_icr;
		vetor_pessoa_p(i).ie_mmed			:= vet01.identificador_mmed;
		
	end loop;
	close c01;
	end;
	
	procedure obter_cursor_nome(	nm_pessoa_fisica_p	varchar2,
					nm_mae_p		varchar2,
					nm_pai_p		varchar2,
					dt_nascimento_p		date,
					vetor_pessoa_p out vetor_nome) is
				
	C01	neti.DW$consulta_paciente.RET_FONETICA;
	vet01	C01%RowType;
	i	integer := 0;
	
	begin
	neti.DW$CONSULTA_PACIENTE.DW$consulta_paciente_nome(	nm_pessoa_fisica_p,
								'P',
								nm_mae_p,
								nm_pai_p,
								dt_nascimento_p,C01);
	loop
	fetch c01 into
		vet01;
	exit when c01%notfound;
		i	:= i + 1;
		
		vetor_pessoa_p(i).nome			:= vet01.nome;
		vetor_pessoa_p(i).IDENTIFICADOR_DW	:= vet01.IDENTIFICADOR_DW;	
		vetor_pessoa_p(i).rghc			:= vet01.rghc;
		vetor_pessoa_p(i).digito_rghc			:= vet01.digito_rghc;
		vetor_pessoa_p(i).nome			:= vet01.nome;
		vetor_pessoa_p(i).sexo			:= vet01.sexo;
		vetor_pessoa_p(i).data_nascimento		:= vet01.data_nascimento;
		vetor_pessoa_p(i).cor			:= vet01.cor;
		vetor_pessoa_p(i).estado_civil			:= vet01.estado_civil;
		vetor_pessoa_p(i).ocupacao			:= vet01.ocupacao;
		vetor_pessoa_p(i).numero_identidade		:= vet01.numero_identidade;
		vetor_pessoa_p(i).orgao_emissor_identidade	:= vet01.orgao_emissor_identidade;
		vetor_pessoa_p(i).numero_cpf			:= vet01.numero_cpf;
		vetor_pessoa_p(i).nacionalidade		:= vet01.nacionalidade;
		vetor_pessoa_p(i).municipio_nascimento		:= vet01.municipio_nascimento;
		vetor_pessoa_p(i).uf_nascimento		:= vet01.uf_nascimento;
		vetor_pessoa_p(i).endereco			:= vet01.endereco;
		vetor_pessoa_p(i).tipo_endereco		:= vet01.tipo_endereco;
		vetor_pessoa_p(i).numero_endereco		:= vet01.numero_endereco;
		vetor_pessoa_p(i).complemento_endereco	:= vet01.complemento_endereco;
		vetor_pessoa_p(i).bairro_endereco		:= vet01.bairro_endereco;
		vetor_pessoa_p(i).distrito_endereco		:= vet01.distrito_endereco;
		vetor_pessoa_p(i).municipio_endereco		:= vet01.municipio_endereco;
		vetor_pessoa_p(i).cep_residencia		:= vet01.cep_residencia;
		vetor_pessoa_p(i).uf_endereco		:= vet01.uf_endereco;
		vetor_pessoa_p(i).pais_endereco		:= vet01.pais_endereco;
		vetor_pessoa_p(i).data_rghc			:= vet01.data_rghc;
		vetor_pessoa_p(i).nome_pai			:= vet01.nome_pai;
		vetor_pessoa_p(i).nome_mae			:= vet01.nome_mae;
		vetor_pessoa_p(i).telefone_residencial		:= vet01.telefone_residencial;
		vetor_pessoa_p(i).telefone_comercial		:= vet01.telefone_comercial;
		vetor_pessoa_p(i).telefone_celular		:= vet01.telefone_celular;
		vetor_pessoa_p(i).email			:= vet01.email;
		vetor_pessoa_p(i).grau_instrucao		:= vet01.grau_instrucao;
		vetor_pessoa_p(i).religiao			:= vet01.religiao;
		vetor_pessoa_p(i).numero_cns		:= vet01.numero_cns;
		vetor_pessoa_p(i).identificador_dw		:= vet01.identificador_dw;
		vetor_pessoa_p(i).identificador_sigh		:= vet01.identificador_sigh;
		vetor_pessoa_p(i).identificador_incor		:= vet01.identificador_incor;
		vetor_pessoa_p(i).identificador_icr		:= vet01.identificador_icr;
		vetor_pessoa_p(i).identificador_mmed		:= vet01.identificador_mmed;
		
	end loop;
	close c01;
	end;
				
end tasy_integracao;
/