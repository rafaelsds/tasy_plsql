create or replace 
package pls_imp_xml_pacote_pck as

procedure criar_lote_importacao(	nm_usuario_p      	in 	usuario.nm_usuario%type,
					nr_seq_lote_p		out 	pls_imp_lote_pacote.nr_sequencia%type);
				
procedure incluir_linha_xml(		ie_tipo_registro_p	in	varchar2,
					ds_linha_p		in	varchar2,
					nr_seq_lote_p		in 	pls_imp_lote_pacote.nr_sequencia%type);
					
procedure gerar_pacotes(		nr_seq_lote_p		in 	pls_imp_lote_pacote.nr_sequencia%type,
					nm_usuario_p      	in 	usuario.nm_usuario%type);
				
procedure limpar_tabela_temp(		nr_seq_lote_p		in 	pls_imp_lote_pacote.nr_sequencia%type);

procedure exclui_lote_importacao(	nr_seq_lote_p		in 	pls_imp_lote_pacote.nr_sequencia%type);

end pls_imp_xml_pacote_pck;
/
create or replace 
package body pls_imp_xml_pacote_pck as

procedure criar_lote_importacao(nm_usuario_p	in 	usuario.nm_usuario%type,
				nr_seq_lote_p	out 	pls_imp_lote_pacote.nr_sequencia%type) is
				
nr_seq_lote_w		pls_imp_lote_pacote.nr_sequencia%type;

begin

insert into pls_imp_lote_pacote
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec)
values (pls_imp_lote_pacote_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p) returning nr_sequencia into nr_seq_lote_w;

commit;
	
nr_seq_lote_p := nr_seq_lote_w;

end criar_lote_importacao;

procedure incluir_linha_xml(	ie_tipo_registro_p	in	varchar2,
				ds_linha_p		in	varchar2,
				nr_seq_lote_p		in 	pls_imp_lote_pacote.nr_sequencia%type) is

begin

insert into w_pls_pacote(
	nr_sequencia,
	nr_seq_imp_lote_pacote,
	ie_tipo_registro,
	ds_linha)
values(	w_pls_pacote_seq.nextval,
	nr_seq_lote_p,
	ie_tipo_registro_p,
	ds_linha_p);

commit;

end incluir_linha_xml;

procedure gerar_pacotes(	nr_seq_lote_p	in 	pls_imp_lote_pacote.nr_sequencia%type,
				nm_usuario_p	in 	usuario.nm_usuario%type) is

cursor c01 (	nr_seq_lote_pc	pls_imp_lote_pacote.nr_sequencia%type) is
	select	ie_tipo_registro,
		ds_linha
	from	w_pls_pacote
	where	nr_seq_imp_lote_pacote = nr_seq_lote_pc
	order by nr_sequencia;

nr_seq_imp_pacote_w	pls_imp_pacote.nr_sequencia%type;
dt_inicio_vigencia_w	pls_imp_pacote.dt_inicio_vigencia%type;
cd_tipo_tabela_imp_w	pls_imp_pacote.cd_tipo_tabela_imp%type;
cd_pacote_w		pls_imp_pacote.cd_pacote%type;
cd_tipo_tabela_comp_w	pls_imp_pacote.cd_tipo_tabela_comp%type;
cd_composicao_w		pls_imp_pacote.cd_composicao%type;
qt_composicao_w		pls_imp_pacote.qt_composicao%type;
vl_unit_composicao_w	pls_imp_pacote.vl_unit_composicao%type;

begin

for r_C01_w in C01( nr_seq_lote_p ) loop

	if	(r_C01_w.ie_tipo_registro = 1) then
		
		select	to_date(to_char(pls_extrair_dado_tag_xml(r_C01_w.ds_linha,'<InicioVigencia>')),'dd/mm/yyyy hh24:mi:ss'),
			pls_extrair_dado_tag_xml(r_C01_w.ds_linha,'<TipoTabelaServico>'),
			to_char(to_number(pls_extrair_dado_tag_xml(r_C01_w.ds_linha,'<Servico>')))
		into	dt_inicio_vigencia_w,
			cd_tipo_tabela_imp_w,
			cd_pacote_w
		from	dual;
		
	elsif	(r_C01_w.ie_tipo_registro = 2) then
	
		select	pls_extrair_dado_tag_xml(r_C01_w.ds_linha,'<TipoTabelaServico>'),
			pls_extrair_dado_tag_xml(r_C01_w.ds_linha,'<Composicao>'),
			to_number(pls_extrair_dado_tag_xml(replace(r_C01_w.ds_linha,'.',','),'<Quantidade>')),
			nvl(to_number(replace(pls_extrair_dado_tag_xml(r_C01_w.ds_linha,'<Valor>'),'.',',')),0)
		into	cd_tipo_tabela_comp_w,
			cd_composicao_w,
			qt_composicao_w,
			vl_unit_composicao_w
		from	dual;
		
		insert into pls_imp_pacote(
			nr_sequencia,			dt_atualizacao,		nm_usuario,
			dt_atualizacao_nrec,		nm_usuario_nrec,	nr_seq_lote_pct,
			dt_inicio_vigencia,		cd_tipo_tabela_imp,	cd_pacote,
			cd_tipo_tabela_comp,		cd_composicao,		qt_composicao,
			vl_unit_composicao,		vl_tot_composicao
		)values(pls_imp_pacote_seq.nextval,	sysdate,		nm_usuario_p,
			sysdate,			nm_usuario_p,		nr_seq_lote_p,
			dt_inicio_vigencia_w,		cd_tipo_tabela_imp_w,	cd_pacote_w,
			cd_tipo_tabela_comp_w,		cd_composicao_w,	qt_composicao_w,
			vl_unit_composicao_w,		qt_composicao_w * vl_unit_composicao_w);
	end if;
end loop;

commit;

end gerar_pacotes;

procedure limpar_tabela_temp(	nr_seq_lote_p	in 	pls_imp_lote_pacote.nr_sequencia%type) is

begin

delete 	from w_pls_pacote
where	nr_seq_imp_lote_pacote = nr_seq_lote_p;

commit;

end limpar_tabela_temp;

procedure exclui_lote_importacao(	nr_seq_lote_p	in 	pls_imp_lote_pacote.nr_sequencia%type) is

begin

delete 	pls_imp_lote_pacote a
where	a.nr_sequencia = nr_seq_lote_p
and not exists(	select	1
		from	pls_imp_pacote b
		where	b.nr_seq_lote_pct = a.nr_sequencia);

commit;

end exclui_lote_importacao;

end pls_imp_xml_pacote_pck;
/