create or replace 
package usuario_resp_pck as

procedure Popular_regras;

procedure popular_regra_usuario;
				
procedure popular_regra_item;			

procedure obter_usuario_responsavel(	cd_convenio_p 			number,
					cd_estabelecimento_p 		number,
					nr_seq_propaci_p		number,
					nr_seq_matpaci_p		number,
					cd_material_atend_paciente_p	number,
					cd_procedimento_pac_p		number,
					ie_origem_proced_pac_p		number,
					nr_interno_conta_p		number,
					cd_autorizacao_p		varchar2,
					nr_seq_protocolo_p		number,
					nm_usuario_p			varchar2,
					nm_usuario_resp 	out varchar2 );
					
procedure inserir_usuario_resp_guia(nm_usuario_p		varchar2,
				    nr_seq_protocolo_p		number,
				    nm_usuario_responsavel_p	varchar2,
				    nr_interno_conta_p		number,
				    nr_seq_regra_p		number,
				    nr_doc_convenio_p		varchar2); 				
					
procedure limpa_listas;	
			
procedure printlog(ds_log_p in varchar2);

end usuario_resp_pck;
/


create or replace 
package body usuario_resp_pck as
i pls_integer := 0;

type regra_prev_t is record (	NR_SEQUENCIA          NUMBER(10),   
				CD_ESTABELECIMENTO     NUMBER(4),    
				CD_CONVENIO            NUMBER(10),     
				IE_TIPO_REGRA 	       varchar2(2));
type regra_prev_v is table of regra_prev_t index by binary_integer;
regra_prev_w		regra_prev_v;


type regra_prev_usu_t is record (nr_seq_prev_analise	number(10),
				NR_SEQUENCIA          NUMBER(10),   
				CD_PROCEDIMENTO        NUMBER(15),   
				CD_MATERIAL            NUMBER(10),   
				IE_ORIGEM_PROCED       NUMBER(10),   
				CD_GRUPO_MATERIAL      NUMBER(3),    
				CD_SUBGRUPO_MATERIAL   NUMBER(3),    
				CD_CLASSE_MATERIAL     NUMBER(5),    
				CD_AREA_PROCEDIMENTO   NUMBER(15),  
				CD_ESPECIALIDADE       NUMBER(15),   
				CD_GRUPO_PROC          NUMBER(15),
				nm_usuario_resp		varchar2(15),
				qt_itens_pendentes	number(10),
				qt_pontos_itens		number(10));
type regra_prev_usu_v is table of regra_prev_usu_t index by binary_integer;
regra_prev_usu_w		regra_prev_usu_v;
regra_prev_usu_ww		regra_prev_usu_v;
regra_prev_usu_item_w		regra_prev_usu_v;
type regra_prev_item_t is record (nr_seq_prev_analise_usu number(10),
				NR_SEQUENCIA          NUMBER(10),   
				CD_PROCEDIMENTO        NUMBER(15),   
				CD_MATERIAL            NUMBER(10),   
				IE_ORIGEM_PROCED       NUMBER(10),   
				CD_GRUPO_MATERIAL      NUMBER(3),    
				CD_SUBGRUPO_MATERIAL   NUMBER(3) ,   
				CD_CLASSE_MATERIAL     NUMBER(5),    
				CD_AREA_PROCEDIMENTO   NUMBER(15),   
				CD_ESPECIALIDADE       NUMBER(15),   
				CD_GRUPO_PROC          NUMBER(15));
type regra_prev_item_v is table of regra_prev_item_t index by binary_integer;
regra_prev_item_w		regra_prev_item_v;


procedure Popular_regras  as
nr_seq_regra_w		number(10);

begin
i := 0;

if	(regra_prev_w.count = 0) then
	
	i := 0;
	FOR r in (	select	nr_sequencia,
			cd_convenio,
			cd_estabelecimento,
			ie_tipo_regra
		from	regra_prev_analise_grg
		where	ie_situacao = 'A') loop
		
		regra_prev_w(i).NR_SEQUENCIA 		:= r.nr_sequencia;
		regra_prev_w(i).cd_estabelecimento 	:= r.cd_estabelecimento;
		regra_prev_w(i).cd_convenio 		:= r.cd_convenio;
		regra_prev_w(i).ie_tipo_regra 		:=  r.ie_tipo_regra;
		i := i + 1;
	end loop;

end if;
popular_regra_usuario;
popular_regra_item;	
	

end;

procedure popular_regra_usuario as

begin
i := 0;

if	(regra_prev_usu_w.count = 0) then	
	
	
	For r in (	select	a.NR_SEQUENCIA,  
			a.NM_USUARIO_RESP,
			a.nr_seq_prev_analise,
			(select  count(*)
			from	lote_audit_hist_item x
			where	x.NM_USUARIO_PREVISTO = a.NM_USUARIO_RESP
			and	x.nm_usuario_resp is null) qt_itens_pendentes
		from	regra_prev_analise_usu a
		where	a.ie_situacao = 'A') loop

		
		regra_prev_usu_w(i).nr_seq_prev_analise		:= r.nr_seq_prev_analise;
		regra_prev_usu_w(i).nr_sequencia		:= r.nr_sequencia;
		regra_prev_usu_w(i).nm_usuario_resp		:= r.nm_usuario_resp;		
		regra_prev_usu_w(i).qt_itens_pendentes		:= r.qt_itens_pendentes;
		regra_prev_usu_w(i).qt_pontos_itens		:= 0;
		i := i + 1;

	end loop;

end if;
end;

procedure popular_regra_item as

begin
i := 0;

if	(regra_prev_item_w.count = 0) then

	For r in (select NR_SEQUENCIA, 
			CD_PROCEDIMENTO,  
			CD_MATERIAL,
			IE_ORIGEM_PROCED,  
			CD_GRUPO_MATERIAL,    
			CD_SUBGRUPO_MATERIAL,  
			CD_CLASSE_MATERIAL,  
			CD_AREA_PROCEDIMENTO,
			CD_ESPECIALIDADE,  
			CD_GRUPO_PROC,
			NR_SEQ_PREV_ANALISE_USU
		from	regra_prev_analise_item
		where	1 = 1 
		and	ie_situacao  = 'A') loop

	
	regra_prev_item_w(i).nr_seq_prev_analise_usu	:= r.NR_SEQ_PREV_ANALISE_USU;
	regra_prev_item_w(i).nr_sequencia		:= r.nr_sequencia;
	regra_prev_item_w(i).cd_procedimento		:= r.cd_procedimento;
	regra_prev_item_w(i).cd_material		:= r.cd_material;
	regra_prev_item_w(i).ie_origem_proced		:= r.ie_origem_proced;
	regra_prev_item_w(i).cd_grupo_material		:= r.cd_grupo_material;
	regra_prev_item_w(i).cd_subgrupo_material	:= r.cd_subgrupo_material;	
	regra_prev_item_w(i).cd_classe_material		:= r.cd_classe_material;
	regra_prev_item_w(i).cd_area_procedimento	:= r.cd_area_procedimento;
	regra_prev_item_w(i).cd_especialidade		:= r.cd_especialidade;
	regra_prev_item_w(i).cd_grupo_proc		:= r.cd_grupo_proc;
	
	
	i := i + 1;

	end loop;

end if;

		
end;

Procedure obter_usuario_responsavel(	cd_convenio_p 			number,
					cd_estabelecimento_p 		number,
					nr_seq_propaci_p		number,
					nr_seq_matpaci_p		number,
					cd_material_atend_paciente_p	number,
					cd_procedimento_pac_p		number,
					ie_origem_proced_pac_p		number,
					nr_interno_conta_p		number,
					cd_autorizacao_p		varchar2,
					nr_seq_protocolo_p		number,
					nm_usuario_p			varchar2,
					nm_usuario_resp  out varchar2 ) as
nr_seq_regra_w		number(10);
ie_tipo_regra_w		varchar2(2);
qt_regra_usuario_w	number(10);
cd_procedimento_w	procedimento.cd_procedimento%type;
ie_origem_proced_w	procedimento.ie_origem_proced%type;
nr_interno_conta_w	conta_paciente.nr_interno_conta%type;
nr_doc_convenio_w	conta_paciente_guia.cd_autorizacao%type;
nr_seq_protocolo_w	protocolo_convenio.nr_seq_protocolo%type;
cd_area_procedimento_w	area_procedimento.cd_area_procedimento%type;
cd_grupo_proc_w		grupo_proc.cd_grupo_proc%type;
cd_espealidade_w	especialidade_proc.cd_especialidade%type;

cd_material_w		material.cd_material%type;
cd_grupo_material_w	grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w	subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w	classe_material.cd_classe_material%type;
nm_usuario_retorno_w	varchar2(15);
nm_usuario_item_w	varchar2(15);	
nm_usuario_gravado_w	varchar2(15);	
qt_vetor_aux_w		number(10) := 0;
nr_seq_guia_w		NUMBER(10);
nr_seq_zero_w		number(10);
qt_itens_pendentes_w	number(10);
nm_usuario_zero_w	varchar2(15);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
cd_convenio_w		convenio.cd_convenio%type;
qt_vetor_itens_w	number(10) := 0;
ds_nao_informado_w	varchar2(20) := substr(obter_desc_expressao(778770),1,20);
begin
Popular_regras;
cd_convenio_w 		:= null;
cd_estabelecimento_w 	:= null;
if	(nr_seq_propaci_p > 0) then
	begin
	select 	a.cd_procedimento,
		a.ie_origem_proced,
		a.nr_interno_conta,
		Nvl(a.nr_doc_convenio,ds_nao_informado_w) nr_doc_convenio,
		b.nr_seq_protocolo,
		e.cd_area_procedimento,
		e.cd_grupo_proc,
		e.cd_especialidade,
		b.cd_convenio_parametro,
		b.cd_estabelecimento
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_interno_conta_w,
		nr_doc_convenio_w,
		nr_seq_protocolo_w,
		cd_area_procedimento_w,
		cd_grupo_proc_w,
		cd_espealidade_w,
		cd_convenio_w,
		cd_estabelecimento_w
	from	procedimento_paciente a,
		conta_paciente b,
		estrutura_procedimento_v e
	where	a.nr_sequencia = nr_seq_propaci_p
	and	a.nr_interno_conta = b.nr_interno_conta
	and	a.cd_procedimento = e.cd_procedimento
	and	a.ie_origem_proced = e.ie_origem_proced;
	exception
	when others then
		cd_convenio_w := null;
	end;

elsif	(nr_seq_matpaci_p > 0) then
	begin
	select 	a.cd_material,
		a.nr_interno_conta,
		Nvl(a.nr_doc_convenio,ds_nao_informado_w) nr_doc_convenio,
		b.nr_seq_protocolo,
		e.cd_grupo_material,
		e.cd_subgrupo_material,
		e.cd_classe_material,
		b.cd_convenio_parametro,
		b.cd_estabelecimento
	into	cd_material_w,
		nr_interno_conta_w,
		nr_doc_convenio_w,
		nr_seq_protocolo_w,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		cd_convenio_w,
		cd_estabelecimento_w
	from	material_atend_paciente a,
		conta_paciente b,
		estrutura_material_v e
	where	a.nr_sequencia = nr_seq_matpaci_p
	and	a.nr_interno_conta = b.nr_interno_conta
	and	a.cd_material = e.cd_material;
	exception
	when others then
		cd_convenio_w := null;
	end;
end if;



if	(regra_prev_w.count > 0) and (cd_convenio_w > 0) then
	
	FOR i IN regra_prev_w.first .. regra_prev_w.last LOOP
		
		if	(nr_seq_regra_w is null) and (regra_prev_w(i).cd_convenio = cd_convenio_w) and (regra_prev_w(i).cd_estabelecimento = cd_estabelecimento_w) then
			nr_seq_regra_w := regra_prev_w(i).nr_sequencia;
			ie_tipo_regra_w := regra_prev_w(i).ie_tipo_regra;
		elsif	(nr_seq_regra_w is null)and (regra_prev_w(i).cd_convenio = cd_convenio_w) and (regra_prev_w(i).cd_estabelecimento is null) then
			nr_seq_regra_w := regra_prev_w(i).nr_sequencia;
			ie_tipo_regra_w := regra_prev_w(i).ie_tipo_regra;
		elsif	(nr_seq_regra_w is null) and (regra_prev_w(i).cd_estabelecimento = cd_estabelecimento_w) and (regra_prev_w(i).cd_convenio is null)then
			nr_seq_regra_w := regra_prev_w(i).nr_sequencia;
			ie_tipo_regra_w := regra_prev_w(i).ie_tipo_regra;
		elsif	(nr_seq_regra_w is null) and (regra_prev_w(i).cd_estabelecimento is null) and (regra_prev_w(i).cd_convenio is null)then
			nr_seq_regra_w := regra_prev_w(i).nr_sequencia;
			ie_tipo_regra_w := regra_prev_w(i).ie_tipo_regra;
		end if;
	end loop;
	
end if;

if	(nr_seq_regra_w > 0) then
	
		
	if	(regra_prev_usu_w.count > 0) then
	
	
		regra_prev_usu_item_w.delete;
		
		
		FOR i IN regra_prev_usu_w.first .. regra_prev_usu_w.last LOOP
			
			if	(regra_prev_usu_w(i).nr_seq_prev_analise = nr_seq_regra_w) then
				
				if (regra_prev_item_w.count > 0) then				
					
					for j in regra_prev_item_w.first ..regra_prev_item_w.last loop
						if (regra_prev_item_w(j).nr_seq_prev_analise_usu = regra_prev_usu_w(i).nr_sequencia) then
							
							if	(nr_seq_propaci_p is not null) then
							
								if	(cd_procedimento_w = regra_prev_item_w(j).cd_procedimento) and (ie_origem_proced_w = regra_prev_item_w(j).ie_origem_proced) then
									regra_prev_usu_w(i).qt_pontos_itens :=  10;
								elsif	(cd_grupo_proc_w = regra_prev_item_w(j).cd_grupo_proc) then
									regra_prev_usu_w(i).qt_pontos_itens :=  7;
								elsif	(cd_espealidade_w = regra_prev_item_w(j).cd_especialidade) then
									regra_prev_usu_w(i).qt_pontos_itens :=  5;
								elsif	(cd_area_procedimento_w = regra_prev_item_w(j).cd_area_procedimento) then
									regra_prev_usu_w(i).qt_pontos_itens :=  3;
								else
									regra_prev_usu_w(i).qt_pontos_itens :=  0;
								end if;
								
							elsif	(nr_seq_matpaci_p is not null) then
							
								if	(cd_material_w = regra_prev_item_w(j).cd_material) then
									regra_prev_usu_w(i).qt_pontos_itens :=  10;
								elsif	(cd_classe_material_w = regra_prev_item_w(j).cd_classe_material) then
									regra_prev_usu_w(i).qt_pontos_itens :=  7;
								elsif	(cd_subgrupo_material_w = regra_prev_item_w(j).cd_subgrupo_material) then
									regra_prev_usu_w(i).qt_pontos_itens :=  5;
								elsif	(cd_grupo_material_w = regra_prev_item_w(j).cd_grupo_material) then
									regra_prev_usu_w(i).qt_pontos_itens :=  3;
								else	
									regra_prev_usu_w(i).qt_pontos_itens :=  0;
								end if;
								
							end if;
							
							if	(regra_prev_usu_w(i).qt_pontos_itens > 0) then
								
								if	(regra_prev_usu_item_w.count = 0) then
									regra_prev_usu_item_w(1) := regra_prev_usu_w(i);
								elsif	(regra_prev_usu_item_w(1).qt_pontos_itens < regra_prev_usu_w(i).qt_pontos_itens) then
									regra_prev_usu_item_w(1) := regra_prev_usu_w(i);
								end if;
							end if;
							
						end if;
					end loop;
					
					
					
					
					
					
				end if;
			end if;
				
		end loop;
		nm_usuario_retorno_w := null;
		nm_usuario_item_w    := null;
		if	regra_prev_usu_item_w.count > 0 then
		
			FOR i IN regra_prev_usu_w.first .. regra_prev_usu_w.last LOOP
				
				if	(regra_prev_usu_w(i).nr_seq_prev_analise = nr_seq_regra_w) and
					(regra_prev_usu_w(i).nr_sequencia = regra_prev_usu_item_w(1).nr_sequencia) and
					(regra_prev_usu_w(i).qt_itens_pendentes = regra_prev_usu_item_w(1).qt_itens_pendentes) and
					(regra_prev_usu_w(i).nm_usuario_resp = regra_prev_usu_item_w(1).nm_usuario_resp) then
					
					
					regra_prev_usu_w(i).qt_itens_pendentes := regra_prev_usu_w(i).qt_itens_pendentes + 1;
					
					nm_usuario_item_w := regra_prev_usu_item_w(1).nm_usuario_resp;
					nm_usuario_retorno_w := nm_usuario_item_w;
					
				end if;
				
			end loop;
		
		end if;
		
		
		
		qt_itens_pendentes_w := 0;
		nm_usuario_zero_w     := null;
		regra_prev_usu_ww.delete;
			
		if	(nm_usuario_item_w is null) then
		
		
			FOR i IN regra_prev_usu_w.first .. regra_prev_usu_w.last LOOP
	
				if	(regra_prev_usu_w(i).nr_seq_prev_analise = nr_seq_regra_w) then
				
					if	(ie_tipo_regra_w = 'P') then
						begin
						select	NR_SEQUENCIA,
							NM_USUARIO_RESPONSAVEL
						into	nr_seq_guia_w,
							nm_usuario_gravado_w
						from	USUARIO_RESP_GUIA_GRG
						where   nr_seq_protocolo = nr_seq_protocolo_w;
						exception
						when others then
							nm_usuario_gravado_w := null;
							nr_seq_guia_w := null;
						end;
					elsif	(ie_tipo_regra_w = 'C') then
						begin
						select	NR_SEQUENCIA,
							NM_USUARIO_RESPONSAVEL
						into	nr_seq_guia_w,
							nm_usuario_gravado_w
						from	USUARIO_RESP_GUIA_GRG
						where	nr_interno_conta = nr_interno_conta_w
						and	cd_autorizacao   = nr_doc_convenio_w;
						exception
						when others then
							nm_usuario_gravado_w := null;
							nr_seq_guia_w := null;
						end;
						
					end if;
					

			
					if	(nm_usuario_gravado_w is null) then 
						
						if	(nm_usuario_zero_w is  null) then
							
							if	(regra_prev_usu_w(i).qt_itens_pendentes = 0) then
								nm_usuario_zero_w 	:= regra_prev_usu_w(i).nm_usuario_resp;
								nr_seq_zero_w	     	:= regra_prev_usu_w(i).nr_sequencia; 
							end if;
							
						end if;
							
						if	(nm_usuario_zero_w is null) and	
							(regra_prev_usu_w(i).qt_itens_pendentes > 0) then
						
							if	(regra_prev_usu_ww.count = 0) then
								
								regra_prev_usu_ww(1) := regra_prev_usu_w(i);
								qt_vetor_aux_w := 1;
								
							elsif	(regra_prev_usu_ww(1).qt_itens_pendentes >  regra_prev_usu_w(i).qt_itens_pendentes) then
									regra_prev_usu_ww(1) := regra_prev_usu_w(i);
									qt_vetor_aux_w := qt_vetor_aux_w + 1;
								
							end if;
							
						end if;
					else	
						nm_usuario_retorno_w := nm_usuario_gravado_w;
						
						FOR i IN regra_prev_usu_w.first .. regra_prev_usu_w.last LOOP
							
							if	(regra_prev_usu_w(i).nr_seq_prev_analise = nr_seq_regra_w) and
								(regra_prev_usu_w(i).nm_usuario_resp = nm_usuario_retorno_w) then
								regra_prev_usu_w(i).qt_itens_pendentes := regra_prev_usu_w(i).qt_itens_pendentes +  1;
							end if;
						end loop;
					end if;
			
				end if;
			end loop;
		end if;
	
		
		
		if	(nm_usuario_item_w is null) and
			(nm_usuario_gravado_w is null) then
			
			
			if	(nm_usuario_zero_w is not null) then
				nm_usuario_retorno_w := nm_usuario_zero_w;
																			
				FOR i IN regra_prev_usu_w.first .. regra_prev_usu_w.last LOOP
				
					if	(regra_prev_usu_w(i).nr_seq_prev_analise = nr_seq_regra_w) and
						(regra_prev_usu_w(i).nr_sequencia = nr_seq_zero_w) and
						(regra_prev_usu_w(i).nm_usuario_resp = nm_usuario_zero_w) then
						
						regra_prev_usu_w(i).qt_itens_pendentes := regra_prev_usu_w(i).qt_itens_pendentes + 1;
						inserir_usuario_resp_guia(nm_usuario_p,nr_seq_protocolo_w,nm_usuario_retorno_w,NR_INTERNO_CONTA_w,nr_seq_regra_w,nr_doc_convenio_w);
					end if;
				end loop;
			elsif	(qt_vetor_aux_w > 0) then
			
				FOR i IN regra_prev_usu_w.first .. regra_prev_usu_w.last LOOP
			
					if	(regra_prev_usu_w(i).nr_seq_prev_analise = nr_seq_regra_w) and
						(regra_prev_usu_w(i).nr_sequencia = regra_prev_usu_ww(1).nr_sequencia) and
						(regra_prev_usu_w(i).qt_itens_pendentes = regra_prev_usu_ww(1).qt_itens_pendentes) and
						(regra_prev_usu_w(i).nm_usuario_resp = regra_prev_usu_ww(1).nm_usuario_resp) then
					
						regra_prev_usu_w(i).qt_itens_pendentes := regra_prev_usu_w(i).qt_itens_pendentes + 1;
						nm_usuario_retorno_w := regra_prev_usu_ww(1).nm_usuario_resp;
						inserir_usuario_resp_guia(nm_usuario_p,nr_seq_protocolo_w,nm_usuario_retorno_w,NR_INTERNO_CONTA_w,nr_seq_regra_w,nr_doc_convenio_w);
					end if;
				end loop;
			end if;	
			
		end if;
	end if;
end if;

nm_usuario_resp := nm_usuario_retorno_w;
end;

procedure inserir_usuario_resp_guia(nm_usuario_p		varchar2,
				    nr_seq_protocolo_p		number,
				    nm_usuario_responsavel_p	varchar2,
				    nr_interno_conta_p		number,
				    nr_seq_regra_p		number,
				    nr_doc_convenio_p		varchar2) as	

begin
insert into USUARIO_RESP_GUIA_GRG (	nr_sequencia,  
					dt_atualizacao_nrec,        
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					NR_SEQ_PROTOCOLO,   
					NM_USUARIO_RESPONSAVEL,
					NR_INTERNO_CONTA,
					NR_SEQ_REGRA_PREVISAO,
					CD_AUTORIZACAO)
values				(	USUARIO_RESP_GUIA_GRG_seq.nextval,  
					sysdate,        
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_protocolo_p,   
					nm_usuario_responsavel_p,
					nr_interno_conta_p,
					nr_seq_regra_p,
					nr_doc_convenio_p);
					
commit;	
end;

procedure limpa_listas as

begin
regra_prev_w.delete;
regra_prev_usu_w.delete;
regra_prev_item_w.delete;
regra_prev_usu_ww.delete;
end;

procedure printlog(ds_log_p in varchar2) as

begin
	dbms_output.put_line(ds_log_p);
end;

end usuario_resp_pck;
/
