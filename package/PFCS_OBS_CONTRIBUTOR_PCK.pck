create or replace package pfcs_obs_contributor_pck as

	function get_observation_seq(nr_seq_encounter_p	number, cd_observation_type_p varchar) return number;
	function get_laceplus(nr_seq_encounter_p number) return varchar;
	function get_freqflyer_visits(nr_seq_encounter_p number) return varchar;
	function get_freqflyer_comorbidities(nr_seq_encounter_p	number) return varchar;
	function get_freqflyer_visit_reason(nr_seq_encounter_p number) return varchar;
	function get_edi_vital_warnings(nr_seq_encounter_p number) return varchar;
	function get_edi_contributors(nr_seq_encounter_p number) return varchar;
	function get_contributor_unit(ds_contributor_p varchar) return varchar;

	procedure obs_extension_single_data(nr_seq_observation_p number, cd_identifier_p varchar, ds_value_p out nocopy varchar, ds_title_p out nocopy varchar);

end pfcs_obs_contributor_pck;
/

create or replace PACKAGE BODY pfcs_obs_contributor_pck AS
	/*=========================================== Variables ===========================================*/
	ds_value_w						pfcs_observ_extension.ds_value%type;
	ds_title_w						pfcs_observ_extension.ds_title%type;
	nr_rank_w						pfcs_observ_extension.nr_rank%type;
	nr_seq_observation_w			pfcs_observ_extension.nr_seq_observation%type;
	ds_unit_w						pfcs_contributor_metadata.ds_unit_disp%type;

	cd_laceplus_type_w				varchar2(100) := 'PFCS#LacePlus';
	cd_freqflyer_type_w				varchar2(100) := 'PFCS#FreqFlyer';
	cd_edi_type_w					varchar2(100) := 'PFCS#edi-v2';

	/*=========================================== Cursors ===========================================*/
	/*** Cursor to get a amount of rows for a particular identifier ***/
	cursor cursor_rank(nr_seq_observation_p number, cd_identifier_p varchar, rows_to_return_p number) is
	select	ds_title,
			ds_value,
			cd_trend
	  from	(select	nvl(ds_title,'') ds_title, 
					nvl(ds_value,'') ds_value,
					nvl(cd_trend,'') cd_trend
		  from	pfcs_observ_extension
		 where	nr_seq_observation = nr_seq_observation_p
		   and	upper(cd_identifier) = upper(cd_identifier_p)
		   and  ds_value is not null
		order by trunc(dt_atualizacao) desc, nr_rank asc)
	where rownum <= rows_to_return_p;

	/*=========================================== Procedures ===========================================*/
	procedure obs_extension_single_data(	nr_seq_observation_p	number,
											cd_identifier_p			varchar,
											ds_value_p			out	nocopy varchar,
											ds_title_p			out	nocopy varchar) is
	begin
		select	nvl(max(ds_value),''),
				nvl(max(ds_title),'')
		  into	ds_value_p,
				ds_title_p
		  from	pfcs_observ_extension
		 where	nr_seq_observation = nr_seq_observation_p
		   and	upper(cd_identifier) = upper(cd_identifier_p);
	end;

	/*=========================================== Functions ===========================================*/
	function get_observation_seq(	nr_seq_encounter_p	number,
									cd_observation_type_p varchar) return number as
		nr_sequencia_w		pfcs_observation.nr_sequencia%type;
	begin
		if (cd_observation_type_p = cd_freqflyer_type_w) then
			select	nvl(max(nr_sequencia),'')
			  into	nr_sequencia_w
			  from	(select	nr_sequencia
					   from	pfcs_observation obs
					  where	nr_seq_encounter =  nr_seq_encounter_p
						and	obs.nr_freq_flyer = 1
						and	cd_observation_type = cd_observation_type_p
					order by obs.dt_atualizacao desc)
			where rownum = 1;
		else
			select	nvl(max(nr_sequencia),'')
			  into	nr_sequencia_w
			  from	(select	nr_sequencia
					   from	pfcs_observation obs
					  where	nr_seq_encounter =  nr_seq_encounter_p
						and	cd_observation_type = cd_observation_type_p
					order by obs.dt_atualizacao desc)
			where rownum = 1;
		end if;

		return nr_sequencia_w;

	end get_observation_seq;

	function get_contributor_unit(ds_contributor_p varchar) return varchar as
		ds_unit_disp_w 		pfcs_contributor_metadata.ds_unit_disp%type;
	begin
		select nvl(max(ds_unit_disp),'')
		into ds_unit_disp_w
		from pfcs_contributor_metadata
		where upper(ds_contributor) = upper(ds_contributor_p);
		return ds_unit_disp_w;

	end get_contributor_unit;

	function get_laceplus(nr_seq_encounter_p	number) return varchar as
		ds_return_w		varchar2(255);
	begin
		ds_return_w := '';
		nr_seq_observation_w := get_observation_seq(nr_seq_encounter_p, cd_laceplus_type_w);

		for c01_w in cursor_rank(nr_seq_observation_w, '#LACEPLUS_CONTRIBUTOR', 3) loop
       if (upper(c01_w.ds_title) != 'NA' and upper(c01_w.ds_title) != 'NAN') then
			if (ds_return_w is null) then
				ds_return_w := c01_w.ds_title;
			else
				ds_return_w := ds_return_w || '|' || c01_w.ds_title;
			end if;
            end if;
		end loop;

		return ds_return_w;

	end get_laceplus;

	function get_freqflyer_visits(nr_seq_encounter_p	number) return varchar as
		ds_return_w		varchar2(255);
	begin
		nr_seq_observation_w := get_observation_seq(nr_seq_encounter_p, cd_freqflyer_type_w);

		obs_extension_single_data(nr_seq_observation_w, '#FREQFLYER_ADMISION_30_DAYS', ds_value_w, ds_title_w);
		select 'ADMISION_30_DAYS:' || decode(nvl(ds_value_w,0),0,'N','','N','0','N','Y') || '|'
        into ds_return_w
        from dual;

		obs_extension_single_data(nr_seq_observation_w, '#FREQFLYER_ADMISION_PAST_YEAR', ds_value_w, ds_title_w);
		ds_return_w :=  ds_return_w || 'ADMISION_PAST_YEAR:' || ds_value_w || '|';

		obs_extension_single_data(nr_seq_observation_w, '#FREQFLYER_ED_PAST_YEAR', ds_value_w, ds_title_w);
		ds_return_w := ds_return_w || 'ED_PAST_YEAR:' || ds_value_w;

		return ds_return_w;

	end get_freqflyer_visits;

	function get_freqflyer_comorbidities(nr_seq_encounter_p	number) return varchar as
		ds_return_w		varchar2(255);
	begin
		ds_return_w := '';
		nr_seq_observation_w := get_observation_seq(nr_seq_encounter_p, cd_freqflyer_type_w);

		for c01_w in cursor_rank(nr_seq_observation_w, '#FREQFLYER_COMORBIDITIES', 3) loop
        if (upper(c01_w.ds_value) != 'NONE' and upper(c01_w.ds_value) != 'NA' and upper(c01_w.ds_value) != 'NAN') then
			if (ds_return_w is null) then
				ds_return_w :=  c01_w.ds_value;
			else
				ds_return_w := ds_return_w || '|' || c01_w.ds_value;
			end if;
            end if;
		end loop;

		return ds_return_w;

	end get_freqflyer_comorbidities;

	function get_freqflyer_visit_reason(nr_seq_encounter_p	number) return varchar as
		ds_return_w		varchar2(255);
	begin
		ds_return_w := '';
		nr_seq_observation_w := get_observation_seq(nr_seq_encounter_p, cd_freqflyer_type_w);

		for c01_w in cursor_rank(nr_seq_observation_w, '#FREQFLYER_EDVISIT_REASON', 3) loop
        if (upper(c01_w.ds_value) != 'NA' and upper(c01_w.ds_value) != 'NAN') then
			if (ds_return_w is null) then
				ds_return_w := c01_w.ds_value;
			else
				ds_return_w := ds_return_w || '|' || c01_w.ds_value;
			end if;
            end if;
		end loop;

		return ds_return_w;

	end get_freqflyer_visit_reason;

	function get_edi_vital_warnings(nr_seq_encounter_p	number) return varchar as
		ds_title_w		pfcs_observ_extension.ds_title%type;
	begin
		nr_seq_observation_w := get_observation_seq(nr_seq_encounter_p, cd_edi_type_w);

		obs_extension_single_data(nr_seq_observation_w, '#EDI_VITALS_WARNING', ds_value_w, ds_title_w);

		if (upper(ds_title_w) = 'NAN' or upper(ds_title_w) = 'NA') then
			ds_title_w := null;
		end if;

		return ds_title_w;

	end get_edi_vital_warnings;

	function get_edi_contributors(nr_seq_encounter_p	number) return varchar as
		ds_return_w		varchar2(255);
	begin
		ds_return_w := '';
		nr_seq_observation_w := get_observation_seq(nr_seq_encounter_p, cd_edi_type_w);

		for c01_w in cursor_rank(nr_seq_observation_w, '#EDI_CONTRIBUTOR', 3) loop
        if (upper(c01_w.ds_title) != 'NA' and upper(c01_w.ds_title) != 'NAN') then
		ds_unit_w := get_contributor_unit(c01_w.ds_title);
			if (ds_return_w is null) then
				ds_return_w := 'Title:' || c01_w.ds_title || ';Value:' || c01_w.ds_value || ' ' || ds_unit_w || ';Trend:' || c01_w.cd_trend;
			else
				ds_return_w := ds_return_w || '|' || 'Title:' || c01_w.ds_title || ';Value:' || c01_w.ds_value || ' ' || ds_unit_w || ';Trend:' || c01_w.cd_trend;
			end if;
            end if;
		end loop;

		return replace(ds_return_w,'slope:');

	end get_edi_contributors;

end pfcs_obs_contributor_pck;
/
