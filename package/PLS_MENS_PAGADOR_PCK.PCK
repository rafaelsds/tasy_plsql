create or replace 
package pls_mens_pagador_pck as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Inserir os pagadores no lote de mensalidade
	Esta package foi desenvolvida para inserir todos os pagadores da tabela PLS_MENS_BENEF_PAG_TMP na PLS_MENSALIDADE
------------------------------------------------------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

procedure inserir_mensalidade_pagador(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type);

end pls_mens_pagador_pck;
/

create or replace
package body pls_mens_pagador_pck as

pls_lote_mensalidade_w		pls_lote_mensalidade%rowtype;

function obter_serie_mensalidade (	ie_contrato_p			varchar2,
					ie_tipo_contratacao_p		pls_plano.ie_tipo_contratacao%type,
					ie_preco_p			pls_plano.ie_preco%type,
					ie_situacao_trabalhista_p	pls_contrato_pagador.ie_situacao_trabalhista%type,
					nr_seq_classif_itens_p		pls_classif_itens_pagador.nr_sequencia%type)
				return pls_mensalidade.nr_serie_mensalidade%type is
nr_serie_mensalidade_w	pls_mensalidade.nr_serie_mensalidade%type;

Cursor C01 is
	select	a.nr_serie_nf
	from	pls_regra_serie_nf		b,
		pls_regra_serie_nf_compl	a
	where	a.nr_seq_regra		= b.nr_sequencia
	and	b.ie_situacao		= 'A'
	and	((a.ie_tipo_contrato	= ie_contrato_p) or (a.ie_tipo_contrato is null))
	and	((a.ie_tipo_contratacao	= ie_tipo_contratacao_p) or (a.ie_tipo_contratacao is null))
	and	((a.ie_preco		= ie_preco_p) or (a.ie_preco is null))
	and	((a.ie_situacao_trabalhista = ie_situacao_trabalhista_p) or (a.ie_situacao_trabalhista is null))
	and	(((a.ie_primeira_mensalidade = 'S') and (pls_lote_mensalidade_w.ie_primeira_mensalidade = 'S')) or (nvl(a.ie_primeira_mensalidade,'N') = 'N'))
	and	((a.nr_seq_classif_itens = nr_seq_classif_itens_p) or (a.nr_seq_classif_itens is null))
	order by nvl(a.ie_primeira_mensalidade,'N'),
		nvl(a.nr_seq_classif_itens,-1),
		nvl(a.ie_situacao_trabalhista,' '),
		nvl(a.ie_preco,'-1'),
		nvl(a.ie_tipo_contratacao,' '),
		nvl(a.ie_tipo_contrato,' ');

begin

for r_c01_w in C01 loop
	nr_serie_mensalidade_w	:= r_c01_w.nr_serie_nf;
end loop;

return nr_serie_mensalidade_w;

end obter_serie_mensalidade;

function obter_dt_vencimento_mens (	nr_seq_pagador_p			pls_contrato_pagador.nr_sequencia%type,
					dt_dia_vencimento_p			pls_contrato_pagador_fin.dt_dia_vencimento%type,
					ie_mes_vencimento_p			pls_contrato_pagador_fin.ie_mes_vencimento%type,
					dt_referencia_p				date,
					dt_primeira_mensalidade_p		pls_contrato_pagador.dt_primeira_mensalidade%type,
					ie_primeira_mens_gerada_p		pls_contrato_pagador.ie_primeira_mensalidade_gerada%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					qt_meses_vencimento_p			pls_contrato_pagador_fin.qt_meses_vencimento%type)
				return date is

dt_vencimento_w		date;
qt_meses_vencimento_w	pls_contrato_pagador_fin.qt_meses_vencimento%type;

begin

if	(dt_dia_vencimento_p = 0) then
	dt_vencimento_w	:= sysdate;
else
	qt_meses_vencimento_w	:= abs(nvl(qt_meses_vencimento_p,1));
	dt_vencimento_w		:= dt_referencia_p + (dt_dia_vencimento_p - 1);
	if	(ie_mes_vencimento_p = 'P') then
		dt_vencimento_w := add_months(dt_vencimento_w,qt_meses_vencimento_w);
	elsif	(ie_mes_vencimento_p = 'N') then
		dt_vencimento_w := add_months(dt_vencimento_w,qt_meses_vencimento_w  * -1);
	else	--	se o mes de vencimento for diferente do mes da geracao da mensalidade, busca o ultimo dia do mes
		if	(dt_referencia_p <> trunc(dt_vencimento_w,'month')) then
			dt_vencimento_w	:= last_day(dt_referencia_p);
		end if;
	end if;
end if;

-- Se o pagador nao possui outras mensalidades geradas, entao a data de vencimento e a data da primeira mensalidade do cadastro no pagador
if	(ie_primeira_mens_gerada_p = 'N') then
	dt_vencimento_w := nvl(dt_primeira_mensalidade_p,dt_vencimento_w);
	
	-- Caso a data de vencimento esteja vencida da primeira mensalidade, incrementa os dias das regras com a data atual
	if	(trunc(dt_vencimento_w,'dd') <= trunc(sysdate,'dd')) and
		(pls_mensalidade_util_pck.get_qt_dias_incrementados_venc is not null) then
		dt_vencimento_w	:= sysdate + pls_mensalidade_util_pck.get_qt_dias_incrementados_venc;
	end if;
end if;

return dt_vencimento_w;

end obter_dt_vencimento_mens;

function obter_geracao_nota_titulo(	ie_geracao_nota_titulo_pag_p	pls_contrato_pagador_fin.ie_geracao_nota_titulo%type,
					cd_pf_pagador_p			pls_contrato_pagador.cd_pessoa_fisica%type)
					return varchar2 is

ie_nota_titulo_w		pls_mensalidade.ie_nota_titulo%type;

begin
if	(ie_geracao_nota_titulo_pag_p is not null) then
	ie_nota_titulo_w	:= ie_geracao_nota_titulo_pag_p;
else
	if	(cd_pf_pagador_p is not null) then
		ie_nota_titulo_w	:= pls_mensalidade_util_pck.get_ie_geracao_nota_titulo_pf;
	else
		ie_nota_titulo_w	:= pls_mensalidade_util_pck.get_ie_geracao_nota_titulo_pj;
	end if;
end if;

return ie_nota_titulo_w;

end obter_geracao_nota_titulo;

function obter_tipo_formacao_preco(	ie_preco_p	pls_plano.ie_preco%type)
					return varchar2 is
ie_tipo_formacao_preco_w	varchar2(1);
begin
if	(ie_preco_p in (1,4)) then
	ie_tipo_formacao_preco_w	:= 'R';
elsif	(ie_preco_p in (2,3)) then
	ie_tipo_formacao_preco_w	:= 'P';
end if;

return ie_tipo_formacao_preco_w;

end obter_tipo_formacao_preco;

function obter_se_proporcional(		ie_calc_primeira_mens_p 	pls_contrato_pagador.ie_calc_primeira_mens%type)
				return varchar2 is
ie_proporcional_w	varchar2(1);
begin
if	(ie_calc_primeira_mens_p = 'P') then
	ie_proporcional_w	:= 'S';
else
	ie_proporcional_w	:= 'N';
end if;

return ie_proporcional_w;

end obter_se_proporcional;

procedure consiste_suspensao_mensalidade(	nr_seq_contrato_p		in	pls_contrato.nr_sequencia%type,
						nr_seq_pagador_p		in	pls_contrato_pagador.nr_sequencia%type,
						dt_referencia_p			in	pls_lote_mensalidade.dt_mesano_referencia%type,
						ie_tipo_pagador_p		in	pls_contrato_susp_mens.ie_tipo_pagador%type,
						nr_seq_motivo_susp_p		out	pls_motivo_susp_cobr_mens.nr_sequencia%type,
						ie_mensalidade_suspensa_p	out	varchar2) is

nr_seq_motivo_susp_w		pls_motivo_susp_cobr_mens.nr_sequencia%type;
qt_item_excecao_w		pls_integer;
ie_mensalidade_suspensa_w	varchar2(1);
nr_seq_grupo_contrato_w		pls_contrato_grupo.nr_seq_grupo%type;
begin

ie_mensalidade_suspensa_w	:= 'N';
nr_seq_motivo_susp_w		:= null;

if	(nr_seq_pagador_p is not null) then
	select	max(nr_seq_motivo_suspensao)
	into	nr_seq_motivo_susp_w
	from	pls_contrato_susp_mens
	where	nr_seq_contrato	is null
	and	nr_seq_pagador	= nr_seq_pagador_p
	and	dt_liberacao is not null
	and	(((dt_fim_suspensao is null) 
	and	 (dt_referencia_p >= dt_inicio_suspensao))
	or	((dt_fim_suspensao is not null) 
	and	 (dt_referencia_p between dt_inicio_suspensao and dt_fim_suspensao) 
	and	 (dt_inicio_suspensao <> dt_fim_suspensao)))
	and	((ie_cobrar_periodo_susp = 'S' and dt_fim_suspensao is null) or (ie_cobrar_periodo_susp <> 'S'))
	and	((ie_tipo_pagador = ie_tipo_pagador_p) or (nvl(ie_tipo_pagador, 'A') = 'A'));
end if;

if	((nr_seq_contrato_p is not null) and (nr_seq_motivo_susp_w is null)) then
	select	max(nr_seq_motivo_suspensao)
	into	nr_seq_motivo_susp_w
	from	pls_contrato_susp_mens
	where	nr_seq_contrato		= nr_seq_contrato_p
	and	((nr_seq_pagador	= nr_seq_pagador_p) or (nr_seq_pagador is null))
	and	dt_liberacao is not null
	and	(((dt_fim_suspensao is null) 
	and	 (dt_referencia_p >= dt_inicio_suspensao))
	or	((dt_fim_suspensao is not null) 
	and	 (dt_referencia_p between dt_inicio_suspensao and dt_fim_suspensao) 
	and	 (dt_inicio_suspensao <> dt_fim_suspensao)))
	and	((ie_cobrar_periodo_susp = 'S' and dt_fim_suspensao is null) or (ie_cobrar_periodo_susp <> 'S'))
	and	((ie_tipo_pagador = ie_tipo_pagador_p) or (nvl(ie_tipo_pagador, 'A') = 'A'));
	
	if	(nr_seq_motivo_susp_w is null) then
		select	max(a.nr_seq_motivo_suspensao)
		into	nr_seq_motivo_susp_w
		from	pls_contrato_susp_mens  a,
			pls_contrato_grupo	b
		where	a.nr_seq_grupo_contrato	= b.nr_seq_grupo
		and	b.nr_seq_contrato 	= nr_seq_contrato_p
		and	a.dt_liberacao is not null
		and	(((a.dt_fim_suspensao is null) 
		and	 (dt_referencia_p >= a.dt_inicio_suspensao))
		or	((a.dt_fim_suspensao is not null) 
		and	 (dt_referencia_p between a.dt_inicio_suspensao and a.dt_fim_suspensao) 
		and	 (a.dt_inicio_suspensao <> a.dt_fim_suspensao)))
		and	((a.ie_cobrar_periodo_susp = 'S' and a.dt_fim_suspensao is null) or (a.ie_cobrar_periodo_susp <> 'S'))
		and	((a.ie_tipo_pagador = ie_tipo_pagador_p) or (nvl(ie_tipo_pagador, 'A') = 'A'));
	end if;
	
	if	(nr_seq_motivo_susp_w is not null) then
		select	count(1)
		into	qt_item_excecao_w
		from	pls_susp_cobr_mens_excecao a,
			pls_motivo_susp_cobr_mens b
		where	a.nr_seq_motivo	= b.nr_sequencia
		and	a.nr_seq_motivo	= nr_seq_motivo_susp_w
		and	b.ie_situacao	= 'A';
		
		if	(qt_item_excecao_w = 0) then
			ie_mensalidade_suspensa_w := 'S';
		end if;
	end if;
end if;

ie_mensalidade_suspensa_p	:= ie_mensalidade_suspensa_w;
nr_seq_motivo_susp_p		:= nr_seq_motivo_susp_w;

end consiste_suspensao_mensalidade;

function obter_se_restricao_uma_mens (	nr_seq_pagador_p	pls_contrato_pagador.nr_sequencia%type,
					nr_seq_contrato_p	pls_contrato.nr_sequencia%type,
					nr_seq_intercambio_p	pls_intercambio.nr_sequencia%type,
					ie_tipo_estipulante_p	varchar2,
					dt_referencia_p		pls_lote_mensalidade.dt_mesano_referencia%type) return varchar2 is
qt_registro_w			pls_integer;
qt_regra_w			pls_integer;
ie_restringe_uma_mens_w		pls_regra_mens_contrato.ie_restringe_uma_mens_pag_mes%type;
begin

select	count(1)
into	qt_regra_w
from	pls_regra_mens_contrato
where	ie_tipo_regra	= 'G'
and	ie_restringe_uma_mens_pag_mes = 'S'
and	dt_inicio_vigencia <= dt_referencia_p
and	(dt_fim_vigencia >= dt_referencia_p or dt_fim_vigencia is null);

if	(qt_regra_w > 0) then
	
	select	nvl(max(ie_restringe_uma_mens_pag_mes),'N')
	into	ie_restringe_uma_mens_w
	from	pls_regra_mens_contrato
	where	ie_tipo_regra	= 'G'
	and	(ie_pessoa_contrato = ie_tipo_estipulante_p or ie_pessoa_contrato = 'A')
	and	dt_inicio_vigencia <= dt_referencia_p
	and	(dt_fim_vigencia >= dt_referencia_p or dt_fim_vigencia is null);
	
	if	(ie_restringe_uma_mens_w = 'S') then
		select	count(1)
		into	qt_registro_w
		from	pls_mensalidade
		where	nr_seq_pagador	= nr_seq_pagador_p
		and	dt_referencia	= dt_referencia_p
		and	ie_cancelamento is null;
		
		if	(qt_registro_w > 0) then
			return 'S';
		else
			return 'N';
		end if;
	else
		return 'N';
	end if;
else
	return 'N';
end if;

end obter_se_restricao_uma_mens;

function obter_se_restringe_pagador (	pls_contrato_pagador_fin_p	pls_contrato_pagador_fin%rowtype) return varchar2 is
ie_restringe_w	varchar2(1);
begin
ie_restringe_w	:= 'N';

if	(pls_lote_mensalidade_w.nr_seq_forma_cobranca is not null) and
	(nvl(pls_lote_mensalidade_w.nr_seq_forma_cobranca, 'X') <> nvl(pls_contrato_pagador_fin_p.nr_seq_forma_cobranca, 'X')) then
	ie_restringe_w	:= 'S';
end if;

if	(pls_lote_mensalidade_w.cd_tipo_portador is not null) and
	(nvl(pls_lote_mensalidade_w.cd_tipo_portador, -1) <> nvl(pls_contrato_pagador_fin_p.cd_tipo_portador, -1)) then
	ie_restringe_w	:= 'S';
end if;

if	(pls_lote_mensalidade_w.cd_portador is not null) and
	(nvl(pls_lote_mensalidade_w.cd_portador, 0) <> nvl(pls_contrato_pagador_fin_p.cd_portador, 0)) then
	ie_restringe_w	:= 'S';
end if;

if	(pls_lote_mensalidade_w.cd_banco is not null) and
	(nvl(pls_lote_mensalidade_w.cd_banco, 0) <> nvl(pls_contrato_pagador_fin_p.cd_banco, 0)) then
	ie_restringe_w	:= 'S';
end if;

if	(pls_lote_mensalidade_w.nr_seq_empresa is not null) and
	(nvl(pls_lote_mensalidade_w.nr_seq_empresa, 0) <> nvl(pls_contrato_pagador_fin_p.nr_seq_empresa, 0)) then
	ie_restringe_w	:= 'S';
end if;

if	(pls_lote_mensalidade_w.ie_geracao_nota_titulo <> 'A') and
	(nvl(pls_lote_mensalidade_w.ie_geracao_nota_titulo, 'X') <> nvl(pls_contrato_pagador_fin_p.ie_geracao_nota_titulo, 'X')) then
	ie_restringe_w	:= 'S';
end if;

if	(pls_lote_mensalidade_w.nr_dia_inicial_venc is not null) and
	(nvl(pls_lote_mensalidade_w.nr_dia_inicial_venc, 0) > nvl(pls_contrato_pagador_fin_p.dt_dia_vencimento, 0)) then
	ie_restringe_w	:= 'S';
end if;

if	(pls_lote_mensalidade_w.nr_dia_final_venc is not null) and
	(nvl(pls_lote_mensalidade_w.nr_dia_final_venc, 0) < nvl(pls_contrato_pagador_fin_p.dt_dia_vencimento, 0)) then
	ie_restringe_w	:= 'S';
end if;

return ie_restringe_w;

end obter_se_restringe_pagador;

procedure atualizar_pls_mensalidade (	tb_nr_seq_pagador_p		in out pls_util_cta_pck.t_number_table,
					tb_nr_seq_pagador_fin_p		in out pls_util_cta_pck.t_number_table,
					tb_dt_referencia_p		in out pls_util_cta_pck.t_date_table,
					tb_dt_vencimento_p		in out pls_util_cta_pck.t_date_table,
					tb_nr_seq_forma_cobranca_p	in out pls_util_cta_pck.t_number_table,
					tb_cd_banco_p			in out pls_util_cta_pck.t_number_table,
					tb_cd_agencia_bancaria_p	in out pls_util_cta_pck.t_varchar2_table_10,
					tb_ie_digito_agencia_p		in out pls_util_cta_pck.t_varchar2_table_2,
					tb_cd_conta_p			in out pls_util_cta_pck.t_varchar2_table_20,
					tb_ie_digito_conta_p		in out pls_util_cta_pck.t_varchar2_table_2,
					tb_ie_endereco_boleto_p		in out pls_util_cta_pck.t_varchar2_table_10,
					tb_nr_seq_conta_banco_p		in out pls_util_cta_pck.t_number_table,
					tb_ie_gerar_cobr_escrit_p	in out pls_util_cta_pck.t_varchar2_table_2,
					tb_nr_seq_conta_banco_deb_au_p	in out pls_util_cta_pck.t_number_table,
					tb_ie_proporcional_p		in out pls_util_cta_pck.t_varchar2_table_1,
					tb_nr_seq_compl_pf_tel_adic_p	in out pls_util_cta_pck.t_number_table,
					tb_nr_seq_compl_pj_p		in out pls_util_cta_pck.t_number_table,
					tb_nr_seq_tipo_compl_adic_p	in out pls_util_cta_pck.t_number_table,
					tb_ie_nota_titulo_p		in out pls_util_cta_pck.t_varchar2_table_2,
					tb_ie_tipo_formacao_preco_p	in out pls_util_cta_pck.t_varchar2_table_1,
					tb_nr_serie_mensalidade_p	in out pls_util_cta_pck.t_varchar2_table_50,
					tb_nr_parcela_p			in out pls_util_cta_pck.t_number_table,
					tb_nr_seq_motivo_susp_p		in out pls_util_cta_pck.t_number_table,
					tb_ie_tipo_estipulante_p	in out pls_util_cta_pck.t_varchar2_table_2,
					tb_ie_indice_correcao_p		in out pls_util_cta_pck.t_varchar2_table_2,
					nm_usuario_p			in varchar2
				) is
begin

if	(tb_nr_seq_pagador_p.count > 0) then
	forall i in tb_nr_seq_pagador_p.first..tb_nr_seq_pagador_p.last
		insert	into	pls_mensalidade
			(	nr_sequencia, nr_seq_lote, nr_seq_pagador, nr_seq_pagador_fin,
				dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
				dt_referencia, vl_mensalidade, dt_vencimento, ie_apresentacao,
				nr_seq_forma_cobranca, cd_banco,
				cd_agencia_bancaria, ie_digito_agencia,
				cd_conta, ie_digito_conta,
				ie_endereco_boleto, nr_seq_conta_banco,
				ie_gerar_cobr_escrit, nr_seq_conta_banco_deb_aut,
				ie_proporcional, nr_seq_compl_pf_tel_adic,
				nr_seq_compl_pj, nr_seq_tipo_compl_adic,
				ie_nota_titulo, ie_tipo_formacao_preco,
				nr_serie_mensalidade, nr_parcela, nr_seq_motivo_susp,
				ie_tipo_estipulante, ie_indice_correcao
				)
			values(	pls_mensalidade_seq.nextval, pls_lote_mensalidade_w.nr_sequencia, tb_nr_seq_pagador_p(i), tb_nr_seq_pagador_fin_p(i),
				sysdate, sysdate, nm_usuario_p, nm_usuario_p,
				tb_dt_referencia_p(i), 0, tb_dt_vencimento_p(i), 1,
				tb_nr_seq_forma_cobranca_p(i), tb_cd_banco_p(i),
				tb_cd_agencia_bancaria_p(i), tb_ie_digito_agencia_p(i),
				tb_cd_conta_p(i), tb_ie_digito_conta_p(i),
				tb_ie_endereco_boleto_p(i), tb_nr_seq_conta_banco_p(i),
				tb_ie_gerar_cobr_escrit_p(i), tb_nr_seq_conta_banco_deb_au_p(i),
				tb_ie_proporcional_p(i), tb_nr_seq_compl_pf_tel_adic_p(i),
				tb_nr_seq_compl_pj_p(i), tb_nr_seq_tipo_compl_adic_p(i),
				tb_ie_nota_titulo_p(i), tb_ie_tipo_formacao_preco_p(i),
				tb_nr_serie_mensalidade_p(i), tb_nr_parcela_p(i), tb_nr_seq_motivo_susp_p(i),
				tb_ie_tipo_estipulante_p(i), tb_ie_indice_correcao_p(i)
				);
	commit;
end if;

-- reinicializa as variaveis
tb_nr_seq_pagador_p.delete;
tb_nr_seq_pagador_fin_p.delete;
tb_dt_referencia_p.delete;
tb_dt_vencimento_p.delete;
tb_nr_seq_forma_cobranca_p.delete;
tb_cd_banco_p.delete;
tb_cd_agencia_bancaria_p.delete;
tb_ie_digito_agencia_p.delete;
tb_cd_conta_p.delete;
tb_ie_digito_conta_p.delete;
tb_ie_endereco_boleto_p.delete;
tb_nr_seq_conta_banco_p.delete;
tb_ie_gerar_cobr_escrit_p.delete;
tb_nr_seq_conta_banco_deb_au_p.delete;
tb_ie_proporcional_p.delete;
tb_nr_seq_compl_pf_tel_adic_p.delete;
tb_nr_seq_compl_pj_p.delete;
tb_nr_seq_tipo_compl_adic_p.delete;
tb_ie_nota_titulo_p.delete;
tb_ie_tipo_formacao_preco_p.delete;
tb_nr_serie_mensalidade_p.delete;
tb_nr_parcela_p.delete;
tb_nr_seq_motivo_susp_p.delete;
tb_ie_tipo_estipulante_p.delete;
tb_ie_indice_correcao_p.delete;

end atualizar_pls_mensalidade;

/*---------------------------------------------------------------------PROCEDURE--------------------------------------------------------------------------------------------------------------------------*/
procedure inserir_mensalidade_pagador(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is

pls_contrato_pagador_fin_w	pls_contrato_pagador_fin%rowtype;
dt_referencia_lote_w		date;
dt_referencia_w			date;
dt_referencia_fimdia_w		date;
ie_log_erro_w			varchar2(1);
nr_parcela_w			number(10);
nr_seq_contrato_plano_w		pls_contrato_plano.nr_sequencia%type;
nr_seq_intercambio_plano_w	pls_intercambio_plano.nr_sequencia%type;
ie_preco_w			pls_plano.ie_preco%type;
ie_tipo_contratacao_w		pls_plano.ie_tipo_contratacao%type;
ie_contrato_w			varchar2(1);
dt_contrato_w			date;
ie_tipo_contrato_w		pls_intercambio.ie_tipo_contrato%type;
ie_mensalidade_suspensa_w	varchar2(1);
nr_seq_motivo_susp_w		pls_motivo_susp_cobr_mens.nr_sequencia%type;
ie_tipo_estipulante_w		varchar2(2);
ie_mensalidade_anterior_w	varchar2(1);
qt_inicial_w			number(1);
qt_registro_w			pls_integer;
qt_inf_finc_futura_w		number(10);

qt_interv_mes_mensalidade_w	pls_regra_mens_contrato.qt_interv_mes_mensalidade%type;
ie_agrupar_valor_w		pls_regra_mens_contrato.ie_agrupar_valor%type;

nr_indice_w			pls_integer;
qt_meses_futuros_w		pls_integer;
qt_meses_anterior_w		pls_integer;
qt_meses_mensalidade_w		pls_integer;

nr_seq_regra_limite_w		pls_regra_mens_contrato.nr_sequencia%type;
ie_agrupar_valor_lim_w		pls_regra_mens_contrato.ie_agrupar_valor%type;
qt_dias_vencimento_w		pls_regra_mens_contrato.qt_dias_vencimento%type;
ie_primeira_mensalidade_w	pls_regra_mens_contrato.ie_primeira_mensalidade%type;
ie_data_base_adesao_w		pls_regra_mens_contrato.ie_data_base_adesao%type;

-- Matriz pagador
tb_nr_seq_pagador_w		pls_util_cta_pck.t_number_table;
tb_nr_seq_pagador_fin_w		pls_util_cta_pck.t_number_table;
tb_dt_referencia_w		pls_util_cta_pck.t_date_table;
tb_dt_vencimento_w		pls_util_cta_pck.t_date_table;
tb_nr_seq_forma_cobranca_w	pls_util_cta_pck.t_number_table;
tb_cd_banco_w			pls_util_cta_pck.t_number_table;
tb_cd_agencia_bancaria_w	pls_util_cta_pck.t_varchar2_table_10;
tb_ie_digito_agencia_w		pls_util_cta_pck.t_varchar2_table_2;
tb_cd_conta_w			pls_util_cta_pck.t_varchar2_table_20;
tb_ie_digito_conta_w		pls_util_cta_pck.t_varchar2_table_2;
tb_ie_endereco_boleto_w		pls_util_cta_pck.t_varchar2_table_10;
tb_nr_seq_conta_banco_w		pls_util_cta_pck.t_number_table;
tb_ie_gerar_cobr_escrit_w	pls_util_cta_pck.t_varchar2_table_2;
tb_nr_seq_conta_banco_deb_au_w	pls_util_cta_pck.t_number_table;
tb_ie_proporcional_w		pls_util_cta_pck.t_varchar2_table_1;
tb_nr_seq_compl_pf_tel_adic_w	pls_util_cta_pck.t_number_table;
tb_nr_seq_compl_pj_w		pls_util_cta_pck.t_number_table;
tb_nr_seq_tipo_compl_adic_w	pls_util_cta_pck.t_number_table;
tb_ie_nota_titulo_w		pls_util_cta_pck.t_varchar2_table_2;
tb_ie_tipo_formacao_preco_w	pls_util_cta_pck.t_varchar2_table_1;
tb_nr_serie_mensalidade_w	pls_util_cta_pck.t_varchar2_table_50;
tb_nr_parcela_w			pls_util_cta_pck.t_number_table;
tb_nr_seq_motivo_susp_w		pls_util_cta_pck.t_number_table;
tb_ie_tipo_estipulante_w	pls_util_cta_pck.t_varchar2_table_2;
tb_ie_indice_correcao_w		pls_util_cta_pck.t_varchar2_table_2;
--Fim matriz pagador

Cursor C01 is
	select	a.nr_seq_pagador,
		b.nr_seq_contrato,
		b.nr_seq_pagador_intercambio nr_seq_intercambio,
		b.ie_calc_primeira_mens,
		b.dt_primeira_mensalidade,
		b.ie_situacao_trabalhista,
		b.nr_seq_classif_itens,
		b.cd_pessoa_fisica,
		b.ie_endereco_boleto,
		b.nr_seq_compl_pf_tel_adic,
		b.nr_seq_compl_pj,
		b.nr_seq_tipo_compl_adic,
		b.ie_primeira_mensalidade_gerada,
		b.ie_tipo_pagador
	from	pls_mens_benef_pag_tmp	a,
		pls_contrato_pagador	b
	where	b.nr_sequencia	= a.nr_seq_pagador
	group by a.nr_seq_pagador,
		b.nr_seq_contrato,
		b.nr_seq_pagador_intercambio,
		b.ie_calc_primeira_mens,
		b.dt_primeira_mensalidade,
		b.ie_situacao_trabalhista,
		b.nr_seq_classif_itens,
		b.cd_pessoa_fisica,
		b.ie_endereco_boleto,
		b.nr_seq_compl_pf_tel_adic,
		b.nr_seq_compl_pj,
		b.nr_seq_tipo_compl_adic,
		b.ie_primeira_mensalidade_gerada,
		b.ie_tipo_pagador;

begin
nr_indice_w	:= 0;
tb_nr_seq_pagador_w.delete; --Garantir que o vetor vai estar vazio e so vai limpar os vetores na primeira execucao
atualizar_pls_mensalidade(	tb_nr_seq_pagador_w,
				tb_nr_seq_pagador_fin_w,
				tb_dt_referencia_w,
				tb_dt_vencimento_w,
				tb_nr_seq_forma_cobranca_w,
				tb_cd_banco_w,
				tb_cd_agencia_bancaria_w,
				tb_ie_digito_agencia_w,
				tb_cd_conta_w,
				tb_ie_digito_conta_w,
				tb_ie_endereco_boleto_w,
				tb_nr_seq_conta_banco_w,
				tb_ie_gerar_cobr_escrit_w,
				tb_nr_seq_conta_banco_deb_au_w,
				tb_ie_proporcional_w,
				tb_nr_seq_compl_pf_tel_adic_w,
				tb_nr_seq_compl_pj_w,
				tb_nr_seq_tipo_compl_adic_w,
				tb_ie_nota_titulo_w,
				tb_ie_tipo_formacao_preco_w,
				tb_nr_serie_mensalidade_w,
				tb_nr_parcela_w,
				tb_nr_seq_motivo_susp_w,
				tb_ie_tipo_estipulante_w,
				tb_ie_indice_correcao_w,
				nm_usuario_p);

pls_lote_mensalidade_w	:= pls_mens_selecao_benef_pck.get_lote_mensalidade;
dt_referencia_lote_w	:= trunc(pls_lote_mensalidade_w.dt_mesano_referencia,'month');

for r_c01_w in C01 loop
	begin
	ie_log_erro_w		:= 'N';
	
	if	(r_c01_w.nr_seq_contrato is not null) then
		select	decode(cd_pf_estipulante,null,'PJ','PF')
		into	ie_tipo_estipulante_w
		from	pls_contrato
		where	nr_sequencia	= r_c01_w.nr_seq_contrato;
	elsif	(r_c01_w.nr_seq_intercambio is not null) then
		select	decode(cd_pessoa_fisica,null,'PJ','PF')
		into	ie_tipo_estipulante_w
		from	pls_intercambio
		where	nr_sequencia	= r_c01_w.nr_seq_intercambio;
	end if;
	
	consiste_suspensao_mensalidade(r_c01_w.nr_seq_contrato, r_c01_w.nr_seq_pagador, dt_referencia_lote_w, r_c01_w.ie_tipo_pagador, nr_seq_motivo_susp_w, ie_mensalidade_suspensa_w);
	
	if	(ie_mensalidade_suspensa_w = 'N') and
		(obter_se_restricao_uma_mens(r_c01_w.nr_seq_pagador, r_c01_w.nr_seq_contrato, r_c01_w.nr_seq_intercambio, ie_tipo_estipulante_w, dt_referencia_lote_w) = 'N') then
		
		qt_inicial_w	:= 0;
		if	(pls_lote_mensalidade_w.ie_mensalidade_mes_anterior = 'S') then
			if	(pls_lote_mensalidade_w.ie_mens_ant_agrupar = 'N') then
				qt_meses_futuros_w	:= 0;
				qt_meses_anterior_w	:= pls_mensalidade_util_pck.get_qt_meses_ant;
				if	(pls_lote_mensalidade_w.ie_mens_ant_mes_atual = 'N') then
					qt_inicial_w	:= 1;
				end if;
			else
				qt_meses_futuros_w	:= 0;
				qt_meses_anterior_w	:= 0;
			end if;
		else
			if	(pls_lote_mensalidade_w.ie_gerar_mensalidade_futura = 'S') then
				qt_meses_futuros_w	:= nvl(pls_lote_mensalidade_w.qt_meses_mensalidade_futura,1)-1;
			else
				pls_mensalidade_util_pck.obter_regra_mens_futura(r_c01_w.nr_seq_contrato, r_c01_w.nr_seq_intercambio, ie_tipo_estipulante_w, dt_referencia_lote_w, qt_interv_mes_mensalidade_w, ie_agrupar_valor_w);
				
				if	(ie_agrupar_valor_w = 'N') and
					(qt_interv_mes_mensalidade_w > 0) then
					qt_meses_futuros_w	:= qt_interv_mes_mensalidade_w-1;
				else
					qt_meses_futuros_w	:= 0;
				end if;
			end if;
			
			pls_mensalidade_util_pck.obter_regra_data_limite(r_c01_w.nr_seq_contrato, r_c01_w.nr_seq_intercambio, ie_tipo_estipulante_w, dt_referencia_lote_w,
									nr_seq_regra_limite_w, ie_agrupar_valor_lim_w, qt_dias_vencimento_w, ie_primeira_mensalidade_w, ie_data_base_adesao_w);
			if	(ie_agrupar_valor_lim_w = 'N') and
				(nr_seq_regra_limite_w is not null) and
				(pls_mensalidade_util_pck.get_qt_meses_ant > 0) then
				qt_meses_anterior_w	:= pls_mensalidade_util_pck.get_qt_meses_ant;
			else
				qt_meses_anterior_w	:= 0;
			end if;
		end if;
		
		qt_meses_mensalidade_w	:= qt_meses_futuros_w + qt_meses_anterior_w;
		for i in qt_inicial_w..qt_meses_mensalidade_w loop
			begin
			if	(i <= qt_meses_futuros_w) then
				dt_referencia_w			:= add_months(dt_referencia_lote_w, i);
				ie_mensalidade_anterior_w	:= 'N';
			else
				ie_mensalidade_anterior_w	:= 'S';
				dt_referencia_w			:= add_months(dt_referencia_lote_w, -(i-qt_meses_futuros_w));
			end if;
			dt_referencia_fimdia_w	:= fim_dia(dt_referencia_w);
			
			begin
			select	*
			into	pls_contrato_pagador_fin_w
			from	pls_contrato_pagador_fin
			where	nr_seq_pagador	= r_c01_w.nr_seq_pagador
			and	dt_inicio_vigencia <= dt_referencia_fimdia_w
			and	((dt_fim_vigencia >= dt_referencia_fimdia_w) or (dt_fim_vigencia is null));
			exception
			when TOO_MANY_ROWS then
				ie_log_erro_w	:= 'S';
				
				pls_gerar_mens_log_erro(nr_seq_lote_p,r_c01_w.nr_seq_pagador,null,null,
							wheb_mensagem_pck.get_texto(1180009),cd_estabelecimento_p,nm_usuario_p);
			when others then
				begin --Se nao encontrar a informacao financeira pela data, procura truncando a data inicial por mes
				select	*
				into	pls_contrato_pagador_fin_w
				from	pls_contrato_pagador_fin
				where	nr_seq_pagador	= r_c01_w.nr_seq_pagador
				and	trunc(dt_inicio_vigencia,'month') <= dt_referencia_fimdia_w
				and	((dt_fim_vigencia >= dt_referencia_fimdia_w) or (dt_fim_vigencia is null));
				exception
				when others then
					ie_log_erro_w	:= 'S';
					
					select	count(1)
					into	qt_inf_finc_futura_w
					from	pls_contrato_pagador_fin
					where	nr_seq_pagador = r_c01_w.nr_seq_pagador
					and	dt_inicio_vigencia > dt_referencia_fimdia_w;
					
					if	(ie_mensalidade_anterior_w = 'N') and
						(((pls_mensalidade_util_pck.get_ie_consis_inf_fin_pag = 'N') and (qt_inf_finc_futura_w = 0)) or
						(pls_mensalidade_util_pck.get_ie_consis_inf_fin_pag = 'S')) then
						pls_gerar_mens_log_erro(nr_seq_lote_p,r_c01_w.nr_seq_pagador,null,null,
									wheb_mensagem_pck.get_texto(1180010),cd_estabelecimento_p,nm_usuario_p);
					end if;
				end;
			end;
			
			if	(ie_log_erro_w = 'N') and
				(obter_se_restringe_pagador(pls_contrato_pagador_fin_w) = 'N') then
				if	(r_c01_w.nr_seq_contrato is not null) then
					ie_contrato_w	:= 'O';
					select	dt_contrato
					into	dt_contrato_w
					from	pls_contrato
					where	nr_sequencia	= r_c01_w.nr_seq_contrato;
					
					nr_parcela_w	:=	(trunc(months_between(dt_referencia_w,trunc(dt_contrato_w,'month'))) + 1);
					
					select	max(nr_sequencia)
					into	nr_seq_contrato_plano_w
					from	pls_contrato_plano
					where	nr_seq_contrato	= r_c01_w.nr_seq_contrato
					and	ie_situacao	= 'A';
					
					if	(nr_seq_contrato_plano_w is null) then
						select	max(nr_sequencia)
						into	nr_seq_contrato_plano_w
						from	pls_contrato_plano
						where	nr_seq_contrato	= r_c01_w.nr_seq_contrato
						and	ie_situacao	= 'I';
					end if;
					
					select	max(b.ie_preco),
						max(b.ie_tipo_contratacao)
					into	ie_preco_w,
						ie_tipo_contratacao_w
					from	pls_contrato_plano	a,
						pls_plano		b
					where	a.nr_seq_plano		= b.nr_sequencia
					and	a.nr_sequencia		= nr_seq_contrato_plano_w;
				elsif	(r_c01_w.nr_seq_intercambio is not null) then
					select	dt_inclusao,
						ie_tipo_contrato
					into	dt_contrato_w,
						ie_tipo_contrato_w
					from	pls_intercambio
					where	nr_sequencia	= r_c01_w.nr_seq_intercambio;
					
					nr_parcela_w	:=	(trunc(months_between(dt_referencia_w,trunc(dt_contrato_w,'month'))) + 1);
					
					if	(ie_tipo_contrato_w = 'I') then
						ie_contrato_w	:= 'I';
					else
						ie_contrato_w	:= 'A';
					end if;
					
					select	max(nr_sequencia)
					into	nr_seq_intercambio_plano_w
					from	pls_intercambio_plano
					where	nr_seq_intercambio	= r_c01_w.nr_seq_intercambio
					and	ie_situacao	= 'A';
					
					if	(nr_seq_contrato_plano_w is null) then
						select	max(nr_sequencia)
						into	nr_seq_contrato_plano_w
						from	pls_intercambio_plano
						where	nr_seq_intercambio	= r_c01_w.nr_seq_intercambio
						and	ie_situacao		= 'I';
					end if;
					
					select	max(b.ie_preco),
						max(b.ie_tipo_contratacao)
					into	ie_preco_w,
						ie_tipo_contratacao_w
					from	pls_intercambio_plano	a,
						pls_plano		b
					where	a.nr_seq_plano		= b.nr_sequencia
					and	a.nr_sequencia		= nr_seq_intercambio_plano_w;
				end if;
				
				tb_nr_seq_pagador_w(nr_indice_w)		:= r_c01_w.nr_seq_pagador;
				tb_nr_seq_pagador_fin_w(nr_indice_w)		:= pls_contrato_pagador_fin_w.nr_sequencia;
				tb_dt_referencia_w(nr_indice_w)			:= dt_referencia_w;
				tb_dt_vencimento_w(nr_indice_w)			:= obter_dt_vencimento_mens(r_c01_w.nr_seq_pagador, pls_contrato_pagador_fin_w.dt_dia_vencimento, pls_contrato_pagador_fin_w.ie_mes_vencimento,
														dt_referencia_w, r_c01_w.dt_primeira_mensalidade, r_c01_w.ie_primeira_mensalidade_gerada, cd_estabelecimento_p,
														pls_contrato_pagador_fin_w.qt_meses_vencimento);
				tb_nr_seq_forma_cobranca_w(nr_indice_w)		:= pls_contrato_pagador_fin_w.nr_seq_forma_cobranca;
				tb_cd_banco_w(nr_indice_w)			:= pls_contrato_pagador_fin_w.cd_banco;
				tb_cd_agencia_bancaria_w(nr_indice_w)		:= pls_contrato_pagador_fin_w.cd_agencia_bancaria;
				tb_ie_digito_agencia_w(nr_indice_w)		:= pls_contrato_pagador_fin_w.ie_digito_agencia;
				tb_cd_conta_w(nr_indice_w)			:= pls_contrato_pagador_fin_w.cd_conta;
				tb_ie_digito_conta_w(nr_indice_w)		:= pls_contrato_pagador_fin_w.ie_digito_conta;
				tb_ie_endereco_boleto_w(nr_indice_w)		:= r_c01_w.ie_endereco_boleto;
				tb_nr_seq_conta_banco_w(nr_indice_w)		:= pls_contrato_pagador_fin_w.nr_seq_conta_banco;
				tb_ie_gerar_cobr_escrit_w(nr_indice_w)		:= pls_contrato_pagador_fin_w.ie_gerar_cobr_escrit;
				tb_nr_seq_conta_banco_deb_au_w(nr_indice_w)	:= pls_contrato_pagador_fin_w.nr_seq_conta_banco_deb_aut;
				tb_ie_proporcional_w(nr_indice_w)		:= obter_se_proporcional(r_c01_w.ie_calc_primeira_mens);
				tb_nr_seq_compl_pf_tel_adic_w(nr_indice_w)	:= r_c01_w.nr_seq_compl_pf_tel_adic;
				tb_nr_seq_compl_pj_w(nr_indice_w)		:= r_c01_w.nr_seq_compl_pj;
				tb_nr_seq_tipo_compl_adic_w(nr_indice_w)	:= r_c01_w.nr_seq_tipo_compl_adic;
				tb_ie_nota_titulo_w(nr_indice_w)		:= obter_geracao_nota_titulo(pls_contrato_pagador_fin_w.ie_geracao_nota_titulo, r_c01_w.cd_pessoa_fisica);
				tb_ie_tipo_formacao_preco_w(nr_indice_w)	:= obter_tipo_formacao_preco(ie_preco_w);
				tb_nr_serie_mensalidade_w(nr_indice_w)		:= obter_serie_mensalidade(ie_contrato_w, ie_tipo_contratacao_w, ie_preco_w, r_c01_w.ie_situacao_trabalhista, r_c01_w.nr_seq_classif_itens);
				tb_nr_parcela_w(nr_indice_w)			:= nr_parcela_w;
				tb_nr_seq_motivo_susp_w(nr_indice_w)		:= nr_seq_motivo_susp_w;
				tb_ie_tipo_estipulante_w(nr_indice_w)		:= ie_tipo_estipulante_w;
				tb_ie_indice_correcao_w(nr_indice_w)		:= pls_contrato_pagador_fin_w.ie_indice_correcao;
				
				if	(nr_indice_w >= (pls_util_pck.qt_registro_transacao_w-1)) then
					atualizar_pls_mensalidade(	tb_nr_seq_pagador_w,
									tb_nr_seq_pagador_fin_w,
									tb_dt_referencia_w,
									tb_dt_vencimento_w,
									tb_nr_seq_forma_cobranca_w,
									tb_cd_banco_w,
									tb_cd_agencia_bancaria_w,
									tb_ie_digito_agencia_w,
									tb_cd_conta_w,
									tb_ie_digito_conta_w,
									tb_ie_endereco_boleto_w,
									tb_nr_seq_conta_banco_w,
									tb_ie_gerar_cobr_escrit_w,
									tb_nr_seq_conta_banco_deb_au_w,
									tb_ie_proporcional_w,
									tb_nr_seq_compl_pf_tel_adic_w,
									tb_nr_seq_compl_pj_w,
									tb_nr_seq_tipo_compl_adic_w,
									tb_ie_nota_titulo_w,
									tb_ie_tipo_formacao_preco_w,
									tb_nr_serie_mensalidade_w,
									tb_nr_parcela_w,
									tb_nr_seq_motivo_susp_w,
									tb_ie_tipo_estipulante_w,
									tb_ie_indice_correcao_w,
									nm_usuario_p);
					nr_indice_w	:= 0;
				else
					nr_indice_w	:= nr_indice_w + 1;
				end if;
			end if;
			end;
		end loop;
	end if;
	end;
end loop;

-- se sobrou alguma coisa manda para o banco
atualizar_pls_mensalidade(	tb_nr_seq_pagador_w,
				tb_nr_seq_pagador_fin_w,
				tb_dt_referencia_w,
				tb_dt_vencimento_w,
				tb_nr_seq_forma_cobranca_w,
				tb_cd_banco_w,
				tb_cd_agencia_bancaria_w,
				tb_ie_digito_agencia_w,
				tb_cd_conta_w,
				tb_ie_digito_conta_w,
				tb_ie_endereco_boleto_w,
				tb_nr_seq_conta_banco_w,
				tb_ie_gerar_cobr_escrit_w,
				tb_nr_seq_conta_banco_deb_au_w,
				tb_ie_proporcional_w,
				tb_nr_seq_compl_pf_tel_adic_w,
				tb_nr_seq_compl_pj_w,
				tb_nr_seq_tipo_compl_adic_w,
				tb_ie_nota_titulo_w,
				tb_ie_tipo_formacao_preco_w,
				tb_nr_serie_mensalidade_w,
				tb_nr_parcela_w,
				tb_nr_seq_motivo_susp_w,
				tb_ie_tipo_estipulante_w,
				tb_ie_indice_correcao_w,
				nm_usuario_p);

end inserir_mensalidade_pagador;

end pls_mens_pagador_pck;
/