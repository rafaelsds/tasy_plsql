create or replace
package	pls_utl_file_pck is

	-- Gerar Criar Novo Arquivo
	-- Declara��o para cria��o de novo arquivo UTL_FILE (Declara apenas no come�o da rotina)
	procedure novo_arquivo (	cd_evento_p		evento_tasy_utl_file.cd_evento%type,
					nm_arquivo_p		varchar2, -- nome do arquivo com a exten��o, exemplo: teste.txt
					ie_valida_dir_ws_p	varchar2, -- valida o diret�rio windows
					qt_tamanho_max_linha_p	number, --tamanho m�ximo da linha. O valor deve estar entre 1 e 32767. O valor padr�o � 1024, se passar null
					ds_local_p		out varchar2,
					ie_local_p		varchar2 default 'W' ); -- W Windows - U UTL
	
	-- Declara��o para escrever dentro do arquivo UTL_FILE (Inserir cada linha no arquivo)
	procedure escrever	(	ds_conteudo_p	varchar2);
	
	
	-- Ler um arquivo existente
	-- Abrir arquivo para leitura
	procedure nova_leitura	(	cd_evento_p		evento_tasy_utl_file.cd_evento%type,
					nm_arquivo_p		varchar2,-- nome do arquivo com a exten��o, exemplo: teste.txt
					ie_acao_p		varchar2 default 'R'); 

	-- Retornar linha do arquivo
	-- Interessante utilizar com while passando como par�metro ie_leitura_p
	procedure ler	(	ds_conteudo_p	out varchar2,
				ie_leitura_p	out boolean );
	
	
	-- SEMPRE ULTIMA LINHA. Declara��o para fechar o arquivo UTL_FILE (Declarar apenas no final da rotina)
	procedure fechar_arquivo(	ie_fechar_p		varchar2 default 'U'); -- U: Fechar arquivo	| T: Fechar todos os arquivos da sess�o
	
	-- Copiar arquivo
	procedure copiar_arquivo(	cd_evento_origem_p	evento_tasy_utl_file.cd_evento%type,
					nm_arquivo_origem_p	varchar2, -- nome do arquivo de origem com a exten��o, exemplo: teste.txt
					cd_evento_dest_p	evento_tasy_utl_file.cd_evento%type,
					nm_arquivo_dest_p	varchar2, -- nome do arquivo de destino com a exten��o, exemplo: teste2.txt (este arquivo n�o pode existir)
					nr_seq_linha_ini_p	number, -- linha inicial a ser copiada
					nr_seq_linha_fim_p	number ); -- linha final a ser copiada
	
	-- Excluir arquivo
	procedure excluir_arquivo(	cd_evento_exc_p		evento_tasy_utl_file.cd_evento%type,
					nm_arquivo_exc_p	varchar2); -- nome do arquivo para exclus�o com a exten��o, exemplo: teste.txt
	
	-- Obtem informa��es sobre o arquivo
	function obter_dados_arquivo (	cd_evento_p		evento_tasy_utl_file.cd_evento%type,
					nm_arquivo_p		varchar2, -- nome do arquivo com a exten��o, exemplo: teste.txt
					ie_opcao_p		varchar2)
					return varchar2;
					
	-- Obter os diretorios UTL / Windows
	function obter_local_windows(	cd_evento_p			evento_tasy_utl_file.cd_evento%type) 
					return varchar2;
					
	-- Obter os diretorios UTL / Windows / Linux
	function obter_local(		cd_evento_p			evento_tasy_utl_file.cd_evento%type,
					ie_opcao_p			varchar2) 
					return varchar2;
					
	-- Obs: Foram utilizado os metodos da fun��o SYS.UTL_FILE
	-- Fun��o: Cadastros gerais (Shift+F11)
	-- Pasta: Sistema -> Usu�rio -> Eventos grava��o/leitura de arquivos (UTL_FILE)
	-- Diret�rio UTL_FILE reconhecido pelo ORACLE utilizado na Philips: /oraprd03/utlfile/
	-- Diret�rio UTL_FILE reconhecido pelo WINDOWS utilizado na Philips: \\192.168.0.230\utlfile
	
	-- Procedures e Functions utilizadas:
	-- fopen: Abrir arquivo
	-- is_open: Verificar se arquivo esta aberto
	-- fclose: Fechar arquivo
	-- fclose_all: Fechar todos os arquivos da sess�o
	-- get_line: Retornar linha do arquivo
	-- put_line: Gravar informa��o no arquivo
	-- fflush: For�a a grava��o no arquivo
	-- fremove: Excluir arquivo
	-- fcopy: Copiar arquivo (inteiro ou parcial)
	-- fgetattr: Obter informa��es sobre o arquivo
	
	/* EXEMPLO DE UTILIZA��O DA ROTINA
	declare
	ds_local_w		varchar2(4000);
	ds_conteudo_w	varchar2(4000);
	ie_leitura_w	boolean := true;
	
	begin
	-- 1. Escrever no arquivo
	pls_utl_file_pck.novo_arquivo( 20, 'tasy.txt', 'S', null, ds_local_w, 'U');
	pls_utl_file_pck.escrever('OPS1');
	pls_utl_file_pck.escrever('OPS2');
	pls_utl_file_pck.fechar_arquivo;
	
	-- 2. Ler o arquivo
	pls_utl_file_pck.nova_leitura( 20, 'tasy.txt');
	while (ie_leitura_w) loop
		pls_utl_file_pck.ler( ds_conteudo_w, ie_leitura_w);
	 
		if (ie_leitura_w) then
			insert into tabela ( campo ) values ( ds_conteudo_w );
		end if;
	end loop;
	commit;
	pls_utl_file_pck.fechar_arquivo;
	
	-- 3. Copiar arquivo 
	pls_utl_file_pck.copiar_arquivo(20,'tasy.txt',20,'tasy2.txt',2,3);
	
	-- 4. Excluir arquivo
	pls_utl_file_pck.excluir_arquivo(20,'tasy2.txt');
	
	end;
	/ */
	
end pls_utl_file_pck;
/

create or replace 
package body pls_utl_file_pck is

	-- Declara��o para criar e abrir o novo arquivo UTL_FILE
	procedure criar_abrir	(	ds_local_p		varchar2,
					nm_arquivo_p		varchar2,
					ie_acao_p		varchar2,
					qt_tamanho_max_linha_p	number);
	
	-- Declara��o para tratamento de erro
	procedure tratar_erro	(	ds_erro_p		varchar2,
					nr_seq_erro_p		dic_objeto.nr_sequencia%type );
	
	-- Validar diret�rio Windows
	procedure validar_diretorio_windows	(ds_local_windows_p	varchar2,
						nr_seq_erro_p	out	dic_objeto.nr_sequencia%type );
	
	-- Obter os diretorios UTL / Windows
	procedure obter_local_utl(	cd_evento_p			evento_tasy_utl_file.cd_evento%type,
					ds_local_utl_p		out	varchar,
					ds_local_windows_p	out	varchar,
					ie_valida_dir_ws_p		varchar);
	
	-- Obter o erro causado no UTL_FILE
	function obter_erro_utl	( sqlcode_p	number )
				return number;
	
	-- Variaveis
	ds_arquivo_w		utl_file.file_type;
	ds_erro_w		varchar2(4000) := null;
	nr_seq_erro_w		dic_objeto.nr_sequencia%type := null;

-- Gerar novo arquivo
procedure novo_arquivo (	cd_evento_p			evento_tasy_utl_file.cd_evento%type,
				nm_arquivo_p			varchar2,
				ie_valida_dir_ws_p		varchar2,
				qt_tamanho_max_linha_p		number,
				ds_local_p		out	varchar2,
				ie_local_p			varchar2 default 'W' ) is 

-- Variaveis				
ds_local_w		varchar2(4000) := null;
ds_local_windows_w	varchar2(4000) := null;

begin
-- Obter local reconhecido pelo ORACLE e pelo WINDOWS para gera��o do arquivo UTL_FILE
pls_utl_file_pck.obter_local_utl( cd_evento_p, ds_local_w, ds_local_windows_w, ie_valida_dir_ws_p);

-- Criar e abrir o arquivo texto por UTL_FILE dentro do diret�rio reconhecido pelo ORACLE
pls_utl_file_pck.criar_abrir( ds_local_w, nm_arquivo_p, 'W', qt_tamanho_max_linha_p);

ds_local_p := ds_local_w;
if	(ie_local_p = 'W') then
	ds_local_p := ds_local_windows_w;
end if;

end novo_arquivo;

-- Criar e abrir o arquivo
procedure criar_abrir	(	ds_local_p		varchar2,
				nm_arquivo_p		varchar2,
				ie_acao_p		varchar2,
				qt_tamanho_max_linha_p	number) is
begin

begin
-- Criar e abrir o arquivo texto por UTL_FILE dentro do diret�rio reconhecido pelo ORACLE
ds_arquivo_w := utl_file.fopen( ds_local_p, nm_arquivo_p, ie_acao_p, qt_tamanho_max_linha_p);
exception
when others then
	-- Retornar o erro gerado
	nr_seq_erro_w := pls_utl_file_pck.obter_erro_utl(sqlcode);
end;

-- Caso tenha erro na cria��o ou na abertura do arquivo por UTL_FILE, barrar o processo
pls_utl_file_pck.tratar_erro( null, nr_seq_erro_w);

end criar_abrir;

-- Escrever no arquivo
procedure escrever (	ds_conteudo_p varchar2 ) is
begin
-- Verificar se o arquivo esta aberto para edi��o
if	(utl_file.is_open(ds_arquivo_w)) then
	-- Escrever a informa��o no arquivo
	begin
	utl_file.put_line( ds_arquivo_w, ds_conteudo_p || chr(13));
	utl_file.fflush( ds_arquivo_w );
	exception
	when others then
		nr_seq_erro_w := 460840; -- O arquivo n�o est� aberto para edi��o.
	end;
end if;

-- Caso o arquivo n�o esteja editavel, barrar o processo
pls_utl_file_pck.tratar_erro( null, nr_seq_erro_w);

end escrever;

-- Tratar a exibi��o de erro
procedure tratar_erro	(	ds_erro_p		varchar2,
				nr_seq_erro_p		dic_objeto.nr_sequencia%type) is
begin

if	(ds_erro_p is not null) or
	(nr_seq_erro_p is not null) then
	ds_erro_w := nvl(ds_erro_p,wheb_mensagem_pck.get_texto(nr_seq_erro_p,null));
	
	-- Exibir o erro e barrar o processo
	wheb_mensagem_pck.exibir_mensagem_abort(460257,'DS_ERRO=' || ds_erro_w);
end if;

end tratar_erro;

-- Fechar o arquivo para edi��o
procedure fechar_arquivo(ie_fechar_p	varchar2 default 'U') is
begin
-- Fechar o arquivo
if	(ie_fechar_p = 'U') then
	utl_file.fclose(ds_arquivo_w);

-- Fechar todos os arquivos da sess�o
elsif	(ie_fechar_p = 'T') then
	utl_file.fclose_all;
end if;

end fechar_arquivo;

-- Validar diret�rio Windows
procedure validar_diretorio_windows	(ds_local_windows_p	varchar2,
					nr_seq_erro_p	out	dic_objeto.nr_sequencia%type ) is
begin

-- Validar para que o diret�rio Windows seja obrigat�rio
if	(ds_local_windows_p is null) then
	nr_seq_erro_p := 460844; -- Favor verificar o cadastro UTL_FILE, necess�rio informar o campo Local na rede (Windows)
end if;

-- Validar se o diret�rio � comum para o Windows
if	(ds_local_windows_p is not null) and
	(instr(ds_local_windows_p, '\') = 0) then --'
	nr_seq_erro_p := 460848; -- Favor verificar o cadastro UTL_FILE, necess�rio informar um diret�rio v�lido para o Windows no campo Local na rede (Windows)
end if;

end validar_diretorio_windows;

-- Obter o erro causado no UTL_FILE
function obter_erro_utl	( sqlcode_p	number )
			return number is

begin

if	(sqlcode = -29289) then
	return 281845; -- O acesso ao arquivo foi negado pelo sistema operacional (access_denied).
	
elsif	(sqlcode = -29298) then
	return 281846; -- O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).
	
elsif	(sqlcode = -29291) then
	return 281847; -- N�o foi poss�vel apagar o arquivo (delete_failed).
	
elsif	(sqlcode = -29286) then
	return 281848; -- Erro interno desconhecido no package UTL_FILE (internal_error).
	
elsif	(sqlcode = -29282) then
	return 281849; -- O handle do arquivo n�o existe (invalid_filehandle).
			
elsif	(sqlcode = -29288) then
	return 281850; -- O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).
			
elsif	(sqlcode = -29287) then
	return 281851; -- O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).
	
elsif	(sqlcode = -29281) then
	return 281852; -- O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).
			
elsif	(sqlcode = -29290) then
	return 281853; -- O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).
			
elsif	(sqlcode = -29283) then
	return 1080852; -- O arquivo n�o p�de ser aberto ou n�o foi encontrado no diret�rio especificado.
	
elsif	(sqlcode = -29280) then
	return 281855; -- O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).
	
elsif	(sqlcode = -29284) then
	return 281856; -- N�o � poss�vel efetuar a leitura do arquivo (read_error).
			
elsif	(sqlcode = -29292) then
	return 281857; -- N�o � poss�vel renomear o arquivo.
			
elsif	(sqlcode = -29285) then
	return 281858; -- N�o foi poss�vel gravar no arquivo (write_error).
			
else
	return 281859; -- Erro desconhecido no package UTL_FILE.
end if;

end;

-- Obter os diretorios UTL / Windows
procedure obter_local_utl(	cd_evento_p			evento_tasy_utl_file.cd_evento%type,
				ds_local_utl_p		out	varchar,
				ds_local_windows_p	out	varchar,
				ie_valida_dir_ws_p		varchar) is
begin

begin
-- Obter local reconhecido pelo ORACLE para gera��o do arquivo UTL_FILE
obter_evento_utl_file( cd_evento_p, null, ds_local_utl_p, ds_erro_w);
exception
when others then
	ds_local_utl_p 		:= null;
end;

-- Obter local reconhecido pelo WINDOWS para gera��o do arquivo UTL_FILE
if	(ie_valida_dir_ws_p = 'S') and
	(ds_erro_w is null)  then
	begin
	pls_obter_dir_rede_utl_file( cd_evento_p, null, ds_local_windows_p, ds_erro_w);
	exception
	when others then
		ds_local_windows_p	:= null;
	end;
	
	-- Validar diret�rio Windows
	if	(ds_erro_w is null) then
		validar_diretorio_windows( ds_local_windows_p, nr_seq_erro_w);
	end if;
end if;

-- Caso tenha erro no diret�rio do arquivo por UTL_FILE, barrar o processo
pls_utl_file_pck.tratar_erro( ds_erro_w, nr_seq_erro_w);

end obter_local_utl;

-- Abrir arquivo para leitura
procedure nova_leitura	(	cd_evento_p		evento_tasy_utl_file.cd_evento%type,
				nm_arquivo_p		varchar2,
				ie_acao_p		varchar2 default 'R') is

-- Variaveis
ds_local_w		varchar2(4000) := null;
ds_local_windows_w	varchar2(4000) := null;

begin
-- Obter local reconhecido pelo ORACLE para gera��o do arquivo UTL_FILE
pls_utl_file_pck.obter_local_utl( cd_evento_p, ds_local_w, ds_local_windows_w, 'N');

-- Abrir para leitura o arquivo texto por UTL_FILE dentro do diret�rio reconhecido pelo ORACLE
pls_utl_file_pck.criar_abrir( ds_local_w, nm_arquivo_p, nvl(ie_acao_p,'R'), 4000);

end nova_leitura;

-- Retornar linha do arquivo
procedure ler	(	ds_conteudo_p	out varchar2,
			ie_leitura_p	out boolean ) is
begin
-- Verificar se o arquivo esta aberto para leitura
if	(utl_file.is_open(ds_arquivo_w)) then
	ie_leitura_p := true;

	-- Obter a linha
	begin
	utl_file.get_line( ds_arquivo_w, ds_conteudo_p);
	exception
	when others then
		ie_leitura_p := false;
	end;
end if;

end ler;

-- Copiar arquivo
procedure copiar_arquivo(	cd_evento_origem_p	evento_tasy_utl_file.cd_evento%type,
				nm_arquivo_origem_p	varchar2,
				cd_evento_dest_p	evento_tasy_utl_file.cd_evento%type,
				nm_arquivo_dest_p	varchar2,
				nr_seq_linha_ini_p	number,
				nr_seq_linha_fim_p	number ) is

-- Variaveis
ds_local_origem_w	varchar2(4000) := null;
ds_local_dest_w		varchar2(4000) := null;
ds_local_windows_w	varchar2(4000) := null;

begin
-- Obter local reconhecido pelo ORACLE para gera��o do arquivo UTL_FILE
pls_utl_file_pck.obter_local_utl( cd_evento_origem_p, ds_local_origem_w, ds_local_windows_w, 'N');

if	(nr_seq_erro_w is null) then
	-- Obter local reconhecido pelo ORACLE para gera��o do arquivo UTL_FILE
	pls_utl_file_pck.obter_local_utl( nvl(cd_evento_dest_p,cd_evento_origem_p), ds_local_dest_w, ds_local_windows_w, 'N');
end if;

-- Copiar as informa��es do arquivo origem para arquivo destino (o arquivo destino n�o pode existir)
begin
utl_file.fcopy( ds_local_origem_w, nm_arquivo_origem_p, ds_local_dest_w, nm_arquivo_dest_p, nr_seq_linha_ini_p, nr_seq_linha_fim_p);
exception
when others then
	nr_seq_erro_w := 462978; -- N�o foi poss�vel copiar o arquivo por UTL_FILE
end;

-- Caso tenha erro para copiar o diret�rio UTL_FILE
pls_utl_file_pck.tratar_erro( null, nr_seq_erro_w);

end copiar_arquivo;

-- Excluir arquivo
procedure excluir_arquivo(	cd_evento_exc_p		evento_tasy_utl_file.cd_evento%type,
				nm_arquivo_exc_p	varchar2) is

-- Variaveis
ds_local_exc_w		varchar2(4000) := null;
ds_local_windows_w	varchar2(4000) := null;

begin
-- Obter local reconhecido pelo ORACLE para gera��o do arquivo UTL_FILE
pls_utl_file_pck.obter_local_utl( cd_evento_exc_p, ds_local_exc_w, ds_local_windows_w, 'N');

-- Comando para excluir arquivo
begin
utl_file.fremove( ds_local_exc_w, nm_arquivo_exc_p);
exception
when others then
	nr_seq_erro_w := 466335; -- N�o foi poss�vel excluir o arquivo por UTL_FILE
end;

-- Caso tenha erro para excluir o diret�rio UTL_FILE
pls_utl_file_pck.tratar_erro( null, nr_seq_erro_w);

end excluir_arquivo;

-- Obtem informa��es sobre o arquivo
function obter_dados_arquivo	(	cd_evento_p		evento_tasy_utl_file.cd_evento%type,
					nm_arquivo_p		varchar2, -- nome do arquivo com a exten��o, exemplo: teste.txt
					ie_opcao_p		varchar2)
					return varchar2 is

-- Variaveis
ds_local_w		varchar2(4000) := null;
ds_local_windows_w	varchar2(4000) := null;
ds_retorno_w		varchar2(4000) := null;
ie_existe_arq_w		boolean;
nr_tamanho_arq_w	number(20,4);
nr_tam_bloco_arq_w	number(20,4);

begin
-- Obter local reconhecido pelo ORACLE para gera��o do arquivo UTL_FILE
pls_utl_file_pck.obter_local_utl( cd_evento_p, ds_local_w, ds_local_windows_w, 'N');

begin
-- Comando para verificar informa��es sobre o arquivo
utl_file.fgetattr(ds_local_w,nm_arquivo_p, ie_existe_arq_w, nr_tamanho_arq_w, nr_tam_bloco_arq_w);
exception
when others then
	ie_existe_arq_w		:= false;
	nr_tamanho_arq_w	:= 0;
	nr_tam_bloco_arq_w	:= 0;
end;

--	Existe o arquivo no diret�rio UTL
if	(ie_opcao_p = 'E') then
	ds_retorno_w := 'N';
	if	(ie_existe_arq_w) then
		ds_retorno_w := 'S';
	end if;

--	Tamanho do arquivo em bytes.
elsif	(ie_opcao_p = 'T') then
	ds_retorno_w := nr_tamanho_arq_w;
	
--	Sistema de arquivos tamanho do bloco em bytes.
elsif	(ie_opcao_p = 'B') then
	ds_retorno_w := nr_tam_bloco_arq_w;
end if;

return ds_retorno_w;

end obter_dados_arquivo;

-- Obtem local do windows do UTL
function obter_local_windows	(	cd_evento_p		evento_tasy_utl_file.cd_evento%type) 
					return varchar2 is

ds_retorno_w		varchar2(4000) := null;

begin
pls_obter_dir_rede_utl_file( cd_evento_p, null, ds_retorno_w, ds_erro_w);

if	(ds_erro_w is not null) then
	tratar_erro( ds_erro_w, null);
end if;

return ds_retorno_w;

end obter_local_windows;

function obter_local(	cd_evento_p			evento_tasy_utl_file.cd_evento%type,
			ie_opcao_p			varchar2) return varchar2 is
			
ds_local_windows_w	varchar2(4000) := null;
ds_local_w		varchar2(4000) := null;
ds_retorno_w		varchar2(4000) := null;

begin

if	(ie_opcao_p = 'W') then
	ds_retorno_w	:= pls_utl_file_pck.obter_local_windows(cd_evento_p);
	
elsif	(ie_opcao_p = 'L') then
	pls_utl_file_pck.obter_local_utl( cd_evento_p, ds_local_w, ds_local_windows_w, 'N');
	
	ds_retorno_w	:= ds_local_w;
end if;

return ds_retorno_w;

end;

end pls_utl_file_pck;
/