create or replace package new_icu_notifications_pck as

type t_central_notices is record (
	ds_titulo      		varchar2(2000),
	ds_conteudo      	varchar2(4000),
	dt_criacao       	date,
	nm_usuarios_destino	varchar2(2000),
	cd_profiles_destino varchar2(2000),
	cd_grupos_destino   varchar2(2000),
	nr_prioridade       number(2)
);

type t_attendances is record (
	nr_atendimento   number
);

type users_integration_not is record (
     ds_titulo  varchar2(2000)
);

type t_alert is record (
	ds_titulo	varchar2(255)
);

type t_alert_details is record (
	nr_atendimento 	atendimento_paciente.nr_atendimento%type,
	dt_evento		date,
	nm_paciente		varchar2(255),
	ds_evento		varchar2(255),
	ie_funcao		varchar2(10)
);

type t_object_central_notices is table of t_central_notices;

type t_object_attendances is table of t_attendances;

type t_object_users_integration_not is table of users_integration_not;

type t_alerts_table is table of t_alert;

type t_alert_details_table is table of t_alert_details;

procedure verify_execution(nm_function_p varchar2);

function get_approval_items(nr_atendimento_p number) return number;

function get_pending_cosign_adm(nr_atendimento_p number) return number;

function get_new_orders_message(nr_atendimento_p number) return varchar2;

function get_pending_verif_items(	cd_pessoa_fisica_p  varchar2,
									nr_atendimento_p  	number) return number;

function get_notifications(	nr_atendimento_p   	number,
							cd_pessoa_fisica_p	varchar2) return varchar2;

function get_mentor_alert(	cd_pessoa_fisica_p  varchar2,
							nr_atendimento_p  	number)	return varchar2;

function get_mentor_alert(	cd_pessoa_fisica_p  varchar2,
							nr_atendimento_p  	number,
							ie_severidade_p  	number) return varchar2;

function get_all_reminders return t_object_central_notices pipelined;

function get_reminders_list(	nm_usuario_p       		varchar2,
								cd_perfil_p       		number,
								cd_estabelecimento_p	number,
								nr_atendimento_p     	number ) return t_object_central_notices pipelined;

function get_enconunter_reminders(nr_atendimento_p number) return varchar2;

function get_all_alert return t_object_central_notices pipelined;

function get_mentor_alert_list(	nm_usuario_p		varchar2,
								nr_atendimento_p	number) return t_object_central_notices pipelined;

function get_mentor_alert_list(	nm_usuario_p    	varchar2,
								nr_atendimento_p	number,
								ie_prioridade_p   	varchar2) return t_object_central_notices pipelined;

function get_new_orders_list(	nm_usuario_p    	varchar2,
								nr_atendimento_p	number) return t_object_central_notices pipelined;

function get_all_new_orders return t_object_central_notices pipelined;

function get_new_notifications_list(	nm_usuario_p    varchar2,
										nr_atendimento_p  number) return t_object_central_notices pipelined;

function get_all_new_notifications return t_object_central_notices pipelined;

function get_new_integration_not return t_object_central_notices pipelined;

function integration_not_users(nr_atendimento_p number) return t_object_users_integration_not pipelined;

function integration_not_get_date(nr_atendimento_p number) return date;

function get_orders_user(cd_pessoa_fisica_p number, nm_usuario_p varchar2) return t_object_attendances pipelined;

function get_visualiza_alertas(nm_usuario_p varchar2, ie_tipo_alerta_p varchar2) return varchar2;

function get_new_orders_list(nm_usuario_p varchar2) return t_alerts_table pipelined;

function get_new_orders_details(nm_usuario_p varchar2) return t_alert_details_table pipelined;

function get_new_orders_items(nr_atendimento_p number) return number;

function get_approval_list(nm_usuario_p varchar2) return t_alerts_table pipelined;

function get_approval_details(nm_usuario_p varchar2) return t_alert_details_table pipelined;

function get_approval_items(nm_usuario_p varchar2, cd_pessoa_fisica_p varchar2, cd_perfil_p number, nr_atendimento_p number) return number;

function get_verification_list(nm_usuario_p varchar2) return t_alerts_table pipelined;

function get_verification_details(nm_usuario_p varchar2) return t_alert_details_table pipelined;

function get_verification_items(nm_usuario_p varchar2, cd_pessoa_fisica_p varchar2, cd_perfil_p number, nr_atendimento_p number) return number;

function get_cosign_list(nm_usuario_p varchar2) return t_alerts_table pipelined;

function get_cosign_details(nm_usuario_p varchar2) return t_alert_details_table pipelined;

function get_sub_cosign_items(nr_atendimento_p number, nm_usuario_p varchar2) return number;

function get_sub_cosign_list(nm_usuario_p varchar2) return t_alerts_table pipelined;

function get_sub_cosign_details(nm_usuario_p varchar2) return t_alert_details_table pipelined;

function get_adm_cosign_items(nr_atendimento_p number) return number;

function get_adm_cosign_list(nm_usuario_p varchar2) return t_alerts_table pipelined;

function get_adm_cosign_details(nm_usuario_p varchar2) return t_alert_details_table pipelined;

function get_overdue_items(	nm_usuario_p varchar2, cd_perfil_p number, cd_estabelecimento_p	number, nr_atendimento_p number) return number;

function get_overdue_list(nm_usuario_p varchar2) return t_alerts_table pipelined;

function get_overdue_details(nm_usuario_p varchar2) return t_alert_details_table pipelined;

function get_need_scheduled_items(nr_atendimento_p number) return number;

function get_need_scheduled_list(nm_usuario_p varchar2) return t_alerts_table pipelined;

function get_need_scheduled_details(nm_usuario_p varchar2) return t_alert_details_table pipelined;

function get_alerts_details(nm_usuario_p varchar2, ie_tipo_alerta_p varchar2) return t_alert_details_table pipelined;

end new_icu_notifications_pck;
/

create or replace package body new_icu_notifications_pck as

function get_visualiza_alertas(nm_usuario_p varchar2, ie_tipo_alerta_p varchar2) return varchar2 is

ie_visualiza_alerta_w	varchar2(1) := 'N';
cd_perfil_ativo_w		perfil.cd_perfil%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;

begin

	cd_perfil_ativo_w := obter_perfil_ativo();
	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);

	select  decode(count(*), 0, 'N', 'S')
	into	ie_visualiza_alerta_w
	from    regra_not_integracao
	where   cd_registro_notificacao = ie_tipo_alerta_p
	and     (cd_pessoa_fisica = cd_pessoa_fisica_w
	or      cd_perfil = cd_perfil_ativo_w);

	return ie_visualiza_alerta_w;

end get_visualiza_alertas;

procedure verify_execution(nm_function_p varchar2)  is

ds_erro_w  varchar2(2000);

begin

	begin
		gravar_processo_longo('',upper(nm_function_p),null);

		select  'executando'
		into  ds_erro_w
		from   v$session
		where  audsid <> (select userenv('sessionid') from dual)
		and  username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
		and  action like upper(nm_function_p||'%')
		and  status = 'ACTIVE';
		exception
		when others then
			ds_erro_w := '';
	end;

	if  ( ds_erro_w is not null )  then
		gravar_processo_longo('','',null);
		wheb_mensagem_pck.exibir_mensagem_abort(278178,'DS_ERRO='||ds_erro_w );
	end if;

end verify_execution;

function get_approval_items(nr_atendimento_p number) return number is

qt_needs_approval_w  pls_integer;

begin

	if (nr_atendimento_p is not null) then

      select	count(nr_sequencia)
      into   	qt_needs_approval_w
      from   	cpoe_action_log_v
      where   	ie_ordered_on_behalf = 'S'
      and   	dt_liberacao is null
      and   	dt_ciencia_medico is null
      and   	nr_atendimento = nr_atendimento_p;

	end if;

	return qt_needs_approval_w;

end get_approval_items;

function get_approval_items(nm_usuario_p varchar2, cd_pessoa_fisica_p varchar2, cd_perfil_p number, nr_atendimento_p number) return number is

qt_needs_approval_w  pls_integer;

begin

	select	sum(qt_pendente)
	into	qt_needs_approval_w
	from
	(select	count(*) qt_pendente
	from    cpoe_material
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and     ((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or      (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'AX') = 'S'))
	union all
	select	count(*) qt_pendente
	from    cpoe_dieta
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'AX') = 'S'))
	union all
	select	count(*) qt_pendente
	from    cpoe_gasoterapia
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'AX') = 'S'))
	union all
	select	count(*) qt_pendente
	from    cpoe_procedimento
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'AX') = 'S'))
	union all
	select	count(*) qt_pendente
	from    cpoe_recomendacao
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'AX') = 'S'))
	union all
	select	count(*) qt_pendente
	from    cpoe_hemoterapia
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'AX') = 'S'))
	union all
	select	count(*) qt_pendente
	from    cpoe_dialise
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'AX') = 'S'))
	union all
	select	count(*) qt_pendente
	from    cpoe_anatomia_patologica
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'AX') = 'S')));

	return qt_needs_approval_w;

end get_approval_items;

function get_approval_details(nm_usuario_p varchar2) return t_alert_details_table pipelined is

nr_atendimento_w 	atendimento_paciente.nr_atendimento%type;
t_alert_details_w	t_alert_details;
cd_pessoa_fisica_w 	pessoa_fisica.cd_pessoa_fisica%type;
ie_medico_w 		varchar2(1);
cd_perfil_w			perfil.cd_perfil%type;

cursor cEncounter is
	select  nr_atendimento,
			obter_nome_paciente(nr_atendimento) as nm_paciente,
			cd_pessoa_fisica
	from	atendimento_paciente
	where   dt_alta is null
	and		((ie_medico_w = 'S' and (cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(nr_atendimento,'C') = cd_pessoa_fisica_w)));


cursor cApprovalItems is
	select  dt_inicio as dt_evento,
			obter_desc_material(cd_material) as descricao
	from    cpoe_material
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and     ((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or      (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'AX') = 'S'))
	union all
	select  dt_inicio as dt_evento,
			cpoe_obter_desc_dieta_simp(nr_sequencia) as descricao
	from    cpoe_dieta
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'AX') = 'S'))
	union all
	select  dt_inicio as dt_evento,
			obter_desc_expressao(290567) as descricao
	from    cpoe_gasoterapia
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'AX') = 'S'))
	union all
	select  dt_inicio as dt_evento,
			obter_desc_proc_interno(nr_seq_proc_interno) as descricao
	from    cpoe_procedimento
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'AX') = 'S'))
	union all
	select  dt_inicio as dt_evento,
			obter_desc_recomendacao(cd_recomendacao) as descricao
	from    cpoe_recomendacao
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'AX') = 'S'))
	union all
	select  dt_inicio as dt_evento,
			Obter_Desc_Hemocomponente(nr_seq_derivado) as descricao
	from    cpoe_hemoterapia
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'AX') = 'S'))
	union all
	select  dt_inicio as dt_evento,
			CPOE_Obter_desc_dialise(ie_tipo_dialise, nr_seq_protocolo,null,null,null,null,'N',null,null,null,null,null,null,null,null,null,null,null) as descricao
	from    cpoe_dialise
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'AX') = 'S'))
	union all
	select  dt_inicio as dt_evento,
			obter_desc_proc_interno(nr_seq_proc_interno) as descricao
	from    cpoe_anatomia_patologica
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao_aux is not null
	and     dt_liberacao is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'AX') = 'S'));

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	cd_perfil_w	:= obter_perfil_ativo;
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		nr_atendimento_w := c_encounter_w.nr_atendimento;

		for c_approval_item_w in cApprovalItems
		loop

			t_alert_details_w.nr_atendimento	:= nr_atendimento_w;
			t_alert_details_w.dt_evento 		:= c_approval_item_w.dt_evento;
			t_alert_details_w.nm_paciente 		:= c_encounter_w.nm_paciente;
			t_alert_details_w.ds_evento			:= c_approval_item_w.descricao;
			t_alert_details_w.ie_funcao			:= 'CPOE';

			pipe row (t_alert_details_w);

		end loop;

	end loop;

	return;

end get_approval_details;

function get_approval_list(nm_usuario_p varchar2) return t_alerts_table pipelined is

t_alert_w			t_alert;
cd_pessoa_fisica_w  varchar2(10);
cd_perfil_w			perfil.cd_perfil%type;
qt_items_w      	number := 0;
ie_medico_w      	varchar2(1);

cursor cEncounter is
	select  a.nr_atendimento,
			a.cd_pessoa_fisica
	from	atendimento_paciente a
	where   a.dt_alta is null
	and		((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w)));

begin

	if (nvl(get_visualiza_alertas(nm_usuario_p, 'ANA'), 'X') <> 'S') then
		return;
	end if;

	cd_perfil_w	:= obter_perfil_ativo;
	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		qt_items_w := get_approval_items(nm_usuario_p, cd_pessoa_fisica_w, cd_perfil_w, c_encounter_w.nr_atendimento);

		if (nvl(qt_items_w, 0) > 0) then
			t_alert_w.ds_titulo := wheb_mensagem_pck.get_texto(1166585,'NM_PACIENTE_P='||obter_nome_paciente(c_encounter_w.nr_atendimento)||';DS_TOTAL_P='||qt_items_w);
			pipe row (t_alert_w);
		end if;
	end loop;

	return;

end get_approval_list;

function get_verification_items(nm_usuario_p varchar2, cd_pessoa_fisica_p varchar2, cd_perfil_p number, nr_atendimento_p number) return number is

qt_verif_items_w	number := 0;

begin

	select	sum(qt_verif_item)
	into	qt_verif_items_w
	from
	(select	count(*) qt_verif_item
	from    cpoe_material
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao is not null
	and     ie_ordered_on_behalf = 'S'
	and     dt_ciencia_medico is null
	and     ((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'NO') = 'S'))
	union all
	select	count(*) qt_verif_item
	from    cpoe_procedimento
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao is not null
	and     ie_ordered_on_behalf = 'S'
	and     dt_ciencia_medico is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_p) or (cpoe_verifica_regra_aprovacao(cd_perfil_p, nm_usuario_p, 'NO') = 'S')));

	return qt_verif_items_w;

end get_verification_items;

function get_verification_list(nm_usuario_p varchar2) return t_alerts_table pipelined is

t_alert_w			t_alert;
cd_pessoa_fisica_w  varchar2(10);
qt_items_w      	number := 0;
ie_medico_w      	varchar2(1);
cd_perfil_w			perfil.cd_perfil%type;

cursor cEncounter is
	select  a.nr_atendimento,
			a.cd_pessoa_fisica
	from	atendimento_paciente a
	where   a.dt_alta is null
	and		((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w)));

begin

	if (nvl(get_visualiza_alertas(nm_usuario_p, 'ANV'), 'X') <> 'S') then
		return;
	end if;

	cd_perfil_w	:= obter_perfil_ativo;
	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		qt_items_w := get_verification_items(nm_usuario_p, cd_pessoa_fisica_w, cd_perfil_w, c_encounter_w.nr_atendimento);

		if (nvl(qt_items_w, 0) > 0) then
			t_alert_w.ds_titulo := wheb_mensagem_pck.get_texto(1166585,'NM_PACIENTE_P='||obter_nome_paciente(c_encounter_w.nr_atendimento)||';DS_TOTAL_P='||qt_items_w);
			pipe row (t_alert_w);
		end if;
	end loop;

return;

end get_verification_list;

function get_verification_details(nm_usuario_p varchar2) return t_alert_details_table pipelined is

nr_atendimento_w 	atendimento_paciente.nr_atendimento%type;
t_alert_details_w	t_alert_details;
cd_pessoa_fisica_w 	pessoa_fisica.cd_pessoa_fisica%type;
ie_medico_w 		varchar2(1);
cd_perfil_w			perfil.cd_perfil%type;

cursor cEncounter is
	select  nr_atendimento,
			obter_nome_paciente(nr_atendimento) as nm_paciente,
			cd_pessoa_fisica
	from	atendimento_paciente
	where   dt_alta is null
	and		((ie_medico_w = 'S' and (cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(nr_atendimento,'C') = cd_pessoa_fisica_w)));


cursor cVerificationItems is
	select  dt_inicio as dt_evento,
			obter_desc_material(cd_material) as descricao
	from    cpoe_material
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao is not null
	and     ie_ordered_on_behalf = 'S'
	and     dt_ciencia_medico is null
	and     ((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'NO') = 'S'))
	union all
	select  dt_inicio as dt_evento,
			obter_desc_proc_interno(nr_seq_proc_interno) as descricao
	from    cpoe_procedimento
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao is not null
	and     ie_ordered_on_behalf = 'S'
	and     dt_ciencia_medico is null
	and    	((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)) is null))
	and     ((cd_medico = cd_pessoa_fisica_w) or (cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'NO') = 'S'));

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	cd_perfil_w	:= obter_perfil_ativo;
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		nr_atendimento_w := c_encounter_w.nr_atendimento;

		for c_verification_item_w in cVerificationItems
		loop

			t_alert_details_w.nr_atendimento	:= nr_atendimento_w;
			t_alert_details_w.dt_evento 		:= c_verification_item_w.dt_evento;
			t_alert_details_w.nm_paciente 		:= c_encounter_w.nm_paciente;
			t_alert_details_w.ds_evento			:= c_verification_item_w.descricao;
			t_alert_details_w.ie_funcao			:= 'CPOE';

			pipe row (t_alert_details_w);

		end loop;

	end loop;

return;

end get_verification_details;

function get_cosign_items(nr_atendimento_p number, nm_usuario_p varchar2) return number is

cd_pessoa_fisica_w 	pessoa_fisica.cd_pessoa_fisica%type;
cd_perfil_w			perfil.cd_perfil%type;
qt_cosign_items_w	number;

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	cd_perfil_w	:= obter_perfil_ativo;

	select	count(*)
	into	qt_cosign_items_w
	from    cpoe_material
	where	nr_atendimento = nr_atendimento_p
	and     dt_liberacao is not null
	and     dt_ciencia_medico is null
	and     nvl(ie_prescritor_aux, 'N') = 'N'
	and     nvl(ie_intervencao_farm, 'N') = 'N'
	and     ie_exige_ciencia = 'L'
	and     ((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and     cd_medico is not null
	and     cd_medico <> cd_pessoa_fisica_w
	and    	cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'CI') = 'S';

	return qt_cosign_items_w;

end get_cosign_items;

function get_cosign_list(nm_usuario_p varchar2) return t_alerts_table pipelined is

t_alert_w			t_alert;
cd_pessoa_fisica_w  varchar2(10);
qt_items_w      	number := 0;
ie_medico_w      	varchar2(1);

cursor cEncounter is
	select  a.nr_atendimento,
			a.cd_pessoa_fisica
	from	atendimento_paciente a
	where   a.dt_alta is null
	and		((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w)));

begin

	if (nvl(get_visualiza_alertas(nm_usuario_p, 'ACO'), 'X') <> 'S') then
		return;
	end if;

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		qt_items_w := get_cosign_items(c_encounter_w.nr_atendimento, nm_usuario_p);

		if (nvl(qt_items_w, 0) > 0) then
			t_alert_w.ds_titulo := wheb_mensagem_pck.get_texto(1166585,'NM_PACIENTE_P='||obter_nome_paciente(c_encounter_w.nr_atendimento)||';DS_TOTAL_P='||qt_items_w);
			pipe row (t_alert_w);
		end if;
	end loop;

return;

end get_cosign_list;

function get_cosign_details(nm_usuario_p varchar2) return t_alert_details_table pipelined is

nr_atendimento_w 	atendimento_paciente.nr_atendimento%type;
t_alert_details_w	t_alert_details;
cd_pessoa_fisica_w 	pessoa_fisica.cd_pessoa_fisica%type;
ie_medico_w 		varchar2(1);
cd_perfil_w			perfil.cd_perfil%type;

cursor cEncounter is
	select  nr_atendimento,
			obter_nome_paciente(nr_atendimento) as nm_paciente,
			cd_pessoa_fisica
	from	atendimento_paciente
	where   dt_alta is null
	and		((ie_medico_w = 'S' and (cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(nr_atendimento,'C') = cd_pessoa_fisica_w)));


cursor cCosignItems is
	select  dt_inicio as dt_evento,
			obter_desc_material(cd_material) as descricao
	from    cpoe_material
	where	nr_atendimento = nr_atendimento_w
	and     dt_liberacao is not null
	and     dt_ciencia_medico is null
	and     nvl(ie_prescritor_aux, 'N') = 'N'
	and     nvl(ie_intervencao_farm, 'N') = 'N'
	and     ie_exige_ciencia = 'L'
	and     ((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and     cd_medico is not null
	and     cd_medico <> cd_pessoa_fisica_w
	and    	cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'CI') = 'S';

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	cd_perfil_w	:= obter_perfil_ativo;
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		nr_atendimento_w := c_encounter_w.nr_atendimento;

		for c_cosign_item_w in cCosignItems
		loop

			t_alert_details_w.nr_atendimento	:= nr_atendimento_w;
			t_alert_details_w.dt_evento 		:= c_cosign_item_w.dt_evento;
			t_alert_details_w.nm_paciente 		:= c_encounter_w.nm_paciente;
			t_alert_details_w.ds_evento			:= c_cosign_item_w.descricao;
			t_alert_details_w.ie_funcao			:= 'CPOE';

			pipe row (t_alert_details_w);

		end loop;

	end loop;

	return;

end get_cosign_details;

function get_sub_cosign_items(nr_atendimento_p number, nm_usuario_p varchar2) return number is

cd_pessoa_fisica_w 	pessoa_fisica.cd_pessoa_fisica%type;
cd_perfil_w			perfil.cd_perfil%type;
qt_cosign_items_w	number;

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	cd_perfil_w	:= obter_perfil_ativo;

	select  count(*)
	into	qt_cosign_items_w
	from   	cpoe_material
	where   nr_atendimento = nr_atendimento_p
	and     dt_liberacao is not null
	and     dt_ciencia_medico is null
	and     nvl(ie_prescritor_aux, 'N') = 'S'
	and     nvl(ie_intervencao_farm, 'N') = 'N'
	and     ie_exige_ciencia = 'V'
	and     ((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and     cd_medico is not null
	and     cd_medico <> cd_pessoa_fisica_w
	and    	cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'CA') = 'S';

	return qt_cosign_items_w;

end get_sub_cosign_items;

function get_sub_cosign_list(nm_usuario_p varchar2) return t_alerts_table pipelined is

t_alert_w			t_alert;
cd_pessoa_fisica_w  varchar2(10);
qt_items_w      	number := 0;
ie_medico_w      	varchar2(1);

cursor cEncounter is
	select  a.nr_atendimento,
			a.cd_pessoa_fisica
	from	atendimento_paciente a
	where   a.dt_alta is null
	and		((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w)));

begin

	if (nvl(get_visualiza_alertas(nm_usuario_p, 'AAC'), 'X') <> 'S') then
		return;
	end if;

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		qt_items_w := get_sub_cosign_items(c_encounter_w.nr_atendimento, nm_usuario_p);

		if (nvl(qt_items_w, 0) > 0) then
			t_alert_w.ds_titulo := wheb_mensagem_pck.get_texto(1166585,'NM_PACIENTE_P='||obter_nome_paciente(c_encounter_w.nr_atendimento)||';DS_TOTAL_P='||qt_items_w);
			pipe row (t_alert_w);
		end if;
	end loop;

return;

end get_sub_cosign_list;

function get_sub_cosign_details(nm_usuario_p varchar2) return t_alert_details_table pipelined is

nr_atendimento_w 	atendimento_paciente.nr_atendimento%type;
t_alert_details_w	t_alert_details;
cd_pessoa_fisica_w 	pessoa_fisica.cd_pessoa_fisica%type;
ie_medico_w 		varchar2(1);
cd_perfil_w			perfil.cd_perfil%type;

cursor cEncounter is
	select  nr_atendimento,
			obter_nome_paciente(nr_atendimento) as nm_paciente,
			cd_pessoa_fisica
	from	atendimento_paciente
	where   dt_alta is null
	and		((ie_medico_w = 'S' and (cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(nr_atendimento,'C') = cd_pessoa_fisica_w)));


cursor cSubmissionItems is
	select  dt_inicio as dt_evento,
			obter_desc_material(cd_material) as descricao
	from   	cpoe_material
	where   nr_atendimento = nr_atendimento_w
	and     dt_liberacao is not null
	and     dt_ciencia_medico is null
	and     nvl(ie_prescritor_aux, 'N') = 'S'
	and     nvl(ie_intervencao_farm, 'N') = 'N'
	and     ie_exige_ciencia = 'V'
	and     ((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or       (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and     cd_medico is not null
	and     cd_medico <> cd_pessoa_fisica_w
	and    	cpoe_verifica_regra_aprovacao(cd_perfil_w, nm_usuario_p, 'CA') = 'S';

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	cd_perfil_w	:= obter_perfil_ativo;
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		nr_atendimento_w := c_encounter_w.nr_atendimento;

		for c_submission_item_w in cSubmissionItems
		loop

			t_alert_details_w.nr_atendimento	:= nr_atendimento_w;
			t_alert_details_w.dt_evento 		:= c_submission_item_w.dt_evento;
			t_alert_details_w.nm_paciente 		:= c_encounter_w.nm_paciente;
			t_alert_details_w.ds_evento			:= c_submission_item_w.descricao;
			t_alert_details_w.ie_funcao			:= 'CPOE';

			pipe row (t_alert_details_w);

		end loop;

	end loop;

return;

end;

function get_adm_cosign_items(nr_atendimento_p number) return number is

qt_cosign_items_w	number;

begin

	select	count(*)
	into	qt_cosign_items_w
	from	prescr_mat_hor
    where   nr_prescricao in (select b.nr_prescricao from prescr_medica b where b.nr_atendimento = nr_atendimento_p)
    and     obter_status_prim_check(dt_primeira_checagem, dt_fim_horario, dt_suspensao, dt_recusa) = 'S'
    and     dt_suspensao is null
    and     dt_fim_horario is null
    and     dt_lib_horario is not null;

	return 	qt_cosign_items_w;

end get_adm_cosign_items;

function get_adm_cosign_list(nm_usuario_p varchar2) return t_alerts_table pipelined is

t_alert_w			t_alert;
cd_pessoa_fisica_w  varchar2(10);
qt_items_w      	number := 0;
ie_medico_w      	varchar2(1);

cursor cEncounter is
	select  a.nr_atendimento,
			a.cd_pessoa_fisica
	from	atendimento_paciente a
	where   a.dt_alta is null
	and		((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w)));

begin

	if (nvl(get_visualiza_alertas(nm_usuario_p, 'AAD'), 'X') <> 'S') then
		return;
	end if;

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		qt_items_w := get_adm_cosign_items(c_encounter_w.nr_atendimento);

		if (nvl(qt_items_w, 0) > 0) then
			t_alert_w.ds_titulo := wheb_mensagem_pck.get_texto(1166585,'NM_PACIENTE_P='||obter_nome_paciente(c_encounter_w.nr_atendimento)||';DS_TOTAL_P='||qt_items_w);
			pipe row (t_alert_w);
		end if;
	end loop;

return;

end get_adm_cosign_list;

function get_adm_cosign_details(nm_usuario_p varchar2) return t_alert_details_table pipelined is

nr_atendimento_w 	atendimento_paciente.nr_atendimento%type;
t_alert_details_w	t_alert_details;
cd_pessoa_fisica_w 	pessoa_fisica.cd_pessoa_fisica%type;
ie_medico_w 		varchar2(1);

cursor cEncounter is
	select  nr_atendimento,
			obter_nome_paciente(nr_atendimento) as nm_paciente,
			cd_pessoa_fisica
	from	atendimento_paciente
	where   dt_alta is null
	and		((ie_medico_w = 'S' and (cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(nr_atendimento,'C') = cd_pessoa_fisica_w)));


cursor cAdminItems is
	select  dt_horario as dt_evento,
			obter_desc_material(cd_material) as descricao
	from	prescr_mat_hor
    where   nr_prescricao in (select b.nr_prescricao from prescr_medica b where b.nr_atendimento = nr_atendimento_w)
    and     obter_status_prim_check(dt_primeira_checagem, dt_fim_horario, dt_suspensao, dt_recusa) = 'S'
    and     dt_suspensao is null
    and     dt_fim_horario is null
    and     dt_lib_horario is not null;

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		nr_atendimento_w := c_encounter_w.nr_atendimento;

		for c_administration_item_w in cAdminItems
		loop

			t_alert_details_w.nr_atendimento	:= nr_atendimento_w;
			t_alert_details_w.dt_evento 		:= c_administration_item_w.dt_evento;
			t_alert_details_w.nm_paciente 		:= c_encounter_w.nm_paciente;
			t_alert_details_w.ds_evento			:= c_administration_item_w.descricao;
			t_alert_details_w.ie_funcao			:= 'ADEP';

			pipe row (t_alert_details_w);

		end loop;

	end loop;

return;

end get_adm_cosign_details;

function get_overdue_items(	nm_usuario_p      		varchar2,
							cd_perfil_p        		number,
							cd_estabelecimento_p	number,
							nr_atendimento_p    	number) return number is

ie_etapas_sol_w     	varchar2(1);
qt_horas_aten_sol_w   	varchar2(255);
qt_minutos_w       		varchar2(255);
qt_minutos_disp_w     	varchar2(255);
ie_consiste_adm_ccg_w	varchar2(255);
qt_overdue_items_w    	number := 0;

begin

    if (nm_usuario_p is not null and cd_estabelecimento_p is not null) then

		obter_Param_Usuario(1113, 144, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_etapas_sol_w);
		obter_Param_Usuario(1113, 153, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, qt_horas_aten_sol_w);
		obter_Param_Usuario(1113, 198, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, qt_minutos_w);
		obter_Param_Usuario(1113, 200, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, qt_minutos_disp_w);
		obter_Param_Usuario(1113, 543, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_consiste_adm_ccg_w);

    end if;

	qt_overdue_items_w := Obter_qt_hor_pendente_prescr (
							cd_estabelecimento_p,
							null,
							cd_perfil_p,
							nr_atendimento_p,
							48,
							trunc(sysdate),
							trunc(sysdate) + 86399 / 86400,
							1,
							ie_etapas_sol_w,
							nvl(qt_horas_aten_sol_w,'30'),
							nvl(qt_minutos_w,'60'),
							qt_minutos_disp_w,
							ie_consiste_adm_ccg_w);

	return qt_overdue_items_w;

end get_overdue_items;

function get_overdue_list(nm_usuario_p varchar2) return t_alerts_table pipelined is

t_alert_w				t_alert;
cd_pessoa_fisica_w  	varchar2(10);
qt_items_w      		number := 0;
ie_medico_w      		varchar2(1);
cd_perfil_w				perfil.cd_perfil%type;
cd_estabelecimento_w 	estabelecimento.cd_estabelecimento%type;
cd_setor_atendimento_w 	setor_atendimento.cd_setor_atendimento%type;

cursor cEncounter is
	select  a.nr_atendimento,
			a.cd_pessoa_fisica
	from	atendimento_paciente a
	where   a.dt_alta is null
	and		((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w)));

begin

	if (nvl(get_visualiza_alertas(nm_usuario_p, 'AOI'), 'X') <> 'S') then
		return;
	end if;

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);
	cd_perfil_w	:= obter_perfil_ativo;
	cd_estabelecimento_w := obter_estabelecimento_ativo;

	for c_encounter_w in cEncounter
	loop

		cd_setor_atendimento_w := Obter_Setor_Atendimento(c_encounter_w.nr_atendimento);

		qt_items_w := get_overdue_items(nm_usuario_p, cd_perfil_w, cd_estabelecimento_w, c_encounter_w.nr_atendimento);

		if (nvl(qt_items_w, 0) > 0) then
			t_alert_w.ds_titulo := wheb_mensagem_pck.get_texto(1166585,'NM_PACIENTE_P='||obter_nome_paciente(c_encounter_w.nr_atendimento)||';DS_TOTAL_P='||qt_items_w);
			pipe row (t_alert_w);
		end if;
	end loop;

return;

end get_overdue_list;

function get_overdue_details(nm_usuario_p varchar2) return t_alert_details_table pipelined is

cd_perfil_w				perfil.cd_perfil%type;
cd_estabelecimento_w 	estabelecimento.cd_estabelecimento%type;
cd_setor_atendimento_w 	setor_atendimento.cd_setor_atendimento%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
cd_pessoa_fisica_w 		pessoa_fisica.cd_pessoa_fisica%type;
ie_medico_w 			varchar2(1);
t_alert_details_w		t_alert_details;

qt_hor_pendente_w		number(30) := 0;
dt_inicial_w			date;
dt_final_w				date;

ie_horarios_dietas_orais_w	varchar2(1);
ie_consistir_med_w			varchar2(1);
ie_consistir_dieta_w		varchar2(1);
ie_consistir_sup_w			varchar2(1);
ie_consistir_mat_w			varchar2(1);
ie_consistir_rec_w			varchar2(1);
ie_consistir_sae_w			varchar2(1);
ie_consistir_me_w			varchar2(1);
ie_consistir_nan_w			varchar2(1);
ie_consistir_sol_w			varchar2(1);
ie_consistir_ccg_w			varchar2(1);
ie_consistir_gas_w			varchar2(1);
ie_etapas_sol_w     		varchar2(1);
qt_horas_aten_sol_w   		varchar2(255);
qt_minutos_w       			varchar2(255);
qt_minutos_disp_w     		varchar2(255);
ie_consiste_adm_ccg_w		varchar2(255);
dt_entrada_w date;

cursor cEncounter is
	select  nr_atendimento,
			obter_nome_paciente(nr_atendimento) as nm_paciente,
			cd_pessoa_fisica
	from	atendimento_paciente
	where   dt_alta is null
	and		((ie_medico_w = 'S' and (cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(nr_atendimento,'C') = cd_pessoa_fisica_w)));

cursor cOverdueItems is
	select  dt_horario as dt_evento,
			ds_item as descricao
	from 	adep_pend_prev_v
	where	adep_obter_regra_inclusao(	adep_obter_tipo_regra_inc_item(ie_tipo_item),
										cd_estabelecimento_w,
										cd_setor_atendimento_w,
										cd_perfil_w,
										decode(ie_tipo_item,'R',null,somente_numero(cd_item)),
										decode(ie_tipo_item,'R',null,somente_numero(cd_item)),
										ie_origem_proced,
										nr_seq_proc_interno,
										cd_setor_prescr,
										null,
										null,
										null)	= 'S'
	and 	dt_inicio_prescr < sysdate and dt_validade_prescr > sysdate
	and 	dt_horario between dt_entrada_w and sysdate
	and		nr_atendimento = nr_atendimento_w
	and		ie_tipo_item = 'M'
	and		ie_dose_especial <> 'S'
	and		ie_consistir_med_w = 'S'
	union
	select  dt_horario as dt_evento,
			ds_item as descricao
	from 	adep_pend_prev_v
	where	adep_obter_regra_inclusao(	adep_obter_tipo_regra_inc_item(ie_tipo_item),
										cd_estabelecimento_w,
										cd_setor_atendimento_w,
										cd_perfil_w,
										decode(ie_tipo_item,'R',null,somente_numero(cd_item)),
										decode(ie_tipo_item,'R',null,somente_numero(cd_item)),
										ie_origem_proced,
										nr_seq_proc_interno,
										cd_setor_prescr,
										null,
										null,
										null)	= 'S'
	and 	dt_inicio_prescr > sysdate - 1 and dt_validade_prescr > sysdate
	and 	dt_horario between dt_entrada_w and sysdate
	and		nr_atendimento = nr_atendimento_w
	and		ie_tipo_item = 'M'
	and		ie_dose_especial = 'S'
	and 	ie_consistir_med_w = 'S'
	union
	select  dt_horario as dt_evento,
			ds_item as descricao
	from 	adep_pend_prev_v
	where	adep_obter_regra_inclusao(	adep_obter_tipo_regra_inc_item(ie_tipo_item),
										cd_estabelecimento_w,
										cd_setor_atendimento_w,
										cd_perfil_w,
										decode(ie_tipo_item,'R',null,somente_numero(cd_item)),
										decode(ie_tipo_item,'R',null,somente_numero(cd_item)),
										ie_origem_proced,
										nr_seq_proc_interno,
										cd_setor_prescr,
										null,
										null,
										null)	= 'S'
	and 	dt_horario between dt_inicial_w and sysdate
	and 	dt_validade_prescr > sysdate
	and		nr_atendimento = nr_atendimento_w
	and		ie_tipo_item in ('D', 'S', 'MAT', 'P', 'L', 'R', 'E', 'CIG', 'CCG', 'NAN', 'O')
	and 	((ie_tipo_item = 'D' and ie_consistir_dieta_w = 'S')
	or		(ie_tipo_item = 'S' and ie_consistir_sup_w = 'S' and ie_horarios_dietas_orais_w = 'S')
	or		(ie_tipo_item = 'MAT' and ie_consistir_mat_w = 'S')
	or		(ie_tipo_item in ('P', 'L') and ADEP_obter_se_consiste_item(ie_tipo_item, cd_perfil_w, cd_item, ie_origem_proced, nr_seq_proc_interno) = 'S')
	or		(ie_tipo_item = 'R' and ie_consistir_rec_w = 'S')
	or		(ie_tipo_item = 'E' and ie_consistir_sae_w = 'S')
	or		(ie_tipo_item = 'ME' and ie_consistir_me_w = 'S')
	or		(((ie_consiste_adm_ccg_w = 'S') or (ie_tipo_item <> 'CCG'))
	and 	ADEP_obter_se_consiste_item(ie_tipo_item, cd_perfil_w,cd_item, ie_origem_proced, nr_seq_proc_interno) = 'S')
	or		(ie_tipo_item = 'NAN' and ie_consistir_nan_w = 'S')
	or		(ie_tipo_item = 'O' and ie_consistir_gas_w = 'S'))
	union
	select  a.dt_prev_prox_etapa as dt_evento,
			ds_solucao as descricao
	from	adep_sol_prescr_v a,
			prescr_mat_hor b
	where   a.nr_prescricao = b.nr_prescricao
	and 	a.nr_seq_solucao = b.nr_seq_solucao
	and		b.dt_horario between dt_entrada_w and sysdate
	and 	a.dt_inicio_prescr between dt_entrada_w and sysdate
	and 	a.dt_validade_prescr > sysdate
	and		a.dt_liberacao is not null
	and		a.dt_susp_prescr is null
	and 	b.dt_suspensao is null
	and		a.ie_suspenso <> 'S'
	and		nvl(a.ie_acm,'N') <> 'S'
	and		nvl(a.ie_adep,'N') = 'S'
	and		a.ie_tipo_solucao = 1
	and		((a.ie_status_solucao = 'N') or
			((ie_etapas_sol_w = 'S') and
			(nvl(a.nr_etapas,0) > 1) and
			(a.ie_status_solucao in ('I', 'INT')) and
			((a.dt_prev_prox_etapa is not null) and (sysdate > (nvl(a.dt_prev_prox_etapa,sysdate) - nvl(qt_horas_aten_sol_w,'30') / 1440)))))
	and		a.nr_atendimento = nr_atendimento_w
	and 	ie_consistir_sol_w = 'S'
	union
	select  dt_prev_prox_etapa as dt_evento,
			ds_solucao as descricao
	from    adep_sol_prescr_v
	where   dt_susp_prescr is null
	and     ie_suspenso <> 'S'
	and     nvl(ie_acm,'N') <> 'S'
	and     ie_status_solucao <> 'T'
	and   	dt_status between dt_entrada_w and sysdate
	and 	dt_inicio_prescr between dt_entrada_w and sysdate
	and 	dt_validade_prescr > sysdate
	and     nr_atendimento = nr_atendimento_w
	and		Obter_se_consiste_min_pausa(nr_prescricao, nr_seq_solucao) = 'S'
	and     nvl(ie_adep,'N') = 'S'
	and     ie_tipo_solucao = 2
	and		nvl(Obter_ult_evento_SNE(nr_prescricao, nr_seq_solucao),0) not in (1,3)
	and     dt_liberacao is not null
	and 	ie_consistir_sup_w = 'S'
	union
	select  a.dt_retirada_prev as dt_evento,
			b.ds_dispositivo as descricao
	from	atend_pac_dispositivo a,
			dispositivo b
	where	a.nr_seq_dispositivo = b.nr_sequencia
	and		a.nr_atendimento = nr_atendimento_w
	and		a.dt_retirada is null
	and		a.dt_retirada_prev is not null
	and		sysdate > a.dt_retirada_prev - decode(nvl(qt_minutos_w,'60'), null, 0, nvl(qt_minutos_w,'60')/1440)
	union
	select  dt_horario as dt_evento,
			ds_item as descricao
	from	adep_glic_pend_v
	where	adep_obter_regra_inclusao(	adep_obter_tipo_regra_inc_item('G'),
										cd_estabelecimento_w,
										cd_setor_atendimento_w,
										cd_perfil_w,
										null,
										null,
										null,
										null,
										null,
										null,
										null,
										null) = 'S'
	and		dt_horario between dt_entrada_w and sysdate
	and 	dt_validade_prescr > sysdate
	and 	dt_inicio_prescr  between dt_entrada_w and sysdate
	and		nr_atendimento		= nr_atendimento_w
	union
	select  a.dt_controle as dt_evento,
			obter_desc_expressao(624689) as descricao
	from	atendimento_glicemia a,
			atend_glicemia c,
			prescr_medica b
	where	a.nr_atendimento 	= b.nr_atendimento
	and		c.nr_sequencia		= a.nr_seq_glicemia
	and		c.nr_atendimento	= a.nr_atendimento
	and		c.nr_atendimento	= b.nr_atendimento
	and		c.nr_prescricao		= b.nr_prescricao
	and 	dt_inicio_prescr between dt_entrada_w and sysdate
	and 	dt_validade_prescr > sysdate
	and		a.dt_suspensao is null
	and		((nvl(a.qt_ui_insulina_calc,0) > 0) or
			(nvl(a.qt_ui_insulina_adm,0) > 0) or
			(nvl(a.qt_ui_insulina_int_calc,0) > 0) or
			(nvl(a.qt_ui_insulina_int_adm,0) > 0) or
			(nvl(a.qt_glicose,0) > 0) or
			(nvl(a.qt_glicose_adm,0) > 0))
	and		nvl(ie_adm_confirmada,'N') <> 'S'
	and		nvl(ie_status_glic, 'P') = 'N'
	and		sysdate > a.dt_controle + nvl(qt_minutos_w,'60') / 1440
	and		b.nr_atendimento = nr_atendimento_w
	and		ie_consistir_ccg_w = 'S';

begin

	dt_inicial_w := sysdate - 2;
	dt_final_w	 := sysdate + 3;

	cd_perfil_w	:= obter_perfil_ativo;
	cd_estabelecimento_w := obter_estabelecimento_ativo;
	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	obter_Param_Usuario(1113, 144, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_etapas_sol_w);
	obter_Param_Usuario(1113, 148, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_horarios_dietas_orais_w);
	obter_Param_Usuario(1113, 153, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, qt_horas_aten_sol_w);
	obter_Param_Usuario(1113, 198, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, qt_minutos_w);
	obter_Param_Usuario(1113, 200, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, qt_minutos_disp_w);
	obter_Param_Usuario(1113, 543, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_consiste_adm_ccg_w);

	ie_consistir_med_w 		:= ADEP_obter_se_consiste_item('M', cd_perfil_w,null, null, null);
	ie_consistir_dieta_w	:= ADEP_obter_se_consiste_item('D', cd_perfil_w,null, null, null);
	ie_consistir_sup_w		:= ADEP_obter_se_consiste_item('S', cd_perfil_w,null, null, null);
	ie_consistir_mat_w		:= ADEP_obter_se_consiste_item('MAT', cd_perfil_w,null, null, null);
	ie_consistir_rec_w		:= ADEP_obter_se_consiste_item('R', cd_perfil_w,null, null, null);
	ie_consistir_sae_w		:= ADEP_obter_se_consiste_item('E', cd_perfil_w,null, null, null);
	ie_consistir_me_w		:= ADEP_obter_se_consiste_item('ME', cd_perfil_w,null, null, null);
	ie_consistir_nan_w		:= ADEP_obter_se_consiste_item('NAN', cd_perfil_w,null, null, null);
	ie_consistir_gas_w		:= ADEP_obter_se_consiste_item('O', cd_perfil_w,null, null, null);
	ie_consistir_sol_w 		:= ADEP_obter_se_consiste_item('SOL', cd_perfil_w, null,null, null);
	ie_consistir_ccg_w		:= ADEP_obter_se_consiste_item('CCG', cd_perfil_w,null,null, null);

	for c_encounter_w in cEncounter
	loop

		nr_atendimento_w := c_encounter_w.nr_atendimento;

		cd_setor_atendimento_w := Obter_Setor_Atendimento(nr_atendimento_w);

		select 	max(dt_entrada)
		into	dt_entrada_w
		from 	atendimento_paciente
		where 	nr_atendimento = nr_atendimento_w;

		for c_overdue_item_w in cOverdueItems
		loop

			t_alert_details_w.nr_atendimento	:= nr_atendimento_w;
			t_alert_details_w.dt_evento 		:= c_overdue_item_w.dt_evento;
			t_alert_details_w.nm_paciente 		:= c_encounter_w.nm_paciente;
			t_alert_details_w.ds_evento			:= c_overdue_item_w.descricao;
			t_alert_details_w.ie_funcao			:= 'ADEP';

			pipe row (t_alert_details_w);

		end loop;

	end loop;

	return;

end get_overdue_details;

function get_pending_cosign_adm (nr_atendimento_p number) return number is

qt_pending_cosign_w  number := 0;

begin

	select  count(*)
    into  	qt_pending_cosign_w
    from   	prescr_mat_hor
    where   nr_prescricao in (select b.nr_prescricao from prescr_medica b where b.nr_atendimento = nr_atendimento_p)
    and   	obter_status_prim_check(dt_primeira_checagem, dt_fim_horario, dt_suspensao, dt_recusa) = 'S'
    and    	dt_suspensao is null
    and   	dt_fim_horario is null
    and   	dt_lib_horario is not null;

	return qt_pending_cosign_w;

end get_pending_cosign_adm;


  function get_all_reminders return t_object_central_notices pipelined is
  t_objeto_row_w    t_central_notices;
  cd_pessoa_fisica_w  varchar2(10);
  qt_pendencias_w    number;
  ds_conteudo_w    varchar2(4000);
  ie_medico_w      varchar2(1);
  ie_notification_w  varchar2(1);
  ds_login_w      subject.ds_login%type;

  cursor usuarios_logados is
  select    distinct(a.ds_login)
  from    subject_activity_log b,
      subject a
  where    a.id = b.id_subject
  and      trunc(b.dt_creation) between sysdate - 1 and sysdate + 86399/86400
  and      trunc(b.dt_expiration) >= trunc(sysdate);

  cursor c01 is
  select  a.nr_atendimento
  from   atendimento_paciente a
  where    ((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, ds_login_w, 36) = 'S'))
  or      (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   a.dt_alta is null;

  begin

    verify_execution('get_all_reminders');

    for usuarios_logado_w in usuarios_logados
    loop
      ds_login_w      := '';
      ie_notification_w  := '';
      ds_login_w      := usuarios_logado_w.ds_login;
      cd_pessoa_fisica_w   := obter_codigo_usuario(usuarios_logado_w.ds_login);
      ie_medico_w     := obter_se_usuario_medico(usuarios_logado_w.ds_login);

      obter_param_usuario(0, 239, 0, usuarios_logado_w.ds_login, 0, ie_notification_w);

      if (ie_notification_w = 'X') then
        for c01_w in c01
        loop
          ds_conteudo_w := '';
          qt_pendencias_w  := get_overdue_items(usuarios_logado_w.ds_login, 0, 0, c01_w.nr_atendimento);
          if (qt_pendencias_w > 0 ) then
            ds_conteudo_w := wheb_mensagem_pck.get_texto(1119579,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
          end if;

          qt_pendencias_w := 0;
          qt_pendencias_w := get_approval_items(c01_w.nr_atendimento);
          if (qt_pendencias_w > 0) then
            if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
              ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119580,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
            else
              ds_conteudo_w := wheb_mensagem_pck.get_texto(1119580,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
            end if;
          end if;

          qt_pendencias_w := 0;
          qt_pendencias_w := get_pending_cosign_adm(c01_w.nr_atendimento);
          if (qt_pendencias_w > 0) then
            if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
              ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119582,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
            else
              ds_conteudo_w := wheb_mensagem_pck.get_texto(1119582,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
            end if;
          end if;

          qt_pendencias_w := 0;
          qt_pendencias_w := get_pending_verif_items(cd_pessoa_fisica_w, c01_w.nr_atendimento);
          if (qt_pendencias_w > 0) then
            if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
              ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119583,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
            else
              ds_conteudo_w := wheb_mensagem_pck.get_texto(1119583,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
            end if;
          end if;

          if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
            t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
            t_objeto_row_w.ds_conteudo       := ds_conteudo_w;
            t_objeto_row_w.dt_criacao       := sysdate;
            t_objeto_row_w.nm_usuarios_destino  := usuarios_logado_w.ds_login;
            pipe row (t_objeto_row_w);
          end if;
        end loop;
      end if;
    end loop;
    return;

  end get_all_reminders;

  function get_reminders_list(  nm_usuario_p varchar2,
                  cd_perfil_p number,
                  cd_estabelecimento_p number,
                  nr_atendimento_p number ) return t_object_central_notices pipelined is

  t_objeto_row_w    t_central_notices;
  cd_pessoa_fisica_w  varchar2(10);
  qt_pendencias_w    number;
  ds_conteudo_w    varchar2(4000);
  ie_medico_w      varchar2(1);
  nr_atendimento_w  atendimento_paciente.nr_atendimento%type;

  cursor c01 is
  select  a.nr_atendimento
  from   atendimento_paciente a
  where   a.nr_atendimento = nr_atendimento_w
  or    ((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, nm_usuario_p, 36) = 'S'))
  or      (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   a.dt_alta is null;

  begin

    nr_atendimento_w := 0;

    if (nr_atendimento_p is not null) then
      nr_atendimento_w := nr_atendimento_p;
    end if;

    cd_pessoa_fisica_w   := obter_codigo_usuario(nm_usuario_p);
    ie_medico_w     := obter_se_usuario_medico(nm_usuario_p);

    for c01_w in c01
    loop

      ds_conteudo_w := '';
      qt_pendencias_w  := get_overdue_items(nm_usuario_p, cd_perfil_p, cd_estabelecimento_p, c01_w.nr_atendimento);
      if (qt_pendencias_w > 0 ) then
        ds_conteudo_w := wheb_mensagem_pck.get_texto(1119579,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
      end if;

      qt_pendencias_w := 0;
      qt_pendencias_w := get_approval_items(c01_w.nr_atendimento);
      if (qt_pendencias_w > 0) then
        if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
          ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119580,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        else
          ds_conteudo_w := wheb_mensagem_pck.get_texto(1119580,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        end if;
      end if;

      qt_pendencias_w := 0;
      qt_pendencias_w := get_pending_cosign_adm(c01_w.nr_atendimento);
      if (qt_pendencias_w > 0) then
        if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
          ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119582,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        else
          ds_conteudo_w := wheb_mensagem_pck.get_texto(1119582,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        end if;
      end if;

      qt_pendencias_w := 0;
      qt_pendencias_w := get_pending_verif_items(cd_pessoa_fisica_w, c01_w.nr_atendimento);
      if (qt_pendencias_w > 0) then
        if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
          ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119583,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        else
          ds_conteudo_w := wheb_mensagem_pck.get_texto(1119583,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        end if;
      end if;

      if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
        t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
        t_objeto_row_w.ds_conteudo       := ds_conteudo_w;
        t_objeto_row_w.dt_criacao       := sysdate;
        t_objeto_row_w.nm_usuarios_destino  := nm_usuario_p;
        pipe row (t_objeto_row_w);
      end if;

    end loop;
    return;

  end get_reminders_list;

  function   get_enconunter_reminders( nr_atendimento_p     number ) return varchar2 is

  ds_conteudo_w    varchar2(4000);
  qt_pendencias_w    number;
  cd_pessoa_fisica_w  varchar2(10);

  begin
    if (nr_atendimento_p > 0) then
      cd_pessoa_fisica_w   := obter_codigo_usuario(wheb_usuario_pck.get_nm_usuario);
      qt_pendencias_w := 0;
      qt_pendencias_w  := get_overdue_items(wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_cd_estabelecimento, nr_atendimento_p);
      if (qt_pendencias_w > 0 ) then
        ds_conteudo_w := wheb_mensagem_pck.get_texto(1119579,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
      end if;

      qt_pendencias_w := 0;
      qt_pendencias_w := get_approval_items(nr_atendimento_p);
      if (qt_pendencias_w > 0) then
        if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
          ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119580,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        else
          ds_conteudo_w := wheb_mensagem_pck.get_texto(1119580,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        end if;
      end if;

      qt_pendencias_w := 0;
      qt_pendencias_w := get_pending_cosign_adm(nr_atendimento_p);
      if (qt_pendencias_w > 0) then
        if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
          ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119582,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        else
          ds_conteudo_w := wheb_mensagem_pck.get_texto(1119582,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        end if;
      end if;

      qt_pendencias_w := 0;
      qt_pendencias_w := get_pending_verif_items(cd_pessoa_fisica_w, nr_atendimento_p);
      if (qt_pendencias_w > 0) then
        if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
          ds_conteudo_w := ds_conteudo_w || chr(13) || wheb_mensagem_pck.get_texto(1119583,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        else
          ds_conteudo_w := wheb_mensagem_pck.get_texto(1119583,'DS_TOTAL_P='||'('||qt_pendencias_w||')');
        end if;
      end if;
    end if;

    return   ds_conteudo_w;

  end get_enconunter_reminders;

  function get_new_orders_items(nr_atendimento_p number) return number is

  qt_pending_ack_w  number := 0;

  begin

    select  sum(qt_pendente)
    into  qt_pending_ack_w
    from  (
        select  count(*) qt_pendente
        from    cpoe_material a
        left join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_mat_cpoe and a.nr_atendimento = b.nr_atendimento
        where  a.nr_atendimento = nr_atendimento_p
        and    a.dt_liberacao is not null
        and    ((decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
        or        (decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) is null))
        and    b.nm_user_ack is null
        union all
        select  count(*) qt_pendente
        from    cpoe_dieta a
        left join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_diet_cpoe and a.nr_atendimento = b.nr_atendimento
        where  a.nr_atendimento = nr_atendimento_p
        and    a.dt_liberacao is not null
        and    ((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
        or       (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
        and    b.nm_user_ack is null
        union all
        select  count(*) qt_pendente
        from  cpoe_recomendacao a
        left join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_rec_cpoe and a.nr_atendimento = b.nr_atendimento
        where  a.nr_atendimento = nr_atendimento_p
        and     a.dt_liberacao is not null
        and    ((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
        or     (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
        and    b.nm_user_ack is null
        union all
        select  count(*) qt_pendente
        from    cpoe_gasoterapia a
        left join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_gaso_cpoe and a.nr_atendimento = b.nr_atendimento
        where  a.nr_atendimento = nr_atendimento_p
        and     a.dt_liberacao is not null
        and    ((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
        or     (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
        and    b.nm_user_ack is null
        union all
        select  count(*) qt_pendente
        from    cpoe_procedimento a
        left join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_exam_cpoe and a.nr_atendimento = b.nr_atendimento
        where  a.nr_atendimento = nr_atendimento_p
        and     a.dt_liberacao is not null
        and    ((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
        or     (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
        and    b.nm_user_ack is null
        union all
        select  count(*) qt_pendente
        from    cpoe_dialise a
        left join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_dial_cpoe and a.nr_atendimento = b.nr_atendimento
        where  a.nr_atendimento = nr_atendimento_p
        and     a.dt_liberacao is not null
        and    ((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
        or     (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
        and    b.nm_user_ack is null);

    return qt_pending_ack_w;

  end get_new_orders_items;

function get_new_orders_details(nm_usuario_p varchar2) return t_alert_details_table pipelined is

qt_pending_ack_w  	number := 0;
cd_pessoa_fisica_w  varchar2(10);
ie_medico_w      	varchar2(1);
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
t_alert_details_w	t_alert_details;

cursor cEncounter is
	select  nr_atendimento,
			obter_nome_paciente(nr_atendimento) as nm_paciente,
			cd_pessoa_fisica
	from	atendimento_paciente
	where   dt_alta is null
	and		((ie_medico_w = 'S' and (cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(nr_atendimento,'C') = cd_pessoa_fisica_w)));

cursor cNewItems is
	select  dt_inicio as dt_evento,
			obter_desc_material(cd_material) as descricao
	from    cpoe_material a
	left 	join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_mat_cpoe and a.nr_atendimento = b.nr_atendimento
	where  	a.nr_atendimento = nr_atendimento_w
	and    	a.dt_liberacao is not null
	and    	((decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
	or       (decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) is null))
	and    b.nm_user_ack is null
	union all
	select  dt_inicio as dt_evento,
			cpoe_obter_desc_dieta_simp(a.nr_sequencia) as descricao
	from    cpoe_dieta a
	left 	join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_diet_cpoe and a.nr_atendimento = b.nr_atendimento
	where  	a.nr_atendimento = nr_atendimento_w
	and    	a.dt_liberacao is not null
	and    	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
	or       (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and    	b.nm_user_ack is null
	union all
	select  dt_inicio as dt_evento,
			obter_desc_recomendacao(cd_recomendacao) as descricao
	from  	cpoe_recomendacao a
	left 	join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_rec_cpoe and a.nr_atendimento = b.nr_atendimento
	where  	a.nr_atendimento = nr_atendimento_w
	and     a.dt_liberacao is not null
	and    	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
	or     	(decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and    	b.nm_user_ack is null
	union all
	select  dt_inicio as dt_evento,
			obter_desc_expressao(290567) as descricao
	from    cpoe_gasoterapia a
	left 	join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_gaso_cpoe and a.nr_atendimento = b.nr_atendimento
	where  	a.nr_atendimento = nr_atendimento_w
	and     a.dt_liberacao is not null
	and    	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
	or     	(decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and   	 b.nm_user_ack is null
	union all
	select  dt_inicio as dt_evento,
			cpoe_obter_desc_proc_Interno(a.nr_seq_proc_interno) as descricao
	from    cpoe_procedimento a
	left 	join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_exam_cpoe and a.nr_atendimento = b.nr_atendimento
	where  	a.nr_atendimento = nr_atendimento_w
	and     a.dt_liberacao is not null
	and    	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
	or     	(decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and    	b.nm_user_ack is null
	union all
	select  dt_inicio as dt_evento,
			CPOE_Obter_desc_dialise(a.ie_tipo_dialise, a.nr_seq_protocolo,null,null,null,null,'N',null,null,null,null,null,null,null,null,null,null,null) as descricao
	from    cpoe_dialise a
	left 	join cpoe_inf_adic b on a.nr_sequencia = b.nr_seq_dial_cpoe and a.nr_atendimento = b.nr_atendimento
	where  	a.nr_atendimento = nr_atendimento_w
	and     a.dt_liberacao is not null
	and    	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate)
	or     	(decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and    	b.nm_user_ack is null;

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		nr_atendimento_w := c_encounter_w.nr_atendimento;

		for c_newItem_w in cNewItems
		loop

			t_alert_details_w.nr_atendimento 	:= c_encounter_w.nr_atendimento;
			t_alert_details_w.dt_evento 		:= c_newItem_w.dt_evento;
			t_alert_details_w.nm_paciente 		:= c_encounter_w.nm_paciente;
			t_alert_details_w.ds_evento			:= c_newItem_w.descricao;
			t_alert_details_w.ie_funcao			:= 'ADEP';

			pipe row (t_alert_details_w);

		end loop;
	end loop;

	return;

end get_new_orders_details;

  function get_new_orders_message(nr_atendimento_p number) return varchar2 is

  qt_pending_ack_w  number := 0;
  ds_message_w  varchar2(100);

  begin
  if (nr_atendimento_p > 0) then
    qt_pending_ack_w:= get_new_orders_items(nr_atendimento_p);

    if (qt_pending_ack_w > 0) then
      ds_message_w:= wheb_mensagem_pck.get_texto(1119570,'DS_TOTAL_P='||'('||qt_pending_ack_w||')');
    end if;
  end if;

  return ds_message_w;

  end get_new_orders_message;

  function get_pending_verif_items(  	cd_pessoa_fisica_p  varchar2,
										nr_atendimento_p  number) return number is

  qt_pending_ver_w  number := 0;

  begin

    select  sum(qt_pendente)
    into  qt_pending_ver_w
    from  (
        select  count(*) qt_pendente
        from    cpoe_material
        where  nr_atendimento = nr_atendimento_p
        and   dt_liberacao_aux is not null
        and    dt_liberacao is null
        and    cd_medico = cd_pessoa_fisica_p
        union all
        select  count(*) qt_pendente
        from    cpoe_dieta
        where  nr_atendimento = nr_atendimento_p
        and   dt_liberacao_aux is not null
        and    dt_liberacao is null
        and    cd_medico = cd_pessoa_fisica_p
        union all
        select  count(*) qt_pendente
        from  cpoe_recomendacao
        where  nr_atendimento = nr_atendimento_p
        and   dt_liberacao_aux is not null
        and    dt_liberacao is null
        and    cd_medico = cd_pessoa_fisica_p
        union all
        select  count(*) qt_pendente
        from    cpoe_gasoterapia
        where  nr_atendimento = nr_atendimento_p
        and   dt_liberacao_aux is not null
        and    dt_liberacao is null
        and    cd_medico = cd_pessoa_fisica_p
        union all
        select  count(*) qt_pendente
        from    cpoe_procedimento
        where  nr_atendimento = nr_atendimento_p
        and   dt_liberacao_aux is not null
        and    dt_liberacao is null
        and    cd_medico = cd_pessoa_fisica_p
        union all
        select  count(*) qt_pendente
        from    cpoe_dialise
        where  nr_atendimento = nr_atendimento_p
        and   dt_liberacao_aux is not null
        and    dt_liberacao is null
        and    cd_medico = cd_pessoa_fisica_p);

    return qt_pending_ver_w;

  end get_pending_verif_items;

  function  get_mentor_alert(  cd_pessoa_fisica_p   varchar2,
                  nr_atendimento_p   number,
                  ie_severidade_p number) return varchar2 is

  ds_return_w                varchar2(4000) := null;
  hr_retroactive_pending_w   number(10)     := Wheb_assist_pck.obterParametroFuncao(355, 2);
  dt_retroactive_w           date           := null;


  cursor   content_mentor is
    select  a.nr_sequencia,
                substr((pep_obter_info_pend_mentor(a.nr_seq_pend_pac_acao,'R')),1,255) DS_REGRA,
                c.ds_acao,
                pkg_date_formaters.to_varchar(a.dt_atualizacao, 'shortDate', wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario) dt_atualizacao
    from  gqa_pend_pac_acao_dest a,
                gqa_pendencia_pac b,
                gqa_pend_pac_acao c,
        gqa_pendencia_regra d
    where  b.nr_sequencia          = c.nr_seq_pend_pac
        and    a.nr_seq_pend_pac_acao  = c.nr_sequencia
    and     d.nr_sequencia         = b.nr_seq_pend_regra
        and    c.dt_encerramento       is null
        and    a.dt_encerramento       is null
        and    b.cd_pessoa_fisica      = cd_pessoa_fisica_p
        and    ((dt_retroactive_w is null) or (a.dt_atualizacao_nrec >= dt_retroactive_w))
    and   nvl(d.ie_advisory_severity, 3) = ie_severidade_p
        union
       select  a.nr_sequencia,
                substr((pep_obter_info_pend_mentor(a.nr_seq_pend_pac_acao,'R')),1,255) DS_REGRA,
                c.ds_acao,
                pkg_date_formaters.to_varchar(a.dt_atualizacao, 'shortDate', wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario) dt_atualizacao
    from  gqa_pend_pac_acao_dest a,
                gqa_pendencia_pac b,
                gqa_pend_pac_acao c left join gqa_pendencia_regra d on c.nr_sequencia = d.nr_sequencia
    where  b.nr_sequencia          = c.nr_seq_pend_pac
        and    a.nr_seq_pend_pac_acao  = c.nr_sequencia
    and     d.nr_sequencia         = b.nr_seq_pend_regra
        and    c.dt_encerramento       is null
        and    a.dt_encerramento       is null
        and    b.nr_atendimento        = nr_atendimento_p
        and    ((dt_retroactive_w is null) or (a.dt_atualizacao_nrec >= dt_retroactive_w))
    and   nvl(d.ie_advisory_severity, 3) = ie_severidade_p
        order by 1;

  begin

    if (hr_retroactive_pending_w > 0) then
      dt_retroactive_w := sysdate - (hr_retroactive_pending_w/24);
    end if;

    for c_content_mentor in content_mentor loop
    begin
      if (ds_return_w is null) then
        ds_return_w   := substr(c_content_mentor.ds_acao, 1, 4000);
      else
        ds_return_w   := substr(ds_return_w || chr(10) || c_content_mentor.ds_acao, 1, 4000);
      end if;
    end;
    end loop;

    return ds_return_w;

  end get_mentor_alert;

  function  get_mentor_alert(  cd_pessoa_fisica_p   varchar2,
                  nr_atendimento_p   number) return varchar2 is

  ds_return_w                varchar2(4000) := null;
  hr_retroactive_pending_w   number(10)     := Wheb_assist_pck.obterParametroFuncao(355, 2);
  dt_retroactive_w           date           := null;


  cursor   content_mentor is
    select  a.nr_sequencia,
                substr((pep_obter_info_pend_mentor(a.nr_seq_pend_pac_acao,'R')),1,255) DS_REGRA,
                c.ds_acao,
                pkg_date_formaters.to_varchar(a.dt_atualizacao, 'shortDate', wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario) dt_atualizacao
    from  gqa_pend_pac_acao_dest a,
                gqa_pendencia_pac b,
                gqa_pend_pac_acao c
    where  b.nr_sequencia          = c.nr_seq_pend_pac
        and    a.nr_seq_pend_pac_acao  = c.nr_sequencia
        and    c.dt_encerramento       is null
        and    a.dt_encerramento       is null
        and    b.cd_pessoa_fisica      = cd_pessoa_fisica_p
        and    ((dt_retroactive_w is null) or (a.dt_atualizacao_nrec >= dt_retroactive_w))
        union
        select  a.nr_sequencia,
                substr((pep_obter_info_pend_mentor(a.nr_seq_pend_pac_acao,'R')),1,255) DS_REGRA,
                c.ds_acao,
                pkg_date_formaters.to_varchar(a.dt_atualizacao, 'shortDate', wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario) dt_atualizacao
    from  gqa_pend_pac_acao_dest a,
                gqa_pendencia_pac b,
                gqa_pend_pac_acao c
    where   b.nr_sequencia          = c.nr_seq_pend_pac
        and     a.nr_seq_pend_pac_acao  = c.nr_sequencia
        and     c.dt_encerramento       is null
        and     a.dt_encerramento       is null
        and     b.nr_atendimento        = nr_atendimento_p
        and     ((dt_retroactive_w is null) or (a.dt_atualizacao_nrec >= dt_retroactive_w))
        order by 1;

  begin

    if (hr_retroactive_pending_w > 0) then
      dt_retroactive_w := sysdate - (hr_retroactive_pending_w/24);
    end if;

    for c_content_mentor in content_mentor loop
    begin
      if (ds_return_w is null) then
        ds_return_w   := substr(c_content_mentor.dt_atualizacao|| ' - '|| c_content_mentor.DS_REGRA || ' - ' ||c_content_mentor.ds_acao,1,4000);
      else
        ds_return_w   := substr(ds_return_w ||chr(10)|| c_content_mentor.dt_atualizacao|| ' - '|| c_content_mentor.DS_REGRA || ' - ' ||c_content_mentor.ds_acao,1,4000);
      end if;
    end;
    end loop;

    return ds_return_w;

  end get_mentor_alert;

  function get_notifications( nr_atendimento_p   number,
                cd_pessoa_fisica_p   varchar2) return varchar2 is

  ds_return_w  varchar2(4000) := null;

  cursor notifications is
    select  ie_type_record,
        substr(Obter_Valor_Dominio(9634,ie_type_record),1,255) ds_type_record,
        count(*) as total
    from    integration_notifications
    where   nr_encounter = nr_atendimento_p
    and     nvl(ie_viewed,'N') = 'N'
    group by ie_type_record, substr(Obter_Valor_Dominio(9634,ie_type_record),1,255)
    union
    select  ie_type_record,
        substr(Obter_Valor_Dominio(9634,ie_type_record),1,255) ds_type_record,
        count(*) as total
    from    integration_notifications
    where   cd_person = cd_pessoa_fisica_p
    and     nvl(ie_viewed,'N') = 'N'
    group by ie_type_record, substr(Obter_Valor_Dominio(9634,ie_type_record),1,255);

  begin

    for c_notification in notifications
    loop
    begin

      if (ds_return_w is null) then
        ds_return_w := wheb_mensagem_pck.get_texto(1119578,'DS_TIPO_NOTIFICACAO_P='||c_notification.ie_type_record||';DS_TOTAL_P='||'('|| c_notification.total||')');
      else
        ds_return_w := ds_return_w || chr(13) || wheb_mensagem_pck.get_texto(1119578,'DS_TIPO_NOTIFICACAO_P='||c_notification.ie_type_record||';DS_TOTAL_P='||'('|| c_notification.total||')');
      end if;

    end;
    end loop;

    return ds_return_w;

  end get_notifications;

  function get_all_alert return t_object_central_notices pipelined is

  t_objeto_row_w    t_central_notices;
  ie_notification_w  varchar2(1);
  ds_login_w      subject.ds_login%type;
  cd_pessoa_fisica_w  pessoa_fisica.cd_pessoa_fisica%type;
  ie_medico_w      varchar2(1);
  ds_conteudo_w    varchar2(4000);
  ds_prioridade_w   varchar2(50);

  cursor   usuarios_logados is
  select  distinct(a.ds_login)
  from    subject_activity_log b,
      subject a
  where   a.id = b.id_subject
  and     trunc(b.dt_creation) between sysdate - 1 and sysdate + 86399/86400
  and     trunc(b.dt_expiration) >= trunc(sysdate);

  cursor c01 is
  select  a.nr_atendimento,
      a.cd_pessoa_fisica
  from   atendimento_paciente a
  where   ((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, ds_login_w, 36) = 'S'))
  or      (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   a.dt_alta is null;

  begin
    verify_execution('get_all_alert');

    for usuarios_logado_w in usuarios_logados
    loop

      ie_notification_w  := '';
      ds_login_w      := usuarios_logado_w.ds_login;
      cd_pessoa_fisica_w   := obter_codigo_usuario(ds_login_w);
      ie_medico_w     := obter_se_usuario_medico(ds_login_w);
      obter_param_usuario(0, 239, 0, usuarios_logado_w.ds_login, 0, ie_notification_w);

      if (ie_notification_w = 'X') then

        for i in 1 .. 3 loop

          if i = 1 then
            ds_prioridade_w  := 'High';
          elsif i   = 2 then
            ds_prioridade_w := 'Medium';
          elsif i   = 3 then
            ds_prioridade_w := 'Low';
          end if;

          ds_conteudo_w := '';

          for c01_w in c01
          loop
            ds_conteudo_w := substr(get_mentor_alert(c01_w.cd_pessoa_fisica, c01_w.nr_atendimento, i), 1, 4000);

            if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
              t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
              t_objeto_row_w.dt_criacao       := sysdate;
              t_objeto_row_w.nm_usuarios_destino  := ds_login_w;
              t_objeto_row_w.ds_conteudo      := ds_conteudo_w;
              t_objeto_row_w.nr_prioridade    := i;
              pipe row (t_objeto_row_w);
              ds_conteudo_w := '';
            end if;

          end loop;
        end loop;
      end if;
    end loop;
    return;

  end get_all_alert;

  function get_mentor_alert_list(  nm_usuario_p    varchar2,
                  nr_atendimento_p  number) return t_object_central_notices pipelined is

  t_objeto_row_w    t_central_notices;
  cd_pessoa_fisica_w  varchar2(10);
  ds_conteudo_w    varchar2(4000);
  ie_medico_w      varchar2(1);
  nr_atendimento_w  atendimento_paciente.nr_atendimento%type;

  cursor c01 is
  select  a.nr_atendimento,
      a.cd_pessoa_fisica
  from   atendimento_paciente a
  where   a.nr_atendimento = nr_atendimento_w
  or     ((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, nm_usuario_p, 36) = 'S'))
  or      (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   a.dt_alta is null;

  begin

    nr_atendimento_w := 0;

    if (nr_atendimento_p is not null) then
      nr_atendimento_w := nr_atendimento_p;
    end if;

    cd_pessoa_fisica_w   := obter_codigo_usuario(nm_usuario_p);
    ie_medico_w     := obter_se_usuario_medico(nm_usuario_p);

    for c01_w in c01
    loop

      ds_conteudo_w := '';
      ds_conteudo_w := get_mentor_alert(c01_w.cd_pessoa_fisica, c01_w.nr_atendimento);

      if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
        t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
        t_objeto_row_w.dt_criacao       := sysdate;
        t_objeto_row_w.nm_usuarios_destino  := nm_usuario_p;
        t_objeto_row_w.ds_conteudo      := ds_conteudo_w;
        pipe row (t_objeto_row_w);
      end if;

    end loop;
    return;

  end get_mentor_alert_list;

  function get_mentor_alert_list(  nm_usuario_p    varchar2,
                  nr_atendimento_p  number,
                  ie_prioridade_p    varchar2) return t_object_central_notices pipelined is

  t_objeto_row_w    t_central_notices;
  cd_pessoa_fisica_w  varchar2(10);
  nr_atendimento_w  atendimento_paciente.nr_atendimento%type;
  ds_conteudo_w    varchar2(4000);
  ie_medico_w      varchar2(1);
  ds_prioridade_w   varchar2(50);

  cursor c01 is
  select  a.nr_atendimento,
      a.cd_pessoa_fisica
  from   atendimento_paciente a
  where   a.nr_atendimento = nr_atendimento_w
  or    ((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, nm_usuario_p, 36) = 'S'))
  or      (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   a.dt_alta is null;

  begin

    nr_atendimento_w := 0;

    if (nr_atendimento_p is not null) then
      nr_atendimento_w := nr_atendimento_p;
    end if;

    cd_pessoa_fisica_w   := obter_codigo_usuario(nm_usuario_p);
    ie_medico_w     := obter_se_usuario_medico(nm_usuario_p);

    for i in 1.. 3 loop

      if i = 1 then
        ds_prioridade_w  := 'High';
      elsif i   = 3 then
        ds_prioridade_w := 'Medium';
      elsif i   = 3 then
        ds_prioridade_w := 'Low';
      end if;

      ds_conteudo_w := '';

      for c01_w in c01
      loop

        if (ds_conteudo_w is null) then
          ds_conteudo_w := get_mentor_alert(c01_w.cd_pessoa_fisica, c01_w.nr_atendimento, i);
        else
          ds_conteudo_w := ds_conteudo_w || chr(13) || get_mentor_alert(c01_w.cd_pessoa_fisica, c01_w.nr_atendimento, i);
        end if;

        if (nvl(ds_conteudo_w, 'XPTO') <> 'XPTO') then
        t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
        t_objeto_row_w.dt_criacao       := sysdate;
        t_objeto_row_w.nm_usuarios_destino  := nm_usuario_p;
        t_objeto_row_w.ds_conteudo      := ds_conteudo_w;
        t_objeto_row_w.nr_prioridade    := i;
        pipe row (t_objeto_row_w);
        ds_conteudo_w := '';
      end if;

      end loop;
    end loop;
    return;

  end get_mentor_alert_list;

  function  get_new_orders_list(  	nm_usuario_p    	varchar2,
									nr_atendimento_p  	number) return t_object_central_notices pipelined is

  t_objeto_row_w    t_central_notices;
  cd_pessoa_fisica_w  varchar2(10);
  nr_atendimento_w  atendimento_paciente.nr_atendimento%type;
  qt_items_w      number := 0;
  ds_conteudo_w    varchar2(4000);
  ie_medico_w      varchar2(1);

  cursor c01 is
  select  a.nr_atendimento,
      a.cd_pessoa_fisica
  from   atendimento_paciente a
  where   a.nr_atendimento = nr_atendimento_w
  or     ((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, nm_usuario_p, 36) = 'S'))
  or      (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   a.dt_alta is null;

  begin

    nr_atendimento_w := 0;

    if (nr_atendimento_p is not null) then
      nr_atendimento_w := nr_atendimento_p;
    end if;

    cd_pessoa_fisica_w	:= obter_codigo_usuario(nm_usuario_p);
    ie_medico_w	:= obter_se_usuario_medico(nm_usuario_p);

    for c01_w in c01
    loop

		qt_items_w := get_new_orders_items(c01_w.nr_atendimento);

		if (nvl(qt_items_w, 0) > 0) then
		t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
		t_objeto_row_w.dt_criacao       := sysdate;
		t_objeto_row_w.nm_usuarios_destino  := nm_usuario_p;
		t_objeto_row_w.ds_conteudo      := wheb_mensagem_pck.get_texto(1119570,'DS_TOTAL_P='||'('||qt_items_w||')');
		pipe row (t_objeto_row_w);
		end if;

    end loop;

   return;

  end get_new_orders_list;

function get_new_orders_list(nm_usuario_p varchar2) return t_alerts_table pipelined is

t_alert_w			t_alert;
cd_pessoa_fisica_w  varchar2(10);
nr_atendimento_w  	atendimento_paciente.nr_atendimento%type;
qt_items_w      	number := 0;
ds_conteudo_w    	varchar2(4000);
ie_medico_w      	varchar2(1);

cursor c01 is
	select  a.nr_atendimento,
			a.cd_pessoa_fisica
	from	atendimento_paciente a
	where   a.dt_alta is null
	and		((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w)));

begin

	if (nvl(get_visualiza_alertas(nm_usuario_p, 'ANO'), 'X') <> 'S') then
		return;
	end if;

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c01_w in c01
	loop

		qt_items_w := get_new_orders_items(c01_w.nr_atendimento);

		if (nvl(qt_items_w, 0) > 0) then
			t_alert_w.ds_titulo := wheb_mensagem_pck.get_texto(1166585,'NM_PACIENTE_P='||obter_nome_paciente(c01_w.nr_atendimento)||';DS_TOTAL_P='||qt_items_w);
			pipe row (t_alert_w);
		end if;
	end loop;

return;

end get_new_orders_list;

function  get_all_new_orders return  t_object_central_notices pipelined is

t_objeto_row_w    	t_central_notices;
cd_pessoa_fisica_w  varchar2(10);
qt_items_w      	number := 0;
ds_conteudo_w    	varchar2(4000);
ie_medico_w      	varchar2(1);
ie_notification_w  	varchar2(2);
ds_login_w      	subject.ds_login%type;

cursor usuarios_logados is
	select 	distinct(a.ds_login)
	from    subject_activity_log b,
			subject a
	where   a.id = b.id_subject
	and     trunc(b.dt_creation) between sysdate - 1 and sysdate + 86399/86400
	and     trunc(b.dt_expiration) >= trunc(sysdate);

cursor c01 is
	select	a.nr_atendimento,
			a.cd_pessoa_fisica
  from 		atendimento_paciente a
  where   	((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, ds_login_w, 36) = 'S'))
  or      	 (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   	a.dt_alta is null;

begin

	verify_execution('get_all_new_orders');

	for usuarios_logado_w in usuarios_logados
	loop
		ds_login_w      := '';
		ie_notification_w  := '';
		ds_login_w      := usuarios_logado_w.ds_login;
		cd_pessoa_fisica_w   := obter_codigo_usuario(ds_login_w);
		ie_medico_w     := obter_se_usuario_medico(ds_login_w);
		obter_param_usuario(0, 239, 0, usuarios_logado_w.ds_login, 0, ie_notification_w);

		if (ie_notification_w = 'X') then

			for c01_w in c01
			loop

				qt_items_w := get_new_orders_items(c01_w.nr_atendimento);

				if (nvl(qt_items_w, 0) > 0) then
					t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
					t_objeto_row_w.dt_criacao       := sysdate;
					t_objeto_row_w.nm_usuarios_destino  := ds_login_w;
					t_objeto_row_w.ds_conteudo      := wheb_mensagem_pck.get_texto(1119570,'DS_TOTAL_P='||'('||qt_items_w||')');
					pipe row (t_objeto_row_w);
				end if;

			end loop;
		end if;
	end loop;

return;

end get_all_new_orders;

  function  get_new_notifications_list(  nm_usuario_p    varchar2,
                      nr_atendimento_p  number) return t_object_central_notices pipelined is

  t_objeto_row_w    t_central_notices;
  cd_pessoa_fisica_w  varchar2(10);
  ds_notifications_w  varchar2(4000);
  ie_medico_w      varchar2(1);
  nr_atendimento_w  atendimento_paciente.nr_atendimento%type;

  cursor c01 is
  select  a.nr_atendimento,
      a.cd_pessoa_fisica
  from   atendimento_paciente a
  where   a.nr_atendimento = nr_atendimento_w
  or     ((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, nm_usuario_p, 36) = 'S'))
  or      (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   a.dt_alta is null;

  begin

    nr_atendimento_w := 0;

    if (nr_atendimento_p is not null) then
      nr_atendimento_w := nr_atendimento_p;
    end if;

    cd_pessoa_fisica_w   := obter_codigo_usuario(nm_usuario_p);
    ie_medico_w     := obter_se_usuario_medico(nm_usuario_p);

    for c01_w in c01
    loop

    ds_notifications_w := get_notifications(c01_w.nr_atendimento, c01_w.cd_pessoa_fisica);

    if (nvl(ds_notifications_w, 'XPTO') <> 'XPTO') then
      t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
      t_objeto_row_w.dt_criacao       := sysdate;
      t_objeto_row_w.nm_usuarios_destino  := nm_usuario_p;
      t_objeto_row_w.ds_conteudo      := ds_notifications_w;
      pipe row (t_objeto_row_w);
    end if;

    end loop;
    return;

  end get_new_notifications_list;

  function  get_all_new_notifications return t_object_central_notices pipelined is

  t_objeto_row_w    t_central_notices;
  cd_pessoa_fisica_w  varchar2(10);
  ds_notifications_w    varchar2(4000);
  ie_medico_w      varchar2(1);
  ie_notification_w  varchar2(2);
  ds_login_w      subject.ds_login%type;

  cursor usuarios_logados is
  select   distinct(a.ds_login)
  from    subject_activity_log b,
      subject a
  where   a.id = b.id_subject
  and     trunc(b.dt_creation) between sysdate - 1 and sysdate + 86399/86400
  and     trunc(b.dt_expiration) >= trunc(sysdate);

  cursor c01 is
  select  a.nr_atendimento,
      a.cd_pessoa_fisica
  from   atendimento_paciente a
  where   ((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w or obter_se_medico_assistente(a.nr_atendimento, cd_pessoa_fisica_w, ds_login_w, 36) = 'S'))
  or      (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w))
  and   a.dt_alta is null;

  begin

    verify_execution('get_all_new_notifications');

    for usuarios_logado_w in usuarios_logados
    loop
      ds_login_w      := '';
      ds_login_w      := usuarios_logado_w.ds_login;
      ie_notification_w  := '';
      cd_pessoa_fisica_w   := obter_codigo_usuario(ds_login_w);
      ie_medico_w     := obter_se_usuario_medico(ds_login_w);
      obter_param_usuario(0, 239, 0, usuarios_logado_w.ds_login, 0, ie_notification_w);

      if (ie_notification_w = 'X') then
        for c01_w in c01
        loop
          ds_notifications_w := '';
          ds_notifications_w := get_notifications(c01_w.nr_atendimento, c01_w.cd_pessoa_fisica);

          if (nvl(ds_notifications_w, 'XPTO') <> 'XPTO') then
            t_objeto_row_w.ds_titulo       := obter_nome_paciente(c01_w.nr_atendimento);
            t_objeto_row_w.dt_criacao       := sysdate;
            t_objeto_row_w.nm_usuarios_destino  := ds_login_w;
            t_objeto_row_w.ds_conteudo      := ds_notifications_w;
            pipe row (t_objeto_row_w);
          end if;

        end loop;
      end if;
    end loop;
    return;

  end get_all_new_notifications;

  function get_new_integration_not return t_object_central_notices pipelined is

    t_objeto_row_w    t_central_notices;

    cursor c01 is
    select
    distinct g.nr_encounter as nr_atendimento
    from integration_notifications g
    where g.ie_type_record = 'SV'
    and g.ds_mensagem = 'SVI'
    and g.ie_viewed = 'N'
    and g.dt_atualizacao >= (sysdate - 1)
    group by g.nr_encounter;

    begin

    for c01_w in c01
    loop
      for b01_w in (
      select ds_titulo from
      table(select new_icu_notifications_pck.integration_not_users(c01_w.nr_atendimento) from dual))
      loop
        t_objeto_row_w.ds_titulo            := obter_valor_dominio(9898,'SVI');
        t_objeto_row_w.dt_criacao           := integration_not_get_date(c01_w.nr_atendimento);
        t_objeto_row_w.nm_usuarios_destino  := b01_w.ds_titulo;
        t_objeto_row_w.ds_conteudo          := obter_desc_expressao(311730) || ': ' || obter_nome_paciente(c01_w.nr_atendimento) || ' - ' || obter_desc_expressao(326147) || c01_w.nr_atendimento;
        pipe row (t_objeto_row_w);
      end loop;
    end loop;
    return;

  end get_new_integration_not;

  function integration_not_users(nr_atendimento_p number) return t_object_users_integration_not pipelined is

    t_objeto_row_w  users_integration_not;
    string_retorno_w varchar2(2000) := '';
    last_date_notification date := integration_not_get_date(nr_atendimento_p);

    cursor c01 is
    select
    rownum num,
    nm_usuario
    from (
    select
    distinct u.nm_usuario
    from integration_notifications g
    inner join usuario u on u.cd_pessoa_fisica = g.cd_person
    where g.nr_encounter = nr_atendimento_p
    and g.ie_type_record = 'SV'
    and g.ds_mensagem = 'SVI'
    and g.ie_viewed = 'N'
    and g.dt_atualizacao = last_date_notification);

    total_w number := 0;
    num_page_w number := 0;
    page_max_w number := 0;
    page_last_w number := 0;

    begin

    select
    count(*) into total_w
    from integration_notifications g
    where g.nr_encounter = nr_atendimento_p
    and g.ie_type_record = 'SV'
    and g.ds_mensagem = 'SVI'
    and g.ie_viewed = 'N'
    and g.dt_atualizacao = last_date_notification;

    num_page_w := 100;
    total_w := ceil(total_w / num_page_w);

    <<loop_fim>> for i in 1..total_w loop
      page_max_w := i * num_page_w;
      page_last_w := (i-1) * num_page_w;

      for c01_w in c01
      loop
        if c01_w.num is not null and (c01_w.num > page_last_w and c01_w.num <= page_max_w) then
           string_retorno_w := string_retorno_w || ';' || c01_w.nm_usuario;
        end if;
      end loop;

      t_objeto_row_w.ds_titulo := string_retorno_w;
      pipe row (t_objeto_row_w);
      string_retorno_w := '';

    end loop;
    return;

  end integration_not_users;

  function integration_not_get_date(nr_atendimento_p number) return date is
    string_retorno_w date;
    begin

    select dt_atualizacao into string_retorno_w from (
    select
    g.dt_atualizacao
    from integration_notifications g
    where g.nr_encounter = nr_atendimento_p
    and g.ie_type_record = 'SV'
    and g.ds_mensagem = 'SVI'
    and g.ie_viewed = 'N'
    and g.dt_atualizacao >= (sysdate - 1)
    order by g.dt_atualizacao desc)
    where rownum = 1;

    return string_retorno_w;

  end integration_not_get_date;


  function get_orders_user(cd_pessoa_fisica_p number, nm_usuario_p varchar2) return t_object_attendances pipelined is

    t_objeto_row_w    t_attendances;

    cursor c01 is
      select  nr_atendimento
      from    atendimento_paciente a
      where   dt_alta is null
      and     ((obter_se_usuario_medico(nm_usuario_p) = 'S' and a.cd_medico_resp = cd_pessoa_fisica_p)
      or       (obter_se_usuario_medico(nm_usuario_p) = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_p));

    begin

    for c01_w in c01
    loop
      t_objeto_row_w.nr_atendimento      :=  c01_w.nr_atendimento;
      pipe row (t_objeto_row_w);
    end loop;
    return;

  end get_orders_user;

function get_need_scheduled_items(nr_atendimento_p number) return number is

qt_need_scheduled_w	number;

begin

select  count(*)
into	qt_need_scheduled_w
from 	cpoe_material
where 	nr_atendimento = nr_atendimento_p
and 	dt_liberacao is not null
and 	((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
or  	 (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
and		ds_horarios is null
and 	nvl(ie_se_necessario, 'N') = 'N'
and 	nvl(ie_acm, 'N') = 'N';

return qt_need_scheduled_w;

end get_need_scheduled_items;

function get_need_scheduled_list(nm_usuario_p varchar2) return t_alerts_table pipelined is

t_alert_w			t_alert;
cd_pessoa_fisica_w  varchar2(10);
qt_items_w      	number := 0;
ie_medico_w      	varchar2(1);
cd_perfil_w			perfil.cd_perfil%type;

cursor cEncounter is
	select  a.nr_atendimento,
			a.cd_pessoa_fisica
	from	atendimento_paciente a
	where   a.dt_alta is null
	and		((ie_medico_w = 'S' and (a.cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(a.nr_atendimento,'C') = cd_pessoa_fisica_w)));

begin

	if (nvl(get_visualiza_alertas(nm_usuario_p, 'ANS'), 'X') <> 'S') then
		return;
	end if;

	cd_perfil_w	:= obter_perfil_ativo;
	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		qt_items_w := get_need_scheduled_items(c_encounter_w.nr_atendimento);

		if (nvl(qt_items_w, 0) > 0) then
			t_alert_w.ds_titulo := wheb_mensagem_pck.get_texto(1166585,'NM_PACIENTE_P='||obter_nome_paciente(c_encounter_w.nr_atendimento)||';DS_TOTAL_P='||qt_items_w);
			pipe row (t_alert_w);
		end if;
	end loop;

return;

end get_need_scheduled_list;

function get_need_scheduled_details(nm_usuario_p varchar2) return t_alert_details_table pipelined is

cd_pessoa_fisica_w  varchar2(10);
ie_medico_w      	varchar2(1);
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
t_alert_details_w 	t_alert_details;

cursor cEncounter is
	select  nr_atendimento,
			obter_nome_paciente(nr_atendimento) as nm_paciente,
			cd_pessoa_fisica
	from	atendimento_paciente
	where   dt_alta is null
	and		((ie_medico_w = 'S' and (cd_medico_resp = cd_pessoa_fisica_w)
	or       (ie_medico_w = 'N' and obter_enfermeiro_resp(nr_atendimento,'C') = cd_pessoa_fisica_w)));

cursor cNeedScheduled is
	select  dt_inicio as dt_evento,
			obter_desc_material(cd_material) as descricao
	from 	cpoe_material
	where 	nr_atendimento = nr_atendimento_w
	and 	dt_liberacao is not null
    and 	((decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) >= sysdate)
	or  	 (decode(dt_lib_suspensao, null, nvl(dt_fim_cih, dt_fim), nvl(dt_suspensao, dt_fim)) is null))
	and 	ds_horarios is null
	and 	nvl(ie_se_necessario, 'N') = 'N'
	and 	nvl(ie_acm, 'N') = 'N';

begin

	cd_pessoa_fisica_w := obter_codigo_usuario(nm_usuario_p);
	ie_medico_w := obter_se_usuario_medico(nm_usuario_p);

	for c_encounter_w in cEncounter
	loop

		nr_atendimento_w := c_encounter_w.nr_atendimento;

		for c_need_schedule_item_w in cNeedScheduled
		loop

			t_alert_details_w.nr_atendimento	:= nr_atendimento_w;
			t_alert_details_w.dt_evento 		:= c_need_schedule_item_w.dt_evento;
			t_alert_details_w.nm_paciente 		:= c_encounter_w.nm_paciente;
			t_alert_details_w.ds_evento			:= c_need_schedule_item_w.descricao;
			t_alert_details_w.ie_funcao			:= 'ADEP';

			pipe row (t_alert_details_w);

		end loop;

	end loop;

return;

end get_need_scheduled_details;

function get_alerts_details(nm_usuario_p varchar2, ie_tipo_alerta_p varchar2) return t_alert_details_table pipelined is

t_alert_details_w 	t_alert_details;
ds_cursor_w 		varchar2 (255);
t_alert_cursor 		sys_refcursor;

begin

	if (ie_tipo_alerta_p = 'newOrders') then
		ds_cursor_w := 'select nr_atendimento, dt_evento, nm_paciente, ds_evento, ie_funcao from table(new_icu_notifications_pck.get_new_orders_details(:nm_usuario_p))';
	elsif (ie_tipo_alerta_p = 'needScheduled') then
		ds_cursor_w := 'select nr_atendimento, dt_evento, nm_paciente, ds_evento, ie_funcao from table(new_icu_notifications_pck.get_need_scheduled_details(:nm_usuario_p))';
	elsif (ie_tipo_alerta_p = 'needApproval') then
		ds_cursor_w := 'select nr_atendimento, dt_evento, nm_paciente, ds_evento, ie_funcao from table(new_icu_notifications_pck.get_approval_details(:nm_usuario_p))';
	elsif (ie_tipo_alerta_p = 'needVerification') then
		ds_cursor_w := 'select nr_atendimento, dt_evento, nm_paciente, ds_evento, ie_funcao from table(new_icu_notifications_pck.get_verification_details(:nm_usuario_p))';
	elsif (ie_tipo_alerta_p = 'needCosign') then
		ds_cursor_w := 'select nr_atendimento, dt_evento, nm_paciente, ds_evento, ie_funcao from table(new_icu_notifications_pck.get_cosign_details(:nm_usuario_p))';
	elsif (ie_tipo_alerta_p = 'needSubCosign') then
		ds_cursor_w := 'select nr_atendimento, dt_evento, nm_paciente, ds_evento, ie_funcao from table(new_icu_notifications_pck.get_sub_cosign_details(:nm_usuario_p))';
	elsif (ie_tipo_alerta_p = 'needAdmCosign') then
		ds_cursor_w := 'select nr_atendimento, dt_evento, nm_paciente, ds_evento, ie_funcao from table(new_icu_notifications_pck.get_adm_cosign_details(:nm_usuario_p))';
	elsif (ie_tipo_alerta_p = 'overdue') then
		ds_cursor_w := 'select nr_atendimento, dt_evento, nm_paciente, ds_evento, ie_funcao from table(new_icu_notifications_pck.get_overdue_details(:nm_usuario_p))';
	end if;

	open t_alert_cursor for ds_cursor_w using nm_usuario_p;
	loop

		fetch t_alert_cursor into t_alert_details_w;

		if (t_alert_details_w.nr_atendimento > 0) then

			pipe row(t_alert_details_w);

			t_alert_details_w.nr_atendimento := '';
			t_alert_details_w.dt_evento	:= '';
			t_alert_details_w.nm_paciente := '';
			t_alert_details_w.ds_evento	:= '';
			t_alert_details_w.ie_funcao := '';

		end if;

		exit when t_alert_cursor%notfound;

	end loop;
	close t_alert_cursor;

	return;

end get_alerts_details;

end  new_icu_notifications_pck;
/