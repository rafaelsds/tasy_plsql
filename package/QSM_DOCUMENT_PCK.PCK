create or replace package qsm_document_pck as

	-- Comum para 2 ou mais mensagens:
	type r_user_row is record(
		user_id			varchar2(32),
		fullName		varchar2(32));

	type t_user is table of r_user_row;

	function createUser(nr_seq_fila_p number) return t_user pipelined;
	function createUserUrl(nm_usuario_p varchar2) return t_user pipelined;
	procedure limpar_atributos_user(r_user_row_p in out r_user_row);

	-- createDocument
	type r_createDocument_header_row is record(
		admissionDate		varchar2(32),
		ds_case			varchar2(32),
		client_id		varchar2(32),
		department		varchar2(32),
		dischargeDate		varchar2(32),
		documentingOU		varchar2(32),
		employeeResponsible	varchar2(32),
		ds_module		varchar2(32),
		oiNumber		varchar2(32),
		patient			varchar2(32),
		patientDateOfBirth	varchar2(32),
		patientName		varchar2(32),
		patientSex		varchar2(32));

	type t_createDocument_header is table of r_createDocument_header_row;

	function createDocument_header(nr_seq_fila_p number) return t_createDocument_header pipelined;
	procedure createDocument_response(nr_sequencia_p in number, ds_xml_p in long);
	procedure limpar_atrib_createDoc_header(r_createDocument_header_row_p in out r_createDocument_header_row);

	-- changeDocument
	type r_changeDocument_header_row is record(
		admissionDate		varchar2(32),
		cancelled		varchar2(32),
		client_id		varchar2(32),
		department		varchar2(32),
		dischargeDate		varchar2(32),
		document		varchar2(32),
		documentingOU		varchar2(32),
		employeeResponsible	varchar2(32),
		oiNumber		varchar2(32),
		patientDateOfBirth	varchar2(32),
		patientName		varchar2(32),
		patientSex		varchar2(32),
		released		varchar2(32));

	type t_changeDocument_header is table of r_changeDocument_header_row;

	function changeDocument_header(nr_seq_fila_p number) return t_changeDocument_header pipelined;
	procedure changeDocument_response(nr_sequencia_p in number, ds_xml_p in long);
	procedure limpar_atrib_changeDoc_header(r_changeDocument_header_row_p in out r_changeDocument_header_row);

	-- getDocumentData
	type r_getDocumentData_row is record(
		client_id		varchar2(32),
		document		varchar2(32),
		changeLogRequested	varchar2(32),
		contentsRequested	varchar2(32),
		headerRequested		varchar2(32),
		statusRequested		varchar2(32));

	type t_getDocumentData is table of r_getDocumentData_row;

	function getDocumentData(nr_seq_fila_p number) return t_getDocumentData pipelined;
	procedure getDocumentData_response(nr_sequencia_p in number);
	procedure limpar_atrib_getDocData(r_getDocumentData_row_p in out r_getDocumentData_row);

	-- getDocumentDataAsFile
	type r_getDocumentDataAsFile_row is record(
		client_id		varchar2(32),
		document		varchar2(32),
		format			varchar2(32));

	type t_getDocumentDataAsFile is table of r_getDocumentDataAsFile_row;

	function getDocumentDataAsFile(nr_seq_fila_p number) return t_getDocumentDataAsFile pipelined;
	procedure getDocumentDataAsFile_response(nr_sequencia_p in number);
	procedure limpar_atrib_getDocDataAsFile(r_getDocumentDataAsFile_row_p in out r_getDocumentDataAsFile_row);

	-- getDepartments
	type r_getDepartments_row is record(
		client_id		varchar2(32));

	type t_getDepartments is table of r_getDepartments_row;

	function getDepartments(nr_seq_fila_p number) return t_getDepartments pipelined;
	procedure getDepartments_response(nr_sequencia_p in number);
	procedure limpar_atrib_getDepartments(r_getDepartments_row_p in out r_getDepartments_row);

	-- getDocumentList
	type r_getDocumentList_row is record(
		client_id		varchar2(32),
		case_id			varchar2(32),
		headerRequested		varchar2(32),
		includeCancelled	varchar2(32),
		statusRequested		varchar2(32));

	type t_getDocumentList is table of r_getDocumentList_row;

	function getDocumentList(nr_seq_fila_p number) return t_getDocumentList pipelined;
	procedure getDocumentList_response(nr_seq_fila_p in number);
	procedure limpar_atrib_getDocumentList(r_getDocumentList_row_p in out r_getDocumentList_row);

	-- getModuleStructure
	type r_getModuleStructure_row is record(
		client_id		varchar2(32),
		module_id		varchar2(32));

	type t_getModuleStructure is table of r_getModuleStructure_row;

	function getModuleStructure(nr_seq_fila_p number) return t_getModuleStructure pipelined;
	procedure getModuleStructure_response(nr_sequencia_p in number);
	procedure limpar_atrib_getModuleStruct(r_getModuleStructure_row_p in out r_getModuleStructure_row);

	-- getModuleProperties
	type r_getModuleProperties_row is record(
		client_id		varchar2(32),
		module_id		varchar2(32));

	type t_getModuleProperties is table of r_getModuleProperties_row;

	function getModuleProperties(nr_seq_fila_p number) return t_getModuleProperties pipelined;
	procedure getModuleProperties_response(nr_sequencia_p in number);
	procedure limpar_atrib_getModuleProp(r_getModuleProperties_row_p in out r_getModuleProperties_row);

	-- getModulesForCase
	type r_getModulesForCase_row is record(
		admissionDate		varchar2(32),
		admissionReason		varchar2(32),
		age			varchar2(32),
		ds_case			varchar2(32),
		client			varchar2(32),
		department		varchar2(32),
		dischargeDate		varchar2(32),
		dischargeReason		varchar2(32),
		patient			varchar2(32),
		patientDateOfBirth	varchar2(32),
		patientName		varchar2(32),
		patientSex		varchar2(32));

	type t_getModulesForCase is table of r_getModulesForCase_row;

	function getModulesForCase(nr_seq_fila_p number) return t_getModulesForCase pipelined;
	procedure getModulesForCase_response(nr_sequencia_p in number);
	procedure limpar_atrib_getModulesForCase(r_getModulesForCase_row_p in out r_getModulesForCase_row);

	-- reassignDocuments
	type r_reassignDocuments_row is record(
		client_id		varchar2(32),
		targetPatient		varchar2(32));

	type t_reassignDocuments is table of r_reassignDocuments_row;

	function reassignDocuments(nr_seq_fila_p number) return t_reassignDocuments pipelined;
	procedure reassignDocuments_response(nr_sequencia_p in number);
	procedure limpar_atrib_reassignDocuments(r_reassignDocuments_row_p in out r_reassignDocuments_row);

	-- prepareDocumentEditor
	type r_prepareDocumentEditor_row is record(
		client_id		varchar2(32),
		document_id		varchar2(32),
		mode_id			varchar2(32));

	type t_prepareDocumentEditor is table of r_prepareDocumentEditor_row;

	function prepareDocumentEditor(nr_seq_document_p number) return t_prepareDocumentEditor pipelined;
	procedure prepareDocumentEditor_response(nr_sequencia_p in number);
	procedure limpar_atrib_prepareDocEditor(r_prepareDocumentEditor_row_p in out r_prepareDocumentEditor_row);

end qsm_document_pck;
/

create or replace package body qsm_document_pck as

function createUser( nr_seq_fila_p number) return t_user pipelined is

cursor C01(nm_usuario_pc intpd_fila_transmissao.nm_usuario%type) is
    select	a.nm_usuario user_id,
			obter_dados_pf(b.cd_pessoa_fisica, 'PNC') fullName
	from	usuario a,
			pessoa_fisica b
	where	a.nm_usuario		= nm_usuario_pc
	and		a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

nm_usuario_w		intpd_fila_transmissao.nm_usuario%type;
r_user_row_w		r_user_row;

begin

select	a.nm_usuario
into	nm_usuario_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia		= nr_seq_fila_p;

for r_C01_w in C01(nm_usuario_w) loop
	begin
	limpar_atributos_user(r_user_row_w);

	r_user_row_w.user_id		:= r_C01_w.user_id;
	r_user_row_w.fullName		:= r_C01_w.fullName;

	pipe row(r_user_row_w);
	end;
end loop;

if(	r_user_row_w.user_id is null and 
	r_user_row_w.fullName is null) then
		r_user_row_w.user_id		:= nm_usuario_w;
		
		pipe row(r_user_row_w);
end if;

end createUser;

function createUserUrl(nm_usuario_p varchar2) return t_user pipelined is

cursor C01(nm_usuario_pc intpd_fila_transmissao.nm_usuario%type) is
    select	a.nm_usuario user_id,
			obter_dados_pf(b.cd_pessoa_fisica, 'PNC') fullName
	from	usuario a,
			pessoa_fisica b
	where	a.nm_usuario		= nm_usuario_pc
	and		a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

r_user_row_w		r_user_row;

begin

for r_C01_w in C01(nm_usuario_p) loop
	begin
	limpar_atributos_user(r_user_row_w);

	r_user_row_w.user_id		:= r_C01_w.user_id;
	r_user_row_w.fullName		:= r_C01_w.fullName;

	pipe row(r_user_row_w);
	end;
end loop;

if(	r_user_row_w.user_id is null and 
	r_user_row_w.fullName is null) then
		r_user_row_w.user_id		:= nm_usuario_p;
		
		pipe row(r_user_row_w);
end if;

end createUserUrl;

procedure limpar_atributos_user(r_user_row_p in out r_user_row) is
begin
	r_user_row_p.user_id		:= null;
	r_user_row_p.fullName		:= null;
end limpar_atributos_user;


-- createDocument
function createDocument_header(	nr_seq_fila_p number) return t_createDocument_header pipelined is

cursor C01(nr_atendimento_pc number) is
select	a.dt_entrada admissionDate,
	nvl(b.nr_episodio, b.nr_sequencia) ds_case,
	e.cd_estabelecimento client_id,
	obter_departamento_data(a.nr_atendimento) department,
	a.dt_alta_medico dischargeDate,
	'' documentingOU,
	a.cd_medico_resp employeeResponsible,
	'' ds_module,
	'' oiNumber,
	nvl(obter_pf_codigo_externo('ISH', pf.cd_pessoa_fisica),pf.cd_pessoa_fisica) patient,
	pf.dt_nascimento patientDateOfBirth,
	obter_dados_pf(pf.cd_pessoa_fisica, 'PNC') patientName,
	decode(pf.ie_sexo,'M','male','F','female','unknown') patientSex
from	atendimento_paciente a,
	episodio_paciente b,
	pessoa_fisica pf,
	estabelecimento e,
	pessoa_juridica pj
where	a.nr_atendimento	= nr_atendimento_pc
and	a.nr_seq_episodio	= b.nr_sequencia
and	a.cd_pessoa_fisica	= pf.cd_pessoa_fisica
and	a.cd_estabelecimento	= e.cd_estabelecimento
and	e.cd_cgc		= pj.cd_cgc;

ds_parametros_adic_w			intpd_fila_transmissao.ds_parametros_adic%type;
ie_conversao_w				intpd_eventos_sistema.ie_conversao%type;
nr_seq_regra_w				intpd_eventos_sistema.nr_seq_regra_conv%type;
r_createDocument_header_row_w		r_createDocument_header_row;
ds_param_list_w				dbms_sql.varchar2_table;

begin
select	nvl(b.ie_conversao,'I'),
	b.nr_seq_regra_conv,
	a.ds_parametros_adic
into	ie_conversao_w,
	nr_seq_regra_w,
	ds_parametros_adic_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia		= nr_seq_fila_p;

ds_param_list_w		:= obter_lista_string(ds_parametros_adic_w, ';');

for r_C01_w in C01(ds_param_list_w(2)) loop
	begin
	limpar_atrib_createDoc_header(r_createDocument_header_row_w);

	r_createDocument_header_row_w.admissionDate		:= to_char(r_C01_w.admissionDate,'yyyy-mm-dd');
	r_createDocument_header_row_w.ds_case			:= r_C01_w.ds_case;
	r_createDocument_header_row_w.client_id			:= intpd_conv('ESTABELECIMENTO', 'CD_ESTABELECIMENTO', r_C01_w.client_id, nr_seq_regra_w, ie_conversao_w, 'E');
	r_createDocument_header_row_w.department		:= intpd_conv('ATEND_PACIENTE_UNIDADE', 'CD_DEPARTAMENTO', r_C01_w.department, nr_seq_regra_w, ie_conversao_w, 'E');
	r_createDocument_header_row_w.dischargeDate		:= to_char(r_C01_w.dischargeDate,'yyyy-mm-dd');
	r_createDocument_header_row_w.documentingOU		:= r_C01_w.documentingOU;
	r_createDocument_header_row_w.employeeResponsible	:= r_C01_w.employeeResponsible;
	r_createDocument_header_row_w.ds_module			:= ds_param_list_w(1);
	r_createDocument_header_row_w.oiNumber			:= r_C01_w.oiNumber;
	r_createDocument_header_row_w.patient			:= r_C01_w.patient;
	r_createDocument_header_row_w.patientDateOfBirth	:= to_char(r_C01_w.patientDateOfBirth,'yyyy-mm-dd');
	r_createDocument_header_row_w.patientName		:= r_C01_w.patientName;
	r_createDocument_header_row_w.patientSex		:= r_C01_w.patientSex;

	pipe row(r_createDocument_header_row_w);
	end;
end loop;

end createDocument_header;

procedure createDocument_response( nr_sequencia_p in number,
									ds_xml_p in long) is

reg_integracao_p			gerar_int_padrao.reg_integracao;				   
nm_usuario_w				intpd_fila_transmissao.nm_usuario%type;
nr_seq_documento_w			intpd_fila_transmissao.nr_seq_documento%type;
ds_parametros_adic_w			intpd_fila_transmissao.ds_parametros_adic%type;
nr_seq_regra_w				intpd_eventos_sistema.nr_seq_regra_conv%type;
ie_conversao_w				intpd_eventos_sistema.ie_conversao%type;

nr_seq_episodio_pac_doc_w	episodio_pac_document.nr_sequencia%type;
ds_case_w					episodio_paciente.nr_sequencia%type;
nr_atendimento_w			atendimento_paciente.nr_atendimento%type;
ds_client_id_w				estabelecimento.cd_estabelecimento%type;
ds_department_w				number(5);
ds_patient_w				atendimento_paciente.cd_pessoa_fisica%type;
nr_seq_tipo_adm_fat_atd_w	atendimento_paciente.nr_seq_tipo_admissao_fat%type;

ds_params_w				dbms_sql.varchar2_table;

ds_document_w				varchar2(32);
ds_client_w				varchar2(32);

xml_w					xmltype;
ds_xml_w				clob;
ds_erro_w				varchar2(4000);

cursor C01(ds_xml_pc xmltype) is
	select	z.document
	from	xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'soapenv:Envelope/soapenv:Body' passing ds_xml_pc columns
			createDocumentResponse		xmltype		path	'doc:createDocumentResponse') x,
		xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc"),
			'doc:createDocumentResponse' passing x.createDocumentResponse columns
			document		xmltype		path	'doc:document') z;

cursor C02(ds_xml_pc xmltype) is
	select	z.messages
	from	xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'soapenv:Envelope/soapenv:Body' passing ds_xml_pc columns
			createDocumentResponse		xmltype		path	'doc:createDocumentResponse') x,
		xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc"),
			'doc:createDocumentResponse' passing x.createDocumentResponse columns
			messages		xmltype		path	'doc:messages') z;
			
cursor C03 is
	select	nvl(b.qt_tempo_normal, 0) qt_tempo_normal,
			nvl(b.nr_sequencia, 0) nr_seq_regra
	from 	wl_regra_worklist a,
			wl_regra_item b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.ie_situacao = 'A'
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
								from	wl_item x
								where	x.nr_sequencia = a.nr_seq_item
								and		x.cd_categoria = 'QA'
								and		x.ie_situacao = 'A');	

begin
begin
select	a.nr_seq_documento,
	ds_parametros_adic,
	nvl(b.ie_conversao,'I'),
	b.nr_seq_regra_conv,
	a.nm_usuario
into	nr_seq_documento_w,
	ds_parametros_adic_w,
	ie_conversao_w,
	nr_seq_regra_w,
	nm_usuario_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia		= nr_sequencia_p;

xml_w 		:= xmltype.createxml( qsm_messages_pck.qsm_remove_tech_descript(ds_xml_p) );

ds_params_w	:= obter_lista_string(ds_parametros_adic_w, ';');

for r_C01_w in C01(xml_w) loop
	begin

	begin
		select	ds_client,
			ds_document
		into	ds_client_w,
			ds_document_w
		from	xmltable(
						xmlnamespaces('http://www.mmm.com/his/qsmed/doc' as "doc"),
						'doc:document' passing r_C01_w.document columns
				ds_client	varchar2(32)	path	'@client',
				ds_document	varchar2(32)	path	'@document');
	exception
	when others then
		ds_client_w	:= null;
		ds_document_w	:= null;
	end;

	select	b.nr_sequencia ds_case,
		e.cd_estabelecimento client_id,
		obter_setor_atendimento(a.nr_atendimento) department,
		pf.cd_pessoa_fisica patient,
		a.nr_atendimento
	into	ds_case_w,
		ds_client_id_w,
		ds_department_w,
		ds_patient_w,
		nr_atendimento_w
	from	atendimento_paciente a,
		episodio_paciente b,
		pessoa_fisica pf,
		estabelecimento e,
		pessoa_juridica pj
	where	a.nr_atendimento	= ds_params_w(2)
	and	a.nr_seq_episodio	= b.nr_sequencia
	and	a.cd_pessoa_fisica	= pf.cd_pessoa_fisica
	and	a.cd_estabelecimento	= e.cd_estabelecimento
	and	e.cd_cgc		= pj.cd_cgc;

	select	episodio_pac_document_seq.nextval
	into	nr_seq_episodio_pac_doc_w
	from	dual;

	insert into episodio_pac_document (
			nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_episodio,
			ds_client_id, cd_pessoa_fisica, ds_depart_id,
			ds_module_id, ds_oper_id, ie_cancelled,
			ie_exported, ie_released, cd_setor_atendimento,
			nr_seq_doc_qas)
	values(
			nr_seq_episodio_pac_doc_w, sysdate, nm_usuario_w,
			sysdate, nm_usuario_w, ds_case_w,
			intpd_conv('ESTABELECIMENTO', 'CD_ESTABELECIMENTO', ds_client_id_w, nr_seq_regra_w, ie_conversao_w, 'E'), ds_patient_w,
			intpd_conv('ATEND_PACIENTE_UNIDADE', 'ATEND_PACIENTE_UNIDADE', ds_department_w, nr_seq_regra_w, ie_conversao_w, 'E'),
			ds_params_w(1), null, null,
			null, null, null,
			ds_document_w);
			
	select	max(nr_seq_tipo_admissao_fat)
	into	nr_seq_tipo_adm_fat_atd_w
	from	atendimento_paciente
	where	nr_atendimento = ds_params_w(2);
	
	for r_C03_w in C03 loop
		if(obter_se_regra_geracao(r_C03_w.nr_seq_regra,ds_case_w,nr_seq_tipo_adm_fat_atd_w) = 'S') then
			wl_gerar_finalizar_tarefa('QA', 'I', ds_params_w(2), ds_patient_w, nm_usuario_w, (sysdate+(r_C03_w.qt_tempo_normal/24)), 'S', null, null, nr_seq_episodio_pac_doc_w,
								  null, null, null, null, null, null, r_C03_w.nr_seq_regra, null, null, null, null, null, null, null, sysdate, ds_case_w);
			gerar_int_padrao.gravar_integracao('357', nr_seq_episodio_pac_doc_w, nm_usuario_w, reg_integracao_p, ds_case_w);
		end if;
	end loop;
	end;
end loop;

for r_C02_w in C02(xml_w) loop
	begin
	qsm_messages_pck.qsm_process_message( nr_sequencia_p, r_C02_w.messages, 'N' );
	end;
end loop;

update	intpd_fila_transmissao
set	ie_status		= 'S',
	ie_response_procedure	= 'S',
	dt_atualizacao		= sysdate,
	ds_xml_retorno		= ds_xml_p
where	nr_sequencia		= nr_sequencia_p;

commit;

exception
when others then
	begin
	ds_erro_w	:= substr(dbms_utility.format_error_backtrace || chr(13) || chr(10) || sqlerrm,1,2000);

	rollback;

	update	intpd_fila_transmissao
	set	ie_status		= 'E',
		ie_response_procedure	= 'S',
		ds_log			= ds_erro_w,
		dt_atualizacao		= sysdate,
		ds_xml_retorno		= ds_xml_p
	where	nr_sequencia		= nr_sequencia_p;

	commit;
	end;
end;
end createDocument_response;

procedure limpar_atrib_createDoc_header(r_createDocument_header_row_p in out r_createDocument_header_row) is
begin
	r_createDocument_header_row_p.admissionDate		:= null;
	r_createDocument_header_row_p.ds_case			:= null;
	r_createDocument_header_row_p.client_id			:= null;
	r_createDocument_header_row_p.department		:= null;
	r_createDocument_header_row_p.dischargeDate		:= null;
	r_createDocument_header_row_p.documentingOU		:= null;
	r_createDocument_header_row_p.employeeResponsible	:= null;
	r_createDocument_header_row_p.ds_module			:= null;
	r_createDocument_header_row_p.oiNumber			:= null;
	r_createDocument_header_row_p.patient			:= null;
	r_createDocument_header_row_p.patientDateOfBirth	:= null;
	r_createDocument_header_row_p.patientName		:= null;
	r_createDocument_header_row_p.patientSex		:= null;
end limpar_atrib_createDoc_header;


-- changeDocument
function changeDocument_header(	nr_seq_fila_p number) 
		return t_changeDocument_header pipelined is

cursor C01(nr_seq_episodio_pc number) is
	select	'' admissionDate,
		'' cancelled,
		a.cd_estabelecimento client_id,
		obter_departamento_data(a.nr_atendimento) department,
		a.dt_alta_medico dischargeDate,
		'' document,
		'' documentingOU,
		a.cd_medico_resp employeeResponsible,
		'' oiNumber,
		'' patientDateOfBirth,
		'' patientName,
		'' patientSex,
		'' released
	from	atendimento_paciente a,
		episodio_paciente b,
		pessoa_fisica pf
	where	a.nr_seq_episodio	= nr_seq_episodio_pc
	and	a.nr_seq_episodio	= b.nr_sequencia
	and	a.cd_pessoa_fisica	= pf.cd_pessoa_fisica;

nr_seq_documento_w			intpd_fila_transmissao.nr_seq_documento%type;
ds_parametros_adic_w			intpd_fila_transmissao.ds_parametros_adic%type;
ie_conversao_w				intpd_eventos_sistema.ie_conversao%type;
nr_seq_regra_w				intpd_eventos_sistema.nr_seq_regra_conv%type;
r_changeDocument_header_row_w		r_changeDocument_header_row;
ds_params_w				dbms_sql.varchar2_table;
nr_seq_episodio_w			episodio_pac_document.nr_seq_episodio%type;
nr_seq_doc_qas_w			episodio_pac_document.nr_seq_doc_qas%type;

begin
select	a.nr_seq_documento,
	nvl(b.ie_conversao,'I'),
	b.nr_seq_regra_conv,
	a.ds_parametros_adic
into	nr_seq_documento_w,
	ie_conversao_w,
	nr_seq_regra_w,
	ds_parametros_adic_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia		= nr_seq_fila_p;

ds_params_w	:= obter_lista_string(ds_parametros_adic_w, ';');

select	max(nr_seq_episodio),
		max(nr_seq_doc_qas)
into	nr_seq_episodio_w,
		nr_seq_doc_qas_w
from	episodio_pac_document
where	nr_sequencia = nr_seq_documento_w;

if(ds_params_w.count > 2 and ds_params_w(3) = 'S')then
	nr_seq_episodio_w := obter_episodio_atendimento(to_number(ds_params_w(4)));
end if;

for r_C01_w in C01(nr_seq_episodio_w) loop
	begin
	limpar_atrib_changeDoc_header(r_changeDocument_header_row_w);

	r_changeDocument_header_row_w.admissionDate		:= to_char(r_C01_w.admissionDate,'yyyy-mm-dd');
	r_changeDocument_header_row_w.cancelled			:= ds_params_w(1);
	r_changeDocument_header_row_w.client_id			:= intpd_conv('ESTABELECIMENTO', 'CD_ESTABELECIMENTO', r_C01_w.client_id, nr_seq_regra_w, ie_conversao_w, 'E');
	r_changeDocument_header_row_w.department		:= intpd_conv('ATEND_PACIENTE_UNIDADE', 'CD_DEPARTAMENTO', r_C01_w.department, nr_seq_regra_w, ie_conversao_w, 'E');
	r_changeDocument_header_row_w.dischargeDate		:= to_char(r_C01_w.dischargeDate,'yyyy-mm-dd');
	if(ds_params_w.count > 2 and ds_params_w(3) = 'S')then
		r_changeDocument_header_row_w.document			:= nr_seq_documento_w;
	else
		r_changeDocument_header_row_w.document			:= nr_seq_doc_qas_w;
	end if;
	r_changeDocument_header_row_w.documentingOU		:= r_C01_w.documentingOU;
	r_changeDocument_header_row_w.employeeResponsible	:= r_C01_w.employeeResponsible;
	r_changeDocument_header_row_w.oiNumber			:= r_C01_w.oiNumber;
	r_changeDocument_header_row_w.patientDateOfBirth	:= to_char(r_C01_w.patientDateOfBirth,'yyyy-mm-dd');
	r_changeDocument_header_row_w.patientName		:= r_C01_w.patientName;
	r_changeDocument_header_row_w.patientSex		:= r_C01_w.patientSex;
	r_changeDocument_header_row_w.released			:= ds_params_w(2);

	pipe row(r_changeDocument_header_row_w);
	end;
end loop;

end changeDocument_header;

procedure changeDocument_response( nr_sequencia_p in number,
				   ds_xml_p in long) is

xml_w			xmltype;
ds_erro_w		varchar2(4000);

cursor C01(ds_xml_pc xmltype) is
	select	z.messages
	from	xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'soapenv:Envelope/soapenv:Body' passing ds_xml_pc columns
			changeDocumentResponse		xmltype		path	'doc:changeDocumentResponse') x,
		xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'doc:changeDocumentResponse' passing x.changeDocumentResponse columns
			messages		xmltype		path	'doc:messages') z;

begin
begin
xml_w		:= xmltype.createxml( qsm_messages_pck.qsm_remove_tech_descript(ds_xml_p) );

for r_C01_w in C01(xml_w) loop
	begin
	qsm_messages_pck.qsm_process_message( nr_sequencia_p, r_C01_w.messages, 'N' );
	end;
end loop;

update	intpd_fila_transmissao
set	ie_status		= 'S',
	ie_response_procedure	= 'S',
	dt_atualizacao		= sysdate,
	ds_xml_retorno		= ds_xml_p
where	nr_sequencia		= nr_sequencia_p;

commit;

exception
when others then
	begin
	ds_erro_w	:= substr(dbms_utility.format_error_backtrace || chr(13) || chr(10) || sqlerrm,1,2000);

	rollback;

	update	intpd_fila_transmissao
	set	ie_status		= 'E',
		ie_response_procedure	= 'S',
		ds_log			= ds_erro_w,
		dt_atualizacao		= sysdate,
		ds_xml_retorno		= ds_xml_p
	where	nr_sequencia		= nr_sequencia_p;

	commit;
	end;
end;

end changeDocument_response;

procedure limpar_atrib_changeDoc_header(r_changeDocument_header_row_p in out r_changeDocument_header_row) is
begin
	r_changeDocument_header_row_p.admissionDate		:= null;
	r_changeDocument_header_row_p.cancelled			:= null;
	r_changeDocument_header_row_p.client_id			:= null;
	r_changeDocument_header_row_p.department		:= null;
	r_changeDocument_header_row_p.dischargeDate		:= null;
	r_changeDocument_header_row_p.document			:= null;
	r_changeDocument_header_row_p.documentingOU		:= null;
	r_changeDocument_header_row_p.employeeResponsible	:= null;
	r_changeDocument_header_row_p.oiNumber			:= null;
	r_changeDocument_header_row_p.patientDateOfBirth	:= null;
	r_changeDocument_header_row_p.patientName		:= null;
	r_changeDocument_header_row_p.patientSex		:= null;
	r_changeDocument_header_row_p.released			:= null;
end limpar_atrib_changeDoc_header;


-- getDocumentData
function getDocumentData( nr_seq_fila_p number)
			  return t_getDocumentData pipelined is
begin
null;
end getDocumentData;

procedure getDocumentData_response(nr_sequencia_p in number) is
begin
null;
end getDocumentData_response;

procedure limpar_atrib_getDocData(r_getDocumentData_row_p in out r_getDocumentData_row) is
begin
null;
end limpar_atrib_getDocData;


-- getDocumentDataAsFile
function getDocumentDataAsFile( nr_seq_fila_p number)
				return t_getDocumentDataAsFile pipelined is
begin
null;
end getDocumentDataAsFile;

procedure getDocumentDataAsFile_response(nr_sequencia_p in number) is
begin
null;
end getDocumentDataAsFile_response;

procedure limpar_atrib_getDocDataAsFile(r_getDocumentDataAsFile_row_p in out r_getDocumentDataAsFile_row) is
begin
null;
end limpar_atrib_getDocDataAsFile;


-- getDepartments
function getDepartments( nr_seq_fila_p number) return t_getDepartments pipelined is
begin
null;
end getDepartments;

procedure getDepartments_response(nr_sequencia_p in number) is
begin
null;
end getDepartments_response;

procedure limpar_atrib_getDepartments(r_getDepartments_row_p in out r_getDepartments_row) is
begin
null;
end limpar_atrib_getDepartments;


-- getDocumentList
function getDocumentList( nr_seq_fila_p number) return t_getDocumentList pipelined is

cursor C01(nr_atendimento_pc number) is
	select	a.cd_estabelecimento client_id,
		b.nr_sequencia case_id,
		'true' headerRequested,
		'true' includeCancelled,
		'true' statusRequested
	from	atendimento_paciente a,
		episodio_paciente b
	where	a.nr_atendimento	= nr_atendimento_pc
	and	a.nr_seq_episodio	= b.nr_sequencia;

nr_seq_documento_w			intpd_fila_transmissao.nr_seq_documento%type;
ie_conversao_w				intpd_eventos_sistema.ie_conversao%type;
nr_seq_sistema_w			intpd_eventos_sistema.nr_seq_sistema%type;
nr_seq_projeto_xml_w			intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_regra_w				intpd_eventos_sistema.nr_seq_regra_conv%type;
r_getDocumentList_row_w			r_getDocumentList_row;
nr_atendimento_w			atendimento_paciente.nr_atendimento%type;

begin
select	a.nr_seq_documento,
	nvl(b.ie_conversao,'I'),
	b.nr_seq_sistema,
	b.nr_seq_projeto_xml,
	b.nr_seq_regra_conv
into	nr_seq_documento_w,
	ie_conversao_w,
	nr_seq_sistema_w,
	nr_seq_projeto_xml_w,
	nr_seq_regra_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia		= nr_seq_fila_p;

for r_C01_w in C01(nr_seq_documento_w) loop
	begin
	limpar_atrib_getDocumentList(r_getDocumentList_row_w);

	r_getDocumentList_row_w.client_id		:= intpd_conv('ESTABELECIMENTO', 'CD_ESTABELECIMENTO', r_C01_w.client_id, nr_seq_regra_w, ie_conversao_w, 'E');
	r_getDocumentList_row_w.case_id			:= r_C01_w.case_id;
	r_getDocumentList_row_w.headerRequested		:= r_C01_w.headerRequested;
	r_getDocumentList_row_w.includeCancelled	:= r_C01_w.includeCancelled;
	r_getDocumentList_row_w.statusRequested		:= r_C01_w.statusRequested;

	pipe row(r_getDocumentList_row_w);
	end;
end loop;

end getDocumentList;

procedure getDocumentList_response(nr_seq_fila_p in number) is

reg_integracao_p			gerar_int_padrao.reg_integracao;
nm_usuario_w				intpd_fila_transmissao.nm_usuario%type;
nr_seq_documento_w			intpd_fila_transmissao.nr_seq_documento%type;

nr_atendimento_w			atendimento_paciente.nr_atendimento%type;

ds_client_w				varchar2(32);
ds_case_documents_w			varchar2(32);
ds_id_document_w			varchar2(32);
ds_header_w				xmltype;
ds_admissionDate_w 			varchar2(32);
ds_cancelled_w				varchar2(32);
ds_case_w				varchar2(32);
ds_department_w				varchar2(32);
ds_dischargeDate_w			varchar2(32);
ds_documentingOU_w			varchar2(32);
ds_employee_Responsible_w		varchar2(32);
ds_exported_w				varchar2(32);
ds_module_w				varchar2(32);
ds_oiNumber_w				varchar2(32);
ds_patient_w				varchar2(32);
ds_patient_birth_date_w			varchar2(32);
ds_patientName_w			varchar2(32);
ds_patientSex_w				varchar2(32);
ds_released_w				varchar2(32);
ds_role_w				varchar2(32);
ds_status_w				varchar2(32);


xml_w					xmltype;
ds_xml_w				clob;
ds_erro_w				varchar2(4000);

cursor C01 is
	select	ds_case,
            ds_client
	from	xmltable(
			xmlnamespaces(
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'soapenv:Envelope/soapenv:Body/getDocumentListResponse/caseDocuments' passing xml_w columns
            ds_case		varchar2(32)	path	'@case',
			ds_client	varchar2(32)	path	'@client');

cursor C02 is
	select	ds_id,
			ds_header,
            ds_status
	from	xmltable(
			xmlnamespaces(
            'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'soapenv:Envelope/soapenv:Body/getDocumentListResponse/caseDocuments/document' passing xml_w columns
			ds_id		varchar2(32)	path	'@id',
            ds_header	xmltype		path	'header',
			ds_status	xmltype		path	'status');

cursor C03 is
	select	ds_role,
            ds_status
	from	xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'soapenv:Envelope/soapenv:Body/getDocumentListResponse/caseDocuments/document/status' passing xml_w columns
	ds_role		varchar2(32)	path	'@role',
    ds_status	varchar2(32)	path	'@status');

begin
begin
select	replace(ds_xml_retorno,'xmlns="http://www.mmm.com/his/qsmed/doc"',''),
	nm_usuario,
	nr_seq_documento
into	ds_xml_w,
	nm_usuario_w,
	nr_seq_documento_w
from	intpd_fila_transmissao a
where	a.nr_sequencia	= nr_seq_fila_p;

xml_w	:= xmltype.createxml(ds_xml_w);
for r_C01_w in C01 loop
	begin
	for r_C02_w in C02 loop
		begin
		for r_C03_w in C03 loop
			begin
			if	((r_C03_w.ds_role = 'USE') and (r_C03_w.ds_status = 'REDUNDANT')) then

				select	max(nr_atendimento)
				into	nr_atendimento_w
				from	atendimento_paciente
				where	nr_seq_episodio		= r_C01_w.ds_case;
				gerar_int_padrao.set_executando_recebimento('N');
				qsm_gravar_integracao('132', r_C02_w.ds_id,nm_usuario_w, 'true;false;S;'||nr_seq_documento_w);
				gerar_int_padrao.set_executando_recebimento('S');
			end if;

			end;
		end loop;
		end;
	end loop;
	end;
end loop;

update	intpd_fila_transmissao
set	ie_status		= 'S',
	ie_response_procedure	= 'S',
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_fila_p;

commit;

exception
when others then
	begin
	ds_erro_w	:= substr(dbms_utility.format_error_backtrace || chr(13) || chr(10) || sqlerrm,1,2000);

	rollback;

	update	intpd_fila_transmissao
	set	ie_status		= 'E',
		ie_response_procedure	= 'S',
		ds_log			= ds_erro_w,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_fila_p;

	commit;
	end;
end;

end getDocumentList_response;

procedure limpar_atrib_getDocumentList(r_getDocumentList_row_p in out r_getDocumentList_row) is
begin
	r_getDocumentList_row_p.client_id		:= null;
	r_getDocumentList_row_p.case_id			:= null;
	r_getDocumentList_row_p.headerRequested		:= null;
	r_getDocumentList_row_p.includeCancelled	:= null;
	r_getDocumentList_row_p.statusRequested		:= null;
end limpar_atrib_getDocumentList;


-- getModuleStructure
function getModuleStructure( nr_seq_fila_p number)
			     return t_getModuleStructure pipelined is

begin
null;
end getModuleStructure;

procedure getModuleStructure_response(nr_sequencia_p in number) is
begin
null;
end getModuleStructure_response;

procedure limpar_atrib_getModuleStruct(r_getModuleStructure_row_p in out r_getModuleStructure_row) is
begin
null;
end limpar_atrib_getModuleStruct;


-- getModuleProperties
function getModuleProperties( nr_seq_fila_p number)
			      return t_getModuleProperties pipelined is
begin
null;
end getModuleProperties;

procedure getModuleProperties_response(nr_sequencia_p in number) is
begin
null;
end getModuleProperties_response;

procedure limpar_atrib_getModuleProp(r_getModuleProperties_row_p in out r_getModuleProperties_row) is
begin
null;
end limpar_atrib_getModuleProp;


-- getModulesForCase
function getModulesForCase( nr_seq_fila_p number)
			    return t_getModulesForCase pipelined is
begin
null;
end getModulesForCase;

procedure getModulesForCase_response(nr_sequencia_p in number) is

begin
null;
end getModulesForCase_response;

procedure limpar_atrib_getModulesForCase(r_getModulesForCase_row_p in out r_getModulesForCase_row) is
begin
null;
end limpar_atrib_getModulesForCase;


-- reassignDocuments
function reassignDocuments( nr_seq_fila_p number)
			    return t_reassignDocuments pipelined is

begin
null;
end reassignDocuments;

procedure reassignDocuments_response(nr_sequencia_p in number) is

begin
null;
end reassignDocuments_response;

procedure limpar_atrib_reassignDocuments(r_reassignDocuments_row_p in out r_reassignDocuments_row) is
begin
null;
end limpar_atrib_reassignDocuments;


-- prepareDocumentEditor
function prepareDocumentEditor( nr_seq_document_p number) return t_prepareDocumentEditor pipelined is

nr_seq_documento_w			intpd_fila_transmissao.nr_seq_documento%type;
r_prepareDocumentEditor_row_w		r_prepareDocumentEditor_row;

cursor C01(nr_seq_documento_pc  episodio_pac_document.nr_sequencia%type) is
	select	e.ds_client_id client_id,
		e.nr_seq_doc_qas document_id,
		'update' mode_id
	from	episodio_pac_document e
	where	e.nr_sequencia = nr_seq_documento_pc;

begin

for r_C01_w in C01(nr_seq_document_p) loop
	begin
	limpar_atrib_prepareDocEditor(r_prepareDocumentEditor_row_w);

	r_prepareDocumentEditor_row_w.client_id		:= r_C01_w.client_id;
	r_prepareDocumentEditor_row_w.document_id	:= r_C01_w.document_id;
	r_prepareDocumentEditor_row_w.mode_id		:= r_C01_w.mode_id;

	pipe row(r_prepareDocumentEditor_row_w);
	end;
end loop;

end prepareDocumentEditor;

procedure prepareDocumentEditor_response(nr_sequencia_p number) is

nr_seq_doc_origem_w		intpd_fila_transmissao.nr_seq_documento%type;

ds_url_w			varchar2(2000);
ds_url_schema_w			varchar2(2000);
ds_id_w				varchar2(32);
ds_value_w			varchar2(32);

xml_w				xmltype;
ds_xml_w			clob;
ds_erro_w			varchar2(4000);

cursor C01 is
	select	editorParameters
	from	xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'soapenv:Envelope/soapenv:Body' passing xml_w columns
			prepareDocumentEditorResponse	xmltype		path	'doc:prepareDocumentEditorResponse') x,
		xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'doc:prepareDocumentEditorResponse' passing x.prepareDocumentEditorResponse columns
			editorParameters	xmltype		path	'doc:editorParameters') z;

cursor C02(editorParameters_pc xmltype) is
	select	z.ds_id,
		z.ds_value
	from	xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc"),
			'doc:editorParameters' passing editorParameters_pc columns
			parameter	xmltype	path	'doc:parameter') x,
		xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc"),
			'doc:parameter' passing x.parameter columns
			ds_id		varchar2(32)	path	'@id',
			ds_value	varchar2(32)	path	'@value') z;

cursor C10 is
	select	messages
	from	xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'soapenv:Envelope/soapenv:Body' passing xml_w columns
			prepareDocumentEditorResponse	xmltype		path	'doc:prepareDocumentEditorResponse') x,
		xmltable(
			xmlnamespaces(
			'http://www.mmm.com/his/qsmed/doc' as "doc",
			'http://schemas.xmlsoap.org/soap/envelope/' as "soapenv"),
			'doc:prepareDocumentEditorResponse' passing x.prepareDocumentEditorResponse columns
			messages		xmltype		path	'doc:messages') z;

begin
begin

select	ds_xml_retorno,
	nr_seq_documento
into	ds_xml_w,
	nr_seq_doc_origem_w
from	intpd_fila_transmissao a
where	a.nr_sequencia	= nr_sequencia_p;

xml_w	:= xmltype.createxml( qsm_messages_pck.qsm_remove_tech_descript(ds_xml_w) );

for r_C01_w in C01 loop
	begin

	begin
		select	ds_url_schema
		into	ds_url_schema_w
		from	xmltable(
				xmlnamespaces(
				'http://www.mmm.com/his/qsmed/doc' as "doc"),
				'doc:editorParameters' passing r_C01_w.editorParameters columns
				ds_url_schema		varchar2(2000)	path	'@urlSchema');

	exception
	when others then
		ds_url_schema_w		:= null;
	end;

	ds_url_w := ds_url_schema_w;

	for r_C02_w in C02(r_C01_w.editorParameters) loop
		begin
			ds_url_w := replace(ds_url_w,'$'||r_C02_w.ds_id||'$', r_C02_w.ds_value||'@');
		end;
	end loop;

	update	episodio_pac_document
	set	ds_url		= ds_url_w
	where	nr_sequencia	= nr_seq_doc_origem_w;

	end;
end loop;

for r_C10_w in C10 loop
	begin
	qsm_messages_pck.qsm_process_message( nr_sequencia_p, r_C10_w.messages, 'N' );
	end;
end loop;

update	intpd_fila_transmissao
set	ie_status		= 'S',
	ie_response_procedure	= 'S',
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_sequencia_p;

commit;

exception
when others then
	begin
	ds_erro_w	:= substr(dbms_utility.format_error_backtrace || chr(13) || chr(10) || sqlerrm,1,2000);

	rollback;

	update	intpd_fila_transmissao
	set	ie_status		= 'E',
		ie_response_procedure	= 'S',
		ds_log			= ds_erro_w,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencia_p;

	commit;
	end;
end;

end prepareDocumentEditor_response;

procedure limpar_atrib_prepareDocEditor(r_prepareDocumentEditor_row_p in out r_prepareDocumentEditor_row) is
begin
	r_prepareDocumentEditor_row_p.client_id		:= null;
	r_prepareDocumentEditor_row_p.document_id	:= null;
	r_prepareDocumentEditor_row_p.mode_id		:= null;
end limpar_atrib_prepareDocEditor;

end qsm_document_pck;
/