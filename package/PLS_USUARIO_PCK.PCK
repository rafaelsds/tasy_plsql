create or replace package pls_usuario_pck as
	function	get_ie_exec_trigger_solic_pf return varchar2;
	procedure	set_ie_exec_trigger_solic_pf(ie_executar_p varchar2);
	----------------------------------------------------------------------------------------------------------------------------------------------------------------
	function	get_ie_lancar_mensagem_alt return varchar2;
	procedure	set_ie_lancar_mensagem_alt(ie_lanc_mensagem_p varchar2);
	----------------------------------------------------------------------------------------------------------------------------------------------------------------
	procedure	set_ie_commit(ie_commit_p	varchar2);
	function	get_ie_commit return varchar2;
	----------------------------------------------------------------------------------------------------------------------------------------------------------------
	procedure	set_ie_importacao_benef(ie_importacao_benef_p	varchar2);
	function	get_ie_importacao_benef return varchar2;

end pls_usuario_pck; 
/

create or replace
package body pls_usuario_pck as
  	
	ie_executar_trigger_w		varchar2(1) := 'S';
	ie_lancar_mensagem_alt_w	varchar2(10) := 'N';
	ie_commit_w			varchar2(10) := 'S';
	ie_importacao_benef_w		varchar2(10) := 'N';
	
	procedure	set_ie_exec_trigger_solic_pf(ie_executar_p varchar2) as
	begin
		ie_executar_trigger_w := ie_executar_p;
	end;

	function	get_ie_exec_trigger_solic_pf return varchar2 as
	begin
		return ie_executar_trigger_w;
	end;
	
	procedure	set_ie_lancar_mensagem_alt(ie_lanc_mensagem_p varchar2) as
	begin
		ie_lancar_mensagem_alt_w := ie_lanc_mensagem_p;
	end;

	function	get_ie_lancar_mensagem_alt return varchar2 as
	begin
		return ie_lancar_mensagem_alt_w;
	end;
	
	procedure set_ie_commit(ie_commit_p	varchar2) is
	begin
	
	ie_commit_w	:= ie_commit_p;
	
	end set_ie_commit;
	
	function get_ie_commit return varchar2 as
	begin
		return ie_commit_w;
	end;
	
	procedure set_ie_importacao_benef(ie_importacao_benef_p	varchar2) is
	begin
	
	ie_importacao_benef_w	:= ie_importacao_benef_p;
	
	end set_ie_importacao_benef;
	
	function get_ie_importacao_benef return varchar2 as
	begin
		return ie_importacao_benef_w;
	end;

end pls_usuario_pck;
/
