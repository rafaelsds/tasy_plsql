create or replace package San_Gerar_Codigo_ISBT as
	
procedure San_gravar_cod_exame_isbt (
	nr_seq_doacao_p	number);
  
function	san_obter_codGer_descri_ISBT(
	cd_test_esp_ger_isbt_p varchar2) return varchar2;
  
function san_obter_fenotipagem_concat(
  nr_seq_doacao_p number, ie_nt_p INTEGER default 0) return VARCHAR2;
  
function san_obter_vlpos_antigeno (
	ds_valor_p varchar2,  ds_antigeno1_p varchar2,
	ds_antigeno2_p varchar2) RETURN number;
	
procedure san_gravar_cod_isbt_fenot (
    nr_seq_doacao_p number);

end San_Gerar_Codigo_ISBT;
/

create or replace 
package body San_Gerar_Codigo_ISBT as

PROCEDURE San_gravar_cod_exame_isbt(
	nr_seq_doacao_p	number)

IS

nr_seq_exame_w      	SAN_EXAME_REALIZADO.NR_SEQ_EXAME%type;
qt_exames_w		number(10,0) := 0;
cd_ISBT_W		SAN_TEST_ESP_GER_COD_ISBT.CD_ISBT%type; 
nr_seq_lote_w 		SAN_EXAME_LOTE.NR_SEQ_DOACAO%type;
ie_possui_versao_isbt_w	varchar2(10);	


Begin

select 	max('S')
into 	ie_possui_versao_isbt_w
from	SAN_TEST_ESP_GER_VER_ISBT
where 	ie_situacao = 'A';

if 	(nr_seq_doacao_p is not null and ie_possui_versao_isbt_w = 'S') then 

	select 	max(nr_sequencia)
	into 	nr_seq_lote_w
	from 	SAN_EXAME_LOTE
	where 	nr_seq_doacao = nr_seq_doacao_p;
  
	if	(nr_seq_lote_w is not null) then
		select  count(*)
		into qt_exames_w
		from san_exame_realizado a, SAN_EXAME b 
		where 	a.NR_SEQ_EXAME_LOTE = nr_seq_lote_w
		and a.NR_SEQ_EXAME = b.NR_SEQUENCIA
		and a.ds_resultado is not null 
		and a.dt_liberacao is not null
		and exists(
			select 1 from 
			SAN_TEST_ESP_REG_GER_ISBT x
			where x.NR_SEQ_EXAME = a.NR_SEQ_EXAME
			and (x.DS_RESULTADO = a.DS_RESULTADO or x.DS_RESULTADO is null)
			and x.IE_SITUACAO = 'A'
			);
	
		if	(qt_exames_w > 0) then
			begin
			select 	a.cd_isbt
			into 	cd_ISBT_W
			from 	SAN_TEST_ESP_GER_COD_ISBT a,
				SAN_TEST_ESP_GER_VER_ISBT d
			where 	a.nr_seq_versao_test = d.nr_sequencia
			and 	a.ie_situacao = 'A'
			and 	d.ie_situacao = 'A'
			and 	a.nr_sequencia in ((
					select  x.NR_SEQ_COD_TEST_ESP 
					from 	san_exame_realizado b, 
						SAN_EXAME c, 
						SAN_TEST_ESP_REG_GER_ISBT x 
					where 	b.NR_SEQ_EXAME_LOTE = nr_seq_lote_w
					and 	b.NR_SEQ_EXAME = c.NR_SEQUENCIA
					and 	b.ds_resultado is not null 
					and 	b.dt_liberacao is not null
					and 	x.NR_SEQ_EXAME = b.NR_SEQ_EXAME
					and 	(x.DS_RESULTADO = b.DS_RESULTADO or x.DS_RESULTADO is null)
					and 	x.IE_SITUACAO = 'A'
					and 	(	select count(*) 
							from SAN_TEST_ESP_REG_GER_ISBT y
							where y.NR_SEQ_COD_TEST_ESP = a.nr_sequencia) = qt_exames_w
					group by NR_SEQ_COD_TEST_ESP  HAVING COUNT(*) = qt_exames_w));
			     exception when others then
			     	cd_ISBT_W := 'N0000'; /* Valor padrao quando nao localiza um codigo para os exames informados */
			     end;
					
		else
			select 	max(a.cd_isbt)
			into 	cd_ISBT_W
			from 	SAN_TEST_ESP_GER_COD_ISBT a,
				SAN_TEST_ESP_GER_VER_ISBT b
			where 	a.nr_seq_versao_test = b.nr_sequencia
			and 	a.ie_situacao = 'A'
			and 	b.ie_situacao = 'A'
			and not exists (select 1 
					from SAN_TEST_ESP_REG_GER_ISBT x
					where x.NR_SEQ_COD_TEST_ESP = a.nr_sequencia);
		end if;
		
		if 	(cd_ISBT_W is not null) then
			update SAN_DOACAO
			set cd_test_esp_ger_isbt = cd_ISBT_W
			where nr_sequencia = nr_seq_doacao_p;
				
			commit;
		end if;
	end if;
end if;

end;

function san_obter_codGer_descri_ISBT(
	cd_test_esp_ger_isbt_p varchar2) return varchar2 is

ds_interpretacao_w		varchar2(255);

begin
	if(cd_test_esp_ger_isbt_p is not null) then
		select substr(ds_interpretacao, 1, 255)
			into ds_interpretacao_w
		from SAN_TEST_ESP_GER_COD_ISBT
		where cd_isbt = cd_test_esp_ger_isbt_p;
	end if;

return ds_interpretacao_w;
end;

function san_obter_fenotipagem_concat (
    nr_seq_doacao_p NUMBER, ie_nt_p INTEGER default 0) RETURN VARCHAR2 IS

ds_retorno_w   VARCHAR2(255);
ds_d_w         VARCHAR2(7);
ds_c_w         VARCHAR2(7);
ds_e_w         VARCHAR2(7);
ds_c_min_w     VARCHAR2(7);
ds_e_min_w     VARCHAR2(7);
ds_f_min_w     VARCHAR2(7);
ds_cw_w        VARCHAR2(7);
ds_k_w         VARCHAR2(7);
ds_k_min_w     VARCHAR2(7);
ds_kpa_w       VARCHAR2(7);
ds_kpb_w       VARCHAR2(7);
ds_jsa_w       VARCHAR2(7);
ds_jsb_w       VARCHAR2(7);
ds_fya_w       VARCHAR2(7);
ds_fyb_w       VARCHAR2(7);
ds_jka_w       VARCHAR2(7);
ds_jkb_w       VARCHAR2(7);
ds_lea_w       VARCHAR2(7);
ds_leb_w       VARCHAR2(7);
ds_s_w         VARCHAR2(7);
ds_s_min_w     VARCHAR2(7);
ds_m_w         VARCHAR2(7);
ds_n_w         VARCHAR2(7);
ds_p1_w        VARCHAR2(7);
ds_lua_w       VARCHAR2(7);
ds_lub_w       VARCHAR2(7);
ds_dia_w       VARCHAR2(7);

cursor c01 is
	SELECT
		decode(ds_d, 1, 'D+', 0, 'D-', 2, 'D_T'),
		decode(ds_c, 1, 'C+', 0, 'C-', 2, 'C_T'),
		decode(ds_e, 1, 'E+', 0, 'E-', 2, 'E_T'),
		decode(ds_c_min, 1, 'c+', 0, 'c-', 2, 'c_T'),
		decode(ds_e_min, 1, 'e+', 0, 'e-', 2, 'e_T'),
		decode(ds_f_min, 1, 'f+', 0, 'f-', 2, 'f_T'),
		decode(ds_cw, 1, 'Cw+', 0, 'Cw-', 2, 'Cw_T'),
		decode(ds_k, 1, 'K+', 0, 'K-', 2, 'K_T'),
		decode(ds_k_min, 1, 'k+', 0, 'k-', 2, 'k_T'),
		decode(ds_kpa, 1, 'Kpa+', 0, 'Kpa-', 2, 'Kpa_T'),
		decode(ds_kpb, 1, 'Kpb+', 0, 'Kpb-', 2, 'Kpb_T'),
		decode(ds_jsa, 1, 'Jsa+', 0, 'Jsa-', 2, 'Jsa_T'),
		decode(ds_jsb, 1, 'Jsb+', 0, 'Jsb-', 2, 'Jsb_T'),
		decode(ds_fya, 1, 'Fya+', 0, 'Fya-', 2, 'Fya_T'),
		decode(ds_fyb, 1, 'Fyb+', 0, 'Fyb-', 2, 'Fyb_T'),
		decode(ds_jka, 1, 'Jka+', 0, 'Jka-', 2, 'Jka_T'),
		decode(ds_jkb, 1, 'Jkb+', 0, 'Jkb-', 2, 'Jkb_T'),
		decode(ds_lea, 1, 'Lea+', 0, 'Lea-', 2, 'Lea_T'),
		decode(ds_leb, 1, 'Leb+', 0, 'Leb-', 2, 'Leb_T'),
		decode(ds_s, 1, 'S+', 0, 'S-', 2, 'S_T'),
		decode(ds_s_min, 1, 's+', 0, 's-', 2, 's_T'),
		decode(ds_m, 1, 'M+', 0, 'M-', 2, 'M_T'),
		decode(ds_n, 1, 'N+', 0, 'N-', 2, 'N_T'),
		decode(ds_p1, 1, 'P1+', 0, 'P1-', 2, 'P1_T'),
		decode(ds_lua, 1, 'Lua+', 0, 'Lua-', 2, 'Lua_T'),
		decode(ds_lub, 1, 'Lub+', 0, 'Lub-', 2, 'Lub_T'),
		decode(ds_dia, 1, 'Dia+', 0, 'Dia-', 2, 'Dia_T')
	FROM
		san_result_fenotipagem
	WHERE
		(nr_seq_doacao = nr_seq_doacao_p or nr_seq_producao = nr_seq_doacao_p)
		AND dt_liberacao IS NOT NULL
		AND dt_inativacao IS NULL
		order by dt_liberacao desc;
BEGIN

ds_retorno_w := '';

IF ( nr_seq_doacao_p IS NOT NULL ) THEN

  open c01;
	loop
	fetch c01 into
		ds_d_w,
                ds_c_w,
                ds_e_w,
                ds_c_min_w,
                ds_e_min_w,
                ds_f_min_w,
                ds_cw_w,
                ds_k_w,
                ds_k_min_w,
                ds_kpa_w,
                ds_kpb_w,
                ds_jsa_w,
                ds_jsb_w,
                ds_fya_w,
                ds_fyb_w,
                ds_jka_w,
                ds_jkb_w,
                ds_lea_w,
                ds_leb_w,
                ds_s_w,
                ds_s_min_w,
                ds_m_w,
                ds_n_w,
                ds_p1_w,
                ds_lua_w,
                ds_lub_w,
                ds_dia_w;
	exit when c01%notfound;
  begin

        IF ( ds_d_w is not null ) THEN
		if((instr(ds_retorno_w, 'D+') = 0 and instr(ds_retorno_w, 'D-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'D_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_d_w <> 'D_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_d_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_d_w;
				end if;
			end if;
		end if;
        END IF;
	 
	 IF ( ds_c_w is not null ) THEN
		if((instr(ds_retorno_w, 'C+') = 0 and instr(ds_retorno_w, 'C-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'C_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_c_w <> 'C_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_c_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_c_w;
				end if;
			end if;
		end if;
        END IF;
        
	IF ( ds_c_min_w is not null ) THEN
		if((instr(ds_retorno_w, 'c+') = 0 and instr(ds_retorno_w, 'c-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'c_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_c_min_w <> 'c_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_c_min_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_c_min_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_e_w is not null ) THEN
		if((instr(ds_retorno_w, 'E+') = 0 and instr(ds_retorno_w, 'E-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'E_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_e_w <> 'E_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_e_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_e_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_e_min_w is not null ) THEN
		if((instr(ds_retorno_w, 'e+') = 0 and instr(ds_retorno_w, 'e-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'e_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_e_min_w <> 'e_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_e_min_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_e_min_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_f_min_w is not null ) THEN
		if((instr(ds_retorno_w, 'f+') = 0 and instr(ds_retorno_w, 'f-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'f_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_f_min_w <> 'f_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_f_min_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_f_min_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_cw_w is not null ) THEN
		if((instr(ds_retorno_w, 'Cw+') = 0 and instr(ds_retorno_w, 'Cw-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Cw_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_Cw_w <> 'Cw_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_cw_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_cw_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_k_w is not null ) THEN
		if((instr(ds_retorno_w, 'K+') = 0 and instr(ds_retorno_w, 'K-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'K_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_k_w <> 'K_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_k_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_k_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_k_min_w is not null ) THEN
		if((instr(ds_retorno_w, 'k+') = 0 and instr(ds_retorno_w, 'k-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'k_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_k_min_w <> 'k_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_k_min_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_k_min_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_kpa_w is not null ) THEN
		if((instr(ds_retorno_w, 'Kpa+') = 0 and instr(ds_retorno_w, 'Kpa-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Kpa_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_kpa_w <> 'Kpa_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_kpa_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_kpa_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_kpb_w is not null ) THEN
		if((instr(ds_retorno_w, 'Kpb+') = 0 and instr(ds_retorno_w, 'Kpb-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Kpb_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_kpb_w <> 'Kpb_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_kpb_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_kpb_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_jsa_w is not null ) THEN
		if((instr(ds_retorno_w, 'Jsa+') = 0 and instr(ds_retorno_w, 'Jsa-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Jsa_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_jsa_w <> 'Jsa_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_jsa_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_jsa_w;
				end if;	
			end if;
		end if;
  END IF;
	 
	IF ( ds_jsb_w is not null ) THEN
		if((instr(ds_retorno_w, 'Jsb+') = 0 and instr(ds_retorno_w, 'Jsb-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Jsb_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_jsb_w <> 'Jsb_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_jsb_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_jsb_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_fya_w is not null ) THEN
		if((instr(ds_retorno_w, 'Fya+') = 0 and instr(ds_retorno_w, 'Fya-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Fya_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_fya_w <> 'Fya_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_fya_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_fya_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_fyb_w is not null ) THEN
		if((instr(ds_retorno_w, 'Fyb+') = 0 and instr(ds_retorno_w, 'Fyb-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Fyb_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_fyb_w <> 'Fyb_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_fyb_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_fyb_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_jka_w is not null ) THEN
		if((instr(ds_retorno_w, 'Jka+') = 0 and instr(ds_retorno_w, 'Jka-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Jka_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_jka_w <> 'Jka_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_jka_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_jka_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_jkb_w is not null ) THEN
		if((instr(ds_retorno_w, 'Jkb+') = 0 and instr(ds_retorno_w, 'Jkb-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Jkb_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_jkb_w <> 'Jkb_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_jkb_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_jkb_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_lea_w is not null ) THEN
		if((instr(ds_retorno_w, 'Lea+') = 0 and instr(ds_retorno_w, 'Lea-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Lea_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_lea_w <> 'Lea_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_lea_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_lea_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_leb_w is not null ) THEN
		if((instr(ds_retorno_w, 'Leb+') = 0 and instr(ds_retorno_w, 'Leb-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Leb_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_leb_w <> 'Leb_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_leb_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_leb_w;
				end if;
			end if;
		end if;
	END IF;
	 
	IF ( ds_p1_w is not null ) THEN
		if((instr(ds_retorno_w, 'P1+') = 0 and instr(ds_retorno_w, 'P1-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'P1_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_p1_w <> 'P1_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_p1_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_p1_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_m_w is not null ) THEN
		if((instr(ds_retorno_w, 'M+') = 0 and instr(ds_retorno_w, 'M-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'M_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_m_w <> 'M_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_m_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_m_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_n_w is not null ) THEN
		if((instr(ds_retorno_w, 'N+') = 0 and instr(ds_retorno_w, 'N-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'N_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_n_w <> 'N_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_n_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_n_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_s_w is not null ) THEN
		if((instr(ds_retorno_w, 'S+') = 0 and instr(ds_retorno_w, 'S-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'S_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_s_w <> 'S_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_s_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_s_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_s_min_w is not null ) THEN
		if((instr(ds_retorno_w, 's+') = 0 and instr(ds_retorno_w, 's-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 's_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_s_min_w <> 's_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_s_min_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_s_min_w;
				end if;
			end if;
		end if;
	END IF;
	 
	IF ( ds_lua_w is not null ) THEN
		if((instr(ds_retorno_w, 'Lua+') = 0 and instr(ds_retorno_w, 'Lua-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Lua_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_lua_w <> 'Lua_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_lua_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_lua_w;
				end if;
			end if;
		end if;
        END IF;
	 
	IF ( ds_lub_w is not null ) THEN
		if((instr(ds_retorno_w, 'Lub+') = 0 and instr(ds_retorno_w, 'Lub-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Lub_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_lub_w <> 'Lub_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_lub_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_lub_w;
				end if;
			end if;
		end if;
	END IF;
	 
	IF ( ds_dia_w is not null ) THEN
		if((instr(ds_retorno_w, 'Dia+') = 0 and instr(ds_retorno_w, 'Dia-') = 0) or ds_retorno_w is null) then
			if(((ie_nt_p = 1 and instr(ds_retorno_w, 'Dia_T') = 0) or (ie_nt_p = 1 and ds_retorno_w is null)) or (ie_nt_p = 0 and  ds_dia_w <> 'Dia_T')) then
				if(ds_retorno_w is null) then
					ds_retorno_w := ds_dia_w;
				else
					ds_retorno_w := ds_retorno_w || ', ' || ds_dia_w;
				end if;
			end if;
		end if;
        END IF;
	
	end;
	end loop;
	close c01;

END IF;

RETURN ds_retorno_w;

END;

function san_obter_vlpos_antigeno(
	ds_valor_p varchar2,  ds_antigeno1_p varchar2,
	ds_antigeno2_p varchar2) RETURN number IS

ie_pos_neg_fen1_w     number(1);
ie_pos_neg_fen2_w     number(1);
nr_retorno_w          number(10) := 9;

begin

if(ds_valor_p is not null) then
	ie_pos_neg_fen1_w := 2;
	ie_pos_neg_fen2_w := 2;

	if(inStr(ds_valor_p, ds_antigeno1_p) <> 0 or inStr(ds_valor_p, ds_antigeno2_p) <> 0) then
		if(inStr(ds_valor_p, ds_antigeno1_p || '+') <> 0) then
			ie_pos_neg_fen1_w := 1;
		end if;

		if(inStr(ds_valor_p, ds_antigeno1_p || '-') <> 0) then
			ie_pos_neg_fen1_w := 0;
		end if;
    
		if(inStr(ds_valor_p, ds_antigeno2_p || '+') <> 0) then
			ie_pos_neg_fen2_w := 1;
		end if;

		if(inStr(ds_valor_p, ds_antigeno2_p || '-') <> 0) then
			ie_pos_neg_fen2_w := 0;
		end if;

		select max(vl_valor)
			into nr_retorno_w
		from SAN_TES_ESP_ANT_RES_ISBT
		where dt_inativacao is null
			and cd_result_prim_antig = decode(ie_pos_neg_fen1_w, 1, 'POS', 0, 'NEG', 2, 'NT')
			and cd_result_seg_antig = decode(ie_pos_neg_fen2_w, 1, 'POS', 0, 'NEG', 2, 'NT');
      
		if(nr_retorno_w is null or nr_retorno_w = 9) then
			nr_retorno_w := 0;
		end if;
	end if;
end if;

return nr_retorno_w;
END;

procedure san_gravar_cod_isbt_fenot (
    nr_seq_doacao_p NUMBER) IS

ds_fenotipagem_w	VARCHAR2(255);
nr_posicao_str_w       	number(10);
nr_seq_doacao_w     SAN_RESULT_FENOTIPAGEM.nr_seq_doacao%type;   
nr_seq_producao_w     SAN_RESULT_FENOTIPAGEM.nr_seq_producao%type;
ds_fenotipo_w          	VARCHAR2(15);
ds_posicao1_w          	VARCHAR2(155);
ds_posicao1_r_w         VARCHAR2(155);
ds_posicao2_w          	VARCHAR2(155);
ds_posicao3_w          	VARCHAR2(155);
ds_posicao4_w 		VARCHAR2(155);
ds_posicao5_w 		VARCHAR2(155);
ds_posicao6_w 		VARCHAR2(155);
ds_posicao7_w 		VARCHAR2(155);
ds_posicao8_w 		VARCHAR2(155);
ds_posicao9_w 		VARCHAR2(155);
ds_posicao10_w		VARCHAR2(155);
ds_posicao11_w		VARCHAR2(155);
ds_posicao12_w		VARCHAR2(155);
ds_posicao13_w		VARCHAR2(155);
ds_posicao14_w		VARCHAR2(155);
ds_posicao15_w		VARCHAR2(155);
ds_posicao16_w		VARCHAR2(155);
ds_posicao17_18_w	VARCHAR2(155);
ie_entrou_w         	VARCHAR(1);
ds_ant1_pos_1_w         VARCHAR2(155);
ds_ant1_pos_2_w         VARCHAR2(155);
ds_ant1_pos_3_w     	VARCHAR2(155);
ds_ant1_pos_4_w 	VARCHAR2(155);
ds_ant1_pos_5_w 	VARCHAR2(155);
ds_ant1_pos_6_w 	VARCHAR2(155);
ds_ant1_pos_7_w 	VARCHAR2(155);
ds_ant1_pos_8_w 	VARCHAR2(155);
ds_ant1_pos_9_w 	VARCHAR2(155);
ds_ant1_pos_10_w	VARCHAR2(155);
ds_ant1_pos_11_w	VARCHAR2(155);
ds_ant1_pos_12_w	VARCHAR2(155);
ds_ant1_pos_13_w	VARCHAR2(155);
ds_ant1_pos_14_w	VARCHAR2(155);
ds_ant1_pos_15_w	VARCHAR2(155);
ds_ant1_pos_16_w	VARCHAR2(155);
ds_ant2_pos_1_w         VARCHAR2(155);
ds_ant2_pos_2_w         VARCHAR2(155);
ds_ant2_pos_3_w     	VARCHAR2(155);
ds_ant2_pos_4_w 	VARCHAR2(155);
ds_ant2_pos_5_w 	VARCHAR2(155);
ds_ant2_pos_6_w 	VARCHAR2(155);
ds_ant2_pos_7_w 	VARCHAR2(155);
ds_ant2_pos_8_w 	VARCHAR2(155);
ds_ant2_pos_9_w 	VARCHAR2(155);
ds_ant2_pos_10_w	VARCHAR2(155);
ds_ant2_pos_11_w	VARCHAR2(155);
ds_ant2_pos_12_w	VARCHAR2(155);
ds_ant2_pos_13_w	VARCHAR2(155);
ds_ant2_pos_14_w	VARCHAR2(155);
ds_ant2_pos_15_w	VARCHAR2(155);
ds_antigeno1_w		VARCHAR2(155);
ds_antigeno2_w		VARCHAR2(155);
nr_posicao_ant_w	number(10);

cursor c01 is
select
  Obter_Descricao_Dominio(3887,CD_PRIMEIRO_ANTIGENO),
  Obter_Descricao_Dominio(3887,CD_segundo_ANTIGENO),
  nr_posicao
from SAN_TES_ESP_GER_ANT_ISBT
where dt_inativacao is null;

begin

if(nr_seq_doacao_p is not null) then

	SELECT San_Gerar_Codigo_ISBT.san_obter_fenotipagem_concat(nr_seq_doacao_p, 1)
		into ds_fenotipagem_w
	FROM dual;

	if(ds_fenotipagem_w is not null) then

		open c01;
		loop
		fetch c01 into
			ds_antigeno1_w,
			ds_antigeno2_w,
			nr_posicao_ant_w;
		exit when c01%notfound;
		begin

		if(nr_posicao_ant_w = 2) then
			ds_ant1_pos_2_w := ds_antigeno1_w;
			ds_ant2_pos_2_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 3) then
			ds_ant1_pos_3_w := ds_antigeno1_w;
			ds_ant2_pos_3_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 4) then
			ds_ant1_pos_4_w := ds_antigeno1_w;
			ds_ant2_pos_4_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 5) then
			ds_ant1_pos_5_w := ds_antigeno1_w;
			ds_ant2_pos_5_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 6) then
			ds_ant1_pos_6_w := ds_antigeno1_w;
			ds_ant2_pos_6_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 7) then
			ds_ant1_pos_7_w := ds_antigeno1_w;
			ds_ant2_pos_7_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 8) then
			ds_ant1_pos_8_w := ds_antigeno1_w;
			ds_ant2_pos_8_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 9) then
			ds_ant1_pos_9_w := ds_antigeno1_w;
			ds_ant2_pos_9_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 10) then
			ds_ant1_pos_10_w := ds_antigeno1_w;
			ds_ant2_pos_10_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 11) then
			ds_ant1_pos_11_w := ds_antigeno1_w;
			ds_ant2_pos_11_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 12) then
			ds_ant1_pos_12_w := ds_antigeno1_w;
			ds_ant2_pos_12_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 13) then
			ds_ant1_pos_13_w := ds_antigeno1_w;
			ds_ant2_pos_13_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 14) then
			ds_ant1_pos_14_w := ds_antigeno1_w;
			ds_ant2_pos_14_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 15) then
			ds_ant1_pos_15_w := ds_antigeno1_w;
			ds_ant2_pos_15_w := ds_antigeno2_w;
		end if;

		if(nr_posicao_ant_w = 16) then
			ds_ant1_pos_16_w := ds_antigeno1_w;
		end if;

		end;
		end loop;
		close c01;

		while(ds_fenotipagem_w is not null or trim(ds_fenotipagem_w) <> '') loop

			ie_entrou_w := 'N';
			nr_posicao_str_w := inStr(ds_fenotipagem_w,', ');

			if(nr_posicao_str_w = 0) then
				ds_fenotipo_w := trim(ds_fenotipagem_w);
				ds_fenotipagem_w := null;
			else
				ds_fenotipo_w := substr(ds_fenotipagem_w, 1, nr_posicao_str_w - 1);
				ds_fenotipagem_w := substr(ds_fenotipagem_w, nr_posicao_str_w + 2);
			end if;

			if(ds_fenotipo_w = 'C+' or  ds_fenotipo_w = 'C-' or
				ds_fenotipo_w = 'c+' or ds_fenotipo_w = 'c-' or
				ds_fenotipo_w = 'E+' or ds_fenotipo_w = 'E-' or
				ds_fenotipo_w = 'e+' or ds_fenotipo_w = 'e-'
				or ds_fenotipo_w = 'c_T' or ds_fenotipo_w = 'C_T'
				or ds_fenotipo_w = 'E_T' or ds_fenotipo_w = 'e_T') then
				ds_posicao1_w := ds_posicao1_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_2_w || '+' or ds_fenotipo_w = ds_ant1_pos_2_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_2_w || '+' or ds_fenotipo_w = ds_ant2_pos_2_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_2_w || '_T' or ds_fenotipo_w = ds_ant2_pos_2_w || '_T') then
				ds_posicao2_w := ds_posicao2_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_3_w || '+' or ds_fenotipo_w = ds_ant1_pos_3_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_3_w || '+' or ds_fenotipo_w = ds_ant2_pos_3_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_3_w || '_T' or ds_fenotipo_w = ds_ant2_pos_3_w || '_T') then
				ds_posicao3_w := ds_posicao3_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_4_w || '+' or ds_fenotipo_w = ds_ant1_pos_4_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_4_w || '+' or ds_fenotipo_w = ds_ant2_pos_4_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_4_w || '_T' or ds_fenotipo_w = ds_ant2_pos_4_w || '_T') then
				ds_posicao4_w := ds_posicao4_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_5_w || '+' or ds_fenotipo_w = ds_ant1_pos_5_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_5_w || '+' or ds_fenotipo_w = ds_ant2_pos_5_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_5_w || '_T' or ds_fenotipo_w = ds_ant2_pos_5_w || '_T') then
				ds_posicao5_w := ds_posicao5_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_6_w || '+' or ds_fenotipo_w = ds_ant1_pos_6_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_6_w || '+' or ds_fenotipo_w = ds_ant2_pos_6_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_6_w || '_T' or ds_fenotipo_w = ds_ant2_pos_6_w || '_T') then
				ds_posicao6_w := ds_posicao6_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_7_w || '+' or ds_fenotipo_w = ds_ant1_pos_7_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_7_w || '+' or ds_fenotipo_w = ds_ant2_pos_7_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_7_w || '_T' or ds_fenotipo_w = ds_ant2_pos_7_w || '_T') then
				ds_posicao7_w := ds_posicao7_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_8_w || '+' or ds_fenotipo_w = ds_ant1_pos_8_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_8_w || '+' or ds_fenotipo_w = ds_ant2_pos_8_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_8_w || '_T' or ds_fenotipo_w = ds_ant2_pos_8_w || '_T') then
				ds_posicao8_w := ds_posicao8_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_9_w || '+' or ds_fenotipo_w = ds_ant1_pos_9_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_9_w || '+' or ds_fenotipo_w = ds_ant2_pos_9_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_9_w || '_T' or ds_fenotipo_w = ds_ant2_pos_9_w || '_T') then
				ds_posicao9_w := ds_posicao9_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_10_w || '+' or ds_fenotipo_w = ds_ant1_pos_10_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_10_w || '+' or ds_fenotipo_w = ds_ant2_pos_10_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_10_w || '_T' or ds_fenotipo_w = ds_ant2_pos_10_w || '_T') then
				ds_posicao10_w := ds_posicao10_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_11_w || '+' or ds_fenotipo_w = ds_ant1_pos_11_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_11_w || '+' or ds_fenotipo_w = ds_ant2_pos_11_w || '-'
				or ds_fenotipo_w = ds_ant1_pos_11_w || '_T' or ds_fenotipo_w = ds_ant2_pos_11_w || '_T') then
				ds_posicao11_w := ds_posicao11_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_12_w || '+' or ds_fenotipo_w = ds_ant1_pos_12_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_12_w || '+' or ds_fenotipo_w = ds_ant2_pos_12_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_12_w || '_T' or ds_fenotipo_w = ds_ant2_pos_12_w || '_T') then
				ds_posicao12_w := ds_posicao12_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_13_w || '+' or ds_fenotipo_w = ds_ant1_pos_13_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_13_w || '+' or ds_fenotipo_w = ds_ant2_pos_13_w || '-'
				or ds_fenotipo_w = ds_ant1_pos_13_w || '_T' or ds_fenotipo_w = ds_ant2_pos_13_w || '_T') then
				ds_posicao13_w := ds_posicao13_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_14_w || '+' or ds_fenotipo_w = ds_ant1_pos_14_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_14_w || '+' or ds_fenotipo_w = ds_ant2_pos_14_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_14_w || '_T' or ds_fenotipo_w = ds_ant2_pos_14_w || '_T') then
				ds_posicao14_w := ds_posicao14_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_15_w || '+' or ds_fenotipo_w = ds_ant1_pos_15_w || '-' or 
				ds_fenotipo_w = ds_ant2_pos_15_w || '+' or ds_fenotipo_w = ds_ant2_pos_15_w || '-' 
				or ds_fenotipo_w = ds_ant1_pos_15_w || '_T' or ds_fenotipo_w = ds_ant2_pos_15_w || '_T') then
				ds_posicao15_w := ds_posicao15_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ds_fenotipo_w = ds_ant1_pos_16_w || '+' or ds_fenotipo_w = ds_ant1_pos_16_w || '-' or ds_fenotipo_w = ds_ant1_pos_16_w || '_T') then
				ds_posicao16_w := ds_posicao16_w || ds_fenotipo_w;
				ie_entrou_w := 'S';
			end if;

			if(ie_entrou_w = 'N' and ds_posicao17_18_w is null) then
				select max(vl_valor)
					into ds_posicao17_18_w
				from SAN_TES_ESP_ANT_ESPEC_ISBT
				where ie_situacao = 'A'
				and obter_valor_dominio(9620,cd_antigeno) || '-' = ds_fenotipo_w;
			end if;

		end loop;

	if(ds_posicao1_w is not null) then
    
		select max(vl_valor)
			into ds_posicao1_w
		from SAN_TES_ESP_ANT_RES_ISBT
		where dt_inativacao is null
		and cd_resultado_rh = ds_posicao1_w;
    
    if(ds_posicao1_w is null) then
	ds_posicao1_w := '0T';
    end if;

	end if;

end if;

	ds_posicao2_w  := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao2_w, ds_ant1_pos_2_w, ds_ant2_pos_2_w);
	ds_posicao3_w  := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao3_w, ds_ant1_pos_3_w, ds_ant2_pos_3_w);
	ds_posicao4_w  := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao4_w, ds_ant1_pos_4_w, ds_ant2_pos_4_w);
	ds_posicao5_w  := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao5_w, ds_ant1_pos_5_w, ds_ant2_pos_5_w);
	ds_posicao6_w  := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao6_w, ds_ant1_pos_6_w, ds_ant2_pos_6_w);
	ds_posicao7_w  := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao7_w, ds_ant1_pos_7_w, ds_ant2_pos_7_w);
	ds_posicao8_w  := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao8_w, ds_ant1_pos_8_w, ds_ant2_pos_8_w);
	ds_posicao9_w  := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao9_w, ds_ant1_pos_9_w, ds_ant2_pos_9_w);
	ds_posicao10_w := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao10_w, ds_ant1_pos_10_w, ds_ant2_pos_10_w);
	ds_posicao11_w := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao11_w, ds_ant1_pos_11_w, ds_ant2_pos_11_w);
	ds_posicao12_w := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao12_w, ds_ant1_pos_12_w, ds_ant2_pos_12_w);
	ds_posicao13_w := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao13_w, ds_ant1_pos_13_w, ds_ant2_pos_13_w);
	 
	if(ds_posicao1_w is null or ds_posicao1_w = '9') then
		ds_posicao14_w := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao14_w, ds_ant1_pos_14_w, ds_ant2_pos_14_w);
		ds_posicao15_w := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao15_w, ds_ant1_pos_15_w, ds_ant2_pos_15_w);
		ds_posicao16_w := San_Gerar_Codigo_ISBT.san_obter_vlpos_antigeno(ds_posicao16_w, ds_ant1_pos_16_w, null);
		if(ds_posicao1_w = '9' or ds_posicao1_w is null) then
			ds_posicao1_w := '9';
		else
			ds_posicao1_w := '0';
		end if;
	else
		if(ds_posicao1_w = '0T') then
			ds_posicao1_w := '0';
		end if;
    
		ds_posicao14_w := '9';
		ds_posicao15_w := '9';
		ds_posicao16_w := '9';
	end if;

end if;

ds_fenotipagem_w := ds_posicao1_w || ds_posicao2_w || ds_posicao3_w || ds_posicao4_w || ds_posicao5_w || ds_posicao6_w || ds_posicao7_w || ds_posicao8_w ||
			ds_posicao9_w || ds_posicao10_w || ds_posicao11_w || ds_posicao12_w || ds_posicao13_w || ds_posicao14_w || ds_posicao15_w || ds_posicao16_w;

if(ds_posicao17_18_w is null) then
	ds_fenotipagem_w := ds_fenotipagem_w || '99';
else
	ds_fenotipagem_w := ds_fenotipagem_w || ds_posicao17_18_w;
end if;
 
if(ds_fenotipagem_w is not null) then
	select max(nr_seq_doacao),
        	max(nr_seq_producao)
  	into  nr_seq_doacao_w,
        	nr_seq_producao_w
  	FROM
		san_result_fenotipagem
	WHERE
		(nr_seq_doacao = nr_seq_doacao_p or nr_seq_producao = nr_seq_doacao_p);
    
  if(nr_seq_doacao_w is not null) then
   	update SAN_DOACAO
    	set cd_isbt_antigeno = ds_fenotipagem_w
    	where nr_sequencia = nr_seq_doacao_w;
  else
    	update SAN_PRODUCAO
    	set cd_isbt_antigeno = ds_fenotipagem_w
    	where nr_sequencia = nr_seq_producao_w;
  end if;

	commit;
end if;
END;

end San_Gerar_Codigo_ISBT;
/
