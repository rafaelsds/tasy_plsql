create or replace 
package pls_mens_processo_pck as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Processar informacoes para a mensalidade
------------------------------------------------------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

procedure processar_mensalidade(nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
				nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
				dt_mesano_referencia_p	pls_lote_mensalidade.dt_mesano_referencia%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type);

procedure add_item_bonificacao(	nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type,
				vl_item_p			pls_mensalidade_seg_item.vl_item%type,
				ds_observacao_p			pls_mensalidade_seg_item.ds_observacao%type,
				nr_seq_bonificacao_vinculo_p	pls_mensalidade_seg_item.nr_seq_bonificacao_vinculo%type,
				nr_seq_vinculo_sca_p		pls_mensalidade_seg_item.nr_seq_vinculo_sca%type,
				dt_inicio_cobertura_p		pls_mensalidade_seg_item.dt_inicio_cobertura%type,
				dt_fim_cobertura_p		pls_mensalidade_seg_item.dt_fim_cobertura%type,
				ie_tipo_mensalidade_p		pls_mensalidade_seg_item.ie_tipo_mensalidade%type);

procedure gerar_mensagens_mensalidade(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
					nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type);

procedure suspender_reajuste_fx_etaria(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
					nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
					dt_mesano_referencia_p	pls_lote_mensalidade.dt_mesano_referencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type);
					
procedure processar_tributos(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
				nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type);

function obter_valor_bonific_vetor(	nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type) return number;

end pls_mens_processo_pck;
/

create or replace
package body pls_mens_processo_pck as

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nm_usuario_w		usuario.nm_usuario%type;

nr_indice_bonificacao_w		pls_integer;
tb_nr_seq_mensalidade_seg_w	pls_util_cta_pck.t_number_table;
tb_vl_item_w			pls_util_cta_pck.t_number_table;
tb_ds_observacao_w		pls_util_cta_pck.t_varchar2_table_255;
tb_nr_seq_bonificacao_vinc_w	pls_util_cta_pck.t_number_table;
tb_nr_seq_vinculo_sca_w		pls_util_cta_pck.t_number_table;
tb_dt_inicio_cobertura_w	pls_util_cta_pck.t_date_table;
tb_dt_fim_cobertura_w		pls_util_cta_pck.t_date_table;
tb_ie_tipo_mensalidade_w	pls_util_cta_pck.t_varchar2_table_1;

function obter_valor_bonific_vetor(	nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type)
					return number is
vl_total_bonif_w	pls_mensalidade_seg_item.vl_item%type;
begin

vl_total_bonif_w := 0;

if	(tb_nr_seq_mensalidade_seg_w.count > 0) then
	for i in tb_nr_seq_mensalidade_seg_w.first..tb_nr_seq_mensalidade_seg_w.last loop
		if	(tb_nr_seq_mensalidade_seg_w(i) = nr_seq_mensalidade_seg_p) then
			vl_total_bonif_w := vl_total_bonif_w + tb_vl_item_w(i);
		end if;
	end loop;
end if;

return vl_total_bonif_w;

end obter_valor_bonific_vetor;

procedure add_item_bonificacao(	nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type,
				vl_item_p			pls_mensalidade_seg_item.vl_item%type,
				ds_observacao_p			pls_mensalidade_seg_item.ds_observacao%type,
				nr_seq_bonificacao_vinculo_p	pls_mensalidade_seg_item.nr_seq_bonificacao_vinculo%type,
				nr_seq_vinculo_sca_p		pls_mensalidade_seg_item.nr_seq_vinculo_sca%type,
				dt_inicio_cobertura_p		pls_mensalidade_seg_item.dt_inicio_cobertura%type,
				dt_fim_cobertura_p		pls_mensalidade_seg_item.dt_fim_cobertura%type,
				ie_tipo_mensalidade_p		pls_mensalidade_seg_item.ie_tipo_mensalidade%type) is
begin

tb_nr_seq_mensalidade_seg_w(nr_indice_bonificacao_w)	:= nr_seq_mensalidade_seg_p;
tb_vl_item_w(nr_indice_bonificacao_w)			:= vl_item_p;
tb_ds_observacao_w(nr_indice_bonificacao_w)		:= ds_observacao_p;
tb_nr_seq_bonificacao_vinc_w(nr_indice_bonificacao_w)	:= nr_seq_bonificacao_vinculo_p;
tb_nr_seq_vinculo_sca_w(nr_indice_bonificacao_w)	:= nr_seq_vinculo_sca_p;
tb_dt_inicio_cobertura_w(nr_indice_bonificacao_w)	:= dt_inicio_cobertura_p;
tb_dt_fim_cobertura_w(nr_indice_bonificacao_w)		:= dt_fim_cobertura_p;
tb_ie_tipo_mensalidade_w(nr_indice_bonificacao_w)	:= ie_tipo_mensalidade_p;

nr_indice_bonificacao_w	:= nr_indice_bonificacao_w + 1;
end add_item_bonificacao;

procedure inserir_bonificacao(	nr_seq_lote_p	pls_lote_mensalidade.nr_sequencia%type,
				nm_usuario_p	usuario.nm_usuario%type) is
begin
if	(tb_nr_seq_mensalidade_seg_w.count > 0) then
	forall i in tb_nr_seq_mensalidade_seg_w.first..tb_nr_seq_mensalidade_seg_w.last
		insert	into	pls_mensalidade_seg_item
			(	nr_sequencia, nr_seq_mensalidade_seg, nr_seq_lote, ie_tipo_mensalidade,
				ie_tipo_item, vl_item, ds_observacao, nr_seq_bonificacao_vinculo, nr_seq_vinculo_sca,
				nm_usuario, dt_atualizacao, nm_usuario_nrec, dt_atualizacao_nrec,
				dt_inicio_cobertura, dt_fim_cobertura)
			values(	pls_mensalidade_seg_item_seq.nextval, tb_nr_seq_mensalidade_seg_w(i), nr_seq_lote_p, tb_ie_tipo_mensalidade_w(i),
				'14', tb_vl_item_w(i), tb_ds_observacao_w(i), tb_nr_seq_bonificacao_vinc_w(i), tb_nr_seq_vinculo_sca_w(i),
				nm_usuario_p, sysdate, nm_usuario_p, sysdate,
				tb_dt_inicio_cobertura_w(i), tb_dt_fim_cobertura_w(i));
	commit;
end if;

tb_nr_seq_mensalidade_seg_w.delete;
tb_vl_item_w.delete;
tb_ds_observacao_w.delete;
tb_nr_seq_bonificacao_vinc_w.delete;
tb_nr_seq_vinculo_sca_w.delete;
tb_dt_inicio_cobertura_w.delete;
tb_dt_fim_cobertura_w.delete;
tb_ie_tipo_mensalidade_w.delete;
nr_indice_bonificacao_w	:= 0;
end inserir_bonificacao;

procedure tratar_valor_min_abat_fatura(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
					nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
					nr_seq_pagador_p	pls_mensalidade.nr_seq_pagador%type,
					dt_referencia_lote_p	pls_lote_mensalidade.dt_mesano_referencia%type,
					vl_minimo_abat_fatura_p	pls_regra_mens_contrato.vl_minimo_abat_fatura%type) is

vl_mensalidade_w		pls_mensalidade_seg_item.vl_item%type;
vl_mens_segurado_w		pls_mensalidade_seg_item.vl_item%type;

Cursor C01 (	nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	nr_sequencia nr_seq_mensalidade_seg,
		nr_seq_segurado
	from	pls_mensalidade_segurado
	where	nr_seq_mensalidade	= nr_seq_mensalidade_pc;

begin

select	sum(a.vl_item)
into	vl_mensalidade_w
from	pls_mensalidade_seg_item a,
	pls_mensalidade_segurado b
where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	b.nr_seq_mensalidade	= nr_seq_mensalidade_p;

if	(vl_mensalidade_w < vl_minimo_abat_fatura_p) then
	for r_c01_w in C01(nr_seq_mensalidade_p) loop
		begin
		
		select	sum(a.vl_item)
		into	vl_mens_segurado_w
		from	pls_mensalidade_seg_item	a,
			pls_mensalidade_segurado	b
		where	a.nr_seq_mensalidade_seg = b.nr_sequencia
		and	b.nr_sequencia	= r_c01_w.nr_seq_mensalidade_seg;
		
		if	(nvl(vl_mens_segurado_w,0) <> 0) then
			insert	into	pls_mensalidade_seg_item
			(	nr_sequencia, nr_seq_mensalidade_seg, nr_seq_lote,
				dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
				ie_tipo_item, vl_item, ie_tipo_mensalidade
				)
			values(	pls_mensalidade_seg_item_seq.nextval, r_c01_w.nr_seq_mensalidade_seg, nr_seq_lote_p,
				sysdate, sysdate, nm_usuario_w, nm_usuario_w,
				'28', vl_mens_segurado_w*-1, 'P'
				);
			
			-- Lancamento programado para o proximo mes
			insert	into	pls_segurado_mensalidade
				(	nr_sequencia, nr_seq_segurado, nr_seq_pagador,
					cd_estabelecimento, dt_referencia, ie_tipo_item,
					dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
					vl_item, tx_desconto, ie_acao_desfazer,
					ie_situacao, ie_tipo_lanc, nr_seq_mens_seg_gerado,
					ds_observacao
					)
				values
				(	pls_segurado_mensalidade_seq.nextval, r_c01_w.nr_seq_segurado, nr_seq_pagador_p,
					cd_estabelecimento_w, add_months(dt_referencia_lote_p,1), '29',
					sysdate, nm_usuario_w, sysdate, nm_usuario_w,
					vl_mens_segurado_w, 0, 'A',
					'A','B', r_c01_w.nr_seq_mensalidade_seg,
					wheb_mensagem_pck.get_texto(391705,'NR_SEQ_LOTE_P='||nr_seq_lote_p)
					);
			commit;
		end if;
		
		end;
	end loop;
end if;
end tratar_valor_min_abat_fatura;

procedure tratar_valor_minimo_fatura(	nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
					vl_minimo_p		pls_regra_mens_contrato.vl_minimo%type) is

vl_total_preco_pre_w	pls_mensalidade_seg_item.vl_item%type;
vl_diferenca_w		pls_mensalidade_seg_item.vl_item%type;
vl_preco_individual_w	pls_mensalidade_seg_item.vl_item%type;
nr_seq_ultima_seq_w	pls_mensalidade_seg_item.nr_sequencia%type;
qt_segurado_w		pls_integer;

nr_indice_w		pls_integer;
tb_nr_seq_preco_pre_w	pls_util_cta_pck.t_number_table;

Cursor C01 (	nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_sequencia
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b
	where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
	and	b.nr_seq_mensalidade	= nr_seq_mensalidade_pc
	and	a.ie_tipo_item	= '1';

begin

select	sum(a.vl_item),
	count(b.nr_sequencia)
into	vl_total_preco_pre_w,
	qt_segurado_w
from	pls_mensalidade_seg_item	a,
	pls_mensalidade_segurado	b
where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	a.ie_tipo_item		= '1'
and	b.nr_seq_mensalidade	= nr_seq_mensalidade_p;

if	(vl_total_preco_pre_w < vl_minimo_p) then
	vl_preco_individual_w	:= trunc(vl_minimo_p / qt_segurado_w,2);
	
	nr_indice_w	:= 0;
	tb_nr_seq_preco_pre_w.delete;
	for r_c01_w in C01(nr_seq_mensalidade_p) loop
		begin
		tb_nr_seq_preco_pre_w(nr_indice_w)	:= r_c01_w.nr_sequencia;
		nr_seq_ultima_seq_w	:= r_c01_w.nr_sequencia;
		nr_indice_w	:= nr_indice_w + 1;
		end;
	end loop;
	
	if	(tb_nr_seq_preco_pre_w.count > 0) then
		forall i in tb_nr_seq_preco_pre_w.first..tb_nr_seq_preco_pre_w.last
			update	pls_mensalidade_seg_item
			set	vl_item	= vl_preco_individual_w
			where	nr_sequencia	= tb_nr_seq_preco_pre_w(i);
		commit;
		
		vl_diferenca_w	:= vl_minimo_p - (vl_preco_individual_w * qt_segurado_w);
		if	(vl_diferenca_w > 0) then
			update	pls_mensalidade_seg_item
			set	vl_item	= vl_item + vl_diferenca_w
			where	nr_sequencia	= nr_seq_ultima_seq_w;
		end if;
	end if;
end if;

end tratar_valor_minimo_fatura;

procedure gerar_taxa_emissao_boleto(	nr_seq_lote_p			pls_lote_mensalidade.nr_sequencia%type,
					nr_seq_mensalidade_p		pls_mensalidade.nr_sequencia%type,
					ie_taxa_emissao_boleto_p	pls_contrato_pagador.ie_taxa_emissao_boleto%type,
					nr_parcela_p			pls_mensalidade.nr_parcela%type,
					nr_seq_pagador_fin_p		pls_contrato_pagador_fin.nr_sequencia%type,
					dt_referencia_p			pls_mensalidade.dt_referencia%type) is

vl_taxa_emissao_w	pls_regra_emissao_boleto.vl_taxa_emissao%type;
cd_tipo_portador_w	pls_contrato_pagador_fin.cd_tipo_portador%type;
cd_portador_w		pls_contrato_pagador_fin.cd_portador%type;

qt_segurado_w		pls_integer;
nr_indice_w		pls_integer;
vl_taxa_benef_w		pls_mensalidade_seg_item.vl_item%type;
vl_diferenca_w		pls_mensalidade_seg_item.vl_item%type;

Cursor C01 is
	select	vl_taxa_emissao
	from	pls_regra_emissao_boleto
	where	ie_situacao	= 'A'
	and	((cd_tipo_portador = cd_tipo_portador_w) or (cd_tipo_portador is null))
	and	((cd_portador = cd_portador_w) or (cd_portador is null))
	and	dt_referencia_p between dt_inicio_vigencia_ref and dt_fim_vigencia_ref
	and	(((ie_primeira_mensalidade = 'N') and (nr_parcela_p > 1)) or (ie_primeira_mensalidade = 'S'))
	order by nvl(ie_primeira_mensalidade,'N'),
		nvl(cd_portador,0),
		nvl(cd_tipo_portador,0);

--Cursor para quando for destacado
Cursor C02 (	nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_mensalidade_seg,
		null nr_seq_preco_pre
	from	pls_mensalidade_segurado a
	where	a.nr_seq_mensalidade	= nr_seq_mensalidade_pc
	and	exists (select	1
			from	pls_mensalidade_seg_item x
			where	a.nr_sequencia = x.nr_seq_mensalidade_seg)
	and	ie_taxa_emissao_boleto_p = 'D'
	union all
	select	b.nr_sequencia nr_seq_mensalidade_seg,
		a.nr_sequencia nr_seq_preco_pre
	from	pls_mensalidade_seg_item	a,
		pls_mensalidade_segurado	b
	where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
	and	a.ie_tipo_item		= '1'
	and	b.nr_seq_mensalidade	= nr_seq_mensalidade_pc
	and	ie_taxa_emissao_boleto_p = 'E';

begin
vl_taxa_emissao_w	:= 0;
nr_indice_w		:= 0;

select	max(cd_tipo_portador),
	max(cd_portador)
into	cd_tipo_portador_w,
	cd_portador_w
from	pls_contrato_pagador_fin
where	nr_sequencia	= nr_seq_pagador_fin_p;

for r_c01_w in C01 loop
	begin
	vl_taxa_emissao_w	:= r_c01_w.vl_taxa_emissao;
	end;
end loop;

if	(vl_taxa_emissao_w > 0) then
	--Se for destacado, ira ratear o valor para todos os beneficiarios da mensalidade
	if	(ie_taxa_emissao_boleto_p = 'D') then
		select	count(1)
		into	qt_segurado_w
		from	pls_mensalidade_segurado a
		where	a.nr_seq_mensalidade	= nr_seq_mensalidade_p
		and	exists (select	1
				from	pls_mensalidade_seg_item x
				where	a.nr_sequencia = x.nr_seq_mensalidade_seg);
	elsif	(ie_taxa_emissao_boleto_p = 'E') then --Se for embutido, ira ratear o valor entre os beneficiarios que tem preco pre
		select	count(b.nr_sequencia)
		into	qt_segurado_w
		from	pls_mensalidade_seg_item	a,
			pls_mensalidade_segurado	b
		where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
		and	a.ie_tipo_item		= '1'
		and	b.nr_seq_mensalidade	= nr_seq_mensalidade_p;
	end if;
	
	if	(qt_segurado_w > 0) then
		vl_taxa_benef_w	:= round(vl_taxa_emissao_w/qt_segurado_w,2);
		
		for r_c02_w in C02(nr_seq_mensalidade_p) loop
			begin
			vl_diferenca_w	:= 0;
			nr_indice_w	:= nr_indice_w + 1;
			if	(nr_indice_w = qt_segurado_w) then
				vl_diferenca_w	:= vl_taxa_emissao_w - (vl_taxa_benef_w * qt_segurado_w);
			end if;
			
			if	(ie_taxa_emissao_boleto_p = 'D') then
				insert	into	pls_mensalidade_seg_item
					(	nr_sequencia, nr_seq_mensalidade_seg, nr_seq_lote,
						dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
						ie_tipo_item, vl_item, ie_tipo_mensalidade
						)
					values(	pls_mensalidade_seg_item_seq.nextval, r_c02_w.nr_seq_mensalidade_seg, nr_seq_lote_p,
						sysdate, sysdate, nm_usuario_w, nm_usuario_w,
						'17', vl_taxa_benef_w+vl_diferenca_w, 'P'
						);
			elsif	(ie_taxa_emissao_boleto_p = 'E') then
				update	pls_mensalidade_seg_item
				set	vl_item		= vl_item + (vl_taxa_benef_w+vl_diferenca_w),
					vl_taxa_boleto	= (vl_taxa_benef_w+vl_diferenca_w)
				where	nr_sequencia	= r_c02_w.nr_seq_preco_pre;
			end if;
			end;
		end loop;
	end if;
end if;

end gerar_taxa_emissao_boleto;

procedure processar_mensalidade(nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
				nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
				dt_mesano_referencia_p	pls_lote_mensalidade.dt_mesano_referencia%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

nr_seq_regra_vl_minimo_w	pls_regra_mens_contrato.nr_sequencia%type;
vl_minimo_w			pls_regra_mens_contrato.vl_minimo%type;
vl_minimo_abat_fatura_w		pls_regra_mens_contrato.vl_minimo_abat_fatura%type;

nr_seq_log_w			pls_mens_log_geracao.nr_sequencia%type;
dt_auxiliar_w			date;
qt_cobranca_adap_w		pls_integer;
dt_inicio_cobertura_w		date;
dt_fim_cobertura_w		date;
nr_seq_item_pre_w		pls_mensalidade_seg_item.nr_sequencia%type;
vl_adaptacao_w			pls_mensalidade_seg_item.vl_item%type;
ds_observacao_w			varchar2(255);
qt_dias_prop_w			number(10);
qt_dias_cobertura_w		number(10);
vl_item_w			pls_mensalidade_seg_item.vl_item%type;
ie_mens_adaptacao_retro_w	pls_parametros.ie_mens_adaptacao_retro%type;

Cursor C01 (	nr_seq_lote_pc		pls_lote_mensalidade.nr_sequencia%type,
		nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_mensalidade,
		a.dt_referencia,
		a.ie_tipo_estipulante,
		a.nr_parcela,
		a.nr_seq_pagador,
		a.nr_seq_pagador_fin,
		a.nr_seq_conta_banco,
		a.nr_seq_forma_cobranca,
		b.dt_mesano_referencia dt_referencia_lote,
		c.nr_seq_contrato,
		c.nr_seq_pagador_intercambio nr_seq_intercambio,
		nvl(c.ie_taxa_emissao_boleto,'N') ie_taxa_emissao_boleto,
		c.nr_seq_classif_itens,
		c.cd_cgc,
		c.cd_pessoa_fisica,
		(select	sum(x.vl_item)
		from	pls_mensalidade_seg_item x,
			pls_mensalidade_segurado y
		where	y.nr_sequencia = x.nr_seq_mensalidade_seg
		and	y.nr_seq_mensalidade = a.nr_sequencia) vl_mensalidade
	from	pls_mensalidade	a,
		pls_lote_mensalidade b,
		pls_contrato_pagador c
	where	b.nr_sequencia	= a.nr_seq_lote
	and	c.nr_sequencia	= a.nr_seq_pagador
	and	b.nr_sequencia	= nr_seq_lote_pc
	and	((a.nr_sequencia = nr_seq_mensalidade_pc) or (nr_seq_mensalidade_pc is null));

--Cursor para gerar bonificacao
Cursor C02 (	nr_seq_lote_pc		pls_lote_mensalidade.nr_sequencia%type,
		nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_mensalidade_seg,
		a.dt_mesano_referencia,
		a.nr_seq_segurado,
		a.qt_idade,
		a.nr_parcela,
		a.nr_parcela_contrato,
		a.nr_seq_segurado_preco,
		a.dt_inicio_cobertura,
		a.dt_fim_cobertura,
		a.ie_remido,
		b.nr_sequencia nr_seq_mensalidade,
		b.nr_seq_pagador,
		b.nr_seq_forma_cobranca,
		c.nr_seq_titular,
		c.nr_seq_contrato,
		c.nr_seq_intercambio,
		c.nr_seq_parentesco,
		c.ie_acao_contrato,
		c.cd_pessoa_fisica,
		c.ie_situacao_trabalhista
	from	pls_mensalidade_segurado a,
		pls_mensalidade b,
		pls_segurado c
	where	c.nr_sequencia		= a.nr_seq_segurado
	and	b.nr_sequencia		= a.nr_seq_mensalidade
	and	b.nr_seq_lote		= nr_seq_lote_pc
	and	((b.nr_sequencia = nr_seq_mensalidade_pc) or (nr_seq_mensalidade_pc is null))
	and	exists (select	1 --Gerar bonificacao somente se existir itens para a mensalidade
			from	pls_mensalidade_seg_item x
			where	a.nr_sequencia	= x.nr_seq_mensalidade_seg);

Cursor C03 (	nr_seq_lote_pc		pls_lote_mensalidade.nr_sequencia%type,
		nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_mensalidade_seg,
		a.dt_mesano_referencia,
		a.nr_seq_segurado,
		b.nr_sequencia nr_seq_mensalidade,
		(	select	max(x.dt_alteracao)
			from	pls_segurado_alt_plano x
			where	x.nr_seq_segurado = c.nr_sequencia
			and	x.ie_situacao = 'A'
			and	x.nr_seq_plano_atual = c.nr_seq_plano) dt_adaptacao,
		d.vl_item,
		d.nr_seq_plano,
		e.tx_adaptacao
	from	pls_mensalidade_seg_item d,
		pls_mensalidade_segurado a,
		pls_mensalidade b,
		pls_segurado c,
		pls_tabela_preco e
	where	a.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	c.nr_sequencia	= a.nr_seq_segurado
	and	b.nr_sequencia	= a.nr_seq_mensalidade
	and	e.nr_sequencia	= c.nr_seq_tabela
	and	b.nr_seq_lote	= nr_seq_lote_pc
	and	((b.nr_sequencia = nr_seq_mensalidade_pc) or (nr_seq_mensalidade_pc is null))
	and	d.ie_tipo_item	= '31'
	order by a.dt_mesano_referencia;

Cursor C04 (	nr_seq_lote_pc		pls_lote_mensalidade.nr_sequencia%type,
		nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_seq_lancamento_mens
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_mensalidade c
	where	c.nr_sequencia = b.nr_seq_mensalidade
	and	b.nr_sequencia = a.nr_seq_mensalidade_seg
	and	((c.nr_sequencia = nr_seq_mensalidade_pc) or (nr_seq_mensalidade_pc is null))
	and	c.nr_seq_lote	= nr_seq_lote_pc
	and	a.nr_seq_lancamento_mens is not null
	group by
		a.nr_seq_lancamento_mens;

cursor c06 (	nr_seq_lote_pc		pls_lote_mensalidade.nr_sequencia%type,
		nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	b.nr_seq_segurado_preco,
		b.nr_seq_segurado,
		e.dt_reajuste,
		b.nr_seq_regra_susp_reaj_fx
	from	pls_mensalidade a,
		pls_mensalidade_segurado b,
		pls_mensalidade_seg_item c,
		pls_segurado_preco e
	where	a.nr_sequencia	= b.nr_seq_mensalidade
	and	b.nr_sequencia	= c.nr_seq_mensalidade_seg
	and	e.nr_sequencia	= b.nr_seq_segurado_preco
	and	b.nr_seq_regra_susp_reaj_fx is not null
	and	a.nr_seq_lote = nr_seq_lote_pc
	and	((a.nr_sequencia = nr_seq_mensalidade_pc) or (nr_seq_mensalidade_pc is null))
	and	c.ie_tipo_item = '5';

cursor c07 (	nr_seq_item_mens_pc	pls_mensalidade_seg_item.nr_sequencia%type) is
	select	nr_sequencia nr_seq_item_mens_aprop
	from	pls_mens_seg_item_aprop
	where	nr_seq_item = nr_seq_item_mens_pc;

begin

cd_estabelecimento_w	:= cd_estabelecimento_p;
nm_usuario_w		:= nm_usuario_p;

update	pls_segurado_preco
set	ie_situacao			= 'I'
where	nr_seq_regra_susp_reaj_fx	is not null
and	nr_seq_lote_mens		= nr_seq_lote_p
and	cd_motivo_reajuste		= 'E'
and	ie_situacao			= 'A';
	
for r_c06_w in c06 (nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	update	pls_segurado_preco
	set	ie_situacao			= 'A',
		nr_seq_regra_susp_reaj_fx	= r_c06_w.nr_seq_regra_susp_reaj_fx,
		ds_observacao			= null
	where	nr_seq_segurado			= r_c06_w.nr_seq_segurado
	and	cd_motivo_reajuste		= 'SF'
	and	dt_reajuste			= add_months(r_c06_w.dt_reajuste,1);
	end;
end loop;

begin
	select	ie_mens_adaptacao_retro
	into	ie_mens_adaptacao_retro_w
	from	pls_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;
exception
when others then
	ie_mens_adaptacao_retro_w := 'N';
end;

if	(ie_mens_adaptacao_retro_w = 'S') and
	(pls_mens_itens_pck.obter_se_item_grupo('40',null) = 'S') then
	--Gerar taxa de adaptacao retroativa
	for r_c03_w in C03(nr_seq_lote_p, nr_seq_mensalidade_p) loop
		begin
		if	(add_months(trunc(r_c03_w.dt_adaptacao,'month'),3) >= r_c03_w.dt_mesano_referencia) then
			dt_auxiliar_w	:= add_months(r_c03_w.dt_mesano_referencia,-1);
			
			while	(dt_auxiliar_w >= trunc(r_c03_w.dt_adaptacao,'month')) loop
				begin
				select	count(1)
				into	qt_cobranca_adap_w
				from	dual
				where	exists (select	1
						from	pls_mensalidade_seg_item a,
							pls_segurado		d,
							pls_mensalidade_segurado b,
							pls_mensalidade		c
						where	a.nr_seq_mensalidade_seg = b.nr_sequencia
						and	b.nr_seq_segurado	= d.nr_sequencia
						and	b.nr_seq_mensalidade	= c.nr_sequencia
						and	c.ie_cancelamento is null
						and	d.nr_sequencia		= r_c03_w.nr_seq_segurado
						and	b.dt_mesano_referencia	= dt_auxiliar_w
						and	a.ie_tipo_item		= '31');
				
				if	(qt_cobranca_adap_w = 0) then
					select	max(a.nr_sequencia)
					into	nr_seq_item_pre_w
					from	pls_mensalidade_seg_item a,
						pls_segurado		d,
						pls_mensalidade_segurado b,
						pls_mensalidade		c
					where	a.nr_seq_mensalidade_seg = b.nr_sequencia
					and	b.nr_seq_segurado	= d.nr_sequencia
					and	b.nr_seq_mensalidade	= c.nr_sequencia
					and	c.ie_cancelamento is null
					and	d.nr_sequencia		= r_c03_w.nr_seq_segurado
					and	b.dt_mesano_referencia	= dt_auxiliar_w
					and	a.ie_tipo_item		= '1';
					
					if	(nr_seq_item_pre_w is not null) then
						select	b.dt_inicio_cobertura,
							b.dt_fim_cobertura,
							a.vl_item
						into	dt_inicio_cobertura_w,
							dt_fim_cobertura_w,
							vl_item_w
						from	pls_mensalidade_seg_item a,
							pls_mensalidade_segurado b
						where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
						and	a.nr_sequencia	= nr_seq_item_pre_w;
						
						if	(r_c03_w.dt_adaptacao between dt_inicio_cobertura_w and dt_fim_cobertura_w) then
							vl_adaptacao_w	:= (vl_item_w * r_c03_w.tx_adaptacao) / 100; --Preca calcular o valor proporcional referente ao valor que foi gerado de mensalidade no mes da daptacao
							
							qt_dias_prop_w		:= (trunc(dt_fim_cobertura_w,'dd') - trunc(r_c03_w.dt_adaptacao,'dd')) + 1;
							if	(pls_mensalidade_util_pck.get_ie_data_base_proporcional = 'T') then
								vl_adaptacao_w		:= (vl_adaptacao_w * ((qt_dias_prop_w * 100) / 30)) / 100;
							else
								qt_dias_cobertura_w	:= (trunc(dt_fim_cobertura_w,'dd') - trunc(dt_inicio_cobertura_w,'dd')) + 1;
								vl_adaptacao_w		:= (vl_adaptacao_w * ((qt_dias_prop_w * 100) / qt_dias_cobertura_w)) / 100;
							end if;
							ds_observacao_w		:= wheb_mensagem_pck.get_texto(1109035,'DT_ADAPTACAO='||to_char(r_c03_w.dt_adaptacao,'dd/mm/yyyy')||';DT_FIM_COBERTURA='||to_char(dt_fim_cobertura_w,'dd/mm/yyyy')||';VL_ADAPTACAO='||vl_adaptacao_w);
						else
							vl_adaptacao_w	:= r_c03_w.vl_item;
							ds_observacao_w	:= wheb_mensagem_pck.get_texto(1109034, 'DT_AUXILIAR='||dt_auxiliar_w||';VL_ADAPTACAO='||vl_adaptacao_w);
						end if;
						
						insert	into	pls_mensalidade_seg_item
							(	nr_sequencia, nr_seq_mensalidade_seg, nr_seq_lote,
								dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
								ie_tipo_item, vl_item, ie_tipo_mensalidade, ds_observacao,
								nr_seq_plano, vl_sca_embutido, dt_inicio_cobertura, dt_fim_cobertura)
							values(	pls_mensalidade_seg_item_seq.nextval, r_c03_w.nr_seq_mensalidade_seg, nr_seq_lote_p,
								sysdate, sysdate, nm_usuario_w, nm_usuario_w,
								'40', vl_adaptacao_w, 'N', ds_observacao_w,
								r_c03_w.nr_seq_plano, 0, dt_inicio_cobertura_w, dt_fim_cobertura_w);
					end if;
					dt_auxiliar_w	:= add_months(dt_auxiliar_w,-1);
				else
					dt_auxiliar_w	:= r_c03_w.dt_adaptacao-1; --Coloca data anterior a adatacao para sair do while se encontrou taxa de adaptacao cobrada
				end if;
				end;
			end loop;
			
		end if;
		end;
	end loop; --C03

end if;

--Gerar bonificacao
if	(pls_mens_itens_pck.obter_se_item_grupo('14',null) = 'S') then
	pls_mens_gravar_log_geracao(nr_seq_lote_p, '8', 'I', nm_usuario_p, nr_seq_log_w);
	tb_nr_seq_mensalidade_seg_w.delete;
	inserir_bonificacao(nr_seq_lote_p, nm_usuario_p);
	for r_c02_w in C02(nr_seq_lote_p, nr_seq_mensalidade_p) loop
		begin
		pls_mens_item_bonificacao
			(	r_c02_w.nr_seq_mensalidade_seg,
				r_c02_w.dt_mesano_referencia,
				r_c02_w.nr_seq_segurado,
				r_c02_w.qt_idade,
				r_c02_w.nr_parcela,
				r_c02_w.nr_parcela_contrato,
				r_c02_w.dt_inicio_cobertura,
				r_c02_w.dt_fim_cobertura,
				nr_seq_lote_p,
				r_c02_w.nr_seq_mensalidade,
				r_c02_w.nr_seq_pagador,
				r_c02_w.nr_seq_forma_cobranca,
				r_c02_w.nr_seq_titular,
				r_c02_w.nr_seq_contrato,
				r_c02_w.nr_seq_intercambio,
				r_c02_w.nr_seq_parentesco,
				r_c02_w.ie_acao_contrato,
				r_c02_w.cd_pessoa_fisica,
				r_c02_w.ie_situacao_trabalhista,
				r_c02_w.nr_seq_segurado_preco,
				r_c02_w.ie_remido,
				nm_usuario_p,
				cd_estabelecimento_p);
		
		if	(nr_indice_bonificacao_w >= pls_util_pck.qt_registro_transacao_w) then
			inserir_bonificacao(nr_seq_lote_p, nm_usuario_p);
		end if;
		end;
	end loop; --C02

	--Inserir os itens pendentes
	inserir_bonificacao(nr_seq_lote_p, nm_usuario_p);
	pls_mens_gravar_log_geracao(nr_seq_lote_p, '8', 'F', nm_usuario_p, nr_seq_log_w);
end if;

--Gerar itens do pagador
pls_mens_gravar_log_geracao(nr_seq_lote_p, '7', 'I', nm_usuario_p, nr_seq_log_w);
for r_c01_w in C01(nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	
	--Gerar lancamento manual para o pagador
	pls_mens_item_lanc_prog_pag(nr_seq_lote_p, r_c01_w.nr_seq_mensalidade, r_c01_w.dt_referencia, r_c01_w.nr_seq_pagador, nm_usuario_p);
	
	--Gerar amortizacao
	if	(pls_mens_itens_pck.obter_se_item_grupo('16',null) = 'S') then
		pls_mens_item_amortizacao(nr_seq_lote_p, r_c01_w.nr_seq_mensalidade, r_c01_w.nr_seq_pagador, r_c01_w.dt_referencia, nm_usuario_p);
	end if;
	
	--Baixar nota de credito
	if	(pls_mens_itens_pck.obter_se_item_grupo('27',null) = 'S') then
		pls_mens_item_nota_credito(nr_seq_lote_p, r_c01_w.nr_seq_mensalidade, r_c01_w.nr_seq_pagador, nm_usuario_p);
	end if;
	
	--Gerar taxa de emissao de boleto
	if	(r_c01_w.ie_taxa_emissao_boleto <> 'N') and
		(pls_mens_itens_pck.obter_se_item_grupo('17',null) = 'S') then
		gerar_taxa_emissao_boleto(nr_seq_lote_p, r_c01_w.nr_seq_mensalidade, r_c01_w.ie_taxa_emissao_boleto, r_c01_w.nr_parcela, r_c01_w.nr_seq_pagador_fin, r_c01_w.dt_referencia);
	end if;
	end;
end loop; --C01
pls_mens_gravar_log_geracao(nr_seq_lote_p, '7', 'F', nm_usuario_p, nr_seq_log_w);

for r_c04_w in c04 (nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	pls_atualizar_saldo_lanc_prog(r_c04_w.nr_seq_lancamento_mens, 'N');
	end;
end loop;

--Consistir regra de valor minimo e geracao de mensagens
pls_mens_gravar_log_geracao(nr_seq_lote_p, '9', 'I', nm_usuario_p, nr_seq_log_w);
for r_c01_w in C01(nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	pls_mensalidade_util_pck.obter_regra_vl_minimo(r_c01_w.nr_seq_contrato, r_c01_w.nr_seq_intercambio, r_c01_w.ie_tipo_estipulante, r_c01_w.dt_referencia_lote,
							nr_seq_regra_vl_minimo_w, vl_minimo_w, vl_minimo_abat_fatura_w);
	if	(nr_seq_regra_vl_minimo_w is not null) then
		if	(vl_minimo_w > 0) then
			tratar_valor_minimo_fatura(r_c01_w.nr_seq_mensalidade, vl_minimo_w);
		elsif	(vl_minimo_abat_fatura_w >= 0) and
			(pls_mens_itens_pck.obter_se_item_grupo('28',null) = 'S') then
			tratar_valor_min_abat_fatura(nr_seq_lote_p, r_c01_w.nr_seq_mensalidade, r_c01_w.nr_seq_pagador, r_c01_w.dt_referencia_lote, vl_minimo_abat_fatura_w);
		end if;
	end if;
	end;
end loop; --C01
pls_mens_gravar_log_geracao(nr_seq_lote_p, '9', 'F', nm_usuario_p, nr_seq_log_w);


--Gerar valores ato cooperado
pls_mens_gravar_log_geracao(nr_seq_lote_p, '80', 'I', nm_usuario_p, nr_seq_log_w);
for r_c01_w in C01(nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	pls_gerar_valor_ato_cooperado(r_c01_w.nr_seq_mensalidade,null);
	end;
end loop; --C01
pls_mens_gravar_log_geracao(nr_seq_lote_p, '80', 'F', nm_usuario_p, nr_seq_log_w);

--Gerar valores pro-rata dia
pls_mens_gravar_log_geracao(nr_seq_lote_p, '85', 'I', nm_usuario_p, nr_seq_log_w);
for r_c01_w in C01(nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	pls_gerar_pro_rata_dia(r_c01_w.nr_seq_mensalidade);
	end;
end loop; --C01
pls_mens_gravar_log_geracao(nr_seq_lote_p, '85', 'F', nm_usuario_p, nr_seq_log_w);

end processar_mensalidade;

procedure gerar_mensagens_mensalidade(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
					nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is

Cursor C01 (	nr_seq_lote_pc		pls_lote_mensalidade.nr_sequencia%type,
		nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_mensalidade,
		a.dt_referencia,
		a.ie_tipo_estipulante,
		a.nr_seq_pagador,
		a.nr_seq_conta_banco,
		c.nr_seq_classif_itens
	from	pls_mensalidade	a,
		pls_lote_mensalidade b,
		pls_contrato_pagador c
	where	b.nr_sequencia	= a.nr_seq_lote
	and	c.nr_sequencia	= a.nr_seq_pagador
	and	b.nr_sequencia	= nr_seq_lote_pc
	and	((a.nr_sequencia = nr_seq_mensalidade_pc) or (nr_seq_mensalidade_pc is null));

begin

for r_c01_w in C01(nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	pls_mens_gerar_mensagem(r_c01_w.nr_seq_mensalidade, r_c01_w.nr_seq_pagador, r_c01_w.dt_referencia, r_c01_w.ie_tipo_estipulante, r_c01_w.nr_seq_conta_banco, r_c01_w.nr_seq_classif_itens, cd_estabelecimento_p, nm_usuario_p);
	end;
end loop;

end gerar_mensagens_mensalidade;

procedure suspender_reajuste_fx_etaria(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
					nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
					dt_mesano_referencia_p	pls_lote_mensalidade.dt_mesano_referencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is

qt_reaj_fx_etaria_w		pls_integer;
nr_seq_seg_preco_w		pls_segurado_preco.nr_sequencia%type;

Cursor C01 (	nr_seq_lote_pc		pls_lote_mensalidade.nr_sequencia%type,
		nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_mensalidade_seg,
		b.nr_seq_segurado,
		b.dt_mesano_referencia,
		b.nr_seq_segurado_preco,
		b.nr_seq_plano,
		c.ie_regulamentacao,
		d.nr_sequencia nr_seq_regra_susp_reaj_fx,
		trunc(d.dt_inicio_susp_aplicado,'mm') dt_inicio_susp_aplicado, 
		fim_mes(d.dt_fim_susp_aplicado) dt_fim_susp_aplicado
	from	pls_mensalidade	a,
		pls_mensalidade_segurado b,
		pls_plano	c,
		pls_susp_reaj_fx_etaria d
	where	a.nr_sequencia	= b.nr_seq_mensalidade
	and	c.nr_sequencia	= b.nr_seq_plano
	and	a.nr_seq_lote	= nr_seq_lote_pc
	and	d.nr_sequencia	= b.nr_seq_regra_susp_reaj_fx
	and	d.ie_susp_reaj_aplicado = 'S'
	and	((a.nr_sequencia = nr_seq_mensalidade_pc) or (nr_seq_mensalidade_pc is null))
	order by b.dt_mesano_referencia;

begin

for r_c01_w in C01(nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	select	count(1)
	into	qt_reaj_fx_etaria_w
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_mensalidade c
	where	b.nr_sequencia = a.nr_seq_mensalidade_seg
	and	c.nr_sequencia = b.nr_seq_mensalidade
	and	b.nr_seq_segurado = r_c01_w.nr_seq_segurado
	and	b.nr_seq_plano	= r_c01_w.nr_seq_plano
	and	c.ie_cancelamento is null
	and	a.ie_tipo_item = '5'
	and	b.dt_mesano_referencia between r_c01_w.dt_inicio_susp_aplicado and r_c01_w.dt_fim_susp_aplicado
	and	a.nr_seq_item_estorno is null
	and	not exists (	select	1
				from	pls_mensalidade_seg_item x
				where	x.nr_seq_item_estorno = a.nr_sequencia)
	and	not exists (	select	1 --Somente se ainda nao gerou preco pre no mes
				from	pls_mensalidade_seg_item x,
					pls_mensalidade_segurado y,
					pls_mensalidade z
				where	y.nr_sequencia	= x.nr_seq_mensalidade_seg
				and	z.nr_sequencia	= y.nr_seq_mensalidade
				and	y.nr_seq_segurado = r_c01_w.nr_seq_segurado
				and	z.ie_cancelamento is null
				and	y.dt_mesano_referencia = r_c01_w.dt_mesano_referencia
				and	x.ie_tipo_item = '1');

	if	(qt_reaj_fx_etaria_w > 0) then
		pls_preco_beneficiario_pck.suspender_faixa_etaria_benef(r_c01_w.nr_seq_segurado,r_C01_w.dt_mesano_referencia,r_C01_w.dt_mesano_referencia,r_c01_w.nr_seq_regra_susp_reaj_fx,nr_seq_lote_p,nm_usuario_p,cd_estabelecimento_p);
		
		nr_seq_seg_preco_w	:= pls_mens_beneficiario_pck.get_segurado_preco(r_c01_w.nr_seq_segurado, r_c01_w.dt_mesano_referencia, r_c01_w.ie_regulamentacao);
		
		if	(nr_seq_seg_preco_w <> r_c01_w.nr_seq_segurado_preco) then
			update	pls_mensalidade_segurado
			set	nr_seq_segurado_preco	= nr_seq_seg_preco_w
			where	nr_sequencia		= r_c01_w.nr_seq_mensalidade_seg;
		end if;
	end if;
	end;
end loop;
commit;
end suspender_reajuste_fx_etaria;

procedure processar_tributos(	nr_seq_lote_p		pls_lote_mensalidade.nr_sequencia%type,
				nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

vl_tributo_w			number(15,2);
vl_diferenca_w			number(15,2);
nr_seq_nota_credito_w		nota_credito.nr_sequencia%type;
vl_soma_total_nc_w		number(15,2);
qt_beneficiario_w		number(10);
vl_nota_credito_benef_w		number(15,2);
ie_possui_nc_tributacao_w	varchar2(2);
nr_seq_mensalidade_seg_item_w	pls_mensalidade_seg_item.nr_sequencia%type;
vl_diferenca_ww			number(15,2);
ie_atualizar_mens_nc_w		varchar2(1) := 'N';
		
Cursor C01 (	nr_seq_lote_pc		pls_lote_mensalidade.nr_sequencia%type,
		nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_mensalidade,
		a.dt_referencia,
		c.nr_seq_contrato,
		c.nr_seq_pagador_intercambio nr_seq_intercambio,
		c.cd_cgc,
		c.cd_pessoa_fisica,
		(select	sum(x.vl_item)
		from	pls_mensalidade_seg_item x,
			pls_mensalidade_segurado y
		where	y.nr_sequencia = x.nr_seq_mensalidade_seg
		and	y.nr_seq_mensalidade = a.nr_sequencia) vl_mensalidade
	from	pls_mensalidade	a,
		pls_lote_mensalidade b,
		pls_contrato_pagador c
	where	b.nr_sequencia	= a.nr_seq_lote
	and	c.nr_sequencia	= a.nr_seq_pagador
	and	b.nr_sequencia	= nr_seq_lote_pc
	and	((a.nr_sequencia = nr_seq_mensalidade_pc) or (nr_seq_mensalidade_pc is null));

Cursor C05 (	nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_mensalidade_seg
	from	pls_mensalidade_segurado a
	where	a.nr_seq_mensalidade = nr_seq_mensalidade_pc
	and	(	nvl( (	select	sum(x.vl_item)
				from	pls_mensalidade_seg_item x
				where	a.nr_sequencia = x.nr_seq_mensalidade_seg),0) > 0);
	
begin

for r_c01_w in C01(nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	pls_mens_tributos_pck.gerar_tributos(r_c01_w.nr_seq_mensalidade, r_c01_w.dt_referencia, r_c01_w.cd_cgc, r_c01_w.cd_pessoa_fisica,
				r_c01_w.nr_seq_contrato, r_c01_w.nr_seq_intercambio, cd_estabelecimento_p, nm_usuario_p);
	end;
end loop; --C01

exec_sql_dinamico(nm_usuario_p, 'truncate table pls_mensalidade_seg_tmp');
exec_sql_dinamico(nm_usuario_p, 'truncate table pls_mensalidade_tmp');

for r_c01_w in c01 (nr_seq_lote_p, nr_seq_mensalidade_p) loop
	begin
	
	select	nvl(sum(d.vl_tributo), 0)
	into	vl_tributo_w
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_mensalidade c,
		pls_mensalidade_trib d
	where	b.nr_sequencia = a.nr_seq_mensalidade_seg
	and	c.nr_sequencia = b.nr_seq_mensalidade
	and	a.nr_sequencia = d.nr_seq_item_mens
	and	c.nr_sequencia = r_c01_w.nr_seq_mensalidade
	and	(d.ie_retencao = 'D' or (d.ie_retencao = 'N' and d.ie_origem_tributo = 'CD'));
	
	vl_soma_total_nc_w	:= 0;
	vl_nota_credito_benef_w := 0;
	
	if	(vl_tributo_w > r_c01_w.vl_mensalidade) then
		select	max(a.nr_seq_nota_credito)
		into	nr_seq_nota_credito_w
		from	pls_mensalidade_seg_item a,
			pls_mensalidade_segurado b,
			pls_mensalidade c
		where	c.nr_sequencia = b.nr_seq_mensalidade
		and	b.nr_sequencia = a.nr_seq_mensalidade_seg
		and	c.nr_sequencia = r_c01_w.nr_seq_mensalidade
		and	a.nr_seq_nota_credito is not null;
		
		if	(nr_seq_nota_credito_w is not null) then
			vl_diferenca_w	:= vl_tributo_w - r_c01_w.vl_mensalidade;
			
			ie_possui_nc_tributacao_w := 'N';
			
			select	count(1)
			into	qt_beneficiario_w
			from	pls_mensalidade_segurado a
			where	a.nr_seq_mensalidade = r_c01_w.nr_seq_mensalidade
			and	(	nvl( (	select	sum(x.vl_item)
						from	pls_mensalidade_seg_item x
						where	a.nr_sequencia = x.nr_seq_mensalidade_seg),0) > 0);

			for r_c05_w in c05 (r_c01_w.nr_seq_mensalidade) loop
				begin
				
				ie_possui_nc_tributacao_w := 'S';
				ie_atualizar_mens_nc_w	:= 'S';
				
				vl_nota_credito_benef_w	:= dividir_sem_round(vl_diferenca_w, qt_beneficiario_w);
				
				insert	into	pls_mensalidade_seg_item
					(	nr_sequencia, nr_seq_mensalidade_seg, ie_tipo_item, vl_item, ie_tipo_mensalidade,
						nm_usuario, nm_usuario_nrec, dt_atualizacao, dt_atualizacao_nrec,
						nr_seq_nota_credito, nr_seq_lote, vl_ato_nao_coop_pro_rata, vl_antecipacao)
				values	(	pls_mensalidade_seg_item_seq.nextval, r_c05_w.nr_seq_mensalidade_seg, '27', vl_nota_credito_benef_w, 'P',
						nm_usuario_p, nm_usuario_p, sysdate, sysdate,
						nr_seq_nota_credito_w, nr_seq_lote_p, vl_nota_credito_benef_w, 0)
					returning nr_sequencia into nr_seq_mensalidade_seg_item_w;
				
				insert into pls_mensalidade_seg_tmp values (r_c05_w.nr_seq_mensalidade_seg);
				
				vl_soma_total_nc_w := vl_soma_total_nc_w + vl_nota_credito_benef_w;
				
				end;
			end loop;
			
			if	(ie_possui_nc_tributacao_w = 'S') then
				vl_diferenca_ww := 0;
				
				if	(vl_soma_total_nc_w <> vl_diferenca_w) then
				
					vl_diferenca_ww	:= vl_soma_total_nc_w - vl_diferenca_w;
				
					update	pls_mensalidade_seg_item
					set	vl_item = vl_item - vl_diferenca_ww
					where	nr_sequencia = nr_seq_mensalidade_seg_item_w;
				end if;
				
				insert into pls_mensalidade_tmp values (r_c01_w.nr_seq_mensalidade);
				
				atualizar_saldo_nota_credito(nr_seq_nota_credito_w, nm_usuario_p);
			end if;
		end if;
	end if;
	end;
end loop;

if	(ie_atualizar_mens_nc_w = 'S') then
	pls_mens_atualizacao_pck.atualizar_valores_lote(nr_seq_lote_p, nm_usuario_p, cd_estabelecimento_p, 'NC');
end if;

end processar_tributos;

end pls_mens_processo_pck;
/