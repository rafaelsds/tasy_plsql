CREATE OR REPLACE package intpd_consulta_atendimento_pck as

	/*Atributos do elemento do XML*/
	type r_atendimento_row is record(nr_atendimento number(10),
					dt_alta date,
					nm_paciente varchar2(255));

	type t_atendimento is table of r_atendimento_row;

	function intpd_consulta_atendimento(nr_seq_fila_p number) return t_atendimento pipelined;

end intpd_consulta_atendimento_pck;
/

create or replace package body intpd_consulta_atendimento_pck as

function intpd_consulta_atendimento(nr_seq_fila_p number)
return t_atendimento pipelined is

r_atendimento_w 	r_atendimento_row;
xml_w			xmltype;
query_w			long;

cursor c01 is
select cd_estabelecimento, cd_setor_atendimento, cd_unidade_basica, cd_unidade_compl
from   xmltable('/STRUCTURE/FILTERS' passing xml_w COLUMNS
		cd_estabelecimento 	varchar2(40) 	path 	'CD_ESTABLISHMENT',
		cd_setor_atendimento 	varchar2(40) 	path 	'CD_CARE_DEPARTMENT',
		cd_unidade_basica 	varchar2(10) 	path 	'CD_ROOM',
		cd_unidade_compl 	varchar2(10) 	path 	'CD_COMPLIMENTARY_UNIT');
c01_w	c01%rowtype;

cursor c02 is
select	a.nr_atendimento,
	a.dt_alta,
	substr(get_person_name_estab(a.cd_pessoa_fisica,null,c01_w.cd_estabelecimento) ,1,255) nm_paciente
from	atendimento_paciente a,
	unidade_atendimento b,
	unidade_atend_hist c
where	a.nr_atendimento = c.nr_atendimento
and	b.nr_seq_interno = c.nr_seq_unidade
and	b.ie_status_unidade in ('P', 'O', 'A')
and	a.cd_estabelecimento = c01_w.cd_estabelecimento
and	((b.cd_setor_atendimento = c01_w.cd_setor_atendimento) or (c01_w.cd_setor_atendimento is null))
and 	b.cd_unidade_basica = c01_w.cd_unidade_basica
and 	((b.cd_unidade_compl = c01_w.cd_unidade_compl) or (c01_w.cd_unidade_compl is null))
and	c.nr_sequencia       =
		(select max(x.nr_sequencia)
		from unidade_atend_hist x
		where x.nr_seq_unidade = c.nr_seq_unidade)
and rownum <= 1;
c02_w	c02%rowtype;

begin
if	(nvl(nr_seq_fila_p,0) > 0) then
	intpd_inicializacao(nr_seq_fila_p);

	select xmltype.createxml(ds_xml)
	into   xml_w
	from   intpd_fila_transmissao
	where  nr_sequencia = nr_seq_fila_p;

	open c01;
	loop
	fetch c01 into
		c01_w;
	exit when c01%notfound;
		begin

		open c02;
		loop
		fetch c02 into
			c02_w;
		exit when c02%notfound;
			begin

			r_atendimento_w.nr_atendimento	:= c02_w.nr_atendimento;
			r_atendimento_w.dt_alta		:= c02_w.dt_alta;
			r_atendimento_w.nm_paciente	:= c02_w.nm_paciente;

			pipe row(r_atendimento_w);

			end;
		end loop;
		close c02;
		end;
	end loop;
	close c01;
end if;

end intpd_consulta_atendimento;

end intpd_consulta_atendimento_pck;
/