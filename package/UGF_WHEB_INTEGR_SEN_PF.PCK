create or replace
package UGF_WHEB_INTEGR_SEN_PF is

procedure inserir_pessoa_fisica(	cd_pessoa_orig_p		varchar2,
				nm_pessoa_fisica_p	varchar2,
				dt_nascimento_p		date,
				ie_sexo_p		varchar2,
				nr_cpf_p			varchar2,
				nr_identidade_p		varchar2,
				ie_tipo_complemento_p	number,
				ds_endereco_p		varchar2,
				nr_endereco_p		varchar2,
				ds_complemento_p		varchar2,
				ds_bairro_p		varchar2,
				ds_municipio_p		varchar2,
				cd_cep_p		varchar2,
				cd_municipio_ibge_p	varchar2,
				nr_ddd_telefone_p		varchar2,
				nr_telefone_p		varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p		out	varchar2,
				cd_pessoa_fisica_p in out	varchar2,
				cd_banco_p		number,
				cd_agencia_bancaria_p	varchar2,
				nr_conta_p		varchar2,
				ie_situacao_p		varchar2,
				nr_digito_conta_p		varchar2,
				ie_digito_agencia_p		varchar2,
				ie_tipo_conta_p		varchar2,
				ie_conta_pagamento_p	varchar2,
				ie_propriedade_conta_p	varchar2,
				ds_observacao_p		varchar2,
				nr_ddd_celular_p	varchar2,
				nr_telefone_celular_p	varchar2);
				
procedure inserir_compl_pessoa(	cd_pessoa_fisica_p		varchar2,
				ie_tipo_complemento_p	number,
				ds_endereco_p		varchar2,
				nr_endereco_p		varchar2,
				ds_complemento_p		varchar2,
				ds_bairro_p		varchar2,
				ds_municipio_p		varchar2,
				cd_cep_p		varchar2,
				cd_municipio_ibge_p	varchar2,
				nr_ddd_telefone_p		varchar2,
				nr_telefone_p		varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p		out	varchar2);
				
				
procedure inserir_conta_bancaria(	cd_pessoa_fisica_p		varchar2,
				cd_banco_p		number,
				cd_agencia_bancaria_p	varchar2,
				nr_conta_p		varchar2,
				ie_situacao_p		varchar2,
				nr_digito_conta_p		varchar2,
				ie_digito_agencia_p		varchar2,
				ie_tipo_conta_p		varchar2,
				ie_conta_pagamento_p	varchar2,
				ie_propriedade_conta_p	varchar2,
				ds_observacao_p		varchar2,
				ds_erro_p		out	varchar2);

end UGF_WHEB_INTEGR_SEN_PF;
/
create or replace
package body UGF_WHEB_INTEGR_SEN_PF is

	procedure inserir_compl_pessoa(	cd_pessoa_fisica_p		varchar2,
					ie_tipo_complemento_p	number,
					ds_endereco_p		varchar2,
					nr_endereco_p		varchar2,
					ds_complemento_p		varchar2,
					ds_bairro_p		varchar2,
					ds_municipio_p		varchar2,
					cd_cep_p		varchar2,
					cd_municipio_ibge_p	varchar2,
					nr_ddd_telefone_p		varchar2,
					nr_telefone_p		varchar2,
					nm_usuario_p		varchar2,
					ds_erro_p	out		varchar2) is
	
	nr_sequencia_w			number(10);
	ds_erro_w			varchar2(255);
		
	begin
		select	nvl(max(nr_sequencia),0)
		into	nr_sequencia_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica		= cd_pessoa_fisica_p
		and	ie_tipo_complemento	= ie_tipo_complemento_p;

		if	(nr_sequencia_w = 0) then
		
			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
			
			nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
			
			begin
			insert into compl_pessoa_fisica(
				cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				ds_endereco,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				cd_cep,
				nr_ddd_telefone,
				nr_telefone,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				cd_municipio_ibge)
			values(	cd_pessoa_fisica_p,
				nr_sequencia_w,
				ie_tipo_complemento_p,
				ds_endereco_p,
				decode(substr(obter_se_somente_numero(nr_endereco_p),1,1),'S',nr_endereco_p,'0'),
				substr(ds_complemento_p || decode(substr(obter_se_somente_numero(nr_endereco_p),1,1),'S','', ' ' || nr_endereco_p),1,40),
				ds_bairro_p,
				ds_municipio_p,
				substr(cd_cep_p,1,15),
				nr_ddd_telefone_p,
				substr(nr_telefone_p,1,15),
				nm_usuario_p,
				sysdate,
				'IntegrSenior',
				sysdate,
				cd_municipio_ibge_p);
			exception when others then
				ds_erro_w	:= substr('Erro ao inserir o complemento da Pessoa: ' || cd_pessoa_fisica_p || chr(13) || sqlerrm(sqlcode),1,255);
				ds_erro_p	:= ds_erro_w;
			end;
		else
			begin
			update compl_pessoa_fisica
			set	dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				ds_endereco		= ds_endereco_p,
				nr_endereco		= decode(substr(obter_se_somente_numero(nr_endereco_p),1,1),'S',nr_endereco_p,'0'),
				ds_complemento		= substr(ds_complemento_p || decode(substr(obter_se_somente_numero(nr_endereco_p),1,1),'S','',' ' || nr_endereco_p),1,40),
				ds_bairro			= ds_bairro_p,
				ds_municipio		= ds_municipio_p,
				nr_ddd_telefone		= nr_ddd_telefone_p,
				nr_telefone		= nr_telefone_p,
				cd_cep			= cd_cep_p,
				cd_municipio_ibge		= cd_municipio_ibge_p
			where	cd_pessoa_fisica		= cd_pessoa_fisica_p
			and	nr_sequencia		= nr_sequencia_w;
			exception when others then
				ds_erro_w	:= substr('Erro ao atualizar o complemento da Pessoa: ' || cd_pessoa_fisica_p || '/' || nr_sequencia_w || chr(13) || sqlerrm(sqlcode),1,255);
				ds_erro_p	:= ds_erro_w;
			end;
		end if;
		
		/*commit;*/
		
	end;
	
	procedure inserir_conta_bancaria(	cd_pessoa_fisica_p		varchar2,
					cd_banco_p		number,
					cd_agencia_bancaria_p	varchar2,
					nr_conta_p		varchar2,
					ie_situacao_p		varchar2,
					nr_digito_conta_p		varchar2,
					ie_digito_agencia_p		varchar2,
					ie_tipo_conta_p		varchar2,
					ie_conta_pagamento_p	varchar2,
					ie_propriedade_conta_p	varchar2,
					ds_observacao_p		varchar2,
					ds_erro_p	out		varchar2) is

	
	qt_existe_w			number(10);
	ds_erro_w			varchar2(255);
	
	begin
		
	if	(nvl(nr_conta_p,'X') <> 'X') then
		update	pessoa_fisica_conta
		set	ie_conta_pagamento = 'N'
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
		
		select	count(*)
		into	qt_existe_w
		from	pessoa_fisica_conta
		where	cd_pessoa_fisica		= cd_pessoa_fisica_p
		and	cd_banco		= cd_banco_p
		and	cd_agencia_bancaria	= substr(cd_agencia_bancaria_p,1,8)
		and	nr_conta			= substr(nr_conta_p,1,20);

		if	(qt_existe_w > 0) then
			update	pessoa_fisica_conta
			set	ie_conta_pagamento	= 'S'
			where	cd_pessoa_fisica 	= cd_pessoa_fisica_p
			and	nr_conta		= nr_conta_p;
		else
			begin
			insert into pessoa_fisica_conta(
				cd_pessoa_fisica,
				cd_banco,
				cd_agencia_bancaria,
				nr_conta,
				dt_atualizacao,
				nm_usuario,
				ie_situacao,
				nr_digito_conta,
				ie_digito_agencia,
				ie_tipo_conta,
				ie_conta_pagamento,
				ie_propriedade_conta,
				nr_sequencia,
				ds_observacao)
			values(	cd_pessoa_fisica_p,
				cd_banco_p,
				substr(cd_agencia_bancaria_p,1,8),
				substr(nr_conta_p,1,20),
				sysdate,
				'IntegrSenior',
				ie_situacao_p,
				substr(nr_digito_conta_p,1,2),
				substr(ie_digito_agencia_p,1,1),
				substr(ie_tipo_conta_p,1,3),
				'S',
				substr(ie_propriedade_conta_p,1,3),
				pessoa_fisica_conta_seq.nextval,
				substr(ds_observacao_p,1,4000));
			exception when others then
				ds_erro_w	:= substr('Erro ao inserir a conta banc�ria da Pessoa: ' || cd_pessoa_fisica_p || chr(13) || sqlerrm(sqlcode),1,255);
				ds_erro_p	:= ds_erro_w;
			end;
		end if;
	end if;
	end;

	procedure inserir_pessoa_fisica(	cd_pessoa_orig_p		varchar2,
					nm_pessoa_fisica_p	varchar2,
					dt_nascimento_p		date,
					ie_sexo_p		varchar2,
					nr_cpf_p			varchar2,
					nr_identidade_p		varchar2,
					ie_tipo_complemento_p	number,
					ds_endereco_p		varchar2,
					nr_endereco_p		varchar2,
					ds_complemento_p		varchar2,
					ds_bairro_p		varchar2,
					ds_municipio_p		varchar2,
					cd_cep_p		varchar2,
					cd_municipio_ibge_p	varchar2,
					nr_ddd_telefone_p		varchar2,
					nr_telefone_p		varchar2,
					nm_usuario_p		varchar2,
					ds_erro_p		out	varchar2,
					cd_pessoa_fisica_p in out 	varchar2,
					cd_banco_p		number,
					cd_agencia_bancaria_p	varchar2,
					nr_conta_p		varchar2,
					ie_situacao_p		varchar2,
					nr_digito_conta_p		varchar2,
					ie_digito_agencia_p		varchar2,
					ie_tipo_conta_p		varchar2,
					ie_conta_pagamento_p	varchar2,
					ie_propriedade_conta_p	varchar2,
					ds_observacao_p		varchar2,
					nr_ddd_celular_p	varchar2,
                                        nr_telefone_celular_p	varchar2) is
	nr_sequencia_w		number(10);
	cd_pessoa_fisica_w	varchar2(10);
	ds_erro_W		varchar2(255);
	qt_existe_w		number(10,0);
		
	begin
	ds_erro_p	:= '';
	
	cd_pessoa_fisica_w	:= nvl(cd_pessoa_fisica_p,'0');
	ds_erro_w		:= '';
	
	if	(cd_pessoa_fisica_w <> '0') then
	
		select	nvl(max(cd_pessoa_fisica),'0')
		into	cd_pessoa_fisica_w
		from	pessoa_fisica
		where	1=1 --cd_sistema_ant	= cd_pessoa_orig_p
		and	cd_pessoa_fisica	= cd_pessoa_fisica_p;
		--and	nm_usuario_nrec 	= 'IntegrSenior';
		
		if	(cd_pessoa_fisica_w = '0') then
			ds_erro_w	:= substr('Pessoa f�sica n�o encontrada no Tasy: ' || 'Orig: ' || cd_pessoa_orig_p || ' C�d PF: ' || cd_pessoa_fisica_p || '/' || nm_pessoa_fisica_p,1,255);
		end if;
	end if;
	
	if	(nvl(nr_cpf_p,'X') <> 'X') then
		select	count(*)
		into	qt_existe_w
		from	pessoa_fisica
		where	nr_cpf 		= nr_cpf_p
		and	dt_nascimento 	= dt_nascimento_p;
		
		if	(qt_existe_w > 0) then
			select	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	pessoa_fisica
			where	nr_cpf		= nr_cpf_p
			and	dt_nascimento 	= dt_nascimento_p;
		end if;
	end if;
	
	if	(nvl(cd_pessoa_fisica_w,'0') = '0') then
		begin
		
		select	pessoa_fisica_seq.nextval
		into	cd_pessoa_fisica_w
		from	dual;
		
		insert into pessoa_fisica ( 
			cd_pessoa_fisica,
			ie_tipo_pessoa,
			nm_pessoa_fisica, 
			dt_atualizacao,
			nm_usuario,
			dt_nascimento, 
			ie_sexo,
			nr_cpf,
			nr_identidade, 
			ie_funcionario,
			cd_sistema_ant,
			nm_pessoa_pesquisa, 
			nm_usuario_original,
			dt_cadastro_original,
			dt_atualizacao_nrec, 
			nm_usuario_nrec,
			nm_pessoa_fisica_sem_acento,
			nr_ddd_celular,
			nr_telefone_celular)
		values(	cd_pessoa_fisica_w,
			1,
			nm_pessoa_fisica_p, 
			sysdate,
			nm_usuario_p,
			dt_nascimento_p, 
			ie_sexo_p, 
			nr_cpf_p,
			nr_identidade_p, 
			'S',
			cd_pessoa_orig_p,
			substr(padronizar_nome(nm_pessoa_fisica_p),1,60), 
			nm_usuario_p,
			sysdate,
			sysdate, 
			'IntegrSenior',
			substr(elimina_acentuacao(nm_pessoa_fisica_p),1,60),
			nr_ddd_celular_p,
			nr_telefone_celular_p);
	
		exception when others then
			ds_erro_w	:= sqlerrm(sqlcode);
		end;
	elsif	(nvl(ds_erro_w,'X') = 'X') then
		update	pessoa_fisica
		set	nm_pessoa_fisica		= nm_pessoa_fisica_p,
			dt_nascimento		= dt_nascimento_p,
			ie_sexo			= ie_sexo_p,
			nr_cpf			= nr_cpf_p,
			nr_identidade		= nr_identidade_p,
			nr_ddd_celular		= nr_ddd_celular_p,
			nr_telefone_celular		= nr_telefone_celular_p,
			nm_usuario		= nm_usuario_p,
			ie_funcionario		= 'S',
			dt_atualizacao		= sysdate
		where	cd_pessoa_fisica		= cd_pessoa_fisica_w;
		
	end if;
	
	if	(nvl(ds_erro_w,'X') = 'X') and
		(cd_pessoa_fisica_w is not null) then
		begin
		
		inserir_compl_pessoa(	cd_pessoa_fisica_w,
					ie_tipo_complemento_p,
					ds_endereco_p,
					nr_endereco_p,
					ds_complemento_p,
					ds_bairro_p,
					ds_municipio_p,
					cd_cep_p,
					cd_municipio_ibge_p,
					nr_ddd_telefone_p,
					nr_telefone_p,
					nm_usuario_p,
					ds_erro_w);
		end;
	end if;
	
	if	(nvl(ds_erro_w,'X') = 'X') and
		(cd_pessoa_fisica_w is not null) then
		begin
		
		inserir_conta_bancaria(	cd_pessoa_fisica_w,
					cd_banco_p,
					cd_agencia_bancaria_p,
					nr_conta_p,
					ie_situacao_p,
					nr_digito_conta_p,
					ie_digito_agencia_p,
					ie_tipo_conta_p,
					ie_conta_pagamento_p,
					ie_propriedade_conta_p,
					ds_observacao_p,
					ds_erro_w);
		end;
	end if;
	
	ds_erro_p		:= ds_erro_w;
	if	(nvl(ds_erro_w,'X') = 'X') then
		cd_pessoa_fisica_p	:= cd_pessoa_fisica_w;
	end if;
	end;
	
	
end UGF_WHEB_INTEGR_SEN_PF;
/