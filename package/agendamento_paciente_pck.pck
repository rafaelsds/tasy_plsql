CREATE OR REPLACE PACKAGE agendamento_paciente_pck AS

    TYPE agendamento_paciente_row IS RECORD(
        ds_tipo_agenda            VARCHAR2(2000),
        ds_agenda                 VARCHAR2(2000),
        dt_ordenacao              DATE,
        cd_tipo_agenda            NUMBER,
        ds_item                   VARCHAR2(2000),
        ds_convenio               VARCHAR2(2000),
        nr_minuto_duracao         NUMBER,
        nr_atendimento            NUMBER,
        ds_idade                  VARCHAR2(2000),
        nr_telefone               VARCHAR2(2000),
        ds_plano                  VARCHAR2(2000),
        ds_categoria              VARCHAR2(2000),
        cd_usuario_convenio       VARCHAR2(2000),
        nr_seq_exame              NUMBER,
        nr_seq_cons               NUMBER,
        nr_seq_radio              NUMBER,
        nr_seq_quimio             NUMBER,
        ds_status                 VARCHAR2(2000),
        cd_convenio_exame         VARCHAR2(2000),
        cd_convenio_consulta      VARCHAR2(2000),
        cd_pessoa_fisica_exame    VARCHAR2(2000),
        cd_pessoa_fisica_consulta VARCHAR2(2000),
        dt_agenda_exame           DATE,
        dt_agenda_consulta        DATE,
        ie_status_agenda_exame    VARCHAR2(5),
        ie_status_agenda_consulta VARCHAR2(5),
        ds_obs_final              VARCHAR2(2000),
        ie_encaixe                VARCHAR2(5),
        nr_seq_ageint             NUMBER,
        cd_estabelecimento        VARCHAR2(2000),
        nr_seq_agenda             NUMBER,
        nr_seq_item               NUMBER,
        nr_reserva                VARCHAR2(2000),
        ie_duplicado_estab        VARCHAR2(5),
        nr_seq_ageint_item        NUMBER,
        nr_seq_lab                NUMBER,
        ds_observacao             VARCHAR2(2000),
        nm_usuario_origem         VARCHAR2(2000),
        dt_resultado              DATE,
        dt_entrega_prevista       DATE,
        ie_transferido            VARCHAR2(5),
        nr_seq_mot_reagendamento  NUMBER,
        ds_classificacao          VARCHAR2(2000),
		nr_secao_consulta         NUMBER);

    TYPE agendamento_paciente_tab IS TABLE OF agendamento_paciente_row;

    FUNCTION get_dados(cd_paciente_p    VARCHAR2,
                       dt_referencia_p  DATE,
                       cd_protocolo_p   NUMBER,
                       cd_convenio_p    NUMBER,
                       nr_reserva_pac_p VARCHAR2,
                       ie_forma_apres_p VARCHAR2) RETURN agendamento_paciente_tab
    
        PIPELINED;

END;
/
CREATE OR REPLACE PACKAGE BODY agendamento_paciente_pck AS
    FUNCTION get_dados(cd_paciente_p    VARCHAR2,
                       dt_referencia_p  DATE,
                       cd_protocolo_p   NUMBER,
                       cd_convenio_p    NUMBER,
                       nr_reserva_pac_p VARCHAR2,
                       ie_forma_apres_p VARCHAR2) RETURN agendamento_paciente_tab
        PIPELINED IS
        agendamento_paciente_w agendamento_paciente_tab := agendamento_paciente_tab();
        index_w                NUMBER := 0;
        c_limit                NUMBER := 500;
        dt_inicio_w            DATE := trunc(dt_referencia_p);
        dt_final_w             DATE := dt_inicio_w + 180;
    
        CURSOR c01 IS
            SELECT decode(a.cd_tipo_agenda, 1, obter_desc_expressao(486364), obter_desc_expressao(289683)) ds_tipo_agenda,
                   a.ds_agenda ds_agenda,
                   b.hr_inicio dt_ordenacao,
                   a.cd_tipo_agenda,
                   substr(obter_desc_prescr_proc(b.cd_procedimento, b.ie_origem_proced, b.nr_seq_proc_interno), 1, 255) ds_item,
                   --SUBSTR(obter_nome_convenio(b.cd_convenio),1,255) ds_convenio,
                   d.ds_convenio ds_convenio,
                   b.nr_minuto_duracao nr_minuto_duracao,
                   b.nr_atendimento nr_atendimento,
                   substr(obter_idade_pac_agenda(a.cd_tipo_agenda, b.nr_sequencia), 1, 30) ds_idade,
                   substr(b.nr_telefone, 1, 60) nr_telefone,
                   substr(obter_desc_plano(b.cd_convenio, b.cd_plano), 1, 255) ds_plano,
                   substr(obter_categoria_convenio(b.cd_convenio, b.cd_categoria), 1, 255) ds_categoria,
                   b.cd_usuario_convenio cd_usuario_convenio,
                   nvl(b.nr_sequencia, 0) nr_seq_exame,
                   0 nr_seq_cons,
                   NULL nr_seq_radio,
                   NULL nr_seq_quimio,
                   --SUBSTR(obter_valor_dominio(83, b.ie_status_agenda),1,110) ds_status,
                   substr(obter_desc_expressao(c.cd_exp_valor_dominio), 1, 110) ds_status,
                   b.cd_convenio cd_convenio_exame,
                   NULL cd_convenio_consulta,
                   b.cd_pessoa_fisica cd_pessoa_fisica_exame,
                   '' cd_pessoa_fisica_consulta,
                   b.dt_agenda dt_agenda_exame,
                   to_date(NULL) dt_agenda_consulta,
                   b.ie_status_agenda ie_status_agenda_exame,
                   '' ie_status_agenda_consulta,
                   substr(ageint_obter_obs_final(b.nr_sequencia, NULL, NULL), 1, 255) ds_obs_final,
                   b.ie_encaixe,
                   e.nr_seq_agenda_int nr_seq_ageint,
                   a.cd_estabelecimento,
                   nvl(b.nr_sequencia, 0) nr_seq_agenda,
                   0 nr_seq_item,
                   b.nr_reserva nr_reserva,
                   nvl(e.ie_duplicado_estab, 'N') ie_duplicado_estab,
                   e.nr_sequencia nr_seq_ageint_item,
                   0 nr_seq_lab,
                   substr(b.ds_observacao, 1, 4000) ds_observacao,
                   b.nm_usuario_orig nm_usuario_origem,
                   e.dt_resultado dt_resultado,
                   e.dt_entrega_prevista dt_entrega_prevista,
                   nvl(b.ie_transferido, 'N') ie_transferido,
                   0 nr_seq_mot_reagendamento,
                   substr(obter_desc_classif_agenda_pac(b.nr_seq_classif_agenda), 1, 255) ds_classificacao,
				   NULL nr_secao_consulta
              FROM agenda_integrada_item e,
                   convenio              d,
                   valor_dominio_v       c,
                   agenda                a,
                   agenda_paciente       b
             WHERE a.cd_agenda = b.cd_agenda
               AND a.cd_tipo_agenda = 2
               AND b.nr_sequencia = e.nr_seq_agenda_exame(+)
               AND c.cd_dominio = 83
               AND c.vl_dominio = b.ie_status_agenda
               AND b.cd_convenio = d.cd_convenio(+)
               ----------------------       
               AND b.cd_pessoa_fisica = cd_paciente_p
               AND b.dt_agenda BETWEEN dt_inicio_w AND dt_final_w
                  
               AND (cd_convenio_p IS NULL OR b.cd_convenio = cd_convenio_p)
               AND (nr_reserva_pac_p IS NULL OR b.nr_reserva = nr_reserva_pac_p)
               AND (cd_protocolo_p IS NULL OR e.nr_seq_agenda_int = cd_protocolo_p)
                  
               AND ((ie_forma_apres_p = '0') OR 
                   ((ie_forma_apres_p = '1') AND (ie_status_agenda <> 'C')) OR
                   ((ie_forma_apres_p = '2') AND (ie_status_agenda = 'C')) OR
                   ((ie_forma_apres_p = '3') AND (cd_tipo_agenda <> 20)));
               -----------
    
        CURSOR c02 IS
        
            SELECT decode(a.cd_tipo_agenda, 5, obter_desc_expressao(298345), obter_desc_expressao(285916)) ds_tipo_agenda,
                   nvl(substr(obter_nome_pf(a.cd_pessoa_fisica), 1, 60),
                       substr(nvl(obter_desc_espec_medica(a.cd_especialidade), a.ds_agenda), 1, 255)) ds_agenda,
                   c.dt_agenda dt_ordenacao,
                   a.cd_tipo_agenda,
                   nvl(substr(obter_nome_pf(a.cd_pessoa_fisica), 1, 60),
                       substr(obter_desc_espec_medica(a.cd_especialidade), 1, 255)) ds_item,
                   --SUBSTR(obter_nome_convenio(c.cd_convenio),1,255) ds_convenio,
                   d.ds_convenio ds_convenio,
                   c.nr_minuto_duracao nr_minuto_duracao,
                   c.nr_atendimento nr_atendimento,
                   substr(obter_idade_pac_agenda(a.cd_tipo_agenda, c.nr_sequencia), 1, 30) ds_idade,
                   substr(c.nr_telefone, 1, 60) nr_telefone,
                   substr(obter_desc_plano(c.cd_convenio, c.cd_plano), 1, 255) ds_plano,
                   substr(obter_categoria_convenio(c.cd_convenio, c.cd_categoria), 1, 255) ds_categoria,
                   c.cd_usuario_convenio cd_usuario_convenio,
                   0 nr_seq_exame,
                   nvl(c.nr_sequencia, 0) nr_seq_cons,
                   NULL nr_seq_radio,
                   NULL nr_seq_quimio,
                   --SUBSTR(obter_valor_dominio(83, c.ie_status_agenda),1,110) ds_status,
                   substr(obter_desc_expressao(b.cd_exp_valor_dominio), 1, 110) ds_status,
                   NULL cd_convenio_exame,
                   c.cd_convenio cd_convenio_consulta,
                   '' cd_pessoa_fisica_exame,
                   c.cd_pessoa_fisica cd_pessoa_fisica_consulta,
                   NULL dt_agenda_exame,
                   c.dt_agenda dt_agenda_consulta,
                   NULL ie_status_agenda_exame,
                   c.ie_status_agenda ie_status_agenda_consulta,
                   substr(ageint_obter_obs_final(NULL, c.nr_sequencia, NULL), 1, 255) ds_obs_final,
                   c.ie_encaixe,
                   e.nr_seq_agenda_int nr_seq_ageint,
                   a.cd_estabelecimento,
                   nvl(c.nr_sequencia, 0) nr_seq_agenda,
                   0 nr_seq_item,
                   c.nr_reserva nr_reserva,
                   nvl(e.ie_duplicado_estab, 'N') ie_duplicado_estab,
                   e.nr_sequencia nr_seq_ageint_item,
                   0 nr_seq_lab,
                   substr(c.ds_observacao, 1, 2000) ds_observacao,
                   c.nm_usuario_origem nm_usuario_origem,
                   e.dt_resultado dt_resultado,
                   e.dt_entrega_prevista dt_entrega_prevista,
                   nvl(c.ie_transferido, 'N') ie_transferido,
                   0 nr_seq_mot_reagendamento,
                   substr(obter_classif_agenda_consulta(c.ie_classif_agenda), 1, 255) ds_classificacao,
				   c.nr_secao nr_secao_consulta
              FROM agenda_integrada_item e,
                   convenio              d,
                   valor_dominio_v       b,
                   agenda                a,
                   agenda_consulta       c
             WHERE a.cd_agenda = c.cd_agenda
               AND c.nr_sequencia = e.nr_seq_agenda_cons(+)
               AND a.cd_tipo_agenda IN (3, 4, 5)
               AND b.cd_dominio = 83
               AND b.vl_dominio = c.ie_status_agenda
               AND c.cd_convenio = d.cd_convenio(+)
                  
               ----------------------       
               AND c.cd_pessoa_fisica = cd_paciente_p
               AND c.dt_agenda BETWEEN dt_inicio_w AND dt_final_w
                  
               AND (cd_convenio_p IS NULL OR c.cd_convenio = cd_convenio_p)
               AND (nr_reserva_pac_p IS NULL OR c.nr_reserva = nr_reserva_pac_p)
               AND (cd_protocolo_p IS NULL OR e.nr_seq_agenda_int = cd_protocolo_p)
                  
               AND ((ie_forma_apres_p = '0') OR 
                   ((ie_forma_apres_p = '1') AND (ie_status_agenda <> 'C')) OR
                   ((ie_forma_apres_p = '2') AND (ie_status_agenda = 'C')) OR
                   ((ie_forma_apres_p = '3') AND (cd_tipo_agenda <> 20)));
               -----------               
    
        CURSOR c03 IS
            SELECT obter_desc_expressao(297224) ds_tipo_agenda,
                   substr(rxt_obter_desc_equipamento(b.nr_seq_equipamento), 1, 250) ds_agenda,
                   b.dt_agenda dt_ordenacao,
                   10 cd_tipo_agenda,
                   substr(rxt_obter_desc_trat_agenda(b.nr_seq_tratamento), 1, 240) ds_item,
                   substr(obter_dados_atendimento(b.nr_atendimento, 'NC'), 1, 255) ds_convenio,
                   b.nr_minuto_duracao,
                   b.nr_atendimento,
                   substr(obter_idade_pf(b.cd_pessoa_fisica, b.dt_agenda, 'A'), 1, 30) ds_idade,
                   substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'T'), 1, 60) nr_telefone,
                   substr(obter_dados_categ_conv(b.nr_atendimento, 'DP'), 1, 255) ds_plano,
                   substr(obter_dados_categ_conv(b.nr_atendimento, 'DC'), 1, 255) ds_categoria,
                   substr(obter_dados_categ_conv(b.nr_atendimento, 'U'), 1, 110) cd_usuario_convenio,
                   NULL nr_seq_exame,
                   NULL nr_seq_cons,
                   b.nr_sequencia nr_seq_radio,
                   NULL nr_seq_quimio,
                   substr(obter_valor_dominio(2217, b.ie_status_agenda), 1, 110) ds_status,
                   to_number(obter_dados_atendimento(b.nr_atendimento, 'CC')) cd_convenio_exame,
                   NULL cd_convenio_consulta,
                   b.cd_pessoa_fisica cd_pessoa_fisica_exame,
                   NULL cd_pessoa_fisica_consulta,
                   b.dt_agenda dt_agenda_exame,
                   NULL dt_agenda_consulta,
                   b.ie_status_agenda ie_status_agenda_exame,
                   NULL ie_status_agenda_consulta,
                   '' ds_obs_final,
                   '' ie_encaixe,
                   NULL nr_seq_ageint,
                   NULL cd_estabelecimento,
                   0 nr_seq_agenda,
                   0 nr_seq_item,
                   NULL nr_reserva,
                   'N' ie_duplicado_estab,
                   0 nr_seq_ageint_item,
                   0 nr_seq_lab,
                   NULL ds_observacao,
                   NULL nm_usuario_origem,
                   NULL dt_resultado,
                   NULL dt_entrega_prevista,
                   NULL ie_transferido,
                   0 nr_seq_mot_reagendamento,
                   substr(rxt_obter_desc_classif_agenda(b.ie_classif_agenda), 1, 255) ds_classificacao,
				   NULL nr_secao_consulta
              FROM rxt_agenda b
             WHERE
             ----------------------       
                 b.cd_pessoa_fisica = cd_paciente_p
             AND b.dt_agenda BETWEEN dt_inicio_w AND dt_final_w
            
             AND (cd_convenio_p IS NULL OR to_number(obter_dados_atendimento(b.nr_atendimento, 'CC')) = cd_convenio_p)
             AND (nr_reserva_pac_p IS NULL)
             AND (cd_protocolo_p IS NULL)
            
             AND ((ie_forma_apres_p = '0') OR 
                 ((ie_forma_apres_p = '1') AND (ie_status_agenda <> 'C')) OR
                 ((ie_forma_apres_p = '2') AND (ie_status_agenda = 'C')));
             -----------
        CURSOR c04 IS
            SELECT obter_desc_expressao(701396) ds_tipo_agenda,
                   substr(obter_descricao_padrao('QT_LOCAL', 'DS_LOCAL', c.nr_seq_local), 1, 60) ds_agenda,
                   c.dt_agenda dt_ordenacao,
                   11 cd_tipo_agenda,
                   substr(obter_desc_prot_medic(e.nr_seq_paciente), 1, 255) ds_item,
                   substr(obter_nome_convenio(f.cd_convenio), 1, 255) ds_convenio,
                   c.nr_minuto_duracao,
                   c.nr_atendimento,
                   substr(obter_idade_pf(c.cd_pessoa_fisica, c.dt_agenda, 'A'), 1, 30) ds_idade,
                   substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'T'), 1, 60) nr_telefone,
                   substr(obter_desc_plano_conv(f.cd_convenio, f.cd_plano), 1, 255) ds_plano,
                   substr(obter_categoria_convenio(f.cd_convenio, f.cd_categoria), 1, 255) ds_categoria,
                   f.cd_usuario_convenio cd_usuario_convenio,
                   NULL nr_seq_exame,
                   NULL nr_seq_cons,
                   NULL nr_seq_radio,
                   c.nr_sequencia nr_seq_quimio,
                   substr(obter_valor_dominio(3192, c.ie_status_agenda), 1, 110) ds_status,
                   f.cd_convenio cd_convenio_exame,
                   NULL cd_convenio_consulta,
                   c.cd_pessoa_fisica cd_pessoa_fisica_exame,
                   NULL cd_pessoa_fisica_consulta,
                   c.dt_agenda dt_agenda_exame,
                   NULL dt_agenda_consulta,
                   c.ie_status_agenda ie_status_agenda_exame,
                   NULL ie_status_agenda_consulta,
                   '' ds_obs_final,
                   c.ie_encaixe,
                   NULL nr_seq_ageint,
                   e.cd_estabelecimento,
                   0 nr_seq_agenda,
                   0 nr_seq_item,
                   NULL nr_reserva,
                   'N' ie_duplicado_estab,
                   0 nr_seq_ageint_item,
                   0 nr_seq_lab,
                   NULL ds_observacao,
                   NULL nm_usuario_origem,
                   NULL dt_resultado,
                   NULL dt_entrega_prevista,
                   NULL ie_transferido,
                   c.nr_seq_mot_reagendamento nr_seq_mot_reagendamento,
                   '' ds_classificacao,
				   NULL nr_secao_consulta
              FROM agenda_quimio           c,
                   paciente_atendimento    d,
                   paciente_setor          e,
                   paciente_setor_convenio f
             WHERE c.nr_seq_atendimento = d.nr_seq_atendimento
               AND d.nr_seq_paciente = e.nr_seq_paciente
               AND e.nr_seq_paciente = f.nr_seq_paciente(+)
               ----------------------       
               AND c.cd_pessoa_fisica = cd_paciente_p
               AND c.dt_agenda BETWEEN dt_inicio_w AND dt_final_w
                  
               AND (cd_convenio_p IS NULL OR f.cd_convenio = cd_convenio_p)
               AND (nr_reserva_pac_p IS NULL)
               AND (cd_protocolo_p IS NULL)
                  
               AND ((ie_forma_apres_p = '0') OR 
                   ((ie_forma_apres_p = '1') AND (ie_status_agenda <> 'C')) OR
                   ((ie_forma_apres_p = '2') AND (ie_status_agenda = 'C')));
               -----------     
    
        CURSOR c05 IS
            SELECT obter_desc_expressao(701396) ds_tipo_agenda,
                   substr(obter_descricao_padrao('QT_LOCAL', 'DS_LOCAL', c.nr_seq_local), 1, 60) ds_agenda,
                   c.dt_agenda dt_ordenacao,
                   11 cd_tipo_agenda,
                   substr(qt_obter_desc_tipo_agenda(b.ie_tipo_pend_agenda), 1, 255) ds_item,
                   substr(obter_nome_convenio(a.cd_convenio), 1, 255) ds_convenio,
                   c.nr_minuto_duracao,
                   c.nr_atendimento,
                   substr(obter_idade_pf(c.cd_pessoa_fisica, c.dt_agenda, 'A'), 1, 30) ds_idade,
                   substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'T'), 1, 60) nr_telefone,
                   substr(obter_desc_plano_conv(a.cd_convenio, a.cd_plano), 1, 255) ds_plano,
                   substr(obter_categoria_convenio(a.cd_convenio, a.cd_categoria), 1, 255) ds_categoria,
                   a.cd_usuario_convenio cd_usuario_convenio,
                   NULL nr_seq_exame,
                   NULL nr_seq_cons,
                   NULL nr_seq_radio,
                   c.nr_sequencia nr_seq_quimio,
                   substr(obter_valor_dominio(3192, c.ie_status_agenda), 1, 110) ds_status,
                   a.cd_convenio cd_convenio_exame,
                   NULL cd_convenio_consulta,
                   c.cd_pessoa_fisica cd_pessoa_fisica_exame,
                   NULL cd_pessoa_fisica_consulta,
                   c.dt_agenda dt_agenda_exame,
                   NULL dt_agenda_consulta,
                   c.ie_status_agenda ie_status_agenda_exame,
                   NULL ie_status_agenda_consulta,
                   '' ds_obs_final,
                   c.ie_encaixe,
                   NULL nr_seq_ageint,
                   a.cd_estabelecimento,
                   0 nr_seq_agenda,
                   b.nr_sequencia nr_seq_item,
                   NULL nr_reserva,
                   nvl(b.ie_duplicado_estab, 'N') ie_duplicado_estab,
                   b.nr_sequencia nr_seq_ageint_item,
                   0 nr_seq_lab,
                   NULL ds_observacao,
                   NULL nm_usuario_origem,
                   NULL dt_resultado,
                   NULL dt_entrega_prevista,
                   NULL ie_transferido,
                   c.nr_seq_mot_reagendamento nr_seq_mot_reagendamento,
                   '' ds_classificacao,
				   NULL nr_secao_consulta
              FROM agenda_quimio         c,
                   agenda_integrada_item b,
                   agenda_integrada      a
             WHERE c.nr_seq_ageint_item = b.nr_sequencia
               AND b.nr_seq_agenda_int = a.nr_sequencia
               ----------------------       
               AND c.cd_pessoa_fisica = cd_paciente_p
               AND c.dt_agenda BETWEEN dt_inicio_w AND dt_final_w
                  
               AND (cd_convenio_p IS NULL OR a.cd_convenio = cd_convenio_p)
               AND (nr_reserva_pac_p IS NULL)
               AND (cd_protocolo_p IS NULL OR b.nr_seq_agenda_int = cd_protocolo_p)
                  
               AND ((ie_forma_apres_p = '0') OR 
                   ((ie_forma_apres_p = '1') AND (ie_status_agenda <> 'C')) OR
                   ((ie_forma_apres_p = '2') AND (ie_status_agenda = 'C')));
                -----------   
         CURSOR C06 IS 
         SELECT obter_desc_expressao(289736) ds_tipo_agenda,
                NULL ds_agenda,
                b.dt_prevista dt_ordenacao,
                20 cd_tipo_agenda,
                substr(nvl(obter_desc_proc_interno(b.nr_seq_proc_interno), obter_desc_exame(b.nr_seq_exame)), 1, 255) ds_item,
                substr(ageint_obter_conv_item_lab(b.nr_sequencia), 1, 255) ds_convenio,
                0 nr_minuto_duracao,
                0 nr_atendimento,
                substr(obter_idade_pf(a.cd_pessoa_fisica, b.dt_prevista, 'A'), 1, 30) ds_idade,
                substr(obter_compl_pf(a.cd_pessoa_fisica, 1, 'T'), 1, 60) nr_telefone,
                substr(obter_desc_plano(a.cd_convenio, a.cd_plano), 1, 255) ds_plano,
                substr(obter_categoria_convenio(a.cd_convenio, a.cd_categoria), 1, 255) ds_categoria,
                a.cd_usuario_convenio cd_usuario_convenio,
                0 nr_seq_exame,
                NULL nr_seq_cons,
                NULL nr_seq_radio,
                NULL nr_seq_quimio,
                substr(decode(b.dt_cancelamento, NULL, obter_desc_status_ageint(a.nr_seq_status), obter_desc_expressao(322155)),
                       1,
                       80) ds_status,
                NULL cd_convenio_exame,
                a.cd_convenio cd_convenio_consulta,
                '' cd_pessoa_fisica_exame,
                a.cd_pessoa_fisica cd_pessoa_fisica_consulta,
                NULL dt_agenda_exame,
                b.dt_prevista dt_agenda_consulta,
                NULL ie_status_agenda_exame,
                NULL ie_status_agenda_consulta,
                substr(a.ds_obs_final, 1, 255) ds_obs_final,
                'N' ie_encaixe,
                b.nr_seq_ageint nr_seq_ageint,
                a.cd_estabelecimento,
                0 nr_seq_agenda,
                0 nr_seq_item,
                '' nr_reserva,
                'N' ie_duplicado_estab,
                0 nr_seq_ageint_item,
                b.nr_sequencia,
                NULL ds_observacao,
                NULL nm_usuario_origem,
                NULL dt_resultado,
                NULL dt_entrega_prevista,
                NULL ie_transferido,
                0 nr_seq_mot_reagendamento,
                '',
				NULL nr_secao_consulta
           FROM ageint_exame_lab b,
                agenda_integrada a
          WHERE b.nr_seq_ageint = a.nr_sequencia
            AND b.dt_prevista IS NOT NULL
            ----------------------       
            AND a.cd_pessoa_fisica = cd_paciente_p
            AND b.dt_prevista BETWEEN dt_inicio_w AND dt_final_w
                  
            AND (cd_convenio_p IS NULL OR a.cd_convenio = cd_convenio_p)
            AND (nr_reserva_pac_p IS NULL )
            AND (cd_protocolo_p IS NULL )
                  
            AND ((ie_forma_apres_p = '0')
                --OR                ((ie_forma_apres_p = '1') AND (ie_status_agenda <> 'C')) 
                --OR                ((ie_forma_apres_p = '2') AND (ie_status_agenda = 'C')) 
                --OR                ((ie_forma_apres_p = '3') AND (cd_tipo_agenda <> 20))
                );
            -----------            
            

          
    
 
    
    BEGIN
    
        OPEN c01;
        LOOP
            FETCH c01 BULK COLLECT
                INTO agendamento_paciente_w LIMIT c_limit;
        
            EXIT WHEN agendamento_paciente_w.count = 0;
            FOR i IN agendamento_paciente_w.first .. agendamento_paciente_w.last LOOP
            
                PIPE ROW(agendamento_paciente_w(i));
            END LOOP;
        END LOOP;
        CLOSE c01;
    
        OPEN c02;
        LOOP
            FETCH c02 BULK COLLECT
                INTO agendamento_paciente_w LIMIT c_limit;
        
            EXIT WHEN agendamento_paciente_w.count = 0;
            FOR i IN agendamento_paciente_w.first .. agendamento_paciente_w.last LOOP
            
                PIPE ROW(agendamento_paciente_w(i));
            END LOOP;
        END LOOP;
    	  CLOSE c02;
        
        OPEN c03;
        LOOP
            FETCH c03 BULK COLLECT
                INTO agendamento_paciente_w LIMIT c_limit;
        
            EXIT WHEN agendamento_paciente_w.count = 0;
            FOR i IN agendamento_paciente_w.first .. agendamento_paciente_w.last LOOP
            
                PIPE ROW(agendamento_paciente_w(i));
            END LOOP;
        END LOOP;
    	  CLOSE c03;
        
        OPEN c04;
        LOOP
            FETCH c04 BULK COLLECT
                INTO agendamento_paciente_w LIMIT c_limit;
        
            EXIT WHEN agendamento_paciente_w.count = 0;
            FOR i IN agendamento_paciente_w.first .. agendamento_paciente_w.last LOOP
            
                PIPE ROW(agendamento_paciente_w(i));
            END LOOP;
        END LOOP;
        CLOSE c04;
    
        OPEN c05;
        LOOP
            FETCH c05 BULK COLLECT
                INTO agendamento_paciente_w LIMIT c_limit;
        
            EXIT WHEN agendamento_paciente_w.count = 0;
            FOR i IN agendamento_paciente_w.first .. agendamento_paciente_w.last LOOP
            
                PIPE ROW(agendamento_paciente_w(i));
            END LOOP;
        END LOOP;
        CLOSE c05;
        
        OPEN c06;
        LOOP
            FETCH c06 BULK COLLECT
                INTO agendamento_paciente_w LIMIT c_limit;
        
            EXIT WHEN agendamento_paciente_w.count = 0;
            FOR i IN agendamento_paciente_w.first .. agendamento_paciente_w.last LOOP
            
                PIPE ROW(agendamento_paciente_w(i));
            END LOOP;
        END LOOP;
        CLOSE c06;
    
    END;
END;
/
