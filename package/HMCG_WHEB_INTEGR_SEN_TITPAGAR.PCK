create or replace
package HMCG_WHEB_INTEGR_SEN_TITPAGAR is

procedure cancelar_tit_pagar(	cd_filial_p 		varchar2,
				cd_fornecedor_p		varchar2,
				nr_titulo_p 		varchar2,
				nr_seq_motivo_cancel_p	varchar2,
				dt_cancelamento_p	date,
				nm_usuario_p		varchar2,
				ds_erro_p		out varchar2);

procedure excluir_titulo_pagar(	cd_filial_p 		varchar2,
				cd_fornecedor_p		varchar2,
				nr_titulo_p 		varchar2,
				nr_fatura_p		varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p		out varchar2);
				
procedure gerar_titulo_pagar(	cd_estabelecimento_p	varchar2,
				cd_fornecedor_p		varchar2,
				nr_titulo_p 		varchar2,
				cd_empresa_p		varchar2,
				ie_situacao_p		varchar2,
				cd_cgc_p		varchar2,
				cd_pessoa_fisica_p	varchar2,
				ie_tipo_titulo_p	varchar2,
				nr_docto_origem_tit_p	varchar2,
				dt_emissao_p		date,
				dt_vencimento_p		date,
				vl_titulo_p		number,
				vl_saldo_titulo_p	number,
				ds_observacao_p		varchar2,
				cd_grupo_conta_p	varchar2,
				dt_entrada_p		date,
				nr_nosso_numero_p	varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p out		varchar2);

procedure gerar_titpagar_classif(	nr_titulo_p		varchar2,
					cd_centro_custo_p	varchar2,
					cd_conta_contabil_p	varchar2,
					vl_titulo_p		number,
					nm_usuario_p		varchar2,
					ds_erro_p	out varchar2);
				
end HMCG_WHEB_INTEGR_SEN_TITPAGAR;
/
create or replace
package body HMCG_WHEB_INTEGR_SEN_TITPAGAR is

	procedure cancelar_tit_pagar(cd_filial_p 		varchar2,
				cd_fornecedor_p		varchar2,
				nr_titulo_p 		varchar2,
				nr_seq_motivo_cancel_p	varchar2,
				dt_cancelamento_p	date,
				nm_usuario_p		varchar2,
				ds_erro_p		out varchar2) is

	nr_titulo_w		number(10);
	dt_cancelamento_w	date;
	ds_erro_w		varchar2(255);
	begin
	
	dt_cancelamento_w	:= nvl(dt_cancelamento_p,sysdate);
	
	select	nvl(max(nr_titulo),0)
	into	nr_titulo_w
	from	titulo_pagar
	where	nr_titulo_externo	= nr_titulo_p;
	
	if	(nr_titulo_w <> 0) then
		begin
		cancelar_titulo_pagar(	nr_titulo_w, nm_usuario_p, dt_cancelamento_w);
		exception when others then
			ds_erro_w	:= substr('Erro ao cancelar o titulo no Tasy: (' || nr_titulo_w || ') ' || sqlerrm(sqlcode),1,255);
		end;
		/*commit;*/
	end if;
	end;
	
	procedure excluir_titulo_pagar(	cd_filial_p 		varchar2,
					cd_fornecedor_p		varchar2,
					nr_titulo_p 		varchar2,
					nr_fatura_p		varchar2,
					nm_usuario_p		varchar2,
					ds_erro_p		out varchar2) is

	nr_titulo_w		number(10);
	nr_lote_contabil_w	number(10);
	ds_erro_w		varchar2(255);
	
	begin
	
	select	nvl(max(nr_titulo),0)
	into	nr_titulo_w
	from	titulo_Pagar
	where 	nr_titulo_externo = nr_titulo_p;
	
	select	nvl(max(nr_lote_contabil),0)
	into	nr_lote_contabil_w
	from	titulo_Pagar
	where	nr_titulo	= nr_titulo_w;
	
	if	(nr_lote_contabil_w > 0) then
		ds_erro_w	:= 'Este t�tulo (' || nr_titulo_w || ') j� foi contabilizado no Tasy. N�o pode ser exclu�do!';
	end if;
	
	if	(nvl(ds_erro_w,'X') = 'X') and
		(nr_titulo_w <> 0) then
		begin
		
		delete	from titulo_pagar
		where	nr_titulo	= nr_titulo_w;
		
		insert into log_tasy(
			cd_log,
			ds_log,
			nm_usuario,
			dt_atualizacao)
		values(	55788,
			'T�tulo exclu�do pela Integra��o: ' || nr_titulo_w || '/' || nr_titulo_p,
			nm_usuario_p,
			sysdate);
		exception when others then
			ds_erro_w	:= substr('Erro ao excluir o titulo no Tasy: ' || nr_titulo_w || chr(13) || sqlerrm(sqlcode),1,255);
			
		end;
	end if;
	
	ds_erro_p	:= ds_erro_w;
	/*commit; Estas procedures n�o podem ter commit porque possuem par�metros OUT via DBLink Erro ORCL*/
	end;
	
	procedure gerar_titulo_pagar(	cd_estabelecimento_p 	varchar2,
					cd_fornecedor_p		varchar2,
					nr_titulo_p 		varchar2,
					cd_empresa_p		varchar2,
					ie_situacao_p		varchar2,
					cd_cgc_p		varchar2,
					cd_pessoa_fisica_p	varchar2,
					ie_tipo_titulo_p	varchar2,
					nr_docto_origem_tit_p	varchar2,
					dt_emissao_p		date,
					dt_vencimento_p		date,
					vl_titulo_p		number,
					vl_saldo_titulo_p	number,
					ds_observacao_p		varchar2,
					cd_grupo_conta_p	varchar2,
					dt_entrada_p		date,
					nr_nosso_numero_p	varchar2,
					nm_usuario_p		varchar2,
					ds_erro_p		out varchar2) is
		
	ds_erro_w			varchar2(255);
	
	cd_tipo_taxa_juro_w		number(10);
	CD_TIPO_TAXA_MULTA_w		number(10);
	ie_tipo_titulo_w		number(2);
	cd_fornecedor_w			varchar2(10);
	cd_pessoa_fisica_w		varchar2(10);
	dt_vencimento_w			date;
	nr_sequencia_w			number(10);
	nr_titulo_w			number(10);
	qt_registro_w			number(10);
	vl_titulo_w			number(15,2);
	
	begin
	
	
	
	begin
	ie_tipo_titulo_w	:= to_number(nvl(substr(ie_tipo_titulo_p,1,2),'1'));
	exception when others then
		ie_tipo_titulo_w	:= 1;	
	end;
	
	select	count(*)
	into	qt_registro_W
	from	estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_p;
	
	if	(qt_registro_w	= 0) then
		ds_erro_w	:= ds_erro_w || 'O estabelecimento: ' || cd_estabelecimento_p || ' n�o est� cadastrado no Tasy';
	end if;
	
	
	select	nvl(max(nr_titulo),0)
	into	nr_titulo_w
	from	titulo_pagar
	where	nr_titulo_externo	= nr_titulo_p
	and	nm_usuario_orig		= 'IntegrSenior'
	and	ie_situacao		<> 'C';
	
	select	nvl(max(cd_tipo_taxa_juro),0),
		nvl(max(cd_tipo_taxa_multa),0)
	into	cd_tipo_taxa_juro_w,
		cd_tipo_taxa_multa_w
	from	parametros_contas_pagar
	where	cd_estabelecimento	= cd_estabelecimento_p;
		
	cd_fornecedor_w	:= nvl(nvl(cd_pessoa_fisica_p,cd_fornecedor_p),'0');
	
	if	(cd_fornecedor_w <> '0') then
		select	nvl(max(cd_pessoa_fisica),'')
		into	cd_pessoa_fisica_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p/*cd_sistema_ant	= cd_fornecedor_w*/
		and	nm_usuario_nrec	= 'IntegrSenior';
		
		if	(nvl(cd_pessoa_fisica_w,'X') = 'X') then
			ds_erro_w	:= ds_erro_w || 'Fornecedor (PF) n�o encontrado no Tasy: ' || cd_fornecedor_w;
		end if;
	end if;
		
	if	(nvl(cd_cgc_p,'X') <> 'X') then
		
		select	count(*)
		into	qt_registro_w
		from	pessoa_juridica
		where	cd_cgc	= cd_cgc_p;
		
		if	(qt_registro_w = 0) then
			ds_erro_w	:= substr('Fornecedor (PJ) n�o encontrado no Tasy: ' || cd_cgc_p,1,255);
		end if;
	end if;
		
	if	(nr_titulo_w = 0) and
		(nvl(ds_erro_w,'X') = 'X') then
		begin
		
		select	titulo_pagar_seq.nextval	
		into	nr_titulo_w
		from	dual;
	
		insert	into titulo_pagar(
			cd_pessoa_fisica,
			cd_cgc,
			nr_titulo,
			cd_estabelecimento,     
			dt_atualizacao,         
			nm_usuario,             
			dt_emissao,             
			dt_vencimento_original,
			dt_vencimento_atual,    	
			vl_titulo,              
			vl_saldo_titulo,        
			vl_saldo_juros,         
			vl_saldo_multa,
			cd_moeda,               
			tx_juros,               
			tx_multa,               
			cd_tipo_taxa_juro,      
			cd_tipo_taxa_multa,     
			tx_desc_antecipacao,    
			ie_situacao,            
			ie_origem_titulo,
			ie_tipo_titulo,
			ie_pls,
			nr_titulo_externo,
			nr_documento,
			nr_seq_trans_fin_baixa,
			nm_usuario_orig)
		values(	cd_pessoa_fisica_w,
			cd_cgc_p,
			nr_titulo_w,
			cd_estabelecimento_p,
			sysdate,
			nm_usuario_p,
			dt_emissao_p,
			dt_vencimento_p,
			dt_vencimento_p,
			vl_titulo_p,
			vl_saldo_titulo_p,
			0,
			0,
			1,
			0,
			0,
			cd_tipo_taxa_juro_w,
			cd_tipo_taxa_multa_w,
			null,
			'A',
			1,
			ie_tipo_titulo_w,
			'N',
			nr_titulo_p,
			substr(nr_titulo_p,1,20),
			null,
			'IntegrSenior');
			
		exception when others then
			ds_erro_w	:= substr('Erro ao gerar o titulo a pagar no Tasy: ' || sqlerrm(sqlcode),1,255);
		end;
	elsif	(nr_titulo_w > 0) then
	
		select	nvl(vl_titulo,0),
			dt_vencimento_atual
		into	vl_titulo_w,
			dt_vencimento_w
		from	titulo_pagar
		where	nr_titulo	= nr_titulo_w;
		
		if	(vl_titulo_w <> vl_titulo_p) then
			/* alt valor */
			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	titulo_pagar_alt_valor
			where	nr_titulo	= nr_titulo_w;
			
			nr_sequencia_w	:= nr_sequencia_w + 1;
			
			begin
			insert into titulo_pagar_alt_valor ( 
				nr_titulo, nr_sequencia, dt_alteracao, 
				vl_anterior, vl_alteracao, cd_moeda, 
				dt_atualizacao, nm_usuario, nr_lote_contabil, 
				ds_observacao, nr_seq_trans_fin, nr_externo, 
				nr_seq_motivo )	 
			values( nr_titulo_w,
				nr_sequencia_w, sysdate, 
				vl_titulo_w, 
				vl_titulo_p,
				1, 
				sysdate,
				nm_usuario_p,
				0, 
				'Alterado via integra��o',
				null,
				null, 
				null);
			exception when others then
				ds_erro_w	:= substr('Erro ao alterar o valor do t�tulo (' || nr_titulo_w || ') no Tasy: ' || sqlerrm(sqlcode),1,255);
			end;
		end if;
		
		if	(dt_vencimento_w <> dt_vencimento_p) then
		
			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	titulo_pagar_alt_venc
			where	nr_titulo	= nr_titulo_w;
			
			nr_sequencia_w	:= nr_sequencia_w + 1;
			
			insert into titulo_pagar_alt_venc ( 
				nr_titulo, 
				nr_sequencia,
				dt_anterior, 
				dt_vencimento, 
				dt_atualizacao, 
				nm_usuario, 
				dt_alteracao, 
				ds_observacao, 
				vl_saldo_multa, 
				vl_saldo_juros, 
				tx_juros, 
				tx_multa, 
				nr_externo) 
			values( nr_titulo_w,
				nr_sequencia_w,
				sysdate, 
				dt_vencimento_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				'Alterado via integra��o',
				0, 
				0,
				0,
				0, 
				null);
		end if;
		
	end if;
	
	ds_erro_p	:= ds_erro_w;
	
	end;
	
	procedure gerar_titpagar_classif(	nr_titulo_p		varchar2,
					cd_centro_custo_p	varchar2,
					cd_conta_contabil_p	varchar2,
					vl_titulo_p		number,
					nm_usuario_p		varchar2,
					ds_erro_p		out varchar2) is
		
	cd_centro_custo_w		number(10);
	cd_conta_contabil_w		varchar2(20);
	ds_erro_w			varchar2(255);
	nr_titulo_w			number(10);
	vl_titulo_w			number(15,2);
	vl_classif_w			number(15,2);
	vl_titulo_classif_w		number(15,2);
	nr_sequencia_w			number(10)	:= 0;
	begin
	
	vl_classif_w	:= nvl(vl_titulo_p,0);
	
	select	nvl(max(cd_centro_custo),0)
	into	cd_centro_custo_w
	from	centro_custo
	where	cd_sistema_contabil	= cd_centro_custo_p;
	
	if	(cd_centro_custo_w = 0) then
		ds_erro_w	:= substr(ds_erro_w || 'Centro de custo n�o cadastrado no Tasy: ' || cd_centro_custo_p,1,255);
	end if;
	
	select	nvl(max(cd_conta_contabil),'0')
	into	cd_conta_contabil_w
	from	conta_contabil
	where	cd_sistema_contabil	= cd_conta_contabil_p;
	
	if	(cd_conta_contabil_w = '0') then
		ds_erro_w	:= substr(ds_erro_w || 'Conta cont�bil n�o cadastrada no Tasy: ' || cd_conta_contabil_p,1,255);
	end if;
	
		
	select	nvl(max(nr_titulo),0)
	into	nr_titulo_w
	from	titulo_Pagar
	where 	nr_titulo_externo = nr_titulo_p;
	
	if	(nr_titulo_w <> 0) and
		(nvl(ds_erro_w,'X') = 'X') then
		begin
		select	nvl(max(vl_titulo),0)
		into	vl_titulo_w
		from	titulo_pagar
		where	nr_titulo	= nr_titulo_w;
		
		select	nvl(sum(vl_titulo),0),
			nvl(max(nr_sequencia),0)
		into	vl_titulo_classif_w,
			nr_sequencia_w
		from	titulo_pagar_classif
		where	nr_titulo	= nr_titulo_w;
	
		vl_titulo_classif_w	:= vl_titulo_classif_w + vl_titulo_p;
		if	(vl_titulo_classif_w > vl_titulo_w) then
			ds_erro_w	:= substr('O total dos rateios � superior ao valor total do t�tulo',1,255);
		end if;
		
		if	(nvl(ds_erro_w,'X') = 'X') then
			begin
			nr_sequencia_w	:= nr_sequencia_w + 1;
			insert into titulo_pagar_classif(
				nr_titulo,
				nr_sequencia,
				cd_centro_custo,
				cd_conta_contabil,
				vl_titulo,
				dt_atualizacao,
				nm_usuario)
			values(	nr_titulo_w,
				nr_sequencia_w,
				cd_centro_custo_w,
				cd_conta_contabil_w,
				vl_classif_w,
				sysdate,
				nm_usuario_p);
			exception when others then
				excluir_titulo_pagar(	null, null, nr_titulo_p, null, nm_usuario_p, ds_erro_w);
				ds_erro_w	:= substr('Erro ao gerar a classifica��o do t�tulo no Tasy: (' || nr_titulo_w || ') ' || sqlerrm(sqlcode),1,255);
			end;
		end if;
		end;
	elsif	(nr_titulo_w = 0) then
		ds_erro_w	:= substr(ds_erro_w || ' T�tulo n�o encontrado no Tasy: ' || nr_titulo_p,1,255);
	end if;
	
	
	
	if	(nvl(ds_erro_w,'X') <> 'X') and
		(nr_titulo_w <> 0) then
		excluir_titulo_pagar(null, null, nr_titulo_p, null, nm_usuario_p, ds_erro_w);
	end if;
	ds_erro_p	:= substr(ds_erro_w,1,255);
	end;
	
end HMCG_WHEB_INTEGR_SEN_TITPAGAR;
/